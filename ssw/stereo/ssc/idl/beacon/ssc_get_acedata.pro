;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_GET_ACEDATA
;
; Purpose     :	SSC-specific front-end to SolarSoft ACE routines
;
; Category    :	STEREO, Telemetry
;
; Explanation : This routine serves as a front-end to the ACE routines in the
;               SolarSoft library, and is specifically for use by the STEREO
;               Science Center.  Returns the SWEPAM and MAG data from ACE.  The
;               MAG data is converted from GSM to Geocentric RTN coordinates.
;
; Syntax      :	SSC_GET_ACEDATA, TIMERANGE, SWEPAM, MAG
;
; Examples    :	See SSC_INSITU_BEACON_PANELS
;
; Inputs      :	TIMERANGE = The time range over which to read the data.
;
; Opt. Inputs :	None.
;
; Outputs     :	SWEPAM = Structure array containing the SWEPAM data.
;
;               MAG    = Structure array containing the MAG data.  The Bx, By,
;                        Bz, Lat, and Long tags are modified to convert from
;                        GSM to Geocentric RTN (GRTN) coordinates.
;
; Opt. Outputs:	None.
;
; Keywords    :	/REALTIME = If set, then bypass GET_ACEDATA, and directly form
;                           the names of the files to read.  This greatly
;                           increases the speed of the processing.  The data
;                           from yesterday, today, and "now" are read in,
;                           processed, and stored in a common block.  Only if
;                           the file timestamps change are the files read in
;                           again.
;
;               Will also accept any other READ_ACE/GET_ACEDATA keywords.
;
; Calls       :	CONCAT_DIR, ANYTIM2CAL, READ_ACE, GET_ACEDATA
;
; Common      :	Uses the internal common block SSC_GET_ACEDATA.
;
; Restrictions:	When used with the /REALTIME qualifier, the full functionality
;               of GET_ACEDATA is not implemented.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 2008-08-11, William Thompson, GSFC
;               Version 2, 2008-08-20, WTT, catch read errors.
;               Version 3, 2008-09-29, WTT, move FILE_INFO to just before read
;               Version 4, 2008-10-08, WTT, more robust concatenation.
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_get_acedata, timerange, swepam, mag, realtime=realtime, _extra=_extra
;
;  Cache already read-in data in the common block.
;
common ssc_get_acedata, yesterday, today, $
  swepam_mtime, swepam_yesterday, swepam_today, swepam_now, $
  mag_mtime, mag_yesterday, mag_today, mag_now
;
;  Check the input parameters.
;
if n_params() ne 3 then message, $
  'Syntax: SSC_GET_ACEDATA, TIMERANGE, SWEPAM, MAG'
if n_elements(timerange) ne 2 then message, $
  'TIMERANGE must have two elements'
;
;  Initialize various common block parameters.
;
if n_elements(swepam_mtime) eq 0 then swepam_mtime = lon64arr(3)
if n_elements(mag_mtime)    eq 0 then mag_mtime    = lon64arr(3)
if n_elements(yesterday)     eq 0 then yesterday     = '00000000'
if n_elements(today)         eq 0 then today         = '00000000'
;
;  If the REALTIME keyword was passed, then form the names of the files to read
;  in.  Start by forming the name of the directory containing the files.
;
if keyword_set(realtime) then begin
    acedir = concat_dir(getenv('SSWDB'), 'ace')
    acedir = concat_dir(acedir, 'daily')
;
;  Convert the time range into date strings, and compare them against those
;  stored in the common block.  If they don't match, then force the files to be
;  read in again.
;
    cal = anytim2cal(timerange[0], /date, form=8)
    get_utc, utc
    cal = [cal, anytim2cal(utc, /date, form=8)]
    if (cal[0] ne yesterday) or (cal[1] ne today) then begin
        yesterday = cal[0]
        today     = cal[1]
        swepam_mtime[*] = 0
        mag_mtime[*]    = 0
    endif
;
;  Form the names of the SWEPAM data files, and get their modification times.
;
    filename = 'ace_swepam_1m.txt'
    files = cal + '_' + filename
    files = [files, filename]
    files = concat_dir(acedir, files)
;
;  If the modification times have changed, then read in the new versions of the
;  files.
;
    for i=0,2 do begin
        info = file_info(files[i])
        if (info.mtime ne swepam_mtime[i]) and (info.exists eq 1) then begin
            read_ace, files[i], swepam, /valid, _extra=_extra
            if datatype(swepam) eq 'STC' then begin
                swepam_mtime[i] = info.mtime
                case i of
                    0: swepam_yesterday = swepam
                    1: swepam_today     = swepam
                    2: swepam_now       = swepam
                endcase
            endif
        endif
    endfor
;
;  Form the names of the MAG data files, and get their modification times.
;
    filename = 'ace_mag_1m.txt'
    files = cal + '_' + filename
    files = [files, filename]
    files = concat_dir(acedir, files)
;
;  If the modification times have changed, then read in and process the new
;  versions of the files.
;
    for i=0,2 do begin
        info = file_info(files[i])
        if (info.mtime ne mag_mtime[i]) and (info.exists eq 1) then begin
            read_ace, files[i], mag, /valid, _extra=_extra
            if datatype(mag) eq 'STC' then begin
                mag_mtime[i] = info.mtime
;
;  Convert from GSM to Geocentric RTN coordinates.
;
                coord = fltarr(3,n_elements(mag))
                coord[0,*] = mag.bx
                coord[1,*] = mag.by
                coord[2,*] = mag.bz
                convert_stereo_coord, mag, coord, 'gsm', 'grtn'
                mag.bx = reform(coord[0,*])
                mag.by = reform(coord[1,*])
                mag.bz = reform(coord[2,*])
                mag.lat = reform(asin(coord[2,*]/mag.bt))  * !radeg
                lon = reform(atan(coord[1,*], coord[0,*])) * !radeg
                w = where(lon lt 0, count)
                if count gt 0 then lon[w] = lon[w] + 360
                mag.long = lon
;
                case i of
                    0: mag_yesterday = mag
                    1: mag_today     = mag
                    2: mag_now       = mag
                endcase
            endif
        endif
    endfor
;
;  Concatenate together the data from yesterday, today, and "now".
;
    if n_elements(swepam_yesterday) gt 0 then swepam = swepam_yesterday
    if n_elements(swepam_today) gt 0 then begin
        if n_elements(swepam) eq 0 then swepam = swepam_today else $
          swepam = [swepam, swepam_today]
    endif
    if n_elements(swepam_now) gt 0 then begin
        if n_elements(swepam) eq 0 then swepam = swepam_now else $
          swepam = [swepam, swepam_now]
    endif
;
    if n_elements(mag_yesterday) gt 0 then mag = mag_yesterday
    if n_elements(mag_today) gt 0 then begin
        if n_elements(mag) eq 0 then mag = mag_today else $
          mag = [mag, mag_today]
    endif
    if n_elements(mag_now) gt 0 then begin
        if n_elements(mag) eq 0 then mag = mag_now else $
          mag = [mag, mag_now]
    endif
;
;  If not doing realtime processing, then use GET_ACEDATA to retrieve the data
;  over the requested time range.
;
end else begin
    swepam = get_acedata(timerange[0], timerange[1], /swepam, /valid, $
                         _extra=_extra)
    mag = get_acedata(timerange[0], timerange[1], /mag, /valid, _extra=_extra)
;
;  Convert the magnetometer data from GSM to Geocentric RTN coordinates.
;
    coord = fltarr(3,n_elements(mag))
    coord[0,*] = mag.bx
    coord[1,*] = mag.by
    coord[2,*] = mag.bz
    convert_stereo_coord, mag, coord, 'gsm', 'grtn'
    mag.bx = reform(coord[0,*])
    mag.by = reform(coord[1,*])
    mag.bz = reform(coord[2,*])
    mag.lat = reform(asin(coord[2,*]/mag.bt))  * !radeg
    lon = reform(atan(coord[1,*], coord[0,*])) * !radeg
    w = where(lon lt 0, count)
    if count gt 0 then lon[w] = lon[w] + 360
    mag.long = lon
endelse
;
return
end
