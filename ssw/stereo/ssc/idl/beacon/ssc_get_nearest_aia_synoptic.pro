;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_GET_NEAREST_AIA_SYNOPTIC
;
; Purpose     :	Retrieve nearest AIA 193 image to specified time.
;
; Category    :	
;
; Explanation :	This routine searches the AIA synoptic website to look for the
;               closest image to the requested time.  Called from
;               SSC_GET_NEAREST_AIA when it fails to find anything through the VSO.
;
; Syntax      :	SSC_GET_NEAREST_AIA_SYNOPTIC, DATE, HEADER, IMAGE, FOUND=FOUND, FILES=FILES
;
; Examples    :	See ssc_get_nearest_aia
;
; Inputs      :	DATE    = Date/time to look for.
;
; Opt. Inputs :	None.
;
; Outputs     :	HEADER  = FITS header of AIA image.
;               IMAGE   = Image array
;
; Opt. Outputs:	None.
;
; Keywords    :	WAVELENGTH = Wavelength to search for.  Default is 193.  If
;                            passed as 195, then automatically converted to 193.
;
;               FOUND   = Returns 1 if an image was found, 0 otherwise.
;
;               FILES   = Returns list of filenames found.  Needed if image is rejected.
;
; Calls       :	ANYTIM2UTC, UTC2TAI, CHECK_EXT_TIME, CONCAT_DIR, ANYTIM2TAI,
;               BOOST_ARRAY, BREAK_FILE, MK_TEMP_DIR, FXREAD
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 17-Feb-2011, William Thompson, GSFC
;               Version 2, 18-Mar-2011, WTT, added keyword FILES
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_get_nearest_aia_synoptic, date, header, image, wavelength=wavelength, found=found, $
                                  files=files
;
if n_elements(wavelength) eq 0 then wavelength = 193
swave = ntrim(wavelength)
if swave eq '195' then swave = '193'
;
aiapath = 'http://jsoc.stanford.edu/data/aia/synoptic/'
ext0 = anytim2utc(date, /external)
tai0 = utc2tai(ext0)
hdelta = 0
found = 0
while (not found) and (hdelta le 24) do begin
    delvarx, files, dates
    ext = ext0
    ext.hour = ext.hour - hdelta
    check_ext_time, ext
    timepath = concat_dir(aiapath, anytim2utc(ext, /ecs, /date_only) + '/H' + $
                          string(ext.hour, format='(I2.2)') + '00/')
    spawn, 'wget ' + timepath + ' -O -', temp
    for i=0,n_elements(temp)-1 do begin
        j1 = strpos(temp[i], '.fits')
        if j1 gt 0 then begin
            j0 = strpos(temp[i], 'href="') + 6
            name1 = strmid(temp[i], j0, j1-j0+5)
            tai1 = anytim2tai(strmid(name1,3,15))
            wave1 = fix(strmid(name1,19,4))
            if wave1 eq wavelength then begin
                boost_array, files, timepath + name1
                boost_array, tais, tai1
            endif
        endif
    endfor
;
    if hdelta ne 0 then begin
        ext = ext0
        ext.hour = ext.hour + hdelta
        check_ext_time, ext
        timepath = concat_dir(aiapath, anytim2utc(ext, /ecs, /date_only) + '/H' + $
                              string(ext.hour, format='(I2.2)') + '00/')
        spawn, 'wget ' + timepath + ' -O -', temp
        for i=0,n_elements(temp)-1 do begin
            j1 = strpos(temp[i], '.fits')
            if j1 gt 0 then begin
                j0 = strpos(temp[i], 'href="') + 6
                name1 = strmid(temp[i], j0, j1-j0+5)
                tai1 = anytim2tai(strmid(name1,3,15))
                wave1 = fix(strmid(name1,19,4))
                if wave1 eq wavelength then begin
                    boost_array, files, timepath + name1
                    boost_array, tais, tai1
                endif
            endif
        endfor
    endif
;
    if n_elements(files) gt 0 then begin
        diff = abs(tais - tai0)
        w = (where(diff eq min(diff)))[0]
        break_file, files[w], disk, dir, name, ext
        mk_temp_dir, get_temp_dir(), temp_dir
        tempfile = concat_dir(temp_dir, name+ext)
        spawn, 'wget ' + files[w] + ' -O ' + tempfile
        message = ''
        fxread, tempfile, image, header, errmsg=message
        if message eq '' then found = 1
        file_delete, temp_dir, /recursive
   endif
;
    hdelta = hdelta + 1
endwhile
;
return
end
