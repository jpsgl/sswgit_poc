;+
; Project     :	STEREO - IMPACT, PLASTIC
;
; Name        : SSC_INSITU_BEACON_PANELS_V1
;
; Purpose     : Plot STEREO in-situ space weather beacon data
;
; Category    :	STEREO, IMPACT, PLASTIC
;
; Explanation :	Plots STEREO in-situ space weather beacon data, for display on
;               the web.
;
; Syntax      :	SSC_INSITU_BEACON_PANELS_V1, IMPACT_A_CDFNAME, IMPACT_B_CDFNAME, $
;                       PLASTIC_A_CDFNAME, PLASTIC_B_CDFNAME
;
; Examples    :	See SSC_BEACON_PLOTS
;
; Inputs      :	IMPACT_A_CDFNAME  = List of IMPACT Beacon CDF filenames for the
;                                   Ahead spacecraft.  Null filenames are
;                                   ignored.
;               IMPACT_B_CDFNAME  = Same for the Behind spacecraft
;               PLASTIC_A_CDFNAME = Same for PLASTIC on Ahead spacecraft
;               PLASTIC_B_CDFNAME = Same for PLASTIC on Behind spacecraft
;
; Opt. Inputs :	None.
;
; Outputs     :	None.
;
; Opt. Outputs:	None.
;
; Keywords    :	TIMERANGE = Time range to plot.  If not passed, then determined
;                           from the data.
;
;               TICK_UNIT = Number of seconds between tick marks,
;                           e.g. TICK_UNIT=21600 for six hours.
;
;               TITLE     = Plot title.  If not passed, then generated
;                           automatically from the end time.
;
; Calls       :	None
;
; Common      : None
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	Based on STEREO_INSITU_BEACON_PANELS, 06-Feb-2006,
;                       Peter Schroeder, UC-Berkeley.
;
; History     :	Version 1, 10-Feb-2006, William Thompson, GSFC
;                       Rewrote to allow multiple files, both A & B spacecraft.
;               Version 2, 13-Jun-2006, William Thompson, GSFC
;                       Added keyword TITLE.
;               Version 3, 10-Jan-2007, William Thompson, GSFC
;                       Changed "Velocity" to "Velocity_HGRTN".
;                       Put in realistic limits for density and velocity
;               Version 4, 30-Jan-2007, William Thompson, GSFC
;                       Fixed bug with density scaling
;               Version 5, 2-Feb-2007, WTT, plot velocity magnitude only
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_insitu_beacon_panels_v1, impact_a_cdfname, impact_b_cdfname, $
                                 plastic_a_cdfname, plastic_b_cdfname, $
                                 timerange=timerange, tick_unit=tick_unit, $
                                 title=k_title
;
;  Set up some plotting constants.
;
charsize=!p.charsize
if charsize eq 0 then charsize=1
ysize = charsize*!d.y_ch_size
;
;  Define the CDF variable names.
;
mag_varname = 'MAGBField'
density_varname = 'Density'
velocity_varname = 'Velocity_HGRTN'
temp_varname = 'Temperature'
;
;  Initialize the input arrays, and read all the IMPACT-A CDF files.
;
delvarx, mag_epoch_a, mag_data_a
for ifile = 0,n_elements(impact_a_cdfname)-1 do begin
    if impact_a_cdfname[ifile] ne '' then begin
        impact_cdfid = cdf_open(impact_a_cdfname[ifile])
;
;  Get the correct epoch variable name for MAG data.
;
        cdf_attget, impact_cdfid, 'DEPEND_0', mag_varname, mag_epoch_name
;
;  Get the number of records for MAG data.
;
        cdf_control, impact_cdfid, variable = mag_epoch_name, $
          get_var_info = mag_epoch_info
        num_mag_vecs = mag_epoch_info.maxrec + 1
;
;  Read in the MAG data.
;
        if num_mag_vecs gt 0 then begin
            cdf_varget, impact_cdfid, mag_epoch_name, mag_epoch, $
                        rec_count = num_mag_vecs
            cdf_varget, impact_cdfid, mag_varname, mag_data, $
                        rec_count = num_mag_vecs
            mag_epoch = reform(mag_epoch)
;
;  Concatenate the data together.
;
            if n_elements(mag_epoch_a) eq 0 then begin
                mag_epoch_a = mag_epoch
                mag_data_a = mag_data
            end else begin
                mag_epoch_a = [mag_epoch_a, mag_epoch]
                mag_data_a = [[mag_data_a], [mag_data]]
            endelse
        endif
;
        cdf_close, impact_cdfid
    endif
endfor
;
;  Convert MAG epoch data into UTC.  Initialize EPOCH, and start accumulating
;  all the epoch data arrays.
;
delvarx, epoch
if n_elements(mag_epoch_a) gt 0 then begin
    epoch = mag_epoch_a
    mag_epoch_a_utc = cdf2utc(mag_epoch_a)
endif
;
;  Initialize the input arrays, and read all the IMPACT-B CDF files.
;
delvarx, mag_epoch_b, mag_data_b
for ifile = 0,n_elements(impact_b_cdfname)-1 do begin
    if impact_b_cdfname[ifile] ne '' then begin
        impact_cdfid = cdf_open(impact_b_cdfname[ifile])
;
;  Get the correct epoch variable name for MAG data.
;
        cdf_attget, impact_cdfid, 'DEPEND_0', mag_varname, mag_epoch_name
;
;  Get the number of records for MAG data.
;
        cdf_control, impact_cdfid, variable = mag_epoch_name, $
          get_var_info = mag_epoch_info
        num_mag_vecs = mag_epoch_info.maxrec + 1
;
;  Read in the MAG data.
;
        if num_mag_vecs gt 0 then begin
            cdf_varget, impact_cdfid, mag_epoch_name, mag_epoch, $
                        rec_count = num_mag_vecs
            cdf_varget, impact_cdfid, mag_varname, mag_data, $
                        rec_count = num_mag_vecs
            mag_epoch = reform(mag_epoch)
;
;  Concatenate the data together.
;
            if n_elements(mag_epoch_b) eq 0 then begin
                mag_epoch_b = mag_epoch
                mag_data_b = mag_data
            end else begin
                mag_epoch_b = [mag_epoch_b, mag_epoch]
                mag_data_b = [[mag_data_b], [mag_data]]
            endelse
        endif
;
        cdf_close, impact_cdfid
    endif
endfor
;
;  Convert MAG epoch data into UTC, and append to the EPOCH array.
;
if n_elements(mag_epoch_b) gt 0 then begin
    if n_elements(epoch) eq 0 then epoch = mag_epoch_b else $
      epoch = [epoch, mag_epoch_b]
    mag_epoch_b_utc = cdf2utc(mag_epoch_b)
endif
;
;  Initialize the input arrays, and read all the PLASTIC-A CDF files.
;
delvarx, density_epoch_a, density_data_a, velocity_epoch_a, velocity_data_a
for ifile = 0,n_elements(plastic_a_cdfname)-1 do begin
    if plastic_a_cdfname[ifile] ne '' then begin
        plastic_cdfid = cdf_open(plastic_a_cdfname[ifile])
;
;  Get epoch variable name for DENSITY.
;
        cdf_attget, plastic_cdfid, 'DEPEND_0', density_varname, $
          density_epoch_name
;
;  Get the number of DENSITY records.
;
        cdf_control, plastic_cdfid, variable = density_epoch_name, $
          get_var_info = density_epoch_info
        num_density_pts = density_epoch_info.maxrec + 1
;
;  Read in the DENSITY data.
;
        if num_density_pts gt 0 then begin
            cdf_varget, plastic_cdfid, density_epoch_name, density_epoch, $
                        rec_count = num_density_pts
            cdf_varget, plastic_cdfid, density_varname, density_data, $
                        rec_count = num_density_pts
            density_epoch = reform(density_epoch)
            density_data = reform(density_data)
;
;  Get epoch variable name for solar wind velocity variable.
;
            cdf_attget, plastic_cdfid, 'DEPEND_0', velocity_varname, $
                        velocity_epoch_name
;
;  Get the number of VELOCITY records.
;
            cdf_control, plastic_cdfid, variable = velocity_epoch_name, $
                         get_var_info = velocity_epoch_info
            num_velocity_pts = velocity_epoch_info.maxrec + 1
;
;  Read in the VELOCITY data.
;
            cdf_varget, plastic_cdfid, velocity_epoch_name, velocity_epoch, $
                        rec_count = num_velocity_pts
            cdf_varget, plastic_cdfid, velocity_varname, velocity_data, $
                        rec_count = num_velocity_pts
            velocity_epoch = reform(velocity_epoch)
;
;  Concatenate the data together.
;
            if n_elements(density_epoch_a) eq 0 then begin
                density_epoch_a = density_epoch
                density_data_a = density_data
                velocity_epoch_a = velocity_epoch
                velocity_data_a = velocity_data
            end else begin
                density_epoch_a = [density_epoch_a, density_epoch]
                density_data_a = [density_data_a, density_data]
                velocity_epoch_a = [velocity_epoch_a, velocity_epoch]
                velocity_data_a = [[velocity_data_a], [velocity_data]]
            endelse
        endif
;
        cdf_close, plastic_cdfid
    endif
endfor
;
;  Convert DENSITY and VELOCITY epoch data to UTC, and append to EPOCH.
;
if n_elements(density_epoch_a) gt 0 then begin
    temp = [density_epoch_a, velocity_epoch_a]
    if n_elements(epoch) eq 0 then epoch = temp else epoch = [epoch, temp]
    density_epoch_a_utc = cdf2utc(density_epoch_a)
    velocity_epoch_a_utc = cdf2utc(velocity_epoch_a)
endif
;
;  Initialize the input arrays, and read all the PLASTIC-A CDF files.
;
delvarx, density_epoch_b, density_data_b, velocity_epoch_b, velocity_data_b
for ifile = 0,n_elements(plastic_b_cdfname)-1 do begin
    if plastic_b_cdfname[ifile] ne '' then begin
        plastic_cdfid = cdf_open(plastic_b_cdfname[ifile])
;
;  Get epoch variable name for DENSITY.
;
        cdf_attget, plastic_cdfid, 'DEPEND_0', density_varname, $
          density_epoch_name
;
;  Get the number of DENSITY records.
;
        cdf_control, plastic_cdfid, variable = density_epoch_name, $
          get_var_info = density_epoch_info
        num_density_pts = density_epoch_info.maxrec + 1
;
;  Read in the DENSITY data.
;
        cdf_varget, plastic_cdfid, density_epoch_name, density_epoch, $
          rec_count = num_density_pts
        cdf_varget, plastic_cdfid, density_varname, density_data, $
          rec_count = num_density_pts
        density_epoch = reform(density_epoch)
        density_data = reform(density_data)
;
;  Get epoch variable name for solar wind velocity variable.
;
        cdf_attget, plastic_cdfid, 'DEPEND_0', velocity_varname, $
          velocity_epoch_name
;
;  Get the number of VELOCITY records.
;
        cdf_control, plastic_cdfid, variable = velocity_epoch_name, $
          get_var_info = velocity_epoch_info
        num_velocity_pts = velocity_epoch_info.maxrec + 1
;
;  Read in the VELOCITY data.
;
        cdf_varget, plastic_cdfid, velocity_epoch_name, velocity_epoch, $
          rec_count = num_velocity_pts
        cdf_varget, plastic_cdfid, velocity_varname, velocity_data, $
          rec_count = num_velocity_pts
        velocity_epoch = reform(velocity_epoch)
;
;  Concatenate the data together.
;
        if n_elements(density_epoch_b) eq 0 then begin
            density_epoch_b = density_epoch
            density_data_b = density_data
            velocity_epoch_b = velocity_epoch
            velocity_data_b = velocity_data
        end else begin
            density_epoch_b = [density_epoch_b, density_epoch]
            density_data_b = [density_data_b, density_data]
            velocity_epoch_b = [velocity_epoch_b, velocity_epoch]
            velocity_data_b = [[velocity_data_b], [velocity_data]]
        endelse
;
        cdf_close, plastic_cdfid
    endif
endfor
;
;  Convert density and velocity epoch data to UTC, and append to EPOCH.
;
if n_elements(density_epoch_b) gt 0 then begin
    temp = [density_epoch_b, velocity_epoch_b]
    if n_elements(epoch) eq 0 then epoch = temp else epoch = [epoch, temp]
    density_epoch_b_utc = cdf2utc(density_epoch_b)
    velocity_epoch_b_utc = cdf2utc(velocity_epoch_b)
endif
;
;  If no files were read, then simply return.
;
if n_elements(epoch) eq 0 then return
;
;  Derive a time range to apply to all the plots.
;
if n_elements(timerange) ne 2 then $
  timerange = cdf2utc([min(epoch,max=emax), emax])
;
;  Set up for plotting.
;
erase
linecolor,1,'red'
linecolor,2,'blue'
xtickname = replicate(' ',30)
;
; Set the plot title.
;
if n_elements(k_title) eq 1 then title=k_title else begin
    lasttime = cdf2tai(max(epoch)) < anytim2tai(timerange[1])
    lasttime = tai2utc(lasttime,/vms,/truncate)
    lasttime = strmid(lasttime,0,strlen(lasttime)-3) ;Remove seconds
    title = 'IMPACT/PLASTIC  ' + lasttime
endelse
;
;  Define a plot range for magnetic field data.
;
ymin = 0
ymax = 1
if n_elements(mag_data_a) gt 0 then begin
    amax=max(good_pixels(mag_data_a), min=amin)
    ymin = ymin < amin
    ymax = ymax > amax
endif
if n_elements(mag_data_b) gt 0 then begin
    bmax=max(good_pixels(mag_data_b), min=bmin)
    ymin = ymin < bmin
    ymax = ymax > bmax
endif
;
;  Plot the magnetic field vector components.
;
n_panels = 5
setview,1.075,1.075,1,n_panels,0,0
ssc_beacon_panel, timerange, [ymin, ymax], ytitle = 'B(x)', $
  xtickname=xtickname, xtitle=' ', tick_unit=tick_unit, title=title
if n_elements(mag_data_a) gt 0 then outplot, mag_epoch_a_utc, $
  mag_data_a[0,*], color=1, psym=3
if n_elements(mag_data_b) gt 0 then outplot, mag_epoch_b_utc, $
  mag_data_b[0,*], color=2, psym=3
;
setview,1.075,1.075,2,n_panels,0,0
ssc_beacon_panel, timerange, [ymin,ymax], ytitle = 'B(y)', $
  xtickname=xtickname, xtitle=' ', tick_unit=tick_unit
ywindow = !y.window * !d.y_size
xyouts, 2*ysize, average(ywindow), /device, '(nanoTesla)', alignment=0.5, $
  orientation=90, charsize=charsize
if n_elements(mag_data_a) gt 0 then outplot, mag_epoch_a_utc, $
  mag_data_a[1,*], color=1, psym=3
if n_elements(mag_data_b) gt 0 then outplot, mag_epoch_b_utc, $
  mag_data_b[1,*], color=2, psym=3
;
setview,1.075,1.075,3,n_panels,0,0
ssc_beacon_panel, timerange, [ymin,ymax], ytitle = 'B(z)', $
  xtickname=xtickname, xtitle=' ', tick_unit=tick_unit
if n_elements(mag_data_a) gt 0 then outplot, mag_epoch_a_utc, $
  mag_data_a[2,*], color=1, psym=3
if n_elements(mag_data_b) gt 0 then outplot, mag_epoch_b_utc, $
  mag_data_b[2,*], color=2, psym=3
;
;  Define a plot range for density data.
;
ymin = 0.1
ymax = 10
if n_elements(density_data_a) gt 0 then begin
    amax=max(good_pixels(density_data_a), min=amin)
    ymin = ymin < amin
    ymax = ymax > amax
endif
if n_elements(density_data_b) gt 0 then begin
    bmax=max(good_pixels(density_data_b), min=bmin)
    ymin = ymin < bmin
    ymax = ymax > bmax
endif
ymin = ymin > 0.1
ymax = ymax < 100
if ymin ge ymax then begin
    ymin = 0.1
    ymax = 100
endif
;
;  Plot the  density data.
;
setview,1.075,1.075,4,n_panels,0,0
ssc_beacon_panel, timerange, [ymin,ymax], ytitle = 'N!LP!N (cm!U-3!N)', $
  xtickname=xtickname, xtitle=' ', tick_unit=tick_unit, /ylog
if n_elements(density_data_a) gt 0 then outplot, density_epoch_a_utc, $
  density_data_a, color=1, psym=3
if n_elements(density_data_b) gt 0 then outplot, density_epoch_b_utc, $
  density_data_b, color=2, psym=3
;
;  Define a plot range for velocity data.
;
ymin = 0
ymax = 1
if n_elements(velocity_data_a) gt 0 then begin
    amax=max(good_pixels(velocity_data_a[3,*]), min=amin)
    ymin = ymin < amin
    ymax = ymax > amax
endif
if n_elements(velocity_data_b) gt 0 then begin
    bmax=max(good_pixels(velocity_data_b[3,*]), min=bmin)
    ymin = ymin < bmin
    ymax = ymax > bmax
endif
ymin = ymin > 0
ymax = ymax < 2000
if ymin ge ymax then begin
    ymin = 0
    ymax = 2000
endif
;
;  Plot the velocity components.
;
setview,1.075,1.075,n_panels,n_panels,0,0
xtitle = 'Start: ' + anytim2utc(timerange[0],/vms,/date) + ' '
xtitle = xtitle + strmid(anytim2utc(timerange[0],/ccsds,/time),0,5) + ' UTC'
ssc_beacon_panel, timerange, [ymin,ymax], ytitle = 'V (km/s)', $
  tick_unit=tick_unit, xtitle=xtitle
if n_elements(velocity_data_a) gt 0 then outplot, velocity_epoch_a_utc, $
  velocity_data_a[3,*], color=1, psym=3
if n_elements(velocity_data_b) gt 0 then outplot, velocity_epoch_b_utc, $
  velocity_data_b[3,*], color=2, psym=3
;
;------------------------------------------------------------------------------
;  Alternate code to display velocity in X,Y,Z components
;
;;;
;;;  Define a plot range for velocity data.
;;;
;;ymin = 0
;;ymax = 1
;;if n_elements(velocity_data_a) gt 0 then begin
;;    amax=max(good_pixels(velocity_data_a), min=amin)
;;    ymin = ymin < amin
;;    ymax = ymax > amax
;;endif
;;if n_elements(velocity_data_b) gt 0 then begin
;;    bmax=max(good_pixels(velocity_data_b), min=bmin)
;;    ymin = ymin < bmin
;;    ymax = ymax > bmax
;;endif
;;ymin = ymin > (-2000)
;;ymax = ymax < 2000
;;if ymin ge ymax then begin
;;    ymin = -2000
;;    ymax =  2000
;;endif
;;;
;;;  Plot the velocity components.
;;;
;;setview,1.075,1.075,5,n_panels,0,0
;;ssc_beacon_panel, timerange, [ymin,ymax], ytitle = 'V(x)', $
;;  xtickname=xtickname, xtitle=' ', tick_unit=tick_unit
;;if n_elements(velocity_data_a) gt 0 then outplot, velocity_epoch_a_utc, $
;;  velocity_data_a[0,*], color=1, psym=3
;;if n_elements(velocity_data_b) gt 0 then outplot, velocity_epoch_b_utc, $
;;  velocity_data_b[0,*], color=2, psym=3
;;
;;setview,1.075,1.075,6,n_panels,0,0
;;ssc_beacon_panel, timerange, [ymin,ymax], ytitle = 'V(y)', $
;;  xtickname=xtickname, xtitle=' ', tick_unit=tick_unit
;;ywindow = !y.window * !d.y_size
;;xyouts, 2*ysize, average(ywindow), /device, '(kilometer/sec)', alignment=0.5, $
;;  orientation=90, charsize=charsize
;;if n_elements(velocity_data_a) gt 0 then outplot, velocity_epoch_a_utc, $
;;  velocity_data_a[1,*], color=1, psym=3
;;if n_elements(velocity_data_b) gt 0 then outplot, velocity_epoch_b_utc, $
;;  velocity_data_b[1,*], color=2, psym=3
;;
;;setview,1.075,1.075,7,n_panels,0,0
;;xtitle = 'Start: ' + anytim2utc(timerange[0],/vms,/date) + ' '
;;xtitle = xtitle + strmid(anytim2utc(timerange[0],/ccsds,/time),0,5) + ' UTC'
;;ssc_beacon_panel, timerange, [ymin,ymax], ytitle = 'V(z)', $
;;  tick_unit=tick_unit, xtitle=xtitle
;;if n_elements(velocity_data_a) gt 0 then outplot, velocity_epoch_a_utc, $
;;  velocity_data_a[2,*], color=1, psym=3
;;if n_elements(velocity_data_b) gt 0 then outplot, velocity_epoch_b_utc, $
;;  velocity_data_b[2,*], color=2, psym=3
;------------------------------------------------------------------------------
;
;  Put on labels for Ahead and Behind.
;
xwindow = !x.window * !d.x_size
xyouts, xwindow[1], ysize, /device, 'RED: Ahead', charsize=charsize, $
  color=1, alignment=1
xyouts, xwindow[0], ysize, /device, 'BLUE: Behind', charsize=charsize, $
  color=2, alignment=0
;
;  Empty the plot buffer, and reset the plot viewport.
;
empty
setview
;
return
end
