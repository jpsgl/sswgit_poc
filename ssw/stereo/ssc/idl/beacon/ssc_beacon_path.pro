;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_BEACON_PATH
;
; Purpose     :	Form the path to a STEREO beacon file.
;
; Category    :	STEREO, Telemetry
;
; Explanation :	Forms a file path out of the environment variable
;               STEREO_BEACON_DATA, the spacecraft identifier, the instrument
;               name, the year, and the month.
;
; Syntax      :	Filepath = SSC_BEACON_PATH(spacecraft, instrument, date)
;
; Examples    :	See SSC_BEACON_READER.
;
; Inputs      :	SPACECRAFT = The spacecraft identifier, "ahead" or "behind".
;               INSTRUMENT = Either "impact", "plastic", "secchi", or "swaves".
;               DATE       = The date, in any format recognized by ANYTIM2UTC.
;
; Opt. Inputs :	None.
;
; Outputs     :	The result of the function is the path to the file.  If
;               STEREO_BEACON_DATA is not defined, or the instrument is not
;               recognized, then it returns the null string.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Calls       :	CONCAT_DIR, FILE_EXIST, MK_DIR, ANYTIM2UTC
;
; Common      :	None.
;
; Env. Vars.  : STEREO_BEACON_DATA points to the top of the beacon data tree.
;               Under this are the directories "ahead" and "behind", each of
;               which contain subdirectories for each of the instruments, which
;               are then further subdivided by year and month,
;               e.g. "ahead/impact/2006/02".
;
; Restrictions:	Probably not currently correct for SECCHI.
;
; Side effects:	If the directories do not exist, then they are created.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 10-Feb-2006
;
; Contact     :	WTHOMPSON
;-
;
function ssc_beacon_path, spacecraft, instrument, date
;
filepath = getenv('STEREO_BEACON_DATA')
if filepath eq '' then return, ''
;
;  Interpret the spacecraft string.
;
sc = strtrim(strlowcase(spacecraft),2)
n = strlen(sc)
if (sc eq strmid('ahead',0,n))  or (sc eq strmid('stereo ahead',0,n)) or $
  (sc eq strmid('stereo-ahead',0,n))  or (sc eq 'sta') then $
  sc = 'ahead'
if (sc eq strmid('behind',0,n)) or (sc eq strmid('stereo behind',0,n)) or $
  (sc eq strmid('stereo-behind',0,n)) or (sc eq 'stb') then $
  sc = 'behind'
filepath = concat_dir(filepath, sc)
if not file_exist(filepath) then mk_dir, filepath
;
;  Interpret the instrument identifier.
;
case strlowcase(strmid(instrument,0,1)) of
    'i': ins = 'impact'
    'p': ins = 'plastic'
    else: begin
        case strlowcase(strmid(instrument,0,2)) of
            'se': ins = 'secchi'
            'sw': ins = 'swaves'
            else: return, ''
        endcase
    endcase
endcase
filepath = concat_dir(filepath, ins)
if not file_exist(filepath) then mk_dir, filepath
;
;  Append the year and month.
;
utc = anytim2utc(date,/external)
filepath = concat_dir(filepath, string(utc.year, format='(I4.4)'))
if not file_exist(filepath) then mk_dir, filepath
filepath = concat_dir(filepath, string(utc.month,format='(I2.2)'))
if not file_exist(filepath) then mk_dir, filepath
;
return, filepath
end
