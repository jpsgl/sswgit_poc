;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_BEACON_SUPPLEMENT
;
; Purpose     :	Preload supplement files for latest images page
;
; Category    :	SECCHI, Quicklook
;
; Explanation :	This routine copies over the most recent SOHO, SDO, and Mark-IV
;               images from the STEREO browse pages to the beacon directory for
;               context.  By having them preloaded, the beacon pages can be
;               generated in realtime.
;
;               Any existing images in the beacon directory are examined for
;               date.  If they're more than 24 hours old, they're deleted.
;
; Syntax      :	SSC_BEACON_SUPPLEMENT
;
; Examples    :	SSC_BEACON_SUPPLEMENT
;
; Inputs      :	None.
;
; Opt. Inputs :	None.
;
; Outputs     :	Files are copied into the beacon directory.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Env. Vars.  :	STEREO_BROWSE      = Top directory of browse pages
;
;               STEREO_BEACON_HTML = Location of beacon web pages
;
; Calls       :	ANYTIM2UTC, CONCAT_DIR, FILE_EXIST
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 10-Apr-2009, William Thompson, GSFC
;               Version 2, 23-Jun-2010, WTT, added SDO images
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_beacon_supplement
;
stereo_browse = getenv('STEREO_BROWSE')
stereo_beacon = getenv('STEREO_BEACON_HTML')
;
soho = 'soho_' + ['eit_171', 'eit_195', 'eit_284', 'eit_304', 'c2', 'c3']
exten = ['.jpg', '.gif']
mk4 = ['mk4.jpg', 'latest.mk4.gif']
sdo = 't0' + ['193','171','211','304'] + '.jpg'
;
;  Step through yesterday and today.
;
get_utc, utc
utc.mjd = utc.mjd - 1
for idate = 0,1 do begin
    ex = anytim2utc(utc, /external)
    path = string(ex.year, format='(I4.4)')  + '/' + $
      string(ex.month, format='(I2.2)') + '/' + $
      string(ex.day, format='(I2.2)') + '/'
    path = concat_dir(stereo_browse, path)
;
    for isoho = 0,n_elements(soho)-1 do begin
        for iexten=0,n_elements(exten)-1 do begin
            name = soho[isoho] + exten[iexten]
            browsefile = concat_dir(path, name)
            beaconfile = concat_dir(stereo_beacon, name)
            if file_exist(browsefile) then begin
                file_copy, browsefile, beaconfile, /overwrite, /force
            end else if file_exist(beaconfile) then begin
                info = file_info(beaconfile)
                delta = systime(1) - info.mtime
                if delta gt 86400. then file_delete, beaconfile
            endif
        endfor
    endfor
;
    for imk4 = 0,n_elements(mk4)-1 do begin
        browsefile = concat_dir(path, mk4[imk4])
        beaconfile = concat_dir(stereo_beacon, mk4[imk4])
        if file_exist(browsefile) then begin
            file_copy, browsefile, beaconfile, /overwrite, /force
        end else if file_exist(beaconfile) then begin
            info = file_info(beaconfile)
            delta = systime(1) - info.mtime
            if delta gt 86400. then file_delete, beaconfile
        endif
    endfor
;
    for isdo = 0,n_elements(sdo)-1 do begin
        browsefile = concat_dir(path, sdo[isdo])
        beaconfile = concat_dir(stereo_beacon, sdo[isdo])
        if file_exist(browsefile) then begin
            file_copy, browsefile, beaconfile, /overwrite, /force
        end else if file_exist(beaconfile) then begin
            info = file_info(beaconfile)
            delta = systime(1) - info.mtime
            if delta gt 86400. then file_delete, beaconfile
        endif
    endfor
;
    utc.mjd = utc.mjd + 1
endfor
;
end
