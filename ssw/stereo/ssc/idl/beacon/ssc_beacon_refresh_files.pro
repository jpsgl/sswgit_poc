;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_BEACON_REFRESH_FILES
;
; Purpose     :	Refresh the STEREO beacon telemetry files
;
; Category    :	STEREO, Telemetry
;
; Explanation : This procedure is called by SSC_BEACON_READER to refresh the
;               STEREO beacon telemetry files for the IMPACT, PLASTIC, and
;               SWAVES instruments.  The procedure SSC_BEACON_OPEN_NEXT is
;               called for any file which has reached its end-of-file, to see
;               if a newer file is available.  Files are refreshed for both the
;               Ahead and Behind spacecraft.
;
; Syntax      :	SSC_BEACON_REFRESH_FILES, IMPACTLUN, PLASTICLUN, SWAVESLUN
;
; Examples    :	See SSC_BEACON_READER.
;
; Inputs      :	None.
;
; Opt. Inputs :	None.
;
; Outputs     :	IMPACTLUN, PLASTICLUN, SWAVESLUN
;                       Each is a two-element array containing the logical unit
;                       numbers of the Ahead and Behind files for each
;                       instrument.
;
; Opt. Outputs:	None.
;
; Keywords    :	NOWAIT  = Normally, if all the files are still at the
;                         end-of-file mark at the end of this procedure, a
;                         small wait is put in to control CPU usage.  If
;                         /NOWAIT is passed, then this wait is bypassed.
;
;               DONE    = Returns 1 (true) if all the files are at their
;                         end-of-file mark.
;
; Calls       :	SSC_BEACON_OPEN_NEXT
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	Currently, only realtime files ingested from the MOC are
;               handled.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 08-Feb-2006
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_beacon_refresh_files, impactlun, plasticlun, swaveslun, done=done, $
                              nowait=nowait
on_error, 2
;
n_done = 0
for isc=0,1 do begin
    if eof(impactlun[isc])  then ssc_beacon_open_next, impactlun,  isc
    if eof(impactlun[isc])  then n_done = n_done + 1
;
    if eof(plasticlun[isc]) then ssc_beacon_open_next, plasticlun, isc
    if eof(plasticlun[isc]) then n_done = n_done + 1
;
    if eof(swaveslun[isc])  then ssc_beacon_open_next, swaveslun,  isc
    if eof(swaveslun[isc])  then n_done = n_done + 1
endfor
;
done = (n_done eq 6)
if done and not keyword_set(nowait) then wait, 10
return
end
