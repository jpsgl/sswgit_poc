;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_MAKE_EUVI_SOS
;
; Purpose     :	Create heliographic maps for Science-on-a-Sphere
;
; Category    :	STEREO, SECCHI, Coordinates
;
; Explanation :	This routine calls SSC_EUVI_SYNOPTIC to create a series of
;               heliographic maps in JPEG format which can be used to make
;               movies by Science-on-a-Sphere.  The maps are made in both 195
;               and 304 at a 10 minute cadence.
;
; Syntax      :	SSC_MAKE_EUVI_SOS, DATE
;
; Examples    :	SSC_MAKE_EUVI_SOS, '2011-01-20'
;
; Inputs      :	DATE = The date to process
;
; Opt. Inputs :	None.
;
; Outputs     :	Creates files under $STEREO_BROWSE_SPHERE for the specified
;               date and wavelengths.
;
; Opt. Outputs:	None.
;
; Keywords    :	SINGLE_SC= If set, then allow map to be built up from a single
;                         STEREO spacecraft.  This option is intended for the
;                         period when one or both spacecraft is behind the Sun.
;
;               REPLACE = Replace existing JPEG files.
;
;               AHEAD_ONLY= If set, then don't look for Behind images.
;
;               BEHIND_ONLY= If set, then don't look for Ahead images.
;
;               Also allows VSO keywords to be passed, such as SITE='SAO'
;
; Env. Vars.  : STEREO_BROWSE_SPHERE = Base directory for JPEG files.
;
; Calls       :	ANYTIM2UTC, SCC_READ_SUMMARY, BREAK_FILE, MATCH, SETPLOT,
;               CONCAT_DIR, FILE_EXIST, MK_DIR, SSC_EUVI_SYNOPTIC, TVREAD,
;               WRITE_JPEG
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 11-Jan-2011, William Thompson, GSFC
;               Version 2, 03-Feb-2011, WTT, call SSC_EUVI_SYNOPTIC with
;                       /NOSEARCH option if ITIME > 0
;               Version 3, 01-Jul-2014, WTT, added keyword SINGLE_SC
;               Version 4, 04-Aug-2014, WTT, added check for prog='Dark'
;               Version 5, 28-Aug-2014, WTT, added keywords AHEAD_ONLY,
;                                            BEHIND_ONLY
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_make_euvi_sos, date, single_sc=single_sc, replace=replace, $
                       ahead_only=ahead_only, behind_only=behind_only, $
                       _extra=_extra
;
;  Look for simultaneous image pairs from both spacecraft.  Filter out any
;  images smaller than 2048x2048, or with missing blocks.
;
utc = anytim2utc(date)
if keyword_set(behind_only) then ncata = 0 else begin
    errmsg = ''
    cata = scc_read_summary(date=utc, spacecraft='a', telescope='euvi', $
                            /nobeacon, errmsg=errmsg)
    if errmsg ne '' then begin
        ncata = 0
        if not keyword_set(single_sc) then begin
            print, errmsg
            return
        endif
    end else begin
        w = where((cata.xsize ge 2048) and (cata.nmiss eq 0) and $
                  (cata.prog ne 'Dark'), ncata)
        if ncata gt 0 then cata = cata[w] else begin
            print, 'No images of size 2048x2048, and without missing blocks'
            if not keyword_set(single_sc) then return
        endelse
    endelse
endelse
;
if keyword_set(ahead_only) then ncatb = 0 else begin
    errmsg = ''
    catb = scc_read_summary(date=utc, spacecraft='b', telescope='euvi', $
                            /nobeacon, errmsg=errmsg)
    if errmsg ne '' then begin
        ncatb = 0
        if (not keyword_set(single_sc)) or (ncatb eq 0) then begin
            print, errmsg
            return
        endif
    end else begin
        w = where((catb.xsize ge 2048) and (catb.nmiss eq 0) and $
                  (catb.prog ne 'Dark'), ncatb)
        if ncatb gt 0 then catb = catb[w] else begin
            print, 'No images of size 2048x2048, and without missing blocks'
            if not keyword_set(single_sc) then return
        endelse
    endelse
endelse
;
;  Look for matching filenames.
;
if (ncata gt 0) and (ncatb gt 0) then begin
    break_file, cata.filename, diska, dira, namea
    break_file, catb.filename, diskb, dirb, nameb
    namea = strmid(namea,0,15)
    nameb = strmid(nameb,0,15)
    match, namea, nameb, wa, wb, count=count
    if count eq 0 then begin
        print, 'No matching filenames'
        return
    endif
    cata = cata[wa]
    catb = catb[wb]
end else if ncata eq 0 then cata = catb else catb = cata
;
;  Make sure the wavelengths are identical.
;
w = where(cata.value eq catb.value, count)
if count eq 0 then begin
    print, 'No matching wavelengths'
    return
endif
cata = cata[w]
catb = catb[w]
;
;  Use the Z buffer.
;
dname = !d.name
setplot, 'z'
!p.color = !d.table_size - 1
!p.background = 0
device, set_resolution=[3000,1500]
;
;  Form the name of the output directory for the given date.  If it doesn't yet
;  exist, then create it.
;
browse_path = getenv('STEREO_BROWSE_SPHERE')
browse_path = concat_dir(browse_path, utc2str(utc, /date_only, /ecs))
if ~file_exist(browse_path) then mk_dir, browse_path
;
;  Step through the wavelengths.
;
waves = [195, 304]
for iwave=0,n_elements(waves)-1 do begin
    wave = waves[iwave]
    jpeg_path = concat_dir(browse_path, ntrim(wave))
    if ~file_exist(jpeg_path) then mk_dir, jpeg_path
    w = where(cata.value eq wave, count)
    if count gt 0 then begin
        lista = cata[w]
        listb = catb[w]
;
;  Step through the times in 10 minute intervals, starting from 00:05 UT.
;
        tai = utc2tai(lista.date_obs)
        utc.time = 300D3
        tai0 = utc2tai(utc)
        lastfile = ''
        for itime = 0,143 do begin
            delta = abs(tai - tai0)
            w = where(delta eq min(delta))
            filename = [lista[w[0]].filename, listb[w[0]].filename]
;
;  If different from the last file, then process it.
;
            if filename[0] ne lastfile then begin
                break_file, filename[0], disk, dir, name
                jpeg_name = strmid(name,0,16) + ntrim(wave) + '.jpg'
                jpeg_name = concat_dir(jpeg_path, jpeg_name)
;
;  If the JPEG file already exists, and the /REPLACE keyword hasn't been
;  passed, then skip to the next file.
;
                if ~file_exist(jpeg_name) or keyword_set(replace) then begin
                    errmsg = ''
                    ssc_euvi_synoptic, /color, npixels=[3000,1500], $
                      filename=filename, wavelength=wave, errmsg=errmsg, $
                      /carrington, nosearch=(itime ne 0), $
                      single_sc=single_sc, _extra=_extra
;
;  Write out the JPEG file.
;
                    if errmsg eq '' then begin
                        image = tvread(red, green, blue)
                        true_image = bytarr(3,3000,1500)
                        true_image[0,*,*] = red[image]
                        true_image[1,*,*] = green[image]
                        true_image[2,*,*] = blue[image]
                        write_jpeg, jpeg_name, true_image, true=1, quality=90
                    endif else print, errmsg
                endif
            endif
;
;  Step by 10 minutes for the next file.
;
            lastfile = filename[0]
            tai0 = tai0 + 600D0
        endfor
    endif
endfor
;
setplot, dname
;
end
