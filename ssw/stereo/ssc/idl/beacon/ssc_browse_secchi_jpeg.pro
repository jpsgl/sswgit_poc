;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_BROWSE_SECCHI_JPEG
;
; Purpose     :	Process SECCHI beacon images for the web
;
; Category    :	SECCHI, Quicklook
;
; Explanation :	Called from SSC_BEACON_SECCHI to generate JPEG images at
;               various resolutions.
;
; Syntax      :	SSC_BROWSE_SECCHI_JPEG, FILE, NAME, SC, STEREO_BROWSE, SDATE
;
; Examples    :	See SSC_BEACON_SECCHI
;
; Inputs      :	FILE    = FITS file to process
;               NAME    = Base filename without directory or extension
;               SC      = Either "ahead" or "behind"
;               STEREO_BROWSE = Top browse directory
;
; Opt. Inputs :	None.
;
; Outputs     :	None required.
;
; Opt. Outputs:	SDATE   = Returns date if file was processed.
;
; Keywords    :	BEACON  = Set to true if one is processing beacon data.
;
; Env. Vars.  :	None.
;
; Calls       :	CONCAT_DIR, BREAK_FILE, FXHREAD, FXPAR, STR2UTC, SCCREADFITS,
;               SCC_IMG_TRIM, DESPIKE_GEN, BASELINE, SB_BYTSCL, EIT_COLORS,
;               UTC2STR, SSC_SECCHI_CAPTION, SSC_SECCHI_DIR_HTML, IS_FITS
;
; Common      :	None.
;
; Restrictions:	Currently, this procedure only handles EUVI images.
;
; Side effects:	The graphics device is set to the Z buffer
;
; Prev. Hist. :	Partially based on EUVI_PRETTY by Jean-Pierre Wuelser
;
; History     :	Version 1, 21-Mar-2007, WTT, split off from SSC_BEACON_SECCHI
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_browse_secchi_jpeg, file, name, sc, stereo_browse, sdate, $
                            beacon=beacon
;
;  Set up some arrays.
;
sdate = ''
res = [2048,1024,512,256,128]
charsize = [8,4,2,1,0.5]
charthick = [4,2,1,1,1]
;
;  Get the modification date of the file.  Also, make sure that the file has
;  data in it, and extract some minimal information from the header.
;
if not is_fits(file) then return
openr, unit, file, /get_lun
mtime0 = (fstat(unit)).mtime
fxhread, unit, textheader
date_obs = fxpar(textheader, 'date-obs')
detector = strlowcase(strtrim(fxpar(textheader, 'detector'),2))
wavelnth = fxpar(textheader, 'wavelnth')
if wavelnth eq 175 then wavelnth = 171
free_lun, unit
file_read = 0
date_obs = str2utc(date_obs, /external)
;
;  Step through the resolutions and form the name of the output file.
;
for ires = 0,n_elements(res)-1 do begin
    jpeg_path = concat_dir(stereo_browse, $
                           string(date_obs.year, format='(I4.4)')  + '/' + $
                           string(date_obs.month, format='(I2.2)') + '/' + $
                           string(date_obs.day, format='(I2.2)')   + '/' + $
                           sc + '/' + detector)
    if detector eq 'euvi' then jpeg_path = $
      concat_dir(jpeg_path, string(wavelnth, format='(I3.3)'))
    jpeg_path = concat_dir(jpeg_path, ntrim(res[ires]))
    jpeg_file = concat_dir(jpeg_path, name)
    if detector eq 'euvi' then jpeg_file = jpeg_file + '_' + $
      string(wavelnth, format='(I3.3)')
    jpeg_file = jpeg_file + '.jpg'
;
;  Get the modification time of the file.  If the file doesn't exist, check to
;  see if there's already a full-resolution version of the same file.
;  Otherwise, fake a modification time earlier than that of the FITS file.
;
    if file_exist(jpeg_file) then begin
        openr, unit, jpeg_file, /get_lun
        mtime1 = (fstat(unit)).mtime
        free_lun, unit
    end else begin
        break_file, jpeg_file, disk0, dir0, name0, ext0
        strput, name0, '??', 16
        temp = file_search(disk0+dir0+name0+ext0, count=count)
        if count gt 0 then mtime1=2*mtime0 else mtime1 = 0
    endelse
;
;  If the JPEG modification time is earlier than the FITS modification time,
;  then create the JPEG file.
;
    if mtime0 gt mtime1 then begin
;
;  If not already done, read in the FITS file, and form the image.  Restrict to
;  the active area of the CCD.
;
        if not file_read then begin
            image = sccreadfits(file, header,/silent)
            image = scc_img_trim(image, header)
            file_read = 1
;
;  Process the image based on the detector type.
;
            case detector of
                'euvi': begin
;
;  Remove any cosmic rays.  Running through the despiking algorithm twice
;  removes larger cosmic ray hits.
;
                    image = despike_gen(image, tn=8)
                    image = despike_gen(image, tn=8)
;
;  Determine the baseline from the corners.
;
                    base = fltarr(64,64,4)
                    sz = size(image)
                    base[*,*,0] = image[0:63, 0:63]
                    base[*,*,1] = image[0:63, sz[2]-64:*]
                    base[*,*,2] = image[sz[1]-64:*, 0:63]
                    base[*,*,3] = image[sz[1]-64:*, sz[2]-64:*]
                    amin = baseline(base, missing=0)
                    amax = float(max(image))
;
;  Rotate the image.
;
                    temp = fltarr(sz[1]+2,sz[2]+2)
                    temp[1,1] = image
                    image = rot(temp, -header.crota, 1, $
                                header.crpix1, header.crpix2)
                    image = image[1:sz[1], 1:sz[2]]
;
;  Scale the image, and get the color tables.
;
                    col = {min: amin, max: amax, gam: 0.25}
                    image = sb_bytscl(image, col)
                    eit_colors, wavelnth, rr, gg, bb
                endcase
                else: return ;Skip unhandled detectors
            endcase
        endif                   ;Code to read and process image
;
;  If necessary, create the JPEG directory.
;
        if not file_exist(jpeg_path) then mk_dir, jpeg_path
;
;  Return the processed date.
;
        sdate = utc2str(date_obs, /date_only)
;
;  Display the image in the Z-buffer, and put on the labels.
;
        temp = rebin(image, res[ires], res[ires])
        if !d.name ne 'Z' then set_plot,'Z'
        device, set_resolution=[res[ires],res[ires]]
        tvlct, rr, gg, bb
        tv, temp
        if header.obsrvtry eq 'STEREO_A' then $
          label = 'Ahead' else label = 'Behind'
        label = 'STEREO ' + label + ' ' + header.detector
        if detector eq 'euvi' then label = label + ' ' + $
          string(wavelnth, format='(I3.3)')
        ysize = !d.y_ch_size * charsize[ires]
        xyouts, res[ires]/2, res[ires] - 1.5*ysize, label, $
          charsize=charsize[ires], color=!d.table_size-1, $
          charthick=charthick[ires], alignment=0.5, /device
        xyouts, res[ires]/2, 0.5*ysize, charsize=charsize[ires], $
          sdate + ' ' + utc2str(date_obs, /time_only, /truncate), $
          color=!d.table_size-1, charthick=charthick[ires], $
          alignment=0.5, /device
        temp = tvrd()
;
;  Convert the image to true-color, and write the JPEG file.
;
        true_image = bytarr(3, res[ires], res[ires])
        true_image[0,*,*] = rr[temp]
        true_image[1,*,*] = gg[temp]
        true_image[2,*,*] = bb[temp]
        write_jpeg, jpeg_file, true_image, true=1, quality=90
;
;  Create a caption for the JPEG file.
;
        ssc_secchi_caption, jpeg_file, header, beacon=beacon
;
;  If not a space weather image, then delete any corresponding space weather
;  images.
;
        break_file, jpeg_file, disk0, dir0, name0, ext0
        if strmid(name0, 17, 1) ne '7' then begin
            strput, name0, '?7', 16
            temp = file_search(disk0+dir0+name0+'.*', count=count)
            if count gt 0 then file_delete, temp
        endif
;
;  Recreate the web pages for the directory.
;
        ssc_secchi_dir_html, jpeg_path
;
    endif                       ;Modification time earlier than FITS file
endfor                          ;ires
;
end
