;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_OVERPLOT_PLANET
;
; Purpose     :	Find planets in SECCHI JPEG image from SSC website
;
; Category    :	SECCHI, Quicklook
;
; Explanation :	This routine adds labels to SECCHI coronagraph JPEG images from
;               the STEREO Science Center website to show where the planets
;               are.  To use this routine, the JPEG must first be downloaded
;               from the SSC website.
;
;               The routine first looks for the corresponding FITS file, from
;               which it extracts the pointing information.  Usually, this
;               means that the JPEG file should be generated from the Level-0.5
;               FITS files rather than from the beacon data.
;
; Syntax      :	SSC_MAKE_PLANET_FINDER, JPEG_FILENAME  [, IMAGE]  [, /DISPLAY]
;
; Examples    :	SSC_MAKE_PLANET_FINDER, '20170603_000518_s4c1A.jpg', /DISPLAY
;
; Inputs      :	JPEG_FILENAME = Name of the JPEG file to be modified.
;
; Opt. Inputs :	None.
;
; Outputs     :	IMAGE   = The modified 24-bit image with the planets
;               identified.  If /DISPLAY is set, then IMAGE can be omitted.
;
; Opt. Outputs:	None.
;
; Keywords    :	FITS_FILE = The name of the corresponding FITS file.  If not
;                           passed, then the program searches the SECCHI
;                           archive.
;
;               BEACON  = Look in the beacon archive instead of the Level-0.5
;                         archive.  Only useful on one of the SSC machines.
;
;               RED     = Label in red.
;
;               GREEN   = Label in green.
;
;               BLUE    = Label in blue.
;
;               COLORIZE = If set, then allow the program to pick a color (red,
;                          green, blue) for labelling the planet.  The default
;                          is to use white.
;
;               OUTLINE = If set, then outline the circles and characters in
;                         black to make them stand out better.  This was found
;                         to be more effective than using color when producing
;                         JPEG images.
;
; Env. Vars.  : None.
;
; Calls       :	BREAK_FILE, SCCFINDFITS, SCCREADFITS, GET_STEREO_HPC_POINT,
;               FITSHEAD2WCS, CIRCLE_SYM, GET_STEREO_LONLAT, WCS_GET_PIXEL
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	By default, the program generates a new window each time
;               it's called.  This can be overridden with the WINDOW
;               keyword to WDISPLAY.
;
; Prev. Hist. :	Based on SSC_MAKE_PLANET_FINDER
;
; History     :	Version 1, 12-Jun-2017, William Thompson, GSFC
;               Version 2, 21-Jun-2017, WTT, add keywords RED, GREEN, BLUE
;                                       fix HI roll bug
;               Version 3, 26-Jun-2017, WTT, return if FITS file not found
;               Version 4, 30-Jun-2017, WTT, pass ERRMSG to SCCFINDFITS
;               Version 5, 31-May-2018, WTT, added keyword OUTLINE
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_overplot_planet, jpeg_file, image, fits_file=fits_file, beacon=beacon,$
                         red=red, green=green, blue=blue, colorize=k_color, $
                         display=display, outline=outline, _extra=_extra
;
read_jpeg, jpeg_file, image
sz = size(image)
true_color = (where(sz[1:sz[0]] eq 3))[0] + 1
jpeg_size = max(sz[1:sz[0]])
;
;  Read in the corresponding FITS file.
;
break_file, jpeg_file, disk, dir, name
errmsg = ''
fits_file = sccfindfits(name+'.fts', beacon=beacon, errmsg=errmsg)
if errmsg ne '' then print, errmsg
if fits_file eq '' then return
dummy = sccreadfits(fits_file, header, outsize=jpeg_size, /silent)
point = get_stereo_hpc_point(header.date_obs, header.obsrvtry, $
                             /post_conjunction)
roll = header.crota
wcs = fitshead2wcs(header)
;
;  Determine the best color to use for the labels.
;
if keyword_set(red) then begin
    color = '0000ff'x
end else if keyword_set(green) then begin
    color = '00ff00'x
end else if keyword_set(blue) then begin
    color = 'ff0000'x
end else if ~keyword_set(k_color) then color='ffffff'x else begin
    case true_color of
        1: test = average(image,[2,3])
        2: test = average(image,[1,3])
        3: test = average(image,[1,2])
    endcase
    w = where(test eq min(test))
    color = ['0000ff'x, '00ff00'x, 'ff0000'x]
    color = color[w[0]]
endelse
;
;  Display the image in the Z buffer.
;
dname = !d.name
set_plot, 'z'
device, set_resolution=[jpeg_size,jpeg_size]
if true_color gt 0 then device, set_pixel_depth=24
tv, image, true=true_color
;
;  Step through the planets to see if they appear in the image.
;
planets = ['Mercury', 'Venus', 'Earth', 'Mars', 'Jupiter Barycenter', $
           'Saturn Barycenter']
names = ['Mercury', 'Venus', 'Earth', 'Mars', 'Jupiter', 'Saturn']
;
;  Start with empty lists for the planet label information,
;
delvarx, f_name, f_nlength, f_istart, f_jpixel, f_jpos, f_above
;
for iplanet = 0,n_elements(planets)-1 do begin
    lonlat = get_stereo_lonlat(header.date_obs, header.obsrvtry, $
                               system='HPC', target=planets[iplanet], /deg)
    lonlat = lonlat[1:2]
    if wcs.cunit[0] eq 'arcsec' then lonlat = lonlat * 3600.
    pixel = wcs_get_pixel(wcs, lonlat)
;
;  Rotate around center to match image.
;
    if (roll ne 0) and (header.detector ne 'HI1') and $
      (header.detector ne 'HI2') then begin
        cosr = cos(roll * !dtor)
        sinr = sin(roll * !dtor)
        temp = pixel - (jpeg_size-1)/2.
        pixel[0] = temp[0]*cosr - temp[1]*sinr + (jpeg_size-1)/2.
        pixel[1] = temp[0]*sinr + temp[1]*cosr + (jpeg_size-1)/2.
    endif
;
;  If the planet is within the image, then mark it.
;  for later.
;
    if (pixel[0] gt 0) and (pixel[0] lt (jpeg_size-1)) and (pixel[1] gt 0) $
      and (pixel[1] lt (jpeg_size-1)) then begin
        if keyword_set(outline) then begin
            circle_sym, thick=5, symsize=3
            plots, pixel[0], pixel[1], /device, psym=8, color=0, _extra=_extra
        endif
        circle_sym, thick=2, symsize=3
        plots, pixel[0], pixel[1], /device, psym=8, color=color, _extra=_extra
;
;  Calculate the planet label information.
;
        name = names[iplanet]
        nlength = strlen(name) * !d.x_ch_size
        istart = pixel[0] - nlength / 2
        if istart lt 1 then istart = 1
        iend = istart + nlength
        if iend gt (jpeg_size-2) then istart = (jpeg_size-2) - nlength
        jpos = pixel[1] - 30
        if jpos lt (0.05*jpeg_size) then begin
            jpos = jpos + 60
            above = 1
        end else above = 0
;
;  Save the planet label information for below.
;
        boost_array, f_name, name
        boost_array, f_nlength, nlength
        boost_array, f_istart, istart
        boost_array, f_jpixel, pixel[1]
        boost_array, f_jpos, jpos
        boost_array, f_above, above
    endif
endfor
;
;  Sort the planet labels by position left to right.
;
if n_elements(f_istart) gt 1 then begin
    s = sort(f_istart)
    f_name    = f_name[s]
    f_nlength = f_nlength[s]
    f_istart  = f_istart[s]
    f_jpixel  = f_jpixel[s]
    f_jpos    = f_jpos[s]
    f_above   = f_above[s]
endif
;
;  If more than one planet was found, adjust the vertical positions of the
;  planet labels so that they don't overlap.  Repeat until no more
;  adjustments are made, but not more than 10 times.

nadj = n_elements(f_name) gt 0
niter = 0
while (nadj gt 0) and (niter lt 10) do begin
    nadj = 0
    niter = niter + 1
    for i=1,n_elements(f_name)-1 do begin
        for j=0,i-1 do begin
            if (f_istart[i] le (f_istart[j]+f_nlength[j])) and $
              (abs(f_jpos[i]-f_jpos[j]) lt !d.y_ch_size) then begin
                if f_above[i] eq 0 then begin
                    f_jpos[i] = f_jpos[i] - !d.y_ch_size
                    if f_jpos[i] lt (0.05*jpeg_size) then begin
                        f_jpos[i] = f_jpixel[i] + 60
                        f_above[i] = 1
                    endif
                end else f_jpos[i] = f_jpos[i] + !d.y_ch_size
                nadj = nadj + 1
            endif
        endfor
    endfor
endwhile
;
;  Overplot the labels.
;
for i = 0,n_elements(f_name)-1 do begin
    name = f_name[i]
    istart = f_istart[i]
    jpos = f_jpos[i]
    if keyword_set(outline) then $
      xyouts, istart, jpos, name, /device, color=0, charthick=5, _extra=_extra
    xyouts, istart, jpos, name, /device, color=color, _extra=_extra
endfor
;
;  Read the image back in from the Z-buffer, and return to the previous
;  device after putting the Z buffer back into 8-bit mode.
;
image = tvrd(true=true_color)
device, set_pixel_depth=8
set_plot, dname
;
;  If the /DISPLAY keyword is set, then display the modified image.
;
if keyword_set(display) then wdisplay, image, true=true_color, _extra=_extra
;
end
