;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_BEACON_COVERAGE
;
; Purpose     :	Process the STEREO in-situ beacon data
;
; Category    :	STEREO, Telemetry
;
; Explanation :	Steps through the IMPACT and PLASTIC beacon telemetry files,
;               processes them into CDF files, and generates a plot of the
;               data.
;
; Syntax      :	SSC_BEACON_COVERAGE
;
; Examples    :	
;
; Inputs      :	None.
;
; Opt. Inputs :	None.
;
; Outputs     :	Generates the beacon CDF files, and the realtime plot.
;
; Opt. Outputs:	None.
;
; Keywords    :	TEST    = If set, then starts at the beginning of March 2006, 
;                         instead of opening the most recent telemetry files.
;                         Stops when you reach the end of any data.
;
;               NOWAIT  = Normally, if all the files are still at the
;                         end-of-file mark at the end of
;                         SSC_BEACON_REFRESH_FILES, a small wait is put in to
;                         control CPU usage.  If /NOWAIT is passed, then this
;                         wait is bypassed.
;
;               NOSHOW  = If set, don't generate the plot.
;
;               QUIET   = Value for !QUIET.  Default is 1.
;
; Calls       :	SEP_BEACON_COMMON, SWEASTE_B_COMMON, SSC_BEACON_OPEN_FILES,
;               DELVARX, READ_STEREO_PKT, PARSE_STEREO_PKT, ANYTIM2CAL,
;               IMPACT_BEACON, PLASTIC_PARSE_PACKET, SSC_BEACON_OPEN_NEXT,
;               SSC_BEACON_PLOTS, SSC_BEACON_REFRESH_FILES, SSC_BEACON_PATH,
;               SSC_PLOT_COVERAGE
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 10-Feb-2006, William Thompson, GSFC
;               Version 2, 21-Feb-2007, WTT, added packet size test
;               Version 3, 26-Jun-2007, William Thompson, GSFC
;                       Use V02 for IMPACT, RTN coordinates, SEP parameters
;               Version 4, 08-Apr-2008, WTT, Call SSC_PLOT_COVERAGE
;               Version 5, 04-Nov-2009, WTT, Catch restore errors
;               Version 6, 09-Nov-2009, WTT, Wait 24 hours before deleting
;                                            save file
;               Version 7, 13-Nov-2009, WTT, Remove 24 hour rule.
;                       Save into temporary file first.
;               Version 8, 23-Nov-2009, WTT, Restore 24 hour rule.
;               Version 9, 01-Jul-2011, WTT, Filter out dates after 2035
;               Version 10, 02-May-2013, WTT, tighten up valid date test
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_beacon_coverage, test=test, nowait=nowait, noshow=noshow, quiet=quiet
;
;  Set the !QUIET system variable.
;
if n_elements(quiet) eq 1 then !quiet = quiet else !quiet = 1
;
;  Restore any existing coverage data.  If any errors are found recovering the
;  file, then delete the file instead.
;
coverage_file = 'coverage.idl'
catch, error_status
if error_status ne 0 then begin
    dtime = systime(1) - (file_info(coverage_file)).mtime
    if dtime gt 86400 then begin
        print, 'Deleting ' + coverage_file
        file_delete, coverage_file
        catch,/cancel
    end else begin
        print, 'Unable to restore save file--returning'
        return
    endelse
endif
;
if file_exist(coverage_file) then restore, coverage_file else $
  delvarx, pkt_dates, dsn_ids, sc_ids
;
;  Open up the beacon files.
;
ssc_beacon_open_files, impactlun, plasticlun, swaveslun, secchilun, $
  /header_only, test=test
;
;  Step through the packets, and process them.
;
done = 0
sc = ['STA','STB']
lasttime = systime(1)
repeat begin
;
;  Step through the two spacecraft.  Initialize N_FOUND and PKT_DATES
;
    n_found = 0
    for isc = 0,1 do begin
;
;  Look for an IMPACT packet.  If found, collect the packet date.  Packets with
;  a datatype of zero mark the end of the packet file.
;
        if not eof(impactlun[isc]) then begin
            read_stereo_pkt, impactlun[isc], packet, /header_only
            if packet.grh.datatype ne 0 then begin
;
;  Make sure that the packet is really a beacon packet, and that the date is
;  valid (i.e. not back in 1958), and that the ground receipt and packet header
;  sizes agree.
;
                diff = packet.grh.size - packet.pkt.size
                if packet.grh.datatype eq 2 then diff_expect = 64 else $
                  diff_expect = 33
                pkt_date = parse_stereo_pkt(packet, /pkt_date)
                if keyword_set(test) then begin
                    minmjd = 50000
                    maxmjd = 65000
                end else begin
                    get_utc, today
                    minmjd = today.mjd - 3
                    maxmjd = today.mjd + 1
                endelse
                if (parse_stereo_pkt(packet, /data_fmt_id) ge 112) and $
                  (pkt_date.mjd ge minmjd) and (pkt_date.mjd le maxmjd) and $
                  (diff eq diff_expect) then begin
;
;  Collect the packet dates.
;
                    if n_elements(pkt_dates) eq 0 then begin
                        pkt_dates = pkt_date
                        dsn_ids = packet.grh.dsn_antenna_id
                        sc_ids  = packet.grh.spacecraft_id
                    end else begin
                        pkt_dates = [pkt_dates, pkt_date]
                        dsn_ids = [dsn_ids, packet.grh.dsn_antenna_id]
                        sc_ids  = [sc_ids,  packet.grh.spacecraft_id]
                    endelse
                    n_found = n_found + 1
                endif
;
;  If an end-of-file packet was found, then try to open the next file.
;
            end else ssc_beacon_open_next, impactlun, isc, /header_only
        endif
;
;  Look for a PLASTIC packet.  If found, collect the packet date.
;
        if not eof(plasticlun[isc]) then begin
            read_stereo_pkt, plasticlun[isc], packet, /header_only
            if packet.grh.datatype ne 0 then begin
;
;  Make sure that the packet is really a beacon packet, and that the date is
;  valid (i.e. not back in 1958).
;
                pkt_date = parse_stereo_pkt(packet, /pkt_date)
                if (parse_stereo_pkt(packet, /data_fmt_id) ge 112) and $
                  (pkt_date.mjd gt 50000) and (pkt_date.mjd lt 65000) then begin
;
;  Collect the packet dates.
;
                    if n_elements(pkt_dates) eq 0 then begin
                        pkt_dates = pkt_date
                        dsn_ids = packet.grh.dsn_antenna_id
                        sc_ids  = packet.grh.spacecraft_id
                    end else begin
                        pkt_dates = [pkt_dates, pkt_date]
                        dsn_ids = [dsn_ids, packet.grh.dsn_antenna_id]
                        sc_ids  = [sc_ids,  packet.grh.spacecraft_id]
                    endelse
                    n_found = n_found + 1
                endif
;
;  If an end-of-file packet was found, then try to open the next file.
;
            end else ssc_beacon_open_next, plasticlun, isc, /header_only
        endif
;
;  Look for a SWAVES packet.  If found, collect the packet date.
;
        if not eof(swaveslun[isc]) then begin
            read_stereo_pkt, swaveslun[isc], packet, /header_only
            if packet.grh.datatype ne 0 then begin
;
;  Make sure that the packet is really a beacon packet, and that the date is
;  valid (i.e. not back in 1958).
;
                pkt_date = parse_stereo_pkt(packet, /pkt_date)
                if (parse_stereo_pkt(packet, /data_fmt_id) ge 112) and $
                  (pkt_date.mjd gt 50000) and (pkt_date.mjd lt 65000) then begin
;
;  Collect the packet dates.
;
                    if n_elements(pkt_dates) eq 0 then begin
                        pkt_dates = pkt_date
                        dsn_ids = packet.grh.dsn_antenna_id
                        sc_ids  = packet.grh.spacecraft_id
                    end else begin
                        pkt_dates = [pkt_dates, pkt_date]
                        dsn_ids = [dsn_ids, packet.grh.dsn_antenna_id]
                        sc_ids  = [sc_ids,  packet.grh.spacecraft_id]
                    endelse
                    n_found = n_found + 1
                endif
;
;  If an end-of-file packet was found, then try to open the next file.
;
            end else ssc_beacon_open_next, swaveslun, isc, /header_only
        endif
;
;  Look for a SECCHI packet.  If found, collect the packet date.
;
        if not eof(secchilun[isc]) then begin
            read_stereo_pkt, secchilun[isc], packet, /header_only
            if packet.grh.datatype ne 0 then begin
;
;  Make sure that the packet is really a beacon packet, and that the date is
;  valid (i.e. not back in 1958).
;
                pkt_date = parse_stereo_pkt(packet, /pkt_date)
                if (parse_stereo_pkt(packet, /data_fmt_id) ge 112) and $
                  (pkt_date.mjd gt 50000) and (pkt_date.mjd lt 65000) then begin
;
;  Collect the packet dates.
;
                    if n_elements(pkt_dates) eq 0 then begin
                        pkt_dates = pkt_date
                        dsn_ids = packet.grh.dsn_antenna_id
                        sc_ids  = packet.grh.spacecraft_id
                    end else begin
                        pkt_dates = [pkt_dates, pkt_date]
                        dsn_ids = [dsn_ids, packet.grh.dsn_antenna_id]
                        sc_ids  = [sc_ids,  packet.grh.spacecraft_id]
                    endelse
                    n_found = n_found + 1
                endif
;
;  If an end-of-file packet was found, then try to open the next file.
;
            end else ssc_beacon_open_next, secchilun, isc, /header_only
        endif
    endfor
;
;  If any packets were found, then generate a new plot.
;
    if (n_found ge 1) and not keyword_set(noshow) then begin
        time = systime(1)
        if (time-lasttime) gt 15 then begin
            ssc_plot_coverage, pkt_dates, dsn_ids, sc_ids
            save, file=coverage_file+'.temp', pkt_dates, dsn_ids, sc_ids
            file_move, coverage_file+'.temp', coverage_file, /overwrite
            lasttime = time
        endif
    endif
;
;  Age off any packet dates that are more than 5 days old.
;
    mjdmax = max(pkt_dates.mjd, min=mjdmin)
    if (mjdmax-mjdmin) gt 5 then begin
        w = where(pkt_dates.mjd gt (mjdmax-5), count)
        if count gt 0 then begin
            pkt_dates = pkt_dates[w]
            dsn_ids   = dsn_ids[w]
            sc_ids    = sc_ids[w]
            save, file=coverage_file+'.temp', pkt_dates, dsn_ids, sc_ids
            file_move, coverage_file+'.temp', coverage_file, /overwrite
        endif
    endif
;
;  Refresh any files that need it.  Let SSC_BEACON_REFRESH_FILES return a DONE
;  signal.
;
    ssc_beacon_refresh_files, impactlun, plasticlun, swaveslun, secchilun, $
      done=done, nowait=nowait, /header_only
;
;  If the /TEST keyword is not set, never stop.
;
    if not keyword_set(test) then done = 0
endrep until done
;
;  Close the files.
;
if impactlun[0] gt 0 then free_lun, impactlun[0]
if impactlun[1] gt 0 then free_lun, impactlun[1]
if plasticlun[0] gt 0 then free_lun, plasticlun[0]
if plasticlun[1] gt 0 then free_lun, plasticlun[1]
if swaveslun[0] gt 0 then free_lun, swaveslun[0]
if swaveslun[1] gt 0 then free_lun, swaveslun[1]
if secchilun[0] gt 0 then free_lun, secchilun[0]
if secchilun[1] gt 0 then free_lun, secchilun[1]
;
end
