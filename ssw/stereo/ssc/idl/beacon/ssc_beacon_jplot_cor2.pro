;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_BEACON_JPLOT_COR2
;
; Purpose     :	Produce J-plots from COR2 beacon data
;
; Category    :	SECCHI, Quicklook
;
; Explanation :	This routine processes SECCHI/COR2 beacon images to produce
;               time-elongation plots (aka "J-plots").  Each file is processed
;               individually, and merged with previously processed data stored
;               in a common block.  An IDL save file is used to maintain data
;               between sessions.
;
; Syntax      :	SSC_BEACON_JPLOT, COR2, FILENAME
;
; Examples    :	See SSC_BROWSE_SECCHI_JPEG
;
; Inputs      :	FILENAME = The name of the COR2 file(s) to process.  Double
;                          exposure images are passed as a single filename.
;                          Polarization sequences are sent as a set of three
;                          filenames.
;
; Opt. Inputs :	None.
;
; Outputs     :	This routine writes GIF files to the $STEREO_BEACON_HTML
;               directory.
;
; Opt. Outputs:	None.
;
; Keywords    :	TEST_MODE = Used for testing via level-0 beacon files.
;
; Env. Vars.  :	STEREO_BEACON_HTML = Location of beacon web pages
;
; Calls       :	SECCHI_PREP, PARSE_STEREO_NAME, AVERAGE, UTC2TAI,
;               COR1_PBSERIES, DATATYPE, SCC_READ_SUMMARY, SCCFINDFITS,
;               ANYTIM2UTC, CHECK_INT_TIME, FITSHEAD2WCS, WCS_GET_COORD,
;               SETPLOT, NTRIM, UTPLOT_IMAGE, TVREAD, CONCAT_DIR, WRITE_GIF,
;               DELVARX
;
; Common      :	SSC_BEACON_JPLOT_COR2 is used internally to maintain
;               information between calls.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 1-Jul-2009, William Thompson, GSFC
;               Version 2, 10-Jul-2009, WTT, better rejection of bad images
;               Version 3, 04-Nov-2009, WTT, Catch restore errors
;               Version 4, 09-Nov-2009, WTT, Wait 24 hours before deleting
;                                            save file
;               Version 5, 13-Nov-2009, WTT, Remove 24 hour rule.
;                       Save into temporary file first.
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_beacon_jplot_cor2, filename, test_mode=test_mode
;
common ssc_beacon_jplot_cor2, utc_cor2, tai_cor2, jplot_cor2
;
;  If the common blocks variables haven't been defined yet, then look for an
;  IDL save file.
;
savefile = 'ssc_beacon_jplot_cor2.idl'
catch, error_status
if error_status ne 0 then begin
    print, 'Deleting ' + savefile
    file_delete, savefile
    catch,/cancel
endif
;
if (n_elements(utc_cor2) eq 0) and file_exist(savefile) then restore, savefile
;
;  Set the BEACON parameter based on the /TEST_MODE keyword
;
beacon = 1 - keyword_set(test_mode)
;
;  Set up the parameters.
;
tdelta = 900.d0                         ;15 minutes
thalf = tdelta / 2
sixhour = 21600D3                       ;milliseconds
nsix = round(sixhour / tdelta / 1000)   ;six hours
ndays  = 5
ntimes = (4*ndays+1)*nsix               ;N days + 6 hours
nstart = 4*(ndays-2)*nsix               ;Show 2 days + 6 hours
rdelta = 0.2                            ;solar radii
nrad = round(12 / rdelta)               ;From 3 to 15 solar radii
rhalf = rdelta / 2
rad = 3 + rdelta*findgen(nrad) + rhalf  ;Central values
pa = [60,90,120]                        ;Position angles
npa = n_elements(pa)
pahalf = 15.                            ;Pos. angle half-width
;
;  If the size of the common block variables doesn't match the above
;  definitions, then reset them.
;
if n_elements(utc_cor2) ne ntimes then delvarx, utc_cor2, tai_cor2, jplot_cor2
;
;  Read in the file.
;
polar = n_elements(filename) eq 3
secchi_prep, sccfindfits(filename, beacon=beacon), h1, a1, /calimg_off, $
  /silent
if total(h1.nmissing) gt 0 then return          ;Incomplete image(s)
if keyword_set(polar) then begin
    a1 = 2*average(a1,3)
    h1 = h1[1]
endif
sc = parse_stereo_name(h1.obsrvtry, ['Ahead','Behind'])
if sc eq 'Ahead' then isc = 0 else isc = 1
tai1 = utc2tai(h1.date_obs)
;
;  Find the file(s) from an hour earlier, +/- TDELTA/2.
;
t0 = tai1 - 3600.d0 - thalf
t0 = [t0, t0+tdelta]
utc0 = tai2utc(t0)
if polar then begin
    if h1.downlink eq 'SW' then ssr = 7 else ssr = 3
    list = cor1_pbseries(utc0, sc, /cor2, beacon=beacon, ssr=ssr, /quiet)
    if datatype(list) ne 'STC' then return
    if total(list.nmiss) gt 0 then return       ;Incomplete image(s)
    file0 = list[*,0].filename
end else begin
    list = scc_read_summary(date=utc0, spacecraft=sc, telescope='cor2', $
                            type='img', beacon=beacon)
    if datatype(list) ne 'STC' then return
    w = where(list.dest eq h1.downlink, count)
    if count gt 0 then list = list[w]
    if list[0].nmiss gt 0 then return           ;Incomplete image
    file0 = sccfindfits(list[0].filename, beacon=beacon)
endelse
;
;  Read in the earlier file, and form the difference image.
;
sz = size(a1)
secchi_prep, file0, h0, a0, /calimg_off, /silent, outsize=sz[1]
if total(h0.nmissing) gt 0 then return          ;Incomplete image(s)
if keyword_set(polar) then begin
    a0 = 2*average(a0,3)
    h0 = h0[1]
endif
diff = a1 - a0
;
;  Make sure the common block variables are defined.
;
if n_elements(utc_cor2) eq 0 then begin
;
;  Calculate a start time on the appropriate 6 hour boundary on the previous
;  day, i.e. between 24-30 hours before the current time.
;
    tstart = anytim2utc(h1.date_obs)
    tstart.mjd = tstart.mjd - ndays
    tstart.time = sixhour * floor(tstart.time/sixhour) + thalf * 1000
    check_int_time, tstart
;
;  Generate an array of times.
;
    utc_cor2 = replicate(tstart, ntimes)
    utc_cor2.time = utc_cor2.time + tdelta * lindgen(ntimes) * 1000
    check_int_time, utc_cor2
    tai_cor2 = utc2tai(utc_cor2)
    tmin = min(tai_cor2, max=tmax)
;
;  Create the JPLOT array.
;
    jplot_cor2 = replicate(!values.f_nan, ntimes, nrad, npa, 2, 2)
endif
;
;  If the time in the FITS file is significantly after the largest time in the
;  array, then move by six hours.
;
t1 = tai1 - thalf
while t1 gt tai_cor2[ntimes-1] do begin
    jplot_new = replicate(!values.f_nan, ntimes, nrad, npa, 2, 2)
    jplot_new[0,0,0,0,0] = jplot_cor2[nsix:*,*,*,*,*]
    jplot_cor2 = temporary(jplot_new)
    utc_cor2 = replicate(utc_cor2[nsix], ntimes)
    utc_cor2.time = utc_cor2.time + tdelta * lindgen(ntimes) * 1000
    check_int_time, utc_cor2
    tai_cor2 = utc2tai(utc_cor2)
    tmin = min(tai_cor2, max=tmax)
endwhile
;
;  If the time is significantly earlier than the smallest time in the array,
;  then return.
;
t1 = tai1 + thalf
if t1 lt tai_cor2[0] then return
;
;  Determine the time index for these data.
;
del = abs(tai_cor2 - tai1)
w = where(del eq min(del), count)
itime = w[0]
if count gt 1 then for i=0,count-1 do $
  if not finite(jplot_cor2[w[i],0,0,0,isc]) then itime = w[i]
;
;  Calculate the position angle and radius arrays.
;
wcs = fitshead2wcs(h1)
coord = wcs_get_coord(wcs)
pa1 = reform(atan(-coord[0,*,*], coord[1,*,*]))
w = where(pa1 lt 0, count)
if count gt 0 then pa1[w] = pa1[w] + 2.d0*!dpi
pa1 = pa1 * (180.d0 / !dpi)
rad1 = sqrt(total(coord^2, 1)) / h1.rsun
;
;  Calculate the J-plot values for each position angle.
;
for irad = 0,nrad-1 do begin
    for ipa = 0,npa-1 do begin
        for ilimb = 0,1 do begin
            pa0 = pa[ipa]
            if ilimb eq 1 then pa0 = 360 - pa0
            w = where((rad1 ge (rad[irad]-rhalf)) and $
                      (rad1 le (rad[irad]+rhalf)) and $
                      (pa1 ge (pa0-pahalf)) and $
                      (pa1 le (pa0+pahalf)), count)
            if count gt 0 then jplot_cor2[itime,irad,ipa,ilimb,isc] = $
              average(diff[w])
        endfor
    endfor
endfor
;
;  Save the common block variables in the save file.
;
save, file=savefile+'.temp', utc_cor2, tai_cor2, jplot_cor2
file_move, savefile+'.temp', savefile, /overwrite
;
;  Make the GIF files.
;
jmax = 5e-12
dname = !d.name
setplot, 'z'
device, set_resolution=[800,250]
loadct, 3, /silent
limb = ['East', 'West']
for ipa=0,npa-1 do begin
    for ilimb=0,1 do begin
        latitude = 90 - pa[ipa]
        if latitude gt 0 then latitude = '+' + ntrim(latitude) else $
          latitude = ntrim(latitude)
        title = 'STEREO COR2 ' + sc + ', ' + limb[ilimb] + $
          ' limb, latitude ' + latitude
        utplot_image, jplot_cor2[nstart:*,*,ipa,ilimb,isc], $
          utc_cor2[nstart:*], rad, min=-jmax, max=jmax, $
          xticklen=-0.03, yticklen=-0.01, tick_unit=21600, $
          ytitle='Solar radii', title=title, yticks=4, yminor=3
        image = tvread(red, green, blue, /quiet)
        filename = 'jplot_cor2_' + strlowcase(sc) + '_' + $
          strlowcase(limb[ilimb]) + '_' + string(pa[ipa],format='(I3.3)') + $
          '.gif'
        path = getenv('STEREO_BEACON_HTML')
        if path ne '' then filename = concat_dir(path, filename)
        write_gif, filename, image, red, green, blue
    endfor
endfor
setplot, dname
;
end
