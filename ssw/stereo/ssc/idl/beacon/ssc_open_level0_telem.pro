;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_OPEN_LEVEL0_TELEM
;
; Purpose     :	Open the appropriate level-0 telemetry file
;
; Category    :	STEREO, Telemetry
;
; Explanation :	This procedure is called from SSC_BEACON_FROM_LEVEL0 to open up
;               the appropriate level-0 telemetry file for processing.
;
; Syntax      :	UNIT = SSC_OPEN_LEVEL0_TELEM(ISC, DATE, INSTRUMENT)
;
; Examples    :	See SSC_BEACON_FROM_LEVEL0
;
; Inputs      :	ISC        = Either 0 for Ahead, or 1 for Behind
;               DATE       = The date of the telemetry file.
;               INSTRUMENT = Either "impact" or "plastc".
;
; Opt. Inputs :	None.
;
; Outputs     :	The result of the function is the logical unit number of the
;               opened file.  If no file was opened, then -1 is returned.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Calls       :	CONCAT_DIR, ANYTIM2UTC, UTC2DOY
;
; Common      :	None.
;
; Env. Vars.  : STEREO_TELEM points to the top of the level-0 telemetry tree.
;               Under this, the routine looks for "ahead" and "behind",
;               followed by the instrument name, and the year and month.
;
; Restrictions:	None.
;
; Side effects:	Prints the name of the telemetry file, unless !QUIET=1.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 28-Jan-2008, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
function ssc_open_level0_telem, isc, date, instrument
;
sc = ['ahead','behind']
;
;  Form the path to the telemetry files.
;
path = concat_dir(getenv('STEREO_TELEM'), sc[isc])
path = concat_dir(path, instrument)
;
;  Include the year and month.
;
utc = anytim2utc(date, /external)
path = concat_dir(path, string(utc.year,  format='(I4.4)'))
path = concat_dir(path, string(utc.month, format='(I2.2)'))
;
;  Form the name of the telemetry files.
;
name = instrument + '_' + sc[isc] + '_' + string(utc.year, format='(I4.4)')
doy = utc2doy(utc)
name = name + '_' + string(doy, format='(I3.3)') + '_1_'
name = concat_dir(path, name)
;
;  Look for telemetry files.  First look for files with the ".fin" (final)
;  extension.  If not found, look for .ptp files.
;
ext = 'fin'
files = file_search(name + '*.' + ext, count=count)
if count eq 0 then begin
    ext = 'ptp'
    files = file_search(name + '*.' + ext, count=count)
    if count eq 0 then return, -1
endif
;
;  Take the file with the highest version number.
;
files = files[reverse(sort(files))]
file = files[0]
;
;  If the file extension is ptp, make sure that the version number is at least
;  02.
;
if ext eq 'ptp' then begin
    len = strlen(file)
    version = strmid(file,len-6,2)
    if version lt '02' then return, -1
endif
;
;  Open the file, and make sure the file has some packets in it.
;
openr, unit, file, /get_lun
if (fstat(unit)).size eq 0 then begin
    free_lun, unit
    return, -1
endif
;
if not !quiet then print, file
return, unit
end
