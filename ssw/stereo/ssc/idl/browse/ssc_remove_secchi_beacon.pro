;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_REMOVE_SECCHI_BEACON
;
; Purpose     :	Remove obsolete SECCHI beacon images
;
; Category    :	SECCHI, Quicklook
;
; Explanation :	Moves obsolete SECCHI beacon images to an "old" subdirectory
;
; Syntax      :	SSC_REMOVE_SECCHI_BEACON, FILENAME
;
; Examples    :	See SSC_BROWSE_SECCHI_JPEG
;
; Inputs      :	FILENAME = Array of filenames to remove, complete with path.
;                          Instead of deleting, the files are moved to a
;                          subdirectory called "old".
;
; Opt. Inputs :	None.
;
; Outputs     :	None.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Env. Vars.  :	None.
;
; Calls       :	BREAK_FILE, CONCAT_DIR, MK_DIR
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 28-Feb-2011, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_remove_secchi_beacon, filename
;
for i=0,n_elements(filename)-1 do begin
    break_file, filename[i], disk, dir, name, ext
    newdir = concat_dir(disk+dir, 'old')
    if not file_exist(newdir) then mk_dir, newdir
    newfile = concat_dir(newdir, name + ext)
    file_move, filename[i], newfile, /overwrite
endfor
;
end
