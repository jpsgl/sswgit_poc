;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_BROWSE_SECCHI_MPEG
;
; Purpose     :	Make daily SECCHI MPEG movies
;
; Category    :	SECCHI, Quicklook
;
; Explanation :	Uses MPEG_ENCODE to merge JPEG images into MPEG movies.  All
;               telescopes are processed for the specified date.  Existing MPEG
;               movies are regenerated only if they are older than the
;               corresponding JPEG directory.  Called from SSC_MAKE_BROWSE.
;
; Syntax      :	SSC_BROWSE_SECCHI_MPEG, DATE
;
; Examples    :	See SSC_MAKE_BROWSE
;
; Inputs      :	DATE    = The date to process.
;
; Opt. Inputs :	None.
;
; Outputs     :	MPEG files are written to the appropriate directory.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Env. Vars.  :	STEREO_BROWSE   = Top browse directory
;
; Calls       :	ANYTIM2UTC, SSW_BIN, CONCAT_DIR, ANYTIM2CAL, BREAK_FILE,
;               FILE_EXIST, MK_TEMP_FILE, GET_TEMP_DIR
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	Partially based on WR_MOVIE.
;
; History     :	Version 1, 05-Sep-2007, William Thompson, GSFC
;               Version 2, 07-Sep-2007, WTT, Use MK_TEMP_FILE, /RANDOM
;               Version 3, 12-Dec-2011, WTT, include _rdiff directories
;               Version 4, 28-Aug-2014, WTT, enforce # of images limit
;               Version 5, 30-Nov-2017, WTT, include COR1 pB
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_browse_secchi_mpeg, date
;
stereo_browse = getenv('STEREO_BROWSE')
if n_params() eq 0 then get_utc, utc, /ext else utc = anytim2utc(date, /ext)
;
mpeg_encode_command = ssw_bin('mpeg_encode', found=found)
;
sc = ['behind','ahead']
tel = ['euvi','cor1','cor1_rdiff','cor1_pb','cor2','cor2_rdiff', $
       'hi1','hi1_rdiff','hi2']
wave = ['195','171','284','304']
res = ['512','256']
nres = n_elements(res)
;
sdate = string(utc.year,  format='(I4.4)') + '/' + $
        string(utc.month, format='(I2.2)') + '/' + $
        string(utc.day,   format='(I2.2)')
filepath = concat_dir(stereo_browse, sdate)
caldate = anytim2cal(utc, form=8, /date)
;
;  Step through the various JPEG directories.
;
for isc = 0,1 do begin
    for itel=0,n_elements(tel)-1 do begin
        telescope = tel[itel]
        if telescope eq 'euvi' then nwave=n_elements(wave) else nwave=1
        for iwave=0,nwave-1 do begin
            for ires=0,nres-1 do begin
;
;  Find the directory containing the JPEG files, and make sure that it exists.
;
                jpeg_path = concat_dir(filepath, sc[isc] + '/' + telescope)
                if telescope eq 'euvi' then jpeg_path = $
                  concat_dir(jpeg_path,wave[iwave])
                jpeg_path = concat_dir(jpeg_path, res[ires])
;
;  Get the names of the jpeg files, if any.  Only process the directory if at
;  least 6 images are found.
;
                jpeg_files = file_search(concat_dir(jpeg_path,'*.jpg'), $
                                         count=count)
                if count gt 5 then begin
                    break_file, jpeg_files, disk, dir, name, ext
                    files = name + ext
;
;  Form the the name of the output MPEG file.
;
                    mpeg_file = sc[isc] + '_' + caldate + '_' + telescope
                    if telescope eq 'euvi' then mpeg_file = $
                      mpeg_file + '_' + wave[iwave]
                    mpeg_file = mpeg_file + '_' + res[ires] + '.mpg'
                    mpeg_file = concat_dir(filepath, mpeg_file)
;
;  If the file already exists, then check the modification dates of the MPEG
;  file and corresponding JPEG files.  Only proceed if the MPEG file is older
;  than the JPEG files, or the MPEG file doesn't exist.
;
                    if file_exist(mpeg_file) then begin
                        mpeg_date = (file_info(mpeg_file)).mtime
                        jpeg_date = max((file_info(jpeg_files)).mtime)
                        test = jpeg_date gt mpeg_date
                    end else test = 1
                    if test then begin
;
;  Open the MPEG parameter file for writing.  Store logical unit number in
;  MPRM.
;
                        paramfile = mk_temp_file(dir=get_temp_dir(), $
                                                'params.mpeg', /random)
                        openw, mprm, paramfile, /get_lun
;
;  Write out the mpeg parameter file.
;
                        printf, mprm, 'PATTERN          IBBBBBBBBBBP'
                        printf, mprm, 'OUTPUT           ' + mpeg_file
                        printf, mprm, 'GOP_SIZE 12'
                        printf, mprm, 'SLICES_PER_FRAME 5'
                        printf, mprm, 'BASE_FILE_FORMAT JPEG'
                        printf, mprm, 'INPUT_CONVERT *'
                        printf, mprm, 'INPUT_DIR        ' + jpeg_path
                        printf, mprm, 'INPUT'
                        for i=0,n_elements(files)-1 do printf, mprm, files[i]
                        printf, mprm, 'END_INPUT'
                        printf, mprm, 'PIXEL            FULL'
                        printf, mprm, 'RANGE            5'
                        printf, mprm, 'PSEARCH_ALG      LOGARITHMIC'
                        printf, mprm, 'BSEARCH_ALG      SIMPLE'
                        printf, mprm, 'IQSCALE          6'
                        printf, mprm, 'PQSCALE          6'
                        printf, mprm, 'BQSCALE          6'
                        printf, mprm, 'REFERENCE_FRAME  ORIGINAL'
                        printf, mprm, 'FORCE_ENCODE_LAST_FRAME'
;
;  Close the MPEG parameter file, and spawn a shell to process the mpeg_encode
;  command.
;
                        free_lun, mprm
                        spawn, mpeg_encode_command + ' ' + paramfile
                        file_delete, paramfile
                    endif       ;Path newer than MPEG file
                endif           ;Path exists
            endfor              ;ires
        endfor                  ;iwave
    endfor                      ;itel
endfor                          ;isc
;
return
end
