;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_BROWSE_INGEST_HI1_SREM
;
; Purpose     :	Ingest HI1 star-removed PNG files into SSC browse pages
;
; Category    :	SECCHI, Quicklook
;
; Explanation :	Called from SSC_MAKE_BROWSE to ingest HI1 star-removed PNG
;               images into the STEREO browse pages.  Any appropriate images in
;               the $NRL_PNG directory are mirrored over into the browse
;               pages.  128x128 thumbnail images are also generated, as well as
;               image captions and HTML directory descriptions.
;
; Syntax      :	SSC_BROWSE_INGEST_HI1_SREM, DATE
;
; Examples    :	SSC_BROWSE_INGEST_HI1_SREM, '2018-04-04'
;
; Inputs      :	DATE    = Date to process.
;
; Opt. Inputs :	None.
;
; Outputs     :	None.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Calls       :	ANYTIM2CAL, ANYTIM2UTC, CONCAT_DIR, BREAK_FILE, REDUCE,
;               SSC_SECCHI_CAPTION, SSC_SECCHI_DIR_HTML
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, William Thompson, GSFC, 13-Apr-2018
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_browse_ingest_hi1_srem, date
;
;  Format the date into the forms needed by the program.
;
sdate0 = anytim2cal(date, form=8, /date)
syear = strmid(sdate0, 0, 4)
sdate1 = anytim2utc(date, /ecs, /date_only)
;
;  Get the environment variables.
;
stereo_browse = getenv('STEREO_BROWSE')
nrl_png = getenv('NRL_PNG')
;
;  Step through the spacecraft, and form the input and output directories.
;
sc = ['ahead', 'behind']
for isc=0,1 do begin
    path0 = concat_dir(nrl_png, strmid(sc[isc],0,1))
    path0 = concat_dir(path0, 'hi_srem')
    path0 = concat_dir(path0, syear)
    path0 = concat_dir(path0, sdate0)
    path0 = concat_dir(path0, '512')
;
    path1 = concat_dir(stereo_browse, sdate1)
    path1 = concat_dir(path1, sc[isc])
    path1 = concat_dir(path1, 'hi1_srem')
;
;  Include the H1 filenames as part of the input directory, and search for the
;  files.
;
    path0 = concat_dir(path0, '*h1*.png')
    files = file_search(path0, count=count)
;
;  If any files were found, the use rsync to ingest them into the browse pages.
;
    if count gt 0 then begin
        path2 = concat_dir(path1, '512')
        if not file_exist(path2) then file_mkdir, path2
        spawn, 'rsync -aWuv ' + path0 + ' ' + path2
        files = file_search(path2, '*.png', count=count)
;
;  Create directories for compressed versions of the images to use as
;  thumbnails.
;
        path3 = concat_dir(path1, '256')
        path4 = concat_dir(path1, '128')
        if not file_exist(path3) then file_mkdir, path3
        if not file_exist(path4) then file_mkdir, path4
;
;  Step through the files, and compare the times of the input files and the
;  compressed versions.
;
        for ifile=0,count-1 do begin
            t0 = (file_info(files[ifile])).mtime
            break_file, files[ifile], disk, dir, name0
            name3 = concat_dir(path3, name0 + '.png')
            name4 = concat_dir(path4, name0 + '.png')
            if file_exist(name3) then t3 = (file_info(name3)).mtime else t3 = 0
            if file_exist(name4) then t4 = (file_info(name4)).mtime else t4 = 0
;
;  If the input file is newer than the compressed versions, or the compressed
;  versions don't exist, then create the compressed versions.
;
            if t0 gt (t3<t4) then begin
                read_png, files[ifile], image, r, g, b
                write_png, name3, reduce(image, 2), r, g, b
                write_png, name4, reduce(image, 4), r, g, b
;
;  Also generate the caption for all versions of the file.
;
                fits_name = name0 + '.fts'
                strput, fits_name, 'n4', 16
                fits_name = sccfindfits(fits_name)
                if fits_name ne '' then begin
                    dummy = sccreadfits(fits_name, header, /nodata)
                    ssc_secchi_caption, files[ifile], header
                    ssc_secchi_caption, name3, header
                    ssc_secchi_caption, name4, header
                endif
            endif
        endfor
;
;  Create the HTML directories.
;
        ssc_secchi_dir_html, path2, /png
        ssc_secchi_dir_html, path3, /png
        ssc_secchi_dir_html, path4, /png
;
    endif                       ;Files were found
endfor                          ;isc
;
end
