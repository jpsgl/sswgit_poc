;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_BROWSE_RDIFF_COR1
;
; Purpose     :	Creates COR1 running difference images
;
; Category    :	SECCHI, Quicklook
;
; Explanation : Called from SSC_BROWSE_SECCHI_JPEG to create a running
;               difference version of a COR1 browse image for the SSC website.
;               The relevant image from 20 minutes earlier is read in and
;               subtracted from the supplied image.
;
; Syntax      :	SSC_BROWSE_RDIFF_COR1, HEADER1, IMAGE1, DIFF
;
; Examples    :	See SSC_BROWSE_SECCHI_JPEG
;
; Inputs      :	HEADER1 = Header of image to be subtracted from
;
;               IMAGE1  = Image to be subtracted from.
;
; Opt. Inputs :	None.
;
; Outputs     :	DIFF    = Byte-scaled difference image
;
; Opt. Outputs:	None.
;
; Keywords    :	BEACON  = Set to true if one is processing beacon images.
;
; Calls       :	UTC2TAI, TAI2UTC, PARSE_STEREO_NAME, COR1_TOTBSERIES, DATATYPE,
;               SECCHI_PREP, SCC_GETBKGIMG, GET_STEREO_HPC_POINT, FITSHEAD2WCS,
;               WCS_GET_PIXEL
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 22-Nov-2011
;               Version 2, 15-Dec-2011, WTT, corrected handling of total
;                                            brightness images
;               Version 3, 18-Jun-2015, WTT, adjust for post-conjunction
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_browse_rdiff_cor1, h1, a1, diff, beacon=beacon
;
;  Set up the parameters.
;
tai1 = utc2tai(h1.date_obs)
sc = parse_stereo_name(h1.obsrvtry, ['A', 'B'])
tdelta = 150.d0                         ;2.5 minutes
thalf = tdelta / 2
delvarx, diff
;
;  Find the file(s) from 20 minutes earlier, +/- TDELTA/2.
;
t0 = tai1 - 1200.d0 - thalf
t0 = [t0, t0+tdelta]
utc0 = tai2utc(t0)
if h1.downlink eq 'SW' then ssr = 7 else ssr = 3
list = cor1_totbseries(utc0, sc, beacon=beacon, ssr=ssr, /quiet, /valid)
if datatype(list) ne 'STC' then return
if total(list.nmiss) gt 0 then return           ;Incomplete image(s)
file0 = list[*,0].filename
if file0[1] eq file0[0] then file0 = file0[0]
;
;  Read in the earlier file.
;
sz = size(a1)
secchi_prep, file0, h0, a0, /calimg_off, /calfac_off, /update_hdr_off, $
  /bkgimg_off, /silent, outsize=sz[1]
if total(h0.nmissing) gt 0 then return          ;Incomplete image(s)
;
;  Get the background image and subtract it.
;
daily = 0
if (sc eq 'ahead') and (h1.date_obs lt '2007-02-03') then daily = 1
interpolate = 1
if (sc eq 'behind') and (h1.date_obs lt '2007-02-17') then interpolate = 0
for i=0,n_elements(h0)-1 do begin
   bkg = scc_getbkgimg(h0[i], /silent, daily=daily, interpolate=interpolate, $
                       outhdr=bkghdr)
   if n_elements(bkg) le 1 then return          ;no background
   if h0[i].ccdsum ne bkghdr.ccdsum then return
   scl = 4^(h0[i].ipsum - bkghdr.ipsum)
   bkg = bkg * scl
   a0[*,*,i] = a0[*,*,i] - bkg
endfor
;
;  Form the difference image.
;
if n_elements(h0) eq 3 then a0 = total(a0, 3)
scl1 = 4^(h1.ipsum - 1)
if h1.polar lt 1000 then scl1 = scl1 * 3
scl0 = 4^(h0[0].ipsum-1) * n_elements(h0)
diff = a1/scl1 - a0/scl0
;
;  Rotate the image about Sun center.
;
point = get_stereo_hpc_point(h1.date_obs, sc, /post_conjunction)
wcs = fitshead2wcs(h1)
center = wcs_get_pixel(wcs, [0,0])
diff = rot(diff, -point[2], 1, center[0], center[1], /pivot, $
           missing=!values.f_nan)
;
;  Scale the image into a byte array.
;
range = 32
diff = bytscl(diff, min=-range, max=range)
;
return
end
