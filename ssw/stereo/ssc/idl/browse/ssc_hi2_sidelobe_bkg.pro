;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_HI2_SIDELOBE_BKG
;
; Purpose     :	Return HI2 background for sidelobe period
;
; Category    :	
;
; Explanation :	Gathers all the HI2 images from two days before and two days
;               after the observation date, and forms a background image out of
;               the minimum image.
;
; Syntax      :	BKG = SCC_HI2_SIDELOBE_BKG( HEADER )
;
; Examples    :	NEWIMAGE = IMAGE - SCC_HI2_SIDELOBE_BKG( HEADER)
;
; Inputs      :	HEADER  = Header structure of image to apply background to.
;
; Opt. Inputs :	None.
;
; Outputs     :	The result of the function is the background image.
;
; Opt. Outputs:	None.
;
; Keywords    :	BEACON  = Set to true if one is processing beacon data.
;
; Calls       :	PARSE_STEREO_NAME, STR2UTC, SCC_READ_SUMMARY, SCCFINDFITS,
;               SECCHI_PREP
;
; Common      :	SSC_HI2_SIDELOBE_BKG stores the previous background image.  If
;               the date range hasn't changed, then the previous value
;               is returned.
;
; Restrictions:	None.
;
; Side effects:	If no images are found, then the scalar zero is returned.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 25-Aug-2014, William Thompson, GSFC
;               Version 2, 22-Sep-2014, WTT, filter out bad background images
;
; Contact     :	WTHOMPSON
;-
;
function ssc_hi2_sidelobe_bkg, header, beacon=beacon
;
on_error, 2
common ssc_hi2_sidelobe_bkg, d1_a, d2_a, bkg_a, d1_b, d2_b, bkg_b
;
;  Get the spacecraft name.
;
sc = parse_stereo_name(header.obsrvtry, ['A','B'])
if (sc ne 'A') and (sc ne 'B') then message, 'Unrecognized observatory name'
;
;  From the observation date, search for files two days before and after.
;
utc = str2utc(header.date_obs)
utc.time = 0
utc1 = utc
utc1.mjd = utc.mjd - 2
utc2 = utc
utc2.mjd = utc.mjd + 3
;
list = scc_read_summary(date=[utc1,utc2], spacecraft=sc, telescope='hi2', $
                       beacon=beacon)
if n_elements(list) le 1 then return, 0
files = list.filename
for i=0,n_elements(files)-1 do begin
    errmsg = ''
    files[i] = sccfindfits(files[i], beacon=beacon, errmsg=errmsg)
endfor
;
;  Filter out images which were not found, or with the wrong file size.
;
w = where((files ne '') and (list.xsize eq header.naxis1) and $
          (list.ysize eq header.naxis2) and (list.nmiss eq 0), count)
if count gt 1 then begin
    list = list[w]
    files = files[w]
end else return, 0
;
;  Based on the first and last observation date, determine whether or not the
;  background image needs to be regenerated.
;
regen_bkg = 1
d1 = min(list.date_obs, max=d2)
case sc of
    'A': if n_elements(bkg_a) ne 0 then begin
        if (d1 eq d1_a) and (d2 eq d2_a) then begin
            bkg = bkg_a
            regen_bkg = 0
        endif
    endif
    'B': if n_elements(bkg_b) ne 0 then begin
        if (d1 eq d1_b) and (d2 eq d2_b) then begin
            bkg = bkg_b
            regen_bkg = 0
        endif
    endif
endcase
;
;  If needed, generate the background image
;
if regen_bkg then begin
    secchi_prep, files, hbkg, image, /desmear_off, /update_hdr_off, $
                 /calimg_off, /calfac_off, /silent
;
;  Filter out any images which are significantly different from the other
;  images in the series.
;
    aa = average(image, [1,2])
    j = indgen(n_elements(hbkg))
    s = 0*aa
    for i=0,n_elements(s)-1 do begin
        w = where(i ne j)
        ai = average(aa[w])
        si = stddev(aa[w])
        s[i] = abs(aa[i]-ai) / si
    endfor
    w = where(s lt 5, count)
    if count gt 3 then image = image[*,*,w]     ;Ignore if less than 3 images
;
;  Form the background image from the minimum of all the images.
;
    bkg = min(image, dim=3)
;
;  Store the background image and associated dates in the common block.
;
    case sc of
        'A': begin
            d1_a = d1
            d2_a = d2
            bkg_a = bkg
        end
        'B': begin
            d1_b = d1
            d2_b = d2
            bkg_b = bkg
        end
    endcase
endif
;
;  Return the background image to the calling routine.
;
return, bkg
end
