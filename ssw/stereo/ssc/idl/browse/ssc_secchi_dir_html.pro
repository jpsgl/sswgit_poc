;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_SECCHI_DIR_HTML
;
; Purpose     :	Create SECCHI browse directory web page
;
; Category    :	SECCHI, Quicklook
;
; Explanation :	Generates a web page listing the images and captions within a
;               browse directory.
;
; Syntax      :	SSC_SECCHI_DIR_HTML, PATH
;
; Examples    :	See SSC_BEACON_SECCHI
;
; Inputs      :	PATH    = The directory to process
;
; Opt. Inputs :	None.
;
; Outputs     :	None.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Env. Vars.  :	STEREO_BROWSE      = Top directory of browse pages
;
; Calls       :	GET_UTC, CONCAT_DIR, BREAK_FILE, CHECK_EXT_TIME
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 20-Feb-2007, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_secchi_dir_html, path
;
on_error, 2
;
stereo_browse = getenv('STEREO_BROWSE')
;
;  Get a list of all the JPEG files within the directory.
;
files = file_search(concat_dir(path, '*.jpg'))
break_file, files, disk, dir, names, ext
;
;  From the filenames, extract the image times.
;
times = strmid(names,9,2) + ':' + strmid(names,11,2) + ':' + strmid(names,13,2)
;
;  Create the web pages initially as temporary files.
;
page_names = ['index', 'thumbnail']
header_names = ['dir_header.txt', 'dir_header_thumb.txt']
for ipage = 0,n_elements(page_names)-1 do begin
    openw, output, concat_dir(path, + page_names[ipage] + '.temp'), /get_lun
;
;  Start by inserting the header.
;
    openr, header, concat_dir(stereo_browse, header_names[ipage]), /get_lun
    line = 'String'
    while not eof(header) do begin
        readf, header, line
        printf, output, line
    endwhile
    free_lun, header
;
;  Write out the table with the SECCHI images
;
    printf, output, '<TABLE CLASS="coords" ' + $
      'ALIGN=CENTER STYLE="text-align:center">'
;
    for ifile = 0,n_elements(names)-1 do begin
        printf, output, '<TR><TD STYLE="text-align:left">'
        printf, output, '<A HREF="' + names[ifile] + '.jpg">'
;
;  Depending on the page being generated, put in either the filename or the
;  thumbnail.
;
        entry = names[ifile] + '.jpg'
        if ipage eq 1 then entry = '<IMG SRC="../128/' + entry + '">'
        printf, output, entry + '</A></TD>'
;
        printf, output, '<TD WIDTH=20></TD>'
        printf, output, '<TD>' + times[ifile] + ' UT</TD>'
        printf, output, '<TD WIDTH=20></TD>'
        printf, output, '<TD STYLE="text-align:left"><A HREF="' + $
          names[ifile] + '.txt">[caption]</A></TD></TR>'
    endfor
;
    printf, output, '</TABLE><P>'
;
;  Write out the footer.
;
    openr, footer, concat_dir(stereo_browse, 'dir_footer.txt'), /get_lun
    line = 'String'
    while not eof(footer) do begin
        readf, footer, line
        printf, output, line
    endwhile
    free_lun, footer, output
endfor
;
;  Rename the temporary files to .shtml
;
for ipage = 0,n_elements(page_names)-1 do begin
    file_move, concat_dir(path, page_names[ipage] + '.temp'), $
      concat_dir(path, page_names[ipage] + '.shtml'), /overwrite
endfor
;
end
