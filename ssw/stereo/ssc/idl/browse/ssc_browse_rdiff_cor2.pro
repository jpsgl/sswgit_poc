;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_BROWSE_RDIFF_COR2
;
; Purpose     :	Creates COR2 running difference images
;
; Category    :	SECCHI, Quicklook
;
; Explanation : Called from SSC_BROWSE_SECCHI_JPEG to create a running
;               difference version of a COR2 browse image for the SSC website.
;               The relevant image from 1 hour earlier is read in and
;               subtracted from the supplied image.
;
; Syntax      :	SSC_BROWSE_RDIFF_COR2, HEADER1, IMAGE1, BKG, DIFF
;
; Examples    :	See SSC_BROWSE_SECCHI_JPEG
;
; Inputs      :	HEADER1 = Header of image to be subtracted from
;
;               IMAGE1  = Image to be subtracted from.
;
;               BKG     = Background image applied to IMAGE1.  The same
;                         background is applied to IMAGE0.
;
; Opt. Inputs :	None.
;
; Outputs     :	DIFF    = Byte-scaled difference image
;
; Opt. Outputs:	None.
;
; Keywords    :	BEACON  = Set to true if one is processing beacon images.
;
;               AS_BEACON = Process images as beacon, but from Level-0
;                           directories.  This is intended to support HGA side
;                           lobe operations.
;
; Calls       :	UTC2TAI, TAI2UTC, PARSE_STEREO_NAME, COR1_PBSERIES, DATATYPE,
;               SCC_READ_SUMMARY, SCCFINDFITS, SECCHI_PREP, SCC_GETBKGIMG,
;               GET_STEREO_HPC_POINT, FITSHEAD2WCS, WCS_GET_PIXEL
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 22-Nov-2011, William Thompson, GSFC
;               Version 2, 22-Dec-2011, WTT, change BKG to input parameter
;               Version 3, 23-Dec-2011, WTT, adjust double exposures
;                       Apply median smoothing to match input image
;               Version 4, 08-Aug-2014, WTT, Added keyword AS_BEACON
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_browse_rdiff_cor2, h1, a1, bkg, diff, beacon=beacon, as_beacon=as_beacon
;
;  Determine the state of the AS_BEACON keyword.
;
if n_elements(as_beacon) eq 0 then as_beacon = keyword_set(beacon)
;
;  Set up the parameters.
;
tai1 = utc2tai(h1.date_obs)
polar = h1.polar le 360
sc = parse_stereo_name(h1.obsrvtry, ['A', 'B'])
tdelta = 900.d0                         ;15 minutes
thalf = tdelta / 2
delvarx, diff
;
;  Find the file(s) from an hour earlier, +/- TDELTA/2.
;
t0 = tai1 - 3600.d0 - thalf
t0 = [t0, t0+tdelta]
utc0 = tai2utc(t0)
if polar then begin
    if h1.downlink eq 'SW' then ssr = 7 else ssr = 3
    list = cor1_pbseries(utc0, sc, /cor2, beacon=beacon, ssr=ssr, /quiet)
    if datatype(list) ne 'STC' then return
    if total(list.nmiss) gt 0 then return       ;Incomplete image(s)
    file0 = list[*,0].filename
end else begin
    list = scc_read_summary(date=utc0, spacecraft=sc, telescope='cor2', $
                            type='img', beacon=beacon)
    if datatype(list) ne 'STC' then return
    w = where(list.dest eq h1.downlink, count)
    if count gt 0 then list = list[w]
    if list[0].nmiss gt 0 then return           ;Incomplete image
    file0 = sccfindfits(list[0].filename, beacon=beacon)
endelse
;
;  Read in the earlier file.
;
sz = size(a1)
nowarp=(as_beacon or (h1.date_obs gt '2011-06-27'))
secchi_prep, file0, h0, a0, /calimg_off, /calfac_off, /update_hdr_off, $
  /silent, /smask, nowarp=nowarp, outsize=sz[1]
if total(h0.nmissing) gt 0 then return          ;Incomplete image(s)
dsatval = 4.^(h0[0].ipsum-1) * 2500
w = where((a0 eq 0) or (a0 gt dsatval), count)
if count gt 0 then flag_missing, a0, w
;
;  Correct double exposure images for the non-linearity effect.
;
if not polar then begin
    p0 = 1.04418
    p1 = -0.00645004
    scl = (p0 + p1*alog(h0.exptime)) + p1*alog(a0>1)
    a0 = a0 / (scl > 1)
endif
;
;  Form the difference image.
;
if keyword_set(polar) then begin
    h0 = h0[0]
    a0 = 2*average(a0,3)
endif
if h0.ipsum ne 0 then a0 = a0 / 4^(h0.ipsum-1)
if as_beacon then nmedian = 3 else nmedian = 5
diff = a1 - median(a0 / bkg, nmedian)
;
;  Rotate the image about Sun center.
;
point = get_stereo_hpc_point(h1.date_obs, sc)
wcs = fitshead2wcs(h1)
center = wcs_get_pixel(wcs, [0,0])
diff = rot(diff, -point[2], 1, center[0], center[1], /pivot, $
           missing=!values.f_nan)
;
;  Scale the image into a byte array.
;
if sc eq 'A' then range = 0.15 else range = 0.1125
diff = bytscl(diff, min=-range, max=range)
;
return
end
