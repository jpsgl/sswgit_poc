;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_MAKE_PLANET_BROWSE
;
; Purpose     :	Find planets in SECCHI daily browse images
;
; Category    :	SECCHI, Quicklook
;
; Explanation :	This routine adds labels to representative SECCHI coronagraph
;               images for each day to show where the planets are.
;
; Syntax      :	SSC_MAKE_PLANET_BROWSE, OBS_DATE
;
; Examples    :	See SSC_MAKE_BROWSE
;
; Inputs      :	OBS_DATE = Observation date
;
; Opt. Inputs :	None.
;
; Outputs     :	JPEG images are written to the browse/.../planets directory.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Env. Vars.  : STEREO_BROWSE      = Location of browse pages
;
; Calls       :	ANYTIM2UTC, CONCAT_DIR, SSC_OVERPLOT_PLANET
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	Based on SSC_MAKE_PLANET_FINDER
;
; History     :	Version 1, 21-Jun-2017, William Thompson, GSFC
;               Version 2, 22-Jun-2017, WTT, add navigation
;                                       Use QUALITY=100, take out red for HI2
;                                       Make a redirect page
;               Version 3, 28-Jun-2017, WTT, use browse_planets.shtml to
;                                       include jump to date capability
;               Version 4, 31-May-2018, WTT, use outline for HI2
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_make_planet_browse, obs_date
;
;  Get the environment variables
;
stereo_browse = getenv('STEREO_BROWSE')
;
;  Get the date, and derive the directory based on the date.
;
date = anytim2utc(obs_date, /ecs, /date_only)
stereo_browse_out = concat_dir(stereo_browse, date)
stereo_planet = concat_dir(stereo_browse_out, 'planets')
if not file_exist(stereo_planet) then file_mkdir, stereo_planet
;
;  Step through the spacecraft, and the telescopes.
;
sc_names = ['Behind', 'Ahead']
if date gt '2015/05/19' then begin
    sc_names = reverse(sc_names)
    hi_exp_file = 'hi_finder_exp_post.txt'
end else hi_exp_file = 'hi_finder_exp.txt'
sc = strlowcase(sc_names)
;
tel = ['cor1', 'cor2', 'hi1', 'hi2']
for isc = 0,n_elements(sc)-1 do begin
    sc_path = concat_dir(stereo_browse_out, sc[isc])
    for itel = 0,n_elements(tel)-1 do begin
        path = concat_dir(sc_path, tel[itel])
        path = concat_dir(path, '256')
;
;  Look for a JPEG file, and read it.
;
        jpeg_file = file_search(concat_dir(path, '*.jpg'), count=njpeg)
        if njpeg gt 0 then begin
            jpeg_file = jpeg_file[njpeg-1]
            beacon = strmid(jpeg_file, strlen(jpeg_file)-8, 1) eq '7'
            ssc_overplot_planet, jpeg_file, image, beacon=beacon, $
                                 outline=tel[itel] eq 'hi2'
;
;  Write out the image to the planets directory.
;
            out_name = sc[isc] + '_' + tel[itel] + '_planets.jpg'
            out_name = concat_dir(stereo_planet, out_name)
            sz = size(image)
            true_color = (where(sz[1:sz[0]] eq 3))[0] + 1
            write_jpeg, out_name, image, true=true_color, quality=100
;
        endif                   ;JPEG image found
    endfor                      ;itel
endfor                          ;isc
;
;  Write out the HTML page.
;
openw, unit, concat_dir(stereo_planet, 'index.shtml'), /get_lun
;
printf, unit, "<!--#set var='title' value='Science Center - Planet Finder' -->"
printf, unit, "<!--#include virtual='/incl2/top.shtml'-->"
printf, unit, '<meta http-equiv="Refresh" CONTENT="300">'
printf, unit, ''
printf, unit, '<!--DO NOT EDIT ABOVE THIS LINE-->'
printf, unit, ''
printf, unit, '<P><H2>STEREO Planet Finder</H2></P>'
printf, unit, ''
printf, unit, '<P>Shown here are the computed positions of various bright planets (out to'
;
printf, unit, '  Saturn) in SECCHI images for ' + date + '.  In some cases, the planets'
;
printf, unit, "  themselves will not show up in the images, either because they're blocked by"
printf, unit, "  an occulter, or because that part of the field of view is missing from the"
printf, unit, "  image.</P>"
printf, unit, ''
printf, unit, "<!--#include virtual='/browse/browse_planets.shtml'-->"
printf, unit, ''
;
;  Find the URLs for one day earlier and one day later.  Set to blank if
;  they don't exist.
;
daypath = strarr(3)
urlpath = strarr(3)
utc = anytim2utc(obs_date)
for iday = 0,2 do begin
    day = utc
    day.mjd = day.mjd + iday - 1
    day = anytim2utc(day, /ext)
    filepath = stereo_browse
    sdate = string(day.year,  format='(I4.4)') + '/' + $
            string(day.month, format='(I2.2)') + '/' + $
            string(day.day,   format='(I2.2)') + '/planets'
    filepath = concat_dir(filepath, sdate)
    daypath[iday] = filepath
    if file_exist(filepath) then urlpath[iday] = '../../../../' + sdate
endfor
;
;  Find the URLs for one week earlier and one week later.  Set to blank
;  if they don't exist.  Do the same for 27 days before and after (one
;  rotation).
;
urlstep = strarr(4)
stepval = [-27,-7,7,27]
steptext = ['&lt;rot','&lt;week','week&gt;','rot&gt;']
for istep=0,3 do begin
    day = utc
    day.mjd = utc.mjd + stepval[istep]
    day = anytim2utc(day, /ext)
    temppath = stereo_browse
    sdate = string(day.year,  format='(I4.4)') + '/' + $
            string(day.month, format='(I2.2)') + '/' + $
            string(day.day,   format='(I2.2)') + '/planets'
    temppath = concat_dir(temppath, sdate)
    if file_exist(temppath) then urlstep[istep] = '../../../../' + sdate
endfor    
;
;  Form the links to the previous and next week and rotation.
;
steppage = steptext
for istep=0,3 do if urlstep[istep] ne '' then steppage[istep] = $
  '<A HREF="' + urlstep[istep] + '/">' + $
  steptext[istep] + '</A>'
;
;  Form the links to the previous and next days.
;
filler1 = ''
for i=1,15 do filler1 = filler1 + "&nbsp;"
filler2 = ''
for i=1,25 do filler2 = filler2 + "&nbsp;"
spacer = ''
for i=1,10 do spacer = spacer + "&nbsp;"
;
if file_exist(concat_dir(daypath[0],'index.shtml')) then begin
    day = utc
    day.mjd = day.mjd - 1
    prev = '<A HREF="' + urlpath[0] + '/">[' + $
           anytim2utc(day,/vms,/date) + ']</A>'
end else begin
    prev = ''
    for i=1,14 do prev = prev + "&nbsp;"
endelse
;
if file_exist(concat_dir(daypath[2],'index.shtml')) then begin
    day = utc
    day.mjd = day.mjd + 1
    next = '<A HREF="' + urlpath[2] + '/">[' + $
           anytim2utc(day,/vms,/date) + ']</A>'
end else begin
    next = ''
    for i=1,10 do next = next + "&nbsp;"
endelse
;
printf, unit, $
        '<TABLE CLASS="coords" ALIGN=CENTER><TR>'
printf, unit, '<TD>' + spacer + '</TD><TD>' + filler1 + '</TD>'
printf, unit, '<TD>' + filler2 + '</TD><TD>' + filler2 + '</TD>'
printf, unit, '<TD>' + filler2 + '</TD><TD>' + filler1 + '</TD>'
printf, unit, '<TD>' + spacer + '</TD></TR><TR>'
printf, unit, '<TD STYLE="text-align:left">' + steppage[0] + '</TD>'
printf, unit, '<TD STYLE="text-align:left">' + steppage[1] + '</TD>'
printf, unit, '<TD STYLE="text-align:left">' + prev + '</TD>'
printf, unit, '<TH STYLE="font-size:large; text-align:center">' + $
        strtrim(anytim2utc(utc,/vms,/date),2) + '</TH>'
printf, unit, '<TD STYLE="text-align:right">' + next + '</TD>'
printf, unit, '<TD STYLE="text-align:right">' + steppage[2] + '</TD>'
printf, unit, '<TD STYLE="text-align:right">' + steppage[3] + $
        '</TD></TR>'
printf, unit, '</TABLE><P>'
printf, unit
;
printf, unit, ''
printf, unit, '<TABLE CLASS="coords" ALIGN=CENTER STYLE="text-align:center">'
;
printf, unit, '<TR>'
printf, unit, '<TH>STEREO ' + sc_names[0] + '</TH>'
printf, unit, '<TD WIDTH=20></TD>'
printf, unit, '<TH>STEREO ' + sc_names[1] + '</TH>'
printf, unit, '</TR>'
;
filename = strarr(2)
for itel = 0,n_elements(tel)-1 do begin
    for isc = 0,1 do begin
        out_name = sc[isc] + '_' + tel[itel] + '_planets.jpg'
        out_name = concat_dir(stereo_planet, out_name)
        if file_exist(out_name) then begin
            break_file, out_name, disk, dir, name, ext
            filename[isc] = name + ext
        end else filename[isc] = '/browse/nodata_256.jpg'
        alt = strupcase(tel[itel]) + '-' + strmid(sc_names,0,1)
    endfor
;
    printf, unit, '<TR>'
    printf, unit, '<TD STYLE="text-align:center">'
    printf, unit, '<IMG SRC="' + filename[0] + '" ALT="' + alt[0] + '">'
    printf, unit, '</TD>'
    printf, unit, '<TD></TD>'
    printf, unit, '<TD STYLE="text-align:center">'
    printf, unit, '<IMG SRC="' + filename[1] + '" ALT="' + alt[1] + '">'
    printf, unit, '</TD>'
    printf, unit, '</TR>'
    if itel eq (n_elements(tel)-3) then begin
        printf, unit, '</TABLE><P>'
        openr, hi_exp, concat_dir(stereo_browse,hi_exp_file), /get_lun
        line = 'String'
        while not eof(hi_exp) do begin
            readf, hi_exp, line
            printf, unit, line
        endwhile
        free_lun, hi_exp
        sc_names = reverse(sc_names)
        sc = reverse(sc)
        printf, unit, '<TABLE CLASS="coords" ALIGN=CENTER STYLE="text-align:center">'
        printf, unit, '<TR>'
        printf, unit, '<TH>STEREO ' + sc_names[0] + '</TH>'
        printf, unit, '<TD WIDTH=20></TD>'
        printf, unit, '<TH>STEREO ' + sc_names[1] + '</TH>'
        printf, unit, '</TR>'
    endif
endfor
;
;  Repeat the navigation parameters at the bottom.
;
printf, unit, $
        '<TABLE CLASS="coords" ALIGN=CENTER><TR>'
printf, unit, '<TD>' + spacer + '</TD><TD>' + filler1 + '</TD>'
printf, unit, '<TD>' + filler2 + '</TD><TD>' + filler2 + '</TD>'
printf, unit, '<TD>' + filler2 + '</TD><TD>' + filler1 + '</TD>'
printf, unit, '<TD>' + spacer + '</TD></TR><TR>'
printf, unit, '<TD STYLE="text-align:left">' + steppage[0] + '</TD>'
printf, unit, '<TD STYLE="text-align:left">' + steppage[1] + '</TD>'
printf, unit, '<TD STYLE="text-align:left">' + prev + '</TD>'
printf, unit, '<TH STYLE="font-size:large; text-align:center">' + $
        strtrim(anytim2utc(utc,/vms,/date),2) + '</TH>'
printf, unit, '<TD STYLE="text-align:right">' + next + '</TD>'
printf, unit, '<TD STYLE="text-align:right">' + steppage[2] + '</TD>'
printf, unit, '<TD STYLE="text-align:right">' + steppage[3] + $
        '</TD></TR>'
printf, unit, '</TABLE><P>'
printf, unit
;
printf, unit, '</TABLE><P>'
printf, unit, ''
printf, unit, '<!--DO NOT EDIT BELOW THIS LINE-->'
printf, unit, ''
printf, unit, "<!--#include virtual='/incl2/footer.shtml'-->"
;
free_lun, unit      
;
;  If today was processed, then create redirect pages.
;
get_utc, today, /ecs, /date_only
if date eq today then begin
    outname = 'planets.shtml
    day = anytim2utc(utc, /ext)
    sdate = string(day.year,  format='(I4.4)') + '/' + $
            string(day.month, format='(I2.2)') + '/' + $
            string(day.day,   format='(I2.2)') + '/planets/'
    openw, output, concat_dir(stereo_browse, outname), /get_lun
    printf, output, '<META HTTP-EQUIV=REFRESH CONTENT="0; URL=' + sdate + '">
;
    openr, header, concat_dir(stereo_browse,'header.txt'), /get_lun
    line = 'String'
    while not eof(header) do begin
        readf, header, line
        printf, output, line
    endwhile
    free_lun, header
;
    printf, output, "<P>If you're not automatically redirected,"
    printf, output, 'please follow this <A HREF="' + sdate + '">link</A>'
    printf, output, 'to see the planet identification pages.</P>'
;
    openr, footer, concat_dir(stereo_browse,'footer.txt'), /get_lun
    line = 'String'
    while not eof(footer) do begin
        readf, footer, line
        printf, output, line
    endwhile
    free_lun, footer, output
endif
;
end
