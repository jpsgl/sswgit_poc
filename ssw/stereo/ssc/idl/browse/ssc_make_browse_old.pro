;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_MAKE_BROWSE_OLD
;
; Purpose     :	Recreate beacon images in "beacon" subdirectory
;
; Category    :	STEREO, Quicklook
;
; Explanation :	The browse page generation software was modified to move
;               obsolete beacon images into an "beacon" subdirectory, rather
;               than simply deleting them as before.  This routine recreates
;               the beacon images that were previously deleted.
;
; Syntax      :	SSC_MAKE_BROWSE_OLD  [, DATE]
;
; Examples    :	SSC_MAKE_BROWSE_OLD, '2011-02-25'
;
; Inputs      :	DATE    = The date to process.
;
; Opt. Inputs :	None.
;
; Outputs     :	Generates images and text files.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Env. Vars.  :	STEREO_BROWSE   = Top browse directory
;               UPDATE_BLDG21_BROWSE = Script to update building 21 copy
;
; Calls       :	ANYTIM2UTC, CONCAT_DIR, SETFLAG, SCC_READ_SUMMARY,
;               SCC_DATA_PATH, BREAK_FILE, SSC_BROWSE_SECCHI_JPEG
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	The graphics device is set to the Z buffer
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 28-Feb-2011, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_make_browse_old, date
;
stereo_browse = getenv('STEREO_BROWSE')
utc = anytim2utc(date)
;
sc = ['behind','ahead']
tel = ['euvi','cor1','cor2','hi1','hi2']
ntel = n_elements(tel)
wave = [195,171,284,304]
res = [2048,1024,512,256,128]   ;JPEG file resolutions
mres = ['512','256']            ;MPEG file resolutions
;
;  Next, create full-size and thumbnail images for each of the SECCHI
;  instruments.
;
loadct, 0
setflag, missing=0
;
;  Read the summary files.
;
nsw_tot = 0
for isc = 0,1 do begin
    cat = scc_read_summary(date=utc, spacecraft=sc[isc], source='lz', /check, /beacon)
    if datatype(cat,1) ne 'Structure' then cat = $
      {filename: '', date_obs: '', telescope: '', exptime: 0.0, xsize: 0, ysize: 0, $
       filter: '', value: 0.0, svalue: '', prog: '', osnum: 0, dest: '', fps: '', $
       led: '', compr: '', nmiss: -1, spacecraft: '', type: ''}
    for itel = 0,ntel-1 do begin
        telescope = tel[itel]
        if telescope eq 'euvi' then nwave=n_elements(wave) else nwave=1
;
;  Step through the wavelengths.  Filter out calibration and subfield images.
;
        for iwave = 0,nwave-1 do begin
            subtest = abs(alog(float(cat.xsize)/cat.ysize))
            w = where((cat.telescope eq strupcase(telescope)) and $
                      (cat.type ne 'cal') and (subtest lt 0.1), count)
;
;  For EUVI, find all images with the right wavelength.
;
            if telescope eq 'euvi' then begin
                if count gt 0 then value = cat[w].value else value = -999
                ww = where((value ge wave[iwave]-5) and $
                            (value le wave[iwave]+5), ccount)
                if ccount gt 0 then begin
                    w = w[ww]
                    count = ccount
                endif
            endif
;
;  For COR1, find all images with a polarization angle of 240.
;
            if telescope eq 'cor1' then begin
                if count gt 0 then value = cat[w].value else value = -999
                ww = where(value eq 240, ccount)
                if ccount gt 0 then begin
                    w = w[ww]
                    count = ccount
                endif
            endif
;
;  For COR2, find all images with a polarization angle of 240, or which are
;  double exposures.
;
            if telescope eq 'cor2' then begin
                if count gt 0 then begin
                    prog  = strupcase(cat[w].prog)
                    value = cat[w].value
                end else begin
                    prog  = 'none'
                    value = -999
                endelse
                ww = where((value eq 240) or (prog eq 'DOUB'), ccount)
                if ccount gt 0 then begin
                    w = w[ww]
                    count = ccount
                endif
            endif
;
;  Step through the files.
;
            for ifile = 0,count-1 do begin
                cat0 = cat[w[ifile]]
                file = scc_data_path(cat0.spacecraft, source='lz', $
                                     type=cat0.type, tel=cat0.telescope, $
                                     date=cat0.date_obs, /beacon)
                file = concat_dir(file, cat0.filename)
                break_file, file, disk, dir, name
                if is_fits(file) then $
                  ssc_browse_secchi_jpeg, file, name, sc[isc], stereo_browse, $
                    /beacon, /olddir
            endfor
        endfor                  ;iwave
    endfor                      ;itel
endfor                          ;isc
;
;  If UPDATE_BLDG21_BROWSE is defined, then call the script to copy over the
;  date tree.
;
update_bldg21_browse = getenv('UPDATE_BLDG21_BROWSE')
if update_bldg21_browse ne '' then begin
    sdate = anytim2utc(date, /ext)
    sdate = string(sdate.year, format='(I4.4)') + ' ' + $
      string(sdate.month, format='(I2.2)') + ' ' + $
      string(sdate.day, format='(I2.2)')
    command = update_bldg21_browse + ' ' + sdate
    print, 'Spawning: ' + command
    spawn, command
endif
;
end
