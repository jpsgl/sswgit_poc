;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_SECCHI_CAPTION
;
; Purpose     :	Generate automatic SECCHI image caption
;
; Category    :	SECCHI, Quicklook
;
; Explanation :	Used to create an automatic caption for a SECCHI JPEG image.
;
; Syntax      :	SSC_SECCHI_CAPTION
;
; Examples    :	See SSC_BEACON_SECCHI
;
; Inputs      :	FILENAME = Name of JPEG file.  The caption will be written to a
;                          file with the same name, and ending in ".txt".
;
;               HEADER   = SECCHI image header
;
; Opt. Inputs :	None.
;
; Outputs     :	The caption is written to a text file.
;
; Opt. Outputs:	None.
;
; Keywords    :	BEACON   = If set, then a beacon disclaimer is appended to the
;                          caption.
;
; Env. Vars.  :	None.
;
; Calls       :	ANYTIM2UTC, BOOST_ARRAY, NTRIM, BREAK_FILE
;
; Common      :	None.
;
; Restrictions:	Currently, this procedure is optimized for EUVI images.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 8-Feb-2007, William Thompson, GSFC
;               Version 2, 10-Aug-2007, WTT, fixed HI bug
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_secchi_caption, filename, header, beacon=beacon
;
on_error, 2
;
month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', $
         'August', 'September', 'October', 'November', 'December']
date_obs = anytim2utc(header.date_obs, /external)
;
detector = strlowcase(strtrim(header.detector,2))
wavelnth = header.wavelnth
if wavelnth eq 175 then wavelnth = 171
if header.obsrvtry eq 'STEREO_A' then sc = 'Ahead' else sc = 'Behind'
;
type = 'Image of the solar corona'
case detector of
    'euvi': begin
        type = 'Image of the Sun'
        tel = 'Extreme Ultraviolet Imager (EUVI)'
        case wavelnth of
            304: begin
                ion = 'He II singly ionized state'
                atom = 'helium'
                temp = '80 thousand'
            end
            171: begin
                ion = 'Fe IX and Fe X ionization states'
                atom = 'iron'
                temp = '1.0 million'
            end
            195: begin
                ion = 'Fe XII ionization state'
                atom = 'iron'
                temp = '1.4 million'
            end
            284: begin
                ion = 'Fe XV ionization state'
                atom = 'iron'
                temp = '2.2 million'
            end
        endcase
    end
;
    'cor1': tel = 'inner coronagraph (COR1)'
    'cor2': tel = 'outer coronagraph (COR1)'
    'hi1' : tel = 'inner Heliospheric Imager (HI-1)'
    'hi2' : tel = 'outer Heliospheric Imager (HI-2)'
endcase
;
text = type + ', taken by the SECCHI ' + tel
boost_array, text, 'on the STEREO ' + sc + ' observatory on ' + $
  month[date_obs.month-1] + ' ' + ntrim(date_obs.day) + ', ' + $
  ntrim(date_obs.year) + ' at ' + utc2str(date_obs,/time_only,/truncate) + $
  ' UT.'
;
if detector eq 'euvi' then begin
    boost_array, text, 'The ' + ntrim(wavelnth) + $
      ' Angstrom bandpass is sensitive to the ' + ion
    boost_array, text, 'of ' + atom + $
      ', at a characteristic temperature of about ' + temp + ' degrees Kelvin.'
endif
;
if keyword_set(beacon) then begin
    boost_array, text, 'This image was produced from the STEREO space ' + $
      'weather beacon telemetry.'
    boost_array, text, 'Because of the high amount of compression used ' + $
      'for the space weather beacon,'
    boost_array, text, 'the image quality is far lower than in the final ' + $
      'science product.'
endif
;
break_file, filename, disk, dir, name, ext
openw, output, disk + dir + name + '.txt', /get_lun
for i=0,n_elements(text)-1 do printf, output, text[i]
free_lun, output
;
end
