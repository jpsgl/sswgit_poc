;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_SIDELOBE_PERIOD()
;
; Purpose     :	Returns whether or not spacecraft in side-lobe mode
;
; Category    :	STEREO, Quicklook
;
; Explanation :	Based on the spacecraft name and date, returns whether or not
;               the spacecraft high gain antenna is using one of the side
;               lobes.
;
; Syntax      :	Result = SSC_SIDELOBE_PERIOD( SPACECRAFT, DATE )
;
; Examples    :	See SSC_MAKE_BROWSE and SSC_BROWSE_SECCHI_JPEG
;
; Inputs      :	SPACECRAFT = The name of the STEREO spacecraft
;
;               DATE = The observation date
;
; Opt. Inputs :	None.
;
; Outputs     :	The result of the function is true if the spacecraft is in
;               side-lobe mode, or false otherwise.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Calls       :	PARSE_STEREO_NAME, ANYTIM2UTC
;
; Common      :	None.
;
; Restrictions:	As the mission progresses, this routine needs to be updated
;               with the dates of the transitions in and out of side-lobe mode.
;
;               Test periods are not included.
;
; Side effects:	If the input values are bad, the routine returns false.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 22-Aug-2014, William Thompson, GSFC
;               Version 2, 24-Sep-2015, WTT, include end date
;
; Contact     :	WTHOMPSON
;-
;
function ssc_sidelobe_period, spacecraft, date
;
if (n_elements(spacecraft) ne 1) or (n_elements(date) ne 1) then return, 0b
sc = parse_stereo_name(spacecraft, ['A','B'])
if (sc ne 'A') and (sc ne 'B') then return, 0b
errmsg = ''
utc = anytim2utc(date, errmsg=errmsg)
if errmsg ne '' then return, 0b
;
;  Side lobe operations on Ahead started 19-Aug-2014, and will end on
;  16-Nov-2015.
;
return, (sc eq 'A') and (utc.mjd ge 56888) and (utc.mjd le 57342)
end
