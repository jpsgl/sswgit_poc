;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_RESTORE_SECCHI_BEACON
;
; Purpose     :	Restore previously deleted SECCHI beacon images
;
; Category    :	STEREO, Quicklook
;
; Explanation :	This routine moves beacon browse images from the "beacon"
;               subdirectory back into the main directory, to mimic the
;               behavior of the routine SSC_PURGE_SECCHI_BEACON.
;
; Syntax      :	SSC_RESTORE_SECCHI_BEACON, DATE
;
; Examples    :	SSC_RESTORE_SECCHI_BEACON, '2010-08-01'
;
; Inputs      :	DATE    = The date to process
;
; Opt. Inputs :	None.
;
; Outputs     :	None.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Env. Vars.  :	STEREO_BROWSE   = Top browse directory
;
;               Other environment variables used by SSC_MAKE_BROWSE
;
; Calls       :	ANYTIM2UTC, CONCAT_DIR, BREAK_FILE, ANYTIM2TAI, SSC_MAKE_BROWSE
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	When done, calls SSC_MAKE_BROWSE to make the browse pages.
;               This may regenerate images from the FITS files.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 4-Mar-2011, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_restore_secchi_beacon, date
;
;  Define the path to the browse directory for the specified date.
;
stereo_browse = getenv('STEREO_BROWSE')
utc = anytim2utc(date, /ecs, /date_only)
datepath = concat_dir(stereo_browse, utc)
;
;  Set up the loop parameters.
;
sc = ['behind','ahead']
tel = ['euvi','cor2','hi1','hi2']       ;Don't restore COR1
margin = [30, 30, 800, 1500]    ;Margin in seconds
wave = [195,171,284,304]
res = [2048,1024,512,256,128]   ;JPEG file resolutions
;
;  Step through the spacecraft.
;
for isc = 0,1 do begin
    scpath = concat_dir(datepath, sc[isc])
;
;  Step through the telescopes.
;
    for itel = 0,n_elements(tel)-1 do begin
        detector = tel[itel]
        telpath = concat_dir(scpath, detector)
;
;  For EUVI, step through the wavelengths.
;
        if itel eq 0 then nwave = 3 else nwave = 0
        for iwave = 0,nwave do begin
            if nwave eq 0 then wavepath = telpath else $
              wavepath = concat_dir(telpath, ntrim(wave[iwave]))
            nres = 0
;
;  Step through the resolutions.
;
            ires0 = 0
            if (detector eq 'cor1') or (detector eq 'hi1') or $
              (detector eq 'hi2') then ires0 = 1
            for ires = ires0,n_elements(res)-1 do begin
                path = concat_dir(wavepath, ntrim(res[ires]))
;
;  Find the JPEG files in both the regular and beacon directories.
;
                files = file_search(path, '*.jpg', count=count)
                if count gt 0 then begin
;
;  Sort on the filename, i.e. by time.
;
                    break_file, files, disk, dir, name
                    s = sort(name)
                    disk = disk[s]
                    dir  = dir[s]
                    name = name[s]
;
;  Determine which files are beacon images, and extract the TAI times.  Form
;  the names of the files to be potentially moved.
;
                    swx = strmid(name, 17, 1) eq '7'
                    tai = anytim2tai(strmid(name, 0, 15))
                    names = disk + dir + name + '.*'
;
;  The date/times embedded in the beacon filenames aren't always the same as
;  for the level-0 files.  Filter out any files which are within a
;  telescope-specific margin of a level-0 file.
;
                    keep = 1b - swx
                    ws = where(swx, complement=w1, count)
                    for i=0,count-1 do begin
                        delta = min(abs(tai[ws[i]] - tai[w1]))
                        if delta gt margin[itel] then keep[ws[i]] = 1b
                    endfor
;
;  If any beacon files were left over, then continue.  Filter out the rejected
;  files.
;
                    w = where(keep, count, ncomplement=nswx)
                    if nswx gt 0 then begin
                        names = names[w]
                        tai = tai[w]
                        swx = swx[w]
;
;  Step through the files, and look for continous sequences of beacon images.
;
                        i1 = 0
                        n2 = count - 1
                        for i2 = 0,n2 do begin
                            if swx[i2] then begin
;
;  Make sure I1 points to a beacon image.
;
                                if ~swx[i1] then i1 = i2
;
;  Check to see if this is the end of the sequence.
;
                                if (i2 eq n2) or ~swx[(i2+1)<n2] then begin
;
;  If the sequence has at least five images, and covers a time range of at
;  least three hours, then move the files into the regular directory.
;
                                    n = i2 - i1 + 1
                                    delta = tai[i2]-tai[i1]
                                    if (n ge 5) and (delta ge 10800) then begin
                                       file_move, names[i1:i2], path, $
                                                  /allow_same
                                       nres = nres + n
                                    endif
                                endif
;
;  If not a beacon image, then start looking for a new sequence.
;
                            end else i1 = i2
;
                        endfor  ;i2
                    endif       ;beacon files remain
                endif           ;JPEG files found
            endfor              ;ires
;
;  If any images were restored, then print a message.
;
            if nres gt 0 then begin
                text = 'Restored files for ' + strupcase(tel[itel]) + '-' + $
                       strupcase(strmid(sc[isc],0,1))
                if nwave gt 0 then text = text + ' ' + ntrim(wave[iwave])
                print, text
            endif
;
        endfor                  ;iwave
    endfor                      ;itel
endfor                          ;isc
;
;  Regenerate the browse pages.
;
ssc_make_browse, date
;
end
