;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_PURGE_SECCHI_BEACON
;
; Purpose     :	Purge SECCHI beacon files from browse directories
;
; Category    :	STEREO, Quicklook
;
; Explanation :	Called from SSC_MAKE_BROWSE to remove extraneous beacon images
;               from the browse tree after a sufficient number of non-beacon
;               images have been created.  Files are allowed to remain if they
;               fall within an uninterrupted sequence of beacon images of at
;               least five, and covering a time range of at least three hours.
;               This allows beacon images to cover major gaps in coverage.
;
; Syntax      :	SSC_PURGE_SECCHI_BEACON, JPEG_PATH, RES, NAME, SW, SAVED
;
; Examples    :	See SSC_MAKE_BROWSE
;
; Inputs      :	JPEG_PATH = Path to JPEG files (without resolution)
;
;               RES     = Array of resolution values
;
;               NAME    = Names of JPEG files (without path or extension)
;
;               SW      = Indices of space weather files
;
; Opt. Inputs :	None.
;
; Outputs     :	SAVED   = Number of files not purged.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Calls       :	ANYTIM2TAI, NTRIM, CONCAT_DIR, SSC_REMOVE_SECCHI_BEACON
;
; Common      :	None.
;
; Restrictions:	Should only be called from within SSC_MAKE_BROWSE
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 02-Mar-2011, William Thompson, GSFC
;               Version 2, 03-Mar-2011, WTT, fixed bug setting I1
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_purge_secchi_beacon, jpeg_path, res, name, sw, saved
;
;  Form an array marking which files are space weather.
;  
n2 = n_elements(name) - 1
swx = bytarr(n2+1)
swx[sw] = 1
;
;  Make sure the filenames are sorted by time, and extract the times.
;
s = sort(name)
name = temporary(name[s])
swx = temporary(swx[s])
tai = anytim2tai(strmid(name, 0, 15))
;
;  Form strings out of the resolution values.
;
nres = n_elements(res) - 1
sres = ntrim(res)
;
;  Step through each file, and find continuous series of beacon files.
;
saved = 0
i1 = 0
for i2 = 0,n2 do begin
    if swx[i2] then begin
        if ~swx[i1] then i1 = i2
        if (i2 eq n2) or ~swx[(i2+1)<n2] then begin
            n = i2 - i1 + 1
            delta = tai[i2]-tai[i1]
;
;  If the series of beacon files has less than five members, or the time span
;  covered is less than 3 hours, then delete the files.
;
            if (n lt 5) or (delta lt 10800) then begin
                for ires = 0,nres do begin
                    temp_path = concat_dir(jpeg_path, sres[ires])
                    swx_files = concat_dir(temp_path, name[i1:i2] + '.*')
                    swx_files = file_search(swx_files, count=count)
                    if count gt 0 then ssc_remove_secchi_beacon, swx_files
                endfor
            end else saved = saved + (i2-i1+1)
        endif
    end else i1 = i2
endfor
;
end
