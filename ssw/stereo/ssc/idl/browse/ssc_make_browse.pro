;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_MAKE_BROWSE
;
; Purpose     :	Create daily browse pages.
;
; Category    :	STEREO, Quicklook
;
; Explanation :	Generate daily browse pages, including SECCHI images and daily
;               browse plots.
;
; Syntax      :	SSC_MAKE_BROWSE  [, DATE]  [, /NODATA]
;
; Examples    :	SSC_MAKE_BROWSE         ;Makes browse page for today
;
; Inputs      :	None required
;
; Opt. Inputs :	DATE    = The date to process.  Defaults to today.
;
; Outputs     :	Generates images and web pages.
;
; Opt. Outputs:	None.
;
; Keywords    :	NODATA  = Used recursively to create default pages for previous
;                         and next days if they don't exist yet.
;
;               NOSECCHI= Don't process SECCHI images.
;
;               MPEG_GEN= Generate MPEG files even if /NOSECCHI is set.
;
;               REPLACE = Replace existing JPEG files
;
;               COR_REPLACE = Replace coronagraph JPEG files (including HI)
;
;               PROVISIONAL = If set, then skip if HI2-A files have already
;                         been processed within the last day.
;
;               ADD_SYNOPTIC = If set, then generate the heliographic maps,
;                         even if /NOSECCHI is set.
;
;               SYNOP_REPLACE = Replace synoptic maps.  These will also be
;                         replaced if either /REPLACE or /COR_REPLACE is set.
;
; Env. Vars.  :	STEREO_BROWSE   = Top browse directory
;               SOHO_JPEG       = Location of SOHO images
;               SOHO_GIF        = Location of alternate GIF images
;               SWAVES_DATA     = Location of SWAVES data
;               UPDATE_BLDG21_BROWSE = Script to update building 21 copy
;
; Calls       :	GET_UTC, ANYTIM2UTC, CONCAT_DIR, FILE_EXIST, MK_DIR, UTC2STR,
;               SSC_BEACON_PLOTS, SETFLAG, SCC_READ_SUMMARY, SCC_DATA_PATH,
;               BREAK_FILE, SCC_BROWSE_SECCHI_JPEG, NTRIM, SSC_SECCHI_DIR_HTML,
;               SCC_BROWSE_SECCHI_MPEG, SSC_REMOVE_SECCHI_BEACON,
;               SSC_PURGE_SECCHI_BEACON, SSC_MAKE_EUVI_BROWSE_SYNOPTIC,
;               SSC_SIDELOBE_PERIOD
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	The graphics device is set to the Z buffer
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 22-Mar-2007, William Thompson, GSFC
;               Version 2, 29-Mar-2007, William Thompson, GSFC
;                       Signal when space weather data is used
;                       Correct counting bug
;                       Add keyword replace
;               Version 3, 12-Apr-2007, WTT, bug fixes
;               Version 4, 17-Apr-2007, WTT, Fix bug not deleting *_n7*.txt
;               Version 5, 18-Apr-2007, WTT, Include SOHO images
;               Version 6, 20-Apr-2007, WTT, Include SWAVES .png files
;               Version 7, 29-Jun-2007, WTT, Omit rows without EUVI images
;               Version 8, 09-Jul-2007, WTT, Include COR1
;               Version 9, 12-Jul-2007, WTT, Added keyword COR_REPLACE
;               Version 10, 13-Jul-2007, WTT, Added MkIV coronagraph
;               Version 11, 18-Jul-2007, WTT, Create 3 & 7-day insitu pages
;                       Added keyword NOSECCHI
;               Version 12, 01-Aug-2007, WTT, Added COR2 support
;               Version 13, 10-Aug-2007, WTT, Added HI1 support
;               Version 14, 04-Sep-2007, WTT, Fix 7-day version (was 5)
;               Version 15, 06-Sep-2007, WTT, call SSC_BROWSE_SECCHI_MPEG
;               Version 16, 26-Sep-2007, WTT, use _EXTRA for some keywords
;               Version 17, 24-Oct-2007, WTT, failover to latest Mk4 image
;               Version 18, 18-Dec-2007, WTT, Add HI2 support
;               Version 19, 19-Dec-2007, WTT, Only use latest Mk4 if < 1 day
;               Version 20, 04-Mar-2008, WTT, Use scc_read_summary(/check)
;               Version 21, 16-Sep-2008, WTT, Different disclaimer for insitu
;               Version 22, 18-Dec-2008, WTT, Add keyword /PROVISIONAL
;               Version 23, 11-Aug-2009, WTT, Delete mk4 file if zero length
;               Version 24, 05-Nov-2009, WTT, Add week, rotation links
;               Version 25, 04-Mar-2010, WTT, Spawn update_bldg21_browse
;               Version 26, 09-Mar-2010, WTT, Change spawn format
;               Version 27, 05-Apr-2010, WTT, Separate image/insitu redirects
;               Version 28, 24-Jun-2010, WTT, Include SDO images
;               Version 29, 07-Dec-2010, WTT, Refresh SDO images.
;               Version 30, 06-Jan-2011, WTT, point to sdo.gsfc
;               Version 31, 28-Feb-2011, WTT, Call SSC_REMOVE_SECCHI_BEACON
;               Version 32, 02-Mar-2011, WTT, Call SSC_PURGE_SECCHI_BEACON
;               Version 33, 07-Jul-2011, WTT, Improved PROVISIONAL test
;               Version 34, 18-Jul-2011, WTT, Handle COR1 total-B images
;               Version 35, 12-Dec-2011, WTT, add running difference links
;               Version 36, 09-Feb-2011, WTT, Check file size for SOHO etc. images
;               Version 37, 27-Feb-2011, WTT, Fix SDO bug introduced in version 36
;               Version 38, 16-Apr-2011, WTT, Call wget with "-t 2" to avoid delays
;               Version 39, 29-Jun-2012, WTT, Incorporated daily heliographic maps
;               Version 40, 27-Nov-2012, WTT, For COR1, process both (0,120,240)
;                                             and (60,180,300)
;               Version 41, 12-Nov-2012, WTT, Process any COR1 sequence within
;                                             60 seconds
;               Version 42, 29-Apr-2014, WTT, Look for KCOR instead of Mk4.
;               Version 43, 12-Aug-2014, WTT, Support side-lobe operations
;               Version 44, 22-Aug-2014, WTT, Call SSC_SIDELOBE_PERIOD
;               Version 45, 30-Sep-2014, WTT, Add /SYNOP_REPLACE
;               Version 46, 19-Nov-2014, WTT, Look for WIND/STEREO summary files.
;               Version 47, 20-Nov-2014, WTT, Update WIND/STEREO summary files w/new
;               Version 48, 20-Mar-2015, WTT, Add logic for solar solar conjunction
;               Version 49, 23-Mar-2015, WTT, Back out some of the changes made
;                                             in version 48.
;               Version 50, 01-Jul-2015, WTT, include date for post-solar conjunction
;               Version 51, 07-Jul-2015, WTT, reverse order post-conjunction
;                                             Use correct post-conj date.
;               Version 52, 16-Jul-2015, WTT, Corrected COR1 bug
;               Version 53, 17-Apr-2017, WTT, Use https where appropriate
;               Version 54, 21-Jun-2017, WTT, Call SSC_MAKE_PLANET_BROWSE
;               Version 55, 03-Oct-2017, WTT, Put in short wait for spawn
;               Version 56, 04-Dec-2017, WTT, include COR1 pB
;               Version 57, 01-Jun-2018, WTT, use outlined labels for HI2
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_make_browse, date, nodata=nodata, nosecchi=nosecchi, $
                     mpeg_gen=mpeg_gen, noprocessing=noprocessing, $
                     provisional=provisional, add_synoptic=add_synoptic, $
                     synop_replace=synop_replace, _extra=_extra
;
stereo_browse = getenv('STEREO_BROWSE')
soho_gif      = getenv('SOHO_GIF')
soho_jpeg     = getenv('SOHO_JPEG')
swaves_data   = getenv('SWAVES_DATA')
if n_params() eq 0 then get_utc, utc else utc = anytim2utc(date)
;
;  If the date is between 21-Mar-2015 and 10-Jul-2015, then apply special
;  processing for the solar conjunction period.
;
solar_conj = (utc.mjd gt 57101) and (utc.mjd lt 57213)
if solar_conj then nosecchi = 1
;
if utc.mjd ge 57161 then scn = ['Ahead','Behind'] else scn = ['Behind','Ahead']
sc = strlowcase(scn)
tel = ['euvi','cor1','cor2','hi1','hi2']
ntel = n_elements(tel)
wave = [195,171,284,304]
res = [2048,1024,512,256,128]   ;JPEG file resolutions
mres = ['512','256']            ;MPEG file resolutions
;
sdo_wave = ['193','171','211','304']
sdo = 't0' + sdo_wave + '.jpg'
;
;  First make sure that the directory exists, as well as that for the previous
;  and next days.  Put a default page in each new directory.  Store the file
;  paths and URLs into daypath and urlpath for later use.
;
daypath = strarr(3)
urlpath = strarr(3)
for iday = 0,2 do begin
    day = utc
    day.mjd = day.mjd + iday - 1
    day = anytim2utc(day, /ext)
    filepath = stereo_browse
    sdate = string(day.year,  format='(I4.4)') + '/' + $
            string(day.month, format='(I2.2)') + '/' + $
            string(day.day,   format='(I2.2)')
    filepath = concat_dir(filepath, sdate)
    if (not file_exist(filepath)) and (not keyword_set(nodata)) then $
      mk_dir, filepath
    daypath[iday] = filepath
    urlpath[iday] = '../../../' + sdate
endfor
filepath = daypath[1]
;
;  Find the URLs for one week earlier, and one week later.  Set to blank if
;  they don't exist.  Do the same for 27 days before and after (one rotation).
;
urlstep = strarr(4)
stepval = [-27,-7,7,27]
steptext = ['&lt;rot','&lt;week','week&gt;','rot&gt;']
for istep=0,3 do begin
    day = utc
    day.mjd = utc.mjd + stepval[istep]
    day = anytim2utc(day, /ext)
    temppath = stereo_browse
    sdate = string(day.year,  format='(I4.4)') + '/' + $
            string(day.month, format='(I2.2)') + '/' + $
            string(day.day,   format='(I2.2)')
    temppath = concat_dir(temppath, sdate)
    if file_exist(temppath) then urlstep[istep] = '../../../' + sdate
endfor    
;
;  Create default pages for the previous and next days, if necessary.
;
if not keyword_set(nodata) then begin
    test = file_exist(concat_dir(daypath[1],'index.shtml'))
    if not test then ssc_make_browse, utc, /nodata
    if not file_exist(concat_dir(daypath[0],'index.shtml')) then begin
        day = utc
        day.mjd = day.mjd - 1
        ssc_make_browse, day, /nodata
    endif
    if not file_exist(concat_dir(daypath[2],'index.shtml')) then begin
        day = utc
        day.mjd = day.mjd + 1
        ssc_make_browse, day, /nodata
    endif
    if not test then ssc_make_browse, utc, /nodata
endif
;
;  If the provisional keyword was set, then look for HI2-A files older than a
;  day.  If none are found, then return.
;
if keyword_set(provisional) then begin
    jpeg_path = sc[n_elements(sc)-1] + '/' + tel[n_elements(tel)-1] + '/' + $
                ntrim(res[n_elements(res)-1]) + '/*.jpg'
    jpeg_path = concat_dir(filepath, jpeg_path)
    files = file_search(jpeg_path, count=njpeg)
    if njpeg gt 0 then begin
        mtime = min((file_info(files)).mtime)
        if (systime(1) - mtime) lt 86400 then return
    endif
endif
;
nsw_tot = 0
if keyword_set(nodata) or keyword_set(noprocessing) then goto, make_pages
;
;  Next, create the in-situ plots.
;
timerange = replicate(utc,2)
timerange.time = 0
timerange[1].mjd = timerange[1].mjd + 1
title = 'IMPACT/PLASTIC  ' + utc2str(utc,/vms,/date)
set_plot,'z'
ssc_beacon_plots, timerange=timerange, tick_unit=21600, filepath=filepath, $
                  title=title
;
;  Create the 3-day version.
;
timerange[0].mjd = timerange[0].mjd - 1
timerange[1].mjd = timerange[1].mjd + 1
ssc_beacon_plots, timerange=timerange, tick_unit=86400, filepath=filepath, $
                  title=title, suffix='_3day'
;
;  Create the 7-day version.
;
timerange[0].mjd = timerange[0].mjd - 2
timerange[1].mjd = timerange[1].mjd + 2
ssc_beacon_plots, timerange=timerange, tick_unit=172800, filepath=filepath, $
                  title=title, suffix='_7day'
;
;  Create the daily heliographic synoptic maps.
;
if keyword_set(add_synoptic) or (not keyword_set(nosecchi)) then begin
    sreplace = keyword_set(replace) or keyword_set(cor_replace) or $
               keyword_set(synop_replace)
    ssc_make_euvi_browse_synoptic, utc, filepath, replace=sreplace, $
                                   beacon=n_params() eq 0, _extra=_extra
endif
;
;  Ingest any HI1 star removed images.
;
ssc_browse_ingest_hi1_srem, utc
;
;  Next, create full-size and thumbnail images for each of the SECCHI
;  instruments.
;
loadct, 0
setflag, missing=0
;
;  Read the summary files.
;
nsw_tot = 0
for isc = 0,1 do begin
    cat = scc_read_summary(date=utc, spacecraft=sc[isc], source='lz', /check)
    if datatype(cat,1) ne 'Structure' then cat = $
      {filename: '', date_obs: '', telescope: '', exptime: 0.0, xsize: 0, ysize: 0, $
       filter: '', value: 0.0, svalue: '', prog: '', osnum: 0, dest: '', fps: '', $
       led: '', compr: '', nmiss: -1, spacecraft: '', type: ''}
;
;  Determine whether or not to allow beacon images through.  This section of
;  the code reflects when side-lobe operations start and stop on each
;  spacecraft.
;
    allow_sw = 0
    if ssc_sidelobe_period(sc[isc], utc) then allow_sw = 1
;
    for itel = 0,ntel-1 do begin
        telescope = tel[itel]
        if telescope eq 'euvi' then nwave=n_elements(wave) else nwave=1
;
;  Step through the wavelengths.  Filter out SSR2, SW, calibration, and
;  subfield images.
;
        for iwave = 0,nwave-1 do begin
            subtest = abs(alog(float(cat.xsize)/cat.ysize))
            w = where((cat.telescope eq strupcase(telescope)) and $
                      ((cat.dest eq 'SSR1') or ((cat.dest eq 'SW') and allow_sw)) and $
                      (cat.type ne 'cal') and (subtest lt 0.1), count)
;
;  For EUVI, find all images with the right wavelength.
;
            if telescope eq 'euvi' then begin
                if count gt 0 then value = cat[w].value else value = -999
                ww = where((value ge wave[iwave]-5) and $
                           (value le wave[iwave]+5), ccount)
                if ccount gt 0 then begin
                    w = w[ww]
                    count = ccount
                endif
            endif
;
;  For COR1, find all image triplets within one minute of each other, or which
;  are total brightness images.
;
            if telescope eq 'cor1' then begin
                if count gt 0 then begin
                    value = cat[w].value
                    tai = utc2tai(cat[w].date_obs)
                    if count gt 2 then dtai = [999,999,tai[2:*]-tai] else $
                      dtai = [999,999]
                    ww = where((dtai le 60) or (value eq 1001), ccount)
                    if ccount gt 0 then begin
                        w = w[ww]
                        count = ccount
                    endif
                endif
            endif
;
;  For COR2, find all images with a polarization angle of 240, or which are
;  double exposures.
;
            if telescope eq 'cor2' then begin
                if count gt 0 then begin
                    prog  = strupcase(cat[w].prog)
                    value = cat[w].value
                end else begin
                    prog  = 'none'
                    value = -999
                endelse
                ww = where((value eq 240) or (prog eq 'DOUB'), ccount)
                if ccount gt 0 then begin
                    w = w[ww]
                    count = ccount
                endif
            endif
;
;  Step through the files.
;
            if not keyword_set(nosecchi) then for ifile = 0,count-1 do begin
                cat0 = cat[w[ifile]]
                file = scc_data_path(cat0.spacecraft, source='lz', $
                                     type=cat0.type, tel=cat0.telescope, $
                                     date=cat0.date_obs)
                file = concat_dir(file, cat0.filename)
                break_file, file, disk, dir, name
;
;  Determine whether to process the file as a beacon image.
;
                as_beacon = 0
                if cat0.dest eq 'SW' then case telescope of
                    'euvi': if cat0.xsize le 512 then as_beacon = 1
                    'hi1': if cat0.compr ne 'RICE' then as_beacon = 1
                    else: as_beacon = 1
                endcase
;
                if is_fits(file) then $
                  ssc_browse_secchi_jpeg, file, name, sc[isc], stereo_browse, $
                    as_beacon=as_beacon, outline=(telescope eq 'hi2'), _extra=_extra
            endfor
;
;  If there are more level-0 files than space weather files, then delete the
;  space weather files.
;
            jpeg_path = concat_dir(filepath, sc[isc] + '/' + telescope)
            if telescope eq 'euvi' then jpeg_path = $
              concat_dir(jpeg_path, string(wave[iwave],format='(I3.3)'))
            jpeg_files = file_search(concat_dir(jpeg_path, '256/*.jpg'))
            break_file, jpeg_files, disk0, dir0, name0
            sw = where(strmid(name0,17,1) eq '7', nsw, ncomplement=nz)
            if (nsw gt 0) and (nz gt nsw) then begin
                if telescope eq 'cor1' then ires0 = 1 else ires0 = 0
                ssc_purge_secchi_beacon, jpeg_path, res[ires0:*], name0, sw, $
                                         saved
                nsw_tot = nsw_tot + saved
            end else nsw_tot = nsw_tot + nsw
;
;  Refresh the index files for each directory.
;
            if telescope eq 'cor1' then ires0 = 1 else ires0 = 0
            for ires = ires0,n_elements(res)-1 do begin
                temp_path = concat_dir(jpeg_path, ntrim(res[ires]))
                if file_exist(temp_path) then ssc_secchi_dir_html, temp_path
            endfor
;
;  Refresh the main pages, in case image links are now broken.
;
            ssc_make_browse, utc, /noprocessing
        endfor                  ;iwave
    endfor                      ;itel
endfor                          ;isc
;
;  Create the MPEG files for the day in question.
;
if (not keyword_set(nosecchi)) or keyword_set(mpeg_gen) then $
  ssc_browse_secchi_mpeg, utc
;
;  Find the most recent SOHO images for the day in question.
;
soho = ['eit_171', 'eit_195', 'eit_284', 'eit_304', 'c2', 'c3']
sohodir = ['eit171', 'eit195', 'eit284', 'eit304', 'c2', 'c3']
caldate = anytim2cal(utc, form=8, /date)
year = strmid(caldate,0,4)
for isoho = 0,n_elements(soho)-1 do begin
    searchfile = anytim2cal(utc, form=8, /date) + '_*_' + soho[isoho] + '.gif'
    files = file_search(concat_dir(soho_gif, searchfile), count=count)
    if count gt 0 then begin
        s = reverse(sort(files))
        sohofile = files[s[0]]
        filename = concat_dir(filepath, 'soho_' + soho[isoho] + '.gif')
        file_copy, sohofile, filename, /overwrite, /force
    endif
;
    sohopath = concat_dir(soho_jpeg, year)
    sohopath = concat_dir(sohopath, sohodir[isoho])
    sohopath = concat_dir(sohopath, caldate)
    files = file_search(concat_dir(sohopath, '*_512.jpg'), count=count)
    if count gt 0 then begin
        s = reverse(sort(files))
        sohofile = files[s[0]]
        filename = concat_dir(filepath, 'soho_' + soho[isoho] + '.jpg')
        file_copy, sohofile, filename, /overwrite, /force
    endif
endfor
;
;  Find the representative SDO images for the day in question.  If the files
;  are not already there, then retrieve them from the LMSAL site.  If not a
;  valid JPEG file, then delete it.
;
ext = anytim2utc(utc, /ext)
sext = string(ext.year,  format='(I4.4)') + '/' + $
       string(ext.month, format='(I2.2)') + '/' + $
       string(ext.day,   format='(I2.2)') + '/'
if sext ge '2010/04/16' then begin
    sdopath = 'http://sdowww.lmsal.com/sdomedia/SunInTime/' + sext
    for isdo = 0,n_elements(sdo)-1 do begin
        sdofile = concat_dir(filepath, sdo[isdo])
        sdotemp = sdofile + '.tmp'
        spawn, 'wget -t 2 ' + sdopath + '/' + sdo[isdo] + ' -O ' + sdotemp
        if not query_jpeg(sdotemp) then $
          file_delete, sdotemp, /allow_nonexistent else $
          file_move, sdotemp, sdofile, /overwrite
    endfor
endif
;
;  Find the MLSO coronagraph image for the day in question.  Start by looking
;  for the KCOR image.
;
if not file_exist(concat_dir(filepath, 'kcor.gif')) then begin
    kcorpath ='http://download.hao.ucar.edu/' + sext
    spawn, 'wget -t 2 ' + kcorpath + ' -O -', temp
    name = ''
    for i=0,n_elements(temp)-1 do begin
        j1 = strpos(temp[i], 'kcor_cropped.gif')
        if j1 gt 0 then begin
            j0 = strpos(temp[i], 'href="') + 6
            name = strmid(temp[i], j0, j1-j0+16)
        endif
    endfor
    if name ne '' then spawn, 'wget -t 2 ' + kcorpath + name + ' -O ' + $
           concat_dir(filepath, 'kcor.gif') else if n_params() eq 0 then begin
;
;  If the MLSO file is not found, and one is processing today's date, then get
;  the latest file.
;
        kcorpath = 'http://download.hao.ucar.edu/d5/www/fullres/latest/latest.kcor.gif'
        tmpdir = get_temp_dir()
        tmpout = concat_dir(tmpdir, 'latest.kcor.gif')
        spawn, 'wget -t 2 ' + kcorpath + ' -O ' + tmpout
        info = file_info(tmpout)
        if ((systime(1) - info.mtime) lt 86400) and (info.size gt 0) then $
          file_move, tmpout, concat_dir(filepath, 'latest.kcor.gif'), /overwrite else $
          file_delete, tmpout
;
;  Otherwise, try looking for the Mk4 file.
;
    end else if not file_exist(concat_dir(filepath, 'mk4.jpg')) then begin
        mk4path ='http://download.hao.ucar.edu/' + sext
        spawn, 'wget -t 2 ' + mk4path + ' -O -', temp
        for i=0,n_elements(temp)-1 do begin
            j1 = strpos(temp[i], '.avg.vig.jpg')
            if j1 gt 0 then begin
                j0 = strpos(temp[i], 'HREF="') + 6
                name = strmid(temp[i], j0, j1-j0+12)
                spawn, 'wget -t 2 ' + mk4path + name + ' -O ' + $
                       concat_dir(filepath, 'mk4.jpg')
            endif
        endfor
    endif
endif
;
;  Create the web pages.  The in-situ pages are created last.
;
make_pages:
pages = ['index','insitu','insitu_3day','insitu_7day'] + '.shtml'
suffix = ['','','_3day','_7day']
npages = n_elements(pages)
for ipage = 0,npages-1 do begin
    openw, output, concat_dir(filepath, pages[ipage]), /get_lun
;
;  Start by inserting the header.
;
    openr, header, concat_dir(stereo_browse,'header.txt'), /get_lun
    line = 'String'
    while not eof(header) do begin
        readf, header, line
        printf, output, line
    endwhile
    free_lun, header
;
;  Print out a disclaimer about space weather data.
;
    if (nsw_tot gt 0) and (ipage eq 0) then begin
        printf, output, ''
        printf, output, '<P>This day includes data derived from the STEREO'
        printf, output, 'space weather beacon telemetry, which is a very'
        printf, output, 'low rate, highly compressed data stream broadcast'
        printf, output, 'by the spacecraft 24 hours per day.  These data'
        printf, output, 'are used for space weather forecasting.  Because'
        printf, output, 'of the large compression factors used, these'
        printf, output, 'beacon images are of much lower quality than the'
        printf, output, 'actual science data.</P>'
        printf, output, ''
    end else if (ipage ge 1) then begin
        printf, output, ''
        printf, output, '<P>These plots include data derived from the STEREO'
        printf, output, 'space weather beacon telemetry, which is a very'
        printf, output, 'low rate, highly compressed data stream broadcast'
        printf, output, 'by the spacecraft 24 hours per day.  These data'
        printf, output, 'are used for space weather forecasting.  Because'
        printf, output, 'the data are produced in real-time, the calibrations'
        printf, output, 'may not match those used for the final data'
        printf, output, 'product.</P>'
        printf, output, ''
    endif
;
;  Form the links to the previous and next week and rotation.
;
    steppage = steptext
    for istep=0,3 do if urlstep[istep] ne '' then steppage[istep] = $
      '<A HREF="' + urlstep[istep] + '/' + pages[ipage] + '">' + $
      steptext[istep] + '</A>'
;
;  Form the links to the previous and next days.
;
    filler = ''
    for i=1,25 do filler = filler + "&nbsp;"
    spacer = ''
    for i=1,10 do spacer = spacer + "&nbsp;"

;
    if file_exist(concat_dir(daypath[0],'index.shtml')) then begin
        day = utc
        day.mjd = day.mjd - 1
        prev = '<A HREF="' + urlpath[0] + '/' + pages[ipage] + '">[' + $
               anytim2utc(day,/vms,/date) + ']</A>'
    end else begin
        prev = ''
        for i=1,14 do prev = prev + "&nbsp;"
    endelse
;
    if file_exist(concat_dir(daypath[2],'index.shtml')) then begin
        day = utc
        day.mjd = day.mjd + 1
        next = '<A HREF="' + urlpath[2] + '/' + pages[ipage] + '">[' + $
               anytim2utc(day,/vms,/date) + ']</A>'
    end else begin
        next = ''
        for i=1,10 do next = next + "&nbsp;"
    endelse
;
    printf, output, $
            '<TABLE CLASS="coords" ALIGN=CENTER><TR>'
    printf, output, '<TD>' + spacer + '</TD><TD>' + filler + '</TD>'
    printf, output, '<TD>' + filler + '</TD><TD>' + filler + '</TD>'
    printf, output, '<TD>' + filler + '</TD><TD>' + filler + '</TD>'
    printf, output, '<TD>' + spacer + '</TD></TR><TR>'
    printf, output, '<TD STYLE="text-align:left">' + steppage[0] + '</TD>'
    printf, output, '<TD STYLE="text-align:left">' + steppage[1] + '</TD>'
    printf, output, '<TD STYLE="text-align:left">' + prev + '</TD>'
    printf, output, '<TH STYLE="font-size:x-large; text-align:center">' + $
            strtrim(anytim2utc(utc,/vms,/date),2) + '</TH>'
    printf, output, '<TD STYLE="text-align:right">' + next + '</TD>'
    printf, output, '<TD STYLE="text-align:right">' + steppage[2] + '</TD>'
    printf, output, '<TD STYLE="text-align:right">' + steppage[3] + $
            '</TD></TR>'
    printf, output, '</TABLE><P>'
    printf, output
;
;  If the NODATA keyword was set, then write out a message saying there's no
;  data.
;
    if keyword_set(nodata) then begin
        printf, output, '<H2 ALIGN=CENTER>NO DATA</H2><P>'
        if solar_conj then begin
            printf, output
            printf, output, 'The IMPACT, PLASTIC, and SECCHI instruments '
            printf, output, 'on Ahead were turned off for superior solar '
            printf, output, 'conjuction on 20 March 2015.  Data from these '
            printf, output, 'instruments will be unavailable until July '
            printf, output, '2015.<P>'
            printf, output
        endif
;
;  Otherwise, write out the body of the page, depending on the kind of page it
;  is.  If the main page, write out the table with the SECCHI images
;
    end else begin
        if ipage eq 0 then begin
            printf, output, '<P ALIGN=CENTER>'
            printf, output, '<A HREF="' + pages[1] + '">In-situ and radio</A>'
            printf, output, '</P>'
            printf, output
            if solar_conj then begin
                printf, output
                printf, output, 'The IMPACT, PLASTIC, and SECCHI instruments '
                printf, output, 'on Ahead were turned off for superior solar '
                printf, output, 'conjuction on 20 March 2015.  Data from these '
                printf, output, 'instruments will be unavailable until July '
                printf, output, '2015.<P>'
                printf, output
            endif
;
;  If the heliographic maps were made, then add a link for the maps.
;
            synop_files = 'euvi_' + ntrim(wave) + '_heliographic.gif'
            synop_path = concat_dir(filepath, synop_files)
            if not file_exist(synop_path[0]) then begin
                synop_files = 'euvi_'+ntrim(wave)+'_heliographic_beacon.gif'
                synop_path = concat_dir(filepath, synop_files)
            endif
            if file_exist(synop_path[0]) then begin
                printf, output, '<P ALIGN="CENTER">
                printf, output, '<IMG SRC="' + synop_files[0] + '" ' + $
                  'ALT="EUVI ' + ntrim(wave[0]) + ' heliographic map"><BR>'
                outline = 'Heliographic maps: <A HREF="' + synop_files[0] + $
                  '">' + ntrim(wave[0]) + '</A>'
                for iwave=1,n_elements(wave)-1 do $
                  if file_exist(synop_path[iwave]) then outline = outline + $
                  ', <A HREF="' + synop_files[iwave] + '">' + $
                  ntrim(wave[iwave]) + '</A>'
                printf, output, outline
                printf, output, '</P>'
                printf, output
            endif
;
;  Look for the most recent SECCHI images on the given day.
;
            nodatajpg = '/browse/nodata_256.jpg'
            euvi_latest_file = replicate(nodatajpg,2,4)
            euvi_latest_path = strarr(2,4)
            cor1_latest_file = replicate(nodatajpg,2)
            cor1_latest_path = strarr(2)
            cor2_latest_file = replicate(nodatajpg,2)
            cor2_latest_path = strarr(2)
            hi1_latest_file  = replicate(nodatajpg,2)
            hi1_latest_path  = strarr(2)
            hi2_latest_file  = replicate(nodatajpg,2)
            hi2_latest_path  = strarr(2)
;
            cor1_rdiff_latest_file = replicate(nodatajpg,2)
            cor1_rdiff_latest_path = strarr(2)
            cor2_rdiff_latest_file = replicate(nodatajpg,2)
            cor2_rdiff_latest_path = strarr(2)
            hi1_rdiff_latest_file  = replicate(nodatajpg,2)
            hi1_rdiff_latest_path  = strarr(2)
            hi1_srem_latest_file  = replicate(nodatajpg,2)
            hi1_srem_latest_path  = strarr(2)
;
            cor1_pb_latest_file = replicate(nodatajpg,2)
            cor1_pb_latest_path = strarr(2)
;
            ex = anytim2utc(utc, /ext)
            for isc = 0,1 do begin
;
;  First, look for EUVI images.
;
                for iwave = 0,3 do begin
                    path = string(ex.year, format='(I4.4)')  + '/' + $
                           string(ex.month, format='(I2.2)') + '/' + $
                           string(ex.day, format='(I2.2)')   + '/' + $
                           sc[isc] + '/euvi/' + $
                           string(wave[iwave], format='(I3.3)')
                    fullpath = concat_dir(stereo_browse, path)
                    files = file_search(concat_dir(fullpath,'256/*.jpg'), $
                                        count=count)
                    if count gt 0 then begin
                        euvi_latest_path[isc,iwave] = sc[isc] + '/euvi/' + $
                          string(wave[iwave], format='(I3.3)')
                        s = reverse(sort(files))
                        temp = files[s[0]]
                        break_file, temp, disk, dir, name, ext
                        temp = concat_dir(euvi_latest_path[isc,iwave], $
                                          '256/' + name + ext)
                        euvi_latest_file[isc,iwave] = temp
                    endif
                endfor
;
;  Next, look for COR1 images.
;
                path = string(ex.year, format='(I4.4)')  + '/' + $
                       string(ex.month, format='(I2.2)') + '/' + $
                       string(ex.day, format='(I2.2)')   + '/' + $
                       sc[isc] + '/cor1'
                fullpath = concat_dir(stereo_browse, path)
                files = file_search(concat_dir(fullpath,'256/*.jpg'), $
                                    count=count)
                if count gt 0 then begin
                    cor1_latest_path[isc] = sc[isc] + '/cor1'
                    s = reverse(sort(files))
                    temp = files[s[0]]
                    break_file, temp, disk, dir, name, ext
                    temp = concat_dir(cor1_latest_path[isc], $
                                      '256/' + name + ext)
                    cor1_latest_file[isc] = temp
                endif
;
;  Look for the COR1 running difference images.
;
                path = string(ex.year, format='(I4.4)')  + '/' + $
                       string(ex.month, format='(I2.2)') + '/' + $
                       string(ex.day, format='(I2.2)')   + '/' + $
                       sc[isc] + '/cor1_rdiff'
                fullpath = concat_dir(stereo_browse, path)
                files = file_search(concat_dir(fullpath,'256/*.jpg'), $
                                    count=count)
                if count gt 0 then begin
                    cor1_rdiff_latest_path[isc] = sc[isc] + '/cor1_rdiff'
                    s = reverse(sort(files))
                    temp = files[s[0]]
                    break_file, temp, disk, dir, name, ext
                    temp = concat_dir(cor1_rdiff_latest_path[isc], $
                                      '256/' + name + ext)
                    cor1_rdiff_latest_file[isc] = temp
                endif
;
;  Look for the COR1 polarized brightness images.
;
                path = string(ex.year, format='(I4.4)')  + '/' + $
                       string(ex.month, format='(I2.2)') + '/' + $
                       string(ex.day, format='(I2.2)')   + '/' + $
                       sc[isc] + '/cor1_pb'
                fullpath = concat_dir(stereo_browse, path)
                files = file_search(concat_dir(fullpath,'256/*.jpg'), $
                                    count=count)
                if count gt 0 then begin
                    cor1_pb_latest_path[isc] = sc[isc] + '/cor1_pb'
                    s = reverse(sort(files))
                    temp = files[s[0]]
                    break_file, temp, disk, dir, name, ext
                    temp = concat_dir(cor1_pb_latest_path[isc], $
                                      '256/' + name + ext)
                    cor1_pb_latest_file[isc] = temp
                endif
;
;  Next, look for COR2 images.
;
                path = string(ex.year, format='(I4.4)')  + '/' + $
                       string(ex.month, format='(I2.2)') + '/' + $
                       string(ex.day, format='(I2.2)')   + '/' + $
                       sc[isc] + '/cor2'
                fullpath = concat_dir(stereo_browse, path)
                files = file_search(concat_dir(fullpath,'256/*.jpg'), $
                                    count=count)
                if count gt 0 then begin
                    cor2_latest_path[isc] = sc[isc] + '/cor2'
                    s = reverse(sort(files))
                    temp = files[s[0]]
                    break_file, temp, disk, dir, name, ext
                    temp = concat_dir(cor2_latest_path[isc], $
                                      '256/' + name + ext)
                    cor2_latest_file[isc] = temp
                endif
;
;  Look for the COR2 running difference images.
;
                path = string(ex.year, format='(I4.4)')  + '/' + $
                       string(ex.month, format='(I2.2)') + '/' + $
                       string(ex.day, format='(I2.2)')   + '/' + $
                       sc[isc] + '/cor2_rdiff'
                fullpath = concat_dir(stereo_browse, path)
                files = file_search(concat_dir(fullpath,'256/*.jpg'), $
                                    count=count)
                if count gt 0 then begin
                    cor2_rdiff_latest_path[isc] = sc[isc] + '/cor2_rdiff'
                    s = reverse(sort(files))
                    temp = files[s[0]]
                    break_file, temp, disk, dir, name, ext
                    temp = concat_dir(cor2_rdiff_latest_path[isc], $
                                      '256/' + name + ext)
                    cor2_rdiff_latest_file[isc] = temp
                endif
;
;  Next, look for HI1 images.
;
                path = string(ex.year, format='(I4.4)')  + '/' + $
                       string(ex.month, format='(I2.2)') + '/' + $
                       string(ex.day, format='(I2.2)')   + '/' + $
                       sc[isc] + '/hi1'
                fullpath = concat_dir(stereo_browse, path)
                files = file_search(concat_dir(fullpath,'256/*.jpg'), $
                                    count=count)
                if count gt 0 then begin
                    hi1_latest_path[isc] = sc[isc] + '/hi1'
                    s = reverse(sort(files))
                    temp = files[s[0]]
                    break_file, temp, disk, dir, name, ext
                    temp = concat_dir(hi1_latest_path[isc], $
                                      '256/' + name + ext)
                    hi1_latest_file[isc] = temp
                endif
;
;  Look for the HI1 running difference images.
;
                path = string(ex.year, format='(I4.4)')  + '/' + $
                       string(ex.month, format='(I2.2)') + '/' + $
                       string(ex.day, format='(I2.2)')   + '/' + $
                       sc[isc] + '/hi1_rdiff'
                fullpath = concat_dir(stereo_browse, path)
                files = file_search(concat_dir(fullpath,'256/*.jpg'), $
                                    count=count)
                if count gt 0 then begin
                    hi1_rdiff_latest_path[isc] = sc[isc] + '/hi1_rdiff'
                    s = reverse(sort(files))
                    temp = files[s[0]]
                    break_file, temp, disk, dir, name, ext
                    temp = concat_dir(hi1_rdiff_latest_path[isc], $
                                      '256/' + name + ext)
                    hi1_rdiff_latest_file[isc] = temp
                endif
;
;  Look for the HI1 star removed images.
;
                path = string(ex.year, format='(I4.4)')  + '/' + $
                       string(ex.month, format='(I2.2)') + '/' + $
                       string(ex.day, format='(I2.2)')   + '/' + $
                       sc[isc] + '/hi1_srem'
                fullpath = concat_dir(stereo_browse, path)
                files = file_search(concat_dir(fullpath,'256/*.png'), $
                                    count=count)
                if count gt 0 then begin
                    hi1_srem_latest_path[isc] = sc[isc] + '/hi1_srem'
                    s = reverse(sort(files))
                    temp = files[s[0]]
                    break_file, temp, disk, dir, name, ext
                    temp = concat_dir(hi1_srem_latest_path[isc], $
                                      '256/' + name + ext)
                    hi1_srem_latest_file[isc] = temp
                endif
;
;  Next, look for HI2 images.
;
                path = string(ex.year, format='(I4.4)')  + '/' + $
                       string(ex.month, format='(I2.2)') + '/' + $
                       string(ex.day, format='(I2.2)')   + '/' + $
                       sc[isc] + '/hi2'
                fullpath = concat_dir(stereo_browse, path)
                files = file_search(concat_dir(fullpath,'256/*.jpg'), $
                                    count=count)
                if count gt 0 then begin
                    hi2_latest_path[isc] = sc[isc] + '/hi2'
                    s = reverse(sort(files))
                    temp = files[s[0]]
                    break_file, temp, disk, dir, name, ext
                    temp = concat_dir(hi2_latest_path[isc], $
                                      '256/' + name + ext)
                    hi2_latest_file[isc] = temp
                endif
            endfor
;
;  Write out the table with the SECCHI images
;
            printf, output, '<TABLE CLASS="coords" ' + $
                    'ALIGN=CENTER STYLE="text-align:center">'
            printf, output, '<TR>'
            printf, output, '<TH>STEREO ' + scn[0] + '</TH>'
            printf, output, '<TD WIDTH=20></TD>'
            ;;href = "http://sdowww.lmsal.com/suntoday"
            href = "https://sdo.gsfc.nasa.gov/data/"
            printf, output, '<TH><A HREF="' + href + '" TARGET="_blank">' + $
                    'SDO</A>'
            href = "https://sohowww.nascom.nasa.gov/data/realtime-images.html"
            printf, output, ' / <A HREF="' + href + '" TARGET="_blank">' + $
                    'SOHO</A>'
            href = "https://www2.hao.ucar.edu/mlso/mlso-home-page"
            printf, output, ' / <A HREF="' + href + '" TARGET="_blank">' + $
                    'MLSO</A></TH>'
            printf, output, '<TD WIDTH=20></TD>'
            printf, output, '<TH>STEREO ' + scn[1] + '</TH>'
            printf, output, '</TR>'
;
;  Start with the EUVI images.
;
            for iwave = 0,3 do begin
                test = where(euvi_latest_file[*,iwave] ne nodatajpg, count)
                if count gt 0 then begin
                    printf, output, '<TR>'
                    for isc = 0,1 do begin
                        printf, output, '<TD STYLE="text-align:center">'
                        printf, output, '<IMG SRC="' + $
                                euvi_latest_file[isc,iwave] + $
                                '" ALT="' + euvi_latest_file[isc,iwave] + '">'
                        printf, output, '</TD>'
;
;  If the SOHO equivalent exists, then include a link.
;
                        if isc eq 0 then begin
                            printf, output, '<TD></TD>'
                            sohofile = 'soho_eit_'+ntrim(wave[iwave])+'.jpg'
                            sohotext = 'SOHO/EIT '+ntrim(wave[iwave])+' A'
                            info = file_info(concat_dir(filepath, sohofile))
                            if (info.exists eq 0) or (info.size eq 0) then begin
                                sohofile = 'soho_eit_'+ntrim(wave[iwave])+'.gif'
                                info = file_info(concat_dir(filepath, sohofile))
                                if (info.exists eq 0) or (info.size eq 0) then begin
                                    sohofile = nodatajpg
                                    sohotext = ''
                                endif
                            endif
;
;  If the SDO equivalent exists, then use it instead.
;
                            sdofile = concat_dir(filepath, sdo[iwave])
                            info = file_info(sdofile)
                            if (info.exists eq 1) and (info.size gt 0) then begin
                                sohofile = sdo[iwave]
                                sohotext = 'SDO/AIA ' + sdo_wave[iwave] + ' A'
                            endif
;
                            printf, output, '<TD STYLE="text-align:center">'
                            printf, output, '<IMG SRC="' + sohofile + $
                              '" WIDTH=256 HEIGHT=256 ALT="' + sohofile + '">'
                            printf, output, '</TD>'
                            printf, output, '<TD></TD>'
                        endif
                    endfor
                    printf, output, '</TR>'
;
;  Write out the links to the JPEG images.
;
                    printf, output, '<TR>'
                    for isc = 0,1 do begin
                        printf, output, '<TD STYLE="text-align:center">'
                        entry = ''
                        if euvi_latest_path[isc,iwave] ne '' then $
                          for ires = 0,n_elements(res)-1 do begin
                            sres = ntrim(res[ires])
                            path = concat_dir(euvi_latest_path[isc,iwave],sres)
                            entry = entry + '<A HREF=' + path + '>' + sres + $
                                    '</A>'
                            if ires lt (n_elements(res)-1) then $
                              entry = entry + ','
                        endfor
                        printf, output, entry
                        printf, output, '</TD>'
                        if isc eq 0 then printf, output, $
                          '<TD></TD><TD STYLE="text-align:center">' + sohotext + $
                          '</TD><TD></TD>'
                    endfor
                    printf, output, '</TR>'
;
;  Write out the links to the MPEG movies, if found.
;
                    printf, output, '<TR>'
                    caldate = anytim2cal(utc, form=8, /date)
                    for isc = 0,1 do begin
                        printf, output, '<TD STYLE="text-align:center">'
                        mpeg_file = sc[isc] + '_' + caldate + '_euvi_' + $
                                    ntrim(wave[iwave]) + '_' + mres + '.mpg'
                        entry = ''
                        if file_exist(concat_dir(filepath,mpeg_file[0])) $
                          then begin
                            entry = 'MPEG: '
                            for ires = 0,n_elements(mres)-1 do begin
                                entry = entry + '<A HREF=' + mpeg_file[ires] +$
                                        '>' + mres[ires] + '</A>'
                                if ires lt (n_elements(mres)-1) then $
                                  entry = entry + ','
                            endfor
                        endif
                        printf, output, entry
                        printf, output, '</TD>'
                        if isc eq 0 then printf, output, $
                          '<TD></TD><TD></TD><TD></TD>'
                    endfor
;
                    printf, output, '</TR><TR><TD></TD></TR>'
                endif           ;One or more EUVI images found
            endfor              ;iwave
;
;  Next, do the COR1 images.
;
            test = where(cor1_latest_file ne nodatajpg, count)
            if count gt 0 then begin
                printf, output, '<TR>'
                for isc = 0,1 do begin
                    printf, output, '<TD STYLE="text-align:center">'
                    printf, output, '<IMG SRC="' + $
                            cor1_latest_file[isc] + '" ALT="' + $
                            cor1_latest_file[isc] + '">'
                    printf, output, '</TD>'
;
;  If the MLSO equivalent exists, then include a link.
;
                    if isc eq 0 then begin
                        printf, output, '<TD></TD>'
                        mlsofile = 'kcor.gif'
                        info = file_info(concat_dir(filepath, mlsofile))
                        if (info.exists eq 0) or (info.size eq 0) then begin
                            mlsofile = 'latest.kcor.gif'
                            info = file_info(concat_dir(filepath, mlsofile))
                            if (info.exists eq 0) or (info.size eq 0) then begin
                                mlsofile = 'mk4.jpg'
                                info = file_info(concat_dir(filepath, mlsofile))
                                if (info.exists eq 0) or (info.size eq 0) then begin
                                    mlsofile = 'latest.mk4.gif'
                                    info = file_info(concat_dir(filepath, mlsofile))
                                    if (info.exists eq 0) or (info.size eq 0) then $
                                      mlsofile = nodatajpg
                                endif
                            endif
                        endif
                        printf, output, '<TD STYLE="text-align:center">'
                        printf, output, '<IMG SRC="' + mlsofile + $
                          '" WIDTH=256 HEIGHT=256 ALT="' + mlsofile + '">'
                        printf, output, '</TD>'
                        printf, output, '<TD></TD>'
                    endif
                endfor
                printf, output, '</TR>'
;
;  Write out the links to the JPEG images.
;
                printf, output, '<TR>'
                for isc = 0,1 do begin
                    printf, output, '<TD STYLE="text-align:center">'
                    entry = ''
                    if cor1_latest_path[isc] ne '' then $
                      for ires = 1,n_elements(res)-1 do begin
                        sres = ntrim(res[ires])
                        path = concat_dir(cor1_latest_path[isc],sres)
                        entry = entry + '<A HREF=' + path + '>' + sres + $
                                '</A>'
                        if ires lt (n_elements(res)-1) then $
                          entry = entry + ','
                    endfor
                    printf, output, entry
                    printf, output, '</TD>'
                    if isc eq 0 then printf, output, $
                      '<TD></TD><TD STYLE="text-align:center">MLSO</TD><TD></TD>'
                endfor
                printf, output, '</TR>'
;
;  Write out the links to the MPEG movies, if found.
;
                printf, output, '<TR>'
                caldate = anytim2cal(utc, form=8, /date)
                for isc = 0,1 do begin
                    printf, output, '<TD STYLE="text-align:center">'
                    mpeg_file = sc[isc] + '_' + caldate + '_cor1_' + mres + $
                                '.mpg'
                    entry = ''
                    if file_exist(concat_dir(filepath,mpeg_file[0])) then begin
                        entry = 'MPEG: '
                        for ires = 0,n_elements(mres)-1 do begin
                            entry = entry + '<A HREF=' + mpeg_file[ires] + $
                                    '>' + mres[ires] + '</A>'
                            if ires lt (n_elements(mres)-1) then $
                              entry = entry + ','
                        endfor
                    endif
                    printf, output, entry
                    printf, output, '</TD>'
                    if isc eq 0 then printf, output, $
                      '<TD></TD><TD></TD><TD></TD>'
                endfor
                printf, output, '</TR>'
;
;  Write out the links to the running difference images if found.
;
                test = where(cor1_rdiff_latest_file ne nodatajpg, count)
                if count gt 0 then begin
                    printf, output, '<TR>'
                    for isc = 0,1 do begin
                        printf, output, '<TD STYLE="text-align:center">'
                        if cor1_rdiff_latest_file[isc] eq nodatajpg then $
                          entry = '' else entry = 'Diff: '
                        if cor1_rdiff_latest_path[isc] ne '' then $
                          for ires = 1,n_elements(res)-1 do begin
                            sres = ntrim(res[ires])
                            path = concat_dir(cor1_rdiff_latest_path[isc],sres)
                            entry = entry + '<A HREF=' + path + '>' + sres + $
                                    '</A>'
                            if ires lt (n_elements(res)-1) then $
                              entry = entry + ','
                        endfor
                        printf, output, entry
                        printf, output, '</TD>'
                        if isc eq 0 then printf, output, $
                          '<TD></TD><TD></TD><TD></TD>'
                    endfor
                    printf, output, '</TR>'
;
;  Write out the links to the running difference MPEG movies, if found.
;
                    printf, output, '<TR>'
                    caldate = anytim2cal(utc, form=8, /date)
                    for isc = 0,1 do begin
                        printf, output, '<TD STYLE="text-align:center">'
                        mpeg_file = sc[isc] + '_' + caldate + '_cor1_rdiff_' + mres + $
                                    '.mpg'
                        entry = ''
                        if file_exist(concat_dir(filepath,mpeg_file[0])) then begin
                            entry = 'Diff MPEG: '
                            for ires = 0,n_elements(mres)-1 do begin
                                entry = entry + '<A HREF=' + mpeg_file[ires] + $
                                        '>' + mres[ires] + '</A>'
                                if ires lt (n_elements(mres)-1) then $
                                  entry = entry + ','
                            endfor
                        endif
                        printf, output, entry
                        printf, output, '</TD>'
                        if isc eq 0 then printf, output, $
                          '<TD></TD><TD></TD><TD></TD>'
                    endfor
                    printf, output, '</TR>'
                endif           ;One or more COR1 running difference images found
;
;
;  Write out the links to the polarized brightness images if found.
;
                test = where(cor1_pb_latest_file ne nodatajpg, count)
                if count gt 0 then begin
                    printf, output, '<TR>'
                    for isc = 0,1 do begin
                        printf, output, '<TD STYLE="text-align:center">'
                        if cor1_pb_latest_file[isc] eq nodatajpg then $
                          entry = '' else entry = 'pB: '
                        if cor1_pb_latest_path[isc] ne '' then $
                          for ires = 1,n_elements(res)-1 do begin
                            sres = ntrim(res[ires])
                            path = concat_dir(cor1_pb_latest_path[isc],sres)
                            entry = entry + '<A HREF=' + path + '>' + sres + $
                                    '</A>'
                            if ires lt (n_elements(res)-1) then $
                              entry = entry + ','
                        endfor
                        printf, output, entry
                        printf, output, '</TD>'
                        if isc eq 0 then printf, output, $
                          '<TD></TD><TD></TD><TD></TD>'
                    endfor
                    printf, output, '</TR>'
;
;  Write out the links to the running difference MPEG movies, if found.
;
                    printf, output, '<TR>'
                    caldate = anytim2cal(utc, form=8, /date)
                    for isc = 0,1 do begin
                        printf, output, '<TD STYLE="text-align:center">'
                        mpeg_file = sc[isc] + '_' + caldate + '_cor1_pb_' + mres + $
                                    '.mpg'
                        entry = ''
                        if file_exist(concat_dir(filepath,mpeg_file[0])) then begin
                            entry = 'pB MPEG: '
                            for ires = 0,n_elements(mres)-1 do begin
                                entry = entry + '<A HREF=' + mpeg_file[ires] + $
                                        '>' + mres[ires] + '</A>'
                                if ires lt (n_elements(mres)-1) then $
                                  entry = entry + ','
                            endfor
                        endif
                        printf, output, entry
                        printf, output, '</TD>'
                        if isc eq 0 then printf, output, $
                          '<TD></TD><TD></TD><TD></TD>'
                    endfor
                    printf, output, '</TR>'
                endif           ;One or more COR1 polarized brightness images found
;
                printf, output, '<TR><TD></TD></TR>'
            endif               ;One or more COR1 images found
;
;  Next, do the COR2 images.
;
            test = where(cor2_latest_file ne nodatajpg, count)
            if count gt 0 then begin
                printf, output, '<TR>'
                for isc = 0,1 do begin
                    printf, output, '<TD STYLE="text-align:center">'
                    printf, output, '<IMG SRC="' + $
                            cor2_latest_file[isc] + '" ALT="' + $
                            cor2_latest_file[isc] + '">'
                    printf, output, '</TD>'
;
;  If the SOHO equivalent exists, then include a link.
;
                    if isc eq 0 then begin
                        printf, output, '<TD></TD>'
                        sohofile = 'soho_c2.jpg'
                        info = file_info(concat_dir(filepath, sohofile))
                        if (info.exists eq 0) or (info.size eq 0) then begin
                            sohofile = 'soho_c2.gif'
                            info = file_info(concat_dir(filepath, sohofile))
                            if (info.exists eq 0) or (info.size eq 0) then $
                              sohofile = nodatajpg
                        endif
                        printf, output, '<TD STYLE="text-align:center">'
                        printf, output, '<IMG SRC="' + sohofile + $
                                '" WIDTH=256 HEIGHT=256 ALT="' + sohofile + '">'
                        printf, output, '</TD>'
                        printf, output, '<TD></TD>'
                    endif
                endfor
                printf, output, '</TR>'
;
;  Write out the links to the JPEG images.
;
                printf, output, '<TR>'
                for isc = 0,1 do begin
                    printf, output, '<TD STYLE="text-align:center">'
                    entry = ''
                    if cor2_latest_path[isc] ne '' then $
                      for ires = 0,n_elements(res)-1 do begin
                        sres = ntrim(res[ires])
                        path = concat_dir(cor2_latest_path[isc],sres)
                        entry = entry + '<A HREF=' + path + '>' + sres + $
                                '</A>'
                        if ires lt (n_elements(res)-1) then $
                          entry = entry + ','
                    endfor
                    printf, output, entry
                    printf, output, '</TD>'
                    if isc eq 0 then printf, output, $
                      '<TD></TD><TD STYLE="text-align:center">SOHO/LASCO/C2</TD><TD></TD>'
                endfor
                printf, output, '</TR>'
;
;  Write out the links to the MPEG movies, if found.
;
                printf, output, '<TR>'
                caldate = anytim2cal(utc, form=8, /date)
                for isc = 0,1 do begin
                    printf, output, '<TD STYLE="text-align:center">'
                    mpeg_file = sc[isc] + '_' + caldate + '_cor2_' + mres + $
                                '.mpg'
                    entry = ''
                    if file_exist(concat_dir(filepath,mpeg_file[0])) then begin
                        entry = 'MPEG: '
                        for ires = 0,n_elements(mres)-1 do begin
                            entry = entry + '<A HREF=' + mpeg_file[ires] + $
                                    '>' + mres[ires] + '</A>'
                            if ires lt (n_elements(mres)-1) then $
                              entry = entry + ','
                        endfor
                    endif
                    printf, output, entry
                    printf, output, '</TD>'
                    if isc eq 0 then printf, output, $
                      '<TD></TD><TD></TD><TD></TD>'
                endfor
                printf, output, '</TR>'
;
;  Write out the links to the running difference images, if found.
;
                test = where(cor2_rdiff_latest_file ne nodatajpg, count)
                if count gt 0 then begin
                    printf, output, '<TR>'
                    for isc = 0,1 do begin
                        printf, output, '<TD STYLE="text-align:center">'
                        if cor2_rdiff_latest_file[isc] eq nodatajpg then $
                          entry = '' else entry = 'Diff: '
                        if cor2_rdiff_latest_path[isc] ne '' then $
                          for ires = 0,n_elements(res)-1 do begin
                            sres = ntrim(res[ires])
                            path = concat_dir(cor2_rdiff_latest_path[isc],sres)
                            entry = entry + '<A HREF=' + path + '>' + sres + $
                                    '</A>'
                            if ires lt (n_elements(res)-1) then $
                              entry = entry + ','
                        endfor
                        printf, output, entry
                        printf, output, '</TD>'
                        if isc eq 0 then printf, output, $
                          '<TD></TD><TD></TD><TD></TD>'
                    endfor
                    printf, output, '</TR>'
;
;  Write out the links to the running difference MPEG movies, if found.
;
                    printf, output, '<TR>'
                    caldate = anytim2cal(utc, form=8, /date)
                    for isc = 0,1 do begin
                        printf, output, '<TD STYLE="text-align:center">'
                        mpeg_file = sc[isc] + '_' + caldate + '_cor2_rdiff_' + mres + $
                                    '.mpg'
                        entry = ''
                        if file_exist(concat_dir(filepath,mpeg_file[0])) then begin
                            entry = 'Diff MPEG: '
                            for ires = 0,n_elements(mres)-1 do begin
                                entry = entry + '<A HREF=' + mpeg_file[ires] + $
                                        '>' + mres[ires] + '</A>'
                                if ires lt (n_elements(mres)-1) then $
                                  entry = entry + ','
                            endfor
                        endif
                        printf, output, entry
                        printf, output, '</TD>'
                        if isc eq 0 then printf, output, $
                          '<TD></TD><TD></TD><TD></TD>'
                    endfor
                    printf, output, '</TR>'
                endif           ;One or more COR2 running difference images found
;
                printf, output, '<TR><TD></TD></TR>'
            endif               ;One or more COR2 images found
;
            printf, output, '</TABLE><P>'
;
;  Write out the table with the HI images
;
            test1 = where(hi1_latest_file ne nodatajpg, count1)
            test2 = where(hi2_latest_file ne nodatajpg, count2)
            if (count1+count2) gt 0 then begin
                if utc.mjd ge 57161 then hi_exp_file = 'hi_exp_post.txt' else $
                  hi_exp_file = 'hi_exp.txt'
                openr, hi_exp, concat_dir(stereo_browse,hi_exp_file), /get_lun
                line = 'String'
                while not eof(hi_exp) do begin
                    readf, hi_exp, line
                    printf, output, line
                endwhile
                free_lun, hi_exp
;
                printf, output, '<TABLE CLASS="coords" ' + $
                        'ALIGN=CENTER STYLE="text-align:center">'
                printf, output, '<TR>'
                printf, output, '<TH>STEREO ' + scn[1] + '</TH>'
                printf, output, '<TD WIDTH=20></TD>'
                printf, output, '<TH>STEREO ' + scn[0] + '</TH>'
                printf, output, '</TR>'
;
;  First, do the HI1 images.
;
                if count1 gt 0 then begin
                    printf, output, '<TR>'
                    for isc = 1,0,-1 do begin
                        printf, output, '<TD STYLE="text-align:center">'
                        printf, output, '<IMG SRC="' + $
                                hi1_latest_file[isc] + '" ALT="' + $
                                hi1_latest_file[isc] + '">'
                        printf, output, '</TD>'
                        if isc eq 1 then printf, output, '<TD></TD>'
                    endfor
                    printf, output, '</TR>'
;
;  Write out the links to the JPEG images.
;
                    printf, output, '<TR>'
                    for isc = 1,0,-1 do begin
                        printf, output, '<TD STYLE="text-align:center">'
                        entry = ''
                        if hi1_latest_path[isc] ne '' then $
                          for ires = 1,n_elements(res)-1 do begin
                            sres = ntrim(res[ires])
                            path = concat_dir(hi1_latest_path[isc],sres)
                            entry = entry + '<A HREF=' + path + '>' + sres + $
                                    '</A>'
                            if ires lt (n_elements(res)-1) then $
                              entry = entry + ','
                        endfor
                        printf, output, entry
                        printf, output, '</TD>'
                        if isc eq 1 then printf, output, '<TD></TD>'
                    endfor
                    printf, output, '</TR>'
;
;  Write out the links to the MPEG movies, if found.
;
                    printf, output, '<TR>'
                    caldate = anytim2cal(utc, form=8, /date)
                    for isc = 1,0,-1 do begin
                        printf, output, '<TD STYLE="text-align:center">'
                        mpeg_file = sc[isc] + '_' + caldate + '_hi1_' + mres +$
                                    '.mpg'
                        entry = ''
                        if file_exist(concat_dir(filepath, mpeg_file[0])) $
                          then begin
                            entry = 'MPEG: '
                            for ires = 0,n_elements(mres)-1 do begin
                                entry = entry + '<A HREF=' + mpeg_file[ires] +$
                                        '>' + mres[ires] + '</A>'
                                if ires lt (n_elements(mres)-1) then $
                                  entry = entry + ','
                            endfor
                        endif
                        printf, output, entry
                        printf, output, '</TD>'
                        if isc eq 1 then printf, output, '<TD></TD>'
                    endfor
                    printf, output, '</TR>'
;
;  Write out the links to the running difference images, if found.
;
                    test = where(hi1_rdiff_latest_file ne nodatajpg, count)
                    if count gt 0 then begin
                        printf, output, '<TR>'
                        for isc = 1,0,-1 do begin
                            printf, output, '<TD STYLE="text-align:center">'
                            if hi1_rdiff_latest_file[isc] eq nodatajpg then $
                              entry = '' else entry = 'Diff: '
                            if hi1_rdiff_latest_path[isc] ne '' then $
                              for ires = 1,n_elements(res)-1 do begin
                                sres = ntrim(res[ires])
                                path = concat_dir(hi1_rdiff_latest_path[isc],sres)
                                entry = entry + '<A HREF=' + path + '>' + sres + $
                                        '</A>'
                                if ires lt (n_elements(res)-1) then $
                                  entry = entry + ','
                            endfor
                            printf, output, entry
                            printf, output, '</TD>'
                            if isc eq 1 then printf, output, '<TD></TD>'
                        endfor
                        printf, output, '</TR>'
;
;  Write out the links to the running difference MPEG movies, if found.
;
                        printf, output, '<TR>'
                        caldate = anytim2cal(utc, form=8, /date)
                        for isc = 1,0,-1 do begin
                            printf, output, '<TD STYLE="text-align:center">'
                            mpeg_file = sc[isc] + '_' + caldate + '_hi1_rdiff_' + mres +$
                                        '.mpg'
                            entry = ''
                            if file_exist(concat_dir(filepath, mpeg_file[0])) $
                              then begin
                                entry = 'Diff MPEG: '
                                for ires = 0,n_elements(mres)-1 do begin
                                    entry = entry + '<A HREF=' + mpeg_file[ires] +$
                                            '>' + mres[ires] + '</A>'
                                    if ires lt (n_elements(mres)-1) then $
                                      entry = entry + ','
                                endfor
                            endif
                            printf, output, entry
                            printf, output, '</TD>'
                            if isc eq 1 then printf, output, '<TD></TD>'
                        endfor
                        printf, output, '</TR>'
                    endif       ;One or more HI1 running difference images found
;
;  Write out the links to the star removed images, if found.
;
                    test = where(hi1_srem_latest_file ne nodatajpg, count)
                    if count gt 0 then begin
                        printf, output, '<TR>'
                        for isc = 1,0,-1 do begin
                            printf, output, '<TD STYLE="text-align:center">'
                            if hi1_srem_latest_file[isc] eq nodatajpg then $
                              entry = '' else entry = 'Star removed: '
                            if hi1_srem_latest_path[isc] ne '' then $
                              for ires = 2,n_elements(res)-1 do begin
                                sres = ntrim(res[ires])
                                path = concat_dir(hi1_srem_latest_path[isc],sres)
                                entry = entry + '<A HREF=' + path + '>' + sres + $
                                        '</A>'
                                if ires lt (n_elements(res)-1) then $
                                  entry = entry + ','
                            endfor
                            printf, output, entry
                            printf, output, '</TD>'
                            if isc eq 1 then printf, output, '<TD></TD>'
                        endfor
                        printf, output, '</TR>'
;
;  Write out the links to the star removed MPEG movies, if found.
;
                        printf, output, '<TR>'
                        caldate = anytim2cal(utc, form=8, /date)
                        for isc = 1,0,-1 do begin
                            printf, output, '<TD STYLE="text-align:center">'
                            mpeg_file = sc[isc] + '_' + caldate + '_hi1_srem_' + mres +$
                                        '.mpg'
                            entry = ''
                            if file_exist(concat_dir(filepath, mpeg_file[0])) then begin
                                entry = 'Star rem. MPEG: '
                                for ires=0,n_elements(mres)-1 do begin
                                    entry = entry + '<A HREF=' + mpeg_file[ires] + $
                                            '>' + mres[ires] + '</A>'
                                    if ires lt (n_elements(mres)-1) then entry = entry + ','
                                endfor
                            endif
                            printf, output, entry
                            printf, output, '</TD>'
                            if isc eq 1 then printf, output, '<TD></TD>'
                        endfor
                        printf, output, '</TR>'
                    endif       ;One or more HI1 star removed images found
;
                    printf, output, '<TR><TD></TD></TR>'
                endif           ;One or more HI1 images found
;
;  Next, do the HI2 images.
;
                if count2 gt 0 then begin
                    printf, output, '<TR>'
                    for isc = 1,0,-1 do begin
                        printf, output, '<TD STYLE="text-align:center">'
                        printf, output, '<IMG SRC="' + $
                                hi2_latest_file[isc] + '" ALT="' + $
                                hi2_latest_file[isc] + '">'
                        printf, output, '</TD>'
                        if isc eq 1 then printf, output, '<TD></TD>'
                    endfor
                    printf, output, '</TR>'
;
;  Write out the links to the JPEG images.
;
                    printf, output, '<TR>'
                    for isc = 1,0,-1 do begin
                        printf, output, '<TD STYLE="text-align:center">'
                        entry = ''
                        if hi2_latest_path[isc] ne '' then $
                          for ires = 1,n_elements(res)-1 do begin
                            sres = ntrim(res[ires])
                            path = concat_dir(hi2_latest_path[isc],sres)
                            entry = entry + '<A HREF=' + path + '>' + sres + $
                                    '</A>'
                            if ires lt (n_elements(res)-1) then $
                              entry = entry + ','
                        endfor
                        printf, output, entry
                        printf, output, '</TD>'
                        if isc eq 1 then printf, output, '<TD></TD>'
                    endfor
                    printf, output, '</TR>'
;
;  Write out the links to the MPEG movies, if found.
;
                    printf, output, '<TR>'
                    caldate = anytim2cal(utc, form=8, /date)
                    for isc = 1,0,-1 do begin
                        printf, output, '<TD STYLE="text-align:center">'
                        mpeg_file = sc[isc] + '_' + caldate + '_hi2_' + mres +$
                                    '.mpg'
                        entry = ''
                        if file_exist(concat_dir(filepath, mpeg_file[0])) $
                          then begin
                            entry = 'MPEG: '
                            for ires = 0,n_elements(mres)-1 do begin
                                entry = entry + '<A HREF=' + mpeg_file[ires] +$
                                        '>' + mres[ires] + '</A>'
                                if ires lt (n_elements(mres)-1) then $
                                  entry = entry + ','
                            endfor
                        endif
                        printf, output, entry
                        printf, output, '</TD>'
                        if isc eq 1 then printf, output, '<TD></TD>'
                    endfor
;
                    printf, output, '</TR><TR><TD></TD></TR>'
                endif           ;One or more HI2 images found
;
                printf, output, '</TABLE><P>'
            endif               ;One or more HI images found
;
;  Write out the in-situ pages
;
        end else begin          ;In-situ pages
            printf, output, '<P ALIGN=CENTER>'
            case npages-ipage of
                1: begin        ;7-day
                    printf, output, '<A HREF="' + pages[ipage-2] + '">'
                    printf, output, '1-day in-situ</A>,'
                    printf, output, '<A HREF="' + pages[ipage-1] + '">'
                    printf, output, '3-day in-situ</A><BR>'
                end
                2: begin        ;3-day
                    printf, output, '<A HREF="' + pages[ipage-1] + '">'
                    printf, output, '1-day in-situ</A>,'
                    printf, output, '<A HREF="' + pages[ipage+1] + '">'
                    printf, output, '7-day in-situ</A><BR>'
                end
                3: begin        ;1-day
                    printf, output, '<A HREF="' + pages[ipage+1] + '">'
                    printf, output, '3-day in-situ</A>,'
                    printf, output, '<A HREF="' + pages[ipage+2] + '">'
                    printf, output, '7-day in-situ</A><BR>'
                end
            endcase
;
            printf, output, '<A HREF="' + pages[0] + '">Images</A>'
            printf, output, '</P>'
            printf, output
            printf, output, '<P ALIGN=CENTER>'
            image_file_name = 'beacon_insitu' + suffix[ipage] + '.gif'
            image_file = concat_dir(filepath, image_file_name)
            if file_exist(image_file) then begin
                printf, output, 'IMPACT/PLASTIC solar wind data<BR>'
                printf, output, '<IMG SRC="' + image_file_name + $
                        '" ALT="in-situ">'
            end else if solar_conj then begin
                printf, output
                printf, output, 'The IMPACT, PLASTIC, and SECCHI instruments '
                printf, output, 'on Ahead were turned off for superior solar '
                printf, output, 'conjuction on 20 March 2015.  Data from these '
                printf, output, 'instruments will be unavailable until July '
                printf, output, '2015.<P>'
                printf, output
            end else printf, output, '<H2 ALIGN=CENTER>NOT YET AVAILABLE</H2>'
            printf, output, '</P>'
            printf, output, ''
;
            sep_file_name = 'beacon_sep' + suffix[ipage] + '.gif'
            sep_file = concat_dir(filepath, sep_file_name)
            if file_exist(sep_file) then begin
                printf, output, '<P ALIGN=CENTER>'
                printf, output, 'IMPACT solar energetic particle data</BR>'
                printf, output, '<IMG SRC="' + sep_file_name + $
                        '" ALT="solar energetic particles"></P>'
                printf, output, ''
            endif
;
;  Look for the SWAVES summary file, but only for the 1-day version.
;
            if ipage eq 1 then begin
                ext     = anytim2utc(utc, /external)
                caldate = anytim2cal(utc, form=8, /date)
;
;  Copy over the standard SWAVES summary file.
;
                syear = string(ext.year,format='(I4.4)')
                sw_path = concat_dir(swaves_data, syear)
                swavesfile = 'swaves_summary_' + caldate + '_g.png'
                if file_exist(concat_dir(sw_path, swavesfile)) then begin
                    file_copy, concat_dir(sw_path, swavesfile), $
                      concat_dir(filepath, swavesfile), /overwrite, /force
                endif
;
;  During the side-lobe operations period, copy over the WIND/STEREO summary
;  file.
;
                if caldate ge '20140820' then begin
                    swavesfile = 'wind_stereo_'+caldate+'.png'
                    swavesout = concat_dir(filepath, swavesfile)
                    dirdate = anytim2utc(utc, /ecs, /date_only)
                    spawn, 'wget https://swaves.gsfc.nasa.gov/data/wind_stereo_plots/' + $
                           dirdate + '/' + swavesfile + ' -O ' + swavesout + '.tmp'
                    wait, 3     ;Allow time for spawn to finish
                    if (file_info(swavesout+'.tmp')).size eq 0 then begin
                        file_delete, swavesout+'.tmp'
                        if not file_exist(swavesout) then $
                          swavesfile = 'swaves_summary_' + caldate + '_g.png'
                    end else file_move, swavesout+'.tmp', swavesout, /overwrite
                endif
;
;  If one or the other SWAVES summary files has been copied over, then use it
;  for the outfile file.
;
                if file_exist(concat_dir(filepath, swavesfile)) then begin
                    printf, output, '<P ALIGN=CENTER>'
                    printf, output, 'SWAVES radio data<BR>'
                    printf, output, '<IMG SRC="' + swavesfile + $
                            '" WIDTH=800 ALT="SWAVES">'
                    printf, output, '</P>'
                endif
            endif
        endelse
;
;  Repeat the navigation parameters at the bottom.
;
        printf, output
        printf, output, $
                '<TABLE CLASS="coords" ALIGN=CENTER><TR>'
        printf, output, '<TD>' + spacer + '</TD><TD>' + filler + '</TD>'
        printf, output, '<TD>' + filler + '</TD><TD>' + filler + '</TD>'
        printf, output, '<TD>' + filler + '</TD><TD>' + filler + '</TD>'
        printf, output, '<TD>' + spacer + '</TD></TR><TR>'
        printf, output, '<TD STYLE="text-align:left">' + steppage[0] + '</TD>'
        printf, output, '<TD STYLE="text-align:left">' + steppage[1] + '</TD>'
        printf, output, '<TD STYLE="text-align:left">' + prev + '</TD>'
        printf, output, '<TH STYLE="text-align:center; ' + $
          'font-size:x-large">' + strtrim(anytim2utc(utc,/vms,/date),2) + $
          '</TH>'
        printf, output, '<TD STYLE="text-align:right">' + next + '</TD>'
        printf, output, '<TD STYLE="text-align:right">' + steppage[2] + '</TD>'
        printf, output, '<TD STYLE="text-align:right">' + steppage[3] + $
                '</TD></TR>'
        printf, output, '</TABLE><P>'
    endelse                     ;Not NODATA
;
;  Write out the footer.
;
    openr, footer, concat_dir(stereo_browse,'footer.txt'), /get_lun
    line = 'String'
    while not eof(footer) do begin
        readf, footer, line
        printf, output, line
    endwhile
    free_lun, footer, output
endfor                          ;ipage
;
;  If today was processed, then create redirect pages.
;
if n_params() eq 0 then begin
    outname = ['index.shtml', 'insitu.shtml']
    for iname = 0,1 do begin
        day = anytim2utc(utc, /ext)
        sdate = string(day.year,  format='(I4.4)') + '/' + $
                string(day.month, format='(I2.2)') + '/' + $
                string(day.day,   format='(I2.2)') + '/'
        if iname eq 1 then sdate = sdate + 'insitu.shtml'
        openw, output, concat_dir(stereo_browse, outname[iname]), /get_lun
        printf, output, '<META HTTP-EQUIV=REFRESH CONTENT="0; URL=' + sdate + '">
;
        openr, header, concat_dir(stereo_browse,'header.txt'), /get_lun
        line = 'String'
        while not eof(header) do begin
            readf, header, line
            printf, output, line
        endwhile
        free_lun, header
;
        printf, output, "<P>If you're not automatically redirected,"
        printf, output, 'please follow this <A HREF="' + sdate + '">link</A>'
        printf, output, 'to enter the STEREO browse pages.</P>'
;
        openr, footer, concat_dir(stereo_browse,'footer.txt'), /get_lun
        line = 'String'
        while not eof(footer) do begin
            readf, footer, line
            printf, output, line
        endwhile
        free_lun, footer, output
    endfor
endif
;
;  Make the planet finder subpage.
;
if not keyword_set(nodata) then ssc_make_planet_browse, utc
;
;  If UPDATE_BLDG21_BROWSE is defined, then call the script to copy over the
;  date tree.
;
update_bldg21_browse = getenv('UPDATE_BLDG21_BROWSE')
if update_bldg21_browse ne '' then begin
    if n_params() eq 0 then get_utc, sdate, /ext else $
      sdate = anytim2utc(date, /ext)
    sdate = string(sdate.year, format='(I4.4)') + ' ' + $
            string(sdate.month, format='(I2.2)') + ' ' + $
            string(sdate.day, format='(I2.2)')
    command = update_bldg21_browse + ' ' + sdate
    print, 'Spawning: ' + command
    spawn, command
endif
;
end
