;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_BROWSE_RDIFF_HI1
;
; Purpose     :	Creates HI1 running difference images
;
; Category    :	SECCHI, Quicklook
;
; Explanation : Called from SSC_BROWSE_SECCHI_JPEG to create a running
;               difference version of an HI1 browse image for the SSC website.
;               The relevant image from 2 hours earlier is read in and
;               subtracted from the supplied image.
;
; Syntax      :	SSC_BROWSE_RDIFF_HI1, HEADER1, IMAGE1, BKG, DIFF
;
; Examples    :	See SSC_BROWSE_SECCHI_JPEG
;
; Inputs      :	HEADER1 = Header of image to be subtracted from
;
;               IMAGE1  = Image to be subtracted from.
;
;               BKG     = Background image applied to IMAGE1.  The same
;                         background is applied to IMAGE0.
;
; Opt. Inputs :	None.
;
; Outputs     :	DIFF    = Byte-scaled difference image
;
; Opt. Outputs:	None.
;
; Keywords    :	BEACON  = Set to true if one is processing beacon images.
;
; Calls       :	UTC2TAI, TAI2UTC, PARSE_STEREO_NAME, DATATYPE,
;               SCC_READ_SUMMARY, SCCFINDFITS, SECCHI_PREP
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 22-Nov-2011
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_browse_rdiff_hi1, h1, a1, bkg, diff, beacon=beacon
;
;  Set up the parameters.
;
tai1 = utc2tai(h1.date_obs)
sc = parse_stereo_name(h1.obsrvtry, ['A', 'B'])
tdelta = 900.d0                         ;15 minutes
thalf = tdelta / 2
delvarx, diff
;
;  Find the file(s) from two hour earlier, +/- TDELTA/2.
;
t0 = tai1 - 7200.d0 - thalf
t0 = [t0, t0+tdelta]
utc0 = tai2utc(t0)
list = scc_read_summary(date=utc0, spacecraft=sc, telescope='hi1', $
                        type='img', beacon=beacon)
if datatype(list) ne 'STC' then return
w = where(list.dest eq h1.downlink, count)
if count gt 0 then list = list[w] else return
w = where(strmid(list.filename,16,1) eq 's', count)
if count gt 0 then list = list[w] else return
if list[0].nmiss gt 0 then return       ;Incomplete image
file0 = sccfindfits(list[0].filename, beacon=beacon)
;
;  Read in the earlier file, and form the difference image.
;
sz = size(a1)
nowarp=(keyword_set(beacon) or (h1.date_obs gt '2011-06-27'))
secchi_prep, file0, h0, a0, /desmear_off, /calimg_off, /update_hdr_off, $
  /silent, outsize=sz[1]
if h0.nmissing gt 0 then return         ;Incomplete image(s)
diff = a1 - a0 / bkg
;
;  Scale the image into a byte array.
;
if keyword_set(beacon) then range = 3e-14 else range = 1e-14
diff = bytscl(diff, min=-range, max=range)
;
return
end
