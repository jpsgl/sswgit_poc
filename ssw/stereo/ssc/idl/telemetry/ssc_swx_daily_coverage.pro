;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_SWX_DAILY_COVERAGE
;
; Purpose     :	Keeps track of the daily beacon percent coverage
;
; Category    :	STEREO, SSC, Telemetry
;
; Explanation :	This routine calculates the daily percent coverage of beacon
;               data for the STEREO Ahead and Behind spacecraft.  The coverage
;               is determined from the IMPACT, PLASTIC, and SWAVES beacon
;               packets, which are each sent down once per minute.
;
; Syntax      :	SSC_SWX_DAILY_COVERAGE
;
; Examples    :	SSC_SWX_DAILY_COVERAGE
;               SSC_SWX_DAILY_COVERAGE, NDAYS=10
;               SSC_SWX_DAILY_COVERAGE, NDAYS=0         ;Initialize
;
; Inputs      :	None.
;
; Opt. Inputs :	None.
;
; Outputs     :	The daily percent coverage is written to the file
;               swx_daily_coverage.dat in the directory $STEREO_BEACON_HTML.
;
; Opt. Outputs:	None.
;
; Keywords    :	NDAYS = The number of days to scan looking for beacon data.
;                       The default is the last 10 days.  If set to zero, then
;                       the entire mission is searched--use this option to
;                       initialize the output file.  Subsequent calls update
;                       the output file with the most recent information.
;
; Env. Vars.  : STEREO_SWX_TELEM = Directory containing STEREO beacon
;                                  telemetry, with "ahead" and "behind"
;                                  subdirectories.
;
;               STEREO_BEACON_HTML = Beacon directory on website.
;
; Calls       :	SSC_SWX_DAILY_COVERAGE_READ, READ_STEREO_PKT, IEEE_TO_HOST,
;               PARSE_STEREO_PKT, CONCAT_DIR, GET_UTC, FILE_EXIST, UTC2STR,
;               DATATYPE
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 21-Jun-2011, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_swx_daily_coverage_read, files, mjd
;
;  Step through the files
;
delvarx, mjd, /old
for ifile = 0,n_elements(files)-1 do begin
;
;  Open the file, and read in the first packet.
;
    openr, unit, files[ifile], /get_lun
    read_stereo_pkt, unit, p
;
;  If successful, get the file length, and determine the maximum number of
;  packets that can be in the file.
;
    if datatype(p) eq 'STC' then begin
       stat = fstat(unit)
       npackets = stat.size / stat.cur_ptr
;
;  Go back to the start of the file, and read in all the packets.
;
       point_lun, unit, 0
       p = replicate(p, npackets)
       readu, unit, p
       ieee_to_host, p
;
;  Collect the Modified Julian Day values.
;
       utc = parse_stereo_pkt(p, /pkt_date)
       if n_elements(mjd) eq 0 then mjd = utc.mjd else mjd = [mjd, utc.mjd]
    endif
    free_lun, unit
endfor
;
end   
;
;------------------------------------------------------------------------------
;
pro ssc_swx_daily_coverage, ndays=ndays
;
;  Set up the time range to search.
;
if n_elements(ndays) eq 0 then ndays = 10
if ndays gt 0 then mtime = ' -mtime -' + ntrim(ndays+2) else mtime = ''
;
swx_telem = getenv('STEREO_SWX_TELEM')
swx_ahead  = concat_dir(swx_telem, 'ahead')
swx_behind = concat_dir(swx_telem, 'behind')
;
;  Read in the IMPACT-A files.
;
spawn, 'find ' + swx_ahead + ' -name impact\*.ptp' + mtime, files
ssc_swx_daily_coverage_read, files, mjd_ahead_impact
;
;  Read in the PLASTIC-A files.
;
spawn, 'find ' + swx_ahead + ' -name plastc\*.ptp' + mtime, files
ssc_swx_daily_coverage_read, files, mjd_ahead_plastc
;
;  Read in the SWAVES-A files.
;
spawn, 'find ' + swx_ahead + ' -name swaves\*.ptp' + mtime, files
ssc_swx_daily_coverage_read, files, mjd_ahead_swaves
;
;  Read in the IMPACT-B files.
;
spawn, 'find ' + swx_behind + ' -name impact\*.ptp' + mtime, files
ssc_swx_daily_coverage_read, files, mjd_behind_impact
;
;  Read in the PLASTIC-B files.
;
spawn, 'find ' + swx_behind + ' -name plastc\*.ptp' + mtime, files
ssc_swx_daily_coverage_read, files, mjd_behind_plastc
;
;  Read in the SWAVES-B files.
;
spawn, 'find ' + swx_behind + ' -name swaves\*.ptp' + mtime, files
ssc_swx_daily_coverage_read, files, mjd_behind_swaves
;
;  Determine the start and end MJD values to search for.
;
get_utc, utc
if ndays eq 0 then begin
    mjd = [mjd_ahead_impact, mjd_ahead_plastc, mjd_ahead_swaves, $
           mjd_behind_impact, mjd_behind_plastc, mjd_behind_swaves]
    mjd1 = min(mjd, max=mjd2)
end else begin
    mjd1 = utc.mjd - ndays
    mjd2 = utc.mjd - 1
endelse
;
;  Form the input and output filenames.
infile = concat_dir(getenv('STEREO_BEACON_HTML'), 'swx_daily_coverage.dat')
outfile = infile + '.TEMP'
;
;  Open the output file, and print out the two-line header.
;
openw, out, outfile, /get_lun
printf, out, 'Date            % Ahead     % Behind'
printf, out, ''
;
;  If the input file exists, then open it and skip over the header.
;
if file_exist(infile) then begin
    openr, in, infile, /get_lun
    line = 'string'
    for i=1,2 do readf, in, line
;
;  Read in the previously determined data up to MJD1, and copy it to the output
;  file.
;
    utc.mjd = mjd1 - 1
    date = utc2str(utc, /date_only)
    line = '0000-00-00'
    while strmid(line,0,10) lt date do begin
        readf, in, line
        printf, out, line
    endwhile
    free_lun, in
endif
;
;  Step through the dates, and determine the percentage coverage for each
;  spacecraft.
;
for mjd = mjd1,mjd2 do begin
    dummy = where(mjd_ahead_impact eq mjd, count_impact)
    dummy = where(mjd_ahead_plastc eq mjd, count_plastc)
    dummy = where(mjd_ahead_swaves eq mjd, count_swaves)
    percent_ahead = (count_impact > count_plastc > count_swaves) / 14.4
;
    dummy = where(mjd_behind_impact eq mjd, count_impact)
    dummy = where(mjd_behind_plastc eq mjd, count_plastc)
    dummy = where(mjd_behind_swaves eq mjd, count_swaves)
    percent_behind = (count_impact > count_plastc > count_swaves) / 14.4
;
;  Write the data to the output file.
;
    utc.mjd = mjd
    date = utc2str(utc, /date_only)
    printf, out, date, percent_ahead, percent_behind
endfor
;
;  Close the output file, and replace the old version, if any.
;
free_lun, out
file_move, outfile, infile, /overwrite
;
end
