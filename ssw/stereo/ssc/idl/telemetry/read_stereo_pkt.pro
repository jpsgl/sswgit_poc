PRO READ_STEREO_PKT, UNIT, PACKET, NODATA=NODATA, ERRPAUSE=ERRPAUSE
;+
; Project     :	STEREO - SSC
;
; Name        :	READ_STEREO_PKT
;
; Purpose     :	Read a STEREO packet from a PTP or STP file.
;
; Category    :	STEREO, Telemetry
;
; Explanation :	Reads a single STEREO packet from a PTP or STP file.  The size
;               of the packet, and the type of file is recognized from the
;               ground receipt header at the front.
;
; Syntax      :	READ_STEREO_PKT, UNIT, PACKET
;
; Examples    :	OPENR, UNIT, FILENAME, GET_LUN
;               PACKET = 0
;               WHILE N_ELEMENTS(PACKET) NE 0 DO BEGIN
;                   READ_STEREO_PKT, UNIT, PACKET
;                   ...
;               ENDWHILE
;               FREE_LUN, UNIT
;
; Inputs      :	UNIT = The unit number of the file
;
; Opt. Inputs :	None.
;
; Outputs     :	PACKET = A structure containing the packet.  If no packet could
;                        be read, then this will be undefined.
;
; Opt. Outputs:	None.
;
; Keywords    :	NODATA = If set, then only the packet header information is
;                        returned.
;               ERRPAUSE = If set, then the normal error handling is bypassed.
;                          Only used for debugging.
;
; Calls       :	IEEE_TO_HOST, DELVARX
;
; Common      :	None.
;
; Restrictions:	The file must first be opened for read access.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 08-Jan-2004, William Thompson, GSFC
;               Version 2, 04-Feb-2005, William THompson, GSFC
;                       Add keyword ERRPAUSE
;               Version 3, 02-Mar-2005, William Thompson, GSFC
;                       Calculate application data size from ground receipt
;                       header, to make to read the entire telemetry packet.
;
; Contact     :	WTHOMPSON
;-
;
IF KEYWORD_SET(ERRPAUSE) THEN ON_ERROR,0 ELSE ON_ERROR, 2
;
;  If the end-of-file has been reached, then return packet as undefined.
;
IF EOF(UNIT) THEN BEGIN
    DELVARX, PACKET
    RETURN
ENDIF
;
;  If an I/O error is reached, go to ERROR_POINT.
;
IF NOT KEYWORD_SET(ERRPAUSE) THEN ON_IOERROR, ERROR_POINT
;
;  Read in the ground receipt header.
;
GRH = {SIZE:		0U,	$
       DATATYPE:	0B,	$
       GRT_USAGE:	0B,	$
       GRH_VERSION_ID:	0B,	$
       SPACECRAFT_ID:	0U,	$
       GRT_EPOCH_DAY:	0U,	$
       GRT_MILLISEC:	0UL,	$
       GRT_EXTENDED:	0U,	$
       FRAME_QUAL_FLAG:	0B,	$
       STREAM_ID:	0B,	$
       PROCESS_ID:	0B,	$
       FRONTEND_ID:	0B,	$
       DSN_ANTENNA_ID:	0B,	$
       ENCODE_TYPE:	0B,	$
       DECODE_QUAL:	0B,	$
       PROJECT_FLAGS:	0B,	$
       SPARE: BYTARR(3)}
READU,UNIT,GRH
IEEE_TO_HOST,GRH
;
;  If the End-of-Transmission is reached, then simply read in the remainder of
;  the file and return.
;
IF GRH.DATATYPE EQ 0 THEN BEGIN
    DATASIZE = GRH.SIZE-26
    IF DATASIZE GT 0 THEN BEGIN
        DAT = BYTARR(DATASIZE)
        READU,UNIT,DAT
        PACKET = {GRH: GRH, DAT: DAT}
    END ELSE PACKET = {GRH: GRH}
    RETURN
ENDIF
;
;  If one is reading in a Supplemented Telemetry Packet (STP), then read in the
;  frame information.
;
IF GRH.DATATYPE EQ 2 THEN BEGIN
    FRM = {FRAMESYNC:		0UL,	$
           ID_FIELD:		0U,	$
           MASTERFRAMECOUNT:	0B,	$
           VIRTUALFRAMECOUNT:	0B,	$
           HEADERPTR:		0U,	$
           SECONDARYID:		0B,	$
           SC_MET_TIME_SEC:	0UL,	$
           SC_MET_TIME_SUBSEC:	0B,	$
           SPARES:		BYTARR(9), $
           OP_CONTROL:		0UL,	$
           FRAME_ERR_CONTROL:	0U}
    READU, UNIT, FRM
    IEEE_TO_HOST,FRM
ENDIF
;
;  Read in the telemetry packet header.
;
PKT = {HDR:	0U,	$
       GRP:     0U,     $
       SIZE:	0U,	$
       SECONDS:	0UL,	$
       SUBSEC:	0B}
READU,UNIT,PKT
IEEE_TO_HOST,PKT
;
;  Read in the application data.  Base the size on the ground receipt header
;  instead of the size in the packet header, to make sure to read the entire
;  packet.
;
;;DATASIZE = PKT.SIZE-4
;
IF GRH.DATATYPE EQ 2 THEN DATASIZE = GRH.SIZE-68 ELSE DATASIZE = GRH.SIZE-37
IF DATASIZE GT 0 THEN BEGIN
    DAT = BYTARR(DATASIZE)
    READU,UNIT,DAT
END ELSE DAT = 0B
;
;  Depending on what was read in, form the structure to return.
;
IF KEYWORD_SET(NODATA) THEN BEGIN
    IF GRH.DATATYPE EQ 2 THEN PACKET = {GRH: GRH, FRM: FRM, PKT: PKT} ELSE $
                              PACKET = {GRH: GRH, PKT: PKT}
END ELSE BEGIN
    IF GRH.DATATYPE EQ 2 THEN   $
            PACKET = {GRH: GRH, FRM: FRM, PKT: PKT, DAT: DAT} ELSE $
            PACKET = {GRH: GRH, PKT: PKT, DAT: DAT}
ENDELSE
RETURN
;
;  An error occured.  Return PACKET as undefined.
;
ERROR_POINT:
MESSAGE, /CONTINUE, 'Unexpected end-of-file encountered'
DELVARX, PACKET
END
