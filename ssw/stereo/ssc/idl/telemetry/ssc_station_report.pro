;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_STATION_REPORT
;
; Purpose     :	Produce beacon station report for previous month
;
; Category    :	STEREO, Operations
;
; Explanation :	This procedure counts the number of unique beacon telemetry
;               packets collected by each ground station for the previous
;               month, and prints out a short report.
;
; Syntax      :	SSC_STATION_REPORT
;
; Examples    :	SSC_STATION_REPORT
;
; Inputs      :	None.
;
; Opt. Inputs :	None.
;
; Outputs     :	The file station_report.txt is written to the directory
;               $STEREO_BEACON_HTML.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Env. Vars.  :	STEREO_BEACON_HTML = Location of beacon web pages
;
;               STEREO_SWX_HEADER  = Location of beacon telemetry header files
;
; Calls       :	GET_UTC, UTC2TAI, CONCAT_DIR, DELVARX, READ_STEREO_PKT,
;               PARSE_STEREO_PKT, ANYTIM2UTC
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 16-Jul-2009, William Thompson, GSFC
;               Version 2, 26-Oct-2010, WTT, use DELVARX, /OLD
;               Version 3, 16-Jul-2014, WTT, add KSWC and APL
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_station_report
;
stations = [1,14,15,16,17,18,19,20]
names = ['DSN', 'Chilbolton', 'Koganei', 'Toulouse','Kiel-Ronne','Bochum',$
         'KSWC','APL']
nstations = n_elements(stations)
;
months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', $
          'August', 'September', 'October', 'November', 'December']
get_utc, utc, /external
year = utc.year
month = utc.month - 1
if month eq 0 then begin
    year = year - 1
    month = 12
endif
;
;  Create the output file.
;
openw, out, concat_dir(getenv('STEREO_BEACON_HTML'), 'station_report.txt'), $
  /get_lun
printf, out, 'STEREO beacon station report for ' + months[month-1] + ', ' + $
  ntrim(year)
printf, out, ''
;
;  Step through the spacecraft, and print out the spacecraft name'
;
stereo_swx_header = getenv('STEREO_SWX_HEADER')
sc = ['ahead','behind']
for isc = 0,1 do begin
    printf, out, 'Packet statistics for ' + strupcase(sc[isc])
    printf, out, ''
    path = concat_dir(stereo_swx_header, sc[isc])
    path = concat_dir(path, ntrim(year))
    path = concat_dir(path, string(month,format='(I2.2)'))
    files = file_search(concat_dir(path, '*.ptp.hdr'), count=count)
    delvarx, packets, /old
    if count gt 0 then for ifile=0,count-1 do begin
        print, files[ifile]
        openr, unit, files[ifile], /get_lun
;
;  First, count the packets in the file.
;
        on_ioerror, count_done
        n_packets = 0L
        while not eof(unit) do begin
            size = 0u
            readu, unit, size
            ieee_to_host, size
            size = size - 261
            b = bytarr(size-2)
            readu, unit, b
            n_packets = n_packets + 1
        endwhile
;
;  Now read the packets.
;
count_done:
        close, unit
        openr, unit, files[ifile]
        on_ioerror, read_done
        read_stereo_pkt, unit, packet, /header_only
        if n_packets gt 1 then p = replicate(packet, n_packets) else p = packet
        i_packet = 0L
        while n_elements(packet) ne 0 do begin
            read_stereo_pkt, unit, packet, /header_only
            if n_elements(packet) ne 0 then begin
                i_packet = i_packet + 1
                if packet.grh.datatype ne 0 then begin
                    if n_packets gt 1 then begin
                        if i_packet ge n_packets then goto, read_done
                        if packet.grh.datatype eq p[0].grh.datatype then $
                          p[i_packet] = packet else p[i_packet].grh.size=0
                    end else p = [p, packet]
                end else p[i_packet].grh.size=0
            endif
        endwhile
;
read_done:
        free_lun, unit
        if n_elements(packets) eq 0 then packets = p else $
          packets = [packets, p]
    endfor                      ;ifile
;
;  Filter out packets with incorrect times.
;
    get_utc, utc1
    tai1 = utc2tai(utc1)
    tai0 = utc2tai('2006-Oct-26')
    tai = parse_stereo_pkt(packets, /pkt_date, /tai)
    w = where((tai ge tai0) and (tai le tai1))
    packets = packets[w]
    tai = tai[w]
;
;  Sort the packets by time, and get the start time.
;
    s = sort(tai)
    packets = packets[s]
    tai = tai[s]
    tai0 = tai[0]
;
;  Step through all the station IDs.
;
    ids = all_vals(packets.grh.dsn_antenna_id)
    for i=0,n_elements(ids)-1 do begin
        w = where(packets.grh.dsn_antenna_id eq ids[i])
        diff = tai[w]-tai0
        test = diff + packets[w].pkt.grp / 16777216d0
        u = uniq(test)
        diff = diff[u]
;
        w = where(ids[i] eq stations, count)
        if count gt 0 then title = names[w[0]] else $
          title = 'Unknown station ID ' + ntrim(ids[i])
        printf, out, format="(A25, I10, ' packets')", title, n_elements(diff)
    endfor
    printf, out, ''
endfor                          ;isc
;
free_lun, out
end
