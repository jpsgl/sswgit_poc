;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_WRITE_APL
;
; Purpose     :	Write out Archived Product List, and archive telemetry
;
; Category    :	STEREO, Operations, Telemetry
;
; Explanation :	This procedure reads the rsync log files to create an archived
;               product list.  The log files are parsed to find the names of
;               level 0 telemetry files, and lists are generated for each
;               spacecraft.  These lists are then written to Archived Product
;               List files, as described in the STEREO MOC/POC/SSC ICD.
;
;               Simultaneously, the telemetry file is copied to a directory
;               tree organized by instrument, year and month.
;
; Syntax      :	SSC_WRITE_APL  [, LOG_FILES ]
;
; Inputs      :	None required
;
; Opt. Inputs :	LOG_FILES = An array of rsync log files to parse.  Ordinarily,
;                           the log files are determined automatically.  This
;                           optional input is mainly for testing.
;
; Outputs     :	The APL files are written to disk.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Env. vars.  : SSC_MOC_SDS_TOP = Top directory for MOC ingest
;               SSC_MOC_SDS_LOG = Directory containing rsync log files
;               SSC_MOC_SDS_APL_A = Output directory for Ahead APL files
;               SSC_MOC_SDS_APL_B = Output directory for Behind APL files
;               SSC_TELEMETRY = Top directory for final telemetry archive
;
; Calls       :	GET_UTC, ANYTIM2CAL, CONCAT_DIR, BOOST_ARRAY, DELVARX, UTC2DOY
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 9 February 2005, William Thompson, GSFC
;               Version 2, 11 August 2005, William Thompson, GSFC
;                       Bug corrections, split Ahead and Behind APL files
;               Version 3, 1-Nov-2005, William Thompson, GSFC
;                       Also archive the telemetry file
;                       Delete APL files if identical to previous files
;               Version 4, 14-Aug-2006, William Thompson, GSFC
;                       Ignore "deleting ..." lines
;               Version 5, 17-Aug-2006, William Thompson, GSFC
;                       Include instrument as part of the file organization
;               Version 6, 22-Feb-2007, William Thompson, GSFC
;                       Don't copy files already in the archive
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_write_apl, log_files
on_error, 2
;
;  Get the input and output directories.  
;
moc_sds_top = getenv('SSC_MOC_SDS_TOP')
if moc_sds_top eq '' then message, 'SSC_MOC_SDS_TOP not defined'
moc_sds_log = getenv('SSC_MOC_SDS_LOG')
if moc_sds_log eq '' then message, 'SSC_MOC_SDS_LOG not defined'
moc_sds_apl_a = getenv('SSC_MOC_SDS_APL_A')
if moc_sds_apl_a eq '' then message, 'SSC_MOC_SDS_APL_A not defined'
moc_sds_apl_b = getenv('SSC_MOC_SDS_APL_B')
if moc_sds_apl_b eq '' then message, 'SSC_MOC_SDS_APL_B not defined'
ssc_telemetry = getenv('SSC_TELEMETRY')
if ssc_telemetry eq '' then message, 'SSC_TELEMETRY not defined'
;
;  Find the relevant rsync log files over the last day.  To make sure that all
;  the relevant data is processed, start from the beginning of yesterday.
;
if n_elements(log_files) eq 0 then begin
    get_utc, today
    yesterday = today
    yesterday.mjd = yesterday.mjd - 1
    today = anytim2cal(today,form=8,/date)
    yesterday = anytim2cal(yesterday,form=8,/date)
    filename = concat_dir(moc_sds_log, 'moc_sds_rsync_' + today + '*.log')
    files = file_search(filename, count=count)
    if count gt 0 then boost_array, log_files, files
    filename = concat_dir(moc_sds_log, 'moc_sds_rsync_' + yesterday + '*.log')
    files = file_search(filename, count=count)
    if count gt 0 then boost_array, log_files, files
endif
if n_elements(log_files) eq 0 then message, 'Log files not found'
log_files = log_files[where(log_files ne '')]
;
;  Make sure that the output arrays for the two spacecraft are initially empty.
;
delvarx, files_a, files_b, sizes_a, sizes_b
;
;  Step through and open each of the log files.
;
for i_file = 0,n_elements(log_files)-1 do begin
    openr, unit, log_files[i_file], /get_lun
;
;  Define the target directory to look for.
;
    target = 'data_products/level_0_telemetry'
;
;  Step through the file line by line.  Ignore lines for deleted files.
;
    line = 'String'
    while not eof(unit) do begin
        readf, unit, line
        if strmid(line,0,9) ne 'deleting ' then begin
;
;  Find the first slash character in the string, and determine whether or not
;  it stars with "ahead" or "behind".
;
            slash = strpos(line, '/')
            if slash gt 1 then sc = strmid(line, 0, slash) else sc = ''
            if (sc eq 'ahead') or (sc eq 'behind') then begin
;
;  Test if the line refers to the target directory, and it's not itself a
;  directory.
;
                if (strmid(line, slash+1, strlen(target)) eq target) and $
                  (strmid(line, strlen(line)-1, 1) ne '/') then begin
;
;  Get the size of the file, in bytes.
;
                    telem_file = concat_dir(moc_sds_top, line)
                    openr, lun, telem_file, /get_lun
                    size = (fstat(lun)).size
                    free_lun, lun
;
;  Extract the filename, without the directory.
;
                    last_slash = strpos(line, '/', /reverse_search)
                    name = strmid(line, last_slash+1, $
                                  strlen(line)-last_slash-1)
;
;  Append it to the list for spacecraft A or spacecraft B.
;
                    case sc of
                        'ahead': begin
                            boost_array, files_a, name
                            boost_array, sizes_a, size
                        end
                        'behind': begin
                            boost_array, files_b, name
                            boost_array, sizes_b, size
                        end
                    endcase
;
;  Copy the telemetry file into it's proper location.  Start by parsing the
;  filename to extract the instrument.  Make sure that the instrument directory
;  exists.
;
                    words = str_sep(name,'_')
                    instrument = words[0]
                    archive = concat_dir(ssc_telemetry, sc)
                    archive = concat_dir(archive, instrument)
                    if not dir_exist(archive) then mk_dir, archive
;
;  Next parse, the filename to extract the date.  If the filename can't be
;  parsed, the file is written to the directory 0000/00.
;
                    errmsg = ''
                    date = str2utc(words[2] + '-' + words[3], /external, $
                                   errmsg=errmsg)
                    if errmsg eq '' then begin
                        year = date.year
                        month = date.month
                    end else begin
                        year = 0
                        month = 0
                    endelse
;
;  Make sure that the year directory exists.
;
                    archive = concat_dir(archive, string(year, $
                                                         format='(I4.4)'))
                    if not dir_exist(archive) then mk_dir, archive
;
;  Make sure that the month directory exists
;
                    archive = concat_dir(archive, string(month, $
                                                         format='(I2.2)'))
                    if not dir_exist(archive) then mk_dir, archive
;
;  Copy the telemetry file to the archive.
;
                    if not file_exist(concat_dir(archive,telem_file)) then $
                      begin
                        command = 'cp -fp ' + telem_file + ' ' + archive
                        print, command
                        spawn, command
                    endif
                endif           ;not a directory
            endif               ;sc = ahead or behind
        endif                   ;not "deleting "
    endwhile
    free_lun, unit
endfor
;
;  Form the date string for the output filename, based on today's date.
;
get_utc, utc, /external
doy = utc2doy(utc)
date = string(utc.year, format='(I4.4)') + '_' + string(doy, format='(I3.3)')
;
;  First process the entries for the Ahead spacecraft.
;
if n_elements(files_a) gt 0 then begin
;
;  Remove any duplicate filenames.
;
    s = sort(files_a)
    files_a = files_a[s]
    sizes_a = sizes_a[s]
    u = uniq(files_a)
    files_a = files_a[u]
    sizes_a = sizes_a[u]
;
;  Find any existing APL files for the current date.  If any are found, then
;  extract the highest index number, and increment by 1.
;
    apl_files = 'sta_' + date + '_????.apl'
    files = file_search( concat_dir(moc_sds_apl_a, apl_files), count=count)
    if count gt 0 then begin
        s = sort(files)
        files = files(reverse(s))
        file = files[0]
        underscore = strpos(file, '_', /reverse_search)
        index = fix(strmid(file, underscore+1, 4)) + 1
    end else index = 1
;
;  Open the output file, and write out the header.
;
    apl_file = concat_dir(moc_sds_apl_a, 'sta_' + date + '_' + $
                          string(index, format='(I4.4)') + '.apl')
    openw, unit, apl_file, /get_lun
    printf, unit, 'Archived Product List'
    printf, unit, 'STEREO AHEAD'
    printf, unit, string(utc.year, format='(I4.4)') + ':' +     $
      string(doy, format='(I3.3)')
    printf, unit, ''
;
;  For each file, print a line containing the filename and the size, separated
;  by one or more spaces.  The specification doesn't require it, but we will
;  try to get the names and sizes into nicely arranged columns.
;
    tabpos = (max(strlen(files_a)) + 5) > 35
    format = '(A,T' + ntrim(tabpos) + ',I15)'
    for i = 0,n_elements(files_a)-1 do  $
      printf, unit, files_a[i], sizes_a[i], format=format
    free_lun, unit
;
;  Check to see if the file is identical to the previously generated APL, if
;  any.
;
    if index gt 1 then begin
        prev_file = concat_dir(moc_sds_apl_a, 'sta_' + date + '_' + $
                               string(index-1, format='(I4.4)') + '.apl')
        spawn, 'cmp ' + apl_file + ' ' + prev_file, result
        if (n_elements(result) eq 1) and (result[0] eq '') then begin
            print, 'Deleting '+apl_file
            file_delete, apl_file
        endif
    endif
endif
;
;  Do the same for the Behind spacecraft 
;
if n_elements(files_b) gt 0 then begin
;
;  Remove any duplicate filenames.
;
    s = sort(files_b)
    files_b = files_b[s]
    sizes_b = sizes_b[s]
    u = uniq(files_b)
    files_b = files_b[u]
    sizes_b = sizes_b[u]
;
;  Find any existing APL files for the current date.  If any are found, then
;  extract the highest index number, and increment by 1.
;
    apl_files = 'stb_' + date + '_????.apl'
    files = file_search( concat_dir(moc_sds_apl_b, apl_files), count=count)
    if count gt 0 then begin
        s = sort(files)
        files = files(reverse(s))
        file = files[0]
        underscore = strpos(file, '_', /reverse_search)
        index = fix(strmid(file, underscore+1, 4)) + 1
    end else index = 1
;
;  Open the output file, and write out the header.
;
    apl_file = concat_dir(moc_sds_apl_b, 'stb_' + date + '_' + $
                          string(index, format='(I4.4)') + '.apl')
    openw, unit, apl_file, /get_lun
    printf, unit, 'Archived Product List'
    printf, unit, 'STEREO BEHIND'
    printf, unit, string(utc.year, format='(I4.4)') + ':' +     $
      string(doy, format='(I3.3)')
    printf, unit, ''
;
;  For each file, print a line containing the filename and the size, separated
;  by one or more spaces.  The specification doesn't require it, but we will
;  try to get the names and sizes into nicely arranged columns.
;
    tabpos = (max(strlen(files_b)) + 5) > 35
    format = '(A,T' + ntrim(tabpos) + ',I15)'
    for i = 0,n_elements(files_b)-1 do  $
      printf, unit, files_b[i], sizes_b[i], format=format
    free_lun, unit
;
;  Check to see if the file is identical to the previously generated APL, if
;  any.
;
    if index gt 1 then begin
        prev_file = concat_dir(moc_sds_apl_b, 'stb_' + date + '_' + $
                               string(index-1, format='(I4.4)') + '.apl')
        spawn, 'cmp ' + apl_file + ' ' + prev_file, result
        if (n_elements(result) eq 1) and (result[0] eq '') then begin
            print, 'Deleting '+apl_file
            file_delete, apl_file
        endif
    endif
endif
;
end
