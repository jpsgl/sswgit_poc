;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_REMOVE_OLD_EPHEM
;
; Purpose     :	Clear out ephemeris files that are no longer needed.
;
; Category    :	STEREO, coordinates
;
; Explanation :	This routine scans through the ephemerides to find files which
;               are not referenced, and which are more than 60 days old, and
;               deletes them.  The 60-day test is to make sure that enough time
;               has passed that one can be sure the file is not needed.
;
; Syntax      :	SSC_REMOVE_OLD_EPHEM
;
; Examples    :	SSC_REMOVE_OLD_EPHEM
;
; Inputs      :	None.
;
; Opt. Inputs :	None.
;
; Outputs     :	None.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Calls       :	DELVARX, BOOST_ARRAY, CONCAT_DIR, BREAK_FILE
;
; Common      :	None.
;
; Env. Vars.  : Uses the environment variables STEREO_SPICE_W,
;               STEREO_SPICE_EPHEM_W, STEREO_SPICE_DEF_EPHEM_W,
;               STEREO_SPICE_ATTIT_SM_W, STEREO_SPICE_ATTITUDE_W
;
; Restrictions:	The above environment variables must be defined--there are no
;               defaults.
;
; Side effects:	Currently, the predictive ephemerides which start with "ahead"
;               or "behind" are unaffected.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 23-Jul-2007, William Thompson, GSFC
;               Version 2, 24-Jul-2007, WTT, changed 30 days to 60 days
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_remove_old_ephem
on_error, 2
;
;  Get the current system time.  This will be compared below to the file
;  modification times.
;
now = systime(1)
;
;  Get the environment variables.
;
stereo_spice = getenv('STEREO_SPICE_W')
if stereo_spice eq '' then message, 'STEREO_SPICE_W not defined'
stereo_spice_ephem = getenv('STEREO_SPICE_EPHEM_W')
if stereo_spice_ephem eq '' then message, 'STEREO_SPICE_EPHEM_W not defined'
stereo_spice_def_ephem = getenv('STEREO_SPICE_DEF_EPHEM_W')
if stereo_spice_def_ephem eq '' then message, 'STEREO_SPICE_DEF_EPHEM_W not defined'
stereo_spice_attit_sm = getenv('STEREO_SPICE_ATTIT_SM_W')
if stereo_spice_attit_sm eq '' then message, 'STEREO_SPICE_ATTIT_SM_W not defined'
stereo_spice_attitude = getenv('STEREO_SPICE_ATTITUDE_W')
if stereo_spice_attitude eq '' then message, 'STEREO_SPICE_ATTITUDE_W not defined'
;
;  Define the variables describing the three types of ephemeris directories.
;
paths = [stereo_spice_ephem, stereo_spice_def_ephem, stereo_spice_attitude]
datfiles = ['ephemerides_', 'definitive_ephemerides_', 'attitude_history_']
pattern = ['ST*.epm.bsp', '*.depm.bsp', '*_2*.ah.bc']
sc = ['ahead','behind']
for itype = 0,2 do begin
    for isc = 0,1 do begin
;
;  Read in the files which are currently being used.
;
        datfile = concat_dir(stereo_spice, datfiles[itype] + sc[isc] + '.dat')
        openr, unit, datfile, /get_lun
        delvarx, used_files
        line = 'string'
        while not eof(unit) do begin
            readf, unit, line
            boost_array, used_files, strtrim(line,2)
        endwhile
        free_lun, unit
;
;  Find all the files in the directory, and check against the list which has
;  been read in.
;
        path = concat_dir(paths[itype], sc[isc])
        files = file_search(concat_dir(path, pattern[itype]))
        break_file, files, disk, dir, name, ext
        names = strtrim(name + ext, 2)
        for ifile=0,n_elements(names)-1 do begin
            w = where(names[ifile] eq used_files, count)
            if count eq 0 then begin
;
;  Get the age of the file, and delete if older than 60 days.
;
                openr, temp, files[ifile], /get_lun
                stat = fstat(temp)
                free_lun, temp
                ndays = (now - stat.mtime) / 60.^2 / 24
                if ndays gt 60 then begin
                    print, 'Deleting ' + files[ifile]
                    file_delete, files[ifile]
;
;  If attitude history, then also delete the smoothed version.
;
                    if itype eq 2 then begin
                        smfile = concat_dir(stereo_spice_attit_sm, sc[isc])
                        smfile = concat_dir(smfile, name[ifile] + '_sm.ah.bc')
                        print, 'Deleting ' + smfile
                        file_delete, smfile, /allow_nonexistent
                    endif
                endif
            endif
        endfor                  ;ifile
;
    endfor                      ;isc
endfor                          ;itype
;
end
