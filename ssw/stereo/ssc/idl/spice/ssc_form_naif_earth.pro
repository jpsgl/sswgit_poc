;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_FORM_NAIF_EARTH
;
; Purpose     :	Form list of Earth PCK files for ITRF93 coordinates
;
; Category    :	STEREO, Orbit
;
; Explanation :	Generates a list of Earth PCK files used to support ITRF93
;               coordinates needed to generate beacon station ephemerides.
;
; Syntax      :	SSC_FORM_NAIF_EARTH
;
; Examples    :	SSC_FORM_NAIF_EARTH
;
; Inputs      :	None.
;
; Opt. Inputs :	None.
;
; Outputs     :	The kernel list is initially written with the extension
;               ".new" instead of ".dat".  If this list is different than the
;               previous list, then it replaces the previous list.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Calls       :	CONCAT_DIR, BREAK_FILE, MAIL, GET_TEMP_DIR, MK_TEMP_DIR
;
; Common      :	None.
;
; Env. vars.  : STEREO_SPICE_W = Directory containing the stereo_kernels
;                                database.
;
;               SSC_NOTIFY = Email address to send problem reports to
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 14-Jun-2010, William Thompson, GSFC
;               Version 2, 02-Oct-2018, WTT, modified to mirror files due to
;                       shift from FTP to HTTP
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_form_naif_earth
;
;  Start by copying over the pck directory from the NAIF server to a temporary
;  directory.
;
mk_temp_dir, get_temp_dir(), temp1
cd, current=current, temp1
command = 'wget -nH -L -r -N -erobots=off --cut-dirs=4 --level=1 ' + $
          'https://naif.jpl.nasa.gov/pub/naif/generic_kernels/pck/'
print, command
spawn, command
cd, current
;
;  Filter the earth*.bpc files into a second temporary directory, and remove
;  the first temporary directory.
;
mk_temp_dir, get_temp_dir(), temp2
command = 'rsync -aWuv ' + concat_dir(temp1,'earth*.bpc') + ' ' + temp2
print, command
spawn, command
file_delete, temp1, /quiet, /recursive
;
;  If the second temporary directory isn't empty, then mirror the files
;  into SSW.  Remove the second temporary directory.
;
stereo_spice_earth = concat_dir(getenv('STEREO_SPICE_W'), 'earth')
files = file_search(temp2, '*.bpc', count=count)
if count gt 0 then begin
    command = 'rsync -aWuv --delete ' + concat_dir(temp2,'') + ' ' + $
              stereo_spice_earth
    print, command
    spawn, command
endif
file_delete, temp2, /quiet, /recursive
;
;  Get a list of all the .bpc files in the Earth directory.
;
files = file_search(concat_dir(stereo_spice_earth, '*.bpc'))
stop
;
;  Open the output file.
;
filename = concat_dir(getenv('STEREO_SPICE_W'), 'naif_earth')
openw, unit, filename + '.new', /get_lun
;
;  Determine the filename lengths.
;
break_file, files, disk, dir, name
len = strlen(name)
nfiles = 0
;
;  The historical file should have the format earth_720101_YYMMDD.bpc.
;
w = where(len eq 19, count)
if count gt 0 then begin
    printf, unit, name[w[0]] + '.bpc'
    nfiles = nfiles + 1
endif
;
;  The long-term prediction file should have the format
;  earth_YYMMDD_YYMMDD_predict.bpc.
;
w = where(len eq 27, count)
if count gt 0 then begin
    printf, unit, name[w[0]] + '.bpc'
    nfiles = nfiles + 1
endif
;
;  The latest file should have the format earth_YYMMDD_YYMMDD_YYMMDD.bpc.
;
w = where(len eq 26, count)
if count gt 0 then begin
    printf, unit, name[w[0]] + '.bpc'
    nfiles = nfiles + 1
endif
;
free_lun, unit
;
;  Compare the two files, and change to the new file if different from the old
;  one.
;
openr, unit1, filename + '.dat', /get_lun
openr, unit2, filename + '.new', /get_lun
test = 0
line1 = 'string'
line2 = 'string'
while (not eof(unit1)) and (not eof(unit2)) do begin
    readf, unit1, line1
    readf, unit2, line2
    test = test or (line1 ne line2)
endwhile
test = test or (not eof(unit1))
test = test or (not eof(unit2))
free_lun, unit1, unit2
if test then begin
    if nfiles eq 3 then begin
        print, 'mv -f ' + filename + '.dat ' + filename + '.old'
        spawn, 'mv -f ' + filename + '.dat ' + filename + '.old'
    endif
    print, 'mv -f ' + filename + '.new ' + filename + '.dat'
    spawn, 'mv -f ' + filename + '.new ' + filename + '.dat'
end else spawn, 'rm -f ' + filename + '.new'
;
;  If the new file doesn't contain three lines, then generate an error message.
;
if nfiles ne 3 then mail, users=ssc_notify, $
  subj='Problem with file naif_earth.dat'
;
end
