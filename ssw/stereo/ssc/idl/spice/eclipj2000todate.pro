;+
; Project     :	STEREO - SSC
;
; Name        :	ECLIPJ2000TODATE
;
; Purpose     :	Precess J2000 ecliptic coordinates
;
; Category    :	Orbit
;
; Explanation :	Precess coordinates derived from the SPICE ECLIPJ2000 reference
;               frame to the mean ecliptic of date.
;
; Syntax      :	ECLIPJ2000TODATE, ET, STATE0  [, STATE1 ]
;
; Examples    :	cspice_spkezr,target,et,'ECLIPJ2000','LT+S','Earth',state,ltime
;               eclipj2000todate,et,state
;
; Inputs      :	ET      = Ephemeris time, in dynamical seconds since J2000.
;               STATE0  = Either a three-value position or velocity vector, or
;                         a six-value state vector containing both position and
;                         velocity information.
;
; Opt. Inputs :	None.
;
; Outputs     :	None required.
;
; Opt. Outputs:	STATE1  = The precessed state vector.  If not passed, then
;                         STATE0 is returned as the precessed vector.
;
; Keywords    :	INVERT  = If set, then the inverse transformation is performed,
;                         from the mean ecliptic of date to the ECLIPJ2000
;                         reference frame.
;
; Calls       :	CSPICE_EUL2M
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	Derived from software written by M. Fraenz, Feb 2000
;
; History     :	Version 1, 11-May-2004, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
pro eclipj2000todate, et, state0, state1, invert=k_invert
;
on_error, 2
if n_params() lt 2 then message, $
        'Syntax:  ECLIPJ2000TODATE, ET, STATE0  [, STATE1 ]'
;
;  Make sure that the input parameters have the right sizes.
;
if n_elements(et) ne 1 then message,'ET must be scalar'
if (n_elements(state0) ne 3) and (n_elements(state0) ne 6) then message, $
        'STATE0 must be have either 3 or 6 elements'
;
;  Convert ephemeris time to Julian centuries since J2000.
;
t = et / 3155760000.D0
;
;  Calculate the inclination (small_pi), ascending node longitude (large_pi),
;  and the differences in the angular distances (p) of the vernal equinoxes
;  from the ascending node.
;
small_pi = poly(t,[47.0029d,-0.03302d,0.000060d])*t
large_pi = poly(t,[(174*60d0+52)*60d0+34.982d,-869.8089d,0.03536d])
p = poly(t,[5029.0966d,1.11113d,-0.000006d])*t
;
;  Calculate the Eulerian rotation angles.
;
arcsec2rad = !dpi / (180d * 3600d)
phi   = large_pi      * arcsec2rad
theta = small_pi      * arcsec2rad
omega = (-p-large_pi) * arcsec2rad
;
;  Calculate the trigonometric terms.
;
sphi = sin(phi)  &  stheta = sin(theta)  &  somega = sin(omega)
cphi = cos(phi)  &  ctheta = cos(theta)  &  comega = cos(omega)
;
;  Form the Euler matrix.
;
e = [[cphi*comega-sphi*somega*ctheta,           $       
      cphi*somega+sphi*comega*ctheta,           $
      sphi*stheta],                             $
     [-sphi*comega-cphi*somega*ctheta,          $
      -sphi*somega+cphi*comega*ctheta,          $
      cphi*stheta],                             $
     [somega*stheta, -comega*stheta, ctheta]]
if keyword_set(k_invert) then e = invert(e,/double)
;
;  Apply the Euler matrix to the first part of the state vector.
;
state1 = state0
state1[0:2] = e ## state0[0:2]
;
;  If necessary, also process the second part.
;
if n_elements(state0) eq 6 then state1[3:5] = e ## state0[3:5]
;
;  If only two parameters were passed, then replace STATE0 with STATE1.
;
if n_params() eq 2 then state0 = state1
end
