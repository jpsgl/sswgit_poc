;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_AH_REDUCE
;
; Purpose     :	Create reduced attitude history file
;
; Category    :	STEREO, Orbit
;
; Explanation :	This procedure takes a full resolution attitude history file,
;               and creates a reduced resolution version, with "_sm" appended
;               to the file name.  Part of the MOC data products ingestion
;               procedure.
;
;               Currently, this procedure works using CKSMRG, which produces
;               crude CK files.  A future version will produce smoothed CK
;               files.
;
; Syntax      :	SSC_AH_REDUCE, FILENAME
;
; Examples    :	ssc_ah_reduce, 'ahead_2006_304_01.ah.bc'
;
;               Creates file ahead_2006_304_01_sm.ah.bc in the appropriate
;               directory.
;
; Inputs      :	FILENAME = Name of the full resolution attitude history file.
;
; Opt. Inputs :	None.
;
; Outputs     :	The procedure creates a reduced resolution file in the
;               appropriate directory.
;
; Opt. Outputs:	None.
;
; Keywords    :	ERRMSG = If defined and passed, then any error messages will be
;                        returned to the user in this parameter rather than
;                        depending on the MESSAGE routine in IDL.  If no errors
;                        are encountered, then a null string is returned.  In
;                        order to use this feature, ERRMSG must be defined
;                        first, e.g.
;
;                               ERRMSG = ''
;                               SSC_AH_REDUCE, ERRMSG=ERRMSG
;                               IF ERRMSG NE '' THEN ...
;
; Calls       :	CONCAT_DIR, GET_STEREO_SPICE_RANGE, BREAK_FILE, FILE_EXIST
;
; Common      :	None.
;
; Env. Vars.  : CKSMRG                  = Path to cksmrg program
;               STEREO_SPICE_GEN        = Generic STEREO SPICE kernel tree
;               STEREO_SPICE_SCLK       = Spacecraft clock files
;               STEREO_SPICE_ATTIT_SM_W = Output tree
;
; Restrictions:	Must have write privilege to output directory
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 05-Jul-2006, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_ah_reduce, filename, errmsg=errmsg
on_error, 2
;
;  Check the input parameter.  Make sure that the file actually exists.
;
if n_params() eq 0 then begin
    message = 'Syntax: SSC_AH_REDUCE, FILENAME'
    goto, handle_error
endif
if n_elements(filename) ne 1 then begin
    message = 'FILENAME must be a scalar string'
    goto, handle_error
endif
if not file_exist(filename) then begin
    message = 'File ' + filename + ' does not exist'
    goto, handle_error
endif
;
;  Get the path to the cksmrg program.
;
cksmrg = getenv('CKSMRG')
if cksmrg eq '' then begin
    message = 'CKSMRG not defined'
    goto, handle_error
endif
;
;  Form a list of the generic kernels which are needed.
;
stereo_spice_gen = getenv('STEREO_SPICE_GEN')
if !version.os_family eq 'Windows' then $
  stereo_spice_gen = concat_dir(stereo_spice_gen, 'dos')
files = file_search( concat_dir(stereo_spice_gen, 'naif*.tls'), count=count)
if count eq 0 then begin
    message = 'Unable to find leap-seconds file'
    goto, handle_error
endif
kernels = max(files)
;
stereo_spice_sclk = getenv('STEREO_SPICE_SCLK')
ahead = concat_dir(stereo_spice_sclk, 'ahead')
if !version.os_family eq 'Windows' then ahead = concat_dir(ahead, 'dos')
files = file_search( concat_dir(ahead, 'ahead_science_*.sclk'), count=count)
if count eq 0 then begin
    message = 'Unable to find spacecraft clock file'
    goto, handle_error
endif
kernels = kernels + ' ' + max(files)
;
behind = concat_dir(stereo_spice_sclk, 'behind')
if !version.os_family eq 'Windows' then behind = concat_dir(behind, 'dos')
files = file_search( concat_dir(behind, 'behind_science_*.sclk'), count=count)
if count eq 0 then begin
    message = 'Unable to find spacecraft clock file'
    goto, handle_error
endif
kernels = kernels + ' ' + max(files)
;
;  Get the spacecraft ID, and translate it into a string name.
;
get_stereo_spice_range, filename, date0, date1, scid
case scid of
    -234000: sc = 'Ahead'
    -235000: sc = 'Behind'
    else: begin
        message = 'Unrecognized Spacecraft ID ' + ntrim(scid)
        goto, handle_error
    endelse
endcase
;
;  Get the path to write the file.  Append the spacecraft name.
;
path = getenv('STEREO_SPICE_ATTIT_SM_W')
if path eq '' then begin
    message = 'STEREO_SPICE_ATTIT_SM_W not defined'
    goto, handle_error
endif
path = concat_dir(path, strlowcase(sc))
;
;  Form the name of the output file.
;
break_file, filename, disk, dir, name, ext
outname = concat_dir(path, name + '_sm' + ext)
;
;  If the output file already exists, then append ".save" to the end.
;
if file_exist(outname) then begin
    tempname = outname + '.save'
    file_move, outname, tempname
end else tempname = ''
;
;  Form the CKSMRG command, and execute it.
;
command = cksmrg + ' -K ' + kernels + ' -I ' + filename + ' -O ' + outname + $
  ' -S "STEREO ' + sc + ' spacecraft bus - merged"' + $
  ' -F "Downsampled attitude history" -B ' + ntrim(scid) + $
  ' -R J2000 -A KEEP -T 5 minutes -D 10 arcseconds'
print, command
spawn, command
;
;  If a file was created, then delete the temporary file.  Otherwise, move it
;  back to where it was.
;
if tempname ne '' then begin
    if file_exist(outname) then file_delete, tempname else $
      file_move, tempname, outname
endif
;
;  Return to the calling routine.
;
return
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message, /continue else $
  errmsg = 'SSC_AH_REDUCE: ' + message
;
end
