;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_OPLOT_PARKER
;
; Purpose     :	Overplot Parker spiral on orbital plots
;
; Category    :	Coordinates, Orbit
;
; Explanation :	Overplots a simple model for the Parker spiral on an existing
;               orbital plot.
;
; Syntax      :	SSC_OPLOT_PARKER  [, keywords ]
;
; Examples    :	See SSC_PLOT_WHERE
;
; Inputs      :	None.
;
; Opt. Inputs :	None.
;
; Outputs     :	The parker spiral is overplotted onto the existing plot.
;
; Opt. Outputs:	None.
;
; Keywords    :	SPEED   = The solar wind speed, in km/sec.  Default=450.
;               ROT     = The solar rotation rate in radians/sec.  Def=2.7e-6.
;               AU      = Plot scale in AU.  Default is kilometers.
;               DELTA   = Spacing between Parker spirals, in degrees.  Def=10.
;
;               Also accepts any keywords used by OPLOT.
;
; Calls       :	None.
;
; Common      :	None.
;
; Restrictions:	The plot must be heliocentric.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 27-Jul-2006, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_oplot_parker, speed=speed, rot=rot, delta=delta, au=au, _extra=_extra
on_error, 2
;
if n_elements(speed) eq 0 then speed = 450      ;km/sec
if keyword_set(au) then vel = speed / 1.4959787D8 else vel = speed
if n_elements(rot) eq 0 then rot = 2.7e-6       ;Radians/sec
;
scale = 3 * max(abs([!x.crange,!y.crange]))
r = scale * findgen(2000) / 2000.
;
if n_elements(delta) eq 0 then delta = 10
n_delta = round(360./delta)
;
for i=0,n_delta do begin
    theta = (!pi/180.d0)*i*delta - r*rot/vel
    x = r*cos(theta)
    y = r*sin(theta)
    oplot, x, y, _extra=_extra
endfor
;
end
