;+
; Project     :	STEREO - SSC
;
; Name        :	STEREO_POINTING_DEMO
;
; Purpose     :	Demonstration of reading STEREO SPICE CK kernel
;
; Category    :	STEREO, Orbit
;
; Explanation :	This procedure demonstrates the process of deriving the
;               attitude of the two STEREO spacecraft in various standard
;               coordinate systems.  The sample attitude data  is taken from
;               Mission Simulation #1, and thus is relatively limited in time.
;               Rather than ask the user for a sample input time, this
;               demonstration program plots the derived roll, pitch, and yaw
;               for the total 5 hour duration of the test.
;
; Syntax      :	STEREO_POINTING_DEMO, SYSTEM  [, UTC, ROLL, PITCH, YAW ]
;
; Examples    :	stereo_pointing_demo, 'GSE'
;
; Inputs      :	SYSTEM     = Character string, giving one of the following
;                            standard coordinate systems: "GEI", "GEO", "GSE",
;                            "HCI", "HAE", "HEE", "HEEQ", "Carrington",
;                            "HGRTN", "SCPNT"
;
; Opt. Inputs :	None.
;
; Outputs     :	The roll, pitch, and yaw values are plotted.
;
; Opt. Outputs:	UTC     = The date/time values.
;               ROLL    = The roll values, defined as a counter-clockwise
;                         rotation about the spacecraft X axis, in degrees.
;               PITCH   = The pitch values, defined as a counter-clockwise
;                         rotation about the spacecraft Z axis, in degrees.
;               YAW     = The yaw values, defined as a counter-clockwise
;                         rotation about the spacecraft Y axis, in degrees.
;
; Keywords    :	PRECESS = If set, then ecliptic coordinates are precessed from
;                         the J2000 reference frame to the mean ecliptic of
;                         date.  Only used for HAE.  Default is PRECESS=0.
;                         GSE and HEE use the ecliptic of date by definition.
;
; Calls       :	ANYTIM2UTC, CONCAT_DIR, CSPICE_FURNSH, CSPICE_SPKEZR,
;               CSPICE_UNLOAD
;
; Common      :	None.
;
; Restrictions:	This procedure works in conjunction with the Icy/CSPICE
;               package, which is implemented as an IDL Dynamically Loadable
;               Module (DLM).  The Icy source code can be downloaded from
;
;                       ftp://naif.jpl.nasa.gov/pub/naif/toolkit/IDL
;
;               Because this uses dynamic frames, it requires Icy/CSPICE
;               version N0058 or higher.
;
; Side effects:	None.
;
; Prev. Hist. :	Based on STEREO_STATE_DEMO
;
; History     :	Version 1, W. Thompson, 20 June 2005
;               Version 2, W. Thompson, 30 June 2005
;                       Include HGRTN, SCPNT
;
; Contact     :	WTHOMPSON
;-
;
pro stereo_pointing_demo, system, utc, roll, pitch, yaw, precess=precess
;
on_error, 2
if n_params() lt 1 then message, $
        'Syntax:  STEREO_POINTING_DEMO, SYSTEM  [, UTC, ROLL, PITCH, YAW ]'
;
;  The sample attitude history file is for the ahead spacecraft, which has the
;  ID number of -234.  In the pointing file, this appears as instrument code
;  number -234000.
;
sc = -234
inst = sc*1000L
;
;  Define the path to the demo files.
;
datapath = concat_dir('$SSW', 'stereo', /DIR)
datapath = concat_dir(datapath, 'ssc', /DIR)
datapath = concat_dir(datapath, 'data', /DIR)
datapath = concat_dir(datapath, 'spice_demo', /DIR)
;
;  Define the files which need to be loaded.  The STEREO Ahead attitude history
;  file is taken from Mission Simulation #1.  To support reading this file,
;  several other files must also be loaded.  The file 'naif0007.tls' contains
;  leap second information used to support the conversion from UTC to the
;  ephemeris time (ET) used by SPICE.  The file 'ahead_science_03.sclk' is the
;  spacecraft clock file, and is used to convert between the onboard clock time
;  used by the attitude history file, and UTC or ET.  The planetary orbit
;  ephemeris file, 'de405.bsp', is the same file used in generating the STEREO
;  ephemeris, and is required to put the STEREO attitude data into the various
;  standard coordinate systems.  Solar and planetary physical data are stored
;  in 'pck00007.tpc', and are used to support the "IAU_EARTH" and "IAU_SUN"
;  reference frames.  Except for the spacecraft clock file, these supporting
;  files were downloaded from
;
;	ftp://naif.jpl.nasa.gov/pub/naif/generic_kernels
;
;  Finally, the heliospheric.tf file is a dynamics frame definition file for
;  many of the standard coordinate systems.  Although developed for the STEREO
;  project, it is applicable to all heliospheric missions.
;
tm_file = concat_dir(datapath, 'naif0007.tls')          ;Leapseconds
de_file = concat_dir(datapath, 'de405.bsp')             ;Planetary orbits
pl_file = concat_dir(datapath, 'pck00007.tpc')          ;Planetary phys. data
fr_file = concat_dir(datapath, 'heliospheric.tf')       ;Heliospheric frames
rt_file = concat_dir(datapath, 'stereo_rtn.tf')         ;STEREO frames
sc_file = concat_dir(datapath, 'ahead_science_03.sclk') ;Spacecraft clock
bc_file = concat_dir(datapath, 'ahead_2006_070_00.ah.bc')
sp_file = concat_dir(datapath, 'ahead_2006_042_p2primary_01.epm.bsp')
;
;  Load the files
;
cspice_furnsh, tm_file
cspice_furnsh, de_file
cspice_furnsh, pl_file
cspice_furnsh, fr_file
cspice_furnsh, rt_file
cspice_furnsh, sc_file
cspice_furnsh, bc_file
cspice_furnsh, sp_file
;
;  Step through the time range in units of one minute.
;
tai0 = anytim2tai('2006-Mar-11T10:03')
tai1 = anytim2tai('2006-Mar-11T15:31')
n_minutes = 1 + (tai1 - tai0) / 60.d0
pitch = dblarr(n_minutes)
yaw = dblarr(n_minutes)
roll = dblarr(n_minutes)
utc = strarr(n_minutes)
for i=0,n_minutes-1 do begin
    tai = tai0 + 60.d0*i
;
;  Convert the date/time to ephemeris time, and then to spacecraft clock double
;  precision time.
;
    utc[i] = tai2utc(tai,/ccsds)
    cspice_str2et, utc[i], et
    cspice_sce2c, sc, et, sclkdp
;
;  Based on the coordinate system requested, get the CK matrix.
;
    tol = 1000
    case strupcase(system) of
        'GEI': cspice_ckgp, inst, sclkdp, tol, 'J2000', cmat, clkout, found
        'GEO': cspice_ckgp, inst, sclkdp, tol, 'IAU_EARTH', cmat, clkout, found
        'GSE': cspice_ckgp, inst, sclkdp, tol, 'GSE', cmat, clkout, found
        'HCI': cspice_ckgp, inst, sclkdp, tol, 'HCI', cmat, clkout, found
        'HAE': begin
            if keyword_set(precess) then frame='ECLIPDATE' else $
              frame='ECLIPJ2000'
            cspice_ckgp, inst, sclkdp, tol, frame, cmat, clkout, found
        endcase
        'HEE': cspice_ckgp, inst, sclkdp, tol, 'HEE', cmat, clkout, found
        'HEEQ': cspice_ckgp, inst, sclkdp, tol, 'HEEQ', cmat, clkout, found
        'CARRINGTON': cspice_ckgp, inst, sclkdp, tol, 'IAU_SUN', cmat, $
          clkout, found
        'HGRTN': cspice_ckgp,inst,sclkdp,tol,'STAHGRTN',cmat,clkout,found
        'SCPNT': cspice_ckgp,inst,sclkdp,tol,'STASCPNT',cmat,clkout,found
        else: message,'Unrecognized coordinate system'
    endcase
;
    if not found then message, /continue, 'Date not found'
    if clkout ne sclkdp then print,clkout-sclkdp
;
;  Get the rotation angles.
;
    cspice_m2eul, cmat, 1, 3, 2, rolli, pitchi, yawi
    pitch[i] = pitchi * (180.d0 / !dpi)
    yaw[i]   = yawi   * (180.d0 / !dpi)
    roll[i]  = rolli  * (180.d0 / !dpi)
endfor
;
;  Determine the best range for the yaw values.
;
ayaw = average_most(abs(yaw))
if ayaw gt 90 then begin
    w = where(yaw lt 0, n_found)
    if n_found gt 0 then yaw[w] = yaw[w] + 360.d0
endif
;
;  Plot the roll, pitch, and yaw values.
;
erase
setview, 1, 1, 1, 3
utplot, utc, roll, /yno, ytitle='Roll', title=strupcase(system)
oplot,!x.crange,[0,0],line=1
setview, 1, 1, 2, 3
utplot, utc, pitch, /yno, ytitle='Pitch'
oplot,!x.crange,[0,0],line=1
setview, 1, 1, 3, 3
utplot, utc, yaw, /yno, ytitle='Yaw'
oplot,!x.crange,[0,0],line=1
if ayaw gt 90 then oplot,!x.crange,[180,180],line=1
setview
;
;  Unload the files
;
cspice_unload, sp_file
cspice_unload, bc_file
cspice_unload, sc_file
cspice_unload, fr_file
cspice_unload, pl_file
cspice_unload, de_file
cspice_unload, tm_file
;
end
