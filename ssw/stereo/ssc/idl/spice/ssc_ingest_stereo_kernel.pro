;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_INGEST_STEREO_KERNEL
;
; Purpose     :	Ingest STEREO SPICE kernels into SolarSoft
;
; Category    :	STEREO, Orbit
;
; Explanation :	Scans the most recent rsync log files looking for ephemeris and
;               attitude history files.  These files are copied to the
;               appropriate directory within the SolarSoft tree, converted to
;               binary format, and registered in the database.
;
; Syntax      :	SSC_INGEST_STEREO_KERNEL  [, LOG_FILES ]
;
; Examples    :	SSC_INGEST_STEREO_KERNEL
;
; Inputs      :	None required.
;
; Opt. Inputs :	LOG_FILES = An array of rsync log files to parse.  Ordinarily,
;                           the log files are determined automatically.  This
;                           optional input is mainly for testing.
;
; Outputs     :	Information about the SPICE kernel is stored in the
;               stereo_kernels.db* database files.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Calls       :	GET_UTC, ANYTIM2CAL, CONCAT_DIR, DIR_EXIST, MK_DIR, RM_FILE,
;               FILE_EXIST, SSC_REGISTER_STEREO_KERNEL
;
; Common      :	None.
;
; Env. vars.  : SSC_MOC_SDS_TOP = Top directory for MOC ingest
;               SSC_MOC_SDS_LOG = Directory containing rsync log files
;               STEREO_SPICE_W  = Top of tree for SPICE file ingest
;               STEREO_SPICE_EPHEM_W     = Directory to write .epm.bsp files
;               STEREO_SPICE_DEF_EPHEM_W = Directory to write .depm.bsp files
;               STEREO_SPICE_ATTITUDE_W  = Directory to write .ah.bc files
;               SSC_NOTIFY = Email address to send problem reports to
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 23-Feb-2006, William Thompson, GSFC
;               Version 2, 11-Jul-2006, William Thompson, GSFC
;                       Added call to SSC_AH_REDUCE
;               Version 3, 14-Aug-2006, William Thompson, GSFC
;                       Ignore "deleting ..." lines
;               Version 4, 1-Nov-2006, William Thompson, GSFC
;                       Scan all log files from yesterday and today.
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_ingest_stereo_kernel, log_files
on_error, 2
;
;  Get the input and output directories.  
;
moc_sds_top = getenv('SSC_MOC_SDS_TOP')
if moc_sds_top eq '' then message, 'SSC_MOC_SDS_TOP not defined'
moc_sds_log = getenv('SSC_MOC_SDS_LOG')
if moc_sds_log eq '' then message, 'SSC_MOC_SDS_LOG not defined'
stereo_spice = getenv('STEREO_SPICE_W')
if stereo_spice eq '' then message, 'STEREO_SPICE_W not defined'
stereo_spice_ephem = getenv('STEREO_SPICE_EPHEM_W')
if stereo_spice_ephem eq '' then message, 'STEREO_SPICE_EPHEM_W not defined'
stereo_spice_def_ephem = getenv('STEREO_SPICE_DEF_EPHEM_W')
if stereo_spice_def_ephem eq '' then message, $
  'STEREO_SPICE_DEF_EPHEM_W not defined'
stereo_spice_attitude = getenv('STEREO_SPICE_ATTITUDE_W')
if stereo_spice_attitude eq '' then message, $
  'STEREO_SPICE_ATTITUDE_W not defined'
ssc_notify = getenv('SSC_NOTIFY')
;
;  Find the relevant rsync log files over the last day.  To make sure that all
;  the relevant data is processed, start from the beginning of yesterday.
;
if n_elements(log_files) eq 0 then begin
    get_utc, today
    yesterday = today
    yesterday.mjd = yesterday.mjd - 1
    today = anytim2cal(today,form=8,/date)
    yesterday = anytim2cal(yesterday,form=8,/date)
    filename = concat_dir(moc_sds_log, 'moc_sds_rsync_' + today + '*.log')
    files = file_search(filename, count=count)
    if count gt 0 then boost_array, log_files, files
    filename = concat_dir(moc_sds_log, 'moc_sds_rsync_' + yesterday + '*.log')
    files = file_search(filename, count=count)
    if count gt 0 then boost_array, log_files, files
endif
if n_elements(log_files) eq 0 then message, 'Log files not found'
log_files = log_files[where(log_files ne '')]
;
;  Define the data types to look for.
;
target = ['.ah.xc', '.depm.xsp', '.epm.xsp']
directory = [stereo_spice_attitude, stereo_spice_def_ephem, stereo_spice_ephem]
;
;  Step through and open each of the log files.
;
for i_file = 0,n_elements(log_files)-1 do begin
    openr, unit, log_files[i_file], /get_lun
;
;  Step through the file line by line.  Ignore lines for deleted files.
;
    line = 'String'
    while not eof(unit) do begin
        readf, unit, line
        if strmid(line,0,9) ne 'deleting ' then begin
;
;  Find the first slash character in the string, and determine whether or not
;  it starts with "ahead" or "behind".
;
            slash = strpos(line, '/')
            if slash gt 1 then sc = strmid(line, 0, slash) else sc = ''
            if (sc eq 'ahead') or (sc eq 'behind') then begin
;
;  Make sure the next directory is "data_products", and that the line does not
;  refer to a logical link.
;
                if (strmid(line, slash, 15) eq '/data_products/') and $
                  (strpos(line,'->') eq -1) then begin
;
;  Test if the line refers to one of the targeted data products.
;
                    pos = -1
                    for itarget=0,n_elements(target)-1 do begin
                        test = strpos(line, target[itarget])
                        if test gt -1 then begin
                            pos = test
                            archive = directory[itarget]
                        endif
                    endfor
                    if pos gt -1 then begin
;
;  Extract the filename, without the directory.
;
                        lastslash = strpos(line, '/', /reverse_search)
                        name = strmid(line, lastslash+1, $
                                      strlen(line)-lastslash-1)
;
;  Copy the ephemeris file into its proper location.  If it already exists,
;  then skip over this file.
;
                        if not dir_exist(archive) then mk_dir, archive
                        archive = concat_dir(archive, sc)
                        if not dir_exist(archive) then mk_dir, archive
                        kernel = concat_dir(archive, name)
                        pos = strpos(kernel, '.x', /reverse_search)
                        newkernel = kernel
                        strput, newkernel, '.b', pos
                        if not file_exist(newkernel) then begin
                            fileorig = concat_dir(moc_sds_top, line)
                            command = 'cp -fp ' + fileorig + ' ' + archive
                            print, command
                            spawn, command
;
;  Convert the file into binary form, and remove the intermediate transfer
;  file.
;
                            command = "perl -pi -e 's/\r\n/\n/;' " + kernel
                            print, command
                            spawn, command
                            command = "/usr/local/icy/exe/tobin " + kernel
                            print, command
                            spawn, command, output
                            rm_file, kernel
;
;  Register the file in the database.
;
                            kernel = newkernel
                            if file_exist(kernel) then begin
                                errmsg = ''
                                ssc_register_stereo_kernel, kernel, fileorig, $
                                  errmsg=errmsg
                                if errmsg ne '' then begin
                                    print, errmsg
                                    if ssc_notify ne '' then $
                                      mail, users=ssc_notify, $
                                      subj='Problem registering file ' + $
                                      kernel, errmsg
                                endif
                            end else begin
                                message, /continue, 'Problem creating file ' +$
                                  kernel
                                for iout=0,n_elements(output)-1 do $
                                  print, output[iout]
                                if ssc_notify ne '' then mail, output, $
                                  users=ssc_notify, $
                                  subj='Problem creating file ' + kernel
                            endelse
;
;  If an attitude history file, create the reduced resolution version.
;
                            if strpos(line, target[0]) gt -1 then $
                              ssc_ah_reduce, kernel
;
                        endif   ;File already exists
                    endif       ;Targeted file
                endif           ;Within data_products
            endif               ;sc = ahead or behind
        endif                   ;not "deleting "
    endwhile
    free_lun, unit
endfor
;
end
