;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_MAKE_VIEWPERIODS
;
; Purpose     :	Write out viewperiod files for beacon stations
;
; Category    :	STEREO, Operations, Orbit
;
; Explanation :	This procedure writes out files containing the daily rise and
;               set times for the two STEREO spacecraft as seen from each of
;               the ground stations.
;
; Syntax      :	SSC_MAKE_VIEWPERIODS
;
; Inputs      :	None.
;
; Opt. Inputs :	STARTDATE = Date to start from.  The default is to start from
;                           today.  This is used only for test purposes.
;
; Outputs     :	The viewperiod file is written to disk with a filename
;               generated from the spacecraft and antenna station, e.g.
;
;                       ahead_koganei.view
;
; Opt. Outputs:	None.
;
; Keywords    :	ERRMSG = If defined and passed, then any error messages will be
;                        returned to the user in this parameter rather than
;                        depending on the MESSAGE routine in IDL.  If no errors
;                        are encountered, then a null string is returned.  In
;                        order to use this feature, ERRMSG must be defined
;                        first, e.g.
;
;                               ERRMSG = ''
;                               SSC_MAKE_VIEWPERIODS, ERRMSG=ERRMSG, ...
;                               IF ERRMSG NE '' THEN ...
;
; Env. vars.  : SSC_VIEWPERIODS_W = Directory to write viewperiod files to.
;
; Calls       :	SSC_MAKE_VIEWPERIODS_AZEL, ANYTIM2UTC, CSPICE_STR2ET,
;               CSPICE_SPKEZR, CSPICE_SXFORM, CSPICE_RECLAT, INTERPOL,
;               LOAD_STEREO_SPICE_EARTH, GET_UTC, UTC2TAI, CSPICE_BODVAR,
;               CONCAT_DIR, BREAK_FILE, CSPICE_FURNSH, CSPICE_GEOREC, TAI2UTC,
;               CSPICE_UNLOAD
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	The SPICE kernels will be loaded, if not loaded already.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 16-Nov-2006, William Thompson, GSFC
;               Version 2, 20-Nov-2006, William Thompson, GSFC
;                       Redefine azimuth to be measured clockwise.
;               Version 3, 07-Dec-2006, William Thompson, GSFC
;                       Corrected calculation of station location
;               Version 4, 12-Jan-2007, William Thompson, GSFC
;                       Added optional STARTDATE
;               Version 5, 14-Jun-2010, William Thompson, GSFC
;                       Use ITRF93 instead of IAU_EARTH
;
; Contact     :	WTHOMPSON
;-
;
;------------------------------------------------------------------------------
;
pro ssc_make_viewperiods_azel, sc, date, topo, origin, az, elev, $
                               az_mask, elev_mask, elev0
;
;  Convert the time value into ephemeris time as used by SPICE.
;
utc = anytim2utc(date, /ccsds)
cspice_str2et, utc, et
;
;  Get the position of the spacecraft in geocentric coordinates, and subtract
;  the origin.  Convert to the station topographical frame.
;
cspice_spkezr, sc, et, 'ITRF93', 'lt+s', 'Earth', state, ltime
state = state - origin
cspice_sxform, 'ITRF93', topo, et, xform
state = transpose(xform) # state
;
;  Convert into azimuth and elev.  Redefine azimuth to advance clockwise.
;
cspice_reclat, state[0:2], distance, az, elev
az   = -az  * (180.d0 / !dpi)
elev = elev * (180.d0 / !dpi)
;
;  Interpolate the elevation mask to the selected azimuth.
;
if az lt 0 then az = az + 360.d0
elev0 = interpol(elev_mask, az_mask, az)
;
return
end
;
;------------------------------------------------------------------------------
;
pro ssc_make_viewperiods, startdate, errmsg=errmsg, _extra=_extra
;
;  Make sure that the SPICE kernels are loaded.
;
message = ''
load_stereo_spice_earth, errmsg=message, _extra=_extra
if message ne '' then goto, handle_error
;
;  Get today's date, and a date 30 days in the future.
;
if n_elements(startdate) eq 1 then utc0 = anytim2utc(startdate) else $
  get_utc, utc0
utc0.time = 0
utc1 = utc0
utc1.mjd = utc1.mjd + 30
;
;  Convert to TAI.
;
tai0 = utc2tai(utc0)
tai1 = utc2tai(utc1)
;
;  Get Earth's radii, and calculate the oblateness parameter FLAT.
;
cspice_bodvar, 399, 'RADII' , radii
flat = (radii[0]-radii[2]) / radii[0]
;
;  Set up the spacecraft names, both for use in SPICE (scs), and for forming
;  the filename (fname).
;
scs = ['STEREO AHEAD','STEREO BEHIND']
fname = ['ahead','behind']
;
;  Form the path to the input data, and find all the station frame files.  Use
;  this to form a list of stations.
;
datpath = concat_dir( getenv('SSW_SSC'), 'data/spice/stations')
files = file_search( concat_dir(datpath, '*.tf') )
break_file, files, disk, dir, stations
;
;  Step through the stations, and load the topographic frame for that station.
;
for istation = 0,n_elements(stations)-1 do begin
    station = stations[istation]
    cspice_furnsh, files[istation]
    topo = strupcase(station) + '_TOPO'
;
;  Get the origin from the stations.dat file.
;
    openr, in, concat_dir(datpath, 'stations.dat'), /get_lun
    line = 'string'
    done = 0
    while not (done or eof(in)) do begin
        readf, in, line
        line = strcompress(strtrim(line,2))
        words = strsplit(line, ' ', /extract)
        if words[0] eq strlowcase(station) then begin
            lat_r = double(words[1]) * !dpi / 180.d0
            lon_r = double(words[2]) * !dpi / 180.d0
            alt   = double(words[3]) / 1000.d0 ;Convert meters -> kilometers
            done = 1
        endif
    endwhile
    free_lun, in
    if not done then begin
        message = 'Station ' + station + ' not found in stations.dat'
        goto, handle_error
    endif
;
    cspice_georec, lon_r, lat_r, alt, radii[0], flat, origin
    origin = [origin, 0, 0, 0]
;
;  Read in the horizon mask information.
;
    openr, in, concat_dir(datpath, station + '.mask')
    vals = dblarr(2)
    readf, in, vals
    az_mask   = vals[0]
    elev_mask = vals[1]
    while not eof(in) do begin
        readf, in, vals
        az_mask   = [az_mask,   vals[0]]
        elev_mask = [elev_mask, vals[1]]
    endwhile
    free_lun, in
;
;  Step through the spacecraft, and open the output file.
;
    for isc = 0,1 do begin
        sc = scs[isc]
        filename = fname[isc] + '_' + station + '.view'
        filename = concat_dir('SSC_VIEWPERIODS_W', filename)
        openw, out, filename, /get_lun
;
;  Print out a header.
;
        printf, out, 'Station: ' + strupcase(station)
        printf, out, 'Target:  ' + sc
        get_utc, now, /ccsds
        printf, out, 'Creation date: ' + now
        printf, out, 'Start time: ' + anytim2utc(utc0, /ccsds)
        printf, out, 'End   time: ' + anytim2utc(utc1, /ccsds)
        printf, out, ''
        printf, out, $
          'Rise time (UTC)            Set time (UTC)      Duration (hours)'
        printf, out, ''
;
;  Get the initial elevation.  If above the horizon, keep moving forward until
;  it's below the horizon.
;
        tai = tai0
        ssc_make_viewperiods_azel, sc, tai, topo, origin, az, elev, $
          az_mask, elev_mask, elev0
        while elev ge elev0 do begin
            tai = tai + 600.d0
            ssc_make_viewperiods_azel, sc, tai, topo, origin, az, elev, $
              az_mask, elev_mask, elev0
        endwhile
;
;  Keep looking for viewperiodds until we pass the end time.
;
        while tai lt tai1 do begin
;
;  Find the next point where the spacecraft appears above the horizon.
;
            while elev lt elev0 do begin
                t0 = tai
                tai = tai + 600.d0
                ssc_make_viewperiods_azel, sc, tai, topo, origin, az, elev, $
                  az_mask, elev_mask, elev0
            endwhile
            t1 = tai
;
;  Use binary search to find the rise time to the nearest second.
;
            while (t1-t0) ge 1 do begin
                tai = (t0 + t1) / 2.d0
                ssc_make_viewperiods_azel, sc, tai, topo, origin, az, elev, $
                  az_mask, elev_mask, elev0
                if elev lt elev0 then t0 = tai else t1 = tai
            endwhile
            tai_rise = (t0+t1) / 2.d0
;
;  Find the next point where the spacecraft appears below the horizon.
;
            repeat begin
                t0 = tai
                tai = tai + 600.d0
                ssc_make_viewperiods_azel, sc, tai, topo, origin, az, elev, $
                  az_mask, elev_mask, elev0
            endrep until elev lt elev0
            t1 = tai
;
;  Use binary search to find the set time to the nearest second.
;
            while (t1-t0) ge 1 do begin
                tai = (t0 + t1) / 2.d0
                ssc_make_viewperiods_azel, sc, tai, topo, origin, az, elev, $
                  az_mask, elev_mask, elev0
                if elev lt elev0 then t1 = tai else t0 = tai
            endwhile
            tai_set = (t0+t1) / 2.d0
;
;  Calculate the pass duration, and filter out abnormally short passes.  Also
;  filter out passes which start past the end time.
;
            delta = (tai_set-tai_rise) / 3600.d0
            if (delta gt 0.1) and (tai_rise lt tai1) then printf, out, $
              tai2utc(tai_rise,/ccsds) + '    ' + $
              tai2utc(tai_set ,/ccsds) + '    ' + ntrim(delta)
;
        endwhile                ;Time within search range
        free_lun, out
        print, 'Created file ' + filename
    endfor                      ;Step through spacecraft
;
    cspice_unload, files[istation]
endfor                          ;Step through stations
;
return
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message else $
  errmsg = 'ssc_make_viewperiods: ' + message
;
end
