;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_MAKE_STATION
;
; Purpose     :	Write out ephemeris files for the next week.
;
; Category    :	STEREO, Operations, Orbit
;
; Explanation :	This procedure writes out the various ground station ephemeris
;               files for the next week, and removes files relating to the
;               past.
;
; Syntax      :	SSC_MAKE_STATION
;
; Inputs      :	None.
;
; Opt. Inputs :	None.
;
; Outputs     :	Various ephemeris files are written into appropriate
;               directories.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Env. vars.  : SSC_STATIONS_W = Directory to write station files to.  Each
;               station will have a subdirectory under this main directory.
;
;               SSC_STATIONS_OEM_W = Directory to write OEM files to.
;
;               SSC_STATIONS_IIRV_W = Directory to write IIRV files to.
;
; Calls       :	GET_UTC, SSC_WRITE_OEM, SSC_WRITE_IIRV, SSC_WRITE_STATION,
;               SSC_WRITE_STATION_RA_DEC, CONCAT_DIR
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	The SPICE kernels will be loaded, if not loaded already.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 14-Nov-2006, William Thompson, GSFC
;               Version 2, 20-Nov-2006, William Thompson, GSFC
;                       Rename Abingdon to Chilbolton
;               Version 3, 15-Dec-2006, William Thompson, GSFC
;                       Add Fairbanks
;               Version 4, 11-Jan-2007, William Thompson, GSFC
;                       Also write RA/DEC version for Koganei
;               Version 5, 20-Feb-2009, WTT, Add Kiel, Bochum
;               Version 6, 02-Dec-2011, WTT, Add Beijing
;               Version 7, 07-Apr-2014, WTT, Add KSWC in Korea
;               Version 8, 23-Jul-2014, WTT, Temporarily add Ansan for testing
;               Version 9, 13-Aug-2014, WTT, Keep old ephem one more day
;               Version 10, 23-Feb-2014, WTT, Temporarily add Green Bank for
;                      solar conjunction observations
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_make_station
;
;  Get today's date, and set up the names of the spacecraft and stations
;  requiring individualized ephemerides.
;
get_utc, utc0
sc = ['ahead', 'behind']
stations = ['tablemnt', 'dskaggs', 'koganei', 'chilbolton', 'fairbanks', $
            'kiel', 'bochum', 'beijing', 'kswc', 'ansan', 'greenbank']
rdstations = ['koganei', 'greenbank']
;
;  Step through the days over the next week, starting from tomorrow, and create
;  the ephemerides.
;
for i=1,7 do begin
    utc = utc0
    utc.mjd = utc.mjd + i
    for j=0,1 do begin
        ssc_write_oem, sc[j], utc
        ssc_write_iirv, sc[j], utc
        for k=0,n_elements(stations)-1 do $
          ssc_write_station, sc[j], utc, stations[k]
        for k=0,n_elements(rdstations)-1 do $
          ssc_write_station_ra_dec, sc[j], utc, rdstations[k]
    endfor
endfor
;
;  Purge ephemerides for dates earlier than yesterday.
;
utc.mjd = utc0.mjd - 1
date = anytim2cal(utc, form=8, /date)
for j=0,1 do begin
    filename = sc[j] + '_' + date
;
    path = getenv('SSC_STATIONS_OEM_W')
    files = file_search( concat_dir(path, sc[j] + '_*.oem') )
    w = where(files lt concat_dir(path, filename), count)
    if count gt 0 then file_delete, files[w]
;
    path = getenv('SSC_STATIONS_IIRV_W')
    files = file_search( concat_dir(path, sc[j] + '_*.iirv') )
    w = where(files lt concat_dir(path, filename), count)
    if count gt 0 then file_delete, files[w]
;
    for k=0,n_elements(stations)-1 do begin
        path = concat_dir( getenv('SSC_STATIONS_W'), stations[k] )
        files = file_search( concat_dir(path, sc[j] + '_*.txt') )
        w = where(files lt concat_dir(path, filename), count)
        if count gt 0 then file_delete, files[w]
    endfor
endfor
;
return
end
