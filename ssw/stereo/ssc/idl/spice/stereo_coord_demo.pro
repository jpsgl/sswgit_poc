;+
; Project     :	STEREO - SSC
;
; Name        :	STEREO_COORD_DEMO
;
; Purpose     :	Demonstration of reading STEREO SPICE kernel
;
; Category    :	STEREO, Orbit
;
; Explanation :	This procedure demonstrates the process of deriving the
;               position and velocity of the two STEREO spacecraft in various
;               standard coordinate systems.  The appropriate SPICE kernels are
;               loaded, the date/time is converted to the ephemeris time which
;               SPICE uses, and the spacecraft letter identifier is converted
;               to the appropriate code.  CSPICE_SPKEZR is used to derive the
;               state vector and light travel time.  Finally, at the end of the
;               procedure, the kernels are unloaded.
;
;               It is not intended that this procedure be used directly.
;               Instead, it is a simple demo of the process of using STEREO
;               SPICE kernels.
;
; Syntax      :	STEREO_COORD_DEMO, DATE, SPACECRAFT, SYSTEM, STATE, LTIME
;
; Examples    :	stereo_coord_demo,'2006-05-06T11:30:00','A','GSE',state,ltime
;
; Inputs      :	DATE       = The date and time.  This can be input in any
;                            format accepted by ANYTIM2UTC.
;               SPACECRAFT = Either "A" or "B".
;               SYSTEM     = Character string, giving one of the following
;                            standard coordinate systems: "GEI", "GEO", "GSE",
;                            "HCI", "HAE", "HEE", "HEEQ", "Carrington"
;
; Opt. Inputs :	None.
;
; Outputs     :	STATE = The six-value state vector, containing the X,Y,Z
;                       coordinates in kilometers, and VX,VY,VZ in km/sec.
;               LTIME = The light travel time, in seconds.
;
; Opt. Outputs:	None.
;
; Keywords    :	CORR = Aberration correction.  Default is 'None'.  Other
;                      possible values are:
;                       'LT'    Light travel time
;                       'LT+S'  Light travel time plus stellar aberration
;               PRECESS = If set, then ecliptic coordinates are precessed from
;                         the J2000 reference frame to the mean ecliptic of
;                         date.  Only used for HAE.  Default is PRECESS=0.
;                         GSE and HEE use the ecliptic of date by definition.
;
; Calls       :	ANYTIM2UTC, CONCAT_DIR, CSPICE_FURNSH, CSPICE_SPKEZR,
;               CSPICE_UNLOAD
;
; Common      :	None.
;
; Restrictions:	This procedure works in conjunction with the Icy/CSPICE
;               package, which is implemented as an IDL Dynamically Loadable
;               Module (DLM).  The Icy source code can be downloaded from
;
;                       ftp://naif.jpl.nasa.gov/pub/naif/toolkit/IDL
;
;               Because this uses dynamic frames, it requires Icy/CSPICE
;               version N0058 or higher.
;
; Side effects:	None.
;
; Prev. Hist. :	Based on STEREO_STATE_DEMO
;
; History     :	Version 1, W. Thompson, 10 May 2004
;               Version 2, W. Thompson, 11 May 2004
;                       Added keyword precess
;               Version 3, W. Thompson, 18 Feb 2005
;                       Modified to use dynamic frames facility added in SPICE
;                       version N0058.  PRECESS keyword restricted to HAE.
;                       Added HCI coordinate system.
;
;                       Older versions did not calculate velocities correctly
;                       in rotating coordinate frames.  The precession
;                       corrections are now calculated within SPICE, and are
;                       slightly different.
;
; Contact     :	WTHOMPSON
;-
;
pro stereo_coord_demo, date, spacecraft, system, state, ltime, corr=k_corr, $
                       precess=precess
;
on_error, 2
if n_params() ne 5 then message, $
        'Syntax:  STEREO_COORD_DEMO, DATE, SPACECRAFT, SYSTEM, STATE, LTIME'
;
;  Determine which spacecraft was requested, and translate it into the proper
;  input for SPICE.  Some SPICE routines use an ID number, which is positive
;  for planets and other solar system bodies, and negative for spacecraft.
;  Other routines use a character string name.  A few routines which take
;  character strings, such as SPKEZR, will also accept the ID number formatted
;  as a string.  Both ID numbers and correct names are demonstrated below for
;  the two STEREO spacecraft.
;
case strupcase(spacecraft) of
    'A': sc = '-234'    ;or 'STEREO AHEAD'
    'B': sc = '-235'    ;or 'STEREO BEHIND'
    else: message, 'Unrecognized spacecraft'
endcase
;
;  Convert the date/time to UTC.
;
errmsg = ''
utc = anytim2utc(date,/ccsds,errmsg=errmsg)
if errmsg ne '' then message, errmsg
;
;  Parse the keywords.
;
if n_elements(k_corr) eq 1 then corr = k_corr else corr = 'None'
;
;  Define the path to the demo files.
;
datapath = concat_dir('$SSW', 'stereo', /DIR)
datapath = concat_dir(datapath, 'ssc', /DIR)
datapath = concat_dir(datapath, 'data', /DIR)
datapath = concat_dir(datapath, 'spice_demo', /DIR)
;
;  Define the files which need to be loaded.  The STEREO ephemeris file is
;  preliminary, based on the projected launch date.  To support reading this
;  file, two other files must also be loaded.  The file 'naif0007.tls' contains
;  leap second information used to support the conversion from UTC to the
;  ephemeris time (ET) used by SPICE.  The planetary orbit ephemeris file,
;  'de405.bsp', is the same file used in generating the STEREO ephemeris, and
;  is required to put the STEREO ephemeris into its proper context relative to
;  the solar system barycenter where SPICE does its calculations.  It also
;  allows STEREO positions to be calculated relative to various solar system
;  objects.  Solar and planetary physical data are stored in 'pck00007.tpc',
;  and are used to support the "IAU_EARTH" and "IAU_SUN" reference frames.
;  These supporting files were downloaded from
;
;	ftp://naif.jpl.nasa.gov/pub/naif/generic_kernels
;
tm_file = concat_dir(datapath, 'naif0007.tls')          ;Leapseconds
de_file = concat_dir(datapath, 'de405.bsp')             ;Planetary orbits
pl_file = concat_dir(datapath, 'pck00007.tpc')          ;Planetary phys. data
st_file = concat_dir(datapath, 'stereo_20060211a_nominal.bsp')
fr_file = concat_dir(datapath, 'heliospheric.tf')       ;Heliospheric frames
fr_file = 'heliospheric.tf'
;
;  Load the files
;
cspice_furnsh, tm_file
cspice_furnsh, de_file
cspice_furnsh, pl_file
cspice_furnsh, st_file
cspice_furnsh, fr_file
;
;  Convert the date/time to ephemeris time.
;
cspice_str2et, utc, et
;
;  Based on the coordinate system requested, get the state and light travel
;  time.
;
case strupcase(system) of
    'GEI': cspice_spkezr, sc, et, 'J2000', corr, 'Earth', state, ltime
    'GEO': cspice_spkezr, sc, et, 'IAU_EARTH', corr, 'Earth', state, ltime
    'GSE': cspice_spkezr, sc, et, 'GSE', corr, 'Earth', state, ltime
    'HCI': cspice_spkezr, sc, et, 'HCI', corr, 'Sun', state, ltime
    'HAE': begin
        if keyword_set(precess) then frame='ECLIPDATE' else frame='ECLIPJ2000'
        cspice_spkezr, sc, et, frame, corr, 'Sun', state, ltime
        end
    'HEE': cspice_spkezr, sc, et, 'HEE', corr, 'Sun', state, ltime
    'HEEQ': cspice_spkezr, sc, et, 'HEEQ', corr, 'Sun', state, ltime
    'CARRINGTON': cspice_spkezr, sc, et, 'IAU_SUN', corr, 'Sun', state, ltime
    else: message,'Unrecognized coordinate system'
endcase
;
;  Unload the files
;
cspice_unload, fr_file
cspice_unload, st_file
cspice_unload, pl_file
cspice_unload, de_file
cspice_unload, tm_file
;
end
