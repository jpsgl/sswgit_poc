;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_MAKE_WHERE_GIF
;
; Purpose     :	Make GIF plot showing STEREO positions.
;
; Category    :	STEREO, Orbit, Graphics
;
; Explanation :	Front end to the SSC_PLOT_WHERE routine to create a GIF image
;               showing the ecliptic positions of the two STEREO spacecraft on
;               top of an planetary orbital chart.  Used by the STEREO Orbit
;               Tool web page.
;
; Syntax      :	SSC_MAKE_WHERE_GIF, DATE, FILENAME
;
; Examples    :	GET_UTC, UTC  &  SSC_MAKE_WHERE_GIF, UTC, 'test.gif'
;
; Inputs      :	DATE     = The date/time to be plotted.  Can by in any format
;                         supported by ANYTIM2UTC.
;
;               FILENAME = The name of the file to be written.  The default
;                          extension is ".gif".
;
; Opt. Inputs :	None.
;
; Outputs     :	A GIF file is created showing the spacecraft positions.
;
; Opt. Outputs:	None.
;
; Keywords    :	RESOLUTION = Two element array giving the size of the GIF image
;                            to create.  Defaults to [500,400].
;
;               INNER   = If set, then expand out to the orbit of Mars.
;               OUTER   = If set, then expand out to the orbit of Saturn.
;
;               PARKER  = If set, then overplot the Parker spiral.
;
;               ULYSSES   = If set, then overplot orbit of Ulysses.
;               MESSENGER = If set, then overplot orbit of Messenger.  The
;                           orbit drawn is Keplerian, and does not include any
;                           deviations due to maneuvers or planet fly-bys.
;
;               Will also accept any keywords for LOAD_STEREO_SPICE or
;               SSC_OPLOT_PARKER.
;
; Calls       :	SETPLOT, SSC_PLOT_WHERE, TVREAD, WRITE_GIF, FORM_FILENAME
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	Loads the STEREO ephemerides if not already loaded.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 20-Jul-2006, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_make_where_gif, date, filename, resolution=resolution, _extra=_extra
;
dname = !d.name
setplot,'z'
device, set_resolution=[500,400]
ssc_plot_where, date, _extra=_extra
img = tvread(r,g,b)
write_gif, form_filename(filename,'.gif'), img, r, g, b
setplot, dname
;
end
