;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_FORM_KERNEL_LISTS
;
; Purpose     :	Form STEREO SPICE kernel lists
;
; Category    :	STEREO, Orbit
;
; Explanation :	Generates STEREO SPICE kernel lists from the entries in the
;               stereo_kernels database.
;
; Syntax      :	SSC_FORM_KERNEL_LISTS
;
; Examples    :	SSC_FORM_KERNEL_LISTS, PATH='$STEREO_SPICE_W', /INSTALL
;
; Inputs      :	None.
;
; Opt. Inputs :	None.
;
; Outputs     :	The kernel lists are initially written with the extension
;               ".new" instead of ".dat".  Only if all the files get written
;               successfully, and if the /INSTALL keyword is set, are the files
;               renamed to ".dat".  
;
; Opt. Outputs:	None.
;
; Keywords    :	PATH    = Path to write output files to.  The default is to
;                         write to the current directory.
;
;               INSTALL = If set, then install files.  The PATH=... keyword
;                         should also be used to define where the files should
;                         be installed.
;
;               ERRMSG = If defined and passed, then any error messages will be
;                        returned to the user in this parameter rather than
;                        depending on the MESSAGE routine in IDL.  In order to
;                        use this feature, ERRMSG must be defined first, e.g.
;
;                               ERRMSG = ''
;                               SSC_FORM_KERNEL_LISTS, ERRMSG=ERRMSG
;                               IF ERRMSG NE '' THEN ...
;
; Calls       :	CONCAT_DIR, DBOPEN, DBFIND, DBEXT, DBCLOSE
;
; Common      :	None.
;
; Env. vars.  : STEREO_SPICE  = Directory containing the stereo_kernels
;                               database.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 28-Feb-2006, William Thompson, GSFC
;               Version 2, 31-Oct-2006, William Thompson, GSFC
;                       Add preliminary phase to look for ephemeris files from
;                       Mission Planning, with names like
;                       "STA_2006_299_03.epm.bsp".
;               Version 3, 1-Nov-2006, William Thompson, GSFC
;                       Improved ordering.
;                       Temporarily disabled use of FDF .epm files.
;               Version 4, 6-Nov-2006, William Thompson, GSFC
;                       Added keywords PATH, INSTALL
;               Version 5, 26-Dec-2006, William Thompson, GSFC
;                       Use PATH instead of $STEREO_SPICE when passed
;               Version 6, 24-Jul-2007, William Thompsong, GSFC
;                       Fixed bug filtering out superceded kernels
;                       Use FDF predictive ephemerides, and only those not
;                       covered by definitive ephemerides
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_form_kernel_lists, path=path, install=install, errmsg=errmsg
on_error, 2
;
get_lun, unit1
get_lun, unit2
;
;  Open the STEREO kernels database.
;
unavail = 0
if n_elements(path) eq 1 then stereo_spice = path else $
  stereo_spice = getenv('STEREO_SPICE')
database = concat_dir(stereo_spice, 'stereo_kernels')
dbopen, database, unavail=unavail
if unavail then begin
    message = 'Unable to open STEREO_KERNELS database'
    goto, handle_error
endif
;
;  Step through all the filetypes and both spacecraft, and look for filenames
;  as described in the MOC Data Products document.
;
sc = ['A','B']
suffix = ['ahead','behind']
filetype = ['ah','depm','epm']
prefix = ['attitude_history', 'definitive_ephemerides', 'ephemerides']
maxdate = dblarr(2)
for itype = 0,2 do begin
    for isc = 0,1 do begin
;
;  Find the valid entries for the given spacecraft and filetype.
;
        entry = dbfind('scid=' + sc[isc] + ',valid=1,filetype=' + $
                       filetype[itype], /silent, /fullstring, count=count)
        if count gt 0 then begin
            dbext, entry, 'fileprefix,version', fileprefix, version
;
;  Filter out lower version numbers.
;
            ok = bytarr(n_elements(entry))
            for i=0,n_elements(entry)-1 do begin
                w = where((fileprefix eq fileprefix[i]) and $
                          (version gt version[i]), count)
                if count eq 0 then ok[i] = 1
            endfor
            w = where(ok)
            entry = entry[w]
;
;  Sort the filenames by filename.  Write the result to the output file.
;
            dbext, entry, 'filename,start_date,end_date', filename, $
              start_date, end_date
            s = sort(filename)
            filename = filename[s]
            start_date = start_date[s]
            end_date = end_date[s]
;
;  Filter out any files completely subsumed within subsequent files.  For
;  predictive ephemeris files, ignore start dates earlier than the maximum end
;  date found for the definite ephemeris files.
;
            save = replicate(1, n_elements(filename))
            for i=0,n_elements(filename)-2 do begin
                start_date0 = start_date[i]
                if filetype[itype] eq 'epm' then start_date0 = $
                  start_date0 > maxdate[isc]
                w = where((start_date0 ge start_date[i+1:*]) and $
                          (end_date[i] le end_date[i+1:*]), count)
                if count gt 0 then save[i] = 0
            endfor
            w = where(save)
            filename = filename[w]
;
;  If processing the definitive ephemeris files, then save the maximum end
;  date.
;
            if filetype[itype] eq 'depm' then maxdate[isc] = max(end_date[w])
;
;  Write the output file.
;
            outfile = prefix[itype] + '_' + suffix[isc] + '.new'
            if n_elements(path) eq 1 then outfile=concat_dir(path,outfile)
            openw, unit1, outfile
            for i=0,n_elements(filename)-1 do $
              printf, unit1, strtrim(filename[i],2)
            close, unit1
        endif
    endfor
endfor
;
;  If the INSTALL keyword was set, then install the new lists, but only if they
;  differ from the current lists.
;
if keyword_set(install) then begin
    for itype = 0,2 do begin
        for isc = 0,1 do begin
            filename = prefix[itype] + '_' + suffix[isc]
            if n_elements(path) eq 1 then filename = concat_dir(path, filename)
            openr, unit1, filename + '.dat'
            openr, unit2, filename + '.new'
            test = 0
            line1 = 'string'
            line2 = 'string'
            while (not eof(unit1)) and (not eof(unit2)) do begin
                readf, unit1, line1
                readf, unit2, line2
                test = test or (line1 ne line2)
            endwhile
            test = test or (not eof(unit1))
            test = test or (not eof(unit2))
            close, unit1, unit2
            if test then begin
                print, 'mv -f ' + filename + '.dat ' + filename + '.old'
                spawn, 'mv -f ' + filename + '.dat ' + filename + '.old'
                print, 'mv -f ' + filename + '.new ' + filename + '.dat'
                spawn, 'mv -f ' + filename + '.new ' + filename + '.dat'
            end else spawn, 'rm -f ' + filename + '.new'
        endfor
    endfor
endif
;
goto, finish
;
;  Error handling point.
;
HANDLE_ERROR:
if n_elements(errmsg) ne 0 then $
  errmsg = 'ssc_form_kernel_lists: ' + message else $
  message, message, /continue
;
;  Close the database, and return whether the routine was successful or not.
;
FINISH:
free_lun, unit1, unit2
dbclose
end
