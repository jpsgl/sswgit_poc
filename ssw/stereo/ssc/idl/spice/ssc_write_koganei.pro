;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_WRITE_KOGANEI
;
; Purpose     :	Write out Koganei ephemeris files.
;
; Category    :	STEREO, Operations, Orbit
;
; Explanation :	This procedure writes out a daily ephemeris for one of the two
;               STEREO spacecraft specific for the Koganei ground station.
;
; Syntax      :	SSC_WRITE_KOGANEI, SPACECRAFT, DATE  [, DELTA=DELTA ]
;
; Inputs      :	SPACECRAFT = Can be one of the following forms:
;
;                               'A'             'B'
;                               'STA'           'STB'
;                               'Ahead'         'Behind'
;                               'STEREO Ahead'  'STEREO Behind'
;                               'STEREO-Ahead'  'STEREO-Behind'
;                               'STEREO_Ahead'  'STEREO_Behind'
;
;                            Case is not important.  The NAIF numeric codes of
;                            -234 and -235 respectively can also be used.
;
;               DATE    = The date that the ephemeris will cover.  Can be in
;                         any format recognized by ANYTIM2UTC.  Only the date
;                         is used--any time information is ignored.
;
; Opt. Inputs :	None.
;
; Outputs     :	The ephemeris file is written to disk with a filename generated
;               from the spacecraft and date, e.g.
;
;                       ahead_20061113_01.koganei
;
;               The last two digits in the filename is a version number which
;               is incremented each time the procedure is called.  If the new
;               version is identical to the previous version, it is deleted.
;
; Opt. Outputs:	None.
;
; Keywords    :	DELTA  = Step size in seconds.  Default is 60.
;
;               ERRMSG = If defined and passed, then any error messages will be
;                        returned to the user in this parameter rather than
;                        depending on the MESSAGE routine in IDL.  If no errors
;                        are encountered, then a null string is returned.  In
;                        order to use this feature, ERRMSG must be defined
;                        first, e.g.
;
;                               ERRMSG = ''
;                               SSC_WRITE_KOGANEI, ERRMSG=ERRMSG, ...
;                               IF ERRMSG NE '' THEN ...
;
;               Will also accept any LOAD_STEREO_SPICE keywords.
;
; Env. vars.  : SSC_STATIONS_KOGANEI_W = Directory to write Koganei files to.
;
; Calls       :	ANYTIM2UTC, PARSE_STEREO_NAME, LOAD_STEREO_SPICE, ANYTIM2CAL,
;               CONCAT_DIR, GET_UTC, ANYTIM2TAI, TAI2UTC, CSPICE_FURNSH,
;               CSPICE_BODVAR, CSPICE_GEOREC, CSPICE_STR2ET, CSPICE_SPKEZR,
;               CSPICE_SXFORM, CSPICE_RECLAT, CSPICE_UNLOAD
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	The SPICE kernels will be loaded, if not loaded already.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 13-Nov-2006, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_write_koganei, spacecraft, date, delta=delta, _extra=_extra
on_error, 2
;
;  Make sure that delta is defined.
;
if n_elements(delta) ne 1 then delta = 60.d0
;
;  Determine the start and end times, based on the beginning and end of a
;  calendar day.
;
message = ''
start_time = anytim2utc(date, errmsg=message)
if message ne '' then goto, handle_error
;
start_time.time = 0
end_time = start_time
end_time.mjd = end_time.mjd + 1
;
;  Determine which spacecraft was requested, and translate it into the proper
;  input for SPICE.  Determine the SPACEWARN object ID number.
;
sc = parse_stereo_name(spacecraft, ['STEREO AHEAD','STEREO BEHIND'])
case sc of
    'STEREO AHEAD': fname = 'ahead'
    'STEREO BEHIND': fname = 'behind'
    else: begin
        message = 'Unable to recognize spacecraft ' + strtrim(sc,2)
        goto, handle_error
    end
endcase
;
;  Make sure that the SPICE kernels are loaded.
;
load_stereo_spice, errmsg=message, _extra=_extra
if message ne '' then goto, handle_error
;
;  Load the station frame file.
;
tf_file = concat_dir(getenv('SSW_SSC'), 'data/spice/stations/koganei.tf')
cspice_furnsh, tf_file
;
;  Create the filename based on the spacecraft and the date.
;
filename = fname + '_' + anytim2cal(date, form=8, /date)
filename = concat_dir(getenv('SSC_STATIONS_KOGANEI_W'), filename)
;
;  Find any existing files, extract the highest index number, and increment it
;  by 1.
;
files = filename + '_??.koganei'
files = file_search(files, count=count)
if count gt 0 then begin
    s = sort(files)
    files = files(reverse(s))
    lastfile = files[0]
    underscore = strpos(lastfile, '_', /reverse_search)
    index = fix(strmid(lastfile, underscore+1, 2)) + 1
end else begin
    index = 1
    lastfile = ''
endelse
filename = filename + '_' + string(index,format='(I2.2)') + '.koganei'
;
;  Open the output file, and write out the header.
;
openw, unit, filename, /get_lun
;
printf, unit, 'Station: Koganei'
printf, unit, 'Target:  ' + sc
get_utc, now, /ccsds
printf, unit, 'Creation date: ' + now
printf, unit, 'Start time: ' + anytim2utc(start_time, /ccsds)
printf, unit, 'End   time: ' + anytim2utc(end_time, /ccsds)
printf, unit, 'Cadence (sec): ' + ntrim(delta)
printf, unit, ''
printf, unit, $
  'Date/Time                      Azimuth        Elevation        Doppler'
printf, unit, $
  '(UTC)                           (deg.)          (deg.)          (km/s)'
printf, unit, ''
;
;  Get the origin.
;
lon_r = (139 + 29/60.d0) * !dpi / 180.d0
lat_r = ( 35 + 43/60.d0) * !dpi / 180.d0
alt = 0.124d0
cspice_bodvar, 399, 'RADII' , radii
flat = (radii[0]-radii[2]) / radii[0]
cspice_georec, lon_r, lat_r, alt, radii[0], flat, origin
origin = [origin, 0, 0, 0]
;
;  Step through the times.
;
t0 = anytim2tai(start_time, /nocorrect)
t1 = anytim2tai(end_time, /nocorrect)
;
for t=t0,t1,delta do begin
    utc = tai2utc(t,/nocorrect,/ccsds)
    cspice_str2et, utc, et
;
;  Get the position of the spacecraft in geocentric coordinates, and subtract
;  the origin.  Convert to the station frame.
;
    cspice_spkezr, sc, et, 'IAU_EARTH', 'lt+s', 'Earth', state, ltime
    state = state - origin
    cspice_sxform, 'IAU_EARTH', 'KOGANEI_TOPO', et, xform
    state = transpose(xform) # state
;
;  Convert into azimuth and elevation.
;
    cspice_reclat, state[0:2], distance, azimuth, elevation
    azimuth   = azimuth   * (180.d0 / !dpi)
    elevation = elevation * (180.d0 / !dpi)
    printf, unit, utc, azimuth, elevation, state[5]
endfor
;
;  Close the output file and check to see if the file is identical to the
;  previously generated file, if any.
;
cspice_unload, tf_file
free_lun, unit
if index gt 1 then begin
    openr, unit1, /get_lun, filename
    openr, unit2, /get_lun, lastfile
    test = 0
    line1 = 'string'
    line2 = 'string'
;
;  Skip over first three lines so as to ignore the creation date.
;
    for i=1,3 do begin
        readf, unit1, line1
        readf, unit2, line2
    endfor
;
    while (not eof(unit1)) and (not eof(unit2)) do begin
        readf, unit1, line1
        readf, unit2, line2
        test = test or (line1 ne line2)
    endwhile
    test = test or (not eof(unit1))
    test = test or (not eof(unit2))
    free_lun, unit1, unit2
    if not test then begin
        print, 'Deleting '+filename
        file_delete, filename
    endif
endif
;
return
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message else $
  errmsg = 'ssc_write_koganei: ' + message
;
end
