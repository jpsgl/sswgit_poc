;+
; Project     :	STEREO - SSC
;
; Name        :	GET_STEREO_ATT_FILE
;
; Purpose     :	Returns the SPICE kernel containing STEREO attitude data
;
; Category    :	STEREO, Orbit
;
; Explanation :	This routine returns the name of the kernel containing the
;               attitude history information for a given date and STEREO
;               observatory.  This routine is for a single date/time
;
; Syntax      :	Kernel = GET_STEREO_ATT_FILE( DATE, SPACECRAFT )
;
; Examples    :	Kernel = GET_STEREO_ATT_FILE( '2006-05-06T11:30:00', 'A' )
;
; Inputs      :	DATE       = The date and time.  This can be input in any
;                            format accepted by ANYTIM2UTC.
;                            Must be a single date/time, not a vector of times!
;
;               SPACECRAFT = Can be one of the following forms:
;
;                               'A'             'B'
;                               'STA'           'STB'
;                               'Ahead'         'Behind'
;                               'STEREO Ahead'  'STEREO Behind'
;                               'STEREO-Ahead'  'STEREO-Behind'
;                               'STEREO_Ahead'  'STEREO_Behind'
;
;                            Case is not important.  The NAIF numeric codes of
;                            -234 and -235 respectively can also be used.
;
; Opt. Inputs :	None.
;
; Outputs     :	The result of the function is the name of the loaded kernel
;               containing the attitude history information.  The
;               null string is returned if there is no match.
;
; Opt. Outputs:	None.
;
; Keywords    : ERRMSG = If defined and passed, then any error messages will be
;                        returned to the user in this parameter rather than
;                        depending on the MESSAGE routine in IDL.  If no errors
;                        are encountered, then a null string is returned.  In
;                        order to use this feature, ERRMSG must be defined
;                        first, e.g.
;
;                               ERRMSG = ''
;                               Kernel = GET_STEREO_SPICE_KERNEL( ERRMSG=ERRMSG, ... )
;                               IF ERRMSG NE '' THEN ...
;
;               Will also accept any ANYTIM2UTC keywords.
;
; Calls       :	PARSE_STEREO_NAME, ANYTIM, MJD2DOY,
;               GET_STEREO_SPICE_RANGE, ANYTIM2UTC, LOAD_STEREO_SPICE,
;               LOAD_STEREO_SPICE_ATT
;
; Common      :	None.
;
; Restrictions:	This procedure works in conjunction with the Icy/CSPICE
;               package, which is implemented as an IDL Dynamically Loadable
;               Module (DLM).  The Icy source code can be downloaded from
;
;                       ftp://naif.jpl.nasa.gov/pub/naif/toolkit/IDL
;
; Side effects:	Will automatically load the SPICE ephemeris files, if not
;               already loaded.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 20-Mar-2006, Jean-Pierre Wuelser
;               derived from get_stereo_spice_kernel
;               Version 2, 25-Jul-2007, WTT, Add LOAD_STEREO_SPICE_ATT
;
; Contact     :	
;-
;
function get_stereo_att_file, date, spacecraft, $
                                  errmsg=errmsg, _extra=_extra
;
on_error, 2
if n_params() ne 2 then begin
    message = 'Syntax:  Kernel = GET_STEREO_ATT_FILE( DATE, SPACECRAFT )'
    goto, handle_error
endif
;
;  Determine which spacecraft (or planetary body) was requested, and translate
;  it into the proper input for SPICE.
;
sc  = parse_stereo_name(spacecraft, ['ahead', 'ehind']) ; 'ehind' not a typo
;
;  Convert the date/time to UTC.
;
message = ''
utc = anytim2utc(date, /ccsds, errmsg=message, _extra=_extra)
if message ne '' then goto, handle_error
;  derive year and doy
yr  = strmid(utc,0,4)
mjd = anytim(utc,/mjd)
doy = string(mjd2doy(mjd),form='(i3.3)')
;
;  Make sure that the kernel files are loaded.
;
message = ''
load_stereo_spice, errmsg=message, _extra=_extra
load_stereo_spice_att, spacecraft, date, _extra=_extra
if message ne '' then goto, handle_error
;
;  Get a report of all loaded kernels.
;
message = ''
spice_kernel_report, kernels=loaded, /quiet, errmsg=message
if message ne '' then goto, handle_error
;
;  Search for the appropriate kernel
;
iname = sc+'_'+yr+'_'+doy+'ah.bc'
lname = strmid(loaded,22,14,/reverse_offs) + strmid(loaded,4,5,/reverse_offs)
ww = where(iname eq lname,nww)
;
;  Check if date falls within kernel time range
;
if nww gt 0 then begin
    message = ''
    get_stereo_spice_range, loaded[ww[0]], date0, date1, /ccsds, errmsg=message
    if message ne '' then goto, handle_error
    if (utc ge date0) and (utc le date1) $
       then kernel = file_basename(loaded[ww[0]]) else kernel = ''
endif else kernel = ''
;
return, kernel
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message else $
  errmsg = 'get_stereo_att_file: ' + message
;
end
