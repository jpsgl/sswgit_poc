;+
; Project     :	STEREO - SSC
;
; Name        :	GET_STEREO_SEP_ANGLE
;
; Purpose     :	Returns the separation angle between STEREO A and B
;
; Category    :	STEREO, Orbit
;
; Explanation :	This routine returns the three-dimensional heliocentric
;               separation angle of the two STEREO spacecraft.  It can
;               also be used to return the separation angle with one of the
;               solar system bodies (e.g. Earth).
;
; Syntax      :	Angle = GET_STEREO_SEP_ANGLE( DATE, BODY1, BODY2 )
;
; Examples    :	Angle = GET_STEREO_SEP_ANGLE( '2006-05-06T11:30:00', 'A', 'B' )
;
; Inputs      :	DATE = The date and time.  This can be input in any
;                      format accepted by ANYTIM2UTC, and can also be an
;                      array of values.
;
;               BODY1, BODY2 = Can be one of the following forms:
;
;                               'A'             'B'
;                               'STA'           'STB'
;                               'Ahead'         'Behind'
;                               'STEREO Ahead'  'STEREO Behind'
;                               'STEREO-Ahead'  'STEREO-Behind'
;
;                      Case is not important.  The NAIF numeric codes of -234
;                      and -235 respectively can also be used.
;
;                      Also can be the name of a solar system body,
;                      e.g. "Earth", "Mars", "Moon", etc.
;
; Opt. Inputs :	None.
;
; Outputs     :	The result of the function is the separation angle, in radians.
;
;               (Note that, because of the three-dimensionality of the problem,
;               the separation angle between STEREO-A and B will not be exactly
;               the same as the summation of the separation of each with
;               Earth.)
;
; Opt. Outputs:	None.
;
; Keywords    : CORR = Aberration correction.  Default is 'None'.  Other
;                      possible values are:
;
;                       'LT'    Light travel time
;                       'LT+S'  Light travel time plus stellar aberration
;                       'XLT'   Light travel time, transmission case
;                       'XLT+S' Light travel plus aberration, transmission case
;
;               DEGREES = If set, then the separation angle is returned in
;                         units of degrees, rather than radians.
;
;               ERRMSG = If defined and passed, then any error messages will be
;                        returned to the user in this parameter rather than
;                        depending on the MESSAGE routine in IDL.  If no errors
;                        are encountered, then a null string is returned.  In
;                        order to use this feature, ERRMSG must be defined
;                        first, e.g.
;
;                               ERRMSG = ''
;                               Angle=GET_STEREO_SEP_ANGLE(ERRMSG=ERRMSG, ...)
;                               IF ERRMSG NE '' THEN ...
;
;               Will also accept any LOAD_STEREO_SPICE or ANYTIM2UTC keywords.
;
; Calls       :	ANYTIM2UTC, CONCAT_DIR, LOAD_STEREO_SPICE, GET_STEREO_COORD,
;               CSPICE_VSEP
;
; Common      :	None.
;
; Restrictions:	This procedure works in conjunction with the Icy/CSPICE
;               package, which is implemented as an IDL Dynamically Loadable
;               Module (DLM).  The Icy source code can be downloaded from
;
;                       ftp://naif.jpl.nasa.gov/pub/naif/toolkit/IDL
;
;               Because this uses dynamic frames, it requires Icy/CSPICE
;               version N0058 or higher.
;
; Side effects:	Will automatically load the SPICE ephemeris files, if not
;               already loaded.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 29-Aug-2005, William Thompson, GSFC
;               Version 2, 29-Aug-2005, William Thompson, GSFC
;                       Added keyword DEGREES
;               Version 3, 12-Sep-2005, William Thompson, GSFC
;                       Accept ANYTIM2UTC keywords
;               Version 4, 20-Jul-2006, William Thompson, GSFC
;                       Call GET_STEREO_COORD instead of CSPICE_SPKEZR
;
; Contact     :	WTHOMPSON
;-
;
function get_stereo_sep_angle, date, body1, body2, corr=corr, $
                               degrees=degrees, errmsg=errmsg, _extra=_extra
;
on_error, 2
if n_params() ne 3 then begin
    message = 'Syntax:  State = GET_STEREO_SEP_ANGLE( DATE, BODY1, BODY2 )'
    goto, handle_error
endif
;
;  Determine which spacecraft (or planetary body) was requested, and translate
;  it into the proper input for SPICE.
;
sc1 = strtrim(strupcase(body1),2)
n = strlen(sc1)
if (sc1 eq strmid('AHEAD',0,n))  or (sc1 eq strmid('STEREO AHEAD',0,n)) or $
  (sc1 eq strmid('STEREO-AHEAD',0,n))  or (sc1 eq 'STA') then $
  sc1 = 'STEREO AHEAD'
if (sc1 eq strmid('BEHIND',0,n)) or (sc1 eq strmid('STEREO BEHIND',0,n)) or $
  (sc1 eq strmid('STEREO-BEHIND',0,n)) or (sc1 eq 'STB') then $
  sc1 = 'STEREO BEHIND'
;
sc2 = strtrim(strupcase(body2),2)
n = strlen(sc2)
if (sc2 eq strmid('AHEAD',0,n))  or (sc2 eq strmid('STEREO AHEAD',0,n)) or $
  (sc2 eq strmid('STEREO-AHEAD',0,n))  or (sc2 eq 'STA') then $
  sc2 = 'STEREO AHEAD'
if (sc2 eq strmid('BEHIND',0,n)) or (sc2 eq strmid('STEREO BEHIND',0,n)) or $
  (sc2 eq strmid('STEREO-BEHIND',0,n)) or (sc2 eq 'STB') then $
  sc2 = 'STEREO BEHIND'
;
;  Convert the date/time to UTC.
;
message = ''
utc = anytim2utc(date, /ccsds, errmsg=message, _extra=_extra)
if message ne '' then goto, handle_error
;
;  Make sure that the ephemeris files are loaded.
;
message = ''
load_stereo_spice, errmsg=message, _extra=_extra
if message ne '' then goto, handle_error
;
;  Get the HCI coordinates of the two bodies, and calculate the separation
;  angle.
;
state1 = get_stereo_coord(utc, sc1, system='HCI', corr=corr)
state2 = get_stereo_coord(utc, sc2, system='HCI', corr=corr)
n = n_elements(utc)
state1 = reform(state1, 6, n)
state2 = reform(state2, 6, n)
sz = size(utc)
if sz[0] eq 0 then dim=1 else dim=sz[1:sz[0]]
if n eq 1 then angle =0.d0 else angle = make_array(dimension=dim,/double)
for i=0,n-1 do angle[i] = cspice_vsep( state1[0:2,i], state2[0:2,i] )
;
if keyword_set(degrees) then angle = (180.d0 / !dpi) * angle 
return, angle
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message else $
  errmsg = 'get_stereo_sep_angle: ' + message
;
end
