;+
; Project     :	STEREO - SSC
;
; Name        :	LOAD_STEREO_SPICE_ATT
;
; Purpose     :	Legacy front-end to LOAD_SUNSPICE_ATT_STEREO
;
; History     :	Version 2, 09-May-2016, WTT, call LOAD_SUNSPICE_ATT_STEREO
;               Version 3, 10-Aug-2017, WTT, fix typo
;
; Contact     :	WTHOMPSON
;-
;
pro load_stereo_spice_att, spacecraft, date, verbose=verbose
load_sunspice_att_stereo, spacecraft, date, verbose=verbose
end
