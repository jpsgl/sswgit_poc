;+
; Project     :	STEREO - SSC
;
; Name        :	LOAD_STEREO_SPICE_EARTH
;
; Purpose     :	Legacy front-end to LOAD_SUNSPICE_EARTH
;
; History     :	Version 2, 27-May-2016, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
pro load_stereo_spice_earth, reload=reload, verbose=verbose, errmsg=errmsg
load_sunspice_earth, reload=reload, verbose=verbose, errmsg=errmsg
end
