;+
; Project     :	STEREO - SSC
;
; Name        :	STEREO_GSE2GSM
;
; Purpose     :	Legacy front-end to CONVERT_SUNSPICE_GSE2GSM
;
; History     :	Version 8, 09-May-2016, WTT, call CONVERT_SUNSPICE_GSE2GSM
;
; Contact     :	WTHOMPSON
;-
;
pro stereo_gse2gsm, date, coord, cmat=cmat, inverse=inverse, errmsg=errmsg
convert_sunspice_gse2gsm, date, coord, cmat=cmat, inverse=inverse, errmsg=errmsg
end
