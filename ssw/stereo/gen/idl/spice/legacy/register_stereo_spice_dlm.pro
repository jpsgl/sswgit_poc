;+
; Project     :	STEREO - SSC
;
; Name        :	REGISTER_STEREO_SPICE_DLM
;
; Purpose     :	Legacy front-end to REGISTER_SUNSPICE_DLM
;
; History     :	Version 6, 08-Nov-2016, WTT, call REGISTER_SUNSPICE_DLM
;               15-Jan-2017, Zarro (ADNET) - added check for SUNSPICE register routine
;
; Contact     :	WTHOMPSON
;-
;
pro register_stereo_spice_dlm
if have_proc('register_sunspice_dlm') then register_sunspice_dlm
end
