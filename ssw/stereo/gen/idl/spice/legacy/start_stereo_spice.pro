;+
; Project     :	STEREO - SSC
;
; Name        :	START_STEREO_SPICE
;
; Purpose     :	Startup SPICE software for STEREO
;
; Category    :	STEREO, Orbit
;
; Explanation :	Registers SPICE DLM, and defines SOHO
;
; Syntax      :	Result = START_STEREO_SPICE()
;
; Examples    :	See $SSW/stereo/gen/setup/IDL_STARTUP
;
; Inputs      :	None.
;
; Opt. Inputs :	None.
;
; Outputs     :	None.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Calls       :	None.
;
; Common      :	Common block START_STEREO_SPICE makes sure this routine is only
;               called once, typically at startup.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 01-Feb-2010, William Thompson, GSFC
;                       Moved from $SSW/stereo/gen/setup/IDL_STARTUP
;
; Contact     :	WTHOMPSON
;-
;
pro start_stereo_spice
;
;  If this routine has already been called, then return.
;
common start_stereo_spice, started
if keyword_set(started) then return
started = 1
;
;  Register the SPICE/Icy DLM.
;
register_stereo_spice_dlm
;
;  If SOHO_SPICE is not yet defined, then define it to point within $SSWDB
;
if 1-file_exist(get_logenv('SOHO_SPICE')) then $
  set_logenv, 'SOHO_SPICE', concat_dir('SSWDB', 'soho/gen/spice')
;
end
