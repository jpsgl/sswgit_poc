
;+
; Project     :	STEREO - SSC
;
; Name        :	TEST_SPICE_ICY_DLM()
;
; Purpose     :	Legacy front-end to TEST_SUNSPICE_DLM()
;
; History     :	Version 3, 09-May-2016, WTT, call TEST_SUNSPICE_DLM
        ;       15-Jan-2017, Zarro (ADNET) - added check for SUNSPICE test routine
;
; Contact     :	WTHOMPSON
;-
;
function test_spice_icy_dlm

if ~have_proc('test_sunspice_dlm') then return,0b
return,call_function('test_sunspice_dlm')
end
