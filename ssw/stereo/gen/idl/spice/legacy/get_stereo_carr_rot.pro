;+
; Project     :	STEREO - SSC
;
; Name        :	GET_STEREO_CARR_ROT
;
; Purpose     :	Legacy front-end to GET_SUNSPICE_CARR_ROT
;
; History     :	Version 4, 09-May-2016, WTT, call GET_SUNSPICE_CARR_ROT
;
; Contact     :	WTHOMPSON
;-
;
function get_stereo_carr_rot, date, spacecraft, integer=integer, $
                              errmsg=errmsg, _extra=_extra
return, get_sunspice_carr_rot(date, spacecraft, integer=integer, $
                              errmsg=errmsg, _extra=_extra)
end
