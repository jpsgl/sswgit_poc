;+
; Project     :	STEREO - SSC
;
; Name        :	GET_STEREO_SPICE_RANGE
;
; Purpose     :	Legacy front-end to GET_SUNSPICE_RANGE
;
; History     :	Version 3, 09-May-2016, WTT, call GET_SUNSPICE_RANGE
;
; Contact     :	WTHOMPSON
;-
;
pro get_stereo_spice_range, filename, date0, date1, scid, tai=tai, $
                            errmsg=errmsg, _extra=_extra
get_sunspice_range, filename, date0, date1, scid, tai=tai, $
                            errmsg=errmsg, _extra=_extra
end
