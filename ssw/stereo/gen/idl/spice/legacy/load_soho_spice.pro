;+
; Project     :	STEREO - SSC - SOHO
;
; Name        :	LOAD_SOHO_SPICE
;
; Purpose     :	Legacy front-end to LOAD_SUNSPICE_SOHO
;
; History     :	Version 3, 09-May-2016, WTT, call LOAD_SUNSPICE_SOHO
;
; Contact     :	WTHOMPSON
;-
;
pro load_soho_spice, verbose=verbose, errmsg=errmsg
load_sunspice_soho, verbose=verbose, errmsg=errmsg
end
