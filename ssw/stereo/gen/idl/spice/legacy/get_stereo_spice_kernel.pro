;+
; Project     :	STEREO - SSC
;
; Name        :	GET_STEREO_SPICE_KERNEL
;
; Purpose     :	Legacy front-end to GET_SUNSPICE_KERNEL
;
; History     :	Version 5, 09-May-2016, WTT, call GET_SUNSPICE_KERNEL
;               Version 6, 08-Nov-2016, WTT, fix typo
;
; Contact     :	WTHOMPSON
;-
;
function get_stereo_spice_kernel, date, spacecraft, attitude=attitude, $
                                  errmsg=errmsg, _extra=_extra
return, get_sunspice_kernel(date, spacecraft, attitude=attitude, $
                     errmsg=errmsg, _extra=_extra)
end
