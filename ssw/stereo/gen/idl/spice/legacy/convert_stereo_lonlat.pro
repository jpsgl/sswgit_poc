;+
; Project     :	STEREO - SSC
;
; Name        :	CONVERT_STEREO_LONLAT
;
; Purpose     :	Legacy front-end to CONVERT_SUNSPICE_LONLAT
;
; History     :	Version 5, 09-May-2016, WTT, call CONVERT_SUNSPICE_LONLAT
;
; Contact     :	WTHOMPSON
;-
;
pro convert_stereo_lonlat, date, coord, k_system_from, k_system_to, $
                           degrees=degrees, ignore_origin=ignore_origin, $
                           pos_long=pos_long, errmsg=errmsg, _extra=_extra
convert_sunspice_lonlat, date, coord, k_system_from, k_system_to, $
                         degrees=degrees, ignore_origin=ignore_origin, $
                         pos_long=pos_long, errmsg=errmsg, _extra=_extra
end
