;+
; Project     :	STEREO - SSC
;
; Name        :	CONVERT_STEREO_COORD
;
; Purpose     :	Legacy front-end to CONVERT_SUNSPICE_COORD
;
; History     :	Version 15, 09-May-2016, WTT, call CONVERT_SUNSPICE_COORD
;
; Contact     :	WTHOMPSON
;-
;
pro convert_stereo_coord, date, coord, system_from, system_to, $
                          spacecraft=spacecraft, precess=precess, $
                          errmsg=errmsg, ignore_origin=ignore_origin, $
                          meters=meters, au=au, _extra=_extra
convert_sunspice_coord, date, coord, system_from, system_to, $
                        spacecraft=spacecraft, precess=precess, $
                        errmsg=errmsg, ignore_origin=ignore_origin, $
                        meters=meters, au=au, _extra=_extra
end
