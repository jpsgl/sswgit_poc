;+
; Project     :	STEREO - SSC
;
; Name        :	GET_STEREO_HPC_POINT
;
; Purpose     :	Legacy front-end to GET_SUNSPICE_HPC_POINT
;
; History     :	Version 5, 09-May-2016, WTT, call GET_SUNSPICE_HPC_POINT
;
; Contact     :	WTHOMPSON
;-
;
function get_stereo_hpc_point, date, spacecraft, found=found, $
                               precess=precess, instrument=instrument, $
                               tolerance=tolerance, degrees=degrees, $
                               post_conjunction=post_conjunction, $
                               radians=radians, errmsg=errmsg, _extra=_extra
return, get_sunspice_hpc_point(date, spacecraft, found=found, $
                               precess=precess, instrument=instrument, $
                               tolerance=tolerance, degrees=degrees, $
                               post_conjunction=post_conjunction, $
                               radians=radians, errmsg=errmsg, _extra=_extra)
end
