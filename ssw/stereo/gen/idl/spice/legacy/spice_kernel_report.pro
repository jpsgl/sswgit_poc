;+
; Project     :	STEREO - SSC
;
; Name        :	SPICE_KERNEL_REPORT
;
; Purpose     :	Legacy front-end to LIST_SUNSPICE_KERNELS
;
; History     :	Version 3, 09-May-2016, WTT, call LIST_SUNSPICE_KERNELS
;
; Contact     :	WTHOMPSON
;-
;
pro spice_kernel_report, kernels=kernels, quiet=quiet, errmsg=errmsg
list_sunspice_kernels, kernels=kernels, quiet=quiet, errmsg=errmsg
end
