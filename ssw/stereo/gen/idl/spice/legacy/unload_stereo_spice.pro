;+
; Project     :	STEREO - SSC
;
; Name        :	UNLOAD_STEREO_SPICE
;
; Purpose     :	Legacy front-end to UNLOAD_SUNSPICE_STEREO
;
; History     :	Version 7, 09-May-2016, WTT, call UNLOAD_SUNSPICE_STEREO
;
; Contact     :	WTHOMPSON
;-
;
pro unload_stereo_spice, verbose=verbose
unload_sunspice_stereo, verbose=verbose
end
