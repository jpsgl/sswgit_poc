;+
; Project     :	STEREO - SSC
;
; Name        :	UNLOAD_STEREO_SPICE_GEN
;
; Purpose     :	Legacy front-end to UNLOAD_SUNSPICE_GEN
;
; History     :	Version 4, 09-May-2016, WTT, call UNLOAD_SUNSPICE_GEN
;
; Contact     :	WTHOMPSON
;-
;
pro unload_stereo_spice_gen, verbose=verbose
unload_sunspice_gen, verbose=verbose
end
