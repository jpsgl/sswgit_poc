;+
; Project     :	STEREO - SSC
;
; Name        :	STEREO_GEO2MAG
;
; Purpose     :	Legacy front-end to CONVERT_SUNSPICE_GEO2MAG
;
; History     :	Version 6, 09-May-2016, WTT, call CONVERT_SUNSPICE_GEO2MAG
;
; Contact     :	WTHOMPSON
;-
;
pro stereo_geo2mag, date, coord, cmat=cmat, inverse=inverse, errmsg=errmsg
convert_sunspice_geo2mag, date, coord, cmat=cmat, inverse=inverse, errmsg=errmsg
end
