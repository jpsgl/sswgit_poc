;+
; Project     :	STEREO - SSC
;
; Name        :	LOAD_STEREO_SPICE
;
; Purpose     :	Legacy front-end to LOAD_SUNSPICE_STEREO
;
; History     :	Version 12, 09-May-2016, WTT, call LOAD_SUNSPICE_STEREO
;
; Contact     :	WTHOMPSON
;-
;
pro load_stereo_spice, reload=k_reload, sim1=sim1, sim2=sim2, sim3=sim3, $
                       verbose=verbose, kernels=kernels, $
                       nominal_ck=k_nominal_ck, errmsg=errmsg
load_sunspice_stereo, reload=k_reload, sim1=sim1, sim2=sim2, sim3=sim3, $
                      verbose=verbose, kernels=kernels, $
                      nominal_ck=k_nominal_ck, errmsg=errmsg
end
