;+
; Project     :	STEREO - SSC
;
; Name        :	GET_STEREO_CMAT
;
; Purpose     :	Legacy front-end to GET_SUNSPICE_CMAT
;
; History     :	Version 13, 09-May-2016, WTT, call GET_SUNSPICE_CMAT
;
; Contact     :	WTHOMPSON
;-
;
function get_stereo_cmat, date, spacecraft, system=k_system, found=found, $
                          precess=precess, instrument=instrument, $
                          tolerance=tolerance, six_vector=six_vector, $
                          nominal=nominal, errmsg=errmsg, _extra=_extra
return, get_sunspice_cmat(date, spacecraft, system=k_system, found=found, $
                          precess=precess, instrument=instrument, $
                          tolerance=tolerance, six_vector=six_vector, $
                          nominal=nominal, errmsg=errmsg, _extra=_extra)
end
