;+
; Project     :	STEREO - SSC
;
; Name        :	GET_STEREO_P0_ANGLE
;
; Purpose     :	Legacy front-end to GET_SUNSPICE_P0_ANGLE
;
; History     :	Version 2, 09-May-2016, WTT, Call GET_SUNSPICE_P0_ANGLE
;
; Contact     :	WTHOMPSON
;-
;
function get_stereo_p0_angle, date, spacecraft, degrees=degrees, corr=k_corr, $
                              errmsg=errmsg, _extra=_extra
return, get_sunspice_p0_angle(date, spacecraft, degrees=degrees, corr=k_corr, $
                              errmsg=errmsg, _extra=_extra)
end
