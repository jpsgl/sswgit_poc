;+
; Project     :	STEREO - SSC
;
; Name        :	LOAD_STEREO_SPICE_GEN
;
; Purpose     :	Legacy front-end to LOAD_SUNSPICE_GEN
;
; History     :	Version 7, 09-May-2016, WTT, call LOAD_SUNSPICE_GEN
;
; Contact     :	WTHOMPSON
;-
;
pro load_stereo_spice_gen, reload=reload, verbose=verbose, errmsg=errmsg
load_sunspice_gen, reload=reload, verbose=verbose, errmsg=errmsg
end
