;+
; Project     :	STEREO - SSC
;
; Name        :	UNLOAD_STEREO_SPICE_EARTH
;
; Purpose     :	Legacy front-end to LOAD_SUNSPICE_EARTH
;
; History     :	Version 2, 27-May-2016, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
pro unload_stereo_spice_earth, verbose=verbose
unload_sunspice_earth, verbose=verbose
end
