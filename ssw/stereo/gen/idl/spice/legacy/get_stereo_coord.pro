;+
; Project     :	STEREO - SSC
;
; Name        :	GET_STEREO_COORD
;
; Purpose     :	Legacy front-end to GET_SUNSPICE_COORD
;
; History     :	Version 14, 09-May-2016, WTT, Call GET_SUNSPICE_COORD
;
; Contact     :	WTHOMPSON
;-
;
function get_stereo_coord, date, spacecraft, system=k_system, ltime=ltime, $
                           corr=k_corr, precess=precess, target=target, $
                           novelocity=novelocity, meters=meters, au=au, $
                           found=found, errmsg=errmsg, _extra=_extra
return, get_sunspice_coord(date, spacecraft, system=k_system, ltime=ltime, $
                           corr=k_corr, precess=precess, target=target, $
                           novelocity=novelocity, meters=meters, au=au, $
                           found=found, errmsg=errmsg, _extra=_extra)
end
