;+
; Project     :	STEREO - SSC
;
; Name        :	STEREO_GSE2SM
;
; Purpose     :	Legacy front-end to CONVERT_SUNSPICE_GSE2SM
;
; History     :	Version 8, 09-May-2016, WTT, call CONVERT_SUNSPICE_GSE2SM
;
; Contact     :	WTHOMPSON
;-
;
pro stereo_gse2sm, date, coord, cmat=cmat, inverse=inverse, errmsg=errmsg
convert_sunspice_gse2sm, date, coord, cmat=cmat, inverse=inverse, errmsg=errmsg
end
