;+
; Project     :	STEREO - SSC
;
; Name        :	STEREO_IGRF_POLE
;
; Purpose     :	Legacy front-end to GET_SUNSPICE_IGRF_POLE
;
; History     :	Version 5, 09-May-2016, WTT, call GET_SUNSPICE_IGRF_POLE
;
; Contact     :	WTHOMPSON
;-
;
pro stereo_igrf_pole, date, theta, phi, errmsg=errmsg
get_sunspice_igrf_pole, date, theta, phi, errmsg=errmsg
end
