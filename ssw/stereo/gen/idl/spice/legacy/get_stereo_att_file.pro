;+
; Project     :	STEREO - SSC
;
; Name        :	GET_STEREO_ATT_FILE
;
; Purpose     :	Legacy front-end to GET_SUNSPICE_KERNEL
;
; History     :	Version 4, 09-May-2016, WTT, call GET_SUNSPICE_KERNEL
;               Version 5, 08-Nov-2016, WTT, fix typo
;
; Contact     :	
;-
;
function get_stereo_att_file, date, spacecraft, $
                                  errmsg=errmsg, _extra=_extra
return, get_sunspice_kernel(date, spacecraft, /attitude, $
                           errmsg=errmsg, _extra=_extra)
end
