;+
; Project     :	STEREO - SSC
;
; Name        :	GET_STEREO_SEP_ANGLE
;
; Purpose     :	Legacy front-end to GET_SUNSPICE_SEP_ANGLE
;
; History     :	Version 6, 09-May-2016, WTT, call GET_SUNSPICE_SEP_ANGLE
;
; Contact     :	WTHOMPSON
;-
;
function get_stereo_sep_angle, date, body1, body2, corr=corr, $
                               degrees=degrees, errmsg=errmsg, _extra=_extra
return, get_sunspice_sep_angle(date, body1, body2, corr=corr, $
                               degrees=degrees, errmsg=errmsg, _extra=_extra)
end
