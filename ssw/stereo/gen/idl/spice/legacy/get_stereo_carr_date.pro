;+
; Project     :	STEREO - SSC
;
; Name        :	GET_STEREO_CARR_DATE()
;
; Purpose     :	Legacy front-end to GET_SUNSPICE_CARR_DATE
;
; History     :	Version 2, 09-May-2016, WTT, Call GET_SUNSPICE_CARR_DATE
;
; Contact     :	WTHOMPSON
;-
;
function get_stereo_carr_date, carr_rot, spacecraft, precision=precision, $
                               tai=k_tai, _extra=_extra
return, get_sunspice_carr_date(carr_rot, spacecraft, precision=precision, $
                               tai=k_tai, _extra=_extra)
end
