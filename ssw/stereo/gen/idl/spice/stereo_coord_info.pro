;+
; Project     :	STEREO - SSC
;
; Name        :	STEREO_COORD_INFO
;
; Purpose     :	Prints out STEREO orbital information
;
; Category    :	STEREO, Orbit
;
; Explanation :	This routine prints out "quick-look" information about the
;               positions of the two STEREO spacecraft, together with similar
;               information about Earth's position.  The information is
;               arranged by viewpoint, with the Behind spacecraft on the left,
;               the Ahead spacecraft on the right, and Earth in between.
;
;               This also serves as a good example of how the various
;               GET_STEREO_* routines work.
;
; Syntax      :	STEREO_COORD_INFO  [, DATE ]
;
; Examples    :	STEREO_COORD_INFO, '2006-05-06T11:30:00'
;
; Inputs      :	None required.
;
; Opt. Inputs :	DATE = The date and time.  This can be input in any format
;                      accepted by ANYTIM2UTC.  The default is the current
;                      date/time.
;
; Outputs     :	Information about the positions of Earth and the two STEREO
;               spacecraft are written to the screen.
;
; Opt. Outputs:	None.
;
; Keywords    : FILENAME = The name of a file to write the output to.  The
;                          default is to write to the terminal.
;
;               Will also accept any LOAD_STEREO_SPICE or ANYTIM2UTC keywords.
;
; Calls       :	GET_STEREO_LONLAT, GET_STEREO_SEP_ANGLE, GET_STEREO_CARR_ROT,
;               LOAD_STEREO_SPICE
;
; Common      :	None.
;
; Restrictions:	This procedure works in conjunction with the Icy/CSPICE
;               package, which is implemented as an IDL Dynamically Loadable
;               Module (DLM).  The Icy source code can be downloaded from
;
;                       ftp://naif.jpl.nasa.gov/pub/naif/toolkit/IDL
;
;               Because this uses dynamic frames, it requires Icy/CSPICE
;               version N0058 or higher.
;
; Side effects:	Will automatically load the SPICE ephemeris files, if not
;               already loaded.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 30-Aug-2005, William Thompson, GSFC
;               Version 2, 12-Sep-2005, William Thompson, GSFC
;                       Accept ANYTIM2UTC keywords
;               Version 3, 26-Oct-2005, William Thompson, GSFC
;                       Fix format code for older versions of IDL
;               Version 4, 20-Jul-2006, William Thompson, GSFC
;                       Added keyword FILENAME
;               Version 5, 26-Dec-2006, William Thompson, GSFC
;                       Added roll information
;               Version 6, 04-Jan-2007, William Thompson, GSFC
;                       Fixed bug where A&B roll angles reversed
;               Version 7, 22-Aug-2007, WTT, add HAE longitude
;               Version 8, 08-Feb-2008, WTT, made compatible with IDLDE
;               Version 9, 03-Apr-2008, WTT, add light travel time
;               Version 10, 13-Nov-2009, WTT, corrected light time order
;
; Contact     :	WTHOMPSON
;-
;
pro stereo_coord_info, date, filename=filename, _extra=_extra
;
on_error, 2
;
;  Interpret the DATE parameter.
;
if n_params() eq 1 then begin
    message = ''
    utc = anytim2utc(date, errmsg=message, _extra=_extra)
    if message ne '' then goto, handle_error
end else get_utc, utc
;
;  Make sure that only one time was entered.
;
if n_elements(utc) gt 1 then begin
    message, /continue, 'Multiple times not supported'
    utc = utc[0]
endif
;
;  Make sure that the SPICE kernels are loaded.
;
message = ''
load_stereo_spice, errmsg=message, _extra=_extra
if message ne '' then goto, handle_error
;
;  Gather the information.
;
carr_a = get_stereo_lonlat(utc, 'A',     /au, /degrees, system='Carrington')
carr_b = get_stereo_lonlat(utc, 'B',     /au, /degrees, system='Carrington')
carr_e = get_stereo_lonlat(utc, 'Earth', /au, /degrees, system='Carrington')
carr = [carr_b[1], carr_e[1], carr_a[1]]
w = where(carr lt 0, count)
if count gt 0 then carr[w] = carr[w] + 360.d0
;
heeq_a = get_stereo_lonlat(utc, 'A',     /au, /degrees, system='HEEQ')
heeq_b = get_stereo_lonlat(utc, 'B',     /au, /degrees, system='HEEQ')
heeq_e = get_stereo_lonlat(utc, 'Earth', /au, /degrees, system='HEEQ')
;
hee_a  = get_stereo_lonlat(utc, 'A',     /au, /degrees, system='HEE')
hee_b  = get_stereo_lonlat(utc, 'B',     /au, /degrees, system='HEE')
hee_e  = get_stereo_lonlat(utc, 'Earth', /au, /degrees, system='HEE')
;
hci_a  = get_stereo_lonlat(utc, 'A',     /au, /degrees, system='HCI')
hci_b  = get_stereo_lonlat(utc, 'B',     /au, /degrees, system='HCI')
hci_e  = get_stereo_lonlat(utc, 'Earth', /au, /degrees, system='HCI')
if hci_a[1] lt 0 then hci_a[1] = hci_a[1] + 360.d0
if hci_b[1] lt 0 then hci_b[1] = hci_b[1] + 360.d0
if hci_e[1] lt 0 then hci_e[1] = hci_e[1] + 360.d0
;
hae_a  = get_stereo_lonlat(utc, 'A',     /au, /degrees, system='HAE')
hae_b  = get_stereo_lonlat(utc, 'B',     /au, /degrees, system='HAE')
hae_e  = get_stereo_lonlat(utc, 'Earth', /au, /degrees, system='HAE')
if hae_a[1] lt 0 then hae_a[1] = hae_a[1] + 360.d0
if hae_b[1] lt 0 then hae_b[1] = hae_b[1] + 360.d0
if hae_e[1] lt 0 then hae_e[1] = hae_e[1] + 360.d0
;
gse_a  = get_stereo_lonlat(utc, 'A', system='GEI')
gse_b  = get_stereo_lonlat(utc, 'B', system='GEI')
c = 2.99773D5
dt_a = gse_a[0] / c / 60
dt_b = gse_b[0] / c / 60
;
semi = (6.96d5 * 648d3 / !dpi / 1.496d8) / [hee_b[0], hee_e[0], hee_a[0]]
;
eroll_a = get_stereo_roll(utc, 'A', system='HAE')
eroll_b = get_stereo_roll(utc, 'B', system='HAE')
sroll_a = get_stereo_roll(utc, 'A', system='HEEQ')
sroll_b = get_stereo_roll(utc, 'B', system='HEEQ')
;
angles = [get_stereo_sep_angle(utc, 'B', 'Earth', /degrees), $
          get_stereo_sep_angle(utc, 'A', 'Earth', /degrees), $
          get_stereo_sep_angle(utc, 'A', 'B',     /degrees)]
;
car_rot= [get_stereo_carr_rot(utc, 'B'), $
          get_stereo_carr_rot(utc, 'Earth'), $
          get_stereo_carr_rot(utc, 'A')]
;
;  Open the output file.
;
if n_elements(filename) eq 1 then openw, lun, filename, /get_lun else lun=-1
;
;  Print the information
;
printf, lun, ''
printf, lun, format="(32X, 'STEREO-B', 11X, 'Earth', 8X, 'STEREO-A')"
printf, lun, ''
printf, lun, 'Heliocentric distance (AU)', hee_b[0], hee_e[0], hee_a[0], $
  format='(A,T31,F10.6,6X,F10.6,6X,F10.6)'
;
format = '(A,T31,F10.3,6X,F10.3,6X,F10.3)'
printf, lun, format=format, 'Semidiameter (arcsec)', semi
printf, lun, ''
printf, lun, 'HCI longitude', hci_b[1], hci_e[1], hci_a[1], format=format
printf, lun, 'HCI latitude ', hci_b[2], hci_e[2], hci_a[2], format=format
printf, lun, ''
printf, lun, 'Carrington longitude', carr, format=format
printf, lun, 'Carrington rotation number', car_rot, format=format
printf, lun, ''
printf, lun, 'Heliographic (HEEQ) longitude', heeq_b[1], heeq_e[1], heeq_a[1],$
  format=format
printf, lun, 'Heliographic (HEEQ) latitude ', heeq_b[2], heeq_e[2], heeq_a[2],$
  format=format
printf, lun, ''
printf, lun, 'HAE longitude', hae_b[1], hae_e[1], hae_a[1], format=format
printf, lun, ''
printf, lun, 'Earth Ecliptic (HEE) longitude', hee_b[1], hee_e[1], hee_a[1], $
  format=format
printf, lun, 'Earth Ecliptic (HEE) latitude ', hee_b[2], hee_e[2], hee_a[2], $
  format=format
printf, lun, ''
rformat = '(A,T31,F10.3,22X,F10.3)'
printf, lun, 'Roll from ecliptic north', eroll_b, eroll_a, format=rformat
printf, lun, 'Roll from solar north',    sroll_b, sroll_a, format=rformat
printf, lun, ''
printf, lun, 'Light travel time to Earth (min)', dt_b, dt_a, $
  format='(A,6X,F10.3,6X,F10.3)'
printf, lun, ''
printf, lun, 'Separation angle with Earth', angles[0], angles[1], $
  format='(A,11X,F10.3,6X,F10.3)'
printf, lun, 'Separation angle A with B', angles[2], format='(A,21X,F10.3)'
;
if lun gt 0 then free_lun, lun
return
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message else $
  errmsg = 'stereo_coord_info: ' + message
;
end
