;+
; Project     :	STEREO - SSC - SOHO
;
; Name        :	LOAD_SOHO_SPICE
;
; Purpose     :	Load the SOHO SPICE ephemerides for use by STEREO software
;
; Category    :	STEREO, SOHO, Orbit
;
; Explanation :	Loads the SOHO ephemeris files in SPICE format.  Also calls
;               LOAD_STEREO_SPICE to load the STEREO kernels.
;
; Syntax      :	LOAD_SOHO_SPICE
;
; Inputs      :	None.
;
; Opt. Inputs :	None.
;
; Outputs     :	None.
;
; Opt. Outputs:	None.
;
; Keywords    : VERBOSE= If set, then print a message for each file loaded.
;
;               ERRMSG = If defined and passed, then any error messages will be
;                        returned to the user in this parameter rather than
;                        depending on the MESSAGE routine in IDL.  If no errors
;                        are encountered, then a null string is returned.  In
;                        order to use this feature, ERRMSG must be defined
;                        first, e.g.
;
;                               ERRMSG = ''
;                               LOAD_SOHO_SPICE, ERRMSG=ERRMSG
;                               IF ERRMSG NE '' THEN ...
;
; Calls       :	TEST_SPICE_ICY_DLM, LOAD_STEREO_SPICE, SPICE_KERNEL_REPORT,
;               MATCH, CSPICE_FURNSH
;
; Common      :	SOHO_SPICE contains the names of the loaded files.
;
; Env. Vars.  : SOHO_SPICE points to the directory containing the SOHO SPICE
;               ephemerides (soho_*.bsp).
;
; Restrictions:	This procedure works in conjunction with the Icy/CSPICE
;               package, which is implemented as an IDL Dynamically Loadable
;               Module (DLM).  The Icy source code can be downloaded from
;
;                       ftp://naif.jpl.nasa.gov/pub/naif/toolkit/IDL
;
;               Because this uses dynamic frames, it requires Icy/CSPICE
;               version N0058 or higher.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 09-Sep-2008, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
pro load_soho_spice, verbose=verbose, errmsg=errmsg
common soho_spice, ephem
on_error, 2
;
;  Make sure that the SPICE/Icy DLM is available.
;
if not test_spice_icy_dlm() then begin
    message = 'SPICE/Icy DLM not available'
    goto, handle_error
endif
;
;  If EPHEM is not yet defined, get a list of all ephemeris files
;
if n_elements(ephem) eq 0 then begin
    soho_spice = getenv('SOHO_SPICE')
    files = file_search(concat_dir(soho_spice, '*.bsp'), count=count)
    if count eq 0 then begin
        message = 'No SOHO ephemeris files found'
        goto, handle_error
    endif
    ephem = files
endif
;
;  Make sure the STEREO kernels are loaded.
;
message = ''
load_stereo_spice, verbose=verbose, errmsg=message
if message ne '' then goto, handle_error
;
;  Get a list of currently loaded kernels, and match it against the SOHO
;  ephemeris list.  Load any kernels not already loaded.
;
spice_kernel_report, kernels=kernels, /quiet
match, ephem, kernels, wephem, wkernels, count=count
if count ne n_elements(ephem) then for i=0,n_elements(ephem)-1 do begin
    w = where(ephem[i] eq kernels, count)
    if count eq 0 then begin
        cspice_furnsh, ephem[i]
        if keyword_set(verbose) then print, 'Loaded ' + ephem[i]
    endif
endfor
;
return
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message else $
  errmsg = 'load_soho_spice: ' + message
;
end
