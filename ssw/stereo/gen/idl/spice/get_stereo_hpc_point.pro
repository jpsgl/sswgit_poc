;+
; Project     :	STEREO - SSC
;
; Name        :	GET_STEREO_HPC_POINT
;
; Purpose     :	Returns the helioprojective pointing of STEREO A or B
;
; Category    :	STEREO, Orbit
;
; Explanation :	This routine returns the helioprojective-cartesian pointing of
;               one of the two STEREO spacecraft.  The yaw (X) and pitch (Y)
;               are returned in arc seconds, and the roll is returned in
;               degrees.
;
; Syntax      :	Pointing = GET_STEREO_HPC_POINT( DATE, SPACECRAFT )
;
; Examples    :	Pointing = GET_STEREO_HPC_POINT( '2006-05-06T11:30:00', 'A' )
;
; Inputs      :	DATE       = The date and time.  This can be input in any
;                            format accepted by ANYTIM2UTC, and can also be an
;                            array of values.
;
;               SPACECRAFT = Can be one of the following forms:
;
;                               'A'             'B'
;                               'STA'           'STB'
;                               'Ahead'         'Behind'
;                               'STEREO Ahead'  'STEREO Behind'
;                               'STEREO-Ahead'  'STEREO-Behind'
;                               'STEREO_Ahead'  'STEREO_Behind'
;
;                            Case is not important.  The NAIF numeric codes of
;                            -234 and -235 respectively can also be used.
;
; Opt. Inputs :	None.
;
; Outputs     :	The result of the function is a three-element vector containing
;               in order the yaw (X) and pitch (Y) in arcseconds, and the roll
;               angle in degrees.  If DATE is a vector, then the result will
;               have additional dimensions.
;
; Opt. Outputs:	None.
;
; Keywords    : INSTRUMENT = The name of a STEREO instrument or sub-instrument
;                            with a defined reference frame.  This capability
;                            is not yet implemented--this keyword is included
;                            as a placeholder for when it is.
;
;                            The default is to return the C-matrix for the
;                            spacecraft as a whole, rather than for any
;                            specific instrument.
;
;               TOLERANCE = The tolerance to be used when looking for pointing
;                            information, in seconds.  The default is 1000.
;
;               FOUND  = Byte array containing whether or not the pointings
;                        were found.
;
;               DEGREES= If set, then the units for all three parameters will
;                        be degrees.
;
;               RADIANS= If set, then the units for all three parameters will
;                        be radians.
;
;               ERRMSG = If defined and passed, then any error messages will be
;                        returned to the user in this parameter rather than
;                        depending on the MESSAGE routine in IDL.  If no errors
;                        are encountered, then a null string is returned.  In
;                        order to use this feature, ERRMSG must be defined
;                        first, e.g.
;
;                               ERRMSG = ''
;                               Pnt = GET_STEREO_HPC_POINT( ERRMSG=ERRMSG, ... )
;                               IF ERRMSG NE '' THEN ...
;
;               Will also accept any LOAD_STEREO_SPICE or ANYTIM2UTC keywords.
;
; Calls       :	DATATYPE, GET_STEREO_CMAT, CSPICE_M2EUL, PARSE_STEREO_NAME
;
; Common      :	None.
;
; Restrictions:	At least one CK file must be loaded.
;
;               This procedure works in conjunction with the Icy/CSPICE
;               package, which is implemented as an IDL Dynamically Loadable
;               Module (DLM).  The Icy source code can be downloaded from
;
;                       ftp://naif.jpl.nasa.gov/pub/naif/toolkit/IDL
;
;               Because this uses dynamic frames, it requires Icy/CSPICE
;               version N0058 or higher.
;
; Side effects:	Will automatically load the SPICE ephemeris files, if not
;               already loaded.
;
; Prev. Hist. :	Based on STEREO_POINTING_DEMO
;
; History     :	Version 1, 17-Oct-2005, William Thompson, GSFC
;               Version 2, 01-Sep-2006, William Thompson, GSFC
;                       Added call to PARSE_STEREO_NAME
;               Version 3, 16-Apr-2006, William Thompson, GSFC
;                       Fix bug with short integers in loop.
;
; Contact     :	WTHOMPSON
;-
;
function get_stereo_hpc_point, date, spacecraft, found=found, $
                               precess=precess, instrument=instrument, $
                               tolerance=tolerance, degrees=degrees, $
                               radians=radians, errmsg=errmsg, _extra=_extra
;
on_error, 2
if n_params() ne 2 then begin
    message = 'Syntax:  Pointing = GET_STEREO_HPC_POINT( DATE, SPACECRAFT )'
    goto, handle_error
endif
;
;  Define the units.
;
roll_units = 180.d0 / !dpi
xy_units = roll_units * 3600.d0
if keyword_set(degrees) then xy_units = roll_units
if keyword_set(radians) then begin
    roll_units = 1
    xy_units   = 1
endif
;
;  Determine which spacecraft was requested, and translate it into the proper
;  input for SPICE.
;
inst = 0L
sc_ahead  = '-234'
sc_behind = '-235'
if datatype(spacecraft,1) eq 'String' then $ 
  sc = parse_stereo_name(spacecraft, [sc_ahead, sc_behind]) else $
  sc = spacecraft
;
if (sc ne sc_ahead) and (sc ne sc_behind) then begin
    message = 'Unable to recognize spacecraft ' + strtrim(sc,2)
    goto, handle_error
endif
;
;  Start by deriving the C-matrices.  Make sure that DATE is treated as a
;  vector.
;
message = ''
cmat = get_stereo_cmat(date[*], spacecraft, system='HPC', found=found, $
                       instrument=instrument, tolerance=tolerance, $
                       errmsg=message, _extra=_extra)
if message ne '' then goto, handle_error
;
sz = size(cmat)
if sz[0] eq 2 then begin
    n = 1
    pointing = dblarr(3)
end else begin
    n = sz[3]
    pointing = dblarr(3,n)
endelse
;
halfpi = !dpi / 2.d0
twopi  = !dpi * 2.d0
for i=0L,n-1L do begin
    cspice_m2eul, cmat[*,*,i], 1, 3, 2, roll, pitch, yaw
    if sc eq sc_behind then begin
        roll = roll + !dpi
        if roll gt !dpi then roll = roll - twopi
    endif
    pointing[0,i] = (halfpi - yaw) * xy_units
    pointing[1,i] = pitch * xy_units
    pointing[2,i] = -roll * roll_units
endfor
;
;  Reformat the output arrays to match the input date/time array.
;
if n gt 1 then begin
    sz = size(date)
    dim = [3,sz[1:sz[0]]]
    pointing = reform(pointing, dim, /overwrite)
endif
;
return,pointing
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message else $
  errmsg = 'get_stereo_hpc_point: ' + message
;
end
