;+
; Project     :	STEREO - SSC
;
; Name        :	CONVERT_STEREO_LONLAT
;
; Purpose     :	Converts between angular coordinate systems
;
; Category    :	STEREO, Orbit
;
; Explanation :	This routine converts angular coordinate arrays, such as those
;               returned by GET_STEREO_LONLAT, from one coordinate system to
;               another.  The angular coordinates are first converted to X,Y,Z
;               triplets, which are then passed to CONVERT_STEREO_COORD.  The
;               result is then converted back into angular coordinates.
;
; Syntax      :	CONVERT_STEREO_LONLAT, DATE, COORD, FROM, TO
;
; Examples    :	CONVERT_STEREO_LONLAT, '2006-05-06T11:30', COORD, 'HCI', 'GSE'
;
; Inputs      :	DATE    = The date and time.  This can be input in any format
;                         accepted by ANYTIM2UTC, and can also be an array of
;                         values.
;
;               COORD   = Either a three-value state vector, containing the
;                         radius in kilometers, and the longitude and latitude
;                         in radians, or a two-value state vector with just the
;                         longitude and latitude.  In the latter case,
;                         coordinate system origins are ignored.  Can also be a
;                         3xN or 2xN array.  If DATE is a vector, then N must
;                         be the size of DATE.
;
;               FROM    = Character string, giving one of the following
;                         standard coordinate systems to convert from:
;
;                               GEI     Geocentric Equatorial Inertial
;                               GEO     Geographic
;                               GSE     Geocentric Solar Ecliptic
;                               MAG     Geomagnetic
;                               GSM     Geocentric Solar Magnetospheric
;                               SM      Solar Magnetic
;                               HCI     Heliocentric Inertial
;                               HAE     Heliocentric Aries Ecliptic
;                               HEE     Heliocentric Earth Ecliptic
;                               HEEQ    Heliocentric Earth Equatorial (or HEQ)
;                               Carrington (can be abbreviated)
;                               HGRTN   Heliocentric Radial-Tangential-Normal
;                               HPC     Helioprojective-Cartesian
;                               RTN     Radial-Tangential-Normal
;                               SCI     STEREO Science Pointing
;                               HERTN   Heliocentric Ecliptic RTN
;
;                         Case is not important.  The last five require that
;                         the SPACECRAFT keyword be passed.
;
;               TO      = Character string, as above, giving the coordinate
;                         system to convert to.
;
; Opt. Inputs :	None.
;
; Outputs     :	COORD   = Returned as the converted coordinates.
;
; Opt. Outputs:	None.
;
; Keywords    : DEGREES = If set, then the longitude and latitude are in units
;                         of degrees, rather than radians.
;
;               IGNORE_ORIGIN = If set, the origins of the FROM and TO
;                         coordinate systems are ignored.  This is used for
;                         vectors which only indicate pointing, such as the
;                         direction of a star.  /IGNORE_ORIGIN is set
;                         automatically when only the longitude and latitude
;                         are passed.
;
;               ERRMSG  = If defined and passed, then any error messages will
;                         be returned to the user in this parameter rather than
;                         depending on the MESSAGE routine in IDL.  If no
;                         errors are encountered, then a null string is
;                         returned.  In order to use this feature, ERRMSG must
;                         be defined first, e.g.
;
;                               ERRMSG = ''
;                               CONVERT_STEREO_LONLAT, ERRMSG=ERRMSG, ...
;                               IF ERRMSG NE '' THEN ...
;
;               Will also accept any CONVERT_STEREO_COORD keywords, which is
;               where most of the work is done.  See the documentation for that
;               routine for more information.
;
; Calls       :	ANYTIM2UTC, CSPICE_STR2ET, CSPICE_SPKEZR, CSPICE_PXFORM,
;               CSPICE_SXFORM, LOAD_STEREO_SPICE, STEREO_GEO2MAG,
;               STEREO_GSE2GSM, STEREO_GSE2SM
;
; Common      :	None.
;
; Restrictions:	Does not currently handle planetographic coordinates.
;
;               This procedure works in conjunction with the Icy/CSPICE
;               package, which is implemented as an IDL Dynamically Loadable
;               Module (DLM).  The Icy source code can be downloaded from
;
;                       ftp://naif.jpl.nasa.gov/pub/naif/toolkit/IDL
;
;               Because this uses dynamic frames, it requires Icy/CSPICE
;               version N0058 or higher.
;
; Side effects:	Will automatically load the SPICE ephemeris files, if not
;               already loaded.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 03-Feb-2006, William Thompson, GSFC
;               Version 2, 03-Oct-2006, William Thompson, GSFC
;                       Add support for HPC coordinates
;               Version 3, 13-Aug-2009, WTT, keep track of NaN values
;
; Contact     :	WTHOMPSON
;-
;
pro convert_stereo_lonlat, date, coord, k_system_from, k_system_to, $
                           degrees=degrees, ignore_origin=ignore_origin, $
                           errmsg=errmsg, _extra=_extra
;
;  Check the input parameters.
;
on_error, 2
if n_params() ne 4 then begin
    message = 'Syntax:  CONVERT_STEREO_LONLAT, DATE, COORD, FROM, TO'
    goto, handle_error
endif
;
n_date = n_elements(date)
if n_date eq 0 then begin
    message = 'DATE not defined'
    goto, handle_error
endif
;
sz = size(coord)
if sz[0] eq 0 then begin
    message = 'COORD must be an array'
    goto, handle_error
endif
;
n_vec = sz[1]
if (n_vec ne 2) and (n_vec ne 3) then begin
    message = 'First dimension of COORD must be either 2 or 3'
    goto, handle_error
endif
;
if sz[0] gt 1 then n_coord = product(sz[2:sz[0]]) else n_coord = 1
if (n_date gt 1) and (n_date ne n_coord) then begin
    message = 'Incompatible DATE and COORD arrays'
    goto, handle_error
endif
;
;  If necessary, reform the coordinate array to be two-dimensional.
;
if sz[0] gt 2 then coord = reform(coord, n_vec, n_coord, /overwrite)
;
;  Create an X,Y,Z array.
;
state = dblarr(3,n_coord)
if n_vec eq 2 then begin
    radius = 1
    lon = coord[0,*]
    lat = coord[1,*]
end else begin
    radius = coord[0,*]
    lon = coord[1,*]
    lat = coord[2,*]
endelse
if keyword_set(degrees) then begin
    dtor = !dpi / 180.d0
    lon = dtor * lon
    lat = dtor * lat
endif
;
;  If the input system is HPC, then convert to RTN first.
;
if n_elements(k_system_from) ne 1 then begin
    message = 'SYSTEM_FROM not properly defined'
    goto, handle_error
endif
system_from = strupcase(k_system_from)
if system_from eq 'HPC' then begin
    system_from = 'RTN'
    lon = !dpi - lon
endif
;
state[0,*] = radius * cos(lat) * cos(lon)
state[1,*] = radius * cos(lat) * sin(lon)
state[2,*] = radius * sin(lat)
;
;  If HPC was selected, calculate using RTN and apply a correction.
;
if n_elements(k_system_to) ne 1 then begin
    message = 'SYSTEM_TO not properly defined'
    goto, handle_error
endif
system_to = strupcase(k_system_to)
if system_to eq 'HPC' then begin
    system_to = 'RTN'
    hpc_conv = 1
end else hpc_conv = 0
;
;  Call CONVERT_STEREO_COORD to convert between coordinate systems.
;
ignore = keyword_set(ignore_origin) or (n_vec eq 2)
if system_from ne system_to then convert_stereo_coord, date, state, $
  system_from, system_to, ignore_origin=ignore, _extra=_extra
;
;  Translate back into radius, longitude, and latitude
;
cspice_reclat, state, radius, longitude, latitude
;
;  Preserve missing (NaN) pixels.
;
wmissing = where(total(finite(state),1) ne 3, nmissing)
if nmissing gt 0 then begin
    flag_missing, radius,    wmissing
    flag_missing, longitude, wmissing
    flag_missing, latitude,  wmissing
endif
;
;  If HPC, then apply a correction to the RTN coordinates.
;
if hpc_conv then begin
    twopi = 2.d0 * !dpi
    longitude = !dpi - longitude
    w = where(longitude gt !dpi, count)
    if count gt 0 then longitude[w] = longitude[w] - twopi
    w = where(longitude lt -!dpi, count)
    if count gt 0 then longitude[w] = longitude[w] + twopi
endif
;
if keyword_set(degrees) then begin
    longitude = (180.d0 / !dpi) * longitude 
    latitude  = (180.d0 / !dpi) * latitude
endif
if n_vec eq 2 then begin
    coord[0,*] = longitude
    coord[1,*] = latitude
end else begin      
    coord[0,*] = radius
    coord[1,*] = longitude
    coord[2,*] = latitude
endelse
;
;  If necessary, restore COORD to its original dimensions.
;
if sz[0] gt 2 then coord = reform(coord, [n_vec,sz[2:sz[0]]], /overwrite)
return
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message else $
  errmsg = 'convert_stereo_lonlat: ' + message
;
end
