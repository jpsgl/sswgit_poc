;+
; Project     :	STEREO - SSC
;
; Name        :	GET_STEREO_SPICE_KERNEL
;
; Purpose     :	Returns the SPICE kernel containing STEREO ephemeris
;
; Category    :	STEREO, Orbit
;
; Explanation :	This routine checks through the various loaded kernels, and
;               returns the name of the kernel containing the ephemeris or
;               attitude history information for a given date and STEREO
;               observatory.
;
; Syntax      :	Kernel = GET_STEREO_SPICE_KERNEL( DATE, SPACECRAFT )
;
; Examples    :	Kernel = GET_STEREO_SPICE_KERNEL( '2006-05-06T11:30:00', 'A' )
;
; Inputs      :	DATE       = The date and time.  This can be input in any
;                            format accepted by ANYTIM2UTC, and can also be an
;                            array of values.
;
;               SPACECRAFT = Can be one of the following forms:
;
;                               'A'             'B'
;                               'STA'           'STB'
;                               'Ahead'         'Behind'
;                               'STEREO Ahead'  'STEREO Behind'
;                               'STEREO-Ahead'  'STEREO-Behind'
;                               'STEREO_Ahead'  'STEREO_Behind'
;
;                            Case is not important.  The NAIF numeric codes of
;                            -234 and -235 respectively can also be used.
;
; Opt. Inputs :	None.
;
; Outputs     :	The result of the function is the name of the loaded kernel(s)
;               containing the ephemeris or attitude history information.  The
;               null string is returned for any cases without a match.
;
; Opt. Outputs:	None.
;
; Keywords    : ATTITUDE = If set, then the program will look for attitude
;                          history files instead of ephemeris files.
;
;               ERRMSG = If defined and passed, then any error messages will be
;                        returned to the user in this parameter rather than
;                        depending on the MESSAGE routine in IDL.  If no errors
;                        are encountered, then a null string is returned.  In
;                        order to use this feature, ERRMSG must be defined
;                        first, e.g.
;
;                               ERRMSG = ''
;                               Kernel = GET_STEREO_SPICE_KERNEL( ERRMSG=ERRMSG, ... )
;                               IF ERRMSG NE '' THEN ...
;
;               Will also accept any LOAD_STEREO_SPICE or ANYTIM2UTC keywords.
;
; Calls       :	PARSE_STEREO_NAME, LOAD_STEREO_SPICE, BREAK_FILE,
;               GET_STEREO_SPICE_RANGE, ANYTIM2UTC
;
; Common      :	None.
;
; Restrictions:	This procedure works in conjunction with the Icy/CSPICE
;               package, which is implemented as an IDL Dynamically Loadable
;               Module (DLM).  The Icy source code can be downloaded from
;
;                       ftp://naif.jpl.nasa.gov/pub/naif/toolkit/IDL
;
; Side effects:	Will automatically load the SPICE ephemeris files, if not
;               already loaded.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 20-Mar-2006, William Thompson, GSFC
;               Version 2, 01-Sep-2006, William Thompson, GSFC
;                       Added call to PARSE_STEREO_NAME
;               Version 3, 30-Nov-2006, William Thompson, GSFC
;                       Recognize STA_* STB_* filenames
;
; Contact     :	WTHOMPSON
;-
;
function get_stereo_spice_kernel, date, spacecraft, attitude=attitude, $
                                  errmsg=errmsg, _extra=_extra
;
on_error, 2
if n_params() ne 2 then begin
    message = 'Syntax:  Kernel = GET_STEREO_SPICE_KERNEL( DATE, SPACECRAFT )'
    goto, handle_error
endif
;
;  Determine which spacecraft (or planetary body) was requested, and translate
;  it into the proper input for SPICE.
;
sc  = parse_stereo_name(spacecraft, ['ahead', 'behind'])
alt = parse_stereo_name(spacecraft, ['STA', 'STB'])
;
;  Convert the date/time to UTC.
;
message = ''
utc = anytim2utc(date, /ccsds, errmsg=message, _extra=_extra)
if message ne '' then goto, handle_error
;
;  Initialize the KERNEL array.
;
kernel = utc
kernel[*] = ''
;
;  Make sure that the ephemeris files are loaded.
;
message = ''
load_stereo_spice, errmsg=message, _extra=_extra
if message ne '' then goto, handle_error
;
;  Get a report of all loaded kernels.
;
spice_kernel_report, kernels=loaded, /quiet, errmsg=message
if message ne '' then goto, handle_error
;
;  Find the kernels specific to the requested spacecraft.
;
break_file, loaded, disk, dir, name, ext
w = where((strmid(name,0,strlen(sc))  eq sc) or $
          (strmid(name,0,strlen(alt)) eq alt), count)
if count eq 0 then return, kernel       ;No matches found
loaded = loaded[w]
name = name[w]
ext = ext[w]
;
;  Find the kernels with the correct extension.
;
len = strlen(ext)
if keyword_set(attitude) then test = '.bc' else test = '.bsp'
w = where(strpos(ext,test) ge 0, count)
if count eq 0 then return, kernel       ;No matches found
loaded = loaded[w]
name = name[w]
ext = ext[w]
;
;  Step through the cases, and get the matches.
;
for i=0,n_elements(loaded)-1 do begin
    message = ''
    get_stereo_spice_range, loaded[i], date0, date1, /ccsds, errmsg=message
    if message ne '' then goto, handle_error
    w = where((utc ge date0) and (utc le date1), count)
    if count gt 0 then kernel[w] = name[i] + ext[i]
endfor
;
return, kernel
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message else $
  errmsg = 'get_stereo_spice_kernel: ' + message
;
end
