;+
; Project     :	STEREO - SSC
;
; Name        :	GET_STEREO_CARR_ROT
;
; Purpose     :	Returns the decimal Carrington rotation number
;
; Category    :	STEREO, Orbit
;
; Explanation :	This routine returns the decimal Carrington rotation number
;               of disk center, either for Earth, or for one of the two STEREO
;               spacecraft.
;
; Syntax      :	Carr = GET_STEREO_CARR_ROT( DATE  [, SPACECRAFT ] )
;
; Examples    :	Carr = GET_STEREO_CARR_ROT( '2006-05-06T11:30:00', 'A' )
;
; Inputs      :	DATE       = The date and time.  This can be input in any
;                            format accepted by ANYTIM2UTC, and can also be an
;                            array of values.
;
; Opt. Inputs :	SPACECRAFT = Can be one of the following forms:
;
;                               'A'             'B'
;                               'STA'           'STB'
;                               'Ahead'         'Behind'
;                               'STEREO Ahead'  'STEREO Behind'
;                               'STEREO-Ahead'  'STEREO-Behind'
;
;                            Case is not important.  The NAIF numeric codes of
;                            -234 and -235 respectively can also be used.
;
;                            Also can be the name of a solar system body,
;                            e.g. "Earth", "Mars", "Moon", etc.  If not passed,
;                            the default is "Earth".
;
; Outputs     :	The result of the function is the decimal Carrington rotation
;               number.
;
; Opt. Outputs:	None.
;
; Keywords    : INTEGER = If set, then the FLOOR() function is called to return
;                         the integer Carrington rotation number of disk
;                         center.
;
;               ERRMSG = If defined and passed, then any error messages will be
;                        returned to the user in this parameter rather than
;                        depending on the MESSAGE routine in IDL.  If no errors
;                        are encountered, then a null string is returned.  In
;                        order to use this feature, ERRMSG must be defined
;                        first, e.g.
;
;                               ERRMSG = ''
;                               Carr = GET_STEREO_CARR_ROT(ERRMSG=ERRMSG, ...)
;                               IF ERRMSG NE '' THEN ...
;
;               Will also accept any LOAD_STEREO_SPICE or ANYTIM2UTC keywords.
;
; Calls       :	LOAD_STEREO_SPICE, GET_STEREO_LONLAT, TIM2CARR
;
; Common      :	None.
;
; Restrictions:	This procedure works in conjunction with the Icy/CSPICE
;               package, which is implemented as an IDL Dynamically Loadable
;               Module (DLM).  The Icy source code can be downloaded from
;
;                       ftp://naif.jpl.nasa.gov/pub/naif/toolkit/IDL
;
;               Because this uses dynamic frames, it requires Icy/CSPICE
;               version N0058 or higher.
;
; Side effects:	Will automatically load the SPICE ephemeris files, if not
;               already loaded.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 30-Aug-2005, William Thompson, GSFC
;               Version 2, 01-Sep-2005, William Thompson, GSFC
;                       Corrected sign error
;               Version 3, 12-Sep-2005, William Thompson, GSFC
;                       Accept ANYTIM2UTC keywords
;
; Contact     :	WTHOMPSON
;-
;
function get_stereo_carr_rot, date, spacecraft, errmsg=errmsg, _extra=_extra
;
on_error, 2
twopi = 2.d0 * !dpi
;
if n_params() lt 1 then begin
    message = 'Syntax:  Carr = GET_STEREO_CARR_ROT( DATE  [, SPACECRAFT ] )'
    goto, handle_error
endif
;
;  Interpret the DATE parameter.
;
message = ''
utc = anytim2utc(date, errmsg=message, _extra=_extra)
if message ne '' then goto, handle_error
;
;  Make sure that the SPICE kernels are loaded.
;
message = ''
load_stereo_spice, errmsg=message, _extra=_extra
if message ne '' then goto, handle_error
;
;  Start by calling TIM2CARR to get the approximate Carrington rotation number.
;
carr_rot = tim2carr(utc, /dc)
if n_elements(utc) eq 1 then carr_rot = carr_rot[0]
;
;  Get the Carrington longitude of Earth, and calculate the fractional part of
;  the decimal rotation number.
;
earth_lon = get_stereo_lonlat(utc, 'Earth', system='Carrington')
if n_elements(utc) eq 1 then earth_lon = earth_lon[1] else $
  earth_lon = earth_lon[1,*,*,*,*,*,*,*]
w = where(earth_lon lt 0, count)
if count gt 0 then earth_lon[w] = earth_lon[w] + twopi
frac = 1.d0 - earth_lon / twopi
;
;  Subtract the fractional part, and round off to get the integer Carrington
;  rotation number.
;
n_carr = round(carr_rot - frac)
diff = carr_rot - frac - n_carr
max_diff = max(abs(diff))
if max_diff gt 0.1 then message, /informational, $
  'Excessive residual ' + ntrim(max_diff)
;
;  Calculate the decimal Carrington rotation number from the integer rotation
;  number, pluse the fraction.
;
carr_rot = n_carr + frac
;
;  If the SPACECRAFT parameter was passed, then calculate the angular offset
;  from the Earth-Sun line in heliographic (HEEQ) coordinates.
;
if n_elements(spacecraft) eq 1 then begin
    body_lon = get_stereo_lonlat(utc, spacecraft, system='HEEQ')
    if n_elements(utc) eq 1 then body_lon = body_lon[1] else $
      body_lon = body_lon[1,*,*,*,*,*,*,*]
    carr_rot = carr_rot - body_lon / twopi
endif
;
if keyword_set(integer) then carr_rot = floor(carr_rot)
return, carr_rot
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message else $
  errmsg = 'get_stereo_carr_rot: ' + message
;
end
