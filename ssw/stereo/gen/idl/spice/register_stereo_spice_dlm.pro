;+
; Project     :	STEREO - SSC
;
; Name        :	REGISTER_STEREO_SPICE_DLM
;
; Purpose     :	Register the SPICE DLM
;
; Category    :	STEREO, Orbit
;
; Explanation :	Finds the correct icy.dlm file, and registers it.
;
; Syntax      :	REGISTER_STEREO_SPICE_DLM
;
; Inputs      :	None.
;
; Opt. Inputs :	None.
;
; Outputs     :	None.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Calls       :	FILE_EXIST
;
; Common      :	None.
;
; Env. Vars.  : SPICE_ICY_DLM points to where the DLM file is found.  If not
;               defined, the program looks within the SolarSoft tree, based on
;               the operating system and architecture.
;
; Restrictions:	IDL needs to be run in 32 bit mode under Solaris to use the
;               SPICE package.
;
;               This procedure works in conjunction with the Icy/CSPICE
;               package, which is implemented as an IDL Dynamically Loadable
;               Module (DLM).  The Icy source code can be downloaded from
;
;                       ftp://naif.jpl.nasa.gov/pub/naif/toolkit/IDL
;
;               Because this uses dynamic frames, it requires Icy/CSPICE
;               version N0058 or higher.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 12-Apr-2006, William Thompson, GSFC
;               Version 2, 20-May-2008, WTT, look for 64-bit versions.
;               Version 3, 22-Jul-2008, WTT, make sure "_64" not in twice.
;               Version 4, 07-Nov-2011, WTT, check for Mac OSX < 10.6.8
;
; Contact     :	WTHOMPSON
;-
;
pro register_stereo_spice_dlm
;
;  First, see if the environment variable SPICE_ICY_DLM is defined.
;
spice_icy_dlm = getenv('SPICE_ICY_DLM')
;
;  Otherwise, try to pick it up from the SolarSoft distribution.
;
if spice_icy_dlm eq '' then begin
    os = !version.os
    arch = !version.arch
    vers = os + '_' + arch
;
;  Check to see if the older version of Darwin needs to be used.  This is
;  expected to be a temporary solution until a new version of SPICE is
;  released.
;
    if (os eq 'darwin') and (arch ne 'ppc') then begin
        spawn, 'sw_vers', output, /noshell
        old = 0
        for i=0,n_elements(output)-1 do begin
            words = strsplit(output[i], /extract)
            if words[0] eq 'ProductVersion:' then old = words[1] lt '10.6.8'
        endfor
        if old then vers = 'old_' + vers
    endif
;
    file = concat_dir(getenv('SSW'), 'stereo/gen/exe/icy/' + vers)
    if !version.memory_bits eq 64 then begin
        test = strmid(file,strlen(file)-3,3)
        if test ne '_64' then file = file + '_64'
    endif
    file = file + '/lib/icy.dlm'
    if file_exist(file) then spice_icy_dlm = file
endif
;
if spice_icy_dlm ne '' then begin
    print, 'Registering DLM ' + spice_icy_dlm
    dlm_register, spice_icy_dlm
endif
;
return
end
