FUNCTION PARSE_STEREO_PKT, PACKET, GRH_DATE=GRH_DATE, FRAME_QUAL=FRAME_QUAL, $
                           ARCHIVE_FLAG=ARCHIVE_FLAG, FRAME_VER=FRAME_VER, $
                           FRAME_SC_ID=FRAME_SC_ID, VIR_CHAN_ID=VIR_CHAN_ID, $
                           OP_CONTROL_FLAG=OP_CONTROL_FLAG, $
                           FRM_SEC_HDR_FLAG=FRM_SEC_HDR_FLAG, $
                           SYNC_FLAG=SYNC_FLAG, PKT_ORDER_FLAG=PKT_ORDER_FLAG,$
                           SEG_LEN_ID=SEG_LEN_ID, FIRST_HDR_PTR=FIRST_HDR_PTR,$
                           FRM_SEC_HDR_VER=FRM_SEC_HDR_VER, $
                           FRM_SEC_HDR_LEN=FRM_SEC_HDR_LEN, FRM_DATE=FRM_DATE,$
                           PKT_VER=PKT_VER, PKT_TYPE=PKT_TYPE, $
                           PKT_SEC_HDR_FLAG=PKT_SEC_HDR_FLAG, APID=APID, $
                           SUBSYS_ID=SUBSYS_ID, DATA_FMT_ID=DATA_FMT_ID, $
                           GROUP_FLAG=GROUP_FLAG, SEQ_COUNT=SEQ_COUNT, $
                           PKT_DATE=PKT_DATE, RAW=RAW, TAI=TAI, _EXTRA=_EXTRA
;+
; Project     :	STEREO - SSC
;
; Name        :	PARSE_STEREO_PKT()
;
; Purpose     :	Extract information from STEREO packets
;
; Category    :	STEREO, Telemetry
;
; Explanation :	Extracts information from STEREO packets read by
;               READ_STEREO_PKT, which can't be obtained by simply browsing the
;               packet structure.
;
; Syntax      :	OUT = PARSE_STEREO_PKT( PACKET, /keyword )
;
; Examples    :	GRH_DATE = PARSE_STEREO_PKT( PACKET, /GRH_DATE )
;
; Inputs      :	PACKET = Packet structure read by READ_STEREO_PKT.
;
; Opt. Inputs :	None.
;
; Outputs     :	The result of the function is the extracted information.
;               Date/times are returned in CDS internal UTC format.
;
; Opt. Outputs:	None.
;
; Keywords    :	One, and only one, of the following keywords should be used:
;
;               Ground Receipt Header keywords:
;                       /GRH_DATE
;                       /FRAME_QUAL
;                       /ARCHIVE_FLAG
;               Frame information keywords (STP packets only):
;                       /FRAME_VER
;                       /FRAME_SC_ID
;                       /VIR_CHAN_ID
;                       /OP_CONTROL_FLAG
;                       /FRM_SEC_HDR_FLAG
;                       /SYNC_FLAG
;                       /PKT_ORDER_FLAG
;                       /SEG_LEN_ID
;                       /FIRST_HDR_PTR
;                       /FRM_SEC_HDR_VER
;                       /FRM_SEC_HDR_LEN
;                       /FRM_DATE       (not currently accurate)
;               Packet header keywords:
;                       /PKT_VER
;                       /PKT_TYPE
;                       /PKT_SEC_HDR_FLAG
;                       /SUBSYS_ID
;                       /DATA_FMT_ID
;                       /APID           (combines SUBSYS_ID and DATA_FMT_ID)
;                       /GROUP_FLAG
;                       /SEQ_COUNT
;                       /PKT_DATE
;
;               In addition, when /GRH_DATE, /FRM_DATE, or /PKT_DATE is used,
;               one can also pass keywords for the routine ANYTIM2UTC, unless
;               one of the following keywords is passed:
;
;                       /RAW    The time is returned as a double precision
;                               number formed from the time information in the
;                               packet.
;
;                       /TAI    A double-precesion TAI time is returned
;
; Calls       :	TAI2UTC, UTC2TAI, ANYTIM2UTC
;
; Common      :	None.
;
; Restrictions:	Frame keywords can only be used with STP packets.
;
;               The /FRM_DATE keyword has not yet been validated, and may
;               require changes in the future.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 08-Jan-2004, William Thompson, GSFC
;               Version 2, 08-Sep-2004, William Thompson, GSFC
;                       Use TAI2UTC with /NOCORRECT
;               Version 3, 02-Mar-2005, William Thompson, GSFC
;                       Corrected calculation of SUBSYS_ID.
;                       Added keyword APID
;               Version 4, 15-Sep-2005, William Thompson, GSFC
;                       Added keywords RAW, TAI, and call to ANYTIM2UTC
;
; Contact     :	WTHOMPSON
;-
;
ON_ERROR, 2
;
N_PACKETS = N_ELEMENTS(PACKET)
OUT = 0
;
;  Parameters from Ground Receipt Header.
;
IF KEYWORD_SET(GRH_DATE) THEN BEGIN
    OUT = {CDS_INT_TIME, MJD: 0L, TIME: 0L}
    IF N_PACKETS GT 1 THEN OUT = REPLICATE(OUT,N_PACKETS)
    OUT.MJD = 36204 + PACKET.GRH.GRT_EPOCH_DAY
    OUT.TIME = PACKET.GRH.GRT_MILLISEC
    IF KEYWORD_SET(RAW) THEN BEGIN
        OUT = UTC2TAI(OUT, /NOCORRECT)
    END ELSE IF KEYWORD_SET(TAI) THEN BEGIN
        OUT=UTC2TAI(OUT)
    END ELSE OUT = ANYTIM2UTC(OUT, _EXTRA=_EXTRA)
;
END ELSE IF KEYWORD_SET(FRAME_QUAL) THEN BEGIN
    OUT = ISHFT(PACKET.GRH.FRAME_QUAL_FLAG AND 2B, -1)
;
END ELSE IF KEYWORD_SET(ARCHIVE_FLAG) THEN BEGIN
    OUT = PACKET.GRH.FRAME_QUAL_FLAG AND 1B
;
;  Parameters from frame information (STP format only).
;
END ELSE IF KEYWORD_SET(FRAME_VER) THEN BEGIN
    OUT = ISHFT(PACKET.FRM.ID_FIELD AND 'C000'X, -14)
;
END ELSE IF KEYWORD_SET(FRAME_SC_ID) THEN BEGIN
    OUT = ISHFT(PACKET.FRM.ID_FIELD AND '3FF0'X, -4)
;
END ELSE IF KEYWORD_SET(VIR_CHAN_ID) THEN BEGIN
    OUT = ISHFT(PACKET.FRM.ID_FIELD AND 'E'X, -1)
;
END ELSE IF KEYWORD_SET(OP_CONTROL_FLAG) THEN BEGIN
    OUT = PACKET.FRM.ID_FIELD AND 1
;
END ELSE IF KEYWORD_SET(FRM_SEC_HDR_FLAG) THEN BEGIN
    OUT = ISHFT(PACKET.FRM.HEADERPTR AND '8000'X, -15)
;
END ELSE IF KEYWORD_SET(SYNC_FLAG) THEN BEGIN
    OUT = ISHFT(PACKET.FRM.HEADERPTR AND '4000'X, -14)
;
END ELSE IF KEYWORD_SET(PKT_ORDER_FLAG) THEN BEGIN
    OUT = ISHFT(PACKET.FRM.HEADERPTR AND '2000'X, -13)
;
END ELSE IF KEYWORD_SET(SEG_LEN_ID) THEN BEGIN
    OUT = ISHFT(PACKET.FRM.HEADERPTR AND '1800'X, -11)
;
END ELSE IF KEYWORD_SET(FIRST_HDR_PTR) THEN BEGIN
    OUT = PACKET.FRM.HEADERPTR AND '7FF'X
;
END ELSE IF KEYWORD_SET(FRM_SEC_HDR_VER) THEN BEGIN
    OUT = ISHFT(PACKET.FRM.SECONDARYID AND 'C0'X, -6)
;
END ELSE IF KEYWORD_SET(FRM_SEC_HDR_LEN) THEN BEGIN
    OUT = PACKET.FRM.SECONDARYID AND '3F'X
;
END ELSE IF KEYWORD_SET(FRM_DATE) THEN BEGIN
    OUT = PACKET.FRM.SC_MET_TIME_SEC  +  PACKET.FRM.SC_MET_TIME_SUBSEC / 256.D0
    IF NOT KEYWORD_SET(RAW) THEN BEGIN
        OUT = TAI2UTC(OUT, /NOCORRECT)
        IF KEYWORD_SET(TAI) THEN OUT = UTC2TAI(OUT) ELSE $
          OUT = ANYTIM2UTC(OUT, _EXTRA=_EXTRA)
    ENDIF
;
;  Parameters from telemetry packet header.
;
END ELSE IF KEYWORD_SET(PKT_VER) THEN BEGIN
    OUT = ISHFT(PACKET.PKT.HDR AND 'E000'X, -13)
;
END ELSE IF KEYWORD_SET(PKT_TYPE) THEN BEGIN
    OUT = ISHFT(PACKET.PKT.HDR AND '1000'X, -12)
;
END ELSE IF KEYWORD_SET(PKT_SEC_HDR_FLAG) THEN BEGIN
    OUT = ISHFT(PACKET.PKT.HDR AND '800'X, -11)
;
END ELSE IF KEYWORD_SET(SUBSYS_ID) THEN BEGIN
    OUT = ISHFT(PACKET.PKT.HDR AND '780'X, -7)
;
END ELSE IF KEYWORD_SET(DATA_FMT_ID) THEN BEGIN
    OUT = PACKET.PKT.HDR AND '7F'X
;
END ELSE IF KEYWORD_SET(APID) THEN BEGIN
    OUT = PACKET.PKT.HDR AND '7FF'X
;
END ELSE IF KEYWORD_SET(GROUP_FLAG) THEN BEGIN
    OUT = ISHFT(PACKET.PKT.GRP AND 'C000'X, -14)
;
END ELSE IF KEYWORD_SET(SEQ_COUNT) THEN BEGIN
    OUT = PACKET.PKT.GRP AND '3FFF'X
;
END ELSE IF KEYWORD_SET(PKT_DATE) THEN BEGIN
    OUT = PACKET.PKT.SECONDS + PACKET.PKT.SUBSEC/256.D0
    IF NOT KEYWORD_SET(RAW) THEN BEGIN
        OUT = TAI2UTC(OUT, /NOCORRECT)
        IF KEYWORD_SET(TAI) THEN OUT = UTC2TAI(OUT) ELSE $
          OUT = ANYTIM2UTC(OUT, _EXTRA=_EXTRA)
    ENDIF
;
ENDIF
RETURN, OUT
END
