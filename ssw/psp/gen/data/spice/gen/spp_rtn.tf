Dynamic Heliospheric Coordinate Frames developed for the NASA Solar Probe Plus
mission

The coordinate frames in this file all have ID values based on the pattern
18ccple, where

	18 = Prefix to put in the allowed 1400000 to 2000000 range
	cc = 03 for geocentric, 10 for heliocentric
	p  = Pole basis: 1=geographic, 2=geomagnetic, 3=ecliptic, 4=solar
	l  = Longitude basis: 1=Earth-Sun, 2=ecliptic, 4=STEREO-A, 5=STEREO-B,
			      6=SOHO, 7=Solar Orbiter, 8=Solar Probe Plus
	e  = Ecliptic basis: 0=J2000, 1=mean, 2=true

     Author:  William Thompson
	      NASA Goddard Space Flight Center
	      Code 612.1
	      Greenbelt, MD 20771

	      William.T.Thompson@nasa.gov


History

    Version 1, 06-Feb-2017, WTT, initial release


Solar Probe Plus - Heliocentric Radial Tangential Normal (SPPHGRTN) Frame

     Definition of the Solar Probe Plus HGRTN Frame
 
              All vectors are geometric: no aberration corrections are used.
 
              The position of the spacecraft relative to the Sun is the primary
              vector: the X axis points from the Sun center to the spacecraft.
 
              The solar rotation axis is the secondary vector: the Z axis is
	      the component of the solar north direction perpendicular to X.
 
              The Y axis is Z cross X, completing the right-handed reference
              frame.

\begindata

        FRAME_SPPHGRTN              =  1810480
        FRAME_1810480_NAME           = 'SPPHGRTN'
        FRAME_1810480_CLASS          =  5
        FRAME_1810480_CLASS_ID       =  1810480
        FRAME_1810480_CENTER         =  10
        FRAME_1810480_RELATIVE       = 'J2000'
        FRAME_1810480_DEF_STYLE      = 'PARAMETERIZED'
        FRAME_1810480_FAMILY         = 'TWO-VECTOR'
        FRAME_1810480_PRI_AXIS       = 'X'
        FRAME_1810480_PRI_VECTOR_DEF = 'OBSERVER_TARGET_POSITION'
        FRAME_1810480_PRI_OBSERVER   = 'SUN'
        FRAME_1810480_PRI_TARGET     = 'SPP'
        FRAME_1810480_PRI_ABCORR     = 'NONE'
        FRAME_1810480_PRI_FRAME      = 'IAU_SUN'
        FRAME_1810480_SEC_AXIS       = 'Z'
        FRAME_1810480_SEC_VECTOR_DEF = 'CONSTANT'
        FRAME_1810480_SEC_FRAME      = 'IAU_SUN'
        FRAME_1810480_SEC_SPEC       = 'RECTANGULAR'
        FRAME_1810480_SEC_VECTOR      = ( 0, 0, 1 )

\begintext

Solar Probe Plus - Heliocentric Ecliptic Radial Tangential Normal (SPPHERTN)
Frame

     Definition of the Solar Probe Plus HERTN Frame
 
              All vectors are geometric: no aberration corrections are used.
 
              The position of the spacecraft relative to the Sun is the primary
              vector: the X axis points from the Sun center to the spacecraft.
 
              The ecliptic axis is the secondary vector: the Z axis is
	      the component of the ecliptic north direction perpendicular to X.
 
              The Y axis is Z cross X, completing the right-handed reference
              frame.

\begindata

        FRAME_SPPHERTN              =  1810381
        FRAME_1810381_NAME           = 'SPPHERTN'
        FRAME_1810381_CLASS          =  5
        FRAME_1810381_CLASS_ID       =  1810381
        FRAME_1810381_CENTER         =  10
        FRAME_1810381_RELATIVE       = 'J2000'
        FRAME_1810381_DEF_STYLE      = 'PARAMETERIZED'
        FRAME_1810381_FAMILY         = 'TWO-VECTOR'
        FRAME_1810381_PRI_AXIS       = 'X'
        FRAME_1810381_PRI_VECTOR_DEF = 'OBSERVER_TARGET_POSITION'
        FRAME_1810381_PRI_OBSERVER   = 'SUN'
        FRAME_1810381_PRI_TARGET     = 'SPP'
        FRAME_1810381_PRI_ABCORR     = 'NONE'
        FRAME_1810381_PRI_FRAME      = 'IAU_SUN'
        FRAME_1810381_SEC_AXIS       = 'Z'
        FRAME_1810381_SEC_VECTOR_DEF = 'CONSTANT'
        FRAME_1810381_SEC_FRAME      = 'ECLIPDATE'
        FRAME_1810381_SEC_SPEC       = 'RECTANGULAR'
        FRAME_1810381_SEC_VECTOR      = ( 0, 0, 1 )

\begintext

