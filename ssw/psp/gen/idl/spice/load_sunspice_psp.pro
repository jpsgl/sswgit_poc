;+
; Project     :	PSP
;
; Name        :	LOAD_SUNSPICE_PSP
;
; Purpose     :	Load the Solar Probe Plus SPICE ephemerides
;
; Category    :	PSP, SUNSPICE, Orbit
;
; Explanation : Loads the Solar Probe Plus ephemeris files in SPICE format.
;               Also calls LOAD_SUNSPICE_GEN to load the generic kernels.
;
; Syntax      :	LOAD_SUNSPICE_PSP
;
; Inputs      :	None
;
; Opt. Inputs :	None
;
; Outputs     :	None
;
; Opt. Outputs:	None
;
; Keywords    : RELOAD = If set, then unload the current ephemeris files, and
;                        redetermine which kernels to load.  The default is to
;                        not reload already loaded kernels.
;
;               ORBIT_FILE = Name of the orbit file to load.  If the full
;                            path is not specified, then looks in
;                            $PSP_SPICE/orbit.  If not passed, or the
;                            specified file cannot be found, then a default
;                            ORBIT file is loaded instead.  Use of the
;                            ORBIT_FILE keyword implies /RELOAD.
;
;               VERBOSE= If set, then print a message for each file loaded.
;
;               ERRMSG = If defined and passed, then any error messages will be
;                        returned to the user in this parameter rather than
;                        depending on the MESSAGE routine in IDL.  If no errors
;                        are encountered, then a null string is returned.  In
;                        order to use this feature, ERRMSG must be defined
;                        first, e.g.
;
;                               ERRMSG = ''
;                               LOAD_SUNSPICE_PSP, ERRMSG=ERRMSG
;                               IF ERRMSG NE '' THEN ...
;
; Calls       :	TEST_SUNSPICE_DLM, LOAD_SUNSPICE_GEN, FILE_EXIST, CSPICE_FURNSH
;
; Common      :	PSP_SUNSPICE contains the names of the loaded files.
;
; Env. Vars.  : PSP_SPICE = points to the directory tree containing the
;                           various Solar Probe Plus SPICE ephemerides.  At
;                           the present, prior to Solar Probe Plus's launch,
;                           this consists of a spacecraft frame file, and
;                           a predictive orbit file.
;
; Restrictions:	This procedure works in conjunction with the Icy/CSPICE
;               package, which is implemented as an IDL Dynamically Loadable
;               Module (DLM).  The Icy source code can be downloaded from
;
;                       ftp://naif.jpl.nasa.gov/pub/naif/toolkit/IDL
;
;               Because this uses dynamic frames, it requires Icy/CSPICE
;               version N0058 or higher.
;
; Side effects:	None
;
; Prev. Hist. :	None
;
; History     :	Version 1, 24-Mar-2017, William Thompson, GSFC
;               Version 2, 17-Oct-2017, WTT, changed SPP to PSP
;               Version 3, 21-Mar-2018, WTT, fixed typo
;               Version 4, 04-May-2018, WTT, include SCLK file
;               Version 5, 22-Aug-2018, WTT, added keyword ORBIT_FILE
;               Version 6, 23-Aug-2018, WTT, fixed /RELOAD bug
;               Version 7, 18-Oct-2018, WTT, updated default orbit file
;               Version 8, 23-Oct-2018, WTT, incorporate new frame files
;                                            latest sclk kernel
;               Version 9, 25-Oct-2018, WTT, include sclk DOS version
;               Version 10, 18-Dec-2018, WTT, modified default orbit file
;               Version 11, 06-Feb-2019, WTT, inc. att. long term predict
;
; Contact     :	WTHOMPSON
;-
;
pro load_sunspice_psp, reload=k_reload, verbose=verbose, $
                       orbit_file=orbit_file, errmsg=errmsg
common psp_sunspice, frames, sclk, orbit
on_error, 2
;
;  Make sure that the SPICE/Icy DLM is available.
;
if not test_sunspice_dlm() then begin
    message = 'SPICE/Icy DLM not available'
    goto, handle_error
endif
;
;  If the ORBIT_FILE keyword was passed then set the /RELOAD keyword.
;
if n_elements(orbit_file) eq 1 then reload=1 else reload=keyword_set(k_reload)
;
;  If the /RELOAD keyword wasn't passed, then check to see if the kernels have
;  already been loaded.
;
n_kernels = n_elements(orbit)
if (not keyword_set(reload)) and (n_kernels gt 0) then return
;
;  Start by unloading any ephemerides which were previously loaded, and then
;  loading the generic kernels.
;
unload_sunspice_psp, verbose=verbose
message = ''
load_sunspice_gen, verbose=verbose, errmsg=message
if message ne '' then goto, handle_error
;
;  Load the frame files
;
psp_spice = getenv('PSP_SPICE')
psp_spice_gen = concat_dir(psp_spice,'gen')
psp_spice_sclk = concat_dir(psp_spice, 'operations_sclk_kernel')
if !version.os_family eq 'Windows' then begin
    psp_spice_gen = concat_dir(psp_spice_gen, 'dos')
    psp_spice_sclk = concat_dir(psp_spice_sclk, 'dos')
endif
;
frame_files = file_search(concat_dir(psp_spice_gen,'*.tf'), count=count)
if count eq 0 then begin
    message = 'No frame files found'
    goto, handle_error
endif
;
frames = frame_files
frame_files = file_search(concat_dir(psp_spice_gen,'*.ti'), count=count)
if count gt 0 then frames = [frames, frame_files]
;
for i=0, n_elements(frames)-1 do begin
    if keyword_set(verbose) then print, 'Loaded ' + frames[i]
    cspice_furnsh, frames[i]
endfor
;
;  Load the spacecraft clock file.
;
files = file_search( concat_dir(psp_spice_sclk, 'spp_sclk_*.tsc'), count=count)
if count eq 0 then begin
    message = 'Unable to find spacecraft clock file'
    goto, handle_error
endif
sclk = max(files)
if keyword_set(verbose) then print, 'Loaded ' + sclk
cspice_furnsh, sclk
;
;  Determine the default orbit file.
;
psp_spice_orbit = concat_dir(psp_spice, 'orbit')
def_orbit = concat_dir(psp_spice_orbit, 'spp_nom_20180812_20250831_v034_RO1_TCM1.bsp')
;
file = concat_dir(psp_spice, 'ephemerides.dat')
if file_exist(file) then begin
    openr, unit, file, /get_lun
    line = 'String'
    readf, unit, line
    free_lun, unit
    line = strtrim(line,2)
    test_file = concat_dir(psp_spice_orbit, line)
    if file_exist(test_file) then def_orbit = test_file
endif
;
;  Determine the orbit file to use.
;
if n_elements(orbit_file) eq 1 then begin
    orbit = orbit_file
    if not file_exist(orbit) then begin
        orbit = concat_dir(psp_spice_orbit, orbit_file)
        if not file_exist(orbit) then begin
            print, 'Unable to load orbit file ' + orbit_file
            print, 'Defaulting to ' + def_orbit
            orbit = def_orbit
        endif
    endif
end else orbit = def_orbit
;
;  Load the orbit file.
;
if not file_exist(orbit) then begin
    message = 'Orbit file ' + orbit + ' not found'
    goto, handle_error
endif
if keyword_set(verbose) then print, 'Loaded ' + orbit
cspice_furnsh, orbit
;
;  Load any long-term predictive orbit files.
;
att_predict = concat_dir(psp_spice, 'attitude_long_term_predict.txt')
if file_exist(att_predict) then begin
    psp_spice_att_ltp = concat_dir(psp_spice, 'attitude_long_term_predict')
    openr, unit, att_predict, /get_lun
    line = 'string'
    while not eof(unit) do begin
        readf, unit, line
        att_file = concat_dir(psp_spice_att_ltp, line)
        if file_exist(att_file) then begin
            if keyword_set(verbose) then print, 'Loaded ' + att_file
            cspice_furnsh, att_file
        endif
    endwhile
    free_lun, unit
endif
;
return
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message else $
  errmsg = 'LOAD_SUNSPICE_PSP: ' + message
;
end
