;+
; Project     :	SPP
;
; Name        :	LOAD_SUNSPICE_SPP
;
; Purpose     :	Load the Solar Probe Plus SPICE ephemerides
;
; Category    :	SPP, SUNSPICE, Orbit
;
; Explanation : Loads the Solar Probe Plus ephemeris files in SPICE format.
;               Also calls LOAD_SUNSPICE_GEN to load the generic kernels.
;
; Syntax      :	LOAD_SUNSPICE_SPP
;
; Inputs      :	None
;
; Opt. Inputs :	None
;
; Outputs     :	None
;
; Opt. Outputs:	None
;
; Keywords    : RELOAD = If set, then unload the current ephemeris files, and
;                        redetermine which kernels to load.  The default is to
;                        not reload already loaded kernels.
;
;               VERBOSE= If set, then print a message for each file loaded.
;
;               ERRMSG = If defined and passed, then any error messages will be
;                        returned to the user in this parameter rather than
;                        depending on the MESSAGE routine in IDL.  If no errors
;                        are encountered, then a null string is returned.  In
;                        order to use this feature, ERRMSG must be defined
;                        first, e.g.
;
;                               ERRMSG = ''
;                               LOAD_SUNSPICE_SPP, ERRMSG=ERRMSG
;                               IF ERRMSG NE '' THEN ...
;
; Calls       :	TEST_SUNSPICE_DLM, LOAD_SUNSPICE_GEN, FILE_EXIST, CSPICE_FURNSH
;
; Common      :	SPP_SUNSPICE contains the names of the loaded files.
;
; Env. Vars.  : SPP_SPICE = points to the directory tree containing the
;                           various Solar Probe Plus SPICE ephemerides.  At
;                           the present, prior to Solar Probe Plus's launch,
;                           this consists of a spacecraft frame file, and
;                           a predictive orbit file.
;
; Restrictions:	This procedure works in conjunction with the Icy/CSPICE
;               package, which is implemented as an IDL Dynamically Loadable
;               Module (DLM).  The Icy source code can be downloaded from
;
;                       ftp://naif.jpl.nasa.gov/pub/naif/toolkit/IDL
;
;               Because this uses dynamic frames, it requires Icy/CSPICE
;               version N0058 or higher.
;
; Side effects:	None
;
; Prev. Hist. :	None
;
; History     :	Version 1, 24-Mar-2017, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
pro load_sunspice_spp, reload=reload, verbose=verbose, errmsg=errmsg
common spp_sunspice, rtnframe, orbit
on_error, 2
;
;  Make sure that the SPICE/Icy DLM is available.
;
if not test_sunspice_dlm() then begin
    message = 'SPICE/Icy DLM not available'
    goto, handle_error
endif
;
;  If the /RELOAD keyword wasn't passed, then check to see if the kernels have
;  already been loaded.
;
n_kernels = n_elements(ephem)
if (not keyword_set(reload)) and (n_kernels gt 0) then return
;
;  Start by unloading any ephemerides which were previously loaded, and then
;  loading the generic kernels.
;
unload_sunspice_spp, verbose=verbose
message = ''
load_sunspice_gen, verbose=verbose, errmsg=message
if message ne '' then goto, handle_error
;
;  Load the RTN frame.
;
spp_spice = getenv('SPP_SPICE')
spp_spice_gen = concat_dir(spp_spice,'gen')
if !version.os_family eq 'Windows' then $
  spp_spice_gen = concat_dir(spp_spice_gen, 'dos')
;
rtnframe = concat_dir(spp_spice_gen, 'spp_rtn.tf')
if not file_exist(rtnframe) then begin
    message = 'Frame file ' + rtnframe + ' not found'
    goto, handle_error
endif
if keyword_set(verbose) then print, 'Loaded ' + rtnframe
cspice_furnsh, rtnframe
;
;  Load the orbit file.
;
spp_spice_orbit = concat_dir(spp_spice, 'orbit')
orbit = concat_dir(spp_spice_orbit, 'SPP20180731P2_ephem_96.bsp')
;
if not file_exist(orbit) then begin
    message = 'Orbit file ' + orbit + ' not found'
    goto, handle_error
endif
if keyword_set(verbose) then print, 'Loaded ' + orbit
cspice_furnsh, orbit
;
return
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message else $
  errmsg = 'LOAD_SUNSPICE_SPP: ' + message
;
end
