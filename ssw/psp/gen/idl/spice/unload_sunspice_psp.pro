;+
; Project     :	PSP
;
; Name        :	UNLOAD_SUNSPICE_PSP
;
; Purpose     :	Unload the Solar Orbiter SPICE kernels
;
; Category    :	PSP, SUNSPICE, Orbit
;
; Explanation :	Unloads any previously loaded SPICE kernels loaded by
;               LOAD_SUNSPICE_PSP.
;
; Syntax      :	UNLOAD_SUNSPICE_PSP
;
; Inputs      :	None
;
; Opt. Inputs :	None
;
; Outputs     :	None
;
; Opt. Outputs:	None
;
; Keywords    :	VERBOSE = If set, then print a message for each file unloaded.
;
; Calls       :	CSPICE_UNLOAD, DELVARX
;
; Common      :	PSP_SUNSPICE contains the names of the loaded files.
;
; Restrictions:	This procedure works in conjunction with the Icy/CSPICE
;               package, which is implemented as an IDL Dynamically Loadable
;               Module (DLM).  The Icy source code can be downloaded from
;
;                       ftp://naif.jpl.nasa.gov/pub/naif/toolkit/IDL
;
;               Because this uses dynamic frames, it requires Icy/CSPICE
;               version N0058 or higher.
;
; Side effects:	None
;
; Prev. Hist. :	None
;
; History     :	Version 1, 6-Feb-2017, William Thompson, GSFC
;               Version 2, 17-Oct-2017, WTT, changed SPP to PSP
;               Version 3, 04-May-2018, WTT, include SCLK file
;               Version 4, 23-Oct-2018, WTT, incorporate new frame files
;
; Contact     :	WTHOMPSON
;-
;
pro unload_sunspice_psp, verbose=verbose
;
common psp_sunspice, frames, sclk, orbit
on_error, 2
;
;  Unload the files.
;
for i=0,n_elements(frames)-1 do begin
    cspice_unload, frames[i]
    if keyword_set(verbose) then print, 'Unloaded ' + frames[i]
endfor
delvarx, frames
;
if n_elements(sclk) eq 1 then begin
    cspice_unload, sclk
    if keyword_set(verbose) then print, 'Unloaded ' + sclk
    delvarx, sclk
endif
;
if n_elements(orbit) eq 1 then begin
    cspice_unload, orbit
    if keyword_set(verbose) then print, 'Unloaded ' + orbit
    delvarx, orbit
endif
;
end
