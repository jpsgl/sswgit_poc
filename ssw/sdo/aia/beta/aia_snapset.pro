
pro aia_snapset, fset=fset, imset=imset, indset=indset, t_mod=t_mod, do_write=do_write, do_z=do_z, outpath=outpath, $
  dir_top_gif=dir_top_gif, dir_next_day=dir_next_day, do_despike=do_despike, do_shift=do_shift, $
  offlimb=offlimb, do_display=do_display, $
  do_time=do_time, mag_only=mag_only, minterp=minterp, error=error, imageRejected=imageRejected, do_gifs=do_gifs, $
  t_now=t_now, dir_html=dir_html, do_stale=do_stale, t_lag_max_hrs=t_lag_max_hrs, qstop=qstop, $
  no_tomorrow=no_tomorrow, $
  f0171=f0171, f0193=f0193, f0211=f0211, f0335=f0335, f0094=f0094, $
  f0131=f0131, f0304=f0304, f4500=f4500, f1700=f1700, f1600=f1600

;+
; Purpose: Given an index, data pair or a fits file name for any AIA image, prepare the image
;          for use by "The Sun Today" page
;-

; Set defaults:
;if not exist(outpath) then outpath='/net/castor/Users/slater/data/aia_snapset/SunInTime'
;if not exist(outpath) then outpath='/archive/sdo/media/SunInTime'
;if not exist(outpath) then outpath='/net/bay/Volumes/uranus/media/SunInTime'
 if not exist(outpath) then outpath='/viz2/media/SunInTime'
;if not exist(outpath) then outpath='/net/bigdata/Volumes/archive3/sdomedia/SunInTime'
;if not exist(dir_top_gif) then dir_top_gif = '/net/solserv/home/slater/public_html/aia_snapset/SunInTime'
if keyword_set(showimages) then if showimages lt 10 then showimages=512
if not exist(do_despike) then unspike = 0 else unspike = do_despike
if not exist(do_shift) then noshift = 1 else noshift = 1-do_shift
if not exist(do_time) then do_time = 1
if not exist(dir_html) then dir_html = outpath
;if not exist(dir_latest_images) then dir_latest_images = '/archive/sdo/media/SunInTime/mostrecent'
;if not exist(dir_latest_images) then dir_latest_images = '/net/bay/Volumes/uranus/media/SunInTime/mostrecent'
 if not exist(dir_latest_images) then dir_latest_images = '/viz2/media/SunInTime/mostrecent'
;if not exist(dir_latest_images) then dir_latest_images = '/net/bigdata/archive3/sdomedia/SunInTime/mostrecent'
if not exist(dir_latest_images_1) then dir_latest_images_1 = '/net/solserv/home/slater/public_html/outgoing/sdo/mostrecent'
if not exist(t_lag_max_hrs) then t_lag_max_hrs = 2.0

; Save current value of plotting device:
cur_val_plot = !d.name
if keyword_set(do_z) then set_plot,'z'

; Initialize error parameters to integer 0
error = ''
imageRejected = 0

 limit   = 10. ; max multiplier of off-disk signal
 waves   = [171,193,211,335, 94,131,304,4500,1600,1700]
;waves   = [171,193,211,335, 94,131,304               ]
 limitss = [  5,  5,  5,  3,  3,  3, 10,   1,   1,   1]
;limitss = [  5,  5,  5,  3,  3,  3, 10               ]

if not exist(fset) then $
; fset = [f0171, f0193, f0211, f0335, f0094, f0131, f0304                     ]
  fset = [f0171, f0193, f0211, f0335, f0094, f0131, f0304, f4500, f1700, f1600]

yoffset=1.+findgen(n_elements(waves))

; set to work with:
timestamp=strmid(fset(0),strpos(fset(0),'AIA20')+3,15)

if not keyword_set(mag_only) then begin
  times_file = concat_dir(outpath, 'times.txt')
  spawn, 'rm ' + times_file
endif

n_waves=n_elements(fset)-2
for i=0,n_waves-1 do begin

if fset[i] ne '' then begin

  print,'Starting with ' + strtrim(waves[i],2)
;  read_sdo, fset[i], header, image, /uncomp_delete

header0 = indset[i]
image0 = imset[*,*,i]
;if header0.lvl_num eq 1.0 then begin
; aia_prep:
   print, 'Adding aia_prep beta path'
   ssw_path, '$SSW_AIA/beta', /prepend
   which, 'aia_prep'
   aia_prep, index, data, indexp, datap, $
      ds='aia_test.master_pointing3h', /refresh, /use_sswmp, /verbose
;  aia_prep_cheung, index, data, indexp, datap, $
;     ds='aia_test.master_pointing3h', /refresh, /use_sswmp, /verbose
;  aia_prep, image0, header0, image, header, input_mode='index_data'
;endif else begin
;  image = image0
;  header = header0
;endelse

;if waves[i] eq 1700 then begin
;  image_stats_1700_log = '~/logs/suntoday/image_stats_1700.log'
;  out_string = 'image stats 1700A::  date_obs: ' + header.date_obs + $
;	       '  image min: ' + strtrim(min(image),2) + $
;	       '  image max: ' + strtrim(max(image),2)
;  file_append, image_stats_1700_log, out_string
;
;  thumbs_1700_savfile = '~/logs/suntoday/thumbs_1700.sav'
;  thumb_1700 = rebin(image, 64, 64)
;  if file_exist(thumbs_1700_savfile) then begin
;    restore, thumbs_1700_savfile
;    thumb_1700_arr = [[[thumb_1700_arr]],[[thumb_1700]]]
;    stats_1700_arr = [stats_1700_arr, out_string]
;  endif else begin
;    thumb_1700_arr = thumb_1700
;    stats_1700_arr = out_string
;  endelse
;  save, stats_1700_arr, thumb_1700_arr, file=thumbs_1700_savfile
;endif

  t_image = header.date_obs
  t_image_sec = anytim(t_image)

  if not keyword_set(mag_only) then begin
    free_lun, 2
    openw, 2, concat_dir(outpath, 'one_time.txt')
    printf, 2, anytim(t_image, /yoh)
    free_lun, 2

    js_doc = concat_dir(outpath, 'one_time.js')
    file_append, js_doc, 'function getDateUpdated() {', /new
    file_append, js_doc, '   var date_updated_string = "<i>AIA images shown were taken at about " + '
    file_append, js_doc, '	    "' + strtrim(anytim(t_image, /yoh),2) + '"'
    file_append, js_doc, '	     + " UT</i>";'
    file_append, js_doc, '   return date_updated_string;'
    file_append, js_doc, '}'
  endif

; exptime = header.aimgshce / 1000.
  exptime = header.exptime
  wavelnth = header.wavelnth

  if unspike eq 1 then image=trace_unspike(image,sens=.3)>0

;  if keyword_set(offlimb) then aia_offlimb,image,wavelnth=wavelnth,limit=limitss(i)<limit, t0=t_image else $
;     image=((image-aia_corners(image,wavelnth=wavelnth,noshift=noshift,t0=t_image)))>0

pmm_unscaled = string(fix(min(image)), format='$(i16.14)') + '  ' + $
	       string(fix(max(image)), format='$(i16.14)')
thumb_unscaled = rebin(image, 64, 64)

  ihist = histogram(image, min = 0, max = 16383, binsize = 1)
  image=aia_intscale(image,exptime=exptime,wavelnth=wavelnth,/bytescale)

pmm_scaled   = string(fix(min(image)), format='$(i16.14)') + '  ' + $
               string(fix(max(image)), format='$(i16.14)')
thumb_scaled = rebin(image, 64, 64)

if ( (wavelnth eq 1600) or (wavelnth eq 1700) or (wavelnth eq 4500) ) then begin

  ohist = histogram(image, min = 0, max = 255, binsize = 1)
  thisstr = {ihist : ihist, ohist : ohist, header : header}
  fname = time2file(header.t_obs, /sec) + '_' + string(header.wavelnth, form = '(i04)') + '.genx'
  savegen, file = concat_dir('~/logs/suntoday/boerner/', fname), str = thisstr

  image_stats_log = '~/logs/suntoday/image_stats_v3_' +  strtrim(wavelnth,2) + '.log'
  out_string = anytim(t_image,/ccsds) + '  ' + strtrim(wavelnth,2) + '  ' + pmm_unscaled + '  ' + $
	       strtrim(header.lvl_num,2) + '  ' + strtrim(header.aimgshce/1000.,2) + '  ' + strtrim(header.exptime,2)
  file_append, image_stats_log, out_string

  thumbs_savfile = '~/logs/suntoday/thumbs_v3_' + strtrim(wavelnth,2) + '.sav'
  if file_exist(thumbs_savfile) then begin
    restore, thumbs_savfile
    thumb_unscaled_arr = [[[thumb_unscaled_arr]],[[thumb_unscaled]]]
    thumb_scaled_arr = [[[thumb_scaled_arr]],[[thumb_scaled]]]
    stats_arr = [stats_arr, out_string]
    files_arr = [files_arr, fset[i]]
  endif else begin
    thumb_unscaled_arr = thumb_unscaled
    thumb_scaled_arr = thumb_scaled
    stats_arr = out_string
    files_arr = fset[i]
  endelse
  save, stats_arr, files_arr, thumb_unscaled_arr, thumb_scaled_arr, file=thumbs_savfile
endif

  aia_lct,r,g,b,wavelnth=wavelnth,/load
; full-disk rebinned to 1024x1024
  fdimage=congrid(image,1024,1024)
; full-disk rebinned to 256x256
  tdimage=congrid(image,256,256)
;
;  timestamp=strmid(fset(i),strpos(fset(i),'AIA20')+3,15)
  timestamp = strmid(anytim(header.date_obs,/ecs),0,19)
  times_fn  = timestamp
  times_fn  = strrempat(times_fn, '/', /all)
  times_fn  = strrempat(times_fn, ':', /all)
  times_fn  = str_replace(times_fn, ' ', '_')
  times_fn  = strmid(times_fn,0,15)

; add timestamps to the images
  if keyword_set(do_time) then begin
     outsind=where(wavelnth eq waves)
     device,set_resolution=[4096,4096]
     tv,image 
     xyouts,10,75*yoffset(outsind),'SDO/AIA-'+strcompress(string(fix(wavelnth),format='(i4)'))+' '+timestamp, $
            charsize=4.0,color=255, /device, charthick=4. 
     image=tvrd() 
     device,set_resolution=[1024,1024]
     tv,fdimage
     xyouts,10,30*yoffset(outsind),'SDO/AIA-'+strcompress(string(fix(wavelnth),format='(i4)'))+' '+timestamp, $
            charsize=1.5,color=255, /device 
     fdimage=tvrd() 
;     device,set_resolution=[256,256]
;     tv,tdimage
;     xyouts,10,10*yoffset(outsind),'SDO/AIA-'+strcompress(string(fix(wavelnth),format='(i4)'))+' '+timestamp, $
;            charsize=1.0,color=255, /device 
;     tdimage=tvrd() 
  endif

; Add additional annotation to thumbnails, as needed:

; If image is stale by more than specified limit, then label it as stale and apologize:
  if ( (keyword_set(do_stale) ) and (( anytim(t_now) - t_image_sec ) gt t_lag_max_hrs*3600d0) ) then begin
     device,set_resolution=[256,256]
     tv, tdimage 
;     xyouts,/norm,.1,.8,'YOUR LOGO HERE',charsize=5.0,color=255, /device, charthick=6.
;     xyouts,/norm,.1,.6,'Call 1-800-SDO-RULE'
     xyouts,/norm,.065,.95,'     DATA PROCESSING LAG     ',charsize=1.0,color=255, charthick=1.
     xyouts,/norm,.065,.90,' MOST RECENT IMAGE DISPLAYED ',charsize=1.0,color=255, charthick=1.
     buff = anytim(t_image, /yoh)
     xyouts,/norm,.065,.85,'      ' + strmid(buff,0,9) + ' ' + strmid(buff,10,5) + '      ', $
       charsize=1.0,color=255, charthick=1.
     tdimage = tvrd() 
  endif

; Label calibration mode images as such:
  if (header.aiftsid gt 49155) then begin
     device,set_resolution=[256,256]
     tv, tdimage 
     xyouts,/norm,.065,.95,'      CALIBRATION IMAGE      ',charsize=1.0,color=255, charthick=1.
;     xyouts,/norm,.065,.90,' MOST RECENT IMAGE DISPLAYED ',charsize=1.0,color=255, charthick=1.
     tdimage = tvrd() 
  endif

; Label eclipse mode images as such:
  if (header.acs_eclp ne 0) then begin
     device,set_resolution=[256,256]
     tv, tdimage 
     xyouts,/norm,.065,.85,'     ECLIPSE IN PROGRESS     ',charsize=1.0,color=255, charthick=1.
;     xyouts,/norm,.065,.95,'      CALIBRATION IMAGE      ',charsize=1.0,color=255, charthick=1.
;     xyouts,/norm,.065,.90,' MOST RECENT IMAGE DISPLAYED ',charsize=1.0,color=255, charthick=1.
     tdimage = tvrd() 
  endif

; write_gif,outpath+'/f'+counterstring(wavelnth,4)+'.gif',image,r,g,b
  filnam_f = concat_dir(outpath, 'f'+counterstring(wavelnth,4)+'.jpg')
  write_jpeg, filnam_f, truecolor(image,r,g,b),true=1,qual=90
  filnam_watermark = '~/soft/idl/idl_startup/suntoday/large_aia_watermark_50pct.jpg
print, 'calling aia_watermark_image now'
  spawn, '~/soft/idl/idl_startup/suntoday/aia_watermark_image' + ' ' + filnam_watermark + ' -20 -20 ' + filnam_f

; write_gif,outpath+'/l'+counterstring(wavelnth,4)+'.gif',fdimage,r,g,b
  filnam_l = concat_dir(outpath, 'l'+counterstring(wavelnth,4)+'.jpg')
  write_jpeg, filnam_l, truecolor(fdimage,r,g,b),true=1,qual=90
  filnam_watermark = '~/soft/idl/idl_startup/suntoday/aia_watermark_50pct.jpg
  spawn, '~/soft/idl/idl_startup/suntoday/aia_watermark_image' + ' ' + filnam_watermark + ' -20 -20 ' + filnam_l
; RPT 3/26/15 change remaining net/bay/Volumes/uranus to viz2.
parent_hourly = concat_dir('/viz2/media/SunInTime', 'hourly_1k_archive')
mkpath, time2dir(header.date_obs), parent=parent_hourly
filtime_hourly = time2file(header.date_obs)
filtime_hourly = strmid(filtime_hourly,0,11) + '00'
filnam_hourly = concat_dir(concat_dir(parent_hourly, time2dir(header.date_obs)), $
			   'sdo_hourly_1k_' + counterstring(wavelnth,4) + '_' + $
			   filtime_hourly + '.jpg')
write_jpeg, filnam_hourly, truecolor(fdimage,r,g,b), true=1, qual=90
file_append, concat_dir(concat_dir(parent_hourly, time2dir(header.date_obs)), $
                           'sdo_hourly_1k_' + counterstring(wavelnth,4) + '_' + $
                           filtime_hourly + '.time'), anytim(header.date_obs, /ccsds), /new

  if keyword_set(do_gif) then write_gif,dir_top_gif+'/t'+counterstring(wavelnth,4)+'.gif',tdimage,r,g,b
  write_jpeg,outpath+'/t'+counterstring(wavelnth,4)+'.jpg',truecolor(tdimage,r,g,b),true=1,qual=90
;STOP
;  if not keyword_set(mag_only) then printf,1,counterstring(wavelnth,4)+': '+times_fn
  if not keyword_set(mag_only) then $
    file_append, times_file, counterstring(wavelnth,4)+': '+times_fn

  if i eq 0 then i0171=image ; save for true-color image
  if i eq 1 then i0193=image ; save for true-color image
  if i eq 2 then i0211=image ; save for true-color image
  if i eq 3 then i0335=image ; save for true-color image
  if i eq 4 then i0094=image ; save for true-color image
  if i eq 5 then i0131=image ; save for true-color image
  if i eq 6 then i0304=image ; save for true-color image

  if i eq 0 then fi0171=fdimage ; save for true-color image
  if i eq 1 then fi0193=fdimage ; save for true-color image
  if i eq 2 then fi0211=fdimage ; save for true-color image
  if i eq 3 then fi0335=fdimage ; save for true-color image
  if i eq 4 then fi0094=fdimage ; save for true-color image
  if i eq 5 then fi0131=fdimage ; save for true-color image
  if i eq 6 then fi0304=fdimage ; save for true-color image

  if i eq 0 then ti0171=tdimage ; save for true-color image
  if i eq 1 then ti0193=tdimage ; save for true-color image
  if i eq 2 then ti0211=tdimage ; save for true-color image
  if i eq 3 then ti0335=tdimage ; save for true-color image
  if i eq 4 then ti0094=tdimage ; save for true-color image
  if i eq 5 then ti0131=tdimage ; save for true-color image
  if i eq 6 then ti0304=tdimage ; save for true-color image

  if keyword_set(showimages) then begin
    window,i,xs=showimages,ys=showimages,title='AIA'+strcompress(string(fix(wavelnth)))+' '+timestamp
    tv,congrid(image,showimages,showimages)

  endif

endif

endfor

if ( exist(i0211) and exist(i0193) and exist(i0171) ) then begin

; 1-2MK EUV combo
  im2=[[[i0211]],[[i0193]],[[i0171]]]
  filnam_f = concat_dir(outpath, 'f_211_193_171.jpg')
  write_jpeg, filnam_f, im2, true=3, qual=85
  filnam_watermark = '~/soft/idl/idl_startup/suntoday/large_aia_watermark_50pct.jpg
  spawn, '~/soft/idl/idl_startup/suntoday/aia_watermark_image' + ' ' + filnam_watermark + ' -20 -20 ' + filnam_f

  im2a=[[[fi0211]],[[fi0193]],[[fi0171]]]
  filnam_l = concat_dir(outpath, 'l_211_193_171.jpg')
  write_jpeg, filnam_l, im2a, true=3, qual=85
  filnam_watermark = '~/soft/idl/idl_startup/suntoday/aia_watermark_50pct.jpg
  spawn, '~/soft/idl/idl_startup/suntoday/aia_watermark_image' + ' ' + filnam_watermark + ' -20 -20 ' + filnam_l

parent_hourly = concat_dir('/viz2/media/SunInTime', 'hourly_1k_archive')
mkpath, time2dir(indset[0].date_obs), parent=parent_hourly
filtime_hourly = time2file(indset[0].date_obs)
filtime_hourly = strmid(filtime_hourly,0,11) + '00'
filnam_hourly = concat_dir(concat_dir(parent_hourly, time2dir(indset[0].date_obs)), $
                           'sdo_hourly_1k_' + 'l_211_193_171_' + $
                           filtime_hourly + '.jpg')
write_jpeg, filnam_hourly, im2a, true=3, qual=85
file_append, concat_dir(concat_dir(parent_hourly, time2dir(indset[0].date_obs)), $
                           'sdo_hourly_1k_' + 'l_211_193_171_' + $
                           filtime_hourly + '.time'), anytim(indset[0].date_obs, /ccsds), /new

  im2b=[[[ti0211]],[[ti0193]],[[ti0171]]]
; write_gif,outpath+'/t_211_193_171.gif',im2b
  write_jpeg,outpath+'/t_211_193_171.jpg',im2b,true=3,qual=85

  if keyword_set(showimages) then begin
    window,10,xs=showimages,ys=showimages,title='AIA 211_193_171'+' '+timestamp
    tv,congrid(im2,showimages,showimages,3),true=3
  endif

endif

if ( exist(i0094) and exist(i0335) and exist(i0193) ) then begin

; higher-T mix:
  im3=[[[i0094]],[[i0335]],[[i0193]]] 
  filnam_f = concat_dir(outpath, 'f_094_335_193.jpg')
  write_jpeg, filnam_f, im3, true=3, qual=85
  filnam_watermark = '~/soft/idl/idl_startup/suntoday/large_aia_watermark_50pct.jpg
  spawn, '~/soft/idl/idl_startup/suntoday/aia_watermark_image' + ' ' + filnam_watermark + ' -20 -20 ' + filnam_f

  im3a=[[[fi0094]],[[fi0335]],[[fi0193]]] 
  filnam_l = concat_dir(outpath, 'l_094_335_193.jpg')
  write_jpeg, filnam_l, im3a, true=3, qual=85
  filnam_watermark = '~/soft/idl/idl_startup/suntoday/aia_watermark_50pct.jpg
  spawn, '~/soft/idl/idl_startup/suntoday/aia_watermark_image' + ' ' + filnam_watermark + ' -20 -20 ' + filnam_l

parent_hourly = concat_dir('/viz2/media/SunInTime', 'hourly_1k_archive')
mkpath, time2dir(indset[1].date_obs), parent=parent_hourly
filtime_hourly = time2file(indset[1].date_obs)
filtime_hourly = strmid(filtime_hourly,0,11) + '00'
filnam_hourly = concat_dir(concat_dir(parent_hourly, time2dir(indset[1].date_obs)), $
                           'sdo_hourly_1k_' + 'l_094_335_193_' + $
                           filtime_hourly + '.jpg')
write_jpeg, filnam_hourly, im3a, true=3, qual=85
file_append, concat_dir(concat_dir(parent_hourly, time2dir(indset[1].date_obs)), $
                           'sdo_hourly_1k_' + 'l_094_335_193_' + $
                           filtime_hourly + '.time'), anytim(indset[1].date_obs, /ccsds), /new

  im3b=[[[ti0094]],[[ti0335]],[[ti0193]]] 
; write_gif,outpath+'/t_094_335_193.gif',im3b
  write_jpeg,outpath+'/t_094_335_193.jpg',im3b,true=3,qual=85

  if keyword_set(showimages) then begin
    window,11,xs=showimages,ys=showimages,title='AIA 094_335_193'+' '+timestamp
    tv,congrid(im3,showimages,showimages,3),true=3
  endif

endif

if ( exist(i0304) and exist(i0211) and exist(i0171) ) then begin

; 304+cor mix:
  im4=[[[i0304]],[[i0211]],[[i0171]]]
  filnam_f = concat_dir(outpath, 'f_304_211_171.jpg')
  write_jpeg, filnam_f, im4, true=3, qual=85
  filnam_watermark = '~/soft/idl/idl_startup/suntoday/large_aia_watermark_50pct.jpg
  spawn, '~/soft/idl/idl_startup/suntoday/aia_watermark_image' + ' ' + filnam_watermark + ' -20 -20 ' + filnam_f

  im4a=[[[fi0304]],[[fi0211]],[[fi0171]]]
  filnam_l = concat_dir(outpath, 'l_304_211_171.jpg')
  write_jpeg, filnam_l, im4a, true=3, qual=85
  filnam_watermark = '~/soft/idl/idl_startup/suntoday/aia_watermark_50pct.jpg
  spawn, '~/soft/idl/idl_startup/suntoday/aia_watermark_image' + ' ' + filnam_watermark + ' -20 -20 ' + filnam_l

parent_hourly = concat_dir('/viz2/media/SunInTime', 'hourly_1k_archive')
mkpath, time2dir(indset[0].date_obs), parent=parent_hourly
filtime_hourly = time2file(indset[0].date_obs)
filtime_hourly = strmid(filtime_hourly,0,11) + '00'
filnam_hourly = concat_dir(concat_dir(parent_hourly, time2dir(indset[0].date_obs)), $
                           'sdo_hourly_1k_' + 'l_304_211_171_' + $
                           filtime_hourly + '.jpg')
write_jpeg, filnam_hourly, im4a, true=3, qual=85
file_append, concat_dir(concat_dir(parent_hourly, time2dir(indset[0].date_obs)), $
                           'sdo_hourly_1k_' + 'l_304_211_171_' + $
                           filtime_hourly + '.time'), anytim(indset[0].date_obs, /ccsds), /new

  im4b=[[[ti0304]],[[ti0211]],[[ti0171]]]
; write_gif,outpath+'/t_304_211_171.gif',im4b
  write_jpeg,outpath+'/t_304_211_171.jpg',im4b,true=3,qual=85

  if keyword_set(showimages) then begin
    window,12,xs=showimages,ys=showimages,title='AIA 304_211_171'+' '+timestamp
    tv,congrid(im4,showimages,showimages,3),true=3
  endif

endif

; Optionally write out suitably labeled combo images as placeholders for movie files:
  if keyword_set(do_cartoon) then begin
    device,set_resolution=[1024,1024]

    tv, fi0094
    xyouts,/norm,.13,.5,'DAILY MOVIE NOT AVAILABLE',charsize=4.0,color=255, charthick=2
    fi0094_a = tvrd()
    tv, fi0171
    xyouts,/norm,.13,.5,'DAILY MOVIE NOT AVAILABLE',charsize=4.0,color=255, charthick=2
    fi0171_a = tvrd()
    tv, fi0193
    xyouts,/norm,.13,.5,'DAILY MOVIE NOT AVAILABLE',charsize=4.0,color=255, charthick=2
    fi0193_a = tvrd()
    tv, fi0211
    xyouts,/norm,.13,.5,'DAILY MOVIE NOT AVAILABLE',charsize=4.0,color=255, charthick=2
    fi0211_a = tvrd()
    tv, fi0335
    xyouts,/norm,.13,.5,'DAILY MOVIE NOT AVAILABLE',charsize=4.0,color=255, charthick=2
    fi0335_a = tvrd()
    tv, fi0304
    xyouts,/norm,.13,.5,'DAILY MOVIE NOT AVAILABLE',charsize=4.0,color=255, charthick=2
    fi0304_a = tvrd()

    img_ph_1 = [[[fi0211_a]],[[fi0193_a]],[[fi0171_a]]]
    write_jpeg,outpath+'/daily_211-193-171.mov',img_ph_1,true=3,qual=85
    img_ph_2 = [[[fi0094_a]],[[fi0335_a]],[[fi0193_a]]]
    write_jpeg,outpath+'/daily_094-335-193.mov',img_ph_2,true=3,qual=85
    img_ph_3 = [[[fi0304_a]],[[fi0211_a]],[[fi0171_a]]]
    write_jpeg,outpath+'/daily_304-211-071.mov',img_ph_3,true=3,qual=85
  endif

; Process los magnetogram, if available:
if fset[n_waves] ne '' then begin
  read_sdo, fset[n_waves], mheader, mag, /uncomp_delete
  mtimestamp = anytim(mheader.date_obs, /ccsds)
  if not keyword_set(mag_only) then begin
;    free_lun, 1
;    openu, 1, concat_dir(outpath, 'times.txt')
;STOP
    file_append, times_file, 'HMIB: '+ time2file(mtimestamp, /sec)
  endif else begin
;    printf,1,'HMIB: '+ time2file(mtimestamp, /sec)
    file_append, times_file, 'HMIB: '+ time2file(mtimestamp, /sec, /new)
  endelse

; Append 1600 file time to times.txt file:
    if file_exist('/viz2/media/SunInTime/mostrecent/1600_time.txt') ne 0 then $
    spawn, 'cat /viz2/media/SunInTime/mostrecent/1600_time.txt >> ' + times_file

; Append 1700 file time to times.txt file:
    if file_exist('/viz2/media/SunInTime/mostrecent/1700_time.txt') ne 0 then $
    spawn, 'cat /viz2/media/SunInTime/mostrecent/1700_time.txt >> ' + times_file

; Append 4500 file time to times.txt file:
    if file_exist('/viz2/media/SunInTime/mostrecent/4500_time.txt') ne 0 then $
    spawn, 'cat /viz2/media/SunInTime/mostrecent/4500_time.txt >> ' + times_file

; Append continuum file time to times.txt file:
    if file_exist('/viz2/media/SunInTime/mostrecent/cont_time.txt') ne 0 then $
    spawn, 'cat /viz2/media/SunInTime/mostrecent/cont_time.txt >> ' + times_file

; Append filtergram file time to times.txt file:
    if file_exist('/viz2/media/SunInTime/mostrecent/filt_time.txt') ne 0 then $
    spawn, 'cat /viz2/media/SunInTime/mostrecent/filt_time.txt >> ' + times_file

; Mask offlimb:
  rmask = header.RSUN_OBS / header.CDELT1-1 ; slightly smaller
  dd = (findgen(4096)-2048)
  mmask = (((dd##(fltarr(4096)+1.))^2+((fltarr(4096)+1.)##dd)^2) lt (rmask^2))
  ss_offlimb = where(mmask eq 0, n_offlimb)
;  mag = mag*mmask

; Scaling:
  smag = bytscl(mag>(-75)<75)
  smag[ss_offlimb] = 0

  ssmag = congrid(smag,1024,1024,interp=minterp)
  sssmag = congrid(smag,256,256,interp=minterp)

; Read and prepare time-matched 171:
if fset[n_waves+1] ne '' then begin
  read_sdo, fset[n_waves+1], header, image, /uncomp_delete
  t_image = header.date_obs
  exptime = header.aimgshce / 1000.
  wavelnth = header.wavelnth

  image=aia_intscale(image,exptime=exptime,wavelnth=wavelnth,/bytescale)

  aia_lct,r,g,b,wavelnth=wavelnth,/load
  i0171 = image

; Full-disk rebinned to 1024x1024
  fdimage=congrid(image,1024,1024)
; Full-disk rebinned to 256x256
  tdimage=congrid(image,256,256)

; Add timestamps to the mag and 171-for-mag:
  if keyword_set(do_time) then begin
    device,set_resolution=[4096,4096]
    tv,smag
    xyouts,10,75*yoffset(0),'SDO/HMI '+mtimestamp,charsize=4.,color=255,/device,charthick=4.
    smag=tvrd() 
    device,set_resolution=[1024,1024]
    tv,ssmag
    xyouts,10,30*yoffset(0),'SDO/HMI '+mtimestamp,charsize=1.5,color=255,/device 
    ssmag=tvrd() 

; If mag image is stale by more than specified limit, then label it as stale and apologize:
    if ( (keyword_set(do_stale) ) and (( anytim(t_now) - t_image_sec ) gt t_lag_max_hrs*3600d0) ) then begin
       device,set_resolution=[256,256]
       tv, sssmag 
       xyouts,/norm,.065,.95,'     DATA PROCESSING LAG     ',charsize=1.0,color=255, charthick=1.
       xyouts,/norm,.065,.90,' MOST RECENT IMAGE DISPLAYED ',charsize=1.0,color=255, charthick=1.
       buff = anytim(mtimestamp, /yoh)
       xyouts,/norm,.065,.85,'      ' + strmid(buff,0,9) + ' ' + strmid(buff,10,5) + '      ', $
         charsize=1.0,color=255, charthick=1.
       sssmag = tvrd() 
    endif

    device,set_resolution=[4096,4096]
    tv, image
    xyouts,10,75*yoffset(1),'SDO/HMI '+mtimestamp,charsize=4.,color=255,/device,charthick=4.
    i0171=tvrd() 
    device,set_resolution=[1024,1024]
    tv, fdimage
    xyouts,10,30*yoffset(1),'SDO/HMI '+mtimestamp,charsize=1.5,color=255,/device 
    fi0171=tvrd() 
  endif

; Create the 171-mag blend:
  mneg=bytscl(mag>0<75)
  mneg[ss_offlimb] = 0
  mpos=bytscl((-mag)>0<75)
  mpos[ss_offlimb] = 0
; im4=[[[mpos]],[[i0171]],[[mneg]]]
  im4=byte([[[float(mpos)*0.3+float(i0171)*0.7]],$
            [[float(i0171)*0.7+0.1*(mpos+float(mneg))]],$
            [[float(mneg)]]])
  im4a=congrid(im4,1024,1024,3)
  im4b=congrid(im4,256,256,3)
endif

;  window,0,xs=1024,ys=1024 & stop,'Test stop'

  loadct,0 & tvlct,r,g,b,/get

;  write_gif,outpath+'/f'+'_HMImag'+'.gif',smag,r,g,b
  write_jpeg,outpath+'/f'+'_HMImag'+'.jpg',truecolor(smag,r,g,b),true=1,qual=90
;  write_gif,outpath+'/l'+'_HMImag'+'.gif',ssmag,r,g,b
  write_jpeg,outpath+'/l'+'_HMImag'+'.jpg',truecolor(ssmag,r,g,b),true=1,qual=90

parent_hourly = concat_dir('/viz2/media/SunInTime', 'hourly_1k_archive')
mkpath, time2dir(mheader.date_obs), parent=parent_hourly
filtime_hourly = time2file(mheader.date_obs)
filtime_hourly = strmid(filtime_hourly,0,11) + '00'
filnam_hourly = concat_dir(concat_dir(parent_hourly, time2dir(mheader.date_obs)), $
                           'sdo_hourly_1k_' + 'lHMImag_' + $
                           filtime_hourly + '.jpg')

write_jpeg, filnam_hourly, truecolor(ssmag,r,g,b), true=1, qual=90
file_append, concat_dir(concat_dir(parent_hourly, time2dir(mheader.date_obs)), $
                           'sdo_hourly_1k_' + 'lHMImag_' + $
                           filtime_hourly + '.time'), anytim(mheader.date_obs, /ccsds), /new

;  write_gif,outpath+'/t'+'_HMImag'+'.gif',sssmag,r,g,b
  write_jpeg,outpath+'/t'+'_HMImag'+'.jpg',truecolor(sssmag,r,g,b),true=1,qual=90

if ( exist(i0171) ) then begin

  write_jpeg,outpath+'/f_HMImag_171.jpg',im4,true=3,qual=85
  write_jpeg,outpath+'/l_HMImag_171.jpg',im4a,true=3,qual=85
  write_jpeg,outpath+'/t_HMImag_171.jpg',im4b,true=3,qual=85

endif

endif else begin
  fmagfil_nya = '$HOME/soft/idl/idl_startup/suntoday/notyetavailable.jpg
  tmagfil_nya = '$HOME/soft/idl/idl_startup/suntoday/tnotyetavailable.jpg

  cmd = 'cp -p ' + fmagfil_nya + ' ' + outpath+'/f'+'_HMImag'+'.jpg'
  spawn, cmd
  cmd = 'cp -p ' + fmagfil_nya + ' ' + outpath+'/l'+'_HMImag'+'.jpg'
  spawn, cmd
  cmd = 'cp -p ' + tmagfil_nya + ' ' + outpath+'/t'+'_HMImag'+'.jpg'
  spawn, cmd

  cmd = 'cp -p ' + fmagfil_nya + ' ' + outpath+'/f_HMImag_171.jpg'
  spawn, cmd
  cmd = 'cp -p ' + fmagfil_nya + ' ' + outpath+'/l_HMImag_171.jpg'
  spawn, cmd
endelse

free_lun, 1

if keyword_set(do_gif) then begin
  giffiles = concat_dir(outpath, [ $
    't0094.gif', $
    't0193.gif', $
    't0335.gif', $
    't0131.gif', $
    't0211.gif', $
    't0171.gif', $
    't0304.gif' ])
;   't4500.gif', $
;   't1600.gif', $
;   't1700.gif', $
;   't_304_211_171.gif', $
;   't_094_335_193.gif', $
;   't_211_193_171.gif'  $
;    ]

;  giffiles = concat_dir(outpath, [ $
;    'f0094.jpg', $
;    'f0193.jpg', $
;    'f0335.jpg', $
;    'f4500.jpg', $
;    'f0131.jpg', $
;    'f0211.jpg', $
;    'f1600.jpg', $
;    'f0171.jpg', $
;    'f0304.jpg', $
;    'f1700.jpg' ])
;    'f_304_211_171.jpg', $
;    'f_094_335_193.jpg', $
;    'f_211_193_171.jpg' $
;    ]

  hdoc = concat_dir(dir_html, 'index.html')
  html = thumbnail_table_html(giffiles, giffiles, ncols=5, factor=.1)
  file_append, hdoc, html, /new
endif

;cmd = 'cp -p ' + outpath + '/* ' + strtrim(dir_latest_images_1,2)
;spawn, cmd
cmd = 'cp -p ' + outpath + '/* ' + strtrim(dir_latest_images,2)
spawn, cmd

if ( (dir_next_day ne outpath) and (not keyword_set(no_tomorrow)) ) then begin
  cmd = 'cp -p ' + outpath + '/* ' + strtrim(dir_next_day,2)
  spawn, cmd
endif

if keyword_set(showimages) then begin
  set_plot, 'x'
  window,13,xs=showimages,ys=showimages,title='HMI/B'+' '+mtimestamp
  tv,congrid(smag,showimages,showimages)
  window,14,xs=showimages,ys=showimages,title='HMI/B+ 171'+' '+mtimestamp
  tv,congrid(im4,showimages,showimages,3),true=3
  set_plot, cur_val_plot
endif

; Restore original value of plotting device:
set_plot, cur_val_plot

end
