; Time range and FOV
t0 = '2016-01-06T00:00'
t1 = '2016-01-06T01:00'
;t1 = '2016-01-06T23:59'
fov = [2048-128,3600-128,2048+127,3600+127]

; AIA channel
waves = [193]

; Search for data
ssw_jsoc_time2data, t0, t1, index, files, ds='aia.lev1_euv_12s', /files, wave=waves, cadence='12m'
good = where(index.quality EQ 0)
files = files[good]
nframes = n_elements(files)
omovie = fltarr(fov[2]-fov[0]+1,fov[3]-fov[1]+1,nframes)
amovie = fltarr(fov[2]-fov[0]+1,fov[3]-fov[1]+1,nframes)

read_sdo, files, index, data, /use_lib

; Default aia_prep
t_st_sec = anytim(!stime)
aia_prep, index, data, oindex, odata, /verbose, /refresh
t_en_sec = anytim(!stime)
sec_per_img_old_refresh = (t_en_sec - t_st_sec)/nframes

t_st_sec = anytim(!stime)
aia_prep, index, data, oindex, odata, /verbose, refresh=0 ; /refresh
;aia_prep, index, data, sindex, sdata, /verbose, ds='sdo.master_pointing', /refresh, /use_sswmp
t_en_sec = anytim(!stime)
sec_per_img_old_no_refresh = (t_en_sec - t_st_sec)/nframes

; Use AIA-specific 3h cadence master pointing series. Must have access to jsoc2
t_st_sec = anytim(!stime)
aia_prep, index, data, aindex, adata, /verbose, ds='aia_test.master_pointing3h', /use_sswmp, refresh=0 ; /refresh
t_en_sec = anytim(!stime)
sec_per_img_new = (t_en_sec - t_st_sec)/nframes

omovie[*,*,0:nframes-1] = odata[fov[0]:fov[2],fov[1]:fov[3],0:nframes-1]
amovie[*,*,0:nframes-1] = adata[fov[0]:fov[2],fov[1]:fov[3],0:nframes-1]

for n=0,nframes-1 do omovie[*,*,n] /= oindex[n].exptime
for n=0,nframes-1 do amovie[*,*,n] /= aindex[n].exptime

odt = anytim(oindex[1:nframes-1].date_obs)-anytim(oindex[0:nframes-2].date_obs)
orunning =  total(total(abs((omovie[*,*,1:nframes-1]-omovie[*,*,0:nframes-1])),1,/double),1,/double)/odt

adt = anytim(aindex[1:nframes-1].date_obs)-anytim(aindex[0:nframes-2].date_obs)
arunning =  total(total(abs((amovie[*,*,1:nframes-1]-amovie[*,*,0:nframes-1])),1,/double),1,/double)/adt

; Find displacements between consecutive images. tr_get_disp is in the
; SSW TRACE respository. Add trace to SSW_INSTR and do ssw_upgrade,
; /spawn to grab routine.

adisp = tr_Get_disp(amovie)
odisp = tr_get_disp(omovie)

;stack = float([[[odata[fov[0]:fov[2],fov[1]:fov[3]]]], $
;               ;[[sdata[fov[0]:fov[2],fov[1]:fov[3]]]], $
;               [[adata[fov[0]:fov[2],fov[1]:fov[3]]]]])
;disp = tr_get_disp(stack)

print, ' Time per frame old_refresh    : ' + strtrim(sec_per_img_old_refresh,2)
print, ' Time per frame old_no_refresh : ' + strtrim(sec_per_img_old_no_refresh,2)
print, ' Time per frame new : ' + strtrim(sec_per_img_new,2)

end
