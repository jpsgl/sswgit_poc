; Author: M. Cheung (cheung@lmsal.com), with modifications by G. Chintzoglou (gchintzo@lmsal.com)
; Purpose: Deconvolve AIA full disk level 1 images before passing on to aia_prep. 
;          Only works locally on the kyoto machine because it uses the Nvidia K40c GPU. 
; Reference: The GPU implementation of the deconvolution routine is described in 
;            http://on-demand.gputechconf.com/gtc/2015/presentation/S5209-Mark-Cheung.pdf
;
;	INPUTS/OUTPUTS:
;
;        INPUT:
;		FILES	-   array containing the input level 1 filenames
;
;		OUTDIR  -   path to store the temporary level 1 deconvolved data
;
;               SILENT  -   (OPTIONAL) suppresses verbosity from routine & read_sdo.
;
;	 OUTPUT:
;		OINDEX  -   output index structure of deconvolved + aia_prepped data
;
;		ODATA   -   output image (or image cube) deconvolved + aia_prep
;		
;	 USAGE:
;
;        IDL>aia_superprep, files, oindex, odata, outdir='/tmp/myusername/'
;
;	 Alternative Usage:
;
;        IDL> read_sdo, files, index, data
;        IDL> aia_superprep, files, index, data, oindex, odata, outdir='/tmp/myusername'
;
; History:      
;        Relaxed the requirement to input index and data externally through the command line. Now program calls "read_sdo" internally and the index and data arrays are outputs - G.C. 03-May-2017         
;
;


pro aia_superprep, files, index, data, oindex, odata, outdir=outdir, silent=silent
  nfiles = n_elements(files)

  if (n_elements(oindex) EQ 0) OR (n_elements(odata) EQ 0) then begin
  if ~keyword_set(silent) then print, '----------------'
  if ~keyword_set(silent) then print, 'reading data using files array.'
  if ~keyword_set(silent) then print, 'If index and data arrays available use alternative syntax:'
  if ~keyword_set(silent) then print, 'aia_superprep, files, index, data, outdir=outdir'
  if ~keyword_set(silent) then print, '----------------'

  read_sdo, files, index, data, /use_shared, silent=silent
  endif

  bin = '/sanhome/cheung/CUDA7/samples/bin/linux/release/deconv_many_conjugate'
  psfbase = '/tmp/cheung/shifted_psf_' ;'/sanhome/cheung/AIA/weber_psfs_rc/shifted_psf_'
  imgdir = ''
  if n_elements(outdir) EQ 0 then begin
     ;print, "Please give the outdir=outdir parameter. This is used to temporarily store the deconvolved level 1 file."
     ;print, "e.g. use /tmp/$
     print, "Warning: outdir parameter not specified. Defaulting to /tmp/$USER."
     outdir = '/tmp/'+getenv('USER')
  endif

  for n=0,nfiles-1 do begin
     ;read_sdo, files[n], index, /use_shared
     ;print,minmax(data)
     psf = psfbase + string(index.wavelnth,format='(I04)')+'.fits'
     imgDir = strmid(files[n],0,(strsplit(files[n],'/'))[-1])
     ;temporary file for GPU routine     
     file = strmid(files[n],(strsplit(files[n],'/'))[-1], strlen(files[n])-1)
     cmd = bin +' --psf='+psf + ' --imgs="'+file + '" --imgDir='+imgDir + ' --outDir='+outDir+ ' --device=0 --iters=25'
     spawn, cmd
     if ~keyword_set(silent) then print, ''
     if ~keyword_set(silent) then print, 'Saved output as : '+outDir+'/'+file
     mreadfits, outDir+'/'+file, index2, data2
     print,minmax(data)
     print, minmax(data2)
     aia_prep, index[n], data2, this_oindex, this_odata
     ;if n_elements(oindex) EQ 0 then begin
     if n EQ 0 then begin
        ooindex=this_oindex
        oodata = this_odata
     endif else begin
        ooindex = [ooindex, this_oindex]
        oodata = [[[oodata]],[[this_odata]]]
     endelse
  endfor

  ;if (keyword_set(oindex) EQ 0) OR (keyword_set(odata) EQ 0) then begin
  ;   if ~keyword_set(silent) then print, 'returning index and data outputs.'
  ;   index=ooindex
  ;   data=oodata
  ;endif else begin
     ;if index and data are fed in through the command line and oindex and odata arrays are desired to return the deconvolved data
  ;   if ~keyword_set(silent) then print, 'returning oindex and odata outputs.'
     oindex=ooindex
     odata=oodata
  ;endelse

end
