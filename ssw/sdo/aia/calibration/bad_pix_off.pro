
function bad_pix_off, image, ss_bad, width=width

if not exist(width) then width = 2

ds = size(image)
nx = ds(1)
ny = ds(2)

tp = size(image, /type)

; Expand image periodically to handle edge points:
image2 = make_array(nx+(2*width),ny+(2*width),type=tp)
mask_bad = image2

image2[width,width] = image
image2[0,0] = rotate(image2[width+1:2*width,*],5)
image2[nx+width,0] = rotate(image2[(nx-width):(nx-1),*],5)
image2[0,0] = rotate(image2[*,width+1:2*width],7)
image2[0,ny+width] = rotate(image2[*,(ny-width):(ny-1)],7)

ds2 = size(image2)
nx2 = ds2(1)
ny2 = ds2(2)

xbad = ss_bad mod nx
ybad = ss_bad  /  nx
xbad2 = xbad + width
ybad2 = ybad + width
ss_bad2 = ybad2*nx2 + xbad2
mask_bad[ss_bad2] = 1

; Nearest neighbor replacement of bad pixels:

xoff_arr = [-1, 1, 0, 0, -1, 1,-1, 1,-2, 2, 0, 0]
yoff_arr = [ 0, 0,-1, 1, -1,-1, 1, 1, 0, 0,-2, 2]
not_finished = 1
i = 0
while not_finished do begin
  xsamp = xbad2 + xoff_arr[i]
  ysamp = ybad2 + yoff_arr[i]
  ss_samp = ysamp*nx2 + xsamp
;STOP
  image2[ss_bad2] = image2[ss_samp]
  mask_bad[ss_bad2] = mask_bad[ss_samp]
  ss_bad_remaining = where(mask_bad[width:width+nx-1,width:width+ny-1] eq 1, n_bad_remaining)
  xbad_remaining = ss_bad_remaining mod nx
  ybad_remaining = ss_bad_remaining  /  nx
  xbad2 = xbad_remaining + width
  ybad2 = ybad_remaining + width
  ss_bad2 = ybad2*nx2 + xbad2
  if n_bad_remaining gt 0 then not_finished = 1 else not_finished = 0
  i = i + 1
;STOP
endwhile

; Extract original image from image2 (with bad pixels replaced):
image3 = image2[width:width+nx-1,width:width+ny-1]

return, image3

end
