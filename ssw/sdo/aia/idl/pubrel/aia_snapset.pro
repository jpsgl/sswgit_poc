
pro aia_snapset, fset=fset, imset=imset, indset=indset, t_mod=t_mod, do_write=do_write, do_z=do_z, outpath=outpath, $
  dir_top_gif=dir_top_gif, do_despike=do_despike, do_shift=do_shift, offlimb=offlimb, do_display=do_display, $
  do_time=do_time, mag_only=mag_only, minterp=minterp, error=error, imageRejected=imageRejected, do_gifs=do_gifs, $
  dir_html=dir_html, t_lag_max_hrs=t_lag_max_hrs, qstop=qstop, $
  f0171=f0171, f0193=f0193, f0211=f0211, f0335=f0335, f0094=f0094, $
  f0131=f0131, f0304=f0304, f4500=f4500, f1700=f1700, f1600=f1600

;+
; Purpose: Given an index, data pair or a fits file name for any AIA image, prepare the image
;          for use by "The Sun Today" page
;-

; Set defaults:
;if not exist(outpath) then outpath='/net/castor/Users/slater/data/aia_snapset/SunInTime'
;if not exist(dir_top_gif) then dir_top_gif = '/net/solserv/home/slater/public_html/aia_snapset/SunInTime'
if not exist(outpath) then outpath='/archive/sdo/media/SunInTime'
if keyword_set(showimages) then if showimages lt 10 then showimages=512
if not exist(do_despike) then unspike = 0 else unspike = do_despike
if not exist(do_shift) then noshift = 1 else noshift = 1-do_shift
if not exist(do_time) then do_time = 1
if not exist(dir_html) then dir_html = outpath
if not exist(dir_latest_images) then dir_latest_images = '/archive/sdo/media/SunInTime/mostrecent'
if not exist(dir_latest_images_1) then dir_latest_images_1 = '/net/solserv/home/slater/public_html/outgoing/sdo/mostrecent'
if not exist(t_lag_max_hrs) then t_lag_max_hrs = 2.0

; Save current value of plotting device:
cur_val_plot = !d.name
if keyword_set(do_z) then set_plot,'z'

; Initialize error parameters to integer 0
error = ''
imageRejected = 0

limit = 10. ; max multiplier of off-disk signal
waves = [171,193,211,335, 94,131,304,4500,1600,1700]
limitss = [5,  5,   5,  3,  3,  3, 10,  1.,  1.,  1.]

if not exist(fset) then $
  fset = [f0171, f0193, f0211, f0335, f0094, f0131, f0304, f4500, f1700, f1600]

yoffset=1.+findgen(n_elements(waves))

; set to work with:
timestamp=strmid(fset(0),strpos(fset(0),'AIA20')+3,15)

if not keyword_set(mag_only) then begin
  free_lun, 1
  openw, 1, concat_dir(outpath, 'times.txt')
endif

n_waves=n_elements(fset)-2
for i=0,n_waves-1 do begin

if fset[i] ne '' then begin

  print,'Starting with ' + strtrim(waves[i],2)
;  read_sdo, fset[i], header, image, /uncomp_delete
header = indset[i]
image = imset[*,*,i]
  t_image = header.date_obs
  t_image_sec = anytim(t_image)

  if not keyword_set(mag_only) then begin
    free_lun, 2
    openw, 2, concat_dir(outpath, 'one_time.txt')
    printf, 2, anytim(t_image, /yoh)
    free_lun, 2

    js_doc = concat_dir(outpath, 'one_time.js')
    file_append, js_doc, 'function getDateUpdated() {', /new
    file_append, js_doc, '   var date_updated_string = "<i>AIA images shown were taken at about " + '
    file_append, js_doc, '	    "' + strtrim(anytim(t_image, /yoh),2) + '"'
    file_append, js_doc, '	     + " UT</i>";'
    file_append, js_doc, '   return date_updated_string;'
    file_append, js_doc, '}'
  endif

  exptime = header.aimgshce / 1000.
  wavelnth = header.wavelnth

  if unspike eq 1 then image=trace_unspike(image,sens=.3)>0

  if keyword_set(offlimb) then aia_offlimb,image,wavelnth=wavelnth,limit=limitss(i)<limit, t0=t_image else $
     image=((image-aia_corners(image,wavelnth=wavelnth,noshift=noshift,t0=t_image)))>0

  image=aia_intscale(image,exptime=exptime,wavelnth=wavelnth,/bytescale)

  aia_lct,r,g,b,wavelnth=wavelnth,/load
; full-disk rebinned to 1024x1024
  fdimage=congrid(image,1024,1024)
; full-disk rebinned to 256x256
  tdimage=congrid(image,256,256)
;
  timestamp=strmid(fset(i),strpos(fset(i),'AIA20')+3,15)
; add timestamps to the images
  if keyword_set(do_time) then begin
     outsind=where(wavelnth eq waves)
     device,set_resolution=[4096,4096]
     tv,image 
     xyouts,10,75*yoffset(outsind),'SDO/AIA-'+strcompress(string(fix(wavelnth),format='(i4)'))+' '+timestamp, $
            charsize=4.0,color=255, /device, charthick=4. 
     image=tvrd() 
     device,set_resolution=[1024,1024]
     tv,fdimage
     xyouts,10,30*yoffset(outsind),'SDO/AIA-'+strcompress(string(fix(wavelnth),format='(i4)'))+' '+timestamp, $
            charsize=1.5,color=255, /device 
     fdimage=tvrd() 
;     device,set_resolution=[256,256]
;     tv,tdimage
;     xyouts,10,10*yoffset(outsind),'SDO/AIA-'+strcompress(string(fix(wavelnth),format='(i4)'))+' '+timestamp, $
;            charsize=1.0,color=255, /device 
;     tdimage=tvrd() 
  endif

; Add additional annotation to thumbnails, as needed:

; If image is stale by more than specified limit, then label it as stale and apologize:
  if ( anytim(ut_time()) - t_image_sec ) gt t_lag_max_hrs*3600d0 then begin
     device,set_resolution=[256,256]
     tv, tdimage 
;     xyouts,/norm,.1,.8,'YOUR LOGO HERE',charsize=5.0,color=255, /device, charthick=6.
;     xyouts,/norm,.1,.6,'Call 1-800-SDO-RULE'
     xyouts,/norm,.065,.95,'     DATA PROCESSING LAG     ',charsize=1.0,color=255, charthick=1.
     xyouts,/norm,.065,.90,' MOST RECENT IMAGE DISPLAYED ',charsize=1.0,color=255, charthick=1.
     buff = anytim(t_image, /yoh)
     xyouts,/norm,.065,.85,'      ' + strmid(buff,0,9) + ' ' + strmid(buff,10,5) + '      ', $
       charsize=1.0,color=255, charthick=1.
     tdimage = tvrd() 
  endif

; Label calibration mode images as such:
  if (header.aiftsid gt 49155) then begin
     device,set_resolution=[256,256]
     tv, tdimage 
     xyouts,/norm,.065,.95,'      CALIBRATION IMAGE      ',charsize=1.0,color=255, charthick=1.
;     xyouts,/norm,.065,.90,' MOST RECENT IMAGE DISPLAYED ',charsize=1.0,color=255, charthick=1.
     tdimage = tvrd() 
  endif

; Label eclipse mode images as such:
  if (header.aiagp6 ne 0) then begin
     device,set_resolution=[256,256]
     tv, tdimage 
     xyouts,/norm,.065,.85,'     ECLIPSE IN PROGRESS     ',charsize=1.0,color=255, charthick=1.
;     xyouts,/norm,.065,.95,'      CALIBRATION IMAGE      ',charsize=1.0,color=255, charthick=1.
;     xyouts,/norm,.065,.90,' MOST RECENT IMAGE DISPLAYED ',charsize=1.0,color=255, charthick=1.
     tdimage = tvrd() 
  endif

; write_gif,outpath+'/f'+counterstring(wavelnth,4)+'.gif',image,r,g,b
  write_jpeg,outpath+'/f'+counterstring(wavelnth,4)+'.jpg',truecolor(image,r,g,b),true=1,qual=90
; write_gif,outpath+'/l'+counterstring(wavelnth,4)+'.gif',fdimage,r,g,b
  write_jpeg,outpath+'/l'+counterstring(wavelnth,4)+'.jpg',truecolor(fdimage,r,g,b),true=1,qual=90
  if keyword_set(do_gif) then write_gif,dir_top_gif+'/t'+counterstring(wavelnth,4)+'.gif',tdimage,r,g,b
  write_jpeg,outpath+'/t'+counterstring(wavelnth,4)+'.jpg',truecolor(tdimage,r,g,b),true=1,qual=90

  if not keyword_set(mag_only) then printf,1,counterstring(wavelnth,4)+': '+timestamp

  if i eq 0 then i0171=image ; save for true-color image
  if i eq 1 then i0193=image ; save for true-color image
  if i eq 2 then i0211=image ; save for true-color image
  if i eq 3 then i0335=image ; save for true-color image
  if i eq 4 then i0094=image ; save for true-color image
  if i eq 5 then i0131=image ; save for true-color image
  if i eq 6 then i0304=image ; save for true-color image

  if i eq 0 then fi0171=fdimage ; save for true-color image
  if i eq 1 then fi0193=fdimage ; save for true-color image
  if i eq 2 then fi0211=fdimage ; save for true-color image
  if i eq 3 then fi0335=fdimage ; save for true-color image
  if i eq 4 then fi0094=fdimage ; save for true-color image
  if i eq 5 then fi0131=fdimage ; save for true-color image
  if i eq 6 then fi0304=fdimage ; save for true-color image

  if i eq 0 then ti0171=tdimage ; save for true-color image
  if i eq 1 then ti0193=tdimage ; save for true-color image
  if i eq 2 then ti0211=tdimage ; save for true-color image
  if i eq 3 then ti0335=tdimage ; save for true-color image
  if i eq 4 then ti0094=tdimage ; save for true-color image
  if i eq 5 then ti0131=tdimage ; save for true-color image
  if i eq 6 then ti0304=tdimage ; save for true-color image

  if keyword_set(showimages) then begin
    window,i,xs=showimages,ys=showimages,title='AIA'+strcompress(string(fix(wavelnth)))+' '+timestamp
    tv,congrid(image,showimages,showimages)

  endif

endif

endfor

if ( exist(i0211) and exist(i0193) and exist(i0171) ) then begin

; 1-2MK EUV combo
  im2=[[[i0211]],[[i0193]],[[i0171]]]
  write_jpeg,outpath+'/f_211_193_171.jpg',im2,true=3,qual=85
  im2a=[[[fi0211]],[[fi0193]],[[fi0171]]]
  write_jpeg,outpath+'/l_211_193_171.jpg',im2a,true=3,qual=85
  im2b=[[[ti0211]],[[ti0193]],[[ti0171]]]
; write_gif,outpath+'/t_211_193_171.gif',im2b
  write_jpeg,outpath+'/t_211_193_171.jpg',im2b,true=3,qual=85

  if keyword_set(showimages) then begin
    window,10,xs=showimages,ys=showimages,title='AIA 211_193_171'+' '+timestamp
    tv,congrid(im2,showimages,showimages,3),true=3
  endif

endif

if ( exist(i0094) and exist(i0335) and exist(i0193) ) then begin

; higher-T mix:
  im3=[[[i0094]],[[i0335]],[[i0193]]] 
  write_jpeg,outpath+'/f_094_335_193.jpg',im3,true=3,qual=85
  im3a=[[[fi0094]],[[fi0335]],[[fi0193]]] 
  write_jpeg,outpath+'/l_094_335_193.jpg',im3a,true=3,qual=85
  im3b=[[[ti0094]],[[ti0335]],[[ti0193]]] 
; write_gif,outpath+'/t_094_335_193.gif',im3b
  write_jpeg,outpath+'/t_094_335_193.jpg',im3b,true=3,qual=85

  if keyword_set(showimages) then begin
    window,11,xs=showimages,ys=showimages,title='AIA 094_335_193'+' '+timestamp
    tv,congrid(im3,showimages,showimages,3),true=3
  endif

endif

if ( exist(i0304) and exist(i0211) and exist(i0171) ) then begin

; 304+cor mix:
  im4=[[[i0304]],[[i0211]],[[i0171]]]
  write_jpeg,outpath+'/f_304_211_171.jpg',im4,true=3,qual=85
  im4a=[[[fi0304]],[[fi0211]],[[fi0171]]]
  write_jpeg,outpath+'/l_304_211_171.jpg',im4a,true=3,qual=85
  im4b=[[[ti0304]],[[ti0211]],[[ti0171]]]
; write_gif,outpath+'/t_304_211_171.gif',im4b
  write_jpeg,outpath+'/t_304_211_171.jpg',im4b,true=3,qual=85

  if keyword_set(showimages) then begin
    window,12,xs=showimages,ys=showimages,title='AIA 304_211_171'+' '+timestamp
    tv,congrid(im4,showimages,showimages,3),true=3
  endif

endif

; Process los magnetogram, if available:
if fset[n_waves] ne '' then begin
  read_sdo, fset[n_waves], mheader, mag, /uncomp_delete
  mtimestamp = anytim(mheader.date_obs, /ccsds)
  if not keyword_set(mag_only) then begin
;    free_lun, 1
;    openu, 1, concat_dir(outpath, 'times.txt')
    file_append, concat_dir(outpath, 'times.txt'), 'HMIB: '+ time2file(mtimestamp, /sec)
  endif else begin
;    printf,1,'HMIB: '+ time2file(mtimestamp, /sec)
    file_append, concat_dir(outpath, 'times.txt'), 'HMIB: '+ time2file(mtimestamp, /sec, /new)
  endelse

; Mask offlimb:
  rmask = header.RSUN_OBS / header.CDELT1-1 ; slightly smaller
  dd = (findgen(4096)-2048)
  mmask = (((dd##(fltarr(4096)+1.))^2+((fltarr(4096)+1.)##dd)^2) lt (rmask^2))
  ss_offlimb = where(mmask eq 0, n_offlimb)
;  mag = mag*mmask

; Scaling:
  smag = bytscl(mag>(-75)<75)
  smag[ss_offlimb] = 0

  ssmag = congrid(smag,1024,1024,interp=minterp)
  sssmag = congrid(smag,256,256,interp=minterp)

; Read and prepare time-matched 171:
if fset[n_waves+1] ne '' then begin
  read_sdo, fset[n_waves+1], header, image, /uncomp_delete
  t_image = header.date_obs
  exptime = header.aimgshce / 1000.
  wavelnth = header.wavelnth

  image=aia_intscale(image,exptime=exptime,wavelnth=wavelnth,/bytescale)

  aia_lct,r,g,b,wavelnth=wavelnth,/load
  i0171 = image

; Full-disk rebinned to 1024x1024
  fdimage=congrid(image,1024,1024)
; Full-disk rebinned to 256x256
  tdimage=congrid(image,256,256)

; Add timestamps to the mag and 171-for-mag:
  if keyword_set(do_time) then begin
    device,set_resolution=[4096,4096]
    tv,smag
    xyouts,10,75*yoffset(0),'SDO/HMI '+mtimestamp,charsize=4.,color=255,/device,charthick=4.
    smag=tvrd() 
    device,set_resolution=[1024,1024]
    tv,ssmag
    xyouts,10,30*yoffset(0),'SDO/HMI '+mtimestamp,charsize=1.5,color=255,/device 
    ssmag=tvrd() 

; If mag image is stale by more than specified limit, then label it as stale and apologize:
    if ( anytim(ut_time()) - t_image_sec ) gt t_lag_max_hrs*3600d0 then begin
       device,set_resolution=[256,256]
       tv, sssmag 
       xyouts,/norm,.065,.95,'     DATA PROCESSING LAG     ',charsize=1.0,color=255, charthick=1.
       xyouts,/norm,.065,.90,' MOST RECENT IMAGE DISPLAYED ',charsize=1.0,color=255, charthick=1.
       buff = anytim(mtimestamp, /yoh)
       xyouts,/norm,.065,.85,'      ' + strmid(buff,0,9) + ' ' + strmid(buff,10,5) + '      ', $
         charsize=1.0,color=255, charthick=1.
       sssmag = tvrd() 
    endif

    device,set_resolution=[4096,4096]
    tv, image
    xyouts,10,75*yoffset(1),'SDO/HMI '+mtimestamp,charsize=4.,color=255,/device,charthick=4.
    i0171=tvrd() 
    device,set_resolution=[1024,1024]
    tv, fdimage
    xyouts,10,30*yoffset(1),'SDO/HMI '+mtimestamp,charsize=1.5,color=255,/device 
    fi0171=tvrd() 
  endif

; Create the 171-mag blend:
  mpos=bytscl(mag>0<75)
  mpos[ss_offlimb] = 0
  mneg=bytscl((-mag)>0<75)
  mneg[ss_offlimb] = 0
; im4=[[[mpos]],[[i0171]],[[mneg]]]
  im4=byte([[[float(mpos)*0.3+float(i0171)*0.7]],$
            [[float(i0171)*0.7+0.1*(mpos+float(mneg))]],$
            [[float(mneg)]]])
  im4a=congrid(im4,1024,1024,3)
  im4b=congrid(im4,256,256,3)
endif

;  window,0,xs=1024,ys=1024 & stop,'Test stop'

  loadct,0 & tvlct,r,g,b,/get

;  write_gif,outpath+'/f'+'_HMImag'+'.gif',smag,r,g,b
  write_jpeg,outpath+'/f'+'_HMImag'+'.jpg',truecolor(smag,r,g,b),true=1,qual=90
;  write_gif,outpath+'/l'+'_HMImag'+'.gif',ssmag,r,g,b
  write_jpeg,outpath+'/l'+'_HMImag'+'.jpg',truecolor(ssmag,r,g,b),true=1,qual=90
;  write_gif,outpath+'/t'+'_HMImag'+'.gif',sssmag,r,g,b
  write_jpeg,outpath+'/t'+'_HMImag'+'.jpg',truecolor(sssmag,r,g,b),true=1,qual=90

if ( exist(i0171) ) then begin

  write_jpeg,outpath+'/f_HMImag_171.jpg',im4,true=3,qual=85
  write_jpeg,outpath+'/l_HMImag_171.jpg',im4a,true=3,qual=85
  write_jpeg,outpath+'/t_HMImag_171.jpg',im4b,true=3,qual=85

endif

endif else begin
  fmagfil_nya = '/net/castor/Users/slater/soft/idl/idl_startup/karel_works/notyetavailable.jpg
  tmagfil_nya = '/net/castor/Users/slater/soft/idl/idl_startup/karel_works/tnotyetavailable.jpg

  cmd = 'cp -p ' + fmagfil_nya + ' ' + outpath+'/f'+'_HMImag'+'.jpg'
  spawn, cmd
  cmd = 'cp -p ' + fmagfil_nya + ' ' + outpath+'/l'+'_HMImag'+'.jpg'
  spawn, cmd
  cmd = 'cp -p ' + tmagfil_nya + ' ' + outpath+'/t'+'_HMImag'+'.jpg'
  spawn, cmd

  cmd = 'cp -p ' + fmagfil_nya + ' ' + outpath+'/f_HMImag_171.jpg'
  spawn, cmd
  cmd = 'cp -p ' + fmagfil_nya + ' ' + outpath+'/l_HMImag_171.jpg'
  spawn, cmd
endelse

free_lun, 1

if keyword_set(do_gif) then begin
  giffiles = concat_dir(outpath, [ $
    't0094.gif', $
    't0193.gif', $
    't0335.gif', $
    't4500.gif', $
    't0131.gif', $
    't0211.gif', $
    't1600.gif', $
    't0171.gif', $
    't0304.gif', $
    't1700.gif' ])
;    't_304_211_171.gif', $
;    't_094_335_193.gif', $
;    't_211_193_171.gif' $
;    ]

;  giffiles = concat_dir(outpath, [ $
;    'f0094.jpg', $
;    'f0193.jpg', $
;    'f0335.jpg', $
;    'f4500.jpg', $
;    'f0131.jpg', $
;    'f0211.jpg', $
;    'f1600.jpg', $
;    'f0171.jpg', $
;    'f0304.jpg', $
;    'f1700.jpg' ])
;    'f_304_211_171.jpg', $
;    'f_094_335_193.jpg', $
;    'f_211_193_171.jpg' $
;    ]

  hdoc = concat_dir(dir_html, 'index.html')
  html = thumbnail_table_html(giffiles, giffiles, ncols=5, factor=.1)
  file_append, hdoc, html, /new
endif

;cmd = 'cp -p ' + outpath + '/* ' + strtrim(dir_latest_images_1,2)
;spawn, cmd
cmd = 'cp -p ' + outpath + '/* ' + strtrim(dir_latest_images,2)
spawn, cmd

if keyword_set(showimages) then begin
  set_plot, 'x'
  window,13,xs=showimages,ys=showimages,title='HMI/B'+' '+mtimestamp
  tv,congrid(smag,showimages,showimages)
  window,14,xs=showimages,ys=showimages,title='HMI/B+ 171'+' '+mtimestamp
  tv,congrid(im4,showimages,showimages,3),true=3
  set_plot, cur_val_plot
endif

; Restore original value of plotting device:
set_plot, cur_val_plot

end
