; function for coarse first-estimate intensity scaling of the images
;
; Karel Schrijver 2010/04/12
;
function aia_intscale,image,exptime=exptime,wavelnth=wavelnth,bytescale=bytescale
;
; allowed values of wavelnth:
wave=[1600,1700,4500,94,131,171,193,211,304,335,6173]
select=where(nint(wavelnth) eq wave)
if select(0) eq -1 then begin
  print,'aia_lct: selected invalid wavelength/channel'
  return,-1
endif 

;stop,'* aia_intscale'
;print,select,wave(select)

if keyword_set(bytescale) then begin
case select of
0: return,bytscl(       (image*(2.99911/exptime)), max = 1000) 
; 1: return,bytscl(     (image*(1.00026/exptime)), max = 2500)
1: return,bytscl(       (image*(1.00026        )), max = 2500)
2: return,bytscl(       (image*(1.00026/exptime)<(26000.)))
3: return,bytscl(  sqrt((image*(4.99803/exptime)>(   1.5/ 1.06 )<(   50/ 1.06 ))))
4: return,bytscl(alog10((image*(6.99685/exptime)>(   7.0/ 1.49 )<( 1200/ 1.49 ))))
5: return,bytscl(  sqrt((image*(4.99803/exptime)>(  10.0/ 1.49 )<( 6000/ 1.49 ))))
6: return,bytscl(alog10((image*(2.99950/exptime)>( 120.0/ 1.08 )<( 6000/ 1.08 ))))
7: return,bytscl(alog10((image*(4.99801/exptime)>(  30.0/ 1.10 )<(13000/ 1.10 ))))
8: return,bytscl(alog10((image*(4.99941/exptime)>(  50.0/12.11 )<( 2000/12.11  ))))
9: return,bytscl(alog10((image*(6.99734/exptime)>(   3.5/ 2.97 )<( 1000/ 2.97 ))))
10: return,bytscl(0>(image*(1.0/exptime)<65535.))
endcase
; Original 304 scaling - used until ~14-jul-2011:
; 8: return,bytscl(alog10((image*(4.99941/exptime)>50<2000)))
; Scaling used for 304A from ~14-jul-2011 until 19-jul-2016:
; 8: return,bytscl(alog10((image*(4.99941/exptime)>15<600)))
endif else begin
case select of
0: return,((image*(2.99911/exptime)<1000))
1: return,((image*(1.00026/exptime)<2500))
2: return,((image*(1.00026/exptime)<26000.))
3: return,(sqrt((image*(4.99803/exptime)>1.5<50)))
4: return,(alog10((image*(6.99685/exptime)>7<1200)))
5: return,(sqrt((image*(4.99803/exptime)>10<6000)))
6: return,(alog10((image*(2.99950/exptime)>120<6000)))
7: return,(alog10((image*(4.99801/exptime)>30<13000)))
8: return,(alog10((image*(4.99941/exptime)>(50/10.9)<(2000/10.9))))
8: return,(alog10((image*(4.99941/exptime)>15<600)))
9: return,(alog10((image*(6.99734/exptime)>3.5<1000)))
10: return,(0>(image*(1.0/exptime)<65535.))
endcase
endelse
;
return,-1
end
