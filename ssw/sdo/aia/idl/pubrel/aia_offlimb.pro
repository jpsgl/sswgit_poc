; procedure to amplify signal off the limb:
; 
; input: scaleheight in km
; output: mask (can be reused if provided as input)
;
; N.B. When offlimb multiplier is used, then images must have been
; centered already!
;
; Karel Schrijver     2010/04/12
;
pro aia_offlimb,image,scaleheight=scaleheight,limit=limit,mask=mask,wavelnth=wavelnth,$
                noshift=noshift
; default scale height:
if not(keyword_set(scaleheight)) then scaleheight=1.e5
; limit amplifier
if not(keyword_set(limit)) then limit=20.

; setup mask array:
if not(keyword_set(mask)) then begin
  x=(findgen(4096)-2047.5)*0.6085*745./700000.
  yim=x##(fltarr(4096)+1.)
  xim=(fltarr(4096)+1.)##x
  mask=((sqrt(xim*xim+yim*yim)-1)>0)*7.e5/scaleheight
  mask=exp(mask)<limit
  mask=aia_centershift(mask,wavelnth=wavelnth)
endif
; determine the 'dark signal' in the corners, then multiply off limb signal:
; make positive definite on output
image=((image-aia_corners(image,wavelnth=wavelnth,noshift=noshift))*mask)>0
;
return
end
