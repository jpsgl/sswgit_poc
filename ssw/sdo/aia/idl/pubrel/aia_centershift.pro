; function to shift AIA images to common center:
;
; ; Same pzt offsets are in firstlight.pro and secondlight.pro (per e-Boerner 2010/04/12)
function aia_centershift,image,wavelnth=wavelnth,noshift=noshift

; enable test whether xcen,ycen were properly set in lev1 data files
if keyword_set(noshift) then return,image

; allowed values of wavelnth:
wave=[1600,1700,4500,94,131,171,193,211,304,335]
select=where(nint(wavelnth) eq wave)
if select(0) eq -1 then begin
  print,'aia_lct: selected invalid wavelength/channel'
  return,-1
endif 

; image centers were initially determined from firstlight and in lev1 headers, but
; with finer corrections for 2010/04/08 flare/eruption event:
xc4500=0; 2048.
xc1600=0; 2048.
xc1700=0; 2048.
xc0171=0; 2048.
yc4500=0; 2075.
yc1600=0; 2075.
yc1700=0; 2075.
yc0171=0; 2075.
xc0094=+12; 2037.
xc0304=+12; 2037.
yc0094=-32; 2104.
yc0304=-32; 2104.
xc0131=-3; 2057.
xc0335=-3; 2057.
yc0131=-13; 2089.
yc0335=-13; 2089.
xc0211=+18; 2029.
xc0193=+12; 2029.
yc0211=-23; 2095.
yc0193=-23; 2095.

case select of
0: xc=xc1600
1: xc=xc1700
2: xc=xc4500
3: xc=xc0094
4: xc=xc0131
5: xc=xc0171
6: xc=xc0193
7: xc=xc0211
8: xc=xc0304
9: xc=xc0335
end
case select of
0: yc=yc1600
1: yc=yc1700
2: yc=yc4500
3: yc=yc0094
4: yc=yc0131
5: yc=yc0171
6: yc=yc0193
7: yc=yc0211
8: yc=yc0304
9: yc=yc0335
end

;print,wavelnth,xc,yc
return,shift(image,xc,yc)
end
