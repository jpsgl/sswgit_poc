
pro aia_image_finder, t_now=t_now, delt_arr=delt_arr, delt_lag=delt_lag, level=level, dir_top=dir_top, $
  do_mkdir=do_mkdir, do_snap=do_snap, fset=fset, use_test_set=use_test_set, do_time=do_time, $
  mag_only=mag_only, use_sdo_cat=use_sdo_cat, minterp=minterp, verbose=verbose

;if not exist(dir_top) then dir_top = '/net/castor/Users/slater/data/aia_snapset/SunInTime'
;if not exist(dir_top) then dir_top = '/net/solserv/home/slater/public_html/SunInTime'
if not exist(dir_top) then dir_top = '/archive/sdo/media/SunInTime'
dir_scratch = concat_dir(get_logenv('HOME'),'data/scratch/suntoday_scratch')
if not exist(do_mkdir) then do_mkdir = 0
if not exist(delt_arr) then delt_arr = [-1, -6,-12,-24,-48,-72]
;if not exist(delt_arr) then delt_arr = [-01,-02,-04,-08,-16,-32,-64]
if not exist(delt_mag) then delt_mag = -24
if not exist(delt_lag) then delt_lag = 0d0 ; 3*3600d0
if not keyword_set(do_time) then do_time = 1
if not keyword_set(do_stale) then do_stale = 1
n_interval = n_elements(delt_arr)

;wave_arr = [ 171,  193,  211, 335,  94, 131, 304,4500,1700,1600]
wave_arr = [171,  193,  211, 335,  94, 131, 304,4500,1700,1600]
nsamp_arr = [2,2,2,2,20,10,2,1,1,1]
;nsamp_arr = [2,2,2,2,2,2,2,2,2,2]
n_waves = n_elements(wave_arr)

if not exist(t_now) then t_now = anytim(ut_time(!stime), /ccsds)
t_lag = anytim(anytim(t_now) - delt_lag, /ccsds)
t_lag_sec = anytim(t_lag)
t_mod = anytim(t_lag, /ex)
t_mod[2:3] = 0
t_mod[1] = (t_mod[1] / 15)*15
print, ' t_mod = ' + anytim(t_mod, /ccsds)

if keyword_set(use_test_set) then fset = [ $
  '/cache/sdo/AIA/lev1/2010/04/20/H0800/AIA20100420_085948_0171.fits', $
  '/cache/sdo/AIA/lev1/2010/04/20/H0800/AIA20100420_085958_0193.fits', $
  '/cache/sdo/AIA/lev1/2010/04/20/H0800/AIA20100420_085949_0211.fits', $
  '/cache/sdo/AIA/lev1/2010/04/20/H0800/AIA20100420_085945_0335.fits', $
  '/cache/sdo/AIA/lev1/2010/04/20/H0800/AIA20100420_085959_0094.fits', $
  '/cache/sdo/AIA/lev1/2010/04/20/H0800/AIA20100420_085953_0131.fits', $
  '/cache/sdo/AIA/lev1/2010/04/20/H0800/AIA20100420_085951_0304.fits' ,$
  '/cache/sdo/AIA/lev1/2010/04/20/H0800/AIA20100420_085911_4500.fits', $
  '/cache/sdo/AIA/lev1/2010/04/20/H0800/AIA20100420_085942_1700.fits', $
  '/cache/sdo/AIA/lev1/2010/04/20/H0800/AIA20100420_085956_1600.fits' ]

if not exist(fset) then begin

  fset = strarr(n_waves+2)

; Find latest los magnetogram files:

;    print, 'Searching last ' + strtrim(abs(delt_arr[0]),2) + ' hours'
;    sdo_cat, anytim(anytim(t_now) - delt_lag + delt_arr[0]*3600d0, /yoh), $
;             anytim(anytim(t_now) - delt_lag, /yoh), cat, files, $
;             search=['img_type=light','wavelnth='+strtrim(wave_arr[i],2)], count=count, tcount=tcount
;    if count eq 0 then $
;      print, 'Searching last ' + strtrim(abs(delt_arr[1]),2) + ' hours'
;      sdo_cat, anytim(anytim(t_now) - delt_lag + delt_arr[1]*3600d0, /yoh), $
;               anytim(anytim(t_now) - delt_lag, /yoh), cat, files, $
;               search=['img_type=light','wavelnth='+strtrim(wave_arr[i],2)], count=count, tcount=tcount
;      if count eq 0 then $
;        print, 'Searching last ' + strtrim(abs(delt_arr[2]),2) + ' hours'
;        sdo_cat, anytim(anytim(t_now) - delt_lag + delt_arr[2]*3600d0, /yoh), $
;                 anytim(anytim(t_now) - delt_lag, /yoh), cat, files, $
;                 search=['img_type=light','wavelnth='+strtrim(wave_arr[i],2)], count=count, tcount=tcount
;        if count eq 0 then $
;          print, 'Searching last ' + strtrim(abs(delt_arr[3]),2) + ' hours'
;          sdo_cat, anytim(anytim(t_now) - delt_lag + delt_arr[3]*3600d0, /yoh), $
;                   anytim(anytim(t_now) - delt_lag, /yoh), cat, files, $
;                   search=['img_type=light','wavelnth='+strtrim(wave_arr[i],2)], count=count, tcount=tcount

;  parent = '/cache/sdo/HMI/blos'
  parent = '/cache/sdo/AIA/lev1p5'
  t_mag0 = anytim(anytim(t_now) - delt_lag + delt_arr[5]*3600d0, /yoh)
  t_mag1 = anytim(anytim(t_now) - delt_lag, /yoh)
  parent_mag0 = '/cache/sdo/AIA/lev1p5' + '/' + $
    string( (anytim(t_mag0,/ex))[6],format='(i4.4)') + '/' + $
    string( (anytim(t_mag0,/ex))[5],format='(i2.2)') + '/' + $
    string( (anytim(t_mag0,/ex))[4],format='(i2.2)')
  parent_mag1 = '/cache/sdo/AIA/lev1p5' + '/' + $
    string( (anytim(t_mag1,/ex))[6],format='(i4.4)') + '/' + $
    string( (anytim(t_mag1,/ex))[5],format='(i2.2)') + '/' + $
    string( (anytim(t_mag1,/ex))[4],format='(i2.2)')
  hr_mag0 = 'H' + string( (anytim(t_mag0,/ex))[0],format='(i2.2)') + '00'
  hr_mag1 = 'H' + string( (anytim(t_mag1,/ex))[0],format='(i2.2)') + '00'
;  files_los = ssw_time2filelist(t_mag0, t_mag1, parent=parent, count=count)
;;  files_los0 = file_search(parent_mag0 + '/' + hr_mag0, '*_blos*')
;;  files_los1 = file_search(parent_mag1 + '/' + hr_mag1, '*_blos*')
;  files_los0 = file_search(parent_mag0, '*_blos*')
;  files_los1 = file_search(parent_mag1, '*_blos*')
;  files_los = [files_los0, files_los1]
;  if files_los0[0] eq '' then files_los = files_los1
;
;  if files_los[0] ne '' then begin
;    nfil_los = n_elements(files_los)
;    s_len = strlen(files_los)
;    ss_match = where(strmid(files_los, s_len[0]-10, 5) eq '_blos', n_match)
;    if n_match gt 0 then begin
;      files_los = files_los[ss_match]
;      t_mag = file2time(files_los)
;      t_mag_sec = anytim(t_mag)
;      ss_close = tim2dset(anytim(t_mag,/int), anytim(t_mod,/int))
;t_mag_close = t_mag[ss_close]
;      fset[n_waves] = files_los[ss_close]
;      print, ' Found mag file: '
;      print, fset[n_waves]
;    endif else begin
;      print, ' Files found, but no mag file found that is close enough in time.'
;      stop
;    endelse
;  endif else begin
;    print, ' No files found at all.'
;    stop
;  endelse

;fset[n_waves] = last_file(wave='blos')
if file_exist(dir_scratch) eq 1 then spawn, 'rm -rf ' + dir_scratch
spawn, 'mkdir ' + dir_scratch
ssw_jsoc_time2data, dum1, dum2, lastn=2, /minutes, iindex_hmi, idata_hmi, max_files=1, ds='hmi.M_45s_nrt', $
  /jsoc2, outdir_top=dir_scratch
files_found_mag = file_list(dir_scratch, '*.fits')
if files_found_mag[0] ne '' then begin
  filnam_mag_orig = files_found_mag[0]
  pos_hmi = strpos(files_found_mag, 'HMI')
  filnam_mag_final = str_replace(filnam_mag_orig, 'HMI', 'AIA')
  filnam_mag_final = str_replace(filnam_mag_final, '6173', 'blos')
  spawn, 'mv ' + filnam_mag_orig + ' ' + filnam_mag_final
;  iindex_hmi = add_tag(iindex_hmi,iindex_hmi.crota2,'crota1')
  iindex_hmi = add_tag(iindex_hmi,iindex_hmi.crota2,'crota1')
;  aia_prep, filnam_mag_final, -1, /do_write_fits 
  aia_prep, iindex_hmi, idata_hmi, /do_write_fits, outfile=filnam_mag_final
  fset[n_waves] = filnam_mag_final
  print, ' Found freaking mag file: '
  print, fset[n_waves]
endif

; Find closest 171 to above magnetogram:

  if keyword_set(use_sdo_cat) then begin

      print, 'Searching last ' + strtrim(abs(delt_arr[0]),2) + ' hours'
      sdo_cat, anytim(anytim(t_now) - delt_lag + delt_arr[0]*3600d0, /yoh), $
               anytim(anytim(t_now) - delt_lag, /yoh), cat, files_sdo, level=level, $
               search=['img_type=light','wavelnth=171'], count=count, tcount=tcount
      if count eq 0 then $
        print, 'Searching last ' + strtrim(abs(delt_arr[1]),2) + ' hours'
        sdo_cat, anytim(anytim(t_now) - delt_lag + delt_arr[1]*3600d0, /yoh), $
                 anytim(anytim(t_now) - delt_lag, /yoh), cat, files_sdo, level=level, $
                 search=['img_type=light','wavelnth=171'], count=count, tcount=tcount
        if count eq 0 then $
          print, 'Searching last ' + strtrim(abs(delt_arr[2]),2) + ' hours'
          sdo_cat, anytim(anytim(t_now) - delt_lag + delt_arr[2]*3600d0, /yoh), $
                   anytim(anytim(t_now) - delt_lag, /yoh), cat, files_sdo, level=level, $
                   search=['img_type=light','wavelnth=171'], count=count, tcount=tcount
          if count eq 0 then $
            print, 'Searching last ' + strtrim(abs(delt_arr[3]),2) + ' hours'
            sdo_cat, anytim(anytim(t_now) - delt_lag + delt_arr[3]*3600d0, /yoh), $
                     anytim(anytim(t_now) - delt_lag, /yoh), cat, files_sdo, level=level, $
                     search=['img_type=light','wavelnth=171'], count=count, tcount=tcount

      if count eq 0 then return

      ss_close = tim2dset(anytim(cat.date_obs,/int), anytim(t_mag_close,/int))

      fset[n_waves+1] = files_sdo[ss_close]

  endif else begin
;     sdo_cat, reltime(hours=(-delt_lag/3600. + delt_arr[0])), reltime(hours=(-delt_lag/3600.)), cat, files, $

      print, 'Searching last ' + strtrim(abs(delt_arr[0]),2) + ' hours'
      files_sdo = sdo_time2files(anytim(anytim(t_now) + delt_arr[0]*3600d0, /yoh), $
               	  		 anytim(anytim(t_now), /yoh), $
               			 level=1.5, waves=strtrim(wave_arr[0],2), /aia)
      if files_sdo[0] eq '' then begin
        print, 'Searching last ' + strtrim(abs(delt_arr[1]),2) + ' hours'
        files_sdo = sdo_time2files(anytim(anytim(t_now) + delt_arr[1]*3600d0, /yoh), $
               	  		   anytim(anytim(t_now), /yoh), $
               			   level=1.5, waves=strtrim(wave_arr[0],2), /aia)
      endif
      if files_sdo[0] eq '' then begin
        print, 'Searching last ' + strtrim(abs(delt_arr[2]),2) + ' hours'
        files_sdo = sdo_time2files(anytim(anytim(t_now) + delt_arr[2]*3600d0, /yoh), $
               	  		   anytim(anytim(t_now), /yoh), $
               			   level=1.5, waves=strtrim(wave_arr[0],2), /aia)
      endif
      if files_sdo[0] eq '' then begin
        print, 'Searching last ' + strtrim(abs(delt_arr[3]),2) + ' hours'
        files_sdo = sdo_time2files(anytim(anytim(t_now) + delt_arr[3]*3600d0, /yoh), $
               	  		   anytim(anytim(t_now), /yoh), $
               			   level=1.5, waves=strtrim(wave_arr[0],2), /aia)
      endif
      if files_sdo[0] eq '' then begin
        print, 'Searching last ' + strtrim(abs(delt_arr[4]),2) + ' hours'
        files_sdo = sdo_time2files(anytim(anytim(t_now) + delt_arr[4]*3600d0, /yoh), $
               	  		   anytim(anytim(t_now), /yoh), $
               			   level=1.5, waves=strtrim(wave_arr[0],2), /aia)
      endif
      if files_sdo[0] eq '' then begin
        print, 'Searching last ' + strtrim(abs(delt_arr[5]),2) + ' hours'
        files_sdo = sdo_time2files(anytim(anytim(t_now) + delt_arr[5]*3600d0, /yoh), $
               	  		   anytim(anytim(t_now), /yoh), $
               			   level=1.5, waves=strtrim(wave_arr[0],2), /aia)
      endif

;      if files_sdo[0] ne '' then begin
;        read_sdo, files_sdo, index_sdo, /uncomp_delete, /mixed, /nodata
;        ss_good = where(index_sdo.img_type eq 'LIGHT', n_good)
;        if n_good gt 0 then files_sdo = files_sdo[ss_good] else files_sdo = ''
;      endif

      if files_sdo[0] eq '' then return

      n_files_sdo = n_elements(files_sdo)
      t_files = anytim(file2time(files_sdo), /int)
      ss_close = tim2dset(t_files, anytim(t_mod,/int))

;      delt_mag_171 = anytim(t_mag_close) - anytim(t_files[ss_close])
;      if abs(delt_mag_171) gt 1800 then 

      fset[n_waves+1] = files_sdo[ss_close]

  endelse

; Find all waves (including another 171):
  if not keyword_set(mag_only) then begin

  for i=0, n_waves-1 do begin

    if keyword_set(use_sdo_cat) then begin
;     sdo_cat, reltime(hours=(-delt_lag/3600. + delt_arr[0])), reltime(hours=(-delt_lag/3600.)), cat, files, $

      print, 'Searching last ' + strtrim(abs(delt_arr[0]),2) + ' hours'
      sdo_cat, anytim(anytim(t_now) - delt_lag + delt_arr[0]*3600d0, /yoh), $
               anytim(anytim(t_now) - delt_lag, /yoh), cat, files_sdo, level=level, $
               search=['img_type=light','wavelnth='+strtrim(wave_arr[i],2)], count=count, tcount=tcount
      if count eq 0 then $
        print, 'Searching last ' + strtrim(abs(delt_arr[1]),2) + ' hours'
        sdo_cat, anytim(anytim(t_now) - delt_lag + delt_arr[1]*3600d0, /yoh), $
                 anytim(anytim(t_now) - delt_lag, /yoh), cat, files_sdo, level=level, $
                 search=['img_type=light','wavelnth='+strtrim(wave_arr[i],2)], count=count, tcount=tcount
        if count eq 0 then $
          print, 'Searching last ' + strtrim(abs(delt_arr[2]),2) + ' hours'
          sdo_cat, anytim(anytim(t_now) - delt_lag + delt_arr[2]*3600d0, /yoh), $
                   anytim(anytim(t_now) - delt_lag, /yoh), cat, files_sdo, level=level, $
                   search=['img_type=light','wavelnth='+strtrim(wave_arr[i],2)], count=count, tcount=tcount
          if count eq 0 then $
            print, 'Searching last ' + strtrim(abs(delt_arr[3]),2) + ' hours'
            sdo_cat, anytim(anytim(t_now) - delt_lag + delt_arr[3]*3600d0, /yoh), $
                     anytim(anytim(t_now) - delt_lag, /yoh), cat, files_sdo, level=level, $
                     search=['img_type=light','wavelnth='+strtrim(wave_arr[i],2)], count=count, tcount=tcount

      if count eq 0 then return

      ss_close = tim2dset(anytim(cat.date_obs,/int), anytim(t_mod,/int))

    endif else begin
;     sdo_cat, reltime(hours=(-delt_lag/3600. + delt_arr[0])), reltime(hours=(-delt_lag/3600.)), cat, files, $

      print, 'Searching last ' + strtrim(abs(delt_arr[0]),2) + ' hours'
      files_sdo = sdo_time2files(anytim(anytim(t_now) + delt_arr[0]*3600d0, /yoh), $
               	  		 anytim(anytim(t_now), /yoh), $
               			 level=1.5, waves=strtrim(wave_arr[i],2), /aia)
      if files_sdo[0] eq '' then begin
        print, 'Searching last ' + strtrim(abs(delt_arr[1]),2) + ' hours'
        files_sdo = sdo_time2files(anytim(anytim(t_now) + delt_arr[1]*3600d0, /yoh), $
               	  		   anytim(anytim(t_now), /yoh), $
               			   level=1.5, waves=strtrim(wave_arr[i],2), /aia)
      endif
      if files_sdo[0] eq '' then begin
        print, 'Searching last ' + strtrim(abs(delt_arr[2]),2) + ' hours'
        files_sdo = sdo_time2files(anytim(anytim(t_now) + delt_arr[2]*3600d0, /yoh), $
               	  		   anytim(anytim(t_now), /yoh), $
               			   level=1.5, waves=strtrim(wave_arr[i],2), /aia)
      endif
      if files_sdo[0] eq '' then begin
        print, 'Searching last ' + strtrim(abs(delt_arr[3]),2) + ' hours'
        files_sdo = sdo_time2files(anytim(anytim(t_now) + delt_arr[3]*3600d0, /yoh), $
               	  		   anytim(anytim(t_now), /yoh), $
               			   level=1.5, waves=strtrim(wave_arr[i],2), /aia)
      endif
      if files_sdo[0] eq '' then begin
        print, 'Searching last ' + strtrim(abs(delt_arr[4]),2) + ' hours'
        files_sdo = sdo_time2files(anytim(anytim(t_now) + delt_arr[4]*3600d0, /yoh), $
               	  		   anytim(anytim(t_now), /yoh), $
               			   level=1.5, waves=strtrim(wave_arr[i],2), /aia)
      endif
      if files_sdo[0] eq '' then begin
        print, 'Searching last ' + strtrim(abs(delt_arr[5]),2) + ' hours'
        files_sdo = sdo_time2files(anytim(anytim(t_now) + delt_arr[5]*3600d0, /yoh), $
               	  		   anytim(anytim(t_now), /yoh), $
               			   level=1.5, waves=strtrim(wave_arr[i],2), /aia)
      endif

;      if files_sdo[0] ne '' then begin
;        read_sdo, files_sdo, index_sdo, /uncomp_delete, /mixed, /nodata
;        ss_good = where(index_sdo.img_type eq 'LIGHT', n_good)
;        if n_good gt 0 then files_sdo = files_sdo[ss_good] else files_sdo = ''
;      endif

      if files_sdo[0] eq '' then return

      n_files_sdo = n_elements(files_sdo)
;      ss_close = n_files_sdo-2
      t_files = anytim(file2time(files_sdo), /int)
      ss_close = tim2dset(t_files, anytim(t_mod,/int))

files_samp = files_sdo[ss_close - reverse(indgen(nsamp_arr[i]))]
read_sdo, files_samp, index_samp, data_samp, /uncomp_delete, /mixed
if nsamp_arr[i] gt 1 then $
  imset0 = long(mean(data_samp, dimension=3) > 0) else $
  imset0 = long(data_samp)
;  imset0 = long(total(data_samp, 3) > 0) else $

if keyword_set(do_plot) then begin
  wdef,0,512
  tvscl, rebin(imset0,512,512,/samp)
endif

if not exist(imset) then begin
  indset = index_samp[0]
  imset = lonarr(4096,4096,n_waves)
;  imset = fltarr(4096,4096,n_waves)
endif else begin
  indset = concat_struct(indset, index_samp[0])
endelse
imset[0,0,i] = imset0

      fset[i] = files_sdo[ss_close]
      if keyword_set(verbose) then print, fset[i]

    endelse

  endfor

  endif

endif

if keyword_set(do_snap) then begin

; Create a directory for the files if it doesn't alrerady exist:
  dir_out = ssw_time2paths(t_mod, t_mod, /daily, parent=dir_top)
  if keyword_set(verbose) then $
    print, ' dir_out = ' + dir_out
  if keyword_set(do_mkdir) then begin
    mk_dir, dir_out
;    spawn, 'chmod -R 775 ' + dir_out
  endif

; Call AIA lightcurve routine:
;  aia_ndaylightcurve, nday=1.0, /do_write, dir_out=dir_out
;  go_aia_lightcurve, nday=2.0, /goesl, /goesh, /do_write, dir_out=dir_out

; Copy fset files to daily directory:
  dir_recent = concat_dir(dir_top, 'mostrecent')
  nfil_fset = n_elements(fset)

  spawn, 'rm ' + concat_dir(dir_out, '*.fits')
  spawn, 'rm ' + concat_dir(dir_recent, '*.fits')

  for i=0, nfil_fset-2 do begin
    s_len0 = strlen(fset[i])
    wave0 = strmid(fset[i],s_len0-9,4)
    spawn, 'cp -p ' + strtrim(fset[i],2) + ' ' + concat_dir(dir_out, 'f' + wave0 + '.fits')
  endfor

  break_file, fset[0:nfil_fset-2], disk_log, dir, filnam, ext
  for i=0, nfil_fset-2 do $
    spawn, 'cp -p ' + fset[i] + ' ' + concat_dir(dir_out, filnam[i] + ext[i])

; Now call aia_snapset:
  aia_snapset, fset=fset, imset=imset, indset=indset, do_write=do_write, outpath=dir_out, $
    do_despike=do_despike, offlimb=offlimb, do_display=do_display, do_time=do_time, $
    do_stale=do_stale, minterp=minterp

; Make sure directory and file permissions are right:
  spawn, 'chmod -R 775 ' + dir_out
  spawn, 'chmod -R 775 ' + dir_recent

; Call AIA lightcurve routine:
;  aia_ndaylightcurve, nday=1.0, /do_write, dir_out=dir_out
;  go_aia_lightcurve, nday=0.5, /goesl, /goesh, /do_write, dir_out=dir_out
  go_aia_lightcurve, nday=1.0, /goesl, /goesh, /do_write, dir_out=dir_out

endif

end

