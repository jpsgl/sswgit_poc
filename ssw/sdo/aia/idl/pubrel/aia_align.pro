; procedure to align a sequence of images
;
inpath='/net/star/Users/schryver/missions/SDO/AIA/thirdlight/b/Iacrg0304_'
outpath='/net/star/Users/schryver/missions/SDO/AIA/thirdlight_align/a/Iacrg0304_'

; NB: doing this for other wavelengths does not work: use channel-specific offsets!
inpath2='/net/star/Users/schryver/missions/SDO/AIA/thirdlight/d/Ibcrg0304_'
outpath2='/net/star/Users/schryver/missions/SDO/AIA/thirdlight_align/b/Ibcrg0304_'
inpath3='/net/star/Users/schryver/missions/SDO/AIA/thirdlight/m/Irg0171_'
outpath3='/net/star/Users/schryver/missions/SDO/AIA/thirdlight_align/d/Irg0171_'
inpath4='/net/star/Users/schryver/missions/SDO/AIA/thirdlight/l/Irg0131_'
outpath4='/net/star/Users/schryver/missions/SDO/AIA/thirdlight_align/c/Irg0131_'
inpath5='/net/star/Users/schryver/missions/SDO/AIA/thirdlight/n/Irg0193_'
outpath5='/net/star/Users/schryver/missions/SDO/AIA/thirdlight_align/e/Irg0193_'
inpath6='/net/star/Users/schryver/missions/SDO/AIA/thirdlight/s/Irg0304_'
outpath6='/net/star/Users/schryver/missions/SDO/AIA/thirdlight_align/f/Irg0304_'

; filelist
spawn,'ls -1 '+inpath+'*.jpg',files
spawn,'ls -1 '+inpath2+'*.jpg',files2
spawn,'ls -1 '+inpath3+'*.gif',files3
spawn,'ls -1 '+inpath4+'*.gif',files4
spawn,'ls -1 '+inpath5+'*.gif',files5
spawn,'ls -1 '+inpath6+'*.gif',files6

istart=0
read_jpeg,files(istart),im0
xoff=dblarr(n_elements(files))
yoff=dblarr(n_elements(files))

window,0

; on redo, skip this set:
;goto,redo1

!p.multi=[0,1,2]

for i=istart+1,n_elements(files)-istart-1 do begin
  read_jpeg,files(i),im1
  imp0=(reform(im0(0,*,*)))(200:500,200:500)
  hccv=[correlate((reform(im1(0,*,*)))(200-4:500-4,200:500),imp0),$
        correlate((reform(im1(0,*,*)))(200-2:500-2,200:500),imp0),$
        correlate((reform(im1(0,*,*)))(200-0:500-0,200:500),imp0),$
        correlate((reform(im1(0,*,*)))(200+2:500+2,200:500),imp0),$
        correlate((reform(im1(0,*,*)))(200+4:500+4,200:500),imp0)]
  vccv=[correlate((reform(im1(0,*,*)))(200:500,200-4:500-4),imp0),$
        correlate((reform(im1(0,*,*)))(200:500,200-2:500-2),imp0),$
        correlate((reform(im1(0,*,*)))(200:500,200-0:500-0),imp0),$
        correlate((reform(im1(0,*,*)))(200:500,200+2:500+2),imp0),$
        correlate((reform(im1(0,*,*)))(200:500,200+4:500+4),imp0)]
  hcc=poly_fit([-4,-2,0,2,4],hccv,2,/double)
  vcc=poly_fit([-4,-2,0,2,4],vccv,2,/double)
  xoff(i)=-hcc(1)/(2*hcc(2))
  yoff(i)=-vcc(1)/(2*vcc(2))
  plot,findgen(n_elements(files)),total(xoff,/cum),psym=10
  plot,findgen(n_elements(files)),total(yoff,/cum),psym=10
  wait,0.1
  im0=im1
endfor
!p.multi=0

cxoff=total(xoff,/cum) & cxoff1=cxoff
cyoff=total(yoff,/cum) & cyoff1=cyoff
; do not allow for drift in y
cyoff=cyoff-(findgen(n_elements(cyoff))*(cyoff(n_elements(cyoff)-1)-cyoff(1))/n_elements(cyoff))

; interpolate the images
read_jpeg,files(0),im0
write_jpeg,outpath+counterstring(0,4)+'.jpg',im0,true=1
nx=(size(im0))(2)
ny=(size(im0))(3)

;window,0,xs=1280,ys=720
for i=1,n_elements(files)-1 do begin
  if (i/50)*50 eq i then print,'Step ',i,' of ',n_elements(files),' in set 1'
  read_jpeg,files(i),im0
  im1a=bilinear(reform(float(im0(0,*,*))),(findgen(nx)+cxoff(i))>0<(nx-1),$
                                          (findgen(ny)+cyoff(i))>0<(ny-1))
  im1b=bilinear(reform(float(im0(1,*,*))),(findgen(nx)+cxoff(i))>0<(nx-1),$
                                          (findgen(ny)+cyoff(i))>0<(ny-1))
  im1c=bilinear(reform(float(im0(2,*,*))),(findgen(nx)+cxoff(i))>0<(nx-1),$
                                          (findgen(ny)+cyoff(i))>0<(ny-1))
  im1=byte([[[im1a]],[[im1b]],[[im1c]]])
  write_jpeg,outpath+counterstring(i,4)+'.jpg',im1,true=3

endfor


redo1:
; N.B.: the initial alignment run used (200:500,200:500), but that
; introduced an artificially large drift in x. So reran with larger
; fov, which is a bit slower, but provides a much better result

istart=0
read_jpeg,files2(istart),im0
xoff=dblarr(n_elements(files2))
yoff=dblarr(n_elements(files2))
; do not allow for drift in y
cyoff=cyoff-(findgen(n_elements(cyoff))*(cyoff(n_elements(cyoff)-1)-cyoff(1))/n_elements(cyoff))

;window,0
!p.multi=[0,1,2]

for i=istart+1,n_elements(files2)-istart-1 do begin
  read_jpeg,files2(i),im1
  imp0=(reform(im0(0,*,*)))(100:600,100:600)
  hccv=[correlate((reform(im1(0,*,*)))(100-4:600-4,100:600),imp0),$
        correlate((reform(im1(0,*,*)))(100-2:600-2,100:600),imp0),$
        correlate((reform(im1(0,*,*)))(100-0:600-0,100:600),imp0),$
        correlate((reform(im1(0,*,*)))(100+2:600+2,100:600),imp0),$
        correlate((reform(im1(0,*,*)))(100+4:600+4,100:600),imp0)]
  vccv=[correlate((reform(im1(0,*,*)))(100:600,100-4:600-4),imp0),$
        correlate((reform(im1(0,*,*)))(100:600,100-2:600-2),imp0),$
        correlate((reform(im1(0,*,*)))(100:600,100-0:600-0),imp0),$
        correlate((reform(im1(0,*,*)))(100:600,100+2:600+2),imp0),$
        correlate((reform(im1(0,*,*)))(100:600,100+4:600+4),imp0)]
  hcc=poly_fit([-4,-2,0,2,4],hccv,2,/double)
  vcc=poly_fit([-4,-2,0,2,4],vccv,2,/double)
  xoff(i)=-hcc(1)/(2*hcc(2))
  yoff(i)=-vcc(1)/(2*vcc(2))
  plot,findgen(n_elements(files2)),total(xoff,/cum),psym=10
  plot,findgen(n_elements(files2)),total(yoff,/cum),psym=10
  wait,0.1
  im0=im1
endfor
!p.multi=0

cxoff=total(xoff,/cum) & cxoff2=cxoff
cyoff=total(yoff,/cum) & cyoff2=cyoff


;stop,'Test stop'

; interpolate the images
read_jpeg,files2(0),im0
write_jpeg,outpath2+counterstring(0,4)+'.jpg',im0,true=1
nx=(size(im0))(2)
ny=(size(im0))(3)

;window,0,xs=1280,ys=720
for i=1,n_elements(files2)-1 do begin
  if (i/50)*50 eq i then print,'Step ',i,' of ',n_elements(files2),' in set 2'
  read_jpeg,files2(i),im0
  im1a=bilinear(reform(float(im0(0,*,*))),(findgen(nx)+cxoff(i))>0<(nx-1),$
                                          (findgen(ny)+cyoff(i))>0<(ny-1))
  im1b=bilinear(reform(float(im0(1,*,*))),(findgen(nx)+cxoff(i))>0<(nx-1),$
                                          (findgen(ny)+cyoff(i))>0<(ny-1))
  im1c=bilinear(reform(float(im0(2,*,*))),(findgen(nx)+cxoff(i))>0<(nx-1),$
                                          (findgen(ny)+cyoff(i))>0<(ny-1))
  im1=byte([[[im1a]],[[im1b]],[[im1c]]])
  write_jpeg,outpath2+counterstring(i,4)+'.jpg',im1,true=3

;  read_gif,files3(i),im0,r,g,b
;  im1=byte(bilinear(float(im0),(findgen(nx)+cxoff(i))>0<(nx-1),$
;                                          (findgen(ny)+cyoff(i))>0<(ny-1)))
;  write_gif,outpath3+counterstring(i,4)+'.gif',im1,r,g,b

endfor

; on redo, skip rest
;goto,redo5

istart=0
read_gif,files3(istart),im0
xoff=dblarr(n_elements(files3))
yoff=dblarr(n_elements(files3))

;window,0
!p.multi=[0,1,2]

for i=istart+1,n_elements(files3)-istart-1 do begin
  read_gif,files3(i),im1
  imp0=(im0)(200:500,200:500)
  hccv=[correlate((im1)(200-4:500-4,200:500),imp0),$
        correlate((im1)(200-2:500-2,200:500),imp0),$
        correlate((im1)(200-0:500-0,200:500),imp0),$
        correlate((im1)(200+2:500+2,200:500),imp0),$
        correlate((im1)(200+4:500+4,200:500),imp0)]
  vccv=[correlate((im1)(200:500,200-4:500-4),imp0),$
        correlate((im1)(200:500,200-2:500-2),imp0),$
        correlate((im1)(200:500,200-0:500-0),imp0),$
        correlate((im1)(200:500,200+2:500+2),imp0),$
        correlate((im1)(200:500,200+4:500+4),imp0)]
  hcc=poly_fit([-4,-2,0,2,4],hccv,2,/double)
  vcc=poly_fit([-4,-2,0,2,4],vccv,2,/double)
  xoff(i)=-hcc(1)/(2*hcc(2))
  yoff(i)=-vcc(1)/(2*vcc(2))
  plot,findgen(n_elements(files3)),total(xoff,/cum),psym=10
  plot,findgen(n_elements(files3)),total(yoff,/cum),psym=10
  wait,0.1
  im0=im1
endfor
!p.multi=0

cxoff=total(xoff,/cum) & cxoff3=cxoff
cyoff=total(yoff,/cum) & cyoff3=cyoff
; do not allow for drift in y
cyoff=cyoff-(findgen(n_elements(cyoff))*(cyoff(n_elements(cyoff)-1)-cyoff(1))/n_elements(cyoff))

; interpolate the images
read_gif,files3(0),im0,r,g,b
write_gif,outpath3+counterstring(0,4)+'.gif',im0,r,g,b
nx=(size(im0))(1)
ny=(size(im0))(2)

;window,0,xs=1280,ys=720
for i=1,n_elements(files3)-1 do begin
  if (i/50)*50 eq i then print,'Step ',i,' of ',n_elements(files3),' in set 3'

  read_gif,files3(i),im0,r,g,b
  im1=byte(bilinear(float(im0),(findgen(nx)+cxoff(i))>0<(nx-1),$
                                          (findgen(ny)+cyoff(i))>0<(ny-1)))
  write_gif,outpath3+counterstring(i,4)+'.gif',im1,r,g,b

endfor



istart=0
read_gif,files4(istart),im0
xoff=dblarr(n_elements(files4))
yoff=dblarr(n_elements(files4))

;window,0
!p.multi=[0,1,2]

for i=istart+1,n_elements(files4)-istart-1 do begin
  read_gif,files4(i),im1
  imp0=(im0)(200:500,200:500)
  hccv=[correlate((im1)(200-4:500-4,200:500),imp0),$
        correlate((im1)(200-2:500-2,200:500),imp0),$
        correlate((im1)(200-0:500-0,200:500),imp0),$
        correlate((im1)(200+2:500+2,200:500),imp0),$
        correlate((im1)(200+4:500+4,200:500),imp0)]
  vccv=[correlate((im1)(200:500,200-4:500-4),imp0),$
        correlate((im1)(200:500,200-2:500-2),imp0),$
        correlate((im1)(200:500,200-0:500-0),imp0),$
        correlate((im1)(200:500,200+2:500+2),imp0),$
        correlate((im1)(200:500,200+4:500+4),imp0)]
  hcc=poly_fit([-4,-2,0,2,4],hccv,2,/double)
  vcc=poly_fit([-4,-2,0,2,4],vccv,2,/double)
  xoff(i)=-hcc(1)/(2*hcc(2))
  yoff(i)=-vcc(1)/(2*vcc(2))
  plot,findgen(n_elements(files3)),total(xoff,/cum),psym=10
  plot,findgen(n_elements(files3)),total(yoff,/cum),psym=10
  wait,0.1
  im0=im1
endfor
!p.multi=0

cxoff=total(xoff,/cum) & cxoff4=cxoff
cyoff=total(yoff,/cum) & cyoff4=cyoff
; do not allow for drift in y
cyoff=cyoff-(findgen(n_elements(cyoff))*(cyoff(n_elements(cyoff)-1)-cyoff(1))/n_elements(cyoff))

; interpolate the images
read_gif,files4(0),im0,r,g,b
write_gif,outpath4+counterstring(0,4)+'.gif',im0,r,g,b
nx=(size(im0))(1)
ny=(size(im0))(2)

;window,0,xs=1280,ys=720
for i=1,n_elements(files4)-1 do begin
  if (i/50)*50 eq i then print,'Step ',i,' of ',n_elements(files4),' in set 4'

  read_gif,files4(i),im0,r,g,b
  im1=byte(bilinear(float(im0),(findgen(nx)+cxoff(i))>0<(nx-1),$
                                          (findgen(ny)+cyoff(i))>0<(ny-1)))
  write_gif,outpath4+counterstring(i,4)+'.gif',im1,r,g,b

endfor


istart=0
read_gif,files5(istart),im0
xoff=dblarr(n_elements(files5))
yoff=dblarr(n_elements(files5))

;window,0
!p.multi=[0,1,2]

for i=istart+1,n_elements(files5)-istart-1 do begin
  read_gif,files5(i),im1
  imp0=(im0)(200:500,200:500)
  hccv=[correlate((im1)(200-4:500-4,200:500),imp0),$
        correlate((im1)(200-2:500-2,200:500),imp0),$
        correlate((im1)(200-0:500-0,200:500),imp0),$
        correlate((im1)(200+2:500+2,200:500),imp0),$
        correlate((im1)(200+4:500+4,200:500),imp0)]
  vccv=[correlate((im1)(200:500,200-4:500-4),imp0),$
        correlate((im1)(200:500,200-2:500-2),imp0),$
        correlate((im1)(200:500,200-0:500-0),imp0),$
        correlate((im1)(200:500,200+2:500+2),imp0),$
        correlate((im1)(200:500,200+4:500+4),imp0)]
  hcc=poly_fit([-4,-2,0,2,4],hccv,2,/double)
  vcc=poly_fit([-4,-2,0,2,4],vccv,2,/double)
  xoff(i)=-hcc(1)/(2*hcc(2))
  yoff(i)=-vcc(1)/(2*vcc(2))
  plot,findgen(n_elements(files5)),total(xoff,/cum),psym=10
  plot,findgen(n_elements(files5)),total(yoff,/cum),psym=10
  wait,0.1
  im0=im1
endfor
!p.multi=0

cxoff=total(xoff,/cum) & cxoff5=cxoff
cyoff=total(yoff,/cum) & cyoff5=cyoff
; do not allow for drift in y
cyoff=cyoff-(findgen(n_elements(cyoff))*(cyoff(n_elements(cyoff)-1)-cyoff(1))/n_elements(cyoff))

; interpolate the images
read_gif,files5(0),im0,r,g,b
write_gif,outpath5+counterstring(0,4)+'.gif',im0,r,g,b
nx=(size(im0))(1)
ny=(size(im0))(2)

;window,0,xs=1280,ys=720
for i=1,n_elements(files5)-1 do begin
  if (i/50)*50 eq i then print,'Step ',i,' of ',n_elements(files5),' in set 5'

  read_gif,files5(i),im0,r,g,b
  im1=byte(bilinear(float(im0),(findgen(nx)+cxoff(i))>0<(nx-1),$
                                          (findgen(ny)+cyoff(i))>0<(ny-1)))
  write_gif,outpath5+counterstring(i,4)+'.gif',im1,r,g,b

endfor



istart=0
read_gif,files6(istart),im0
xoff=dblarr(n_elements(files6))
yoff=dblarr(n_elements(files6))

;window,0
!p.multi=[0,1,2]

for i=istart+1,n_elements(files6)-istart-1 do begin
  read_gif,files6(i),im1
  imp0=(im0)(200:500,200:500)
  hccv=[correlate((im1)(200-4:500-4,200:500),imp0),$
        correlate((im1)(200-2:500-2,200:500),imp0),$
        correlate((im1)(200-0:500-0,200:500),imp0),$
        correlate((im1)(200+2:500+2,200:500),imp0),$
        correlate((im1)(200+4:500+4,200:500),imp0)]
  vccv=[correlate((im1)(200:500,200-4:500-4),imp0),$
        correlate((im1)(200:500,200-2:500-2),imp0),$
        correlate((im1)(200:500,200-0:500-0),imp0),$
        correlate((im1)(200:500,200+2:500+2),imp0),$
        correlate((im1)(200:500,200+4:500+4),imp0)]
  hcc=poly_fit([-4,-2,0,2,4],hccv,2,/double)
  vcc=poly_fit([-4,-2,0,2,4],vccv,2,/double)
  xoff(i)=-hcc(1)/(2*hcc(2))
  yoff(i)=-vcc(1)/(2*vcc(2))
  plot,findgen(n_elements(files6)),total(xoff,/cum),psym=10
  plot,findgen(n_elements(files6)),total(yoff,/cum),psym=10
  wait,0.1
  im0=im1
endfor
!p.multi=0

; fix a minor glitch:
fff=where(finite(xoff) eq 0)
if fff(0) ge 0 then xoff(fff)=0.
fff=where(finite(yoff) eq 0)
if fff(0) ge 0 then yoff(fff)=0.

cxoff=total(xoff,/cum) & cxoff6=cxoff
cyoff=total(yoff,/cum) & cyoff6=cyoff
; do not allow for drift in y
cyoff=cyoff-(findgen(n_elements(cyoff))*(cyoff(n_elements(cyoff)-1)-cyoff(1))/n_elements(cyoff))

; interpolate the images
read_gif,files6(0),im0,r,g,b
write_gif,outpath6+counterstring(0,4)+'.gif',im0,r,g,b
nx=(size(im0))(1)
ny=(size(im0))(2)

;window,0,xs=1280,ys=720
for i=1,n_elements(files6)-1 do begin
  if (i/50)*50 eq i then print,'Step ',i,' of ',n_elements(files6),' in set 6'

  read_gif,files6(i),im0,r,g,b
  im1=byte(bilinear(float(im0),(findgen(nx)+cxoff(i))>0<(nx-1),$
                                          (findgen(ny)+cyoff(i))>0<(ny-1)))
  write_gif,outpath6+counterstring(i,4)+'.gif',im1,r,g,b

endfor



redo5:

save,file='aia_align.sav'

;return
end
