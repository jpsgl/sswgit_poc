
pro go_mk_aia_lc_data, t0, t1, verbose=verbose

;IDL> times = strmid(anytim(timegrid('30-jun-2010','02-jan-2011',days=1),/ccsds),0,10)
;IDL> ssu = uniq(strmid(times,0,7))                                                   
;IDL> print,times[ssu]                                                                
;2010-06-30 2010-07-31 2010-08-31 2010-09-30 2010-10-31 2010-11-30 2010-12-31 2011-01-02
;IDL> prstr,times[ssu]                                                                
;2010-06-30
;2010-07-31
;2010-08-31
;2010-09-30
;2010-10-31
;2010-11-30
;2010-12-31
;2011-01-02
;IDL> prstr,times[ssu+1]

if not exist(filnam) then filnam = 'aia_light_curves'
if not exist(dir_top) then dir_top = '/viz2/media/SunInTime'
if not exist(dir_top_monthly) then dir_top_monthly = '/viz2/media/SunInTime/monthly_aia_lc_data'

if not exist(waves_euv) then waves_euv = [094,131,171,193,211,304,335]
if not exist(waves_uv) then waves_uv = [1600,1700]
if not exist(ss_euv) then ss_euv = 'aia.lev1_euv_12s'
if not exist(ss_uv) then ss_uv = 'aia.lev1_uv_24s'

if not exist(cadence) then cadence = '24s' ; '14400s' ; seconds

t_grid = anytim(timegrid(t0, t1, days=1), /ccsds)
dir_arr = ssw_time2paths(t_grid, parent=dir_top) 
dir_monthly = concat_dir(dir_top_monthly, strmid(ssw_time2paths(t_grid[0], parent='./'),2,4))

file_date = time2file(t_grid, /date)

file = concat_dir(dir_arr, filnam + '.txt')
file_dated = concat_dir(dir_arr, filnam + '_' + file_date + '.txt')
file_monthly_dated = concat_dir(dir_monthly, filnam + '_' + strmid(file_date[0],0,6) + '.txt')

n_grid_times = n_elements(t_grid)
;STOP
for i=0,n_grid_times-2 do begin

  t_check0 = anytim(!stime)

  ssw_jsoc_time2data, t_grid[i], t_grid[i+1], drms_euv, wave=waves_euv, $
    key='date__obs,wavelnth,datamean,exptime,quality,fsn', ds=ds_euv, jsoc2=jsoc2, cadence=cadence

  t_check1 = anytim(!stime)
  text = anytim(t_check1, /ccsds) + ':  EUV Read time = ' + string((t_check1-t_check0)/60d0,'$(f8.3)') + ' minutes.'
  file_append, './jsoc_euv_read_times.txt', text

  ssw_jsoc_time2data, t_grid[i], t_grid[i+1], drms_uv, wave=waves_uv,  $
    key='date__obs,wavelnth,datamean,exptime,quality,fsn', ds=ds_uv, jsoc2=jsoc2, cadence=cadence

  t_check2 = anytim(!stime)
  text = anytim(t_check2, /ccsds) + ':   UV Read time = ' + string((t_check2-t_check1)/60d0,'$(f8.3)') + ' minutes.'
  file_append, './jsoc__uv_read_times.txt', text

  if keyword_set(verbose) then begin
    text = 'Read time (EUV + UV) = ' + strtrim((t_check2-t_check0)/60d0,2) + ' minutes.'
    print, text
  endif

  drms = concat_struct(drms_euv, drms_uv)
  drms = drms[sort(drms.date_obs)]
  info = get_infox(drms, 'date_obs,wavelnth,datamean,quality', header=header)
  file_append, file[i], [header,info], /new
  if i eq 0 then file_append, file_monthly_dated, [header,info], /new else $
                 file_append, file_monthly_dated, [info]

endfor

end

