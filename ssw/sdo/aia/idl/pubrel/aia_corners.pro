; function to estimate read-out pedestal for an image by looking
; at areas in the vignetted corners, and applying that offset per
; quadrant.
;
; input: s image
; output(return) darr array to be subtracted
; Karel Schrijver     2010/04/12
;
function aia_corners,s,wavelnth=wavelnth,noshift=noshift
d=[total(s(300:499,300:499)),$
   total(s(3595:3794,3595:3794)),$
   total(s(300:499,3595:3794)),$
   total(s(3595:3794,300:499))]/(200.^2)
darr=fltarr(4096,4096)
darr(0:2047,0:2047)=d(0)
darr(2048:*,2048:*)=d(1)
darr(0:2047,2048:*)=d(2)
darr(2048:*,0:2047)=d(3)
darr=aia_centershift(darr,wavelnth=wavelnth,noshift=noshift)
return,darr
end
