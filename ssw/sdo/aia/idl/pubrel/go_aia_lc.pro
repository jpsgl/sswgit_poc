
pro go_aia_lc, dir_out=dir_out, t_ref=t_ref, _extra=_extra

if not exist(dir_parent) then dir_parent = '/viz2/media/SunInTime'

if (exist(t_ref)) then begin
   dir_daily = ssw_time2paths(anytim(t_ref, /ccsds), parent=dir_parent)
   if not exist(t0) then t0 = anytim(anytim(t_ref, /yoh, /date), /yoh)
   if not exist(t1) then t1 = anytim(anytim(t0) + 86400l, /yoh)
endif else begin
   dir_daily = ssw_time2paths(anytim(ut_time(), /ccsds), parent=dir_parent)
   if not exist(t1) then t1 = anytim(ut_time(!stime),     /yoh)
   if not exist(t0) then t0 = anytim(anytim(t1) - 86400l, /yoh)
endelse

if not exist(dir_out) then dir_out = dir_daily

; Call AIA lightcurve routine:
go_aia_lightcurve, t0=t0, t1=t1, nday=1.0, $
   /goesl, /goesh, goes_only=goes_only, /do_write, dir_out=dir_out

end
