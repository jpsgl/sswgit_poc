pro ssw_aia_time2lightcurve, time0, time1, drms, ds=ds, goes=goes, $
   waves=waves, _extra=_extra, quality=quality, colorlc=colorlc, $
   xsize=xsize, ysizepp=ysizepp, no_normalize=no_normalize, $
   no_quality_filter=no_quality_filter, silent=silent, quiet=quiet, $
   jsoc2=jsoc2, noreset=noreset, ngrid=ngrid,ymargin=ymargin, $
   charsize=charsize,thumbnail=thumbnail, cadence=cadence, $
   max_quality=max_quality, lo=lo, camera=camera
;+
;   Name: ssw_aia_time2lightcurves
;
;   Purpose: time range -> AIA light curves using drms/metadata;
;
;   Input Parameters:
;      time0, time1 - time range
;
;   Output Parameters:
;      drms - optional drms/index vector (output from ssw_jsoc_time2data.pro
;             less bad .QUALITY records)
;
;   Keyword Parameters:
;      goes (switch) - if set, include GOES XRS in stack
;      waves - comma delimited list of one or more AIA wavelnths
;      xsize - plot width in pixels (default=1024)
;      ysizepp - plot height PER Wave/GOES in pixels
;      _extra - any plot keywords (charsize, psym, symsize... etc)
;      ds - data series name - default = (deltaT time0:time1 dependent)
$                         <= 2 hours: aia_test.lev1p5
;                         otherwise : aia_test.synoptic2
;      jsoc2 (switch) - if set, use jsoc2 (if you know DS "unpublished" )
;      no_normalize (switch) - if set, do not normalize by drms.EXPTIME
;      no_quality=no_quality (switch) - if set, don't filter "bad" quality
;      silent/quiet (switch synonyms) - if set, inhibit some messages
;      noreset - (switch) if set, don't restore plot params (ex. annotate output plot)
;      cadence - inherit -> ssw_jsoc_time2data.pro, like cadence='1h'
;      max_quality - switch - if set, only samples which have most
;                    common value for QUALITY (e.g., max histogram)
;
;   Calling Example:
;      IDL> ssw_aia_time2lightcurve,reltime(days=-2),reltime(/now),$
;              drms,waves='94,171,304',/goes  ; last 24 hours
;
;   History:
;      20-oct-2010 - S.L.Freeland
;      12-jan-2011 - S.L.Freeland - add CADENCE & MAX_QUALITY
;
;  Method:
;-

if n_elements(waves) eq 0 then waves=171
silent=keyword_set(silent)
quiet=keyword_set(quiet)
loud=total([silent,quiet]) eq 0

if n_params() lt 2 then begin
    box_message,'require a starttime and stoptime.... bailing'
    return
endif 
dtr=ssw_deltat(time0,time1,/hours)
case 1 of 
   n_elements(ds) ne 0: ; User supplied data series
   dtr le 2: begin 
      box_message,'Using Series aia_test.lev1p5'
      ds='aia_test.lev1p5'
      jsoc2=1
   endcase
   else: ds='aia_test.synoptic2'
endcase

ssw_jsoc_time2data,time0,time1,drms,wave=waves, cadence=cadence, $
   key='date__obs,wavelnth,datamean,camera,exptime,img_type,quality,fsn',ds=ds,jsoc2=jsoc2
if get_logenv('check') ne '' then stop,'drms'

if not data_chk(drms,/struct) then begin 
   box_message,'Some problem accessing JSOC DRMS for series = '+ds
   return
endif
qfilt=1-keyword_set(no_quality_filter)
if qfilt then begin 
   aqual=all_vals(drms.quality)
   nq=n_elements(aqual)
   if keyword_set(max_quality) then begin ; poor mans histogram
      hqual=lonarr(nq)
      for q=0,nq-1 do begin
         sq=where(drms.quality eq aqual(q),qcnt)
         hqual(q)=qcnt
      endfor
      mq=(where(hqual eq max(hqual)))(0)
      ssmq=where(drms.quality eq aqual(mq))
      drms=drms(ssmq)
      aqaul=all_vals(drms.quality) 
   endif
   if n_elements(aqual) gt 1 then begin 
      ssok=where(drms.quality eq min(drms.quality),qfcnt)
      bcnt=n_elements(drms)-qfcnt
      drms=drms(ssok)
      if loud then box_message,'Filtering out ' + strtrim(bcnt,2) + $
         ' records for bad QUALITY'
   endif
endif

mwaves=strtrim(all_vals(drms.wavelnth),2)

mwaves=str2arr(waves)
goes=keyword_set(goes)
nstack=n_elements(mwaves) + goes
!p.multi=[0,1,nstack]
if n_elements(colorlc) eq 0 then colorlc=4

if n_elements(xsize) eq 0 then xsize=1024
if n_elements(ysizepp) eq 0 then ysizepp=150
wdef,xx,xsize,ysizepp*nstack, zbuffer=zbuffer,/ur
if !d.name eq 'X' then device,decompose=0 ; assure color table visibility
linecolors
xrng=[time0,time1]
dt1=ssw_deltat(time1,reltime(/now),/days)
if n_elements(charsize) eq 0 then charsize=1.3 ; should scale by YSIZE?
if goes then plot_goes,time0,time1,back=11,color=9,/goes14,$
   lo=lo, xrange=xrng , $
   ascii=(dt1 le 3.), three=(dt1 gt 3),/xstyle,_extra=_extra,gcolor=80, charsize=charsize
exptime=gt_tagval(drms,/exptime)
nonorm=keyword_set(no_normalize)
if nonorm then exptime=replicate(1.0,n_elements(drms))
if n_elements(ymargin) eq 0 then ymargin=1
g0=reltime(xrng(0),/hour)
g0=anytim(g0) - (anytim(g0) mod 3600)
cam=keyword_set(camera)
for i=0,nstack-1-goes-area do begin 
   ssw=where(drms.wavelnth eq mwaves(i),wcnt)
   utplot,drms(ssw).date_obs,drms(ssw).datamean/exptime(ssw), /ynozero,$
   ;   title='Wave: '+mwaves(i) + ' JSOC Data Series: ' + ds, $
   ;ytitle='Datamean' + (['/Exptime',''])(nonorm), $
      color=7,back=11,psym=-3, /xstyle, xrange=xrng, $
      _extra=_extra, xticklen=.01, charsize=charsize, nolabel=(i ne nstack-1), $
      ymargin=([ymargin,5])(i eq (nstack-goes)), tick_unit=tick_unit
   tgrid=timegrid(g0,xrng(1),sec=tick_unit)
;   evt_grid,tgrid,color=4
if get_logenv('checkleg') ne '' then stop,'mwaves'
   camn=strtrim(drms(ssw(0)).camera,2)
   wstring=strtrim(mwaves(i),2) + (['',' (Camera '+camn+')'])(cam) 
   if i eq 0 then legend,['Plot Timerange: ' + arr2str(xrng,' -to -'), 'Data Series: ' + ds(0)] ,/top, $
      /clear,back=11,charsize=charsize+.1,textcolor=4,box=0
   if keyword_set(thumbnail) then legend,strtrim(mwaves(i),2),/left,/clear,back=11,charsize=.6,box=0,textcolor=4 else $
   legend,['Wave: '+wstring,'(Cts/Second)'],/left,/bottom, /clear,back=11, $
      charsize=charsize
endfor

if ~keyword_set(noreset) then restsys,/aplot,/init

end
