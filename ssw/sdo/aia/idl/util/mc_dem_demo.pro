pro mc_dem_demo, pros_only=pros_only, genx_only=genx_only, outdir=outdir
;
;+
;   Name: mc_dem_demo
;
;   Purpose: get *pro & *genx for Mark Cheung DEM demo/exercises
;
;   Calling Sequence:
;      IDL> mc_dem_demo ; gets stuff from Mark's master
;
;   History:
;      18-oct-2016 - S.L.Freeland
;
;   Side Effects:
;      if OUTDIR not defined makes a directoy, copies stuff there, and
;      leaves you in that directory... (def=$HOME/mc_dem_demo)
;-

if n_elements(outdir) eq 0 then outdir=concat_dir('$HOME','mc_dem_demo')
mk_dir,outdir
tempdir=curdir()

mctop='http://www.lmsal.com/~cheung/AIA/tutorial_dem/'

sock_list,mctop,mclist

pss=where(strpos(mclist,'.pro') ne -1, pcnt)
gss=where(strpos(mclist,'.genx') ne -1, gcnt)
 
pros=strextract(mclist[pss],'href="','"')
genxs=strextract(mclist[gss],'href="','"')
glist=''
case 1 of 
   pcnt gt 0 and keyword_set(pros_only): glist=pros
   gcnt gt 0 and  keyword_set(genx_only): glist=genxs
   else: glist=[pros,genxs] ; default=all *pro & *genx
endcase

if glist[0] eq '' then box_message,'?? no pros or genx files??' else begin
   cd,outdir
   sock_copy,mctop+glist,_extra=_extra
   box_message,'done; warning PWD='+ outdir
   ssw_path,curdir(),/prepend ; add these *.pro to path
endelse

return
end


