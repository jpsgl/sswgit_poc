function ssw_aia_data2suntoday, index, data, r,g,b, unspike=unspike, $
   outsize=outsize, load=load
;
;+
;   Name: ssw_aia_data2suntoday
;
;   Purpose: AIA "index,data" -> pr images using suntoday logic (memory-only operation)
;
;   Input Parameters:
;      "index,data" - AIA science data, assumed 4096^2
;
;   Output:
;      function returns scaled/graphics ready
;
;   Output Paramters:
;      r,g,b, - wave dependent color table
;
;   Keyword Parameters:
;      unspike - if set, apply despiker
;      oustsize - desired output size; def=4096
;
;   Calling Sequence:
;      IDL> stgraphic=ssw_aia_data2suntoday(index, data, R,G,B [,outsize=NX] [,/unspike])
;
;   History:
;      26-apr-2010 - S.L.Freeland - memory version of aia_snapset/suntoday ($SSW_AIA/idl/pubrel/....)
;
;-
;
limit = 10. ; max multiplier of off-disk signal
waves = [171,193,211,335, 94,131,304,4500,1600,1700]
limitss = [5,  5,   5,  3,  3,  3, 10,  1.,  1.,  1.]

swaves=strtrim(waves,2)

iwave=strtrim(gt_tagval(index,/wavelnth,missing=''),2)  
ssw=where(iwave eq waves,wcnt)

if wcnt eq 0 then begin
   box_message,'are you sure that you input AIA "index,data"'
   return,-1
endif

image=data
exptime=gt_tagval(index,/exptime)
wavelenth=waves(ssw(0))
time=index.date_obs

if keyword_set(unspike) then image=trace_unspike(image,sens=.3)>0
image=aia_centershift2(image,wavelnth=wavelenth,noshift=noshift,t0=time)
image=((image-aia_corners2(image,wavelnth=wavelenth,noshift=noshift,t0=time)))>0
image=aia_intscale(image,exptime=exptime,wavelnth=wavelenth,/bytescale)

aia_lct,r,g,b, load=load,wavelnth=waves(ssw(0))
if keyword_set(outsize) then image=congrid(image,outsize(0),outsize(0))

return, image
end



