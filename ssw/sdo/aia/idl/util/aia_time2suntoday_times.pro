function aia_time2suntoday_times, time, wave=wave, force_remote=force_remote
;
;+
;  Name: aia_time2suntoday
;
;  Purpose: return time tags for SunToday images
;
;  Input Parameters:
;     time - desired time
;
;  Output:
;     function returns time(s) of associated SunToday images
;
;   Keyword Parameters:
;      wave - optional single wave of interest
;      force_remote - use HTTP (even if local SunToday tree exists)
;
;   Calling Examples:
;      IDL> all=aia_time2suntoday_times
;
;  History;
;     23-oct-2010 - S.L.Freeland - initially for aia get data gui requests missing REF_TIME
;                   but I can think of a couple other uses, so broke it out.
;-


force_remote=keyword_set(force_remote)

fm=(['','remote'])(keyword_set(force_remote))

sttop='/archive/sdo/media/SunInTime/'
httop='http://sdowww.lmsal.com/sdomedia/SunInTime/'
daydir=ssw_time2paths(time,time,parent=sttop)
tfile='times.txt'+fm ; 
files=file_search(daydir,tfile)
if file_exist(files) then tdata=rd_Tfile(files(0)) else begin 
   box_message,'No local, trying web...'
   subtop=ssw_time2paths(time,time,parent=httop)+'/'
   sock_list,subtop+str_replace(tfile,fm,''),tdata
endelse
tdata=str2cols(tdata,':',/trim)
strtab2vect,tdata,waves,ftimes
nwaves=n_elements(waves)
retval=replicate({wave:'',date_obs:''},nwaves)
retval.wave=waves
retval.date_obs=anytim(file2time(ftimes),/ccsds,/trunc)
if keyword_set(wave) then begin 
   ss=where(strpos(retval.wave,strupcase(strtrim(wave,2))) ne -1,wcnt)
   if wcnt ne 1 then box_message,'cannot find wave' else $
      retval=retval(ss(0)).date_obs
endif
 
return,retval
end
