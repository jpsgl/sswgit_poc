; function to shift AIA images to common center:
;
; ; Same pzt offsets are in firstlight.pro and secondlight.pro (per e-Boerner 2010/04/12)
function aia_centershift2,image,wavelnth=wavelnth,noshift=noshift,t0=t0

; enable test whether xcen,ycen were properly set in lev1 data files
if keyword_set(noshift) then return,image

; allowed values of wavelnth:
wave=[1600,1700,4500,94,131,171,193,211,304,335]
select=where(nint(wavelnth) eq wave)
if select(0) eq -1 then begin
  print,'aia_lct: selected invalid wavelength/channel'
  return,-1
endif 

; during commissioning, the offsets change irregularly - for now work with these

xc4500=-999

if utc2sec(anytim2utc(t0)) ge utc2sec(anytim2utc('2010/03/30 00:00:00')) and $
   utc2sec(anytim2utc(t0)) le utc2sec(anytim2utc('2010/03/31 23:59:59')) then begin
; image centers were initially determined from firstlight and in lev1 headers, but
; with finer corrections for 2010/04/08 flare/eruption event:
xc4500=-2048.+2048.
xc1600=-2048.+2048.
xc1700=-2048.+2048.
xc0171=-2048.+2048.
yc4500=-2075.+2048.
yc1700=-2075.+2048.
yc1600=-2075.+2048.
yc0171=-2075.+2048.
xc0094=-2037.+2048.
xc0304=-2037.+2048.
yc0094=-2104.+2048.
yc0304=-2104.+2048.
xc0131=-2057.+2048.
xc0335=-2057.+2048.
yc0131=-2089.+2048.
yc0335=-2089.+2048.
xc0211=-2029.+2048.
xc0193=-2029.+2048.
yc0211=-2095.+2048.
yc0193=-2095.+2048.
endif

if utc2sec(anytim2utc(t0)) ge utc2sec(anytim2utc('2010/04/08 00:00:00')) and $
   utc2sec(anytim2utc(t0)) le utc2sec(anytim2utc('2010/04/09 00:00:00')) then begin
; image centers were initially determined from firstlight and in lev1 headers, but
; with finer corrections for 2010/04/08 flare/eruption event:
xc4500=0; 2048.
xc1600=0; 2048.
xc1700=0; 2048.
xc0171=0; 2048.
yc4500=0; 2075.
yc1600=0; 2075.
yc1700=0; 2075.
yc0171=0; 2075.
xc0094=+12; 2037.
xc0304=+12; 2037.
yc0094=-32; 2104.
yc0304=-32; 2104.
xc0131=-3; 2057.
xc0335=-3; 2057.
yc0131=-13; 2089.
yc0335=-13; 2089.
xc0211=+18; 2029.
xc0193=+12; 2029.
yc0211=-23; 2095.
yc0193=-23; 2095.
endif

if utc2sec(anytim2utc(t0)) ge utc2sec(anytim2utc('2010/04/18 00:00:00')) and $
   utc2sec(anytim2utc(t0)) le utc2sec(anytim2utc('2010/04/19 00:00:00')) then begin
; image centers were initially determined from firstlight and in lev1 headers, but
; with finer corrections for 2010/04/18 limb-loop dynamics (offsets
; now very close to zero overall):
xc4500=0; 2048.
xc1600=0; 2048.
xc1700=0; 2048.
xc0171=0; 2048.
yc4500=0; 2075.
yc1600=0; 2075.
yc1700=0; 2075.
yc0171=0; 2075.
xc0094=+0; 2037.
xc0304=+0; 2037.
yc0094=+4; 2104.
yc0304=+4; 2104.
xc0131=+1; 2057.
xc0335=+6; 2057.
yc0131=+7; 2089.
yc0335=+4; 2089.
xc0211=+3; 2029.
xc0193=+2; 2029.
yc0211=-1; 2095.
yc0193=-4; 2095.
endif

; if near alignment, just return input
if xc4500 eq -999 then return,image

case select of
0: xc=xc1600
1: xc=xc1700
2: xc=xc4500
3: xc=xc0094
4: xc=xc0131
5: xc=xc0171
6: xc=xc0193
7: xc=xc0211
8: xc=xc0304
9: xc=xc0335
end
case select of
0: yc=yc1600
1: yc=yc1700
2: yc=yc4500
3: yc=yc0094
4: yc=yc0131
5: yc=yc0171
6: yc=yc0193
7: yc=yc0211
8: yc=yc0304
9: yc=yc0335
end

;print,wavelnth,xc,yc
fact=data_chk(image,/nx)/4096.

return,shift(image,xc*fact,yc*fact)
end
