; function to estimate read-out pedestal for an image by looking
; at areas in the vignetted corners, and applying that offset per
; quadrant.
;
; input: s image
; output(return) darr array to be subtracted
; Karel Schrijver     2010/04/12
; SLF. generalize for application to rebinned AIA
;
function aia_corners2,s,wavelnth=wavelnth,noshift=noshift, t0=t0

validflats='171,304'
if strpos(validflats,strtrim(wavelnth,2)) ne -1 and ssw_deltat(t0,ref='29-may-2010') gt 0 then begin 
   box_message,'flat field OK, returning zero'
   return,0
endif else box_message,'??

; corner def w/scaling for < 4096
nx=data_chk(s,/nx)
fact=nx/4096. ; reduction factor
x0=300 ; corner start in full res
dx=199 ; corner size in full res

outer=(nx-(x0*fact)-dx-2)
d=[total(s(x0*fact:(x0*fact)+(dx*fact), x0*fact:(x0*fact)+(dx*fact))), $
    total(s(outer:outer+(dx*fact), outer:outer+(dx*fact))), $
    total(s(x0*fact:(x0*fact)+(dx*fact),outer:outer+(dx*fact))), $
    total(s(outer:outer+(dx*fact),x0*fact:x0*fact+(dx*fact)))]/(200^2.)

darr=fltarr(nx,nx)
darr(0:2047*fact,0:2047*fact)=d(0)
darr(2048*fact:*,2048*fact:*)=d(1)
darr(0:2047*fact,2048*fact:*)=d(2)
darr(2048*fact:*,0:2047*fact)=d(3)
darr=aia_centershift2(darr,wavelnth=wavelnth,noshift=noshift, t0=t0)
return,darr
end
