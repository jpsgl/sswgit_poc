
pro aia_write_fits, oindex, odata, outdir=outdir, outfile=outfile

;+
; Purpose: Obvious
; History:
;   ~2012 - Break out FITS write from main program 
;    2013-08-01 - Define necessary arrays from main program
;-

progver = 'V2.0   ; 2013-08-01 (GLS)

hmi_content_value = $
  ['dopplergram', 'magnetogram', 'level 1p image', 'linewidth', 'linedepth', 'continuum intensity']
hmi_outfil_suffix = $
  ['dop','mag','img','wid','dep','cont']

if not exist(outfile) then begin
  if not exist(outdir) then outdir = './'

  instr_prefix = strupcase(strmid(oindex.instrume,0,3))

  if instr_prefix eq 'HMI' then begin
    ss_match = where(hmi_content_value eq strlowcase(oindex.content), n_match)
    if n_match gt 0 then $
      outfil_suffix = hmi_outfil_suffix[ss_match[0]] else $
      outfil_suffix = string(wave_val, format='$(i4.4)')
  endif

  if instr_prefix eq 'AIA' then begin
    outfil_suffix = string(oindex.wavelnth, format='$(i4.4)')
  endif
  outfil = instr_prefix + time2file(oindex.date_obs, /sec) + '_' + outfil_suffix + '.fits'
  outfil = concat_dir(outdir, outfil)
endif else begin
  if not exist(outdir) then outfil = outfile else $
    outfil = concat_dir(outdir, outfile)
endelse

n_img = n_elements(oindex)
for i=0,n_img-1 do mwritefits, oindex[i], odata[*,*,i], outfile=outfil[i]

end
