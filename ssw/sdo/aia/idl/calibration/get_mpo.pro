
function get_mpo, t0, t1, ds=ds, threeh=threeh, weekly=weekly

;+
;   Name: ssw_sdo_master_pointing
;
;   Purpose: Get SDO master pointing records between t0 and t1
;
;   Keyword Parameters:
;      ds - data series to use - default is three hour cadence series
;
;   Method:
;      Use ssw_jsoc_time2data
;
;   History:
;      2017-06-17 - GLS
;      2018-02-11 - Ryan Timmons, changing 3h seris to
;                   aia.master_pointing3h based on JPS advice
;-

if not exist(t0) then return, -1
if not exist(t1) then return, -1

if not exist(ds) then ds = 'aia.master_pointing3h'
if keyword_set(threeh) then ds = 'aia.master_pointing3h'
if keyword_set(weekly) then ds = 'sdo.master_pointing'

ssw_jsoc_time2data, t0, t1, mpo_str, ds=ds

return, mpo_str
end
