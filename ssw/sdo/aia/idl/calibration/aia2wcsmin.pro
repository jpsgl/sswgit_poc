
function aia2wcsmin, index, cutout=cutout, use_hdr_pnt=use_hdr_pnt, rec_num=rec_num, t_ref=t_ref, $
  mpo_str=mpo_str, use_indmpo=use_indmpo, use_sswmp=use_sswmp, verbose=verbose, _extra=_extra

;+
; HISTORY:
;   2013-07-22 - GLS, Alessandro Cilla - Corrected bug associated with print statements in case
;                                        of /VERBOSE set.
;   2014-10-27 - GLS, Paul Higgins - Corrected bug associated with non finite SAT_ROT tag
;-

common ssw_sdo_master_pointing_blk, mpall, mplatest

; Define prognam, progver variables
prognam = 'AIA2WCSMIN.PRO'
progver = 'V1.00' ; 2013-05-28 (GLS)
progver = 'V5.10' ; 2013-07-22 (GLS, Alessandro Cilla)

; Define constants:
wave_val_arr = [ 0094,   0131,   0171,   0193,   0211,   0304,   0335,   1600,   1700,   4500,   6173 ]
wave_str_arr = [ '094',  '131',  '171',  '193',  '211',  '304',  '335', '1600', '1700', '4500', '6173']
hmi_cam_st_arr = ['cam1', 'cam2']

if not exist(crpix_diff_max) then crpix_diff_max = 0.1

wcs = index

instr_prefix = strupcase(strmid(index.instrume,0,3))

; Optionally use master pointing file for pointing parameters:
if not keyword_set(use_hdr_pnt) then begin

  found_orig = 0

; Define SAT_ROT ( TOCHECK: Why is finite check on sat_rot necessary? ):
  if tag_exist(index, 'sat_rot') then begin
    if finite(index.sat_rot) then sat_rot = index.sat_rot else sat_rot = 0
  endif else begin
    sat_rot = 0
  endelse

; Determine wavelength and other properties:

  if instr_prefix eq 'AIA' then begin
    wavelnth = strtrim(fix(index.wavelnth),2)
    ss_match_wave = where(wavelnth eq wave_val_arr, n_match_wave)
    wave_val = wave_val_arr[ss_match_wave]
    wave_string = wave_str_arr[ss_match_wave]
    mpo_prefix = 'a_' + wave_string
  endif

  if instr_prefix eq 'HMI' then begin
;    wavelnth = '6173'
    mpo_prefix = 'h_cam' + strtrim(index.camera,2)
  endif

  if keyword_set(use_sswmp) then begin
    if keyword_set(verbose) then print, 'Calling ssw_sdo_master_pointing directly:'
    mpo_str_new = ssw_sdo_master_pointing(index.t_obs)
  endif else begin
    mpo_str_new = index2mpo(index, rec_num=rec_num, t_ref=t_ref, use_indmpo=use_indmpo, _extra=_extra)
  endelse

  if tag_exist( mpo_str_new, 'mpo_rec_num') then $
    mpo_rec_num_new = mpo_str_new.mpo_rec_num

; Extract from the preferred mpo_rec appropriate IMSCALE, X0, Y0, and INSTROT tags for wavelength:

  t_names_mpo_new = strlowcase(tag_names(mpo_str_new))
  ss_x0_new = where(strpos(t_names_mpo_new, mpo_prefix + '_x0') ne -1, n_match_x0_new)
  crpix1_new = mpo_str_new.(ss_x0_new) + 1
  ss_y0_new = where(strpos(t_names_mpo_new, mpo_prefix + '_y0') ne -1, n_match_x0_new)
  crpix2_new = mpo_str_new.(ss_y0_new) + 1
  ss_imscale_new = where(strpos(t_names_mpo_new, mpo_prefix + '_imscale') ne -1, n_match_imscale_new)
  cdelt1_new = mpo_str_new.(ss_imscale_new)
  cdelt2_new = mpo_str_new.(ss_imscale_new)
  ss_instrot_new = where(strpos(t_names_mpo_new, mpo_prefix + '_instrot') ne -1, n_match_instrot_new)

; Add SOT_ROT offset to CROTA2:
  crota2_new = mpo_str_new.(ss_instrot_new) + sat_rot

  if not keyword_set(cutout) then begin

    wcs.crpix1 = crpix1_new
    wcs.crpix2 = crpix2_new
    wcs.cdelt1 = cdelt1_new
    wcs.cdelt2 = cdelt2_new
    wcs.crota2 = crota2_new

; Update or add XCEN, YCEN tags:
    xcen = comp_fits_cen(wcs.crpix1, wcs.cdelt1, wcs.naxis1, wcs.crval1)
    if tag_exist(wcs, 'XCEN') then wcs.xcen = xcen else $
      wcs = add_tag(wcs, xcen, 'XCEN')
    ycen = comp_fits_cen(wcs.crpix2, wcs.cdelt2, wcs.naxis2, wcs.crval2)
    if tag_exist(wcs, 'YCEN') then wcs.ycen = ycen else $
      wcs = add_tag(wcs, ycen, 'YCEN')

  endif else begin
  
; Image is cutout (partial frame). Must adjust header values of crpix1/2, cdelt1/2, and crota2
;   by first finding the differences in the values of the parameters in the selected pointing
;   data source and the original full frame header values used in making the cutout, if these
;   can be determined.
    
    if tag_exist(index, 'mpo_rec') then begin

; Extract from the original mpo_rec the appropriate IMSCALE, X0, Y0, and INSTROT tags for wavelength:

      mpo_str_orig = index2mpo(index, rec_num=index.mpo_rec, _extra=_extra)
      t_names_mpo_orig = strlowcase(tag_names(mpo_str_orig))
      ss_x0_orig = where(strpos(t_names_mpo_orig, mpo_prefix + '_x0') ne -1, n_match_x0_orig)
      crpix1_orig = mpo_str_orig.(ss_x0_orig) + 1
      ss_y0_orig = where(strpos(t_names_mpo_orig, mpo_prefix + '_y0') ne -1, n_match_x0_orig)
      crpix2_orig = mpo_str_orig.(ss_y0_orig) + 1
      ss_imscale_orig = where(strpos(t_names_mpo_orig, mpo_prefix + '_imscale') ne -1, n_match_imscale_orig)
      cdelt1_orig = mpo_str_orig.(ss_imscale_orig)
      cdelt2_orig = mpo_str_orig.(ss_imscale_orig)
      ss_instrot_orig = where(strpos(t_names_mpo_orig, mpo_prefix + '_instrot') ne -1, n_match_instrot_orig)

; Add SOT_ROT offset to CROTA2:
      crota2_orig = mpo_str_orig.(ss_instrot_orig) + sat_rot

; Now adjust the wcs parameters for the cutout using the deltas or factors derived from the differences or
;   factors from the orig and new pointing parameters:

      wcs.crpix1 = wcs.crpix1 + (crpix1_new - crpix1_orig)
      wcs.crpix2 = wcs.crpix2 + (crpix2_new - crpix2_orig)
      wcs.cdelt1 = wcs.cdelt1 * (cdelt1_new / cdelt1_orig)
      wcs.cdelt2 = wcs.cdelt2 * (cdelt2_new / cdelt2_orig)
      wcs.crota2 = wcs.crota2 + (crota2_new - crota2_orig)

; Update or add XCEN, YCEN tags:
      xcen = comp_fits_cen(wcs.crpix1, wcs.cdelt1, wcs.naxis1, wcs.crval1)
      if tag_exist(wcs, 'XCEN') then wcs.xcen = xcen else $
        wcs = add_tag(wcs, xcen, 'XCEN')
      ycen = comp_fits_cen(wcs.crpix2, wcs.cdelt2, wcs.naxis2, wcs.crval2)
      if tag_exist(wcs, 'YCEN') then wcs.ycen = ycen else $
        wcs = add_tag(wcs, ycen, 'YCEN')

    endif else begin

      print, 'index structure does not include MPO_REC tag.'
      print, 'Looking for original pointing in history tag.'

      if tag_exist(index, 'history') then begin

        buff_hist = parse_hist(index.history)
        if size(buff_hist, /type) eq 8 then begin

          if ( tag_exist(buff_hist, 'crpix1_orig') and $
               tag_exist(buff_hist, 'crpix1_orig') ) then begin
;            crpix1_orig_ff = buff_hist.crpix1_orig
;            crpix2_orig_ff = buff_hist.crpix2_orig
            crpix1_orig = buff_hist.crpix1_orig
            crpix2_orig = buff_hist.crpix2_orig
;            crpix1_new_pf_from_crpix1_orig = wcs.crpix1 + (crpix1_new - crpix1_orig)
;            crpix2_new_pf_from_crpix2_orig = wcs.crpix2 + (crpix2_new - crpix2_orig)
            crpix1_from_crpix1_orig = wcs.crpix1 + (crpix1_new - crpix1_orig)
            crpix2_from_crpix2_orig = wcs.crpix2 + (crpix2_new - crpix2_orig)
            found_orig = 1
          endif

          if ( tag_exist(buff_hist, 'xll') and $
               tag_exist(buff_hist, 'xyy') ) then begin
;            crpix1_new_pf_from_xll = crpix1_new_ff - buff_hist.xll
;            crpix2_new_pf_from_yll = crpix2_new_ff - buff_hist.xyy
            crpix1_from_xll = crpix1_new - buff_hist.xll
            crpix2_from_yll = crpix2_new - buff_hist.xyy
            found_orig = 1
          endif

          if ( tag_exist(buff_hist, 'cdelt1_orig') and $
               tag_exist(buff_hist, 'cdelt2_orig') and $
               tag_exist(buff_hist, 'crota2_orig') ) then begin
            cdelt1_orig = buff_hist.cdelt1_orig
            cdelt2_orig = buff_hist.cdelt2_orig
            crota2_orig = buff_hist.crota2_orig
            found_orig = 1
          endif else begin
            found_orig = 0
          endelse

          if exist(crpix1_from_crpix1_orig) and exist(crpix1_from_xll) then begin
            if abs(crpix1_from_crpix1_orig - crpix1_from_xll) gt crpix_diff_max then begin
              print, 'Discrepant values for CRPIX1 derived from HISTORY tag values for
              print, 'CRPIX1_ORIG and XLL.  Therfore using header values for CRPIX1/2 keywords.'
              found_orig = 0
            endif
          endif

          if exist(crpix2_from_crpix2_orig) and exist(crpix2_from_yll) then begin
            if abs(crpix2_from_crpix2_orig - crpix2_from_yll) gt crpix_diff_max then begin
              print, 'Discrepant values for CRPIX2 derived from HISTORY tag values for
              print, 'CRPIX2_ORIG and YLL.  Therfore using header values for CRPIX1/2 keywords.'
              found_orig = 0
            endif
          endif

          if found_orig eq 1 then begin

            if exist(crpix1_from_crpix1_orig) then begin
              wcs.crpix1 = crpix1_from_crpix1_orig
              wcs.crpix2 = crpix2_from_crpix2_orig
            endif
            if exist(crpix1_from_xll) then begin
              wcs.crpix1 = crpix1_from_xll
              wcs.crpix2 = crpix2_from_yll
            endif

;            wcs.crpix1 = wcs.crpix1 + (crpix1_new - crpix1_orig)
;            wcs.crpix2 = wcs.crpix2 + (crpix2_new - crpix2_orig)
            wcs.cdelt1 = wcs.cdelt1 * (cdelt1_new / cdelt1_orig)
            wcs.cdelt2 = wcs.cdelt2 * (cdelt2_new / cdelt2_orig)
            wcs.crota2 = wcs.crota2 + (crota2_new - crota2_orig)

; Update or add XCEN, YCEN tags:
            xcen = comp_fits_cen(wcs.crpix1, wcs.cdelt1, wcs.naxis1, wcs.crval1)
            if tag_exist(wcs, 'XCEN') then wcs.xcen = xcen else $
              wcs = add_tag(wcs, xcen, 'XCEN')
            ycen = comp_fits_cen(wcs.crpix2, wcs.cdelt2, wcs.naxis2, wcs.crval2)
            if tag_exist(wcs, 'YCEN') then wcs.ycen = ycen else $
              wcs = add_tag(wcs, ycen, 'YCEN')

          endif else begin
            print, ' Problem in determining original full frame pointing information.'
            print, ' Using header pointing.'
          endelse
        
        endif else begin
          print, ' Cannot determine original full frame pointing information.'
          print, ' Using header pointing.'
        endelse

      endif else begin
        print, ' AIA2WCSMIN: No history record found in header.  Cannot update pointing.'
        print, ' Using header pointing.'
      endelse

    endelse
  endelse

endif else begin
  print, " AIA2WCSMIN: 'USE_HDR_PNT' flag set. Using header values for pointing keywords."
endelse

if keyword_set(verbose) then begin
  print, ''
  print, 't_obs:     ' + strtrim(index.t_obs,2)
  if tag_exist(index, 'mpo_rec') then print, 'mpo_rec    ' + strtrim(index.mpo_rec,2)
  print, ''
  if exist(mpo_str) then begin
    print, 'mpo start: ' + strtrim(mpo_str.t_start,2)
    print, 'mpo stop:  ' + strtrim(mpo_str.t_stop,2)
    print, 'mpo date:  ' + strtrim(mpo_str.date,2)
    print, 'mpo ver:   ' + strtrim(mpo_str.version,2)
  endif

  print, 'crpix1 (hdr): ' + strtrim(index.crpix1,2) + '  ' + $
         'crpix2 (hdr): ' + strtrim(index.crpix2,2)
  print, 'crpix1 (mpo): ' + strtrim(wcs.crpix1,2) + '  ' + $
         'crpix2 (mpo): ' + strtrim(wcs.crpix2,2)

  print, 'cdelt1 (hdr): ' + strtrim(index.cdelt1,2) + '  ' + $
         'cdelt2 (hdr): ' + strtrim(index.cdelt2,2)
  print, 'cdelt1 (mpo): ' + strtrim(wcs.cdelt1,2) + '  ' + $
         'cdelt2 (mpo): ' + strtrim(wcs.cdelt2,2)

  print, 'crota2 (hdr): ' + strtrim(index.crota2,2)
  print, 'crota2 (mpo): ' + strtrim(wcs.crota2,2)

  if exist(mpo_str) then print, 'inst_rot (mpo): ' + strtrim(mpo_str.(ss_instrot),2)

  print, 'sat_rot (hdr): ' + strtrim(sat_rot,2)
  print, ''
endif

; Update history record:

buff_history = ''
if keyword_set(use_hdr_pnt) then begin
  buff_history = [buff_history, 'USE_HDR_PNT: 1']
  if tag_exist(wcs, 'mpo_rec') then begin
    mpo_str = index2mpo(wcs, /use_indmpo)
    buff_history = [buff_history, 'MPO rec_date: ' + strtrim(mpo_str.date,2)]
    buff_history = [buff_history, 'MPO rec_t_start: ' + strtrim(mpo_str.t_start,2)]
    buff_history = [buff_history, 'MPO rec_t_stop: ' + strtrim(mpo_str.t_stop,2)]
    buff_history = [buff_history, 'MPO rec_version: ' + strtrim(mpo_str.version,2)]
  endif
endif else begin
  if exist(mpo_str_new) then begin
    if exist(mpo_rec_num_new) then $
      buff_history = [buff_history, 'MPO_recnum: ' + strtrim(mpo_rec_num_new,2)]
    buff_history = [buff_history, 'MPO_date: ' + strtrim(mpo_str_new.date,2)]
    buff_history = [buff_history, 'MPO_t_start: ' + strtrim(mpo_str_new.t_start,2)]
    buff_history = [buff_history, 'MPO_t_stop: ' + strtrim(mpo_str_new.t_stop,2)]
    buff_history = [buff_history, 'MPO_version: ' + strtrim(mpo_str_new.version,2)]
  endif
endelse

update_history, wcs, version=progver, caller=prognam
update_history, wcs, buff_history

return, wcs

end
