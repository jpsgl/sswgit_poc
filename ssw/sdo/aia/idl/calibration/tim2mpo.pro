;RPT edit 2-11-18, change default/three hour to aia.master_pointing3h
;based on JPS talk (although due to way written, this edit does
;nothing without changing get_mpo. pro aslo)
; corrected bad interpretation of what time range the mpo entries are
;good for.
;RPT another update 3-8-18 - do simple before/after averaging to
;                            handle the MISSING records.  Note this is a bit kludgish
                        

function tim2mpo, t, ds=ds, threeh=threeh, weekly=weekly

if not exist(ds) then ds = 'aia.master_pointing3h'
t_buff = 86400d0  
if keyword_set(threeh) then begin
   ds = 'aia.master_pointing3h'  ; question outstanding to jps about whether it should be aia.master_pointing3h
   t_buff = 86400d0
endif
if keyword_set(weekly) then begin
   ds = 'sdo.master_pointing'
   t_buff = 86400d0*8
endif

t_sec = anytim(t)
n_t = n_elements(t)
t0 = anytim(min(t_sec) - t_buff, /ccsds)
t1 = anytim(max(t_sec) + t_buff, /ccsds)

mpo_arr = get_mpo(t0, t1, ds=ds, threeh=threeh, weekly=weekly)
ts_mpo = mpo_arr.t_start
te_mpo = mpo_arr.t_stop
ts_mpo_sec = anytim(ts_mpo)
te_mpo_sec = anytim(te_mpo)

; Now we have all MPO records covering the input times.
; First code slow way to match times with nearest in time MPO record:
for i=0, n_t-1 do begin
   ;Flawed old one, but keep as a backup
   ts_mpo_match = min(abs(t_sec[i] - ts_mpo_sec), ss_match)
   ind_match = where(t_sec[i] ge ts_mpo_sec, count)
   if (count ne 0) then ss_match = ind_match(count-1)
   if not exist(ss_mpo) then ss_mpo = ss_match else ss_mpo = [ss_mpo, ss_match]
endfor

;RPT adding very simple interpolation code for cases with missing
;records
; just average one before/one after.  Sure *could* do weighting, other
;interp etc, but I'm mostly concerned with just getting cubes out the door.  
;  A_171_X0        FLOAT          -999999.
;   A_171_Y0        FLOAT          -999999.

;valtags = ['A_094_X0', 'A_094_Y0', 'A_131_X0', 'A_131_Y0']
valind_kw = [18,19,22,23,26,27,30,31,34,35,38,39,42,43,46,47,50,51]

indfix = uniq(ss_mpo)
for i=0, n_elements(indfix)-1 do begin
   fixind = ss_mpo(indfix(i))
   numkw = n_elements(valind_kw)
   for j=0, numkw-1 do begin
      kw_index = valind_kw(j)
      ;stop
      if mpo_arr(fixind).(kw_index) lt -10000. then begin
         print, 'found bad val for ' + string(fixind) + ' ' + string(kw_index)
         valbefore =  mpo_arr(fixind).(kw_index)
         valafter =  mpo_arr(fixind).(kw_index)
         beforeidx = fixind-1
         afteridx = fixind+1
         while valbefore lt -10000. and beforeidx ge 0 do begin
            valbefore = mpo_arr(beforeidx).(kw_index)
            beforeidx = beforeidx - 1
         endwhile

         print, 'found valbefore' +  string(valbefore)

          while valafter lt -10000. and afteridx lt n_elements(mpo_arr) do begin
            valafter = mpo_arr(afteridx).(kw_index)
            afteridx = afteridx + 1
         endwhile

          print, 'found valafter' +  string(valafter) + ' and will kludge with average of those:' + string(0.5 * valbefore + 0.5 * valafter)

          mpo_arr(fixind).(kw_index) = 0.5 * valbefore + 0.5 * valafter
          
          ;4-3-18 one more addition, for case where valid data found only in one direction (maybe only happened once with loadshed end on 2016-08-10)
          if (valbefore lt -10000) then begin
          	print, 'no good values before, use just the latter'
          	mpo_arr(fixind).(kw_index) = valafter
          endif
          	
          if (valafter lt -10000) then begin
          	 mpo_arr(fixind).(kw_index) = valbefore
          	print, 'no good values after, use just the before'
          endif
            
         ;valbefore = mpo_arr(fixind-1).(kw_index)
        ; valafter = mpo_arr(fixind+1).(kw_index) ;
         ;print, 'found adjancents: '  + string(valbefore) + ", " + string(valafter)
      endif
      
   endfor
   


endfor


mpo_match = mpo_arr[ss_mpo]

return, mpo_match
end
