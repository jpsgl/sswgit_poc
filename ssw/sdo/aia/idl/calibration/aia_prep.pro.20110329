
PRO aia_prep, input1, input2, oindex, odata, infil=infil, nearest=nearest, interp=interp, cubic=cubic, $
  use_pnt_file=use_pnt_file, not_use_ssw=not_use_ssw, no_uncomp_delete=no_uncomp_delete, $
  do_write_fits=do_write_fits, outdir=outdir, scale_fac=scale_fac, _extra=_extra, $
  qstop=qstop, quiet=quiet, verbose=verbose, run_time=run_time, progver=progver, prognam=prognam

;+
; NAME:
;   AIA_PREP
; PURPOSE:
;   Perform image registration (rotation, translation, scaling) of Level 1 AIA images, and update
;   the header information.
; CATEGORY:
;   Image alignment
; SAMPLE CALLS:
;   Inputing infil (in this case iindex and idata are returned with 
;   IDL> AIA_PREP, infil, [0,1,2], oindex, odata
;   Inputing iindex and idata: 
;   IDL> AIA_PREP, iindex, idata, oindex, odata
; INPUTS:
;   There are 2 basic usages for inputing the image and header data into AIA_PREP:
;   Case 1: References FITS file name on disk:
;           input1 - String array list of AIA FITS files
;           input2 - List of indices of FITS files to read 
;   Case 2. References index structure and data array in memory
;           (index, data already read from FITS file using, for example, READ_SDO.PRO):
;           input1 - index structure
;           input2 - data array
; OUTPUTS (OPTIONAL):
;   oindex - The updated index structure of the input image
;   odata - Registered output image.
; KEYWORDS:
;   DO_WRITE_FITS - if set, write the registered image and updated header structure to disk
;   NEAREST - If set, use nearest neighbor interpolatipon
;   INTERP - If set, use bilinear interpolation
;   CUBIC - If set, use cubic convolution interpolation ith the specified value (in the range [-1,0]
;           as the interpolation parameter.  Cubic interpolation with this parameter equal -0.5
;           is the default.
; TODO:
;   Reference scale should be read from the database file.
;   Decide if CROTA1 should be added
; HISTORY:
;   2010 (circa), Created ab initio - GLS (slater@lmsal.com)
;   2010-12-07 - GLS - Corrected call to break_file - GLS
;   2011-02-10 - GLS - 1. Corrected sign error on roll (Thanks to Ralph Seguin)
;                      2. Corrections to tags CRPIX(1,2), CDELT(1,2, and CROT2A were not being 
;                         propagated to output header structure (OINDEX).  This was fixed
;                         (Thanks to Benjamin Mampaey)
;   2011-02-28 - GLS - 1. Added missing half pixel to output CRPIX1/2
;                      2. Defined LVL_NUM keyword to be 1.5 (should it be 1.51 to differentiate from
;                         real time?)
;                      3. Added _EXTRA in call for keyword inheritance
;                      4. Made UNCOMP_DELETE the default in call to READ_SDO.
;   2011-03-02 - GLS - 1. Corrected references to NAXIS1/2 in case of compressed file headers
;                      2. Changed default interpolation for ROT function from nearest neighbor to
;                         damped cubic.

; Define prognam, progver variables
prognam = 'AIA_PREP.PRO'
progver = 'V4.0' ; 2011-02-10 (GLS)
progver = 'V4.1' ; 2011-03-01 (GLS)
progver = 'V4.2' ; 2011-03-02 (GLS)

; Start the clock running
t0 = systime(1)
t1 = t0	; Keep track of running time

; Default definitions for relevant directories:
if not exist(outdir) then outdir = './'

; Define reference plate scale in arsec per pixel, if not already defined:
if not exist(scale_ref) then scale_ref = 0.6
if not exist(missing) then missing = 0

; Define constants:
 wave_val_arr = [ 0094,   0131,   0171,   0193,   0211,   0304,   0335,   1600,   1700,   4500 ]
 wave_str_arr = [ '094',  '131',  '171',  '193',  '211',  '304',  '335', '1600', '1700', '4500']

; Process keywords:
loud = 1 - KEYWORD_SET(quiet)
verbose = KEYWORD_SET(verbose)
if (verbose eq 1) then loud = 1
if (loud) then PRINT, 'Running ', prognam, ' ', progver

if not exist(scale_ref) then scale_ref = 0.60
if ( (not exist(nearest)) and (not exist(interp)) and (not exist(cubic)) ) then cubic = -0.5

if keyword_set(no_uncomp_delete) then uncomp_delete = 0 else uncomp_delete = 1

input_err = 1
if ( (size(input1, /tname) eq 'STRING') and (size(input2, /n_dim) le 1) ) then begin
  input_err = 0
  do_read = 1
  infil_arr = input1
  ss_infil = input2
  if ss_infil[0] eq -1 then ss_infil = indgen(n_elements(infil_arr))
  n_img = n_elements(ss_infil)

  ss_not_exist = where(file_exist(infil_arr) ne 1, n_not_exist)
  if n_not_exist eq 0 then begin
    read_sdo, infil_arr[ss_infil[0]], iindex0, idata0, uncomp_delete=uncomp_delete, _extra=_extra
    data_type = size(idata0, /type)
    data_dim = size(idata0, /dim)
    data_ndim = size(idata0, /n_dim)
  endif else begin
    input_err = 2
    print, ' Input error: Not all files in file list found.  Returning.'
    return
  endelse
endif

if ( (size(input1, /tname) eq 'STRUCT') and ( (size(input2, /n_dim) eq 2) or (size(input2, /n_dim) eq 3) ) ) then begin
  input_err = 0
  do_read = 0
  iindex = input1
  idata = input2
  ss_infil = indgen(n_elements(iindex))
  n_img = n_elements(ss_infil)
  data_type = size(idata, /type)
  data_dim = size(idata, /dim)
  data_ndim = size(idata, /n_dim)
endif

if input_err eq 1 then begin
  print, ' Input error: INPUT1 and INPUT2 must be:'
  print, '   EITHER: FITS file list and indices list'
  print, '   OR:     INDEX array and DATA array.'
  print, ' Returning.'
  return
endif

; If iindex not passed in, then read all FITS index struxtures into array:
if do_read eq 1 then read_sdo, infil_arr[ss_infil], iindex, /nodata

; Update history record(s)
update_history, iindex, version=progver

; Create template oindex structure array:
if n_params() ge 3 then oindex = iindex

; Create empty array for odata:
if n_params() ge 4 then odata = make_array([data_dim[0], data_dim[1], n_img], type=data_type)

for i=0, n_img-1 do begin  
  if do_read eq 1 then begin
    file0 = infil_arr[ss_infil[i]]
    read_sdo, file0, iindex0, idata0, /uncompdelete, /mixed
    iindex0 = iindex[i]
  endif else begin
    iindex0 = iindex[i]
    idata0 = idata[*,*,i]
  endelse
  oindex0 = iindex0

; Determine wavelength and other properties:
  ss_match_wave = where(strtrim(fix(iindex0.wavelnth),2) eq wave_val_arr, n_match_wave)
  wave_val = wave_val_arr[ss_match_wave]
  wave_string = wave_str_arr[ss_match_wave]

; Define scale factor for binned images:
  bin_fac = round(10*iindex0.cdelt1)/(10.0*scale_ref)

; Optionally use master pointing file for pointing parameters:
  if keyword_set(use_pnt_file) then begin

; Read master pointing file and find the nearest record to AIA image:
    pnt_struct = ssw_sdo_master_pointing(ssw=1-keyword_set(not_use_ssw))
    ss_close_pnt = tim2dset(anytim(pnt_struct.date_obs, /ints), anytim(iindex0.date_obs, /ints))
    pnt_str_match = pnt_struct[ss_close_pnt]

; Extract appropriate IMSCALE, X0, Y0, and INSTROT values for wavelength:
    t_names_pnt = strlowcase(tag_names(pnt_str_match))
    ss_imscale = where(strpos(t_names_pnt, wave_string + '_imscale') ne -1, n_match_imscale)
    imscale = pnt_str_match.(ss_imscale)
    scale_fac = imscale/scale_ref
    ss_x0 = where(strpos(t_names_pnt, wave_string + '_x0') ne -1, n_match_x0)
    x0 = (pnt_str_match.(ss_x0) - 1)
    ss_y0 = where(strpos(t_names_pnt, wave_string + '_y0') ne -1, n_match_x0)
    y0 = (pnt_str_match.(ss_y0) - 1)
    ss_instrot = where(strpos(t_names_pnt, wave_string + '_instrot') ne -1, n_match_instrot)
    instrot = pnt_str_match.(ss_instrot)  

  endif else begin
    scale_fac = iindex0.cdelt1/(scale_ref*bin_fac)
    x0 = iindex0.crpix1
    y0 = iindex0.crpix2
    instrot = iindex0.crota2
  endelse

; Use these values in call to ROT function to register image:
  odata0 = rot(idata0, -instrot, scale_fac, x0, y0, interp=interp, cubic=cubic, missing=missing)

; Update header tag values as needed:
  naxis1 = gt_tagval(oindex0, /znaxis1, missing=gt_tagval(oindex0, /naxis1))
  naxis2 = gt_tagval(oindex0, /znaxis2, missing=gt_tagval(oindex0, /naxis2))
  oindex0.crpix1 = naxis1/2 + 0.5
  oindex0.crpix2 = naxis2/2 + 0.5
  oindex0.cdelt1 = scale_ref*bin_fac
  oindex0.cdelt2 = scale_ref*bin_fac
  oindex0.crota2 = 0.0
  oindex0.r_sun  = iindex0.rsun_obs/iindex0.cdelt1
  oindex0.lvl_num = 1.5

; Update history record(s):
;  update_history, oindex0, version=progver

; Update the output oindex structure array, and data array, if necessary:
  if n_params() ge 3 then oindex[i] = oindex0
  if n_params() ge 4 then odata[0,0,i] = odata0

; Optionally write out new FITS file:
  if keyword_set(do_write_fits) then begin
    if do_read eq 1 then begin
      break_file, file0, val1, val2, val3, val4
      outfil = val3 + val4
      if not exist(outdir) then outdir = val2
    endif else begin
      if not exist(outdir) then outdir = './'
      outfil = 'AIA' + time2file(oindex0.date_obs, /sec) + string(wave_val, format='$(i4.4)') + '.fits'
    endelse
    mwritefits, oindex0, odata0, outfile=concat_dir(outdir, outfil)
  endif

endfor

end
