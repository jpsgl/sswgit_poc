
function index2mpo, index, rec_num=rec_num, t_ref=t_ref, use_indmpo=use_indmpo, _extra=_extra

;+
; Keywords:
;   index
;     Either: any struct with time in any of standard accepted ssw formats (see anytim.pro)
;     Or:     any vector of times in one of standard ssw formats
;   REC_NUM:
;     if set, return the mpo series record specified by REC_NUM
;   t_ref:
;     if set, return the mpo series record with the nearest earlier MPO_REC.DATE to T_REF for
;     which the INDEX.DATE_OBS tag falls within the range [MPO_REC.T_START, MPO_REC.T_END].
;   Procedure:
;     If no over-riding keyword is passed, then return the most recent mpo series record
;       for which INDEX.DATE_OBS tag falls within the range [MPO_REC.T_START, [MPO_REC.T_END].
;     If REC_NUM is passed, then return the mpo series record corrsponding to that record
;       number.
;     If T_REF is passed, then return the mpo series record with the nearest earlier
;       MPO_REC.DATE to T_REF for which the INDEX.DATE_OBS tag falls within the
;       range [MPO_REC.T_START, [MPO_REC.T_END].
;     if USE_INDMPO is set, and INDEX.MPO_REC exists, then return the mpo record specified
;       by INDEX.MPO_REC.
;     If more than one of REC_NUM, T_REF, or USE_INDMPO are passed, then return with an
;       error message: 'More than one of REC_NUM, T_REF, or USE_INDMPO passed.  Returning '-1'.
;-

common ssw_sdo_master_pointing_blk, mpall, mplatest

if exist(rec_num) and exist(t_ref) then begin
  print, 'Both REC_NUM and T_REF passed.  Returning "-1" .'
  return, -1
endif

; Test whether INDEX is a structure or not:
n_rec = n_elements(index)
typ_index = size(index, /type)
if typ_index eq 8 then begin
  time = index.date_obs
  if tag_exist(index, 'MPO_REC') then mpo_rec = index.mpo_rec
endif else begin
  time = index
endelse

if not exist(mpall) then mpo_str_full = ssw_sdo_master_pointing(/all_versions) else $
  mpo_str_full = mpall

; Handle the case in which REC_NUM is passed:
; -------------------------------------------

if exist(rec_num) then begin
  n_rec_spec = n_elements(rec_num)
  if n_rec_spec eq n_rec then begin
    for i=0,n_rec-1 do begin
      if i eq 0 then mpo_str = ssw_sdo_master_pointing(rec=rec_num[i]) else $
        mpo_str = str_concat(mpo_str, ssw_sdo_master_pointing(rec=rec_num[i]))
    endfor
    return, mpo_str
  endif else begin
    print, 'INDEX and REC_NUM have different number of elements. Returning "-1" .'
    return, -1
  endelse
endif

; Handle the case in which T_REF is passed:
; -----------------------------------------

if exist(t_ref) then begin

  t_obs_sec = anytim(time)                    ; Time of image
  t_ref_sec = anytim(t_ref)                   ; Time of processing of image
  t_date_sec = anytim(mpo_str_full.date)      ; Time of processing of pointing calibration
  t_start_sec = anytim(mpo_str_full.t_start)  ; Start time of interval of validity for calibration
  t_stop_sec = anytim(mpo_str_full.t_stop)    ; End time of interval of validity of calibration

  for i=0,n_rec-1 do begin

; Find closest prior calibration epoch start time to image time
; in order to find the correct calibration epoch:
    ss_predate_obs = where(t_start_sec le t_obs_sec[i])
    mpo_str_predate_obs = mpo_str_full[ss_predate_obs]
    t_predate_obs_sec = t_start_sec[ss_predate_obs]
    t_predate_obs_max = max(t_predate_obs_sec, ss_predate_obs_max)

; Find all mpo records corresponding the matching epoch:
    ss_match_epoch = where(t_start_sec eq t_predate_obs_max)
    mpo_str_match_epoch = mpo_str_full[ss_match_epoch]
    t_date_sec_match_epoch = t_date_sec[ss_match_epoch]

; Find the closest mpo record creation date prior to t_ref (image processing time):
    ss_predate_t_ref = where(t_date_sec_match_epoch le t_ref_sec)
    mpo_str_predate_t_ref = mpo_str_match_epoch[ss_predate_t_ref]
    t_date_sec_predate_t_ref = t_date_sec_match_epoch[ss_predate_t_ref]
    t_date_sec_predate_t_ref_max = max(t_date_sec_predate_t_ref, ss_predate_t_ref_max)

; Find the mpo record corresponding to this date:
    mpo_str_predate_t_ref_max = mpo_str_predate_t_ref[ss_predate_t_ref_max]

; Build array of matching mpo records as image times are looped through:
    if not exist(mpo_str) then mpo_str = mpo_str_predate_t_ref_max else $
      mpo_str = str_concat(mpo_str, mpo_str_predate_t_ref_max)
  endfor

  return, mpo_str
endif

; Handle the case in which USE_INDMPO is set and MPO_REC tag exists:
; ------------------------------------------------------------------

; TODO - Check whether all records can be retrieved with a single call to
;        ssw_sdo_master_pointing.pro:

if keyword_set(use_indmpo) then begin
  if exist(mpo_rec) then begin
    print, ' Found MPO_REC tag: ' + strtrim(mpo_rec,2)
    for i=0, n_rec-1 do begin
      if i eq 0 then mpo_str = ssw_sdo_master_pointing(rec=index[i].mpo_rec) else $
        mpo_str = str_concat(mpo_str, ssw_sdo_master_pointing(rec=index[i].mpo_rec))
    endfor
    return, mpo_str
  endif else begin
    print, ' Use of tag MPO_REC requested, but tag does not exist in INDEX record.
    print, ' Returning "-1" .'
    return, -1
  endelse
endif

; By default, if none of REC_NUM, USE_INDMPO, or T_REF are passed, return the most
; recent mpo series record for which INDEX.DATE_OBS tag falls within the range
; [MPO_REC.T_START, [MPO_REC.T_END].
; --------------------------------------------------------------------------------

if ( (not exist(rec_num)) and (not exist(t_ref)) and (not keyword_set(use_indmpo)) ) then begin

  t_obs_sec = anytim(time)                    ; Time of image
  t_date_sec = anytim(mpo_str_full.date)      ; Time of processing of pointing calibration
  t_start_sec = anytim(mpo_str_full.t_start)  ; Start time of interval of validity for calibration
  t_stop_sec = anytim(mpo_str_full.t_stop)    ; End time of interval of validity of calibration

  for i=0,n_rec-1 do begin

; Find closest prior calibration epoch start time to image time
; in order to find the correct calibration epoch:
    ss_predate_obs = where(t_start_sec le t_obs_sec[i])
    mpo_str_predate_obs = mpo_str_full[ss_predate_obs]
    t_predate_obs_sec = t_start_sec[ss_predate_obs]
    t_predate_obs_max = max(t_predate_obs_sec, ss_predate_obs_max)

; Find all mpo records corresponding the matching epoch:
    ss_match_epoch = where(t_start_sec eq t_predate_obs_max)
    mpo_str_match_epoch = mpo_str_full[ss_match_epoch]
    t_date_sec_match_epoch = t_date_sec[ss_match_epoch]

; Find most recent mpo record corresponding to matching epoch:
; TODO - Check whether this can be done directly with call to ssw_sdo_master_pointing.pro:
    t_date_sec_match_epoch_max = max(t_date_sec_match_epoch, ss_match_epoch_max)
    mpo_str_match_epoch_max = mpo_str_match_epoch[ss_match_epoch_max]

    if not exist(mpo_str) then mpo_str =  mpo_str_match_epoch_max else $
      mpo_str = str_concat(mpo_str,  mpo_str_match_epoch_max)
  endfor

  return, mpo_str
endif

end
