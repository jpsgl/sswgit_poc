
function parse_hist, hist

n_rec = n_elements(hist)

for i=0,n_rec-1 do begin

  suffix = ''

  buff = str2arr(hist[i], '  ')
  if n_elements(buff) eq 2 then begin
;    pos_space = strpos(buff, '  ')
;    buff = strmid(buff, pos_space+2)

    buff = str2arr(buff[1], ': ')
    if n_elements(buff) eq 2 then begin
      buff_names = buff[0]
      buff_vals = buff[1]

      buff_names = str2arr(buff_names,' ')
      if n_elements(buff_names) eq 2 then begin
        if (buff_names[0] eq 'Orig') and (strpos(buff_names[1], '&') eq -1) then begin
          suffix = '_orig'
          buff_names = buff_names[1]
        endif else begin
;          stop, ' Problem.  Stopping.'
          print, 'Uninterpretted parameter: ' + strtrim(buff_names[1],2)
        endelse
      endif       

      name_arr = str2arr(buff_names, ',') + (['',suffix])[suffix eq '_orig']
      val_arr = str2arr(strtrim(buff_vals,2), ',')

      n_names = n_elements(name_arr)
      n_vals = n_elements(val_arr)

      if n_vals eq n_names then begin
        if not exist(statement) then statement = 'var_struct = {'
        for j=0,n_names-1 do begin
          statement = statement + name_arr[j] + ':' + (["'",''])[is_number(name_arr[j])] + $
                      val_arr[j] + (["'",''])[is_number(name_arr[j])] + ', '
        endfor
      endif

    endif

  endif

endfor

if not exist(statement) then begin
  var_struct = ''
endif else begin
  statement = strmid(statement,0,strlen(statement)-2) + '}'
  result = execute(statement)
endelse

return, var_struct

end

