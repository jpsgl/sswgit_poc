
pro test_3h

l1co = ssw_service_filter(/iris, /all) ; cut job : IRIS OBS 1:1
last10 = last_nelem(l1co, 10) ; last 'many' are valid for testing

; Loop through 'index,data' for given .JOBID:

for i=0,9 do begin

   ssw_service_read_data, last10[0].JOBID, index, data, wave=1600, max_images=10

   help, index, data
; INDEX           STRUCT    = -> <Anonymous> Array[10]
; DATA            INT       = Array[654, 467, 10]

; Optionally, get the L1 cutout file list for the above:
   ssw_service_read_data, last10[0].JOBID, files, /files_only, wave=171
   help, files
; FILES           STRING    = Array[292]  << L1 cutout file list

; Update pointing tags using both 3h and weekly MPT databases, and compare to original:
   index_3h = aia_pnt_update(index, /threeh)
   index_weekly = aia_pnt_update(index, /weekly)

   print, ''
   print, 'Original CRPIX1:'
   print, index.crpix1
   print, '3h CRPIX1:'
   print, 'MPT series: ' + index_3h.ds_mpo
   print, index_3h.crpix1
   print, 'Weekly CRPIX1:'
   print, 'MPT series: ' + index_weekly.ds_mpo
   print, index_weekly.crpix1
   print, ''
   print, 'Original CRPIX2:'
   print, index.crpix2
   print, '3h CRPIX2:'
   print, 'MPT series: ' + index_3h.ds_mpo
   print, index_3h.crpix2
   print, 'Weekly CRPIX2:'
   print, 'MPT series: ' + index_weekly.ds_mpo
   print, index_weekly.crpix2
   print, ''
   print, 'Original CDELT1:'
   print, index.cdelt1
   print, '3h CDELT1:'
   print, 'MPT series: ' + index_3h.ds_mpo
   print, index_3h.cdelt1
   print, 'Weekly CDELT1:'
   print, 'MPT series: ' + index_weekly.ds_mpo
   print, index_weekly.cdelt1
   print, ''
   print, 'Original CROTA2:'
   print, index.crota2
   print, '3h CROTA2:'
   print, 'MPT series: ' + index_3h.ds_mpo
   print, index_3h.crota2
   print, 'Weekly CROTA2:'
   print, 'MPT series: ' + index_weekly.ds_mpo
   print, index_weekly.crota2
   print, ''
STOP
endfor

end
