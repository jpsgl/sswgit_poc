
pro aia_respike, iindex, idata, oindex, odata, ispikedd=ispikedd, $
  spikedd=spikedd, spike_coords=spike_coords, x_coords=x_coords, $
  y_coords=y_coords, refresh=refresh

;+
; NAME:
;   aia_unspike
; PURPOSE:
;   Re-insert spikes to level1 images that were removed during the
;   level0 to level1 processing.
; CALL:
;   aia_respike, iindex, idata, oindex, odata
; INPUTS:
;   iindex - Level 1 index structure (or array of structures)
;   idata  - Corresponding level 1 data array (or cube)
; OPTIONAL INPUTS:
;   spikedd - User input array of dimensions [n,3] where n=number of spikes,
;             spikedd[0,*] contains spike positions (in 1-d array coordinates),
;             and spikedd[1,*] contains spike values corresponding to the given
;             coordiantes.
; RESTRICTIONS:
;   Should only be applied to level1 images.  If applied, for example to
;   level1.5 images, the result will be in error because the level 1.5 data was
;   been re-mapped.
; HISTORY:
;   2010-11-17 - GLS - Written
;   2010-01-28 - GLS:
;              - Populate the output index (oindex)
;              - Allow user-input of spikedd array
;   2012-12-05 - GLS - Included proper handling of cutout images
;   2013-09-20 - GLS - Corrected error in handling of full frame images
; TODO:
;-

if exist(ispikedd) then spikedd = ispikedd else $
  aia_jsoc_getcaldata, iindex, spikeii, spikedd, /spikes, refresh=refresh

odata = idata
oindex = iindex

if iindex.lvl_num eq 1 then begin

  if ( (iindex.naxis1 lt 4096) or (iindex.naxis2 lt 4096) ) then begin

    print, ' Array dimensions suggest image is a cutout.'
    print, ' Spike coordinates will be transformed.'

;    siz_data = size(data)
;    his_arr = iindex.history
;    pos_xll = strpos(his_arr, 'xll')
;    ss_rec_match = where(pos_xll ne -1, n_match)
;    if n_match eq 1 then begin
;      coord_rec = his_arr[ss_rec_match]
;      pos_colon = strpos(coord_rec, ':')

    coord_rec = get_history(iindex, 'xll', found=found)
    if found then begin
      cutout_params = fix(str2arr(ssw_strsplit(coord_rec, ':', /tail)))
      xll = cutout_params[0]
      yll = cutout_params[1]
      nx_part = cutout_params[2]
      ny_part = cutout_params[3]
    endif else begin
      xll = ((iindex.x0_mp + 1) - iindex.crpix1) + 1
      yll = ((iindex.y0_mp + 1) - iindex.crpix2) + 1
      nx_part = iindex.naxis1
      ny_part = iindex.naxis2
    endelse

    spike_coords1d_full = reform(spikedd[*,0])
    spike_coords1d_part = $
      full2part(reform(spikedd[*,0]), xll=xll, yll=yll, nx_part=nx_part, $
                ny_part=ny_part, x_part=x_coords, y_part=y_coords, /mk_vec, $
                ss_match=ss_match)
    spike_vals_full = reform(spikedd[*,1])
    if ss_match[0] ne -1 then begin
      spike_vals_part = spike_vals_full[ss_match]

      odata[spike_coords1d_part] = spike_vals_part
    endif

  endif else begin
    spike_coords1d_full = reform(spikedd[*,0])
    spike_vals_full = reform(spikedd[*,1])
    odata[spike_coords1d_full] = reform(spike_vals_full)
  endelse

  oindex.lvl_num = 0.5d
  oindex.nspikes = 0

endif else begin
  print, ' Input image must be level 1 for spikes to be inserted.  Returning.'
  return
endelse

end
