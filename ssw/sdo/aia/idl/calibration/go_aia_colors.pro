
function go_aia_colors, t0, t_win_sec=t_win_sec, wave=wave, $
  index_arr, data_arr, do_disp=do_disp, _extra=_extra, qstop=qstop

if not exist(index_arr) or not exist(data_arr) then begin
  if not exist(t0) then t0 = anytim(!stime, /ccsds)
  if not exist(t_win_sec) then t_win_sec = 12d0
  if not exist(t1) then t1 = anytim(anytim(t0)+t_win_sec, /ccsds)
  if not exist(wave) then wave = [171,304,94,193,211,335]
;  if not exist(wave) then wave = [171, 193, 211]

;  ssw_jsoc_time2data, t0, t1, index_arr, data_arr, /jsoc2, wave=wave, last_n=1
  ssw_jsoc_time2data, t0, t1, index_arr, data_arr, /jsoc2, wave=wave
endif

if ( keyword_set(do_disp) and not exist(outsiz) ) then outsiz = 256

aia_colors, index_arr, data_arr, $
  odata_arr=odata_arr, odata_true_arr=odata_true_arr, $
  data_euv_true=data_euv_true, data_hi_t_true=data_hi_t_true, $
  data_lohi_true=data_lohi_true, $
  img_out=img_out, $
  outsiz=outsiz, do_jpeg=do_jpeg, dir_out=dir_out, $
  _extra=_extra, qstop=qstop

if keyword_set(do_disp) then begin
  wdef,0,outsiz*3,outsiz*3
  n_waves = n_elements(odata_true_arr[0,0,0,*])
  for i=0,n_waves-1 do tv, odata_true_arr[*,*,*,i], i, true=1
  tv, data_euv_true,  i,   true=3
  tv, data_hi_t_true, i+1, true=3
  tv, data_lohi_true, i+2, true=3
endif

if keyword_set(qstop) then $
  stop, 'GO_AIA_COLORS: Stopping on request before return.'

return, img_out

end
