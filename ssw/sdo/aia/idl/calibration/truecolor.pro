
; convert a 2d image to a true-color image
function truecolor,image,red,green,blue
  thisImage = BYTSCL(image)
  s = SIZE(thisImage)
  image3d = BYTARR(3, s(1), s(2))
  image3d(0, *, *) = red(thisImage)
  image3d(1, *, *) = green(thisImage)
  image3d(2, *, *) = blue(thisImage)
return,image3d
end
