
function full2part, x, y, xll=xll, yll=yll, xsiz_orig=xsiz_orig, ysiz_orig=ysiz_orig, $
  nx_part=nx_part, ny_part=ny_part, x_part=x_part, y_part=y_part, mk_vec=mk_vec, $
  ss_match=ss_match

if not exist(xsiz_orig) then xsiz_orig = 4096l
if not exist(ysiz_orig) then ysiz_orig = 4096l
if not exist(y) then begin
  x_orig = long(x) mod xsiz_orig
  y_orig = long(x)  /  xsiz_orig
endif else begin
  x_orig = x
  y_orig = y
endelse

x_part = x_orig - long(xll) + 1
y_part = y_orig - long(yll) + 1

ss_match = where( ((x_part ge 0) and (y_part ge 0) and $
                   (x_part lt nx_part) and (y_part lt ny_part)), n_match)
if n_match gt 0 then begin
  x_part = x_part[ss_match]
  y_part = y_part[ss_match]

  if keyword_set(mk_vec) then $
    out_arr = y_part*nx_part + x_part else $
    out_arr = [[x_part],[y_part]]
endif else begin
  out_arr = -1
endelse

return, out_arr

end

