
pro ssw_reg, iwcs, idata, owcs, odata, wcs_ref=wcs_ref, $
  cutout=cutout, match_fov=match_fov, $
  scale_ref=scale_ref, roll_ref=roll_ref, crpix1_ref, crpix2_ref, $
  x0_ref=x0_ref, y0_ref=y0_ref, $
  interp=interp, cubic=cubic, x_off=x_off, y_off=y_off, $
  _extra=_extra, qstop=qstop

;+
; NAME:
;   ssw_reg
; PURPOSE:
;   Register any image (rotate, translate, scale) to arbitrary reference values of
;   scale, roll, and image center x and y, given a structure with this minimal tag set:
;     [naxis1, naxis2, crpix1, crpix2, cdelt1, cdelt2, crota2]
; CATEGORY:
;   Image registration
; CALL:
;
; OUTPUTS:
;   owcs - The updated index structure of the input image
;   odata - Registered output image.
; KEYWORDS:
; TODO:
; HISTORY:
;   2012-10-17 - GLS
;   2012-11-15 - GLS - Fixed errors in definition of CRPIX1,2 in OINDEX.
;   2013_03_27 - GLS - Added 'cutout' keyword switch to be compatible with previous
;                      versions of AIA_PREP. 
;-

; Define prognam, progver variables
prognam = 'SSW_REG.PRO'

;progver = 'V1.0' ; 2012-10-17 (GLS)
;progver = 'V1.1' ; 2012-11-15 (GLS)
progver = 'V1.2' ; 2013-05-28 (GLS)

; Define defaults for ROT interpolation and fill parameters:
if ( (not exist(interp)) and (not exist(cubic)) ) then cubic = -0.5
if not exist(missing) then missing = min(idata)

do_display = 1

if n_params() eq 0 then begin
  print, ' Program version number = ' + strtrim(progver,2)
  return
endif

if keyword_set(cutout) then begin
;  pivot = 1
  if not exist(roll_ref) then roll_ref = wcs_ref.crota2
  if not exist(scale_ref) then scale_ref = wcs_ref.cdelt1

  img1_cen_pix = wcs_pix2pix(wcs_ref, iwcs)
  if exist(crpix1_ref) then x0 = crpix1_ref-1.0 else x0 = img1_cen_pix[0]
  if exist(crpix2_ref) then y0 = crpix2_ref-1.0 else y0 = img1_cen_pix[1]
endif else begin
;  pivot = 0
  if not exist(roll_ref)  then roll_ref  = 0   ; default roll 0 degress
  if not exist(scale_ref) then scale_ref = 0.6 ; default scale is 0.6 arcsec per pixel

  if exist(crpix1_ref) then x0 = crpix1_ref-1.0 else x0 = iwcs.crpix1-1.0
  if exist(crpix2_ref) then y0 = crpix2_ref-1.0 else y0 = iwcs.crpix2-1.0
endelse

; Define deltas for rotation, translation, and scaling by subtracting the reference
; parameters from the corresponding image parameters:
delta_roll = roll_ref - iwcs.crota2
scale_fac = iwcs.cdelt1/scale_ref

; Call ROT.PRO to re-map image:
;odata = rot(idata, delta_roll, scale_fac, crpix1_ref-1.0, crpix2_ref-1.0, $
;            interp=interp, cubic=cubic, pivot=pivot, missing=missing)
odata = rot(idata, delta_roll, scale_fac, x0, y0, $
            interp=interp, cubic=cubic, pivot=pivot, missing=missing)

; Set coordinate tags equal to reference values:
owcs = iwcs
owcs.crota2 = roll_ref
owcs.cdelt1 = scale_ref
owcs.cdelt2 = scale_ref





if keyword_set(cutout) then begin

  if tag_exist(wcs_ref, 'crval1') then begin
    if wcs_ref.crval1 eq 0 then begin
      owcs.crpix1 = (iwcs.naxis1/2.-wcs_ref.naxis1/2.) + wcs_ref.crpix1
      owcs.crpix2 = (iwcs.naxis2/2.-wcs_ref.naxis2/2.) + wcs_ref.crpix2
    endif else begin
;      owcs.crpix1 = iwcs.naxis1/2. + 0.5 + wcs_ref.xcen/owcs.cdelt1
;      owcs.crpix2 = iwcs.naxis2/2. + 0.5 + wcs_ref.ycen/owcs.cdelt2
      owcs.crpix1 = ((iwcs.crpix1-1) - x0)*iwcs.cdelt1/owcs.cdelt1 + (iwcs.naxis1/2. + 0.5)
      owcs.crpix2 = ((iwcs.crpix2-1) - y0)*iwcs.cdelt2/owcs.cdelt2 + (iwcs.naxis2/2. + 0.5)
    endelse
  endif else begin
    owcs.crpix1 = (iwcs.naxis1/2.-wcs_ref.naxis1/2.) + wcs_ref.crpix1
    owcs.crpix2 = (iwcs.naxis2/2.-wcs_ref.naxis2/2.) + wcs_ref.crpix2
  endelse

endif else begin
  owcs.crpix1 = iwcs.naxis1/2 + 0.5
  owcs.crpix2 = iwcs.naxis2/2 + 0.5
endelse

if keyword_set(match_fov) then begin
  print, 'Matching FOV.'

  if owcs.naxis1 gt wcs_ref.naxis1 then begin
    del_naxis1_half = (owcs.naxis1 - wcs_ref.naxis1)/2
    xll_crop = del_naxis1_half
    nx_crop = wcs_ref.naxis1
    odata = odata[xll_crop:xll_crop+nx_crop-1,*]
    owcs.naxis1 = wcs_ref.naxis1
    owcs.crpix1 = owcs.crpix1 - del_naxis1_half
  endif

  if owcs.naxis2 gt wcs_ref.naxis2 then begin
    del_naxis2_half = (owcs.naxis2 - wcs_ref.naxis2)/2
    yll_crop = del_naxis2_half
    ny_crop = wcs_ref.naxis2
    odata = odata[*,yll_crop:yll_crop+ny_crop-1]
    owcs.naxis2 = wcs_ref.naxis2
    owcs.crpix2 = owcs.crpix2 - del_naxis2_half
  endif
endif

;owcs.xcen = comp_fits_cen(owcs.crpix1,owcs.cdelt1,owcs.naxis1,owcs.crval1)
;owcs.ycen = comp_fits_cen(owcs.crpix2,owcs.cdelt2,owcs.naxis2,owcs.crval2)

oxcen = comp_fits_cen(owcs.crpix1, owcs.cdelt1, owcs.naxis1, owcs.crval1)
oycen = comp_fits_cen(owcs.crpix2, owcs.cdelt2, owcs.naxis2, owcs.crval2)
if tag_exist(owcs, 'XCEN') then owcs.xcen = oxcen else $
  owcs = add_tag(owcs, oxcen, 'XCEN')
if tag_exist(owcs, 'YCEN') then owcs.ycen = oycen else $
  owcs = add_tag(owcs, oycen, 'YCEN')

if keyword_set(do_display) then begin
  wdef,0,512
  if not exist(odata_max) then odata_max = max(odata)/2
;  index2map, oindex, safe_log10(odata), omap
  index2map, owcs, safe_log10(odata), omap
  plot_map, omap, /limb, grid=15
endif





if tag_exist(owcs, 'lvl_num') then owcs.lvl_num = 1.5

; Update history record:

buff_history = ''

interpolation_algorithm = 'ROT called with nearest neighbor sampling'
if exist(interp) then $
  if interp ne 0 then $
    interpolation_algorithm = 'ROT called with linear interpolation'
if exist(cubic) then $
  if cubic ne 0 then $
    interpolation_algorithm = 'ROT called with cubic interpolation: cubic = ' + strtrim(cubic,2)

buff_history = [buff_history, interpolation_algorithm]

if exist(wcs_ref) then begin
  if tag_exist(wcs_ref,'fsn') then $
    buff_history = [buff_history, 'Image registered to SDO image with FSN = ' + $
                                   strtrim(wcs_ref.fsn,2)]
  if tag_exist(wcs_ref,'t_obs') then $
    buff_history = [buff_history, 'Image registered to SDO image with T_OBS = ' + $
                                   strtrim(wcs_ref.t_obs,2)]
endif else begin
  buff_history = [buff_history, 'Image registered to sun center, roll 0, and ' + $
                                'platescale = 0.6 arcsec per pixel.']
endelse

update_history, owcs, version=progver, caller=prognam
update_history, owcs, buff_history

if keyword_set(qstop) then stop,' Stopping on request.'

end
