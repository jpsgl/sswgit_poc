
pro aia_colors, input1, input2, $
  odata_arr=odata_arr, odata_true_arr=odata_true_arr, $
  data_euv_true=data_euv_true, data_hi_t_true=data_hi_t_true, $
  data_lohi_true=data_lohi_true, $
  img_out=img_out, $
  outsiz=outsiz, do_jpeg=do_jpeg, dir_out=dir_out, fn_suffix=fn_suffix, $
  _extra=_extra, qstop=qstop

;+
; NAME:
;   AIA_COLORS
; PURPOSE:
;   Given an array of AIA files or arrays of index and data,
;   apply AIA SunToday scaling and color tables, create SunToday composite
;   triplets when requisite components are available, and optionally
;   write out in either jpeg or gif format.
;   NB: For triplets, the images should all be co-registered (level 1.5)
;       AIA_PREP may be used for this.
; CATEGORY:
;   Image scaling and color tables.
; SAMPLE CALLS:
;   IDL> img_out = aia_colors(file_arr, odata_arr_true=odata_arr_true 
;   IDL> img_out = aia_colors(index_arr, data_arr, odata_arr=odata_arr, $
;                             data_euv_true=data_euv_true, $
;                             data_hi_t_true=data_hi_t_true, $
;                             data_lohi_true=data_lohi_true)
; INPUTS:
;   POSITIONAL PARAMETERS:
;     INPUT1: If no INPUT2 then this must be an AIA file list.
;             If with INPUT2, then this must be a header structure
;             (may be vector).
;     INPUT2: Image or cube of images. Passed with INPUT1 when former
;             is a header structure.
; KEYWORD PARAMETERS:
;   INPUTS:
;     DO_JPEG: If set then create and write out jpeg images for generated single and
;     composite images
;   OUTPUTS:
;     ODATA_TRUE_ARR: Array of true color images for individual wavelengths.
;     DATA_EUV_TRUE:  True color image for 211/193/171 composite
;     DATA_HI_T_TRUE: True color image for  94/335/193 composite
;     DATA_LOHI_TRUE: True color image for 304/211/171 composite
; HISTORY:
;   2012 - GLS
;-

wave_arr = [0094, 0131, 0171, 0193, 0211, 0304, 0335, 4500, 1600, 1700]

; By default, write out images to current directory:
if not exist(dir_out) then dir_out = './'
if not exist(fn_suffix) then fn_suffix = ''

; Figure out what parameters have been passed.
;   If only one input then it is assumed to be a list of AIA files.
;   If two, then assume index, data (possible vectore/cube) are passed:
case n_params() of
  0: begin
       print, ' Must pass either a file list (one paramter) or'
       print, '   index, data (two params, possible, vector/cube).'
       print, ' Returning.'
       return
     end
  1: begin
       file_arr = input1
       if size(file_arr, /type) ne 7 then begin
         print, ' One parameter passed.  Assumed to be a file list,'
         print, '   so it must be a string or a vector of strings.'
         print, ' Returning.'
         return
       end
     end
  2: begin
       index_arr = input1
       data_arr = input2
       if size(index_arr, /type) ne 8 then begin
         print, ' First parameter (in case of two) must be a header'
         print, '   structure (may be vector).  Returning.'
         return
       end
       if ( (size(data_arr, /type) eq 7) or $
            (size(data_arr, /type) eq 8) ) then begin
         print, ' First parameter (in case of two) must be a header'
         print, '   structure (may be vector).  Returning.'
         return
       end
     end
  else: begin
       print, 'Too many params.  Returning.
       return
     end
endcase

; Determine number of images to process:
if exist(file_arr) then n_img = n_elements(file_arr) else $
  if exist(index_arr) then n_img = n_elements(index_arr) else begin
    print, ' Neither file list nor image cube passed.  Returning.'
    return
    end

; Read all headers if not passed:
if not exist(index_arr) then $
  read_sdo, file_arr, index_arr, /no_data

if not exist(outsiz) then outsiz = index_arr[0].naxis1
t_arr    = index_arr.date_obs
wavelnth = index_arr.wavelnth
exptime  = index_arr.exptime

; Create container arrays for composite and true color output images:
odata_arr = bytarr(outsiz, outsiz, n_img)
odata_true_arr = bytarr(3, outsiz, outsiz, n_img)

; Prepare each image and write out if requested:
for i=0, n_img-1 do begin
  if not exist(data_arr) then $
    read_sdo, file_arr[i], index0, data0 else begin
      index0 = index_arr[i] & data0 = data_arr[*,*,i]
      end
  if outsiz ne index0.naxis1 then $
    odata0 = rebin(data0, outsiz, outsiz, /samp) else odata0 = data0

  ss_match = where(strtrim(wavelnth[i],2) eq wave_arr, n_match)
  odata0 = aia_intscale(odata0, exptime=exptime[i], wavelnth=wavelnth[i], /bytescale)
; Load appropriate color table for wavelength:
  aia_lct, r, g, b, wavelnth=wavelnth[i], /load
; Create true color version:
  odata0_true = truecolor(odata0,r,g,b)
; Insert composite and true color images into respective cubes:
  odata_arr[0,0,i] = odata0
  odata_true_arr[0,0,0,i] = odata0_true

  if keyword_set(do_jpeg) then $
    write_jpeg, concat_dir(dir_out, counterstring(wavelnth[i],4) + $
      fn_suffix + '.jpg'), odata0_true, true=1, qual=90
  if keyword_set(do_gif) then $
    write_gif,  concat_dir(dir_out, counterstring(wavelnth[i],4) + $
      fn_suffix + '.gif'), odata0_true, r, g, b

  img_out = odata0_true
endfor

; Make standard triplets, if components are available:
ss_094 = where(wavelnth eq 094, n_094)
ss_171 = where(wavelnth eq 171, n_171)
ss_193 = where(wavelnth eq 193, n_193)
ss_211 = where(wavelnth eq 211, n_211)
ss_304 = where(wavelnth eq 304, n_304)
ss_335 = where(wavelnth eq 335, n_335)

; 1-2 MK EUV combo:
if n_171*n_193*n_211 ne 0 then begin
  data_euv_true = odata_arr[*,*,[ss_211[0],ss_193[0],ss_171[0]]]
  if keyword_set(do_jpeg) then $
    write_jpeg, concat_dir(dir_out, '211_193_171.jpg'), $
      data_euv_true, true=3, qual=85
  img_out = data_euv_true
endif

; High temperature mix:
if n_094*n_193*n_335 ne 0 then begin
  data_hi_t_true = odata_arr[*,*,[ss_094[0],ss_335[0],ss_193[0]]]
  if keyword_set(do_jpeg) then $
    write_jpeg, concat_dir(dir_out, '094_335_193.jpg'), $
      data_hi_t_true, true=3, qual=85
  img_out = data_hi_t_true
endif

; 304A plus corona mix:
if n_171*n_211*n_304 ne 0 then begin
  data_lohi_true = odata_arr[*,*,[ss_304[0],ss_211[0],ss_171[0]]]
  if keyword_set(do_jpeg) then $
    write_jpeg, concat_dir(dir_out, '304_211_171.jpg'), $
      data_lohi_true, true=3, qual=85
  img_out = data_lohi_true
endif

if keyword_set(qstop) then $
  stop, 'AIA_COLORS: Stopping on request before return.'

end
