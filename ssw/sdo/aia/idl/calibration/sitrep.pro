
pro sitrep, item, brief=brief, sdo=sdo, sot=sot, iris=iris

;+
; TODO:
;   - Make 'sparse database' (text?) with one tag (or one line) per FITS header
;     Searchable with ssw routine TIME2SITREP.pro 
;-

if not exist(item) then begin
  index = last_file(/head)
endif else begin
  type_item = size(item, /type)
  if type_item eq 7 then $
    index = last_file(item, /head) else $
    index = item
endelse

;help, $
;index,DATE_OBS, $
;index.IMG_TYPE, $
;index.ACS_ECLP, $
;index.ACS_MODE, $    
;index.ACS_SUNP, $
;index.ACS_SAFE, $
;index.QUALITY , $
;index.QUALLEV0

print, ''
print, 'SDO STATUS:'
print, ''
print, 'DATE_NOW: ' + strmid(anytim(ut_time(!stime), /ccsds),0,22) + ' UT'
print, 'DATE_OBS: ' + index.date_obs
print, 'IMG_TYPE: ' + strtrim(index.IMG_TYPE,2)
print, 'ACS_ECLP: ' + strtrim(index.ACS_ECLP,2)
print, 'ACS_MODE: ' + strtrim(index.ACS_MODE,2)
print, 'ACS_SUNP: ' + strtrim(index.ACS_SUNP,2)
print, 'ACS_SAFE: ' + strtrim(index.ACS_SAFE,2)
print, 'QUALITY : ' + strtrim(index.QUALITY,2)
print, 'QUALLEV0: ' + strtrim(index.QUALLEV0,2)
print, ''

end

