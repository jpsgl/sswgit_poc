
pro aia_pimpout, file_arr=file_arr, index_arr=index_arr, $
  data_arr=data_arr, data_cube_out=data_cube_out, $
  data_euv=data_euv, data_hi_t=data_hi_t, data_304=data_304, $
  do_jpeg=do_jpeg, do_gif=do_gif, dir_out=dir_out

;+
; NAME:
;   AIA_PIMPOUT
; PURPOSE:
;   Given an array of AIA files or arrays of index and data,
;   apply AIA SunToday scaling and color tables,
;   and write out in either jpeg or gif format.
;   NB: For composites, the images must all be co-registered.
;       AIA_PREP may be used for this.
; CATEGORY:
;   Image scaling and color tables.
; SAMPLE CALLS:
;   IDL> aia_pimpout, index_arr=index0_prep, data_arr=data0_prep, $
;                     data_cube_out=data_cube_out 
;   IDL> aia_pimpout, index_arr=index0_prep, data_arr=data0_prep, $
;                     data_euv=data_euv, data_hi_t=data_hi_t, data_304=data_304
; INPUTS:
;   POSITIONAL PARAMETERS:
;   KEYWORD PARAMETERS:
;     FILE_ARR:  If present, read files and use associated images
;     INDEX_ARR: Index structure array for AIA FITS images
;     DATA_ARR:  AIA image cube including wavelengths to be given AIA standard
;                  color tables
; OUTPUTS:
;   KEYWORDS PARAMETERS:
;     DATA_EUV:  Output true color image for 171/193/211 composite
;     DATA_HI_T: Output true color image for  94/335/193 composite
;     DATA_304:  Output true color image for 304/211/171 composite
; DO_JPEG:
;   If set then create and write out jpeg images for generated single and
;     composite images
; DO_GIF:
;   If set then create and write out gif images for generated single images
; HISTORY:
;   2012 - GLS
;-

; By default, write out images to current directory:
if not exist(dir_out) then dir_out = './'

; To keep track of which wavelengths have been processed:
wave_list = [0094, 0131, 0171, 0193, 0211, 0304, 0335, 4500, 1600, 1700]
n_waves = n_elements(wave_list)
ss_match_arr = bytarr(n_waves)

; Read in images if necessary:
if (not exist(index_arr)) or (not_exist(data_arr)) then begin
  if exist(file_arr) then begin
    ss_exist = file_exist(file_arr)
    if min(ss_exist) eq 1 then begin
      read_sdo, file_arr, index_arr, data_arr
    endif else begin
      print, ' Input files not found.  Returning.
      return
    endelse
  endif else begin
    print, ' Must input either file list or index, data pairs.  Returning.
    return
  endelse
endif

; Extract needed tags from all image headers:
t_img    = index_arr.date_obs
exptime  = index_arr.aimgshce / 1000.
wavelnth = index_arr.wavelnth

; Prepare each image and write out:
n_img = n_elements(index_arr)
data_cube_out = bytarr(4096,4096,n_waves)
for i=0, n_img-1 do begin
  ss_match = where(strtrim(wavelnth[i],2) eq strtrim(wave_list,2), n_match)
  if n_match gt 0 then ss_match_arr[ss_match] = 1
  print, 'Now processing wavelength ' + strtrim(wavelnth[i],2)
  index_in = index_arr[i]
  data_in  = data_arr[*,*,i]

  data_out = aia_intscale(data_in, exptime=exptime[i], wavelnth=wavelnth[i], /bytescale)
  data_cube_out[0,0,ss_match[0]] = data_out

  aia_lct, r, g, b, wavelnth=wavelnth[i], /load

  if keyword_set(do_jpeg) then $
  write_jpeg, concat_dir(dir_out, counterstring(wavelnth[i],4) +'.jpg'), truecolor(data_out,r,g,b), true=1, qual=90
  if keyword_set(do_gif) then $
  write_gif,  concat_dir(dir_out, counterstring(wavelnth[i],4) +'.gif'), data_out, r, g, b
endfor

; Now prepare and write out standard 'SunToday' triplets, if components are available:

; 1-2 MK EUV combo:
if ( ( ss_match_arr[where(strtrim(wave_list,2) eq '171') ] ) and $
     ( ss_match_arr[where(strtrim(wave_list,2) eq '193') ] ) and $
     ( ss_match_arr[where(strtrim(wave_list,2) eq '211') ] ) ) then begin
  data_euv = data_cube_out[*,*,[where(strtrim(wave_list,2) eq '171'), $
                                where(strtrim(wave_list,2) eq '193'), $
                                where(strtrim(wave_list,2) eq '211')]]
  if keyword_set(do_jpeg) then $
    write_jpeg, concat_dir(dir_out, '211_193_171.jpg'), data_euv, true=3, qual=85
endif

; Higher temperature mix:
if ( ( ss_match_arr[where(strtrim(wave_list,2) eq  '94') ] ) and $
     ( ss_match_arr[where(strtrim(wave_list,2) eq '193') ] ) and $
     ( ss_match_arr[where(strtrim(wave_list,2) eq '335') ] ) ) then begin
  data_hi_t = data_cube_out[*,*,[where(strtrim(wave_list,2) eq  '94'), $
                                 where(strtrim(wave_list,2) eq '193'), $
                                 where(strtrim(wave_list,2) eq '335')]]
  if keyword_set(do_jpeg) then $
    write_jpeg, concat_dir(dir_out, '094_335_193.jpg'), data_hi_t, true=3, qual=85
endif

; 304A plus corona mix:
if ( ( ss_match_arr[where(strtrim(wave_list,2) eq '171') ] ) and $
     ( ss_match_arr[where(strtrim(wave_list,2) eq '211') ] ) and $
     ( ss_match_arr[where(strtrim(wave_list,2) eq '304') ] ) ) then begin
  data_304_mix = data_cube_out[*,*,[where(strtrim(wave_list,2) eq '171'), $
                                    where(strtrim(wave_list,2) eq '211'), $
                                    where(strtrim(wave_list,2) eq '304')]]
  if keyword_set(do_jpeg) then $
    write_jpeg, concat_dir(dir_out, '304_211_171.jpg'), data_304_mix, true=3, qual=85
endif

end

