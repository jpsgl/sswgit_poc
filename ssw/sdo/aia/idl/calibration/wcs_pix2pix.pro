
function wcs_pix2pix, index1, index2

wcs1 = fitshead2wcs(index1)
wcs2 = fitshead2wcs(index2)

img1_cen_pix1 = [(wcs1.naxis[0]/2+0.5)-1, (wcs1.naxis[1]/2+0.5)-1]
img1_cen_hg = wcs_get_coord(wcs1, img1_cen_pix1)

img1_cen_pix2 = wcs_get_pixel(wcs2, img1_cen_hg)

return, img1_cen_pix2

end
