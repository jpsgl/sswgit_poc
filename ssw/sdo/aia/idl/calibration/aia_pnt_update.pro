
function aia_pnt_update, index, ds=ds, threeh=threeh, weekly=weekly, _extra=_extra

;+
;   Name: aia_pnt_update
;   Purpose:
;      - Update pointing of an AIA header using a record from one of the JSOC MPT
;        data series
;   Input Parameters:
;      index
;   Keyword Paramters:
;   History:
;      2017-06-17 - GLS
;      2017-07-28 - SLF - strpad
;      2018-02-12 - Ryan Timmons fix indexing bug in FITS header update
;-

oindex = index

t_img = index.date_obs
n_img = n_elements(index)

mpo_match = tim2mpo(t_img, ds=ds, threeh=threeh, weekly=weekly)
tnames_mpo = tag_names(mpo_match)

; Extract original MPO pointing, scale, roll from header:
if tag_exist(index, 'wavelnth') then wave_arr = index.wavelnth else $
   if tag_exist(index, 'origin') then wave_arr = strmid(index.origin, 10)

wave_buff = 'A_' + strpad(strtrim(wave_arr,2),3,fill='0')  + '_'

wave_x0 = strlowcase(wave_buff + 'x0')
wave_y0 = strlowcase(wave_buff + 'y0')
wave_imscale = strlowcase(wave_buff + 'imscale')
wave_instrot = strlowcase(wave_buff + 'instrot')

tnames_mpo = strlowcase(tag_names(mpo_match[0]))
ss_x0 = where(tnames_mpo eq wave_x0[0], n_x0)
ss_y0 = where(tnames_mpo eq wave_y0[0], n_y0)
ss_imscale = where(tnames_mpo eq wave_imscale[0], n_imscale)
ss_instrot = where(tnames_mpo eq wave_instrot[0], n_instrot)

if n_x0 ne 0 then x0_mpo = mpo_match.(ss_x0[0])
if n_y0 ne 0 then y0_mpo = mpo_match.(ss_y0[0])
if n_imscale ne 0 then imscale_mpo = mpo_match.(ss_imscale[0])
if n_instrot ne 0 then instrot_mpo = mpo_match.(ss_instrot[0])

;buff = 'x0_mpo = mpo_match.' + wave_x0
;status = execute(buff)
;buff = 'y0_mpo = mpo_match.' + wave_y0
;status = execute(buff)
;buff = 'imscale_mpo = mpo_match.' + wave_imscale
;status = execute(buff)
;buff = 'intsrot_mpo = mpo_match.' + wave_instrot
;status = execute(buff)

if tag_exist(index, 'x0_mp') then begin
   crota2 = index.crota2

   x0_orig = index.x0_mp
   y0_orig = index.y0_mp
   imscale_orig = index.imscl_mp
   instrot_orig = index.inst_rot
endif else begin
;  x0_orig = index.crpix1 - 1
;  y0_orig = index.crpix2 - 1
;  imscale_orig = index.cdelt1
;  instrot_orig = index.crota2 + index.sat_rot

;  read_sdo  Orig CRPIX1,CRPIX2: 2042.4800,2043.9500
;  read_sdo  Orig CDELT1,CDELT2: 0.60069800,0.60069800
;  read_sdo  Orig CROTA2: -0.13866800
   crpix_arr = reform(float(str2arr(arr2str(strtrim(strmid(index.history[7], 30), 2)))), 2, 10)
   cdelt_arr = reform(float(str2arr(arr2str(strtrim(strmid(index.history[8], 30), 2)))), 2, 10)
   crota_arr = float(        strtrim(strmid(index.history[9], 23), 2) )
   crpix1 = reform(crpix_arr[0,*])
   crpix2 = reform(crpix_arr[1,*])
   cdelt1 = reform(cdelt_arr[0,*])
   cdelt2 = reform(cdelt_arr[1,*])
   crota2 = crota_arr

   x0_orig = crpix1 - 1
   y0_orig = crpix2 - 1
   imscale_orig = cdelt1
   instrot_orig = crota2 + index.sat_rot
endelse

; Computer mpo-index differences:
x0_diff = x0_mpo - x0_orig
y0_diff = y0_mpo - y0_orig
imscale_ratio = imscale_mpo / imscale_orig
instrot_diff = instrot_mpo - instrot_orig

; Update pointing (wcs) tags in headers:
oindex.crpix1 = index.crpix1 + x0_diff
oindex.crpix2 = index.crpix2 + y0_diff
oindex.cdelt1 = index.cdelt1 * imscale_ratio
oindex.cdelt2 = index.cdelt2 * imscale_ratio
oindex.xcen   = index.xcen + x0_diff * oindex.cdelt1
oindex.ycen   = index.ycen + y0_diff * oindex.cdelt2
if tag_exist(oindex, 'crota2') then oindex.crota2 = crota2 + instrot_diff else $
   oindex = add_tag(oindex, crota2 + instrot_diff, 'crota2')

; Update mpo tags in headers:
if tag_exist(oindex, 'x0_mp') then oindex.x0_mp = x0_mpo else $
   oindex = add_tag(oindex, x0_mpo, 'x0_mp')
;RPT fix bug below - was 'then oindex.x0_mp = y0_mpo', both x,y were wrong
if tag_exist(oindex, 'y0_mp') then oindex.y0_mp = y0_mpo else $
   oindex = add_tag(oindex, y0_mpo, 'y0_mp')
if tag_exist(oindex, 'imscale_mp') then oindex.imscale_mp = imscale_mpo else $
   oindex = add_tag(oindex, imscale_mpo, 'imscale_mp')
if tag_exist(oindex, 'instrot_mp') then oindex.instrot_mp = instrot_mpo else $
   oindex = add_tag(oindex, instrot_mpo, 'instrot_mp')

; Add tag for mpo data series:
if tag_exist(oindex, 'ds_mpo') then oindex.ds_mpo = ds else $
   oindex = add_tag(oindex, ds, 'ds_mpo')

; Optionally print out old and new tags:
if keyword_set(verbose) then begin
   n_print = (n_img-1) < n_print_max
   print, ''
   prstr, oindex[0:n_print].ds_mpo
   print, ''
   prstr, string( index[0:n_print].x0_mp, '$(f10.4)') + '   ' + $
          string(oindex[0:n_print].x0_mp, '$(f10.4)')
   prstr, string( index[0:n_print].y0_mp, '$(f10.4)') + '   ' + $
          string(oindex[0:n_print].y0_mp, '$(f10.4)')
   prstr, string( index[0:n_print].imscale_mp, '$(f10.4)') + '   ' + $
          string(oindex[0:n_print].imscale_mp, '$(f10.4)')
   prstr, string( index[0:n_print].instrot_mp, '$(f10.4)') + '   ' + $
          string(oindex[0:n_print].instrot_mp, '$(f10.4)')
   print, ''
   prstr, string( index[0:n_print].crpix1, '$(f10.4)') + '   ' + $
          string(oindex[0:n_print].crpix1, '$(f10.4)')
   prstr, string( index[0:n_print].crpix2, '$(f10.4)') + '   ' + $
          string(oindex[0:n_print].crpix2, '$(f10.4)')
   prstr, string( index[0:n_print].cdelt1, '$(f10.4)') + '   ' + $
          string(oindex[0:n_print].cdelt1, '$(f10.4)')
   prstr, string( index[0:n_print].crota2, '$(f10.4)') + '   ' + $
          string(oindex[0:n_print].crota2, '$(f10.4)')
   print, ''
endif

return, oindex
end 
