function AIA_BP_MAKE_TRESP, emiss_wave, emiss_logte, emiss, effarea_wave, effarea, $
	name, platescale, full_trespstr, countunits

;+
;
; $Id: aia_bp_make_tresp.pro,v 1.2 2010/06/21 23:34:14 boerner Exp $
;
; $Log: aia_bp_make_tresp.pro,v $
; Revision 1.2  2010/06/21 23:34:14  boerner
; Pass countinuts from read to make tresp
;
; Revision 1.1  2010/06/15 22:58:53  boerner
; Initial version
;
;
; Returns an instrument temperature response function, given the effective area
; and solar emissivity.
;
; INPUTS:
;	emiss_wave	-	the wavelength grid of the emissivity
;	emiss		-	the emissivity
;	emiss_logte	-	the temperature grid of the emissivity
;
;	effarea_wave-	the wavelength grid of the effective area
;	effarea		-	the effective area
;	
;	name		-	the name to be attached to the resulting structure
;	platescale	-	the platescale of the instrument (sr per pixel)
;	countunits	-	photons or DN (units of the effective area)
;
; RETURNS:	
;	trespstr	-	a structure giving the instrument temperature response, with
;							.logte, .tresp, .name and .units fields
;
; OUTPUTS:
; 	full_trespstr-	the more detailed temperature response structure,
;					including the wavelength throughput at each temperature
;					bin
;-

if N_ELEMENTS(name) eq 0 then name = ''
if N_ELEMENTS(countunits) eq 0 then countunits = 'DN'

iresponse = INTERPOL(effarea, effarea_wave, emiss_wave)

lowave = WHERE(emiss_wave lt MIN(effarea_wave), numlow)
if numlow gt 0 then iresponse[lowave] = 0.
hiwave = WHERE(emiss_wave gt MAX(effarea_wave), numhi)
if numhi gt 0 then iresponse[hiwave] = 0.

goodpoints = WHERE(iresponse ge 0)
wavelimits = LIMITS(emiss_wave[goodpoints])
wavestep = emiss_wave[1] - emiss_wave[0]

; Generate the simple temperature response function
tresp = REFORM( (iresponse>0) # emiss)
tresp = tresp * platescale * wavestep
trespstr = {name : name, units : countunits + ' cm^5 s^-1 pix^-1', logte : emiss_logte, tresp : tresp}

; Generate the more detailed temperature response matrix
numtemp = N_ELEMENTS(emiss_logte)
numwave = N_ELEMENTS(emiss_wave)
response_matrix = REBIN(iresponse, numwave, numtemp)
full_tresp = (response_matrix>0) * emiss
full_tresp = full_tresp * platescale
full_trespstr = CREATE_STRUCT(trespstr, 'wave', emiss_wave, 'full_units', $
	countunits + ' cm^5 s^-1 A^-1 pix^-1', 'full_tresp', full_tresp)

RETURN, trespstr

end
