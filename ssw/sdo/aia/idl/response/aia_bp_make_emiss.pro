function AIA_BP_MAKE_EMISS, $
	add_ni11 = add_ni11, cut_lilike = cut_lilike, cut_lyman = cut_lyman, $
	no_he2 = no_he2, he2boost = he2boost, $
	wvlrange = wvlrange, wvlstep = wvlstep, $
    abundfile = abundfile, ioneqfile = ioneqfile, $
    pressure = pressure, all = all, $
    verbose = verbose, saveresults = saveresults

;+
;
; $Id: aia_bp_make_emiss.pro,v 1.8 2013/01/07 19:22:33 boerner Exp $
;
; $Log: aia_bp_make_emiss.pro,v $
; Revision 1.8  2013/01/07 19:22:33  boerner
; Fixed labelling of fixscales for cutlyman and he2boost (note that the suvi emiss made before this fix had the right factors applied, but bad self-documentation in .fixes)
;
; Revision 1.7  2013/01/07 19:13:57  boerner
; Fix comment on He2 fix
;
; Revision 1.6  2012/11/13 20:21:49  boerner
; Only one 950 line
;
; Revision 1.5  2012/09/27 20:00:22  boerner
; Use loop to fix HeII lines; also cut H Lyman lines
;
; Revision 1.4  2012/03/27 22:16:17  boerner
; Fixed unit label on continuum
;
; Revision 1.3  2011/09/13 22:09:01  boerner
; Removed dependence on local routines
;
; Revision 1.2  2011/06/03 20:49:39  boerner
; Use same wavlength grid for continuum and lines
;
; Revision 1.1  2010/08/03 00:27:48  boerner
; initial version
;
;
; Generates a structure containing a complete model of the solar emissivity.
;
; NOTE: THIS INCLUDES THE EFFECT OF ELEMENTAL ABUNDANCES.
;
; RETURNS:  emiss    -- a structure with five fields:
;               general --	structure listing date and filename of the source data
;               lines --	an array of chianti linelist structures with the contribution
;							function (goft) of all the lines in the solar spectrum
;               cont  --	a structure describing the emissivity of the continuum
;               notes --	a string giving descriptive notes
;               total --	a structure with the total (line + continuum) emissivity; 
;					Units are ph cm3 s-1 sr-1 A-1, so multiplying by DEM and 
;					integrating over temperature gives ph cm-2 s-1 sr-1 A-1
;
;   KEYWORDS: /add_ni11    -- if set, the Ni XI line at 148 Angstroms is added (as, essentially,
;                a duplicate of the Ni XII line at 152 Angstroms). This is critical
;                for the 150 A bandpass; much less so for other instruments.
;
;          /cut_lilike --  if set, the strong Li-like lines that are important for the AIA
;                or MSSTA bandpasses have their intensities cut in half, as suggested
;                by E. Landi. This is most important for the 335 bandpass.
;
;          /cut_lyman --  if set, the Lyman beta/gamma/delta lines (at 1026/973/950 A)
;				are cut dramatically; by default, they are overpredicted by several 
;				order of magnitude
;
;			/no_he2	--	if set, the He II 304 emissivity is not boosted to agree with V&R
;				observations. By default, it is multiplied by a factor of he2boost (below)
;
;			he2boost=	--	the factor by which to boost the HeII emissivity (if 
;							/no_he2 is not set)
;
;      /saveresults    -- if set, the resulting solar emissivity structure is saved at
;               !data + 'chianti\solar_emiss.genx'
;
;          /all   --    include even "unobserved" lines
;      /verbose    -- print lots of diagnostic information during the calculation
;      ioneqfile=  --   ionization equilibrium; defaults to !xuvtop + '\ioneq\mazotta_etal_ext.ioneq'
;          pressure=  --   isobaric model pressure; defaults to 1.e15
;
;     abundfile=   --    abundance; defaults to !xuvtop + '\abund\sun_coronal_ext.ioneq'
;
;		wvlrange=	--	can be set to a 2-element vector giving the minimum and maximum wavelength
;						in Angstroms for the resulting spectrum. Defaults to [25, 413]
;
;		wvlstep=	--	the wavelength step size, in Angstroms (defaults to 0.1). Seems not to work very well.
;
;-

if KEYWORD_SET(verbose) then t0 = SYSTIME(1)

if not KEYWORD_SET(abundfile) then abundfile = !xuvtop+'/abundance/sun_coronal_ext.abund'
READ_ABUND, abundfile, abundz, abundref

fixes = 'Generated by AIA_BP_MAKE_EMISS on ' + SYSTIME()
general = {emiss_date : SYSTIME(), abundfile : abundfile}

if not KEYWORD_SET(wvlrange) then begin
	minwv = 25.
	maxwv = 413.
endif else begin
    minwv = wvlrange[0]
	maxwv = wvlrange[1]
endelse

if not KEYWORD_SET(verbose) then verbose = 0
if not KEYWORD_SET(all) then all = 0
if not KEYWORD_SET(pressure) then pressure = 1.e15
if not KEYWORD_SET(ioneqfile) then ioneqfile = !xuvtop+'/ioneq/chianti.ioneq'
CH_SYNTHETIC, minwv, maxwv, output=synspec, pressure=pressure, /goft, $
	verbose=verbose, ioneq_name=ioneqfile, all=all
general = CREATE_STRUCT(general, 'source', 'CH_SYNTHETIC run on ' + SYSTIME())

linez = synspec.lines

for i = 0ll, N_ELEMENTS(linez)-1 do linez[i].goft = linez[i].goft * abundz[linez[i].iz-1]

spectags = TAG_NAMES(synspec)
for i = 0, N_TAGS(synspec) - 1 do if spectags[i] ne 'LINES' then $
    general = CREATE_STRUCT(general, spectags[i], synspec.(i))
tempgrid = FLOAT(general.ioneq_logt)
numtemp = N_ELEMENTS(tempgrid)


;++++++++++++++++++++++++++++++++++++++++++++
; Implement empirical corrections to line 
; emissivities (based on optional keywords)
;--------------------------------------------

; Add Ni XI line at 152 A (important for MSSTA 150 channel)
if KEYWORD_SET(add_ni11) then begin
	    ni12line_index = WHERE(linez.wvl eq 152.15401, count)
	    if count eq 1 then begin
	    	ni12line = linez[ni12line_index]
	       linez = [linez, ni12line]
	       numlines = N_ELEMENTS(linez)
	       linez[numlines - 1].ion = 11
	       linez[numlines - 1].ident = 'Manually added Ni XI line based on M&H data'
	       linez[numlines - 1].wvl = 148.38
	       linez[numlines - 1].snote = 'Ni XI'
	       fixes = [fixes, 'Manually added Ni XI 148 '+STRING("305B)+' line based on M&H data']
	    endif else PRINT, count, ' lines found with lambda = 152.14...cannot add Ni XI line'
endif

; Cut emissivity on select Li-like lines important for AIA, per suggestion by Enrico Landi
if KEYWORD_SET(cut_lilike) then begin
    fixlines = [335.41000, 302.19019]
    for i = 0, N_ELEMENTS(fixlines)-1 do begin
       fixid = WHERE(linez.wvl eq fixlines[i], count)
       if count eq 1 then begin
            linez[fixid].goft = linez[fixid].goft / 2.
            fixes = [fixes, 'Cut emissivity on '+STRING(fixlines[i])+' line by factor of 2']
        endif else PRINT, 'Error fixing Li-like line at ', fixlines[i]
    endfor
endif

; Cut emissivity on strong neutral H lyman beta-gamma-delta lines (1026-973-950 A)
if KEYWORD_SET(cut_lyman) then begin
    fixlines 	= [949.74500, 972.53802, 972.53900, 1025.7240, 1025.7250]
    fixscales 	= [6.47e4, 6.6596e4, 6.6596e4, 1.13e5, 1.13e5]
    for i = 0, N_ELEMENTS(fixlines)-1 do begin
       fixid = WHERE(linez.wvl eq fixlines[i] and linez.snote eq 'H I', count)
       if count ge 1 then begin
            linez[fixid].goft = linez[fixid].goft / fixscales[i]
            fixtag = STRING('Cut emissivity on ', fixlines[i], ' line by factor of ', $
            	fixscales[i], format = '(a20, f10.5, a20, e10.5)')
            fixes = [fixes, fixtag]
        endif else PRINT, 'Error fixing H I Lyman line at ', fixlines[i]
    endfor
endif

; Boost emissivity on He II 304 A lines 
if not KEYWORD_SET(no_he2) then begin
	if not KEYWORD_SET(he2boost) then he2boost = 5.
    fixlines 	= [303.78101, 303.78601]
    for i = 0, N_ELEMENTS(fixlines)-1 do begin
       fixid = WHERE(linez.wvl eq fixlines[i] and linez.snote eq 'He II', count)
       if count eq 1 then begin
            linez[fixid].goft = linez[fixid].goft * he2boost
            fixtag = STRING('Boosted emissivity on ', fixlines[i], ' line by ', $
            	he2boost, format = '(a23, f10.5, a10, f8.2)')
            fixes = [fixes, fixtag]
        endif else PRINT, 'Error fixing He II line at ', fixlines[i]
    endfor
endif


;++++++++++++++++++++++++++++++++++++++++++++
; Calculate continuum emissivity and generate
; total emissivity (combining lines and
; continuum)
;--------------------------------------------

if not KEYWORD_SET(wvlstep) then wvlstep = 0.1
wvlgrid = FINDGEN((maxwv - minwv)/wvlstep+1) * wvlstep + minwv
cont_emiss = AIA_BP_CH_CONTINUUM(wvlgrid, ioneqfile = ioneqfile, abundfile = abundfile, /noloud)
cont_str = {emiss : cont_emiss, wave : wvlgrid, temp : tempgrid, $
    units : 'photons cm3 s-1 sr-1 A-1', note : 'Generated by AIA_BP_CH_CONTINUUM on '+SYSTIME()}

emissgrid = DBLARR(N_ELEMENTS(wvlgrid), numtemp)
for i = 0, N_ELEMENTS(wvlgrid)-1 do begin
   emissgrid[i,*] = cont_emiss[i, *]
   inlines = WHERE(ABS(linez.wvl - wvlgrid[i]) le 0.5 * wvlstep, count)
   if count gt 0 then for j = 0, count - 1 do $
     emissgrid[i, *] = emissgrid[i,*] + linez[inlines[j]].goft / wvlstep / $
        (1.602e-12 * 12398. / (linez[inlines[j]].wvl))
endfor
struct_date = AIA_BP_DATE_STRING(/time)
totalstr = {wave : wvlgrid, logte : tempgrid, emissivity : emissgrid, $
    units : 'photons cm3 s-1 sr-1 A-1', date : struct_date}
emiss = {date : struct_date, general : general, lines : linez, $
	cont : cont_str, fixes : fixes, total : totalstr}
if KEYWORD_SET(saveresults) then begin
    SAVEGEN, file = !data + 'chianti/solar_emiss.genx', $
       names = ['GENERAL', 'LINES', 'CONT', 'FIXES', 'TOTAL'], $
       general, linez, cont_str, fixes, totalstr, $
       text = 'Written by AIA_BP_MAKE_EMISS on ' + SYSTIME()
endif


if KEYWORD_SET(verbose) then begin
	t1 = SYSTIME(1)
	PRINT, 'AIA_BP_MAKE_EMISS:  elapsed run time	= ', t1 - t0, 'seconds', $
		format = '(a50, f10.1, a15)'
endif


RETURN, emiss


end