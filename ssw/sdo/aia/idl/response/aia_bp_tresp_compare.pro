pro AIA_BP_TRESP_COMPARE, separate = separate, chiantifix = chiantifix, $
	version = version, oldversion = oldversion, ps = ps, emversion = emversion, $
	evenorm = evenorm, fixold = fixold, normold = normold, $
	demcal = demcal, demall = demall, pstop = pstop

;+
;
; Generates a plot comparing version 1 and 2 of the temperature response functions. Set
; the /separate keyword to see a 2-window plot showing the old and new functions overlaid; 
; leave it off to see just the ratio of new over old.
;
; $Log: aia_bp_tresp_compare.pro,v $
; Revision 1.7  2013/07/10 18:35:27  boerner
; Added pstop and remove stim_date
;
; Revision 1.6  2013/05/07 21:38:48  boerner
; Added demall
;
; Revision 1.5  2013/02/19 22:19:56  boerner
; Added demcal option, and fixed ps plotting
;
; Revision 1.4  2013/01/09 21:43:01  boerner
; Added version 4 and emversion options
;
; Revision 1.3  2013/01/09 20:47:33  boerner
; Added V3 and PS option
;
; Revision 1.2  2012/01/05 01:35:19  boerner
; Fixed sign error, updated header
;
; Revision 1.1  2012/01/05 01:25:38  boerner
; Initial version
;
;
;-

if not KEYWORD_SET(chiantifix) then chiantifix = 0
if not KEYWORD_SET(evenorm) then evenorm = 0
if not KEYWORD_SET(version) then version = 2
if not KEYWORD_SET(oldversion) then oldversion = 1
if not KEYWORD_SET(emversion) then emversion = 0
if not KEYWORD_SET(fixold) then fixold = 0
if not KEYWORD_SET(normold) then normold = evenorm


if KEYWORD_SET(demall) then begin
	oldr = AIA_GET_RESPONSE(/temp, /dn, ver=1, emver=1, evenorm = 0, chiantifix = 0)
	midr = AIA_GET_RESPONSE(/temp, /dn, ver=4, emver=4, evenorm = 1, chiantifix = 0)
	newr = AIA_GET_RESPONSE(/temp, /dn, ver=4, emver=4, evenorm = 1, chiantifix = 1)
endif else case version of
	4	:	begin
				oldr = AIA_GET_RESPONSE(ver=oldversion, /temp, /dn, evenorm = normold, chiantifix = fixold)
				newr = AIA_GET_RESPONSE(ver=4, /temp, /dn, emversion = emversion, $
					chiantifix = chiantifix, evenorm = evenorm)
				singletitle = 'Version 4 T response'
				xleg = 6
				yleg = 0.8
			end
	3	:	begin
				oldr = AIA_GET_RESPONSE(ver=oldversion, /temp, /dn, evenorm = normold, chiantifix = fixold)
				newr = AIA_GET_RESPONSE(ver=3, /temp, /dn, chiantifix = chiantifix, evenorm = evenorm, $
					emversion = emversion)
				singletitle = 'Version 3 T response'
				xleg = 6
				yleg = 0.8
			end
	2	:	begin
				oldr = AIA_GET_RESPONSE(ver=oldversion, /temp, /dn, evenorm = normold, chiantifix = fixold)
				newr = AIA_GET_RESPONSE(ver=2, /temp, /dn, chiantifix = chiantifix, evenorm = evenorm, $
					emversion = emversion)
				singletitle = 'Version 2 T response'
				xleg = 7
				yleg = 8
			end
	else :	begin
				BOX_MESSAGE, ['Set version to 2, 3 or 4', 'Exiting...']
				RETURN
			end
endcase

wvls = [94, 131, 171, 193, 211, 335]
cols = [5,9,4,3,7,6]
acols = [5,9,4,3,7,2,6]

TVLCT, rr, gg, bb, /get

if KEYWORD_SET(ps) then begin
	oldpfont = !p.font
	!p.font = 0
	thick = 5
	SET_PLOT, 'ps'
	DEVICE, file = '~/Desktop/scratch/' + TIME2FILE(/sec, RELTIME(/now)) + '_tresp_compare.eps', $
		/portrait, /inches, xsize = 6, ysize = 6, /helvetica, $
		/isolatin, /encapsulate, /color
	eps = 1
	chars = 1
endif else begin
	thick = 1
	eps = 0
	chars = 1.5
endelse

PB_SET_LINE_COLOR

if KEYWORD_SET(demcal) then begin
	if not KEYWORD_SET(ps) then WINDOW, 10, xs = 800, ys = 600
	PLOT, /ylog, xrange = [1e5,1e8], yrange = [1e-28, 1e-25], xtit = 'Temperature [K]', $
		ytit = 'Temperature Response [DN cm!u-5!n s!u-1!n pix!u-1!n]', /xlog, $
		col = 255, 10.^newr.logte, newr.all[*,0], chars = chars, thick = thick
	OPLOT, 10.^newr.logte, newr.all[*,1], col = 5, thick = thick
	OPLOT, 10.^oldr.logte, oldr.all[*,0], col = 255, line = 2, thick = thick
	OPLOT, 10.^oldr.logte, oldr.all[*,1], col = 5, line = 2, thick = thick
	PB_LEGEND, ['94 ', '131 '] + P_ANG(), 2e7, 2e-26, col = [255, 5], /line, size = chars, eps = eps
endif else if KEYWORD_SET(demall) then begin
	oldpmulti = !p.multi
	!p.multi = [0, 1, 2]
	if not KEYWORD_SET(ps) then WINDOW, 10, xs = 1200, ys = 900
	PLOT, /ylog, xrange = [1e5,1e8], yrange = [1e-28, 1e-25], $	;, xtit = 'Temperature [K]'
		ytit = '[DN cm!u-5!n s!u-1!n pix!u-1!n]', /xlog, ymargin = [3, 1], $
		col = 255, 10.^midr.logte, midr.all[*,0], chars = chars, /ystyle, psym = -3
	OPLOT, 10.^newr.logte, newr.all[*,0], col = 5, line = 3, thick = thick
	OPLOT, 10.^newr.logte, newr.all[*,1], col = 9, line = 3, thick = thick
	OPLOT, 10.^oldr.logte, oldr.all[*,1], col = 9, line = 2, thick = thick
	OPLOT, 10.^oldr.logte, oldr.all[*,0], col = 5, line = 2, thick = thick
	OPLOT, 10.^midr.logte, midr.all[*,0], col = 5, line = 0, thick = thick
	OPLOT, 10.^midr.logte, midr.all[*,1], col = 9, line = 0, thick = thick
	PB_LEGEND, STRTRIM(wvls[0:1],2) + ' ' + P_ANG(), 2.5e7, 8e-27, col = cols[0:1], /line, size = chars, eps = eps
	PB_LEGEND, ['Preflight (AIA v1)', 'Current (AIA v4)', 'AIA v4 with empirical fix'], 1.5e6, 9e-26, $
		line = [2, 0, 3], size = chars, eps = eps, thick = thick + INTARR(3)
	
	PLOT, /ylog, xrange = [1e5,1e8], yrange = [1e-28, 2e-24], xtit = 'Temperature [K]', $
		ytit = 'Temperature Response', /xlog, ymargin = [4, 1], $
		col = 255, 10.^newr.logte, newr.all[*,2], chars = chars, /ystyle	
	for i = 2, 5 do begin
		thisind = WHERE(newr.channels eq 'A' + STRTRIM(wvls[i], 2))
		OPLOT, 10.^newr.logte, newr.all[*,thisind], col = cols[i], thick = thick
		OPLOT, 10.^oldr.logte, oldr.all[*,thisind], col = cols[i], line = 2, thick = thick
	endfor
	PB_LEGEND, STRTRIM(wvls[2:5],2) + ' ' + P_ANG(), 1e7, 1e-24, col = cols[2:5], /line, size = chars, eps = eps
	!p.multi = oldpmulti
	
endif else if KEYWORD_SET(separate) then begin
	if not KEYWORD_SET(ps) then WINDOW, 10, xs = 800, ys = 600
	PLOT, /ylog, xrange = [5,8], yrange = [1e-28, 1e-23], xtit = 'Log(T)', ytit = 'T resp', col = 255, $
		oldr.logte, oldr.all[*,0], chars = 1.5, tit = 'Old (solid) and New (dashed) : 94, 171, 211, 335'
	for i = 0, 3 do OPLOT, oldr.logte, oldr.all[*,i*2], col = acols[i*2], thick = thick
	for i = 0, 3 do OPLOT, newr.logte, newr.all[*,i*2], col = acols[i*2], line = 2, thick = thick
	if not KEYWORD_SET(ps) then WINDOW, 11, xs = 800, ys = 600
	PLOT, /ylog, xrange = [5,8], yrange = [1e-28, 1e-23], xtit = 'Log(T)', ytit = 'T resp', col = 255, $
		oldr.logte, oldr.all[*,1], chars = 1.5, tit = 'Old (solid) and New (dashed) : 131, 193, 304'
	for i = 0, 2 do OPLOT, oldr.logte, oldr.all[*,i*2+1], col = acols[i*2+1], thick = thick
	for i = 0, 2 do OPLOT, newr.logte, newr.all[*,i*2+1], col = acols[i*2+1], line=2, thick = thick
endif else begin
	PLOT, newr.logte, newr.all[*,0] / oldr.all[*,0], /ylog, yrange = [0.1, 10.], chars = 1.5, xrange = [5, 8], $
		xtit = 'Log(T)', ytit = 'Ratio NEW/OLD', title = singletitle, col = 255
	for i = 0, 6 do OPLOT, newr.logte, newr.all[*,i] / oldr.all[*,i], col = acols[i], thick = thick
	PB_LEGEND, oldr.channels, xleg, yleg, col = acols, /line, size = 1.5, eps = KEYWORD_SET(ps), thick = thick
endelse

if KEYWORD_SET(ps) then begin
	DEVICE, /close
	!p.font = oldpfont
	SET_PLOT, 'X'
endif

if KEYWORD_SET(pstop) then STOP

TVLCT, rr, gg, bb

end
