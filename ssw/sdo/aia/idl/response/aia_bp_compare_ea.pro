pro AIA_BP_COMPARE_EA, full = full

;+
;
;
;-

a3 = AIA_GET_RESPONSE(/dn, ver=3, /full)
a4 = AIA_GET_RESPONSE(/dn, ver=4, /full)

HELP, a3, a4, /str

numchan = N_ELEMENTS(a3.channels)
tag3 = TAG_NAMES(a3)
tag4 = TAG_NAMES(a4)
for i = 0, numchan - 1 do begin
	thischan = a3.channels[i]
	thiswave = FIX(STRMID(thischan, 1, 4))
	if KEYWORD_SET(full) then begin
		this3 = a3.(WHERE(tag3 eq thischan + '_FULL'))
		this4 = a4.(WHERE(tag4 eq thischan + '_FULL'))
		PLOT, this4.wave, this4.effarea, xrange = [0, 1000], /ylog, $
			chars = 1.5, title = thischan
		OPLOT, this3.wave, this3.effarea, col = 2
		PMM, this3.effarea / this4.effarea
	endif else begin
		this3 = a3.(WHERE(tag3 eq thischan))
		this4 = a4.(WHERE(tag4 eq thischan))
		PLOT, this4.wave, this4.ea, xrange = [0, 1000], /ylog, $
			chars = 1.5, title = thischan
		OPLOT, this3.wave, this3.ea, col = 2
		PMM, this3.ea / this4.ea
	endelse
	answer = ''
	READ, 'press key to continue...', answer
endfor

end