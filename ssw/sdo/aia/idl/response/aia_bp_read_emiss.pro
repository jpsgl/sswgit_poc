pro AIA_BP_READ_EMISS, filename, $
	emissstr, fullemissstr, $
	warnings, $
	logdir = logdir, silent = silent, write = write

;+
;
; $Id: aia_bp_read_emiss.pro,v 1.4 2012/10/02 22:49:53 boerner Exp $
;
;
; Reads in an emiss.txt file and returns an IDL structure containing the
; solar emissivity, calculated as specified in the file
;
; INPUT:	filename-	the name of the emiss.txt file to be read (including 
;						the path, if necessary, from the current directory; 
;						generally, no path should be used, and the function
;						should be run from the appropriate directory)
;
; OUTPUTS:	fullemissstr-	a structure giving the emissivity along with
;							some information about how it was generated
;
;			emissstr-	a structure giving just the emissivity vs. temperature
;
;			warnings-	[OPTIONAL] a string array containing any descriptive
;						text on potential errors encountered during the generation
;						of the emissivity structure
; 
; KEYWORDS:	/silent-	if set, nothing is printed to the terminal
;
;			logdir=		if set, the warnings are written to a text file in the
;						logdir
; 
;			/write-		if set, the resulting wavelength and emissivity are
;						written to a .dat file. 
;
;-

errors = ''
warnings = ''

if not KEYWORD_SET(logdir) then logdir = 0
if not KEYWORD_SET(silent) then silent = 0

; Check that the filename format is AAAAA_emiss.txt
AIA_BP_PARSE_FILENAME, filename, 'emiss', shortfile = shortfile, $
	basefile = basefile, warnings = warnings, path=filepath
CD, filepath, current = orig_dir


result = RD_TFILE(filename, 2, /nocomm, /compress, delim = '--')
rsize = SIZE(result)


; Set up the spectrum information structure and fill it with the defaults
emissstr_template = {Name : 'noname', $
			AbundFile : !xuvtop + '/abundance/sun_coronal_ext.ioneq', $
			IoneqFile : !xuvtop + '/ioneq/mazzotta_etal_ext.ioneq', $
			AllLines : 'Yes', Pressure : 1.0e15, $
			WaveMin : 1.0, WaveStep : 1.0, WaveNumSteps : 400l, $
			AddNiXI : 'No', CutLiLike : 'No', BoostHeII : 5.0, $
			CutLyman : 'No', OutFilePath : '.'}


; Go through each field in the emissivity information structure and check to
; see whether the spec.txt file sets its value. If not (or if it is specified
; redundantly), use the default and print a warning
temp_emissstr = AIA_BP_FILE2TEMPLATE(emissstr_template, result, $
	warnings = warnings)
if temp_emissstr.name ne 'noname' then basefile = temp_emissstr.name

ni11on = STRUPCASE(temp_emissstr.AddNiXI) eq 'YES'
lilikeon = STRUPCASE(temp_emissstr.CutLiLike) eq 'YES'
allon = STRUPCASE(temp_emissstr.AllLines) eq 'YES'
lymanon = STRUPCASE(temp_emissstr.CutLyman) eq 'YES'
wvlrange = [temp_emissstr.wavemin, temp_emissstr.wavemin + $
	temp_emissstr.wavenumsteps * temp_emissstr.wavestep]
verb = not KEYWORD_SET(silent)

outspec = AIA_BP_MAKE_EMISS(add_ni11 = ni11on, cut_lilike = lilikeon, all = allon, $
	he2boost = temp_emissstr.boostheii, cut_lyman = lymanon, $
	pressure = temp_emissstr.pressure, $
	abundfile = temp_emissstr.abundfile, ioneqfile = temp_emissstr.ioneqfile, $
	wvlrange = wvlrange, wvlstep = temp_emissstr.wavestep, $
	verbose = verb)
	
; Look for notes specified in the emiss.txt file and add them to the
; emissivity structure. 
numnotes = 0
notes = ''
for i = 0, rsize[2] - 1 do begin
	if result[0,i] eq 'NOTE' then begin
		numnotes = numnotes + 1
		notes = [ notes, STRTRIM(result[1,i], 2) ]
	endif
endfor
if numnotes ge 1 then begin
	notes = notes[1:numnotes]
	outspec = CREATE_STRUCT(outspec, 'notes', notes)
endif


emissstr = outspec.total
fullemissstr = outspec


; If requested, write the output to a .dat file
if KEYWORD_SET(write) then begin
	CD, temp_emissstr.OutFilePath, current = old_dir
	OPENW, lun, /get_lun, basefile + '_emiss.dat'
	PRINTF, lun, '; Written by AIA_BP_READ_EMISS on ' + SYSTIME()
	PRINTF, lun, '; Emissivity file from ' + filename
	PRINTF, lun, '; Emissivity is in units of ' + emissstr.units
	PRINTF, lun, 'Wavelength[A]', emissstr.logte, $
		format = '(a20, ' + STRTRIM(N_ELEMENTS(emissstr.logte),2) + 'f20.2)'
	numlines = N_ELEMENTS(emissstr.wave)
	for i = 0, numlines-1 do PRINTF, lun, emissstr.wave[i], $
		emissstr.emissivity[i,*], $
		format = '(f20.4, ' + STRTRIM(N_ELEMENTS(emissstr.logte),2) + 'e20.5)'
	FREE_LUN, lun
	SAVEGEN, file = basefile + '_emiss.genx', str = emissstr
	SAVEGEN, file = basefile + '_fullemiss.genx', str = fullemissstr
	CD, old_dir
endif


; Check for errors and inconsistencies, and write out a warning log file
; if necessary
numwarn = N_ELEMENTS(warnings)
if numwarn gt 1 then begin
	if logdir eq '1' then logdir = temp_emissstr.OutFilePath
	warnings = warnings[1:numwarn-1]	;	drop the dummy first element
	if not KEYWORD_SET(silent) then for i = 0, numwarn-2 do PRINT, warnings[i]
	if KEYWORD_SET(logdir) then begin
		sepfile = STRSPLIT(/extract, filename, '.')
		warnfile = AIA_BP_DATE_STRING(/time) + '_' + basefile + '_warn.txt'
		CD, logdir, current = old_dir
		OPENW, loglun, /get_lun, warnfile
		for i = 0, numwarn-2 do PRINTF, loglun, warnings[i]
		FREE_LUN, loglun
		CD, old_dir
	 endif
endif 

CD, orig_dir

end