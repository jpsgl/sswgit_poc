function AIA_BP_PARSE_EFFAREA, oldfullresp, $
	smallresp, $
	sampleutc = sampleutc, resptable = resptable, version = version, $
	evenorm = evenorm, timedepend = timedepend, ver_date = ver_date, $
	channels = channels, dn = dn, uv = uv

;+
;
; $Id: aia_bp_parse_effarea.pro,v 1.14 2013/02/04 23:48:37 boerner Exp $
;
; Helper routine called by AIA_GET_RESPONSE to reformat effective area 
; structure, convert to DN units, select a subset of channels, etc.
; 
; INPUTS:
;	oldfullresp	--	Full response structure as read in from .genx file
;
; OUTPUTS:
;	smallresp	--	Reduced response structure 
;
; RETURNS:
;	newfullresp	--	Full response structure with specified channels, units, reduced
;					set of keywords, etc.
;
; KEYWORDS:
;	resptable	--	a string array holding data read in from the AIA response table .txt file
;	sampleutc	--	CCSDS-formatted time string describing the time for which the response
;					should be calculated
;	version		--	integer indicating the version number of the response
;	channels	--	string array with the names of the channels to be included in
;					the output response structures
;	ver_date	--	a string indicating the version date of the response table (used to update
;					the time-dependent correction). If not set, defaults to the null string
;
;	dn			--	if set, then the output units are cm2 DN phot-1; by default, they 
;					are just cm2
;
;-

if N_ELEMENTS(version) eq 0 then version = 2
uv = KEYWORD_SET(uv)
otags = TAG_NAMES(oldfullresp)
ntags = N_TAGS(oldfullresp)

;+++++++++++++++++++++++++++++++++
; Add the informational fields from
; the old response structure to
; the new response structure
;--------------------------------
newfullresp = CREATE_STRUCT('name', oldfullresp.name, $
							'version', FIX(version), $
							'date', AIA_BP_UTC2DATE_STRING(sampleutc), $
							'filedate', oldfullresp.date)

;+++++++++++++++++++++++++++++++++
; Decide which channels will be
; included, and set up the field
; appropriately
;--------------------------------
if N_ELEMENTS(channels) gt 0 then begin
	cindex = WHERE_ARR(oldfullresp.channels, STRUPCASE(channels))
endif else begin
	channels = oldfullresp.channels
	cindex = INDGEN(N_ELEMENTS(channels))
endelse
newfullresp = CREATE_STRUCT(newfullresp, 'CHANNELS', oldfullresp.channels[cindex])

;++++++++++++++++++++++++++++++++
; Generate substructure
; describing corrections to the
; instrument calibration
;--------------------------------
if N_TAGS(resptable) gt 0 then begin
	corr_str = AIA_BP_CORRECTIONS(resptable, sampleutc, oldfullresp, ver_date, /effarea, uv = uv)
	if KEYWORD_SET(evenorm) then begin
		corr_str.evenorm_applied = 'YES'
		evechan = STRMID(corr_str.evenorm.channels, 0, 3)
		evescale = corr_str.evenorm.aia_over_eve
	endif
	if KEYWORD_SET(timedepend) then	begin
		corr_str.time_applied = 'YES'
		timestr = corr_str.time
		timechan = STRMID(timestr.channels, 0, 3)
		timescale = timestr.ea_sample_over_t0	
	endif
endif else begin
	; For some reason, the response table information was not passed in. 
	; Silently ignore all requests to include corrections
	evechan = channels
	evescale = DBLARR(N_ELEMENTS(channels)) + 1d
	timechan = channels
	timescale = DBLARR(N_ELEMENTS(channels)) + 1d
	if KEYWORD_SET(evenorm) or KEYWORD_SET(timedepend) then begin
		BOX_MESSAGE, ['AIA_BP_PARSE_EFFAREA: No input response table', 'Cannot apply evenorm or timedepend corrections']
		evenorm = 0
		timedepend = 0
	endif
endelse



;+++++++++++++++++++++++++++++++++
; Loop through each channel and
; add it to the new response 
; structure, correcting the units
; and format of the substructure
; as you go
;--------------------------------
for i = 0, N_ELEMENTS(cindex)-1 do begin
	thischan = STRUPCASE(channels[i])
	thischanstr = oldfullresp.(WHERE(otags eq thischan))
	thisfullstr = STR_SUBSET(oldfullresp.(WHERE(otags eq thischan)+1), $
		'name,date,wave,effarea,units,geoarea,scale,platescale,wavemin,wavestep,wavenumsteps,filtersincludemesh,contamthick,usecontam,usephottoelec,usephottodn,elecperev,elecperdn,useerror,extrapolatecomponents,ent_filter,fp_filter,primary,secondary,ccd,contam,cross_area')

	if i eq 0 then begin
		wave = thischanstr.wave
		platescale = thischanstr.platescale
		units = thischanstr.units
		smallresp = CREATE_STRUCT('name', newfullresp.name, 'version', newfullresp.version, 'date', newfullresp.date, $
			'channels', channels, 'wave', wave)
	endif
	
	effarea = thisfullstr.effarea
	if KEYWORD_SET(dn) then begin
		evperphot = 12398. / wave
		elecperphot = (evperphot * thisfullstr.elecperev) > 1
		elecperdn = thisfullstr.elecperdn
		effarea = effarea * elecperphot / elecperdn
		units = 'cm^2 DN phot^-1'
		thisfullstr.usephottoelec = 1
		thisfullstr.usephottodn = 1
	endif
	if KEYWORD_SET(evenorm) then begin
		evechan_id = WHERE(STRMID(thischan, 0, 3) eq evechan)
		this_evescale = evescale[evechan_id]
		effarea = effarea * this_evescale[0]
	endif
	if KEYWORD_SET(timedepend) then begin
		timechan_id = WHERE(STRMID(thischan, 0, 3) eq timechan)
		this_timescale = timescale[timechan_id]
		effarea = effarea * this_timescale[0]
	endif
	
	shortstr = {name : thischanstr.name, wave : wave, ea : effarea, $
			platescale : platescale, units : units}
	thisfullstr.effarea = effarea
	thisfullstr.units = units
	
	if i eq 0 then begin
		all = effarea 
		smallrespstr = CREATE_STRUCT(thischan, shortstr)
	endif else begin
		all = [[all], [effarea]]
		smallrespstr = CREATE_STRUCT(smallrespstr, thischan, shortstr)
	endelse
	
	newfullresp = CREATE_STRUCT(newfullresp, thischan, shortstr, $
		thischan + '_full', thisfullstr)
		
endfor
	
smallresp = CREATE_STRUCT(smallresp, 'all', TRANSPOSE(all), 'platescale', platescale, 'units', units, smallrespstr)

; Add notes and corrections to the end of the structures
if N_TAGS(resptable) gt 0 then begin
	newfullresp = CREATE_STRUCT(newfullresp, 'corrections', corr_str)
	smallresp = CREATE_STRUCT(smallresp, 'corrections', corr_str)
endif
wherenotes = WHERE(otags eq 'NOTES', hasnotes)
if hasnotes gt 0 then newfullresp = CREATE_STRUCT(newfullresp, 'notes', oldfullresp.notes)

RETURN, newfullresp

end
