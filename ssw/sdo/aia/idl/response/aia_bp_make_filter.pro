pro AIA_BP_MAKE_FILTER, filename, $
	filtstr, $
	write = write

;+
;
; $Id: aia_bp_make_filter.pro,v 1.1 2010/06/15 22:58:53 boerner Exp $
;
; $Log: aia_bp_make_filter.pro,v $
; Revision 1.1  2010/06/15 22:58:53  boerner
; Initial version
;
;
; Reads in a filt.txt file and returns an IDL array containing the filter transmission
; (while also writing a comp.dat file for the filter)
;
; INPUT:	filename-	the name of the filt.txt file to be read (including 
;						the path, if necessary, from the current directory; 
;						generally, no path should be used, and the function
;						should be run from the appropriate directory)
;
; OUTPUTS:	filtstr-	a structure giving the filter transmission and some data used
;						in generating the filter
;
; KEYWORDS:	/write-		if set, an output comp.dat file for the filter is created.
;
;-


errors = ''
warnings = ''

result = RD_TFILE(filename, 2, /nocomm, /compress, delim = '--')
rsize = SIZE(result)


; Set up the filter information structure and fill it with the defaults
filtstr_template = {Name : 'al1500', OutFilePath : '/Volumes/disk2/data/bandpass/scratch/', $
					InFilePath : '/Volumes/disk2/data/bandpass/scratch/pbimd_aia_materials/', $
					WaveMin : 1.0, WaveStep : 1.0, WaveNumSteps : 400l, $
					WaveLog : 0, UseWaveFile : 0, WaveFile  : 'TEMPLATE_wave.txt', $
					FiltersIncludeMesh : 0, MeshTrans : 0.82, OpenArea : 0.}


; Go through each field in the filter information structure and check to
; see whether the filt.txt file sets its value. If not (or if it is specified
; redundantly), use the default and print a warning
filtstr = AIA_BP_FILE2TEMPLATE(filtstr_template, result,	warnings = warnings)

; Make a wavelength grid for the filter based on the parameters specified
; in the file. The transmission will be interpolated to this grid
if filtstr.UseWaveFile then begin
	CD, filtstr.InFilePath, current = old_dir
	wavegrid = AIA_BP_READ_WAVE(filtstr.wavefile)
	CD, old_dir
endif else begin
	wavegrid = FINDGEN(filtstr.WaveNumSteps)
	wavegrid = wavegrid * filtstr.WaveStep
	wavegrid = wavegrid + filtstr.WaveMin
	if filtstr.WaveLog then wavegrid = 10.^wavegrid
endelse

transmission = FLTARR(N_ELEMENTS(wavegrid)) + 1.0
if filtstr.FiltersIncludeMesh then transmission = transmission * filtstr.MeshTrans

; Add the top-level result fields to the output structure
filtstr = CREATE_STRUCT('Wave', wavegrid, 'Transmittivity', transmission, $
	'Filename', filename, 'Date', SYSTIME(), filtstr)	

; Look for layers specified in the filt.txt file and add them to the
; filter response structure. As you go, calculate the total filter transmission
numlayer = 0
for i = 0, rsize[2] - 1 do begin
	tagsep = STR_SEP(result[0,i], ' ')
	if STRUPCASE(tagsep[0]) eq 'LAYER' then begin
	
		numlayer = numlayer + 1
		layername = tagsep[1]
		layerthick = tagsep[2]
		layerfile = result[1,i]

		CD, filtstr.InFilePath, current = old_dir
		layerdat = AIA_BP_READ_COMP(layerfile)
		CD, old_dir

		; Check component response for possible errors/mis-specifications		
		if MIN(layerdat[0,*]) gt MIN(wavegrid) then begin
			PRINT, 'Layer ' + layername + ' fixed to 100% at 0 A and extrapolated to short wavelengths'
			layerdat = [[0., 0.], [layerdat]]
		endif
		if MAX(layerdat[0,*]) lt MAX(wavegrid) then begin
			PRINT, 'Layer ' + layername + ' extrapolated to long wavelengths'
		endif

		ilayerdat = INTERPOL(layerdat[1,*], layerdat[0,*], filtstr.Wave)
		ilayerdat = EXP(layerthick * ilayerdat)

		if MAX(ilayerdat) gt 1. then begin
			PRINT, 'Layer ' + layername + ' greater than 100% efficient'
		endif
		if MIN(ilayerdat) lt 0. then begin
			PRINT, 'Layer ' + layername + ' less than 0% efficient'
		endif

		filtstr = CREATE_STRUCT(filtstr, layername, ilayerdat)		

		; Finish up and include results in chanstr
		transmission = transmission * ilayerdat

	endif
endfor
filtstr.Transmittivity = transmission + filtstr.OpenArea

; If requested, write out the response in a dat file
if KEYWORD_SET(write) then begin
	CD, current = old_dir, filtstr.OutFilePath
	OPENW, lun, /get_lun, filtstr.Name + '_comp.dat'
	PRINTF, lun, '# Written by AIA_BP_MAKE_FILTER on ' + SYSTIME()
	PRINTF, lun, '# Transmission for ' + filtstr.Name
	PRINTF, lun, '#', 'Wave [Ang]', 'Transmittivity', format = '(a1, a14, a15)'
	for i = 0l, N_ELEMENTS(filtstr.wave)-1 do $
		PRINTF, lun, '', filtstr.wave[i], filtstr.transmittivity[i], $
		format = '(a1, f14.2, e15.5)'
	FREE_LUN, lun
	CD, old_dir
endif


end