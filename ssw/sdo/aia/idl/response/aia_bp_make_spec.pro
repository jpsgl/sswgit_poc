function AIA_BP_MAKE_SPEC, wave, logte, emissivity, demstr, $
	spec_matrix, tgrid
	
;+
;
; $Id: aia_bp_make_spec.pro,v 1.1 2010/06/24 17:02:32 boerner Exp $
;
; $Log: aia_bp_make_spec.pro,v $
; Revision 1.1  2010/06/24 17:02:32  boerner
; initial version
;
;
; This routine is the computational guts of AIA_BP_READ_SPEC
;
; INPUTS: 
;	wave
;	logte
; 	emissivity
;	demstr
;
; RETURNS:
;	spectrum
;
; OUTPUT:
;	spec_matrix
;	tgrid
;
;-



demtlim = LIMITS(demstr.logte)

interpdem = INTERPOL(demstr.logdem, demstr.logte, logte)

tempstep = logte[1] - logte[0]
overlaptemp = WHERE(logte ge demtlim[0] and logte le demtlim[1])

reformdem = REFORM(10.^interpdem[overlaptemp] * 10.^logte[overlaptemp] * ALOG(10.^tempstep))

spectrum = TRANSPOSE(emissivity[overlaptemp,*]) # reformdem

; Generate the more detailed spectral contribution matrix
tgrid = logte[overlaptemp]
numtemp = N_ELEMENTS(tgrid)
numwave = N_ELEMENTS(wave)
dem_matrix = REBIN(reformdem, numtemp, numwave)
spec_matrix = emissivity[overlaptemp,*] * dem_matrix

; We seem to get the same result for the spectrum if we do
;	spectrum = TOTAL(spec_matrix, 1)

RETURN, spectrum

end