function AIA_BP_FILE2TEMPLATE, templatestr, data, $
	warnings = warnings

;+
;
; $Log: aia_bp_file2template.pro,v $
; Revision 1.1  2010/05/28 22:31:25  boerner
; Initial version
;
;
; Helper routine for AIA_BP_READ_AAAA.PRO programs. 
; 
; Takes data read in from a .txt makefile by one of those routines, and 
; parses it according to the template structure also passed in by that routine.
;
; INPUTS: 
;	tempatestr	--	a structure giving the name and default value of all of the
; 					parameters that might be specified in the makefile
;
;	data		--	a string array containing the data read in from a .txt file
;					which is to be fit into a structure of the same format as 
;					the template
;
; KEYWORDS:
;	warnings	--	a string array containing any warning messages generated
;					during the parsing of the filename. Can be passed in, in
;					which case new warnings are appended to the input array
;
; RETURNS:
;	resultstr	--	a structure with the same format as the templatestr, with
;					values set according to the information contained in data
;
;-

resultstr = templatestr

ntags = N_TAGS(templatestr)
tagnames = TAG_NAMES(templatestr)

; Go through each field in the template structure and check to
; see whether the data read in from the txt file sets the value
; of that field. If not (or if it is specified redundantly), use
; the default and print a warning
for i = 0, ntags-1 do begin
	thistag = tagnames[i]
	tagind = WHERE(STRUPCASE(data[0,*]) eq thistag, hastag)
	case hastag of
		0	:	begin
					thisval = templatestr.(i)
					warnstring = 'Missing tag: ' + thistag + ' set to ' + STRTRIM(thisval,2)
					warnings = [warnings, 'AIA_BP_FILE2TEMPLATE...WARNING! ' + warnstring]
				end
		1	:	begin
					thisval = data[1,tagind[0]]
					resultstr.(i) = thisval
				end
		else:	begin		;	Currently, this is the same as not specifying a value
					thisval = templatestr.(i)
					warnstring = 'Redundant tags: ' + thistag + ' set to ' + STRTRIM(thisval,2)
					warnings = [warnings, 'AIA_BP_FILE2TEMPLATE...WARNING! ' + warnstring]
				end
	endcase
endfor

RETURN, resultstr

end
