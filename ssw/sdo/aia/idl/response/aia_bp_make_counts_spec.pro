function AIA_BP_MAKE_COUNTS_SPEC, areastr, specstr, $
	wcontrib, wgrid, throughspec, $
	_extra = extra

;+
;
; $Log: aia_bp_make_counts_spec.pro,v $
; Revision 1.2  2010/08/10 22:46:07  boerner
; Initial version
;
; Revision 1.1  2010/06/24 17:02:32  boerner
; initial version
;
;
; Given an instrument wavelength response and a spectrum, returns a predicted
; count rate. 
;
; INPUTS:
;	areastr--	a channel effective area structure (like the substructures in
; 				the effective area structure returned by aia_get_response(/area),
;				with .ea, .platescale and .wave fields).
;
;	specstr	--	(optional) a spectrum structure (like those made by aia_bp_default_spectrum() ),
;				with .wave and .spec fields.
;
; OUTPUTS (optional):
;	wcontrib--	a vector giving the counts due to each wavelength bin (the detected
;				spectrum), in counts/sec/pix (NOT per Angstrom; the step size
;				is included)
;
;	wgrid	--	a vector giving the wavelength bins corresponding to wcontrib
;
;	throughspec--	a vector giving the detected spectrum on the same wavelength
;					grid as wcontrib (in units of counts/sec/sr/A)
;
; RETURNS:
;	fluence	--	a count rate, generally in counts/s/pix
;
; (NOTE that the units on the returned fluence, and on the wcontrib and throughspec, 
; are those in the input areastr, so if that includes the DN/phot correction then
; the results will as well)
;
; KEYWORDS:
; 	Accepts the keyword inputs used by aia_bp_default_spectrum (e.g. /ar or /qs) 
; and passes them through if the input specstr is not specified (so if you just 
; want to use the default active region spectrum, leave specstr blank
; and set the /ar keyword).
;
;-

if N_ELEMENTS(specstr) eq 0 then begin
	specstr = AIA_BP_DEFAULT_SPECTRUM(_extra = extra)
endif

iresponse = INTERPOL(areastr.ea, areastr.wave, specstr.wave)

lowave = WHERE(specstr.wave lt MIN(areastr.wave), numlow)
if numlow gt 0 then iresponse[lowave] = 0.
hiwave = WHERE(specstr.wave gt MAX(areastr.wave), numhi)
if numhi gt 0 then iresponse[hiwave] = 0.

numwave = N_ELEMENTS(specstr.wave)
wavestep = FLTARR(numwave)								;	A / bin
for i = 0l, numwave-2 do wavestep[i] = specstr.wave[i+1] - specstr.wave[i]
wavestep[numwave-1] = wavestep[numwave-2]				
platescale = areastr.platescale							;	sr / pixel

wgrid = specstr.wave
throughspec = specstr.spec * iresponse					;	counts / s / sr / A
wcontrib = throughspec * wavestep * platescale			;	counts / s / pix / bin

fluence = TOTAL(wcontrib)								;	counts / s / pix

RETURN, fluence

end
