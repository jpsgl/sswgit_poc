pro AIA_BP_READ_TRESP, filename, $
	trespstr, fulltrespstr, $
	warnings, $
	write = write, logdir = logdir, silent = silent
	
;+
;
; $Id: aia_bp_read_tresp.pro,v 1.3 2010/08/03 00:29:32 boerner Exp $
;
; $Log: aia_bp_read_tresp.pro,v $
; Revision 1.3  2010/08/03 00:29:32  boerner
; Use date label; use AIA subroutines
;
; Revision 1.2  2010/06/21 23:34:14  boerner
; Pass countinuts from read to make tresp
;
; Revision 1.1  2010/06/15 22:58:53  boerner
; Initial version
;
;
; INPUTS:
;	filename	-	string giving the name of a _tresp.txt file to be used as
;					the makefile for the temperature response. (See
;					TEMPLATE_tresp.txt for an example).
;
; OUTPUTS:
;	trespstr	-	a simple structure giving the resulting T response. Has 
;						4 fields: name, logte, tresp and units
;
;	fulltrespstr-	a structure giving the T response and the full T-lambda
;					response matrix
;
;	warnings	-	a string array containing any warnings produced during the 
;					calculation
;
; KEYWORDS:
;	/write		-	if set, the trespstr and fulltrespstr are written to 
;					.genx files, and an ASCII file containing the tresponse
;					is saved as well.
;
;	/silent		-	if set, warnings are suppressed (though they are still 
;					passed back out of the routine)
;
;	logdir=		-	can be set to a path, in which case the warnings are also
;					written to a text file at that path
;
;-


errors = ''
warnings = ''

if not KEYWORD_SET(logdir) then logdir = 0
if not KEYWORD_SET(silent) then silent = 0

; Check that the filename format is AAAAA_tresp.txt
AIA_BP_PARSE_FILENAME, filename, 'tresp', shortfile = shortfile, $
	basefile = basefile, warnings = warnings, path=filepath
CD, filepath, current = orig_dir

	
result = RD_TFILE(filename, 2, /nocomm, /compress, delim = '--')
rsize = SIZE(result)


; Set up the tresp information structure and fill it with the defaults
trespstr_template = {Name : '', $
					EmissGenxFile : '', $
					ChanGenxFile : '', $
					InstGenxFile : '', $
					EmissDatFile : '', $
					ChanDatFile : '', $
					InstDatFile : '' }
if KEYWORD_SET(write) then begin
	trespstr_template = CREATE_STRUCT(trespstr_template, 'OutFilePath', '.')
endif


; Go through each field in the tresp information structure and check to
; see whether the tresp.txt file sets its value. If not (or if it is specified
; redundantly), use the default and print a warning

temp_trespstr = AIA_BP_FILE2TEMPLATE(trespstr_template, result, $
	warnings = warnings)
if temp_trespstr.name ne '' then basefile = temp_trespstr.name


; Look for notes specified in the tresp.txt file and add them to the
; spectrum structure. 
numnotes = 0
notes = ''
for i = 0, rsize[2] - 1 do begin
	if result[0,i] eq 'NOTE' then begin
		numnotes = numnotes + 1
		notes = [ notes, STRTRIM(result[1,i], 2) ]
	endif
endfor
if numnotes gt 1 then begin
	notes = notes[1:numnotes-1]
	temp_trespstr = CREATE_STRUCT(temp_trespstr, 'notes', notes)
endif


; Read in the emissivity data
if temp_trespstr.EmissGenxFile eq '' then begin
	if temp_trespstr.EmissDatFile eq '' then begin
		warnstring = 'No Emissivity file (.dat or .genx) specified!'
		warnings = [warnings, 'ERROR! ' + warnstring]
		PRINT, warnstring
		PRINT, 'Stopping...'
		STOP
	endif
	emissdat = RD_TFILE(temp_trespstr.EmissDatFile, nocomment = ';', /autocol)
	emiss_size = SIZE(emissdat)
	headrow = emissdat[*,0]
	logte = FLOAT(REFORM(headrow[1:emiss_size[1]-1]))
	ewave = FLOAT(REFORM(emissdat[0, 1:emiss_size[2]-1]))
	emissivity = DOUBLE(emissdat[1:emiss_size[1]-1, 1:emiss_size[2]-1])
	units = 'photons cm^3 s^-1 sr^-1 A^-1'
endif else begin
	RESTGEN, file = temp_trespstr.EmissGenxFile, str = emissstr
	logte = emissstr.logte
	ewave = emissstr.wave
	emissivity = emissstr.emissivity
	units = emissstr.units
endelse


; Read in the channel or instrument data
hasdn = 1	;	Assume that instrument is in DN units; will check later
if (temp_trespstr.InstGenxFile eq '') and (temp_trespstr.InstDatFile eq '') then begin
	; Single channel should be specified
	numchan = 1
	if temp_trespstr.ChanGenxFile eq '' then begin
		if temp_trespstr.ChanDatFile eq '' then begin
			; No effective area supplied!
			warnstring = 'No Effective area file (.dat or .genx) specified!'
			warnings = [warnings, 'ERROR! ' + warnstring]
			PRINT, warnstring
			PRINT, 'Stopping...'
			STOP
		endif else begin
			chandat = RD_TFILE(temp_trespstr.ChanDatFile, nocomment = '#', /autocol)
			wave = FLOAT(chandat[0,*])
			ea = FLOAT(chandat[1,*])
			name = ''
			scale = 1
		endelse
	endif else begin
		RESTGEN, file = temp_trespstr.ChanGenxFile, str = chanstr
		wave = chanstr.wave
		ea = chanstr.ea
		name = chanstr.name
		scale = chanstr.platescale
	endelse
endif else begin
	; Instrument (multiple channels) specified
	if temp_trespstr.InstGenxFile eq '' then begin
		instdat = RD_TFILE(temp_trespstr.InstDatFile, nocomment = '#', /autocol)
		instsize = SIZE(instdat)
		numchan = instsize[1]
		wave = REFORM(FLOAT(instdat[0,*]))
		ea = FLOAT(instdat[1:numchan-1, *])
		name = STRARR(numchan)
		scale = 1
	endif else begin
		RESTGEN, file = temp_trespstr.InstGenxFile, str = inststr
		numchan = N_ELEMENTS(inststr.channels)
		wave = inststr.wave
		ea = inststr.all
		name = inststr.channels
		scale = inststr.platescale
		iunits = inststr.units
		hasdn = STRPOS(STRUPCASE(iunits), 'DN')
		if hasdn lt 0 then begin
			; Instrument is not in DN units, so tresp will be in photons -- no good!
			warnstring = 'Instrument is in photon units...should be in DN'
			warnings = [warnings, 'WARNING: ' + warnstring]
			PRINT, warnstring
		endif
	endelse
endelse
if hasdn lt 0 then countunits = 'phot' else countunits = 'DN'


; Compute temperature response functions from the given emissivity and effective
; area functions
for i = 0, numchan-1 do begin
	if numchan gt 1 then begin
		thisea = REFORM(ea[i,*]) 
		thisname = name[i]
	endif else begin
		thisea = ea
		thisname = name
	endelse
	this_trespstr = AIA_BP_MAKE_TRESP(ewave, logte, emissivity, wave, thisea, $
		thisname, scale, this_fulltrespstr, countunits)
	if scale eq 1 then begin
		this_trespstr.units = countunits + ' cm^5 s^-1 sr^-1'
		this_fulltrespstr.units = countunits + ' cm^5 s^-1 sr^-1 A^-1'
	endif
	if i eq 0 then begin
		all_trespstr = this_trespstr 
		all_fulltrespstr = this_fulltrespstr
	endif else begin
		all_trespstr = [all_trespstr, this_trespstr]
		all_fulltrespstr = [all_fulltrespstr, this_fulltrespstr]
	endelse
endfor


; Check for errors and inconsistencies, and write out a warning log file
; if necessary
numwarn = N_ELEMENTS(warnings)
if numwarn gt 1 then begin
	warnings = warnings[1:numwarn-1]	;	drop the dummy first element
	if not KEYWORD_SET(silent) then for i = 0, numwarn-2 do PRINT, warnings[i]
	if KEYWORD_SET(logdir) then begin
		if logdir eq '1' and KEYWORD_SET(write) then logdir = temp_trespstr.OutFilePath
		sepfile = STRSPLIT(/extract, filename, '.')
		warnfile = AIA_BP_DATE_STRING(/time) + '_' + sepfile[0] + '_warn.txt'
		CD, logdir, current = old_dir
		OPENW, loglun, /get_lun, warnfile
		for i = 0, numwarn-2 do PRINTF, loglun, warnings[i]
		FREE_LUN, loglun
		CD, old_dir
	 endif
endif 


; Generate the fulltrespstr and the simple trespstr
if numchan eq 0 then begin	
	; Single channel case
	trespstr = all_trespstr
	fulltrespstr = CREATE_STRUCT(all_fulltrespstr, 'Date', AIA_BP_DATE_STRING(/time))
	if numnotes gt 0 then fulltrespstr = CREATE_STRUCT(fulltrespstr, 'Notes', notes)
endif else begin
	; Instrument (multi-channel) case
	datelabel = AIA_BP_DATE_STRING(/time)
	trespstr = CREATE_STRUCT('Name', temp_trespstr.name, $
		'Date', datelabel, $
		'Channels', all_trespstr.name, $
		'Logte', all_trespstr[0].logte, $
		'All', all_trespstr.tresp, $
		'Units', all_trespstr[0].units)
	fulltrespstr = CREATE_STRUCT('Name', temp_trespstr.name, $
		'Date', datelabel, $
		'Channels', all_fulltrespstr.name, $
		'Logte', all_fulltrespstr[0].logte, $
		'Wave', all_fulltrespstr[0].wave, $
		'Tresp', all_fulltrespstr.tresp, $
		'TWresp', all_fulltrespstr.full_tresp, $
		'TUnits', all_fulltrespstr[0].units, $
		'TWunits', all_fulltrespstr[0].full_units )		
	for i = 0, numchan - 1 do begin
		if all_trespstr[i].name eq '' then begin
			thisname = 'Chan' + STRTRIM(i,2)
		endif else begin
			thisname = all_trespstr[i].name
		endelse
		trespstr = CREATE_STRUCT(trespstr, thisname, all_trespstr[i])
		fulltrespstr = CREATE_STRUCT(fulltrespstr, thisname, all_fulltrespstr[i])
	endfor
	if numnotes gt 0 then fulltrespstr = CREATE_STRUCT(fulltrespstr, 'Notes', notes)
endelse


; If requested, write out the response in a dat and genx file. The files are 
; written in the directory specified by the OutFilePath field (defaults to .), 
; and are names according to the name of the input file
if KEYWORD_SET(write) then begin
	CD, current = old_dir, temp_trespstr.OutFilePath
	OPENW, lun, /get_lun, basefile + '_tresp.dat'
	PRINTF, lun, '# Written by AIA_BP_READ_TRESP on ' + SYSTIME()
	PRINTF, lun, '# T Response for ' + trespstr.Name
	PRINTF, lun, '# Units are ' + trespstr.Units
	if numchan eq 0 then begin
		PRINTF, lun, '#', 'Log_10(T)', 'Response', format = '(a1, a14, a15)'
		for i = 0l, N_ELEMENTS(trespstr.logte)-1 do $
			PRINTF, lun, '', trespstr.logte[i], trespstr.tresp[i], format = '(a1, f14.3, e15.5)'
		FREE_LUN, lun
	endif else begin
		PRINTF, lun, '#', 'Log_10(T)', trespstr.channels, $
			format = '(a1, a14, ' + STRTRIM(numchan,2) + 'a15)'
		for i = 0l, N_ELEMENTS(trespstr.logte)-1 do $
			PRINTF, lun, '', trespstr.logte[i], trespstr.all[i,*], $
				format = '(a1, f14.3, ' + STRTRIM(numchan, 2) + 'e15.5)'
		FREE_LUN, lun
	endelse
	SAVEGEN, file = basefile + '_tresp.genx', str = trespstr
	SAVEGEN, file = basefile + '_fulltresp.genx', str = fulltrespstr
	CD, old_dir
endif

CD, orig_dir
 
end



