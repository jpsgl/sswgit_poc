pro AIA_BP_WRITE_ERROR_TABLE, $
	write = write, silent = silent

;+
;
; $Log: aia_bp_write_error_table.pro,v $
; Revision 1.1  2012/02/10 01:39:22  boerner
; Initial version
;
;
;
;-

wvls = [94, 131, 171, 193, 211, 304, 335, 1600, 1700, 4500]
stringwvls = STRTRIM(wvls, 2)
numwave = N_ELEMENTS(wvls)
nullwvls = FLTARR(numwave)

date = STRARR(numwave) + ANYTIM2UTC(/ccsds, RELTIME(/now))

t_start = STRARR(numwave) + ANYTIM2UTC(/ccsds, '1-may-2010T00:00:00Z')

t_stop = STRARR(numwave) + ANYTIM2UTC(/ccsds, '1-may-2050T00:00:00Z')

ver_num = INTARR(numwave) + 2

filters = ['_THIN', '_THICK', '_OPEN']
wave_strs = stringwvls 
wave_strs[0:6] = wave_strs[0:6] + filters[0]		;	currently only use thin filters

aiaresp = AIA_GET_RESPONSE(/full, /dn, /area)
aiaresp_uv = AIA_GET_RESPONSE(/full, /dn, /area, /uv)
resptags = TAG_NAMES(aiaresp)
resptags_uv = TAG_NAMES(aiaresp_uv)
for i = 0, numwave - 1 do begin
	if wvls[i] gt 1000 then begin
		chanid = WHERE(resptags_uv eq 'A' + stringwvls[i] + '_FULL', haschan)
		fullstr = aiaresp_uv.(chanid)
	endif else begin
		chanid = WHERE(resptags eq 'A' + stringwvls[i] + '_FULL', haschan)
		fullstr = aiaresp.(chanid)
	endelse
	thiselecperpht = (12398d / wvls[i] * fullstr.elecperev) > 1.
	thisdnperpht = thiselecperpht / fullstr.elecperdn
	if i eq 0 then dnperpht = thisdnperpht else dnperpht = [dnperpht, thisdnperpht]
endfor

compress = 1./(wvls / 171. / 4. / 3.6)	;	Standard lookup table bin width is set to ~1/4 the shot noise 
compress[8] = compress[8] / 4			;	assuming 171 A photons; 1700 and 4500 use 4x and 8x
compress[9] = compress[9] / 8			;	wider bins

calerr = [0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.5, 0.5, 0.5]

chianti = [0.5, 0.5, 0.25, 0.25, 0.25, 0.5, 0.25, 1., 1., 1.]

resptable = AIA_BP_READ_RESPONSE_TABLE()
for i = 0, numwave - 1 do begin
	lineid = MIN(WHERE(resptable.wavelnth eq wvls[i]))
	this_eveerr = resptable[lineid].rmse * 2.
	if i eq 0 then eveerr = this_eveerr else eveerr = [eveerr, this_eveerr]
endfor

if KEYWORD_SET(write) then begin
	OPENW, lun, /get, '~/Desktop/scratch/resp_table_' + AIA_BP_DATE_STRING(/time) + '.txt'
	PRINTF, lun, 'DATE', 'T_START', 'T_STOP', 'VER_NUM', 'WAVE_STR', 'WAVELNTH', 'DNPERPHT', $
		'COMPRESS', 'CALERR', 'CHIANTI', 'EVEERR', format = '(3a26, 8a11)'
	for i = 0, numwave - 1 do PRINTF, lun, date[i], t_start[i], t_stop[i], ver_num[i], wave_strs[i], $
		wvls[i], dnperpht[i], compress[i], calerr[i], chianti[i], eveerr[i], $
		format = '(3a26, i11, a11, i11, 5f11.3)'
	FREE_LUN, lun
endif

if not KEYWORD_SET(silent) then begin
	PRINT, 'DATE', 'T_START', 'T_STOP', 'VER_NUM', 'WAVE_STR', 'WAVELNTH', 'DNPERPHT', $
		'COMPRESS', 'CALERR', 'CHIANTI', 'EVEERR', format = '(3a26, 8a11)'
	for i = 0, numwave - 1 do PRINT, date[i], t_start[i], t_stop[i], ver_num[i], wave_strs[i], $
		wvls[i], dnperpht[i], compress[i], calerr[i], chianti[i], eveerr[i], $
		format = '(3a26, i11, a11, i11, 5f11.3)'
endif

end