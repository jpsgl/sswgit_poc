pro AIA_BP_PARSE_FILENAME, filename, type, $
	shortfile = shortfile, basefile = basefile, path = filepath, extension = extension, $
	warnings = warnings

;+
;
; $Log: aia_bp_parse_filename.pro,v $
; Revision 1.4  2011/04/21 17:49:13  boerner
; Added extension removal, and option to run without error checking
;
; Revision 1.3  2010/08/17 23:26:56  boerner
; Added batch mode, null input for type
;
; Revision 1.2  2010/06/15 22:59:23  boerner
; Don't give errors on subchan files
;
; Revision 1.1  2010/05/28 22:31:25  boerner
; Initial version
;
;
; Helper routine for the aia_bp_read_AAAAA type programs. It parses an input
; file name (including the full path), separating the extension and the path,
; and then checks to see whether the filenames obey the convention AAAA_TTTT.txt 
; where AAAA is a descriptor explaining the specific identity of the file and 
; TTTT indicates the type of file. For example, AAAA could be something like 
; "080519_verify" and TTTT could be "chan" or "inst"
;
; INPUTS:
;	filename	--	the full filename to be parsed, including the path
;
;	type		--	a string; should be one of "CHAN", "INST", "SPEC", 
;					"EMISS", "TRESP". If not specified, then no checking 
;					is performed and only the output keywords are generated
;
; KEYWORDS:
;	shortfile	--	an output string giving the filename without the path
;
;	basefile	--	an output string giving the filename without the path or the
;					extension
;
;	filepath	--	an output string giving the path to the file
;
;	extension	--	an output string giving the file extension (only the part after the last .)
;
;	warnings	--	a string array containing any warning messages generated
;					during the parsing of the filename. Can be passed in, in
;					which case new warnings are appended to the input array
;
;-

if N_ELEMENTS(filename) gt 1 then begin		;	Run in batch mode; no checking, but return keyword vectors
	numfiles = N_ELEMENTS(filename)
	shorts	= STRARR(numfiles)
	bases	= STRARR(numfiles)
	paths	= STRARR(numfiles)
	exts	= STRARR(numfiles)
	for i = 0, numfiles - 1 do begin
		AIA_BP_PARSE_FILENAME, filename[i], short = thisshort, base = thisbase, path = thispath, ext = thisext
		shorts[i] = thisshort
		bases[i] = thisbase
		paths[i] = thispath
		exts[i] = thisext
	endfor
	shortfile = shorts
	basefile = bases
	filepath = paths
	extension = exts
	RETURN
endif

if N_ELEMENTS(warnings) eq 0 then warnings = ''
if N_ELEMENTS(type) eq 0 then type = ''

sepfile = STRSPLIT(/extract, filename, '/')
numpaths = N_ELEMENTS(sepfile)
nopathfile = sepfile[numpaths-1]
if numpaths gt 1 then begin
	filepath = STRJOIN(sepfile[0:numpaths-2], '/') + '/'
	if STRMID(filename, 0, 1) eq '/' then filepath = '/' + filepath
endif else begin
	filepath = '.'
endelse

; If no type is specified (implying no filename checking is desired), just
; parse the filename and return the components via the output keywords
if type eq '' then begin
	shortfile = nopathfile
	sepfile = STRSPLIT(/extract, shortfile, '.')
	if N_ELEMENTS(sepfile) eq 1 then begin
		extension = ''
		basefile = sepfile
	endif else begin
		extension = sepfile[N_ELEMENTS(sepfile)-1]
		basefile = STRJOIN(sepfile[0:N_ELEMENTS(sepfile)-2], '.')
	endelse
	RETURN
endif 


; From this point onwards, assume that the file is to be checked according to 
; AIA_BP filename conventions

; Check to make sure filename ends with .txt
shortfile = STRSPLIT(/extract, nopathfile, '.')
if N_ELEMENTS(shortfile) lt 2 then begin
	warnstring = 'No .txt extension on filename!'
	warnings = [warnings, 'AIA_BP_PARSE_FILENAME…WARNING! ' + warnstring]
	shortfile = [shortfile, 'txt']
endif
extension = shortfile[N_ELEMENTS(shortfile)-1]

; Check to make sure that the part of the name before the .txt in _TTTT
checkfile = STRSPLIT(/extract, shortfile[N_ELEMENTS(shortfile)-2], '_')
num_underscores = N_ELEMENTS(checkfile) - 1
last_string_after_underscore = checkfile[num_underscores]
if STRUPCASE(last_string_after_underscore) ne STRUPCASE(type) then begin
	; check if it is a subchan file...
	subtype = STRUPCASE('sub' + type)
	if STRUPCASE(last_string_after_underscore) ne subtype then begin
		warnstring = 'Filename does not end with _' + type
		warnings = [warnings, 'AIA_BP_PARSE_FILENAME…WARNING! ' + warnstring]
	endif
endif
if num_underscores lt 1 then begin
	warnstring = 'Filename does not have a descriptor before the _TTTT'
	warnings = [warnings, 'AIA_BP_PARSE_FILENAME…WARNING! ' + warnstring]
	checkfile = ['noname', checkfile]
	num_underscores = num_underscores + 1
endif

if type ne '' then basefile = STRJOIN(checkfile[0:num_underscores-1], '_') $
	else basefile = STRJOIN(checkfile[0:num_underscores], '_') 
	
shortfile = STRJOIN(shortfile, '.')

end
