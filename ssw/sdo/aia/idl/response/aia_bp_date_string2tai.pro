function AIA_BP_DATE_STRING2TAI, stringin

;+
;
; $Log: aia_bp_date_string2tai.pro,v $
; Revision 1.1  2010/11/09 19:36:41  boerner
; Initial version
;
;
;-

if N_ELEMENTS(stringin) gt 1 then begin		;	Run in batch mode; return vector
	numstrings = N_ELEMENTS(stringin)
	taiout = DBLARR(numstrings)
	for i = 0, numstrings - 1 do taiout[i] = AIA_BP_DATE_STRING2TAI(stringin[i])
	RETURN, taiout
endif

strlen = STRLEN(stringin)
year = STRMID(stringin, 0, 4)
month = STRMID(stringin, 4, 2)
day = STRMID(stringin, 6, 2)

if strlen gt 8 then begin
	hour = STRMID(stringin, 9, 2)
	minute = STRMID(stringin, 11, 2)
	second = STRMID(stringin, 13, 2)
endif else begin
	hour = '12'
	minute = '00'
	second = '00'
endelse

ccsds = STRJOIN([year, month, day], '-') + 'T' + STRJOIN([hour, minute, second], ':') + 'Z'

tai = ANYTIM2TAI(ccsds)

RETURN, tai

end