pro AIA_TREND_DARK, alldat, $
	init = init, test = test, build = build, tfits = tfits, $
	plot = plot, win = win, poly = poly, pstop = pstop

;+
;
; KEYWORDS:
;	/init	-	if set, then query JSOC for all daily darks from start of mission
;				to 2014-09 and write monthly files
;	/build	-	if set, then turn the monthly files into a unified genx file
;	/test	-	if set, then query the JSOC for the data from the one-day test
;				and write the results to a unified genx file
;
;
;
;-

if N_ELEMENTS(poly) eq 0 then poly = 2
tt0 = SYSTIME(/sec)
path = '/sanhome/boerner/data/aia/cal/dark/'
pathlen = STRLEN(path)
wvls = [94, 131, 171, 193, 211, 304, 335, 1600]
nwvls = N_ELEMENTS(wvls)

; retrieve and save data for test on 2014-12-01/02
if KEYWORD_SET(test) then begin
	t1 = '2014-12-01T19:00'
	t2 = '2014-12-02T20:00'
	SSW_JSOC_TIME2DATA, t1, t2, drms, files, ds = 'aia.lev1', /files_only, $
		xquery = ['aiftsid>41472', 'aiftsid<41476'], /jsoc2
	STOP
endif

; retrieve data for 2011-2013 
if KEYWORD_SET(init) then begin
	months = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec']
	for year = 2010, 2015 do begin
		fmonth = 0
		lmonth = 11
		if year eq 2010 then fmonth = 5
		if year eq 2015 then lmonth = 7
		for imnth = fmonth, lmonth do begin
			mtag = STRING(year, imnth+1, form = '(i4,i02)')
			ofile = CONCAT_DIR(path, mtag + '.genx')
			t1 = '01-' + months[imnth] + '-' + STRTRIM(year, 2)
			if imnth eq 11 then begin
				t2 = '01-jan-' + STRTRIM(year+1, 2)
			endif else begin
				t2 = '01-' + months[imnth+1] + '-' + STRTRIM(year, 2)
			endelse
			SSW_JSOC_TIME2DATA, t1, t2, drms, ds = 'aia.lev1', xquery = 'aiftsid=41472', /jsoc2
			SAVEGEN, file = ofile, drms
			PRINT, mtag, SYSTIME(/sec) - tt0, form = '(a10, f10.1)'
		endfor
	endfor
endif

; Build a unified genx file from the date files currently stored
if KEYWORD_SET(build) then begin
	ff = FILE_SEARCH(CONCAT_DIR(path, '2*.genx'))
	filedates = FIX(STRMID(ff, pathlen, 6))
	filesort = SORT(filedates)
	fullfile = 'u_' + STRTRIM(MIN(filedates), 2) + '-' + STRTRIM(MAX(filedates), 2) + '.genx'
	for i = 0, N_ELEMENTS(ff) - 1 do begin
		RESTGEN, file=ff[i], thisdrms
		if N_ELEMENTS(alldat) eq 0 then alldat = thisdrms else alldat = [alldat, thisdrms]
		PRINT, i
	endfor
	SAVEGEN, file = CONCAT_DIR(path, fullfile), alldat
endif

; read the data
if N_ELEMENTS(alldat) eq 0 then begin
	fullfile = FILE_SEARCH(CONCAT_DIR(path, 'u*.genx'))
	RESTGEN, file = fullfile, alldat
endif

; Calculate temperature dependence
tfits = FLTARR(nwvls, poly+1)
for i = 0, nwvls - 1 do begin
	ind = WHERE(alldat.wavelnth eq wvls[i], numin)
	thisdat = alldat[ind]
	thiscam = MEAN(thisdat.camera)
	medint = MEDIAN(thisdat.int_time)
	gooddat = WHERE(thisdat.tempccd ge -75 and thisdat.tempccd le -65 and $
		ABS(thisdat.int_time - medint) lt 0.1, numgood)
	tfit = POLY_FIT(thisdat[gooddat].tempccd, thisdat[gooddat].datamean, poly, yfit = fitmean)
	tfits[i,*] = REFORM(tfit)
endfor

; Make a plot, if requested
if KEYWORD_SET(plot) then begin
	
	; Set up for plotting
	TVLCT, rr, gg, bb, /get
	PB_SET_LINE_COLOR
;	oldpmulti = !p.multi
;	!p.multi = [0,1,2]
	if N_ELEMENTS(win) gt 0 then WDEF, win, 1000, 1000
		
	for i = 0, nwvls - 1 do begin
		ind = WHERE(alldat.wavelnth eq wvls[i], numin)
		thisdat = alldat[ind]
		thiscam = MEAN(thisdat.camera)
		medint = MEDIAN(thisdat.int_time)
		gooddat = WHERE(thisdat.tempccd ge -75 and thisdat.tempccd le -65 and $
			ABS(thisdat.int_time - medint) lt 0.1, numgood)
		
		case thiscam of
			1		:	begin
				tmin = -75
				tmax = -69
			end
			2		:	begin
				tmin = -72
				tmax = -66
			end
			3		:	begin
				tmin = -73
				tmax = -67
			end
			4		:	begin
				tmin = -75
				tmax = -69
			end
			else	:	begin
				PRINT, 'wtf?'
			end
		endcase

		; Plot datamean as a function of temperature
		PLOT, thisdat.tempccd, thisdat.datamean, psym = 4, chars = 1.5, thick = 2, $
			yrange = [-0.25, 0.25], xrange = [tmin, tmax], /ystyle, xtit = 'T_CCD', $
			ytitle = 'Datamean [DN]', /xstyle, pos = [0.06, 0.52, 0.5, 0.95], $
			title = 'Darks [' + STRTRIM(wvls[i],2) + ']'
		tfit = POLY_FIT(thisdat[gooddat].tempccd, thisdat[gooddat].datamean, poly, yfit = fitmean)
		OPLOT, thisdat[gooddat].tempccd, fitmean, col = 2
		
		; Plot histogram of datamean with and without temperature correction
		bin = 0.01
		rhist = HISTOGRAM(thisdat.datamean, min = -0.25 - bin/2., bin = bin, $
			max = 0.25 + bin/2., locations = histx_start)
		dhist = HISTOGRAM(thisdat.datamean - fitmean, min = -0.25-bin/2., bin = bin, $
			max = 0.25 + bin/2., locations = dhistx_start)
		PLOT, histx_start + bin / 2., rhist, psym = 10, chars = 1.5, $
			xtitle = 'Datamean [DN]', ytit = 'Frequency', pos = [0.56, 0.52, 0.97, 0.95], $
			yrange = [0, MAX([rhist, dhist])], /noerase, /xstyle, $
			title = 'Dark Histogram [' + STRTRIM(wvls[i],2) + ']'
		OPLOT, dhistx_start + bin / 2., dhist, psym = 10, col = 2
		rawstr = STRING('Raw [', MEAN(thisdat.datamean), '+/-', $
			STDEV(thisdat.datamean), 'DN]', form = '(a5,f6.3,a3,f5.2,a3)')
		corrstr = STRING('Tfit [', MEAN(thisdat.datamean - fitmean), '+/-', $
			STDEV(thisdat.datamean - fitmean), 'DN]', form = '(a6,f6.3,a3,f5.2,a3)')
		LEGEND, pos = 10, char = 1.5, col = [255, 2], [rawstr, corrstr]
		
		; Plot datamean as a function of time
		UTPLOT, thisdat.t_obs, thisdat.datamean, psym = 4, chars = 1.5, $
			ytitle = 'Datamean [DN]', /xstyle, yrange = [-0.25, 0.25], /ystyle, $
			pos = [0.06, 0.06, 0.97, 0.46], /noerase
		OUTPLOT, thisdat[gooddat].t_obs, fitmean, col = 2, psym = 3
		PB_WIN2PNG, !desk + 'scratch/aia_' + STRING(wvls[i], form = '(i04)') + '_dark.png'
		if KEYWORD_SET(pstop) then STOP
	endfor

	; Put plotting back the way it was	
	TVLCT, rr, gg, bb, /get
;	oldpmulti = !p.multi

	
endif

end