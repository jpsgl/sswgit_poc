function AIA_BP_READ_RESPONSE_TABLE, tablefile, $
	silent = silent

;+
;
; $Log: aia_bp_read_response_table.pro,v $
; Revision 1.2  2013/07/10 18:34:53  boerner
; Update to remove wildcards for Windows compatibility
;
; Revision 1.1  2011/10/03 23:21:06  boerner
; Initial version
;
; 
; INPUTS:
;	tablefile	--	string giving the name of the _response_table.txt file to be read in
;
; RETURNS:
;	tablestr	--	a structure array containing the data from the response table file
;
;-

silent = KEYWORD_SET(silent)
if N_ELEMENTS(tablefile) eq 0 then begin
	tablefile = GET_LOGENV('AIA_RESPONSE_DATA') + '/aia_V2_response_table.txt'
endif

hastable = FILE_EXIST(tablefile)
if hastable then begin
	temp_tablefile = STRJOIN(STRSPLIT(/extract, tablefile, '*'))
	tabledat = RD_TFILE(temp_tablefile, 14)
endif else begin
	if not silent then box_message, 'AIA_BP_READ_RESPONSE_TABLE: Missing file ' + tablefile
	RETURN, 0
endelse

table_template = {Date : '', t_start : '', t_stop : '', ver_num : 0, $
	wave_str : '', wavelnth : 0, eperdn : 0d, dnperpht : 0d, eff_area : 0d, eff_wvln : 0d, $
	effa_p1 : 0d, effa_p2 : 0d, effa_p3 : 0d, rmse : 0d}

tablesize = SIZE(tabledat)
for i = 1, tablesize[2] - 1 do begin
	thisline = table_template
	thisline.date 		= tabledat[0,i]
	thisline.t_start	= tabledat[1,i]
	thisline.t_stop		= tabledat[2,i]
	thisline.ver_num	= tabledat[3,i]
	thisline.wave_str	= tabledat[4,i]
	thisline.wavelnth	= tabledat[5,i]
	thisline.eperdn		= tabledat[6,i]
	thisline.dnperpht	= tabledat[7,i]
	thisline.eff_area	= tabledat[8,i]
	thisline.eff_wvln	= tabledat[9,i]
	thisline.effa_p1	= tabledat[10,i]
	thisline.effa_p2	= tabledat[11,i]
	thisline.effa_p3	= tabledat[12,i]
	thisline.rmse		= tabledat[13,i]
	if i eq 1 then tablestr = thisline else tablestr = [tablestr, thisline]
endfor

RETURN, tablestr

end