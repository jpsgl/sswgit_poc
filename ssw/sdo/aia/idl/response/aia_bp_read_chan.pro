pro AIA_BP_READ_CHAN, filename, $
	chanstr, fullchanstr, $
	warnings, $
	write = write, logdir = logdir, silent = silent, $
	official = official

;+
;
; $Id: aia_bp_read_chan.pro,v 1.5 2010/08/10 22:47:07 boerner Exp $
;
; Reads in a chan.txt file and returns an IDL structure containing the
; parameters of the channel. 
;
; INPUT:	
;	filename-	the name of the chan.txt file to be read (including 
;				the path, if necessary, from the current directory; 
;				generally, no path should be used, and the function
;				should be run from the appropriate directory)
;
; OUTPUTS:
;	chanstr-	a structure giving the channel response (wavelength, 
;				effective area, platescale, units, and name)
;
;	fullchanstr-	a structure giving the channel response, along with
;					some information about how it was generated (including
;					the component level information)
;
;	warnings-	[OPTIONAL] a string array containing any descriptive
;				text on potential errors encountered during the generation
;				of the response structure
;
; KEYWORDS:
;	/silent-	if set, nothing is printed to the terminal
;
;	logdir=		if set, the warnings are written to a text file in the
;				logdir
; 
;	/write-		if set, the resulting wavelength and effective area are
;				written to a dat file called 
;
;	/official=	if set, then the input and output files are all copied to the
;				"official" bandpass file directory
;
; $Log: aia_bp_read_chan.pro,v $
; Revision 1.5  2010/08/10 22:47:07  boerner
; Added crossarea
;
; Revision 1.4  2010/08/03 00:28:17  boerner
; Change to calculate contam on the fly
;
; Revision 1.3  2010/06/21 23:33:14  boerner
; Changed error to wraning for uninherited parameter
;
; Revision 1.2  2010/06/15 22:59:50  boerner
; Added phottoDN handling
;
; Revision 1.1  2010/05/28 22:31:25  boerner
; Initial version
;
;
;-


;++++++++++++++++++++++++++++++++++++
; Set up variables and templates
;------------------------------------

; Initialize variables
common aia_bp_inststr, inherit_str

codename 	= 'AIA_BP_READ_CHAN.PRO'
codeversion = '$Id: aia_bp_read_chan.pro,v 1.5 2010/08/10 22:47:07 boerner Exp $'

errors = ''
warnings = ''

if not KEYWORD_SET(logdir) then logdir = 0
if not KEYWORD_SET(silent) then silent = 0
if not KEYWORD_SET(official) then official = 0

; Check that the filename format is AAAAA_chan.txt
AIA_BP_PARSE_FILENAME, filename, 'chan', shortfile = shortfile, $
	basefile = basefile, warnings = warnings, path=filepath

if KEYWORD_SET(official) then begin
	if official eq '1' then begin
		opath = !official_bp + datenow + '/'
	endif else begin
		opath = official
	endelse
	mkcmd = 'mkdir ' + opath
	SPAWN, mkcmd, result, error
	CD, opath, current = orig_dir
	cpcmd = 'cp ' + filename + './'
	SPAWN, cpcmd, result, error
endif else begin
	CD, filepath, current = orig_dir
endelse

chantxt = RD_TFILE(shortfile, 2, /nocomm, /compress, delim = '--')
rsize = SIZE(chantxt)

; Set up the channel information structure and fill it with the defaults
chanstr_template = {Name : 'A171', GeoArea : 27.5, Scale : 1.0, Platescale : 8.5e-5, $
					NumFilters : 2, NumMirrors: 2, $
					InFilePath : '.', OutFilePath : '.', $
					WaveMin : 1.0, WaveStep : 1.0, WaveNumSteps : 400l, $
					WaveLog : 0, UseWaveFile : 0, WaveFile  : 'TEMPLATE_wave.txt', $
					FiltersIncludeMesh : 0, MeshTrans : 0.82, $
					UseContam : 0, ContamFile : 'TEMPLATE_contam.txt', ContamThick : 200, $
					UsePhotToElec : 0, ElecPerEV : 1/3.65, $
					UsePhotToDN : 0, ElecPerDN : 18.0, $
					UseError : 0, ErrorFile : 'TEMPLATE_err.txt', $
					ExtrapolateComponents : 1 }
ntags = N_TAGS(chanstr_template)
tagnames = TAG_NAMES(chanstr_template)
chanstr = chanstr_template
if logdir eq '1' then logdir = chanstr.OutFilePath


; Go through each field in the channel information structure and check to
; see whether the chan.txt file sets its value. If not (or if it is specified
; redundantly), use the default and print a warning
for i = 0, ntags-1 do begin
	thistag = tagnames[i]
	tagind = WHERE(STRUPCASE(chantxt[0,*]) eq thistag, hastag)
	case hastag of
		0	:	begin
					thisval = chanstr.(i)
					warnstring = 'Missing tag: ' + thistag + ' set to ' + STRTRIM(thisval,2)
					warnings = [warnings, 'WARNING! ' + warnstring]
				end
		1	:	begin
					thisval = chantxt[1,tagind[0]]
					; inherit value from instrument if value is set to 'II'
					if STRUPCASE(thisval) eq 'II' then begin
						inheritind = WHERE(TAG_NAMES(inherit_str) eq thistag, inherited)
						if not inherited then begin
							warnstring = 'Parameter ' + thistag + ' not specified by instrument!!'
							warnings = [warnings, 'WARNING!! ' + warnstring]
							; PRINT, warnings
							; STOP
							; Instead of halting, use the hard-coded default from the channel
							thisval = chanstr.(i)
						endif else begin
							thisval = inherit_str.(inheritind)
						endelse
					endif
					chanstr.(i) = thisval
				end
		else:	begin		;	Currently, this is the same as not specifying a value
					thisval = chanstr.(i)
					warnstring = 'Redundant tags: ' + thistag + ' set to ' + STRTRIM(thisval,2)
					warnings = [warnings, 'WARNING! ' + warnstring]
				end
	endcase
endfor


; Make a wavelength grid for the channel based on the parameters specified
; in the file. All components will be interpolated to this grid
if chanstr.UseWaveFile then begin
	if KEYWORD_SET(official) then begin
		cpcmd = 'cp ' + chanstr.InFilePath + chanstr.wavefile + ' ./'
		SPAWN, cpcmd, result, error
		wavegrid = AIA_BP_READ_WAVE(chanstr.wavefile)
	endif else begin
		CD, chanstr.InFilePath, current = old_dir
		wavegrid = AIA_BP_READ_WAVE(chanstr.wavefile)
		CD, old_dir
	endelse
endif else begin
	wavegrid = FINDGEN(chanstr.WaveNumSteps)
	wavegrid = wavegrid * chanstr.WaveStep
	wavegrid = wavegrid + chanstr.WaveMin
	if chanstr.WaveLog then wavegrid = 10.^wavegrid
endelse
effarea = FLTARR(N_ELEMENTS(wavegrid)) + chanstr.GeoArea * chanstr.Scale


ea_units = 'cm2'
; If requested, add the photon-to-electron conversion into the effective
; area funtion for the channel. Check that the conversion factor is within
; 10% of the default (3.65 ev per electron)
if (chanstr.UsePhotToElec or chanstr.UsePhotToDN) then begin
	if ABS(chanstr.ElecPerEV * 3.65 - 1) gt 0.1 then begin
		warnstring = 'Non-standard electron per eV conversion in use'
		warnings = [warnings, 'WARNING! ' + warnstring]
	endif
	evperphot = 12398. / wavegrid
	elecperphot = evperphot * chanstr.ElecPerEV
	effarea = effarea * elecperphot
	ea_units = 'cm^2 elec phot^-1'
endif
; If requested, also add the electron-to-DN conversion into the effective
; area funtion for the channel
if chanstr.UsePhotToDN then begin
	if ABS(chanstr.ElecPerDN - 18) gt 2 then begin
		warnstring = 'Non-standard electron per DN conversion in use'
		warnings = [warnings, 'WARNING! ' + warnstring]
	endif
	effarea = effarea / chanstr.ElecPerDN
	ea_units = 'cm^2 DN phot^-1'
endif		

; Add the top-level result fields to the output structure
chanstr = CREATE_STRUCT('Wave', wavegrid, 'EffArea', effarea, 'Units', ea_units, $
	'Filename', filename, 'Date', AIA_BP_DATE_STRING(/time), chanstr)	
		

; If requested, read in the error parameter file
if chanstr.UseError then begin
	errfile = chanstr.ErrorFile
	AIA_BP_PARSE_FILENAME, errfile, 'err', shortfile = eshortfile, $
		basefile = ebasefile, warnings = warnings, path = efilepath
	if KEYWORD_SET(official) then begin
		cpcmd = 'cp ' + chanstr.InFilePath + errfile + ' ./'
		SPAWN, cpcmd, result, error
	endif
	errstr = AIA_BP_READ_ERR(chanstr.ErrorFile, logdir = logdir, $
		silent = silent)
	errcomps = TAG_NAMES(errstr)
endif


;++++++++++++++++++++++++++++++++++++
; Look for components specified in
; the chan.txt file and add them to 
; the channel response structure. As 
; you go, calculate the total channel 
; response
;------------------------------------
numcomp = 0
for i = 0, rsize[2] - 1 do begin
	tagsep = STRSPLIT(/extract, chantxt[0,i], ' ')
	if STRUPCASE(tagsep[0]) eq 'COMP' then begin

		numcomp = numcomp + 1
		compname = tagsep[1]
		compfile = chantxt[1,i]
		compfiletag = compname + '_file'
		if KEYWORD_SET(official) then begin
			cpcmd = 'cp ' + chanstr.InFilePath + compfile + ' ./'
			SPAWN, cpcmd, result, error
			compdat = AIA_BP_READ_COMP(compfile)
		endif else begin
			CD, chanstr.InFilePath, current = old_dir
			compdat = AIA_BP_READ_COMP(compfile)
			CD, old_dir
		endelse
		icompdat = INTERPOL(compdat[1,*], compdat[0,*], chanstr.Wave) 
		icompdat = 0. > icompdat < 1.
		
		; Check component response for possible errors/mis-specifications
		if chanstr.ExtrapolateComponents then begin
			if MIN(compdat[0,*]) gt MIN(wavegrid) then begin
				warnstring = 'Component ' + compname + ' extrapolated to short wavelengths'
				warnings = [warnings, 'WARNING! ' + warnstring]
			endif
			if MAX(compdat[0,*]) lt MAX(wavegrid) then begin
				warnstring = 'Component ' + compname + ' extrapolated to long wavelengths'
				warnings = [warnings, 'WARNING! ' + warnstring]
			endif
		endif else begin
			if MIN(compdat[0,*]) gt MIN(wavegrid) then begin
				icompdat[WHERE(wavegrid lt MIN(compdat[0,*]))] = 0.
				warnstring = 'Component ' + compname + ' zeroed outside defined range'
				warnings = [warnings, 'WARNING! ' + warnstring]		
			endif
			if MAX(compdat[0,*]) lt MAX(wavegrid) then begin
				icompdat[WHERE(wavegrid gt MAX(compdat[0,*]))] = 0.
				warnstring = 'Component ' + compname + ' zeroed outside defined range'
				warnings = [warnings, 'WARNING! ' + warnstring]		
			endif				
		endelse
		
		if MAX(icompdat) gt 1. then begin
			warnstring = 'Component ' + compname + ' greater than 100% efficient'
			warnings = [warnings, 'WARNING! ' + warnstring]
		endif
		if MIN(icompdat) lt 0. then begin
			warnstring = 'Component ' + compname + ' less than 0% efficient'
			warnings = [warnings, 'WARNING! ' + warnstring]
		endif

		chanstr = CREATE_STRUCT(chanstr, compname, icompdat, compfiletag, compfile)		

		; Add simulated calibration error, if requested
		if chanstr.UseError then begin
			errind = WHERE(STRUPCASE(errcomps) eq STRUPCASE(compname), haserr)
			if haserr eq 1 then begin
				thiserrstr = errstr.(errind)
				errcompdat = AIA_BP_ADD_ERR(chanstr.Wave, icompdat, $
					normout, noiseout, shiftout, norm = thiserrstr.norm, $
					noise = thiserrstr.noise, wshift = thiserrstr.wshift, $
					noisesmooth = thiserrstr.noisesmooth)
				chanstr = CREATE_STRUCT(chanstr, compname + '_norm', normout, $
					compname + '_noise', noiseout, compname + '_wshift', shiftout)
				icompdat = errcompdat
			endif else begin
				warnstring = 'No error specified for component ' + compname
				warnings = [warnings, 'WARNING! ' + warnstring]
			endelse
		endif

		; Finish up and include results in chanstr
		effarea = effarea * icompdat

	endif
endfor
target_numcomp = chanstr.NumMirrors + chanstr.NumFilters + 1 
if numcomp ne target_numcomp then begin
	warnstring = 'Number of components not equal to mirrors + filters + CCD'
	warnings = [warnings, 'WARNING! ' + warnstring]
endif

; If requested, add in the effects of contamination
if chanstr.UseContam then begin
	if KEYWORD_SET(official) then begin
		cpcmd = 'cp ' + chanstr.InFilePath + chanstr.ContamFile + ' ./'
		SPAWN, cpcmd, result, error
		compdat = AIA_BP_READ_COMP(chanstr.ContamFile)
	endif else begin
		CD, chanstr.InFilePath, current = old_dir
		contamdat = AIA_BP_READ_COMP(chanstr.ContamFile)
		CD, old_dir
	endelse
	icontam_coeff = INTERPOL(contamdat[1,*], contamdat[0,*], chanstr.Wave)
	icontam_od = (-1) * ALOG(icontam_coeff) / 1000.	;	Assumes IMD layer file using 1000 A
	icontamdat = EXP((-1) * chanstr.ContamThick * icontam_od)
	
	icontamdat = 0. > icontamdat < 1.
	
	if chanstr.ExtrapolateComponents then begin
		if MIN(contamdat[0,*]) gt MIN(wavegrid) then begin
			warnstring = 'Contam extrapolated to short wavelengths'
			warnings = [warnings, 'WARNING! ' + warnstring]
		endif
		if MAX(contamdat[0,*]) lt MAX(wavegrid) then begin
			warnstring = 'Contam extrapolated to long wavelengths'
			warnings = [warnings, 'WARNING! ' + warnstring]
		endif
	endif else begin
		if MIN(contamdat[0,*]) gt MIN(wavegrid) then begin
			icontamdat[WHERE(wavegrid lt MIN(contamdat[0,*]))] = 0.
			warnstring = 'Contam zeroed outside defined range'
			warnings = [warnings, 'WARNING! ' + warnstring]		
		endif
		if MAX(contamdat[0,*]) lt MAX(wavegrid) then begin
			icontamdat[WHERE(wavegrid gt MAX(contamdat[0,*]))] = 0.
			warnstring = 'Contam zeroed outside defined range'
			warnings = [warnings, 'WARNING! ' + warnstring]		
		endif				
	endelse
	
	if MAX(icontamdat) gt 1. then begin
		warnstring = 'Contam greater than 100% efficient'
		warnings = [warnings, 'WARNING! ' + warnstring]
	endif
	if MIN(icontamdat) lt 0. then begin
		warnstring = 'Contam less than 0% efficient'
		warnings = [warnings, 'WARNING! ' + warnstring]
	endif
	
	if chanstr.UseError then begin
		wherecontam = WHERE(errcomps eq 'CONTAM', hascontam)
		if hascontam eq 1 then begin
			conterrstr = errstr.(wherecontam)
			errcontamdat = AIA_BP_ADD_ERR(chanstr.Wave, icontamdat, $
				normout, noiseout, shiftout, norm = conterrstr.norm, $
				noise = conterrstr.noise, wshift = conterrstr.wshift, $
				noisesmooth = conterrstr.noisesmooth)
			chanstr = CREATE_STRUCT(chanstr, 'contam_norm', normout, $
				'contam_noise', noiseout, 'contam_wshift', shiftout)
			icontamdat = errcontamdat
		endif else begin
			warnstring = 'No error specified for contam'
			warnings = [warnings, 'WARNING! ' + warnstring]
		endelse
	endif
	effarea = effarea * icontamdat
	chanstr = CREATE_STRUCT(chanstr, 'Contam', icontamdat, $
		'Contam_file', chanstr.ContamFile)
endif


; Add a placeholder for crosstalk from another channel
crossarea = wavegrid * 0.
chanstr = CREATE_STRUCT(chanstr, 'Cross_area', crossarea)


; If the filter transmission component files do NOT include the shadowing
; effects of the filter mesh, then incorporate that effect now
if chanstr.FiltersIncludeMesh then begin
	chanstr.EffArea = effarea
endif else begin
	meshtrans = chanstr.MeshTrans ^ chanstr.NumFilters
	chanstr.EffArea = effarea * meshtrans
endelse


; If requested, add overall error (in addition to/instead of component level error)
if chanstr.UseError then begin
	wheresimple = WHERE(errcomps eq 'SIMPLE', hassimple)
	if hassimple eq 1 then begin
		simpleerrstr = errstr.(wheresimple)
		errcompdat = AIA_BP_ADD_ERR(chanstr.Wave, chanstr.EffArea, $
			normout, noiseout, shiftout, norm = simpleerrstr.norm, $
			noise = simpleerrstr.noise, wshift = simpleerrstr.wshift, $
			noisesmooth = simpleerrstr.noisesmooth)
		chanstr = CREATE_STRUCT(chanstr, 'total_norm', normout, $
			'total_noise', noiseout, 'total_wshift', shiftout)
		effarea = errcompdat
		chanstr.EffArea = errcompdat
	endif else begin
		warnstring = 'No top-level error specified'
		warnings = [warnings, 'WARNING! ' + warnstring]
	endelse
endif


; Look for notes specified in the chan.txt file and add them to the
; channel response structure. 
numnotes = 0
notes = ''
for i = 0, rsize[2] - 1 do begin
	if chantxt[0,i] eq 'NOTE' then begin
		numnotes = numnotes + 1
		notes = [ notes, STRTRIM(chantxt[1,i], 2) ]
	endif
endfor
if numnotes ge 1 then begin
	notes = notes[1:numnotes-1]
	chanstr = CREATE_STRUCT(chanstr, 'notes', notes)
endif


; Check for errors and inconsistencies, and write out a warning log file
; if necessary
numwarn = N_ELEMENTS(warnings)
if numwarn gt 1 then begin
;	warnings = warnings[1:numwarn-1]	;	drop the dummy first element
	if not KEYWORD_SET(silent) then for i = 1, numwarn-1 do PRINT, warnings[i]
	if KEYWORD_SET(logdir) then begin
		sepfile = STRSPLIT(/extract, filename, '.')
		warnfile = AIA_BP_DATE_STRING(/time) + '_' + basefile + '_warn.txt'
		if not KEYWORD_SET(official) then CD, logdir, current = old_dir
		OPENW, loglun, /get_lun, warnfile
		for i = 1, numwarn-1 do PRINTF, loglun, warnings[i]
		FREE_LUN, loglun
		if not KEYWORD_SET(official) then CD, old_dir
	 endif
endif 


; If requested, pull out only the basic fields (wavelength, effective area, 
; name and units) and return a short-form structure
fullchanstr = chanstr

shortstr = {Name : chanstr.Name, Wave : chanstr.Wave, EA : chanstr.EffArea, $
	Platescale : chanstr.Platescale, units : chanstr.Units}
chanstr = shortstr


; If requested, write out the response in a dat and genx file. Note that the default is 
if KEYWORD_SET(write) then begin
	if not KEYWORD_SET(official) then CD, current = old_dir, fullchanstr.OutFilePath
	OPENW, lun, /get_lun, basefile + '_chan.dat'
	PRINTF, lun, '# Written by AIA_BP_READ_CHAN on ' + SYSTIME()
	PRINTF, lun, '# Effective area for ' + chanstr.Name
	PRINTF, lun, '# Units are ' + chanstr.Units
	PRINTF, lun, '#', 'Wave [Ang]', 'EffArea', format = '(a1, a14, a15)'
	for i = 0l, N_ELEMENTS(chanstr.wave)-1 do $
		PRINTF, lun, '', chanstr.wave[i], chanstr.ea[i], format = '(a1, f14.3, e15.5)'
	FREE_LUN, lun
	SAVEGEN, file = basefile + '_chan.genx', str = chanstr
	SAVEGEN, file = basefile + '_fullchan.genx', str = fullchanstr
	if not KEYWORD_SET(official) then CD, old_dir
endif

CD, orig_dir

end
