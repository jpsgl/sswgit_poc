function AIA_BP_GET_CORRECTIONS, $
	respversion = respversion, version = version, uv = uv, pstop = pstop

;--------------------------------------------------------------------------------
;+
;
;Purpose: Returns a structure array that specifies the time-dependent correction for
; each of the AIA channels
;
;Inputs:
;  version: should be one of the allowedversions = [1,2,3,4,6,7,8], otherwise forced to be defaultversion
;
;
;Outputs:
;
;History:
; 2017/11/30, Thu, Wei Liu: 1) revised to include V8 of the response table.
;   2) added version, respversion, tablefile to the output structure
; 2017/12/01, Fri: 1) take 7 out of allowedversions, it's just an intermediate version made before Paul left, but never released to SSW
;
;-
;--------------------------------------------------------------------------------

if not KEYWORD_SET(uv) then uv = 0

	;--- 2017/11/30, replaced below, follow revised aia_bp_get_corrections.pro ---------
	;defaultversion = 6
	;allowedversions = [1,2,3,4,6]
allowedversions = [1,2,3,4,6,8]		;[1,2,3,4,6,7,8]
defaultversion = max(allowedversions)
	; note as of 2017/11/30, allowedversions = [1,2,3,4,5,6] in /archive/ssw/sdo/aia/idl/response/aia_bp_get_corrections.pro dated Feb 6, 2017, 
	; somebody added [5] there, which is incorrect (since V5 was not released to SSW, only exists in Paul's scratch space /net/crom/Volumes/disk2/data/aia/response/)

if N_ELEMENTS(version) eq 0 then version = defaultversion
okversion = WHERE(FIX(version) eq allowedversions, isok)
if not isok then begin
	print, 'Warning: user-specified version ' + trim(version) + ' NOT allowed, now force version to be default of '  + trim(defaultversion)
	version = defaultversion	;	silently set the version to the default if a funny value is used
endif
case version of
	1	:	vstring = 'aia_preflight_'
	else:	vstring = 'aia_V' + trim(version) + '_'		;2017/11/30

	;2	:	vstring = 'aia_V2_'
	;3	:	vstring = 'aia_V3_'
	;4	:	vstring = 'aia_V4_'
	;5	:	vstring = 'aia_V5_'
	;6	:	vstring = 'aia_V6_'
endcase

day0 = '1-may-2010 12:00:00'
daynow = RELTIME(/now)
tai0 = ANYTIM2TAI(day0)
tainow = ANYTIM2TAI(daynow)
numdays = LONG64(tainow - tai0)/86400l
taiarr = DINDGEN(numdays) * 86400 + tai0
utcs = TAI2UTC(/ccsds, taiarr)

; Locate the appropriate response table text file to read (if not specified,
; just read in the latest date for the chosen calibration version)
aiaresp=GET_LOGENV('AIA_RESPONSE_DATA')
if aiaresp eq '' then aiaresp=CONCAT_DIR('$SSW_AIA','response')
tablefilebase = aiaresp + '/'+ vstring
respfiles = FILE_SEARCH(tablefilebase + '*_response_table.txt')
for i = 0, N_ELEMENTS(respfiles)-1 do $
respfiles[i] = STRMID(STRSPLIT(respfiles[i], /extract, tablefilebase, /regex), 0, 15)
	;respfiles[i] = STRMID(file_basename(respfiles[i]), 7, 15)	; 2017/11/29 tried, but will not work for version > 9; so keep Paul's original as above
respfile_tais = ANYTIM2TAI(FILE2TIME(respfiles))
if not KEYWORD_SET(respversion) then respversion = respfiles[WHERE(respfile_tais eq MAX(respfile_tais))]
if not FILE_EXIST(tablefilebase + respversion + '*') then begin
	BOX_MESSAGE, ['File not found: ', tablefilebase + respversion + '*']
	respversion = respfiles[WHERE(resptfile_tais eq MAX(respfile_tais))]
endif
tablefilebase = tablefilebase + respversion + '_*'
ver_date = respversion
tablefile = tablefilebase + 'response_table.txt'
tabledat = AIA_BP_READ_RESPONSE_TABLE(tablefile, silent = silent)
startts = ANYTIM2TAI(tabledat.t_start)

if KEYWORD_SET(uv) then begin
	channels = [1600, 1700]
endif else begin
	channels = [94, 131, 171, 193, 211, 304, 335]
endelse
numchan = N_ELEMENTS(channels)
quickresp = AIA_GET_RESPONSE(/dn, version = version, uv = uv, respver = respversion)
quickresp0 = AIA_GET_RESPONSE(/dn, version = version, uv = uv, respver = respversion, $
	/evenorm, time = '2010-03-24T00:00:00.000')

corr_str = CREATE_STRUCT('version', version, 'respversion', respversion, 'tablefile', tablefile, $
	'utc', utcs, 'tai', taiarr, $
	'channels', channels, 'ea_aia_t0', FLTARR(numchan), 'ea_eve_t0', FLTARR(numchan), $
	'ea_over_ea0', FLTARR(numdays, numchan), $
	'ea_aia_over_eve', quickresp.corrections.evenorm.aia_over_eve)

for i = 0, numchan - 1 do begin
	thiswave 	= channels[i]
	wavelines 	= WHERE(tabledat.wavelnth eq thiswave, numwavelines)
	wlsort		= SORT(startts[wavelines])
	firstline = tabledat[wavelines[wlsort[0]]]
	corr_str.ea_eve_t0[i] = firstline.eff_area
	corr_str.ea_aia_t0[i] = firstline.eff_area * corr_str.ea_aia_over_eve[i]
	
	for j = 0, numwavelines-1 do begin
		thisline = tabledat[wavelines[wlsort[j]]]
		ea_epoch = thisline.eff_area / firstline.eff_area
		taistart = ANYTIM2TAI(thisline.t_start)
		taistop = ANYTIM2TAI(thisline.t_stop)
		intime = WHERE( (taiarr ge taistart) and (taiarr lt taistop), numin)
		dt = (taiarr[intime] - taistart) / 86400d
		tcorr = 1 + thisline.effa_p1 * dt + thisline.effa_p2 * dt^2 + thisline.effa_p3 * dt^3
		corr_str.ea_over_ea0[intime, i] = tcorr * ea_epoch
	endfor
	
endfor

if KEYWORD_SET(pstop) then STOP

RETURN, corr_str

end
