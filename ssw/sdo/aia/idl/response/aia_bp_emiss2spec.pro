function AIA_BP_EMISS2SPEC, emiss_struct, $
	fullspecstr = fullspecstr, $
	dem_struct = dem_struct, $
	old_interp = old_interp, loud = loud, _extra = extra

;
;+
;   Name: aia_bp_emiss2spec
;
;   Purpose: converts an emissivity structure into a predicted spectrum, given
;		a differential emission measure
;
;   Input Parameters:
;		emiss_struct -	(OPTIONAL) an emissivity structure; if not specified, a default
;					emissivity (spectral model) is loaded
;
;	Keyword Parameters:
;		dem_struct	-	(OPTIONAL) a DEM structure, such as those returned by AIA_BP_READ_DEM
;					if not specified, the default (AR) DEM is used (and can be returned)
;		_extra		-	keyword parameters are passed to AIA_BP_READ_DEM
;		fullspecstr	-	(output) the FULL spectrum structure (including a matrix showing
;						the temperature contribution to each wavelength bin)
;
;   Returns:
;		A spectrum structure.
;		A description of the response data files are available via: 
;         http://sohowww.nascom.nasa.gov/solarsoft/sdo/aia/response/README.txt
;
;
; Example:
;	IDL> ardem = AIA_BP_READ_DEM(/ar)
;	IDL> qsdem = AIA_BP_READ_DEM(/qs)
;	IDL> emiss_str = AIA_GET_RESPONSE(/emiss)
;	IDL> arspec = AIA_BP_EMISS2SPEC(emiss_str, dem = ardem, full_ar_spec)
;	IDL> qsspec = AIA_BP_EMISS2SPEC(emiss_str, dem = qsdem, full_qs_spec)
; or
;	IDL> qsspec = AIA_BP_EMISS2SPEC(/qs)
;
;-
;

if N_ELEMENTS(emiss_struct) eq 0 then emiss_struct = AIA_GET_RESPONSE(/emiss)
if N_ELEMENTS(dem_struct) eq 0 then dem_struct = AIA_BP_READ_DEM(_extra = extra)

wave = emiss_struct.wave
logte = emiss_struct.logte
emissivity = emiss_struct.emissivity
numwave = N_ELEMENTS(wave)

t0 = SYSTIME(/sec)
if KEYWORD_SET(old_interp) then begin

	demtlim = LIMITS(dem_struct.logte)
	interpdem = INTERPOL(dem_struct.logdem, dem_struct.logte, logte)
	tempstep = logte[1] - logte[0]
	overlaptemp = WHERE(logte ge demtlim[0] and logte le demtlim[1])
	reformdem = REFORM(10.^interpdem[overlaptemp] * 10.^logte[overlaptemp] * ALOG(10.^tempstep))	
	spectrum = emissivity[*,overlaptemp] # reformdem
	tgrid = logte[overlaptemp]
	; Generate the more detailed spectral contribution matrix
	numtemp = N_ELEMENTS(tgrid)
	dem_matrix = REBIN(TRANSPOSE(reformdem), numwave, numtemp)
	spec_matrix = emissivity[*, overlaptemp] * dem_matrix
	; We seem to get the same result for the spectrum if we do
	;	spectrum = TOTAL(spec_matrix, 2)

endif else begin

	; Added 2013-01-16
	numtemp_in = N_ELEMENTS(dem_struct.logte)
	tgrid = dem_struct.logte
	numtemp = N_ELEMENTS(tgrid)
;	interp_emiss = DBLARR(numwave, numtemp)
;	for i = 0, numwave - 1 do $
;		for j = 0, numtemp - 1 do $
;			interp_emiss[i,j] = INTERPOL(emissivity[i,*], emiss_struct.logte, dem_struct.logte[j])
	ix = REBIN(FINDGEN(numwave), numwave, numtemp_in)
	jy = FLTARR(1, numtemp_in)
	for i = 0, numtemp_in-1 do jy[0,i] = INTERPOL(INDGEN(N_ELEMENTS(logte)), logte, tgrid[i])
	jy = REBIN(jy, numwave, numtemp_in)
	interp_emiss = BILINEAR(emissivity, ix, jy)
	dem_tempstep = dem_struct.logte[1] - tgrid[0]
	nolog_dem = 10.^(dem_struct.logdem) * 10.^tgrid * ALOG(10.^dem_tempstep)
	spectrum = interp_emiss # nolog_dem
	; Generate the more detailed spectral contribution matrix
	dem_matrix = REBIN(TRANSPOSE(nolog_dem), numwave, numtemp)
	spec_matrix = interp_emiss * dem_matrix
	; We seem to get the same result for the spectrum if we do
	;	spectrum = TOTAL(spec_matrix, 2)

endelse

if KEYWORD_SET(loud) then begin
	PRINT, SYSTIME(/sec) - t0
	STOP
endif

; Generate the fullspecstr and the simple specstr
datetag = AIA_BP_DATE_STRING(/time)
demtags = TAG_NAMES(dem_struct)
nameid = WHERE(demtags eq 'NAME', hasname)
if hasname then demname = dem_struct.name else demname = ''
fullspecstr = CREATE_STRUCT('Date', datetag, 'DEMName', demname, $
	'EmissDate', emiss_struct.date, 'Wave', wave, 'Spec', spectrum, $
	'Units', 'photons cm-2 s-1 sr-1 A-1', $
	'LogTe', tgrid, 'Spec_matrix', spec_matrix)
specstr = {Date : datetag, Wave : fullspecstr.Wave, Spec : fullspecstr.spec, $
	units : fullspecstr.Units}

RETURN, specstr

end