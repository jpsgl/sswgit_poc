function AIA_BP_CH_CONTINUUM, xax, pressure = pressure, noloud = noloud, setup = setup, $
    ioneqfile = ioneqfile, abundfile = abundfile

;+
;
;
; $ Log: $
;
; INPUTS: [xax] --    optionally, can be set to an array giving the wavelength or energy values for which the continuum
;             intensity should be found.
;
; OUTPUTS: xax --   if an empty variable is passed, it will be returned with the default wavelength vector,
;             which uses 389 points between 25 and 413 Angstroms
;
; RETURNS: cont --  an array containing continuum emissivity (intensity per unit EM) as a function of wavelength
;      and temperature. Units are photons cm-2 s-1 sr-1 A-1 per EM [I think!!] = ph cm3 s-1 sr-1 A-1.
;
; KEYWORDS: pressure --  Assumes constant pressure; default is 1.e15
;
;      /noloud --  If not set, the continuum is plotted before it is returned
;
;     /setup -- if set, the user is prompted to pick abundance and ionization equilibrium files.
;          By default, it is assumed that those values have already been stored in the appropriate
;          common block (or that they are passed in via the ioneqfile and abundfile keywords)
;
;   abundfile/ioneqfile   --    if set (to the complete path + filename of a CHIANTI data file), the
;      abundance/ionization equilibrium of the specified type is read in. By default, the program
;      will either a)prompt the user for these files, or b) use the values stored in the common
;      block.
;
;
;-

if not KEYWORD_SET(pressure) then pressure = 1.e15

if N_ELEMENTS(xax) eq 0 then xax = FINDGEN(413-24) + 25
wvls = xax
wavestep = wvls[1] - wvls[0]

common elements, abund, abund_ref, ioneq, ioneq_logt, ioneq_ref

if KEYWORD_SET(abundfile) then READ_ABUND, abundfile, abund, abund_ref
if KEYWORD_SET(ioneqfile) then READ_IONEQ, ioneqfile, ioneq_logt, ioneq, ioneq_ref
if KEYWORD_SET(setup) or N_ELEMENTS(abund) eq 0 or N_ELEMENTS(ioneq) eq 0 then SETUP_ELEMENTS


temp = FLOAT(ioneq_logt)
logtstep = ioneq_logt[1] - ioneq_logt[0]
numtemp = N_ELEMENTS(temp)
dem = FLTARR(numtemp)
edensity = pressure/(10.^temp)

common proton, pstr, pe_ratio
pe_ratio=PROTON_DENS(temp)

TWO_PHOTON, 10.^temp, wvls, tp, /photons, /no_setup, edensity=edensity
FREEFREE, 10.^temp, wvls, ff, /photons, /no_setup
FREEBOUND, 10.^temp, wvls, fb, /photons, /no_setup
tp = tp/1d40
ff = ff/1d40
fb = fb/1d40

cont = tp + ff + fb

if not KEYWORD_SET(noloud) then PLOT, xax, cont[*,numtemp/2], /ylog, $
	xtitle = 'Wavelength [' + STRING("305B) + ']', ytitle = 'Emissivity',$
    title = 'Emissivity at LogT= ' + STRTRIM(ioneq_logt[numtemp/2],2)

RETURN, cont

end
