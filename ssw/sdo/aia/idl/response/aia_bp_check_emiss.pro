pro AIA_BP_CHECK_EMISS

;+
;
; Compares the reference emissivity function with one generated using the built-in
; CHIANTI routine ISOTHERMAL
;
;-

pb_emiss_3 = AIA_BP_MAKE_EMISS(wvlrange = [1, 661], wvlstep = 0.1, $
	pressure = 1e15, /no_he2, abundfile=!xuvtop+'/abundance/sun_coronal_ext.abund', $
	ioneqfile= !xuvtop+'/ioneq/chianti.ioneq', /all)

pb_emiss_2 = AIA_GET_RESPONSE(/emiss, /full, ver = 2)
pb_emiss_1 = AIA_GET_RESPONSE(/emiss, /full, ver = 1)

t=INDGEN(101)*0.05+4.0
temp = 10^t

ardem = AIA_BP_READ_DEM(/ar)
tstep = ardem.logte[1] - ardem.logte[0]
ar_em = 10.^ardem.logdem * 10.^ardem.logte * ALOG(10.^tstep)
i_ar_em = INTERPOL( ar_em, ardem.logte, t)
i_ar_em[WHERE(t lt MIN(ardem.logte) or t gt MAX(ardem.logte), numout)] = 0

ISOTHERMAL, 1, 661, 0.1, temp, lambda, iso_spectrum, list_wvl, list_ident, $
                 pressure=1e15, /photons, /all, em = i_ar_em, $
                 abund_name = !xuvtop + '/abundance/sun_coronal_ext.abund', $
                 ioneq_name = !xuvtop + '/ioneq/chianti.ioneq'

ISOTHERMAL, 1, 661, 0.1, temp, lambda_c, iso_spectrum_c, list_wvl, list_ident, /cont,$
                 pressure=1e15, /photons, /all, em = i_ar_em, $
                 abund_name = !xuvtop + '/abundance/sun_coronal_ext.abund',$
                 ioneq_name = !xuvtop + '/ioneq/chianti.ioneq'

arspec1 = AIA_BP_EMISS2SPEC(pb_emiss_1.total, dem = ardem)
c1 = {wave : pb_emiss_1.cont.wave, emissivity : pb_emiss_1.cont.emiss, logte : pb_emiss_1.cont.temp, date : ''}
c2 = {wave : pb_emiss_2.cont.wave, emissivity : pb_emiss_2.cont.emiss, logte : pb_emiss_2.cont.temp, date : ''}
c3 = {wave : pb_emiss_3.cont.wave, emissivity : pb_emiss_3.cont.emiss, logte : pb_emiss_3.cont.temp, date : ''}
ar_cont_spec1 = AIA_BP_EMISS2SPEC(c1, dem = ardem)
arspec2 = AIA_BP_EMISS2SPEC(pb_emiss_2.total, dem = ardem)
ar_cont_spec2 = AIA_BP_EMISS2SPEC(pb_emiss_2.cont, dem = ardem)
arspec3 = AIA_BP_EMISS2SPEC(pb_emiss_3.total, dem = ardem)
ar_cont_spec3 = AIA_BP_EMISS2SPEC(pb_emiss_3.cont, dem = ardem)
iso_ar_cont_spec = TOTAL(iso_spectrum_c - iso_spectrum, 2)

isodem = FLTARR(101)
isodem[40] = INTERPOL(ardem.logdem, ardem.logte, t[40])
pb_isospec = AIA_BP_EMISS2SPEC(pb_emiss_3.total, dem = {logte : t, logdem : isodem})
isospec = TOTAL(iso_spectrum_c, 2)

STOP

end
