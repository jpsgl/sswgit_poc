pro AIA_BP_TEST_CORRECTIONS

;+
;
; Routine to test new version of effective area and temperature response functions
;
; $Log: aia_bp_test_corrections.pro,v $
; Revision 1.2  2012/02/09 17:53:28  boerner
; check time dependence, add some fixes
;
; Revision 1.1  2012/01/05 01:36:28  boerner
; Initial version
;
;
;-

;oldresp = GET_LOGENV('AIA_RESPONSE_DATA')
oldrespdir = '/Users/boerner/ssw//sdo/aia/response'
newrespdir = '/Volumes/disk2/data/aia/response/'

SET_LOGENV, 'AIA_RESPONSE_DATA', newrespdir
nemiss = AIA_GET_RESPONSE(/emiss)
ntresp = AIA_GET_RESPONSE(/dn, /temp)
nwresp = AIA_GET_RESPONSE(/dn)
ntresp_tags = TAG_NAMES(ntresp)
nwresp_tags = TAG_NAMES(nwresp)

oemiss = AIA_GET_RESPONSE(/emiss, version = 1)
otresp = AIA_GET_RESPONSE(/dn, /temp, version = 1)
owresp = AIA_GET_RESPONSE(/dn, version = 1)
otresp_tags = TAG_NAMES(otresp)
owresp_tags = TAG_NAMES(owresp)

SET_LOGENV, 'AIA_RESPONSE_DATA', oldrespdir
ooemiss = AIA_GET_RESPONSE(/emiss, version = 1)
ootresp = AIA_GET_RESPONSE(/dn, /temp, version = 1)
oowresp = AIA_GET_RESPONSE(/dn, version = 1)
ootresp_tags = TAG_NAMES(ootresp)
oowresp_tags = TAG_NAMES(oowresp)

SET_LOGENV, 'AIA_RESPONSE_DATA', newrespdir
RESTGEN, file = !data + 'aia/dem/' + '20110823_165850_eve_dem_fit.genx', preflare_str
RESTGEN, file = !data + 'aia/dem/' + '20110823_165848_eve_dem_fit.genx', postflare_str
;	preflare_str = postflare_str[200]

; Simple consistency check: Verify that, by running the new code/database and asking for version 1, you get the same thing 
; you used to get
PRINT, 'TRESP: New Ver1 versus old data: PMM A/B, PMM A-B'
PMM, otresp.all / ootresp.all
PMM, otresp.all - ootresp.all

; Check time dependence of effective area
twresp = AIA_GET_RESPONSE(/dn, /time, evenorm = 0)
twresp_tags = TAG_NAMES(twresp)

evewave = preflare_str.evewave
evtophot 	= 12398 / evewave
aiaplatescale = 8.46158e-12			;	sr per pixel
srpersun = 2d^24 * aiaplatescale
eve_Wtophot = 1.602e-19 * evtophot * 1.d4 * 10. * srpersun
evedemspec = {wave : evewave, spec : preflare_str.pred_spec / eve_Wtophot}
eveobsspec = {wave : evewave, spec : preflare_str.obs_spec / eve_Wtophot}
preflare_demstr = {logte : preflare_str.logt_dem, logdem : preflare_str.dem}
rawdemspec = AIA_BP_EMISS2SPEC(emiss, dem=preflare_demstr)
evelinespec = AIA_SPECTRUM2EVE(rawdemspec.spec, rawdemspec.wave, evelinewave, bin = 0.1)
evelinspec = {wave : evelinewave, spec : evelinespec}

channame = ['A94', 'A131', 'A171', 'A193', 'A211', 'A335']
for i = 0, 5 do begin
	this_tresp = ntresp.(WHERE(ntresp_tags eq channame[i]))
	this_wresp = nwresp.(WHERE(nwresp_tags eq channame[i]))	
	demcounts 			= AIA_BP_MAKE_COUNTS_DEM(this_tresp, preflare_demstr)
	evedemspeccounts 	= AIA_BP_MAKE_COUNTS_SPEC(this_wresp, evedemspec, null, evedemwave, evedemthru)
	eveobsspeccounts	= AIA_BP_MAKE_COUNTS_SPEC(this_wresp, eveobsspec, null, eveobswave, eveobsthru)
	rawdemspeccounts	= AIA_BP_MAKE_COUNTS_SPEC(this_wresp, rawdemspec, null, rawdemwave, rawdemthru)
	evelinspeccounts	= AIA_BP_MAKE_COUNTS_SPEC(this_wresp, evelinspec, null, evelinwave, evelinthru)
	PRINT, channame[i]
	PRINT, 'Blurring effect: ', evedemspeccounts / rawdemspeccounts
	PRINT, 'TResp error: ', demcounts / rawdemspeccounts
	PRINT, 'DEM mismatch: ', evedemspeccounts / eveobsspeccounts
	PRINT, 'EVE Line/spec : ', evelinspeccounts / evedemspeccounts
	PRINT
	
	this_otresp = otresp.(WHERE(otresp_tags eq channame[i]))
	this_owresp = owresp.(WHERE(owresp_tags eq channame[i]))	
	odemcounts 			= AIA_BP_MAKE_COUNTS_DEM(this_otresp, preflare_demstr)
	oevedemspeccounts 	= AIA_BP_MAKE_COUNTS_SPEC(this_owresp, evedemspec, null, evedemwave, evedemthru)
	oeveobsspeccounts	= AIA_BP_MAKE_COUNTS_SPEC(this_owresp, eveobsspec, null, eveobswave, eveobsthru)
	orawdemspeccounts	= AIA_BP_MAKE_COUNTS_SPEC(this_owresp, rawdemspec, null, rawdemwave, rawdemthru)
	oevelinspeccounts	= AIA_BP_MAKE_COUNTS_SPEC(this_owresp, evelinspec, null, evelinwave, evelinthru)
	PRINT, channame[i]
	PRINT, 'DEM count ratio: ', demcounts / odemcounts
	PRINT, 'EVEDEMSPEC count ratio: ', evedemspeccounts / oevedemspeccounts
	PRINT, 'EVEOBSSPEC count ratio: ', eveobsspeccounts / oeveobsspeccounts
	PRINT, 'RAWDEMSPEC count ratio: ', rawdemspeccounts / orawdemspeccounts
	PRINT, 'EVELINESPEC count ratio: ', evelinspeccounts / oevelinspeccounts
	PRINT	

	this_twresp = twresp.(WHERE(twresp_tags eq channame[i]))
	corrchan = WHERE(twresp.corrections.time.channels eq channame[i])
	PRINT, 'Sample date : ', twresp.corrections.time.sampletime
	PRINT, 'Time dependence : ', MEDIAN(this_twresp.ea / this_wresp.ea), $
		twresp.corrections.time.ea_sample_over_t0[corrchan]
	PRINT
	
	PLOT, rawdemwave, rawdemthru, xrange = FIX(STRMID(channame[i], 1, 3)) * [0.97, 1.03], $
		chars = 1.5, /xstyle, psym = 10
	OPLOT, eveobswave, eveobsthru, col = 2, psym = 10
	OPLOT, evedemwave, evedemthru, col = 6, psym = 10
	OPLOT, evelinwave, evelinthru, col = 5, psym = 10
	PB_LEGEND, ['Raw DEM', 'EVE Obs', 'EVE DEM L2', 'EVE DEM L1'], 200, 500, col = [255, 2, 6, 5], $
		/line, /dev, size = 1.5

	STOP
	
endfor

RESTGEN, file = !data + 'aia/dem/' + '20110923_172224_eve_dem_fit_full.genx', quiet_str
RESTGEN, file = !data + 'aia/dem/' + '20110823_165848_eve_dem_fit.genx', postflare_str

end