function AIA_BP_MAKE_COUNTS_DEM, trespstr, demstr, $
	tcontrib, tgrid, $
	silent = silent, $
	_extra = extra

;+
;
; $Log: aia_bp_make_counts_dem.pro,v $
; Revision 1.7  2013/01/31 22:16:15  boerner
; Ensure compatibility with different DEM formats
;
; Revision 1.6  2012/02/08 17:49:40  boerner
; Remove minimum threshold on logdem
;
; Revision 1.5  2012/01/30 23:08:30  boerner
; Work on vector input
;
; Revision 1.4  2012/01/05 01:05:37  boerner
; Corrected typos/vagueness in header
;
; Revision 1.3  2011/09/13 22:08:41  boerner
; Minor update to comment header
;
; Revision 1.2  2010/11/09 19:36:13  boerner
; Added silent option
;
; Revision 1.1  2010/06/24 17:02:32  boerner
; initial version
;
;
; Given an instrument temperature response and a DEM, returns a predicted
; count rate. 
;
; INPUTS:
;	trespstr--	an instrument temperature response structure (like those made by
;				aia_get_response) with .tresp and .logte fields.
;				Can be an N-element vector of tresponse structures, in which case fluence, 
;				tgrid and tcontrib are also arrays
;
;	demstr	--	[Optional; see keywords below] A DEM structure (like those read
;				in by aia_bp_read_dem),	with at least fields .logte and .logdem
;
; OUTPUTS:
;	tgrid	--	an M-element vector giving the temperature bins corresponding to tcontrib
;				(only defined over temperature range where both the response function
;				and the DEM are valid)
;
;	tcontrib--	an M-element vector giving the counts/s due to plasma at each temperature bin
;				(or an N-by-M element array of contributions versus temperature
;				for each of the channels specified in the trespstr vector)
;
; RETURNS:
;	fluence	--	a count rate, in phot/s/pix (or DN/s/pix, depending on the units of
;				the trespstr input)
;				(or can be an N-element vector of count rates for each channel)
;
; KEYWORDS:
; 	Accepts the keyword inputs used by aia_bp_read_dem (e.g. /ar or /qs) and passes
; them through if the input demstr is not specified (so if you just want to use
; the default active region DEM specified in aia_bp_read_dem, leave demstr blank
; and set the /ar keyword).
;
;-

if N_ELEMENTS(demstr) eq 0 then demstr = AIA_BP_READ_DEM(_extra = extra)

;+++++++++++++++++++++++++++++++++++++
; Ensure compatibility with different
; formats for demstr
;-------------------------------------
demtags = TAG_NAMES(demstr)
oldtag = WHERE(demtags eq 'LOGT_DEM', hasold)
if hasold gt 0 then begin
	use_demstr = CREATE_STRUCT('logte', demstr.logt_dem, $
		'logdem', demstr.dem)
endif else begin
	use_demstr = demstr
endelse

; Generate a temperature grid for the calculation
tmin = MIN(trespstr.logte) > MIN(use_demstr.logte)
tmax = MAX(trespstr.logte) < MAX(use_demstr.logte)
;tgood = WHERE(demstr.logte ge tmin and demstr.logte le tmax and demstr.logdem gt 0, numgood)
tgood = WHERE(use_demstr.logte ge tmin and use_demstr.logte le tmax, numgood)
tstep = use_demstr.logte[1] - use_demstr.logte[0]

; If given an array of temperature response structures, then run recursively on each element
; in the array and generate tgrid and tcontrib outputs
if N_ELEMENTS(trespstr) gt 1 then begin
	numtresp = N_ELEMENTS(trespstr)
	fluence = DBLARR(numtresp)
	tgrid = DBLARR(numgood)
	tcontrib = DBLARR(numtresp, numgood)
	for i = 0, numtresp - 1 do begin
		thisfluence = AIA_BP_MAKE_COUNTS_DEM(trespstr[i], use_demstr, thistcontrib, thistgrid)
		fluence[i] = thisfluence
		if i eq 0 then tgrid = thistgrid
		tcontrib[i, *] = thistcontrib
	endfor
	RETURN, fluence
endif

; Check that the DEM has an even temperature grid. If not, use INT_TABULATED, 
; which is sometimes prone to instabilities but generally works ok. If the T grid
; is even, then just add everything up, which is more reliable. These methods seem
; to agree for well-behaved functions.
all_tsteps = ROUND(DERIV(use_demstr.logte)*100)/100.
if MAX(all_tsteps) - MIN(all_tsteps) ne 0 then begin
	; Irregular T Grid; use INT_TABULATED
	if not KEYWORD_SET(silent) then PRINT, 'WARNING! AIA_BP_MAKE_COUNTS_DEM: irregular T grid on DEM'
	tgrid = use_demstr.logte
	interp_tresp = INTERPOL(trespstr.tresp, trespstr.logte, tgrid)
	tcontrib = 10.^use_demstr.logdem * interp_tresp
	fluence = INT_TABULATED(10.^tgrid, tcontrib)
endif else begin
	; Regular T Grid; just use TOTAL
	interp_tresp = INTERPOL(trespstr.tresp, trespstr.logte, use_demstr.logte)
	tcontrib = 10.^use_demstr.logdem[tgood] * interp_tresp[tgood] * $
		10.^use_demstr.logte[tgood] * ALOG(10.^tstep)
	tgrid = use_demstr.logte[tgood]
	fluence = TOTAL(tcontrib)
endelse

RETURN, fluence

end
