function AIA_BP_DEFAULT_SPECTRUM, full = full, _extra = extra

;+
;
; $Log: aia_bp_default_spectrum.pro,v $
; Revision 1.2  2010/08/10 22:56:22  boerner
; Added full keyword
;
; Revision 1.1  2010/08/10 22:46:07  boerner
; Initial version
;
;
;
; Calculates a "default" solar spectrum using the standard AIA emissivity
; and one of the standard CHIANTI DEM functions. It's actually just a wrapper
; for AIA_BP_EMISS2SPEC.
;
; KEYWORDS:
;	/full	- if set, then the full spectrum structure is returned (by
;				default, the simple spectrum structure is returned)
;	
;	_extra	- extra keywords are passed to AIA_BP_EMISS2SPEC
;
; RETURNS:
;	specstr	-	the spectrum structure
;
;-

specstr = AIA_BP_EMISS2SPEC(_extra = extra, full = fullspec)

if KEYWORD_SET(full) then retval = fullspec else retval = specstr

RETURN, retval

end
