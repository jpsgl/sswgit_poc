function AIA_BP_CONTAM_THICK, infullresp, $
	contamthick_ratio, $
	short = short

;+
;
; $Id: aia_bp_contam_thick.pro,v 1.1 2010/08/03 00:30:40 boerner Exp $
;
; Takes in a long-form instrument response structure and adjusts the thickness 
; of the contamination layer (and, thus, effective area) for the EUV channels.
;
;-

resptags = TAG_NAMES(infullresp)
outfullresp = infullresp

orig_contam = infullresp.a94_full.contam
new_contam = orig_contam ^ FLOAT(contamthick_ratio)

inchan = infullresp.channels
for i = 0, N_ELEMENTS(inchan)-1 do begin
	thischan = inchan[i]
	thischan_ind = WHERE(resptags eq (thischan + '_FULL') )
	thisstr = infullresp.(thischan_ind)
	thiscontam = new_contam
	thiseffarea = thisstr.ent_filter * thisstr.primary * thisstr.secondary * thisstr.fp_filter * thiscontam * thisstr.ccd * thisstr.geoarea
	outfullresp.(thischan_ind).contam = thiscontam
	outfullresp.(thischan_ind).effarea = thiseffarea
	shortind = WHERE(resptags eq thischan)
	outfullresp.(shortind).ea = thiseffarea
endfor

outfullresp.notes = outfullresp.notes + ' ADJUSTED CONTAM THICKNESS BY ' + STRTRIM(contamthick_ratio)

if KEYWORD_SET(short) then presp = AIA_BP_PARSE_EFFAREA(outfullresp, retval) else retval = outfullresp

RETURN, retval

end