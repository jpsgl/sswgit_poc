function AIA_BP_AREA2TRESP, area_struct, emiss_struct, $
	fulltrespstr, $
	emversion

;
;+
;   Name: aia_bp_area2tresp
;
;   Purpose: converts an effective area function into a temperature response function, given
;		an emissivity from CHIANTI or APEC or similar
;
;   Input Parameters:
;		area_struct	-	an effective area structure such as the one returned by aia_get_response
;		emiss_struct -	(OPTIONAL) an emissivity structure; if not specified, a default
;					emissivity (spectral model) is loaded
;		emversion	-	an integer indicating the version number of the emissivity data. This 
;						is put into the tresp structure as an informational tag
;
;   Returns:
;		A temperature response structure.
;		A description of the response data files are available via: 
;         http://sohowww.nascom.nasa.gov/solarsoft/sdo/aia/response/README.txt
;
;	Output Parameters:
;		fulltrespstr	-	the long form temperature response structure
;
;-
;

if N_ELEMENTS(emiss_struct) eq 0 then begin
	emiss_struct = AIA_GET_RESPONSE(/emiss, /full)
	emversion = emiss_struct.version
endif
if N_ELEMENTS(emversion) eq 0 then begin
	BOX_MESSAGE, ['AIA_BP_AREA2TRESP : emversion not specified', 'Plugging in dummy value of 0']
	emversion = 0
endif

emissinfo = STR_SUBSET(emiss_struct.general, 'abundfile,source,ioneq_logt,ioneq_name,ioneq_ref,wvl_limits,model_name,model_ne,model_pe,model_te,wvl_units,add_protons,version,photoexcitation')
areatags = TAG_NAMES(area_struct)
;datetag = 'A_' + area_struct.date + '_E_' + emiss_struct.total.date
datetag = area_struct.date
gendate = AIA_BP_DATE_STRING(/time)

trespstr = CREATE_STRUCT('Name', area_struct.name, $
		'Date', datetag, $
		'EffArea_Version', area_struct.version, 'Emiss_Version', emversion, $
		'Channels', area_struct.channels, $
		'Logte', emiss_struct.total.logte)

fulltrespstr = CREATE_STRUCT('Name', area_struct.name, $
		'Date', datetag, 'LoadDate', gendate, $
		'EffArea_Version', area_struct.version, 'Emiss_Version', emversion, $
		'EmissInfo', emissinfo, $
		'Channels', area_struct.channels, $
		'Logte', emiss_struct.total.logte, $
		'Wave', emiss_struct.total.wave)
		
hasdn = STRPOS(STRUPCASE(area_struct.units), 'DN')
if hasdn lt 0 then countunits = 'phot' else countunits = 'DN'
for i = 0, N_ELEMENTS(area_struct.channels)-1 do begin
	thischan = area_struct.channels[i]
	thisareastr = area_struct.(WHERE(areatags eq thischan))	
	thistrespstr = AIA_BP_MAKE_TRESP(emiss_struct.total.wave, emiss_struct.total.logte, $
		emiss_struct.total.emissivity, thisareastr.wave, thisareastr.ea, $
		thischan, thisareastr.platescale, thisfullstr, countunits)
	if i eq 0 then begin
		shortchans = CREATE_STRUCT(thischan, thistrespstr)
		longchans = CREATE_STRUCT(thischan + '_FULL', thisfullstr)
		tresp = thistrespstr.tresp
		twresp = thisfullstr.full_tresp
		units = thisfullstr.units
		full_units = thisfullstr.full_units
	endif else begin
		shortchans = CREATE_STRUCT(shortchans, thischan, thistrespstr)
		longchans = CREATE_STRUCT(longchans, thischan + '_FULL', thisfullstr)
		tresp = [[tresp], [thistrespstr.tresp]]
		twresp = [[[twresp]], [[thisfullstr.full_tresp]]]
	endelse
endfor

notestr = 'Made by AIA_BP_AREA2TRESP'
trespstr = CREATE_STRUCT(trespstr, 'all', tresp, 'units', countunits, shortchans, 'Note', notestr)
fulltrespstr = CREATE_STRUCT(fulltrespstr, 'tresp', tresp, 'twresp', twresp, $
	'tunits', units, 'twunits', full_units, shortchans, longchans, 'note', notestr)

corrtag = WHERE(areatags eq 'CORRECTIONS', hascorr)
if hascorr gt 0 then begin
	corrstr = area_struct.corrections
	trespstr = CREATE_STRUCT(trespstr, 'Corrections', corrstr)
	fulltrespstr = CREATE_STRUCT(fulltrespstr, 'corrections', corrstr)
endif

RETURN, trespstr

end
