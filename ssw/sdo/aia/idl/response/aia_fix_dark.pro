function AIA_FIX_DARK, index, $
	silent = silent, nojsoc = nojsoc

;+
;
; Calculates a small (~0.1 DN) temperature-dependent correction to the pedestal
; for an AIA image; the result can be subtracted from each pixel in the image 
; (or from the datamean if using the band irradiance for sun-as-a-star applications)
;
; Looks only at the .tempccd and .wavelnth FITS keywords. Hard-coded correction relationship
; calculated using AIA_TREND_DARK, is valid up through 2014-09 (though to that 
; point there is no long-term time dependence in evidence) 
;
; KEYWORDS:
;	/silent 	-	If set, then don't print out messages if you have problems
;	/nojsoc		-	If not set, then it will query the JSOC for the CCD temperature
;					if it's not in the header
;
;-

silent = KEYWORD_SET(silent)

; If given a vector, just call itself recursively
if N_ELEMENTS(index) gt 1 then begin
	nind = N_ELEMENTS(index)
	result = DBLARR(nind)
	for i = 0, nind - 1 do result[i] = AIA_FIX_DARK(index[i], silent = silent)
	RETURN, result
endif

; Check that input temperature and wavelength are in range
tempccd = !values.f_nan
itags = TAG_NAMES(index)
tempind = WHERE(itags eq 'TEMPCCD', nummatch)
if nummatch lt 1 then begin
	if not KEYWORD_SET(nojsoc) then begin
		if not silent then PRINT, 'AIA_FIX_DARK: Getting TEMPCCD from JSOC...'
		SSW_JSOC_TIME2DATA, RELTIME(index.t_obs,min=-0.5), RELTIME(index.t_obs,min=0.5), $
			ds = 'aia.lev1', drms, key = 'TEMPCCD'
		if N_TAGS(drms) gt 0 then begin
			drmsmatch = WHERE(drms.fsn eq index.fsn, nummatch)
			if nummatch gt 0 then tempccd = drms[drmsmatch].tempccd
		endif
	endif
	if not FINITE(tempccd) then begin
		if not silent then BOX_MESSAGE, ['AIA_FIX_DARK: No CCD temperature in index']
		RETURN, 0.	
	endif
endif else begin
	tempccd = index.tempccd
endelse

if tempccd gt -60 then begin
	if not silent then BOX_MESSAGE, ['AIA_FIX_DARK: TempCCD too high (max=-60)', tempccd]
	RETURN, 0.	
endif

if tempccd lt -80 then begin
	if not silent then BOX_MESSAGE, ['AIA_FIX_DARK: TempCCD too low (min=-80)', tempccd]
	RETURN, 0.	
endif

wvls = [94, 131, 171, 193, 211, 304, 335, 1600, 1700]
wvlind = WHERE(index.wavelnth eq wvls, nummatch)
if nummatch lt 1 then begin
	if not silent then BOX_MESSAGE, ['AIA_FIX_DARK: No correction for this wavelength', index.wavelnth]
	RETURN, 0.	
endif

aiaresp=GET_LOGENV('AIA_RESPONSE_DATA')
if aiaresp eq '' then aiaresp=CONCAT_DIR('$SSW_AIA','response')
tfitfile = FILE_SEARCH(CONCAT_DIR(aiaresp, '*tfits*'))
if tfitfile eq '' then begin
	if not silent then BOX_MESSAGE, ['AIA_FIX_DARK: No tfit file found!']
	RETURN, 0.	
endif

RESTGEN, file = tfitfile, tfits
tfitsize = SIZE(tfits)
corr = tfits[wvlind, 0]
for i = 1d, tfitsize[2] - 1 do corr = corr + tfits[wvlind,i] * tempccd ^ i
RETURN, corr

end