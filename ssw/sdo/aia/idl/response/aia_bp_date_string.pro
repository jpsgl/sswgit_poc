function AIA_BP_DATE_STRING, time = time, local = local

;+
; $Id: aia_bp_date_string.pro,v 1.1 2010/05/28 22:31:25 boerner Exp $
;
; $Log: aia_bp_date_string.pro,v $
; Revision 1.1  2010/05/28 22:31:25  boerner
; Initial version
;
;
; Returns an 8-char string giving the current UTC date in YYYYMMDD format
;
; If keyword /time is set, also returns the 6 character time in YYYYMMDD_HHMMSS
;
; If keyword /local is set, then the local time and date is used, instead of UTC
;-

use_utc = not KEYWORD_SET(local)
datevect = BIN_DATE(SYSTIME(utc = use_utc))

if datevect[1] ge 10 then month = STRTRIM(datevect[1], 2) $
	else month = '0' + STRTRIM(datevect[1], 2)

if datevect[2] ge 10 then day = STRTRIM(datevect[2], 2) $
	else day = '0' + STRTRIM(datevect[2], 2)

datestring = STRTRIM(datevect[0],2) + month + day

if KEYWORD_SET(time) then begin
	if datevect[3] ge 10 then hour = STRTRIM(datevect[3], 2) $
		else hour = '0' + STRTRIM(datevect[3], 2)
	if datevect[4] ge 10 then minute = STRTRIM(datevect[4], 2) $
		else minute = '0' + STRTRIM(datevect[4], 2)
	if datevect[5] ge 10 then second = STRTRIM(datevect[5], 2) $
		else second = '0' + STRTRIM(datevect[5], 2)
	datestring = datestring + '_' + hour + minute + second
endif

RETURN, datestring

end
