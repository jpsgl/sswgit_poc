pro AIA_BP_COMBINE_MIRRORS, infile1, infile2, outfile

;+
;
; $Log: aia_bp_combine_mirrors.pro,v $
; Revision 1.1  2010/07/21 18:50:04  boerner
; Initial version
;
;
;-


m1 = AIA_BP_READ_COMP(infile1)
m2 = AIA_BP_READ_COMP(infile2)

w1 = REFORM(m1[0,*])
w2 = REFORM(m2[0,*])
wall = [w1, w2]
wsort = SORT(wall)

r1 = m1[1,*]
r2 = m2[1,*]

wout = wall[wsort]
r1out = INTERPOL(r1, w1, wout)
r2out = INTERPOL(r2, w2, wout)

rall = r1out + r2out

OPENW, lun, /get_lun, outfile
PRINTF, lun, '; Written by AIA_BP_COMBINE_MIRRORS on ' + SYSTIME(0)
PRINTF, lun, '; Combined reflectance from '
PRINTF, lun, '; ' + infile1 + ' and'
PRINTF, lun, '; ' + infile2
PRINTF, lun
PRINTF, lun, 'Wavelength [A]', 'Reflectance', format = '(2a20)'

for i = 0, N_ELEMENTS(wsort)-1 do $
	PRINTF, lun, wout[i], rall[i], format = '(2f20.8)'

FREE_LUN, lun

end
