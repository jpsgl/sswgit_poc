function AIA_BP_READ_COMP, filename

;+
;
; $Id: aia_bp_read_comp.pro,v 1.1 2010/05/28 22:31:25 boerner Exp $
;
; $Log: aia_bp_read_comp.pro,v $
; Revision 1.1  2010/05/28 22:31:25  boerner
; Initial version
;
;
; Helper routine for AIA_BP_READ_AAAA.PRO programs. 
; 
; Reads in a comp.dat or comp.txt file (which contains data as wavelength
; and efficiency pairs) and returns the contents as a string array; 
; doesn't write anything. 
;
;-

result = RD_TFILE(filename, 2, /nocomm, /compress, /convert)

RETURN, result

end
