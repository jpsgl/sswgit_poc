;
;+
;Category: Utility
;
;
;Purpose:
;  plot corrections returned from aia_bp_get_corrections.pro,
;
;
;Calling Sequences:
;    aia_bp_get_corrections_plot, version=8, corr_euv=corr_euv, corr_uv=corr_uv
;
;
;Output:
; corr_euv: structure for AIA EUV channels effective area (EA) correction;
; corr_uv:  ... UV channels...
;
; Example:
; IDL> help,corr_euv, /st	; @ 2017/12/13 16:18:46
; ** Structure <a664638>, 10 tags, length=144888, data length=144876, refs=1:
;   VERSION         INT              8
;   RESPVERSION     STRING    Array[1]
;   TABLEFILE       STRING    Array[1]
;   UTC             STRING    Array[2783]
;   TAI             DOUBLE    Array[2783]
;   CHANNELS        INT       Array[7]      	; 94     131     171     193     211     304     335
;   EA_AIA_T0       FLOAT     Array[7]
;   EA_EVE_T0       FLOAT     Array[7]
;   EA_OVER_EA0     FLOAT     Array[2783, 7]	; ratio of EA(t)/EA(t=t0) that quantifies degradation
;   EA_AIA_OVER_EVE DOUBLE    Array[7]			; AIA-to-EVE normalization on day 0 of the SDO mission
;
;IDL> print, corr_euv.EA_AIA_OVER_EVE
;       1.0140375      0.94678238       1.2030335       1.2019872       1.0747562       1.6578861       1.3558810
;
;
; Files saved (by default, to the current directory):
;  aia_bp_get_corrections_plot_V*.png
;  aia_bp_get_corrections_plot_V*.sav: contain the structures of corr_euv, corr_uv
;
;
;History:
; 2017/11/30, Thu: Wei Liu, written at ~/idl_lib/ops/sdo/boerner/aia/weiliu_eve
;   workspace: /disk2/scr0/weiliu/sdo/docs/my-job/calib/eve/wkspc/run01_20170926_FISM-V2_PhilC/v1a_batch/cmpr_real-eve_v2_scale-model/response-table/v1_new-V8
;   followed instruction/example at file:///disk2/scr0/weiliu/sdo/docs/my-job/calib/eve/Paul/resp/aiaresp/AIA-Throughput-trending.html
; 2017/12/13: minor revision
;
;-

pro aia_bp_get_corrections_plot, version=version, yrange=yrange, outdat=outdat, _extra=_extra   $
  , corr_euv=corr_euv, corr_uv=corr_uv		;optional outputs

;--- set params ------------------------
checkvar, version, 6
checkvar, outdat, 'aia_bp_get_corrections_plot_V' +trim(version)

if ~keyword_set(yrange) then begin
 	; yrange=fltarr(2,9) & yrange[0,*]=0 & yrange[1,*]=y1
  y0=0.5
  y1=1.05
  yrange=[ [y0, y1], [y0, y1], [y0, y1] $
         , [y0, y1], [y0, y1], [0.0, y1] $ 
         , [0.0, y1], [0.5, y1], [0.5, y1] ]
endif

checkvar, charsize, 2.5
window, 10, xs=2000, ys=1200

 !p.color=0
 !p.background=255

!p.multi=[0,3,3,0]

;--- load correction data ------------------------
this_version= version	; protect version from being overwritten from the call; but this_version could be modified upon return here
corr_euv = aia_bp_get_corrections(version=this_version, _extra=_extra)
this_version= version
corr_uv = aia_bp_get_corrections(version=this_version, _extra=_extra, /uv)

	;neuv= N_elements(corr_euv.channels)
	;nuv= N_elements(corr_uv.channels)

;--- make plots ------------------------

for j=0, 1 do begin		;--- loop over EUV, the UV channels -------
 if j eq 0 then corr= corr_euv $
   else corr= corr_uv
 nchans= N_elements(corr.channels)
 for i=0, nchans-1 do begin	;--- loop over channels within each category ----
  utplot, corr.utc, corr.ea_over_ea0[*,i], yrange=yrange[*,i], chars=charsize, /xstyle, /ystyle  $
    , title='AIA Effective Area Correction Factor, Version: ' + trim(version) + ', Channel: ' + trim(corr.channels[i]), ytitle='EA/EA0 ratio'  $
    , labelpar=[3,10], /y2k
  endfor
endfor

!p.multi=0

;--- save output ------------------------
x2png, outdat + '.png'
save, filename=outdat + '.sav', corr_euv, corr_uv, description='corr_euv, corr_uv', /compress

end
