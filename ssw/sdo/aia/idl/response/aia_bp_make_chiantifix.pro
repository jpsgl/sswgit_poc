pro AIA_BP_MAKE_CHIANTIFIX, $
	make_flare_data = make_flare_data, make_quiet_data = make_quiet_data, $
	onlyfull = onlyfull, fit_eve_dems = fit_eve_dems, fit_flare_resp = fit_flare_resp, $
	check_flare = check_flare, fit_cool_resp = fit_cool_resp, get_response = get_response, $
	check_daily = check_daily, input_tresp = input_tresp, version = version, eps = eps, $
	save_data = save_data, write_plots = write_plots, pstop = pstop, silent = silent, $
	final4 = final4, veve = veve, emversion = emversion, _extra = extra
	
;+
;
; KEYWORD_PARAMETERS:
;	/make_flare_data	-	If set, then the flare data are read in from the
;							AIA and EVE archives and repacked into a GENX file
;							By default, a previously-written GENX file is read 
;							in
;
;	/make_quiet_data	-	If set, then the quiet (daily) data are read in 
;							from the AIA and EVE archives and repacked into a GENX file
;							By default, a previously-written GENX file is read 
;							in
;
;	/onlyfull			-	If set (and make_quiet_data is set), then only days
;							where the full MEGS spectrum (out to 660 A) is 
;							available are included
;
;	input_tresp=		-	Can be set to a 2 (or more)-element array of temperature
;							response functions which are used as the output of 
;							the flare fit and the input to the cool-end fit
;							(useful if you want to kluge a flare fit response and
;							proceed with the cool-end fitting)
;
;	/final4				-	If set, then a handful of hard-wired kluges are applied
;							to give the best agreement with V4 data
;
;	/fit_eve_dems		-	If set, then the DEMs are fit to the EVE data
;
;	/fit_flare_resp		-	If set, then the overall DEM normalization is adjusted
;							by fitting to the X2 Valentine's day flare
;
;	/fit_cool_resp		-	If set, then the cool end of the T response is adjusted
;							by fitting to a long time series of AIA/EVE data
;
;	/check_flare		-	If set, then some plots are generated to look in more
;							detail at the consistency of the flare fit results
;
;	/check_daily		-	If set, then some plots are generated to look in more
;							detail at the consistency of the daily fit results
;
;	version				-	Set to the version of the response functions and
;							emissivity to use (defaults to 4)
;
;	emversion=			-	Set to the version of the emissivity to use (defaults
;							to use the same as version)
;
;	veve 				-	version of the EVE data to use
;
;	/eps				-	If set, then postscript plots are generated instead of
;							X windows
;
;	/save_data			-	If set, then the results of make_flare_data, make_quiet_data,
;							and fit_eve_dems are saved in GENX files
;
;	/write_plots		-	If set, then the plots are saved as PNGs
;
;	/pstop				-	Switch to stop inside routine for debugging
;
;	/silent				-	If set, then no plots are generated
;
;-

t0 = SYSTIME(/sec)
runtime = TIME2FILE(RELTIME(/now), /seconds)

outpath = '/Volumes/disk2/data/aia/dem/' + runtime
wvls	= [94, 131, 171, 193, 211, 335]
wvlcols	= [255, 5, 4, 3, 2, 6]

if not KEYWORD_SET(version) then version = 6
if not KEYWORD_SET(emversion) then emversion = version
if not KEYWORD_SET(veve) then veve = 4
if not KEYWORD_SET(silent) then begin
	oldpmulti = !p.multi
	oldpfont = !p.font
	TVLCT, rr, gg, bb, /get
	if KEYWORD_SET(eps) then begin
		SET_PLOT, 'ps'
	endif
endif
if KEYWORD_SET(save_data) or KEYWORD_SET(write_plots) then begin
	PRINT, 'AIA_BP_MAKE_CHIANTIFIX: Saving data/plots to ', outpath
	PRINT, 'AIA Version', 'EM Version', 'EVE Version', format = '(3a20)'
	PRINT, version, emversion, veve, format = '(3i20)'
	SPAWN, 'mkdir ' + outpath, result, errcode
endif
if not KEYWORD_SET(eps) then eps = 0
if not KEYWORD_SET(write_plots) then write_plots = 0

;++++++++++++++++++++++++++++++++++++++++++++++++++++++++
; The final corrections that go into V4 do not come out
; of this routine automatically. They require some 
; quasi-manual tweaking, as follows:
;--------------------------------------------------------
if KEYWORD_SET(final4) then begin
	raw_tresp = AIA_GET_RESPONSE(/temp, /dn, /eve, ver=4, emver=4)
	tresp_out = [raw_tresp.a94, raw_tresp.a131, raw_tresp.a171, raw_tresp.a193, raw_tresp.a211, raw_tresp.a335]
	tresp_out[0].tresp = tresp_out[0].tresp * 0.7
	tresp_out[1].tresp[WHERE(tresp_out[1].logte ge 6.7)] = tresp_out[1].tresp[WHERE(tresp_out[1].logte ge 6.7)] * 0.81
	fitco = FLTARR(6, 6)
	fitco[0,0] = ALOG10(0.7)
	fitco[0,1] = ALOG10(0.79)
	input_tresp = tresp_out
endif

;++++++++++++++++++++++++++++
; Load response functions and
; set up some other generally
; useful variables
;----------------------------
if KEYWORD_SET(get_response) then begin
	emiss	= AIA_GET_RESPONSE(version = emversion, /emiss)
	tresps	= AIA_GET_RESPONSE(version = version, emversion = emversion, /temp, /dn)
	wresps	= AIA_GET_RESPONSE(version = version, /area, /dn)			
	ptresps	= AIA_GET_RESPONSE(version = version, emversion = emversion, /temp, /phot)			
	pwresps	= AIA_GET_RESPONSE(version = version, /area, /phot)			
	corrstr	= AIA_BP_GET_CORRECTIONS(version = version)
	tresp_tags = TAG_NAMES(tresps)
	wresp_tags = TAG_NAMES(wresps)
	respfile = FILE_SEARCH('/net/crom/Volumes/disk2/data/aia/response/aia_V' + STRTRIM(version, 2) + '*_response_table.txt')
	resptab = AIA_BP_READ_RESPONSE_TABLE(respfile[-1])
	
	if KEYWORD_SET(save_data) then begin
		response_str = CREATE_STRUCT('tresps', tresps, 'wresps', wresps, $
			'ptresps', ptresps, 'pwresps', pwresps, 'corrstr', corrstr, 'emiss', emiss, $
			'tresp_tags', tresp_tags, 'wresp_tags', wresp_tags, 'resptab', resptab)
		SAVEGEN, file = outpath + '/response.genx', str = response_str
	endif

	PRINT & PRINT, 'Elapsed time (loading response functions) : ', SYSTIME(/sec) - t0, form = '(a50, f10.1)'
	if KEYWORD_SET(pstop) then STOP

endif else begin
	case version of 
		4	:	case emversion of
					4	:	respfile = '/Volumes/disk2/data/aia/dem/20130121_213632/response.genx'
					5	:	respfile = '/Volumes/disk2/data/aia/dem/20130711_203937/response.genx'
				endcase
		5	:	case emversion of
					4	:	respfile = '/Volumes/disk2/data/aia/dem/20130711_203152/response.genx'
					5	:	respfile = '/Volumes/disk2/data/aia/dem/20130711_002419/response.genx'
				endcase
		6	:	case emversion of
					6	:	respfile = ''
					else:	respfile = ''
				endcase
		else:	case emversion of
					4	:	respfile = ''
					5	:	respfile = ''
				endcase
	endcase
	RESTGEN, file = respfile, str = response_str
	tresps		= response_str.tresps
	wresps		= response_str.wresps
	ptresps		= response_str.ptresps
	pwresps		= response_str.pwresps
	corrstr		= response_str.corrstr
	emiss		= response_str.emiss
	tresp_tags	= response_str.tresp_tags
	wresp_tags	= response_str.wresp_tags
	resptab		= response_str.resptab
endelse

;+++++++++++++++++++
; Get the X2 flare
; data
;-------------------
if KEYWORD_SET(make_flare_data) then begin		; Read (and write) the flare data from AIA and EVE

	; Get the EVE data for the Valentine's day flare
	epath = '/Volumes/disk2/data/eve/archive/v' + STRTRIM(veve, 2) + '/2011/'
	sfilt = 'EVS_L2_2011046_0[12]*'
	sff = FILE_SEARCH(epath + sfilt)
	eve_flare_obs_str = PB_READ_EVE(sff, /flat)

	; Reformat the EVE flare data and create some useful variables
	evewave = eve_flare_obs_str.wave
	flare_tai = eve_flare_obs_str.tai
	flare_tai0 = flare_tai[300]
	preflare_tai = MEAN(flare_tai[0:295])

	; Get the EVE pre-flare spectrum and background-subtracted flare spectra
	preflare_spec = TOTAL(eve_flare_obs_str.spec[*,0:294], 2) / 295.
	preflare_spec_square = REBIN(preflare_spec, 5200, 720)
	flarespec = eve_flare_obs_str.spec - preflare_spec_square

	; Get the AIA data for the Valentine's day flare
	atstart = '15-feb-2011 01:00'
	atstop = '15-feb-2011 03:00'
	ads = 'aia.lev1'
	SSW_JSOC_TIME2DATA, atstart, atstop, aiadat, /jsoc2, ds = ads, $
		keyw='t_obs,datamean,exptime,quality,wavelnth,dsun_obs,dsun_ref'
	aia_flare_obs_str = aiadat[WHERE(aiadat.wavelnth lt 400 and aiadat.quality eq 0)]
	
	; Clean up and re-grid the AIA flare observations so they better
	; match the EVE flare observations
	aia_flare_tai = DBLARR(600, 6)			;	AIA time grid
	aia_flare_obs_norm = DBLARR(600, 6)		;	AIA EUV data on native t grid, but adjusted for exptime and 1AU
	aia_flare_obs_evetime = DBLARR(720, 6)	;	AIA EUV data on EVE t grid
	aia_flare_obs_tcorr = DBLARR(720, 6)	;	AIA EUV data on EVE t grid, corrected for AIA degradation
	aia_preflare_obs = DBLARR(6)			;	Pre-flare average in each channel
	aia_flare_obs_bgsub = DBLARR(720, 6)	;	Same as _tcorr with pre-flare background subtracted out
	corrstr = AIA_BP_GET_CORRECTIONS(version = version)
	
	for i = 0, 5 do begin				;		Loop through the 6 Fe channels
		ids = WHERE(aia_flare_obs_str.wavelnth eq wvls[i], numthischan)
		aia_flare_tai[*,i] = ANYTIM2TAI(aia_flare_obs_str[ids].t_obs)
		aia_flare_obs_norm[*,i] = aia_flare_obs_str[ids].datamean / aia_flare_obs_str[ids].exptime * $
			(aia_flare_obs_str[ids].dsun_obs / aia_flare_obs_str[ids].dsun_ref)^2.
		aia_flare_obs_evetime[*,i] = INTERPOL(aia_flare_obs_norm[*,i], aia_flare_tai[*,i], $
			flare_tai)
		corrstr_ind = WHERE(corrstr.channels eq wvls[i])
		tcorr = INTERPOL(corrstr.ea_over_ea0[*, corrstr_ind], corrstr.tai, flare_tai[*])
		aia_flare_obs_tcorr[*,i] = aia_flare_obs_evetime[*,i] / tcorr
		aia_preflare_obs[i] = TOTAL(aia_flare_obs_tcorr[0:294,i], 1) / 295.
		aia_flare_obs_bgsub[*,i] = aia_flare_obs_tcorr[*,i] - aia_preflare_obs[i]
	endfor

	; Save what is needed from the processed flare data
	if KEYWORD_SET(save_data) then begin
		flare_data = CREATE_STRUCT('evewave', evewave, 'flare_tai', flare_tai, $
			'flare_tai0', flare_tai0, 'preflare_tai', preflare_tai, $
			'aia_flare_obs_bgsub', aia_flare_obs_bgsub, 'aia_preflare_obs', aia_preflare_obs, $
			'flarespec', flarespec, 'preflare_spec', preflare_spec)
		SAVEGEN, file = outpath + '/flare_data.genx', str = flare_data
	endif
	
	PRINT & PRINT, 'Elapsed time (make flare data) : ', SYSTIME(/sec) - t0, form = '(a50, f10.1)'
	if KEYWORD_SET(pstop) then STOP

endif else begin
	; use pre-saved batch of flare observations
	case veve of
		2	:	evefile = '/Volumes/disk2/data/aia/dem/20130121_213632/flare_data.genx'
		3	:	evefile = '/Volumes/disk2/data/aia/dem/20130612_191433/flare_data.genx'
		4	:	evefile = '/Volumes/disk2/data/aia/dem/20140319_235807/flare_data.genx'
	endcase
	RESTGEN, file = evefile, str = flare_data
	evewave 			=	flare_data.evewave
	flare_tai 			=	flare_data.flare_tai
	flare_tai0			=	flare_data.flare_tai0
	preflare_tai		=	flare_data.preflare_tai
	aia_flare_obs_bgsub	=	flare_data.aia_flare_obs_bgsub
	aia_preflare_obs	=	flare_data.aia_preflare_obs
	flarespec			=	flare_data.flarespec
	preflare_spec		=	flare_data.preflare_spec
endelse

;+++++++++++++++++++
; Get the quiet 
; (daily) data
;-------------------
if KEYWORD_SET(make_quiet_data) then begin	; Read (and write) the daily data from AIA and EVE

	daily_path = '/net/crom/Volumes/disk2/data/eve/aiafiles/'
	case veve of
		2	:	daily_file = daily_path + '20121016_153130_EVSshort_aia-eve_ratios.genx'
		3	:	daily_file = daily_path + '20130610_102019_EVSshort34_aia-eve_ratios.genx'
		4	:	daily_file = daily_path + '20140508_161740_EVSshort44_aia-eve_ratios.genx'
	endcase
	AIA_EVE_PLOT_RATIOS, /spec, aepr_result, aepr_aia, aepr_evespec, aepr_evedat, /silent, $
		file = daily_file, /load

	; Create some useful variables
	daily_tai = ANYTIM2TAI(aepr_result.date)
	num_daily = N_ELEMENTS(daily_tai)
	chans = aepr_result.chans

	; The AIA daily observations have already been normalized and re-gridded to
	; match EVE, but it is still necessary to divide out the degradation
	aia_daily_obs	= DBLARR(num_daily, 6)	;	AIA EUV data on EVE t grid, corrected for AIA degradation
	eve_daily_obs	= DBLARR(num_daily, 6)	;	Corresponding EVE measurements of predicted AIA flux
	for i = 0, 5 do begin				;		Loop through the 6 Fe channels
		aepr_ind = WHERE(chans eq wvls[i])
		corrstr_ind = WHERE(corrstr.channels eq wvls[i])
		tcorr = INTERPOL(corrstr.ea_over_ea0[*, corrstr_ind], corrstr.tai, daily_tai)
		aia_daily_obs[*,i] = aepr_result.aia[aepr_ind, *] / tcorr
		eve_daily_obs[*,i] = aepr_result.eve[aepr_ind, *]
	endfor

	; If requested, trim out EVE data without MEGS-B
	full_ind = WHERE(aepr_evespec[3000,*] gt 0)
	if KEYWORD_SET(onlyfull) then quietinds = full_ind else quietinds = INDGEN(N_ELEMENTS(daily_tai))
	aia_daily_obs = aia_daily_obs[quietinds, *]
	eve_daily_obs = eve_daily_obs[quietinds, *]
	eve_daily_spec = aepr_evespec[*,quietinds]
	daily_tai = daily_tai[quietinds]
	
	quiet_obs = CREATE_STRUCT('daily_tai', daily_tai, 'aia_daily_obs', aia_daily_obs, $
		'eve_daily_obs', eve_daily_obs, 'eve_daily_spec', eve_daily_spec, 'chans', chans)

	if KEYWORD_SET(save_data) then SAVEGEN, file = outpath + '/quiet_obs.genx', str = quiet_obs

	PRINT & PRINT, 'Elapsed time (make quiet data) : ', SYSTIME(/sec) - t0, form = '(a50, f10.1)'
	if KEYWORD_SET(pstop) then STOP

endif else begin
	;use pre-saved batch of daily observations
	case veve of
		2	:	evefile = '/Volumes/disk2/data/aia/dem/20130121_213632/quiet_obs.genx'
		3	: 	evefile = '/Volumes/disk2/data/aia/dem/20130612_191433/quiet_obs.genx'
		4	: 	evefile = '/Volumes/disk2/data/aia/dem/20140509_000921/quiet_obs.genx'
	endcase
	RESTGEN, file = evefile, str = quiet_obs
	aia_daily_obs 	= quiet_obs.aia_daily_obs
	eve_daily_obs	= quiet_obs.eve_daily_obs
	eve_daily_spec	= quiet_obs.eve_daily_spec
	daily_tai		= quiet_obs.daily_tai
	chans			= quiet_obs.chans
endelse

;++++++++++++++++++++++++++++++++
; Get the DEMs for the EVE data
;--------------------------------
if KEYWORD_SET(fit_eve_dems) then begin
	; Load the specified version of the emissivity to use for the DEM fitting
	common emiss_block, dem_emiss
	dem_emiss = emiss

	; Fit the preflare background for a representative quiet sun spectrum
	AIA_EVE_DEM_FIT_LINES_MULTI, evewave, preflare_spec, preflare_tai, $
		result = preflare_dem, _extra = extra

	; Fit preflare-subtracted spectra during the flare
	AIA_EVE_DEM_FIT_LINES_MULTI, evewave, flarespec[*,300:*] > 0, flare_tai, $
		result = postflare_dem, /flare, _extra = extra

	; Fit the daily observations
	AIA_EVE_DEM_FIT_LINES_MULTI, evewave, eve_daily_spec, daily_tai, $
		result = daily_dem, _extra = extra
	
	; Save the results
	if KEYWORD_SET(save_data) then begin
		eve_dem_str = CREATE_STRUCT('preflare_dem', preflare_dem, $
			'postflare_dem', postflare_dem, 'daily_dem', daily_dem)
		SAVEGEN, file = outpath + '/eve_dem.genx', str = eve_dem_str
	endif
	
	PRINT & PRINT, 'Elapsed time (fit EVE DEMs) : ', SYSTIME(/sec) - t0, form = '(a50, f10.1)'
	if KEYWORD_SET(pstop) then STOP

endif else begin
	case veve of
		2	:	evefile = '/Volumes/disk2/data/aia/dem/20130121_213632/eve_dem.genx'
		3	:	evefile = '/Volumes/disk2/data/aia/dem/20130612_191433/eve_dem.genx'
		4	:	evefile = '/Volumes/disk2/data/aia/dem/20140509_000921/eve_dem.genx'
	endcase
	RESTGEN, file = evefile, str = eve_dem_str
	preflare_dem	= eve_dem_str.preflare_dem
	postflare_dem	= eve_dem_str.postflare_dem
	daily_dem		= eve_dem_str.daily_dem
endelse

;+++++++++++++++++++++++++++++++
; Do the fit for the flare data
;-------------------------------
if KEYWORD_SET(fit_flare_resp) then begin

	coeff_val 	= [	[-0.1,	0,	0,	0,	0,	0], $	;	94
					[-0.2,	0,	0,	0,	0,	0], $	;	131
					[-0.0,	0,	0,	0,	0,	0], $	;	171
					[-0.0,	0,	0,	0,	0,	0], $	;	193
					[-0.0,	0,	0,	0,	0,	0], $	;	211
					[-0.0,	0,	0,	0,	0,	0] ]	;	335
	coeff_mask = [ 	[1, 0, 0, 0, 0, 0], $	;	94
					[1, 0, 0, 0, 0, 0], $	;	131
					[1, 0, 0, 0, 0, 0], $	;	171
					[1, 0, 0, 0, 0, 0], $	;	193
					[1, 0, 0, 0, 0, 0], $	;	211
					[1, 0, 0, 0, 0, 0] ]	;	335
	fitco = FLTARR(6,6)
	satpoint = [1.5, 5, 2, 25, 8, 1]
	plim = [2.5, 10, 5, 40, 10, 2]

	; Perform the flare fit
	for i = 0, 5 do begin
		thistresp = tresps.(WHERE(tresp_tags eq 'A' + STRTRIM(wvls[i], 2)))
		unsat_ind = WHERE(aia_flare_obs_bgsub[300:*,i] lt satpoint[i] , numunsat)
		fdem_fit = AIA_FIT_TRESP(postflare_dem[unsat_ind], aia_flare_obs_bgsub[300+unsat_ind,i]>0, thistresp, $
			pre_pred, post_pred, coeff_val = coeff_val[*,i], coeff_mask = coeff_mask[*,i], $
			fit_coeffs = fit_coeffs, /silent)
		fitco[*,i] = fit_coeffs
		if i eq 0 then tresp_out = fdem_fit else tresp_out = [tresp_out, fdem_fit]
		; Generate some summary plots of the flare fit
		if not KEYWORD_SET(silent) then begin
			AIA_BP_CHIANTIFIX_PLOT_FLAREFIT, i, flare_tai, aia_flare_obs_bgsub, $
				post_pred, pre_pred, unsat_ind, thistresp, fdem_fit, fit_coeffs, plim, $
				eps = eps, outpath = outpath, write_plots = write_plots
		endif
		STOP
	endfor

	; Save the results
	if KEYWORD_SET(save_data) then begin
		flare_fit_str = CREATE_STRUCT('tresp_out', tresp_out, $
			'fitco', fitco)
		SAVEGEN, file = outpath + '/flare_fit.genx', str = flare_fit_str
	endif

	PRINT & PRINT, 'Elapsed time (fitting flare data) : ', SYSTIME(/sec) - t0, form = '(a50, f10.1)'
	if KEYWORD_SET(pstop) then STOP

endif else begin
	if not KEYWORD_SET(final4) then begin
		case veve of
			2	:	evefile = '/Volumes/disk2/data/aia/dem/20130121_213632/flare_fit.genx'
			3	:	evefile = '/Volumes/disk2/data/aia/dem/20130612_191433/flare_fit.genx'
			4	:	evefile = '/Volumes/disk2/data/aia/dem/20140507_230957/flare_fit.genx'
		endcase
		RESTGEN, file = evefile, str = flare_fit_str
		tresp_out = flare_fit_str.tresp_out
		fitco = flare_fit_str.fitco
	endif
endelse

;+++++++++++++++++++++++++++++++
; Do some checking (compare 
; flare norm with wave norm / 
; wave resolution / DEM fit err)
;-------------------------------
if KEYWORD_SET(check_flare) and not KEYWORD_SET(silent) and not KEYWORD_SET(eps) then begin

	aia_wvl_preds 		= aia_flare_obs_bgsub * 0				;	(AIA Raw  Wresp * EVE Obs Spec)
	aia_wvl_preds_corr	= aia_flare_obs_bgsub * 0				;	(AIA Corr Wresp * EVE Obs Spec)
	aia_dem_preds 		= FLTARR(N_ELEMENTS(postflare_dem), 6)	;	(AIA Raw  Wresp * EVE DEM Spec)
	aia_dem_preds_corr 	= FLTARR(N_ELEMENTS(postflare_dem), 6)	;	(AIA Corr Wresp * EVE DEM Spec)
	trendfactor = FLTARR(6)										;	Ratio of AIA EA (with T1 and EVEnorm) to Raw EA
	for i = 0, 5 do begin
		thiswresp = pwresps.(WHERE(wresp_tags eq 'A' + STRTRIM(wvls[i], 2)))
		this_respind = WHERE(resptab.wavelnth eq wvls[i])
		this_corrind = WHERE(corrstr.channels eq wvls[i])
		evecorr = corrstr.ea_aia_over_eve[this_corrind]
		tcorr = INTERPOL(corrstr.ea_over_ea0[*, this_corrind], corrstr.tai, MEDIAN(flare_tai[*]))
		resp_str = {wave : thiswresp.wave, elecperdn : resptab[this_respind[0]].eperdn, $
			effarea : thiswresp.ea}
		trendfactor[i] = tcorr * evecorr[0]
		aia_wvl_preds[*,i] = EVE2AIA(flarespec, resp_str, thrupred) 
		aia_wvl_preds_corr[*,i] = aia_wvl_preds[*,i] * trendfactor[i]
		aia_dem_preds[*,i] = EVE2AIA(postflare_dem.pred_spec, resp_str, thrupred)
		aia_dem_preds_corr[*,i] = aia_dem_preds[*,i] * trendfactor[i]
	endfor
	eaexcess = aia_flare_obs_bgsub[300:*,*] / aia_wvl_preds_corr[300:*,*]	;	What you need to multiply the AIA EA by to match the EVE observations
	eaex_mean = MEAN(eaexcess, dim = 1)
	band_demex = aia_dem_preds / aia_wvl_preds[300:*,*]	;	How much the EVE flare DEM spectrum overestimates
														;	the EVE observed spectrum within each of the AIA
														; 	bands
	banddemex_mean = 10.^(MEDIAN(ALOG10(band_demex), dim = 1))
	no171 = [0,1,3,4,5]

	;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	; Look at how well the AIA effective area (including the
	; rough normalization applied based on long-term trending)
	; agrees with what EVE observes during the flare. A
	; discrepancy from 1 here indicates that the EA at the flare 
	; line is off
	;-----------------------------------------------------------
	WINDOW, 10
	UTPLOT, TAI2UTC(flare_tai[300:*]), eaexcess[*,0], yrange = [0.5, 1.5], title = 'Simple Effective Area Comparison', $
		ytitle = 'Ratio (AIA observed) over (EVE observed x AIA Effective Area)', chars = 1.5
	;OUTPLOT, TAI2UTC(LIMITS(flare_tai[300:*])), trendfactor[0]+[0,0], col = 255, line = 2
	for i = 1, 5 do if i ne 2 then begin
		OUTPLOT, TAI2UTC(flare_tai[300:*]), eaexcess[*,i], col = wvlcols[i]
	;	OUTPLOT, TAI2UTC(LIMITS(flare_tai[300:*])), trendfactor[i]+[0,0], col = wvlcols[i], line = 2
	endif
	legend_string = STRTRIM(wvls[no171], 2) + P_ANG() + ' (' + STRING(eaex_mean[no171], form = '(f5.2)') + ')'
	PB_LEGEND, legend_string, /dev, 600, 750, /line, col = wvlcols[no171], size = 1.5
	if KEYWORD_SET(write_plots) then begin
		fea_fname = outpath + '/fea_fit.png'
		PB_WIN2PNG, fea_fname
	endif

	;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	; Figure out how closely you expect the AIA observations
	; to match the predictions by looking at how well the EVE
	; DEMs reproduce the EVE observations in those channels.
	; (Basically a measure of CHIANTI and your DEM fitter)
	; OK for 131, a little high for 94 (EVE DEM spec higher
	; than EVE Obs spec in that region)
	;-----------------------------------------------------------
	WINDOW, 11
	UTPLOT, TAI2UTC(flare_tai[300:*]), band_demex[*,0], yrange = [0., 1.5], chars = 1.5, title = 'Accuracy of Flare DEMs in band', $
		ytitle = 'Ratio (EVE DEM Spec x AIA W-resp) over (Observed EVE spectrum x AIA W-resp)'
	for i = 1, 5 do if i ne 2 then OUTPLOT, TAI2UTC(flare_tai[300:*]), band_demex[*,i], col = wvlcols[i]
	PB_LEGEND, STRTRIM(wvls[no171], 2) + P_ANG(), /dev, 600, 750, /line, col = wvlcols[no171], size = 1.5
	if KEYWORD_SET(write_plots) then begin
		fcount_fname = outpath + '/fcount_fit.png'
		PB_WIN2PNG, fcount_fname
	endif

	;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	; Calculate a correction for the effect of EVE spectral resolution
	; to account for the fact that you'd expect the simple effective
	; area comparison above to be off by a little bit based on EVE's
	; resolution.
	;-------------------------------------------------------------------
	finecounts_dem	= FLTARR(420, 6)	;	(AIA raw Tresp * EVE DEM)
	finecounts_spec	= FLTARR(420, 6)	;	(AIA raw Wresp * (EVE DEM spec, no EVE blurring))
	blurcounts		= FLTARR(420, 6)	;	(AIA raw Wresp * (EVE DEM spec))

	for i = 0, 419 do begin
		thisdemstr 	= {logte : postflare_dem[i].logt_dem, logdem : postflare_dem[i].dem}
		thisspec 	= AIA_BP_EMISS2SPEC(emiss, dem = thisdemstr)
		thisespec	= AIA_SPECTRUM2EVE(thisspec.spec, thisspec.wave, thisewave, /shift)
		for j = 0, 5 do begin
			thistresp = tresps.(WHERE(tresp_tags eq 'A' + STRTRIM(wvls[j], 2)))
			thiswresp = wresps.(WHERE(wresp_tags eq 'A' + STRTRIM(wvls[j], 2)))
			finecounts_dem[i,j] = AIA_BP_MAKE_COUNTS_DEM(/silent, thistresp, thisdemstr)
			finecounts_spec[i,j] = AIA_BP_MAKE_COUNTS_SPEC(/silent, thiswresp, thisspec)
			blurcounts[i,j] = AIA_BP_MAKE_COUNTS_SPEC(/silent, thiswresp, {wave : thisewave, spec : thisespec})
		endfor
	endfor
	blurratio = blurcounts / finecounts_spec	;	The ratio of the count rate predicted with a DEM
												;	spectrum blurred to EVE resolution and a count
												;	rate predicted with the same spectrum predicted at
												;	0.1 A unblurred resolution taken "straight"
												; 	through the wavelength resp.
	blurratio_mean = 10.^(MEDIAN(ALOG10(blurratio), dim = 1))

	; pmm, finecounts_dem / finecounts_spec 		;	(these should be about the same, right?)

	WINDOW, 12
	UTPLOT, TAI2UTC(flare_tai[300:*]), blurratio[*,0], yrange = [0.6, 1.2], chars = 1.5, $
		title = 'Effect of EVE spectral resolution', $
		ytitle = 'Count rates predicting using CHIANTI spectrum   (EVE resolution) / (Raw resolution)'
	for i = 1, 5 do if i ne 2 then OUTPLOT, TAI2UTC(flare_tai[300:*]), blurratio[*,i], col = wvlcols[i]
	PB_LEGEND, STRTRIM(wvls[no171], 2) + P_ANG(), /dev, 600, 750, /line, col = wvlcols[no171], size = 1.5
	if KEYWORD_SET(write_plots) then begin
		fblur_fname = outpath + '/fea_blur.png'
		PB_WIN2PNG, fblur_fname
	endif

	;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	; Calculate a correction for the fact that the normalization factor 
	; obtained in the flare fit still doesn't give a perfect match to 
	; the data. This factor (missfit) / the factor above (band_demex) 
	; that accounts for the discrepancy in the EVE DEM fit 
	;------------------------------------------
	normfactor = 10.^fitco[0,*]		;	What it wants to multiply the tresp by to match the flare DEM
									;	(i.e. numbers less than 1 indicates too much EA or Tresp)
	normfactorarr = REBIN(normfactor, 420, 6)		 ;	Fit Normalization factor for each channel
	trendarr = REBIN(TRANSPOSE(trendfactor), 420, 6) ;	Trend normalization factor for each channel
	corrpred = finecounts_dem * normfactorarr	; 	DEM-predicted count rates using corrected response functions
	missfit = corrpred / aia_flare_obs_bgsub[300:*,*]	;	How much the corrected DEM prediction disagrees with the observations
									;	that were used to generate the correction (numbers less than 1 indicate
									;	that the normfactor is too low because the normalized response underpredicts
									;	the data)
	missfit_mean = 10.^(MEDIAN(ALOG10(missfit), dim = 1))
	predicted_normarr = ( eaexcess * missfit * blurratio / band_demex )
	
	;++++++++++++++++++++++++++++++++++++++++++
	; Show that the normalization factor with 
	; all these corrections looks about as we'd
	; expect it to (this should include all
	; the factors that go into the fit)
	;------------------------------------------
	WINDOW, 13
	norm_check_arr = normfactorarr / trendarr / predicted_normarr
	UTPLOT, TAI2UTC(flare_tai[300:*]), norm_check_arr[*,0], yrange = [0.7, 1.3], chars = 1.5, $
		title = 'Ratio of Best-fit Tresp normalization factor to Expected', $
		ytitle = 'Normfactor / (EffareaNorm * MissFit * BlurRatio / DEMexcess)'
	for i = 1, 5 do if i ne 2 then OUTPLOT, TAI2UTC(flare_tai[300:*]), norm_check_arr[*,i], col = wvlcols[i]
	PB_LEGEND, STRTRIM(wvls[no171], 2) + P_ANG(), /dev, 600, 750, /line, col = wvlcols[no171], size = 1.5
	if KEYWORD_SET(write_plots) then begin
		fcheck_fname = outpath + '/fea_check.png'
		PB_WIN2PNG, fcheck_fname
	endif

	; Print out a table of correction factors
	PRINT, INDGEN(8), format = '(8i15)'
	PRINT, 'Chan', 'Fit NormFactor', 'Simple EA Comp', 'Trend EA Comp', $
		'Blurring', 'DEM prediction', 'Miss-Fit', 'Predicted Norm', format = '(8a15)'
	div = REPLICATE('============', 8)
	PRINT, div, format = '(8a15)'
	for i = 0, 5 do begin
		PRINT, wvls[i], normfactor[i], eaex_mean[i], trendfactor[i], $
			blurratio_mean[i], banddemex_mean[i], missfit_mean[i], $
			eaex_mean[i] * missfit_mean[i] * blurratio_mean[i] / banddemex_mean[i] / trendfactor[i], $
			format = '(i15, 7f15.2)'
	endfor

	PRINT & PRINT, 'Elapsed time (checking flare data) : ', SYSTIME(/sec) - t0, form = '(a50, f10.1)'
	if KEYWORD_SET(pstop) then STOP

endif

;+++++++++++++++++++++++++++++++
; Do the fit for the low-T end
;-------------------------------
if KEYWORD_SET(fit_cool_resp) then begin
	coeff_val = [ [0, 0, -4.0, 0, 0, -3.0], [0, -1.50, 0, 0, -3, 0] ]	;	Tweaking this gives different results
	coeff_mask = [ [0, 0, 1, 0, 0, 1], [0, 1, 0, 0, 1, 0] ]				;	Tweaking this gives different results
	qfitco = FLTARR(6,2)
	qtresp_out = tresp_out

	; Perform the daily DEM fit
	for i = 0, 1 do begin
	
		this_orig_tresp = tresps.(WHERE(tresp_tags eq 'A' + STRTRIM(wvls[i], 2)))
		this_flarefit_tresp = tresp_out[i]
		if N_ELEMENTS(input_tresp) eq 0 then begin
			this_input_tresp = this_flarefit_tresp
		endif else begin
			this_input_tresp = input_tresp[i]
		endelse
		goodfitpoints = WHERE(aia_daily_obs[*,i] gt 0 and daily_dem.chisq gt 0, numgood)
		qdem_fit = AIA_FIT_TRESP(daily_dem[goodfitpoints], aia_daily_obs[goodfitpoints, i], $
			this_input_tresp, pre_pred, post_pred, coeff_val = coeff_val[*,i], $
			coeff_mask = coeff_mask[*,i], fit_coeffs = fit_coeffs, /silent)
		qfitco[*,i] = fit_coeffs
		qtresp_out[i] = qdem_fit
		; Generate some summary plots of the daily fit
		if not KEYWORD_SET(silent) then begin
			if KEYWORD_SET(eps) then begin
				!p.font = 0
				LOADCT, 0
				chars = 0.95
				if i eq 0 then begin
					!p.multi = [0, 1, 2]
					DEVICE, file = outpath + '/aia_eve_dem_psplot_qs.eps', $
						/portrait, /inches, xsize = 7, ysize = 6, $
						/color, /isolatin1, /encapsulate, /helvetica
					ymargin = [1,1]
					xtit = ''
					ylabel = '94 '
				endif else begin
					!p.multi = [1, 1, 2]
					ymargin = [4,1]
					ylabel = '131 '
				endelse
				plim = [1.5, 6]
				prescale = MEAN(pre_pred / aia_daily_obs[goodfitpoints, i])
				UTPLOT, TAI2UTC(daily_tai[goodfitpoints]), aia_daily_obs[goodfitpoints, i], thick = 6, $
					yrange = [0, plim[i]], charsize = chars, ytit = ylabel + 'Mean [DN]', /ymin, psym = -3
				PB_SET_LINE_COLOR
				OUTPLOT, TAI2UTC(daily_tai[goodfitpoints]), post_pred, col = 5, thick = 4
				OUTPLOT, TAI2UTC(daily_tai[goodfitpoints]), pre_pred/prescale, col = 2, line = 1
				OUTPLOT, TAI2UTC(daily_tai[goodfitpoints]), pre_pred, col = 2
				PB_LEGEND, ['AIA Observed', 'Predicted: DEM + Orig Tresp', 'Predicted: DEM + Adjusted Tresp', 'Predicted: DEM + Scaled Tresp'], $
					col = [255, 2, 5, 2], /norm, 0.2, 0.42, line = [0, 0, 0, 1], size = chars, /eps
				if i eq 1 then begin
					DEVICE, /close
					!p.font = oldpfont
					!p.multi = oldpmulti
					TVLCT, rr, gg, bb
				endif
			endif else begin
				; Plot Revised T response
				WINDOW, 10
				PLOT, this_orig_tresp.logte, this_orig_tresp.tresp, chars = 1.5, thick = 2, $
					xtitle = 'Log(T)', ytitle = 'T Response', xmargin = [12, 4], xrange = [5, 8], $
					title = STRTRIM(wvls[i], 2) + P_ANG() + ' Channel T response -- Disk Average Fit', $
					yrange = [0, MAX([this_orig_tresp.tresp, this_flarefit_tresp.tresp, qdem_fit.tresp])]
				OPLOT, this_input_tresp.logte, this_input_tresp.tresp, col = 2, thick = 2
				OPLOT, qdem_fit.logte, qdem_fit.tresp, col = 6, thick = 2, line = 2
				PB_LEGEND, /dev, col = [255, 2, 6], ['Original', 'Flare Normalized', 'Quiet Sun Adjusted'], 200, 600, $
					size = 1.5, line = [0, 0, 2]
				if KEYWORD_SET(write_plots) then begin
					qtresp_fname = outpath + '/qtresp_fit_' + STRTRIM(wvls[i], 2) + '.png'
					PB_WIN2PNG, qtresp_fname
				endif
				; Plot observed and predicted count rate
				WINDOW, 11
				plim = [1.5, 6]
				prescale = MEAN(pre_pred / aia_daily_obs[goodfitpoints, i])
				UTPLOT, TAI2UTC(daily_tai[goodfitpoints]), aia_daily_obs[goodfitpoints, i], thick = 2, $
					yrange = [0, plim[i]], charsize = 1.5, ytit = 'Full Disk Average Count Rate', $
					title = STRTRIM(wvls[i], 2) + P_ANG() + ' Channel Observed and Predicted Counts', psym = -3
				OUTPLOT, TAI2UTC(daily_tai[goodfitpoints]), post_pred, col = 6, thick = 2
				OUTPLOT, TAI2UTC(daily_tai[goodfitpoints]), pre_pred/prescale, col = 2, line = 1
				OUTPLOT, TAI2UTC(daily_tai[goodfitpoints]), pre_pred, col = 2
				PB_LEGEND, ['AIA Observed', 'Predicted: DEM + Orig Tresp', 'Predicted: DEM + Adjusted Tresp', 'Predicted: DEM + Scaled Tresp'], $
					col = [255, 2, 6, 2], /dev, 200, 700, line = [0, 0, 0, 1], size = 1.5
				if KEYWORD_SET(write_plots) then begin
					qcount_fname = outpath + '/qcount_fit_' + STRTRIM(wvls[i], 2) + '.png'
					PB_WIN2PNG, qcount_fname
				endif
				; Plot the accuracy of the DEM fit
				WINDOW, 12
				thiswresp = pwresps.(WHERE(wresp_tags eq 'A' + STRTRIM(wvls[i], 2)))
				this_respind = WHERE(resptab.wavelnth eq wvls[i])
				resp_str = {wave : thiswresp.wave, elecperdn : resptab[this_respind[0]].eperdn, $
					effarea : thiswresp.ea}
				obsct = EVE2AIA(daily_dem[goodfitpoints].obs_spec, resp_str)
				predct = EVE2AIA(daily_dem[goodfitpoints].pred_spec, resp_str)
				UTPLOT, TAI2UTC(daily_tai[goodfitpoints]), obsct / predct, ytit = 'Ratio of EVE counts in AIA band: Observed / Predicted', $
					title = 'Accuracy of CHIANTI and DEM fit in AIA band (' + STRING(wvls[i]) + ')', chars = 1.5
				if KEYWORD_SET(write_plots) then begin
					qmiss_fname = outpath + '/qdem_miss_' + STRTRIM(wvls[i], 2) + '.png'
					PB_WIN2PNG, qmiss_fname
				endif
				; Plot the ratio of observed to predicted for various Tresps (see win 11)
				WINDOW, 13
				hlim = [4, 2]
				prehist = HISTOGRAM(aia_daily_obs[goodfitpoints, i]/(pre_pred), loc = preloc, bins = 0.05, min = 0.525)
				posthist = HISTOGRAM(aia_daily_obs[goodfitpoints, i]/(post_pred), loc = postloc, bins = 0.05, min = 0.525)
				scaledhist = HISTOGRAM(aia_daily_obs[goodfitpoints, i]/(pre_pred/prescale), loc = scaleloc, bins = 0.05, min = 0.525)
				ymax = MAX([prehist, posthist, scaledhist])
				PLOT, preloc + 0.025, prehist, psym  = 10, ytitle = 'Frequency', xtitle = 'Ratio Observed/Predicted', $
					title = STRTRIM(wvls[i], 2) + P_ANG() + ' Channel Ratio Observed/Predicted Counts', $
					chars = 1.5, thick = 2, yrange = [0, ymax], xrange = [0.5, hlim[i]]
				OPLOT, postloc + 0.025, posthist, psym = 10, col = 6
				OPLOT, scaleloc + 0.025, scaledhist, psym = 10, col = 2, line = 2
				PB_LEGEND, ['DEM + Orig Tresp', 'DEM + Adjusted Tresp', 'DEM + Scaled Tresp'], $
					col = [255, 6, 2], /dev, 200, 700, size = 1.5
				if KEYWORD_SET(write_plots) then begin
					qcount_fname = outpath + '/qcount_fit_ratio_hist' + STRTRIM(wvls[i], 2) + '.png'
					PB_WIN2PNG, qcount_fname
				endif
			endelse
		endif
	endfor

	; Save the results
	if KEYWORD_SET(save_data) then begin
		cool_fit_str = CREATE_STRUCT('qtresp_out', qtresp_out, $
			'qfitco', qfitco, 'goodfitpoints', goodfitpoints, 'numgood', numgood)
		SAVEGEN, file = outpath + '/cool_fit.genx', str = cool_fit_str
		fixstr = CREATE_STRUCT('EMPIRICAL_MINUS_RAW', DBLARR(N_ELEMENTS(tresps.logte), N_ELEMENTS(tresps.channels)), $
			'LOGTE', tresps.logte, 'CHANNELS', tresps.channels)
		fixstr.empirical_minus_raw[*,0] = qtresp_out[0].tresp - tresps.a94.tresp * corrstr.ea_aia_over_eve[0]
		if KEYWORD_SET(final4) then begin
			fixstr.empirical_minus_raw[*,1] = input_tresp[1].tresp - $
				tresps.a131.tresp * corrstr.ea_aia_over_eve[1]
		endif else begin
			fixstr.empirical_minus_raw[*,1] = qtresp_out[1].tresp - $
				tresps.a131.tresp * corrstr.ea_aia_over_eve[1]
		endelse
		SAVEGEN, file = outpath + '/aia_chiantifix.genx', str = fixstr
		; Note that it needs to be renamed aia_V#_chiantifix.genx. 
		endif

	PRINT & PRINT, 'Elapsed time (fitting daily data) : ', SYSTIME(/sec) - t0, form = '(a50, f10.1)'
	if KEYWORD_SET(pstop) then STOP

endif else begin
	case veve of 
		2	:	RESTGEN, file = '/Volumes/disk2/data/aia/dem/20130129_193129/cool_fit.genx', str = cool_fit_str
		3	:	RESTGEN, file = '/Volumes/disk2/data/aia/dem/20130612_191433/cool_fit.genx', str = cool_fit_str
		4	:	RESTGEN, file = '/Volumes/disk2/data/aia/dem/20140509_000921/cool_fit.genx', str = cool_fit_str
	endcase
	qtresp_out = cool_fit_str.qtresp_out
	qfitco = cool_fit_str.qfitco
endelse

;+++++++++++++++++++++++++++++++
; Do some checking on the daily
; observations (constraint, 
; off-limb, comparison with
; preflare, etc.)
;-------------------------------
if KEYWORD_SET(check_daily) then begin

	; Take the preflare spectrum and convert it to CHIANTI units
	evtophot = 12398. / evewave
	aiaplatescale = 8.46158e-12			;	sr per pixel
	srpersun = 2d^24 * aiaplatescale
	preflare_obs_spec = {wave : evewave, spec : (preflare_dem.obs_spec>0) / 1.602e-19 / evtophot / 1.d4 / 10.	/ srpersun	}	;	Observed by EVE
	preflare_dem_spec = {wave : evewave, spec : preflare_dem.pred_spec / 1.602e-19 / evtophot / 1.d4 / 10.	/ srpersun	}	;	Predicted from EVE DEM

	for i = 0, 1 do begin
		thistresp = tresp_out[i]
		thiswresp = wresps.(WHERE(wresp_tags eq 'A' + STRTRIM(wvls[i], 2)))
		thiswresp.ea = thiswresp.ea * 10.^fitco[0,i]						;	EA with flare scaling applied
		tcounts = AIA_BP_MAKE_COUNTS_DEM(thistresp, preflare_dem)			;	Flare-scaled Tresp + Preflare DEM
		new_tcounts = AIA_BP_MAKE_COUNTS_DEM(qtresp_out[i], preflare_dem)	;	QS-fixed Tresp + Preflare DEM
		demspeccounts = AIA_BP_MAKE_COUNTS_SPEC(thiswresp, preflare_dem_spec) ; Flare-scaled Wresp + preflare DEM spec
		obsspeccounts = AIA_BP_MAKE_COUNTS_SPEC(thiswresp, preflare_obs_spec) ; Flare-scaled Wresp + preflare obs spec

		PRINT
		PRINT, 'Checking ' + STRTRIM(wvls[i], 2) + ' against pre-flare (quiet sun) spectrum:'
		PRINT, 'Ratio between (FL-Revised Wresp * EVE DEM Spec) and (FL-Revised Tresp * EVE DEM) [blurring?]: ', demspeccounts / tcounts
		PRINT, 'Ratio between (FL-Revised Wresp * EVE DEM Spec) and (FL-Revised Wresp * EVE obs spec) [DEM fit error?]: ', demspeccounts / obsspeccounts
		PRINT, 'Ratio between (QS-Revised Tresp * EVE DEM) and (FL-Revised Wresp * EVE obs spec) [quality of QS revision w/o blurring?]: ', new_tcounts / obsspeccounts
		PRINT, 'Product of line 1 and 3 [quality of QS revision]: ', (new_tcounts / obsspeccounts) * (demspeccounts / tcounts)
		PRINT
	endfor

	; Plot the Ratio of 171 / 193 counts, first to verify that the EVE DEM fits do a good job of capturing
	; the variation in the ratio of Fe IX to Fe XII emission (the average AR temperature)
	WINDOW, 10
	aia_temp_ratio = quiet_obs.aia_daily_obs[*, 2] / quiet_obs.aia_daily_obs[*, 3]
	eve_temp_ratio = quiet_obs.eve_daily_obs[*, 2] / quiet_obs.eve_daily_obs[*, 3]
	numdays = N_ELEMENTS(daily_dem)
	pred171 = FLTARR(numdays)
	pred193 = FLTARR(numdays)
	npred94 = FLTARR(numdays)
	pred94 = FLTARR(numdays)
	for i = 0, numdays - 1 do begin
		pred171[i] = AIA_BP_MAKE_COUNTS_DEM(tresps.a171, daily_dem[i])
		pred193[i] = AIA_BP_MAKE_COUNTS_DEM(tresps.a193, daily_dem[i])
		pred94[i] = AIA_BP_MAKE_COUNTS_DEM(tresps.a94, daily_dem[i])
		npred94[i] = AIA_BP_MAKE_COUNTS_DEM(qtresp_out[0], daily_dem[i])
	endfor
	coolratio = pred171 / pred193
	UTPLOT, TAI2UTC(daily_tai), aia_temp_ratio, title = 'Ratio of 171 / 193 counts', $
		chars = 1.5, yrange = 0.5>LIMITS([aia_temp_ratio, eve_temp_ratio, coolratio])<2.0
	OUTPLOT, TAI2UTC(daily_tai), eve_temp_ratio, col = 2
	OUTPLOT, TAI2UTC(daily_tai), coolratio, col = 6
	eocorr = CORRELATE( aia_temp_ratio, eve_temp_ratio)
	edcorr = CORRELATE( aia_temp_ratio, coolratio)
	PB_LEGEND, ['AIA observed', 'EVE observed [CC=' + STRING(eocorr, form = '(f5.3)') + ']', $
		'EVE DEM predicted [CC=' + STRING(edcorr, form = '(f5.3)') + ']'], $
		/dev, col = [255, 2, 6], /line, size = 1.5, 200, 430 
	if KEYWORD_SET(write_plots) then begin
		artempratio_fname = outpath + '/daily_fe9-12ratio.png'
		PB_WIN2PNG, artempratio_fname
	endif

	; Now we need to see whether the predicted/observed 94 count rate correlates
	; with the 171/193 ratio, and whether that correlation is reduced with the new tresp
	WINDOW, 11
	q94dat = quiet_obs.aia_daily_obs[*,0]
	UTPLOT, TAI2UTC(daily_tai), coolratio / coolratio[0], yrange = [0, 2.0], chars = 1.5, ytitle = 'Ratio'
	OUTPLOT, TAI2UTC(daily_tai), (q94dat/q94dat[0]) / (pred94/pred94[0]), col = 2
	OUTPLOT, TAI2UTC(daily_tai), (q94dat/q94dat[0]) / (npred94/npred94[0]), col = 6
	ocorr = 'DEM Predicted 94 / Observed 94 (original Tresp) : ' + STRING(CORRELATE(coolratio, q94dat/pred94), form = '(f5.3)')
	ncorr = 'DEM Predicted 94 / Observed 94 (revised Tresp) : ' + STRING(CORRELATE(coolratio, q94dat/npred94), form = '(f5.3)')
	PB_LEGEND, ['DEM Predicted 171 / 193', ocorr, ncorr], /dev, 160, 600, /line, col = [255, 2, 6, 5], size = 1.5
	if KEYWORD_SET(writeplots) then begin
		artemp94_fname = outpath + '/daily_fe9-12ratio_94.png'
		PB_WIN2PNG, artemp94_fname
	endif
	
	;+++++++++++++++++++++++++++++++
	; Compare the results with HPW's
	; off-limb AIA observations
	;-------------------------------
	CD, '/Volumes/disk2/data/aia/dem/hpw/', current = old_dir
	ff = FILE_SEARCH('aia_ints.*.txt')
	RESTGEN, file = 'aia_fix_response_hpw.genx', hplogte, hpt94, hpt131
	logt_dem = FINDGEN(40) * 0.05 + 5.0
	logte = tresps.a94.logte
	hpw94 = {name : 'A94', units : 'DN cm^5 s^-1 pix^-1', logte : logte, tresp : INTERPOL(hpt94, hplogte, logte)>0}
	hpw131 = {name : 'A131', units : 'DN cm^5 s^-1 pix^-1', logte : logte, tresp : INTERPOL(hpt131, hplogte, logte)>0}
	numfiles = N_ELEMENTS(ff)
	otresps = [tresps.a94, tresps.a131, tresps.a171, tresps.a193, tresps.a211, tresps.a335]
	ftresps = [qtresp_out[0], qtresp_out[1], tresps.a171, tresps.a193, tresps.a211, tresps.a335]
	htresps = [hpw94, hpw131, tresps.a171, tresps.a193, tresps.a211, tresps.a335]
	tcorr = DBLARR(6)
	evecorr = DBLARR(6)
	wvlind = [0, 1, 2, 3, 4, 6]
	for i = 0, 5 do begin
		tcorr[i] = INTERPOL(corrstr.ea_over_ea0[*, wvlind[i]], corrstr.tai, ANYTIM2TAI('15-feb-2011 00:00'))
		evecorr[i] = corrstr.ea_aia_over_eve[wvlind[i]]
		;tcorr = [1.043, 1., 1.004, 0.978, 0.964, 0.682]
	endfor	
	for i = 0, numfiles - 1 do begin
		dat = RD_TFILE(ff[i], 4, /auto)
		wvl = FIX(dat[1,*])
		goodwaves = WHERE(wvl ne 304, numwaves)
		int = REFORM(FLOAT(dat[2,goodwaves])) / tcorr * evecorr
		odemout = PB_GEN_DEM_FIT(int, otresps, opreds, logt_dem = logt_dem, logt_knots = [5.9, 6.2, 6.5])
		fdemout = PB_GEN_DEM_FIT(int, ftresps, fpreds, logt_dem = logt_dem, logt_knots = [5.9, 6.2, 6.5])
		hdemout = PB_GEN_DEM_FIT(int, htresps, hpreds, logt_dem = logt_dem, logt_knots = [5.9, 6.2, 6.5])
		thisstr = {obs : int, opreds : opreds, odem : odemout, fpreds : fpreds, fdem : fdemout, $
			hpreds : hpreds, hdem : hdemout, logt_dem : logt_dem}
		if i eq 0 then demstrs = thisstr else demstrs = [demstrs, thisstr]
	endfor
	CD, old_dir
	
	WINDOW, 12
	PLOT, TRANSPOSE(demstrs.obs / demstrs.opreds), yrange = [0, 4], chars = 1.5, $
		ytitle = 'Ratio [Observed / Predicted]', title = 'AIA Modified T response'
	OPLOT, TRANSPOSE(demstrs.obs / demstrs.fpreds), col = 2
	OPLOT, TRANSPOSE(demstrs.obs / demstrs.hpreds), col = 6
	for i = 0, 4 do PLOTS, [24.5, 24.5] + i * 25, [0, 4], line = 2
	XYOUTS, 10, 0.3, '94', chars = 1.5
	XYOUTS, 35, 0.3, '131', chars = 1.5
	XYOUTS, 60, 0.3, '171', chars = 1.5
	XYOUTS, 85, 0.3, '193', chars = 1.5
	XYOUTS, 110, 0.3, '211', chars = 1.5
	XYOUTS, 135, 0.3, '335', chars = 1.5
	PB_LEGEND, ['Original', 'PB Fit', 'HPW Fit'], 106, 3, col = [255, 2, 6], /line, size = 1.5
	if KEYWORD_SET(writeplots) then begin
		hpw_count_fname = outpath + '/hpw_count_comp.png'
		PB_WIN2PNG, hpw_count_fname
	endif
	
	WINDOW, 13
	PLOT, demstrs.logt_dem, demstrs.odem, xtit = 'Log(T)', ytit = 'Log(DEM)', $
		charsize = 1.5, yrange = [18, 22], title = 'Best fit DEMs'
	for i = 0, 24 do OPLOT, demstrs[i].logt_dem, demstrs[i].odem, col = 255
	for i = 0, 24 do OPLOT, demstrs[i].logt_dem, demstrs[i].hdem, col = 6
	for i = 0, 24 do OPLOT, demstrs[i].logt_dem, demstrs[i].fdem, col = 2
	PB_LEGEND, ['Original', 'PB Fit', 'HPW Fit'], col = [255, 2, 6], /line, size = 1.5, 5.2, 21
	if KEYWORD_SET(writeplots) then begin
		hpw_dem_fname = outpath + '/hpw_dem_comp.png'
		PB_WIN2PNG, hpw_dem_fname
	endif

	PRINT & PRINT, 'Elapsed time (checking daily data) : ', SYSTIME(/sec) - t0, form = '(a50, f10.1)'
	if KEYWORD_SET(pstop) then STOP

endif


end