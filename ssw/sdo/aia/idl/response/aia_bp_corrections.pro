function AIA_BP_CORRECTIONS, tabledat, sampleutc, respstr, ver_date, $
	effarea = effarea, tresp = tresp, uv = uv, $
	pstop = pstop, silent = silent

;+
;
; $Log: aia_bp_corrections.pro,v $
; Revision 1.4  2012/03/28 23:45:57  boerner
; Updates to allow for date-dependent response table
;
; Revision 1.3  2012/01/05 00:52:02  boerner
; Minor re-organization
;
; Revision 1.2  2011/11/18 00:27:58  boerner
; Added EVE version (currently hard-coded)
;
; Revision 1.1  2011/10/03 23:20:13  boerner
; Initial version
;
;
; INPUT PARAMETERS:
;	tabledat	--	a string array holding data read in from the AIA response table .txt file
;	sampleutc	--	CCSDS-formatted time string describing the time for which the response
;					should be calculated
;	respstr		--	the structure giving the instrument response. Should be either the short-form
;					effective area or temperature response
;	ver_date	--	a string specifying the version date of the response table file. If not
;					specified, defaults to a null string
;
; KEYWORD PARAMETERS:
;	/effarea	--	respstr is an effective area (wavelength response) structure
;	/tresp		--	respstr is a temperature response structure
;
; RETURNS:
;	corr_str		--	a structure containing a description of the corrections that 
;					can be applied to the response.
;
;-

loud = not KEYWORD_SET(silent)

;+++++++++++++++++++++++++++
; Parse out information from
; the response table strings
;---------------------------
waves 	=	tabledat.wavelnth
startts	=	tabledat.t_start
endts	=	tabledat.t_stop
ea0s	=	tabledat.eff_area
effwvls	=	tabledat.eff_wvln
eap1s	=	tabledat.effa_p1
eap2s	=	tabledat.effa_p2
eap3s	=	tabledat.effa_p3

stais	=	ANYTIM2TAI(startts)
etais	=	ANYTIM2TAI(endts)
ttai	=	ANYTIM2TAI(sampleutc)

if N_ELEMENTS(ver_date) eq 0 then ver_date = ''

uwaves = UNIQ(waves[SORT(waves)])
if KEYWORD_SET(uv) then begin
	uwaves = uwaves[WHERE(waves[uwaves] gt 1000)]
endif else begin
	uwaves = uwaves[WHERE(waves[uwaves] lt 1000)]
endelse
channels = 'A' + STRTRIM(waves[uwaves], 2)

numchan = N_ELEMENTS(channels)
rtags	= TAG_NAMES(respstr)

;+++++++++++++++++++++++++++
; Set up variables to hold
; data for the correction
; structure
;---------------------------
epochs 	=	STRARR(numchan)
etais	=	DBLARR(numchan)
ewaves	=	DBLARR(numchan)
ea0		=	DBLARR(numchan)
ea1		=	DBLARR(numchan)
p1		=	DBLARR(numchan)
p2		=	DBLARR(numchan)
p3		=	DBLARR(numchan)
eve0	=	DBLARR(numchan)

;+++++++++++++++++++++++++++
; Set up the self-documenting
; info string arrays
;---------------------------
tinfo	= [	'VERSION_DATE: Date of the response table file', $
			'EPOCH: Start time of last update to time dependent response (last bakeout of each channel?)', $
			'EPOCH_TAI: TAI time of EPOCH', $
			'SAMPLETIME: Time for which the response was calculated', $
			'SAMPLE_TAI: TAI time of sample', $
			'EA_EPOCH_OVER_T0: Ratio of effective area at EPOCH time to effective area at t0 (24-mar-2010)', $
			'EA_SAMPLE_OVER_EPOCH: Ratio of effective area at sample time to effective area at EPOCH', $
			'EA_SAMPLE_OVER_T0: Product of EA_EPOCH_OVER_T0 * EA_SAMPLE_OVER_EPOCH', $
			'P1/P2/P3: Polynomial terms for time dependent correction', $
			'Polynomial: ea_t1 = ea_epoch * (1 + p1*dt + p2*dt^2 + p3*dt^3)', $
			'Polynomial: t1 = sample date  dt = (sample date) - (epoch)  in days', $
			'EFFWAVE: Wavelength at which effective area ratio for that channel is calculated', $
			'CHANNELS: List of channels for which response trend is tracked' ]
			
einfo	= [ 'AIA_OVER_EVE: Ratio of AIA effective area implied by EVE data on 24-mar-2010 to ground AIA measurements', $
			'EFFWAVE: Wavelength at which effective area ratio for each channel is calculated', $
			'CHANNELS: List of channels for which response trend is tracked', $
			'EVE_VERSION: Version of the EVE data used to calculate the normalization' ]

cinfo	= [	'EMPIRICAL_OVER_RAW: Ratio of empirically-corrected temperature response to response using only "raw" CHIANTI data', $
			'LOGTE: Temperature grid for empirical correction', $
			'CHANNELS: List of channels for which the correction is derived' ]

ainfo 	= [ 'TIME: Sub-structure describing how the time-dependent response correction for each channel is calculated', $
			'TIME_APPLIED: Does the response function use the time-dependent correction?', $
			'EVENORM: Sub-structure giving the normalization constant used to ensure agreement with EVE observations', $
			'EVENORM_APPLIED: Does the response function use the EVE-derived normalization constant?', $
			'', $
			'' ]

if KEYWORD_SET(uv) then begin
	ainfo[2] = 'SEENORM: Sub-structure giving the normalization constant used to ensure agreement with TIMED/SEE observations'
	ainfo[3] = 'SEENORM_APPLIED: Does the response function use the TIMED/SEE-derived normalization constant?'
endif

;++++++++++++++++++++++++++++++++
; Loop through wavelengths
; and populate the correction
; structures for each wavelength
;--------------------------------
for i = 0, numchan - 1 do begin
	thiswave 	= waves[uwaves[i]]
	wavelines 	= WHERE(waves eq thiswave, numwavelines)
	wlsort		= SORT(startts[wavelines])
	presample	= WHERE(stais[wavelines[wlsort]] le ttai, numpre)
	useline		= wavelines[wlsort[presample[numpre-1]]]	;	The line in the response table corresponding to the
															; 	epoch for this sampledate and this wavelength
	firstline	= wavelines[wlsort[0]]						;	The line in the response table corresponding to the
															;	earliest epoch for this wavelength
	if startts[firstline] ne '2010-03-24T00:00:00.000' then begin
		if loud then box_message, 'AIA_BP_CORRECTIONS: Start time of first epoch is not nominal'
	endif
	epochs[i] 	= startts[useline]
	etais[i]	= stais[useline]
	ewaves[i]	= effwvls[useline]
	p1[i]		= eap1s[useline]
	p2[i]		= eap2s[useline]
	p3[i]		= eap3s[useline]

	thisea = respstr.(WHERE(rtags eq channels[i]))
	nom_ea0 = INTERPOL(thisea.ea, thisea.wave, ewaves[i])
	eve0[i]	= ea0s[firstline] / nom_ea0
	ea0[i]	= ea0s[useline] / ea0s[firstline]
	dt 		= (ttai - etais[i]) / 86400d
	ea1[i]	= 1 + p1[i] * dt + p2[i] * dt^2 + p3[i] * dt^3

endfor

if KEYWORD_SET(pstop) then STOP

;+++++++++++++++++++++++++++
; Generate the sub-structures
; and output structure
;---------------------------
tdepend_str = CREATE_STRUCT('VERSION_DATE', ver_date, $
	'EPOCH', epochs, 'EPOCH_TAI', etais, 'SAMPLETIME', sampleutc, 'SAMPLE_TAI', ttai, $
	'EA_EPOCH_OVER_T0', ea0, 'EA_SAMPLE_OVER_EPOCH', ea1, 'EA_SAMPLE_OVER_T0', ea1 * ea0, $
	'P1', p1, 'P2', p2, 'P3', p3, $
	'EFFWAVE', ewaves, 'CHANNELS', channels, 'INFO', tinfo)

evenorm_str = CREATE_STRUCT('AIA_OVER_EVE', eve0, 'EFFWAVE', ewaves, 'CHANNELS', channels, $
	'EVE_VERSION', 'EVL_L2_*_002', 'INFO', einfo)

corr_str = CREATE_STRUCT('TIME', tdepend_str, 'TIME_APPLIED', 'NO', $
	'EVENORM', evenorm_str, 'EVENORM_APPLIED', 'NO', 'INFO', ainfo)

RETURN, corr_str

end