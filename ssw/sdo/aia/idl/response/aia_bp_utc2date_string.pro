function AIA_BP_UTC2DATE_STRING, utc

;+
;
; $Log: aia_bp_utc2date_string.pro,v $
; Revision 1.1  2012/01/31 00:39:15  boerner
; Initial version
;
;
;-

step1 = STRSPLIT(/extract, utc, '.')
step2 = STRJOIN(STRSPLIT(/extract, step1[0], ':'))
step3 = STRJOIN(STRSPLIT(/extract, step2, '-'))
result = STRJOIN(STRSPLIT(/extract, step3, 'T'), '_')

RETURN, result

end
