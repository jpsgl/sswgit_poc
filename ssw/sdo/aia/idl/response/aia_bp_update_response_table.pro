pro AIA_BP_UPDATE_RESPONSE_TABLE, wavelength, $
	start_date = start_date, stop_date = stop_date, $
	old_table = old_table, poly = poly, overwrite = overwrite, $
	all = all, loud = loud, pstop = pstop, write = write

;+
;
; $Log: aia_bp_update_response_table.pro,v $
; Revision 1.4  2012/07/21 00:07:38  boerner
; Implemented /all keyword
;
; Revision 1.3  2012/03/29 00:28:47  boerner
; Added overwrite keyword
;
; Revision 1.2  2012/03/28 23:51:29  boerner
; Use V3 response tables
;
; Revision 1.1  2012/03/28 23:45:30  boerner
; Initial version
;
; INPUT:
;	wavelength	-	INTEGER specifying the channel wavelength to update
;
; KEYWORD PARAMETERS:
;
;	start_date	-	Starting date for the new entry
;
;	stop_date	-	Stop date for the new entry
;
;	old_table	-	File name for the response table file to update. If not set, looks for the 
;					latest aia_V3 file in the scratchpath
;
;	poly		-	Polynomial degree to use for the fit; defaults to 1
;
;	/overwrite	-	If set, then the entry in the old_table that currently governs the specified 
;					start_date is overwritten and replaced with a new fit using the specified
;					stop_date and poly (the start_date in this case is changed to match the old
;					entry)		By default, the stop_date on the old entry is changed to 
;					match the specified start_date, and a new entry with the specified start_date
;					is appended to the table
;
;	/all		-	Runs recursively to update all wavelengths
;
;	/loud		-	Doesn't do anything at the moment
;
;	/pstop		-	Standard debug switch
;
;	/write		-	If set, then response_table.txt and .set_keys files are written	in the scratchpath
;		
;-


; Run recursively to update all wavelengths
if KEYWORD_SET(all) then begin
	wvls = 	[94,	131,	171,	193,	211,	304,	335,	1600,	1700]
	polys = [0,		0,		0,		1,		1,		1,		1,		1,		1]
	for i = 0, N_ELEMENTS(wvls)-1 do begin
		AIA_BP_UPDATE_RESPONSE_TABLE, wvls[i], start_date = start_date, stop_date = stop_date, $
			old_table = old_table, /write, poly = polys[i]
		if i ne 0 then begin
			AIA_BP_PARSE_FILENAME, old_table, base = base, path = path
			SSW_FILE_DELETE, path + base + '.txt'
			SSW_FILE_DELETE, path + base + '.set_keys'
		endif
		old_table = ''
		dummy = ''
		READ, dummy, prompt = 'Check plots, hit enter to continue...'
	endfor
	RETURN
endif

; Set up defaults
if N_ELEMENTS(poly) eq 0 then begin
	MESSAGE, /info, 'Keyword poly not set... setting to 1'
	poly = 1
endif
if N_ELEMENTS(wavelength) eq 0 then begin
	MESSAGE, /info, 'Parameter wavelength not set...setting to 304'
	wavelength = 304
endif
if N_ELEMENTS(start_date) eq 0 then begin
	MESSAGE, /info, 'Parameter start_date not set...setting to 1-Jan-2012'
	start_date = '1-Jan-2012'
endif
if N_ELEMENTS(stop_date) eq 0 then begin
	MESSAGE, /info, 'Keyword stop_date not set...setting to 1-May-2040'
	stop_date = '1-May-2040'
endif
scratchpath = '/Volumes/disk2/data/aia/response/tablescratch/'

; Generate a response table entry spanning the requested dates
newline = AIA_BP_RESPONSE_TABLE(fitdat, wave = wavelength, t_start = start_date, t_stop = stop_date, $
	poly = poly, /silent, ver_num = 1)

; Identify the file holding the "old" (current default) response table data
if KEYWORD_SET(old_table) then begin
	if FILE_EXIST(old_table) then tablefile = old_table
endif
if N_ELEMENTS(tablefile) eq 0 then begin		;	old_table keyword either not set, or does not exist
	tablefilebase = scratchpath + 'aia_V3_'
	respfiles = FILE_SEARCH(tablefilebase + '*_response_table.txt')
	for i = 0, N_ELEMENTS(respfiles)-1 do $
		respfiles[i] = STRMID(STRSPLIT(respfiles[i], /extract, tablefilebase, /regex), 0, 15)
	respfile_tais = ANYTIM2TAI(FILE2TIME(respfiles))
	if not KEYWORD_SET(respversion) then respversion = respfiles[WHERE(respfile_tais eq MAX(respfile_tais))]
	if not FILE_EXIST(tablefilebase + respversion + '_*') then begin
		BOX_MESSAGE, ['File not found: ', tablefilebase + respversion + '_*']
		respversion = respfiles[WHERE(resptfile_tais eq MAX(respfile_tais))]
	endif
	tablefile = tablefilebase + respversion + '_response_table.txt'
	old_table = tablefile
endif

; Pull out the line from the response table that currently controls the specified date range
old_rtable = AIA_BP_READ_RESPONSE_TABLE(tablefile)
wvl_id = WHERE(old_rtable.wavelnth eq wavelength)
taistarts = ANYTIM2TAI(old_rtable.t_start)
taistarts_wvl = taistarts[wvl_id]
oldline_id = WHERE(old_rtable.wavelnth eq wavelength and taistarts eq MAX(taistarts_wvl < ANYTIM2TAI(start_date)))
oldline = old_rtable[oldline_id]

; If the user wants to overwrite this entry (rather than appending a new entry), then re-run the 
; newline generation
if KEYWORD_SET(overwrite) then begin
	newline = AIA_BP_RESPONSE_TABLE(fitdat, wave = wavelength, t_start = oldline.t_start, t_stop = stop_date, $
		poly = poly, /silent, ver_num = oldline.ver_num + 1)
endif

; Look at what the "old" response table would do over the requested dates
epoch_tai = ANYTIM2TAI(oldline.t_start)
utcx = TAI2UTC(fitdat.usetime)
dt = (fitdat.usetime - epoch_tai) / 86400d		;	delta time in days
scale = 1 + oldline.effa_p1 * dt + oldline.effa_p2 * dt^2 + oldline.effa_p3 * dt^3
ea_ratio = fitdat.useratios[0] * oldline.eff_area / newline.eff_area 

old_fit = scale * ea_ratio
old_rms = SQRT( MEAN( (old_fit - fitdat.useratios)^2 ) )

; Plot the data with the old and new trend lines
UTPLOT, chars = 1.5, utcx, fitdat.useratios, psym = -4, /xstyle, $
	ytitle = 'Ratio AIA/EVE', title = wavelength
OUTPLOT, utcx, fitdat.yfit, col = 2
OUTPLOT, utcx, old_fit, col = 6

oslopelabel = STRING('Old slope: ', oldline.effa_p1, format = '(a20, e10.2)')
nslopelabel = STRING('New slope: ', newline.effa_p1, format = '(a20, e10.2)')
ormslabel = STRING('RMS over previous fit period:', oldline.rmse, format = '(a50, f7.4)')
nrmslabel = STRING('RMS over this fit period with new trend line:', newline.rmse, format = '(a50, f7.4)')
onrmslabel = STRING('RMS over this fit period with old trend line:', old_rms, format = '(a50, f7.4)')
labeler = [oslopelabel, nslopelabel, nrmslabel, onrmslabel, ormslabel]
PB_LEGEND, /dev, labeler, 200, 300, size = 1.5, col = [6, 2, 255, 255, 255]

if KEYWORD_SET(pstop) then STOP

if KEYWORD_SET(write) then begin
	; Add this line to the response table text file, and modify the old line to stop short
	; of this time
	fname = tablefilebase + TIME2FILE(/sec, RELTIME(/now)) + '_response_table'
	outstr = old_rtable
	outstr_template = outstr[0]
	STRUCT_ASSIGN, newline, outstr_template
	if KEYWORD_SET(overwrite) then begin
		outstr[oldline_id] = outstr_template				
		numnew = 1
	endif else begin
		outstr[oldline_id].t_stop = newline.t_start
		outstr[oldline_id].ver_num = outstr[oldline_id].ver_num + 1
		outstr = [outstr, outstr_template]
		numnew = 2
	endelse
	numentries = N_ELEMENTS(outstr)
	
	OPENW, txtlun, fname + '.txt', /get_lun
	otags = TAG_NAMES(outstr)
	PRINTF, txtlun, otags, format = '(3a26, 11a11)'
	for i = 0, numentries - 1 do begin
		PRINTF, txtlun, outstr[i], format = '(3a26, i11, a11, i11, f11.2, 2f11.5, f11.2, 4f11.5)'
	endfor
	FREE_LUN, txtlun
	
	OPENW, setlun, fname + '.set_keys', /get_lun
	setstring = "set_keys -c ds='aia.response' "
	if wavelength lt 1000 then count = 1 else count = 0
	thinthickrat = [1.39321, 11.9752, 1.21283, 1.19869, 1.21940, 1.38455, 1.41263]
	euvwvls = [94, 131, 171, 193, 211, 304, 335]
	line_ids = [oldline_id, numentries - 1]	;	Only write the changes to the set_keys file
	for i = 0, numnew-1 do begin
		thisline = outstr[line_ids[i]]
 		for k = 0, count do begin			;	For EUV channels, write both THIN and THICK lines
 			setstring = ''
			if k eq 1 then begin
				thisline.wave_str = STRING(wavelength) + '_THICK'
				thisline.eff_area = thisline.eff_area / thinthickrat[WHERE(euvwvls eq wavelength)]
			endif	
			for j = 0, N_ELEMENTS(otags) - 2 do begin		;	Don't include RMSE
				thisval = STRTRIM(thisline.(j), 2)
				if otags[j] eq 'WAVE_STR' then thisval = "'" + thisval + "'" 
				setstring = setstring + otags[j] + '=' + thisval + ' '
			endfor
			PRINTF, setlun, setstring
		endfor
	endfor
	FREE_LUN, setlun

endif


end
