pro AIA_BP_PRINT_KEYWORDS

;+
;
; $Log: aia_bp_print_keywords.pro,v $
; Revision 1.1  2011/04/21 17:49:25  boerner
; Initial version
;
;
; Reads in the response functions stored in solarsoft and prints out the 
; values that belong in the AIA_RESPONSE series (for populating the FITS
; headers) 
;
;-

fullresp = AIA_GET_RESPONSE(/all,/ full)

strtags = TAG_NAMES(fullresp)
peakwaves = [93.9, 131.2, 171.1, 195.1, 211.3, 303.8, 335.4]
channames = fullresp.channels
chans = channames + '_FULL'
chaninds = INTARR(14)
gains = FLTARR(14)
eas = FLTARR(14)
for i = 0, 13 do chaninds[i] = WHERE(strtags eq chans[i])

PRINT, 'THIN'
PRINT, '', peakwaves, format = '(a12, 7f11.1)'

title = 'GAIN'
for i = 0, 6 do gains[i] = 12398./peakwaves[i]/3.65/fullresp.(chaninds[i]).elecperdn
PRINT, title, gains[0:6], format = '(a12, 7f11.3)'

title = 'EFFAREA'
for i = 0, 6 do eas[i] = INTERPOL(fullresp.(chaninds[i]).effarea, fullresp.(chaninds[i]).wave, peakwaves[i])
PRINT, title, eas[0:6], format = '(a12, 7f11.3)'	;	Print effarea in file to make sure it agrees

PRINT
PRINT, 'THICK'
PRINT, '', peakwaves, format = '(a12, 7f11.1)'

title = 'GAIN'
for i = 7, 13 do gains[i] = 12398./peakwaves[i-7]/3.65/fullresp.(chaninds[i]).elecperdn
PRINT, title, gains[7:13], format = '(a12, 7f11.3)'

title = 'EFFAREA'
for i = 7, 13 do eas[i] = INTERPOL(fullresp.(chaninds[i]).effarea, fullresp.(chaninds[i]).wave, peakwaves[i-7])
PRINT, title, eas[7:13], format = '(a12, 7f11.3)'	;	Print effarea in file to make sure it agrees

channames[0:6] = channames[0:6] + '_THIN'
channames = STRMID(channames, 1, 15)
for i = 0, 13 do begin
	PRINT
	PRINT, channames[i], 'DN_GAIN', gains[i], format = '(2a12, f11.3)'
	PRINT, channames[i], 'EFF_AREA', eas[i], format = '(2a12, f11.3)'
	PRINT, channames[i], 'EffWavelnth', peakwaves[i mod 7], format = '(2a12, f11.1)'
	PRINT
endfor


fullresp = AIA_GET_RESPONSE(/all,/ full, /uv)

strtags = TAG_NAMES(fullresp)
peakwaves = [1600, 1700, 4500]
channames = fullresp.channels
chans = channames + '_FULL'
chaninds = INTARR(3)
gains = FLTARR(3)
eas = FLTARR(3)
for i = 0, 2 do chaninds[i] = WHERE(strtags eq chans[i])

PRINT, '', peakwaves, format = '(a12, 7f11.1)'

title = 'GAIN'
for i = 0, 2 do gains[i] = 12398./peakwaves[i]/3.65/fullresp.(chaninds[i]).elecperdn
PRINT, title, gains, format = '(a12, 7f11.3)'

title = 'EFFAREA'
for i = 0, 2 do eas[i] = INTERPOL(fullresp.(chaninds[i]).effarea, fullresp.(chaninds[i]).wave, peakwaves[i])
PRINT, title, eas, format = '(a12, 7f11.3)'	;	Print effarea in file to make sure it agrees

channames = STRMID(channames, 1, 15)
for i = 0, 2 do begin
	PRINT
	PRINT, channames[i], 'DN_GAIN', gains[i], format = '(2a12, f11.3)'
	PRINT, channames[i], 'EFF_AREA', eas[i], format = '(2a12, f11.5)'
	PRINT, channames[i], 'EffWavelnth', peakwaves[i mod 7], format = '(2a12, f11.1)'
	PRINT
endfor

end