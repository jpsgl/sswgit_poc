function AIA_BP_BLEND_CHANNELS, infullresp, $
	short = short

;+
;
; $Id: aia_bp_blend_channels.pro,v 1.3 2010/08/10 22:47:51 boerner Exp $
;
; Takes in a long-form instrument response structure and adjusts the primary
; and secondary mirror reflectivity (and, thus, effective area) for the channels
; on telescope 1 and telescope 4 to account for potential cross-contamination
; between the channels.
;
;-

resptags = TAG_NAMES(infullresp)
wvls = FIX(STRMID(infullresp.channels, 1, 3))
w94 = WHERE(wvls eq 94, num94)
w131 = WHERE(wvls eq 131, num131)
w304 = WHERE(wvls eq 304, num304)
w335 = WHERE(wvls eq 335, num335)

outfullresp = infullresp
if num94 gt 0 then for i = 0, num94 - 1 do begin
	if num304 le 0 then STOP	;	Can't calculate blend if you don't have data on the other channel
	thischan = infullresp.channels[w94[i]]
	thisfullindex = WHERE(resptags eq thischan + '_FULL')
	thisindex = WHERE(resptags eq thischan)
	thisstr = infullresp.(thisfullindex)
	crosschan = infullresp.a304_full
	crosstalk_effarea = crosschan.ent_filter * crosschan.primary * crosschan.secondary * crosschan.contam * crosschan.ccd * crosschan.geoarea * thisstr.fp_filter
	effarea = thisstr.effarea + crosstalk_effarea
	outfullresp.(thisfullindex).cross_area = crosstalk_effarea
	outfullresp.(thisfullindex).effarea = effarea
	outfullresp.(thisindex).ea = effarea
endfor

if num131 gt 0 then for i = 0, num131 - 1 do begin
	if num335 le 0 then STOP	;	Can't calculate blend if you don't have data on the other channel
	thischan = infullresp.channels[w131[i]]
	thisfullindex = WHERE(resptags eq thischan + '_FULL')
	thisindex = WHERE(resptags eq thischan)
	thisstr = infullresp.(thisfullindex)
	crosschan = infullresp.a335_full
	crosstalk_effarea = crosschan.ent_filter * crosschan.primary * crosschan.secondary * crosschan.contam * crosschan.ccd * crosschan.geoarea * thisstr.fp_filter
	effarea = thisstr.effarea + crosstalk_effarea
	outfullresp.(thisfullindex).cross_area = crosstalk_effarea
	outfullresp.(thisfullindex).effarea = effarea
	outfullresp.(thisindex).ea = effarea
endfor

if num304 gt 0 then for i = 0, num304 - 1 do begin
	if num94 le 0 then STOP	;	Can't calculate blend if you don't have data on the other channel
	thischan = infullresp.channels[w304[i]]
	thisfullindex = WHERE(resptags eq thischan + '_FULL')
	thisindex = WHERE(resptags eq thischan)
	thisstr = infullresp.(thisfullindex)
	crosschan = infullresp.a94_full
	crosstalk_effarea = crosschan.ent_filter * crosschan.primary * crosschan.secondary * crosschan.contam * crosschan.ccd * crosschan.geoarea * thisstr.fp_filter
	effarea = thisstr.effarea + crosstalk_effarea
	outfullresp.(thisfullindex).cross_area = crosstalk_effarea
	outfullresp.(thisfullindex).effarea = effarea
	outfullresp.(thisindex).ea = effarea
endfor

if num335 gt 0 then for i = 0, num335 - 1 do begin
	if num131 le 0 then STOP	;	Can't calculate blend if you don't have data on the other channel
	thischan = infullresp.channels[w335[i]]
	thisfullindex = WHERE(resptags eq thischan + '_FULL')
	thisindex = WHERE(resptags eq thischan)
	thisstr = infullresp.(thisfullindex)
	crosschan = infullresp.a131_full
	crosstalk_effarea = crosschan.ent_filter * crosschan.primary * crosschan.secondary * crosschan.contam * crosschan.ccd * crosschan.geoarea * thisstr.fp_filter
	effarea = thisstr.effarea + crosstalk_effarea
	outfullresp.(thisfullindex).cross_area = crosstalk_effarea
	outfullresp.(thisfullindex).effarea = effarea
	outfullresp.(thisindex).ea = effarea
endfor

outfullresp.notes = outfullresp.notes + ' BLENDED'

if KEYWORD_SET(short) then presp = AIA_BP_PARSE_EFFAREA(outfullresp, retval) else retval = outfullresp

RETURN, retval

end