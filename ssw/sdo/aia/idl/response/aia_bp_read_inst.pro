pro AIA_BP_READ_INST, filename, $
	inststr, fullinststr, $
	warnings, $
	write = write, logdir = logdir, silent = silent, $
	chanwrite = chanwrite, official = official

;+
;
; $Id: aia_bp_read_inst.pro,v 1.4 2011/04/21 17:50:12 boerner Exp $
;
; Reads in an inst.txt file and returns an IDL structure containing the
; parameters of the instrument. 
;
; INPUT:
;	filename-	the name of the inst.txt file to be read (including 
;				the path, if necessary, from the current directory; 
;				generally, no path should be used, and the function
;				should be run from the appropriate directory)
;
; OUTPUTS:
;	fullinststr-	a structure giving the instrument response along with
;					some information about how it was generated
;
;	inststr-	a structure giving the basic parameters of the instrument
;				response
;
;	warnings-	[OPTIONAL] a string array containing any descriptive
;				text on potential errors encountered during the generation
;				of the response structure
;
; KEYWORDS:
;	/silent-	if set, nothing is printed to the terminal
;
;	logdir=		if set, the warnings are written to a text file in the
;				logdir
;
;	/write-		if set, three output files are created:
;				_inst.dat	--	an ASCII file giving wavelength and effective 
;								area for all channels
;				_inst.genx	--	an SSW GENX file containing the inststr output
;				_fullinst.genx	--	an SSW GENX file containing the fullinststr
;									output
; 
;	/chanwrite-	if set, output files are created for each channel:
;				_chan.dat	--	an ASCII file giving wavelength and effective 
;								area for the individual channel
;				_chan.genx	--	an SSW GENX file with the channel response
;				_fullchan.genx	--	an SSW GENX file containing the full channel 
;									response
;				Also, if /chanwrite is set then any subchan.txt files created
;				by the instrument are left in place; the default is to remove them
;
;	/official-	if set, then the input and output files are all copied to the
;				"official" bandpass file directory. This keyword is passed to 
;				aia_bp_read_chan and lower level routines with the path of the 
;				bandpass file directory
;
; $Log: aia_bp_read_inst.pro,v $
; Revision 1.4  2011/04/21 17:50:12  boerner
; Fixed ability to use wavelength file
;
; Revision 1.3  2010/08/03 00:28:17  boerner
; Change to calculate contam on the fly
;
; Revision 1.2  2010/06/15 23:00:01  boerner
; Added phottoDN handling
;
; Revision 1.1  2010/05/28 22:31:25  boerner
; Initial version
;
;
;
;-


;++++++++++++++++++++++++++++++++++++
; Set up variables and templates
;------------------------------------

; Initialize variables
codename 	= 'AIA_BP_READ_INST.PRO'
codeversion = '$Id: aia_bp_read_inst.pro,v 1.4 2011/04/21 17:50:12 boerner Exp $'
datenow		= AIA_BP_DATE_STRING(/time)

errors 		= ''
warnings 	= ''

if not KEYWORD_SET(silent) then silent = 0
if not KEYWORD_SET(logdir) then logdir = 0
if not KEYWORD_SET(chanwrite) then chanwrite = 0
if not KEYWORD_SET(official) then official = 0

; Check that the filename format is AAAAA_inst.txt
AIA_BP_PARSE_FILENAME, filename, 'inst', shortfile = shortfile, $
	basefile = basefile, warnings = warnings, path=filepath
	
if official then begin
	opath = !official_bp + datenow + '/'
	official = opath
	mkcmd = 'mkdir ' + opath
	SPAWN, mkcmd, result, error
	CD, opath, current = orig_dir
	cpcmd = 'cp ' + filename + './'
	SPAWN, cpcmd, result, error
endif else begin
	CD, filepath, current = orig_dir
endelse

filetext = RD_TFILE(shortfile, 2, /nocomm, /compress, delim = '--')
rsize = SIZE(filetext)

; Set up the instrument information structure and fill it with the defaults
inststr_template = {Name : 'AIA', FilePath : '.', MakeChanFiles : 1}

; Go through each field in the instrument information structure and check to
; see whether the inst.txt file sets its value. If not (or if it is specified
; redundantly), use the default and print a warning
inststr = AIA_BP_FILE2TEMPLATE(inststr_template, filetext, $
	warnings = warnings)

; Add the top-level result fields to the output structure
inststr = CREATE_STRUCT('Filename', filename, 'Date', datenow, $
	'CodeName', codename, 'CodeVersion', codeversion, inststr)	


;++++++++++++++++++++++++++++++++++++
; Look for inheritable parameters and 
; set up the inheritance structure 
; which the channels can use if they 
; request it
;------------------------------------
common aia_bp_inststr, inherit_str
inherit_str = CREATE_STRUCT('CreationDate', AIA_BP_DATE_STRING(/time))
inheritableparams = ['GeoArea', 'Scale', 'Platescale', 'NumFilters', 'NumMirrors', $
	'WaveMin', 'WaveStep', 'WaveNumSteps', 'WaveLog', 'UseWaveFile', 'WaveFile', $
	'FiltersIncludeMesh', 'MeshTrans', 'UseContam', 'ContamFile', 'ContamThick', $
	'UsePhotToElec', 'ElecPerEV', 'UsePhotToDN', 'ElecPerDN', $
	'UseError', 'ErrorFile', 'ExtrapolateComponents']
numinheritable = N_ELEMENTS(inheritableparams)
num2inherit = 0
for i = 0, numinheritable - 1 do begin
	paramind = WHERE(STRUPCASE(filetext[0,*]) eq STRUPCASE(inheritableparams[i]), hasparam)
	if hasparam ge 1 then begin
		if num2inherit eq 0 then begin
			inherit_str = CREATE_STRUCT(filetext[0,paramind], filetext[1,paramind])
		endif else begin
			inherit_str = CREATE_STRUCT(inherit_str, $
				filetext[0,paramind], filetext[1,paramind])
		endelse
		num2inherit = num2inherit + 1
	endif
endfor


;++++++++++++++++++++++++++++++++++++
; Generate the channel substructures
;------------------------------------
if inststr.MakeChanFiles eq 1 then begin
	; If the channels that make up the instrument are defined in the inst.txt 
	; file, then use them to generate subchan.txt files and read those in
	numchan = 0
	channames = ''
	chanlines = INTARR(rsize[2])
	for i = 0, rsize[2] - 1 do begin
		; loop through and find lines that identify a numbered channel
		tagsep = STRSPLIT(/extract, filetext[0,i], ' ')
		if STRUPCASE(STRMID(tagsep[0],0, 4)) eq 'CHAN' then begin
			chanid = STRMID(tagsep[0], 4, 3)
			if chanid ne '' then chanlines[i] = FIX(chanid)
		endif
	endfor
	schanlines = chanlines[SORT(chanlines)]
	uchanlines = schanlines[UNIQ(schanlines)]
	nuchans = N_ELEMENTS(uchanlines) - 1
	if nuchans gt 0 then begin
		for i = 0, nuchans - 1 do begin
			thischannum = uchanlines[i+1]
			thischanfile = AIA_BP_DATE_STRING(/time) + '_chan_' + $
				STRTRIM(thischannum, 2) + '_subchan.txt'
			OPENW, lun, /get_lun, thischanfile, width=200
			PRINTF, lun, '# ' + thischanfile
			PRINTF, lun, '# Written by AIA_BP_READ_INST on ' + SYSTIME()
			PRINTF, lun, '# Generated automatically by ' + filename
			PRINTF, lun, '#'
			thischanlines = WHERE(chanlines eq thischannum, numlines)
			inheritmask = INTARR(numinheritable)
			for j = 0, numlines - 1 do begin
				fieldsep = STRSPLIT(/extract, filetext[0, thischanlines[j]], ' ')
				linestruct = CREATE_STRUCT('linetype', fieldsep[1], 'tagname', $
					fieldsep[2], 'value', filetext[1, thischanlines[j]])
				if STRUPCASE(linestruct.linetype) eq 'GEN' then begin
					PRINTF, lun, linestruct.tagname, ' -- ', linestruct.value
				endif else begin
					PRINTF, lun, linestruct.linetype, '  ', linestruct.tagname, $
						' -- ', linestruct.value
				endelse
				whereinherit = WHERE(STRUPCASE(inheritableparams) eq $
					STRUPCASE(linestruct.tagname), hasparam)
				if hasparam ge 1 then inheritmask[whereinherit[0]] = 1
			endfor
			for j = 0, numinheritable -1  do begin
				if inheritmask[j] ne 1 then PRINTF, lun, inheritableparams[j], $
					' -- ', 'II'
			endfor
			FREE_LUN, lun
			AIA_BP_READ_CHAN, thischanfile, chanstr, fullchanstr, $
				silent=silent, logdir=logdir, write = chanwrite, official = official
			if not KEYWORD_SET(chanwrite) then FILE_DELETE, thischanfile
			numchan = numchan + 1
			channame = chanstr.name
			channames = [channames, channame]
			chanfiletag = channame + '_file'
			if numchan eq 1 then begin
				substr = CREATE_STRUCT(channame, chanstr, channame + '_full', $
					fullchanstr, chanfiletag, thischanfile)
			endif else begin
				substr = CREATE_STRUCT(substr, channame, chanstr, $
					channame + '_full', fullchanstr, $
					chanfiletag, thischanfile)
			endelse
		endfor
	endif else begin
		warnstring = 'No numbered channels defined in inst.txt file!'
		warnings = [warnings, 'WARNING! ' + warnstring]
	endelse
endif else begin
	; Look for channels specified in the inst.txt file and add them to the
	; instrument response structure.
	numchan = 0
	channames = ''
	for i = 0, rsize[2] - 1 do begin
		tagsep = STRSPLIT(/extract, filetext[0,i], ' ')
		if STRUPCASE(tagsep[0]) eq 'CHAN' then begin
			numchan = numchan + 1
			channame = tagsep[1]
			channames = [channames, channame]
			chanfile = filetext[1,i]
			chanfiletag = channame + '_file'

			if KEYWORD_SET(official) then begin
				cpcmd = 'cp ' + inststr.FilePath + chanfile + ' ./'
				AIA_BP_READ_CHAN, chanfile, chanstr, fullchanstr, silent = silent, $
					logdir = logdir, write = chanwrite, official = official
			endif else begin
				CD, inststr.FilePath, current = old_dir
				AIA_BP_READ_CHAN, chanfile, chanstr, fullchanstr, silent = silent, $
					logdir = logdir, write = chanwrite, official = official
				CD, old_dir
			endelse
			
			if numchan eq 1 then begin
				substr = CREATE_STRUCT(channame, chanstr, channame + '_full', $
					fullchanstr, chanfiletag, chanfile)
			endif else begin
				substr = CREATE_STRUCT(substr, channame, chanstr, $
					channame + '_full', fullchanstr, $
					chanfiletag, chanfile)
			endelse
		endif
	endfor
endelse

channames = channames[1:N_ELEMENTS(channames)-1]
inststr = CREATE_STRUCT(inststr, 'Channels', channames, substr)


;++++++++++++++++++++++++++++++++++++
; Incorporate notes and warning
;------------------------------------

; Look for notes specified in the inst.txt file and add them to the
; instrument response structure. 
numnotes = 0
notes = ''
for i = 0, rsize[2] - 1 do begin
	if filetext[0,i] eq 'NOTE' then begin
		numnotes = numnotes + 1
		notes = [ notes, STRTRIM(filetext[1,i], 2) ]
	endif
endfor
if numnotes ge 1 then begin
	notes = notes[1:numnotes]
	inststr = CREATE_STRUCT(inststr, 'notes', notes)
endif

; Check for errors and inconsistencies, and write out a warning log file
; if necessary
numwarn = N_ELEMENTS(warnings)
if numwarn gt 1 then begin
;	warnings = warnings[1, numwarn[0]-1]	;	drop the dummy first element
	if not KEYWORD_SET(silent) then for i = 1, numwarn-1 do PRINT, warnings[i]
	if KEYWORD_SET(logdir) then begin
		if logdir eq '1' then logdir = inststr.FilePath
		sepfile = STRSPLIT(/extract, filename, '.')
		warnfile = AIA_BP_DATE_STRING(/time) + '_' + basefile + '_warn.txt'
		if not KEYWORD_SET(official) then CD, logdir, current = old_dir
		OPENW, loglun, /get_lun, warnfile
		for i = 1, numwarn-1 do PRINTF, loglun, warnings[i]
		CLOSE, loglun
		if not KEYWORD_SET(official) then CD, old_dir
	 endif
endif 


;++++++++++++++++++++++++++++++++++++
; Generate output variables and files
;------------------------------------

; Calculate a top-level instrument response array, and then
; pull out only the basic fields and return a short-form structure
fullinststr = inststr

insttags = TAG_NAMES(inststr)
inherittags = TAG_NAMES(inherit_str)
wavefiletag = WHERE(inherittags eq 'USEWAVEFILE', inherit_usewave)
if inherit_usewave then begin
	wavegrid = substr.(0).wave
	numsteps = N_ELEMENTS(wavegrid)
endif else begin
	wavemin = 15000.
	wavemax = 1.
	wavestep = 1000.
	for i = 0, numchan - 1 do begin
		thisfield = WHERE(insttags eq STRUPCASE(channames[i]))
		thisstr = inststr.(thisfield[0])
		thiswavemin = MIN(thisstr.wave)
		thiswavemax = MAX(thisstr.wave)
		thiswavestep = thisstr.wave[1] - thisstr.wave[0]
		wavemin = wavemin < thiswavemin
		wavemax = wavemax > thiswavemax
		wavestep = wavestep < thiswavestep
	endfor
	waverange = wavemax - wavemin
	numsteps = (waverange / wavestep) + 1
	wavegrid = FINDGEN(numsteps) * wavestep + wavemin
endelse
all_eas = FLTARR(numchan, numsteps)
for i = 0, numchan - 1 do begin
	thisfield = WHERE(insttags eq STRUPCASE(channames[i]))
	thisstr = inststr.(thisfield[0])
	all_eas[i, *] = INTERPOL(thisstr.ea, thisstr.wave, wavegrid)
endfor
shortstr = {Name : inststr.Name, Channels : channames, Wave : wavegrid, $
	all : all_eas, Platescale : thisstr.platescale, Units : thisstr.units}	
	;	Assumes all channels have the same units
for i = 0, numchan - 1 do begin
	thisfield = WHERE(insttags eq STRUPCASE(channames[i]))
	thisstr = inststr.(thisfield[0])
	shortstr = CREATE_STRUCT(shortstr, channames[i], thisstr)
endfor
inststr = shortstr

; If requested, write out a .dat and .genx files summarizing the instrument
; response in the FilePath
if KEYWORD_SET(write) then begin
	if not KEYWORD_SET(official) then CD, fullinststr.FilePath, current = old_dir
	OPENW, lun, /get_lun, basefile + '_inst.dat'
	PRINTF, lun, '# Written by AIA_BP_READ_INST on ' + SYSTIME()
	PRINTF, lun, '# Effective area for all channels on ' + fullinststr.Name
	PRINTF, lun, '# Units are ' + inststr.Units
	PRINTF, lun, '#', 'Wave [Ang]', inststr.Channels, $
		format = '(a1, a14, ' + STRTRIM(numchan, 2) + 'a15)'
	for i = 0l, N_ELEMENTS(inststr.wave)-1 do $
		PRINTF, lun, '', inststr.wave[i], inststr.all[*,i], $
		format = '(a1, f14.3, ' + STRTRIM(numchan, 2) + 'e15.5)'
	CLOSE, lun
	SAVEGEN, file = basefile + '_inst.genx', str = inststr
	SAVEGEN, file = basefile + '_fullinst.genx', str = fullinststr
	if not KEYWORD_SET(official) then CD, old_dir
endif

CD, orig_dir


end

