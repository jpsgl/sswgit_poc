pro AIA_FIX_EXPTIME, header, $
	update = update, silent = silent

;+
;
; Checks the header of an AIA image for consistency between commanded exposure
; (AIMGSHCE) and actual exposure (EXPTIME) . 
;
; KEYWORDS:
;	/update	-	If set, then the header .exptime and .history are adjusted in
;				place if necessary
;	/silent	-	If not set, then if any discrepancy is detected then a message is
;				printed (using BOX_MESSAGE)
;
;-

errlist = WHERE( ABS(header.exptime*1000 - header.aimgshce) gt 5 , numerr)
if numerr gt 0 then for i = 0, numerr - 1 do begin
	corrstr = STRING('AIA_FIX_EXPTIME: measured = ', header[errlist[i]].exptime * 1000., $
		' commanded = ', header[errlist[i]].aimgshce, form = '(a30, f7.1, a15, f7.1)')
	if KEYWORD_SET(update) then begin
		header[errlist[i]].exptime = header[errlist[i]].aimgshce/1000. 
		actstr = 'Header exptime updated to commanded value...'
		UPDATE_HISTORY, header[errlist[i]], $
			'AIA_FIX_EXPTIME set exptime to ' + STRTRIM(header[errlist[i]].exptime, 2)
	endif else begin
		actstr = 'Header not changed; consider using commanded value...'
	endelse
	if not KEYWORD_SET(silent) then BOX_MESSAGE, [corrstr, actstr]	
endfor

end