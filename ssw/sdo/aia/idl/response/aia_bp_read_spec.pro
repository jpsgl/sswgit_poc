pro AIA_BP_READ_SPEC, filename, $
	specstr, fullspecstr, $
	warnings, $
	write = write, logdir = logdir, silent = silent
	
;+
;
; $Id: aia_bp_read_spec.pro,v 1.2 2010/08/03 00:28:38 boerner Exp $
;
; INPUTS:
;	filename	-	string giving the name of a _spec.txt file to be used as
;					the makefile for the spectrum. (See TEMPLATE_spec.txt for
;					an example).
;
; OUTPUTS:
;	specstr		-	a simple structure giving the resulting spectrum. Has 4 fields:
;						name, wave, spec and units
;
;	fullspecstr	-	a structure giving the spectrum along with the breakdown
; 					of spectral contribution vs. temperature
;
;	warnings	-	a string array containing any warnings produced during the 
;					calculation
;
; KEYWORDS:
;	/write		-	if set, the specstr and fullspecstr are written to 
;					.genx files, and an ASCII file containing the spectrum
;					is saved as well.
;
;	/silent		-	if set, warnings are suppressed (though they are still 
;					passed back out of the routine)
;
;	logdir=		-	can be set to a path, in which case the warnings are also
;					written to a text file at that path
;
;-

errors = ''
warnings = ''

if not KEYWORD_SET(logdir) then logdir = 0
if not KEYWORD_SET(silent) then silent = 0

; Check that the filename format is AAAAA_spec.txt
AIA_BP_PARSE_FILENAME, filename, 'spec', shortfile = shortfile, $
	basefile = basefile, warnings = warnings, path=filepath
CD, filepath, current = orig_dir

	
result = RD_TFILE(filename, 2, /nocomm, /compress, delim = '--')
rsize = SIZE(result)


; Set up the spectrum information structure and fill it with the defaults
specstr_template = {Name : 'noname', $
					DEMFile : 'TEMPLATE_dem.dat', $
					EmissDatFile : 'TEMPLATE_emiss.dat', $
					UseFullEmiss : 0 }
if KEYWORD_SET(write) then begin
	; only add these fields to the structure if writing output files
	specstr_template = CREATE_STRUCT(specstr_template, $
						'FullEmissGenxFile', '0', $
						'OutFilePath', '.' )
endif


; Go through each field in the spectrum information structure and check to
; see whether the spec.txt file sets its value. If not (or if it is specified
; redundantly), use the default and print a warning
temp_specstr = AIA_BP_FILE2TEMPLATE(specstr_template, result, $
	warnings = warnings)
if temp_specstr.name ne 'noname' then basefile = temp_specstr.name


; Read in the emissivity data
if temp_specstr.UseFullEmiss eq 0 then begin
	emissdat = RD_TFILE(temp_specstr.EmissDatFile, nocomment = ';', /autocol)
	emiss_size = SIZE(emissdat)
	headrow = emissdat[*,0]
	logte = FLOAT(REFORM(headrow[1:emiss_size[1]-1]))
	wave = FLOAT(REFORM(emissdat[0, 1:emiss_size[2]-1]))
	emissivity = DOUBLE(emissdat[1:emiss_size[1]-1, 1:emiss_size[2]-1])
	units = 'photons cm3 s-1 sr-1 A-1'
endif else begin
	RESTGEN, file = temp_specstr.FullEmissGenxFile, str = emissstr
	logte = emissstr.total.logte
	wave = emissstr.total.wave
	emissivity = TRANSPOSE(emissstr.total.emissivity)	; to match the form
								; of emissivity read in from a DAT file
	units = emissstr.total.units
	temp_specstr = CREATE_STRUCT(temp_specstr, 'emiss_notes', emissstr.notes, $
		'emiss_general', emissstr.general)
endelse


; Read in the DEM data
demstr = AIA_BP_READ_DEM(temp_specstr.DEMFile)


; Make the spectrum
specout = AIA_BP_MAKE_SPEC(wave, logte, emissivity, demstr, spec_matrix, tgrid)


; Look for notes specified in the spec.txt file and add them to the
; spectrum structure. 
numnotes = 0
notes = ''
for i = 0, rsize[2] - 1 do begin
	if result[0,i] eq 'NOTE' then begin
		numnotes = numnotes + 1
		notes = [ notes, STRTRIM(result[1,i], 2) ]
	endif
endfor
if numnotes ge 1 then begin
	notes = notes[1:numnotes-1]
	temp_specstr = CREATE_STRUCT(temp_specstr, 'notes', notes)
endif


; Check for errors and inconsistencies, and write out a warning log file
; if necessary
numwarn = N_ELEMENTS(warnings)
if numwarn gt 1 then begin
	warnings = warnings[1:numwarn-1]	;	drop the dummy first element
	if not KEYWORD_SET(silent) then for i = 0, numwarn-2 do PRINT, warnings[i]
	if KEYWORD_SET(logdir) then begin
		if logdir eq '1' and KEYWORD_SET(write) then logdir = temp_specstr.OutFilePath
		sepfile = STRSPLIT(/extract, filename, '.')
		warnfile = AIA_BP_DATE_STRING(/time) + '_' + basefile[0] + '_warn.txt'
		CD, logdir, current = old_dir
		OPENW, loglun, /get_lun, warnfile
		for i = 0, numwarn-2 do PRINTF, loglun, warnings[i]
		FREE_LUN, loglun
		CD, old_dir
	 endif
endif 


; Generate the fullspecstr and the simple specstr
fullspecstr = CREATE_STRUCT(temp_specstr, 'Wave', wave, 'Spec', specout, $
	'Units', 'photons cm-2 s-1 sr-1 A-1', 'LogTe', tgrid, 'Spec_matrix', spec_matrix, $
	'Date', SYSTIME())

specstr = {Name : temp_specstr.Name, Wave : fullspecstr.Wave, Spec : fullspecstr.spec, $
	units : fullspecstr.Units}


; If requested, write out the response in a dat and genx file.  
if KEYWORD_SET(write) then begin
	CD, current = old_dir, temp_specstr.OutFilePath
	OPENW, lun, /get_lun, basefile + '_spec.dat'
	PRINTF, lun, '# Written by AIA_BP_READ_SPEC on ' + SYSTIME()
	PRINTF, lun, '# Spectrum for ' + specstr.Name
	PRINTF, lun, '# Units are ' + specstr.Units
	PRINTF, lun, '#', 'Wave [Ang]', 'Spectrum', format = '(a1, a14, a15)'
	for i = 0l, N_ELEMENTS(specstr.wave)-1 do $
		PRINTF, lun, '', specstr.wave[i], specstr.spec[i], format = '(a1, f14.3, e15.5)'
	FREE_LUN, lun
	SAVEGEN, file = basefile + '_spec.genx', str = specstr
	SAVEGEN, file = basefile + '_fullspec.genx', str = fullspecstr
	CD, old_dir
endif


CD, orig_dir

 
end