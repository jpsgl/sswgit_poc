pro AIA_BP_CHIANTIFIX_PLOT_FLAREFIT, i, flare_tai, aia_flare_obs_bgsub, $
	post_pred, pre_pred, unsat_ind, thistresp, fdem_fit, fit_coeffs, plim, $
	eps = eps, outpath = outpath, write_plots = write_plots

;+
;
;
;
;-

if KEYWORD_SET(eps) and (i lt 2) then begin	;	Only 94 and 131 go to PS
	!p.font = 0
	LOADCT, 0
	thick = 2
	chars = 0.95
	if i eq 0 then begin
		!p.multi = [0, 1, 2]
		DEVICE, file = outpath + '/aia_eve_dem_psplot.eps', $
			/portrait, /inches, xsize = 7, ysize = 6, $
			/color, /isolatin1, /encapsulate, /helvetica
		ymargin = [1,1]
		xtit = ''
		ylabel = '94 '
	endif else begin
		!p.multi = [1, 1, 2]
		ymargin = [4,1]
		ylabel = '131 '
	endelse
	UTPLOT, TAI2UTC(flare_tai), aia_flare_obs_bgsub[*,i], thick = thick, col = 0, chars = chars, $
		yrange = [0, plim[i]], ytit = ylabel + 'Mean [DN]', $
		/ymin, xtit = xtit, ymarg = ymargin, /xminor
	PB_SET_LINE_COLOR
	OUTPLOT, TAI2UTC(flare_tai[300+unsat_ind]), post_pred, col = 5, thick = thick
	OUTPLOT, TAI2UTC(flare_tai[300+unsat_ind]), pre_pred, col = 2, thick = thick
	if i eq 0 then begin
		PB_LEGEND, col = [255, 2, 5], /norm, /line, thick = thick, size = chars, /eps, $
			['AIA Observed', 'Predicted: DEM + Original Tresp', 'Predicted: DEM + Scaled Tresp'], $
			0.5, 0.4
	endif else begin
		DEVICE, /close
		!p.font = oldpfont
		!p.multi = oldpmulti
		TVLCT, rr, gg, bb
	endelse
endif else begin
	; Plot the revised T response
	WINDOW, 10
	PLOT, thistresp.logte, thistresp.tresp, xtitle = 'Log(T)', ytitle = 'T Response', chars = 1.5, $
		title = STRTRIM(wvls[i], 2) + P_ANG() + ' Channel T response -- Flare Spectrum Fit', $
		yrange = [0, MAX([thistresp.tresp, fdem_fit.tresp])], thick = 2, xmargin = [12, 4], xrange = [5, 8]
	OPLOT, fdem_fit.logte, fdem_fit.tresp, col = 2, thick = 2
	PB_LEGEND, ['Original', 'Scaled by ' + STRING(10.^fit_coeffs[0], format = '(f6.3)')], $
		/dev, 200, 700, col = [255, 2], size = 1.5, /line
	if KEYWORD_SET(write_plots) then begin
		ftresp_fname = outpath + '/ftresp_fit_' + STRTRIM(wvls[i], 2) + '.png'
		PB_WIN2PNG, ftresp_fname
	endif
	; Plot the results of the fit
	WINDOW, 11
	UTPLOT, TAI2UTC(flare_tai), aia_flare_obs_bgsub[*,i], thick = 2, $
		yrange = [0, plim[i]], charsize = 1.5, ytit = 'Full Disk Average Count Rate', $
		title = STRTRIM(wvls[i], 2) + P_ANG() + ' Channel Observed/Predicted Counts'
	OUTPLOT, TAI2UTC(flare_tai[300+unsat_ind]), post_pred, col = 6, thick = 2
	OUTPLOT, TAI2UTC(flare_tai[300+unsat_ind]), pre_pred, col = 2
	PB_LEGEND, ['AIA Observed', 'Predicted: DEM + Original Tresp', 'Predicted: DEM + Scaled Tresp'], $
		col = [255, 2, 6], /dev, 700, 700, /line, size = 1.5
	if KEYWORD_SET(write_plots) then begin
		fcount_fname = outpath + '/fcount_fit_' + STRTRIM(wvls[i], 2) + '.png'
		PB_WIN2PNG, fcount_fname
	endif
endelse

end