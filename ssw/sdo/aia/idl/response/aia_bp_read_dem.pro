function AIA_BP_READ_DEM, filename, $
	ch = ch, qs = qs, ar = ar, fl = fl, $
	name = name
	
;+
;
; Wrapper for CHIANTI READ_DEM routine
;
; $Id: aia_bp_read_dem.pro,v 1.2 2010/08/10 22:46:07 boerner Exp $
;
; $Log: aia_bp_read_dem.pro,v $
; Revision 1.2  2010/08/10 22:46:07  boerner
; Initial version
;
; Revision 1.1  2010/06/24 17:02:32  boerner
; initial version
;
;
; RETURNS:	demstr-	a structure containing the DEM
;
; INPUT:	filename- the _dem.txt (or _dem.dat) file name containing the 
;				DEM to be read in. Should have 2 columns, with temperature
;				and DEM both in log10 units. 
;
; KEYWORDS:	/ch /qs /ar /fl	--	set either none or one of these
;
;				If any one of them is set AND filename is not specified
;				(either not set or an undefined variable), then the standard
;				DEM for the specified feature type is read in from !xuvtop.
;			
;				If multiple keywords are set, then /ar (active region) takes
;				precedence
;
;				If the filename input parameter is passed in, then these keywords
;				are ignored
;
;			name=	can be set to a string that will go in as the name of the 
;					demstr. Otherwise, the filename is used.
;
;-

if N_ELEMENTS(filename) eq 0 then begin
	if KEYWORD_SET(ar) then demfile = 'active_region.dem' $
		else if KEYWORD_SET(qs) then demfile = 'quiet_sun.dem' $
			else if KEYWORD_SET(fl) then demfile = 'flare.dem' $
				else if KEYWORD_SET(ch) then demfile = 'coronal_hole.dem' $
					else demfile = 'active_region.dem'
	CD, !xuvtop + '/dem', current = old_dir
	READ_DEM, demfile, t, dem, ref
	CD, old_dir
	filename = demfile
endif else begin
	result = RD_TFILE(filename, 2, /nocomm, /convert)
	t = REFORM(result[0,*])
	dem = REFORM(result[1,*])
endelse

if not KEYWORD_SET(name) then name = filename

demstr = {Name : name, LogTE : t, LogDEM : dem, Units : 'cm-5 K-1'}

RETURN, demstr


end