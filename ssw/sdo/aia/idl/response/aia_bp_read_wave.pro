function AIA_BP_READ_WAVE, filename

;+
;
; $Id: aia_bp_read_wave.pro,v 1.1 2010/05/28 22:31:25 boerner Exp $
;
; $Log: aia_bp_read_wave.pro,v $
; Revision 1.1  2010/05/28 22:31:25  boerner
; Initial version
;
; 
; Helper routine for AIA_BP_READ_AAAA.PRO programs. 
; 
; Reads in a wave.txt file (which describes the wavelength grid over
; which a spectrum or instrument response is to be calculated) and 
; returns the contents as a string array
;
;-

result = RD_TFILE(filename, /nocomm, /compress, /convert)

RETURN, result

end
