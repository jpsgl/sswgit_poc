# $Id: README.txt,v 1.5 2012/01/31 00:37:27 boerner Exp $
#
# Description of the types of files used in the bandpass modeling routines
#

#+++++++++++++++++++++++++++++++++++
# STRUCTURE VARIABLES
#-----------------------------------

EFFECTIVE AREA:

IDL> help, aia_get_response(/area, /dn), /str
** Structure <489ec08>, 16 tags, length=353864, data length=353826, refs=1:
   NAME            STRING    'AIA'
   VERSION         INT              2
   DATE            STRING    '20120130_235659'
   CHANNELS        STRING    Array[7]
   WAVE            FLOAT     Array[4001]
   ALL             FLOAT     Array[7, 4001]
   PLATESCALE      FLOAT       8.46158e-12
   UNITS           STRING    'cm^2 DN phot^-1'
   A94             STRUCT    -> <Anonymous> Array[1]
   A131            STRUCT    -> <Anonymous> Array[1]
   A171            STRUCT    -> <Anonymous> Array[1]
   A193            STRUCT    -> <Anonymous> Array[1]
   A211            STRUCT    -> <Anonymous> Array[1]
   A304            STRUCT    -> <Anonymous> Array[1]
   A335            STRUCT    -> <Anonymous> Array[1]
   CORRECTIONS     STRUCT    -> <Anonymous> Array[1]

IDL> help, aia_get_response(/area, /dn, /full), /str                           
** Structure <405c608>, 21 tags, length=1234792, data length=1234716, refs=1:
   NAME            STRING    'AIA'
   VERSION         INT              2
   DATE            STRING    '20120130_235720'
   FILEDATE        STRING    '20100804_232510'
   CHANNELS        STRING    Array[7]
   A94             STRUCT    -> <Anonymous> Array[1]
   A94_FULL        STRUCT    -> <Anonymous> Array[1]
   A131            STRUCT    -> <Anonymous> Array[1]
   A131_FULL       STRUCT    -> <Anonymous> Array[1]
   A171            STRUCT    -> <Anonymous> Array[1]
   A171_FULL       STRUCT    -> <Anonymous> Array[1]
   A193            STRUCT    -> <Anonymous> Array[1]
   A193_FULL       STRUCT    -> <Anonymous> Array[1]
   A211            STRUCT    -> <Anonymous> Array[1]
   A211_FULL       STRUCT    -> <Anonymous> Array[1]
   A304            STRUCT    -> <Anonymous> Array[1]
   A304_FULL       STRUCT    -> <Anonymous> Array[1]
   A335            STRUCT    -> <Anonymous> Array[1]
   A335_FULL       STRUCT    -> <Anonymous> Array[1]
   CORRECTIONS     STRUCT    -> <Anonymous> Array[1]
   NOTES           STRING    Array[1]

IDL> help, (aia_get_response(/area, /dn [, /full])).a94, /str
** Structure <18f004a8>, 5 tags, length=32048, data length=32044, refs=1:
   NAME            STRING    'A94'
   WAVE            FLOAT     Array[4001]
   EA              FLOAT     Array[4001]
   PLATESCALE      FLOAT       8.46158e-12
   UNITS           STRING    'cm^2 DN phot^-1'

IDL> help, (aia_get_response(/area, /dn)).corrections, /str
** Structure <18f06b98>, 5 tags, length=1320, data length=1320, refs=1:
   TIME            STRUCT    -> <Anonymous> Array[1]
   TIME_APPLIED    STRING    'NO'
   EVENORM         STRUCT    -> <Anonymous> Array[1]
   EVENORM_APPLIED STRING    'NO'
   INFO            STRING    Array[6]

IDL> help, (aia_get_response(/area, /dn, /full)).a94_full, /str
** Structure <4015a08>, 27 tags, length=144136, data length=144130, refs=1:
   WAVE            FLOAT     Array[4001]
   EFFAREA         FLOAT     Array[4001]
   UNITS           STRING    'cm^2 DN phot^-1'
   DATE            STRING    '20100804_232510'
   NAME            STRING    'A94'
   GEOAREA         FLOAT           83.0000
   SCALE           FLOAT           1.00000
   PLATESCALE      FLOAT       8.46158e-12
   WAVEMIN         FLOAT           25.0000
   WAVESTEP        FLOAT          0.100000
   WAVENUMSTEPS    LONG              4001
   FILTERSINCLUDEMESH
                   INT              1
   USECONTAM       INT              1
   CONTAMTHICK     INT            275
   USEPHOTTOELEC   INT              1
   ELECPEREV       FLOAT          0.273973
   USEPHOTTODN     INT              1
   ELECPERDN       FLOAT           18.3000
   USEERROR        INT              0
   EXTRAPOLATECOMPONENTS
                   INT              1
   ENT_FILTER      FLOAT     Array[4001]
   FP_FILTER       FLOAT     Array[4001]
   PRIMARY         FLOAT     Array[4001]
   SECONDARY       FLOAT     Array[4001]
   CCD             FLOAT     Array[4001]
   CONTAM          FLOAT     Array[4001]
   CROSS_AREA      FLOAT     Array[4001]
   
Note: 
	Here is how EFFAREA is calculated:
	if USEPHOTTOELEC and USEPHOTTODN then UNITS = 12398./WAVE * ELECPEREV / ELECPERDN else UNITS = 1
	EFFAREA = ((GEOAREA * ENT_FILTER * FP_FILTER * PRIMARY * SECONDARY * CCD * CONTAM) + CROSS_AREA) * UNITS
	
	
TEMPERATURE RESPONSE:

IDL> help, aia_get_response(/temp, /dn), /str
** Structure <2ab9608>, 17 tags, length=23472, data length=23416, refs=1:
   NAME            STRING    'AIA'
   DATE            STRING    '20120130_235838'
   EFFAREA_VERSION INT              2
   EMISS_VERSION   INT              2
   EMISSINFO       STRUCT    -> <Anonymous> Array[1]
   CHANNELS        STRING    Array[7]
   UNITS           STRING    'DN cm^5 s^-1 pix^-1'
   LOGTE           FLOAT     Array[101]
   ALL             DOUBLE    Array[101, 7]
   A94             STRUCT    -> <Anonymous> Array[1]
   A131            STRUCT    -> <Anonymous> Array[1]
   A171            STRUCT    -> <Anonymous> Array[1]
   A193            STRUCT    -> <Anonymous> Array[1]
   A211            STRUCT    -> <Anonymous> Array[1]
   A304            STRUCT    -> <Anonymous> Array[1]
   A335            STRUCT    -> <Anonymous> Array[1]
   CORRECTIONS     STRUCT    -> <Anonymous> Array[1]

IDL> help, aia_get_response(/temp, /dn, /full), /str
** Structure <1926e08>, 20 tags, length=37385144, data length=37385092, refs=1:
   NAME            STRING    'AIA'
   DATE            STRING    '20120130_235857'
   EFFAREA_VERSION INT              2
   EMISS_VERSION   INT              2
   EMISSINFO       STRUCT    -> <Anonymous> Array[1]
   LOGTE           FLOAT     Array[101]
   WAVE            FLOAT     Array[6601]
   TUNITS          STRING    'DN cm^5 s^-1 pix^-1'
   TWUNITS         STRING    'DN cm^5 s^-1 A^-1 pix^-1'
   CHANNELS        STRING    Array[7]
   TRESP           DOUBLE    Array[101, 7]
   TWRESP          DOUBLE    Array[6601, 101, 7]
   A94             STRUCT    -> <Anonymous> Array[1]
   A131            STRUCT    -> <Anonymous> Array[1]
   A171            STRUCT    -> <Anonymous> Array[1]
   A193            STRUCT    -> <Anonymous> Array[1]
   A211            STRUCT    -> <Anonymous> Array[1]
   A304            STRUCT    -> <Anonymous> Array[1]
   A335            STRUCT    -> <Anonymous> Array[1]
   CORRECTIONS     STRUCT    -> <Anonymous> Array[1]

IDL> help, (aia_get_response(/temp, /dn)).corrections, /str
** Structure <18003788>, 7 tags, length=7512, data length=7508, refs=1:
   TIME            STRUCT    -> <Anonymous> Array[1]
   TIME_APPLIED    STRING    'NO'
   EVENORM         STRUCT    -> <Anonymous> Array[1]
   EVENORM_APPLIED STRING    'NO'
   INFO            STRING    Array[6]
   CHIANTIFIX      STRUCT    -> <Anonymous> Array[1]
   CHIANTIFIX_APPLIED
                   STRING    'NO'

IDL> help, (aia_get_response(/temp, /dn [, /full])).a94, /str
** Structure <e8020f8>, 4 tags, length=1248, data length=1244, refs=1:
   NAME            STRING    'A94'
   UNITS           STRING    'DN cm^5 s^-1 pix^-1'
   LOGTE           FLOAT     Array[101]
   TRESP           DOUBLE    Array[101]


EMISSIVITY:

IDL> help, aia_get_response(/emiss), /str     
** Structure <95004c8>, 7 tags, length=5361512, data length=5361490, refs=1:
   WAVE            FLOAT     Array[6601]
   LOGTE           FLOAT     Array[101]
   EMISSIVITY      DOUBLE    Array[6601, 101]
   UNITS           STRING    'photons cm3 s-1 sr-1 A-1'
   DATE            STRING    '20111117_010509'
   CH_INFO         STRUCT    -> MS_066526235008 Array[1]
   VERSION         INT              2

IDL> help, aia_get_response(/emiss, /full), /str
** Structure <10800008>, 8 tags, length=282351472, data length=279292560, refs=1:
   DATE            STRING    '20111117_010509'
   GENERAL         STRUCT    -> MS_066526235008 Array[1]
   LINES           STRUCT    -> MS_066526235009 Array[305889]
   CONT            STRUCT    -> MS_066526235010 Array[1]
   FIXES           STRING    Array[3]
   TOTAL           STRUCT    -> MS_066526235011 Array[1]
   NOTES           STRING    Array[1]
   VERSION         INT              2

IDL> help, (aia_get_response(/emiss, /full)).lines, /str
** Structure MS_066526235009, 11 tags, length=888, data length=878:
   IZ              INT              2
   ION             INT              2
   IDENT           STRING    '1s 2S1/2 - 2p 2P1/2'
   IDENT_LATEX     STRING    '1s $^2$S$_{1/2}$ - 2p $^2$P$_{1/2}$'
   SNOTE           STRING    'He II'
   LVL1            INT              1
   LVL2            INT              3
   TMAX            FLOAT           4.90000
   WVL             DOUBLE           303.78601
   FLAG            INT              0
   GOFT            DOUBLE    Array[101]


SPECTRUM:

IDL> help, aia_bp_default_spectrum(/ar), /str
** Structure <18000848>, 4 tags, length=79248, data length=79244, refs=1:
   DATE            STRING    '20120131_000055'
   WAVE            FLOAT     Array[6601]
   SPEC            DOUBLE    Array[6601]
   UNITS           STRING    'photons cm-2 s-1 sr-1 A-1'
   
IDL> help, aia_bp_default_spectrum(/ar, /full), /str
** Structure <f98c328>, 8 tags, length=3089568, data length=3089560, refs=1:
   DATE            STRING    '20120131_000113'
   DEMNAME         STRING    'active_region.dem'
   EMISSDATE       STRING    '20111117_010509'
   WAVE            FLOAT     Array[6601]
   SPEC            DOUBLE    Array[6601]
   UNITS           STRING    'photons cm-2 s-1 sr-1 A-1'
   LOGTE           FLOAT     Array[57]
   SPEC_MATRIX     DOUBLE    Array[6601, 57]


#+++++++++++++++++++++++++++++++++++
# SAMPLE PROGRAM CALLS
#-----------------------------------

IDL> tresp = aia_get_response(/temp, /dn)
IDL> print, aia_bp_make_counts_dem(tresp.a193, /qs)
IDL> effarea = aia_get_response(/area, /dn)
IDL> print, aia_bp_make_counts_spec(effarea.a193, /qs)


#+++++++++++++++++++++++++++++++++++
# DATA FILES
#-----------------------------------

_wave.txt, wave.dat	#	ASCII file listing wavelengths (in Angstroms); can be
					#	used as the grid for an instrument or channel response

_filt.txt			#	Makefile for a filter component (PBGEN_MAKE_FILTER)

_layer.dat			#	ASCII file containing a material layer's cross-section

_comp.txt, comp.dat	#	ASCII file containing a component's efficiency

_chan.txt			# 	Makefile for the channel response (PBGEN_READ_CHAN)
_chan.dat			#	ASCII data file containing channel response
_chan.genx			#	IDL file containing simple channel response structure
_full_chan.genx		#	IDL file containing detailed channel response structure,
					#	including the breakdown of components

_inst.txt			#	Makefile for the instrument response (PBGEN_READ_INST)
_inst.dat			#	ASCII data containing instrument response
_inst.genx			#	IDL file containing simple instrument response structure
_fullinst.genx		#	IDL file containing detailed instrument response structure,
					#	including the breakdown of components 


_emiss.txt			#	Makefile for PBGEN_READ_EMISS
_emiss.dat			#	ASCII data file containing emissivity vs. T and lambda
_emiss.genx			#	IDL file containing the total emissivity structure
_fullemiss.genx		#	IDL file containing the detailed emissivity structure, 
					#	including the breakdown of lines/continuum, etc.

_dem.txt, dem.dat	#	ASCII data on the DEM

_spec.txt			#	Makefile for the spectrum
_spec.dat			#	ASCII data file for the spectrum
_spec.genx			#	IDL file containing the total spectrum
_fullspec.genx		#	IDL file containing the full spectrum, 
					#	including the 2D breakdown vs wavelength and temperature


_tresp.txt			#	Makefile for an instrument temperature response
_tresp.dat			#	ASCII file with temperature response data
_tresp.genx			#	IDL file containing the simple instrument T response
_fulltresp.genx		#	IDL file containing the full instrument T response 
					#	including the 2D breakdown vs wavelength and temperature


#+++++++++++++++++++++++++++++++++++
# PROGRAM FILES
#-----------------------------------

make_filter_materials	#	One-time use procedure to make layer.dat files out of CXRO or IMD files

aia_bp_make_filter		#	Turns a filt.txt file into a comp.dat file for that filter (one-time use)

aia_bp_read_wave		#	Reads in a wave.txt file (which describes the wavelength grid over
						#	which a spectrum or instrument response is to be calculated) and 
						#	returns the contents as a string array

aia_bp_read_err			#	Reads in an err.txt file (which describes a variety of calibration
						#	errors for the instrument wavelength response) and returns the contents
						#	as a structure
	aia_bp_add_err		#	Adds pseudorandom errors to a component and return the component
						#	efficiency
	
aia_bp_read_chan		#	Turns a chan.txt file into chan.dat, chan.genx and full_chan.genx files
	aia_bp_read_comp	#	Reads in a comp.dat or comp.txt file (which contains data as wavelength
						#	and efficiency pairs) and returns the contents as a string array; 
						#	doesn't write anything. 
	
aia_bp_read_inst		#	Turns an inst.txt file into inst.dat, inst.genx and fullinst.genx files

aia_bp_read_spec		#	Turns a spec.txt file into spec.dat, spec.genx and fullspec.genx files
	aia_bp_make_spec	# 	Helper for aia_bp_read_spec; shouldn't really be called directly

aia_bp_read_emiss		#	Turns an emiss.txt file into emiss.dat, emiss.genx and fullemiss.genx files
	aia_bp_make_emiss	# 	Helper for aia_bp_read_emiss; shouldn't really be called directly
	aia_bp_ch_continuum	#	Helper for aia_bp_make_emiss; generates CHIANTI continuum
	
aia_bp_read_tresp		#	Turns a tresp.txt file into tresp.dat, tresp.genx and tresp.genx files
	aia_bp_make_tresp	# 	Helper for aia_bp_read_tresp; can be called directly

aia_bp_make_counts_spec	#	Given an instrument (wavelength) response and a spectrum, produces a count rate
aia_bp_make_counts_dem	#	Given a temperautre response and a DEM, produces a count rate
aia_bp_read_dem			#	Reads in a DEM file

aia_bp_parse_filename	#	Helper that breaks a filename into parts (like path, basefile, etc)
aia_bp_file2template	# 	Helper that fills in a template structure using a string array from a text file
aia_bp_date_string		#	Helper that returns the current YYYYMMDD[_HHMMSS]					

#+++++++++++++++++++++++++++++++++++
# COMMON BLOCKS
#-----------------------------------
common aia_bp_inststr, inherit_str


