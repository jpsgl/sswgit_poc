---------------------------------------------------------------------------------
AIA_GET_RESPONSE Version 8 Release Notes
2017-Dec-11
Wei Liu
weiliu ~at~ lmsal ~dot~ com

What's new in this version:

1)	For dates after the loss of EVE/MEGS-A on 2014-May-25, the evenorm and timedepend_date 
	corrections have been updated based on degradation estimate from 
	the Flare Irradiance Spectral Model (FISM, lasp.colorado.edu/lisird/data/fism). 

	This was done with the updated
		$SSW/sdo/aia/idl/response/aia_bp_response_table.pro 
	that generated the latest:
		$SSW/sdo/aia/response/aia_V8_YYYYMMDD_HHMMSS_response_table.txt
	 	$SSW/sdo/aia/response/aia_V8_YYYYMMDD_HHMMSS_response_table.set_keys

	Files with newer timestamps of YYYYMMDD_HHMMSS include incremental improvements within V8
	and are recommended. By default, the latest response table is used by aia_get_response.pro

2)	The rest of the V8 database remains identical to V6, and are actually symbolic links to
	the corresponding V6 files:
	> ls -l $SSW/sdo/aia/response/aia_V8*
		 Nov 30 18:04 aia_V8_all_fullinst.genx -> aia_V6_all_fullinst.genx
		 Nov 30 18:13 aia_V8_chiantifix.genx -> aia_V6_chiantifix.genx
		 Nov 30 18:13 aia_V8_fullemiss.genx -> aia_V6_fullemiss.genx
		 Nov 30 18:05 aia_V8_fuv_fullinst.genx -> aia_V6_fuv_fullinst.genx
		 Nov 30 18:13 aia_V8_uv_fullemiss.genx -> aia_V6_uv_fullemiss.genx
		 Nov 30 18:13 aia_V8_uv_photo_fullemiss.genx -> aia_V6_uv_photo_fullemiss.genx

3)	Two V7 files are included in the SSW distribution, for bookkeeping purposes only,
	and not recommended for actual use:
		 Nov 29 11:56 aia_V7_20171129_195626_response_table.set_keys
		 Nov 29 11:56 aia_V7_20171129_195626_response_table.txt
	They were an intermediate version introduced in 2015-Aug after the loss of EVE/MEGS-A,
	in an effort to account for AIA degradation. 

	In V7 and the last V6 update (aia_V6_20141027_230030_response_table.txt), for dates after 2014-May-25,
 	the degradation curve remains flat for short wavelength EUV channels (94, 131, 171, 193, 211);
	while for long wavelength channels (304, 355), TIMED/SEE data was used to offer
	reasonably good estimates of degradation.

	In V8, all EUV channels (including 304 & 335) were switched to FISM model for degradation estimates
	after 2014-May-25.

	For UV channels (1600, 1700), it remains the same, as in V6, to use TIMED/SEE for trending degradation.


4)	Details of implementation of update #1):

a)	FISM model provides daily average solar irradiance spectra at coarse resolution 
	of 1 Angstrom (instead of 0.1 Angstrom from EVE/MEGS-A), which are used as a proxy 
	of the EVE/MEGS-A data and are convolved with the AIA bandpass to generate a predicted AIA count rate.

b)	For each day, AIA full-image DATAMEAN values at 10-minute cadence (a higher cadence gives <1% difference) 
	from 00 to 24 UT are averaged to give a corresponding daily average. This is then divided by the 
	FISM-predicted count rate to give an AIA/FISM ratio that tracks AIA degradation.

	Note: in previous versions with AIA/EVE ratio, the time window for obtaining the corresponding AIA data is 
	[-100, +120] seconds around the time of the EVE data point.

c)	For dates overlapping with available EVE/MEGS-A data and thus AIA/EVE ratio, i.e., 2010-May-16 to 2014-May-25,
	AIA/FISM and AIA/EVE ratios show good linear correlations, especially for 94, 131, 171, 304, and 335 channels.
	Using this correlation, the AIA/FISM ratio after 2014-May-25 is linearly rescaled to approximate
	the AIA/EVE ratio to provide a proxy for tracking degradation.

d)	The degradation curve is then fitted with a piecewise linear function to provide time-dependent
	effective area (EA) saved to aia_V8_YYYYMMDD_HHMMSS_response_table.txt. 
	EA remains the same as in V6 for dates before 2014-May-25.
	Because of the switch from real EVE/MEGS-A data to FISM-model synthetic data on 2014-May-25,
	this unavoidably introduces a discontinuity (jump usually < 5%) on this date in the EA correction.


As before, if you wish to access an older version of the instrument
calibration or temperature response, you can set the VERSION, EMVERSION and 
RESPVERSION keywords in AIA_GET_RESPONSE


---------------------------------------------------------------------------------
AIA_GET_RESPONSE Version 6 Release Notes
2014-May-8
Paul Boerner
boerner ~at~ lmsal ~dot~ com

What's new in this version:
 *	The temperature response functions have been updated to use CHIANTI v7.1.3
 	(version 4 used CHIANTI v7.1)
 * 	The effective area (wavelength response) functions have not changed.
 *	The evenorm and timedepend_date corrections have been updated based on 
 	EVE v4 calibration updates (previous AIA versions relied on EVE v2).
 *	The "chiantifix" was adjusted to account for the updated EVE calibration
 	
As before, if you wish to access an older version of the instrument
calibration or temperature response, you can set the VERSION, EMVERSION and 
RESPVERSION keywords in AIA_GET_RESPONSE


---------------------------------------------------------------------------------


AIA_GET_RESPONSE Version 4 Release Notes
2013-February-19
Paul Boerner
boerner ~at~ lmsal ~dot~ com

What's new in this version:
 *	The temperature response functions have been updated to use CHIANTI v7.1
 	(version 2 and 3 used CHIANTI v7.0)
 * 	The effective area (wavelength response) functions for the EUV channels
 	have been extended to cover the range from 25 to 900 A, based on 
 	Soufli et al., Proc SPIE Sept 2012 (doi:10.1117/12.927274)
 	(Previously the EUV channel effective area stopped at 425 A).
 *	The temperature response functions were updated to reflect the extension
 	of the effective area definition
 *	The "chiantifix" was adjusted to account for the move to CHIANTI 7.1
 *	Some bugs in AIA_GET_RESPONSE and the underlying routines were fixed
 	(these bugs were the type to cause crashes, rather than bad results)
 	
As before, if you wish to access an older version of the instrument
calibration or temperature response, you can set the VERSION, EMVERSION and 
RESPVERSION keywords in AIA_GET_RESPONSE


---------------------------------------------------------------------------------


AIA_GET_RESPONSE Version 3 Release Notes
2012-September-18
Paul Boerner
boerner ~at~ lmsal ~dot~ com

What's new in this version:
 * 	The data underlying the effective area (wavelength response) have not changed
 * 	An error was introduced to the continuum calculation used for the emissivity
 	in V2; this was fixed for V3.
 *	The time-dependent corrections introduced in V2 were updated for V3.
 
Note that the time-dependent corrections are themselves now time-dependent. That
is, for images taken recently, the current response is estimated by extrapolating
recent trends in the effective area to the present date. This estimate may
change when better cross-calibration measurements spanning the date in question
become available.

If you wish to access an older version of the degradation correction, set the 
RESPVERSION keyword to the date of the desired version (the date can be found in 
the .corrections.time.version_date field of a response structure returned by
AIA_GET_RESPONSE)


---------------------------------------------------------------------------------


AIA_GET_RESPONSE Version 2 Release Notes
2012-February-06
Paul Boerner
boerner ~at~ lmsal ~dot~ com

What's new in this version:
 * 	The data underlying the effective area (wavelength response) have not changed
 * 	The data underlying the emissivity (CHIANTI model) has been updated to use CHIANTI v7.0,
 	to cover a broader wavelength range, and to fix a bug in the calculation of the 
 	continuum emissivity
 * 	The temperature response functions are calculated from the effective area and the 
 	emissivity,	so they will reflect minor changes because of the revised emissivity
 * 	It is now required to specify either /DN or /PHOT to explicitly include or exclude the 
 	DN-per-photon correction
 * 	Several new keywords have been added to apply corrections to the response function 
 	(EVENORM, TIMEDEPEND_DATE, CHIANTIFIX)
 * 	There is improved self-documentation in the structures returned by this routine

See Also:
 *	An overview of the structure format returned by this routine is available at
	http://sohowww.nascom.nasa.gov/solarsoft/sdo/aia/response/README.txt
 *	More detailed documentation on the empirical corrections to the 94 and 131 A channels
 	is included at http://sohowww.nascom.nasa.gov/solarsoft/sdo/aia/response/chiantifix_notes.txt
 *	The header of the routine itself has more documentation on how each keyword is used
 

---------------------------------------------------------------------------------


Brief Overview of how to run AIA_GET_RESPONSE:

 
To return the effective area (wavelength response) function:

aiaresp = AIA_GET_RESPONSE(/dn)

Note that you should specify whether you want to include the DN/photon conversion by setting
either then /dn or /phot keyword. If you don't set either of those, the routine will ask what
you want to do. The default is to include the DN/photon conversion; prior to this version 2 
release, the default was not to include the conversion (and the routine did not ask if nothing
was specified).

There are a number of options that can be specified:

/uv	 	:	Return response for the 1600/1700/4500 channels instead of the EUV channels
/all 	:	Include functions for both thin and thick focal plane filters
/full	:	Include full detail on the components (mirrors, filters, etc.). Note that the 
			resulting structure is formatted differently than the simple effective area structure
/noblend:	Don't include potential crosstalk between 2 channels on the same telescope 
			(131/335, 94/304)
version=:	Currently only 2 versions are available; 1 is the preflight effective area, and 2 
			is the current release. There isn't any difference in the wavelength response 
			functions.

There are also some optional corrections to the effective area that can be applied by 
specifying keywords:
/evenorm	:	Normalize the response to agree with observations from SDO/EVE on 1 May 2010
/timedepend_date :	apply a time-dependent correction to account for on-orbit degradation
					(can be set as a switch, in which case the current date is used; or as
					a string specifying the time for which the correction is desired)

Note that the time-dependent correction is calculated by trending AIA data vs. EVE. It generally 
makes sense to include /evenorm when you are looking at time dependence, so if you specify 
/timedepend_date but not /evenorm, the routine will check to make sure that's what you want to do
(unless you also explicitly set evenorm=0).

These corrections are included in a substructure of the effective area function:
aiaresp = AIA_GET_RESPONSE(/dn, /evenorm, /time)
help, aiaresp
help, aiaresp.corrections
aiaresp = AIA_GET_RESPONSE(/dn, /evenorm, /time, /full)
help, aiaresp

Note that the CORRECTIONS substructure is included and populated whether or not you request the
corrections to be applied to the effective area.

Help on the wavelength response structure:
 *	Version is a new field; it is an integer (currently either 1 or 2)
 *	Date is the date on which the response function was retrieved
 *	The effective area for each channel can be accessed from either the .all array at the top of 
	the response structure, or from the substructure corresponding to each channel. For example, 
	the 94 A effective area can be read from aiaresp.all[0,*] or aiaresp.a94.ea
 *	The full response structure also includes a filedate field which specifies the date on which 
	the underlying data files were last updated. This should be 20100804_232510 for both version 1
	and version 2 of the effective area
 *	The full response structure has elecperev and elecperdn fields in each of the wavelength
	substructures (e.g. aiaresp.a94_full.elecperev). These are used to calculate the photon-to-DN
	conversion when the /dn keyword is set
 *	The wavelength response can be used, along with a spectrum, to predict an instrument count 
	rate using AIA_BP_MAKE_COUNTS_SPEC


To return the emissivity (CHIANTI) function:

emiss = AIA_GET_RESPONSE(/emiss)

The following options are available:
/full		:	Include exhaustive detail on the CHIANTI calculation, including a line list 
				(array of structures describing each emission line)
version=	:	Currently there are only 2 emissivity versions available; 1 is the preflight
				emissivity, and 2 is the current release. The current release uses a newer 
				version of the CHIANTI database (7.0 vs. 6.0.1), fixes a bug in the calculation
				of the continuum, and covers a broader wavelength range.

Help on the emissivity structure:
 *	Version is a new field; it is an integer (currently either 1 or 2)
 *	Note that documentation on how CHIANTI was run (ionization balance, abundance, pressure,
 	etc) is included in the CH_INFO substructure (or the GENERAL substructure if the full model
 	was run)
 *	The emissivity can be turned into a model spectrum using AIA_BP_EMISS2SPEC


To return the temperature response function:

aiatresp = AIA_GET_RESPONSE(/dn, /temp)

The temperature response function is generated by first retrieving the wavelength response 
(effective area), and then folding it with the spectral model (emissivity). Note that many of
the keywords and notes listed above for the effective area apply for the temperature response
as well:
	/all, /noblend, /evenorm, /timedepend_date
These keywords are applied to the effective area before it is used to generate the temperature
response function

The following keywords work slightly differently:
/uv		:	the temperature response of the UV channels is not calculated by this routine
/full	:	As with the wavelength response, this option returns full detail on the temperature
			response. However, it does not include the component calibration information; 
			instead, it includes the matrix of wavelength and temperature that is flattened to 
			produce the temperature response
version=:	Applies to both the wavelength response and the emissivity model. If you wish to
			use different versions for the wavelength response, set version to the wavelength 
			response version that you want to use, and set emversion to the version of the 
			emissivity that you want.
emversion=	: Version of the emissivity to use (only specify if you want a different version of 
			emissivity and wavelength response)

And there is an additional correction that can be applied to the temperature response function:
/chiantifix	:	If set, an empirical correction is applied to the temperature response of the
				94 and 131 channels in order to try to account for emission that is known
				to be missing in CHIANTI. The details of the correction are included
				in the CORRECTIONS sub-structure
				
Help on the temperature response structure:				
 *	The temperature response can be used, along with a DEM, to predict an instrument count 
	rate using AIA_BP_MAKE_COUNTS_DEM
 *	Note that documentation on how CHIANTI was run (ionization balance, abundance, pressure,
 	etc) is included in the EMISSINFO substructure
