# $Id: README_V1.txt,v 1.1 2012/01/31 00:37:52 boerner Exp $
#
# Description of the types of files used in the bandpass modeling routines
#

#+++++++++++++++++++++++++++++++++++
# STRUCTURE VARIABLES
#-----------------------------------

EFFECTIVE AREA:

IDL> help, aia_get_response(/area, /dn), /str
** Structure <304f608>, 14 tags, length=352536, data length=352504, refs=1:
   NAME            STRING    'AIA'
   DATE            STRING    '20100715_174803'
   CHANNELS        STRING    Array[7]
   WAVE            FLOAT     Array[4001]
   ALL             FLOAT     Array[7, 4001]
   PLATESCALE      FLOAT       8.46158e-12
   UNITS           STRING    'cm^2 DN phot^-1'
   A94             STRUCT    -> <Anonymous> Array[1]
   A131            STRUCT    -> <Anonymous> Array[1]
   A171            STRUCT    -> <Anonymous> Array[1]
   A193            STRUCT    -> <Anonymous> Array[1]
   A211            STRUCT    -> <Anonymous> Array[1]
   A304            STRUCT    -> <Anonymous> Array[1]
   A335            STRUCT    -> <Anonymous> Array[1]

IDL> help, aia_get_response(/area, /dn, /full), /str                           
** Structure <38b5608>, 21 tags, length=1121440, data length=1121384, refs=1:
   FILENAME        STRING    'aia_preflight_all_inst'
   DATE            STRING    '20100715_174803'
   CODENAME        STRING    'AIA_BP_READ_INST.PRO'
   CODEVERSION     STRING    '$Id: README_V1.txt,v 1.1 2012/01/31 00:37:52 boerner Exp $'
   NAME            STRING    'AIA'
   CHANNELS        STRING    Array[7]
   A94             STRUCT    -> <Anonymous> Array[1]
   A94_FULL        STRUCT    -> <Anonymous> Array[1]
   A131            STRUCT    -> <Anonymous> Array[1]
   A131_FULL       STRUCT    -> <Anonymous> Array[1]
   A171            STRUCT    -> <Anonymous> Array[1]
   A171_FULL       STRUCT    -> <Anonymous> Array[1]
   A193            STRUCT    -> <Anonymous> Array[1]
   A193_FULL       STRUCT    -> <Anonymous> Array[1]
   A211            STRUCT    -> <Anonymous> Array[1]
   A211_FULL       STRUCT    -> <Anonymous> Array[1]
   A304            STRUCT    -> <Anonymous> Array[1]
   A304_FULL       STRUCT    -> <Anonymous> Array[1]
   A335            STRUCT    -> <Anonymous> Array[1]
   A335_FULL       STRUCT    -> <Anonymous> Array[1]
   NOTES           STRING    Array[1]

IDL> help, (aia_get_response(/area, /dn [, /full])).a94, /str
** Structure <14a1208>, 5 tags, length=32048, data length=32044, refs=1:
   NAME            STRING    'A94'
   WAVE            FLOAT     Array[4001]
   EA              FLOAT     Array[4001]
   PLATESCALE      FLOAT       8.46158e-12
   UNITS           STRING    'cm^2 DN phot^-1'

IDL> help, (aia_get_response(/area, /dn [, /full])).a94_full, /str
** Structure <1a1fe08>, 27 tags, length=144136, data length=144130, refs=2:
   WAVE            FLOAT     Array[4001]
   EFFAREA         FLOAT     Array[4001]
   UNITS           STRING    'cm^2 DN phot^-1'
   DATE            STRING    '20100804_232512'
   NAME            STRING    'A335'
   GEOAREA         FLOAT           83.0000
   SCALE           FLOAT           1.00000
   PLATESCALE      FLOAT       8.46158e-12
   WAVEMIN         FLOAT           25.0000
   WAVESTEP        FLOAT          0.100000
   WAVENUMSTEPS    LONG              4001
   FILTERSINCLUDEMESH
                   INT              1
   USECONTAM       INT              1
   CONTAMTHICK     INT            275
   USEPHOTTOELEC   INT              1
   ELECPEREV       FLOAT          0.273973
   USEPHOTTODN     INT              1
   ELECPERDN       FLOAT           17.6000
   USEERROR        INT              0
   EXTRAPOLATECOMPONENTS
                   INT              1
   ENT_FILTER      FLOAT     Array[4001]
   FP_FILTER       FLOAT     Array[4001]
   PRIMARY         FLOAT     Array[4001]
   SECONDARY       FLOAT     Array[4001]
   CCD             FLOAT     Array[4001]                                   
   CONTAM          FLOAT     Array[4001]
   CROSS_AREA      FLOAT     Array[4001]
   
Note: 
	Here is how EFFAREA is calculated:
	if USEPHOTTOELEC and USEPHOTTODN then UNITS = 12398./WAVE * ELECPEREV / ELECPERDN else UNITS = 1
	EFFAREA = ((GEOAREA * ENT_FILTER * FP_FILTER * PRIMARY * SECONDARY * CCD * CONTAM) + CROSS_AREA) * UNITS
	
	
TEMPERATURE RESPONSE:

IDL> help, aia_get_response(/temp, /dn), /str
** Structure <4150a08>, 12 tags, length=14944, data length=14912, refs=1:
   NAME            STRING    'AIA'
   LOGTE           FLOAT     Array[101]
   CHANNELS        STRING    Array[7]
   UNITS           STRING    'DN cm^5 s^-1 pix^-1'
   ALL             DOUBLE    Array[101, 7]
   A94             STRUCT    -> <Anonymous> Array[1]
   A131            STRUCT    -> <Anonymous> Array[1]
   A171            STRUCT    -> <Anonymous> Array[1]
   A193            STRUCT    -> <Anonymous> Array[1]
   A211            STRUCT    -> <Anonymous> Array[1]
   A304            STRUCT    -> <Anonymous> Array[1]
   A335            STRUCT    -> <Anonymous> Array[1]

IDL> help, aia_get_response(/temp, /dn, /full), /str
** Structure <4156208>, 16 tags, length=28371576, data length=28371544, refs=1:
   NAME            STRING    'AIA'
   DATE            STRING    '20100730_205612'
   LOGTE           FLOAT     Array[101]
   WAVE            FLOAT     Array[5010]
   TUNITS          STRING    'DN cm^5 s^-1 pix^-1'
   TWUNITS         STRING    'DN cm^5 s^-1 A^-1 pix^-1'
   CHANNELS        STRING    Array[7]
   TRESP           DOUBLE    Array[101, 7]
   TWRESP          DOUBLE    Array[5010, 101, 7]
   A94             STRUCT    -> <Anonymous> Array[1]
   A131            STRUCT    -> <Anonymous> Array[1]
   A171            STRUCT    -> <Anonymous> Array[1]
   A193            STRUCT    -> <Anonymous> Array[1]
   A211            STRUCT    -> <Anonymous> Array[1]
   A304            STRUCT    -> <Anonymous> Array[1]
   A335            STRUCT    -> <Anonymous> Array[1]

IDL> help, (aia_get_response(/temp, /dn [, /full])).a94, /str
** Structure <2607248>, 4 tags, length=1248, data length=1244, refs=2:
   NAME            STRING    'A94'
   UNITS           STRING    'DN cm^5 s^-1 pix^-1'
   LOGTE           FLOAT     Array[101]
   TRESP           DOUBLE    Array[101]


EMISSIVITY:

IDL> help, aia_get_response(/emiss), /str     
** Structure MS_019083287008, 5 tags, length=4068560, data length=4068556:
   WAVE            FLOAT     Array[5010]
   LOGTE           FLOAT     Array[101]
   EMISSIVITY      DOUBLE    Array[5010, 101]
   UNITS           STRING    'photons cm3 s-1 sr-1 A-1'
   DATE            STRING    '20100729_173028'

IDL> help, aia_get_response(/emiss, /full), /str
** Structure MS_019083287009, 7 tags, length=207564320, data length=205277280:
   DATE            STRING    '20100729_173028'
   GENERAL         STRUCT    -> MS_019083287005 Array[1]
   LINES           STRUCT    -> MS_019083287006 Array[228702]
   CONT            STRUCT    -> MS_019083287007 Array[1]
   FIXES           STRING    Array[3]
   TOTAL           STRUCT    -> MS_019083287008 Array[1]
   NOTES           STRING    Array[1]

IDL> help, (aia_get_response(/emiss, /full)).lines, /str
** Structure MS_019083287006, 11 tags, length=888, data length=878:
   IZ              INT              2
   ION             INT              2
   IDENT           STRING    '1s 2S1/2 - 2p 2P1/2'
   IDENT_LATEX     STRING    '1s $^2$S$_{1/2}$ - 2p $^2$P$_{1/2}$'
   SNOTE           STRING    'He II'
   LVL1            INT              1
   LVL2            INT              3
   TMAX            FLOAT           4.90000
   WVL             DOUBLE           303.78601
   FLAG            INT              0
   GOFT            DOUBLE    Array[101]


SPECTRUM:

IDL> help, aia_bp_default_spectrum(/ar), /str
** Structure <145d8d8>, 4 tags, length=60152, data length=60152, refs=1:
   DATE            STRING    '20100810_225229'
   WAVE            FLOAT     Array[5010]
   SPEC            DOUBLE    Array[5010]
   UNITS           STRING    'photons cm-2 s-1 sr-1 A-1'
   
IDL> help, aia_bp_default_spectrum(/ar, /full), /str
** Structure <146f838>, 8 tags, length=2344976, data length=2344972, refs=1:
   DATE            STRING    '20100810_225239'
   DEMNAME         STRING    'active_region.dem'
   EMISSDATE       STRING    '20100729_173028'
   WAVE            FLOAT     Array[5010]
   SPEC            DOUBLE    Array[5010]
   UNITS           STRING    'photons cm-2 s-1 sr-1 A-1'
   LOGTE           FLOAT     Array[57]
   SPEC_MATRIX     DOUBLE    Array[5010, 57]


#+++++++++++++++++++++++++++++++++++
# SAMPLE PROGRAM CALLS
#-----------------------------------

IDL> tresp = aia_get_response(/temp, /dn)
IDL> print, aia_bp_make_counts_dem(tresp.a193, /qs)
IDL> effarea = aia_get_response(/area, /dn)
IDL> print, aia_bp_make_counts_spec(effarea.a193, /qs)


#+++++++++++++++++++++++++++++++++++
# DATA FILES
#-----------------------------------

_wave.txt, wave.dat	#	ASCII file listing wavelengths (in Angstroms); can be
					#	used as the grid for an instrument or channel response

_filt.txt			#	Makefile for a filter component (PBGEN_MAKE_FILTER)

_layer.dat			#	ASCII file containing a material layer's cross-section

_comp.txt, comp.dat	#	ASCII file containing a component's efficiency

_chan.txt			# 	Makefile for the channel response (PBGEN_READ_CHAN)
_chan.dat			#	ASCII data file containing channel response
_chan.genx			#	IDL file containing simple channel response structure
_full_chan.genx		#	IDL file containing detailed channel response structure,
					#	including the breakdown of components

_inst.txt			#	Makefile for the instrument response (PBGEN_READ_INST)
_inst.dat			#	ASCII data containing instrument response
_inst.genx			#	IDL file containing simple instrument response structure
_fullinst.genx		#	IDL file containing detailed instrument response structure,
					#	including the breakdown of components 


_emiss.txt			#	Makefile for PBGEN_READ_EMISS
_emiss.dat			#	ASCII data file containing emissivity vs. T and lambda
_emiss.genx			#	IDL file containing the total emissivity structure
_fullemiss.genx		#	IDL file containing the detailed emissivity structure, 
					#	including the breakdown of lines/continuum, etc.

_dem.txt, dem.dat	#	ASCII data on the DEM

_spec.txt			#	Makefile for the spectrum
_spec.dat			#	ASCII data file for the spectrum
_spec.genx			#	IDL file containing the total spectrum
_fullspec.genx		#	IDL file containing the full spectrum, 
					#	including the 2D breakdown vs wavelength and temperature


_tresp.txt			#	Makefile for an instrument temperature response
_tresp.dat			#	ASCII file with temperature response data
_tresp.genx			#	IDL file containing the simple instrument T response
_fulltresp.genx		#	IDL file containing the full instrument T response 
					#	including the 2D breakdown vs wavelength and temperature


#+++++++++++++++++++++++++++++++++++
# PROGRAM FILES
#-----------------------------------

make_filter_materials	#	One-time use procedure to make layer.dat files out of CXRO or IMD files

aia_bp_make_filter		#	Turns a filt.txt file into a comp.dat file for that filter (one-time use)

aia_bp_read_wave		#	Reads in a wave.txt file (which describes the wavelength grid over
						#	which a spectrum or instrument response is to be calculated) and 
						#	returns the contents as a string array

aia_bp_read_err			#	Reads in an err.txt file (which describes a variety of calibration
						#	errors for the instrument wavelength response) and returns the contents
						#	as a structure
	aia_bp_add_err		#	Adds pseudorandom errors to a component and return the component
						#	efficiency
	
aia_bp_read_chan		#	Turns a chan.txt file into chan.dat, chan.genx and full_chan.genx files
	aia_bp_read_comp	#	Reads in a comp.dat or comp.txt file (which contains data as wavelength
						#	and efficiency pairs) and returns the contents as a string array; 
						#	doesn't write anything. 
	
aia_bp_read_inst		#	Turns an inst.txt file into inst.dat, inst.genx and fullinst.genx files

aia_bp_read_spec		#	Turns a spec.txt file into spec.dat, spec.genx and fullspec.genx files
	aia_bp_make_spec	# 	Helper for aia_bp_read_spec; shouldn't really be called directly

aia_bp_read_emiss		#	Turns an emiss.txt file into emiss.dat, emiss.genx and fullemiss.genx files
	aia_bp_make_emiss	# 	Helper for aia_bp_read_emiss; shouldn't really be called directly
	aia_bp_ch_continuum	#	Helper for aia_bp_make_emiss; generates CHIANTI continuum
	
aia_bp_read_tresp		#	Turns a tresp.txt file into tresp.dat, tresp.genx and tresp.genx files
	aia_bp_make_tresp	# 	Helper for aia_bp_read_tresp; can be called directly

aia_bp_make_counts_spec	#	Given an instrument (wavelength) response and a spectrum, produces a count rate
aia_bp_make_counts_dem	#	Given a temperautre response and a DEM, produces a count rate
aia_bp_read_dem			#	Reads in a DEM file

aia_bp_parse_filename	#	Helper that breaks a filename into parts (like path, basefile, etc)
aia_bp_file2template	# 	Helper that fills in a template structure using a string array from a text file
aia_bp_date_string		#	Helper that returns the current YYYYMMDD[_HHMMSS]					

#+++++++++++++++++++++++++++++++++++
# COMMON BLOCKS
#-----------------------------------
common aia_bp_inststr, inherit_str


