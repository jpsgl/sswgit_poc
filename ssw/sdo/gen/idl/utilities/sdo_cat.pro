pro sdo_cat,t0,t1,cat,files, search_array=search_array, $
   count=count, tcount=tcount, aia=aia, hmi=hmi, level=level, $
   drms=drms, _extra=_extra, $
   synoptic=synoptic, ds=ds, series=series, parent=parent, check_files=check_files
;+
;   Name: sdo_cat
;
;   Purpose: read aia/hmi catalog -or- DRMS for user Trange, optionally SEARCH
;
;   Input Parameters:
;      t0,t1 - time range of interest
; 
;   Output Parameters:
;      cat - catalog records  - or DRMS records ala ssw_jsoc_time2data
;      files - associated filenames
; 
;   Keyword Parameters:
;      search_array - optional search param(s) per where_array.pro
;      tcount - (output) - number of records in time range
;      count - (output) - number of records returned (=tcount if no SEARCH_ARR)
;      aia/hmi - desired sdo instrument (def=aia)
;      level - desired data level (def=1)
;      _extra - inherit -> ssw_jsoc_time2data
;      ds -or- series (synonyms) - explicit jsoc series to try (implies /DRMS)
;      key - optional comma delimited list of drms params / tags to return
;             Default: key='date__obs,t_obs,fsn,wavelnth,wave_str,quality,datamean,img_type,missvals,exptime,date-obs'
;      check_files - if DRMS, then only return subset of cat+files which match local files
;                    (default returns deterministic file names which may or may not exist locally)
;
;   History:
;      Circa April 2010 - S.L.Freeland 
;      12-oct-2010 - S.L.Freeland - add /DRMS switch -> ssw_jsoc_time2data
;      28-jan-2011 - S.L.Freeland - dusted off, beta->online /DRMS option
;
;   Method: local catalog -or- DRMS 
;
;   Restrictions:
;      Pending publication, requires jsoc2 priviledge
;      Remote users without jsoc2 priviledge may use /SYNOPTIC switch
;      (lower cadence, but universally available)
;     
;-
hmi=keyword_set(hmi)
inst=(['aia','hmi'])(hmi)

if n_elements(level) eq 0 then level=(['1','1.5'])(ssw_deltat(t0,ref='00:00 15-jun-2010') gt 0)
level=([level,'1.5'])(ssw_deltat(t0,ref='15-may-2010') lt 0)

slev=strmid(strtrim(level,2),0,3)
top=concat_dir('$SSWDB',concat_dir(concat_dir('sdo',inst),'genxcat_l'+slev))

drms=keyword_set(drms)
if not file_exist(top) and ~drms then begin 
   box_message,'No catalog (yet) for inst: ' + inst + ' ,data level='+slev
   box_message,'Will try drms...'
   drms=1
endif

case n_params() of 
   0: begin
         box_message,'Need time or time range
         return
   endcase
   1: begin 
         time0=anytim(t0,/ecs)
         time1=reltime(time0,/hours,out='ecs') 
   endcase
   else: begin 
      time0=anytim(t0,/ecs)
      time1=anytim(t1,/ecs)
   endcase
endcase

tcount=0
init=keyword_set(init)

if drms then begin 
   ; select data series
   nrt=(['','_nrt'])(ssw_deltat(reltime(days=-2),time0) gt 0)
   slevel=str_replace(strmid(strtrim(level,2),0,3),'.','p')
   case 1 of
      level  eq 1 and n_elements(ds) eq 0: ds='aia.lev1'   ; Finally, released and published
      data_chk(series,/string): ds=series ; user verbatim
      data_chk(ds,/string) : ; ditto (synonym)
      keyword_set(synoptic): ds='aia_test.synoptic2' 
      else: begin
         ; ds=inst+'.lev'+ slevel + nrt
         jsoc2=1 
         ds='aia_test.lev1p5' ; TODO - remove jsoc2 two restriction post-publication
      endcase
   endcase
   case 1 of 
      data_chk(key,/string):  ; user supplied drms key/param list
      keyword_set(all_keys): 
      else: key='date__obs,t_obs,wave_str,wavelnth,quality,datamean,instrume,img_type,missvals,exptime,date-obs,fsn'
   endcase
   final=(['','_final'])(slevel eq '1p5') 
   ;jsoc2=strpos(ds,'_nrt') ne -1 and final ne '' 
   box_message,'Data Series> ' + ds
   tcount=0
   delvarx,cat
   ssw_jsoc_time2data,time0,time1,cat,ds=ds,_extra=_extra,key=key, $
      jsoc2=jsoc2
   if data_chk(cat,/struct) then begin
      if n_elements(parent) eq 0 then $
         parent=(['/archive/sdo/AIA/lev1','/cache/sdo/AIA/lev1p5'])(strpos(ds,'1p5') ne -1)
      files=ssw_jsoc_index2filenames(cat,parent=parent,/t_obs,/chron) ; determistic file names
      cat=add_tag(cat,files,'filename')
      tcount=n_elements(cat)
      if keyword_set(check_files) then begin 
         box_message,'only checking first/last files - use file_exist.pro if required'
         if total(file_exist([files(0),last_nelem(files)])) ne 2 then $
            box_message,'Warning: not all files exist on local machine' else box_message,'files exist'
      endif
   endif else begin 
      box_message,'No drms records match time+series..'
   endelse
endif else begin 
   read_genxcat,time0,time1,cat,count=tcount, topdir=top, init=1
endelse
count=tcount ; so far, these are the same

if n_elements(search_array) gt 0 and tcount gt 0 then begin 
   ss=struct_where(cat,count,search_array=search_array,/fold_case)
   if count gt 0 then cat=cat(ss) else begin
      cat=-1
      box_message,'records in your time, but none maching SEARCH_ARRAY
   endelse 
endif

if count gt 0 then files=cat.filename

return
end






