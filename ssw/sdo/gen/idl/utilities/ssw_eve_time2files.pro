function ssw_eve_time2files, time0, time1, lev1=lev1, lev2=lev2, level=level, $
   parent_url=parent_url, lines=lines, spectra=spectra, $
   parent_local=parent_local, confirm=confirm, parent_out=parent_out, $
   get_urls=get_urls, local_files=local_files, version=version, silent=silent
;+
;   Name: ssw_time2evefiles
;
;   Purpose: ssw time range -> eve files (urls@lasp)
;
;   Input Parameters:
;      time0, time1 - desired time range

;   Keyword Parameters:
;      lev1/lev2 switches to select desired processing level ; def level2(ok?)
;      level - alternate LEVEL=<lev#>
;      parent_url - url of data@eve server - defult=http://lasp.colorado.edu/eve/data_access/evewebdataproducts/
;      lines - (switch) - return EVE linds/bands product EVL...
;      spectra - (switch) - return EVE spectra products  EVS... default (ok?)
;      parent_path - optional name of local nfs (return filenames not urls)
;                   (analogous level to PARENT_URL)
;      confirm - if set, actually check server for file(s)
;                default returns "ideal" url or path list for time range
;      count=count - number of urls/filenames returned (post-confirm)
;      get_urls=get_urls (switch) - if set, and URLS, get them server->local
;               (implies /CONFIRM)
;      parent_out - in conjunction with /GET_URLS, put them here
;                   (default=current)
;      local_files - (output) - local names if /GET_URLS was issued
;      version_number - optional version# - default=3
;		silent	-	if set, don't show progress bar even on X
;
;   Calling Examples:
;      1. return the "ideal" deterministic url list 
;         (default=level2 + spectra)
;      IDL> laspurls=ssw_eve_time2files('12:00 12-jan-2011','6:00 13-jan-2011')
;      IDL> help,laspurls & print,laspurls(0)
; LASPURLS        STRING    = Array[19]
; http://lasp.colorado.edu/eve/data_access/evewebdataproducts/level2/2011/012//EVS_L2_2011012_12_001_01.fit.gz
;      
;      2. same as above with /CONFIRM (verifies url exists on server)
;      IDL> laspurls=ssw_eve_time2files('12:00 12-jan-2011','6:00 13-jan-2011',/confirm)
;| Only 7 out of 19 exist.. returning that subset |
;IDL> help,laspurls
;LASPURLS        STRING    = Array[7]
;      3. select LINES, and get the data  server->local
;      
;      IDL> laspurls=ssw_eve_time2files('12:00 12-jan-2011','6:00 13-jan-2011',/lines,parent_out='evel',local_files=lfiles,/get_urls)
;      IDL> help,laspurls & print,lfiles(0) & print,file_exist(lfiles)
;   History:
;      27-Jan-2011 - S.L.Freeland
;      11-mar-2013 - S.L.Freeland - use sock_head instead of sock_check until further notice
;                    add VERSION keyword and function
;      5-Aug-2013 - P. Boerner	- 	use sock_header instead of sock_head
;					 add SILENT keyword and function
;					make VERSION default to 3
;      7-Jan-2013 - P. Boerner	- 	Back to using sock_head (where did sock_header go?)
;					make VERSION default to 4
;
;   Method: usual ssw suspects
;
if n_elements(parent_url) eq 0 then parent_url = $
   'http://lasp.colorado.edu/eve/data_access/evewebdataproducts/'
;
; select product level 
case 1 of 
   keyword_set(lev1): lev=1
   keyword_set(lev2): lev=2
   keyword_set(lev3): lev=3 
   keyword_set(level): lev=fix(level)
   else: lev=2 ; default to level2
endcase

; select product type
lines=keyword_set(lines)
spectra=keyword_set(spectra) or (1-lines)    ; default

pchar=(['L','S'])(spectra) 

slev='level'+strtrim(lev,2) ; 

sub=(['','/esp/'])(lev eq 1)

dgrid=timegrid(time0,time1,out='ecs',/hour,/quiet)
hgrid=strmid(timegrid(time0,time1,/hour,out='ecs',/time_only,/quiet),0,2)
yyyy=strmid(dgrid,0,4)

doys=anytim2doy(dgrid)
sdoys=string(doys,format='(i3.3)') ; to do early 2011 - verify 3 digit DOY
froots=yyyy+sdoys

if n_elements(version) eq 0 then version=4
sver='_'+string(version,format='(I3.3)')

prefix=(['esp_L1_','EV'+pchar+'_L2_'])(lev eq 2)
suffix=([sver+'.fit',sver+'_01.fit.gz'])(lev ge 2) ; todo? ever pattern change?

case 1 of 
   data_chk(parent_local,/string): top=parent_local
   else: top=parent_url ; default are URLS
endcase

; form ideal file/url vector
rpaths=top+slev+'/'+yyyy+'/'+sdoys+'/'
rfiles=prefix+froots+'_'+hgrid+suffix
full=rpaths+'/'+rfiles ; fully qualified files/urls "ideal"

nret=n_elements(full)

retval=full ; default is all
confirm=keyword_set(confirm) or keyword_set(get_urls)
if confirm then begin ; check and only return existing subset
   if file_exist(parent_local) then ss=where(file_exist(full),ecnt) else begin
      status=intarr(nret)
      for i=0,nret-1 do begin
;         shead=sock_header(full[i],code=code)
         shead=sock_head(full[i],code=code)
         status(i)=code eq 200
      endfor
      ss=where(status,ecnt)
   endelse
   case 1 of
      ecnt eq nret: ; all exist
      ecnt eq 0: begin 
         box_message,'None of the desired files/urls exist
         retval=''
      endcase
      else: begin 
         box_message,'Only '+ strtrim(ecnt,2) + ' out of ' + $
            strtrim(nret,2) + ' exist.. returning that subset'
         retval=retval(ss)
         rfiles=rfiles(ss)
      endcase
   endcase
endif ; end confirm  

count=n_elements(retval) * (retval(0) ne '') 
if keyword_set(silent) or !d.name ne 'X' then progress = 0 else progress = 1

get_urls=keyword_set(get_urls) and count gt 0
if get_urls then begin
   if n_elements(parent_out) eq 0 then parent_out=curdir()
   mk_dir,parent_out ; dodo? allow option to preserve lasp tree structure?
   sock_copy,retval,progress=progress and spectra,out_dir=parent_out
   local_files=concat_dir(parent_out,rfiles)
   ss=where(file_exist(local_files),gcnt)
   case 1 of 
      gcnt eq count: ; all server->client ok
      gcnt eq 0: begin
         box_message,'Problem with all transfers'
         count=0
         retval=''
      endcase
      else: begin
         box_message,'Not all transfers successful...'
         count=gcnt
         local_files=local_files(ss)
      endcase
    endcase
endif

return,retval
end
