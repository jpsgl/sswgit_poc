pro sdo_master_pointing2genx, topdir=topdir 
;
;+
;   Name: sdo_master_pointing2genx
;
;   Purpose: jsoc series master pointing -> genx ssw/sdo distribution
;
;   Input Parameters:
;      NONE 
;
;   Restrictions:
;     Generally only useful on server
;
if n_elements(topdir) eq 0 then $
   topdir=concat_dir('$SSW','sdo/gen/data/pointing')


mpoint=ssw_sdo_master_pointing() ; mission jsoc series->structure
write_genxcat,mpoint,topdir=topdir,prefix='sdo_master_pointing_', $
   /nelements,/geny

return
end
  

