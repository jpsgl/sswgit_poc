;+
; Project     : HESSI
;
; Name        : EIS_SOCK_FIND
;
; Purpose     : Find EIS FITS files on a remote Web server
;
; Category    : utility system sockets
;                   
; Inputs      : FILE = filename to search for
;
; Outputs     : OUT = filename with server path name
;
; Keywords    : ERR = error string
;               COUNT = # of files found [ = 1]
;
; History     : 7-Jan-2002,  D.M. Zarro (EITI/GSFC)  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function eis_sock_find,file,count=count,err=err

err=''
count=0
if n_elements(file) gt 1 then begin
 err='input file must be scalar'
 message,err,/cont
 return,''
endif

if is_blank(file) then return,''
server=synop_server()
if not have_network(server,err=err,/verb) then return,''

subs='cdsfits/arch'+trim2(sindgen(28)+1)
http='http://'+server+'/'

a=obj_new('http',server,err=err)

for i=0,n_elements(subs)-1 do begin
 path=subs[i]+'/'+file
 s=a->file_found(path,/quiet)
 if s then begin
  count=1
  obj_destroy,a
  return,http+path
 endif
endfor

err=file+' not found'
message,err,/cont
obj_destroy,a
return,''
end



