;+
; Project     : EIS
;
; Name        : EIS_DBASE
;
; Purpose     : install EIS Database
;
; Category    : system
;                   
; Inputs      : None
;
; Outputs     : None
;
; Keywords    : USER = set to user defined ZDBASE
;
; History     : 28-Jan-2003,  D.M. Zarro (EER/GSFC)  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro eis_dbase,err=err,verbose=verbose,user=user

err=''
defsysv,'!priv',2
mklog,'ZDBASE_CDS,''

path=''
if keyword_set(user) then begin
 path=chklog('ZDBASE_USER')
 if is_string(path) then path=local_name(path) else begin
  err='ZDBASE_USER is undefined, using SSW DBASE..'
  message,err,/cont
 endelse
endif else mklog,'ZDBASE_USER',''

if is_blank(path) then begin
 path=local_name('$SSW/solarb/eis/database')
 if not is_dir(path) then begin
  err='Local EIS Database not found'
  message,err,/cont
  return
 endif
 path='+'+path
endif

;-- expand directories if preceded by '+'

chk=where(strpos(path,'+') gt -1,count)
if count gt 0 then begin
 dprint,'% EIS_DBASE: Expanding directories...'
 dirs=str2arr(path,delim=':')
 ndirs=n_elements(dirs)
 for i=0,ndirs-1 do begin
  dbase=dirs[i]
  if strpos(dbase,'+') eq 0 then dbase=expand_path(dbase,/all,/array)
  if exist(zdbase) then zdbase=[zdbase,dbase] else zdbase=dbase
 endfor
 zdbase=arr2str(zdbase,delim=':')
endif else zdbase=path

setenv,'ZDBASE='+zdbase

if keyword_set(verbose) then message,'Successfully loaded - '+path,/cont
return & end

