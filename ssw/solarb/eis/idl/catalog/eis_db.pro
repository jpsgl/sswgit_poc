;+
; Project     : EIS
;
; Name        : EIS_DB
;
; Purpose     : install EIS Database
;
; Category    : system
;                   
; Inputs      : None
;
; Outputs     : None
;
; Keywords    : None
;
; History     : 28-Jan-2003,  D.M. Zarro (EER/GSFC)  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro eis_db

path=local_name('$SSW/solarb/eis/database')
if not is_dir(path) then begin
 message,'Local EIS Database not found',/cont
 return
endif

if not have_proc('fix_zdbase') then return

setenv,'ZDBASE_USER=+'+path
defsysv,'!priv',2

s=call_function('fix_zdbase',/user)


return & end


