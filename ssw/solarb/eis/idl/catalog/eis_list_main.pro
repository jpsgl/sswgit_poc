;+
; Project     : EIS
;
; Name        : EIS_LIST_MAIN
;
; Purpose     : List entries in EIS main catalog. Checks for local
;               copy of catalog in ZDBASE. If not available, accesses
;               remote copy via socket call.
;
; Category    : utility sockets
;                   
; Inputs      : TSTART, TEND = start/end times for list
;
; Outputs     : OBS = main observation structure
;               COUNT = number of elements found
;
; Keywords    : SEARCH = search string
;               OUT_FILE = output file in which OBS is saved
;               REMOTE = force a remote search
;               FITS = output in FITS file
;               NO_CACHE = don't check cache
;
; History     : 29-Aug-2002,  D.M. Zarro (LAC/GSFC)  Written
;               2-Jan-2005, Zarro (L-3Com/GSFC) - modified to use FITS format
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-
;------------------------------------------------------------------------

;-- save utility for passing DB structures via socket

pro eis_save,obs,save_file,err=err

err=''
if is_string(save_file) then begin
 break_file,save_file,dsk,dir
 outdir=dsk+dir
 if is_blank(outdir) then begin
  outdir=curdir()
  outfile=concat_dir(outdir,out_file)
 endif else outfile=save_file
 if test_dir(outdir,err=err) then begin
  null={null:-1}
  if is_struct(obs) then begin
   pad_tag,obs
   mwrfits,obs,outfile,/create 
  endif else mwrfits,null,outfile,/create
 endif
endif

return & end

;------------------------------------------------------------------------

pro eis_list_main,tstart,tend,obs,count,search=search,err=err,fits=fits,$
           out_file=out_file,verbose=verbose,reset=reset,remote=remote,$
           no_cache=no_cache

common eis_list_main,fifo

err=''
count=0
verbose=keyword_set(verbose)

;-- check for valid time 

if valid_time(tstart)*valid_time(tend) eq 0 then begin
 err='invalid time input'
 pr_syntax,'eis_list_main,tstart,tend,obs,count,[search=search]'
 return
endif

;-- create a cache object for storing results

if not obj_valid(fifo) then fifo=obj_new('fifo')
if keyword_set(reset) then fifo->empty

;-- convert time part of search to string format

t1=trim(str_format(anytim2tai(tstart),'(f15.0)'))
t2=trim(str_format(anytim2tai(tend),'(f15.0)'))
if is_string(search) then str=trim(search) else str=''

;-- check if cached

status=0b
delvarx,obs
id=t1+t2+str
cache=1-keyword_set(no_cache)
if cache then fifo->get,id,obs,status=status

if status then begin
 count=n_elements(obs)
 eis_save,obs,out_file,err=err & return
endif

;-- check if db is available locally

remote=keyword_set(remote)
if not have_proc('list_main') then remote=1b

if not remote then begin
 defsysv,'!priv',2
 unavail=0
 dbopen,'main',unavail=unavail
 if (unavail ne 0) then remote=1b
endif

if (not remote) then begin

 list_main,tstart,tend,obs,count,err=err,search=search

;-- Save results 

 eis_save,obs,out_file,err=err

endif else begin

;-- Run eis_list_main on a remote server via RPC and copy results back.
;   Have to put all command line arguments into strings for this to work.

 ts="'"+trim(tstart)+"'"
 te="'"+trim(tend)+"'"
 rcache=",no_cache='"+trim(keyword_set(no_cache))+"'"
 rset=",reset='"+trim(keyword_set(reset))+"'"

;-- construct output file name on remote server

 get_sid,sid
 ext='.fits'
 remote_dir='/service/soho-arch00/soho/public/data/synoptic'
 temp_dir='/temp'
 temp_name='m'+sid+ext
 temp_file=temp_dir+'/'+temp_name
 remote_file=remote_dir+temp_file
 rfile=",out_file='"+remote_file+"'"
 query="eis_list_main,"+ts+","+te+rcache+rset+rfile
 if is_string(search) then begin
  query=query+",search='"+strcompress(search)+"'"
 endif

;-- send query

 sock_rpc,query,err=err
 if err ne '' then return

;-- now retrieve results from remote server either by directly copying
;   save file or reading FITS file

 shost=synop_server(path=spath,/full)
 query=shost+spath+temp_file

 if keyword_set(fits) then begin
  sock_fits,query,obs,err=err,extension=1
 endif else begin

  ldir=get_temp_dir()
  sock_copy,query,out_dir=ldir,err=err
  chk=loc_file(temp_name,path=ldir,count=fcount)

;-- remove results file from local directory

  if fcount gt 0 then begin
   obs=mrdfits(chk[0],1)
   file_delete,chk[0],/quiet
  endif

 endelse

;-- remove results file from remote location

 sock_rpc,"file_delete,"+"'"+remote_file+"'"+",quiet='1'"

endelse

if is_struct(obs) then begin
 if (1-tag_exist(obs,'null')) then count=n_elements(obs)
endif

;-- cache search results

if cache and (count gt 0) then fifo->set,id,obs

return
end


