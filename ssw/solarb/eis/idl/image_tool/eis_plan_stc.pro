;+
; Project     : SOLAR-B/EIS
;
; Name        : EIS_PLAN_STC
;
; Purpose     : Convert EIS image tool structure to format
;               recognized by eis_mk_plan
;
; Category    : Planning
;
; Syntax      : IDL> stc=eis_plan_stc(eis_stc)
;
; Inputs      : EIS_STC = EIS image tool pointing structure
;
; Outputs     : STC = EIS planning tool pointing structure
;
; Keywords    : None
;       
; History     : 23-Aug-2006, Zarro (ADNET/GSFC) - written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function eis_plan_stc,eis_stc

 if not have_tag(eis_stc,'n_pointings') then begin
  pr_syntax,'stc=eis_plan_stc(eis_stc)'
  if exist(eis_stc) then return, eis_stc else return,-1
 endif

 n_pointings=eis_stc.n_pointings

 stc=eis_point_stc(n_pointings)
 stc.id=stc.messenger
 stc.date_obs=anytim2utc(eis_stc.date_obs,/vms)
 pointings=eis_stc.pointings
 stc.ras_width=pointings.width
 stc.ras_height=pointings.height
 stc.xcen=pointings.ins_x
 stc.ycen=pointings.ins_y
 stc.sci_obj=pointings.zone

 return,stc

 end





