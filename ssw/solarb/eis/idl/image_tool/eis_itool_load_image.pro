PRO eis_itool_load_image, image_file, group=group, column=column, $
                      err=err, status=status
;+
; PROJECT:
;       SOHO - CDS/SUMER
;
; NAME:
;       EIS_LOAD_IMAGE
;
; PURPOSE:
;       Load in a FITS or GIF file and try to determine the CSI structure
;
; SYNTAX:
;       eis_itool_load_image, image_file
;
; INPUTS:
;       IMAGE_FILE -- Name of the image file to be loaded
;                is returned if no error occurs
;       STATUS - 0/1, status flag indicating failure/sucess of operation
;
; HISTORY:
;       Written, 1-Jan-2006, Zarro (L-3Com/GSFC) - based on ITOOL_LOAD_IMAGE
;
; CONTACT:
;       dzarro@solar.stanford.edu
;-

@image_tool_com
   err = ''
   status = 1

;---------------------------------------------------------------------------
;  Make sure the image file exists
;---------------------------------------------------------------------------
   IF NOT file_exist(image_file) THEN BEGIN
      err = 'File '+image_file+' not exist!'
      status = 0
      RETURN
   ENDIF

   IF N_ELEMENTS(image_arr) NE 0 AND !version.release GE '3.6.1' THEN BEGIN
;---------------------------------------------------------------------------
;     Save the curreny image status information for stacking
;---------------------------------------------------------------------------

      IF grep('sohologo', prev_file) EQ '' THEN BEGIN
         TVLCT, r, g, b, /get
         rgb = [[r], [g], [b]]
         icon = mk_img_icon(icon_size, image_arr, err=err)
         stack = {prev_file:prev_file, image_arr:image_arr, csi:csi, rgb:rgb, $
                  cur_min:cur_min, cur_max:cur_max, binary_fits:binary_fits, $
                  data_info:data_info, header_cur:header_cur, $
                  exptv_rel:exptv_rel, src_name:src_name, img_type:img_type, $
                  img_lock:img_lock, gif_file:gif_file, $
                  d_mode:d_mode, prev_col:prev_col, log_scaled:log_scaled, $
                  scview:scview,noaa:noaa}
      ENDIF
   ENDIF
;---------------------------------------------------------------------------
;  Test to see if the file to be loaded is already in the image stack
;---------------------------------------------------------------------------
   fname = strip_dirname(image_file)
   IF N_ELEMENTS(column) NE 0 THEN fname = fname+'_'+STRTRIM(column, 2)
   IF N_ELEMENTS(img_icon) NE 0 THEN BEGIN
      id = (WHERE(fname EQ img_icon.filename))(0)
      IF id GE 0 THEN BEGIN
         dprint, 'Switch images from the stack...'
         itool_xchg_stack, id, stack, icon, err=err
         IF err EQ '' THEN BEGIN
            itool_icon_plot
            RETURN
         ENDIF
      ENDIF
   ENDIF
;---------------------------------------------------------------------------
;  Do nothing if the same file is tried to be loaded again
;---------------------------------------------------------------------------
   IF N_ELEMENTS(prev_file) NE 0 THEN BEGIN
      IF image_file EQ prev_file THEN BEGIN
         redo = 0
         IF N_ELEMENTS(column) NE 0 THEN BEGIN
            redo = 1
         ENDIF
         IF NOT redo THEN BEGIN
            itool_refresh
            RETURN
         ENDIF
      ENDIF
   ENDIF

   gif_true = valid_gif(image_file) or valid_jpeg(image_file)
   IF NOT gif_true THEN BEGIN
      eis_itool_rd_fits, image_file, image_arr, header_cur, $
         image_max=image_max, image_min=image_min, data_info=data_info, $
         errmsg=errmsg, group=group, column=column, csi=img_csi, $
         status=status,index=index
      IF NOT status THEN BEGIN
         err = errmsg
         xack,err
         RETURN
      ENDIF
      gif_file = 0
   ENDIF ELSE BEGIN
      itool_rd_gif, image_file, image_arr, minimum=image_min, err=err, $
         maximum=image_max, color=ctable, group=group, $
         status=status, csi=img_csi
      IF NOT status THEN BEGIN
         xack,err
         RETURN
      ENDIF
      header_cur = ''
      data_info = {binary:0, label:'', col:1, cur_col:1}
      gif_file = 1
   ENDELSE

   IF !version.release GE '3.6.1' AND exist(stack) THEN BEGIN
;---------------------------------------------------------------------------
;     Put the current image and related variables into image stack
;---------------------------------------------------------------------------
      n_stack = N_ELEMENTS(img_stack)
      fname = strip_dirname(prev_file)
      IF prev_col NE 0 THEN BEGIN
         fname = fname+'_'+STRTRIM(prev_col, 2)
      ENDIF
      IF n_stack EQ 0 THEN BEGIN
         make_pointer,img_stack
         set_pointer,img_stack,stack
         img_icon = {data:icon, filename:fname}
      ENDIF ELSE BEGIN
;---------------------------------------------------------------------------
;        Pop off the last image if more than 12 images already
;---------------------------------------------------------------------------
         IF n_stack EQ max_stack THEN BEGIN
            free_pointer,img_stack(n_stack-1)
            img_stack = img_stack(0:n_stack-2)
            img_icon = img_icon(0:n_stack-2)
         ENDIF
         make_pointer,temp
         set_pointer,temp,stack
         img_stack = [temp,img_stack]
         img_icon = concat_struct({data:icon, filename:fname},img_icon)
      ENDELSE
      itool_update_iconbt
   ENDIF

   prev_file = image_file
   IF N_ELEMENTS(column) NE 0 THEN prev_col = column ELSE prev_col = 0
   disp_utc = anytim2utc(img_csi.date_obs, /ecs)

   binary_fits=0
   cur_max = image_max & cur_min=image_min
   exptv_rel = 1.0
   widget_control2, draw_id, map=1
   IF !d.window NE root_win THEN setwindow, root_win


   csi = itool_new_csi()
   log_scaled = 0
   scview = 0
   log_list = ['YOHK', 'MLSO', 'SEIT', 'PDMO']
   src_name = itool_img_src(img_csi.origin)
   img_type = itool_img_type(img_csi.imagtype)
   itool_display, image_arr, max=cur_max, min=cur_min, relative=exptv_rel, $
      csi=csi
   itool_icon_plot
   itool_adj_ctable, /init

   copy_struct, img_csi, csi

   doy=' (doy '+trim(string(utc2doy(anytim2utc(disp_utc))))+')'
   widget_control2, obs_text, set_value=disp_utc+doy

   IF NOT csi.flag THEN BEGIN
      widget_control2, ptool_send, sensitive=0
      flash_msg, comment_id, $
         ['Warning: This image file does NOT have necessary '+$
          'information to establish a solar coordinate system. '+$
             'Pointing Tool is disabled for this image.', $
          '    To make Pointing Tool available, please use the '+$
          '"Limb Fitter" Tool to fit the solar disc limb.'], $
         num=2
      
      IF tools(curr_tool).uvalue EQ 'ptool' THEN BEGIN
;---------------------------------------------------------------------------
;        Since CSI is not complete for pointing, we have to
;        desensitize the pointing tool
;---------------------------------------------------------------------------
         widget_control2, tools(curr_tool).base, sensitive=0
      ENDIF
   ENDIF

;---------------------------------------------------------------------------
;  Since a new image is loaded in, we have to reset certain things
;---------------------------------------------------------------------------
   IF N_ELEMENTS(initial) NE 0 THEN delvarx, initial
   IF zoom_in EQ 1 THEN BEGIN
      zoom_in = 0
      widget_control2, zoom_bt, set_value='Zoom In'
   ENDIF

   img_lock = 0
   widget_control2, lock_bt, set_value='Lock Orientation'

   itool_disp_plus, /keep
   widget_control2, save_img, sensitive=1
   RETURN
END

