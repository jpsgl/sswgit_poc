;+
; Project     : SOLAR-B/EIS
;
; Name        : EIS_ITOOL_STC
;
; Purpose     : Convert EIS pointing structure to format
;               recognized by EIS_IMAGE_TOOL
;
; Category    : Planning
;
; Syntax      : IDL> stc=eis_itool_stc(eis_stc)
;
; Inputs      : EIS_STC = EIS pointing structure
;
; Outputs     : STC = EIS_IMAGE_TOOL pointing structure
;
; Keywords    : None
;       
; History     : 16-May-2006, Zarro (L-3Com/GSFC) - written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function eis_itool_stc,eis_stc

 if not is_struct(eis_stc) then begin
  pr_syntax,'stc=eis_itool_stc(eis_stc)'
  if exist(eis_stc) then return, eis_stc else return,-1
 endif

;-- return if input stucture in required format

 if have_tag(eis_stc,'pointings') then return,eis_stc
 
;-- create template pointing structure for IMAGE_TOOL

 n_pointings=n_elements(eis_stc)
 mk_point_stc,stc,n_pointings=n_pointings,/fov
 
;-- copy start time of first raster

 time=anytim2tai(str_replace(eis_stc[0].date_obs,'(0)',''))
 stc.messenger=eis_stc[0].id
 stc.date_obs=time
 stc.sci_spec=eis_stc[0].sci_obj
 stc.instrume='EIS'

 stc.pointings.ins_x=eis_stc.xcen
 stc.pointings.ins_y=eis_stc.ycen
 stc.pointings.width=eis_stc.ras_width
 stc.pointings.height=eis_stc.ras_height
 stc.pointings.zone=eis_stc.sci_obj 
 stc.rasters.pointing=0
 return,stc

 end





