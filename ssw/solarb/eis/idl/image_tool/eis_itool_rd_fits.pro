;+
; PROJECT:
;       SOLAR-B/EIS
;
; NAME:
;       EIS_ITOOL_RD_FITS
;
; PURPOSE:
;       Driver program of FXREAD and CDS_IMAGE to read any FITS file
;
; CALLING SEQUENCE:
;       eis_itool_rd_fits, data_file, image, header, min=min, max=max, $
;                      image_max=image_max, image_min=image_min $
;                      [,errmsg=errmsg]
;
; INPUTS:
;       DATA_FILE - name of FITS file
;
; OUTPUTS:
;       IMAGE  - A 2-D image array
;       HEADER - Header of the image
;       CSI     - Part of CSI structure. It's important to get this structure
;
; KEYWORD PARAMETERS:
;       IMAGE_MIN - Minimum value of the image array
;       IMAGE_MAX - Maximum value of the image array
;       ERRMSG    - Error message returned (null string if no error)
;       STATUS    - Flag (0/1) of failure/success
;
; MODIFICATION HISTORY:
;       Written, 1-Jan-06, Zarro (L-3Com/GSFC) - modified from ITOOL_RD_FITS
;
; CONTACT:
;       dzarro@solar.stanford.edu
;-
;-------------------------------------------------------------------------

pro eis_itool_rd_fits, data_file, image, header, group=group, data_info=data_info,$
                   image_max=image_max, image_min=image_min, errmsg=errmsg, $
                   column=column, csi=csi, status=status,index=index,_extra=extra
    
   status=0
   if n_params() lt 3 then begin
      message,'usage: eis_itool_rd_fits, file, image, header [,keyword=]',/cont
      return
   endif
   errmsg = ''

   fits=obj_new('fits')
   fits->read,data_file,err=errmsg
   if is_string(errmsg) then return
   status=1
   image=fits->get(/data,/no_copy)
   header=fits->get(/header)
   csi = itool_set_csi(header, err=errmsg)
   index=fits->get(/index)
   image_max=max(image,min=image_min)
   obj_destroy,fits

   return & end


