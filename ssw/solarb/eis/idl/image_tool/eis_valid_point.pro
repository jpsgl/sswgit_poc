;+
; Project     : SOLAR-B/EIS
;
; Name        : EIS_VALID_POINT
;
; Purpose     : Check if EIS raster pointing is within EIS FOV
;
; Category    : Planning
;
; Syntax      : IDL> valid=eis_valid_point(x,y,point_stc,index)
;
; Inputs      : X,Y = pointing coordinates (arcsecs) to check
;               POINT_STC = EIS pointing structure (EIS_MK_PLAN &
;               EIS_IMAGE_TOOL formats are supported).
;               EIS_FOV = EIS fov structure
;
; Outputs     : VALID = 1/0 if valid/invalid
;
; Keywords    : INDEX = index of raster (if more than one raster)
;       
; History     : 16-Aug-2006, Zarro (ADNET/GSFC) - written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function eis_valid_point,x,y,point_stc,eis_fov,index=index

;-- check inputs

if (1-is_number(x)) or (1-is_number(y)) or (1-is_struct(point_stc)) $
 or (1-is_struct(eis_fov)) then return,1b

if (1-is_number(index)) then index=0
index= 0 > index < (n_elements(point_stc.pointings)-1)

;-- check type of pointing strucure

itool_stc=have_tag(point_stc,'pointings')
if (1-itool_stc) then return,1b

;-- determine raster limits for input coordinates

pointings=point_stc.pointings[index]
width=pointings.width
height=pointings.height
max_x=float(x)+width/2.
min_x=float(x)-width/2.
max_y=float(y)+height/2.
min_y=float(y)-height/2.

;-- determine current FOV limits

xfov=eis_fov.x
yfov=eis_fov.y
mx=xfov[0]
px=xfov[1]
my=yfov[0]
py=yfov[2]

;-- finally check if raster fits in fov

tol=10.

d1=(max_x le px) or ((max_x - px) le tol)
d2=(max_y le py) or ((max_y - py) le tol)
d3=(min_x ge mx) or ((mx - min_x) le tol)
d4=(min_y ge my) or ((my - min_y) le tol)

fits= d1 and d2 and d3 and d4

print,d1,d2,d3,d4

return,fits

end





