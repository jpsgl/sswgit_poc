;+
; Project     : SOLAR-B/EIS
;
; Name        : EIS_ENV_INI
;
; Purpose     : Extract system parameters for EIS_MK_PLAN from environment variables
;               in file "setup.eis_env"
;
; Category    : planning
;
; Syntax      : IDL> eis_env_ini,system_data,status=status
;
; Inputs      : SYSTEM_DATA = structure defined by {eis_system_data}
;
; Outputs     : SYSTEM_DATA = structure with fields populated from environment variables
;
; Keywords    : STATUS = 1/0 if successful or not 
;
; History     : Written, 30-May-2006, Zarro (L-3Com/GSFC)
;
; Contact     : dzarro@solar.stanford.edu
;-

pro eis_env_ini,system_data,status=status

status=0b
if not is_struct(system_data) then system_data={eis_system_data}

system_data.ancillary=local_name(getenv('ancillary'))
system_data.study_descriptions=local_name(getenv('study_descriptions'))
system_data.official_database_directory=local_name(getenv('official_database_directory'))
system_data.engineering_file_directory=local_name(getenv('engineering_file_directory'))
system_data.resources_directory=local_name(getenv('resources_directory'))
system_data.cpt_software_dir=local_name(getenv('cpt_software_dir'))
system_data.cpt_output_dir=local_name(getenv('cpt_output_dir'))
system_data.cpt_input_dir=local_name(getenv('cpt_input_dir'))

status=is_string(system_data.ancillary)

return & end

