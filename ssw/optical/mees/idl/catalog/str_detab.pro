;+
; 
; Removes tabs from strings and replaces them with the
; correct number of blanks.
;

FUNCTION str_detab, instr, tabspace=tabspace

;
; INPUT PARAMETERS:
;	instr = String array to remove tabs from.
;
; RETURNS:
;	String array with blanks in place of tabs.
;
; OPTIONAL INPUT KEYWORDS:
;	tabspace = number of characters that correspond to one tab.
;		Default = 8.
;
; HISTORY:
;	Written  August 2, 2000  Barry LaBonte
;
;-

nlines = N_ELEMENTS(instr)
outstr = STRARR(nlines)

IF( KEYWORD_SET(tabspace) ) THEN tabs = tabspace ELSE tabs = 8
blank = REPLICATE( BYTE(32), tabs )

FOR j=0,nlines-1 DO BEGIN

	nchr = STRLEN( instr(j) )
	bstr = BYTE( instr(j) )
	tx = WHERE( bstr EQ 9, ntx )

; Just copy if no content or no tabs
	IF( ntx LE 0 OR nchr LE 0 ) THEN BEGIN

		outstr(j) = instr(j)

	   ENDIF ELSE BEGIN
; Work to do
		count = 0
		obstr = BYTE(0)
		FOR i=0,ntx-1 DO BEGIN
; How many nontabs in front of this tab to copy?
		   i0 = tx(i)
		   IF( i EQ 0 ) THEN ncopy=i0 ELSE ncopy=i0-tx(i-1)
		   IF( ncopy GT 0 ) THEN obstr = [obstr, bstr(i0-ncopy:i0-1)]
		   count = count + ncopy
; Now add on needed blanks
		   need = (tabs - 1) - (count MOD tabs)
		   IF( need GT 0 ) THEN obstr = [obstr, blank(0:need-1)]
		   count = count + need
		ENDFOR
; Finally, the end of the string
		nend = (nchr - 1) - tx(ntx-1)
		IF( nend GT 0 ) THEN obstr = [obstr, bstr(tx(ntx-1)+1:*)]
		outstr(j) = STRING( obstr(1:*) )

	ENDELSE

ENDFOR


RETURN, outstr
END
