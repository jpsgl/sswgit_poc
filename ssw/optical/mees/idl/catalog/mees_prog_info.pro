;+
;
; Returns information about observing setups for instruments
; at Mees Solar Observatory
;

FUNCTION mees_prog_info, inray

;
; INPUT PARAMETERS:
;	inray = array of requests, string array.  inray(0,*) = instrument
;		ID, inray(1,*) = program ID.
;		Valid instrument IDs are:
;		'MWLT', 'K-LINE', 'POI', 'CORON', 'STOKES', 'IVM', 'MCCD'.
;
; RETURNS:
;	outray = array of information, string array.  Elements are:
;		outray(0,*) = Temporal cadence, seconds. Set to -1 if single
;				observations only.
;		outray(1,*) = FOV, arcseconds.
;		outray(2,*) = Pixel scale, arcseconds.
;		outray(3,*) = Wavelength, nm.
;
; RESTRICTIONS:
;	Returns blanks if an invalid instrument/program pair are requested.
;
; HISTORY:
;	Written  October 19, 1999  Barry LaBonte
;
;-


; Valid program information
vpi = [ 'MWLT',   'JPW06', '300.', '2000', '2',     '460' ]
vpi = [ vpi, 'K-LINE', 'BJL01',  '30.', '2000', '5.6', '393.3' ]
vpi = [ vpi, 'K-LINE', 'KAB01',  '19.', '2000', '5.6', '393.3' ]
vpi = [ vpi, 'KLINE', 'BJL01',  '30.', '2000', '5.6', '393.3' ]
vpi = [ vpi, 'KLINE', 'KAB01',  '19.', '2000', '5.6', '393.3' ]
vpi = [ vpi, 'POI',    'BJL11', '5400', '2000', '2',   '393.3' ]
vpi = [ vpi, 'CORON',  'HALPH',    '1', '1200', '2.2', '656.3' ]
vpi = [ vpi, 'STOKES', 'RCC01',   '-1',  '240', '5.6', '630.3' ]
vpi = [ vpi, 'IVM',    'RK01',    '60',  '141', '0.55','630.3' ]
vpi = [ vpi, 'IVM',    'DLM03',  '480',  '282', '1.1', '630.3' ]
vpi = [ vpi, 'IVM',    'DLM06',  '480',  '282', '1.1', '630.3' ]
vpi = [ vpi, 'IVM',    'DLM09',  '180',  '282', '1.1', '630.3' ]
vpi = [ vpi, 'IVM',    'DLM10',  '180',  '282', '1.1', '589.0' ]
vpi = [ vpi, 'IVM',    'DLM11',  '120',  '282', '0.55','630.3' ]
vpi = [ vpi, 'IVM',    'DLM12',  '240',  '282', '0.55','630.3' ]
vpi = [ vpi, 'IVM',    'DLM13',  '190',  '282', '1.1', '630.3' ]
vpi = [ vpi, 'MCCD',   'KPR01',   '35',  '264', '2.4', '656.3' ]
vpi = [ vpi, 'MCCD',   'GNC01',  '160',  '210', '1.2', '656.3' ]
vpi = [ vpi, 'MCCD',   'GNC04',   '80',  '100', '0.58','656.3' ]
vpi = [ vpi, 'MCCD',   'JPW04',   '20',  '240', '2.4', '656.3' ]
vpi = [ vpi, 'MCCD',   'JPW05',   '20',  '240', '2.4', '656.3' ]
vpi = [ vpi, 'MCCD',   'TRM01',   '20',  '220', '2.4', '656.3' ]
vpi = REFORM( vpi, 6, N_ELEMENTS(vpi)/6 )



; How big?
nreq = N_ELEMENTS(inray(0,*))
outray = STRARR(4, nreq)

FOR i=0,nreq-1 DO BEGIN
	ix = WHERE( vpi(0,*) EQ inray(0,i) AND vpi(1,*) EQ inray(1,i), nix )
	IF( nix GT 0 ) THEN outray(*,i) = vpi(2:*,ix(0))
ENDFOR

RETURN, outray
END
