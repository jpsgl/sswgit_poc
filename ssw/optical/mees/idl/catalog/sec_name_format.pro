;+
;
; Generates complete URLs for Space Environment Center databases
;

FUNCTION sec_name_format, datatype, date

;
; INPUT PARAMETERS:
;       datatype = Data name, may include preceding 'SEC_'.  Valid names are:
;		EVENTS		Flare events list
;		GOES_FLUX	GOES 5 minute soft x-ray flux
;	date = Date of desired file, any standard format.
;
; RETURNS:
;	Cpmplete URL.
;
; HISTORY:
;	Written September 13, 2000  Barry LaBonte
;
;-

; Get date parts
; Now
now = STRCOMPRESS( SYSTIME() )
now = STR_SEP( now, ' ' )
nex = ANYTIM( now(2)+'-'+now(1)+'-'+now(4), /EXTERNAL )
nyear = now(4)

; Day to plot
tex = ANYTIM( date, /EXTERNAL )

dd = STRTRIM( STRING( tex(4) ), 2 )
IF( STRLEN(dd) LT 2 ) THEN dd = '0' + dd
mm = STRTRIM( STRING( tex(5) ), 2 )
IF( STRLEN(mm) LT 2 ) THEN mm = '0' + mm
year = STRTRIM( STRING( tex(6) ), 2 )

y2 = STRMID( year, 2, 2 )

doy = STRTRIM( ANYTIM2DOY( tex ), 2 )
doy = doy(0)
IF( STRLEN(doy) EQ 1 ) THEN doy = '00' + doy
IF( STRLEN(doy) EQ 2 ) THEN doy = '0' + doy

; What kind of data?
six = STRPOS( datatype, 'SEC_' )
IF( six LT 0 ) THEN type = datatype $
	ELSE type = STRMID( datatype, six+4, 100 )

CASE type OF

; GOES 5 minute flux
	'GOES_FLUX': BEGIN
		urltop = 'gopher://solar.sec.noaa.gov:70/00/lists/xray/'
		filename = year + mm + dd + '_G10xr_5m.txt'
	END

; Flare events list
	'EVENTS': BEGIN
		urltop = 'ftp://www.sec.noaa.gov/pub/indices/'
		ssubdir = 'events/'
		IF( year NE nyear ) THEN ssubdir = year + '_events/'
		IF( year EQ nyear AND nex(5)-tex(5) GT 3 ) THEN $
			ssubdir = year+'_events/'
		sfile =  year + mm + dd + 'events.txt'
		filename = ssubdir + sfile
	END
ENDCASE

RETURN, urltop + filename
END
