;+
;
; Main program for daily Mees Log plot
;

;  MEES_LOG_START

;
; RESTRICTIONS:
;	Uses GUNZIP to read Mees Met files.
;
; HISTORY:
;	 Written  October 25, 1999  Barry LaBonte
;	Updated filename generation  Sept 13, 2000  BJL
;
;-


;====================================================================

; Day to plot
pday = STR_SEP( STRCOMPRESS(SYSTIME()), ' ', /TRIM )
date = pday(2) + '-' + pday(1) + '-' + pday(4)

; Mees Observing log filename
mlfile = REMOTE_NAME_FORMAT( 'MEES_LOG', date )
; Mees Meteorology filename
mwfile = REMOTE_NAME_FORMAT( 'MEES_MET', date )
; GOES filename
gfile =  REMOTE_NAME_FORMAT( 'SEC_GOES_FLUX', date )

; Temporary files
tlog = '/tmp/mlog'+pday(3)
tsol = '/tmp/msol'+pday(3)
tgoes = '/tmp/goes'+pday(3)

; Get data and put in temporary directory
cmd = 'perl -I $SSW_PERL/url_get $SSW_PERL/url_get/url_get'

; First the Mees Log
SPAWN, cmd + ' ' + mlfile + ' > ' + tlog
; Does the logfile exist?
OPENR, lun, tlog, /GET_LUN
res = FSTAT(lun)
CLOSE, lun
FREE_LUN, lun
; No, quit
IF( res.size LE 0 ) THEN BEGIN &$
	SPAWN, 'rm ' + tlog &$
	EXIT &$
ENDIF

; Now the Met data
SPAWN, cmd + ' ' + mwfile + '| gunzip -c > ' + tsol
OPENR, lun, tsol, /GET_LUN
res = FSTAT(lun)
CLOSE, lun
FREE_LUN, lun
IF( res.size LE 0 ) THEN BEGIN &$
	SPAWN, 'rm ' + tsol &$
	tsol = '' &$
ENDIF
; And GOES
SPAWN, cmd + ' ' + gfile + ' > ' + tgoes
OPENR, lun, tgoes, /GET_LUN
res = FSTAT(lun)
CLOSE, lun
FREE_LUN, lun
IF( res.size LE 0 ) THEN BEGIN &$
        SPAWN, 'rm ' + tgoes &$
	tgoes = '' &$
ENDIF


; Set up and plot
SET_PLOT, 'Z'

MEES_LOG_PLOT, tlog, GOES=tgoes, SOLFLUX=tsol, /GIF

; Clean up
SPAWN, 'rm -f ' + tlog + ' ' + tsol + ' ' + tgoes

EXIT
