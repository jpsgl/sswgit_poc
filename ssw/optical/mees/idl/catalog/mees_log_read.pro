;+
;
; Reads the Mees Solar Observatory daily log text file
; and returns the information in a structure.
;

FUNCTION mees_log_read, file

;
; INPUT PARAMETERS:
;	file = name of log file to read, string.
;
; RETURNS:
;	outstr = log output structure, one element per log text line.
;
; RESTRICTIONS:  Does not work well on early data formats.  Problem
;	is connected with Tab spacing of files.  (17-Jul-2000)
;
; HISTORY:
;	Written  Oct 14, 1999  Barry LaBonte
;	Use ANYTIM in place of FMT_TIM to get century  July 13, 2000  BJL
;	Use STR_DETAB to align columns  August 2, 2000  BJL
;
;-

; Instrument names
instr = ['MWLT', 'KLINE', 'K-LINE', 'POI', 'CORON', 'STOKES', 'IVM', 'MCCD']
ninstr = N_ELEMENTS( instr )


;====================================================================


; Does the file exist?
OPENR, lun, file, /GET_LUN, ERROR=ferror
IF( ferror NE 0 ) THEN RETURN,-1

; Yes, read the log file
fstr = ''
tmp = ''
WHILE( NOT EOF(lun) ) DO BEGIN
	READF, lun, tmp
	fstr = [fstr, tmp]
ENDWHILE
CLOSE, lun
FREE_LUN, lun
cfstr = STRCOMPRESS(fstr, /REMOVE_ALL)
good = WHERE( STRLEN(fstr) GT 0 AND STRLEN(cfstr) GT 0, ngood )
IF( ngood GT 0 ) THEN fstr = fstr(good) ELSE RETURN,-1

; Clean up the tabs
fstr = STR_DETAB( fstr )

; Pick up the date and format lines, get date string
dx = STRPOS( fstr, 'DATE' )
ix = WHERE( dx GE 0 )
IF( dx(ix(0)) EQ 0 ) THEN BEGIN
	dateline = STRTRIM( fstr(ix(0)+2), 2 )
	day_str = STR_SEP( dateline, ' ' )
	day_str = day_str(0)
	IF( STRLEN( day_str ) EQ 6 ) THEN BEGIN
; Old style, mmddyy
		day_str = '19' + STRMID(day_str,4,2) $
			+ '/' + STRMID(day_str,0,2) $
			+ '/' + STRMID(day_str,2,2)
	   ENDIF ELSE BEGIN
; Old style, yy.mm.dd
		day_str = '19' + STRMID(day_str,0,2) $
			+ '/' + STRMID(day_str,3,2) $
			+ '/' + STRMID(day_str,6,2)
	ENDELSE
	fmtline = fstr(ix(0))
   ENDIF ELSE BEGIN
; New style, ccyy.mm.dd
	dateline = STRTRIM( STRMID( fstr(ix(0)), dx(ix(0))+4, 20 ), 2 )
	day_str = STRMID(dateline,0,4) + '/' + STRMID(dateline,5,2) $
		+ '/' + STRMID(dateline,8,2)
	fx = WHERE( STRPOS( fstr, 'INST' ) GE 0 )
	fmtline = fstr(fx(0))
ENDELSE

; Locate the format fields
iprog = STRPOS( fmtline, 'PROG' )
istart = STRPOS( fmtline, 'START' ) - 1
iend = STRPOS( fmtline, 'END' )
ins = STRPOS( fmtline, 'N-S' )
iwe = STRPOS( fmtline, 'W-E' )
irgn = STRPOS( fmtline, 'RGN' ) - 1
icom = STRPOS( fmtline, 'COMMENT' )

; And the comments
ic = WHERE( STRPOS( fstr, 'COMMENTS:' ) GE 0, ncomnt )
IF( ncomnt GT 0 ) THEN BEGIN
; New style, COMMENT heading
        ic = ic(0)
   ENDIF ELSE BEGIN
; Old style, start in on NOTE
	snote = STRPOS( fstr, 'NOTE' )
        ic = WHERE( snote GE 0 AND snote LT 8, ncomnt )
        ic = ic(0)
; But check for out of order
	imso = WHERE( STRPOS( fstr, 'MSO Observer') GE 0, nimso )
	IF( nimso GT 0 ) THEN BEGIN	
		IF( imso(0) LT ic ) THEN ic = imso(0)
	ENDIF

ENDELSE

; Now find the instrument sections and sort
ixnstr = REPLICATE( -1, ninstr )
FOR i=0,ninstr-1 DO BEGIN
	ix = WHERE( STRPOS( fstr, instr(i) ) GT 0, nix )
	IF( nix GT 0 ) THEN ixnstr(i) = ix(0)
ENDFOR
good = WHERE( ixnstr GT 0 AND ixnstr LT ic, nnstr )
IF( nnstr LE 0 ) THEN RETURN,-1
tinstr = instr(good)
ixnstr = ixnstr(good)
sx = SORT( ixnstr )
tinstr = tinstr(sx)
ixnstr = ixnstr(sx)

IF( N_ELEMENTS(ixnstr) GT 1 ) THEN jxnstr = [ixnstr(1:*), ic] - 1 $
	ELSE jxnstr = [ic] - 1
; Force good behavior
jxnstr = jxnstr > ixnstr
sadd = STRPOS( fstr, 'ADDITIONAL')
icend = WHERE( sadd GE 0 AND sadd LT 8, ncend )
IF( ncend GT 0 ) THEN icend = icend(0) ELSE icend = ngood - 2
ncomment = icend - ic - 2
IF( ncomment GT 0 ) THEN cstr = STRCOMPRESS( fstr(ic+1:icend-1) )

; Loop through the instruments present and organize
newstr = STRARR(8)
FOR i=0,nnstr-1 DO BEGIN

   nnow = jxnstr(i) - ixnstr(i) + 1
   now = fstr(ixnstr(i):jxnstr(i))

; Pull out the info
   prog = STRTRIM( STRMID( now, iprog-1, istart-iprog+1 ), 2 )
   tstart = STRTRIM( STRMID( now, istart-1, iend-istart ), 2 )
   tend = STRTRIM( STRMID( now, iend-1, ins-iend ), 2 )
   ns = STRTRIM( STRMID( now, ins-1, iwe-ins ), 2 )
   we = STRTRIM( STRMID( now, iwe-1, irgn-iwe ), 2 )
   rgn = STRTRIM( STRMID( now, irgn-1, icom-irgn ), 2 )
   jcom = STRTRIM( STRMID( now, icom-1, 12 ), 2 )

; Are there valid times?
   btmp = BYTE(tstart(0))
   IF( MIN(btmp) LT 48 OR MAX(btmp) GT 57 ) THEN tstart(0)=''
   minus = WHERE( tend EQ '----', nminus )	
   IF( nminus GT 0 ) THEN tend(minus) = tstart(minus)
; Is there actually data for this instrument?
   IF( tstart(0) NE '' ) THEN BEGIN
; Convert current times
	hrs = STRMID(tstart,0,2)
	mins = STRMID(tstart,2,2)
	next = WHERE( FIX(hrs) LT 12, nnext )
	IF( nnext GT 0 ) THEN hrs(next) = STRTRIM(STRING(FIX(hrs(next)) + 24),2)
	hre = STRMID(tend,0,2)
	mine = STRMID(tend,2,2)
	next = WHERE( FIX(hre) LT 12, nnext )
	IF( nnext GT 0 ) THEN hre(next) = STRTRIM(STRING(FIX(hre(next)) + 24),2)
	tims = STRARR(nnow)
	time = STRARR(nnow)
	tims = day_str + ' ' + hrs + ':' + mins
	time = day_str + ' ' + hre + ':' + mine

; Create new format
	tmp = STRARR(8, nnow)
	FOR j=0,nnow-1 DO BEGIN
; Pick up comment?
	   IF( jcom(j) NE '' AND SIZE( cstr, /N_ELEMENTS) GT 0 ) THEN BEGIN
		hit = WHERE( STRPOS( cstr, tinstr(i) ) GE 0 AND $
			STRPOS( cstr, tstart(j) ) GT 0, nhit )
		IF( nhit GT 0 ) THEN $
		   jcom(j) = STRTRIM( STRMID( cstr(hit(0)), $
			STRPOS(cstr(hit(0)), tstart(j))+4, 80), 2)
	   ENDIF
; Generate region?
	   IF( rgn(j) EQ '' AND ns(j) NE '' ) THEN rgn(j) = ns(j) + we(j)

	   tmp(*,j) = [tinstr(i),prog(j),ANYTIM(tims(j),/STIME), $
		ANYTIM(time(j),/STIME), ns(j), we(j), rgn(j), jcom(j) ]
	ENDFOR

	newstr = [[newstr],[tmp]]
   ENDIF
ENDFOR

; Was there any data?
IF( N_ELEMENTS(newstr(0,*)) EQ 1 ) THEN RETURN,-1

; Yes
newstr = newstr(*,1:*)
nlines = N_ELEMENTS(newstr(0,*))

; Sort into order
ixnew = INTARR(nlines)
FOR i=0,nlines-1 DO BEGIN
	kx = WHERE( instr EQ newstr(0,i) )
	ixnew(i) = kx(0)
ENDFOR
newstr = newstr( *, SORT(ixnew) )

; Interpret programs
proginfo = MEES_PROG_INFO( newstr(0:1,*) )

newstr = [newstr(0:6,*), proginfo, newstr(7,*)]

; Finally, replace old names with new
iold = WHERE( newstr(0,*) EQ 'STOKES', nold )
IF( nold GT 0 ) THEN newstr(0,iold) = 'HSP'
iold = WHERE( newstr(0,*) EQ 'CORON', nold )
IF( nold GT 0 ) THEN newstr(0,iold) = 'COR'
iold = WHERE( newstr(0,*) EQ 'K-LINE', nold )
IF( nold GT 0 ) THEN newstr(0,iold) = 'KLINE'

; Convert to a structure
tags = ['INSTRUMENT', 'PROGRAM', 'TSTART', 'TEND', 'NS', 'EW', 'REGION', $
	 'CADENCE', 'FOV', 'PIXELSCALE', 'WAVELENGTH', 'COMMENT']
types = ['A', 'A', 'A', 'A', 'I', 'I', 'A', 'F', 'F', 'F', 'F', 'A']
CREATE_STRUCT, outstr, 'MEES_OBS_LOG', tags, types
outstr = REPLICATE( outstr, nlines )
ntags = N_ELEMENTS(tags)
FOR i=0,ntags-1 DO BEGIN
	IF( types(i) EQ 'A' ) THEN outstr(*).(i) = REFORM( newstr(i,*) )
	IF( types(i) EQ 'F' ) THEN outstr(*).(i) = REFORM(FLOAT( newstr(i,*) ) )
	IF( types(i) EQ 'I' ) THEN BEGIN
		sgn = STRMID( newstr(i,*), 0, 1 )
		val = FIX( STRMID( newstr(i,*), 1, 2 ) )
		neg = WHERE( sgn EQ 'S' OR sgn EQ 'E', nneg )
		IF( nneg GT 0 ) THEN val(neg) = -val(neg)
		outstr(*).(i) = REFORM( val )
	ENDIF
ENDFOR

RETURN, outstr
END
