pro read_nso, files, index, data, outsize=outsize
;
;   Name: read_nso
;
;   Purpose: read one or more NSO FITS into structure/data cube -> SSW
;
;   Input Parameters:
;      files - list of one or more NSO file names
;  
;   Output Parameters:
;      index - vector of 'header structures'
;      data (optional output) - corresonding 2D or 3D data implied by 'files'
;  
;   Keyword Parameters:
;      outsize - optional output size for DATA [nxny] or [nx,ny]
;  
;   Calling Sequence:
;      IDL> read_nso, files , index             ; header only read->index
;      IDL> read_nso, files , index , data      ; also read data (2D/3D)  
;  
;   History:
;      4-March-1999  - S.L.Freeland - based on read_eit, read_spartan et al...
;      6-March-1999  - S.L.Freeland - time conversion from STARTIME
;
;   Routines Referenced:
;      mreadfits, struct2ssw, etc. 
;-  

if n_params() lt 2 then begin
   box_messge,['Need at least input filelist and an output parameter',$
	       'IDL> read_nso, filelist, index [,data]']
endif

nodata=n_params() lt 3         ; dont bother reading data if no output param

; ------------- Use mreadfits , map headers=>struct ----------------------

mreadfits,files,index ,data ,nodata=nodata , /add_standard, outsize=outsize
; -------------------------------------------------------------------------

; -------------- fill boot up set of pointing tags ---------
; -----------------------------------------------------------
res=1788/gt_tagval(index,/naxis1)
index.crpix1=gt_tagval(index,/e_xcen)/res
index.crpix2=gt_tagval(index,/e_ycen)/res
index.cdelt1=gt_tagval(index,/scale)*res
index.cdelt2=gt_tagval(index,/scale)*res
; -----------------------------------------------------------
; -----------------------------------------------------------

; -----------------------------------------------------------
; time conversion use NSO .STARTIME (secs since 1-1-1970)
dateobs=anytim(anytim2ints('1-jan-1970',$
   off=float(gt_tagval(index,/startime))) ,/ccsds)
index.date_obs=dateobs
; -----------------------------------------------------------

; -----------------------------------------------------------
struct=struct2ssw(index)           ; derive & fill SSW from boot-set
; -----------------------------------------------------------

return
end
