;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Document name: soonfits.pro
; Created by:    Randy Meisner, HAO/NCAR, Boulder, CO, March 23, 1997
;
; Last Modified: Sun Mar 23 17:36:55 1997 by meisner (Randy Meisner) on hale
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
FUNCTION soonfits, file, head, extention=extention, help=help
;+
;
; PROJECT:
;       Master's Thesis
;
; NAME:
;       soonfits     (function)
;
; PURPOSE:
;       The purpose of this function is to read in image and header
;       data from a SOONSPOT extended FITS file.
;
; CALLING SEQUENCE:
;
;       result = soonfits(file, head [,extention=extention, help=help])
;
; INPUTS:
;       file - the file name of the SOONSPOT extended FITS file.
;
; OPTIONAL INPUTS: 
;       extention - the "frame" of the image file to read.  The
;                   default is to read image zero.
;
; OUTPUTS:
;       A 512x512 byte array containing the SOONSPOT image.
;
; OPTIONAL OUTPUTS:
;       head - the image header.
;
; KEYWORD PARAMETERS: 
;       /help.  Will call doc_library and list header, and return
;
; CALLS:
;       None.
;
; COMMON BLOCKS:
;       None.
;
; RESTRICTIONS: 
;       None.
;
; SIDE EFFECTS:
;       None.
;
; CATEGORY:
;       Randy Meisner's Master's Thesis.
;
; PREVIOUS HISTORY:
;       Written March 23, 1997, by Randy Meisner, HAO/NCAR, Boulder, CO
;
; MODIFICATION HISTORY:
;       
; VERSION:
;       Version 1, March 23, 1997
;-
;
ON_ERROR, 2

IF(N_ELEMENTS(help) GT 0) THEN BEGIN
  doc_library,'soonfits'
  RETURN, -999
ENDIF

IF(N_PARAMS() LT 1 OR N_PARAMS() GT 2) THEN BEGIN
  PRINT, ''
  PRINT, 'Usage:'
  PRINT, ''
  RETURN, -999
ENDIF

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Open the file and obtain the total number of available images.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

openr, in, file, /get_lun

check = fstat(in)

maxim = check.size/270720.0

print, '% SOONFITS:  ' + strtrim(string(maxim), 2) + ' images available ' + $
   'in ' + file + '.'

print, ''

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Setup file pointing constant variable.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

fstep = long(270720)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Setup header and image variables.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

hdr = bytarr(80, 72)

im = bytarr(512, 512)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Check if image extension was input.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

IF(n_elements(extention) GT 0) THEN BEGIN
   
   IF(extention GT maxim) THEN BEGIN
      
      print, ''
      print, '% SOONFITS:  Extention number can not exceed maximum number' + $
         ' of images in file.'
      print, ''
      
      close, /all
      free_lun, in
      
      return, -1
      
   ENDIF
   
ENDIF ELSE BEGIN
   
                                ; If no extention was input, default
                                ; to zero.
   extention = 0
   
ENDELSE

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Position the file pointer to the image in the file.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

pointlun = fstep*extention

point_lun, in, pointlun

print, '% SOONFITS:  Reading image ' + strtrim(string(extention), 2) + $
   '...'

print, ''

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Read in the image and header.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

readu, in, hdr

readu, in, im

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Close the file.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

close, /all
free_lun, in

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Convert the header to a string array.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

head = string(hdr > 32b)

RETURN, im

END

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; End of 'soonfits.pro'.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
