;+
; NAME:
;	UNIQ
; PURPOSE:
;	The uniq command scans the input array comparing adjacent
;	elements.  The second and succeeding copies of repeated values are
;	removed. The result is an array of indicies into the original
;	array which don't include the duplicates. Note that repeated
;	elements must be adjacent in order to be found. This routine is
;	intended to be used with the SORT command (See the discussion
;	of the IDX argument below).
;
;	This command is inspired by the Unix uniq(1) command.
; CATEGORY:
;	Array manipulation.
; CALLING SEQUENCE:
;	UNIQ(array [, IDX])
; INPUTS:
;	ARRAY: The array to be sorted. The type and number of dimensions
;		in the array is not important.
;
;	IDX: This optional array is an array of indices into ARRAY.
;		The expression:
;
;			ARRAY(IDX)
;
;		yields an array in which the elements of ARRAY are
;		rearranged by IDX. To understand the reason for this
;		argument, consider the fact that SORT returns an array
;		of indices, and that the expression:
;
;			ARRAY(SORT(ARRAY))
;
;		yields a sorted version of the original array. This
;		argument is intended to allow combining SORT and UNIQ
;		efficiently:
;
;			ARRAY = ARRAY(UNIQ(ARRAY, SORT(ARRAY)))
;
; OUTPUTS:
;	An array of indicies into ARRAY is returned. The expression:
;
;		ARRAY(UNIQ(ARRAY))
;
;	will be a copy of ARRAY with duplicate adjacent elements removed.
;
; KEYWORD PARAMETERS:
;   	first - if set, then the subscript of the first occurence of 
;		duplicate elements are returned (default is last occurence)
;
; COMMON BLOCKS:
;	None.
; MODIFICATION HISTORY:
;	5 January 1991, AB
;	17-Apr-92 (MDM) - Adjusted so that a scalar input would work (should
;			  return a value of zero)
;	12-Jan-92 (slf) - added 'first' keyword 
;	24-Apr-95 (MDM) - Modified to work on arrays over 32,000 elements
;-
;

function UNIQ, ARRAY_input, IDX, first=first

  array = array_input			;MDM added 17-Apr-92
  ; Check the arguments.
  s = size(ARRAY)
  ;if (s(0) eq 0) then message, 'ARRAY must be an array.'
  if (s(0) eq 0) then begin		;MDM added 17-Apr-92
	array = [array]			;turn into an array
	s = size(array)
  end
  if (n_params() lt 2) then IDX = lindgen(s(s(0)+2))

  s = size(IDX)
  res = idx

; slf, 12-Jan-92, added first keyword logic 
  if keyword_set(first) then begin
     for i = s(s(0)+2)-1, 1, -1 do $
       if (array(res(i)) eq array(res(i-1))) then res(i) = -1 
  endif else begin
     for i = 0L, s(s(0)+2)-2 do $
       if (array(res(i)) eq array(res(i+1))) then res(i) = -1
  endelse

  return, res(where(res ne -1))
end
