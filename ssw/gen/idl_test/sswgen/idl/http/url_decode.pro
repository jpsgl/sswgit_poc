function url_hexreplace, bval
;
;  Name: url_hexreplace
;
;  Purpose: replace encoded HEX with byte equivilent
;
;  Calling Sequence:
;    updated=url_hexreplace(bval)  
;
;    (generally called from url_decode)
; 
special=where_pattern(bval,'%',scnt)		; identify HEX characters
if scnt gt 0 then begin 			; 
   zvals=string(transpose(bval(special+1))) + $ ; extract HEX digits
         string(transpose(bval(special+2)))
   bexchange=bytarr(n_elements(zvals))		
   reads,zvals,bexchange,format='(z2.2)'	; convert to byte equiv
   bval(special)=bexchange			; replace '%' with byte(HEX) 
   bval([special+1,special+2])=32b		; blank fill other bytes
endif
return, bval
end

function url_decode, query, qfile=qfile
;+
;   Name: url_decode
;
;   Purpose: decode WWW URL-encoded query (ex: POST query -> IDL structure)
;
;   Input Parameters:
;      query - url encoded query (ex: from POST WWW Form)
;
;   Keyword Parameters:
;      qfile - file containing query (used in place of query parameter)
;
;   Output:
;      function returns IDL structure of form...
;         { name1: value1		     ; values are string/string arrays
;         [,name2: value2, nameN: valueN] }  ; one tag per query field
;
;   History:
;      20-March-1996 S.L.Freeland (for WWW/IDL server use)
;-
;  read WWW POST Query file
if keyword_set(qfile) then begin			; not found
   if not file_exist(qfile) then begin			; so look in "standard" 
      break_file,qfile,log,path,file,ext,version
      top_http=get_logenv('path_http')
      queryf=concat_dir(top_http,concat_dir('text_client',file+ext+version))
   endif else queryf=qfile
   if not file_exist(queryf) then begin
      message,/info,"Cannot find file: " + queryf
      return,''
   endif
   query=(rd_tfile(queryf))(0)
endif

;  parse query 
parts=str2arr(query,'&')			; field breaks
keys=strsplit(parts,'=',tail=values)		; tag/value breaks
nkeys=n_elements(keys)

; ------- process first key ---------
i=0
val=str2arr(values(i),'%0D%0A')				; inter-tag line breaks
bval=url_hexreplace(byte(val))				; convert to byte
val=str_replace(strcompress(bval,/remove),'+',' ')	; handle blanks

; define structure
exe=execute('outstr={' + keys(i)+ ':val}')		; initial structure

; --------- do the rest (if any) ----------------
while i lt (nkeys-1) do begin
   i=i+1
   val=str2arr(values(i),'%0D%0A')			; inter-tag line breaks
   bval=url_hexreplace(byte(val))			; convert HEX-> byte
   val=str_replace(strcompress(bval,/remove),'+',' ')   ; trim excess blanks
						        ;   and add true blanks
   outstr=add_tag(outstr,val,keys(i))			; new tag -> output str
endwhile

return,outstr
end
