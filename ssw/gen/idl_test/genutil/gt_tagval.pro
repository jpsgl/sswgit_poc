function gt_tagval, item, tag, str_pattern=str_pattern, $
      level=level, struct=struct, found=found
;+
;   Name: gt_tagval
;
;   Purpose: return value stored in specified tag - (nested N-deep struct OK)
;
;   Input Parameters:
;      item       - structure (FITS header)
;      tag/field  - tag to search for
;      
;   Optional Keyword Parameters:
;      str_pattern (input)  - optional match pattern in structure name
;      struct 	   (output) - structure name at match level 
;      level       (output) - nest level where match found 
;		   [ -1 -> not found, 0 -> top level, 1 -> nested one down, etc)
;      found       (boolean) - true if found (level ne -1)
;
;   Calling Sequence:
;      tag_val=gt_tagval(item, tagname [,str_patt='pattern', struct=struct, $
;					level=level, found=found)
;
;   Calling Examples:
;      tagval=gt_tagval(structure,'tagname',level=level)
;      tagval=gt_tagval(index,'periph',str_pattern='sxt',level=level)
;      tagval=gt_tagval(rmap ,'periph',str_pattern='sxt',level=level)
;
;   Method:
;      recursive for nested structures (just calls str_tagval)
;
;   Restrictions:
;     need to add case where item is FITS header (call WT routine)
;     
;   History:
;      Circa 1-dec-1995 S.L.Freeland  
;      15-aug-1996   Just make this a synonym for str_tagval 
;
;   Notes: Slightly more efficient to call 'str_tagval' directly - 
;          this is a 'gt' synonym for str_tagval.pro
;          (operation is generic for structures in many other applications)
;-
return, str_tagval(item, tag,$ 
           str_pattern=str_pattern,level=level, struct=struct,found=found)
end

