pro tv2, image, x00, y00, init=init, landscape=landscape, $
		window=window, already=already, ppinch=ppinch_in, $
		color=color, hwfont=hwfont, revcolor=revcolor
;+
;NAME:
;	tv2
;PURPOSE:
;	To allow a user to output an image to a PS device and
;	to specify the location (and size) in pixels.
;SAMPLE CALLING SEQUENCE:
;	tv2, image, x0, y0
;	tv2, xsiz_pix, ysiz_pix, /init
;	tv2, xsiz_pix, ysiz_pix, /init, window=0
;INPUTS:
;	image	- The byte scaled image to display 
;		  (except when /INIT is used -- in that case it is 
;		  the X size of the window in pixels)
;	x00	- The left corner pixel coordinate of the image
;		  (except when /INIT is used -- in that case it is
;		  the Y size of the window in pixels)
;	y00	- The lower corner pixel coordinate of the image
;OPTIONAL KEYWORD INPUT:
;	landscape - If set, output in postscript mode
;		  Has no effect when the output device is "X"
;	window	- The window number to create 
;		  Has no effect when the output device is "PS"
;	already	- Do not create the window if it already exists
;		  and is the proper size
;		  Has no effect when the output device is "PS"
;	ppinch	- Force the size of the output to be a fixed number
;		  of "pixels per inch".
;	color	- If set, issue the device commands for color
;		  Has no effect when the output device is "X"
;	hwfont	- If set, use hardware fonts
;		  Has no effect when the output device is "X"
;	revcolor - If set, set !color to 0 for device PS so that
;		  writing will be in black on white paper.  Set
;		  !color to 255 for device X so that writing is
;		  white on a black background.
;METHOD:
;	Since the output size of the window is defined in pixels,
;	TV2, XYOUTS2, and PLOTS2 can convert the pixel coordinates
;	into inches when sending the output to the postscript device.
;	The aspect ratios are all figured out to maintain proper
;	proportions
;HISTORY:
;	Written Feb-94 by M.Morrison
;	 9-Jun-94 (MDM) - 
;	10-Aug-94 (MDM) - Added ppinch optional input
;	 7-Apr-95 (MDM) - Added /COLOR and /HWFONT options
;-
;
common tv2_blk, xsiz_pix, ysiz_pix, xsiz_inch, ysiz_inch, ppinch
;
if (n_elements(x00) eq 0) then x0 = 0 else x0 = x00
if (n_elements(y00) eq 0) then y0 = 0 else y0 = y00
if (n_elements(window) eq 0) then win = 0 else win = window
;
if (keyword_set(init) and (n_elements(image) eq 0)) then begin
    print, 'TV2 Error: The X window size in pixels has not been defined.  Using 512'
    image = 512
end
if (keyword_set(init) and (n_elements(x00) eq 0)) then begin
    print, 'TV2 Error: The Y window size in pixels has not been defined.  Using 512'
    x00 = 512
end
;
if (!d.name ne 'PS') then begin
    if (keyword_set(init)) then begin
	xsiz_pix = image
	ysiz_pix = x00
	wdef, win, xsiz_pix, ysiz_pix, already=already
	if (keyword_set(hwfont)) then !p.font=-1
	if (keyword_set(revcolor)) then !color=255	;write in white on black background
    end else begin
	tv, image, x0, y0
    end
end else begin
    if (keyword_set(init)) then begin
	xsiz_pix = image
	ysiz_pix = x00
	xpage = 7.0
	ypage = 9.5
	xoffs = 0.75
	yoffs = 1.
	qland = 0
	if (keyword_set(landscape)) then begin
	    qland = 1
	    ypage = 7.0
	    xpage = 9.5
	    xoffs = 0.75
	    yoffs = xpage+0.75
	end
	if (n_elements(ppinch_in) eq 0) then ppinch = (xsiz_pix/xpage) > (ysiz_pix/ypage) $	;pixels per inch
					else ppinch = ppinch_in
	xsiz_inch = xsiz_pix/ppinch
	ysiz_inch = ysiz_pix/ppinch
	;
	;;device, /inches, xsize=xsiz_inch, ysize=ysiz_inch, xoffset=xoffs, yoffset=yoffs, portrait=1-qland, land=qland
	if (keyword_set(landscape)) then begin
	    device, /inches, xsize=xsiz_inch, ysize=ysiz_inch, xoffset=xoffs, yoffset=yoffs, /land
	end else begin
	    device, /inches, xsize=xsiz_inch, ysize=ysiz_inch, xoffset=xoffs, yoffset=yoffs, /portrait
	end
	;
	if (keyword_set(color)) then device, /color, bits=8, /helvetica
	if (keyword_set(hwfont)) then !p.font=0
	if (keyword_set(revcolor)) then !color=0	;write in black on white paper
    end else begin
	xi0 = x0 / ppinch
	yi0 = y0 / ppinch
	xisiz = n_elements(reform(image(*,0))) / ppinch
	yisiz = n_elements(reform(image(0,*))) / ppinch
	tv, image, xi0, yi0, xsiz=xisiz, ysiz=yisiz, /inch
    end
end
;
end
