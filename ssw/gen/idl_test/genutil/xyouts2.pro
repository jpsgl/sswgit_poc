pro xyouts2, x00, y00, str, size=size0, orientation=orientation0, $
			alignment=alignment0, device=device, color=color0
;
;
;	xyouts2, x0, y0, str
;
;	/device does not mean anything - xyouts2 units are always in window pixel device units
;
common tv2_blk, xsiz_pix, ysiz_pix, xsiz_inch, ysiz_inch, ppinch
;
if (n_elements(x00) eq 0) then x0 = 0 else x0 = x00
if (n_elements(y00) eq 0) then y0 = 0 else y0 = y00
if (n_elements(str) eq 0) then str = ' '
if (n_elements(size0) eq 0) then size = 1 else size = size0
if (n_elements(orientation0) eq 0) then orientation = 0 else orientation = orientation0
if (n_elements(alignment0) eq 0) then alignment = 0 else alignment = alignment0
if (n_elements(color0) eq 0) then color=!color else color=color0
;
if (!d.name ne 'PS') then begin
    xyouts, x0, y0, str, size=size, orientation=orientation, alignment=alignment, /device, color=color
end else begin
    xi0 = x0 / float(xsiz_pix) * !d.x_size
    yi0 = y0 / float(ysiz_pix) * !d.y_size
    xyouts, xi0, yi0, str, /device, size=size, orientation=orientation, alignment=alignment, color=color
end
;
end
