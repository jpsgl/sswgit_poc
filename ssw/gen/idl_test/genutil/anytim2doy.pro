function anytim2doy, tim_in, year=year, qstop=qstop
;
;+
;NAME:
;	anytim2doy
;PURPOSE:
;	Given a time in the form of a (1) structure, (2) 7-element time
;	representation, or (3) a string representation, 
;	return the day of the year for that day
;CALLING SEQUENCE:
;	xx = anytim2doy(roadmap)
;	xx = anytim2doy('12:33 5-Nov-91')
;INPUT:
;	tim_in	- The input time
;		  Form can be (1) structure with a .time and .day
;		  field, (2) the standard 7-element external representation
;		  or (3) a string of the format "hh:mm dd-mmm-yy"
;OPTIONAL KEYWORD OUTPUT:
;	year	- The year for the time(s) passed in
;HISTORY:
;	Written 7-Jun-92 by M.Morrison
;	11-Jan-94 (MDM) - Updated the header information
;-
;
daytim = anytim2ints(tim_in)
timarr = anytim2ex(tim_in)
year = timarr(6,*)
daytim_jan1 = anytim2ints('1-jan-' + strtrim( year, 2) )
;
doy = daytim.day - daytim_jan1.day + 1
;
return, doy
end
