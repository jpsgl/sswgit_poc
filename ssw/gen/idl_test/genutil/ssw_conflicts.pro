pro ssw_conflicts, pattern, conffile=conffile, _extra=_extra, $
   full=full, conflicts=conflicts , debug=debug
;+
;   Name: ssw_conflicts
;
;   Purpose: check input file , or pattern against SSW online routines
;
;   Input Parameters:
;   
;   Keyword Parameters:
;      conffile     - optional conflict file name 
;      full         - if set, do a full listing
;      conflicts    - if set, only show conflicts
;
;   Calling Sequence:
;      ssw_conflicts,/xxx		; Routines under instrument XXX 
;      ssw_conflicts,'pattern'          ; check SSW files matching pattern
;
;   History:
;      31-oct-1996 - derive from chk_conflict 
;  
;   Method:
;      Call file_diff(/idlpro) to differentiate CODE & HEADER differences
;
;-
case 1 of 
   keyword_set(_extra) and n_elements(pattern) eq 0: begin
      pattern=strrep_logenv(get_logenv('SSW_'+(tag_names(_extra))(0)),'SSW')
      conffile=concat_dir('SSW_SITE_LOGS', $
         str_replace(strmid(pattern,1,100),'/','_')) + '.conflicts'
      sswloc,pattern,tfile
   endcase 
   file_exist(pattern(0)): tfile=pattern
   n_elements(pattern) gt 1: tfile=pattern
   else: sswloc,pattern,tfile
endcase

pros=strsplit(tfile,'/',/last,/tail)
npros=n_elements(pros)

statmess  =strarr(npros)
cstatus    =intarr(npros)
sswconf   =strarr(npros)

for i=0,n_elements(tfile) -1 do begin
  sswloc, '/'+pros(i), sswfiles, count,/quiet
  sswfile=''
  case 1 of 
     count eq 0: mess="Not in SSW MAP"
     count eq 1: mess="No conflicts 
     else: begin
        sswfiles=sswfiles(rem_elem(sswfiles,tfile(i)))  ; remove THIS one
        diff=file_diff(tfile(i),sswfiles(0), mess=mess, status=status,/idlpro)
        sswfile=sswfiles(0)
     endelse
  endcase
  cstatus(i)=count
  statmess(i)= mess
  sswconf(i)=sswfile  
  if keyword_set(full) or count gt 1 then $
      more,tfile(i) + ' NConflicts: ' + strtrim(count,2) + ' ' + mess
  if count gt 1 and keyword_set(debug) then stop
endfor

conflicts =where(cstatus gt 1,nconf)

omess=strjustify(tfile) + '  ' + strjustify(sswconf) + $ 
      string(cstatus,format='(i3)') + '  ' + strjustify(statmess)

if data_chk(conffile,/string) then begin
   message,/info,"Writing results to: " + conffile
   pr_status,status,/idldoc, caller='ssw_conflicts'
   file_append,conffile,status,/new
   file_append,conffile,"; Number of files checked: " + strtrim(npros,2)
   file_append,conffile,";     Number of conflicts: " + strtrim(nconf,2) 
   file_append,conffile,"; ----------------------------------------------"
   if keyword_set(full) then ss=lindgen(npros) else ss=conflicts
   file_append,conffile,omess(ss)
endif

return
end
