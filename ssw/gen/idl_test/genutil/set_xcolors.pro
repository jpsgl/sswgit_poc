pro set_xcolors
;+
;   Name: set_xcolors 
;
;   Purpose: part of idl setup - avoid order dependent X call effects 
;
;   History: slf, 3-Apr-1992
;	     slf, 9-Aug-1992 - verify DISPLAY is defined 
;            slf,  1-nov-96  - 24 bit support check SSW_X_DEPTH,X_COLORS,X_RETAIN
;                              use device,decompose=0 
;            slf,  7-nov-96  - no action if remote DISPLAY 
;-
; avoid execution order dependent window/widget behavior

if !d.name ne 'X' then return           ; early exit if not X windows
if get_logenv('ssw_nox') ne '' then return ; likewise if NOX set
on_error,2
version= strlowcase(!version.os) 
colors=fix(getenv('ys_ncolors'))        ; original, backwardly compatible
display=getenv('DISPLAY')               ; check DISPLAY environment/log

; ---------- check for environmental override ----------
depth  =     get_logenv('SSW_X_DEPTH')
retain = fix(get_logenv('SSW_X_RETAIN'))
xcolors= fix(get_logenv('SSW_X_COLORS'))
pseudo =     get_logenv('SSW_X_PSEUDO')
;--------------------------------------------------------

colors=colors > xcolors                        ; YS or SSW backwardly compatible
eight_bit=depth eq '8'
localx=strlen(display) le 4  and display ne '' ; '', :0, or .0:0 assumed local 

if not localx then return                    ; dont bother if remote

;if depth eq '' and version ne 'vms' then begin
;   spawn,'xdpyinfo',xinfo,/noshell                
;   depth=where(strpos(xinfo,'depths') ne -1,dcnt)  
;   if dcnt gt 0 then begin
;     depths=strtrim(str2arr(xinfo(depth(0)),' '),2)
;     eight_bit=depths(n_elements(depths)-1) eq '8'
;   endif     
;endif  
;  
case eight_bit and localx of  
      colors gt 0:				; environmental used
      version eq 'ultrix': colors=241
      version eq 'irix'  : colors=235
      version eq 'sunos' : colors=241
      else: 				        ; standard default
endcase

case 1 of
   colors gt 0: begin
      window,/free,xs=2,ys=2,/pix,colors=colors
      wdelete 
   endcase
   not eight_bit: begin
      window,/free,xs=2,ys=2,/pix
      wdelete
   endcase
   else:
endcase

if pseudo then device,pseudo=8 else device,decompose=0  ; permit 24 to look like 8
if retain gt 0 then device,retain=retain

return
end
