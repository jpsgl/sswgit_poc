pro ssw_upgrade, _extra=_extra, site=site, $
                 outdir=outdir, outpackage=outpackage, $
		 nopackage=nopackage,                  $
		 nospawn=nospawn, noexecute=noexecute, $
		 local_sets=local_sets, remote_sets=remote_sets, $
		 debug=debug
;+
; Name: ssw_upgrade
;
; Purpose: generate SSW set list, generaate packages and spawn mirror job
;
; Keyword Parameters:
;   SITE - if set, include $SSW/site (only on request)
;   XXX  - ex: /eit, /mdi, /sxt, /trace.. update only specified instruments
;                (if none specified , list is derived from $SSW_INSTR)
;   outdir -     local path to keep mirror files (def=$SSW_SITE_MIRROR)
;   outpackage - name of mirror file (def='ssw_upgrade.mirror')
;   no package - if set, do not generate new package 
;   nospawn -   if set, do not spawn/execute mirror (ex: just make package)
;   noexecute - synonym for nospawn
;   remote_sets (output) - list of remote sets/paths (on SSW master)
;   local_sets  (output) - list of local ( relative to $SSW)
;           
; Calling Examples:
;   ssw_upgrade,/eit,/sxt  - update specified instrument trees
;   ssw_upgrade            - update trees implied by $SSW_INSTR
;   ssw_upgrade,/site      - same but update SITE (! warning)
;   ssw_upgrade,/nospawn   - generate package but dont spawn mirror
;   ssw_upgrade, remote=remote, local=local,/nospawn,/nopackage - return paths
;
; Restrictions:
;   for now, all local in terms of local $SSW (no split instrument trees)
;   Assume SSW, Perl and Mirror installed on local machine
;-
debug=keyword_set(debug)
nospawn=keyword_set(nospawn) or keyword_set(noexecute)

sswtop='/solarsoft'                                        ; host tree top
ssw_host=(get_logenv('ssw_mirror_site'))(0)                ; defult=sohoftp

if ssw_host eq '' then ssw_host='sohoftp.nascom.nasa.gov'

multi_miss=str2arr('yohkoh,soho,smm')

; Optionally generate Instrument list via keyword inheritance
if keyword_set(_extra) then begin
   instr=strlowcase(str_replace(tag_names(_extra),'SSW_',''))
endif else instr=   str2arr(get_logenv('SSW_INSTR'),' ')

; ---------- prepare the lists (remote and local SSW pathames) ------------
allinstrx=str2arr(get_logenv('SSW_INSTR_ALL'),' ')
allmiss =strsplit(allinstrx,'/',tail=allinstr)
missions=str2arr(get_logenv('SSW_MISSIONS'),' ')

; protect against unexpected environmentals (missions ne '')

for i=0,n_elements(missions)-1 do set_logenv,missions(i),'',/quiet

ss=where_arr(allinstr,instr, count)                       ; map local->remote
gensets=['gen']                                           ; implied GEN trees
if count gt 0 then insets=concat_dir(allmiss(ss),allinstr(ss))            ; instruments
mm=where_arr(multi_miss,allmiss(ss),count)
if count gt 0 then gensets=[gensets,concat_dir(multi_miss(mm),'gen')]     ; implied mission GEN
gensets=gensets(uniq(gensets,sort(gensets)))
allsets=[gensets,insets]  
allsets=allsets(uniq(allsets,sort(allsets)))              ; uniq list
;------------------------------------------------------------------------

if keyword_set(site) then allsets=['site',allsets]  ; add site only on req.

; define local and remote                           ;ouput and for mirror 
remote_sets=concat_dir(sswtop,allsets)
local_sets =concat_dir('SSW',allsets)

; ----------------- generate mirror package -------------------------
if not keyword_set(nopackage) then begin
   if not keyword_set(outpackage) then outpackage='ssw_upgrade.mirror' 
   break_file,outpackage,ll,pp,ff,ee,vv    
   if not keyword_set(outdir) and file_exist(pp) then $
      outdir=pp else outdir='SSW_SITE_MIRROR'
   if not file_exist(outdir) then begin
      message,/info,'No directory $SSW/site/mirror - you might want to create one'
      outdir='SSW_SITE_SETUP'
   endif     
   pfile=concat_dir(outdir,ff+ee+vv)
   make_mirror,ssw_host,remote_sets,local_sets, $
      comment='ssw_upgrade_'+ str_replace(remote_sets,'/','_'), $
      mirror_file=pfile,/mode_copy,use_timelocal='false'
endif

; --------------- spawn mirror (do the update) ----------------------
if not keyword_set(nospawn) then begin
   mircmd='mirror ' + pfile
   message,/info,"Spawning mirror cmd: " + mircmd
   spawn,mircmd,result
endif  

if debug then stop
return
end
