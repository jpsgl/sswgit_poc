pro ucon_path, yohkoh=yohkoh, soho=soho, ssw=ssw, $
	add=add, remove=remove, quiet=quiet, prepend=prepend
;+
;   Name: ucon_path
;
;   Purpose: manage ucon portions of SSW path
;
;   Calling Sequence:
;      ucon_path, [/yohkoh, /soho, /ssw, /add, /remove, /quiet]
;
;   Input Paramters:
;      NONE:
;
;   Keyword Parameters:
;      yohkoh - switch , if set, yohkoh ucon path is target (default)
;      soho   - switch , if set, soho ucon path is target
;      sss    - switch , if set, ssw  ucon path is target
;      add    - switch , if set, add the associated ucon paths
;      remove - switch , if set, remove the associated ucon paths
;
;   Side Effects:
;      The IDL !path variable may be changed
;
;   Category:
;      system, environment, IDL
;
;   Method:
;      may call ssw_path and/or pathfix
;
;   History:
;      21-Feb-1996 S.L. Freeland
;      29-feb-1996 S.L. Freeland
;       9-oct-1996 S.L. Freeland - add hudson
;-

soho=keyword_set(soho)
ssw=keyword_set(ssw)
yohkoh=keyword_set(yohkoh) or (1-soho and 1-ssw)
quiet=keyword_set(quiet)


rempath=''
addpath=''
; SSW users who have ucon routines referenced by 'outside' routines 

ext_ucon=concat_dir(get_logenv('$SSW_SITE_SETUPD'),'ext_ucon')  ; file via ssw monitor job

if file_exist(ext_ucon) then begin
   ucon=rd_tfile(ext_ucon)
   yg=ucon(wc_where(ucon,'ys'))
endif else begin
   yg=['acton','bentley','freeland','hudson','labonte','lemen','mcallister','sato']
   yg=[yg,'mctiernan','metcalf','morrison','schwartz','slater','wuelser','zarro']
endelse

root=concat_dir('ucon','idl')			; assumption for all 

case 1 of 
   yohkoh: begin
      addpath=concat_dir(concat_dir('$ys',root),yg)
      rempath=addpath
   endcase
   soho: message,/info,"No SOHO ucon areas yet defined..."
   ssw:  message,/info,"No SSW ucon areas yet defined..."
   else:
endcase

remove=keyword_set(remove) and rempath(0) ne ''
add=keyword_set(add) or (addpath(0) ne '' and 1-remove)

nadd=n_elements(addpath)
nrem=n_elements(rempath)


case 1 of 
   add: ssw_path, addpath, quiet=quiet, prepend=prepend
   remove: ssw_path,'$ys' + strmid(root,4,1) + root,/remove
   else: if not quiet then message,/info,"No changes to UCON areas"
endcase

return
end

