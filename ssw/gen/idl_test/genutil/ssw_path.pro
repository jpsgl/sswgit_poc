pro ssw_path, path, soho=soho, yohkoh=yohkoh, $
   _extra=_extra, remove=remove, test=test, prepend=prepend, $
   show=show, inquire=inquire, quiet=quiet, $
   save=save, restore=restore, ucon=ucon, more=more, ops=ops
;+
;   Name: ssw_path
;
;   Purpose: add or remove SSW elements from IDL path
;
;   Input Parameters:
;      path - optional path to add (if not an SSW instrument)
;
;   Keyword Parameters:
;      remove  - switch, if set, remove (default is to ADD paths)
;      test    - switch, if set, add/remove the TEST directory (def=idl)
;      prepend - switch, if set, Prepend the paths (default is append to SSW paths)
;      quiet   - switch, if set, suppress some messages
;      show/inquire switch, if set, print current !path to screen and exit
;      more    - switch, if set, same as SHOW with MORE-like behavior
;      save    - switch, if set, store current !path prior to change
;      restore - switch, if set, restore !path saved with /save (previous call)
;      ops     - switch, if set, $SSW/MMM/ops/III/idl (ops software)
;
;      <instrument> - switch, any SSW instrument [/eit,/sxt,/cds,/mdi,/bcs...]
;                     (may use multiple instrument switches in single call)
;
;   Category: environment, system
;
;   Calling Sequence:
;      ssw_path,/instrument [,/instrument ,/remove]
;
;   Calling Examples:
;      ssw_path,/show		   ; display current !path
;      ssw_path,/eit,/sxt	   ; append  EIT and SXT paths
;      ssw_path,/eit,/sxt,/remove  ; remove  EIT and SXT paths
;      ssw_path,/cds,/prepend	   ; prepend CDS paths
;      ssw_path,/save,/sumer	   ; save current !path, then add SUMER
;      ssw_path,/restore	   ; restore saved !path and exit
;      ssw_path,/ucon,/yohkoh	   ; add "selected" Yohkoh ucon areas 
;				   ; (only those UCON with external references)
;      ssw_path,/ops,/cds	   ; deal with OPS SW (instead of ANAL)
;
;   Side Effects:
;      *** Updates IDL !PATH *** 
;
;   History:
;      15-Jun-1995 (SLF)
;      15-Feb-1996 (S.L.Freeland) - major overhaul, keywords added, etc
;      13-mar-1996 (S.L.Freeland) - add /OPS keyword and function
;      28-May-1996 (S.L.Freeland) - prepend atest if present
;      18-aug-1996 (S.L.Freeland) - add SPARTAN
;      29-aug-1996 (S.L.Freeland) - status message (order changed?) , add TRACE
;
;   Common Block:
;      ssw_path_private1 - for !path when called with /save and /restore
;      
;   Method:
;      uses keyword inheritance for Instrument keywords 
;      uses <expand_path> for additions
;
;   Notes:
;      adding an Instrument tree also adds the associated Mission tree
;-
; common for save and restore function
common ssw_path_private, save_path
save=keyword_set(save)
restore=keyword_set(restore)
; restore function
if restore then begin
   if n_elements(save_path) eq 0 then begin
      message,/info,"No !path saved yet, returning..."
   endif else begin
      message,/info,"Restoring !path from last save..."
      !path=save_path
   endelse
   return						; early exit...
endif
quiet=keyword_set(quiet)

if save then begin
   if not quiet then $
      message,/info,"Saving !path (before changes); use IDL> ssw_path,/restore to recover"
   save_path=!path				   ; save !path on request
endif

; system dependent !path delimiter
pdelim=([':',','])(strupcase(!version.os) eq 'VMS')	; function!!!
pdelim=([pdelim,';'])(strpos(!path,';') ne -1)   
fdelim=get_delim()
oldpath=str2arr(!path,pdelim)			 	; save contents
nold=n_elements(oldpath)

ssw=get_logenv('SSW')
show=keyword_set(show) or keyword_set(inquire) or keyword_set(print) or keyword_set(more)

if show then begin
   prstr,strrep_logenv(str2arr(!path,pdelim),'SSW'),nomore=1-keyword_set(more)
   message,/info,"Number of current paths: " + strtrim(nold,2)
   return
endif

instr=''
if not keyword_set(path) then path=''
if n_elements(_extra) eq 1 then $ 
   instr=tag_names(_extra)
sinstr=ssw_instruments(/soho)
yinstr=ssw_instruments(/yohkoh) 
spinstr=ssw_instruments(/spartan)
trinstr=ssw_instruments(/trace)

imap=strupcase([sinstr,yinstr,spinstr,trinstr])
mmap=strupcase([replicate('soho',n_elements(sinstr)),$
                replicate('yohkoh',n_elements(yinstr)), $
                'SPARTAN','TRACE'])

soho=keyword_set(soho)
yohkoh=keyword_set(yohkoh)
spartan=keyword_set(spartan)

remove=keyword_set(remove)

if soho   then instr=[instr,sinstr]
if yohkoh then instr=[instr,yinstr]
if spartan then instr=[instr,spinstr]

case 1 of 
   n_params() gt 0:					; optional input path
   keyword_set(ucon): begin
     ucon_path, yohkoh=yohkoh, soho=soho, remove=remove, prepend=prepend
     return						;!! unstructured exit
   endcase

   n_elements(instr) eq 1 and instr(0) eq '' and not keyword_set(save): begin
      ssw_path,/show					;!! recurse
      return						;!! unstructured exit
   endcase
   soho and remove: begin				;!! *** fix this
      remain=where(strpos(oldpath,fdelim+'soho'+fdelim) eq -1,rcnt)
      !path=arr2str(oldpath(remain),pdelim)
      return
   endcase
   yohkoh and remove: begin				;!! *** fix this
      remain=where(strpos(oldpath,fdelim+'yohkoh'+fdelim) eq -1,rcnt)
      !path=arr2str(oldpath(remain),pdelim)
      return						;!! unstructured exit
   endcase
   instr(0) eq '': instr=instr(1:*)
   else:
endcase

instr = strupcase(instr)

ipat=''
mpat=''

; ********** can be made more concise after ssw_instrument update *****
for i=0,n_elements(instr)-1 do begin
   pattern=instr(i)
   mission=wc_where(imap , '*'+pattern+'*' ,pcnt)
   if pcnt eq 0 then begin
      message,/info,"Instrument: " + pattern(0) + " not recognized, returning..." 
         return
   endif else begin
      mpattern=mmap(mission(0))
      mpat=[mpat,mpattern]
      ipat=[ipat,pattern]
   endelse
endfor

if n_elements(ipat) gt 1 then begin
   ipat=ipat(1:*)
   mpat=mpat(1:*)
endif
; *****************************************************************

delim=(['/','.'])(strupcase(!version.os) eq 'VMS')
proc= (['strlowcase','strupcase'])(strupcase(!version.os) eq 'VMS')
ipat=call_function(proc,ipat)
mpat=call_function(proc,mpat)

if keyword_set(remove) then begin
;  use existing <pathfix.pro> for removal
   if ipat(0) eq '' and path ne '' then ipat=path
   for i=0,n_elements(ipat)-1 do begin
      if strpos(ipat(i),'$') eq 0 then begin
         env=strsplit(ipat(i),delim,/head,tail=tail)
         pat=concat_dir(get_logenv(env),tail) + delim + '*'
      endif else pat=delim + '*' + ipat(i) + delim + '*'
      pathfix,pat,/quiet
   endfor
endif else begin
   newpath=path
   addinst=ipat(0) ne '' 
   if addinst then begin
;     Add SSW Mission generic (GEN) tree 
      for i=0,n_elements(mpat)-1 do begin
         genpath=concat_dir(concat_dir(ssw,mpat(i) ),'gen')
         if mpat(i) eq 'yohkoh' then genpath = concat_dir(get_logenv('ys'),'gen')
         newpath=[newpath,concat_dir(genpath,(['idl','test'])(keyword_set(test)))]
      endfor

;     Add SSW Instrument tree
      for i=0,n_elements(ipat)-1 do begin
         if keyword_set(ops) then $
            top=concat_dir(concat_dir(concat_dir(ssw,mpat(i)),'ops'),ipat(i) ) else $
            top=get_logenv('SSW_'+strupcase(ipat(i)))
         if top eq '' then top=concat_dir(ssw,concat_dir(mpat(i),ipat(i)) )
         newpath=[newpath,concat_dir(top,(['idl','test'])(keyword_set(test)))]
      endfor
   endif
   np=where(newpath ne '',npcnt)
   if npcnt gt 0 then newpath=newpath(np)
   if npcnt eq 0 then message,/info,"No new paths..." else begin
      for i=0,n_elements(newpath)-1 do pathfix,newpath(i),/remove,/quiet
      sswp=where(strpos(oldpath,ssw) ne -1,sswcnt)       ; SSW part of !path
;     generate new path array

      nps=strarr(npcnt*2)
      nps( indgen(npcnt)*2)=concat_dir(newpath,'atest')
      nps( indgen(npcnt)*2 +1)=newpath
      newpath=nps
      new=expand_path('+' + arr2str(newpath,pdelim+ '+'),/array,count=ncount)

;
      sswpaths=''		; SSW part
      trailer=''		; post SSW part
      header=''			; pre  SSW part

      if sswcnt gt 0 then begin
         if sswp(0) ne 0 then header=oldpath(0:sswp(0)-1)
         if sswp(sswcnt-1) ne n_elements(oldpath) then trailer=oldpath(sswp(sswcnt-1)+1:*) 
         sswpaths=oldpath(sswp(0):sswp(sswcnt-1))
      endif

      case 1 of 
         keyword_set(prepend): sswpaths=[new,sswpaths]
         else: sswpaths=[sswpaths,new]
      endcase

;     new path array and cleanup
      outarr=[header,sswpaths,trailer]
      outarr=outarr(where(strtrim(outarr,2) ne ''))
      outarr=outarr(uniqo(outarr))
      if not quiet then begin
         if new(0) ne '' then prstr, /nomore, $
            ['Including Paths:',strjustify(str_replace(new,ssw,'$SSW'),/box)] else $
            message,/info,"No matches, nothing added..."
      endif
      !path=arr2str(outarr,pdelim)		; **** update !path ***
   endelse
endelse

newpath=str2arr(!path,pdelim)			 ; save contents
nnew=n_elements(newpath)
case 1 of
   nnew eq nold: begin
     changed=where(newpath ne oldpath,ccnt)
     mess=(['Path ORDER changed ','Path not changed ']) (ccnt eq 0) + $
              "Number of paths: "+strtrim(nnew,2)
   endcase
   else: mess="Number of paths changed from " + strtrim(nold,2) + " to "   + strtrim(nnew,2)
endcase

if not quiet then message,/info,mess

return
end      


