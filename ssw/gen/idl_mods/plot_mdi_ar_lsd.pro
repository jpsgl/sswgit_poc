pro plot_mdi_ar_lsd,tstart,tend,gsize=gsize,back=back, $
     file=file,gif=gif,map=map,wave=wave,path=path,reuse=reuse,$
     nar=nar,quiet=quiet,err=err,rotate=rotate,last=last,_extra=extra,$
     limb=limb,grid=grid,noimg=noimg,charsize=charsize,charthick=charthick

;flatfield_file = "/archive/soho/public/data/synoptic/sunspots/mdi_flat_mar2005.fits"
flatfield_file = "/archive/soho/public/data/synoptic/sunspots/mdi_flat_2008.fits"

on_error,1

print, 'DIR_GEN_NAR: ', getenv('DIR_GEN_NAR')

common plot_mdi_point,last_map
quiet=keyword_set(quiet)
loud=1-quiet
limb_only=keyword_set(noimg)

;-- check time inputs

t1=get_def_times(tstart,tend,dend=t2)
t1=anytim2tai(t1)
help, t1
if keyword_set(tstart) then begin
  print, 'Resetting t1'
  t1 = anytim2tai(tstart)
endif
help, t1
which, 'get_def_times', /all

;-- make GIF file?

do_gif=0
if keyword_set(gif) then begin
 dcode=date_code(t1)
 if datatype(gif) eq 'STR' then gfile=gif else $
  gfile=concat_dir(curdir(),'sunspots_'+dcode+'.jpg')
 break_file,gfile,dsk,dir,name
 outdir=dsk+dir
 if trim(outdir) eq '' then outdir=curdir()
 if not test_dir(outdir) then return
 do_gif=1
endif
         
;-- check for FITS file input

if not limb_only then begin

 reuse=keyword_set(reuse) or keyword_set(last)
 if keyword_set(reuse) and valid_map(last_map) then map=last_map


 if not valid_map(map) then begin
  if datatype(file) eq 'STR' then fits2map,file,map,err=err
 endif

;-- if FITS file or map structure not input, check SUMMARY and PRIVATE
;   locations for latest MDI files.
;   If no files found, then just plot limb

 err=''
 if not valid_map(map) then begin
  mdi_file=get_recent_mdi(t1,count=count,/quiet,err=err,$
                         back=back,path=path)

  if err eq '' then begin
   if loud then message,'using MDI file -> '+mdi_file,/cont
;   fits2map,mdi_file,map,err=err,out=512
   h = headfits(mdi_file)
   if err ne '' then message,err,/cont
  endif
 endif

 if valid_map(map) then last_map=map 

endif

;-- Apply limb darkening correction (Luis S�nchez, 2001-07-17)
;-- Apply correction for spacecraft roll attitude (Luis Sanchez, 2010-11-04)

naxis = sxpar (h, 'naxis1')
angle = sxpar (h, 'crot')
xyz = [sxpar(h,'crpix1'), sxpar(h,'crpix2'), sxpar(h,'radius')]
fits2map, mdi_file, map, err=err, out=naxis
map.data = (map.data/10.)^2.
if (n_elements(flatfield) eq 0) then begin
  flatfield = rfits(flatfield_file)
  flatfield = rebin(flatfield, naxis, naxis)
endif
map_data = map.data*flatfield
darklimb_correct, map.data, temp_img, limbxyr=xyz, lambda=6767
map.data = rot(temp_img, angle, cubic=-0.5, missing=min(temp_img))

;-- if making GIF file use a Z-buffer

dsave=!d.name
xsize=512 & ysize=512
ncolors=!d.table_size
if not exist(gsize) then zsize=[xsize,ysize] else $
 zsize=[gsize(0),gsize(n_elements(gsize)-1)]

if do_gif then begin
 set_plot,'z'

 device,/close,set_resolution=zsize,set_colors=ncolors,$
  set_character_size=[5,5]
endif

                               
;-- plot limb if requested or input map is not valid

if not valid_map(map) then limb_only=1b else rtime=map.time
if limb_only then begin
 plot_helio,t1,grid=grid
 over=2
endif else begin
 plot_map,map,xsize=zsize(0),ysize=zsize(1),rotate=0,/square,/noaxes,$
   _extra=extra,grid=grid,/date_only, dmax=1.2*max(map.data)
 over=1
endelse

;-- add Active Regions

do_nar=keyword_set(nar)
if do_nar then begin

;-- get NOAA data directly from SWPC

 nar=get_swpc(rtime,/unique,/nearest,count=count)
 if count gt 0 then nar=drot_nar(nar,rtime,count=count)
 if count gt 0 then oplot_nar,nar,charsize=charsize,charthick=charthick,font=-1,off=[0,40]

; if have_proc('get_nar') then begin
;  nar=call_function('get_nar',rtime,quiet=quiet,count=count,/nearest)
;  nar=call_function('get_nar',rtime,quiet=quiet,count=count,/nearest,/swpc)
;  if count gt 0 then nar=call_function('drot_nar',nar,rtime,count=count) 
;  if count gt 0 then oplot_nar,nar,charsize=charsize,charthick=charthick,font=-1,off=[0,40]
; endif

endif

if do_gif then begin
 temp=tvrd()
 jpegimg = bytarr(512,512,3)
 jpegimg(*,*,0) = byte(temp)
 jpegimg(*,*,1) = byte(temp)
 jpegimg(*,*,2) = byte(temp)
; jpegimg(*,*,1) = byte(temp)-byte(temp*.7)
; jpegimg(*,*,2) = 0
  tvlct,rs,gs,bs,/get
 device,/close
 message,'Writing GIF file to -> '+gfile,/cont
; mdi_color='$SYNOP_DATA/mdi/mdi_colors.sav'
; chk=loc_file(mdi_color,count=count)
; if count gt 0 then begin
;   restore,file=mdi_color
; endif
; write_gif,gfile,temp,r,g,b
; write_gif,gfile,temp,rs,gs,bs
 write_jpeg, gfile, jpegimg, quality=85, true=3
 set_plot,dsave
endif

return & end


