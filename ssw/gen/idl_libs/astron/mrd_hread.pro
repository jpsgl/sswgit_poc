pro mrd_hread, unit, header, status 
;+
; NAME: 
;	MRD_HREAD
;
; PURPOSE: 
;       Reads a FITS header from an opened disk file or Unix pipe
;	Like FXHREAD but works with compressed Unix files,
;
; CALLING SEQUENCE: 
;	MRD_HREAD, UNIT, HEADER  [, STATUS ]
; INPUTS: 
;	UNIT	= Logical unit number.
; OUTPUTS: 
;	HEADER	= String array containing the FITS header.
; OPT. OUTPUTS: 
;	STATUS	= Condition code giving the status of the read.  Normally, this
;		  is zero, but is set to !ERR if an error occurs, or if the
;		  first byte of the header is zero (ASCII null).
; RESTRICTIONS: 
;	The file must already be positioned at the start of the header.  It
;	must be a proper FITS file.
; SIDE EFFECTS: 
;	The file ends by being positioned at the end of the FITS header, unless
;	an error occurs.
; REVISION HISTORY:
;	Written,  Thomas McGlynn                     August 1995
;-
	block = strarr(36)
	block(*) = string(replicate(32b, 80))
		
	header = ' '
	w = [-1]

	while w(0) eq -1 do begin
		
		; Shouldn't get eof in middle of header.
		if eof(unit) then begin
			free_lun, unit
			status = -1
			return
		endif
		
		on_ioerror, error_return
		readu, unit, block
		on_ioerror, null
		
		w = where(strmid(block, 0, 8) eq 'END     ')
		if w(0) eq -1 then begin
			header = [header, block]
		endif else begin
			header = [header, block(0:w(0))]
		endelse
			
	endwhile
		
	header = header(1:*)
	status = 0
	return
error_return:
        status = -1
	return
end
			
