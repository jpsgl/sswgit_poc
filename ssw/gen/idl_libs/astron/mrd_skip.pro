pro mrd_skip, unit, nskip
;+
; NAME:
;	MRD_SKIP
; PURPOSE:
;	Skip a number of bytes from the current location in a file or a pipe
;	First tries using POINT_LUN and if this doesn't work, perhaps because
;	the unit is a pipe, MRD_SKIP will just read in the requisite number 
;	of bytes.
; CALLING SEQUENCE:
;	MRD_SKIP, Unit, Nskip
;
; INPUTS:
; 	Unit - File unit for the file or pipe in question, integer scalar
;	Nskip - Number of bytes to be skipped, positive integer
; NOTES:
; 	This routine should be used in place of POINT_LUN wherever a pipe
;	may be the input unit (see the procedure FXPOSIT for an example).  
;	Note that it assumes that it can only work with nskip >= 0 so it 
;	doesn't even try for negative values.    
;
; REVISION HISTORY:
;	Written, Thomas A. McGlynn    July 1995
;- 

	if nskip le 0 then return

	on_ioerror, try_pipe

        point_lun, -unit, curr_pos
	point_lun, unit, curr_pos+nskip
	on_ioerror, null
	return

try_pipe:

	on_ioerror, null

	buf = bytarr(nskip,/nozero)
	readu, unit, buf

	return
end
