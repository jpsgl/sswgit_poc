

pro xalert_event,event

widget_control, event.id, get_uvalue = uservalue
if not exist(uservalue) then uservalue=''
uservalue=trim(uservalue)

if uservalue eq 'close' then xkill,event.top

return & end

;--------------------------------------------------------------------------- 

pro xalert,instruct,wbase=wbase,title=title,group=group,ysize=ysize,$
           tfont=tfont,bfont=bfont,xsize=xsize

if not is_number(ysize) then ysize=3 
if not is_number(xsize) then xsize=10

if datatype(title) ne 'STR' then atitle=' ' else atitle=title
if datatype(instruct) eq 'STR' then mess=instruct else $
 mess='Add your alert instructions here'

val=[' ',mess,' ']

;-- update text if a live base

if xalive(wbase) then begin
 wtext=widget_info(wbase,/child)
 if widget_info(wtext,/name) eq 'TEXT' then begin
  widget_control,wtext,set_value=val
  xshow,wbase
  return
 endif
end

;-- create main base 

wbase=widget_mbase(title=atitle,/column,group=group)

wtext=widget_text(wbase,value=val,ysize=ysize,font=tfont,xsize=xsize)

row=widget_base(wbase,/row,/align_center) 
closeb=widget_button(row,uvalue='close',/frame,value='Close',font=bfont)

;-- realize & manage

xrealize,wbase,group=group,/center

xmanager,'xalert',wbase,/no_block

return & end

