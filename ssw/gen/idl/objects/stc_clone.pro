;+
; Project     : HESSI
;
; Name        : STC_CLONE
;
; Purpose     : Clone structures
;
; Category    : utility pointers objects
;
; Syntax      : IDL> dest=stc_clone(source)
;
; Inputs      : SOURCE = structure to clone (array or scalar) 
;
; Outputs     : DEST = cloned structure
;
; History     : Written 24 Aug 2000, K. Tolbert (STX), D. Zarro (EIT/GSFC)
;
; Contact     : dzarro@solar.stanford.edu
;-


function stc_clone,source,_extra=extra

if not exist(source) then return,-1
if size(source, /tname) ne 'STRUCT' then return, source

;-- copy top-level variables

dest=source

;-- following doesnt seem to recurse on pointers in nested structures

;struct_assign,source,dest 

;-- handle pointers, objects & structures recursively

np=n_elements(source)
for j = 0, n_tags(source)-1 do begin
 stype=size(source.(j), /tname)
 case stype of
  'POINTER': dest.(j)= ptr_clone(source.(j),_extra=extra)
  'OBJREF': dest.(j) = obj_clone(source.(j),_extra=extra)
  'STRUCT': dest.(j) = stc_clone(source.(j),_extra=extra)
  else: do_nothing=1
 endcase
endfor

if np eq 1 then dest=dest[0]
return, dest
end

