;+
; Project     : HESSI
;
; Name        : OVRO_DEFINE
;
; Purpose     : Define an OVRO data object
;
; Category    : Ancillary GBO Synoptic Objects
;
; Syntax      : IDL> c=obj_new('ovro')
;
; History     : Written 22 May 2000, D. Zarro, SM&A/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-
;-----------------------------------------------------------------------------
;-- init

function ovro::init,_ref_extra=extra

ret=self->site::init(_extra=extra)
if not ret then return,ret

ret=self->utplot::init(_extra=extra)
if not ret then return,ret

self->setprop,/fits,rhost='www.ovsa.njit.edu',ext=''

;-- useful pointers/objects for storage

self.index_ptr=ptr_new(/all)
self.data_ptr=ptr_new(/all)
self.fits_obj=obj_new('fits',/no_map)
self.feed=ptr_new(/all)
self->set,dim1_use=indgen(4),/positive,/sel_dim1

return,1

end

;-----------------------------------------------------------------------------

pro ovro::cleanup

self->utplot::cleanup
self->site::cleanup

free_var,self.index_ptr
free_var,self.data_ptr
free_var,self.fits_obj
free_var,self.feed

return & end

;------------------------------------------------------------------------------
;-- SET method

pro ovro::setprop,fits=fits,gif=gif,err=err,_extra=extra

;-- set file type and location to download

if keyword_set(fits) then begin
 ftype='*.fts' & topdir='/pub/data/fits'
endif

if keyword_set(gif) then begin
 ftype='*.gif' & topdir='/pub/data/gifs'
endif

self->site::setprop,ftype=ftype,topdir=topdir,_extra=extra,err=err

return & end

;-----------------------------------------------------------------------------
;-- get remote subdirectory id's based on file dates

function ovro::get_sdir

fids=self->site::get_sdir(/full,/no_day)

return,fids

end

;----------------------------------------------------------------------------
;-- driver to ftp files to $SYNOP_DATA

pro ovro::synop,_extra=extra

message,'copying OVRO synoptic data',/cont

;-- default settings

get_utc,utc
utc.time=0
self->setprop,tstart=utc,back=30,/verbose,/subdir,err=err,_extra=extra
if err ne '' then return

self->setprop,ldir='$SYNOP_DATA/lightcurves',/fits,/gzip,err=err

if err eq '' then self->copy

return & end

;----------------------------------------------------------------------------
;-- OVRO FITS reader

pro ovro::read,file,err=err,_ref_extra=extra,nodata=nodata,header=header

chk=loc_file(file,/verb,err=err,count=count)
if count eq 0 then return
if count ne 1 then begin
 err='Input file name must be scalar'
 message,err,/cont
 return
endif

;-- read file

if keyword_set(nodata) then begin
 self.fits_obj->read,file,extension=1,header=header,err=err,/nodata
 return
endif

self->fbreak,file,dir,new_file
delvarx,*self.data_ptr
self->empty
self.fits_obj->readfits,file,index,data,extension=1,_extra=extra,err=err,header=header
if not is_struct(data) then return
*self.data_ptr=temporary(data)
*self.index_ptr=temporary(index)
*self.feed=(*self.data_ptr).FEEDNUMBERANDPOLARIZATION
dim1_id=trim2(str_format((*self.data_ptr).freq_ghz,'(f5.1)')+' GHz ')
self->set,dim1_id=dim1_id
self->set_file,new_file

utbase=(*self.index_ptr).date_obs
utbase=anytim2utc(utbase,/vms)
self->set,utbase=utbase

return & end

;----------------------------------------------------------------------------
;-- retrieve current filename

function ovro::get_file

filename=self->get(/filename)

pos=strpos(filename,'-')
if pos eq -1 then return,filename

return,strmid(filename,pos+1,strlen(filename))

end

;----------------------------------------------------------------------------
;-- set new filename based on antenna choice

pro ovro::set_file,filename

if is_blank(filename) then filename=self->get_file()
if is_blank(filename) then return
antenna=fix(self->get(/antenna))
self->set,filename=trim(antenna)+'-'+filename
return

end

;-----------------------------------------------------------------------------
;-- select antenna/polarization option

pro ovro::options,group=group,title=title,cancel=cancel

cancel=0b
if not allow_windows() then return
if not self->has_data() then return
if not self->get(/sel_dim1) then return

;-- get new selections

self->select,group=group,title=title
cancel=self->get(/cancel)
if cancel then return

antenna=self->get(/antenna)
dim1_id=self->get(/dim1_id)
dim1_use=self->get(/dim1_use)
feed=self->get(/feed)
id='OVSA '+feed[antenna]

data=transpose((*self.data_ptr).channel_freq_time[antenna,*,*])

utbase=self->get(/ubase)
time=(*self.data_ptr).time_msec/1000.+anytim2tai(utbase)

;-- update UTPLOT object with new selection

self->set,xdata=temporary(time),ydata=temporary(data),/no_copy

dim1_unit='frequency (GHz)'
data_unit = 'Flux (SFU)'
self->set,dim1_use=dim1_use,dim1_id=dim1_id,dim1_unit=dim1_unit,data_unit=data_unit
self->set_file
self->set,/enab_sum,id=id

return & end

;--------------------------------------------------------------------------
;-- check if antenna has data

function ovro::check_antenna,antenna,err=err

err=''
if not is_number(antenna) then antenna=-1
feed=self->get(/feed)
nfeed=n_elements(feed)
if (antenna gt nfeed-1) or (antenna lt 0) then begin
 err='Antenna value out of range (0 - '+trim(nfeed-1)+')'
 message,err,/cont
 return,0b
endif

data=(*self.data_ptr).channel_freq_time[antenna,*,*]
chk=where(finite(data,/nan),count)
ndata=n_elements(data)
if count gt 0 then data[chk]=0
chk=where(data eq 0,count)
if count eq ndata then begin
 err='Warning. Zero data for '+feed[antenna]+'.'
 message,err,/cont
 return,0b
endif

(*self.data_ptr).channel_freq_time[antenna,*,*]=temporary(data)

return,1b
end

;---------------------------------------------------------------------------
;-- check if object has data

function ovro::has_data

if not ptr_valid(self.data_ptr) then return,0b
return,exist(*self.data_ptr) 

end

;---------------------------------------------------------------------------
;-- set method

pro ovro::set,antenna=antenna,_ref_extra=extra

if is_number(antenna) then begin
 feed=self->get(/feed)
 nfeed=n_elements(feed)
 self.antenna=0 > antenna < (nfeed-1)
endif

self->utplot::set,_extra=extra

return & end

;-----------------------------------------------------------------------

pro ovro::ant,event

widget_control,event.top,get_uvalue=self
antenna=event.index
self->set,antenna = antenna

return
end

;-----------------------------------------------------------------------

pro ovro::accept,event

widget_control,event.top,get_uvalue=self

wtext=self->get(/wtext)

wchan=self->get(/wchan)
s=widget_info(wchan,/list_select)
if s[0] eq -1 then begin
 widget_control,wtext,set_value='Select at least one channel.',/append
 return
endif else self->set,dim1_use=s

antenna=self->get(/antenna)
if not self->check_antenna(antenna,err=err) then begin
 widget_control,wtext,set_value=err,/append
 return
endif

self.cancel=0b
xkill,event.top 

return
end

;----------------------------------------------------------------------------
;-- antennae/polarization options

pro ovro::select,group=group,title=title

feed=self->get(/feed)
file=self->get_file()
bfont=self->get(/bfont)
lfont=self->get(/lfont)

base = widget_mbase (group=group,/column, $
                      title='OVSA Antennae/Polarization Options', $
                      /modal)


wbase=widget_base(base,/column,/frame)


if is_string(title) then begin
 label=widget_label(wbase,value=' ',font=lfont)
 label=widget_label(wbase,value=title,font=lfont)
 label=widget_label(wbase,value='  ',font=lfont)
endif

;-- antenna choices

temp=widget_base(wbase,/row)
label=widget_label(temp,value='Select Antenna and Polarization:',font=lfont)

want = widget_droplist (temp,font=bfont,$
                          value=' ' + feed + ' ',uvalue='ant')
widget_control,want,set_droplist_select=self->get(/antenna)

;-- channel choices

self->wchan,wbase

;-- message box

cbase=widget_base(wbase,/row,/align_center)
self.wtext=widget_text(cbase,ysize=4,xsize=50,font=lfont)

tbase=widget_base(base,/row,/align_center)
waccept = widget_button(tbase, value='Accept',uvalue='accept',font=bfont)
wcancel = widget_button(tbase, value='Cancel',uvalue='cancel',font=bfont)

xrealize,base,group=group,/center,/screen

widget_control,base,set_uvalue=self
xmanager,'self->select',base,event='obj_event'

return
end

;------------------------------------------------------------------------------
;-- OVRO site structure

pro ovro__define

self={ovro,data_ptr:ptr_new(),index_ptr:ptr_new(),fits_obj:obj_new(), $
      antenna:0,feed:ptr_new(),inherits site, inherits utplot}

return & end

