;+
; Project     : HESSI
;
; Name        : CACHE__DEFINE
;
; Purpose     : Define a cache object
;
; Category    : Ancillary GBO Synoptic Objects
;
; Syntax      : IDL> c=obj_new('cache')
;
; History     : Written 8 Apr 2000, D. Zarro, SM&A/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

;---------------------------------------------------------------------------
;-- constructor

function cache::init

return,1

end

;--------------------------------------------------------------------------
;-- destructor

pro cache::cleanup


return & end 


;--------------------------------------------------------------------------
;-- set properties

pro cache::set,tstart=tstart,tend=tend,name=name

if is_string(name) then self.name=trim(name)

err=''
dstart=anytim2tai(tstart,err=err)
if err eq '' then self.tstart=dstart

err=''
dend=anytim2tai(tend,err=err)
if err eq '' then self.tend=dend

t1=self.tstart & t2=self.tend
self.tend=t2 > t1
self.tstart= t1 < t2

return & end

;---------------------------------------------------------------------------
;-- show properties

pro cache::show

print,''
print,'CACHE properties:'
print,'----------------'
print,'% cache name: ',self.name

tstart=0. & tend=0.
if self.tstart gt 0 then tstart=anytim2utc(self.tstart,/vms)
if self.tend gt 0 then tend=anytim2utc(self.tend,/vms)

print,'% tstart: ',tstart
print,'% tend:   ',tend

return & end
               
 
;----------------------------------------------------------------------------
;-- set cache data

pro cache::setdata,data,times,verb=verb

cache_list,self.name,data,times,tstart=self.tstart,tend=self.tend,verb=verb,/set

return & end

;---------------------------------------------------------------------------
;-- get cache data

pro cache::getdata,data,times,verb=verb,count=count
 
cache_list,self.name,data,times,tstart=self.tstart,tend=self.tend,verb=verb,count=count

return & end

                                
;------------------------------------------------------------------------------
;-- define cache object

pro cache__define                 

temp={cache,name:'',tstart:0d,tend:0d}

return & end


