;+
; Project     : HESSI
;
; Name        : OMAP__DEFINE
;
; Purpose     : Define an object map
;
; Category    : imaging objects
;
; Syntax      : IDL> new=obj_new('omap')
;
; History     : Written 22 Nov 1999, D. Zarro, SM&A/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

;-------------------------------------------------------------------------

function omap::init,map

self->setmap,map

return,1

end

;------------------------------------------------------------------------

pro omap::help

temp={omap}
struct_assign,self,temp,/nozero
help,/st,temp

return
end

;-----------------------------------------------------------------------

pro omap::cleanup                ;--destroy map object

ptr_free, self.ptr
return
end

;-------------------------------------------------------------------

pro omap::set,_extra=_extra                 ;-- set map object values
  
if datatype(_extra) eq 'STC' then struct_assign,_extra,self,/nozero
      
return 
end

;-------------------------------------------------------------------

pro omap::get,xc=xc,yc=yc,dx=dx,dy=dy,roll_angle=roll_angle,roll_center=roll_center,$
              time=time,xrange=xrange,yrange=yrange,nx=nx,ny=ny,xp=xp,yp=yp,$
              soho=soho,drange=drange

if arg_present(xp) then xp=self->xp()
if arg_present(yp) then yp=self->yp()
if arg_present(xrange) then xrange=self->xrange()
if arg_present(yrange) then yrange=self->yrange()
if arg_present(nx) then nx=self.nx
if arg_present(ny) then ny=self.ny
if arg_present(xc) then xc=self.xc
if arg_present(yc) then yc=self.yc
if arg_present(dx) then dx=self.dx
if arg_present(dy) then dy=self.dy
if arg_present(roll_angle) then roll_angle=self.roll
if arg_present(roll_center) then roll_center=self.roll_center
if arg_present(time) then time=anytim2utc(self.time,/vms)
if arg_present(soho) then soho=self.soho
if arg_present(drange) then drange=self->drange()

return       
end

;-------------------------------------------------------------------

function omap::getmap

err=''
time=anytim2utc(self.time,err=err,/vms)
if err ne '' then begin
 message,'invalid time in map structure',/cont
 return,-1
endif

temp=rem_tag({omap},['ptr','nx','ny'])

struct_assign,self,temp,/nozero
return,make_map(*self.ptr,_extra=temp,time=time)

end


;-------------------------------------------------------------------

pro omap::setmap,map

if not valid_map(map) then return
struct_assign,map,self,/nozero
self.nx=data_chk(map.data,/nx)
self.ny=data_chk(map.data,/ny)
self->setdata,map.data

end


;--------------------------------------------------------------------------

function omap::getdata,no_copy=no_copy,pointer=pointer          ;-- get map data

if not ptr_valid(self.ptr) then return,-1
if not exist(*self.ptr) then return,-1
if keyword_set(pointer) then return,self.ptr
if keyword_set(no_copy) then return,temporary(*self.ptr) else return,*self.ptr 

end

;--------------------------------------------------------------------------

pro omap::setdata,data          ;-- set map data

if data_chk(data,/ndim) ne 2 then return
if ptr_valid(self.ptr) then *self.ptr=data else begin
 ptr=self.ptr
 ptr_alloc,ptr
 *ptr=data
 self.ptr=ptr
 self.nx=data_chk(data,/nx)
 self.ny=data_chk(data,/ny)
endelse

end

;-----------------------------------------------------------------------------

function omap::xrange

self->get,xc=xc,nx=nx,dx=dx
xmin=min(xc-dx*(nx-1.)/2.)
xmax=max(xc+dx*(nx-1.)/2.)
return,[xmin,xmax]

end

;-----------------------------------------------------------------------------

function omap::yrange

self->get,yc=yc,ny=ny,dy=dy
ymin=min(yc-dy*(ny-1.)/2.)
ymax=max(yc+dy*(ny-1.)/2.)
return,[ymin,ymax]

end

;-----------------------------------------------------------------------------

function omap::drange

dmin=min(*self.ptr,max=dmax)
return,[dmin,dmax]

end

;-------------------------------------------------------------------------------

function omap::xp,oned=oned

self->get,xc=xc,nx=nx,dx=dx,ny=ny

if keyword_set(oned) then ny=1

return,mk_map_xp(xc,dx,nx,ny)

end

;-----------------------------------------------------------------------------------

function omap::yp,oned=oned

self->get,yc=yc,ny=ny,dy=dy,nx=nx

if keyword_set(oned) then nx=1

return,mk_map_yp(yc,dy,ny,ny)

end

;----------------------------------------------------------------------------

pro omap::plot,_extra=extra

plot_map,self->getmap(),_extra=extra

return & end

;---------------------------------------------------------------------------

pro omap__define                         

omap={omap, $
     ptr: ptr_new(), $
     nx:0l,$
     ny:0l,$
     xc: 0., $
     yc: 0., $
     dx: 0., $
     dy: 0., $
     time: '', $
     id:'',$
     roll:0.,$
     roll_center:[0.,0.],$
     soho:0b}
     


end

