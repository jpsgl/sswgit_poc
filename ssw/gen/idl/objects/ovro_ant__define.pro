;+
; Project     : HESSI
;
; Name        : OVRO_ANT__DEFINE
;
; Purpose     : Define an OVRO/OVSA antennna selection object
;
; Category    : Ancillary GBO Synoptic Objects
;
; Syntax      : IDL> c=obj_new('ovro_ant')
;
; History     : Written 2 March 2002, D. Zarro, L3/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-
;-----------------------------------------------------------------------------
;-- init

function ovro_ant::init

def=['Ant 1 LIN','Ant 1 LCP','Ant 2 RCP','Ant 2 LIN',$
     'Ant 4 LIN','Ant 5 LIN','Ant 6 LIN','Ant 7 LIN']

self.freq_ptr=ptr_new(/all)
self.ant_ptr=ptr_new(def,/all)

return,1

end

;-----------------------------------------------------------------------------

pro ovro_ant::cleanup

free_var,self.freq_ptr
free_var,self.ant_ptr

return & end

;---------------------------------------------------------------------------
;-- object widget event handler

pro ovro_ant::event,event

widget_control, event.id, get_uvalue=uvalue

case uvalue of
        'ant': self.choice = event.index
        'accept': xkill,event.top
endcase

end

;----------------------------------------------------------------------------
;-- antennae/polarization options

pro ovro_ant::select,choice,group=group,input=input,file=file,freq=freq

if is_string(input) then *self.ant_ptr=trim(input)
if is_string(freq) then *self.freq_ptr=trim(freq)

choices=*self.ant_ptr

mk_dfont,lfont=lfont,bfont=bfont

base = widget_mbase (group=group, /column, $
                      title='OVSA Antennae/Polarization Options', $
                      xpad=40, ypad=40, space=20,/modal)


if is_string(file) then begin
 temp=widget_base(base,/column)
 break_file,file,dsk,dir,name,ext
 label=widget_label(temp,value='OVSA Filename: '+trim(name+ext),font=lfont)
endif

w_base=widget_base(base,/column,/frame)

;-- antenna choices

temp=widget_base(w_base,/row)
label=widget_label(temp,value='Select Antenna and Polarization:',font=lfont)

w_ant = widget_droplist (temp,font=bfont,$
                          value=' ' + choices + ' ',uvalue='ant')

;-- frequency choices

if is_string(freq) then begin
 pad='    '
 temp=widget_base(w_base,/row)
 left=widget_base(temp,/column)
 label=widget_label(left,value='Select Frequencies (GHz):',font=lfont)
 right=widget_base(temp,/column)
 w_freq=widget_list(right,value=pad+freq+pad,ysize=10,xsize=8,font='fixed',/multiple)
endif

;-- show current state

widget_control, w_ant, set_droplist_select=self.choice

w_accept = widget_button(base, value='Accept',uvalue='accept',$
                         font=bfont,/align_center)

xrealize,base,group=group,/center,/screen

widget_control,base,set_uvalue={object:self,method:'event'}

xmanager,'self->select',base,event='obj_event'

choice=self.choice

return
end

;------------------------------------------------------------------------------
;-- define SELF structure

pro ovro_ant__define

self={ovro_ant,choice:0,ant_ptr:ptr_new(),freq_ptr:ptr_new()}

return & end
