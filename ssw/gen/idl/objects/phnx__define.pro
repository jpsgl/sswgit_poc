;+
; Project     : HESSI
;
; Name        : PHNX__DEFINE
;
; Purpose     : Define a site object for Phoenix data
;
; Category    : Ancillary GBO Synoptic Objects
;
; Syntax      : IDL> c=obj_new('phnx')
;
; History     : Written 18 Oct 2000, D. Zarro (EIT/GSFC)
;
; Contact     : dzarro@solar.stanford.edu
;-
;-----------------------------------------------------------------------------
;-- init 

function phnx::init,_ref_extra=extra

ret=self->site::init(_extra=extra)
if not ret then return,ret
ret=self->specplot::init(_extra=extra)
if not ret then return,ret

self.fits=obj_new('fits')           

self->setprop,rhost='www.astro.phys.ethz.ch',ext='.fits',org='month',/gzip,$
                 topdir='/pub/rag/phoenix-2/bursts/fits',ftype='phnx'

self->set,/sel_dim1

return,1

end

;-----------------------------------------------------------------------

pro phnx::cleanup

obj_destroy,self.fits
self->specplot::cleanup
self->site::cleanup

return & end
                                                                                     
;-----------------------------------------------------------------------------
;-- get remote subdirectory id's based on file dates

function phnx::get_sdir
      
fids=self->site::get_sdir(/full,delim='/')

return,fids

end

;----------------------------------------------------------------------------
;-- read method

pro phnx::read,file,err=err,_extra=extra,header=header,nodata=nodata

err=''
if keyword_set(nodata) then begin
 mrd_head,file,header,err=err,_extra=extra
 return
endif

;-- read main data

self.fits->readfits,file,index,data,extension=0,_extra=extra,err=err,header=header

;-- read extension

if err ne '' then begin
 message,err,/cont
 return
endif

self.fits->readfits,file,index1,data1,extension=1,_extra=extra,err=err,header=header1

if err ne '' then begin
 message,err,/cont
 return
endif

time=data1.time*index1.tscal1+index1.tzero1
frequency=data1.frequency*index1.tscal2+index1.tzero2
utbase=anytim2utc(index.time_d$obs+' '+index.date_d$obs,/vms)

time=anytim2tai(utbase)+temporary(time)
freq=string(float(frequency),format='(i7.0)')+' MHz'
dunit='Flux (45*log10[SFU + 10]'

self->set,xdata=time,ydata=data,dim1_id=freq,dim1_unit='Frequency (MHz)',$
             data_unit=dunit,id='Phoenix: '+utbase,file=file,/no_copy,$
             dim1_val=float(frequency)

self->fbreak,file,dir,name

self->set,filename=name,dim1_use=indgen(4),/positive

return & end

;----------------------------------------------------------------------------
;-- driver to ftp PHNX files to $SYNOP_DATA

pro phnx::synop,_extra=extra

message,'copying PHNX synoptic data',/cont

;-- default settings

get_utc,utc
utc.time=0
self->setprop,tstart=utc,back=20,/verbose,/subdir,err=err,_extra=extra

if err ne '' then return

;-- start with daily files

self->setprop,ldir='$SYNOP_DATA/spectra',err=err

if err eq '' then self->copy
                                          

return & end
                                          

;------------------------------------------------------------------------------
;-- Phoenix site structure

pro phnx__define                 

self={phnx,file:'',fits:obj_new(),inherits site, inherits specplot}

return & end

