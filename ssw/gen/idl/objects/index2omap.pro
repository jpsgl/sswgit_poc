;+
; Project     : HESSI
;
; Name        : INDEX2OMAP
;
; Purpose     : convert INDEX/DATA to object maps
;
; Category    : imaging, FITS, objects
;
; Syntax      : index2omap,index,data,omap
;
; Inputs      : INDEX = index structure array
;               DATA = data array
;
; Outputs     : OMAP = map object linkedlist 
;
; History     : Written, 22 April 2000, D.M. Zarro (SM&A/GSFC)
;
; Contact     : dzarro@solar.stanford.edu
;-

pro index2omap,index,data,omap,_extra=extra,err=err

np=n_elements(index)

;-- Start by making each map the old way as a structure. Store
;-- it in a temporary map object and then store the object in a linkedlist.
;-- Be careful not to destroy any pointers when cleaning up

if obj_valid(omap) then obj_destroy,omap
omap=obj_new('map_list')

for i=0,np-1 do begin
 index2map,index(i),data(*,*,i),map,err=terr,_extra=extra 
 if terr eq '' then begin
  tmap=obj_new('map',map)
  omap->setmap,tmap,i
 endif
endfor

if exist(aerr) then err=arr2str(terr)

return
end

