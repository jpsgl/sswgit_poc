;+
; Project     : HESSI
;
; Name        : LTC__DEFINE
;
; Purpose     : Define a lightcurve class
;
; Category    : objects
;
; Syntax      : IDL> new=obj_new('ltc')
;
; History     : Written 14 June 2000, D. Zarro, EIT/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;
; Modified:
;     10 Oct 2000, Kim Tolbert. Added dim1_colors and dim1_linestyles properties, status
;        and err_msg keywords (err_msg was err), and added legend to plot.
;     15 Oct 2000, Kim Tolbert.  Added time_range keyword to get method.  Changed time
;        keyword in get method to times (to avoid ambiguity with time_range).
;        And added return, ' ' in get method when falls through all tests.
;     5-Dec-2000 - Kim Tolbert.  Change sum keyword to dim1_sum
;     6-Dec-2000 - Kim Tolbert.  Change label property to be a pointer to allow for vector
;     17-Mar-2000 - Zarro (EITI/GSFC), merged LTC & LTC_PLOT. Made easier to
;                  maintain.
;-

;-------------------------------------------------------------------------

function ltc::init

add_method,'gen',self
self.plot_type='utplot'
self.nchan=1
self.ntime=1
self->set,over=1,legend_loc=1
self->set_colors
self->set_linestyles

return,1

end

;-----------------------------------------------------------------------
;--destroy object

pro ltc::cleanup

dprint,'% LTC::CLEANUP'
add_method,'free_var',self
self->free_var

return
end      

;-------------------------------------------------------------------
;-- set data properties

pro ltc::set_data,times,data,id=id,dim1_id=dim1_id,$
           data_unit=data_unit,dim1_unit=dim1_unit, $
           status=status, err_msg=err_msg,no_copy=no_copy

status = 1
err_msg=''

nt=data_chk(times,/nx)
np=data_chk(data,/nx)
no_copy=keyword_set(no_copy)

if (nt eq 0) or (np eq 0) then return
if nt ne np then begin
 err_msg='Input time and data arrays do not match'
 message,err_msg,/cont
 status = 0
 return
endif

self.ntime=nt
self.nchan=data_chk(data,/ny)
self.utbase=anytim2utc(times[0],/vms)
if is_string(id) then self.id=id

if is_string(dim1_unit) then self.dim1_unit=dim1_unit
if is_string(data_unit) then self.data_unit=data_unit

ptr=self.data_ptr
ptr_alloc,ptr
if no_copy then *ptr=temporary(data) else *ptr=data
self.data_ptr=ptr

ptr=self.time_ptr
ptr_alloc,ptr
if no_copy then *ptr=temporary(times) else *ptr=times
self.time_ptr=ptr

if is_string(dim1_id) then begin
 nchan=self.nchan
 if n_elements(dim1_id) ne nchan then begin
  err_msg="Number of data ID's and lightcurves do not match"
  message,err_msg,/cont
  status = 0
  return
 endif
 ptr=self.dim1_id
 ptr_alloc,ptr
 if no_copy then *ptr=temporary(dim1_id) else *ptr=dim1_id
 self.dim1_id=ptr
endif

self->set_channels
self->set_colors
self->set_linestyles

return
end

;--------------------------------------------------------------------------
;-- get data properties

function ltc::get,times=times,time_range=time_range, $
             data_array=data_array,ut_ref=ut_ref,$
             _extra=extra


if keyword_set(times) and ptr_valid(self.time_ptr) then begin
 if exist(*self.time_ptr) then return,*self.time_ptr
endif

if keyword_set(time_range) and ptr_valid(self.time_ptr) then begin
 if exist(*self.time_ptr) then $
  return,[(*self.time_ptr)[0], (*self.time_ptr)[self.ntime-1]] - (*self.time_ptr)[0]
endif

if keyword_set(data_array) and self->has_data() then begin
 channels=self->get(/dim1_use)
 return,reform((*self.data_ptr)[*,channels])
endif

if keyword_set(ut_ref) then return,anytim(self->get(/utbase))

return,self->getprop(_extra=extra)

end

;---------------------------------------------------------------------------
;-- get data range

pro ltc::get_drange,dmin,dmax,trange=trange,err_msg=err_msg, status=status

status = 1
err_msg=''

if not self->has_data() then begin
 err_msg='No data to plot'
 message,err_msg,/cont
 status = 0
 return
endif

;-- check if trange is passed

use_trange=0
if valid_range(trange) then begin
 tai=anytim(trange,/tai)
 tmin=min(tai,max=tmax)
 ok=where( (*self.time_ptr ge tmin) and (*self.time_ptr le tmax),count)
 if count eq 0 then begin
  err_msg = 'No data during specified time range, using all data'
  message, err_msg, /cont
  tmin=min(*self.time_ptr,max=tmax)
  trange=[tmin,tmax]
 endif else use_trange=1
endif

channels=self->get(/dim1_use)
nchans=n_elements(channels)

ylog=self->get(/ylog)
sum=self->get(/dim1_sum)

if ylog then begin
 if (sum and nchans gt 1) then begin
  if use_trange then $
   good=where( total( ((*self.data_ptr)[ok,*])[*,channels],2) gt 0,count) else $
    good=where( total((*self.data_ptr)[*,channels],2) gt 0,count)
 endif else begin
  if use_trange then $
   good=where( ((*self.data_ptr)[ok,*])[*,channels] gt 0,count) else $
    good=where( (*self.data_ptr)[*,channels] gt 0,count)
 endelse
 if count eq 0 then begin
  err_msg = 'Cannot plot all negative data'
  message, err_msg, /cont
  dmin=.0001 & dmax=1.
  return
 endif
endif


;-- GOOD -> indicies of data > 0 (needed when YLOG=1)
;-- OK   -> indicies of data within user specified TRANGE

if ylog then begin
 if sum and (nchans gt 1) then begin
  if use_trange then $
   dmax=max( (total( ((*self.data_ptr)[ok,*])[*,channels],2))[good],min=dmin) else $
    dmax=max( (total( (*self.data_ptr)[*,channels],2))[good],min=dmin)
 endif else begin
  if use_trange then $
   dmax=max( (((*self.data_ptr)[ok,*])[*,channels])[good],min=dmin) else $
    dmax=max( ((*self.data_ptr)[*,channels])[good],min=dmin)
 endelse
endif else begin
 if sum and (nchans gt 1) then begin
  if use_trange then $
   dmax=max( total( ((*self.data_ptr)[ok,*])[*,channels],2),min=dmin) else $
    dmax=max( total ((*self.data_ptr)[*,channels],2),min=dmin)
 endif else begin
  if use_trange then $
   dmax=max( ((*self.data_ptr)[ok,*])[*,channels],min=dmin) else $
    dmax=max( (*self.data_ptr)[*,channels],min=dmin)
 endelse
endelse


return & end

;----------------------------------------------------------------------------
;-- lightcurve plotter

pro ltc::plot,dim1_use=channels,yrange=yrange,ylog=ylog,$
          nsum=nsum,over=over,$
          all=all,dim1_sum=dim1_sum,timerange=timerange, legend_loc=legend_loc, $
          label=label, err_msg=err_msg, status=status, $
          _extra=extra
                                                                         
status = 1
err_msg = ''

if not self->has_data() then begin
 err_msg = 'No data to plot'
 message, err_msg, /cont
 status = 0
 return
endif
                                                                         
self->set,dim1_use=channels,ylog=ylog,nsum=nsum,$
    all=all,over=over,dim1_sum=dim1_sum, legend_loc=legend_loc,label=label

;-- get default data ranges if not specified by user

if valid_range(yrange, /allow_zeros) then self->set,yrange=yrange

if not ( valid_range(yrange) or valid_range(self->get(/yrange)) ) then begin
 self->get_drange,dmin,dmax,trange=timerange, status=status, err_msg=err_msg
 if not status then return
 yrange=[dmin,dmax]
 dprint,'% YRANGE ',yrange
 self->set,yrange=yrange
endif

utbase=self->get(/utbase)

data_unit=self->get(/data_unit)
ylog=self->get(/ylog)
over=self->get(/over)
nsum=self->get(/nsum)
sum=self->get(/dim1_sum)
id=self->get(/id)
yrange=self->get(/yrange)
channels=self->get(/dim1_use)
linestyles=self->get(/dim1_linestyles)
colors=self->get(/dim1_colors)
nplot=n_elements(channels)

;-- construct utplot command to execute

plot_keywords=['noerase=(i gt 0) and over','nsum=nsum',$
               'ytitle=data_unit','ylog=ylog','title=id',$
               'yrange=yrange','timerange=timerange',$
               '_extra=extra']

;-- if sum then don't with internal colors and linestyles
;-- if different colors are set, then don't bother with different linestyles

if (not sum) then begin
 cmax=max(colors,min=cmin)
 use_colors=cmin ne cmax 
 if use_colors then extra_key='color=colors[k]' else $
  extra_key='linestyle=linestyles[k]'
 plot_keywords=[plot_keywords,extra_key]
endif

plot_cmd='utplot,*self.time_ptr-anytim2tai(utbase)'
yval='(*self.data_ptr)[*,k]'

;-- check if summing

if sum then begin
 yval='(*self.data_ptr)[*,channels]'
 if nplot gt 1 then yval='total('+yval+',2)'
endif

;-- check if log plot

eps=.0001
if ylog then yval='('+yval+') > eps'
plot_cmd=plot_cmd+','+yval+',utbase,'+arr2str(plot_keywords)

for i=0,nplot-1 do begin
 k=channels[i]
 status=execute(plot_cmd)
 if sum then goto,done
endfor

done:

self->write_legend,_extra=extra

if (!d.name eq 'X') or (!d.name eq 'WIN') then wshow

return & end

;--------------------------------------------------------------------------
;-- check if object has data

function ltc::has_data

if not ptr_valid(self.data_ptr) then return,0b
return,exist(*self.data_ptr)

end

;---------------------------------------------------------------------------
;-- show LTC properties

pro ltc::show

print,''
print,'LTC properties:'
print,'---------------'
print,'% DATA ID: ',self->get(/id)
print,'% # DATA POINTS: ',self->get(/ntime)
print,'% # CHANNELS: ',self->get(/nchan)
print,'% UTBASE:' ,self->get(/utbase)
print,'% CHANNELS SELECTED: ',self->get(/dim1_use)
print,'% PLOT_TYPE: ',self->get(/plot_type)
print,'% YRANGE: ',self->get(/yrange)
print,'% OVER/YLOG: ',self->get(/over),self->get(/ylog)
print,'% ALL/SUM/ENAB_SUM: ',self->get(/all),self->get(/dim1_sum),self->get(/enab_sum)
print,'% NSUM: ',self->get(/nsum)
print,''

return & end
         
;-----------------------------------------------------------------------
;-- set plot properties

pro ltc::set,over=over,ylog=ylog,all=all,enab_sum=enab_sum,$
             dim1_sum=dim1_sum,$
             nsum=nsum,yrange=yrange, legend_loc=legend_loc, label=label,$
             dim1_colors=dim1_colors, dim1_linestyles=dim1_linestyles,$
             charsize=charsize,dim1_use=dim1_use

if is_number(all) then self.all= 0 > all < 1
if is_number(over) then self.over= 0 > over < 1
if is_number(ylog) then self.ylog= 0 > ylog < 1
if is_number(enab_sum) then self.enab_sum= 0 > enab_sum < 1
if is_number(dim1_sum) then self.dim1_sum= 0 > dim1_sum < 1
if is_number(nsum) then self.nsum=nsum > 0
if n_elements(yrange) eq 2 then self.yrange=yrange
if is_number(legend_loc) then self.legend_loc = 0 > legend_loc < 4
if is_string(label) then begin
 ptr = self.label
 ptr_alloc, ptr
 *ptr = label
 self.label = ptr
endif

if exist(dim1_colors) then self->set_colors, dim1_colors
if exist(dim1_linestyles) then self->set_linestyles,dim1_linestyles
if exist(dim1_use) then self->set_channels,dim1_use

return & end

;-----------------------------------------------------------------------------
;--- set channels to plot

pro ltc::set_channels, dim1_use

nchan=self->get(/nchan)

def_chans=indgen(nchan)
if not exist(dim1_use) then dim1_use=def_chans

dim1_use=self->valid_channels(dim1_use)
if self->get(/all) then dim1_use=def_chans

ptr=self.dim1_use
ptr_alloc,ptr
*ptr=dim1_use
self.dim1_use=ptr

return
end

;--------------------------------------------------------------------------
;-- validate input channels

function ltc::valid_channels,channels

if not exist(channels) then nchannels=0 else nchannels=channels
nchan=self->get(/nchan)
if nchan gt 1 then begin
 ok=where( (nchannels lt nchan) and (nchannels gt -1),count)
 if count gt 0 then nchannels=nchannels[ok] else nchannels=0
 if count eq 1 then nchannels=nchannels[0]
endif

return,get_uniq(nchannels) & end

;-----------------------------------------------------------------------------
;--- set colors. if user didn't set enough colors, then keep appending

pro ltc::set_colors, dim1_colors

def_colors=!d.table_size-1
if not exist(dim1_colors) then dim1_colors=def_colors

nchan=self->get(/nchan)
cc = 0 > dim1_colors < def_colors
while n_elements(cc) lt nchan do cc = append_arr (cc, dim1_colors)
cc = cc[0:nchan-1]
ptr=self.dim1_colors
ptr_alloc,ptr
*ptr=cc
self.dim1_colors=ptr

return
end

;-----------------------------------------------------------------------------
;--- set linestyles.  if user didn't set enough linestyles, then keep appending

pro ltc::set_linestyles, dim1_linestyles

def_linestyles=indgen(6)

if not exist(dim1_linestyles) then dim1_linestyles=def_linestyles

nchan=self->get(/nchan)
ll = dim1_linestyles
while n_elements(ll) lt nchan do ll = append_arr (ll, dim1_linestyles)
ll =  ll[0:nchan-1]
ptr=self.dim1_linestyles
ptr_alloc,ptr
*ptr=ll
self.dim1_linestyles=ptr

return & end

;------------------------------------------------------------------------------

pro ltc::write_legend,charsize=charsize

; write legend if requested: legend_loc = 0/1/2/3/4 = none, topleft, topright,
; bottomleft, bottomright
; scale character size for legend and timestamp to size of window or requested
; size of plot labels.

label_size = ch_scale(.8, /xy)
time_size = ch_scale(.6, /xy)
if exist(charsize) then begin
 label_size = charsize * .8
 time_size = charsize * .6
endif

loc = self->get(/legend_loc)
sum= self->get(/dim1_sum)

if loc ne 0 then begin

 top = loc lt 3
 bottom = loc ge 3
 right = (loc mod 2) eq 0
 left = loc mod 2

 loc_cmd = ',top_legend=top, bottom_legend=bottom, right_legend=right, left_legend=left'

 text = ''
 nlabel = 0

;-- label might contain additional lines to put in legend.  If so, be sure to 
;   prepend colors and linestyles arrays with extra values for these lines.
 
 text=self->get(/label)
 if is_string(text) then nlabel=n_elements(text)
 channels=self->get(/dim1_use)
 nchans=n_elements(channels)
 sum=self->get(/dim1_sum)
 cmd = 'legend, text, box=0'

 if (not sum) then begin

  ids=(self->get(/dim1_id))[channels]
  colors=(self->get(/dim1_colors))[channels]
  linestyles=(self->get(/dim1_linestyles))[channels]

  if text[0] eq '' then text = ids else text = [text, ids]

;-- linestyle -99 means don't draw line or indent
  
  more_cmd=',linestyle=linestyles' 
  cmax=max(colors,min=cmin)
  use_colors=(cmin ne cmax) or (nchans eq 1)
  if use_colors then begin
   linestyles=intarr(nchans)
   if nlabel gt 0 then begin
    colors =[replicate(0,nlabel), colors]    
    linestyles=[replicate(-99,nlabel), linestyles]
    more_cmd=more_cmd+',color=colors'
   endif
  endif else begin
   if nlabel gt 0 then linestyles = [replicate(-99, nlabel), linestyles]
  endelse
  cmd = cmd + more_cmd

 endif

 if text[0] ne '' then ok = execute (cmd + loc_cmd + ', charsize=label_size')

endif

;-- write little time stamp on bottom of plot

timestamp, /bottom, charsize=time_size

return & end
    
;------------------------------------------------------------------------------
;-- LTC properties definition         

pro ltc__define

temp={ltc,               $
     id:'',              $     ;-- data identifier
     utbase:'',          $     ;-- UT base time of data
     time_ptr:ptr_new(), $     ;-- pointer for time axis array (TAI secs)
     data_ptr:ptr_new(), $     ;-- pointer for data array (NTIME x NCHAN)
     dim1_id:ptr_new(),  $     ;-- label ID (individual channels, e.g. 300 GHz)
     dim1_unit:'',       $     ;-- channel unit (e.g. frequency)
     data_unit:'',       $     ;-- data unit (e.g. SFU)
     ntime:0l,           $     ;-- # of time points
     nchan:0l,           $     ;-- # of channels
     plot_type:'',       $     ;-- plot type
     yrange:[0.,0.],     $     ;-- data yrange
     over:0b,            $     ;-- plot as overlay
     ylog:0b,            $     ;-- plot as log
     all:0b,             $     ;-- plot all channels
     enab_sum: 0b,       $     ;-- allow sum channels option
     nsum:0l,            $     ;-- sum in time domain
     legend_loc: 0b,     $     ;-- location for legend 0/1/2/3/4 = no legend/ topleft/topright/ bottomleft/bottomright
     label: ptr_new(),   $     ;-- additional info to put in legend
     dim1_sum:0b,        $     ;-- sum channels
     dim1_use:ptr_new(), $     ;-- array of channels to plot
     dim1_colors: ptr_new(), $ ;-- array of colors to use for plots
     dim1_linestyles: ptr_new() $   ;-- array of linestyles to use for plots
     }

return
end

