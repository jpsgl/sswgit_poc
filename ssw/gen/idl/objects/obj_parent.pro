;+
; Project     : HESSI
;
; Name        : OBJ_PARENT
;
; Purpose     : find parents of object or class
;
; Category    : utility objects
;
; Explanation : checks CLASS__DEFINE procedure before trying 
;               to create objects. 
;               This is better that calling OBJ_CLASS( /SUPER)
;               since the latter returns blanks if an
;               object has not yet been created.
;               
; Syntax      : IDL> parent=obj_parent(class)
;
; Inputs      : CLASS = class name or object variable name 
;
; Outputs     : PARENT = class name of parent
;
; Keywords    : SUPER = check SUPER classes
;
; History     : Written 10 Oct 1999, D. Zarro, SM&A/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

function obj_parent,class,super=super

valid_obj=0b
super=keyword_set(super)
if datatype(class) eq 'OBJ' then begin
 valid_obj=obj_valid(class)
 if not valid_obj then begin
  err='Input object is null'
  message,err,/cont
  return,''
 endif
 class_name=obj_class(class) 
endif else begin
 if datatype(class) ne 'STR' then begin
  pr_syntax,'parent=obj_parent(class_name [,/super])'
  err='Invalid input'
  return,''
 endif
 class_name=class
endelse

if valid_obj then begin
 parent=obj_class(class,super=super) 
 if not super then parent=parent(0)
 if (n_elements(parent) eq 1) then parent=parent(0)
 return,parent
endif

;-- if object isn't yet defined, we check it's constructor

class_def=strlowcase(trim(class_name))+'__define'
have_con=have_proc(class_def,out=fname)
if have_con then begin
 tfile=rd_tfile(fname)
 np=n_elements(tfile)
 patt1='{'+class_name+','
 patt2='inherits'
 forever=0b
 for i=0,n_elements(tfile)-1 do begin
  temp=strlowcase(strcompress(tfile(i),/rem))
  chk1=strpos(temp,patt1)
  if (chk1 gt -1) then begin
   repeat begin    
    chk2=strpos(temp,patt2)
    if (chk2 gt -1) then begin
     parent=strmid(temp,chk2+strlen(patt2),strlen(temp)-strlen(patt2)-chk2-1)
     if super then begin
      last_parent=parent
      repeat begin
       new_parent=obj_parent(last_parent)
       if new_parent ne '' then begin
        last_parent=new_parent
        parent=append_arr(parent,new_parent)
       endif        
      endrep until (new_parent eq '')
     endif
     return,parent
    endif
    if (i eq (np-1)) then return,''
    i=i+1
    temp=strlowcase(strcompress(tfile(i),/rem))    
   endrep until forever
  endif
 endfor
endif else begin

;-- if no constructor, try creating object

 chk=valid_class(class_name,/quiet,err=err)
 if not chk then begin
  message,err,/cont
  return,''
 endif
 parent=obj_class(class,super=super)
 if not super then parent=parent(0)
 if (n_elements(parent) eq 1) then parent=parent(0)
 return,parent
endelse
        
return,''

end
