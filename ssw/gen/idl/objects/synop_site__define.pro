;+
; Project     : HESSI
;
; Name        : SYNOP_SITE__DEFINE
;
; Purpose     : Define a SYNOP data object
;
; Category    : GBO Synoptic Objects
;
; Syntax      : IDL> c=obj_new('synop')
;
; History     : Written 5 March 2000, D. Zarro, SM&A/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-
;-----------------------------------------------------------------------------
;-- init 

function synop_site::init

success=self->site::init()

if success then begin                         

 user_synop_data=chklog('USER_SYNOP_DATA')
 if not test_dir(user_synop_data) then begin
  user_synop_data=curdir()
  if not write_dir(user_synop_data) then user_synop_data=get_temp_dir()
  mklog,'USER_SYNOP_DATA',user_synop_data
 endif 
 dprint,'% SYNOP_SITE::INIT -> USER_SYNOP_DATA: ',user_synop_data

 self->setprop,org='day',types='images',ext='',err=err,ldir=user_synop_data
 success=err eq ''
endif

if success eq 0 then return,0b

success=self->synop_db::init()

dprint,'% SYNOP_SITE::INIT ',success

return,success

end

;----------------------------------------------------------------------------

pro synop_site::cleanup

self->site::cleanup
self->synop_db::cleanup

dprint,'% SYNOP_SITE::CLEANUP'

return & end

;------------------------------------------------------------------------------
;-- SET method

pro synop_site::setprop,types=types,last_count=last_count,err=err,$
                   wave=wave,mode=mode,_extra=extra


;-- set different search types 
;   (comma delimited string: spectra, images, lightcurve)
                                
self->add_types,types,err=err
if err ne '' then begin
 message,err,/cont
 return
endif

if is_string(wave) then self.wave=trim(wave)
if is_number(mode) then self.mode=(0 > mode < 2) 

if is_number(last_count) then self.last_count=last_count

;-- set the rest

self->site::setprop,_extra=extra,err=err

return & end

;-----------------------------------------------------------------------------
;--copy specified file(s)

function synop_site::copy_synop,file,err=err,count=count

err='' & lfile='' & count=0

if not is_string(file,cfile,err=err) then begin
 message,err,/cont
 return,''
endif

;-- if input files don't have a path name attached, then we search first

self->fbreak,cfile,cdir,cname
do_search=(not is_string(cdir))

if do_search then begin
 cfile=self->find_synop(cfile,count=count,err=err)
 if count eq 0 then return,'' 
endif

self->setprop,rfile=cfile
self->copy_file,lfile,count=count,err=err
                               
return,lfile
 
end

;----------------------------------------------------------------------------
;-- return subdirectory name where site files are located

function synop_site::get_search_dir

mode=self->getprop(/mode)
types=self->get_synop_types(/lower)

case mode of
0: return,self->get_site_dir(self->getprop(/ftype))
1: return,self->get_wave_dir(self->getprop(/wave))
else: return,types
endcase
    
end

;------------------------------------------------------------------------------
;-- show properties

pro synop_site::show

self->site::show
print,''
print,'SYNOP properties:'
print,'-----------------'
print,'% types: ',self.types
print,'% last_count: ',self.last_count
return & end
                                           
;------------------------------------------------------------------------------
;-- get data

pro synop_site::getdata,data,file=file,err=err,lfile=lfile

err=''
;-- start with special case if filenames entered

if size(file,/tname) eq 'STRING' then begin
 lfile=self->copy_synop(file,err=err,count=count)
 found=count gt 0
 if err ne '' then message,err,/cont
endif else begin
 self->copy,lfile,count=count
 found=count gt 0
endelse

if found then self->read,lfile,data,err=err

return & end

;-----------------------------------------------------------------------------
;-- file reader

pro synop_site::read,file,data,err=err

err=''   
self->get_class_cat,file,class,err=err
if err ne '' then return

if not have_method(class,'read') then begin
 err='Class "'+class+'" does not have "read" method'
 message,err,/cont
 return
endif

dprint,'% synop_site::READ: ',class

stat=execute('data=obj_new("'+class+'")')

if not stat then begin
 err='Failed to create object from: "'+class+'"'
 message,err,/cont
 return
endif

data->read,file,err=err

return & end
           
;------------------------------------------------------------------------------

pro synop_site::apply,event

xkill,event.top

return & end

;----------------------------------------------------------------------------
;-- widget base setup

pro synop_site::config,group=group,no_types=no_types

clobber=self->getprop(/clobber)
cache=self->getprop(/cache)
ldir=self->getprop(/ldir)
types=self->getprop(/types)
last_time=self->getprop(/last_time)
copy_mode=self->getprop(/copy_mode,count=copy_count)
 
save_config={clobber:clobber,copy_mode:copy_mode,cache:cache,ldir:ldir,$
             user_synop_data:chklog('USER_SYNOP_DATA'),types:types,$
             last_time:last_time}

mk_dfont,bfont=bfont,lfont=lfont
base=widget_mbase(/column,title="SHOW_SYNOP DOWNLOAD OPTIONS",/modal,group=group,uvalue=self)
                                 

;-- save time interval

base1=widget_base(base,/column,/frame)

trow=widget_base(base1,/row)
xmenu2,['Yes','No'],trow,/row,/exclusive,font=lfont,/no_rel,$
      buttons=tbuttons,uvalue=['yes_save','no_save'],$
      title='Save last search time interval? ',lfont=lfont 
widget_control,tbuttons[1-last_time],/set_button  

;-- use caching

crow=widget_base(base1,/row)
xmenu2,['Yes','No'],crow,/row,/exclusive,font=lfont,/no_rel,$
      buttons=sbuttons,uvalue=['yes_cache','no_cache'],$
      title='Cache search results (recommended for speed)? ',lfont=lfont 
widget_control,sbuttons[1-cache],/set_button  
                                    
;-- choice of data type 

if (1-keyword_set(no_types)) then begin
 supp_types=self->get_synop_types()
 row1=widget_base(base1,/row)
 xmenu2,supp_types,row1,/row,/nonexclusive,font=lfont,$
      buttons=dbuttons,uvalue=strlowcase(supp_types),$
      title='Data types to download: ',lfont=lfont
 curr_types=self->get_types() 
 val=where_vector(curr_types,supp_types,count)
 if count gt 0 then $
  for i=0,count-1 do widget_control,dbuttons[val[i]],/set_button          
endif
                                    
;-- clobber data?
                       
                                               
row3=widget_base(base1,/row)
xmenu2,['Yes','No'],row3,/row,/exclusive,font=lfont,/no_rel,$
      buttons=cbuttons,uvalue=['yes_clobber','no_clobber'],$
      title='Overwrite existing files when downloading? ',lfont=lfont 
widget_control,cbuttons[1-clobber],/set_button                        
                   

;-- download directory

row2=widget_base(base1,/row)

col21=widget_base(row2,/column)
dlabel=widget_label(col21,value='Download directory: ',font=lfont,/align_left)
dlabel=widget_label(col21,value='(USER_SYNOP_DATA)',font=lfont,/align_left)

col22=widget_base(row2,/row)
dtext=widget_text(col22,value=ldir,xsize=25,/editable,uvalue='directory')
dbutt=widget_button(col22,value='Browse',font=bfont,uvalue='browse')
                        
;-- make links to data (UNIX only and LOCAL_SYNOP locally mounted)

if (os_family() eq 'unix') and is_dir('SYNOP_DATA') and (copy_count gt 0) then begin
 local_base=widget_base(base,/row,/frame)
 xmenu2,[' Copy files from archive when downloading',' Make symbolic links to archive files (recommended to save disk space)'],$
  local_base,/column,/exclusive,font=lfont,/no_rel,uvalue=['yes_copy','no_copy'],$
  title='Copy mode (for locally available Synoptic archive): ',buttons=lbuttons,lfont=lfont
 copy_mode=(copy_mode-1) > 0
 widget_control,lbuttons[copy_mode],/set_button
endif


row0=widget_base(base,/row,/align_center)

;doneb=widget_button(row0,value='Apply',uvalue={object:self, method:'apply'},$
;       font=bfont,/frame)

doneb=widget_button(row0,value='Apply',uvalue='apply',$
       font=bfont,/frame)
cancb=widget_button(row0,value='Cancel',uvalue='cancel',$
       font=bfont,/frame)
                                 
;-- share widget id's thru child's uvalue

child=widget_info(base,/child)
info={dtext:dtext,save_config:save_config}
widget_control,child,set_uvalue=info

xrealize,base,/center 

xmanager,'synop_site::config',base,event='synop_event'

return & end

;----------------------------------------------------------------------------
;-- object event handler. Since actual methods cannot be event-handlers,
;   we shunt events thru this wrapper

pro synop_event,event
widget_control,event.top,get_uvalue=object
if obj_valid(object) then object->synop_event,event
end

;----------------------------------------------------------------------------

pro synop_site::synop_event,event

widget_control,event.id,get_uvalue=uvalue
widget_control,event.top,get_uvalue=self
child=widget_info(event.top,/child)
widget_control,child,get_uvalue=info

uvalue=trim(uvalue[0])

;-- intercept manual directory change

if (uvalue eq 'apply') or (uvalue eq 'directory') then begin
 widget_control,info.dtext,get_value=cur_dir
 cur_dir=trim(cur_dir)
 new_dir=trim(self->getprop(/ldir))
 if cur_dir eq '' then begin
  xack,'Download directory cannot be blank'
  return
 endif
 if cur_dir ne new_dir then begin
  if not test_dir(cur_dir) then begin
   xack,'No write access to '+new_dir
   return
  endif else begin
   if cur_dir ne chklog('USER_SYNOP_DATA') then begin
    warn=['Do you wish to redefine USER_SYNOP_DATA to be:',cur_dir+' ?']
    ans=xanswer(warn)
    if ans then mklog,'USER_SYNOP_DATA',cur_dir
   endif   
   self->setprop,ldir=cur_dir
  endelse
 endif
endif

;-- determine selected SYNOP types

tvalue=trim(uvalue[0])
self->update_types,tvalue,event

case tvalue of

 'yes_clobber': self->setprop,clobber=1
 'no_clobber': self->setprop,clobber=0

 'yes_cache': self->setprop,cache=1
 'no_cache': self->setprop,cache=0

 'yes_save': self->setprop,last_time=1
 'no_save': self->setprop,last_time=0

 'yes_copy': self->setprop,copy=1
 'no_copy': self->setprop,copy=2
                 
 'browse': begin
   ldir=self->getprop(/ldir)
   save_dir=curdir() 
   if is_dir(ldir) then cd,ldir 
   new_dir=dialog_pickfile(/direct,group=event.top)
   cd,save_dir
   if trim(new_dir) ne '' then begin
    if test_dir(new_dir) then begin
     self->setprop,ldir=new_dir 
     widget_control,info.dtext,set_value=new_dir
    endif else xack,'No write access to '+new_dir
   endif
  end

 'apply':xkill,event.top

 'cancel': begin  
   struct_assign,info.save_config,self,/nozero
   mklog,'USER_SYNOP_DATA',(info.save_config).user_synop_data
   xkill,event.top
  end

 else: return
endcase

return & end

;------------------------------------------------------------------------------
;-- update list of data types to search for

pro synop_site::update_types,tvalue,event

supp_types=self->get_synop_types(/lower)
chk=where(strlowcase(tvalue) eq supp_types,count)
if count gt 0 then begin
 type=supp_types[chk[0]]
 if event.select then self->add_types,type,err=err else $
  self->rem_types,type,err=err
 if err ne '' then begin
  xack,err,/cont
  widget_control,event.id,/set_button
 endif
endif

return & end
                
;-----------------------------------------------------------------------------
;-- list method (allows multiple SYNOP types)

pro synop_site::list,files,count=count

count=0 & files=''
types=self->get_types()
sdir=self->synop_dir()

for i=0,n_elements(types)-1 do begin
 self->setprop,topdir=sdir+'/'+types[i]
 self->site::list,lfiles,count=count
 if count gt 0 then dfiles=append_arr(dfiles,lfiles,/no_copy)
endfor

count=n_elements(dfiles)
if count eq 0 then files='' else files=temporary(dfiles)
if count eq 1 then files=files[0] 
 
self->setprop,last_count=count

return & end

;------------------------------------------------------------------------------
;-- SYNOP site structure

pro synop_site__define                 

self={synop_site,last_count:0l,mode:0, wave:'', types:'',inherits site, inherits synop_db}

return & end
