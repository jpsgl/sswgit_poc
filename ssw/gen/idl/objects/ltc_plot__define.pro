;+
; Project     : HESSI
;
; Name        : LTC_PLOT__DEFINE
;
; Purpose     : Define a plot control class for lightcurve LTC objects
;
; Category    : objects
;
; Syntax      : IDL> new=obj_new('ltc_plot')
;
; History     : Written 14 June 2000, D. Zarro, EIT/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;
; Modified:
;   10 Oct 2000, Kim Tolbert.  Add enab_sum,  legend_loc, and label  properties
;   5-Dec-2000 - Kim Tolbert.  Change sum keyword to dim1_sum
;-

;-------------------------------------------------------------------------

function ltc_plot::init

self.plot_type='utplot'
self.over=1
self.legend_loc=1

return,1

end


;-----------------------------------------------------------------------
;--destroy object

pro ltc_plot::cleanup
dprint,'% LTC_PLOT::CLEANUP'
return
end

;-----------------------------------------------------------------------
;-- set plot properties

pro ltc_plot::set,over=over,ylog=ylog,all=all,enab_sum=enab_sum,dim1_sum=sum,$
             nsum=nsum,yrange=yrange, legend_loc=legend_loc, label=label

if is_number(all) then self.all= 0 > all < 1
if is_number(over) then self.over= 0 > over < 1
if is_number(ylog) then self.ylog= 0 > ylog < 1
if is_number(enab_sum) then self.enab_sum= 0 > enab_sum < 1
if is_number(sum) then self.sum= 0 > sum < 1
if is_number(nsum) then self.nsum=nsum > 0
if n_elements(yrange) eq 2 then self.yrange=yrange
if is_number(legend_loc) then self.legend_loc = 0 > legend_loc < 4
if is_string(label) then self.label = label

return & end

;---------------------------------------------------------------------------
;-- show LTC properties

pro ltc_plot::show

print,''
print,'LTC_PLOT properties:'
print,'-------------------'
print,'% PLOT_TYPE: ',self.plot_type
print,'% YRANGE: ',self.yrange
print,'% OVER/YLOG: ',self.over,self.ylog
print,'% ALL/SUM: ',self.all,self.sum
print,'% NSUM: ',self.nsum
print,''

return & end

;----------------------------------------------------------------------

pro ltc_plot__define

temp={ltc_plot,           $
      plot_type:'',       $     ;-- plot type
      yrange:[0.,0.],     $     ;-- data yrange
      over:0b,            $     ;-- plot as overlay
      ylog:0b,            $     ;-- plot as log
      all:0b,             $     ;-- plot all channels
      enab_sum: 0b, $  ;-- allow sum channels option
      sum:0b,             $     ;-- sum channels
      nsum:0l,            $     ;-- sum in time domain
      legend_loc: 0b, $   ;-- location for legend 0/1/2/3/4 = no legend/ topleft/topright/ bottomleft/bottomright
      label: '',               $  ;-- additional info to put in legend
      inherits gen}

return
end

