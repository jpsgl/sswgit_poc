;+
; Project     : SXI
;
; Name        : SXI__DEFINE
;
; Purpose     : Define an SXI data object
;
; Category    : Ancillary Synoptic Objects
;
; Syntax      : IDL> c=obj_new('sxi')
;
; History     : Written 16 Feb 2003, D. Zarro, (EER/GSFC)
;
; Contact     : dzarro@solar.stanford.edu
;-
;-----------------------------------------------------------------------------
;-- init 

function sxi::init,_ref_extra=extra

ret=self->hfits::init(_extra=extra)

if ret eq 0b then return,0b

;-- check if SXI branch installed
    
sxi_startup,err=err
if err eq '' then self.installed=1b
self->hset,/gateway,buffsize=2048l
       
return,ret

end

;---------------------------------------------------------------------------
;-- Set property method

pro sxi::hset,ngdc=ngdc,_ref_extra=extra
if is_number(ngdc) then self.ngdc=  0b > byte(ngdc) < 1b
if is_string(extra) then self->hfits::hset,_extra=extra
return & end

;----------------------------------------------------------------------------

pro sxi::cleanup

self->hfits::cleanup

return & end

;----------------------------------------------------------------------------
; SXI front-end FITS reader

pro sxi::read,file,data,_ref_extra=extra,ngdc=ngdc,remote=remote,err=err
err=''
if is_blank(file) then return
file=strtrim(file,2)
chk=loc_file(file,count=count)
ngdc=keyword_set(ngdc) or keyword_set(remote) or (count eq 0)

installed=self->hget(/installed) 
if not installed then begin
 warn=['$SSW/goes/sxig12 branch does not appear to be installed on this system.',$
       'Special SXI FITS readers will not be used resulting in images with',$
       'possibly inaccurate pointings.']
 xack,warn,/suppress
endif

no_copy=1-arg_present(data)

;-- read directly from NGDC if /NGDC

if ngdc then begin
 server=self->get_server(network=network,path=path,_extra=extra)
 if not network then return
 temp=parse_time(file,ymd=ymd)
 file_url=server+path+'/'+ymd+'/'+file_break(file)
 self->hfits::read,file_url,data,_extra=extra,no_copy=no_copy,err=err
endif else self->fits::read,file,data,_extra=extra,no_copy=no_copy,err=err

if err eq '' then self->set,/log_scale

return & end

;----------------------------------------------------------------------------
; SXI low-level FITS reader

pro sxi::mreadfits,file,data,header=header,index=index,_ref_extra=extra,$
                   nodata=nodata,err=err,dscale=dscale

forward_function sxig12_read,sxig12_read_one

installed=self->hget(/installed)

dscale=keyword_set(dscale)
noscale=1b-dscale

;-- if SXI branch not installed, use standard FITS reader

err=''
if not installed then begin
 self->fits::mreadfits,file,data,header=header,index=index,$
             _extra=extra,err=err,dscale=dscale
 return
endif

;-- /NODATA?

if keyword_set(nodata) then begin
 mrd_head,file,header,err=err,_extra=extra
 if err eq '' then index=fitshead2struct(header)
 return
endif else begin
 level1=stregex(file,'B_',/bool) eq 1b
 if level1 then data=sxig12_read_one(file,header,noscale=noscale,_extra=extra) else $
  data=sxig12_read(file,header,_extra=extra,noscale=noscale)
endelse
if is_string(header) then index=fitshead2struct(header)

return & end

;---------------------------------------------------------------------
;-- return SXI server & path

function sxi::get_server,_ref_extra=extra

return,sxi_server(_extra=extra)

end

;-----------------------------------------------------------------------
;-- list SXI files by searching  NGDC catalog

function sxi::list,tstart,tend,count=count,_extra=extra

tcat=self->list_cat(tstart,tend,count=count,_extra=extra)
if count gt 0 then begin
 return,tcat.fname+'.FTS'
endif else begin
 message,'No SXI files found',/cont
 return,''
endelse

end

;----------------------------------------------------------------------------
;-- HTTP search of SXI archive at NGDC

pro sxi::find,files,tstart,tend,level1=level1,level0=level0,$
               _ref_extra=extra,ngdc=ngdc

fid='_B'
if keyword_set(level0) then fid='_A'
pattern='.*SXI[^\.\\/]+'+fid+'.+\.FTS'

ngdc_sav=self->hget(/ngdc)
self->hset,ngdc=0
self->hset,ngdc=ngdc
self->hfits::find,files,tstart,tend,pattern=pattern,_extra=extra
self->hset,ngdc=ngdc_sav

return & end

;----------------------------------------------------------------------------
;-- SXI help

pro sxi::help

print,''
print,"IDL> sxi=obj_new('sxi')                         ;-- create object
print,'IDL> files=sxi->list(tstart [,tend]             ;-- list leve1 1 files at NGDC
print,'IDL> sxi->copy,file_name [,out_dir=out_dir]     ;-- download
print,'IDL> sxi->read,file_name                        ;-- read
print,'IDL> sxi->plot                                  ;-- plot
print,'IDL> map=sxi->get(/map)                         ;-- extract map
print,'IDL> data=sxi->get(/data)                       ;-- extract data
print,'IDL> obj_destroy,sxi                            ;-- destroy
print,''
self->filters

return & end

;--------------------------------------------------------------------------
;-- SXI dynamic catalog

pro sxi::cat,catalog,tstart,tend

catalog=-1

if (not valid_time(tstart)) or (not valid_time(tend)) then begin
 message,"Please enter start and end time (e.g. ->cat,catalog,'1-mar-03','2-mar-03')",/cont
 return
endif

;-- list remote file names at NGDC

ngdc_sav=self->hget(/ngdc)
self->hset,/ngdc
self->find,files,tstart,tend,count=count
self->hset,ngdc=ngdc_sav

if count eq 0 then return

;-- read their headers

self->read,files,index=catalog,/ngdc,/nodata

return & end

;------------------------------------------------------------------------------
;-- SXI copy from NGDC

pro sxi::copy,file,out_file,_ref_extra=extra,err=err,cancelled=cancelled,status=status

;-- determine server and search directories

status=0b
cancelled=0b
err=''

if is_blank(file) then begin
 err='Missing input filename'
 message,err,/cont
 return
endif

server=self->get_server(network=network,path=path,/verb,_extra=extra,err=err)
if not network then return

dfile=file_break(file)
temp=parse_time(dfile,ymd=ymd)
file_url=server+path+'/'+ymd+'/'+dfile

nfile=n_elements(file)
if is_string(out_file) then begin
 if n_elements(out_file) ne nfile then begin
  err='# of input and output filenames must match'
  message,err,/cont
  return
 endif
 ofile=out_file
endif else ofile = replicate('', nfile) ;jmm, 22-sep-2003

for i=0,nfile-1 do begin
 self->hfits::copy,file_url[i],ofile[i],_extra=extra,status=status,cancelled=cancelled,err=err
 if cancelled then return
endfor

return & end

;------------------------------------------------------------------------------
;-- write SXI file catalog on $SYNOP_DATA/sxi (sxi_yyyymmdd.fits)

pro sxi::write_cat,tdate,back=back,verbose=verbose,out_dir=out_dir

if is_dir(out_dir) then tdir=out_dir else tdir='$SYNOP_DATA/sxi'
if not test_dir(tdir,out=odir) then return

if valid_time(tdate) then dstart=anytim2utc(tdate) else get_utc,dstart

if is_number(back) then tback=long(back) else tback=0

;-- force searching at NGDC

ngdc_sav=self->hget(/ngdc)
cache_sav=self->hget(/cache)
self->hset,/ngdc,cache=0

dstart.time=0
for i=0,tback do begin
 dend=dstart
 dend.mjd=dend.mjd+1
 self->find,files,dstart,dend,sizes=sizes,times=times,count=count,pattern='',/ngdc
 if count gt 0 then begin
  get_ymd,files[0],ymd,/full
  out_file=concat_dir(odir,'sxi_'+ymd+'.fits')
  if keyword_set(verbose) then message,'Writing to '+out_file,/cont
  mwrfits,{files:files,sizes:sizes,times:times},out_file,/create
  chmod,out_file,/g_write,/g_read
 endif
 dstart.mjd=dstart.mjd-1
endfor

error=0
catch,error
catch,/cancel

self->hset,ngdc=ngdc_sav,cache=cache_sav

return & end


;------------------------------------------------------------------------------
;-- read SXI index file catalog

pro sxi::read_index,data,tdate,count=count,verbose=verbose,err=err,$
          no_cache=no_cache

common sxi_index,cache

if not obj_valid(cache) then cache=obj_new('fifo')

count=0 & err=''
if not valid_time(tdate) then get_utc,udate else udate=tdate
server=self->get_server(path=path,network=network,/full)
if not network then return

ymd=time2fid(udate,/full)
cat_file='INDEX_'+ymd+'_B_12.FTS'
cat_dir=time2fid(udate,/full,delim='/')
url_path=server+path+'/'+cat_dir
url_file=url_path+'/'+cat_file

;-- check if last file is cached. If not read it.

delvarx,data
if (1-keyword_set(no_cache)) then data=cache->get(cat_file)

if not is_struct(data) then begin
 if keyword_set(verbose) then message,'Reading '+url_file,/cont
 self->readfits,url_file,data,extension=1,err=err
 if is_struct(data) then cache->set,cat_file,data
endif

if is_struct(data) then count=n_elements(data.fname)

return & end

;------------------------------------------------------------------------------
;-- read SXI file catalog

pro sxi::read_cat,data,tdate,count=count,err=err,verbose=verbose,_extra=extra

count=0 & err=''
if not valid_time(tdate) then get_utc,udate else udate=tdate
server=synop_server(path=path,network=network,/full)
if not network then return

ymd=time2fid(udate,/full)
cat_file='sxi_'+ymd+'.fits'
cat_dir='/sxi/'
url_file=server+path+cat_dir+cat_file

verbose=keyword_set(verbose)
if verbose then message,'Reading '+url_file,/cont

self->readfits,url_file,data,extension=1,err=err,_extra=extra

if is_struct(data) then begin
 count=n_elements(data.files)
endif

return & end

;---------------------------------------------------------------------------
;-- redirect HTTP list method to lookup SYNOP server 

pro sxi::list,url,output,_ref_extra=extra,err=err

ngdc=self->hget(/ngdc)
err=''

if not ngdc then begin
 message,'Searching Synoptic data catalog...',/cont
 tdate=stregex(url,'[0-9]{4}/[0-9]{2}/[0-9]{2}',/extract)

;-- lookup metadata file listing at SYNOP archive

 self->read_cat,output,tdate,_extra=extra,err=err

 if err eq '' then return
endif

;-- failing that, perform remote listing of NGDC 

message,'Searching NGDC archives...',/cont
self->hfits::list,url,output,err=err,_extra=extra

return & end

;-----------------------------------------------------------------------------
;-- get NGDC catalog listing for input time range

function sxi::list_cat,tstart,tend,count=count,verbose=verbose,_extra=extra

count=0
verbose=keyword_set(verbose)
nearest=valid_time(tstart) and (not valid_time(tend))

d1=get_def_times(tstart,tend,dend=d2,/utc)
ndays=d2.mjd-d1.mjd+1
if nearest then ndays=1
td=d1
for i=0,ndays-1 do begin
 dcat=-1
 td.mjd=d1.mjd+i
; if verbose then message,'Reading NGDC catalog for '+anytim2utc(td,/vms,/date),/cont
 self->read_index,dcat,td,_extra=extra,verbose=verbose
 if is_struct(dcat) then tcat=merge_struct(tcat,dcat)
endfor

count=n_elements(tcat)

if (count gt 1) then begin

 tdate=tcat.date_obs
 if nearest then begin
  ts=anytim2utc(d1,/ccsds,/trunc)
  ts=strep(ts,'T',' ')
  chk1=where( (tdate ge ts), c1)
  chk2=where( (tdate le ts), c2)
  if (c1 gt 0) then begin
   tcat1=tcat[chk1[0]]
   f1=tcat1.date_obs
   d1=abs(anytim2tai(f1)-anytim2tai(ts))
  endif
  if c2 gt 0 then begin
   tcat2=tcat[chk2[c2-1]]
   f2=tcat2.date_obs
   d2=abs(anytim2tai(f2)-anytim2tai(ts))
  endif
  case 1 of 
   (c1 gt 0) and (c2 eq 0) : tcat=tcat1
   (c1 eq 0) and (c2 gt 0) : tcat=tcat2
   (c1 gt 0) and (c2 gt 0) : begin
     if d1 lt d2 then tcat=tcat1 else tcat=tcat2
    end
   else: message,'improbable case!!!!'
  endcase
  count=1
 endif else begin
  if (d1.time gt 0) or (d2.time gt 0) then begin
   ts=anytim2utc(d1,/ccsds,/trunc)
   ts=strep(ts,'T',' ')
   te=anytim2utc(d2,/ccsds,/trunc)
   te=strep(te,'T',' ')
   chk=where( (tdate ge ts) and $
              (tdate le te),count)
   if count gt 0 then tcat=tcat[chk]
  endif
 endelse
endif

if count eq 0 then tcat=-1
if count eq 1 then tcat=tcat[0]
if verbose then message,'Found '+trim(count)+' files',/cont

return,tcat
end

;---------------------------------------------------------------------------
;-- get SXI URL path

function sxi::url_path,file

server=self->get_server(path=path,/no_check,/full)
dfile=file_break(file)
temp=parse_time(dfile,ymd=ymd)
return,server+path+'/'+ymd+'/'+dfile+'.FTS'

end

;----------------------------------------------------------------------------
;-- get SXI image url nearest to specified time

function sxi::nearest,time,_extra=extra,err=err,back=back,filter=filter,$
              verbose=verbose

err=''
verbose=keyword_set(verbose)

if valid_time(time) then utc=anytim2utc(time) else get_utc,utc
if is_number(back) then tback=back else tback=1.
if verbose then message,'Searching for data nearest '+anytim2utc(utc,/vms),/cont

;-- start with current time and look back 

dend=utc & dend.time=0
dstart=dend & dstart.mjd=dstart.mjd-tback
tcat=self->list_cat(dstart,dend,count=count,_extra=extra)
if count eq 0 then begin
 err='No SXI data found at or near '+anytim2utc(utc,/vms)
 message,err,/cont
 return,''
endif

;-- get matching filter

if is_struct(extra) then begin
 self->list_filter,filters
 tags=tag_names(extra)
 chk=where_vector(tags,filters,count)
 if count gt 0 then filter=filters[chk[0]]
endif

if is_string(filter) then begin
 ss=where(stregex(tcat.wavelnth,strup(filter),/bool),count)
 if count eq 0 then begin
  err='No SXI data found matching '+filter
  message,err,/cont
  return,''
 endif
 tcat=tcat[ss]
endif

;-- get nearest time

ss=near_time(tcat.date_obs,utc)

tcat=tcat[ss]
file=self->url_path(tcat.fname)

if verbose then begin
 message,'Found SXI '+tcat.wavelnth+' data at '+ tcat.date_obs,/cont
endif

return,file & end

;------------------------------------------------------------------------------
; get latest SXI image

pro sxi::latest,ofile,out_dir=out_dir,_ref_extra=extra,err=err,hours=hours

err=''
;-- default to current directory

if is_blank(out_dir) then odir=curdir() else odir=out_dir
if not test_dir(odir,err=err,out=out) then return
odir=out

if is_number(hours) then mhours=hours else mhours=24.
get_utc,dend
dend=anytim2tai(dend)
dstart=dend
dstart=dstart-mhours*3600.

tcat=self->list_cat(dstart,dend,count=count,_extra=extra)

if count eq 0 then begin
 message,'No SXI files found within last '+trim(mhours)+' hours',/cont
 return
endif

;-- get most recent file or check if filter is selected

file=self->find_filter(tcat,_extra=extra,err=err)
if is_string(err) then return

ofile=concat_dir(odir,file)
self->copy,file,out_dir=odir,err=err,/no_change
if err ne '' then begin
 message,err,/cont
 return
endif
self->read,ofile,err=err,_extra=extra
if err ne '' then begin
 message,err,/cont
 return
endif
return & end

;--------------------------------------------------------------------------
;-- filter files based on wavelength and exposure time

function sxi::find_filter,cat,_extra=extra,filter=filter,$
         exptime=exptime,err=err,verbose=verbose

verbose=keyword_set(verbose)
err=''
file=''
ext='.FTS'
if not is_struct(cat) then return,''

;--- determine desired exposure

index=cat
if is_string(exptime) then begin
 chk=where_val(index.exptime,exptime,count)
 if count eq 0 then begin
  err='No images with matching exposure '+exptime+' found.'
  message,err,/cont
  return,''
 endif else index=cat[chk]
endif

count=n_elements(index)
files=index.fname+ext
exptimes=index.exptime
def_filter=index[count-1].wavelnth
def_exp=index[count-1].exptime
file=files[count-1]

;-- determine desired filter [def = P_THN_B]

if is_string(filter) then sfilters=strup(str2arr(filter,delim=',')) else $
 if is_struct(extra) then sfilters=tag_names(extra) 
if is_blank(sfilters) then sfilters='P_THN_B'

for i=0,n_elements(sfilters)-1 do begin
 chk=where(index.wavelnth eq sfilters[i],count)
 if count gt 0 then begin
  file=files[chk[count-1]]
  exptime=exptimes[chk[count-1]]
  if verbose then message,'Found match for '+sfilters[i]+' with '+trim(exptime)+' sec exposure.',/cont
  return,files[chk[count-1]] 
 endif
endfor

message,'No images matching '+arr2str(sfilters)+' found.',/cont
message,'Defaulting to '+def_filter+' with '+trim(def_exp)+' sec exposure.',/cont,/noname

return,file & end

;----------------------------------------------------------------------------

pro sxi::filters

self->list_filter,/print

return & end 

;----------------------------------------------------------------------------
;-- list SXI filters

pro sxi::list_filter,filters,print=print

filters=['P_THN_A','P_THN_B','P_MED_B','B_THN_B','B_MED','B_THK','RDSH','OPEN',$
         'B_THN_A','P_THK','P_MED_A']

if keyword_set(print) then begin
 print,'Available SXI filters: '
 print,'-----------------------'
 print,transpose(filters)
endif

return & end

;------------------------------------------------------------------------------
;-- SXI data structure (inherits from HFITS class)

pro sxi__define,void                 

void={sxi, installed:0b, ngdc:0b, inherits hfits}

return & end


