;+
; Project     : SOHO - CDS     
;                   
; Name        : DELETE_CDS_ADEF
;               
; Purpose     : Delete and free handles of a CDS Analysis Definition (ADEF)
;               
; Explanation : Handy to make sure that discarded ADEFs are not taking up
;               space by storing stuff on their handles.
;               
; Use         : DELETE_CDS_ADEF,ADEF
;    
; Inputs      : ADEF : CDS Analysis Definition
; 
; Opt. Inputs : None
;               
; Outputs     : None
;               
; Opt. Outputs: None
;               
; Keywords    : None
;
; Calls       : delvarx
;
; Common      : None
;               
; Restrictions: ...
;               
; Side effects: ...
;               
; Category    : QuickLook, Analysis
;               
; Prev. Hist. : None
;
; Written     : SVH Haugan, UiO, 29 September 1997
;               
; Modified    : Not yet.
;
; Version     : 1, 29 September 1997
;-            
PRO delete_cds_adef,adef
  
  IF datatype(adef) EQ 'STC' THEN BEGIN 
     IF handle_info(adef.windows_h,/valid_id) THEN handle_free,adef.windows_h
     IF handle_info(adef.fit_h,/valid_id) THEN handle_free,adef.fit_h
     
  END 
  delvarx,adef
END
