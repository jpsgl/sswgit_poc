;+
; Project     : SOHO - CDS     
;                   
; Name        : MK_CDS_ANALYSIS()
;               
; Purpose     : Make a CFIT analysis structure from CDS data
;               
; Explanation : This function extracts data from one data window of a QLDS,
;               along with other information, and returns it as an ANALYSIS
;               structure, ready for input to e.g., XCFIT_BLOCK.
;
;               WAVELENGTH ARRAY
;
;               A a wavelength array that is tilt-corrected (according to the
;               values in TILT_NIS1_DEMO & TILT_NIS2_DEMO) is calculated, with
;               the same dimension as the window data. It is expected that the
;               wavelength calculation will include e.g., mirror position
;               dependence some time in the future.
;
;               GRATING ORDER
;               
;               The wavelength array is divided by the value of the keyword
;               GR_ORDER, whose default value is taken from the DETDESC tag
;               describing the window that is extracted.
;
;               DATA WEIGHTS
;               
;               The routine calculates data weights according to the
;               theoretical Poisson noise levels. Note that due to the
;               smoothing of single-photon events over several pixels, the
;               error estimate is too high (too small weights) - but there is
;               no "better" model in existence at the moment.
;
;               DATA UNITS/CALIBRATION
;
;               (applies only to NIS data)
;
;               The input QLDS must have data that have been calibrated *no
;               further* than VDS_CALIB (i.e., VDS_DEBIAS/VDS_CALIB may have
;               been performed, but not NIS_CALIB). This is to ensure that
;               it's possible to calculate weights according to Poisson
;               statistics.
;
;               Output units may be selected by setting various calibration
;               flags, (DEBIAS and VDS_CALIB will have a permanent effect on
;               the QLDS!).
;
;               The calibration flags are:
;
;                   DEBIAS, VDS_CALIB, NOBURNIN, USE_COUNTS, CALIBRATE, ERGS,
;                   STERADIANS, ANGSTROMS.
;
;               If no flags are set, the output data will have the units of
;               the data in the QLDS on input.
;
;               The following units will result if the input QLDS has units
;               'ADC' (analog digital counts) by setting flags:
;
;                      DEBIAS     : DEBIASED-ADC
;
;                      VDS_CALIB  : PHOTON-EVENTS/PIXEL/SEC
;
;                      USE_COUNTS : PHOTON-EVENTS/PIXEL
;
;                      CALIBRATE  : PHOTONS/CM^2/SEC/ARCSEC^2
;
;               When calibrate is set, the keywords ERGS/STERADIANS/ANGSTROMS
;               may be set to modify the units - see NIS_CALIB for further
;               information.
;
;               PRE-CALIBRATED INPUT
;               
;               It is also possible to supply your own, pre-calibrated QLDS,
;               through the keyword QLDS_CALIBRATED - in that case, all
;               calibration keywords will be ignored (but the input QLDS must
;               still be no more than VDS_CALIB'ed, to enable weight
;               calculations).
;
;               To receive (for later re-use) the calibrated QLDS that is used
;               internally in this routine, set the keyword
;               /LEAVE_CALIBRATION, and the calibrated QLDS will be returned
;               through the QLDS_CALIBRATED keyword.
;
;               DATA CROPPING
;
;               A useful feature for analyzing parts of e.g., a full-spectrum
;               readout is the data cube cropping mechanism. A cropping of
;               *any* IDL data cube may be described by two LONG arrays with 7
;               elements each, representing the start and stop indices. A
;               cropping structure thus consists of two tags, e.g.:
;
;                  CROP = {B:LONARR(7),E:LONARR(7)}
;
;               and the cropping is performed as (with B=CROP.B & E=CROP.E):
;
;                  DATA = DATA(b(0):e(0),b(1):e(1),b(2):e(2),b(3):e(0),$
;                              b(0):e(0),b(0):e(0),b(0):e(0))
;
;               By supplying a cropping structure through the keyword CROP
;               this will be done automatically.
;               
; Use         : ANA = MK_CDS_ANALYSIS(QLDS,WINDOWI [,DOWNSAMPLE] [calib. kw])
;
;               then
;
;               XCFIT_BLOCK,ANA=ANA
;    
; Inputs      : QLDS : Quick Look Data Structure
;
;               WINDOWI : The window index of the window to extract.
;
; Opt. Inputs : DOWNSAMPLE : A factor to downsample the spatial/time
;                            dimensions of the data block (for smaller, test
;                            data sets). Not completely robust...
;
;
; Outputs     : Returns a CFIT ANALYSIS structure, containing wavelengths,
;               data, weights, various auxiliary information (ORIGIN/SCALE,
;               MISSING, etc) to be used by e.g., XCFIT_BLOCK.
;
; Opt. Outputs: 
;               
; Keywords    : QLDS_CALIBRATED : If supplied, this QLDS will be taken as the
;                                 source for the data cube, with no more
;                                 calibrations applied.
;
;               LEAVE_CALIBRATION : If set, QLDS_CALIBRATED will contain the
;                                   calibrated QLDS on output.
;                                   
;               DEBIAS : Perform VDS_DEBIAS on QLDS before extracting data
;
;               VDS_CALIB : Perform VDS_CALIB on QLDS before extracting data
;                           (also implies DEBIAS).
;                           
;               NOBURNIN : Passed on to VDS_CALIB
;
;               USE_COUNTS : Perform VDS_CALIB and multiply by exposure time
;                            to get units in PHOTON-EVENTS/PIXEL.
;
;               CALIBRATE : Perform NIS_CALIB (on a *copy* of the QLDS) before
;                           extracting data. Modify with /ERGS, /STERADIANS
;                           and /ANGSTROMS.
;
; Calls       : 
;
; Common      : None
;               
; Restrictions: ...
;               
; Side effects: ...
;               
; Category    : 
;               
; Prev. Hist. : None
;
; Written     : SVH Haugan, UiO, 
;               
; Modified    : Not yet.
;
; Version     : 1,  
;-            
;
; Routine for downsampling (up to 4-dimensional) data blocks
;
PRO mk_cds_analysis_dowsnamp,da,down,missing
  
  sz = size(da)
  
; Final sizes of 2nd, 3rd (4th) dimension after resampling
  
  fsz = (sz(2:sz(0))/down) > 1
  
  IF down GT 1 THEN BEGIN 
     
     ;; Downsample FIRST dimension by averaging
     
     ix = lindgen(fsz(0))*down
     dad = [da(*,ix,*,*),da(*,ix+1,*,*)] ;; Down *must* be > 1...
     FOR ii = 2,down-1 DO dad = [dad,da(*,ix+ii,*,*)]
     
     dad = dimreform(dad,[sz(1),down,fsz(0),sz(3:sz(0))])
     da = average(dad,2,missing=missing)
     
     ;; Downsample SECOND dimension by averaging
     iy = lindgen(fsz(1))*down
     dad = [da(*,*,iy,*),da(*,*,iy+1,*)]
     FOR ii = 2,down-1 DO dad = [dad,da(*,*,iy+ii,*)]
     
     IF sz(0) EQ 4 THEN dad = reform(dad,sz(1),down,fsz(0),fsz(1),sz(4)) $
     else dad = reform(dad,sz(1),down,fsz(0),fsz(1))
     
     da = average(dad,2,missing=missing)
     
     ;; Downsample DEL_TIME dimension if present
     
     if sz(0) eq 4 then begin
        it = lindgen(fsz(2))*down
        dad = [da(*,*,*,it),da(*,*,*,it+1)]
        FOR ii = 2,down-1 DO dad = [dad,da(*,*,*,it+ii)]
        
        dad = reform(dad,sz(1),down,fsz(0),fsz(1),fsz(2))
        da = average(dad,2,missing=missing)
     end
     
  END
END

;
FUNCTION mk_cds_analysis,qlds,win,down,$
                         debias=debias, $
                         vds_calib=vds_calib,noburnin=noburnin, $
                         use_counts=use_counts,$
                         calibrate=calibrate,$
                         ergs=ergs,steradians=steradians,angstroms=angstroms,$
                         qlds_calibrated=qlds_calib,$
                         leave_calibration=leave_calibration,$
                         crop=crop,gr_order=gr_order
  
  on_error,2
  IF !debug NE 0 THEN on_error,0
  
  IF n_params() LT 2 THEN BEGIN
     on_error,2
     message,"Use: ana = mk_cds_analysis(qlds,windowi)"
  END
  
  qlmgr,qlds,valid
  
  IF NOT valid THEN BEGIN
     on_error,2
     message,"First parameter must be a valid QLDS"
  END
  
  units_in = qlds.detdesc(win).units
  valid_units = ['ADC','DEBIASED-ADC','PHOTON-EVENTS/PIXEL/SEC',$
                 'COUNTS','SMOOTHED']
  
  nis = qlds.header.detector EQ 'NIS'
  
  IF total(strupcase(units_in) EQ valid_units) EQ 0 THEN BEGIN 
     on_error,2
     message,"Input QLDS must have data in one of the " + $
        "following units:",/continue
     message,arr2str(valid_units)
  END
  
  IF datatype(qlds_calib) EQ 'STC' THEN input_calib = 1 $
  ELSE input_calib = 0
  
  IF keyword_set(calibrate) OR keyword_set(use_counts) THEN vds_calib = 1
  
  IF keyword_set(vds_calib) THEN debias = 1
  
  IF nis AND keyword_set(debias) THEN BEGIN
     print,"Debiasing the input QLDS"
     vds_debias,qlds
  END
  
  IF nis AND keyword_set(vds_calib) THEN BEGIN
     print,"Using VDS_CALIB on input QLDS"
     vds_calib,qlds,noburnin=noburnin
  END
  
  ;; In this program, every gt_windata should be followed by a crop operation
  ;; (if do_crop is true), but at this point we have not decided on the
  ;; cropping yet -- so it follows a few lines further down.
  
  da = gt_windata(qlds,win)
  units = qlds.detdesc(win).units
  missing = qlds.detdesc(win).missing
  
  default,gr_order,qlds.detdesc(win).gr_order
  
  sz = size(da)
  
  IF NOT keyword_set(crop) THEN BEGIN
     ;; IDL v 4 allows 8-dimensional data, but not 8 downscripts!
     crop = {b:lonarr(7),e:lonarr(7)}
     crop.e = sz(1:sz(0))-1
  END
  
  ;; To save some space...
  
  z_crop = {b:lonarr(7),e:lonarr(7)}
  z_crop.e = sz(1:sz(0))-1 ;; This means zero crop
  
  do_crop = 0
  IF total(crop.b EQ z_crop.b AND crop.e EQ z_crop.e) LT 7 THEN do_crop = 1
  b = crop.b
  e = crop.e
  
  IF do_crop THEN da = da(b(0):e(0),b(1):e(1),b(2):e(2),b(3):e(3),b(4):e(4),$
                          b(5):e(5),b(6):e(6))
  
  missing = qlds.detdesc(win).missing
  
;
; This is the sampling factor (set to e.g., 2 when testing)
;
  
  if n_elements(down) ne 1 then down = 1
  
  IF down NE 1 THEN mk_cds_analysis_downsamp,da,down,missing
  
;
; This is the calculation of fractional noise (cf. swnote #49)
;

  IF nis THEN BEGIN 
     IF keyword_set(vds_calib) THEN BEGIN
        vds_cal_qlds = temporary(qlds)
     END ELSE BEGIN
        vds_cal_qlds = copy_qlds(qlds)
        print,"Executing VDS_CALIB for weight calculations"
        vds_calib,vds_cal_qlds,errmsg='',noburnin=noburnin
     END
     
     cmissing = vds_cal_qlds.detdesc(win).missing
     
     count_da = gt_windata(vds_cal_qlds,win)
     IF do_crop THEN $
        count_da = count_da(b(0):e(0),b(1):e(1),b(2):e(2),$
                            b(3):e(3),b(4):e(4),b(5):e(5),b(6):e(6))
  
     ix = where(count_da EQ cmissing)

     count_da = temporary(count_da)*(vds_cal_qlds.header.exptime+0.1165)
  
     IF ix(0) NE -1L THEN count_da(ix) = cmissing
  
     IF keyword_set(vds_calib) THEN BEGIN
        qlds = temporary(vds_cal_qlds)
     END ELSE BEGIN
        delete_qlds,vds_cal_qlds
     END
  END ELSE BEGIN
     ;; GIS
     count_da = da
     cmissing = missing
  END
     
  
  ;; Ok - noise from poisson stats \approx sqrt(count_da) But for low counts
  ;; (and certainly not negative counts!) this has to be modified.
  ;;
  ;; Also, using this formula tends to produce way to low chi^2 values (around
  ;; 0.1 or so) after fitting, so I've left out the factor of sqrt(2)
  ;; 
  ;; The formula below adds a constant noise factor to mimic bias noise
  ;;
  ;; Additional trouble is introduced by the fact that e.g., calibrated data
  ;; are not directly proportional to the counts (there is some spread).
  ;;
  ;; This is attempted fixed by introducing a noise floor level.
  
  IF down NE 1 THEN mk_cds_analysis_downsamp,count_da,down,cmissing
  
  counts_above = count_da GT 1.5 AND count_da NE cmissing
  
  ix = where(count_da EQ 0)
  IF ix(0) NE -1L THEN BEGIN
     count_da(ix) = max(count_da)
  END
  
  noise_model = 'fracnoise = sqrt(count_da > 1.5)/abs(count_da)'
  dummy = execute(noise_model)
  
  IF ix(0) NE -1L THEN BEGIN
     fracnoise(ix) = max(fracnoise)
  END
  
  noise_model = '   '+noise_model
  
  IF keyword_set(vds_calib) OR keyword_set(use_counts) OR $
     keyword_set(calibrate) THEN BEGIN
     
     IF NOT input_calib THEN qlds_calib = copy_qlds(qlds)
     
     IF keyword_set(calibrate) THEN BEGIN
        print,"Using calibrated data"
        IF nis AND NOT input_calib THEN $
           nis_calib,qlds_calib,ergs=ergs,angstroms=angstroms,$
           steradians=steradians
        
        da = gt_windata(qlds_calib,win)
        units = qlds_calib.detdesc(win).units
        missing = qlds.detdesc(win).missing
        IF do_crop THEN $
           da = da(b(0):e(0),b(1):e(1),b(2):e(2),b(3):e(3),b(4):e(4),$
                   b(5):e(5),b(6):e(6))
     END
     
     
     IF nis AND keyword_set(use_counts) THEN BEGIN
        ix = where(da EQ qlds_calib.detdesc(win).missing)
        da = temporary(da)*(qlds.header.exptime+0.1165)
        units = repstr(units,'/SEC','')
        IF ix(0) NE -1L THEN da(ix) = qlds.detdesc(win).missing
     END
     
     IF NOT keyword_set(leave_calibration) AND NOT input_calib THEN $
        delete_qlds,qlds_calib
     
     IF down NE 1 THEN mk_cds_analysis_downsamp,da,down,missing
  END
  
  noise = temporary(fracnoise)*abs(da)
  
  noise_above_ix= where(counts_above,npix)
  
  IF npix GT 0 AND npix LT n_elements(counts_above) THEN BEGIN
     noise_floor = min(noise(noise_above_ix))
     noise = noise > noise_floor
  END
  
;  trouble = noise EQ 0 OR da EQ 0 OR da EQ missing
;  ix = where(trouble)
;  IF ix(0) NE -1L THEN noise(ix) = min(noise(where(1b-trouble)))
  
  wts = 1.0/temporary(noise)^2
  
  sz = size(da)
  
  ;; Make detector x/y arrays for calculating tilted wavelength array
  
  if sz(0) eq 4 then begin
     detx = qlds.detdesc(win).detx + dimrebin(findgen(sz(1),1,1,1),sz(1:4))
     detx = detx+crop.b(0)
     IF nis THEN BEGIN 
        dety = qlds.detdesc(win).dety + $
           dimrebin(findgen(1,1,sz(3),1)*down,sz(1:4))
        dety = dety + crop.b(2)
     END ELSE dety = 0
  end else begin
     detx = qlds.detdesc(win).detx + dimrebin(findgen(sz(1),1,1),sz(1:3))
     detx = detx+crop.b(0)
     IF nis THEN BEGIN
        dety = qlds.detdesc(win).dety +  $
           dimrebin(findgen(1,1,sz(3))*down,sz(1:3))
        dety = dety + crop.b(2)
     END ELSE dety = 0
  end

;; Which detector are we using?
  
  IF nis THEN BEGIN
     det = (['N1','N2'])(qlds.detdesc(win).dety LT 512)
  END ELSE BEGIN
     det = (['G1','G2','G3','G4'])(detx(0)/2048)
  END
  
; Calculate tilt for all pixels - values taken from TILT_NIS1_DEMO and
; TILT_NIS2_DEMO
  
  CASE det OF 
     'N1': tilt = 0.0075942 - 6.135603e-6*detx
     'N2': tilt = 0.00397 - 4.763135e-6*detx + 4.764016e-9 * detx^2
     ELSE: tilt = 0
  END
  
;; Find the median detector y position - use as zero point for tilt correction

  med_dety = median(dety(0,0,*))

  ndetx = temporary(detx) + (temporary(dety)-med_dety) * temporary(tilt)

  lam = float(pix2wave(det,ndetx MOD 2048,/nolimit,/throwaway))
  
  IF gr_order NE 1 THEN BEGIN
     lam = lam/gr_order
     message,"Warning: Grating order correction applied to lambda array",$
        /continue
  END
  
  ;; Put the stuff into the analysis structure
  
  ana = mk_analysis(lam,da,wts,fit,missing,/no_copy)
  
  label = qlds.detdesc(win).label
  
  handle_value,ana.history_h,h
  h = [h,'','Noise model used to generate weights:','','    '+noise_model]
  h = [h,'',$
       'Window extracted: '+label+' (win='+trim(win)+')',$
       '',$
       'Units in QLDS on input:'+units_in,$
       'Units in data block   :'+units]
  
  handle_value,ana.history_h,h,/set
  

  
  ;; Make default filename for this analysis
  
  break_file,qlds.header.filename,disk,dir,fnam,ext
  basename = fnam + '_win'+trim(win)
  IF keyword_set(calibrate) THEN basename = basename+'_cal'
  
  dir = getenv("CDS_ANALYSIS")
  IF dir EQ '' THEN BEGIN
     message,"You should set $CDS_ANALYSIS to point to a directory",$
        /informational
  END
  
  ana.filename = concat_dir(dir,basename+'.ana')
  ana.datasource = qlds.header.filename
  ana.definition = ''
  ana.label = qlds.detdesc(win).label
  
  origin = qlds.detdesc(0).origin
  scale = qlds.detdesc(0).spacing*[1,down,down,down]
  phys_scale = [0,1,1,0] * (origin EQ origin)
  dimnames = qlds.detdesc(0).axes
  
  handle_value,ana.origin_h,origin,/set
  handle_value,ana.scale_h,scale,/set
  handle_value,ana.phys_scale_h,phys_scale,/set
  handle_value,ana.dimnames_h,dimnames,/set
  
  return,ana
END





