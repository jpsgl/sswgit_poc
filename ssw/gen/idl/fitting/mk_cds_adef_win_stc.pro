;+
; Project     : SOHO - CDS     
;                   
; Name        : MK_CDS_ADEF_WIN_STC
;               
; Purpose     : Create CDS Analysis Definition (ADEF) window structure
;               
; Explanation : Used to create a structure describing one data block (data
;               window) to be included into a CDS Analysis.
;
;               The structure definition (with default values):
;
;               {CDS_ADEF_WINDOW,
;                IX : 0L          ; A long containing the WINDOW INDEX 
;                GR_ORDER:1       ; Integer telling what grating order is to
;                                 ; be used when generating the lambda array.
;                LABEL: ''        ; Window label
;                CROP: CROP       ; A structure describing how the data window
;                                 ; is to be cropped when the data are included
;                                 ; into an analysis.
;               
; Use         : WIN_STC = MK_CDS_ADEF_WIN_STC()
;    
; Inputs      : None
; 
; Opt. Inputs : None.
;               
; Outputs     : Returns a CDS ADEF window structure.
;               
; Opt. Outputs: None.
;               
; Keywords    : None
;
; Calls       : None
;
; Common      : None
;               
; Restrictions: ...
;               
; Side effects: ...
;               
; Category    : QuickLook, Analysis
;               
; Prev. Hist. : None
;
; Written     : SVH Haugan, UiO, 29 September 1997
;               
; Modified    : Not yet.
;
; Version     : 1, 29 September 1997
;-            
FUNCTION mk_cds_adef_win_stc
  
  crop = {cds_adef_crop,b:lonarr(7),e:lonarr(7)}
  
  win = {CDS_ADEF_WINDOW,$
         ix:0L,$
         gr_order:1,$
         label:'',$
         crop:crop}
  
  return,win
END
