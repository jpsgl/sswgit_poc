;+
; Project     : SOHO - CDS     
;                   
; Name        : XCDS_ANALYSIS
;               
; Purpose     : Widget interface to do analyses of CDS data.
;               
; Explanation : This program defines CDS Analysis Definition (CDS ADEF)
;               structures.
;      
;               The program is a front-end to the routine MK_CDS_ANALYSIS(),
;               but with a few added features to allow (semi-)automatic
;               handling of cosmic rays, automatic naming of analysis files,
;               and multi-window analysis.
;               
;               An ADEF structure may be applied to all data sets with the
;               same raster id/variation, although a specific data set (QLDS)
;               must be present to enable editing of the ADEF.
;               
;               Here is a short description of the various sections:
;               
;               ANALYSIS FILE NAMES & COSMIC RAY TREATMENT
;               
;               The "Pattern for analysis file names" defines how to infer a
;               file name for storing an analysis, given the ADEF file
;               name/path and a specific data set (raster). Push the button to
;               edit, and to get a more detailed description.
;               
;               The rules for how to deal with cosmic rays function similarly
;               to the analysis file name pattern - push the button to edit
;               and to get more detailed information.
;               
;               DATA TREATMENT - DEBIAS/CALIBRATION
;               
;               The next section (with e.g., DEBIAS:ON etc.) deals with how
;               the data from the QLDS is to be treated before extracting the
;               data block for analysis. All of these switches are keywords to
;               MK_CDS_ANALYSIS().
;               
;               WINDOW LIST
;               
;               The section with the "Add this window" button is where you
;               determine which data windows to include in the analysis. Use
;               the pulldown menu to select a window, and then press
;               "Add....", and it will appear in the window list below.
;               
;               You may add the same window more than one time if you like.
;               
;               The windows will be sorted in ascending wavelength order.
;               
;               You may also choose to intepret the data in the window as a
;               second-order line (dividing the wavelengths by two) by
;               pressing the button under the heading "GR_ORDER".
;               
;               To select a subset of the whole data window, press the "Crop"
;               button.  This will start a program called XCROP_CUBE that
;               allows you to define which part of the data window is to be
;               included in the analysis.
;               
;               If you would like to delete a window from the window list,
;               guess what you should do...
;               
;               APPLY ANALYSIS DEFINITION
;               
;               To apply the analysis definition to the current QLDS, select
;               "Apply analysis : Generate analysis and edit in XCFIT_BLOCK"
;               
;               If you would like to load an existing analysis matching this
;               analysis definition, press "Load analysis". This may be done
;               even if no QLDS was passed to the program.
;               
; Use         : XCDS_ANALYSIS,ADEF,ANALYSIS,QLDS=QLDS
;    
; Inputs      : Both of these are input/output:
;
;               ADEF : Analysis definition structure
;
;               ANALYSIS : CFIT analysis structure generated from the analysis
;                          definition.
;
; Opt. Inputs : QLDS : QuickLook Data Structure. If the qlds is managed by the
;                      calling widget program, XCDS_ANALYSIS will fall
;                      through, expecting that the QLDS will continue to be
;                      available through the Quick Look memory management
;                      system. 
;               
; Outputs     : ADEF, ANALYSIS
;               
; Opt. Outputs: None
;               
; Keywords    : None
;
; Calls       : qlmgr, exist(), datatype(), handle_killer_hookup
;               qlds_managed() qlds_report default xmanager qlds_fetch
;               xalive()
;
; Common      : None
;               
; Restrictions: Uses the quick look data storage system, see e.g.,
;               QLDS_REPORT.
;               
; Side effects: ...
;               
; Category    : QuickLook, Analysis
;               
; Prev. Hist. : None
;
; Written     : SVH Haugan, UiO, 25 September -- 17 October 1997
;               
; Modified    : Version 2, SVHH, 15 December 1997
;                       Circumventing IDL v 5 widget problems...
;
; Version     : 2, 15 December 1997
;-            

PRO xcds_analysis_show_status,info
  handle_value,info.int.adef_h,adef
  
  xupdate,info.int.debias_id,0
  
  onoff = ['ON','OFF']
  
  widget_control,info.int.fill_cosmic_id,set_value=$
     'FILL_COSMIC:'+onoff(adef.fill_cosmic EQ 0)
  
  adef.calibrate = $
     adef.calibrate OR adef.ergs OR adef.steradians OR adef.angstroms
  
  adef.vds_calib = adef.vds_calib OR adef.calibrate OR adef.use_counts
  
  adef.use_counts = adef.use_counts AND adef.debias AND NOT adef.calibrate
  adef.noburnin = adef.noburnin AND adef.vds_calib
  
  adef.debias = adef.debias OR adef.vds_calib
  
  handle_value,info.int.adef_h,adef,/set
  
  widget_control,info.int.debias_id,set_value = $
     'DEBIAS:'+onoff(adef.debias EQ 0)
  widget_control,info.int.vds_calib_id,set_value = $
     'VDS_CALIB:'+onoff(adef.vds_calib EQ 0)
  widget_control,info.int.use_counts_id,set_value = $
     'USE_COUNTS:'+onoff(adef.use_counts EQ 0)
  widget_control,info.int.noburnin_id,set_value = $
     'NOBURNIN:'+onoff(adef.noburnin EQ 0)
  widget_control,info.int.calibrate_id,set_value = $
     'CALIBRATE:'+onoff(adef.calibrate EQ 0)
  widget_control,info.int.ergs_id,set_value = $
     'ERGS:'+onoff(adef.ergs EQ 0)
  widget_control,info.int.steradians_id,set_value = $
     'STERADIANS:'+onoff(adef.steradians EQ 0)
  widget_control,info.int.angstroms_id,set_value = $
     'ANGSTROMS:'+onoff(adef.angstroms EQ 0)
  
  xupdate,info.int.debias_id,1
END

PRO xcds_analysis_show_filenames,info
  COMMON QLSAVE,qlds,saved
  
  qlds_checkin,info.int.ql_no,/test
  
  xupdate,info.int.top,0
  
  handle_value,info.int.adef_h,adef,/no_copy
  
  ;; Display current definition file name
  widget_control,info.int.filename_lid,set_value='File:'+adef.filename
  
  ;; Find current name of analysis file (if generated or loaded)
  cds_adef_parsefile,adef,anafile,qlds,iname=adef.ana_filename
  ;; And current appearance of the "cosmics" string
  cds_adef_parsefile,adef,cosmics,qlds,iname=adef.cosmic
  
  widget_control,info.int.def_afile_id,set_value=adef.ana_filename
  widget_control,info.int.def_afile_lid,set_value='=> '+anafile
  
  widget_control,info.int.cosmic_id,set_value=adef.cosmic
  widget_control,info.int.cosmic_lid,set_value='=> '+cosmics
  
  ;; Find any existing analysis, display it's filename
  handle_value,info.int.analysis_h,ana,/no_copy
  
  IF exist(ana) THEN BEGIN
     widget_control,info.int.ana_filename_lid,set_value='File:'+ana.filename
     widget_control,info.int.edit_id,sensitive=1
     widget_control,info.int.have_analysis_id,set_value=1
  END ELSE BEGIN
     widget_control,info.int.ana_filename_lid,set_value='File:'
     widget_control,info.int.edit_id,sensitive=0
     widget_control,info.int.have_analysis_id,set_value=0
  END
  
  
  handle_value,info.int.analysis_h,ana,/set,/no_copy
  handle_value,info.int.adef_h,adef,/set,/no_copy
     
  xupdate,info.int.top,1
END
  

PRO xcds_analysis_show_windows,info
  COMMON QLSAVE,qlds,saved
  
  qlds_checkin,info.int.ql_no,/test
  
  handle_value,info.int.adef_h,adef,/no_copy
  
  handle_value,adef.windows_h,windows,/no_copy
  
  xupdate,info.int.window_base,0
  
  current = widget_info(info.int.window_base,/child)
  IF widget_info(current,/valid_id) THEN widget_control,current,/destroy
  
  new = widget_base(info.int.window_base,/row)
  
  nc = 6
  
  c = lonarr(nc)
  
  FOR i = 0,nc-1 DO c(i) = widget_base(new,/column,/frame)
  
  dummy = widget_label(c(0),value='Index')
  dummy = widget_label(c(1),value='Label')
  dummy = widget_label(c(2),value='GR_ORDER')
  dummy = widget_label(c(3),value='Push to crop')
  dummy = widget_label(c(4),value='Current size')
  dummy = widget_label(c(5),value='Push to delete')
  
  orders = ['1','2']
  corders = ':'+orders
  
  FOR i = 0,n_elements(windows)-1 DO BEGIN
     itx = ':'+trim(i)
     
     sz = windows(i).crop.e-windows(i).crop.b+1
     sztx = trim(sz(0))
     last_nonsingular = (reverse(where(sz GT 1)))(0)
     FOR j = 1,last_nonsingular DO $
        sztx = sztx+' x '+trim(sz(j))
     
     index = widget_label(c(0),value=trim(windows(i).ix))
     label = cw_enterb(c(1),value=windows(i).label,uvalue='WINLABEL'+itx,$
                       instruct='Enter the name/label for this data window')
     
     grorder = cw_flipswitch(c(2),value=orders,uvalue='GR_ORDER'+itx+corders)
     cropb = widget_button(c(3),value='Crop',uvalue='CROP'+itx)
     sizeb = widget_label(c(4),value=sztx)
     delete = widget_button(c(5),value='Delete',uvalue='DELETE'+itx)
     IF info.int.windowi EQ -1 THEN BEGIN
        widget_control,delete,sensitive=0
        widget_control,cropb,sensitive=0
        widget_control,grorder,sensitive=0
     END
     
     widget_control,grorder, $
        set_value=('GR_ORDER'+itx+corders)(windows(i).gr_order-1)
     
     all = [index,label,grorder,sizeb,cropb,delete]
     geo = widget_info(all,/geometry)
     maxy = max(geo(*).scr_ysize)
     FOR ii = 0,n_elements(all)-1 DO widget_control,all(ii),scr_ysize=maxy
  END
  
  handle_value,adef.windows_h,windows,/set,/no_copy
  handle_value,info.int.adef_h,adef,/set,/no_copy

  xupdate,info.int.window_base,1
END

PRO xcds_analysis_sortwin,info
  COMMON QLSAVE,qlds,saved
  qlds_checkin,info.int.ql_no,/test
  
  IF NOT exist(qlds) THEN return
  
  handle_value,info.int.adef_h,adef,/no_copy
  handle_value,adef.windows_h,windows,/no_copy
  
  ;; Sort according to wavelength of first (cropped) pixel
  
  nwin = n_elements(windows)
  
  wavelengths = fltarr(nwin)
  FOR i = 0,nwin-1 DO BEGIN 
     dummy = gt_spectrum(qlds,window=windows(i).ix,$
                         xix=0,yix=0,tix=0,$ ; Make no mistake..
                         lambda=lam)
     wavelengths(i) = lam(windows(i).crop.b(0))/windows(i).gr_order
  ENDFOR
  
  windows = windows(sort(wavelengths))
  
  handle_value,adef.windows_h,windows,/set,/no_copy
  handle_value,info.int.adef_h,adef,/set,/no_copy
END


FUNCTION xcds_analysis_crop_ok,info
  handle_value,info.int.adef_h,adef,/no_copy
  handle_value,adef.windows_h,windows,/no_copy
  
  ok = 1
  
  IF n_elements(windows) GT 1 THEN BEGIN 
     sz = (windows(0).crop.e-windows(0).crop.b)(1:*)
     FOR i = 1,n_elements(windows)-1 DO BEGIN
        this_sz = (windows(i).crop.e-windows(i).crop.b)(1:*)
        IF total(this_sz NE sz) NE 0 THEN ok = 0
     END
  ENDIF
  
  handle_value,adef.windows_h,windows,/set,/no_copy
  handle_value,info.int.adef_h,adef,/set,/no_copy
  return,ok
END


PRO xcds_analysis_check_crop,info
  
  IF NOT xcds_analysis_crop_ok(info) THEN BEGIN
     xack,['In order to analyze more than one data window as a',$
           'single data cube, their dimensions must be compatible.',$
           '',$
           'All dimensions except the first one must have identical sizes.',$
           '',$
           'There is at least one window in this selection that has a',$
           'size differing from the others']
  END
END


PRO xcds_analysis_addwin,info 
  COMMON QLSAVE,qlds,saved
 
  qlds_checkin,info.int.ql_no,/test
  
  IF NOT exist(qlds) THEN return
  
  handle_value,info.int.adef_h,adef,/no_copy
  
  handle_value,adef.windows_h,windows,/no_copy
  
  wi = info.int.windowi
  
  size = abs(qlds.detdesc(wi).ixstop)-qlds.detdesc(wi).ixstart+1
  label = qlds.detdesc(wi).label
  
  gr_order = qlds.detdesc(info.int.windowi).gr_order
  
  ;; Get a window structure
  thiswin = mk_cds_adef_win_stc()
  
  ;; Put the data in place
  thiswin.ix = info.int.windowi
  thiswin.gr_order = gr_order
  thiswin.label = label
  thiswin.crop.e = size-1
  
  IF NOT exist(windows) THEN windows = thiswin $
  ELSE                       windows = [windows,thiswin]
  
  adef.filename = ''
  
  handle_value,adef.windows_h,windows,/set,/no_copy
  
  handle_value,info.int.adef_h,adef,/set,/no_copy
  
  xcds_analysis_sortwin,info
  xcds_analysis_check_crop,info
END



PRO xcds_analysis_cropwin,info,i
  COMMON QLSAVE,qlds,saved
  
  qlds_checkin,info.int.ql_no,/test
  
  IF NOT exist(qlds) THEN return
  
  handle_value,info.int.adef_h,adef,/no_copy
  handle_value,adef.windows_h,windows,/no_copy
  
  thiswin = windows(i)
  wi = thiswin.ix
  
  crop = thiswin.crop
  da = gt_windata(qlds,wi)
  origin = qlds.detdesc(wi).origin
  scale = qlds.detdesc(wi).spacing
  phys_scale = [0b,1b,1b,0b]*(scale EQ scale) 
  
  xcrop_cube,da,crop,missing=qlds.detdesc(wi).missing
  
  windows(i).crop = crop
  
  handle_value,adef.windows_h,windows,/set,/no_copy
  handle_value,info.int.adef_h,adef,/set,/no_copy
  
  xcds_analysis_check_crop,info
  
  xcds_analysis_show_windows,info
END


PRO xcds_analysis_load_analysis,info
  COMMON QLSAVE,qlds,saved
  
  ;; Get QLDS
  qlds_checkin,info.int.ql_no,/test
  
  ;; Get analysis definition
  handle_value,info.int.adef_h,adef,/no_copy
  
  ;; Make a new analysis structure (delete it if unsuccessful)
  ana = mk_analysis()
  
  ;; Find default file name
  
  cds_adef_parsefile,adef,ana_file,qlds,iname=adef.ana_filename
  ana.filename = ana_file
  
  ;; Attempt restore
  ;; 
  new_ana = restore_analysis(ana)
  
  IF datatype(new_ana) EQ 'STC' THEN BEGIN
     ;; Delete old analysis
     handle_value,info.int.analysis_h,old_ana,/no_copy
     IF exist(old_ana) THEN BEGIN
        old_ana.fit_h = 0 & delete_analysis,old_ana
     END
     ;; Copy fit structure from restored analysis
     handle_value,new_ana.fit_h,fit
     handle_value,adef.fit_h,fit,/set,/no_copy
     
     ;; Store new analysis
     ;; 
     handle_value,info.int.analysis_h,new_ana,/set,/no_copy
  END ELSE BEGIN
     ;; Delete new fit (no success)
     delete_analysis,ana
  END
  
  ;; Put back analysis definition
  handle_value,info.int.adef_h,adef,/set,/no_copy
  
  ;; Show changes
  xcds_analysis_show_filenames,info
  xcds_analysis_show_status,info
END

PRO xcds_analysis_generate,info
  COMMON QLSAVE,qlds,saved
  
  qlds_checkin,info.int.ql_no,/test
  IF NOT exist(qlds) THEN return
  
  IF NOT xcds_analysis_crop_ok(info) THEN BEGIN
     xcds_analysis_check_crop,info
     return
  END
  
  handle_value,info.int.analysis_h,ana,/no_copy
  IF exist(ana) THEN BEGIN
     ana.fit_h = 0              ;!!
     delete_analysis,ana
     widget_control,info.int.have_analysis_id,set_value=0
  END
  handle_value,info.int.adef_h,adef,/no_copy
  handle_value,adef.windows_h,windows,/no_copy
  IF NOT exist(windows) THEN BEGIN
     handle_value,adef.windows_h,windows,/set,/no_copy
     handle_value,info.int.adef_h,adef,/set,/no_copy
     xack,['Cannot generate an analysis with zero windows selected!']
     return
  END
  handle_value,adef.windows_h,windows,/set,/no_copy
  
  ana = apply_cds_adef(adef,qlds=qlds,/link_fit)
  handle_value,info.int.adef_h,adef,/set,/no_copy
  xcfit_block,analysis=ana  ;; Always modal
  handle_value,info.int.analysis_h,ana,/set,/no_copy
  xcds_analysis_show_filenames,info
END 

PRO xcds_analysis_clean,id
  widget_control,id,get_uvalue=info,/no_copy
  IF NOT exist(info) THEN BEGIN
     print,"Info structure disappeared"
     return
  END
  
  help,calls = calls ;; Figure out why we're dying
  ix = where(strpos(calls,'XCDS_ANALYSIS') GE 0)
  IF ix(0) EQ -1 THEN info.int.freebie = 1b  ;; No main program, we're doomed!
  
  ix = where(strpos(calls,'XKILL') GE 0)
  IF ix(0) NE -1 THEN info.int.freebie = 1b ;; We're being slayed!
  
  ;; Free handles etc. if already exited from the main
  ;; program - also do a SAVE_AS operation first
  
  explain = $
     ['The XCDS_ANALYSIS widget is dying without being able to return',$
      'the analysis definition (ADEF) to the caller (since the call returned',$
      'once the widget was built). You may wish to save the ADEF structure',$
      'for future use if you have made changes to it after it was saved last',$
      'time.']
  
  IF info.int.freebie THEN BEGIN
     handle_value,info.int.adef_h,adef
     IF exist(adef) THEN BEGIN
        xack,explain,/modal
        save_cds_adef,adef,/save_as
     END
     handle_free,info.int.adef_h
     handle_value,info.int.analysis_h,ana
     IF exist(ana) THEN BEGIN
        message,"Deleting analysis",/continue
        delete_analysis,ana
     END
     handle_free,info.int.analysis_h
  END ELSE BEGIN
     ;; nothing
     
  END 
  
END

PRO xcds_analysis_help,top
  helptx = $
     ['',$
      'This program defines CDS Analysis Definition (CDS ADEF) structures.',$
      '',$
      'The program is a front-end to the routine MK_CDS_ANALYSIS(), but',$
      'with a few added features to allow (semi-)automatic handling of',$
      'cosmic rays, automatic naming of analysis files, and multi-window',$
      'analysis.',$
      '',$
      'An ADEF structure may be applied to all data sets with the same',$
      'raster id/variation, although a specific data set (QLDS) must be',$
      'present to enable editing of the ADEF.',$
      '',$
      'Here is a short description of the various sections:',$
      '',$
      'ANALYSIS FILE NAMES & COSMIC RAY TREATMENT',$
      '',$
      'The "Pattern for analysis file names" defines how to infer a file',$
      'name for storing an analysis, given the ADEF file name/path and',$
      'a specific data set (raster). Push the button to edit, and to get',$
      'a more detailed description.',$
      '',$
      'The rules for how to deal with cosmic rays function similarly to the',$
      'analysis file name pattern - push the button to edit and to get more',$
      'detailed information.',$
      '',$
      'DATA TREATMENT - DEBIAS/CALIBRATION',$
      '',$
      'The next section (with e.g., DEBIAS:ON etc.) deals with how the data',$
      'from the QLDS is to be treated before extracting the data block for ',$
      'analysis. All of these switches are keywords to MK_CDS_ANALYSIS().',$
      '',$
      'WINDOW LIST',$
      '',$
      'The section with the "Add this window" button is where you determine',$
      'which data windows to include in the analysis. Use the pulldown menu',$
      'to select a window, and then press "Add....", and it will appear in',$
      'the window list below.',$
      '',$
      'You may add the same window more than one time if you like.',$
      '',$
      'The windows will be sorted in ascending wavelength order.',$
      '',$
      'You may also choose to intepret the data in the window as a ',$
      'second-order line (dividing the wavelengths by two) by pressing ',$
      'the button under the heading "GR_ORDER".',$
      '',$
      'To select a subset of the whole data window, press the "Crop" button.',$
      'This will start a program called XCROP_CUBE that allows you to define',$
      'which part of the data window is to be included in the analysis.',$
      '',$
      'If you would like to delete a window from the window list, guess what',$
      'you should do...',$
      '',$
      'APPLY ANALYSIS DEFINITION',$
      '',$
      'To apply the analysis definition to the current QLDS, select',$
      '"Apply analysis : Generate analysis and edit in XCFIT_BLOCK"',$
      '',$
      'If you would like to load an existing analysis matching this',$
      'analysis definition, press "Load analysis". This may be done even if',$
      'no QLDS was passed to the program.',$
      '']
  
  xtext,' '+helptx,group=top,/scroll

END

  
PRO xcds_analysis_event,ev
  widget_control,ev.top,get_uvalue=info,/no_copy
  
  widget_control,ev.id,get_uvalue=uvalue
  
  uvalue = str_sep(uvalue,':')
  
  CASE uvalue(0) OF 
  'QUIT':BEGIN 
     widget_control,ev.top,set_uvalue=info,/no_copy ;; So the callback works
     widget_control,ev.top,/destroy
     return
     ENDCASE
     
  'HELP':BEGIN
     xcds_analysis_help,ev.top
     ENDCASE
     
  'SAVE':BEGIN
     handle_value,info.int.adef_h,adef,/no_copy
     save_cds_adef,adef
     handle_value,info.int.adef_h,adef,/set,/no_copy
     xcds_analysis_show_filenames,info
     ENDCASE
     
  'SAVE_AS':BEGIN
     handle_value,info.int.adef_h,adef,/no_copy
     save_cds_adef,adef,/save_as
     handle_value,info.int.adef_h,adef,/set,/no_copy
     xcds_analysis_show_filenames,info
     ENDCASE
     
  'RESTORE':BEGIN
     handle_value,info.int.adef_h,adef,/no_copy
     new_adef = restore_cds_adef(adef,/other)
     IF datatype(new_adef) EQ 'STC' THEN adef = new_adef
     handle_value,info.int.adef_h,adef,/set,/no_copy
     xcds_analysis_show_windows,info
     change = 1
     ENDCASE
     
  'ANA_FILENAME':BEGIN
     handle_value,info.int.adef_h,adef,/no_copy
     adef.ana_filename = ev.value
     handle_value,info.int.adef_h,adef,/set,/no_copy
     xcds_analysis_show_filenames,info
     ENDCASE
     
  'DEBIAS':BEGIN
     handle_value,info.int.adef_h,adef,/no_copy
     adef.debias = uvalue(1) EQ 'ON'
     handle_value,info.int.adef_h,adef,/set,/no_copy
     xcds_analysis_show_status,info
     ENDCASE
     
  'VDS_CALIB':BEGIN
     handle_value,info.int.adef_h,adef,/no_copy
     adef.vds_calib = uvalue(1) EQ 'ON'
     IF NOT adef.vds_calib THEN BEGIN
        adef.use_counts = (adef.steradians = 0b)
        adef.ergs = (adef.angstroms = (adef.calibrate = 0b))
     END
     handle_value,info.int.adef_h,adef,/set,/no_copy
     xcds_analysis_show_status,info
     ENDCASE
     
  'USE_COUNTS':BEGIN
     handle_value,info.int.adef_h,adef,/no_copy
     adef.use_counts = uvalue(1) EQ 'ON'
     IF adef.use_counts THEN BEGIN
        adef.steradians = 0b
        adef.ergs = (adef.angstroms = (adef.calibrate = 0b))
     END
     handle_value,info.int.adef_h,adef,/set,/no_copy
     xcds_analysis_show_status,info
     ENDCASE

  'NOBURNIN':BEGIN
     handle_value,info.int.adef_h,adef,/no_copy
     adef.noburnin = uvalue(1) EQ 'ON'
     IF adef.noburnin THEN BEGIN
        adef.vds_calib = 1b
     END
     handle_value,info.int.adef_h,adef,/set,/no_copy
     xcds_analysis_show_status,info
     ENDCASE
     
  'CALIBRATE':BEGIN
     handle_value,info.int.adef_h,adef,/no_copy
     adef.calibrate = uvalue(1) EQ 'ON'
     IF NOT adef.calibrate THEN $
        adef.ergs = (adef.steradians = (adef.angstroms = 0b))
     handle_value,info.int.adef_h,adef,/set,/no_copy
     xcds_analysis_show_status,info
     ENDCASE
     
  'ERGS':BEGIN
     handle_value,info.int.adef_h,adef,/no_copy
     adef.ergs = uvalue(1) EQ 'ON'
     handle_value,info.int.adef_h,adef,/set,/no_copy
     xcds_analysis_show_status,info
     ENDCASE
     
  'STERADIANS':BEGIN
     handle_value,info.int.adef_h,adef,/no_copy
     adef.steradians = uvalue(1) EQ 'ON'
     handle_value,info.int.adef_h,adef,/set,/no_copy
     xcds_analysis_show_status,info
     ENDCASE
     
  'ANGSTROMS':BEGIN
     handle_value,info.int.adef_h,adef,/no_copy
     adef.angstroms = uvalue(1) EQ 'ON'
     handle_value,info.int.adef_h,adef,/set,/no_copy
     xcds_analysis_show_status,info
     ENDCASE
     
     
  'COSMIC':BEGIN
     handle_value,info.int.adef_h,adef,/no_copy
     adef.cosmic = ev.value
     handle_value,info.int.adef_h,adef,/set,/no_copy
     change = 1
     ENDCASE
     
  'FILL_COSMIC':BEGIN
     handle_value,info.int.adef_h,adef,/no_copy
     adef.fill_cosmic = uvalue(1) EQ 'ON'
     handle_value,info.int.adef_h,adef,/set,/no_copy
     change = 1
     ENDCASE
     
  'CALIBRATE':BEGIN
     handle_value,info.int.adef_h,adef,/no_copy
     adef.calibrate = ev.value
     handle_value,info.int.adef_h,adef,/set,/no_copy
     change = 1
     ENDCASE
     
  'WINDOW':BEGIN
     info.int.windowi = ev.windowi
     ENDCASE
     
  'ADD':BEGIN
     xcds_analysis_addwin,info
     change = 1
     ENDCASE
     
  'CROP':BEGIN
     xcds_analysis_cropwin,info,fix(uvalue(1))
     change = 1
     ENDCASE
     
  'WINLABEL':BEGIN
     handle_value,info.int.adef_h,adef,/no_copy
     handle_value,adef.windows_h,windows,/no_copy
     windows(fix(uvalue(1))).label = ev.value
     widget_control,ev.id,set_value=ev.value
     handle_value,adef.windows_h,windows,/set,/no_copy
     handle_value,info.int.adef_h,adef,/set,/no_copy
     ENDCASE
     
  'GR_ORDER':BEGIN
     handle_value,info.int.adef_h,adef,/no_copy
     handle_value,adef.windows_h,windows,/no_copy
     windows(fix(uvalue(1))).gr_order = fix(uvalue(2))
     handle_value,adef.windows_h,windows,/set,/no_copy
     handle_value,info.int.adef_h,adef,/set,/no_copy
     xcds_analysis_sortwin,info
     change = 1
     ENDCASE
     
     
  'DELETE':BEGIN
     handle_value,info.int.adef_h,adef,/no_copy
     handle_value,adef.windows_h,windows,/no_copy
     
     nw = n_elements(windows)
     IF nw GT 1 THEN BEGIN
        i = fix(uvalue(1))
        CASE i OF 
           0 : windows = windows(1:*)
           nw-1 : windows = windows(0:nw-2)
           ELSE: windows = [windows(0:i-1),windows(i+1:*)]
        END
        handle_value,adef.windows_h,windows,/set,/no_copy
     ENDIF 
     adef.filename = ''
     handle_value,info.int.adef_h,adef,/set,/no_copy
     change = 1
     ENDCASE
     
  'LOAD':BEGIN ;; Load existing analysis - from default file name/other
     xcds_analysis_load_analysis,info
     ENDCASE
     
  'GENERATE':BEGIN
     xcds_analysis_generate,info
     ENDCASE
     
  'EDIT':BEGIN
     handle_value,info.int.analysis_h,ana,/no_copy
     IF exist(ana) THEN BEGIN 
        xcfit_block,analysis=ana
        ;; Copy (possibly changed) fit structure into adef
        handle_value,info.int.adef_h,adef
        handle_value,ana.fit_h,fit
        handle_value,adef.fit_h,fit,/set
        ;; Then store analysis
        handle_value,info.int.analysis_h,ana,/set,/no_copy
     END
     xcds_analysis_show_filenames,info
     ENDCASE
     
  END
  
  IF keyword_set(change) THEN BEGIN
     handle_value,info.int.analysis_h,ana,/no_copy
     IF exist(ana) THEN BEGIN
        ana.fit_h = 0
        delete_analysis,ana
     END
     xcds_analysis_show_filenames,info
     xcds_analysis_show_windows,info
  END
  
  widget_control,ev.top,set_uvalue=info,/no_copy
END



PRO xcds_analysis_instruct,cosmic_instruct,anafile_instruct
  
cosmic_instruct = $
   ['This pattern defines how cosmic ray pixels are to be located.',$
    '',$
    'Several alternate methods may be specified, separated by a "|",',$
    'in which case the methods are tried one after another until one',$
    'method is successful. Each alternate sequence may, however, be a',$
    'sequence of individual methods, separated by a "+" sign',$
    '',$
    'The first character of the method defines the type of method:',$
    '',$
    '  "@"  means that the following string is a procedure name.',$
    '       The procedure will always be called as e.g.,',$
    '             PROC,QLDS,NOFILL=0/1',$
    '       where the NOFILL keyword is set according to the FILL_COSMIC',$
    '       status of the analysis definition. A procedure name is always',$
    '       considered "successful"',$
    '',$
    '  "!"  means that the following string is to be interpreted as the',$
    '       name of a file, generated by the procedure CDS_SAVE_MISSING.',$
    '',$
    'Like the pattern for forming analysis file names, it is subject',$
    'to substring replacement, so a pattern like e.g.,',$
    '',$
    '  %path/missing/%raster.miss',$
    '',$
    'would be translated into',$
    '',$
    '  /myhome/missing/s3300r00.miss',$
    '',$
    'if it appeared in an analysis definition called e.g.,',$
    '',$
    '  /myhome/my_analysis_r11v14_w_0_1.adef',$
    '',$
    'and was applied to the data in file s3300r00.fits']


anafile_instruct = $
   ['This pattern defines how the file name of an analysis is formed.',$
    '',$
    'Some substrings (starting with a "%") may be used to form',$
    'a unique analysis file name based on the file name of the analysis',$
    'definition and the study/raster name to which it is applied.',$
    '',$
    'E.g., given an analysis definition stored in a file called',$
    '',$
    '  /myhome/my_analysis_r11v14_w_0_1.adef',$ 
    '',$
    'the string ',$
    '',$
    '  %path/analysis/%base%raster_%rid_%winspec.ana',$
    '',$
    'would result in an analysis file name of',$
    '',$
    '/myhome/analysis/my_analysis_s3300r00_r11v14_w_0_1.ana', $
    '',$
    'when applied to raster s3300r00',$
    '',$
    'I.e., "%path"    => "/myhome/"',$
    '      "%base"    => "my_analysis_"',$
    '      "%raster"  => "s3300r00"',$
    '      "%rid"     => "r11v14"    (raster id/variation)',$
    '      "%winspec" => "w_0_1"']
END

FUNCTION xcds_analysis_buildwidget,adef,qlds,valid_qlds,windowix,$
                               info=info
  
  ;; Start making the widget
  
  sml = {xpad:1,ypad:1,space:1}
  
  top = widget_base(/column,title='XCDS_ANALYSIS',space=5)
  buttons = widget_base(top,/row,_extra=sml)
  
  ;; FILE menu
  ;; 
  file = widget_button(buttons,value='File',menu=2)
  save = widget_button(file,value='Save',uvalue='SAVE')
  save_as = widget_button(file,value='Save as',uvalue='SAVE_AS')
  restore = widget_button(file,value='Restore',uvalue='RESTORE')
  quit = widget_button(file,value='Quit',uvalue='QUIT')
  
  ;; Help button
  
  dummy = widget_button(buttons,value='Help',uvalue='HELP')
  
  ;; File name
  ;; 
  fn_base = widget_base(top,_extra=sml,/row)
  filename_lid = widget_label(fn_base,value='File:'+adef.filename)
  IF since_version('4.0.1') THEN widget_control,filename_lid,/dynamic_resize
  

  ;; Get instructions for cosmic ray treatment + analysis file name patterns
  xcds_analysis_instruct,c_instr,a_instr
  
  ;; Pattern stuff
  
  patternbase0 = widget_base(top,/column,frame=4)
;  patternbase1 = widget_base(patternbase0,/row)
;  patternbase2 = widget_base(patternbase1,/column)
  patternbase = patternbase0 ; widget_base(patternbase1,/column)
  
  ;; Analysis file name template
  ;; 
  
  afileb = widget_base(patternbase,/column)
  afile_r1 = widget_base(afileb,/row,_extra=sml)
  dummy = widget_label(afile_r1,value='Pattern for analysis file names:')
  def_afile_id = cw_enterb(afile_r1,uvalue='ANA_FILENAME',instruct = a_instr)
  def_afile_lid = widget_label(widget_base(afileb,_extra=sml))
  
  ;; Cosmic ray treatment (label/button values will be initialized later)
  ;; Also, _lid labels must be /dynamic_resize
  
  cosmicb = widget_base(patternbase,/column)
  cosmic_r1 = widget_base(cosmicb,/row,_extra=sml)
  dummy = widget_label(cosmic_r1,value='How to find cosmic rays:')
  cosmic_id = cw_enterb(cosmic_r1,uvalue='COSMIC',instruct=c_instr)
  cosmic_lid = widget_label(widget_base(cosmicb,_extra=sml))
  
  IF since_version('4.0.1') THEN BEGIN
     widget_control,cosmic_lid,/dynamic_resize
     widget_control,def_afile_lid,/dynamic_resize
  END
  
  onoff = ['ON','OFF']
  ;;
  ;; Fill-cosmic on/off
  ;;
  fill_cosmic_id = cw_flipswitch(cosmicb,uvalue='FILL_COSMIC:'+onoff,$
                                 value='Fill in cosmic ray pixels:'+onoff)
  
;  fill_cosmic_id = cw_checkbox(cosmicb,value=adef.fill_cosmic, $
;                               uvalue='FILL_COSMIC',thick=2,$
;                               label='Fill in cosmic ray pixels:')
  
  
  ;; TREATMENT stuff
  
  treatbase0 = widget_base(top,/column,frame=4)
  treatbase1 = widget_base(treatbase0,/row)
  treatbase2 = widget_base(treatbase1,/column)
  treatbase = widget_base(treatbase1,/column)
  
  ;;
  ;; Debiasing on/off, etc..
  ;;
  onoffb = widget_base(treatbase,/row,_extra=sml)
  
  debias_id = cw_flipswitch(onoffb,value='DEBIAS:'+onoff,$
                            uvalue='DEBIAS:'+onoff)
  
  onoffb = widget_base(treatbase,/row,_extra=sml)
  vds_calib_id = cw_flipswitch(onoffb,value='VDS_CALIB:'+onoff,$
                               uvalue='VDS_CALIB:'+onoff)
  use_counts_id = cw_flipswitch(onoffb,value='USE_COUNTS:'+onoff,$
                                uvalue='USE_COUNTS:'+onoff)
  noburnin_id = cw_flipswitch(onoffb,value='NOBURNIN:'+onoff,$
                              uvalue='NOBURNIN:'+onoff)
  
  onoffb = widget_base(treatbase,/row,_extra=sml)
  calibrate_id = cw_flipswitch(onoffb,value='CALIBRATE:'+onoff, $
                               uvalue='CALIBRATE:'+onoff)
  ergs_id = cw_flipswitch(onoffb,value='ERGS:'+onoff, $
                          uvalue='ERGS:'+onoff)
  steradians_id = cw_flipswitch(onoffb,value='STERADIANS:'+onoff, $
                                uvalue='STERADIANS:'+onoff)
  angstroms_id = cw_flipswitch(onoffb,value='ANGSTROMS:'+onoff, $
                               uvalue='ANGSTROMS:'+onoff)
  
;  debias_id = cw_flipswitch(onoffb,value=adef.debias,label='vds_debias:'+onoff,$
;                            uvalue='DEBIAS:'+onoff)
;  vds_calib_id = cw_checkbox(onoff,valule=adef.vds_calib,label='VDS_CALIB:',$
;                             u
;  calibrate_id = cw_checkbox(onoff,value=adef.calibrate,uvalue='CALIBRATE',$
;                             label='Calibrate data',thick=2)
  
  win_b = widget_base(top,/column,frame=4)
  
  ;; Add window stuff
  
  IF valid_qlds THEN BEGIN 
     add_win = widget_base(win_b,/row,_extra=sml)
     winsel = cwq_winsel(add_win,qlds,uvalue='WINDOW',title='Current window: ')
     add = widget_button(add_win,value='Add this window',uvalue='ADD')
  END
  
  window_base = widget_base(win_b,/column,$
                            ysize=5000,y_scroll_size=200,x_scroll_size=750)
  
  ;; APPLY/ANALYSIS stuff
  
  ;; Generate/edit associated fit
  
  apply_b = widget_base(top,/column,/frame,_extra=sml)
  
  apply_row1 = widget_base(apply_b,/row,_extra=sml)
  
  makefit_m = widget_button(apply_row1,value='Apply analysis definition',$
                            menu=2)
  makefit = widget_button(makefit_m,UVALUE='GENERATE',$
                          value='Generate analysis & edit in XCFIT_BLOCK')
  IF NOT valid_qlds THEN widget_control,makefit_m,sensitive=0
  
  load = widget_button(apply_row1,value='Load analysis',uvalue='LOAD')
  edit_id = widget_button(apply_row1,value='Edit analysis',uvalue='EDIT')
  have_analysis_id = cw_checkbox(apply_row1,label='Analysis present:',$
                                 /display_only,thick=2,value=0)
  
  apply_row2 = widget_base(apply_b,/row,_extra=sml)
  ana_filename_lid = widget_label(apply_row2)
  IF since_version('4.0.1') THEN $
     widget_control,ana_filename_lid,/dynamic_resize
  
  xrealize,top,/center
  
  int = {top               :top,$
         ql_no             :-1L,$
         freebie           :0b,$
         windowi           :windowix(0),$
         filename_lid      :filename_lid,$
         def_afile_id      :def_afile_id,$
         def_afile_lid     :def_afile_lid,$
         ana_filename_lid  :ana_filename_lid,$
         $
         cosmic_id         :cosmic_id,$
         cosmic_lid        :cosmic_lid,$
         fill_cosmic_id    :fill_cosmic_id,$
         debias_id         :debias_id,$
         vds_calib_id      :vds_calib_id,$
         use_counts_id     :use_counts_id,$
         noburnin_id       :noburnin_id,$
         calibrate_id      :calibrate_id,$
         ergs_id           :ergs_id,$
         steradians_id     :steradians_id,$
         angstroms_id      :angstroms_id,$
         $
         adef_h            : 0L,$
         analysis_h        : 0L,$
         window_base       :window_base,$
         edit_id           :edit_id,$
         have_analysis_id  :have_analysis_id }
  
  info = {int:int}
  
  return,info
END



PRO xcds_analysis,adef,analysis,qlds=qlds,modal=modal
  
  on_error,2
  IF !debug NE 0 THEN on_error,0
  
  IF n_params() LT 1 THEN BEGIN
     on_error,2
     message,"Use: XCDS_ANALYSIS,ADEF [,ANALYSIS] [,QLDS=QLDS]"
  END
  
  qlmgr,qlds,valid_qlds
  
  IF exist(qlds) AND NOT valid_qlds THEN BEGIN
     on_error,2
     message,"Use: XCDS_ANALYSIS,ADEF [,ANALYSIS] [,QLDS=QLDS]",/continue
     message,"Supplied QLDS is not valid"
  END
  
  IF NOT valid_qlds THEN $
     message,"No QLDS passed - no editing of window list allowed",/continue
  
  IF datatype(adef) NE 'STC' THEN BEGIN
     IF NOT valid_qlds THEN $
        message,"Must have an analysis definition when no QLDS is supplied"
     adef = mk_cds_adef_stc()
     adef.ras_id = qlds.header.ras_id
     adef.ras_var = qlds.header.ras_var
  END
  
  IF valid_qlds THEN BEGIN
     IF adef.ras_id NE qlds.header.ras_id OR $
        adef.ras_var NE qlds.header.ras_var THEN BEGIN
        on_error,2
        message,"Mismatch between raster id/variation of"+$
           "QLDS and the analysis definition"
     END
  END
  
  IF valid_qlds THEN BEGIN 
     windowix = where(qlds.detdesc(*).ixstop(0) GE 0)
     IF windowix(0) EQ -1 THEN BEGIN
        on_error,2
        message,"Must have at least one window available"
     END
  END ELSE BEGIN
     windowix = -1
  END
  
  info = xcds_analysis_buildwidget(adef,qlds,valid_qlds,windowix)
  
  IF getenv("CDS_ANALYSIS") EQ '' THEN $
     xack,['The environment variable CDS_ANALYSIS is not set.',$
           '','It will be useful to point this variable to a directory',$
           'under which most analysis data are to be stored'],/suppress
  
  IF valid_qlds THEN info.int.ql_no = qlds.ql_no
  
  info.int.adef_h = handle_create(value=adef)  
  handle_killer_hookup,info.int.adef_h ;; Will be killed on xkill,/all
  
  info.int.analysis_h = handle_create() ;; Careful about this one!
  
  IF valid_qlds THEN BEGIN
     IF NOT qlds_managed(qlds) THEN BEGIN
        modal = 1
        qlds_report,dummy,qlds,/store
     END
  END
  
  help,calls=calls
  IF strmid(calls(1),0,6) EQ '$MAIN$' THEN modal = 1
  
  default,modal,0
  
  IF exist(analysis) THEN BEGIN
     handle_value,info.int.analysis_h,analysis,/set
  END
  
  xcds_analysis_show_filenames,info
  xcds_analysis_show_status,info
  xcds_analysis_show_windows,info
  
  widget_control,info.int.top,set_uvalue = info ;; Keep a copy
  
  xmanager,'xcds_analysis',info.int.top,modal=modal,$
     cleanup='xcds_analysis_clean'
  
  ;; something needs to be done here...
  
  IF valid_qlds AND modal THEN BEGIN
     qlds_fetch,info.int.ql_no,qlds
  END
  
  IF NOT xalive(info.int.top) THEN BEGIN 
     handle_value,info.int.adef_h,adef
     handle_free,info.int.adef_h
     handle_value,info.int.analysis_h,analysis
     IF exist(analysis) AND n_params() LT 2 THEN BEGIN
        print,"Deleting analysis"
        delete_analysis,analysis
     END
     handle_free,info.int.analysis_h
  END ELSE BEGIN
     widget_control,info.int.top,get_uvalue=info,/no_copy ;; The *real* info
     info.int.freebie = 1 ;; Clear up things on exit
     widget_control,info.int.top,set_uvalue=info,/no_copy
  END
  print,"Exiting XCDS_ANALYSIS"
END



