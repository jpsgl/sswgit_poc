;+
; Project     : SOHO - CDS     
;                   
; Name        : SAVE_CDS_ADEF
;               
; Purpose     : Save a CDS Analysis Definition (ADEF)
;               
; Explanation : Saves the contents of a CDS ADEF to the file name found in the
;               ADEF structure (ADEF.FILENAME). If the file name is empty, or
;               if the keyword SAVE_AS is set, the user is prompted (by
;               pickfile) for the file name. The FILENAME tag of the ADEF will
;               be updated to reflect the actual file name where the data were
;               saved.
;               
; Use         : SAVE_CDS_ADEF,ADEF
;    
; Inputs      : ADEF : CDS ANALYSIS DEFINITION structure.
; 
; Opt. Inputs : None
;               
; Outputs     : None
;               
; Opt. Outputs: None.
;               
; Keywords    : VERBOSE : Propagated to the SAVE command.
;
;               SAVE_AS : Set to prompt the user for a file name.
;
; Calls       : bigpickfile(), break_file, test_open(), chk_dir(), default
;
; Common      : None
;               
; Restrictions: Needs widgets to prompt the user.
;               
; Side effects: Modifies ADEF.FILENAME
;               
; Category    : QuickLoook,Analysis
;               
; Prev. Hist. : None
;
; Written     : SVH Haugan, UiO, 25 September 1997
;               
; Modified    : Not yet.
;
; Version     : 1,  25 September 1997
;-            
PRO save_cds_adef,adef,verbose=verbose,save_as=save_as
  
  IF adef.filename EQ '' OR keyword_set(save_as) THEN BEGIN
     
     ;; This is just to get raster id (rid) etc..
     
     cds_adef_parsefile,adef,dummy,no_qlds,path,base,rid,raster,winspec
     
     filter = '*'+rid+'*'+winspec+'.adef'
     file = rid+'_'+winspec+'.adef'
     
     IF adef.filename NE '' THEN BEGIN
        break_file,adef.filename,disk,dir,fnam,ext
        path = disk+dir
        IF path EQ '' THEN path = getenv("CDS_ANALYSIS")
        file = fnam+ext
     END ELSE BEGIN
        path = getenv("CDS_ANALYSIS")
     END
     
     file = bigpickfile(file=file,/write,path=path,get_path=path,filter=filter)
     
     break_file,file,disk,dir,fnam,ext
     
     IF file NE '' THEN filename = path+fnam+ext ELSE filename=''
     
     IF NOT test_open(filename,/write) OR chk_dir(filename) THEN filename = ''
     
     IF filename EQ '' THEN BEGIN
        print,"You must give me a file name of a writeable file"
        return
     END
     
     adef.filename = filename
  END
  
  default,verbose,0
  
  print,"Saving to file "+adef.filename
  
  handle_value,adef.fit_h,fit
  handle_value,adef.windows_h,windows
  
  filename = adef.filename
  ras_id = adef.ras_id
  ras_var = adef.ras_var
  ana_filename = adef.ana_filename
  debias = adef.debias
  cosmic = adef.cosmic
  fill_cosmic = adef.fill_cosmic
  vds_calib = adef.vds_calib  
  noburnin = adef.noburnin
  use_counts = adef.use_counts
  calibrate = adef.calibrate
  ergs = adef.ergs
  steradians = adef.steradians
  angstroms = adef.angstroms
  
  save,/xdr,filename=filename,ras_id,ras_var,ana_filename,debias,cosmic, $
     fill_cosmic,vds_calib,noburnin,use_counts,calibrate,ergs,steradians, $
     angstroms,fit,windows,verbose=verbose
END


