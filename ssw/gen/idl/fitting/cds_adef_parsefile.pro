;+
; Project     : SOHO - CDS     
;                   
; Name        : CDS_ADEF_PARSEFILE
;               
; Purpose     : Parse file names++ according to a CDS Analysis Definition 
;               
; Explanation : A CDS Analysis Definition (ADEF) contains a pattern for
;               automatic generation of the analysis file name.
;
;               This routine is used to convert the pattern into a file name,
;               by replacing strings in the pattern, based on the current file
;               name of the ADEF and the supplied QLDS.
;
;               Given an ADEF file name of e.g.,
;
;                   /usr/myhome/mydef_r10v7_w_0.adef
;
;               then the following patterns will be replaced with:
;
;                   %path  =>   /usr/myhome/
;
;                   %base  =>   mydef_
;
;                   %rid   =>   r10v7
;
;                   %winspec => w_0
;
;                   %raster  => s*r*
;
;               If a QLDS is supplied, "%raster" will be replaced by the
;               corresponding file name (e.g., s5050r00).
;
;               By default, the pattern for the output ANA_FILENAME will be
;               taken from the ADEF.ANA_FILENAME, but you may specify a
;               different pattern through the keyword INAME.
;
;               You may also retrieve the values of the %path, %base,
;               etc. patterns, through the output parameters with
;               corresponding names, e.g.:
;
;                 CDS_ADEF_PARSEFILE,ADEF,OUTPUT_NAME,QLDS,PATH,BASE,$
;                                    RID,RASTER,WINSPEC
;
;                 PRINT,"Raster id/variation string: "+RID
;
; Use         : CDS_ADEF_PARSEFILE,ADEF,ANA_FILENAME [ ,QLDS ]
;    
; Inputs      : ADEF : CDS Analysis Definition
; 
; Opt. Inputs : QLDS : QuickLook Data Structure
;               
; Outputs     : ANA_FILENAME : Contains the parsed analysis file name (or the
;                              parsed input INAME).
;
; Opt. Outputs: PATH, BASE, RID,
;               RASTER, WINSPEC : Contain the strings that have been used to
;                                 replace the corresponding patterns.
;               
; Keywords    : INAME : Input name - use to apply the pattern replacement to
;                       strings other than the ADEF.ANA_FILENAME 
;
; Calls       : trim(), break_file, datatype(), concat_dir(), repstr()
;
; Common      : None
;               
; Restrictions: ...
;               
; Side effects: ...
;               
; Category    : Analysis
;               
; Prev. Hist. : None
;
; Written     : SVH Haugan, UiO, 21 October 1997
;               
; Modified    : Not yet.
;
; Version     : 1,  21 October 1997
;-            
PRO cds_adef_parsefile,adef,ana_filename,qlds,path,base,rid,raster,winspec,$
                       iname=iname
  
  rid = "r"+trim(adef.ras_id)+'v'+trim(adef.ras_var)
  
  IF adef.filename EQ '' THEN BEGIN
     path = getenv("CDS_ANALYSIS")
     base = ''
  END ELSE BEGIN
     break_file,adef.filename,disk,dir,fnam,ext
     path = disk+dir
     
     base = ''
     rid_pos = strpos(fnam,rid)
     IF rid_pos GE 0 THEN base = strmid(fnam,0,rid_pos)
     
  END
  
  CASE datatype(qlds) OF 
     'STR':break_file,qlds,disk,dir,raster,ext
     'STC': BEGIN 
        break_file,qlds.header.filename,disk,dir,fnam,ext
        raster = fnam
     END
     ELSE:raster = 's*r*'
  END
  
  winspec = 'w'
  handle_value,adef.windows_h,windows,/no_copy
  
  FOR i = 0,n_elements(windows)-1 DO BEGIN
     winspec = winspec + '_'+trim(windows(i).ix)
  END
  
  handle_value,adef.windows_h,windows,/no_copy,/set
  
  ;; Do replacements (if applicable)
  
  IF datatype(iname) EQ 'STR' THEN ana_filename = iname $
  ELSE                             ana_filename = adef.ana_filename
  
  dummyfile = concat_dir(path,'&&')
  path = strmid(dummyfile,0,strpos(dummyfile,'&&'))
  
  ana_filename = repstr(ana_filename,'%base',base)
  ana_filename = repstr(ana_filename,'%rid',rid)
  ana_filename = repstr(ana_filename,'%raster',raster)
  ana_filename = repstr(ana_filename,'%winspec',winspec)
  
  ana_filename = repstr(ana_filename,'%path/',path)
  
  ana_filename = repstr(ana_filename,'%path',path)
END
