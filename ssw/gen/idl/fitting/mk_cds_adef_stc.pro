;+
; Project     : SOHO - CDS     
;                   
; Name        : MK_CDS_ADEF_STC()
;               
; Purpose     : Make a CDS Analysis Definition Structure with default values.
;               
; Explanation : Creates and returns a CDS Analysis Definition (ADEF)
;               structure, and initializes it with default values. If a QLDS
;               is passed as a keyword, the RAS_ID and RAS_VAR fields will be
;               filled with information from the QLDS.
;               
; Use         : ADEF = MK_CDS_ADEF_STC( [ QLDS=QLDS ] )
;    
; Inputs      : None required.
; 
; Opt. Inputs : QLDS : Quick Look Data Structure
;               
; Outputs     : Returns ADEF.
;               
; Opt. Outputs: None.
;               
; Keywords    : QLDS.
;
; Calls       : qlmgr
;
; Common      : None
;               
; Restrictions: ...
;               
; Side effects: ...
;               
; Category    : Analysis
;               
; Prev. Hist. : None
;
; Written     : SVH Haugan, UiO, 17 October 1997
;               
; Modified    : Not yet.
;
; Version     : 1, 17 October 1997
;-            
FUNCTION mk_cds_adef_stc,qlds=qlds
  
  
  debias = 1b  ;; Whether to do debias or not - off => units = 'ADC'
  
  ;; Note that e.g., CDS_CLEAN_EXP will perform a VDS_DEBIAS operation!
  ;; 
  cosmic = '!%path%raster.miss|@cds_clean_exp'
  fill_cosmic = 0b
  
  vds_calib = 1b  ;; Whether to do vds_calib or not
  noburnin = 0b   ;; 
  use_counts = 0b ;; Whether to multiply up to get units = COUNTS
  calibrate = 1b  ;; Whether to run nis_calib or not
  ergs = 0b       ;; ERGS flag to nis_calib
  steradians = 0b ;; STERADIANS flag
  angstroms = 0b  ;; ANGSTROMS flag
  
  adef = {cds_adef_stc,$
          filename : '',$
          ras_id : -1L,$
          ras_var : -1L,$
          ana_filename : '%path%base%raster_%rid_%winspec.ana',$
          debias:debias,$
          cosmic:cosmic,$
          fill_cosmic:fill_cosmic,$
          vds_calib:vds_calib,$
          noburnin:noburnin,$
          use_counts:use_counts,$
          calibrate:calibrate,$
          ergs:ergs,$
          steradians:steradians,$
          angstroms:angstroms,$
          windows_h:handle_create(),$
          fit_h:handle_create()}
  
  qlmgr,qlds,valid
  
  IF valid THEN BEGIN
     adef.ras_id = qlds.header.ras_id
     adef.ras_var = qlds.header.ras_var
  END
  
  return,adef
END
