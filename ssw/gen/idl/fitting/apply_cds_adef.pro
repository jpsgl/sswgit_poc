;+
; Project     : SOHO - CDS     
;                   
; Name        : APPLY_CDS_ADEF
;               
; Purpose     : Apply CDS analysis definition(s), generating an analysis.
;               
; Explanation : Applies the information in one or more CDS Analysis
;               Definition(s) - ADEF(s) - to a specific CDS data set returning
;               the resulting analysis structure(s).
;
;               The data set (raster) may be given either by the file name (in
;               which case the QLDS will be read in) or through the QLDS
;               keyword.
;
;               If the QLDS has to be read in, the following applies:
;
;                    Only the data windows used in the analysis will be
;                    selected.
;
;                    Also, unless the /LEAVE_QLDS flag is set, the QLDS will
;                    be deleted after the data has been extracted in
;
;               
;               
;               
; Use         : ANALYSIS = APPLY_CDS_ADEF(ADEF,FILE)
;    
; Inputs      : ADEF : CDS Analysis Defintion
;
; Opt. Inputs : FILE : E.g., "s5500r00" - the name of a CDS fits file.  Must
;                      be supplied if no QLDS is supplied.
;               
; Outputs     : Returns CFIT Analysis Structure according to the Analysis
;               Definition.
;               
; Opt. Outputs: 
;               
; Keywords    : QLDS : You may supply the QLDS to which the analysis
;                      definition should be applied through this keyword.
;                      Alternatively, if /LEAVE_QLDS is set, get the QLDS
;                      through this keyword after it has been read in.
;
;               LEAVE_QLDS : See QLDS.
;
;               CALIBRATED_QLDS : Returns a *copy* of the QLDS after the
;                                 calibrations defined in the ADEF have been
;                                 applied. Only valid when /LEAVE_CALIBRATION
;                                 is set, and only if calibrations beyond
;                                 VDS_DEBIAS has been selected.
;
;               LEAVE_CALIBRATION : See CALIBRATED_QLDS
;
; Calls       : 
;
; Common      : None.
;               
; Restrictions: ...
;               
; Side effects: ...
;               
; Category    : Analysis
;               
; Prev. Hist. : None.
;
; Written     : SVH Haugan, UiO, 21 October 1997
;               
; Modified    : Not yet.
;
; Version     : 1, 21 October 1997
;-            


FUNCTION apply_cds_adef,adef,file,qlds=qlds,calibrated_qlds=calibrated_qlds,$
                        leave_calibration=leave_calibration,$
                        leave_qlds=leave_qlds,link_fit=link_fit
  
  on_error,2
  IF !debug NE 0 THEN on_error,0
  
  IF datatype(adef) NE 'STC' THEN BEGIN
     message,"The first argument must be a CDS analysis definition"
  END
  
  ;; Multi-analysis treatment
  IF n_elements(adef) GT 1 THEN BEGIN
     
     FOR i = 0,n_elements(adef)-1 DO BEGIN
        handle_value,adef(i).windows_h,windows
        IF exist(windowix) THEN windowix = [windowix,windows(*).ix] $
        ELSE                    windowix = windows(*).ix
     END 
     
     windowix = windowix(uniq(windowix,sort(windowix)))
     
     input_qlds = 0
     
     IF exist(qlds) THEN BEGIN
        qlmgr,qlds,valid
        IF NOT valid THEN message,"Supplied QLDS is not valid"
        input_qlds = 1
     END ELSE BEGIN
        errmsg = ''
        ;;
        ;; Read only windows that are used.
        ;;
        print,"Trying to read file "+file
        qlds = readcdsfits(file,preselect=windowix,errmsg=errmsg,/batch)
        IF errmsg NE '' THEN message,errmsg
        print,"Read file "+qlds.header.filename
     END
     
     FOR i = 0,n_elements(adef)-1 DO BEGIN
        ana = apply_cds_adef(adef(i),qlds=qlds,/leave_calibration,$
                             calibrated_qlds=calibrated_qlds)
        IF exist(anas) THEN anas = [anas,ana] $
        ELSE                anas = ana
     END
     
     IF datatype(calibrated_qlds) EQ 'STC' $
        AND NOT keyword_set(leave_calibration) THEN BEGIN
        print,"Deleting calibrated QLDS"
        delete_qlds,calibrated_qlds
     END
     
     ;; Finally, if the QLDS was not supplied in the call, maybe we should
     ;; delete it?
     IF NOT input_qlds AND NOT keyword_set(leave_qls) THEN BEGIN
        print,"Deleting QLDS"
        delete_qlds,qlds
     END
     
     return,anas
     
  END
  
  handle_value,adef.windows_h,windows
  IF NOT exist(windows) THEN BEGIN
     message,"No windows selected"
  END
  
  ;; Check to see if we already have the data available
  ;;
  IF exist(qlds) THEN BEGIN
     qlmgr,qlds,valid
     IF NOT valid THEN message,"Supplied QLDS is not valid"
     input_qlds = 1
  END ELSE BEGIN
     input_qlds = 0
     
     ;; Nope - we should should have a file name.
     ;;
     IF datatype(file) NE 'STR' THEN message,"Must have file name or QLDS"
     ;;
     ;; ..which windows will be used?
     ;;
     handle_value,adef.windows_h,windows,/no_copy
     windowix = windows(*).ix
     handle_value,adef.windows_h,windows,/set,/no_copy
     errmsg = ''
     ;;
     ;; Read only windows that are used.
     ;;
     print,"Trying to read file "+file
     qlds = readcdsfits(file,preselect=windowix,errmsg=errmsg,/batch)
     IF errmsg NE '' THEN message,errmsg
     print,"Read file "+qlds.header.filename
  END
  
  nis = qlds.header.detector EQ 'NIS'
  
  history = 'Apply_cds_adef: '+!stime
  
  ;;
  ;; Apply treatments as required
  ;;
  
  IF nis AND  adef.debias THEN BEGIN
     print,"Debiasing data",format='($,A)'
     vds_debias,qlds
     print,"--done"
     history = [history,"Debiased"]
  END
  
  ;; Make sure vds_debias etc. doesn't go twice..
  validwin = where(qlds.detdesc(*).ixstop(0) GE 0)
  
  qlds.detdesc(*).units = qlds.detdesc(validwin(0)).units
  
  ;; Cosmic ray treatments..
  ;;
  IF adef.cosmic EQ '' THEN GOTO,cosmic_finished
  
  IF NOT nis THEN GOTO,cosmic_finished
  
  cosmics = str_sep(adef.cosmic,'|')  ;;
  FOR i = 0L,n_elements(cosmics)-1 DO BEGIN
     success = 0b
     csequence = str_sep(cosmics(i),'+')
     FOR j = 0L,n_elements(csequence)-1 DO BEGIN
        this = csequence(j)
        CASE strmid(this,0,1) OF 
        '!':BEGIN
           filedesc = strmid(this,1,1000)
           cds_adef_parsefile,adef,filedesc,qlds,iname=filedesc
           break_file,filedesc,disk,dir,fnam,ext
           paths = disk+dir+':$CDS_ANALYSIS'
           print,"Looking for saved list of missing pixels:"
           print,"     path = "+paths
           print,"     filename= "+fnam+ext
           mfile = find_with_def(fnam+ext,paths)
           IF mfile EQ '' THEN BEGIN
              print,"Warning: No file with MISSING pixels/cosmic rays found"
           END ELSE BEGIN
              print,"Getting missing pixels from "+mfile
              cds_read_missing,qlds,mfile
              success = 1b
              history = [history,"Missing pixels from "+mfile]
           END
           ENDCASE
        '@':BEGIN 
           this = strmid(this,1,1000)
           nofill = (1-keyword_set(adef.fill_cosmic))
           call = this+",qlds, nofill="+trim(fix(nofill))
           print,"Calling "+call,format='($,A)'
           call_procedure,this,qlds,nofill=nofill
           history = [history,"Called: "+call]
           success = 1b
           ENDCASE 
        ELSE:BEGIN
           message,"Incomprehensible instruction for removing cosmic rays: "+$
              this,/continue
           ENDCASE 
        END
     END
     IF success THEN GOTO,cosmic_finished
  END
  
COSMIC_FINISHED:
  
  ;; Now extract data as specified
  
  ;; Create the final analysis structure first:
  
  ana = mk_analysis()
  
  IF keyword_set(link_fit) THEN BEGIN
     handle_free,ana.fit_h
     ana.fit_h = adef.fit_h
  END
  
  handle_value,adef.windows_h,windows
  
  FOR i = 0,n_elements(windows)-1 DO BEGIN
     a = mk_cds_analysis(qlds,windows(i).ix,/leave_calibration,$
                         debias=adef.debias,$
                         vds_calib=adef.vds_calib,noburnin=adef.noburnin,$
                         use_counts=adef.use_counts,$
                         calibrate=adef.calibrate,$
                         ergs=adef.ergs,steradians=adef.steradians,$
                         angstroms=adef.angstroms,$
                         qlds_calibrated=calibrated_qlds,$
                         gr_order=windows(i).gr_order,$
                         crop=windows(i).crop)
     
     handle_value,a.history_h,h
     history = [history,'Block '+trim(i),'     '+h]
     
     handle_value,a.lambda_h,l,/no_copy
     IF exist(lambda) THEN lambda = [lambda,temporary(l)]  $
     ELSE                  lambda = temporary(l)
     
     handle_value,a.data_h,d,/no_copy
     IF exist(data) THEN data = [data,temporary(d)]  $
     ELSE                data = temporary(d)
     
     handle_value,a.weights_h,w,/no_copy
     IF exist(weights) THEN weights = [weights,temporary(w)] $
     ELSE                   weights = temporary(w)
     
     handle_value,a.origin_h,o
     IF exist(origin) THEN origin = [[origin],[temporary(o)]] $
     ELSE                  origin = temporary(o)
     
     handle_value,a.scale_h,s
     IF exist(scale) THEN scale = [[scale],[temporary(s)]]  $
     ELSE                 scale = temporary(s)
     
     IF NOT exist(phys_scale) THEN handle_value,a.phys_scale_h,phys_scale
     IF NOT exist(dimnames) THEN handle_value,a.dimnames_h,dimnames
     
     ;; Cleanup
     delete_analysis,a
  END
  
  ;; Delete the calibrated data...
  
  IF datatype(calibrated_qlds) EQ 'STC' $
     AND NOT keyword_set(leave_calibration) THEN BEGIN
     print,"Deleting calibrated QLDS"
     delete_qlds,calibrated_qlds
  END
  
  
  sz = size(data)
  
  ;; Copy the fit structure from analysis definition
  
  handle_value,adef.fit_h,fit
  handle_value,ana.fit_h,fit,/set
  
  ;; Get the short fit structure to generate result/const/include arrays
  
  IF datatype(fit) EQ 'STC' THEN BEGIN 
     sfit = make_sfit_stc(fit,/double)
     
     inc_dim = [n_elements(sfit.include),sz(2:sz(0))] 
     inc_dim1 = inc_dim*0+1
     inc_dim1(0) = inc_dim(0)
     include = dimrebin(dimreform(sfit.include,inc_dim1),inc_dim,/sample)
     handle_value,ana.include_h,include,/set
     
     const_dim = [n_elements(sfit.const),sz(2:sz(0))] ;; CONST block dimension
     const_dim1 = const_dim*0+1
     const_dim1(0) = const_dim(0)
     const = dimrebin(dimreform(sfit.const,const_dim1),const_dim,/sample)
     handle_value,ana.const_h,const,/set
     
     res_dim = [n_elements(sfit.a_nom)+1,sz(2:sz(0))]
     res_dim1 = res_dim*0+1
     res_dim1(0) = res_dim(0)
     result = [sfit.a_nom,0.0d]
     result = dimrebin(dimreform(result,res_dim1),res_dim,/sample)
     handle_value,ana.result_h,result,/set
  END
  
  default,dimnames,(['LAMBDA','X','Y','T','DIM5','DIM6','DIM7'])(0:sz(0)-1)
  default,origin,replicate(0.0,sz(0))
  default,spacing,replicate(1.0,sz(0))
  default,phys_scale,replicate(0b,sz(0))
  
  IF n_elements(windows) GT 1 THEN BEGIN
     IF (size(origin))(0) EQ 2 THEN origin = average(origin,2)
     IF (size(scale))(0) EQ 2 THEN scale = average(scale,2)
     dimnames(0) = 'DISPERSION px'
     origin(0) = 0
     scale(0) = 1
  END
  
  handle_value,ana.history_h,history,/set
  
  ;; Put back window list
  
  handle_value,adef.windows_h,windows,/set,/no_copy
  
  ;; Put data cubes into the final analysis structure
  
  handle_value,ana.lambda_h,lambda,/set,/no_copy
  handle_value,ana.data_h,data,/set,/no_copy
  handle_value,ana.weights_h,weights,/set,/no_copy
  
  handle_value,ana.origin_h,origin,/set,/no_copy
  handle_value,ana.scale_h,scale,/set,/no_copy
  handle_value,ana.phys_scale_h,phys_scale,/set,/no_copy
  handle_value,ana.dimnames_h,dimnames,/set,/no_copy
  
  ;; Fix file name etc..
  
  cds_adef_parsefile,adef,ana_filename,qlds,iname=adef.ana_filename
  ana.filename = ana_filename
  ana.datasource = qlds.header.filename
  ana.definition = adef.filename
  
  ;; Finally, if the QLDS was not supplied in the call, maybe we should
  ;; delete it?
  IF NOT input_qlds AND NOT keyword_set(leave_qls) THEN BEGIN
     print,"Deleting QLDS"
     delete_qlds,qlds
  END
  
  return,ana
  
END





