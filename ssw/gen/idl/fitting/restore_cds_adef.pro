;+
; Project     : SOHO - CDS     
;                   
; Name        : RESTORE_CDS_ADEF()
;               
; Purpose     : Restore a CDS Analysis Definition (saved with SAVE_CDS_ADEF)
;               
; Explanation : Fairly straightforward - uses the file name passed (or the
;               file name of the passed ADEF), then restores it from disk and
;               returns it. If an ADEF is passed, it's handles etc. will be
;               (re)-used, otherwise a new one is generated.
;               
; Use         : ADEF = RESTORE_CDS_ADEF( [ FILENAME | ADEF ] )
;    
; Inputs      : None required.
; 
; Opt. Inputs : FILENAME or ANALYSIS DEFINITION (containing file name)
;               
; Outputs     : Returns analysis definition, or 0 if aborted.
;               
; Opt. Outputs: None.
;               
; Keywords    : /OTHER : Set to always ask the user for confirmation of the
;                        file name, even if supplied.
;
;               /VERBOSE : Passed on to the SAVE command.
;
; Calls       : datatype() default test_open() chk_dir() exist() break_file
;               bigpickfile(), mk_cds_adef_stc()
;
; Common      : None
;               
; Restrictions: ...
;               
; Side effects: ...
;               
; Category    : Analysis
;               
; Prev. Hist. : None
;
; Written     : SVH Haugan, UiO, 17 October 1997
;               
; Modified    : Not yet.
;
; Version     : 1,  17 October 1997
;-            
FUNCTION restore_cds_adef,filename_adef,verbose=verbose,other=other
  
  on_error,2
  IF !debug NE 0 THEN on_error,0
  
  CASE datatype(filename_adef) OF
  'STC':BEGIN
     adef = filename_adef
     filename = adef.filename
     ENDCASE
  'STR':BEGIN
     filename = filename_adef
     ENDCASE
  'UND':BEGIN
     ENDCASE 
  ELSE:BEGIN
     on_error,2
     message,'Use: ADEF = RESTORE_CDS_ADEF( [ FILENAME | ADEF ] )
     ENDCASE
  END
  
  default,filename,''
  
  IF filename NE '' THEN BEGIN
     IF NOT test_open(filename) OR chk_dir(filename) THEN other = 1
  END
  
  IF filename EQ '' OR keyword_set(other) THEN BEGIN
     
     ;; Basic filter:
     
     filter = '*r*v*.adef'
     
     ;; If a definition exists - use the best guess filter
     
     IF exist(adef) THEN BEGIN
        cds_adef_parsefile,adef,dummy,no_qlds,path,base,rid,raster,winspec
        filter = '*'+rid+'*.adef'
     END
     
     ;; If either a filename was supplied, or a definition passed, base
     ;; the filter, path and file on the existing file name (if present)
     
     IF filename NE '' THEN BEGIN
        break_file,filename,disk,dir,fnam,ext
        path = disk+dir
        file = fnam+ext
        filter = '*'+fnam+'*'+ext
     END
     
     ;; But the default path is:
     
     default,path,getenv("CDS_ANALYSIS")
     
     file = bigpickfile(/read,path=path,file=file,get_path=path,filter=filter)
     
     break_file,file,disk,dir,fnam,ext
     
     IF file NE '' THEN filename = path+fnam+ext ELSE filename=''
     
     IF NOT test_open(filename) OR chk_dir(filename) THEN filename = ''
     
     IF filename EQ '' THEN BEGIN
        print,"You must give me a file name of an existing file"
        return,0
     END
     
  END
  
  real_filename = filename
  
  default,verbose,0
  
  IF NOT exist(adef) THEN new = mk_cds_adef_stc() $
  ELSE                    new = adef
  
  IF NOT keyword_set(verbose) THEN print,"Restoring file "+filename
  
  restore,filename,verbose=verbose
  
  new.filename = filename
  new.ras_id = ras_id
  new.ras_var = ras_var
  new.ana_filename = ana_filename
  new.debias = debias
  new.cosmic = cosmic
  new.fill_cosmic = fill_cosmic
  new.vds_calib = vds_calib
  new.noburnin = noburnin
  new.use_counts = use_counts
  new.calibrate = calibrate
  new.ergs = ergs
  new.steradians = steradians
  new.angstroms = angstroms
  
  IF NOT handle_info(new.fit_h,/valid) THEN new.fit_h = handle_create()
  IF NOT handle_info(new.windows_h,/valid) THEN $
     new.windows_h = handle_create()
  
  handle_value,new.fit_h,fit,/set
  handle_value,new.windows_h,windows,/set
  
  return,new
END   






