;+
; Project     : SOHO - CDS
;
; Name        : XSEL_PRINTER
;
; Purpose     : select printer 
;
; Category    : Device, Widgets
;
; Explanation : retrieves a printer name from a selection
;               obtained from PRINTCAPS file
;
; Syntax      : IDL> xsel_printer,printer,err=err,status=status
;
; Inputs      : None
;
; Opt. Inputs : None
;
; Outputs     : printer = selected printer name
;
; Opt. Outputs: None
;
; Keywords    : GROUP = widget ID of calling widget
;               STATUS = 0/1 if selection aborted/completed
;               INSTRUCT = instructions for user
;               DEFAULT = default printer selection
;
; Common      : XSEL_PRINTER: holds last selected printer name
;
; Restrictions: currently works best for UNIX
;
; Side effects: None
;
; History     : Version 1, 7-Sep-1995,  D.M. Zarro.  Written
;               Version 2, 25-Feb-1997, Zarro, added printer environmentals
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-


pro xsel_printer,printer,group=group,err=err,status=status,$
                default=default,instruct=instruct

common xsel_printer,last_choice
err=''
if datatype(instruct) ne 'STR' then instruct=''
if datatype(last_choice) ne 'STR' then last_choice='---'

;-- check user's input

def_printers=strarr(4)
def_desc=strarr(4)
if datatype(default) eq 'STR' then begin
 def_printers(0)=trim(default)
 def_desc(0)='User Input'
endif

;-- check user's last choice

if (trim(last_choice) ne '---') then begin
 dash=strpos(last_choice,'---')
 if (dash gt -1) then begin
  def_printers(1)=trim(strmid(last_choice,0,dash))
  def_desc(1)=trim(strmid(last_choice,dash+3,strlen(last_choice)))
 endif
endif

;-- check environmentals

def_printers(2)=trim(getenv('PSLASER'))
def_desc(2)='PSLASER'
def_printers(3)=trim(getenv('PRINTER'))
def_desc(3)='PRINTER'

;-- default to whatever lpr is doing

;def_printers(4)='lpr '
;def_desc(4)='Default Printer'

;-- list available printers

printer='' & desc='' & status=1
printers=''
desc=''
list_printer,printers,desc,err=err

if printers(0) eq '' then begin
 valid=where(def_printers ne '',cnt)
 if cnt gt 0 then begin
  printers=def_printers(valid)
  desc=def_desc(valid)
 endif
endif

;-- add in default printers that are not in printcap list

printers=trim(printers)
for i=0,n_elements(def_printers)-1 do begin
 if def_printers(i) ne '' then begin
  clook=where(trim(def_printers(i)) eq printers,cnt)
  if cnt eq 0 then begin
   if printers(0) eq '' then begin
    printers=def_printers(i) 
    desc=def_desc(i)
   endif else begin
    printers=[def_printers(i),printers]
    desc=[def_desc(i),desc]
   endelse
  endif
 endif
endfor

;-- list printers and highlight selection

if printers(0) ne '' then begin

;-- sort our duplicates

 rs=uniq([printers],sort([printers]))
 printers=printers(rs)
 desc=desc(rs)

;-- parse out descriptions

 for i=0,n_elements(printers)-1 do begin
  if printers(i) eq getenv('PSLASER') and (desc(i) ne 'PSLASER') then desc(i)=desc(i)+' (PSLASER)'
  if printers(i) eq getenv('PRINTER') and (desc(i) ne 'PRINTER') then desc(i)=desc(i)+' (PRINTER)'
  if trim(printers(i)) eq 'lpr' then desc(i)='Default Printer'
 endfor

 choices=printers+' --- '+desc
 index=0
 for i=0,n_elements(def_printers)-1 do begin
  if def_printers(i) ne '' then begin
   index=where(trim(def_printers(i)) eq printers,cnt)
   goto,done
  endif
 endfor
done:
 last_choice=choices(index(0))
 temp = xsel_list(choices, group=group,$
                 title = 'Available Printers',$
                 initial=last_choice,/index,$
                 status = status,/no_remove,subtitle=instruct)
 if status then begin 
  printer=printers(temp) 
;  if (trim(printer) ne '') and (getenv('PRINTER') eq '') then mklog,'PRINTER',printer 
  last_choice=choices(temp)
 endif
endif 

if (printer eq '') and status then begin
 message,'Using default printer',/cont
 if os_family() eq 'vms' then printer='SYS$PRINT' else printer='lpr'
endif

return & end

