;+
; Project     :	SOHO - CDS
;
; Name        :	CONCAT_STRUCT
;
; Purpose     :	concatanate two structures
;
; Explanation :	concatanates two structures by using COPY_STRUCT to
;               avoid the problem of concatanating two differently named
;               structures.
;
; Use         : NEW_STRUCT=CONCAT_STRUCT(STRUCT1,STRUCT2)
;
; Inputs      :	STRUCT1,2 = input structures
;
; Opt. Inputs :	None.
;
; Outputs     :	NEW_STRUCT = concatanated structure
;
; Opt. Outputs:	None.
;
; Keywords    :	ERR = err string
;
; Calls       :	COPY_STRUCT
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Category    :	Structure handling
;
; Prev. Hist. :	None.
;
; Written     :	Dominic Zarro (ARC)
;
; Version     :	Version 1.0, 22 September 1994
;-


function concat_struct,struct1,struct2,_extra=extra,err=err      ;-- join two structures

on_error,1

err=''
if (datatype(struct1) ne 'STC') or (datatype(struct2) ne 'STC')  then begin
 if (datatype(struct1) eq 'STC') then return,struct1
 if (datatype(struct2) eq 'STC') then return,struct2
 err='invalid input structures'
 message,err,/cont
 return,0
endif

s1=tag_names(struct1,/struct)
s2=tag_names(struct2,/struct)

smatch=match_struct(struct1,struct2,/tags) 
if not smatch then begin
 err='Input structures do not have matching tag names'
 message,err,/cont
 return,struct1
endif

new_struct=0
if (s1 eq s2) and (s1 ne '') then return,[struct1,struct2]

n1=n_elements(struct1)
n2=n_elements(struct2)

;-- make sure destination structure can accomodate source

dim1=max_tag(struct1,/nest)
dim2=max_tag(struct2,/nest)

if dim1 eq dim2 then begin
 if n1 lt n2 then begin
  temp_struct=replicate(clear_struct(struct2(0)),n1)
  copy_struct,struct1,temp_struct
  new_struct=[temporary(temp_struct),struct2]
 endif else begin
  temp_struct=replicate(clear_struct(struct1(0)),n2)
  copy_struct,struct2,temp_struct
  new_struct=[struct1,temporary(temp_struct)]
 endelse
endif else begin
 if dim1 gt dim2 then new_struct=replicate(struct1(0),n1+n2) else $
  new_struct=replicate(struct2(0),n1+n2) 
  dprint,'% CONCAT_STRUCT: looping...'
 for i=0,n1-1 do begin
  temp=new_struct(0)
  copy_struct,struct1(i),temp,_extra=extra & new_struct(i)=temp
 endfor

 for i=n1,n1+n2-1 do begin
  temp=new_struct(0)
  copy_struct,struct2(i-n1),temp,_extra=extra & new_struct(i)=temp
 endfor
endelse

return,new_struct & end

