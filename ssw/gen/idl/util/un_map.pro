;+
; Project     : SOHO-CDS
;
; Name        : UN_MAP
;
; Purpose     : Unpack data and pixel coordinate arrays for image
;               saved in a map structure created by MK_MAP
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      :  un_map,img,data,xp,yp
;
; Examples    :
;
; Inputs      :  MAP = map structure created by MK_MAP
;
; Opt. Inputs : None
;
;
; Outputs     : DATA = 2d image array
;               XP = cartesian coordinates of image pixels in x-direction (+ W)
;               YP = cartesian coordinates of image pixels in y-direction (+ N)
; Opt. Outputs: 
;
; Keywords    : VELOCITY = set to check and extract velocity map data
;               stored with tagname starting with "VEL"
;               DX,DY = mean spacing between pixels
;               XC,YC = mean center of image
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 22 October 1996, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

pro un_map,img,data,xp,yp,dx=dx,dy=dy,xc=xc,yc=yc,xside=xside,yside=yside,velocity=velocity

sc=size(img) 
if datatype(img) ne 'STC' then begin
 message,'input image not a valid structure',/cont & return
endif
sz=size(img.data)

;-- is there a velocity map?

do_vel=0
if keyword_set(velocity) then begin
 tags=tag_names(img)
 vel_pos=strpos(strupcase(tags),'VEL')
 vel_find=where(vel_pos gt -1,count)
 do_vel=(count gt 0)
endif
 
twod=sz(0) eq 2
if twod then begin
 if do_vel then data=temporary(img.(vel_find(0))) else $
  data=temporary(img.data)
 xp=temporary(img.xp)
 yp=temporary(img.yp) & nx=sz(1) & ny=sz(2)
endif else begin
 mbuf=sz(1) & nx=img.nx & ny=img.ny 
 if nx*ny gt mbuf then begin
  message,'image size exceeds buffer size',/cont & return
 endif
 if datatype(img.data) eq 'BYT' then data=bytarr(nx,ny) else data=fltarr(nx,ny) 
 xp=float(data) & yp=float(data)
 data(0)=img.data(0:nx*ny-1)
 xp(0)=img.xp(0:nx*ny-1) & yp(0)=img.yp(0:nx*ny-1)
endelse

;-- take care of strip rasters

if (nx eq 1) and (not 2d) then begin
 if datatype(img.data) then buff=bytarr(ny,2) else buff=fltarr(ny,2)
 buff(0)=data(*) & data=transpose(buff) & nx=2 & img.nx=2
 dx=(max(yp)-min(yp))/(ny-1)
 xp=[xp,xp+dx] &  yp=[yp,yp] 
 img.xp=xp(*) & img.yp=yp(*) & img.data=data
endif

;-- compute size, center, and pixel spacings for image
;-- (warning: these concepts are meaningless if image has been warped)

 min_x=min(xp) & max_x=max(xp)
 min_y=min(yp) & max_y=max(yp)
 xb = [min_x,max_x,max_x,min_x,min_x]
 yb = [min_y,min_y,max_y,max_y,min_y]
 xc=(min_x+max_x)/2. &  yc=(min_y+max_y)/2.
 xside=abs(max_x-min_x) & yside=abs(max_y-min_y)
 dx=xside/(nx-1.) & dy=yside/(ny-1.)

 return & end

