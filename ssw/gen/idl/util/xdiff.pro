;+
; Project     : SOHO - CDS     
;                   
; Name        : XDIFF
;               
; Purpose     : Show differences between program versions
;               
; Explanation : When altering IDL software I often find it convenient to be
;               able to pinpoint the changes that have been made, in order to
;               ascertain that no unintended alterations were made, and in
;               order to write up a modification notice in the documentation.
;
;               Given the name of an IDL .pro file, this program locates the
;               first occurence of the program in the path, including the
;               current directory. This file is assumed to be the new,
;               "development" copy. Then the second occurence of the program
;               in the path is located (assumed to be the old version), and
;               the two are processed by the Unix program diff, with default
;               flags to make no distinction between upper and lower case, and
;               to ignore white space differences.
;
;               The output from diff is processed and the files are padded as
;               to display them side by side by XTEXT, with marks inside two
;               vertical bars for lines that have been changed. The notation
;               is:
;
;               |c| Line changed
;               |+| Line was added (blank line inserted in display)
;               |-| Line was deleted (i.e., added in the *other* copy).
;               | | Line was unchanged.
;
;               If more than one "old" copy are present, it is possible to
;               tell XDIFF to skip one or more versions when locating the old
;               copy, by setting the SKIP keyword to the number of versions to
;               skip.
;
;               You can compare two different programs as well, by supplying a
;               two-element string array as the program name. This is useful
;               for comparing "isomorphic" routines.
;               
;               I normally use a suicidally small font when programming, so
;               I've included a check whether it's me working or someone else
;               when setting the font. It's also possible to use the FONT
;               keyword to change the display font.
;               
; Use         : XDIFF,'<program-name>' [,flags]
;    
; Inputs      : '<program-name>' the name of the program to compare with older
;               versions.
;
;               If program_name is a two-element array, the two different
;               programs are compared (always the first found copies).
;               
; Opt. Inputs : FLAGS : The diff flags to be used. Defaults to "-iw", which
;                       ignores case changes and white space, which is quite
;                       convenient when comparing programs.
;               
; Outputs     : None.
;               
; Opt. Outputs: None.
;               
; Keywords    : SKIP : Number of (old) versions in the path to skip.
;
;               FONT : The font to use for the text display.
;
; Calls       : BREAK_FILE, DEFAULT, DETABIFY(), FIND_WITH_DEF(), RD_ASCII(),
;               XTEXT
;
; Common      : None.
;               
; Restrictions: Unix specific.
;               
; Side effects: None.
;               
; Category    : Utility
;               
; Prev. Hist. : Yes, I know about dxdiff, but I cannot control the diff flags
;               used in it, and I don't like the visual appearance.
;
; Written     : Stein Vidar Hagfors Haugan, UiO, 17 June 1996
;               
; Modified    : Version 2, SVHH, 18 June 1996
;                       Added comparison of two differently named files.
;               Version 3, SVHH, 16 October 1996
;                       Fixed a bug in XDIFF_ADD that crashed in marginal
;                       cases.
;
; Version     : 3, 16 October 1996
;-            



PRO xdiff_parse4,str,sep,n1,n2,n3,n4
  
  sep = str_sep(str,sep)
  
  s12 = str_sep(sep(0),',')
  
  n1 = LONG(s12(0))
  n2 = LONG(s12(N_ELEMENTS(s12)-1))
  
  s34 = str_sep(sep(1),',')
  n3 = LONG(s34(0))
  n4 = LONG(s34(N_ELEMENTS(s34)-1))
  
END


FUNCTION xdiff_rempath,path,file
  
  break_file,file,disk,dir,filnam,ext
  foundpath = disk+dir
  
  IF foundpath EQ '' THEN RETURN,path
  
  cpath = str_sep(path,':')
  
  ;; Chop / at end
  foundpath = STRMID(foundpath,0,STRLEN(foundpath)-1) 
  
  goodix = WHERE(STRPOS(cpath,foundpath) EQ -1,count)
  IF count EQ -1 THEN BEGIN
     PRINT,"What's wrong with your !path"
     RETURN,path
  END
  ;; Gather new path
  cpath = cpath(goodix)
  npath = cpath(0)
  FOR i = 1,N_ELEMENTS(cpath)-1 DO npath =  npath + ':'+ cpath(i)
  RETURN,npath
END


PRO xdiff_add,to,from,n1,n2,n3,n4,toadded,fromadded
  ON_ERROR,0
  n1 = n1 + toadded
  n2 = n2 + toadded
  n3 = n3 + fromadded
  n4 = n4 + fromadded
  add = n4-n3+1
  toadded = toadded+add
  
  IF N_elements(to) EQ 0 THEN to = REPLICATE('|+|',add) $
  ELSE IF n1 EQ 0 THEN to = [REPLICATE('|+|',add),to(n1:*)] $
  ELSE IF n1 EQ N_elements(to) THEN to = [to,REPLICATE('|+|',add)] $
  ELSE to = [to(0:n1-1),REPLICATE('|+|',add),to(n1:*)]
  from(n3-1:n4-1) = '|-|' + from(n3-1:n4-1)
END


PRO xdiff,file,flags,skip=skip,font=font
  ON_ERROR,0
  
  default,flags,"-iw"
  default,font,''
  default,skip,0
  
  parcheck,file,1,typ(/str),[0,1],'FILE'
  parcheck,flags,2,typ(/str),0,'FLAGS'
  
  flags = str_sep(flags,' ')
  
  
  IF N_ELEMENTS(file) GT 2 THEN BEGIN
     PRINT,"Can only compare 2 files at a time"
     RETURN
  END
  
  ;; Find the current one (first in path)
     
  curf = find_with_def(file(0),!Path,'.pro')
  
  IF curf EQ '' THEN BEGIN
     PRINT,"File not found:" + file(0)
     RETURN
  END
  
  IF N_ELEMENTS(file) EQ 1 THEN BEGIN
     path = xdiff_rempath(!path,curf)
     nextf = find_with_def(file,path,'.pro',/nocurrent)
     
     WHILE skip GT 0 DO BEGIN
        path = xdiff_rempath(path,nextf)
        nextf = find_with_def(file,path,'.pro',/nocurrent)
        skip = skip-1
     END
  END ELSE BEGIN
     nextf = find_with_def(file(1),!path,'.pro')
     IF nextf EQ '' THEN BEGIN
        PRINT,"File not found:"+file(1)
        RETURN
     END
  END
  
     
  IF nextf(0) EQ '' THEN BEGIN
     PRINT,"Only one copy found"
     RETURN
  END
  
  spawn,/noshell,["diff",flags,nextf,curf],result
  
  IF result(0) EQ '' THEN BEGIN
     PRINT,"No differences found between files:"
     PRINT,nextf
     PRINT,curf
     RETURN
  END
  
  org = detabify(rd_ascii(nextf))
  cur = detabify(rd_ascii(curf))
  
  stato = intarr(N_ELEMENTS(org))
  statc = intarr(N_ELEMENTS(cur))
  
  oadded = 0
  cadded = 0
  
  i = 0
  nres = N_ELEMENTS(result)
  
  WHILE i LT nres DO BEGIN
     
     
     ccom = result(i)
     IF STRPOS(ccom,"a") NE -1 THEN BEGIN
        xdiff_parse4,ccom,'a',n1,n2,n3,n4
        xdiff_add,org,cur,n1,n2,n3,n4,oadded,cadded
     END ELSE IF STRPOS(ccom,"d") NE -1 THEN BEGIN
        xdiff_parse4,ccom,'d',n1,n2,n3,n4
        xdiff_add,cur,org,n3,n4,n1,n2,cadded,oadded
     END ELSE IF STRPOS(ccom,"c") NE -1 THEN BEGIN
        xdiff_parse4,ccom,'c',n1,n2,n3,n4
        nn1 = n1 + oadded
        nn2 = n2 + oadded
        nn3 = n3 + cadded
        nn4 = n4 + cadded
        ochange = n2-n1
        cchange = n4-n3
        delta = cchange - ochange
        both = ochange < cchange
        org(nn1-1:nn1+both-1) = '|c|' + org(nn1-1:nn1+both-1)
        cur(nn3-1:nn3+both-1) =  '|c|' + cur(nn3-1:nn3+both-1)
        IF delta GT 0 THEN BEGIN
           xdiff_add,org,cur,n1+both,n1+both,n3+both+1,n4,oadded,cadded
           ;org=[org(0:n2-1),REPLICATE('|+|',delta),org(n2:*)]
           ;oadded = oadded + delta
           ;cur(n3+both:n4-1) = '|x|' + cur(n3+both:n4-1)
        END
        IF delta LT 0 THEN BEGIN 
           xdiff_add,cur,org,n3+both,n3+both,n1+both+1,n2,cadded,oadded
           ;cur=[cur(0:n4-1),REPLICATE('|-|',-delta),cur(n4:*)]
           ;cadded = cadded - delta
           ;org(n1+both:n2-1) = '|+|' + org(n1+both:n2-1)
        END
     END
     
     i = i+1
     
     WHILE STRPOS("<->",STRMID(result(i),0,1)) NE -1 AND i LT nres-1 DO $
        i = i+1
     
     IF STRPOS("<->",STRMID(result(i),0,1)) NE -1 THEN i = i+1
  END
  
  ix = WHERE(STRMID(org,0,1) NE '|',count)
  IF count GT 0 THEN org(ix) = '| |'+org(ix)
  
  ix = WHERE(STRMID(cur,0,1) NE '|',count)
  IF count GT 0 THEN cur(ix) = '| |'+cur(ix)
  
  org = byte(org)
  ix = WHERE(org EQ 0b,count)
  IF count GT 0 THEN org(ix) = 32b
  
  
  cur = byte(cur)
  ix = WHERE(cur EQ 0b,count)
  IF count GT 0 THEN cur(ix) = 32b  
  
  xsize = (SIZE(cur))(1) + (SIZE(org))(1) 

  IF getenv("USER") EQ 'steinhh' THEN  font =  $
     '-schumacher-clean-medium-r-normal-*-8-*-*-*-*-50-iso8859-1'
  
  xtext,STRING(org)+STRING(cur),xsize=xsize,font=font,$
     title = nextf + '     ==>    ' + curf
  
END


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; End of 'xdiff.pro'.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
