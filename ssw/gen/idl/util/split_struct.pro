;+
; Project     :	SOHO - CDS
;
; Name        :	SPLIT_STRUCT
;
; Purpose     :	split two structures apart
;
; Explanation :
;
; Use         : SPLIT_STRUCT,STRUCT,INDEX,S1,S2
;
; Inputs      :	STRUCT = input structure to split
;               INDEX  = index (or tag name) at which to break off structure
;
; Opt. Inputs :	None.
;
; Outputs     :	S1, S2 = split structures
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Calls       :	None.
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Category    :	Structure handling
;
; Prev. Hist. :	None.
;
; Written     :	Dominic Zarro (ARC)
;
; Version     :	Version 1.0, 22 January 1995
;-


pro split_struct,struct,index,s1,s2

on_error,1

if (datatype(struct) ne 'STC') or (n_elements(index) eq 0) then begin
 message,'syntax --> SPLIT_STRUCT,STRUCT,INDEX,S1,S2',/cont
 return
endif

tags=tag_names(struct)
ntags=n_elements(tags)
if datatype(index) eq 'STR' then begin
 cut=where(strupcase(strtrim(index,2)) eq tags,count)
 if (count eq 0) then begin
  message,'no such tag',/cont
  return
 endif
endif else cut=index
cut=cut(0)

if cut eq 0 then begin
 delvarx,s1 & s2=struct
 return
endif


s1_tags=tags(0:cut-1)
s2_tags=tags(cut:ntags-1)

s1=struct
s2=struct
for i=cut,ntags-1 do s1=rem_tag(s1,tags(i))
for i=0,cut-1 do s2=rem_tag(s2,tags(i))

return & end

