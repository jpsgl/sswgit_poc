;+
; Project     : Hinode/EIS
;
; Name        : LIST_CACHE
;
; Purpose     : Cache time-based search results
;
; Category    : utility
;
; Syntax      : IDL> list_cache,id,tstart,tend
;
; Inputs      : ID = string identifier for results
;               TSTART/TEND: start/end times used for searching
;               TIMES = time array of search results
;               DATA = structure array of search results
;
; Keywords    : SET - to save results
;               GET - to restore results
;               DELETE - to delete results for input ID
;               COUNT - # of returned results
;               WITHIN_TIMES - true if search times fall within range
;                of last search times
;               CLEAR - clear cache completely
;
; History     : Written 16 March 2007, Zarro (ADNET) 
;
; Contact     : dzarro@solar.stanford.edu
;-


pro list_cache,id,tstart,tend,times,data,status=status,set=set,$
         delete=delete,count=count,within_times=within_times,$
         clear=clear

count=0 & last_count=0
common list_cache,fifo
if not obj_valid(fifo) then fifo=obj_new('fifo')

if keyword_set(clear) then begin
 fifo->empty
 return
endif

if is_blank(id) then begin
 message,'ID name not set',/cont
 return
endif

;-- delete data

if keyword_set(delete) then begin
 fifo->delete,id
 return
endif

if (1-valid_time(tstart)) or (1-valid_time(tend)) then begin
 message,'TSTART/TEND not set',/cont
 return
endif
dstart=anytim2tai(tstart) & dend=anytim2tai(tend)

;-- set data

if keyword_set(set) then begin

 if not exist(times) then begin
  err='missing data times'
  message,err,/cont
  return
 endif

 dcount=n_elements(data)                                                
 count=n_elements(times)
 if dcount eq 0 then $
  last={data:0,times:times,tstart:dstart,tend:dend,count:0} else $
   last={data:data,times:times,tstart:dstart,tend:dend,count:count}
 fifo->set,id,last
 return
endif

;-- get data

delvarx,times,data
fifo->get,id,last
if (1-is_struct(last)) then return

within_times= (dstart le last.tend) and (dstart ge last.tstart) and $
              (dend le last.tend) and (dend ge last.tstart)

last_count=last.count
if (within_times) and (last.count gt 0) then begin
 keep=where(last.times ge dstart and last.times le dend,count)
 if count gt 0 then begin
  if count lt n_elements(last.times) then begin
   data=(last.data)[keep]
   times=(last.times)[keep]
  endif else begin
   data=last.data
   times=last.times
  endelse
 endif
endif
 
return & end


