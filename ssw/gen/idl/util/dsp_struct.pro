;+
; Project     : SOHO - CDS     
;                   
; Name        : DSP_STRUCT
;               
; Purpose     : Display contents of a structure as a pulldown menu.
;               
; Explanation : A pulldown menu is created representing all the
;		levels of "introspection" of the structure variable.
;
;		If a tag in a structure is an array, it's "written out"
;		if it's less than SIZE, which by default is set to 30.
;
;		It may be used as a standalone widget or as part of a
;		menu. In the latter case, it generates a pulldown button
;		that unfolds the structure.
;               
; Use         : DSP_STRUCT, DATA_STRUCTURE
;    
; Inputs      : DATA_STRUCTURE: Any IDL structure.
;               
; Opt. Inputs : None.
;               
; Outputs     : None.
;               
; Opt. Outputs: None.
;               
; Keywords    : TITLE: The title of the menu/button.
;
;		SIZE: Controls the maximum size of arrays to be expanded
;
;		ON_BASE: The base to place the button on if it's supposed
;			to be a subpart of another widget hierarchy.
;			
;		ALONE: Set to make it be a stand-alone widget application.
;
; Calls       : DATATYPE(), TRIM()
;
; Common      : None.
;               
; Restrictions: None.
;               
; Side effects: 
;               
; Category    : CDS, QL, DISPLAY, UTILITY
;               
; Prev. Hist. : Requested simultaneously by the Goddard & Rutherford
;		people, independent of each other.
;
; Written     : SVHH, 15 January 1994
;               
; Modified    : SVHH, Documentation added March 1994, stand-alone mode added.
;		SVHH, 3-May-1995, Version 1.1
;			Made arrays unfold the first <SIZE> number
;			of elements.
; Version     : 1.1
;-            

PRO dsp_array,arr,menu=menu,elem=elem,Size=Size
  IF N_elements(elem) eq 0 THEN	elem = '('
  sz = Size(arr)
  FOR i=0, ((sz(1)-1)<Size) DO BEGIN
      item = reform(arr(i,*,*,*,*,*,*))
      menuline = '"'+elem+trim(i)+')'
      IF (Size(arr))(0)	gt 1 THEN BEGIN
	  menu = [menu,menuline]
	  dsp_array,item,elem=elem+trim(i)+',',Size=Size
      END ELSE IF datatype(item) eq 'STC' THEN BEGIN
	  menu = [menu,menuline+'" {']
	  dsp_struct,item,menu=menu,Size=Size
      END ELSE BEGIN
	  menu = [menu,menuline	+' = '+ntrim(item(0))+'"']
      END
  EndFOR
  
  menu = [menu,'}']
END


PRO no_operation,event
  Widget_CONTROL,event.id,Get_UVALUE=uvalue
  
  IF uvalue eq 'QUIT' THEN Widget_CONTROL,event.top,/destroy
  
END




PRO dsp_struct,str,menu=menu,Size=Size,$
			title=title,on_base=on_base,alone=alone
  names	= tag_names(str)
  tag =	0
  menuowner = 1
  
  IF N_elements(Size) eq 0 THEN	Size = 30
  
  IF N_elements(menu) gt 0 THEN	menuowner = 0
  
  FOR i=0,N_elements(names)-1 DO BEGIN
      r	= execute("tag = str."+names(i))
      
      menuline = '"'+names(i)
      
      dtype = datatype(tag)
      
      IF N_elements(tag) eq 1 THEN BEGIN
	  IF dtype eq 'STC' THEN BEGIN
	      menuline=menuline	+ '('+ tag_names(tag,/structure)+")""  {"
	      IF N_elements(menu) gt 0 THEN menu = [menu,menuline] $
	      ELSE			    menu = menuline
	      dsp_struct,tag,menu=menu,Size=Size
	  END ELSE BEGIN
	      menuline = menuline + ' = ' +ntrim(tag) +'"' ; "
	      IF N_elements(menu) gt 0 THEN menu = [menu,menuline] $
	      ELSE			    menu = menuline
	  END
      END ELSE BEGIN
	  IF dtype eq 'STC' THEN $
		  menuline = menuline+'('+tag_names(tag,/structure)+')'
	  menuline = menuline +	'('
	  sz = Size(tag)
	  Ndims	= sz(0)
	  FOR d	= 1,Ndims DO BEGIN
	      menuline = menuline + trim(sz(d))
	      IF d ne ndims THEN menuline = menuline+','
	  EndFOR
	  menuline = menuline+')'
	  ; Now it displays some elements no matter what!
	  IF sz(0) eq 1	 and (1 or sz(1) lt Size) THEN	BEGIN
	      menuline = menuline + '" {'		   ; "
	      IF N_elements(menu) gt 0 THEN menu = [menu,menuline] $
	      ELSE			    menu = menuline
	      dsp_array,tag,menu=menu,Size=Size
	  END ELSE BEGIN
	      menuline = menuline + '"'	; "
	      IF N_elements(menu) gt 0 THEN menu = [menu,menuline] $
	      ELSE			    menu = menuline
	  END
	  
      END
      
  EndFOR
  
  IF not menuowner THEN	BEGIN
      menu = [menu,"}"]
      return
  END
  
  IF N_elements(title) eq 0 THEN BEGIN
      title = tag_names(str,/structure_name)
      IF title eq '' THEN title	= 'Anonymous structure'	$
      ELSE title = 'Structure: '+title
  EndIF
  
  ix = where(strpos(menu,'"') gt -1)		     ; "
  len =	strpos(strmid(menu(ix),1,1000),'"')	   ; "
  maxw = max(len)
  FOR i=0,N_elements(ix)-1 DO $
	  menu(ix(i)) =	strmid(menu(ix(i)),0,len(i)+1) + $
			  strmid("                       ",0,maxw-len(i)) + $
			  strmid(menu(ix(i)),len(i)+1,1000)
  
  IF N_elements(on_base) eq 0 THEN BEGIN
      base = Widget_BASE(title=title,/column,event_pro="no_operation")
      IF Keyword_SET(alone) THEN BEGIN
	  menu = [ '"'+title+'" {', menu, '}' ]
	  dummy	= Widget_BUTTON(base,value='Quit',uvalue='QUIT')
      EndIF
      XPdMenu,menu,base,/column
      Widget_CONTROL,base,/realize
      IF Keyword_SET(alone) THEN Xmanager,"dsp_struct",base,$
					      event_handler="no_operation"
  END ELSE BEGIN
      localbase=Widget_BASE(on_base,event_pro="no_operation",/row)
      XPdMenu,['"'+title+'" {',menu,'}'],localbase
  END
  
END
