;+
; Project     : SOHO - CDS
;
; Name        : GET_POINTER()
;
; Purpose     : to get a pointer value from a pointer variable
;
; Category    : Help
;
; Explanation : retrieves a pointer value from a pointer variable.
;               Pointer variable can be an unrealized widget or handle.
;
; Syntax      : IDL> value = get_pointer(pointer)
;
; Inputs      : POINTER = pointer variable
;
; Opt. Inputs : None
;
; Outputs     : Value associated with POINTER
;
; Opt. Outputs: None
;
; Keywords    : NOCOPY   -  do not make internal copy of value
;             : UNDEFINED - 0/1 if returned value is defined/undefined
;
; Common      : None
;
; Restrictions: POINTER must be defined via MAKE_POINTER
;
; Side effects: memory value of POINTER is removed when /NO_COPY set
;
; History     : Version 1,  1-Sep-1995,  D.M. Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function get_pointer,pointer,no_copy=no_copy,$
         undefined=undefined,handle=handle,quiet=quiet

on_error,1

undefined=0
if not exist(pointer) then goto,bail_out

;-- check if pointer is an unrealized widget

if xalive(pointer) and (not keyword_set(handle)) then begin
 if not widget_info(pointer,/realized) then begin
  widget_control,pointer,get_uvalue=value,no_copy=keyword_set(no_copy)
  if not exist(value) then begin
   undefined=1 & value=-1
   return,value
  endif
 endif
endif

;-- check if pointer is a valid handle 

vers=float(strmid(!version.release,0,3))
if exist(pointer) and (vers ge 3.6) then begin
 if call_function('handle_info',pointer,/valid_id) then begin
  handle_value,pointer,value,no_copy=keyword_set(no_copy)
  if not exist(value) then begin
   undefined=1 & value=-1
  endif
  return,value
 endif
endif

if not keyword_set(quiet) then begin
 dprint,'% GET_POINTER: invalid pointer passed to '+get_caller()
endif

bail_out:
if not exist(value) then begin
 undefined=1 & value=-1
endif

return,value & end
