;+
; Project     : SOHO - CDS
;
; Name        : SET_POINTER
;
; Purpose     : to set a pointer value to a pointer variable
;
; Category    : Help
;
; Explanation : assign a pointer value to a pointer variable.
;               Pointer variable can be an unrealized widget or handle.
;
; Syntax      : IDL> set_pointer,pointer,value
;
; Inputs      : POINTER = pointer variable
;             : VALUE = value to assign
;
; Opt. Inputs : None
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : NOCOPY   -  do not make internal copy of value
;
; Common      : None
;
; Restrictions: POINTER must be defined via MAKE_POINTER
;
; Side effects: external value of POINTER is removed when /NO_COPY set
;
; History     : Version 1,  1-Sep-1995,  D.M. Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro set_pointer,pointer,value,no_copy=no_copy,handle=handle

on_error,1

if not exist(value) then begin
 message,'SYNTAX --> set_pointer,pointer,value,[/no_copy]',/cont
 return
endif

;-- check if pointer is an unrealized widget

if xalive(pointer) and (not keyword_set(handle)) then begin
 if not widget_info(pointer,/realized) then begin
  widget_control,pointer,set_uvalue=value,no_copy=keyword_set(no_copy)
  return
 endif
endif

;-- check if pointer is a valid handle 

vers=float(strmid(!version.release,0,3))
if exist(pointer) and (vers ge 3.6) then begin
 if call_function('handle_info',pointer,/valid_id) then begin
  handle_value,pointer,value,no_copy=keyword_set(no_copy),/set
  return
 endif
endif

dprint,'%SET_POINTER: input pointer is invalid'

return & end
