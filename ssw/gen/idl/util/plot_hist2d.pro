;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;+
; Name:
;
;      PLOT_HIST2D
;
; Purpose:
;
;      Display a 2-D histogram of two arrays/images.
;
; Category:
;
;      Data processing
;
; Calling sequence:
;
;      PLOT_HIST2D,image1,image2 [ , XHRANGE=xhrange , YHRANGE=yhrange ,
;                                    XRANGE=xrange   , YRANGE=yrange   ,
;                  HISTOGRAM=hist , INFO=hist_info , IMAGES=hist_input ,
;                  LOG=log_hist ,
;                  ... other MAKE_HIST2D/PLOT_IMAGE keywords ... ]
;
; Inputs:
;
;      image1: 1st image (or set of images) [X]
;      image2: 2nd image (       "        ) [Y]
;
; Keyword parameters:
;
;      X/YHRANGE = range of X/Y values to be used in computing the histogram
;      X/YRANGE =  range of X/Y axes
;      HISTOGRAM = 2D histogram (2-elements array; OUT)
;      INFO   = info. (dimension, origin, binsize) about the histogram array; 
;               array (2 elements) of structures (OUT), tags:
;                 dim: i-th dimension of the histogram image;
;                 zero: origin of the i-th axis;
;                 bin: bin factor for the i-th axis;
;                 log: (boolean) histogram in logarithm space?
;      IMAGES = actual images used to form the histogram; structure (OUT), tags:
;                 flt: float input array (log scaled, if X/YLOG is set);
;                 int: integer-scaled input array;
;               Given in output only if explicitly requested by setting 
;               hist_input to some value.
;      LOG = display log of histogram
;
; Output:
;
;      Graphic
;
; Common blocks:
;
;      None
;
; Calls:
;
;      MAKE_HIST2D,PLOT_IMAGE
;
; Description:
;
;      Plot a two-dimensional histogram created from two images/arrays. 
;
; Side effects:
;
;      None
;
; Notes:
;
;        The values of PLOT_IMAGE keywords ORIGIN and SCALE are overridden 
;      by the values provided by MAKE_HIST2D. 
;        When X/YLOG are set, the convention for representing ranges 
;      with keywords X/YHRANGE and X/YRANGE differs. X/YHRANGE represent the 
;      ranges in logarithm space of the values to be considered in forming 
;      the 2-D histogram; thus, for example, the values [-3,1] limit the range 
;      to the values between 0.001 and 10. On the other hand, X/YRANGE give 
;      the actual plot range; e. g.: [1.e-3,10]. 
;        When LOG is set, the logarithm of the histogram image augmented by 
;      one is displayed. This offset is added so that bins with no data 
;      will not be treated as missing by PLOT_IMAGE: otherwise they will assigned,  
;      by default, the same color index as bins with the next lowest number of 
;      data.
;        See MAKE_HIST2D and PLOT_IMAGE for other notes. 
;
; Modification history:
;
;      V. Andretta, 22/Nov/1998 - Created
;      V. Andretta, 27/Feb/1999 - Added LOG keyword
;      W. Thompson, 17-Mar-1999, Use PLOT_IMAGE instead of TVPLOT
;
; Contact:
;
;      VAndretta@solar.stanford.edu
;-
;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;

  pro PLOT_HIST2D,image1,image2 $
                 ,XHRANGE=xhrange,YHRANGE=yhrange $
                 ,XRANGE=xrange  ,YRANGE=yrange   $
                 ,HISTOGRAM=hist,INFO=hist_info,IMAGES=hist_input $
                 ,LOG=log_hist,_EXTRA=_extra

  ON_ERROR,2



;%%%%%
;%%%%% Compute histogram
;%%%%%

  hist=MAKE_HIST2D(image1,image2,XHRANGE=xhrange,YHRANGE=yhrange $
                  ,INFO=hist_info,IMAGES=hist_input,_EXTRA=_extra)

  if N_ELEMENTS(hist) le 1 or N_ELEMENTS(hist_info) eq 0 then RETURN


;%%%%%
;%%%%% Define origin, scale and range of plot axes (must also override  
;%%%%% values of keywords ORIGIN and SCALE given in input
;%%%%%

;Define plot axes
  origin=hist_info.zero ;% +0.5*hist_info.bin
  scale=hist_info.bin

;Override values of ORIGIN and SCALE
  if N_ELEMENTS(_extra) gt 0 then if N_TAGS(_extra) gt 0 then begin

;Get number of tags and their names:
    Ntags=N_TAGS(_extra)
    kwd=TAG_NAMES(_extra)

;Check whether ORIGIN and SCALE keywords are set
;ORIGIN:
    tag1=WHERE(STRMID(kwd,0,3) eq 'ORI',Ntag1)
;SCALE:
    tag2=WHERE(STRMID(kwd,0,2) eq 'SC' ,Ntag2)
;All others:
    tags=WHERE(STRMID(kwd,0,3) ne 'ORI' and STRMID(kwd,0,2) ne 'SC' ,Ntags)

;Create new _EXTRA structure
    if Ntag1 gt 0 or Ntag2 gt 0 then begin
      p_extra=0
;All keywords except ORIGIN and SCALE:
      for i=0,Ntags-1 do begin
        itag=tags(i)
        if N_TAGS(p_extra) eq 0 then $
          p_extra=CREATE_STRUCT(kwd(itag),_extra.(itag)) $
        else $
          p_extra=CREATE_STRUCT(p_extra,kwd(itag),_extra.(itag))
      endfor
;Keyword ORIGIN:
      for i=0,Ntag1-1 do begin
        itag=tag1(i)
        if N_TAGS(p_extra) eq 0 then $
          p_extra=CREATE_STRUCT(kwd(itag),origin) $
        else $
          p_extra=CREATE_STRUCT(p_extra,kwd(itag),origin)
      endfor
;Keyword SCALE:
      for i=0,Ntag2-1 do begin
        itag=tag2(i)
        if N_TAGS(p_extra) eq 0 then $
          p_extra=CREATE_STRUCT(kwd(itag),scale) $
        else $
          p_extra=CREATE_STRUCT(p_extra,kwd(itag),scale)
      endfor
    endif else $
      p_extra=_extra

  endif


;%%%%%
;%%%%% Plot histogram
;%%%%%


  PLOT_IMAGE,hist+KEYWORD_SET(log_hist) $
        ,XRANGE=xrange,YRANGE=yrange,ORIGIN=origin,SCALE=scale $
        ,_EXTRA=p_extra


;%%% Return

  RETURN
  end
