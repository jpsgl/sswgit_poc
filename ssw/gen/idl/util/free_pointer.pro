;+
; Project     : SOHO - CDS
;
; Name        : FREE_POINTER()
;
; Purpose     : to free a pointer variable
;
; Category    : Help
;
; Explanation : removes a pointer variable.
;               Pointer variable can be an unrealized WIDGET or HANDLE.
;
; Syntax      : IDL> free_pointer(pointer)
;
; Inputs      :  POINTER = pointer variable
;
; Opt. Inputs : None
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Common      : None
;
; Restrictions: POINTER becomes invalid
;
; Side effects: None
;
; History     : Version 1,  1-Sep-1995,  D.M. Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro free_pointer,pointer

on_error,1

;-- check if an unrealized widget id is being used as a pointer
;-- if so, then kill it and return

if datatype(pointer) ne 'LON' then return

np=n_elements(pointer)
if np gt 1 then begin
 for i=0,np-1 do free_pointer,pointer(i)
 return
endif

vers=float(strmid(!version.release,0,3))
if exist(pointer) and (vers ge 3.6) then begin
 if call_function('handle_info',pointer,/valid_id) then begin
  handle_free,pointer
  return
 endif
endif

;-- if we got this far, then it's probably a widget

if xalive(pointer) then begin
 if not widget_info(pointer,/realized) then begin
  xkill,pointer
  return
 endif
endif

return & end
