;+
; Project     :  YOHKOH-BCS
;
; Name	      :  GAUSS_FIT
;
; Purpose     : Gaussian function fit to line profile
;
; Explanation:
;       If the (MAX-AVG) of Y is larger than (AVG-MIN) then it is assumed
;	that the line is an emission line, otherwise it is assumed there
;	is an absorbtion line.  The estimated center is the MAX or MIN
;	element.  The height is (MAX-AVG) or (AVG-MIN) respectively.
;	The width is found by searching out from the extrema until
;	a point is found less than the 1/e value.
;
; Category    : fitting
;
; Syntax      : g=gauss_fit(x,y,a,sigmaa)
;
; Inputs      : y = data to fit
;               x = bin or wavelength
;
; Outputs     : background=a(0)+a(1)*x+a(2)*x^2
;               a(3) = total intensity
;               a(4) = center
;               a(5) = doppler width
;
; Opt. Outputs: sigmaa = sigma errors
;
; Keywords    : weights = data weights
;               nfree  = number of free parameters
;               chi2   = chi^2
;               last   = set to use input 'a' values as starting values
;               fixp   = indicies of parameters to fix
; Restrictions: 
;               The peak or minimum of the Gaussian must be the largest
;	        or smallest point in the Y vector.
;
; Side effects: None
;
; History     : Version 1,  17-July-1987,  D M Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function gauss_fit, x, y, a,sigmaa,chi2=chi2,weights=weights,nfree=nfree,$
                    fixp=fixp,last=last

on_error,1

if (not keyword_set(last)) and (n_elements(a) eq 0) then begin

 n = n_elements(y)		;# of points.
 c = poly_fit(x,y,1,yf)		;fit a straight line.
 yd = y-yf			;difference.

 ymax=max(yd) & xmax=x(!c) & imax=!c	;x,y and subscript of extrema
 ymin=min(yd) & xmin=x(!c) & imin=!c
 a=fltarr(6)			;coefficient vector
 if abs(ymax) gt abs(ymin) then i0=imax else i0=imin ;emiss or absorp?
 i0 = i0 > 1 < (n-2)		;never take edges
 dy=yd(i0)			;diff between extreme and mean
 del = dy/exp(1.)		;1/e value
 i=0
 while ((i0+i+1) lt n) and $	;guess at 1/e width.
	((i0-i) gt 0) and $
	(abs(yd(i0+i)) gt abs(del)) and $
	(abs(yd(i0-i)) gt abs(del)) do i=i+1
 a = [c(0),0,0,yd(i0), x(i0), .5*abs(x(i0)-x(i0+i))] ;estimates
endif

return,funct_fit(x,y,a,sigmaa,weights=weights,fixp=fixp,$
		funct = 'mgauss',chi2=chi2,nfree=nfree) 
end

