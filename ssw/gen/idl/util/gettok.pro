function gettok,st,char
;+
; Project     : SOHO - CDS
;
; Name        : 
;	GETTOK()
; Purpose     : 
;	Extracts tokens from strings.
; Explanation : 
;	Function to retrieve the first part of the string until the character
;	char is encountered.
;
;	If ST is 'abc=999' then gettok(ST,'=') would return 'abc' and ST would
;	be left as '999'.
;
;	This routine is included mainly to support older routines that were
;	written using it.  Users are encouraged to use the similar routine
;	STR_SEP from the IDL User's Library instead for any new routines.
;
; Use         : 
;	Token = GETTOK(ST,CHAR)
; Inputs      : 
;	ST	= Scalar string to get token from.  On output the first token
;		  is removed from the string.
;	CHAR	= Character separating tokens, scalar string
; Opt. Inputs : 
;	None.
; Outputs     : 
;	The result of the function is the first token in the string, from the
;	beginning of the string up to the first instance of the separator
;	character.
; Opt. Outputs: 
;	None.
; Keywords    : 
;	None.
; Calls       : 
;	None.
; Common      : 
;	None.
; Restrictions: 
;	None.
; Side effects: 
;	The input parameter ST is modified by this routine, so that it has one
;	less token in it.
; Category    : 
;	Utilities, Strings.
; Prev. Hist. : 
;	version 1  by D. Lindler APR,86
;	Remove leading blanks    W. Landsman (from JKF)    Aug. 1991
; Written     : 
;	Don Lindler, GSFC/HRS, April 1986.
; Modified    : 
;	Version 1, William Thompson, GSFC, 12 April 1993.
;		Incorporated into CDS library.
; Version     : 
;	Version 1, 12 April 1993.
;-
;
;----------------------------------------------------------------------
        On_error,2                           ;Return to caller
;
; if char is a blank treat tabs as blanks
;
	tab='	'
	while strpos(st,tab) GE 0 do begin    ;Search for tabs
		pos=strpos(st,tab)
		strput,st,' ',pos
	end

        st = strtrim(st,1)                    ;Remove leading blanks
	;
	; find character in string
	;
	pos=strpos(st,char)
	if pos EQ -1 then begin	;char not found?
		token=st
		st=''
		return,token
	endif

	;
	; extract token
	;
	token=strmid(st,0,pos)
	len=strlen(st)
	if pos EQ (len-1) then st='' else st=strmid(st,pos+1,len-pos-1)

	;
	;  Return the result.
	;
	return,token
	end
