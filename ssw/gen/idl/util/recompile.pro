;+
; Project     : SOHO - CDS
;
; Name        : RECOMPILE
;
; Purpose     : recompile a routine
;
; Category    : utility
;
; Explanation : a shell around RESOLVE_ROUTINE (> vers 4) that checks
;               if compiled routine is not recursive, otherwise 
;               recompile will stop suddenly.
;
; Syntax      : IDL> recompile,proc
;
; Inputs      : PROC = procedure name
;
; Opt. Inputs : None
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : /IS_FUNCTION - set if routine is a function
;
; Common      : None
;
; Restrictions: None
;
; Side effects: PROC is recompiled
;
; History     : Version 1,  1-Sep-1996,  D.M. Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro recompile,proc,is_function=is_function,status=status

status=0
if datatype(proc) ne 'STR' then return

;-- version 4 or better only

new_vers=float(strmid(!version.release,0,3)) ge 4.
if not new_vers then return

;-- can't compile if being called earlier by itself

help,calls=calls
nproc=n_elements(proc)
for i=0,n_elements(proc)-1 do begin
 angle=strpos(calls,'<') 
 for j=0,n_elements(calls)-1 do begin
  if angle(j) gt 0 then calls(j)=trim(strmid(calls(j),0,angle(j)))+'.pro'
 endfor
 check_calls=strpos(strupcase(calls),strupcase(trim(proc(i))+'.pro'))
 called=where(check_calls gt -1,cnt)
 if cnt gt 0 then begin
  dprint,'% RECOMPILE: '+'"'+proc+'"'+' being called recursively. Cannot compile.'
 endif else begin
  if not exist(is_function) then is_function=bytarr(nproc)
  key=is_function(i)
  call_procedure,'resolve_routine',proc(i),is_function=keyword_set(key)
  status=1
 endelse
endfor


return & end
