;+
; Project     :	SDAC
;
; Name        :	XALIVE
;
; Purpose     :	To check if an X widget is alive
;
; Explanation :	So obvious, that explaining it will take more
;               lines than the code.
;
; Use         :	ALIVE=XALIVE(ID)
;              
; Inputs      :	ID = widget id to check
;
; Opt. Inputs : None.
;
; Outputs     :	1/0 if alive/dead
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Calls       :	None.
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Category    :	Widgets
;
; Prev. Hist. :	None.
;
; Written     :	Dominic Zarro (ARC)
;
; Version     :	Version 1.0, 18 September 1993
;-

function xalive,id

nid=n_elements(id)
if nid ne 0 then begin
 out=intarr(nid)
 for i=0,nid-1 do begin
  val=id(i)
  if (datatype(val) eq 'LON') then out(i)=widget_info(val,/valid)
 endfor
endif else out=0
if nid eq 1 then out=out(0)
return,out

end
