pro rd_mdi, files, index, data
;
;+
;   Name: rd_mdi
;
;   Purpose: simple mdi front end to mreadfits
;
;   Input Parameters:
;      files - list of one or more mdi fits files
;
;   Output Paramters:
;      index - the structure equivilent header vector
;      data  - optional output 2D or 3D data
;
;   Calling Sequence:
;     rd_mdi, filelist, index [,data ] [,outsize=outsize]
;
;   History:
;      7-April-1998 - S.L.Freeland - handle the large SOI versions
;
;-     

mreadfits,files,index,data,strtemp=mdi_struct()

; fix some SOI fields -> SSW standards
index.cdelt1=gt_tagval(index,/xscale)
index.cdelt2=gt_tagval(index,/yscale)
index.crpix1=gt_tagval(index,/center_x)
index.crpix2=gt_tagval(index,/center_y)
index.solar_r=gt_tagval(index,/r_sun)

ints=anytim(gt_tagval(index,/date_obs),/ints)
index.time=ints.time
index.day=ints.day

return
end
