;+
; Project     :	SDAC
;
; Name        :	XSHOW
;
; Purpose     :	To show (unmap) an X widget
;
; Explanation :	So obvious, that explaining it will take more
;               lines than the code.
;
; Use         :	XSHOW,ID
;              
; Inputs      :	ID = widget ids to show
;
; Opt. Inputs : None.
;
; Outputs     :	None.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Calls       :	None.
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Category    :	Useful stuff
;
; Prev. Hist. :	None.
;
; Written     :	Dominic Zarro (ARC)
;
; Version     :	Version 1.0, 18 September 1993
;-

pro xshow,id

nid=n_elements(id)
if nid gt 0 then begin
 for i=0,nid-1 do begin
  if xalive(id(i)) then begin
   realized=widget_info(id(i),/realized)
   widget_control,id(i),/map,/show,realize=(not realized)
  endif
 endfor
endif

return & end
