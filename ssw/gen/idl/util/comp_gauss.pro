;+
; Project     : SOHO - CDS     
;                   
; Name        : COMP_GAUSS
;               
; Purpose     : Evaluate gaussian component for use in CURVEFIT/CFIT/MCURVEFIT
;               
; Explanation : Evaluates a single gaussian component. The parameters have the
;               same meaning as the gaussian parameters in the standard
;               GAUSSFIT procedure.
;               
; Use         : COMP_GAUSS,X,A,F [,PDER]
;    
; Inputs      : As usual for any CURVEFIT function
;               
; Opt. Inputs : PDER : Partial derivatives are calculated if parameter is
;                      present 
;               
; Outputs     : F : The evaluated gaussian at the given points
;               
; Opt. Outputs: PDER
;               
; Keywords    : None.
;
; Calls       : None.
;
; Common      : None.
;               
; Restrictions: None.
;               
; Side effects: None.
;               
; Category    : Analysis
;               
; Prev. Hist. : None.
;
; Written     : S.V.H.Haugan, UiO, 21 January 1997
;               
; Modified    : Not yet
;
; Version     : 1, 21 January 1997
;-            
PRO comp_gauss,x,a,f,pder
  on_error,0
  
  nx = n_elements(x)
  
  z = (x-a(1))/a(2)
  
  IF n_params() EQ 3 THEN BEGIN
     f = a(0)*exp(-z^2*0.5)
     return
  END
  
  kern = exp(-z^2*0.5)
  
  f = a(0)*kern
  
  pder = fltarr(nx,3,/nozero)
  pder(0,0) = kern              ;
  pder(0,1) = f * z/a(2)        ; a(0)exp(-0.5*(x-a(1))^2/a(2)^2) * (x-a(1))/a(2)^2
  pder(0,2) = pder(*,1) * z     ; a(0)exp(...) * (x-a(1))^2/a(2)^3
END

