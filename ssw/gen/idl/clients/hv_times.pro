;+
; Project     : HELIOVIEWER
;
; Name        : HV_TIMES
;
; Purpose     : Return start/end times of Helioviewer (HV) JPEG2000 files for
;               specified SOURCE ID
;
; Category    : utility system sockets
;
; Example     : IDL> hv_times,source_id,tstart,tend
;
; Inputs      : SOURCE_ID = HV source ID
;
; Outputs     : TSTART, TEND = UT start and end times of JPEG2000 files
;
; Keywords    : VERBOSE = show output results
;
; History     : 5-Dec-2015, Zarro (ADNET) - written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro hv_times,source_id,tstart,tend,err=err,verbose=verbose

tstart=!null & tend=!null
err=''
common hv_source,results

if ~is_number(source_id) then begin
 pr_syntax,'hv_times,source_id,tstart,tend'
 return
endif

if ~is_struct(results) then begin
 server=hv_server()
 sources=server+'/v2/getDataSources/?'
 sock_list,sources,json,err=err
 if is_string(err) then begin
  mprint,err & return
 endif
 results=json_parse(json,/tostruct)
endif

;-- parse JSON output into a structure and drill for times

if ~is_struct(results) then begin 
 err='Helioviewer source file not readable.'
 mprint,err & return
endif

chk=chktag(results,'sourceid',value=source_id,nest=nest,/recurse)
if chk then begin
 tstart=nest.start
 tend=nest._end
 if keyword_set(verbose) then help,nest
endif

return
end

