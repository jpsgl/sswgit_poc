;+
; Project     : SOHO-CDS
;
; Name        : UPDATE_SYNOP_IMAGES
;
; Purpose     : Driver to update SYNOP WWW images for a given date
;
; Category    : planning
;
; Explanation : calls appropriate FTP_xxxx_IMAGES 
;
; Syntax      : update_synop_images,date
;
; Examples    :
;
; Inputs      : None
;
; Opt. Inputs : DATE = date to retrieve
;
; Outputs     : 
;
; Opt. Outputs: None
;
; Keywords    : BACK = # of days to look back 
;               MAIL = send some mail
;
; Common      : None
;
; Restrictions: Unix only 
;
; Side effects: None
;
; History     : Written 14 May 1998 D. Zarro, SAC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

pro update_synop_images,date,back=back,mailto=mailto,_extra=extra

on_error,1

err=''

;-- check for supported images

if datatype(extra) ne 'STC' then begin
 pr_syntax,'update_synop_images,date,[/bbso,/kis,/mlso,back=back]'
 return
endif

supp=['bbso','kis','mlso']
cd,curr=curr_dir
synop_data=trim(getenv('SYNOP_DATA'))
wr_access=test_open(synop_data,/write)

if wr_access then wr_dir=synop_data else begin
 message,'using current directory',/cont & wr_dir=curr_dir
endelse
if not test_open(wr_dir,/write) then return

tags=strlowcase(tag_names(extra))
do_bbso=grep('bbso',tags,index=index1)
do_kis=grep('kis',tags,index=index2)
do_mlso=grep('mlso',tags,index=index3)

case 1 of
 index1 gt -1: begin ftper='ftp_bbso_images' & type='BBSO' & end
 index2 gt -1: begin ftper='ftp_kis_images'  & type='KIS' & end
 index3 gt -1: begin ftper='ftp_mlso_images'  & type='MLSO' & end
 else: begin
  message,'currently only supporting MLSO, BBSO, Kiepenheuer H-alpha files',/cont
  return
 end
endcase

if not exist(back) then back=0
cdate=anytim2utc(date,err=err)
if err ne '' then get_utc,cdate
for i=0,back do begin
 do_date=cdate
 do_date.mjd=do_date.mjd-i
 hcode=strmid(date_code(do_date),2,100)
 outdir=wr_dir+'/gif/'+hcode
 chk_loc=loc_file(outdir,count=dcount)
 if dcount eq 0 then spawn,'mkdir -p '+outdir
 files=call_function(ftper,do_date,outdir=outdir,/kill,count=count,err=err,_extra=extra)
 if count gt 0 then begin
  if exist(lfiles) then lfiles=[lfiles,files] else lfiles=files
 endif
 spawn,'chmod -R ug+w '+outdir
endfor

if exist(mailto) then begin
 run_id=['Results of UPDATE_SYNOP_IMAGES run on '+anytim2utc(cdate,/vms)+':','']
 if exist(lfiles) then $
  mess=['Following '+type+' files created: ','',lfiles] else $
   mess=['No new '+type+' files created']
 if datatype(mailto) eq 'STR' then recip=mailto else recip=get_user_id()
 send_mail,array=[run_id,mess],address=recip
endif


return & end

