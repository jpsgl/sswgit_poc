;+
; Project     : SOHO-CDS
;
; Name        : FTP_MLSO_IMAGES
;
; Purpose     : FTP MLSO MK3 coronameter GIF images
;
; Category    : planning
;
; Explanation : 
;
; Syntax      : files=ftp_mlso_images(date)
;
; Examples    :
;
; Inputs      : None
;
; Opt. Inputs : DATE = date to retrieve
;
; Outputs     : 
;
; Opt. Outputs: FILES = found and renamed filenames
;
; Keywords    : OUTDIR = output directory for file [def = current]
;               ERR = error string
;               COUNT = no of files copied
;               CLOBBER= set to clobber previously copied files
;               BACK= # of days backward to look [def=0]
;
; Common      : None
;
; Restrictions: Unix only 
;
; Side effects: None
;
; History     : Written 18 Dec 1998 D. Zarro, SM&A/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

function ftp_mlso_images,date,count=count,err=err,_extra=extra,quiet=quiet,$
                      outdir=outdir,clobber=clobber,back=back
on_error,1
err=''
count=0
files=''
if not exist(back) then back=0
quiet=keyword_set(quiet)
if not exist(clobber) then clobber=1 else clobber=keyword_set(clobber)
loud=1-quiet
server='ftp.hao.ucar.edu'
port=122

;-- check write access

if datatype(outdir) ne 'STR' then put_dir=curdir() else put_dir=outdir
if not test_dir(put_dir,quiet=quiet,err=err) then return,files

;-- default to current date

cerr=''
hdate=anytim2utc(date,err=cerr)
if cerr ne '' then get_utc,hdate

;-- construct filenames to copy

months=['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec']

raw_get_dir='mk3/raw_daily_image'
for i=0,back do begin
 tdate=hdate
 tdate.mjd=tdate.mjd-i
 hcode=date_code(tdate)
 year=strmid(hcode,0,4)
 month=strmid(hcode,4,2)
 yy=strmid(hcode,2,2)
 doy=utc2doy(tdate) 
 get_dir='mk3/daily_images/'+year+'/'+month+'_'+months(fix(month)-1)
 get_files=yy+'d'+num2str(doy)+'.mk3.gif'

 dprint,'% get_dir, get_files: ',get_dir,'/',get_files

 smart_ftp,server,get_files,get_dir,files=files,count=count,port=port,$
          quiet=quiet,err=err,outdir='/tmp',_extra=extra,/anon
 
 if err eq '' then begin

  if count eq 0 then begin
   if loud then begin
    message,'No files found for '+anytim2utc(hdate,/date,/vms),/cont
    message,'Checking raw daily directory...',/cont
   endif
   get_dir=raw_get_dir
   smart_ftp,server,get_files,get_dir,files=files,count=count,port=port,$
          quiet=quiet,err=err,outdir='/tmp',_extra=extra,/anon
  endif

;-- move files to desired directory

  if count gt 0 then begin
   for k=0,count-1 do begin
    new_file=concat_dir(put_dir,'mlso_cogmk_fd_'+hcode+'_0000')+'.gif'
    chk=loc_file(new_file,count=fcount)
    if (fcount eq 0) or clobber then spawn,'mv -f '+files(k)+' '+new_file
    spawn,'rm -f '+files(k)
    files(k)=new_file 
   endfor
   if exist(tfiles) then tfiles=[tfiles,files] else tfiles=files
  endif
 endif
endfor

count=n_elements(tfiles)
if count gt 0 then files=tfiles
if count eq 1 then files=files(0)

return,files & end

