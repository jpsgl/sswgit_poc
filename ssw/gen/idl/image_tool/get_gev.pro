;+
; Project     : SOHO - CDS
;
; Name        : GET_GEV
;
; Purpose     : Wrapper around RD_GEV
;
; Category    : planning
;
; Explanation : Get GOES Event listing
;
; Syntax      : IDL>gev=get_gev(tstart)
;
; Inputs      : TSTART = start time 
;
; Opt. Inputs : TEND = end time
;
; Outputs     : GEV = event listing in structure format
;
; Opt. Outputs: None
;
; Keywords    : COUNT = # or entries found
;               ERR = error messages
;               QUIET = turn off messages
;               STRING = for string instead of structure output
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Version 1,  20-June-1999,  D.M. Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function get_gev,tstart,tend,count=count,err=err,quiet=quiet,$
               string_format=string_format,print=print

on_error,1
err=''
delvarx,gev
count=0

;-- start with error checks

if not have_proc('rd_gev') then begin
 err='cannot find RD_GEV in IDL !path'
 message,err,/cont
 return,''
endif

if trim(getenv('DIR_GEN_GEV')) eq '' then begin
 err='DIR_GEN_GEV not defined'
 message,err,/cont
 return,''
end

err=''
t1=anytim2utc(tstart,err=err)
if err ne '' then get_utc,t1
t1.time=0

t2=anytim2utc(tend,err=err)
if err ne '' then begin
 t2=t1
 t2.mjd=t2.mjd+1
endif

;-- call RD_GEV

loud=1-keyword_set(quiet)
if loud then begin
 message,'retrieving GEV data for '+ anytim2utc(t1,/vms),/cont
endif

rd_gev,anytim2utc(t1,/vms),anytim2utc(t2,/vms),gev,/nearest,status=pstat
pstat=pstat > 0
status=(1-pstat)
if not status then begin
 err='GOES event data not found for specified times'
 message,err,/cont
 return,''
endif


np=n_elements(gev)
if keyword_set(string_format) then begin
 out=strarr(np)
 times=replicate({time:0l,day:0},np)
 day=gt_day(gev,/str) & tstart=gt_time(gev,/str) 
 times.day=gev.day
 times.time=gev.time
 tpeak=times & tpeak.time=tpeak.time+gev.peak*1000
 tstop=times & tstop.time=tstop.time+gev.duration*1000
 tpeak=gt_time(tpeak,/str)
 tstop=gt_time(tstop,/str)
 class=trim(string(gev.st$class))
 halpha=trim(string(gev.st$halpha))
 noaa=trim(string(gev.noaa))
 none=where(noaa eq '0',count)
 if count gt 0 then noaa(none)=''
 location=gev.location 
 ns=location(1,*) & ew=location(0,*) 
 none=where( (abs(ns) gt 970) or (abs(ew) gt 970),count)
 south=where(ns lt 0,scount)
 north=where(ns ge 0,ncount)
 east=where(ew lt 0,ecount)
 west=where(ew ge 0,wcount)
 ns=string(abs(ns),'(i2.2)')
 ew=string(abs(ew),'(i2.2)')
 if scount gt 0 then ns(south)='S'+ns(south)
 if ncount gt 0 then ns(north)='N'+ns(north)
 if ecount gt 0 then ew(east)='E'+ew(east)
 if wcount gt 0 then ew(west)='W'+ew(west)
 loc=ns+ew
 if count gt 0 then loc(none)=''
 out=day+' '+tstart+' '+tpeak+' '+tstop+' '+class+' '+halpha+' '+loc+' '+noaa
 if keyword_set(print) then print,out
 gev=out 
endif
return,gev

end


