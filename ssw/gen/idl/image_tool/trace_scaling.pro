;+
; PROJECT:
;       SOHO - CDS
;
; NAME:
;       TRACE_SCALING
;
; PURPOSE:
;       Rescale TRACE image
;
; CATEGORY:
;       Image, utility
;
; EXPLANATION:
;
; SYNTAX:
;       Result = trace_scaling(image)
;
; INPUTS:
;       IMAGE   - 2D image array; may be rescaled
;       HEADER  - String vector holding header of TRACE FITS file
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       RESULT  - Rescaled image array
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORDS:
;       MIN_VAL    - Named output variable, new miminum value in IMAGE
;       MAX_VAL    - Named output variable, new maxinum value in IMAGE
;       COLOR_ONLY - Set this keyword to just get color table
;       LOG_SCALED - 1/0 if returned image is log scaled or not
;       NO_PREP    - set to not call TRACESCALE
;
; COMMON:
;       None.
;
; RESTRICTIONS:
;       Use in SSW environment for optimum results
;
; SIDE EFFECTS:
;       Input IMAGE array is rescaled.
;
; HISTORY:
;       Version 1, June 20, 1998, Zarro (SAC/GSFC), Written
;
; CONTACT: 
;       dzarro@solar.stanford.edu
;-
;

function trace_scaling,image,header,min_val=min_val,max_val=max_val,$
                  log_scaled=log_scaled,color_only=color_only,no_prep=no_prep

common trace_scaling,have_tracescale,last_path

log_scaled=0

;-- check if required TRACE routines are in !path

if not exist(last_path) then last_path=''
if (not exist(have_tracescale)) or (!path ne last_path) then begin
 which,/all,'tracescale',out=out
 have_tracescale=trim(out(0)) ne ''
 last_path=!path
endif

color_only=keyword_set(color_only)
do_prep=1-keyword_set(no_prep)

;-- apply color scaling if requested

if (1-color_only) then begin
 if have_tracescale then begin
  if do_prep then image=call_function('tracescale',temporary(image),/despike,/byte)
 endif else begin
  image=cscale(image,/log,/no_copy)
  log_scaled=1
 endelse
endif
max_val = max(image,min=min_val)

if have_tracescale then begin
 fits_interp,header,index
 trace_colors,index 
endif else loadct,3,/silent

return,image & end

