;+
; Project     : SOHO-CDS
;
; Name        : FTP_KIS_IMAGES
;
; Purpose     : FTP Kiepenheuer-Institut H-alpha files for a given date
;
; Category    : planning
;
; Explanation : FTP's KIS Full-Disk H-alpha JPEG files 
;
; Syntax      : files=ftp_kis_images(date)
;
; Examples    :
;
; Inputs      : None
;
; Opt. Inputs : DATE = date to retrieve
;
; Outputs     : 
;
; Opt. Outputs: FILES = found and renamed filenames
;
; Keywords    : OUTDIR = output directory for file [def = current]
;               ERR = error string
;               COUNT = no of files copied
;               CLOBBER= set to clobber previously copied files
;               BACK= # of days backward to look [def=0]
;               FITS = look for FITS files
;
; Common      : None
;
; Restrictions: Unix only 
;
; Side effects: None
;
; History     : Written 8 June 1998 D. Zarro, SAC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

function ftp_kis_images,date,count=count,err=err,quiet=quiet,$
                      outdir=outdir,clobber=clobber,back=back,fits=fits,$
                      _extra=extra
on_error,1
err=''
count=0
files=''
if not exist(back) then back=0
quiet=keyword_set(quiet)
clobber=keyword_set(clobber)
loud=1-quiet
server='ftp.kis.uni-freiburg.de'

;-- check write access

if datatype(outdir) ne 'STR' then put_dir=curdir() else put_dir=outdir
if not test_dir(put_dir,quiet=quiet,err=err) then return,files

;-- default to current date

cerr=''
hdate=anytim2utc(date,err=cerr)
if cerr ne '' then get_utc,hdate

;-- construct filenames to copy

if keyword_set(fits) then begin
 ext='.fts'
 dext='_fits' 
endif else begin
 dext='_jpeg'
 ext='.jpg'
endelse

for i=0,back do begin
 tdate=hdate
 tdate.mjd=tdate.mjd-i
 hcode=date_code(tdate)
 year=strmid(hcode,0,4)
 month=strmid(hcode,4,2)
 day=strmid(hcode,6,2)
 get_dir='halpha/all'+dext+'/'+year+dext+'/'+month
 get_files='kisf_halph_fd_'+hcode+'_*'+ext

 dprint,'% get_dir, get_files: ',get_dir,'/',get_files

 smart_ftp,server,get_files,get_dir,files=files,count=count,$
          quiet=quiet,err=err,outdir='/tmp',_extra=extra,/anon

 if err eq '' then begin

  if loud then begin
   if count eq 0 then message,'No files found for '+anytim2utc(hdate,/date,/vms),/cont
  endif

;-- move files to desired directory

  if count gt 0 then begin
   for k=0,count-1 do begin
    break_file,files(k),fdsk,fdir,fname,fext
    new_file=concat_dir(put_dir,fname+fext)
    chk=loc_file(new_file,count=fcount)
    if (fcount eq 0) or clobber then spawn,'mv -f '+files(k)+' '+new_file
    spawn,'rm -f '+files(k)
    files(k)=new_file 
   endfor
   if exist(tfiles) then tfiles=[tfiles,files] else tfiles=files
  endif
 endif
endfor

count=n_elements(tfiles)
if count gt 0 then files=tfiles
if count eq 1 then files=files(0)

return,files & end

