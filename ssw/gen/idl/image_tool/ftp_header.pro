;+
; Project     : SOHO-CDS
;
; Name        : FTP_HEADER
;
; Purpose     : standard FTP header to include in FTP access routines
;
; Category    : planning
;
; Explanation : 
;
; Syntax      : None
;
; Examples    :
;
; Inputs      : None
;
; Opt. Inputs : None
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 14 Jan 1998 D. Zarro, SM&A/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-


on_error,1
err=''
count=0
delvarx,files
if not exist(back) then back=0
whole=keyword_set(month_all)
if whole then back=0
quiet=keyword_set(quiet)
loud=1-quiet

;-- check write access

if not test_dir(out_dir,quiet=quiet,err=err) then return

;-- default to current date

cerr=''
hdate=anytim2utc(date,err=cerr)
if cerr ne '' then get_utc,hdate

;-- check if end date was entered

derr=''
edate=anytim2utc(edate,err=derr)
if derr eq '' then begin
 back=edate.mjd-hdate.mjd+back
 hdate=edate
endif

if loud then message,'retrieving images for '+anytim2utc(hdate,/vms,/date),/cont

