function idl_server_control, pid=pid, running=running, kill=kill, $
     restart=restart, start_command=start_command

kill = keyword_set(kill) or keyword_set(restart)
restart = keyword_set(restart) or keyword_set(start_command)

; ----------------- find the idl/www server job info ----------
pid=-1

case 1 of 
   is_member(!version.os,'OSF',/ignore_case): begin 
      spawn,['ps','auxwww'],psout
      sserv=where( (strpos(psout,'ssw_idl_server') ne -1) or $
                   (strpos(psout,'idlrpc') ne -1)         or $
                   (strpos(psout,'CAF') ne -1),     sscnt)
  
      if sscnt gt 0 then begin 
         cols=str2cols(psout(sserv),/un,/trim)
         strtab2vect,cols,pid
         user=replicate(get_user(),n_elements(pid))
      endif

   endcase
   is_member(!version.os,'IRIX',/ignore_case): begin 
      spawn,['ps','-u',get_user()],/noshell,psout
      sserv=where(strpos(psout,'idlrpc') ne -1, sscnt)
      if sscnt gt 0 then begin 
         cols=str2cols(psout(sserv),/un,/trim)
         strtab2vect,cols,pid
         user=replicate(get_user(),n_elements(pid))
      endif
   endcase
   else: begin
      box_message,'This OS/Arch not yet handled...'
      return,-1
   endcase   
endcase   

; --------------------------------------------------------
status=pid(0) ne -1

; -------------- handle killing ---------------------------
if kill then begin 
   if pid(0) eq -1 then box_message,'No idl/www server running' else begin 
      for i=0,n_elements(pid)-1 do begin 
         box_message,['Killing PID ' + pid(i), psout(sserv(i))]
         spawn,['kill','-9',pid(i)],/noshell         
      endfor
   endelse
endif

; -----------------------------------------------------------

; -------- handle start/restart ----------------------------
if not data_chk(start_command,/string) then start_command=''
defstart='start_idlserver_'+strtrim(fix(!version.release),2)
sitestart=concat_dir('$SSW_SITE_SETUP',defstart)
genstart=concat_dir('$SSW_BIN',defstart)
perstart=sitestart+'_'+get_host(/short)

sorder=[start_command,perstart,sitestart,genstart] 
sexist=(where(file_exist(sorder),ecnt))(0)         ; first existing

if restart then begin 
   if ecnt eq 0 then begin
       box_message,'No valid start command' 
       status=0
   endif else begin 
      espawn,'csh -f ' + sorder(sexist) 
   endelse 
endif

return, status
end



