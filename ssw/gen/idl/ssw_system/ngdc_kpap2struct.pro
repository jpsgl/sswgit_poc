function ngdc_kpap2struct, ngdcdata, file=file, summary=summary
;+
;   Name: ngdc_kpap2struct
;
;   Purpose: convert NGDC KP_AP indices files->structure
;
;   History:
;      12-March-2000 - S.L.Freeland
;       4-April-2000 - S.L.Freeland - keep 8 samp/day Ap and Kp
;
;   Method: use 'table2struct'
;-

common ngdc_kpap2struct_blk, strtemp, sumstrtemp

case 1 of
   data_chk(ngdcdata,/string):
   data_chk(file,/string): if file_exist(file(0)) then ngdcdata=rd_tfile(file) else begin
         box_message,'Cannot find file: ' + file(0)
	 return,-1
      endelse
   else: begin
	box_message,['Must supply NGDC ascii file array or file name...',$
		     'IDL>structs=ngdc_kpap2struct( [ngdcdata] [file=file] )' ]
        return,-1
   endcase
endcase

format='(a6,i4,i2,' + $
       'i2,i2,i2,i2,i2,i2,i2,i2,i3,'    + $
       'i3,i3,i3,i3,i3,i3,i3,i3,i3,' + $
       'f3.1,i1,i3,f5.1,i1)'

names= 'yymmdd,bartels,bartels_nday,' + $
       'kp00,kp03,kp06,kp09,kp12,kp15,kp18,kp21,kpsum,'  + $
       'ap00,ap03,ap06,ap09,ap12,ap15,ap18,ap21,apmean,' + $
       'cp,cpconv,sunspot,f10_7,flux_qual'

retval=table2struct(ngdcdata, names, format=format )
retval.yymmdd=str_replace(retval.yymmdd,' ','0')
year=fix(strmid(retval.yymmdd,0,2))
retval.yymmdd=(['19','20'])(year lt 31)+ retval.yymmdd ; WARNING 2031 
                                                       ;     CATASTROPHE
if keyword_set(summary) then begin 
   times=file2time(retval.yymmdd,out='utc_int')
   times=join_struct(times,$
      replicate({Ap:lonarr(8),Kp:lonarr(8)},n_elements(times)))
   for i=0,7 do begin 
      estat=execute('times.ap(i)=retval.AP' + string(i*3,format="(i2.2)"))
      estat=execute('times.kp(i)=retval.KP' + string(i*3,format="(i2.2)"))
   endfor
   newstr=str_subset(retval,'bartels,bartels_nday,kpsum,apmean,cp,cpconv,sunspot,f10_7,flux_qual')
   retval=join_struct(times,newstr)
endif

return,retval
end
