;+
; Project     : HESSI
;                  
; Name        : SSW_LOAD_INST
;               
; Purpose     : Platform/OS independent SSW startup.
;               Executes IDL startups and loads environment variables
;               for instruments and packages in $SSW_INSTR
;                             
; Category    : utility
;               
; Syntax      : IDL> ssw_load_inst
;
; Inputs      : None
; 
; Outputs     : None
;
; Keywords    : VERBOSE - set for verbose output
;               ERR - error string
;               ENV_ONLY = load environment only
;                                   
; History     : 30-April-2017, written Zarro (ADNET)
;
; Contact     : dzarro@solar.stanford.edu
;-    

pro ssw_load_inst,verbose=verbose,err=err,env_only=env_only

do_startup=~keyword_set(env_only)
verbose=keyword_set(verbose)
err=''

;main_execute,local_name('$SSW/gen/setup/IDL_STARTUP')
file_setenv,local_name('$SSW/gen/setup/setup.ssw_env')

ssw_instr=getenv('SSW_INSTR')
if is_blank(ssw_instr) then begin
 err='SSW_INSTR undefined. No instruments loaded.'
 mprint,err
 return
endif
inst=str2arr(ssw_instr,delim=' ')

;-- find and read latest SSW map file

map_file=local_name('$SSW/gen/setup/ssw_map.dat')
chk=file_test(map_file)
if ~chk then begin
 err='Non-standard SSW installation.'
 mprint,err
 return
endif
ssw_map=rd_tfile(map_file)

;-- cycle through each INST and look for first instance of
;   inst/idl

for i=0,n_elements(inst)-1 do begin

 ival=inst[i]

;-- handle special cases

 if ival eq 'rhessi' then ival='hessi'
 if stregex(ival,'(euvi|cor)',/bool,/fold) then ival='secchi'
 item='/'+ival+'/idl'
 chk=stregex(ssw_map,item,/bool,/fold)
 found=where(chk,count)
 if count gt 0 then begin
  path=ssw_map[found[0]]
  pos=strpos(path,item)
  base=strmid(path,0,pos)
  top=base+item

;-- add instrument path

  root=local_name(str_replace(top,'/idl',''))
  setenv,'SSW_'+strupcase(ival)+'='+root
  add_cmd='ssw_path,/'+ival+',quiet=~verbose'
  status=execute(add_cmd,1,1)
  setup=local_name(str_replace(top,'/idl','/setup'))

  if file_test(setup,/dir) then begin

;-- execute instrument IDL_STARTUP
   
   if do_startup then begin
    idl_startup=concat_dir(setup,'IDL_STARTUP')
    if file_test(idl_startup) then begin
     if verbose then mprint,'Executing IDL_STARTUP for - '+ival
     main_execute,idl_startup
    endif
   endif

;-- load instrument environment variables
 
   idl_env='setup.'+ival+'_env'
   idl_gen_env=concat_dir(setup,idl_env)
   if file_test(idl_gen_env) then begin
    if verbose then mprint,'Loading GEN environment for - '+ival
    file_setenv,idl_gen_env
   endif
   idl_site_env=local_name(concat_dir('$SSW/site/setup',idl_env))
   if file_test(idl_site_env) then begin
    if verbose then mprint,'Loading SITE environment for - '+ival
    file_setenv,idl_site_env
   endif
  endif
 endif
endfor

return
end
