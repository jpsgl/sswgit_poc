;+
; Project     : VSO
;
; Name        : RD_CHUNKED
;
; Purpose     : read chunked-encoded data 
;
; Category    : utility system sockets
;
; Syntax      : IDL> out=rd_chunked(lun)
;
; Inputs      : LUN = socket unit number
;
; Output      : OUTPUT = byte array
;
; History     : Written, 27-Dec-2012, Zarro (ADNET)
;
; Contact     : dzarro@solar.stanford.edu
;-

function rd_chunked,lun,err=err,counts=counts

err='' & counts=0l
if ~is_number(lun) then return,''
if ~(fstat(lun)).open then return,''
on_ioerror,done

repeat begin
 chunk=''
 readf,lun,chunk
 if is_string(chunk) then begin
  chunk=strtrim(chunk,2)
  chk=strpos(chunk,';')
  if chk gt -1 then chunk=strmid(chunk,0,chk)
  bsize=hex2decf(chunk)
  if bsize gt 0 then begin
   b=bytarr(bsize,/nozero)
   readu,lun,b
   if exist(temp) then temp=[temporary(temp),temporary(b)] else temp=temporary(b)
  endif
 endif
endrep until eof(lun)

counts=n_elements(temp)
return,temp

done:on_ioerror,null

if ~exist(temp) then temp=''
err='I/O error during read.'
message,err,/info
message,/reset

return,temp

end
