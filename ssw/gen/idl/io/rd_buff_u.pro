;+
; Project     : HESSI
;
; Name        : RD_BUFF_U
;
; Purpose     : read unformatted data buffer
;
; Category    : utility system
;
; Syntax      : IDL> out=rd_buff_u(lun,maxsize,buffsize)
;
; Inputs      : LUN = logical unit number
;               MAXSIZE = max size of file in bytes
;
; Opt. Inputs : CHUNK = chunk factor to break buffer into [def=10]
;
; History     : Written, 3 April 2002, D. Zarro (L-3Com/GSFC)
;
; Contact     : dzarro@solar.stanford.edu
;-

function rd_buff_u,lun,maxsize,chunk,err=err

err=''
if not is_number(lun) then return,0b
if not is_number(maxsize) then return,0b

;-- read in chunks until EOF

if is_number(chunk) then fac=float(abs(chunk)) > 1. else fac=10.
buffsize=long( maxsize/10.)
if buffsize lt 1 then buffsize=maxsize

on_ioerror,done
clean_break=0b

repeat begin
 b=bytarr(buffsize,/nozero)
 readu,lun,b,transfer=count
 if exist(temp) then temp=[temporary(temp),b] else temp=b
endrep until eof(lun)
clean_break=1b

done:

on_ioerror,null
if not clean_break and exist(temp) then temp=[temporary(temp),temporary(b)]
if not exist(temp) then temp=temporary(b)
delvarx,b

cmax=n_elements(temp)
if cmax lt maxsize then begin
 err='Problem with buffered read.'
 message,err,/cont
 return,0b
endif

return,temporary(temp[0l:maxsize-1])

end
