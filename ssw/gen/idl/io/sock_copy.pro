;+
; Project     : HESSI
;
; Name        : SOCK_COPY
;
; Purpose     : copy file via HTTP sockets
;
; Category    : utility system sockets
;
; Syntax      : IDL> sock_copy,url_file,outdir=outdir
;                   
; Inputs      : URL_FILE = remote file name to copy with URL path
;
; Outputs     : None
;
; Keywords    : OUT_DIR = output directory to copy file
;               ERR   = string error message
;               NO_CLOBBER = do not clobber existing file
;
; Example     : 
;
; IDL> f='smmdac.nascom.nasa.gov/synop_data/kanz/kanz_halph_fd_20000114_1016.fts
; IDL> sock_copy,f
;
; History     : 27-Dec-2001,  D.M. Zarro (EITI/GSFC)  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro sock_copy,url_file,err=err,_extra=extra

err=''
if is_blank(url_file) then begin
 err='blank URL filename entered'
 message,err,/cont
 return
endif

http=obj_new('http',err=err)
if err ne '' then return

http->copy,url,_extra=extra,err=err

obj_destroy,http

return
end


