;+
; Project     : HESSI
;
; Name        : SOCKFILE
;
; Purpose     : socket version of FINDFILE
;
; Category    : string utility system sockets
;                   
; Inputs      : FILES = remote file name (with full path) to search
;               SERVER = remote server name to search
;
; Example     : IDL> server='smmdac.nascom.nasa.gov'
;               IDL> a= rloc_file('/sdb/yohkoh/ys_dbase/g81/g*3*',server)
;
; Outputs     : Match results
;
; Keywords    : COUNT = # of matches
;               ERR   = string error message
;
; History     : 27-Dec-2001,  D.M. Zarro (EITI/GSFC)  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function sockfile,files,server,count=count,err=err

common rloc_file,saved

err=''
count=0
if is_blank(files) then return,''

http=obj_new('http',server,err=err)
if err ne '' then return,''

break_file,files,dsk,dir,name,ext
sdir=trim(dsk+dir)
sname=trim(name+ext)

;-- check if this directory already searched

hrefs=''
if exist(saved) then begin
 chk=where(sdir eq saved.sdir,scount)
 if scount gt 0 then hrefs=*((saved.hrefs)[chk[0]])
endif


if is_blank(hrefs) then begin
 http->links,sdir,hrefs,err=err
 if err eq '' then begin
  temp={rloc_file,sdir:sdir,hrefs:ptr_new(/all)}
  *(temp.hrefs)=hrefs
  if exist(saved) then saved=[temporary(saved),temp] else saved=temp
 endif
endif

out=''
if is_string(hrefs) then begin
 out=str_find(hrefs,sname,count=count)
 if count gt 0 then out=concat_dir(sdir,out)
endif

obj_destroy,http
return,out

end


