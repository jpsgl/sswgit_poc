
;-- plot latest search results in PNG file 

pro plot_png,fid,_extra=extra

;-- return existing png file if present, else create new one

if is_blank(fid) then return
cache_gbo,fid,records,/get

png='/tmp/gbo'+trim(fid)+'.png'

chk=loc_file(png,count=count)
if count ne 0 then return

;-- check for latest search results in cache

plot_gbo,records,png=png,_extra=extra

return
end

;----------------------------------------------------------------------------------

pro plot_gbo,ids,tmin,tmax,_extra=extra,png=png,gsize=gsize

common gbo_com,gbo_records

np=n_elements(ids)
if np eq 0 then return
if is_struct(ids) then records=ids else begin
 if ids[0] eq -1 then return
 records=gbo_records[ids]
endelse

;-- establish time range

temp={mjd:0l,time:0l}
tstart=replicate(temp,np)
tend=tstart
tstart.mjd=records.dstart
tstart.time=records.tstart
tend.mjd=records.dend
tend.time=records.tend

tstart=anytim(tstart)
tend=anytim(tend)

if valid_time(tmin) and valid_time(tmax) then begin
 dmin=anytim(tmin)
 dmax=anytim(tmax)
endif else begin
 dmin=min(tstart)
 dmax=max(tend)
endelse


dmin=anytim(dmin,/utc_int)
dmin.time=0
dmin=anytim(dmin)
tmin=dmin
utbase=anytim(tmin,/vms)
message,'UTBASE: '+utbase,/cont

tstart=tstart-tmin
tend=tend-tmin
dmax=dmax-tmin
dmin=dmin-tmin

;-- establish observatories

obs=records.observatory
uobs=get_uniq(obs,count=count)
nticks=count+1
yrange=[0,nticks]
ytick=replicate(' ',nticks+1)

;-- establish Z-buffer

do_png=0b
if is_string(png) then begin
 break_file,png,dsk,dir
 do_png=test_dir(dsk+dir)
 if do_png then begin
  dsave=!d.name & psave=!p.color
  zsize=[800,400]
  if exist(gsize) then zsize=[gsize[0],gsize[n_elements(gsize)-1]]
  ncolors=!d.table_size
  set_plot,'z',/copy
  device,/close,set_resolution=zsize,set_colors=ncolors
  !p.color=ncolors-1
 endif
endif

;-- establish plot window


utplot,[dmin,dmax],[1,1],utbase,yrange=yrange,/ystyle,ytickname=ytick,$
                         yticks=nticks,/nodata,/xstyle,$
                         title='Max Millennium Search Results'

mtime=600.
width=!x.crange[1]-!x.crange[0]
mtime=width*.01
set_line_color
for i=0,count-1 do begin
 message,'plotting '+uobs[i],/cont
 chk=where(uobs[i] eq obs,cobs)
 cstart=tstart[chk]
 cend=tend[chk]

 diff=cend-cstart
 zero=where(diff le 0,zcount)
 if zcount gt 0 then cend[zero]=cstart[zero]+mtime
 ctime=(transpose( [[cstart],[cend]]))[*]
 black=replicate(0,cobs)
 color=replicate(i+1,cobs)
 ycol=(transpose( [[black],[color]]))[*]
 yval=replicate(i+1,2*cobs)

 xyouts,[!x.crange[0],!x.crange[0]]+.05*width,[i+1.15,i+1.15],uobs[i],/data,$
        charthick=1,font=0,_extra=extra

 plots,ctime,yval,color=ycol,_extra=extra,noclip=0,thick=6

endfor

if do_png then begin
 temp=tvrd()
 device,/close
 tvlct,rs,gs,bs,/get
 if idl_release(upper=5.3,/inc) then $
  write_png,png,rotate(temp,7),rs,gs,bs else $
   write_png,png,temp,rs,gs,bs
 espawn,'chmod g+w '+png
 set_plot,dsave
 !p.color=psave
endif

return & end


