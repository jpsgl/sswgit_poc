;+
; Project     : HESSI
;
; Name        : SHOW_SYNOP
;
; Purpose     : widget interface to Synoptic data archive
;
; Category    : HESSI, Synoptic, Database, widgets, objects
;
; Syntax      : IDL> show_synop
;
; Keywords    : GROUP = widget ID of any calling widget
;               PTR = pointer to last displayed object
;               NO_PLOT = set to not allow plotting
;
; History     : 12-May-2000,  D.M. Zarro (SM&A/GSFC)  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-


 pro show_synop_event,event          

;-- retrieve pointer reference from uvalue of child

 wmain=event.top
 child=widget_info(wmain,/child)
 widget_control,child, get_uvalue = ptr
 info=*ptr
 
 widget_control, event.id, get_uvalue=uvalue
 if not exist(uvalue) then uvalue=''
 bname=trim(uvalue(0))

;-- timer

 if bname eq 'timer' then begin
  get_utc, curr_ut, /ecs,/vms
  widget_control,wmain, $
            tlb_set_title ='SHOW SYNOP UT: '+strmid(curr_ut,0,20)
  widget_control,wmain,timer=1.
  widget_control,info.readb,sensitive=(info.cur_fsel ne '')
  if xalive(info.plotb) then widget_control,info.plotb,sensitive=(info.cur_fsel ne '')
  widget_control,info.downb,sensitive=(info.cur_sel(0) ne '')
  widget_control,info.remb,sensitive=(info.cur_fsel ne '')
  return
 endif

;-- quit here

 if bname eq 'exit' then begin
  synop=info.synop
  save,file=info.config_file,synop
  obj_destroy,synop
  if ptr_valid(info.obj_ptr) then begin
   if not exist(*info.obj_ptr) then ptr_free,info.obj_ptr
  endif
  ptr_free,ptr
  xkill,event.top
  return
 endif

;-- check time inputs and search pattern

 if bname eq 'search' then begin 
  good=xvalidate(info,event,time_change=time_change,/diff,/round,/vms,/hour) 
  if not good then return
  wstart=info.wstart & wend=info.wend
  tstart=info.tstart & tend=info.tend

  widget_control,info.pattb,get_value=new_patt
  info.synop->set,type=trim(new_patt)   

  if time_change then *ptr=info
  show_synop_slist,info
  return
 endif

;-- check class
        
 chk=where(bname eq info.synop->classes(),count)
 if count gt 0 then info.synop->set,class=bname

;-- configure

 if bname eq 'config' then begin
  old_dir=info.synop->getprop(/ldir)
  info.synop->config,group=event.top,/no_class
  new_dir=info.synop->getprop(/ldir)
  if (old_dir ne new_dir) then begin
   show_synop_flist,info
   *ptr=info
  endif
 endif
  
;-- reload cache 

 if bname eq 'reload' then show_synop_slist,info,/reload

;-- download selected file 

 if (event.id eq info.slist) then if (event.clicks eq 2) then bname='download' 
 if bname eq 'download' then begin  
  if trim(info.cur_sel(0)) eq '' then return     
  xtext,'Please wait. Downloading...',wbase=tbase,/just_reg
  widget_control,/hour
  lfile=info.synop->get_synop(info.cur_sel,err=err)
  xkill,tbase
  if err ne '' then begin
   xack,err
   return
  endif
  show_synop_flist,info
  *ptr=info
 endif

;-- top list selection event

 if event.id eq info.slist then begin
  new_sel=widget_selected(info.slist)
  info=rep_tag_value(info,new_sel,'cur_sel')
  *ptr=info

;-- highlight first file in download list

  info.synop->break,new_sel(0),sdir,sname
  widget_control,info.flist,get_uvalue=files
  info.synop->break,files,cdir,cname
  sel=where(sname eq cname,scount)
  if scount gt 0 then begin
   widget_control,info.flist,set_list_select=sel(0)
   info.cur_fsel=files(sel(0))
   *ptr=info
  endif
 endif 

;-- bottom list selection event

 if event.id eq info.flist then begin
  last_fsel=info.cur_fsel
  new_fsel=uvalue(event.index)
  if new_fsel ne last_fsel then begin
   info.cur_fsel=new_fsel
   *ptr=info
  endif
 endif 

;-- delete from download list

 if bname eq 'delete' then begin  
  if trim(info.cur_fsel) eq '' then return
  info.synop->break,info.cur_fsel,fdir,fname
  chk=xanswer('Delete '+fname+' from local directory?',$
              message_supp='Do not request confirmation for future deletes',$
              /suppre,/check,instruct='Delete ? ',space=1)
  if chk then begin 
   rm_file,info.cur_fsel
   info.cur_fsel=''
   show_synop_flist,info
   *ptr=info
  endif
 endif

;-- plot downloaded file

 if (event.id eq info.flist) then if (event.clicks eq 2) then bname='read'

 if (bname eq 'plot') or (bname eq 'read') then begin
  file_id=trim(info.cur_fsel)
  if (file_id eq '') then return
  
;-- check if this selection already cached

  info.last_fsel=file_id
  *ptr=info
  info.fifo->get,file_id,data
  if obj_valid(data) then begin
   data->get,time=time
   err=''
   chk=anytim2utc(time,err=err)
   status=err eq ''
  endif else status=0b

;-- if not, read it
                  
  if not status then begin
   xtext,'Please wait. Reading file...',wbase=tbase,/just_reg
   widget_control,/hour
   info.synop->read,file_id,data,err=err
   xkill,tbase
   if err ne '' then begin
    xack,err
    return
   endif
   info.fifo->set,file_id,data
  endif else begin
   if (bname eq 'read') then xtext,'File already read into memory',wbase=tbase,/just_reg,wait=1
  endelse
  
;-- send event to calling widget

  if datatype(info.widget_event) eq 'STC' then begin
   if tag_exist(info.widget_event, 'id') then begin
    if xalive(info.widget_event.id) then begin
     widget_control,info.widget_event.id,send_event=info.widget_event
    endif
   endif
  endif

;-- pass data back to caller via OBJ_PTR

  obj_ptr=info.obj_ptr
  ptr_alloc,obj_ptr
  *obj_ptr=data
  info.obj_ptr=obj_ptr                       
                                                                  
;-- plot it

  if bname eq 'plot' then begin
   widget_control,/hour
   data->plot,/sq 
  endif


 endif
                          
 return & end

;--------------------------------------------------------------------------
;-- list currently downloaded files

pro show_synop_flist,info

ldir=info.synop->getprop(/ldir)
files=loc_file('*.*',path=ldir,count=count)
widget_control,info.flist,set_uvalue='',set_value=''
if count gt 0 then times=info.synop->get_time(files,count=count)
if count gt 0 then begin
 info.synop->break,files,cdir,cnames
 sorder=uniq([cnames],sort([cnames]))
 files=files(sorder) & cnames=cnames(sorder) 
 widget_control,info.flist,set_uvalue=files,set_value=files
 info.synop->break,info.cur_sel(0),sdir,sname
 sel=where(sname eq cnames,scount)
 if scount gt 0 then begin
  widget_control,info.flist,set_list_select=sel(0)
  info.cur_fsel=files(sel(0))
 endif
endif

return & end

;--------------------------------------------------------------------------- 
;-- list contents of synoptic data files

pro show_synop_slist,info,reload=reload
                     
;-- initialize

synop=info.synop
class=synop->getprop(/class)
slist=info.slist
no_files='No Synoptic files of type "'+class+'" found in time range'
type=synop->getprop(/type)
if type ne '' then no_files=no_files+', with "'+type+'" in filename.'

;-- start listing

xtext,'Please wait. Searching Synoptic archive...',wbase=tbase,/just_reg
widget_control,/hour
                
old_cache=synop->getprop(/cache)
if keyword_set(reload) then synop->set,cache=0
synop->set,tstart=info.tstart,tend=info.tend
synop->list,files,count=count
synop->set,cache=old_cache

xkill,tbase                     
if count eq 0 then begin
 widget_control,slist,set_value=no_files
 widget_control,slist,set_uvalue='-1'
 return
endif

;-- format output 

synop->break,files,fdir,fname

synop->get_class_type,fname,class,type

ftimes=fid2time(fname,/no_sec)

fdata=str_cut(fname,35,pad=2)+ftimes+'   '+class
                 
widget_control,slist,set_value=fdata

widget_control,slist,set_uvalue=files
chk=where(info.cur_sel(0) eq files,count)
if count gt 0 then widget_control,slist,set_list_select=chk(0)

return & end
                                             
;--------------------------------------------------------------------------- 

pro show_synop_cleanup,id

return
child=widget_info(id,/child)
widget_control,child, get_uvalue = ptr
if ptr_valid(ptr) then ptr_free,ptr

return & end

;----------------------------------------------------------------------------
pro show_synop,group=group,modal=modal,reset=reset,ptr=obj_ptr,$
              no_plot=no_plot,widget_event=widget_event

common show_synop_cache,fifo

;-- defaults

select_windows
if not have_widgets() then begin
 message,'widgets unavailable',/cont
 return
endif
     
if keyword_set(reset) then xkill,'show_synop'

;if (xregistered('show_synop') ne 0) then return
                      
;-- reconcile times

secs_per_day=24l*3600l
week=7l*secs_per_day

get_utc,tstart & tstart.time=0
tstart=(utc2tai(tstart)-week) > utc2tai(anytim2utc('2-dec-95')) 
get_utc,tend & tend.time=0
tend.mjd=tend.mjd+1
tend=utc2tai(tend)

;-- default time window

tstart=round_time(tstart,/hour)
tend=round_time(tend,/hour)

;-- restore settings from last saved SYNOP object
;   (catch any errors in case saved SYNOP is outdated)

config_file=concat_dir(getenv('HOME'),'.show_synop_config')

error=0
catch,error
if error ne 0 then reset=1

if keyword_set(reset) then begin
 rm_file,config_file
 if obj_valid(fifo) then obj_destroy,fifo
endif

chk=loc_file(config_file,count=count)
if count gt 0 then begin
 temp={synop}
 restore,file=chk(0),/relaxed,restored=synop
endif

catch,/cancel

;-- compile methods

if (obj_valid(synop))[0] then begin
 synop=synop[0]
 obj_compile,synop
endif

;-- create FIFO object for caching

if not obj_valid(fifo) then fifo=obj_new('fifo')

;-- override with last time settings

if obj_valid(synop) then begin
 message,'restored previous settings',/cont
 tstart=synop->getprop(/tstart)
 tend=synop->getprop(/tend)
endif else begin 
 synop=obj_new('synop')
 synop->set,copy=1
endelse

relist=synop->getprop(/last_count) gt 0

;-- load fonts

mk_dfont,lfont=lfont,bfont=bfont

modal=keyword_set(modal)                            
wmain = widget_mbase(title = 'SHOW_SYNOP',uvalue='timer',group=group,$
                   modal=modal,/column)
child=widget_base(wmain,map=0)

;-- operation buttons

row1=widget_base(wmain,/row,/frame)
exitb=widget_button(row1,value='Done',uvalue='exit',font=bfont)
relistb=widget_button(row1,value='Relist',uvalue='reload',font=bfont)
conb=widget_button(row1,value='Configure',$
                    font=bfont,uvalue='config')  

;-- date/time fields

row2=widget_base(wmain,/column,/frame)

temp=widget_base(row2,/column)

tlabel=widget_label(temp,font=lfont,$
    value='Time range (UT) to search (units: DAY-MON-YR HH:MM)')

trow=widget_base(row2,/row)
wstart=cw_field(trow, Title= 'Start Time:  ',value=' ',$
                     xsize = 15,font=lfont)

wend=cw_field(trow,  Title='Stop Time:   ', value=' ',$
                    xsize = 15,font=lfont)

;-- choice of data class 

choices=synop->classes()
crow=widget_base(row2,/row)
xmenu2,choices,crow,/row,/exclusive,font=lfont,/no_rel,$
      buttons=cbuttons,uvalue=choices,$
      title='Data types to search: ',lfont=lfont 
             
;-- pattern to search

srow=widget_base(row2,/row)
pattb=cw_field(srow,  Title='String characters in filename to search (e.g. mwso): ', value=' ',$
                     xsize = 15,font=lfont)

;-- search button

temp=widget_base(row2,/row)
slabel1=widget_label(temp,font=lfont,value='Press: ')
searchb=widget_button(temp,value='Search',uvalue='search',font=bfont)
slabel2=widget_label(temp,font=lfont,value=' to find available Synoptic files')
                                               
;-- Synoptic files list
           
row3=widget_base(wmain,/column,/frame)
blabel=widget_label(row3,value='Synoptic Data Archive Search Results',font=lfont)
brow=widget_base(row3,/row)
dlabel1=widget_label(brow,font=lfont,value='Press: ')
downb=widget_button(brow,value='Download',font=bfont,uvalue='download')
dlabel2=widget_label(brow,font=lfont,value=' to copy selected file(s)')

                    
slabel='FILENAME                       DATE_OBS         TYPE      '
label=widget_list(row3,value=slabel,ysize=1,/frame,xsize=70,font=lfont)
slist=widget_list(row3,value='   ',ysize=10,font='fixed',/multiple)

;-- selected files list

row4=widget_base(wmain,/column,/frame)
label=widget_label(row4,value='Currently Downloaded Local Files',font=lfont)
brow=widget_base(row4,/row)
readb=widget_button(brow,value='Read',$
                    font=bfont,uvalue='read')

if keyword_set(no_plot) then plotb=0 else begin
 plotb=widget_button(brow,value='Plot',$
                    font=bfont,uvalue='plot')
endelse

remb=widget_button(brow,value='Delete',font=bfont,uvalue='delete')

flist=widget_list(row4,value=' ',ysize=10,xsize=70,font='fixed')

;-- realize main base

widget_control,wmain,group=group,/realize

;-- set defaults
                     
widget_control,wstart,set_value=round_time(tstart,/hour,/vms)
widget_control,wend,set_value=round_time(tend,/hour,/vms)
class=synop->getprop(/class)               
val=where(class eq strlowcase(choices),count)
if count gt 0 then widget_control,cbuttons(val(0)),/set_button    
curr_patt=synop->getprop(/type)
widget_control,pattb,set_value=curr_patt

widget_control,wmain,timer=1.

;-- ensure a valid pointer to pass back
                                                         
ptr_alloc,obj_ptr

;-- check if caller passed in a widget event structure

if datatype(widget_event) ne 'STC' then widget_event=0b

;-- create INFO structure                      
 
info={wstart:wstart,wend:wend,tstart:tstart,tend:tend,synop:synop,$
      obj_ptr:obj_ptr,readb:readb,pattb:pattb,$
      slist:slist,flist:flist,cur_sel:'',cur_fsel:'',last_fsel:'',$
      config_file:config_file,remb:remb,plotb:plotb,downb:downb,fifo:fifo,$
      widget_event:widget_event}
   
;-- produce initial listing

if relist then show_synop_slist,info
show_synop_flist,info

;-- use pointer variable for passing INFO to event handler
                                      
ptr=ptr_new(/all)
*ptr=info            
widget_control,child,set_uvalue =ptr
                                            
;-- start timer and XMANAGER

widget_control,wmain,timer=1.

xmanager,'show_synop',wmain,group=group,/no_block

;cleanup='show_synop_cleanup'
;xmanager_reset,wmain,modal=modal,group=group,crash='show_synop',/no_block

return & end

