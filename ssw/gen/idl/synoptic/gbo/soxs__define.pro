;+
; Project     : HESSI
;
; Name        : SOXS__DEFINE
;
; Purpose     : Define a SOXS data object
;
; Category    : Ancillary GBO Synoptic Objects
;
; Syntax      : IDL> c=obj_new('soxs')
;
; History     : Written 19 March 2009, Zarro (ADNET)
;               Modified 20 March 2009, Tolbert (Wyle)
;                - added OSPEX class inheritance
;
; Contact     : dzarro@solar.stanford.edu
;-
;-----------------------------------------------------------------------------

function soxs::init, _ref_extra=_extra

if ~self->spex::init(_extra=_extra, /no_gui) then return, 0b
return, 1b
end

;------------------------------------------------------------------------------

pro soxs::read, file,_ref_extra=_extra
if is_blank(file) then return
data = self->getdata(spex_specfile=file,_extra=_extra)
self.filename=file_break(file)
end

;------------------------------------------------------------------------------

pro soxs::plot, spectrum=spectrum, _ref_extra=_extra
if keyword_set(spectrum) then self -> plot_spectrum,/tband,/allint,_extra=_extra else self -> plot_time,_extra=_extra
end

;------------------------------------------------------------------------------
;-- wrapper around SOX GET

function soxs::get,_ref_extra=extra,filename=filename
if keyword_set(filename) then return,self.filename
return,self->spex::get(_extra=extra)
end

;------------------------------------------------------------------------------

pro soxs__define                 

self={soxs, filename:'',inherits spex}

return & end

