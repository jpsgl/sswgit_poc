
function sff_help

  info = 'Flare Finder is an interactive widget to search for solar flares observed by various combinations of space-based solar observatories. The code searches for all GOES events from 1-May-2010 onwards (i.e. since the launch of SDO) above the C1 level for which the RHESSI flag was active for more than 50% of the rise phase of the GOES event (GOES start->GOES end). It is assumed that AIA and HMI were observing continuously.'
  
  return, info

end
