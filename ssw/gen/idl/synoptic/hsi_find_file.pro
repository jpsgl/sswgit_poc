;+
; Project     : HESSI
;
; Name        : HSI_FIND_FILE
;
; Purpose     : Wrapper around HSI_LOC_FILE and HSI_SOCK_COPY
;
; Category    : utility system sockets
;
; Syntax      : IDL> file=hsi_find_file(file,outdir=outdir)
;
; Inputs      : FILE = filename to find and possibly copy
;
; Outputs     : FILE = filename with local path
;
; Keywords    : OUT_DIR = output directory to copy file
;               ERR   = string error message
;               NO_CLOBBER = set to not clobber existing file
;               VERBOSE = set for message output
;
; History     : 27-March-2002,  D.M. Zarro (L-3Com/GSFC) Written.
;-

function hsi_find_file,file,verbose=verbose,count=count,_ref_extra=extra

count=0
verbose=keyword_set(verbose)
if is_blank(file) then begin
 if verbose then message,'Please enter a valid file name to search for',/cont
 return,''
endif

;-- check local archives first

result=''
if have_proc('hsi_loc_file') then begin
 result=call_function('hsi_loc_file',file,/no_dialog,_extra=extra,$
                       count=count,verbose=verbose)
endif

;-- check remote archives next

if trim(chklog('SKIP_NETWORK_SEARCH')) eq '1' then return,result

if not allow_sockets(_extra=extra,/verbose) then return,result

ifile=get_uniq(file,count=nfiles)
break_file,local_name(ifile),dsk,dir,name,ext
ifile=name+ext

for i=0,nfiles-1 do begin
 if is_string(ifile[i]) then begin
  chk=stregex(result,ifile[i],/bool)
  found=where(chk gt 0,count)
  if count eq 0 then begin
   rfile=''
   if have_network(_extra=extra,/verbose) then begin
    rfile=hsi_sock_copy(ifile[i],_extra=extra,/verbose)
   endif
   if is_string(rfile) then sfile=append_arr(sfile,rfile)
  endif else sfile=append_arr(sfile,result[found])
 endif
endfor

count=n_elements(sfile)
if count eq 0 then sfile='' 
sfile=get_uniq(sfile,count=count)
if count eq 1 then sfile=sfile[0]

return,sfile

end
