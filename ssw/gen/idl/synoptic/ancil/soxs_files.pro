;+
; Project     : VSO
;
; Name        : SOXS_FILES
;
; Purpose     : Search SOXS files
;
; Category    : ancillary data
;
; Example     : IDL>  files=soxs_files('1-may-07','2-May-07')
;
; Inputs      : TSTART, TEND = start, end times to search
;
; Outputs     : FILE = file url's of search results
;
; Keywords    : TIMES = times (TAI) of returned files
;               SIZES = sizes (bytes) of returned files
;               COUNT = # of returned files
;
; History     : Written 28-Mar-2009, D.M. Zarro (ADNET/GSFC)
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;
;-

function soxs_files,tstart,tend,sizes=sizes,times=times,$
                   _extra=extra,count=count,verbose=verbose
sizes='' & times=-1d
count=0
verbose=keyword_set(verbose)

if ~valid_time(tstart) then begin
 pr_syntax,'files=soxs_files(tstart [,tend])'
 return,''
endif

urls='' & count=0 & nearest=0b
if valid_time(tstart) and ~valid_time(tend) then begin
 if is_number(window) then win=window/2. else win=24*3600.
 dstart=anytim2tai(tstart)-win
 dend=dstart+2*win
 dstart=anytim2utc(dstart,/external)
 dend=anytim2utc(dend,/external)
 nearest=1b
endif else dstart=get_def_times(tstart,tend,dend=dend,_extra=extra,/external)

;-- cycle thru relevant year directories

ystart=dstart.year
yend=dend.year
for i=ystart,yend do begin
 if verbose then message,'Searching '+trim(i),/cont
 files=sock_find('http://hesperia.gsfc.nasa.gov',path='soxs/'+trim(i),'*.les',count=count)
 if count gt 0 then dfiles=append_arr(dfiles,files,/no_copy)
endfor

serr='No SOXS files found.'
count=n_elements(dfiles)
if count eq 0 then begin
 message,serr,/cont
 return,''
endif

;-- find matching times

dtimes=anytim2tai(file_break(dfiles,/no_ext))

if nearest then begin
 diff=abs(dtimes-anytim2tai(tstart))
 ok=where(diff eq min(diff))
 times=dtimes[ok] & files=dfiles[ok]
 count=1
endif else begin
 dstart=anytim2utc(dstart) & dstart.time=0
 dend=anytim2utc(dend) & dend.time=0
 tmin=anytim2tai(dstart)
 tmax=anytim2tai(dend)
 keep=where( (dtimes le tmax) and (dtimes ge tmin), count)
 if count eq 0 then begin
  message,serr,/cont
  return,''
 endif
 files=dfiles[keep] & times=dtimes[keep]
endelse

if count gt 1 then begin
 ok=get_uniq(times,sorder,count=count)
 files=files[sorder] & times=times[sorder]
 sizes=strarr(count)
endif else begin
 files=files[0] & times=times[0]
endelse

return,files
end
