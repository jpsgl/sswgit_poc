;+
; Project     : EIS
;
; Name        : EIS_PATH
;
; Purpose     : install EIS IDL path
;
; Category    : system
;                   
; Inputs      : None
;
; Outputs     : None
;
; Keywords    : QUIET = set for no output
;
; History     : 10-Sept-2002,  D.M. Zarro (EITI/GSFC)  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-


pro eis_path,quiet=quiet

if have_proc('ecat') then return

emess='SolarB/EIS branch of SSW needs to be installed'
eis_dir=local_name('$SSW/solarb/eis/idl')
if is_dir(eis_dir,ename) then begin
 add_path,ename,/expand,/quiet,index=index
endif else begin
 message,emess,/cont
 return
endelse

if not have_proc('ecat') then begin
 message,emess,/cont
 return
endif

if (1-keyword_set(quiet)) then message,'SolarB/EIS branch installed',/cont
return
end
