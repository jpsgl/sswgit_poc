;+
; Project     : HESSI
;
; Name        : HSI_SOCK_FILE
;
; Purpose     : locate & copy HSI FITS file via HTTP sockets
;
; Category    : utility system sockets
;
; Syntax      : IDL> file=hsi_sock_copy(file,outdir=outdir,err=err,_extra=extra)
;                   
; Inputs      : FILE = filename to copy
;
; Outputs     : FILE = filename with local path 
;
; Keywords    : OUT_DIR = output directory to copy file
;               ERR   = string error message
;               NO_CLOBBER = set to not clobber existing file
;               VERBOSE = set for message output
;
; History     : 27-March-2002,  D.M. Zarro (L-3Com/GSFC)  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function hsi_sock_copy,file,err=err,out_dir=out_dir,_extra=extra,verbose=verbose

err=''

if is_blank(file) then begin
 err='non-string filename entered'
 message,err,/cont
 return,''
endif

if is_blank(out_dir) then out_dir='$HSI_DATA_USER' 
if not is_dir(out_dir) then out_dir=curdir()
if not test_dir(out_dir,err=err) then return,''
 
;-- first find nearest server

server=hsi_sock_server(err=err,path=path,verbose=verbose)
if err ne '' then return,''

;-- find directory location based on filename

break_file,file,dsk,dir,name,ext
dfile=name+ext
check=stregex(dfile,'_([0-9]{4})([0-9]{2})([0-9]{2})_',/extra,/sube)
sub_dir='/'+check[1]+'/'+check[2]+'/'+check[3]+'/'
url=server+path+sub_dir+dfile

sock_copy,url,err=err,out_dir=out_dir,_extra=extra,verbose=verbose
if err ne '' then return,''

return,concat_dir(out_dir,dfile)

end


