;+
; Project     : HESSI
;
; Name        : HSI_SOCK_SERVER
;
; Purpose     : return host name of nearest HESSI data server
;
; Category    : sockets
;           
; Outputs     : SERVER = server name
;
; Keywords    : PATH = path to data directories
;
; History     : Written, 22-Mar-2002,  D.M. Zarro (L-3Com/GSFC)
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-


function hsi_sock_server,path=path,err=err,verbose=verbose,reset=reset

common hsi_sock_server,server,last_path
err=''

if keyword_set(reset) then delvarx,server,last_path

if is_string(server) and is_string(last_path) then begin
 path=last_path
 return,server
endif

server='hesperia.gsfc.nasa.gov'
sock_ping,server,err=err

if err ne '' then begin
 if keyword_set(verbose) then message,err,/cont
 return,''
endif

path='/hessidata'
last_path=path

return,server

end
