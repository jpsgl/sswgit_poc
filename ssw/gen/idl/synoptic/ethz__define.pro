;+
; Project     : HESSI
;
; Name        : ETHZ__DEFINE
;
; Purpose     : Define a site object for Phoenix Radio Spectrogram data
;
; Category    : Ancillary GBO Synoptic Objects
;
; Syntax      : IDL> c=obj_new('ethz')
;
; History     : Written 18 Nov 2002, D. Zarro (EER/GSFC)
;
; Contact     : dzarro@solar.stanford.edu
;-
;-----------------------------------------------------------------------------
;-- init 

function ethz::init,_ref_extra=extra

if not self->specplot::init(_extra=extra) then return,0

;-- define SITE object for downloading

self.site=obj_new('site',_extra=extra)
if not obj_valid(self.site) then return,0

;-- define FITS object for reading

self.fits=obj_new('fits',/no_map,_extra=extra)
if not obj_valid(self.fits) then return,0

self.site->setprop,rhost='www.astro.phys.ethz.ch',ext='.fits',org='month',$
                 topdir='/pub/rag/phoenix-2/bursts/fits',/full,$
                 delim='/',ftype='phnx',smode=0

return,1
end

;--------------------------------------------------------------------------
;-- search ETHZ archive using FTP

function ethz::search,tstart,tend,_ref_extra=extra

return,self.site->flist(tstart,tend,_extra=extra)

end

;-----------------------------------------------------------------------

pro ethz::cleanup

self->specplot::cleanup
obj_destroy,self.site
obj_destroy,self.fits

return & end

;---------------------------------------------------------------------

pro ethz::read,file,data,header=header,index=index,err=err,nodata=nodata,$
                  _ref_extra=extra

self.fits->read,file,data,header=header,index=index,$
          extension=0,err=err,nodata=nodata,_extra=extra

if is_string(err) then return
if keyword_set(nodata) then return

self.fits->read,file,data1,index=index1,$
          extension=1,err=err,_extra=extra

if is_string(err) then return

self->set_fits,file,index,data,index1,data1,header=header

return & end

;------------------------------------------------------------------------

;-- store FITS info in object

pro ethz::set_fits,file,index,data,index1,data1,header=header

if not is_struct(index) then return

;time=data1.time*index1.tscal1+index1.tzero1

frequency=data1.frequency*index1.tscal2+index1.tzero2

time=index.crval1+index.cdelt1*dindgen(index.naxis1)
utbase=anytim2utc(index.time_d$obs+' '+index.date_d$obs,/vms)

freq=string(float(frequency),format='(i7.0)')+' MHz'
dunit='Flux (45*log10[SFU + 10]'

self->set,xdata=time,ydata=data,dim1_ids=freq,dim1_unit='Frequency (MHz)',$
             data_unit=dunit,id='Phoenix: '+utbase,/no_copy,$
             dim1_vals=float(frequency),utbase=utbase,/secs,$
             filename=file,dim1_use=indgen(4),/positive,header=header

return & end

;------------------------------------------------------------------------------
;-- self structure

pro ethz__define                 

self={ethz, site:obj_new(), fits:obj_new(), inherits specplot}

return & end

