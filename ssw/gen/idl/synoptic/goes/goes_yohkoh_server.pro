;+
; Project     : HESSI
;
; Name        : GOES_YOHKOH_SERVER
;
; Purpose     : return available Yohkoh GOES data server
;
; Category    : synoptic sockets
;
; Inputs      : None
;
; Outputs     : SERVER = Yohkoh GOES data server name
;
; Keywords    : NETWORK = returns 1 if network to that server is up
;               PATH = path to data
;
; History     : Written 15-Nov-2006, Zarro (ADNET/GSFC) 
;               Modified 22-Feb-2012, Zarro (ADNET)
;               - made /FULL the default
;               14-Dec-2012, Zarro (ADNET)
;               - removed redundant call to HAVE_NETWORK
;               - switched primary server to faster sohowww
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function goes_yohkoh_server,_ref_extra=extra, path=path,network=network

primary='sohowww.nascom.nasa.gov'
secondary='umbra.nascom.nasa.gov'

;secondary='moat.nascom.nasa.gov'
path='/sdb/yohkoh/ys_dbase'

;-- primary server

server=primary
network=have_network(server,_extra=extra)

 ;-- if primary server is down, try secondary

if ~network then begin
 server2=secondary
 message,'Trying '+server2+'...',/info
 network=have_network(server2,_extra=extra)
 if network then server=server2
endif

return,'http://'+server

end
