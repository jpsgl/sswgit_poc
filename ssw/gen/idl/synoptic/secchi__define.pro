;+
; Project     : STEREO
;
; Name        : SECCHI__DEFINE
;
; Purpose     : Define a SECCHI data/map object
;
; Category    : Objects
;
; Syntax      : IDL> a=obj_new('secchi')
;
; Examples    : IDL> a->read,'20070501_000400_n4euA.fts' ;-- read FITS file
;               IDL> a->plot                             ;-- plot image
;               IDL> map=a->getmap()                     ;-- access map
;               IDL> data=a->getdata()                   ;-- access data
;                       
;               Searching via VSO:                                     
;               IDL> files=a->search('1-may-07','02:00 1-may-07')
;               IDL> print,files[0]
;               http://stereo-ssc.nascom.nasa.gov/data/ins_data/secchi/L0/a/img/euvi/20070501/20070501_000400_n4euA.fts
;               
;               Copying from VSO:
;               IDL> sock_copy,files[0],/verbose
;
; History     : Written 13 May 2007, D. Zarro (ADNET)
;
; Contact     : dzarro@solar.stanford.edu
;-

;-----------------------------------------------------------------------------
;-- check for SECCHI branch in !path

function secchi::have_secchi_path,err=err

err=''
if not have_proc('sccreadfits') then begin
 epath=local_name('$SSW/stereo/secchi/idl')
 if is_dir(epath) then add_path,epath,/expand,/quiet,/append
 if not have_proc('read_secchi') then begin
  err='STEREO/SECCHI branch of SSW not installed'
  message,err,/cont
  return,0b
 endif
endif

return,1b

end

;--------------------------------------------------------------------------
;-- FITS reader

pro secchi::read,file,data,_ref_extra=extra,err=err,no_prep=no_prep

if is_blank(file) then begin
 err='No filenames entered'
 message,err,/cont
 return
endif

if ~self->have_secchi_path(err=err) then return

if keyword_set(no_prep) then $
 data=sccreadfits(file,index,_extra=extra) else $
  secchi_prep,file,index,data,_extra=extra

if ~is_struct(index) then begin
 err='Problems reading FITS file'
 return
endif

;-- No DATA returned

if size(data,/n_dim) lt 2 then return

;-- store DATA in MAP objects
 
ndata=n_elements(index)

have_rsun=have_tag(index,'rsun')
for i=0,ndata-1 do begin
 if have_rsun then extra=rep_tag_value(extra,index[i].rsun,'rsun')
 self->mk_map,index[i],data[*,*,i],i,filename=file[i],_extra=extra
 self->set,i,/log_scale,grid=0,limb=0
endfor

return & end

;-----------------------------------------------------------------------
;-- VSO search function

function secchi::search,tstart,tend,_ref_extra=extra

dstart=get_def_times(tstart,tend,dend=dend,_extra=extra,/vms)
records=vso_search(dstart,dend,_extra=extra,/url,inst='secchi')
if is_struct(records) then return,records.url else return,''

end

;------------------------------------------------------------------------
;-- SECCHI data structure

pro secchi__define,void                 

void={secchi, inherits map}

return & end
