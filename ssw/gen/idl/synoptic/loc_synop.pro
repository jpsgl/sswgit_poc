;+
; Project     : HESSI
;
; Name        : LOC_SYNOP
;
; Purpose     : Locate Synoptic data file 
;
; Category    : HESSI, Synoptic, utility
;
; Explanation : 
;
; Syntax      : IDL> result=loc_synop(file)
;
; Inputs      : FILE = file name (e.g. kpno_10830_fd_19990516_1523.fts)
;
; Opt. Inputs : None
;
; Outputs     : RESULT = found file (with full path name)
;
; Opt. Outputs: None
;
; Keywords    : ERR = error string
;               COUNT = # of files found

; History     : Version 1,  17-May-1999,  D.M. Zarro (SM&A/GSFC),  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function loc_synop,file,err=err,count=count,class=class

err=''
count=0

;-- input validation
      
synop_data=chklog('SYNOP_DATA')
if synop_data eq '' then begin
 err='SYNOP_DATA location env is not defined'
 return,''
endif

if not is_string(file,nfile) then begin
 err='at least one non-blank filename is required'
 message,err,/cont
 return,''
endif

;-- default to images directory

if not is_string(class) then class='images'
synop_dir=concat_dir(synop_data,class)

;-- determine subdirectory names

nfiles=n_elements(nfile)
break_file,nfile,dsk,dir,name,ext
fname=trim(name+ext)
for i=0,nfiles-1 do begin 
 time=fid2time(fname(i),ymd=ymd,count=fcount,err=ferr)
 if ferr ne '' then err=err+' '+ferr
 if fcount eq 1 then begin
  path=concat_dir(synop_dir,ymd)
  tfile=loc_file(fname(i),path=path,count=tcount)
  if tcount gt 0 then cfile=append_arr(cfile,tfile,/no_copy)
 endif
endfor

count=n_elements(cfile)
if count eq 1 then cfile=cfile(0)
if count eq 0 then begin 
 cfile=''
 err=trim(err)
 if err eq '' then err='no file(s) found'
 message,err,/cont
endif

return,cfile

end

