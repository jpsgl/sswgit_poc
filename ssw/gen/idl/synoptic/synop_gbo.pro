;+
; Project     : HESSI
;
; Name        : SYNOP_GBO
;
; Purpose     : update Max Millennium catalog with Synoptic data file info
;
; Category    : synoptic gbo
;
; Syntax      : IDL> synop_gbo
;
; Inputs      : TSTART,TEND = time range to process
;     
; Keywords    : REPROCESS = reprocess existing entries
;               BACK = # of days back to process
;               VERBOSE = set verbose output
;               TYPE = type of file process [e.g. 'mdi']
;             
; Restrictions: Unix only
;
; History     : Written 4 Dec 2001, D. Zarro (EITI/GSFC)
;
; Contact     : dzarro@solar.stanford.edu
;-

pro synop_gbo,tstart,tend,type=type,reprocess=reprocess,back=back,verbose=verbose

;-- usual error checks
 
if os_family(/lower) ne 'unix' then begin
 err='sorry, Unix only'
 message,err,/cont
 return
endif

if not is_dir('$SYNOP_DATA') then begin
 err='$SYNOP_DATA is undefined'
 message,err,/cont
 return
endif

;-- locate database file

db_file=loc_file('$GBO_DB',count=count,err=err,/verb)
if count eq 0 then return else db_file=db_file[0]
if not test_open(db_file,/write) then return
resolve_routine,'db_gbo'

if is_blank(type) then type=['mdi']

if not valid_time(tstart) then dstart=anytim2utc(!stime) else $
 dstart=anytim2utc(tstart)

if not valid_time(tend) then dend=anytim2utc(!stime) else $
 dend=anytim2utc(tend)

dstart.time=0 & dend.time=0
if dend.mjd eq dstart.mjd then dend.mjd=dend.mjd+1
if is_number(back) then tback=back > 0 else tback=0
if tback gt 0 then dstart.mjd=dstart.mjd-tback

message,'processing: '+anytim2utc(dstart,/vms)+' to '+anytim2utc(dend,/vms),/cont

;-- GBO catalog info

reprocess=keyword_set(reprocess)
url='http://sohowww.nascom.nasa.gov/data/synoptic/images/'

restore_gbo
common gbo_com, gbo_records
def_gbo,def_gbo

omdi=obj_new('mdi')
t1=dstart
repeat begin
 fid=time2fid(t1)

 for i=0,n_elements(type)-1 do begin
  files=loc_file(type[i]+'*',path=concat_dir('$SYNOP_DATA/images',fid),$
               count=count,/recheck)
  if count gt 0 then begin
   for k=0,count-1 do begin
    break_file,files[k],dsk,dir,name,ext
    file=name+ext
    done=where(file eq gbo_records.file,fcount)
    if (fcount eq 0) or reprocess then begin
     if fcount gt 0 and reprocess then gbo_records[done].deleted=1b
     omdi->read,files[k],index=index,/nodata
     utc=anytim2utc(index.date_obs)

     temp_gbo=def_gbo
     temp_gbo.dstart=utc.mjd
     temp_gbo.tstart=utc.time
     temp_gbo.dend=utc.mjd
     temp_gbo.tend=utc.time
     temp_gbo.url=url+concat_dir(fid,file)
     temp_gbo.file=file
     temp_gbo.class='Image'
     temp_gbo.format='FITS'

     if stregex(file,'mdi',/bool) then begin
      temp_gbo.observatory='SOHO/MDI'
      temp_gbo.instrument='MDI'
      temp_gbo.email='gregory@mdisas.nascom.nasa.gov'
      temp_gbo.type='Optical'
      if stregex(file,'_mag',/bool) then temp_gbo.subtype='Magnetograms (line of sight)'
     endif

     if stregex(file,'eit',/bool) then begin
      wave=stregex(file,'(304|171|195|284)',/ext)
      temp_gbo.observatory='SOHO/EIT'
      temp_gbo.instrument='EIT '+wave
      temp_gbo.type='XUV,EUV,UV'
      temp_gbo.email='thompson@eitv3.nascom.nasa.gov'
     endif

;-- get GOES events

     gev=nearest_gev(utc,during=during)
     if is_struct(gev) then begin
      class=trim(string(gev.st$class))
      day=trim(gt_day(gev,/str))
      fstart=strmid(trim(gt_time(gev,/str)),0,5)
      result=[day[*]+','+fstart[*]+','+class[*]]
      temp_gbo.goes=arr2str(result,delim='+')
     endif

;-- get NOAA AR's

     day=utc
     day.time=0
     temp_gbo.noaa=list_nar(day,/all)
     temp_gbo.submitted=anytim2tai(!stime)
     update_gbo,temp_gbo,/no_save
    endif
   endfor
  endif

 endfor

 t1.mjd=t1.mjd+1
endrep until (t1.mjd gt dend.mjd)

obj_destroy,omdi
purge_gbo,/save
 
return & end
