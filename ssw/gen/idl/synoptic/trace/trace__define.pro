;+
; Project     : HESSI
;
; Name        : TRACE__DEFINE
;
; Purpose     : Define a TRACE data object
;
; Category    : Ancillary GBO Synoptic Objects
;
; Syntax      : IDL> c=obj_new('trace')
;
; History     : Written 30 Dec 2007, D. Zarro (ADNET)
;
; Contact     : dzarro@solar.stanford.edu
;-

;------------------------------------------------------------------------

function trace::get,dummy,selection=selection,_ref_extra=extra,multiple=multiple

if keyword_set(selection) then return,self.selection
if keyword_set(multiple) then return,self.multiple

return,self->fits::get(dummy,_extra=extra)

end

;-------------------------------------------------------------------------
;-- set TRACE calibration environment  

pro trace::set_calibration,err=err,quiet=quiet

mklog,'SSW_TRACE','$SSW/trace',/local
mklog,'TRACE_DBASE','$SSW_TRACE/dbase',/local
mklog,'TRACE_CAL','$TRACE_DBASE/cal',/local
mklog,'TRACE_RESPONSE','$SSW_TRACE/response',/local
mklog,'TR_CAL_INFO','$TRACE_CAL/info',/local
mklog,'TRACE_EGSE_DBASE','$TRACE_DBASE/egse/dbs',/local
mklog,'TOPS_PATH','$SSW_TRACE/obs',/local
mklog,'TOPS_IDS_PATH','$SSW_TRACE/obs/dbase',/local
mklog,'HTAB','$SSW_TRACE/dbase/tables',/local
mklog,'QTAB','$SSW_TRACE/dbase/tables',/local

if ~is_dir('$tdb') then begin
 err='Warning: TRACE calibration directory not installed.'
 if ~keyword_set(quiet) then message,err,/cont
endif

return
end

;-------------------------------------------------------------------------
;-- check for trace_decode_idl shareable object

function trace::have_decoder,err=err,quiet=quiet,_extra=extra

wdir=!version.OS + '_' + !version.ARCH 
decomp='trace_decode_idl.so'
if os_family() eq 'Windows' then decomp='trace_decode_idl.dll'

share=local_name('$SSW_TRACE/binaries/'+wdir+'/'+decomp)
chk=file_search(share,count=count)
if count eq 0 then begin
 share=local_name('$CALL_EXTERNAL_USER/'+decomp)
 chk=file_search(share,count=count)
endif

;-- download a copy to temporary directory

if count eq 0 then begin
 warn='IDL shareable object "trace_decode_idl" not found on this system.'
 message,warn+' Downloading from server...',/cont
 sdir=get_temp_dir()
 udir=concat_dir(sdir,wdir)
 mk_dir,udir
 mklog,'CALL_EXTERNAL_USER',udir
 sloc=ssw_server(/full)
 sfile=sloc+'/solarsoft/trace/binaries/'+wdir+'/'+decomp
 sock_copy,sfile,out_dir=udir,_extra=extra
 chk=file_search(share,count=count)
endif

return,count ne 0

end

;--------------------------------------------------------------------------
;-- VSO search wrapper

function trace::search,tstart,tend,_ref_extra=extra

return,vso_files(tstart,tend,inst='trace',_extra=extra)

end

;---------------------------------------------------------------------------
;-- check for TRACE branch in !path

function trace::have_path,err=err,quiet=quiet

err=''
if ~have_proc('read_trace') then begin
 epath=local_name('$SSW/trace/idl')
 if is_dir(epath) then ssw_path,/trace,/quiet
 if ~have_proc('read_trace') then begin
  err='TRACE branch of SSW not installed.'
  if ~keyword_set(quiet) then message,err,/cont
  return,0b
 endif
endif

return,1b

end

;--------------------------------------------------------------------------
;-- FITS reader

pro trace::read,file,data,index=index,_ref_extra=extra,$
               no_prep=no_prep,nodata=nodata,err=err

;-- download if URL
 
self->get,file,local_file=ofile,_extra=extra
if is_blank(ofile) then return

;-- check calibration files

prep=~keyword_set(no_prep)
self->set_calibration,err=warn_cal

;-- read files

warned=0b & warned_1=0b & warned_2=0b 
k=-1l
self->empty
nfiles=n_elements(ofile)
for i=0,nfiles-1 do begin
 err=''
 valid=self->is_valid(ofile[i],err=err,level=level)
 if is_string(err) then continue
 if ~valid or (level lt 0) then begin
  err='Invalid TRACE file - '+file
  message,err,/cont
  continue
 endif

;-- level 0, read INDEX first to see what observations are present

 if level eq 0 then begin

  if ~warned then begin
   if ~self->have_path(err=warn,/quiet) then begin
    err=[warn,' ','Cannot read Level 0 file without READ_TRACE procedure.']
    xack,err,/suppress
    message,arr2str(err,delim=''),/cont
    warned=1b
   endif
  endif
  if warned then continue
  self.selection=1b
  self.multiple=1b

  if ~warned_1 then begin
   if is_string(warn_cal) then begin
    xack,[warn_cal,'Cannot background subtract and flatfield images.'],/suppress
    prep=0b & warned_1=1b
   endif
  endif
 
  call_procedure,'read_trace',ofile[i],-1,index,/nodata
 
  if ~is_struct(index) then begin
   err='No data records in file'
   message,err,/cont
   continue
  endif

  output='TIME: '+trim(anytim2utc(index.date_obs,/vms))+ $
         ' WAVELENGTH: '+strpad(trim(index.wave_len),4,/after)+ $
         ' NAXIS1: '+strpad(trim(index.naxis1),4,/after)+ $
         ' NAXIS2: '+strpad(trim(index.naxis2),4,/after)

  list=xsel_list_multi(output,/index,cancel=cancel,$
                       label='Select observing program from list below:')
  if cancel then begin
   err='Reading cancelled.'
   continue
  endif

;-- check for JPEG decoder

  if ~warned_2 then begin
   if ~self->have_decoder(err=warn,/quiet) then begin
    err=[warn,' ','Cannot read Level 0 file without TRACE JPEG decompressor.']
    xack,err,/suppress
    message,arr2str(err,delim=''),/cont
    warned_2=1b
   endif
  endif
  if warned_2 then continue

  call_procedure,'read_trace',ofile[i],list,index,data,_extra=extra
 endif else begin
  self->fits::mreadfits,ofile[i],data,_extra=extra,index=index,err=err
 endelse
 if keyword_set(nodata) or ~is_struct(index) then continue

;-- insert data into maps

 for j=0,n_elements(index)-1 do begin
  k=k+1
  nx=index[j].naxis1 & ny=index[j].naxis2 
  wave=index[j].wave_len
  time=time2fid(index[j].date_obs,/time,/full,/sec,/mill)
  name='trace_'+time+'_'+wave+'_'+trim(nx)
  self->mk_map,index[j],data[0:nx-1,0:ny-1,j],k,/no_copy,$
           id='TRACE '+trim(index[j].wave_len),$
           err=err,filename=name,_extra=extra
  if is_string(err) then begin
   k=k-1
   continue
  endif
  self->set,k,/log,grid=30,/limb
 endfor
endfor

count=self->get(/count)
err=''
if count eq 0 then begin
 err='No TRACE data read.'
 return
endif

if prep and ~warned then self->prep,_extra=extra,err=err
 
return & end

;-----------------------------------------------------------------------------
;-- prep TRACE image

pro trace::prep,k,_extra=extra,err=err

count=self->get(/count)
if count eq 0 then return
if ~self->have_path(err=err,/quiet) then return

if is_number(k) then begin
 istart=k & iend=k
endif else begin
 istart=0 & iend=count-1
endelse

for i=istart,iend do begin

 if ~self->has_data(i,err=err) then begin
  message,err,/cont
  continue
 endif

;-- skip if prepped

 index=self->get(i,/index)
 prepped=0b
 if have_tag(index,'history') then begin
  chk=where(stregex(index.history,'trace_prep',/bool,/fold),count)
  prepped=count gt 0
 endif
 if prepped then continue

 map=self->get(i,/map,/no_copy,err=err)
 call_procedure,'trace_prep',index,temporary(map.data),nindex,ndata,_extra=extra,$
                     /wave2point,/normalize,/float
 map.xc=nindex.xcen
 map.yc=nindex.ycen
 map.data=temporary(ndata)
 self->set,i,map=map,index=nindex,/no_copy,/replace

endfor

return & end

;------------------------------------------------------------------------------
;-- check if valid TRACE file

function trace::is_valid,file,err=err,level=level

level=-1
mrd_head,file,header,err=err
if is_string(err) then return,0b

chk1=where(stregex(header,'MPROGNAM.+TR_REFORMAT',/bool,/fold),count1)
chk2=where(stregex(header,'(INST|TEL|DET|ORIG).+TRAC',/bool,/fold),count2)

valid=(count1 ne 0) or (count2 ne 0)
if count1 eq 1 then level=0
if count2 eq 1 then level=1

return,valid
end

;------------------------------------------------------------------------------
;-- TRACE structure definition

pro trace__define,void                 

void={trace, selection:0b, multiple:0b,inherits fits}

return & end
