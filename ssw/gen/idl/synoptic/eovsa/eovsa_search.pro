;+
; Project     : EOVSA
;                  
; Name        : EOVSA_SEARCH
;               
; Purpose     : Search EOVSA catalog for files
;                             
; Category    : Utility database 
;               
; Syntax      : IDL> files=eovsa_search(tstart,tend)
; 
; Inputs      : TSTART = UT to start search
;               TEND = UT to end search (optional)
; Outputs     : FILES = matching files
;
; Keywords    : Coming soon
;                   
; History     : 22 November 2013 (ADNET)
;
; Contact     : dzarro@solar.stanford.edu
;-    

function eovsa_search,tstart,tend,_ref_extra=extra

e=obj_new('eovsa',_extra=extra)

files=e->search(tstart,tend,_extra=extra)

obj_destroy,e

return,files
end
