;+
; Project     : VSO
;
; Name        : EOVSA__DEFINE
;
; Purpose     : Class definition for EOVSA data object
;
; Category    : Objects
;
; History     : Written 23 September 2013, D. Zarro (ADNET)
;
; Contact     : dzarro@solar.stanford.edu
;-

;---------------------------------------------------

function eovsa::init,_ref_extra=extra

if ~self->fits::init(_extra=extra) then return,0
self.eovsa_db=obj_new('eovsa_db',_extra=extra,/url)
return,1

end

;-------------------------------------------------

pro eovsa::cleanup

obj_destroy,self.eovsa_db
self->fits::cleanup

return & end

;--------------------------------------------------
function eovsa::search,tstart,tend,_ref_extra=extra,count=count,$
         times=times,type=type,metadata=metadata

files='' & times=0.d & type=''
self.eovsa_db->list,tstart,tend,metadata,_extra=extra,count=count

dtype=['image','spectrum','lightcurve']
if count gt 0 then begin
 times=parse_time(metadata.filename,/tai,ymd=ymd,sep='')
 files=self->server()+'/'+ymd+'/'+metadata.filename
 stype=metadata.type 
 type=strarr(count)
 for i=0,2 do begin
  chk=where(i eq stype,scount)
  if scount gt 0 then type[chk]=dtype[i]
 endfor
 type='radio/'+type
endif

return,files

end

;--------------------------------------------------------

pro eovsa::read,file,_ref_extra=extra

self->getfile,file,local_file=cfile,err=err,_extra=extra
if is_blank(cfile) or is_string(err) then return

self->fits::read,cfile,_extra=extra 
count=self->get(/count) 
if count gt 0 then for i=0,count-1 do self->set,i,grid=30,/limb else $
 message,'No maps created.' ,/info

return & end


;----------------------------------------------------

function eovsa::server

return,'http://ovsa.njit.edu/fits'

end

;-----------------------------------------------------------------------------
;-- check for EOVSA branch in !path

function eovsa::have_path,err=err,verbose=verbose

err=''
if ~have_proc('read_eovsa') then begin
 epath=local_name('$SSW/radio/eovsa/idl')
 if is_dir(epath) then add_path,epath,/append,/quiet,/expand
 if ~have_proc('read_eovsa') then begin
  err='EOVSA branch of SSW not installed.'
  if keyword_set(verbose) then message,err,/info
  return,0b
 endif
endif

return,1b
end

;------------------------------------------------------
pro eovsa__define,void                 

void={eovsa, inherits fits, eovsa_db:obj_new()}

return & end
