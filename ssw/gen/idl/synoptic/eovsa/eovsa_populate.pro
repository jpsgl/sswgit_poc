 ;+
; Project     : EOVSA
;                  
; Name        : EOVSA_POPULATE
;               
; Purpose     : Populate EOVSA catalog
;                             
; Category    : Utility database 
;               
; Syntax      : IDL> eovsa_populate,tstart,tend
; 
; Inputs      : TSTART,TEND = UT start and end times to populate.
;
; Outputs     : None
;
; Keywords    : FILES = filenames used to populate catalog
;                   
; History     : 17 April 2014 (ADNET)
;
; Contact     : dzarro@solar.stanford.edu
;-    

pro eovsa_populate,tstart,tend,files=files,count=count,_extra=extra

err='' & files='' & count=0
if ~valid_time(tstart) then begin
 pr_syntax,'eovsa_populate,tstart,tend'
 return
end

files=eovsa_list(tstart,tend,count=count,_extra=extra)
if count eq 0 then return
eovsa_update,files,_extra=extra

return
end
