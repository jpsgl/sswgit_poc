 ;+
; Project     : EOVSA
;                  
; Name        : EOVSA_UPDATE
;               
; Purpose     : Update EOVSA catalog
;                             
; Category    : Utility database 
;               
; Syntax      : IDL> eovsa_update,files
; 
; Inputs      : FILES = array of filenames to ingest
;
; Outputs     : None
;
; Keywords    : None
;                   
; History     : 29 December 2013 (ADNET)
;
; Contact     : dzarro@solar.stanford.edu
;-    

pro eovsa_update,files,_ref_extra=extra

e=obj_new('eovsa_db',_extra=extra)

e->update,files,_extra=extra

obj_destroy,e

return
end
