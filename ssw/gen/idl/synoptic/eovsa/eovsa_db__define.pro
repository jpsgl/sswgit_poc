;+
; Project     :	VSO
;
; Name        :	EOVSA_DB__DEFINE
;
; Purpose     :	Object wrapper around EOVSA database software
;
; Category    :	Databases
;
; History     :	3-May-2013, Zarro (ADNET), written.
;
;-

function eovsa_db::init,_ref_extra=extra

chk=self->dbase::init('eovsa',_extra=extra)
if ~chk then return,0 
self->set_zdbase,_extra=extra
return,1

end

;----------------------------------------------------------

pro eovsa_db::set_zdbase,test=test,url=url,verbose=verbose

case 1 of
 keyword_set(test): mklog,'ZDBASE','~/idl/database' 
 keyword_set(url): setenv,'ZDBASE=http://sohowww.nascom.nasa.gov/solarsoft/radio/eovsa/catalog'
 else: mklog,'ZDBASE','$SSW/radio/eovsa/catalog' 
endcase

if keyword_set(verbose) then message,'Using ZDBASE '+getenv('ZDBASE'),/info

return 
end

;--------------------------------------------------------------------
;-- update catalog with FITS header metadata

pro eovsa_db::update,files,verbose=verbose,older=older,$
                     force=force,err=err,purge=purge

err=''
if ~self->write_access(err=err) then return

if keyword_set(purge) then begin
 self->purge,err=err
 return
endif

verbose=keyword_set(verbose)
force=keyword_set(force)

if is_blank(files) then begin
 err='No filenames entered.'
 message,err,/info
 return
endif

if verbose then message,'Writing to '+getenv('ZDBASE'),/info

nfiles=n_elements(files)
updated=0

for i=0,nfiles-1 do begin
; if verbose then message,'Processing '+files[i],/info

 self->parse,files[i],metadata

;-- First check for metadata nearest file time. If none, then add it.
;-- If same, check that it differs

 window=5*60.d
 time=metadata.date_obs
 tstart=time-window & tend=time+window
 self->list,tstart,tend,entries,count=count,err=err
 if count eq 0 then begin
  if verbose then message,'Adding ' +metadata.filename,/info
  self->add,metadata,err=err
  updated=updated+1
  continue
 endif

;-- How does this one differ?

 check=where(trim(metadata.filename) eq trim(entries.filename),count)

 if count eq 0 then begin
  if verbose then message,'Adding ' +metadata.filename,/info
  self->add,metadata,err=err
  updated=updated+1
  continue
 endif

 if count gt 1 then begin
  message,'Duplicate file names!',/info
  continue
 endif

 if count eq 1 then begin
  entry=entries[check]
  same=match_struct(metadata,entry,exclude=['id','cat_date','deleted'],dtag=dtag)
  if same and ~force then continue
  if verbose and ~same then message,'Metadata differences - '+arr2str(dtag),/info

;-- If current file date is older than cataloged file date, then skip
;   unless /older is set

  if ~force and ((metadata.file_date lt entry.file_date) and ~keyword_set(older)) then continue 
  metadata.id=entry.id 
  if verbose then message,'Replacing ' +metadata.filename,/info
  self->add,metadata,err=err,/replace
  updated=updated+1
 endif

endfor

if verbose then message,trim(updated) +' catalog entries updated.',/info

return
end

;--------------------------------------------------------------------
;-- parse FITS file header into catalog metadata

pro eovsa_db::parse,file,metadata,err=err,header=header

hfits=obj_new('hfits')
hfits->read,file,header=header,err=err,/nodata
obj_destroy,hfits
if is_string(err) then begin
 message,err,/info
 return
endif

stc=fitshead2struct(header)
metadata=self->get_metadata(err=err)
if is_string(err) then return
struct_assign,stc,metadata
metadata.id=-1
metadata.date_obs=anytim2tai(stc.date_obs)
metadata.date_end=anytim2tai(stc.date_end)
metadata.filename=file_basename(file)

if ~have_tag(stc,'type') then begin
 if stregex(metadata.filename,'_im',/bool) then metadata.type=0
 if stregex(metadata.filename,'_sp',/bool) then metadata.type=1
 if stregex(metadata.filename,'_(lt|li)',/bool) gt -1 then metadata.type=2
endif

if is_url(file) then begin
 resp=sock_head(file,date=date)
endif else begin
 info=file_info(file)
 date=systim(0,info.mtime,/utc)
endelse
metadata.file_date=anytim2tai(date)

get_utc,utc
metadata.cat_date=anytim2tai(utc)

return
end 

;----------------------------------------------------------------------
;-- check if item is valid metadata

function eovsa_db::valid,item

if ~is_struct(item) then return,0b
def=self->get_metadata(err=err)
if is_string(err) then return,0b
if ~match_struct(item,def,/tags_only) then return,0b
if ~valid_time(item.date_obs) then return,0b
check=where(item.type eq [0,1,2],count)
if count eq 0 then return,0b

return,1b

end
;-----------------------------------------------------------------------

pro eovsa_db__define

struct={eovsa_db,inherits dbase}

return & end
