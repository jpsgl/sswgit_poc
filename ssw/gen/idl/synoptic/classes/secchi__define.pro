;+
; Project     : STEREO
;
; Name        : SECCHI__DEFINE
;
; Purpose     : Define a SECCHI data/map object
;
; Category    : Objects
;
; Syntax      : IDL> a=obj_new('secchi')
;
; Examples    : IDL> a->read,'20070501_000400_n4euA.fts' ;-- read FITS file
;               IDL> a->plot                             ;-- plot image
;               IDL> map=a->getmap()                     ;-- access map
;               IDL> data=a->getdata()                   ;-- access data
;                       
;               Searching via VSO:                                     
;               IDL> files=a->search('1-may-07','02:00 1-may-07')
;               IDL> print,files[0]
;               http://stereo-ssc.nascom.nasa.gov/data/ins_data/secchi/L0/a/img/euvi/20070501/20070501_000400_n4euA.fts
;               
;               Copying from VSO:
;               IDL> a->get,files[0],/verbose
;
; History     : Written 13 May 2007, D. Zarro (ADNET)
;               Version 2, 31-Oct-2007, William Thompson, modified for COR1/COR2
;
; Contact     : dzarro@solar.stanford.edu
;-

;-----------------------------------------------------------------------------
;-- check for SECCHI branch in !path

function secchi::have_path,err=err

err=''
if ~have_proc('sccreadfits') then begin
 epath=local_name('$SSW/stereo/secchi/idl')
 if is_dir(epath) then ssw_path,/secchi,/quiet
 if ~have_proc('sccreadfits') then begin
  err='STEREO/SECCHI branch of SSW not installed.'
  message,err,/cont
  return,0b
 endif
endif

return,1b

end

;---------------------------------------------------------------------

function secchi::have_calibration,err=err

;-- ensure that calibration directories are properly defined

SSW_SECCHI=local_name('$SSW/stereo/secchi')
mklog,'SSW_SECCHI',SSW_SECCHI
SECCHI_CAL=local_name('$SSW_SECCHI/calibration')
mklog,'SECCHI_CAL',SECCHI_CAL

have_cal=1b
if ~is_dir('$SSW_SECCHI/calibration') then begin
 err='Warning - $SSW_SECCHI/calibration directory not found'
 message,err,/cont
 have_cal=0b
endif

return,have_cal & end

;--------------------------------------------------------------------------
;-- FITS reader

pro secchi::read,file,data,index=index,_ref_extra=extra,err=err,$
            no_prep=no_prep,nodata=nodata,polar=polar

self->empty

;-- download files if not present

err=''
self->get,file,local_file=cfile,err=err,_extra=extra
if is_blank(cfile) then return

;-- check that calibration and path directories are properly defined

have_cal=self->have_calibration(err=warn)
if ~have_cal then xack,warn,/suppress

have_path=self->have_path(err=warn)
if ~have_path then xack,[warn,'Pointing information may be unreliable for co-registration.'],/suppress

no_prep=keyword_set(no_prep) or keyword_set(nodata) or $
        ~have_path or ~have_cal

;-- validate input files

nfiles=n_elements(cfile)
valid=bytarr(nfiles)
detector=strarr(nfiles)
for i=0,nfiles-1 do begin
 valid[i]=self->is_valid(cfile[i],det=det)
 detector[i]=det
endfor
ok=where(valid,count)
if count eq 0 then begin
 err='Input file[s] are not SECCHI FITS format'
 message,err,/cont
 return
endif

cfile=cfile[ok]
detector=strupcase(strtrim(detector[ok],2))

;-- check for COR1/COR2 triplet

polar=keyword_set(polar)
if (count eq 3) and ~no_prep then begin
 chk1=where(detector eq 'COR1',c1)
 chk2=where(detector eq 'COR2',c2)
 use_polar=(c1 eq 3) or (c2 eq 3)
 if polar and ~use_polar then begin
  message,'Cannot use /POLAR since coronal triplet images do not match',/cont
  polar=0b
 endif
endif else polar=0b

if have_path then begin
 if no_prep then $
  data=call_function('sccreadfits',cfile,index,nodata=nodata,_extra=extra) else $
   secchi_prep,cfile,index,data,_extra=extra,polar=polar
endif else begin
 self->fits::mreadfits,cfile,data,_extra=extra,index=index,err=err,nodata=nodata
endelse

if keyword_set(nodata) then return

;-- insert data into maps

self->index2map,index,data,err=err,_extra=extra
if is_string(err) then message,err,/cont

return & end

;---------------------------------------------------------------------
;-- store INDEX and DATA into MAP objects

pro secchi::index2map,index,data,err=err

err=''

;-- check inputs

if ~is_struct(index) then begin
 err='Input index is not a valid structure'
 return
endif

ndim=size(data,/n_dim)
if (ndim lt 2) or (ndim gt 3) then begin
 err='Input image is not a valid data array. Check source FITS file'
 return
endif


nindex=n_elements(index)

if (ndim eq 2) and (nindex ne 1) then begin
 err='Inconsistent index and image data size'
 return
endif

if (ndim eq 3) then begin
 sz=size(data)
 ndata=sz[n_elements(sz)-3]
 if ndata ne nindex then begin
  err='Inconsistent index and image data size'
  return
 endif
endif

;-- add STEREO-specific properties

for i=0,nindex-1 do begin
 id=index[i].OBSRVTRY+' '+index[i].INSTRUME+' '+index[i].DETECTOR+' '+trim(index[i].WAVELNTH)
 nx=index[i].naxis1 & ny=index[i].naxis2

 self->mk_map,index[i],data[0:nx-1,0:ny-1,i],i,_extra=extra,id=id,/no_copy,$
       roll_angle=index[i].crota,roll_center=[index[i].xcen,index[i].ycen],$
       b0=index[i].hglt_obs,l0=index[i].hgln_obs,rsun=index[i].rsun,$
       err=err,filename=index[i].filename
 if is_string(err) then continue
 if index[i].detector eq 'EUVI' then self->set,i,/log_scale,grid=30,/limb
; self->colors,i
endfor

return & end

;-----------------------------------------------------------------------
;-- VSO search function

function secchi::search,tstart,tend,_ref_extra=extra,$
                 euvi=euvi,cor1=cor1,cor2=cor2

case 1 of
 keyword_set(euvi): det='euvi'
 keyword_set(cor1): det='cor1'
 keyword_set(cor2): det='cor2'
 else: det=''
endcase

return,vso_files(tstart,tend,inst='secchi',_extra=extra,det=det)

end

;------------------------------------------------------------------------------
;-- save SECCHI color table

pro secchi::colors,k

if ~have_proc('secchi_colors') then return
index=self->get(k,/index)
secchi_colors,index.detector,index.wavelnth,red,green,blue
self->set,k,red=red,green=green,blue=blue,/load_colors,/has_colors

return & end

;------------------------------------------------------------------------------
;-- check if valid SECCHI file

function secchi::is_valid,file,err=err,detector=detector,_extra=extra

detector='' & instrument=''
mrd_head,file,header,err=err
if is_string(err) then return,0b

s=fitshead2struct(header)
if have_tag(s,'dete',/start,index) then detector=strup(s.(index))
if have_tag(s,'inst',/start,index) then instrument=strup(s.(index))

return,instrument eq 'SECCHI'
end

;------------------------------------------------------------------------
;-- SECCHI data structure

pro secchi__define,void                 

void={secchi, inherits fits}

return & end
