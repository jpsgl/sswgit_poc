;+
; Project     : HESSI
;
; Name        : GET_LATEST_IMAGE
;
; Purpose     : return latest image from Synoptic archive
;
; Category    : synoptic gbo hessi
;
; Syntax      : IDL> map=get_latest_image(time,back=back)
;
; Inputs      : TIME = image closest to this time is returned
;     
; Output      : MAP = image in map structure format
;
; Keywords    : BACK = # of days back in time to search [def=5]
;               TYPE = /EIT,/BBSO,/TRACE,/MEUD,/MDI
;               LAST = return last saved image
;               FOUND = 1/0 if image is found/not found
;             
; History     : Written 1 Feb 2003, D. Zarro (EER/GSFC)
;
; Contact     : dzarro@solar.stanford.edu
;-

function get_latest_image,time,back=back,_extra=extra,err=err,$
                    last=last,verbose=verbose,found=found

err=''

common latest_image,synop,last_map

found=0b

;-- return last map if requested 

if keyword_set(last) and valid_map(last_map) then begin
 found=1b
 return,last_map
endif

;-- default to current UT

secs_per_day=24.*3600.d
if valid_time(time) then tend=time else get_utc,tend
if is_number(back) then tback=float(back) else tback=3.

;-- create and store a SYNOP object

if not obj_valid(synop) then synop=obj_new('synop')

;-- set up start/end times

tend=anytim2tai(tend)+secs_per_day
tstart=tend-(tback+1.)*secs_per_day

dprint,'% TSTART/TEND: ',anytim2utc(tstart,/vms)+' - '+anytim2utc(tend,/vms)
synop->setprop,mode=1,tstart=tstart,tend=tend,verbose=0,ldir=get_temp_dir()

;-- list all images in archive

synop->list,files,times=times,count=count,err=err

no_files='No matching files found'
if count eq 0 then begin
 message,no_files,/cont
 return,-1
endif

;-- filter out type 

if is_struct(extra) then begin
 tags=tag_names(extra)
 chk=where(stregex(files,strmid(tags[0],0,3),/bool,/fold),count)
 if count eq 0 then begin
  message,no_files,/cont
  return,-1
 endif
 files=files[chk] & times=times[chk]
endif

;-- get nearest file in time

diff=times-tend
nearest=where(diff eq min(diff))
tfile=files[nearest[0]]

;-- download it

lfile=synop->fetch(tfile,err=err)

if err ne '' then begin
 message,err,/cont
 return,-1
endif

;-- make a map

fits2map,lfile,map,err=err
if err ne '' then return,-1

last_map=map

if keyword_set(verbose) then message,'Found - '+map.id+' at '+map.time,/cont

found=1b
return,map

end
