;+
; Project     : HINODE/EIS
;
; Name        : HALPHA__DEFINE

; Purpose     : Define a H-alpha data object
;
; Category    : Ancillary GBO Synoptic Objects
;
; Syntax      : IDL> c=obj_new('halpha')
;
; History     : Written 23 Oct 2006, D. Zarro, (ADNET/GSFC)
;
; Contact     : dzarro@solar.stanford.edu
;-
;-----------------------------------------------------------------------------
;-- init 

function halpha::init,_ref_extra=extra

ret=self->bbso::init(_extra=extra)
              
if ret then self->setprop,ftype='*_ha*',ext='.fts,.fits'

return,ret

end

;----------------------------------------------------------------------------

pro halpha::cleanup

self->bbso::cleanup

return & end

;-------------------------------------------------------------------------------
;-- FTP search of BBSO archive

function halpha::list,tstart,tend,_ref_extra=extra,full_path=full_path

self->setprop,tstart=tstart,tend=tend
self->site::list,files,_extra=extra

if is_blank(files) then return,''
rhost=self->getprop(/rhost)
if keyword_set(full_path) then files='ftp://'+rhost+files else files=file_break(files)

return,files

end

;------------------------------------------------------------------------------

pro halpha::copy,file,out_dir=out_dir,copy_file=copy_file,_ref_extra=extra

if is_blank(file) then return

;-- strip off URI

s='(ftp://)([^/:,]+)(/[^:]+)'
nfile=stregex(file,s,/ext,/sub)

;-- strip remote file and directory names

nfile=nfile[3,*]
rfile=file_basename(nfile)
rdir=file_break(nfile,/path)

if is_string(out_dir) then ldir=file_dirname(out_dir)

;-- save current object state

temp=create_struct(name=obj_class(self))
struct_assign,self,temp

;-- download

self->setprop,rfile=rfile,rdir=rdir,ldir=ldir,_extra=extra,bytes=0

self->copy_file,copy_file,_extra=extra

;-- restore state

struct_assign,temp,self,/nozero

return & end

;------------------------------------------------------------------------------------------
;-- def structure

pro halpha__define                 

self={halpha,inherits bbso}

return & end
