pro rpc_test,output,ofile=ofile

output=eit_files('1-may-01','01:00 1-may-01')

if is_blank(output) then return
openw,lun,ofile,/get_lun,err=err
if (err ne 0) then return
np=n_elements(output)
for i=0,np-1 do printf,lun,output[i]
close_lun,lun

return & end

