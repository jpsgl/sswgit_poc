;+
; Project     : HESSI
;
; Name        : HSI_SYNOP
;
; Purpose     : update Max Millennium catalog with RHESSI flare catalog info
;
; Category    : synoptic gbo hessi
;
; Syntax      : IDL> hsi_synop
;
; Inputs      : TSTART,TEND = time range to process
;     
; Keywords    : CLOBBER = clobber existing entries
;               BACK = # of days back to process
;               VERBOSE = set verbose output
;             
; Restrictions: Unix only
;
; History     : Written 28 July 2002, D. Zarro (LAC/GSFC)
;
; Contact     : dzarro@solar.stanford.edu
;-

pro hsi_synop,tstart,tend,clobber=clobber,back=back,verbose=verbose

;-- usual error checks
 
if os_family(/lower) ne 'unix' then begin
 err='sorry, Unix only'
 message,err,/cont
 return
endif

;-- read HESSI flare catalog

flare_data = hsi_read_flarelist (err=err, /force)
if is_string(err) then return

;-- locate MM database file

db_file=loc_file('$GBO_DB',count=count,err=err,/verb)
if count eq 0 then return else db_file=db_file[0]
if not test_open(db_file,/write) then return
recompile,'db_gbo'

;-- restore MM catalog info

restore_gbo
common gbo_com, gbo_records
def_gbo,def_gbo

;-- establish processing times

if not valid_time(tstart) then dstart=anytim2utc(!stime) else $
 dstart=anytim2utc(tstart)

if not valid_time(tend) then dend=dstart else $
 dend=anytim2utc(tend)

;-- round to start and end of day

dstart.time=0 & dend.time=0
dend.mjd=dend.mjd+1
if dend.mjd le dstart.mjd then dend.mjd=dstart.mjd+1

if is_number(back) then tback=back > 0 else tback=0
if tback gt 0 then dstart.mjd=dstart.mjd-tback

message,'processing: '+anytim2utc(dstart,/vms)+' to '+anytim2utc(dend,/vms),/cont

;-- figure out overlapping RHESSI events

dstart=anytim2tai(dstart)
dend=anytim2tai(dend)
fstart=anytim(flare_data.start_time,/tai)
fend=anytim(flare_data.end_time,/tai)

d1= (fstart gt dend) 
d2= (fend gt dend)
d3= (fstart lt dstart) 
d4= (fend lt dstart) 

ochk=where( (d1 and d2) or (d3 and d4),ncomplement=count,complement=fchk)
if count eq 0 then begin
 message,'no cataloged RHESSI events during specified input times',/cont
 return
endif

;-- check if any HESSI events are in MM catalog

hchk=where(gbo_records.observatory eq 'RHESSI',rcount)
clobber=keyword_set(clobber)
for i=0,count-1 do begin

 flare=flare_data[fchk[i]]
 flare_id='Flare ID: '+trim2(flare.id_number)

;-- add new events to MM catalog or replace old (if clobber is set)

 new_event=1b
 if rcount gt 0 then begin
  look=where(flare_id eq gbo_records.info,clook)
  if (clook gt 0) then begin
   if clobber then gbo_records[clook].deleted=1b else new_event=0b
  endif
 endif
 
 if new_event then begin
 
  temp=def_gbo
  rstart=anytim(flare.start_time,/utc_int)
  rend=anytim(flare.end_time,/utc_int)
  temp.dstart=rstart.mjd
  temp.tstart=rstart.time
  temp.dend=rend.mjd
  temp.tend=rend.time
  temp.observatory='RHESSI'
  temp.instrument='RHESSI'
  temp.class='Image Spectrum Lightcurve'
  temp.type='Soft X-ray, Hard X-ray, Gamma-ray'
  temp.subtype='Full-Disk'
  temp.format='FITS'
  temp.name='D. Zarro'
  temp.campaign='009. Default HESSI Collaboration'
  temp.email='zarro@smmdac.nascom.nasa.gov'
  temp.file=flare.filename
  temp.url="http://hesperia.gsfc.nasa.gov/hessidata"
  temp.info=flare_id

;-- get GOES events

  gev=nearest_gev(rstart)
  if is_struct(gev) then begin
   class=trim(string(gev.st$class))
   day=trim(gt_day(gev,/str))
   fstart=strmid(trim(gt_time(gev,/str)),0,5)
   result=[day[*]+','+fstart[*]+','+class[*]]
   temp.goes=arr2str(result,delim='+')
  endif

  temp.submitted=anytim2tai(!stime)
  update_gbo,temp,/no_save

 endif

endfor

if new_event then purge_gbo else message,'No new RHESSI events added',/cont
 
return & end
