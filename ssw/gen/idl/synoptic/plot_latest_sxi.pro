;+
; Project     : SXI
;
; Name        : PLOT_LATEST_SXI
;
; Purpose     : Searches, downloads, and plots latest GOES12/SXI Level-1 image
;               from NGDC. 
;
; Category    : Synoptic display
;
; Syntax      : IDL> plot_latest_sxi,file
;
; Inputs      : None
;
; Outputs     : FILE = name of FITS file found and plotted.
;               Plot to current window, or PNG file (/PNG)
;
; Keywords    : PNG = set to plot PNG
;               OUT_DIR = directory to download FITS file and create PNG file
;               BACK = # of days to look back [def = 3]
;
; History     : Written 8 March 2003, Zarro (EER/GSFC)
;
; Contact     : dzarro@solar.stanford.edu
;-

pro plot_latest_sxi,file,png=png,_extra=extra

;-- create SXI object and store in common for re-use

common plot_latest_sxi,sxi
if not obj_valid(sxi) then sxi=obj_new('sxi')

;-- search and download latest SXI FITS file at NGDC

message,'Searching NGDC...',/cont
sxi->latest,file,err=err,_extra=extra
if err ne '' then return

;-- restore initial plot device in case of error

psave=!d.name
error=0
catch,error
if error ne 0 then begin
 set_plot,psave
 catch,/cancel
 return
endif

;-- plot it


sxi->rotate,roll=0
if not keyword_set(png) then sxi->plot,/log,/noaxes,font=0,grid=0,_extra=extra
if keyword_set(png) then begin
 set_plot,'z',/copy
 device,set_resolution=[512,512]
 sxi->plot,/log,/noaxes,font=0,grid=0,_extra=extra
 data=tvrd()
 tvlct,r,g,b,/get
 break_file,file,dsk,dir,name
 file=dsk+dir+name+'.png'
 write_png,file,data,r,g,b
 message,'Wrote PNG file - '+file,/cont
 set_plot,psave
endif

return & end
