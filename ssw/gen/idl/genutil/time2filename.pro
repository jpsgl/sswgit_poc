function time2filename, times, delimit=delimit, $
   seconds=seconds, year2digit=year2digit, date_only=date_only
;
;   Name: time2filename
;
;   Purpose: convert input times (any SSW format) -> [YY]YYMMDD?HHMM[SS]
;
;   Input Parameters:
;      times - time array , any "standard" format (anytim.pro compatible)
;
;   Output:
;      function returns string array of 'filenames' (time portion)
;
;   Keyword Parameters:
;      delimit -    string delimter between date and time (default='_')
;      seconds -    if set, include SECONDS 
;      year2digit - if set, make year 2 digits (default=YYYY)
;
;   Calling Sequence:
;      filenames=time2filename(timearray )
;
;   Calling Examples:
;  
;   History:
;      6-may-1997 - S.L.Freeland - extract code from dat2files et al.
;-
; force  keyword definition
seconds=keyword_set(seconds)
year2digit=keyword_set(year2digit)

; derive extraction parameters 
first=([0,2])(year2digit)
length=strlen('yyyymmdd_hhmm') + ([0,2])(seconds) - first

; Tactic - replace unwanted characters with blanks and then using 
; strcompress,/remove to efficiently destroy them
strtimes=str_replace(anytim(times,out_style='ecs'),' ','_') ; blank ->"_"
strtimes=str_replace(str_replace(strtimes,'/',' '),':',' ') ; delim ->" "
filenames=strmid(strcompress(strtimes,/remove),first,length)

if data_chk(delimit,/string) then $
   filenames=str_replace(filenames,'_',delimit) else delimit='_'

if keyword_set(date_only) then filenames=strsplit(filenames,delimit,/head)

return, filenames
end
