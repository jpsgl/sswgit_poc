function testcube, n,nx
;
;+
;   Name: testcube
; 
;   Purpose: return byte test data cube (shifted dist)
;
;   Input:
;      n  (opt) - number of 'images' - default=5
;      nx (opt) - size of 'images'   - default=256
;
;
;-
if n_elements(n) eq 0 then n=5
if n_elements(nx) eq 0 then nx=256
im0=bytscl(dist(nx))
shifter=nx/n
ocube=bytarr(nx,nx,n)
for i=0,n-1 do ocube(0,0,i)=shift(im0,shifter*i,shifter*i)
return,ocube
end
