function str2cols, inarr, delim, ncols=ncols, colpos=colpos, trim=trim
;+
;   Name: str2cols
;
;   Purpose: break strarry into columns
;
;   Input Parameters:
;      inarr - string or string array to break
;
;   Optional Keyword Parameters:
;      ncols  - number of columns in output (default is auto at line1 delimiter)
;      colpos - user supplied position breaks (default is via delimiter search)
;      trim   - if set, trim leading and trailing blanks from output
;   
;   History:
;      19-May-1994 (SLF) Written
;       2-Jun-1994 (SLF) call revised remtab if delimiter is blank
;       3-Jun-1994 (SLF) return value order = arr(cols,rows)
;      13-Jun-1994 (SLF) change leading blank handler, add TRIM
;      14-Jun-1994 (SLF) protect against ncols > ndelimiters!
;       9-mar-1996 (SLF) more elegant algorithm (total delimiter bit map 
;			                         along columns)
;
;   Restrictions:
;      delimiter is assumed one character in length
;
;   Method:
;      convert to bytes (makes 2D array)
;      total columns where character=delimiter - if zero, column of delimiters
;-

if not keyword_set(delim) then delim=' '		; blank is default
incol=keyword_set(ncols)				; user defined

if not data_chk(inarr,/defined,/string) then begin	; verify input
   message,/info,"Input must be string or string array"
   return,inarr
endif
inarray=inarr						; protect input
if delim eq ' ' then  remtab,inarray, inarray		; remove tabs

nrows=n_elements(inarray)

; convert null lines to line of delimiters
nulls=where(strlen(strcompress(inarray,/remove)) eq 0,nullcnt)
if nullcnt gt 0 then $
   inarray(nulls)= string(replicate( (byte(delim))(0),max(strlen(inarray) )))

nnulls=where(inarray ne '',nnullcnt)
if nnullcnt eq 0 then begin				; ignore null lines
   nnulls = indgen(nrows)
   nnullcnt = nrows
endif

case 1 of 
   strlen(delim) gt 1: begin
      message,/info,"Delimiter must be 1 character...
      retval=inarray
   endcase
   else: begin
      if not keyword_set(colpos) then begin
;        make a bit map of non-delimiters, then take column totals
;        (total = 0 implies every row has delimiter in that column)
         barray=byte(inarray)      
         dmap=(barray ne (byte(delim))(0))
         colpos=where(total(dmap,2) eq 0 and total(shift(dmap,-1),2) ne 0)
      endif
      
      if colpos(0) eq 0 then cpos=[colpos] else cpos=[0,colpos]
      cpos=cpos
      upos=deriv_arr(cpos) 
      if not keyword_set(ncols) then ncols=n_elements(upos)+1 else $
         if ncols gt n_elements(upos)+1 then ncols=n_elements(upos)+1

      retval=strarr(ncols,nrows)

      for i=0,ncols-2 do begin
         retval(i,*)=strmid(inarray,cpos(i),upos(i))
      endfor
      retval(i,*)=strmid(inarray,cpos(i),max(strlen(inarray)))
   endcase
endcase

; 
if n_elements(retval(*,0)) gt 1 and delim eq ' ' then begin
   notnull=where(strtrim(retval(0,*),2) ne '',nncnt)
   if nncnt eq 0 then begin
      lead_blank=retval(0,*)
      retval(1,*)=retval(0,*) + retval(1,*)
      retval=retval(1:*,*)
   endif
endif 

if delim ne ' ' then begin		; blank out delimiters
   bretval=byte(retval)
   wdelim=where(bretval eq (byte(delim))(0),bcnt)
   if bcnt gt 0 then bretval(wdelim)=32b
   retval=string(bretval)
endif

if keyword_set(trim) then retval=strtrim(retval,2)

return,retval
end   
   
