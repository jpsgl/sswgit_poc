;+
; Project     : HESSI
;
; Name        : GOES_SERVER
;
; Purpose     : return first avaliable GOES data server
;
; Category    : synoptic sockets
;
; Inputs      : None
;
; Outputs     : SERVER = GOES data server name
;
; Keywords    : SDAC - If set, use SDAC archive on UMBRA
;               FULL - If set, return full url
;               NETWORK = returns 1 if network to that server is up
;               PATH = path to data
;
; History     : Written  29-Dec-2001, Zarro (EITI/GSFC)
;               Modified 1-Jun-2005, Zarro (L-3Com/GSFC)
;               - added sdac keyword, and set server to hesperia for sdac.
;               - added full keyword to return full http://name.
;               - don't check sohowww alternate server if sdac is
;                 selected.
;               Modified 15-Nov-2006, Zarro (ADNET/GSFC) 
;               - added UMBRA as primary SDAC server
;               - added PATH return keyword
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function goes_server,_ref_extra=extra,sdac=sdac, full=full, path=path,network=network

;-- primary server

primary='umbra.nascom.nasa.gov'
secondary='hesperia.gsfc.nasa.gov'

primary_path='/goes/fits'
secondary_path='/goes'

server= keyword_set(sdac) ? primary : secondary
path= keyword_set(sdac) ? primary_path : secondary_path
network=have_network(server,_extra=extra)

;-- if primary server is down, try secondary

if not network then begin
 server2= keyword_set(sdac) ? secondary : primary
 path2= keyword_set(sdac) ? secondary_path : primary_path
 network=have_network(server2,_extra=extra)
 if network then begin
  server=server2
  path=path2
 endif
endif

return, keyword_set(full) ? 'http://'+server : server
end
