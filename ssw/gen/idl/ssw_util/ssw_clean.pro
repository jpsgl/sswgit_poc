;+
; Project     : SOHO-CDS
;
; Name        : SSW_CLEAN
;
; Purpose     : clean up SSW path by reorganizing directories
;
; Category    : utility
;
; Explanation : use this to move SUMER and YOHKOH UCON libraries to
;               end of IDL path to avoid potential conflicts.
;
; Syntax      : ssw_clean,new_path
;
; Examples    :
;
; Inputs      : None
;
; Opt. Inputs : None
;
; Outputs     : NEW_PATH = rearranged IDL !path
;
; Opt. Outputs: None
;
; Keywords    : NOSITE = exclude site directories
;               RESET = reset back to original state
;
; Common      : None
;
; Restrictions: None
;
; Side effects: If NEW_PATH not on command line, !path is reset automatically
;
; History     : Written 22 Oct 1997, D. Zarro, SAC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

;----------------------------------------------------------------------------

pro check_ssw_path,dir,libs,dlibs,exclude=exclude
delvarx,dlibs
find_dir=strpos(libs,dir)
if n_elements(exclude) gt 0 then begin
 ex_dir=strpos(libs,exclude)
 have_dir=where((find_dir gt -1) and (ex_dir eq -1),count)
 no_dir=where( (find_dir lt 0) or (ex_dir gt -1),ncount)
endif else begin
 have_dir=where(find_dir gt -1,count)
 no_dir=where(find_dir lt 0,ncount)
endelse

if count gt 0 then dlibs=libs(have_dir)
if ncount gt 0 then libs=libs(no_dir) else delvarx,libs
return & end

;----------------------------------------------------------------------------

pro ssw_clean,new_path,reset=reset,nosite=nosite

common ssw_clean,orig_path

if keyword_set(reset) then begin
 if exist(orig_path) then !path=orig_path
 return
endif else begin
 if not exist(orig_path) then orig_path=!path
endelse

libs=strlowcase(get_lib())

;-- remove path elements

if keyword_set(nosite) then check_ssw_path,'/site',libs
check_ssw_path,'/spartan',libs,spart_libs
check_ssw_path,'/jhuapl',libs,jhu_libs
check_ssw_path,'/sdac',libs,sdac_libs
check_ssw_path,'/astron',libs,astro_libs
check_ssw_path,'/soho/sumer',libs,sumer_libs
check_ssw_path,'/soho/lasco',libs,lasco_libs
check_ssw_path,'yohkoh/ucon',libs,ucon_libs
check_ssw_path,'/rsi',libs,rsi_libs
check_ssw_path,'/packages',libs,pack_libs
check_ssw_path,'/smm',libs,smm_libs,exclude='/smmdac'
check_ssw_path,'/trace/ssw_contributed',libs,trace_cont
check_ssw_path,'/trace/idl',libs,trace_libs
check_ssw_path,'/mdi/idl',libs,mdi_libs
check_ssw_path,'yohkoh',libs,yohkoh_libs,exclude='yohkoh/ucon'
check_ssw_path,'/hessi',libs,hessi_libs
check_ssw_path,'/batse',libs,batse_libs

;-- put path back together

if exist(yohkoh_libs) then libs=[libs,yohkoh_libs]
if exist(batse_libs) then libs=[libs,batse_libs]
if exist(hessi_libs) then libs=[libs,hessi_libs]
if exist(mdi_libs) then libs=[libs,mdi_libs]
if exist(trace_cont) then libs=[libs,trace_cont]
if exist(trace_libs) then libs=[libs,trace_libs]
if exist(rsi_libs) then libs=[libs,rsi_libs]
if exist(astro_libs) then libs=[libs,astro_libs]
if exist(jhu_libs) then libs=[libs,jhu_libs]
if exist(pack_libs) then libs=[libs,pack_libs]
if exist(smm_libs) then libs=[libs,smm_libs]
if exist(sdac_libs) then libs=[libs,sdac_libs]
if exist(ucon_libs) then libs=[libs,ucon_libs]
if exist(lasco_libs) then libs=[libs,lasco_libs]
if exist(sumer_libs) then libs=[libs,sumer_libs]
if exist(spart_libs) then libs=[libs,spart_libs]

new_path=arr2str(libs,delim=':')
if n_params() eq 0 then !path=new_path

return & end
