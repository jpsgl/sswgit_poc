;+
; Project     : HESSI
;                  
; Name        : ALLOW_FONT
;               
; Purpose     : platform/OS independent check if current system
;               supports given font
;                             
; Category    : system utility
;               
; Syntax      : IDL> a=allow_font()
;                                        
; Outputs     : 1/0 if yes/no
;                   
; History     : 14 May 2003, Zarro (EER/GSFC)
;
; Contact     : dzarro@solar.stanford.edu
;-    

function allow_font,font,err=err

err=''

if not is_string(font) then begin
 err='String font name required'
 message,err,/cont
 return,0b
endif

;-- try to open a window

if not allow_windows(/quiet) then return,0b
if not is_wopen(!d.window) then begin
 window,/pix,/free,xsize=1,ysize=1
 wpix=!d.window                                     
endif      
in_font=font
 
error=0
catch,error
if error ne 0 then begin
 catch,/cancel
 err=in_font+' unsupported on current system' 
 message,err,/cont
 wdel,wpix
 return,0b
endif

device,font=in_font
wdel,wpix
return,1b

end
