;+
; Project     : HESSI
;                  
; Name        : SUPP_WIN
;               
; Purpose     : platform/OS independent check if current device
;               supports windows 
;                             
; Category    : system utility
;               
; Syntax      : IDL> a=supp_win()
;                                        
; Outputs     : 1/0 if yes/no
;                   
; History     : Version 1,  4-Nov-1999, Zarro (SM&A/GSFC)
;
; Contact     : dzarro@solar.stanford.edu
;-    

function supp_win,dummy

chk=where(!d.name eq ['X','WIN','MAC'],count)
return,(count gt 0)

end
