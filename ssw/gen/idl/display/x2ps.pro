;+
; Project     : SOHO - CDS
;
; Name        : X2PS
;
; Purpose     : convert X window plot to postscript file
;
; Category    : plotting
;
; Explanation : uses TVREAD
;
; Syntax      : IDL> x2ps,file,windex=windex
;
; Inputs      : FILE = filename to print
;
; Opt. Inputs : FILE - output postscript file name
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    :
;               windex = index of window to be plotted (def = last window)
;               nocolor = for B/W
;               x_size,y_size = size of current window to read (def = whole)
;               print  = send PS file to printer
;               err    = err string
;               delete = delete plot file when done
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Version 1,  1-Sep-1995,  D.M. Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-
 
pro x2ps,file,windex=windex,x_size=x_size,y_size=y_size,_extra=e,$
          nocolor=nocolor,print=print,err=err,delete=delete

on_error,1
err=''
color=1-keyword_set(nocolor)
dev_sav=!d.name
set_plot,'x'

;-- validate plot window

if n_elements(windex) ne 0 then curr_window=windex else curr_window=!d.window

if curr_window gt -1 then begin
 device,window=wind
 clook=where(wind eq 1,count)
 if count gt 0 then begin
  wlook=where(curr_window eq clook,cnt)
  valid_window=(cnt gt 0)
 endif
endif else valid_window=0

if valid_window then wset,curr_window else begin
 err='No valid plot window available'
 message,err,/cont
 return
endelse

if n_elements(file) eq 0 then begin
 file=concat_dir(getenv('HOME'),'idl.ps')
 if not keyword_set(delete) then message,'Postscript file saved in '+file,/cont
endif

;-- defaults

xsize=18
ysize=18
yoff=25.5
bits=8

;-- read window

if n_elements(x_size) eq 0 then x_size=!d.x_size
if n_elements(y_size) eq 0 then y_size=!d.y_size

a=tvrd(0,0,x_size,y_size)

yscale=float(y_size)/float(x_size)
dprint,yscale

;-- output to Postscript

set_plot,'PS',/copy
device,/land,xsize=xsize,ysize=xsize*yscale,yoff=yoff,bits=bits,_extra=e,$
       color=color,file=file
tv,a
device,/close
set_plot,dev_sav

if keyword_set(print) then xprint,file,delete=delete,/confirm

return & end


