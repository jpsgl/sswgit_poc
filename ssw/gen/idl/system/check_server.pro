;+
; Project     : SOHO/CDS
;                   
; Name        : CHECK_SERVER
;               
; Purpose     : check if a server is alive
;               
; Category    : utility
;               
; Explanation : uses 'ping' and 'ftp'
;               
; Syntax      : IDL> check_server,server,alive
;    
; Examples    : 
;
; Inputs      : SERVER = server name (e.g. smmdac.nascom.nasa.gov)
;               
; Opt. Inputs : None.
;               
; Outputs     : ALIVE = 0/1 if dead or alive
;
; Opt. Outputs: None.
;               
; Keywords    : QUIET = turn off messages
;               FTP = check if FTP server is up
;               ERR = error string
;
; Common      : None.
;               
; Restrictions: None.
;               
; Side effects: None.
;               
; History     : 6-Jan-97, Zarro (SAC) - written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-            

pro check_server,server,alive,quiet=quiet,err=err,ftp=ftp

alive=0 & err=''
if datatype(server) ne 'STR' then begin
 err='Invalid server name'
 message,err,/cont
 return
endif

loud=1-keyword_set(quiet)

espawn,'which ping',out
print,out
if strpos(out(0),'not found') gt -1 then ping='/sbin/ping' else ping=out(0)
lping=loc_file(ping,count=count)
if count gt 0 then begin
 cping=ping+' -c 1 '+server
 espawn,cping,out
 out=strlowcase(out)
 chk=where(strpos(out,'1 packets received') gt -1,rpos)
 chk=where(strpos(out,'unknown host') gt -1,upos)

;-- check if server responds by returning packets

 case 1 of
  (rpos gt 0): begin
   if loud then message,server+' is up',/cont
  end
  (upos gt 0): err='check '+server+' name'
  else: err=server+' is down'
 endcase
endif else err='cannot locate "ping"'

if err ne '' then begin
 if loud then message,err,/cont
endif else alive=1

;-- no point going on

if not alive then return

;-- check if ftp works

do_ftp=keyword_set(ftp)
if not do_ftp then return

ftp_com='ftp'
ftp_input=concat_dir(getenv('HOME'),'ftp.input') 
ftp_net=concat_dir(getenv('HOME'),'.netrc')
check=loc_file(ftp_net,count=count)
have_net=count gt 0
openw,unit,ftp_input,/get_lun 
if not have_net then begin
 printf,unit,'anonymous'
 printf,unit,get_user_id()
endif
printf,unit,'quit'
free_lun,unit 

;-- look for likely errors

cmd=ftp_com+' '+server+' < '+ftp_input
espawn,cmd,out
out=strlowcase(out)
chk=where(strpos(out,"connection refused") gt -1,count)
if count gt 0 then err=server+ 'is not accepting connections'

if err eq '' then begin
 chk=where(strpos(out,"not responding") gt -1,count)
 if count gt 0 then err=server+ 'is not responding'
endif

if err eq '' then begin
 chk=where(strpos(out,"unavailable") gt -1,count)
 if count gt 0 then err=server+ 'is unavailable'
endif

if err eq '' then begin
 chk=where(strpos(out,"login failed") gt -1,count)
 if count gt 0 then err=server+ ' ftp-login failed'
endif

if err eq '' then begin
 chk=where(strpos(out(0),"unknown") gt -1,count)
 if count gt 0 then err=server+ ' ftp-login not supported'
endif
  
if err ne '' then begin
 if loud then message,err,/cont
 alive=0
endif else alive=1

rm_file,ftp_input
return & end
