;+
; Project     : SOHO - CDS
;
; Name        :
;	CHKLOG
; Purpose     :
;	Determine actual name of logical or environment variable.
; Explanation :
;	This routine determines the actual name of a logical name (VMS) or
;	environment variable (UNIX).  In VMS the routine TRNLOG,/FULL is used;
;	otherwise GETENV is used.
; Use         :
;	Result = CHKLOG( VAR  [, OS ] )
; Inputs      :
;	VAR = String containing the name of the variable to be translated.
; Outputs     :
;	The result of the function is the translated name, or (in VMS) an array
;	containing the translated names.
; Opt. Outputs:
;       OS = The name of the operating system, from !VERSION.OS.
; Keywords    :
;	DELIM = delimiter to use for separating substrings
;       FULL = do full translation (VMS only)
;       PRESERVE = return input name if no translation found
;       FIX_DELIM = fix slash to match OS-appropriate delimiter
; Category    :
;	Utilities, Operating_system.
; Prev. Hist. :
;       Written  - DMZ (ARC) May 1991
;       Modified - DMZ (ARC) Nov 1992, to use GETENV
; Written     :
;	D. Zarro, GSFC/SDAC, May 1991.
; Modified    :
;	Version 1, Zarro, ARC/GSFC 23 April 1993.
;       Version 2, GSFC, 1 August 1994.
;               Added capability for vector inputs
;       Version 3, Liyun Wang, GSFC/ARC, January 3, 1995
;               Added capability of interpreting the "~" character under UNIX
;       Version 4, Zarro, GSFC/ARC, February 17 1997
;               Added call to EXPAND_TILDE, corrected many potential bugs
;       Version 5, Zarro, GSFC/SAC, June 9 1998
;               Added recursive call for environment variables embedded
;               in input
;       Version 6, Zarro, GSFC/SAC, August 10 1998
;               Added recursive call for nested environment variables
;               (after RAS broke it)
;       Version 7, Zarro, SM&A/GSFC, 16 May 1999
;               Added check for "naked" "$" or "~" inputs
;       Version 8, Zarro, SM&A/GSFC, 10 June 1999
;               Added check for different OS delimiters and made Windows
;               friendly
;       Version 9, 9-Sep-1999, William Thompson, GSFC
;               Fixed bug with TRNLOG,FULL=FULL under version 4 in VMS.
;       Version 10, 14-dec-1999, richard.schwartz@gsfc.nasa.gov
;               Switched get_path_delim to get_delim and allowed
;               both slashes under Windows, '/' and '\'.
;       Version 11, 20-Dec-1999, Zarro
;               Fixed bug when recursing on delimited input
;	Version 12, 07-Mar-2000, William Thompson, GSFC
;		Don't translate terminal logical names in VMS, i.e. those which
;		end in the characters ".]"
;       Version 13, 25-April-2000, Zarro (SM&/GSFC)
;               Added another level of recursion for multiply-defined
;               env's, i.e., env's defined in terms of other env's, which
;               somehow stopped working after version 5.
;               e.g., $SSW_HESSI -> $SSW/hessi/idl -> /ssw/hessi/idl
;       Version 14, 28-Jul-2000, R.D.Bentley (MSSL)
;               Suppress replacement of \\ with \ for windows
;       Version 15, 22-Aug-2000, Zarro (EIT/GSFC)
;               Removed calls to DATATYPE
;       Version 16, 24-July-2002, Zarro (LAC/GSFC)
;               Replaced TRIM with faster TRIM2
;       Version 17, 4-Nov-2002, Zarro (EER/GSFC)
;               Removed checks for multiple delimiters in input
;       Modified, 24 October, 2007, Zarro (ADNET) 
;               - Removed executes and added local_name call
;       Modified, 10-Dec-2008, Kim Tolbert
;               -Fixed bug with windows os switching of
;                forward/backward slash
;       11-Oct-2018, Zarro (ADNET)
;               - Fixed bug with OS delimiter slashes being accidentally
;                 switched
;               - Added /FIX_DELIM for backwards-compatibility with
;                 previous behavior
;               - Removed VMS support and GOTO
;                   
;-                

   function chklog2,var,os,norecurse=norecurse,delim=delim,_extra=extra,$
              preserve=preserve,fix_delim=fix_delim,debug=debug

   preserve=keyword_set(preserve)
   os=os_family(/lower)
   
   if is_blank(var) then begin
    if exist(var) && preserve then return,var else return,''
   endif

   debug=keyword_set(debug)
   if debug then mprint,var
   fix_delim=keyword_set(fix_delim)
   recurse=~keyword_set(norecurse)
   svar=trim2(var)
   nvar=n_elements(svar)
   if nvar eq 1 then svar=getenv2(svar,/preserve)

;-- recurse on array inputs

   if nvar gt 1 then begin
    for i=0,nvar-1 do begin
     out=chklog2(svar[i],os,norecurse=norecurse,delim=delim,preserve=preserve,$
                fix_delim=fix_delim,debug=debug)
     ovar=append_arr(ovar,out,/no_copy)
    endfor
    return,ovar
   endif

;-- recurse on user-specified delimiters
  
   if is_string(delim) then begin
    if strpos(svar,delim) gt -1 then begin
     dvar=str2arr(svar,delim=delim)
     if n_elements(dvar) gt 1 then begin
      out=chklog2(dvar,os,/norecurse,/preserve,fix_dlim=0b,debug=debug)
      ovar=arr2str(out,delim=delim)
      if (ovar eq var) && ~preserve then ovar=''
      if fix_delim then ovar=fix_slash(ovar)
      return,ovar
     endif
    endif
   endif
   
;-- recurse on delimited elements in string

   if recurse then begin
    if stregex(svar,'\\|\/',/bool) then begin
     ovar=svar
     flim='/'
     if strpos(ovar,flim) gt -1 then ovar=chklog2(ovar,os,/norecurse,fix_delim=0b,delim=flim,/preserve,debug=debug)
     flim='\'
     if strpos(ovar,flim) gt -1 then ovar=chklog2(ovar,os,/norecurse,fix_delim=0b,delim=flim,/preserve,debug=debug)
     if (ovar eq var) && ~preserve then ovar=''
     if fix_delim then ovar=fix_slash(ovar)
     return,ovar
    endif
   endif

 ;-- check for preceding $ or ~

   tilde=strpos(svar,'~')
   if tilde eq 0 then ovar=expand_tilde(svar) else $
    ovar=getenv2(svar,preserve=preserve)

;-- fix slashes

   if fix_delim then ovar=fix_slash(ovar)

   return,ovar

   end
        
