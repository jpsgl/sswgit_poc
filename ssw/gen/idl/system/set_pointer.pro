;+
; Project     : SOHO - CDS
;
; Name        : SET_POINTER
;
; Purpose     : to set a pointer value to a pointer variable
;
; Category    : Help
;
; Explanation : assign a pointer value to a pointer variable.
;
; Syntax      : IDL> set_pointer,pointer,value
;
; Inputs      : POINTER = pointer variable
;             : VALUE = value to assign
;
; Opt. Inputs : None
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : NOCOPY   -  do not make internal copy of value
;
; Common      : None
;
; Restrictions: POINTER must be defined via MAKE_POINTER
;
; Side effects: external value of POINTER is removed when /NO_COPY set
;
; History     : Version 1,  1-Sep-1995,  D.M. Zarro.  Written
;               Version 2, 17-Jul-1997, D.M. Zarro. Modified
;                 -- Updated to version 5 pointers 
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro set_pointer,pointer,value,no_copy=no_copy

if not exist(value) then begin
 message,'SYNTAX --> set_pointer,pointer,value,[/no_copy]',/cont
 return
endif

;-- check if pointer is valid 

valid=valid_pointer(pointer,type)
if valid then begin
 case type of
  0: widget_control,pointer,set_uvalue=value,no_copy=keyword_set(no_copy)
  1: handle_value,pointer,value,no_copy=keyword_set(no_copy),/set
  2: begin
      if keyword_set(no_copy) then arg='temporary(value)' else arg='value'
      express='*pointer='+arg 
      status=execute(express)
     end
 else: do_nothing=1
 endcase
endif else dprint,'%SET_POINTER: input pointer is invalid'

return & end
