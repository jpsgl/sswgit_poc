PRO SCREEN_SELECT, selections, iselected, comments, command_line, only_one
;+
; Project     :	SOHO - CDS
;
; Name        :	
;	SCREEN_SELECT
;
; Purpose     :	Interactively select from list using widgets, X or terminal
;
; Explanation :	
;	Routine to allow a user to make an interactive screen selection
;	from a list (array) of strings.  This procedure determines whether
;	to use dumb terminal version, the non- widget x-windows version or 
;	the widget version by examining the !D.NAME system variable.
;
;	The actual processing is farmed out to different procedures depending
;	on the terminal type.    
;
;	Widget Terminal   ==>  SELECT_W.PRO
;	Vanilla X windows ==>  SELECT_X.PRO 
;	VT100 Terminal  ==>    SELECT_O.PRO
;
; Use         :	
;	screen_select, selections, iselected, comments, command_line, only_one
;
; Inputs      :	
;	selections - string array giving list of items that can be
;		selected.
;
; Opt. Inputs :	
;	comments - comments which can be requested for each item in
;		array selections.  It can be:
;			string array - same length as array selections.
;			null string - no comments available
;			scalar string - name of a procedure which will
;				return comments.  It will take selections
;				as its first argument and return comments
;				as its second argument.
;	command_line - optional command line to be placed at the bottom
;		of the screen.  It is usually used to specify what the
;		user is selecting.
;	only_one - integer flag. If set to 1 then the user can only select
;		one item.  The routine returns immediately after the first
;		selection is made.
;
; Outputs     :	
;	iselected - list of indices in selections giving the selected
;		items.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Calls       :	SELECT_O, SELECT_W, SELECT_X
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	
;	!err is set to the number of selections made
;
; Category    :	Utilities, User_interface
;
; Prev. Hist. :	
;	Written by M. Greason, STX, May 1990.
;       Added widget support    W. Landsman           January, 1992
;
; Written     :	M. Greason, GSFC/UIT (STX), May 1990
;
; Modified    :	Version 1, William Thompson, GSFC, 29 March 1994
;			Incorporated into CDS library
;		Version 2, Anonymous, 4 November 1994
;			Remove X window but no widget option
;
; Version     :	Version 2, 4 November 1994
;-
;
;--------------------------------------------------------------------------
;			Set defaults.
;
if N_params() LT 3 then comments = ''
if N_params() LT 4 then command_line = ''
if N_params() LT 5 then only_one = 0
;
;			If no selection array or output array has been 
;			supplied, set iselected to -1 and !err to 0.
;			Then quit.
;
 if N_params() LT 2 then begin
	!ERR = 0
	iselected = -1
 endif else begin
;
;			Determine which procedure to farm the work out
;			to depending upon the contents of !d.name and !D.flags
;
   if (!D.FLAGS and 65536) EQ 65536 then $       ;Widgets?

      select_w, selections,iselected, comments, command_line, only_one $

   else  $                                       ;Dumb Terminal?

      select_o, selections, iselected, comments, command_line, only_one

 endelse
;
 return
 end
