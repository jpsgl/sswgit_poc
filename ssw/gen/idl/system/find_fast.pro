;+
; Project     : SOHO - CDS
;
; Name        : FIND_FAST
;
; Purpose     : fast find of images based on date code
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : IDL> find_fast,tstart,tend,files
;
; Inputs      : TSTART = search start time
;               TEND   = search end time
;
; Opt. Inputs : None
;
; Outputs     : FILES = found files (rounded to nearest day)
;
; Opt. Outputs: None
;
; Keywords    : EXT = extension to search for (def = '.gif')
;               INDIR = root directory name to search
;               COUNT = # of files found
;               PATTERN = special pattern to search for (def = '*')
;
; Common      : None
;
; Restrictions: Unix systems only
;
; Side effects: None
;
; History     : Version 1,  14-April-1999,  D.M. Zarro (SM&A/GSFC),  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro find_fast,t1,t2,files,pattern=pattern,$
  indir=indir,outdir=outdir,count=count,ext=ext,verbose=verbose

err=''

if n_params() lt 3 then begin
 pr_syntax,'find_fast,tstart,tend,files'
 return
endif

tstart=anytim2utc(t1,err=err)
if err ne '' then begin message,err,/cont & return & endif

tend=anytim2utc(t2,err=err)
if err ne '' then begin message,err,/cont & return & endif

;-- set defaults


verbose=keyword_set(verbose)
if not data_chk(ext,/string) then ext='gif'
if not data_chk(pattern,/string) then pattern='*'
if not data_chk(indir,/string) then indir=concat_dir('$SUMMARY_DATA',ext)
if not data_chk(outdir,/string) then outdir='/tmp/+ext

;-- look for starting/end directories to search


count=0
delvarx,files
dstart=tstart.mjd
dend=tend.mjd
last_temp=''
wild=strpos(pattern,'*') gt -1
get_utc,cur_utc
for i=dstart,dend do begin
 if i gt cur_utc.mjd then return
 temp=date_code({mjd:long(i),time:0l})
 if temp ne last_temp then begin
  if verbose then message,'searching '+temp,/cont
  if strpos(temp,'19') eq 0 then temp=strmid(temp,2,strlen(temp))
  search_dir=concat_dir(indir,temp)
  if wild then search_file=pattern+'.'+ext else search_file='*'+pattern+'*.'+ext
  v=loc_file(concat_dir(search_dir,search_file),count=vcount)
  if vcount gt 0 then begin
   files=append_arr(files,v)
   count=n_elements(files)
  endif
 endif
 last_temp=temp
endfor

return & end



