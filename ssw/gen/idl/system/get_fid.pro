;+
; Project     : HESSI
;
; Name        : GET_FID
;
; Purpose     : determine YYMMDD names based on date/time 
;
; Category    : utility io 
;
; Syntax      : rdir=get_fid(tstart,tend)
;
; Inputs      : TSTART/TEND = start/end times to base search
;               e.g. TSTART = 10-may-99 -> 990510
;                    TEND   = 20-dec-99 -> 991220
;
; Outputs     : Array directory names 
;
; Keywords    : NODAY = exclude day from output
;               FULL= include full year in output
;
; History     : Written 6 Jan 1999, D. Zarro (SM&A/GSFC)
;
; Contact     : dzarro@solar.stanford.edu
;-

function get_fid,tstart,tend,noday=noday,full=full

dstart=get_def_times(tstart,tend,dend=dend,/ext)

sdir=mk_fid(dstart,full=full,noday=noday)
edir=mk_fid(dend,full=full,noday=noday)

jdate=dstart
mstart=anytim2utc(dstart)
i=0
while ((where(edir eq sdir))(0) eq -1) do begin
 mdate=anytim2utc(jdate)
 i=i+1
 mdate.mjd=mstart.mjd+i
 jdate=anytim2utc(mdate,/ext)
 dir=mk_fid(jdate,full=full,noday=noday)
 skip=0
 if keyword_set(noday) then begin
  np=n_elements(sdir)
  skip=dir eq sdir(np-1) 
 endif
 if not skip then sdir=append_arr(sdir,dir)
endwhile

return,sdir & end

