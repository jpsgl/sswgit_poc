;+
; Project     : SOHO-CDS
;
; Name        : FILE2FID
;
; Purpose     : move files from a directory into another based on file 
;               date/time               
;
; Category    : planning
;
; Explanation : looks at files encoded with "_yyyymmdd_hhmm"
;               and moves them into appropriate directory
;               e.g. out_dir/yymmdd
;
; Syntax      : file2fid,files,out_dir
;
; Examples    :
;
; Inputs      : FILES = array of filenames, complete with path
;               OUT_DIR = target directory for files
;
; Opt. Inputs : None
;
; Outputs     : 
;
; Opt. Outputs: None
;
; Keywords    : COPY = copy files instead of move
;               NOCLOBBER = don't clobber existing files 
;
; Common      : None
;
; Restrictions: Unix only 
;
; Side effects: None
;
; History     : Written 1 Feb 1999 D. Zarro, SM&A/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

pro file2fid,files,out_dir,copy=copy,noclobber=noclobber,err=err

on_error,1

err=''
                 
if os_family() ne 'unix' then begin
 message,'sorry, UNIX only',/cont
endif

if (datatype(files) ne 'STR') then begin
 pr_syntax,'file2fid,files,out_dir,[copy=copy,noclobber=noclobber]'
 return
endif

;-- output directory
                 
if datatype(out_dir) ne 'STR' then out_dir=curdir() 

;-- check write access

if not test_open(out_dir,err=err,/write) then return

;-- now do the real work

clobber=1-keyword_set(noclobber)
if keyword_set(copy) then cmd='cp ' else cmd='mv -f '
nf=n_elements(files)
for i=0,nf-1 do begin
 time=fid2time(files(i),ymd=ymd,err=err)

 if (err eq '') and (ymd ne '') then begin

;-- ensure target directory is created

  sub_dir=concat_dir(out_dir,ymd)
  if not chk_dir(sub_dir) then spawn,'mkdir -p '+sub_dir

;-- check if copy of file exists there

  break_file,files(i),dsk,dir,fname,ext
  target=concat_dir(sub_dir,fname+ext)
  chk_target=loc_file(target,count=fcount)

;-- move there if clobber or file isn't there

  if clobber or (fcount eq 0) then spawn,cmd+files(i)+' '+sub_dir

;-- ensure read/write access for everyone
  
  spawn,'chmod -R ug+w '+sub_dir 
 endif

endfor

return & end

