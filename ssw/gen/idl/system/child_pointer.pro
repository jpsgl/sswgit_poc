;+
; Project     : SOHO - CDS
;
; Name        : CHILD_POINTER()
;
; Purpose     : to retrieve child of a pointer variable
;
; Category    : Help
;
; Explanation : use appropriate WIDGET or HANDLE info routines
;
; Syntax      : IDL> child=child_pointer(pointer)
;
; Inputs      :  POINTER = pointer variable
;
; Opt. Inputs : None
;
; Outputs     : CHILD = child ID of pointer
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Version 1,  1-Sep-1995,  D.M. Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function child_pointer,pointer

on_error,1

;-- check if an unrealized widget id is being used as a pointer

if xalive(pointer) then begin
 if not widget_info(pointer,/realized) then return,widget_info(pointer,/child)
endif

;-- if we got this far, then it's probably a handle

if exist(pointer) then begin
 if call_function('handle_info',pointer,/valid_id) then $
  return,call_function('handle_info',pointer,/first_child)
endif

return,0 & end
