;+
; PROJECT:
;	SDAC
; NAME:
;	WIN_SPAWN
;
; PURPOSE:
;	This procedure allows SPAWN to return results under WINdows.
;
; CATEGORY:
;	SYSTEM, WINDOWS
;
; CALLING SEQUENCE:
;	WIN_SPAWN, Command, Result
;;
; INPUTS:
;       Command- Set of DOS commands.
;
; KEYWORDS INPUTS:
;	TEMP_FILE - file to overwrite with text output of command.
;	The default location is c:/windows/temp/spawn_results.txt
;       DELETE = set to delete temporary file when done
; KEYWORD OUTPUTS:
;	COUNT - number of lines in result
;
; PROCEDURE:
;	This procedure spawns and saves the results to a local file which
;	is subsequently read back to make the results available in the same
;	way spawn can return results under Unix and VMS.
;
; MODIFICATION HISTORY:
;	Version 1. richard.schwartz@gsfc.nasa.gov, 8-Jun-1998
;	Version 2. Kim Tolbert, 18-Aug-1998 - Use TMP env. variable
;       Version 3. Zarro (SM&A/GSFC), 12-Nov-1999 - added /DELETE and a 
;       CONCAT_DIR
;       Version 4. Zarro (SM&A/GSFC), 17-March-2000 - added some input
;       error checking as well as check if return result requested.
;       Also, added a random number to TEMP_FILE to avoid collisions
;       if more than one copy of program runs simultaneously, or TEMP_FILE
;       already exists
;       Version 5. Zarro (SM&A/GSFC), 23-March-2000
;       added spawning a batch file using START /min /b /high.
;       On some systems, these switches "may" inhibit the annoying shell window
;-

pro win_spawn2, command, result, count=count, temp_file=temp_file,$
               delete=delete

count=0 & result=''
if strlowcase(os_family()) ne 'windows' then return
if datatype(command) ne 'STR' then return

;-- temporary directory to stash files

tempdir = get_temp_dir()

;-- create and spawn a batch file using START 

bat_file=mk_temp_file('win_spawn_'+get_rid()+'.bat',direc=tempdir)
file_append,bat_file,[commmand,'exit'], /new

;-- capture output in temp_file

checkvar, temp_file, concat_dir(tempdir,'spawn_result.txt_'+get_rid())

spawn,'start /b /min /high /wait '+bat_file+'  > '+temp_file

;-- wait until output is ready

rcount=0
repeat begin
 chk=test_open(temp_file)
 rcount=rcount+1
 too_long=rcount gt 100
endrep until (chk or too_long)

result=rd_ascii(temp_file)
count=n_elements(result)

result_out=n_params() eq 2
if not result_out then print,result

;-- clean up

rm_file,bat_file
if keyword_set(delete) then rm_file,temp_file

return
end
