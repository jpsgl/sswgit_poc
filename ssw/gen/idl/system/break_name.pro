;+
; Project     : HESSI
;                  
; Name        : BREAK_NAME
;               
; Purpose     : new improved BREAK_FILE uses STREGEX
;                             
; Category    : system utility string
;               
; Syntax      : IDL> name=break_name(file)
;
; Inputs:     : FILE = file names (scalar or array) 
;
; Outputs     : Name part of file
;
; Keywords    : PATH = path part of file 
;               EXT  = extension part of file
;               
; Restrictions: Needs IDL version > 5.3
;               
; History     : Written,11-Nov-2002, Zarro (EER/GSFC)
;
; Contact     : dzarro@solar.stanford.edu
;-    

function break_name,file,path=path,ext=ext

sz=size(file)
dtype=sz(n_elements(sz)-2)
if dtype ne 7 then begin
 path=''
 ext=''
 return,''
endif

;-- known delimiters

delim='(\\|/|:| )' 

;-- construct regular expression to search for last part of string 
;   that doesn't have any delimiters or spaces. Part before name must be path.
;   

regex='[^'+delim+']+$'
name=stregex(file,regex,/ext)

;-- extract extension

if arg_present(ext) then ext=stregex(file,'\.'+regex,/ext)

;-- extract path

if arg_present(path) then begin
 nfile=n_elements(file)
 spos=abs(stregex(file,regex))
 path=strmid(file,0,spos)
 index=lindgen(nfile)*(nfile+1)
 path=path[index]
endif 

return,name

end

