;+
; Project     : SOHO - SUMER
;
; Name        : GET_FILE_TIME
;
; Purpose     : Infer file time from file name with "_yymmdd_hhmmss"
;
; Category    : I/O
;
; Explanation : Same as EXTRACT_FID, but sets missing elements to 0
;
; Syntax      : IDL> times=get_file_time(files)
;
; Inputs      : FILES = file names (e.g. sum_961020_201023.fits)
;
; Opt. Inputs : None
;
; Outputs     : TIMES = string times (e.g. 20-Oct-96 20:10:23)
;
; Opt. Outputs: None
;
; Keywords    : DELIM = time delimiter (def= '_')
;               TAI = return time in TAI format
;               FID = return yymmdd
;               
; Common      : None
;
; Restrictions: Not vectorized
;
; Side effects: None
;
; History     : Version 1,  20-May-1998, Zarro (SM&A) - written
;               Version 2,  1-Feb-1999, Zarro - made Y2K compliant
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-
;

function get_file_time,files,delim,tai=tai,fid=fid,err=err

on_error,1
err=''

if datatype(files) ne 'STR' then begin
 pr_syntax,'times=get_file_time(files,[delim,tai=tai])'
 return,''
endif

nf=n_elements(files)
times=strarr(nf)
fid=strarr(nf)
if keyword_set(tai) then times=dblarr(nf) 
mons=['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec']

if datatype(delim) ne 'STR' then delim='_'
blim=byte(delim)
for i=0,nf-1 do begin
 break_file,files(i),fdsk,fdir,fname,fext
 temp=byte(fname)
 nt=n_elements(temp)
 chk=where(blim(0) eq temp,count)
 if count gt 0 then begin
  if count gt 2 then f1=chk(count-2) else f1=chk(0)
  if ((f1+1) lt nt) and ((f1+2) lt nt) then yy=string(temp(f1+1:f1+2)) else yy=''
  if ((f1+3) lt nt) and ((f1+4) lt nt) then mm=string(temp(f1+3:f1+4)) else mm=''             
  if ((f1+5) lt nt) and ((f1+6) lt nt) then dd=string(temp(f1+5:f1+6)) else dd='00'

;-- Y2K correction

  if (yy eq '19') or (yy eq '20') then begin
   yy=mm & mm=dd 
   if ((f1+7) lt nt) and ((f1+8) lt nt) then dd=string(temp(f1+7:f1+8)) else dd='00'
  endif

;-- use 50 as pivot year

  if strlen(yy) eq 2 then begin
   if (yy ge '50') and (yy le '99') then full_yy='19'+yy else full_yy='20'+yy
  endif

  hh='00' & min='00' & ss='00'
  if count gt 1 then begin   
   if count gt 2 then f2=chk(count-1) else f2=chk(1)
   if ((f2+1) lt nt) and ((f2+2) lt nt) then hh=string(temp(f2+1:f2+2)) 
   if ((f2+2) lt nt) and ((f2+4) lt nt) then min=string(temp(f2+3:f2+4)) 
   if ((f2+5) lt nt) and ((f2+6) lt nt) then ss=string(temp(f2+5:f2+6)) 
  endif
  mon=fix(mm)
  if (mon ge 1) and (mon le 12) and (yy ne '') and (dd ne '') then begin
   string_mon=mons(mon-1)
   time=dd+'-'+string_mon+'-'+full_yy+' '+hh+':'+min+':'+ss
   err=''
   fid(i)=yy+mm+dd
   if keyword_set(tai) then ftime=anytim2tai(time,err=err) else $
    ftime=anytim2utc(time,err=err,/vms) 
   if err eq '' then times(i)=ftime
  endif
 endif
endfor

nf=n_elements(times)
if nf eq 1 then begin 
 times=times(0)
 fid=fid(0)
endif

return,times & end


