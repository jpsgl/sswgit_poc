;+
; Project     : HESSI
;
; Name        : FREE_VAR
;
; Purpose     : free variable memory (pointer, objects, structures)
;
; Category    : utility objects
;
; Syntax      : IDL> free_var,var
;
; Inputs      : VAR = any type of IDL variable (scalar or array)
;
; Keywords    : DELETE = set to delvarx variable
;
; History     : Written 28 May 2000, D. Zarro, EIT/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-       

pro free_var,var,delete=delete

np=n_elements(var)
if np eq 0 then return
dtype=datatype(var)
for i=0,np-1 do begin
 
 case 1 of
  dtype eq 'OBJ': if obj_valid(var(i)) then obj_destroy,var(i)

;-- recurse on structure tags

  dtype eq 'STC': begin
   tags=tag_names(var(i))
   nt=n_elements(tags)
   for j=0,nt-1 do free_var,var(i).(j)
  end  

;-- recurse on pointer values

  dtype eq 'PTR' : begin
   if ptr_valid(var(i)) then free_var,*var(i)
   ptr_free,var(i)
  end 

  else: do_nothing=1
 endcase

endfor

;-- finally remove variable

if keyword_set(delete) then delvarx,var

return & end
