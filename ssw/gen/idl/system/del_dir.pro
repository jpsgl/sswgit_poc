;+
; Project     : HESSI
;                  
; Name        : DEL_DIR
;               
; Purpose     : wrapper around FILE_DELETE that catches errors
;                             
; Category    : system utility
;               
; Syntax      : IDL> del_dir,dir
;
; Inputs      : DIRS= directory string names
;                                        
; Outputs     : None
;
; Keywords    : RECURSIVE = recurse on directories [def]
;                   
; History     : 10-Jan-2019, Zarro (ADNET) - written
;
; Contact     : dzarro@solar.stanford.edu
;-    

pro del_dir,dir,_extra=extra,err=err,$
          recursive=recursive,allow_nonexistent=allow_nonexistent,quiet=quiet

do_recursive=1b
if is_number(recursive) then do_recursive=fix(recursive) ne 0
allow_nonexistent=keyword_set(allow_nonexistent)
quiet=keyword_set(quiet)

err=''
if is_blank(dir) then begin
 err='Blank directory name entered.'
 mprint,err
 return
endif

for i=0,n_elements(dir)-1 do begin
 error=0
 catch,error
 if error ne 0 then begin
  err=err_state()
  mprint,err
  message,/reset
  catch,/cancel
  continue
 endif

 dname=chklog(dir[i],/pre)
 if is_blank(dname) then continue
 if ~is_dir(dname) then begin
  if ~allow_nonexistent && ~quiet then mprint,'Non-existent directory - '+dname
  continue
 endif
 if do_recursive then begin
  path=concat_dir(dname,'*')
  rdirs=file_search(path,count=rcount,/test_directory)
  if rcount gt 0 then del_dir,rdirs,_extra=extra,$
   recursive=do_recursive,allow_nonexistent=allow_nonexistent,quiet=quiet,err=err
 endif
 file_delete,dname,_extra=extra,recursive=do_recursive,allow_nonexistent=allow_nonexistent,quiet=quiet
endfor

return & end
