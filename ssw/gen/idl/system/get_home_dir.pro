;+
; Project     : RHESSI
;
; Name        : GET_HOME_DIR
;
; Purpose     : Return OS dependent HOME directory
;
; Category    : OS utility 
;
; Syntax      : IDL> home=get_home_dir()
;
; Inputs      : None
;
; Outputs     : Expanded equivalent of users directory
;
; Keywords    : None
;
; History     : 10-Feb-2018 Zarro (ADNET) - written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function get_home_dir,_ref_extra=extra

windows=os_family(/lower) eq 'windows'
if windows then return,get_windows_home(_extra=extra) 

out=chklog(['~','HOME','$HOME'])
for i=0,n_elements(out)-1 do if is_string(out[i]) then return,out[i]

return,''
end
