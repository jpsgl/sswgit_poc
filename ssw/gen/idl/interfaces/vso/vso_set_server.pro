;+
; Project     : HESSI
;
; Name        : VSO_SET_SERVER
;
; Purpose     : Set VSO PrepServer
;
; Category    : synoptic sockets VSO
;
; Inputs      : SERVER = server to select (WILCO or HESPERIA)
;
; History     : Written 24-July-2010, Zarro (ADNET)
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro vso_set_server,server

if is_blank(server) then return
vserver=strupcase(strtrim(server,2))
if vserver eq 'WILCO' then vserver='wilco.gsfc.nasa.gov'
if vserver eq 'HESPERIA' then vserver='hesperia.gsfc.nasa.gov'

mklog,'vso_prep_server',vserver

return & end
