;-- Unit test for VSO_PREP 

cd,current=curr
if ~write_dir(curr) then begin
 message,'Needs write access to current directory.',/cont
 return
endif

journal,'trace_test.dat'
qsave=!quiet
!quiet=1
message,!stime,/cont
help,/st,!version

message,'Running TRACE test 1 with URL file name...',/cont

file_delete,'test1.fits',/quiet
file='http://sdac2.nascom.nasa.gov/data/trace/week20080323/tri20080325.1800'
vso_prep,file,image=0,out='test1.fits',inst='trace'

chk=loc_file('test1.fits',count=count)
if count eq 0 then begin
 message,'Test 1 failed.',/cont
 vso_debug
endif else message,'Test 1 succeeded.',/cont

message,'Running TRACE test 2 with local file name...',/cont
file_delete,'test2.fits',/quiet

file='http://sdac2.nascom.nasa.gov/data/trace/week20080323/tri20080325.1800'
sock_copy,file,local=local
vso_prep,local,image=0,out='test2.fits',inst='trace'

chk=loc_file('test2.fits',count=count)
if count eq 0 then begin
 message,'Test 2 failed.',/cont
 vso_debug 
endif else message,'Test 2 succeeded.',/cont

message,'Journal file saved in trace_test.dat.',/cont

journal
!quiet=qsave
end
