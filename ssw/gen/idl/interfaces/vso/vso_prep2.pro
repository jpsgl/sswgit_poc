;+
; Project     : VSO
;
; Name        : VSO_PREP
;
; Purpose     : Wrapper around VSO PREP object
;
; Category    : utility sockets
;
; Example     : IDL> vso_prep,file
;
; Inputs      : FILE = input file to process (with optional URL path)
;
; Keywords    : STATUS= 1/0 for success or failure
;               OUTFILE = prepped output filename
;               INSTRUMENT = optional instrument [in case can't get from FILE]
;               ERR = error messages
;               OPREP = object with prepped data
;
; History     : Written 31-March-2009, D.M. Zarro (ADNET)
;               15-Sept-2009, Zarro (ADNET) 
;               - removed INST input (made it optional keyword)
;               - added more error checking and messaging
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro vso_prep2,file,outfile=outfile,_extra=extra,$
            status=status,err=err,instrument=instrument,oprep=oprep

status=0b
error=0
catch,error
if error ne 0 then begin
 catch,/cancel
 message,err_state(),/cont
 status=0b
 if obj_valid(vsoprep) then obj_destroy,vsoprep
 if obj_valid(request) then obj_destroy,request
 if obj_valid(otemp) then obj_destroy,otemp
 return
endif

if is_blank(file) and is_blank(instrument) then begin
 pr_syntax,'vso_prep,file [,outfile=outfile]'
 return
endif

;-- determine instrument

if is_string(file) then begin
 inst=get_fits_inst(file,prepped=prepped)
 if is_blank(inst) and is_string(instrument) then inst=strupcase(instrument)

 if is_blank(inst) then begin
  err='Could not determine instrument.'
  message,err,/cont
  return
 endif

 if prepped then begin
  message,file+' already prepped.',/cont
  return
 endif
endif else begin
 if is_blank(inst) and is_string(instrument) then inst=strupcase(instrument)
endelse

vso_prep_list=['EIT','TRACE','EUVI','XRT','RHESSI']
chk=where(inst eq vso_prep_list,count)
if count eq 0 then begin
 err='Prepping not currently supported for '+inst+' data.'
 message,err,/cont
 return
endif

;-- default output prepped file to current directory

ofile='temp.fits'
if is_string(outfile) then ofile=outfile else $
 if is_string(file) then ofile='prepped_'+file_break(file) 
file_delete,ofile,/quiet

;-- load Java files

vso_startup, status=status
if ~status then begin
 err='Problem loading IDL-Java bridge.'
 message,err,/cont
 return
endif

;-- check if preselecting image records

otemp=obj_new(inst)
if have_method(otemp,'preselect') then otemp->preselect,file,image_no

;-- create new VSO PREP object

status=0b
vsoprep = OBJ_NEW('IDLJavaObject$STATIC$GOV_NASA_GSFC_JIDL_VPS_CLIENT_PREPROCESSORCLIENTREQUEST', 'gov.nasa.gsfc.jidl.vps.client.PreprocessorClientRequest')
if ~obj_valid(vsoprep) then begin
 err='Error creating VSO PREP object.'
 message,err,/cont
 return
endif

;-- create request object

request=vsoprep->newrequest(inst)
;if is_url(file) then rfile=file else rfile = request->uploadData(file)
request->addData,file
if exist(image_no) then request->addparameter,'image_no',image_no

;-- pass command line prep keywords

if is_struct(extra) then begin
 names=tag_names(extra)
 for i=0,n_elements(names)-1 do request->addparameter,names[i],extra.(i)
endif

;-- send to prep server

message,'Prepping '+inst+' data...',/cont

request->preprocess

;-- download prepped file

result=request->getData()
if is_string(result) then begin
 sock_copy,result,ofile,_extra=extra
 chk=file_search(ofile,count=count)
endif else count=0

if count gt 0 then begin
 message,'Prepping completed successfully.',/cont 
 message,'Wrote prepped data to - '+ofile,/cont
 status=1b 

;-- return prepped object

 if arg_present(oprep) then begin
  if obj_valid(oprep) then obj_destroy,oprep
  oprep=otemp
  oprep->read,ofile
 endif
endif else begin
 err='Prepping failed.'
 message,err,/cont
 obj_destroy,otemp
endelse

;-- cleanup

obj_destroy,vsoprep
obj_destroy,request


return & end
