;+
; Project     : VSO
;
; Name        : LOCATE_JAVA
;
; Purpose     : LOCATE_JAVA searches a sequence of pre-defined paths for a folder containing the Java Virtual Machine binary.
;               This JVM binary is required by the IDL-Java Bridge for it to work. Before returning, this routine will set
;	              the user environment variable IDLJAVAB_LIB_LOCATION to the JVM folder (if it was found).
;	              NOTICE: You have to call this routine before calling any other routine that depends on the IDL-Java Bridge!
;			                  Otherwise the bridge cannot initialize.
;
; Category    : VSO, PrepServer
;
; Example     : IDL> locate_java
;
; Inputs      : N/A
;
; Keywords    : VERBOSE, activate verbose mode
;               FORCE, search and set library even if IDLJAVAB_LIB_LOCATION might have been set already
;
; History     : 15-Jun-2009  L. I. Etesi (CUA,FHNW/GSFC), Initial release
;             : 16-Jun-2009  L. I. Etesi (CUA,FHNW/GSFC), Changed documentation, added VERBOSE and FORCE arguments
;
; Contact     : LASZLO.ETESI@GSFC.GOV
;-

PRO locate_java, status=status, verbose=verbose, force=force

status = 1

IF STRLOWCASE(!VERSION.OS) NE 'linux' THEN RETURN

; Configure search path
; Linux:
searchfolder = ['/usr/java', '/usr/lib/java', '/usr/lib/jvm', '/usr']
searchlib = 'libjvm.so'

IF NOT KEYWORD_SET(force) THEN BEGIN
  IF is_string(GETENV('IDLJAVAB_LIB_LOCATION')) THEN BEGIN
    oldjava = GETENV('IDLJAVAB_LIB_LOCATION')
    
    IF file_exist(oldjava + '/' + searchlib) THEN RETURN
  ENDIF
ENDIF

; Determine Java version
SPAWN, 'java -version', stdout, stderr

IF exist(stderr) THEN cout = stderr
IF NOT exist(cout) AND exist(stdout) THEN cout = stdout

IF NOT exist(cout) THEN BEGIN
  print, 'Could not read Java version. Abort'
  status = 0
  return
ENDIF

jversion1 = STRSPLIT(cout[0], '"', /EXTRACT)
jversion1 = jversion1[1]

; Sub-versions of Java are indicated by an underscore. On the file system however, this underscore is often
; represented by a point.
IF (STRPOS(jversion1, '_') GT -1) THEN BEGIN
  jversion2 = str_replace(jversion1, '_', '.')
ENDIF

; Search the folder hierarchy for Java home
n = N_ELEMENTS(searchfolder)

; Go through all possible folders specified above
FOR i = 0, n-1 DO BEGIN
  IF file_exist (searchfolder[i]) THEN BEGIN
  
    IF KEYWORD_SET(verbose) THEN  print, 'Searching ' + searchfolder[i] + ' for JVM (' + searchlib + ')'
    
    res = FILE_SEARCH(searchfolder[i], searchlib)
    
    IF is_string(res) THEN BEGIN
      nfound = N_ELEMENTS(res)
      
      IF KEYWORD_SET(verbose) THEN print, 'Found ' + STRCOMPRESS(STRING(nfound), /REMOVE_ALL) + ' versions of ' + searchlib + ' in ' + searchfolder[i]
      
      ; If found candidates, search for 'client' and suitable Java version
      FOR j = 0, nfound-1 DO BEGIN
      
        IF KEYWORD_SET(verbose) THEN print, 'Checking ' + res[j]
        foundclient = STRPOS(res[j], 'client')
        foundjversion = STRPOS(res[j], jversion1)
        
        ; If jversion1 (underscore) did not yield a match, try jversion2 (point)
        IF (foundjversion LT 0) AND is_string(jversion2) THEN BEGIN
          foundjversion = STRPOS(res[j], jversion2)
        ENDIF
        
        ; Exit if found a valid JVM  
        IF (foundclient GT -1) AND (foundjversion GT -1) THEN BEGIN
          found = res[j]
          BREAK
        ENDIF
        
      ENDFOR
            
    ENDIF
    
  ENDIF
  
  ; Exit if found a valid JVM
  IF is_string(found) THEN BREAK
  
ENDFOR

; Print appropriate message
IF is_string(found) THEN BEGIN

  jdir = FILE_DIRNAME(found)

  setenv, 'IDLJAVAB_LIB_LOCATION=' + jdir

  print, '*'
  print, '* IDLJAVAB_LIB_LOCATION=' + jdir
  print, '*'
  print, '* Add the following line to your IDL startup script to activate the IDL-Java Bridge permanently:'
  print, '* setenv IDLJAVAB_LIB_LOCATION ' + jdir
  print, '*'
  print, '* NOTICE: Should the IDL-Java Bridge fail to initialize, restart your IDL session (no .reset!) and execute this'
  print, '*         script before any other IDL-Java dependent routines!'
  print, '*'

ENDIF ELSE BEGIN

  print, '*'
  print, '* Could not locate JVM on your system. You do not appear to'
  print, '* have Java installed. Please talk to your system administrator'
  print, '* or go to http://java.sun.com to download and install Java.'
  print, '*'
  status = 0
  
ENDELSE

END
