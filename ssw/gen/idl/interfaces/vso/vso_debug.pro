
;-- IDL-Java Bridge debugger

pro vso_debug

oJSession = OBJ_NEW('IDLJavaObject$IDLJAVABRIDGESESSION')
oJExc = oJSession->GetException()
oJExc->PrintStackTrace

obj_destroy,[ojsession,ojexc]

return & end
