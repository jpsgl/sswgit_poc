;+
; :Author: László István Etesi
; 
; TODO Add asserts for all the in-parameters
;-

;+
; :Description:
;    Client interface to the Prep Service. Allows users to send
;    their prep requests to a remote server. There is no need
;    anymore to have the EIT software or its calibration data
;    installed.
;
;    Currently only standard prep for ETI available.
;
;    Based on Java (gov.nasa.gsfc.hessi.client.PrepServerBridge);
;
; :Author: László István Etesi
;-

;+
; :Description:
;    Initializes the IdlPrepServerBridge.
;
; :Returns:
;    True if the initialization process was successful
;
; :Author: László István Etesi
;-
FUNCTION IdlPrepServerBridge::init
  self.jPrepServerBridge = OBJ_NEW('IDLJavaObject$GOV_NASA_GSFC_HESSI_JIDL_PREP_CLIENT_BRIDGE_PREPSERVERBRIDGE', 'gov.nasa.gsfc.hessi.jidl.prep.client.bridge.PrepServerBridge')
  RETURN, 1
END

;+
; :Description:
;    Cleans up when the IdlPrepServerBridge
;    instance is destroyed.
;
; :Author: László István Etesi
;-
PRO IdlPrepServerBridge::cleanUp
  OBJ_DESTROY, self.jPrepServerBridge
END

;+
; :Description:
;    Reads the image file at the given http
;    location.
;
; :Params:
;    link is the http link pointing to the
;         image file.
;
; :Returns:
;    A byte array containing the binary
;    image data.
;
; :Author: László István Etesi
;-
FUNCTION IdlPrepServerBridge::fetchFile, link
  RETURN, self.jPrepServerBridge->fetchFile(link)
END

;+
; :Description:
;    Reads and preps the EIT image file at the
;    given http location.
;
; :Params:
;    link is the http link pointing to the
;         image file.
;
; :Returns:
;    A byte array containing the prepped EIT
;    data in binary form.
;
; :Author: László István Etesi
;-
FUNCTION IdlPrepServerBridge::getPrepEIT, link
  RETURN, self.jPrepServerBridge->getPrepEIT(link)
END

;+
; :Description:
;    Utility routine that writes the binary data
;    to a file
;
; :Params:
;    data is the binary array
;    filePath is the destination file path for the
;         data
;
; :Author: László István Etesi
;-
PRO IdlPrepServerBridge::writeDataToFile, data, filePath
  self.jPrepServerBridge->writeDataToFile, data, filePath
END

;+
; :Description:
;    Object definition routine
;
; :Author: László István Etesi
;-
PRO IdlPrepServerBridge__define
  void = { IdlPrepServerBridge, jPrepServerBridge:OBJ_NEW() }
END