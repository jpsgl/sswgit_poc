;+
; Project     : VSO
;
; Name        : VXO_PREP__DEFINE
;
; Purpose     : IDL wrapper for Java VxoPrepClient
;
; Category    : VSO, PrepServer
;
; Syntax      : IDL> obj=obj_new('vxo_prep')
;
; Example     : IDL> prep=obj_new('vxo_prep')
;             : IDL> prep->preprocess, 'eit', 'http://...', out='eit.fits', /cosmic
;
; History     : 04-Mar-2009  L. I. Etesi (CUA,FHNW/GSFC), initial release after redesign
;               23-Mar-2009  L. I. Etesi (CUA,FHNW/GSFC), redesign, depend on IDL/SSW objects
;               07-Apr-2009  L. I. Etesi (CUA,FHNW/GSFC), changed prepping
;               08-Jun-2009  L. I. Etesi (CUA,FHNW/GSFC), original filename now preserved, fix prepping bug (date_obs keyword not found)
;               10-Jun-2009  L. I. Etesi (CUA,FHNW/GSFC), added catch statement to preprocess to avoid dangling Java references that lead to OOM Exceptions
;
; Contact     : LASZLO.ETESI@NASA.GOV
;-

;+
; :Description:
;    Client interface to the Prep Service. Allows users to send
;    their prep requests to a remote server. 
;
;    Based on Java (gov.nasa.gsfc.hessi.client.VxoPrepClient);
;
; :Author: Laszlo Istvan Etesi
;-

;+
; :Description:
;    Initializes the VxoPrepClient / vxo_prep.
;
; :Returns:
;    True if the initialization process was successful
;
; :Author: Laszlo Istvan Etesi
;-
FUNCTION vxo_prep::init
  self.vxoPrep = OBJ_NEW('IDLJavaObject$GOV_NASA_GSFC_HESSI_JIDL_PREP_CLIENT_BRIDGE_VXOPREPCLIENT', 'gov.nasa.gsfc.hessi.jidl.prep.client.bridge.VxoPrepClient')
  RETURN, 1
END

;+
; :Description:
;    Cleans up when the vxo_prep
;    instance is destroyed.
;
; :Author: Laszlo Istvan Etesi
;-
PRO vxo_prep::cleanUp
  OBJ_DESTROY, self.vxoPrep
END

;+
; :Description:
;    Cals the remote prep routine on the server
;
; :params:
;    instrument is the instrument with which the data file has been taken
;    url is the http url to the data file
;    outfile points to where the file is saved (include path)
;    _extra takes all additional prepping parameters
;
; :Author: Laszlo Istvan Etesi
;-
PRO vxo_prep::preprocess, instrument, infile, outfile=outfile, _extra=parameters
    ; Activate error handling
    error = 0
    CATCH, error
    IF error NE 0 THEN BEGIN
      CATCH, /cancel
      GOTO, clean_up
    ENDIF
        
    ; Create a new parameter set that will hold all preprocessing parameters
    prepParamSet = self.vxoPrep->createPrepParameterSet(instrument)
    
    IF isvalid(infile) THEN BEGIN ; Checks if infile is set. If not, that might mean we're dealing with a RHESSI image, so just leave it empty
      IF is_string(infile) THEN BEGIN ; Checks if infile is a string. If not, it might be a data array
        IF is_url(infile) THEN BEGIN ; Check if it is a valid URL. If not, assume it's a local file
          prepParamSet->setUrl, infile
        ENDIF ELSE BEGIN
          prepParamSet->setRawData, self.vxoPrep->readByteArrayFromFile(infile)
          prepParamSet->setUrl, file_basename(infile)
        ENDELSE
      ENDIF
    ENDIF
    
    IF ISVALID(parameters) THEN BEGIN
      paramNames = TAG_NAMES(parameters);
    
      FOR i = 0, N_TAGS(parameters) - 1 DO BEGIN
        pName = paramNames(i)
        pValue = parameters.(i)
      
        self.vxoPrep->addParameter, prepParamSet, pName, pValue
        ENDFOR    
    ENDIF
    
    presp = self.vxoPrep->preprocess(prepParamSet, outfile)
    
    status = presp->getStatus();
    print, status->toString()
    print, 'User Message: ' + presp->getUserMessage()
    print, 'Debug Message: ' + presp->getDebugMessage()
    
    ; Cleanup
    clean_up:
    
    IF isvalid(prepParamSet) THEN BEGIN       
      OBJ_DESTROY, prepParamSet
    ENDIF
    
    IF isvalid(presp) THEN BEGIN
      OBJ_DESTROY, presp
    ENDIF
    
    IF isvalid(status) THEN BEGIN
      OBJ_DESTROY, status
    ENDIF
END

;+
; :Description:
;    Utility routine that writes the binary data
;    to a file
;
; :Params:
;    data is the binary array
;    filePath is the destination file path for the
;         data
;
; :Author: Laszlo Istvan Etesi
;-
PRO vxo_prep::writeDataToFile, data, filePath
  IF isvalid(data) THEN BEGIN
    self.vxoPrep->writeDataToFile, data, filePath
  ENDIF ELSE BEGIN
    print, 'Cannot write data to file (' + filePath + '), since there are no data to be written.'
  ENDELSE
END

;+
; :Description:
;    Object definition routine
;
; :Author: Laszlo Istvan Etesi
;-
PRO vxo_prep__define
  void = { vxo_prep, vxoPrep:OBJ_NEW() }
END
