;+
; Project     : VSO
;
; Name        : IDL_BRIDGE_CONFIGURATOR
;
; Purpose     : Writes a IDL-Java Bridge configuration file
;
; Category    : utility, java
;
; Syntax      : idl_bridge_configurator, /force
;
; Inputs      : N/A
;
; Outputs     : N/A
;
; Keywords    : FORCE = Forces to re-create the config file
;
; History     : 11-Mar-2010  L. I. Etesi (CUA,FHNW/GSFC), Initial release
;
; Contact     : laszlo.etesi@nasa.gov
;-

PRO idl_bridge_configurator, force=force
  cfg_file = local_name(get_temp_dir() + '/.bridgecfg')

  IF NOT file_exist(cfg_file) OR keyword_set(force) THEN BEGIN  
    endorsed = local_name('$SSW/gen/java/slibs/endorsed')
  
    cfg = 'JVM Option1 = -Djava.endorsed.dirs=' + endorsed
  
    OPENW, mylun, cfg_file, /GET_LUN
    PRINTF, mylun, cfg
    FREE_LUN, mylun
  ENDIF
  
  SETENV, 'IDLJAVAB_CONFIG=' + cfg_file
END