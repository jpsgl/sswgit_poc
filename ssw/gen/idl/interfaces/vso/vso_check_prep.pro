;+
; Project     : HESSI
;
; Name        : VSO_CHECK_PREP
;
; Purpose     : Check VSO_PREP server
;
; Category    : synoptic sockets VSO
;
; Inputs      : None
;
; Outputs     : 1/0 if up or down
;
; History     : Written 22-Dec-2009, Zarro (ADNET)
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function vso_check_prep

error=0
catch,error
if error ne 0 then begin
 catch,/cancel
 if obj_valid(vsoprep) then obj_destroy,vsoprep
 return,0b
endif

vso_startup
vsoprep = OBJ_NEW($
'IDLJavaObject$STATIC$GOV_NASA_GSFC_JIDL_VPS_CLIENT_PREPROCESSORCLIENTREQUEST', $
'gov.nasa.gsfc.jidl.vps.client.PreprocessorClientRequest')

if ~obj_valid(vsoprep) then return,0b
obj_destroy,vsoprep 
return,1b
end
