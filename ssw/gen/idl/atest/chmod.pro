;+
; Project     : HESSI
;                  
; Name        : CHMOD
;               
; Purpose     : wrapper around FILE_CHMOD that recurses and catches errors
;                             
; Category    : system utility
;               
; Syntax      : IDL> file_chmod,file
;                                        
; Outputs     : None
;
; Keywords    : RECURSIVE = set to recurse on subdirectories and files
;                   
; History     : 17-Apr-2003, Zarro (EER/GSFC)
;               12-Feb-2019, Zarro (ADNET/GSFC) - added recursion
;
; Contact     : dzarro@solar.stanford.edu
;-    

pro chmod,file,mode,_extra=extra,verbose=verbose,recursive=recursive,err=err

err=''
if ~since_version('5.4') then return
if is_blank(file) then begin
 err='Missing input file.'
 return
endif

verbose=keyword_set(verbose)
use_mode=exist(mode)
use_key=is_struct(extra)
recursive=keyword_set(recursive)
if ~use_mode && ~use_key then return

for i=0,n_elements(file)-1 do begin

 dfile=0b & direc=0b
 if is_string(file[i]) then begin
  dfile=file_test(file[i],/reg)
  direc=file_test(file[i],/direc)
  if ~dfile && ~direc then continue
 endif

 error=0
 catch,error
 if error ne 0 then begin
  err=err_state()
  if verbose then mprint,err
  catch,/cancel
  continue
 endif

 if use_mode then file_chmod,file[i],mode else $
  file_chmod,file[i],_extra=extra

;-- recurse on subdirectories

 if direc && recursive then begin
  dir=file[i]
  chmod,dir,/u_read
  path=concat_dir(dir,'*')
  out=file_search(path,count=fcount,/match_initial_dot)
  if fcount gt 0 then chmod,out,mode,_extra=extra,/recursive,verbose=verbose
  out=file_search(path,count=dcount,/test_directory,/match_initial_dot)
  if dcount gt 0 then chmod,out,mode,_extra=extra,/recursive,verbose=verbose
 endif
 
endfor

return

end
