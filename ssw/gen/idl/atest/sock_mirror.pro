;+
; Project     : VSO
;
; Name        : SOCK_MIRROR
;
; Purpose     : Mirror files and directories between local and remote directory
;
; Category    : utility system sockets
;
; Syntax      : IDL> sock_mirror,dir,url,log=log
;
; Inputs      : DIR = local directory to check
;               URL = remote URL directory to check
;
; Outputs     : None
;               
; Keywords    : ERR = error string
;               LOG = log of changes
;               PROCEED = set to actually mirror
;               RECURSE = set to recurse over directories
;               DIRECTORY_ONLY = only mirror directories
;               NO_DELETES = set to not delete files/directories (just update)
;
; History     : 31-Dec-2018, Zarro (ADNET/GSFC)
;-

pro sock_mirror,dir,url,_ref_extra=extra,err=err,proceed=proceed,$
                recurse=recurse,log=log,verbose=verbose,$
                directory_only=directory_only,no_deletes=no_deletes

err=''
proceed=keyword_set(proceed)
recurse=keyword_set(recurse)
verbose=keyword_set(verbose)
directory_only=keyword_set(directory_only)
no_deletes=keyword_set(no_deletes)

;-- compare directories first

sock_compare,dir,url,old=old,new=new,_extra=extra,err=err,ocount=ocount,ncount=ncount,$
             ulist=udirs,ucount=ucount,log=log,verbose=verbose,/directory
if is_string(err) then begin
 log=[log,err] 
 if verbose then print,transpose(log)
 return
endif
 
odir=chklog(dir,/pre)

if proceed then begin

;-- delete old directories
 
 if (ocount gt 0) && ~no_deletes then begin
  log=[log,' ','% Deleting directories in: '+odir]
  for i=0,ocount-1 do begin
   del_dir,old[i],/recursive,/allow_nonexistent,err=err
   if is_string(err) then log=append_arr(log,err,/no_copy)
  endfor
 endif

;-- create new directories

 if (ncount gt 0) then begin
  log=[log,' ','% Creating directories in: '+odir]
  mk_dir,odir,err=err
  if is_blank(err) then chk=test_dir(odir,err=err)
  if is_blank(err) then begin
   for i=0,ncount-1 do begin
    mk_dir,new[i],err=err
    if is_string(err) then log=append_arr(log,err,/no_copy)
   endfor
  endif else log=append_arr(log,err,/no_copy)
 endif

endif

;-- next compare files 

if ~directory_only then begin
 sock_compare,odir,url,old=old,new=new,_extra=extra,err=err,ocount=ocount,ncount=ncount,$
             log=log,verbose=verbose,diff=diff,dcount=dcount,/append

 if is_string(err) then begin
  log=[log,err] 
  if verbose then print,transpose(log)
  return
 endif

 if proceed then begin

;-- delete old files

  if (ocount gt 0) && ~no_deletes then begin
   log=[log,' ','% Deleting files in: '+odir]
   for i=0,ocount-1 do begin
    if ~file_test(old[i],/write) then err='% Denied delete access to: '+old[i]
    if is_blank(err) then file_delete,old[i],/quiet,/allow_nonexistent 
    if is_string(err) then log=append_arr(log,err,/no_copy)
   endfor
  endif

;-- download new files

  if (ncount gt 0) then begin
   log=[log,' ','% Downloading files to: '+odir]
   mk_dir,odir,err=err
   if is_blank(err) then chk=test_dir(odir,err=err)
   if is_blank(err) then begin
    for i=0,ncount-1 do begin
     sock_get,new[i],out_dir=odir,/clobber,/no_check,err=err,/quiet
     if is_string(err) then log=append_arr(log,err,/no_copy)
    endfor
   endif else log=append_arr(log,err,/no_copy)
  endif

;-- update changed files

  if (dcount gt 0) then begin
   log=[log,' ','% Updating files in: '+odir]
   for i=0,dcount-1 do begin
    ofile=concat_dir(odir,file_basename(diff[i]))
    if file_test(ofile,/write) then sock_get,diff[i],out_dir=odir,/clobber,/no_check,/quiet,err=err else $
     err='% Denied write access to: '+ofile
    if is_string(err) then log=append_arr(log,err,/no_copy)
   endfor
  endif

 endif
endif

;-- recurse 

if recurse then begin
 if ucount gt 0 then begin
  ubdirs=file_basename(udirs)
  for i=0,ucount-1 do begin
   sock_mirror,udirs[i],url+'/'+ubdirs[i],proceed=proceed,recurse=recurse,err=err,$
             _extra=extra,directory_only=directory_only,/append,log=log,no_deletes=no_deletes
  endfor
 endif
endif

if is_blank(log) then log=['','% No changes.']
if verbose then print,transpose(log)

return & end

