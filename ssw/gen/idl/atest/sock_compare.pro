;+
; Project     : VSO
;
; Name        : SOCK_COMPARE
;
; Purpose     : Compare files or directories in local directory with remote directory
;               and return differences
;
; Category    : utility system sockets
;
; Syntax      : IDL> sock_compare,dir,url
;
; Inputs      : DIR = local directory to check
;               URL = remote URL directory to check
;
; Outputs     : See keywords
;
; Keywords    : ERR = error string
;               OLD = files/directories in DIR that are not at URL 
;               NEW = files/directories at URL that are not in DIR
;               DIFF = files in DIR that diffier from URL
;               ULIST = updated list of files/directories in DIR after removing OLD
;               and adding NEW
;               LOG = log of results
;               APPEND = append to input log
;               DIRECTORY = compare directories (def is files)
;               IGNORE_FILES= file patterns to ignore 
;               (e.g. ignore= '~$' to ignore files ending in ~)
;               SKIP_DIRECTORIES = directory name to not compare 
;               (e.g. skip='widgets,sockets' to skip comparing directories with
;               these substrings.
;               
; History     : 20-Dec-2018, Zarro (ADNET)
;-

pro sock_compare,dir,url,old=old,new=new,_ref_extra=extra,err=err,ulist=ulist,ucount=ucount,$
                 ncount=ncount,ocount=ocount,log=log,append=append,$
                 inform=inform,directory=directory,diff=diff,dcount=dcount,$
                 ignore_files=ignore_files,skip_directories=skip_directories

diff=''
old=''
new=''
err=''
ulist=''
ncount=0l
ocount=0l
ucount=0l
dcount=0l
inform=keyword_set(inform)
directory=keyword_set(directory)

if is_blank(dir) || ~is_url(url) then begin
 err='Missing input DIR and/or URL to compare.'
 mprint,err
 return
endif

if is_ftp(url) then begin
 err='FTP is not currently supported.'
 mprint,err
 return
endif

;-- check remote directory

sock_search,url,rout,_extra=extra,err=err,count=rcount,/cache,/no_check,directory=directory
if is_string(err) then return
  
;-- check local directory

odir=chklog(dir,/pre)
lcount=0l
if is_dir(dir) then begin
 path=concat_dir(odir,'*')
 if directory then $
  lout=file_search(path,count=lcount,/test_directory,/match_initial_dot) else $
   lout=file_search(path,count=lcount,/test_reg,/match_initial_dot) 
endif

;-- remove skipped directories

if is_string(skip_directories) then begin
 rout=str_remove(rout,skip_directories,count=rcount,_extra=extra,/sub)
 lout=str_remove(lout,skip_directories,count=ocount,_extra=extra,/sub)
endif

if rcount gt 0 then rbout=file_basename(rout)
if lcount gt 0 then lbout=file_basename(lout)

;-- now do matching

if is_blank(log) || ~keyword_set(append) then log=''
out='% Comparing '+odir+' and '+url
if inform then log=append_arr(log,out)
 
case 1 of
 (lcount eq 0) && (rcount eq 0): do_nothing=1
 (lcount eq 0) && (rcount gt 0): new=rout
 (lcount gt 0) && (rcount eq 0): old=lout
 else: begin

;-- look for remote file/directories not on local directory

  for i=0l,rcount-1 do begin
   chk=where(rbout[i] eq lbout,tcount)
   k=chk[0]
   if tcount eq 0 then begin
    new=append_arr(new,rout[i],/no_copy) 
   endif else begin
    if ~directory then begin
     stc=file_info(lout[k])
     lsize=double(stc.size)
     ldate=file_time(lout[k])
     ltime=anytim(ldate)
     rsize=sock_size(rout[i],date=rdate,err=err)
     rtime=anytim(rdate)
     if (ltime ne rtime) || (lsize ne rsize) then begin
      diff=append_arr(diff,rout[i],/no_copy)
     endif
    endif  
   endelse
  endfor

;-- look for local files/directories not on remote directory

  index=rem_elem(lbout,rbout,ocount)
  if ocount gt 0 then old=lout[index]
 end
endcase

chk=where(old ne '',ocount)
if ocount gt 0 then begin
 old=old[chk]
 if ocount eq 1 then old=old[0]
 item=file_basename(old)
 log=[log,'',$
      '% Following local items not on remote directory will be deleted:',$
      '----------------------------------------------------------------']
 log=[log,odir+':',strjoin(item,' '),' ']
endif


chk=where(new ne '',ncount)
if ncount gt 0 then begin
 new=new[chk]
 if ncount eq 1 then new=new[0]
 item=file_basename(new)
 if directory then begin
  new=concat_dir(odir,item)
  log=[log,'',$
      '% Following remote directories not on local directory will be created:',$
      '----------------------------------------------------------------------']
  endif else begin
  log=[log,'',$
      '% Following remote files not on local directory will be downloaded:',$
      '-------------------------------------------------------------------']
 endelse
 log=[log,odir+':',strjoin(item,' '),' ']
endif

chk=where(diff ne '',dcount)
if dcount gt 0 then begin
 diff=diff[chk]
 if dcount eq 1 then diff=diff[0]
 item=file_basename(diff)
 log=[log,'',$
      '% Following local files will be updated:',$
      '----------------------------------------']
 log=[log,odir+':',strjoin(item,' '),' ']
endif

if (ncount eq 0) && (ocount eq 0) && (dcount eq 0) && inform then begin
 if is_dir(odir) then log=[log,'% Local and remote directories match.'] 
endif

;-- generate update file/directory listing

if lcount gt 0 then begin
 ulist=lout
 if (ocount gt 0) then ulist=str_remove(lout,old)
endif

if (ncount gt 0) then ulist=[ulist,new]

chk=where(ulist ne '',ucount)
if ucount gt 0 then begin
 ulist=ulist[chk]
 if ucount eq 1 then ulist=ulist[0]
endif

return & end
