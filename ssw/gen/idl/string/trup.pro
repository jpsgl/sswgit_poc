;+
; Project     : HESSI
;                  
; Name        : TRUP
;               
; Purpose     : simultaneously trim and uppercase a string
;                             
; Category    : string utility
;               
; Syntax      : IDL> out=trup(in)
;
; Inputs      : IN = input string
;                                   
; Outputs     : OUT = output string
;               ;               
; History     : Written, 4-Jan-2000, Zarro (SM&A/GSFC)
;
; Contact     : dzarro@solar.stanford.edu
;-    

function trup,in

return,strupcase(strtrim(in,2))

end
