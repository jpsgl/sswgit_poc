function strsplit, inarray, pattern, tail=tail, head=head, lastpos=lastpos, $
	ss=ss, _ref_extra=extra
;
;+ 
;   Name: strsplit
;
;   Purpose: split string array at first (or last) occurence of pattern
;
;   **********************************************************************
;   *** WARNING:  THIS ROUTINE IS OBSOLETE -- USE SSW_STRSPLIT INSTEAD ***
;   **********************************************************************
;
;   Input Paramters:
;      inarry  - initial string array to split
;      pattern - search string (default is blank)
;
;   Output:
;      function return value is string array 
;
;   Calling Sequence:
;      strarr=strsplit(inarray, pattern , tail=tail)
;      strarr=strsplit(inarray, pattern , /tail, head=head)
;
;   Calling Examples:
;      head=strsplit(inarray, pattern)
;      tail=strsplit(inarray, pattern, /tail)
; 
;   History:
;      13-Jan-1993 (SLF)
;      11-Mar-1993 (SLF) 'released'
;      10-jun-1994 (SLF) bug fix
;      09-May-2003, William Thompson, Added obsolete warning.
;               Also attempts to determine if the standard STRSPLIT is meant,
;               based on what keywords were passed.  This technique doesn't
;               work if no keywords are passed.
;      20-May-2003, William Thompson, Use _extra instead of _strict_extra
;
;-
;
;  If none of the above keywords are called, and other keywords are called,
;  then assume that the standard RSI version of strsplit was intended.
;
if (n_elements(tail) eq 0) and (n_elements(head) eq 0) and		$
	(n_elements(lastpos) eq 0) and (n_elements(ss) eq 0) and	$
	(n_elements(extra) ne 0) then begin
    ON_ERROR, 2  ; return to caller
    RETURN, (n_params() eq 1) ? STRTOK(inarray, _extra=extra) : $
        STRTOK(inarray, pattern, _extra=extra)
endif
;
;  Otherwise, issue a warning message.
;
message,/continue,'OBSOLETE ROUTINE -- Use SSW_STRSPLIT instead'
help,/traceback
;
if n_elements(pattern) eq 0 then pattern = ' '	; default to blank
tail=data_chk(tail,/scaler) and (1-data_chk(tail,/string))

; determine positions
postype=['strpos','str_lastpos']
pos=call_function(postype(keyword_set(tail)),inarray,pattern)

plen=strlen(pattern)
mlen=max(strlen(inarray))

; only loop for uniq positions (use vector strmids whenever possible)
if n_elements(pos) gt 1 then upos=pos(uniq(pos,sort(pos))) else upos=pos

ohead=strarr(n_elements(inarray))
otail=ohead

for i=0,n_elements(upos)-1 do begin
   which =where(pos eq upos(i))
   ohead(which)=strmid(inarray(which), 0, upos(i))
   otail(which)=strmid(inarray(which), upos(i)+plen,mlen)      
endfor

if keyword_set(tail) then begin
   head=ohead
   retval=otail
endif else begin
   tail=otail
   retval=ohead
endelse

return,retval
end
