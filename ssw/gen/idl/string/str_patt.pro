;+
; Project     : HESSI     
;                   
; Name        : str_patt
;               
; Purpose     : find position of a pattern within a string
;               
; Category    : string utility
;               
; Syntax      : IDL> spos=str_patt(pattern,target)
;
; Example     : IDL> spos=str_patt('abc','ndmmdabc') 
;                    spos=5
;    
; Inputs      : PATTERN = pattern string to search for (scalar)
;               TARGET  = target string to search (array or scalar)
;               
; Opt. Inputs : None
;               
; Outputs     : SPOS = start position (-1 if not found)
;
; Opt. Outputs: None
;               
; Keywords    : None
;
; Restrictions: None
;               
; Side effects: None.
;               
; History     : Version 1,  20-May-1999,  D M Zarro (SM&A/GSFC)  Written
;     
; Contact     : dzarro@solar.stanford.edu
;-

function str_patt,pattern,target


if datatype(target) ne 'STR' then return,-1
nt=n_elements(target)
out=replicate(-1,nt)

if datatype(pattern) ne 'STR' then return,-1
if n_elements(pattern) ne 1 then begin
 pr_syntax,'spos=str_patt(pattern,target)'
 if nt eq 1 then out=out(0)
 return,out
endif

len=strlen(pattern)
for j=0,n_elements(target)-1 do begin
 btarget=byte(target(j))
 nb=n_elements(btarget)
 if len gt nb then goto,next
 for i=0,nb-1 do begin
  ns=i & nend=ns+len-1
  if nend ge nb then goto,next
  chunk=string(btarget(ns:nend))
  if pattern eq chunk then out(j)=i
 endfor
next: 
endfor

if nt eq 1 then out=out(0)
return,out
end

