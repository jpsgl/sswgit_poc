;+
; Project     : VSO
;
; Name        : STR_REMOVE
;
; Purpose     : Remove matching strings from a string
;
; Category    : utility strings
;
; Syntax      : IDL> output=str_remove(input,remove)
;
; Inputs      : INPUT = scalar or vector of strings to check
;               ELEMENTS = scalar or vector of strings to remove from input
;
; Outputs     : OUTPUT = remaining string after removal ('' if nothing left)
;
; Keywords    : COUNT = # of remaining elements in INPUT
;               REGEX = elements is Reg Exp
;               FOLD_CASE = match is case-insensitive
;               INDEX = indicies of kept elements in INPUT
;
; History     : 15-Jan-2019, Zarro (ADNET)
;-

function str_remove,input,elements,count=count,fold_case=fold_case,$
                    index=index,regex=regex,err=err

err=''
index=-1L
count=n_elements(input)
if is_blank(input) then begin
 count=0 & return,''
endif

count=n_elements(input) & index=lindgen(count)
if is_blank(elements) then return,input

fold_case=keyword_set(fold_case)

if keyword_set(regex) then begin
 if n_elements(elements) ne 1 then begin
  err='Regexp must be scalar string.'
  return,input
 endif
 index=where(~stregex(input,elements,/bool,fold_case=fold_case),scount)
endif else begin
 if fold_case then $
  index=rem_elem(strlowcase(input),strlowcase(elements),scount) else $
   index=rem_elem(input,elements,scount)
endelse

if scount ne count then begin
 if scount gt 0 then begin
  output=input[index] & count=scount
 endif else begin
  output='' & count=0
 endelse 
endif else output=input

if count eq 1 then begin
 output=output[0]
 index=index[0]
endif

return,output
end
