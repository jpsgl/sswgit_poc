;+
; Project     :	SOHO - CDS
;
; Name        :	SPLIT_STRUCT
;
; Purpose     :	split two structures apart
;
; Explanation :
;
; Use         : SPLIT_STRUCT,STRUCT,INDEX,S1,S2
;
; Inputs      :	STRUCT = input structure to split
;               INDEX  = index (or tag name) at which to break off structure
;                        (all tags after and including this tag will be removed)
; Opt. Inputs :	None.
;
; Outputs     :	S1, S2 = split structures
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Calls       :	None.
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Category    :	Structure handling
;
; Prev. Hist. :	None.
;
; Written     :	Dominic Zarro (ARC)
;
; Version     :	Version 1.0, 22 January 1995
;               Version 2.0, 8 Oct 1998 -- modified to use CREATE_STRUCT
;-

pro split_struct2,struct,index,s1,s2

on_error,1

if (datatype(struct) ne 'STC') or (n_elements(index) eq 0) then begin
 pr_syntax,'split_struct,struct,index,s1,s2'
 return
endif

tags=tag_names(struct)
ntags=n_elements(tags)
if datatype(index) eq 'STR' then begin
 cut=where(strupcase(strtrim(index,2)) eq tags,count)
 if (count eq 0) then begin
  message,'no such tag',/cont
  return
 endif
endif else cut=index
cut=cut(0)

if cut le 0 then begin
 delvarx,s1 & s2=struct
 return
endif

if cut ge (ntags-1) then begin
 delvarx,s2 & s1=struct
 return
endif

pairs=pair_struct(struct,'struct')
s1=exec_struct(pairs(0:cut-1),struct,err=err)
if err ne '' then delvarx,s1

s2=exec_struct(pairs(cut:ntags-1),struct,err=err)
if err ne '' then delvarx,s2

return & end

