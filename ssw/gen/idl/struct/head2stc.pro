;+
; Project     : SOHO-CDS
;
; Name        : HEAD2STC
;
; Purpose     : convert FITs header to structure
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : stc=head2stc(head)
;
; Examples    :
;
; Inputs      : HEAD = FITS header 
;
; Opt. Inputs : None
;
; Outputs     : STC = structure with header keys as tags
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 22 November 1996, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

function head2stc,head
if datatype(head) ne 'STR' then return,-1
equ=strpos(head,'= ')
slash=strpos(head,' /')
for i=0,n_elements(head)-1 do begin
 if equ(i) gt -1 then begin
  field=strmid(head(i),0,equ(i))
  if (slash(i) gt -1) and (slash(i) gt equ(i)) then begin
   val=strmid(head(i),equ(i)+1,slash(i)-equ(i)-1)
   field=trim(strcompress(field))
   field=strep(field,'-','__',/all)
   val=trim(strcompress(val))
   val=strep(val,"'","",/all)
   stc=add_tag(stc,val,field,/dup)
  endif
 endif
endfor
return,stc & end
