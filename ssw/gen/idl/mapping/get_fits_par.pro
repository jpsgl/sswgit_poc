;+
; Project     : SOHO-CDS
;
; Name        : GET_FITS_PAR
;
; Purpose     : get image parameters from FITS header
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : get_fits_par,header,xp,yp,dx,dy
;
; Examples    :
;
; Inputs      : HEADER = FITS header
;
; Opt. Inputs : None
;
; Outputs     : XP,YP = image pixel coordinates
;               DX,DY = image pixel spacings
;
; Opt. Outputs: None
;
; Keywords    : ERR = error string
;               SOHO = use SOHO view
;               STC = header in structure format
;               TIME = image time
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 22 August 1997, D. Zarro, SAC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

pro get_fits_par,header,xp,yp,dx,dy,err=err,soho=soho,time=time,stc=stc

on_error,1
err=''

if datatype(header) ne 'STR' then begin
 pr_syntax,'get_fits_par,header,xp,yp,xc,yc'
 return
endif

stc=head2stc(header,err=err)
if err ne '' then return

;-- extract time (start with standard formats)

time=''
if tag_exist(stc,'date_obs') then time=anytim2utc(stc.date_obs,/ecs,/vms) else begin
 if tag_exist(stc,'date') and tag_exist(stc,'time') then begin
  time=anytim2utc(stc.date+' '+stc.time,/ecs,/vms)
 endif
endelse
if trim(time) eq '' then begin
 if tag_exist(stc,'date__obs') and tag_exist(stc,'time__obs') then begin
  time=anytim2utc(stc.date__obs,/date,/vms)+' '+anytim2utc(stc.time__obs,/time,/vms)
 endif
endif
if trim(time) eq '' then begin
 if tag_exist(stc,'startime') then begin
  time=anytim2utc( double(stc.startime)+anytim2tai('1-jan-1970'),/vms)
 endif
endif

if trim(time) eq '' then begin
 err='cannot determine image time'
 message,err,/cont
 ans=''
 read,'* enter full date and time [e.g. 6-jun-96 08:40:30]: ',ans
 if ans ne '' then begin
  err=''
  time=anytim2utc(ans,/vms,err=err)
 endif
 if err ne '' then begin
  message,err,/cont
  return
 endif
endif

if n_params() lt 2 then return

;-- extract coordinates

if tag_exist(stc,'telescop') then begin
 if trim(strupcase(stc.telescop)) eq 'SOHO' then soho=1
endif

if tag_exist(stc,'cdelt1') then begin
 dx=float(stc.cdelt1)
 dy=float(stc.cdelt2)
endif else begin
 if tag_exist(stc,'radius') then begin
  h=float(pb0r(time,/arc,soho=soho))
  dx=h(2)/stc.radius
  dy=dx
 endif
endelse

if not exist(dx) then begin
 err='cannot determine image scale'
 message,err,/cont
 return
endif

xcen=float((stc.naxis1)*dx)/2.
ycen=float((stc.naxis2)*dy)/2.
xorg=float(stc.crpix1)*dx
yorg=float(stc.crpix2)*dy
xp=xcen-xorg
if tag_exist(stc,'crval1') then xp=xp+stc.crval1
yp=ycen-yorg
if tag_exist(stc,'crval2') then yp=yp+stc.crval2

return & end

