;+
; Project     : SOHO-CDS
;
; Name        : INTER_MAP
;
; Purpose     : interpolate an image map onto a new coordinate system
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : imap=inter_map(map,rmap)
;
; Examples    :
;
; Inputs      : MAP = image map structure
;               RMAP = reference map with coordinates 
;
; Opt. Inputs : None
;
; Outputs     : IMAP = interpolated map
;
; Opt. Outputs: None
;
; Keywords    : ERR = error strings
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 22 August 1997, D. Zarro, SAC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

function inter_map,map,rmap,err=err,_extra=extra

err=''
on_error,1
if (not valid_map(map)) or (not valid_map(rmap))  then begin
 pr_syntax,'imap=inter_map(map,rmap)'
 return,-1
endif

xr=get_map_xp(rmap)
yr=get_map_yp(rmap)
xmax=max(xr)
xmin=min(xr)
ymax=max(yr)
ymin=min(yr)

xp=get_map_xp(map)
yp=get_map_yp(map)

;-- flag data outside range

outside=where( (xp gt xmax) or (xp lt xmin) or $
               (yp gt ymax) or (yp lt ymin), count)

imap=repack_map(map,xr,yr)
imap=rep_tag_value(imap,interp2d(map.data,xp,yp,xr,yr,_extra=extra),'data')

if count gt 0 then imap.data(outside)=0.

return,imap

end

