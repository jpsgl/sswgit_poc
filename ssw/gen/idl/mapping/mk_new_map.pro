;+
; Project     : SOHO-CDS
;
; Name        : MK_NEW_MAP
;
; Purpose     : convert old format map to new format
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : new=mk_new_map(map)
;
; Examples    :
;
; Inputs      : MAP = map structure with old format
;
; Opt. Inputs : None
;
; Outputs     : NEW = map structure with new format
;
; Opt. Outputs: None
;
; Keywords    : ERR = error string
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 16 Feb 1998, D. Zarro, SAC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

function mk_new_map,map,err=err

on_error,1
err=''

if not valid_map(map,old=old,err=err) then begin
 pr_syntax,'nmap=mk_new_map(map)'
 return,-1
endif

if not old then begin
 message,'already using new format',/cont
 return,map
endif

for i=0,n_elements(map)-1 do begin
 xp=get_map_xp(map(i))
 yp=get_map_yp(map(i))
 xc=get_img_center(xp,dx=dx)
 yc=get_img_center(yp,dy=dy)
 tmp=rem_tag(map(i),'xp')
 tmp=rem_tag(tmp,'yp')
 tmp=add_tag(tmp,xc,'xc',index='data')
 tmp=add_tag(tmp,yc,'yc',index='xc')
 tmp=add_tag(tmp,dx,'dx',index='yc')
 tmp=add_tag(tmp,dy,'dy',index='dx')
 nmap=merge_struct(nmap,tmp)
endfor

return,nmap & end


