;+
; Project     : SOHO-CDS
;
; Name        : GET_MAP_SPACE
;
; Purpose     : extract dx,dy spacings from map
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : space=get_map_space(map)
;
; Examples    :
;
; Inputs      : MAP = image map
;
; Opt. Inputs : None
;
; Outputs     : SPACE = [dx,dy]
;
; Opt. Outputs: None
;
; Keywords    : ERR = error string
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 16 Feb 1998, D. Zarro, SAC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

function get_map_space,map,err=err

on_error,1

err=''
if not valid_map(map,err=err,old=old) then return,-1
if n_elements(map) ne 1 then begin
 err='cannot handle more than one map'
 message,err,/cont
 return,-1
endif

if old then begin
 xc=get_img_center(map.xp,dx=dx)
 yc=get_img_center(map.yp,dy=dy)
 space=[dx,dy]
endif else begin
 space=[map.dx,map.dy]
endelse

return,space

end
