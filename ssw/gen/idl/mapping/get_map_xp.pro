;+
; Project     : SOHO-CDS
;
; Name        : GET_MAP_XP
;
; Purpose     : extract X-coordinate arrays of map
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : xp=get_map_xp(map)
;
; Examples    :
;
; Inputs      : MAP = image map
;
; Opt. Inputs : None
;
; Outputs     : XP = 2d X-coordinate array
;
; Opt. Outputs: None
;
; Keywords    : ERR = error string
;               DX  = X-pixel spacing
;               XC  = X-pixel center coordinate
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 16 Feb 1998, D. Zarro, SAC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

function get_map_xp,map,dx=dx,xc=xc,err=err

on_error,1

err=''
if not valid_map(map,err=err) then return,-1
if n_elements(map) ne 1 then begin
 err='cannot handle more than one map'
 message,err,/cont
 return,-1
endif


old_format=tag_exist(map,'xp')

if old_format then begin
 xp=map.xp 
 xc=get_img_center(xp,dx=dx)
endif else begin
 sz=size(map.data)
 nx=sz(1) & ny=sz(2)
 dx=map.dx & xc=map.xc
 xp=mk_map_xp(xc,dx,nx,ny)
endelse

return,xp

end
