;+
; Project     : SOHO-CDS
;
; Name        : MAP__DEFINE
;
; Purpose     : Define a map object
;
; Category    : imaging
;
; Explanation : The map object is actually a structure with a pointer
;               field. The map structure is stored in this pointer.
;
; Syntax      : This procedure is invoked when a new MAP object is
;               created via:
;
;               IDL> new=obj_new('map')
; Examples    :
;
; Inputs      : 'map' = object classname 
;
; Opt. Inputs : map = map structure created by MAKE_MAP
;
; Outputs     : A MAP object with methods: SET, GET, & PLOT
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 22 Nov 1997, D. Zarro, SAC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

;-------------------------------------------------------------------------

function map::init,map,no_copy=no_copy     ;-- initialize object values
make_pointer,p
self.pointer=p
if valid_map(map) then self->set,map,no_copy=no_copy
return,1
end

;------------------------------------------------------------------------

pro map::cleanup                          ;--destroy map object
free_pointer,self.pointer
obj_destroy,self
dprint,'% cleaning up'
end

;-------------------------------------------------------------------

pro map::set,stc,no_copy=no_copy                     ;-- set map object values
set_pointer,self.pointer,stc,no_copy=no_copy
end

;-------------------------------------------------------------------

function map::get,index,status=status,no_copy=no_copy      ;-- get map object values

status=0 & stc=-1
stc=get_pointer(self.pointer,no_copy=no_copy,status=status)
if status then begin
 ns=n_elements(stc)
 if exist(index) then return,stc(index < (ns-1)) else return,stc
endif else return,-1
end

;-------------------------------------------------------------------

pro map::plot,index,_extra=extra                ;-- plot map data
if not exist(index) then index=0
stc=self->get(status=status)
if status and valid_map(stc) then plot_map,stc(index),_extra=extra
end

;-------------------------------------------------------------------

function map::id                               ;-- print map id
stc=self->get(status=status)
if status then if tag_exist(stc,'id') then return,stc.id
return,-1
end

;-------------------------------------------------------------------

function map::xp,status=status             ;-- return x-coordinates of map

stc=self->get(status=status)
if status then return,get_map_xp(stc) else return,-1
end

;-------------------------------------------------------------------

function map::yp,status=status              ;-- return y-coordinates of map

stc=self->get(status=status)
if status then return,get_map_yp(stc) else return,-1
end

;-------------------------------------------------------------------

function map::dx,status=status          ;-- return x-spacing of map

stc=self->get(status=status)
if status then begin
 dx=get_map_prop(stc,/dx)
 return,dx
endif else return,-1
end

;-------------------------------------------------------------------

function map::dy,status=status                ;-- return y-spacing of map

stc=self->get(status=status)
if status then begin
 dy=get_map_prop(stc,/dy)
 return,dy
endif else return,-1
end

;-------------------------------------------------------------------

function map::yc,status=status                ;-- return center y-pixel of map

stc=self->get(status=status)
if status then begin
 yc=get_map_prop(stc,/yc)
 return,yc
endif else return,-1
end

;-------------------------------------------------------------------

function map::xc,status=status                ;-- return center x-pixel of map

stc=self->get(status=status)
if status then begin
 xc=get_map_prop(stc,/xc)
 return,xc
endif else return,-1
end

;-------------------------------------------------------------------

pro map::prop                               ;-- print map properties

stc=self->get(status=status)
if status then print,tag_names(stc)

end
;-------------------------------------------------------------------

pro map::movie,_extra=extra                ;-- movie map
stc=self->get(status=status)
if status and valid_map(stc) and (n_elements(stc) gt 1) then movie_map,stc,_extra=extra
end

;-------------------------------------------------------------------

function map::time                               ;-- print map time
stc=self->get(status=status)
if status then if tag_exist(stc,'time') then return,stc.time
return,-1
end


;-------------------------------------------------------------------

function map::check,tag,index=index       ;--check for valid object field names
status=0
if datatype(tag) eq 'STR' then begin
 stc=self->get(status=status)
 if status then begin
  tags=tag_names(stc)
  clook=where(strupcase(trim(tag)) eq strupcase(trim(tags)),count)
  if count gt 0 then begin
   index=clook(0) & status=1
  endif
 endif
endif
return,status
end

;-----------------------------------------------------------------------------

pro map__define                          ;-- define base map structure
make_pointer,p
map={map,pointer:p}

end

