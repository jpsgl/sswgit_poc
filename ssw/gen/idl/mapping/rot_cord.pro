;+
; Project     : SOHO-CDS
;
; Name        : ROT_CORD
;
; Purpose     : rotate image coordinates
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : rot_cord,xarr,yarr,angle,rx,ry
;
; Examples    :
;
; Inputs      : XARR,YARR = image (X,Y) coordinates
;               ANGLE = angle in degrees (+ for clockwise)
;
; Opt. Inputs : None
;
; Outputs     : RX,RY = rotated coordinates
;
; Opt. Outputs: None
;
; Keywords    : CENTER= [XC,YC] = center of rotation [def = center of image]
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 22 November 1996, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

pro rot_cord,xarr,yarr,angle,rx,ry,center=center

if n_elements(angle) eq 0 then begin
 repeat begin
  angle='' & read,'* enter angle [deg] by which to rotate image [+ clockwise]: ',angle
 endrep until angle ne ''
 angle=float(angle)
endif

angle=float(angle)

if (angle mod 360.) eq 0 then begin
 rx=xarr & ry=yarr
 return
endif

theta=angle*!dtor
costh=cos(theta) & sinth=sin(theta)

;-- rotate pixel arrays about requested center 

if n_elements(center) eq 2 then begin
 xc=float(center(0)) & yc=float(center(1))
endif else begin
 min_x=min(xarr) & max_x=max(xarr)
 min_y=min(yarr) & max_y=max(yarr)
 xc=(min_x+max_x)/2. &  yc=(min_y+max_y)/2.
endelse

rx=xc+costh*(xarr-xc)+sinth*(yarr-yc)
ry=yc-sinth*(xarr-xc)+costh*(yarr-yc)

return & end

