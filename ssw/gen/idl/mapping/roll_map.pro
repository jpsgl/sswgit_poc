;+
; Project     : SOHO-CDS
;
; Name        : ROLL_MAP
;
; Purpose     : roll image contained within structure created by MAKE_MAP
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : rmap=roll_map(map,angle)
;
; Examples    :
;
; Inputs      : MAP = map structure created by MAKE_MAP
;               ANGLE = angle in degrees (+ for clockwise)
;
; Opt. Inputs : None
;
; Outputs     : RMAP = map with rotated coordinates
;
; Opt. Outputs: None
;
; Keywords    : NOREMAP = don't remap image data (just rotate coords)
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 14 Feb 1998, D. Zarro, SAC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

function roll_map,map,angle,noremap=noremap,err=err,$
                _extra=extra

err=''
rmap=rot_map(map,angle,noremap=noremap,err=err,_extra=extra,center=[0,0])

if err ne '' then begin
 if exist(map) then return,map else return,-1
endif
 
if (1-keyword_set(noremap)) then begin
 if not tag_exist(rmap,'roll') then rmap=add_tag(rmap,0.,'roll')
 rmap.roll=(rmap.roll+angle) mod 360
endif

return,rmap & end

