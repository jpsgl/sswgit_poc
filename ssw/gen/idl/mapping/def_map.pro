;+
; Project     : SOHO-CDS
;
; Name        : DEF_MAP
;
; Purpose     : Define a basic 2x2 element image map 
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : def_map,map
;
; Examples    :
;
; Inputs      : None
;
; Opt. Inputs : None
;
;
; Outputs     : MAP ={data:data,xp:xp,yp:yp,id:id,time:time,dur:dur,soho:soho}
;               where,
;               DATA  = 2x2 image array
;               XP,YP = 2x2 cartesian coordinate arrays
;               ID    = blank ID label
;               TIME  = blank start time of image
;               DUR   = 0 duration 
;               SOHO  = 0, flag identifying that image is not SOHO-viewed
;
; Opt. Outputs: None
;
; Keywords    : OLD = use old format
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 22 October 1997, D. Zarro, SAC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

pro def_map,map,old_format=old_format

base=fltarr(2,2)
if keyword_set(old) then begin
 map={data:base,xp:base,yp:base,time:'',dur:0.,id:'',soho:0} 
endif else begin
 map={data:base,xc:0.,yc:0.,dx:1.,dy:1.,time:'',dur:0.,id:'',soho:0}
endelse 

return
end
