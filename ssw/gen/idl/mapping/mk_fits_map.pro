;+
; Project     : SOHO-CDS
;
; Name        : MK_FITS_MAP
;
; Purpose     : Make an image map from a FITS file
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : map=mk_fits_map(file)
;               map=mk_fits_map(data,header)
;
; Examples    :
;
; Inputs      : FILE = FITS file name (or FITS data + HEADER)
;
; Opt. Inputs : None
;
; Outputs     : MAP = map structure from MK_MAP
;
; Opt. Outputs: None
;
; Keywords    : SUB = [x1,x2,y1,y2] = indicies of subarray to extract
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 22 January 1997, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

function mk_fits_map,data,header,sub=sub,stc=stc,soho=soho

;-- check inputs

if datatype(data) eq 'STR' then begin
 fl=loc_file(data,count=count,err=err)
 if count eq 0 then begin
  message,err,/cont & return,0
 endif
 file=fl(0)
 fxread,file,fdata,fhead
endif else begin
 if exist(data) then fdata=data
 if datatype(header) eq 'STR' then fhead=header
endelse

if (not exist(fdata)) or (not exist(fhead)) then begin
 message,'Syntax: map=mk_fits_map(data,header)',/cont
 return,0
endif

;-- extract time

time=''
stc=head2stc(fhead)
if tag_exist(stc,'date_obs') then time=anytim2utc(stc.date_obs,/ecs,/vms) else begin
 if tag_exist(stc,'date') and tag_exist(stc,'time') then begin
  time=anytim2utc(stc.date+' '+stc.time,/ecs,/vms)
 endif
endelse


if trim(time) eq '' then begin
 if tag_exist(stc,'date__obs') and tag_exist(stc,'time__obs') then begin
  time=anytim2utc(stc.date__obs,/date,/vms)+' '+anytim2utc(stc.time__obs,/time,/vms)
 endif
endif

if trim(time) eq '' then begin
 message,'cannot determine image time',/cont
 return,0
endif

;-- extract pointing stuff

if tag_exist(stc,'cdelt1') then begin
 dx=float(stc.cdelt1)
 dy=float(stc.cdelt2)
endif else begin
 if tag_exist(stc,'radius') then begin
  h=pb0r(time,/arc,soho=soho) 
  dx=h(2)/stc.radius
  dy=dx
 endif
endelse

if not exist(dx) then begin
 message,'cannot determine image scale',/cont
 return,0
endif

xcen=float((stc.naxis1+1.)*dx)/2.
ycen=float((stc.naxis2+1.)*dy)/2.
xorg=float(stc.crpix1)*dx
yorg=float(stc.crpix2)*dy
xc=xcen-xorg
if tag_exist(stc,'crval1') then xc=xc+stc.crval1
yc=ycen-yorg
if tag_exist(stc,'crval2') then yc=yc+stc.crval2

if tag_exist(stc,'INSTRUME') then inst=trim(stc.instrume) else inst=''
if tag_exist(stc,'DETECTOR') then det=trim(stc.detector)  else det=''
if tag_exist(stc,'exptime') then dur=float(stc.exptime) else dur=0.
if dur eq 0. then begin
 if tag_exist(stc,'date_obs') and tag_exist(stc,'date_end') then $
  dur=anytim2tai(stc.date_end)-anytim2tai(stc.date_obs)
endif
if tag_exist(stc,'telescop') then soho=(strpos(stc.telescop,'SOHO') gt -1) else soho=0
if tag_exist(stc,'wavelnth') then wave=stc.wavelnth else wave=''
id=trim(inst+' '+det+' '+wave)

;-- extract sub-array

if exist(sub) then begin
 if n_elements(sub) ne 4 then begin
  exptv,fdata
  wshow
  sub_data=tvsubimage(fdata,x1,x2,y1,y2)
  sub=[x1,x2,y1,y2]
 endif
endif

;-- make map
 
map=make_map(fdata,xc,yc,dx,dy,id=id,time=time,dur=dur,sub=sub,soho=soho)

return,map


end
