;+
; Project     : HESSI
;
; Name        : EIT__DEFINE
;
; Purpose     : Define an EIT map object
;
; Category    : imaging maps
;
; Explanation : The map object is actually a structure with a pointer
;               field. The map structure is stored in this pointer.
;               The EIT object inherits MAP object properties and methods,
;               and includes a special EIT reader
;               ;
; Syntax      : This procedure is invoked when a new EIT object is
;               created via:
;
;               IDL> new=obj_new('eit')
; Examples    :
;
; Inputs      : 'EIT' = object classname (a subclass of MAP)
;
; Opt. Inputs : map = map structure created by MAKE_MAP
;
; Outputs     : A MAP object with methods: SET, GET, & PLOT
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 19 May 1998, D. Zarro, SM&A/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

;-- define EIT object

pro eit__define                 

eit_struct={eit, inherits map}

return & end

;-- EIT reader and prep function

pro eit::read,file,err=err,noprep=noprep,_extra=extra
 
err=''
if datatype(file) ne 'STR' then begin
 err='Invalid input file'
 message,err,/cont
 return
endif

chk=loc_file(file,count=count,err=err)
if count ne 1 then begin
 message,err,/cont
 return
endif

read_eit,file,index,data,_extra=extra
if keyword_set(noprep) then index2map,index,data,map else begin
 eit_prep,index,data=data,ni,nd
 index2map,ni,nd,map
endelse

self->set,map

end


