;+
; Project     : SOHO-CDS
;
; Name        : REMAKE_MAP
;
; Purpose     : Regularize (X,Y) coordinates for image map
;
; Category    : imaging
;
; Explanation :
;
; Syntax      : remake_xy,map,xp,yp,size=size,spacing=spacing
;
; Examples    :
;
; Inputs      : MAP = map structure
;
; Opt. Inputs : None
;
;
; Outputs     : XP = new X-coordinates
;               YP = new Y-coordinates
; Opt. Outputs:
;
; Keywords    :
;               SPACING = [DX,DY] = new pixel spacing [or get from MAP]
;               SIZE = [NX,NY] = new image size [or get from SPACING]
;               SAME = set to preserve same size as original MAP
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 6 August 1997, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

pro remake_xy,map,xp,yp,size=msize,spacing=spacing,err=err,same=same

unpack_map,map,data,dx=dx,dy=dy,xc=xc,yc=yc,err=err,xside=xside,yside=yside

if err ne '' then begin
 message,err,/cont
 return
endif
                                                                              
sz=size(data)
nx=sz(1) & ny=sz(2)

if n_elements(spacing) ne 2 then spacing=float([dx,dy]) 
if n_elements(msize) ne 2 then msize=[xside/spacing(0)+1.,yside/spacing(1)+1.]
if keyword_set(same) then msize=[nx,ny]

stop

make_xy,msize(0),msize(1),xp,yp,xc=xc,yc=yc,dx=spacing(0),dy=spacing(1)

return & end

