;+
; Project     : SOHO-CDS
;
; Name        : SOHO_MAP
;
; Purpose     : Fast EARTH-to-SOHO conversion of map coordinates
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : tmap=soho_map(map)
;
; Examples    :
;
; Inputs      : MAP = map structure
;
; Opt. Inputs : None
;
; Outputs     : TMAP = map with new viewed coordinates
;
; Opt. Outputs: None
;
; Keywords    : INVERSE = set for SOHO-to-EARTH transform
;
; Common      : None
;
; Restrictions: Map data is not rebinned. Only coordinates transformed.
;
; Side effects: None
;
; History     : Written 7 July 1998, D. Zarro, SAC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

function soho_map,map,inverse=inverse,err=err

on_error,1
err=''

;--check inputs

if not valid_map(map,err=err,old=old) then begin
 pr_syntax,'tmap=soho_map(map, [inverse=inverse])'
 if exist(map) then return,map else return,-1
endif

inverse=keyword_set(inverse)
nmap=n_elements(map)


for i=0,nmap-1 do begin
 rmap=map(i)
 if not tag_exist(rmap,'soho') then add_prop,rmap,soho=0
 soho=rmap(i).soho
 do_it=(soho and inverse) or ((1-soho) and (1-inverse))
 if (1-do_it) then dprint,'% view already corrected:',i
 if do_it then begin
  xp=get_map_prop(rmap,/xp)
  yp=get_map_prop(rmap,/yp)
  soho_cord,xp,yp,rmap.time,xr,yr,inverse=inverse,err=err
  if err eq '' then begin
   rmap=mk_map_old(rmap)
   rmap=repack_map(rmap,xr,yr)
   rmap.soho=1-inverse
  endif
 endif
 tmap=merge_struct(tmap,rmap)
endfor

return,tmap

end
