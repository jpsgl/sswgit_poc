;+
; Name:
;
;      DROT_RASTER
;
; Purpose:
;
;      Differentially rotate solar raster maps
;
; Category:
;
;      Maps
;
; Calling sequence:
;
;      map_drot=DROT_RASTER(map [ , t_ref ] [ , REF_MAP=map_ref ] 
;                           [ , ROLL=roll_angle , RCENTER=roll_center ]
;                           [ , /ADD_RDUR , /VERBOSE , SPHERE=0 , /KEEP ] )
;
; Inputs:
;
;      map : input map (size of data array: [Nx,Ny])
;      t_ref : reference time for rotation (def.: current time)
;
; Keyword parameters:
;
;      REF_MAP = reference map for rotation and gridding. Its TIME, ROLL 
;                ROLL_CENTER values supersede those specified by t_ref, 
;                ROLL and RCENTER.
;      ROLL = final roll angle of the image
;      RCENTER = center of rotation (if rolling)
;      ADD_RDUR = add to the output map a tag (RDUR) containing an array that 
;                 estimates the duration of the rotation interval on a 
;                 pixel-by-pixel basis.
;      VERBOSE = issue messages warning about off-limb pixels
;      SPHERE = keyword passed to ARCMIN2HEL throuth ROT_XY (IN, default: 1; 
;               note that the default in ROT_XY is 0)
;      KEEP = passed to DROT_COORD: keeps same same P,B0,R values when 
;             rotating: use TIME tag as reference epoch for computing these 
;             values
;      all keywords accepted by INTERP2D
;
; Outputs:
;
;      map_drot : new map (differentially rotated)
;
; Common blocks:
;
;      None
;
; Calls:
;
;      VALID_MAP,GET_UTC,ANYTIM2TAI,UNPACK_MAP,SOHO_VIEW,USE_SOHO_VIEW,
;      USE_EARTH_VIEW,GET_MAP_XRANGE,GET_MAP_YRANGE,GET_ARR_CENTER,ROT_XY,
;      REP_TAG_VALUE,ADD_PROP,ROLL_XY,DROT_COORD,PB0R
;
; Description:
;
;        A map is considered to represent a raster if it contains a tag,
;      named START, giving the start times of each exposure (column) of the
;      raster. 
;        The coordinates of each column (exposure) are differentially rotated
;      to the reference time (t_ref). The data array is then interpolated on 
;      a regular grid. If an array of maps is given, the new grid covers 
;      the largest rectangular FOV included in the rotated FOV's of all maps, 
;      otherwise the original grid is used.
;        When ROLL and/or RCENTER, are specified, the resulting maps are 
;      also rotated to the new roll angle.
;        When a reference map is provided, its TIME, ROLL, ROLL_CENTER,  
;      grid parameters override all other input values. 
;
; Side effects:
;
;      None
;
; Restrictions:
;
;      - Assumes that rasters are built in the E-W direction or viceversa, 
;      not N-S (see DROT_COORD).
;      - Mixing maps (including MAP_REF) with SOHO and EARTH coordinates is 
;      not allowed.
;
;
; Modification history:
;
;      V. Andretta,   26/Jan/1999 - Written
;      V. Andretta,   28/Feb/1999 - Added ROLL, RCENTER, REF_MAP, SPHERE 
;        keywords
;      V. Andretta,   11/Apr/1999 - Added KEEP keyword
;      V. Andretta,   20/Apr/1999 - Specified ambiguous keyword "R" in ROT_XY; 
;        fixed erroneous definition of ROLL/ROLL_CENTER kwds for arrays of maps
;
; Contact:
;
;      VAndretta@solar.stanford.edu
;-
;===============================================================================
;
;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;
;
;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  function DROT_RASTER,map,t_ref,REF_MAP=map_ref $
                      ,ROLL=roll_angle,RCENTER=roll_center,ADD_RDUR=add_rdur $
                      ,VERBOSE=verbose,_EXTRA=_extra,SPHERE=sphere,KEEP=keep

  ON_ERROR,2


;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;%%% Check input and some definitions
;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  map_drot=0

  if N_PARAMS() lt 1 then begin
    PRINT,'%E> DROT_RASTER: Usage: map_drot=DROT_RASTER,map,t_ref'
    PRINT,'                    or: map_drot=DROT_RASTER,map,REF_MAP=ref_map'
    RETURN,map_drot
  endif

  if VALID_MAP(map) eq 0 then begin
    PRINT,'%E> DROT_RASTER: Input structure not a map'
    RETURN,map_drot
  endif

  Nmaps=N_ELEMENTS(map)

  verb=KEYWORD_SET(verbose)

  if N_ELEMENTS(sphere) eq 0 then sphere=1

  do_add_rdur=KEYWORD_SET(add_rdur)

  using_soho_view=SOHO_VIEW()

  keep_angles=KEYWORD_SET(keep)

;% Get reference time and roll properties from input parameters/keywords

;% Get roll values for all the maps

  map_roll=FLTARR(Nmaps)
  map_rcenter=FLTARR(2,Nmaps)

  for i=0,Nmaps-1 do begin
    UNPACK_MAP,map(i),ROLL=curr_roll,RCENTER=curr_rcenter
    curr_roll=FLOAT(curr_roll) mod 360
    map_roll(i)=curr_roll
    map_rcenter(*,i)=curr_rcenter
  endfor

;% Initial estimate of reference time

  GET_UTC,t0 & t0=ANYTIM2TAI(t0)

  if not VALID_MAP(map_ref) then begin

;% Estimate (or use input) reference time for rotation...

    if N_ELEMENTS(t_ref) ne 0 then begin
      errmsg=''
      t0=ANYTIM2TAI(t_ref(0),ERRMSG=errmsg)
      if errmsg ne '' then PRINT,'%W> DROT_RASTER: '+errmsg
    endif

;% ...and final roll angle and center of rotated map(s)

    if N_ELEMENTS(roll_angle) ne 0 then $
      roll=REPLICATE(FLOAT(roll_angle(0)) mod 360,Nmaps) $
    else $
      roll=map_roll

    if N_ELEMENTS(roll_center) eq 2 then $
      rcenter=TRANSPOSE([[REPLICATE(FLOAT(roll_center(0)),Nmaps)] $
                        ,[REPLICATE(FLOAT(roll_center(1)),Nmaps)]]) $
    else $
      rcenter=map_rcenter

  endif

;% Check if map represents a raster

  sz=SIZE(map(0).data)
  Nx=sz(1)
  Ny=sz(2)
  multi_time=0
  if TAG_EXIST(map(0),'START') then begin
    if N_ELEMENTS(map(0).start) ne Nx then $
      PRINT,'%W> DROT_RASTER: ' $
           +'Number of START times inconsistent with DATA 1st dimension' $
    else $
      multi_time=1
  endif


;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;%%% Create new grid
;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

;%%%%%

  if VALID_MAP(map_ref) then begin

;% Get grid parameters from reference map; also get reference time and 
;% roll parameters

    UNPACK_MAP,map_ref(0),t0,TAG='TIME' $
              ,NX=Nxg,NY=Nyg,XC=xcg0,YC=ycg0,DX=dxg,DY=dyg $
              ,ROLL=roll,RCENTER=rcenter
    roll=FLOAT(roll) mod 360
    xpg0=GET_MAP_XP(map_ref(0))
    ypg0=GET_MAP_YP(map_ref(0))

;% Final roll angle for all maps

    roll=REPLICATE(roll,Nmaps)
    rcenter=TRANSPOSE([[REPLICATE(FLOAT(rcenter(0)),Nmaps)] $
                      ,[REPLICATE(FLOAT(rcenter(1)),Nmaps)]])

;%%%%%

  endif else begin

   if Nmaps gt 1 then begin

;% Define a common grid for all the maps to be rotated

;% Define new field of view

;%%% Define current field of view

      xr=FLTARR(2,2,Nmaps) ; => first coordinate: E-W, second coordinate: N-S
      yr=FLTARR(2,2,Nmaps)
      tr=DBLARR(2,Nmaps)
;% Times of the first and last column
      if multi_time eq 1 then begin
        tr(0,*)=map.start(0)
        tr(1,*)=map.start(Nx-1)
      endif else begin
        tr(0,*)=ANYTIM2TAI(map.time)
        tr(1,*)=ANYTIM2TAI(map.time)
      endelse
;% Times and coordinates of the FOV corners
      for i=0,Nmaps-1 do begin
        xr(*,0,i)=GET_MAP_XRANGE(map(i))
        xr(*,1,i)=xr(*,0,i)
        yr(0,*,i)=GET_MAP_YRANGE(map(i))
        yr(1,*,i)=yr(0,*,i)
      endfor

;%%% Differentially rotate current field of view

      xr_drot=xr
      yr_drot=yr
      view_type=GET_MAP_PROP(map(0),/SOHO)>0
      if view_type then USE_SOHO_VIEW else USE_EARTH_VIEW
      for i=0,Nmaps-1 do begin
;% Roll back boundaries of field of view
        curr_roll=map_roll(i)
        curr_rcenter=map_rcenter(*,i)
        xr_i=xr(*,*,i)
        yr_i=yr(*,*,i)
        if curr_roll ne 0 then ROLL_XY,xr(*,*,i),yr(*,*,i) $
          ,+curr_roll,CENTER=curr_rcenter,xr_i,yr_i
        xr_drot_i=xr_i
        yr_drot_i=yr_i
;% If KEEP keyword is set, compute here P,B0,R values; these values will be
;% passed to ROT_XY
        if keep_angles then begin
          angles=PB0R(map(i).time,SOHO=SOHO_VIEW())
          P_sun=angles(0)
          B0_sun=angles(1)
        ;% convert solar radius to observer's distance, in units of solar radii
          D_sun=1./ATAN(angles(2)/(60.*!RADEG))
        endif
;% Differentially rotate field of view
        for iix=0,1 do begin
          crd=ROT_XY(xr_i(iix,*),yr_i(iix,*),TSTART=tr(iix,i),TEND=t0 $
             ,OFFLIMB=offlimb,INDEX=disk_index $
             ,P=P_sun,B0=B0_sun,R0=D_sun,KEEP=keep_angles $
             ,SPHERE=KEYWORD_SET(sphere))
          if disk_index(0) ge 0 then begin
            xr_drot_i(iix,disk_index)=crd(disk_index,0)
            yr_drot_i(iix,disk_index)=crd(disk_index,1)
          endif
        endfor
;% Return FOV coordinates to original roll angle
        if curr_roll ne 0 then ROLL_XY,xr_drot_i,yr_drot_i $
          ,-curr_roll,CENTER=curr_rcenter,xr_drot_i,yr_drot_i
        xr_drot(*,*,i)=xr_drot_i
        yr_drot(*,*,i)=yr_drot_i
      endfor
      if using_soho_view then USE_SOHO_VIEW else USE_EARTH_VIEW

;% Define new grid

;%%% New pixel sizes

      dxg=MIN(map.dx)
      dyg=MAX(map.dy)

;%%% New field of view

      x1=MAX(xr_drot(0,*,*))
      x2=MIN(xr_drot(1,*,*))
      y1=MAX(yr_drot(*,0,*))
      y2=MIN(yr_drot(*,1,*))

      Nxg=FIX((x2-x1)/dxg)>1
      Nyg=FIX((y2-y1)/dyg)>1

      xc=0.5*(x1+x2)
      x0=xc-0.5*(Nxg-1)*dxg
      yc=0.5*(y1+y2)
      y0=yc-0.5*(Nyg-1)*dyg

      xpg0=(x0+dxg*FINDGEN(Nxg))#REPLICATE(1,Nyg)
      ypg0=REPLICATE(1,Nxg)#(y0+dyg*FINDGEN(Nyg))

      xcg0=GET_ARR_CENTER(xpg0)
      ycg0=GET_ARR_CENTER(ypg0)

    endif else begin

;% Just use current grid

      UNPACK_MAP,map(0),NX=Nxg,NY=Nyg,XC=xcg0,YC=ycg0,DX=dxg,DY=dyg

      xpg0=GET_MAP_XP(map(0))
      ypg0=GET_MAP_YP(map(0))

    endelse

  endelse

;%%% New START times

  tpg=REPLICATE(t0,Nxg)


;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;%%% Define new map(s)
;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

;% Create template map
  map_drot0=map(0)
  data_drot=MAKE_ARRAY(DIM=[Nxg,Nyg],VALUE=map(0).data(0))
  map_drot0=REP_TAG_VALUE(map_drot0,data_drot,'DATA')         ; DATA
  map_drot0.xc=xcg0                                           ; XC
  map_drot0.yc=ycg0                                           ; YC
  map_drot0.dx=dxg                                            ; DX
  map_drot0.dy=dyg                                            ; DY
  map_drot0=REP_TAG_VALUE(map_drot0,tpg,'START')              ; START
  rtime=ANYTIM2CAL(t0,FORM=9)                                 ; RTIME
  if TAG_EXIST(map_drot0,'RTIME') then $
    map_drot0=REP_TAG_VALUE(map_drot0,rtime,'RTIME') $
  else $
    ADD_PROP,map_drot0,RTIME=rtime
  if do_add_rdur then begin                                   ; RDUR 
    rdur=DOUBLE(data_drot)
    if TAG_EXIST(map_drot0,'RDUR') then $
      map_drot0=REP_TAG_VALUE(map_drot0,rdur,'RDUR') $
    else $
      ADD_PROP,map_drot0,RDUR=rdur
  endif
  if TAG_EXIST(map_drot0,'ROLL') then $                       ; ROLL
    map_drot0=REP_TAG_VALUE(map_drot0,roll(0),'ROLL') $
  else $
    ADD_PROP,map_drot0,ROLL=roll(0)
  if TAG_EXIST(map_drot0,'ROLL_CENTER') then $                ; ROLL_CENTER
    map_drot0=REP_TAG_VALUE(map_drot0,rcenter(*,0),'ROLL_CENTER') $
  else $
    ADD_PROP,map_drot0,ROLL_CENTER=rcenter(*,0)
;% Replicate template map to create the output map(s)
  map_drot=REPLICATE(map_drot0,Nmaps)
  map_drot0=0
  map_drot.roll_angle=roll
  map_drot.roll_center=rcenter
;% Copy all other tags into the new map(s)
  name=TAG_NAMES(map)
  if do_add_rdur then begin
    others=WHERE(name ne 'DATA'        $
             and name ne 'XC'          $
             and name ne 'YC'          $
             and name ne 'DX'          $
             and name ne 'DY'          $
             and name ne 'START'       $
             and name ne 'RTIME'       $
             and name ne 'RDUR'        $
             and name ne 'ROLL'        $
             and name ne 'ROLL_CENTER' $
                ,N_others)
  endif else begin
    others=WHERE(name ne 'DATA'        $
             and name ne 'XC'          $
             and name ne 'YC'          $
             and name ne 'DX'          $
             and name ne 'DY'          $
             and name ne 'START'       $
             and name ne 'RTIME'       $
             and name ne 'ROLL'        $
             and name ne 'ROLL_CENTER' $
                ,N_others)
  endelse
  for itag=0,N_others-1 do map_drot.(others(itag))=map.(others(itag))


;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;%%% Differentially rotating map data
;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  for i=0,Nmaps-1 do begin

;% Rotate coordinates

    xyr=DROT_COORD(map(i),t0,VERBOSE=verb,SPHERE=KEYWORD_SET(sphere) $
                  ,KEEP=keep_angles)
    xpr=xyr(*,*,0)
    ypr=xyr(*,*,1)
    xyr=0

;% Roll back coordinates to zero roll angle

    if map_roll(i) ne 0 then $
      ROLL_XY,xpr,ypr,-map_roll(i),CENTER=map_rcenter(*,i),xpr,ypr

;% Roll new coordinates to the final angle

    xpg=xpg0
    ypg=ypg0
    xcg=xcg0
    ycg=ycg0
    if roll(i) ne 0 then begin
      ROLL_XY,xpg,ypg,-roll(i),CENTER=rcenter(*,i),xpg,ypg
      ROLL_XY,xcg,ycg,-roll(i),CENTER=rcenter(*,i),xcg,ycg
    endif
    map_drot(i).xc=xcg
    map_drot(i).yc=ycg

;% Resample data

    if verb then PRINT,'%I> DROT_RASTER: Resampling data array...'
    map_drot(i).data=INTERP2D(map(i).data,xpr,ypr,xpg,ypg,[Nx,Ny],_EXTRA=_extra)

;% Estimate rotation interval on a pixel-by-pixel basis;
;% N.B.: no fancy interpolation keywords used here (_extra keywords); only 
;% using here keyword EXTRAPOLATE.
    if do_add_rdur then begin
      if multi_time then $
        dt=t0-map(i).start $
      else $
        dt=REPLICATE(t0-ANYTIM2TAI(map(i).time),Nx)
      dt=dt#REPLICATE(1,Ny)
      map_drot(i).rdur=INTERP2D(dt,xpr,ypr,xpg,ypg,[Nx,Ny],/EXTRAP)
    endif

  endfor


;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;%%% End
;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


  RETURN,map_drot
  END

