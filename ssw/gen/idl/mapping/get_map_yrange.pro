;+
; Project     : SOHO-CDS
;
; Name        : GET_MAP_YRANGE
;
; Purpose     : extract min/max Y-coordinate of map
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : yrange=get_map_yrange(map)
;
; Examples    :
;
; Inputs      : MAP = image map
;
; Opt. Inputs : None
;
; Outputs     : YRANGE = [ymin,ymax]
;
; Opt. Outputs: None
;
; Keywords    : ERR = error string
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 16 Feb 1998, D. Zarro, SAC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

function get_map_yrange,map,err=err

on_error,1

err=''
if not valid_map(map,err=err) then return,-1
if n_elements(map) ne 1 then begin
 err='cannot handle more than one map'
 message,err,/cont
 return,-1
endif


old_format=tag_exist(map,'yp')

if old_format then begin
 yrange=[min(map.yp),max(map.yp)]
endif else begin
 sz=size(map.data)
 ny=sz(2)
 dy=map.dy & yc=map.yc
 ymin=yc-dy*(ny-1.)/2.
 ymax=yc+dy*(ny-1.)/2.
 yrange=[ymin,ymax]
endelse

return,yrange

end
