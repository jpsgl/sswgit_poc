;+
; Project     : SOHO-CDS
;
; Name        : GET_IMG_CENTER
;
; Purpose     : compute center of input coordinates array
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : center=get_img_center(array)
;
; Examples    :
;
; Inputs      : ARRAY = 1 or 2d coordinate array
;
; Opt. Inputs : None
;
; Outputs     : CENTER = center coordinate value
;
; Opt. Outputs: None
;
; Keywords    : ERR = error string
;               DX, DY = mean pixel spacing in x and y directions
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 16 Feb 1998, D. Zarro, SAC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

function get_img_center,array,err=err,dx=dx,dy=dy

on_error,1
center=-9999.
dx=0. & dy=0.

if not exist(array) then return,center
amin=min(array)
amax=max(array)
center=(amin+amax)/2.

sz=size(array)
if sz(0) eq 2 then begin
 nx=sz(1) & ny=sz(2)
endif else begin
 nx=sz(1) & ny=1
endelse

dx=(amax-amin)/(nx-1.)
dy=dx
if (ny gt 1.) then dy=(amax-amin)/(ny-1.)

return,center & end

