;+
; Project     : SOHO-CDS
;
; Name        : VIEW_MAP_XY
;
; Purpose     : wrapper around SOHO_XY to check for roll in map
;
; Category    : imaging
;
; Explanation : for rolled images, coordinates have to be corrected
;               before applying solar view correction
;
; Syntax      : view_map_xy,map,xp,yp
;
; Examples    :
;
; Inputs      : MAP = map structure
;
; Opt. Inputs : None
;
; Outputs     : XP,YP = view-adjusted coordinate arrays
;
; Opt. Outputs: None
;
; Keywords    : EARTH = map to EARTH-view
;               SOHO = map to SOHO-view
;               ERR = error string
;               VERBOSE = print messages
;  
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 15 Feb 1999, D. Zarro, SM&A/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

pro view_map_xy,map,xp,yp,earth=earth,soho=soho,err=err,$
                verbose=verbose
on_error,1
err=''

if not valid_map(map) then begin
 pr_syntax,'view_map_xy,map,xp,yp,earth=earth,[soho=soho]'
 err='input error'
 return
endif

;-- check view direction

verbose=keyword_set(verbose)
to_earth=keyword_set(earth)
to_soho=keyword_set(soho)

if (to_earth eq to_soho) then begin
 err='use /earth or /soho'
 message,err,/cont
 return
endif
 
if tag_exist(map,'soho') then curr_view=map.soho else curr_view=0

if to_earth and (curr_view eq 0) then begin
 message,'already in EARTH-view',/cont
 return
endif

if to_soho and (curr_view eq 1) then begin
 message,'already in SOHO-view',/cont
 return
endif
                        
xp=get_map_xp(map) & yp=get_map_yp(map)
date=get_map_time(map)                      

;-- correct for non-zero roll in map before adjusting

rflag=0
if tag_exist(map,'roll') then begin
 roll=map.roll
 if roll ne 0. then begin
  if tag_exist(map,'roll_center') then $
   roll_center=map.roll_center else roll_center=[0.,0.]
  if verbose then message,'correcting for '+num2str(roll)+' deg roll',/cont
  roll_xy,xp,yp,-roll,xp,yp,center=roll_center
  rflag=1
 endif
endif

;-- now adjust the view
                       
soho_xy,xp,yp,date,xp,yp,err=err,inverse=to_earth

;-- roll back to original roll

if rflag then roll_xy,xp,yp,roll,xp,yp,center=roll_center

return & end


