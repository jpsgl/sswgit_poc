;+
; Name: plot_map_contour
;
; Purpose: include file for plot_map
;-

if prange(0) eq prange(1) then prange=[pmin,pmax]

if cont then begin

 pic=cscale(pic,max=prange(1),min=prange(0),/no_copy,$
            missing=missing,err=err,log=dlog,/no_color)
 if err ne '' then goto,done

 cmin=min(prange,max=cmax)
 if dlog then begin
  cmin=alog10(cmin)
  cmax=alog10(cmax)
 endif
 
;-- establish contour levels

 corange=[cmin,cmax]
 nlev=n_elements(levels)
 if nlev eq 0 then begin
  nlev=10
  c_levels=corange(0)+findgen(nlev)*(corange(1)-corange(0))/(nlev-1.)
 endif else begin
  c_levels=levels
  if dlog then begin
   ok=where(c_levels gt 0.,nlev)
   if nlev gt 0 then c_levels=alog10(c_levels(ok)) else begin
    err='cannot plot logarithms of zero or negative contours'
    message,err,/cont
    return
   endelse
  endif
 endelse

;-- establish default contour properties

 c_thick=1 & c_style=0 & c_color=white 
 if exist(cthick) then c_thick=fix(cthick) 
 if exist(cstyle) then c_style=fix(cstyle)
 if exist(lcolor) then c_color=byte(lcolor)
 clabel=keyword_set(clabel)
 
;-- tailor contours

 if keyword_set(tail) then begin        
  print,'* default is equispaced contours between:'
  print,' MIN = ',corange(0),' and MAX = ',corange(1)
  print,'* enter explicit values for contour levels'
  clev='' & read,'----> ',clev
  if clev eq '' then begin
   nlev='' & read,'* enter number of equispaced contour levels for image [def=10]: ',nlev
   if nlev eq '' then nlev=10 else nlev=fix(nlev)
   c_levels=corange(0)+findgen(nlev)*(corange(1)-corange(0))/(nlev-1.)
  endif else c_levels=float(str2arr(clev))

  ans='' & read,'* label contours [def=n]? ',ans
  if ans eq '' then ans='n' & ans=strupcase(strmid(ans,0,1))
  clabel=ans eq 'Y'
  c_style='' & read,'* enter contour linestyle [def=0, solid]: ',c_style
  c_style=fix(c_style)

  c_thick='' & read,'* enter contour thickness [def=1]: ',c_thick
  if c_thick eq '' then c_thick=1. & c_thick=fix(c_thick)
 endif

;-- sort the whole thing out

 cs=uniq([c_levels],sort([c_levels]))
 c_levels=c_levels(cs)
 nlev=n_elements(c_levels)
 if nlev lt 2 then begin
  err='need more than one contour level'
  message,err,/cont
  return
 endif
 
 c_labels=intarr(nlev)
 if clabel then begin
  c_labels=c_levels & odd=where(findgen(nlev) mod 2) & c_labels(odd)=0.
 endif                

 c_thick=replicate(c_thick,nlev)
 c_style=replicate(c_style,nlev)
 c_color=replicate(c_color,nlev)
 
endif

