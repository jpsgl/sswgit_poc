;+
; Project     : SOHO-CDS
;
; Name        : MAP2SOHO
;
; Purpose     : convert EARTH-view image map to SOHO-view
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : map2soho,map
;
; Examples    :
;
; Inputs      : MAP = image map structure
;
; Opt. Inputs : None
;
; Outputs     : MAP = remapped structure 
;
; Opt. Outputs: None
;
; Keywords    : INVERSE = set to map SOHO to EARTH-view
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 22 April 1997, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-


pro map2soho,map,inverse=inverse

if not valid_map(map,old=old_format) then begin
 message,'Invalid input map',/cont
 return
endif
if (old_format) then begin
 message,'Old format not supported',/cont
 return
endif

inverse=keyword_set(inverse)

nmap=n_elements(map)
for i=0,nmap-1 do begin
 dx=get_map_prop(map(i),/dx)
 dy=get_map_prop(map(i),/dy)
 soho=get_map_prop(map(i),/soho,def=0b,/quiet)
 time=get_map_prop(map(i),/time)
 rfac=soho_fac(time) & dfac=1. & dsoho=soho
 if (1-inverse) then begin
  if (not soho) then begin
   dfac=rfac
   dsoho=1b
  endif
 endif else begin
  if (soho) then begin
   dfac=1./rfac
   dsoho=0b
  endif
 endelse
 dprint,'% MAP2SOHO: ',dfac
 map(i).dx=dfac*dx
 map(i).dy=dfac*dy
 map(i).soho=dsoho
endfor


return & end


