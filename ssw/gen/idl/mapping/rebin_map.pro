;+
; Project     : SOHO-CDS
;
; Name        : REBIN_MAP
;
; Purpose     : Rebin an image map to new dimensions
;
; Category    : imaging
;
; Explanation : Rebin a map to user-specified dimensions and
;               compute new output pixel spacings
;
; Syntax      : gmap=rebin_map(map,gx,gy)
;
; Examples    :
;
; Inputs      : MAP = image map structure
;               GX,GY = new dimensions
;
; Opt. Inputs : None
;
; Outputs     : GMAP = rebinned map
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 22 August 1997, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

function rebin_map,map,gx,gy,err=err,_extra=extra

on_error,1

;-- check inputs (valid map & dimensions)

if (not valid_map(map,old=old)) or (not exist(gx)) then begin
 pr_syntax,'gmap=rebin_map(map,gx,gy)'
 if exist(map) then return,map else return,-1
endif
if not exist(gy) then gy=gx

for i=0,n_elements(map)-1 do begin
 err=''
 tmap=map(i)
 if old then tmap=mk_map_new(temporary(tmap))
 unpack_map,tmap,data,xp,yp,dx=dx,dy=dy,nx=nx,ny=ny
 if (gx eq nx) and (gy eq ny) then begin
  message,'no rebinning necessary',/cont
  goto,jump
 endif
 data=congrid(temporary(data),gx,gy,_extra=extra)
 dx=dx*nx/gx
 dy=dy*ny/gy
 tmap=rep_tag_value(tmap,data,'data')
 tmap.dx=dx
 tmap.dy=dy
jump:
 gmap=merge_struct(gmap,tmap)
endfor

return,gmap & end

