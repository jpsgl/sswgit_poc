;+
; Project     : SOHO-CDS
;
; Name        : RESPACE_MAP
;
; Purpose     : Rebin an image map to new pixel spacing  
;
; Category    : imaging
;
; Explanation : Rebin a map to user-specified spacings and
;               compute new output dimensions
;
; Syntax      : gmap=respace_map(map,sx,sy)
;
; Examples    :
;
; Inputs      : MAP = image map structure
;               SX,SY = new (x,y) spacing
;
; Opt. Inputs : None
;
; Outputs     : GMAP = rebinned map
;
; Opt. Outputs: None
;
; Keywords    : 
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 22 March 1998, D. Zarro, SAC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

function respace_map,map,sx,sy,err=err,_extra=extra,max_dim=max_dim

on_error,1
err=''

;-- check inputs (valid map & spacings)

if (not valid_map(map,old=old)) or (not exist(sx)) then begin
 pr_syntax,'gmap=respace_map(map,sx,sy)'
 if exist(map) then return,map else return,-1
endif
if not exist(sy) then sy=sx

asked=0
for i=0,n_elements(map)-1 do begin
 err=''
 tmap=map(i)
 if old then tmap=mk_new_map(temporary(tmap))
 unpack_map,tmap,data,xp,yp,dx=dx,dy=dy,nx=nx,ny=ny
 if (sx eq dx) and (sy eq dy) then begin
  message,'no rebinning necessary',/cont
  goto,jump
 endif

;-- compute and check new output dimensions

 gx=nint(dx*nx/sx)
 gy=nint(dy*ny/sy)
 if not asked then begin
  if (gx gt 1024) or (gy gt 1024) then begin
   message,'excessive new output dimensions - '+trim(string(gx))+','+trim(string(gy)),/cont
   ans='' & read,'* continue [def=n]?',ans
   ans=strmid(strupcase(ans),0,1)
   if ans ne 'Y' then begin
    err='Aborted' & message,err,/cont
    return,map
   endif
   asked=0
  endif
 endif
 data=congrid(temporary(data),gx,gy,_extra=extra)
 tmap=rep_tag_value(tmap,data,'data')
 tmap.dx=sx
 tmap.dy=sy
jump:
 gmap=merge_struct(gmap,tmap)
endfor

return,gmap & end

