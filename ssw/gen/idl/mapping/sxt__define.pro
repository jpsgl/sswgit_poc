;+
; Project     : HESSI
;
; Name        : SXT__DEFINE
;
; Purpose     : Define an SXT map object
;
; Category    : imaging maps
;
; Explanation : The map object is actually a structure with a pointer
;               field. The map structure is stored in this pointer.
;               The SXT object inherits MAP object properties and methods,
;               and includes a special SXT reader
;               ;
; Syntax      : This procedure is invoked when a new SXT object is
;               created via:
;
;               IDL> new=obj_new('sxt')
; Examples    :
;
; Inputs      : 'SXT' = object classname (a subclass of MAP)
;
; Opt. Inputs : map = map structure created by MAKE_MAP
;
; Outputs     : A MAP object with methods: SET, GET, & PLOT
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 19 May 1998, D. Zarro, SM&A/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;+

;-- define SXT object

pro sxt__define                 

sxt_struct={sxt, inherits map}

return & end

;-- SXT reader 

pro sxt::read,file,dset,err=err,_extra=extra
 
err=''
if datatype(file) ne 'STR' then begin
 err='Invalid input file'
 message,err,/cont
 return
endif

chk=loc_file(file,count=count,err=err)
if count ne 1 then begin
 message,err,/cont
 return
endif

if not exist(dset) then dset=-1
read_sxt,file,dset,index,data,_extra=extra
 
index2map,index,data,map

self->set,map

end

