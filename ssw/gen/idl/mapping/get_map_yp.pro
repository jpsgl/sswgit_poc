;+
; Project     : SOHO-CDS
;
; Name        : GET_MAP_YP
;
; Purpose     : extract Y-coordinate arrays of map
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : yp=get_map_yp(map)
;
; Examples    :
;
; Inputs      : MAP = image map
;
; Opt. Inputs : None
;
; Outputs     : YP = 2d X-coordinate array
;
; Opt. Outputs: None
;
; Keywords    : ERR = error string
;               DY  = y-pixel spacing
;               YC  = y-pixel center coordinate
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 16 Feb 1998, D. Zarro, SAC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

function get_map_yp,map,dy=dy,yc=yc,err=err

on_error,1

err=''
if not valid_map(map,err=err) then return,-1
if n_elements(map) ne 1 then begin
 err='cannot handle more than one map'
 message,err,/cont
 return,-1
endif

old_format=tag_exist(map,'yp')
if old_format then begin
 yp=map.yp 
 yc=get_img_center(yp,dy=dy)
endif else begin
 sz=size(map.data)
 nx=sz(1) & ny=sz(2)
 dy=map.dy
 yc=map.yc
 yp=mk_map_yp(yc,dy,nx,ny)
endelse

return,yp

end
