;+
; Project     : SOHO-CDS
;
; Name        : MAKE_XY
;
; Purpose     : Make a uniform 2d X-Y grid of coordinates
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : make_xy,nx,ny,xp,yp
;
; Examples    :
;
; Inputs      : NX,NY = X,Y dimensions
;
; Opt. Inputs : None
;
; Outputs     : XP,YP = 2d (X,Y) coordinates
;
; Opt. Outputs: None
;
; Keywords    : DX, DY = grid spacing [def=1,1]
;               XC, YC = grid center  [def=0,0]
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 22 June 1997, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

pro make_xy,nx,ny,xp,yp,xc=xc,yc=yc,dx=dx,dy=dy

if n_params(0) ne 4 then begin
 message,'Syntax: make_xy,nx,ny,xp,yp,[xc=xc,yc=yc,dx=dx,dy=dy]',/cont
 return
endif

if not exist(dx) then dx=1.
if not exist(dy) then dy=1.
if not exist(xcen) then xcen=0.
if not exist(ycen) then ycen=0.

dumx = nx*dx/2. & dumy=ny*dy/2.
xcor =(findgen(nx)+.5)*dx - dumx + xcen
ycor =(findgen(ny)+.5)*dy - dumy + ycen
xp=rebin(xcor,nx,ny)
yp=rebin(ycor,ny,nx)
yp=rotate(temporary(yp),1)

delvarx,xcor,ycor

return & end

