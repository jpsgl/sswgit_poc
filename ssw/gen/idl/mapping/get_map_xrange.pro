;+
; Project     : SOHO-CDS
;
; Name        : GET_MAP_XRANGE
;
; Purpose     : extract min/max X-coordinate of map
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : xrange=get_map_xrange(map)
;
; Examples    :
;
; Inputs      : MAP = image map
;
; Opt. Inputs : None
;
; Outputs     : XRANGE = [xmin,xmax]
;
; Opt. Outputs: None
;
; Keywords    : ERR = error string
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 16 Feb 1998, D. Zarro, SAC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

function get_map_xrange,map,err=err

on_error,1

err=''
if not valid_map(map,err=err) then return,-1
if n_elements(map) ne 1 then begin
 err='cannot handle more than one map'
 message,err,/cont
 return,-1
endif


old_format=tag_exist(map,'xp')

if old_format then begin
 xrange=[min(map.xp),max(map.xp)]
endif else begin
 sz=size(map.data)
 nx=sz(1)
 dx=map.dx & xc=map.xc
 xmin=xc-dx*(nx-1.)/2.
 xmax=xc+dx*(nx-1.)/2.
 xrange=[xmin,xmax]
endelse

return,xrange

end
