;+
; Project     : SOHO-CDS
;
; Name        : SOHO_CORD
;
; Purpose     : convert EARTH-view coordinates to SOHO-view
;
; Category    : imaging
;
; Explanation : convert projected (x,y) grid on solar surface from EARTH-view
;               to SOHO-view, and vice-versa
;
; Syntax      : soho_cord,x,y,date,xs,ys
;
; Examples    :
;
; Inputs      : X,Y = input coordinates (arcsec) (+W, +N)
;               DATE = date of observations
;
; Opt. Inputs : None
;
; Outputs     : XS,YS = output coordinates (arcsec)
;
; Opt. Outputs: None
;
; Keywords    : INVERSE = set to convert SOHO to EARTH-view
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 15 April 1998, D. Zarro, SAC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-


pro soho_cord,x,y,date,xs,ys,err=err,inverse=inverse

on_error,1
err=''
tdate=anytim2utc(date,err=err)

if (err ne '') or (not exist(x)) or (not exist(y)) or $
 data_chk(x,/ndim) ne data_chk(y,/ndim) then begin
 err='input error'
 pr_syntax,'soho_cord,x,y,date,xs,ys'
 return
endif

;-- check if input is 2-d (x and y dimensions need to be the same)

twod=0
if data_chk(x,/ndim) eq 2 then begin
 twod=1
 nx=data_chk(x,/nx) & ny=data_chk(y,/ny)
 xs=reform(x,nx*ny)/60. & ys=reform(y,nx*ny)/60.
endif else begin
 xs=x/60. & ys=y/60.
endelse

;-- flag off limb points

pb=pb0r(tdate,soho=inverse,/retain)
radius=float(pb(2))
off_limb=where(sqrt(xs^2+ys^2) gt radius,ocount)
if ocount gt 0 then begin
 xs_off=xs(off_limb)
 ys_off=ys(off_limb)
endif

;-- transform to heliographic

inverse=keyword_set(inverse)
solar_helio=arcmin2hel(xs,ys,date=tdate,soho=inverse,/no_copy)

if inverse then dprint,'% SOHO_CORD: mapping from SOHO- to Earth-view' else $
 dprint,'% SOHO_CORD: mapping from Earth- to SOHO-view'

;-- transform back to cartesian

soho_xy=hel2arcmin(solar_helio(0,*),solar_helio(1,*),date=tdate,soho=1-inverse)

if twod then begin
 xs=reform(soho_xy(0,*),nx,ny)*60.
 ys=reform(soho_xy(1,*),nx,ny)*60.
endif else begin
 xs=reform(soho_xy(0,*)*60.)
 ys=reform(soho_xy(1,*)*60.)
endelse

;-- set off limb points back to original values

if ocount gt 0 then begin
 xs(off_limb)=xs_off
 ys(off_limb)=ys_off
endif

if n_elements(xs) eq 1 then xs=xs(0)
if n_elements(ys) eq 1 then ys=ys(0)

delvarx,solar_helio,soho_xy

return & end

