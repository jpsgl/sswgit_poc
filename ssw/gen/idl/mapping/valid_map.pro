;+
; Project     : SOHO-CDS
;
; Name        : VALID_MAP
;
; Purpose     : check if input image map is of valid type
;
; Category    : imaging
;
; Syntax      : valid=valid_map(map)
;
; Inputs      : MAP = image map structure
;
; Outputs     : VALID = 1/0 if valid/invalid
;
; Keywords    : OLD_FORMAT = 1/0 if using old .xp, .yp format or not
;
; History     : Written 22 October 1997, D. Zarro, SAC/GSFC
;               Modified 22 December 2002, Zarro (EER/GSFC) - added where_vector call
;
; Contact     : dzarro@solar.stanford.edu
;-

function valid_map,map,err=err,old_format=old_format

old_format=0b

if not is_struct(map) then return,0b

tags=tag_names(map(0))

req_tags=['data','xc','yc','dx','dy','time','id']

check=where_vector(req_tags,tags,count)

return,count eq n_elements(req_tags)

;--------------------------------------------------------------------------------------------

dprint,'% VALID_MAP: ',get_caller()

err='invalid input map'
valid=0b

;-- check if true MAP object (IDL > 5)

sz=size(map)
dtype=sz(n_elements(sz)-2)
if dtype eq 11 then begin
 if not call_function('obj_valid',map(0)) then return,valid
 if call_function('obj_class',map(0)) ne 'MAP' then return,valid
 stat=execute('valid=valid_map(map(0)->get(),old_format=old_format)')
 return,valid
endif

;-- otherwise check for required tags

if dtype ne 8 then return,valid
if not tag_exist(map,'DATA') then return,valid
if not tag_exist(map,'TIME') then return,valid

old_format=tag_exist(map,'xp') and tag_exist(map,'yp')
if not old_format then begin
 if not tag_exist(map,'XC') then return,valid
 if not tag_exist(map,'YC') then return,valid
 if not tag_exist(map,'DX') then return,valid
 if not tag_exist(map,'DY') then return,valid
endif

;-- if we made it here then we're ok

err=''
valid=1b
return,valid & end

