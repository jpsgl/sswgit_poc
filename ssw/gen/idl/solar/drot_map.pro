;+
; Project     : SOHO-CDS
;
; Name        : DROT_MAP
;
; Purpose     : differentially rotate image contained within structure created by MAKE_MAP
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : rmap=drot_map(map,duration)
;
; Examples    :
;
; Inputs      : MAP = map structure created by MAKE_MAP
;               DURATION = hours to rotate by
;
; Opt. Inputs : None
;
; Outputs     : RMAP = map with rotated coordinates
;
; Opt. Outputs: None
;
; Keywords    : INTERP = for bilinear interpolation [def = none]
;               SECONDS = duration units in seconds
;               DEGREE = degree of fitting [def = 2]
;               TIME = time to rotate image to
;               CORDS_ONLY = only rotate coordinates (not image)
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 22 November 1996, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

function drot_map,map,duration,seconds=seconds,interp=interp,degree=degree,$
                     time=time,soho=soho,cords_only=cords_only

if exist(interp) then interp=1 else interp=0
seconds=keyword_set(seconds)

cur_time=anytim2tai(map.time)
if not exist(duration) then begin
 err=''
 tend=anytim2tai(time,err=err)
 if err eq '' then dtime=(tend-cur_time)
 if not exist(dtime) then begin
  repeat begin
   dtime='' & read,'* enter duration (hours) to rotate by : ',dtime
  endrep until dtime ne ''
  dtime=float(dtime)*3600.
 endif
endif else begin
 duration=dtime
 if not seconds then dtime=dtime*3600.
endelse

;-- rotate coordinates

dprint,'%duration ',dtime
rmap=map
if tag_exist(map,'SOHO') then soho=map.soho else soho=keyword_set(soho)
new_time=cur_time+dtime
nx=(size(map.xp))(1)
ny=(size(map.xp))(2)
rx=reform(map.xp,nx*ny)
ry=reform(map.yp,nx*ny)
rcor=rot_xy(rx,ry,tstart=cur_time,tend=new_time,soho=soho,/keep)
sz=size(rcor)
if sz(2) eq 2 then rcor=transpose(rcor)

;-- update pixel arrays 

rmap.xp=reform(rcor(0,*),nx,ny)
rmap.yp=reform(rcor(1,*),nx,ny)
rmap.time=anytim2utc(new_time,/ecs)

;-- rebin image

if not keyword_set(cords_only) then begin
 if not exist(degree) then degree=2
 polywarp,map.xp,map.yp,rmap.xp,rmap.yp,degree,p,q 
 rmap.data=poly_2d(map.data,p,q,interp)
endif

return,rmap & end

