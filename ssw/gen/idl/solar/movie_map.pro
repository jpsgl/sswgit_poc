;+
; Project     : SOHO-CDS
;
; Name        : MOVIE_MAP
;
; Purpose     : make movie of series of map images
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : movie_map,map
;
; Examples    :
;
; Inputs      : MAP = array of map structures created by MAKE_MAP
;
; Opt. Inputs : None
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : ROTATE = differentially rotate images
;               TREF = reference time to rotate to [def = first image]
;               GIF = make series of GIF files
;               MAX = data max to scale to
;               CMAP = contour map to overlay
;               CMIN,CMAX = min/max values for contoured data
;               TSTART/TSTOP = movie start/stop times
;               CROTATE = diff_rot contour images
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 22 November 1996, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

pro movie_map,map,tref=tref,xsize=xsize,ysize=ysize,_extra=extra,trans=trans,$
 rotate=rotate,gif=gif,max=dmax,min=dmin,cmap=cmap,cmax=cmax,cmin=cmin,$
 tstart=tstart,tstop=tstop,use_cont=use_cont,ctol=ctol,crotate=crotate

;-- setup window

get_xwin,index,xsize=xsize,ysize=ysize
window=!d.window
sx=!d.x_size & sy=!d.y_size

cont=exist(cmap)
if cont then begin
 ctimes=anytim2tai(cmap.time)
; if n_elements(cmap) ne nmap then begin
;  message,'# of contour images must equal # of movie images',/cont
;  return
; endif
endif

;-- handle times

mtimes=anytim2tai(map.time)
mvtimes=mtimes
use_cont=keyword_set(use_cont)
if cont and use_cont then mvtimes=ctimes
tmin=min(mvtimes)
tmax=max(mvtimes)
err=''
pstart=anytim2tai(tstart,err=err)
if err ne '' then pstart=tmin
err=''
pstop=anytim2tai(tstop,err=err)
if err ne '' then pstop=tmax

do_movie=where( (mtimes ge pstart) and (mtimes le pstop),count)
if count eq 0 then begin
 message,'no images during specified TSTART/STOP',/cont
 return
endif

;-- start movie

if not exist(dmax) then dmax=max(temporary(map(do_movie).data))
if not exist(dmin) then dmin=min(temporary(map(do_movie).data)) > 0
dprint,'%dmax, ',dmax
dprint,'%dmin, ',dmin

rotate=keyword_set(rotate)
crotate=keyword_set(crotate)
err=''
time=anytim2tai(tref,err=err)
if (err ne '') and (rotate or crotate) then time=map(do_movie(0)).time

xinteranimate,set=[sx,sy,count]
if not exist(ctol) then ctol=0

for j=0,count-1 do begin
 i=do_movie(j)
 temp=map(i)
 if rotate then temp=drot_map(map(i),time=time) else temp=map(i)
 temp.time=map(i).time
 plot_map,temp,xsize=xsize,ysize=ysize,window=window,_extra=extra,max=dmax,$
  min=dmin,trans=trans,rotate=rotate
 if cont then begin
  diff=abs(anytim2tai(map(i).time)-ctimes)
  use=where(diff eq min(diff))
  dprint,'% base image time:    '+map(i).time
  dprint,'% contour image time: '+cmap(use).time
  if ctol eq 0 then do_it=1 else do_it=(min(diff) le ctol)
  if do_it then begin
   plot_map,cmap(use),_extra=extra,/over,max=cmax,min=cmin,rotate=crotate,$
    tref=time,/pos,lcolor=2
   plot_map,cmap(use),_extra=extra,/over,max=cmax,min=cmin,rotate=crotate,$
    tref=time,/neg,lcolor=5
  endif else dprint,'% diff: ',min(diff)
 endif
 window=!d.window
 xinteranimate,frame=j,window=[window,0,0,sx,sy]
 if keyword_set(gif) then begin
  counter=trim(string(j))
  if j lt 10 then counter='0'+counter
  gif_file='frame'+counter+'.gif'
  x2gif,gif_file
 endif 
endfor

xinteranimate,/keep

if keyword_set(gif) then begin
 spawn,'ls frame*.gif > agif.lis'
 message,'created GIF filenames are listed in file: agif.lis',/cont
endif

return & end

