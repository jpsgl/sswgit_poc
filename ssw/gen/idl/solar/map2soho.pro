;+
; Project     : SOHO-CDS
;
; Name        : MAP2SOHO
;
; Purpose     : convert EARTH-view image map coordinates to SOHO-view
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : smap=map2soho(map)
;
; Examples    :
;
; Inputs      : MAP = image map structure
;
; Opt. Inputs : None
;
; Outputs     : SMAP = remapped structure 
;
; Opt. Outputs: None
;
; Keywords    : INVERSE = set to map SOHO to EARTH-view
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 22 April 1997, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-


function map2soho,map,inverse=inverse

if datatype(map) ne 'STC' then begin
 message,'input must be structure',/cont
 if exist(map) then return,map else return,0
endif

inverse=keyword_set(inverse)

if not inverse then begin
 if map.soho then begin
  message,'already in SOHO-view system',/cont
  return,map
 endif
endif else begin
 if not map.soho then begin
  message,'already in EARTH-view system',/cont
  return,map
 endif
endelse

sz=size(map.data)
nx=sz(1) & ny=sz(2)
xs=reform(map.xp,nx*ny)/60.
ys=reform(map.yp,nx*ny)/60.
new=arcmin2hel(xs,ys,date=map.time,soho=1-inverse)
new2=hel2arcmin(new(0,*),new(1,*),date=map.time,soho=inverse)
nmap=map
nmap.xp=reform(new2(0,*),nx,ny)*60.
nmap.yp=reform(new2(1,*),nx,ny)*60.
nmap.soho=1-inverse
return,nmap & end


