;+
; Project     : SOHO-CDS
;
; Name        : MK_EIT_MAP
;
; Purpose     : Make an image map from EIT FITS data
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : map=mk_eit_map(data,header)
;
; Examples    :
;
; Inputs      : DATA,HEADER = FITS image/header combination
;
; Opt. Inputs : None
;
; Outputs     : MAP = map structure
;
; Opt. Outputs: None
;
; Keywords    : SUB = set to compute subarray
;               DEGRID = set to degrid
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 22 January 1997, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-


function mk_eit_map,data,header,degrid=degrid,sub=sub

;-- degrid?

if keyword_set(degrid) and exist(header) then begin
 response_dir=concat_dir(getenv('SSW_EIT'),'response',/dir)
 if chk_dir(response_dir) then begin
  eit_image=itool_eit_degrid(temporary(data-eit_dark()),header)
 endif
endif else eit_image=data

;-- decode header

stc=head2stc(header)
dx=float(stc.cdelt1)
dy=float(stc.cdelt2)
xcen=float(stc.naxis1+1)*dx/2.
ycen=float(stc.naxis2+1)*dy/2.
xorg=float(stc.crpix1)*dx
yorg=float(stc.crpix2)*dy
xc=xcen-xorg
if tag_exist(stc,'crval1') then xc=xc+stc.crval1
yc=ycen-yorg
if tag_exist(stc,'crval2') then yc=yc+stc.crval2

;-- extract sub-array

if keyword_set(sub) then begin
 exptv,alog10(eit_image > .01)
 wshow
 sub_data=temporary(tvsubimage(eit_image,x1,x2,y1,y2))
 delvarx,sub_data
 sub_array=[x1,x2,y1,y2]
endif

;-- make map
 
map=make_map(eit_image,xc,yc,dx,dy,time=anytim2utc(stc.date_obs,/ecs),$
           id='EIT: '+stc.wavelnth,/soho,sub=sub_array)

return,map & end

