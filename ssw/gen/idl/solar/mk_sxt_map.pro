;+
; Project     : SOHO-CDS
;
; Name        : MK_SXT_MAP
;
; Purpose     : Make an image map from an SXT index/data structure
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : map=mk_sxt_map(index,data)
;
; Examples    :
;
; Inputs      : INDEX,DATA = index/data combination
;
; Opt. Inputs : None
;
; Outputs     : MAP = map structure from MK_MAP
;
; Opt. Outputs: None
;
; Keywords    : SUB = [x1,x2,y1,y2] = indicies of subarray to extract
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 22 January 1997, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

function mk_sxt_map,index,data,sub=sub,use_hist=use_hist,$
         tstart=tstart,tstop=tstop

if datatype(index) ne 'STC' then begin
 message,'enter an SXT index structure',/cont
 return,0
endif

xc=gt_center(index,/ang,/x,use_hist=use_hist)
yc=gt_center(index,/ang,/y,use_hist=use_hist)
dx=gt_pix_size(index)
dy=dx

np=n_elements(index)
times=gt_day(index,/str)+' '+gt_time(index,/str)
if not exist(tstart) then tstart=anytim2tai(times(0)) else tstart=anytim2tai(tstart)
if not exist(tstop) then tstop=anytim2tai(times(np-1)) else tstop=anytim2tai(tstop)

for i=0,n_elements(index)-1 do begin
 time=anytim2tai(times(i))
 if (time ge tstart) and (time le tstop) then begin
  map=make_map(data(*,*,i),xc,yc,dx,dy,time=times(i),$
             dur=gt_expdur(index(i))/1000.,id=gt_filtb(index(i),/str),sub=sub)

  img=concat_struct(img,map)
 endif

endfor

if not exist(img) then begin
 message,'No images during specified times',/cont
 img=0
endif

return,img

end
