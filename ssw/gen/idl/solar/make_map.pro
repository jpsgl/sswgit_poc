;+
; Project     : SOHO-CDS
;
; Name        : MAKE_MAP
;
; Purpose     : Make an image map 
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : map=make_map(data,xcen,ycen,dx,dy,time=time,id=id,soho=soho)
;
; Examples    :
;
; Inputs      : DATA = 2d data array
;               XCEN,YCEN = center of image in arcsec units
;               DX,DY = pixel spacing (arcsecs) in X and Y directions
;               TIME = time of image in UTC format
;               SOHO = 0/1 for earth/soho-view
;               DUR  = duration of map (in secs)
;
; Opt. Inputs : None
;
;
; Outputs     : MAP ={data:data,xp:xp,yp:yp,id:id,time:time,dur:dur,soho:soho}
;               where,
;               DATA  = 2d image array
;               XP,YP = 2d cartesian coordinate arrays
;               ID    = ID label
;               TIME  = start time of image
;               DUR   = seconds duration of map [if 0 then unknown]
;               SOHO  = 1, flag identifying that image is SOHO-viewed
;               SUB   = [x1,x2,y1,y2] = indicies of sub-array to extract
;
; Opt. Outputs: None
;
; Keywords    : ID   = optional ID string
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 22 October 1996, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

function make_map,data,xcen,ycen,dx,dy,time=time,id=id,soho=soho,$
                  dur=dur,sub=sub

;-- check image

sz=size(data)
if sz(0) ne 2 then begin
 message,'input image must be 2-d',/cont
 return,0
endif

;-- check time

err=''
stime=anytim2utc(time,/ecs,err=err)
if err ne '' then begin
 message,err,/cont
 return,0
endif

;-- some defaults (Sun center, unit pixel scale)

if not exist(soho) then soho=0 else soho=1 > soho > 0
if not exist(xcen) then xcen=0.
if not exist(ycen) then ycen=0.
if not exist(dx) then dx=1.
if not exist(dy) then dy=dx
if not exist(id) then id=' '
if not exist(dur) then dur=0.

xcen=xcen(0) & ycen=ycen(0)
dx=dx(0) & dy=dy(0)
nx=sz(1) & ny=sz(2)
dumx = nx*dx/2. & dumy=ny*dy/2.
xcor =(findgen(nx)+.5)*dx - dumx + xcen
ycor =(findgen(ny)+.5)*dy - dumy + ycen
xp=rebin(xcor,nx,ny)
yp=rotate(rebin(ycor,ny,nx),1)

;-- extract subarray?

if n_elements(sub) eq 4 then begin
 x1=sub(0) < (nx-1) & x2=sub(1) < (nx-1)
 y1=sub(2) < (ny-1) & y2=sub(3) < (ny-1)
endif else begin
 x1=0 & x2=nx-1
 y1=0 & y2=ny-1
endelse

map={data:data(x1:x2,y1:y2),xp:xp(x1:x2,y1:y2),yp:yp(x1:x2,y1:y2),$
     time:stime,dur:dur,id:id,soho:soho}

return,map

end
