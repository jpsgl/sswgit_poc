;+
; Project     : SMM-XRP
;
; Name        : PLOT_MAP
;
; Purpose     : Plot an image map 
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : plot_map,map
;
; Examples    :
;
; Inputs      : MAP = image structure map created by MK_MAP
;
; Opt. Inputs : None
;
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : 
;     CONT = contour the image
;     SMOOTH = smooth the image
;     FOV = [fx,fy] = field of view to be plotted (arcmin; def= [4,4])
;     GRID = grid spacing (deg) for latitude-longitude grid (def= 0, no grid)
;     GLABEL = label grid with coordinate values (def = 0, no labels)
;     COLOR = colorize contours
;     CENTER = [xc,yc] = center coordinates of FOV (def = center of image)
;     MIN,MAX = min, max data for plot [def = data min,max]
;     BORDER = draw border around image [def = no]
;     PLABELS = plot labels
;     /DRAW = alerts PLOT_MAP that window is a DRAW widget
;     /TAIL = allows user to tailor contours
;     /LOG  = log_10 scale image
;     WINDOW = window index to send plot to
;     NOAXES = inhibit labelling axes
;     VELOCITY = scale data for velocity color table
;     COMBINED = scale data for combined color table
;     ROTATE  = permit solar rotation when overlaying contours from different
;               time 
;     LEVELS  = user specified contour levels
;     POSITIVE = plot positive data
;     NEGATIVE = plot negative data
;
; Restrictions: None
;      - do not set /GRID unless absolute coordinates of image are known
;      - do not set /OVER unless a plot exists on the current device
;      - do not set /COLOR unless a color output device exists
;
; Side effects: None
;
; History     : Written 22 December 1991, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

pro plot_map,img,cont=cont,over=over,smooth=smooth,border=border,$
 fov=fov,grid=grid,glabel=glabel,color=color,center=center,$
 plabels=plabels,draw=draw,gang=gang,tail=tail,log=log,$
 lcolor=lcolor,window=window,soho=soho,noaxes=noaxes,$
 xsize=xsize,ysize=ysize,new=new,rotate=rotate,levels=levels,$
 margin=margin,noscale=noscale,missing=missing,max=dmax,min=dmin,$
 top=top,velocity=velocity,combined=combined,lower=lower,_extra=extra,$
 tag_no=tag_no,trans=trans,positive=positive,negative=negative,$
 centroid=centroid,offset=offset,tref=tref,bottom=bottom,charsize=charsize,$
 cstyle=cstyle,cthick=cthick,date_only=date_only,time_only=time_only,font=font

;-- viewport, latest image, and latest plot mode saved in memory for overlay

common plot_map_com1,viewport,last_window,last_img,last_xmargin,last_ymargin,$
       last_charsize,last_font     

on_error,1
space='                                    '
done=0
clight=3.e5

;-- check input image

if datatype(img) ne 'STC' then begin
 plabels='input image undefined'
 message,plabels,/cont
 return
endif
have_time=tag_exist(img,'TIME')
have_id=tag_exist(img,'ID')

;-- examine keywords

if n_elements(grid) eq 0 then grid=0                   ;-- LAT-LON spacing
if n_elements(fov) eq 0 then fov=4                     ;-- FOV size (arcmin)
if n_elements(fov) lt 2 then fov=[fov,fov]
if n_elements(color) eq 0 then color=0                 ;-- default no color
if not exist(thick) then thick=1
if not exist(charthick) then charthick=1
if not exist(charsize) then charsize=1
if not exist(font) then font=-1

if n_elements(center) eq 0 then use_im=1 else begin
 use_im=0
 if n_elements(center) lt 2 then center=[center,center]
endelse

if keyword_set(over) then over=1 else over=0      ;-- overlay?
if keyword_set(cont) then cont=1 else cont=0      ;-- contour?
if keyword_set(smooth) then smo=1 else smo=0      ;-- smooth?
if keyword_set(border) then bord=1 else bord=0    ;-- plot image border?
if keyword_set(log) then log=1 else log=0         ;-- log scale

white=!d.n_colors-1 & black=0 
if n_elements(lcolor) eq 0 then begin
 if !d.name eq 'X' then lcolor=white else lcolor=black
endif

;-- always overlay as a contour

if over then cont=1 
if not over then begin last_img=img & endif

;-- gang plots

case n_elements(gang) of
  0 : begin !p.multi=0 & gang=[0,0] & end
  1 : begin
       if gang eq 0 then !p.multi=0 else !p.multi([1,2])=gang
      end
 else:!p.multi([1,2])=gang(0:1) 
endcase

over:           ;-- return loop for overlay

;-- read image and pixel coordinates 

if keyword_set(velocity) then begin
 vfound=0
 if tag_exist(img,'velocity') then begin
  pic=img.velocity
  vfound=1
 endif else begin
  if tag_exist(img,'centroid') then begin
   if not exist(centroid) then begin
    repeat begin
     ans='' & read,'* enter reference centroid: ',ans
     centroid=float(ans)
    endrep until (centroid gt 0)
   endif
   pic=clight*(img.centroid-centroid)/centroid
   vfound=1
  endif 
 endelse
 if not vfound then begin
  message,'No velocity data in image map',/cont
  return
 endif
endif else begin
 pic=img.data
 if exist(tag_no) then begin
  tags=tag_names(img)
  ntags=n_elements(tags)
  if (tag_no lt 0) or (tag_no ge ntags) then begin
   message,'Invalid tag number',/cont
   return
  endif
  pic=img.(tag_no)
 endif
endelse


sp=size(pic) 
if sp(0) ne 2 then begin
 plabels='Image not 2-d' &  message,plabels,/cont & return
endif

if exist(offset) then pic=pic-offset
if keyword_set(positive) then pic=pic > 0
if keyword_set(negative) then pic=pic < 0

soho=keyword_set(soho)
if not soho then begin
 if tag_exist(img,'SOHO') then soho=img.soho
endif
if soho then dprint,'% using SOHO view'

;-- pixel coordinates

xarr=img.xp & yarr=img.yp

;-- any offsets?

xoff=0. & yoff=0.
if (n_elements(trans) eq 2) then begin
 xoff=trans(0) & yoff=trans(1)
endif
xarr=xarr+xoff & yarr=yarr+yoff

;-- compute extrema, center, and pixel spacings for image
;-- (warning: these concepts are meaningless if image has been warped)

min_x=min(xarr) & max_x=max(xarr)
min_y=min(yarr) & max_y=max(yarr)
xb = [min_x,max_x,max_x,min_x,min_x]
yb = [min_y,min_y,max_y,max_y,min_y]
xp=(min_x+max_x)/2. &  yp=(min_y+max_y)/2.
xside=abs(max_x-min_x) & yside=abs(max_y-min_y)
sz=size(pic)
nx=sz(1) & ny=sz(2)
dx=xside/(nx-1.) & dy=yside/(ny-1.)
xarc=(xside+dx)/60. & yarc=(yside+dy)/60.

;-- image center

if use_im then center=[xp,yp] 
xc=center(0) & yc=center(1) & half_fov= (60.*fov)/2.     
dprint,'xc,yc,dx,dy,xarc,yarc',xc,yc,dx,dy,xarc,yarc

;-- open a new window if one doesn't exist

if (not over) then begin
 if (!d.name eq 'X') and (not keyword_set(draw)) then begin
  if keyword_set(new) then delvarx,last_window
  if (not exist(window)) and exist(last_window) then window=last_window
  get_xwin,window,xsize=xsize,ysize=ysize
 endif
 xmin=-half_fov(0)+xc & xmax=half_fov(0)+xc
 ymin=-half_fov(1)+yc & ymax=half_fov(1)+yc
 viewport=[xmin,xmax,ymin,ymax]              ;save viewport for overlay
endif else begin                             ;get viewport from previous plot
 if exist(window) then begin
  if window gt -1 then begin
   xmin=!x.crange(0) & xmax=!x.crange(1)
   ymin=!y.crange(0) & ymax=!y.crange(1)
  endif
 endif else begin
  xmin=viewport(0) & xmax=viewport(1)
  ymin=viewport(2) & ymax=viewport(3)
 endelse
endelse
if exist(window) then last_window=window

;-- adjust x and y margins to ensure a square image

if (not over) then begin
 skew=float(!d.x_ch_size)/float(!d.y_ch_size)
 xmargin=!x.margin & ymargin=!y.margin
 cl=where_vector(xmargin,ymargin*skew,count)
 if (count eq 0) or exist(margin) then begin
  if not exist(margin) then margin=xmargin(0)
  !x.margin=[margin,margin] & !y.margin=!x.margin*skew
 endif 
 last_xmargin=!x.margin
 last_ymargin=!y.margin
 last_charsize=charsize
 last_font=font
endif else begin
 if exist(last_xmargin) then !x.margin=last_xmargin
 if exist(last_ymargin) then !y.margin=last_ymargin
 if exist(last_charsize) then charsize=last_charsize
 if exist(last_font) then font=last_font
endelse

;-- select data within FOV

vfind=where( (xarr gt xmin) and (xarr lt xmax) and (yarr gt ymin) and $
             (yarr lt ymax) ,count)

if (count le 2) then begin
 plabels='only 2 data points within FOV' & message,plabels,/cont
 return
endif

;-- extract fov for image plots

if not cont then begin
 xy1=get_ij(vfind(0),nx)
 xy2=get_ij(vfind(count-1),nx)
 imin=[0,0]
 imax=imin
 imin(0)=min([xy1(0),xy2(0)])
 imin(1)=min([xy1(1),xy2(1)])
 imax(0)=max([xy1(0),xy2(0)])
 imax(1)=max([xy1(1),xy2(1)])
 imin(0)= 0 > imin(0) < (nx-1)
 imax(0)= 0 > imax(0) < (nx-1)
 imin(1)= 0 > imin(1) < (ny-1)
 imax(1)= 0 > imax(1) < (ny-1)
 if (abs(imax(1)-imin(1)) lt 2) or (abs(imax(0)-imin(0)) lt 2) then begin
  plabels='insufficient data within FOV' & message,plabels,/cont
  return
 endif
 pic=temporary(pic(imin(0):imax(0),imin(1):imax(1)))
 xarr=temporary(xarr(imin(0):imax(0),imin(1):imax(1)))
 yarr=temporary(yarr(imin(0):imax(0),imin(1):imax(1)))
endif

if count eq nx*ny then message,'fov > image size',/info else begin
 message,'fov < image size',/info
 min_x=min(xarr) & max_x=max(xarr)
 min_y=min(yarr) & max_y=max(yarr)
 xb = [min_x,max_x,max_x,min_x,min_x]
 yb = [min_y,min_y,max_y,max_y,min_y]
 xp=(min_x+max_x)/2. &  yp=(min_y+max_y)/2.
 sz=size(pic) & nx=sz(1) & ny=sz(2)
 print,'-- image size reduced to: ',nx,ny
endelse

;-- log image?

if n_elements(dmin) eq 0 then min_pic=min(pic) else min_pic=dmin 
if n_elements(dmax) eq 0 then max_pic=max(pic) else max_pic=dmax
min_pic=float(min_pic) & max_pic=float(max_pic)
pcheck=where( (pic lt min_pic) or (pic gt max_pic),pcount)
if pcount eq n_elements(pic) then begin
 message,'insufficient data points to plot',/cont
 return
endif else begin
 if pcount gt 0 then begin
  if not exist(missing) then missing=0
  pic(pcheck)=missing
 endif
endelse

if log and (not cont) then begin
 pic=alog10(temporary(pic) > .01)
 min_pic=alog10(min_pic > .01)
 max_pic=alog10(max_pic > .01)
endif

if min_pic eq max_pic then begin
 message,'MIN data = MAX data',/cont
 return
endif

;-- contour plot?

if cont then begin
 if n_elements(nlev) eq 0 then nlev=10
 if (n_elements(clev) eq 0) or over then $
  clev=min_pic+findgen(nlev)*(max_pic-min_pic)/(nlev-1.)
 if n_elements(clabel) eq 0 then clabel=intarr(nlev) 
 if n_elements(cthick) eq 0 then cthick=replicate(1.,nlev)
 if n_elements(cstyle) eq 0 then cstyle=replicate(0,nlev)
 colors=replicate(fix(lcolor),nlev)  
 in_levels=0
 if n_elements(levels) gt 1 then begin
  clev=levels
  in_levels=1
 endif
 if keyword_set(tail) then begin                ;-- tailor contours?
  if not in_levels then begin
   print,'* default is equispaced contours between:'
   print,' MIN = ',min_pic,' and MAX = ',max_pic
   print,'* enter explicit values for contour levels'
   clev='' & read,'----> ',clev
   if clev eq '' then begin
    nlev='' & read,'* enter number of equispaced contour levels for image [def=10]: ',nlev
    if nlev eq '' then nlev=10 else nlev=fix(nlev)
    clev=min_pic+findgen(nlev)*(max_pic-min_pic)/(nlev-1.)
   endif else begin
    clev=float(str2arr(clev)) & cs=sort(clev) & clev=clev(cs)
   endelse
  endif
  if n_elements(clev) lt 2 then clev=[clev,max_pic]
  nlev=n_elements(clev) & clabel=intarr(nlev) 
  ans='' & read,'* label contours [def=n]? ',ans
  if ans eq '' then ans='n' & ans=strupcase(strmid(ans,0,1))
  if ans eq 'Y' then begin
   clabel=clev & odd=where(findgen(nlev) mod 2) & clabel(odd)=0.
  endif
  cstyle='' & read,'* enter contour linestyle [def=0, solid]: ',cstyle
  cstyle=replicate(fix(cstyle),nlev)
  cthick='' & read,'* enter contour thickness [def=1]: ',cthick
  if cthick eq '' then cthick=1. & cthick=replicate(float(cthick),nlev)
 endif
endif

;-- set viewport

if (not over) and (have_time) and (have_id) then begin
 err=''
 date_obs=anytim2utc(img.time,/vms,err=err,time_only=time_only,date_only=date_only)
 if err ne '' then date_obs=string(img.time)
 mtitle=img.id+' '+date_obs
endif else mtitle=''

;-- set window

if (!d.name eq 'X') and exist(last_window) then begin
 if last_window gt -1 then begin
  if (not keyword_set(draw)) then wshow,last_window,1
  wset,last_window
 endif
endif

;-- plot fov

xstyle=1 & ystyle=1 & xtitle='X (arcsecs)' & ytitle='Y (arcsecs)'
if keyword_set(noaxes) then begin
 ystyle=5
 xstyle=5
endif

;-- contour?

if (not over) then begin
 plot,[xmin,xmax,xmax,xmin,xmin],[ymin,ymin,ymax,ymax,ymin],/data,/noclip, $
  xstyle=xstyle,ystyle=ystyle,noeras=over,nodata=over,xrange=[xmin,xmax],$
  yrange=[ymin,ymax],color=lcolor,xtitle=xtitle,ytitle=ytitle,title=mtitle,$
  _extra=extra,charsize=charsize,font=font
endif

if cont then begin   
 if gang(0) ne 0 then !p.multi(0)=!p.multi(0)+1
 cs=uniq([clev],sort([clev]))
 if over and keyword_set(rotate) then begin
  if exist(tref) then time=tref else time=last_img.time
  pimg=img
  pimg.xp=pimg.xp+xoff
  pimg.yp=pimg.yp+yoff
  rmap=drot_map(pimg,time=time,soho=soho)
  xarr=rmap.xp
  yarr=rmap.yp
 endif
 contour,pic,xarr,yarr,/data,xstyle=xstyle,ystyle=ystyle,c_linestyle=cstyle(cs),$
  levels=clev(cs),c_labels=clabel(cs),c_thick=cthick(cs),c_colors=colors(cs),$
  max_value=max_pic,thick=thick,min_value=min_pic,charsize=charsize,font=font,$
  spline=smo,/noeras,noclip=0,/follow,xrange=[xmin,xmax],yrange=[ymin,ymax]
  if gang(0) ne 0 then !p.multi(0)=!p.multi(0)-1

;-- image?

endif else begin
 xb_dev=!d.x_vsize*(!x.s(0)+!x.s(1)*xb)
 yb_dev=!d.y_vsize*(!y.s(0)+!y.s(1)*yb)
 sx=abs(max(xb_dev)-min(xb_dev)) > 1.
 sy=abs(max(yb_dev)-min(yb_dev)) > 1.

;-- rebin image for workstation (!d.name = 'X')
;-- or plot in Postscript using scalable pixels (!d.name = 'PS')

 if not exist(top) then top=!d.n_colors-1 else top=byte(top) < (!d.n_colors-1)
 if not exist(bottom) then bottom=0b else bottom=byte(bottom) > 0
 bscale,pic,top=top,max=max_pic,min=min_pic,$
  noscale=noscale,missing=missing,$
  velocity=velocity,combined=combined,lower=lower
 pic=pic+bottom
 if (!d.name eq 'X') then pic=congrid(pic,sx,sy,interp=smo)
 tv,pic,xb_dev(0),yb_dev(0),xsize=sx,ysize=sy
 if !p.multi(0) eq 0 then begin
  plot,[xmin,xmax,xmax,xmin,xmin],[ymin,ymin,ymax,ymax,ymin],/data,/noclip, $
   xstyle=xstyle,ystyle=ystyle,/noeras,/nodata,xrange=[xmin,xmax],$
   yrange=[ymin,ymax],color=lcolor,_extra=extra,charsize=charsize,font=font
 endif
endelse

;-- overlay a solar latitude-longitude grid

slon='?' & slat='?'
if (grid gt 0) and (have_time) then begin
 pvec=pb0r(img.time,/arcsec,soho=soho)
 bangle=pvec(1) & radius=pvec(2)
 if !quiet eq 0 then begin
  print,space
  print,'---> UT TIME: ',img.time
  print,'---> SOLAR B ANGLE (deg): ',bangle
  print,'---> SOLAR RADIUS (arcsec): ',radius
 endif
 if (not over) then plot_helio,radius,bangle,roll,grid=grid,glabel=glabel,$
   /noeras,color=lcolor,font=font
endif

;-- plot border edges

if bord then begin
 edge=replicate(1.,nx,ny) 
 if ((nx gt 2) and (ny gt 2)) then edge(1:nx-2,1:ny-2) = 0
 if gang(0) ne 0 then !p.multi(0)=!p.multi(0)+1
 contour,edge,xarr,yarr,xstyle=5,ystyle=5,levels=[0,1],thick=thick,$
  /data,noclip=0,/noeras,/follow,spline=smo,c_linestyle=[0,0],$
  xrange=[xmin,xmax],yrange=[ymin,ymax],c_color=[lcolor,lcolor]
 if gang(0) ne 0 then !p.multi(0)=!p.multi(0)-1
endif

;-- print labels

if (not over) then begin
 plabels=[ 'MAX VALUE:  '+ string(max_pic,'(f6.1)'),$
           'SIZE (ARCMIN):  '+ string(xarc,'(f5.2)') +' X '+string(yarc,'(f5.2)'),$
           'DX STEP (ARCSEC):  '+ string(dx,'(f5.2)'),$
           'DY STEP (ARCSEC):  '+ string(dy,'(f5.2)')]
 if have_time then plabels=['TIME:  ' + img.time,plabels]
 if have_id then plabels=['IMAGE:  '+img.id,plabels]
endif

return & end


