;+
; Project     : STEREO
;
; Name        : map2stereo
;
; Purpose     : Project an image map to STEREO view
;
; Category    : imaging, maps
;
; Syntax      : IDL> smap=map2stereo(map)
;
; Inputs      : MAP = image map structure
;               TIME = UT time of STEREO image to project map to, 
;                      or a STEREO map structure
;
; Outputs     : SMAP = map projected to STEREO view
;
; Keywords    : /AHEAD for STEREO A, /BEHIND for STEREO B
;
; History     : Written 4 October 2007 - Zarro (ADNET)
;
; Contact     : dzarro@solar.stanford.edu
;-

function map2stereo,map,time,ahead=ahead,behind=behind,_extra=extra,err=err

forward_function get_stereo_lonlat,get_stereo_roll

err=''

if ~have_proc('get_stereo_lonlat') then begin
 err='STEREO orbital position routine - get_stereo_lonlat - not found'
 message,err,/cont
 return,''
endif

;-- validate inputs

if ~valid_map(map,err=err) or $
   (~valid_time(time,err=err) and ~valid_map(time,err=err)) then begin
 pr_syntax,'smap=map2stereo(map,time)'
 return,''
endif

;-- check if STEREO map entered

if valid_map(time) then begin
 proj_time=anytim2tai(time.time)
 b0=time.b0
 l0=time.l0
 rsun=time.rsun
 roll=time.roll_angle
 rcenter=time.roll_center
 spacecraft=''
 if stregex(time.id,'STEREO[-|_]A',/bool,/fold) then spacecraft='A'
 if stregex(time.id,'STEREO[-|_]B',/bool,/fold) then spacecraft='B'
 if is_blank(spacecraft) then begin
  err='Input map not a STEREO map'
  message,err,/cont
  return,''
 endif
endif else begin

 if ~have_proc('get_stereo_lonlat') then begin
  err='STEREO orbital position routine - get_stereo_lonlat - not found'
  message,err,/cont
  return,''
 endif

 stereo_launch=anytim2tai('26-oct-2006')
 proj_time=anytim2tai(time)
 if proj_time lt stereo_launch then begin
  err='STEREO orbital data unavailable for this input time'
  message,err,/cont
  return,''
 endif

;-- STEREO values of l0, b0, rsun, and roll for input time

 spacecraft='A'
 err=''
 if keyword_set(behind) then spacecraft='B' 
 pos=get_stereo_lonlat(time, spacecraft, system="HEEQ", /degrees,err=err)

 if is_string(err) then begin
  message,err,/cont
  return,''
 endif

 b0=pos[2]
 l0=pos[1]
 rsun=sol_rad(pos[0])

 roll=get_stereo_roll(time, spacecraft,err=err)
 if is_string(err) then begin
  message,err,/cont
  return,''
 endif
endelse

;-- do the reprojecting

mtime=get_map_time(map,/tai)
center=get_map_center(map)
interval=proj_time-mtime
smap=drot_map(map,interval,b0=b0,l0=l0,rsun=rsun,roll=-roll,$
              rcenter=center,_extra=extra,err=err,/sec)

if have_tag(smap,'SOHO') then smap.soho=0b

return,smap
end

