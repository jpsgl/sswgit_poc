;+
; Project     : SOHO
;
; Name        : HAVE_SOHO_ROLL
;
; Purpose     : Check INDEX structure for SOHO 180 degree roll
;
; Category    : imaging
;
; Syntax      : IDL> chk=have_soho_roll(index)
;
; Inputs      : INDEX = index 
;
; Outputs     : 1/0 if rolled 180 or not
;
; Keywords    : None
;
; History     : Written 20 July 2009, Zarro (ADNET)
;
; Contact     : dzarro@solar.stanford.edu
;-

function have_soho_roll,index

if ~is_struct(index) then return,0b

if have_tag(index,'sc_roll',k,/exact) then $
 if nint((abs(index.(k)) mod 360)) eq 180 then return,1b
if have_tag(index,'p_angle',k,/exact) then $
 if nint((abs(index.(k)) mod 360)) eq 180 then return,1b
if have_tag(index,'crot',k,/exact) then $
 if nint((abs(index.(k)) mod 360)) eq 180 then return,1b
if have_tag(index,'crota1',k,/exact) then $
 if nint((abs(index.(k)) mod 360)) eq 180 then return,1b
if have_tag(index,'crota2',k,/exact) then $
 if nint((abs(index.(k)) mod 360)) eq 180 then return,1b
if have_tag(index,'solar_p',k,/exact) then $
 if nint((abs(index.(k)) mod 360)) eq 180 then return,1b

return,0b

end
