;+
; Project     : HESSI
;
; Name        : MDI__DEFINE
;
; Purpose     : Define an MDI data object
;
; Category    : Ancillary GBO Synoptic Objects
;
; Syntax      : IDL> c=obj_new('mdi')
;
; History     : Written 17 Feb 2001, D. Zarro, EIT/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-
;----------------------------------------------------------------------------
;-- FITS reader

pro mdi::read,file,_ref_extra=extra

if is_blank(file) then begin
 pr_syntax,'obj->read,file'
 return
endif

self->get,file,local_file=ofile,_extra=extra
if is_blank(ofile) then return
nfiles=n_elements(ofile)
for i=0,nfiles-1 do begin
 if ~self->is_valid(ofile[i],_extra=extra) then continue
 rd_mdi,ofile[i],index,data
 index2map,index,data,map,_extra=extra,correct_roll=correct_roll
 if correct_roll then index=add_fits_hist(index,'SOHO Roll Correction Applied')
 if have_tag(index,'obs_mode') then begin
  tag=index.obs_mode
  case 1 of 
   stregex(tag,'intensi',/bool,/fold): id='Intensitygram'
   stregex(tag,'doppler',/bool,/fold): id='Dopplergram'
   stregex(tag,'magnet',/bool,/fold): id='Magnetogram'
  else: id=''
  endcase
 endif
 map.id=strtrim('SOHO MDI '+id,2)
 index=rep_tag_value(index,file_break(ofile[i]),'filename')
 self->set,i,map=map,index=index,grid=30,/limb
 self->colors,i
endfor

return & end

;-------------------------------------------------------------------
;-- set MDI colors

pro mdi::colors,k
dsave=!d.name
set_plot,'z'
tvlct,r0,g0,b0,/get
loadct,0
tvlct,red,green,blue,/get
tvlct,r0,g0,b0
set_plot,dsave
self->set,k,red=red,green=green,blue=blue,/has_colors
return & end

;--------------------------------------------------------------------------
;-- check if already flatfielded

function mdi::flat_fielded

return,self->has_history('Flatfield applied')

end

;--------------------------------------------------------------------------
;-- check if already darklimb corrected

function mdi::limb_corrected

return,self->has_history('Dark limb corrected')

end

;--------------------------------------------------------------------------
;-- apply limb correction

pro mdi::limb_correct,err=err
err=''

if ~self->has_data() then begin
 err='No image read'
 message,err,/cont
 return
endif

;-- flatfield first and correct for possible roll

if ~self->flat_fielded() then self->flat_field,err=err

if is_string(err) then return

if self->limb_corrected() then return

time=self->get(/time)
xc=self->get(/xc)
yc=self->get(/yc)
nx=self->get(/nx)
ny=self->get(/ny)
dx=self->get(/dx)
dy=self->get(/dy)
soho=self->get(/soho)
pbr=pb0r(time,soho=soho,/arcsec)
radius=2.*pbr(2)/(dx+dy)

crpix1=comp_fits_crpix(xc,dx,nx)
crpix2=comp_fits_crpix(yc,dy,ny)

map=self->get(/map,/no_copy)
darklimb_correct,map.data,temp_img,limbxyr=[crpix1,crpix2,radius],lambda=6767

;bdata=cscale(temporary(temp_img),/no_copy)
map.data=temporary(temp_img)

self->set,map=map,/no_copy

;-- update history

self->update_history,'Dark limb corrected'

return & end

;-----------------------------------------------------------------------------
;-- apply flatfield

pro mdi::flat_field,err=err

common mdi_flatfield,flat_map

err=''

if ~self->has_data() then begin
 err='No image read'
 message,err,/cont
 return
endif

;-- check if already flatfielded

if self->flat_fielded() then return
 
nx=self->get(/nx)
ny=self->get(/ny)
dx=self->get(/dx)
dy=self->get(/dy)

if (nx ne 1024) or (ny ne 1024) or ((dx lt 1.) and (dy le 1.)) then begin
 err='Image is not full-disk'
 message,err,/cont
 return
endif

;-- read flat field file

if ~valid_map(flat_map) then begin
 flatfield_file ='$SSWDB/soho/mdi/flatfield/mdi_flat_jan2001.fits'
 loc=loc_file(flatfield_file,count=count)
 if count eq 0 then begin
  err='Unable to locate latest MDI flatfield file - mdi_flat_jan2001.fits'
  message,err,/cont
  return
 endif
 flat=obj_new('fits')
 flat->read,flatfield_file
 flat_map=flat->get(/map,/no_copy)
 obj_destroy,flat
endif

;-- normalize MDI image

map=self->get(/map,/no_copy)

map.data = temporary(flat_map.data)*temporary(map.data)

self->set,map=map,/no_copy

;-- update history

self->update_history,'Flatfield applied'

return & end

;--------------------------------------------------------------------------
;-- SOI search method

function mdi::search,tstart,tend,_ref_extra=extra,$
  times=times,sizes=sizes,count=count,verbose=verbose

dstart=get_def_times(tstart,tend,dend=dend,_extra=extra,/vms)

if valid_time(tend) then $
 files=mdi_time2file(dstart,dend, /stanford,/after) else $
  files=mdi_time2file(dstart, /stanford,/after) 

count=n_elements(files)
if count eq 1 and is_blank(files) then begin
 count=0 & times=-1 & sizes=''
 return,''
endif

;-- parse file times by remotely reading each file header (can be slow)

sizes=strarr(count)
if arg_present(times) then begin
 if keyword_set(verbose) then message,'extracting MDI times...',/cont
 times=(-1.d)+dblarr(count)
 for i=0,count-1 do begin
  value=sock_head(files[i],'date_obs')
  if is_string(value) then begin
   err=''
   stime=anytim2tai(value,err=err)
   if err eq '' then times[i]=stime
  endif 
 endfor
endif

if count eq 1 then begin
 files=files[0] & sizes=sizes[0]
 if exist(times) then times=times[0]
endif

return,files

end

;---------------------------------------------------------------------
;-- check if file is valid

function mdi::is_valid,file,err=err,verbose=verbose

verbose=keyword_set(verbose)
mrd_head,file,header,err=err
if is_string(err) then return,0b

;-- check if valid MDI file

chk=where(stregex(header,'(INST|TEL|DET|ORIG).+MDI',/bool,/fold),count)
valid=count ne 0
if ~valid then begin
 message,'Invalid MDI file - '+file,/cont
 return,0b
endif

return,1b

end
;------------------------------------------------------------------------------
;-- MDI data structure

pro mdi__define                 

self={mdi,inherits fits}

return & end



