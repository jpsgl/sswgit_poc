;+
; Project     : STEREO
;
; Name        : SECCHI__DEFINE
;
; Purpose     : Define a SECCHI data/map object
;
; Category    : Objects
;
; Syntax      : IDL> a=obj_new('secchi')
;
; Examples    : IDL> a->read,'20070501_000400_n4euA.fts' ;-- read FITS file
;               IDL> a->plot                             ;-- plot image
;               IDL> map=a->getmap()                     ;-- access map
;               IDL> data=a->getdata()                   ;-- access data
;                       
;               Searching via VSO:                                     
;               IDL> files=a->search('1-may-07','02:00 1-may-07')
;               IDL> print,files[0]
;               http://stereo-ssc.nascom.nasa.gov/data/ins_data/secchi/L0/a/img/euvi/20070501/20070501_000400_n4euA.fts
;               IDL> a->read,files[0],/verbose
;
; History     : Written 13 May 2007, D. Zarro (ADNET)
;               Modified 31-Oct-2007 William Thompson (ADNET)
;                - modified for COR1/COR2
;               Modified 26 March 2009 - Zarro (ADNET)
;                - renamed index2map method to mk_map
;
; Contact     : dzarro@solar.stanford.edu
;-

;-----------------------------------------------------------------------------
;-- check for SECCHI branch in !path

function secchi::have_path,err=err,verbose=verbose

common have_path,registered

err=''
if ~have_proc('sccreadfits') then begin
 epath=local_name('$SSW/stereo/secchi/idl')
 if is_dir(epath) then ssw_path,/secchi,/quiet
 if ~have_proc('sccreadfits') then begin
  err='STEREO/SECCHI branch of SSW not installed.'
  if keyword_set(verbose) then message,err,/cont
  return,0b
 endif
endif

;-- register DLM to read SPICE files

if ~exist(registered) then begin
 if have_proc('register_stereo_spice_dlm') then begin
  register_stereo_spice_dlm
  registered=1b
 endif
endif

return,1b

end

;---------------------------------------------------------------------

function secchi::have_cal,err=err,verbose=verbose

;-- ensure that calibration directories are properly defined

;SSW_SECCHI=local_name('$SSW/stereo/secchi')
;mklog,'SSW_SECCHI',SSW_SECCHI
;SECCHI_CAL=local_name('$SSW_SECCHI/calibration')
;mklog,'SECCHI_CAL',SECCHI_CAL

err=''
if ~is_dir('$SSW_SECCHI/calibration') then begin
 err='SECCHI calibration directory not found.'
 if keyword_set(verbose) then message,err,/cont
 return,0b
endif

return,1b & end

;--------------------------------------------------------------------------
;-- FITS reader

pro secchi::read,file,data,_ref_extra=extra,err=err,$
            no_prep=no_prep

forward_function discri_pobj,sccreadfits

if is_blank(file) then begin
 pr_syntax,'obj->read,file'
 return
endif

self->empty
do_prep=~keyword_set(no_prep)
have_cal=self->have_cal(_extra=extra,err=err1)
have_path=self->have_path(_extra=extra,err=err2)
err=''

;-- download files if not present

self->get,file,local_file=cfile,err=err,_extra=extra
if is_blank(cfile) then return
nfiles=n_elements(cfile) 

for i=0,nfiles-1 do begin
 valid=self->is_valid(cfile[i],level=level,_extra=extra)
 if ~valid then continue

 case 1 of
 (level eq 0) and do_prep and have_cal and have_path: begin
  dfile=find_compressed(cfile[i],count=count)
  if count eq 1 then secchi_prep,dfile,index,data,_extra=extra
 end
 
 (level eq 0) and ~do_prep and have_path: begin
  dfile=find_compressed(cfile[i],count=count)
  if count eq 1 then data=sccreadfits(dfile,index,_extra=extra)
 end

 else: begin
  if do_prep then begin
   if ~have_cal then message,err1,/cont
   if ~have_path then message,err2,/cont
  endif
  self->mreadfits,cfile[i],data,_extra=extra,index=index
 end
 endcase

 ;-- insert data into maps
 
 index=rep_tag_value(index,cfile[i],'filename')
 self->mk_map,i,index,data,err=err
 if is_string(err) then message,err,/cont

endfor
count=self->get(/count)
if count eq 0 then message,'No maps created.',/cont

return & end

;---------------------------------------------------------------------
;-- store INDEX and DATA into MAP objects

pro secchi::mk_map,i,index,data,err=err

err=''
if ~is_number(i) then i=0

;-- check inputs

if ~is_struct(index) or (n_elements(index) ne 1) then begin
 err='Input index is not a valid structure.'
 return
endif

ndim=size(data,/n_dim)
if (ndim ne 2) then begin
 err='Input image is not a 2-D array.'
 return
endif

;-- add STEREO-specific properties

id=index.OBSRVTRY+' '+index.INSTRUME+' '+index.DETECTOR+' '+trim(index.WAVELNTH)
nx=index.naxis1 & ny=index.naxis2
dx=index.cdelt1 & dy=index.cdelt2
dur=index.exptime
time=anytim2utc(index.date_obs,/vms)
comp_pc_cen,index,xc,yc
map=make_map(data,time=time,dx=dx,dy=dy,id=id,/no_copy,$
      roll_angle=index.crota,roll_center=[xc,yc],xc=xc,yc=yc,dur=dur,$
       l0=index.hgln_obs,b0=index.hglt_obs,rsun=index.rsun,$
       err=err)
if is_string(err) then return
self->set,i,index=index,map=map,/no_copy
if index.detector eq 'EUVI' then self->set,i,/log_scale,grid=30,/limb
self->colors,i

return & end

;-----------------------------------------------------------------------
;-- VSO search function

function secchi::search,tstart,tend,_ref_extra=extra,$
                 euvi=euvi,cor1=cor1,cor2=cor2,det=det

case 1 of
 keyword_set(euvi): det='euvi'
 keyword_set(cor1): det='cor1'
 keyword_set(cor2): det='cor2'
 else: do_nothing=''
endcase

return,vso_files(tstart,tend,inst='secchi',_extra=extra,det=det,window=3600.)

end

;------------------------------------------------------------------------------
;-- save SECCHI color table

pro secchi::colors,k

if ~have_proc('secchi_colors') then return
index=self->get(k,/index)
dsave=!d.name
set_plot,'z'
tvlct,r0,g0,b0,/get
secchi_colors,index.detector,index.wavelnth,red,green,blue
tvlct,r0,g0,b0
set_plot,dsave
self->set,k,red=red,green=green,blue=blue,/has_colors

return & end

;------------------------------------------------------------------------------
;-- check if valid SECCHI file

function secchi::is_valid,file,err=err,detector=detector,verbose=verbose,$
                  level=level,_extra=extra,euvi=euvi,cor1=cor1,cor2=cor2

level=0
detector='' & instrument=''
mrd_head,file,header,err=err
if is_string(err) then return,0b

s=fitshead2struct(header)
if have_tag(s,'dete',/start,index) then detector=strup(s.(index))
if have_tag(s,'inst',/start,index) then instrument=strup(s.(index))

if have_tag(s,'history') then begin
 chk=where(stregex(s.history,'Applied Flat Field',/bool),count)
 level=count gt 0
endif

verbose=keyword_set(verbose)

if keyword_set(euvi) and detector ne 'EUVI' then begin
 if verbose then message,'Input file is not EUVI',/cont & return,0b
endif

if keyword_set(cor1) and detector ne 'COR1' then begin
 if verbose then message,'Input file is not COR1',/cont & return,0b
endif

if keyword_set(cor2) and detector ne 'COR2' then begin
 if verbose then message,'Input file not COR2',/cont & return,0b
endif

return,instrument eq 'SECCHI'
end

;------------------------------------------------------------------------
;-- SECCHI data structure

pro secchi__define,void                 

void={secchi, inherits fits}

return & end
