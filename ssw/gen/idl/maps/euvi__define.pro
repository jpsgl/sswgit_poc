;+
; Project     : STEREO
;
; Name        : EUVI__DEFINE
;
; Purpose     : stub that inherits from SECCHI class
;
; Category    : Objects
;
; History     : Written 7 April 2009, D. Zarro (ADNET)
;
; Contact     : dzarro@solar.stanford.edu
;-

function euvi::search,tstart,tend,_ref_extra=extra

return,self->secchi::search(tstart,tend,/euvi,_extra=extra)

end

;----------------------------------------------------
pro euvi__define,void                 

void={euvi, inherits secchi}

return & end
