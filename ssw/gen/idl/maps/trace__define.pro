;+
; Project     : HESSI
;
; Name        : TRACE__DEFINE
;
; Purpose     : Define a TRACE data object
;
; Category    : Ancillary GBO Synoptic Objects
;
; Syntax      : IDL> c=obj_new('trace')
;
; History     : Written 30 Dec 2007, D. Zarro (ADNET)
;
; Contact     : dzarro@solar.stanford.edu
;-

;------------------------------------------------------------------------

function trace::get,dummy,selection=selection,_ref_extra=extra,multiple=multiple

if keyword_set(selection) then return,self.selection
if keyword_set(multiple) then return,self.multiple

return,self->fits::get(dummy,_extra=extra)

end

;-------------------------------------------------------------------------
;-- set TRACE calibration environment  

pro trace::set_calibration,err=err,quiet=quiet

mklog,'SSW_TRACE','$SSW/trace',/local
mklog,'TRACE_DBASE','$SSW_TRACE/dbase',/local
mklog,'TRACE_CAL','$TRACE_DBASE/cal',/local
mklog,'TRACE_RESPONSE','$SSW_TRACE/response',/local
mklog,'TR_CAL_INFO','$TRACE_CAL/info',/local
mklog,'TRACE_EGSE_DBASE','$TRACE_DBASE/egse/dbs',/local
mklog,'TOPS_PATH','$SSW_TRACE/obs',/local
mklog,'TOPS_IDS_PATH','$SSW_TRACE/obs/dbase',/local
mklog,'HTAB','$SSW_TRACE/dbase/tables',/local
mklog,'QTAB','$SSW_TRACE/dbase/tables',/local

if ~is_dir('$tdb') then begin
 err='Warning: TRACE calibration directory not installed.'
 if ~keyword_set(quiet) then message,err,/cont
endif

return
end

;----------------------------------------------------------------------
function trace::have_cal

if is_dir('$tdb') then return,1b
message,'Warning, TRACE calibration directory not installed. Level 0 images will not be prepped properly.',/cont
return,1b
end

;-------------------------------------------------------------------------
;-- check for trace_decode_idl shareable object

function trace::have_decoder,err=err,verbose=verbose,_extra=extra

verbose=keyword_set(verbose)
wdir=!version.OS + '_' + !version.ARCH 
decomp='trace_decode_idl.so'
if os_family() eq 'Windows' then decomp='trace_decode_idl.dll'

share=local_name('$SSW_TRACE/binaries/'+wdir+'/'+decomp)
chk=file_search(share,count=count)
if count eq 0 then begin
 share=local_name('$CALL_EXTERNAL_USER/'+decomp)
 chk=file_search(share,count=count)
endif

;-- download a copy to temporary directory

if count eq 0 then begin
 warn='IDL shareable object "trace_decode_idl" not found on this system.'
 if verbose then message,warn+' Downloading from server...',/cont
 sdir=get_temp_dir()
 udir=concat_dir(sdir,wdir)
 mk_dir,udir
 mklog,'CALL_EXTERNAL_USER',udir
 sloc=ssw_server(/full)
 sfile=sloc+'/solarsoft/trace/binaries/'+wdir+'/'+decomp
 sock_copy,sfile,out_dir=udir,_extra=extra
 chk=file_search(share,count=count)
endif

return,count ne 0

end

;--------------------------------------------------------------------------
;-- VSO search wrapper

function trace::search,tstart,tend,_ref_extra=extra

return,vso_files(tstart,tend,inst='trace',_extra=extra,window=3600.)

end

;---------------------------------------------------------------------------
;-- check for TRACE branch in !path

function trace::have_path,err=err,quiet=quiet

err=''
if ~have_proc('read_trace') then begin
 epath=local_name('$SSW/trace/idl')
 if is_dir(epath) then ssw_path,/trace,/quiet
 if ~have_proc('read_trace') then begin
  err='TRACE branch of SSW not installed.'
  if ~keyword_set(quiet) then message,err,/cont
  return,0b
 endif
endif

return,1b

end

;--------------------------------------------------------------------------
;-- FITS reader

pro trace::read,file,_ref_extra=extra,no_prep=no_prep,image_no=image_no,$
           nodata=nodata,index=index

;-- download if URL

self->empty
if is_blank(file) then begin
 pr_syntax,'object_name->read,filename'
 return
endif
file=strtrim(file,2)
self->get,file,local_file=ofile,_extra=extra

do_prep=~keyword_set(no_prep)
have_path=1b
have_cal=1b
have_decoder=1b
if do_prep then begin
 have_path=self->have_path()
 have_cal=self->have_cal()
 have_decoder=self->have_decoder(_extra=extra)
endif

;-- read files

nfiles=n_elements(ofile)
j=0
nsub=1
sel_img=exist(image_no)
if sel_img then sel_img=is_number(image_no[0])
for i=0,nfiles-1 do begin
 valid=self->is_valid(ofile[i],err=err,level=level,_extra=extra)
 if ~valid then continue
 prepping=(level eq 0) and do_prep and have_path and have_cal and have_decoder
 if prepping then begin
  self.selection=1b
  self.multiple=1b
  dfile=find_compressed(ofile[i])
  records=self->read_records(dfile,/no_check)
  if is_blank(records) then continue
  nsub=n_elements(records)
  if ~sel_img then begin
   image_no=xsel_list_multi(records,/index,cancel=cancel,$
    label=file_break(dfile)+' - Select image numbers from list below:')
   if cancel then continue
  endif
 endif else image_no=0
 nimg=n_elements(image_no)
 for k=0,nimg-1 do begin
  if prepping then begin
   if (image_no[k] gt -1) and (image_no[k] lt nsub) then begin
    message,'Prepping image '+trim(image_no[k]),/cont
    trace_prep,dfile,image_no[k],index,data,/norm,/wave2point,/float,_extra=extra
   endif
  endif else begin
   if (level eq 1) then self->fits::mreadfits,ofile[i],data,index=index,_extra=extra
  endelse
  sz=size(data)
  if (sz[0] lt 2) or ~is_struct(index) then continue
  index=rep_tag_value(index,file_break(ofile[i]),'filename')
  index2map,index,data,map,/no_copy,_extra=extra
  map.id='TRACE '+trim(index.wave_len)
  self->set,j,map=map,index=index,/no_copy,/log_scale,grid=30,/limb
  j=j+1
 endfor
endfor
if self->get(/count) eq 0 then message,'No map images created.',/cont

return & end

;-----------------------------------------------------------------------------
;--- return records in a TRACE level 0 file

function trace::read_records,file,count=count,no_check=no_check

count=0
records=''
check=~keyword_set(no_check)
if check then begin
 valid=self->is_valid(file,level=level)
 if level ne 0 then message,file+' is not a level 0 file',/cont
 if ~valid or (level ne 0) then return,''
endif
read_trace,file,-1,index,/nodata
if ~is_struct(index) then return,''
count=n_elements(index)
records=trim(sindgen(count))+') TIME: '+trim(anytim2utc(index.date_obs,/vms))+ $
        ' WAVELENGTH: '+strpad(trim(index.wave_len),4,/after)+ $
        ' NAXIS1: '+strpad(trim(index.naxis1),4,/after)+ $
        ' NAXIS2: '+strpad(trim(index.naxis2),4,/after)
return,records
end

;------------------------------------------------------------------------------
;-- check if valid TRACE file

function trace::is_valid,file,err=err,level=level,verbose=verbose

valid=0b & level=0
verbose=keyword_set(verbose)
mrd_head,file,header,err=err
if is_string(err) then return,valid

chk1=where(stregex(header,'MPROGNAM.+TR_REFORMAT',/bool,/fold),count1)
chk2=where(stregex(header,'(INST|TEL|DET|ORIG).+TRAC',/bool,/fold),count2)

valid=(count1 ne 0) or (count2 ne 0)

if ~valid then begin
 message,'Invalid TRACE file - '+file,/cont
 return,valid
endif

if count1 ne 0 then level=0 else if count2 ne 0 then level=1

chk=where(stregex(header,'trace_prep',/bool,/fold),count)
if count gt 0 then level=1
if verbose and (level eq 1) then message,'TRACE image is already prepped.',/cont

return,valid
end

;------------------------------------------------------------------------
function trace::select,index,filename=filename

if ~is_struct(index) then return,-1
count=n_elements(index)
if count eq 1 then return,0
if is_string(filename) then sfile=filename+' - ' else sfile=''
output=trim(sindgen(count))+') TIME: '+trim(anytim2utc(index.date_obs,/vms))+ $
        ' WAVELENGTH: '+strpad(trim(index.wave_len),4,/after)+ $
        ' NAXIS1: '+strpad(trim(index.naxis1),4,/after)+ $
        ' NAXIS2: '+strpad(trim(index.naxis2),4,/after)


list=xsel_list_multi(output,/index,cancel=cancel,$
label=sfile+'Select image numbers from list below:')
if cancel then begin
 err='Reading cancelled.'
 return,-1
endif

return,list & end

;------------------------------------------------------------------------------
;-- TRACE structure definition

pro trace__define,void                 

void={trace, selection:0b, multiple:0b,inherits fits}

return & end
