function mrd_fstruct2, names, values, nrow, $
    structyp=structyp, tempdir=tempdir, silent=silent
;
; Create a structure with column names given in names, a string 
; giving the value of the columns in values and optionally the
; structure name in structyp.  This function differs from mrd_struct
; in that it creates a temporary procedure.  It is used when the
; string length for the execute function exceeds 131 characters.
;
; 
; Inputs:
;       Names: a string array giving the names of the structure
;             columns.
;       Values: a string array giving the prototype value for 
;               the corresponding column.
;       Nrec: The number of instances of the structure
;             to be returned.
;       Structyp=  An optional name if the structure is to be a named
;                  structure.
; 	Tempdir=   Optional directory where temporary files are to be
;                  created.
;       /Silent    If set, the !quiet system variable is set to
;                  suppress compilation messages.
;
;  Uses the common area MRD_COMMON to track usage for versions < 4.0
;  The common block is not used (though it is defined) for version >= 4.0
;
;  Modified September 7, 1995 to handle more than structures
;  with more than 127 elements (the IDL limit).  This uses
;  structures of structures and may fail on earlier versions
;  of IDL.		TAM.
;  Modified October 16, 1995 to add SILENT keyword.
;  Modified April 25, 1996 by TAM to use RESOLVE_ROUTINE function
;    when !version.release > 4.0
;  Use /usr/bin/rm rather than rm to delete temporary file, W. Landsman 6/1997
;  
;  2-April-1998 S.L.Freeland - use system/shell independent file delete
;  use some SSW stuff

common mrd_common, usage

proname=make_str('dummy',/noexe)    ; generate a unique name
isvms=os_family() eq 'vms'

if not keyword_set(tempdir) then tempdir =get_logenv('HOME')

filename=concat_dir(tempdir,proname+'.pro')

openw, lun, filename, /get_lun
printf,lun, 'function '+proname
printf, lun, 'return, $'

; If we are going to return an array of structures, then we use
; the replicate function.
if nrow gt 1 then begin
	printf, lun, "replicate({ $"
endif else begin
	printf, lun, "{$"
endelse

; Put in the structure name if the user has specified one.
if keyword_set(structyp) then begin
	printf, lun,  structyp + ",$"
endif

; Now for each element put in a name/value pair.
nel = n_elements(names)

comma = ' '
exten = ''

; Do the output for the structure.  Note that if the structure contains more than
; 127 elements, then the first 64 elements will be processed normally, and the
; remaining elements will be as substructures of up to 64 elements in the last
; 63 elements of the primary structure.  The structure elements will have the
; name ss## where ## ranges from 1 to 63.  This allows a total of 64^2 or 4096
; elements to be defined in a structure.  Note that IDL may have problems
; before that.


substruct = nel gt 127

for i=0, nel-1 do begin

	if (i mod 64) eq 0 and i ne 0 and substruct then begin
		if i ne 64 then begin
		    ; Close the previous substructure.
		    printf, lun, "} $"
		endif
		; Create substructure to contain next 64 columns.
		exten = "ss"+strcompress(string(i/64),/remo)
		printf, lun, comma+exten+':{ $'
		comma = ' '  ; Don't put a comma before this element
        endif
	
	printf, lun, comma+names(i) + ':'+ values(i) + '$'
	comma = ','
endfor

; Close last substructure.
if substruct then begin
    printf, lun, "} $"
endif
	
if nrow gt 1 then begin
	printf, lun, "}$" 
	printf, lun,  "," + strtrim(long(nrow),2)+ ")"
endif else begin
	printf, lun, "}"
endelse

printf, lun, "end"

free_lun, lun

a = 0

; SLF - assure tempath in !path 

temppath=!path
delim=([':',',',';'])(where(os_family() eq ['vms','Windows'])+1)
!path=tempdir+delim+!path
tquiet=!quiet
!quiet=1

a = call_function(proname)    ; compile & execute the function

; restore global
!quiet=tquiet
!path=temppath

dname=filename+(['',';'])(isvms)
file_delete,dname

return, a
end


function mrd_struct2, names, values, nrow, $
    structyp=structyp, tempdir=tempdir, silent=silent
;+
; NAME:
;       MRD_STRUCT
; PURPOSE:
;       Return a structure as defined in the names and values data.
; CALLING SEQUENCE:
;       struct = MRD_STRUCT(NAMES, VALUES, NROW,                $
;                   STRUCTYP=structyp,                          $
;                   TEMPDIR=tempdir)
; INPUT PARAMETERS:
;       NAMES   = A string array of names of structure fields.
;       VALUES  = A string array giving the values of the structure
;                 fields.  See examples below.
;       NROW    = The number of elements in the structure array.
;       
; RETURNS:
;       A structure as described in the parameters or 0 if an error
;       is detected.
;
; OPTIONAL KEYWORD PARAMETERS:
;       STRUCTYP = The structure type.  Since IDL does not allow the
;                  redefinition of a named structure it is an error
;                  to call MRD_STRUCT with different parameters but
;                  the same STRUCTYP in the same session.  If this
;                  keyword is not set an anonymous structure is created.
;       TEMPDIR  = If the structure is more than modestly complex a
;                  temporary file is created.  This file will be
;                  created in the current directory unless the TEMPDIR
;                  keyword is specified.  Note that the temporary directory
;                  should also be in the IDL search path.
; COMMON BLOCKS:
;       MRD_COMMON
;                  Used to store a counter to create unique procedure
;                  names.
; SIDE EFFECTS:                                                            
;	May create a temporary file if the structure definition is too long 
;	for the EXECUTE function.   In V4.0 and later the temporary file is
;	always named 'mrd_structtemp.pro', while prior to V4.0 the temporary
;       file is of the form MRDT_# where # increments with each call that has 
;	a sufficiently complex structure (and is stored as a counter in
;	MRD_COMMON.
; RESTRICTIONS:
;       This procedure uses EXECUTE if the structure definition can
;       be fit into 131 characters.  Otherwise a temporary procedure
;       is used.  The 131 character limit is given in the IDL documentation
;       but in practice the limit seems to be higher in many implementations.
;       Users may wish to change this limit at their installation if
;       appropriate.
; PROCEDURE:
;       A structure definition is created using the parameter values.
;       If it is too long, MRD_FSTRUCT is called to write, compile and
;       execute a function which will define the structure.
; EXAMPLES:
;       str = mrd_struct(['fld1', 'fld2'], ['0','dblarr(10,10)'],3)
;       print, str(0).fld2(3,3)
;
;       str = mrd_struct(['a','b','c','d'],['1', '1.', '1.d0', "'1'"],1)
;               ; returns a structure with integer, float, double and string
;               ; fields.
; MODIFICATION HISTORY:
;       Created by T. McGlynn October, 1994.
;       Modified by T. McGlynn September, 1995.
;          Added capability to create substructures so that structure
;          may contain up to 4096 distinct elements.  [This can be
;          increased by futher iteration of the process used if needed.]
;       2-April-1998 - S.L.Freeland use some SSW stuff
;                      force TEMPDIR=HOME and put in !path
;                      (avoid crash if CD not write allowed by user) 
;-



; Create an instance of A, since an execute function cannot
; create an instance of an undefined variable.

a = 0

; Check that the number of names is the same as the number of values.
nel = n_elements(names)
if nel ne n_elements(values) then return, 0

; Start formatting the string.
strng = "a="

; If we are going to return an array of structures, then we use
; the replicate function.
if nrow gt 1 then begin
	strng = strng + "replicate({"
endif else begin
	strng = strng + "{"
endelse

; Put in the structure name if the user has specified one.
if keyword_set(structyp) then begin
	strng = strng + structyp + ","
endif

; Now for each element put in a name/value pair.
for i=0, nel-1 do begin
	if i ne 0 then strng = strng + ','
	strng = strng + names(i) + ':'
	strng = strng + values(i)
endfor

strng = strng + "}"

; Put in the second argument to the REPLICATE function if
; needed.
if nrow gt 1 then begin
	strng = strng + "," + strtrim(long(nrow),2)+ ")"
endif

; The IDL documentation implies that 131 is the maximum length
; for EXECUTE although many implementations seem to support longer
; strings.  We'll use this value though to be safe.

if strlen(strng) gt 131 then begin
	return, mrd_fstruct2(names, values, nrow, structyp=structyp, $
	   tempdir=tempdir, silent=silent)
endif else begin
	; Execute the string.  RES should be 1 if the execution was successful.
	res = execute(strng)

	if res eq 0 then return, 0 else return, a
endelse

end
