;+
; Project     : SOHO, YOHKOH
;
; Name        : GET_FITS_YCEN
;
; Purpose     : compute YCEN from FITS standard keywords
;
; Category    : imaging, FITS
;
; Explanation : 
;
; Syntax      : ycen=get_fits_ycen(naxis2,crpix2,cdelt2,crval2)
;
; Examples    :
;
; Inputs      : NAXIS2 = Y pixel dimensions of image
;               CRPIX2 = reference Y pixel coordinate
;               CDELT2 = Y pixel scaling
;
; Opt. Inputs : CRVAL2 = reference Y data coordinate [def=0]
;
; Outputs     : YCEN = center of FOV in data units
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written, 15 November 1998, D.M. Zarro (SMA)
;
; Contact     : dzarro@solar.stanford.edu
;-

function get_fits_ycen,naxis2,crpix2,cdelt2,crval2

on_error,2

present=exist(naxis2) and exist(crpix2) and exist(cdelt2)

if not present then begin
 pr_syntax,'get_fits_ycen,naxis2,crpix2,cdelt2 [,crval2]'
 return,0.
endif

if not exist(crval2) then crval2=0.

ycen=float(crval2)+float(cdelt2)*( (float(naxis2)+1.)/2. - float(crpix2))

return,ycen
end

