;+
; Project     : VSO
;
; Name        : GET_FITS_INST
;
; Purpose     : Get instrument name from FITS header
;
; Category    : imaging, FITS
;
; Syntax      : inst=get_fits_inst(file)
;
; Inputs      : FILE = FITS file name
;
; Outputs     : INST = instrument acronym
;
; Keywords    : PREPPED = 1/0 if file is prepped or not
;
; History     : 24 July 2009, Zarro (ADNET) - written
;
; Contact     : dzarro@solar.stanford.edu
;-

function get_fits_inst,file,_ref_extra=extra,err=err,stc=stc,prepped=prepped

if is_blank(file) then return,''

;-- check if remote or local file

if is_url(file) then sock_fits,file,data,header,/nodata,err=err else $
 mrd_head,file,header,_extra=extra,err=err
if is_string(err) then return,''

;-- look for "standard" keywords

inst=''
stc=fitshead2struct(header)
chk=['detec','instr','tele']
for i=0,n_elements(chk)-1 do begin
 if have_tag(stc,chk[i],/start,index) then begin
  inst=stc.(index)
  if is_string(inst) then break
 endif
endfor

;-- check if prepped by looking for "Degrid" or "Flat Field" keywords

prepped=0b
chk=where(stregex(header,'(Degrid|Flat Field)',/bool,/fold),count)
prepped=count gt 0

return,inst
end
