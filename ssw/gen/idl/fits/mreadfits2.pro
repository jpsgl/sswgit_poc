;+
; Project     : HESSI
;
; Name        : MREADFITS2
;
; Purpose     : wrapper around MREADFITS and MRDFITS
;
; Category    : utility I/O
;
; Syntax      : IDL> mreadfits2,file,index,data,_extra=extra
;
; Inputs      : FILE = FITS file
;
; Outputs     : INDEX/DATA = index/data 
;
; Keywords    : See MRDFITS
;
; History     : Written 2 July 2000, D. Zarro, EIT/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

pro mreadfits2,file,index,data,_extra=extra,err=err,extension=extension

;-- check file locations

err=''
nfiles=loc_file(file,err=err,count=count)
if count eq 0 then begin
 message,'Input file(s) not found',/cont
 return
endif

if count gt 1 then begin
 err='Currently limited to single FITS files'
 message,err,/cont
 return 
endif

cfile=nfiles(0)

;-- check if valid FITS files

compressed=is_compressed(cfile,type)
if (not compressed) then begin
 if not is_fits(cfile) then begin
  err='Invalid FITS file'
  message,err,/cont
  return
 endif
endif

;-- use new MRDFITS if file is compressed and version 5.3 or better

use_mrdfits=idl_release(lower=5.3,/inc) and have_proc('mrdfits') and compressed

if (type eq 'Z') and (os_family(/lower) eq 'windows') then use_mrdfits=0b

if use_mrdfits then begin
 if not is_number(extension) then extension=0
 dprint,'% MREADFITS2: decompressing on the fly...'
 data=call_function('mrdfits',cfile,extension,header,_extra=extra,status=status)
 if status ge 0 then begin
  index=fitshead2struct(header)
 endif else begin
  if status eq -2 then err='End of file during read' else $ 
   err='Error during read'
 endelse
 return
endif

;-- otherwise use MREADFITS

dfile=cfile
if compressed then begin
 dfile=find_compressed(cfile,err=err)
 if err ne '' then begin
  message,err,/cont
  return
 endif
endif

if not is_fits(dfile) then begin
 err='Invalid FITS file'
 message,err,/cont
 return
endif

mreadfits,dfile,index,data,_extra=extra

return & end
