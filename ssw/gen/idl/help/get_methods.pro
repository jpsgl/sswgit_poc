;+
; Project     : HESSI
;
; Name        : GET_METHODS
;
; Purpose     : find methods in an object
;
; Category    : utility objects
;
; Explanation : checks CLASS__DEFINE procedure for input class name
;
; Syntax      : IDL>get_method,class,out
;
; Examples    :
;
; Inputs      : CLASS = class name 
;
; Opt. Inputs : None
;
; Outputs     : OUT = string array of method calls
;
; Opt. Outputs: None
;
; Keywords    : QUIET = inhibit printing
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 20 May 1999, D. Zarro, SM&A/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

pro get_methods,class,out,quiet=quiet

loud=1-keyword_set(quiet)

if datatype(class) ne 'STR' then begin
 pr_syntax,'get_meth,class_name,output'
 return
endif 

;-- extract internal calls

temp=''
chkarg,trim(class)+'__define',out=temp,/quiet
if trim(temp(0)) eq '' then begin
 message,'"'+class+'" is probably not a valid object class name',/cont
 return
endif

;-- search for method calls with '::'

calls=where(strpos(temp,'::') gt -1,count)
if count gt 0 then begin
 if count gt 0 then out=temp(calls)
 if count eq 1 then out=out(0)
 if loud then for i=0,count-1 do print,out(i)
endif else message,'No methods found for "'+out+'"',/cont

return & end
