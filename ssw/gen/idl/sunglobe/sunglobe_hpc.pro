;+
; Project     :	ORBITER - SPICE
;
; Name        :	SUNGLOBE_HPC
;
; Purpose     :	Widget to show HPC coordinates in SUNGLOBE
;
; Category    :	Object graphics, 3D, Planning, generic
;
; Explanation : This routine brings up a widget to display the pointing
;               converted from spacecraft X and Y, which depend on the
;               spacecraft roll, into Helioprojective Cartesian (HPC) X and Y,
;               which are independent of roll.  The HPC values can also be
;               edited and passed back to the SunGlobe widget.
;
; Syntax      :	SUNGLOBE_HPC, sState
;
; Examples    :	See sunglobe_event.pro
;
; Inputs      :	sState = SunGlobe state structure.
;
; Keywords    :	GROUP_LEADER = The widget ID of the group leader.
;
;               MODAL   = Run as a modal widget.
;
; History     :	Version 1, 30-Aug-2016, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;

;------------------------------------------------------------------------------

pro sunglobe_hpc_event, event
;
;  If the window close box has been selected, then kill the widget.
;
if (tag_names(event, /structure_name) eq 'WIDGET_KILL_REQUEST') then $
  goto, destroy
;
;  Get the current state structure.
;
widget_control, event.top, get_uvalue=sstate, /no_copy
;
widget_control, event.id, get_uvalue=uvalue
case uvalue of
    'XYHPC': begin
        widget_control, sstate.wxhpc, get_value=xhpc
        widget_control, sstate.wyhpc, get_value=yhpc
        widget_control, sstate.mstate.wroll, get_value=roll
;
;  Calculate the S/C coordinates.
;
        roll = roll * !dpi / 180.d0
        croll = cos(roll)
        sroll = sin(roll)
        xsc = xhpc * croll - yhpc * sroll
        ysc = xhpc * sroll + yhpc * croll
;
;  Refresh the graphics window.
;
        widget_control, sstate.wxhpc, set_value=xhpc
        widget_control, sstate.wyhpc, set_value=yhpc
        widget_control, sstate.mstate.wxsc, set_value=xsc
        widget_control, sstate.mstate.wysc, set_value=ysc
        sunglobe_scpoint, sstate.mstate
    end
    'EXIT': begin
destroy:
        widget_control, event.top, /destroy
        return
    end
    else:
endcase
;
widget_control, event.top, set_uvalue=sstate, /no_copy
end

;------------------------------------------------------------------------------

pro sunglobe_hpc, mstate, group_leader=group_leader, modal=modal, _extra=_extra
;
;  Get the boresight position from the widget, and the roll.
;
widget_control, mstate.wxsc, get_value=xsc
widget_control, mstate.wysc, get_value=ysc
widget_control, mstate.wroll, get_value=roll
;
;  Calculate the HPC coordinates.
;
roll = roll * !dpi / 180.d0
croll = cos(roll)
sroll = sin(roll)
xhpc =  xsc * croll + ysc * sroll
yhpc = -xsc * sroll + ysc * croll
;
;  Set up the top base as a column widget.
;
wtopbase = widget_base(/column, group_leader=group_leader, modal=modal, $
                      _extra=_extra)
dummy = widget_label(wtopbase, value='Derolled HPC coordinates', /align_center)
whpcpoint = widget_base(wtopbase, /align_center, /row)
wxhpc = cw_field(whpcpoint, /frame, /row, value=xhpc, uvalue='XYHPC', $
                 title="X", /float, xsize=9, /return_events)
wyhpc = cw_field(whpcpoint, /frame, /row, value=yhpc, uvalue='XYHPC', $
                 title="Y", /float, xsize=9, /return_events)
wbuttonbase = widget_base(wtopbase, /align_center, /row)
dummy = widget_button(wbuttonbase, value='Apply', uvalue='XYHPC')
dummy = widget_button(wbuttonbase, value='Dismiss', uvalue='EXIT')
;
;  Realize the widget hierarchy.
;
widget_control, wtopbase, /realize
;
;  Define the state structure, and store it in the top base.
;
sstate = {mstate: mstate, $
          wtopbase: wtopbase, $
          wxhpc: wxhpc, $
          wyhpc: wyhpc}
widget_control, wtopbase, set_uvalue=sstate, /no_copy
;
;  Start the whole thing going.
;
xmanager, 'sunglobe_hpc', wtopbase
;
end
