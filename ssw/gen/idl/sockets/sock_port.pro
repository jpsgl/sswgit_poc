;+
; Project     : VSO
;
; Name        : SOCK_PORT
;
; Purpose     : Return port assigned by socket
;
; Category    : system utility sockets
;
; Syntax      : IDL> port=sock_port()
;
; Inputs      : None
;
; Outputs     : PORT = assigned port
;
; Keywords    : None
;
; History     : 28 December 2015, Zarro (ADNET) - written
;
; Contact     : dzarro@solar.stanford.edu
;-

function sock_port

help,/files,out=out
out=strjoin(out,',')
chk=stregex(out,'\.([0-9]+)',/ext,/sub)
if is_number(chk[1]) then return,long(chk[1]) else return,-1
end
