;+
; Project     : EIS
;
; Name        : EIS_LIST_MAIN
;
; Purpose     : List entries in EIS main catalog. Checks for local
;               copy of catalog in ZDBASE. If not available, accesses
;               remote copy via socket call.
;
; Category    : utility sockets
;                   
; Inputs      : TSTART, TEND = start/end times for list
;
; Outputs     : OBS = main observation structure
;               N_FOUND = number of elements found
;
; Keywords    : SEARCH = search string
;               SAVE_FILE = save file in which OBS is saved
;
; History     : 29-Aug-2002,  D.M. Zarro (LAC/GSFC)  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-
;------------------------------------------------------------------------

;-- save utility for passing DB structures via socket

pro eis_save,obs,save_file

if is_string(save_file) then begin
 break_file,save_file,dsk,dir
 outdir=dsk+dir
 if is_blank(outdir) then begin
  outdir=curdir()
  outfile=concat_dir(outdir,save_file)
 endif else outfile=save_file
 if test_dir(outdir) then begin
  null=-1
  if datatype(obs) eq 'STC' then $
   savegen,obs,file=outfile,/notype else savegen,null,file=outfile,/notype
 endif
endif

return & end

;------------------------------------------------------------------------

pro eis_list_main,tstart,tend,obs,n_found,search=search,err=err,$
                  query=query,save_file=save_file,verbose=verbose

common eis_list_main,t1_l,t2_l,obs_l,search_l

err=''
n_found=0
delvarx,obs
verbose=keyword_set(verbose)

;-- check for valid time 

if valid_time(tstart)*valid_time(tend) eq 0 then begin
 err='invalid time input'
 pr_syntax,'eis_list_main,tstart,tend,obs,n_found,[search=search]'
 return
endif

;-- convert time part of search to string format

t1=trim(str_format(anytim2tai(tstart),'(f15.0)'))
t2=trim(str_format(anytim2tai(tend),'(f15.0)'))

;-- check if cached

if datatype(obs_l) eq 'STC' then begin
 cached= (t1 eq t1_l) and (t2 eq t2_l)
 if cached and is_string(search) and is_string(search_l) then cached = search eq search_l
 if cached then begin
  obs=obs_l 
  n_found=n_elements(obs)
  if verbose then message,'restoring from cache',/cont
  eis_save,obs,save_file
  return
 endif
endif

;-- check if db is available locally

defsysv,'!priv',2
unavail=0
dbopen,'main',unavail=unavail

if (unavail eq 0) then begin

 list_main,tstart,tend,obs,n_found,err=err

;-- save results (if requested)

 eis_save,obs,save_file

endif else begin

;-- else check for network connection to server

 server=rpc_server()
 if not have_network(server,err=err) then begin
  message,err,/cont
  return
 endif

;-- create a session ID

 get_sid,sid

;-- construct query string

 rpc_cmd="http://"+server+"/cgi-bin/rpc?"
 save_file='/tmp/m'+sid+'.sav'            
 rfile="'"+save_file+"'"
 ts="'"+trim(tstart)+"'"
 te="'"+trim(tend)+"'"
 query=rpc_cmd+"eis_list_main,"+ts+","+te+",save="+rfile

;-- send query

 if verbose then message,'checking remote server DB...',/cont
 sock_list,query,out,err=err

;-- now look for and copy file from remote server

 query='http://'+server+'/~zarro'+save_file
 delvarx,obs
 ldir=get_temp_dir()
 sock_copy,query,out_dir=ldir,err=err
 chk=loc_file(save_file,path=ldir,count=count)

;-- remove save file from local and remote directories

 if count gt 0 then begin
  restgen,obs,file=chk[0]
  file_delete,chk[0],/quiet
  query=rpc_cmd+"file_delete,"+rfile+",quiet='1'"
  sock_list,query,out
 endif else begin
  err='remote server currently unavailable'
  message,err,/cont
  return
 endelse

endelse

;-- cache search results

if datatype(obs) eq 'STC' then begin
 obs_l=obs & t1_l=t1 & t2_l=t2
 if is_string(search) then search_l=search else delvarx,search_l
 n_found=n_elements(obs)
endif else begin
 if verbose then message,'no records found for specified time',/cont
 delvarx,obs,obs_l,t1_l,t2_l,search_l
endelse

return
end




