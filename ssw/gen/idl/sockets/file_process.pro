pro file_process,buffer,fname,fid

temp_dir=getenv('IDL_TMPDIR')
outdir=concat_dir(temp_dir,trim(fid))
if ~file_test(outdir,/dir) then file_mkdir,outdir
outfile=concat_dir(outdir,fname)
poutfile=concat_dir(outdir,'prepped_'+fname)

openw,lun,outfile,lun,/get_lun
writeu,lun,buffer
close_lun,lun
mprint,'Wrote file to '+outfile
sdo=obj_new('sdo')
sdo->read,outfile
sdo->write,poutfile
mprint,'Wrote prepped file to '+poutfile
obj_destroy,sdo
return & end
