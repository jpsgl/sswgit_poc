;+
; Project     : VSO
;
; Name        : RECVAR
;
; Purpose     : Receive a variable sent from an open socket
;
; Category    : sockets
;
; Inputs      : LUN = socket logical unit number
;
; Outputs     : NAME = string name of variable
;               VALUE = variable value
;
; Keywords    : FORMAT = type of value ('data','file','command')
;               SESSION = unique session ID to identify sender
;               ERR = error string
;
; History     : 22-Nov-2015, Zarro (ADNET) - written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro recvar,lun,name,value,err=err,verbose=verbose,format=format,session=session

 on_ioerror,bail
 success=0b
 verbose=keyword_set(verbose)
 err=''
 name='' & value=!null & format=''

 if ~is_number(lun) then begin
  pr_syntax,'recvar,socket_lun,variable_name,variable_value'
  return
 endif

 if ~is_socket(lun) then begin
  err='Socket closed.'
  mprint,err
  return
 endif

 hsize=0L
 readu,lun,hsize
 if hsize eq 0 then begin
  err='Cannot read data.'
  mprint,err
  return
 endif

 hdata=bytarr(hsize,/nozero)
 readu,lun,hdata
 jheader=data_unstream(hdata,type=7,err=err)
 if is_string(err) then return
 header=json_parse(jheader,/tostruct,/toarray)
 bsize=header.bsize
 buffer=bytarr(bsize,/nozero)
 name=header.name
 type=header.type
 compress=header.compress
 format=header.format
 session=header.session
 dimensions=header.dimensions
 if verbose then mprint,'Receiving variable "'+name+'"'
 ReadU, lun, Buffer, Transfer_Count = TC
 T = TC
 While (T ne bsize) Do Begin
  B = BytArr(bsize - T)
  ReadU, lun, B, Transfer_Count = TC
  If (TC ne 0) then Begin
   Buffer[T] = B[0:TC - 1]
   T += TC
  Endif
 Endwhile
 if verbose then mprint,'Read '+trim(t)+' bytes.'
 if t ne bsize then begin
  err='Data partially read.'
  mprint,err
  return
 endif
 if format eq 'file' then begin
  if compress then value=zlib_uncompress(temporary(buffer),type=1,dimensions=dimensions) else value=temporary(buffer) 
 endif else value=data_unstream(buffer,type=type,dimensions=dimensions,err=err,/no_copy)
 success=1b
 bail:
 if ~success then begin
  err='Error reading data from socket.' 
  mprint,err
 endif

 return & end
