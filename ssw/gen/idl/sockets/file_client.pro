

Pro File_ServerCallback, ID, serverlun
Compile_Opt IDL2

status=File_Poll_Input(serverlun, Timeout = .01)
If status then begin
 file_readu,serverlun,buffer,fname=fname,fid=fid,err=err
 if is_blank(err) then file_process,buffer,fname,fid
endif
!null = Timer.Set(.01, 'File_ServerCallback', serverlun)
End

;----------------------------------------------------------------
pro file_readu,serverlun,buffer,fname=fname,fid=fid,err=err
 err=''
 bsize=0L
 readu,serverlun,bsize
 bname=bytarr(bsize)
 readu,serverlun,bname
 fname=string(bname)
 fid=long64(0)
 readu,serverlun,fid
 mprint,'Reading '+fname+' with ID '+trim(fid)
 fsize=long64(0)
 readu,serverlun,fsize
 if fsize gt 0 then begin
  buffer=bytarr(fsize,/nozero)
  ReadU, serverlun, Buffer, Transfer_Count = TC
  T = TC
  While (T ne fsize) Do Begin
    B = BytArr(fsize - T)
    ReadU, serverlun, B, Transfer_Count = TC
    If (TC ne 0) then Begin
      Buffer[T] = B[0:TC - 1]
      T += TC
    Endif
   Endwhile
   mprint,'Read '+trim(t)+' bytes'
   if t ne fsize then begin
    err='File partially read.'
    mprint,err
   endif
 endif else begin
  err='File has zero byte size.'
  mprint,err
 endelse
return & end

;-------------------------------------------------------------------------

Pro file_client, Server = Server
Compile_Opt IDL2
Port = (UInt(Byte('IDLRocks'), 0, 2))[1]
slun=sock_lun(port)
if slun eq -1 then begin
 Socket, ServerLUN, Server eq !null ? 'localhost' : Server, Port, /Get_LUN, $
  Connect_Timeout = 10., $
  Read_Timeout = 10., Write_Timeout = 10., /RawIO, $
    /Swap_If_Big_Endian
endif else begin
 mprint,'Client already running on port '+trim(port)+' with LUN '+trim(slun)
 serverlun=slun
endelse
!null = Timer.Set (.001, 'File_ServerCallback',serverlun)
End
