;+
; Project     : VSO
;
; Name        : SENDVAR
;
; Purpose     : Send variable to an open socket
;
; Category    : sockets
;
; Inputs      : LUN = socket logical unit number
;               NAME = string name of variable
;               VALUE = variable value
;
; Outputs     : None
;
; Keywords    : ERR = error string
;               FILE = name is a filename string
;               COMMAND = name is a command string
;               HELP = return help on name
;               GET = get variable with name 
;
; History     : 22-Nov-2015, Zarro (ADNET) - written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro sendvar,lun,name,value,err=err,debug=debug,file=file,command=command,$
            compress=compress,help=help,print=print,get=get,session=session

on_ioerror,bail

success=0b
err=''
debug=keyword_set(debug)
if ~is_number(lun) then begin
 pr_syntax,'sendvar,socket_lun,variable_name,variable_value'
 return
endif

if ~is_socket(lun) then begin
 err='Server stopped.'
 mprint,err
 return
endif

if is_blank(name) then begin
 err='Variable name not entered.'
 mprint,err
 return
endif

command=keyword_set(command)
file=keyword_set(file)
help=keyword_set(help)
print=keyword_set(print)
get=keyword_set(get)
if is_blank(session) then session=session_id()

case 1 of
 file: begin
  format='file' 
  value=name
 end
 command: begin
  format='command'
  value=name
 end
 help: begin
  format='help'
  value=name
 end
 print: begin
  format='print'
  value=name
 end
 get: begin
  format='get'
  value='get'
 end
 else: format='data'
endcase

if n_elements(value) eq 0 then begin
 err='Variable value not entered.'
 mprint,err
 return
endif

;-- convert value into a byte stream

compress=byte(keyword_set(compress))
if file then begin
 type=1
 bdata=file_stream(value,compress=compress,err=err,bsize=bsize,osize=dimensions)
endif else bdata=data_stream(value,type=type,dimensions=dimensions,err=err,bsize=bsize)
if is_string(err) then return

;-- create header with byte stream parameters
 
header={name:name,format:format,compress:compress,bsize:bsize,type:type,dimensions:dimensions,session:session}
jheader=json_serialize(header)
hdata=data_stream(jheader,bsize=hsize)

if debug then begin
 case 1 of 
  file: mprint,'Sending file "'+name+'"'
  command: mprint,'Sending command "'+name+'"'
  get: mprint,'Requesting variable "'+name+'"'
  else: mprint,'Sending variable "'+name+'"'
 endcase
endif

;-- send the header and data

writeu,lun,hsize
writeu,lun,hdata
writeu,lun, bdata, Transfer_Count = tc
if debug then mprint,'Sent '+trim(tc)+' bytes of '+trim(bsize)+' bytes.'

success=1b

bail:
destroy,bdata
if ~success then mprint,'Error writing data to socket.'

return & end
