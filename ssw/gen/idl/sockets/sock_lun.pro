;+
; Project     : HESSI
;
; Name        : SOCK_LUN
;
; Purpose     : Check which socket LUN a port is assigned to
;
; Category    : system utility sockets
;
; Syntax      : IDL> lun=sock_lun(port)
;
; Inputs      : PORT = port number
;
; Outputs     : LUN = unit number associated with a port
;
; Keywords    : SERVER = set if seeking a LUN assigned to a SERVER socket
;               LISTENER = set if seeking a LUN assigned to a LISTENER socket
;
; History     : 20 October 2015, Zarro (ADNET) - written
;
; Contact     : dzarro@solar.stanford.edu
;-

function sock_lun,port,server=server,listener=listener,count=count

count=0
if ~is_number(port) then return,-1
reg='\.'
if keyword_set(listener) then reg='\<listener\>\.' else $
 if keyword_set(server) then reg='\<server\>\.'

help,/files,out=out
plun=-1
reg=reg+trim(port)
for i=0,n_elements(out)-1 do begin
 chk=stregex(out[i],'([0-9]+) +',/ext,/sub)
 if is_number(chk[1]) then begin
  slun=long(chk[1])
  stat=(fstat(slun)).name
  check=stregex(stat,reg,/bool,/fold)
  if check then plun=[plun,slun]
 endif
endfor

nlun=n_elements(plun)
if nlun gt 1 then begin
 plun=plun[1:nlun-1]
 count=n_elements(plun)
endif

if count eq 1 then plun=plun[0]
return,plun

end
