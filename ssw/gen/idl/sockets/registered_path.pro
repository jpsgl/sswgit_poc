

function registered_path,file,err=err

err=''
if is_blank(file) then return,''
rdir=local_name('PYIDL_REGISTERED')

if is_blank(rdir) then begin
 err='PYIDL_REGISTERED environment variable undefined.' 
 return,''
endif

dfile=strtrim(file,2)
if ~stregex(dfile,'\.pro$',/bool) then dfile=dfile+'.pro'
if file_test(concat_dir(rdir,dfile),/regular) then return,rdir else return,''
end
