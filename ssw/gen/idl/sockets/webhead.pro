;+
; Project     : VSO
;
; Name        : WEBHEAD
;
; Purpose     : Use IDLnetURL object to send a HEAD request
;
; Category    : utility system sockets
;
; Syntax      : IDL> header=webhead(url)
;
; Inputs      : URL = URL file name to check
;
; Outputs     : HEADER = response header 
;
; Keywords    : RESPONSE_CODE = status code (e.g. 200 for success)
;
; Example     :
;
;          IDL> f='http://sohowww.nascom.nasa.gov/pickoftheweek/Oct_C3_halo.jpg'
;          IDL> h=webhead(f)
;          IDL> print,h
;               HTTP/1.1 200 OK
;               Date: Thu, 30 Oct 2014 15:46:31 GMT
;               Server: Apache/2.4.10 (Unix)
;               Last-Modified: Fri, 17 Oct 2014 20:05:21 GMT
;               ETag: "24b46-505a3e28764e6"
;               Accept-Ranges: bytes
;               Content-Length: 150342
;               Content-Type: image/jpeg
;
; History     : 30-October-2014, Zarro (ADNET) - Written
;               31-January-2015, Zarro (ADNET)
;               - added support for FTP
;
;-

function webhead_callback, status, progress,data  

;-- since we only need the response header, we just read until
;   non-zero bytes are encountered.

if (progress[0] eq 1) && (progress[2] gt 0) then return,0

return,1

end

;-----------------------------------------------------------------------------
  
function webhead,url,response_code=response_code,_ref_extra=extra

if size(url,/tname) ne 'STRING' then return,''
if n_elements(url) ne 1 then begin
 message,'Input URL must be scalar.',/info
 return,''
endif

;-- ensure we have a URL scheme

durl=url
if ~stregex(url,'(ftp[s]?\:\/\/)|(http[s]?\:\/\/)',/bool,/fold) then durl='http://'+url 

stc=parse_url(durl)

o=obj_new('idlneturl')

;-- add a USER-AGENT as some servers expect one

agentstr='IDL/'+!version.release+' '+!version.os+'/'+!version.arch
o->setproperty,header='User-Agent: '+agentstr

;-- trick to pass URL properties to object so as not to expose
;   passwords

tags='URL_'+tag_names(stc)
for i=0,n_elements(tags)-1 do begin
 if i eq 0 then stash=create_struct(tags[i],stc.(i)) else $
  stash=create_struct(stash,tags[i],stc.(i))
endfor
o->setproperty,_extra=stash
o->setproperty,_extra=extra

;-- establish callback function to interrupt GET
;   (not necessary if URL path is blank and just pinging server)

if stc.path ne '' then o->setproperty,callback_function='webhead_callback'

;-- use a catch since canceling the callback triggers it

header=''
error=0
catch, error
if (error eq 0) then output=o->get(/buffer) else catch,/cancel
o->getproperty,response_header=response_header

obj_destroy,o

;--  extract status code from header response

response_code=404
if strpos(durl,'http') gt -1 then begin
 u=stregex(response_header,'http[s]?\/[0-9]{1}\.[0-9]{1} +([0-9]+).*',/sub,/extr,/fold)
 chk=where(u[1,*] ne '',count)
 if count gt 0 then response_code=fix(u[1,chk[0]])
endif else begin
 if stregex(response_header,'(150 )',/bool) then response_code=200
endelse

return,response_header 
end  
