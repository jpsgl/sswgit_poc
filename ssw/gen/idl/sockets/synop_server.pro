;+
; Project     : HESSI
;
; Name        : SYNOP_SERVER
;
; Purpose     : return first available Synoptic data server
;
; Category    : synoptic sockets
;                   
; Inputs      : None
;
; Outputs     : SERVER = Synoptic data server name
;
; Keywords    : NETWORK = 1 if network is up
;               PATH = path to synoptic data
;               NO_CHECK = don't check network status
;               FULL_NAME = prepend 'http://'
;
; History     : 29-Dec-2001,  D.M. Zarro (EITI/GSFC)  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-


function synop_server,path=path,_ref_extra=extra,network=network,$
                           no_check=no_check,full_name=full_name

;-- primary server

full_name=keyword_set(full_name)
network=1b
path='/data/synoptic'
server='sohowww.nascom.nasa.gov'
if keyword_set(no_check) then begin
 if full_name then server='http://'+server
 return,server
endif

network=have_network(server,_extra=extra)

;-- if primary server is down, try secondary

if not network then begin
 path='/synop_data'
 server='smmdac.nascom.nasa.gov'
 network=have_network(server,_extra=extra)
endif

if full_name then server='http://'+server
return,server
end
