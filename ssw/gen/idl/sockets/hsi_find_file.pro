;+
; Project     : HESSI
;
; Name        : HSI_FIND_FILE
;
; Purpose     : Wrapper around HSI_LOC_FILE and HSI_SOCK_COPY
;
; Category    : utility system sockets
;
; Syntax      : IDL> file=hsi_find_file(file,outdir=outdir)
;
; Inputs      : FILE = filename to find and possibly copy
;
; Outputs     : FILE = filename with local path
;
; Keywords    : OUT_DIR = output directory to copy file
;               ERR   = string error message
;               CLOBBER = set to clobber existing file when downloading
;               VERBOSE = set for message output
;               SEARCH_NETWORK = force a network search if file not
;                         found locally
;
; History     : 27-March-2002,  D.M. Zarro (L-3Com/GSFC) Written.
;-

function hsi_find_file,file,verbose=verbose,count=count,_ref_extra=extra,$
                       search_network=search_network,clobber=clobber

count=0
verbose=keyword_set(verbose)
if is_blank(file) then begin
 if verbose then message,'Please enter a valid file name to search for',/cont
 return,''
endif

;-- check local archives first

result=''
if have_proc('hsi_loc_file') then begin
 result=call_function('hsi_loc_file',file,/no_dialog,_extra=extra,$
                       count=count,verbose=verbose)
endif

;-- check remote archives next

if (1-keyword_set(search_network)) then begin
 if not check_network() then return,result
endif

ifile=get_uniq(file,count=nfiles)
break_file,local_name(ifile),dsk,dir,name,ext
ifile=name+ext

clobber=keyword_set(clobber)
for i=0,nfiles-1 do begin
 if is_string(ifile[i]) then begin
  chk=stregex(result,ifile[i],/bool)
  found=where(chk gt 0,count)
  if (count eq 0) or clobber then begin
   rfile=''
   if i eq 0 then begin
    if not allow_sockets(_extra=extra,/verbose) then return,result
    network_flag=have_network(_extra=extra,/verbose)
    if not network_flag then return,result
   endif
   rfile=hsi_sock_copy(ifile[i],_extra=extra,/verbose,err=err)
   if err ne '' then begin
    xack,err
    return,result
   endif   
   if is_string(rfile) then sfile=append_arr(sfile,rfile)
  endif else sfile=append_arr(sfile,result[found])
 endif
endfor

count=n_elements(sfile)
if count eq 0 then sfile='' 
sfile=get_uniq(sfile,count=count)
if count eq 1 then sfile=sfile[0]

return,sfile

end
