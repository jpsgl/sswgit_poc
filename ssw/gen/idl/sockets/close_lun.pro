;+
; Project     : HESSI
;                  
; Name        : CLOSE_LUN
;               
; Purpose     : Same as FREE_LUN but with error checks
;                             
; Category    : system utility i/o
;               
; Syntax      : IDL> close_lun,lun
;
; Inputs      : LUN = logical unit number to free and close
;
; History     : 6 May 2002, Zarro (L-3Com/GSFC)
;               25 November 2015, Zarro (ADNET) - added _extra
;
; Contact     : dzarro@solar.stanford.edu
;-    

pro close_lun,lun,_ref_extra=extra,all=all

on_ioerror,bail

error=0
catch,error
if error ne 0 then begin
 catch,/cancel
 return
endif

if keyword_set(all) then begin
 close,/all,/force
 return
endif

if ~is_number(lun) then return
if lun le 0 then return
close,lun,/force,_extra=extra
free_lun,lun,/force,_extra=extra

bail:

return & end
