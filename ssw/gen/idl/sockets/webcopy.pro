;+
; Project     : VSO
;
; Name        : WEBCOPY
;
; Purpose     : Use IDLnetURL object to download multiple files
;
; Category    : utility system sockets
;
; Syntax      : IDL> webcopy,files,outdir
;
; Inputs      : FILES = File URL's to download
;               OUTDIR = Directory to download to [def= current]
;
; Outputs     : None
;
; Keywords    : CLOBBER = clobber output file if already exists
;               VERBOSE = set for informative messages
;               URL_USERNAME = username for server (not always required)
;               URL_PASSWORD = password for server (not always required)
;
; History     : 22-January-2015, Zarro (ADNET) - Written
;               30-January-2015, Zarro (ADNET) 
;               - added check for Content-Disposition in header
;               - added username/password support
;-

pro webcopy_main,url,outdir,verbose=verbose,clobber=clobber,_ref_extra=extra

if size(url,/tname) ne 'STRING' then return

;-- default to current directory for download

cd,current=current
if n_elements(outdir) eq 0 then outdir=current
if outdir eq '' then outdir=current

;-- ensure download directory is accessible

if ~file_test(outdir,/dir,/write) then begin
 message,'Download directory not accessible - '+outdir,/info
 return
endif

;-- check if we have a URL scheme as IDLnetURL expects one 

durl=url
if ~stregex(url,'(ftp[s]?\:\/\/)|(http[s]?\:\/\/)',/bool,/fold) then durl='http://'+url

;-- check that file exists on server

header=webhead(durl,response_code=code,_extra=extra)
if fix(strmid(strtrim(code,2),0,1)) ne 2 then begin
 message,'File not found on server - '+durl,/info
 return
endif

;-- create the output filename

stc=parse_url(durl)
local_name=file_basename(stc.path)

;-- check for Content-Disposition keyword in header in case downloaded
;   filename differs from that on server

regex='Content-Disposition:.*filename=\"(.+)\".*'
check=stregex(header,regex,/extrac,/sub)
if check[1] ne '' then local_name=check[1]

outfile=strjoin([outdir,local_name],path_sep())
 
;-- overwrite if clobber is set

verbose=keyword_set(verbose)

if file_test(outfile) and ~keyword_set(clobber) then begin
 if verbose then begin
  message,'File already downloaded - '+durl,/info
  message,'Use /clobber to re-download.',/info
 endif
 return
endif

;-- create IDL network object and set a catch in case of problems

o=obj_new('IDLnetURL')

;-- add a USER-AGENT as some servers expect one

agentstr='IDL/'+!version.release+' '+!version.os+'/'+!version.arch
o->setproperty,header='User-Agent: '+agentstr

;-- trick to pass URL properties to object so as not to expose
;   passwords 

tags='URL_'+tag_names(stc)
for i=0,n_elements(tags)-1 do begin
 if i eq 0 then stash=create_struct(tags[i],stc.(i)) else $
  stash=create_struct(stash,tags[i],stc.(i))
endfor

o->setproperty,_extra=stash
o->setproperty,_extra=extra

;-- now start downloading

error=0
catch, error

if (error eq 0) then begin
 output=o->get(file=outfile) 
endif else begin
 catch,/cancel
 file_delete,outfile,/allow,/quiet
endelse

if ~file_test(outfile) then message,'Error downloading - '+durl,/info else if verbose then message,'Successfully downloaded to '+outfile,/info

obj_destroy,o
return 
end

;----------------------------------------------------------------
pro webcopy,files,outdir,_ref_extra=extra

nfiles=n_elements(files)
if nfiles eq 0 then return

for i=0,nfiles-1 do webcopy_main,files[i],outdir,_extra=extra

return 
end
