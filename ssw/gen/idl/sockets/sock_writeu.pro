;+
; Project     : VSO
;
; Name        : SOCK_WRITEU
;
; Purpose     : Write byte data to an open socket.
;
; Category    : system utility sockets
;
; Syntax      : IDL> sock_write,lun,data
;
; Inputs      : LUN = open socket LUN
;               DATA = byte data
;
; Outputs     : VALUE = HTTP header
;
; Keywords    : ERR = error string
;               VERBOSE = set for messages
;               TIME_OUT= seconds after which to time out (def 30)
;
; History     : 13 February 2016, Zarro (ADNET) - written
;
; Contact     : dzarro@solar.stanford.edu
;-

pro sock_writeu,lun,data,time_out=out,verbose=verbose,err=err

if ~is_socket(lun) then begin
 err='Socket unavailable.'
 mprint,err
 return
endif

if ~is_byte(data) then return
if ~is_number(time_out) then time_out=30
verbose=keyword_set(verbose)
bsize=n_elements(data)
success=0b

on_ioerror,bail

t1=systime(/sec)
again: flush,lun
writeu,lun, data
tcb=(fstat(lun)).Transfer_Count
if verbose then mprint,'Sent '+trim(tcb)+' bytes of '+trim(bsize)+' bytes.'
success=tcb eq bsize

;-- beware, ugly goto shit below

bail: 
if ~success then begin 
 tcb=(fstat(lun)).Transfer_Count
 if (tcb lt bsize) then begin
  t2=systime(/sec)
  if (t2-t1) lt time_out then begin
   data=data[tcb:bsize-1]
   bsize=n_elements(data)
   mprint,'Retrying with '+trim(bsize)+' bytes.'
   goto,again 
  endif else begin
   err='Socket not responding.'
   mprint,err
  endelse
 endif
 help,tcb
 err='Error writing data to socket.'
 mprint,err
 serr=err_state()
 if is_string(serr) then mprint,serr
endif

return & end
