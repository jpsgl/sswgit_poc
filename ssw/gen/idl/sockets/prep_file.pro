;+
; Project     : VSO
;
; Name        : PREP_FILE
;
; Purpose     : PREP a file using corresponding instrument prep routine
;
; Category    : utility analysis
;
; Inputs      : FILE = string file name to prep
;
; Outputs     : PFILE = prepped file name
;
; Keywords    : EXTRA = prep keywords to pass to prep routine
;               ERR = error string
;               OUT_DIR = directory of prepped file (input)
;
; History     : 25-Nov-2015, Zarro (ADNET) - written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro prep_file,file,pfile,_ref_extra=extra,err=err,$
              out_dir=out_dir

pfile=''
err=''
if is_blank(file) then begin
 pr_syntax,'prep_file,file,pfile [,out_dir=out_dir]'
 return
endif

if is_string(out_dir) then begin
 if ~file_test(out_dir,/dir,/write) then begin
  err='Inaccessible output directory '+out_dir
  mprint,err
  return
 endif 
endif

session=session_id()
temp_dir=concat_dir(get_temp_dir(),session)

uflag=0b
dfile=file
if is_url(file,/scheme) then begin
 uflag=1b
 file_mkdir,temp_dir
 sock_get,file,local_file=dfile,out_dir=temp_dir,_extra=extra,err=err
 if is_string(err) then goto,cleanup
endif  

if ~file_test(dfile,/regular,/read) then begin
 err='Unreadable or missing input file - '+dfile
 mprint,err
 goto,cleanup
endif

;-- check if compressed

if is_compressed(dfile) then begin
 file_decompress,dfile,out_dir=temp_dir,local_file=ufile,err=err,_extra=extra
 if is_string(err) then goto,cleanup
 dfile=ufile
endif

;-- check for instrument/detector

inst=get_fits_det(dfile,prepped=prepped,err=err)
if is_string(err) then begin
 mprint,err
 goto,cleanup
endif

;--check if prepped

if prepped then begin
 err='File already prepped.'
 mprint,err
 goto,cleanup
endif

;-- check if object is available

error=0
catch, error
if (error ne 0) then begin
 mprint,err_state()
 err='No Prep function associated with this data type.'
 mprint,err
 catch,/cancel
 goto,cleanup
endif

pobj=obj_new(inst)
if ~have_prop(pobj,'prep_prop') then begin
 err='No Prep function associated with this instrument - '+inst
 mprint,err
 goto,cleanup
endif

pobj->read,dfile,_extra=extra,err=err
if uflag then odir=curdir() else odir=file_dirname(def_file(file))
ofile=concat_dir(odir,'prepped_'+file_basename(dfile))
pobj->write,ofile,_extra=extra,err=err,local_file=pfile,out_dir=out_dir

;--clean up 

cleanup:

if obj_valid(pobj) then obj_destroy,pobj
file_delete,temp_dir,/allow,/quiet,/recursive

return & end

