;+
; Project     : HESSI
;                  
; Name        : SEARCH_NETWORK
;               
; Purpose     : Enable/Disable network connection checking
;                             
; Category    : system utility sockets
;               
; Syntax      : IDL> search_network
;
; Outputs     : None
;
; Keywords    : ENABLE = enable network checking [def]
;               DISABLE = disable network checking
;               SSL, GSFC, HEDC = force checking servers at
;                                 Berkeley, Greenbelt, and Zurich
;               DEFAULT = set to default checking mode
;                   
; Side Effects: Sets env SEARCH_NETWORK = 1 if enable 
;
; History     : 14 April 2002, Zarro (L-3Com/GSFC)
;               18 April 2006, Zarro (L-3Com/GSFC) - added server keywords
;
; Contact     : dzarro@solar.stanford.edu
;-    

pro search_network,disable=disable,_extra=extra,$
                default=default,ssl=ssl,gsfc=gsfc,hedc=hedc

case 1 of
 keyword_set(ssl): mklog,'HSI_SOCK_SERVER','hessi.ssl.berkeley.edu'
 keyword_set(gsfc): mklog,'HSI_SOCK_SERVER','hesperia.gsfc.nasa.gov'
 keyword_set(hedc): mklog,'HSI_SOCK_SERVER','www.hedc.ethz.ch'
 keyword_set(default): mklog,'HSI_SOCK_SERVER',''
 else:do_nothing=1
endcase

value='1'
if keyword_set(disable) then value=''
mklog,'SEARCH_NETWORK',value
return
end

