;+
; Project     : HESSI
;
; Name        : SYNOP_SOCK_SERVER
;
; Purpose     : return first available Synoptic data server
;
; Category    : synoptic sockets
;                   
; Inputs      : None
;
; Outputs     : SERVER = Synoptic data server name
;
; Keywords    : NETWORK = 1 if network is up
;               PATH = path to synoptic data
;               NO_CHECK = don't check network status
;
; History     : 29-Dec-2001,  D.M. Zarro (EITI/GSFC)  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-


function synop_sock_server,path=path,_ref_extra=extra,network=network,$
                           no_check=no_check

;-- primary server

network=1b
path='/synop_data'
server='smmdac.nascom.nasa.gov'
if keyword_set(no_check) then return,server

network=have_network(server,_extra=extra)

;-- if primary server is down, try secondary

if not network then begin
 path='/synoptic'
 server='orpheus.nascom.nasa.gov'
 network=have_network(server,_extra=extra)
endif

return,server
end
