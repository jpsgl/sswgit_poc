;+
; Project     : HESSI
;
; Name        : GOES_SERVER
;
; Purpose     : GOES data server
;
; Category    : synoptic sockets
;                   
; Inputs      : None
;
; Outputs     : SERVER = GOES data server name
;
; Keywords    : None
;
; History     : 29-Dec-2001,  D.M. Zarro (EITI/GSFC)  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-


function goes_server,force=force

common goes_server,last_server

if keyword_set(force) then delvarx,last_server
if exist(last_server) then return,last_server

;-- primary server

server='smmdac.nascom.nasa.gov'
if not allow_sockets() then return,server
if not have_network(server,force=force) then return,server

err=''
sock_open,server,lun,err=err
close_lun,lun

;-- if primary server is down, try secondary

if err ne '' then begin
 server='orpheus.nascom.nasa.gov'
 sock_open,server,lun,err=err
 close_lun,lun
endif

if err eq '' then last_server=server

return,server
end
