;+
; Project     : VSO
;
; Name        : WEBREAD
;
; Purpose     : Read remote file using byte serving
;
; Category    : utility system sockets
;
; Syntax      : IDL> output=webread(url,range=range)
;
; Inputs      : URL = remote URL file to read
;
; Outputs     : OUTPUT = output byte array
;
; Keywords    : RANGE = [n1,n2] = byte range to read
;               If range is scalar, will read from value to end of
;               file. 
;               RESPONSE_HEADER = server response header
;
; Restrictions: Server must support byte serving 
;               (i.e. 'Accept-Ranges: bytes')
;
; Example:
;          IDL> f='http://sohowww.nascom.nasa.gov/pickoftheweek/Oct_C3_halo.jpg'
;          IDL> o=webread(f,response=response,range=[100,200])
;          IDL> help,o
;               O               BYTE      = Array[101]
;
;          IDL> print,response
;               HTTP/1.1 206 Partial Content
;               Date: Thu, 23 Oct 2014 15:56:31 GMT
;               Server: Apache/2.4.10 (Unix)
;               Last-Modified: Fri, 17 Oct 2014 20:05:21 GMT
;               ETag: "24b46-505a3e28764e6"
;               Accept-Ranges: bytes
;               Content-Length: 101
;               Content-Range: bytes 100-200/150342
;               Content-Type: image/jpeg
;
; History     : 23-Oct-2014 Zarro (ADNET) - Written
;               30-Oct-2014, Zarro (ADNET)
;               - added pre-check for byte-serving
;               - added USER-AGENT HTTP string
;-

function webread,url,range=range,response_header=response_header,verbose=verbose,_ref_extra=extra

response_header=''
if size(url,/tname) ne 'STRING' then return,''
if n_elements(url) ne 1 then begin
 message,'Input URL must be scalar.',/info
 return,''
endif

;-- check if we have a URL scheme as IDLnetURL expects one 

durl=url
if ~stregex(url,'(ftp[s]?\:\/\/)|(http[s]?\:\/\/)',/bool,/fold) then durl='http://'+url

;-- check if server accepts byte ranges

nrange=n_elements(range)
range_requested= (nrange eq 1) || (nrange eq 2)

;-- check that file exists on server

response_header=webhead(durl,response_code=code,_extra=extra)
if fix(strmid(strtrim(code,2),0,1)) ne 2 then begin
 message,'File not found on server - '+durl,/info
 return,''
endif

if range_requested then begin
 chk=where(stregex(response_header,'Accept-Ranges: bytes',/bool,/fold),count)
 if count eq 0 then begin
  message,'Server does not accept range requests.',/info
  return,''
 endif
endif

o=obj_new('idlneturl')

;-- add a USER-AGENT as some servers expect one

agentstr='IDL/'+!version.release+' '+!version.os+'/'+!version.arch
o->setproperty,header='User-Agent: '+agentstr

;-- trick to pass URL properties to object so as not to expose
;   passwords

stc=parse_url(durl)
tags='URL_'+tag_names(stc)
for i=0,n_elements(tags)-1 do begin
 if i eq 0 then stash=create_struct(tags[i],stc.(i)) else $
  stash=create_struct(stash,tags[i],stc.(i))
endfor

o->setproperty,_extra=stash
o->setproperty,_extra=extra

if range_requested then begin
 header='Range: bytes='+strtrim(range[0],2)+'-'
 if nrange eq 2 then header=header+strtrim(range[1],2)
 o->setproperty,header=header
 o->setproperty,header='Accept: */*'
 o->setproperty,header='Connection: keep-alive'
endif

if keyword_set(verbose) then begin
 o->getproperty,header=header
 message,strjoin(header,' ',/single),/info
endif

;-- set a catch in case of I/O problems

output=''
error=0
catch, error
if (error eq 0) then output=o->get(/buffer) else begin
 catch,/cancel
 message,!error_state.msg,/info
endelse

obj_destroy,o

return,output

end
