;+
; Project     : VSO
;
; Name        : SENDCMD
;
; Purpose     : Send command to an open socket
;
; Category    : sockets
;
; Inputs      : LUN = socket logical unit number
;               CMD = command string
;
; History     : 22-Nov-2015, Zarro (ADNET) - written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro sendcmd,lun,cmd,_ref_extra=extra

sendvar,lun,cmd,/command,_extra=extra

return & end
