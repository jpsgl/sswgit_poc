
pro file_callback,id,flun

common file_bank,files

if is_struct(files) then begin
 check=where(files.sent eq 0b,count)
 if count gt 0 then begin
  for i=0,count-1 do begin
   k=check[i]
   fname=files[k].fname
   fsize=files[k].fsize
   fid=files[k].fid
   mprint,'Sending '+fname
   openr,lun,fname,/get_lun
   buffer=bytarr(fsize,/nozero)
   readu,lun,buffer
   close_lun,lun
   bname=byte(file_basename(fname))
   bsize=n_elements(bname)
   writeu,flun,bsize
   writeu,flun,bname
   writeu,flun,fid
   writeu,flun,fsize
   writeu,flun, buffer, Transfer_Count = tc
   mprint,'Sent '+trim(tc)+' bytes'
   files[k].sent=1b
  endfor
 endif
endif
!null = Timer.Set(1., 'file_callback',flun)

return & end

;--------------------------------------------------------------------

pro file_results,id,null

common file_bank,files

if is_struct(files) then begin
 check=where((files.sent eq 1b) and (files.received eq 0b),count)
 if count gt 0 then begin
  for i=0,count-1 do begin
   k=check[i]
   fname=file_basename(files[k].fname)
   fid=files[k].fid
   outdir=concat_dir(get_temp_dir(),trim(fid))
   outfile=concat_dir(outdir,'prepped_'+fname)
   if file_test(outfile,/read) then begin
    mprint,'Prepped file returned at '+outfile
    files[k].received=1b
   endif
  endfor
 endif
endif
!null = timer.set(1.,'file_results')

return & end

;---------------------------------------------------------------------
Pro File_ListenerCallback, ID, ListenerLUN
Compile_Opt IDL2
Status = File_Poll_Input(ListenerLUN, Timeout = .1)
If (Status) then Begin
  mprint,'Connection request received.'
  Socket, ClientLUN, Accept = ListenerLUN, /Get_LUN, /RawIO, $
    Connect_Timeout = 30., Read_Timeout = 30., Write_Timeout = 30.,$
    /Swap_if_Big_Endian
  !null = Timer.Set(.01, 'file_callback', clientlun)
Endif Else Begin
  !null = Timer.Set(.1, 'File_ListenerCallback', ListenerLUN)
Endelse
End

;----------------------------------------------------------------
pro file_server
Compile_Opt IDL2
Port = Swap_Endian((UInt(Byte('IDLRocks'), 0, 2))[1],$
  /Swap_if_Big_Endian)
slun=sock_lun(port)
if slun eq -1 then begin
 Socket, ListenerLUN, Port, /Listen, /Get_LUN, $
  Read_Timeout = 60., Write_Timeout = 60., /RawIO
endif else begin
 mprint,'Server already running on port '+trim(port)+' with LUN '+trim(slun)
 ListenerLUN=slun
endelse

!null = Timer.Set (.1, 'File_ListenerCallback', ListenerLUN)
!null = timer.set(1.,'file_results')
slun_server=sock_lun(port,/server)
if slun_server gt -1 then  !null = Timer.Set(.01, 'file_callback', slun_server)

return & end

