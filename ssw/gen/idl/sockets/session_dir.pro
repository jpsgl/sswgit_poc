;+
; Project     : SOHO - CDS
;
; Name        : TEMP_DIR
;
; Purpose     : Create a temporary session directory
;
; Category    : Utility
;
; Syntax      : IDL> sub_dir=session_dir()
;
; Inputs      : None
;
; Outputs     : SESSION_DIR = sub directory with unique name in temp directory
;
; Keywords    : ERR = error string
;               NEW = set to create new subdir [def = return last]
;
; History     : 3-Aug-2017, Zarro (ADNET) - Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function session_dir,_ref_extra=extra,new=new

common session_dir,last_sdir

recycle=~keyword_set(new)

if recycle then begin
 if is_dir(last_sdir) then return,last_sdir
endif else last_sdir=''

mk_temp_dir,get_temp_dir(),sdir,_extra=extra

if recycle then last_sdir=sdir

return,sdir
end

