;+
; Project     : SXI
;
; Name        : SXI_COPY
;
; Purpose     : copy SXI file via HTTP sockets
;
; Category    : utility system sockets
;
; Syntax      : IDL> sxi_copy,file,outdir=outdir
;                   
; Inputs      : FILE = remote file name to copy with URL path (optional)
;
; Outputs     : None
;
; Keywords    : OUT_DIR = output directory to copy file
;               ERR   = string error message
;
; Example     :
;              First list remote files from NGDC server using 'sxi_files'  
;
;              IDL> f=sxi_files('1-dec-01','2-dec-01',/ngdc,/full)
;              IDL> sxi_copy,f,/progress
;              
;              Or copy without including path name:
;
;              IDL> sxi_copy,'SXI_20011201_000202041_BB_12.FTS'
;
; History     : 15-Jan-2003,  D.M. Zarro (EER/GSFC)  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro sxi_copy,url_file,err=err,_ref_extra=extra

err=''
if is_blank(url_file) then begin
 err='blank URL filename entered'
 message,err,/cont
 return
endif

;-- create HTTP object

http=obj_new('http',err=err)
if err ne '' then return

;-- check for network connection and if SXI server is up

server=sxi_server(network=network,path=path,err=err)
if not network then return

;-- open a socket to server (using server gateway)

http->open,server,/gateway

;-- download files
;-- if path not included then construct it

dfile=url_file
tpath=file_break(dfile,/path)
no_path=where(tpath eq '',count)
if count gt 0 then begin
 temp=parse_time(url_file[no_path],ymd=ymd)
 dfile[no_path]=path+'/'+ymd+'/'+dfile[no_path]
endif

for i=0,n_elements(url_file)-1 do begin
 http->copy,dfile[i],_extra=extra,err=err,cancelled=cancelled
 if cancelled then break
endfor

;-- cleanup

obj_destroy,http

return
end


