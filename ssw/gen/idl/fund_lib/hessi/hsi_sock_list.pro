;+
; Project     : HESSI
;
; Name        : HSI_SOCK_LIST
;
; Purpose     : List files on nearest HESSI HTTP data server
;
; Category    : Utility
;
; Syntax      : IDL> files=hsi_sock_list(tstart,tend,server=server)
;
; Inputs      : TSTART, TEND = start/end times to search [inclusive]
;
; Outputs     : FILES = files found, with full path
;
; Keywords    : SERVER = server where files located
;
; History     : Written 21 March 2002, D. Zarro (L-3Com/GSFC)
;
; Contact     : dzarro@solar.stanford.edu
;-

function hsi_sock_list,tstart,tend,server=server,count=count,err=err

count=0
err=''

;-- ping nearest server

server=hsi_sock_server(path=path,err=err,/verbose)
if err ne '' then return,''

;-- remote directories to search

fid=get_fid(tstart,tend,/full,delim='/',dstart=dstart,dend=dend,/no_next)

;-- list via sockets

for i=0,n_elements(fid)-1 do begin
 rfiles=sock_find(server,'*.fits',path=path+'/'+fid[i],count=rcount)
 if rcount gt 0 then files=append_arr(files,rfiles)
endfor

count=n_elements(files)
if count eq 0 then return,''

if count gt 0 then begin
 times=hsi_times(files,/tai)
 ok=where(  (times le dend) and (times ge dstart), count)
 if count gt 0 then files=files[ok]
endif

return,comdim2(files) & end


