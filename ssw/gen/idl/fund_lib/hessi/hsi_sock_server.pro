;+
; Project     : HESSI
;
; Name        : HSI_SOCK_SERVER
;
; Purpose     : return host name of nearest HESSI data server
;
; Category    : sockets
;           
; Outputs     : SERVER = server name
;
; Keywords    : PATH = path to data directories
;
; History     : Written, 22-Mar-2002,  D.M. Zarro (L-3Com/GSFC)
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-


function hsi_sock_server,path=path,err=err,verbose=verbose,force=force

common hsi_sock_server,last_server,last_path
err=''

if keyword_set(force) then delvarx,last_server,last_path

if is_string(last_server) and is_string(last_path) then begin
 path=last_path
 return,last_server
endif

path='/hessidata'
server='hesperia.gsfc.nasa.gov'
if not allow_sockets() then return,server
if not have_network(server,force=force) then return,server

sock_ping,server,err=err

if err eq '' then begin
 last_server=server
 last_path=path
endif

return,server

end
