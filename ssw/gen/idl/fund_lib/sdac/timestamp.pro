;+
;
; NAME: TIMESTAMP
;
;
; PURPOSE: Place the current time outside the upper right-hand corner
;	of the plot window
;
;
; CATEGORY: Util,Gen, Graphics
;
;
; CALLING SEQUENCE: TIMESTAMP
;
;
; CALLED BY:
;
;
; CALLS:
;	FCOLOR
;
; INPUTS:
;       none 
;
; OPTIONAL INPUTS:
;	XYOUTS KEYWORDS-
;	CHARSIZE
;	CHARTHICK
;	COLOR
;
; OUTPUTS:
;       none 
;
; OPTIONAL OUTPUTS:
;	none
;
; COMMON BLOCKS:
;	none
;
; SIDE EFFECTS:
;	none
;
; RESTRICTIONS:
;	none
;
; PROCEDURE:
;	none
;
; MODIFICATION HISTORY:
;	ras, 24-jan-96
;	ras, version 2, 13-dec-1996, added keywords charsize, charthick, and color
;	ras, version 3, 24-dec-1996, fixed bug in charsize
;
;-


pro TIMESTAMP, dummy, charsize=charsize, charthick=charthick, color=color

xw = !x.window

xyouts,/norm, xw(1)+.01*(xw(1)-xw(0)), $
	!y.window(1), strmid(!stime,0,17), orientation=-90,$
	charsize=fcheck(charsize,1), charthick=fcheck(charthick,1), color=fcolor(color)


end
