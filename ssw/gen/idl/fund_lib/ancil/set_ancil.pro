;+
; Project     : HESSI     
;                   
; Name        : SET_ANCIL
;               
; Purpose     : Set ZDBASE env to locate ancillary DB
;               
; Category    : HESSI GBO Database
;               
; Syntax      : IDL> set_ancil,direct
;
; Example     : 
;    
; Inputs      : 
;               
; Opt. Inputs : DIRECT = string directory name where ancil.db files are 
;               located. Default = $SYNOP_DATA/ancil_db.
;               
; Outputs     : None
;
; Opt. Outputs: None
;               
; Keywords    : ERR = blank string if no errors
;               ORIGINAL = set to original ZDBASE
;               FIRST = set to make added directory first in ZDBASE
;               LAST  = set to make added directory last in ZDBASE
;
; Restrictions: None
;               
; Side effects: None.
;               
; History     : Version 1,  15-May-1999,  D M Zarro (SM&A/GSFC)  Written
;     
; Contact     : dzarro@solar.stanford.edu
;-

pro set_ancil,direct,err=err,original=original,first=first,last=last,$
             show=show

common set_ancil,orig_zdbase
err=''
zdbase=trim(strlowcase(chklog('ZDBASE')))

have_orig=datatype(orig_zdbase) eq 'STR'
if not have_orig and (zdbase ne '') then orig_zdbase=zdbase
if keyword_set(original) and have_orig then mklog,'ZDBASE',orig_zdbase

have_input=datatype(direct) eq 'STR'
if keyword_set(show) and (not have_input) then begin
 message,chklog('ZDBASE'),/cont
 return
endif

;-- default to $SYNOP_DATA/ancil_db

synop_dir=concat_dir(chklog('SYNOP_DATA'),'ancil_db')
if datatype(direct) eq 'STR' then begin
 ancil_dir=direct
 tmp=chklog(direct)
 if trim(tmp) ne '' then ancil_dir=tmp
endif else ancil_dir=synop_dir

if not chk_dir(ancil_dir) then begin
 err='Invalid directory - '+ancil_dir
 message,err,/cont
 return
endif

;-- check if already installed in ZDBASE

have_ancil=str_patt(ancil_dir,zdbase) gt -1

if not have_ancil then begin
 if zdbase eq '' then zdbase=ancil_dir else zdbase=ancil_dir+':'+zdbase
endif

;-- move to front or back of line

if keyword_set(first) or keyword_set(last) then begin
 zarray=str2arr(zdbase,delim=':')
 with_ancil=grep(ancil_dir,zarray,/exact,index=with_index)
 without_ancil=grep(ancil_dir,zarray,/exact,index=without_index,/exclude)
 if min(without_index) gt -1 then z1=zarray(without_index)
 if keyword_set(first) then zarray=[ancil_dir,z1] else $
  zarray=[z1,ancil_dir]
 zdbase=arr2str(zarray,delim=':')
endif

mklog,'ZDBASE',zdbase
if keyword_set(show) then message,chklog('ZDBASE'),/cont

return & end

