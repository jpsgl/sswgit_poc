;+
; Project     : HESSI
;
; Name        : DEF_ANCIL
;
; Purpose     : define structure for ancillary data
;
; Category    : HESSI GBO analysis
;
; Explanation :
;
; Syntax      : IDL> def_ancil,ancil
;
; Inputs      : None
;
; Opt. Inputs : None
;
; Outputs     : ANCIL = ancillary data structure
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Version 1,  7-May-1996,  D.M. Zarro (SM&A/GSFC) Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro def_ancil,ancil      

ancil=  {cat_num:-1l,event_num:-1l, filename:'', date_obs:0d, date_end:0d, $
         xcen:0., ycen:0., xsize: 0., $
         ysize:0., roll:0.,source:'',class:-1, type:-1,date_mod:0d}

return & end


