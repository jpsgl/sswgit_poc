;+
; Project     : HESSI     
;                   
; Name        : ANCIL_SITE
;               
; Purpose     : convert ANCIL site name to IAU abbrev or vice-versa
;               
; Category    : HESSI, ANCIL
;               
; Syntax      : IDL> iau=ancil_site(name)
;
; Example     : print,ancil_site('Big Bear') --> 'bbso'
;    
; Inputs      : Observatory name
;               
; Opt. Inputs : None
;               
; Outputs     : Abbreviated IAU name
;
; Opt. Outputs: None
;               
; Keywords    : COUNT = number of matches (could be > 1 if ambiguous input)
;               /ABBR = signifies that input is abbreviated and output
;                       site name is requested.
;               /HELP = set to list current stored sites
;
; Restrictions: List of sites and abbreviations is limited but growing
;               
; Side effects: None.
;               
; History     : Version 1,  12-May-1999,  D M Zarro (SM&A/GSFC)  Written
;     
; Contact     : dzarro@solar.stanford.edu
;-

function ancil_site,name,abbr=abbr,count=count,help=help

on_error,1
common ancil_site,sabbr,sname

count=0
help=keyword_set(help)

if (datatype(name) ne 'STR') and (not help) then return,''
if trim(name) eq '' then return,''

;-- list of known abbreviations and locations (save in common for speed)

if (not exist(sabbr)) or (not exist(sname)) then begin

 sites = ['kbou','Space Environment Lab',$
         'khmn', 'Holloman AFB', $
         'htpr', 'Haute-Provence',$
         'lear', 'Learmonth Solar Observatory',$
         'meud', 'Observatory of Paris at Meudon',$
         'mitk', 'Mitaka',                         $
         'mlso', 'Mauna Loa Solar Obs. at HAO',    $
         'nobe', 'Nobeyama Radio Observatory',     $
         'ondr', 'Ondrejov',                       $
         'pdmo', 'Pic du Midi Observatory',        $
         'ksac', 'Nat. Solar Obs. at Sac. Peak',   $
         'bbso', 'Big Bear Solar Observatory',     $
         'kpno', 'Nat. Solar Obs. at Kitt Peak',   $
         'mees', 'Mees Solar Observatory',         $
         'mwno', 'Mt. Wilson Observatory',         $
         'kisf', 'Kiepenheuer Inst. for Solar Phys.', $
         'kanz', 'Kanzelhöhe Solar Observatory',      $
         'yohk', 'Yohkoh Soft-X Telescope',           $
         'nanc', 'Nancay Radioheliograph',            $
         'stra', 'Transition Region and Coronal Explorer (TRACE)', $
         'seit', 'SOHO EIT',$
         'smdi', 'SOHO MDI',$
         'hxi' , 'Yohkoh HXT',$
         'sxt' , 'Yohkoh SXT',$
         'goes', 'GOES',$
         'xxxx', 'Source unknown']

 nsites=n_elements(sites)
 even=where((indgen(nsites) mod 2) eq 0)
 odd=where((indgen(nsites) mod 2) ne 0)
 sabbr=sites(even)
 sname=sites(odd)
endif

if help then begin
 print,'  '
 for i=0,n_elements(sname)-1 do print,num2str(i)+') '+strpad(sabbr(i),5)+' -> '+sname(i)
 print,'  '
 return,''
endif

tname=strupcase(trim(name))
if keyword_set(abbr) then begin
 tpos=where(strpos(strupcase(sname),tname) gt -1,count)
 if count gt 0 then site=sabbr(tpos) else site='xxxx'
endif else begin
 tpos=where(strpos(strupcase(sabbr),tname) gt -1,count)
 if count gt 0 then site=sname(tpos) else site='Unknown'
endelse

if n_elements(site) eq 1 then site=site(0)
return,site

end



