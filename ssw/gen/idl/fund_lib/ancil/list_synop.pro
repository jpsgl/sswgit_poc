;+
; Project     : HESSI
;
; Name        : LIST_SYNOP
;
; Purpose     : list SYNOP files based on site name
;
; Category    : HESSI, GBO, utility
;
; Explanation : 
;
; Syntax      : IDL> files=list_synop(site,tstart,tend)
;
; Inputs      : SITE = site name to search (e.g. bbso, kpno, eit)
;               TSTART = search start time
;               TEND   = search end time
;
; Opt. Inputs : None
;
; Outputs     : FILES = found files (rounded to nearest day)
;
; Opt. Outputs: None
;
; Keywords    : EXT = extension to search for (def = '.fts')
;               ROOT = root directory name to search (def = '$SYNOP_DATA')
;               COUNT = # of files found
;
; Common      : None
;
; Restrictions: Unix systems only.
;               Assumes files are stored in subdirs encoded by site
;               abbreviation (e.g. bbso) in $SYNOP_DATA
;
; Side effects: None
;
; History     : Version 1,  14-May-1999,  D.M. Zarro (SM&A/GSFC),  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function list_synop,site,t1,t2,err=err,$
  root=root,count=count,ext=ext,verbose=verbose

count=0
err=''
if (datatype(site) ne 'STR') then begin
 pr_syntax,'files=list_synop(site,tstart [,tend])'
 return,''
endif

if strlowcase(ancil_site(site)) eq 'unknown' then begin
 err='warning, unrecognized site - '+site
 message,err,/cont
endif

get_utc,cur_utc
tstart=anytim2utc(t1,err=err)
if err ne '' then tstart=cur_utc
tend=anytim2utc(t2,err=err)
if err ne '' then tend=tstart

;-- set defaults

site=trim(site)
verbose=keyword_set(verbose)
if not data_chk(ext,/string) then ext='fts'
site=trim(site)

;-- check user provided directory, followed by $SYNOP_DATA and $SUMMARY_DATA

start_dir=curdir()
if data_chk(root,/string) then start_dir=root
check_dir=[start_dir,chklog('SYNOP_DATA'),chklog('SUMMARY_DATA')]
for i=0,n_elements(check_dir)-1 do begin         
 sub_dir=concat_dir(check_dir(i),site)
 if chk_dir(sub_dir) then goto,search
endfor

err='could not locate site directory'
message,err,/cont
return,''

search:

dstart=tstart.mjd
dend=tend.mjd
last_temp=''
for i=dstart,dend do begin
 if i gt cur_utc.mjd then goto,done
 temp=date_code({mjd:long(i),time:0l})
 if temp ne last_temp then begin
  if verbose then message,'searching '+temp,/cont
  search_file='*_*'+temp+'_*.'+ext 
  v=loc_file(concat_dir(sub_dir,search_file),count=vcount,/no_recheck)
  if vcount gt 0 then files=append_arr(files,v)
 endif
 last_temp=temp
endfor

done: count=n_elements(files)


if count eq 0 then begin 
 err='No files found' & files='' 
 message,err,/cont
endif

return,files & end


