;+
; Project     :	HESSI
;
; Name        :	GET_ANCIL
;
; Purpose     :	Extracts an ancillary data record from ancillary database
;
; Explanation :	
;
; Syntax      :	GET_ANCIL,CAT_NUM,DEF

;
; Inputs      :	CAT_NUM = ancillary data catalog number
;
; Opt. Inputs :	None.
;
; Outputs     :	DEF	= Structure containing the ancillary data definition. 
;
; Opt. Outputs:	None.
;
; Keywords    : ERR    = error string (blank if no errors)
;               EVENT  = input CAT_NUM is really  EVENT number
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Category    :	HESSI GBO Analysis Databases.
;
; Prev. Hist. :	None.
;
; Written     :	Dominic Zarro, SM&A/GSFC, 8 May 1999
;
; Contact     : dzarro@solar.stanford.edu
;
;-

	pro get_ancil,cat_num,def, err=err,event=event,quiet=quiet

	on_error, 1
        err=''
;;
;  Check the number of parameters.
;
        if n_params() ne 2 then begin
         err = 'SYNTAX:  GET_ANCIL, CAT_NUM, DEF'
	 mesage,err,/cont & return
        endif
;
;  Check the input parameter CAT_NUM.
;
	type = datatype(cat_num,2)
	if type eq 0 then begin
         err = 'CAT_NUM field is undefined'
         message,err,/cont & return
	end else if type ge 4 then begin
         err = 'CAT_NUM field must be an integer'
         message,err,/cont & return
	end else if n_elements(cat_num) ne 1 then begin
         err = 'CAT_NUM field must be a scalar'
         message,err,/cont & return
	endif
;
;  Open the database.
;
	dbopen, 'ancil',unavail=unavail
        if unavail then begin
         err='ANCIL DB currently unavailable'
         close_ancil,err & return
        endif

;
;  Search on CAT_NUM or EVENT_NUM field.
;
        if keyword_set(event) then search='event_num' else search='cat_num'
	entries = dbfind(search+'='+strtrim(long(cat_num),2)+',deleted=n',/silent)

;
;  If no entries were found, then return immediately.
;
	if !err eq 0 then begin
	 err = 'Input CAT_NUM not found'
	 close_ancil,err & return
	endif else nentries=!err

;
;  Extract the relevant entry from the database.
;
	def_ancil,db_def
        db_def=replicate(db_def,nentries)
        tags=tag_names(db_def)
        ntags=n_elements(tags)
        tag_list=arr2str(tags(0:11),delim=',')
        for j=0,nentries-1 do begin
         s=execute('dbext, entries(j),"'+tag_list+'",'+tag_list)
         if s eq 0 then begin
          err = 'Failed Database read'
          close_ancil,err & return
         endif
         tag_list=arr2str(tags(12:ntags-1),delim=',')
         s=execute('dbext, entries(j),"'+tag_list+'",'+tag_list)

;
;  Copy ancil fields into structure definition
;

         for i=0,ntags-1 do s=execute('db_def(j).('+num2str(i)+')='+tags(i))
        endfor

        def=copy_var(db_def)
        dbclose
	return
	end
