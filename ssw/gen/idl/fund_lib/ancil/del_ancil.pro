;+
; Project     :	HESSI
;
; Name        :	DEL_ANCIL,CAT_NUM
;
; Purpose     :	Delete a record from ancillary database
;
; Explanation :	
;
; Syntax      : DEL_ANCIL, DEF, STATUS
;
; Inputs      :	CAT_NUM = catalog number to delete
;
; Opt. Inputs :	None.
;
; Outputs     :	STATUS = logical value representing
;		whether or not the operation was successful, where 1 is
;		successful and 0 is unsuccessful.
;
; Opt. Outputs:	None.
;
; Keywords    : ERR    = error string (blank if no errors).
;
; Restrictions:	!PRIV must be 2 or greater to use this routine.
;
; Side effects:	CAT_NUM entry is flagged as deleted. Use PURGE_ANCIL
;               to clear it permanently
;
; Category    :	HESSI GBO Analysis Database
;
; Written     :	Dominic Zarro, SM&A/GSFC, 8 May 1999
;
; Contact     : dzarro@solar.stanford.edu
;-

	pro del_ancil,cat_num,status, err=err

	on_error, 1
        err=''
	status = 0 
;
;  Check input CAT_NUM
;
        if not exist(cat_num) then begin
         pr_syntax,'del_ancil,cat_num'
         return
        endif

        if not is_number(cat_num) then begin   
         err='Input CAT_NUM must be integer'
         message,err,/cont
         return
        endif

        if cat_num lt 0 then begin
         err='Input CAT_NUM must be non-negative'
         message,err,/cont
         return
        endif
;
;  Ensure that the user has privilege to write into the database.
;
        !priv=3
	if !priv lt 2 then begin $
         err = '!PRIV must be 2 or greater to write into the Database'
         message,err,/cont & return
 	endif

        loc=getenv('ANCIL_DB')
        ancil_db=concat_dir(getenv('ANCIL_DB'),'ancil.dbf')
        chk=loc_file(ancil_db,count=count,err=err)
        if count eq 0 then begin
         message,err,/cont & return
        endif
        stat=test_open(ancil_db,/write)
        if not stat then begin
         err='Write access to Ancillary Database is denied'
         message,err,/cont & return
        endif
;
;  Open the database for write access.
;
	dbopen, 'ancil', 1
	n_entries = db_info('entries','ancil')
	if n_entries gt 0 then begin
         entries = dbfind('cat_num='+strtrim(long(cat_num),2)+',deleted=n',/silent)
         if !err ne 0 then begin
          for i=0,n_elements(entries)-1 do dbupdate, entries(i), 'deleted', 'y'
         endif else begin
          err='Could not delete entry '+num2str(cat_num)
          close_ancil,err & return
         endelse
        endif
	status = 1
	dbclose
	return & end

