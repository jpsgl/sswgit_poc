;+
; Project     : HESSI
;
; Name        : VALID_ANCIL
;
; Purpose     : validate ancillary DB record
;
; Category    : HESSI, GBO, utility
;
; Explanation : 
;
; Syntax      : IDL> valid=valid_ancil(record)
;
; Inputs      : RECORD = Ancillary DB record as defined in DEF_ANCIL
;
; Opt. Inputs : None
;
; Outputs     : 1/0 = valid/invalid
;
; Opt. Outputs: None
;
; Keywords    : ERR = error string
;
; Common      : None
;
; Restrictions: None
;                
; Side effects: None
;
; History     : Version 1,  17-May-1999,  D.M. Zarro (SM&A/GSFC),  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU


function valid_ancil,record,err=err

err='invalid ancillary record'
if datatype(record) ne 'STC' then return,0b

def_ancil,template
sample=clear_struct(record(0))
template=clear_struct(template)
if not match_struct(sample,template) then return,0b

err=''
return,1b
end
