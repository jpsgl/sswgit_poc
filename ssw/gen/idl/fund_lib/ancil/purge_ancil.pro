;+
; Project     :	HESSI
;                         
; Name        :	PURGE_ANCIL
;
; Purpose     :	Purges old and deleted ancillary records
;
; Explanation :	
;
; Use         :	PURGE_ANCIL,STATUS
;
; Inputs      :	None.
;
; Opt. Inputs :	None.
;
; Outputs     :	STATUS = logical value representing
;		whether or not the operation was successful, where 1 is
;		successful and 0 is unsuccessful.
;
; Opt. Outputs:	None.
;
; Keywords    : ERR    = error string (blank if no errors)
;;
; Restrictions:	None.
;
; Side effects:	None.
;
; Category    :	HESSI Analysis Databases.
;
; Prev. Hist. :	None.
;
; Written     :	Dominic Zarro (SM&A/GSFC) 10 May 1999
;
; Contact     : dzarro@solar.stanford.edu
;-

        pro purge_ancil,status,err=err

	on_error, 1
        err='' & status=0

;-- Check for write access

@check_ancil


;-- Open the database for update, and search for entries which are marked for
;   deletion.  If any are found, then delete them.

	dbopen, 'ancil', 1,unavail=unavail

        if unavail then begin
         err='ANCIL DB currently unavailable'
         message,err,/cont
         return
        endif

	entries = dbfind('deleted=y', /silent)
	if !err gt 0 then dbdelete, entries
        status=1
	dbclose

	return & end
