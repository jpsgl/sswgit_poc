;+
; Project     :	HESSI
;
; Name        :	ADD_ANCIL()
;
; Purpose     :	Adds a record to ancillary database
;
; Explanation :	
;
; Syntax      : ADD_ANCIL, DEF, STATUS
;
; Inputs      :	DEF = This is an anonymous structure containing
;		      tags defined by DEF_ANCIL
;
; Opt. Inputs :	None.
;
; Outputs     :	STATUS = logical value representing
;		whether or not the operation was successful, where 1 is
;		successful and 0 is unsuccessful.
;
; Opt. Outputs:	None.
;
; Keywords    : ERR    = error string (blank if no errors).
;               UPDATE = set to update a record if it already exists
;
; Restrictions:	None.
;
; Side effects:	If input ANCILLARY catalog number is already in DB, then input
;               DEF will replace current entry, otherwise it
;               will be added with a new catalog number.
;
; Category    :	HESSI GBO Analysis Database
;
; Written     :	Dominic Zarro, SM&A/GSFC, 8 May 1999
;
; Contact     : dzarro@solar.stanford.edu
;-

	pro add_ancil, def,status, err=err,update=update

	on_error, 1
        err=''
	status = 0 

;
;  Check the input parameters
;
        if datatype(def) ne 'STC' then begin
         err= 'SYNTAX:  STATUS = ADD_ANCIL(DEF)'
         message,err,/cont & return
        endif

@check_ancil

;
; Check if identical entry already in the database
; If input catalog number gt 0 and different entry found in DB then replace it
;
        replacing=0
        if def.cat_num ge 0 then begin
         get_ancil,def.cat_num,db_def,err=err,/quiet
         if err eq '' then begin
          if match_struct(def,db_def,exclude='date_mod') then begin
           err = 'Identical Ancillary entry already in Database'
           close_ancil,err & return
          endif
          if keyword_set(update) then replacing=1 else begin
           message,'use /UPDATE to update existing entry',/cont
           close_ancil & return
          endelse
         endif
        endif

; Check each of the structure components 

        tdef=def
        tdef.filename=strmid(strtrim(def.filename,2),0,80)
        if tdef.filename eq '' then begin
         err='FILENAME field is missing'
         close_ancil,err & return
        endif

; If DATE_OBS is not provided, then try figuring it out from the filename
 
        if (def.date_obs le 0.) then begin
         date_obs=fid2time(tdef.filename,/tai,err=err)
         if err ne '' then begin
          err='DATE_OBS field (TAI format) is missing'
          close_ancil,err & return
         endif
        endif else date_obs=tdef.date_obs
        tdef.date_obs=double(str_format(date_obs))
        tdef.date_end=double(str_format(date_end))

; If DATA source not provided, then try figuring it out from the filename

        if (trim(def.source) eq '') then tdef.source=ancil_site(tdef.filename)
        tdef.source=strmid(strtrim(tdef.source,2),0,20)

; Optional pointer to HESSI event database 

        tdef.event_num = long(tdef.event_num)

;
;  Open the database for write access.
;
	dbopen, 'ancil', 1
;
;  Find the largest catalog number currently in the database, and add one
;  to it or, if replacing, delete the old entry and give old number to new entry
;
	n_entries = db_info('entries','ancil')
	if n_entries eq 0 then cat_num = 0L else begin
         if replacing then begin
          cat_num=tdef.cat_num 
          entries = dbfind('cat_num='+strtrim(long(cat_num),2)+',deleted=n',/silent)
          if !err ne 0 then begin
           for i=0,n_elements(entries)-1 do dbupdate, entries(i), 'deleted', 'y'
          endif else begin
           err='Could not replace old entry'
           close_ancil,err & return
          endelse
         endif else begin
          dbext, -1, 'cat_num', cat_nums
	  cat_num = max(cat_nums) + 1L
         endelse
	endelse
;
;  Add the record to the database.
;

        date_mod=double(str_format(anytim2tai(!stime)))
	dbbuild,cat_num,tdef.filename,tdef.event_num,tdef.date_obs,tdef.date_end,$
         tdef.xcen,tdef.ycen,tdef.roll,$
         tdef.xsize,tdef.ysize,tdef.source,$
         tdef.class,tdef.type,'n',date_mod,$
         status=status
	if status eq 0 then begin
         err = 'Write to Ancillary database was unsuccessful'
         close_ancil,err & return
	endif
;
;  Update the catalog number in the structure and signal success.
;
	def.cat_num = cat_num
	status = 1
	dbclose

	return & end

