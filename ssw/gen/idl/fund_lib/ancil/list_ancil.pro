;+
; Project     :	HESSI
;
; Name        :	LIST_ANCIL
;
; Purpose     :	List ancillary database records
;
; Explanation :	
;
; Use         :	OBS=LIST_ANCIL(START, END, COUNT=COUNT  [, /DATE_MOD ]
;		OBS=LIST_ANCIL(CAT_NUM, OBS, COUNT=COUNT [, /EVENT_NUM]
;
; Inputs      :	START   = Start time for DB search
;               or
;		CAT_NUM = Catalog number to use in search
;
; Opt. Inputs :	END     = End time for search [def = end of current dat]
;
; Outputs     :	OBS	= structure array containing returned records
;;
; Opt. Outputs:	None.
;
; Keywords    :	DATE_MOD = If set, then the search is done on the modification
;			   date rather than the start and stop dates.
;
;		SEARCH	 = Allows one to pass in an optional search string, as
;			   used by DBFIND.  For example, if one wants to search
;			   for all image records:
;
;				SEARCH='CLASS=0'
;
;			   Multiple search criteria can be passed as a single
;			   (case-insensitive) string separated by commas, e.g.
;
;				SEARCH='CLASS=0,TYPE=1'
;
;		ERR   = error string (blank if no errors)
;
;               EVENT_NUM = associate CAT_NUM with EVENT_NUM
;
;               COUNT  = # or records found
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Category    :	HESSI GBO Database.
;
; Written     :	Zarro (SM&A/GSFC) 10 May 1998
;
; Contact     :	dzarro@solar.stanford.edu
;-
                      
	function list_ancil,p1, p2, err=err, date_mod=mod_date, $
		search=search,event_num=event_num,count=count
                      
        err=''
	count = 0
        
;  Check input parameters.

        tstart=anytim2tai(p1,err=err)
        time_entered=err eq ''
        if time_entered then begin
         tend=anytim2tai(p2,err=err)
         if err ne '' then tend=tstart
         if str_format(tstart) eq str_format(tend) then begin
          tend=anytim2utc(tstart)
          tend.mjd=tend.mjd+1l
          tend.time=0l
          tend=anytim2tai(tend)
         endif
        endif
        
        num_entered=0
        if not time_entered then begin
         if exist(p1) then begin
          chk_num=where(is_number(p1),ncount)
          num_entered=ncount eq n_elements(p1)
         endif
        endif

        if (not time_entered) and (not num_entered) then begin
         pr_syntax,'obs=list_ancil(start, end,count=count)' +     $
                    'or obs=list_ancil(cat_num,count=count)'
         err='Invalid inputs'
         return,-1
        endif

	if n_elements(p1) ne 1 then begin
         if num_entered then err = 'CAT_NUM must be a scalar' else $
          err='TSTART must be a scalar'
         message,err,/cont & return,-1
        endif

        if time_entered then begin
         if n_elements(p2) gt 1 then begin
          err='TEND must be a scalar'
          message,err,/cont
          return,-1
         endif
        endif
;
;  Open database
;
        dbopen,'ancil',unavail=unavail
        if unavail then begin
         err='Could not open ANCIL database'
         message,err,/cont
        endif
;
;  Setup search string
;

	if num_entered then begin
         if keyword_set(event_num) then key='event_num' else key='cat_num'
	 test = key+'=' + trim(p1)
	endif else begin
         if keyword_set(mod_date) then begin
	  test = 'date_mod>' + trim(tstart,'(f15.3)') + $
	   ',date_mod<' + trim(tend,'(f15.3)')
	 endif else begin
	  test = 'date_obs>' + trim(tstart,'(f15.3)') + $
	   ',date_obs<' + trim(tend,'(f15.3)')
         endelse
	endelse

	if n_elements(search) eq 1 then test = test + ',' + search
	entries = dbfind(test+',deleted=n', /silent, err=err)
	if err ne '' then begin
	 close_ancil,err & return,-1
        endif

; Entries found?

	if !err eq 0 then begin
	 count = 0
         message,'no matching records found',/cont
	 dbclose & return,-1
	end else begin
	 entries = entries(uniq(entries))
	 count = n_elements(entries)
	endelse

; Do the actual search
 
        def_ancil,db_def
        db_def=replicate(db_def,count)
        tags=tag_names(db_def)
        ntags=n_elements(tags)
        tag_list=arr2str(tags(0:11),delim=',')
	entries = dbsort(entries,'date_obs')
        s=execute('dbext, entries,"'+tag_list+'",'+tag_list)
        if s eq 0 then begin
         err = 'Failed Database read'
         close_ancil,err & return,-1
        endif
        tag_list=arr2str(tags(12:ntags-1),delim=',')
        s=execute('dbext, entries,"'+tag_list+'",'+tag_list)
        for i=0,ntags-1 do s=execute('db_def.('+num2str(i)+')='+tags(i))

;-- Cleanup

	dbclose

        return,db_def & end
