
;-- CHECK_ANCIL 
;
;   Include file for checkig existence and write access to ANCIL DB
;        

loc=getenv('ANCIL_DB')
ancil_db=concat_dir(getenv('ANCIL_DB'),'ancil.dbf')
chk=loc_file(ancil_db,count=count,err=err)
if count eq 0 then begin
 message,err,/cont & return
endif

stat=test_open(ancil_db,/write)
if not stat then begin
 err='Write access to Ancillary Database is denied'
 message,err,/cont & return
endif

