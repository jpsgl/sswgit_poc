function secchi_time2files, time0, time1, level=level, pb=pb, rt=rt,$
   pattern=pattern, euvi=euvi, cor1=cor1,cor2=cor2,hi1=hi1,hi2=hi2,$
   a=a,b=b, urls=urls, parent=parent, dtype=dtype, debug=debug
;
;+
;   Name: secchi_time2files
;
;   Purpose: return secchi files for user time/timerange 
;
;   Input Paramters:
;      time0 - time or start time of range
;      time1 - stop time of range
;
;   Keyword Parameters:
;      level - processing level - default= Zero aka 0
;      pb - switch - if set, playback data is considered
;      rt - switch - if set, real time data is considered
;      pattern - optional file pattern - default = *fts
;      euvi, cor1, cor2, hi1, hi2 - mutually exclusive instrument - def = euvi
;      a,b - probably obvious - default = /a
;      dtype - "type" of data returned, def='img' - tbd... 
;      parent - top level directory - if not supplied, use '$SSW_SECCHI_DATA'
;               
;
;   Calling Sequence:
;      IDL> secfiles=secchi_time2files(t0,t1 [,level=#] $
;                      [,/a] -or [,/b] [,/rt] -or- [,/pb] $
;                      [,/corN] -or- [,/hiN] -or- [,/euvi]
;
;   Calling Examples:
;      IDL> euvi=secchi_time2files('4-dec-2006','5-dec-2006',/pb) ; def=euvi/a - fits
;      IDL> cor1=secchi_time2files('4-dec-2006','5-dec-2006',/rt,/b,/cor1) 
;
;   History:
;      4-dec-2006 - S.L.Freeland - celebrate SECCHI first light
;                                  in the mold of xxx_time2files.pro suite 
;
;   Method:
;     set up call to 'ssw_time2filelist' based on user time+keywords
;     (which calls 'ssw_time2paths')
;
;  Restrictions:
;     dtype and pattern not yet explored; assume FITS images are desired for today 
;-
debug=keyword_set(debug)
eparent=get_logenv('SSW_SECCHI_DATA')
case 1 of 
   data_chk(parent,/string):  ; user supplied vi keyword
   file_exist(eparent): parent=eparent ; via environmental
   else: parent='/service/stereo4/data/secchi/' ; local gsfc 
endcase

if n_elements(level) eq 0 then level=0
slevel='L'+strtrim(level,2) ; processing level subdirectory hook

case 1 of   ; set telemetry type string/subdirectory
   keyword_set(rt): ttype='rt'
   keyword_set(pb): ttype='pb'
   else: ttype='pb' ; default is playback
endcase
 
sat=(['a','b'])(keyword_set(b))  ; satellite select, default='a'

if n_elements(dtype) eq 0 then dtype='img'
if n_elements(pattern) eq 0 then pattern='*.fts'

case 1 of 
   keyword_set(cor1): inst='cor1'
   keyword_set(cor2): inst='cor2'
   keyword_set(hi1): inst='hi1'
   keyword_set(hi2): inst='hi2'
   else: inst='euvi'
endcase

topparent=arr2str([parent,ttype,slevel,sat,dtype,inst],get_delim())
retval=ssw_time2filelist(time0,time1,pattern=pattern,parent=topparent,/flat)
if debug then stop,'retval,topparent'

return,retval
end



