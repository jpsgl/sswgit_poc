function ratio, nom, denom, blank=blank
; $Id: ratio.pro,v 1.1 2007/01/31 15:47:08 nathan Exp $
; 
; Project:	STEREO - SECCHI
;
; Form the ratio of 2 images/cubes
; Written by A. Vourlidas 02/05
;
;
   IF NOT keyword_set(blank) THEN blank = 0 ;default blanking value
   tmp = denom*0.
   ind = where((denom NE 0.) AND (abs(denom) GT blank))
   tmp(ind) = nom(ind)/denom(ind)
   return, tmp
END
