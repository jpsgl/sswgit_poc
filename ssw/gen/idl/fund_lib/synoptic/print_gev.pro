;+
; Project     : SOHO - CDS
;
; Name        : PRINT_GEV
;
; Purpose     : Wrapper around GET_GEV
;
; Category    : planning
;
; Explanation : Get GOES Event listing
;
; Syntax      : IDL>print_gev,tstart,tend
;
; Inputs      : TSTART = start time 
;
; Opt. Inputs : TEND = end time
;
; Outputs     : GOES event listing in HTML table format
;
; Opt. Outputs: None
;
; Keywords    : COUNT = # or entries found
;               ERR = error messages
;
; History     : Version 1,  20-June-1999,  D.M. Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro print_gev,tstart,tend,count=count,err=err

               
on_error,1
err=''
count=0

;-- call RD_GEV

gev=get_gev(tstart,tend,count=count,err=err,/quiet)

if err ne '' then begin
 print,err  & return
endif
                                                

times=replicate({time:0l,day:0},count)
day=gt_day(gev,/str) & tstart=gt_time(gev,/str) 
times.day=gev.day
times.time=gev.time
tpeak=times & tpeak.time=tpeak.time+gev.peak*1000
tstop=times & tstop.time=tstop.time+gev.duration*1000
tpeak=gt_time(tpeak,/str)
tstop=gt_time(tstop,/str)
class=trim(string(gev.st$class))
halpha=trim(string(gev.st$halpha))
noaa=trim(string(gev.noaa))
none=where(noaa eq '0',acount)
if acount gt 0 then noaa(none)='&nbsp'
location=gev.location 
ns=location(1,*) & ew=location(0,*) 
none=where( (abs(ns) gt 970) or (abs(ew) gt 970),zcount)
south=where(ns lt 0,scount)
north=where(ns ge 0,ncount)
east=where(ew lt 0,ecount)
west=where(ew ge 0,wcount)
ns=string(abs(ns),'(i2.2)')
ew=string(abs(ew),'(i2.2)')
if scount gt 0 then ns(south)='S'+ns(south)
if ncount gt 0 then ns(north)='N'+ns(north)
if ecount gt 0 then ew(east)='E'+ew(east)
if wcount gt 0 then ew(west)='W'+ew(west)
loc=ns+ew
if zcount gt 0 then loc(none)='&nbsp'

check=replicate('<input type=checkbox>',count)
head='<center><form name="GOES"> <b>GOES Events (times in UT)</b> <br><br> <table border=1 cellspacing=0 cellwidth=2>'
thead='<tr bgcolor="lightblue">
th='<th align=center>'
eth='</th>
heads=['Date','Start','Peak','End','Class','Location','NOAA AR #','&nbsp']
for i=0,n_elements(heads)-1 do thead=thead+th+heads(i)+eth

tr=replicate('<tr>',count)
td=replicate('<td align=center>',count)

tbody=tr+td+day+td+tstart+td+tpeak+td+tstop+td+class+td+loc+td+noaa+td+check

table=[head,thead,tbody,'</table></form></center>']


print,table

return & end


