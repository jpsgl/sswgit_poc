;+
; Project     : HESSI
;
; Name        : LIST_SYNOP
;
; Purpose     : Search for SYNOPTIC FITS files
;
; Category    : HESSI, GBO, utility
;
; Explanation : 
;
; Syntax      : IDL> list_synop,date,files
;
; Inputs      : DATE = date to search (def = current)
;
; Opt. Inputs : None
;
; Outputs     : FILES = files found
;
; Opt. Outputs: None
;
; Keywords    : COUNT = # of files found
;               TOP_DIR = top directory to search (try $SYNOP_DATA, then current dir)
;               SUB_DIR = subdirectory to search (usually site names like 'bbso')
;
; Restrictions: Requires $SYNOP_DATA directories to be NSF mounted
;                
; Side effects: None
;
; History     : Version 1,  17-June-1999,  D.M. Zarro (SM&A/GSFC),  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

                                  
pro list_synop,date,files,verbose=verbose,count=count,top_dir=top_dir,sub_dir=sub_dir

verbose=keyword_set(verbose)

;-- def to current day

err='' & date_entered=1b
time=anytim2utc(date,err=err)
if err ne '' then begin
 get_utc,time
 date_entered=0b
endif
time.time=0

if (not date_entered) and verbose then $
 message,'defaulting to '+anytim2utc(time,/vms),/cont

;-- search directories

if not is_dir(top_dir) then begin 
 top_dir=chklog('SYNOP_DATA')
 if top_dir eq '' then begin
  err='$SYNOP_DATA directory environment not defined, using current'
  message,err,/cont
  top_dir=curdir()
 endif
endif

dirs=['sxt','trace','kanz','mwso','nobe','pdmo','kpno','meud','lear','eit']
if not is_dir(sub_dir) then sub_dir=dirs
dirs=concat_dir(top_dir,sub_dir)

delvarx,files
count=0
pattern=date_code(time)
for i=0,n_elements(dirs)-1 do begin
 if is_dir(dirs(i)) then begin
  if verbose then message,'searching "'+dirs(i)+'"',/cont
  sfiles=loc_file('*'+pattern+'*.*f*ts*',path=dirs(i),count=scount,/no_recheck)
  if scount gt 0 then files=append_arr(files,sfiles)
 endif
endfor

count=n_elements(files)
if count eq 0 then files=''
if not date_entered and (count gt 0) then date=files

return & end
  
  
