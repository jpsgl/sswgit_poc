;+
; Project     : HESSI
;
; Name        : LIST_MEUD
;
; Purpose     : List MEUDON H-alpha files 
;
; Category    : GBO ancillary
;
; Explanation : List H-alpha files from ftp://mesola.obspm.fr/pub/meudon/Halpha/
;               Typical remote filenames are mh991101.083700.fits.Z
;
; Syntax      : list_meud,tstart,files=files
;
; Inputs      : TSTART = start date to retrieve 
;
; Opt. Inputs : TEND = end date to retrieve [def = end of current day]
;
; Keywords    : 
;               FILES = files found
;               COUNT = no of files listed
;               CACHE = check last cached results before ftp
;
; History     : 14 Nov 1999 D. Zarro (SM&A/GSFC) - written
;
; Contact     : dzarro@solar.stanford.edu
;-

pro list_meud,tstart,tend,files=files,count=count,day=day,$
              cache=cache          
                                      
files='' & count=0
verbose=1-keyword_set(quiet)
                                      
;-- construct start/end month directories

dstart=get_def_times(tstart,tend,dend=dend,/ext,day=day)

;-- check cache

if keyword_set(cache) then begin
 cache_ancil,'meud',dstart,dend,files=files,count=count
 if count gt 0 then return
endif

form='(i2.2)'
syear=strmid(trim(dstart.year),2,2)
sdir=str_format(syear,form)+str_format(dstart.month,form)
eyear=strmid(trim(dend.year),2,2)
edir=str_format(eyear,form)+str_format(dend.month,form)

jdate=dstart
i=0
while ((where(edir eq sdir))(0) eq -1) do begin
 i=i+1
 jdate.month=dstart.month+i
 jdate=anytim(jdate,/utc_ext)
 year=strmid(trim(jdate.year),2,2)
 dir=str_format(year,form)+str_format(jdate.month,form)
 sdir=append_arr(sdir,dir)
endwhile

;-- do remote listing

server='mesola.obspm.fr'
remote_dir='/pub/meudon/Halpha/'
rdir=remote_dir+sdir
ftp=obj_new('ftp',server)
ftp->rdir,rdir
ftp->ls,ofiles
obj_destroy,ftp

;-- convert filenames to times and filter out desired times

ocount=n_elements(ofiles)
if ocount gt 0 then begin
 otimes=mfid2time(ofiles,err=err)
 if err eq '' then begin
  t1=anytim2tai(dstart)
  t2=anytim2tai(dend)
  keep=where( (otimes le t2) and (otimes ge t1),count)
  if count gt 0 then begin
   files=ofiles(keep)
   ftimes=otimes(keep)
  endif
 endif
endif

;-- cache listing

if count gt 0 then cache_ancil,'meud',tstart,tend,files=files,times=ftimes,/set

return & end
