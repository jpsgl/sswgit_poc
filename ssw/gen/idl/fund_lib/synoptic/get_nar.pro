;+
; Project     : SOHO - CDS
;
; Name        : GET_NAR
;
; Purpose     : Wrapper around RD_NAR
;
; Category    : planning
;
; Explanation : Get NOAA AR pointing from $DIR_GEN_NAR files
;
; Syntax      : IDL>nar=get_nar(tstart)
;
; Inputs      : TSTART = start time 
;
; Opt. Inputs : TEND = end time
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : COUNT = # or entries found
;               ERR = error messages
;               QUIET = turn off messages
;               NO_HELIO = don't do heliographic conversion
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Version 1,  20-June-1998,  D.M. Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function get_nar,tstart,tend,count=count,err=err,quiet=quiet,$
                 no_helio=no_helio,nearest=nearest

on_error,1
err=''
delvarx,nar
count=0

;-- start with error checks

if not have_proc('rd_nar') then begin
 err='cannot find RD_NAR in IDL !path'
 message,err,/cont
 return,''
endif

if trim(getenv('DIR_GEN_NAR')) eq '' then begin
 err='DIR_GEN_NAR not defined'
 message,err,/cont
 return,''
end

err=''
t1=anytim2utc(tstart,err=err)
if err ne '' then get_utc,t1
t1.time=0

t2=anytim2utc(tend,err=err)
if err ne '' then begin
 t2=t1
 t2.mjd=t2.mjd+1
endif

t2.mjd=t2.mjd+1
err=''

loud=1-keyword_set(quiet)
if anytim2tai(t2) le anytim2tai(t1) then begin
 err='Start time must be before End time'
 if loud then message,err,/cont
 return,''
endif

;-- call RD_NAR

if loud then begin
 message,'retrieving NAR data for '+ anytim2utc(t1,/vms),/cont
endif

rd_nar,anytim2utc(t1,/vms),anytim2utc(t2,/vms),nar,nearest=nearest

if datatype(nar) ne 'STC' then begin
 err='NOAA data not found for specified times'
 return,''
endif
                  
;-- determine unique AR pointings

count=n_elements(nar)

if (1-keyword_set(no_helio)) then begin
 sorder = uniq([nar.noaa], sort([nar.noaa]))
 nar=nar(sorder)
 count=n_elements(nar)
 for i=0,count-1 do begin
  temp=nar(i)
  helio=temp.location
  xy=hel2arcmin(helio(1),helio(0),soho=0,date=anytim(temp,/utc_int))*60.
  temp=add_tag(temp,xy(0),'x')
  temp=add_tag(temp,xy(1),'y',index='x')
  new_nar=merge_struct(new_nar,temp) 
 endfor
 return,new_nar
endif else return,nar

end


