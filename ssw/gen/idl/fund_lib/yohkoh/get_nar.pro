;+
; Project     : SOHO - CDS
;
; Name        : GET_NAR
;
; Purpose     : Wrapper around RD_NAR
;
; Category    : planning
;
; Explanation : Get NOAA AR pointing from $DIR_GEN_NAR files
;
; Syntax      : IDL>nar=get_nar(tstart)
;
; Inputs      : TSTART = start time 
;
; Opt. Inputs : TEND = end time
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : COUNT = # or entries found
;               ERR = error messages
;               QUIET = turn off messages
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Version 1,  20-June-1998,  D.M. Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function get_nar,tstart,tend,count=count,err=err,quiet=quiet

on_error,1
err=''
delvarx,nar
count=0

;-- start with error checks

if not have_proc('rd_nar') then begin
 err='cannot find RD_NAR in IDL !path'
 message,err,/cont
 return,''
endif

if trim(getenv('DIR_GEN_NAR')) eq '' then begin
 err='DIR_GEN_NAR not defined'
 message,err,/cont
 return,''
end

err=''
t1=anytim2utc(tstart,err=err,/vms)
if err ne '' then get_utc,t1,/vms

t2=anytim2utc(tend,err=err,/vms)
if err ne '' then t2=t1

;-- call RD_NAR

loud=1-keyword_set(quiet)
if loud then begin
 message,'retrieving NAR data for '+ anytim2utc(t1,/vms),/cont
endif

rd_nar,t1,t2,nar,/nearest,status=pstat
pstat=pstat > 0
status=(1-pstat)
if not status then begin
 err='NOAA data not found for specified times'
 message,err,/cont
 return,''
endif

;-- determine unique AR pointings

sorder = uniq([nar.noaa], sort([nar.noaa]))
nar=nar(sorder)
count=n_elements(nar)
save_view=soho_view()
use_earth_view
for i=0,count-1 do begin
 temp=nar(i)
 helio=temp.location
 xy=hel2arcmin(helio(1),helio(0),date=anytim(temp,/utc_int))*60.
 temp=add_tag(temp,xy(0),'x')
 temp=add_tag(temp,xy(1),'y',index='x')
 new_nar=merge_struct(new_nar,temp)
endfor
if save_view then use_soho_view 

return,new_nar

end


