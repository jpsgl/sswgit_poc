;+
; Project     : SOHO - CDS
;
; Name        : XCAT
;
; Purpose     : widget interface to CDS AS-RUN catalog
;
; Category    : operations, widgets
;
; Explanation :
;
; Syntax      : IDL> XCAT
;
; Inputs      : None
;
; Opt. Inputs : TSTART,TEND = start/end times to list (any UT format)
;
; Outputs     : 
;
; Opt. Outputs: QL = CDS quicklook structure
;
; Keywords    : GROUP = widget ID of any calling widget
;               TSTART,TEND = start/end times to list (any UT format)
;               LAST = use last times specified by user
;
; Common      : XCAT - last saved AS-RUN listing
;
; Restrictions: None.
;
; Side effects: None.
;
; History     : Version 1,  2-Feb-1996,  D.M. Zarro.  Written
;               Version 2, 16-Jun-1996, Zarro, added call to CDS_SNAPSHOT
;		Version 3, 18-Jun-1996, William Thompson, GSFC
;			Added XACK if CDS_SNAPSHOT returns an error.
;		Version 4, 18-Jul-1996, William Thompson, GSFC
;			Call XCDS_SNAPSHOT instead of CDS_SNAPSHOT.
;		Version 5, 18-Aug-1997, William Thompson, GSFC
;			Modified to allow 5 digit program numbers.
;               Version 6, 22-Jan-1998, SVH Haugan, UiO
;                       Added call to readcdsfits(/header) to test for
;                       compressed fits files when appropriate.
;               Version 7, 10-March-1998, Zarro, SAC/GSFC
;                       Allowed repeat calls to XCDS_SNAPSHOT
;               Version 8, 2-June-1998, Zarro, SAC/GSFC
;                       Allowed searching on solar radius
;               Version 9, 1-Sept-1999, Zarro, SM&A/GSFC
;                       Added call to CDS_STACK to save QL structures
;                       in a pointer stack
;               Version 10, 7-Nov-1999, Zarro, SM&A/GSFC
;                       Fixed IDL V5 bug when calling DSP_MENU
;               Version 11, 13-Feb-2000, Zarro, (SM&A/GSFC)
;                       Added check for SDB and clean-up
;               Version 12, 23-Mar-2000, Zarro, (SM&A/GSFC)
;                       Added /NO_CACHE
;               Version 13, 19-Jan-2001, Zarro, (EITI/GSFC)
;                       Fixed long integer overflow
;               Version 14, 15-Jun-2001, Zarro, (EITI/GSFC)
;                       Improved database environment variable handling
;		Version 15, 30-May-2002, William Thompson, GSFC
;			Don't pass null string for USE_DIR to FIND_COMPRESSED
;               Version 16, 7-June-2002, Zarro (GSFC) - fixed USE_DIR
;               Version 17, 23-Aug-2002, Zarro (LAC/GSFC) - changed TRIM to
;                           faster, vectorized TRIM2
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

;-----------------------------------------------------------------------------

 pro xcat_cleanup,id                          ;-- cleanup routine

 dprint,'% XCAT cleaning up'
 set_cds_sdb,/orig

 return & end

;-----------------------------------------------------------------------------
 pro xcat_event,event                         ;-- event driver routine

common xcat_com2,cstart,cend,mdata,mlines,elines,efiles,edata,sbase,stags,mlabel,$
 info,wops1,wops2,elist,mlist,prog_num,ops1,ops2,cur_det,$
 viewb,fits_file,fits_dir,ftext,fdata,printb,struct,saveb,event_top,$
 cur_acronym,stype,atype,last_mdata,copyb,xcat_lis,xcat_stc,cur_search,dtype,$
 list_exp,list_f,xcat_sort,xcat_edit,ebut,plabel,xcat_rlist,ibase,xcat_base,$
 curr_struct,asrun,gifb,cur_radius,rtype,radbut,cur_dir,radius,extensions,$
 dsp_running,dsp_files,xcat_empty,xcat_cache,empty_but,cache_but,dsp_wids

 event_top=event.top

 widget_control, event.id, get_uvalue = uservalue
 if (n_elements(uservalue) eq 0) then uservalue=''
 bname=strtrim(uservalue,2)

;-- timer

 if bname(0) eq 'timer' then begin
  get_utc, curr_ut, /ecs,/vms
  widget_control,xcat_base, $
            tlb_set_title ='XCAT Current UT: '+strmid(curr_ut,0,20)
  if not xalive(sbase) then delvarx,curr_struct
  snap_running=xalive('xcds_snapshot',/name)
  xcat_dsp
  if dsp_running and snap_running then xkill,'xcds_snapshot'  

  widget_control,asrun,sensitive=exist(edata) and (fits_file ne '')
  device2,window=ow
  chk=where(ow gt 0,count)
  widget_control,gifb,sensitive=count gt 0
  widget_control,xcat_base,timer=1.
  return
 endif

;-- quit here

 if bname(0) eq 'exit' then begin
  if dsp_running then begin
   value=xanswer('DSP_MENU is still running.',$
                 instruct='Close DSP_MENU?',group=event_top)
   dsp_bases=get_handler_id('dsp_menu',/all)
   if value then xkill,dsp_bases else xshow,dsp_bases
  endif
  xtext_reset,[atype,wops1,wops2,stype,rtype] 
  xkill,event_top
  if xcat_empty then begin
   cds_stack,/clear
   if valid_ql(fdata) then delete_qlds,fdata
  endif
  return
 endif

;-- XSTUDY

 if bname(0) eq 'xstudy' then begin
  xstudy,group=event_top,/nowarn
  return
 endif

;-- XCAMP

 if bname(0) eq 'xcamp' then begin
  xcamp,tstart=ops1,tend=ops2,/last,group=event_top,/nowarn
  return
 endif

;-- OPTIONS

 if bname(0) eq 'options' then xcat_options

 if bname(0) eq 'asrun' then begin
  xcat_view
  return
 endif

 if bname(0) eq 'idone' then xhide,ibase

;-- check time inputs

 relist=0 & force=0
 info={wops1:wops1,wops2:wops2,ops1:ops1,ops2:ops2}
 good=xvalidate(info,event)
 ops1=info.ops1 & ops2=info.ops2
 if good then relist=(event.id eq wops1) or (event.id eq wops2) else return

;-- check search string

 widget_control,stype,get_value=new_search
 new_search=strupcase(trim(new_search(0)))
 if (new_search ne cur_search) then begin
  widget_control,stype,set_value=new_search
  cur_search=new_search
  relist=1 & force=1
 endif
 
;-- check search acronym

 widget_control,atype,get_value=new_acronym
 new_acronym=strupcase(strmid(trim(new_acronym(0)),0,8))
 if (new_acronym ne cur_acronym) then begin
  widget_control,atype,set_value=new_acronym
  cur_acronym=new_acronym
  relist=1 & force=1
;  if cur_search ne '' then force=1
 endif

;-- check search radius

 widget_control,rtype,get_value=new_radius
 new_radius=trim(new_radius(0))
 if (new_radius ne cur_radius) then begin
  widget_control,rtype,set_value=new_radius
  cur_radius=new_radius
  relist=1 & force=1
;  if cur_search ne '' then force=1
 endif

;-- sorting

 if strmid(bname(0),0,4) eq 'sort' then begin
  xcat_sort=bname(0) eq 'sort_on'
  relist=1
 endif
                          
;-- raster # listing

 if strmid(bname(0),0,5) eq 'rlist' then begin
  xcat_rlist=bname(0) eq 'rlist_on'
  return
 endif
                          
;-- switch on caching

 if strmid(bname(0),0,5) eq 'cache' then begin
  xcat_cache=bname(0) eq 'cache_on'
  if not xcat_cache then cds_stack,/clear
  widget_control,empty_but(0),sensitive=xcat_cache
  widget_control,empty_but(1),sensitive=xcat_cache
  return
 endif

;-- empty cache when exiting

 if strmid(bname(0),0,5) eq 'empty' then begin
  xcat_empty=bname(0) eq 'empty_on'
  return
 endif
                    
                          
;-- editing

 if strmid(bname(0),0,4) eq 'edit' then begin
  xcat_edit=bname(0) eq 'edit_on'

  if xcat_edit then begin
   if not priv_zdbase(/cat) then begin
    xack,'Sorry, but you do not have priviledge to edit current AS-RUN database',$
     group=event_top,/icon
    xcat_edit=0
    widget_control,ebut(1-xcat_edit),/set_button
   endif
  endif
  return
 endif

;-- search examples 

 if bname(0) eq 'examples' then begin
  xcat_help,group=event_top
  return
 endif

;-- reset fields

 if bname(0) eq 'reset' then begin
  cur_search='' & cur_acronym='' & cur_radius=''
  widget_control,atype,set_value=''
  widget_control,stype,set_value=''
  widget_control,rtype,set_value=''
  cur_det=0
  if widget_info(dtype,/type) eq 8 then $
   widget_control,dtype,set_droplist_select=cur_det else $
    widget_control,dtype,set_value=cur_det
  relist=1 & force=1
 endif

;-- new detector 

 if bname(0) eq 'dtype' then begin
  new_det=(event.index)
  if new_det ne cur_det then begin
   cur_det=new_det
   relist=1 & force=1
;   if cur_search ne '' then force=1
  endif
 endif

;-- radius search

 if bname(0) eq 'rswitch' then begin
  new_dir=(event.index)
  if new_dir ne cur_dir then begin
   cur_dir=new_dir
   if trim(cur_radius) ne '' then begin
    relist=1 & force=1
;    if cur_search ne '' then force=1
   endif
  endif
 endif

;-- switch DB

 if bname(0) eq 'switch' then begin
  db_val=event.index
  if db_val eq 1 then begin
   err=''
   xhour
   s=fix_zdbase(/cds,err=err)
   if err ne '' then begin
    xack,str2lines(err),group=event_top
    if widg_type(event.id) eq 'DROPLIST' then $
     widget_control,event.id,set_droplist_select=0 else $
    widget_control,event.id,set_value=0
    set_cds_sdb
    return
   endif
  endif else set_cds_sdb
  relist=1 & force=1
  xkill,sbase,stags
  if not priv_zdbase(/cat) then begin
   xcat_edit=0
   widget_control,ebut(1-xcat_edit),/set_button
  endif
  print,chklog('ZDBASE')
 endif

;-- relist 

 if bname(0) eq 'relist' then begin
  relist=1 & force=1
 endif

 if relist then xcat_relist,force=force

;-- print main list

 if bname(0) eq 'print' then xcat_mlist,/print

 if bname(0) eq 'save' then xcat_mlist,/save

 if bname(0) eq 'gif' then begin
  gfile=mk_temp_file('xcat.gif')
  wshow
  xinput,gfile,'Current window saved in GIF file: '+gfile,group=event_top,$
   status=status
  if status then x2gif,gfile
 endif

;-- edit comments database

 if bname(0) eq 'add' then begin
  xcat_seq,seq_num
  if exist(seq_num) then begin
   xcat_comment,seq_num,group=event_top,err=err
   if err ne '' then xack,err,group=event_top
  endif else xack,'Please select a RASTER-LEVEL entry first'
 endif

;-- view fits file

 if (bname(0) eq 'quick') or (bname(0) eq 'snap') or (bname(0) eq 'bail') $
    or (bname(0) eq 'itool') then begin

  if (bname(0) eq 'snap') and dsp_running then begin
   xack,'Please run SNAPSHOT from DSP_MENU.'
   return
  endif       

  new_file=1b 
  if valid_ql(fdata) and (fits_file ne '') and xcat_cache then begin
   new_file=(fits_file) ne trim(get_tag_value(fdata,/filename))
  endif

  if new_file then begin
   file_loc=chklog('CDS_FITS_DATA')
   if file_loc eq '' then begin
    xack,'CDS_FITS_DATA environmental/logical not defined',group=event_top
    return
   endif

;-- check if QL structure for this file is already saved in stack
   
   xhour
   fits_full_name = find_with_def(fits_file,'CDS_FITS_DATA',extensions,/reset)
   in_stack=0b
   if xcat_cache then cds_stack,fdata,/get,file=fits_full_name,status=in_stack 
   if not in_stack then begin
    xtext,'Please wait. Reading FITS file',wbase=tbase,/just_reg
    xhour
    fits_full_name=find_compressed(fits_full_name,status=compressed,$
                   use_dir='CDS_FITS_TEMP_W')

    if fits_full_name eq '' then begin
     xack,str2lines('Cannot locate FITS file: '+fits_file+$
         ' in CDS_FITS_DATA --> '+chklog('CDS_FITS_DATA')),group=event_top
     return
    endif
    err=''
    fdata=readcdsfits(fits_full_name,err=err)

;-- cleanup and check output

    if compressed then rm_file,fits_full_name    
    xkill,tbase
    if err ne '' then begin
     xack,err
     return
    endif

    if not valid_ql(fdata) then begin
     delvarx,fdata
     err='Invalid QL data in FITS file'
     xack,err,group=event_top
     return
    endif  
   endif

;-- save in stack

   if xcat_cache then cds_stack,fdata,/set,max_size=8
  endif 

  if bname eq 'itool' then begin
   mk_point_stc,stc
   shead=fdata.header
   stc.pointings.ins_x=float(shead.xcen)
   stc.pointings.ins_y=float(shead.ycen)
   stc.pointings.width=float(shead.ixwidth)
   stc.pointings.height=float(shead.iywidth)
   stc.date_obs=anytim2tai(shead.date_obs)
   stc.std_id=long(shead.study_id)    
   stc.sci_spec=shead.sci_spec
   stc.instrume='CDS'
   if xregistered('image_tool') eq 0 then begin
    xtext,'Please wait. Loading IMAGE_TOOL...',wbase=wbase,/just_reg,/hour
   endif
   image_tool,point=stc,group=event_top,/auto
   xkill,wbase
   return
  endif

  case bname(0) of
   'bail'  : begin
     xtext_reset,[stype,atype,wops1,wops2,rtype]
     xkill,event_top
    end
   'quick' : begin
     new_dsp=1b
     if dsp_running then begin
      break_file,fits_file,dsk,dir,fname,ext
      chk=where(strlowcase(fname) eq dsp_files,count)
      new_dsp=count eq 0
     endif
     if new_dsp then begin       
      dsp_menu,fdata,/nocheck,/nokeep,/delete
     endif else xshow,dsp_wids(chk)
    end
   else: begin
    xkill,'xcds_snapshot'
    err='' & xcds_snapshot,fdata,err=err,/own,group=event_top
    if err ne '' then xack,err,group=event_top
   end
  endcase
  return
 endif

;-- list events

 if event.id eq mlist then begin
  prog_num=long(bname(event.index))
  xcat_elist
 endif

 if event.id eq elist then begin
  fits_file=trim(bname(event.index))
  xhour
  fits_name = find_with_def(fits_file,'CDS_FITS_DATA',extensions,/reset)
  break_file,fits_name,dsk,dir
  fits_dir=trim(dsk+dir)
  if xcat_stc then xcat_view
 endif

;-- auto-list

 if strmid(bname(0),0,4) eq 'auto' then xcat_lis=bname(0) eq 'auto_on'

;-- raster details

 if strmid(bname(0),0,3) eq 'stc' then begin
  xcat_stc=bname(0) eq 'stc_on'
  if not xcat_stc then begin
   xkill,sbase & delvarx,struct
  endif
 endif

 xcat_buttons

 return & end

;---------------------------------------------------------------------------

pro xcat_dsp       ;-- check which QL files are in DSP_MENU

common xcat_com2

chk=xalive('dsp_menu',/name,count=count,ids=dsp_ids)
dsp_running=count gt 0 
dsp_files=''
if dsp_running then begin
 for i=0,n_elements(dsp_ids)-1 do begin  
  if xalive(dsp_ids(i)) then begin
   dsp_label=widget_info(dsp_ids(i),/child)
   if widget_info(dsp_label,/name) eq 'LABEL' then begin
    widget_control,dsp_label,get_value=value
    if datatype(value) eq 'STR' then begin
     if trim(value) ne '' then begin
      colon=strpos(value,':')
      if colon gt -1 then begin
       filid=strlowcase(trim(strmid(value,colon+1,strlen(value))))
       if i eq 0 then begin
        dsp_files=filid & dsp_wids=dsp_ids(i)
       endif else begin
        dsp_files=[dsp_files,filid]
        dsp_wids=[dsp_wids,dsp_ids(i)]
       endelse
      endif
     endif
    endif
   endif
  endif
 endfor
endif

return & end

;-----------------------------------------------------------------------------    

pro xcat_options,noshow=noshow              ;-- some options

common xcat_com2

show=1-keyword_set(noshow)
if not exist(xcat_edit) then xcat_edit=0b
if not exist(xcat_rlist) then xcat_rlist=0b
if not exist(xcat_lis) then xcat_lis=1b
if not exist(xcat_sort) then xcat_sort=1b
if not exist(xcat_stc) then xcat_stc=0b
if not exist(xcat_cache) then xcat_cache=1b
if not exist(xcat_empty) then xcat_empty=0b

if xalive(ibase) then begin
 if show then xshow,ibase
 return
endif

state='ibase=widget_base(/column,group=xcat_base,title="XCAT Options",event_pro="xcat_event"'
if idl_release(lower=3) then state=state+',tlb_frame_attr=8)' else state=state+')'
s=execute(state)

mk_dfont,bfont=bfont
lfont=''
first=widget_base(ibase,/row)
hideb=widget_button(first,value='Done',uvalue='idone',font=bfont,/frame)
row=widget_base(ibase,/row)
j1=widget_label(row,font=lfont,value='* List most recent entries first? ')
choices=['Yes','No']
xmenu,choices,row,/row,/exclusive,uvalue=['sort_on','sort_off'],font=bfont,$
                buttons=sbut,/no_rel
widget_control,sbut(1-xcat_sort),/set_button

row=widget_base(ibase,/row)
if list_f then begin
 j1=widget_label(row,font=lfont,value='* List EXPECTED vs ACTUAL number of rasters? ')
 choices=['Yes','No']
 xmenu,choices,row,/row,/exclusive,uvalue=['rlist_on','rlist_off'],font=bfont,$
                buttons=rbut,/no_rel
 widget_control,rbut(1-xcat_rlist),/set_button
endif

row=widget_base(ibase,/row)
j1=widget_label(row,font=lfont,value='* Automatically list AS-RUN entries each time XCAT is started? ')
choices=['Yes','No']
xmenu,choices,row,/row,/exclusive,uvalue=['auto_on','auto_off'],font=bfont,$
                buttons=abut,/no_rel
widget_control,abut(1-xcat_lis),/set_button

row=widget_base(ibase,/row)
j1=widget_label(row,font=lfont,value='* Automatically view AS-RUN details for selected FITS file?   ')
choices=['Yes','No']
xmenu,choices,row,/row,/exclusive,uvalue=['stc_on','stc_off'],font=bfont,$
                buttons=sbut,/no_rel
widget_control,sbut(1-xcat_stc),/set_button


row=widget_base(ibase,/row)
j1=widget_label(row,font=lfont,value='* Automatically enter EDIT mode when viewing AS-RUN details?  ')
choices=['Yes','No']
xmenu,choices,row,/row,/exclusive,uvalue=['edit_on','edit_off'],font=bfont,$
                buttons=ebut,/no_rel
widget_control,ebut(1-xcat_edit),/set_button



row=widget_base(ibase,/row)
j1=widget_label(row,font=lfont,value='* Cache QL data structures? (recommended for speed)') 
choices=['Yes','No']
xmenu,choices,row,/row,/exclusive,uvalue=['cache_on','cache_off'],font=bfont,$
                buttons=cache_but,/no_rel
widget_control,cache_but(1-xcat_cache),/set_button


row=widget_base(ibase,/row)
j1=widget_label(row,font=lfont,value='* Empty cache when exiting XCAT?  ')
choices=['Yes','No']
xmenu,choices,row,/row,/exclusive,uvalue=['empty_on','empty_off'],font=bfont,$
                buttons=empty_but,/no_rel
widget_control,empty_but(1-xcat_empty),/set_button
widget_control,empty_but(0),sensitive=xcat_cache
widget_control,empty_but(1),sensitive=xcat_cache


if keyword_set(show) then xshow,ibase

return & end

;--------------------------------------------------------------------------- 

pro xcat_edit          ;-- edit AS-RUN DB

common xcat_com2

  if datatype(struct) eq 'STC' then begin
   xkill,sbase,stags & new_struct=struct
   xhour
   ins='Edit desired fields and press "APPLY" to save the changes'
   xstruct,new_struct,nx=4,xsize=23,/noff,title='Raster-Level AS-RUN Entry',$
    group=event_top,/edit,/all,/modal,ins=ins,status=status,acc='APPLY'
   if not match_struct(new_struct,struct) and status then begin
    xhour
    xtext,'Please wait. Updating catalog database...',wbase=tbase,/just_reg
    temp=new_struct
    temp=rep_tag_value(temp,utc2tai(temp.date_obs),'date_obs')
    temp=rep_tag_value(temp,utc2tai(temp.date_end),'date_end')
    temp=rep_tag_value(temp,utc2tai(temp.obt_time),'obt_time')
    temp=rep_tag_value(temp,utc2tai(temp.obt_end),'obt_end')
    new_struct=rep_tag_value(temp,utc2tai(temp.date_mod),'date_mod')
    err=''
    clook=where(edata.seq_ind eq struct.seq_ind,cnt)
    if cnt eq 0 then begin
     err='Problems. Cannot locate original entry.'
    endif else begin
     orig_struct=edata(clook)
     status=call_function('mod_catalog',orig_struct,new_struct,err=err)
    endelse
    if err ne '' then begin
     xkill,tbase
     xack,err,group=event_top
    endif else begin
     orig_struct=edata(clook)
     copy_struct,new_struct,orig_struct
     edata(clook)=orig_struct
     xtext,'Update completed successfully',wbase=tbase,/wait,/append,/just_reg
     xcat_mlist,/force
    endelse
   endif else xtext,'No changes made',/wait,/just_reg
  endif 

  return & end

;----------------------------------------------------------------------------

 pro xcat_comment_event,  event                         ;event driver routine

;-- extract values passed as UVALUES in bases

 widget_control, event.top, get_uvalue = unseen
 info=get_pointer(unseen,/no_copy)
 if datatype(info) ne 'STC' then return

 widget_control, event.id, get_uvalue = uservalue
 wtype=widget_info(event.id,/type)
 if (n_elements(uservalue) eq 0) then uservalue=''
 bname=strtrim(uservalue,2)

;-- button events

 case bname of 

  'exit': xkill,event.top

  'add': begin
    err=''
    access=priv_zdbase(/cat,err=err)
    if not access then begin
     xack,err,group=event.top
    endif else begin
     instruct='Enter a comment [max 60 characters]'
     xinput,comment,instruct,group=event.top,max_len=info.max_len,$
            ysize=1,status=status
     if status then begin
      err='' & comm_stc={seq_num:info.seq_num,comm_no:-1l,comment:comment}
      xtext,'Updating Comments database...',/just_reg,wbase=tbase
      xhour
      s=call_function('add_exp_comm',comm_stc,err=err)
      xkill,tbase
      if err ne '' then xack,err,group=event.top else xcat_comment_list,info
     endif
    endelse
   end

  else:do_nothing=1
 endcase

 set_pointer,unseen,info,/no_copy
 return & end

;--------------------------------------------------------------------------- 

pro xcat_comment_list,info

list_exp_comm,info.seq_num,comments,nf

if nf eq 0 then begin
 lines='No Comments for Sequence Number: '+strtrim(string(info.seq_num),2)
 sens=0 
endif else begin
 lines=strarr(nf)
 comm_num=strtrim(string(comments.comm_no,'(i3)'),2)
 for i=0l,nf-1 do begin
  lines(i)=' '+strpad(comm_num(i),/aft,10)+strpad(comments(i).comment,60,/aft)
  sens=1
 endfor
endelse

widget_control,info.alist,sensitive=sens
widget_control,info.alist,set_value=lines
return & end

;--------------------------------------------------------------------------- 

pro xcat_comment,seq_num,group=group,err=err

;-- defaults

select_windows
if not have_widgets() then begin
 err='widget unavailable'
 message,err,/cont
 return
endif

err=''
if not exist(seq_num) then begin
 err='SEQ_NUM not entered' 
 message,err,/cont & return
endif

zdbase=chklog('ZDBASE')
file = find_with_def('exper_comment.dbf','ZDBASE',/reset) 
if strtrim(file,2) eq '' then begin
 err='Cannot find comment database files in current ZDBASE'
 message,err,/cont
 return
endif

;-- load fonts

mk_dfont,lfont=lfont,bfont=bfont

;-- make widgets

wbase=widget_base(title='SEQ_NUM:'+strtrim(string(seq_num),2),/column)

row1=widget_base(wbase,row=1,/frame)

;-- operation buttons

doneb=widget_button(row1,value='DONE',font=bfont,/no_rel,uvalue='exit')
newb=widget_button(row1,value='ADD NEW COMMENT', uvalue='add',font=bfont,/no_rel)

;-- comment list

max_len=60
mlabel=strpad('COMM_NO',10,/aft) +' '+strpad('COMMENT',max_len,/aft)
alabel=widget_list(wbase,value=mlabel,font=lfont,ysize=1,/frame)
alist=widget_list(wbase,value='',font=lfont,ysize=20,xsize=70)
s=seq_num
list_exp_comm,s,comm,n_found

;-- realize and center main base

xrealize,wbase,group=group,/center 

;-- stuff info structure into pointer

make_pointer,unseen
info ={alist:alist,seq_num:long(seq_num),max_len:max_len}

xcat_comment_list,info

set_pointer,unseen,info,/no_copy
widget_control,wbase,set_uvalue=unseen

xmanager,'xcat_comment',wbase,group=group,/modal
xmanager_reset,wbase,group=group,/modal

info=get_pointer(unseen,/no_copy)

;-- clean-up

free_pointer,unseen
 
return & end

;--------------------------------------------------------------------------- 

pro xcat_check,in_search,out_search,exper=exper,status=status,syntax=syntax

out_search=''
status=0 & syntax=0
if datatype(in_search) ne 'STR' then return
if trim(in_search) eq '' then begin
 status=1 & syntax=1 & return
endif

temp=trim2(str2arr(strupcase(strcompress(in_search))))

nitems=n_elements(temp)

;-- setup valid search fields

main_tags=['PROG_NUM','PROG_ID','PROG_IND','STUDY_ID','STUDYVAR',$
           'OBS_PROG','DETECTOR','SCI_OBJ','SCI_SPEC','CMP_NO',$
           'OBJECT','OBJ_ID','DATE_OBS','DATE_END','OBT_TIME','OBT_END',$
           'XCEN','YCEN','ANGLE','IXWIDTH','IYWIDTH','SEQ_FROM','SEQ_TO',$
           'COMMENTS','DATE_MOD']

exper_tags=['PROG_NUM','PROG_ID','PROG_IND','N_REPEAT_S','STUDY_ID',$
            'STUDYVAR','OBS_PROG','SCI_OBJ','SCI_SPEC','CMP_NO',$
            'OBJECT','OBJ_ID','SEQ_NUM','OBS_SEQ','COUNT','SEQ_IND',$
            'RAS_ID','RAS_VAR','EXPTIME','OBS_MODE','DW_ID',$
            'DATE_OBS','DATE_END','OBT_TIME','OBT_END','XCEN',$
            'YCEN','ANGLE','IXWIDTH','IYWIDTH','INS_X0','INS_Y0',$
            'INS_ROLL','SC_X0','SC_ROLL','WAVEMIN','WAVEMAX',$
            'TRACKING','SER_ID','OPSLBITS','OPSRBITS',$
            'SLIT_POS','MIR_POS','EV_ENAB','COMP_ERR',$
            'VDS_PMCP','VDS_MODE','VDS_ORI','VDS_ACC','GSET_ID','DETECTOR',$
            'ZONE_ID','SLIT_NUM','FILENAME','SEQVALID','DATASRC','COMMENTS',$
            'DATE_MOD']

if keyword_set(exper) then tags=exper_tags else tags=main_tags
ntags=n_elements(tags)

;-- now do validation

delim=['=','<','>']

ndelim=n_elements(delim)
valid_tag=bytarr(nitems)
bad_input=bytarr(nitems)
for i=0l,nitems-1 do begin
 tsplit=str2arr(temp(i),'=')
 if n_elements(tsplit) eq 1 then tsplit=str2arr(temp(i),'<')
 if n_elements(tsplit) eq 1 then tsplit=str2arr(temp(i),'>')
 tsplit=trim2(tsplit)
 ok=where_vector(tsplit,tags,tcount)

;-- check for sensible fields

 if tcount eq 1 then begin
  if n_elements(tsplit) eq 1 then bad_input(i)=1
  if n_elements(tsplit) eq 2 then begin
   if (tsplit(0) ne tags(ok(0))) or (tsplit(1) eq '') then bad_input(i)=1
  endif
  if n_elements(tsplit) eq 3 then begin
   if (tsplit(1) ne tags(ok(0))) or (tsplit(0) eq '') or $
      (tsplit(2) eq '') or (strpos(temp(i),'>') gt -1) then bad_input(i)=1
  endif
 endif
 if (tcount eq 1) and (not bad_input(i)) then begin
  valid_tag(i)=1
  if exist(done) then chk=where(ok(0) eq done,dcount) else dcount=0
  if dcount eq 0 then begin
   if exist(valid) then valid=[valid,temp(i)] else valid=temp(i)
   if exist(done) then done=[done,ok(0)] else done=ok(0)
  endif
 endif else begin
  if keyword_set(exper) then atags=main_tags else atags=exper_tags
  acheck=where_vector(tsplit,atags,acount)
  if (acount gt 0) then valid_tag(i)=1
 endelse
endfor

if exist(valid) then out_search=arr2str(valid)

if max(valid_tag) eq 1 then status=1
if max(bad_input) eq 1 then syntax=1

dprint,'out_search: ',out_search

return & end


;--------------------------------------------------------------------------- 

pro xcat_help,group=group

common xcat_com2_help,tbase

if xalive(tbase) then begin
 xshow,tbase
 return
endif

examples=['Use commas to separate search strings.',$
          'Search is case-insensitive.','Some examples:','',$
          'SCI_OBJ = Active Region, STUDY_ID = 10','',$
          'XCEN > 100, YCEN > 200','',$
          'RAS_ID = 10']

xtext,examples,group=group,/no_print,/no_save,/no_find,space=2,$ 
     title='Search examples',xsize=50,wbase=tbase

return & end

;--------------------------------------------------------------------------- 

pro xcat_seq,seq_num

common xcat_com2

if exist(edata) and (fits_file ne '') then begin
 elook=where(trim(edata.filename) eq fits_file,count)
 if count eq 1 then seq_num=edata(elook).seq_num
endif

return & end

;--------------------------------------------------------------------------- 

pro xcat_view

common xcat_com2

if exist(edata) and (fits_file ne '') then begin
 elook=where(trim2(edata.filename) eq fits_file,count)
 if count eq 1 then begin
  if exist(curr_struct) then begin
   if curr_struct eq fits_file then begin
    xshow,sbase
    return
   endif
  endif
  xhour
  entry=edata(elook)
  entry=rep_tag_value(entry,tai2utc(entry.date_obs,/ecs),'date_obs')
  entry=rep_tag_value(entry,tai2utc(entry.date_end,/ecs),'date_end')
  entry=rep_tag_value(entry,tai2utc(entry.obt_time,/ecs),'obt_time')
  entry=rep_tag_value(entry,tai2utc(entry.obt_end,/ecs),'obt_end')
  struct=rep_tag_value(entry,tai2utc(entry.date_mod,/ecs),'date_mod')
  if xcat_edit then xcat_edit else begin
   xstruct,struct,nx=4,xsize=23,/noff,title='Raster-Level AS-RUN Entry',$
    wbase=sbase,wtags=stags,/just_reg,group=event_top
   curr_struct=fits_file
  endelse
  
 endif
endif
return & end

;--------------------------------------------------------------------------- 

pro xcat_init

common xcat_com2

delvarx,struct
fits_file='' & prog_num=-1
fits_dir=''

return & end

;--------------------------------------------------------------------------- 


pro xcat_buttons

common xcat_com2

if not exist(fits_dir) then fits_dir=''
temp=trim2(concat_dir(fits_dir,fits_file))
widget_control,ftext,set_value=temp
if trim(temp) ne '' then dprint,temp
widget_control,viewb,sensitive=(fits_file ne '')
widget_control,printb,sensitive=datatype(mlines) eq 'STR'
widget_control,saveb,sensitive=datatype(mlines) eq 'STR'
return & end

;--------------------------------------------------------------------------- 

pro xcat_relist,force=force

xcat_init
xcat_mlist,force=force
xcat_elist

return & end

;--------------------------------------------------------------------------- 

pro xcat_mlist,print=print,force=force,save=save

common xcat_com2

;-- initialize

no_lines='No AS-RUN Entries in Main DB for specified interval and/or search string(s)'

;-- read main catalog

time_change=1
if exist(cstart) and exist(cend) then begin
 time_change=(cstart ne ops1) or (cend ne ops2)
endif

widget_control,mlabel,set_value=plabel

if time_change or keyword_set(force) then begin
 widget_control,mlist,set_value=''
 widget_control,elist,set_value=' '
 xtext,'Please wait. Searching Main AS-RUN Catalog',wbase=tbase,/just_reg
 delvarx,mdata,mlines
 xhour
 nfound=0
 if trim(cur_search) eq '' then begin
  list_main,ops1,ops2,mdata,nfound
 endif else begin
  xcat_check,cur_search,ok_search,status=status,syntax=syntax
  if (not syntax) then begin
   list_main,ops1,ops2,mdata,nfound,search=ok_search
  endif else begin
   if (not status) then begin
    xmess=['Invalid input search string:','',cur_search,'','Search not implemented']
    xkill,tbase
    xack,xmess,group=event_top
    return
   endif
  endelse
 endelse
 cstart=ops1 & cend=ops2
 if nfound gt 0 then last_mdata=mdata
 xkill,tbase
endif

nfound=n_elements(mdata)
if (nfound gt 0) then begin

 dets=['A','N','G']
 extra1='' & extra2='' 
 if cur_det gt 0 then extra1='(mdata.detector eq dets(cur_det))'
 if cur_acronym ne '' then begin
  check=strpos(mdata.obs_prog,cur_acronym)
  extra2='(check gt -1)'
 endif

 if (extra1 ne '') or (extra2 ne '') then begin
  if (extra1 ne '') and (extra2 ne '') then action=extra1+' and '+extra2 else $
   action=extra1+extra2
  s='pick=where('+action+',nfound)'
  dprint,s
  s=execute(s)
  if nfound gt 0 then mdata=temporary(mdata(pick))
 endif

;-- filter out studies within specified radius 
;   (for speed, only compute solar radius once)

 if cur_radius ne '' then begin
  if nfound gt 0 then begin
   if not exist(radius) then begin
    mstart=(mdata(0).date_obs)
    err=''
    pb=pb0r(mstart,/soho,/arcsec,/retain,err=err)
    if err ne '' then radius=float(pb(2)) else radius=960.
   endif
   mrad=sqrt(mdata.xcen^2+mdata.ycen^2)/radius
   if cur_dir eq 0 then bool='le' else bool='ge'
   expre='keep=where(mrad '+bool+' float(cur_radius),nfound)'
   state=execute(expre)
   if nfound gt 0 then mdata=temporary(mdata(keep))
  endif
 endif

 if nfound gt 0 then begin
  psort=sort([mdata.date_obs])
  if xcat_sort then psort=reverse(psort)
  xhour
  acronym=mdata.obs_prog
  sprog_num=strpad(trim2(fstring(mdata.prog_num,'(i5)')),5)
  scamp_num=strpad(trim2(fstring(mdata.cmp_no,'(i4)')),4)
  desc=sprog_num+' | '+strtrim(tai2utc(mdata.date_obs,/ecs),2)
  desc=desc+' | '+strpad(strtrim(acronym,2),8,/after)
  desc=desc+' |  '+scamp_num
  desc=desc+' | '+strtrim(mdata.detector,2)+'IS'
  desc=desc+' | '+strpad(strmid(strtrim(mdata.sci_obj,2),0,50),50,/after)
  mlines=desc(psort)       
  mdata=temporary(mdata(psort))
 endif
endif

;-- list main entries

if (nfound gt 0) and (keyword_set(print) or keyword_set(save)) then begin
 tstart=strmid(tai2utc(ops1,/ecs,/vms),0,17)
 tstop=strmid(tai2utc(ops2,/ecs,/vms),0,17)
 marker='-------------------------------------------------------------'
 plist=['CDS FITS FILE LISTING FOR PERIOD: '+tstart+' TO '+tstop,'']
 plist=[plist,'Printed on '+!stime,marker,'',plabel,'']
 if keyword_set(print) then xprint,array=[plist,mlines],group=event_top else begin
  file=mk_temp_file('xcat.lis')
  fwrite=pickfile(group=event_top,path=home,/write,file=file)
  if strtrim(fwrite) ne '' then str2file,[plist,mlines],fwrite
  return
 endelse
 return
endif

if (nfound eq 0) then begin
 widget_control,mlist,set_value=no_lines
 widget_control,mlist,set_uvalue='-1'
endif else begin
 widget_control,mlist,set_value=mlines
 widget_control,mlist,set_uvalue=trim2(string(mdata.prog_num))
endelse
widget_control,printb,sensitive=(nfound gt 0)
widget_control,saveb,sensitive=(nfound gt 0)

if (nfound gt 0) then begin
 clook=where(mdata.prog_num eq prog_num,count)
 if count eq 1 then widget_control,mlist,set_list_select=clook(0) else $
  prog_num=-1
endif

return & end

;--------------------------------------------------------------------------- 

pro xcat_elist,infile=infile

common xcat_com2

if prog_num lt 0 then begin
 widget_control,elist,set_value='         '
 return
endif

no_lines='No AS-RUN Raster Entries in Experiment DB for specified Prog Num and/or search string(s)'

;-- relist experiments

relist=0
if exist(edata) then begin
 elook=where(edata.prog_num eq prog_num,count)
 if count eq 0 then relist=1
endif else relist=1 

if relist then begin
 fits_dir=''
 if (1-keyword_set(infile)) then fits_file=''
 widget_control,elist,set_value='         '
 delvarx,edata,elines,efiles
 xtext,'Please wait. Searching Experiment AS-RUN Catalog',wbase=tbase,/just_reg
 xhour
 nfound=0
 if trim(cur_search) eq '' then begin
  list_exper,prog_num,edata,nfound
 endif else begin
  xcat_check,cur_search,ok_search,/exp,syntax=syntax,status=status
  if (not syntax) then begin
   list_exper,prog_num,edata,nfound,search=ok_search
  endif else begin
   if (not status) then begin
    xmess=['Invalid input search string:','',cur_search,'','Search not implemented']
    xkill,tbase
    xack,xmess,group=event_top
    return
   endif
  endelse
 endelse
 nfound=n_elements(edata)
 if (nfound gt 0) then begin
  diff=(edata.date_end-edata.date_obs) > 0.
  sdur=strarr(nfound)
  for i=0l,nfound-1 do sdur(i)=strpad(trim2(sec2dhms(diff(i))),0)
  xcen=strpad(trim2(fstring(edata.xcen,'(f7.1)')),7)
  ycen=strpad(trim2(fstring(edata.ycen,'(f7.1)')),7)

  elines=strpad(strtrim(string(edata.seq_ind),2),6,/after)+$
        ' | '+strtrim(tai2utc(edata.date_obs,/ecs),2) + $
        ' | '+strpad(strmid(strtrim(edata.sci_spec,2),0,20),20,/after)+$
        ' | '+strpad(strtrim(edata.filename,2),14) + '  | '+$
              strtrim(edata.detector,2)+'IS'+' |'+$
        '    '+xcen+$
        '          '+ycen+$
        '        '+sdur

;-- append raster info

  efiles=trim2(edata.filename)
  if list_f and xcat_rlist then xcat_raster
 endif
 xkill,tbase
endif

nfound=n_elements(elines)
if (nfound eq 0) then begin
 widget_control,elist,set_value=no_lines
 widget_control,elist,set_uvalue=' '
endif else begin
 widget_control,elist,set_value=elines
 widget_control,elist,set_uvalue=efiles
endelse

if (nfound gt 0) then begin
 flook=where(strtrim(edata.filename,2) eq trim(fits_file),count)
 if count eq 1 then widget_control,elist,set_list_select=flook(0)
endif

return & end

;--------------------------------------------------------------------------- 

pro xcat_raster           ;-- determine how many rasters were performed

common xcat_com2

nfound=n_elements(efiles)
if nfound gt 0 then begin
 err=''
 e_id=edata(0).study_id
 e_var=edata(0).studyvar
 eplan=call_function('get_cds_plan',edata(0).date_obs,e_id,e_var,$
                     prog=edata(0).prog_num,err=err)
 if (err eq '') then begin
  get_study_par,e_id,e_var,err=err,reps=reps
  if err eq '' then begin
   nrep=n_elements(reps) & rinit=0
   if nrep gt 1 then rinit=total(reps(0:nrep-2))
   nrast=(eplan.n_repeat_s > 1)*(eplan.n_pointings > 1)*(rinit+eplan.n_rasters1)
   line1='Actual   # of rasters: '+trim2(string(fix(nfound)))
   line2='Expected # of rasters: '+trim2(string(fix(nrast)))
   elines=[elines,'',line1,line2]
   efiles=[efiles,'','','']
  endif
 endif
endif

return & end

;--------------------------------------------------------------------------- 

pro xcat2,ql,tstart=tstart,tend=tend,group=group,modal=modal,$
            last=last,reset=reset,no_cache=no_cache

common xcat_com2

on_error,1

;-- defaults

select_windows
if not have_widgets() then begin
 message,'widgets unavailable',/cont
 return
endif

defsysv,'!def_gset_id',exists=defined
if not defined then defsysv,'!def_gset_id',22

;-- caching on?

cache_env=trim(getenv('XCAT_CACHE'))
if cache_env ne '' then begin
 if cache_env eq '0' then begin
  xcat_cache=0b
  message,'XCAT caching deactivated. Use OPTIONS to reactivate',/cont
 endif
endif

if keyword_set(no_cache) then xcat_cache=0b
   
;-- how was XCAT called?

caller=get_caller(stat)
if stat or (strpos(caller,'XCAT') gt -1) then xkill,/all
if (xregistered('xcat') ne 0) then begin
 xack,'Only one copy of XCAT can run.'
 id=get_handler_id('xcat')
 xshow,id
 return
endif

modal=keyword_set(modal)

;-- check databases

s=fix_zdbase(/user,/init)
if (s eq 0) then s=fix_zdbase(/cds,/init)
if caller eq '' then set_cds_sdb
find_zdbase,cur_db_type,status=status,/cat,/off
if not status then begin
 zdbase=chklog('ZDBASE')
 emess=['MAIN and EXPERIMENT Catalogs not in: ',zdbase]
 xack,emess,group=group
 return
endif

;-- initialize

if not exist(prog_num) then prog_num=-1
if datatype(fits_file) ne 'STR' then fits_file=''
ql_in=n_params() eq 1
extensions='.Z,.gz'

;-- reconcile times

secs_per_day=24l*3600l
week=7l*secs_per_day
month=30l*secs_per_day
last=keyword_set(last)

get_utc,ops1 & ops1.time=0
ops1=(utc2tai(ops1)-week) > utc2tai(anytim2utc('2-dec-95')) 
get_utc,ops2 & ops2.time=0
ops2.mjd=ops2.mjd+1
ops2=utc2tai(ops2)
dtime=[ops1,ops2]
ctime=[0.d,0.d]
if exist(cstart) then ctime(0)=anytim2tai(cstart)
if exist(cend) then ctime(1)=anytim2tai(cend)

times=pick_times(tstart,tend,ctime=ctime,dtime=dtime,last=last)
ops1=times(0) & ops2=times(1)

relist=1
if datatype(sav_db) ne 'STR' then sav_db=chklog('ZDBASE')
cur_db=chklog('ZDBASE')
if cur_db ne sav_db then begin
 message,'ZDBASE changed!!!',/contin
 xcat_init & sav_db=cur_db & relist=1
endif

;-- load fonts

mk_dfont,lfont=lfont,bfont=bfont

xcat_base=widget_base(title='XCAT',/column,uvalue='timer')

;-- operation buttons

row1=widget_base(xcat_base,/row,/frame)
exitb=widget_button(row1,value='Exit',uvalue='exit',font=bfont,/no_rel)
relistb=widget_button(row1,value='Relist',uvalue='relist',font=bfont,/no_rel)

outb=widget_button(row1,value='Output',font=bfont,/menu)
printb=widget_button(outb,value='Print Main AS-RUN List',uvalue='print',font=bfont,/no_rel)
saveb=widget_button(outb,value='Save Main AS-RUN List',uvalue='save',font=bfont,/no_rel)
gifb=widget_button(outb,value='Save Latest Plot As GIF File',uvalue='gif',font=bfont)
widget_control,printb,sensitive=0
widget_control,saveb,sensitive=0
widget_control,gifb,sensitive=0

;-- only allow catalog editing if in CDS environment

list_f=have_proc('list_f_study')
list_exp=have_proc('list_exp_comm')

;-- only allow database switching if not called from MK_PLAN

exclude=['mk_cds_plan_event']
chk=where(strlowcase(caller) eq exclude,cnt)
allow_db=cnt eq 0

if allow_db then begin
 datab=widget_button(row1,value='Other Databases',font=bfont,/menu)
 studyb=widget_button(datab,value='View CDS Study Database (XSTUDY)',uvalue='xstudy',font=bfont,/no_rel)
 campb=widget_button(datab,value='View SOC Campaign Database (XCAMP)',uvalue='xcamp',font=bfont,/no_rel)
 widget_control,studyb,sensitive=list_f
endif

if list_exp then $
 opb=widget_button(row1,value='View Comments',font=bfont,uvalue='add')

asrun=widget_button(row1,value='View AS-RUN Raster Details',font=bfont,uvalue='asrun')

opt=widget_button(row1,value='Options',font=bfont,uvalue='options')

;-- date/time fields

row2=widget_base(xcat_base,/column,/frame)

temp=widget_base(row2,/row)
tlabel=widget_label(temp,font='',$
       value='* Edit Start/Stop Time fields to list Main AS-RUN entries for different periods (Time Units: DD-MON-YR HH:MM:SS.MS)')



trow=widget_base(row2,/row)
wops1=cw_field(trow, Title= 'Start Time:  ',value=' ',$
                    /ret, xsize = 24, font = bfont,field=bfont)

wops2=cw_field(trow,  Title='Stop Time:   ', value=' ',$
                    /ret, xsize = 24, font = bfont,field=bfont)


;-- detector and search fields

row=widget_base(xcat_base,/column,/frame)
row3=widget_base(row,/row)

new_vers=float(strmid(!version.release,0,3)) ge 4.
choices=['NIS & GIS', 'NIS only', 'GIS only ']
if new_vers then $
 dtype=call_function('widget_droplist',row3,value=choices,font=bfont,$
  uvalue='dtype',title='Detector:') else $
 dtype = cw_bselector2(row3,choices,label_left='Detector:',$
                     /return_index, uvalue='dtype',font=bfont,/no_rel)

;-- Acronym

atype=cw_field(row3,  Title='Study Acronym: ', value=' ',uvalue='atype',$
                        xsize = 8, font = bfont,field=bfont,/ret)

temp=widget_base(row3,/row)
slab=widget_label(temp,value='Solar Radius: ',font=bfont)

radbut=cw_bselector2(temp,['<','>'],uvalue='rswitch',/no_rel,font=bfont,/return_index)
rtype=widget_text(temp,value='',uvalue='radius',xsize=4,font=bfont,/editable)

;rtype=cw_field(row3,  Title='Solar Radius: ', value='  ',uvalue='radius',$
;                        xsize = 4, font = bfont,field=bfont,/ret)

sbutt=widget_button(row3,value='Search',uvalue='relist',font=bfont)

rbutt=widget_button(row3,value='Reset',uvalue='reset',font=bfont)

;-- search string

row4=widget_base(row,/row)
stype=cw_field(row4,  Title='Search String(s): ', value=' ',uvalue='stype',$
                        xsize = 44, font = bfont,field=bfont,/ret)

exam=widget_button(row4,value='Examples',uvalue='examples',font=bfont)


;-- view FITS files
 

row=widget_base(xcat_base,/column,/frame)

row1=widget_base(row,/row)
r2=widget_base(row1,/row)
ilabel=widget_label(r2,font='',value='* To view FITS files, '+$
       'first select PROG # from list of AS-RUN entries and then select '+$
       'INDEX from list of RASTER-LEVEL entries')

row2=widget_base(row,/row)
viewb=widget_button(row2,value='Read & View FITS File:',$
                    font=bfont,/no_rel,/menu)
snapb=widget_button(viewb,value='Snapshot?',$
                    font=bfont,/no_rel,uvalue='snap')
quickb=widget_button(viewb,value='Quicklook?',$
                    font=bfont,/no_rel,uvalue='quick')
itoolb=widget_button(viewb,value='Overplot FOV in IMAGE_TOOL?',$
                    font=bfont,/no_rel,uvalue='itool')

if ql_in then begin
 bailb=widget_button(viewb,value='Read and Exit with QL data structure?',$
                     font=bfont,/no_rel,uvalue='bail')
endif

ftext=widget_text(row2,value='',font=bfont,xsize=45)
widget_control,viewb,sensitive=0

;-- switch DB

if allow_db then begin
 choices=['Personal','Official']

 db_val=(cur_db_type ne 'USER')
 new_vers=float(strmid(!version.release,0,3)) ge 4.
 if new_vers then begin
  db_but=call_function('widget_droplist',row2,value=choices,$
   uvalue='switch',font=bfont,title='Database:') 
  widget_control,db_but,set_droplist_select=db_val
 endif else begin
  db_but=cw_bselector2(row2,choices,uvalue='switch',$
   /no_rel,font=bfont,/return_index,label_left='Database:')
  widget_control,db_but,set_value=db_val
 endelse
endif


;-- list main CDS as-run entries on top
;-- list raster-level as-run entries on bottom

temp=xcat_base
mtop=widget_label(font=lfont,temp,value='MAIN CDS AS-RUN ENTRIES')

plabel='PROG #  DATE_OBS                  ACRONYM   CMP_NO   DET  SCI_OBJ' 
mlabel=widget_list(temp,value=plabel,font=lfont,ysize=1,/frame,xsize=60)
mlist=widget_list(temp,value=' ',font=lfont,ysize=10)
etop=widget_label(temp,value='RASTER-LEVEL AS-RUN ENTRIES',font=lfont)
value='INDEX    DATE_OBS                  SCI_SPEC               FITS FILE  '+$
       '    DETECTOR    XCEN (+W/-E)     YCEN (+N/-S)   DUR'
elabel=widget_list(temp,value=value,font=lfont,ysize=1,/frame,xsize=20)
elist=widget_list(temp,value='   ',font=lfont,ysize=10)

;-- set up options base

xcat_options,/noshow

;-- offset main base

xrealize,xcat_base,group=group,/center

if keyword_set(reset) then begin
 cur_acronym='' & cur_search='' & cur_det=0 & relist=1 & cur_radius=''
endif

widget_control,wops1,set_value=tai2utc(ops1,/ecs,/vms)
widget_control,wops2,set_value=tai2utc(ops2,/ecs,/vms)
if not exist(cur_det) then cur_det=0
if not exist(cur_acronym) then cur_acronym=''
if not exist(cur_radius) then cur_radius=''
if not exist(cur_dir) then cur_dir=0
if not exist(cur_search) then cur_search=''
if widget_info(dtype,/type) eq 8 then $
 widget_control,dtype,set_droplist_select=cur_det else $
  widget_control,dtype,set_value=cur_det
widget_control,atype,set_value=cur_acronym
widget_control,stype,set_value=cur_search
widget_control,rtype,set_value=cur_radius
widget_control,radbut,set_value=cur_dir

;-- load input QL

if ql_in then begin
 if valid_ql(ql) then begin
  fdata=temporary(ql)
  fits_file=trim(get_tag_value(fdata,/filename,err=err))
  prog_num=get_tag_value(fdata,/prog_num)
  if err then fits_file=''
  infile=1 
 endif
endif

if xcat_lis or relist then begin
 xcat_mlist,force=relist
 xcat_elist,infile=infile
 xcat_buttons
endif else xcat_init

widget_control,xcat_base,timer=1.

xmanager,'xcat',xcat_base,modal=modal,group=group,cleanup='xcat_cleanup'
xmanager_reset,xcat_base,modal=modal,group=group,crash='xcat'

;-- copy FITS data from memory
;   (only pass QL out of common if user specifies it on command line)

if not xalive(xcat_base) then begin
 if ql_in then begin
  if valid_ql(fdata) then begin
   ql=temporary(fdata)
  endif
 endif
endif

xcat_cleanup

return & end
