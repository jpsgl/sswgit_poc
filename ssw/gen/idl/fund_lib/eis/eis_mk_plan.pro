;+
; Project     : SOLAR-B/EIS
;
; Name        : EIS_MK_PLAN
;
; Purpose     : Driver for EIS_MK_PLAN objects
;
; Category    : planning
;
; Syntax      : IDL> eis_mk_plan
;
; Inputs      : None
;
; Outputs     : None
;
; Keywords    : /EIS - set to force EIS official DB
;               /USER - set for personal DB
;
; History     : Written, 30-May-2006, Zarro (L-3Com/GSFC)
;
; Contact     : dzarro@solar.stanford.edu
;-

pro eis_mk_plan,eis=eis,user=user

;-- Start the timeline using the official EIS database.
;-- Call EIS_MK_PLAN with /USER for personal database

defsysv,'!PRIV', 2

;-- only set ZDBASE if undefined, or requested via keyword

status=1b
case 1 of
 keyword_set(eis) : status=fix_zdbase(/eis)
 keyword_set(user) :status=fix_zdbase(/user)
 if (getenv('ZDBASE') eq '') : status=fix_zdbase(/eis)
 else: do_nothing=1
endcase

if not status then return

error=0
catch,error
if error ne 0 then begin
 message,err_state(),/cont
 catch,/cancel
 eis_mk_plan_obj
 return
endif

;-- Call "eis_mk_plan_gui" first to force complilation of GUI widgets.
;-- It produces "undefined procedure/function" errors that we ignore via CATCH.
;-- These errors occur when the standard IDL convention of placing class
;-- definitions in files named class__define.pro is not being adhered to.

 
eis_mk_plan_gui

eis_mk_plan_obj

return & end

