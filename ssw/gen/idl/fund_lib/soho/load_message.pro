;+
; Project     : SOHO - LASCO
;
; Name        : 
;
; Purpose     : 
;
; Category    : 
;
; Explanation : 
;
; Syntax      : 
;
; Examples    : 
;
; Inputs      : None
;
; Opt. Inputs : None
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Common      : 
;
; Restrictions:                                  
;
; Side effects: Not known
;
; History     : Version 1, 02-Sep-1995, B Podlipnik. Written
;
; Contact     : BP, borut@lasco1.mpae.gwdg.de
;-

function load_message 

message    = strarr(10)
message(0) = ' READY '
message(1) = ' FILE NOT FOUND '
message(2) = ' WAIT WHILE LOADING '
message(3) = ' WORKING ... '

return,message
end
