;+
; NAME:
;	rdbda_smm
; PURPOSE:
;	read SMM BCS BDA file, accumulate, and produce BSC index, data structs
; CALLING SEQUENCE:
;	rdbda_smm,file,index,data,verbose=verbose,$
;           chan=chan,accum=accum,tstart=tstart,tend=tend,ierr=ierr
; INPUTS:
;	file = string BDA file name
; OUTPUTS:
;       index,data = BSC index and data array structures (Yohkoh format)
; KEYWORDS:
;       chan (in)   = channel to process [def =1]
;       accum (in)  = accumulation time (secs) [def=20]
;       tstart,tend (in) = start,end times to process (e.g. 0700:30) [def=all times]
;	verbose (in) = on, for verbose output
;       ierr (out) = 0/1 for success/failure
; PROCEDURE:
;       Based on D. Mathur's fortran BDA_SPECTRA
; RESTRICTIONS:
;       Requires following logical/environment variable definitions:
;       BCSBINLST = bcsbinlst.idl (BCS bin list id file)
;       BCSFMTLST = bcsfmtlst.idl (BCS format id file)
; MODIFICATION HISTORY:
;       31 Jan 1994, written DMZ (ARC)
;-

        pro rdbda_smm,file,index,data,verbose=verbose,$
           chan=chan,accum=accum,tstart=tstart,tend=tend,ierr=ierr,notch=notch                     
@bda_com_blk

        ierr=1

;-- check and open bda file

        flook=loc_file(file,count=nf)
        if nf eq 0 then message,'bda file not found'
        openr,lunda,flook(0),/get_lun,/block


;-- open bin and fmt list files
 
        cd,curr=curr
        if !version.os eq 'vms' then delim=',' else delim=':'
        cpath=get_lib()
        binlist=loc_file('BCSBINLST',count=nf)
        if nf eq 0 then begin
         binlist=loc_file('bcsbinlst.idl',path=cpath,count=nf)
         if nf eq 0 then message,'bcs bin list file not found'
        endif
        openr,lunbin,binlist(0),/get_lun,/block

        fmtlist=loc_file('BCSFMTLST',count=nf)
        if nf eq 0 then begin
         fmtlist=loc_file('bcsfmtlst.idl',path=cpath,count=nf)
         if nf eq 0 then message,'bcs fmt list file not found'
        endif
        openr,lunfmt,fmtlist(0),/get_lun,/block

;-- prepare input/out structures for reading/writing

        bda_structure,ixr & ixr=[ixr,ixr]
        pnt_structure,pntrec
        max_spec=600
        maxb=256
        index=mk_bsc_str(/sample,ncopy=max_spec)
        index.bsc.st$spacecraft=byte('SMM')
        tindex=index(0)
        cindex=tindex
        dtype=3
        data=mk_bsc_str(dtype,maxb,ncopy=max_spec)
        tdata=data(0)
        cdata=tdata

;-- processing options

        if keyword_set(verbose) then verb=1 else verb=0
        if n_elements(accum) eq 0 then accum=20.
        if n_elements(chan) eq 0 then chan=1
        ok=where( (chan ge 1) and (chan le 8),count)
        if count eq 0 then message,'invalid channel input '+arr2str(chan)
        chan=chan(ok)
        message,'reading channel(s)  -> '+arr2str(chan),/contin
        message,'accumulation (secs) -> '+string(accum),/con,/nonam

        detsiz=[256,256,256,256,128,128,128,256]
        maxb=max(detsiz)
        nchan=n_elements(chan)
        ms_day=24l*3600l*1000l

;-- first and last day of SMM data

        tstart_smm='80/2/14'
        tend_smm='89/12/17'
        if n_elements(tstart) eq 0 then tstart=tstart_smm 
        if n_elements(tend) eq 0 then tend=tend_smm

;-- corrections

        if n_elements(notch) eq 0 then notch=1 
        ntch = 0 > notch < 1
        if ntch then message,'applying notch correction',/contin
        chk=1     ;-- checksum
        bltyp=0   ;-- queue data
        if ntch then ncut=2 else ncut=0
    
;-- find first record

        ex2int,anytim(tstart_smm,/ext),msod_s,ds79_s
        bda_bixrcs,ds79_s,msod_s,bltyp,chk,recno,ierr
        if ierr ne 0 then begin
         close,lunda & free_lun,lunda
         message,'no valid data in file'
        endif

;-- determine start time of data

        dgi=ixrec.dgi*64l
        statim=ixrec.blocktime
        staday=ixrec.blockday
        int2ex,statim,staday,td1
        start_time=anytim(td1,/atime)
        message,'data start time --> '+start_time(0),/contin

;-- did user enter date?

        tstart_stc=anytim(tstart,/stc)
        if tstart_stc.day eq 1 then tstart_stc.day=staday
        tend_stc=anytim(tend,/stc)
        if tend_stc.day eq 1 then tend_stc.day=staday

;-- update start and end times and move to start of data

        tstart=anytim(tstart_stc,/atime)
        tend=anytim(tend_stc,/atime)
        start_time=anytim(tstart_stc,/utime)
        end_tim=anytim(tend_stc,/utime)
        bda_bixrcs,tstart_stc.day,tstart_stc.time,bltyp,chk,recno,ierr

;-- start reading 

        nbyte=1664L
        ispec=0
        rec_tim=0
        prev_rec_tim=0
	end_of_file=0

;-- outer readloop

	while (not end_of_file) and (rec_tim le end_tim) do begin

;-- initialise accum and spectra
	
         rcnt=fltarr(nbyte)
         bcdata=bytarr(nbyte)
         new_accum=1
         rd_error=0
         sdgi=0.
         nspec=0
         start_fmt=ixrec.fmtid
 
;-- inner accumulation loop

         repeat begin

;-- check current record

          dgi=ixrec.dgi*64l
          int2ex,ixrec.blocktime,ixrec.blockday,td1
          statim=ixrec.blocktime-dgi/2
          staday=ixrec.blockday

;-- correct for midnite

          if statim lt 0 then begin
           statim=statim+ms_day
           staday=staday-1
          endif

          int2ex,statim,staday,td1
          rec_tim=anytim(td1,/utime)
          fmtid=ixrec.fmtid
          bsize=ixrec.blocksize

          if (not rd_error) and (dgi gt 0.) and (bsize gt 0) and $
             (rec_tim gt prev_rec_tim) and (fmtid eq start_fmt) then begin
           astart=anytim(td1,/atime)
           if new_accum then begin
            if verb then print,'-- accum start  '+astart(0)
            tindex.gen.time=statim
            tindex.gen.day=staday
            start_ixrec=ixrec
            new_accum=0
           endif
           bda_bcdabl,lunda, ixrec, bcdata, status					;read data block
	   bda_expand,1, rcnt, bcdata, ixrec.blocksize, status			;expand data
	   if (status gt 0) then message,'warning, expansion errors',/contin
           nspec=nspec+1
           sdgi = sdgi+float(dgi)/1000.
           end_ixrec=ixrec
           prev_rec_tim=rec_tim
          endif

;-- look at next record

          bda_nxtbdaixr,bltyp, chk, recno, ierr
          case ierr of

           0: rd_error=0       ;-- next record ok, accumulate it

           1: begin            ;-- end of file
               end_of_file=1
               rd_error=1
              end

           else: rd_error=1     ;-- possible mode change, don't accumulate it
          endcase

          next_fmt=ixrec.fmtid
          next_sdgi=sdgi+ixrec.dgi*.064

         endrep until (next_sdgi gt accum) or (end_of_file) or $
                      (rec_tim ge end_tim) or (next_fmt ne start_fmt)

;-- end accumulation loop

         if (next_fmt ne start_fmt) then begin
          message,'mode change during accumulation',/cont
         endif

;-- degroup accumulated spectra

         if sdgi gt 0 then begin
          berr=0 & derr=0                                             
          for i=0,nchan-1 do begin
	   k = chan(i)         	
	   bda_bcchfm,lunfmt, lunbin, start_ixrec, k, binlst, stcnt, numcnt, berr
           if stcnt ge n_elements(rcnt) then berr=1

           if (berr eq 0) then begin
            nbin=detsiz(k-1)
	    bda_degrp,binlst,start_fmt,rcnt(stcnt-1:*), k,nbin,buff_spectra,ntch,derr
            if (derr eq 0) then begin
             tindex.bsc.interval=(end_ixrec.blocktime-start_ixrec.blocktime)+$
                               32l*(end_ixrec.dgi+start_ixrec.dgi)+ $
                               ms_day*(end_ixrec.blockday-start_ixrec.blockday)

             tindex.bsc.actim=1000*sdgi
             tindex.bsc.nspec=nspec
             tindex.bsc.chan=k
             tindex.bsc.nstart=0
             tindex.bsc.nend=nbin-ncut-1
             tindex.bsc.datarectypes=dtype
             tindex.bsc.nbin=nbin-ncut
             tindex.bsc.total_cnts=total(buff_spectra(0:nbin-1-ncut))
             tdata.bin=-(1+indgen(nbin-ncut))
             tdata.counts=buff_spectra(0:nbin-1-ncut)
             if ispec lt max_spec then begin
              index(ispec)=tindex 
              data(ispec)=tdata
             endif else begin
              index=[temporary(index),tindex]
              data=[temporary(data),tdata]
             endelse
            endif
           endif
           if (berr eq 0) and (derr eq 0) then ispec=ispec+1
	  endfor
         endif 
      	endwhile               ;-- end read loop


;-- remove null spectra
     
        clook=where(index.bsc.nspec ne 0,count)
        if count gt 0 then begin
         index=index(clook)
         data=data(clook)
         ierr=0
         int2ex,index(count-1).gen.time,index(count-1).gen.day,td1
         end_tim=anytim(td1,/atime)
         message,'data end time --> '+end_tim(0),/contin
        endif else begin
         index=index(0)
         data=data(0)
         message,'no valid spectra present',/contin
         ierr=1
        endelse

;-- check for missing channels
       
        for i=0,n_elements(chan)-1 do begin
         clook=where(index.bsc.chan eq chan(i),count)
         if count eq 0 then begin
          message,'no spectra accumulated for channel '+string(chan(i)),/cont
          cindex.bsc.chan=chan(i)
          index=[index,cindex]
          data=[data,cdata]
         endif
        endfor
        clook=where(index.bsc.chan ne 0,count)
        if count ne 0 then begin
         index=index(clook)
         data=data(clook)
        endif

exit:
        
        close,lunbin,lunfmt,lunda
        free_lun,lunda,lunbin,lunfmt

	return
	end

