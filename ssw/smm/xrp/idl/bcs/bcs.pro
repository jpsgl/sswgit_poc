;+
; NAME:
;             BCS
; PURPOSE:
;             to provide GUI interface to BCS software 
; CATEGORY: 
;             widgets 
; CALLING SEQUENCE:
;             BCS
; RESTRICTIONS: 
;             needs X-windows 
; MODIFICATION HISTORY:
;             written Feb'92 by DMZ (ARC) 
;-

pro bcs_event, event                         ;BCS event handler

@bcs_com

;--initialize

ngang=[ [1,1],[1,2],[1,3],[1,4],[2,3],[2,3],[2,4],[2,4] ]
shard=0 & lhard=0 & read_file=0 
maxy=.9 & miny=.1

;-- get event type

widget_control, event.id, GET_UVALUE = USERVALUE
if (n_elements(uservalue) eq 0) then uservalue=''
wtype=widg_type(event.id)

;-- SLIDER widget?

if (wtype eq 'SLIDER') then begin
 accum=float(event.value)
 widget_control,comment,$
  set_value='SELECTED ACCUMULATION TIME: '+string(accum,'(i3)'),/append
 read_file=1 & plotlc=1
endif

;-- DRAW widget?
;--  save latest two cursor positions in plot range variables for later 
;--  zooming or fitting applications)

if (wtype eq 'DRAW') then begin

 if (event.press eq 0b) then return

 if (event.id eq ldraw) then begin      ;-- lightcurve window?

  wset,lval & !x=lx & !y=ly & !p=lp
  data_x=(float(event.x)/ld.x_vsize - lx.s(0))/lx.s(1)
  data_y=(float(event.y)/ld.y_vsize - ly.s(0))/ly.s(1)

  if log then ylim=10.^ly.crange else ylim=ly.crange
  if n_elements(lxrange) eq 0 then lxrange=[data_x,data_x] else begin
   if (lxrange(0) gt min(lx.crange)) and (lxrange(0) lt max(lx.crange)) then $
    oplot,[lxrange(0),lxrange(0)],ylim,color=0
   lxrange=[lxrange(1),data_x]
  endelse

  if n_elements(lyrange) eq 0 then lyrange=[data_y,data_y] else $
   lyrange=[lyrange(1),data_y]
  plotlc=0 & stime=data_x
  oplot,[data_x,data_x],ylim,linestyle=2
 endif

 if (event.id eq sdraw) then begin      ;-- spectrum window?
  mode='SPECT'
  wset,sval & !x=sx & !y=sy & !p=sp
  data_x=(float(event.x)/sd.x_vsize - sx.s(0))/sx.s(1)
  data_y=(float(event.y)/sd.y_vsize - sy.s(0))/sy.s(1)

;-- fitting and zooming limits

  if szoom or fit then begin

   if szoom then oplot,[data_x,data_x],[data_y,data_y],psym=1
   if fit then oplot,[data_x,data_x],sy.crange,linestyle=2

   if (n_elements(xb) ne 0) and (n_elements(yb) ne 0) then begin
    if szoom then oplot,xb,yb,color=0
    if fit then oplot,[xb(0),xb(0)],sy.crange,color=0
    if fit or szoom then begin
     oplot,[sx.crange(0),sx.crange(0)],sy.crange
     oplot,[sx.crange(1),sx.crange(1)],sy.crange
     oplot,sx.crange,[sy.crange(0),sy.crange(0)]
     oplot,sx.crange,[sy.crange(1),sy.crange(1)]
    endif
   endif

   ok=(sxrange(0) ne 0) and (sxrange(1) ne 0)

   if ok then begin
    syrange=[syrange(1),data_y] 
    sxrange=[sxrange(1),data_x] 
    xb=[sxrange(0),sxrange(1),sxrange(1),sxrange(0),sxrange(0)]
    zb=[syrange(1),syrange(1),syrange(0),syrange(0),syrange(1)]
    if szoom then oplot,xb,zb,linestyle=1
   endif else begin
    sxrange=[data_x,data_x] 
    syrange=[data_y,data_y]
   endelse

  endif
  return  
 endif

endif

;-- BUTTON widget?

if wtype eq  'BUTTON' then begin
 buttname=strupcase(strtrim(uservalue,2))

;-- prompt user for output filename if WRITE button was pressed

 find=where(buttname eq ['S_ASCII','S_XDR','L_ASCII','L_XDR'],count)
 if count ne 0 then begin     
  widget_control,wbase,/realize
  xmanager,'bcs',wbase,group_leader=base
  widget_control,wbase,show=1,map=1
  wopt=buttname & return
 endif

 case buttname of 

  'ACCUM':    begin
               read_file=1 & plotlc=1
              endif


  'ABORT':    begin
               if event.id eq wabort then widget_control,wbase,map=0
               if event.id eq eabort then widget_control,ebase,map=0
               return
              end

  'BLUE': begin
               if event.select eq 1 then begin
                fit=1 & fit_funct='fblue' & fixp=2 
               endif
              end

  'TYPE'  :   begin                              ;-- change directory location
               widget_control,ebase,/realize
               xmanager,'bcs',ebase,group_leader=base
               widget_control,elabel2,set_value='DEFAULT: '+cur_dir
               widget_control,ebase,show=1,map=1
               return
              end

  'DERIV'   : begin 
               diff=1 & plotlc=1 & log=0 
              end

  'BLUE2' : begin fit=1 & fit_funct='fblue2' & end


  'EBAR'    : begin
               if event.select eq 1 then ebar=1 else ebar=0
               plotlc=1 
              end
  'GAUSS'   : begin 
               if event.select eq 1 then begin
                fit=1 & fit_funct='gauss' & fixp=[1,2]
               endif
              end

  'HELP'    : begin                                        ;-- get help
               if event.select eq 1 then begin
                xdisplayfile,'HELP',text=bcs_help()
                return
               endif
              end

  'INTEG'   : begin 
               stime=lxrange & plotlc=0   ;-- integrate spectra
              end


  'LHARD'   : begin lhard=1 & plotlc=1 & end      ;-- hardcopy lightcurve

  'LDONE'   : begin      
               widget_control,lbase,show=0
               widget_control,base,show=1
               return
              end

  'LOG'     : begin                               ;-- log lightcurve
               if event.select eq 1 then log=1 else log=0
               plotlc=1
              end

  'LZOOM'   : begin                 ;-- zoom lightcurve
               plotlc=1
               wset,lval & !p=lp & !x=lx & !y=ly & lzoom=1
               if n_elements(lxrange) eq 2 then begin
                xz=[lxrange(0),lxrange(1),lxrange(1),lxrange(0),lxrange(0)]
                yz=[lyrange(1),lyrange(1),lyrange(0),lyrange(0),lyrange(1)]
                if log then yz=10.^yz  & oplot,xz,yz 
               endif else return
              end

;  'OVER'    : begin over=1 & return & end              ;-- overplot spectra

  'QUIT'    : goto, exit

  'REPLOT'  : begin
               if bname eq 'no files' then begin
                widget_control,set_value='PLEASE SELECT A FILE',flabel
                return
               endif
               if n_elements(index) eq 0 then read_file=1 else begin
                set_plot,'X' 
                plotlc=1 & read_file=0 & list_file=0
                lxrange=[0,0] & sxrange=[0,0] 
               endelse
              end

  'NOFIT'   : begin
               if event.select eq 1 then fit=0
              end
               
  'SBIN'    : begin                              ;-- double bin spectra
               if event.select eq 1 then sbin=2 else sbin=1
              end

  'SDA'     : begin                               ;-- examine SDA
               if sda_dir eq '' then begin
                widget_control,comment,set_value='BCS SDA UNAVAILABLE'
                widget_control,set_value='MAKE ANOTHER CHOICE',flabel
                return
               endif 
               twidget,sdate,group_leader=base
               if sdate.year le 1979 then yy='8*' else $
               yy=strmid(string(sdate.year,'(i4.2)'),2,2)
               if sdate.month le 0 then mm='*' else mm=string(sdate.month,'(i2.2)')
               if sdate.day le 0 then dd='*' else dd=string(sdate.day,'(i2.2)')
               fext=yy+mm+dd+'.*' 
               cur_dir=sda_dir
               mklog,'CUR_BCS',cur_dir & list_file=1 & plotlc=0
              end

  'SDONE'   : begin      
               widget_control,sbase,show=0
               widget_control,base,show=1
               return
              end

  'SHARD'   : if event.select eq 1 then shard=1          ;-- hard copy spectra

  'SZOOM'   : begin                         ;-- zoom on spectra

               if (n_elements(chans) gt 1) then begin
                widget_control,slabel,set_value='ZOOM WORKS ONLY IN SINGLE CHANNEL MODE'
                return
               endif
               widget_control,slabel,$
                set_value='USE CURSOR TO MARK DIAGONALLY OPPOSITE ENDS OF ZOOM REGION'
               widget_control,slabel,$
                set_value='PRESS "Apply" BUTTON TO PROCEED WITH ZOOM',/append
               widget_control,slabel,$
                set_value='PRESS "Reset" BUTTON TO RESET TO ORIGINAL',/append
               szoom=1 & fit=0 & sxrange=[0.,0] & syrange=sxrange & return
              end

  'SRESET'  : begin                         ;-- reset window
               szoom=0 & sxrange=[0.,0.] & syrange=sxrange & rd_spec=0 
              end
  
  'LRESET'  : begin                         ;-- reset window
               lzoom=0 & lxrange=[0.,0.] & lyrange=lxrange & rd_spec=0 
               plotlc=1 & read_file=0 & list_file=0
              end
  

  'APPLY'   : begin 
               if fit then syrange=[0.,0.]
              end


  'OS'     : begin                                   ;-- spawn to OS
               widget_control,base,show=0 & spawn
               widget_control,base,show=1 & return

              end

  'VOIGT'   : begin 
               if event.select eq 1 then begin 
                fit=1 & fit_funct='voigt' & fixp=[2,6]
               endif
              end

  'DVOIGT'   : begin 
               if event.select eq 1 then begin 
                fit=1 & fit_funct='dvoigt' & fixp=[2,6,10] 
               endif
              end

  'WAVE'    : begin 
               szoom=0 & sxrange=[0,0] 
               if event.select eq 1 then wave=1 else wave=0
              end

  else:       begin
               det=strpos(buttname,'D_')    ;-- channel button
               if det ge 0 then begin

                if (event.select eq 0) and (n_elements(chans) eq 1) then begin
                 widget_control,set_value='NEED AT LEAST ONE CHANNEL TO PLOT',flabel
                 chans=0 & return
                endif

                dname=fix(strmid(buttname,2,2))
                if (event.select) eq 0 then begin
                 find=where(dname ne chans,count)
                 if count gt 0 then chans=chans(find)
                endif else begin
                 find=where(dname eq chans,count)
                 if count eq 0 then chans=[chans,dname]
                endelse
                find=where(chans ne 0,count)
                if count gt 0 then chans=chans(find)
                widget_control,set_value='RESELECT ACCUMULATION TIME TO START READ',flabel
                lzoom=0 & szoom=0 
                return
               endif else begin
                widget_control,comment,set_value='SELECTED INVALID BUTTON',/append
                return
               endelse
              end
 endcase

 if (n_elements(chans) gt 1) and fit then begin
  widget_control,slabel,set_value='FIT WORKS ONLY IN SINGLE CHANNEL MODE'
  fit=0
  return
 endif

 if fit and (buttname ne 'APPLY') then begin             
  mess='USE CURSOR TO MARK LIMITS OF FIT'
  widget_control,slabel,set_value=mess
  widget_control,slabel,$
   set_value='PRESS "Apply" BUTTON TO PROCEED WITH FIT',/append
  szoom=0 & sxrange=[0.,0] & syrange=sxrange
  return
 endif

endif


;-- TEXT widgets

if (event.id eq etext) then begin                   ;-- change input directory
 widget_control,etext,get_value=def_dir,set_value=''
 def_dir=def_dir(0)
 if def_dir eq '' then def_dir=cur_dir else begin
  cur_log=chklog(def_dir)
  if cur_log ne '' then def_dir=cur_log
 endelse
 cur_dir=strtrim(def_dir,2)
 widget_control,comment,set_value='SELECTED: '+cur_dir,/append
 mklog,'CUR_BCS',cur_dir
 widget_control,ebase,map=0 & bname='no files' & fext='8*.*'
 list_file=1
endif

;-- list new files

if list_file then begin
 widget_control,flabel,set_value='SEARCHING...'
 files=loc_file(concat_dir(cur_dir,'bda'+fext),count=count)
 if count eq 0 then begin
  tfile='NO BDA FILES FOUND' & files='no files'
  widget_control,flist,set_value=tfile,set_uvalue=files
  widget_control,set_value='PLEASE CHANGE TO ANOTHER DIRECTORY LOCATION',flabel
  widget_control,comment,set_value=tfile+' IN '+strupcase(cur_dir),/append
  return 
 endif else begin
  break_file,files,dsk,direc,name,ext
  tfile=name+ext
 endelse

 widget_control,flist,set_value=tfile,set_uvalue=files
 plotlc=1 & list_file=0 & read_file=1
 if count gt 1 then begin
  widget_control,set_value='PLEASE SELECT A FILE',flabel & return
 endif else begin 
  bname=files(0)
  break_file,bname,dsk,direc,name,ext
  widget_control,comment,set_value=strupcase('SELECTED: '+name+ext),/append  
 endelse
endif

;-- LIST widgets

;-- get new BDA filename

if wtype eq 'LIST' then begin
 bname=strtrim(uservalue(event.index),2)
 if bname eq 'no files' then return
 break_file,bname,dsk,direc,name,ext
 widget_control,comment,set_value=strupcase('SELECTED: '+name+ext),/append  
 read_file=1 
endif

;-- get BDA file name

if read_file then begin
 if bname eq 'no files' then begin
  widget_control,set_value='PLEASE SELECT A FILE',flabel & return
 endif

;-- start reading

 ierr=0l
 if (min(chans) lt 1) or (max(chans) gt 8) then begin
  widget_control,flabel,set_value='PLEASE SELECT AT LEAST ONE CHANNEL'
  return
 endif

 widget_control,flabel,set_value='READING BDA FILE, PLEASE STAND BY....'
 widget_control,base,sensitive=0

 rdbda_smm,bname,index,data,chan=chans,accum=accum,ierr=ierr
 cok=where(index.bsc.nspec ne 0,count)
 if count gt 0 then begin
  index=index(cok) & data=data(cok)
  chans=gt_bsc_chan(index,/unique)
  ndet=n_elements(chans)
 endif else ierr=1

 lxrange=[0,0] & sxrange=[0,0]
 widget_control,base,sensitive=1
 if (ierr gt 0) then begin      ;-- I/O problems?
   widget_control,comment,set_value='ERROR READING: '+bname,/append
   widget_control,comment,set_value='ZERO DATA FOR REQUESTED CHANNEL',/append
   widget_control,set_value='PLEASE TRY ANOTHER FILE OR CHANNEL',flabel & return
 endif else begin
  plotlc=1 & read_file=0 & list_file=0 
 endelse
endif


;-- write data 

if (event.id eq wtext) then begin    
 widget_control,wtext,get_value=outfile,set_value=''
 outfile=outfile(0)
 if outfile eq '' then outfile='BCS.DMP'
 widget_control,comment,set_value='WRITING DATA TO '+outfile,/append
 case wopt of
   'S_ASCII': begin
               if (n_elements(w) ne 0) then begin     ;-- write spectrum
                wr_asc,outfile,slabels,w,z
               endif
              end
   'L_ASCII': begin
               if (n_elements(yb) ne 0) then begin  ;-- write lightcurve
                wr_asc,outfile,llabels,tb,yb
               endif
              end
   'S_XDR'  : begin
               if (n_elements(w) ne 0) then begin     ;-- write spectrum
                save,slabels,w,z,filename=outfile,/verbose,/xdr
               endif
              end
   'L_XDR'  :  if (n_elements(yb) ne 0) then begin     ;-- write lightcurve
                save,llabels,utbase,tb,yb,filename=outfile,/verbose,/xdr
               endif
   else     :  return
 endcase
  widget_control,wbase,map=0 & return
endif

;-- force user to select spectrum from lightcurve plot

if ndet eq 0 then begin
 widget_control,comment,set_value='PLEASE SELECT A CHANNEL'
 return
endif

if plotlc then begin
 if not lhard then begin
  widget_control,get_value=lval,ldraw    ;-- lightcurve window realized?
  if lval eq -1 then begin
   widget_control,lbase,/realize
   xmanager,'bcs',lbase,group_leader=base
   widget_control,get_value=lval,ldraw
  endif else widget_control,lbase,show=1
  wset,lval
 endif

;-- set plot parameters for lightcurve
 
 if n_elements(lp) ne 0 then begin
  !x=lx & !y=ly & !p=lp         
 endif
 set_utlabel,0
 !p.multi=0 & lgang=ngang(*,ndet-1)
 lxtitle='TIME (UT)'
 if diff then lytitle='COUNTS PER SEC^2' else lytitle='COUNTS PER SEC'

;-- get lightcurve arrays

 if lhard then set_hard

 for i=0,ndet-1 do begin
  chan=chans(i)
  clook=where((index.bsc.chan eq chan),count)
  tb=index(clook).gen.time/1000.
  interval=index(clook).bsc.interval/1000.
  times=tb
  dt=times(1:*)-times
  nok=where(dt lt 0,count)
  if count gt 0 then times(nok(0)+1:*)=times(nok(0)+1:*)+24*3600.
  sdgi=index(clook).bsc.actim/1000.
  yb=index(clook).bsc.total_cnts/sdgi
  utbase=atime((index(clook(0)).gen.day-1)*24.*3600.)
  utdate=anytim(utbase,/yymmdd,/date)

;-- plot options

  if log then yb=(yb > .01)
  if ebar then eb=sqrt(yb*sdgi)/sdgi else eb=0. 
  if diff then begin                    ;-- derivative
   yb=deriv(yb,tb) & eb=0.
  endif

;-- ranges

  lyrange=[0,0]
  if not lzoom then lxrange=[0,0] 

  dlabels=['DATE: '+utdate]
  elabels=['CHANNEL: '+string(chan,'(i1)'),'ION: '+ions(chan-1)]
  if i eq 0 then llabels=[dlabels,elabels] else llabels=[llabels,elabels]

  plotltc,tb,yb,utbase,ebar=eb,log=log,xrange=lxrange,ytitle=lytitle,$
     yrange=lyrange,xtitle=lxtitle,gang=lgang,/draw,$
     title=utdate+' '+ions(chan-1)+' ('+strtrim(string(accum,'(f6.1)'),2)+' s)',interval=interval
 endfor

 if not lhard then begin
  widget_control,llabel,set_value=llabels(*) 
  lx=!x & ly=!y & lp=!p & ld=!d
 endif else set_hard,/send

 widget_control,set_value='USE CURSOR TO SELECT TIME OF SPECTRUM',flabel
 plotlc=0 & return                         ;-- wait for user to select spectrum
endif 

;-- spectrum window realized?

if (not shard) then begin
 widget_control,get_value=sval,sdraw
 if sval eq -1 then begin     
  widget_control,sbase,/realize
  xmanager,'bcs',sbase,group_leader=base
  widget_control,get_value=sval,sdraw
 endif else widget_control,sbase,show=1
 wset,sval       
endif

;-- set plot parameters for spectra

if n_elements(sp) ne 0 then begin   
 !p=sp & !x=sx & !y=sy
endif 
if n_elements(sxrange) gt 1 then begin
 s=sort(sxrange) & sxrange=sxrange(s)
endif

;-- now plot spectra


dmin=abs(times-min(stime)) & dmax=abs(times-max(stime))
imin=where(dmin eq min(dmin),c1) & imax=where(dmax eq min(dmax),c2)
if (c1 gt 0) and (c2 gt 0) and (min(stime) ge min(times)) and $
 (max(stime) le max(times)) then begin 
 v=imin(0)+indgen(imax(0)-imin(0)+1)
 saccum=total(sdgi(v))
 sxtitle='BINS' & sytitle='COUNTS PER SEC PER BIN' 
 clook=where(index.bsc.chan eq chans(0))
 cindex=index(clook)
 dtime=anytim(cindex(imin(0)),/atime)
 slabels=[llabels,'MEAN TIME: '+strmid(dtime,9,100)]
 slabels=[slabels,'TOTAL ACCUMULATION: '+string(saccum,'(f7.2)')]
endif else begin
 widget_control,comment,set_value='SELECTED INVALID SPECTRUM',/append
 plotlc=1 & return
endelse

!p.multi=0 & sgang=ngang(*,ndet-1)
if shard then set_hard

for i=0,ndet-1 do begin
 clook=where(index.bsc.chan eq chans(i),count)
 spectra=data(clook).counts
 nbins=index(clook).bsc.nbin
 nbins=nbins(0)
 bins=data(clook).bin
 z=spectra(0:nbins-1,v)
 if n_elements(v) gt 1 then z=total(z,2)
 z=z/saccum
 w=bins(0:nbins-1,0)
 if wave then begin
  w=bda_wave(chans(i))
  w=w(0:nbins-1)
  sxtitle='ANGSTROM'
 endif
  
 if (not fit) or szoom then begin
  plotspec,w,z,xtitle=sxtitle,ytitle=sytitle,yrange=syrange,/draw,$
      over=over,bin=sbin,gang=sgang,xrange=sxrange,psym=10,$
      title=dtime(0)+' '+ions(chans(i)-1)+$
      ' ('+strtrim(string(saccum,'(f7.2)'),2)+' s)'
 endif

 if not shard then widget_control,slabel,set_value=slabels(*)

;-- fit spectra

 if fit then begin     
  if wave then damp=arock(chans(0)-1) else damp=arock(chans(0)-1)/adw(chans(0)-1)
  damp=damp/(2.*sqrt(alog(2.)))   ;--convert FWHM to 1/e width
  weights=replicate(1.,n_elements(z))
  ok=where(z gt 0.)
  weights(ok)=saccum/z(ok)
  fit_spec,w,z,fw,f,flabels=flabels,fit_funct=fit_funct,fxrange=sxrange,wave=wave,$
   damp=damp,disp=adw(chans(0)-1),weights=weights,fixp=fixp
  plotspec,fw,f,/draw,/over,psym=0
  widget_control,slabel,set_value=flabels(*)
  fit=0
 endif
endfor

if shard then set_hard,/send else begin
 sx=!x & sy=!y & sp=!p & sd=!d 
endelse
szoom=0

widget_control,set_value='PLEASE SELECT ANOTHER CHANNEL OR SPECTRUM TIME',flabel

return       ;-- return and wait for user to make another selection

exit: 

;xbackregister,'flash_bck',event.top,/unregister ;-- kill flashing widget

bda_files=''

widget_control,event.top,/destroy,/clear_events,bad_id=destroyed


return & end

;------------------------------------------------------------------------------

pro bcs, group_leader=group, just_reg=just_reg                ;-- widget creator

@bcs_com

set_plot,'X'
if (!d.flags and 65536) eq 0 then message,'widgets are unavailable'

 
if (n_elements(group) eq 0) and (not keyword_set(just_reg)) then begin
 widget_control,/reset,/clear_events
endif else begin
 if xregistered('bcs') then begin
  message,'BCS already registered',/contin
  return
 endif
endelse


welcome='WELCOME TO THE BCS DATA ANALYSIS WORKBENCH'

;-- some defaults

!p.multi=0
over=0 & lgang=0 
sgang=0 & fit=0 & diff=0 & stime=0
szoom=0 & lzoom=0 & lval=-1 & sval=-1 & sxrange=[0,0] & syrange=sxrange
bda_files=''

if n_elements(accum) eq 0 then accum=30.
if n_elements(chans) eq 0 then chans=0
if n_elements(bname) eq 0 then bname='no files'
if n_elements(slabels) eq 0 then slabels=''
if n_elements(llabels) eq 0 then llabels=''
wave=1

list_file=0 & plotlc=1 & read_file=0

;-- check definition of BCS_DATA; if undefined prompt user for data 

vms=(!version.os eq 'vms')
cd,current=def
sda_dir=chklog('BCS_DATA')
if (sda_dir eq '') then begin
 if vms then sda_dir='15837::SYS$DATA:[XRP.DATA.BDA...]'
endif
cur_dir=chklog('CUR_BCS')

case 1 of
 (cur_dir eq '') and (sda_dir eq '') : begin
   read,'* PLEASE ENTER DIRECTORY LOCATION OF YOUR DATA [DEF=CURRENT]: ',cur_dir
   if cur_dir eq '' then cur_dir=def
  end

  (sda_dir ne '') and (cur_dir eq ''): cur_dir=sda_dir

  else:cur_dir=cur_dir
endcase
mklog,'CUR_BCS',cur_dir


if n_elements(fext) eq 0 then begin
 if strupcase(cur_dir) eq strupcase(sda_dir) then fext='8*.*' else fext='8*.*'
endif

;-- parent widget

btitle='BCS DATA ANALYSIS WORKBENCH'
base = widget_base(TITLE =btitle, /column,/frame)

;-- top row of buttons

row1=widget_base(base,/row)


;-- quit button

main = widget_button(row1,value='Quit',uvalue='QUIT',/no_release,/frame)

;-- Disk window

cbutton=lonarr(1,3)
cbutton(0,0)=widget_button(row1,value='Change Directory',uvalue='CHANGE',$
                          /no_release,menu=2)
cbutton(0,1)=widget_button(cbutton(0,0),value='Type Directory Name',$
                           uvalue='TYPE',/no_release)
cbutton(0,2)=widget_button(cbutton(0,0),value='BCS Selected Data Archive', $
                           uvalue='SDA',/no_release)

;-- help button

help = widget_button(row1,value='Help',uvalue='HELP',/no_release)

;-- replot

replot = widget_button(row1,value='Replot Last File',uvalue='REPLOT',/no_release)


;-- OS button

osb=widget_button(row1,value='Spawn To OS',uvalue='OS',/no_release)

;-- 2nd row

row2=widget_base(base,/row)

;-- first column

r2c1=widget_base(row2,/column)

;-- flashing instruction label

temp=widget_base(r2c1,/column,/frame)
flabel=widget_text(temp,uvalue='BACKGROUND',value='',xsize=50)
widget_control,set_value='PLEASE MAKE YOUR SELECTION',flabel

;-- comment window

temp=widget_base(r2c1,/column,/frame)
 mlabel=widget_label(temp,value='MESSAGE WINDOW')
 comment=widget_text(temp,/scroll,ysize=10,xsize=50)

;-- column 1 in row 2

r2r2=widget_base(r2c1,/row)
col1=widget_base(r2r2,/column)

;-- channel buttons

bions = ['Ca XVIII-XIX','Fe K-alpha  ','Fe XVIII-XXI', $
	'Fe XXII-XXV ','Fe XXIII     ','Fe XXIV     ','Fe XXV (R)  ', $
	'Fe XXVI K-al']

detw=widget_base(col1,/column)
channels=strtrim(string(indgen(8)+1),2)
detb=lonarr(n_elements(channels))
xmenu,'CH '+channels+' - '+bions,detw,buttons=detb,/frame,/nonexclusive,$
 title='CHANNEL BUTTONS', /column,uvalue='D_'+strtrim(channels,2)

ndet=0
if n_elements(index) gt 0 then begin
 cok=where(index.bsc.nspec ne 0,count)
 if count gt 0 then begin
  index=index(cok) & data=data(cok)
  chans=gt_bsc_chan(index,/unique)
  ndet=n_elements(chans)
  for i=0,ndet-1 do begin
   widget_control,detb(chans(i)-1),set_button=1
  endfor
 endif
endif

;-- column 2 in row 2

col2=widget_base(r2r2,/column)

;-- file window

temp=widget_base(col2,/column,/frame)
 label=widget_label(temp,value='SELECT FROM THE FOLLOWING FILES')
 flist=widget_list(temp,ysize=10)


;-- integration time

junk=widget_base(col2,/column,/frame)
acc=widget_slider(junk,minimum=0,maximum=200,title='ACCUMULATION TIME (SECS)',$
                  value=accum)

temp=widget_base(junk,/column)
start=widget_button(temp,value='Accumulate/Display Lightcurve',uvalue='ACCUM',/no_release,/frame)

;-- list available files first time thru

widget_control,comment,set_value=welcome
widget_control,comment,set_value='CURRENT DIRECTORY: '+strupcase(cur_dir),/append
files=loc_file(concat_dir(cur_dir,'bda'+fext),count=count)
help,cur_dir
if count eq 0 then begin
 tfile='NO BDA FILES' & files='no files'
 widget_control,flist,set_value=tfile,set_uvalue=files
 widget_control,set_value='PLEASE CHANGE TO ANOTHER DIRECTORY LOCATION',flabel
 widget_control,comment,$
  set_value='There are no BDA files in the current directory',/append
endif else begin
 break_file,files,dsk,direc,name,ext
 tfile=name+ext
endelse 
widget_control,flist,set_value=tfile,set_uvalue=files

if bname eq 'no files' then begin               ;-- file in memory?
 widget_control,comment,set_value='Please proceed by selecting a filename',/append
 widget_control,set_value='PLEASE SELECT A FILE',flabel
endif else begin
 widget_control,comment,set_value='CURRENT BDA FILE: '+strupcase(bname),/append
endelse


widget_control,comment,$
 set_value='CURRENT ACCUMULATION TIME: '+string(accum,'(i3)'),/append


;-- change directory window

ebase=widget_base(title='  ',/column)
temp=widget_base(ebase,/column,/frame)
elabel=widget_label(temp,value='ENTER NEW DIRECTORY NAME')
elabel2=widget_label(temp,value='DEFAULT: '+cur_dir)
etext=widget_text(temp,/editable,ysize=1,xsize=20)
eabort=widget_button(ebase,uvalue='Abort',value='ABORT',/no_release)

;-- output file name window

wbase=widget_base(title='  ',/column)
temp=widget_base(wbase,/column,/frame)
wlabel=widget_label(temp,value='ENTER OUTPUT FILE NAME')
wlabel2=widget_label(temp,value='DEFAULT: BCS.DMP')
wtext=widget_text(temp,/editable,ysize=1,xsize=20)
wabort=widget_button(wbase,uvalue='Abort',value='ABORT',/no_release)

;-- lightcurve window

if n_elements(log) eq 0 then log=0
if n_elements(ebar) eq 0 then ebar=0

popts=['Log Scale','Oplot Errors'] & pvalues=['LOG','EBAR']

topts=['"Zoom Lightcurve" LZOOM',$
       '"Integrate Spectra" INTEG',$
       '"Reset Lightcurve" LRESET',$
       '"Hard Copy" LHARD',$
       '"Write Data" {', $
                       '"Write To ASCII File" L_ASCII',$
                       '"Write To IDL/XDR Saveset" L_XDR','}']




doneb=['Done','LDONE']

wobject,popts,pvalues,topts,doneb,lplotb,lbase,ldraw,llabel,tbase=ltbase,$
       xsize=400,ysize=400,$
       title='BCS LIGHTCURVE',pbase=lpbase

for i=0,n_elements(lplotb)-1 do $
 s=execute('widget_control,lplotb(i),set_button=('+pvalues(i)+' ne 0)')

;-- plot options

popts=['Double Bin Spectrum','Wavelength Axis']
sbin=0 & if n_elements(wave) eq 0 then wave=0
pvalues=['SBIN','WAVE']


;-- tools

topts=['"Zoom Spectrum" SZOOM',$
        '"Fit Options" {' , $
                '"Gauss" GAUSS',$
                '"Voigt " VOIGT',$
                '"Double Voigt " DVOIGT',$
                '"Blue Wing " BLUE',$
                '"No Fit" NOFIT','}',$
       '"Apply" APPLY',$
       '"Reset Spectrum" SRESET',$ 
       '"Hard Copy" SHARD',$
       '"Write Data" {', $
                       '"Write To ASCII File" S_ASCII',$
                       '"Write To IDL/XDR Saveset" S_XDR','}']


;-- done button

doneb=['Done', 'SDONE']

wobject,popts,pvalues,topts,doneb,splotb,sbase,sdraw,slabel,tbase=stbase,$
        xsize=400,ysize=400,$
        title='BCS SPECTRUM',pbase=spbase

for i=0,n_elements(splotb)-1 do $
 s=execute('widget_control,splotb(i),set_button=('+pvalues(i)+' ne 0)')

;tlabel=widget_label(temp,value='USE CURSOR TO SELECT TIME OF SPECTRUM')

;-- realize main widget

widget_control,base,/realize

;if xregistered('flash_bck') then xbackregister,'flash_bck',base,/unregister
xmanager,'bcs',base,group_leader=group,just_reg=just_reg

if keyword_set(just_reg) then $
 widget_control,flabel,set_value='PLEASE MAKE A SELECTION' else begin
 xmanager
 if n_elements(group) eq 0 then begin
  cleanplot & message,'cleaning up...',/contin
 endif
endelse

return & end
