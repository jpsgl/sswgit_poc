;+
; NAME:
;	gextal
; PURPOSE:
;	gets rocking widths and refectivities vs wavelength 
; CATEGORY:
;       I/O
; CALLING SEQUENCE:
;	gextal,ch,wave,rock,iref,ref
; INPUTS:
;	ch = required channel no.
; OPTIONAL INPUT PARAMETERS:
;       None
; OUTPUTS:
;	rock = rocking width in arcsec units (FWHM)
;       wave =  wavelength in angstroms
; OPTIONAL OUTPUT PARAMETERS:
;       iref = integrated reflectivity (10-5 radians)
;       ref  = peak reflectivity (%)
; SIDE EFFECTS:
;       None
; RESTRICTIONS:
;       Ensure CRYSTAL logical is defined
; PROCEDURE:
;	Uses simple open and read commands. Data in CRYSTAL stored
;       in columns of increasing wavelength. Different channels are
;       blocked in order of decreasing wavelength.
; MODIFICATION HISTORY:
;	DMZ, ARC, July 11 1986.
;- 

 pro gextal,ch,wave,rock,iref,ref

;-- check that CRYSTAL logical defined

 crystal=loc_file('CRYSTAL',count=nf)
 if nf eq 0 then begin
  crystal=loc_file('crystal.dat',path=get_lib(),count=nf)
  if nf eq 0 then begin
   message,'FCS CRYSTAL file not found',/cont
   repeat begin
    crystal=''
    read,'* enter CRYSTAL file name with full directory specification: ',crystal
   endrep until crystal ne ''
  endif
  mklog,'CRYSTAL',crystal
 endif

;-- can't use EOF test across a network

 is_node=(strpos(chklog(crystal),'::') gt 0)

 if (ch lt 1) or (ch gt 7) then return

 conv=2.06                          ;arcsec per 10-5 radians
 openr,lun,crystal,/get_lun                  ;open data file
 row=fltarr(4) & rock=fltarr(1000) & wave=rock & iref=rock & ref=iref

 j=-1 & k=1 & wprev=0.

 on_ioerror,quit
 forever=0
 repeat begin 
  readf,lun,row
  if wprev gt row(0) then k=k+1     ;check for end of channel
  if k eq ch then begin
   j=j+1 
   wave(j)=row(0) & rock(j)=row(1) & ref(j)=row(2) & iref(j)=row(3)
  endif
  wprev=row(0)
  if is_node then ok_exit=0 else ok_exit=eof(lun)
 endrep until (forever) or (ok_exit)

 quit:on_ioerror,null
 rock=rock(0:j) & wave=wave(0:j) & iref=iref(0:j)/conv
 ref=ref(0:j)
 close,lun & free_lun,lun
 return
 end
