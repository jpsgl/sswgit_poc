;+
; NAME:
;             FCS 
; PURPOSE:
;             to provide GUI interface to FCS software 
; CATEGORY: 
;             widgets 
; CALLING SEQUENCE:
;             FCS
; RESTRICTIONS: 
;             needs X-windows
; MODIFICATION HISTORY:
;             written March'92 by DMZ (ARC) 
;             made UNIX compatible Jan'93 (DMZ)
;-

pro fcs_event, event                         ;FCS event handler

@fcs_com

                        
;-- FCS ion information

hp_list=['O VIII','Ne IX','Mg XI','Si XIII', 'S XV', 'Ca XIX','Fe XXV','WHITE']
add_list=[2304,6361,847,1543]
ion_list=['Fe XVII','HOME','Ne IX','Fe XVIII']

;--disable hardcopy button

ihard=0 & shard=0 

;-- get event type

widget_control, event.id, GET_UVALUE = USERVALUE
if (n_elements(uservalue) eq 0) then uservalue=''
wtype=widg_type(event.id)


;-- DRAW widget?
;--  save latest two cursor positions in plot range variables for later 
;--  zooming or fitting applications)

if (wtype eq 'DRAW') then begin

 if (event.press eq 0b) or (event.id eq idraw) then return

 if (event.id eq sdraw) then begin      ;-- spectrum window?
  mode='SPECT'
  wset,sval & !x=sx & !y=sy & !p=sp
  data_x=(float(event.x)/sd.x_vsize - sx.s(0))/sx.s(1)
  data_y=(float(event.y)/sd.y_vsize - sy.s(0))/sy.s(1)

;-- fitting and zooming limits

  if szoom or fit then begin

   if szoom then oplot,[data_x,data_x],[data_y,data_y],psym=1
   if fit then oplot,[data_x,data_x],sy.crange,linestyle=2

   if (n_elements(xb) ne 0) and (n_elements(yb) ne 0) then begin
    if szoom then oplot,xb,yb,color=0
    if fit then oplot,[xb(0),xb(0)],sy.crange,color=0
   endif

   oplot,[sx.crange(0),sx.crange(0)],sy.crange
   oplot,[sx.crange(1),sx.crange(1)],sy.crange
   oplot,sx.crange,[sy.crange(0),sy.crange(0)]
   oplot,sx.crange,[sy.crange(1),sy.crange(1)]


   ok=(sxrange(0) ne 0) and (sxrange(1) ne 0)

   if ok then begin
    syrange=[syrange(1),data_y] 
    sxrange=[sxrange(1),data_x]
    xb=[sxrange(0),sxrange(1),sxrange(1),sxrange(0),sxrange(0)]
    yb=[syrange(1),syrange(1),syrange(0),syrange(0),syrange(1)]
    if szoom then oplot,xb,yb,linestyle=1
   endif else begin
    sxrange=[data_x,data_x] 
    syrange=[data_y,data_y]
   endelse

  endif
   
 endif
 return
endif

;-- BUTTON widget?

if wtype eq  'BUTTON' then begin
 buttname=strupcase(strtrim(uservalue,2))

;-- prompt user for output filename if WRITE button was pressed

 find=where(buttname eq ['S_ASCII','S_XDR','I_ASCII','I_XDR'],count)
 if count ne 0 then begin     
  widget_control,wbase,/realize
  xmanager,'fcs',wbase,group_leader=base
  widget_control,wbase,show=1,map=1
  wopt=buttname & return
 endif

 case buttname of 


  'ABORT':    begin
               if event.id eq wabort then widget_control,wbase,map=0
               if event.id eq eabort then widget_control,ebase,map=0
               return
              end

  'ADD':      begin
               mode='SPECT' 
               if event.select eq 0 then add=0 else add=1 
               return 
              end

  'ADJUST'  : begin                          ;-- adjust color table
               mode='RAST'
               if event.select eq 1 then begin
                xloadct,group=base 
                color=1
                return 
               endif
              end

  'BKG'     : begin
               mode='RAST'
               if event.select eq 1 then bkg=1 else bkg=0
               rd_img=1
              end

  'BLUE':    begin
               mode='SPECT' & fit=1 & fit_funct='fblue' & fixp=2
              end


  'DVOIGT'   : begin
               if event.select eq 1 then begin
                fit=1 & fit_funct='dvoigt' & fixp=[2,6,10]
               endif
              end

  'BW'      : begin
               mode='RAST' & color=0 & & ihard=1 
              end

  'CHECK'   : begin
               mode='RAST'
               widget_control,flabel,set_value='CHECKING S/C POINTING, STANDBY...'
               chkpoint,img,info          ;-- check image pointing

               if info ne '' then widget_control,comment,set_value=info,/append
              end

  'UVSP'    : begin
               mode='RAST'
               find_uvexp,img.time,expno,info=info,err=err,/image
               widget_control,comment,set_value=info,/append
               if err then return
               get_uvsp,uvimg,expno,err=err,info=info,/sum
               widget_control,comment,set_value=info,/append
               if err then return
               if not iover then $
                get_wind,windex,xsize=400,ysize=400,/free else windex=ival
               if iover then begin
                uvimg.xp=uvimg.xp-uvimg.yaw+img.yaw
                uvimg.yp=uvimg.yp-uvimg.pitch+img.pitch
               endif
               plotmap,uvimg,cont=cont,color=color,grid=grid,$
                hard=ihard,gang=igang,plabels=ilabels,over=iover,bin=ibin,$
                log=log,draw=iover,/glabel,windex=windex
               return
              end

  'HXIS'    : begin
               widget_control,comment,set_value='Option not yet implemented',/append
               return
              end

  'COLOR'   : begin 
               mode='RAST' & color=1 & ihard=1 
              end

  'CONT'    : begin
               mode='RAST'
               if event.select eq 1 then cont=1 else cont=0
              end
 
  'IDONE'    : begin                ;-- hide image widget
                widget_control,ibase,show=0
                widget_control,base,show=1
                return
               end

  'FLUX'    : begin 
               mode='SPECT' & rd_spec=1
               if event.select eq 1 then flux=1 else flux=0
               syrange=[0,0]
              end

  'GAUSS'   : begin
               mode='SPECT' & fit=1 & fit_funct='gauss' & fixp=[1,2]
              end

  'GRID'    : begin                         ;-- plot heliographic grid
               mode='RAST'
               if event.select eq 1 then grid=10 else grid=0
              end
 
  'HELP'    : begin                              ;-- get help
               if event.select eq 1 then begin
                xdisplayfile,'HELP',text=fcs_help()
                return
               endif
              end

  'IGANG'   : begin                               ;-- gang plots
               mode='RAST'
               if event.select eq 1 then igang=[2,2] else igang=0
               return
              end

  'IBIN'    : begin                         ;-- double bin image
               mode='RAST'
               if event.select eq 1 then ibin=2 else ibin=0
              end

  'IOVER'   : begin                           ;-- overplot 
               mode='RAST' & last_cont=cont 
               if event.select eq 1 then iover=1 else iover=0
               return
              end

  'LOG'     : begin
               mode='RAST' & if event.select eq 1 then log=1 else log=0
              end

  'QUIT'    : goto, exit

  'APPLY'   : begin 
               mode='SPECT' & rd_spec=0 
               if fit then syrange=[0.,0.]
              end

  'RAST'    : begin                            ;-- plot images
               if (mode eq 'RAST') and (event.select eq 0) then begin
                widget_control,detb(7),sensitive=0,set_value='CH 8 - '
                widget_control,modeb(0),set_button=1 & return
               endif
               widget_control,modeb(1),set_button=0 & mode='RAST'
               widget_control,detb(7),sensitive=1,set_value='CH 8 - WHITE'
               listmode=1 
              end

  'SAVE'    : begin
               if n_elements(ival) ne 0 then begin
                wset,ival
                widget_control,flabel,$
                 set_value='SAVING WINDOW, PLEASE STAND BY.......'
                tvgrab,/color & notice=''
               endif else notice='NO WINDOW ACTIVE'
               widget_control,comment,set_value=notice,/append
               return
              end


  'SBIN'    : begin                          ;-- double bin spectra
               mode='SPECT' 
               if event.select eq 1 then sbin=2 else sbin=0
              end

  'SDA'     : begin                               ;-- examine SDA
               if sda_dir eq '' then begin
                widget_control,comment,set_value='FCS SDA UNAVAILABLE'
                widget_control,set_value='MAKE ANOTHER CHOICE',flabel
                return
               endif 
               twidget,sdate,group_leader=base
               if sdate.year le 1979 then yy='8*' else $
                yy=strmid(string(sdate.year,'(i4.2)'),2,2)
               if sdate.month le 0 then mm='*' else mm=string(sdate.month,'(i2.2)')
               if sdate.day le 0 then dd='*' else dd=string(sdate.day,'(i2.2)')
               fext=yy+mm+dd+'.*' 
               cur_dir=sda_dir
               mklog,'CUR_FCS',cur_dir & listfile=1 
              end

  'NOFIT'   : begin
               mode='SPECT'
               if event.select eq 1 then fit=0
              end

  'SDONE'   : begin                ;-- hide spectrum widget
               widget_control,sbase,show=0
               widget_control,base,show=1
               return
              end

  'SGANG'   : begin                               ;-- gang plots
               mode='SPECT'
               if event.select eq 1 then sgang=[2,2] else sgang=0
               return
              end

  'SHARD'   : begin
               mode='SPECT' & shard=1                       ;-- hard copy spectra
              end

  'SPECT'   : begin                             ;-- plot spectra
               if (mode eq 'SPECT') and (event.select eq 0) then begin
                widget_control,detb(7),sensitive=1,set_value='CH 8 - WHITE'
                widget_control,modeb(1),set_button=1 & return
               endif
               widget_control,detb(7),sensitive=1,set_value='CH 8 -'
               widget_control,modeb(0),set_button=0
               widget_control,detb(7),sensitive=0
               mode='SPECT' & listmode=1 
              end

  'SOVER'   : begin                           ;-- overplot 
               mode='SPECT'
               if event.select eq 1 then sover=1 else sover=0 
               return
              end

  'SZOOM'   : begin                         ;-- zoom on spectra
               rd_spec=0 & mode='SPECT'
               widget_control,slabel,$
                set_value='USE CURSOR TO MARK DIAGONALLY OPPOSITE ENDS OF ZOOM REGION'
               widget_control,slabel,$
                set_value='PRESS "Apply" BUTTON TO PROCEED WITH ZOOM',/append
               widget_control,slabel,$
                set_value='PRESS "Reset" BUTTON TO RESET TO ORIGINAL',/append
               szoom=1 & fit=0 & sxrange=[0.,0] & syrange=sxrange & return
              end

  'SRESET'  : begin                         ;-- reset window
               mode='SPECT'
               szoom=0 & sxrange=[0.,0.] & syrange=sxrange & rd_spec=0 
              end

  'OS'     :  begin                              ;-- spawn to OS
               widget_control,base,show=0 & spawn
               widget_control,base,show=1 & return
              end

  'TYPE'  :   begin                              ;-- change directory location
               widget_control,ebase,/realize
               xmanager,'fcs',ebase,group_leader=base
               widget_control,elabel2,set_value='DEFAULT: '+cur_dir
               widget_control,ebase,show=1,map=1
               return
              end


  'VOIGT'   : begin
               mode='SPECT' & fit=1 & fit_funct='voigt' & fixp=[2,6]
              end

  'WAVE'    : begin
               mode='SPECT'
               if event.select eq 1 then wave=1 else wave=0
               sxrange=[0.,0.] & rd_spec=1 
              end

  else:       begin
               if event.select eq 0 then return
               det=strpos(buttname,'D_')    ;-- channel button
               if det ge 0 then begin
                dname=strmid(buttname,2,2) 
                if (fix(dname) eq detno) and (detno ne 0) then $ 
                  widget_control,detb(detno-1),set_button=1
                detno=fix(dname) 
                szoom=0 
                if mode eq 'RAST' then begin
                 rd_img=1 & rd_spec=0
                endif else begin
                 rd_spec=1 & rd_img=0
                endelse
                syrange=[0,0] & sxrange=syrange
                if rd_spec and (datatype(sdb) ne 'STC') then begin
                 widget_control,flabel,set_value='PLEASE SELECT A SPECTRAL MODE'
                 return
                endif
                if rd_img and (datatype(idb) ne 'STC') then begin
                 widget_control,flabel,set_value='PLEASE SELECT A RASTER MODE'
                 return
                endif
               endif else begin
                widget_control,comment,set_value='SELECTED INVALID BUTTON',/append
                return
               endelse
               listchan=0
              end
 endcase
 
 if fit and (buttname ne 'APPLY') then begin             
  mess='USE CURSOR TO MARK LIMITS OF FIT'
  widget_control,slabel,set_value=mess
  widget_control,slabel,$
   set_value='PRESS "Apply" BUTTON TO PROCEED WITH FIT',/append
  szoom=0 & sxrange=[0.,0] & syrange=sxrange
  return
 endif

 
endif

;-- take care of LIST widgets

if wtype eq 'LIST' then begin

 ename=uservalue(event.index) & prefix=strmid(ename,0,4) & len=strlen(ename)
 suffix=strmid(ename,4,len-4)
 widget_control,modeb(0),sensitive=1
 widget_control,modeb(1),sensitive=1

;-- get new fis filename and read it

 if prefix eq 'FILE' then begin 
        
  if suffix eq 'NO FILES' then return
  sover=0 & add=0 & iover=0 & szoom=0
  fdecomp,suffix,dsk,direc,name,ext
  widget_control,comment,set_value='SELECTED: '+name+'.'+ext,/append  

  for i=0,n_elements(detb)-1 do widget_control,detb(i),sensitive=0
  widget_control,flabel,set_value='READING FIS HEADER, PLEASE STAND BY....'
  fname=suffix 
  if n_elements(filunit) ne 0 then begin  ;-- close old file
   close,filunit & free_lun,filunit
  endif
  openr,filunit,fname,64,/get_lun         ;-- open new file

  rdmap,fname,modes,times                 ;-- read modes
  if modes(0) eq 'NO MODES' then begin    ;-- I/O problems?
   widget_control,comment,set_value='ERROR READING '+fname,/append
   widget_control,set_value='PLEASE SELECT ANOTHER FILE',flabel & return
  endif
  listmode=1 & if mode eq 'RAST' then rd_img=1 else rd_spec=1        
  widget_control,set_value='PLEASE SELECT IMAGE OR SPECTRAL MODE TYPE',flabel
 endif

;-- read fis data block 

 if prefix eq 'TIME' then begin
  if suffix eq 'NO MODES' then begin blk=-1 & return & endif
  widget_control,comment,set_value='READING BLOCK NO: '+suffix,/append
  blk=fix(suffix)+1
  on_ioerror,retry
  widget_control,flabel,set_value='READING FIS FILE, PLEASE STAND BY....'
  getfisblk, filunit, fileheader, sdb, idb, roadmap,blk
  on_ioerror,null
  if mode eq 'RAST' then chan=idb.idx.whichpara else chan=sdb.idx.whichpara
  on=where(chan ne 0,count)
  if count eq 0 then begin
retry:
   widget_control,comment,set_value='ERROR READING '+fname,/append
   widget_control,set_value='PLEASE SELECT ANOTHER MODE TYPE',flabel & return
  endif
  listmode=0 & listchan=1 & if mode eq 'RAST' then rd_img=1 else rd_spec=1
 endif
endif

;-- TEXT widgets

if wtype eq 'TEXT' then begin

 if (event.id eq etext) then begin                   ;-- change input directory
  widget_control,etext,get_value=def_dir,set_value=''
  def_dir=def_dir(0)
  if def_dir eq '' then def_dir=cur_dir else begin
   cur_log=chklog(def_dir)
   if cur_log ne '' then def_dir=cur_log
  endelse
  cur_dir=strtrim(def_dir,2)
  widget_control,comment,set_value='SELECTED: '+cur_dir,/append
  mklog,'CUR_FCS',cur_dir
  widget_control,ebase,map=0 & fname='NO FILES' & fext='8*.*'
  listfile=1
 endif

 if (event.id eq wtext) then begin                  ;-- write data 
  widget_control,wtext,get_value=outfile,set_value=''
  outfile=outfile(0)
  if outfile eq '' then outfile='fcs.dmp'
  widget_control,comment,set_value='WRITING DATA TO '+outfile,/append

  if (mode eq 'RAST') and (n_elements(img) ne 0) then begin   ;-- unpack image
   rust,img,idata,xp,yp,dx,dy,xc,yc
   rust,bimg,bdata
   center=' XC: '+string(xc,'(f7.2)')+' YC: '+string(yc,'(f7.2)')
  endif

  case wopt of
   'I_ASCII' : begin
                if (n_elements(idata) ne 0) then begin       ;-- write image
                 wr_asc,outfile,[ilabels(*),center],idata 
                 wr_asc,outfile,'BACKGROUND',bdata,/append 
                endif
               end
   'S_ASCII' : begin
                if (n_elements(x) ne 0) then begin     ;-- write spectrum
                 wr_asc,outfile,slabels(*),x,y
                endif
               end
   'I_XDR'   : begin
                if (n_elements(idata) ne 0) then begin  ;-- write image
                 save,ilabels,center,img,bimg,filename=outfile,/verbose,/xdr
                endif
               end
   'S_XDR'   : begin
                if (n_elements(x) ne 0) then begin     ;-- write spectrum
                 save,slabels,x,y,filename=outfile,/verbose,/xdr
                endif
               end
   else      : return
  endcase
  widget_control,wbase,map=0 & return
 endif

endif

;-- list files

if listfile then begin
 widget_control,flabel,set_value='SEARCHING...'
 widget_control,tlist,set_value=' ',set_uvalue='NO MODES'
 widget_control,tlist,sensitive=0
 thefiles=concat_dir(cur_dir,'fis'+fext)
 files=loc_file(thefiles,count=count)
 if count eq 0 then begin
  tfile='NO FIS FILES FOUND' & files='NO FILES'
  widget_control,flist,set_value=tfile,set_uvalue='FILE'+files
  widget_control,set_value='PLEASE CHANGE TO ANOTHER DIRECTORY',flabel
  widget_control,comment,set_value=tfile,/append
  widget_control,comment,set_value='IN '+strupcase(cur_dir),/append
  return 
 endif else begin
  tfile=strarr(count)
  for i=0,count-1 do begin
   fdecomp,files(i),dsk,direc,name,ext
   tfile(i)=name+'.'+ext
  endfor
 endelse
 widget_control,flist,set_value=tfile,set_uvalue='FILE'+files
 listfile=0 & listmode=1 & if mode eq 'RAST' then rd_img=1 else rd_spec=1
 widget_control,set_value='PLEASE SELECT A FILE',flabel & return
endif

;-- initialize plot labels

if  listchan then begin
 ilabels='' & slabels=''
endif

;-- list available modes 

if listmode then begin

 if mode eq '' then begin
  widget_control,set_value='PLEASE SELECT IMAGE OR SPECTRAL MODE TYPE',flabel & listmode=1 
  for i=0,1 do widget_control,modeb(i),sensitive=1
  return
 endif

 if mode eq 'RAST' then find=where(modes eq mode,mcount) else $
  find=where(modes ne 'RAST',mcount)

 if (mcount eq 0) or ( modes(0) eq 'NO MODES') then begin
  widget_control,set_value='PLEASE SELECT ANOTHER FILE OR MODE TYPE',flabel
  value=['NO MODES OF TYPE: '+mode+' IN FILE'] & uvalue=['TIME'+'NO MODES']
  widget_control,tlist,set_value=value,set_uvalue=uvalue
  return
 endif else begin
  ntimes=n_elements(times) & blks=sindgen(ntimes)
  mtimes=times(find) & mblks=blks(find)
  value=mtimes+'   '+modes(find) & uvalue='TIME'+mblks
  widget_control,tlist,sensitive=1
  widget_control,set_value='PLEASE SELECT A TIME ',flabel
  widget_control,tlabel,set_value='SELECT FROM THESE FCS TIMES'
  listmode=0
  widget_control,tlist,set_value=value,set_uvalue=uvalue
  return
 endelse

endif

;-- list available channels

if listchan then begin
 if mode eq 'RAST' then temp=idb.idx else temp=sdb.idx
 chans=fix(temp.whichpara) & on=where(chans ne 0,count) 
 chans=chans(on)
 xtal=temp.cryadd & if xtal eq 0 then xtal=(temp.cryadd > temp.cryaddnexton)
 diff=abs(xtal-add_list)
 add_val=where(diff eq min(diff),acount)
 if (acount ne 1) or (xtal eq 0.) then ion='?' else ion=(ion_list(add_val))(0)

;-- identify ions

 ions=replicate('?',n_elements(chans))
 for k=0,n_elements(chans)-1 do begin
  if ion eq 'HOME' then ions(k)=hp_list(chans(k)-1) else ions(0)=ion
 endfor
 channels='CH '+strtrim(string(indgen(8)+1),2)
 values=strarr(8)
 for i=0,6 do begin
  find=where( (i+1)  eq chans,count)
  if count ne 0 then values(i)=ions(find(0)) 
 endfor
 if detno ne 0 then widget_control,detb(detno-1),set_button=0

;-- turn-on active channels

 if mode eq 'RAST' then values(7)='WHITE' 
 for i=0,7 do begin
  if values(i) ne cbuff(i) then begin
    widget_control,detb(i),set_value=channels(i)+' - '+values(i)
  endif
  widget_control,detb(i),sensitive=(strtrim(values(i),2) ne '')
 endfor
 cbuff=values
 widget_control,set_value='PLEASE SELECT A CHANNEL',flabel
 listchan=0 & return
endif

;--at last plot the data

if detno ne 0 then begin

;-- plot FCS raster

 if (mode eq 'RAST') then begin

;-- image window realized?

  if not ihard then begin
   widget_control,get_value=ival,idraw
   if ival eq -1 then begin 
    widget_control,ibase,/realize
    xmanager,'fcs',ibase,group_leader=base
    widget_control,get_value=ival,idraw
   endif else widget_control,ibase,show=1
   wset,ival 
  endif

;-- read image

  if rd_img and (not ihard) then begin
   rdfis_img,idb,detno,img,bkg,bimg
   rd_img=0              
  endif 

;-- don't read again until new mode or new chan
                                    
  rust,img,idata
  if max(idata) eq 0 then begin
   zm='ZERO DATA AT '+img.time
   widget_control,comment,set_value=zm,/append
   widget_control,ilabel,set_value='  '
   erase & xyouts,.1,.5,zm,/norm,size=2 & return
  endif else begin

;-- restore plot system variables 

   if n_elements(ip) ne 0 then begin
    !x=ix & !y=iy & !p=ip   
   endif

   plotmap,img,cont=cont,color=color,grid=grid,hard=ihard,gang=igang,$
    plabels=ilabels,over=iover,bin=ibin,log=log,/draw,/glabel,windex=ival
   if not ihard then widget_control,ilabel,set_value=ilabels(*) 

;-- save plot system variables 

   ix=!x & iy=!y & ip=!p  

  endelse
 endif

;-- plot FCS spectra

 if (mode eq 'SPECT') then begin

  if wave then xtitle='ANGSTROM' else xtitle='ADDRESS'
  if flux then ytitle='PH CM-2 S-1 A-1' else ytitle='COUNTS PER SEC PER ADDRESS'
  if rd_spec and (not shard) then begin
   rdfis_spec,sdb(0),detno,x,y,step,wave=wave,plabels=slabels,flux=flux,ey=ey
   rd_spec=0
  endif

 ;-- spectrum window realized?

  if not shard then begin
   widget_control,get_value=sval,sdraw
   if sval eq -1 then begin     
    widget_control,sbase,/realize
    xmanager,'fcs',sbase,group_leader=base
    widget_control,get_value=sval,sdraw
   endif else widget_control,sbase,show=1
   wset,sval 
  endif

;-- restore plot system variables 
  
  if (n_elements(sp) ne 0) then begin
   !x=sx & !y=sy & !p=sp  
  endif

;-- plot spectra

  if shard then set_hard
  mtitle=slabels(0)
  if (not fit) or szoom then begin
   plotspec,x,y,xtitle=xtitle,ytitle=ytitle,$
       over=sover,bin=sbin,gang=sgang,xrange=sxrange,$
       yrange=syrange,title=mtitle,/draw,psym=10
   if not shard then widget_control,slabel,set_value=slabels(*) 
  endif

;-- fit spectra

  if fit then begin

;-- get disp and damp parameters

   rdfis_spec,sdb(0),detno,address
   cfac=fluxcon(detno,address,disp,drock)
   if wave then xtemp=x else xtemp=address
   find=where((xtemp ge min(sxrange)) and (xtemp le max(sxrange)),good)
   if good gt 0 then begin 
    disper=total(disp(find))/good & damp=total(drock(find))/good
   endif else begin 
    widget_control,slabel,set_value='NO VALID DATA POINTS TO FIT'
    return
   endelse
   if wave then damp=damp*disper
   damp=damp/(2.*sqrt(alog(2.)))   ;--convert FWHM to 1/e width
   weights=replicate(1.,n_elements(y))
   ok=where(ey gt 0.)
   weights(ok)=1./ey(ok)^2
   fit_spec,x,y,fx,f,fit_funct=fit_funct,weights=weights,$
    damp=damp,disp=disper,flabels=fslabels,fxrange=sxrange,wave=wave,flux=flux
   plotspec,fx,f,/over,/draw,psym=0
   fit=0    
   if not shard then widget_control,slabel,set_value=fslabels(*)
  endif

;-- save latest plot system variables 

  if shard then set_hard,slabels,/send else begin
   sx=!x & sy=!y & sp=!p & sd=!d 
  endelse
  szoom=0

 endif
 widget_control,set_value='SELECT ANOTHER FILE, TIME, CHANNEL, OR PLOT OPTION',flabel
endif

return       ;-- return and wait for user to make another selection

exit:

if n_elements(filunit) ne 0 then free_lun,filunit
;xbackregister,'flash_bck',event.top,/unregister
widget_control,event.top,/destroy,/clear_events,bad_id=destroyed
if n_elements(windex) ne 0 then wdelete,windex
return & end

;------------------------------------------------------------------------------

pro fcs, group_leader=group, just_reg=just_reg                ;-- widget creator

@fcs_com

set_plot,'X'
if (!d.flags and 65536) eq 0 then message,'widgets are unavailable'

if (n_elements(group) eq 0) and (not keyword_set(just_reg)) then begin
 if n_elements(filunit) ne 0 then begin
  close,filunit & free_lun,filunit
 endif  
 widget_control,/reset,/clear_events 
endif else begin
 if xregistered('fcs') then begin
  message,'FCS already registered',/contin
  return
 endif
endelse

;-- autosize screen

welcome='WELCOME TO THE FCS DATA ANALYSIS WORKBENCH'

;-- some defaults

!p.multi=0 
iover=0 & color=0  & sover=0
sgang=0 & igang=0 & fit=0 & add=0 & szoom=0 & rd_img=1 & rd_spec=1
cbuff=strarr(8)
sxrange=[0.,0] & syrange=sxrange
cont=0

if n_elements(fit_funct) eq 0 then fit_funct=''
if n_elements(log) eq 0 then log=0
if n_elements(sbin) eq 0 then sbin=0
if n_elements(ibin) eq 0 then ibin=0
if n_elements(grid) eq 0 then grid=10
if n_elements(bkg) eq 0 then bkg=0
if n_elements(flux) eq 0 then flux=0
if n_elements(wave) eq 0 then wave=1
if n_elements(detno) eq 0 then detno=0
if n_elements(blk) eq 0 then blk=-1
if n_elements(fname) eq 0 then fname='NO FILES'
if fname ne 'NO FILES' then openr,filunit,fname,64,/get_lun     ;-- reopen last file
 
if n_elements(modes) eq 0 then modes=['NO MODES']
if n_elements(ilabels) eq 0 then ilabels=''
if n_elements(slabels) eq 0 then slabels=''
if n_elements(listmode) eq 0 then listmode=1
if n_elements(mode) eq 0 then mode='RAST'
listchan=1 & listfile=0

;-- check definition of FCS_DATA; if undefined prompt user for data 

cd,current=def

vms=!version.os eq 'vms'
sda_dir=chklog('FCS_DATA')
if (sda_dir eq '') and vms then sda_dir='15837::SYS$DATA:[XRP.DATA.FIS...]'
cur_dir=chklog('CUR_FCS')

case 1 of
 (cur_dir eq '') and (sda_dir eq '') : begin
   read,'* PLEASE ENTER DIRECTORY LOCATION OF YOUR DATA [DEF=CURRENT]: ',cur_dir
   if cur_dir eq '' then cur_dir=def
  end

  (sda_dir ne '') and (cur_dir eq ''): cur_dir=sda_dir

  else:cur_dir=cur_dir
endcase
mklog,'CUR_FCS',cur_dir
if n_elements(fext) eq 0 then fext='8*.*'



;-- parent widget

btitle='FCS DATA ANALYSIS WORKBENCH'
base = widget_base(TITLE =btitle, /column,/frame)

;-- top row of buttons

row1=widget_base(base,/row)


;-- quit button

main = widget_button(row1,value='Quit',uvalue='QUIT',/no_release,/frame)


;-- Disk window

cbutton=lonarr(1,3)
cbutton(0,0)=widget_button(row1,value='Change Directory',uvalue='CHANGE',$
                          /no_release,menu=2)
cbutton(0,1)=widget_button(cbutton(0,0),value='Type Directory Name',$
                           uvalue='TYPE',/no_release)
cbutton(0,2)=widget_button(cbutton(0,0),value='FCS Selected Data Archive', $
                           uvalue='SDA',/no_release)

;-- OS button

osb=widget_button(row1,value='Spawn To OS',uvalue='OS',/no_release)

;-- Save button

;copyb=widget_button(row1,value='SAVE WINDOW',uvalue='SAVE')

;-- help button

help = widget_button(row1,value='Help',uvalue='HELP',/no_release)


;-- 2nd row

row2=widget_base(base,/row)

;-- first column

r2c1=widget_base(row2,/column)

;-- flashing instruction label

temp=widget_base(r2c1,/column,/frame)
flabel=widget_text(temp,uvalue='BACKGROUND',value='',xsize=50)

;-- comment window

temp=widget_base(r2c1,/column,/frame)
 mlabel=widget_label(temp,value='MESSAGE WINDOW')
 comment=widget_text(temp,/scroll,ysize=10,xsize=50)

;-- column 1 in row 2

r2r2=widget_base(r2c1,/row)
col1=widget_base(r2r2,/column)


;-- FCS file window

temp=widget_base(col1,/column,/frame)

;temp1=widget_base(temp,/column,/frame)
 temp1=temp
 label=widget_label(temp1,value='SELECT FROM THE FOLLOWING FILES')
 flist=widget_list(temp1,ysize=10)

;-- FCS mode window

;temp2=widget_base(temp,/column,/frame)
temp2=temp
 tlabel=widget_label(temp2,value='SELECT FROM THESE FCS TIMES')
 tlist=widget_list(temp2,ysize=10,value=' ')

;-- column 2 in row 2

col2=widget_base(r2r2,/column)

;-- mode buttons

temp=widget_base(col2,/frame)

choices=['IMAGES','SPECTRA']
mtypes=['RAST','SPECT']
modeb=lonarr(n_elements(choices))
xmenu,choices,temp,buttons=modeb,/nonexclusive,$
      /row,uvalue=mtypes
for i=0,1 do widget_control,modeb(i),set_button=(mode eq mtypes(i))

;-- channel buttons

detw=widget_base(col2,/column)

channels=strtrim(string(indgen(8)+1),2)
detb=lonarr(n_elements(channels))
xmenu,'CH '+channels+' -            ',detw,buttons=detb,/frame,/exclusive,$
 title='CHANNEL BUTTONS',$
 /column,uvalue='D_'+strtrim(channels,2)

;-- reset channel buttons

for i=0,n_elements(detb)-1 do widget_control,detb(i),sensitive=0

;-- list fis first time thru

widget_control,comment,set_value=welcome

files=loc_file(concat_dir(cur_dir,'fis'+fext),count=count)
widget_control,comment,set_value='CURRENT DIRECTORY:'+strupcase(cur_dir),/append
if count eq 0 then begin            ;-- fis files?
 tfile='NO FIS FILES FOUND' & files='NO FILES' & fname='NO FILES'
 widget_control,comment,set_value='There are no FIS files in the current directory:',/append
 widget_control,set_value='PLEASE CHANGE TO ANOTHER DIRECTORY',flabel
endif else begin
 tfile=strarr(count)
 for i=0,count-1 do begin
  fdecomp,files(i),dsk,direc,name,ext
  tfile(i)=name+'.'+ext
 endfor
 widget_control,flist,set_value=tfile,set_uvalue='FILE'+files

 if fname eq 'NO FILES' then begin               ;-- file in memory?
  widget_control,comment,set_value='Please proceed by selecting a filename',/append
  widget_control,set_value='PLEASE SELECT A FILE',flabel
  for i=0,1 do widget_control,modeb(i),sensitive=0
 endif else begin
  listmode=1 
  widget_control,comment,set_value='CURRENT FIS FILE: '+fname,/append
 endelse

 if modes(0) ne 'NO MODES' then begin     ;-- mode in memory?

  if mode eq '' then begin
   widget_control,set_value='PLEASE SELECT IMAGES OR SPECTRA',flabel & listmode=1
   for i=0,1 do widget_control,modeb(i),sensitive=1
  endif else begin

   if mode eq 'RAST' then find=where(modes eq mode,mcount) else $
    find=where(modes ne 'RAST',mcount)

   if (mcount eq 0) or ( modes(0) eq 'NO MODES') then begin
    widget_control,set_value='PLEASE SELECT ANOTHER FILE',flabel
    value=['NO MODES OF TYPE: '+mode+' IN FILE'] & uvalue=['TIME'+'NO MODES']
   endif else begin
    ntimes=n_elements(times) & blks=sindgen(ntimes)
    mtimes=times(find) & mblks=blks(find)
    value=mtimes+'   '+modes(find) & uvalue='TIME'+mblks
    widget_control,tlist,sensitive=1
    widget_control,set_value='PLEASE SELECT A TIME',flabel
    widget_control,tlabel,set_value='SELECT FROM THESE FCS TIMES'
    listmode=0 
   endelse
   widget_control,tlist,set_value=value,set_uvalue=uvalue
  endelse
 endif
endelse

;-- change directory window

ebase=widget_base(title='  ',/column)
temp=widget_base(ebase,/column,/frame)

elabel=widget_label(temp,value='ENTER NEW DIRECTORY NAME')
elabel2=widget_label(temp,value='DEFAULT: '+cur_dir)
etext=widget_text(temp,/editable,ysize=1,xsize=20)
eabort=widget_button(ebase,uvalue='Abort',value='ABORT',/no_release)

;-- output file name window

wbase=widget_base(title='  ',/column)
temp=widget_base(wbase,/column,/frame)
wlabel=widget_label(temp,value='ENTER OUTPUT FILE NAME')
wlabel2=widget_label(temp,value='DEFAULT: fcs.dmp')
wtext=widget_text(temp,/editable,ysize=1,xsize=20)
wabort=widget_button(wbase,uvalue='Abort',value='ABORT',/no_release)

;-- image object


popts=['Double Bin Image','Contour Image','Gang Images','Heliographic Grid',$
       'Log Scale Image','Overplot Images','Subtract Background']

pvalues=['IBIN','CONT','IGANG','GRID','LOG','IOVER','BKG']


topts=['"Tools" {',    $
                '"Adjust Color Table" ADJUST',$
                '"Check Image Pointing" CHECK',$
                '"Return" ABORT','}']

iopts=['"Import Another SMM Image" {', $
                 '"Ultraviolet Spectrometer Polarimeter" UVSP', $
                 '"Hard X-ray Imaging Spectrometer" HXIS', '}']

hopts=['"Hard Copy" {',    $
                '"B/W" BW',$
                '"Color" SAVE',$
                '"Return" ABORT','}']

wopts=['"Write Data" {', $
                       '"Write To ASCII File" I_ASCII',$
                       '"Write To IDL/XDR Saveset" I_XDR',$
                       '"Return" ABORT','}']

topts=[topts,iopts,hopts,wopts]

doneb=['Done','IDONE']

wobject,popts,pvalues,topts,doneb,iplotb,ibase,idraw,ilabel,xsize=400,$
     ysize=400,title='FCS IMAGE',$
     tbase=itbase,pbase=ipbase

for i=0,n_elements(iplotb)-1 do $
 s=execute('widget_control,iplotb(i),set_button=('+pvalues(i)+' ne 0)')

;-- spectrum object

;-- plot options

popts=['Double Bin Spectrum','Gang Spectra',$
       'Overplot Spectra','Wavelength Axis','Flux Axis']

pvalues=['SBIN','SGANG','SOVER','WAVE','FLUX']


;-- tools

topts=['"Zoom Spectrum" SZOOM',$
        '"Fit Options" {' , $
                '"Gauss" GAUSS',$
                '"Voigt " VOIGT',$
                '"Double Voigt " DVOIGT',$
                '"Blue Wing" BLUE',$
                '"No Fit" NOFIT','}',$

       '"Apply" APPLY',$
       '"Reset Spectrum" SRESET',$ 
       '"Hard Copy" SHARD',$
       '"Write Data" {', $
                       '"Write To ASCII File" S_ASCII',$
                       '"Write To IDL/XDR Saveset" S_XDR',$
                       '"Return" ABORT','}']


;-- done button

doneb=['Done', 'SDONE']

wobject,popts,pvalues,topts,doneb,splotb,sbase,sdraw,slabel,xsize=400,$
        ysize=400,title='FCS SPECTRUM',$
        tbase=stbase,pbase=spbase

for i=0,n_elements(splotb)-1 do $
 s=execute('widget_control,splotb(i),set_button=('+pvalues(i)+' ne 0)')


;-- realize main widget

widget_control,base,/realize
;if xregistered('flash_bck') then xbackregister,'flash_bck',base,/unregister
xmanager,'fcs',base,group_leader=group,just_reg=just_reg

if keyword_set(just_reg) then widget_control,flabel,set_value='PLEASE MAKE A SELECTION' else begin
 xmanager
 if n_elements(group) eq 0 then begin
  message,'cleaning up...',/contin
  cleanplot 
 endif  
endelse

if n_elements(filunit) ne 0 then begin
 close,filunit & free_lun,filunit
endif

return & end


