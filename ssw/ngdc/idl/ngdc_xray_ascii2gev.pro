pro ngdc_xray_ascii2gev, time0, time1, gevdata, xryyyy=xryyyy, xrayyyyy=xrayyyyy,$
               outdir=outdir,refresh=refresh, debug=debug
;+
;   Name: ngdc_xray_ascii2gev 
;
;   Purpose: optionally get, then read one ngdc XRAY events file (ascii)
;
;   Input Parameters:
;      time0, time1 - time range desired 
;
;   Output:
;      gevdata - event data - vector of event info structures inc ssw-times 
;              format are 'gev-like' (somewhat cryptic, but see 'gev_decode.pro'
;              to extract/rationalize desired info)
;
;   Keyword Parameters:
;      xryyyy - apply to ngdc xray event files named xrYYYY (prelim, no locations)
;      xrayyyyy - apply to ngdc xray event files named xrayYYYY (default)
;      outdir - if file transfer is required, put them here def=get_temp_dir() 
;
;   Common Blocks:
;      ngdc_xray_ascii2gev_blk - cache most recent year of data to avoid
;                                 retransfer/re-read if not required
;
;   History:
;      20-Oct-2004 - S.L.Freeland - per requests from B.Labonte & R.Bentley
;                    (useful for comparisons with and extension of SSW GEV dbase)
;
;   Note: applies to xray event data archived at:
;   ftp://ftp.ngdc.noaa.gov/STP/SOLAR_DATA/SOLAR_FLARES/XRAY_FLARES/  
;
;   Restrictions:
;      If requested data is not local to $SSWDB, then socket requests -> ngdc
;      imply IDL Version >=5.4
;      if xrYYYY format/files are implied, locations and other structure info
;      will not be defined
;-
common ngdc_xray_ascii2gev_blk,lastdata
debug=keyword_set(debug)

xraystr={cod:0,stat:0,yy:0,mm:0,dd:0,change:'',start:0l,endt:0l,peak:0l,$
ns:'',nshel:0,ew:'',ewhel:0,optimp:'',optbright:'',class:'',intens:0.,$
region:0,station:'',remarks:'',noaareg:'',reglet:''}
;,centyy:0,centmm:0,centdd:0.,blank:''}

xrayfmt='('+ngdc_xray_fmt() + ')'

ngdcurl='ftp://ftp.ngdc.noaa.gov/STP/SOLAR_DATA/SOLAR_FLARES/XRAY_FLARES/'

case 1 of 
   keyword_set(xryyyy): prefix='xr'
   keyword_set(xrayyyyy): prefix='xray'
   else: prefix='xray'
endcase

case 1 of 
   n_elements(time0) eq 0: begin 
      box_message,'Need at least one time
      return
   endcase
   n_params() lt 3: begin
      box_message,'You did not supply an output param, so why bother...'
      return
   endcase
   n_elements(time1) eq 0: time1=time0 
   else:
endcase

uyears=all_vals(strmids(timegrid(time0,time1,/days,out='ecs'),0,4)) ; all YYYY

gbo_struct,gev=gev_struct

; times within lastdata?
refresh=keyword_set(refresh) or n_elements(lastdata) eq 0 

if data_chk(lastdata,/struct) and (1-refresh) then begin
   ss=sel_timrange(lastdata,time0,time1,/between)
   if ss(0) ne -1 then begin 
      box_message,'Using cached data...'
      gevdata=lastdata(ss)
      return
   endif
endif
   
yfiles=prefix+uyears
allurls=ngdcurl+yfiles

if not file_exist(outdir) then outdir=get_temp_dir()
ngdcdat=''
for i=0,n_elements(uyears)-1 do begin 
   local=concat_dir(concat_dir('$SSWDB','ngdc/xray_events'),yfiles(i))
   local=[local,concat_dir(outdir,yfiles(i))]
   ssloc=where(file_exist(local),sscnt)
   if sscnt eq 0 then begin 
        if since_version('5.4') then begin 
           box_message,'NGDC file:' + urls(i)
           sock_copy,allurls(i),outdir=outdir 
        endif else begin 
           box_message,'Sorry, you need at least IDL V5.4 for transfer ngdc-local (via socket'
           return
        endelse
   endif
   ssloc=where(file_exist(local),sscnt)
   if sscnt gt 0 then begin 
      box_message,'Processing File: ' + local

   endif else begin 
      box_message,'Trouble with socket transfer NGDC->local, aborting...
      return ; unstructured exit....
   endelse
   ngdcdat=[temporary(ngdcdat),rd_tfile(local(ssloc(0)))]
endfor
ssok=where(strlen(strtrim(ngdcdat,2)) ge 50)
ngdcdat=ngdcdat(ssok)
if n_elements(ngdcdat) gt 1 then begin 
   ngdcdat=strpad(strtrim(ngdcdat(1:*),2),90,/after)
   ndat=n_elements(ngdcdat)
   ngdcstr=replicate(xraystr,ndat)
   temp=xraystr
   ssbad=where(strpos(ngdcdat,'G') gt 80,bcnt)
   if bcnt gt 0 then ngdcdat(ssbad)=str_replace(ngdcdat(ssbad),'G',' ')   ; bad 1994 record?
   ;ngdcstruct=table2struct(ngdcdat,strtemplate=xraystr,format=xrayfmt)
   for ii=0,ndat-1 do begin ; TODO figure out why a loop is needed! 
      reads,ngdcdat(ii),temp,format=xrayfmt
      ngdcstr(ii)=temp
   endfor
endif else begin 
   box_message,'No ngdc data read?'
endelse

retval=-1
case 1 of 
   1-data_chk(ngdcstr,/struct): begin 
      box_message,'Problem reading/translating ngdc->structures
   endcase
   keyword_set(ngdcout): retval=ngdcstr ; return ngdc structure vector
   else: begin                          ; default is GEV structure
      retval=replicate(gev_struct,ndat)
      extt=anytim(retval,/ext)
       
      ft0=string(ngdcstr.start,format='(I4.4)')
      ft0=strmid(ft0,0,2)+':'+strmid(ft0,2,2)
      fte=string(ngdcstr.endt,format='(I4.4)')
      fte=strmid(fte,0,2)+':'+strmid(fte,2,2)
      ftp=string(ngdcstr.peak,format='(I4.4)')
      ftp=strmid(ftp,0,2)+':'+strmid(ftp,2,2)
      ftday= string(ngdcstr.yy,format='(I2.2)') +'/'+$
             string(ngdcstr.mm,format='(I2.2)') +'/'+$
             string(ngdcstr.dd,format='(I2.2)') 
             
      fstart=ft0 + ' '  + ftday
      fend=fte+' ' + ftday
      fpeak=ftp+' ' + ftday 
      dte=ssw_deltat(fend,ref=fstart,/sec)
      dtp=ssw_deltat(fpeak,ref=fstart,/sec)
      dte=dte + (dte lt 0)*86400. ; end time next day
      dtp=dtp + (dtp lt 0)*86400. ; peak next day
      gevt0=anytim(fstart,/int)   ; gev standard
;     map ngdc -> gev 
      retval.time=gevt0.time
      retval.day= gevt0.day
      retval.peak=dtp
      retval.duration=dte
      
      intensity=ngdcstr.intens
      intensity=float(intensity)/([1.,10.])(intensity ge 10.)
      class=ngdcstr.class + string(intensity,format='(f3.1)')
      retval.st$class=byte(class)
      retval.st$halpha(0,0)=reform(byte(ngdcstr.optimp))
      retval.st$halpha(1,0)=reform(byte(ngdcstr.optbright))
      retval.noaa=fix(ngdcstr.noaareg)
      ngdcstr.ewhel=ngdcstr.ewhel + ([0,999])(ngdcstr.ew eq ' ') ; gev-like missing fill 
      ngdcstr.nshel=ngdcstr.nshel + ([0,999])(ngdcstr.ns eq ' ')
      retval.location(0,0)=reform(ngdcstr.ewhel)*([-1,1])(ngdcstr.ew eq 'W')
      retval.location(1,0)=reform(ngdcstr.nshel)*([-1,1])(ngdcstr.ns eq 'N')
      lastdata=retval
      gevdata=retval
      ss=sel_timrange(retval,time0,time1,/between)
      if ss(0) ne -1 then gevdata=retval(ss) else begin
          box_message,'No records within time range'
          gevdata=-1
      endelse   
        
   endcase
endcase

if debug then stop,'before return'
return
end 
;   
;  
;  
