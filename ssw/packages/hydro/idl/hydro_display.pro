pro hydro_display,tmin,tmax,s_heat,len,s,t_num,t_ana,p_num,p_ana,n_num,n_ana,f_num,f_ana,$
       eh0_num,eh0_ana,dptot,dptot_ana,etot,etot_ana,dp_ds,dpgrav,econd,eheat,erad,radref,$
       io,form,char,fignr,ref,plotname,unit

col1	= 50
col2	=150
h_chr	=s(0)
ns	=n_elements(s)

fig_open,io,form,char,fignr,plotname,unit
loadct,5
clearplot
!p.position=[0.1,0.65,0.35,0.90]
!x.title=''
!x.range=[1.e4,len]/1.e8
!p.title='Temperature'
!y.title='T(s) [MK]'
!y.range=[0,1.1*max(t_num/1.e6)]
plot, s/1.e8,t_num/1.e6,charsize=char
oplot,s/1.e8,t_ana/1.e6,color=col2
oplot,h_chr*[1,1]/1.e8,!y.crange,linestyle=1
xyouts_norm,0.3,0.5,'T!Dmax1!N='+string(tmax/1.e6,'(f8.3)')+' MK',char
xyouts_norm,0.3,0.4,'T!Dmin1!N='+string(tmin/1.e6,'(f8.3)')+' MK',char
xyouts_norm,0.3,0.3,'L!Dloop!N='+string(len/1.e8,'(f8.3)')+' Mm',char
xyouts_norm,0.3,0.2,'s!Dheat!N='+string(s_heat/1.e8,'(f8.3)')+' Mm',char
!noeras=1

!p.position=[0.4,0.65,0.65,0.90]
!y.title='log[T(s)]'
!x.range=minmax(s(1:ns-1)-s(0))
!y.range=minmax(t_num(1:ns-1))
plot_oo,s(1:ns-1)-s(0),t_num(1:ns-1),charsize=char
oplot  ,s(1:ns-1)-s(0),t_ana(1:ns-1),color=col2

!p.title='Conductive flux'
!p.position=[0.7,0.65,0.95,0.90]
!y.title='F!DC!N(s)'
!y.range=[min(f_num),max(f_num)]
!x.range=[1.e4,len]/1.e8
!x.title=''
plot,s/1.e8,f_num,charsize=char
oplot,s/1.e8,f_ana,color=col2
oplot,!x.crange,[0,0]
oplot,h_chr*[1,1]/1.e8,!y.crange,linestyle=1

!p.position=[0.1,0.35,0.35,0.60]
!x.title=''
!p.title='Density'
!y.title='n(s) [cm!U-3!N]'
!y.range=[min(n_num),max(n_num)]
plot_io,s/1.e8,n_num,charsize=char
oplot,s/1.e8,n_ana,color=col2
oplot,h_chr*[1,1]/1.e8,!y.crange,linestyle=1
xyouts_norm,0.3,0.8,'n!D0!N='+string(n_num(0),'(e10.2)')+' cm!U-3!N',char
xyouts_norm,0.3,0.7,'n!DA!N='+string(n_ana(0),'(e10.2)')+' cm!U-3!N',char
q_n     =n_ana/n_num
statistic,q_n(1:ns-1),qn,qnsig
xyouts_norm,0.3,0.55,'q!Dn!N='+string(qn,'(f8.3)'),char*1.5

!p.position=[0.4,0.35,0.65,0.60]
!y.title='log[n(s)]'
!x.range=minmax(s(1:ns-1)-s(0))
!y.range=minmax(n_num(1:ns-1))
plot_oo,s(1:ns-1)-s(0),n_num(1:ns-1),charsize=char
oplot  ,s(1:ns-1)-s(0),n_ana(1:ns-1),color=col2

!p.position=[0.7,0.35,0.95,0.60]
!p.title='Momentum balance'
!y.title='dp/ds [dyne cm!U-3!N]'
fmax	=max(abs(f_num),im)
!y.range=[-1,+1]*2.*abs(dp_ds(im))
!x.range=[1.e4,len]/1.e8
!x.title=''
plot,!x.crange,[0,0],charsize=char
x1  =!x.crange(0)
x2  =!x.crange(1)
y1  =!y.crange(0)
y2  =!y.crange(1)
xx  =x1+(x2-x1)*[0.5,0.7]
yy  =y1+(y2-y1)*[0.8,0.8]
oplot,s(3:ns-1)/1.e8,-dp_ds(3:ns-1),linestyle=1  &oplot,xx,yy,linestyle=1  &xyouts,xx(1),yy(1),' -dp/ds',size=char
yy  =y1+(y2-y1)*[0.7,0.7]
oplot,s(3:ns-1)/1.e8, dpgrav(3:ns-1),linestyle=2 &oplot,xx,yy,linestyle=2  &xyouts,xx(1),yy(1),'  dp!Dgrav!N/ds',size=char
yy  =y1+(y2-y1)*[0.6,0.6]
oplot,s(3:ns-1)/1.e8, dptot(3:ns-1),thick=3    &oplot,xx,yy     &xyouts,xx(1),yy(1),'  dp!Dtot!N/ds',size=char
oplot,s(3:ns-1)/1.e8, dptot_ana(3:ns-1),col=col2     
oplot,h_chr*[1,1]/1.e8,!y.crange,linestyle=1

!p.position=[0.1,0.05,0.35,0.3]
!x.title=''
!p.title='Pressure'
!y.range=[0,max([p_num,p_ana])]
!y.title='p(s) [dyne cm!U-2!N]'
plot, s/1.e8,p_num,charsize=char
oplot,s/1.e8,p_ana,color=col2
oplot,h_chr*[1,1]/1.e8,!y.crange,linestyle=1
oplot,!x.crange,[0,0]
xyouts_norm,0.3,0.8,'p!D0!N='+string(p_num(0),'(e10.2)')+' cm!U-3!N',char
xyouts_norm,0.3,0.7,'p!DA!N='+string(p_ana(0),'(e10.2)')+' cm!U-3!N',char
q_p     =p_ana/p_num
statistic,q_p(1:ns-1),qp,qpsig
xyouts_norm,0.3,0.55,'q!Dp!N='+string(qp,'(f8.3)'),char*1.5

!p.position=[0.4,0.05,0.65,0.3]
!y.title='log[p(s)]'
!x.range=minmax(s(1:ns-1)-s(0))
!y.range=minmax([p_num(1:ns-1),p_ana(1:ns-1)])
plot_oo,s(1:ns-1)-s(0),p_num(1:ns-1),charsize=char
oplot  ,s(1:ns-1)-s(0),p_ana(1:ns-1),color=col2

!p.position=[0.7,0.05,0.95,0.3]
!p.title='Energy balance'
!y.title='dE(s)/dV*dt [erg cm!U-3!N s!U-1!N]'
!y.range=[-1.2,1.2]*eheat(0)
!x.range=[1.e4,len]/1.e8
!x.title='Loop distance  s[Mm]'
plot,!x.crange,[0,0],charsize=char
y1  =!y.crange(0)
y2  =!y.crange(1)
yy  =y1+(y2-y1)*[0.9,0.9]
oplot,s(3:ns-1)/1.e8,-econd(3:ns-1),linestyle=3   &oplot,xx,yy,linestyle=3 &xyouts,xx(1),yy(1),' -E!Dcond!N',size=char
yy  =y1+(y2-y1)*[0.8,0.8]
oplot,s/1.e8,eheat,linestyle=4    &oplot,xx,yy,linestyle=4 &xyouts,xx(1),yy(1),' E!Dheat!N',size=char
yy  =y1+(y2-y1)*[0.7,0.7]
oplot,s/1.e8,erad     		  &oplot,xx,yy             &xyouts,xx(1),yy(1),' E!Drad!N',size=char
yy  =y1+(y2-y1)*[0.6,0.6]
oplot,s(3:ns-1)/1.e8,etot(3:ns-1),thick=3 
oplot,s(3:ns-1)/1.e8,etot_ana(3:ns-1),col=col2 
oplot,h_chr*[1,1]/1.e8,!y.crange,linestyle=1
xx  =x1+(x2-x1)*0.05
yy  =y1+(y2-y1)*0.25
xyouts,xx,yy,'E_H0 = '+string(eh0_num,'(e10.3)')+' erg cm!U-3!N s!U-1!N',size=char
yy  =y1+(y2-y1)*0.15
xyouts,xx,yy,'E_H0 = '+string(eh0_ana,'(e10.3)')+' erg cm!U-3!N s!U-1!N',size=char
yy  =y1+(y2-y1)*0.03
qe      =eh0_ana/eh0_num
xyouts,xx,yy,'q_EH = '+string(qe,'(f5.3)'),size=char*1.5,color=col2
xyouts,0.5,0.95,'Radiative loss function = '+radref,size=char,/normal
xyouts,0.1,0.95,plotname+' '+fignr,size=char*2.0,/normal

fig_close,io,fignr,ref
end
