function ssw_rstn_srs_index, time0, time1, $
   generate=generate, refresh=refresh, topdir=topdir, invalid=invalid, obs=obs
;+
;   Name: ssw_rstn_srs_index

;   Purpose: read or optionally generate rstn_srs genx index
;
;   Input Paramters:
;      time0, time1 - optional time range ; default is entire index     
;
;   Keyword Parameters:
;      generate - if set, generate index record (authorized users only..)
;      obs - desired observatory to override default - note that may not exist
;            for given input time range - 
;            OBS={ SVTO, HOLL, PALE, LEAR, SGMR }
;      refresh - if set, force refresh of dbase "cache" (common...)
;      topdir - parent NFS location of SPECTRAL_RSTN (data) and spectral_rstn_genx (index)
;      invalid - if set, return only Invalid records (default is only VALID) 
;                (invalid records indicate that read_rstn_srs could not read/process)
;
;   Motivation: SRS files @ NGDC have non-deterministic file names 
;
;   Common Blocks:
;      ssw_rstn_srs_index_blk - store index 1st access (use /REFRESH to refresh...)
;
;   29-August-2005 - S.L.Freeland - rationalize time based RSTN_SRS file selection
;                    (called by ssw_time2rstn_files.pro -> get_rstn_srs.pro)
;
;   
;   
;-
;
common ssw_rstn_srs_index_blk,rstnx  ; store index on 1st access

rootrstn='/ngdc/STP/SOLAR_DATA/SOLAR_RADIO/SPECTRAL_RSTN'
toprstn=concat_dir('$SSWDB',rootrstn)
genrstn=str_replace(toprstn,'SPECTRAL_RSTN','spectral_rstn_genx')

generate=keyword_set(generate) 

irec={file:'',mjd:0l,time:0l,mjd_stop:0l,time_stop:0l,maxgap_seconds:0.}
if generate then begin 
   if not data_chk(topdir,/string) then $
      topdir='/net/vestige/Volumes/disk1/ops/DataCenter/sswdb/ngdc/STP/SOLAR_DATA/SOLAR_RADIO'
      files=dir_since(concat_dir(topdir,'SPECTRAL_RSTN'),0,/sub) ; all SRS files
      ss=where(strpos(files,'.SRS') ne -1 or strpos(files,'.srs') ne -1,sscnt)
      fnames=ssw_strsplit(files(ss),'/',/last,/tail)
      retval=replicate(irec,sscnt)
      retval.file=fnames
      for i=0, sscnt-1 do begin
         delvarx,ts,fr,sp,fdate         
         read_rstn_srs,files(ss(i)),ts,fr,sp,fdate
         if data_chk(fdate,/string) then begin 
            sswt=anytim(anytim(file2time(fdate))+ts,/utc_int)
            time_window,sswt,t0,t1,out='utc_int'
            retval(i).time=t0.time
            retval(i).mjd=t0.mjd
            retval(i).mjd_stop=t1.mjd
            retval(i).time_stop=t1.time
            retval(i).maxgap_seconds=max(ssw_deltat(sswt,/sec))
         endif else box_message,'Error with>> ' + fnames(i)
      endfor
      write_genxcat,prefix='rstn_index_',retval,$
        /geny,/nelem,topdir=concat_dir(topdir,'spectral_rstn_genx')

endif else begin 
   if n_elements(findfile(genrstn)) gt 1 then begin 
;      local $SSWDB index
   endif else begin 
      refresh=keyword_set(refresh) or n_elements(rstnx) eq 0
      if refresh then begin
         out=get_temp_dir()
         topurl='http://www.lmsal.com/solarsoft/sswdb/'
         indurl=topurl+str_replace(rootrstn,'SPECTRAL_RSTN','spectral_rstn_genx/')
         sock_list,indurl,list
         indss=where(strpos(list,'geny') ne -1,sscnt)
         if sscnt eq 0 then begin 
            box_message,'Cannot find index file local or WWW, returning
            return,''
         endif
         indname=str_replace(strextract(list(indss),'"rstn','geny"',/inc),'"','')
         index=indurl+ indname
         locindex=concat_dir(out,indname)
      
         sock_copy,index,out_dir=out
         if not file_exist(locindex) then begin 
            box_message,'Problem with index http transfer, bailing...
            return,''
         endif
         restgenx,file=locindex,rstnrecs
         ss=sort_index(rstnrecs,/ss)
         retval=rstnrecs(ss)
         rstnx=retval  ; initialize common
      endif else retval=rstnx ; else use "cached" (ie, common)
      valid=retval.mjd ne 0
      if keyword_set(invalid) then ss=where(1-valid,sscnt) else $
         ss=where(valid,sscnt)
      if sscnt gt 0 then retval=retval(ss) else begin 
         box_message,'No Valid and/or Invalid records.., returning
         return,''
      endelse
      if data_chk(obs,/string) then begin 
         pre=(strmid(obs,0,2))(0)
         pre=([pre,'LM'])(pre eq 'LE')
         pre=([pre,'K7'])(pre eq 'SG')
         ss=where(strmid(retval.file,0,2) eq pre,sscnt)
         if sscnt eq 0 then begin 
            box_message,'No records with OBS = ' + obs(0) + ' , returning..'
            return,''
         endif
         retval=retval(ss) ; OBS subset
      endif
      
      ssscnt=0
      case n_params() of 
         1: sss=last_nelem(where(positive(anytim(time0)-anytim(retval)))) 
         2: begin 
               ssaft=last_nelem(where(positive(anytim(time0)-anytim(retval)),ssscnt))
               if ssscnt eq 0 then begin 
                  box_message,'Input start time after last RSTN/SRS file time'
                  return,''
               endif
               ssbef=max(where(anytim(retval(ssaft))-anytim(time1) gt 0) > 0) 
               sss=ssaft(0:ssbef)
         endcase
         else:
      endcase
      if ssscnt gt 0 then retval=retval(sss)
   endelse 
endelse

return,retval
end

 
