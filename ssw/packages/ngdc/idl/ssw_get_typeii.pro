function ssw_get_typeii, time0, time1, year=year , count=count, $
    potsdam=potsdam, ngdc=ngdc 
;+
;   Name: ssw_get_typeii  
;  
;   Purpose: return TYPE II times within callers time range
;
;   Input Paramters:
;      time0 - time of closest interest or start time of range
;      time1 - stop time of time range
;
;   Output Parameters:
;      returns structure vector ; TYPE II start/stop & "other" info - utplot ready...
;
;   Keyword Parameters: 
;      ngdc - (switch) - use NGDC lists
;      potsdam - (switch) - Andreas Klassen's lists: http://www.aip.de/People/AKlassen/
;      year - (input) - year(s) desired (full years in lieu of time range)
;      count - (output) - number of events returned after time filter applied.
;
;   Calling Example:
;      tii=t2s=ssw_get_typeii('15-nov-2001 12:00','15-jul-2001',/potsdam,count=count)
;
;   Restrictions:
;      /POTSDAM only for today
;
;   History:
;      29-Aug-2005 - S.L.Freeland - helper utility; cross calibrate RSTN/SRS suite
;
;- 

potsdam=1 ; only this for now

potsdam=keyword_set(potsdam)

rettemp={mjd:0l,time:0l,mjd_stop:0l,time_stop:0l, source:'', other:'', remote_url:''}

case 1 of 
   potsdam: begin 
      source='http://www.aip.de/People/AKlassen/'
      if keyword_set(year) then years=year else begin 
         tg=timegrid(time0,time1,days=200,/ecs,/quiet)
         year=strmid(tg,0,4)
         year=year(uniq(year))
      endelse
      ynames='type_II_list_'+year+'.html'
      urls=source+ynames
      nyears=n_elements(year)
      list=''
      for i=0,nyears-1 do begin 
         sock_list,urls(i),ulist
         ulist=strupcase(strarrcompress(ulist))
         sse=where(is_number(strextract(ulist,'<TD>','</TD>')) and strpos(ulist, $
                   '&NBSP') eq -1,ecnt)
         temp=replicate(rettemp,ecnt) 
         temp.source=urls(i)
         event=strextract(ulist(sse),'<TD>','</TD>')
         udate=str2cols(strtrim(strextract(ulist(sse+1),'<TD>','&'),2),/unaligned)
         strtab2vect,udate,yy,mm,dd
         udates=dd+ '-' + strcapitalize(strmid(mm,0,3)) + '-' + yy
         t0=strtrim(strextract(ulist(sse+2),'<TD>','&NBSP'))
         t1=strtrim(strextract(ulist(sse+3),'<TD>','&NBSP'))
         rem=strtrim(strextract(ulist(sse+4),'<TD>','&NBSP'))
         ut0=anytim(udates + ' ' + strmid(t0,0,2) + ':' + strmid(t0,2,2),/utc_int)
         ut1=anytim(udates + ' ' + strmid(t1,0,2) + ':' + strmid(t1,2,2),/utc_int)
         ut1.mjd=ut1.mjd + (t0 gt t1) 
         temp.mjd=ut0.mjd
         temp.time=ut0.time
         temp.mjd_stop=ut1.mjd
         temp.time_stop=ut1.time
         temp.other='Event# ' + event + ' ' + rem     
         if data_chk(allrecs,/struct) then $
             allrecs=concat_struct(allrecs,temporary(temp)) else $
              allrecs=temporary(temp)       
      endfor 
      case n_params() of 
         0: retval=allrecs
         1: begin 
              ss=tim2dset(anytim(allrecs,/int),anytim(time0,/int))
              retval=allrecs(ss)
         endcase
         else: begin 
            ss=sel_timrange(anytim(allrecs,/int), $
               anytim(time0,/int),anytim(time1,/int),/between)
            if ss(0) ne -1 then retval=allrecs(ss) else retval=-1
         endcase
      endcase 
         
   endcase
   else: begin 
      box_messsage,'Data source not recognized
   endcase
endcase

count=n_elements(retval) * data_chk(retval,/struct)

return,retval
end

