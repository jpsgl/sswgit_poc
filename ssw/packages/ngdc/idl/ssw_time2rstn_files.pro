function ssw_time2rstn_files,time0, time1, count=count, $
   spectral_rstn=spectral_rstn, srs=srs, sswdb=sswdb, obs=obs
;
;+
;   Name: ssw_time2rstnfiles
;
;   Purpose: map from ssw time or time range -> rstn files@NGDC or $SSWDB
;
;   Input Parameters:
;      time0, time1 - desired time range
;
;   Keyword Paramters:
;      spectral_rstn & srs (synonyms) (switch) if set, dynamic spectral data SRS
;      obs -  obs desired RSTN {SVTO, HOLL, PALE, LEAR, SGMR} 
;      sswdb - if set, check $SSW/ngdc/.. first
;      ngdc - if set, NGDC (default)
;
;   Calling Sequence:
;      files=ssw_time2rstnfiles(time0,time1 [,/sswdb] [,/srs] [,/ngdc]
;
;   History:
;      8-aug-2005 - S.L.Freeland - starting with logic from Stephen White's
;                   original plot_rstn_srs.pro 
;     15-aug-2005 - S.L.Freeland - add hooks for SSWDB and alternate http
;     29-aug-2005 - S.L.Freeland - use rstn/srs catalog via ssw_rstn_srs_index call
;
;
; determine observatory

;SVTO 5-16, HOLL 13-24, PALE 17-04, LEAR 23-10, SGMR 1-22 ; from S.White plot_rstn_srs.pro  
if not data_chk(observatory,/string) then begin 
;  default obs is start time dependent
   dth=(anytim(time0,/ex))(0) ; UT hours 
   case 1 of 
      is_member(dth,[0,1,22,23,24]):         obs='PALE'
      is_member(dth,[2,3,4,5,6,7,8]):        obs='LEAR'
      is_member(dth,[9,10,11,12,13,14,15]) : obs='SVTO'
      is_member(dth,[16,17,18,19,20,21]):    obs='HOLL'
      else: begin 
         box_message,'Unexpected hour??'
         obs='PALE'
      endcase 
   endcase
endif  

shobs=strmid(obs,0,2) ; shorthand used in file name
shobs=([shobs,'LM'])(obs eq 'LEAR')
shobs=([shobs,'K7'])(obs eq 'SGMR') ; adjust per S.White plot_rstn_srs

finfo=ssw_rstn_srs_index(time0,time1,obs=obs)  ; use rationalized index. 
fname=finfo.file
fdate=time2file(file2time(finfo.file),/date_only)

rstnenv=get_logenv('ssw_rstn_srs')
defsub='STP/SOLAR_DATA/SOLAR_RADIO/SPECTRAL_RSTN/'
ngdcftp='ftp://ftp.ngdc.noaa.gov/'
sswdb=concat_dir('$SSWDB/ngdc',defsub)

topngdc='ftp://ftp.ngdc.noaa.gov/STP/SOLAR_DATA/SOLAR_RADIO/SPECTRAL_RSTN/'
subdir=obs+strmid(fdate,2,4)

ngdc=topngdc+subdir+'/'+fname
nonngdc=rstnenv+'/'+ subdir+'/'+fname
sswdb=concat_dir(sswdb,concat_dir(subdir,fname))
locfile=concat_dir(rstnenv,concat_dir(subdir,fname))

case 1 of 
    file_exist(locfile): retval=locfile
    file_exist(sswdb): retval=sswdb
    rstnenv ne '':  retval=nonngdc ;non ngdc url for example
    else: retval=ngdc 
endcase

return,retval
end

