; uses this routine from Goddard library
;-----------------------------------------------------------------------------
      FUNCTION tenv,dd,mm,ss
;+
; NAME:
;	TENV()
; PURPOSE:
;	Converts sexigesimal number or vector to decimal.  
; EXPLANATION:
;	Like TEN() but allows vector input.
;
; CALLING SEQUENCES:
;	Result = TENV( dd, mm )           ; result = dd + mm/60.
;	Result = TENV( dd, mm, ss)        ; result = dd + mm/60. + ss/3600.
;
; INPUTS:
;	dd - Sexigesimal element(s) corresponding to hours or degrees
;	mm - Sexigesimal element(s) corresponding to minutes
;	ss - Sexigesimal element(s) corresponding to seconds (optional)
;		The input parameters can be scalars or vectors.   However, the
;		number of elements in each parameter must be the same.
;
; OUTPUTS:
;	Result -  double, decimal equivalent of input sexigesimal 
;		quantities.  Same number of elements as the input parameters.
;		If the nth element in any of the input parameters is negative 
;		then the nth element in Result wil also be negative.
;
; EXAMPLE:
;	If dd = [60,60,0], and mm = [30,-30,-30], then
;
;	IDL> Result = TENV(dd,mm)  ====>   Result =  [60.5,-60.5,-0.5]
;
; PROCEDURE:
;	Mostly involves checking arguments and setting the sign.
;
;   MODIFICATION HISTORY:
;	Written by W.B. Landsman           April, 1991
;	Converted to IDL V5.0   W. Landsman   September 1997
;-
 On_error,2                                 ;Return to caller

 npts = N_elements(dd)
 npar = N_params()
 if npts EQ 0 then begin
     print,'Syntax -  RESULT = TENV( dd, mm, ss)'
     return, 0.0d
 endif

 if ( npar EQ 1 ) then return,double( dd )   ;No need to check for neg values.

 value = double( abs(dd) ) 

 if ( npar GT 1 ) then begin               ;Add minutes/60., check for <0

      if N_elements(mm) NE npts then $
           message,'ERROR - Number of elements in each parameter must be equal'
      neg = (dd LT 0) OR  (mm LT 0) 
      value = value + abs(mm)/60.0d

 endif

 if ( npar GT 2 ) then begin               ;Add sec/3600., check for <0

      if N_elements(ss) NE npts then $
           message,'ERROR - Number of elements in each parameter must be equal'
      neg = neg OR  (ss LT 0) 
      value = value + abs(ss)/3600.0d

 endif

 neg = where( neg, Nfound )                  ;Account for negative values
 if ( Nfound GT 0 ) then value[neg] = -value[neg]

 return,value      
 end
;-----------------------------------------------------------------------------
;+
; NAME:
;     READ_RSTN_SRS
; PURPOSE:
;     Read RSTN SRS 25-180 MHz dynamic spectrum SRS files. 
; CATEGORY:
;     RSTN SRS
; CALLING SEQUENCE:
;     read_rstn_srs, srs_filename, tseconds, frequency, spectrum, date
; INPUTS:
;     filename  The name of the SRS-format file to open, found at
;            ftp://ftp.ngdc.noaa.gov/STP/SOLAR_DATA/SOLAR_RADIO/SPECTRAL_RSTN/
; OUTPUTS:
;     tseconds  Array of times in seconds 
;     frequency Array of frequencies in MHz
;     spectrum  Spectrum(time,freq) as a byte array, with log scaling
;                  (as storeed in SRS files)
;     date      Date of the first time stamp in YYYYMMDD format
; COMMENTS:
; SIDE EFFECTS:
; RESTRICTIONS:
; MODIFICATION HISTORY:
;     Written 13-Sep-2004 by Stephen White
;-

pro read_rstn_srs, file, tsecs, freq, spec, date

; read SRS file from RSTN digital 25-180 MHz system 
; files are 826 byte records for each 3-second interval

IF (n_params(0) LT 4) THEN BEGIN
PRINT,'---------------------------------------------------------------------'
PRINT,'Call: read_rstn_srs, srs_filename, tseconds, frequency, spectrum, date'
PRINT,'---------------------------------------------------------------------'
PRINT,'Reads tseconds, frequency, spectrum, date from srs_filename.'
PRINT,'---------------------------------------------------------------------'
return
endif

   ; Open file 

   openr,lun,/get_lun,file

   ; Calculate the number of records in the file from
   ; the file size (should be a multiple of 826)
   fs = fstat(lun)
   nrec = fs.size/826L

   ; If not a multiple of 826, bail out
   if (nrec*826L ne fs.size) then begin
      print,'File length not a multiple of 826 bytes: terminating ....'
      free_lun,lun
      return
   endif

   print,'---------------------------------------------------------------'
   print,'There are '+strtrim(string(nrec),2)+' records in the file.'
   print,'---------------------------------------------------------------'

   ; create a template structure to be read
   ; the 2-byte values are translated as 256*byte1+byte2
   templ = { yr:0B, mon:0B, day:0B, hr:0B, mm:0B, ss:0B, site:0B, nbands:0B, $
     sfreq1:[0B,0B], efreq1:[0B,0B], nbytes1:[0B,0B], anref1:0B, anatt1:0B, $
     sfreq2:[0B,0B], efreq2:[0B,0B], nbytes2:[0B,0B], anref2:0B, anatt2:0B, $
     band1:bytarr(401), band2:bytarr(401) }

   ; now make an array of structures
   tmps = replicate(templ, nrec)
   readu,lun,tmps
   close,lun
   free_lun,lun
   
   ; extract the data
   yr=fix(tmps[0].yr) & mon=fix(tmps[0].mon) & day=fix(tmps[0].day)
   if (yr gt 50) then yr=yr+1900 else yr=yr+2000
   date=string(yr,format='(i4.4)')+string(mon,format='(i2.2)')+string(day,format='(i2.2)')
   tsecs=tenv(fix(tmps.hr),fix(tmps.mm),fix(tmps.ss))*3600.
   ; wrap times around if needed
   ix=where(tsecs lt tsecs[0]-1.0) 
   if (ix[0] gt 0) then tsecs[ix]=tsecs[ix]+86400.
   freq=fltarr(802)
   f1=tmps[0].sfreq1[0]*256.0+float(tmps[0].sfreq1[1])
   f2=tmps[0].efreq1[0]*256.0+float(tmps[0].efreq1[1])
   df=(f2-f1)/400. & freq[0:400]=f1+df*indgen(401)
   f1=tmps[0].sfreq2[0]*256.0+float(tmps[0].sfreq2[1])
   f2=tmps[0].efreq2[0]*256.0+float(tmps[0].efreq2[1])
   df=(f2-f1)/400. & freq[401:801]=f1+df*indgen(401)
   ; fluxes stored as byte values on a log scale: retain this format?
   spec=bytarr(nrec,802) 
   spec[*,0:400]=transpose(tmps.band1)
   spec[*,401:801]=transpose(tmps.band2)

end

