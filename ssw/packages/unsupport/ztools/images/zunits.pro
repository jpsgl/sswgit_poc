;+
; NAME:
;	ZUNITS(whatyougot,whatyouwant)
; PURPOSE:
;	provide a conversion factor between given and desired units,
;	for certain recognized MSSTA length units
; CALLING SEQUENCE:
;	a=ZUNITS("whatyougot","whatyouwant")
; INPUTS:
;	whatyougot: a string containing the existing length unit
;	whatyouwant: a string containing the desired length unit
; OPTIONAL INPUT PARAMETERS:
;	(none)
; KEYWORD PARAMETERS:
;	(none)
; OUTPUTS:
;	a number containing the conversion factor
; RESTRICTIONS:
;	Only some units are recognized.
; MODIFICATION HISTORY:
;	Created by Craig DeForest, 1/30/1995
;	Added "solar-x" and "solar-y" as alternate "arcsec" fields
; 		to support SoHO, 8/28/1996
;-
function zunit2r0,foo
whatyougot = strcompress(strtrim(STRLOWCASE(foo)))
if((whatyougot eq "") or (fug(string(whatyougot)) eq fug(string(0)))) then whatyougot = "arcsec"

ampersr = 0.987 * 16.0
srperam = 1.0 / ampersr

if(whatyougot eq "solar-radii") 	then return, 1.0
if(whatyougot eq "milli-solar-radii") 	then return, 0.001
if(whatyougot eq "solar-radii") 	then return, 1.0
if(whatyougot eq "solar radii") 	then return, 1.0
if(whatyougot eq "arcmin") 		then return, (srperam)
if(whatyougot eq "arcminutes")		then return, (srperam)
if(whatyougot eq "solar-x") 		then return, (srperam/60)
if(whatyougot eq "solar-y")		then return, (srperam/60)
if(whatyougot eq "arcsec") 		then return, (srperam/60)
if(whatyougot eq "arcseconds")		then return, (srperam/60)
if(whatyougot eq "meters") 		then return, (1.0/6.96e8)
if(whatyougot eq "m") 			then return, (1.0/6.96e8)
if(whatyougot eq "centimeters") 	then return, (1.0/6.96e10)
if(whatyougot eq "cm") 			then return, (1.0/6.96e10)
if(whatyougot eq "kilometers") 		then return, (1.0/6.96e5)
if(whatyougot eq "km") 			then return, (1.0/6.96e5)


print,"zunit2r0: warning -- unit ",whatyougot," not recognized Returning 1."
return,1.0

end

;----------------------------------------------
function zunits,whatyougot,whatyouwant
return,(zunit2r0(whatyougot) / zunit2r0(whatyouwant))
end

