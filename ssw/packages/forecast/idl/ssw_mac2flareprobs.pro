function ssw_mac2flareprobs, macintosh
;
;+
;   Name: ssw_mac2flareprobs
;
;   Purpose: return GOES flare class proabilities for input MacIntosh class(es)
;    
;   Input Parameters:
;      macintosh - MacIntosh class (string or string array)
;
;   Output:
;      function returns structure vector of 
;         {macintosh:<string>,A:<float>,B<float>:,C:<float>,M<float>,X<float>}
;
;   Calling Seqeunce:
;      probstr=ssw_mac2flareprobs(macintosh_classes)

;   History:
;      18-may-2005 - S.L.Freeland - just wrapper for Greg Slater routine;
;-
common ssw_mac2flareprobs_blk,str_template

if not data_chk(str_template,/struct) then $
   str_template={MacIntosh:'',A:0.d,B:0.d,C:0.d,M:0.d,X:0.d}


if not data_chk(macintosh,/string) then begin 
   box_message,'Need one or more MacInstosh class as input'
   return,-1
endif

nout=n_elements(macintosh)
retval=replicate(str_template,nout)
probabilities=mac2flareprob(macintosh)

goeslevels=str2arr('A,B,C,M,X')

retval.macintosh=macintosh
for i=0,4 do retval.(i+1)=reform(probabilities(i,*))

return,retval
end

