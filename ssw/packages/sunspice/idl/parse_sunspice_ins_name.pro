;+
; Project     :	Multimission
;
; Name        :	PARSE_SUNSPICE_INS_NAME
;
; Purpose     :	Parses spacecraft,instrument name combination for SPICE input
;
; Category    :	SUNSPICE, Orbit
;
; Explanation :	This routine translates the combination of a spacecraft name
;               and instrument name into the ID code used by the JPL SPICE
;               software.  The spacecraft name is first parsed into it's
;               ID code by PARSE_SUNSPICE_NAME.  Then, depending on the
;               spacecraft, the routine will attempt to modify the ID based on
;               the instrument name.
;
; Syntax      :	Output = PARSE_SUNSPICE_INS_NAME( SPACECRAFT, INSTRUMENT )
;
; Examples    :	PARSE_SUNSPICE_INS_NAME('solo','spice') returns -144800
;
; Inputs      :	SPACECRAFT = Spacecraft name.  See PARSE_SUNSPICE_NAME.
;
;               INSTRUMENT = Instrument name, which must be one of a specified
;                            list depending on the spacecraft.  If omitted or
;                            blank, then '000' is appended to the spacecraft
;                            ID, representing the spacecraft itself.
;
; Outputs     : The result of the function is a long integer containing the
;               spacecraft/instrument ID to be passed to SPICE routines.  In
;               case of error, the result will be returned as 0.
;
; Opt. Outputs:	None.
;
; Keywords    : ERRMSG = If defined and passed, then any error messages will be
;                        returned to the user in this parameter rather than
;                        depending on the MESSAGE routine in IDL.  If no errors
;                        are encountered, then a null string is returned.  In
;                        order to use this feature, ERRMSG must be defined
;                        first, e.g.
;
;                        ERRMSG = ''
;                        ID = PARSE_SUNSPICE_INS_NAME( ERRMSG=ERRMSG, ... )
;                        IF ERRMSG NE '' THEN ...
;
; Calls       :	PARSE_SUNSPICE_NAME, VALID_NUM
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 11-May-2018, William Thompson, GSFC
;               Version 2, 30-Oct-2018, WTT, Added PSP, fixed bug add SC
;
; Contact     :	WTHOMPSON
;-
;
function parse_sunspice_ins_name, spacecraft, instrument, errmsg=errmsg
on_error, 2
;
;  Check the input values.
;
if n_elements(spacecraft) ne 1 then begin
    message = 'SPACECRAFT must be a scalar'
    goto, handle_error
endif
if n_elements(instrument) gt 1 then begin
    message = 'INSTRUMENT must be a scalar'
    goto, handle_error
endif
;
;  If the spacecraft is unrecognized, then flag an error.
;
sc = parse_sunspice_name(spacecraft)
if not valid_num(sc) then begin
    message = 'Unable to recognize spacecraft ' + strtrim(spacecraft,2)
    goto, handle_error
endif
;
;  If the instrument name was not passed, then use '000' to represent the
;  spacecraft.
;
if n_elements(instrument) eq 0 then ins = '000' else begin
;
;  If the instrument name was passed in as a number, then reformat into three
;  digits.  Otherwise, prepare for the case matches below.
;
    if valid_num(instrument) then $
      ins = string(instrument,format='(I3.3)') $
    else begin
        ins = strtrim(strupcase(instrument),2)
        if ins eq '' then ins = '000'
    endelse
endelse
;
;  Parse instrument names for Solar Orbiter.  If the name starts with "SOLO_",
;  then trim it off.
;
if sc eq '-144' then begin
    if strmid(ins,0,5) eq 'SOLO_' then ins = strmid(ins,5,strlen(ins)-5)
    case ins of
        'HGA':              ins = '013'
        'LGA_PZ':           ins = '020'
        'LGA_MZ':           ins = '021'
        'MGA':              ins = '032'
        'INS_BOOM':         ins = '040'
        'INS_BOOM_IB':      ins = '041'
        'INS_BOOM_OB':      ins = '042'
        'STR1':             ins = '051'
        'STR2':             ins = '052'
        'EPD_STEP':         ins = '100'
        'EPD_SIS_ASW':      ins = '111'
        'EPD_SIS_SW':       ins = '112'
        'EPD_EPT_MY':       ins = '123'
        'EPD_EPT_PY':       ins = '124'
        'EPD_HET_MY':       ins = '125'
        'EPD_HET_PY':       ins = '126'
        'EUI':              ins = '200'
        'EUI_FSI':          ins = '210'
        'EUI_HRI_LYA':      ins = '220'
        'EUI_HRI_EUV':      ins = '230'
        'MAG':              ins = '300'
        'MAG_IBS':          ins = '301'
        'MAG_OBS':          ins = '302'
        'METIS':            ins = '400'
        'METIS_EUV':        ins = '410'
        'METIS_EUV_MIN':    ins = '413'
        'METIS_EUV_MAX':    ins = '414'
        'METIS_VIS':        ins = '420'
        'METIS_VIS_MIN':    ins = '423'
        'METIS_VIS_MAX':    ins = '424'
        'METIS_IEO-M0':     ins = '430'
        'PHI':              ins = '500'
        'PHI_FDT':          ins = '510'
        'PHI_HRT':          ins = '520'
        'RPW':              ins = '600'
        'RPW_ANT_1':        ins = '610'
        'RPW_ANT_2':        ins = '620'
        'RPW_ANT_3':        ins = '630'
        'RPW_SCM':          ins = '640'
        'SOLOHI':           ins = '700'
        'SPICE':            ins = '800'
        'SPICE_SW':         ins = '810'
        'SPICE_LW':         ins = '820'
        'STIX':             ins = '850'
        'SWA':              ins = '870'
        'SWA_HIS':          ins = '871'
        'SWA_PAS':          ins = '872'
        'SWA_EAS':          ins = '873'
        else:
    endcase
endif
;
;  Parse instrument names for Parker Solar Probe.  If the name starts with
;  "SPP_" or "PSP_", then trim it off.
;
if sc eq '-96' then begin
    test = strmid(ins,0,4)
    if (test eq 'SPP_') or (test eq 'PSP_') then $
      ins = strmid(ins,4,strlen(ins)-4)
    case ins of
        'SOLARPANEL_PLUS':      ins = 001
        'SP_PLUS_BASE':         ins = 011
        'SOLARPANEL_MINUS':     ins = 002
        'SP_MINUS_BASE':        ins = 012
        'HIGH_GAIN_ANTENNA':    ins = 003
        'HGA_BASE':             ins = 013
        'DECK_1':               ins = 081
        'DECK_2':               ins = 082
        'DECK_3':               ins = 083
        'DECK_4':               ins = 084
        'DECK_5':               ins = 085
        'DECK_6':               ins = 086
        'WISPR_INNER':          ins = 100
        'WISPR_OUTER':          ins = 120
        'SWEAP_SPAN_A':         ins = 200
        'SWEAP_SPAN_A_ION':     ins = 201
        'SWEAP_SPAN_A_ELECTRON':ins = 202
        'SWEAP_SPAN_B':         ins = 203
        'SWEAP_SPC':            ins = 204
        'EPILO_BASE':           ins = 401  
        'EPILO_W0':             ins = 411 
        'EPILO_L00':            ins = 412
        'EPILO_L01':            ins = 413
        'EPILO_L02':            ins = 414
        'EPILO_L03':            ins = 415
        'EPILO_L04':            ins = 416
        'EPILO_L05':            ins = 417
        'EPILO_L06':            ins = 418
        'EPILO_L07':            ins = 419
        'EPILO_L08':            ins = 420
        'EPILO_L09':            ins = 421
        'EPILO_W1':             ins = 422 
        'EPILO_L10':            ins = 423
        'EPILO_L11':            ins = 424
        'EPILO_L12':            ins = 425
        'EPILO_L13':            ins = 426
        'EPILO_L14':            ins = 427
        'EPILO_L15':            ins = 428
        'EPILO_L16':            ins = 429
        'EPILO_L17':            ins = 430
        'EPILO_L18':            ins = 431
        'EPILO_L19':            ins = 432 
        'EPILO_W7':             ins = 488
        'EPILO_L70':            ins = 489
        'EPILO_L71':            ins = 490
        'EPILO_L72':            ins = 491
        'EPILO_L73':            ins = 492
        'EPILO_L74':            ins = 493
        'EPILO_L75':            ins = 494
        'EPILO_L76':            ins = 495
        'EPILO_L77':            ins = 496
        'EPILO_L78':            ins = 497
        'EPILO_L79':            ins = 498
    endcase
endif
;
;  Combine the spacecraft and instrument codes.  If this doesn't result
;  in a valid number, then flag an error.
;
ins = sc*1000L + sign(ins,sc)
if not valid_num(ins) then begin
    message = 'Unable to recognize instrument ' + strtrim(instrument,2)
    goto, handle_error
end else return, long(ins)
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message, /continue else $
  errmsg = 'PARSE_SUNSPICE_INS_NAME: ' + message
return, 0L
;
end
