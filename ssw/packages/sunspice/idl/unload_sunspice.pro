;+
; Project     :	Multimission
;
; Name        :	UNLOAD_SUNSPICE
;
; Purpose     :	Load spacecraft SPICE ephemeris and attitude kernels
;
; Category    :	SUNSPICE, Orbit
;
; Explanation :	Front-end routine to unload the spacecraft ephemeris and
;               attitude history files in SPICE format, based on the specified
;               spacecraft name or NAIF code.
;
; Syntax      :	UNLOAD_SUNSPICE, SPACECRAFT
;
; Inputs      :	SPACECRAFT = The name or NAIF numeric code of the spacecraft.
;                            See PARSE_SUNSPICE_NAME for more information about
;                            recognized names.
;
;                            Note that for missions consisting of multiple
;                            spacecraft, such as STEREO, the name of a specific
;                            spacecraft must be specified, rather than the name
;                            of the mission, e.g.
;
;                               UNLOAD_SUNSPICE, 'STEREO AHEAD'     [OK]
;                               UNLOAD_SUNSPICE, 'STEREO'           [Fails]
;
;                            Passing the name of either STEREO spacecraft will
;                            unload the ephemerides for both spacecraft.
;
; Opt. Inputs :	None
;
; Outputs     :	None
;
; Opt. Outputs:	None
;
; Keywords    :	See the individual mission-specific routines for information
;               about supported keywords.
;
;               ERRMSG = If defined and passed, then any error messages will be
;                        returned to the user in this parameter rather than
;                        depending on the MESSAGE routine in IDL.  If no errors
;                        are encountered, then a null string is returned.  In
;                        order to use this feature, ERRMSG must be defined
;                        first, e.g.
;
;                               ERRMSG = ''
;                               UNLOAD_SUNSPICE, spacecraft, ERRMSG=ERRMSG
;                               IF ERRMSG NE '' THEN ...
;
; Calls       :	TEST_SUNSPICE_DLM, PARSE_SUNSPICE_NAME, UNLOAD_SUNSPICE_STEREO
;
; Common      :	None
;
; Env. Vars.  : None
;
; Restrictions:	This procedure works in conjunction with the Icy/CSPICE
;               package, which is implemented as an IDL Dynamically Loadable
;               Module (DLM).  The Icy source code can be downloaded from
;
;                       ftp://naif.jpl.nasa.gov/pub/naif/toolkit/IDL
;
;               Because this uses dynamic frames, it requires Icy/CSPICE
;               version N0058 or higher.
;
; Side effects:	None
;
; Prev. Hist. :	None
;
; History     :	Version 1, 25-Apr-2016, William Thompson, GSFC
;               Version 2, 06-Feb-2017, WTT, support Solar Orbiter (-144)
;               Version 3, 27-Feb-2017, WTT, use catch instead of which
;               Version 4, 24-Mar-2017, WTT, support Solar Probe Plus (-96)
;               Version 5, 17-Oct-2017, WTT, changed SPP to PSP
;               Version 6, 08-Feb-2018, WTT, use ERR_STATE()
;
; Contact     :	WTHOMPSON
;-
;
pro unload_sunspice, spacecraft, errmsg=errmsg, _extra=_extra
on_error, 2
if n_params() ne 1 then begin
    message = 'A spacecraft name must be passed.'
    goto, handle_error
endif
;
;
;  Make sure that the SPICE/Icy DLM is available.
;
if not test_sunspice_dlm() then begin
    message = 'SPICE/Icy DLM not available'
    goto, handle_error
endif
;
;  Parse the spacecraft name, and act accordingly.
;
sc = parse_sunspice_name(spacecraft)
case sc of
    '-234': begin
        catch, error_status
        if error_status ne 0 then begin
            message = err_state()
            break
        endif
        unload_sunspice_stereo, _extra=_extra
    end
    '-235': begin
        catch, error_status
        if error_status ne 0 then begin
            message = err_state()
            break
        endif
        unload_sunspice_stereo, _extra=_extra
    end
    '-21': begin
        catch, error_status
        if error_status ne 0 then begin
            message = err_state()
            break
        endif
        unload_sunspice_soho, _extra=_extra
    end
    '-144': begin
        catch, error_status
        if error_status ne 0 then begin
            message = err_state()
            break
        endif
        unload_sunspice_solo, _extra=_extra
    end
    '-96': begin
        catch, error_status
        if error_status ne 0 then begin
            message = err_state()
            break
        endif
        unload_sunspice_psp, _extra=_extra
    end
    else: 
endcase
catch, /cancel
;
return
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message else $
  errmsg = 'UNLOAD_SUNSPICE: ' + message
;
end
