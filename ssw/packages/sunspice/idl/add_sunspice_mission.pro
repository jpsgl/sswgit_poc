;+
; Project     :	Multimission
;
; Name        :	ADD_SUNSPICE_MISSION
;
; Purpose     :	Adds mission-specific SunSPICE routines to path
;
; Category    :	SUNSPICE, Orbit
;
; Explanation :	If the mission-specific SunSPICE routines are not in the path,
;               and the routines are available within the local SSW tree, then
;               SSW_PATH is called to add them to the path.
;
; Syntax      :	LOAD_SUNSPICE_MISSION, SPACECRAFT
;
; Inputs      :	SPACECRAFT = The name or NAIF numeric code of the spacecraft.
;                            See PARSE_SUNSPICE_NAME for more information about
;                            recognized names.
;
;                            Note that for missions consisting of multiple
;                            spacecraft, such as STEREO, the name of a specific
;                            spacecraft must be specified, rather than the name
;                            of the mission, e.g.
;
;                               LOAD_SUNSPICE, 'STEREO AHEAD'     [OK]
;                               LOAD_SUNSPICE, 'STEREO'           [Fails]
;
;                            Passing the name of either STEREO spacecraft will
;                            load the software for the STEREO mission.
;
; Opt. Inputs :	None
;
; Outputs     :	None
;
; Opt. Outputs:	None
;
; Keywords    :	ERRMSG = If defined and passed, then any error messages will be
;                        returned to the user in this parameter rather than
;                        depending on the MESSAGE routine in IDL.  If no errors
;                        are encountered, then a null string is returned.  In
;                        order to use this feature, ERRMSG must be defined
;                        first, e.g.
;
;                               ERRMSG = ''
;                               ADD_SUNSPICE_MISSION, spacecraft, ERRMSG=ERRMSG
;                               IF ERRMSG NE '' THEN ...
;
; Calls       :	
;
; Common      :	None
;
; Env. Vars.  : None
;
; Restrictions:	This procedure works in conjunction with the Icy/CSPICE
;               package, which is implemented as an IDL Dynamically Loadable
;               Module (DLM).  The Icy source code can be downloaded from
;
;                       ftp://naif.jpl.nasa.gov/pub/naif/toolkit/IDL
;
;               Because this uses dynamic frames, it requires Icy/CSPICE
;               version N0058 or higher.
;
; Side effects:	None
;
; Prev. Hist. :	None
;
; History     :	Version 1, 28-Jul-2017, William Thompson, GSFC
;               Version 2, 03-Aug-2017, WTT, rename spp directory to psp
;                       Routine names unchanged.
;               Version 3, 17-Oct-2017, WTT, changed SPP to PSP
;
; Contact     :	WTHOMPSON
;-
;
pro add_sunspice_mission, spacecraft
on_error, 2
if n_params() ne 1 then begin
    message = 'A spacecraft name must be passed.'
    goto, handle_error
endif
;
;  Make sure that the SPICE/Icy DLM is available.
;
if not test_sunspice_dlm() then begin
    message = 'SPICE/Icy DLM not available'
    goto, handle_error
endif
;
;  Parse the spacecraft name, and act accordingly.
;
sc = parse_sunspice_name(spacecraft)
temp = 'Dummy'
case sc of
    '-234': begin
        which, 'load_sunspice_stereo', /quiet, outfile=temp
        path = 'stereo/gen/idl/spice'
    end
    '-235': begin
        which, 'load_sunspice_stereo', /quiet, outfile=temp
        path = 'stereo/gen/idl/spice'
    end
    '-21': begin
        which, 'load_sunspice_soho', /quiet, outfile=temp
        path = 'soho/gen/idl/spice'
    end
    '-144': begin
        which, 'load_sunspice_solo', /quiet, outfile=temp
        path = 'so/gen/idl/sunspice'
    end
    '-96': begin
        which, 'load_sunspice_psp', /quiet, outfile=temp
        path = 'psp/gen/idl/spice'
    end
    else: 
endcase
;
;  If the mission software is already in the path, or the mission is
;  unrecognized, then simply return.
;
if temp ne '' then return
;
;  If the path to the mission software exists, then add it to IDL's path.
;
path = concat_dir(getenv('SSW'), path)
if dir_exist(path) then ssw_path, path
return
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message else $
  errmsg = 'ADD_SUNSPICE_MISSION: ' + message
;
end
