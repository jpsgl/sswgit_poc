;+
; Project     :	Multimission
;
; Name        :	GET_SUNSPICE_HPC_POINT
;
; Purpose     :	Returns the helioprojective pointing of a spacecraft.
;
; Category    :	SUNSPICE, Orbit
;
; Explanation :	This routine returns the helioprojective-cartesian pointing of
;               a spacecraft.  The yaw (X) and pitch (Y) are returned in arc
;               seconds, and the roll is returned in degrees.
;
; Syntax      :	Pointing = GET_SUNSPICE_HPC_POINT( DATE, SPACECRAFT )
;
; Examples    :	Pointing = GET_SUNSPICE_HPC_POINT( '2006-05-06T11:30:00', 'A' )
;
; Inputs      :	DATE       = The date and time.  This can be input in any
;                            format accepted by ANYTIM2UTC, and can also be an
;                            array of values.
;
;               SPACECRAFT = The name or NAIF numeric code of a spacecraft.
;                            See PARSE_SUNSPICE_NAME for more information about
;                            recognized names.  Case is not important.
;
; Opt. Inputs :	None.
;
; Outputs     :	The result of the function is a three-element vector containing
;               in order the yaw (X) and pitch (Y) in arcseconds, and the roll
;               angle in degrees.  If DATE is a vector, then the result will
;               have additional dimensions.
;
; Opt. Outputs:	None.
;
; Keywords    : INSTRUMENT = The name of an instrument reference frame.  See
;                            GET_SUNSPICE_CMAT for more information.
;
;                            The default is to return the C-matrix for the
;                            spacecraft as a whole, rather than for any
;                            specific instrument.
;
;               TOLERANCE = The tolerance to be used when looking for pointing
;                            information, in seconds.  The default is 1000.
;
;               FOUND  = Byte array containing whether or not the pointings
;                        were found.
;
;               DEGREES= If set, then the units for all three parameters will
;                        be degrees.
;
;               RADIANS= If set, then the units for all three parameters will
;                        be radians.
;
;               POST_CONJUNCTION= A STEREO-specific keyword.  If set, then
;                                 modify the roll angle by 180 degrees if the
;                                 date is after 2015-05-19.  Earlier dates are
;                                 unaffected, as are the yaw and pitch values.
;
;               ERRMSG = If defined and passed, then any error messages will be
;                        returned to the user in this parameter rather than
;                        depending on the MESSAGE routine in IDL.  If no errors
;                        are encountered, then a null string is returned.  In
;                        order to use this feature, ERRMSG must be defined
;                        first, e.g.
;
;                               ERRMSG = ''
;                               Pnt = GET_SUNSPICE_HPC_POINT( ERRMSG=ERRMSG, ... )
;                               IF ERRMSG NE '' THEN ...
;
;               Will also accept any LOAD_SUNSPICE or ANYTIM2UTC keywords.
;
; Calls       :	DATATYPE, GET_SUNSPICE_CMAT, CSPICE_M2EUL, PARSE_SUNSPICE_NAME
;
; Common      :	None.
;
; Restrictions:	At least one CK file must be loaded.
;
;               This procedure works in conjunction with the Icy/CSPICE
;               package, which is implemented as an IDL Dynamically Loadable
;               Module (DLM).  The Icy source code can be downloaded from
;
;                       ftp://naif.jpl.nasa.gov/pub/naif/toolkit/IDL
;
;               Because this uses dynamic frames, it requires Icy/CSPICE
;               version N0058 or higher.
;
; Side effects:	Will automatically load the SPICE ephemeris files, if not
;               already loaded.
;
; Prev. Hist. :	Based on STEREO_POINTING_DEMO
;
; History     :	Version 1, 17-Oct-2005, William Thompson, GSFC
;               Version 2, 01-Sep-2006, William Thompson, GSFC
;                       Added call to PARSE_SUNSPICE_NAME
;               Version 3, 16-Apr-2006, William Thompson, GSFC
;                       Fix bug with short integers in loop.
;               Version 4, 17-Jun-2015, William Thompson, GSFC
;                       Added keyword POST_CONJUNCTION
;               Version 5, 29-Apr-2016, WTT, renamed from GET_STEREO_HPC_POINT
;               Version 6, 07-May-2018, WTT, incorporate Solar Orbiter
;		Version 7, 10-May-2018, WTT, correct Orbiter pitch and yaw
;               Version 8, 23-Oct-2018, WTT, add Parker Solar Probe
;		Version 9, 09-Nov-2018, WTT, catch error when pitch > +/-90
;		Version 10, 20-Nov-2018, WTT, updated PSP calculation by
;			recasting into STEREO-like axes.
;
; Contact     :	WTHOMPSON
;-
;
function get_sunspice_hpc_point, date, spacecraft, found=found, $
                               precess=precess, instrument=instrument, $
                               tolerance=tolerance, degrees=degrees, $
                               post_conjunction=post_conjunction, $
                               radians=radians, errmsg=errmsg, _extra=_extra
;
on_error, 2
if n_params() ne 2 then begin
    message = 'Syntax:  Pointing = GET_SUNSPICE_HPC_POINT( DATE, SPACECRAFT )'
    goto, handle_error
endif
;
;  Define the units.
;
roll_units = 180.d0 / !dpi
xy_units = roll_units * 3600.d0
if keyword_set(degrees) then xy_units = roll_units
if keyword_set(radians) then begin
    roll_units = 1
    xy_units   = 1
endif
;
;  Determine which spacecraft was requested, and translate it into the proper
;  input for SPICE.
;
inst = 0L
sc_ahead  = '-234'
sc_behind = '-235'
sc_solo   = '-144'
sc_psp    = '-96'
sc = parse_sunspice_name(spacecraft)
sc_stereo = (sc eq sc_ahead) or (sc eq sc_behind)
;
;  Start by deriving the C-matrices.  Make sure that DATE is treated as a
;  vector.
;
message = ''
cmat = get_sunspice_cmat(date[*], spacecraft, system='HPC', found=found, $
                         instrument=instrument, tolerance=tolerance, $
                         errmsg=message, _extra=_extra)
if message ne '' then goto, handle_error
;
sz = size(cmat)
if sz[0] eq 2 then begin
    n = 1
    pointing = dblarr(3)
end else begin
    n = sz[3]
    pointing = dblarr(3,n)
endelse
;
halfpi = !dpi / 2.d0
twopi  = !dpi * 2.d0
;
for i=0L,n-1L do begin
;
;  Parker Solar Probe
;
    if sc eq '-96' then begin
	cmat0 = [[0,0,1],[-1,0,0],[0,-1,0]] ## cmat[*,*,i]
        cspice_m2eul, cmat0, 1, 3, 2, roll, pitch, yaw
	yaw = halfpi - yaw
        roll = roll + halfpi
        if abs(roll) gt !dpi then roll = roll - sign(twopi, roll)
;
;  STEREO or Solar Orbiter
;
    end else begin
        cspice_m2eul, cmat[*,*,i], 1, 3, 2, roll, pitch, yaw
        yaw = halfpi - yaw
        if sc eq sc_solo then roll = roll + halfpi
        if sc eq sc_behind then begin
            roll = roll + !dpi
            if roll gt !dpi then roll = roll - twopi
        endif
    endelse
;
;  If the POST_CONJUNCTION keyword was set, then modify the roll angle by 180
;  degrees.
;
    if keyword_set(post_conjunction) and sc_stereo then begin
        if anytim2utc(date[i],/ccsds) ge '2015-05-19' then begin
            roll = roll + !dpi
            if roll gt !dpi then roll = roll - twopi
        endif
    endif
;
;  Correct any cases where the pitch is greater than +/- 90 degrees
;
    if abs(pitch) gt halfpi then begin
        pitch = sign(!dpi,pitch) - pitch
        yaw = yaw - sign(!dpi, yaw)
        roll = roll - sign(!dpi, roll)
    endif
;
;  Apply the units.
;
    pointing[0,i] = yaw * xy_units
    pointing[1,i] = pitch * xy_units
    pointing[2,i] = -roll * roll_units
endfor
;
;  Reformat the output arrays to match the input date/time array.
;
if n gt 1 then begin
    sz = size(date)
    dim = [3,sz[1:sz[0]]]
    pointing = reform(pointing, dim, /overwrite)
endif
;
return,pointing
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message else $
  errmsg = 'GET_SUNSPICE_HPC_POINT: ' + message
;
end
