;+
; Project     :	Multimission
;
; Name        :	GET_SUNSPICE_OBSGEO
;
; Purpose     :	Return ground observatory geodetic X,Y,Z coordinates
;
; Category    :	SUNSPICE
;
; Explanation :	Uses the SunSPICE package to calculate the geodetic X,Y,Z
;               coordinates of a ground-based observatory for inclusion in FITS
;               headers in the keywords OBSGEO-X, OBSGEO-Y, and OBSGEO-Z.
;
; Syntax      :	GET_SUNSPICE_OBSGEO, LON, LAT, ALT, OBSGEO
;
; Examples    :	GET_SUNSPICE_OBSGEO, -17.880757, 28.759693, 2426, OBSGEO
;
; Inputs      :	LON     = Observatory longitude, in degrees
;               LAT     = Observatory latitude, in degrees
;               ALT     = Observatory altitude, in meters
;
; Opt. Inputs :	None
;
; Outputs     :	OBSGEO  = Three element vector giving the X,Y,Z coordinates of
;                         the observatory in meters.
;
; Opt. Outputs:	None
;
; Keywords    :	None
;
; Calls       :	LOAD_SUNSPICE_GEN
;
; Common      :	None
;
; Restrictions:	None
;
; Side effects:	None
;
; Prev. Hist. :	None
;
; History     :	Version 1, 05-Jan-2016, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
pro get_sunspice_obsgeo, lon, lat, alt, obsgeo
;
on_error, 2
;
if n_params() ne 4 then message, $
  'Syntax: GET_SUNSPICE_OBSGEO, LON, LAT, ALT, OBSGEO'
if n_elements(lon) eq 0 then message, 'LON not defined'
if n_elements(lat) eq 0 then message, 'LAT not defined'
if n_elements(alt) eq 0 then message, 'ALT not defined'
;
load_sunspice_gen                       ;Load general ephemerides
cspice_bodvar, 399, 'RADII', radii      ;Get Earth radii (km)
flat = (radii[0] -radii[2]) / radii[0]  ;Derive flatness parameter
dtor = !dpi / 180.d0
cspice_georec, lon*dtor, lat*dtor, alt, radii[0]*1000, flat, obsgeo
;
end
