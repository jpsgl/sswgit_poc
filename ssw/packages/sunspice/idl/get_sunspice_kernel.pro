;+
; Project     :	Multimission
;
; Name        :	GET_SUNSPICE_KERNEL
;
; Purpose     :	Returns the SPICE kernel containing ephemeris data
;
; Category    :	SUNSPICE, Orbit
;
; Explanation :	This routine checks through the various loaded kernels, and
;               returns the name of the kernel containing the ephemeris or
;               attitude history information for a given date and spacecraft.
;
; Syntax      :	Kernel = GET_SUNSPICE_KERNEL( DATE, SPACECRAFT )
;
; Examples    :	Kernel = GET_SUNSPICE_KERNEL( '2006-05-06T11:30:00', 'A' )
;
; Inputs      :	DATE       = The date and time.  This can be input in any
;                            format accepted by ANYTIM2UTC, and can also be an
;                            array of values.
;
;               SPACECRAFT = The name or NAIF numeric code of a spacecraft.
;                            See PARSE_SUNSPICE_NAME for more information about
;                            recognized names.  Case is not important.
;
; Opt. Inputs :	None.
;
; Outputs     :	The result of the function is the name of the loaded kernel(s)
;               containing the ephemeris or attitude history information.  The
;               null string is returned for any cases without a match.
;
; Opt. Outputs:	None.
;
; Keywords    : ATTITUDE = If set, then the program will look for attitude
;                          history files instead of ephemeris files.
;
;               ERRMSG = If defined and passed, then any error messages will be
;                        returned to the user in this parameter rather than
;                        depending on the MESSAGE routine in IDL.  If no errors
;                        are encountered, then a null string is returned.  In
;                        order to use this feature, ERRMSG must be defined
;                        first, e.g.
;
;                               ERRMSG = ''
;                               Kernel = GET_SUNSPICE_KERNEL( ERRMSG=ERRMSG, ... )
;                               IF ERRMSG NE '' THEN ...
;
;               Will also accept any LOAD_SUNSPICE or ANYTIM2UTC keywords.
;
; Calls       :	PARSE_SUNSPICE_NAME, LOAD_SUNSPICE, BREAK_FILE, GET_SUNSPICE_RANGE,
;               ANYTIM2UTC, LOAD_SUNSPICE_ATT
;
; Common      :	None.
;
; Restrictions:	This procedure works in conjunction with the Icy/CSPICE
;               package, which is implemented as an IDL Dynamically Loadable
;               Module (DLM).  The Icy source code can be downloaded from
;
;                       ftp://naif.jpl.nasa.gov/pub/naif/toolkit/IDL
;
; Side effects:	Will automatically load the SPICE ephemeris files, if not
;               already loaded.
;
; Prev. Hist. :	Based on GET_STEREO_KERNEL
;
; History     :	Version 1, 29-Apr-2016, William Thompson, GSFC
;               Version 2, 22-Feb-2018, WTT, pass keywords to PARSE_SUNSPICE_NAME
;               Version 3, 06-Feb-2019, WTT, fix problem with PSP attitude files
;
; Contact     :	WTHOMPSON
;-
;
function get_sunspice_kernel, date, spacecraft, attitude=attitude, $
                                  errmsg=errmsg, _extra=_extra
;
on_error, 2
if n_params() ne 2 then begin
    message = 'Syntax:  Kernel = GET_SUNSPICE_KERNEL( DATE, SPACECRAFT )'
    goto, handle_error
endif
;
;  Determine which spacecraft (or planetary body) was requested, and translate
;  it into the proper input for SPICE.
;
sc  = parse_sunspice_name(spacecraft, _extra=_extra)
;
;  Convert the date/time to UTC.
;
message = ''
utc = anytim2utc(date, /ccsds, errmsg=message, _extra=_extra)
if message ne '' then goto, handle_error
;
;  Initialize the KERNEL array.
;
kernel = utc
kernel[*] = ''
;
;  Make sure that the ephemeris files are loaded.
;
message = ''
if keyword_set(attitude) then $
  load_sunspice_att, sc, date, _extra=_extra else $
  load_sunspice, sc, errmsg=message, _extra=_extra
if message ne '' then goto, handle_error
;
;  Get a report of all loaded kernels.
;
list_sunspice_kernels, kernels=loaded, /quiet, errmsg=message
if message ne '' then goto, handle_error
;
;  Find the kernels with the correct extension.
;
break_file, loaded, disk, dir, name, ext
len = strlen(ext)
if keyword_set(attitude) then test = '.bc' else test = '.bsp'
w = where(strpos(ext,test) ge 0, count)
if count eq 0 then return, kernel       ;No matches found
loaded = loaded[w]
name = name[w]
ext = ext[w]
;
;  Step through the cases, and get the matches.
;
for i=0,n_elements(loaded)-1 do begin
    message = ''
    get_sunspice_range, loaded[i], date0, date1, scid, /ccsds, errmsg=message
    if keyword_set(attitude) then scid = scid[0] / 1000
    if message ne '' then goto, handle_error
;
    if where(scid eq sc) ge 0 then begin
        w = where((utc ge date0) and (utc le date1), count)
        if count gt 0 then kernel[w] = name[i] + ext[i]
    endif
endfor
;
return, kernel
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message else $
  errmsg = 'GET_SUNSPICE_KERNEL: ' + message
;
end
