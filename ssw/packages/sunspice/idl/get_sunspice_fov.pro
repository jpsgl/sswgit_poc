;+
; Project     :	Multimission
;
; Name        :	GET_SUNSPICE_FOV
;
; Purpose     :	Returns the field-of-view (FOV) parameters for an instrument
;
; Category    :	SUNSPICE, Orbit, Attitude
;
; Explanation :	This routine calls CSPICE_GETFOV to get the field-of-view
;               parameters of an instrument.
;
; Syntax      :	GET_SUNSPICE_FOV, SPACECRAFT, INSTRUMENT, SHAPE, FRAME, $
;                       BSIGHT, BOUNDS
;
; Examples    :	GET_SUNSPICE_FOV, 'SPP', 'SPP_WISPR_INNER', SHAPE, FRAME, $
;                       BSIGHT, BOUNDS
;
; Inputs      :	SPACECRAFT = The name or NAIF numeric code of a spacecraft.
;                            See PARSE_SUNSPICE_NAME for more information about
;                            recognized names.  This parameter is primarily
;                            used to make sure that the proper ephemeris files
;                            are loaded, but can also be used in conjunction
;                            with the SYSTEM keyword to specify a
;                            mission-specific coordinate frame.
;
;               INSTRUMENT = The name of an instrument whose field-of-view is
;                            specified in a .ti instrument kernel.
;
; Opt. Inputs :	None
;
; Outputs     :	SHAPE   = A string describing the FOV shape.  Possible values:
;                         "POLYGON", "RECTANGLE", "CIRCLE", "ELLIPSE".
;
;               FRAME   = A string identifying the frame in which the FOV is
;                         defined.
;
;               BSIGHT  = A double precision 3-vector pointing in the direction
;                         of the FOV center (boresight).
;
;               BOUNDS  = An array of 3-vectors pointing to the "corners"
;                         of the instrument FOV.
;
; Opt. Outputs:	None
;
; Keywords    :	ROOM    = The maximum number of double-precision BOUNDS vectors
;                         to return.  Default is 10, which is usually adequate.
;
;               SYSTEM  = Optional coordinate system for BSIGHT and BOUNDS.
;                         Can be one of the following standard coordinate
;                         systems:
;
;                               GEI     Geocentric Equatorial Inertial
;                               GEO     Geographic
;                               GSE     Geocentric Solar Ecliptic
;                               GAE     Geocentric Aries Ecliptic
;                               MAG     Geomagnetic
;                               GSM     Geocentric Solar Magnetospheric
;                               SM      Solar Magnetic
;                               HCI     Heliocentric Inertial (default)
;                               HAE     Heliocentric Aries Ecliptic
;                               HEE     Heliocentric Earth Ecliptic
;                               HEEQ    Heliocentric Earth Equatorial (or HEQ)
;                               Carrington (can be abbreviated)
;                               HGRTN   Heliocentric Radial-Tangential-Normal
;                               RTN     Radial-Tangential-Normal
;                               SCI     STEREO Science Pointing
;                               HERTN   Heliocentric Ecliptic RTN
;
;                         Note that the output parameter FRAME will depend on
;                         SYSTEM.  For example, if SYSTEM='GEI', then FRAME
;                         will be returned as 'J2000'.  For magnetic
;                         coordinates, FRAME is the name of the coordinate
;                         system.
;
;               DATE    = Observation date, required when SYSTEM is specified.
;                         Must be single-valued.
;
;               PRECESS = If set, then ecliptic coordinates are precessed from
;                         the J2000 reference frame to the mean ecliptic of
;                         date.  Only used for HAE/GAE.  Default is PRECESS=0.
;                         GSE and HEE use the ecliptic of date by definition.
;
;               ITRF93 = If set, then use the high precision Earth PCK files
;                        loaded by LOAD_SUNSPICE_EARTH instead of the default
;                        IAU_EARTH frame.  Only relevant for GEO, MAG, GSM, and
;                        SM coordinates.
;
;               ERRMSG = If defined and passed, then any error messages will be
;                        returned to the user in this parameter rather than
;                        depending on the MESSAGE routine in IDL.  If no errors
;                        are encountered, then a null string is returned.  In
;                        order to use this feature, ERRMSG must be defined
;                        first, e.g.
;
;                               ERRMSG = ''
;                               GET_SUNSPICE_FOV, ERRMSG=ERRMSG, ...
;                               IF ERRMSG NE '' THEN ...
;
; Calls       :	LOAD_SUNSPICE, VALID_NUM, CSPICE_BODC2N, CSPICE_BODN2C,
;               LOAD_SUNSPICE_ATT, PARSE_SUNSPICE_NAME, LOAD_SUNSPICE_EARTH,
;               ANYTIM2UTC, CSPICE_STR2ET, CSPICE_PXFORM,
;               CONVERT_SUNSPICE_GEO2MAG, CONVERT_SUNSPICE_GSE2GSM,
;               CONVERT_SUNSPICE_GSE2SM
;
; Common      :	None
;
; Restrictions:	None
;
; Side effects:	This procedure works in conjunction with the Icy/CSPICE
;               package, which is implemented as an IDL Dynamically Loadable
;               Module (DLM).  The Icy source code can be downloaded from
;
;                       ftp://naif.jpl.nasa.gov/pub/naif/toolkit/IDL
;
;               Because this uses dynamic frames, it requires Icy/CSPICE
;               version N0058 or higher.
;
; Prev. Hist. :	None
;
; History     :	Version 1, 11-Feb-2019, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
pro get_sunspice_fov, spacecraft, instrument, shape, frame, bsight, bounds, $
                      room=room, system=k_system, date=date, precess=precess, $
                      itrf93=itrf93, errmsg=errmsg, _extra=_extra
;
on_error, 2
if n_elements(room) eq 0 then room = 10
;
;  Make sure that the mission ephemerides are loaded.
;
message = ''
load_sunspice, spacecraft, errmsg=message, _extra=_extra
if message ne '' then goto, handle_error
;
;  Get the instrument ID.
;
if valid_num(instrument) then begin
    instid = long(instrument)
    cspice_bodc2n, instid, name, found
end else cspice_bodn2c, instrument, instid, found
if not found then begin
    message = 'Instrument "' + ntrim(instrument) + '" not recognized.'
    goto, handle_error
endif
;
;  Get the field-of-view in the instrument frame.
;
catch, error_status
if error_status ne 0 then begin
    catch, /cancel
    message = !error_state.msg
    goto, handle_error
endif
cspice_getfov, instid, room, shape, frame, bsight, bounds
catch, /cancel
;
;  If SYSTEM was specified, then convert to the output frame.
;
if n_elements(k_system) eq 1 then begin
    outframe = ''
;
;  Load the attitude history file for the specified date.
;
    if n_elements(date) gt 1 then begin
        message = 'DATE must be scalar'
        goto, handle_error
    endif
    message = ''
    if n_elements(date) eq 1 then $
      load_sunspice_att, spacecraft, date, errmsg=message
    if message ne '' then goto, handle_error
;
    system = strupcase(k_system)
    if system eq 'HEQ' then system = 'HEEQ'
    if system eq strmid('CARRINGTON',0,strlen(system)) then $
      system = 'CARRINGTON'
;
;  Radial-tangential-normal coordinate systems require mission-specific frame
;  definition files to be loaded.
;
    if (system eq 'HGRTN') or (system eq 'RTN') then begin
        sc = parse_sunspice_name(spacecraft)
        if sc eq '-234' then begin
            outframe = 'STAHGRTN'
        end else if sc eq '-235' then begin
            outframe = 'STBHGRTN'
        end else if sc eq '-21' then begin
            outframe = 'SOHOHGRTN'
        end else if sc eq '-144' then begin
            outframe = 'SOLOHGRTN'
        end else if sc eq '-96' then begin
            outframe = 'PSPHGRTN'
        end else begin
            if not !quiet then print, 'Assuming Earth observation'
            outframe = 'GEORTN'
        endelse
    endif
;
;  The STEREO Science Pointing frame requires a mission-specific frame
;  definition file to be loaded.
;
    if (system eq 'SCI') then begin
        sc = parse_sunspice_name(spacecraft)
        if sc eq '-234' then begin
            outframe = 'STASCPNT'
        end else if sc eq '-235' then begin
            outframe = 'STBSCPNT'
        end else begin
            message = 'Unable to recognize spacecraft specification'
            goto, handle_error
        endelse
    endif
;
;  The Helioecliptic Radial-Tangential-Normal frame requires a mission-specific
;  frame definition file to be loaded.
;
    if (system eq 'HERTN') then begin
        sc = parse_sunspice_name(spacecraft)
        if sc eq '-234' then begin
            outframe = 'STAHERTN'
        end else if sc eq '-235' then begin
            outframe = 'STBHERTN'
        end else if sc eq '-21' then begin
            outframe = 'SOHOHERTN'
        end else if sc eq '-144' then begin
            outframe = 'SOLOHERTN'
        end else if sc eq '-96' then begin
            outframe = 'PSPHERTN'
        end else begin
            message = 'Unable to recognize spacecraft specification'
            goto, handle_error
        endelse
    endif
;
;  Determine whether or not the ITRF93 kernels for Earth should be loaded.
;
    if keyword_set(itrf93) then begin
        message = ''
        load_sunspice_earth, errmsg=message, _extra=_extra
        if message ne '' then goto, handle_error
        earth_frame = 'ITRF93'
    end else earth_frame = 'IAU_EARTH'
;
;
;  Based on the coordinate system requested, get the equivalent frame.
;
    if outframe eq '' then case system of
        'GEI': outframe = 'J2000'
        'GEO': outframe = earth_frame
        'MAG': outframe = earth_frame
        'GSE': outframe = 'GSE'
        'GSM': outframe = 'GSE'
        'SM':  outframe = 'GSE'
        'GAE': if keyword_set(precess) then outframe='ECLIPDATE' else $
          outframe='ECLIPJ2000'
        'HCI': outframe = 'HCI'
        'HAE': if keyword_set(precess) then outframe='ECLIPDATE' else $
          outframe='ECLIPJ2000'
        'HEE': outframe = 'HEE'
        'HEEQ': outframe = 'HEEQ'
        'CARRINGTON': outframe = 'IAU_SUN'
        'HGRTN': 
        'RTN': 
        'SCI': 
        'HERTN': 
        else: begin
            message = 'Unrecognized coordinate system'
            goto, handle_error
        endelse
    endcase
;
;  Convert the date to ephemeris time.
;
    message = ''
    utc = anytim2utc(date, /ccsds, errmsg=message, _extra=_extra)
    if message ne '' then goto, handle_error
    cspice_str2et, utc, et
;
;  Convert to the new coordinate system.
;
    cspice_pxform, frame, outframe, et, rotate
    bsight = matrix_multiply(rotate, bsight, /atranspose)
    bounds = matrix_multiply(rotate, bounds, /atranspose)
    frame = outframe
;
;  Magnetic coordinates require an extra step.
;
    case system of
        'MAG': begin
            convert_sunspice_geo2mag, utc, bsight
            convert_sunspice_geo2mag, utc, bounds
            frame = 'MAG'
        end
        'GSM': begin
            convert_sunspice_gse2gsm, utc, bsight, itrf93=itrf93
            convert_sunspice_gse2gsm, utc, bounds, itrf93=itrf93
            frame = 'GSM'
        end
        'SM': begin
            convert_sunspice_gse2sm, utc, bsight, itrf93=itrf93
            convert_sunspice_gse2sm, utc, bounds, itrf93=itrf93
            frame = 'SM'
        end
        else:
    endcase
endif
return
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message else $
  errmsg = 'GET_SUNSPICE_FOV: ' + message
;
end
