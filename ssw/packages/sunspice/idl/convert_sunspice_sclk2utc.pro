;+
; Project     :	Multimission
;
; Name        :	CONVERT_SUNSPICE_SCLK2UTC()
;
; Purpose     :	Converts spacecraft clock values to UTC (or TAI) date/time.
;
; Category    :	SUNSPICE, Time
;
; Explanation :	This routine takes a spacecraft clock strings and converts them
;               into standard date/time formats.
;
; Syntax      :	Result = CONVERT_SUNSPICE_SCLK2UTC( SPACECRAFT, CLKSTR )
;
; Examples    :	UTC = CONVERT_SUNSPICE_SCLK2UTC('orbiter', '521651623:37539')
;
; Inputs      :	SPACECRAFT = The name of the spacecraft.  Must be recognized by
;                            the routine PARSE_SUNSPICE_NAME.  Alternatively,
;                            one can enter in the numeric code of the
;                            spacecraft, e.g. -144 for Solar Orbiter.
;
;               CLKSTR  = Spacecraft SCLK clock string.  The format of this is
;                         mission dependent.  For example, valid string values
;                         for the Solar Orbiter mission would be
;                         '521651623:37539' or '1/0521651623:37539'.
;
; Opt. Inputs :	None.
;
; Outputs     :	The result of the function is the UTC time corresponding to the
;               input spacecraft clock string.  If an error is encountered,
;               then the scalar null string '' will be returned.
;
; Opt. Outputs:	None.
;
; Keywords    :	TAI     = If set, then UTC2TAI is called to convert to a double
;                         precision floating point TAI value.
;
;               ASIS    = If set, then don't call ANYTIM2UTC.  The date
;                         and time are returned in CCSDS format,
;                         e.g. '2016-07-12T15:13:46.381'.
;
;               ERRMSG = If defined and passed, then any error messages will be
;                        returned to the user in this parameter rather than
;                        depending on the MESSAGE routine in IDL.  If no errors
;                        are encountered, then a null string is returned.  In
;                        order to use this feature, ERRMSG must be defined
;                        first, e.g.
;
;                               ERRMSG = ''
;                               utc = CONVERT_SUNSPICE_SCLK2UTC( ERRMSG=ERRMSG, ... )
;                               IF ERRMSG NE '' THEN ...
;
;                       Note also that the result of the function will be the
;                       null string if an error is encountered, regardless of
;                       whether or not ERRMSG is defined.
;
;               Will also accept any LOAD_SUNSPICE, UTC2TAI, or ANYTIM2UTC keywords.
;
; Calls       :	PARSE_SUNSPICE_NAME, VALID_NUM, LOAD_SUNSPICE, UTC2TAI, ANYTIM2UTC
;
; Common      :	None
;
; Restrictions:	This procedure works in conjunction with the Icy/CSPICE
;               package, which is implemented as an IDL Dynamically Loadable
;               Module (DLM).  The Icy source code can be downloaded from
;
;                       ftp://naif.jpl.nasa.gov/pub/naif/toolkit/IDL
;
;               Because this uses dynamic frames, it requires Icy/CSPICE
;               version N0058 or higher.
;
; Side effects:	Will automatically load the SPICE ephemeris files, if not
;               already loaded.
;
; Prev. Hist. :	Based on Python code supplied by the Solar Orbiter operations
;               team.
;
; History     :	Version 1, 16-Nov-2016, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
function convert_sunspice_sclk2utc, spacecraft, clkstr, tai=tai, asis=asis, $
                                    errmsg=errmsg, _extra=_extra
;
;  Parse the name of the spacecraft.  If not recognized as a spacecraft, i.e. a
;  negative number, then go to the error handling point.
;
message = ''
sc = parse_sunspice_name(spacecraft)
if not valid_num(sc) then begin
    message = 'Unrecognized spacecraft name "' + string(spacecraft) + '"'
    goto, handle_error
endif
if sc ge 0 then begin
    message = '"' + string(spacecraft) + '" is not a spacecraft'
    goto, handle_error
endif
;
;  Make sure that the ephemeris files for this spacecraft are loaded.
;
load_sunspice, sc, errmsg=message
if message ne '' then goto, handle_error
;
;  Create an array to return the results in.
;
sz = size(clkstr)
if sz[0] eq 0 then result = '' else begin
    dim = sz[1:sz[0]]
    result = make_array(dimension=dim, /string)
endelse
;
;  Catch any errors.
;
catch, error_status
if error_status ne 0 then begin
    message = !error_state.msg
    catch, /cancel
    goto, handle_error
endif
;
;  Convert from spacecraft clock string to ephemeris time.
;
for i=0,n_elements(clkstr)-1 do begin
    cspice_scs2e, long(sc), clkstr[i], et
;
;  Convert from ephemeris time to UTC string.
;
    cspice_et2utc, et, 'ISOC', 3, utc
    result[i] = utc
endfor
;
;  If the /TAI keyword was passed, then convert to TAI time.
;
if keyword_set(tai) then return, utc2tai(result, _extra=_extra)
;
;  Otherwise, use ANYTIM2UTC to convert to various time formats, or simply
;  return the result as is.
;
if keyword_set(asis) then return, result else return, anytim2utc(result, _extra=_extra)
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message, /continue else $
  errmsg = 'CONVERT_SUNSPICE_SCLK2UTC: ' + message
return, ''
;
end
