;+
; Project     :	Multimission
;
; Name        :	GET_SUNSPICE_RADVEL()
;
; Purpose     :	Get radial velocity of Sun relative to observer
;
; Category    :	SUNSPICE
;
; Explanation :	This routine returns the radial velocity of the Sun relative to
;               the observer.  There are two modes of operation, depending on
;               whether the observations are made from a spacecraft with
;               ephemerides supported by the SunSPICE package, or from a
;               ground-based observatory.  Each has it's own calling
;               sequence, as shown below.
;
; Syntax      :	Result = GET_SUNSPICE_RADVEL(DATE, SPACECRAFT=SPACECRAFT)
;               Result = GET_SUNSPICE_RADVEL(DATE, LON, LAT [, ALT ])
;
; Examples    :	Result = GET_SUNSPICE_RADVEL('2017-09-19T12:30', SPACECRAFT='SOHO')
;               Result = GET_SUNSPICE_RADVEL('2017-09-19T12:30', -156.3, 20.7, 3084.)
;
; Inputs      :	DATE    = Array of date-time values, in any format recognized
;                         by the routine ANYTIM2UTC.
;
;               LON     = Observatory geodetic longitude, in degrees.
;
;               LAT     = Observatory geodetic latitude, in degrees.
;
;               Note that LON, LAT are not required, and are ignored, if the
;               SPACECRAFT keyword is passed.
;
; Opt. Inputs :	ALT     = Observatory geodetic altitude, in meters.  If not
;                         passed, then assumed to be zero.
;
; Outputs     : The result of the function is the solar radial velocity, in
;               meters per second.
;
; Opt. Outputs:	None.
;
; Keywords    :	SPACECRAFT = Name of the spacecraft from which the observations
;                            are made.  Can also be the name of a solar system
;                            body, e.g. "Earth", in which case the results are
;                            barycentric rather than topocentric.
;
;               TARGET = Target name.  Default is "Sun".
;
;               ITRF93 = If set, then use the high precision Earth PCK files
;                        loaded by LOAD_SUNSPICE_EARTH instead of the default
;                        IAU_EARTH frame.
;
;               ERRMSG = If defined and passed, then any error messages will be
;                        returned to the user in this parameter rather than
;                        depending on the MESSAGE routine in IDL.  If no errors
;                        are encountered, then a null string is returned.  In
;                        order to use this feature, ERRMSG must be defined
;                        first, e.g.
;
;                               ERRMSG = ''
;                               Result= GET_SUNSPICE_RADVEl(..., ERRMSG=ERRMSG)
;                               IF ERRMSG NE '' THEN ...
;
;               Additional keywords to the underlying routines can also be
;               passed.
;
; Calls       :	LOAD_SUNSPICE_GEN, ANYTIM2UTC, PARSE_SUNSPICE_NAME, VALID_NUM,
;               LOAD_SUNSPICE_EARTH
;
; Common      :	None
;
; Restrictions:	This procedure works in conjunction with the Icy/CSPICE
;               package, which is implemented as an IDL Dynamically Loadable
;               Module (DLM).  The Icy source code can be downloaded from
;
;                       ftp://naif.jpl.nasa.gov/pub/naif/toolkit/IDL
;
;               Because this uses dynamic frames, it requires Icy/CSPICE
;               version N0058 or higher.
;
; Side effects: Loads the SunSPICE generic ephemerides, and optionally the most
;               current high resolution Earth ephemerides.
;
; Prev. Hist. :	Partially based on the routine SSC_WRITE_STATION_RA_DEC in the
;               $SSW/stereo/ssc tree.
;
; History     :	Version 1, 19-Sep-2017
;               Version 2, 22-Feb-2018, WTT, pass keywords to PARSE_SUNSPICE_NAME
;
; Contact     :	WTHOMPSON
;-
;
function get_sunspice_radvel, date, lon, lat, alt, spacecraft=spacecraft, $
                              target=target, itrf93=itrf93, errmsg=errmsg, $
                              _extra=_extra
;
on_error, 2
dtor = !dpi / 180.d0
;
;  If the target is not defined, then the target is the Sun.
;
if n_elements(target) eq 0 then target = 'Sun'
;
;  Make sure that the generic SPICE kernels are loaded.
;
message = ''
load_sunspice_gen, errmsg=message, _extra=_extra
if message ne '' then goto, handle_error
;
;  Convert the dates to ephemeris time.
;
sdate = anytim2utc(date, /ccsds, errmsg=message, _extra=_extra)
if message ne '' then goto, handle_error
cspice_str2et, sdate, et
;
;  If a spacecraft name or ID was passed, then simply calculate the position
;  and velocity of the target relative to the spacecraft.
;
if n_elements(spacecraft) eq 1 then begin
    sc = parse_sunspice_name(spacecraft, _extra=_extra)
    if not valid_num(sc) then begin
        message = 'Spacecraft "' + spacecraft + '" not recognized'
        goto, handle_error
    endif
    load_sunspice, sc
    cspice_spkezr, target, et, 'J2000', 'lt+s', sc, state, ltime
;
;  Otherwise, the observatory longitude, latitude, and optionally altitude
;  should be passed.  The default altitude is 0.
;
end else begin
    if n_elements(alt) eq 0 then alt = 0
    if (n_elements(lon) ne 1) or (n_elements(lat) ne 1) or $
      (n_elements(alt) ne 1) then begin
        message = 'Observatory longitude and latitude must be specified'
        goto, handle_error
    endif
;
;  Make sure that the high precision Earth kernels are loaded.
;
    if keyword_set(itrf93) then begin
        load_sunspice_earth, errmsg=message, _extra=_extra
        if message ne '' then goto, handle_error
        frame = 'ITRF93'
    end else frame = 'IAU_EARTH'
;
;  Calculate the observatory position.
;
    cspice_bodvar, 399, 'RADII' , radii
    flat = (radii[0]-radii[2]) / radii[0]
    cspice_georec, lon*dtor, lat*dtor, alt*1d-3, radii[0], flat, origin
    origin = [origin, 0, 0, 0]
    nt = n_elements(et)
    if nt gt 1 then origin = rebin(reform(origin,6,1), 6, nt)
;
;  Get the position of the Sun in geocentric coordinates, and subtract the
;  origin.  Calculate the component of the velocity along the radial vector.
;
    cspice_spkezr, 'Sun', et, frame, 'lt+s', 'Earth', state, ltime

    state = state - origin
endelse
;
;  Calculate the component of the velocity along the radial vector.
;
rad = sqrt(total(state[0:2,*]^2, 1))
return, 1000 * total(state[0:2,*] * state[3:5,*], 1) / rad
;
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message else $
  errmsg = 'spice_ra_dec: ' + message
return, -1
;
end
