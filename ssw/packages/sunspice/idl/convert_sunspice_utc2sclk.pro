;+
; Project     :	Multimission
;
; Name        :	CONVERT_SUNSPICE_UTC2SCLK()
;
; Purpose     :	Converts UTC (or TAI) date/time to spacecraft clock string.
;
; Category    :	SUNSPICE, Time
;
; Explanation : This routine takes standard date/time formats and converts them
;               into spacecraft clock strings.
;
; Syntax      :	Result = CONVERT_SUNSPICE_UTC2SCLK( SPACECRAFT, DATE )
;
; Examples    :	UTC = CONVERT_SUNSPICE_UTC2SCLK('orbiter', '2016-07-12T15:13:46.381')
;
; Inputs      :	SPACECRAFT = The name of the spacecraft.  Must be recognized by
;                            the routine PARSE_SUNSPICE_NAME.  Alternatively,
;                            one can enter in the numeric code of the
;                            spacecraft, e.g. -144 for Solar Orbiter.
;
;               DATE    = UTC (or TAI) date/time in a format recognizable by
;                         the routine ANYTIM2UTC.
;
; Opt. Inputs :	None.
;
; Outputs     : The result of the function is the equivalent spacecraft clock
;               string.  If an error is encountered, then the scalar null
;               string '' will be returned.
;
; Opt. Outputs:	None.
;
; Keywords    :	ERRMSG = If defined and passed, then any error messages will be
;                        returned to the user in this parameter rather than
;                        depending on the MESSAGE routine in IDL.  If no errors
;                        are encountered, then a null string is returned.  In
;                        order to use this feature, ERRMSG must be defined
;                        first, e.g.
;
;                               ERRMSG = ''
;                               utc = CONVERT_SUNSPICE_UTC2SCLK( ERRMSG=ERRMSG, ... )
;                               IF ERRMSG NE '' THEN ...
;
;                       Note also that the result of the function will be the
;                       null string if an error is encountered, regardless of
;                       whether or not ERRMSG is defined.
;
;               Will also accept any LOAD_SUNSPICE, UTC2TAI, or ANYTIM2UTC keywords.
;
; Calls       :	PARSE_SUNSPICE_NAME, VALID_NUM, LOAD_SUNSPICE, ANYTIM2UTC
;
; Common      :	None
;
; Restrictions:	This procedure works in conjunction with the Icy/CSPICE
;               package, which is implemented as an IDL Dynamically Loadable
;               Module (DLM).  The Icy source code can be downloaded from
;
;                       ftp://naif.jpl.nasa.gov/pub/naif/toolkit/IDL
;
;               Because this uses dynamic frames, it requires Icy/CSPICE
;               version N0058 or higher.
;
; Side effects:	Will automatically load the SPICE ephemeris files, if not
;               already loaded.
;
; Prev. Hist. :	Based on Python code supplied by the Solar Orbiter operations
;               team.
;
; History     :	Version 1, 16-Nov-2016, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
function convert_sunspice_utc2sclk, spacecraft, date, $
                                    errmsg=errmsg, _extra=_extra
;
;  Parse the name of the spacecraft.  If not recognized as a spacecraft, i.e. a
;  negative number, then go to the error handling point.
;
message = ''
sc = parse_sunspice_name(spacecraft)
if not valid_num(sc) then begin
    message = 'Unrecognized spacecraft name "' + string(spacecraft) + '"'
    goto, handle_error
endif
if sc ge 0 then begin
    message = '"' + string(spacecraft) + '" is not a spacecraft'
    goto, handle_error
endif
;
;  Make sure that the ephemeris files for this spacecraft are loaded.
;
load_sunspice, sc, errmsg=message
if message ne '' then goto, handle_error
;
;  Make sure the date is in CCSDS format.
;
UTC = ANYTIM2UTC(DATE, /CCSDS, _EXTRA=_EXTRA)
;
;  Create an array to return the results in.
;
sz = size(utc)
if sz[0] eq 0 then result = '' else begin
    dim = sz[1:sz[0]]
    result = make_array(dimension=dim, /string)
endelse
;
;  Catch any errors
;
catch, error_status
if error_status ne 0 then begin
    message = !error_state.msg
    catch, /cancel
    goto, handle_error
endif
;
;  Convert from UTC to ephemeris time.
;
for i=0,n_elements(utc)-1 do begin
    cspice_utc2et, utc[i], et
;
;  Convert from ephemeris time to spacecraft SCLK clock string.
;
    cspice_sce2s, long(sc), et, clkstr
    result[i] = clkstr
endfor
;
return, result
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message, /continue else $
  errmsg = 'CONVERT_SUNSPICE_UTC2SCLK: ' + message
return, ''
;
end
