CACTus readme v.1.2

Written:		Gareth Lawrence (ROB)		 Apr 29 2005
Modified:									May 05 2006

This file explains how to  run the Computer Aided Cme Tracking (CACTus) 
software deveoped originally by David Berghmans (Royal Observatory of 
Belgium)(*) between 2001-2004 (work partly undertaken at ESA-ESTEC 
via ESA funding). The routines have been substantially rewritten in 
conjunction with Eva Robbrecht (ROB) since 2003. The conversion to 
Solarsoft(**) was done by Gareth Lawrence (ROB) in 2005, with 
modifications thereafter.

(*)		http://sidc.oma.be/cactus/
(**)		http://www.lmsal.com/solarsoft/

In its present form CACTus runs on SOHO(+)-LASCO(++) C2 and C3 
white-light broadband coronagraph images. The user is required to have 
a fully operational SolarSoft installation available, as well as an archive of
LASCO images located in the 'usual' place (see SolarSoft setup notes 
for details). Reading from the LASCO archive is done by means of a
dedicated routine and alternatives for other coronagraph datasets are
easily written and will generally be provided when needed. The
archive-reading routine is easily adapted to read from a specified 
directory should an archive not be available to the user.

(+)		http://sohowww.nascom.nasa.gov/
(++)	http://lasco-www.nrl.navy.mil/lasco.html

Once the string 'cactus' has been added to $SSW_INSTR, and the local 
SSW installation upgraded to download the routines, there should be a 
file at the following location:

	$SSW/packages/cactus/setup/setup.cactus_env :

containing the following definitions

setenv CACTUSDIR		$SSW_CACTUS, $SSW/packages/cactus/
setenv CACTUSBIN		$CACTUSDIR/bin/
setenv CACTUSVAR		$CACTUSDIR/var/

setenv CACTUSREAD		$LZ_IMG/level_05/
setenv CACTUSOUT		$HOME/cactusout/, $HOME
setenv CACTUSWRITE		$CACTUSOUT, $HOME
setenv CACTUSLOG		$CACTUSOUT, $CACTUSOUT/log/, $HOME/log/

and the user may create a ~/cactusout/ directory in their home directory,
to which the output will be directed. Of course, the user is free to set these
environmental variables and directories however they choose to do so.

Within the $SSW_CACTUS/idl/ directory are 20 .pro files, and in $SSW_CACTUS/bin/ 
two shared objects (.so files) containing the pre-compiled C files that are called 
externally in two of the IDL routines, and two C files, included for reference. The 
main calling routine is cactus_cmes.pro. Additionally, $SSW_CACTUS/var/ contains
some parameters that are restored during runtime. The calling syntax is as follows:

IDL> cactus_cmes [startdate, enddate] [,LOGFILE = J] [/BOX_PLOTS]

where J =0 is default (screen)
            J = 1 writes to $CACTUSOUT/cactus_log.txt
            J = 2 writes one log file per routine to $CACTUSOUT

The user is prompted for start and end dates by a widget application. CACTus
can be run in batch mode allowing the user to to specify dates in the calling 
sequence and skip the widget. These dates _must_ be in YYMMDD format or 
the software will crash; also, either no dates or two dates must be specified, 
any other number will crash the software. Currently, the checking/correction 
procedure for date input is not rigorous; this may be improved in future versions.
Note also that the logging procedure is to always append the existing logfile
(which is created if it does not yet exist). CACTus never deletes log files; it is
the user's responsibility to delete them as and when they choose to do so.
In particular if LOGFILE=1 is set then the single file $CACTUSOUT/cactus_log.txt 
can grow to be very long after multiple runs.

CACTus output nominally consists of a text file cmecat.txt and a PNG file 
detectionmap.png. These contain, respectively, the parameters of all CMEs
detected including start time, position angle, angular span and velocity (along
with associated errors), and a graphical representation of these CME on a
time-angle plot. Additionally, a directory is created per CME detected and an
intensity-time plot written, with graphical information about the detection 
of each event; as well as this the user can use the /BOX_PLOTS keyword 
which will create a velocity box plot(#) for each CME in its directory.

(#) http://en.wikipedia.org/wiki/Box-and-whisker_diagram

NOTE A: Memory requirements  

  CACTus is extremely memory intensive as a result of multiple transforms and 
  iterations, and the typically large datasets that will be passed to it. A computer 
  with 2 GB of RAM available to IDL can process up to one month of LASCO data.
  1 GB is sufficient for 2 weeks of images. Most users will not want to analyse 
  such a large quantity in one go since it is time intensive to do so, and a 
  catalogue of CACTus results is already available at: 
		
		http://sidc.oma.be/cactus/scan/scan.htm

NOTE B: Data requirements

  CACTus expects to have access to, and to process, both LASCO-C2 and 
  LASCO-C3. Images. Performance on only one of these datasets has not
  been rigorously tested, but at the very least very few CMEs will be detected
  if only C2 data are available. As a very basic rule of thumb, any given CME needs 
  to be visible in at least 13 different images to be detected. The default is to
  look for the chosen dates within the LASCO Level_05 data archive; to change
  this to eg the LASCO Quicklook archive or your private data then please change
  the definition of $CACTUSREAD in $SSW/packages/cactus/setup/setup.cactus_env,
  accordingly. Greater flexibility will be incorporated in future releases.
  
NOTE C: Performance

  Since the final stage of the calculation is to look for patterns in a transformed
  datacube, spurious detections and/or inaccurate parametrisations are to be
  expected at the beginning and end of the data set. Please add a day before 
  and after your period of interest to ensure that the results for that period are
  reliable. Furthermore, CACTus should be given at least three days' worth of
  data to analyse, again to ensure reliable results. Also, during periods of 
  exceptionally low coronal activity CACTus can make spurious detections as a 
  result of noise patterns and exposure time fluctuations, especially close to the 
  LASCO/C3 pylon. This will be improved in future releases.

NOTE D: Data sources

  As stated above, CACTus presently only works with SOHO-LASCO C2 and C3 
  images. In future its capability will be expanded to work with data, such as
  STEREO/SECCHI Cor-1 and Cor-2 from late 2006. While the software team
  will modify the routines to provide the ability to select the data source, it will
  remain the user's responsibility to ensure that these data are located in the
  'normal' location so that CACTus can read them.

NOTE E: Upgrades

  Please bear in mind that CACTus is a work in progress so modifications and
  enhancements to the routines are to be expected. Please be sure to upgrade
  your SSW regularly in order to remain up to date. 
