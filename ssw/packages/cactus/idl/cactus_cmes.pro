 ;======================== header ===============================================
;+
; NAME:
;   grl_cac
; PURPOSE:
;   Wrapper for standalone SSW-friendly CACTus development 
;
; CATEGORY:
;   cactus ver 4.00 (paperA&A-version, May 2004)
; CALLING SEQUENCE:
;   (meant to be used in batchmode)
; INPUTS:
;   
; 
; KEYWORD PARAMETERS:
;   /logfile: to print diagnostics to a logfile 
; 			0 =default=stdout=screen
; 			1 = single file, cactus_log.txt
;			2 = multiple files for each module, eg incube_log.txt etc
; OUTPUTS:
;   flags/velmap
;   flags/catalo
; COMMON BLOCKS: 
;   none
; SIDE EFFECTS: 
;   unknown
; RESTRICTIONS:
;   unknown
; PROCEDURE:
;
; CALLS:
;
; TO DO:
;
; MODIFICATION HISTORY:
;   2005/04/11, ver 1.00, Gareth Lawrence - original SSW-friendly implementation of standalone CACTus software
;   2006/05/05	GRL		Implement widget for date selection; cast logging routines properly
;-
;======================== end header ===========================================
PRO cactus_cmes , logfile = logfile, box_plots = box_plots, startdate, enddate

COMMON CACTUS_ENVS, cactusdir,cactusbin,cactusvar,cactusread,cactusout,cactuswrite,cactuslog
COMMON CACTUS_JULTIME, julstart
COMMON CACTUSINDEX, cactusindex						
COMMON CACTUS_FOV, npixelr, npixela, npixelx, npixely, km_per_cactusmap_pix,  $
					    minradius_c2, maxradius_c2, minradius_c3, maxradius_c3 					   
					    
  cactusdir =	getenv('CACTUSDIR')
  cactusbin =	getenv('CACTUSBIN')
  cactusvar =	getenv('VARDIR')

  cactusread  = getenv('READDIR')
  cactusout =	   getenv('OUTDIR')
  cactuswrite = getenv('WRITEDIR')
  cactuslog =	   getenv('LOGDIR')

;; Below now obsolete since introduction of date widget
;startdate=''
;enddate=''
;read,'Please enter start date in 6-digit YYMMDD format: ', startdate
;read,'Please enter end date in 6-digit YYMMDD format: ', enddate

if not (keyword_set(startdate) and keyword_set(enddate)) then begin
cactus_date, cac_dates
  if (datatype(cac_dates) eq 'UND') then begin
	print,'No dates, aborting CACTUS...'
	return
  endif else begin
	startdate = cac_dates.beg_date
	enddate = cac_dates.end_date
  endelse
endif else begin
	print,'  !!WARNING!!: CACTus will CRASH if the dates have not been specified in YYMMDD format! ... '
endelse

restore,cactusvar+'fov.sav'
restore,cactusvar+'julstart.sav'
restore,cactusvar+'cactusindex.sav'

if keyword_set(logfile) then logfile=logfile else logfile=0
  					    
cactus_incube, logfile=logfile, startdate, enddate, $
			   c2_ar, c2_xy, c2_ind, c3_ar, c3_xy, c3_ind
   
			   
reldev, logfile=logfile, c2_ar, c2_xy, c2_ind, c3_ar, c3_xy, c3_ind, $
			   datacube, index, slices  
			   

velmap, logfile=logfile, slices, index, $ 
			   velocity_settings, vectors, tvanglecube

; if not keyword_set(box_plots) then box_plots = 0

cactus_cat, logfile=logfile, box_plots=box_plots, tvanglecube, velocity_settings, vectors, index, slices

end



