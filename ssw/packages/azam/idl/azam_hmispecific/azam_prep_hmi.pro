pro azam_prep_hmi,hmi_datadir,afiles_dir=afiles_dir

;  routine to generate the ASP-format a_* files needed for
;  input to AZAM.  This routine reads the FITS output from the
;  HMI inversion, then prepares the needed a_files and writes them out

;INPUTS:	hmi_datadir	= path to directory containing HMI FITS inversion output
;		

;KEYWORDS:	afiles_dir	= output directory for a_* files.  If not
;				  set, files output to the present working
;				  directory


;  common area for inversion results read in rdmerlinftis.pro
common com_invertresults, $
    fld, psi, azm, eta0, dmp, dop, bzero, b1mu, cen1, alpha, $
    cct, chisq, occt, fld_er, psi_er, azm_er, $
    cen1_er, alpha_er, xx, yy, slat, slong, $
    yr, mnth, dy, hr, minit, sec, cdelt1, cdelt2,rconv


outdir = './'
if keyword_set(afiles_dir) then outdir = afiles_dir
; !!!check to see if directory exists, if not, create it!!!


;  first read in the HMI inversion results
print, 'in azam_prep_hmi, begin rdhmifits'
rdhmifits,hmi_datadir

;  generate the a_files
print, 'in azam_prep_hmi, begin azam_hmi2afiles'
azam_hmi2afiles,outdir

return
end
