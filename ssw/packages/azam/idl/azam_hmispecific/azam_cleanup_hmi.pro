pro azam_cleanup_hmi, mainDir, tempDir
;  Routine to clean up the temporary directory and place newly
;  processed files in main directory.
;  Modified slightly for HMI data

  ; Check path strings for ending '/'
  if(strmid(mainDir, strlen(mainDir)-1, 1) ne '/') then mainDir = mainDir+'/'
  if(strmid(tempDir, strlen(tempDir)-1, 1) ne '/') then tempDir = tempDir+'/'
  
  ;  Delete files in temporary directory
  file_delete, tempDir+'BACKUP', /noexpand_path, /allow_nonexistent
  file_delete, tempDir+'a__cct', /noexpand_path
  file_delete, tempDir+'AZAM', /noexpand_path
  file_delete, tempDir+'a_1azm', /noexpand_path
  file_delete, tempDir+'a_1incl', /noexpand_path
  file_delete, tempDir+'a_2azm', /noexpand_path
  file_delete, tempDir+'a_2incl', /noexpand_path
  file_delete, tempDir+'a___header', /noexpand_path
  file_delete, tempDir+'a__latitude', /noexpand_path
  file_delete, tempDir+'a__longitude', /noexpand_path
  file_delete, tempDir+'a__mu', /noexpand_path
  ;file_delete, tempDir+'a__pip', /noexpand_path
  file_delete, tempDir+'a__points', /noexpand_path
  file_delete, tempDir+'a__utime', /noexpand_path
  file_delete, tempDir+'a_alpha', /noexpand_path
  file_delete, tempDir+'a_azm', /noexpand_path
  file_delete, tempDir+'a_azm0', /noexpand_path
  file_delete, tempDir+'a_cen1', /noexpand_path
  ;file_delete, tempDir+'a_cen2', /noexpand_path
  file_delete, tempDir+'a_fld', /noexpand_path
  file_delete, tempDir+'a_psi', /noexpand_path
  file_delete, tempDir+'a_solved', /noexpand_path
  file_delete, tempDir+'a__rgtasn', /noexpand_path
  file_delete, tempDir+'a__dclntn', /noexpand_path
  file_delete, tempDir+'a_b1mu', /noexpand_path
  file_delete, tempDir+'a_bzero', /noexpand_path
  ;file_delete, tempDir+'a_delta', /noexpand_path
  file_delete, tempDir+'a_dmp', /noexpand_path
  file_delete, tempDir+'a_dop', /noexpand_path
  file_delete, tempDir+'a_eta0', /noexpand_path

;  FOR NEW AZAM, move the .ps and .eps files to the main directory
  ;  move any postscript files from the temporary directory
  derf = file_search(tempdir, '/*.ps')	;  get PostScript files in tempDir
  derfe = file_search(tempdir, '/*.eps')	;  get PostScript files in tempDir
  tempdir_len = strlen(tempDir)	; length of string tempDir
  ;  first for .ps files
  nfps = n_elements(derf)
  if nfps eq 1 then begin
	  if derf eq '' then nfps = 0
  endif
  if nfps gt 0 then for kk = 0,nfps-1 do begin
  ;  extract name of files
	psfilename = strmid(derf(kk),tempdir_len)
  ;	move file to main directory  
  	file_move, derf(kk),maindir+psfilename, /overwrite, /noexpand_path
  endfor
  ;  now for .eps files
  nfeps = n_elements(derfe)
  if nfeps eq 1 then begin
	  if derfe eq '' then nfeps = 0
  endif
  if nfeps gt 0 then for kk = 0,nfeps-1 do begin
  ;  extract name of files
	psfilename = strmid(derfe(kk),tempdir_len)
  ;	move file to main directory  
  	file_move, derfe(kk),maindir+psfilename, /overwrite, /noexpand_path
  endfor
 
  ;  Delete the temporary directory.
  file_delete, tempDir, /noexpand_path
  
  ;  Move the temporary files to permanent files.
  if(file_test(mainDir+'n_a_azm')) then begin 
    file_move, mainDir+'n_a_azm', mainDir+'a_azm', /overwrite, /noexpand_path
  endif
  if(file_test(mainDir+'n_a_1azm')) then begin
    file_move, mainDir+'n_a_1azm', mainDir+'a_1azm', /overwrite, /noexpand_path
  endif
  if(file_test(mainDir+'n_a_2azm')) then begin
    file_move, mainDir+'n_a_2azm', mainDir+'a_2azm', /overwrite, /noexpand_path
  endif
  if(file_test(mainDir+'n_a_1incl')) then begin 
    file_move, mainDir+'n_a_1incl', mainDir+'a_1incl', /overwrite, /noexpand_path
  endif
  if(file_test(mainDir+'n_a_2incl')) then begin
    file_move, mainDir+'n_a_2incl', mainDir+'a_2incl', /overwrite, /noexpand_path
  endif
  
  ;  Copy the a_azm file to AZAM.
  file_copy, mainDir+'a_azm', mainDir+'AZAM', /overwrite, /noexpand_path
  return
end
