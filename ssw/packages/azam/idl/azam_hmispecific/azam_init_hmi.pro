pro azam_init_hmi, hmi_datadir, data_directory=data_directory, $
	azam_source_directory=azam_source_directory

; Initialize AZAM for Hinode

;  INPUTS:
;	hmi_datadir					- directory containing HMI inversion FITS files

;  KEYWORDS:
;	data_directory				- path to output directory for AZAM results
;								  default = current directory
;	azam_source_directory		- path to AZAM source code to be used here


  ; If directory not passed, use current working directory.
  if keyword_set(data_directory) then begin
	  print,' using current directory to output AZAM results'
	  cd, current = data_directory
  endif else begin
	  data_directory = './'
  endelse

  ; For consistency, append directory path with '/'.
  if(strmid(data_directory, strlen(data_directory)-1, 1) ne '/') then begin
    data_directory = data_directory + '/'
  endif

  ; Prepend source to SSW path.
  path = './'
  if keyword_set(azam_source_directory) then begin
  	ssw_path, azam_source_directory, /full_prepend
    path = azam_source_directory
  endif

  ; Prepend source to IDL path.
  ;path = azam_source_directory 
  path_separator = path_sep(/search_path)
  !path = expand_path('+'+path) + path_separator + !path

  ;  prepare the ASP-like a_* files for AZAM
  ;  check to see if AZAM file exists in data_directory.  If it does,
  ;  then bypass the azam_prep_hinode and continue to the main part of
  ;  azam
  azam_file = read_floats( data_directory+'AZAM', error )
  if error ne 0 then  begin
	azam_prep_hmi, hmi_datadir, afiles_dir = data_directory
  endif

  print,' in azam_init_hmi, completed azam_prep_hmi'

  ;  run the AZAM code adapted for HMI data
  azam_hmi, data_directory, hmi_datadir

end
