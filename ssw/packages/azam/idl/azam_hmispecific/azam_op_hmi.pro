pro azam_op_usage
  ;+
  ;
  ;	procedure:  azam_op
  ;
  ;	purpose:  compute azam structure & display an ASP op
  ;
  ;	routines:  azam_op_usage  azam_op_image  azam_op
  ;
  ;	author:  paul@ncar  6/93
  ;-
; Check number of parameters.
  
  if(1) then begin
    print
    print, "usage:	azam_op_hmi, aa, hh"
    print
    print, "	Compute azam structure & display an ASP op."
    print
    print, "	Arguments"
    print, "		aa	- output azam data set structure."
    print, "			  Some info from the input aa is"
    print, "			  relayed to output aa.  Input aa.dty"
    print, "			  must be path to directory with"
    print, "			  a_* files."
    print, "		hh	- output highlight structure"
    print
    return
  endif
end

;------------------------------------------------------------------------------
;	procedure:  azam_op_image
;
;	purpose:  install byte image in azam parameter structure
;
;	usage:	azam_op_image, aa, iprm
;
;		aa	- input azam structure.
;		iprm	- input parameter number.
;
;		There are aa.nprm parameter structures 'aa.pp(*)'.
;		Parameter structure iprm includes floating point array
;		'aa.pp(iprm).value'.   'tvasp.pro' keyword values to
;		produce a byte image are in structure 'aa.pp(iprm).color'.
;		azam_op_image uses color coding aa.pp(iprm).color to
;		produce a byte image of aa.pp(iprm).value and store
;		the image in 'aa.pp(iprm).img'.
;------------------------------------------------------------------------------
pro azam_op_image, aa, iprm
  case 1 of
  
    ; All raster points on white background.
    aa.pp(iprm).a_np eq aa.npoints  and  aa.pp(iprm).color.back eq aa.white: $
      tvasp, aa.pp(iprm).value, bi=tmp, /notable, /notv $
      , min=aa.pp(iprm).color.min, max=aa.pp(iprm).color.max $
      , gray=aa.pp(iprm).color.gray, invert=aa.pp(iprm).color.invert $
      , wrap=aa.pp(iprm).color.wrap $
      , white=aa.pbkg
      
    ; Solved raster points on white background.
    aa.pp(iprm).a_np eq aa.nsolved  and  aa.pp(iprm).color.back eq aa.white: $
      tvasp, aa.pp(iprm).value, bi=tmp, /notable, /notv $
      , min=aa.pp(iprm).color.min, max=aa.pp(iprm).color.max $
      , gray=aa.pp(iprm).color.gray, invert=aa.pp(iprm).color.invert $
      , wrap=aa.pp(iprm).color.wrap $
      , white=aa.sbkg
      
    ; All raster points on black background.
    aa.pp(iprm).a_np eq aa.npoints  and  aa.pp(iprm).color.back eq aa.black: $
      tvasp, aa.pp(iprm).value, bi=tmp, /notable, /notv $
      , min=aa.pp(iprm).color.min, max=aa.pp(iprm).color.max $
      , gray=aa.pp(iprm).color.gray, invert=aa.pp(iprm).color.invert $
      , wrap=aa.pp(iprm).color.wrap $
      , black=aa.pbkg
      
    ; Solved raster points on black background.
    aa.pp(iprm).a_np eq aa.nsolved  and  aa.pp(iprm).color.back eq aa.black: $
      tvasp, aa.pp(iprm).value, bi=tmp, /notable, /notv $
      , min=aa.pp(iprm).color.min, max=aa.pp(iprm).color.max $
      , gray=aa.pp(iprm).color.gray, invert=aa.pp(iprm).color.invert $
      , wrap=aa.pp(iprm).color.wrap $
      , black=aa.sbkg
      
    ; Use data background.
    else:	tvasp, aa.pp(iprm).value, bi=tmp, /notable, /notv $
      , min=aa.pp(iprm).color.min, max=aa.pp(iprm).color.max $
      , gray=aa.pp(iprm).color.gray, invert=aa.pp(iprm).color.invert $
      , wrap=aa.pp(iprm).color.wrap
  endcase
  
  ; Install byte image in parameter structure.
  aa.pp(iprm).img = tmp
end

;------------------------------------------------------------------------------
;	procedure:  azam_op_hmi
;
;	purpose:  compute azam structure & display an ASP op
;------------------------------------------------------------------------------
pro azam_op_hmi, aa, hh

  ; Check number of parameters.
  if(n_params() eq 0) then begin
    azam_op_usage
    return
  endif
  
  ; Common info about tvasp color table.
  @tvasp.com

  ; FOR NEW AZAM, common area with X-, Y-pixel sizes for image displays
  common psimages,delx,dely 

  ; common area for azam spectra.  Needed here for mscatt flag for 
  ; fill fraction/scattered light formulation for flux determination below
  common com_azamspectra, xllo,xuro,yllo,yuro,full_directory, $
	iwstrt,iwend,dispers,wavlngth,basewav,etaratio,vbroad, $
	maxsz,ntran,mscatt
  
  ; ADDED 8/4/2010 if 2x or 3x magnification will not fit on screen,
  ; disable buttons.
  instrument2x = 'instrument 2x'
  instrument3x = 'instrument 3x'

  ; Get the size of the map.
  cct = azam_a_image(aa.dty+'a__cct', kk, r='i')
  szimg = size(cct)
  
  ; Defined in azam_large.pro
  xDimension = szimg[1]
  yDimension = szimg[2]
  if(azam_fitsOnScreen(xDimension * 2, yDimension * 2) eq 'false') then begin
    instrument2x = '^xinstrument 2x'
    instrument3x = '^xinstrument 3x'
  endif else begin
    if(azam_fitsOnScreen(xDimension * 3, yDimension * 3) eq 'false') then begin
      instrument3x = '^xinstrument 3x'
    endif
  endelse
  ; END ADDED
  
  
  ; Get reference frame and magnification.
  aa.what_now = pop_cult(/string, rows=3, winfree=aa.winfree $
    , title='Click reference frame & magnification' $
    ;+ ' (zenith frame required for PostScript)' $
    , ['^ginstrument 1x', instrument2x, instrument3x $
    , 'zenith 1x', 'zenith 2x', 'zenith 3x' $
    , '^g** DEFAULT **', '** HELP **', '^r** EXIT AZAM **'] $
    , help='azam', arg0='type', arg2= $
    ["   instrument	~ Use instrument reference frame.   Images displayed"$
    , "		  on rectangular grid the way it was taken."$
    , "     zenith	~ Use zenith reference frame.   Images displayed"$
    , "		  for observer normal to solar surface."$
    , "   1x,2x,3x	~ Magnification of displayed images."$
    , " ** DEFAULT **	~ Same as clicking other green button."$
    , "  ** HELP **	~ Print this information."$
    , "** EXIT AZAM **	~ Exit AZAM."])
    
  case aa.what_now of
    '** EXIT AZAM **': return
    'instrument 1x': begin
      stretch = 0
      t = 1
    end
    'instrument 2x': begin
      stretch = 0
      t = 2
    end
    'instrument 3x': begin
      stretch = 0
      t = 3
    end
    'zenith 1x': begin
      stretch = 1
      t = 1
    end
    'zenith 2x': begin
      stretch = 1
      t = 2
    end
    'zenith 3x': begin
      stretch = 1
      t = 3
    end
    else: begin
      stretch = 0
      t = 1
    end
  endcase
  
  ; Retain whats needed from input structure.
  ; Frees memory if aa is being replaced.
  aa = $
    { dty:		aa.dty $	    ;Path to a_* file directory.
    , wina:		aa.wina $	    ;Window number displaying ASCII values.
    , winp:		aa.winp $	    ;Window number of pixmap copied to wina.
    , winb:		aa.winb $	    ;Interaxtive winb window number.
    , blt:		aa.blt $	    ;Interactive winb window button length.
    , bwd:		aa.bwd $	    ;Interactive winb window button width.
    , btm:		aa.btm $	    ;Interactive winb window button text marg.
    , csiz:		aa.csiz $	    ;Character size for IDL's xyouts.
    , winfree:	aa.winfree $	    ;Set to open windows position free.
    }
    
  ; Read continuum data.
  if(stretch) then rframe = 'zenith' else rframe = 'instrument'
  b__cct = azam_a_image(aa.dty+'a__cct', b, ref_frame=rframe)
  
  ; Local dimension & size variables.
  xdim = b.xdim
  ydim = b.ydim
  txdim = t*xdim
  tydim = t*ydim
  
  ; Interactive image sizes.
  ; There are extra right margins for color
  ; bars and computed cursor offset.
  xsz = txdim + 3 * aa.bwd
  ysz = 300 > tydim
  
  ; Open window for azam image 2.
  if(aa.winfree) then begin
    window, /free, xsize=xsz, ysize=ysz, title=aa.dty
  endif else begin
    window, /free, xsize=xsz, ysize=ysz, title=aa.dty, xpos=0, ypos=900 - (6+3*aa.bwd+ysz) - 80
  endelse
  win2 = !d.window
  
  ; Compute and display continuum image.
  tvasp, b__cct, /notv, bi=i__cct, /gray, min=b.cct_min, max=b.cct_max, white=b.pbkg
  tv, puff(i__cct,t)
  
  ; Compute vertical current.
  a_jz, jzl, jzt, aa.dty, a_str=b, dbzl=dbzl, dbzt=dbzt
  
  ; Patch in line to replace Jz by d(Bz)/dx.
  ; jzl=dbzl  &  jzt=dbzt
  ; Read azimuth data.
  b_azm = s_image(aa.dty+'a_azm', b)
  
  ; Read longitude and latitude.
  tmp1 = read_floats(aa.dty+'a__utime')
  tmp = read_floats(aa.dty+'a__longitude')
  tmp0 = read_floats(aa.dty+'a__latitude')
  
  ; 2D images of ut longitude latitude.
  b__ut = fltarr(xdim,ydim)  &  b__ut(b.pxy)  = tmp1(b.vec_pxy)
  b__lon = fltarr(xdim,ydim)  &  b__lon(b.pxy) = tmp(b.vec_pxy)
  b__lat = fltarr(xdim,ydim)  &  b__lat(b.pxy) = tmp0(b.vec_pxy)
  
  ; Great circle approximation of longitude.
  lng_min = min(tmp, max=lng_max)
  tmp = (tmp-.5 * (lng_min+lng_max)) * cos((!pi/180.) * tmp0)
  
  ; Form 2D images of east-west.
  mn_e_w = min(tmp)
  mn_lat = min(tmp0)
  b__e_w = fltarr(xdim,ydim)  &  b__e_w(b.pxy) = tmp( b.vec_pxy)
  
  ; For unstretched images find angle CCW
  ; to local frame x-axis.
  angxloc = 0.
  if(stretch eq 0 and (xdim < ydim) gt 1) then begin
    i0 = xdim/2
    j0 = ydim/2
    
    ; Some datasets like Hinode have noise in latitude.  Smooth with
    ; linear surface fit
    surfit = sfit(b__lat,2)
    ; angxloc = atan( b__lat(i0+1,j0)-b__lat(i0,j0) $
    angxloc = atan( surfit(i0+1,j0)-surfit(i0,j0) $
      , b__e_w(i0+1,j0)-b__e_w(i0,j0) )*(180./!pi)
    print
    print, 'Angle CCW to stretched display is about', angxloc
  endif
  
  ; Logical 2D array for solved data points.
  pdat = bytarr(xdim,ydim)
  sdat = pdat
  pdat(b.pxy) = 1B
  sdat(b.sxy) = 1B
  
  ; Structure to control color for parameters.
  ; This one for continuum to be replicated.
  color_str = $
    { min: b.cct_min $	;tvasp.pro min keyword.
    , max: b.cct_max $	;tvasp.pro max keyword.
    , gray:	1L $		    ;tvasp.pro gray keyword.
    , wrap:	0L $		    ;tvasp.pro wrap keyword.
    , invert:	0L $		  ;tvasp.pro invert keyword.
    , back:	long( tvasp.ix_white ) $ ;Background color index, -1 to use data.
    }
    
  ; Structure for a parameter.
  ; This one for continuum to be replicated.
  pp = $
    { value: b__cct $	    ;Floating point data array.
    , img: byte(i__cct) $	;Byte image.
    , a_np:	b.npoints $	  ;Number of points in a_* file.
    , color: color_str $	;Color structure.
    , color0:	color_str $	;Original color structure.
    , name:	'continuum' $	;Name of parameter.
    , bar: 'bar' $		    ;Type of color scale to draw.
    }
    
  ; Structure for an image window.
  ; To be replicated 3 times.
  ww = $
    { win: win2 $	;IDL window number.
    , img: bytarr(txdim,tydim,/nozero)$ ;Byte array of image with magnification.
    , iprm:	(cct=16L) $	;Number of parameter being displayed.
    , drag:	cct $	 ;Number of parameter painted on with mouse.
    }
    
  ; Structure for magnified image.
  mgfy = $
    { win: -1L $	;IDL window number.
    , siz: (mgfy_siz=38L) $   ;Dimension of sub array.
    , mag: (mgfy_mag=14L) $   ;Screen resolution of one pixel.
    , xc:	xdim/2 $	;x location of last cursor location.
    , yc:	ydim/2 $	;y location of last cursor location.
    , x0: 0L > (xdim/2-mgfy_siz/2) $  ;Sub array lower left x location.
    , y0: 0L > (ydim/2-mgfy_siz/2) $  ;Sub array lower left y location.
    , iprm:	cct $	;Number of parameter being displayed.
    , bttn:	'no magnify' $	;Interactive if 'magnify' or 'short arrows'
    }

  ; Create azam structure.
  aa = $
    { index: 2L $	;Index for standard azam structure.
    , stretch: stretch $	  ;1 for zenith frame, 0 for instrument frame.
    , angxloc:	angxloc $	  ;
    , npoints:	b.npoints $	;Number of points in an a__* file.
    , nsolved:	b.nsolved $	;Number of points in an a_* file.
    , xdim:	xdim $		    ;x dimension of parameter arrays.
    , ydim:	ydim $		    ;y dimension of parameter arrays.
    , pxy:	b.pxy $		    ;Maps vector to 2D arrays for a__* files.
    , sxy:	b.sxy $		    ;Maps vector to 2D arrays for a_* files.
    , pbkg:	b.pbkg $	    ;Background map for a__* 2D arrays.
    , sbkg:	b.sbkg $	    ;Background map for a_* 2D arrays.
    , head:	b.head $	    ;Copy of a___header file.
    , vec_pxy: b.vec_pxy $	;Maps vector to stretched vector for a__*.
    , vec_sxy: b.vec_sxy $	;Maps vector to stretched vector for a_*.
    , mm_per_deg:	b.mm_per_deg $ ;Mega meters per degree on solar surface.
    , wina:	aa.wina $	    ;Window number displaying ASCII values.
    , winb:	aa.winb $	    ;Interaxtive winb window number.
    , winp:	aa.winp $	    ;Window number of pixmap copied to wina.
    , mgfy:	mgfy $		    ;Structure for magnified image.
    , bx:	-1L $		    ;Cursor x on interactive winb.
    , by:	-1L $		    ;Cursor y on interactive winb.
    , bs:	0L $		    ;Mouse button state on interactive winb.
    , black: tvasp.ix_black $    ;tvasp.pro color index for black.
    , gray:	tvasp.ix_gray $	    ;tvasp.pro color index for gray.
    , white:tvasp.ix_white $    ;tvasp.pro color index for white.
    , yellow:	tvasp.ix_yellow $   ;tvasp.pro color index for yellow.
    , red: tvasp.ix_red $	    ;tvasp.pro color index for red.
    , blue:	tvasp.ix_blue $	    ;tvasp.pro color index for blue.
    , green: tvasp.ix_green $    ;tvasp.pro color index for green.
    , cen_lat: -999. $		    ;Lat for azimuth center disambiguation.
    , cen_e_w:	-999. $		    ;Long for azimuth center disambiguation.
    , pix_deg:	b.pix_deg $	    ;Pixels per degree on stretched images.
    , zoom:	0L $		    ;Set to 1 if zoom structure.
    , x0:	0L $		    ;Set to lower left x in zoom.
    , y0:	0L $		    ;Set to lower left y in zoom.
    , xrpnt: replicate(-1,xdim,ydim) $  ;original x raster point numbers.
    , yrpnt: replicate(-1,xdim,ydim) $  ;original y raster point numbers.
    , nprm:		(nprm=21L) $	    ;Number of parameter structures.
    , azm_amb: 0L $		    ;Parameter number ambiguous azimuth.
    , azz_amb:	1L $		    ;Parameter number ambiguous zenith azimuth.
    , zen_amb:	2L $		    ;Parameter number ambiguous zenith angle.
    , jz_amb:	3L $		    ;Parameter number ambiguous current.
    , flx_amb:	4L $		    ;Parameter number ambiguous flux.
    , azz_orig:	5L $		    ;Parameter number original zenith azimuth.
    , azz_ref:	6L $		    ;Parameter number reference zenith azimuth.
    , zen_orig:	7L $		    ;Parameter number original zenith angle.
    , zen_ref:	8L $		    ;Parameter number reference zenith angle.
    , azm: 9L $		    ;Parameter number instrument azimuth.
    , azz: 10L $		    ;Parameter number zenith azimuth.
    , zen_ang: 11L $		    ;Parameter number zenith angle.
    , jz:	12L $		    ;Parameter number of current.
    , flx: 13L $		    ;Parameter number flux.
    , psi: 14L $		    ;Parameter number instrument field incline.
    , fld: 15L $		    ;Parameter number field strength.
    , cct: cct $		    ;Parameter number continuum.
    , alpha: 17L $		    ;ASP: Parameter number 1-fill, Hinode is fill
    , cen1:	18L $		    ;Parameter number doppler velocity.
    , extra0: 19L $		    ;Parameter number extra parameter 0.
    , extra1: 20L $		    ;Parameter number extra parameter 1.
    , ww:	replicate(ww,3) $   ;Array of structures for displayed images.
    , pp:	replicate(pp,nprm)$ ;Array of parameter structures.
    , b__ut:	b__ut $		    ;2D array latitude.
    , b__lon:	b__lon $	    ;2D array latitude.
    , b__lat:	b__lat $	    ;2D array latitude.
    , b__e_w:	b__e_w $	    ;2D array east to west great circle.
    , pdat:	pdat $		    ;2D array if data present for a__* files.
    , sdat:	sdat $		    ;2D array if data present for a__* files.
    , rfrnc:	sdat $		    ;Reference array of azimuth resolution.
    , rslv:	sdat $		    ;Ongoing array of azimuth resolution.
    , defroi: replicate(255B,xdim,ydim)$;Define region of interest array.
    , mn_lat:	mn_lat $	    ;Latitude of lower left corner.
    , mn_e_w:	mn_e_w $	    ;Longitude of lower left corner.
    , blt: aa.blt $	    ;Interactive winb window button length.
    , bwd: aa.bwd $	    ;Interactive winb window button width.
    , btm: aa.btm $	    ;Interactive winb window button text marg.
    , csiz:	aa.csiz $	    ;Character size for IDL's xyout.
    , winfree:	aa.winfree $	    ;Set to open windows position free.
    , t: t $		    ;Resolution of raster point on screen.
    , t0:	t $		    ;Original raster point resolution.
    , pwr: 4L $		    ;2 to pwr is mouse box interaction size.
    , lock: 'lock drag' $	    ;Otherwise 'unlock drag' mouse drag effect.
    , cri: 'reference' $	    ;Drag effect of left mouse button.
    , anti: 'anti reference' $  ;Drag effect of center mouse button.
    , prime: 'wads' $	    ;Drag effect of right mouse button.
    , rehi: 1L $		    ;True if azam_image.pro to redo highlights.
    , hilite: 'ambigs' $	    ;Type of highlight to draw.
    , spectra: '' $		    ;Path to calibrated data file.
    , dty: aa.dty $	    ;Path to directory with a_* files.
    , ps_dir:	aa.dty $	    ;Path to directory for PostScript output.
    , rframe:	rframe $	    ;'instrument' or 'zenith' reference frame.
    , fname:	'' $		    ;Name of last a_* file read.
    , fdim:	0L $		    ;Vector dimension of last a_* file read.
    , what_now:	'' $		    ;Instruction for called or calling program.
;  FOR NEW AZAM, ADD X-, Y-PIXEL SCALES
	, delx: delx $	; X-pixel scale in arcseconds
	, dely: dely $	; Y-pixel scale in arcseconds
    }
    
  ; Velocity of light (km/sec).
  ; Initial guess for line center (pixel).
  ; Dispersion (mA/pixel).
  ; Line wavelength (A).
  ; Velocity per pixel ((km/sec)/pixel).
  cee = 299792.458
  cen0 = b.head(36)
  disper = b.head(39)
  wavlth = b.head(40)
  km_sec_pixel = disper*cee/(1000.*wavlth)
  
  ; Compute Doppler shift.
  b_cen1 = s_image(aa.dty + 'a_cen1', b)
  tmp = b_cen1(aa.sxy)
  cen_avg = total(tmp) / n_elements(tmp)
  b_cen1(aa.sxy) = -(b_cen1(aa.sxy)-cen_avg) * km_sec_pixel
  print
  print, ' initial center guess ', cen0
  print, 'average fitted center ', cen_avg
  
  ; Structure for Doppler.
  aa.pp(aa.cen1).value = b_cen1
  aa.pp(aa.cen1).a_np = b.nsolved
  aa.pp(aa.cen1).color.min = -2.
  aa.pp(aa.cen1).color.max = 2.
  aa.pp(aa.cen1).color.gray = 0L
  aa.pp(aa.cen1).color.wrap = 0L
  aa.pp(aa.cen1).color.invert = 0L
  aa.pp(aa.cen1).color.back = tvasp.ix_white
  aa.pp(aa.cen1).name = 'Doppler'
  azam_op_image, aa, aa.cen1
  
  ; Structure for line of sight azimuth.
  aa.pp(aa.azm).value = s_image(aa.dty+'a_azm', b)
  aa.pp(aa.azm).a_np = b.nsolved
  aa.pp(aa.azm).color.min = -180.
  aa.pp(aa.azm).color.max = 180.
  aa.pp(aa.azm).color.gray = 0L
  aa.pp(aa.azm).color.wrap = 1L
  aa.pp(aa.azm).color.invert = 0L
  aa.pp(aa.azm).color.back = tvasp.ix_white
  aa.pp(aa.azm).name = 'sight azimuth'
  aa.pp(aa.azm).bar = 'azimuth'
  azam_op_image, aa, aa.azm
  
  ; Structure for ambig line of sight azimuth.
  aa.pp(aa.azm_amb).value = (((aa.pp(aa.azm).value+360.) mod 360.)-180.)*sdat
  aa.pp(aa.azm_amb).a_np = b.nsolved
  aa.pp(aa.azm_amb).color =  aa.pp(aa.azm).color
  aa.pp(aa.azm_amb).name = 'ambig los az'
  aa.pp(aa.azm_amb).bar = 'azimuth'
  azam_op_image, aa, aa.azm_amb
  
  ; Structure for local frame azimuth.
  aa.pp(aa.azz).value = s_image(aa.dty+'a_1azm', b)
  aa.pp(aa.azz).a_np = b.nsolved
  aa.pp(aa.azz).color =  aa.pp(aa.azm).color
  aa.pp(aa.azz).name = 'local azimuth'
  aa.pp(aa.azz).bar = 'azimuth'
  azam_op_image, aa, aa.azz
  
  ; Structure for ambig local frame azimuth.
  aa.pp(aa.azz_amb).value = s_image(aa.dty+'a_2azm', b)
  aa.pp(aa.azz_amb).a_np = b.nsolved
  aa.pp(aa.azz_amb).color =  aa.pp(aa.azm).color
  aa.pp(aa.azz_amb).name = 'ambig azimuth'
  aa.pp(aa.azz_amb).bar = 'azimuth'
  azam_op_image, aa, aa.azz_amb
  
  ; Structure for psi.
  aa.pp(aa.psi).value = s_image(aa.dty+'a_psi', b, bkg=90.)
  aa.pp(aa.psi).a_np = b.nsolved
  aa.pp(aa.psi).color.min = 0.
  aa.pp(aa.psi).color.max = 180.
  aa.pp(aa.psi).color.gray = 0L
  aa.pp(aa.psi).color.wrap = 0L
  aa.pp(aa.psi).color.invert = 0L
  aa.pp(aa.psi).color.back = tvasp.ix_white
  aa.pp(aa.psi).name = 'sight incline'
  aa.pp(aa.psi).bar = 'zenith'
  azam_op_image, aa, aa.psi
  
  ; Structure for zenith angle.
  aa.pp(aa.zen_ang).value = s_image(aa.dty+'a_1incl', b, bkg=90.)
  aa.pp(aa.zen_ang).a_np = b.nsolved
  aa.pp(aa.zen_ang).color = aa.pp(aa.psi).color
  aa.pp(aa.zen_ang).name = 'local incline'
  aa.pp(aa.zen_ang).bar = 'zenith'
  azam_op_image, aa, aa.zen_ang
  
  ; Structure for ambig zenith angle.
  aa.pp(aa.zen_amb).value = s_image(aa.dty+'a_2incl', b, bkg=90.)
  aa.pp(aa.zen_amb).a_np = b.nsolved
  aa.pp(aa.zen_amb).color = aa.pp(aa.psi).color
  aa.pp(aa.zen_amb).name = 'ambig incline'
  aa.pp(aa.zen_amb).bar = 'zenith'
  azam_op_image, aa, aa.zen_amb
  
  ; Structure for scattered light.
  aa.pp(aa.alpha).value = s_image(aa.dty+'a_alpha', b, bkg=1.)
  aa.pp(aa.alpha).a_np = b.nsolved
  aa.pp(aa.alpha).color.min = 0.
  aa.pp(aa.alpha).color.max = 1.
  aa.pp(aa.alpha).color.gray = 1L
  aa.pp(aa.alpha).color.wrap = 0L
  aa.pp(aa.alpha).color.invert = 1L
  aa.pp(aa.alpha).color.back = tvasp.ix_black
  ;  FOR NEW AZAM FOR HMI, alpha is fill factor
  ;  mscatt hardwired to 0 for HMI inversions
  mscatt = 0
  if mscatt ne 0 then begin
  	aa.pp(aa.alpha).name = '1-fill'
  endif else begin
  	aa.pp(aa.alpha).name = 'fill'
  endelse
  azam_op_image, aa, aa.alpha
  ; Structure for field strength.
  
  aa.pp(aa.fld).value = s_image(aa.dty+'a_fld', b)
  aa.pp(aa.fld).a_np = b.nsolved
  aa.pp(aa.fld).color.min = 0.
  aa.pp(aa.fld).color.max = 2500.
  aa.pp(aa.fld).color.gray = 1L
  aa.pp(aa.fld).color.wrap = 0L
  aa.pp(aa.fld).color.invert = 1L
  aa.pp(aa.fld).color.back = tvasp.ix_white
  aa.pp(aa.fld).name = 'field'
  azam_op_image, aa, aa.fld
  
  ; Structure for flux.
  ;  FOR NEW AZAM HINODE, the fill factor is alpha-parameter
  if mscatt ne 0 then begin
  	aa.pp(aa.flx).value = (1.-aa.pp(aa.alpha).value)*aa.pp(aa.fld).value $
    *cos(aa.pp(aa.zen_ang).value*(!pi/180.))
  endif else begin
  	aa.pp(aa.flx).value = aa.pp(aa.alpha).value*aa.pp(aa.fld).value $
    *cos(aa.pp(aa.zen_ang).value*(!pi/180.))
  endelse
  aa.pp(aa.flx).a_np = b.nsolved
  aa.pp(aa.flx).color.min = -2000.
  aa.pp(aa.flx).color.max = 2000.
  aa.pp(aa.flx).color.gray = 1L
  aa.pp(aa.flx).color.wrap = 0L
  aa.pp(aa.flx).color.invert = 0L
  aa.pp(aa.flx).color.back = -1L
  aa.pp(aa.flx).name = 'flux'
  azam_op_image, aa, aa.flx
  
  ; Structure for ambig flux.
  ;  FOR NEW AZAM HINODE, the fill factor is alpha-parameter
  if mscatt ne 0 then begin
  	aa.pp(aa.flx_amb).value = (1.-aa.pp(aa.alpha).value)*aa.pp(aa.fld).value $
    *cos(aa.pp(aa.zen_amb).value*(!pi/180.))
  endif else begin
  	aa.pp(aa.flx_amb).value = aa.pp(aa.alpha).value*aa.pp(aa.fld).value $
    *cos(aa.pp(aa.zen_amb).value*(!pi/180.))
  endelse
  aa.pp(aa.flx_amb).a_np = b.nsolved
  aa.pp(aa.flx_amb).color = aa.pp(aa.flx).color
  aa.pp(aa.flx_amb).name = 'ambig flux'
  azam_op_image, aa, aa.flx_amb
  
  ; Structure for current.
  aa.pp(aa.jz).value = jzl+jzt
  aa.pp(aa.jz).a_np = b.nsolved
  aa.pp(aa.jz).color.min = -600.
  aa.pp(aa.jz).color.max =  600.
  aa.pp(aa.jz).color.gray = 1L
  aa.pp(aa.jz).color.wrap = 0L
  aa.pp(aa.jz).color.invert = 0L
  aa.pp(aa.jz).color.back = -1L
  aa.pp(aa.jz).name = 'Jz'
  azam_op_image, aa, aa.jz
  
  ; Structure for ambig current.
  aa.pp(aa.jz_amb) = aa.pp(aa.jz)
  aa.pp(aa.jz_amb).value = jzl-jzt
  aa.pp(aa.jz_amb).name = 'ambig Jz'
  azam_op_image, aa, aa.jz_amb
  
  ; Structure for extra parameter 0.
  ; Initially set to a__pip.
  ;  FOR HMI, SET TO 0.15*(FIELD STRENGTH)/2800.
  ;  to approximate the PIP variable
  ;aa.pp(aa.extra0).value = s_image(aa.dty+'a__pip', b)
  aa.pp(aa.extra0).value = (0.15/2800.)*s_image(aa.dty+'a_fld', b)
  aa.pp(aa.extra0).a_np = b.npoints
  aa.pp(aa.extra0).color.min = 0.
  ;  FOR NEW AZAM,  pip is fraction, not percent, incr. to 15 pct
  ;  was 12 percent for ASP
  ;aa.pp(aa.extra0).color.max = 12.
  aa.pp(aa.extra0).color.max = 0.15
  aa.pp(aa.extra0).color.gray = 0L
  aa.pp(aa.extra0).color.wrap = 0L
  aa.pp(aa.extra0).color.invert = 1L
  aa.pp(aa.extra0).color.back = tvasp.ix_white
  aa.pp(aa.extra0).name = 'a__pip'
  azam_op_image, aa, aa.extra0
  
  ; Structure for extra parameter 1.
  ; Initially set to a__mu.
  aa.pp(aa.extra1).value = s_image(aa.dty+'a__mu', b)
  aa.pp(aa.extra1).a_np = b.npoints
  aa.pp(aa.extra1).color.min = min(aa.pp(aa.extra1).value(aa.pxy), max=mx)
  aa.pp(aa.extra1).color.max = mx
  aa.pp(aa.extra1).color.gray = 1L
  aa.pp(aa.extra1).color.wrap = 0L
  aa.pp(aa.extra1).color.invert = 0L
  aa.pp(aa.extra1).color.back = tvasp.ix_white
  aa.pp(aa.extra1).name = 'a__mu'
  azam_op_image, aa, aa.extra1
  
  ; Structures for original & reference az-el.
  aa.pp(aa.azz_orig)=aa.pp(aa.azz) & aa.pp(aa.azz_orig).name= 'orig azimuth'
  aa.pp(aa.azz_ref)=aa.pp(aa.azz) & aa.pp(aa.azz_ref).name='refer azimuth'
  aa.pp(aa.zen_orig)=aa.pp(aa.zen_ang) & aa.pp(aa.zen_orig).name= 'orig incline'
  aa.pp(aa.zen_ref)=aa.pp(aa.zen_ang) & aa.pp(aa.zen_ref).name='refer incline'
  
  ; Save original color keys.
  aa.pp(*).color0 = aa.pp(*).color
  
  ; Set arrays for raster point numbers.
  aa.xrpnt(aa.pxy) = b.xpnt(b.vec_pxy)
  aa.yrpnt(aa.pxy) = b.ypnt(b.vec_pxy)
  
  ; Open magnified window.
  if(aa.winfree) then begin
    window, /free, xsize=mgfy.siz*mgfy.mag, ysize=mgfy.siz*mgfy.mag, title=aa.dty
  endif else begin
    window, /free, xsize=mgfy.siz*mgfy.mag, ysize=mgfy.siz*mgfy.mag, $
        xpos=1144-mgfy.siz*mgfy.mag, ypos=0, title=aa.dty
  endelse
  aa.mgfy.win = !d.window
  
  ; Open Interactive windows.
  if aa.winfree then begin
    window, /free, xsize=xsz, ysize=ysz, title=aa.dty
  end else begin
    window, /free, xsize=xsz, ysize=ysz, title=aa.dty $
      , xpos=(txdim+3*aa.bwd+8)<(1144-(txdim+3*aa.bwd)) $
      , ypos=900-(6+3*aa.bwd+ysz)
  endelse
  aa.ww(1).win = !d.window
  aa.ww(1).iprm = aa.zen_ang
  aa.ww(1).drag = aa.zen_ang
  
  if(aa.winfree) then begin
    window, /free, xsize=xsz, ysize=ysz, title=aa.dty
  endif else begin
    window, /free, xsize=xsz, ysize=ysz, title=aa.dty, xpos=0, ypos=900-(6+3*aa.bwd+ysz)
  endelse
  aa.ww(0).win  = !d.window
  aa.ww(0).iprm = aa.azz
  aa.ww(0).drag = aa.azz
  
  ; Set null highlight arrays.
  hh = { hi:-1, lo:-1, iprm:-1, name:'', level:0., toggle:1L, $
          outlineroi: -1L, arrow:{ hi:-1, lo:-1 }, laluz:-1L }
    
  ; Display azimuth, incline, continuum
  ; and magnified images.
  for inum = 0, 2 do begin 
    azam_display, inum, aa, hh
  endfor
  azam_mgfy, aa
end
