function s_image, a_file, b, bkg=bkg
;+
;
;	function:  s_image
;
;	purpose:  return a 2D array of an a_* file dumped by program "bite"
;		  expansions of *.bi files with the -x option.
;		  Structure b of data and directory must already exist
;		  and can be for unstretched or stretched images or
;		  non zoom azam structure.
;
;	author:  paul@ncar, 11/93	(minor mod's by rob@ncar)
;
;==============================================================================
;
;	Check number of parameters.
;
if n_params() eq 0 then begin
	print
	print, "usage:	image = s_image( file, b )"
	print
	print, "	Return a 2D array of vector file dumped by program"
	print, "	'bite'expansions of *.bi files with the -x option."
	print, "	Structure b of data and directory must already exist"
	print, "	and can be for unstretched or stretched images or"
	print, "	non zoom azam structure.  The structure has the"
	print, "	directory path to a a_* data set.  Input file must"
	print, "	be the same length as an imageable a_* file."
	print
	print, "	Arguments"
	print, "		file	- string variable with file path"
	print, "		b	- input structure of data and"
	print, "			  directory."
	print, "	Keywords"
	print, "		bkg	- input background value (def=0.)"
	print
	print, "   ex:"
	print, "	a_path = '/hilo/d/asp/data/red/92.06.19/op9/a_1incl'"
	print, "	b_1incl = s_image( a_path, b, bkg=90. )"
	print
	return, 0
endif
;-
				    ;Read data file.
a_data = read_floats( a_file )
				    ;Isolate directory path by finding
				    ;last '/'.
j = -1
i =  0
while  i ne -1  do begin
	i = strpos( a_file, '/', i )
	if  i ne -1  then  begin
		j = i
		i = i+1
	end
end
dty = strmid( a_file, 0, j+1 )
				    ;Get file name.
a_name = strcompress( strmid( a_file, strlen(dty), 1000 ), /remove_all )

				    ;Put local frame azimuth in range
				    ;-180. to 180.
if  a_name eq 'a_1azm'  or  a_name eq 'a_2azm'  then begin
	whr = where( a_data gt 180. or a_data lt -180., nwhr )
	if nwhr gt 0 then begin
		print
		print, "s_image.pro: normalizing file "+a_file
		print, "in range -180. to 180."
		a_data(whr) = $
		(((a_data(whr) mod 360.)+(360.+180.)) mod 360.)-180.
		i = write_floats( a_file, a_data )
		print
	end
end
				    ;Allocate 2D image array and install
				    ;background.
if  n_elements(bkg) eq 0  then  bg = 0.  else  bg = bkg
image = replicate( float(bg), b.xdim, b.ydim )

				    ;Check if file is correct length.
vdim = n_elements( a_data )
if vdim ne b.npoints and vdim ne b.nsolved then $
stop, a_file+': is wrong length'
				    ;Set file name and length in structure.
b.fname = a_name
b.fdim  = vdim
				    ;Check for stretched images.
if b.stretch then begin
				    ;Check if unsolved raster points are in
				    ;image.  Expand data vector to right
				    ;number of points and move data into
				    ;2D array.
	if  vdim eq b.npoints $
	then  image( b.pxy ) = a_data( b.vec_pxy ) $
	else  image( b.sxy ) = a_data( b.vec_sxy )

end else begin
				    ;Move data into 2D array.
	if  vdim eq b.npoints $
	then  image( b.pxy ) = a_data $
	else  image( b.sxy ) = a_data
end
				    ;Return 2D image as function value.
return, image

end
