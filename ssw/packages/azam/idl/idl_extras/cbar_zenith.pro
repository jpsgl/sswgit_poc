function cbar_zenith, dummy $
, bw=bw_, D0=D0, black_bkg=black_bkg, notable=notable $
, gray=gray, wrap=wrap, invert=invert_, min=min_, max=max_
;+
;
;	function:  cbar_zenith
;
;	purpose:  Return tvasp zenith color bar image.
;
;	author:  paul@ncar, 4/96
;
;==============================================================================
;
;       Check number of parameters.
;
if n_params() ne 0 then begin
	print
	print, "usage:	azbar = cbar_zenith()"
	print
	print, "	Return tvasp zenith color bar image."
	print
	print, "keywords:"
	print, "        bw      - set to get black & white lithograph."
	print, "	min	- min    keyword for tvasp.pro (def: 0    )"
	print, "	max	- max    keyword for tvasp.pro (def: 180  )"
	print, "	invert	- invert keyword for tvasp.pro (def: bw   )"
	print, "	wrap	- wrap   keyword for tvasp.pro (def: unset)"
	print, "	gray	- gray   keyword for tvasp.pro (def: unset)"
	print, "	D0	- y dimension of returned image"
	print, "		  x dimension is half this size."
	print, "		  (def: 100)."
	print
	print, "	notable - set to prevent tvasp from loading"
	print, "	          color table (def: load color table)."
	print
	print, "	black_bkg - set to make background black"
	print, "		    (def: make background white)."
	print
	print, "ex:"
	print, "	;  Display zenith color bar at 0,0 in window."
	print, "	tv, cbar_zenith(), 0,0"
	print
	print, "	;  Compute 100x200 zenith color bar image."
	print, "	cbar_image = cbar_zenith(D0=200)"
	print
	print, "	;  Compute 100x200 zenith color bar image,"
	print, "	;  use black background and prevent loading"
	print, "	;  tvasp color table."
	print, "	cbar_image = cbar_zenith(D0=200,/black,/notable)"
	return, 0
endif
;-
                                    ;Set black & white lithograph.
bw = keyword_set(bw_)
				    ;Set wrap or default.
if n_elements(invert_) eq 0 then  invert=bw  else  invert=invert_

				    ;Set min max or default.
if n_elements(min_) eq 0 then  min=0    else  min=min_
if n_elements(max_) eq 0 then  max=180  else  max=max_

				    ;Set image dimensions.
if n_elements(D0) ne 0 then  ydim=round(D0)  else  ydim=100L
xdim = ydim/2
				    ;Rasters arrays of x & y.
tmp   = lindgen(xdim,ydim)
yrast = tmp/xdim
xrast = tmp-yrast*xdim
				    ;Where colors are in the image.
radius = xdim-1.
whr = where( xrast^2+(yrast-radius)^2 le radius^2 )

				    ;Array with zenuth angle values.
tmp = replicate(1000.,xdim,ydim)
tmp(whr) = atan( xrast(whr), yrast(whr)-radius )*(180./!pi)

				    ;Background where array.
if keyword_set( black_bkg ) $
then  bbkg = where( tmp eq 1000. ) $
else  wbkg = where( tmp eq 1000. )
				    ;Compute byte image.
tvasp, tmp, bi=bi, min=min, max=max $
, bw=bw, white=wbkg, black=bbkg, /notv, notable=notable $
, gray=gray, wrap=wrap, invert=invert

				   ;Return byte image.
return, bi

end
