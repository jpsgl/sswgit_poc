pro undefine_sbsp, a00, a01, a02, a03, a04, a05, a06, a07, a08, a09 $
            , a10, a11, a12, a13, a14, a15, a16, a17, a18, a19
;+
;
;	procedure:  undefine_sbsp
;
;	purpose:  undefine all arguments (20 max)
;
;	author:  paul@ncar, 5/95
;
;==============================================================================
;
;	Check number of parameters.
;
if n_params() eq 0 then begin
	print
	print, "usage:  undefine_sbsp, a00 [,a01 [,a02 [,a03 [,a04 [$"
	print, "        , a05 [,a06 [,a07 [,a08 [,a09 [$"
	print, "        , a10 [,a11 [,a12 [,a13 [,a14 [$"
	print, "        , a15 [,a16 [,a17 [,a18 [,a19 ]]]]]]]]]]]]]]]]]]]"
	print
	print, "	Undefine all arguments (max 20)"
	print
	return
endif
;-
				    ;Undefine all arguments.
a00 = 0  &  a = temporary(a00)
a01 = 0  &  a = temporary(a01)
a02 = 0  &  a = temporary(a02)
a03 = 0  &  a = temporary(a03)
a04 = 0  &  a = temporary(a04)
a05 = 0  &  a = temporary(a05)
a06 = 0  &  a = temporary(a06)
a07 = 0  &  a = temporary(a07)
a08 = 0  &  a = temporary(a08)
a09 = 0  &  a = temporary(a09)
a10 = 0  &  a = temporary(a10)
a11 = 0  &  a = temporary(a11)
a12 = 0  &  a = temporary(a12)
a13 = 0  &  a = temporary(a13)
a14 = 0  &  a = temporary(a14)
a15 = 0  &  a = temporary(a15)
a16 = 0  &  a = temporary(a16)
a17 = 0  &  a = temporary(a17)
a18 = 0  &  a = temporary(a18)
a19 = 0  &  a = temporary(a19)

end
