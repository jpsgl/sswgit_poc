pro ps_where_lines, xdim, ydim, whereLines $
, origin_xy, inches_xy, LLxy=LLxy, URxy=URxy, color=color, tplate=tpl_
;+
;
;	procedure:  ps_where_lines
;
;	purpose:  on PostScript device with image already output by
;		  ps_image.pro draw vectors connecting adjacent points
;		  as indicated by a where array
;
;	authors: paul@ncar 5/95
;
;==============================================================================
;
;	Check number of parameters
;
if n_params() eq 0 then begin
	print
	print, "usage:  ps_where_lines, xdim, ydim, whereLines $"
	print, "	[, origin_xy [, inches_xy ]]"
	print
	print, "	On PostScript device with image already output by"
	print, "	ps_image.pro draw vectors connecting adjacent points"
	print, "	as indicated by a where array."
	print
	print, "Arguments:"
	print, "		xdim"
	print, "		  ydim	- input 2D array dimensions on which"
	print, "		  		whereLines is valid; a sample"
	print, "				array is not input"
	print, "	     whereLines	- input where array adjacent points"
	print, "			  are to be connected by vectors"
	print, "	     origin_xy	- input two vector:"
	print, "				[x_origin,y_origin] position"
	print, "				in inches of 2D area lower"
	print, "				left corner (def [0.,0.])"
	print, "	     inches_xy	- input two vector:"
	print, "				[x_inches,y_inches] size in"
	print, "				inches of 2D area (def based"
	print, "				on other conditions (likely"
	print, "				unsatisfactory))"
	print, "Keywords:"
	print, "		LLxy"
	print, "		  URxy	- input two vectors:"
	print, "				A local 2D array is created"
	print, "				with points set corresponding"
	print, "				to whereLines.  The plot 2D"
	print, "				area is then for sub array"
	print, "				with indices LLxy=[X0,Y0] &"
	print, "				URxy=[X1,Y1] (def LLxy=[0,0]"
	print, "				URxy=[xdim-1,ydim-1] )
	print, "		color	- color index of drawn vectors (def 0)"
	print, "		tplate	- template structure"
	print, "Examples:"
	print, "	(see ps_image.pro)"
	print
	return
endif
;-
				    ;Check for null conditions.
if n_elements(whereLines) eq 0 then return
if whereLines(0) eq -1         then return
if xdim lt 2                   then return
if ydim lt 2                   then return

				    ;Get template structure if not present.
if n_elements(tpl_) eq 0 $
then  ps_tplate, tpl, portrait=!d.y_size gt !d.x_size $
else  tpl = tpl_
				    ;Get origin or default to [0,0].
if n_elements(origin_xy) eq 2 then  org=origin_xy  else  org=[0,0]

				    ;Get plot area in inches or defalut
if n_elements(inches_xy) eq 2 then begin
	inches = inches_xy
end else begin

	inches = [ tpl.xdevice, tpl.ydevice ]-org

	if inches(0)/(xdim-1) gt inches(1)/(ydim-1) $
	then  inches(0) = (xdim-1)*inches(1)/(ydim-1) $
	else  inches(1) = (ydim-1)*inches(0)/(xdim-1)
end
				    ;Get corner indices or default.
if n_elements(LLxy) eq 2 then  LL=LLxy  else  LL=[0,0]
if n_elements(URxy) eq 2 then  UR=URxy  else  UR=[xdim-1,ydim-1]

				    ;Set some local scaler varibles.
xorg    = org(0)
yorg    = org(1)
xinches = inches(0)
yinches = inches(1)
xx0     = LL(0)
xx1     = UR(0)
yy0     = LL(1)
yy1     = UR(1)
				    ;Logical array points to connect.
high = lonarr(xdim,ydim)
high(whereLines) = 1
				    ;Inches per pixel.
iper = xinches/(xx1-xx0)
				    ;Loop over two spacial dimensions.
for  ix = xx0,xx1-1  do begin
for  iy = yy0,yy1-1  do begin

	if high(ix,iy) then begin
				    ;Horizontal vector.
		if high(ix+1,iy) then begin
			ps_plots, color=color, tplate=tpl $
			, xorg+([ix,ix+1]-xx0)*iper $
			, yorg+( iy      -yy0)*iper
		end
				    ;Vertical vector.
		if high(ix,iy+1) then begin
			ps_plots, color=color, tplate=tpl $
			, xorg+( ix      -xx0)*iper $
			, yorg+([iy,iy+1]-yy0)*iper
		end
				    ;Diagonal vector.
		if high(ix+1,iy+1)   then begin
		if not high(ix,iy+1) then begin
		if not high(ix+1,iy) then begin
			ps_plots, color=color, tplate=tpl $
			, xorg+([ix,ix+1]-xx0)*iper $
			, yorg+([iy,iy+1]-yy0)*iper
		end
		end
		end

	end else begin
				    ;Anti diagonal vector.
		if high(ix,iy+1)       then begin
		if high(ix+1,iy)       then begin
		if not high(ix+1,iy+1) then begin
			ps_plots, color=color, tplate=tpl $
			, xorg+([ix,ix+1]-xx0)*iper $
			, yorg+([iy+1,iy]-yy0)*iper
		end
		end
		end
	end
end
end

end
