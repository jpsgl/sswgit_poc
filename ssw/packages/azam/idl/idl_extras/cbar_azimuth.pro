function cbar_azimuth, dummy $
, bw=bw_, D0=D0, black_bkg=black_bkg, notable=notable $
, gray=gray, wrap=wrap_, invert=invert, min=min_, max=max_
;+
;
;	function:  cbar_azimuth
;
;	purpose:  Return tvasp azimuth color bar image.
;
;	author:  paul@ncar, 4/96
;
;==============================================================================
;
;       Check number of parameters.
;
if n_params() ne 0 then begin
	print
	print, "usage:	azbar = cbar_azimuth()"
	print
	print, "	Return tvasp azimuth color bar image."
	print
	print, "keywords:"
	print, "        bw      - set to get black & white lithograph."
	print, "        min     - min    keyword for tvasp.pro (def: -180 )"
	print, "        max     - max    keyword for tvasp.pro (def: 180  )"
	print, "	wrap	- wrap   keyword for tvasp.pro (def: set  )"
	print, "	gray	- gray   keyword for tvasp.pro (def: unset)"
	print, "	invert	- invert keyword for tvasp.pro (def: unset)"
	print, "	D0	- x,y dimension of returned image"
	print, "		  (def: 100)."
	print
	print, "	notable - set to prevent tvasp from loading"
	print, "	          color table (def: load color table)."
	print
	print, "	black_bkg - set to make background black"
	print, "		    (def: make background white)."
	print
	print, "ex:"
	print, "	;  Display azimuth color bar at 0,0 in window."
	print, "	tv, cbar_azimuth(), 0,0"
	print
	print, "	;  Compute 200x200 azimuth color bar image."
	print, "	cbar_image = cbar_azimuth(D0=200)"
	print
	print, "	;  Compute 200x200 azimuth color bar image,"
	print, "	;  use black background and prevent loading"
	print, "	;  tvasp color table."
	print, "	cbar_image = cbar_azimuth(D0=200,/black,/notable)"
	return, 0
endif
;-
				    ;Set black & white lithograph.
bw = keyword_set(bw_)
				    ;Set wrap or default.
if n_elements(wrap_) eq 0 then  wrap=1  else  wrap=wrap_

				    ;Set min max or default.
if n_elements(min_) eq 0 then  min=-180  else  min=min_
if n_elements(max_) eq 0 then  max=180   else  max=max_

				    ;Set image dimension.
if n_elements(D0) ne 0 then  dsz=round(D0)  else  dsz=100L

				    ;Rasters arrays of x & y.
tmp   = lindgen(dsz,dsz)
yrast = tmp/dsz
xrast = tmp-yrast*dsz
				    ;Where colors are in the image.
radius = .5*(dsz-1)
whr = where( (xrast-radius)^2+(yrast-radius)^2 le radius^2 )

				    ;Array with azimuth values.
tmp = replicate(1000.,dsz,dsz)
tmp(whr) = atan( yrast(whr)-radius, xrast(whr)-radius )*(180./!pi)

				    ;Background where array.
if keyword_set( black_bkg ) $
then  bbkg = where( tmp eq 1000. ) $
else  wbkg = where( tmp eq 1000. )
				    ;Compute byte image.
tvasp, bw=bw, tmp, bi=bi, min=min, max=max $
, white=wbkg, black=bbkg, /notv, notable=notable $
, gray=gray, wrap=wrap, invert=invert

				    ;Return byte image.
return, bi

end
