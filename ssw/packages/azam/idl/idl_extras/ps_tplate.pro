pro ps_tplate, tplate, portrait=portrait_, poster=poster_
;+
;
;	procedure:  ps_tplate
;
;	purpose:  return template structure optionally used by
;		  various ps_*.pro by keyword entry
;
;	author:  paul@ucar.edu 1/98
;
;==============================================================================
;
;       Check number of parameters.
;
if n_params() eq 0 then begin
	print
	print, "usage:	ps_tplate, tplate"
	print
	print, "	Return template structure optionally used by"
	print, "	various ps_*.pro by keyword entry."
	print
	print, "	Arguments"
	print, "		tplate	- output template structure"
	print
	print, "	Keywords"
	print, "	      portrait	- set for portrait orientation"
	print, "			  (def 0 (landscape))"
	print, "		poster	- set for poster size 36x48"
	print, "			  (def 0 (8.5x11))"
	return
endif
;-
				    ;Variables for set keywords.
portrait = long(keyword_set(portrait_))
poster   = long(keyword_set(poster_  ))

				    ;Minimum margin on 8.5x11 printer, inches.
mrg0 = 9./16.
				    ;8.5x11 device size.
if portrait then begin
	xdevice0 = 8.5
	ydevice0 = 11.
end else begin
	xdevice0 = 11.
	ydevice0 = 8.5
end
				    ;Check for poster.
if poster then begin
				    ;Minimum margin for poster.
	mrg = 1.
				    ;Poster size, inches.
	if portrait then begin
		xdevice = 36.
		ydevice = 48.
	end else begin
		xdevice = 48.
		ydevice = 36.
	end
end else begin
				    ;Standard sizes for 8.5x11.
	mrg = mrg0
	xdevice = xdevice0
	ydevice = ydevice0
end
				    ;Useful area on 8.5x11.
xuse0 = xdevice0-2.*mrg0
yuse0 = ydevice0-2.*mrg0
				    ;Useful area.
xuse = xdevice-2.*mrg
yuse = ydevice-2.*mrg
				    ;Magnification if any.
mfy = (xuse/xuse0) < (yuse/yuse0)
				    ;Useful area in porportion to 8.5x11.
xuse = xuse0*mfy
yuse = yuse0*mfy
				    ;xy margins.
xmrg = .5*(xdevice-xuse)
ymrg = .5*(ydevice-yuse)
				    ;Set template structure.
tplate = $
{ portrait:	portrait $
, poster:	poster $
, mfy:		mfy $
, xdevice:	xdevice $
, ydevice:	ydevice $
, xmrg:		xmrg $
, ymrg:		ymrg $
, lft:		xmrg+mfy*5./16. $
, rgt:		xmrg+mfy*3./4. $
, btm:		ymrg+mfy*5./16. $
, top:		ymrg+mfy*5./8. $
, xgap:		mfy*1./2. $
, ygap:		mfy*7./16. $
}

end
