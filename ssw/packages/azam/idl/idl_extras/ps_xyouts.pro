pro ps_xyouts, x, y, s $
, color=color, charsize=chars, align=align, orient=orien, tplate=tpl
;+
;
;	procedure:  ps_xyouts
;
;	purpose:  for PostScript device do IDL's xyouts procedure with
;		  inch scale converted to device coordinates
;
;	author:  paul@ncar, 4/95	(minor mod's by rob@ncar)
;
;==============================================================================
;
;       Check number of parameters.
;
if n_params() eq 0 then begin
	print
	print, "usage:	ps_xyouts, X, Y, String"
	print
	print, "	For PostScript device do IDL's xyouts procedure with"
	print, "	inch scale converted to device coordinates"
	print
	print, "	Arguments"
	print, "		X,Y	- X,Y coordinates in inches for"
	print, "			  printing String (either can be"
	print, "			  arrays)"
	print, "		String	- String or string array to print"
	print, "	Keywords"
	print, "		color	- color table index (def 0)"
	print, "	      charsize	- character size (def 1.)"
	print, "		align	- string alignment,"
	print, "			  0.0 < align < 1.0 (def 0.0)"
	print, "	       orient	- string orientation,"
	print, "			  0.0 < orient < 360. (def 0.0)"
	print, "                tplate  - template structure"
	print
	return
endif
;-
				   ;Set keywords or default.
if n_elements(color) ne 0 then  clr=color  else  clr=0
if n_elements(chars) ne 0 then  chs=chars  else  chs=1.
if n_elements(align) ne 0 then  aln=align  else  aln=0.0
if n_elements(orien) ne 0 then  ort=orien  else  ort=0.0

                                  ;Get page size from template if available.
if n_elements(tpl) gt 0 then begin
        xdev = tpl.xdevice
        ydev = tpl.ydevice
end else begin
                                   ;Assume 8.5 x 11.0 and get page size.
        if !d.x_size gt !d.y_size then begin
                xdev = 11.0
                ydev =  8.5
        end else begin
                xdev =  8.5
                ydev = 11.0
        end
end
				    ;Do xyouts procedure with inches scaled
				    ;to device coordinates.
xyouts, /device, x*(!d.x_size/xdev), y*(!d.y_size/ydev), s $
, color=clr, charsize=chs, align=aln, orient=ort

end
