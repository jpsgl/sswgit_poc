pro ps_plots, x, y, tplate=tpl $
, color=color, thick=thick, linestyle=linestyle, psym=psym, symsize=symsize
;+
;
;	procedure:  ps_plots
;
;	purpose:  do IDL plots procedure with inches scaled to device
;		  coordinates
;
;	author:  paul@ncar, 4/95	(minor mod's by rob@ncar)
;
;==============================================================================
;
;       Check number of parameters.
;
if n_params() eq 0 then begin
	print
	print, "usage:	ps_plots, X, Y"
	print
	print, "	Do IDL plots procedure with inches scaled to device"
	print, "	coordinates."
	print
	print, "	Arguments"
	print, "		X	- Vector of X coordinates to be"
	print, "			  connected"
	print, "		Y	- Vector of Y coordinates to be"
	print, "			  connected"
	print, "	Keywords"
	print, "		color	- color table index (def 0)"
	print, "		thick	- line thickness (def !p.thick)"
	print, "	      linestyle - linestyle graphics keyword"
	print, "		psym    - psym graphics keyword"
	print, "		symsize - symsize graphics keyword"
	print, "		tplate	- template structure"
	print
	return
endif
;-
				   ;Set keywords or default.
if n_elements(color)     ne 0 then  clr=color      else  clr=0
if n_elements(thick)     ne 0 then  thk=thick      else  thk=!p.thick
if n_elements(linestyle) ne 0 then  lss=linestyle  else  lss=0
if n_elements(psym)      ne 0 then  psm=psym       else  psm=0
if n_elements(symsize)   ne 0 then  ssz=symsize    else  ssz=0

				   ;Get page size from template if available.
if n_elements(tpl) gt 0 then begin
	xdev = tpl.xdevice
	ydev = tpl.ydevice
end else begin
				   ;Assume 8.5 x 11.0 and get page size.
	if !d.x_size gt !d.y_size then begin
		xdev = 11.0
		ydev =  8.5
	end else begin
		xdev =  8.5
		ydev = 11.0
	end
end
				    ;Do plots procedure with inches scaled
				    ;to device coordinates.
plots, /device, x*(!d.x_size/xdev), y*(!d.y_size)/ydev $
, color=clr, thick=thk, linestyle=lss, psym=psm, symsize=ssz

end
