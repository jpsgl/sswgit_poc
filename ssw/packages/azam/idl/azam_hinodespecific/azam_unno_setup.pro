pro azam_unno_setup,aa,scatlp

;  Sets up the data for computing profiles on the fly
;  this routine now does all the preliminary stuff before the mouse
;  starts moving


;  This routine hardwired for standard Hinode Level2 data reduction

;  purpose:  Compute Unno Stokes profiles from MERLIN output
;  parameters.  This version inputs arrays of parameters
;  and performs operations for arrays of dimension [npix = nx*nsl] 
;  Unno spectra calculated in same way as ASP inversion code, but
;  modified for either scattered light/fill fraction formulation

;	INPUT:
;		aa		- AZAM input structure
;		scatlp	- observed hinode scattered light profile

;	OUTPUTS:
;		Outputs are to common com_mefitparams
;
;------------------------------------------------------------------------------

;  some constants specific to standard Hinode data output
ccc = 299792.458D	;  light speed in km/sec

;  common area for usage in azam_spectra.pro
common com_azamspectra, xllo,xuro,yllo,yuro,full_directory, $
	iwstrt,iwend,dispers,wavlngth,basewav,etaratio,vbroad, $
	maxsz,ntran,mscatt

;  common area for storing the inversion results and other parameters
;  that will be used for the display of ME profiles
common com_mefitparams, nlinr,nwave,ctrwav,fld,eta0,dmp,dop,bzero, $
    b1mu,cen1,cen2,alpha,cct,delta,ffscat,cos2azm,sin2azm,csn, $
    ffinst,mask1,mask2,isolv

nwl1 = maxsz-1

;  calculate the pixel ranges for wavelength cutouts
nline = n_elements(iwstrt)
;  nlinr = ranges of pixels for output array for each spectrum line
nlinr = intarr(nline,2)
;  nwave = total number of wavelengths to output
nwave = 0
for kk = 0,nline-1 do begin
	nwlin = (iwend(kk)-iwstrt(kk) + 1)
	nlinr(kk,0) = nwave
	nlinr(kk,1) = nlinr(kk,0) + nwlin - 1
	nwave = nwave + nwlin
endfor

;  compute wavelengths of line centers in mA from 0th pixel
ctrwav = float(iwstrt)*dispers + 1000.*(wavlngth - basewav)


;  read in the data from the temporary ASP-like files
fld = azam_a_image(full_directory+'a_fld',bb,r='i')
psi = azam_a_image(full_directory+'a_psi',bb,r='i')
azm = azam_a_image(full_directory+'a_azm',bb,r='i')
eta0 = azam_a_image(full_directory+'a_eta0',bb,r='i')
dmp = azam_a_image(full_directory+'a_dmp',bb,r='i')
dop = azam_a_image(full_directory+'a_dop',bb,r='i')
bzero = azam_a_image(full_directory+'a_bzero',bb,r='i')
b1mu = azam_a_image(full_directory+'a_b1mu',bb,r='i')
;  note: like MERLIN inversions, line center positions are assumed
;  to be in km/sec with redshift positive
cen1 = azam_a_image(full_directory+'a_cen1',bb,r='i')
cen2 = azam_a_image(full_directory+'a_cen2',bb,r='i')
alpha = azam_a_image(full_directory+'a_alpha',bb,r='i')
cct = azam_a_image(full_directory+'a__cct',bb,r='i')
delta = azam_a_image(full_directory+'a_delta',bb,r='i')
solv = long(azam_a_image(full_directory+'a_solved',bb,r='i'))

;  get dimensions of input map
sz = size(fld)
nx = sz(1) & ny = sz(2)

;  the solv array is non-zero if a point is solved but starts at
;  zero for the [0,0] position.  Check to see if that point is
;  solved by examining inversions
isolv = bytarr(nx,ny)
isolv(*,*) = 1
whr = where(solv eq 0L)
isolv(whr) = 0
if (fld(0,0) ne 0. and dop(0,0) ne 0.) then isolv(0,0) = 1


;  define temporary 1-D arrays
	dmpp = fltarr(ny) & dopp = fltarr(ny) & fldd = fltarr(ny)
	ctr = dblarr(ny) & etaz = fltarr(ny) & csnn = fltarr(ny)

;  temporary array for full fitted profile
ifit = lonarr(maxsz)

;  compute fourier transform of scattered light profile for
;  fourier shifting.  First step is to extend to ntran
scatsh = fltarr(ntran)
scatsh(0:nwl1) = scatlp
scatsh = extend_sbsp(scatsh,0,nwl1)
;  now fourier transform
ffscat = fft(scatsh,-1)

;  convert azimuth to radians, get sines, cosines
azz = azm*!dtor
cos2azm = cos(2.*azz)
sin2azm = sin(2.*azz)

;  get cosine of inclination to LOS
csn = cos(psi*!dtor)

;  construct an analytic fourier transform of the Gaussian
gausstrans,wavlngth,ntran,vbroad,dispers,ffinst

;  generate the mask functions for extension of data to ntran pixels
maskgen,ntran,iwstrt,iwend,maxsz,mask1,mask2

return
end
