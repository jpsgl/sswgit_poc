function azam_merlin_fits_read, directory, file

tmp = mrdfits(file, 0, header)
index = fitshead2struct(header)
output = add_tag(output, index, 'HEADER')
for i = 1, 41 do begin
	tmp_data = mrdfits(file, i, tmp_header)
	name = fxpar(tmp_header, 'EXTNAME')
	name = strcompress(name, /rem)
	output = add_tag(output, tmp_data, name)
endfor

;  save MERLIN FITS header in the  AZAM directory as an
;  IDL save-file
save,filename=directory+'MERLIN_FITSheader.save',header

return, output
end
