 pro azam_spectra, aa, hh
;------------------------------------------------------------------------------
;
;	procedure:  azam_spectra
;
;	purpose:  display data spectra.
;
;   lites:modified 16 Aug 2010 for Hinode data input
;	Original code for 2-component fit remains, but is commented
;
;------------------------------------------------------------------------------
; INPUTS:
;     xllo,xuro,yllo,yuro = Input corner pixels of reduced map
;     aa                  = I/O azam data set structure
;     hh                  = I/O highlight structure


;  common area for usage in azam_spectra.pro
	common com_azamspectra, xllo,xuro,yllo,yuro,full_directory, $
		iwstrt,iwend,dispers,wavlngth,basewav,etaratio,vbroad, $
		maxsz,ntran,mscatt

;  calculate the pixel ranges for wavelength cutouts
nline = n_elements(iwstrt)
;  nlinr = ranges of pixels for output array for each spectrum line
nlinr = intarr(nline,2)
;  nwave = total number of wavelengths to output
nwave = 0
for kk = 0,nline-1 do begin
	nwlin = (iwend(kk)-iwstrt(kk) + 1)
	nlinr(kk,0) = nwave
	nlinr(kk,1) = nlinr(kk,0) + nwlin - 1
	nwave = nwave + nwlin
endfor

		      
				    ;Save system variables.
sav_p = !p
				    ;Ask if user wants new file.
if aa.spectra ne '' then $
if	pop_cult( /string, winfree=aa.winfree $
	, title='Use Hinode spectra file already entered ?' $
	, ['^gyes','no','^g** DEFAULT **','** HELP **'] $
	, help='azam', arg0='window', arg1=aa, arg2= $
       ["If the path to the Hinode spectra file is the same as for the last"$
       ,"'menu'-->'spectra';  'yes' saves retyping the path."$
       ,""$
       ,"     yes       - use same file path as last 'menu'-->'spectra'"$
       ,"     no        - user enters Hinode specta file path in a widget that"$
       ,"                 that pops up"$
       ,"** DEFAULT **  - same as clicking other green button"$
       ," ** HELP **    - print this information"$
       ] ) $
eq 'no' then  aa.spectra=''
				    ;Get file path.
if aa.spectra eq '' then begin
	path = azam_text_in( aa, 'Enter path Hinode map fits directory)')
	if path eq '' or path eq 'q' then return
; For consistency, append directory path with '/'.
	if(strmid(path, strlen(path)-1, 1) ne '/') then path = path + '/'
	print,'level1 data path: ',path
end else begin
	path = aa.spectra 
end


  ; Get observed spectra for image area being displayed
  print,'Collecting observed Stokes profiles....'
  derf = file_search(path, '*SP3D*.fits')
  nfile = sizeof_sbsp(derf)
  print,'number of files = ',nfile
  ; Get image dimensions
  readl1_sbsp,derf(0),obstks,hdr
  sz = size(obstks)
  numx = sz(1) & nsl = sz(2) & nstks = sz(3)
  nstk1 = nstks-1
  ; Dimensions of reduced array
  nmstep = xuro-xllo+1
  numy = yuro-yllo+1
  ; Create observed data array
  print, 'creating observed spectra data base...'
  obsdat = lonarr(numx,nstks,nmstep,numy)
  for kk = xllo,xuro do begin
	  readl1_sbsp,derf(kk),obstks,hdr
	  for jj = yllo,yuro do begin
		  for istk = 0,nstk1 do begin
		  	obsdat(*,istk,kk-xllo,jj-yllo) = long(obstks(*,jj,istk))
		  endfor
  	  endfor
  endfor
  ;  Get the scattered light profile from this directory
  flnm_stksimg = file_search(path, '*_stksimg.save')
  restore,flnm_stksimg
  ;  Set first scan number to 0, using full reduced sizearray
  snum0 = 0

;  for computing profiles on the fly, setup the input parameters in
;  common com_mefitparams
	azam_unno_setup,aa,scatlp

; get number of wavelength ranges
  nwrng = n_elements(iwstrt)
  
aa.spectra = path
				    ;Set plot charsize.
charsize = .5*aa.csiz

				    ;x axis ramp array.
vecx  = indgen(numx)

				    ;Get instrument frame structure.
void, azam_b_image( aa.dty+'a__cct', b_str=b )

				    ;Read a_bzero file.
	bzero = azam_a_image( aa.dty+'a_bzero',aa)

				    ;Form instrument map of xy locations.
tmp = replicate(-1L,b.npoints)
tmp(aa.vec_pxy) = (lindgen(aa.xdim,aa.ydim))(aa.pxy)
off_xy = replicate(-1L,b.xdim,b.ydim)
off_xy(b.pxy) = tmp
off_x0 = b.x0
off_y0 = b.y0
off_x1 = b.x0+b.xdim-1
off_y1 = b.y0+b.ydim-1
tmp=0  &  b=0
				    ;Old cursor postions.
xsav = -2
ysav = -1
				    ;Initially no instructions.
azam_message, aa, color=aa.red
in_red = 1
				    ;Initialize automatic scaling.
fix_i=0.  &  max_i=0.
fix_q=0.  &  max_q=0.
fix_v=0.  &  max_v=0.
				    ;Initially do not do crossing spectra.
alongy = 0
				    ;Open window spectra image.
if aa.winfree then begin
	window, /free, xsize=4*numx, ysize=numy+20, title='scan step'
end else begin
	window, /free, xsize=4*numx, ysize=numy+20, title='scan step' $
	, xpos=1130-2*(numx > nmstep), ypos=890-20-2*numy-50
end
wg = !d.window
				    ;Open windows for profile plots.
sz = 300

;if twoComp then begin
;	if aa.winfree then begin
;		window, /free, xsize=2*sz, ysize=2*sz, title='two component'
;	end else begin
;		window, /free, xsize=2*sz, ysize=2*sz, title='two component' $
;		, xpos=1130-3*sz, ypos=40+40
;	end
;	wt = !d.window
;end

if aa.winfree then begin
	window, /free, xsize=2*sz, ysize=2*sz, title='(x,y) profiles'
end else begin
	window, /free, xsize=2*sz, ysize=2*sz, title='(x,y) profiles' $
	, xpos=1130-2*sz, ypos=40
end
wu = !d.window

window, /free, xsize=2*sz, ysize=2*sz, /pixmap
wp = !d.window

have_un = 0L
				    ;Infinite loop.
infinity:
				    ;Presume coodinates are off windows.
	xs = -1
	ys = -1
				    ;Check for initialization pass.
	if xsav eq -2 then begin

		if aa.xrpnt(aa.xdim/2,aa.ydim/2) ge 0 then begin
			xs = aa.xrpnt(aa.xdim/2,aa.ydim/2)
			ys = aa.yrpnt(aa.xdim/2,aa.ydim/2)
		end
		err_ = 1
		xstep = -1
		ystep = -1

	end else begin
				    ;Print instructions.
		if in_red then begin
			azam_message, aa $
			, 'left(scan_step) middle(rescale) right(menu)'
			in_red = 0
		end
				    ;Get cursor status on scan step image.
		wset,wg  &  cursor,xg,yg,/device,/nowait  &  errg=!err
		xj = -1
		if alongy then begin
			wset,wj  &  cursor,xj,yj,/device,/nowait  &  errj=!err
		end
				    ;Cursor on profile image.
		if xg ge 0 or xj ge 0 then begin

			if xg ge 0 then begin
				err_ = errg
				xs = xstep  &  if yg gt 2*numy then  xs=-1
				ys = yg mod numy
			end else begin
				err_ = errj
				xs = (xj mod nmstep)+snum0
				ys = ystep  &  if yj gt 2*numx then  xs=-1
			end
				    ;Get (x,y) coordinates for computed cursor.
			xcyc = 0
			if  xs ge off_x0 and xs le off_x1 $
			and ys ge off_y0 and ys le off_y1 then begin
				tmp = off_xy( xs-off_x0, ys-off_y0 )
				ycc = tmp/aa.xdim
				xcc = tmp-ycc*aa.xdim
				if tmp ge 0 then   xcyc=[xcc,ycc]
			end
					    ;Do computed cursor.
			azam_cursor, aa, xerase, yerase $
			,aa.ww(0).win,aa.ww(1).win,aa.ww(2).win, undef,undef $
			,                                        undef,undef $
			,     eraser0,     eraser1,     eraser2, undef,undef $
			, xc, yc, state $
			, likely=likely $
			, no_ttack=no_ttack, xcyc=xcyc

		end else begin
				    ;Do computed field cursors.
			azam_cursor, aa, xerase, yerase $
			,aa.ww(0).win,aa.ww(1).win,aa.ww(2).win, undef,undef $
			,                                        undef,undef $
			,     eraser0,     eraser1,     eraser2, undef,undef $
			, xc, yc, state $
			, likely=likely $
			, no_ttack=no_ttack

				    ;Button state.
			err_ = state > aa.bs

				    ;Cursor on main windows.
			if xc ge 0 then begin
			if aa.xrpnt(xc,yc) ge 0 then begin
				xs = aa.xrpnt(xc,yc)
				ys = aa.yrpnt(xc,yc)
			end
			end
		end
	end
				    ;Process mouse button state.
	case err_ of
				    ;Plot scan_step if left button clicked.
				    ;(redo all spectra plots)
	1:		xsav = -1
				    ;Rescale if middle button clicked.
	2:	begin
			fix_i = max_i
			fix_q = max_q
			fix_v = max_v
			xsav = -1
		end
				    ;Put up menu on right button click.
	4:	begin



					;  Select the observed and fitted profiles for this point
					;  Observed profiles
			i_obs = obsdat(*,0,xs,ys)
			q_obs = obsdat(*,1,xs,ys)
			u_obs = obsdat(*,2,xs,ys)
			v_obs = obsdat(*,3,xs,ys)
					;  adjust for pixel offsets of sub=area
			unno_sgl,xllo+xs,yllo+ys,scatlp,mefit_sgl,ifit_sgl,nounnofit
			have_un = 0
			if nounnofit eq 0 then begin
				i_mag = mefit_sgl(*,0)
				i_fit = ifit_sgl(*)
				q_fit = mefit_sgl(*,1)
				u_fit = mefit_sgl(*,2)
				v_fit = mefit_sgl(*,3)
				have_un = 1L
			endif



				    ;Clear instructions.
			azam_message, aa, color=aa.red
			in_red = 1
				    ;Pop up menu.
			btn = pop_cult( /string, winfree=aa.winfree $
			, title='Click on one.' $
			, ['auto scaling','cross spectra' $
			, (['^x',''])(have_un)+'postScript' $
			, (['^x',''])(have_un)+'encapsulated' $
			, '^g** DISMISS **','^yEXIT SPECTRA','** HELP **',''] $
			, help='azam', arg0='window', arg1=aa, arg2= $
		       [" auto scaling  - plot spectra with auto scaling"$
		       ," cross spectra - enable Y pixel images (takes time)"$
		       ,"  postScript   - Output postScript file for point"$
		       ,"encapsulated   - Output encapsulated postScript file"$
		       ,"** DISMISS **  - continue with spectra"$
		       ," EXIT SPECTRA  - return to regular azam"$
		       ," ** HELP **    - print this information"$
		       ] )
			case btn of
			'auto scaling': begin
				fix_i = 0.
				fix_q = 0.
				fix_v = 0.
				end
			'cross spectra': begin
				if alongy eq 0 then begin
					    ;Open window for crossing spectra image.
					if aa.winfree then begin
						window, /free $
						,xsize=2*nmstep $
						,ysize=2*numx+20 $
						,title='along slit'
					end else begin
						window, /free $
						,xsize=2*nmstep $
						,ysize=2*numx+20 $
						,title='along slit' $
						,xpos=1130-2*(numx>nmstep)-20 $
						,ypos=890-20-2*numx
					end
					wj = !d.window
				end
				alongy = 1
				xsav = -1
				err_ = 1
				end

			'postScript': begin
				noImag = pop_cult( [ '^gyes', 'no' ] $
				, title='plot i-magnetic profile ?' $
				, winfree=aa.winfree )
				if azam_out_dir( aa, o_dir ) then $
				ps_profiles,i_obs,q_obs,u_obs,v_obs,have_un,i_mag,i_fit, $
						q_fit,u_fit,v_fit,have_un,xs,ys,noImag=noImag
				end

			'encapsulated': begin
				noImag = pop_cult( [ '^gyes', 'no' ] $
				, title='plot i-magnetic profile ?' $
				, winfree=aa.winfree )
				if azam_out_dir( aa, o_dir ) then $
				ps_profiles,i_obs,q_obs,u_obs,v_obs,have_un,i_mag,i_fit, $
						q_fit,u_fit,v_fit,have_un,xs,ys,/encapsulated, $
						noImag=noImag
				end

			'EXIT SPECTRA':	goto, break0
			else:
			end
		end
	else:
	end
				    ;Check if spectra position unchanged.
	if xs eq xsav and ys eq ysav then begin
		wait, .005
		goto, infinity
	end
				    ;Erase windows if off data.
	if xs lt 0 then begin
		if xsav ge 0 then begin
			wset, wu
			erase, aa.white
;			if twoComp then begin
;				wset, wt
;				erase, aa.white
;			end
		end
		xsav = -1
		have_un = 0
		wait, .005
		goto, infinity
	end
				    ;Save spectra position.
	xsav = xs
	ysav = ys

;  Observed profiles
	i_obs = obsdat(*,0,xs,ys)
	q_obs = obsdat(*,1,xs,ys)
	u_obs = obsdat(*,2,xs,ys)
	v_obs = obsdat(*,3,xs,ys)

;  compute ME fits on the fly
;  adjust for pixel offsets of sub=area
unno_sgl,xllo+xs,yllo+ys,scatlp,mefit_sgl,ifit_sgl,nounnofit
	have_un = 0
	if nounnofit eq 0 then begin
		i_mag = mefit_sgl(*,0)
		i_fit = ifit_sgl(*)
		q_fit = mefit_sgl(*,1)
		u_fit = mefit_sgl(*,2)
		v_fit = mefit_sgl(*,3)
		have_un = 1L
	endif

;				    ;Read 2nd Unno component.
;		if twoComp then begin
;			i_2nd = as_fb(offfb+5)
;			q_2nd = as_fb(offfb+6)
;			u_2nd = as_fb(offfb+7)
;			v_2nd = as_fb(offfb+8)
;
;				    ;Swap bytes for !version.os
;				    ;(linux known to do this).
;			;if  !version.os eq 'linux'  then begin
;				;i_2nd = swap_endian( i_2nd )
;				;q_2nd = swap_endian( q_2nd )
;				;u_2nd = swap_endian( u_2nd )
;				;v_2nd = swap_endian( v_2nd )
;			;end
;
;				    ;Recover 1st Unno component.
;			b0 = bzero(zxc,zyc)*aa.pp(aa.cct).value(zxc,zyc)
;			i_1st = i_mag-i_2nd-b0
;			q_1st = q_fit-q_2nd
;			u_1st = u_fit-u_2nd
;			v_1st = v_fit-v_2nd
;		end

				    ;Factor to allow margin.
	fm = 1.05
				    ;Plot unsolved point.
	if not have_un then begin
				    ;Do plotting in pixmap window.
		wset, wp
				    ;Plot i profile.
		!p.region = [ 0.0, 0.5, 0.5, 1.0 ]
		max_i = max(i_obs)
		mx = fm*max_i
		plot, [0,numx-1], [0,mx], ystyle=1, /nodata $
		, charsize=charsize, background=aa.white, color=aa.black
		plots, vecx, i_obs, color=aa.black

				    ;Plot q profile.
		!p.region = [ 0.5, 0.5, 1.0, 1.0 ]
		max_q = max(abs(q_obs)) > max(abs(u_obs)) 
		if fix_q ne 0. then  mx=fix_q  else  mx=max_q
		plot, [0,numx-1], fm*[-mx,mx], ystyle=1, /nodata, /noerase $
		, charsize=charsize, background=aa.white, color=aa.black
		plots, vecx, q_obs, color=aa.black

				    ;Plot u profile.
		!p.region = [ 0.0, 0.0, 0.5, 0.5 ]
		plot, [0,numx-1], fm*[-mx,mx], ystyle=1, /nodata, /noerase $
		, charsize=charsize, background=aa.white, color=aa.black
		plots, vecx, u_obs, color=aa.black

				    ;Plot v profile.
		!p.region = [ 0.5, 0.0, 1.0, 0.5 ]
		max_v = max(abs(v_obs))
		if fix_v ne 0. then  mx=fix_v  else  mx=max_v
		plot, [0,numx-1], fm*[-mx,mx], ystyle=1, /nodata, /noerase $
		, charsize=charsize, background=aa.white, color=aa.black
		plots, vecx, v_obs, color=aa.black

				    ;Print some information.
;  print out coordinates from start of full map
		xyouts, /device, align=.5, sz, sz-3, color=aa.black $
		, '('+stringit(xllo+xs)+','+stringit(yllo+ys)+')', charsize=1.7

				    ;Copy pixmap to screen.
		wset, wu  &  device, copy=[0,0,2*sz,2*sz,0,0,wp]
;		if twoComp then begin
;			wset, wt  &  device, copy=[0,0,2*sz,2*sz,0,0,wp]
;		end
	end
				    ;Plot profiles & Unno fit.
	if have_un then begin
				    ;Do plotting in pixmap window.
		wset, wp
				    ;Plot i profiles with Unno fit.
		!p.region = [ 0.0, 0.5, 0.5, 1.0 ]
		max_i = max(i_obs) > max(i_fit)
		mx=max_i
		plot, [0,numx-1], fm*[0,mx], ystyle=1, /nodata $
		, charsize=charsize, background=aa.white, color=aa.black
		for iline = 0,nwrng-1 do begin
;  plot fitted i_mag in blue
			plots, vecx(iwstrt(iline):iwend(iline)), $
				i_mag(nlinr(iline,0):nlinr(iline,1)), color=aa.blue
			plots, vecx(iwstrt(iline):iwend(iline)), $
				i_fit(nlinr(iline,0):nlinr(iline,1)), color=aa.red
		endfor
;  bypass magnetic only I-profile plotting
;		plots, vecx, i_mag, color=aa.red
		plots, vecx, i_obs, color=aa.black

				    ;Plot q profile with Unno fit.
		!p.region = [ 0.5, 0.5, 1.0, 1.0 ]
		max_q = max(abs(q_obs)) > max(abs(u_obs)) $
	 	      > max(abs(q_fit)) > max(abs(u_fit)) 
;		if twoComp then $
;		max_q = max_q $
;	 	      > max(abs(q_1st)) > max(abs(u_1st)) $
;	 	      > max(abs(q_2nd)) > max(abs(u_2nd))
		if fix_q ne 0. then  mx=fix_q  else  mx=max_q
		plot, [0,numx-1], fm*[-mx,mx], ystyle=1, /nodata, /noerase $
		, charsize=charsize, background=aa.white, color=aa.black
		for iline = 0,nwrng-1 do begin
			plots, vecx(iwstrt(iline):iwend(iline)), $
				q_fit(nlinr(iline,0):nlinr(iline,1)), color=aa.red
		endfor
		plots, vecx, q_obs, color=aa.black

				    ;Plot u profile with Unno fit.
		!p.region = [ 0.0, 0.0, 0.5, 0.5 ]
		plot, [0,numx-1], fm*[-mx,mx], ystyle=1, /nodata, /noerase $
		, charsize=charsize, background=aa.white, color=aa.black
		for iline = 0,nwrng-1 do begin
			plots, vecx(iwstrt(iline):iwend(iline)), $
				u_fit(nlinr(iline,0):nlinr(iline,1)), color=aa.red
		endfor
		plots, vecx, u_obs, color=aa.black

				    ;Plot v profile with Unno fit.
		!p.region = [ 0.5, 0.0, 1.0, 0.5 ]
		max_v = max(abs(v_obs)) > max(abs(v_fit))
;		if twoComp then $
;		max_v = max_v > max(abs(v_1st)) > max(abs(v_2nd))
		if fix_v ne 0. then  mx=fix_v  else  mx=max_v
		plot, [0,numx-1], fm*[-mx,mx], ystyle=1, /nodata, /noerase $
		, charsize=charsize, background=aa.white, color=aa.black
		for iline = 0,nwrng-1 do begin
			plots, vecx(iwstrt(iline):iwend(iline)), $
				v_fit(nlinr(iline,0):nlinr(iline,1)), color=aa.red
		endfor
		plots, vecx, v_obs, color=aa.black

				    ;Print some information.
;  print out coordinates from start of full map
		xyouts, /device, align=.5, sz, sz-3, color=aa.black $
		, '('+stringit(xllo+xs)+','+stringit(yllo+ys)+')', charsize=1.7

				    ;Copy pixmap to screen.
		wset, wu  &  device, copy=[0,0,2*sz,2*sz,0,0,wp]
	end
				    ;Plot two component fit.
;	if have_un and twoComp then begin
;
;				    ;Do plotting in pixmap window.
;		wset, wp
;
;		!p.region = [ 0.0, 0.5, 0.5, 1.0 ]
;		mx = max_i
;		plot, [0,numx-1], fm*[0,mx], ystyle=1, /nodata $
;		, charsize=charsize, background=aa.white, color=aa.black
;		plots, vecx, i_mag, color=aa.red
;		plots, vecx, i_1st, color=aa.blue
;		plots, vecx, i_2nd, color=aa.green
;		plots, vecx, i_fit, color=aa.red
;		plots, vecx, i_obs, color=aa.black
;
;				    ;Plot q profile with Unno fit.
;		!p.region = [ 0.5, 0.5, 1.0, 1.0 ]
;		if fix_q ne 0. then  mx=fix_q  else  mx=max_q
;		plot, [0,numx-1], fm*[-mx,mx], ystyle=1, /nodata, /noerase $
;		, charsize=charsize, background=aa.white, color=aa.black
;		plots, vecx, q_1st, color=aa.blue
;		plots, vecx, q_2nd, color=aa.green
;		plots, vecx, q_obs, color=aa.black
;
;				    ;Plot u profile with Unno fit.
;		!p.region = [ 0.0, 0.0, 0.5, 0.5 ]
;		plot, [0,numx-1], fm*[-mx,mx], ystyle=1, /nodata, /noerase $
;		, charsize=charsize, background=aa.white, color=aa.black
;		plots, vecx, u_1st, color=aa.blue
;		plots, vecx, u_2nd, color=aa.green
;		plots, vecx, u_obs, color=aa.black
;
;				    ;Plot v profile with Unno fit.
;		!p.region = [ 0.5, 0.0, 1.0, 0.5 ]
;		if fix_v ne 0. then  mx=fix_v  else  mx=max_v
;		plot, [0,numx-1], fm*[-mx,mx], ystyle=1, /nodata, /noerase $
;		, charsize=charsize, background=aa.white, color=aa.black
;		plots, vecx, v_1st, color=aa.blue
;		plots, vecx, v_2nd, color=aa.green
;		plots, vecx, v_obs, color=aa.black
;
;				    ;Print some information.
;		xyouts, /device, align=.5, sz, sz-3, color=aa.black $
;		, '('+stringit(xs)+','+stringit(ys)+')', charsize=1.7
;
;				    ;Copy pixmap to screen.
;		wset, wt  &  device, copy=[0,0,2*sz,2*sz,0,0,wp]
;	end
				    ;Do gray scale plot if clicked.
	if err_ eq 1 then begin
				    ;Erase instructions.
		azam_message, aa, color=aa.red
		in_red = 1
				    ;Flag so (x,y) plot will not erase.
		xsav = -1
				    ;Save profile image step postion number.
		xstep = xs
		ystep = ys
				    ;Read and plot slit position image.
		if alongy then begin
			wset, wj  &  erase, aa.white
	        i_obs = lonarr(nmstep,numx,/nozero)
	        q_obs = lonarr(nmstep,numx,/nozero)
	        u_obs = lonarr(nmstep,numx,/nozero)
	        v_obs = lonarr(nmstep,numx,/nozero)

			temp = lonarr(nmstep,numx,/nozero)
			temp(*,*) = transpose(obsdat(*,0,*,ys))
			iscale = 32767./max(temp)
			i_obs(*,*) = long(temp*iscale)

	        ;i_obs(*,*) = transpose(obsdat(*,0,*,ys))
	        q_obs(*,*) = transpose(obsdat(*,1,*,ys))
	        u_obs(*,*) = transpose(obsdat(*,2,*,ys))
	        v_obs(*,*) = transpose(obsdat(*,3,*,ys))

			tvasp, long(i_obs) and '0000FFFF'XL, 0, numx $
			, /gray & i_obs=0

			mx = max(abs(q_obs)) > max(abs(u_obs))
			tvasp, q_obs, nmstep, numx, min=-mx, max=mx $
			, /gray & q_obs=0
			tvasp, u_obs,      0,    0, min=-mx, max=mx $
			, /gray & u_obs=0

			mx = max(abs(v_obs))
			tvasp, v_obs, nmstep,    0, min=-mx, max=mx $
			, /gray & v_obs=0

;  print out coordinates from start of full map
			xyouts, nmstep, 2*numx+3, 'slit pix '+stringit(yllo+ys) $
			, /device, align=.5, color=aa.black, charsize=1.7
			q_obs = replicate(aa.white,1,20)
			;tv, q_obs,        xs-snum0, 2*numx-20
			;tv, q_obs, nmstep+xs-snum0, 2*numx-20
			tv, q_obs,        xs-snum0,   numx-20
			tv, q_obs, nmstep+xs-snum0,   numx-20
			tv, q_obs, 2*nmstep+xs-snum0,   numx-20
			tv, q_obs, 3*nmstep+xs-snum0,   numx-20
		end
				    ;Read and plot I profile scan_step. 
;		point_lun, unit, 512+(xs-snum0)*rcd+512
;		i_obs = intarr(numx,numy,/nozero) & readu, unit, i_obs
		i_obs = intarr(numx, numy, /nozero)
		temp = lonarr(numx,numy, /nozero)
		temp(*,*) = obsdat(*,0,xs,*)
		iscale = 32767./max(temp)
		i_obs(*,*) = fix(temp*iscale)


		wset, wg  &  erase, aa.white
		;tvasp, long(i_obs) and '0000FFFF'XL, 0, numy, /gray
		tvasp, long(i_obs) and '0000FFFF'XL, 0, 0, /gray
		i_obs = 0
				    ;Read QUV scan steps.
		q_obs = intarr(numx, numy, /nozero)
		q_obs(*,*) = obsdat(*,1,xs,*)
		u_obs = intarr(numx, numy, /nozero)
		u_obs(*,*) = obsdat(*,2,xs,*)
		v_obs = intarr(numx, numy, /nozero)
		v_obs(*,*) = iscale*obsdat(*,3,xs,*)


				    ;Plot QUV to same gray scale.
		mx = max(abs(q_obs)) > max(abs(u_obs))
		tvasp, q_obs, numx, 0, min=-mx, max=mx, /gray
		tvasp, u_obs,2*numx,    0, min=-mx, max=mx, /gray

		mx = max(abs(v_obs))
		tvasp, v_obs, 3*numx,    0, min=-mx, max=mx, /gray

				    ;Plot position along slit.
			;  print out coordinates from start of full map
			xyouts, 2*numx,numy+3, 'scan pix '+stringit(xllo+xs) $
			, /device, align=.5, color=aa.black, charsize=1.7
		q_obs = replicate(aa.white,20)
		tv, q_obs,   numx-20,      ys
		tv, q_obs,   numx-20, numy+ys
		tv, q_obs, 2*numx-20,      ys
		tv, q_obs, 2*numx-20, numy+ys

				    ;Print some information.
;  print out coordinates from start of full map
		xyouts, numx, 2*numy+3, 'step '+stringit(xllo+xs) $
		, /device, align=.5, color=aa.black, charsize=1.7
	end
				    ;Go to beginning of infinite loop.
goto, infinity
				    ;I/O error exit.
error0: print, !err_string
aa.spectra=''
				    ;Normal exit.
break0:
wset, aa.winb  &  erase
				    ;Delete windows.
if n_elements(wt) ne 0 then  wdelete, wt
if n_elements(wu) ne 0 then  wdelete, wu
if n_elements(wp) ne 0 then  wdelete, wp
if n_elements(wg) ne 0 then  wdelete, wg
if n_elements(wj) ne 0 then  wdelete, wj

				    ;Restore display windows.
tw, aa.ww(0).win, aa.ww(0).img, 0, 0
tw, aa.ww(1).win, aa.ww(1).img, 0, 0
tw, aa.ww(2).win, aa.ww(2).img, 0, 0

				    ;Restore system variables.
!p = sav_p

end
