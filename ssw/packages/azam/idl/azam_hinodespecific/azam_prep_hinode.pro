;pro azam_prep_hinode,merlin_fitsfile,byte_ambig,afiles_dir=afiles_dir
;  for now do not output byte_ambig
pro azam_prep_hinode,merlin_fitsfile,afiles_dir=afiles_dir

;  routine to generate the ASP-format a_* files needed for
;  input to AZAM.  This routine reads the FITS output from MERLIN,
;  then prepares the needed a_files and writes them out

;INPUTS:	merlin_fitsfile	= path to MERLIN FITS inversion output
;		
;OUTPUTS:	byte_ambig	= byte array with second bit altered
;				  for AZAM ambiguity resolution.  First
;				  bit of this array is reserved for 
;				  Metcalf auto ambig resolution. Set to
;				  zero means unchanged from original MERLIN
;				  observer-frame azimuth (a_azm), set to 1 means
;				  opposite observer-frame azimuth

;KEYWORDS:	afiles_dir	= output directory for a_* files.  If not
;				  set, files output to the present working
;				  directory


;  common area for inversion results read in rdmerlinftis.pro
common com_merlinresults, $
	fld, psi, azm, eta0, dmp, dop,$
	bzero, b1mu, vmac, cen1, cen2, alpha, $
	delta, cct, chisq, chisq_i, chisq_q, $
	chisq_u, chisq_v, signed_v, niter, occt, $
	rconv, netpol, netpol_full, $
	fld_er, psi_er, azm_er, eta0_er, dmp_er, dop_er, $
	bzero_er, b1mu_er, vmac_er, cen1_er, cen2_er, $
	alpha_er, delta_er,xx,yy,slat,slong, $
	yr,mnth,dy,hr,minit,sec



outdir = './'
if keyword_set(afiles_dir) then outdir = afiles_dir
; !!!check to see if directory exists, if not, create it!!!


;  first read in the MERLIN inversion results
rdmerlinfits,afiles_dir,merlin_fitsfile

;  generate the a_files
azam_hinode2afiles,outdir

;;  prepare output files
;sz = size(cct)
;nxx = sz(1) & nyy = sz(2)
;byte_ambig = bytarr(nxx,nyy)
;;  for present, set the byte to 0, 1.  Later must change to
;;  setting second bit
;
;
;
;stop

end
