pro azam_zoom, aa, hh
;+
;
;	procedure:  azam_zoom
;
;	purpose:  zoom portion of an azam op.
;
;	author:  paul@ncar, 8/93	(minor mod's by rob@ncar)
;
;==============================================================================
;
;       Check number of parameters.
;
if n_params() eq 0 then begin
	print
	print, "usage:	azam_zoom, aa, hh"
	print
	print, "	Compute an azam structure for an ASP op and display"
	print, "	window."
	print
	print, "	Arguments"
	print, "		aa	- input/output azam data structure"
	print, "		hh	- input highlight structure"
	print
	return
endif
;-
				    ;Prompt for area center.
azam_click_xy, aa, 'Click on center of area.', xctr, yctr
wset, aa.winb  &  erase, aa.red
				    ;Set magnification factor.
t = 6L
				    ;Size original area to fit on screen.
xsz = 510L/t
ysz = 760L/t
				    ;Set zoom image bounds.
xx0 = 0 > ( (xctr-xsz/2) < (aa.xdim-xsz) )
yy0 = 0 > ( (yctr-ysz/2) < (aa.ydim-ysz) )
xx1 = (xx0+xsz) < (aa.xdim-1)
yy1 = (yy0+ysz) < (aa.ydim-1)
xdim = xx1-xx0+1
ydim = yy1-yy0+1
				    ;Sub area of already magnified images.
tx0 = aa.t*xx0
ty0 = aa.t*yy0
tx1 = aa.t*xx1+aa.t-1
ty1 = aa.t*yy1+aa.t-1
				    ;Form where there is data in sub arrays.
tmp0 = lonarr(aa.xdim,aa.ydim)
tmp0(aa.pxy) = 1
tmp0 = tmp0(xx0:xx1,yy0:yy1)
npxy = total(tmp0)

tmp1 = lonarr(aa.xdim,aa.ydim)
tmp1(aa.sxy) = 1
tmp1 = tmp1(xx0:xx1,yy0:yy1)
nsxy = total(tmp1)
                                    ;Structure for a parameter.
pp = $
{ value:        fltarr(xdim,ydim,/nozero) $
, img:          bytarr(xdim,ydim,/nozero) $
, a_np:         -1L $
, color:        aa.pp(0).color $
, color0:       aa.pp(0).color $
, name:         '' $
, bar:		'' $
}
                                    ;Structure for an image window.
ww = $
{ win:          1L $
, img:          bytarr(t*xdim,t*ydim,/nozero) $
, iprm:         1L $
, drag:         1L $
}
				    ;Create azam sub structure.
az = $
{ index:	3L $
, stretch:	aa.stretch $
, angxloc:	aa.angxloc $
, npoints:	aa.npoints $
, nsolved:	aa.nsolved $
, xdim:		xdim $
, ydim:		ydim $
, pxy:		where(tmp0) $
, sxy:		where(tmp1) $
, pbkg:		aa.pbkg $
, sbkg:		aa.sbkg $
, head:		aa.head $
, vec_pxy:	aa.vec_pxy $
, vec_sxy:	aa.vec_sxy $
, mm_per_deg:	aa.mm_per_deg $
, wina:		aa.wina $
, winb:		aa.winb $
, winp:		aa.winp $
, mgfy:		aa.mgfy $
, bx:		-1L $
, by:		-1L $
, bs:		0L $
, black:	aa.black $
, gray:		aa.gray $
, white:	aa.white $
, yellow:	aa.yellow $
, red:		aa.red $
, blue:		aa.blue $
, green:	aa.green $
, cen_lat:	aa.cen_lat $
, cen_e_w:	aa.cen_e_w $
, pix_deg:	aa.pix_deg $
, zoom:		1L $
, x0:		xx0 $
, y0:		yy0 $
, xrpnt:	aa.xrpnt( xx0:xx1,yy0:yy1) $
, yrpnt:	aa.yrpnt( xx0:xx1,yy0:yy1) $
, nprm:		aa.nprm $
, azm_amb:	aa.azm_amb $
, azz_amb:	aa.azz_amb $
, zen_amb:	aa.zen_amb $
, flx_amb:	aa.flx_amb $
, jz_amb:	aa.jz_amb $
, azz_orig:	aa.azz_orig $
, azz_ref:	aa.azz_ref $
, zen_orig:	aa.zen_orig $
, zen_ref:	aa.zen_ref $
, azm:		aa.azm $
, azz:		aa.azz $
, zen_ang:	aa.zen_ang $
, jz:		aa.jz $
, flx:		aa.flx $
, psi:		aa.psi $
, fld:		aa.fld $
, cct:		aa.cct $
, alpha:	aa.alpha $
, cen1:		aa.cen1 $
, extra0:	aa.extra0 $
, extra1:	aa.extra1 $
, ww:		replicate( ww, 3 ) $
, pp:		replicate( pp, aa.nprm ) $
, b__ut:	aa.b__ut( xx0:xx1,yy0:yy1) $
, b__lon:	aa.b__lon(xx0:xx1,yy0:yy1) $
, b__lat:	aa.b__lat(xx0:xx1,yy0:yy1) $
, b__e_w:	aa.b__e_w(xx0:xx1,yy0:yy1) $
, pdat:		aa.pdat(  xx0:xx1,yy0:yy1) $
, sdat:		aa.sdat(  xx0:xx1,yy0:yy1) $
, rfrnc:	aa.rfrnc( xx0:xx1,yy0:yy1) $
, rslv:		aa.rslv(  xx0:xx1,yy0:yy1) $
, defroi:	aa.defroi(xx0:xx1,yy0:yy1) $
, mn_lat:	aa.mn_lat $
, mn_e_w:	aa.mn_e_w $
, blt:		aa.blt $
, bwd:		aa.bwd $
, btm:		aa.btm $
, csiz:		aa.csiz $
, winfree:	aa.winfree $
, t:		t $
, t0:		aa.t $
, pwr:		aa.pwr $
, lock:		aa.lock $
, cri:		aa.cri $
, anti:		aa.anti $
, prime:	aa.prime $
, hilite:	aa.hilite $
, rehi:		1L $
, spectra:	aa.spectra $
, dty:		aa.dty $
, ps_dir:	aa.ps_dir $
, rframe:	aa.rframe $
, fname:	'' $
, fdim:		0L $
, what_now:	'' $
}
				    ;Set parameter structures.
az.pp(*).value  = aa.pp(*).value(xx0:xx1,yy0:yy1)
az.pp(*).img    = aa.pp(*).img(  xx0:xx1,yy0:yy1)
az.pp(*).a_np   = aa.pp(*).a_np
az.pp(*).color  = aa.pp(*).color
az.pp(*).color0 = aa.pp(*).color0
az.pp(*).name   = aa.pp(*).name
az.pp(*).bar    = aa.pp(*).bar
				    ;Set window some of structures.
az.ww(*).iprm =	aa.ww(*).iprm
az.ww(*).drag =	aa.ww(*).drag
				    ;Reset magnified window structure.
az.mgfy.xc = xdim/2
az.mgfy.yc = ydim/2
az.mgfy.x0 = 0 > (xdim/2-az.mgfy.siz/2)
az.mgfy.y0 = 0 > (ydim/2-az.mgfy.siz/2)

				    ;Initial highlight structure
				    ;(redone by azam_image.pro).
hz = hh
				    ;Open display windows.
xsize = t*xdim+3*az.bwd
ysize = t*ydim > 300
if aa.winfree then begin
	window, /free, xsize=xsize, ysize=ysize, title=aa.dty
end else begin
	window, /free, xsize=xsize, ysize=ysize, title=aa.dty $
	, xpos=0, ypos=900-(6+3*az.bwd+ysize)
end
az.ww(0).win  = !d.window

if aa.winfree then begin
	window, /free, xsize=xsize, ysize=ysize, title=aa.dty
end else begin
	window, /free, xsize=xsize, ysize=ysize, title=aa.dty $
	, xpos=(1144-xsize)/2, ypos=900-(6+3*az.bwd+ysize)
end
az.ww(2).win  = !d.window

if aa.winfree then begin
	window, /free, xsize=xsize, ysize=ysize, title=aa.dty
end else begin
	window, /free, xsize=xsize, ysize=ysize, title=aa.dty $
	, xpos=1144-xsize, ypos=900-(6+3*az.bwd+ysize)
end
az.ww(1).win  = !d.window
				    ;Show ascii window.
wshow, aa.wina
				    ;Display interactive images.
for win=0L,2L do  azam_display, win, az, hz
azam_mgfy, az
				    ;Do bulk of azam_program.
azam_bulk, az, hz
				    ;Delete zoom windows.
wdelete, az.ww(0).win , az.ww(1).win , az.ww(2).win

				    ;Check if user wants to keep changes.
wset, aa.winb  &  erase, aa.red
				    ;Update parameter values & byte images.
aa.pp(*).value(xx0:xx1,yy0:yy1) = az.pp(*).value
aa.pp(*).img(xx0:xx1,yy0:yy1) = az.pp(*).img

				    ;Update reference & current arrays.
aa.rfrnc(xx0:xx1,yy0:yy1) = az.rfrnc
aa.rslv( xx0:xx1,yy0:yy1) = az.rslv
				    ;Update defroi array.
if (where(aa.defroi(xx0:xx1,yy0:yy1) ne az.defroi ))(0) ge 0 then begin
	aa.defroi = 0B
	aa.defroi(xx0:xx1,yy0:yy1) = az.defroi
	if az.defroi(0,0) eq 255B then  aa.defroi=255B
end
				    ;Update image color.
for iprm=0L,aa.nprm-1 do begin
	if  az.pp(iprm).color.min    ne  aa.pp(iprm).color.min    $
	or  az.pp(iprm).color.max    ne  aa.pp(iprm).color.max    $
	or  az.pp(iprm).color.gray   ne  aa.pp(iprm).color.gray   $
	or  az.pp(iprm).color.invert ne  aa.pp(iprm).color.invert $
	or  az.pp(iprm).color.back   ne  aa.pp(iprm).color.back   then begin
		aa.pp(iprm).color = az.pp(iprm).color
		azam_op_image, aa, iprm
	end
end
				    ;Update highlight structure
				    ;(redone by azam_image.pro).
hh = hz
aa.rehi = 1L
				    ;Display interactive images.
aa.ww(*).iprm = az.ww(*).iprm
for win=0L,2L do  azam_display, win, aa, hh
azam_mgfy, aa

end
