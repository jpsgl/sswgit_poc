pro azam_azam, dir, new_azam, dummy, type=type, azam=aa
;+
;
;	procedure:  azam_azam
;
;	purpose:  disambiguate a_* file directory based
;		  new_azam argument or AZAM file in the directory
;
;	author:  paul@ncar, 6/93	(minor mod's by rob@ncar)
;
;==============================================================================
;
;       Check number of parameters.
;
if n_params() gt 2 then begin
	print
	print, "usage:	azam_azam [, dir [, new_azam ] ]"
	print
	print, "	Disambiguate a_* file directory based"
	print, "	new_azam argument or AZAM file in the directory."
	print, "	No arguments or keywords changed."
	print
	print, "	Arguments"
	print, "		dir	- directory with a_* files (def '')"
	print, "		new_azam	- input sight azimuth file"
	print, "			  (def: read AZAM file in dir or if"
	print, "			  no AZAM file read a_azm file)"
	print, "	Keywords"
	print, "		type	- set for typed ok to overwrite"
	print, "		azam    - input azam structure.
	print
	return
endif
;-
print
print, 'Starting azam_azam.pro procedure'

				    ;Get directory path.
dty = ''
if n_elements(dir) ne 0 then dty=dir
if dty ne '' then if strmid(dty,strlen(dty)-1,1) ne '/' then dty=dty+'/'
if dty ne '' then print, 'directory: '+dty

				    ;Read a_azm sight azimuth file.
a_azm = read_floats( dty+'a_azm', error )
if error ne 0 then  begin
	print, 'error reading a_azm file'
	print, !err_string
	stop, 'Enter .c to return'
	return
end
				    ;Read AZAM file.
azam_file = read_floats( dty+'AZAM', error )

				    ;Create AZAM file on read error.
if error ne 0 then  begin
	azam_file = a_azm
	if write_floats( dty+'AZAM', a_azm, err ) eq 0 $
	then  print, 'AZAM file created as copy of a_azm'
end
				    ;If AZAM file intended for azimuth
				    ;resolution, check for correct length.
if n_elements(new_azam) eq 0 $
and n_elements(azam_file) ne n_elements(a_azm) then  begin
	print, 'AZAM file can not be used for azimuth resolution.'
	print, 'a_azm and AZAM files have different lengths.'
	stop, 'Enter .c to continue'
	return
end
				    ;Select azimuth resolution array.
if n_elements(new_azam) ne 0 $
then  azam = new_azam $
else  azam = azam_file
				    ;Check that files agree in length.
if  n_elements(azam) ne n_elements(a_azm)  then begin
	print,'azam_azam.pro: Something bad wrong.
	print,'Azimuth resolution array and a_azm file differ in length.'
	stop, 'Enter .c to continue'
	return
end
				    ;Find where azimuth resolution differ.
chg = where( (((azam-a_azm)+(760.+90.)) mod 360.) gt 180., nchg )

				    ;If no swaps update AZAM file.
if nchg eq 0 then begin
				    ;Check if AZAM file shows different
				    ;azimuth resolution.
	nwhr = 1
	if n_elements(azam_file) eq n_elements(a_azm) then $
	whr = where( (((azam_file-a_azm)+(760.+90.)) mod 360.) gt 180., nwhr )

	if nwhr ne 0 then begin
		if write_floats( dty+'AZAM', a_azm, err ) eq 0 $
		then  print, 'AZAM file overwritten by a_azm file'
	end else begin
		print, 'Directory is up to date'
	end

	return
end
				    ;Prompt for ok to overwrite files.
if n_elements(aa) ne 0 then begin

	clk = pop_cult( /string, winfree=aa.winfree $
	, title = 'Want to update a_* files ?' $
	, [ 'yes', '^gno', '^g** DEFAULT **', '** HELP **'] $
	, help='azam', arg0='window', arg1=aa, arg2= $
      ["     yes      - update the directory.  The current line of sight"$
      ,"                azimuth resolution may differ from the file 'a_azm'."$
      ,"                A 'yes' reply will update file set 'a_azm', 'a_1azm,'"$
      ,"                'a_1incl', 'a_2azm', 'a_2incl', 'AZAM'."$
      ,"     no       - do not update the directory."$
      ,"** DEFAULT ** - same as clicking other green button"$
      ," ** HELP **   - print this information"$
      ]	)

	if clk eq 'no' or clk eq '** DEFAULT **' then return

end else if n_elements(type) then begin
	print
	yn = ''
	read, 'Overwrite a_* files ? (y or n)--> ', yn
	if  yn ne 'y'  then return
end

print,'Updateing azimuth and inclination files'

				    ;Update sight azimuth data.
a_azm(chg) = a_azm(chg)+180.
whr = where( a_azm gt 180., nwhr )
if nwhr ne 0 then  a_azm(whr) = a_azm(whr)-360.
i = write_floats( dty+'a_azm', a_azm, err )
i = write_floats( dty+'AZAM',  a_azm, err )

				    ;On error Write AZAM file to
				    ;alternate directory.
if  i  and  n_elements(aa) ne 0 then $
if azam_out_dir( aa, o_dir ) then $
i = write_floats( o_dir+'AZAM',  a_azm, err )

				    ;Update local azimuth.
tmp0 = read_floats( dty+'a_1azm' )
tmp1 = read_floats( dty+'a_2azm' )
tmp       = tmp0(chg)
tmp0(chg) = tmp1(chg)
tmp1(chg) = tmp
i = write_floats( dty+'a_1azm', tmp0, err )
i = write_floats( dty+'a_2azm', tmp1, err )

				    ;Update local inclination.
tmp0 = read_floats( dty+'a_1incl' )
tmp1 = read_floats( dty+'a_2incl' )
tmp       = tmp0(chg)
tmp0(chg) = tmp1(chg)
tmp1(chg) = tmp
i = write_floats( dty+'a_1incl', tmp0, err )
i = write_floats( dty+'a_2incl', tmp1, err )

print,'Finished azam_amam.pro'
return


end
