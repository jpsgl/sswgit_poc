;
;	Compile azam codes.
;
@azam_tools
@azam_menu
@azam_help
;
pro azam, dir, dummy, winfree=winfree
;+
;
;	procedure:  azam
;
;	purpose:  use interactive cursor to disambiguate azimuth in
;		  directories with a_* files
;
;	author:  paul@ncar, 6/93
;
;==============================================================================
;
;       Check number parameters.
;
if n_params() gt 1 then begin
	print
	print, "usage:	azam [, dir ]"
	print
	print, "	Use interactive cursor to disambiguate azimuth in"
	print, "	directories with a_* files."
	print
	print, "	Arguments"
	print, "		dir	- directory path (string;"
	print, "			  def=use current working directory)"
	print
	print, "	Keywords"
	print, "		winfree	- set to open windows postion free"
	return
endif
;-
				    ;Directory path.
				    ;Full path to current working directory.
				    ;Append directory path with / 
if n_elements(dir) ne 0 then  dty=dir  else  dty=''
if dty eq '' then  cd, current=dty
if strmid(dty,strlen(dty)-1,1) ne '/' then dty=dty+'/'

				    ;Read header, check for existence.
junk = read_floats( dty+'a___header', error )
if  error ne 0  then begin
	print, !err_string
	print, 'a_* files may not exit or directory path wrong'
	return
end
				    ;Save system variables.
sav_p = !p
sav_x = !x
sav_y = !y
sav_z = !z
sav_o = !order
sav_n = !d.name
				    ;Set 'X' windows.
set_plot, 'X'
				    ;Set font.
!p.font = -1
				    ;Initial null azam structure.
				    ;Redefined by azam_op.pro to
				    ;larger structure.
aa0 = $
{ index:	-1L $
, dty:		dty $
, what_now:	'' $
, wina:		-1L $
, winb:		-1L $
, winp:		-1L $
, blt:		110L $		    ;Interactive winb window button length.
, bwd:		18L $		    ;Interactive winb window button width.
, btm:		4L $		    ;Interactive winb window button text marg.
, csiz:		0. $		    ;Character size for IDL's xyouts.
, winfree:	keyword_set(winfree) $ ;Not nailed down sindows.
}

				    ;Open button window.
if aa0.winfree then begin
	window, /free, xsize=6*aa0.blt, ysize=3*aa0.bwd $
	, title='azam: interactive buttons'
end else begin
	window, /free, xsize=6*aa0.blt, ysize=3*aa0.bwd $
	, xpos=0, ypos=900-3*aa0.bwd $
	, title='azam: interactive buttons'
end
aa0.winb=!d.window
				    ;Open ascii window.
if aa0.winfree then begin
	window, /free, xsize=(xsize=3*aa0.blt+10), ysize=13*aa0.bwd $
	, title='azam: data at cursor'
end else begin
	window, /free, xsize=(xsize=3*aa0.blt+10), ysize=13*aa0.bwd $
	, xpos=0, ypos=40 $
	, title='azam: data at cursor'
end
aa0.wina=!d.window
				    ;Open pixmap ascii window.
				    ;wina is updated by device copy of
				    ;this window.
window, /free, xsize=xsize, ysize=13*aa0.bwd, /pixmap
aa0.winp=!d.window
                                    ;Get character size for IDL's xyouts.
erase, 0  &  xyouts, 0, 0, 'X', color=1, /device
cpix = 1.+max(where(tvrd()))/xsize
aa0.csiz = (aa0.bwd-2*aa0.btm)/cpix
				    ;Initial null structure for second op
				    ;if loaded by * OTHER OP * option.
aa1 = aa0
				    ;Create op0.
azam_op, aa0, hh0
if aa0.what_now eq '** EXIT AZAM **' then  goto, exit_azam

				    ;active = 0 if op0 is active.
				    ;active = 1 if op1 is active.
active = 0
				    ;Infinite loop till exit.
while 1 do begin
				    ;Activate op0.
if  active eq 0  then begin
				    ;Run bulk of program
	azam_bulk, aa0, hh0

	case aa0.what_now of
				    ;Check for zoom images.
	'zoom': azam_zoom, aa0, hh0

				    ;Check for ** EXIT AZAM **
	'** EXIT AZAM **': goto, exit_azam

				    ;Check for replace op request.
	'** REPLACE OP **': begin
				    ;Prompt for directory path.
		dty = azam_dir(aa0)
				    ;Check for valid directory path.
		if dty ne 'quit' then begin

				    ;Permit saving active op.
			azam_azam, aa0.dty, azam_a_azm(aa0), azam=aa0

				    ;Recreate op0.
			aa0.dty = dty
			azam_op, aa0, hh0
			if aa0.what_now eq '** EXIT AZAM **' $
			then  goto, exit_azam
		end
		end

;				    ;Switch to other op if it exits.
;  bypass option for OTHER OP
	'* OTHER OP *': begin
;				    ;If not present create another op.
;		if  aa1.index eq -1  then begin
;
;				    ;Prompt for directory path.
;			dty = azam_dir(aa0)
;
;				    ;Check for valid directory path.
;			if dty ne 'quit' then begin
;
;				    ;Permit saving active op.
;				print,'Warning: may exceed core limit.'
;				azam_azam, aa0.dty, azam_a_azm(aa0), azam=aa0
;
;				    ;Create op1.
;				aa1.dty = dty
;				azam_op, aa1, hh1
;				if aa1.what_now eq '** EXIT AZAM **' $
;				then  goto, exit_azam
;			end
;		end
;				    ;Switch to other op if it exits.
;		if  aa1.index ne -1  then  active = 1
		end
	else:
	end
				    ;Activate op1.
end else begin
				    ;Run bulk of program
	azam_bulk, aa1, hh1

	case aa1.what_now of
				    ;Check for zoom images.
	'zoom': azam_zoom, aa1, hh1

				    ;Check for ** EXIT AZAM **
	'** EXIT AZAM **': goto, exit_azam

				    ;Check for replace op request.
	'** REPLACE OP **': begin

				    ;Prompt for directory path.
		dty = azam_dir(aa1)
				    ;Check for valid directory path.
		if dty ne 'quit' then begin

				    ;Permit saving active op.
			azam_azam, aa1.dty, azam_a_azm(aa1), azam=aa1

				    ;Recreate op1.
			aa1.dty = dty
			azam_op, aa1, hh1
			if aa1.what_now eq '** EXIT AZAM **' $
			then  goto, exit_azam
		end
		end

;  bypass option for OTHER OP
;				    ;Check for other op request.
;	'* OTHER OP *': active = 0

	else:
	end
end
end
				    ;Start exit sequence.
exit_azam:
				    ;Close windows.
wdelete, aa0.wina, aa0.winb, aa0.winp

				    ;Save results.
if aa0.index ne -1 then  azam_azam, aa0.dty, azam_a_azm(aa0), azam=aa0
if aa1.index ne -1 then  azam_azam, aa1.dty, azam_a_azm(aa1), azam=aa1

				    ;Restore system variables.
set_plot, sav_n
!p = sav_p
!x = sav_x
!y = sav_y
!z = sav_z
!order = sav_o

end
