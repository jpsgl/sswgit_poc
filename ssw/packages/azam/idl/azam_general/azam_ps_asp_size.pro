pro azam_ps_asp_size_use
;+
;
;	procedure:  azam_ps_asp_size
;
;	purpose:  for ASP Postscript image output determine sub array indices
;		  and array size in inches depending on space available
;
;	routines: azam_ps_asp_size_use  azam_ps_asp_sz  azam_ps_asp_size
;
;	author:  paul@ncar, 4/95	(minor mod's by rob@ncar)
;
;==============================================================================
;
if 1 then begin
	print
	print, "usage:	azam_ps_asp_size, c"
	print
	print, "	For ASP Postscript image output determine sub array"
	print, "	indices and array size in inches depending on space"
	print, "	available."
	print
	print, "	Arguments"
	print, "		c	- Input ASP data set structure with"
	print, "			  zenith or disk reference frame."
	print, "		rframe	- input reference frame"
	print, "				(zenith or instrument)."
	print, "	Keywords"
	print, "	     origin_xy	- input two vector:"
	print, "				[x_origin,y_origin] position"
	print, "				in inches of available area"
	print, "				for image on page"
	print, "				(def [0.,0.])."
	print, "			  output two vector:"
	print, "				Origin adjusted to center on"
	print, "				space available."
	print, "	  hold_origin	- set to disable adjusting origin_xy"
	print, "	     inches_xy	- input two vector:"
	print, "				[x_inches,y_inches] size in"
	print, "				inches available for plotting"
	print, "				an image (def: [8.5,11.] or"
	print, "				[11.,8.5])."
	print, "			  output two vector:"
	print, "				[x_inches,y_inches] size in"
	print, "				inches to be used for plotting"
	print, "				an image."
	print, "	   mm_per_inch	- input scaler:"
	print, "				Sun megameters per printer"
	print, "				page inch.  Ignored if"
	print, "				keyword mm_inch_std is input."
	print, "			  output scaler:"
	print, "				If undefined set scale so that"
	print, "				one dimension just fits space"
	print, "				available.  If keyword"
	print, "				mm_inch_std is input,"
	print, "				mm_per_inch will be set to"
	print, "				actual value used."
	print, "	   mm_inch_std	- input this keyword to get standard"
	print, "			  image proportions based on the one"
	print, "			  landscape image per page format."
	print, "			  Input value is used for megameters"
	print, "			  per printer page inch that comes"
	print, "			  out on the one image format."
	print, "		LLxy	- input two vector:"
	print, "				[X0,Y0] lower left index"
	print, "				for sub arrays to plot"
	print, "			  output two vector:"
	print, "				Round off if input; otherwise"
	print, "				determine appropriate to info"
	print, "				available."
	print, "		URxy	- input two vector:"
	print, "				[X1,Y1] upper right index"
	print, "				for sub arrays to plot."
	print, "				Ignored if LLxy is not input."
	print, "			  output two vector:"
	print, "				Round off if input; it may be"
	print, "				reset if space is not"
	print, "				available depending on"
	print, "				mm_per_inch scale."
	print, "	       ABOUTxy	- input two vector:"
	print, "				[XA,YA] index in images about"
	print, "				where to plot."
	print, "				Ignored if LLxy is input"
	print, "				(def: image center)."
	print
	print, "		tplate	- input template structure."
	return
endif
;-
end
;------------------------------------------------------------------------------
;
;	procedure:  azam_ps_asp_sz
;
;	purpose:  does ps_azp_size except for keyword mm_inch_std
;
;------------------------------------------------------------------------------
pro azam_ps_asp_sz, c, tpl, rframe $
, origin_xy = origin_xy $
, hold_origin = hold_origin $
, inches_xy = inches_xy $
, mm_per_inch = mm_per_inch $
, LLxy    = LLxy $
, URxy    = URxy $
, ABOUTxy = ABOUTxy 

; FOR NEW AZAM, common area with X-, Y-pixel sizes for image displays
  common psimages,delx,dely 

				    ;Input array size.
xyDim = [ c.xdim, c.ydim ]
;				    ;Pixels per megameter.

;  FOR NEW AZAM, get pixel scaling appropriate for reference frame
if rframe eq 'zenith' then pix_per_mm = c.pix_deg / c.mm_per_deg
;  FOR NEW AZAM, for Arcseconds, set pix_per_mm to that for X-dimension
if rframe eq 'instrument' then pix_per_mm = 1./delx

				    ;Size available to plot image, inches.
if n_elements(inches_xy) eq 2 $
then  avail_xy = inches_xy $
else  avail_xy = [ tpl.xdevice, tpl.ydevice ]

				    ;If megameters per inch is known,
				    ;find max size array that can be plotted.
if n_elements(mm_per_inch) eq 1 $
then  pix_avail = long( avail_xy*mm_per_inch*pix_per_mm + .001 )

				    ;Check if lower left or center coordinates
				    ;are given
case 1 of
	n_elements(LLxy) eq 2: begin

				    ;Given lower left coordinates.
		LLxy = long( 0 > LLxy )

				    ;Having lower left given set upper right
				    ;coordinates.
		case 1 of
			n_elements(URxy) eq 2 $
			and n_elements(mm_per_inch) ne 1: $
				URxy = round( URxy )

			n_elements(URxy) eq 2 $
			and n_elements(mm_per_inch) eq 1: $
				URxy = round( URxy < (LLxy+pix_avail) )

			n_elements(mm_per_inch) eq 1: $
				URxy = LLxy+pix_avail

			else:	URxy = xyDim-1
		end
		end

	n_elements(mm_per_inch) eq 1 : begin

		if n_elements(ABOUTxy) eq 2 $
		then  ABOUT=ABOUTxy $
		else  ABOUT=xyDim/2

		LLxy = round( 0 > $
		( (ABOUT-.5*pix_avail) < (xyDim-1-pix_avail)) )
		URxy = LLxy+pix_avail
		end

	else: begin
		LLxy = [ 0, 0 ]
		URxy = xyDim-1
		end
end
				    ;Do not exceed maximum dimensions.
URxy = round( URxy < (xyDim-1) )
				    ;Image size in megameters.
xymm = (URxy-LLxy)/pix_per_mm
				    ;Default megameters per inch to just fit.
if n_elements(mm_per_inch) eq 0 $
then   mm_per_inch = max( xymm / avail_xy )

				    ;Set xy size needed by plot, inches.
if rframe eq 'zenith' then inches_xy = (URxy-LLxy)/(pix_per_mm*mm_per_inch)
;  FOR NEW AZAM, multiply y-size for pixel aspect ratio
if rframe eq 'instrument' then $
		inches_xy = [1.,dely/delx]*(URxy-LLxy)/(pix_per_mm*mm_per_inch)

				    ;Set default origin.
if n_elements(origin_xy) ne 2 then  origin_xy=[0.,0.]

				    ;Set origin to center on space available.
if keyword_set(hold_origin) eq 0 $
then  origin_xy = origin_xy+.5*(avail_xy-inches_xy)

end
;------------------------------------------------------------------------------
;
;	procedure:  azam_ps_asp_size
;
;	purpose:  for ASP Postscript image output determine sub array indices
;		  and array size in inches depending on space available
;
;------------------------------------------------------------------------------
pro azam_ps_asp_size, c , rframe $
, origin_xy = origin_xy $
, hold_origin = hold_origin $
, inches_xy = inches_xy $
, mm_per_inch = mm_per_inch $
, mm_inch_std = mm_inch_std $
, LLxy    = LLxy $
, URxy    = URxy $
, ABOUTxy = ABOUTxy $
, tplate  = tpl_
				    ;Check number of parameters.
if n_params() eq 0 then begin
	azam_ps_asp_size_use
	return
end


				    ;Set template structure if not present.
if n_elements(tpl_) eq 0 $
then  ps_tplate, tpl, portrait=!d.y_size gt !d.x_size $
else  tpl = tpl_
				    ;Check if mm_inch_std keyword missing.
if n_elements(mm_inch_std) eq 0 then begin

	azam_ps_asp_sz, c, tpl, rframe $
	, origin_xy = origin_xy $
	, hold_origin = hold_origin $
	, inches_xy = inches_xy $
	, mm_per_inch = mm_per_inch $
	, LLxy    = LLxy $
	, URxy    = URxy $
	, ABOUTxy = ABOUTxy

	return
end
				    ;Size available to plot image, inches.
if n_elements(inches_xy) eq 2 $
then  avail_xy = inches_xy $
else  avail_xy = [ tpl.xdevice, tpl.ydevice ]

				    ;Standard device size & margins.
ps_tplate, std
xdev=std.xdevice  &  lft=std.lft  &  rgt=std.rgt
ydev=std.ydevice  &  btm=std.btm  &  top=std.lft

				    ;Standard image size, inches.
i_xy = [ xdev-lft-rgt, ydev-btm-top ]

				    ;Scale factor compared to standard.
factor = min(avail_xy/i_xy)

				    ;Get ASP sizes.
azam_ps_asp_sz, c, tpl, rframe $
, inches_xy = i_xy $
, mm_per_inch = mm_inch_std $
, LLxy    = LLxy $
, URxy    = URxy $
, ABOUTxy = ABOUTxy
				    ;Set megameters per inch.
mm_per_inch = mm_inch_std/factor
				    ;Set xy size needed by plot, inches.
inches_xy = factor*i_xy

				    ;Set default origin.
if n_elements(origin_xy) ne 2 then  origin_xy=[0.,0.]

				    ;Set origin to center on space available.
if keyword_set(hold_origin) eq 0 $
then  origin_xy = origin_xy+.5*(avail_xy-inches_xy)

end
