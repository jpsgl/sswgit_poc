pro azam_tools_usage
;+
;
;	procedure:  azam_tools
;
;	purpose:  compile several azam routines
;
;	author:  paul@ncar, 10/93	(minor mod's by rob@ncar)
;
;	routines:  azam_tools_usage
;		   tw                avg_azm
;		   azam_tools        azam_message
;		   azam_click_xy     azam_text_in
;		   azam_wpermit      azam_out_dir
;		   azam_image_names  azam_flipa        azam_a_azm
;		   azam_cursor       azam_a_read       azam_average
;		   azam_flick        azam_spectra
;		   azam_clevels      azam_contour      azam_mgfy
;		   azam_c_bar        azam_display      azam_image_color
;		   azam_typeroi      azam_defroi
;		   azam_tools
;
;==============================================================================
;
if 1 then begin
	print
	print, "usage:	@azam_tools"
	print
	print, "	Compile several azam routines."
	print
	return
endif
;-
end
;------------------------------------------------------------------------------
;
;	procedure:  tw
;
;	purpose:  set window and do tv procedure
;
;------------------------------------------------------------------------------
pro tw, window, image, x, y
wset, window
tv, image, x, y
end
;------------------------------------------------------------------------------
;
;	procedure:  avg_azm
;
;	purpose:  return average azimuth
;
;------------------------------------------------------------------------------
function avg_azm, ar_ray
				    ;Get number or array elements.
n = n_elements(ar_ray)
if n eq 0 then return, 0.
if n eq 1 then return, ar_ray(0)
				    ;Return average azimuth.
ang = ar_ray*(!pi/180.)
return, atan( total( sin(ang) ), total( cos(ang) ) )*(180./!pi)

end
;------------------------------------------------------------------------------
;
;	procedure:  azam_message
;
;	purpose:  print message in azam button window.
;
;------------------------------------------------------------------------------
pro azam_message, aa, message, color=color

				    ;Set to azam button window.
wset, aa.winb  &  wshow, aa.winb
				    ;Set default margin color.
if  n_elements(message) eq 0  then begin
	if  n_elements(color) eq 0  then  color=aa.black
end else begin
	if  n_elements(color) eq 0  then  color=aa.yellow
end
				    ;Erase window with color index.
erase, color
				    ;Check if there is a message.
nmess = n_elements(message)
if nmess ne 0 then begin

				    ;Set character color.
	if  color eq aa.black  then  c_color=aa.white  else  c_color=aa.black

				    ;Print message.
	for k=0L,nmess-1 do begin
		xyouts, 3*aa.blt, (nmess-1-k)*aa.bwd+aa.btm, message(k) $
		, /device, align=0.5, charsize=aa.csiz, color=c_color
	end
end
				    ;Wait for no mouse button pressed.
repeat  cursor, xx, yy, /device, /nowait  until  !err eq 0

end
;-----------------------------------------------------------------------------
;
;	procedure:  azam_text_in
;
;	purpose:  open text widget and input character string
;
;-----------------------------------------------------------------------------
function azam_text_in, aa, title
				    ;Set default prompt.
if n_elements(title) eq 0 then title='Enter character string'

				    ;Open base widget.
base = widget_base( title=title )
				    ;Open text widget.
text = widget_text( base, /editable, ysize=1, xsize=90, font='10x20' )

				    ;Realize the widget.
widget_control, base, /realize
				    ;Wait for event.
event = widget_event( text )
				    ;Get character string.
widget_control, text, get_value=buff

				    ;Destroy the widget.
widget_control, base, /destroy
				    ;Erase user prompt.
azam_message, aa
				    ;Return character string.
return, buff(0)

end
;------------------------------------------------------------------------------
;
;	function:  azam_wpermit
;
;	purpose:  determine if directory is write permited
;
;------------------------------------------------------------------------------
function azam_wpermit, dir, err_string

				    ;Path to scratch file.
if dir ne '' then  file=dir+'/junk_bunk_crap'  else  file='junk_bunk_crap'

				    ;Enable i/o error return.
on_ioerror, ioerror
				    ;Open scratch file.
openw, /get_lun, unit, file, /delete
free_lun, unit
				    ;Normal return.
err_string = ''
return, 1
				    ;Abnormal return.
ioerror:
err_string = !err_string
return, 0

end
;------------------------------------------------------------------------------
;
;	function:  azam_out_dir
;
;	purpose:  prompt user for output directory path
;
;------------------------------------------------------------------------------
function azam_out_dir, aa, o_dir
				    ;Try repeatedly.
while 1 do begin
				    ;Last 25 characters of last widget entry.
lth = strlen(aa.ps_dir)
ps_lbl = strmid( aa.ps_dir, 0 > (lth-26), 0 > ( (lth-1) < 25 ) )
if ps_lbl eq '' then  ps_lbl='cwd'
				    ;Set labels for pop_cult.
				    ;Prompt user for PostScript file
				    ;name extension.
lbs = [ 'a_* file directory', 'current directory' $
, 'tilde home directory', '/d disk directory', ps_lbl $
, '^y** DISMISS **', '** DEFAULT **','** HELP **', '', 'WIDGET ENTRY' ]

				    ;Check write permits.
if azam_wpermit(aa.dty) $
then  lbs([0,6]) = '^g'+lbs([0,6]) $
else  lbs([0,6]) = '^x'+lbs([0,6])

if azam_wpermit(''  )      eq 0 then  lbs(1) = '^x'+lbs(1)
if azam_wpermit('~' )      eq 0 then  lbs(2) = '^x'+lbs(2)
if azam_wpermit('/d')      eq 0 then  lbs(3) = '^x'+lbs(3)
if azam_wpermit(aa.ps_dir) eq 0 then  lbs(4) = '^x'+lbs(4)

ext = pop_cult( lbs, /string, winfree=aa.winfree $
, title='click output directory path' $
, help='azam', arg0='window', arg1=aa, arg2= $
["a_* file directory   - output postScript files to a_* directory"$
,"current directory    - output postScript files to current directory"$
,"tilde home directory - output postScript files to user home directory"$
,"/d disk directory    - output postScript files to user /d disk"$
,             ps_lbl+" - same output directory as last widget entry"$
,"   WIDGET ENTRY      - user types directory path in pop up widget"$
,"  ** DISMISS **      - give up on this"$
,"  ** DEFAULT **      - same as clicking other green button"$
,"    ** HELP **       - print this information"$
] )
				    ;Select button.
case 1 of
				    ;Abnormal return.
ext eq '** DISMISS **': return, 0
				    ;a_* file directory.
ext eq 'a_* file directory' or $
ext eq '** DEFAULT **':        o_dir = aa.dty

				    ;Current directory.
ext eq 'current directory':    o_dir = ''

				    ;Home directory.
ext eq 'tilde home directory': o_dir = '~/'

				    ;/d disk directory.
ext eq '/d disk directory':    o_dir = '/d/'

				    ;Last widget entry.
ext eq ps_lbl:                 o_dir = aa.ps_dir

				    ;Prompt for widget entry.
else: begin
	o_dir = strcompress( /remove_all $
	, azam_text_in( aa, 'Enter directory path' ) )
	if o_dir ne '' $
	then  if strmid(o_dir,strlen(o_dir)-1,1) ne '/' then  o_dir=o_dir+'/'
	aa.ps_dir = o_dir
	end
end
				    ;Check write permit for normal return.
if azam_wpermit(o_dir,err_string) then  return, 1

				    ;Print error message & try agian.
print, err_string
end

end
;------------------------------------------------------------------------------
;
;	procedure:  azam_click_xy
;
;	purpose:  prompt for and return azam image cooridnate click
;
;------------------------------------------------------------------------------
pro azam_click_xy, aa, message, x, y, w3, img3, state=state

				    ;Print message in button window.
azam_message, aa, message
				    ;Loop till mouse buttons up then down.
for clk=0,1 do begin
	repeat azam_cursor, aa, xerase, yerase $
		, aa.ww(0).win, aa.ww(1).win, aa.ww(2).win,      w3, undef $
		,                                              img3, undef $
		,      eraser0,      eraser1,      eraser2, eraser3, undef $
		, x, y, state $
		, likely=likely $
		, maybe=w3 $
		, no_ttack=no_ttack $
		, delay=.2 $
	until ((state > aa.bs) < 1) eq clk
end
				    ;Restore windows.
if xerase ge 0 then begin
	tw, aa.ww(0).win, eraser0, xerase, yerase
	tw, aa.ww(1).win, eraser1, xerase, yerase
	tw, aa.ww(2).win, eraser2, xerase, yerase
	if n_elements(w3) then  tw, w3, eraser3, xerase, yerase
end
				    ;Wait up mouse buttons.
wset, likely
repeat  cursor, x0,y0, /device, /nowait  until  (!err eq 0)  or  (x0 eq -1)

end
;------------------------------------------------------------------------------
;
;	function: azam_image_names
;
;	purpose:  return string array with recognized azam image names
;		  for pop_cult.pro prompts.
;
;------------------------------------------------------------------------------
function azam_image_names, aa, protect=protect_

				    ;On keyword protect most parameters.
protect = ([ '', '^x' ])(keyword_set(protect_))

				    ;Return string array.
return, $
[ protect+aa.pp(aa.cct).name $
, protect+aa.pp(aa.fld).name $
, protect+aa.pp(aa.flx).name $
, protect+aa.pp(aa.jz).name $
, protect+aa.pp(aa.alpha).name $
, protect+aa.pp(aa.cen1).name $
, '' $
, protect+aa.pp(aa.azm).name $
, '' $
, protect+aa.pp(aa.azz).name $
, protect+aa.pp(aa.azz_amb).name $
, protect+aa.pp(aa.azz_orig).name $
, protect+aa.pp(aa.azz_ref).name $
, '' $
, aa.pp(aa.extra0).name $

, '^g** DISMISS **' $
, '** HELP **' $
, '' $
, '' $
, '' $
, '' $
, '' $
, protect+aa.pp(aa.psi).name $
, '' $
, protect+aa.pp(aa.zen_ang).name $
, protect+aa.pp(aa.zen_amb).name $
, protect+aa.pp(aa.zen_orig).name $
, protect+aa.pp(aa.zen_ref).name $
, '' $
, aa.pp(aa.extra1).name $
]

end
;------------------------------------------------------------------------------
;
;	procedure:  azam_flipa
;
;	purpose:  flip azimuth in azam structure based on where array. 
;
;------------------------------------------------------------------------------
pro azam_flipa, aa, chg
				    ;Check for null where array.
if chg(0) eq -1 then return
				    ;Comparison array for original & reference.
aa.rslv(chg) = (aa.rslv(chg)+1) and aa.sdat(chg)

				    ;Flip data in 2D arrays.
tmp = aa.pp(0:4).value(chg)
aa.pp(0:4).value(chg) = aa.pp(aa.azm:aa.azm+4).value(chg)
aa.pp(aa.azm:aa.azm+4).value(chg) = tmp

				    ;Flip colors in 2D images.
tmp = aa.pp(0:4).img(chg)
aa.pp(0:4).img(chg) = aa.pp(aa.azm:aa.azm+4).img(chg)
aa.pp(aa.azm:aa.azm+4).img(chg) = tmp

				     ;Flag azam_image.pro renew highlights.
aa.rehi = 1L

end
;------------------------------------------------------------------------------
;
;	function:  azam_a_azm
;
;	purpose:  return current azimuth solution as a stream vector
;
;------------------------------------------------------------------------------
function azam_a_azm, aa
				    ;Check if working with stretched images.
if  aa.stretch  then begin
				    ;Get copy of original azimuth stream
				    ;vector from directory.
				    ;This is to assures that all solved
				    ;data points are present.
	tmp = read_floats(aa.dty+'a_azm')

				    ;Set current azimuth resolution.
	tmp(aa.vec_sxy) = aa.pp(aa.azm).value(aa.sxy)

				    ;Return stream vector of current azimuth.
	return, tmp

end else begin
				    ;Return stream vector of current azimuth.
	return, aa.pp(aa.azm).value(aa.sxy)
end

end
;------------------------------------------------------------------------------
;
;	procedure:  azam_cursor
;
;	purpose:  do most details of azam.pro computed cursor.
;
;------------------------------------------------------------------------------
pro azam_cursor, aa, xerase, yerase $
,      w0,      w1,      w2,      w3,      w4 $
,                               img3,    img4 $
, eraser0, eraser1, eraser2, eraser3, eraser4 $
, xc, yc, state $
, likely=likely $
, maybe=maybe_ $
, no_ttack=no_ttack $
, delay=delay $
, xcyc = xcyc
				    ;Often used parameters.
t     = aa.t
xdim  = aa.xdim
ydim  = aa.ydim
txdim = t*xdim
tydim = t*ydim
				    ;Offset of computed cursor.
c_off = 28L
				    ;Initialize undefined variables.
if n_elements(likely) eq 0 then begin
	xc       = -1L
	yc       = -1L
	xerase   = -1L
	yerase   = -1L
	state    =  0L
	no_ttack =  0L
	likely   = aa.ww(0).win
end
				    ;Initialize interactive button window
				    ;status as being off window.
aa.bx = -1L
aa.by = -1L
aa.bs =  0L
				    ;Set images cursor may be on.
maybe = [ aa.ww(*).win, aa.mgfy.win, aa.winb ]
if n_elements(maybe_) ne 0 then   maybe = [ maybe, maybe_ ]

				    ;Get mouse status from possible images.
start = where( likely eq maybe )
ntry = n_elements(maybe)
i = 0
repeat begin
	likely = maybe((i+start(0)) mod ntry)
	wset, likely   &  cursor, xxxx, yyyy, /device, /nowait
	i = i+1
end until  (i eq ntry) or (xxxx ne -1)

				    ;Check if on button window or no window.
if  n_elements(xcyc) ne 2  and  ( xxxx eq -1 or likely eq aa.winb )  then begin

				    ;Mouse status on button window.
	if xxxx ge 0 and likely eq aa.winb then begin
		aa.bx = xxxx
		aa.by = yyyy
		aa.bs = !err
	end
				    ;Erase old crusors and ascii window.
	wset, aa.wina  &  erase, aa.white
	if xerase ge 0 then begin
		tw, w0, eraser0, xerase, yerase
		tw, w1, eraser1, xerase, yerase
		tw, w2, eraser2, xerase, yerase
		if n_elements(w3) then  tw, w3, eraser3, xerase, yerase
		if n_elements(w4) then  tw, w4, eraser4, xerase, yerase
	end
				    ;Null returns for cursor not on images.
	xerase = -1L
	xc     = -1L
	yc     = -1L
	state  =  0L
				    ;Delay on keyword if cursor off windows.
	if xxxx lt 0 then  if n_elements(delay) ne 0 then  wait, delay

	return
end
				    ;Check if cursor on magnified continuum.
if  likely eq aa.mgfy.win  and  xxxx ne -1  then begin
	xxxx = (xxxx/aa.mgfy.mag) < aa.xdim
	yyyy = (yyyy/aa.mgfy.mag) < aa.ydim
	xxxx = (xxxx+aa.mgfy.x0)*aa.t+c_off
	yyyy = (yyyy+aa.mgfy.y0)*aa.t
end
				    ;Get mouse button state.
state = !err
				    ;Get unmagnified coordinates.
xi = 0 > ((xxxx-c_off)/t) < (xdim-1)
yi = 0 >          (yyyy/t) < (ydim-1)

				    ;Check for coordinates from keyword.
if n_elements(xcyc) eq 2 then begin
	xi = xcyc(0)
	yi = xcyc(1)
	state = 0L
end
				    ;Check if position has changed.
if xi eq xc and yi eq yc then return

				    ;Set new coordinates.
xc = xi
yc = yi
				    ;Return if displays not changed.
if no_ttack then return
				    ;Save cursor informtion common to 
				    ;entire run.
common azam_cursor_com, cradius, cdim, xcirc, ycirc, xspin
if  n_elements(cradius) eq 0  then begin
	cradius = 12
	cdim    = 2*cradius+1
	circum  = round(2.*!pi*(cradius-1))
	tmp     = indgen(circum)*2.*!pi/circum
	ycirc   = (cradius-1)*sin(tmp)
	xcirc   = (cradius-1)*cos(tmp)
	xspin   = lindgen(cradius-2)+1.
end
				    ;Set bounds.
xcenter = t*xc+t/2
ycenter = t*yc+t/2
xx0 = 0 > (xcenter-cradius)
yy0 = 0 > (ycenter-cradius)
xx1 = (xcenter+cradius) < (txdim-1)
yy1 = (ycenter+cradius) < (tydim-1)
				    ;Blank cursor image without valid colors.
pad = replicate( -1, cdim, cdim )

if  aa.sdat(xc,yc) then begin
				    ;Compute where arrays for tack.
	agl  = ( aa.pp(aa.azz).value(xc,yc)-aa.angxloc )*!pi/180.
	snn  = sin(agl)
	csn  = cos(agl)

	xrast = xcirc*cos(aa.pp(aa.zen_ang).value(xc,yc)*!pi/180.)
	xprim = round( csn*xrast-snn*ycirc + cradius )
	yprim = round( snn*xrast+csn*ycirc + cradius )
	gyro  = yprim*cdim+xprim

	xprim = round( csn*xspin + cradius )
	yprim = round( snn*xspin + cradius )
	spdl  = yprim*cdim+xprim
				    ;Check for local frame reversal.
	if  aa.pp(aa.zen_ang).value(xc,yc) le 90. then begin

				    ;Tack and outline colors.
		colr = aa.yellow
		outl = aa.red
				    ;Set tack head colors.
		pad(gyro+1     ) = outl
		pad(gyro-1     ) = outl
		pad(gyro+cdim  ) = outl
		pad(gyro+cdim+1) = outl
		pad(gyro+cdim-1) = outl
		pad(gyro-cdim  ) = outl
		pad(gyro-cdim+1) = outl
		pad(gyro-cdim-1) = outl
		pad(gyro       ) = colr

				    ;Set tack spindle colors.
		pad(spdl+1     ) = outl
		pad(spdl-1     ) = outl
		pad(spdl+cdim  ) = outl
		pad(spdl+cdim+1) = outl
		pad(spdl+cdim-1) = outl
		pad(spdl-cdim  ) = outl
		pad(spdl-cdim+1) = outl
		pad(spdl-cdim-1) = outl
		pad(spdl       ) = colr

	end else begin
				    ;Tack and outline colors.
		colr = aa.white
		outl = aa.black 
				    ;Set tack spindle colors.
		pad(spdl+1     ) = outl
		pad(spdl-1     ) = outl
		pad(spdl+cdim  ) = outl
		pad(spdl+cdim+1) = outl
		pad(spdl+cdim-1) = outl
		pad(spdl-cdim  ) = outl
		pad(spdl-cdim+1) = outl
		pad(spdl-cdim-1) = outl
		pad(spdl       ) = colr

				    ;Set tack head colors.
		pad(gyro+1     ) = outl
		pad(gyro-1     ) = outl
		pad(gyro+cdim  ) = outl
		pad(gyro+cdim+1) = outl
		pad(gyro+cdim-1) = outl
		pad(gyro-cdim  ) = outl
		pad(gyro-cdim+1) = outl
		pad(gyro-cdim-1) = outl
		pad(gyro       ) = colr
	end

end else begin
				    ;Cursor is not on a data point.
				    ;Set cross with outline.
	pad(*,cradius-1:cradius+1) = aa.black
	pad(cradius-1:cradius+1,*) = aa.black
	pad(*,cradius) = aa.white
	pad(cradius,*) = aa.white
end
				    ;Set center to image color.
tt = t < 3
xy0 = cradius-tt/2
xy1 = xy0+tt-1
pad(xy0:xy1,xy0:xy1) = -1
				    ;Truncate due to array bounds.
pad = pad( cradius+xx0-xcenter:cradius+xx1-xcenter $
         , cradius+yy0-ycenter:cradius+yy1-ycenter )

				    ;Translate cursor to where array form.
whrcrs = where( pad ge 0 )
whrcrs(0) = 0 > whrcrs(0) 
crs = pad( whrcrs )
				    ;Erase old crusors.
if xerase ge 0 then begin
	tw, w0, eraser0, xerase, yerase
	tw, w1, eraser1, xerase, yerase
	tw, w2, eraser2, xerase, yerase
	if n_elements(w3) then  tw, w3, eraser3, xerase, yerase
	if n_elements(w4) then  tw, w4, eraser4, xerase, yerase
end
				    ;Update cursor coordinates.
xerase  = xx0
yerase  = yy0
				    ;Plot cursor in window 0.
eraser0 = aa.ww(0).img(xx0:xx1,yy0:yy1)
tmp = eraser0
tmp( whrcrs ) = crs
tw, w0, tmp, xerase, yerase
				    ;Plot cursor in window 1.
eraser1 = aa.ww(1).img(xx0:xx1,yy0:yy1)
tmp = eraser1
tmp( whrcrs ) = crs
tw, w1, tmp, xerase, yerase
				    ;Plot cursor in window 2.
eraser2 = aa.ww(2).img(xx0:xx1,yy0:yy1)
tmp = eraser2
tmp( whrcrs ) = crs
tw, w2, tmp, xerase, yerase
				    ;Plot cursor in window 3.
if n_elements(w3) then begin
	eraser3 = img3(xx0:xx1,yy0:yy1)
	tmp = eraser3
	tmp( whrcrs ) = crs
	tw, w3, tmp, xerase, yerase
end
				    ;Plot cursor in window 4.
if n_elements(w4) then begin
	eraser4 = img4(xx0:xx1,yy0:yy1)
	tmp = eraser4
	tmp( whrcrs ) = crs
	tw, w4, tmp, xerase, yerase
end
				    ;Print data near cursor.
azam_average, aa, xc,yc, xc,yc, xc,yc

				    ;Set window where cursor is at.
wset, likely

end
;------------------------------------------------------------------------------
;
;	procedure:  azam_a_read
;
;	purpose:  read a_* file and install as extra0 or extra1 parameter.
;
;------------------------------------------------------------------------------
pro azam_a_read, aa, hh
				   ;Names of some a_* files.
file_names = $
[ '^la_1azm' $
, '^la_1incl' $
, '^la_2azm' $
, '^la_2incl' $
, '^la__cct' $
, '^la__dclntn' $
, '^la__latitude' $
, '^la__longitude' $
, '^la__mu' $
, '^la__pip' $
, '^la__pvpv' $
, '^la__rgtasn' $
, '^la__utime' $
, '^la_alpha' $
, '^la_alpha_er' $
, '^la_azm' $
, '^la_azm_er' $
, '^la_b1mu' $
, '^la_b1mu_er' $
, '^la_bzero' $
, '^la_bzero_er' $
, '^la_cen1' $
, '^la_cen1_er' $
, '^la_cen2' $
, '^la_cen2_er' $

, '^g** DISMISS **' $
, '** HELP **' $
, '' $
, 'WIDGET ENTRY' $
, '' $
, '^la_chi' $
, '^la_chi_ii' $
, '^la_chi_qq' $
, '^la_chi_uu' $
, '^la_chi_vv' $
, '^la_delta' $
, '^la_delta_er' $
, '^la_dmp' $
, '^la_dmp_er' $
, '^la_dop' $
, '^la_dop_er' $
, '^la_eta0' $
, '^la_eta0_er' $
, '^la_fld' $
, '^la_fld_er' $
, '^la_icmag' $
, '^la_iters' $
, '^la_psi' $
, '^la_psi_er' $
, '^la_vint' ]
				    ;Help message for file name input.
file_help = $
["   a_*        - read already named a_* fileg"$
,"WIDGET ENTRY  - user enters path of file to be read in a widget"$
,"                that pops up"$
,"** DISMISS ** - return to interactive azam" $
," ** HELP **   - print this information"$
]
				   ;Help info for widget entry file name.
wdgt_help =$
["Is file in directory with a_* files?:"$
,""$
,"      yes       - this is the usual case where the data file to be read"$
,"                  is in the same directory as the a_* files."$
,"      no        - click 'no' if the file to be read resides somewhere other"$
,"                  than the directory with the a_* files."$
," ** DEFAULT **  - same as pressing other green button"$
,"  ** HELP **    - print this information"$
]
				    ;Loop for valid file entry or dismiss.
error = 1
while error ne 0 do begin
				    ;Prompt for file name.
	error = 0
	a_name = pop_cult( /string, winfree=aa.winfree $
	, title='Click on file name' $
	, file_names $
	, help='azam', arg0='window', arg1=aa, arg2=file_help $
	)
	if a_name eq '** DISMISS **' then return

				    ;Enter file name from widget.
	cdty = aa.dty
	if a_name eq 'WIDGET ENTRY' then begin

		clk = pop_cult( /string, winfree=aa.winfree $
		, title = 'Is file in directory with a_* files?' $
		,['^gyes','no','^g** DEFAULT **','** HELP **']$
		, help='azam', arg0='window', arg1=aa $
		, arg2=wdgt_help $
		)

		if clk eq '** DEFAULT **' $
		or clk eq 'yes' then begin
			a_name = azam_text_in( aa, 'Enter file name (q=quit)' )
			if a_name eq 'q' then return
		end else begin
				    ;Read full file path.
			a_file = azam_text_in( aa, 'Enter file path (q=quit)' )
			if a_file eq 'q' then return

				    ;Isolate directory path.
			jj = 0
			kk = 0
			while  kk ne -1  do begin
				kk = strpos( a_file, '/', kk )
				if  kk ne -1  then  begin
					kk = kk+1
					jj = kk
				end
			end
				    ;Seperate directory and name.
			cdty = strmid( a_file, 0, jj )
			a_name = strmid( a_file, jj, 1000 )
		end
	end
				    ;Try to read file.
	tmp = read_floats( cdty+a_name, error )

	if error ne 0 then begin
		azam_click_xy, aa $
		, [ a_name+': read error','click to continue']
	end else begin
				    ;Check for correct number of points.
		npnts = n_elements(tmp)
		if  npnts ne aa.npoints $
		and npnts ne aa.nsolved then begin
			azam_click_xy, aa $
			, [ a_name+': file is wrong size','click to continue']
			error = 1
		end
	end
end
				    ;Get parameter to overwrite.
iname = pop_cult( /string, azam_image_names(aa,/protect) $
, title='click parameter to overwrite' $
, help='azam', arg0='names', arg1=aa, winfree=aa.winfree )
if iname eq '** DISMISS **' then return

				    ;Select parameter to overwrite
if iname eq aa.pp(aa.extra0).name then  iprm=aa.extra0  else  iprm=aa.extra1

				    ;Read 2D data array.
aa.pp(iprm).value = s_image( cdty+a_name, aa )

aa.pp(iprm).name = a_name
aa.pp(iprm).a_np = npnts
if npnts eq aa.npoints $
then  aa.pp(iprm).color.min = min( aa.pp(iprm).value(aa.pxy), max=mx ) $
else  aa.pp(iprm).color.min = min( aa.pp(iprm).value(aa.sxy), max=mx )
aa.pp(iprm).color.max    = mx
aa.pp(iprm).color.gray   = 1L
aa.pp(iprm).color.wrap   = 0L
aa.pp(iprm).color.invert = 0L
aa.pp(iprm).color.back   = aa.white
aa.pp(iprm).bar = 'bar'
				    ;Set a__pip same as azam_op.pro.
if a_name eq 'a__pip' then begin
	aa.pp(iprm).color.min    = 0.
	aa.pp(iprm).color.max    = 12.
	aa.pp(iprm).color.invert = 1L
	aa.pp(iprm).color.gray   = 0L
	aa.pp(iprm).color.back   = aa.white
end
				    ;Set original color.
aa.pp(iprm).color0 = aa.pp(iprm).color

				    ;Compute parameter image array.
azam_op_image, aa, iprm

end
;------------------------------------------------------------------------------
;
;	procedure:  azam_average
;
;	purpose:  average and print data near cursor.
;
;------------------------------------------------------------------------------
pro azam_average, aa, xc,yc, x0,y0, x1,y1


;  common area for usage in azam_spectra.pro
;  included here to allow printing of original coordinates of full map
;  instead of relative coordinates
common com_azamspectra, xllo,xuro,yllo,yuro,full_directory, $
	iwstrt,iwend,dispers,wavlngth,basewav,etaratio,vbroad, $
	maxsz,ntran,mscatt


				    ;Erase ascii pixmap window.
wset, aa.winp
erase, aa.white
				    ;Print coordinates.
xrp = aa.xrpnt(xc,yc)
yrp = aa.yrpnt(xc,yc)
if xrp ge 0 then begin
	
	megmx = string( (aa.b__e_w(xc,yc)-aa.mn_e_w)*aa.mm_per_deg $
	, format='(f9.2)' )
	megmy = string( (aa.b__lat(xc,yc)-aa.mn_lat)*aa.mm_per_deg $
	, format='(f9.2)' )
	xyouts, 4, aa.btm $
	, strcompress( /remove_all $
	, string('(',xc+aa.x0,',', yc+aa.y0,')(' $
	, xllo+xrp,',',yllo+yrp,')(' $
	, megmx,',',megmy,')Mm') )$
	, align=0.0, color=aa.black, /device, charsize=aa.csiz
end else begin
	xyouts, 4, aa.btm $
	, strcompress( /remove_all $
	, string('(',xc+aa.x0,',', yc+aa.y0,')') ) $
	, align=0.0, color=aa.black, /device, charsize=aa.csiz
end
				    ;Get where array for continuum.
wct = where( aa.pp(aa.cct).value(x0:x1,y0:y1) ne 0., nct )

				    ;Check if there is data.
if nct gt 0 then begin
				    ;Convert where array to full image.
	xdm = x1-x0+1
	yyy = wct/xdm
	xxx = wct-yyy*xdm
	wct = (y0+yyy)*aa.xdim+(x0+xxx)

				    ;Average continuum
	cct = total(aa.pp(aa.cct).value(wct))/nct

				    ;Print continuum.
	xyouts $
	, [ 1, 2]*aa.blt $
	, 12*aa.bwd+aa.btm $
	, strcompress( /remove_all, $
	[ 'continuum', string(cct,fo='(f12.1)') ] ) $
	, align=1.0, color=aa.black, /device, charsize=aa.csiz

				    ;Print number data points.
	if nct ne 1 then $
	xyouts $
	, 2*aa.blt+10 $
	, 12*aa.bwd+aa.btm $
	, strcompress( string(nct), /remove_all )+' points' $
	, align=0.0, color=aa.black, /device, charsize=aa.csiz

				    ;Print extra0 extra1 image averages.
	for ixtr=0,1 do begin
		iprm = ([ aa.extra1, aa.extra0 ])(ixtr)
		if aa.pp(iprm).a_np eq aa.npoints then begin
			cst = total(aa.pp(iprm).value(wct))/nct
			xyouts, [1,2]*aa.blt , (7+ixtr)*aa.bwd+aa.btm $
			, strcompress( /remove_all $
			, [  aa.pp(iprm).name, string(cst) ] ) $
			, align=1.0, color=aa.black, /device, charsize=aa.csiz
		end
	end
				    ;Where array for solved points.
	wdt = where( aa.sdat(x0:x1,y0:y1), ndt )

				    ;Check if there is solved data.
	if ndt gt 0 then begin
				    ;Convert where array to full image.
		yyy = wdt/xdm
		xxx = wdt-yyy*xdm
		wdt = (y0+yyy)*aa.xdim+(x0+xxx)

				    ;Averages.
		azm = avg_azm(aa.pp(aa.azm    ).value(wdt))
		aza = avg_azm(aa.pp(aa.azm_amb).value(wdt))
		az1 = avg_azm(aa.pp(aa.azz    ).value(wdt))
		az2 = avg_azm(aa.pp(aa.azz_amb).value(wdt))
		psi = total(  aa.pp(aa.psi    ).value(wdt))/ndt
		in1 = total(  aa.pp(aa.zen_ang).value(wdt))/ndt
		in2 = total(  aa.pp(aa.zen_amb).value(wdt))/ndt
		cn1 = total(  aa.pp(aa.cen1   ).value(wdt))/ndt
		alp = total(  aa.pp(aa.alpha  ).value(wdt))/ndt
		fld = total(  aa.pp(aa.fld    ).value(wdt))/ndt
		flx = total(  aa.pp(aa.flx    ).value(wdt))/ndt
		fxa = total(  aa.pp(aa.flx_amb).value(wdt))/ndt
		jz  = total(  aa.pp(aa.jz     ).value(wdt))/ndt
		jza = total(  aa.pp(aa.jz_amb ).value(wdt))/ndt

				    ;Print info in window.
		xyouts $
		, [ 1, 2, 1, 2,1,2,3,1,2,3,1,2,3,1,2,3,1,2,1,2,3,1,2,3]*aa.blt $
		, [11,11,10,10,9,9,7,6,6,6,5,5,5,4,4,4,3,3,2,2,2,1,1,1]*aa.bwd $
		+ aa.btm $
		, strcompress( /remove_all, $
		[          'field',string(fld,fo='(f12.1)') $
		,        'Doppler',string(cn1,fo='(f12.4)') $
		,         '1-fill',string(alp,fo='(f12.4)') $
		,                  'ambig'                  $
		,           'flux',string(flx,fo='(f12.1)') $
		,		   string(fxa,fo='(f12.1)') $
		,             'Jz',string( jz,fo='(f12.1)') $
		,		   string(jza,fo='(f12.1)') $
		,        'azimuth',string(azm,fo='(f12.2)') $
		,		   string(aza,fo='(f12.2)') $
		,        'incline',string(psi,fo='(f12.2)') $
		, 'zenith_azimuth',string(az1,fo='(f12.2)') $
		,		   string(az2,fo='(f12.2)') $
		,   'zenith_angle',string(in1,fo='(f12.2)') $
		,		   string(in2,fo='(f12.2)') $
		] ) $
		, align=1.0, color=aa.black, /device, charsize=aa.csiz

				    ;Print number data points.
		if ndt ne 1 then $
		xyouts $
		, 2*aa.blt+10 $
		, 9*aa.bwd+aa.btm $
		, strcompress( string(ndt), /remove_all )+' points' $
		, align=0.0, color=aa.black, /device, charsize=aa.csiz

				    ;Print extra0 extra1 image averages.
		for ixtr=0,1 do begin
			iprm = ([ aa.extra1, aa.extra0 ])(ixtr)
			if aa.pp(iprm).a_np ne aa.npoints then begin
				cst = total(aa.pp(iprm).value(wdt))/ndt
				xyouts, [1,2]*aa.blt , (7+ixtr)*aa.bwd+aa.btm $
				, strcompress( /remove_all $
				, [  aa.pp(iprm).name, string(cst) ] ) $
				, align=1.0, /device, charsize=aa.csiz $
				, color=aa.black
			end
		end
	end
end
				    ;Copy pixmap to screen.
wset, aa.wina  &  device, copy=[0,0,3*aa.blt+10,13*aa.bwd,0,0,aa.winp]

end
;------------------------------------------------------------------------------
;
;	procedure:  azam_flick
;
;	purpose:  blink an image on azam display images
;
;------------------------------------------------------------------------------
pro azam_flick, aa, hh, sub=sub

				     ;Popular variables.
t     = aa.t
xdim  = aa.xdim
ydim  = aa.ydim
txdim = t*xdim
tydim = t*ydim
win0 = aa.ww(0).win
win1 = aa.ww(1).win
win2 = aa.ww(2).win
				    ;Sub array dimension.
if keyword_set(sub) then   xsub=180  else  xsub=txdim > tydim

				    ;Get image name.
iname = pop_cult( /string, azam_image_names(aa), title='click on one' $
, help='azam', arg0='names', arg1=aa, winfree=aa.winfree )
if iname eq '** DISMISS **' then return

				    ;Set hidden pixmap windows.
img3 = azam_image( iname, aa, hh )
window, /free, /pixmap, xsize=txdim, ysize=tydim
w3 = !d.window
tv, img3, 0, 0

window, /free, /pixmap, xsize=txdim, ysize=tydim
w4 = !d.window
tv, aa.ww(0).img, 0, 0

window, /free, /pixmap, xsize=txdim, ysize=tydim
w5 = !d.window
tv, aa.ww(1).img, 0, 0

window, /free, /pixmap, xsize=txdim, ysize=tydim
w6 = !d.window
tv, aa.ww(2).img, 0, 0
				    ;Print instructions.
azam_message, aa, 'left(faster) middle(slower) right(stop)'

				    ;Flink images.
rate = 2.25
faster = 0
x0 = 0
y0 = 0
while 1 do begin
				    ;Loop for time interval.
	t0 = systime(1)
	while  systime(1)-t0 lt 1./rate  do begin

				    ;Do computed field cursors.
		azam_cursor, aa, xerase, yerase $
		,      w4,      w5,      w6,      w3, undef $
		,                               img3, undef $
		, eraser0, eraser1, eraser2, eraser3, undef $
		, xc, yc, state $
		, likely=likely $
		, no_ttack=no_ttack $
		, delay=1./rate
				    ;Process mouse button state.
		case  state > aa.bs  of
			0: faster = faster mod 8
			1: if faster eq 0 then  faster = 1
			2: if faster eq 0 then  faster = 2
			4: goto, break0
			else:
		end
				   ;Set sub array locations.
		if xc ge 0 then begin
			x0 = 0 > ( (t*xc-90) < (txdim-xsub) )
			y0 = 0 > ( (t*yc-90) < (tydim-xsub) )
		end
		x1 = (x0+xsub-1) < (txdim-1)
		y1 = (y0+xsub-1) < (tydim-1)

				    ;Copy flick image from pixmap window.
		wset, win0  &  device, copy=[x0,y0,x1-x0+1,y1-y0+1,x0,y0,w3]
		wset, win1  &  device, copy=[x0,y0,x1-x0+1,y1-y0+1,x0,y0,w3]
		wset, win2  &  device, copy=[x0,y0,x1-x0+1,y1-y0+1,x0,y0,w3]
	end
				    ;Loop for time interval.
	t0 = systime(1)
	while  systime(1)-t0 lt 1./rate  do begin

				    ;Do computed field cursors.
		azam_cursor, aa, xerase, yerase $
		,      w4,      w5,      w6,      w3, undef $
		,                               img3, undef $
		, eraser0, eraser1, eraser2, eraser3, undef $
		, xc, yc, state $
		, likely=likely $
		, no_ttack=no_ttack $
		, delay=1./rate
				    ;Process mouse button state.
		case  state > aa.bs  of
			0: faster = faster mod 8
			1: if faster eq 0 then  faster = 1
			2: if faster eq 0 then  faster = 2
			4: goto, break0
			else:
		end
				    ;Copy flick image from pixmap window.
		wset, win0  &  device, copy=[0,0,txdim-1,tydim-1,0,0,w4]
		wset, win1  &  device, copy=[0,0,txdim-1,tydim-1,0,0,w5]
		wset, win2  &  device, copy=[0,0,txdim-1,tydim-1,0,0,w6]
	end
				    ;Adjust cycle rate.
	if faster ne 0 then begin
		if faster eq 1 then  rate = rate*1.5
		if faster eq 2 then  rate = rate/1.5
		faster = 8
	end
end
break0:
				    ;Delete extra windows.
wdelete, w4, w5, w6, w3
				    ;Restore display windows.
tw, win0, aa.ww(0).img, 0, 0
tw, win1, aa.ww(1).img, 0, 0
tw, win2, aa.ww(2).img, 0, 0

end
;------------------------------------------------------------------------------
;
;	procedure:  azam_clevels
;
;	purpose:  compute contour where arrays hi & lo for levels in
;		  highlight structure hh
;
;------------------------------------------------------------------------------
pro azam_clevels, aa, hh, hi, lo, x0, x1, y0, y1

				    ;Initial null contour where arrays.
hi = -1L
lo = -1L
				    ;Check if contours present.
if  (hh.iprm)(0) lt 0 then return

				    ;Loop through contour levels.
for ip=0L,n_elements(hh.iprm)-1 do begin

				    ;Parameter to take contour on.
	iprm = hh.iprm(ip)
				    ;Skip if parameter swapped out.
	if hh.name(ip) eq aa.pp(iprm).name then begin

				    ;Check if working on sub area.
		if n_elements(x0) then begin

				    ;Get where array for contour.
			if aa.pp(iprm).a_np eq aa.npoints then begin
				azam_cont, aa.pp(iprm).value(x0:x1,y0:y1) $
				, aa.pdat(x0:x1,y0:y1) $
				, (hh.level)(ip), aa.t, whr
			end else begin
				azam_cont, aa.pp(iprm).value(x0:x1,y0:y1) $
				, aa.sdat(x0:x1,y0:y1) $
				, (hh.level)(ip), aa.t, whr
			end
		end else begin
				    ;Get where array for contour.
			if aa.pp(iprm).a_np eq aa.npoints then begin
				azam_cont, aa.pp(iprm).value, aa.pdat $
				, (hh.level)(ip), aa.t, whr
			end else begin
				azam_cont, aa.pp(iprm).value, aa.sdat $
				, (hh.level)(ip), aa.t, whr
			end
		end
				    ;Append to hi or lo depending on
				    ;color toggle.
		if whr(0) ge 0 then begin
			if (hh.toggle)(ip) then begin
				if hi(0) ge 0 $
				then  hi = [ hi, whr ] $
				else  hi = whr
			end else begin
				if lo(0) ge 0 $
				then  lo = [ lo, whr ] $
				else  lo = whr
			end
		end
	end
end

end
;------------------------------------------------------------------------------
;
;	procedure:  azam_contour
;
;	purpose:  compute contours and install in highlight structure hh
;
;------------------------------------------------------------------------------
pro azam_contour, aa, hh, iprm, level

				    ;Set flag for defrio return.
defroi = n_params() ge 4
				    ;Prompt for data image.
slab = pop_cult( azam_image_names(aa), /string, winfree=aa.winfree $
, title='Select data array for contour levels' $
, help='azam', arg0='names', arg1=aa )
if slab eq '** DISMISS **' then return

				    ;Parameter number.
iprm = (where( slab eq aa.pp.name ))(0)

				    ;Open window if data is not displayed.
if   iprm ne aa.ww(0).iprm  and  iprm ne aa.ww(1).iprm $
and  iprm ne aa.ww(2).iprm  then begin
	if aa.winfree then begin
		window, /free, xsize=aa.t*aa.xdim+3*aa.bwd $
		, ysize=300 > aa.t*aa.ydim $
		, title=aa.pp(iprm).name
	end else begin
		window, /free, xsize=aa.t*aa.xdim+3*aa.bwd $
		, ysize=300 > aa.t*aa.ydim $
		, xpos=100, ypos=40 $
		, title=aa.pp(iprm).name
	end
	w3 = !d.window
	img3 = azam_image( iprm, aa, hh )
	tw, w3, img3, 0, 0
	azam_c_bar, w3, iprm, aa
end

				    ;Infinite loop till right button clicked.
while 1 do begin
				    ;Flag azam_image.pro to redo high lights.
	aa.rehi = 1L
				    ;Initial flag next contour not appended.
	append = 0
				    ;Index of next an last contour.
	if (hh.iprm)(0) ge 0 $
	then  next = n_elements(hh.iprm) $
	else  next = 0
	last = next-1
				    ;Messages for user.
	prior = ''
	mssg = 'left(click contour) center(append contour) right(menu/exit)'
	if last ge 0 then begin
		tmp = (hh.name)(last)+' '+stringit((hh.level)(last))
		prior='(last contour: '+tmp+')'
		mssg = [ prior, mssg ]
	end
				    ;Get (x,y) from window click.
	repeat azam_click_xy, aa, mssg, xc, yc, w3, img3, state=state $
	until  state gt 0  or  aa.bs ge 2

				    ;Erase button window.
	wset, aa.winb & erase, aa.red

				    ;Select state.
	case  state > aa.bs  of
				    ;Left button:  contour click.
	1: clvl = aa.pp(iprm).value(xc,yc)

				    ;Center button:  append contour.
	2: begin
		clvl = aa.pp(iprm).value(xc,yc)
		append = 1
		end
				    ;Right button: menu/exit.
	else: begin
				    ;Prompt with contour menu.
		x = (['','^x'])(defroi)
		bttn = $
		pop_cult( /string, winfree=aa.winfree $
		, title = 'click on choice '+ prior $
		, [ x+'clear contours', x+'toggle line color','','','','' $
		, '^g** DISMISS **', '^yEXIT CONTOUR','** HELP **' $
		, '', 'WIDGET ENTRY', x+'APPEND WIDGET ENTRY'] $
		, help='azam', arg0='window', arg1=aa, arg2= $
	       ["  clear contours    - delete all contours"$
	       ," toggle line color  - change color of last and "$
	       ,"                      subsequent contour lines"$
	       ,"   ** DISMISS **    - return to interactive contouring"$
	       ,"   EXIT CONTOUR     - return to interactive azam"$
	       ,"     ** HELP **     - print this information"$
	       ,"    WIDGET ENTRY    - replace last contour level by"$
	       ,"                      value typed in widget"$
	       ,"APPEND WIDGET ENTRY - append new contour level with"$
	       ,"                      value typed in widget"$
	       ] )
				     ;Check for appended widget entry.
		if bttn eq 'APPEND WIDGET ENTRY' then begin
			append = 1
			bttn = 'WIDGET ENTRY'
		end

		case bttn of
		'** DISMISS **': goto, break31

		'EXIT CONTOUR': begin
			if n_elements(w3) then  wdelete, w3
			return
			end

		'clear contours': begin
			hh = { hi:-1, lo:-1 $
			, iprm:-1, name:'', level:0., toggle:0L $
			, outlineroi:hh.outlineroi, arrow:hh.arrow $
			, laluz:hh.laluz }
			goto, break30
			end

		'toggle line color': begin

			if n_elements(hh.toggle) eq 1 $
			then  hh.toggle = (hh.toggle+1) mod 2 $
			else  hh.toggle(last) = (hh.toggle(last)+1) mod 2

			goto, break30
			end

				    ;Read contour from widget.
		'WIDGET ENTRY': begin
			mess = 'Enter contour '+prior+' (def: clear contour)'
			on_ioerror, ioerror0
			ioerror0:
			txt = azam_text_in(aa $
			, 'Enter contour'+prior+' (def: dismiss)' )

				    ;Default contour clear.
			if txt eq '' then  goto, break31

				    ;Read contour from text.
			clvl = 0.
			reads, txt, clvl
			end

		else: goto, break31
		end
		end
	end
				    ;Return contour level for defroi min max.
	if defroi then begin
		level = clvl
		if n_elements(w3) then  wdelete, w3
		return
	end


	if append eq 0 then begin
				    ;Set 0th contour level.
		if n_elements(hh.iprm) eq 1 then begin
			hh = { hi:-1, lo:-1 $
			, iprm:iprm, name:slab, level:clvl, toggle:1L $
			, outlineroi:hh.outlineroi, arrow:hh.arrow $
			, laluz:hh.laluz }

				    ;Replace last contour.
		end else begin
			hh.iprm( last) = iprm
			hh.name( last) = slab
			hh.level(last) = clvl
		end

	end else begin
				    ;Set 0th contour level.
		if (hh.iprm)(0) lt 0 then begin
			hh = { hi:-1, lo:-1 $
			, iprm:iprm, name:slab, level:clvl, toggle:1L $
			, outlineroi:hh.outlineroi, arrow:hh.arrow $
			, laluz:hh.laluz }

				    ;Append contour level.
		end else begin
			hh = { hi:-1, lo:-1 $
			, iprm:   [ hh.iprm,   iprm ] $
			, name:   [ hh.name,   slab ] $
			, level:  [ hh.level,  clvl ] $
			, toggle: [ hh.toggle, (hh.toggle)(last) ] $
			, outlineroi:hh.outlineroi, arrow:hh.arrow $
			, laluz:hh.laluz }
		end
	end
				    ;Display contour image.
	break30:
	if n_elements(w3) then begin
		img3 = azam_image( iprm, aa, hh )
		tw, w3, img3, 0, 0
	end
				    ;Display azam images.
	for inum=0,2 do  azam_display, inum, aa, hh

break31:
end

end
;------------------------------------------------------------------------------
;
;	procedure: azam_mgfy
;
;	prupose:  Update plot of magnified continuum in window winc.
;
;------------------------------------------------------------------------------
pro azam_mgfy, aa, xc_, yc_
				    ;Return if not 2D array.
if aa.xdim lt 2 or aa.ydim lt 2 then return

				    ;Set window to magnified continuum.
wsav = !d.window
wset, aa.mgfy.win
				    ;Continuum magnification factor.
mf = aa.mgfy.mag
				    ;Dimensions of magnified image.
dx = aa.mgfy.siz < aa.xdim
dy = aa.mgfy.siz < aa.ydim
				    ;Pixels per Gauss for arrow lengths.
if aa.mgfy.bttn eq 'short arrows' then  pix_G=.00005*mf  else  pix_G=.00015*mf

				    ;(x,y) of lower left corner of image.
if n_params() eq 3 then begin
	aa.mgfy.xc = xc_
	aa.mgfy.yc = yc_
	x0 = 0 > ( (aa.mgfy.xc-dx/2) < (aa.xdim-dx) )  &  aa.mgfy.x0 = x0
	y0 = 0 > ( (aa.mgfy.yc-dy/2) < (aa.ydim-dy) )  &  aa.mgfy.y0 = y0
end else begin
	x0 = aa.mgfy.x0
	y0 = aa.mgfy.y0
end
				    ;(x,y) of upper right corner of image.
x1 = (x0+dx-1) < (aa.xdim-1)
y1 = (y0+dy-1) < (aa.ydim-1)
				    ;Sub array of data.
fl = aa.pp(aa.fld).value(  x0:x1,y0:y1)
az = aa.pp(aa.azz).value( x0:x1,y0:y1)*(!pi/180.)
ps = aa.pp(aa.zen_ang).value(x0:x1,y0:y1)*(!pi/180.)
cc = aa.pp(aa.cct).value( x0:x1,y0:y1)
				    ;Transverse field.
lg = fl*sin(ps)
				    ;Flux.
ls = aa.pp(aa.flx).value(x0:x1,y0:y1)

				    ;Sin & cosine of field azimuth.
sa = sin(az)
ca = cos(az)  & az=0
				    ;Output underlying image.
if (dx<dy) lt aa.mgfy.siz then  erase, aa.white
tv, puff(aa.pp(aa.mgfy.iprm).img(x0:x1,y0:y1),mf), 0, 0

				    ;Contour levels.
lvls = (findgen(3000/250)+1)*250
lvls = [ -lvls, lvls, -.001, .001 ]
				    ;Select colors depending if image
				    ;is gray scale.
gray = aa.pp(aa.mgfy.iprm).color.gray
up0 = ([ aa.white, aa.yellow ])(gray)
up1 = ([ aa.white, aa.red    ])(gray)
dn0 = ([ aa.black, aa.green  ])(gray)
dn1 = ([ aa.black, aa.blue   ])(gray)

				    ;For color image plot black over
				    ;white for reversal.
if gray eq 0 then lvls=-lvls
				    ;Draw field contours.
for i = 0L, n_elements(lvls)-1L do begin
	lv = round(lvls(i))
	case 1 of
	lv gt  1000:  colr = up0
	lv lt -1000:  colr = dn0
	lv eq 0 and lvls(i) gt 0:  colr = up0
	lv eq 0 and lvls(i) lt 0:  colr = dn0
	lv gt     0:  colr = up1
	lv lt     0:  colr = dn1
	lv eq     0:  colr = up0
	end
	contour, ls, /noerase $
	, position=[.5,.5,dx-.5,dy-.5]/aa.mgfy.siz $
	, xstyle=13, ystyle=13 $
	, xrange=[ 0., dx-1. ], yrange=[ 0., dy-1. ] $
	, levels=lvls(i), c_colors=colr
end
				    ;Point out xc yc if present.
oplot, aa.mgfy.xc-x0+[-.5,.5], aa.mgfy.yc-y0+[ .0,.0], color=dn1
oplot, aa.mgfy.xc-x0+[ .0,.0], aa.mgfy.yc-y0+[-.5,.5], color=dn1

				    ;Draw arrows.
xr = x1-x0+1
yr = y1-y0+1
for j=0L,yr-1L do begin
for i=0L,xr-1L do begin
if fl(i,j) ne 0. then begin
				    ;Arrow length and point width.
	lth = lg(i,j)*pix_G
	wdh = (.15*lth) < .1
				    ;Coordinates to draw arrow.
	t0 = [ 0., lth, lth-2.*wdh, lth, lth-2.*wdh ]
	t1 = [ 0.,  0.,        wdh,  0.,       -wdh ]
	ax = ca(i,j)*t0-sa(i,j)*t1
	ay = sa(i,j)*t0+ca(i,j)*t1
				    ;Use contrasting color for arrow.
	if gray then begin
		if cc(i,j) lt $
		.5*(aa.pp(aa.cct).color.min+aa.pp(aa.cct).color.max) $
		then  clr=aa.white $
		else  clr=aa.black
	end else begin
		clr=aa.black
	end
				    ;Draw arrow shaft and point.
	oplot, ax(0:1)+i, ay(0:1)+j, color=clr
	oplot, ax(2:4)+i, ay(2:4)+j, color=clr
end
end
end
				    ;Restore active window.
if wsav ge 0 then  wset, wsav

end
;------------------------------------------------------------------------------
;
;	procedure:  azam_c_bar
;
;	purpose:  display color bar on window.
;
;------------------------------------------------------------------------------
pro azam_c_bar, win, iprm, aa
				    ;Open window.
if win lt 0 then return
wset, win
				    ;Window ysize, see azam_op.pro
ysz = 300 > (aa.t*aa.ydim)
				    ;Select color scale type.
case aa.pp(iprm).bar of
				    ;Plot color bar
'bar': begin
	x0 = aa.t*aa.xdim+aa.bwd
	y0 = ysz/2-125
	tv, replicate(aa.red,aa.bwd+2,252), x0-1, y0-1
	tvasp, /notable, lindgen(aa.bwd,250)/aa.bwd, x0, y0 $
	, gray=aa.pp(iprm).color.gray, wrap=aa.pp(iprm).color.wrap $
	, invert=aa.pp(iprm).color.invert

				    ;Print min max scale values.
	xyouts, aa.t*aa.xdim+aa.btm, y0,     stringit(aa.pp(iprm).color.min) $
	, /device, align=1.0, charsize=aa.csiz, color=aa.white, orient=270.
	xyouts, aa.t*aa.xdim+aa.btm, y0+250, stringit(aa.pp(iprm).color.max) $
	, /device, align=0.0, charsize=aa.csiz, color=aa.white, orient=270.

				    ;Print name.
	xyouts, aa.t*aa.xdim+2*aa.bwd+aa.btm, y0+125, aa.pp(iprm).name $
	, /device, align=0.5, charsize=aa.csiz, color=aa.white, orient=270.

	end
				    ;Plot azimuth color scale.
'azimuth': begin

	xdim = 3*aa.bwd
	tv, cbar_azimuth( D0=xdim, /notable, /black_bkg $
	, min=aa.pp(iprm).color.min, max=aa.pp(iprm).color.max $
	, gray=aa.pp(iprm).color.gray, wrap=aa.pp(iprm).color.wrap $
	, invert=aa.pp(iprm).color.invert ) $
	, aa.t*aa.xdim, ysz/2-xdim/2

	xyouts, aa.t*aa.xdim+xdim/2, ysz/2+[ -xdim/2-aa.bwd, xdim/2 ]+aa.btm $
	, [ '-90', '90' ] $
	, /device, align=0.5, charsize=aa.csiz, color=aa.white

				    ;Print name.
	xyouts, aa.t*aa.xdim+2*aa.bwd+aa.btm, ysz-2, aa.pp(iprm).name $
	, /device, align=0.0, charsize=aa.csiz, color=aa.white, orient=270.

	end
				    ;Plot zenith color scale.
'zenith': begin

	xdim = 3.*aa.bwd-2
	tv, cbar_zenith( D0=2*xdim, /notable, /black_bkg $
	, min=aa.pp(iprm).color.min, max=aa.pp(iprm).color.max $
	, gray=aa.pp(iprm).color.gray, wrap=aa.pp(iprm).color.wrap $
	, invert=aa.pp(iprm).color.invert ) $
	, aa.t*aa.xdim+1, ysz/2-xdim

	xyouts, aa.t*aa.xdim+2, ysz/2+[ -xdim-aa.bwd, xdim ]+aa.btm $
	, [ '180', '0' ] $
	, /device, align=0.0, charsize=aa.csiz, color=aa.white

				    ;Print name.
	xyouts, aa.t*aa.xdim+2*aa.bwd+aa.btm, ysz-2, aa.pp(iprm).name $
	, /device, align=0.0, charsize=aa.csiz, color=aa.white, orient=270.

	end
else:
end

end
;------------------------------------------------------------------------------
;
;	procedure:  azam_display
;
;	purpose:  magnify and display interactive image
;
;------------------------------------------------------------------------------
pro azam_display, inum, aa, hh
				    ;Erase button window.
wset, aa.winb  &  erase, aa.red
				    ;Parameter number.
iprm = aa.ww(inum).iprm
				    ;Magnify images and install high lights.
aa.ww(inum).img = azam_image( iprm, aa, hh )

				    ;Display the image.
wset, aa.ww(inum).win  &  erase, aa.black
tv, aa.ww(inum).img, 0, 0
				    ;Plot color bar.
azam_c_bar, aa.ww(inum).win, iprm, aa

end
;------------------------------------------------------------------------------
;
;	procedure:  azam_image_color
;
;	purpose:  reset image color
;
;------------------------------------------------------------------------------
pro azam_image_color, aa, hh
				    ;Popular variables.
t     = aa.t
xdim  = aa.xdim
ydim  = aa.ydim
txdim = t*xdim
tydim = t*ydim
white = aa.white
black = aa.black
                                    ;Get image name.
iname = pop_cult( /string, azam_image_names(aa), title='click on one' $
, help='azam', arg0='names', arg1=aa, winfree=aa.winfree )
if iname eq '** DISMISS **' then return

				   ;Get parameter number.
iprm = (where( iname eq aa.pp(*).name ))(0)

				   ;Open window for image.
xsize = txdim+3*aa.bwd
ysize = 300 > tydim
if aa.winfree then begin
	window, /free, xsize=xsize, ysize=ysize, title=iname
end else begin
	window, /free, xsize=xsize, ysize=ysize, title=iname $
	, xpos=1144-xsize, ypos=40
end
w3 = !d.window
				   ;Show ascii window.
wshow, aa.wina
				   ;image color menu.
amenu = $
[ '^loriginal color' $
, '' $
, '^lgray off' $
, '^lgray on' $
, '' $
, '^linvert off' $
, '^linvert on' $
, '' $
, '^lwrap off' $
, '^lwrap on' $
, '^g** DISMISS **' $
, '^yEXIT IMAGE COLOR' $
, '** HELP **' $
, '' $
, '^lscale minimum' $
, '^lscale maximum' $
, '' $
, '^lwhite background' $
, '^lblack background' $
, '^ldata background' $
] 
				    ;Menu striped of escapes.
bmenu = amenu
for ii=0,n_elements(bmenu)-1 do $
while strmid( bmenu(ii), 0, 1 ) eq '^' do $
bmenu(ii)=strmid( bmenu(ii), 2, 1000 )

				    ;Help information for image color.
menu_help = $
["    scale minimum   - reset minimum data value aligned to scale bar."$
,"    scale maximum   - reset maximum data value aligned to scale bar."$
,""$
,"      gray off      - produce color byte image."$
,"      gray on       - produce black & white byte image."$
,""$
,"     invert off     - produce positive image."$
,"     invert on      - produce negative image."$
,""$
,"      wrap off      - produce wrap around byte image."$
,"      wrap on       - produce direct scale byte image."$
,""$
,"  white background  - set background for unsolved data to white."$
,"  black background  - set background for unsolved data to black."$
,"   data background  - use data value entered by user for background."$
,""$
,"   original color   - reset image color to original."$
,""$
,"    ** DISMISS **   - dismiss; do not invoke a menu option."$
,"  EXIT IMAGE COLOR  - return to regular azam."$
,"     ** HELP **     - print this information."$
]
				    ;Loop till user clicks right button.
while 1 do begin
				    ;Compute byte image.
	img3 = azam_image( iprm, aa, hh )

				    ;Display image.
	wset, w3  &  erase, black
	tv, img3, 0, 0
				    ; Plot color bar
	azam_c_bar, w3, iprm, aa
				    ;Do computed cursors till right
				    ;button clicked.
	break8:
	azam_click_xy, aa, 'click for menu', xc, yc, w3, img3, state=state

				    ;Erase AZAM button window.
	wset, aa.winb  &  erase, aa.red

				    ;Copy of menu.
	cmenu = amenu
				    ;Color current state buttons yellow.
	whr = where( bmenu eq 'gray off'   ) + aa.pp(iprm).color.gray
	cmenu(whr) = '^y'+cmenu(whr)
	whr = where( bmenu eq 'invert off' ) + aa.pp(iprm).color.invert
	cmenu(whr) = '^y'+cmenu(whr)
	whr = where( bmenu eq 'wrap off'   ) + aa.pp(iprm).color.wrap
	cmenu(whr) = '^y'+cmenu(whr)

				    ;Color yellow button representing
				    ;current background.
	bwhr = where( bmenu eq 'black background' )
	wwhr = where( bmenu eq 'white background' )
	dwhr = where( bmenu eq 'data background'  )
	case aa.pp(iprm).color.back of
		black:	cmenu(bwhr) = '^y'+cmenu(bwhr)
		white:	cmenu(wwhr) = '^y'+cmenu(wwhr)
		else:	cmenu(dwhr) = '^y'+cmenu(dwhr)
	end
				    ;Prompt for choice.
	case	pop_cult( /string, cmenu, winfree=aa.winfree $
		, title = 'Pick one (yellow is present case)' $
		, help='azam', arg0='window', arg1=aa, arg2=menu_help $
		) $
	of
				    ;Continue on with image color.
	'** DISMISS **': goto, break8

				    ;Exit image color.
	'EXIT IMAGE COLOR': goto, break51

				    ;Reset original color.
	'original color':  aa.pp(iprm).color = aa.pp(iprm).color0

				    ;Set gray off/on.
	'gray off': aa.pp(iprm).color.gray = 0
	'gray on':  aa.pp(iprm).color.gray = 1

				    ;Set invert off/on.
	'invert off': aa.pp(iprm).color.invert = 0
	'invert on':  aa.pp(iprm).color.invert = 1

				    ;Set wrap off/on.
	'wrap off': aa.pp(iprm).color.wrap = 0
	'wrap on':  aa.pp(iprm).color.wrap = 1

				    ;Set backgroud color.
	'data background': begin

		on_ioerror, ioerror3
		ioerror3:
		rd = 0.
		reads, azam_text_in(aa,'Enter background value'), rd

		aa.pp(iprm).color.back = -1

		if aa.pp(iprm).a_np eq aa.npoints $
		then if (aa.pbkg)(0) ge 0 then aa.pp(iprm).value(aa.pbkg)=rd $
		else if (aa.sbkg)(0) ge 0 then aa.pp(iprm).value(aa.sbkg)=rd

		end
				    ;Set black background.
	'black background':  aa.pp(iprm).color.back = black

				    ;Set white background.
	'white background':  aa.pp(iprm).color.back = white

				    ;Set scale bar minimum.
	'scale minimum': begin
		on_ioerror, ioerror0
		mess = 'Enter minimum (was '+stringit(aa.pp(iprm).color.min)+')'
		ioerror0:
		tmp = 1.
		reads, azam_text_in(aa,mess), tmp
		aa.pp(iprm).color.min = tmp
		end
				    ;Set scale bar maximum.
	'scale maximum': begin
		on_ioerror, ioerror1
		mess = 'Enter maximum (was '+stringit(aa.pp(iprm).color.max)+')'
		ioerror1:
		tmp = 1.
		reads, azam_text_in(aa,mess), tmp
		aa.pp(iprm).color.max = tmp
		end
	else:
	end
				    ;Set 2D color image.
	azam_op_image, aa, iprm
end
				    ;Erase AZAM button window.
break51:  wset, aa.winb  &  erase
				    ;Global azimuth color update.
prms = [ aa.azm-aa.azm, aa.azm, aa.azz, aa.azz_orig, aa.azz_ref, aa.azz_amb ]
if  (where( iprm eq prms ))(0) ge 0  then begin
	for k=0L,n_elements(prms)-1 do begin
		aa.pp(prms(k)).color = aa.pp(iprm).color
		azam_op_image, aa, prms(k)
	end
end
				    ;Global incline color update.
prms = [ aa.psi, aa.zen_ang, aa.zen_orig, aa.zen_ref, aa.zen_amb ]
if  (where( iprm eq prms ))(0) ge 0  then begin
	for k=0L,n_elements(prms)-1 do begin
		aa.pp(prms(k)).color = aa.pp(iprm).color
		azam_op_image, aa, prms(k)
	end
end
				    ;Global flux color update.
if  iprm eq aa.flx  then begin
	aa.pp(aa.flx-aa.azm).color = aa.pp(iprm).color
	azam_op_image, aa, aa.flx-aa.azm
end
				    ;Restore display images.
for inum=0,2 do  azam_display, inum, aa, hh
azam_mgfy, aa
				    ;Delete extra window
wdelete, w3

end
;------------------------------------------------------------------------------
;
;	procedure:  azam_typeroi
;
;	purpose:  type out defroi parameter averages.
;
;------------------------------------------------------------------------------
pro azam_typeroi, aa, pwhr, swhr, file

np = n_elements(pwhr)
if swhr(0) ge 0 then  ns=n_elements(swhr)  else  ns=0

				    ;Isolate last 14 characters of
				    ;extra parameter names.
name0 = aa.pp(aa.extra0).name
name0 = strmid( name0, (strlen(name0)-14) > 0, 14 )
name1 = aa.pp(aa.extra1).name
name1 = strmid( name1, (strlen(name1)-14) > 0, 14 )

				    ;Compute various averages for (x,y).
xq   = float( total( (pwhr mod aa.xdim)+aa.x0 ) )/np
yq   = float( total( (pwhr /   aa.xdim)+aa.y0 ) )/np
xrpq = float( total( aa.xrpnt(pwhr) ) )/np
yrpq = float( total( aa.yrpnt(pwhr) ) )/np
xmmq = (total(aa.b__e_w(pwhr))/np-aa.mn_e_w)*aa.mm_per_deg
ymmq = (total(aa.b__lat(pwhr))/np-aa.mn_lat)*aa.mm_per_deg
ctmq = total( aa.pp(aa.cct).value(pwhr))/np
ex0q = total( aa.pp(aa.extra0).value(pwhr))/np
ex1q = total( aa.pp(aa.extra1).value(pwhr))/np
utq  = total( aa.b__ut(pwhr) )/np
lnq  = total( aa.b__lon(pwhr) )/np
ltq  = total( aa.b__lat(pwhr) )/np

if ns ne 0 then begin
	x   = float( total( (swhr mod aa.xdim)+aa.x0 ) )/ns
	y   = float( total( (swhr /   aa.xdim)+aa.y0 ) )/ns
	xrp = float( total( aa.xrpnt(swhr) ) )/ns
	yrp = float( total( aa.yrpnt(swhr) ) )/ns
	xmm = (total(aa.b__e_w(swhr))/ns-aa.mn_e_w)*aa.mm_per_deg
	ymm = (total(aa.b__lat(swhr))/ns-aa.mn_lat)*aa.mm_per_deg
	ctm = total( aa.pp(aa.cct).value(swhr) )/ns
	fld = total( aa.pp(aa.fld).value(swhr) )/ns
	flx = total( aa.pp(aa.flx).value(swhr) )/ns
	dvd = total( aa.pp(aa.cen1).value(swhr) )/ns
	alf = total( aa.pp(aa.alpha).value(swhr) )/ns
	jz  = total( aa.pp(aa.jz).value(swhr) )/ns
	jza = total( aa.pp(aa.jz_amb).value(swhr) )/ns
	psi = total( aa.pp(aa.psi).value(swhr) )/ns
	zen = total( aa.pp(aa.zen_ang).value(swhr) )/ns
	zea = total( aa.pp(aa.zen_amb).value(swhr) )/ns
	azm = avg_azm(aa.pp(aa.azm).value(swhr) )
	azz = avg_azm(aa.pp(aa.azz).value(swhr) )
	aza = avg_azm(aa.pp(aa.azz_amb).value(swhr))
	aaz = ( (azm+180.+180.) mod 360. ) - 180.
	ex0 = total( aa.pp(aa.extra0).value(swhr) )/ns
	ex1 = total( aa.pp(aa.extra1).value(swhr) )/ns
	ut  = total( aa.b__ut(swhr) )/ns
	ln  = total( aa.b__lon(swhr) )/ns
	la  = total( aa.b__lat(swhr) )/ns
end else begin
	x   = 0.
	y   = 0.
	xrp = 0.
	yrp = 0.
	xmm = 0.
	ymm = 0.
	ctm = 0.
	fld = 0.
	flx = 0.
	dvd = 0.
	alf = 0.
	jz  = 0.
	jza = 0.
	psi = 0.
	zen = 0.
	zea = 0.
	azm = 0.
	azz = 0.
	aza = 0.
	aaz = 0.
	ex0 = 0.
	ex1 = 0.
	ut  = 0.
	ln  = 0.
	la  = 0.
end
if aa.pp(aa.extra0).a_np eq aa.nsolved then  ex0q=ex0
if aa.pp(aa.extra1).a_np eq aa.nsolved then  ex1q=ex1

				    ;Formats.
gg = '(a14,g14.6,g26.6)'
f1 = '(a14,f14.1,f12.1,f14.1,f12.1)'
f2 = '(a14,f14.2,f12.2,f14.2,f12.2)'
f4 = '(a14,f14.4,f12.4,f14.4,f12.4)'

if n_elements(file) eq 0 then begin

print,'AZAM defroi averages ------------------------------------------------'
print, aa.head(2) mod 100,'.',aa.head(0),'.',aa.head(1) $
, '               solved points                all points' $
, fo='(i2.2,a1,i2.2,a1,i2.2,a)' 
print,'universal time',  ut,       utq,       fo='(a14,f14.4,f26.4)'
print,'number points ',  ns,        np,       fo='(a14,i14,i26)'
print,'image (x,y)   ',   x,   y,   xq,   yq, fo=f2
print,'  asp (x,y)   ', xrp, yrp, xrpq, yrpq, fo=f2
print,'   Mm (x,y)   ', xmm, ymm, xmmq, ymmq, fo=f2
print,'longitude lat ',  ln,  la,  lnq,  ltq, fo=f4
print, name0          , ex0,      ex0q,       fo=gg
print, name1          , ex1,      ex1q,       fo=gg
print,'continuum     ', ctm,      ctmq,       fo='(a14,f14.1,f26.1)'
print,'field         ', fld,                  fo=f1
print,'flux          ', flx,                  fo=f1
print,'Doppler       ', dvd,                  fo=f4
print,'1-fill        ', alf,                  fo=f4
print,'                               ambiguous'
print,'Jz            ',  jz, jza,             fo=f1
print,'azimuth       ', azm, aaz,             fo=f2
print,'incline       ', psi,                  fo=f2
print,'zenith azimuth', azz, aza,             fo=f2
print,'zenith angle  ', zen, zea,             fo=f2
print

return
end
				    ;Output ascii defrio file.
openw, u, file, /get_lun

printf,u,'AZAM defroi averages ------------------------------------------------'
printf,u, aa.head(2) mod 100,'.',aa.head(0),'.',aa.head(1) $
, '               solved points                all points' $
, fo='(i2.2,a1,i2.2,a1,i2.2,a)' 
printf,u,'universal time',  ut,       utq,       fo='(a14,f14.4,f26.4)'
printf,u,'number points ',  ns,        np,       fo='(a14,i14,i26)'
printf,u,'image (x,y)   ',   x,   y,   xq,   yq, fo=f2
printf,u,'  asp (x,y)   ', xrp, yrp, xrpq, yrpq, fo=f2
printf,u,'   Mm (x,y)   ', xmm, ymm, xmmq, ymmq, fo=f2
printf,u,'longitude lat ',  ln,  la,  lnq,  ltq, fo=f4
printf,u, name0          , ex0,      ex0q,       fo=gg
printf,u, name1          , ex1,      ex1q,       fo=gg
printf,u,'continuum     ', ctm,      ctmq,       fo='(a14,f14.1,f26.1)'
printf,u,'field         ', fld,                  fo=f1
printf,u,'flux          ', flx,                  fo=f1
printf,u,'Doppler       ', dvd,                  fo=f4
printf,u,'1-fill        ', alf,                  fo=f4
printf,u,'                               ambiguous'
printf,u,'Jz            ',  jz, jza,             fo=f1
printf,u,'azimuth       ', azm, aaz,             fo=f2
printf,u,'incline       ', psi,                  fo=f2
printf,u,'zenith azimuth', azz, aza,             fo=f2
printf,u,'zenith angle  ', zen, zea,             fo=f2

free_lun, u

end
;------------------------------------------------------------------------------
;
;	procedure:  azam_defroi
;
;	purpose:  define region of interest aa.defroi
;
;------------------------------------------------------------------------------
pro azam_defroi, aa, hh
				    ;Infinite loop till dismiss request.
while 1 do begin
				    ;Where array for aa.defroi.
	pwhr = -1L
	if aa.defroi(0,0) ne 255B then  pwhr = where(aa.defroi)

	swhr = -1L
	if pwhr(0) ge 0 then begin
				    ;Where array for inverted points.
		swhr = where( aa.defroi and aa.sdat )

				    ;Type out defroi averages.
		azam_typeroi, aa, pwhr, swhr
	end
				    ;Set flag for uninverted points.
	if n_elements(pwhr) gt n_elements(swhr) then begin
		uninv = 1L
	end else begin
		uninv = 0L
		if  pwhr(0) ge 0L  and  swhr(0) lt 0L  then  uninv=1L
	end
				    ;Some menu strings.
	clearDefroi = 'clear defroi'
	addDefroi   = 'add defroi'
	subDefroi   = 'subtract defroi'
	xorDefroi   = 'xor defroi'
	andDefroi   = 'and defroi'
	delUninv    = 'delete uninverted points'
	writeDefroi = 'write defroi file'
	readDefroi  = 'read defroi file'
	mincontour  = 'minimum contour'
	maxcontour  = 'maximum contour'

				    ;Disable menu items for empty defroi.
	if pwhr(0) lt 0 then begin
		clearDefroi = '^x'+clearDefroi
		addDefroi   = '^x'+addDefroi
		subDefroi   = '^x'+subDefroi
		xorDefroi   = '^x'+xorDefroi
		andDefroi   = '^x'+andDefroi
		delUninv    = '^x'+delUninv
		writeDefroi = '^x'+writeDefroi
	end
				    ;If none present, disable delete of
				    ;uninverted points.
	if uninv eq 0L then  delUninv='^x'+delUninv

				    ;Disable read/write defroi in zoom.
	if aa.index eq 3 then begin
		readDefroi  = '^x'+readDefroi
		writeDefroi = '^x'+writeDefroi
	end
				    ;Pop up menu.
	mlab = pop_cult( /string, title='Click on choice', winfree=aa.winfree $
	, [ clearDefroi, 'one point defroi', '^greset defroi' $
	, '', addDefroi, subDefroi, andDefroi, xorDefroi, '', delUninv $
	, '^y** DISMISS **', '** HELP **', '', mincontour, maxcontour, '', '' $
	, '', writeDefroi, readDefroi ] $
	, help='azam', arg0='window', arg1=aa, arg2= $
	["" $
	,"'clear defroi'             ~ remove already defined region"$
	,"'one point defroi'         ~ set region to one point only"$
	,"'reset defroi'             ~ reset region with IDL's defroi"$
	,"" $
	,"'add defroi'               ~ add to region with IDL's defori"$
	,"'subtract defroi'          ~ subtract region with IDL's defori"$
	,"'and defroi'               ~ and with IDL's defori"$
	,"'xor defroi'               ~ exclusive or with IDL's defori"$
	,"" $
	,"'delete uninverted points' ~ remove all uninverted points"$
	,"" $
	,"'minimum contour'          ~ remove points below contour level" $
	,"'maximum contour'          ~ remove points above contour level" $
	,"" $
	,"'write defroi file'        ~ output file of 1.'s & 0.'s; a_* format"$
	,"'read defroi file'         ~ input a_* format file; non-zero is true"$
	,"" $
	,"'** DISMISS **'            ~ return to interactive azam"$
	,"'** HELP **'               ~ print this information"$
	] )

	case 1 of
				    ;Normal return.
	'** DISMISS **' eq mlab: return
				    ;Write defroi file.
	'write defroi file' eq mlab: begin

		mess = 'Enter output file name (q=quit) (Return=DEFROI)'
		error = 1
		while error ne 0 do begin
			error = 0
                                    ;Prompt for file name.
			azam_file = strcompress( azam_text_in(aa,mess) $
			, /remove_all )

			if azam_file eq 'q' then  return
			if azam_file eq ''  then  azam_file = 'DEFROI'

                                    ;Protect a_* files.
			if  strmid( azam_file, 0, 2 ) eq 'a_'  then begin
				mess = 'REENTER, ' $
				+ 'output to a_file not allowed (q=quit)'
				error = 1
			end
		end
				    ;Set output vector.
		if uninv then begin
			vector = fltarr(aa.npoints)
			vector(aa.vec_pxy) = aa.defroi(aa.pxy)
		end else begin
			vector = fltarr(aa.nsolved)
			vector(aa.vec_sxy) = aa.defroi(aa.sxy)
		end

                                    ;Output stream file.
		o_dir = aa.dty
		if write_floats( o_dir+azam_file, vector, err ) then $
		if azam_out_dir( aa, o_dir ) then $
		i = write_floats( o_dir+azam_file, vector )

				    ;Output ascii file.
		azam_typeroi, aa, pwhr, swhr, o_dir+azam_file+'.av'

		end
				    ;Read defroi file.
	'read defroi file' eq mlab: begin

		mess = 'Enter "path" defroi file (q=quit) (Return=DEFROI)'
		error1:
		error = 1
		while error ne 0 do begin

				    ;Prompt for file name.
			azam_file = strcompress( azam_text_in(aa,mess) $
			, /remove_all )

			if azam_file eq 'q' then  return
			if azam_file eq ''  then  azam_file = 'DEFROI'

				    ;Try to read file.
			a_azam = read_floats( azam_file, error )
			if error ne 0 then  print, !err_string

			if error eq 0 then begin

				    ;Check if file is right length.
				np = n_elements(a_azam)
				if  np ne aa.npoints $
				and np ne aa.nsolved then begin
					mess = 'REENTER, ' $
					+ 'file is wrong length (q=quit)'
					error = 1
				end
			end
	 	end
				    ;Set 2D defroi array.
		aa.defroi = bytarr(aa.xdim,aa.ydim)
		if np eq aa.npoints $
		then  aa.defroi(aa.pxy) = a_azam(aa.vec_pxy) ne 0. $
		else  aa.defroi(aa.sxy) = a_azam(aa.vec_sxy) ne 0.

		aa.rehi = 1L
		for inum=0,2 do  azam_display, inum, aa, hh
		end
				    ;Get click for one point defroi.
	'one point defroi' eq mlab: begin
		azam_click_xy, aa, 'click defroi point', xc, yc
		aa.defroi = 0B
		aa.defroi(xc,yc) = 1B
		aa.rehi = 1L
		for inum=0,2 do  azam_display, inum, aa, hh
		end
				    ;Delete uninverted points.
	'delete uninverted points' eq mlab: begin
		aa.rehi = 1L
		aa.defroi = aa.defroi and aa.sdat
		for inum=0,2 do  azam_display, inum, aa, hh
		end
				    ;Clear defroi array and return.
	'clear defroi' eq mlab: begin
		aa.defroi = 255B
		hh.outlineroi = -1L
		for inum=0,2 do  azam_display, inum, aa, hh
		return
		end
				    ;Reset defroi to contour level.
	'minimum contour' eq mlab  or $
	'maximum contour' eq mlab: begin

		undefine_sbsp, iprm, level
		azam_contour, aa, hh, iprm, level

		if n_elements(level) ne 0 then begin

				    ;If empty, fill defroi array.
			if pwhr(0) lt 0 then begin
				aa.defroi(*,*) = 0
				if aa.pp(iprm).a_np eq aa.npoints $
				then  aa.defroi(aa.pxy) = 1 $
				else  aa.defroi(aa.sxy) = 1
			end

			if 'minimum contour' eq mlab $
			then  aa.defroi $
			      = aa.defroi  and  aa.pp(iprm).value ge level $
			else  aa.defroi $
			      = aa.defroi  and  aa.pp(iprm).value lt level

				    ;Restore display.
			aa.rehi = 1L
			for inum=0,2 do  azam_display, inum, aa, hh
		end
		end
				    ;Rerun IDL's defroi.
	'add defroi'      eq mlab  or $
	'subtract defroi' eq mlab  or $
	'and defroi'      eq mlab  or $
	'xor defroi'      eq mlab  or $
	'reset defroi'    eq mlab: begin

				    ;Prompt for data image.
		slab = pop_cult( azam_image_names(aa), /string $
		, winfree=aa.winfree $
		, title='Select data array for defroi' $
		, help='azam', arg0='names', arg1=aa )
		if slab eq '** DISMISS **' then return

				    ;Parameter number.
		iprm = (where( slab eq aa.pp.name ))(0)

				    ;Print instructions.
		azam_message, aa $
		, [ 'Left button to mark point (at least 3)' $
		,   'Middle button to erase previous point' $
		,   'Right button to close region' ]

				    ;Open window for defroi.
		txdim = aa.t*aa.xdim
		tydim = aa.t*aa.ydim
		if aa.winfree then begin
			window, /free, xsize=txdim, ysize=tydim $
			, title='DEFROI'
		end else begin
			window, /free, xsize=txdim, ysize=tydim $
			, title='DEFROI' $
			, xpos=100, ypos=40
		end
		w3 = !d.window
		img3 = azam_image( iprm, aa, hh )
		tv, img3, 0, 0
				    ;Run IDL's defroi.
		whr = defroi( txdim, tydim, 0, 0, /restore )
		wdelete, w3
				    ;Set new defroi.
		newroi = bytarr(aa.xdim,aa.ydim)
		if whr(0) ge 0 then begin
			y = whr/txdim
			x = whr-y*txdim
			y = y/aa.t
			x = x/aa.t
			newroi(y*aa.xdim+x) = 1B
		end
				    ;Reset or add or subtract defroi.
		if aa.defroi(0,0) eq 255B then  aa.defroi=0B
		case mlab of
		addDefroi: aa.defroi = aa.defroi or newroi
		subDefroi: aa.defroi = aa.defroi-(aa.defroi and newroi)
		andDefroi: aa.defroi = aa.defroi and newroi
		xorDefroi: aa.defroi = aa.defroi xor newroi
		else:      aa.defroi = newroi
		end
				    ;Check for unsolved points.
		aa.defroi = aa.defroi and aa.pdat
		whr = where( aa.defroi ne (aa.defroi and aa.sdat) )
		if whr(0) ge 0 then begin
			if	1 - pop_cult( ['^gyes','no'], title $
				= 'Delete uninverted raster points?' $
				, winfree=aa.winfree ) $
			then  aa.defroi = aa.defroi and aa.sdat
		end
				    ;Restore display.
		aa.rehi = 1L
		for inum=0,2 do  azam_display, inum, aa, hh

		end

	else: stop
	end
end

end
;------------------------------------------------------------------------------
;
;	procedure:  azam_tools
;
;	purpose:  compiles azam_tools if needed and print usage
;
;------------------------------------------------------------------------------
pro azam_tools
azam_tools_usage
end
