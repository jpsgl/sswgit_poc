pro azam_findvrtx, image, redfac,vx1, vy1, vx2, vy2, sx = sx, sy = sy, wsize = wsize, $
	xpos=xpos, ypos=ypos
;+
; NAME:
;	AZAM_FINDVRTX
;
; PURPOSE:
;	Interactively select the vertices of a rectangle on an image
;
; CATEGORY:
;	Image analysis.
;
; INPUTS:
;	Image:	The variable that represents the image displayed in current 
;		window.  This data need not be scaled into bytes.
;		The profile graphs are made from this array.
;	redfac = pixel size reduction factor
;
; KEYWORD PARAMETERS:
;	SX:	Starting X position of the image in the window.  If this 
;		keyword is omitted, 0 is assumed.
;
;	SY:	Starting Y position of the image in the window.  If this
;		keyword is omitted, 0 is assumed.
;
;	WSIZE:	The size of the PROFILEP window as a fraction or multiple 
;		of 640 by 300.
;
;	xpos,
;	ypos:  pixel position for window
;
; OUTPUTS:
;	vx1,vy1:	x,y indices of 1st rectangle vertex
;	vx2,vy2:	x,y indices of 2nd rectangle vertex
;
; COMMON BLOCKS:
;	None.
;
; SIDE EFFECTS:
;	A new window is created and used for the Information.  When done,
;	the new window is deleted.
;
; RESTRICTIONS:
;	None.
;
;==============================================================================
;
;	Check parameters.
;
if n_params() ne 6 then begin
	print
	print, "Incorrect number of parameters for AZAM_FINDVRTX"
	print
	return
endif
;-

on_error,2                              ;Return to caller if an error occurs
if n_elements(sx) eq 0 then sx = 0	;Default start of image
if n_elements(sy) eq 0 then sy = 0
if n_elements(wsize) eq 0 then wsize = .75
s = size(image)
maxv = max(image) 			;Get extrema
minv = min(image)
orig_w = !d.window
nx = s(1)				;Cols in image
ny = s(2)				;Rows in image

jump3:	; entry point for re-selection of area

tvcrs,sx+nx/2,sy+ny/2,/dev
tickl = 0.1				;Cross length
;print,'Left mouse button to toggle between rows and columns.'
;print,'Right mouse button to Exit.'

;-----------

if (n_elements(xpos) eq 0) or $
   (n_elements(ypos) eq 0) then message, 'must specify xpos and ypos!'
window,/free ,xs=wsize*640, ys=wsize*300,title='Rectangle Vertices',$ ;Make new window
	xpos=xpos, ypos=ypos

;-----------

new_w = !d.window	; Info window
old_mode = -1				;Mode = 0 for rows, 1 for cols
old_font = !p.font			;Use hdw font
!p.font = 0
mode = 0
ivrtx = 0

while 1 do begin
	wset,orig_w		;Image window
	cursor,x,y,2,/dev	;Read position
	x = x - sx		;Remove bias
	y = y - sy

	wset,new_w	; Info Window
	msg0 = 'LEFT CLICK FOR 1st VERTEX OF RECTANGLE'
	xyouts,.50,.65,/norm,msg0,align=.5
	wset,orig_w	;Image window
;  x,y positions of first vertex of rectangular area
	if !mouse.button eq 1 and ivrtx eq 0 then begin
		cursor,vx1,vy1,4,/dev
		vx1 = vx1 - sx & vy1 = vy1 - sy
		vx1r = round((vx1 - sx)/redfac) 
		vy1r = round((vy1 - sy)/redfac)
		msg1 = '1st Vertex -- X:' + strmid(vx1r,8,4) + '  ' + $
			'Y:' + strmid(vy1r,8,4)
		print,msg1
		wset,new_w	; Info window
		xyouts,.50,.50,/norm,msg1,align=.5
		ivrtx = 1
		!mouse.button = 0
	endif
	if ivrtx eq 1 then begin
		wset,new_w	; Info window
		msg0 = 'LEFT CLICK FOR 2nd VERTEX OF RECTANGLE'
		xyouts,.50,.35,/norm,msg0,align=.5
		wset,orig_w	;Image window
	endif
;  x,y positions of second vertex of rectangular area
	if !mouse.button eq 1 and ivrtx eq 1 then begin
		cursor,vx2,vy2,4,/dev
		vx2 = vx2 - sx & vy2 = vy2 - sy
		vx2r = round((vx2 - sx)/redfac) 
		vy2r = round((vy2 - sy)/redfac)
		msg2 = '2nd Vertex -- X:' + strmid(vx2r,8,4) + '  ' + $
			'Y:' + strmid(vy2r,8,4)
		print,msg2
		wset,new_w	; Info window
		xyouts,.50,.20,/norm,msg2,align=.5
		print,msg2
		ivrtx = 2
		wdelete,new_w	; DELETE INFO WINDOW
		return
	endif

	wset,new_w	; Info window

	if mode ne old_mode then begin
		old_mode = mode
		first = 1
	endif

	if (x lt nx) and (y lt ny) and $
		(x ge 0) and (y ge 0) then begin	;Draw it
		if first eq 0 then begin	;Erase?
			xyouts,.50,.85,/norm,value,col=0,align=.5 ;Erase text
			empty
		endif else first = 0
		xr = round((x - sx)/redfac) 
		yr = round((y - sy)/redfac)
		value = 'Current Pos X:' + strmid(xr,8,4) + '  ' + $
			'Y:' + strmid(yr,8,4)
		xyouts,.50,.85,/norm,value,align=.5	;Text of locn
	endif
endwhile

wdelete,new_w		; DELETE INFO WINDOW

return
end
