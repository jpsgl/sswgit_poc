pro azam_help_usage
;+
;
;	procedure:  azam_help
;
;	purpose:  print help info about azam procedure
;
;	routines: azam_help_usage
;		  azam_help_win     azam_help_yellow   azam_help_button
;		  azam_help
;
;	author:  paul@ncar, 3/95	(minor mod's by rob@ncar)
;
;==============================================================================
;
if 1 then begin
	print
	print, "usage:	azam_help, branch, labels, title, aa, rows, bwd, blt"
	print
	print, "	Print help info about azam procedure."
	return
endif
;-
end
;-----------------------------------------------------------------------------
;
;	procedure:  azam_help_win
;
;	purpose:  open window and print help text
;
;-----------------------------------------------------------------------------
pro azam_help_win, aa, message
				    ;Maximum text lines per window.
lmax = 40
				    ;Text lines per page.
pmax = lmax*2/3
				    ;Open window for text output.
wsav = !d.window
xsize = aa.blt*6
ysize = aa.bwd*lmax
if aa.winfree then begin
	window, title='AZAM HELP' $
	, /free, xsize=xsize, ysize=ysize
end else begin
	window, title='AZAM HELP' $
	, /free, xsize=xsize, ysize=ysize $
	, xpos=1140-xsize, ypos=900-(ysize+3*aa.bwd+40)
end

				    ;Append message to instructions.
mess = ["CLICK left(continue) center(continue) right(dismiss)","",message]

				    ;Get number of text lines.
void, n_dims( mess, nline )
				    ;Loop through windows of text.
for j=0,((nline+pmax-1)/pmax)-1 do begin

				    ;Line number range to print this window.
	first = j*pmax
	last  = ( ((j+1)*pmax+lmax-pmax) < nline ) - 1

				    ;Erase window.
	erase, aa.white
				    ;Outline paging.
	if last lt nline-1 then begin
		yll = aa.bwd*(lmax-pmax)-2
		tmp = replicate(aa.red,xsize)
		tv, tmp, 0, ysize-1
		tv, tmp, 0, ysize-2
		tv, tmp, 0, yll
		tmp = replicate(aa.red,1,aa.bwd*pmax)
		tv, tmp, 0, yll
		tv, tmp, xsize-1, yll
	end
				    ;Print text window.
	for i=first,last do begin
				    ;Print text line.
		xyouts, 10, ysize-(i-first+1)*aa.bwd+2, mess(i) $
		, /device, align=0.0, color=aa.black, charsize=aa.csiz
	end
				    ;Wait for user to click on window.
	cursor, xc, yc, 4, /down, /device
	mouse_state = !err
	repeat  cursor, xc, yc, 4, /device, /nowait  until  !err eq 0
	if mouse_state eq 4 or last eq nline-1 then  goto, break0

end
				    ;Restore active window.
break0:
wdelete, !d.window
if wsav ge 0 then  wset, wsav

end
;-----------------------------------------------------------------------------
;
;	procedure:  azam_help_yellow
;
;	purpose:  return append message about interactive yellow buttons
;
;-----------------------------------------------------------------------------
function azam_help_yellow
return, [""$
," The three yellow buttons go together.  They refer to interactive"$
," criteria enabled for the left, center, right mouse buttons."$
,""$
," Usually the left yellow button refers to ambiguity resolution"$
," by some criterion with the middle button refering to the anti-criterion."$
," Clicking the left or center button will cycle through"$
," 'original'/'anti original', 'reference'/'anti reference',"$
," 'up down'/'anti up down', 'azimuth'/'anti azimuth', 'reference/smooth'."$
,""$
," The right yellow button refers to the criterion enabled for the right"$
," mouse button.  Clicking this button will cycle through 'wads', 'smooth',"$
," 'no change', 'average'."$
]
end
;-----------------------------------------------------------------------------
;
;	procedure:  azam_help_button
;
;	purpose:  print help info about an azam button
;
;-----------------------------------------------------------------------------
pro azam_help_button, labels, branch, aa $
, xpos=xpos, ypos=ypos, rows=rows, bLength=bLength, bWidth=bWidth

				    ;Prompt for button number.
nlab = pop_cult( '^b'+labels, winfree=aa.winfree $
, title='What do you want help with?' $
, xpos=xpos, ypos=ypos, rows=rows, bLength=bLength, bWidth=bWidth )

				    ;Get button label.
lbl = labels(nlab)
				    ;Strip label of escapes.
while strmid(lbl,0,1) eq '^' do  lbl=strmid(lbl,2,1000)

				    ;Label string with quotes.
QlabelQ = ''''+lbl+''''
				    ;Initialize message to one line.
message = [ QlabelQ+':' ]
				    ;Select help message depending on case.
case 1 of

'** DISMISS **' eq lbl: message = [ message, '', $
[" Do not take further action on this request.  Return to some higher" $
," level procudure." $
]]

'** EXIT AZAM **' eq lbl: message = [ message, '', $
[" Do sequence that will return from AZAM to calling program (usually" $
," the user interactive dialog)." $
,"" $
," The exit sequence goes as follows:" $
," 1. Compare in core line of sight azimuth array with the" $
,"    file 'a_azm'." $
," 2. If core line of sight file does not agree with file 'a_azm'" $
,"    prompt user if the set of files 'a_azm', 'a_1azm', 'a_1incl'," $
,"    'a_2azm', 'a_2incl' are to be updated." $
," 3. At user's ok, update the 'a_*' files." $
," 4. Update file 'AZAM' as a copy of file 'a_azm' if needed." $
," 5. Do various clean up and return to calling program." $
]]

'menu' eq lbl: message = ["Pick option from menu that pops up."]

'read a_* file' eq lbl: $
message = $
[" Read an a_* image file or one of same length into azam."$
,""$
," A menu will pop up with name of most a_* files." $
," If the file name you want to read is not present you"$
," can click 'WIDGET ENTRY' and enter the file path."$
,""$
," There are presently two structures in azam available"$
," to process user read files.   You will be prompted for which"$
," one to overwrite."$
]

'all clear' eq lbl: $
message = $
[" Clear all representation of:"$
," ambigs, reversal, sight reveral, contour, defroi, arrow points"$
]

(where( [ 'magnify', 'short arrows', 'no magnify' ] eq lbl ))(0) ge 0: $
message = $
[" This interactive button has states:" $
,""$
," magnify      ~ enable mouse updates of magnified window"$
," short arrows ~ enable mouse updates of magnified window"$
," no magnify   ~ disable mouse updates of magnified window"$
,""$
,"Enable mouse updates of magnified window to see magnified changes"$
,"immediately or move area displayed."$
,""$
," Magnified window has 38x38 raster points with 14x resolution."$
," Arrows are surface tangent field; length proportionate to strength."$
," Contours are flux; 250 Gauss resolution."$
,""$
]

(where([ 'clear', 'ambigs', 'reversal', 'sight reversal' ] eq lbl ))(0) ge 0: $
message = $
[" User chooses one high light from set:" $
,""$
," clear          ~ use no high light from this set"$
," ambigs         ~ high light obtuce point to point instrument frame azimuth"$
," reversal       ~ high light zenith frame field reversal"$
," sight reversal ~ high light instrument frame field reversal"$
,""$
," Choice is available both locally with interactive mouse and globally" $
," from 'menu'.  Interactive mouse high light type is reset by clicking on" $
," interactive button than cycles through these states.  When azam loads"$
," 'ambigs' is selected."$
]

(where( [ 'defroi', 'clear defroi' ] eq lbl ))(0) ge 0: $
message = $
[" IDL has a procedure defroi to 'define a region of interest'."$
," Clicking defroi invokes azam's application of IDL's defroi."$
," 'azam' has some added features such as adding two regions."$
," Methods to output/input defroi files are also avialable."$
,""$
," 'defroi files' are in the same format as a_* image files."$
," These files have 1.'s to define the region, 0.'s otherwise."$
," Users can ASP routine a_image.pro to read a 'defroi file'."$
]

'image color' eq lbl: $
message = $
[" Users have some control over color for all azam images."$
," Internally this is done by resetting tvasp.pro keywords."$
,""$
," A file read by menu item 'read a_* file' usually will"$
," not have good color resolution.   Use 'image color' to"$
," adjust."$
]

(where(['1x1','2x2','4x4','8x8','16x16'] eq lbl) ge 0)(0): $
message = [ message, '', $
[" This button determines the size of the image area effected about where"$
," the mouse is drug.  Clicking this button cycles through sizes"$
," 1x1, 2x2, 4x4, 8x8, 16x16."$
]]

'original' eq lbl : message = [ message, '', $
[  QlabelQ+" is a criterion invoked interactively by the mouse on small"$
," image areas.  This criterion restores the 'original' (when the"$
," program began) azimuth resolution."$
], azam_help_yellow() ]

'anti original' eq lbl: message = [ message, '', $
[  QlabelQ+" is a criterion invoked interactively by the mouse on small"$
," image areas.  This criterion sets the 'anti original' (opposite to"$
," when the program began) azimuth resolution."$
], azam_help_yellow() ]

'reference' eq lbl: message = [ message, '', $
[  QlabelQ+" is a criterion invoked interactively by the mouse on small"$
," image areas.  This criterion restores the 'reference' azimuth resolution."$
,""$
," 'reference' is the azimuth resolution saved in core the last time"$
," 'set reference' was clicked.  If 'set reference' has not been clicked,"$
," 'reference' is the same as the 'original'."$
], azam_help_yellow() ]

'anti reference' eq lbl: message = [ message, '', $
[  QlabelQ+" is a criterion invoked interactively by the mouse on small"$
," image areas.  This criterion sets the 'anti reference' (opposite to"$
," 'reference') azimuth resolution."$
,""$
," 'reference' is the azimuth resolution saved in core the last time"$
," 'set reference' was clicked.  If 'set reference' has not been clicked,"$
," 'reference' is the same as the 'original'."$
], azam_help_yellow() ]

'up down' eq lbl : message = [ message, '', $
[  QlabelQ+" is a criterion invoked interactively by the mouse on small"$
," image areas.  This criterion resolves azimuth by picking the most"$
," vertical magnetic field (either away or into the solar surface)."$
], azam_help_yellow() ]

'anti up down' eq lbl : message = [ message, '', $
[  QlabelQ+" is a criterion invoked interactively by the mouse on small"$
," image areas.  This criterion resolves azimuth by picking the magnetic"$
," field most horizontal to the solar surface."$
], azam_help_yellow() ]

'azimuth' eq lbl : message = [ message, '', $
[  QlabelQ+" is a criterion invoked interactively by the mouse on small"$
," image areas.  This criterion resolves azimuth by picking in the local"$
," solar frame the field azimuth most away from a 'center'."$
,""$
," Azimuth 'center' is set with option 'menu'-->'center'.  If you try"$
," 'azimuth'/'anti azimuth' without setting 'center', you will be"$
," prompted to set 'center'."$
], azam_help_yellow() ]

'anti azimuth' eq lbl : message = [ message, '', $
[  QlabelQ+" is a criterion invoked interactively by the mouse on small"$
," image areas.  This criterion resolves azimuth by picking in the local"$
," solar frame the field azimuth most toward a 'center'."$
,""$
," Azimuth 'center' is set with option 'menu'-->'center'.  If you try"$
," 'azimuth'/'anti azimuth' without setting 'center', you will be"$
," prompted to set 'center'."$
], azam_help_yellow() ]

'wads' eq lbl : message = [ message, '', $
[  QlabelQ+" is a criterion invoked interactively by the mouse on small"$
," image areas.  This criterion resolves azimuth from continuity of"$
," dot products of magnetic field point to point."$
,""$
," There are similar criteria 'wads' and 'smooth'.  'wads' is more bold"$
," because it does not perserve continuity to outside the boundary"$
," where it is invoked; 'smooth' perserves this continuity."$
," It is usually recomended to use 'wads' before 'smooth'."$
], azam_help_yellow() ]

'smooth' eq lbl : message = [ message, '', $
[  QlabelQ+" is a criterion invoked interactively by the mouse on small"$
," image areas.  (Its also in the main 'menu' to work on the whole"$
," data set).  This criterion resolves azimuth from continuity of"$
," dot products of magnetic field point to point."$
,""$
," There are similar criteria 'wads' and 'smooth'.  'wads' is more bold"$
," because it does not perserve continuity to outside the boundary"$
," where it is invoked; 'smooth' perserves this continuity."$
," It is usually recomended to use 'wads' before 'smooth'."$
], azam_help_yellow() ]

'no change' eq lbl : message = [ message, '', $
[  QlabelQ+" is a criterion invoked interactively by the mouse on small"$
," image areas.  This is the null azimuth resolution criterion.  Its permits,"$
," without changing azimuth resolution, painting over some part of the"$
," display with other data.  What is painted on the display images is picked"$
," by the three red buttons."$
,""$
," NOTE: if a red button is clicked the right yellow button"$
," automatically changes to 'no change'."$
], azam_help_yellow() ]

'average' eq lbl : message = [ message, '', $
[  QlabelQ+" is a criterion invoked interactively by the mouse on small"$
," image areas.  This is a null azimuth resolution criterion.  The average"$
," data values about the cursor is printed in the ASCII window."$
], azam_help_yellow() ]

'lock drag' eq lbl  or  'unlock drag' eq lbl: message = [ message, '', $
[" Clicking this button shows two states 'lock drag' and 'unlock drag'."$
,""$
," Drag means to hold a mouse button down and move the cursor about."$
," This is done a lot in AZAM and holding a button down is fatiguing."$
," With 'lock drag', a mouse button is not held down; the user releases"$
," the button and still has the drag effect.   Click a mouse button to"$
," release a locked drag."$
]]

'bulk' eq branch  and $
( aa.pp(aa.ww(0).drag).name eq lbl  or  aa.pp(aa.ww(1).drag).name eq lbl $
or  aa.pp(aa.ww(2).drag).name eq lbl ): $
message = [ message, '', $
[" Three red buttons pick what paints on the display where the mouse is"$
," drug.  The left red button picks what paints on the azimuth image; the"$
," right red button picks what paints on the incline image.  With right"$
," red button saying 'local incline' and with the local incline image"$
," displayed, you'll see the colors change with azimuth disambiguation."$
," With right red button set 'continuum' and with the local incline image"$
," displayed, you'll see the continuum paint on the image where the mouse"$
," is drug."$
,""$
," WARNING: this get confusing."$
,""$
," NOTE: clicking a red button automatically changes the right yellow"$
," button to the 'no change' criterion.  Dragging the right button then"$
," paints what's picked without changing the current azimuth resolution."$
]]

'set reference' eq lbl: message = [ message, '', $
[" Clicking of this button saves in core the current azimuth resolution"$
," as the 'reference'."$
,""$
," The starting 'reference' is the same as the 'original' azimuth" $
," at the beginning."$
,""$
," To recover an entire 'reference' click:"$
,"     'menu'-->'recover reference'"$
,""$
," To restore 'reference' on limited area:"$
,"     (1) Click left yellow button in button window till it says"$
,"         'reference'."$
,"     (2) Move cursor to area to be changed."$
,"     (3) Drag mouse over area to restore."$
]]

'* OTHER OP *' eq lbl : message = [ message, '', $
[" AZAM can load two data sets."$
,""$
," If a second data set is not already present; OTHER OP loads the second"$
," data set; there will be interactive dialog."$
,""$
," If a second data set is already present; OTHER OP will switch to"$
," working on the other data set."$
]]

'primer' eq lbl: message = [ message, '', $
[" Chooses another image for display.  The image names menu will pop"$
," up; click on your choice.  Another button set will pop up to"$
," asking where to display the image."$
,"" $
," WARNING - overwriting one the two main display images with a"$
," original (or reference) azimuth (or incline) image can be"$
," very confussing."$
]]

'clear' eq lbl: message = [ message, '', $
[" Paints back the current solution, free of all 'highlights'." $
," This will clear any painting you may have done with, say," $
," the continuum over an image of local azimuth." $
]]

'recomended modes' eq lbl: message = [ message, '', $
[" Configure AZAM with recomended modes displaying local frame azimuth"$
," and local frame incline."$
]]

'zoom' eq lbl: message = [ message, '', $
[" Zooms in area chosen by mouse with 6x6 screen pixels per image" $
," pixel.  Mouse functions (wads, etc.) work in zoom mode, too." $
,"" $
," To return to un-zoomed mode, click 'menu' and click 'EXIT ZOOM'." $
," There may be prompts for saving changes made, etc." $
,"" $
," WARNING: don't close zoom windows by hand." $
]]

'** EXIT ZOOM **' eq lbl: message = [ message, '', $
[" Returns to un-zoomed mode." $
," There may be prompts for saving changes made, etc." $
]]

'static window' eq lbl: message = [ message, '', $
[" Open a static (abandoned by AZAM) window to display an image." $
," Image displayed is picked from the names popup menu." $
,"" $
," Delete static windows by hand.  AZAM does not maintain static" $
," windows." $
]]

'profiles' eq lbl: message = [ message, '', $
[" Plots profiles of the image selected from image names popup menu."$
," This option works similar to the IDL library 'profiles.pro'." $
,""$
," Click left mouse button to cycle rows/column profiles."$
," Click right mouse button to exit."$
]]

'spectra' eq lbl: message = [ message, '', $
[" Reads ASP data files, displays polarization profiles a la aspview" $
," but w/o the grey-scale image; needs calibrated data file." $
]]

'flick' eq lbl: message = [ message, '', $
[" Flickers image requested in subsequent menu with the two main" $
," display windows." $
]]

'sub flick' eq lbl: message = [ message, '', $
[" Flickers sub-image requested in subsequent menu with the two main" $
," display windows.  Sub-image is tracked by the mouse." $
," (play to see how this works)" $
]]

'blink' eq lbl: message = [ message, '', $
[" Choose two images from the popup names menu, and it opens" $
," another window & blinks them." $
]]

'arrow points' eq lbl: message = [ message, '', $
[" Draws arrow points on images in the direction of the magnetic field."$
,"" $
," Arrow points may differ in color depending on field vector polarity."$
," They are shortened to give perspective of field incline."$
,"" $
," 'arrow points' are not kept current with azimuth resolution."$
,"" $
," Erased by 'clear' option."$
]]

'contour' eq lbl: message = [ message, '', $
[" Draws the contour level on images.  The user selects, from the image"$
," names popup menu, on what data the contour is run.  A window opens and"$
," selected data is displayed."$
,""$
," Click left button on point to spawn the contour."$
," Click center button to enter contour in popup text widget."$
," Click right button to exit contouring."$
,""$
," Erased by 'clear' or 'clear contour' option."$
]]

'clear contour' eq lbl: message = [ message, '', $
[" Clear contour set by 'contour' option."$
]]

'** REPLACE OP **' eq lbl: message = [ message, '', $
[" Starts all over with a new dataset." $
]]

'PostScript' eq lbl: message = [ message, '', $
[" Output PostScript files." $
]]

'UH potential field' eq lbl: message = [ message, '', $
[" Computes potential field using the longitudinal field as a" $
," boundary condition, then does the ambiguity resolution so as" $
," to agree most closely with this...you can then opt to have this" $
," resolution solution become the current solution." $
,"" $
," WARNING - the a__rgtasn and a__dclntn files" $
," must be present." $
]]

'up down everywhere' eq lbl: message = [ message, '', $
[" Do azimuth disambiguation so that the field is most vertical." $
]]

'center' eq lbl: message = [ message, '', $
[" Asks you to click on a magnetic field azimuth center (i.e., center" $
," of a nice round sunspot)." $
,"" $
," The center is used to disambiguate azimuth choosing what best" $
," agrees radially to/from the center." $
,"" $
," The user is given the option to disambiguate the entire data set" $
," based on the azimuth center." $
]]

'other azimuth' eq lbl: message = [ message, '', $
[" Whatever azimuth you've got, puts up the opposite choice for the" $
," entire data set." $
]]

'recover reference' eq lbl: message = [ message, '', $
[" Restore either original azimuth or what was last stored in memory" $
," via the 'set reference' button." $
]]

'recover original' eq lbl: message = [ message, '', $
[" Recover azimuth loaded in core at the beginning." $
]]

'write azimuth file' eq lbl: message = [ message, '', $
[" Output current azimuth resolution to a file.  The file will have the"$
," resolved line of sight azimuth values.  The azimuth resolution could"$
," later be recovered with option 'read azimuth file'."$
,""$
," The user is prompted for the output file name (defaults to 'AZAM')."$
]]

'BACKUP' eq lbl: message = [ message, '', $
[" Output current azimuth resolution to a file named 'BACKUP'.  The file"$
," will have the resolved line of sight azimuth values.  The azimuth"$
," resolution could later be recovered with option 'read azimuth file'."$
]]

'read azimuth file' eq lbl: message = [ message, '', $
[" Read 'AZAM' file or another line of sight azimuth file." $
]]

'update a_* files' eq lbl: message = [ message, '', $
[" Write out your new ambiguity resolution with what you've come" $
," up with." $
]]

'another inversion azimuth' eq lbl: message = [ message, '', $
[" Applies ambiguity solution from anohter inversion of the same op." $
]]

'palette' eq lbl: message = [ message, '', $
[" Invokes IDL library pocedure 'palette.pro'." $
,"" $
," This option lets the user tinker with the color table.  It is" $
," intended to aid the user to understand how ASP codes that use" $
," 'tvasp.pro' handle color." $
,"" $
," Restore tvasp color table with menu option 'tvasp color table'" $
]]

'tvasp color table' eq lbl: message = [ message, '', $
[" Restores tvasp color table; the table might have been changed" $
," with 'palette'" $
]]

'continuum' eq lbl: message = [ message, '', $
["Selects for an AZAM option the "+QlabelQ+" image."$
,""$
,"'continuum' is a gray scale image of maximum intensity observed in"$
,"ASP profiles."$
]]

'field' eq lbl: message = [ message, '', $
["Selects for an AZAM option the "+QlabelQ+" image."$
,""$
,"'field' is a gray scale image of the magnetic field strength."$
]]

'fill factor' eq lbl: message = [ message, '', $
["Selects for an AZAM option the "+QlabelQ+" image."$
,""$
,"'fill factor' is a gray scale image of the quiet sun filling factor."$
]]

'Doppler' eq lbl: message = [ message, '', $
["Selects for an AZAM option the "+QlabelQ+" image."$
,""$
,"'Doppler' is a color image of velocities along the line of sight."$
,"Velocity is determined from the spectra line centers.  Zero velocity"$
,"is the image average."$
]]

'sight azimuth' eq lbl: message = [ message, '', $
["Selects for an AZAM option the "+QlabelQ+" image."$
,""$
,"'sight azimuth' is a color image of the magnetic field azimuth in the"$
,"telescope frame.  Azimuth resolution is the current solution held"$
,"in core."$
]]

'local azimuth' eq lbl: message = [ message, '', $
["Selects for an AZAM option the "+QlabelQ+" image."$
,""$
,"'local azimuth' is a color image of the magnetic field azimuth in the"$
,"local solar frame.  Azimuth resolution is the current solution held"$
,"in core."$
]]

'ambig azimuth' eq lbl: message = [ message, '', $
["Selects for an AZAM option the "+QlabelQ+" image."$
,""$
,"'ambig azimuth' is a color image of the magnetic field azimuth in the"$
,"local solar frame.  Azimuth resolution is opposite everywhere from the"$
,"current solution held in core."$
]]

'original azimuth' eq lbl: message = [ message, '', $
["Selects for an AZAM option the "+QlabelQ+" image."$
,""$
,"'original azimuth' is a color image of the magnetic field azimuth in the"$
,"local solar frame.  Azimuth resolution is what was present when"$
,"the program began."$
]]

'reference azimuth' eq lbl: message = [ message, '', $
["Selects for an AZAM option the "+QlabelQ+" image."$
,""$
,"'reference azimuth' is a color image of the magnetic field azimuth in the"$
,"local solar frame.  Azimuth resolution is what was present in core when"$
,"'set reference' was last clicked.  If 'set reference' has not been"$
,"clicked, 'refer incline' and 'original incline' are the same."$
]]

'sight incline' eq lbl: message = [ message, '', $
["Selects for an AZAM option the "+QlabelQ+" image."$
,""$
,"'sight incline' is a color image of the magnetic field incline away from"$
,"the line of sight.  This image does not depend on azimuth resolution."$
]]

'local incline' eq lbl: message = [ message, '', $
["Selects for an AZAM option the "+QlabelQ+" image."$
,""$
,"'local incline' is a color image of the magnetic field incline away from"$
,"the zenith in the local solar frame.  Azimuth resolution is the current"$
,"solution held in core."$
]]

'ambig incline' eq lbl: message = [ message, '', $
["Selects for an AZAM option the "+QlabelQ+" image."$
,""$
,"'ambig incline' is a color image of the magnetic field incline away from"$
,"the zenith in the local solar frame.  Azimuth resolution is opposite"$
,"everywhere from the current solution held in core."$
]]

'original incline' eq lbl: message = [ message, '', $
["Selects for an AZAM option the "+QlabelQ+" image."$
,""$
,"'local incline' is a color image of the magnetic field incline away from"$
,"the zenith in the local solar frame.  Azimuth resolution is what was"$
,"present when the program began."$
]]

'refer incline' eq lbl: message = [ message, '', $
["Selects for an AZAM option the "+QlabelQ+" image."$
,""$
,"'local incline' is a color image of the magnetic field incline away from"$
,"the zenith in the local solar frame.  Azimuth resolution is what was"$
,"present in core when 'set reference' was last clicked.  If 'set reference'"$
,"has not been clicked, 'refer azimuth' and 'original azimuth' are the"$
,"same."$
]]

'flux' eq lbl: message = [ message, '', $
["Selects for an AZAM option the "+QlabelQ+" image."$
]]

'1-fill' eq lbl: message = [ message, '', $
["Selects for an AZAM option the "+QlabelQ+" image."$
]]

aa.pp(aa.extra0).name eq lbl: message = [ message, '', $
["Selects for an AZAM option the extra image which is presently named"$
,+QlabelQ+"."$
]]

aa.pp(aa.extra1).name eq lbl: message = [ message, '', $
["Selects for an AZAM option the extra image which is presently named"$
,+QlabelQ+"."$
]]

else: message = [ message, '', $
[" We are sorry; help text not yet written."$
]]

end
				    ;Print message in window.
azam_help_win, aa, message

end
;-----------------------------------------------------------------------------
;
;	procedure:  azam_help
;
;	purpose:  print help info about azam procedure
;
;-----------------------------------------------------------------------------
pro azam_help, labels, branch, aa, arg2, arg3, arg4, arg5 $
, title=title, xpos=xpos, ypos=ypos, rows=rows, bLength=bLength, bWidth=bWidth
				    ;Check number of parameters.
if n_params() eq 0 then begin
	azam_help_usage
	return
end
				    ;Pick branch.
case branch of
				    ;Type help message.
'type': begin
	print & print & print
	print,"///////////////////////////////////////////////////////////////"
	print,title+":"
	print
	for i=0,n_elements(arg2)-1 do  print, arg2(i)
	end
				    ;Open window and print message.
'window': $
	azam_help_win, aa, arg2
				    ;Select help from button window.
else:	azam_help_button, labels, branch, aa $
	, xpos=xpos, ypos=ypos, rows=rows, bLength=bLength, bWidth=bWidth
     
end

end
