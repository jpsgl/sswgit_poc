; Routine to extract the fitted-only elements of an instrument
; frame map, then put them into an array without non-fitted
; points

pro azam_subarea, orig_map, xll, yll, xur, yur, whrfit, kk, sub_map

  temp = orig_map(xll:xur, yll:yur)
  sub_map = temp(whrfit)
  
  ; Set the structure variables
  
  return
end
