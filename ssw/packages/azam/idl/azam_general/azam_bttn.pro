pro azam_bttn, aa
;+
;
;	procedure:  azam_bttn
;
;	purpose:  display buttons for 'azam.pro'
;
;	author:  paul@ncar, 6/93	(minor mod's by rob@ncar)
;
;==============================================================================
;
;       Check number of parameters.
;
if n_params() ne 1 then begin
	print
	print, "usage:	azam_bttn, aa"
	print
	print, "	Display buttons for 'azam.pro'."
	print
	print, "	Arguments"
	print, "		aa	- azam data set structure"
	print
	print, "	Keywords"
	print, "		(none)"
	print
	return
endif
;-
				    ;Recall some display parameters.
t      = aa.t
blt    = aa.blt
bwd    = aa.bwd
btm    = aa.btm
csiz   = aa.csiz
xdim   = aa.xdim
ydim   = aa.ydim
				    ;Form image of button.
common common_bttn, button, xbutton, rbutton, bbutton, mouse
if  n_elements( button ) eq 0  then begin
	xx = lindgen(bwd,bwd)
	yy = xx/bwd
	xx = xx-yy*bwd
	brd = .5*(bwd-1)
	xx = xx-brd
	yy = yy-brd
	dot = replicate(aa.white,bwd,bwd)
	dot(where(round(xx*xx+yy*yy) ge round(brd*brd))) = aa.black
	button = replicate( aa.black, blt, bwd )
	button(0:bwd-1,*) = dot
	button(blt-bwd:blt-1,*) = dot
	button(bwd/2:blt-bwd/2,1:bwd-2) = aa.white

	whr = where(button eq aa.white)
	rbutton = button
	bbutton = button
	rbutton(whr) = aa.red
	bbutton(whr) = aa.blue

	xbutton = button
	ii = lindgen(blt)
	jj = (ii*bwd)/blt
	xbutton(blt*jj+ii) = aa.black
	xbutton(blt*(bwd-1-jj)+ii) = aa.black

	mouse = replicate( aa.black, 3*blt, bwd )
	mouse(0:bwd-1,*) = dot
	mouse(3*blt-bwd:3*blt-1,*) = dot
	mouse(bwd/2:3*blt-bwd/2,1:bwd-2) = aa.white
	mouse(blt,*) = aa.black
	mouse(2*blt,*) = aa.black
	mouse(where(mouse eq aa.white)) = aa.yellow
end
				    ;Set, erase, and show button window.
wset, aa.winb  &  erase, aa.black  &  wshow, aa.winb

				    ;Display blank buttons.
tv, button,      0, 2*bwd
tv, button,    blt, 2*bwd
tv, button,  2*blt, 2*bwd
tv, button,  3*blt, 2*bwd
tv, button,  4*blt, 2*bwd
tv, button,  4*blt,     0
tv, rbutton, 3*blt,   bwd
tv, rbutton, 4*blt,   bwd
tv, rbutton, 3*blt,     0
tv, bbutton, 5*blt,     0
tv, mouse,       0,   bwd
				    ;Scratch some zoom buttons.
if aa.zoom eq 0 then begin
	tv, button, 5*blt,   bwd
	tv, button, 5*blt, 2*bwd
end else begin
	tv, xbutton, 5*blt,   bwd
	tv, xbutton, 5*blt, 2*bwd
end
				    ;Print black button text.
axa = (['1x1','2x2','4x4','8x8','16x16'])(aa.pwr)
xaway =  '^x' 
xyouts, /device, align=.5, charsize=csiz, color=aa.black $
, [ 0, 1, 2, 0, 1, 2, 4, 3, 4, 5, 5 ]*blt+blt/2 $
, [ 1, 1, 1, 2, 2, 2, 0, 2, 2, 1, 2 ]*bwd+btm $
, [ aa.cri, aa.anti, aa.prime, axa, aa.lock, aa.mgfy.bttn $
;, 'menu', aa.hilite, 'set reference', 'BACKUP', '* OTHER OP *' ]
, 'menu', aa.hilite, 'set reference', 'BACKUP', '(not used)' ]

				    ;Print white button text.
xyouts, /device, align=.5, charsize=csiz, color=aa.white $
, [ 0, 1, 2, 3, 4, 3, 5 ]*blt+blt/2 $
, [ 0, 0, 0, 1, 1, 0, 0 ]*bwd+btm $
, [ 'left', 'center', 'right' $
, aa.pp(aa.ww(0).drag).name $
, aa.pp(aa.ww(1).drag).name $
, aa.pp(aa.ww(2).drag).name $
, '** HELP **' ]
				    ;Wait till no buttons pressed on 
				    ;button window.
repeat  cursor, xx, yy, /device, /nowait  until  !err eq 0

end
