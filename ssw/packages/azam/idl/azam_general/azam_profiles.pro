pro azam_profiles, hh, aa
;+
;
;	procedure:  azam_profiles
;
;	purpose:  do image profiles for 'azam.pro'
;
;	author:  paul@ncar, 11/93	(minor mod's by rob@ncar)
;		 (heavy revised  $IDL_DIR/lib/userlib/porfiles.pro )
;
;=============================================================================
;
;       Check number of parameters.
;
if n_params() eq 0 then begin
	print
	print, "usage:	azam_profiles, hh, aa"
	print
	print, "	Do image profiles for 'azam.pro'."
	print
	print, "	Arguments (input)"
	print, "		hh	- highlight structure"
	print, "		aa	- azam data set structure"
	print
	return
endif
;-
				    ;Popular parameters.
t      = aa.t
xdim   = aa.xdim
ydim   = aa.ydim
txdim  = t*xdim
tydim  = t*ydim
win0   = aa.ww(0).win
win1   = aa.ww(1).win
win2   = aa.ww(2).win
				    ;Prompt for azam image name.
slab = pop_cult( azam_image_names(aa), /string $
, title='Select image to profile' $
, help='azam', arg0='names', arg1=aa, winfree=aa.winfree )
if slab eq '** DISMISS **' then return

				    ;Get parameter number.
iprm = (where( slab eq aa.pp.name ))(0)

if   iprm ne aa.ww(0).iprm  and  iprm ne aa.ww(1).iprm $
and  iprm ne aa.ww(2).iprm  then begin

				    ;Get image for display.
	img3 = azam_image( slab, aa, hh )

                                    ;Open window to display data.
	if aa.winfree then begin
        	window, /free, xsize=aa.t*aa.xdim+3*aa.bwd $
		, ysize=300 > aa.t*aa.ydim $
        	, title=aa.pp(iprm).name
	end else begin
        	window, /free, xsize=aa.t*aa.xdim+3*aa.bwd $
		, ysize=300 > aa.t*aa.ydim $
       		, xpos=100, ypos=40 $
        	, title=aa.pp(iprm).name
	end
        w3 = !d.window

	tv, img3, 0, 0
	azam_c_bar, w3, iprm, aa
end
				    ;Set plot charsize.
charsize = aa.csiz
				    ;Initialize some variables.
xprt = -1
yprt = -1
vecx = findgen(xdim)
vecy = findgen(ydim)
				    ;Cross length.
tickl = 0.025
				    ;Compute extrema.
minv = min( aa.pp(iprm).value, max=maxv )
				    ;Instructions.
azam_message, aa, 'left(rows/columns); center(); right(exit)'

				    ;Open window for profiles.
if aa.winfree then begin
	window, /free, xsize=640, ysize=512
end else begin
	window, /free, xsize=640, ysize=512, xpos=1144-640, ypos=30
end
w1 = !d.window
window, /free, xsize=640, ysize=512, /pixmap
w2 = !d.window
if n_elements(w3) then  wshow, w3
				    ;Mode = 0 for rows, 1 for cols
mode = 0
old_mode = -1
				    ;Infinite loop to right mouse button
				    ;is pressed.
while 1 do begin
				    ;Do computed field cursor.
	repeat begin
		azam_cursor, aa, xerase, yerase $
		,    win0,    win1,    win2,      w3, undef $
		,                               img3, undef $
		, eraser0, eraser1, eraser2, eraser3, undef $
		, x, y, state $
		, likely=likely $
		, maybe=w3 $
		, no_ttack=0 $
		, delay=.2
				    ;Honor right buttom click on button window.
		if aa.bs eq 4 then begin
			state=4
			x = 0
		end
	end until x ne -1

	if state ne 0 then begin

		old_mode = -1
				    ;Wait till mouse buttons are up.
		repeat  cursor, xx, yy, /device, /nowait  until  !err eq 0

		case state of
				    ;Toggle mode.
		1: mode = 1-mode
				    ;Return if left button was pressed.
		4: begin
			wset, aa.winb  &  erase, aa.red
			wdelete, w1, w2
			if n_elements(w3) then  wdelete, w3
			wset, win0  &  tv, aa.ww(0).img, 0, 0
			wset, win1  &  tv, aa.ww(1).img, 0, 0
			wset, win2  &  tv, aa.ww(2).img, 0, 0
			return
			end
		else:
		end
	end
				    ;Toggle row/column scales.
	if mode ne old_mode then begin
		xprt = -1
		old_mode = mode
		if mode then begin
			wset, w2
			plot, [minv,maxv], [0,ydim-1], /nodata $
			, title=slab+' Column Profile' $
			, background=aa.white, color=aa.black $
			,charsize=charsize
			wset, w1
			plot, [minv,maxv], [0,ydim-1], /nodata $
			, background=aa.white, color=aa.black $
			, charsize=charsize
			crossx = [-tickl, tickl]*(maxv-minv)
			crossy = [-tickl, tickl]*ydim
		end else begin
			wset, w2
			plot, [0,aa.xdim-1], [minv,maxv], /nodata $
			, title=slab+' Row Profile' $
			, background=aa.white, color=aa.black $
			, charsize=charsize
			wset, w1
			plot, [0,aa.xdim-1], [minv,maxv], /nodata $
			, background=aa.white, color=aa.black $
			, charsize=charsize
			crossx = [-tickl, tickl]*xdim
			crossy = [-tickl, tickl]*(maxv-minv)
		end
	end
				    ;Check if image coordinates changed.
	if  xprt ne x  or  yprt ne y  then begin

				    ;Save image coordimates.
		xprt = x
		yprt = y
				    ;Refresh window.
		wset, w1
		device, copy=[0,0,640,512,0,0,w2]

				    ;Plot column or row data.
		ixy  = aa.pp(iprm).value(x,y)
		if mode then begin
			plots, aa.pp(iprm).value(x,*), vecy, color=aa.black
			plots, crossx+ixy, [y,y],   color=aa.red
			plots, [ixy,ixy], crossy+y, color=aa.red
		end else begin
			plots, vecx, aa.pp(iprm).value(*,y), color=aa.black
			plots, [x,x], crossy+ixy,   color=aa.red
			plots, crossx+x, [ixy,ixy], color=aa.red
		end
	end
end

end
