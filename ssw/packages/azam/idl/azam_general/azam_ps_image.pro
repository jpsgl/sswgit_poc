pro azam_ps_image_usage
;+
;
;	procedure:  azam_ps_image
;
;	purpose:  output PostScript image of a_* file
;
;	routines:  azam_ps_image_usage
;	azam_ps_line_color
;	azam_ps_image_arrow
;	azam_ps_image_frame
;	azam_ps_key_set
;	azam_ps_image
;
;	authors:  paul@ncar 6/95  rob@ncar 1/93
;
;==============================================================================
;
if 1 then begin
	print
	print, "usage:  azam_ps_image, aspdata, c"
	print
	print, "	Output PostScript image of a_* file."
	print
	print, "Arguments:"
	print
	print, "	  aspdata	- aspdata can take three forms:"
	print, "			  (1) String -"
	print, "			  	path to a_* file."
	print, "			  (2) Integer or floating point -"
	print, "				neighbor interpolated array"
	print, "				consistant with already"
	print, "				computed c structure."
	print, "			  (3) Byte -"
	print, "				neighbor interpolated array"
	print, "				consistant with already"
	print, "				computed c structure."
	print, "				NOTE, tvasp is not called"
	print, "				if aspdata is a byte image;"
	print, "				color table is not modified."
	print, "		c	- ASP data set structure of the"
	print, "			  nieghbor interpolated type."
	print, "Keywords:"
	print, "	     bw         - output file in no color 0-255"
	print, "			  black & white."
	print, "	encapsulated	- set for encapsulated PostScript"
	print, "	    tplate	- template structure"
	print, "	    		  see ps_tplate.pro"
	print, "	    whole	- Set if aspdata is integer or"
	print, "	    		  floating and data is all points"
	print, "	    		  (def: assume data valid for solved"
	print, "	    		  points only)."
	print, "	  landscape	- Open new file for PostScript output."
	print, "			  Input this keyword as a string used"
	print, "			  as path to the new file.  The file"
	print, "			  opens in 11.x8.5 landscape mode."
	print, "			  (Def, don't open file)"
	print, "	   portrait	- Open new file for PostScript output."
	print, "			  Input this keyword as a string used"
	print, "			  as path to the new file.  The file"
	print, "			  opens in 8.5x11. portrait mode."
	print, "			  (Def, don't open file)"
	print, "	  ref_frame	- Selects reference frame used to"
	print, "			  neighbor interpolated images."
	print, "			  Recognized values are 'instrument',"
	print, "			  'zenith' or 'disk'.  This keyword"
	print, "			  is used only if azam_ps_image reads the"
	print, "			  data from a file.  (Def 'zenith')"
	print, "			  (First character only 'i', 'z'"
	print, "			  or 'd' has to be correct)."
	print, "	     bkg	- Value set in data array where there"
	print, "			  is no data.  This keyword is used"
	print, "			  only if azam_ps_image reads the data"
	print, "			  from a file.  (Def 0.0)"
	print
	print, "           mm_per_inch  - input scaler:" 
	print, "                                Sun megameters per printer"
	print, "                                page inch.  Ignored if"
	print, "                                keyword mm_inch_std is input."
	print, "                          output scaler:"
	print, "                                If undefined set scale so that"
	print, "                                one dimension just fits space"
	print, "                                available.  If keyword"
	print, "                                mm_inch_std is input,"
	print, "                                mm_per_inch will be set to"
	print, "                                actual value used."
	print, "           mm_inch_std  - input this keyword to get standard"
	print, "                          image proportions based on the one"
	print, "                          landscape image per page format."
	print, "                          Input value is used for megameters"
	print, "                          per printer page inch that comes"
	print, "                          out on the one image format."
	print, "	    origin_xy	- input two vector:"
	print, "				[x_origin,y_origin] position"
	print, "				in inches of available area"
	print, "				for image on page"
	print, "				(def [0.,0.])"
	print, "			  output two vector:"
	print, "				Origin adjusted to center on"
	print, "				space available."
	print, "	    inches_xy	- input two vector:"
	print, "				[x_inches,y_inches] size in"
	print, "				inches available for plotting"
	print, "				an image (def: [8.5,11.] or"
	print, "				[11.,8.5])"
	print, "			  output two vector:"
	print, "				[x_inches,y_inches] size in"
	print, "				inches to be used for plotting"
	print, "				an image."
	print, "		LLxy	- input two vector:"
	print, "				[X0,Y0] lower left index"
	print, "				for sub arrays to plot"
	print, "			  output two vector:"
	print, "				Round off if input; otherwise"
	print, "				determine appropriate to info"
	print, "				available."
	print, "		URxy	- input two vector:"
	print, "				[X1,Y1] upper right index"
	print, "				for sub arrays to plot."
	print, "				Ignored if LLxy is not input."
	print, "			  output two vector:"
	print, "				Round off if input; it may be"
	print, "				reset if space is not"
	print, "				available depending on"
	print, "				mm_per_inch scale."
	print, "	       ABOUTxy	- input two vector:"
	print, "				[XA,YA] index in images about"
	print, "				where to plot."
	print, "				Ignored if LLxy is input"
	print, "				(def: image center)"
	print, "	    white_bkg	- Set to color missing data areas"
	print, "			  white.  This keyword is ignored if"
	print, "			  input data is a 2D byte array."
	print, "			  (Def: treat background a though it"
	print, "			  were data)"
	print, "	    black_bkg	- Set to color missing data areas"
	print, "			  black.  This keyword is ignored if"
	print, "			  input data is a 2D byte array."
	print, "			  (Def, treat background a though it"
	print, "			  were data)"
	print, "	    gray	- Set for tvasp.pro to use gray"
	print, "			  scale portion of color table."
	print, "			  (Def, use color scale)" 
	print, "	    invert	- Set fot tvasp.pro to invert"
	print, "			  color table.  (Def, don't invert)."
	print, "	    wrap	- Set for tvasp.pro to wrap color"
	print, "			  table.  (Def, don't wrap)"
	print, "	    min		- Data value tvasp.pro scales to low"
	print, "			  color table index.  (Def, use data"
	print, "			  minimum)"
	print, "	    max		- Data value tvasp.pro scales to high"
	print, "			  color table index.  (Def, use data"
	print, "			  maximum)"
	print, "	   title	- Title string for image."
	print, "	symbolsize	- Global symbol size factor effecting"
	print, "			  labels, numbers & arrow point"
	print, "			  lengths"
	print, "	frame		- Set to draw box about image."
	print, "	tick		- Set and if frame is set draw tick"
	print, "			  marks"
	print, "	xnumber		- Set to number x azis; value is used"
	print, "			  as an additional factor to"
	print, "			  symbolsize."
	print, "	ynumber		- Set to number y azis; value is used"
	print, "			  as an additional factor to"
	print, "			  symbolsize."
	print, "	arrow		  set to draw arrow points."
	print, "			  Input may be scaler or a two vector."
	print, "			  Values effects color."
	print, "	t_arrow		  a special patch set to draw arrows"
	print, "			  on a large poster image.  Presently"
	print, "			  used only by paul's ps_1incl.pro"
	print, "	a_length	  scale factor for arrow point size."
	print, "	ambigs		  set to draw ambigs lines."
	print, "			  Value effects color."
	print, "	reversal	  set to draw zenith frame reversal."
	print, "			  Value effects color; see below."
	print, "	sight_reversal	  set to draw instrument frame"
	print, "			  Value effects color."
	print, "	azm		- input/output instrument azimuth."
	print, "			  Read from c.dty+'a_azm' as needed."
	print, "	psi		- input/output instrument psi angle."
	print, "			  Read from c.dty+'a_psi' as needed."
	print, "	azimuth		- input/output zenith frame azimuth."
	print, "			  Read from c.dty+'a_1azm' as needed."
	print, "	zenith		- input/output zenith angle."
	print, "			  Read from c.dty+'a_1incl' as needed."
	print, "	whrA		- input where array to connect"
	print, "			  adjacent points by vectors."
	print, "	   colA		- input color for vectors drawn with"
	print, "			  whrA array."
	print, "	whrB		- input where array to connect"
	print, "			  adjacent points by vectors."
	print, "	   colB		- input color for vectors drawn with"
	print, "			  whrB array."
	print, "	whrC		- input where array to connect"
	print, "			  adjacent points by vectors."
	print, "	   colC		- input color for vectors drawn with"
	print, "			  whrC array."
	print
	return
endif
;-
end
;-----------------------------------------------------------------------------
;
;
;
;
;________________	Procedure to output a ASP image to a PostScript
;| procedure:	|	file.
;| azam_ps_image.pro	|
;----------------	Examples:  ps_cct.pro  ps_iiii.pro
;	|			   ps_zen_fld.pro  azam_ps.pro
;	|
;	|		Author: paul@ncar 6/95
;	|
;_______V________	_________________	If keyword landscape/portrait 
;|Open file ?	| yes	|Open file	|	is set a PostScript file is
;| (landscape) 	|------>| (landscape)	|	opened.  If landscape/portrait
;| (portrait)	|	| (portrait)	|	is a string, it is used for
;----------------	| (bw)		|	the file path (default path is
;	| no		-----------------	'rm_me.ps').
;	|			|
;	|			|		If bw keyword is set the device
;	|			|	   	is opened for black & white
;	|<-----------------------		lithograph; i.e., no color ~
;	|					'device, color=0, ...' 
;	|
;_______V________	_________________	Read a_* file and form 2D
;|Is aspdata	| yes	|Read a_* file	|	asp data array.  The structure
;|file path	|------>| (bkg)		|	(2nd argument) is computed
;|string ?	|	| (ref_frame)	|	if needed.
;----------------	-----------------
;	| no			|		If bkg keyword is set its
;	| 			|		value is used for background
;_______V_______________	|		data (def 0.).
;|Program bombs if asp |	|
;|structure not input  |	|		Keyword ref_frame selects
;-----------------------	|		reference frame; 'instrument',
;	|			|		'zenith' or 'disk'.  If frame
;	|<-----------------------		is not already set in asp
;	|					structure the default is
;	|					'zenith'.  Only the first
;	|					is checked: 'i', 'z' or 'd'.
;	|
;_______V________	_________________	Scale to byte image using
;|Is data 2D	| yes	|Byte scale the |	'tvasp.pro'.
;|non byte	|------>|image with	|
;|array ?	|	|'tvasp.pro'	|	If keyword bw is set the 
;----------------	| (bw)		|	0B_255B black_white scale
;	| already	| (gray)	|	is used; otherwise the tvasp
;	| byte		| (min)		|	color table is loaded to which
;	|		| (max)		|	the image is scaled.
;	|		| (invert)	|
;	|		| (wrap)	|	The gray keyword scales the
;	|		| (white_bkg)	|	image to the black_white 
;	|		| (black_bkg)	|	portion of the tvasp color
;	|		| (whole)	|	table (gray is ignored if
;	|		-----------------	keyword bw is set).
;	|			|
;	|			|		Keywords min & max are data
;	|<-----------------------		values that correspond to the
;	|					color table limits (def: use
;	|					data min & max).
;	|
;	|					Keyword invert inverts the
;	|					data corresponding to the
;	|					color table; black_white
;	|					becomes white_black; yellow_
;	|					red_magenta_blue_cyan becomes
;	|					cyan_blue_magenta_red_yellow.
;	|
;	|					Keyword wrap returns a byte
;	|					image that appears wraped
;	|					around.  Gray scale is returned
;	|					black_white_black.  Color
;	|					scale is returned green_yellow_
;	|					red_magenta_blue_cyan_green.
;	|
;	|					Keyword white_bkg sets array
;	|					elements to the white index
;	|					for locations where there is
;	|					no data (see whole below).
;	|
;	|					Keyword black_bkg sets array
;	|					elements to the black index
;	|					for locations where there is
;	|					no data (see whole below).
;	|
;	|					Keyword whole applies only
;	|					if the input data type is
;	|					2D non byte numeric.  Set  
;	|					if data is present for the
;	|					whole image and not just
;	|					solved points.
;	|
;______	V________ Calculate image size and bounds; 'azam_ps_asp_size.pro' is called.
;|Size the image| 
;| (mm_per_inch)| mm_per_inch	- Input scaler:
;| (mm_inch_std)|			Megameters on the Sun corresponding
;| (origin_xy)	|			to inches on the printer page.
;| (hold_origin)|			Ignored if mm_inch_std is input.
;| (inches_xy)	|		  Output scaler:
;| (LLxy)	|			If undefined on input set scale so
;| (URxy)	|			that one demension just fits space
;| (ABOUTxy)	|			available on printed page.
;----------------			If mm_inch_std input, mm_per_inch
;	|				is output with the actual value used.
;	|
;	|	   mm_inch_std  - Input this keyword to get standard image
;	|			  proprotions based on the one image per page
;	|			  format.  Input value is megameters per inch
;	|			  that prints on standard one image per
;	|			  landscape page format.
;	|
;	|	     origin_xy	- Input two vector:
;	|				[x_origin,y_origin] position in inches
;	|				of lower left corner of area available
;	|				for image on page (def [0.,0.]).
;	|			  Output two vector:
;	|				Origin adjusted for image centered on
;	|				space available.
;	|
;	|	   hold_origin	- Set to keep keyword origin_xy i/o the same.
;	|
;	|	     inches_xy	- Input two vector:
;	|				[x_inches,y_inches] size in inches
;	|				available for plotting the image
;	|				(def: [8.5,11.] or [11.,8.5])
;	|			  Output two vector:
;	|				[x_inches,y_inches] size in inches
;	|				actually used to plot the image.
;	|
;	|		  LLxy	- Input two vector:
;	|				[X0,Y0] lower left index of sub array
;	|				to plot.
;	|			  Output two vector:
;	|				If defined on input, output is rounded
;	|				to long integers.  If undefined,
;	|				determine appropriate to info
;	|				available.
;	|
;	|		  URxy	- Input two vector:
;	|				[X1,Y1] upper right index of sub array
;	|				to plot.  Ignored if LLxy is not input.
;	|			  Output two vector:
;	|				If defined on input, output is rounded
;	|				to long integers.  It may be reset
;	|				if space is not available depending on
;	|				mm_per_inch scale.  If undefined,
;	|				determine appropriate to info
;	|				available.
;	|
;	|	       ABOUTxy	- Input two vector:
;	|				[XA,YA] index in the image of about
;	|				where to plot.  Ignored if LLxy is
;	|				input.  (Def: image center.)
;_______V________
;|Output image	|
;|to PostScript	|
;----------------
;	|
;	|
;_______V________	Ambigs lines are drawn if keyword ambigs is set.
;|Draw ambigs	|	Calculation require line of sight azimuth
;|lines		|	array 'azm'.   Line of sight azimuth array
;| (ambigs)	|	can be input by keyword '...,azm=azm,...'.
;| (azm)	|	As needed 'azm' is read from file
;| (bw)		|	"c.dty+'a_azm'" and returnable by keyword.
;| (gray)	|
;----------------	For bw=1         line color is 'white'.
;	|		For bw=0, gray=1 line color is 'blue'.
;	|		For bw=0, gray=0 line color is 'black'.
;	|		Seek other info on how to change line color.
;	|
;_______V________	Reversal lines are drawn if keyword reversal is set.
;|Draw reversal	|	Calculation require zenith angle array
;|lines		|	'zenith'.  Zenith angle array can be input by
;| (reversal)	|	keyword '...,zenith=zenith,...'.  As needed
;| (zenith)	|	'zenith' is read from file "c.dty+'a_1incl'"
;| (bw)		|	and returnable by keyword.
;| (gray)	|
;----------------	For bw=1         line color is 'white'.
;	|		For bw=0, gray=1 line color is 'yellow'.
;	|		For bw=0, gray=0 line color is 'black'.
;	|		Seek other info on how to change line color.
;	|
;_______V___________	Sight reversal lines are drawn if keyword
;|Draw sight	   |	sight_reverasl is set.  Calculation require
;|reversal lines   |	psi angle array 'psi'.  Psi angle array can be input
;| (sight_reversal)|	by keyword '...,psi=psi,...'.  As needed
;| (psi)	   |	'psi' is read from file "c.dty+'a_psi'"
;| (bw)		   |	and returnable by keyword.
;| (gray)	   |
;-------------------	For bw=1         line color is 'white'.
;	|		For bw=0, gray=1 line color is 'yellow'.
;	|		For bw=0, gray=0 line color is 'black'.
;	|		Seek other info on how to change line color.
;	|
;_______V________	Arrow points are drawn if keyword arrow is set.
;|Draw arrow	|	Calculation require local frame 'azimuth' and
;|points	|	'zenith' angle arrays.  Arrays can be input
;| (arrow)	|	by keywords '...,azimuth=azimuth,zenith=zenith,...'.
;| (azimuth)	|	As needed the arrays are read from
;| (zenith)	|	files "c.dty+'a_1azm'" & "c.dty+'a_1incl'"
;| (a_length)	|	and returnable by keywords.
;| (bw)		|
;| (gray)	|	Set keyword 'a_length' to scale arrow point length.
;----------------	Arrow point length will be a_length*.125 inches.
;	|
;	|		For bw=1         point color 'white'  on plus  field.
;	|		For bw=1         point color 'black'  on minus field.
;	|		For bw=0, gray=1 point color 'red'    on plus  field.
;	|		For bw=0, gray=1 point color 'yellow' on minus field.
;	|		For bw=0, gray=0 point color 'white'  on plus  field.
;	|		For bw=0, gray=0 point color 'black'  on minus field.
;	|		Seek other info on how to change color.
;	|
;_______V________	Draw lines based on where() array 'whrA'.  Vectors 
;|Draw where()	|	are drawn connecting adjacent points pointed to
;|array lines	|	by the where() array.  Example to draw 1000 gauss
;| (whrA)	|	field coutours (assume c already defined):
;| (colA)	|
;| (bw)		|	whrA=cont(a_image(c.dty+'a_fld',c),c.sxy,1000.)
;| (gray)	|
;----------------	For bw=1         line color is 'white'.
;	|		For bw=0, gray=1 line color is 'red'.
;	|		For bw=0, gray=0 line color is 'white'.
;	|		Seek other info on how to change line color, colA.
;	|
;_______V________	Draw lines based on where() array 'whrB'.  Same as
;|Draw where()	|	'whrA' except for default line color.
;|array lines	|
;| (whrB)	|	For bw=1         line color is 'gray'.
;| (colB)	|	For bw=0, gray=1 line color is 'blue'.
;| (bw)		|	For bw=0, gray=0 line color is 'gray'.
;| (gray)	|	Seek other info on how to change line color, colB.
;----------------
;	|
;	|
;_______V________	Draw lines based on where() array 'whrC'.  Same as
;|Draw where()	|	'whrA' except for default line color.
;|array lines	|
;| (whrC)	|	For bw=1         line color is 'white'.
;| (colC)	|	For bw=0, gray=1 line color is 'yellow'.
;| (bw)		|	For bw=0, gray=0 line color is 'black'.
;| (gray)	|	Seek other info on how to change line color, colC.
;----------------
;	|
;	|
;_______V________	_________________	Draw box about the image
;|Draw frame ?	| yes	|Draw image	|	if keyword frame is set.
;| (frame)	|------>|frame and scale|
;----------------	| (frame)	|	Draw tick marks if keyword
;	|		| (tick)	|	tick is set.  Ignored unless
;	|		| (xnumber)	|	frame is set.
;	|		| (ynumber)	|
;	|		| (symbolsize)	|	Set keyword xnumber to print
;	|		-----------------	image scale below the image.
;	|			|		Ignored unless tick is set.
;	|			|
;	|			|		Set keyword ynumber to print
;	|<-----------------------		image scale to left of image.
;	|					Ignored unless tick is set.
;	|
;	|					Set keyword symbolsize to
;	|					value used to scale printed 
;	|					characters.
;	|
;_______V________	Set keyword title to character string that prints
;|Print title	|	above top center of the image.
;| (title)	|
;| (symbolsize)	|	Set keyword symbolsize to value used to scale printed
;----------------	characters.
;	|
;	|
;	V
;     done
;
;
;-----------------------------------------------------------------------------
;
;	function:  azam_ps_image_color
;
;	purpose:  get tvasp color index from string or use default index.
;
;------------------------------------------------------------------------------
function azam_ps_line_color, keycolor, bw, bwcolor, gray, ongray, offgray

				    ;Common with 'tvasp.pro'.
@tvasp.com
				    ;Determine input type.
ndim = n_dims( keycolor, type=type )

				    ;Return byte type directly.
if type eq 'Byte' then  return, keycolor

				    ;Returns for black & white.
if bw then begin
	if type eq 'String' then   str=keycolor  else  str=bwcolor
	case str of
		'white'	: return, 255B
		'gray'	: return, 128B
		'black'	: return, 0B
		else	: return, 255B
	end
end
				    ;Use string if supplied else default
				    ;depending on gray scale.
if type eq 'String' then begin
	str = keycolor
end else begin
	if keyword_set(gray) then  str=ongray  else  str=offgray
end
				    ;Set tvasp.com if needed.
if n_elements(tvasp) eq 0 then  tvasp, lindgen(2,2), /notv

				    ;Match to tvasp colors.
case str of
	'white'	: return, tvasp.ix_white
	'yellow': return, tvasp.ix_yellow
	'red'	: return, tvasp.ix_red
	'green'	: return, tvasp.ix_green
	'blue'	: return, tvasp.ix_blue
	'gray'	: return, tvasp.ix_gray
	'black'	: return, tvasp.ix_black
	else	: return, tvasp.ix_black
end

end
;------------------------------------------------------------------------------
;
;	procedure:  azam_ps_image_arrow
;
;	purpose:  draw arrow points on PostScript images 
;
;------------------------------------------------------------------------------
pro azam_ps_image_arrow, c, bw $
, arrow, gray, a_length, symsz, azimuth, zenith, tpl $
, origin_xy, inches_xy, LLxy=LLxy, URxy=URxy, t_arrow=t_arrow

				    ;Set some local variables.
xorg=origin_xy(0)  &  xinches=inches_xy(0)  &  xx0=LLxy(0)  &  xx1=URxy(0)
yorg=origin_xy(1)  &  yinches=inches_xy(1)  &  yy0=LLxy(1)  &  yy1=URxy(1)

				    ;Set arrow point color.
upc = azam_ps_line_color( arrow(                  0) $
	, bw, 'white', gray,    'red', 'white' )
dnc = azam_ps_line_color( arrow(n_elements(arrow)-1) $
	, bw, 'black', gray, 'yellow', 'black' )

				    ;If undefined get azimuth and zenith angle.
if n_elements(azimuth) ne c.xdim*c.ydim $
then  azimuth = azam_a_image( c.dty+'a_1azm',  c )

if n_elements(zenith) ne c.xdim*c.ydim $
then  zenith = azam_a_image( c.dty+'a_1incl', c )

				    ;Array with data locations.
ifdata = lonarr(c.xdim,c.ydim)
ifdata(c.sxy) = 1

if n_elements(t_arrow) eq 0 then begin

				    ;Arrow point length in inches.
if n_elements(a_length) eq 1 then  alth=a_length  else  alth=1.
alth = alth*symsz*1./8.
				    ;Loop over two spacial dimensions.
for  ix = alth/2., xinches-alth/2., 1.2*alth  do begin
for  iy = alth/2., yinches-alth/2., 1.2*alth  do begin

	xc = round(xx0+(xx1-xx0+1)*ix/xinches)
	yc = round(yy0+(yy1-yy0+1)*iy/yinches)

	if  ifdata(xc,yc)  then begin

				    ;Compute arrow point.
		xrast = [ -alth/2.,  alth/2., -alth/2. ] $
		* sin(zenith(xc,yc)*!pi/180.)
		yrast = [  alth/4.,        0., -alth/4. ]
		cn = cos(azimuth(xc,yc)*!pi/180.)
		sn = sin(azimuth(xc,yc)*!pi/180.)
		xprm = cn*xrast-sn*yrast
		yprm = sn*xrast+cn*yrast

				    ;Draw arrow point.
		if zenith(xc,yc) le 90. then  col=upc  else  col=dnc
		ps_plots, xorg+ix+xprm, yorg+iy+yprm, color=col, tplate=tpl
	end
end
end

return
end
				    ;Draw tanget arrows.
fld = azam_a_image( c.dty+'a_fld', c )

for  xc = xx0+1,xx1-1  do begin
for  yc = yy0+1,yy1-1  do begin

        ix = (.5+xc-xx0)*xinches/(xx1-xx0+1)
        iy = (.5+yc-yy0)*yinches/(yy1-yy0+1)

        if  ifdata(xc,yc)  then begin

                                    ;Approximate max arrow length.
                alth = 2.*xinches/(xx1-xx0+1)

                                    ;Arrow length scaled by tangent field.
                lth = alth*sin(zenith(xc,yc)*!pi/180.)*fld(xc,yc)/2500.
                wth = (.05*alth) > (.15*lth)

                                    ;Compute arrow.
                xrast = [ 0., lth, lth-2.*wth, lth, lth-2.*wth ]
                yrast = [ 0.,  0.,        wth,  0.,       -wth ]
                cn = cos(azimuth(xc,yc)*!pi/180.)
                sn = sin(azimuth(xc,yc)*!pi/180.)
                xprm = cn*xrast-sn*yrast
                yprm = sn*xrast+cn*yrast

                                    ;Draw arrow point.
                ps_plots, xorg+ix+xprm[0:1], yorg+iy+yprm[0:1] $
                , color=0, thick=.25, tplate=tpl
                ps_plots, xorg+ix+xprm[2:4], yorg+iy+yprm[2:4] $
                , color=0, thick=.25, tplate=tpl
        end
end
end

end
;------------------------------------------------------------------------------
;
;	procedure: azam_ps_image_frame
;
;	purpose:  draw frame about Postscript image with tick marks and
;		  number scale.
;
;------------------------------------------------------------------------------
;  FOR NEW AZAM, pass AZAM structure winfre = aa.winfree
pro azam_ps_image_frame, rframe, winfre, xorg, yorg, xinches, yinches, mm_per_inch $
, tick, xnumber, ynumber, symsz, tpl

;  FOR NEW AZAM, common area for remembering widget input for ticks
common popticks,popmajor,popminor,clktk,clkmtk

				    ;Draw box about image.
ps_plots, color=black, tplate=tpl $
, [ 0.,      0., xinches, xinches, 0. ]+xorg $
, [ 0., yinches, yinches,      0., 0. ]+yorg

				    ;Draw tick marks.
if keyword_set(tick) then begin
				    ;Interval for axis numbers and tick marks.
	interval = 10./mm_per_inch

;  FOR NEW AZAM, allow for input tick interval
;Prompt for PostScript file name extension.
	if (rframe eq 'instrument') then titl = 'Select ' + rframe + ' MAJOR tick interval, arcsec' 
	if (rframe eq 'zenith') then titl = 'Select ' + rframe + ' MAJOR frame tick interval, Mm' 
	if popmajor ne 'savclktk' then begin
		clktk = pop_cult( /string, winfree=winfre $
		, title=titl, $
		['1','5','^g10','20','50','100','^g** DEFAULT **'])
		popmajor = 'savclktk'
	endif
;Set default. 
		case clktk of
			'** DEFAULT **' : interval = 10./mm_per_inch
		        '1'         : interval = 1./ mm_per_inch
		        '5'         : interval = 5./ mm_per_inch
		        '10'        : interval = 10./ mm_per_inch
		        '20'        : interval = 20./ mm_per_inch
		        '50'        : interval = 50./ mm_per_inch
		        '100'       : interval = 100./ mm_per_inch
		endcase
		tickdel = round(interval*mm_per_inch)

;  FOR NEW AZAM, allow for input minor tick interval
;Prompt for PostScript file name extension.
	titl = 'Select MINOR ticks per major tick interval' 
	if popminor  ne 'savclkmtk' then begin
		clkmtk = pop_cult( /string, winfree=winfre $
		, title=titl, $
		['^g1','2','4','5','10','^g** DEFAULT **'])
		popminor = 'savclkmtk'
	endif
;Set default. 
		case clkmtk of
			'** DEFAULT **' : minortk = interval/1.
		        '1'         : minortk = interval/1.
		        '2'         : minortk = interval/2.
		        '4'         : minortk = interval/4.
		        '5'         : minortk = interval/5.
		        '10'        : minortk = interval/10.
		endcase

				    ;Tick length.
	ticklen = .04*( xinches < yinches )
;  FOR NEW AZAM, X-axis minor Tick length.
	mticklen = .02*( xinches < yinches )
	mnlbl	= long(xinches/minortk)

				    ;Draw x-axis tick marks.
	nlbl = long(xinches/interval)

;  FOR NEW AZAM, plot X-ticks even if Instrument frame
	;if  rframe ne 'instrument'  and  nlbl ne 0  then begin
	if nlbl ne 0  then begin

		lgen = lindgen(nlbl)+1
		tk   = xorg+lgen*interval
		for i=0,nlbl-1 do begin
			ps_plots, color=0, tplate=tpl $
			, tk(i), [0., ticklen]+yorg
			ps_plots, color=0, tplate=tpl $
			, tk(i), [0.,-ticklen]+yorg+yinches
		end

				    ;Number x-azis.
		if keyword_set(xnumber) then begin
			sz = symsz*xnumber
			ps_xyouts, charsize=sz*1.75*tpl.mfy $
			, align=0.0, tplate=tpl $
			, xorg, yorg-sz*1./4.*tpl.mfy $
			, '0'
			ps_xyouts, charsize=sz*1.75*tpl.mfy $
			, align=0.5, tplate=tpl $
			, tk, yorg-sz*1./4.*tpl.mfy $
			, stringit(tickdel*lgen)
		end

;  FOR NEW AZAM, plot minor ticks on X-axis
		lgen = lindgen(mnlbl)+1
		tk   = xorg+lgen*minortk
		for i=0,mnlbl-1 do begin
			ps_plots, color=0, tplate=tpl $
			, tk(i), [0., mticklen]+yorg
			ps_plots, color=0, tplate=tpl $
			, tk(i), [0.,-mticklen]+yorg+yinches
		end

	end

				    ;Draw y-axis tick marks.
	nlbl = long(yinches/interval)
;  FOR NEW AZAM, Y-axis minor Tick length.
	mnlbl	= long(yinches/minortk)
	
	if nlbl ne 0 then begin

		lgen = lindgen(nlbl)+1
		tk   = yorg+lgen*interval
		for i=0,nlbl-1 do begin
			ps_plots, color=0, tplate=tpl $
			, [0., ticklen]+xorg, tk(i)
			ps_plots, color=0, tplate=tpl $
			, [0.,-ticklen]+xorg+xinches, tk(i)
		end
				    ;Number y-azis.
		if keyword_set(ynumber) then begin
			sz = symsz*ynumber
			ps_xyouts, charsize=sz*1.75*tpl.mfy $
			, align=1.0, tplate=tpl $
			, xorg-sz*1./16.*tpl.mfy, yorg $
			, '0'
			ps_xyouts, charsize=sz*1.75*tpl.mfy $
			, align=1.0, tplate=tpl $
			, xorg-sz*1./16.*tpl.mfy, tk-sz*3./32. $
			, stringit(tickdel*lgen)
		end

;  FOR NEW AZAM, plot minor ticks on Y-axis
		lgen = lindgen(mnlbl)+1
		tk   = yorg+lgen*minortk
		for i=0,mnlbl-1 do begin
			ps_plots, color=0, tplate=tpl $
			, [0., mticklen]+xorg, tk(i)
			ps_plots, color=0, tplate=tpl $
			, [0.,-mticklen]+xorg+xinches, tk(i)
		end

	end
end

end
;------------------------------------------------------------------------------
;
;	function:  azam_ps_key_set
;
;	purpose:  check if arrow, ambigs, or reversal keyword is valid
;
;------------------------------------------------------------------------------
function azam_ps_key_set, key

	ndim = n_dims( key, type=type )

	case type of
	'Undefined':	return, 0
	'Byte':		return, 1
	'String':	return, 1
	'Structure':	return, 0
	else:		return, key(0) ne 0
	end
end
;------------------------------------------------------------------------------
;
;	procedure:  azam_ps_image
;
;	purpose:  output PostScript image of a_* file
;
;------------------------------------------------------------------------------
pro azam_ps_image, aspdata, c, whole=whole $
, bw=bw_, landscape=landscape, portrait=portrait, encapsulated=encapsulated_ $
, tplate=tpl_, ref_frame=ref_frame $
, mm_per_inch = mm_per_inch, mm_inch_std=mm_inch_std $
, origin_xy=origin_xy, hold_origin=hold_origin, inches_xy=inches_xy $
, LLxy=LLxy, URxy=URxy, ABOUTxy=ABOUTxy $
, bkg=bkg, white_bkg=white_bkg, black_bkg=black_bkg $
, gray=gray, invert=invert, wrap=wrap $
, min=min, max=max $
, title=title, symbolsize=symbolsize $
, frame=frame, tick=tick, xnumber=xnumber, ynumber=ynumber $
, arrow=arrow, a_length=a_length, t_arrow=t_arrow $
, ambigs=ambiguous, reversal=reversal, sight_reversal=sight_reversal $
, azm=azm, psi=psi, azimuth=azimuth, zenith=zenith $
, whrA=whrA, colA=colA, whrB=whrB, colB=colB, whrC=whrC, colC=colC

				    ;Check number of parameters.
if n_params() eq 0 then begin
	azam_ps_image_usage
	return
end

;  FOR NEW AZAM, set reference frame
rframe = ref_frame

				    ;Get black & white keyword.
bw = keyword_set(bw_)
				    ;Get encapsulated keyword.
encapsulated = keyword_set(encapsulated_)

				    ;Get symbol size factor.
if n_elements(symbolsize) eq 1 then  symsz=symbolsize  else  symsz=1.

				    ;Check for landscape of portrait keyword
				    ;to open PostScript file.
case 1 of
				    ;Open PostScript device in landscape mode.
keyword_set(landscape): begin

	!p.thick = 5.
	!p.font  = 0
	set_plot, 'ps'

	fi_le  = 'rm_me.ps'
	ndim = n_dims( landscape, type=type )
	if type eq 'String' then  fi_le=landscape

				    ;Get template structure.
	if n_elements(tpl_) eq 0 $
	then  ps_tplate, tpl $
	else  tpl = tpl_
				    ;Open PostScript file.
	device, file=fi_le, bits_per_pixel=8, /inches $
	, xoffset=0., yoffset=tpl.xdevice $
	, xsize=tpl.xdevice, ysize=tpl.ydevice $
	, color=1-bw, /times, /bold, /landscape $
	, encapsulated=encapsulated

	end
				    ;Open PostScript device in portrait mode.
keyword_set(portrait): begin

	!p.thick = 5.
	!p.font  = 0
	set_plot, 'ps'

	fi_le  = 'rm_me.ps'
	ndim = n_dims( portrait, type=type )
	if type eq 'String' then  fi_le=portrait

				    ;Get template structure.
	if n_elements(tpl_) eq 0 $
	then  ps_tplate, tpl, /portrait $
	else  tpl = tpl_
				    ;Open PostScript file.
	device, file=fi_le, bits_per_pixel=8, /inches $
	, xoffset=0., yoffset=0. $
	, xsize=tpl.xdevice, ysize=tpl.ydevice $
	, color=1-bw, /times, /bold, /portrait $
	, encapsulated=encapsulated

	end
else:
end
				    ;Set template structure if not present.
if n_elements(tpl_) eq 0 $
then  ps_tplate, tpl, portrait=!d.y_size gt !d.x_size $
else  tpl = tpl_
				    ;Determine input type.
ndim = n_dims( aspdata, type=type )
				    ;Proceed depending on data type.
case type of
'Byte'			    : byte_image = aspdata
'Integer'		    : data_image = aspdata
'Longword integer'	    : data_image = aspdata
'Floating point'	    : data_image = aspdata
'Double precision floating' : data_image = aspdata
'String'		    :
else:	begin
		print
		print, type+' data type not allowed.'
		stop
	end
end

				    ;Input file for string type.
if type eq 'String' then begin
				    ;Get reference frame.
	if n_elements(ref_frame) ne 0 $
	then  rframe = ref_frame $
	else  begin
		rframe = 'zenith'
		if n_elements(c) ne 0 then  rframe=c.rframe
	end
				    ;Check first character only.
	case strmid(rframe,0,1) of
	'i': rframe='instrument'
	'z': rframe='zenith'
	'd': rframe='disk'
	else: begin
		stop, 'azam_ps_image.pro; invalid ref_frame; '+ref_frame
		return
		end
	end

	data_image = azam_a_image( aspdata, c, bkg=bkg, ref_frame=rframe )
end
				    ;Compute byte image.
if n_elements(byte_image) eq 0 then begin

				    ;Set background arrays.
	w_bkg = -1
	b_bkg = -1
	if keyword_set(white_bkg) or keyword_set(black_bkg) then begin
		tmp = replicate( 1, c.xdim, c.ydim )
		if type eq 'String' then begin
			if c.fdim eq c.npoints $
			then  tmp(c.pxy)=0  else  tmp(c.sxy)=0
		end else begin
			if keyword_set(whole) $
			then  tmp(c.pxy)=0  else  tmp(c.sxy)=0
		end
		if keyword_set(white_bkg) then  w_bkg=where(tmp)
		if keyword_set(black_bkg) then  b_bkg=where(tmp)
		tmp = 0
	end
				    ;Compute byte image.
	tvasp, bw=bw, /notv, data_image, bi=byte_image $
	, min=min, max=max $
	, invert=invert, gray=gray, wrap=wrap $
	, white=temporary(w_bkg), black=temporary(b_bkg)
end
				    ;Get image size and location.
azam_ps_asp_size, c, rframe $
, mm_per_inch = mm_per_inch $
, mm_inch_std = mm_inch_std $
, origin_xy   = origin_xy $
, hold_origin = hold_origin $
, inches_xy   = inches_xy $
, LLxy        = LLxy $
, URxy        = URxy $
, ABOUTxy     = ABOUTxy
				    ;Set local variables with better names.
xorg=origin_xy(0)  &  xinches=inches_xy(0)  &  xx0=LLxy(0)  &  xx1=URxy(0)
yorg=origin_xy(1)  &  yinches=inches_xy(1)  &  yy0=LLxy(1)  &  yy1=URxy(1)

				    ;Output PostScript image.
tv, byte_image(xx0:xx1,yy0:yy1), xorg, yorg $
, xsize=xinches, ysize=yinches, /inches

				    ;Draw ambiguous lines.
if azam_ps_key_set(ambiguous) then begin
				    ;Read sight azimuth if undefined.
	if n_elements(azm) ne c.xdim*c.ydim $
	then  azm = azam_a_image( c.dty+'a_azm', c )

				    ;Get ambigs where array & draw lines.
	ps_where_lines, c.xdim, c.ydim, ambigs( azm, c.sxy ) $
	, origin_xy, inches_xy, LLxy=LLxy, URxy=URxy, tplate=tpl $
	, color = azam_ps_line_color( ambiguous $
	, bw, 'white', gray, 'blue', 'black' )
end
				    ;Draw reversal lines.
if azam_ps_key_set(reversal) then begin
				    ;Read zenith angle if undefined.
	if n_elements(zenith) ne c.xdim*c.ydim $
	then  zenith = azam_a_image( c.dty+'a_1incl', c )

				    ;Get reversal where array & draw lines.
	ps_where_lines, c.xdim, c.ydim, cont( zenith, c.sxy, 90. ) $
	, origin_xy, inches_xy, LLxy=LLxy, URxy=URxy, tplate=tpl $
	, color = azam_ps_line_color( reversal $
	, bw, 'white', gray, 'yellow', 'black' )
end
				    ;Draw sight reversal lines.
if azam_ps_key_set(sight_reversal) then begin

				    ;Read psi angle if undefined.
	if n_elements(psi) ne c.xdim*c.ydim $
	then  psi = azam_a_image( c.dty+'a_psi', c )

				    ;Get reversal where array & draw lines.
	ps_where_lines, c.xdim, c.ydim, cont( psi, c.sxy, 90. ) $
	, origin_xy, inches_xy, LLxy=LLxy, URxy=URxy, tplate=tpl $
	, color = azam_ps_line_color( sight_reversal $
	, bw, 'white', gray, 'yellow', 'black' )
end
				    ;Draw arrow points.
if keyword_set(t_arrow) then  arrow=1
if azam_ps_key_set(arrow) ne 0 $
then	azam_ps_image_arrow, c, bw $
	, arrow, gray, a_length, symsz, azimuth, zenith, tpl $
	, origin_xy, inches_xy, LLxy=LLxy, URxy=URxy, t_arrow=t_arrow

				    ;Draw where lines whrA.
ps_where_lines, c.xdim, c.ydim, whrA $
, origin_xy, inches_xy, LLxy=LLxy, URxy=URxy, tplate=tpl $
, color = azam_ps_line_color( colA, bw, 'white', gray, 'red', 'white' )

				    ;Draw where lines whrB.
ps_where_lines, c.xdim, c.ydim, whrB, tplate=tpl $
, origin_xy, inches_xy, LLxy=LLxy, URxy=URxy $
, color = azam_ps_line_color( colC, bw, 'gray', gray, 'blue', 'gray' )

				    ;Draw where lines whrC.
ps_where_lines, c.xdim, c.ydim, whrC $
, origin_xy, inches_xy, LLxy=LLxy, URxy=URxy, tplate=tpl $
, color = azam_ps_line_color( colB, bw, 'white', gray, 'yellow', 'black' )

				    ;Frame image.
;  FOR NEW AZAM, pass AZAM structure parameter aa.winfree
if keyword_set(frame) $
then	azam_ps_image_frame, c.rframe,c.winfree, xorg, yorg, xinches, yinches, mm_per_inch $
	, tick, xnumber, ynumber, symsz, tpl

				    ;Print title.
if keyword_set(title) $
then	ps_xyouts, charsize=symsz*1.75*tpl.mfy, align=0.5, tplate=tpl $
	, xorg+.5*xinches, yorg+yinches+symsz*1./8.*tpl.mfy, title

end
