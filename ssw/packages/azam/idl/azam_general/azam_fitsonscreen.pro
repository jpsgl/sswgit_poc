;+
; Check the screen size, to see if an image will 
; fit in a subset of the total screen.
; 
; Robert Markel, 08-02-2010
;-
 function azam_fitsOnScreen, imageX, imageY
 
    ; Test to see if map will fit within available screen size, 
    ; leave edge margin for taskbars, plot headers etc.
    percentEdge = 0.90
    
    ; Get the screen resolution.
    screenSize = get_screen_size()
    screenX = screenSize[0] * percentEdge
    screenY = screenSize[1] * percentEdge
    
    if((imageX gt screenX) or (imageY gt screenY)) then begin
      return, 'false'
    endif else begin
      return, 'true'
    endelse
  
 end
