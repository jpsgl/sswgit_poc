pro azam_ps_usage
;+
;
;	procedure:  azam_ps
;
;	purpose:   from AZAM output PostScript files of azam images
;
;	authors:  paul@ncar 5/95  rob@ncar 1/93 lites@ncar 11/10
;		MODIFIED BY LITES 11/10 FOR NEW AZAM THAT OUTPUTS ONLY
;		SINGLE IMAGES
;
;	routines:  azam_ps_usage  azam_ps_extend  azam_mm_inch
;		   azam_ps_files   azam_ps
;
;==============================================================================
;
if 1 then begin
	print
	print, "usage:  azam_ps, aa, hh"
	print
	print, "	From AZAM output PostScript files of ASP images"
	print
	print, "	Arguments"
	print, "		aa	- input azam data set structure"
	print, "		hh	- input highlight structure"
	print
	print, "	;related UNIX"
	print, "	$ pageview -dpi 72 -left 1azm.ps &"
	print, "	$ lpr -Phaocolor 1azm.ps"
	print, "	$ hcprint *.ps ### high quality printing ###"
	return
endif
;-
end
;-----------------------------------------------------------------------------
;	For reference the complete azam_ps_image.pro
;	call is.
;
;	azam_ps_image, cct, c $
;	, bw=bw, landscape=landscape, portrait=portrait $
;	, encapsulated=encapsulated $
;	, ref_frame=ref_frame $
;	, mm_per_inch = mm_per_inch, mm_inch_std = mm_inch_std $
;	, origin_xy=origin_xy, hold_origin=hold_origin, inches_xy=inches_xy $
;	, LLxy=LLxy, URxy=URxy, ABOUTxy=ABOUTxy $
;	, bkg=bkg, white_bkg=white_bkg, black_bkg=black_bkg $
;	, gray=gray, invert=invert, wrap=wrap $
;	, min=min, max=max $
;	, title=title, symbolsize=symbolsize $
;	, frame=frame, tick=tick, xnumber=xnumber, ynumber=ynumber $
;	, arrow=arrow, a_length=a_length $
;	, ambigs=ambiguous, reversal=reversal, sight_reversal=sight_reversal $
;	, azm=azm, psi=psi, azimuth=azimuth, zenith=zenith
;
;------------------------------------------------------------------------------
;
;	procedure:  azam_ps_extend
;
;	purpose:  prompt user for file name extension
;
;------------------------------------------------------------------------------
pro azam_ps_extend, aa, f_extend, encapsulated

				    ;Prompt if encapsulated.
encapsulated = 1L-pop_cult(['yes','^gno'], winfree=aa.winfree $
, title='Want encapsulated PostScript ?')
e = (['','e'])(encapsulated)
				    ;Prompt for PostScript file name extension.
ext = pop_cult( /string, winfree=aa.winfree $
, title='click on file name extension' $ ;"
, $
['.'+e+'ps','^g'+'_rm_me.'+e+'ps' $
,['_0.','_1.','_2.','_3.','_4.','_5.','_6.','_7.','_8.','_9.']+e+'ps' $
,'^g** DEFAULT **','** HELP **','','WIDGET ENTRY','' $
,['_10.','_11.','_12.','_13.','_14.','_15.','_16.']+e+'ps' $
] $
, help='azam', arg0='window', arg1=aa, arg2= $
["    *ps*       - picks a file name extension for output PostScript files."$
,"                 '_rm_me.ps' is recomended; other users should feel free"$
,"                 to remove or overwrite files named '*._rm_me.ps'."$
," WIDGET ENTRY  - user to type file name extension in widget that pops up"$
,"** DEFAULT **  - same as clicking other green button"$
,"  ** HELP **   - print this information"$
] )
				    ;Set default. 
if ext eq '** DEFAULT **' then  ext='_rm_me.'+e+'ps'

				    ;Return click value if not keyboard entry.
if ext ne 'WIDGET ENTRY' then begin
	;f_extend = '.'+ext
	f_extend = ext
	return
end
				    ;Prompt for widget entry.
f_extend = '.'+azam_text_in( aa, 'Enter file name extension' )

end
;------------------------------------------------------------------------------
;
;	procedure:  azam_mm_inch
;
;	purpose:  prompt user for image scale in megameters/inch
;
;------------------------------------------------------------------------------
pro azam_mm_inch, aa, land_xy, just_fit, mm_per_inch, rframe

; FOR NEW AZAM, common area with X-, Y-pixel sizes for image displays
common psimages,delx,dely 


				    ;Plot ranges in megameters.
x_mm = (aa.xdim-1.) / aa.pix_deg * aa.mm_per_deg
y_mm = (aa.ydim-1.) / aa.pix_deg * aa.mm_per_deg

;  FOR NEW AZAM, X-, Y-ranges in arcseconds
if (aa.stretch eq 0) then begin
	x_mm = (aa.xdim-1.)*delx
	y_mm = (aa.ydim-1.)*dely
endif

				    ;Pop up window with choices.
if (aa.stretch ne 0) then begin

;  FOR NEW AZAM, this case for Zenith reference frame
	rframe = 'zenith'
				    ;Scale that will just fit.
just_fit = (x_mm/land_xy(0)) > (y_mm/land_xy(1))

whole = stringit(just_fit)+' just fits'

clk = pop_cult( /string, winfree=aa.winfree $
, title='pick printer xy scale' $
, [ '^g10. megameters/inch', whole, '' $
, '^g** DEFAULT **', '** HELP **', 'WIDGET ENTRY' ] $
, help='azam', arg0='window', arg1=aa, arg2= $
["  10. megameters/inch - this scale works well for ASP data."$
,"                        The scale is what comes out on the printers in the"$
,"                        one image per page format.  For four images per"$
,"                        page the scale will be about 20. meganeters/inch."$
,"    xxxx just fits    - this is the scale in megameters/inch that will"$
,"                        scale the images to the PostScript page size."$
,"                        Note - the default 10. megameters/inch will clip"$
,"                        the images if xxxx is greater than 10."$
,"     WIDGET ENTRY     - user to enter scale in widget that pops up"$
,"    ** DEFAULT **     - same as clicking other green button"$
,"     ** HELP **       - print this information"$
] )
				    ;Return click value if not keyboard entry.
case clk of
	'** DEFAULT **'		: begin
		mm_per_inch = 10.
		return
		end
	'10. megameters/inch'	: begin
		mm_per_inch = 10.
		return
		end
	whole			: begin
		mm_per_inch = just_fit
		return
		end
	else			:
end

endif else begin

;  FOR NEW AZAM, this case for Instrument reference frame
	rframe = 'instrument'

				    ;Scale that will just fit.
just_fit = (x_mm/land_xy(0)) > (y_mm/land_xy(1))

whole = stringit(just_fit)+' just fits'

clk = pop_cult( /string, winfree=aa.winfree $
, title='pick printer xy scale' $
, [ '^g10. arcseconds/inch', whole, '' $
, '^g** DEFAULT **', '** HELP **', 'WIDGET ENTRY' ] $
, help='azam', arg0='window', arg1=aa, arg2= $
["  10. arcseconds/inch - this scale works well for ASP data."$
,"                        The scale is what comes out on the printers in the"$
,"                        one image per page format.  For four images per"$
,"                        page the scale will be about 20. meganeters/inch."$
,"    xxxx just fits    - this is the scale in arcseconds/inch that will"$
,"                        scale the images to the PostScript page size."$
,"                        Note - the default 10. arcseconds/inch will clip"$
,"                        the images if xxxx is greater than 10."$
,"     WIDGET ENTRY     - user to enter scale in widget that pops up"$
,"    ** DEFAULT **     - same as clicking other green button"$
,"     ** HELP **       - print this information"$
] )
				    ;Return click value if not keyboard entry.
case clk of
	'** DEFAULT **'		: begin
		mm_per_inch = 10.
		return
		end
	'10. arcseconds/inch'	: begin
		mm_per_inch = 10.
		return
		end
	whole			: begin
		mm_per_inch = just_fit
		return
		end
	else			:
end

endelse





				    ;Prompt for widget entry.
on_ioerror, ioerror0
ioerror0:
mm_per_inch = 0.

if (aa.stretch ne 0) then begin
reads, azam_text_in( aa, 'Enter megameters/inch scale' ), mm_per_inch
endif else begin
reads, azam_text_in( aa, 'Enter arcseconds/inch scale' ), mm_per_inch
endelse

end
;------------------------------------------------------------------------------
;
;	procedure:  azam_ps_files
;
;	purpose:  prompt for what files to output.
;
;------------------------------------------------------------------------------
pro azam_ps_files, aa, roots, psFiles
				    ;Names of output files.
;  FOR NEW AZAM, do not prompt for two or four per page
psNames =  $
;[       roots, '',    'iiii', '',    'two on one page' $
;, 'bw_'+roots, '', 'bw_iiii', '', 'bw two on one page' ]
[       roots  $
, 'bw_'+roots  ]

				    ;Prompt user for PostScript file names.
psFiles = pop_cult( /string, winfree=aa.winfree $
, title='pick files to output & continue' $
, [ psNames, '^ycontinue', '** HELP **' ], 100, rows=n_elements(roots)+4 $
, help='azam', arg0='window', arg1=aa, arg2= $
["   name     - select this file to be output in PostScript"$
," continue   - click continue after all files have been selected"$
,"** HELP **  - print this information"$
] )
				    ;Return if no files selected.
if psFiles(0) eq 'continue' then  return

				    ;Check if ALL was clicked.
whr = where( psFiles eq '** ALL **', nwhr )
if nwhr ne 0 then begin
	psFiles = psNames
	return
end
				    ;Truncate string at continue.
whr = where( psFiles eq 'continue' )
psFiles = psFiles( 0 : whr(0)-1 )

end
;------------------------------------------------------------------------------
;
;	procedure:  azam_ps
;
;	purpose:  output PostScript files of azam images
;
;------------------------------------------------------------------------------
pro azam_ps, aa, hh


;  Modified FOR NEW AZAM to accept either instrument or local frame images 

;  FOR NEW AZAM, common area for remembering widget input for ticks
common popticks,popmajor,popminor,clktk,clkmtk
popmajor = ' ' & popminor = ' ' & clktk = ' ' & clkmtk = ' '

				    ;Check number of parameters.
if n_params() eq 0 then begin
	azam_ps_usage
	return
end

				    ;Set some variables locally.
t     = aa.t
xdim  = aa.xdim
ydim  = aa.ydim
txdim = t*xdim


				    ;Standard template structure.
ps_tplate, tpl
				    ;Minimum margin.
mrg = tpl.xmrg
				    ;Landscape image size and margin for one
				    ;image per page format.
xdev0=tpl.xdevice  &  lft0=tpl.lft  &  rgt0=tpl.btm
ydev0=tpl.ydevice  &  btm0=tpl.btm  &  top0=tpl.top

				    ;Inches available in xy for ploting image.
land_xy = [ xdev0-lft0-rgt0, ydev0-btm0-top0 ]

				    ;Prompt for output directory path.
if azam_out_dir( aa, o_dir ) eq 0 then return

				    ;Prompt for file name extension.
azam_ps_extend, aa, f_extend, encapsulated

				    ;Prompt for xy image scale in
				    ;mega meters per inch.
azam_mm_inch, aa, land_xy, just_fit, mm_per_inch, rframe

				    ;Prompt for area of interest.
if mm_per_inch lt just_fit then begin
	azam_click_xy, aa, 'Click on area of interest', xctr, yctr
	ABOUTxy = [xctr,yctr]
end else begin
	ABOUTxy = [xdim/2,ydim/2]
end
				    ;Set mu value string.
tmp = azam_a_image( aa.dty+'a__mu', aa ) < .99
muStr = '!M'+string("155B)+'!X ' $ ;"
      + strmid(string(tmp(xdim/2,ydim/2),format='(f4.2)'),1,3)

				    ;Set date string.
month = round(aa.head(0))
day   = round(aa.head(1))
year  = round(aa.head(2))
tmp = [' zip' $
,' Jan ',' Feb ',' Mar ',' Apr ',' May ',' Jun ' $
,' Jul ',' Aug ',' Sep ',' Oct ',' Nov ',' Dec ' ]
dateStr = stringit(day)+tmp(month)+stringit(year)

				    ;Compute highlight structure.
tmp = azam_image( 0, aa, hh )
				    ;Flag if arrow points are present
				    ;(note: make long, byte value 0
				    ;interpreted as true color index).
arrow_on = long( (hh.arrow.hi)(0) ge 0  or  (hh.arrow.lo)(0) ge 0 )

				    ;Unmagnify contour where arrays, 0th color.
if (hh.hi)(0) ge 0 then begin
	tmp = hh.hi
	yy  = tmp/txdim
	xx  = tmp-yy*txdim
	yy  = yy/t
	xx  = xx/t
	tmp = lonarr(xdim,ydim)
	tmp( yy*xdim+xx ) = 1
	whrA = where( tmp )
end
				    ;Unmagnify contour where arrays, 1th color.
				    ;Append defroi outline.
if  (hh.lo)(0) ge 0  or  (hh.outlineroi)(0) ge 0  then begin

	case 1 of
	(hh.lo)(0) ge 0  and  (hh.outlineroi)(0) ge 0: $
		tmp = [ hh.lo, hh.outlineroi ]
	(hh.outlineroi)(0) ge 0: tmp = hh.outlineroi
	else:  tmp = hh.lo
	end

	yy  = tmp/txdim
	xx  = tmp-yy*txdim
	yy  = yy/t
	xx  = xx/t
	tmp = lonarr(xdim,ydim)
	tmp( yy*xdim+xx ) = 1
	whrB = where( tmp )
end
				    ;Unmagnify ambigs or reversal where array.
if hh.laluz(0) ge 0 then begin
	yy = hh.laluz/txdim
	xx = hh.laluz-yy*txdim
	yy = yy/t
	xx = xx/t
	laluz0 = yy*xdim+xx
	tmp = lonarr(xdim,ydim)
	tmp(laluz0) = 1
	laluz0 = where( tmp )
end
				    ;Load tvasp color table.
tvasp, /notv, lindgen(2,2)
				    ;Get local copies of field angles.
aa_azm     = aa.pp(aa.azm).value
aa_psi     = aa.pp(aa.psi).value
aa_azimuth = aa.pp(aa.azz).value
aa_zenith  = aa.pp(aa.zen_ang).value

				    ;One image/page parameters indices.
iprms = $
[ aa.cct $
, aa.fld $
, aa.flx $
, aa.jz $
, aa.cen1 $
, aa.azm $
, aa.psi $
, aa.azz $
, aa.zen_ang $
, aa.alpha $
, aa.extra0 $
, aa.extra1 $
]
				    ;One image/page file name roots.
roots = $
[ 'cct' $
, 'fld' $
, 'flux' $
, 'jz' $
, 'cen1' $
, 'azm' $
, 'psi' $
, '1azm' $
, '1incl' $
, 'fill' $
, aa.pp(aa.extra0).name $
, aa.pp(aa.extra1).name $
]
				    ;One image/page titles.
titles = $
[ 'Continuum Intensity' $
, 'Field Magnitude' $
, 'Magnetic Flux' $
, 'Jz' $
, 'Doppler Shift' $
, 'Observed Azimuth' $
, 'Observed Incline' $
, 'Field Azimuth' $
, 'Zenith Angle' $
;  FOR NEW HINODE AZAM, alpha is fill factor
;, '1.0-Fill' $
, 'Fill' $
, aa.pp(aa.extra0).name $
, aa.pp(aa.extra1).name $
]
				    ;One image/page color bar scales.
scales = $
[ 'Instrument scale' $
, 'Gauss' $
, 'Gauss' $
, 'Gauss/Mm' $
, 'km/sec' $
, 'degrees' $
, 'degrees' $
, 'degrees' $
, 'degrees' $
, '' $
, '' $
, '' $
]
				    ;One image/page formats for color bar range.
formats = $
[ 'integer' $
, 'integer' $
, 'integer' $
, 'integer' $
, '(f10.2)' $
, '(f10.2)' $
, '(f10.2)' $
, '(f10.2)' $
, '(f10.2)' $
, '(f10.2)' $
, '' $
, '' $
]
				    ;Prompt for files to output.
infinity:
wset, aa.winb  &  erase
azam_ps_files, aa, roots, psFiles
				    ;Return if no files selected.
if psFiles(0) eq 'continue' then return


				    ;Save active window & !p system variables.
wset, aa.winb  &  erase, aa.red
w_sav = !d.window
p_sav = !p

				    ;Start one per page outputs.

;  FOR NEW AZAM, prompt user for tick marks on plots
	tick = 1
	clk = pop_cult( /string, winfree=aa.winfree $
	, title='Plot tick marks on images?' $
	, ['^gDefalult YES','^rNO'])
	if clk eq 'Default YES' then tick = 1
	if clk eq 'NO' then tick = 0

				    ;Loop over color and black & white.
for bw=0,1 do begin

				    ;Loop over parameter indices.
for pass=0L,n_elements(iprms)-1 do begin

				    ;Filename root.
root = ([ '', 'bw_' ])(bw) + roots(pass)

				    ;Test if file is requested for output.
if total( psFiles eq root ) ge 1 then begin

				    ;Parameter index.
	iprm = iprms( pass )
				    ;Color structure for parameter.
	clr = aa.pp(iprm).color
				    ;Invert color scale for bw_ incline.
	if  clr.gray eq 0  then $
	if  titles(pass) eq 'Zenith Angle' $
	or  titles(pass) eq 'Observed Incline' $
	then clr.invert = (clr.invert+bw) mod 2

				    ;PostScript device size and margin.
	xdev=xdev0  &  lft=lft0  &  rgt=rgt0
	ydev=ydev0  &  btm=btm0  &  top=top0

				    ;Set variable origin_xy & inches_xy so
				    ;azam_ps_image can return actual values used.
	origin_xy = [ lft, btm ]
	inches_xy = [ xdev-lft-rgt, ydev-btm-top ]

				    ;Call azam_ps_image with selected keyword i/o.
	azam_ps_image, aa.pp(iprm).value, aa $
	, bw=bw, landscape = o_dir+root+f_extend, encapsulated=encapsulated $
	, min       = clr.min $
	, max       = clr.max $
	, gray      = clr.gray $
	, wrap      = clr.wrap $
	, invert    = clr.invert $
	, white_bkg = clr.back eq aa.white $
	, black_bkg = clr.back eq aa.black $
	, whole     = aa.pp(iprm).a_np eq aa.npoints $
	, mm_inch_std = mm_per_inch $
	, origin_xy = origin_xy, inches_xy = inches_xy, ABOUTxy = ABOUTxy $
	, title = titles( pass ) $
;  FOR NEW AZAM, pass the tick flag
	, /frame, tick = tick, /xnumber, /ynumber $
	, azm=aa_azm, psi=aa_psi, azimuth=aa_azimuth, zenith=aa_zenith $
	, arrow = arrow_on $
	, whrA = whrA, whrB = whrB, whrC = laluz0 $
	, ref_frame = rframe



				    ;Set scalers for origin and size.
	xorg=origin_xy(0)  &  xinches=inches_xy(0)
	yorg=origin_xy(1)  &  yinches=inches_xy(1)

;  FOR NEW AZAM, STANDARD OFFSETS
xoffst = mrg + 0.75
yoffst = yorg + yinches + 0.6

;  FOR NEW AZAM, do not print title, adjust offsets of annotations
;		so that cropping can give: image alone, image with axis
;		annotations, or image with axis annotations and color bar

;				    ;Print title.
;	ps_xyouts, charsize=2.0, align=0.0 $
;	, mrg, ydev-mrg-1./4., 'HAO/NSO Advanced Stokes Polarimeter'

				    ;Print xy scale.
	;ps_xyouts, charsize=1.75, align=0.0 $
	;, mrg, yorg+yinches+3./32., 'Megameters xy'
if (aa.stretch ne 0) then begin
	ps_xyouts, charsize=1.5, align=0.0 $
	, xoffst, yoffst, 'Megameters xy'
endif else begin
	ps_xyouts, charsize=1.75, align=0.0 $
	, xoffst, yoffst, 'Arcseconds xy'
endelse

				    ;Print date.
	;ps_xyouts, charsize=2.0, align=1.0 $
	;, xdev-mrg, ydev-mrg-1./4., dateStr
	ps_xyouts, charsize=1.5, align=1.0 $
	, xdev-mrg-2.5, yoffst, dateStr

				    ;Print mu value.
	;ps_xyouts, charsize=2.0, align=1.0 $
	;, xdev-mrg, ydev-mrg-9./16., muStr
	ps_xyouts, charsize=1.5, align=1.0 $
	, xdev-mrg-1.0, yoffst, muStr

;  FOR NEW AZAM, do not print NSEW symbol
;				    ;               N
;				    ;Print symbol  E W.
;				    ;               S
;	ps_xyouts, charsize=2.0, align=.0 $
;	, [ .220, .235, .000, .400 ]+xdev-mrg-3./4. $
;	, [ .500, .000, .250, .250 ]+ydev-mrg-1.5 $
;	, [  'N',  'S',  'E',  'W' ]

;  FOR NEW AZAM, offset color bar by 0.1 inches to separate from plot
	cbxoffst = 0.1
				    ;Select color bar type.
	case aa.pp(iprm).bar of
				    ;Do straight color bar.
	'bar': begin
				    ;Bounds for color bar.
;  FOR NEW AZAM, offset color bar by 0.1 inches to separate from plot
		;x0 = xorg+xinches+1./4. 
		x0 = xorg+xinches+1./4. + cbxoffst
		x1 = x0+1./2.
		y0 = yorg+yinches/2-1.5
		y1 = y0+3.
				    ;Plot color bar.
		tvasp, lindgen(1,128), bw=bw, bi=tmp $
		, gray=clr.gray, wrap=clr.wrap, invert=clr.invert
		tv, tmp, x0, y0, xsize=x1-x0, ysize=y1-y0, /inches

				    ;Draw box about gray bar.
		ps_plots, [x0,x0,x1,x1,x0], [y0,y1,y1,y0,y0]

				    ;Format color bar min max.
		case 1 of
		formats(pass) eq 'integer':  mnmx = $
			string( [ round(clr.min), round(clr.max) ] )

		formats(pass) ne '':   mnmx = $
			string( [ clr.min, clr.max ], format=formats(pass) )

		else:  mnmx = string( [ clr.min, clr.max ] )
		end
				    ;Print color bar min max.
		ps_xyouts, charsize=1.20, align=1.0 $
		, x1, [ y0-1./4., y1+1./16. ] $
		, strcompress( /remove_all, mnmx )

				    ;Color bar title.
		if scales(pass) ne '' then $
		ps_xyouts, charsize=1.20, align=0.5, orient=90. $
		, x0-1./16., .5*(y0+y1), scales(pass)

		end
				    ;Plot azimuth color scale.
	'azimuth': begin
				    ;Bounds for color bar.
;  FOR NEW AZAM, offset color bar by 0.1 inches to separate from plot
		;x0 = xorg+xinches
		x0 = xorg+xinches + cbxoffst
		x1 = x0+3./4.
		y0 = yorg+yinches/2.-3./4.
		y1 = y0+1.5
				    ;Plot color scale ring.
		tv, cbar_azimuth( bw=bw, /notable, min=clr.min, max=clr.max $
		, gray=clr.gray, wrap=clr.wrap, invert=clr.invert ) $
		, x0, y0+1./4., xsize=3./4., ysize=3./4., /inches

				    ;Draw box about color scale ring.
		ps_plots, [x0,x0,x1,x1,x0], [y0,y1,y1,y0,y0]

				    ;Draw color scale zero line.
		ps_plots, [ x1-3./16., x1 ], y0+5./8.

		ps_xyouts, charsize=1.2, align=0.5 $
		, .5*(x1+x0) $
		, [ y0+1./16., y0+1.+1./16., y0+1.+1./4. ] $
		, [ '270', '90', 'Degrees' ]

		end
				    ;Plot zenith angle color cresent.
	'zenith': begin
				    ;Bounds for color bar.
		;x0 = xorg+xinches
;  FOR NEW AZAM, offset color bar by 0.1 inches to separate from plot
		x0 = xorg+xinches + cbxoffst
		x1 = x0+3./4.
		y0 = yorg+yinches/2-(1.+1./8.)
		y1 = y0+2.+1./4.
				    ;Plot color scale cresent.
		tv, cbar_zenith( bw=bw, /notable, min=clr.min, max=clr.max $
		, gray=clr.gray, wrap=clr.wrap, invert=clr.invert ) $
		, x0, y0+1./4., xsize=3./4., ysize=1.+1./2., /inches

				    ;Draw box about color scale cresent.
		ps_plots, [x0,x0,x1,x1,x0], [y0,y1,y1,y0,y0]

				    ;Draw color scale 90 deg line.
		ps_plots, [ x1-3./8., x1 ], y0+1.

		ps_xyouts, charsize=1.2, align=0.0 $
		, x0+.03 $
		, [ y0+1./16., y0+1.75+1./16., y0+1.75+5./16. ] $
		, [ '180', '0', 'Degrees' ]

		end
	end
				    ;Close PostScript file
	device, /close_file
end
end
end


				    ;Return to X windowing system.
				    ;Restore !p system variables.
				    ;Restore X window.
set_plot, 'X'
!p = p_sav
if w_sav ge 0 then  wset, w_sav
				    ;Prompt for more files to output.
goto, infinity

end
