pro azam_ambigs, azm, sdat, t, amb, namb
;+
;
;	procedure:  azam_ambigs
;
;	purpose:  return WHERE() magnetic field vector is not
;		  continuous point to point
;
;	author:  paul@ncar, 4/93	(minor mod's by rob@ncar)
;
;==============================================================================
;
;       Check number of parameters.
;
if n_params() eq 0 then begin
	print
	print, "usage:	azam_ambigs, azm, sxy, t, amb, namb"
	print
	print, "	Return WHERE() magnetic field vector is not"
	print, "	continuous point to point"
	print
	print, "	Arguments"
	print, "		azm	- input line of sight azimuth array."
	print, "		sdat	- input logical 2D array for data"
	print, "			  in input array."
	print, "		t	- magnification 1 2 3 or 6."
	print, "		amb	- output where array."
	print, "		namb	- size of output array."
	print
	return
endif
;-
			;Array sizes.
ndim  = n_dims( azm, xdim, ydim )
txdim = t*xdim
			;Convert to integer.
izm = round(azm)
			;Find ambigs in x direction.
if xdim ge 2 then begin
	whrx = where( sdat(1:xdim-1,*) and sdat(0:xdim-2,*) and $
	((izm(1:xdim-1,*)-izm(0:xdim-2,*))+(360+90)) mod 360  gt  180 $
	, nwhrx )
end else begin
	whrx  = -1
	nwhrx = 0
end
			;Find ambigs in y direction.
if ydim ge 2 then begin
	whry = where( sdat(*,1:ydim-1) and sdat(*,0:ydim-2) and $
	((izm(*,1:ydim-1)-izm(*,0:ydim-2))+(360+90)) mod 360  gt  180 $
	, nwhry )
end else begin
	whry  = -1
	nwhry = 0
end
			;Mangify in x direction.
if nwhrx gt 0 then begin

	y = whrx/(xdim-1)
	x = whrx-y*(xdim-1)

	y = t*y
	x = t*(x+1)-1

	whrx = y*txdim+x

	case t of
		2: whrx = [ whrx, whrx+txdim ]
		3: whrx = [ whrx, whrx+txdim, whrx+2*txdim ]
		6: whrx = [ whrx, whrx+txdim, whrx+2*txdim $
			  , whrx+3*txdim, whrx+4*txdim, whrx+5*txdim ]
		else:
	end
end
			;Mangify in y direction.
if nwhry gt 0 then begin

	y = whry/xdim
	x = whry-y*xdim

	y = t*(y+1)-1
	x = t*x

	whry = y*txdim+x

	case t of
		2: whry = [ whry, whry+1 ]
		3: whry = [ whry, whry+1, whry+2 ]
		6: whry = [ whry, whry+1, whry+2, whry+3, whry+4, whry+5 ]
		else:
	end
end
			;Dimension of returned where array.
namb = t*(nwhrx+nwhry)
			;Combine where arrays.
case 1 of
	nwhrx eq 0: amb = whry
	nwhry eq 0: amb = whrx
	else:       amb = [ whrx, whry ]
end

end
