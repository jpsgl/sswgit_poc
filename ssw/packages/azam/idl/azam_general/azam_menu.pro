  pro azam_menu, aa, hh
  ;+
  ;
  ;	procedure:  azam_menu
  ;
  ;	purpose:  do or set path to azam menu option.
  ;
  ;	author:  paul@ncar, 6/93
  ;-
  
  
    ;	Check number of parameters.
    if(n_params() eq 0)  then begin
      print
      print, "usage:	azam_menu, aa, hh"
      print
      print, "	Do or set path to azam menu option."
      print
      print, "	Arguments"
      print, "		aa	- I/O azam data set structure"
      print, "		hh	- I/O highlight structure"
      print
      return
    endif
    
    ; Often used variables.
    t = aa.t
    xdim = aa.xdim
    ydim = aa.ydim
    
    ;Set pop up menu.
    amenu = $
      [ '^lprimer' $
      , '^lzoom' $
      , '^lstatic window' $
      , '^lread a_* file' $
      , '' $
      , '^lall clear' $
      , '' $
      , '^lclear' $
      , '^lambigs' $
      , '^lreversal' $
      , '^lsight reversal' $
      , '' $
      , '^lcontour' $
      , '^lclear contour' $
      , '' $
      , '^ldefroi' $
      , '^lclear defroi' $
      , '' $
      , '^larrow points' $
      , '^lclear arrow points' $
      , '' $
      , '^lrecomended modes'$
      , '' $
      , '^lprofiles' $
      , '^lspectra' $
      , '' $
      , '^lflick' $
      , '^lsub flick' $
      , '^lblink' $
      , '' $
      , '^limage color' $
      , '^g** DISMISS **' $
      , '** HELP **' $
      , '^x^y** EXIT ZOOM **' $
      , '^r** EXIT AZAM **' $
      , '** REPLACE OP **' $
      , '' $
      , '' $
      , '^lUH potential field' $
      , '^lminimum Jz' $
      , '^lup down everywhere' $
      , '^lcenter' $
      , '^lsmooth' $
      , '^lone iteration smooth' $
      , '' $
      , '^lother azimuth' $
      , '^lrecover reference' $
      , '^lrecover original' $
      , '' $
      , '' $
      , '^lread azimuth file' $
      , '^lwrite azimuth file' $
      , '^lupdate a_* files' $
      , '^lanother inversion azimuth' $
      , '' $
      , '' $
      , '^lpalette' $
      , '^ltvasp color table' $
      , '' $
      , '' $
      , '' $
      , '^lPostScript']
      
    ; Menu striped of escapes.
    bmenu = amenu
    for ii = 0, n_elements(bmenu) - 1 do begin
      while strmid(bmenu(ii), 0, 1) eq '^' do begin
        bmenu(ii) = strmid(bmenu(ii), 2, 1000)
      endwhile
    endfor
    
    ; Another menu copy.
    cmenu = amenu
    
;  FOR New AZAM, ALLOW PostScript FOR ALL IMAGES
    ;; Disable PostScript if images are
    ;; not stretched.
    ;if(aa.stretch eq 0) then begin
    ;  whr = where(bmenu eq 'PostScript')
    ;  cmenu(whr) = '^x'+cmenu(whr)
    ;endif
    
    ; Disable UH pot field if parallactic
    ; angle not in header.
    if(aa.head(104) eq 0.) then begin
      whr = where(bmenu eq 'UH potential field')
      cmenu(whr) = '^x'+cmenu(whr)
    endif
    
    ; Modify buttons for zoom.
    if(aa.zoom ne 0) then begin
      cmenu(where(bmenu eq '** EXIT ZOOM **')) = '^y** EXIT ZOOM **'
      
      ; Disable some options in zoom.
      whr = where( $
        bmenu eq 'PostScript' $
        or bmenu eq 'read azimuth file' $
        or bmenu eq 'write azimuth file' $
        or bmenu eq 'read a_* file' $
        or bmenu eq 'another inversion azimuth' $
        or bmenu eq 'update a_* files' $
        or bmenu eq 'UH potential field' $
        or bmenu eq 'zoom' $
        or bmenu eq 'spectra' $
        or bmenu eq '** REPLACE OP **' $
        or bmenu eq '** EXIT AZAM **')
      cmenu(whr) = '^x' + cmenu(whr)
    endif
    
    ; Prompt for menu choice.
    aa.what_now = pop_cult(cmenu, /string, help='azam', arg0='menu', arg1=aa, $
      winfree=aa.winfree, rows=21, bLength=220, bWidth=20)
      
    ; Check for return to calling program.
    case aa.what_now of
      '** DISMISS **' : return
      '** REPLACE OP **' : return
      '** EXIT AZAM **'	: return
      '** EXIT ZOOM **'	: return
      'zoom' : return
      
      ; Check for help button.
      '** HELP **' : azam_help
      
      ; Read a_* file, overwrites extra parameter.
      'read a_* file'	: azam_a_read, aa, hh
      
      ; Reset image color.
      'image color'	: azam_image_color, aa, hh
      
      ; Check if current results are to be saved.
      'update a_* files': azam_azam, aa.dty, azam_a_azm(aa), azam=aa
      
      ; Check for profiles.
      'profiles' : azam_profiles, hh, aa
      
      ; Check for specta display.
      'spectra'	: azam_spectra, aa, hh
      
      ; Check for blinking.
      'blink'	: azam_blink, hh, aa
      
      ; Check for blinking in both display windows.
      'flick'	: azam_flick, aa, hh
      
      ; Check for blinking sub image in both display windows.
      'sub flick'	: azam_flick, aa, hh, /sub
      
      ; Check for PostScript.
      'PostScript' : azam_ps, aa, hh
      
      ; Compute a contour on an image.
      'contour'	: azam_contour, aa, hh
      
      ; Define region of interest.
      'defroi'	: azam_defroi, aa, hh
      
      ; Run palette library routine.
      'palette'	: xpalette
      
      ; Reset tvasp color table.
      'tvasp color table': begin
        wset, aa.wina
        tvasp, lindgen(2,2)
      end
      
      ; Check for static window.
      'static window': begin
      
        ; Prompt for azam image name.
        lb = pop_cult( azam_image_names(aa), /string $
          , winfree=aa.winfree, help='azam', arg0='names', arg1=aa )
        if(lb eq '** DISMISS **') then return
        
        ; Open static window.
        window, title=lb+'(static window)' $
          , /free, xsize=( aa.t*aa.xdim > 250 ), ysize=aa.t*aa.ydim
          
        ; Display the window.
        tv, azam_image( lb, aa, hh ), 0, 0
      end
      
      ; Write sight azimuth file.
      'write azimuth file': begin
        mess = 'Enter output file name (q=quit) (Return=AZAM)'
        error = 1
        while error ne 0 do begin
          error = 0
          
          ; Prompt for file name.
          azam_file = strcompress( azam_text_in(aa,mess), /remove_all )
          
          if(azam_file eq 'q') then return
          if(azam_file eq '') then azam_file = 'AZAM'
          
          ;Protect a_* files.
          if(strmid( azam_file, 0, 2 ) eq 'a_') then begin
            mess = 'REENTER, ' $
              + 'output to a_file not allowed (q=quit)'
            error = 1
          endif
        endwhile
        
        ; Output stream file.
        if(write_floats(aa.dty+azam_file, azam_a_azm(aa), err)) then begin
          if(azam_out_dir(aa, o_dir)) then begin
            i = write_floats(o_dir+azam_file, azam_a_azm(aa))
          endif
        endif
      end
      
      ; Check for whole image primer.
      'primer': begin
      
        ; Prompt for azam image name.
        lb = pop_cult(azam_image_names(aa), /string, winfree=aa.winfree, help='azam', arg0='names', arg1=aa)
        if(lb eq '** DISMISS **') then return
        npick = (where(lb eq aa.pp.name))(0)
        
        ; Prompt for which image to overwrite.
        name0 = aa.pp(aa.ww(0).iprm).name
        name1 = aa.pp(aa.ww(1).iprm).name
        name2 = aa.pp(aa.ww(2).iprm).name
        lbls = [ name0, name2, '', '', name1, 'magnified', '^g** DISMISS **', '** HELP **' ]
        lpick = pop_cult(lbls, title='overwrite which image?' $
          , winfree=aa.winfree, help='azam', arg0='window', arg1=aa, arg2= $
          [name0+" ~ overwrite window showing "+name0 $
          ,"              with '"+lb+"'" $
          ,name1+" ~ overwrite window showing "+name1 $
          ,"              with '"+lb+"'" $
          ,name2+" ~ overwrite window showing "+name2 $
          ,"              with '"+lb+"'" $
          ,"   magnified   ~ overwrite magnified image with '"+lb+"'" $
          ," ** DISMISS ** ~ return to interactive AZAM"$
          ,"  ** HELP **   ~ print this message"$
          ])
        if(lbls(lpick) eq '^g** DISMISS **') then return
        
        ; Block dragging orig or refer images.
        drag = npick
        if(drag eq aa.azz_orig or drag eq aa.azz_ref or drag eq aa.azz_amb) then drag=aa.azz
        if(drag eq aa.zen_orig or drag eq aa.zen_ref or drag eq aa.zen_amb) then drag=aa.zen_ang
        
        ; Take proper action.
        ; Set parameter number for displayed image
        ; with corresponding mouse drag effect.
        case lpick of
          0: begin
            aa.ww(0).iprm = npick
            aa.ww(0).drag = drag
            azam_display, 0, aa, hh
          end
          1: begin
            aa.ww(2).iprm = npick
            aa.ww(2).drag = drag
            azam_display, 2, aa, hh
          end
          4: begin
            aa.ww(1).iprm = npick
            aa.ww(1).drag = drag
            azam_display, 1, aa, hh
          end
          else: begin
            aa.mgfy.iprm = npick
            azam_mgfy, aa
          end
        endcase
      end
      else: goto, continue14
    end
    return
    
    continue14:
    case aa.what_now of
    
      ; Clear contour structure.
      'clear contour': begin
        hh = { hi:-1L, lo:-1L, iprm:-1L, name:'', level:0., toggle:1L, $
          outlineroi:hh.outlineroi, arrow:hh.arrow, laluz:hh.laluz }
      end
      
      ; Clear region of interest.
      'clear defroi': begin
        aa.defroi = 255B
        hh.outlineroi = -1L
      end
      
      ; Flag azam_image.pro compute arrow points.
      'arrow points': begin
        aa.rehi = 1L
        hh.arrow.hi = 0L
        hh.arrow.lo = 0L
      end
      
      ; Clear arrow points.
      'clear arrow points': begin
        hh.arrow.hi = -1L
        hh.arrow.lo = -1L
      end
      
      ; Clear all high lights..
      'all clear': begin
        aa.hilite  = 'clear'
        aa.defroi = 255B
        hh = $
          { hi:-1L, lo:-1L, iprm:-1L, name:'', level:0., toggle:1L $
          , outlineroi:-1L $
          , arrow:{ hi:-1L, lo:-1L } $
          , laluz:-1L $
          }
      end
      
      ; Clear laluz hilite.
      'clear': begin
        aa.hilite  = 'clear'
        hh.laluz = -1L
      end
      
      ; Check for recomended configuration.
      'recomended modes': begin
        aa.rehi = 1L
        aa.ww(0).iprm = aa.azz
        aa.ww(1).iprm = aa.zen_ang
        aa.ww(0).drag = aa.azz
        aa.ww(1).drag = aa.zen_ang
        aa.hilite = 'ambigs'
        aa.cri = 'reference'
        aa.anti = 'anti reference'
        aa.prime = 'wads'
        aa.pwr = 3
      end
      
      ; Set laluz hilite to ambigs
      ; (see azam_image.pro).
      'ambigs': begin
        aa.rehi = 1L
        aa.hilite = aa.what_now
      end
      
      ; Set laluz hilite to reversal
      ; (see azam_image.pro).
      'reversal': begin
        aa.rehi = 1L
        aa.hilite = aa.what_now
      end
      
      ; Set laluz hilite to sight reversal
      ; (see azam_image.pro).
      'sight reversal': begin
        aa.rehi = 1L
        aa.hilite = aa.what_now
      end
      
      else: goto, continue11
    end
    
    ; Redo three interactive images.
    for inum = 0,2 do begin
      azam_display, inum, aa, hh
    endfor
    return
    
    continue11:
    case aa.what_now of
    
      ; Reverse current azimuth everywhere.
      'other azimuth'	: azam_flipa, aa, aa.sxy
      
      ; Recover the original azimuth.
      'recover original' : azam_flipa, aa, where(aa.rslv ne aa.sdat)
      
      ; Recover the reference azimuth.
      'recover reference'	: azam_flipa, aa, where(aa.rslv ne aa.rfrnc)
      
      ; Pick most vertical field everywhere.
      'up down everywhere'	: azam_flipa, aa, $
        where(abs(aa.pp(aa.zen_amb).value-90.) gt abs(aa.pp(aa.zen_ang).value-90.))
        
      ; Pick minimum current.
      'minimum Jz' : azam_flipa, aa, where(abs(aa.pp(aa.jz_amb).value) lt abs(aa.pp(aa.jz).value))
      
      ; Do potential field disambiguation.
      'UH potential field' : azam_flipa, aa, azam_pot_field(aa)
      
      ; Do whole image smooth.
      'smooth' : azam_flipa, aa, azam_smooth(aa.pp(aa.azm).value, $
        aa.pp(aa.fld).value, aa.pp(aa.psi).value, aa.sdat, nchg, 0, 0, xdim, ydim)
        
      ; Do one iteration whole image smooth.
      'one iteration smooth'	: azam_flipa, aa, azam_smooth(aa.pp(aa.azm).value, $
        aa.pp(aa.fld).value, aa.pp(aa.psi).value, aa.sdat, nchg, 0, 0, xdim, ydim, its=1)
        
      ; Set azimuth from another directory.
      'another inversion azimuth' : begin
      
        ; Prompt for directory path.
        dty = azam_dir(aa)
        if(dty eq 'quit') then return
        
        ; Get unstretched 2D image info for current directory.
        if(aa.stretch) then begin
          azm1  = azam_b_image(aa.dty+'a_azm', b_str=b)
          sxy0  = b.sxy
          xdim0 = b.xdim
          ydim0 = b.ydim
        endif else begin
          sxy0  = aa.sxy
          xdim0 = aa.xdim
          ydim0 = aa.ydim
        endelse
        
        ; Get 2D azimuth image and structure of other directory.
        azm1 = azam_b_image(dty+'a_azm', b_str=b, bkg=9999.)
        
        ; Check that image size is the same.
        if(xdim0 ne b.xdim or ydim0 ne b.ydim) then begin
          azam_click_xy, aa, 'Image size differ; click image to contunue'
          return
        endif
        
        ; Get vector form for azimuth.
        azm1 = azm1(sxy0)
        
        ; Form 2D array.
        azam = fltarr(aa.xdim,aa.ydim)
        azam(aa.sxy) = azm1(aa.vec_sxy)
        
        ; Replace missing data by current azimuth.
        whr = where(azam eq 9999., nwhr)
        if(nwhr ne 0) then azam(whr) = aa.pp(aa.azm).value(whr)
        
        ; Find azimuth difference.
        azam = (azam-aa.pp(aa.azm).value+720.) mod 360.
        
        ; Disambiguate azimuth.
        azam_flipa, aa, where(azam gt 90.  and  azam lt 270.)
      end
      
      ; Read sight azimuth file.
      'read azimuth file': begin
        mess = 'Enter "path" sight azimuth file (q=quit) (Return=AZAM)'
        error1:
        error = 1
        while error ne 0 do begin
        
          ; Prompt for file name.
          azam_file = strcompress( azam_text_in(aa,mess), /remove_all )
          if(azam_file eq 'q') then return
          if(azam_file eq '') then azam_file = 'AZAM'
          
          ; Try to read file.
          a_azam = read_floats(azam_file, error)
          if(error ne 0) then  print, !err_string
          if(error eq 0) then begin
          
            ; Check if file is right length.
            if(n_elements(a_azam) ne aa.nsolved) then begin
              mess = 'REENTER, ' + 'file is wrong length (q=quit)'
              error = 1
            endif
          endif
        end
        
        ; Form 2D array.
        azam = fltarr(aa.xdim,aa.ydim)
        azam(aa.sxy) = a_azam(aa.vec_sxy)
        
        ; Find azimuth difference.
        azam = (azam-aa.pp(aa.azm).value+720.) mod 360.
        
        ; Disambiguate azimuth.
        azam_flipa, aa, where( azam gt 90.  and  azam lt 270. )
      end
      
      ; Check for center.
      'center': begin
        if(0) then begin
        
          ; Patch to test a_jz.pro
          sinz1 = sin(aa.pp(aa.zen_ang).value*(!pi/180.))
          sinz2 = sin(aa.pp(aa.zen_amb).value*(!pi/180.))
          sina1 = sin(aa.pp(aa.azz).value*(!pi/180.))
          cosa1 = cos(aa.pp(aa.azz).value*(!pi/180.))
          sina2 = sin(aa.pp(aa.azz_amb).value*(!pi/180.))
          cosa2 = cos(aa.pp(aa.azz_amb).value*(!pi/180.))
          
          bx = aa.pp(aa.fld).value*(1.-aa.pp(aa.alpha).value) * (sinz1*cosa1 - sinz2*cosa2)/2.
          by = aa.pp(aa.fld).value*(1.-aa.pp(aa.alpha).value) * (sinz1*sina1 - sinz2*sina2)/2.
          
          wcut = where(aa.pp(aa.fld).value ne 0. and bx*bx+by*by lt 100., nchg)
          print, nchg
          ;azam_flipa, aa, whr
          ;goto, patch9
          
          jl = (aa.pp(aa.jz).value+aa.pp(aa.jz_amb).value)/2.
          jp = (aa.pp(aa.jz).value-aa.pp(aa.jz_amb).value)/2.
          tmp = write_floats(aa.dty+'jl', jl(aa.sxy))
          tmp = write_floats(aa.dty+'jp', jp(aa.sxy))
          jps = azam_a_image(aa.dty+'jps', aa)
          djp = jp-jps
          tmp = write_floats(aa.dty+'djp', djp(aa.sxy))
          whr = where(jp*jps lt 0., nchg)
          print, nchg, max(abs(jp))
          azam_flipa, aa, whr
          goto, patch9
        endif
        
        azam_click_xy, aa, 'Click on azimuth center', xcen, ycen
        aa.cen_lat = aa.b__lat(xcen,ycen)
        aa.cen_e_w = aa.b__e_w(xcen,ycen)
        
        ; Prompt if want disambiguation.
        wset, aa.winb
        erase, aa.black
        clk = pop_cult(/string, title='Want center disambiguation?', $
          ['^gno', 'yes (positive polarity)', 'yes (negative polarity)', $
          '^g** DEFAULT **','** HELP **', ''] $
          , winfree=aa.winfree, help='azam', arg0='window', arg1=aa, arg2= $
          ["            no           - no further action;"$
          ,"                           return to interactive AZAM"$
          ,""$
          ," yes (positive polarity) - pick local frame field azimuth most"$
          ,"                           away from the center.  Spot center"$
          ,"                           usually yellow in incline image"$
          ," yes (negative polarity) - pick local frame field azimuth most"$
          ,"                           toward the center.  Spot center"$
          ,"                           usually cyan in incline image"$
          ,""$
          ,"     ** DEFAULT **       - no further action;"$
          ,"                           return to interactive AZAM"$
          ,"      ** HELP **         - print this information"])
        case clk of
          '** DEFAULT **': return
          'no': return
          'yes (positive polarity)': polarity =  1
          'yes (negative polarity)': polarity = -1
          else: stop, 'something wrong here'
        endcase
        
        ; Radial azimuth from center.
        azc = atan(polarity * (aa.b__lat-aa.cen_lat), polarity * (aa.b__e_w-aa.cen_e_w)) * 180./!pi
        
        ; Differences to two local frames.
        az0 = ((aa.pp(aa.azz).value - azc + (720.+180.)) mod 360.) - 180.
        az1 = ((aa.pp(aa.azz_amb).value - azc + (720.+180.)) mod 360.) - 180.
        
        ; Choose closest azimuth to radial.
        azam_flipa, aa, where(abs(az1) lt abs(az0))
        
        patch9:
      end
      else:
    endcase
    
    ; Restore display images.
    for inum = 0, 2 do begin
      azam_display, inum, aa, hh
    endfor
    azam_mgfy, aa
  end
