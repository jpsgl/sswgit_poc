pro azam_convertolocal,azm_obs,incl_obs,yr,mnth,day,hr,minit,sec, $
	slat,slong,rconv,az0,el0,az1,el1

;  routine to convert azimuth, elevation from observers frame
;  to local frame using time and solar position data

;INPUTS:  azm_obs -- azimuth of field in observer's frame, degrees 
;			CCL from solar west [nxx,nyy]
;	  incl_obs-- inclination of field in observer's frame, degrees 
;			from direction toward LOS [nxx,nyy]
;	  yr	-- 1-D array of year of observation, one per slit 
;			position [nxx]
;	  mnth	-- 1-D array of month of observation, one per slit 
;			position [nxx]
;	  day	-- 1-D array of day of observation, one per slit 
;			position [nxx]
;	  hr	-- 1-D array of hour of observation, one per slit 
;			position [nxx]
;	  minit	-- 1-D array of minute of observation, one per slit 
;			position [nxx]
;	  sec	-- 1-D array of second of observation, one per slit 
;			position [nxx]
;	  slat	-- solar latitude N of solar equator, degrees [nxx,nyy]
;	  slong	-- solar longitude W of meridian, degrees [nxx,nyy]
;	  rconv -- fitting index for map, value=0 when pt
;			not fit [nxx,nyy]
;
;OUTPUTS: az0	-- local frame azimuth, degrees  [nxx,nyy]
;	  el0	-- local frame inclination, degrees [nxx,nyy]
;	  az1	-- local frame ambiguous azimuth, degrees  [nxx,nyy]
;	  el1	-- local frame ambiguous inclination, degrees [nxx,nyy]


;  get array sizes
sz = size(azm_obs)
nxx = sz(1) & nyy = sz(2)

;ras = fltarr(nxx) & dcs = fltarr(nxx) & gsdt = fltarr(nxx)
;b0ang = fltarr(nxx) & peeang = fltarr(nxx) & ctrlong = fltarr(nxx)
;r0 = fltarr(nxx)
b0ang = fltarr(nxx)

for ii = 0,nxx-1 do begin
	uut = float(hr(ii)) + float(minit(ii))/60. + float(sec(ii))/3600.
	iy = yr(ii) & im = mnth(ii) & id = day(ii)

	solareph_sbsp,iy,im,id,uut,aras,adcs,aagsdt,ab0ang, $
		apeeang,actrlong,ar0
;	ras(ii) = aras
;	dcs(ii) = adcs
	;gsdt(ii) = aagsdt
	b0ang(ii) = ab0ang
;	peeang(ii) = apeeang
;	ctrlong(ii) = actrlong
;	r0(ii) = ar0
endfor


;  make arrays for output local frame angles
az0 = fltarr(nxx,nyy) & az1 = fltarr(nxx,nyy)
el0 = fltarr(nxx,nyy) & el1 = fltarr(nxx,nyy)
az0(*,*) = 0. & el0(*,*) = 0.
;  arrays for ambiguous azimuth
az1 = az0 & el1 = el0



;  loop over image and convert angles to local frame

for ii = 0,nxx-1 do begin
	b0a = b0ang(ii)
	for jj = 0,nyy-1 do begin

		if rconv(ii,jj) eq 0 then goto,jump44
;  normal azimuth
		r_frame_sphduo, 0., b0a,  $
		azm_obs(ii,jj)*!dtor-!pi/2.,!pi/2.-incl_obs(ii,jj)*!dtor, $
		slong(ii,jj)*!dtor, slat(ii,jj)*!dtor, azz, eel
		az0(ii,jj) = azz 
		el0(ii,jj) = eel
;  ambiguous azimuth
		r_frame_sphduo, 0., b0a,  $
		azm_obs(ii,jj)*!dtor+!pi/2.,!pi/2.-incl_obs(ii,jj)*!dtor, $
		slong(ii,jj)*!dtor, slat(ii,jj)*!dtor, azz, eel
		az1(ii,jj) = azz 
		el1(ii,jj) = eel
	jump44:
	endfor


endfor
az0 = (az0 +!pi/2.)/!dtor	;  convert back to solar west frame
el0 = (!pi/2.-el0)/!dtor	;  convert back to 0 - 180 deg.
az1 = (az1 +!pi/2.)/!dtor	;  convert back to solar west frame
el1 = (!pi/2.-el1)/!dtor	;  convert back to 0 - 180 deg.

;  rectify azimuth to be between +/-180 degrees
whr = where(az0 gt 180.,count)
if count gt 0 then az0(whr) = az0(whr) -360.
whr = where(az0 lt -180.,count)
if count gt 0 then az0(whr) = az0(whr) + 360.
whr = where(az1 gt 180.,count)
if count gt 0 then az1(whr) = az1(whr) -360.
whr = where(az1 lt -180.,count)
if count gt 0 then az1(whr) = az1(whr) + 360.

;save,filename='tempcnvrtlcl.save',az0,el0,az1,el1,incl_obs,azm_obs
;stop

end

