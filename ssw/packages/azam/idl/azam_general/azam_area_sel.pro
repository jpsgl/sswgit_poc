pro azam_area_sel, imagin,redfac, xll,yll,xur,yur

;  Display input image, click to define vertices of rectangular
;  sub-area

;  INPUT:	imagin	= input image
;		redfac	= pixel size reduction factor (passed to
;			  findvrtx.pro)
;  OUTPUTS:	xll,yll	= coordinates of lower-left of rectangle
;		xur,yur = coordinates of upper-right of rectangle

bbb = bytscl(imagin)
szz = size(imagin)
maxv = max(imagin) 			;Get extrema
minv = min(imagin)
orig_w = !d.window
nx = szz(1)				;Cols in image
ny = szz(2)				;Rows in image

jump10:
window, /free, xsize=nx, ysize=ny, title='Subarea Selection'
tv, bbb

azam_findvrtx,bbb,redfac,vx1,vy1,vx2,vy2,xpos=50,ypos=400,wsize=0.4

;  check to see that a finite area is selected
if vx1 eq vx2 or vy1 eq vy2 then begin
	clk = pop_cult(/string, xpos=100,ypos=250 $
		, title = 'Vertices Identical, Redo Selection' $
		,['^gOK'])
	if clk eq 'OK' then begin
		delwin
		goto,jump10
	endif
endif

;  highlight pixels defining the rectangle
xst = min([vx1,vx2]) & yst = min([vy1,vy2])
nxx = abs(vx1-vx2) & nyy = abs(vy1-vy2)
xnd = xst + nxx -1 & ynd = yst + nyy -1
;  check ranges
if xst lt 0 then xst = 0
if xnd gt nx-1 then xnd = nx-1
if yst lt 0 then yst = 0
if ynd gt ny-1 then ynd = ny-1
tbb = bbb
tbb(xst:xnd,yst) = 255
tbb(xst:xnd,ynd) = 255
tbb(xst,yst:ynd) = 255
tbb(xnd,yst:ynd) = 255
tv, tbb


;  determine if necessary to re-select window
clk = pop_cult(/string, xpos=100,ypos=100 $
	, title = 'Re-do sub-area selection?' $
	,['Redo Selection','^gselection OK'])
if clk eq 'Redo Selection' then begin
	delwin
	goto,jump10
endif

xll = xst & yll = yst
xur = xst + nxx -1 & yur = yst + nyy -1

return
end
