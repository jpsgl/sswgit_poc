pro azam_blink, hh, aa
;+
;
;	procedure:  azam_blink
;
;	purpose:  do blinking for 'azam.pro'
;
;	author:  paul@ncar, 6/93	(minor mod's by rob@ncar)
;
;=============================================================================
;
;       Check number of parameters.
;
if n_params() eq 0 then begin
	print
	print, "usage:	azam_blink, hh, aa"
	print
	print, "	Do blinking for 'azam.pro'."
	print
	print, "	Arguments (input)"
	print, "		hh	- highlight structure"
	print, "		aa	- azam data set structure"
	print
	return
endif
;-
				    ;Popular varialbles.
t     = aa.t
xdim  = aa.xdim
ydim  = aa.ydim
txdim = t*xdim
tydim = t*ydim
				    ;Pop up image menu for two choices.
LL = pop_cult( /string, azam_image_names(aa), 2, title='click on two' $
, help='azam', arg0='names', arg1=aa, winfree=aa.winfree )

if  LL(1) eq '** DISMISS **' then return

				    ;Set hidden pixmap windows.
window, /free, /pixmap, xsize=txdim, ysize=tydim
w3 = !d.window
img3 = azam_image( LL(0), aa, hh )
tv, img3, 0, 0

window, /free, /pixmap, xsize=txdim, ysize=tydim
w4 = !d.window
img4 = azam_image( LL(1), aa, hh )
tv, img4, 0, 0
				    ;Open window for blinking.
if aa.winfree then begin
	window, /free, xsize=txdim+3*aa.bwd, ysize=tydim
end else begin
	window, /free, xsize=txdim+3*aa.bwd, ysize=tydim $
	, xpos=1140-txdim+3*aa.bwd, ypos=40
end
w5 = !d.window
				    ;Show ascii image
wshow, aa.wina
				    ;Print instructions.
azam_message, aa, 'left(faster) middle(slower) right(stop)'

				    ;Blink images.
rate = 2.25
faster = 0
while 1 do begin
for imx=0L,1L do begin
				    ;Loop for time interval.
	t0 = systime(1)
	while  systime(1)-t0 lt 1./rate  do begin

				    ;Copy blink image from pixmap window.
		wset, w5  &  device, copy=[0,0,txdim,tydim,0,0,([w3,w4])(imx)]

				    ;Do computed cursor.
		azam_cursor, aa, xerase, yerase $
		, aa.ww(0).win, aa.ww(1).win, aa.ww(2).win,      w3,      w4 $
		,                                              img3,    img4 $
		,      eraser0,      eraser1,      eraser2, eraser3, eraser4 $
		, xc, yc, state $
		, likely=likely $
		, maybe=w5 $
		, no_ttack=0 $
		, delay=1./rate
				    ;Process mouse buttons.
		case  state > aa.bs  of
			0: faster = faster mod 8
			1: if faster eq 0 then  faster = 1
			2: if faster eq 0 then  faster = 2
			4: goto, break0
			else:
		end
	end
				    ;Adjust cycle rate.
	if faster ne 0 then begin
		if faster eq 1 then rate = rate*1.5
		if faster eq 2 then rate = rate/1.5
		faster = 8
	end
end
end
break0:
				    ;Delete extra windows.
wdelete, w3, w4, w5
				    ;Restore display windows.
for k=0L,2L do tw, aa.ww(k).win, aa.ww(k).img, 0, 0

end
