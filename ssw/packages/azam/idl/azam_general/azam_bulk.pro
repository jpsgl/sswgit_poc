  ;+
  ; procedure:  azam_bulk
  ;
  ; purpose:  do bulk of processing for 'azam.pro'
  ;
  ; routines:  azam_bulk_usage  azam_b_menu  azam_bulk
  ;
  ; author:  paul@ncar, 6/94
  ;-

pro azam_bulk_usage
  if(1) then begin
    print
    print, "usage:	azam_bulk, aa, hh"
    print
    print, "	Do bulk of processing for 'azam.pro'."
    print
    print, "	Arguments"
    print, "		aa	- I/O azam data set structure"
    print, "		hh	- I/O highlight structure"
    print
    return
  endif
end

;------------------------------------------------------------------------------
;	procedure:  azam_b_menu
;
;	purpose:  process clicks on azam button window
;------------------------------------------------------------------------------
pro azam_b_menu, aa, hh

  ; Set null return.
  aa.what_now = ''
  
  ; Done if no mouse button pressed.
  if(aa.bs eq 0) then return
  
  ; Set button number.
  bttn = 6 * (aa.by / aa.bwd) + aa.bx / aa.blt
  
  ; Check if button is active.
  if(aa.zoom ne 0) then begin
    if(bttn eq 11) then return
    if(bttn eq 17) then return
  endif
  if(total(bttn eq [3,4,5,6,7,8,9,10,11,12,13,14,15,16,17]) eq 0) then return
  
  ; Set that some action taken.
  aa.what_now = 'something else'
  
  ; Erase button window.
  wset, aa.winb & erase, aa.black
  
  case 1 of
  
    ; Do menu options.
    bttn eq 4: begin
      azam_menu, aa, hh
    end
    
    ; Do help option.
    bttn eq 5: begin
    
      ; Interactive window menu.
      amenu = $
      ; Reverse menu direction to decreasing, 10/2/2010
        ;[(['1x1','2x2','4x4','8x8','16x16'])(aa.pwr) $
        [(['16x16', '8x8', '4x4', '2x2', '1x1'])(aa.pwr) $
        , aa.cri $
        , '' $
        , aa.lock $
        , aa.anti $
        , '' $
        , aa.mgfy.bttn $
        , aa.prime $
        , '' $
        , aa.hilite $
        , aa.pp(aa.ww(0).drag).name $
        , aa.pp(aa.ww(2).drag).name $
        , 'set reference' $
        , aa.pp(aa.ww(1).drag).name $
        , 'menu' $
        , '* OTHER OP *' $
        , 'BACKUP' $
        , '** HELP **']
        
      ; Scratch some zoom buttons.
      if(aa.zoom ne 0) then begin
        whr = where(amenu eq 'BACKUP' or amenu eq '* OTHER OP *')
        amenu(whr) = '^x'+amenu(whr)
      endif

	  ;  set OTHER OP to non-functional
        amenu(15) = '^x'+amenu(15)
      
      ; Call help procedure.
      azam_help, amenu, 'bulk', aa, xpos=0, ypos=900-3*aa.bwd, rows=3, bLength=aa.blt, bWidth=aa.bwd       
    end
    
    ; Change flip criteria.
    bttn eq 6 or bttn eq 7: begin
      tmp = ['anti azimuth', 'anti original', 'anti reference', 'smooth', 'anti up down', 'maximum Jz']
      whr = where(aa.anti eq tmp)
      whr = (whr(0)+1) mod 6
      aa.anti = tmp(whr)
      tmp = ['azimuth', 'original', 'reference', 'reference', 'up down', 'minimum Jz']
      aa.cri = tmp(whr)
    end
    
    ; Change right buttom effect.
    bttn eq 8: begin
      tmp = ['wads','smooth','no change','average']
      whr = where( aa.prime eq tmp )
      whr = (whr(0)+1) mod 4
      aa.prime = tmp(whr)
    end
    
    ; Change azimuth window drag effect.
    bttn eq 9: begin
      aa.ww(0).drag = aa.azm > ((aa.ww(0).drag+1) mod aa.nprm)
      aa.prime = 'no change'
    end
    
    ; Change incline window drag effect.
    bttn eq 10: begin
      aa.ww(1).drag = aa.azm > ((aa.ww(1).drag+1) mod aa.nprm)
      aa.prime = 'no change'
    end 
    
    ; Change continuum window drag effect.
    bttn eq 3: begin
      aa.ww(2).drag = aa.azm > ((aa.ww(2).drag+1) mod aa.nprm)
      aa.prime = 'no change'
    end
    
    ; Write BACKUP file.
    bttn eq 11: begin
      if(aa.zoom eq 0) then $
        if(write_floats(aa.dty+'BACKUP', azam_a_azm(aa), err) eq 0) then begin
        azam_message, aa, 'BACKUP file written'
      endif else begin
        if(azam_out_dir(aa, o_dir)) then begin
          i = write_floats(o_dir+'BACKUP', azam_a_azm(aa))
        endif
      endelse
    end
    
    ; Change of sub array size.
    ;bttn eq 12: aa.pwr = (aa.pwr+1) mod 5
    ;
    ; Button powers for mangnification - modified 8/2/2010
    bttn eq 12: begin
      ;print, 'AA.PWR: ', aa.pwr
      aa_power = aa.pwr
      if(aa_power - 1 eq -1) then begin 
        aa_power = 4
        aa.pwr = aa_power mod 5
      endif else begin
        aa.pwr = ((aa.pwr-1)) mod 5
      endelse
    end
    
    ; Lock or unlock mouse buttons.
    bttn eq 13: if aa.lock eq 'unlock drag' then aa.lock='lock drag' else aa.lock='unlock drag'
    
    ; Auto or no continuum magnification.
    bttn eq 14: begin
      case aa.mgfy.bttn of
        'no magnify':	aa.mgfy.bttn = 'magnify'
        'magnify':	aa.mgfy.bttn = 'short arrows'
        'short arrows':	aa.mgfy.bttn = 'no magnify'
      endcase
    end
    
    ; Set laluz hilite type.
    bttn eq 15: begin
      case aa.hilite of
        'clear': aa.hilite = 'ambigs'
        'ambigs': aa.hilite = 'reversal'
        'reversal': aa.hilite = 'sight reversal'
        'sight reversal': aa.hilite = 'clear'
      endcase
      aa.rehi = 1L
    end
    
    ; Set reference structure.
    bttn eq 16: begin
      azam_message, aa, 'Reference azimuth resetting' 
      aa.rfrnc = aa.rslv
      
      refnam = aa.pp(aa.azz_ref).name
      aa.pp(aa.azz_ref) = aa.pp(aa.azz)
      aa.pp(aa.azz_ref).name = refnam
      
      refnam = aa.pp(aa.zen_ref).name
      aa.pp(aa.zen_ref) = aa.pp(aa.zen_ang)
      aa.pp(aa.zen_ref).name = refnam
    end
    
    ; Return if other op button was clicked.
    bttn eq 17: aa.what_now = '* OTHER OP *'
    else:
  endcase
end


;------------------------------------------------------------------------------
;	procedure:  azam_bulk
;
;	purpose:  do bulk of processing for 'azam.pro'
;------------------------------------------------------------------------------
pro azam_bulk, aa, hh

  ; Check number of parameters.
  if(n_params() eq 0) then begin
    azam_bulk_usage
    return
  endif
  
  ; Reset some variables.
  t = aa.t
  xdim = aa.xdim
  ydim = aa.ydim
  yellow = aa.yellow
  red = aa.red
  black = aa.black
  white = aa.white
  blue = aa.blue
  green = aa.green
  
  ; Display azam buttons.
  azam_bttn, aa
  
  ; Numbers saved to check that cursor has changed.
  xsav = -1
  ysav = -1
  ssav = -1
  slock =  0
  mc_do = 0
  
  ; Infinite loop till ** EXIT AZAM **.
  infinity:
  
  ; Do computed cursor.
  azam_cursor, aa, xerase, yerase, $
      aa.ww(0).win, aa.ww(1).win, aa.ww(2).win, undef, undef, undef, undef, $
      eraser0, eraser1, eraser2, undef, undef, $
      xc, yc, state, likely=likely, no_ttack=no_ttack, delay = .2
      
  ; Update magnified continuum window.
  if(mc_do and xc ne -1 and aa.mgfy.bttn ne 'no magnify') then azam_mgfy, aa, xc, yc
  mc_do = 0
  
  ; Check if cursor is off images.
  if(xc eq -1) then begin
  
    ; Cursor is off window. Try again.
    if(aa.bx eq -1) then goto, infinity
    
    ; Process interaction with button window.
    azam_b_menu, aa, hh
    
    ; Process returned instruction string.
    case aa.what_now of
      '** REPLACE OP **' : return
      '** EXIT AZAM **'	: return
      '** EXIT ZOOM **'	: return
      '* OTHER OP *' : return
      'zoom' : return
      '' :
      else: begin
      
        ; Remove state lock.
        slock = 0
        no_ttack = 0
        
        ; Display azam buttons.
        azam_bttn, aa
      end
    endcase
    goto, infinity
  endif
  
  ; Remove multi button states.
  tmp = [0,1,2,1,4,1,2,1]
  state = tmp(state)
  
  ; Set state lock.
  if(aa.lock eq 'unlock drag') then begin
    slock = 0
  endif else if state eq 0 then begin
    state = slock
  endif else if slock eq 0 then begin
    slock = state
    wset, likely
    repeat	cursor, xxxx, yyyy, /device, /nowait  until  !err eq 0
  endif else begin
    state = 0
    slock = 0
    xc = -1
    ssav = -1
    wset, likely
    repeat	cursor, xxxx, yyyy, /device, /nowait  until  !err eq 0
    goto, infinity
  endelse
  
  ; Check if point number or mouse button state has changed.
  if(xc eq xsav and yc eq ysav and ssav eq state) then begin
    ;if n_elements(sum_same) eq 0 then  sum_same=0
    ;sum_same = (sum_same+1) < 1000
    ;if sum_same eq 1000 then  wait, .25
    wait, .005
    goto, infinity
  end
  
  ; Save status to prevent reentry.
  xsav = xc
  ysav = yc
  ssav = state
  
  ; No image changes if state is zero.
  if(state eq 0) then begin
  
    ; Plot magnified continuum.
    mc_do = no_ttack
    if(no_ttack) then xc=-1
    no_ttack = 0
    goto, infinity
  endif
  
  ; Erase ascii window.
  if(no_ttack eq 0) then begin
    wset, aa.wina
    erase, aa.white
  endif
  
  ;Flag computed cursor routine not to display thumb tack cursor.
  no_ttack = 1
  
  ; Set cat after the mouse.
  case state of
    1: cat = aa.cri
    2: cat = aa.anti
    else:	cat = aa.prime
  endcase
  
  ; Use at least 4x4 area for point to point.
  if(aa.pwr lt 2) then begin
    if(cat eq 'wads' or cat eq 'smooth') then begin
      aa.pwr = 2
      azam_bttn, aa
    endif
  endif
  
  ; (x,y) ranges for area about cursor.
  hlf = ([0,1,2,4,8])(aa.pwr)
  x0 =  0 > (xc-hlf)
  y0 =  0 > (yc-hlf)
  x1 = x0 > (xc+hlf-1) < (xdim-1)
  y1 = y0 > (yc+hlf-1) < (ydim-1)
  
  ; Data locations about cursor location.
  cr_sdat  = aa.sdat(x0:x1,y0:y1)
  
  ; Initialize number azimuth flips to zero.
  nchg = 0
  avg = 0
  
  ; Check for anti criteria.
  anti = 1
  case cat of
    'anti azimuth':	cat  = 'azimuth'
    'anti original': cat  = 'original'
    'anti reference':	cat  = 'reference'
    'anti up down':	cat  = 'up down'
    'maximum Jz':	cat  = 'minimum Jz'
    else:	anti = 0
  endcase
  
  ; Feed the cat.
  case cat of
  
    ; Find likely point to point ambigs.
    'wads': chg = azam_wads(aa.pp(aa.azm).value(x0:x1,y0:y1) $
      , aa.pp(aa.fld).value(x0:x1,y0:y1), aa.pp(aa.psi).value(x0:x1,y0:y1) $
      , cr_sdat, nchg)
      
    ; Find likely point to point ambigs.
    'smooth': chg = azam_smooth(aa.pp(aa.azm).value(x0:x1,y0:y1) $
      , aa.pp(aa.fld).value(x0:x1,y0:y1), aa.pp(aa.psi).value(x0:x1,y0:y1) $
      , cr_sdat, nchg, x0, y0, aa.xdim, aa.ydim)
      
    ; Print averages about cursor.
    'average': begin
      avg = 1
      azam_average, aa, xc,yc, x0,y0, x1,y1
    end
    
    ; Do azimuth flip criteria.
    'azimuth': begin
    
      ;Check that center is set.
      if(aa.cen_lat eq -999.) then begin
        slock = 0
        no_ttack = 0
        azam_click_xy, aa, 'Click on azimuth center', xcen, ycen
        aa.cen_lat = aa.b__lat(xcen,ycen)
        aa.cen_e_w = aa.b__e_w(xcen,ycen)
        azam_bttn, aa
        goto, infinity
      endif
      
      ; Radial azimuth from center.
      azc = atan((aa.b__lat(x0:x1,y0:y1)-aa.cen_lat), (aa.b__e_w(x0:x1,y0:y1)-aa.cen_e_w)) * 180./!pi
        
      ; Differences to two local frames.
      az0 = ((aa.pp(aa.azz).value(x0:x1,y0:y1)-azc+(720.+180.)) mod 360.) -180.
      az1 = ((aa.pp(aa.azz-aa.azm).value(x0:x1,y0:y1)-azc+(720.+180.)) mod 360.) -180.
      
      ;Pick closest azimuth.
      chg = where(abs(az1) lt abs(az0), nchg)
    end
    
    ;Pick most radial inclination.
    'up down': chg = where( $
        abs(aa.pp(aa.zen_amb).value(x0:x1,y0:y1)-90.) gt abs( aa.pp(aa.zen_ang).value(x0:x1,y0:y1)-90.), nchg)
      
    ;Pick minimum current.
    'minimum Jz': chg = where(abs(aa.pp(aa.jz_amb).value(x0:x1,y0:y1)) lt abs(aa.pp(aa.jz).value(x0:x1,y0:y1)), nchg)
      
    ;Pick original azimuth.
    'original': chg = where(aa.rslv( x0:x1,y0:y1) ne aa.sdat(x0:x1,y0:y1), nchg)
      
    ;Pick reference azimuth.
    'reference': chg = where(aa.rslv( x0:x1,y0:y1) ne aa.rfrnc(x0:x1,y0:y1), nchg)
    else:
  endcase
  
  ; Reverse for anti criteria.
  if(anti) then begin
    tmp = cr_sdat
    if(nchg ne 0) then tmp(chg) = 0
    chg = where(tmp, nchg)
  endif
  
  ; Do azimuth flips if any.
  if(nchg ne 0) then begin
    xdm  = x1-x0+1
    ychg = chg/xdm
    xchg = chg-ychg*xdm
    azam_flipa, aa, (y0+ychg)*xdim+(x0+xchg)
  endif
  
  ;Set sub images to drag on display.
  luz0 = black
  luz1 = black
  luz2 = black
  hi0 = black
  hi1 = black
  hi2 = black
  lo0 = black
  lo1 = black
  lo2 = black
  if(avg eq 0) then begin
    gray_on = aa.pp(aa.ww(0).drag).color.gray
    luz0 = ([black, yellow])(gray_on)
    hi0 = ([white, red])(gray_on)
    lo0 = ([black, blue])(gray_on)
    new0 = aa.pp(aa.ww(0).drag).img(x0:x1,y0:y1)
    
    gray_on = aa.pp(aa.ww(1).drag).color.gray
    luz1 = ([black, yellow])(gray_on)
    hi1 = ([white, red])(gray_on)
    lo1 = ([black, blue])(gray_on)
    new1 = aa.pp(aa.ww(1).drag).img(x0:x1,y0:y1)
    
    gray_on = aa.pp(aa.ww(2).drag).color.gray
    luz2 = ([black, yellow])(gray_on)
    hi2 = ([white, red])(gray_on)
    lo2 = ([black, blue])(gray_on)
    new2 = aa.pp(aa.ww(2).drag).img(x0:x1,y0:y1)
    
    ; Update core interactive images.
    tx0 = t*x0 & tx1 = t*x1+t-1
    ty0 = t*y0 & ty1 = t*y1+t-1
    aa.ww(0).img(tx0:tx1,ty0:ty1) = puff(new0,t)
    aa.ww(1).img(tx0:tx1,ty0:ty1) = puff(new1,t)
    aa.ww(2).img(tx0:tx1,ty0:ty1) = puff(new2,t)
  end
  
  ; Get data and image bounds with one pixel margin.
  xb0 = 0 > (x0-1)
  yb0 = 0 > (y0-1)
  xb1 = (x1+1) < (xdim-1)
  yb1 = (y1+1) < (ydim-1)
  txb0 = t*xb0
  tyb0 = t*yb0
  txb1 = t*xb1+t-1
  tyb1 = t*yb1+t-1
  xdm = txb1-txb0+1
  ydm = tyb1-tyb0+1
  ll = t-1
  xr = xdm-t
  yu = ydm-t
  
  ; Get images for computed cursor.
  xers = txb0
  yers = tyb0
  ers0 = aa.ww(0).img(txb0:txb1,tyb0:tyb1)
  ers1 = aa.ww(1).img(txb0:txb1,tyb0:tyb1)
  ers2 = aa.ww(2).img(txb0:txb1,tyb0:tyb1)
  new0 = ers0
  new1 = ers1
  new2 = ers2
  
  ; Do cursor highlights.
  if(avg eq 0) then begin
    case aa.hilite of
      'ambigs': $
        azam_ambigs, aa.pp(aa.azm).value(xb0:xb1,yb0:yb1), aa.sdat(xb0:xb1,yb0:yb1), t, amb
      'reversal': $
        azam_cont, aa.pp(aa.zen_ang).value(xb0:xb1,yb0:yb1), aa.sdat(xb0:xb1,yb0:yb1), 90., t, amb
      'sight reversal': $
        azam_cont, aa.pp(aa.psi).value(xb0:xb1,yb0:yb1), aa.sdat(xb0:xb1,yb0:yb1), 90., t, amb
      else: amb = -1L
    endcase
    
    ; Get contours on sub area.
    azam_clevels, aa, hh, hi, lo, xb0, xb1, yb0, yb1
    
    ; Get defroi outline.
    outroi = -1L
    if(aa.defroi(0,0) ne 255) then begin
      azam_cont, aa.defroi(xb0:xb1,yb0:yb1), aa.pdat(xb0:xb1,yb0:yb1), .5, t, outroi
    endif
      
    if(amb(0) ge 0) then begin
      new0(amb) = luz0
      new1(amb) = luz1
      new2(amb) = luz2
    endif
    
    if(hi(0) ge 0) then begin
      new0(hi) = hi0
      new1(hi) = hi1
      new2(hi) = hi2
    endif
    
    if(lo(0) ge 0) then begin
      new0(lo) = lo0
      new1(lo) = lo1
      new2(lo) = lo2
    endif
    
    if(outroi(0) ge 0) then begin
      new0(outroi) = lo0
      new1(outroi) = lo1
      new2(outroi) = lo2
    endif
    
    if(amb(0) ge 0  or  hi(0) ge 0  or  lo(0) ge 0 or  outroi(0) ge 0) then begin
      ers0(ll:xr,ll:yu) = new0(ll:xr,ll:yu)
      ers1(ll:xr,ll:yu) = new1(ll:xr,ll:yu)
      ers2(ll:xr,ll:yu) = new2(ll:xr,ll:yu)
      aa.ww(0).img(txb0:txb1,tyb0:tyb1) = ers0
      aa.ww(1).img(txb0:txb1,tyb0:tyb1) = ers1
      aa.ww(2).img(txb0:txb1,tyb0:tyb1) = ers2
      new0 = ers0
      new1 = ers1
      new2 = ers2
    endif
  endif
  
  ; Place box around computed cursors.
  if(avg) then begin
    new0(*,*) = yellow
    new1(*,*) = yellow
    new2(*,*) = yellow
  endif
  
  new0(ll:xr,ll:yu) = luz0
  new1(ll:xr,ll:yu) = luz1
  new2(ll:xr,ll:yu) = luz2
  imn= (ll+1)<(xr-1)
  imx= (ll+1)>(xr-1)
  jmn= (ll+1)<(yu-1)
  jmx= (ll+1)>(yu-1)
  new0(imn:imx,jmn:jmx) = ers0(imn:imx,jmn:jmx)
  new1(imn:imx,jmn:jmx) = ers1(imn:imx,jmn:jmx)
  new2(imn:imx,jmn:jmx) = ers2(imn:imx,jmn:jmx)
  
  ; Erase old computed cursors.
  if(xerase ge 0) then begin
    tw, aa.ww(0).win, eraser0, xerase, yerase
    tw, aa.ww(1).win, eraser1, xerase, yerase
    tw, aa.ww(2).win, eraser2, xerase, yerase
  end
  
  ; Display computed cursors.
  eraser0 = ers0
  eraser1 = ers1
  eraser2 = ers2
  xerase  = xers
  yerase  = yers
  tw, aa.ww(0).win, new0, xerase, yerase
  tw, aa.ww(1).win, new1, xerase, yerase
  tw, aa.ww(2).win, new2, xerase, yerase
  goto, infinity
end
