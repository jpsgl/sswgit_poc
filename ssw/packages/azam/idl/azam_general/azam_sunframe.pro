pro azam_sunframe, iyr, imth, iday, utimed, $
     	ra, dec, $
     	sunlong, sunlat, valuemu, $
     	ras, dcs, gsdt, b0ang, peeang, ctrlong, r0
;-----------------------------------------------------------------------------
;
;	procedure:  azam_sunframe
;
;	purpose:  translate from line of sight coordinates to solar
;		  longitude and latitude.  In doing so find mu value
;		  and do solar ephemeris.
;
;	authors:  paul@ncar, modified for IDL by BWL (30 Aug. 2007)
; 			modified 17Jan2012 by BWL for AZAM vector/array inputs for AZAM
;
;-----------------------------------------------------------------------------

;	INPUTS:
;	
;		    iyr	= year (i.e. 2007)
;		   imth	= month (1-12)
;		   iday	= day of month
;		  utimed= universal time of day (hours).
;	
;		     ra	= E-W heliocentric coordinate wrt sun center (arcsecs)
;			  (positive westward).
;		    dec	= N-S heliocentric coordinate wrt sun center (arcsecs)
;			  (positive northward).
;
;	OUTPUTS:
;
;		sunlong	= solar longitude positive west of disk center(radians).
;		 sunlat	= solar latitude positive north of equator(radians).
;		valuemu	= mu-value of location of observation on disk.
;
;		    ras	= right ascension disk center(radians).
;		    dcs	= declination disk center(radians).
;		   gsdt	= right ascension from central meridian(radians).
;		  b0ang	= b0-angle(radians).
;		 peeang	= p-angle(radians).
;		ctrlong	= carrington longitude disk center(radians).
;		     r0	= solar radius (arcsecs).



        djd = 0.d

        iy = long(iyr)
        im = long(imth)
        id = long(iday)


;       --<Julian day that starts at 12:00 ut on the given date.
        ;julian1200_sbsp, iy, im, id, jd
        azam_julian1200, iy, im, id, jd

;       --<Set Julian day in continuous form.
        utimed = double(utimed)
        djd = 1.d*jd-.5+utimed/24.d

;	--<Solar coordinates:
;	--<  b0ang:  b0-angle
;	--< peeang:  p-angle
;	--<     r0:  radius in arcseconds
	azam_solcor, djd, ras, dcs, gsdt, b0ang, peeang, ctrlong, r0 

;	--<Radius of observation from disk center.
	robs = sqrt( dec*dec+ra*ra )

;  modifications for processing arrays, not single values
	ondisk = where(robs lt r0,countn,complement=offdisk,ncomplement=counto)
;  establish arrays
	sz = size(ra)
	nxx = sz(1) & nyy = sz(2)
;  locations on and off disk
	abovlimb = fltarr(nxx,nyy) & abovlimb(*,*) = 0.
	valuemu = fltarr(nxx,nyy) & valuemu(*,*) = 0.
	if counto gt 0 then begin
		abovlimb(offdisk) = 0.
		valuemu(offdisk) = 0.
	endif
	if countn gt 0 then begin
		cosobs = robs(ondisk)/r0
		abovlimb(ondisk) = acos(cosobs)
		valuemu(ondisk) = sqrt(1.-cosobs*cosobs)
	endif


;  old single value computations
;	if( robs le r0 ) then begin
;;				    ;Elevation angle of observation above limb.
;		cosobs = robs/r0
;		abovlimb = acos( cosobs )
;
;;				    ;Calculate mu-value of observation.
;		valuemu = sqrt( 1.-cosobs*cosobs )
;	endif else begin
;		abovlimb = 0.
;		valuemu = 0.
;	endelse

;	--<Azimuth of observation CCW from solar north.
	ccwnorth = atan( -ra, dec )

;	--<Transform to solar longitude and latitude.
	azam_sphtri0, b0ang, ccwnorth, abovlimb, sunlong, sunlat 

	return
	end
