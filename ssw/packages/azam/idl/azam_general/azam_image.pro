function azam_image, iprm_, aa, hh
;+
;
;	function:  azam_image
;
;	purpose:  magnify azam image for display and set highlights.
;
;	author:  paul@ncar, 9/93
;
;==============================================================================
;
;       Check number of parameters.
;
if  n_params() eq 0  then begin
	print
	print, "usage:	image = azam_image( iprm, aa, hh )"
	print
	print, "	Magnify azam image for display and set highlights."
	print
	print, "	Arguments"
	print, "		iprm	- input index of parameter"
	print, "			  or string name of parameter"
	print, "		aa	- input azam data set structure"
	print, "		hh	- I/O highlight structure"
	print
	return, 0
endif
;-
				    ;Check if high lights need redone.
if aa.rehi then begin
				    ;Update laluz hilite where array.
	case aa.hilite of
	'ambigs': $
		azam_ambigs, aa.pp(aa.azm).value, aa.sdat, aa.t, laluz
	'reversal': $
		azam_cont, aa.pp(aa.zen_ang).value, aa.sdat, 90., aa.t, laluz
	'sight reversal': $
		azam_cont, aa.pp(aa.psi).value, aa.sdat, 90., aa.t, laluz
	else: laluz = -1L
	end
				    ;Update arrow point structure.
	arrow = { hi:-1L, lo:-1L }
	if  (hh.arrow.hi)(0) ge 0  or  (hh.arrow.lo)(0) ge 0  then $
	arrow = azam_arrow( aa.pp(aa.azz).value $
	, aa.pp(aa.zen_ang).value, aa.sxy, aa.t, aa.angxloc )

				    ;Defroi outline.
	outlineroi = -1L
	if aa.defroi(0,0) ne 255 then $
	azam_cont, aa.defroi, aa.pdat, .5, aa.t, outlineroi

				    ;Update contours.
	azam_clevels, aa, hh, hi, lo

				    ;Save highlights in structure.
	hh = { hi:hi, lo:lo $
	, iprm:hh.iprm, name:hh.name $
	, level:hh.level, toggle:hh.toggle $
	, outlineroi:outlineroi, arrow:arrow, laluz:laluz }

				    ;Flag highlights current.
	aa.rehi = 0L
end
				    ;Find parameter index.
if sizeof_sbsp(iprm_,-3) eq 'String' $
then  iprm = (where( iprm_ eq aa.pp.name ))(0) $
else  iprm = long( iprm_ )
iprm = 0 > iprm < (aa.nprm-1)
				    ;Highlight colors.
umb = ([ aa.white, aa.red    ])((gray = aa.pp(iprm).color.gray))
luz = ([ aa.black, aa.yellow ])( gray )
c0  = ([ aa.white, aa.red    ])( gray )
c1  = ([ aa.black, aa.blue   ])( gray )

				    ;Puff the image.
tmp = puff( aa.pp(iprm).img, aa.t )
				    ;Draw arrows points.
if (hh.arrow.hi)(0) ge 0 then  tmp(hh.arrow.hi) = umb
if (hh.arrow.lo)(0) ge 0 then  tmp(hh.arrow.lo) = luz

				    ;Set contours.
if (hh.hi)(0) ge 0 then  tmp(hh.hi) = c0
if (hh.lo)(0) ge 0 then  tmp(hh.lo) = c1

				    ;Set laluz hilite.
if hh.laluz(0) ge 0 then  tmp(hh.laluz)=luz

				    ;Set defroi outline.
if hh.outlineroi(0) ge 0 then  tmp(hh.outlineroi) = c1

				    ;Return magnified image.
return, tmp

end
