pro unno_sgl,isl,jj,scatlp,mefit_sgl,ifit_sgl,nounnofit

;  modified to compute profiles on the run
;  this routine generates unno profiles for a single point within
;  a map
;  starts moving

;  purpose:  Compute Unno Stokes profiles from MERLIN output
;  parameters.
;  Unno spectra calculated in same way as ASP inversion code, but
;  modified for either scattered light/fill fraction formulation

;	INPUT:
;		isl		- slit scan position in map
;		jj		- position along slit in map
;		scatlp	- scattered light profile

;	OUTPUTS:
;		mefit_sgl- Milne-Eddington fit profiles, [maxsz,4]
;		ifit_sgl - full fit of Stokes-I, including scattered light
;		nounnofit- set to non-zero of this raster point not fit
;
;------------------------------------------------------------------------------

;  some constants 
ccc = 299792.458D	;  light speed in km/sec

;  common area for usage in azam_spectra.pro
common com_azamspectra, xllo,xuro,yllo,yuro,full_directory, $
	iwstrt,iwend,dispers,wavlngth,basewav,etaratio,vbroad, $
	maxsz,ntran,mscatt

;  note:  in com_azamspectra, mscatt flags the fill fraction/scattered light
;  formulation
;		mscatt	- if nonzero, then use ASP scattered light formulation:
;					I = alpha*Iscatt + Imagnetic
;					default: use fill fraction formulation that is
;					default zero: MERLIN output for Hinode data:
;					I = fill*Imagnetic + (1-fill)*Iscatt

;  common area for storing the inversion results and other parameters
;  that will be used for the display of ME profiles
common com_mefitparams, nlinr,nwave,ctrwav,fld,eta0,dmp,dop,bzero, $
	b1mu,cen1,cen2,alpha,cct,delta,ffscat,cos2azm,sin2azm,csn, $
	ffinst,mask1,mask2,isolv

;  first, check to see if this point was fit
nounnofit = 0
if isolv(isl,jj) eq 0. then begin
	nounnofit = 1
	return
endif

nwl1 = maxsz-1

;  calculate the pixel ranges for wavelength cutouts
nline = n_elements(iwstrt)
;  nlinr = ranges of pixels for output array for each spectrum line
nlinr = intarr(nline,2)
;  nwave = total number of wavelengths to output
nwave = 0
for kk = 0,nline-1 do begin
	nwlin = (iwend(kk)-iwstrt(kk) + 1)
	nlinr(kk,0) = nwave
	nlinr(kk,1) = nlinr(kk,0) + nwlin - 1
	nwave = nwave + nwlin
endfor


;  initialize the full wavelength Unno profiles
ifit = lonarr(maxsz)
ifit_sgl = lonarr(maxsz)
mefit_sgl = fltarr(maxsz,4)
unno = fltarr(maxsz,4)
unno(*,*) = 0.

;  loop over the spectral line wavelength ranges
for iline = 0,1 do begin

	ipx = iwstrt(iline)
	jpx = iwend(iline)
	wavzm = wavlngth(iline)
	disper = dispers(iline)
	etazero = eta0(isl,jj)*etaratio(iline)
;  MERLIN line centers are given in km/sec with redshift positive
;  required parameter ctr is line center position in mA relative to
;  pixel 0
	if iline eq 0 then begin
		ctr = ctrwav(iline) + 1000.*wavzm*cen1(isl,jj)/ccc
	endif 
	if iline eq 1 then begin
		ctr = ctrwav(iline) + 1000.*wavzm*cen2(isl,jj)/ccc
	endif

;  build appropriate Voigt functions
	dmpp = dmp(isl,jj)
	dopp = dop(isl,jj)
	fldd = fld(isl,jj)
	vgtv_sgl, ipx, jpx, maxsz, disper, wavzm, dmpp, dopp, $
		ctr, fldd, hgen, fgen

;  Eta and rho
	;etaz = etazero(isl,jj)
	etaz = etazero
	csnn = csn(isl,jj)

	etarho_sgl, ipx, jpx, csnn, etaz, hgen, fgen, maxsz, er

;  Unno functions
	srcftn_sgl, ipx, jpx, er, maxsz, un

	unno = unno + un
;  end loop over lines
endfor

;  rotate Q,U profiles to field azimuth
for iw = 0,nwl1 do begin
	qqqq = unno(iw,1)
	uuuu = unno(iw,2)
	unno(iw,1) = cos2azm(isl,jj)*qqqq-sin2azm(isl,jj)*uuuu
	unno(iw,2) = sin2azm(isl,jj)*qqqq+cos2azm(isl,jj)*uuuu
endfor


;  broaden the synthetic Stokes profiles
;!!!NOTE
;  for now just smear the entire profile with the braodening
;  function for the first line.  If in the future we work with
;  lines at separate wavelengths in AZAM, will be necessary to
;  smear profiles individually
	;smear,unno,ntran,ffinst(*,0),iwstrt,iwend,mask1,mask2
	smear_sgl,unno,ntran,ffinst(*,0),iwstrt,iwend,mask1,mask2

;  add normalizations for muB1, offset for I for B0
	for iw = 0,nwl1 do begin
		for istks = 0,3 do unno(iw,istks) = b1mu(isl,jj)*unno(iw,istks)
		unno(iw,0) = bzero(isl,jj) + unno(iw,0) 
	endfor

;  add scattered light profile
;  first shift the scattered light profile according to parameter delta
;  the parameter delta is the shift of the profile redward in km/sec
;  scattered light shifts in pixels
;!!NOTE, ASSUMES ALL LINES ARE APPROX. AT SAME WAVELENGTH 
	dpix = (1000.*wavlngth(0)*delta(isl,*)/ccc)/dispers(0)
		sh = dpix(jj)
		alph = alpha(isl,jj)
		sh_scat = real_part(fft(xshift_sbsp(ffscat,sh),1))

		if (mscatt ne 0) then begin
			for iw = 0,nwl1 do begin
				ifit(iw) = unno(iw,0) + alph*sh_scat(iw)
			endfor
		endif else begin
			for iw = 0,nwl1 do begin
				unno(iw,*) = alph*unno(iw,*)
				ifit(iw) = unno(iw,0) + (1. - alph)*sh_scat(iw)
			endfor
		endelse
;  outputs only the magnetic Stokes-I component
;  save only those wavelengths to be plotted
		for kk = 0,nline-1 do begin
			mefit_sgl(nlinr(kk,0):nlinr(kk,1),*) = $
				long(unno(iwstrt(kk):iwend(kk),*))
			ifit_sgl(nlinr(kk,0):nlinr(kk,1)) = $
				long(ifit(iwstrt(kk):iwend(kk)))
		endfor
;	endfor

;  end loop over slit positions
;endfor

return
end
