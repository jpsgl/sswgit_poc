pro dipoletrans,wavzm,d,s,nt
;------------------------------------------------------------------------------
;
;	subroutine:  dipoleTrans
;
;	purpose:  For input wavelength calulate zeeman shifts and
;		  normalized intensities for dipole transition.
;
;	author: paul@ncar 12/95
;------------------------------------------------------------------------------

;	Input/output:
;		wavzm	~ wavelength (A).
;	Output:
;		shifts and normalized intensities
;		( ( d(i,k), s(i,k), k=0,nt(i)-1 ), i=0,2 )
;		d(i,k) are in mA/G
;		nt [i=0,2] is number of zeeman components for each of
;		sigma (2) and pi (1)

;  define output arrays
d = fltarr(3,20)
s = fltarr(3,20)
nt = intarr(3)


;  data for dipole transitions
ldat = [ $
      [ 3.,1.,1. , 1.,0.,0. , 4571.    ], $; Mg I
      [ 3.,0.,1. , 3.,1.,1. , 5172.698 ], $; Mg I
      [ 3.,0.,1. , 3.,1.,2. , 5183.615 ], $; Mg I
      [ 7.,2.,3. , 5.,2.,2. , 5247.060 ], $; Mg I
      [ 7.,2.,1. , 5.,2.,0. , 5250.210 ], $; Fe I
      [ 5.,2.,3. , 5.,3.,2. , 5506.782 ], $; Fe I
      [ 3.,0.,1. , 3.,1.,1. , 6122.2   ], $; Ca I
      [ 4.,1.,0.5, 4.,2.,0.5, 6149.238 ], $; Fe II
      [ 5.,2.,0. , 5.,1.,1. , 6173.340 ], $; Fe I
      [ 5.,2.,2. , 5.,1.,2. , 6301.509 ], $; Fe I
      [ 5.,2.,0. , 5.,1.,1. , 6302.501 ], $; Fe I
      [ 3.,1.,1. , 1.,0.,0. , 6767.778 ], $; Ni I
      [ 5.,1.,1. , 5.,1.,1. , 8468.418 ], $; Ni I
      [ 2.,1.,1.5, 2.,2.,1.5, 8498.018 ], $; Ca II
      [ 2.,1.,1.5, 2.,2.,2.5, 8542.089 ], $; Ca II
      [ 5.,1.,3. , 5.,1.,3. , 8688.615 ]  $; Fe I
	  		]

;  get size of array of possible lines
sz = size(ldat)
nline = sz(2)
nparam = sz(1)
;  wavelength is last parameter
w3 = fltarr(nline)
w3(*) = ldat(6,*)

;  Isolate input wavelength.
for kk = 0,nline-1 do begin
	ww1 = fix(w3(kk)+.001)-.001
	ww2 = fix(w3(kk)+1.001)
	if(wavzm gt fix(w3(kk)+.001)-.001 $
    	and wavzm lt fix(w3(kk)+1.001) ) then begin
;  Reset wavelength to hardwired value.
		wavzm = w3(kk)
;  LS coupling parameters
		ru = ldat(0,kk)
		lu = ldat(1,kk)
		ju = ldat(2,kk)
		rl = ldat(3,kk)
		ll = ldat(4,kk)
		jl = ldat(5,kk)
;  get zeeman pattern
		zeem,ru,lu,ju,rl,ll,jl
		return
	endif
endfor

print,'in dipoletrans.pro, unresolved wavelength'
stop
end
