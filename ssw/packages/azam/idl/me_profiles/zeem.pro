pro zeem, ru, lu, ju, rl, ll, jl

;  common zeempattern saves the zeeman pattern of a given line wavelength
;  for subsequent calls
common zeempattern,wavzm,d,s,nt,wavsave

;  define temporary arrays
ml = fltarr(20)
mu = fltarr(20)

;	wavzm  = wavelength of line in A
;	ru,rl = upper, lower multiplicity of line
;	lu,ll = upper, lower orbital angular momentum quantum numbers
;	ju,jl = upper, lower total angular momentum quantum numbers

;	calulates zeeman shifts and normalized intensities
;	for dipole transition
;	(ru,lu,ju)-->(rl,ll,jl)

;  Outputs (to common area):
;	d,s	= Zeeman component amplitudes
;	nt	= number of Zeeman components

;  Upper level.
su=(ru-1.)/2.
gu = 0.
if(ju ne 0.) then gu = 1.5+(su*(su+1.)-lu*(lu+1.))/(2.*ju*(ju+1.))
nu = 2*ju+1
for kk = 0,nu-1 do mu(kk) = ju - kk

;  Lower level.
sl=(rl-1.)/2.
gl = 0.
if(jl ne 0.) then gl = 1.5+(sl*(sl+1.)-ll*(ll+1.))/(2.*jl*(jl+1.))
nl = 2*jl+1
for kk = 0,nl-1 do ml(kk) = jl - kk

n = min(nu,nl)
for ii = 0,2 do begin
	delm = float(2-(ii+1))
	nt(ii) = n
	if((ju eq jl) and (delm ne 0.)) then nt(ii) = n-1
;  Test to see that nt(ii) does not exceed array size
	if(nt(ii) gt 20) then begin
		print,' in zeem.pro, zeem components exceeds 20'
		stop
	endif
	sums = 0.
	for kk = 0,nt(ii)-1 do begin
		if ((ju gt jl) or ((ju eq jl) and (delm le 0.))) then begin
			mup = ml(kk)+delm
			mlo = ml(kk)
		endif else begin
			mup = mu(kk)
			mlo = mu(kk)-delm
		endelse
		d(ii,kk) = gl*mlo - gu*mup
		s(ii,kk) = str(ju,mup,jl,mlo)
		sums = sums+s(ii,kk)
	endfor
	for kk = 0,nt(ii)-1 do begin
		s(ii,kk) = s(ii,kk)/sums
	endfor
endfor

;  If transition is a triplet, lump components to save time
if((ju eq 0.) or (jl eq 0.) or (gu eq gl)) then begin
	for ii = 0,2 do begin
		nt(ii)=1
		s(ii,0) = 1.
	endfor
endif

;  Convert d's to  mA/G
for ii=0,2 do for kk = 0,nt(ii)-1 do begin
	d(ii,kk)=d(ii,kk)*4.6686e-10*wavzm*wavzm
endfor

return
end
