pro vgtv_sgl, ipx, jpx, maxsz, disper, cwavzm, dmp, dvd, ctr, $
		fld, hgen, fgen

;  modified from vgtv_vec.pro for single profile

;	Output generalized voigt and dispersion functions & derivatives
;	for zeeman multiplet from electric dipole transition (hardwired 
;	for wavelength)
;
;	THIS VERSION PROCESSES Single point FIT PARAMETERS
;
;		(ru,lu,ju)-->(rl,ll,jl)
;
;		ru,rl	= upper, lower multiplicity of line
;		lu,ll	= upper, lower orbital angular momentum quantum numbers
;               		       angular momentum (S,P,D,F,G,H)
;		ju,jl	= upper, lower total angular momentum quantum numbers
;		* 
;	        *         e.g for  Fe I 5250 	
;		* (ru,lu,ju)-->(rl,ll,jl) is  (7,D,1)-->(5,D,0)
;
;	input:
;		ipx	= first wavelength pixel to solve
;		jpx	= last wavelength pixel to solve
;		maxsz	= maximum pixel dimension in wavelength space
;		disper	= dispersion mA/pixel
;		cwavzm	= line wavelength, A
;		dmp	= collisional + natural broadening relative to doppler width
;		dvd	= doppler width in mA
;		ctr	= line center mA; relative to pixel 0
;		fld	= absolute magnetic field in G
;
;	output:
;		hgen(iw,i) = generalized voigt function
;		fgen(iw,i) = 2 * ( generalized dispersion function)
;         		i = 1,2 & 3 mean (mu - ml) = +1,0 & -1
;			so in Landi's notation
;			i = 1,2 & 3 mean b,p & r respectively

;  size of arrays
npix = n_elements(fld)

;  define intermediate arrays
hgen = fltarr(maxsz,3)
fgen = fltarr(maxsz,3)

;  common zeempattern saves the zeeman pattern of a given line wavelength
;  for subsequent calls
common zeempattern,wavzm,d,s,nt,wavsave
wavzm = cwavzm
wavsave = 0.

;  intermediate arrays
v = fltarr(maxsz) & w = fltarr(maxsz)
h = fltarr(maxsz) & f = fltarr(maxsz)

;  For input wavelength calulate Zeeman shifts and normalized 
;  intensities for dipole transition.
;  call this routine only when line wavelength changes
if wavzm ne wavsave then begin
	dipoletrans, wavzm, d, s, nt
	wavsave = wavzm
	cwavzm = wavzm
endif

;  setup wavelength scale relative to line center, normalized
;  normalized to Doppler width
for iw = ipx,jpx do v(iw,*) = (disper*iw-ctr)/dvd

;  Magnetic field per doppler width (Gauss/mA).
dmag = fld/dvd

;  Initialize to zero 
;for ii = 0,2 do for iw = ipx,jpx do begin
hgen(*,*) = 0.
fgen(*,*) = 0.
;endfor

;  Loops for Zeeman pattern
for ii = 0,2 do for kk = 0,(nt(ii)-1) do begin

	for iw = ipx,jpx do begin
		w(iw,*) = v(iw,*) - dmag*d(ii,kk)
	endfor

;stop

;  get voigt functions
	voigt_sgl,dmp,w,h,f,ipx,jpx

	for iw = ipx,jpx do begin
		hgen(iw,ii) = hgen(iw,ii) + s(ii,kk)*h(iw)
		fgen(iw,ii) = fgen(iw,ii) + s(ii,kk)*f(iw)
	endfor

;  end loops over transitions
endfor


return
end
