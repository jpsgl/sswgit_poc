pro gausstrans,wavlngth,ntran,vbroad,dispers,ffinst

;  routine to construct an analytic fourier transform of the Gaussian

;!!!NOTE
;  ANALYTIC FORM MUST BE USED HERE BECAUSE OTHERWISE THE PROFILE IS 
;  UNDERSAMPLED AND THERE IS ALIASING IN THE FFT DOMAIN
;  ALSO, MERLIN SMEARS INDIVIDUAL PROFILES SEPARATELY.  IN FUTURE 
;  WHEN LINES AT DIFFERENT WAVELENGTHS ARE FIT, IT WILL BE NECESSARY
;  TO SMEAR EACH PROFILE SEPARATELY HERE TOO.

;INPUTS:
;	wavlngth	= array of line center wavelengths [nline]
;	ntran		= dimension to extend data in wavelength to even power 
;					of 2
;	vbroad		= Gaussian broadening parameter, km/s
;	dispers		= array of dispersions for each line, mA/pix [nline]

;OUTPUT:
;	ffinst		= Fourier transform of instrument function, 
;					complex[ntran,nline]
;					(For now, same for each line)

ccc = 299792.458	;  light speed in km/sec

ntran2 = ntran/2
nline = n_elements(wavlngth)
ffinst = complexarr(ntran,nline)
smearp = complexarr(ntran)
denom = float(ntran)
for kline = 0,nline-1 do begin
	wavzm = wavlngth(kline)
	vbpix = (1000.*wavzm*vbroad/ccc)/dispers(kline)
	smearp(*) = complex(0.,0.)
	for ii = 0,ntran2 do begin
	    arg = -(!pi*float(ii)*vbpix/denom)^2
;  for one argument of complex(), IDL sets the imaginary part=0
	    if (arg ge -35.) then smearp(ii) = complex(exp(arg))
	endfor
;  add in conjugate part.  Some FFT routines do not use conjugate part
	for ii = 0,ntran2-2 do begin
	    smearp(ntran-1-ii) = smearp(ii+1)
	endfor
	ffinst(*,kline) = smearp
endfor

return
end
