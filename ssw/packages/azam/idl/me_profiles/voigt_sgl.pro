pro voigt_sgl,aa,vv,hh,ff,ipx,jpx 

;  modified from voigt_vec.pro for single profile

;	Computes: h(i)=H(a,v(i)), f(i)=2F(a,v(i));  i = ipx,jpx
;	COMPUTES COMPLEX PROBABILITY FUNCTION AFTER
;	HUMLICEK (1982) JQSRT 27, 437.
;	RETURNS REAL & IMAGINARY PARTS AS VOIGT FUNCTIONS
;	compares well with Beckers, SP, 9 (1969)

;  INPUTS:
;	aa		=	ratio of damping to Doppler widths
;	vv		= 	wavelength array, displacement from line center,
;				relative to Doppler width
;	ipx		= 	first wavelength pixel to compute functions
;	jpx		=	last wavelength pixel to compute functions
;
;	OUTPUTS:
;	hh		= 	Voigt function array
;	ff		=	Faraday-Voigt function array


sz = size(vv)
;nwl = sz(1) & npix = sz(2)
nwl = sz(1) 
h = fltarr(nwl) & f = fltarr(nwl)
;hh = fltarr(nwl,npix) & ff = fltarr(nwl,npix)
;hh(*,*) = 0. & ff(*,*) = 0.

;  begin loop over pixels
;for kk = 0,npix-1 do begin
	;v = double(vv(*,kk))
	;a = double(aa(kk))
	v =vv(*)
	a = aa
	h(*) = 0. & f(*) = 0.

irun = ipx-1

;  begin wavelength loop
for many = ipx,jpx do begin

	irun = irun+1
	if( irun gt jpx ) then goto,jump60

	istr  = irun
	istr1 = min( [istr+1, jpx] )

	absv = abs( v(irun) )
	s = absv+a


;  REGION 1
	if( s ge 15. ) then begin
;print,'at region 1'
		for ii = istr1,jpx do begin
			absv = abs( v(ii,*) )
			s = absv+a
			if( s lt 15. ) then goto,jump101
			irun = ii
		endfor
		jump101:

		for ii = istr,irun do begin
			t = complex( a, -v(ii) )
			;t = dcomplex( a, -v(ii) )
			t = t*.5641896/(.5+t*t)
			h(ii) =  real_part(t)
			f(ii) = imaginary(t)
		endfor
	endif


;  REGION 2
	if( s ge 5.5 ) then begin
;print,'at region 2'
		for ii = istr1,jpx do begin
			absv = abs( v(ii) )
			s = absv+a
			if( s ge 15. or s lt 5.5 ) then goto,jump202
			irun = ii
		endfor
		jump202:

		for ii = istr,irun do begin
			t = complex( a, -v(ii) )
			;t = dcomplex( a, -v(ii) )
			u = t*t
			t = t*(1.410474+u*.5641896)/(.75+u*(3.+u))
			h(ii) =  real_part(t)
			f(ii) = imaginary(t)
		endfor
	endif


;  REGION 3
	if( (a+.276)  ge  (.195*absv+.1) ) then begin
;print,'at region 3'
		for ii = istr1,jpx do begin
			absv = abs( v(ii) )
			s = absv+a
			if( s ge 5.5 or (a+.276) lt (.195*absv+.1)) then goto,jump303
			irun = ii
		endfor
		jump303:

		for ii = istr,irun do begin
			t = complex( a, -v(ii) )
			;t = dcomplex( a, -v(ii) )
			t = (16.4955+t*(20.20933+t* $
    			(11.96482+t*(3.778987+t*.5642236)))) / $
     			(16.4955+t*(38.82363+t* $
     			(39.27121+t*(21.69274+t*(6.699398+t)))))
			h(ii) =  real_part(t)
			f(ii) = imaginary(t)
		endfor
	endif


;  REGION 4
	for ii = istr1,jpx do begin
		absv = abs( v(ii) )
		s = absv+a
		if( s ge 5.5 or (a+.276) ge (.195*absv+.1) ) then goto,jump404
		irun = ii
	endfor
	jump404:

;print,'at region 4'
	for ii = istr,irun do begin
		t = complex( a, -v(ii) )
		;t = dcomplex( a, -v(ii) )
		u = t*t
		t = exp(u)- $
   			t*(36183.31-u*(3321.9905-u*(1540.787 $
   			-u*(219.0313-u*(35.76683-u*(1.320522 -u*.56419)))))) / $
   			(32066.6-u*(24322.84-u*(9022.228 $
   			-u*(2186.181-u*(364.2191-u*(61.57037 $
   			-u*(1.841439-u)))))))
		h(ii) =  real_part(t)
		f(ii) = imaginary(t)
	endfor
			

;  end loop over wavelengths
endfor
jump60:

	hh(*) = h
	ff(*) = f


;  end loop over pixels
;endfor

return
end
