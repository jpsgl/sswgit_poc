pro smear,stks,ntran,ffinst,iwstrt,iwend,mask1,mask2

;  routine to convolve stokes profile array with instrument profile

;	INPUTS:
;	stks = Stokes profile array [nwl,4,npix]
;			(npix= number of pixels in map)
;	ntran	= number of wavelength points to transform (>maxsz),
;				i.e. for hinode, next power of 2 > 112 = 128
;	ffinst	= fourier transform of instrument profile defined
;				on [0:ntran-1] in real domain
;	iwstrt	= array of start pixels for synthetic profiles
;	iwend	= array of end pixels for synthetic profiles
;   mask1 = ascending wavelength array mask function [ntran,nline]
;   mask2 = descending wavelength array mask function [ntran,nline]
;!!NOTE:
;  THIS ROUTINE ASSUMES ALL LINES ARE APPROXIMATELY AT THE SAME
;  WAVELENGTH, AND THAT THE SMEARING FUNCTION IS THE SAME FOR
;  EACH LINE 
;
;	OUTPUT:
;	stks = Stokes profile array [nwl,4,npix]

ccc = 299792.458	; speed of light, km/sec

;  first find dimensions of input Stokes profile array
sz = size(stks)
nwl = sz(1)
nstk = sz(2)
npix = sz(3)
nline = n_elements(iwstrt)      ; number of computed spectral lines

;  begin convolution of profiles
datm = fltarr(ntran)
;  begin loop over profiles
for kk = 0,npix-1 do for istk = 0,nstk-1 do begin

	datm(*) = 0.
	dat = stks(*,istk,kk)	;  select individual profile

datsav = dat

	datm(0:nwl-1) = dat
;  extend profile smoothly between computed line positions
;  iwendd = index for end point of previous range
	iwendd = shift(iwend,1)
	for im = 0,nline-1 do begin
		datm = datm + dat(iwendd(im))*mask1(*,im) + $
			dat(iwstrt(im))*mask2(*,im)
	endfor
	
;  perform FFT on extended profile
	fftdat = fft(datm,-1)

;  apply convolution in Fourier domain
	conv = fftdat*ffinst

;  reverse transform
	ffterc = fft(conv,1)

;  replace analytic profiles with convolved data
	stks(*,istk,kk) = real_part(ffterc(0:nwl-1))

endfor

return
end
