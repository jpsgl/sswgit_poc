pro etarho_sgl,ipx, jpx, csn, eta, hgen, fgen, maxsz, er

;  modified from etarho_vec.pro for single profile

;	purpose:  Compute eta's & rho's 
;
;	THIS VERSION PROCESSES ARRAYS OF FIT PARAMETERS


;   INPUTS:
;   ipx,jpx = first, last wavelength pixels to compute
;   csn      = cosine of field inclination to LOS [npix]
;	eta		 = ratio of line center to continuum absorption [npix]
;	hgen	 = voigt functions [maxsz,3,npix]
;	fgen	 = faraday-voigt functions [maxsz,3,npix]
;   maxsz   = maximum wavelength dimension

;   OUTPUTS:
;   er      = eta/rho parameters


;  size of arrays
;npix = n_elements(csn)
;ftn2 = fltarr(npix) & ftn3 = ftn2 & ftn4 = ftn2 & ftn5 = ftn2

;  Output arrays of eta's & rho's
	er	= fltarr(maxsz,5)

	sp2 = 1.-csn*csn
	ez2 = .5*eta

	for iw = ipx,jpx do begin
;  Combinations that are in both eta's rho's
		ftn2 = ez2*(hgen(iw,1)-.5*(hgen(iw,0)+hgen(iw,2)))
		ftn3 = ez2*(hgen(iw,2)-hgen(iw,0))
		ftn4 = ez2*(fgen(iw,1)-.5*(fgen(iw,0)+fgen(iw,2)))
		ftn5 = ez2*(fgen(iw,2)-fgen(iw,0))

;  Compute eta's & rho's
		er(iw,1) = ftn2*sp2
		er(iw,2) = ftn3*csn
		er(iw,3) = ftn4*sp2
		er(iw,4) = ftn5*csn
		er(iw,0) = er(iw,1)+ez2*(hgen(iw,0)+hgen(iw,2))

	endfor

	return
	end
