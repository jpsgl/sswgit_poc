function str,ju,mu,jl,ml
;  Un-normalized zeeman strengths.
;  Ref: CONDON&SHORTLEY P.387 EQ.1-3
;  inputs:	upper/lower level quantum numbers ju,mu,jl,ml


if(ju eq jl) then begin
	if( mu eq ml ) then strr = mu*mu
	if( mu gt ml ) then strr = (ju+mu)*(ju-mu+1.)
	if( mu lt ml ) then strr = (ju-mu)*(ju+mu+1.)
endif

if( ju lt jl ) then begin
	if( mu eq ml ) then strr = (ju+1.)*(ju+1.)-mu*mu
	if ( mu gt ml ) then strr = (ju-mu+1.)*(ju-mu+2.)
	if ( mu lt ml) then strr = (ju+mu+1.)*(ju+mu+2.)
endif

if( ju gt jl ) then begin
	if( mu eq ml ) then strr = ju*ju-mu*mu
	if( mu gt ml ) then strr = (ju+mu)*(ju+mu-1.)
	if( mu lt ml ) then strr = (ju-mu)*(ju-mu-1.)
endif

return,strr
end
