pro srcftn_sgl,ipx, jpx, er, maxsz, un

;  modified from srcftn_vec.pro for single profile

;	purpose:  Compute Unno forms having eta's & rho's.
;
;	THIS VERSION PROCESSES ARRAYS OF FIT PARAMETERS

;	INPUTS:
;	ipx,jpx	= first, last wavelength pixels to compute
;	er		= eta, rho values
;	maxsz	= maximum wavelength dimension

;	OUTPUT:
;	un		= Unno Stokes profiles, [nwl,4,npix] ([0,3] = [I,Q,U,V])


;  size of arrays
sz = size(er)
;nwl = sz(1) & npix = sz(3)
nwl = sz(1) 
;eipm = fltarr(npix)
;term1 = eipm & term2 = eipm & term3 = eipm & term4 = eipm

;stop

;  Output arrays of Unno functions
	un = fltarr(maxsz,4)

	for iw = ipx,jpx do begin
;  Auxiliary terms that frequently occur
		eipm  = 1. + er(iw,0)
		eipm2 = eipm*eipm
		term1 = eipm2 + er(iw,3)*er(iw,3) + er(iw,4)*er(iw,4)
		term2 = er(iw,1)*er(iw,3) + er(iw,2)*er(iw,4)
		term3 = er(iw,1)*er(iw,4) - er(iw,2)*er(iw,3)
		term4 = term1 - er(iw,1)*er(iw,1) - er(iw,2)*er(iw,2)
		ovdet = 1./( eipm2*term4 - term2*term2 )

;if iw eq 73 then stop

;  Unno profiles
		un(iw,0) =  eipm*term1*ovdet
		un(iw,1) = -(eipm2*er(iw,1)+er(iw,3)*term2)*ovdet
		un(iw,2) = -eipm*term3*ovdet
		un(iw,3) = -(eipm2*er(iw,2)+er(iw,4)*term2)*ovdet
	endfor


	return
	end
