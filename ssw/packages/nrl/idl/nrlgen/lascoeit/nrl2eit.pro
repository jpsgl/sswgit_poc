function nrl2eit,nrlhdr
;+
; $Id: nrl2eit.pro,v 1.3 2009/12/15 18:13:23 nathan Exp $
;
; NAME:     NRL2EIT
;
; PURPOSE:  To make a NRL formatted EIT FITS file compatible with EIT software.
;
; CATEGORY: FITS processing
;
; CALLING SEQUENCE:
;       eithdr = nrl2eit(nrlhdr)
;
; INPUTS:  NRL formatted FITS header
;
; KEYWORD PARAMETERS: none
;
; OUTPUTS:
;	FITS header comptaible with EIT software and WCS.
;
; COMMON BLOCKS: none.
;
; SIDE EFFECTS: none.
;
; RESTRICTIONS: none.
;
; PROCEDURE:
;
; Calls:    eit_pixsize.pro, eit_point.pro, get_sc_point.pro
;
; MODIFICATION HISTORY:
; $Log: nrl2eit.pro,v $
; Revision 1.3  2009/12/15 18:13:23  nathan
; added CRVAL1,2
;
; Revision 1.2  2009/11/30 22:16:18  nathan
; added crpix, crota, sc_roll
;
; 	Written by:  J. Newmark 	Date.  Oct 08 1996
;	2001.6.21, N.Rich - Fixed date_obs and object
;       2002.8.06 J. Newmark, fixed EXPTIME
;	2003.4.22, N.Rich - For MAP compatibility, Fixed date_obs AGAIN; 
;			    added CDELT1,2
;
;-

eithdr = nrlhdr
wave = fxpar(nrlhdr,'SECTOR')
wave = fix(strmid(wave,0,3))
fxaddpar, eithdr,'WAVELNTH',wave
;
date_obs = fxpar(nrlhdr,'DATE-OBS')+'t'+fxpar(nrlhdr,'TIME-OBS')
fxaddpar, eithdr,'DATE_OBS',anytim2utc(date_obs,/ccsds)
;
;if eit_fxpar(eithdr,'NAXIS1') + eit_fxpar(eithdr,'NAXIS2') eq 2048 $
actual = eit_fxpar(eithdr,'NAXIS1') + eit_fxpar(eithdr,'NAXIS2')
original = fxpar(nrlhdr,'R2COL') - fxpar(nrlhdr,'R1COL') $
  	  +fxpar(nrlhdr,'R2ROW') - fxpar(nrlhdr,'R1ROW')
IF original EQ 2046 $
   then object = 'Full FOV' else object = 'Partial FOV' 
shrinkage = float(actual)/original
cdelt = eit_pixsize() / shrinkage
fxaddpar, eithdr,'CDELT1',cdelt
fxaddpar, eithdr,'CDELT2',cdelt
fxaddpar, eithdr,'OBJECT',object
;
xy = eit_point(date_obs,bin=1./shrinkage)
fxaddpar, eithdr, 'CRPIX1', xy(0)
fxaddpar, eithdr, 'CRPIX2', xy(1)
fxaddpar, eithdr, 'CRVAL1',0.
fxaddpar, eithdr, 'CRVAL2',0.
;
rota=fxpar(nrlhdr,'crota1')
IF !err LT 0 THEN BEGIN
    xyp=get_sc_point(date_obs)
    rota=xyp.sc_roll
    fxaddpar,eithdr,'CROTA1',rota
    fxaddpar,eithdr,'CROTA2',rota
    help,rota
ENDIF
fxaddpar,eithdr,'SC_ROLL',rota
;
fxaddpar, eithdr,'COMMENT','SHUTTER CLOSE TIME = ' +$
    strtrim(fxpar(nrlhdr,'EXP3'),2) + ' s'
fxaddpar, eithdr,'comment','P1_X =    ' + strtrim(fxpar(nrlhdr,'P1ROW'),2)
fxaddpar, eithdr,'comment','P2_X =    ' + strtrim(fxpar(nrlhdr,'P2ROW'),2)
fxaddpar, eithdr,'comment','P1_Y =    ' + strtrim(fxpar(nrlhdr,'P1COL'),2)
fxaddpar, eithdr,'comment','P2_Y =    ' + strtrim(fxpar(nrlhdr,'P2COL'),2)
fxaddpar, eithdr,'HISTORY','$Id: nrl2eit.pro,v 1.3 2009/12/15 18:13:23 nathan Exp $'
return, eithdr
end

