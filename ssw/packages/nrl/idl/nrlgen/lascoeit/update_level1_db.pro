pro update_level1_db, inp, HDR_ORIG=hdr_orig, REPROCESS=reprocess
;$Id: update_level1_db.pro,v 1.14 2017/12/19 23:01:48 nathan Exp $
;
; Project     :  SOHO - LASCO/(EIT  C2 C3)(dual-use)
;                   
; Name        : update_level1_db
;               
; Purpose     : write MySQL database commands for a Level-1 LASCO FITS header
;               
; Input     ; 	inp STRARR  FITS header for Level-1 file -OR- 
;   	    	    	    List of Level-1 FITS files to read in
;
; Keywords:
;   HDR_ORIG=	Header structure from original Level-0.5 FITS file (optional input)
;   /REPROCESS	Put lines to delete rows before writing rows; use if reprocessing
;   	    	what is already in database.
;
; Use         : IDL> update_level1_db, fits_hdr
;    
; Calls       : 
;
; Comments    : 
;               
; Side effects: writes files in $REDUCE_DB and $REDUCE_LOG/log
;
; Category    : 
;               
; Written     : N.Rich NRL/I2 June 2010
;               
; $Log: update_level1_db.pro,v $
; Revision 1.14  2017/12/19 23:01:48  nathan
; commit current versions
;
; Revision 1.13  2012/12/11 23:19:05  nathan
; search for date_obs range
;
; Revision 1.12  2012/12/11 22:55:17  nathan
; fix error from last
;
; Revision 1.11  2012/12/06 20:59:06  nathan
; allow long number of files
;
; Revision 1.10  2012/12/06 20:58:15  nathan
; do date_obs or for img_leb_hdr update
;
; Revision 1.9  2012/11/20 23:31:37  nathan
; use 22 chars for date_mod
;
; Revision 1.8  2012/11/16 20:26:57  nathan
; fix root for diskpath
;
; Revision 1.7  2011/08/05 16:54:31  nathan
; fix date_mod and reprocess img_leb_hdr
;
; Revision 1.6  2011/08/05 15:48:44  nathan
; typo
;
; Revision 1.5  2011/08/05 15:33:48  nathan
; need to use different date_obs for /reprocess
;
; Revision 1.4  2011/08/04 17:04:54  nathan
; added /REPROCESS
;
; Revision 1.3  2010/10/01 15:28:17  nathan
; fixed bug 8/5/10
;
; Revision 1.2  2010/08/05 19:31:27  nathan
; missing a comma
;
; Revision 1.1  2010/06/18 17:41:40  nathan
; for lasco mysql
;

COMMON dbms, ludb,lulog, delflag

dbdir=getenv('REDUCE_DB')
IF dbdir EQ '' THEN message,'You must exit IDL and source l1.env.'	
IF keyword_set(REPROCESS) THEN message,'/REPROCESS set.',/info

IF strmid(inp[0],0,6) NE 'SIMPLE' THEN BEGIN
; input is list of files
    ninp=n_elements(inp)
    filelist=inp
	
ENDIF ELSE BEGIN
    ninp=1
    filelist='none'
    hdr1=lasco_fitshdr2struct(inp)
ENDELSE

IF datatype(lulog) EQ 'UND' THEN BEGIN
    logpath = getenv_slash ('REDUCE_LOG')
    IF ninp EQ 1 THEN $
    	yymmdd=utc2yymmdd(anytim2utc(fxpar(inp,'DATE-OBS'))) $
    ELSE BEGIN
    	xx=lasco_readfits(filelist[0],lashdr,/no_img)
    	yymmdd=utc2yymmdd(anytim2utc(lashdr.date_obs))
    ENDELSE
    logfile = logpath+'log/update_level1_db_'+yymmdd+'_ff.log'
    opt = strupcase (getenv('REDUCE_OPTS'))
    print,'Opening ',logfile
    openw,lulog,logfile,/GET_LUN,APPEND=1
    wait,2
ENDIF

FOR i=0L,ninp-1 DO BEGIN

    IF filelist[0] NE 'none' THEN xx=lasco_readfits(filelist[i], hdr1, /no_img)
    
    IF keyword_set(HDR_ORIG) THEN hdr=hdr_orig $
    ELSE BEGIN
	fn0=lz_from_ql(hdr1)
	x=lasco_readfits(fn0,hdr,/no_img)
    ENDELSE
    ; hdr is from original LZ level-0.5 file
    ; hdr1 is from the Level-1 file

    udobs=anytim2utc(hdr1.date_obs)	; level-1 conforms to fits standard defn
    yymmdd=utc2yymmdd(udobs)
    today=strmid(hdr1.date,0,22)    	; in db it is 23 char string	
    ; use date of FITS file creation for DATE_MOD in db
    outname=(hdr1.filename)
    sd = filepath(strlowcase(hdr1.detector),root='/net/corona/lasco/level_1',subd=yymmdd) 
    source=strmid(hdr.filename,1,1)
    root=strmid(outname,0,8)
    ;
    ;  If generating the DBMS update commands, then open a file
    ;
    ;  dbfile = log+'db/red_'+root+'.db'
    
    dbfile = concat_dir(dbdir,'las1_'+yymmdd+'_'+root+'.db')
    openw,ludb,dbfile,/get_lun
    ;today = FXPAR(fits_hdr,'DATE')	; use fits header DATE
    printf,ludb,'use lasco;'
    printf,ludb,'start transaction;'
    printf,lulog,'DB update file = '+dbfile
    ;
    ;  update db table IMG_LEB_HDR
    ;
    PRINTF,lulog,'Updating DBMS table = img_leb_hdr'
    printf,ludb,'update img_leb_hdr'
    printf,ludb,'set'
    printf,ludb,'date_mod="',	today,'",'
    printf,ludb,'dateorig="',	hdr.date_obs,' ',hdr.time_obs,'",'
    printf,ludb,'exptimeorig=',	hdr.exptime,','
    printf,ludb,'date_obs="',	hdr1.date_obs,'",'
    printf,ludb,'exptime=',	hdr1.exptime,','
    printf,ludb,'nmissing=',	hdr1.nmissing,','
    printf,ludb,'misslist="',	hdr1.misslist,'",'
    printf,ludb,'crota=',	hdr1.crota
    tobstai=anytim2tai(hdr1.date_obs)
    plus60min =utc2str(tai2utc(tobstai+3600),/ecs)
    minus60min=utc2str(tai2utc(tobstai-3600),/ecs)
    printf,ludb,'where fileorig="'+hdr.fileorig+'" and'
    ; the following is for speeding up the query:
    printf,ludb,'date_obs between "'+strmid(minus60min,0,19)+'" and "'+strmid(plus60min,0,19) + '";'
    printf,ludb,'commit;'

    delflag=0
    ;
    ;  update db table IMG_FILES
    ;
    IF keyword_set(REPROCESS) THEN BEGIN
    	printf,lulog,'REPROCESS: Removing img_files rows with filename=',outname
	printf,ludb,'delete from img_files where filename = "'+outname+'";' 
    ENDIF
    printf,lulog,'Updating DB table = img_files'
    a = get_db_struct ('lasco','img_files')
    a.filename=outname
    a.fileorig=hdr.fileorig
    a.filetype = 2
    a.source = source
    a.composite= 0
    if (hdr1.detector eq 'C1') then a.bunit='erg/cm2/ster/A/sec' else a.bunit = 'MSB'
    a.datamax= hdr1.datamax 
    a.datamin= hdr1.datamin
    ;if(a.datamax gt 0.0) then a.datamin= min(b(where(b gt 0.0))) 
    a.hdr_only = 0
    ;spawn,'pwd',cwd
    a.diskpath = sd	;cwd(0)
    a.date_mod = today
    sql_db_insert,a
    
    ;
    ;  update db table IMG_BROWSE
    ;
    ;  printf,lulog,'Updating DB table = img_browse'
    ;  a = get_db_struct ('lasco','img_browse')
    ;  a.filename=outname
    ;  a.browse_img = make_browse (b,fits_hdr,outname)
    ;  a.date_mod = today
    ;  db_insert,a
    ;
    ;  update db table IMG_HISTORY
    ;
    IF keyword_set(REPROCESS) THEN BEGIN
    	printf,lulog,'REPROCESS: Removing img_history rows with filename=',outname
	printf,ludb,'delete from img_history where filename = "'+outname+'";' 
    ENDIF
    printf,lulog,'Updating DB table = img_history'
    a=get_db_struct ('lasco','img_history')
    a.filename=outname
    a.date_mod = today
    hst = hdr1.history
    j=0
    while hst[j] NE '' do begin
      a.history = hst[j]
      sql_db_insert,a
      j=j+1
    endwhile
    ;
    ;  update db table IMG_PARENT
    ;
    IF keyword_set(REPROCESS) THEN BEGIN
    	printf,lulog,'REPROCESS: Removing img_parent rows with filename=',outname
	printf,ludb,'delete from img_parent where filename = "'+outname+'";' 
    ENDIF
    printf,lulog,'Updating DB table = img_parent'
    a = get_db_struct ('lasco','img_parent')
    a.filename=	outname
    a.parent_num=	0
    a.parent=	hdr.filename
    a.date_mod = 	today
    sql_db_insert,a

    printf,ludb,'commit;'

    free_lun,ludb
  
ENDFOR

IF filelist[0] NE 'none' THEN free_lun,lulog

end
