;$Id: mk_lasco_hdr.pro,v 1.13 2012/02/15 17:32:44 nathan Exp $
;
; Project     : SECCHI and SOHO - LASCO/(EIT  C2 C3)(dual-use)
;                   
; Name        : mk_lasco_hdr
;               
; Purpose     : create a lasco file header from mvi frame header without reading in fits file.
;               
; Explanation : 
;
; Use         : IDL>fhdr=mk_lasco_hdr(ahdr)
;
;  Example    : IDL> fhdr=mk_lasco_hdr(ahdr)
;
;    
; Calls       : GET_SUN_CENTER, EIT_POINT, GET_SEC_PIXEL, GET_CROTA
;
; Comments    : 
;               
; Side effects: assumes some values, if fhdr.NAXIS1 + fhdr.P1row *2 gt 1044 then sumval =2 else 1 (will not work for all subfields)
;               
; Category    : 
;               
; Written     : Lynn Simpson NRL 12/17/2008
;               
; $Log: mk_lasco_hdr.pro,v $
; Revision 1.13  2012/02/15 17:32:44  nathan
; modified update_level1_hdrs.pro
;
; Revision 1.12  2010/11/30 20:34:20  nathan
; use get_solar_radius(/noexternal)
;
; Revision 1.11  2010/07/26 14:27:04  battams
; convert ra/dec to degrees
;
; Revision 1.10  2010-07-26 12:41:14  mcnutt
; corrected GEI crvals to sun position
;
; Revision 1.9  2010/07/22 18:34:45  mcnutt
; corrected GEI crvals
;
; Revision 1.8  2010/07/16 16:52:52  mcnutt
; Set crval1a and crval2a
;
; Revision 1.7  2010/07/16 16:20:15  mcnutt
; Sets RA and Dec coords
;
; Revision 1.6  2010/07/12 13:21:00  mcnutt
; will now work with a lasco fits header input
;
; Revision 1.5  2010/05/14 17:55:08  mcnutt
; added common block for lasco header
;
; Revision 1.4  2009/09/24 20:22:13  nathan
; added info messages
;
; Revision 1.3  2009/09/24 20:16:58  nathan
; added dummy values for EIT and C2 if original file is not available
;
; Revision 1.2  2009/01/29 20:47:24  nathan
; check input to see if it is a complete header structure, and if so use it as base for output
;
; Revision 1.1  2008/12/19 13:12:58  mcnutt
; creats minimal fits hdr with enough info to create wsc hdr with out reading in fits file
;
function mk_lasco_hdr,ahdrin, TIME=time, CAMERA=camera,  _EXTRA=_extra

common lasco_hdr,lasco_hdr_struct

if datatype(lasco_hdr_struct) eq 'UND' then $
RESTORE, FILEPATH('example_hdr.sav', ROOT_DIR=getenv('NRL_LIB'), SUBDIRECTORY=['idl','inout'])

islascohdr=0
cam = 'none'
qltime='1995/01/01'
fname='10'
filt ='none'
IF N_PARAMS() EQ 1 THEN BEGIN
    ahdr=ahdrin
    IF datatype(ahdr) NE 'STC' THEN hdr=lasco_fitshdr2struct(ahdr) ELSE hdr=ahdr
    qltime = hdr.date_obs + ' ' + hdr.time_obs
    cam = STRUPCASE(hdr.detector)
    fname=hdr.filename
    FILT= hdr.filter
ENDIF ELSE ahdr=lasco_hdr_struct
IF KEYWORD_SET(TIME) THEN qltime = utc2str(anytim2utc(time),/ecs) 
IF KEYWORD_SET(CAMERA) THEN cam = STRUPCASE(camera)      
dobs=strmid(qltime,0,10)
tobs=strmid(qltime,11,8)
IF tag_exist(ahdr,'telescop') THEN if ahdr.TELESCOP eq 'SOHO' then islascohdr=1

if ~(islascohdr) then begin

  gotit=0

  cfile=file_search(getenv('QL_IMG')+'/catalogs/daily_combined/'+strmid(dobs,2,2)+strmid(dobs,5,2)+strmid(dobs,8,2)+'_??_05.txt')
  if cfile(0) ne '' then begin
    openr,catlun,cfile,/get_lun
    line=''
    repeat begin
      readf,catlun,line
      if strmid(line,0,strlen(fname)) eq fname then gotit=1
    endrep until (gotit eq 1 or eof(catlun))
    close,catlun
    free_lun,catlun
  endif

  if gotit ne 1  then begin
    cfile=file_search(getenv('LZ_IMG')+'/catalogs/daily_combined/'+strmid(dobs,2,2)+strmid(dobs,5,2)+strmid(dobs,8,2)+'_??_05.txt')
    if cfile(0) ne '' then begin
       openr,catlun,cfile,/get_lun
       line=''
       repeat begin
          readf,catlun,line
          if strmid(line,0,strlen(fname)) eq fname then gotit=1
       endrep until (gotit eq 1 or eof(catlun))
       close,catlun
       free_lun,catlun
    endif ELSE BEGIN
    	;print,''
    	message,'Original file not found; using default values for '+cam,/info
    	IF cam EQ 'EIT' THEN $
    	line='41486953.fts    2008/11/22  00:00:10  EIT    12.9   1024   1024     20      1  Al +1   195A    Normal' $ 
	ELSE IF cam EQ 'C2' THEN $
	line='21303104.fts    2009/09/09  03:30:03   C2    25.1   1024   1024     20      1  Orange  Clear   Normal       0.0000  3389' $
	ELSE IF cam EQ 'C3' THEN $
    	line='32228757.fts    2010/07/12  15:18:15   C3    19.1   1024   1024     20      1  Clear   Clear   Normal       0.0000  3390' $
 	ELSE IF cam EQ 'C1' THEN $
    	line='12012057.fts    1996/08/05  06:59:24   C1    25.1    832    672    115    193  Fe XIV  Clear   Line Sca  5302.7273  1697'
   	print,line
    ENDELSE
  
  endif
   
;0         1         2         3         4         5         6         7         8         9         10        11  
;0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
;41486953.fts    2008/11/22  00:00:10  EIT    12.9   1024   1024     20      1  Al +1   195A    Normal       0.0000  3406

    IF tag_exist(ahdr,'cunit2') THEN fhdr=ahdr ELSE fhdr=lasco_hdr_struct

     fhdr.NAXIS  =2 
     fhdr.NAXIS1 =fix(strmid(line,52,4))
     fhdr.NAXIS2 =fix(strmid(line,59,4)) 
     fhdr.FILENAME =fname
     fhdr.DATE_OBS  =dobs
     fhdr.TIME_OBS  =tobs
     fhdr.TELESCOP    ='SOHO'
     IF filt EQ 'none' THEN filt=strmid(line,79,5)
     fhdr.FILTER      =FILT
     fhdr.DETECTOR    =cam 
    if fhdr.DETECTOR eq 'EIT' then BEGIN
     	fhdr.SECTOR = strmid(line,87,4)
    	fhdr.INSTRUME='EIT'
    ENDIF ELSE fhdr.INSTRUME='LASCO'
     fhdr.r1col=fix(strmid(line,66,4))
     fhdr.r1row= fix(strmid(line,73,4))
     if ((fhdr.r1col+fhdr.NAXIS2)*2 gt 1044 or (fhdr.r1row+fhdr.NAXIS1)*2 gt 1044) then sumval=1 else sumval=2

     fhdr.r2col=(fhdr.r1col-1)+fhdr.NAXIS2*sumval
     fhdr.r2row= (fhdr.r1row-1)+fhdr.NAXIS1*sumval
     fhdr.SUMCOL = sumval
     fhdr.SUMROW = sumval

     fhdr.PLATESCL=GET_SEC_PIXEL(fhdr)
     fhdr.cdelt1=fhdr.PLATESCL
     fhdr.cdelt2=fhdr.PLATESCL
     fhdr.CUNIT1= 'arcsec'
     fhdr.CUNIT2= 'arcsec'
     keyhole=get_crota(fhdr.date_obs)
     if fhdr.DETECTOR eq 'EIT' then begin
       newcen=eit_point(fhdr.date_obs,strmid(fhdr.sector,0,3))
       fhdr.crpix1=newcen(0)
       fhdr.crpix2=newcen(1)
       fhdr.crota=0
     endif else begin
       fhdr.crota= get_roll_or_xy(fhdr,'ROLL',/AVG,/SILENT)
       asunc1 = GET_SUN_CENTER(fhdr,FULL=fhdr.NAXIS1,DOCHECK=gotit)
       fhdr.crpix1=asunc1.xcen;/(fhdr.naxis1/mvihdr.nx)
       fhdr.crpix2=asunc1.ycen;/(fhdr.naxis2/mvihdr.ny)
     endelse
     if keyhole eq 180 then begin
        fhdr.crpix1=fhdr.naxis1-fhdr.crpix1
        fhdr.crpix2=fhdr.naxis2-fhdr.crpix2
     endif
     fhdr.rsun=GET_SOLAR_RADIUS(fhdr, /NOEXTERNAL)
endif else begin

     fhdr=lasco_hdr_struct

     fhdr.NAXIS  =2 
     fhdr.NAXIS1 =ahdr.NAXIS1
     fhdr.NAXIS2 =ahdr.NAXIS2
     fhdr.FILENAME =fname
     fhdr.DATE_OBS  =ahdr.DATE_D$OBS
     fhdr.TIME_OBS  =ahdr.TIME_D$OBS
     fhdr.TELESCOP    =ahdr.TELESCOP
     fhdr.FILTER      =ahdr.filter
     fhdr.DETECTOR    =cam 
     if fhdr.DETECTOR eq 'EIT' then fhdr.SECTOR = ahdr.SECTOR

     fhdr.r1col=ahdr.r1col
     fhdr.r1row= ahdr.r1row
     if ((fhdr.p1col+fhdr.NAXIS2)*2 gt 1044 or (fhdr.p1row+fhdr.NAXIS1)*2 gt 1044) then sumval=1 else sumval=2

     fhdr.r2col=(fhdr.r1col-1)+fhdr.NAXIS2*sumval
     fhdr.r2row= (fhdr.r1row-1)+fhdr.NAXIS1*sumval
     fhdr.SUMCOL = sumval
     fhdr.SUMROW = sumval

     fhdr.PLATESCL=GET_SEC_PIXEL(fhdr)
     fhdr.cdelt1=fhdr.PLATESCL
     fhdr.cdelt2=fhdr.PLATESCL
     fhdr.CUNIT1= 'arcsec'
     fhdr.CUNIT2= 'arcsec'
     if fhdr.DETECTOR eq 'EIT' then begin
       newcen=eit_point(fhdr.date_obs,strmid(fhdr.sector,0,3))
       fhdr.crpix1=newcen(0)
       fhdr.crpix2=newcen(1)
       fhdr.crota=ahdr.crota1
     endif else begin
       fhdr.crota= get_roll_or_xy(fhdr,'ROLL',/AVG,/SILENT)
       asunc1 = GET_SUN_CENTER(fhdr,FULL=fhdr.NAXIS1,DOCHECK=gotit)
       fhdr.crpix1=asunc1.xcen;/(fhdr.naxis1/mvihdr.nx)
       fhdr.crpix2=asunc1.ycen;/(fhdr.naxis2/mvihdr.ny)
     endelse
     fhdr.rsun=GET_SOLAR_RADIUS(fhdr, /NOEXTERNAL)

     dr=!DPI/180.d
     fhdr=add_tag(fhdr,cos(fhdr.crota*dr),'PC1_1')
     fhdr=add_tag(fhdr,-sin(fhdr.crota*dr),'PC1_2')
     fhdr=add_tag(fhdr,sin(fhdr.crota*dr),'PC2_1')
     fhdr=add_tag(fhdr,cos(fhdr.crota*dr),'PC2_2')

     fhdr=add_tag(fhdr,fhdr.cdelt1/3600,'CDELT1A')
     fhdr=add_tag(fhdr,fhdr.cdelt2/3600,'CDELT2A')
     fhdr=add_tag(fhdr,fhdr.crpix1,'CRPIX1A')
     fhdr=add_tag(fhdr,fhdr.crpix2,'CRPIX2A')
     fhdr=add_tag(fhdr,'RA---TAN','CTYPE1A')
     fhdr=add_tag(fhdr,'DEC--TAN','CTYPE2A')
     fhdr=add_tag(fhdr,'deg','CUNIT1A')
     fhdr=add_tag(fhdr,'deg','CUNIT2A')

     if fhdr.DETECTOR eq 'EIT' then offset=0.
     if fhdr.DETECTOR eq 'C2' then offset=0.5 
     if fhdr.DETECTOR eq 'C3' then  offset=0.24
     orbit = get_orbit_fits(fhdr.date_obs+'T'+fhdr.time_obs,/retain)
     state = [orbit.hec_x, orbit.hec_y, orbit.hec_z]
     cspice_reclat, state, radius, longitude, latitude
     GEI_crota = (-1*cos(longitude-(11.2606*!dtor))*23.45-offset)
     print,GEI_crota
     fhdr=add_tag(fhdr,cos(GEI_crota*dr),'PC1_1A')
     fhdr=add_tag(fhdr,-sin(GEI_crota*dr),'PC1_2A')
     fhdr=add_tag(fhdr,sin(GEI_crota*dr),'PC2_1A') 
     fhdr=add_tag(fhdr,cos(GEI_crota*dr),'PC2_2A')

     state=get_stereo_coord(fhdr.date_obs+'T'+fhdr.time_obs,'SUN',system='gei',/novel)
     cspice_reclat, state, radius, ra, dec
     ra=ra * !radeg
     dec=dec * !radeg
     fhdr=add_tag(fhdr,ra,'CRVAL1A') 
     fhdr=add_tag(fhdr,dec,'CRVAL2A') 

endelse

return,fhdr

end
