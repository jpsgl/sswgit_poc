#! /bin/csh -m
# $Id: lasco_pretties.csh,v 1.2 2013/05/15 18:43:01 mcnutt Exp $
# $Log: lasco_pretties.csh,v $
# Revision 1.2  2013/05/15 18:43:01  mcnutt
# *** empty log message ***
#
# Revision 1.1  2011/05/02 18:34:02  mcnutt
# lasco prettie automation
#
# Made for automatic real-time application
# -32 is needed because this is a 64-bit computer. It will make the SPICE software work!
echo $HOME
source /net/cronus/opt/nrl_ssw_setup
setup
idl7 $HOME/secchi/idl/nrlgen/lascoeit/make_lasco_pretties.idl

