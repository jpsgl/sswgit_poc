pro update_lasco_hdrs, d0,d1
;$Id: update_lasco_hdrs.pro,v 1.3 2017/12/19 23:01:47 nathan Exp $
;
; Project     :  SOHO - LASCO/(C2 C3)
;                   
; Name        : update_lasco_hdrs
;               
; Purpose     : fix LZ level-0.5 FITS header in existing file
;               
; Input     ; 	d0,d1	YYMMDD	date range to fix; does both C2 and C3
;
; Keywords:
;
; Use         : IDL> 
;    
; Calls       : 
;
; Category    : 
;               
; $Log: update_lasco_hdrs.pro,v $
; Revision 1.3  2017/12/19 23:01:47  nathan
; commit current versions
;
; Revision 1.2  2012/02/15 17:58:01  nathan
; remove gzip!
;
; Revision 1.1  2012/02/15 17:32:44  nathan
; modified update_level1_hdrs.pro
;
; Revision 1.4  2011/11/16 22:16:50  nathan
; account for rectify correction to image
;
; Revision 1.3  2011/11/09 22:20:54  nathan
; minor adjustments
;
; Revision 1.2  2011/11/08 23:01:42  nathan
; log file
;
; Revision 1.1  2011/11/08 22:31:09  nathan
; fix sign on CROTA
;


info="$Id: update_lasco_hdrs.pro,v 1.3 2017/12/19 23:01:47 nathan Exp $"
len=strlen(info)
histinfo=strmid(info,5,40)

dbdir=getenv('REDUCE_DB')
;IF dbdir EQ '' THEN message,'You must exit IDL and source l1.env.'	
IF keyword_set(REPROCESS) THEN message,'/REPROCESS set.',/info

IF strmid(d0[0],0,6) NE 'SIMPLE' THEN BEGIN
; input is dates
    IF n_params() EQ 1 THEN d1=d0
    logpath = getenv_slash ('REDUCE_LOG')
    logfile = logpath+'updates/update_lasco_hdrs_'+d0+'_'+d1+'.log'
    print,'Opening ',logfile
    wait,2
    openw,lulog,logfile,/GET_LUN,/append
    get_utc,now,/ecs
    printf,lulog,'Starting update_lasco_hdrs.pro at ',now
    ut0=yymmdd2utc(d0)
    ut1=yymmdd2utc(d1)
    ut=ut0
ENDIF ELSE BEGIN
    ninp=1
    filelist='none'
ENDELSE

FOR i=ut0.mjd,ut1.mjd DO BEGIN
    ;get_utc,now,/ecs
    ; just use one time for new DATE
    ut.mjd=i
    ymd=utc2yymmdd(ut)
    dir=getenv_slash('LZ_IMG')+'level_05/'+ymd
    ;c2files=file_search(dir+'/c2/*.fts')
    ;c3files=file_search(dir+'/c3/*.fts')
    c4files=file_search(dir+'/c4/*.fts')
    help,ymd,c2files,c3files,c4files
    wait,1
    ;c23files=[c2files,c3files]
    c23files=c4files
    w=where(c23files)
    IF w[0] GE 0 Then BEGIN
    	c23files=c23files[where(c23files)]
	nf=n_elements(c23files)
	FOR j=0,nf-1 DO BEGIN
    	    fnj=c23files[j]
	    hdr=headfits(fnj)
    	    h=lasco_fitshdr2struct(hdr)
    	    diff=h.cdelt1-fxpar(hdr,'cdelt1')
	    IF abs(diff) GT 0.1 THEN BEGIN
		printf,lulog,h.date_obs+' '+h.filename+' fixed CDELT'
	    ENDIF
	    fxhmodify,fnj,'CDELT1',h.cdelt1
	    fxhmodify,fnj,'CDELT2',h.cdelt2
    	    fxhmodify,fnj,'HISTORY',histinfo+' fixed CDELT'
	    ;fxhmodify,fnj,'DATE',now
	ENDFOR
    ENDIF
    

ENDFOR
get_utc,now,/ecs
printf,lulog,'Ending update_lasco_hdrs.pro at ',now

 free_lun,lulog

end
