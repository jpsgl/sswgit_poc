;+
; $Id: adjust_all_date_obs.pro,v 1.2 2017/08/04 21:51:47 nathan Exp $
; NAME:
;       ADJUST_ALL_DATE_OBS
;
; PURPOSE:
;       This function returns a structure of three string elements containing the
;       adjusted date-obs and time-obs for a given C1, C2, C3, or EIT image header
;   	and approximate error.
;   	It is basically a wrapper for time_correction.pro.
;
; CATEGORY:
;       LASCO DATA_ANAL
;
; CALLING SEQUENCE:
;       adj = adjust_all_date_obs(hdr) 
;
; INPUTS:
;       hdr: A C1, C2, C3, or C4 image header
;
; OPTIONAL INPUTS:
;       verbose: print diagnostic messages.
;                adj = adjust_all_date_obs(hdr,/verbose)
;
; OUTPUTS:
;      A two element structure of DATE and TIME containing the adjusted
;      DATE_OBS and TIME_OBS.
;
; Optional Outputs:
;   	OFFSET= orig time - adj time in seconds
;
; Example:
;
; IDL[iapetus]>print,h.date_obs,' ',h.time_obs
; 2010/01/01 22:18:03.575
; IDL[iapetus]>adj=adjust_all_date_obs(h,offset=dt)           
; IDL[iapetus]>help,dt
; DT              FLOAT     =       227.160
; IDL[iapetus]>help,/str,adj
; ** Structure <85fed54>, 3 tags, length=36, data length=36, refs=1:
;   DATE            STRING    '2010/01/01'
;   TIME            STRING    '22:14:16.415'
;   ERR             STRING    '-15.0000 , 15.0000' ;; from time_correction.pro
;
; SCCS Revision 11/28/12 @(#)adjust_all_date_obs.pro	1.4 
;
; MODIFICATION HISTORY:
; $Log: adjust_all_date_obs.pro,v $
; Revision 1.2  2017/08/04 21:51:47  nathan
; return 0 change if offset>500sec
;
;       Written by:     Ed Esfandiari, July 2002 - equivalent to adjust_date_obs_new1.pro
;       09/22/05  Ed Esfandiari -  Check hhdr and if time_obs='' assume level-1
;                                  file and do not adjust date_time (already adjusted). 
;   2010/11/04, nbr - added OFFSET= output for get_starref(_ed).pro
;   2012/11/28, nbr - Use time_correction2.pro in $SSW/packages/nrl/idl/nrlgen/lascoeit
;

function adjust_all_date_obs,hdr,verbose=verbose, OFFSET=offset

  adj= {date:'', time:'', err:''}

  shdr= hdr
  IF (DATATYPE(shdr) NE 'STC') THEN shdr=LASCO_FITSHDR2STRUCT(hdr) 
  tel= strupcase(STRTRIM(shdr.detector,2))
  date= shdr.date_obs
  time= STRTRIM(shdr.time_obs,2)

  ; Level-1 headers (and higher) are already adjusted for date and time. For these files,
  ; Unlike level-0.5 files, shdr.date_obs is set to 'yyyy-mm-ddThh:mm:ss.msc' and shdr.date 
  ; is set to ''. So check the shdr.time and if it is blank assume it is a level-1 hdr which
  ; does not need time correction:


  IF (time EQ '') THEN BEGIN
    adj.date= STRMID(date,0,4)+'/'+STRMID(date,5,2)+'/'+STRMID(date,8,2) ;=>yyyy/mm/dd
    adj.time= STRMID(date,11,12)
    adj.err= 'level-1 hdr, not adjusted'
    IF (KEYWORD_SET(verbose)) THEN PRINT,'level-1 hdr input: '+date +' not adjusted' 
    return, adj    
  ENDIF


     offset= time_correction2(ANYTIM2UTC(date+' '+time,/ecs),err,correction_string=cstr,verbose=verbose) 

  ;+ offset=[sec]

  offset2=offset
  IF (abs(offset) GT 500) THEN BEGIN
    message,'WARNING: error in time_correction2; delta_time='+trim(offset)+'; using 0 instead.',/info
    wait,5
    offset2=0
  ENDIF
  ; Subtrct the offset (changed to sec) from the date/time_obs after
  ; converting them to utc format:

  tai_time= UTC2TAI(ANYTIM2UTC(date+' '+time))
  adj_tai_time= tai_time - offset2
  adjusted= TAI2UTC(adj_tai_time)

  ; Change from utc format to string of form "yyyy-mm-ddThh:mm:ss.mmmZ"
  ; and pick up new date/time:

  stime= utc2str(adjusted)
  new_date= strmid(stime,0,4)+'/'+strmid(stime,5,2)+'/'+strmid(stime,8,2)
  new_time= strmid(stime,11,12)

  if(KEYWORD_SET(verbose)) then begin
    print,''
    print,'Camera= ',tel
    print,'Original date/time: ',shdr.date_obs,' ',shdr.time_obs
    print,'Adjusted date/time: ',new_date,' ',new_time
    print,''
  end

  adj.date= new_date
  adj.time= new_time 
  adj.err = strmid(trim(err),0,6)+' sec'

  return,adj

end
