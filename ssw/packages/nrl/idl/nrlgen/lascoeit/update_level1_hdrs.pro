pro update_level1_hdrs, d0,d1
;$Id: update_level1_hdrs.pro,v 1.5 2017/12/19 23:01:48 nathan Exp $
;
; Project     :  SOHO - LASCO/(C2 C3)
;                   
; Name        : update_level1_hdrs
;               
; Purpose     : fix level-1 FITS header in existing file
;               
; Input     ; 	d0,d1	YYMMDD	date range to fix; does both C2 and C3
;
; Keywords:
;
; Use         : IDL> 
;    
; Calls       : 
;
; Category    : 
;               
; $Log: update_level1_hdrs.pro,v $
; Revision 1.5  2017/12/19 23:01:48  nathan
; commit current versions
;
; Revision 1.4  2011/11/16 22:16:50  nathan
; account for rectify correction to image
;
; Revision 1.3  2011/11/09 22:20:54  nathan
; minor adjustments
;
; Revision 1.2  2011/11/08 23:01:42  nathan
; log file
;
; Revision 1.1  2011/11/08 22:31:09  nathan
; fix sign on CROTA
;


info="$Id: update_level1_hdrs.pro,v 1.5 2017/12/19 23:01:48 nathan Exp $"
len=strlen(info)
histinfo=strmid(info,5,40)

dbdir=getenv('REDUCE_DB')
IF dbdir EQ '' THEN message,'You must exit IDL and source l1.env.'	
IF keyword_set(REPROCESS) THEN message,'/REPROCESS set.',/info

IF strmid(d0[0],0,6) NE 'SIMPLE' THEN BEGIN
; input is dates
    IF n_params() EQ 1 THEN d1=d0
    logpath = getenv_slash ('REDUCE_LOG')
    logfile = logpath+'updates/update_level1_hdrs_'+d0+'_'+d1+'.log'
    print,'Opening ',logfile
    openw,lulog,logfile,/GET_LUN
    ut0=yymmdd2utc(d0)
    ut1=yymmdd2utc(d1)
    ut=ut0
ENDIF ELSE BEGIN
    ninp=1
    filelist='none'
ENDELSE

FOR i=ut0.mjd,ut1.mjd DO BEGIN
    get_utc,now,/ecs
    ut.mjd=i
    ymd=utc2yymmdd(ut)
    dir='/net/corona/lasco/level_1/'+ymd
    cmd='gunzip '+dir+'/c[23]/*.gz'
    print,cmd
    wait,1
    spawn,cmd,/sh
    wait,1
    c2files=file_search(dir+'/c2/*.fts')
    c3files=file_search(dir+'/c3/*.fts')
    help,c2files,c3files
    c23files=[c2files,c3files]
    w=where(c23files)
    IF w[0] GE 0 Then BEGIN
    	c23files=c23files[where(c23files)]
	nf=n_elements(c23files)
	FOR j=0,nf-1 DO BEGIN
    	    fnj=c23files[j]
	    x=lasco_readfits(fnj,h,/no_img,/silent)
	    tcr = adjust_hdr_tcr(h)
	    note='Final Correction + fixed sign'
	    rect=get_crota(h.date_obs)
	    tcroll=tcr.roll-rect
	    diff=tcroll+h.crota1
	    IF abs(diff) GT 0.001 THEN BEGIN
	    ;if 0 then begin
		note='Updated '+note
		print,h.date_obs+' '+h.filename+' New CROTA varies by '+trim(diff)
		wait,1
		printf,lulog,h.date_obs+' '+h.filename+' New CROTA varies by '+trim(diff)
	    ENDIF
	    fxhmodify,fnj,'CROTA',tcroll,note
	    fxhmodify,fnj,'CROTA1',tcroll
	    fxhmodify,fnj,'CROTA2',tcroll
	    IF rect EQ 180 THEN fxhmodify,fnj,'RECTIFY',180,' post-proc hdr correction'
    	    fxhmodify,fnj,'HISTORY',histinfo+' fixed CROTA sign'
	    ;fxhmodify,fnj,'DATE',now
	ENDFOR
    ENDIF
    cmd='gzip '+dir+'/c[23]/*.fts'
    print,cmd
    spawn,cmd,/sh
    

ENDFOR

 free_lun,lulog

end
