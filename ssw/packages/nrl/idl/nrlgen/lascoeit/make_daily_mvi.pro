; $Id: make_daily_mvi.pro,v 1.15 2017/12/19 23:01:47 nathan Exp $
; MAKE_DAILY_MVI, yymmdd, camera
;
; INPUTS:
;  dte:	'970502'
;  cam:	'c2' or 'c3' or 'eit_195' or 'eit_284' or 'eit_171' or 'eit_304'
;
; KEYWORDS:
;  /NOMVI   	Write mpeg with existing MVI file, if available
;  SAVE=	string, filename of result, default is yymmdd_cam.mvi
;  RATIO:	currently unused
;  /QL:		Use quicklook and save in $MVIS/daily/; 
;		otherwise use LZ and save in $MVIS/daily/yyyy_mm/
;  FOUND=	Set to 1 if daily movie is made
;  /NOSAVE:	Do not save to file, but wrunmovie when finished
;  /MPG:	Write MPEG in $MVIS/daily/mpg/
;  /REDO:	Checks if movie should be redone based on header info; if set to 
;		an ECS date, redo if before this date
;  FILTER=	Set = to unusual filter: so far, 'orange'
;  /FFV:	Full size image 
;  /DEEP    	Do not filter on EXPTIME
;  /RDIFFMPG	make running difference MPG from file list using scc_mkmovie
;  /RDIFFMVI	make running difference MVI from file list using scc_mkmovie
;  /rdiff_only  only make running difference movie
;
; $Log: make_daily_mvi.pro,v $
; Revision 1.15  2017/12/19 23:01:47  nathan
; commit current versions
;
; Revision 1.14  2015/05/19 16:24:44  nathan
; set_plot x when finished
;
; Revision 1.13  2012/08/02 14:47:39  mcnutt
; add time stamp to difference mpg movie only
;
; Revision 1.12  2012/08/01 21:01:09  nathan
; if RDIFF loadct,0; adjust RDIFF mkmovie parameters; set_plot,z
;
; Revision 1.11  2012/08/01 19:32:10  nathan
; implement /RDIFFMVI and /RDIFFMPG instead of /DORDIFF
;
; Revision 1.10  2012/08/01 17:44:33  mcnutt
; changed rdiff directory for mvi
;
; Revision 1.9  2012/08/01 17:36:31  mcnutt
; added option to make runing difference movie using scc_mkmovie
;
; Revision 1.8  2012/07/27 20:36:53  nathan
; set_plot,z and set tt_font so works without X
;
; Revision 1.7  2012/07/13 23:05:13  nathan
; changes for new No-X scc_mkmovie.pro
;
; Revision 1.6  2011/11/29 18:16:42  nathan
; fix /NOSAVE
;
; Revision 1.5  2011/11/07 22:43:22  nathan
; fix use_model for new usage
;
; Revision 1.4  2010/12/23 16:36:31  nathan
; add /debug; adjust c2 min max
;
; Revision 1.3  2010/12/22 23:44:43  nathan
; adjust c2 bmin, box, box_ref, automax
;
; Revision 1.2  2010/11/22 16:50:05  nathan
; use scc_mkmovie so solar north is always up
;
;** mods:  10/1/97 SEP Changed C2/C3 size to 512x512
;	   08/02/99 NBR Add AUTOMAX keyword for MKMOVIE
;	   99/02/09 NBR Add 284 option
;	   99/02/19 NBR Add 171 option
;	   99/05/07 NBR Change naxis1 to 1024 for C2 and C3
;	99/08/25 NBR  Omit C3 exptime LT 15 sec.; use FIXGAPS=2
;	1999/12  NBR  Use LG_MASK_OCC
;	2000/01  NBR  Change EIT input and output names
;	2000/12/04  NBR  Increase bmax for C2 and C3 (has no effect)
;	2001/1/2, nbr - Add NOSAVE keyword; only make eit304 if >10 frames
;	2001/1/3, nbr - Change input tel to cam; add WIN_INDEX common block, MPG keyword
;	2001/1/31,nbr - Remove BAD_SKIP from mkmovie call
;	2001.3.1, nbr - Do any camera if GT 10 frames, always do 195A
;	2001.3.6, nbr - Omit C2 and C3 frames not between .7x and 1.3x default exptime
;	2001.3.21,nbr - Add REDO keyword and function; use expmin and expmax; set FIXGAPS=1;
;			reset EIT bmin/bmax
;	2001.4.2, nbr - Allow REDO to = ECS date and redo if prior to date
;	2001.4.23,nbr - Set automaxmin=0
;	2001.6.13, nbr - Add c3 any_year feature for before May 1996; move W256
;	2001.6.14, nbr - Set automaxmin=1
;	2001.6.15, nbr - Add FILTER keyword
;	2001.11.02, nbr - Add SCCS version stamp
;	2002.06.13, nbr - Change location of color tables
;	030129	jake	changed naxis1 on c2 to 512 b/c c2 are all 512 right now
;	030714 jake added PNG keyword
;	2003.09.11, nbr - Fix color table location for EIT 195
;	2003.10.15, nbr - Remove win_index from COMMON WIN_INDEX (conflict with wrunmovie.pro)
;	2003.12.17, KB - Add FFV keyword
;       2005.03.28, KB - Fix a bizarre bug that was preventing movies being made. See line 222-226-ish
;                        the list of filenames to be made into a movie was turning out to be a
;                        single-element strarr. Therefore, the movies were not getting made. Not good.
;                      - Fix bug in mvi2mpg call -- changed svname to be a string, not a strarr. Line 290
;   	2008-05-14, nbr - Added /NOMVI and _EXTRA
;   	2008-05-16, nbr - Added device,decomposed commands around mkmovie call; if not /QL, remove QL mvi and mpg
;   	2010-05-17, nbr - Always use_model=1 (ANY_YEAR)
;   	2010/11/15, nbr - Add /DEEP
;
;   	%H% %W%   NRL LASCO IDL Library
;-

PRO MAKE_DAILY_MVI, dte, cam, SAVE=save, RATIO=ratio, QL=ql, FOUND=found, NOSAVE=nosave, MPG=mpg, $
	REDO=redo,FILTER=filter, PNG=png, FFV=ffv, NOMVI=nomvi, DEEP=deep, DEBUG=debug, RDIFFMPG=rdiffmpg, $
	RDIFFMVI=rdiffmvi, rdiff_only=rdiff_only, _EXTRA=_extra

COMMON WIN_INDEX, base	;, win_index

   found=0

   utc = YYMMDD2UTC(dte)
   mvidir = GETENV('MVIS')+'/daily/'
   ;mvidir='/home/nathan/test/mvi/'
   datdir = concat_dir(GETENV('LASCO_DATA'),'color')
   ;tel = tele
   IF KEYWORD_SET(SAVE) THEN svfile = save ELSE svfile=dte+'_'+cam
   IF keyword_set(NOMVI) THEN mpg=1
   qlmviname = mvidir+svfile
   qlmpgname = mvidir+'mpg/'+dte+'_'+cam
   qlmpgdir = concat_dir(mvidir,'mpg')
    dte_str=UTC2STR(utc,/ecs)
    mondir=STRMID(dte_str,0,4)+'_'+STRMID(dte_str,5,2)
    mpgddir = concat_dir(qlmpgdir,mondir)
    
   IF KEYWORD_SET(QL) THEN BEGIN
	img_dir = GETENV('QL_IMG') 
	svname = qlmviname
	mpgname = qlmpgname
	mviddir=mvidir
	mpgdir=qlmpgdir
   ENDIF ELSE BEGIN
   
	img_dir = GETENV('LZ_IMG')
	IF not keyword_set(NOSAVE) THEN BEGIN
	    cmd='/bin/rm '+qlmviname+'*'
	    print,cmd
	    spawn,cmd,/sh
	    cmd='/bin/rm '+qlmpgname+'*'
	    print,cmd
	    spawn,cmd,/sh
	ENDIF
	mviddir = concat_dir(mvidir,mondir)	
	svname = concat_dir(mviddir,svfile)
	mpgname= concat_dir(mpgddir,dte+'_'+cam)
    	mpgdir=mpgddir
   ENDELSE
   IF ~keyword_set(MPG) THEN mpgdir=0

   utc.mjd = utc.mjd+1
   dte2 = UTC2YYMMDD(utc)
         ref = 'DECODED'
         scale = 8

; MVI Defaults set for EIT

    naxis1 = 512
    naxis2 = 512
    bmin = 0.0
    bmax = 4.4	; nbr, 3/21/01
    coords=[0,1023,0,1023]
    box=[11,210,821,1010]
    box_ref=30.

    doratio=0
    use_model=0
    use_mask=0
    calimg_off=0
    dbias=0
    dlogscl=1
    automaxmin=0

   CASE (cam) OF 
      'c2' : BEGIN
		tel = 'c2'
                fw = 'Orange'
                pw = 'Clear'
		expmin=5.
		expmax=35.
                bmin = 0.85		; ** set 02/11/99, NBR
                bmax = 2.25		; ** set 12/4/00, NBR (was 1.6)
;;                coords=[0,1023,128,895]
                box=0	;[261,760,696,795]
                box_ref=2900.	;** no summing, not divided by exptime
                use_model=1
                ;IF utc.mjd  GT 51879 THEN use_model=2 ELSE use_model=1
			; set 1/2/01 for new reflex feature appearing after 2000/12/01, NBR
		automaxmin=0
		scale = 4
         	RESTORE, concat_dir(datdir,'c2_mpg_col.dat')
		doratio=1
		use_mask=1
		calimg_off=1
		dlogscl=0
		
             END
      'c3' : BEGIN
		tel = 'c3'
		fw = 'Clear'
                pw = 'Clear'
                naxis1 = 256	; ** 5/7/99 - changed from 512 t0 1024, NBR
                naxis2 = 256	; ** 6/19/01 - changed to 256/256
		expmin=15.	
;		expmin=8.	;	temporary for 030130-31
		expmax=30.
                bmin = 0.87
                bmax = 1.4	; ** set 12/4/00, NBR (was 1.3)
;;                coords=[0,1023,224,799]
                box=[350,760,80,160]
                box_ref=700.	;** no summing, not divided by exptime
                use_model=1
		;IF utc.mjd  LT 50204 THEN use_model=1 ELSE use_model=2
			; use any_year for before 1996/05/01, NBR
			; for getbkgimg, 3 = yearly model, 2 = current year, 1 = any-year model
			; for scc_mkmovie, 2 = any-year, 1 = current year
		automaxmin=1	; set 6/14/01, NBR
		dbias=0 	; set 2/9/00, NBR
	 	RESTORE, concat_dir(datdir,'c3_mpg_col.dat')
                IF keyword_set(FILTER) THEN BEGIN
		   naxis1 = 512
		   naxis2 = 256
		   expmin = 4.
		   expmax = 100.
		   filt = STRMID(STRLOWCASE(filter),0,2)
		   IF filt EQ 'or' THEN fw = 'Orange' 
		   mpgname = mpgname+filt
		   svname = svname+filt
		   RESTORE, concat_dir(datdir,'c2_mpg_col.dat')
		ENDIF
		doratio=1
		use_mask=1
		calimg_off=1
		dlogscl=0
             END
      'eit_195' : BEGIN
                tel = 'c4'
                pw = '195A'
         	RESTORE, concat_dir(datdir,'195_mpg_col.dat')
		;RESTORE, mvidir+'195_mpg_col.dat'
		scale = 4
             END
      'eit_284' : BEGIN
                tel = 'c4'
                pw = '284A'
                bmax = 4.2	; nbr, 3/21/01
		RESTORE,  concat_dir(datdir,'284_mpg_col.dat')
             END
      'eit_171' : BEGIN
                tel = 'c4'
                pw = '171A'
		RESTORE, concat_dir(datdir,'171_mpg_col.dat')
             END
      'eit_304' : BEGIN
                tel = 'c4'
                pw = '304A'
		RESTORE, concat_dir(datdir,'304_mpg_col.dat')
             END
      ELSE : BEGIN
                PRINT, '%%MAKE_DAILY_MVI: Unknown telescope: ', tel
                RETURN
             END
   ENDCASE
        
    IF keyword_set(DEEP) THEN BEGIN
    	expmin=0.
	expmax=300.
    ENDIF
    
set_plot,'z'
   TVLCT, r,g,b

   mpgname=mpgname+'.mpg'
   svname=svname+'.mvi'
   ;stop
   IF KEYWORD_SET(NOSAVE) THEN svname=0
   dir = img_dir+'/level_05/'+dte+'/'+tel+'/'
   dir2 = img_dir+'/level_05/'+dte2+'/'+tel+'/'
   stc = IMG_HDR_TXT2STRUCT(dir+'img_hdr.txt')

   IF (DATATYPE(stc) NE 'STC') THEN RETURN

   IF (tel NE 'c4') THEN $
      good = WHERE( (stc.fw EQ fw) AND (stc.pw EQ pw) AND $
                   ((stc.naxis1 GE naxis1) AND (stc.naxis2 GE naxis2)) AND $
                    (stc.lp NE 'Sum/Diff') AND $
		    (stc.exptime LT expmax and stc.exptime GT expmin) ) $
   ELSE $	;** take any filter for EIT
      good = WHERE( (stc.pw EQ pw) AND $
                   ((stc.naxis1 GE naxis1) AND (stc.naxis2 GE naxis2)) AND $
                    (stc.lp NE 'Sum/Diff') )

   ngood = N_ELEMENTS(good)
   IF ngood EQ 1 THEN BEGIN
      PRINT, '%%MAKE_DAILY_MVI: No images found for: ', dir
      RETURN
   ENDIF
   list=strarr(ngood)                   ; KB 050328
   list = stc(good).filename
   for i=0, ngood-1 DO BEGIN            ; KB 050328
   list[i] = dir+list[i]
   ;list=dir+list
   endfor                               ; KB 050328

;list=list[0:9]
   nlist=n_elements(list)
   help,list

   IF NOT(keyword_set(REDO)) THEN BEGIN
	;W256
      	!P.FONT=0
   	;DEVICE, set_FONT='-adobe-helvetica-medium-r-normal--24-240-75-75-p-130-iso8859-1'
	;device,set_font='Helvetica',/tt_font
   ENDIF

   IF (nlist GE 10) or (cam EQ 'eit_195') THEN BEGIN

     IF keyword_set(REDO) THEN IF file_exist(svname) THEN BEGIN
	dtmod = FILE_DATE_MOD(svname,/date_only)
	typredo = DATATYPE(redo)
	old_date=0b
	IF typredo EQ 'STR' THEN old_date = dtmod LT redo
	too_short = 0b
	IF NOT(old_date) THEN BEGIN
	   openr,lu,svname,/get_lun
	   read_mvi,lu,file_hdr,ihdrs,imgs,swapflag, /NO_COLOR
	   close,lu
	   free_lun,lu
	   IF file_hdr.nf LT ngood THEN too_short = 1
	ENDIF
	IF too_short THEN BEGIN
	   print,'Redoing ',string(format='(a18)',svfile),': file_hdr= ',string(file_hdr.nf,format='(i3)'),',   good= ',string(ngood,format='(i3)'),'  ',dtmod
	   print
	ENDIF ELSE IF old_date THEN BEGIN
	   print,'Redoing ',string(format='(a18)',svfile),'  ',dtmod
	   print
	ENDIF ELSE BEGIN
	   print,string(format='(a18)',svfile),' OK: file_hdr= ',string(file_hdr.nf,format='(i3)'),',   good= ',string(ngood,format='(i3)'),'  ',dtmod
	   GOTO, skipit
	ENDELSE
	
     ENDIF

      ;tmp = LASCO_READFITS(list(0), h, /NO_IMG)
      ;IF (DATATYPE(h) NE 'UND') THEN BEGIN 
      ;   sum = (h.sumrow+1) * h.lebxsum
      ;   IF (sum NE 1) THEN pan=sum/2 ELSE pan=0.5
      ;ENDIF ELSE pan=0.5
      ;pan = 0.5
      IF keyword_set(NOMVI) and file_exist(svname) THEN goto,skipmvi
      print,'Compiling ',svname
    	if not file_exist(mviddir) THEN spawn,['mkdir',mviddir],/noshell
      IF keyword_set(FFV) THEN BEGIN   ; ** KB
      		pan = 1024
      ENDIF ELSE BEGIN
      		pan = 512
      ENDELSE
 ;     device,/decomposed

    IF keyword_set(DEBUG) THEN BEGIN
    	help,bmin,bmax,doratio,pan,use_mask,box,use_model,svname,box_ref,automaxmin
	wait,3
    ENDIF      

      IF ~keyword_set(rdiff_only) THEN $
      scc_MKMOVIE, list, bmin, bmax, RATIO=doratio, PAN=pan, FIXGAPS=1, LG_MASK_OCC=use_mask, $
            COORDS=coords, BOX=box, USE_MODEL=use_model, SAVE=svname, REF_BOX=box_ref, $
	    AUTOMAX = automaxmin, DBIAS=dbias, log_scl=dlogscl, NOCAL=calimg_off, /DOROTATE, $
	    OUTER=use_mask, LIMB=use_mask, /BAD_SKIP, _EXTRA=_extra, DEBUG=debug, MPG=mpgdir, $
	    SVADDTIMES=[0,1.5]

      IF keyword_set(RDIFFMPG) or keyword_set(RDIFFMVI) THEN BEGIN
            bmin = -15.
            bmax = 15.
            automaxmin = 0
	    break_file,svname,di1,pa1,ro1,sfx1
	    strput,ro1,'d',7
	    IF keyword_set(RDIFFMVI) 	THEN SVADDTIMES=[0] ELSE SVADDTIMES=[1.5]
	    IF keyword_set(RDIFFMVI) 	THEN rsvname=concat_dir(pa1,ro1+'.mvi') $
	    	    	    	    	ELSE rsvname=concat_dir(mpgddir,ro1+'.mpg')
	    IF keyword_set(RDIFFMPG) THEN BEGIN
	    	SPAWN, 'if [ ! -d ' + mpgddir + ' ]; then mkdir ' + mpgddir + ' ; fi', /SH
    	    	IF ~keyword_set(RDIFFMVI) THEN mpgddir=0
	        IF keyword_set(RDIFFMVI) THEN SVADDTIMES=[SVADDTIMES,1.5]
	    ENDIF ELSE mpgddir=0
	    
set_plot,'z'
	    loadct,0
	    
    	    scc_MKMOVIE, list, bmin, bmax, /RUNNING_DIFF, PAN=pan, FIXGAPS=1, LG_MASK_OCC=use_mask, $
        	COORDS=coords, BOX=box, SAVE=rsvname, REF_BOX=box_ref, SVADDTIMES=svaddtimes, $
		AUTOMAX = automaxmin, DBIAS=dbias, log_scl=dlogscl, NOCAL=calimg_off, /DOROTATE, $
		OUTER=use_mask, LIMB=use_mask, /BAD_SKIP, _EXTRA=_extra, DEBUG=debug, MPG=mpgddir
      
      ENDIF

      ;stop
      skipmvi:
;      device,decomposed=0
      IF keyword_set(MPG) and keyword_set(NOMVI) THEN BEGIN
    	if not file_exist(mpgddir) THEN spawn,['mkdir',mpgddir],/noshell
    	svname=svname[0]     ; KB 050328  did this because svname was being passed as a strarr[1], which mvi2mpg didn't like...
	print
	print,'Saving ',mpgname
	print
	MVI2MPG,file=svname,MPEG_NAME=mpgname,/USE_CURR_COLORS,/FULL, SCALE=scale, REF=ref, /TIMES, PNG=png
      ENDIF
      ;FOR j=0,n_elements(win_index)-1 DO WDELETE,win_index[j]
   ENDIF
   found=1
   skipit:

set_plot,'x'

END
