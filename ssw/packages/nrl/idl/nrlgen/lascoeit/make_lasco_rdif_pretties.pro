pro make_lasco_rdif_pretties,yyyymmdd,cmbonly=cmbonly, C2=c2, C3=c3,LENGTH=length
;+
; $Id: make_lasco_rdif_pretties.pro,v 1.1 2013/05/15 18:44:54 mcnutt Exp $
; NAME:
;	make_lasco_pretties
;
; PURPOSE:
;   	To create daily pretties from lasco quick look fits files
;
; CATEGORY:
;	LASCO UTIL
;
; CALLING SEQUENCE:
;	make_lasco_pretties,yyyymmdd
;
; INPUTS:
;	yyyymmddd:	Year month day
;
; KEYWORD PARAMETERS:
;
; OUTPUTS:
;   	LASCO daily pretties in $SECCHI_PNG/soho/[c2,c3,lasco]/yyyymmdd/[512,1024]
;
; RESTRICTIONS:
;   	NRL only and wscc_combine_mvi must be complied before running make_lasco_pretties
;
; $Log: make_lasco_rdif_pretties.pro,v $
; Revision 1.1  2013/05/15 18:44:54  mcnutt
; creates lasco daily rdif pngs
;
; Revision 1.7  2012/01/23 20:55:52  mcnutt
; checking version
;
; Revision 1.6  2011/10/13 19:13:58  nathan
; Add /C2,/C3,LENGTH= keywords; implement scc_suffix() for filename
;
; Revision 1.5  2011/07/26 18:43:35  mcnutt
; selects first c3mvi header
;
; Revision 1.4  2011/03/16 17:24:55  mcnutt
; added new os numbers for c2 and c3
;
; Revision 1.3  2011/02/22 19:56:58  mcnutt
; change size of times to 3
;
; Revision 1.2  2011/02/11 17:10:17  mcnutt
; uses scc_mkframe values from make_daily_mvi.pro
;
; Revision 1.1  2011/02/08 18:52:23  mcnutt
; new to create lasco daily pretties
;
;

date=strmid(yyyymmdd,0,4)+'-'+strmid(yyyymmdd,4,2)+'-'+strmid(yyyymmdd,6,2)

device,decompose=0
   datdir = concat_dir(GETENV('LASCO_DATA'),'color')

IF not keyword_set(C3) THEN BEGIN
  nz2=0
  c2files=las_read_summary(date=date,tel='C2',SOURCE='ql')
  if datatype(c2files) ne 'STC' then z2=-1 else z2=where(c2files.os eq 3389, nz2)
  if datatype(c2files) eq 'STC' and nz2 LT 2 then z2=where(c2files.os eq 4092,nz2)
  if datatype(c2files) eq 'STC' and nz2 LT 2 then z2=where(c2files.os eq 4136,nz2)
  if n_Elements(z2) gt 1 then begin
   IF keyword_set(LENGTH) THEN IF length LT nz2 THEN z2=z2[0:length-1]
    ;loadct,3
       c2names=c2files(z2).dir+c2files(z2).filename
       IF not keyword_set(cmbonly) THEN BEGIN

      ;values from secchi/idl/nrlgen/lascoeit/make_daily_mvi.pro
                dbias=0
                bmin = 0.85		; ** set 02/11/99, NBR
                bmax = 2.25		; ** set 12/4/00, NBR (was 1.6)
;;                coords=[0,1023,128,895]
                box=0	;[261,760,696,795]
                box_ref=2900.	;** no summing, not divided by exptime
                use_model=1
                ;IF utc.mjd  GT 51879 THEN use_model=2 ELSE use_model=1
			; set 1/2/01 for new reflex feature appearing after 2000/12/01, NBR
		automaxmin=0
		scale = 4
		doratio=1
		use_mask=1
		calimg_off=1
		dlogscl=0

          RESTORE, concat_dir(datdir,'c2_mpg_col.dat')
          TVLCT, r,g,b

        outmovie=getenv('SECCHI_PNG')+'/soho/c2_rdif/'+yyyymmdd+'/1024/
        spawn,'mkdir -p '+outmovie 
        wait,2
       outmovie=outmovie+'yyyymmdd_hhmmss_rdlc2.png'
;  scc_mkmovie,c2names,use_model=1,0.810,1.485,/dorotate,/ratio,/fixgaps,/limb,/times,/mask_occ,/outer,box=box,save=outmovie

        outmovie=getenv('SECCHI_PNG')+'/soho/c2_rdiff/'+yyyymmdd+'/1024/
        spawn,'mkdir -p '+outmovie 
        wait,2
       outmovie=outmovie+'yyyymmdd_hhmmss_rdlc2.png'
;  scc_mkmovie,c2names,use_model=1,0.810,1.485,/dorotate,/ratio,/fixgaps,/limb,/times,/mask_occ,/outer,box=box,save=outmovie

         scc_MKMOVIE, c2names, -10.,40., RATIO=doratio, PAN=pan, FIXGAPS=1, LG_MASK_OCC=use_mask, $
            COORDS=coords, BOX=box, USE_MODEL=use_model, SAVE=outmovie, REF_BOX=box_ref, $
	    AUTOMAX = automaxmin, DBIAS=dbias, log_scl=dlogscl, NOCAL=calimg_off, /DOROTATE, $
	    OUTER=use_mask, LIMB=use_mask, /BAD_SKIP, times=3, _EXTRA=_extra, DEBUG=debug,/RUNNING_DIFF
stop
         scc_MKMOVIE, c2names, bmin, bmax, RATIO=doratio, PAN=pan, FIXGAPS=1, LG_MASK_OCC=use_mask, $
            COORDS=coords, BOX=box, USE_MODEL=use_model, SAVE=outmovie, REF_BOX=box_ref, $
	    AUTOMAX = automaxmin, DBIAS=dbias, log_scl=dlogscl, NOCAL=calimg_off, /DOROTATE, $
	    OUTER=use_mask, LIMB=use_mask, /BAD_SKIP, times=3, _EXTRA=_extra, DEBUG=debug,/RUNNING_DIFF

      endif
   endif
ENDIF

IF not keyword_set(C2) THEN BEGIN
   c3files=las_read_summary(date=date,tel='C3',SOURCE='ql')
   if datatype(c3files) ne 'STC' then z3=-1 else z3=where(c3files.os eq 3387)
   if datatype(c3files) eq 'STC' and z3(0) eq -1 then z3=where(c3files.os eq 4093)
   if datatype(c3files) eq 'STC' and z3(0) eq -1 then z3=where(c3files.os eq 4137)
   if datatype(c3files) eq 'STC' and z3(0) eq -1 then z3=where(c3files.os eq 4138)
   if n_Elements(z3) gt 1 then begin
       IF keyword_set(LENGTH) THEN z3=z3[0:length-1]
       c3names=c3files(z3).dir+c3files(z3).filename

       IF not keyword_set(cmbonly) THEN BEGIN
          img=sccreadfits(c3names(0),hdr)

      ;values from secchi/idl/nrlgen/lascoeit/make_daily_mvi.pro
                bmin = 0.87
                bmax = 1.4	; ** set 12/4/00, NBR (was 1.3)
;;                coords=[0,1023,224,799]
                box=[350,760,80,160]
                box_ref=700.	;** no summing, not divided by exptime
                use_model=1
		;IF utc.mjd  LT 50204 THEN use_model=1 ELSE use_model=2
			; use any_year for before 1996/05/01, NBR
			; for getbkgimg, 3 = yearly model, 2 = current year, 1 = any-year model
			; for scc_mkmovie, 2 = any-year, 1 = current year
		automaxmin=1	; set 6/14/01, NBR
		dbias=0 	; set 2/9/00, NBR
		doratio=1
		use_mask=1
		calimg_off=1
		dlogscl=0

;    loadct,1
          RESTORE, concat_dir(datdir,'c3_mpg_col.dat')
          TVLCT, r,g,b
          outmovie=getenv('SECCHI_PNG')+'/soho/c3/'+yyyymmdd+'/1024/
          spawn,'mkdir -p '+outmovie 
          wait,2
          outmovie=outmovie+'yyyymmdd_hhmmss_ssuff.png'
    ;scc_mkmovie,c3names,use_model=1,0.96,1.09,/dorotate,/ratio,/fixgaps,/limb,/times,/mask_occ,/outer,box=box,save=outmovie
            scc_MKMOVIE, c3names, bmin, bmax, RATIO=doratio, PAN=pan, FIXGAPS=1, LG_MASK_OCC=use_mask, $
            COORDS=coords, BOX=box, USE_MODEL=use_model, SAVE=outmovie, REF_BOX=box_ref, $
	    AUTOMAX = automaxmin, DBIAS=dbias, log_scl=dlogscl, NOCAL=calimg_off, /DOROTATE, $
	    OUTER=use_mask, LIMB=use_mask, /BAD_SKIP, times=3,_EXTRA=_extra, DEBUG=debug
       endif
   endif
ENDIF

IF not (keyword_set(C2) or keyword_set(C3)) THEN BEGIN
;create a subfield of 537X537 approx 512X512 output
   c2mvi=file_search(getenv('SECCHI_PNG')+'/soho/c2/'+yyyymmdd+'/1024/*.hdr')
   c3mvi=file_search(getenv('SECCHI_PNG')+'/soho/c3/'+yyyymmdd+'/1024/*.hdr')
   if c2mvi(0) ne '' and c3mvi(0) ne '' then begin
     times=2.0
     pixscale=58.8  ;cor2 scale
     app_512=512/(56.0/pixscale)   ;c3 plate scale / secchi cor2 plate scale
     tmp=sccreadfits(c3names(0),hdr,/nodata)
     cropxy=[      hdr.crpix1-(app_512/2)   ,   (hdr.crpix2-(app_512/2))+(16*times)   ,    hdr.crpix1+(app_512/2)   ,   (hdr.crpix2+(app_512/2)) ]
     mvis=[c2mvi(0),c3mvi(0)]
     GET_SCC_COMBINE_MVI, mvis
     outmovie=getenv('SECCHI_PNG')+'/soho/lasco/'+yyyymmdd
     spawn,'mkdir '+outmovie 
     wait,2
     outmovie=getenv('SECCHI_PNG')+'/soho/lasco/'+yyyymmdd+'/512/
     spawn,'mkdir '+outmovie 
     wait,2
     outmovie=outmovie+'yyyymmdd_hhmmss_Lc2c3.png'
     scc_combine_mvi ,cropxy=cropxy, pixscale=pixscale, TRUECOLOR=1, outmovie=outmovie,/save_only,forcesize=[512,512],times=times
   endif
ENDIF

end
