; $Id: lasco_lister.pro,v 1.4 2015/01/27 20:06:51 hutting Exp $
;
; NAME: LASCO_LISTER
;
; PURPOSE: Simple widget for querying LASCO images
;
; CALL:   lasco_lister, list
;
; INPUTS:   None
;
; OUTPUTS:  list    STRARR  List of FITS files with path
;
; $Log: lasco_lister.pro,v $
; Revision 1.4  2015/01/27 20:06:51  hutting
; corrected filter and polarize default values
;
; Revision 1.3  2010/12/09 22:48:08  nathan
; improvements
;
; Revision 1.2  2010/05/13 18:18:51  nathan
; Added common listerinitvals and use cw_bselector2.pro to save menu selections
; between calls
;
; Ed Esfandiari, Jan 27, 1999 - Added fix for Y2K problem.
;
pro read_lasco_imghdr,file,text,filter,polar,xsize,ysize,err
fn=''
date=''
time=''
tel=''
exptime=1.
naxis1=1 & naxis2=1 & p1col=1 & p1row=1
filt=''
pol=''
lp_num=''

	text=''
openr,lur,file,/get_lun,err=err

if err eq 0 then begin
;
	a=''
	while not eof(lur) do begin
		readf,lur,a
	reads,a,fn,date,time,tel,exptime,naxis1,naxis2,p1col,p1row,filt,pol,lp_num,  $
            format='(a12,2x,a12,2x,a8,2x,a3,f8.1,4i7,2x,a6,2x,a6,2x,a8,f11.4,i6)'

;print,date,time,naxis1,naxis2,p1col,p1row,',',filt,',',pol,',',lp_num


		if filter ne '' then begin
			filt=strtrim(filt,2)
			if filter eq filt then tstfilter=1 else tstfilter=0
		endif else tstfilter=1

		if polar ne '' then begin
			pol=strtrim(pol,2)
			if polar eq pol then tstpolar=1 else tstpolar=0
		endif else tstpolar=1

		if xsize ne '' then begin
			if xsize eq naxis1 then tstxsize=1  else tstxsize=0
		endif else tstxsize=1

		if ysize ne '' then begin
			if ysize eq naxis2 then tstysize=1  else tstysize=0
		endif else tstysize=1

		if tstpolar and  tstfilter and tstxsize and tstysize then begin
			text=[text,a]
		endif
			; 104 to get just the necessary number of characters for EIT

	endwhile

;     next, get rid of blank lines
   tstsiz=size(text)
;print,' tstsiz = ',tstsiz
   if tstsiz(0) eq 1 and tstsiz(1) eq 1 then begin
	tst3=0  
   endif else if tstsiz(0) eq 0 then begin
	tst3=0
   endif else tst3 =1
;	if text(0) ne '' then tst3=1
;   endif
   if tst3 then text=text(1:*)

endif else print,!err_string,file
;
if err eq 0 then free_lun,lur
end
;
; ---------------------------------------------------------
;
;
pro lasco_dbq,lascodbqs,startdate,endate,cg,filter,polar,xsize,ysize
;
common ldirectory, qkldir,lzdir,lzyn
;
if strtrim(endate,2) eq '' then begin
	dates=strarr(1)
	utc1=anytim2utc(startdate)
        mjd2date,utc1.mjd,year,month,day
;	dates(0)=string((year-1900)*10000L+month*100L+day,'(i6)')
        if(year lt 2000) then begin
          dates(0)=string((year-1900)*10000L+month*100L+day,'(i6)')
        endif else begin
          dates(0)=string((year-2000)*10000L+month*100L+day,'(i6.6)')
        end
	ndays=1
endif else begin
	utc1=anytim2utc(startdate,/external)
	sec1=utc2sec(utc1)
	utc2=anytim2utc(endate)
	sec2=utc2sec(utc2)
	ndays=fix((sec2-sec1)/(24.*3600.)+1.01)
	print,' ndays = ',ndays

	if ndays lt 0 then begin
	    popup_msg,' Incorrect dates'
  	   return
	endif
;
	dates=strarr(ndays)
	utc1=anytim2utc(startdate)
;        mjd2date,utc1.mjd,year,month,day
;	dates(0)=string((year-1900)*10000L+month*100L+day,'(i6)')
	for iday=0,ndays-1 do begin
       	        mjd2date,utc1.mjd+iday,year,month,day
                if(year lt 2000) then begin
                  date=(year-1900)*10000L+month*100L+day
                endif else begin
                  date=(year-2000)*10000L+month*100L+day
                end
		dates(iday)=string(date,'(i6.6)')
	endfor

endelse
;
if lzyn eq 1 then dir = lzdir else dir = qkldir
;
file=dir+'/'+dates(0)+'/'+cg+'/img_hdr.txt'
;
lascodbqs=''
for iday=0,ndays-1 do begin
   file=dir+'/'+dates(iday)+'/'+cg+'/img_hdr.txt'
   read_lasco_imghdr,file,text,filter,polar,xsize,ysize,err
;  next is to make sure there are no blank lines
   tst1 = err ne 1
   tstsiz=size(text)
   tst2 = tstsiz(0) ne 0
   if tstsiz(0) eq 1 and tstsiz(1) eq 1 then begin
	tst3= text(0) ne ''
   endif else tst3 =1
   if tst1 and tst2 and tst3 then lascodbqs=[lascodbqs,text]
endfor
esiz=size(lascodbqs)
if esiz(1) ge 1 and esiz(0) ge 1 then lascodbqs=lascodbqs(1:*) ;  else lascodbqs='No files selected'


end
;
;-------------------------------------------------------------
;
pro lasco_lister_event,event
;
common lwidgetnames,exit,startdate,endate,sec,fil,scl,srch,lz,msglbl
common lstrings, cgstr,filterstr,sclstr
common lvalues, date1,date2,cgsel,filtersel,polarsel,sclnum,lascodbqs,xsizesel,ysizesel
common ldirectory, qkldir,lzdir,lzyn
common llister, selected, outfilelist
common listerinitvals, telval, filval, polval, levval, xval, yval

cgstr=['c1','c2','c3']
filterstr=strtrim(filterstr,2)
polarstr=['','Clear','0 Deg','+60 De','-60 De']
xsizestr=['',string(1024-indgen(31)*32,'(i4)')]
ysizestr=['',string(1024-indgen(31)*32,'(i4)')]


WIDGET_CONTROL, event.id, GET_UVALUE = eventval,/hourglass ;find the user value
							;of the widget where
							;the event occured
WIDGET_CONTROL, event.id, GET_UVALUE=uval

CASE eventval OF

	 "EXIT": WIDGET_CONTROL, event.top, /DESTROY

	"xldct":  xloadct,/use_current
            

	"startdate":  begin
		WIDGET_CONTROL,startdate, GET_VALUE=a0
		print,' start date = ',a0
		date1=a0
	end

	"endate":  begin
		WIDGET_CONTROL,endate, GET_VALUE=a0
		print,' end date = ',a0
		date2=a0
	end

	"cg":  begin
		print,' event.index (cg) = ',event.index
                cgsel=cgstr(event.index)
		print,' cgsel = ',cgsel
		icg=event.index
		filterstr=''
		for i=0,4 do filterstr=[filterstr,cnvrt_filter(icg,i)]
		filterstr=strtrim(filterstr,2)
		filterdropstr='All Filters'
		for i=0,4 do filterdropstr=[filterdropstr,cnvrt_filter(icg,i)]
		WIDGET_CONTROL,fil,set_value=filterdropstr
		filtersel=filterstr(0)
		telval=icg

	end

	"fil":  begin
		print,' event.index (fil) = ',event.index
                filtersel=filterstr(event.index)
		print,' filtersel = ',filtersel
		filval=event.index
	end

	"pol":  begin
		print,' event.index (pol) = ',event.index
                polarsel=polarstr(event.index)
		print,' polarsel = ',polarsel
		polval=event.index
	end


	"xsz":  begin
                xsizesel=xsizestr(event.index)
		xval=event.index
	end

	"ysz":  begin
                ysizesel=ysizestr(event.index)
		yval=event.index
	end


	"lz":  begin
		print,' event.index (lzyn) = ',event.index
		if event.index eq 0 then lzyn=1 else lzyn=0
		levval=event.index
	end

	"srch":  begin
		print,' begin search'
		WIDGET_CONTROL,startdate, GET_VALUE=date1
                date1=date1(0)
		WIDGET_CONTROL,endate, GET_VALUE=date2
                date2=date2(0)
                print,' date1, date2 = ',date1,date2
                print,' cgsel ,filtersel, polarsel = ',cgsel,'  ',filtersel,polarsel
		
                lasco_dbq,lascodbqs,date1,date2,cgsel,filtersel,polarsel,xsizesel,ysizesel

		if lascodbqs(0) ne '' then begin
		   WIDGET_CONTROL,msglbl,SET_VALUE='Date format = 7/oct/96 '
		nlist=n_elements(lascodbqs)
		selected=intarr(nlist)
		selected(*)=1
		xselect_eit,lascodbqs,selected,abort,/nonexclusive, $
			x_scroll_size=750   , y_scroll_size=500
		selgood=where(selected lt 0,nselgood)
		if nselgood gt 0 then outfilelist=strarr(nselgood)
		for is=0,nselgood-1 do begin
			isel=selgood(is)
			filename=strmid(lascodbqs(isel),0,12)
			if lzyn eq 1 then dir = lzdir else dir = qkldir
			completefilename=qkldir+'/'+filename
			datestr=strmid(lascodbqs(isel),18,8)
			remchar,datestr,'/'
			completefilename=dir+'/'+datestr+'/'+cgsel+'/'+filename
			print,' filename = ',completefilename
			outfilelist(is)=completefilename
		endfor
		endif else begin
		   WIDGET_CONTROL,msglbl,SET_VALUE=' No files found, try changing Level Zero/Quicklook'
		endelse
	end



	ELSE:   MESSAGE, "Event User Value Not Found"



ENDCASE


end
;
;  --------------------------------------------------------------
;
pro lasco_lister,outfiles
;
common lwidgetnames,exit,startdate,endate,sec,fil,scl,srch,lz,msglbl
common lvalues, date1,date2,cgsel,filtersel,polarsel,sclnum,lascodbqs,xsizesel,ysizesel
common ldirectory, qkldir,lzdir,lzyn
common llister, selected, outfilelist
common lstrings, cgstr,filterstr,sclstr
common listerinitvals, telval, filval, polval, levval, xval, yval
;

ql=getenv('QL_IMG')
qkldir=ql+'/level_05'
lz=getenv('LZ_IMG')
lzdir=lz+'/level_05'
;
help,telval
IF datatype(telval) EQ 'UND' THEN BEGIN
; first time called
      get_utc,utc,/external
      ;sec1=utc2sec(utc)
      date1=anytim2cal(utc,form=0,/date)
      date2=''
      ;date1=strtrim(string(utc.month,'(i2)'),2)+'/'+strtrim(string(utc.day,'(i2)'),2)+  $
      ;       '/'+strtrim(string(utc.year,'(i4)'),2)
    telval=0
    filval=0
    polval=0
    levval=0
    xval=1
    yval=1
ENDIF

print,' date1 = ',date1
cgdropstr=['C1','C2','C3']
cgsel=strlowcase(cgdropstr[telval])

filterdropstr='All Filters'
for i=0,4 do filterdropstr=[filterdropstr,cnvrt_filter(telval,i)]
filterstr=''
for i=0,4 do filterstr=[filterstr,cnvrt_filter(telval,i)]
filtersel=filterstr[filval]

; next assumes initial cg = c1
polardropstr=['All Polarizers','Clear','0 Deg','+60 Deg','-60 Deg']
polarstr=['','Clear','0 Deg','+60 Deg','-60 Deg']
polarsel=polarstr[polval]


xsizedropstr=['Any',string(1024-indgen(31)*32,'(i4)')]
xsizesel=xsizedropstr[xval]
ysizedropstr=['Any',string(1024-indgen(31)*32,'(i4)')]
ysizesel=ysizedropstr[yval]
sclnum=0    ; logarithmic scaling
lzstr=['Level Zero','Quick Look']
IF levval EQ 0 THEN lzyn=1 ELSE lzyn=0
font='-adobe-times-bold-r-normal--14-140-75-75-p-77-iso8859-1'
;
lascodbase = WIDGET_BASE(TITLE = 'LASCO LISTER',/column)
	;create the main base
;
lascodbase1 = WIDGET_BASE(lascodbase,/frame,/row) ;create secondary base 1
;
exit = WIDGET_BUTTON(lascodbase1, $  
		VALUE = 'EXIT',  $   ;The button label.
		UVALUE = 'EXIT', $   ;The button's User Value.
	FONT=font)
  
xldct=WIDGET_BUTTON(lascodbase1,/frame,value='XLOADCT',uvalue='xldct', $
	FONT=font)

lascodbase2 = WIDGET_BASE(lascodbase,/frame,/row) ;create secondary base 2

startdatelbl=WIDGET_LABEL(lascodbase2, VALUE='Start Date: ',uvalue='startdatelbl', $
	FONT=font)

startdate=WIDGET_TEXT(lascodbase2, VALUE=date1,/editable ,uvalue='startdate', /frame, $
	FONT=font)

endatelbl=WIDGET_LABEL(lascodbase2, VALUE='End Date: ',uvalue='endatelbl', $
	FONT=font)

endate=WIDGET_TEXT(lascodbase2, VALUE=date2,/editable ,uvalue='endate', /frame, $
	FONT=font)
endatelbl2=WIDGET_LABEL(lascodbase2, VALUE='(Optional)',uvalue='endatelbl2')

lascodbase3 = WIDGET_BASE(lascodbase,/frame,/row) ;create secondary base 3

cg = CW_BSELECTOR2(lascodbase3,/frame, cgdropstr, UVALUE='cg', SET_VALUE=telval, FONT=font)
;cg = WIDGET_DROPLIST(lascodbase3,/frame,value=cgdropstr,uvalue='cg', $
;	FONT=font)

fil = CW_BSELECTOR2(lascodbase3,/frame,filterdropstr, uvalue='fil', SET_VALUE=filval, FONT=font)

pol = CW_BSELECTOR2(lascodbase3,/frame,polardropstr, uvalue='pol', SET_VALUE=polval, FONT=font)

lz = CW_BSELECTOR2(lascodbase3,/frame,lzstr, uvalue='lz',  SET_VALUE=levval, FONT=font)

srch=WIDGET_BUTTON(lascodbase3,/frame,value='SEARCH',uvalue='srch', $
	FONT=font)

lascodbase4 = WIDGET_BASE(lascodbase,/frame,/row) ;create secondary base 4

xsizelbl=WIDGET_LABEL(lascodbase4, VALUE=' X Size:',uvalue='xsizelbl', $
	FONT=font)

xsz = CW_BSELECTOR2(lascodbase4,/frame,xsizedropstr,uvalue='xsz', SET_VALUE=xval, FONT=font)

ysizelbl=WIDGET_LABEL(lascodbase4, VALUE=' Y Size:',uvalue='ysizelbl', $
	FONT=font)

ysz = CW_BSELECTOR2(lascodbase4,/frame,ysizedropstr,uvalue='ysz', SET_VALUE=yval, FONT=font)

lascodbase5 = WIDGET_BASE(lascodbase,/frame,/row) ;create secondary base 4


msglbl=WIDGET_LABEL(lascodbase5, $
	VALUE=' Date format = 6/oct/96                                                 ', $
	uvalue='msglbl', $
	FONT=font)



WIDGET_CONTROL, lascodbase, /REALIZE

XManager, "wtest", lascodbase, $			;register the widgets
		EVENT_HANDLER = "lasco_lister_event", $	;with the XManager
		GROUP_LEADER = GROUP	

if datatype(outfilelist) ne 'UND' then outfiles=outfilelist

print,'the end'
end
