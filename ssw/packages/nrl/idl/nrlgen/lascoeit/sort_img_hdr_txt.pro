;+
; $Id: las_read_summary.pro,v 1.9 2013/10/21 19:52:31 nathan Exp $
;
; Project     :	LASCO
;
; Name        :	LAS_READ_SUMMARY()
;
; Purpose     :	Read in img_hdr.txt file, sort by date, and re-write it inplace
;
; Category    :	LASCO, Catalog
;
; Explanation :	
;
; Syntax      :	List = LAS_READ_SUMMARY( [ FILES ], keywords )
;
; Examples    :	List = LAS_READ_SUMMARY(DATE='1-Nov-2006')
;
; Inputs      :	None required.
;
; Opt. Inputs :	
;
; Outputs     :	
;
; Opt. Outputs:	None
;
; Keywords    :	SAVEDIR=    write img_hdr.txt in a different directory than input
;
; Env. Vars.  : $LZ_IMG or $QL_IMG
;
; Calls       :	ANYTIM2UTC, UTC2TAI, BREAK_FILE, VALID_NUM, IMG_HDR_TXT2STRUCT
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	
; 
;   	    	$Log: las_read_summary.pro,v $
; Contact     :	n.rich
;-
;
pro sort_img_hdr_txt, indxfile, SAVEDIR=savedir

rows=readlist(indxfile)
n=n_elements(rows)
s=sort(strmid(rows,17,36))

IF keyword_set(SAVEDIR) THEN BEGIN
    break_file,indxfile,de,pa,ro,sf
    outfile=concat_dir(savedir,ro+sf)
ENDIF ELSE outfile=indxfile

openw,1,outfile
FOR i=0,n-1 DO printf,1,rows[s[i]]
close,1
                           
;
end
