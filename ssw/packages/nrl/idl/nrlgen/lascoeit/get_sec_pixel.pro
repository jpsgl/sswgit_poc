FUNCTION GET_SEC_PIXEL, hdr, FULL=FULL, LOUD=loud
;
;+
; $Id: get_sec_pixel.pro,v 1.6 2014/10/27 20:41:02 nathan Exp $
; NAME:
;	GET_SEC_PIXEL
;
; PURPOSE:
;	This function returns plate scale in arc seconds per pixel.
;
; CATEGORY:
;	LASCO_ANALYSIS
;
; CALLING SEQUENCE:
;	Result = GET_SEC_PIXEL (Hdr)
;
; INPUTS:
;	Hdr:	A LASCO header structure -or- one of 
;   	    	'C1','C2','C3','EIT','MK3','MK4'
;
; OPTIONAL INPUTS:
;       FULL=FULL:      If the image has been placed in a square field. Ex:
;                   Result = GET_SEC_PIXEL (Hdr, FULL=1024)
;                   Result = SEC_PIXEL (Hdr, FULL=512)
;
; OUTPUTS:
;	arc seconds per pixel
;
;
; PROCEDURE:
;	Returns values that have been determined by other means and put
;	into a table here.
;
; MODIFICATION HISTORY:
; 	Written by:	S.E. Paswaters, 30 August 1996
; $Log: get_sec_pixel.pro,v $
; Revision 1.6  2014/10/27 20:41:02  nathan
; add warning message if subfield detected
;
; Revision 1.5  2014/10/27 20:19:47  nathan
; fix detector input bug; use rcol not sumcol to compute sec_pix (will only work with ffv in x dimension)
;
; Revision 1.4  2012/02/15 17:57:39  nathan
; add /loud
;
; Revision 1.3  2012/02/15 17:52:35  nathan
; return type float not double
;
; Revision 1.2  2011/01/21 18:18:14  nathan
; allow input to be detector string
;
; Revision 1.1  2008/04/14 17:55:06  nathan
; moved from lasco/idl/data_anal for scc_wrunmovie.pro
;
;       Updated:
;                       96/10/04  SEP  Changed FULL to allow different sizes.
;			98/08/28  RAH  Mods for MLO/MK3
;	99/08/05  NBR	Fixed FULL keyword to compute floating point factor
;	00/01/14  DAB	Mods for MLO/MK4
;	01/02/12  NBR	Mods for MK4 and elimination of sec_pix as array
;
;	08/19/05 @(#)get_sec_pixel.pro	1.12 : LASCO IDL LIBRARY
;-

    ishdr=0
    IF (DATATYPE(hdr) NE 'STC') THEN BEGIN
    	IF n_elements(hdr) EQ 1 THEN detector=trim(strupcase(hdr)) ELSE BEGIN

	    detector = trim(FXPAR(hdr,'DETECTOR'))
      	    IF (detector EQ 'MK3') or (detector EQ 'MK4') THEN shdr=MLO_FITSHDR2STRUCT(hdr) ELSE shdr=LASCO_FITSHDR2STRUCT(hdr)
    	    ishdr=1
    	ENDELSE
    ENDIF ELSE BEGIN
    	shdr=hdr
	ishdr=1
    	detector=shdr.detector
    ENDELSE
   cam = ['C1','C2','C3','EIT','MK3','MK4']
   tel = WHERE(detector EQ cam)
   tel = tel(0)

;   sec_pix = [5.6D,11.4D,56.8D,2.5D]
;   sec_pix = DBLARR(6)
   
   ;FOR i=0,5 DO sec_pix(i) = DOUBLE(SUBTENSE(cam(i)))
   IF tel LT 0 and ishdr THEN $
	sec_pix = shdr.cdelt1 ELSE $
   IF tel LT 4 THEN $
        sec_pix = (SUBTENSE(detector)) $
   ELSE BEGIN	; MK3 or MK4
	yymmdd=strmid(shdr.date_obs,2,2)+strmid(shdr.date_obs,5,2)+strmid(shdr.date_obs,8,2)
	solar_ephem,yymmdd,RADIUS=radius
	sec_pix = shdr.cdelt1*radius*3600.
   ENDELSE
   IF keyword_set(loud) THEN help,sec_pix

   IF KEYWORD_SET(FULL) THEN BEGIN
      IF tel LT 4 THEN factor = 1024./float(full) ELSE factor = 960./Float(full)
      RETURN, sec_pix*factor
   ENDIF

    IF ishdr THEN BEGIN
    ;** apply correction for on chip summing
    	binfac=float(shdr.r2col-shdr.r1col+1)/shdr.naxis1
	IF (binfac NE (shdr.sumcol+1)*shdr.lebxsum) THEN message,'Warning: Result not correct for Subfields!',/info
	wait,1
    	sec_pix = sec_pix * binfac
 
    ENDIF

   RETURN, sec_pix

END
