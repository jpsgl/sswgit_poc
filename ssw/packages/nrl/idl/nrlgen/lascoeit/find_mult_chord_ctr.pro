function find_mult_chord_ctr, files, INTEN=inten, _EXTRA=_extra
;+
; $Id: find_mult_chord_ctr.pro,v 1.1 2010/02/08 22:26:22 nathan Exp $
;
; INPUTS:
;
;   	files	List of LASCO FITS files
;
; OPTIONAL INPUTS:
;	
; KEYWORD PARAMETERS:
;   	See find_chord_ctr.pro and reduce_std_size.pro
;
; OUTPUTS:
;   	Nx2 FLTARR of x,y center of occulter found using find_chord_ctr.pro
;
; $Log: find_mult_chord_ctr.pro,v $
; Revision 1.1  2010/02/08 22:26:22  nathan
; run find_chord_ctr.pro on multiple lasco images
;
;-
;

nf=n_elements(files)

c=fltarr(nf,2)

IF ~keyword_set(INTEN) THEN inten=1000

for i=0,nf-1 DO BEGIN
    im=lasco_readfits(files[i],h)
    im1=reduce_std_size(im,h,_EXTRA=_extra)
    IF i EQ 0 THEN BEGIN
    	sz=size(im1)
	window,xsiz=0.7*sz[1],ysiz=0.7*sz[2]
	tvscl,im1<inten
	wait,2
    ENDIF
    FIND_CHORD_CTR,Im1,Xcen,Ycen,Xsig,Ysig, INTEN=inten, _EXTRA=_extra
    c[i,0]=xcen
    c[i,1]=ycen
endfor

return,c

end
