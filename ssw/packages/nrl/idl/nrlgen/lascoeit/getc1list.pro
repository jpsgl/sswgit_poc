function getc1list, wlupl, rng, STC=stc, DOORCLS=doorcls
; $Id: getc1list.pro,v 1.2 2010/12/09 22:46:49 nathan Exp $
;
; NAME:		getc1doorclosed
;
; PURPOSE:	returns filenames with path of c1 images at given wavelength or 
;   	    	wavelength range for 1996-1998
;
; INPUTS:	wlupl  FLOAT   Desired wavelength/value of FP_WL_CMD (Angstroms)
;
; OPTIONAL INPUT:   rng     Search + this much (default is 0.1 A)
;		
; KEYWORDS:	STC=	Returns structure from db query
;   	    	/DOORCLS    Return door-closed images
;
; OUTPUTS:	STRARR
;
; ROUTINES CALLED:
;   	qdb.pro
;
; AUTHOR:	Nathan Rich, Dec 2009
;
; $Log: getc1list.pro,v $
; Revision 1.2  2010/12/09 22:46:49  nathan
; get r2row col also
;
; Revision 1.1  2009/12/31 17:27:59  nathan
; implementing c1 green line maps with fpc1_2img.pro
;


IF n_params() LT 2 THEN rng=0.1
IF keyword_set(DOORCLS) THEN cls='1' ELSE cls='0'

query =  'select img_files.filename, date_obs, fp_wl_upl, r1row, r1col, r2row, r2col, exptime, lebxsum, naxis1, naxis2, polar, filter, lp_num, diskpath from img_leb_hdr, img_files where img_leb_hdr.fileorig=img_files.fileorig and camera=0 and filetype=1 and source=2 and date_obs between "1996/01/01" and "1998/07/01" and fp_wl_upl between '+trim(wlupl)+' and '+trim(wlupl+rng)+' and naxis1 > 0 and door_pos='+cls
stc=qdb('lasco',query)
nf=n_elements(stc)
IF datatype(stc) EQ 'STC' THEN BEGIN
    files=strarr(nf)
    IF cls EQ '1' THEN BEGIN
	FOR i=0,nf-1 DO BEGIN
    	    yymmdd=utc2yymmdd(anytim2utc(stc[i].date_obs))
	    files[i]='$LZ_IMG/misc/door_cls/c1/'+yymmdd+'/'+stc[i].filename
	ENDFOR
    ENDIF ELSE files=stc.diskpath+'/'+stc.filename
ENDIF ELSE files=''
return,files

end	
	
	
	
	
	
	
	
	
	
	
