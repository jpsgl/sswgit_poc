pro mysql_update_rows,d1, d2, QL=ql, LZ=lz, TELESCOPE=telescope
;$Id: mysql_update_rows.pro,v 1.1 2015/07/14 17:56:03 nathan Exp $
;
; Project     :  SOHO - LASCO/(EIT  C2 C3)(dual-use)
;                   
; Name        : mysql_update_rows
;               
; Purpose     : write MySQL database commands to fix [diskpath] field
;               
; Input     ; 	d1  	anytim  date range
;   	    	d2  	anytim  end of range (NOT inclusive)
;
; Output    :	mysql script file
;
; Keywords: 	/QL 	Change quicklook
;   	    	/LZ 	change LZ
;   TELESCOPE=	STRING c1,c2,c3, or c4 , default is all
;
; Use         : IDL> 
;    
; Calls       : 
;
; Comments    : 
;               
; Side effects: 
;
; Category    : 
;               
; Written     : N.Rich NRL July 2015
;               
; $Log: mysql_update_rows.pro,v $
; Revision 1.1  2015/07/14 17:56:03  nathan
; update single rows
;

COMMON dbms, ludb,lulog, delflag

IF keyword_set(QL) THEN src='ql' ELSE src='lz'
IF keyword_SET(TELESCOPE) THEN tel=telescope ELSE tel='c*'
table='img_files'
field='diskpath'

ut1=anytim2utc(d1)
openw,1,'las_'+src+'_'+table+'_'+utc2yymmdd(ut1)+'_update.db'
printf,1,'use lasco;'
ut2=anytim2utc(d2)
lzpath='/net/corona/lasco/'+src+'/level_05/'
utn=ut1
for i=ut1.mjd,ut2.mjd do begin
    utn.mjd=i
    ymd=utc2yymmdd(utn)
    li=findfile(lzpath+ymd+'/'+tel+'/*.fts')
    nli=n_elements(li)
    help,ymd,li
    for j=0,nli-1 do begin
    	break_file,li[j],di,pa,fi,su
	len=strlen(pa)
	dpa=strmid(pa,0,len-1)
	
	printf,1,'update '+table+' set '+field+'="'+dpa+'" where filename="'+fi+su+'";'
    endfor
endfor

printf,1,'commit;'
close,1

end
    
