pro reduce_daily_polariz,stdate,endate,C2=c2, C3=c3, _EXTRA=_extra
;+
; $Id: reduce_daily_polariz.pro,v 1.1 2016/12/28 17:07:43 nathan Exp $
; NAME:
;	REDUCE_DAILY_POLARIZ
;
; PURPOSE:
;	Wrapper for do_polariz.pro
;
; CATEGORY:
;	LASCO REDUCE
;
; CALLING SEQUENCE:
;
; INPUTS:
;	STDate:	A string in the format YYMMDD that defines the starting date to be processed.
;	ENDATE:	A string in the format YYMMDD that defines the ending date to be processed.
;		If not present, then only one date is processed
;	
; KEYWORD PARAMETERS - one of these is required:
;   	/C2 	Do C2
;   	/C3 	Do C3
;
; PROCEDURE:
;	This procedure calls the routines to do the following tasks:
;
;		generate PB and %P images
;
; EXAMPLE:
;
; MODIFICATION HISTORY:
;-
;


dir = GETENV('LZ_IMG')

np = N_PARAMS()

    	dates = [stdate]
	IF (np EQ 2)  THEN BEGIN
		dtea = YYMMDD2UTC(stdate)
		dteb = YYMMDD2UTC(endate)
		dte = dtea
		FOR i=dtea.mjd+1,dteb.mjd DO BEGIN
			dte.mjd = i
			s=UTC2YYMMDD(dte)
			dates = [dates,s]
		ENDFOR
	ENDIF

nd = N_ELEMENTS(dates)

	poldir = getenv('POLDIR')

FOR id=0,nd-1 DO BEGIN
	PRINT,'REDUCE_DAILY: Processing '+dates(id)

    	dte = dates[id]
	utc=yymmdd2utc(dte)
	print,'Computing polarization images for ',dte
	ecsdte = utc2str(utc,/ecs,/date_only)
	dirdate = strmid(ecsdte,0,4)+'_'+strmid(ecsdte,5,2)	;** Ex. '1999_03'
	savedir2 = poldir+'/'+dirdate+'/vig/c2'
	savedir3 = poldir+'/'+dirdate+'/vig/c3'
	
	IF keyword_set(C2) THEN BEGIN
	    dummy = file_exist(savedir2)
	    IF ~dummy THEN spawn, 'mkdir -p '+savedir2, /SH

	    hdrtxt = dir+'/level_05/'+dte+'/c2/img_hdr.txt'
	    openw,tlun,'~/pol_list.txt',/get_lun
	    printf,tlun,hdrtxt
	    close,tlun
	    free_lun,tlun
	    cam = 2

	    DO_POLARIZ, INDEXLIST='~/pol_list.txt', SAVEPATH=savedir2, CAMERA=cam, $
		    /VIG,/PTF, /SAVE_POLARIZ, /SAVE_PERCENT, /AUTO, _EXTRA=_extra
	ENDIF ; /C2	
	
	IF keyword_set(C3) THEN BEGIN
	    dummy = file_exist(savedir3)
	    IF ~dummy THEN spawn, 'mkdir -p '+savedir3, /SH
	    
	    hdrtxt = dir+'/level_05/'+dte+'/c3/img_hdr.txt'
	    openw,tlun,'~/pol_list.txt',/get_lun
	    printf,tlun,hdrtxt
	    close,tlun
	    free_lun,tlun
	    cam = 3

	    DO_POLARIZ, INDEXLIST='~/pol_list.txt', SAVEPATH=savedir3, CAMERA=cam, $
		    /VIG,/PTF,/SAVE_POLARIZ, /SAVE_PERCENT, /AUTO, /FIXC3ZERO, _EXTRA=_extra
	ENDIF ; /C2	
ENDFOR

RETURN
END
