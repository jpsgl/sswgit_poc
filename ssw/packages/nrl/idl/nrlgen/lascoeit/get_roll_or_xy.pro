;+
; NAME:
;       GET_ROLL_OR_XY
;
; Version     : 
; $Id: get_roll_or_xy.pro,v 1.31 2016/05/11 22:22:51 nathan Exp $
;
; PURPOSE:
;       This function returns the roll angle  of solar north in radians 
;       or the sun center position as a structure:
;       {sun_center,xcen:xcen,ycen:ycen} (pixels).
;
;       It uses C2 and C3 center and roll files created by star routines
;       after adjusting header times using EIT/LASCO time-offsets. It should
;       only be used with ql or lz level_05 data. For other telescopes, the
;       roll, and center are set to but if a roll is requested and it is zero, 
;       the SOHO roll is returned instead - as was the case in old version of
;       get_roll_or_xy.
;
; CATEGORY:
;       LASCO_ANALYSIS
;
; CALLING SEQUENCE:
;       Result = GET_ROLL_OR_XY (Hdr, Roll_xy)
;
; INPUTS:
;       Hdr:      A LASCO header structure or array of structures
;       Roll_xy: 'ROLL' :  return the CW roll angle
;                'CENTER': return the sun center
;
; OUTPUTS:
;       Roll angle in radians OR sun center {xcen,ycen} in pixels IDL coords.
;   	Facing sun, roll is of observer, from solar north, radians, positive CCW
;   	If input is binned, output is corrected for binning.
;
; OPTIONAL OUTPUTS:
;   	ROLL_OUT=named_var	Named_var contains value of roll angle
;   	CENTER_OUT=named_var	Named_var contains sun center structure
;
; OPTIONAL KEYWORD INPUTS:
;       MEDIAN:  Not used - kept for compatability with previous version.
;       STAR:    Not used - kept for compatability with previous version.
;       AVG:     Not used - kept for compatability with previous version.
;       SILENT:  Stop printing of warning and informational messages
;       DEGREES: Return roll angle in degrees not radians
;
; OPTIONAL OUTPUT:
;       source: 'SunDec files using EIT/LASCO adjusted time-offsets and csol1a.pro.'
;
; $Log: get_roll_or_xy.pro,v $
; Revision 1.31  2016/05/11 22:22:51  nathan
; fix interpolate test
;
; Revision 1.30  2015/04/21 16:06:17  nathan
; one less help info line
;
; Revision 1.29  2014/12/23 18:24:25  nathan
; correct previous fix
;
; Revision 1.28  2014/12/23 17:56:07  nathan
; account for cal roll Nov 2001
;
; Revision 1.27  2014/12/22 15:28:03  nathan
; allow 60 sec difference from .sav file for match
;
; Revision 1.26  2014/12/18 21:18:30  nathan
; use conditional interpolatedroll
;
; Revision 1.25  2014/12/15 19:53:50  nathan
; fix previous mod
;
; Revision 1.24  2014/12/15 19:51:17  nathan
; do interpolate for roll for binned or subfields
;
; Revision 1.23  2014/11/20 20:11:28  nathan
; typo
;
; Revision 1.22  2014/11/13 15:45:09  nathan
; fix source bug
;
; Revision 1.21  2014/11/12 22:42:34  nathan
; do not interpolate roll
;
; Revision 1.20  2014/04/14 18:05:30  nathan
; if later than last time in correction file, use estimate for roll and return 0,0 for center
;
; Revision 1.19  2013/04/01 21:14:00  nathan
; rearrange positions of binning correction and checking for rectification (for case where crota* not in header)
;
; Revision 1.18  2013/03/29 17:49:37  nathan
; output center corrected for binning
;
; Revision 1.17  2013/03/26 18:36:09  nathan
; fix rectify detection; subtract 32 sec from pointing TAI
;
; Revision 1.16  2012/02/15 17:36:13  nathan
; fix previous log entry
;
; Revision 1.15  2012/02/15 17:32:44  nathan
; make sure center is IDL coords (crpix-1)
;
; Revision 1.14  2011/04/27 16:58:19  nathan
; fix pntroll check
;
; Revision 1.13  2011/04/25 21:38:43  nathan
; check for so_at_roll 180 deg error
;
; Revision 1.12  2011/02/08 19:48:16  nathan
; add interpolated to source
;
; Revision 1.11  2010/12/27 20:23:21  nathan
; call read_so_at_roll_dat if att_pre FITS not found
;
; Revision 1.10  2010/11/09 15:29:00  nathan
; update source output
;
; Revision 1.9  2010/11/01 21:07:18  nathan
; accomodate new sun-center-roll .sav files; add _EXTRA
;
; Revision 1.8  2010/09/15 21:53:46  nathan
; use estimated roll offset for C2 and C3
;
; Revision 1.7  2010/05/14 18:22:53  mcnutt
; added common block for last_day and recovery_adjusted_xyr save files
;
; Revision 1.6  2010/03/03 19:03:14  nathan
; do not reform output so scalar remains scalar
;
; Revision 1.5  2010/02/08 22:27:12  nathan
; finish previous mod
;
; Revision 1.4  2010/02/01 22:41:48  nathan
; allow vector input
;
; Revision 1.3  2009/06/01 21:41:44  nathan
; return crpix (not zero) if EIT or C1
;
; Revision 1.2  2009/02/12 17:53:48  esfand
; do not interpolate during long gaps or rolls - use prev values - aee
;
; Revision 1.1  2008/04/29 20:02:35  nathan
; moved from lasco/idl/data_anal for scc_wrunmoviem.pro
;
; History:
; 05/02/06 @(#)get_roll_or_xy.pro	1.4 :NRL Solar Physics
; 2005 January 05 - Ed Esfandiari  First version - based on adjust_hdr_tcr.
; 2005 January 05 - Ed Esfandiari  Changed call to linterp to linear_interp.
; 2005 April 12   - Karl Battams - Fixed problem with incompatible date formats
; 2006 April 25   - Nathan Rich - Add ROLL_OUT and CENTER_OUT keywords
; 2009 Feb   12   - Ed Esfandiari - Don't interpolate during long gaps or
;                                   rolls (if difference between two adjacent 
;                                   rolls is more than 5 degree).
;-
;

FUNCTION GET_ROLL_OR_XY, hdr, roll_xy, source, STAR=STAR, MEDIAN=MEDIAN, AVG=AVG, $
        SILENT=SILENT, DEGREES=degrees, ROLL_OUT=roll_out, CENTER_OUT=center_out, _EXTRA=_extra

common roll_data, odatafile, last_day ,C_DT,C_R,C_RMED,C_TAI,C_UTC,C_X,C_XMED,C_Y,C_YMED

source=''
IF keyword_set(SILENT) THEN verbose=0 ELSE verbose=1

   if datatype(odatafile) eq 'UND' then odatafile=''

  adjusted= {xpos:0.0, ypos:0.0, roll:0.0}
  shdrs= hdr
 
  IF (DATATYPE(shdrs) NE 'STC') THEN BEGIN
        tel = STRUPCASE(STRTRIM(FXPAR(hdr,'TELESCOP'),2))
        IF (tel EQ 'MK3') THEN shdrs=MLO_FITSHDR2STRUCT(hdr) $
            ELSE shdrs=LASCO_FITSHDR2STRUCT(hdr)
  ENDIF

hsz=size(shdrs)
nh=hsz[1]
center= {sun_center,xcen:0.0D,ycen:0.0D}
sunroll=0.0
interpolatedroll=0.

IF nh GT 1 THEN BEGIN
    center=replicate(center,nh)
    sunroll=replicate(sunroll,nh)
ENDIF

if datatype(last_day) eq 'UND' then begin
     ldayfile= 'c2_c3_last_post_recovery_day.sav' 
     ;restore,'~/idl/sandbox/ed/'+ldayfile
     restore,filepath(ldayfile,ROOT=getenv('NRL_LIB'),SUB=['idl','data','calib'])
     ; => last_day (i.e. '2004/10/22'), c2_last_day, c3_last_day
     ;	    last_day is always oldest of two other dates
endif

FOR i=0,nh-1 DO BEGIN
  
  shdr=shdrs[i]
  tel= strupcase(STRTRIM(shdr.detector,2))
  date= shdr.date_obs
  time= STRTRIM(shdr.time_obs,2)

  utclast_day=anytim2utc(last_day)  ; added to fix date formatting issues.  KB 050412
  utcdate= anytim2utc(date)
  IF (utcdate.MJD GT utclast_day.MJD ) THEN BEGIN
        msg='Level-1 CNTR/ROLL correction only available thru '+last_day+'.'
	PRINT,''
	PRINT,msg
	PRINT,''
	;RETURN,adjusted    
        GOTO, next 
  ENDIF

  IF (NOT KEYWORD_SET(silent)) THEN $ 
	adj_dt= ADJUST_ALL_DATE_OBS(shdr,/verbose) $
  ELSE $
	adj_dt= ADJUST_ALL_DATE_OBS(shdr)

  ; Add 32.184 sec to match c2[3]_tai from sunDec file:

  tai= utc2tai(anytim2utc(adj_dt.date+' '+adj_dt.time)) + 32.184 

  CASE tel OF 
	'C2': BEGIN  
		IF (adj_dt.date LT '1998/07/01') THEN $
		datafile ='c2_pre_recovery_adj_xyr_medv2.sav' ELSE $
		datafile ='c2_post_recovery_adj_xyr_medv2.sav'
		if odatafile ne datafile then begin
                   odatafile=datafile
		   pathdatafile=filepath(datafile,ROOT=getenv('NRL_LIB'),SUB=['idl','data','calib'])
		   ;pathdatafile='~/idl/sandbox/ed/'+datafile
		   print,'Restoring ',pathdatafile
		   restore,pathdatafile
        ;=> c_tai (from sunDec file is 32.184sec ahead of adjusted time in header) is already sorted
        ;=> c_utc (tai changed to utc format)
        ;=> c_dt (tai changed to date/time string)
        ;=> c_x,c_y,c_r,c_xmed,c_ymed,c_rmed 
                endif
	END
	'C3': BEGIN
		IF (adj_dt.date LT '1998/07/01') THEN $
		datafile ='c3_pre_recovery_adj_xyr_medv2.sav' ELSE $
		datafile ='c3_post_recovery_adj_xyr_medv2.sav'
		if odatafile ne datafile then begin
                   odatafile=datafile
		   pathdatafile=filepath(datafile,ROOT=getenv('NRL_LIB'),SUB=['idl','data','calib'])
		   ;pathdatafile='~/idl/sandbox/ed/'+datafile
		   print,'Restoring ',pathdatafile
		   restore,pathdatafile
       ;=> c_tai (from sunDec file is 32.184sec ahead of adjusted time in header) is already sorted
        ;=> c_utc (tai changed to utc format)
        ;=> c_dt (tai changed to date/time string)
        ;=> c_x,c_y,c_r,c_xmed,c_ymed,c_rmed 
                endif

	END
        'MLO': BEGIN
           IF (NOT KEYWORD_SET(silent)) THEN PRINT,'MLO: Returning 0.0 for CENTER and ROLL.' 
           GOTO, next
        END
	ELSE: BEGIN  ; C1 and C4
		PRINT,''
		PRINT,'WARNING: Header is for a '+tel+ $
                      ' image. Only C2 and C3 images can be corrected for sun-center and roll.'
		PRINT,'        (using roll=0.0, xpos=crpix1, ypos=crpix2).'
                ;PRINT,'        (using roll=0.0, xpos= 0.0, ypos= 0.0)'
		PRINT,''
		IF tag_exist(shdr,'crpix1') THEN adjusted.xpos= shdr.crpix1-1
		IF tag_exist(shdr,'crpix2') THEN adjusted.ypos= shdr.crpix2-1
                ;adjusted.xpos= 0.0
                ;adjusted.ypos= 0.0
		adjusted.roll= 0.0
		datafile="Returning header CRPIX"
		;RETURN, adjusted
                GOTO, next
	END
  ENDCASE
  source=datafile
  help,datafile
  IF keyword_set(VERBOSE) THEN help,odatafile
  
  IF (KEYWORD_SET(verbose)) THEN BEGIN
      PRINT,'Input hdr date_obs=    ',anytim2utc(date+' '+time,/ecs)
      PRINT,'Adjusted hdr date_obs= ',anytim2utc(adj_dt.date+' '+adj_dt.time,/ecs)
      HELP,/ST,adjusted
  ENDIF
  
 ind= WHERE(c_tai LE tai, cnt)

    IF cnt LT 1 THEN BEGIN  ;hdr time is LT first time of the sunDec file. use the first row. 
	bind=0
	aind=1
	message,'WARNING: Header date_obs LT first date_obs in sunDec file; Using first 2 values.' ,/info
    ENDIF ELSE BEGIN
	bind= cnt-1
	IF abs(c_tai[bind] - tai) LT 60 THEN aind= bind $  ; found a match 
	ELSE aind= bind+1 
	IF ((tai GT anytim2tai('2001/11/14 10:00')) and (tai LT anytim2tai('2001/11/15 15:00'))) then aind=bind+1
	; calibration roll
      	IF (aind EQ N_ELEMENTS(c_tai)) THEN BEGIN  ;hdr time is GT last time of the sunDec file. use last row.
        	aind= bind
		bind=aind-1
        	message,'WARNING: Header date_obs GT last date_obs row in center and roll file; Using estimates.',/info
		adjusted= {xpos:0.0, ypos:0.0, roll:0.0}
		goto,next

      	END
    ENDELSE
  ;help,c_tai,tai,bind,aind

  
  IF (bind NE aind) THEN BEGIN ; interpolate
    	LINEAR_INTERP,c_tai(bind),c_xmed(bind),c_tai(aind),c_xmed(aind),tai,val
	adjusted.xpos= val
	LINEAR_INTERP,c_tai(bind),c_ymed(bind),c_tai(aind),c_ymed(aind),tai,val
	adjusted.ypos= val
	IF (shdr.naxis1 NE 1024) or (shdr.naxis2 NE 1024) THEN BEGIN    ; problem with subfields 1996 Aug - Oct
      	    LINEAR_INTERP,c_tai(bind),c_rmed(bind),c_tai(aind),c_rmed(aind),tai,val
	    interpolatedroll=val
	    ;adjusted.roll= val
	ENDIF
        interp_flag= 1
	source='INTERPOLATED '+source

        ; Do not interpolate during long gaps or during rolls where
        ; difference in two adjacent roll values is more than 5 degree:
        IF(ABS(c_rmed(bind)-c_rmed(aind)) GT 5) THEN BEGIN 
          adjusted.xpos= c_xmed(bind) 
          adjusted.ypos= c_ymed(bind)
          ;adjusted.roll= c_rmed(bind)
          interp_flag= 0
        ENDIF
  ENDIF ELSE BEGIN ; bind eq aind
	adjusted.xpos= c_xmed(bind)
	adjusted.ypos= c_ymed(bind)
	adjusted.roll= c_rmed(bind)
  ENDELSE

  IF (NOT KEYWORD_SET(silent)) THEN BEGIN
	PRINT,'Input hdr date_obs   = ',anytim2utc(date+' '+time,/ecs)
	PRINT,'Adjusted hdr date_obs= ',anytim2utc(adj_dt.date+' '+adj_dt.time,/ecs)
	IF(bind EQ aind) THEN BEGIN
		PRINT,'  Found a matching date_obs, center and roll. No need to interpolate.'
	ENDIF ELSE BEGIN
          IF (interp_flag) THEN BEGIN
		PRINT,'  Interpolate for center using these Before and After date_obs:'
		PRINT,'    Before = ',anytim2utc(tai2utc(c_tai(bind)-32.184),/ecs)
		PRINT,'    After  = ',anytim2utc(tai2utc(c_tai(aind)-32.184),/ecs)
          ENDIF ELSE BEGIN
                PRINT,'Date_obs is during a long gap or a roll - using Before date_obs:'
                PRINT,'    Before = ',anytim2utc(tai2utc(c_tai(bind)-32.184),/ecs)
          ENDELSE
	ENDELSE
	HELP,/ST,adjusted
  ENDIF

;
; Adjust for binning. This is only relevant for center from stars.
;
    binfac=1
    IF tag_exist(shdr,'LEBXSUM') THEN binfac=(shdr.lebxsum>1) 
    	
    center[i].xcen= adjusted.xpos/binfac
    center[i].ycen= adjusted.ypos/binfac

next:
  ;rectify=0
    rollval= adjusted.roll
    IF (tel NE 'MLO' AND rollval EQ 0.0) THEN BEGIN
    	source=source +'; Using SOHO roll'
	IF tel EQ 'C2' THEN BEGIN
    	    	defroll=0.5 	; deg, +/- 0.1 
		varianc=0.1
		source=source+' + offset'
	ENDIF ELSE $
	IF tel EQ 'C3' THEN BEGIN
	    	defroll=-0.23 	; deg, +/- 0.07
		varianc=0.07
		source=source+' + offset'
	ENDIF ELSE BEGIN
	    	defroll=0.0 	; deg 
		varianc=0.0
	ENDELSE
    	IF (NOT KEYWORD_SET(silent)) THEN message,'LASCO Roll not available. Using SOHO roll value + estimate, +/-'+trim(varianc)+' deg',/info
	dobs=ANYTIM2UTC(date+' '+time)
    	pnt = GET_SC_POINT(dobs,type,/retain)
	pntroll=pnt.sc_roll
	IF (verbose) THEN help,pntroll
	IF dobs.mjd GT 55501 and (pntroll EQ 0 or pntroll EQ 180.) THEN pntroll=read_so_at_roll_dat(dobs, type)
	; SO_AT_R1H_20101105_V01.DAT
	; after 101101 and att_pre FITS file not available
	IF type EQ 'None' THEN source='No so_at_rol found; CROTA1 is from ecliptic.'
	; Sometimes so_at_roll are 180 deg off
	
	IF abs(pntroll) LT 3 and interpolatedroll NE 0 THEN rollval=interpolatedroll $
	; case in Oct 1996 where pntroll ~2.5 but is apparently incorrect, so don't trust pntroll near zero.
	ELSE BEGIN
	    IF dobs.mjd GT 52821 THEN $ 	; 2003/07/01
	    IF abs(abs(pntroll)-get_crota(date+' '+time)) gt 10 THEN pntroll=pntroll+180
	    
    	    rollval = pntroll + defroll
	ENDELSE
    ENDIF ELSE BEGIN
    
    	IF tag_exist(shdr,'CROTA1') THEN sroll=shdr.crota1 ELSE sroll=shdr.crota
    	IF abs(sroll-adjusted.roll) GT 170 THEN BEGIN

            msg='Center may have shifted with rectification; using header values.'
	    PRINT,''
	    message,msg,/info
	    PRINT,''
	    center[i].xcen=shdr.crpix1-1
	    center[i].ycen=shdr.crpix2-1    
            ;binfac=1.   ; no change
    	ENDIF 
    ENDELSE
    
    sunroll[i]=rollval
ENDFOR	; each header in
    
IF (NOT KEYWORD_SET(silent))   THEN IF (STRUPCASE(roll_xy) EQ 'CENTER') THEN print,'get_roll_or_xy center=',center ELSE help,rollval

    CENTER_OUT=center
    IF keyword_set(DEGREES) THEN $
      ROLL_OUT=sunroll $
    ELSE $
      ROLL_OUT=sunroll * !PI/180.0  ;convert from degrees to radians 
  
  IF (STRUPCASE(roll_xy) EQ 'CENTER') THEN BEGIN
    RETURN, center
  ENDIF ELSE BEGIN
    RETURN, roll_out
  ENDELSe
END

