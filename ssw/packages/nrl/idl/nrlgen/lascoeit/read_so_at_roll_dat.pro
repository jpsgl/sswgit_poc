; $Id: read_so_at_roll_dat.pro,v 1.4 2012/12/11 22:09:33 nathan Exp $
	FUNCTION read_so_at_roll_dat, DATE, TYPE, ERRMSG=ERRMSG, RETAIN=RETAIN,	$
		SUB_DATE=sub_date, FIND_CLOSEST=FIND_CLOSEST
;+
; Project     :	SOHO - CDS
;
; Name        :	
;
; Purpose     :	Get the SOHO roll from SO_AT_ROL_*.DAT files
;
; Category    :	Class3, Orbit
;
; Explanation : Valid for Bogart missin ONLY!!! (After 2010/11/01) 
;   	    	Read the  predictive roll file to get the
;               spacecraft pointing and roll information.  If no file is found,
;               then all parameters are returned as zero, and the type is
;               returned as "None".
;
; Syntax      :	Result = read_so_at_roll_dat( DATE  [, TYPE ] )
;
; Examples    :	
;
; Inputs      :	DATE	= The date/time value to get the attitude information
;			  for.  Can be in any CDS time format.
;
; Opt. Inputs :	None.
;
; Outputs     :	roll of SOHO from solar north in Deg, same sense as output of get_sc_point.pro
;
; Opt. Outputs:	TYPE	= Returns whether predictive data was
;			  used to calculate the result.  
;			  
;
; Keywords    :	
;
;
;		SUB_DATE= If data from a date other than was called is used,
;			  this keyword returns the date used. Otherwise returns
;			  ''.
;
;		ERRMSG	= If defined and passed, then any error messages will
;			  be returned to the user in this parameter rather than
;			  depending on the MESSAGE routine in IDL.  If no
;			  errors are encountered, then a null string is
;			  returned.  In order to use this feature, ERRMSG must
;			  be defined first, e.g.
;
;				ERRMSG = ''
;				Result = GET_SC_ATT( ERRMSG=ERRMSG, ... )
;				IF ERRMSG NE '' THEN ...
;
; Calls       :	CONCAT_DIR, READCOL, ANYTIM2UTC
;
; Common      :	 GET_AT_ROLL 
;
; Restrictions:	The attitude entries for the time closest to that requested is
;		used to calculate the parameters.  Since the attitude data
;		is calculated every 10 minutes, this should be correct within
;		+/-5 minutes.  No attempt is made to interpolate to closer
;		accuracy than that.
;
; Side effects:	Any data with too much variation (max-min) in the attitude data
;		are rejected.  The limits are one arcminute in pitch and yaw,
;		and one degree in roll.
;
; Prev. Hist. :	Parts copied from get_sc_att.pro
;
; History     :	
; $Log: read_so_at_roll_dat.pro,v $
; Revision 1.4  2012/12/11 22:09:33  nathan
; actually do check yyyy subdir
;
; Revision 1.3  2010/12/27 20:53:41  nathan
; save filename in last_file
;
; Revision 1.2  2010/12/27 20:52:14  nathan
; correct sign on returned value
;
; Revision 1.1  2010/12/27 20:26:06  nathan
; call read_so_at_roll_dat if att_pre FITS not found
;
;
; Contact     :	N.Rich, NRL
;-
;
	COMMON GET_at_roll, LAST_FILE, times, vals
;
;  Make sure that LAST_FILE is defined.
;
	IF N_ELEMENTS(LAST_FILE) EQ 0 THEN LAST_FILE = ''
;
;  Make up to two passes through the software.  In the first pass, look in the
;  top level directory.  In the second pass, if needed, try appending the year
;  to the directory.
;
	USE_YEAR = 0
	FIRST=1
	TEMP = ANYTIM2UTC(DATE,/EXT)
	S_YEAR = TRIM(TEMP.YEAR)
START_PASS:

        FILETYPE = "PredictFile"
        PATH = CONCAT_DIR('$ANCIL_DATA', 'attitude', /DIR)
        PATH = CONCAT_DIR(PATH, 'predictive', /DIR)
        IF USE_YEAR THEN PATH = CONCAT_DIR(PATH, S_YEAR, /DIR)
        NAME = 'SO_AT_ROL_' + ANYTIM2CAL(DATE,FORM=8,/DATE) + '_V*.DAT'
        FILENAME = CONCAT_DIR(PATH, NAME)
;
	IF FILENAME NE LAST_FILE THEN BEGIN
	; if day not matching day in common block

	;
	;  Look for any files that match the search criteria.
	;
        	FILES = FINDFILE(FILENAME, COUNT=COUNT)
 ;  A file was found.  Read in the one with the highest version number.
;
	    	TYPE = FILETYPE
	    	IF COUNT GT 1 THEN FILES = FILES(REVERSE(SORT(FILES)))
		
       	    	IF COUNT EQ 0 and (first) THEN BEGIN
		    first=0
		    IF OS_FAMILY() EQ 'vms' THEN BEGIN
        	    	FILENAME = CONCAT_DIR(PATH, '$'+NAME)
        	    	FILES = FINDFILE(FILENAME, COUNT=COUNT)
		    ENDIF ELSE BEGIN
		    	USE_YEAR=1
			goto, start_pass
		    ENDELSE
        	ENDIF
	;
	;  If not found in the top directory, try a year subdirectory.
	;
        	IF COUNT EQ 0 THEN BEGIN
    	    	    message,'Sorry; '+filename+' not found; returning 0.',/info
		    vals=dblarr(2)
		    times=[1,83999999]
		    type='None'
        	ENDIF ELSE BEGIN

    		    
		    break_file,FILES[0],dlog,dir,fname,sfx
		    print, 'Using ',fname+sfx,' for attitude info for ' +	$
			    anytim2utc(date,/ccsds,/date) + '.'
    ;
    ;
		    readcol, FILES[0], datetimes, vals, FORMAT='A'
		    ;
		    ; datetimes is YYYY-MM-DDTHH:MM:SS.SSS
		    ;
		    ; vals is roll in deg
		    ;
		    utc=anytim2utc(datetimes)
		    times=utc.time
		    ;
		    ; times is milliseconds
		    ;
		ENDELSE
		last_file=filename
	ENDIF
;
;  Find the closest entry to the target time.
;
	TARGET = ANYTIM2UTC(DATE)
	TARGET = TARGET.TIME
	DIFF = ABS(TIMEs - TARGET)
	MINDIF = MIN(abs(DIFF), W)
	result = -vals[w]
;
	GOTO, FINISH
;
;HANDLE_ERROR:
;	IF N_ELEMENTS(ERRMSG) NE 0 THEN ERRMSG = 'GET_SC_ATT: ' + MESSAGE $
;		ELSE MESSAGE, MESSAGE, /CONTINUE
FINISH:
	RETURN, RESULT
;
	END
