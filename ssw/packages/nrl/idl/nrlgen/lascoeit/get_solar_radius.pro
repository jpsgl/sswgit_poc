;+
; $Id: get_solar_radius.pro,v 1.4 2011/04/29 17:40:56 nathan Exp $
FUNCTION GET_SOLAR_RADIUS, hdr0, PIXEL=PIXEL, DISTANCE=distance, TELESCOPE=telescope, $
	USE_EXTERNAL=USE_EXTERNAL, _EXTRA=_extra
; NAME:
;       GET_SOLAR_RADIUS
;
; PURPOSE:
;       This function returns the radius of the sun from Earth in arc seconds.
;
; CATEGORY:
;       LASCO_ANALYSIS
;
; CALLING SEQUENCE:
;       Result = GET_SOLAR_RADIUS (Hdr)
;
; INPUTS:
;       Hdr:    A LASCO header structure
;   	    --or--
;   	    	time, if TELESCOPE= is set
;
; KEYWORDS:
;   	TELESCOPE=  Set to value of DETECTOR in fits header
;	PIXEL	Output result in pixels
;	DISTANCE	Set equal to variable which will contain distance 
;			to sun in km
;	/NOEXTERNAL	(obsolete/default) Call solar_ephem instead of sohoephem 
;   	/USE_EXTERNAL	Get value from external compiled fortran code sohoephem (platform dependency)
;   	    	    	Differs by up to 1.5 arcsec
;
; OUTPUTS:
;       solar radius in arc seconds
;
;
; PROCEDURE:
;	Determines the solar radius from the position of SOHO as
;	defined in the *.cdf format files.
;
; MODIFICATION HISTORY:
; $Log: get_solar_radius.pro,v $
; Revision 1.4  2011/04/29 17:40:56  nathan
; avoid changing input param
;
; Revision 1.3  2011/02/15 15:54:36  nathan
; get distance from solar_ephem
;
; Revision 1.2  2011/01/21 18:21:13  nathan
; Add TELESCOPE= which allows input to be time; make /NO_EXTERNAL the default
;
;       Written by:     D.A. Biesecker, 12 September 1996
;	General routine taken from POINTING3.PRO written by S.P. Plunkett
;       Modified 30 Sept. 1996 by DAB: return default values of the solar
;        radius if calls to read the SOHO orbit files fail.
;	RA Howard 5/20/97	Added keyword PIXEL to return solar radius in pixels
;	NB Rich 8/25/98		Use detector instead of telescop to allow MVIHDR
;	NB Rich 12/16/98	Use get_orbit_cdf2 instead of get_orbit_cdf
;	NB Rich 11/13/01	Use solar_ephem instead of sohoephem if linux is OS
;	NB Rich 07/15/03	Ditto for darwin (MaxOSX)
;	NB Rich 04.03.29	Fix /pixel for linux case
;   NB Rich, 05.08.19 - Try to make general for any (FITS) header
;	NB Rich 10.08.30	Linux now works with sohoephem? (for me anyway)
;	NB Rich 10.11.30	Add /NOEXTERNAL
;
;	12/01/10 @(#)get_solar_radius.pro	1.16 - LASCO IDL LIBRARY
;-
;
IF keyword_set(TELESCOPE) THEN BEGIN
; implies input is time not header
    date_obs=anytim2utc(hdr0)
    tel=strupcase(telescope)
    ishdr=0
    tai_obs=utc2tai(date_obs)
    IF keyword_set(PIXEL) THEN message,'Warning: result is for unbinned image.',/info
    hdr=tel
ENDIF ELSE BEGIN
    hdr=hdr0
   IF datatype(hdr) NE 'STC' THEN hdr=lasco_fitshdr2struct(hdr)
   tags=tag_names(hdr)
   tel = STRTRIM(STRUPCASE(hdr.detector),2)
   ishdr=1
   ; !!! NOTE: MVI hdr does not have TELESCOP or INSTRUME !!! -nbr, 05.08.19
    date_obs = anytim2utc(hdr.date_obs+' '+hdr.time_obs)
    tai_obs = utc2tai(date_obs) + hdr.exptime / 2.
ENDELSE
   telnum = where(tel EQ ['C1','C2','C3','EIT'], soho)

;  Check to see whether LASCO or HAO-MLO
;
   IF keyword_set(PIXEL) and ishdr and ((tel EQ 'MK3') or (tel EQ 'MK4')) THEN $
        return, hdr.CRRADIUS
   
;
; Obtain the midpoint time of the observation, 'YYYY/MM/DD HH:MM:SS.SSS'
;

	tdb_obs = tai_obs + 32.184
	tdb_utc = tai2utc(tdb_obs, /ecs)
;
;
; Use solar_ephem.pro by default because systems have problems with executing external
; binary in sohoephem.
;
;IF !version.os EQ 'linux' OR !version.os EQ 'darwin' OR NOT(soho) THEN BEGIN
;IF !version.os EQ 'darwin' OR NOT(soho) or get_host() EQ 'hercules' THEN BEGIN
; temp workaround because of lib error in sohoephem. -nr, 9/28/10

    yymmdd = utc2yymmdd(date_obs)
    solar_ephem,yymmdd,radius=radius,SOHO=soho, DIST=dsunau
    distance=dsunau*oneau('km')
    radius = radius*3600
noexternal=1

IF keyword_set(USE_EXTERNAL) and !version.os NE 'darwin' and (soho) THEN BEGIN
    noexternal=0
; BEGIN SOHO-specific computation from external
    ;
    ; Obtain the Julian Date (needed to get the solar ephemeris)
    ;

	    jd = cds2jd(tdb_utc)
	    tjd = jd.int + jd.frac

    ;
    ; Determine which type of ORBIT files exist.  
    ;

    type = orbit_file_type(anytim2utc(tdb_utc))

    ; If the files are of type 'CDF' then determine whether to
    ; use GET_ORBIT_CDF or GET_ORBIT_CDF2.  If the date is on or before
    ; March 10, 1996, use GET_ORBIT_CDF.  Otherwise, use GET_ORBIT_CDF2.
    ;

    icoord = 1                      ; if SOHO coordinates not found, use L1 coords.
    if type eq 'CDF' then begin
      cdf = tdb_obs lt utc2tai('11-Mar-1996 00:00:00.000')

    ;
    ; Read the appropriate CDF format file.
    ;

      if cdf then soho_orbit = get_orbit_cdf2(tdb_utc) else begin
        check = execute('soho_orbit = get_orbit_cdf2(tdb_utc)')
        if not check then goto, skipexternal ; Return if execute failed.
      endelse

      hec_pos = soho_orbit.hec_pos / 1.495979d8
      hec_vel = soho_orbit.hec_vel * (8.64d5 / 1.495979d8)
      icoord = 2
    ;
    ; Or else read the FITS file if it exists.  If not, return a default
    ; value of solar radius
    ;

    endif else if type eq 'FITS' then begin
        check = execute('soho_orbit = get_orbit_fits(tdb_utc)')

       if not check then goto, skipexternal ; Return if execute failed.

        hec_pos = [soho_orbit.hec_x, soho_orbit.hec_y, soho_orbit.hec_z]
        distance = SQRT(TOTAL(double(hec_pos)^2))
        hec_pos = hec_pos / 1.495979d8
        hec_vel = [soho_orbit.hec_vx, soho_orbit.hec_vy, soho_orbit.hec_vz]
        hec_vel = hec_vel * (8.64d5 / 1.495979d8)
        icoord = 2
    endif else if type eq 'NULL' then goto, skipexternal

    ;
    ; Obtain the SOHO orbit parameters.  Distance (AU) and velocity
    ; (AU/day).
    ;

	    hec_orbit = [hec_pos, hec_vel]

    ;
    ; Obtain the ephemerides for the sun and planets
    ;

	    sohoephem, tjd, hec_orbit, icoord, sunephem, planephem


    ;
    ; Return the radius of the sun (arcseconds).
    ;

    RADIUS = (sunephem.diameter.arcmin*60.d + sunephem.diameter.arcsec) / 2.d

ENDIF

goto, theend
    skipexternal:
    noexternal=1
    message,'Warning: call_external failed.',/info
theend:

IF (noexternal) THEN    message,'Using solar_ephem.pro',/info

IF (KEYWORD_SET(pixel)) THEN radius = radius/GET_SEC_PIXEL(hdr)

RETURN, radius
END
