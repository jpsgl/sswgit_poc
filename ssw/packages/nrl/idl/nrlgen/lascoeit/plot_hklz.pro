;
;+
; $Id: plot_hklz.pro,v 1.3 2013/01/30 20:12:51 nathan Exp $
;
; Widget routine to plot LASCO HK values.
;
; Use: plot_hklz
;
; NAME:
;       PLOT_HKLZ
;
; PURPOSE:
;        This subroutine plots various temperatures and voltages of LEB, 
;        C1, C2, C3, and EIT for a day (or range of days) using the level-0
;        housekeeping datafiles. between 1 to 9 plots can be displayed at
;        the same time with the options of saving them to a gif file or
;        printing them to a printer of choice.
;
;   	If present, will read in $HOME/hklz.sav before reading in files (summary data only).
;
; CALLING SEQUENCE:
;        PLOT_HKLZ
;
; Optional INPUTS:
;        infile     IDL saveset containing hk1_orig,hk3_orig,hk2_orig,old_start,old_stop for summary data.
;
; OUTPUTS:
;        None
;
; $Log: plot_hklz.pro,v $
; Revision 1.3  2013/01/30 20:12:51  nathan
; try to update functionality
;
; Jan 1998
; Ed Esfandiari - first version.
;                 This pro uses level-0 housekeeping files to plot various
;                 temperatures and voltages. It is based on plot_temps,
;                 plot_bs_pes, plot_ccd_temps, and plot_lev_volt pros.
;
; Jan 28 1998 AEE Added xzoom option.
;
; Sep 14 1998 AEE Added 21 Motor Encoder Positions from Hk2.
;
; Oct 19 1998 AEE Corrected DN plot for BS and PES.
;
; Dec 22 1998 AEE added y-axes rescale option.
;
; Dec 23 1998 AEE made y-axes rescale more precise.
;
; Jan 26 1999 AEE Fixed for Y2K problem.
;
; Sep 08 1999 AEE Fixed code to handle days with no/bad packets.
;
; Jun 29 2000 AEE Added psym option and corrected start/stop dates display.
;
; Jul 28 2000 AEE Added code to crate the gif files using white backgrounds.
;
; @(#)plot_hklz.pro	1.2 03/03/98 :LASCO IDL LIBRARY
;-
;

FUNCTION GET_LZ_DAILY_PCKTS, startt, stopt

message,'Checking which days are a
   IF (stopt EQ '') THEN stopt= startt 
   y0 = FIX(STRMID(startt, 0, 4)) & y1 = FIX(STRMID(stopt, 0, 4))
   m0 = FIX(STRMID(startt, 4, 2)) & m1 = FIX(STRMID(stopt, 4, 2))
   d0 = FIX(STRMID(startt, 6, 2)) & d1 = FIX(STRMID(stopt, 6, 2))
   m0 = m0 > 1 & m1 = m1 > 1
   d0 = d0 > 1 & d1 = d1 > 1

   arr = '000000'

   YEAR0 = ((y0 + 50) MOD 100) + 1950
   YEAR1 = ((y1 + 50) MOD 100) + 1950

   MONTHS = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
;   DAY = (JULDAY(1, 1, y0+1900) + 1) MOD 7
;   DAY1_NEXT_YEAR = (JULDAY(1, 1, y0+1900+1) + 1) MOD 7

   DAY = (JULDAY(1, 1, YEAR0) + 1) MOD 7
   DAY1_NEXT_YEAR = (JULDAY(1, 1, YEAR0+1) + 1) MOD 7

   case ((DAY1_NEXT_YEAR + 7 - DAY) mod 7) of
        2 :
        1 : MONTHS(1) = 28      ; Not a leap year
        else: MONTHS(8) = 19    ; 1752
        endcase

;   FOR y=y0, y1 DO BEGIN
FOR y= y0, y1 DO BEGIN
    nm0 = 1
    nm1 = 12
;     IF (y EQ y0) THEN nm0 = m0 
    IF (y EQ YEAR0) THEN nm0 = m0
;    IF (y EQ d1) THEN nm1 = m1
;     IF (y EQ y1) THEN nm1 = m1
     IF (y EQ YEAR1) THEN nm1 = m1
;    FOR m=m0, m1 DO BEGIN
       FOR d=nd0, nd1 DO BEGIN
;         add = STRMID(STRING(100+y,FORM='(i3)'),1,2) + $
         add = STRMID(STRING(y,FORM='(i4.4)'),2,2) + $
               STRMID(STRING(100+m,FORM='(i3)'),1,2) + $
               STRMID(STRING(100+d,FORM='(i3)'),1,2)
         arr = [arr, add]
       ENDFOR
   ENDFOR
   len = N_ELEMENTS(arr)
   IF (len LE 1) THEN RETURN, arr
   RETURN, arr(1:N_ELEMENTS(arr)-1)
END



function add_lz_days,hk,new_hk

;  shk=size(hk)
;  snew_hk=size(new_hk)
;  hk_all=bytarr(shk(1),shk(2)+snew_hk(2))
;  shk_all=size(hk_all)

;  hk_all(*,0:shk(2)-1)=hk
;  hk_all(*,shk(2):shk_all(2)-1)= new_hk 

;; These loops seem to be faster for concatenating more than 5 days:
;;  for i= 0L,shk(2)-1 do hk_all(*,i)=hk(*,i)
;;  for i= shk(2),shk_all(2)-1 do hk_all(*,i)=new_hk(*,i-shk(2))

;  hk_all= transpose([transpose(hk),transpose(new_hk)])
;  hk_all= [hk,new_hk]
  return, [hk,new_hk]
end





PRO PLOT_HKLZ_EVENT, event

  common WPLOT_COMMON,sv2,sv4,time2,time4,gap_ind2,gap_ind4,hk,hk1,hk2,hk3, $
                      old_start,old_stop,hk1_keep,hk3_keep,xzoom,x1,x2, $
                      hk1_orig,hk3_orig,hk2_keep,hk2_orig,yzoom
  common YSCALE_INFO, min_y,max_y,ptitle

  WIDGET_CONTROL, event.id, GET_UVALUE=uval
  WIDGET_CONTROL, event.top, GET_UVALUE=nonopv   ; get structure from UVALUE


  CASE (uval) OF

    'QUIT' : BEGIN	;** exit program
	WIDGET_CONTROL, /DESTROY, nonopv.base
	GOTO, done
    END

    'QUITandSAVE' : BEGIN	;** exit program
	save,filename=getenv_slash('HOME')+'hklz.sav',hk1_orig,hk3_orig,hk2_orig,old_start,old_stop
	WIDGET_CONTROL, /DESTROY, nonopv.base
	GOTO, done
    END

   'RTYPE': BEGIN
        nonopv.read_res= nonopv.read_types(event.index)
    END
    'PSYM': BEGIN
;        nonopv.psym_types= nonopv.psym_types(event.index)
        syms=[3,1,2,0,10]
        !psym= syms(event.index)
    END
    'PWIN' : BEGIN
        if(event.type eq 1 and xzoom eq 'true') then begin
;          print,'event type, x, y= ',event.type,event.x,event.y
          if(x1 eq -1) then begin
            x1= event.x
            xyouts,event.x,event.y,'X',/dev
            msg='First point is selected. Now select the second point.'
            WIDGET_CONTROL,nonopv.msglbl,SET_VALUE= msg  
          endif else begin
            x2= event.x
            xyouts,event.x,event.y,'X',/dev
            WIDGET_CONTROL,nonopv.msglbl,SET_VALUE='Plotting the selected region....'
            if(x2 lt x1) then begin
              tmp= x1 & x1=x2 & x2= tmp
            end
            xzoom= 'false'
            yzoom= 'xzoom_on' ; so that last y-range scale is used again.
            PT,hk1,hk2,hk3,nonopv,nonopv.selected,x1,x2,yzoom
            msg='Press X/Y ZOOM to zoom again. Press PLOT to plot the original data again.'
            WIDGET_CONTROL,nonopv.msglbl,SET_VALUE= msg
          endelse
        end
    END

    'XZOOM' : BEGIN
        fsel= where(nonopv.plotted_select eq 1,fcnt)
        IF (DATATYPE(hk1) NE 'BYT' or fcnt eq 0 ) THEN BEGIN 
          WIDGET_CONTROL,nonopv.msglbl,SET_VALUE='No plot to zoom on!'  
        ENDIF ELSE BEGIN
          xzoom= 'true'
          x1= -1 & x2= -1
          msg='Select a new x-axis range by clicking on two points on any plot.'
          WIDGET_CONTROL,nonopv.msglbl,SET_VALUE= msg 
        ENDELSE
    END

    'YZOOM' : BEGIN
        fsel= where(nonopv.plotted_select eq 1,fcnt)
        IF (DATATYPE(hk1) NE 'BYT' or fcnt eq 0 ) THEN BEGIN
          WIDGET_CONTROL,nonopv.msglbl,SET_VALUE='No plot to zoom on!'
        ENDIF ELSE BEGIN
          yzoom= 'true'
          msg='Select new y-scale(s).'
          WIDGET_CONTROL,nonopv.msglbl,SET_VALUE= msg
          PT,hk1,hk2,hk3,nonopv,nonopv.selected,-1,-1,yzoom
        ENDELSE
        yzoom= 'false'
    END

    'READ' : BEGIN
        x1= -1 & x2= -1 ; disable xzoom, if on.
        yzoom= 'false'  ; disable yzoom, if on.
        WIDGET_CONTROL,nonopv.msglbl,SET_VALUE=' '
        WIDGET_CONTROL,nonopv.startt,GET_VALUE=startt & startt=startt(0)
        WIDGET_CONTROL, nonopv.stopt, GET_VALUE=stopt & stopt = stopt(0)
        startt = STRTRIM(startt, 2)
        if(strlen(startt) ne 8) then begin 
          msg= 'Invalid date: '+startt+'. Use yyyymmdd format.'
          WIDGET_CONTROL,nonopv.msglbl,SET_VALUE= msg
          return
        end
        stopt  = STRTRIM(stopt , 2)
        if(strlen(stopt) ne 8) then begin 
          msg= 'Invalid date: '+startt+'. Use yyyymmdd format.' 
          WIDGET_CONTROL,nonopv.msglbl,SET_VALUE= msg
          return
        end
        if (stopt lt startt ) then begin
    	    msg= 'Invalid date range: '+startt+'-'+stopt
    	    WIDGET_CONTROL,nonopv.msglbl,SET_VALUE= msg 
    	    return
        end
        if(startt ge old_start and old_stop ge stopt and $
           nonopv.last_res eq nonopv.read_res) then begin
    	    msg= 'Packets for '+startt+'-'+stopt+' have been already read in.'
    	    WIDGET_CONTROL,nonopv.msglbl,SET_VALUE=msg
	    stop
    	    return 
        end
        nonopv.last_res= nonopv.read_res
        fst_hk= 'true'
        ;lz_days= GET_LZ_DAILY_PCKTS(startt,stopt) 
	tima=maketimearr(startt,stopt+'_01',1,/days)
	lz_days=utc2yymmdd(tima,/yyyy)
        
        if(nonopv.read_res eq 'Full') then begin
          pmsg= ['Reading and plotting the "Full" resolution data could ',$
                 'take a long time, especially for big date ranges. A ',$
                 'faster alternative is to use the "30 min" resolution ',$
                 'data, instead. Do you want to continue with the "Full" ',$
                 'resolution option?']
          res= WIDGET_MESSAGE(pmsg,/question)
          if(res ne "Yes") then return
        endif
        WIDGET_CONTROL, /HOUR

;spawn,'date'
    	IF old_start EQ '' or startt LT old_start THEN old_start=startt
        for i= 0,n_elements(lz_days)-1 do begin
    	    dayn=lz_days[i]
	    IF dayn LT old_stop and dayn GE old_start THEN goto, skipday
	  msg= 'Reading '+nonopv.read_res+' resolution packet files for '+ dayn+'....'
          WIDGET_CONTROL,nonopv.msglbl,SET_VALUE= msg
          if(nonopv.read_res eq 'Full') then begin
            nhk= readallhk(dayn,/hklz)
          endif else begin
            nhk= readallhk(dayn,/hklz,/low_res)
          endelse
          nhkz= size(nhk)
          if(nhkz(0) eq 0) then begin
            	msg= 'Sorry -  No HK data for '+dayn+'.'
            	WIDGET_CONTROL,nonopv.msglbl,SET_VALUE= msg
    	    	;wait,2
	    	openw,1,getenv_slash('HOME')+'plot_hklz.log',/append
		printf,1,'No data for ',dayn
		close,1
          endif else begin
;            nhk= transpose(nhk) ; change (105,n) to (n,105) so can concatenate
            nhk1 = gethkn(nhk,1)
            nhk2 = gethkn(nhk,2)
            nhk3 = gethkn(nhk,3)

;          Make sure gethkn does not return -1 (data file(s) exist but does not have what we need):

           if(DATATYPE(nhk1) eq 'INT' or DATATYPE(nhk2) eq 'INT' or DATATYPE(nhk3) eq 'INT') then begin
             WIDGET_CONTROL,nonopv.msglbl,SET_VALUE= 'Sorry - No hk1, hk2, or hk3 data for '+dayn+'.'
           endif else begin

;           Data is Ok. Now only keep fields that are needed and then transpose:

            nhk1= nhk1(hk1_keep,*)
            nhk2= nhk2(hk2_keep,*)
            nhk3= nhk3(hk3_keep,*)

            nhk1 = transpose(nhk1)
            nhk2 = transpose(nhk2)
            nhk3 = transpose(nhk3)
            if(fst_hk eq 'true') then begin 
    	    	fst_hk= 'false' 
    	    	hk1_orig= nhk1
    	    	hk2_orig= nhk2
    	    	hk3_orig= nhk3
            endif else begin 
    	    	hk1_orig= [temporary(hk1_orig),nhk1]
    	    	hk2_orig= [temporary(hk2_orig),nhk2]
    	    	hk3_orig= [temporary(hk3_orig),nhk3]
            endelse
           endelse
          endelse
          
	  IF dayn GT old_stop THEN old_stop=dayn
	  skipday:
	  
        end
	stop
        shk= size(hk1_orig)
        if(shk(0) ne 2) then return 
;       hk=transpose(temporary(hk));change back to original (294,n) from (n,294)
        hk1= transpose(hk1_orig)
        hk2= transpose(hk2_orig)
        hk3= transpose(hk3_orig)
;spawn,'date'

        WIDGET_CONTROL,nonopv.msglbl,SET_VALUE=' '
        erase
    END

    'PLOT_C' : BEGIN
        x1= -1 & x2= -1 ; disable xzoom, if on.
        yzoom= 'false'  ; disable yzoom, if on.
        hk1= transpose(hk1_orig) & hk2= transpose(hk2_orig) & hk3= transpose(hk3_orig)
        WIDGET_CONTROL,nonopv.msglbl,SET_VALUE=' '
        IF (DATATYPE(hk1) NE 'BYT') THEN BEGIN
          WIDGET_CONTROL,nonopv.msglbl, $
                         SET_VALUE='Must read in packet files first.'
        ENDIF ELSE BEGIN 
          WIDGET_CONTROL,nonopv.startt,GET_VALUE=startt & startt=startt(0)
          WIDGET_CONTROL, nonopv.stopt, GET_VALUE=stopt & stopt = stopt(0)
          startt = STRTRIM(startt, 2)
          stopt = STRTRIM(stopt, 2)
          if(startt ne old_start or stopt ne old_stop or $
             nonopv.last_res ne nonopv.read_res) then begin
            msg=['One or more of start/stop dates or resolution was changed',$
                 'but new packets were not read in.',$
                 '',$
                 ' * Press "Yes" to ignore the newly enterd value(s) and plot',$
                 '   the existing data.',$
                 '',$
                 ' * Press "No" and then use the "READ PACKETS" icon to read',$
                 '   in the new packets.']
            res= WIDGET_MESSAGE(msg,/question)
  stop
            if(res eq "No") then return
          end
          WIDGET_CONTROL, /HOUR
          WIDGET_CONTROL,nonopv.msglbl,SET_VALUE='Plotting....'
          nonopv.ytitle='Convert'
          PT,hk1,hk2,hk3,nonopv,nonopv.selected,x1,x2,yzoom
        ENDELSE
    END

    'PLOT_DN' : BEGIN
        x1= -1 & x2= -1 ; disable xzoom, if on.
        yzoom= 'false'  ; disable yzoom, if on.
        hk1= transpose(hk1_orig) & hk2= transpose(hk2_orig) & hk3= transpose(hk3_orig)
        WIDGET_CONTROL,nonopv.msglbl,SET_VALUE=' '
        IF (DATATYPE(hk1) NE 'BYT') THEN BEGIN
          WIDGET_CONTROL,nonopv.msglbl, $
                         SET_VALUE='Must read in packet files first.'
        ENDIF ELSE BEGIN
          WIDGET_CONTROL,nonopv.startt,GET_VALUE=startt & startt=startt(0)
          WIDGET_CONTROL, nonopv.stopt, GET_VALUE=stopt & stopt = stopt(0)
          startt = STRTRIM(startt, 2)
          stopt = STRTRIM(stopt, 2)
          if(startt ne old_start or stopt ne old_stop or $
             nonopv.last_res ne nonopv.read_res) then begin
            msg=['One or more of start/stop dates or resolution was changed',$
                 'but new packets were not read in.',$
                 '',$
                 ' * Press "Yes" to ignore the newly enterd value(s) and plot',$
                 '   the existing data.',$
                 '',$
                 ' * Press "No" and then use the "READ PACKETS" icon to read',$
                 '   in the new packets.']
            res= WIDGET_MESSAGE(msg,/question)
            if(res eq "No") then return
          end
          WIDGET_CONTROL, /HOUR
          WIDGET_CONTROL,nonopv.msglbl,SET_VALUE='Plotting....'
          nonopv.ytitle='DN'
          PT,hk1,hk2,hk3,nonopv,nonopv.selected,x1,x2,yzoom
        ENDELSE
    END

    'PNM': BEGIN
        WIDGET_CONTROL, event.id ,GET_VALUE= pname
        nonopv.printer= pname(0)
    END

    'PRINT' : BEGIN
        x1= -1 & x2= -1 ; disable xzoom, if on.
        WIDGET_CONTROL,nonopv.msglbl,SET_VALUE=' '
        fsel= where(nonopv.plotted_select eq 1,fcnt)
        IF (DATATYPE(hk1) NE 'BYT' or fcnt eq 0 ) THEN BEGIN 
          WIDGET_CONTROL,nonopv.msglbl,SET_VALUE='Nothing to print!'  
        ENDIF ELSE BEGIN
          pmsg= 'Use '+nonopv.printer+' printer?'
          res= WIDGET_MESSAGE(pmsg,/question)
          if(res ne "Yes") then return
          WIDGET_CONTROL, /HOUR
          WIDGET_CONTROL,nonopv.msglbl,SET_VALUE='Printing to '+nonopv.printer+'....'
          SET_PLOT, 'PS'
          DEVICE, /INCHES, XSIZE = 9.5, YSIZE = 7.5, $
                  /LANDSCAPE,  BITS_PER_PIXEL=8, FILENAME = 'hklzplot.ps'
          yzoom='print' ; so that last y-range scale is used.
          PT,hk1,hk2,hk3,nonopv,nonopv.plotted_select,x1,x2,yzoom
          DEVICE, /CLOSE
          SET_PLOT, 'X'
          spawn,'lp -o nobanner -d '+nonopv.printer+' hklzplot.ps',/sh
          wait,5
          SPAWN, '/bin/rm hklzplot.ps'
        ENDELSE
    END

    'GIFN' : BEGIN
        WIDGET_CONTROL, event.id ,GET_VALUE= gname
        nonopv.gifname= gname(0)
    END

    'SAVE' : BEGIN
        WIDGET_CONTROL,nonopv.msglbl,SET_VALUE=' '
        fsel= where(nonopv.plotted_select eq 1,fcnt)
        IF (DATATYPE(hk1) NE 'BYT' or fcnt eq 0 ) THEN BEGIN 
          WIDGET_CONTROL,nonopv.msglbl,SET_VALUE='Nothing to save!' 
        ENDIF ELSE BEGIN
          gmsg= 'Save in '+nonopv.gifname+'?'
          res= WIDGET_MESSAGE(gmsg,/question)
          if(res ne "Yes") then return
          WIDGET_CONTROL, /HOUR
          WIDGET_CONTROL,nonopv.msglbl,SET_VALUE='Saving as '+nonopv.gifname+'....'
          img = TVRD()
          ; revers the image (make the bakground white) before creating the gif file:
          img1= img
          z= where(img1 eq 0,zc)
          nz= where(img1 ne 0,nzc)
          if (nzc gt 0) then img1(nz)= 0
          if (zc gt 0 and nzc gt 0) then img1(z)= img(nz(0))
       
          TVLCT, r, g, b, /GET
          WRITE_GIF, nonopv.gifname, img1, r, g, b
          WIDGET_CONTROL,nonopv.msglbl,SET_VALUE=' '
        ENDELSE
    END

    'CNVRT' : BEGIN
        IF (event.select) THEN BEGIN
          nonopv.cnvrt = 1
        ENDIF ELSE BEGIN
          nonopv.cnvrt = 0
        ENDELSE
    END

    'SINGLE' : BEGIN
        IF (event.select) THEN BEGIN
          nonopv.single = 1
        ENDIF ELSE BEGIN
          nonopv.single = 0
        ENDELSE
    END

    'SELECT' : BEGIN
        ind = WHERE(nonopv.button_id EQ event.id)
        IF (event.select) THEN BEGIN
;         PRINT, 'Selected ', nonopv.fields(ind)
          nonopv.selected(ind) = 1
        ENDIF ELSE BEGIN
;         PRINT, 'De-selected ', nonopv.fields(ind)
          nonopv.selected(ind) = 0
        ENDELSE
    END

    ELSE : BEGIN
    END

  ENDCASE

  WIDGET_CONTROL, event.top, SET_UVALUE=nonopv

done:
END

;-----------------------------------------------------------------------


pro  pt_temps,plt_info,pind,p,hk,utc,ctind,min_y,max_y,ptitle,yzoom
  v = fix(hk(plt_info.offs(pind(p)),*))
  if(plt_info.ytitle eq 'Convert') then begin
    v = cnvrt_temps(v,ctind)
    !y.title = 'Temp (Deg C)'
  end
  mn= min(v)-1 & mx= max(v)+1
  if(yzoom eq 'false') then begin 
     min_y(p)= mn & max_y(p)= mx & ptitle(p)= plt_info.fields(pind(p))
  end
  !y.range=[0.0,0.0]
  !y.style= 1
  utplot,utc,v,title=ptitle(p),yrange=[min_y(p),max_y(p)],$
         timerange=timerange

  return
end


pro pt_bs_pes,plt_info,pind,p,hk,utc,min_y,max_y,ptitle,yzoom

    mtitle= plt_info.fields(pind(p))
    A_or_B= where(plt_info.fields eq 'LEB +5V')
    A_or_B= plt_info.offs(A_or_B(0))

    if(strpos(plt_info.fields(pind(p)),'LASCO PES') ge 0 and $
;       (hk(28) and 8) le 0) then begin ; PES side B
       (hk(A_or_B) and 8) le 0) then begin ; PES side B
      mtitle= plt_info.fields(pind(p))+' (B)' ; change title to PES side B
      mtitle= strmid(mtitle,0,9)+'(B) - '+strmid(mtitle,10,1)
      v = fix(hk(plt_info.offs(pind(p))+2,*))  
    endif else begin                      ; either PES side A or Boresight
      if(strpos(plt_info.fields(pind(p)),'LASCO PES') ge 0) then begin
        mtitle= plt_info.fields(pind(p)) ; change title to PES side A
        mtitle= strmid(mtitle,0,9)+'(A) - '+strmid(mtitle,10,1) 
      endif
      if(plt_info.ytitle eq 'Convert') then begin
        v = fix(hk(plt_info.offs(pind(p)),*)) - plt_info.cval(pind(p))
      endif else begin
        v = fix(hk(plt_info.offs(pind(p)),*))
      endelse
    endelse

    if(plt_info.ytitle eq 'Convert') then begin
      !y.title = 'Arc Sec'
      if(pind(p) le 19) then begin ; Boresight
        v = plt_info.cf(pind(p)) * v 
      endif else begin ; PES - determine side A or B?
;        if((hk(28) and 8) gt 0) then begin ; side A
        if((hk(A_or_B) and 8) gt 0) then begin ; side A
          if(strpos(plt_info.fields(pind(p)),'LASCO PES-X') ge 0) then begin
            v_y= fix(hk(plt_info.offs(pind(p)+1),*)) - plt_info.cval(pind(p)+1) 
;            v = 2.0 * v + 0.1 * v_y
            v = plt_info.cf(pind(p)) * v + plt_info.cf(pind(p)+1) * v_y
          endif else begin  ; PES-Y 
            v_x= fix(hk(plt_info.offs(pind(p)-1),*)) - plt_info.cval(pind(p)-1) 
;            v = 2.0 * v - 0.1 * v_x
            v = plt_info.cf(pind(p)-1) * v - plt_info.cf(pind(p)) * v_x
          endelse 
        endif else begin ; side B
          v = 2.0 * v
        endelse
      endelse
    end
    mn= min(v)-1 & mx= max(v)+1
    if(yzoom eq 'false') then begin 
      min_y(p)= mn & max_y(p)= mx & ptitle(p)= mtitle
    end
    !y.range=[0.0,0.0]
    !y.style= 1
    utplot,utc,v,title=ptitle(p),yrange=[min_y(p),max_y(p)],timerange=timerange
    return
end



pro pt_motor_epos,plt_info,pind,p,hk,utc,min_y,max_y,ptitle,yzoom
  v = fix(hk(plt_info.offs(pind(p)),*))
  v = v and '0F'X  ; pick up first 4 bits (from right) of the byte.
  mn= min(v)-1 & mx= max(v)+1
  !y.title = 'DN' 
  if(yzoom eq 'false') then begin 
    min_y(p)= mn & max_y(p)= mx & ptitle(p)= plt_info.fields(pind(p))
  end
  !y.range=[0.0,0.0]
  !y.style= 1
  utplot,utc,v,title=ptitle(p),yrange=[min_y(p),max_y(p)],$
         timerange=timerange
;         timerange=timerange,psym=10  ; use histogram (psym= 10).
  return
end



pro pt_ccd_temps,plt_info,pind,p,hk,utc,min_y,max_y,ptitle,yzoom
   vh= fix(hk(plt_info.offs(pind(p)),*))   ; high byte
   vl= fix(hk(plt_info.offs(pind(p))+1,*)) ; low byte
   v= (vh AND 3) * 256 +vl                  ; form 10 bit quantity
   v= REFORM(v)
   !y.title= 'DN'
   if(plt_info.ytitle eq 'Convert') then begin
     if(strpos(plt_info.fields(pind(p)),'Cold Finger') ge 0) then begin
;       allv= cnvrt_temps(v(0),0)
;       for j= 1,n_elements(v)-1 do allv= [allv,cnvrt_temps(v(j),0)] 
;       v= allv
       v= cnvrt_temps(v,0)
     endif else begin ; CCD Temps
       v= cnvrt_temps(v,1)
     endelse
     !y.title= 'Temp (Deg C)'
   endif
   mn= min(v)-1 & mx= max(v)+1
   if(yzoom eq 'false') then begin 
     min_y(p)= mn & max_y(p)= mx & ptitle(p)= plt_info.fields(pind(p))
   end
   !y.range=[0.0,0.0]
   !y.style= 1
   utplot,utc,v,title= ptitle(p),yrange=[min_y(p),max_y(p)],$
          timerange=timerange 
   return
end



pro pt_leb_volt,plt_info,pind,p,hk,utc,min_y,max_y,ptitle,yzoom
 
  v = fix(hk(plt_info.offs(pind(p)),*))
  if(plt_info.ytitle eq 'Convert') then begin
    !y.title = 'Voltage (Volt)'
    if(strpos(plt_info.fields(pind(p)),'LEB Current') ge 0) then begin 
      !y.title='Current (amp)' 
      w= where(v gt 157,nw) 
      w1= where(v lt 158,nw1)
      fv= float(v)
      if(nw gt 1) then fv(w)= (v(w)-158)*.1249+1.93
      if(nw1 gt 1) then fv(w1)= (v(w1)-128)*.05897
      v= fv 
    endif else begin 
      v = (v-128) * plt_info.cf(pind(p)) 
    endelse
  endif
  mn= min(v)-1 & mx= max(v)+1
  if(yzoom eq 'false') then begin 
    min_y(p)= mn & max_y(p)= mx & ptitle(p)= plt_info.fields(pind(p))
  end
  !y.range=[0.0,0.0]
  !y.style= 1
  utplot,utc,v,title= ptitle(p),yrange=[min_y(p),max_y(p)],$
         timerange=timerange 
  return
end



pro pt_cam_volt,plt_info,pind,p,hk,utc,min_y,max_y,ptitle,yzoom

  v = fix(hk(plt_info.offs(pind(p)),*) and 3) * 256 + $
      fix(hk(plt_info.offs(pind(p))+1,*))
  if(plt_info.ytitle eq 'Convert') then begin
    !y.title = 'Voltage (Volt)'
    case plt_info.fields(pind(p)) of
      'C1 Camera +15V': v = v*(.010*((200+130)/200.)) 
      'C1 Camera -15V': v = ((200+226)/200.)*(v*.01-15.3)+15.3
      'C1 Camera +24V': v = v*(.010*((200+110)/110.))
      'C1 Camera +10V': v = v*(.010*((825+110)/825.))
      'C1 Camera +5V Analog': v = v*.010
      'C1 Camera +5V Digital': v = v*.010
      'C2 Camera +15V': v = v*(.010*((200+130)/200.))
      'C2 Camera -15V': v = ((200+226)/200.)*(v*.01-15.3)+15.3
      'C2 Camera +24V': v = v*(.010*((200+110)/110.))
      'C2 Camera +10V': v = v*(.010*((825+110)/825.))
      'C2 Camera +5V Analog': v = v*.010
      'C2 Camera +5V Digital': v = v*.010
      'C3 Camera +15V': v = v*(.010*((200+130)/200.))
      'C3 Camera -15V': v = ((200+226)/200.)*(v*.01-15.3)+15.3
      'C3 Camera +24V': v = v*(.010*((200+110)/110.))
      'C3 Camera +10V': v = v*(.010*((825+110)/825.))
      'C3 Camera +5V Analog': v = v*.010
      'C3 Camera +5V Digital': v = v*.010
      'EIT Camera +15V': v = v*(.010*((200+130)/200.))
      'EIT Camera -15V': v = ((200+226)/200.)*(v*.01-15.3)+15.3
      'EIT Camera +24V': v = v*(.010*((200+110)/110.))
      'EIT Camera +10V': v = v*(.010*((825+110)/825.))
      'EIT Camera +5V Analog': v = v*.010
      'EIT Camera +5V Digital': v = v*.010
    endcase
  endif
  mn= min(v)-1 & mx= max(v)+1
  if(yzoom eq 'false') then begin 
    min_y(p)= mn & max_y(p)= mx & ptitle(p)= plt_info.fields(pind(p))
  end
  !y.range=[0.0,0.0]
  !y.style= 1
  utplot,utc,v,title= ptitle(p),yrange=[min_y(p),max_y(p)],$
         timerange=timerange 

  return
end



PRO PT,hk1,hk2,hk3,plt_info,selected,x1,x2,yzoom, timeint, single=single
  common YSCALE_INFO, min_y,max_y,ptitle

pind= where(selected eq 1,pcnt)
if(pcnt eq 0) then begin
  msg='Please select one or more (up to 9) fields to plot.'
  msg='Nothing is selected. Please select one or more (up to 9) fields first.'
  WIDGET_CONTROL,plt_info.msglbl,SET_VALUE=msg
  return
end
if(pcnt gt 9) then begin
  n= string(pcnt-9,'(i2)')+' field'
  if(pcnt gt 10) then n= n+'s' 
  msg='Maximum of 9 fields can be plotted at once. Deselect '+n+' and try again.'
  WIDGET_CONTROL,plt_info.msglbl,SET_VALUE=msg
  return
end

if (yzoom eq 'false') then begin 
  min_y= fltarr(pcnt)
  max_y= fltarr(pcnt)
  ptitle= strarr(pcnt)
  erase
endif else begin
  if(yzoom eq 'true') then begin 
    success= new_yscale(pcnt,min_y,max_y,ptitle)
    WIDGET_CONTROL,plt_info.msglbl,SET_VALUE=''
    if(not success) then return
    WIDGET_CONTROL, /HOUR
  end
  ; don't do anything for
  ; other yzoom values such as 'print' and 'xzoom_on' so that the y-range
  ; scale from the last time PT was called is used again.
endelse

case pcnt of
;  1: begin !p.multi=[0,1,1,0,1] & xr= 620 & fp= 60 & end
  1: begin !p.multi=[0,1,2,0,1] & xr= 620 & fp= 60 & end
  2: begin !p.multi=[0,1,2,0,1] & xr= 620 & fp= 60 & end
  3: begin !p.multi=[0,1,3,0,1] & xr= 660 & fp= 30 & end
  4: begin !p.multi=[0,2,2,0,1] & xr= 270 & fp= 60 
       if(x1 ge 410) then fp= 410
     end
  5: begin !p.multi=[0,2,3,0,1] & xr= 310 & fp= 30 
       if(x1 ge 380) then fp= 380
     end
  6: begin !p.multi=[0,2,3,0,1] & xr= 310 & fp= 30 
       if(x1 ge 380) then fp= 380
     end
  7: begin !p.multi=[0,3,3,0,1] & xr= 195 & fp= 30 
       if(x1 ge 260) then fp= 260 
       if(x1 ge 495) then fp= 495
     end
  8: begin !p.multi=[0,3,3,0,1] & xr= 195 & fp= 30 
       if(x1 ge 260) then fp= 260
       if(x1 ge 495) then fp= 495
     end
  9: begin !p.multi=[0,3,3,0,1] & xr= 195 & fp= 30 
       if(x1 ge 260) then fp= 260
       if(x1 ge 495) then fp= 495
     end
endcase 

IF KEYWORD_SET(timeint) THEN timerange=timeint ELSE timerange=0
IF NOT(KEYWORD_SET(single)) THEN single=0
 
  if(x1 ge 0 and x2 ge 0) then begin ;xzoom requested
    sz= size(hk1)
    hk1_pts= sz(2)
    p1=0 & p2=hk1_pts-1
    p1= (hk1_pts*(x1-fp))/xr
    p2= (hk1_pts*(x2-fp))/xr
    if(p1 lt 0 or p1 ge hk1_pts) then p1= 0
    if(p2 ge hk1_pts or p2 lt 0) then p2= hk1_pts-1
    hk1= hk1(*,p1:p2)
  endif

  if(x1 ge 0 and x2 ge 0) then begin ;xzoom requested
    sz= size(hk2)
    hk2_pts= sz(2)
    p1=0 & p2=hk2_pts-1
    p1= (hk2_pts*(x1-fp))/xr
    p2= (hk2_pts*(x2-fp))/xr
    if(p1 lt 0 or p1 ge hk2_pts) then p1= 0
    if(p2 ge hk2_pts or p2 lt 0) then p2= hk2_pts-1
    hk2= hk2(*,p1:p2)
  endif

  if(x1 ge 0 and x2 ge 0) then begin ;xzoom requested
    sz= size(hk3)
    hk3_pts= sz(2)
    p1=0 & p2=hk3_pts-1
    p1= (hk3_pts*(x1-fp))/xr
    p2= (hk3_pts*(x2-fp))/xr
    if(p1 lt 0 or p1 ge hk3_pts) then p1= 0
    if(p2 ge hk3_pts or p2 lt 0) then p2= hk3_pts-1
    hk3= hk3(*,p1:p2)
  endif
 
getcputime,hk1,firstday,time1,cdstime=utc1
;getcputime,hk2,firstday,time2,cdstime=utc2
getcputime,hk3,firstday,time3,cdstime=utc3

;help,hk1,hk2,hk3

!x.title = 'Time (Hours) Starting Date:  '+ utc2str(utc1(0))
!y.title = 'DN' 
!x.style=1
      
for p= 0, pcnt-1 do begin
    
 if (pind(p) ge 0 and pind(p) le 17) then begin ; Temperatures
   if(plt_info.pckts(pind(p)) eq 1) then $ 
     pt_temps,plt_info,pind,p,hk1,utc1,3,min_y,max_y,ptitle,yzoom $
   else $ 
     pt_temps,plt_info,pind,p,hk3,utc3,2,min_y,max_y,ptitle,yzoom
 endif else begin
   if(pind(p) ge 18 and pind(p) le 21) then begin ; Boresight and PES values
     pt_bs_pes,plt_info,pind,p,hk1,utc1,min_y,max_y,ptitle,yzoom
   endif else begin
     if(pind(p) ge 22 and pind(p) le 29) then begin ; CCD temps 
       pt_ccd_temps,plt_info,pind,p,hk1,utc1,min_y,max_y,ptitle,yzoom
     endif else begin
       if(pind(p) ge 30 and pind(p) le 41) then begin ; LEB voltages
         pt_leb_volt,plt_info,pind,p,hk1,utc1,min_y,max_y,ptitle,yzoom
       endif else begin
         if(pind(p) ge 42 and pind(p) le 65) then begin ; Camera's voltages
           pt_cam_volt,plt_info,pind,p,hk1,utc1,min_y,max_y,ptitle,yzoom
         endif else begin
           if(pind(p) ge 66 and pind(p) le 86) then begin ; Motors Encoder Pos
             pt_motor_epos,plt_info,pind,p,hk2,utc1,min_y,max_y,ptitle,yzoom
           endif
         endelse
       endelse
     endelse
   endelse
 endelse
end

!p.multi = 0 
!x.title = '' 
!y.title = '' 
empty 
plt_info.plotted_select= plt_info.selected
WIDGET_CONTROL,plt_info.msglbl,SET_VALUE=' '
return
end 
 


PRO PLOT_HKLZ, infile

common WPLOT_COMMON,sv2,sv4,time2,time4,gap_ind2,gap_ind4,hk,hk1,hk2,hk3, $
                    old_start,old_stop,hk1_keep,hk3_keep,xzoom,x1,x2, $
                    hk1_orig,hk3_orig,hk2_keep,hk2_orig,yzoom

   hk1= 'string_datatype'
   read_res= '30 min'
   xzoom= 'false' 
   yzoom= 'false'
   x1= -1 & x2= -1

IF n_params() LT 1 THEN infile=getenv_slash('HOME')+'hklz.sav'

IF datatype(hk1_orig) EQ 'UND' THEN BEGIN
    old_start= ''
    old_stop= ''
    hk1_orig= hk1
    hk2_orig= hk1
    hk3_orig= hk1
    IF file_exist(infile) THEN BEGIN
    	message,'Restoring '+infile,/info
    	restore,infile
    ENDIF
ENDIF
stop
;   num= 21 
;   num= 18
;   num= 18+4+8+36
   num= 18+4+8+36+21
   fields=STRARR(num) & pckts=INTARR(num) & offs=INTARR(num) & cnvrs=INTARR(num)
   cf= fltarr(num) & cval= INTARR(num)
    i=0 & fields(i)='LEB Power Converter Thermistor' & pckts(i)=1 & offs(i)= 40
;    i=i+1 & fields(i)=' C1 Thermistor 1' & pckts(i)=1 & offs(i)= 49 
;    i=i+1 & fields(i)=' C1 Thermistor 2' & pckts(i)=1 & offs(i)= 48 
;    i=i+1 & fields(i)=' C1 Thermistor 3' & pckts(i)=1 & offs(i)= 51 
    i=i+1 & fields(i)=' C2 Thermistor 1' & pckts(i)=1 & offs(i)= 50 
    i=i+1 & fields(i)=' C2 Thermistor 2' & pckts(i)=1 & offs(i)= 53 
    i=i+1 & fields(i)=' C2 Thermistor 3' & pckts(i)=1 & offs(i)= 52 
    i=i+1 & fields(i)=' C3 Thermistor 1' & pckts(i)=1 & offs(i)= 55 
    i=i+1 & fields(i)=' C3 Thermistor 2' & pckts(i)=1 & offs(i)= 54 
    i=i+1 & fields(i)=' C3 Thermistor 3' & pckts(i)=1 & offs(i)= 57 
    i=i+1 & fields(i)='EIT Thermistor 1' & pckts(i)=1 & offs(i)= 56 
    i=i+1 & fields(i)='EIT Thermistor 2' & pckts(i)=1 & offs(i)= 59 
    i=i+1 & fields(i)='EIT Thermistor 3' & pckts(i)=1 & offs(i)= 58
    i=i+1 & fields(i)='EIT Thermistor 4' & pckts(i)=1 & offs(i)= 61 
    i=i+1 & fields(i)='LASCO Zone Therm 1' & pckts(i)=3 & offs(i)=191 
    i=i+1 & fields(i)='LASCO Zone Therm 2' & pckts(i)=3 & offs(i)=190 
    i=i+1 & fields(i)='LASCO Zone Therm 3' & pckts(i)=3 & offs(i)=193 
    i=i+1 & fields(i)='LASCO Zone Therm 4' & pckts(i)=3 & offs(i)=192 
    i=i+1 & fields(i)='LASCO Zone Therm 5' & pckts(i)=3 & offs(i)=195 
    i=i+1 & fields(i)='EIT Zone Therm 6' & pckts(i)=3 & offs(i)=194 
    i=i+1 & fields(i)='EIT Zone Therm 7' & pckts(i)=3 & offs(i)=197 

    i=i+1 & fields(i)='LASCO BS-X' & pckts(i)=1 & offs(i)=47 & cf(i)= 0.9091
            cval(i)= 126
    i=i+1 & fields(i)='LASCO BS-Y' & pckts(i)=1 & offs(i)=46 & cf(i)= 1.0
            cval(i)= 185
    i=i+1 & fields(i)='LASCO PES-X' & pckts(i)=1 & offs(i)=43 & cf(i)=2.0
            cval(i)= 104  ; also 45 needed for side B.
    i=i+1 & fields(i)='LASCO PES-Y' & pckts(i)=1 & offs(i)=42 & cf(i)=0.1
            cval(i)= 129  ; also 44 needed for side B. 

    i=i+1 & fields(i)=' C1 Cold Finger Temp' & pckts(i)=1 & offs(i)=72  ; +73
    i=i+1 & fields(i)=' C1 CCD Temp' & pckts(i)=1 & offs(i)=74          ; +75
    i=i+1 & fields(i)=' C2 Cold Finger Temp' & pckts(i)=1 & offs(i)=120 ; +121
    i=i+1 & fields(i)=' C2 CCD Temp' & pckts(i)=1 & offs(i)=122         ; +123
    i=i+1 & fields(i)=' C3 Cold Finger Temp' & pckts(i)=1 & offs(i)=168 ; +169
    i=i+1 & fields(i)=' C3 CCD Temp' & pckts(i)=1 & offs(i)=170         ; +171
    i=i+1 & fields(i)='Eit Cold Finger Temp' & pckts(i)=1 & offs(i)=216 ; +217
    i=i+1 & fields(i)='Eit CCD Temp' & pckts(i)=1 & offs(i)=218         ; +219

    i=i+1 & fields(i)='LEB +5V' & pckts(i)=1 & offs(i)=28 & cf(i)= 0.0782
;   Note: 'LEB +5V' or hk1(28) is also used for figuring out PES side A or B.
    i=i+1 & fields(i)='LEB +9V' & pckts(i)=1 & offs(i)=30 & cf(i)= 0.0869
    i=i+1 & fields(i)='LEB +10V' & pckts(i)=1 & offs(i)=31 & cf(i)= 0.0978
    i=i+1 & fields(i)='LEB -10V' & pckts(i)=1 & offs(i)=33 & cf(i)= 0.0978
    i=i+1 & fields(i)='LEB +15V' & pckts(i)=1 & offs(i)=32 & cf(i)= 0.1567
    i=i+1 & fields(i)='LEB -15V' & pckts(i)=1 & offs(i)=35 & cf(i)= 0.1567
    i=i+1 & fields(i)='LEB +24V' & pckts(i)=1 & offs(i)=34 & cf(i)= 0.2346
    i=i+1 & fields(i)='LEB +31V' & pckts(i)=1 & offs(i)=37 & cf(i)= 0.3399
    i=i+1 & fields(i)='LEB -20V' & pckts(i)=1 & offs(i)=36 & cf(i)= 0.2058
    i=i+1 & fields(i)='LEB +120V' & pckts(i)=1 & offs(i)=39 & cf(i)= 1.1417
    i=i+1 & fields(i)='LEB Ground' & pckts(i)=1 & offs(i)=24 & cf(i)= 0.0782
    i=i+1 & fields(i)='LEB Current' & pckts(i)=1 & offs(i)=41

    ; next byte for each of the following fields is also needed (ie. 64 and 65
    ; for 'C1 Camera +15V').

    i=i+1 & fields(i)='C1 Camera +15V' & pckts(i)=1 & offs(i)=64
    i=i+1 & fields(i)='C1 Camera -15V' & pckts(i)=1 & offs(i)=62
    i=i+1 & fields(i)='C1 Camera +24V' & pckts(i)=1 & offs(i)=66
    i=i+1 & fields(i)='C1 Camera +10V' & pckts(i)=1 & offs(i)=70
    i=i+1 & fields(i)='C1 Camera +5V Analog' & pckts(i)=1 & offs(i)=68
    i=i+1 & fields(i)='C1 Camera +5V Digital' & pckts(i)=1 & offs(i)=76
    i=i+1 & fields(i)='C2 Camera +15V' & pckts(i)=1 & offs(i)=112
    i=i+1 & fields(i)='C2 Camera -15V' & pckts(i)=1 & offs(i)=110
    i=i+1 & fields(i)='C2 Camera +24V' & pckts(i)=1 & offs(i)=114
    i=i+1 & fields(i)='C2 Camera +10V' & pckts(i)=1 & offs(i)=118
    i=i+1 & fields(i)='C2 Camera +5V Analog' & pckts(i)=1 & offs(i)=116
    i=i+1 & fields(i)='C2 Camera +5V Digital' & pckts(i)=1 & offs(i)=124
    i=i+1 & fields(i)='C3 Camera +15V' & pckts(i)=1 & offs(i)=160
    i=i+1 & fields(i)='C3 Camera -15V' & pckts(i)=1 & offs(i)=158
    i=i+1 & fields(i)='C3 Camera +24V' & pckts(i)=1 & offs(i)=162
    i=i+1 & fields(i)='C3 Camera +10V' & pckts(i)=1 & offs(i)=166
    i=i+1 & fields(i)='C3 Camera +5V Analog' & pckts(i)=1 & offs(i)=164
    i=i+1 & fields(i)='C3 Camera +5V Digital' & pckts(i)=1 & offs(i)=172
    i=i+1 & fields(i)='EIT Camera +15V' & pckts(i)=1 & offs(i)=208
    i=i+1 & fields(i)='EIT Camera -15V' & pckts(i)=1 & offs(i)=206
    i=i+1 & fields(i)='EIT Camera +24V' & pckts(i)=1 & offs(i)=210
    i=i+1 & fields(i)='EIT Camera +10V' & pckts(i)=1 & offs(i)=214
    i=i+1 & fields(i)='EIT Camera +5V Analog' & pckts(i)=1 & offs(i)=212
    i=i+1 & fields(i)='EIT Camera +5V Digital' & pckts(i)=1 & offs(i)=220

;   21 Motor Encoder positions (from HK2):
    i=i+1 & fields(i)='C1 Door Position' & pckts(i)=2 & offs(i)= 16
    i=i+1 & fields(i)='C1 Polariz Position' & pckts(i)=2 & offs(i)= 20
    i=i+1 & fields(i)='C1 Filter Position' & pckts(i)=2 & offs(i)= 24
    i=i+1 & fields(i)='C1 Shutter Position' & pckts(i)=2 & offs(i)= 28
    i=i+1 & fields(i)='C2 Door Position' & pckts(i)=2 & offs(i)= 32
    i=i+1 & fields(i)='C2 Filter Position' & pckts(i)=2 & offs(i)= 36
    i=i+1 & fields(i)='C2 Polariz Position' & pckts(i)=2 & offs(i)= 40
    i=i+1 & fields(i)='C2 Shutter Position' & pckts(i)=2 & offs(i)= 44
;    i=i+1 & fields(i)='C2 Occult X Position' & pckts(i)=2 & offs(i)= 48
;    i=i+1 & fields(i)='C2 Occult Y Position' & pckts(i)=2 & offs(i)= 52
    i=i+1 & fields(i)='C3 Door Position' & pckts(i)=2 & offs(i)= 56
    i=i+1 & fields(i)='C3 Filter Position' & pckts(i)=2 & offs(i)= 60
    i=i+1 & fields(i)='C3 Polariz Position' & pckts(i)=2 & offs(i)= 64
    i=i+1 & fields(i)='C3 Shutter Position' & pckts(i)=2 & offs(i)= 68
    i=i+1 & fields(i)='Leg 1 Position' & pckts(i)=2 & offs(i)= 72
    i=i+1 & fields(i)='Leg 2 Position' & pckts(i)=2 & offs(i)= 76
    i=i+1 & fields(i)='EIT Door Position' & pckts(i)=2 & offs(i)= 80
    i=i+1 & fields(i)='EIT Sector Position' & pckts(i)=2 & offs(i)= 84
    i=i+1 & fields(i)='EIT Filter Position' & pckts(i)=2 & offs(i)= 88
    i=i+1 & fields(i)='EIT Shutter Position' & pckts(i)=2 & offs(i)= 92
    i=i+1 & fields(i)='FP Launch Lock Position' & pckts(i)=2 & offs(i)= 96
    i=i+1 & fields(i)='C1 Focus Position' & pckts(i)=2 & offs(i)= 100
    i=i+1 & fields(i)='C2 Focus Position' & pckts(i)=2 & offs(i)= 104

;   Mark to keep those hk1 bytes needed by this program to save time/memory:

    p1= where(pckts eq 1,p1_cnt)
    hk1_keep= offs(p1)
;   Also add extra hk1 bytes that are needed:
    hk1_keep= [hk1_keep,0,1,2,3,4,5,6,7,8,9,10,11]  ; add offset (cput time)
    hk1_keep= [hk1_keep,45,44,73,75,121,123,169,171,217,219]
    hk1_keep= [hk1_keep,65,63,67,71,69,77,113,111,115,119,117,125,161]
    hk1_keep= [hk1_keep,159,163,167,165,173,209,207,211,215,213,221]
    hk1_keep= hk1_keep(sort(hk1_keep))

;   Mark to keep those hk2 bytes needed by this program to save time/memory:

    p2= where(pckts eq 2,p2_cnt)
    hk2_keep= offs(p2)

;   Mark to keep those hk3 bytes needed by this program to save time/memory:

    p3= where(pckts eq 3,p3_cnt)
    hk3_keep= offs(p3)
;   Also add extra hk3 bytes that are needed:
    hk3_keep= [hk3_keep,0,1,2,3,4,5,6,7,8,9,10,11]  ; add offset (cput time)
    hk3_keep= hk3_keep(sort(hk3_keep))

;   Now map the old hk1,hk2,and hk3 offsets (from 294 bytes) to the new hk1_keep
;   and hk3_keep arrays (a lot less than 294 bytes per record):

    new_hk1_offs= offs(p1)
    for j= 0,p1_cnt-1 do new_hk1_offs(j)= where(hk1_keep eq new_hk1_offs(j)) 
    new_hk2_offs= offs(p2)
    for j= 0,p2_cnt-1 do new_hk2_offs(j)= where(hk2_keep eq new_hk2_offs(j))
    new_hk3_offs= offs(p3)
    for j= 0,p3_cnt-1 do new_hk3_offs(j)= where(hk3_keep eq new_hk3_offs(j))

    offs(p1)= new_hk1_offs
    offs(p2)= new_hk2_offs
    offs(p3)= new_hk3_offs

    button_id = LONARR(num)
    selected = BYTARR(num)
    plotted_select= BYTARR(num)
;    selected(0:8) = 1
    cnvrt = 1
    single = 1

    LOADCT, 0
    TVLCT, 255,0,0, 1      ;** load red into entry 1
    TVLCT, 0,255,0, 2      ;** load green into entry 2
    TVLCT, 142,229,238, 3  ;** load blue into entry 3
    TVLCT, 255,255,0,4     ;** load yellow into entry 4
    TVLCT, 200,0,0, 5      ;** load dull red into entry 5
    TVLCT, 0,200,0, 6      ;** load dull green into entry 6
    TVLCT, 0,206,237, 7    ;** load dull blue into entry 7
    TVLCT, 200,200,0,8     ;** load dull yellow into entry 8
    TVLCT, 255,255,255, 9  ;** load white into entry 9

    font='-adobe-times-bold-r-normal--14-140-75-75-p-77-iso8859-1'

    ;********************************************************************
    ;** SET UP WIDGETS **************************************************

    base = WIDGET_BASE(/ROW, TITLE='LASCO/EIT LEVEL-0 HOUSE KEEPING TEMPERATURES, VOLTAGES, AND MOTORS', /FRAME)

    col1 = WIDGET_BASE(base, /COLUMN)

    col2 = WIDGET_BASE(col1, /COLUMN, /FRAME)
    row = WIDGET_BASE(col2, /ROW)

;    info_file= '/net/corona/export/raid2/hklz/' ; use a env. variable instead 
    dir= '/net/corona/lasco/lz/hk/g015/'
    IF old_start eq '' and old_stop eq '' THEN BEGIN
	yli=file_search(dir+'????')
	; get years
	help,yli
	nli=n_elements(yli)
	dli=file_search(yli[nli-1]+'/*.d01')
	help,dli
	nda=n_elements(dli)
	break_file,dli[nda-1],di,pa,fin,sf
	break_file,yli[nli-1],diy,pay,finy,sfy
	en_str=utc2yymmdd(doy2utc(fix(strmid(fin,1,3)),finy),/yyyy)
	break_file,dli[0],di,pa,fin,sf
	st_str=utc2yymmdd(doy2utc(fix(strmid(fin,1,3)),finy),/yyyy)
    ENDIF ELSE BEGIN
    	st_str=old_start
	en_str=old_stop
    ENDELSE




    lab = WIDGET_LABEL(row, VALUE='START :')
    startt = WIDGET_TEXT(row, XSIZE=10, VALUE=st_str, /EDITABLE, UVALUE='START')
    lab = WIDGET_LABEL(row, VALUE='  Available Dates:',FONT=font)
    row = WIDGET_BASE(col2, /ROW)
    lab = WIDGET_LABEL(row, VALUE='STOP  :')
    stopt = WIDGET_TEXT(row, XSIZE=10, VALUE=en_str, /EDITABLE, UVALUE='STOP')

    ; If an env. variable is used instead of next path, change it in the
    ; readallhk.pro as well.

    txt='  last date: '+en_str
    lab = WIDGET_LABEL(row, VALUE=txt,FONT=font)

;    junk = WIDGET_BUTTON(col2, VALUE='READ PACKETS', UVALUE='READ')
    rd= WIDGET_BASE(col2,/ROW)
;    junk= WIDGET_LABEL(rd,VALUE='READ PACKETS: ')
;    junk = WIDGET_BUTTON(rd, VALUE='Full Res', UVALUE='READ')
;    junk = WIDGET_BUTTON(rd, VALUE='30 min', UVALUE='READ')

    read_types= ['30 min','Full']
    junk = WIDGET_BUTTON(rd, VALUE='READ PACKETS', UVALUE='READ')
    junk= WIDGET_LABEL(rd,VALUE=' Resolution:')
    rdid= CW_BSELECTOR(rd,UVALUE='RTYPE',read_types)

    col2 = WIDGET_BASE(col1, /COLUMN, /NONEXCLUSIVE, /FRAME, $
           /SCROLL, X_SCROLL_SIZE=200, Y_SCROLL_SIZE=400)
    FOR i=0, num-1 DO BEGIN
       button_id(i) = WIDGET_BUTTON(col2, VALUE=fields(i), UVALUE='SELECT')
    ENDFOR

;    col2 = WIDGET_BASE(col1, /COLUMN, /NONEXCLUSIVE, /FRAME)
;    conv = WIDGET_BUTTON(col2, VALUE='Convert to Deg C', UVALUE='CNVRT')

;    col2 = WIDGET_BASE(col1, /COLUMN, /NONEXCLUSIVE, /FRAME)
;    sing = WIDGET_BUTTON(col2, VALUE='Plot all on single plot', UVALUE='SINGLE')
   
;    junk = WIDGET_BUTTON(plt, VALUE='PLOT', UVALUE='PLOT')

    plt= WIDGET_BASE(col1,/ROW)
;    junk= WIDGET_LABEL(plt,VALUE=' ')
;    conv = WIDGET_BUTTON(plt,VALUE='PLOT (DN value)', UVALUE='PLOT_DN')
    conv = WIDGET_BUTTON(plt,VALUE='PLOT (DN)', UVALUE='PLOT_DN')
    junk= WIDGET_LABEL(plt,VALUE=' ')
    junk = WIDGET_BUTTON(plt,VALUE='PLOT (convert)', UVALUE='PLOT_C')

    !psym= 3  ; use '.' as default

    psym_types= ['.','+','*','line','hist']
    junk= WIDGET_LABEL(plt,VALUE='psym:')
    rdid= CW_BSELECTOR(plt,UVALUE='PSYM',psym_types)

    prnt= WIDGET_BASE(col1,/ROW,/FRAME)
    junk = WIDGET_BUTTON(prnt, VALUE='PRINT PLOTS', UVALUE='PRINT')
    printer= 'hp4-247'
    junk = CW_FIELD(prnt,VALUE=printer,title=" ",UVALUE='PNM',/ROW,/ALL_EVENTS)

    gifs= WIDGET_BASE(col1,/ROW,/FRAME)
    junk = WIDGET_BUTTON(gifs, VALUE='SAVE AS GIF', UVALUE='SAVE')
    gifname=  'hklz_plot.gif'
    junk = CW_FIELD(gifs,VALUE=gifname,title=" ",UVALUE='GIFN',/ROW,/ALL_EVENTS)

;    junk = WIDGET_BUTTON(col1, VALUE='QUIT', UVALUE='QUIT')


;    draw = WIDGET_DRAW(base, XSIZE=700, YSIZE=600, RETAIN=2, /FRAME)
;    WIDGET_CONTROL, draw, GET_VALUE=win      ; find out window's index no.

    r1= WIDGET_BASE(base,/COL)

    r2= WIDGET_BASE(r1,/ROW)
;    junk= WIDGET_LABEL(r2,VALUE='                        ')
;    junk = WIDGET_BUTTON(r2, VALUE='XZOOM', UVALUE='XZOOM',/FRAME)
    blnks='                   '
    junk= WIDGET_LABEL(r2,VALUE=blnks)
    junk = WIDGET_BUTTON(r2, VALUE='QUIT', UVALUE='QUIT',/FRAME)
    junk = widget_button(r2, VALUE='QUIT and Save $HOME/hklz.sav', Uvalue='QUITandSAVE',/frame)

    draw = WIDGET_DRAW(r1, XSIZE=700, YSIZE=600, RETAIN=2, /button_events,$
                       UVALUE= 'PWIN',/FRAME)
    WIDGET_CONTROL, draw, GET_VALUE=win      ; find out window's index no.

;    dum= WIDGET_BASE(r1,/ROW)  
;    mmm= WIDGET_LABEL(dum,VALUE='   ',uvalue='mmm',FONT=font) 

;    msgbase= WIDGET_BASE(r1,/ROW,/frame)
    msgbase= WIDGET_BASE(r1,/ROW)
  
    junk= WIDGET_LABEL(msgbase,VALUE=' ') 
    junk = WIDGET_BUTTON(msgbase, VALUE='YZOOM', UVALUE='YZOOM',/FRAME)
    junk = WIDGET_BUTTON(msgbase, VALUE='XZOOM', UVALUE='XZOOM',/FRAME) 
;    txt='                                                           '
    txt='                                                 '
    txt= txt+txt+txt
    msglbl= WIDGET_LABEL(msgbase,VALUE=txt,uvalue='msg',FONT=font)

    ;********************************************************************
    ;** REALIZE THE WIDGETS *********************************************

    WIDGET_CONTROL, /REAL, base

    FOR i=0, N_ELEMENTS(button_id)-1 DO $
       WIDGET_CONTROL, button_id(i), SET_BUTTON=selected(i)
;    WIDGET_CONTROL, conv, SET_BUTTON=cnvrt
;    WIDGET_CONTROL, sing, SET_BUTTON=single

    ;********************************************************************
    ;** Create a structure that will be carried around as the UVALUE of 
    ;** the main base (BASE)

    nonopv = { $
	base:base, 			$
	win:win, 			$
        read_res: read_res,             $
        last_res: read_res,             $
        read_types: read_types,         $
        psym_types: psym_types,         $
	fields:fields, 			$
	button_id:button_id, 		$
	startt:startt, 			$
	stopt:stopt, 			$
	pckts:pckts, 			$
	offs:offs, 			$
        cf:cf,                          $
        cval:cval,                      $
	cnvrs:cnvrs, 			$
	cnvrt:cnvrt, 			$
	single:single, 			$
	selected:selected, 		$
        plotted_select:plotted_select,  $
        printer:printer,                $
        gifname:gifname,                $
        ytitle:'Unit (DN)',             $
        msglbl:msglbl                   $
    }

    ; Load the structure into the main base's UVALUE for one way trip to handler
    WIDGET_CONTROL, base, SET_UVALUE=nonopv

    XMANAGER, 'PLOT_LZ_HK_TEMPS', base, EVENT_HANDLER='PLOT_HKLZ_EVENT',/no_block

RETURN
END
