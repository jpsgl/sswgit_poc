; $Id: make_image_c1.pro,v 1.3 2010/12/09 22:54:47 nathan Exp $
;
; NAME:		GETC1IM3
;
; PURPOSE:	Returns a legible C1 image which has been on/off 
;		subtracted, etc. Scales for exposure time. 
;
; INPUTS:	onimg	INTARR	C1  image
;   	    	onhdr	STC 	C1  image header structure
;
;   	-OR-	onimg:  STRING  name of C1  online image
;				with diskpath
;		
; KEYWORDS:	/USEALL	    If set, increases number of closed-door images which
;   	    	    	    probably have a height limit of 1.42 Rsun at south, but 
;   	    	    	    which are unbinned
;
; OUTPUTS:	floating point array
;
; ROUTINES CALLED:
;   	    	lasco_readfits.pro
;		find_closest.pro
;		reduce_std_size.pro
;   	    	fpc1_3img.pro
;
; AUTHOR:	Nathan Rich, Dec 2009
;
; $Log: make_image_c1.pro,v $
; Revision 1.3  2010/12/09 22:54:47  nathan
; add stuff
;
; Revision 1.2  2009/12/31 17:27:59  nathan
; implementing c1 green line maps with fpc1_2img.pro
;

function correct_exptime, h

exp0=h.exp0

if exp0-h.expcmd gt 25. then exp0 = h.exp0-32.

if exp0 eq h.expcmd then expt=h.exptime else expt=exp0+h.exp3

IF expt/h.exptime NE 1. THEN BEGIN
    message,'expt= '+trim(expt)+' while h.exptime= '+trim(h.exptime),/info
    wait,2
ENDIF

if expt lt 0. then expt=exp0+h.exp3

return,expt

end

function make_image_c1, onimg0, onhdr, USEALL=useall, RESET=reset, MASK_OCC=mask_occ, OFF_HDR=h1, $
    	    DEBUG=debug
;COMMON get_im, 	off_s,off_times,ff_ratio, off_hdr, prev, pylonim, l1, ffv
COMMON c1lists, offop, drcls, coff1c, coff2c, conimgc
       ; filenames and date-obs in TAI format for offgreenline, offgreenlinedoorclosed, ongreenlinedoorclosed

filterstr=['Ca XV','Fe X','Orange','Na I','Fe XIV']
polarstr =['Clear','H Alpha','-60 Deg','0 Deg','+60 Deg']

; list bad closed door images
bad=['12000753.fts','12000754.fts','12000810.fts','12000812.fts','12028747.fts', '12028748.fts','12028749.fts','12083259.fts']

IF datatype(onimg0) EQ 'STR' THEN BEGIN
    img=lasco_readfits(onimg0,onhdr)
ENDIF ELSE img=onimg0
IF datatype(onhdr) NE 'STC' THEN onhdr=fitshead2struct(onhdr,/date2underscore)
onhdr=scc_update_history(onhdr,'$Id: make_image_c1.pro,v 1.3 2010/12/09 22:54:47 nathan Exp $')

IF keyword_set(USEALL) THEN maxr1row=257 ELSE maxr1row=onhdr.r1row
onwl=long(onhdr.fp_wl_up*10)/10.
pol=where(onhdr.polar EQ polarstr)
pol=pol[0]
fil=where(onhdr.filter EQ filterstr)
fil=fil[0]
lpnum=cnvrt_lp(onhdr.lp_num)
expt=float(fix(correct_exptime(onhdr)))
help,onwl,pol,fil,expt

IF pol[0] LT 0 THEN message,'Unrecognized onhdr.POLAR'
IF fil[0] LT 0 THEN message, 'Unrecognized onhdr.FILTER'

IF onwl GT 5295 and onwl LT 5309 THEN BEGIN
    offwl=5309.2 
    coff1fn= GETENV('NRL_LIB')+'/lasco/data/calib/fVof0596.fts'
    coff2fn= GETENV('NRL_LIB')+'/lasco/data/calib/fVof0996.fts'
    conimfn= GETENV('NRL_LIB')+'/lasco/data/calib/fVon0996.fts'
    IF datatype(coff1c) EQ 'UND' or keyword_set(RESET) THEN coff1c=lasco_readfits(coff1fn,hc1)
    IF datatype(coff2c) EQ 'UND' or keyword_set(RESET) THEN coff2c=lasco_readfits(coff2fn,hc2)
    IF datatype(conimc) EQ 'UND' or keyword_set(RESET) THEN conimc=lasco_readfits(conimfn,ho1)
ENDIF ELSE IF onwl GT 6370 and onwl LT 6390 THEN BEGIN
    offwl=6380.9
    coff1fn= GETENV('NRL_LIB')+'/lasco/data/calib/fXof0996.fts'
    conimfn= GETENV('NRL_LIB')+'/lasco/data/calib/fXon0996.fts'
    IF datatype(coff1c) EQ 'UND' or keyword_set(RESET) THEN coff1c=lasco_readfits(coff1fn,hc1)
    ;IF datatype(coff2c) EQ 'UND' or keyword_set(RESET) THEN coff2=lasco_readfits(coff2fn,hc2)
    IF datatype(coff1c) EQ 'UND' or keyword_set(RESET) THEN conimc=lasco_readfits(conimfn,ho1)

ENDIF ELSE message,'Offline wavelength not defined for '+trim(onwl)

IF keyword_set(USEALL) THEN BEGIN
    coff1=coff1c
    coff2=coff2c 
ENDIF ELSE BEGIN
    coff1=coff2c
    coff2=coff1
ENDELSE
conim=conimc

useprev=1
IF datatype(offop) EQ 'STC' THEN $
IF offwl NE long(offop[0].fp_wl_upl*10)/10. THEN useprev=0

goto, skipdrcls

IF datatype(drcls) EQ 'UND' THEN BEGIN
; get list of all door cls at beginning
    files2=getc1list( 5000,2000,STC=drcls,/DOORCLS)
; get rid of bad files
    udobs=anytim2utc(drcls.date_obs)
; 970113 - 970117 are masked images
    wnm=where(udobs.mjd LT 50461 or udobs.mjd GT 50465)
    drcls=drcls[wnm]
    ns=n_elements(drcls)
    nb=n_elements(bad)
    wg=replicate(1,ns)
    FOR i=0,nb-1 DO BEGIN
    	w=where(drcls.filename EQ bad[i],nw)
	IF nw GT 0 THEN wg[w]=0
    ENDFOR
    wgood=where(wg EQ 1,nwg)
    
    help,drcls,wgood
    print,'Omitting ',trim(ns-nwg),' bad door closed files.'
    wait,5
    drcls=drcls[wgood]
ENDIF    

; Find appropriate door closed images
won=where(drcls.filter EQ fil[0] and drcls.polar EQ pol[0] and drcls.r1row LE maxr1row and $
    	  drcls.exptime GT expt and drcls.exptime LT expt+1 and $
	  drcls.fp_wl_upl GT onwl and drcls.fp_wl_upl LT onwl+0.1, nwon)
woff=where(drcls.filter EQ fil[0] and drcls.polar EQ pol[0] and drcls.r1row LE maxr1row and $
    	  drcls.exptime GT expt and drcls.exptime LT expt+1 and $
	  drcls.fp_wl_upl GT offwl and drcls.fp_wl_upl LT offwl+0.1, nwoff)

; closed door images should link from level_05 to misc/door_cls
offgcf=drcls[woff].diskpath+'/'+drcls[woff].filename
offgcdobs=anytim2tai(drcls[woff].date_obs)

ongcf=drcls[won].diskpath+'/'+drcls[won].filename
ongcdobs=anytim2tai(drcls[won].date_obs)

help,offgf,offgcf,ongcf

stop

noffgc=n_elements(offgcf)
ci1=(find_closest(dobs0,offgcdobs,/less))>0
ci2=ci1+1
IF ci2 GT noffgc-1 THEN ci2=ci1-1

nongc=n_elements(ongcf)
ci0=find_closest(dobs0,ongcdobs)

cref10=lasco_readfits(offgcf[ci1],hc1)
cref1 =reduce_std_size(cref10,hc1,/full,/bias)/correct_exptime(hc1)

cref20=lasco_readfits(offgcf[ci2],hc2)
cref2 =reduce_std_size(cref20,hc2,/full,/bias)/correct_exptime(hc2)

cimg0=lasco_readfits(ongcf[ci0],hc0)
cimg =reduce_std_size(cimg0,hc0,/full,/bias)/correct_exptime(hc0)

skipdrcls:

IF datatype(offop) EQ 'UND' or ~useprev THEN BEGIN
; init different lists from database for C1 processing
    files1=getc1list(offwl,STC=offop)
;IDL[iapetus]>help,/str,offop                                                     
;** Structure LASCO$FDFRRELNNPFDN_LWLISSSREP, 12 tags, length=60, data length=58:
;   FILENAME        STRING    '12000834.fts'
;   DATE_OBS        STRING    '1996-02-23 23:37:21'
;   FP_WL_UPL       FLOAT           5302.42
;   R1ROW           INT            161
;   R1COL           INT            117
;   R2ROW           INT            832
;   R2COL           INT            948
;   EXPTIME         FLOAT           10.0625
;   LEBXSUM         INT              1
;   NAXIS1          INT           1024
;   NAXIS2          INT            512
;   POLAR           INT              0
;   FILTER          INT              4
;   DISKPATH        STRING    '/net/corona/cplex2/lz_data/level_05/960223/c1'
ENDIF

; Find appropriate door open off-line images
woffo=where(offop.filter EQ fil and offop.polar EQ pol and $
    	    offop.r1row LE maxr1row and $
    	    offop.exptime GT expt and offop.exptime LT expt+1 , nwoffo)

IF nwoffo EQ 0 THEN BEGIN
	print,string(7b)    ; bell
	print,'No offline images found to match  ',onhdr.fp_wl_up,', ', onhdr.polar,', ', onhdr.filter,', EXPT=',trim(expt)
	wait,5
	return,fltarr(1024,1024)
ENDIF
    
offgf=offop[woffo].diskpath+'/'+offop[woffo].filename
offgdobs=anytim2tai(offop[woffo].date_obs)

; Find appropriate offline images

dobs0=anytim2tai(onhdr.date_obs+' '+onhdr.time_obs)

noffg=n_elements(offgf)
i1=(find_closest(dobs0,offgdobs,/less))>0
ref10=lasco_readfits(offgf[i1],h1)
; find 2nd open door offline  to match 2nd closed door image
;i2=find_closest(offgcdobs[ci2],offgdobs)
;stop
IF keyword_set(DEBUG) THEN BEGIN
    help,img
    IF datatype(onimg0) EQ 'STR' THEN print,onimg0 ELSE print,onhdr.filename
    help,ref10
    print,offgf[i1]
    wait,5
ENDIF

; Now prep 2 images
onimg=reduce_std_size(img,onhdr,/full,/bias)/expt
onhdr.exptime=1.

ref1 =reduce_std_size(ref10,h1,/full,/bias)/correct_exptime(h1)

;ref20=lasco_readfits(offgf[i2],h2)
;ref2 =reduce_std_size(ref20,h2,/full,/bias)/correct_exptime(h2)

print,'Done with reduce_std_size.pro'
IF keyword_set(DEBUG) THEN wait,5

IF lpnum EQ 19 THEN sumfac=8. ELSE sumfac=1.
; It appears that LP_NUM=Sum/Diff is a factor of 8x bigger than Line Scan

im= fpc1_2img(ref1/sumfac, onimg/sumfac, coff1, coff2, conim)

IF keyword_set(MASK_OCC) THEN BEGIN
    ;sunc = GET_SUN_CENTER(hdr, /NOCHECK, FULL=1024); JAKE ADDED /FULL 030127
    arcs = GET_SEC_PIXEL(onhdr, FULL=1024)
    yymmdd=UTC2YYMMDD(STR2UTC(onhdr.date_obs+' '+onhdr.time_obs))
    solar_ephem,yymmdd,radius=radius,/soho
    asolr = radius*3600
    r_sun = asolr/arcs

    ;** draw mask

    set_plot,'z'
    device,set_resolution=[1024,1024] 
    TV,bytarr(1024,1024)
    TVCIRCLE, 1.05*r_sun,onhdr.crpix1,onhdr.crpix2, /FILL, COLOR=1
    TVCIRCLE, r_sun, onhdr.crpix1,onhdr.crpix2, color=2, THICK=4      
    tmp_img = TVRD()
    set_plot,'x'   
    
    mask=where(tmp_img EQ 1)
    circle=where(tmp_img EQ 2)
    im[mask]=median(im)
    im[circle]=max(im)
ENDIF 
    
return,im

end	
	
	
	
	
	
	
	
	
	
	
