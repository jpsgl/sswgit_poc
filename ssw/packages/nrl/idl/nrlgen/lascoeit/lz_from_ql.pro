FUNCTION LZ_FROM_QL, hdr0, HDR=hdr, TIME=time, CAMERA=camera, PATH=path, LOUD=loud, QL=ql
;+
; $Id: lz_from_ql.pro,v 1.7 2011/04/25 21:37:10 nathan Exp $
; NAME: LZ_FROM_QL
;
; PURPOSE:
;	Finds the filename of the LZ image that corresponds
;	to the input image description or header, or QL if LZ not found.
;   	Also works with Level-1 images.
;
; CATEGORY:
;	UTILITY
;
; CALLING SEQUENCE:
;	Result = LZ_FROM_QL(hdr) 
;  OR 	Result = LZ_FROM_QL(TIME = time)
;  OR 	Result = LZ_FROM_QL(TIME = time, CAMERA = cam)
;
; INPUTS:
;	HDR:	A LASCO header structure OR FITS strarr
;
; OPTIONAL INPUTS:
;	TIME:	The time at which the image was taken (any time format)
;	CAMERA: 'c1', 'c2', 'c3', or 'c4'
;
; KEYWORDS:
;   	/QL 	Search for quicklook file
;
; OUTPUTS:
;	Returns the fits filename of the lz image with path.
;
; OPTIONAL OUTPUTS:
;	PATH=	A named variable into which the full path of the 
;		lz fits file will be stored.
;
; PROCEDURE:
;	Uses img_hdr.txt files to find the lz images that match ql images.
;	There are three different ways to call the function:
;		1. With a header
;		2. With a time
;		3. With a time and camera
;	Calling the function in either the first or third method is most efficient.
;	If a header is supplied, neither time or camera should be supplied.
; EXAMPLE:
;	Result = LZ_FROM_HDR(Hdr)
;	Result = LZ_FROM_HDR(Hdr, PATH = path)
;
; MODIFICATION HISTORY:
; 	Written by:	Jeff Walters, Sep 1997
; $Log: lz_from_ql.pro,v $
; Revision 1.7  2011/04/25 21:37:10  nathan
; allow eit
;
; Revision 1.6  2011/03/31 20:19:32  nathan
; search for QL if LZ img_hdr.txt not found
;
; Revision 1.5  2011/02/08 19:48:45  nathan
; fix qltime bug; implement /LOUD
;
; Revision 1.4  2011/01/25 18:53:05  nathan
; add /QL option; search for minute not second in img_hdr.txt
;
; Revision 1.3  2010/12/09 22:54:05  nathan
; change change hdr input to argument, other enhancements
;
; Revision 1.2  2010/03/23 21:10:21  nathan
; return none if directory not found
;
; Revision 1.1  2008/04/14 16:38:57  nathan
; moved from lasco/idl/inout for get_sun_center.pro
;	
;	@(#)lz_from_ql.pro	1.4 11/22/05 LASCO IDL LIBRARY
;-


cam = ''
qltime='1995/01/01'
fname=''
IF keyword_set(HDR) THEN hdr0=hdr
IF N_PARAMS() EQ 1 or keyword_set(HDR) THEN BEGIN
    IF datatype(hdr0) NE 'STC' THEN hdr=lasco_fitshdr2struct(hdr0) ELSE hdr=hdr0
    qltime = hdr.date_obs + ' ' + hdr.time_obs
    cam = STRLOWCASE(hdr.detector)
    fname=hdr.filename
ENDIF 
IF KEYWORD_SET(TIME) THEN qltime = time 
IF KEYWORD_SET(CAMERA) THEN cam = STRLOWCASE(camera)      
tryagain=1
IF cam EQ 'eit' THEN cam='c4'

   ;** Cutoff is Mar 1, 1996 00:00:00 in TAI
   cutoff = 1204329630 
   szti=size(qltime)
   ut = anytim2utc(qltime)
   tai =utc2TAI(ut)
   IF (tai le cutoff) THEN lztime0 = tai - 1 ELSE lztime0 = tai
   ulztime = ANYTIM2UTC(lztime0)
   lztime = utc2str(ulztime,/ecs,/trunc)
   
retry:
IF keyword_set(QL) THEN rdir = concat_dir(getenv('QL_IMG'),'level_05') ELSE $
rdir = concat_dir(getenv('LZ_IMG'),'level_05')

IF ut.mjd GT 49718 and cam NE '' THEN dir=filepath(cam,root=rdir,subdir=utc2yymmdd(ulztime)) ELSE $
    return,'none'

grepval=strmid(lztime,11,6) ; HH:MM:
; use time only because spacing varies'
IF fname NE '' THEN lvl=strmid(fname,1,1) ELSE lvl='0'
IF lvl EQ '4' or lvl EQ '5' THEN grepval=strmid(fname,2,10)

    grepf=concat_dir(dir,'img_hdr.txt')
    IF file_exist(grepf) THEN BEGIN
    	cmd="grep '" + grepval + "' " + grepf
    	IF keyword_set(LOUD) THEN print,cmd
    	spawn, cmd, result, /sh
    ENDIF ELSE BEGIN
    	ql=1
	IF (tryagain) THEN goto,retry
	tryagain=0
    ENDELSE
   result = result[0]
   IF keyword_set(LOUD) THEN print,'Result= ',result
   lz_file = STRMID(result, 0, 12)
   
   path = dir
   IF result EQ '' and (tryagain) THEN BEGIN
    	IF ulztime.time/1000. LT 300 THEN ulztime.mjd=ulztime.mjd-1 ELSE ulztime.mjd=ulztime.mjd+1
	tryagain=0
	goto, retry
    ENDIF
   IF result eq '' THEN return,'none'
   RETURN, concat_dir(dir,lz_file)
END
