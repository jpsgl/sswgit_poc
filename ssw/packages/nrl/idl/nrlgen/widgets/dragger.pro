;+
;Dragger.pro
;Nishant Patel, Russell Howard, Arnaud Thernisien, et. al.
;July 23, 2004
;dragger,/DEMO for sample data
;dragger,/DEMO,/SOLID for sample solid data
;Normally called from frontend.pro, see MANUAL
; CVS:
;  $Id: dragger.pro,v 1.3 2007/02/13 17:16:06 antunes Exp $
;-
function d_objReadNoff, $
             file, $            ; IN: filename
             xr, $              ; OUT: x radius
             yr, $              ; OUT: y radius
             zr                 ; OUT: z radius

COMPILE_OPT idl2, hidden

t0 = systime(1)
s = ' '
npsize = 1

RESTORE, file


xr = [min(x, max = xx), xx]     ;Get ranges
yr = [min(y, max = xx), xx]
zr = [min(z, max = xx), xx]

sc = [xr[1]-xr[0], yr[1]-yr[0], zr[1]-zr[0]] ;Ranges
xr[0] = (xr[1] + xr[0])/2.0     ;Midpoint
yr[0] = (yr[1] + yr[0])/2.0
zr[0] = (zr[1] + zr[0])/2.0
s = max(sc)                     ;Largest range...

x = (x - xr[0]) / s
y = (y - yr[0]) / s
z = (z - zr[0]) / s

xr = [-0.7, 0.7]                ;Fudge the ranges
yr = xr
zr = xr
s = OBJ_NEW("IDLgrPolygon", TRANSPOSE([[x],[y],[z]]), $
            SHADING=1, $
            POLY=mesh, COLOR=[200,200,200])

; print, 'D_OBJREADNOFF:', systime(1)-t0
RETURN, s

end                             ;   of d_objReadNoff

;--------------------------
FUNCTION NORM_COORD, inRange

rtype = SIZE(inRange, /TYPE)
if (rtype eq 5) then range = inRange else range = FLOAT(inRange)

scale = [-range[0]/(range[1]-range[0]), 1/(range[1]-range[0])]

RETURN, scale

END


;--------------------------
FUNCTION Orb::Init, POS=pos, RADIUS=radius, DENSITY=density, $
                       TEX_COORDS=tex_coords, _EXTRA=e

    IF (self->IDLgrModel::Init(_EXTRA=e) NE 1) THEN RETURN, 0
    self.pos = [0.0,0.0,0.0]
    self.radius = 1.0
    self.density = 1.0
    IF (N_ELEMENTS(pos) EQ 3) THEN $
        self.pos = pos
    IF (N_ELEMENTS(radius) EQ 1) THEN $
        self.radius = radius
    IF (N_ELEMENTS(density) EQ 1) THEN $
        self.density = density
    IF (N_ELEMENTS(tex_coords) EQ 1) THEN $
        self.texture = tex_coords
    self.oPoly = OBJ_NEW('IDLgrPolygon', SHADING=1, /REJECT, _EXTRA=e)
    self->Add,self.oPoly
    self->BuildPoly

    RETURN, 1
END


;----------------------------------------------------------------------------
PRO orb::Cleanup
    OBJ_DESTROY, self.oPoly
    self->IDLgrModel::Cleanup
END

;----------------------------------------------------------------------------
PRO orb::SetProperty, POS=pos, RADIUS=radius, DENSITY=density, _EXTRA=e
    rebuild = 0B
    self->IDLgrModel::SetProperty, _EXTRA=e
    self.oPoly->SetProperty, _EXTRA=e
    IF (N_ELEMENTS(pos) EQ 3) THEN BEGIN
        self.pos = pos
        rebuild = 1B
    ENDIF
    IF (N_ELEMENTS(radius) EQ 1) THEN BEGIN
        self.radius = radius
        rebuild = 1B
    ENDIF
    IF (N_ELEMENTS(density) EQ 1) THEN BEGIN
        self.density = density
        rebuild = 1B
    ENDIF
    IF (rebuild) THEN self->BuildPoly
END

;----------------------------------------------------------------------------
PRO orb::GetProperty, POS=pos, RADIUS=radius, DENSITY=density,$
                         POBJ=pobj, _REF_EXTRA=re

    self->IDLgrModel::GetProperty, _EXTRA=re
    self.oPoly->GetProperty, _EXTRA=re
    pos = self.pos
    radius = self.radius
    density = self.density
    pobj = self.oPoly
END

PRO orb::Print
    PRINT, self.pos
    PRINT, self.radius
    PRINT, self.density
END

;----------------------------------------------------------------------------
PRO orb::BuildPoly
    nrows = LONG(20.0*self.density)
    ncols = LONG(20.0*self.density)
    IF (nrows LT 2) THEN nrows = 2
    IF (ncols LT 2) THEN ncols = 2
    nverts = nrows*ncols + 2
    nconn = (ncols*(nrows-1)*5) + (2*ncols*4)
    conn = LONARR(ncols*(nrows-1)*5 + 2*ncols*4)
    verts = FLTARR(3, nverts)
    IF (self.texture NE 0) THEN $
        tex = FLTARR(2,nverts)
    i = 0L
    j = 0L
    k = 0L
    tzinc = (!PI)/FLOAT(nrows+1)
    tz = (!PI/2.0) - tzinc
    FOR k=0,(nrows-1) DO BEGIN
        z = SIN(tz)*self.radius
        r = COS(tz)*self.radius
        t = 0
        IF (self.texture NE 0) THEN BEGIN
            tinc = (2.*!PI)/FLOAT(ncols-1)
        ENDIF ELSE BEGIN
            tinc = (2.*!PI)/FLOAT(ncols)
        ENDELSE
        FOR j=0,(ncols-1) DO BEGIN
            verts[0,i] = r*COS(t) + self.pos[0]
            verts[1,i] = r*SIN(t) + self.pos[1]
            verts[2,i] = z + self.pos[2]
            IF (self.texture NE 0) THEN BEGIN
                tex[0,i] = t/(2.*!PI)
                tex[1,i] = (tz+(!PI/2.0))/(!PI)
            ENDIF
            t = t + tinc
            i = i + 1L
        ENDFOR
        tz = tz - tzinc
    ENDFOR
    top = i
    verts[0,i] = self.pos[0]
    verts[1,i] = self.pos[1]
    verts[2,i] = self.radius*1.0 + self.pos[2]
    i = i + 1L
    bot = i
    verts[0,i] = self.pos[0]
    verts[1,i] = self.pos[1]
    verts[2,i] = self.radius*(-1.0) + self.pos[2]

    IF (self.texture NE 0) THEN BEGIN
        tex[0,i] = 0.5
        tex[1,i] = 0.0
        tex[0,i-1] = 0.5
        tex[1,i-1] = 1.0
    ENDIF

    i = 0
    FOR k=0,(nrows-2) DO BEGIN
        FOR j=0,(ncols-1) DO BEGIN
            conn[i] = 4
            conn[i+4] = k*ncols + j
            w = k*ncols + j + 1L
            IF (j EQ (ncols-1)) THEN w = k*ncols
            conn[i+3] = w
            w = k*ncols + j + 1L + ncols
            IF (j EQ (ncols-1)) THEN w = k*ncols + ncols
            conn[i+2] = w
            conn[i+1] = k*ncols + j + ncols
            i = i + 5L
            IF ((self.texture NE 0) AND (j EQ (ncols-1))) THEN $
                i = i - 5L
        ENDFOR
    ENDFOR
    FOR j=0,(ncols-1) DO BEGIN
        conn[i] = 3
        conn[i+3] = top
        conn[i+2] = j+1L
        IF (j EQ (ncols-1)) THEN conn[i+2] = 0
        conn[i+1] = j
        i = i + 4L
        IF ((self.texture NE 0) AND (j EQ (ncols-1))) THEN $
            i = i - 4L
    ENDFOR
    FOR j=0,(ncols-1) DO BEGIN
        conn[i] = 3
        conn[i+3] = bot
        conn[i+2] = j+(nrows-1L)*ncols
        conn[i+1] = j+(nrows-1L)*ncols+1L
        IF (j EQ (ncols-1)) THEN conn[i+1] = (nrows-1L)*ncols
        i = i + 4L
        IF ((self.texture NE 0) AND (j EQ (ncols-1))) THEN $
            i = i - 4L
    ENDFOR
    self.oPoly->SetProperty, DATA=verts, POLYGONS=conn
    IF (self.texture NE 0) THEN $
        self.oPoly->SetProperty, TEXTURE_COORD=tex
END

;----------------------------------------------------------------------------
PRO orb__define
    struct = { orb, $
               INHERITS IDLgrModel, $
               pos: [0.0,0.0,0.0], $
               radius: 1.0, $
               density: 1.0, $
               texture: 0, $
               oPoly: OBJ_NEW() $
             }
END
;$Id: dragger.pro,v 1.3 2007/02/13 17:16:06 antunes Exp $
;
;  [Very few parts] Copyright (c) 1997-2002, Research Systems, Inc. All rights reserved.
;       Unauthorized reproduction prohibited.
;
;+
;  FILE:
;       dragger.pro
;
;  CALLING SEQUENCE: dragger
;
;  PURPOSE:
;       
;
;  MAJOR TOPICS: Widgets
;
;  CATEGORY:
;       IDL Demo System
;
;  INTERNAL FUNCTIONS and PROCEDURES:
;       pro d_widgetsEvent            -  Event handler
;       pro d_widgetsCleanup          -  Cleanup
;       pro d_widgets                 -  Main procedure
;
;
;  REFERENCE: IDL Reference Guide, IDL User's Guide
;
;  NAMED STRUCTURES:
;       none.
;
;  COMMON BLOCS:
;       none.
;
;  MODIFICATION HISTORY:
;   96,   DAT        - Written.
;   98,   ACY        - Major rewrite.
;   04,   NSP        - Major rewrite.
;-


; -----------------------------------------------------------------------------
;
;  Purpose:  Function returns the 3 angles of a space three 1-2-3
;            given a 3 x 3 cosine direction matrix
;            else -1 on failure.
;
;  Definition :  Given 2 sets of dextral orthogonal unit vectors
;                (a1, a2, a3) and (b1, b2, b3), the cosine direction matrix
;                C (3 x 3) is defined as the dot product of:
;
;                C(i,j) = ai . bi  where i = 1,2,3
;
;                A column vector X (3 x 1) beomes X' (3 x 1)
;                after the rotation as defined as :
;
;                X' = C X
;
;                The space three 1-2-3 means that the x rotation is first,
;                followed by the y rotation, then the z.
;
function angle3123, $
    cosMat           ; IN: cosine direction matrix (3 x 3)


    ;  Verify the input parameters
    ;
    if (N_PARAMS() ne 1) then begin
        PRINT,'Error in angle3123: 1 parameters must be passed.'
        RETURN, -1
    endif
    sizec = size(cosMat)
    if (sizec[0] ne 2) then begin
        PRINT,'Error, the input matrix must be of dimension 2'
        RETURN, -1
    endif
    if ((sizec[1] ne 3) or (sizec[2] ne 3)) then begin
        PRINT,'Error, the input matrix must be 3 by 3'
        RETURN, -1
    endif

    ;  Compute the 3 angles (in degrees)
    ;
    ;cosMat = TRANSPOSE(cosMat)
    angle = FLTARR(3)

    angle[0]=asin(-cosMat[1,2])
    c1=cos(angle[0])
    if (ABS(c1) lt 1.0e-6) then begin
        angle[2] = ATAN(-cosMat[0,1], cosMat[0,0])
        angle[1] = 0.0
    endif else begin
        angle[1] = ATAN( cosMat[0,2], cosMat[2,2])
        angle[2] = ATAN( cosMat[1,0], cosMat[1,1])
    endelse
    

    RETURN, angle * (180./!DPI)

end                             ;   of angle3123

FUNCTION EULER_PARAMS, theta, phi, psi
;
;   Calculates the Euler Parameters, given the Euler angles
;
th = theta*!DTOR
ph = phi * !DTOR
ps = psi * !DTOR
th2 = 0.5 * th
ph2 = 0.5 * ph
ps2 = 0.5 * ps
euler = FLTARR(4)
euler[0] = COS (ph2+ps2) * COS(th2)
euler[1] = COS (ph2-ps2) * SIN(th2)
euler[2] = COS (ph2-ps2) * SIN(th2)
euler[3] = COS (ph2+ps2) * COS(th2)
RETURN,euler


END
; -----------------------------------------------------------------------------
;
;  Purpose:  Function returns the cosine direction matrix (3 x 3)
;            given the space three 1-2-3 rotation angles(i.e. rotation around
;            x axis, followed by Y axis, then z axis),
;            else -1 on failure.
;
;  Definition :  Given 2 sets of dextral orthogonal unit vectors
;                (a1, a2, a3) and (b1, b2, b3), the cosine direction matrix
;                C (3 x 3) is defined as the dot product of:
;
;                C(i,j) = ai . bi  where i = 1,2,3
;
;                A column vector X (3 x 1) becomes X' (3 x 1)
;                after the rotation as defined as :
;
;                X' = C X
;

function space3123, $
    phi, $        ; IN: angle of rotation around the x axis(in degrees)
    theta, $          ; IN: angle of rotation around the y axis(in degrees)
    gamma           ; IN: angle of rotation around the z axis(in degrees)


    ;  Verify the input parameters.
    ;
    if (N_PARAMS() ne 3) then begin
        PRINT,'Error in space3123: 3 parameters must be passed.'
        RETURN, -1
    endif


    cosMat = ROTNMAT(phi,theta,gamma)
    ;print,cosMat

    ;print,'ANGLES:',phi,theta,gamma
    ;print,'ANGLE3123:',angle3123(cosMat)
    
    RETURN, cosMat


end    ;   of space3123


; -----------------------------------------------------------------------------
;
;  Purpose:  Draw view.  Some platforms throw math errors that are
;            beyond our control.  Supress the printing of those errors.
;
pro d_widgetsDraw, sState

;  Flush and print any accumulated math errors
;
void = check_math(/print)


;  Silently accumulate any subsequent math errors, unless we are debuggung.
;
orig_except = !except
!except = ([0, 2])[keyword_set(sState.debug)]


;  Draw.
;
sState.drawWindowID->Draw, sState.oView


;  Silently (unless we are debuggung) flush any accumulated math errors.
;
void = check_math(PRINT=keyword_set(sState.debug))


;  Restore original math error behavior.
;
!except = orig_except
end
;
; -----------------------------------------------------------------------------
;
;  Purpose:  Event handler
;
pro d_widgetsEvent, $
    sEvent     ; IN: event structure

common share1, WIDS, COMMONMP

    ;  Quit the application using the close box.
    ;
    if (TAG_NAMES(sEvent, /STRUCTURE_NAME) EQ $
        'WIDGET_KILL_REQUEST') then begin
        WIDGET_CONTROL, sEvent.top, /DESTROY
        RETURN
    endif


    ;  Get the info structure from top-level base.
    ;
    WIDGET_CONTROL, sEvent.top, GET_UVALUE=sState, /NO_COPY

    ;  Determine which event.
    ;
    WIDGET_CONTROL, sEvent.id, GET_UVALUE=eventval


    ;  Take the following action based on the corresponding event.
    ;
    case eventval of
        'SUNCHECK' : begin
            sState.oSun->SetProperty,HIDE=sEvent.SELECT
            d_widgetsDraw,sState
        end
        'FITS' : begin
            ;BG IMAGE
            filenameIN=pickfile(TITLE='Select parameter data file')
            if (filenameIN ne '') AND $
              (strpos(filenameIN,'/',/REVERSE_SEARCH) ne strlen(filenameIN)-1) then begin
                im=readfits(filenameIN,hdr)
                im=discri_pobj(im)
                norm_im,im,hdr,im
                ;bgnd=readfits('$MONTHLY_IMAGES/2m_orcl_xx0107.fts',hdrbgnd)
                ;im=im-bgnd/float(sxpar(hdrbgnd,'exptime')) 

                FILENAME=strtrim(fxpar(hdr,'FILENAME'),2)
                DETECTOR=strtrim(fxpar(hdr,'DETECTOR'),2)
                CRPIX1 = fxpar(hdr,'CRPIX1')
                CRPIX2 = fxpar(hdr,'CRPIX2')
                NAXIS1 = fxpar(hdr,'NAXIS1')
                NAXIS2 = fxpar(hdr,'NAXIS2')
                CDELT1 = fxpar(hdr,'CDELT1')
                CDELT2 = fxpar(hdr,'CDELT2')
                print,'FILENAME: ',FILENAME
                print,'DETECTOR: ',DETECTOR

                ;FAKE Image Dimensions
                if DETECTOR eq 'C2' then DIMENSIONS=[12,12]
                if DETECTOR eq 'C3' then DIMENSIONS=[60,60]

                ;C2 : 11.9 arcsec per pixel
                ;C3 : 56.1 arcsec per pixel 

                Pix_size_in_Radian = (CDELT1 / 3600) * !dtor
                Pix_size_in_Rsun = tan(Pix_size_in_Radian / 2.) * 2. * 210
                
                ;LOCATION=[-CRPIX1*DIMENSIONS[0]/2/sState.xdim,-CRPIX2*DIMENSIONS[1]/2/sState.ydim]
                LOCATION=[-CRPIX1*Pix_size_in_Rsun,-CRPIX2*Pix_size_in_Rsun]
                
                DIMENSIONS=[NAXIS1*Pix_size_in_Rsun,NAXIS2*Pix_size_in_Rsun]
                               
                sState.oBackImage->SetProperty,DATA=im,DIMENSIONS=DIMENSIONS,$
                  LOCATION=LOCATION
                                             
                d_widgetsDraw,sState
                
            end
        end
        'VERT' : begin
            ones = replicate(1.0, 360) ;Create fancy colors for vertices
            Color_convert, findgen(360), ones, ones, rr, gg, bb, /HSV_RGB
            vc = transpose([[rr], [gg],[bb]])
            if sEvent.SELECT eq 0 then sState.oPolyLine->SetProperty, $
              VERT_COLORS=0 else $
              sState.oPolyLine->SetProperty, VERT_COLORS=vc
            d_widgetsDraw,sState
        end
        'SOLIDRESET' : begin
            sState.oPolyLine->SetProperty,STYLE=1, HIDDEN_LINES=0, $
              SHADING=1, VERT_COLORS=0
            sState.oEdgeModel->SetProperty,/HIDE
            d_widgetsDraw,sState
            WIDGET_CONTROL, sState.wStyleMenu, SET_DROPLIST_SELECT=1
            WIDGET_CONTROL, sState.wHideMenu, SET_VALUE=0
            WIDGET_CONTROL, sState.wShadeMenu, SET_VALUE=1
            WIDGET_CONTROL, sState.wEdgeMenu, SET_VALUE=0
            WIDGET_CONTROL, sState.wColorMenu, SET_VALUE=0
        end
        'STYLE' : begin
            sState.oPolyLine->SetProperty,STYLE=sEvent.INDEX
            d_widgetsDraw,sState
        end
        'EDGE' : begin
            if sEvent.SELECT eq 0 then sState.oEdgeModel->SetProperty,/HIDE else $
              sState.oEdgeModel->SetProperty,HIDE=0
            d_widgetsDraw,sState
        end
        'HIDDEN' : begin
            
            sState.oPolyLine->SetProperty,HIDDEN_LINES=sEvent.SELECT
            d_widgetsDraw,sState
        end
        'SHADE' : begin
             sState.oPolyLine->SetProperty,SHADING=sEvent.SELECT
             d_widgetsDraw,sState
        end
        'ZOOM' : begin
            WIDGET_CONTROL, sState.WZoom, GET_VALUE=zoom
            ;zoom=alog(zoom)+1
            sState.oRotationModel->Reset
            sState.oStaticModel->Reset
            WIDGET_CONTROL, sState.wXSlider, GET_VALUE=xDegree
            WIDGET_CONTROL, sState.wYSlider, GET_VALUE=yDegree
            WIDGET_CONTROL, sState.wZSlider, GET_VALUE=zDegree
            sState.oRotationModel->Rotate, [0,0,1], -zDegree
            sState.oRotationModel->Rotate, [1,0,0], xDegree
            sState.oRotationModel->Rotate, [0,1,0], yDegree
            sState.oStaticModel->Scale, zoom/10., zoom/10., zoom/10.
            d_widgetsDraw,sState
        end


        'RESET' : begin
            WIDGET_CONTROL, sState.wXSlider, SET_VALUE=0
            WIDGET_CONTROL, sState.wYSlider, SET_VALUE=0
            WIDGET_CONTROL, sState.wZSlider, SET_VALUE=0
            WIDGET_CONTROL, sState.WZoom, SET_VALUE=10
            sState.oRotationModel->Reset
            sState.oStaticModel->Reset
            d_widgetsDraw,sState
            
        end ;reset

        'EXPORT' : begin
            if (n_elements(WIDS) ne 0) then begin
                WIDGET_CONTROL, sState.wXSlider, GET_VALUE=xDegree
                WIDGET_CONTROL, sState.wYSlider, GET_VALUE=yDegree
                WIDGET_CONTROL, sState.wZSlider, GET_VALUE=zDegree
                WIDGET_CONTROL,WIDS.neangx,SET_VALUE=strtrim(string(yDegree),2)
                WIDGET_CONTROL,WIDS.neangy,SET_VALUE=strtrim(string(xDegree),2)
                WIDGET_CONTROL,WIDS.neangz,SET_VALUE=strtrim(string(zDegree),2)
            endif else print,'You must be running Frontend to use this feature.'
        end

        'RRT' : begin
            WIDGET_CONTROL, sState.wXSlider, GET_VALUE=yDegree
            WIDGET_CONTROL, sState.wYSlider, GET_VALUE=xDegree
            WIDGET_CONTROL, sState.wZSlider, GET_VALUE=zDegree
            ;print,xDegree,yDegree,zDegree
;            plot_rrt,xDegree,yDegree,zDegree
            plot_rrt,sEvent.top,xDegree,yDegree,zDegree
        end
        
        'GO' : begin
            if (n_elements(WIDS) ne 0) then begin
                WIDGET_CONTROL, sState.wXSlider, GET_VALUE=yDegree
                WIDGET_CONTROL, sState.wYSlider, GET_VALUE=xDegree
                WIDGET_CONTROL, sState.wZSlider, GET_VALUE=zDegree
                WIDGET_CONTROL,WIDS.neangx,SET_VALUE=strtrim(string(xDegree),2)
                WIDGET_CONTROL,WIDS.neangy,SET_VALUE=strtrim(string(yDegree),2)
                WIDGET_CONTROL,WIDS.neangz,SET_VALUE=strtrim(string(zDegree),2)
                GO_PRO
            endif else print,'You must be running Frontend to use this feature.'
        end

        'ALIGN' : begin
            if (n_elements(WIDS) ne 0) then begin
                sState.oRotationModel->Reset
                WIDGET_CONTROL, sState.wXSlider, SET_VALUE=0
                WIDGET_CONTROL, sState.wYSlider, SET_VALUE=0
                WIDGET_CONTROL, sState.wZSlider, SET_VALUE=0
                d_widgetsDraw,sState

            ;------MOD
                WIDGET_CONTROL, WIDS.neangx, GET_VALUE=xDegree
                WIDGET_CONTROL, WIDS.neangy, GET_VALUE=yDegree
                WIDGET_CONTROL, WIDS.neangz, GET_VALUE=zDegree
                
                sState.NeAngX = fix(xDegree)
                sState.NeAngY = fix(yDegree)
                sState.NeAngZ = fix(zDegree)
            ;------MOD
            ;Z, X, Y:  ;Y, then X, then Z
                if sState.NeAngY GT 180 then sState.NeAngY = sState.NeAngY - 360
                if sState.NeAngX GT 180 then sState.NeAngX = sState.NeAngX - 360
                if sState.NeAngZ GT 180 then sState.NeAngZ = sState.NeAngZ - 360
                if sState.NeAngY LT -180 then sState.NeAngY = sState.NeAngY + 360
                if sState.NeAngX LT -180 then sState.NeAngX = sState.NeAngX + 360
                if sState.NeAngZ LT -180 then sState.NeAngZ = sState.NeAngZ + 360
                
                for i=0, abs(sState.NeAngZ)-1 do begin
                    WIDGET_CONTROL, sState.wZSlider, GET_VALUE=zDegree
                    if sState.NeAngZ lt 0 then begin
                        WIDGET_CONTROL, sState.wZSlider, SET_VALUE=zDegree-1
                        sState.oRotationModel->Rotate, [0,0,1], 1
                    endif else begin
                        WIDGET_CONTROL, sState.wZSlider, SET_VALUE=zDegree+1
                        sState.oRotationModel->Rotate, [0,0,1], -1
                    end
                    d_widgetsDraw,sState
                end
                
                for i=0, abs(sState.NeAngY)-1 do begin
                    WIDGET_CONTROL, sState.wXSlider, GET_VALUE=xDegree
                    if sState.NeAngY lt 0 then begin
                        sState.oRotationModel->Rotate, [1,0,0], -1
                        WIDGET_CONTROL, sState.wXSlider, SET_VALUE=xDegree-1
                    endif else begin
                        sState.oRotationModel->Rotate, [1,0,0], 1
                        WIDGET_CONTROL, sState.wXSlider, SET_VALUE=xDegree+1
                    end
                    d_widgetsDraw,sState
                end
                
                for i=0, abs(sState.NeAngX)-1 do begin
                    WIDGET_CONTROL, sState.wYSlider, GET_VALUE=yDegree
                    if sState.NeAngX lt 0 then begin
                        sState.oRotationModel->Rotate, [0,1,0], -1
                        WIDGET_CONTROL, sState.wYSlider, SET_VALUE=yDegree-1
                    endif else begin
                        sState.oRotationModel->Rotate, [0,1,0], 1
                        WIDGET_CONTROL, sState.wYSlider, SET_VALUE=yDegree+1
                    end
                    d_widgetsDraw,sState
                end                
            endif else print,'You must be running Frontend to use this feature.'
        end ;align
            
        ;  Handle the rotation.
        'SLIDER' : begin


            WIDGET_CONTROL, sState.wXSlider, GET_VALUE=xDegree
            WIDGET_CONTROL, sState.wYSlider, GET_VALUE=yDegree
            WIDGET_CONTROL, sState.wZSlider, GET_VALUE=zDegree
            zDegree = -zDegree

            matFinal = FLTARR(3,3)
            matFinal = space3123(xDegree, yDegree, zDegree)

            sState.oRotationModel->GetProperty, TRANSFORM=t
            tempMat = FLTARR(3,3)
            tempMat[0:2, 0:2] = t[0:2, 0:2]
            
            rotMat = matFinal # tempMat

            e4 = 0.5 * SQRT(1.0 + rotMat[0,0] + $
                rotMat[1,1] + rotMat[2,2])

            if (e4 eq 0) then begin
                if (rotMat[0,0] eq 1) then begin
                    axisRot = [1, 0, 0]
                endif else if(rotMat[1,1] eq 1) then begin
                    axisRot = [0, 1, 0]
                endif else begin
                    axisRot = [0, 0, 1]
                endelse
                angleRot = 180.0
            endif else begin
                e1 = (rotMat[2,1] - rotMat[1,2])/(4.0*e4)
                e2 = (rotMat[0,2] - rotMat[2,0])/(4.0*e4)
                e3 = (rotMat[1,0] - rotMat[0,1])/(4.0*e4)
                modulusE = SQRT(e1*e1 + e2*e2 +e3*e3)
                if(modulusE eq 0.0) then begin
                    WIDGET_CONTROL, sEvent.top, $
                        SET_UVALUE=sState, /NO_COPY
                    RETURN
                endif
                axisRot = FLTARR(3)
                axisRot[0] = e1/modulusE
                axisRot[1] = e2/modulusE
                axisRot[2] = e3/modulusE
                angleRot = (2.0 * ACOS(e4)) * 180 / !DPI
            endelse


            for i = 0, 2 do begin
                if(ABS(axisRot[i]) lt 1.0e-6) then axisRot[i]=1.0e-6
            endfor
            sState.oRotationModel->Rotate, axisRot, angleRot
            d_widgetsDraw, sState


        end    ;  of SLIDER


        'DRAW': begin


            ;  Expose.
            if (sEvent.type eq 4) then begin
                d_widgetsDraw, sState
                WIDGET_CONTROL, sEvent.top, SET_UVALUE=sState, /NO_COPY
                RETURN
            endif



            ;  Handle trackball update
            bHaveTransform = sState.oTrack->Update(sEvent, TRANSFORM=qmat )
            if (bHaveTransform NE 0) then begin
                sState.oRotationModel->GetProperty, TRANSFORM=t
                mt = t # qmat
                sState.oRotationModel->SetProperty,TRANSFORM=mt
            endif


            ;  Button press.
            if (sEvent.type eq 0) then begin
                sState.btndown = 1B
                sState.drawWindowID->SetProperty, QUALITY=2
                WIDGET_CONTROL, sState.wDraw, /DRAW_MOTION
            endif    ;     of Button press


            ;  Button motion.
            if ((sEvent.type eq 2) and (sState.btndown eq 1B)) then begin
                if (bHaveTransform) then begin
                    ;  Reset the x, y, z axis angle values.
                    ;
                    sState.oRotationModel->GetProperty, TRANSFORM=transform
                    tempMat = FLTARR(3,3)
                    xyzAngles = FLTARR(3)
                    tempMat[0:2, 0:2] = transform[0:2, 0:2]
                    xyzAngles = Angle3123(invert(tempMat))
                    WIDGET_CONTROL, sState.wXSlider, SET_VALUE=xyzAngles[0]
                    WIDGET_CONTROL, sState.wYSlider, SET_VALUE=xyzAngles[1]
                    WIDGET_CONTROL, sState.wZSlider, SET_VALUE=-xyzAngles[2]
                    d_widgetsDraw, sState
                endif


            endif  

            if (sEvent.type eq 1) then begin
                if (sState.btndown EQ 1b) then begin
                    sState.drawWindowID->SetProperty, QUALITY=2
                    d_widgetsDraw, sState
                endif
                sState.btndown = 0B
                WIDGET_CONTROL, sState.wDraw, DRAW_MOTION=0
            endif
        end                 ;     of DRAW
        "ABOUT": begin

            ;  Display the information.
            ;
            base = WIDGET_BASE(TITLE="About", xsize=600, ysize=150, /COLUMN)
            label_text = WIDGET_LABEL(base, value="Frontend by Nishant Patel", /ALIGN_CENTER)
            label_text = WIDGET_LABEL(base, $
                                      value="Dragger by Nishant Patel, Dr. Russell Howard, and Arnaud Thernisien",$
                                      /ALIGN_CENTER)
            label_text = WIDGET_LABEL(base, value="Raytrace by Arnaud Thernisien", /ALIGN_CENTER)
            label_text = WIDGET_LABEL(base, $
                                      value="Models by Arnaud Thernisien, Dr. Russell Howard, Angelos Vourlidas et. al.",$
                                      /ALIGN_CENTER)
            label_text = WIDGET_LABEL(base, $
                                      value="Movie Maker by Scott Paswaters", $
                                      /ALIGN_CENTER)
            exit_butt = WIDGET_BUTTON(base, value="OK", /ALIGN_CENTER, EVENT_PRO="EXIT_PRO")
            WIDGET_CONTROL, base, /REALIZE
            XMANAGER, 'About', base
            
        end
        'XLOADCT': begin
            ;just start xloadct
            xloadct
        end

        'QUIT': begin
            print,'quitting'
            ;  Restore the info structure before destroying event.top
            ;
            WIDGET_CONTROL, sEvent.top, SET_UVALUE=sState, /NO_COPY

            ;  Destroy widget hierarchy.
            ;
            WIDGET_CONTROL, sEvent.top, /DESTROY

            RETURN
        end

        'EMBEDQUIT': begin

            WIDGET_CONTROL, sEvent.top, /DESTROY
            RETURN

        end
        

        ELSE :  begin
            PRINT, 'Case Statement found no matches'
        end


    endcase

    WIDGET_CONTROL, sEvent.top, Set_UValue=sState, /No_Copy
end               ; of d_widgetsEvent
;---------------------
PRO EXIT_PRO, ev
common share1

WIDGET_CONTROL, ev.top, /DESTROY

END
; -----------------------------------------------------------------------------
pro d_widgetsCleanup, $
    wTopBase      ; IN: top level base associated with the cleanup

    WIDGET_CONTROL, wTopBase, GET_UVALUE=sState,/No_Copy

    OBJ_DESTROY, sState.oStaticModel
    OBJ_DESTROY, sState.oContainer

    if (WIDGET_INFO(sState.groupBase, /VALID_ID)) then $
        WIDGET_CONTROL, sState.groupBase, /MAP


end  

; -----------------------------------------------------------------------------
pro dragger, x,y,z, $
    GROUP=group, $     ; IN: (opt) group identifier
    RECORD_TO_FILENAME=record_to_filename, $
    DEBUG=debug, $     ; IN: (opt)
    APPTLB = appTLB, $    ; OUT: (opt) TLB of this application
    DEMO = demo, $
    POLYGONS = polygons, $
    SOLID = solid, $
    neangx=neangx, $
    neangy=neangy, $
    neangz=neangz, $
    RADIUS=radius, $
    embed=embed, $
    nozoom=nozoom, $
    sc=sc, $
    WINDOW_SIZE=window_size ; IN: (opt) Window size of graphics window (default screenSize[0]*0.4 ^2)

if (n_elements(embed) eq 0) then embed=0
if (n_elements(nozoom) eq 0) then nozoom=0

if (n_elements(x) eq 0 OR n_elements(y) eq 0 OR n_elements(z) eq 0) AND $
  n_elements(demo) eq 0 then begin
    if (embed gt 0) then begin
        x=[0];
        y=[0];
        z=[0];
    end else begin
        print,'You must specify 3 vectors or use /DEMO [,/SOLID]'
        return
    end
end

if (n_elements(z) ne 0) then z=-z; not sure why this z=-z is here

if (embed gt 0 and n_elements(window_size) eq 0) then window_size=[150,150]

if n_elements(radius) eq 0 then radius=10

xrng=[0,radius]
yrng=[0,radius]
zrng=[0,radius]

if (n_elements(neangx) ne 0) then begin
    neangx=fix(neangx)
    neangy=fix(neangy)
    neangz=fix(neangz)
endif else begin 
    neangx=0
    neangy=0
    neangz=0
end
Device, GET_SCREEN_SIZE = screenSize
if (n_elements(window_size) eq 0) then begin
    xdim = screenSize[0]*0.4
    ydim = xdim                 ; * 0.75
endif else begin
    xdim=window_size[0]
    ydim=window_size[1]
endelse
if (embed eq 0) then print,'Screen size: ',screenSize
    ngroup = N_ELEMENTS(group)
    if (ngroup NE 0) then begin
        check = WIDGET_INFO(group, /VALID_ID)
        if (check NE 1) then begin
            print,'Error, the group identifier is not valid'
            print, 'Return to the main application'
            RETURN
        endif
        groupBase = group
    endif else groupBase = 0L

    if (n_elements(demo) eq 0) then demo = 0
    if (n_elements(polygons) eq 0) then polygons = [0]
    if (n_elements(solid) eq 0) then solid = 0  
    
    if (embed eq 0) then begin
        which,'dragger.pro',outfile=filename
        mypath = STRMID(filename,0,strlen(filename)-11)

        splashbase = WIDGET_BASE(TITLE="Welcome!", $
                                 /COLUMN, XOFFSET=screenSize[0]/2-128, YOFFSET=screenSize[1]/2-128)
                                ;label_text = WIDGET_LABEL(splashbase, value="Welcome to Dragger!", /ALIGN_CENTER)
        splash_img = WIDGET_BUTTON(splashbase, $
                          value=filepath(mypath[0]+"dragger_splash.bmp", root_dir='/'),$
                                   /bitmap)
        WIDGET_CONTROL, splashbase, /REALIZE
        XMANAGER, 'Welcome', splashbase,/JUST_REG
        wait,1
        WIDGET_CONTROL, splashbase, /DESTROY
    end

    if (embed gt 0) then begin
        wTopBase=WIDGET_BASE(embed,/COLUMN, TLB_FRAME_ATTR=1)
    end else if (N_ELEMENTS(group) EQ 0) then begin
        wTopBase = WIDGET_BASE(TITLE="Dragger", /COLUMN, $
            /TLB_KILL_REQUEST_EVENTS, $
            TLB_FRAME_ATTR=1, MBAR=barBase)
    endif else begin
        wTopBase = WIDGET_BASE(TITLE="Dragger", /COLUMN, $
            /TLB_KILL_REQUEST_EVENTS, $
            GROUP_LEADER=group, $
            TLB_FRAME_ATTR=1, MBAR=barBase)
    endelse

    if (embed eq 0) then begin
        wFileButton = WIDGET_BUTTON(barBase, VALUE= 'File', /MENU)

        wSaveButton = WIDGET_BUTTON(wFileButton, SENSITIVE=0, $
                                    VALUE='Export', UVALUE='EXPORT')

        wPrintButton = WIDGET_BUTTON(wFileButton, SENSITIVE=0, $
                                     VALUE='Print', UVALUE='PRINT')
    
        wQuitButton = WIDGET_BUTTON(wFileButton, $
                                    VALUE='Quit', UVALUE='QUIT')

        wViewButton = WIDGET_BUTTON(barBase, VALUE= 'View', /MENU)
        
        wFitsButton = WIDGET_BUTTON(wViewButton, $
                                    VALUE='Underlay FITS Image', UVALUE='FITS')

        wXloadctButton = WIDGET_BUTTON(wViewButton, $
                                       VALUE='Load Color Table', UVALUE='XLOADCT')

        wHelpButton = WIDGET_BUTTON(barBase, /HELP, $
                                    VALUE='Help', /MENU)

        wReadmeButton = WIDGET_BUTTON(wHelpButton, SENSITIVE=0, $
                                      VALUE='Readme', UVALUE='README')

        wAboutButton = WIDGET_BUTTON(wHelpButton, $
                                     VALUE='About Dragger/RaytraceWL', UVALUE='ABOUT')

    end


    wSubBase = WIDGET_BASE(wTopBase, COLUMN=3)

        wLeftBase = WIDGET_BASE(wSubBase, /BASE_ALIGN_CENTER, $
                                XPAD=5, YPAD=5, $
                                /FRAME, /COLUMN)
        
                wSliderBase = WIDGET_BASE(wLeftBase, /COLUMN, $
                    /FRAME, YPAD=8, XPAD=8)

                    xRot = 0
                    yRot = 0
                    zRot = 0
                    wSliderLabel = WIDGET_LABEL(wSliderBase, $
                        VALUE='Rotation', /ALIGN_CENTER)


                    wYSlider = WIDGET_SLIDER(wSliderBase, $
                        UVALUE='SLIDER', /DRAG, $
                        VALUE=xRot, MINIMUM=-180, MAXIMUM=180)

                    wSliderXLabel = WIDGET_LABEL(wSliderBase, $
                        VALUE='X Axis')

                    wXSlider = WIDGET_SLIDER(wSliderBase, $
                        UVALUE='SLIDER', /DRAG, $
                        VALUE=yRot, MINIMUM=-180, MAXIMUM=180)

                    wSliderYLabel = WIDGET_LABEL(wSliderBase, $
                        VALUE='Y Axis')

                    wZSlider = WIDGET_SLIDER(wSliderBase, $
                        VALUE=zRot, MINIMUM=-180, MAXIMUM=180, $
                        UVALUE='SLIDER', /DRAG)

                    wSliderZLabel = WIDGET_LABEL(wSliderBase, $
                        VALUE='Z Axis')

                    if (nozoom) then begin
                        wzoom=0
                    end else begin
                        wzoom = WIDGET_SLIDER(wSliderBase, VALUE=10, $
                                              MINIMUM=1,MAXIMUM=100, $
                                              UVALUE='ZOOM', /DRAG)

                        wZoomLabel = WIDGET_LABEL(wSliderBase, $
                                                  VALUE='Zoom x10')

                    end

                    if (embed gt 0) then begin

                        walign=0
                        wexport=0
                        wSunCheck=0

                        wmini = WIDGET_BASE(wLeftBase, /ROW, /FRAME)

                        wQuitButton = WIDGET_BUTTON(wmini, $
                                            VALUE='Quit', UVALUE='EMBEDQUIT')

                        wreset = WIDGET_BUTTON(wmini, VALUE='Reset', $
                                               UVALUE='RESET')
                        wgo = WIDGET_BUTTON(wmini, $
                                            VALUE='Go!', UVALUE='RRT')

                    end else begin

                        wreset = WIDGET_BUTTON(wLeftBase, VALUE='Reset', $
                                               UVALUE='RESET')

                        wFrontBase = WIDGET_BASE(wLeftBase, /COLUMN, $
                                                 /FRAME, YPAD =8, XPAD=8)

                        walign = WIDGET_BUTTON(wFrontBase, $
                                               VALUE='Align with Frontend',$
                                               UVALUE='ALIGN')

                        wexport = WIDGET_BUTTON(wFrontBase, $
                                                VALUE='Export to Frontend',$
                                                UVALUE='EXPORT')

                        wgo = WIDGET_BUTTON(wFrontBase, $
                                            VALUE='Go!', UVALUE='GO')

                        label_blank = WIDGET_LABEL(wLeftBase, VALUE='')

                        wMiscBase = WIDGET_BASE(wLeftBase, /COLUMN, $
                                                /FRAME, YPAD=5,XPAD=5, $
                                                /ALIGN_CENTER)

                        wSunCheck = CW_BGROUP(wMiscBase, ['Hide Sun'], /NONEXCLUSIVE, uvalue='SUNCHECK')

                    end

            ;  Create a base for the right column.
            ;
            wRightBase = WIDGET_BASE(wSubBase, /COLUMN)

            wDrawBase = WIDGET_BASE(wRightBase, /COLUMN, $
                                    /FRAME, UVALUE=-1)
            wDraw = WIDGET_DRAW(wDrawBase, $
                                XSIZE=xdim, YSIZE=ydim, /BUTTON_EVENTS, $
                                /EXPOSE_EVENTS, UVALUE='DRAW', $
                                RETAIN=0, $
                                GRAPHICS_LEVEL=2)
            
            if solid ne 0 then begin
                wRightSubBase = WIDGET_BASE(wSubBase, /COLUMN, /FRAME, XPAD=15, $
                                            YPAD=8, /BASE_ALIGN_CENTER)
                wSolidLabel = WIDGET_LABEL(wRightSubBase, $
                        VALUE='Solid', /ALIGN_CENTER)
                wSolidBase = WIDGET_BASE(wRightSubBase, /COLUMN, /FRAME, $
                                         title='Solid', XPAD=5, YPAD=5)
                wStyleMenu = WIDGET_DROPLIST(wSolidBase, value=['Points','Wire','Solid'], $
                                             title='Style: ', uvalue='STYLE')

                wHideMenu = CW_BGROUP(wSolidBase, ['Hidden Lines'], /NONEXCLUSIVE, uvalue='HIDDEN')
                wShadeMenu = CW_BGROUP(wSolidBase, ['Gauraud Shading'], /NONEXCLUSIVE, uvalue='SHADE')

                wEdgeMenu = CW_BGROUP(wSolidBase, ['Edging'], /NONEXCLUSIVE, uvalue='EDGE')

                wColorMenu = CW_BGROUP(wSolidBase, ['Vertex Colors'], /NONEXCLUSIVE, uvalue='VERT')
                wSolidReset = WIDGET_BUTTON(wRightSubBase, value='Reset', $
                                            UVALUE='SOLIDRESET', /ALIGN_CENTER) 

                WIDGET_CONTROL, wStyleMenu, SET_DROPLIST_SELECT=2
                WIDGET_CONTROL, wShadeMenu, SET_DROPLIST_SELECT=1
                WIDGET_CONTROL, wColorMenu, SET_DROPLIST_SELECT=0
                
            endif else begin
                wStyleMenu=0
                wHideMenu=0
                wShadeMenu=0
                wEdgeMenu=0
                wColorMenu=0
                wSolidReset=0
            endelse

    WIDGET_CONTROL, wTopBase, /REALIZE
    appTLB = wTopBase
    WIDGET_CONTROL, wTopBase, SENSITIVE=0
    WIDGET_CONTROL, wDraw, GET_VALUE=drawWindowID

    sqr3 = SQRT(3)/1.5   ; length of a diagonal in a cube
    radx = SQRT(2.0)       ; viewport in normal coordinates
    rady = SQRT(2.0)

    axesX = -0.5
    axesY = -0.5
    axesZ = -0.5

    xMargin = (1.0-sqr3)/2.0
    yMargin = (1.0-sqr3)/2.0
    xv = ((xMargin)*radx); + axesX)
    yv = ((yMargin)*rady); + axesY)
    width = 1.0 - 2.0 * xMargin* radx
    height = 1.0 - 2.0 * yMargin * radY
    myview = [xv, yv, width, height]

    oView = OBJ_NEW('idlgrview', COLOR=[0,0,0], zclip=[10,-10], $
                    eye=10.1)
    
    oStaticModel = OBJ_NEW('idlgrmodel')
    oMovableModel = OBJ_NEW('idlgrmodel')
    oRotationModel = OBJ_NEW('idlgrmodel')
    oScalingModel = OBJ_NEW('idlgrmodel')
    oTranslationModel = OBJ_NEW('idlgrmodel')
    
    oMovableModel->Add, oRotationModel
    oRotationModel->Add, oScalingModel
    oScalingModel->Add, oTranslationModel

    if (n_elements(sc) eq 0) then sc = 1  ;0.7
    oStaticModel->Scale, sc, sc, sc

    oLight1 = OBJ_NEW('idlgrLight', TYPE=1, INTENSITY=0.5, $
        LOCATION=[1.5, 0, 1],HIDE=0)
    oLight2 = OBJ_NEW('idlgrLight', TYPE=0, $
        INTENSITY=0.75)
    oLight3 = OBJ_NEW('idlgrLight', TYPE=1, INTENSITY=1.0, $
        LOCATION=[0, 0, 0],CONEANGLE=360)
    oStaticModel->Add, oLight1
    oStaticModel->Add, oLight2
    oStaticModel->Add, oLight3


    if demo eq 1 then begin
    ;  Generates a wire frame for model 6 - simple streamer belt

        nr = 9
        nth = 100
        nd = 7
        d0 = 0.3
        dr = (10.-1.)/nr
        nd = 1
        r = dr*findgen(nr)+1
        delth = 360./(nth-1)
        th = findgen(nth)*delth*!dtor
        x = fltarr(nth,nd,nr)
        y = x
        z = x
        f = x+1
        d = (FINDGEN(nd)-nd/2)*0.1
        FOR k=0,nth-1 DO BEGIN
            FOR i=0,nr-1 DO BEGIN
                FOR j=0,nd-1 DO BEGIN
                    phi0 = !pi/2+asin(0.5*cos(2*th(k)))
                    phi = phi0  ;asin(d(j)/r(i))+phi0
                    u = r(i)*cos(th(k))*sin(phi)
                    v = r(i)*sin(th(k))*sin(phi)
                    w = r(i)*cos(phi)
                    x(k,j,i) = u
                    y(k,j,i) = v
                    z(k,j,i) = w
                ENDFOR
            ENDFOR
        ENDFOR
        x = REFORM(x,nth*nr*nd)
        y = REFORM(y,nth*nr*nd)
        z = REFORM(z,nth*nr*nd)

    ;-----------------------------------------
    ;SOLID BEGIN
    if solid eq 1 then begin
        which,'dragger.pro',outfile=x
        mypath = STRMID(x,0,strlen(x)-11)
        filename = mypath+'knot.dat'
        COMPILE_OPT idl2, hidden
        oPolyLine = d_objReadNoff(filename, xr, yr, zr)
        oPolyLine->GetProperty, POLY=pmesh
        p1 = OBJ_NEW('IDLgrPolyline', SHARE_DATA=oPolyLine, $
                     POLY=pmesh, COLOR=[255,0,0])
     endif else begin
        oPolyline = obj_new('IDLgrPolyline',  $
                            x, $
                            y, $
                            z, $
                            _extra=extra, $
                            name=name, $
                            thick=2, $
                            color=[255, 255, 0] $
                           )
    end

endif else if solid eq 1 then begin
    if polygons eq [0] then oPolyline = obj_new('IDLgrPolygon', $
                                                x, y, z, color=[200,200,200], $
                                                shading=1, style=0) $
    else begin
        oPolyline = obj_new('IDLgrPolygon', x, y, z, shading=1, $
                            POLYGONS=polygons, color=[200,200,200])
        
    endelse
    oPolyLine->GetProperty, POLY=pmesh

    p1 = OBJ_NEW('IDLgrPolyline', SHARE_DATA=oPolyLine, $
                 POLY=pmesh, COLOR=[255,0,0])
endif else begin
    oPolyline = obj_new('IDLgrPolyline',  $
    x, $
    y, $
    z, $
    _extra=extra, $
    name=name, $
    thick=2, $
    color=[255, 255, 0] $
    )

end

;FIGURE OUT DATA RANGES
    dummy = -1
    
    if n_elements(xrng) eq 0 then $
      xrange = float([min(x), max(x)]) $
    else $
      xrange = float(xrng)
    
    if n_elements(yrng) eq 0 then $
      yrange = float([min(y), max(y)]) $
    else $
      yrange = float(yrng)
    
    if n_elements(zrng) eq 0 then $
      zrange = float([min(z), max(z)]) $
    else $
      zrange = float(zrng)
    
    if xrange[0] ge xrange[1] then begin
        xrange[1] = xrange[0] + 1
    end
    
    if yrange[0] ge yrange[1] then begin
        yrange[1] = yrange[0] + 1
    end
    
    if zrange[0] ge zrange[1] then begin
        zrange[1] = zrange[0] + 1
    end
    
    xs = norm_coord(xrange)
    ys = norm_coord(yrange)
    zs = norm_coord(zrange)
    
    if ((demo ne 1) OR (solid ne 1)) then begin
        oPolyline->SetProperty,xcoord_conv=xs, ycoord_conv=ys, zcoord_conv=zs
        if solid eq 1 then p1->SetProperty,xcoord_conv=xs, $
          ycoord_conv=ys, zcoord_conv=zs, thick=1
    end
    oTranslationModel->Add, oPolyline
     

    if solid eq 1 then begin
        oEdgeModel = OBJ_NEW('IDLgrModel', /HIDE)
        oEdgeModel->Add, p1 
        oEdgeModel->scale, 1.005, 1.005, 1.005   ;Offset Z to make visible
        oTranslationModel->Add, oEdgeModel
        ;oEdgeModel->SetProperty, /HIDE
    endif else oEdgeModel=0

    oView->Add, oStaticModel
    oStaticModel->Add, oMovableModel, position=1

    ;BG IMAGE
    ;img = alog10(readfits('~/test17.fts')>1e-13)
    ;img = img-min(img)
    ;img = img*50
    
    oBackImage = OBJ_NEW('IDLgrImage',$
                         fltarr(1024,1024), DIMENSIONS=[30,30], $
                         GREYSCALE=0, LOCATION=[-15.15,-15.65],xcoord_conv=xs, $
                         ycoord_conv=ys, zcoord_conv=zs)
    
    oStaticModel->Add, oBackImage, position=0
    ;END BGIMAGE
    
    oTrack = OBJ_NEW('Trackball', [xdim/2.0, ydim/2.0], xdim/2.0)

    oContainer = OBJ_NEW('IDLgrContainer')
    oContainer->Add, oView
    oContainer->Add, oTrack

    xrange=[-radius,radius]
    yrange=[-radius,radius]
    zrange=[-radius,radius]

    oXaxis = obj_new('IDLgrAxis', $
        0, $
        ticklen=0.1, $
        minor=0, $
        ;notext=1, $
        /exact, $
        color=[255,255,255], $
        range=[-5,5] $
        )
    oXaxis->GetProperty, ticktext=oXaxisText
    oXaxisText->SetProperty, font=oHelvetica10pt

    oXaxis2 = obj_new('IDLgrAxis', 0, minor=0, notext=1, /exact, ticklen=.1)
    oXaxis3 = obj_new('IDLgrAxis', 0, minor=0, notext=1, /exact, ticklen=0)

    oXaxis->SetProperty, range=xrange,  location=[dummy, 0, 0], $
      xcoord_conv=xs
    oXaxis2->SetProperty, range=xrange, location=[dummy, 1, 0], $
      xcoord_conv=xs, color=[255,255,255]
    oXaxis3->SetProperty, range=xrange, location=[dummy, 1, 1], $
      xcoord_conv=xs, color=[255,255,255]

    oYaxis = obj_new('IDLgrAxis', $
        1, $
        ticklen=0.1, $
        minor=0, $
        ;notext=1, $
        /exact, $
        color=[255,255,255], $
        range=[-5,5] $
        )

    oYaxis->GetProperty, ticktext=oYaxisText
    oYaxisText->SetProperty, font=oHelvetica10pt

    oYaxis2 = obj_new('IDLgrAxis', 1, minor=0, notext=1, /exact, ticklen=.1)
    oYaxis3 = obj_new('IDLgrAxis', 1, minor=0, notext=1, /exact, ticklen=0)

    oYaxis->SetProperty, range=yrange,  location=[0, dummy, 0], $
      ycoord_conv=ys
    oYaxis2->SetProperty, range=yrange, location=[1, dummy, 0], $
      ycoord_conv=ys, color=[255,255,255]
    oYaxis3->SetProperty, range=yrange, location=[1, dummy, 1], $
      ycoord_conv=ys, color=[255,255,255]

    oZaxis = obj_new('IDLgrAxis', $
        2, $
        ticklen=0.1, $
        minor=0, $
        ;notext=1, $
        /exact, $
        color=[255,255,255], $
        range=[5,-5], $
        textupdir=[0,1,0] $
        )

    oZaxis->GetProperty, ticktext=oZaxisText
    oZaxisText->SetProperty, font=oHelvetica10pt

    oZaxis2 = obj_new('IDLgrAxis', 2, minor=0, notext=1, /exact, ticklen=.1, tickdir=1)
    oZaxis3 = obj_new('IDLgrAxis', 2, minor=0, notext=1, /exact, ticklen=.1, tickdir=1)

    oZaxis->SetProperty, range=zrange,  location=[0, 1, dummy], $
      zcoord_conv=-zs
    oZaxis2->SetProperty, range=zrange, location=[1, 0, dummy], $
      zcoord_conv=-zs, color=[255,255,255]
    oZaxis3->SetProperty, range=zrange, location=[1, 1, dummy], $
      zcoord_conv=-zs, color=[255,255,255]

    oAxes = [ $
        oXaxis, $
        oXaxis2, $
        ;oXaxis3, $

        oYaxis, $
        oYaxis2, $
        ;oYaxis3, $

        oZaxis, $
        oZaxis2 $
        ;oZaxis3 $
        
        ;oGridx, $
        ;oGridy, $
        ;oGridz $
        ]


    oRotationModel->Add, oAxes ;ADD AXES HERE


    sphere = Obj_New('Orb', color=[255,90,0])
    sphere->Scale, 1., 1., 1.
    sphere->SetProperty,xcoord_conv=xs, ycoord_conv=ys, zcoord_conv=zs
    oSymbol = OBJ_NEW('IDLgrSymbol', sphere)
    oSun = OBJ_NEW('IDLgrPolyline', [0.,0],[0.,0],[0.,0], symbol=oSymbol)

    oStaticModel->Add, oSun


    ;  Create the info structure
    ;
    sState={ $

        OSun: oSun, $                           ; Sun Orb Object
        BtnDown: 0, $                           ; mouse button down flag
        OTrack: oTrack, $                       ; Trackball object
        OContainer: oContainer, $               ; Container object
        OView: oView, $                         ; View object
        OStaticModel: oStaticModel, $           ; Models objects
        ORotationModel: oRotationModel, $
        OPolyline: oPolyline, $                 ; Line object
        OEdgeModel: oEdgeModel, $               ; Edges (Polyline)
        OBackImage: oBackImage, $               ; BG Image
        WXSlider: wXSlider, $                   ; Widget sliders ID
        WYSlider: wYSlider, $
        WZSlider: wZSlider, $
        WReset: wreset, $                       ; Reset button ID
        WSunCheck: wSunCheck, $
        WSolidReset: wSolidReset, $
        WZoom: wzoom, $                         ; Zoom button
        WAlign: walign, $                       ; Align button ID
        WExport: wexport, $                     ; Export angles to Frontend
        WGo: wgo, $
        WStyleMenu: wStyleMenu, $               ; pick style of solid (wire, pts, solid)
        WHideMenu: wHideMenu, $                 ; Hidden lines menu  
        WShadeMenu: wShadeMenu, $               ; solid shading menu
        WEdgeMenu: wEdgeMenu, $                 ; edging menu
        WColorMenu: wColorMenu, $               ; Vertex_color menu
        ;Solid: solid, $
        NeAngX: neangx, $                       ; Frontend Ne parameters
        NeAngY: neangy, $
        NeAngZ: neangz, $
        DrawWindowID: drawWindowID, $           ; Window ID
        WDraw: wDraw, $                         ; Widget draw ID
        debug: keyword_set(debug), $            ; debug flag
        Xs: xs, $
        Ys: ys, $
        Zs: zs, $
        Radius: radius, $
        Xdim: xdim, $
        Ydim: ydim, $
        groupBase: groupBase $                 ; Base of Group Leader
    }


    d_widgetsDraw, sState

    ;  Register the info structure in the user value of the top-level base
    if (embed gt 0) then begin
        ; state var has to be in the topmost base, so
;        print,'embedding into ',embed
        owner = WIDGET_INFO(embed,/PARENT)
        WIDGET_CONTROL, owner, SET_UVALUE=sState, /NO_COPY
    end else begin
        WIDGET_CONTROL, wTopBase, SET_UVALUE=sState, /NO_COPY
    end

    WIDGET_CONTROL, wTopBase, SENSITIVE=1

    ;  Map the top level base.
    WIDGET_CONTROL, wTopBase, MAP=1

;    print,'xmanager needs ',wTopBase

    if (embed gt 0) then begin

        embed=long(wTopBase)          ; return for event handler

    end else begin

        XMANAGER, "dragger", wTopBase, $
          /NO_BLOCK, $
          EVENT_HANDLER="d_widgetsEvent", CLEANUP="d_widgetsCleanup"

    end

end   ;  main procedure


