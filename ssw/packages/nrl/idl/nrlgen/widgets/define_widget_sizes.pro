function define_widget_sizes
;+
;
;$Id: define_widget_sizes.pro,v 1.1 2009/10/21 18:55:08 nathan Exp $
;
; Project     : SECCHI
;                   
; Name        : DEFINE_WIDGET_SIZES
;               
; Purpose     : Define sizes of various widget features
;
; Inputs:   None
;
; Outputs:  Structure of various lengths in pixels
;               
; Explanation : The various sizes of window features were measures or approximated.
;               
; Use         : wsizes=define_widget_sizes() 
;
; Side effects: None.
;               
; Category    : Image Display Widget Utility
;               
; Written     : Nathan Rich, NRL 2009
;               
;-            
;$Log: define_widget_sizes.pro,v $
;Revision 1.1  2009/10/21 18:55:08  nathan
;from scc_movie_win.pro
;

wsizes = { $
    	    dsktop:22, $
	    scrbar:36, $ 
    	    border:8, $ 
    	    winhdr:22, $ 
    	    onerow:44}
	    
return,wsizes

end
