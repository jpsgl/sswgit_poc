pro wplotfits, im0, rows=rows,columns=columns, title=title0
; $Id: wplotfits.pro,v 1.2 2016/04/26 18:46:39 nathan Exp $
;
; Project   : WISPR
;                   
; Name      : wplotfits
;               
; Purpose   : display image along with one or more plots from image
;               
; Explanation:  Use IDL graphics to plot image on top and plot on bottom of window
;		This can than be saved or printed by user.
;               
; Use       : IDL> plotfits, im, rows=[1,5]	; plots rows 1 and 5 of im
;    
; Inputs    : im0	2d array or FITS filename
;
; Optional Inputs: 
;               
; Outputs   : 
;
; Keywords  :	ROWS=[...]	; rows of im to plot
;		COLUMNS=[...]	; columns of im to plot
;		-- Max of 7 columns and rows
;
;		TITLE='string above image' If input is FITS file, defaults to filename.
;
; Common    : 
;               
; Restrictions:
;               
; Side effects: 
;               
; Category    : display, image processing
;               
; Prev. Hist. : None.
;
; Written     : N.Rich, 
;               
; $Log: wplotfits.pro,v $
; Revision 1.2  2016/04/26 18:46:39  nathan
; fix things
;
; Revision 1.1  2016/04/20 22:44:49  nathan
; tool for generating figures for reports
;

plotms=plot(indgen(4),/buffer)	; get dummy plot method
colors=['b','g','r','c','m','y']
IF datatype(im0) EQ 'STR' THEN mreadfits,im0[0],h,im ELSE im=im0

s=size(im)

IF keyword_SET(title) THEN title=title0 ELSE BEGIN
	title=''
	IF datatype(h) NE 'UND' THEN title=h.filename
ENDELSE
title=title + ' ('+trim(s[1])+'x'+trim(s[2])+')'

ims=image(hist_equal(im),layout=[1,2,1],margin=[.01,.01,.01,.1],dimen=[600,900],title=title, buffer=0)

colndx=0
isrows=0

IF keyword_set(ROWS) THEN BEGIN
	isrows=1
	n=n_elements(rows)
	p=plot(im[*,rows[0]],'k', NAME='Row '+trim(rows[0]), layout=[1,2,2],/current)
	plotms=[plotms,p]
	IF n GT 1 THEN FOR i=1,n-1 DO BEGIN
		p=plot(im[*,rows[i]], colors[colndx], NAME='Row '+trim(rows[i]), /overplot)
		colndx=colndx+1
		plotms=[plotms,p]
	ENDFOR
ENDIF
IF keyword_set(COLUMNS) THEN BEGIN
	n=n_elements(columns)
	IF (isrows) THEN BEGIN
		p=plot(im[columns[0],*], colors[colndx], NAME='Col '+trim(columns[0]), /overplot)
		colndx=colndx+1
	ENDIF ELSE $
	p=plot(im[columns[0],*],'k', NAME='Col '+trim(columns[0]), layout=[1,2,2],/current)
	plotms=[plotms,p]
	
	IF n GT 1 THEN FOR i=1,n-1 DO BEGIN
		p=plot(im[columns[i],*], colors[colndx], NAME='Col '+trim(columns[i]), /overplot)
		colndx=colndx+1
		plotms=[plotms,p]
	ENDFOR
ENDIF


n=n_elements(plotms)
leg=legend(target=plotms[1:n-1],position=[0.9,0.5], /auto_text_color)

end
	
	
	
