pro rebin_huw,v,x,u,av,st,mn,mx,missing0=missing0,bincount=bincount,interpol=interpol 
 
if n_elements(missing0) eq 0 then missing=!values.f_nan $ 
else missing=missing0[0] 

if keyword_set(interpol) then missing=!values.f_nan
 
n=n_elements(u) 
av=fltarr(n) 
st=fltarr(n) 
mn=fltarr(n) 
mx=fltarr(n) 
 
mnn=min(u,max=mxx,/nan) 
bincount=histogram(x,nbin=n,min=mnn,max=mxx,reverse_=ri,/nan) 
indok=where(bincount gt 1,cntok,comp=indnotok,ncomp=cntnotok) 
 
if cntnotok ne 0 then begin 
  av[indnotok]=missing 
  st[indnotok]=missing 
  mn[indnotok]=missing 
  mx[indnotok]=missing 
endif 
 
if cntok eq 0 then return 
 
bincount=float(bincount) 
ind=ri[0:n]-n-1 
out=ri[n+1:*] 
for ii=0,cntok-1 do begin 
  i=indok[ii] 
  arr=v[out[ind[i]:ind[i+1]-1]] 
  av[i]=total(arr,/nan)/bincount[i] 
  st[i]=total((arr-av[i])^2,/nan)/bincount[i] 
  mn[i]=min(arr,max=mxn,/nan) 
  mx[i]=mxn 
endfor 
st=sqrt(st) 
 
bincount=long(bincount) 

if keyword_set(interpol) and cntnotok gt 0 then begin
	av[indnotok]=interpol(av[indok],u[indok],u[indnotok]) 
	st[indnotok]=interpol(st[indok],u[indok],u[indnotok]) 
	mn[indnotok]=interpol(mn[indok],u[indok],u[indnotok]) 
	mx[indnotok]=interpol(mx[indok],u[indok],u[indnotok])
endif 
 
end 
