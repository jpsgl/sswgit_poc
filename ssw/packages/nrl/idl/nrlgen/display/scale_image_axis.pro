;+
;
;$Id: scale_image_axis.pro,v 1.1 2010/10/27 16:43:51 mcnutt Exp $
;
; Project     : SECCHI and SOHO - LASCO/EIT (dual-use)
;                   
; Name        : scale_image_axis
;               
; Purpose     : to scale/crop an image to given scale and range
;               
; Explanation : This tool will enlarge or crop an images x and y axis and convert a linear axis to log if the keyword is set.
;               
; Use         :
;
; Calls       :  newimg=scale_image_axis(im,x,y,xrange=[0,180],yrange=[0.5,88],outsize=[512,512],/ylog,/xlog)
;
; Inputs      : im = image array to be modified
;   	        x - an n_element array (if not given, [1,2,...,n] is used)
;   	    	y - an m-element array (if not given, [1,2,...,m] is used)
;
; Output      : axis scaled image array
;
; Keywords    : xrange = [y1,y2] of output image  (if not given, min/max of x is used)
;               yrange = [x1,x2] of output image  (if not given, min/max of y is used)
;               xlog sets the x-axis to log scale
;               ylog sets the y-axis to log scale
;               outsize array containing the output image size in pixels, default to input size
; 
; outputs
;
; Comments    : image scaling came from pg_plotimage written by  Paolo Grigis, SAO
;               
; Side effects: None.
;               
; Category    : Image Display.  
;               
; Written     : Lynn Simpson, NRL Oct. 25 2010.
;               

function scale_image_axis,im,x,y,xrange=xrange,yrange=yrange,xlog=xlog,ylog=ylog,outsize=outsize

  if keyword_set(xlog) then xlog=1 else xlog=0
  if keyword_set(ylog) then ylog=1 else ylog=0

  nx=n_elements(x)
  ny=n_elements(y)

  s=size(im)
  IF s[0] NE 2 THEN return,-1
  IF nx EQ 0 THEN x=findgen(s[1])+1
  IF ny EQ 0 THEN y=findgen(s[2])+1
  
  IF n_elements(xrange) NE 2 THEN xrange=[min(x),max(x)]
  IF n_elements(yrange) NE 2 THEN yrange=[min(y),max(y)] 

  if keyword_set(outsize) then begin
    dxpix=outsize(0)
    if n_Elements(outsize) eq 2 then dypix=outsize(1) else dypix=outsize
  endif else begin
    dxpix=n_Elements(x)
    dypix=n_Elements(y)
  endelse
  
  newimage=bytarr(dxpix,dypix) & newimage=newimage*(im(0)*byte(0))

  ;compute coordinates in data space for the all the *pixels* of the image to be displayed
  ;in both linear and log scale
  IF xlog EQ 1 THEN nxpix=10^(alog10(xrange(0))+findgen(dxpix)/(dxpix-1.)*(alog10(xrange(1))-alog10(xrange(0)))) ELSE nxpix=xrange(0)+findgen(dxpix)/(dxpix-1.)*(xrange(1)-xrange(0))
  IF ylog EQ 1 THEN nypix=10^(alog10(yrange(0))+findgen(dypix)/(dypix-1.)*(alog10(yrange(1))-alog10(yrange(0)))) ELSE nypix=yrange(0)+findgen(dypix)/(dypix-1.)*(yrange(1)-yrange(0))

  neededcoordindx=where(nxpix GE min(x) AND nxpix LE max(x),countx)
  neededcoordindy=where(nypix GE min(y) AND nypix LE max(y),county)

  ;original image pixels only in xrange and yrange
  neededindx=where(x GE xrange[0] AND x LE xrange[1],countx2)
  neededindy=where(y GE yrange[0] AND y LE yrange[1],county2)

  IF (countx EQ 0) OR (county EQ 0) THEN RETURN,-1
  IF (countx2 EQ 0) OR (county2 EQ 0) THEN RETURN,-1

  ;interpolates the coordinates to get back to index space for the new image
  xint=interpol(findgen(countx2),x[neededindx],nxpix[neededcoordindx])
  yint=interpol(findgen(county2),y[neededindy],nypix[neededcoordindy])

  ;this makes blocky pixels if interpolation is not wanted
  IF NOT keyword_set(smooth) THEN BEGIN 
     xint=round(xint)
     yint=round(yint)
  ENDIF

  ;interpolate the original image to get the new image to display
  ;the new image size is given by the pixel in the plot area
  newimage[min(neededcoordindx):max(neededcoordindx),min(neededcoordindy):max(neededcoordindy)]=interpolate(im[min(neededindx):max(neededindx),min(neededindy):max(neededindy)],xint,yint,/grid) 


return,newimage
end
