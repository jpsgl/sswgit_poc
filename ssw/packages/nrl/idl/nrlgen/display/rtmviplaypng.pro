;+
; $Id: rtmviplaypng.pro,v 1.1 2008/02/15 20:54:39 nathan Exp $
;
; Project     : SOHO - LASCO/EIT, STEREO - SECCHI
;
; Name        : RTMVIPLAYPNG
;
; Purpose     : Widget tool to display animation sequence from png files.
;
; Explanation : This tool allows the user to view a series of png images as
;               an animation sequence.  The user can control the direction,
;               speed, and number of frames with widget controls.  A directory
;		is monitored for new images and they are added to the movie
;		in time order.
;
; Use         : IDL> RTMVIPLAYPNG, match, LENGTH=length, PNG_DIR=png_dir, IMG_REBIN=img_rebin, 
; 			RUNNING_DIFF=running_diff
;
; Inputs      : match : string to use to match png files to load.
;	           ex : RTMVIPLAYPNG, 'c1_fexiv'
;	           ex : RTMVIPLAYPNG, 'c2'
;	           ex : RTMVIPLAYPNG, 'c3'
;	           ex : RTMVIPLAYPNG, 'eit_195'
;	           ex : RTMVIPLAYPNG, 'eit_304'
;	           ex : RTMVIPLAYPNG, 'eit_171'
;	           ex : RTMVIPLAYPNG, 'eit_284'
;
; Outputs     : None.
;
; Keywords    : 
;               LENGTH=length     : maximum number of frames to load (default is all).
;               PNG_DIR=png_dir   : directory to read png images from (default is $MVIS/rtmovie/pngs/ 
;                                   or current directory if $MVIS doesn't exist)
;               IMG_REBIN=img_rebin  : set to dimensions to rebin (or congrid) images to
;				     : ex. IMG_REBIN=[512,512]
;               /RUNNING_DIFF     : set this flag for a running difference movie
;
; Calls       :
;
; Side effects: None.
;
; Category    : Image Display.  Animation.
;
; Written     : Scott Paswaters, NRL Apr. 8 1998.
;
; MODIFIED:
; $Log: rtmviplaypng.pro,v $
; Revision 1.1  2008/02/15 20:54:39  nathan
; moved from lasco/movie because it is called by scc_pngplay.pro
;
;	A. Vourlidas, 11/9/01 - Put WRUNMOVIEM_RT button in widget
;	N. Rich, 11/14/01 - CD to olddir if calling wrunmoviem_rt
;	jake 030430 - created RTMVIPLAYPNG from RTMVIPLAY
;				changed all GIF to PNG
;	jake 030509 - idl53 needs pngs rotated
;       Karl Battams 06/27/2007-- add code to allow a filelist to be passed instead of just a directory to search
;                        (Search for two "KB Edit" areas to see changes)
;       Karl Battams 10/02/07 -- I have put some pretty ugly hacks in here so that this routine will work with some of
;                       the SECCHI anaglyph PNG files. It's ugly code but it's hidden behind keyword_set() calls so
;                       does not interfere with anything else... (or so I hope...)
;
;
; See Also    : MKMOVIE.PRO
;
;       %W%, %H% - NRL LASCO IDL LIBRARY
;-

PRO RTMVIPLAYPNG_DRAW, ev

	COMMON RTWIN_INDEX, base, pnglist

	WIDGET_CONTROL, base, GET_UVALUE=moviev   ; get structure from UVALUE

	IF (ev.press GT 0) THEN RETURN 		;** only look at PRESS events
	IF (moviev.showmenu EQ 0) THEN BEGIN
		WIDGET_CONTROL, moviev.base, MAP=1
		moviev.showmenu = 1
	ENDIF ELSE BEGIN
		WIDGET_CONTROL, moviev.base, MAP=0
		moviev.showmenu = 0
	ENDELSE

	WIDGET_CONTROL, base, SET_UVALUE=moviev

END



;------------------ runmovie event -------------------------------
PRO RTMVIPLAYPNG_EVENT, ev

	COMMON RTWIN_INDEX, base, pnglist

	WIDGET_CONTROL, ev.top, GET_UVALUE=moviev   ; get structure from UVALUE

	IF (ev.id EQ moviev.cname) THEN BEGIN   ; Timer to look for new PNG images
		newpnglist = FINDFILE(moviev.match_png)
		new = ''
		FOR f=0, N_ELEMENTS(newpnglist)-1 DO BEGIN
			ind = WHERE(pnglist EQ newpnglist(f))
			IF (ind(0) EQ -1) THEN BEGIN
				IF (new(0) EQ '') THEN new = newpnglist(f) ELSE new = [new, newpnglist(f)]
			ENDIF
		ENDFOR

		IF (new(0) NE '') THEN BEGIN
			WIDGET_CONTROL, moviev.base, TIMER=10000.   ; Put timer on hold
			FOR f=0, N_ELEMENTS(new)-1 DO BEGIN
				loop = 0
				CATCH, error_status
				IF (error_status NE 0) THEN BEGIN
					loop = loop + 1
					PRINT, 'Error ', loop
					IF (loop GE 3) THEN BEGIN
						PRINT, 'Continuing'
						GOTO, CONT
					ENDIF
				ENDIF

				image=READ_PNG( new(f), r, g, b)
				if ( !version.release EQ '5.3' ) then image=rotate(image, 7)
				pnglist = [pnglist, new(f)]
				sz=SIZE(image)
				nx = sz(1)
				ny = sz(2)
				IF (moviev.rnx NE nx) THEN BEGIN
					IF (moviev.use_rebin EQ 1) THEN $
						image = REBIN(image, moviev.rnx, moviev.rny) $
					ELSE $
						image = CONGRID(image, moviev.rnx, moviev.rny, /INTERP)
				ENDIF 
				hdr = moviev.ahdr
				hdr.filename=new(f)
				hdr.detector=STRMID(new(f),14,3)
				REMCHAR, hdr.detector, '_'
				hdr.time_obs=STRMID(new(f),9,2)+':'+STRMID(new(f),11,2)
				hdr.date_obs=STRMID(new(f),0,4)+'/'+STRMID(new(f),4,2)+'/'+STRMID(new(f),6,2)
				tai = UTC2TAI(STR2UTC(hdr.date_obs + ' ' + hdr.time_obs))

				loc = WHERE(tai GT moviev.img_tai)
				IF (loc(0) NE -1) THEN BEGIN  ;** this image is before all current images - don't include
					FOR ff=0, N_ELEMENTS(loc)-2 DO BEGIN       ;** shift all images down 1 pixmap
						moviev.img_tai(ff) = moviev.img_tai(ff+1)
						moviev.img_hdrs(ff) = moviev.img_hdrs(ff+1)
						WSET, moviev.win_index(ff)
						DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(ff+1)]
					ENDFOR
					moviev.img_tai(ff) = tai
					moviev.img_hdrs(ff) = hdr
					WSET, moviev.win_index(ff)
					IF (moviev.running_diff EQ 1) AND (ff GE 1) THEN BEGIN
						prev=READ_PNG( moviev.img_hdrs(ff-1).filename )
						if ( !version.release EQ '5.3' ) then prev=rotate(prev, 7)
						sz=SIZE(prev)
						nx = sz(1)
						ny = sz(2)
						IF (moviev.rnx NE nx) THEN BEGIN
							IF (moviev.use_rebin EQ 1) THEN $
								prev = REBIN(prev, moviev.rnx, moviev.rny) $
							ELSE $
								prev = CONGRID(prev, moviev.rnx, moviev.rny, /INTERP)
						ENDIF 
						orig_image = image
						image = BYTSCL(FIX(image)-prev, -30,30)
					ENDIF

					TV, image
					IF (moviev.times EQ 1) THEN $
						XYOUTS, 10, 10, hdr.date_obs + ' ' + STRMID(hdr.time_obs,0,5),/DEVICE
					PRINT, '%%RTMVIPLAYPNG: Adding frame '+hdr.filename+' '+hdr.date_obs+' '+$
					hdr.time_obs

					;** if doing a running_diff and this isn't the last frame 
					;** we need to update the next frame also
					IF (moviev.running_diff EQ 1) AND (ff NE moviev.len-1) THEN BEGIN
						image = READ_PNG( moviev.img_hdrs(ff+1).filename )
						if ( !version.release EQ '5.3' ) then image=rotate(image, 7)
						sz=SIZE(image)
						nx = sz(1)
						ny = sz(2)
						IF (moviev.rnx NE nx) THEN BEGIN
							IF (moviev.use_rebin EQ 1) THEN $
								image = REBIN(image, moviev.rnx, moviev.rny) $
							ELSE $
								image = CONGRID(image, moviev.rnx, moviev.rny, /INTERP)
						ENDIF 
						hdr = moviev.img_hdrs(ff+1)
						image = BYTSCL(FIX(image)-orig_image, -30,30)
						WSET, moviev.win_index(ff+1)
						TV, image
						IF (moviev.times EQ 1) THEN $
							XYOUTS, 10, 10, hdr.date_obs + ' ' + STRMID(hdr.time_obs,0,5),/DEVICE
						PRINT, '%%RTMVIPLAYPNG: Updating frame '+hdr.detector+' '+hdr.date_obs+' '+$
						hdr.time_obs+' '+hdr.filename
					ENDIF
				ENDIF
CONT:
			ENDFOR

		ENDIF
		WIDGET_CONTROL, ev.top, SET_UVALUE=moviev
		WIDGET_CONTROL, moviev.base, TIMER=moviev.stall   	; Request another timer event
		WIDGET_CONTROL, moviev.cname, TIMER=30		;** timer to monitor .mvi file for changes

	ENDIF ELSE $

		IF (ev.id EQ ev.handler) THEN BEGIN   ; A top level base timer event
 
			IF (moviev.forward) THEN BEGIN		;** going forward
				IF ((moviev.bounce) AND (moviev.current EQ moviev.last)) THEN BEGIN
					moviev.current = moviev.last-1
					moviev.pause = 1
					moviev.forward = 0		;** set direction to reverse
				ENDIF ELSE BEGIN
					moviev.current = (moviev.current + moviev.nskip + 1) MOD moviev.len
					IF ((moviev.current EQ 0) AND (moviev.bounce EQ 0)) THEN moviev.pause = 1
					IF ((moviev.current GT moviev.last) OR (moviev.current LT moviev.first)) THEN BEGIN
						moviev.pause = 1	
						moviev.current = moviev.first
					ENDIF
				ENDELSE
			ENDIF ELSE BEGIN			;** going in reverse
				IF ((moviev.bounce) AND (moviev.current EQ moviev.first)) THEN BEGIN
					moviev.current = moviev.first+1
					moviev.pause = 1
					moviev.forward = 1		;** set direction to forward
				ENDIF ELSE BEGIN
					moviev.current = (moviev.current - 1) MOD moviev.len
					IF ((moviev.current EQ moviev.last) AND (moviev.bounce EQ 0)) THEN moviev.pause = 1
					IF ((moviev.current LT moviev.first) OR (moviev.current GT moviev.last)) THEN BEGIN
						moviev.current = moviev.last
						moviev.pause = 1
					ENDIF
				ENDELSE
			ENDELSE
	
			IF (moviev.pause AND moviev.dopause) THEN BEGIN
				moviev.pause = 0
				WAIT, .75
			ENDIF
	
			;** out of range error checks (to stop bombs from COPY)
			IF (moviev.current LT 0) THEN moviev.current = moviev.first
			IF (moviev.current GE moviev.len) THEN moviev.current = moviev.last
			IF (NOT(moviev.deleted(moviev.current)) AND ((moviev.current MOD (moviev.nskip+1)) EQ 0) ) THEN BEGIN		;** if not deleted then display
				WSET, moviev.draw_win
				DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
				WIDGET_CONTROL, moviev.cframe, SET_VALUE=STRCOMPRESS(moviev.current, /REMOVE_ALL)
				WIDGET_CONTROL, moviev.cname, SET_VALUE=STRCOMPRESS(moviev.frames(moviev.current), /REMOVE_ALL)
				WIDGET_CONTROL, moviev.sliden, SET_VALUE=moviev.current
				WIDGET_CONTROL, moviev.base, TIMER=moviev.stall   ; Request another timer event
			ENDIF ELSE $	;** else immediately look for the next non-deleted frame
				WIDGET_CONTROL, moviev.base, TIMER=0.0 ;.000001
	
		ENDIF ELSE BEGIN	;** event other than timer
 
			IF (TAG_EXIST(ev, 'value')) THEN BEGIN
				IF (DATATYPE(ev.value) EQ 'STR') THEN BEGIN
					CASE (ev.value) OF
						'Postscript' : BEGIN 	;save ps image
							CD, moviev.olddir
							BREAK_FILE, moviev.filename, a, dir, name, ext
							filename = name + '.ps'
							filename = PICKFILE(file=filename, filter='*.ps', $
								TITLE='Enter name of postscript file to save', $
								PATH=moviev.filepath,GET_PATH=path)
							IF (filename EQ '') THEN RETURN
							BREAK_FILE, filename, a, dir, name, ext
							IF (dir EQ '') THEN filename=path+filename
							BREAK_FILE, filename, a, dir, name, ext
							moviev.filepath=dir
							moviev.filename=moviev.filepath+name+ext
							WSET, moviev.draw_win
							WIN2PS, moviev.filename
							CD, moviev.pngdir
						END
	
						'   png    ' : BEGIN 	;save png image
							CD, moviev.olddir
							BREAK_FILE, moviev.filename, a, dir, name, ext
							filename = name + '.png'
							filename = PICKFILE(file=filename, filter='*.png', $
								TITLE='Enter name of png file to save', $
								PATH=moviev.filepath,GET_PATH=path)
							IF (filename EQ '') THEN RETURN 
							BREAK_FILE, filename, a, dir, name, ext
							IF (dir EQ '') THEN filename=path+filename
							BREAK_FILE, filename, a, dir, name, ext
							moviev.filepath=dir
							moviev.filename=moviev.filepath+name+ext
							IF (CHECK_PERMISSION(moviev.filename) EQ 0) THEN RETURN
							WSET, moviev.draw_win
							TVLCT, r, g, b, /GET
							PRINT, '%%RTMVIPLAYPNG: Saving window to png file: '+ moviev.filename
							PRINT, '%%RTMVIPLAYPNG: Use mvi2frames.pro to save entire mvi to individual frames.'
							WRITE_PNG, moviev.filename, TVRD(), r, g, b
							CD, moviev.pngdir
						END
	
						'Save Movie..mvi format': BEGIN 	;save to .mvi file
							CD, moviev.olddir
							BREAK_FILE, moviev.filename, a, dir, name, ext
							filename = name + '.mvi'
							filename = PICKFILE(file=filename, filter='*.mvi', $
								TITLE='Enter name of movie file to save', $
								PATH=moviev.filepath,GET_PATH=path)
							IF (filename EQ '') THEN RETURN
							BREAK_FILE, filename, a, dir, name, ext
							IF (dir EQ '') THEN filename=path+filename
							BREAK_FILE, filename, a, dir, name, ext
							moviev.filepath=dir
							moviev.filename=moviev.filepath+name+ext
							IF (CHECK_PERMISSION(moviev.filename) EQ 0) THEN RETURN
							; BREAK_FILE, moviev.filename, a, dir, name, ext
							; moviev.filepath = dir
							; IF (dir EQ '') THEN BEGIN
							;    moviev.filepath = './'
							;    moviev.filename = path+moviev.filename
							; ENDIF
							WIDGET_CONTROL, /HOUR
							first = 1
							maxnum = N_ELEMENTS(WHERE(moviev.deleted NE 1))
							FXADDPAR, hdr, 'TIME-OBS', ' '
							FXADDPAR, hdr, 'DATE-OBS', ' '
							FXADDPAR, hdr, 'FILTER', ' '
							FXADDPAR, hdr, 'DETECTOR', ' '
							FXADDPAR, hdr, 'SECTOR', ' '
							FXADDPAR, hdr, 'POLAR', ' '
							FOR i=0, moviev.len-1 DO BEGIN
								IF (NOT(moviev.deleted(i)) AND (i GE moviev.first) AND (i LE moviev.last)) THEN BEGIN
									PRINT, '%%RTMVIPLAYPNG saving frame ', STRING(i+1,FORMAT='(I4)'), $
										' to movie file ', moviev.filename
									FXADDPAR, hdr, 'FILENAME', moviev.frames(i)
									IF (DATATYPE(moviev.img_hdrs) EQ 'STC') THEN BEGIN
										FXADDPAR, hdr, 'TIME-OBS', moviev.img_hdrs(i).time_obs
										FXADDPAR, hdr, 'DATE-OBS', moviev.img_hdrs(i).date_obs
										FXADDPAR, hdr, 'FILTER', moviev.img_hdrs(i).filter
										detector = STRTRIM(moviev.img_hdrs(i).detector,2)
										FXADDPAR, hdr, 'DETECTOR', detector
										IF (detector EQ 'EIT') THEN $
											;FXADDPAR, hdr, 'SECTOR', moviev.img_hdrs(i).sector $
											FXADDPAR, hdr, 'SECTOR', '' $
										ELSE $
											FXADDPAR, hdr, 'POLAR', moviev.img_hdrs(i).polar
									ENDIF
									WSET, moviev.win_index(i)
									img = TVRD(0,0,moviev.hsize, moviev.vsize)
									IF (first EQ 1) THEN BEGIN
										WRITE_DISK_MOVIE, moviev.filename, img, hdr, /NEW, MAXNUM=moviev.len, $
											SUNXCEN=moviev.xcen, SUNYCEN=moviev.ycen, SEC_PIX=moviev.secs
										first = 0
									ENDIF ELSE BEGIN
										WRITE_DISK_MOVIE, moviev.filename, img, hdr
									ENDELSE
								ENDIF
							ENDFOR
							WSET, moviev.draw_win
							CD, moviev.pngdir
						END
					ELSE : BEGIN
						CD, moviev.olddir
						BREAK_FILE, moviev.filename, a, dir, name, ext
						filename = name+'.mpg'
						filename = PICKFILE(file=filename, filter='*.mpg', $
						TITLE='Enter name of mpeg file to save', $
						PATH=moviev.filepath,GET_PATH=path)
						IF (filename EQ '') THEN RETURN
						BREAK_FILE, filename, a, dir, name, ext
						IF (dir EQ '') THEN filename=path+filename
						BREAK_FILE, filename, a, dir, name, ext
						moviev.filepath=dir
						moviev.filename=moviev.filepath+name+ext
						IF (CHECK_PERMISSION(moviev.filename) EQ 0) THEN RETURN
						IF (STRMID(ev.value, 23, 4) NE 'full') THEN $
							img_rebin = [moviev.hsize/2,moviev.vsize/2] $
						ELSE $
							img_rebin=0
						CASE STRMID(ev.value,46,1) OF
							'1' : BEGIN
								scale=4 & ref='DECODED'
							END
							'2' : BEGIN
								scale=8 & ref='DECODED'
							END
							'3' : BEGIN
								scale=16 & ref='ORIGINAL'
							END
							'4' : BEGIN
								scale=24 & ref='ORIGINAL'
							END
							ELSE : BEGIN
								PRINT, 'No match for case stmt.'
							END
						ENDCASE
						good = WHERE(moviev.deleted NE 1)
						WIDGET_CONTROL, /HOUR
						PIXMAPS2MPEG, moviev.filename, moviev.win_index(good), SCALE=scale, $
							REFERENCE=ref, IMG_REBIN=img_rebin
						CD, moviev.pngdir
					END
				ENDCASE
			ENDIF ELSE BEGIN
    	                    
				WIDGET_CONTROL, ev.id, GET_UVALUE=input
	
				CASE (input) OF
					'DRAW' : BEGIN	;image window
					END
					'SPEED' : BEGIN 	;speed
						IF (ev.value EQ 0) THEN $
							moviev.stall = 5. $
						ELSE $
							moviev.stall = (100 - ev.value)/50.
						moviev.stallsave = moviev.stall
						WIDGET_CONTROL, moviev.base, TIMER=moviev.stall   ; Request another timer event
					END
					'SLIDE_FIRST' : BEGIN	;first frame
						moviev.first = ev.value
					END
					'SLIDE_LAST' : BEGIN	;last frame
						moviev.last = ev.value
					END
					'SLIDE_SKIP' : BEGIN	;skip frame
						moviev.nskip = ev.value
					END
					'SLIDE_NUM' : BEGIN   ;change current frame number
						WIDGET_CONTROL, moviev.sliden, GET_VALUE=val
						val = 0 > FIX(val(0)) < moviev.last
						moviev.current=val
						WSET, moviev.draw_win
						DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
						WIDGET_CONTROL, moviev.cframe, SET_VALUE=STRCOMPRESS(moviev.current, /REMOVE_ALL)
						WIDGET_CONTROL, moviev.cname, SET_VALUE=STRCOMPRESS(moviev.frames(moviev.current),/REMOVE_ALL)
						WIDGET_CONTROL, moviev.base, TIMER=10000.   ; Put timer on hold
						moviev.stall = 10000.
					END
					ELSE	 : BEGIN
						PRINT, '%%RTMVIPLAYPNG_EVENT.  Unknown event.'
					END
				ENDCASE
			ENDELSE
		ENDIF ELSE BEGIN
			WIDGET_CONTROL, ev.id, GET_UVALUE=input
			CASE (input) OF
				'FRAME_NUM' : BEGIN   ;change current frame number
					WIDGET_CONTROL, moviev.cframe, GET_VALUE=val
					val = 0 > FIX(val(0)) < moviev.last
					moviev.current=val
					DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
					WIDGET_CONTROL, moviev.cframe, SET_VALUE=STRCOMPRESS(moviev.current, /REMOVE_ALL)
					WIDGET_CONTROL, moviev.cname, SET_VALUE=STRCOMPRESS(moviev.frames(moviev.current),/REMOVE_ALL)
					WIDGET_CONTROL, moviev.sliden, SET_VALUE=moviev.current
				END
				ELSE : BEGIN
					PRINT, '%%RTMVIPLAYPNG_EVENT.  Unknown event.'
				END
			ENDCASE
	
		ENDELSE
	ENDELSE
	
	WIDGET_CONTROL, ev.top, SET_UVALUE=moviev

END ;------------------ end runmovie_event -------------------------


;------------------ runmovie event for CW_BGROUP buttons ---------

FUNCTION RTMVIPLAYPNG_BGROUP, ev

	WIDGET_CONTROL, ev.top, GET_UVALUE=moviev   ; get structure from UVALUE

	input = ev.value

	CASE (input) OF

		'DONE' : BEGIN       ;** exit program
			WIDGET_CONTROL, /DESTROY, moviev.base
			WIDGET_CONTROL, /DESTROY, moviev.winbase
			FOR i=0, moviev.len-1 DO WDELETE, moviev.win_index(i)
			CD, moviev.olddir
			RETURN, 0
		END
		'ADJCT' : BEGIN	;color table adjustment
			XLOADCT
		END
		'DELETE' : BEGIN	;delete current frame
			WIDGET_CONTROL, moviev.base, TIMER=10000.   ; Put timer on hold
			moviev.deleted(moviev.current) = 1
		END
		'CALLHT' : BEGIN	;call wrunmoviem
			tmp = INDGEN(N_ELEMENTS(moviev.win_index))
			good = WHERE((moviev.deleted NE 1) AND (tmp GE moviev.first) AND (tmp LE moviev.last))
			CD, moviev.olddir
			WRUNMOVIEM_RT, moviev.win_index(good), HDRS=moviev.img_hdrs(good), NAMES=moviev.frames(good), $
			SUNXCEN=moviev.xcen, SUNYCEN=moviev.ycen, SEC_PIX=moviev.secs
		END 
		'NEXT' : IF (ev.select NE 0) THEN BEGIN	;next frame
				WIDGET_CONTROL, moviev.base, TIMER=10000.   ; Put timer on hold
				moviev.stall = 10000.
				moviev.forward = 1
				REPEAT BEGIN
					moviev.current = (moviev.current + 1) MOD moviev.len
					IF (moviev.current GT moviev.last) THEN moviev.current = moviev.first
					IF (moviev.current LT moviev.first) THEN moviev.current = moviev.first
				ENDREP UNTIL (NOT(moviev.deleted(moviev.current)))
				WSET, moviev.draw_win
				DEVICE, COPY=[0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
				WIDGET_CONTROL, moviev.cframe, SET_VALUE=STRCOMPRESS(moviev.current, /REMOVE_ALL)
				WIDGET_CONTROL, moviev.cname,SET_VALUE=STRCOMPRESS(moviev.frames(moviev.current),/REMOVE_ALL)
				WIDGET_CONTROL, moviev.sliden, SET_VALUE=moviev.current
			ENDIF
		'PREV' : IF (ev.select NE 0) THEN BEGIN	;previous frame
				WIDGET_CONTROL, moviev.base, TIMER=10000.   ; Put timer on hold
				moviev.stall = 10000.
				moviev.forward = 0
				REPEAT BEGIN
					moviev.current = (moviev.current - 1) MOD moviev.len
					IF (moviev.current LT moviev.first) THEN moviev.current = moviev.last
					IF (moviev.current GT moviev.last) THEN moviev.current = moviev.last
				ENDREP UNTIL (NOT(moviev.deleted(moviev.current)))
				WSET, moviev.draw_win
				DEVICE, COPY=[0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
				WIDGET_CONTROL, moviev.cframe, SET_VALUE=STRCOMPRESS(moviev.current, /REMOVE_ALL)
				WIDGET_CONTROL, moviev.cname,SET_VALUE=STRCOMPRESS(moviev.frames(moviev.current),/REMOVE_ALL)
				WIDGET_CONTROL, moviev.sliden, SET_VALUE=moviev.current
			ENDIF
		'CONTINUOUS' : IF (ev.select NE 0) THEN BEGIN	;continuous
					;** set direction to saved direction
					moviev.forward = moviev.dirsave
					moviev.stall = moviev.stallsave
					WIDGET_CONTROL, moviev.base, TIMER=moviev.stall   ; Request another timer event
				ENDIF
		'FORWARD' : IF (ev.select NE 0) THEN BEGIN	;forward
					moviev.bounce = 0
					moviev.forward = 1
					moviev.dirsave = 1
				ENDIF
		'REVERSE' : IF (ev.select NE 0) THEN BEGIN	;reverse
					moviev.bounce = 0
					moviev.forward = 0
					moviev.dirsave = 0
				ENDIF
		'BOUNCE' : IF (ev.select NE 0) THEN BEGIN	;bounce
					moviev.bounce = 1
				ENDIF
		'PAUSE' : IF (ev.select NE 0) THEN BEGIN	;pause
					moviev.dopause = 1
				ENDIF
		'NO_PAUSE' : IF (ev.select NE 0) THEN BEGIN	;no pause
					moviev.dopause = 0
				ENDIF
		'LOAD' : BEGIN	;** load movie from file
			win_index = PICKFILE(filter='*.mvi',file=moviev.filename,/MUST_EXIST, $
			TITLE='Select movie file to load', $
			PATH=moviev.filepath, GET_PATH=path)
			IF ((win_index EQ '') OR (win_index EQ moviev.filename)) THEN RETURN, 0
			BREAK_FILE, win_index, a, dir, name, ext
			moviev.filepath = dir
			IF (dir EQ '') THEN BEGIN
				moviev.filepath = './'
				win_index = path+win_index
			ENDIF
			WIDGET_CONTROL, /DESTROY, moviev.base
			WIDGET_CONTROL, /DESTROY, moviev.winbase
			FOR i=0, moviev.len-1 DO WDELETE, moviev.win_index(i)
			RTMVIPLAYPNG, win_index
			RETURN, 0
		END
		ELSE : BEGIN
			PRINT, '%%RTMVIPLAYPNG_BGROUP.  Unknown event.'
		END
	ENDCASE

	WIDGET_CONTROL, ev.top, SET_UVALUE=moviev
	RETURN, 0
END



;----------------------- wrunmovie ---------------------------------

PRO RTMVIPLAYPNG, name, filelist=filelist, LENGTH=length, IMG_REBIN=img_rebin, SUNXCEN=sunxcen, SUNYCEN=sunycen, SEC_PIX=sec_pix, SKIP=skip, $
    RUNNING_DIFF=running_diff, PNG_DIR=png_dir,noloadct=noloadct

	COMMON RTWIN_INDEX, base, pnglist


	IF NOT(KEYWORD_SET(RUNNING_DIFF)) THEN running_diff=0
	IF NOT(KEYWORD_SET(IMG_REBIN)) THEN img_rebin=0
	IF keyword_set(SKIP) THEN skip=skip ELSE skip=0
	
	IF (KEYWORD_SET(RUNNING_DIFF)) THEN LOADCT, 0

	times = 0
	nskip=0
	stall0 = 97
	stall = (100 - stall0)/50.
	stallsave = stall
	filename = 'default.mvi'
	filepath = './'
	ftitle=''
	IF KEYWORD_SET(SUNXCEN) THEN xcen = sunxcen ELSE xcen = 0
	IF KEYWORD_SET(SUNYCEN) THEN ycen = sunycen ELSE ycen = 0
	IF KEYWORD_SET(SEC_PIX) THEN secs = sec_pix ELSE secs = 0
	;stop
	;** find files matching input name and load
        
        ;  *#*#*#*#*#*#*#*#*#*#*#*#*#* KB Edit 06/27/07 *#*#*#*#*#*#*#*#*#*#*#*#*#* 
	IF keyword_set(filelist) THEN BEGIN
            pnglist=filelist 
            CD,current=olddir
            break_file,filelist[0],disk_log, dir, filnam, ext, fversion, node
            pngdir=dir
            match_png='Input file list'
        ENDIF ELSE BEGIN
            IF KEYWORD_SET(PNG_DIR) THEN pngdir = png_dir ELSE BEGIN
		mvidir = GETENV('MVIS')
		IF (mvidir NE '') THEN pngdir = mvidir+'/rtmovie/pngs/' ELSE pngdir = './'
	    ENDELSE
	    match_png = '*'+name+'*.png'
	    CD, pngdir, current=olddir
	    pnglist = FINDFILE(match_png)
	    IF (pnglist(0) EQ '') THEN BEGIN
		PRINT, '%%RTMVIPLAYPNG: No files found matching: ', match_png
		RETURN
	    ENDIF
        ENDELSE
	; *#*#*#*#*#*#*#*#*#*#*#*#*#* END KB Edit 06/27/07 *#*#*#*#*#*#*#*#*#*#*#*#*#* 
        len = N_ELEMENTS(pnglist)
	loaded_pngs = pnglist
	IF KEYWORD_SET(LENGTH) THEN BEGIN
		IF (len GE length) THEN BEGIN
			loaded_pngs = pnglist(len-length:*)
			len = length
		ENDIF
	ENDIF 

	img_tai = DBLARR(len)

	image = READ_PNG( loaded_pngs(0), r, g, b )
        
       ; if keyword_set(noloadct) THEN BEGIN
       ;     window,0
       ;     tvscl,image, true=1
       ;     wdelete,0
       ;     image=color_quan(image,1,r,g,b,cube=6)
       ; ENDIF
        
	if ( !version.release EQ '5.3' ) then image=rotate(image, 7)
	sz=SIZE(image)
        IF n_elements(sz) EQ 6 THEN BEGIN
            nx = sz(2)
            ny = sz(3)
	    rnx = nx
	    rny = ny
        ENDIF ELSE BEGIN
	    nx = sz(1)
	    ny = sz(2)
	    rnx = nx
	    rny = ny
        ENDELSE
	use_rebin = 1
	IF KEYWORD_SET(IMG_REBIN) THEN BEGIN
		rrnx = img_rebin(0)
		rrny = img_rebin(1)
		;** if shrinking by integer factor use REBIN otherwise use CONGRID with linear interpolation
		IF ((nx MOD rrnx) EQ 0) THEN use_rebin=1 ELSE use_rebin=0
		xcen = xcen * FLOAT(rrnx)/rnx
		ycen = ycen * FLOAT(rrny)/rny
		secs = secs / (FLOAT(rrny)/rny)
		rnx = rrnx
		rny = rrny
	ENDIF

        ;window,0
	ahdr = {mvihdr, filename:'',detector:'',time_obs:'',date_obs:'',filter:'',polar:'',sector:'',exptime:0.0}
	FOR f=0, len-1,skip+1 DO BEGIN
		image = READ_PNG( loaded_pngs(f), r, g, b )
                if keyword_set(noloadct) THEN BEGIN
                    ;r=image[0,*,*]
                    ;g=image[1,*,*]
                   ; b=image[2,*,*]
                   ;device, decomposed=0
                    ;window,0
                    tvscl,image, true=1
                    image=color_quan(image,1,r,g,b,cube=6)
                   ; tvlct,r,g,b,/get
                    
                ENDIF
        ;stop
		if ( !version.release EQ '5.3' ) then image=rotate(image, 7)
		sz=SIZE(image)
                IF n_elements(sz) EQ 6 THEN BEGIN
                    nx = sz(2)
		    ny = sz(3)
                ENDIF ELSE BEGIN
		    nx = sz(1)
		    ny = sz(2)
                ENDELSE
		IF (f EQ 0) AND NOT(KEYWORD_SET(RUNNING_DIFF)) AND ~keyword_set(noloadct) THEN TVLCT, r, g, b
		PRINT, '%%RTMVIPLAYPNG reading image ', loaded_pngs(f), STRING(f+1,FORMAT='(I4)'), ' of ',$
		STRING(len,FORMAT='(I4)')
		WINDOW, XSIZE = rnx, YSIZE = rny, /PIXMAP, /FREE
		IF (f EQ 0) THEN win_index = !D.WINDOW ELSE win_index = [win_index, !D.WINDOW]
		IF (nx NE rnx) THEN BEGIN
			IF (use_rebin EQ 1) THEN $
				image = REBIN(image, rnx, rny) $
			ELSE $
				image = CONGRID(image, rnx, rny, /INTERP)
		ENDIF 

		IF (KEYWORD_SET(RUNNING_DIFF)) THEN BEGIN
			times = 1
			!P.FONT=0
			DEVICE, FONT='-adobe-helvetica-medium-r-normal--24-240-75-75-p-130-iso8859-1'
			IF (f GT 0) THEN BEGIN
				old = image
				image = BYTSCL(FIX(image)-prev, -30,30)
				prev = old
			ENDIF ELSE prev = image
		ENDIF

		TV, image
                
                ; *#*#*#*#*#*#*#*#*#*#*#*#*#*  KB Edit 06/27/07 *#*#*#*#*#*#*#*#*#*#*#*#*#* 
                IF keyword_set(filelist) THEN BEGIN ; Only do this if a list of files is passed in
                    break_file,loaded_pngs[0],disk_log, dir, filnam, ext, fversion, node
                    ahdr.filename=filnam+ext
                    ahdr.detector = strmid(filnam,18,2)
                    ahdr.time_obs=strmid(filnam,9,2)+':'+STRMID(filnam,11,2)
		    ahdr.date_obs=STRMID(filnam,0,4)+'/'+STRMID(filnam,4,2)+'/'+STRMID(filnam,6,2)
                ENDIF ELSE BEGIN    
		    ahdr.filename=loaded_pngs(f)
		    tmp=STRMID(loaded_pngs(f),14,3)
		    REMCHAR, tmp, '_'
		    REMCHAR, tmp, '.'
		    ahdr.detector = tmp
		    ahdr.time_obs=STRMID(loaded_pngs(f),9,2)+':'+STRMID(loaded_pngs(f),11,2)
		    ahdr.date_obs=STRMID(loaded_pngs(f),0,4)+'/'+STRMID(loaded_pngs(f),4,2)+'/'+STRMID(loaded_pngs(f),6,2)
		ENDELSE
                ; *#*#*#*#*#*#*#*#*#*#*#*#*#*  END KB Edit  06/27/07 *#*#*#*#*#*#*#*#*#*#*#*#*#* 
                
                tai = UTC2TAI(STR2UTC(ahdr.date_obs + ' ' + ahdr.time_obs))
		IF (f EQ 0) THEN hdrs=ahdr ELSE hdrs=[hdrs,ahdr]
		IF (f EQ 0) THEN names = ahdr.filename ELSE names = [names, ahdr.filename]
		IF (f EQ 0) THEN img_tai = tai ELSE img_tai=[img_tai,tai]
		IF (times EQ 1) THEN $
		XYOUTS, 10, 10, ahdr.date_obs + ' ' + STRMID(ahdr.time_obs,0,5), CHARSIZE=1.5,/DEVICE
	ENDFOR	;** reading in .png images
        
        if keyword_set(noloadct) then wdelete,0
	;** load movies from existing pixmaps
	WSET, win_index(0)
	hsize = !D.X_SIZE
	vsize = !D.Y_SIZE
	;** get length of movie
	len = N_ELEMENTS(win_index)
	if keyword_set(noloadct) THEN win_index=win_index[0:len-2]
        len--
        frames = names

	first = 0
	last = len-1
	current = 0
	forward = 1
	dirsave = 1
	bounce = 0
	pause = 0
	dopause = 1
	deleted = BYTARR(len)
	showmenu = 0


	;**--------------------Create Widgets-------------------------------**
	hhsize = (hsize+20) > (512+20)
	title = 'Runmovie: '+ftitle
	winbase = WIDGET_BASE(TITLE=title)
	base = WIDGET_BASE(/COLUMN, XOFFSET=275, YOFFSET=275, TITLE='Runmovie Control')

	;** create window widget to display images in
	draw_w = WIDGET_DRAW(winbase, XSIZE=hsize, YSIZE=vsize, EVENT_PRO='RTMVIPLAYPNG_DRAW', $
		/FRAME, /BUTTON_EVENTS, RETAIN=2)

	base1 = WIDGET_BASE(base, /ROW)
	base2 = WIDGET_BASE(base1, /COLUMN, /FRAME)
	flabel = WIDGET_LABEL(base2, VALUE=' #   Frame')
	base25 = WIDGET_BASE(base2, /ROW)
	cframe = WIDGET_TEXT(base25, VALUE='0', XSIZE=3, YSIZE=1, /EDITABLE, UVALUE='FRAME_NUM')
	cname = WIDGET_TEXT(base25, VALUE=' ', XSIZE=12, YSIZE=1)
	slide2 = WIDGET_SLIDER(base1, TITLE='First Frame', $
		VALUE=0, UVALUE='SLIDE_FIRST', MIN=0, MAX=len)
	slide3 = WIDGET_SLIDER(base1, TITLE='Last Frame', $
		VALUE=len-1, UVALUE='SLIDE_LAST', MIN=0, MAX=len-1)
	slide1 = WIDGET_SLIDER(base1, TITLE='Playback Speed', SCROLL=1, $
		VALUE=stall0, UVALUE='SPEED', MIN=0, MAX=100, /DRAG)

	base3 = WIDGET_BASE(base, /ROW)

	col = WIDGET_BASE(base3, /COLUMN, /FRAME)
	tmp = CW_BGROUP(col,  ['Forward', 'Reverse', 'Bounce'], /EXCLUSIVE, /COLUMN, IDS=dirb, $
		BUTTON_UVALUE = ['FORWARD', 'REVERSE','BOUNCE'], EVENT_FUNCT='RTMVIPLAYPNG_BGROUP')
	col = WIDGET_BASE(base3, /COLUMN, /FRAME)
	tmp = CW_BGROUP(col,  ['Pause At End', 'No Pause'], /EXCLUSIVE, /COLUMN, IDS=pauseb, $
		BUTTON_UVALUE = ['PAUSE','NO_PAUSE'], EVENT_FUNCT='RTMVIPLAYPNG_BGROUP')
	tmp = CW_BGROUP(col,  ['Call wrunmoviem_rt'], /COLUMN, $
		BUTTON_UVALUE = ['CALLHT'], EVENT_FUNCT='RTMVIPLAYPNG_BGROUP')

	col = WIDGET_BASE(base3, /COLUMN, /FRAME)
	tmp = CW_BGROUP(col,  ['Continuous', 'Next Frame', 'Prev. Frame'], /COLUMN, $
		BUTTON_UVALUE = ['CONTINUOUS', 'NEXT','PREV'], EVENT_FUNCT='RTMVIPLAYPNG_BGROUP')

	junk={CW_PDMENU_S, flags:0, name:''}
	desc = [ { CW_PDMENU_S, 1, 'Save Frame' }, $
		{ CW_PDMENU_S, 0, 'Postscript' }, $
		{ CW_PDMENU_S, 0, '   png    ' } ]

	col = WIDGET_BASE(base3, /COLUMN, /FRAME)
	tmp = CW_BGROUP(col,  ['Adjust Color', 'Delete Frame'], /COLUMN, IDS=otherb, $
		BUTTON_UVALUE = ['ADJCT', 'DELETE'], EVENT_FUNCT='RTMVIPLAYPNG_BGROUP')
	tmp = CW_PDMENU(col, desc, /RETURN_NAME)
	desc = [ { CW_PDMENU_S, 1, 'Save Movie' }, $
		{ CW_PDMENU_S, 0, '.mvi format' }, $
		{ CW_PDMENU_S, 1, '.mpg format' }, $
		{ CW_PDMENU_S, 1, 'full resolution' }, $
		{ CW_PDMENU_S, 0, 'format 1 high quality/low  compression' }, $
		{ CW_PDMENU_S, 0, 'format 2 high quality/good compression' }, $
		{ CW_PDMENU_S, 0, 'format 3 good quality/good compression' }, $
		{ CW_PDMENU_S, 2, 'format 4 low  quality/high compression' }, $
		{ CW_PDMENU_S, 1, '1/2  resolution' }, $
		{ CW_PDMENU_S, 0, 'format 1 high quality/low  compression' }, $
		{ CW_PDMENU_S, 0, 'format 2 high quality/good compression' }, $
		{ CW_PDMENU_S, 0, 'format 3 good quality/good compression' }, $
		{ CW_PDMENU_S, 2, 'format 4 low  quality/high compression' } ]
	col = WIDGET_BASE(base3, /COLUMN, /FRAME)
	tmp = CW_PDMENU(col, desc, /RETURN_FULL_NAME)
	tmp = CW_BGROUP(col,  [' Load Movie ', '    Quit    '], /COLUMN, $
		BUTTON_UVALUE = ['LOAD','DONE'], EVENT_FUNCT='RTMVIPLAYPNG_BGROUP')
	sliden = WIDGET_SLIDER(base, TITLE='Current Frame', VALUE=0, UVALUE='SLIDE_NUM', $
		/DRAG, /SCROLL, MIN=0, MAX=len-1)


	;**--------------------Done Creating Widgets-----------------------------**

	WIDGET_CONTROL, /REAL, winbase
	WIDGET_CONTROL, draw_w, GET_VALUE=draw_win
	WIDGET_CONTROL, base, MAP=0
	WIDGET_CONTROL, /REAL, base
	WIDGET_CONTROL, dirb(0), SET_BUTTON=1
	WIDGET_CONTROL, pauseb(0), SET_BUTTON=1
	WIDGET_CONTROL, base, TIMER=0. ;.000001
	WIDGET_CONTROL, cname, TIMER=60	;** timer to monitor for new .png files
	WSET, draw_win

	CASE STRUPCASE(ahdr.detector) OF 
		'EIT':BEGIN
			factor = FLOAT(rnx)/1024.
			xcen = 505.5*factor
			ycen = 514.3*factor
			secs = 2.59/factor
			IF (STRPOS(name,'195') GE 0) THEN hdrs(*).sector = '195A' $
			ELSE IF (STRPOS(name,'304') GE 0) THEN hdrs(*).sector = '304A' $
			ELSE IF (STRPOS(name,'284') GE 0) THEN hdrs(*).sector = '284A' $
			ELSE hdrs(*).sector = '171A'
			hdrs(*).filter = 'Al +1'
		END
		'C1':BEGIN
			factor = FLOAT(rnx)/768.
			xcen = 382.4*factor
			ycen = 303.4*factor
			secs = 5.8/factor
			hdrs(*).filter = 'Fe XIV'
			hdrs(*).polar = 'Clear'
		END
		'C2':BEGIN
			factor = FLOAT(rnx)/1024.
			xcen = 512.6*factor
			ycen = 506.2*factor
			secs = 12.1/factor
			hdrs(*).filter = 'Orange'
			hdrs(*).polar = 'Clear'
		END
		'C3':BEGIN
			factor = FLOAT(rnx)/1024.
			xcen = 517.7*factor
			ycen = 531.9*factor
			secs = 56.0/factor
			hdrs(*).filter = 'Clear'
			hdrs(*).polar = 'Clear'
		END
		ELSE: BEGIN
		END
	ENDCASE

	img_hdrs = hdrs
	moviev = { $
		base:base, $ 
		running_diff:running_diff, $ 
		img_rebin:img_rebin, $ 
		use_rebin:use_rebin, $ 
		olddir:olddir, $ 
		rnx:rnx, $ 
		rny:rny, $ 
		times:times, $ 
		xcen:xcen, $ 
		ycen:ycen, $ 
		secs:secs, $ 
		winbase:winbase, $ 
		current:current, $ 
		forward:forward, $ 
		dirsave:dirsave, $ 
		bounce:bounce, $ 
		first:first, $ 
		last:last, $ 
		pause:pause, $ 
		dopause:dopause, $ 
		len:len, $ 
		hsize:hsize, $ 
		vsize:vsize, $ 
		win_index:win_index, $ 
		draw_win:draw_win, $ 
		cframe:cframe, $ 
		cname:cname, $ 
		frames:frames, $ 
		deleted:deleted, $ 
		showmenu:showmenu, $
		filename:filename, $
		filepath:filepath, $
		img_hdrs:img_hdrs, $
		img_tai:img_tai, $
		nskip:nskip, $ 
		stallsave:stallsave, $ 
		sliden:sliden, $ 
		pngdir:pngdir, $ 
		match_png:match_png, $ 
		loaded_pngs:loaded_pngs, $ 
		ahdr:ahdr, $ 
		stall:stall $ 
	}

	WIDGET_CONTROL, base, SET_UVALUE=moviev
	XMANAGER, 'RTMVIPLAYPNG', base, EVENT_HANDLER='RTMVIPLAYPNG_EVENT'

END
