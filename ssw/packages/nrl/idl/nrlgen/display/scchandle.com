; $Id: scchandle.com,v 1.1 2008/02/15 22:47:55 nathan Exp $
;
; $Log: scchandle.com,v $
; Revision 1.1  2008/02/15 22:47:55  nathan
; moved from lasco/idl/display because called by scclister.pro
;
 COMMON chandle,     $
	image,       $ ; image array ( byt, int, flt, ...)
	name,        $ ; image name (string)
	head,        $ ; image header (string)
        h,           $ ; image header structure
        a3,          $ ; rebined image in window no. 3
        solar_radius,$ ; solar radius from SOHO orbit
	h_array,     $ ; image index array
	h_name,      $ ; image name index array
	h_head,      $ ; image header index array 
	h_hs,        $ ; image structure header index array 
	h_small,     $ ; small image header index array 
        h_solar,     $
	top_array,   $    
	top_name,    $
	top_head,    $
	top_hs,      $
	top_small,   $
        top_solar,   $
	string_name, $
	s_handle,    $
	sz,          $ ; image size: sz = SIZE( image )
	index,       $
        box

