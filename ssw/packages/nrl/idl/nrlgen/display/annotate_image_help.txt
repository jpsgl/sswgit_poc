Using ANNOTATE_IMAGE.PRO

Text: Enter one line of text. To add multiple or long lines, use 
Save Annotation button to add one line and then proceed with the 
next line.

Fonts: To see the list of available options for each category, 
click on the displayed value. Then select your choice with another 
click. You may choose from Hershey Vector Fonts, Truetype Fonts, 
and available Hardware Fonts. The Show Font button calls 
showfont.pro which will display characters for the last-selected 
Vector or TrueType font. The listed embedded commands allow you 
to do things like subscripts and single-character-symbols in the 
same line of text. The Select a Hardware Font button calls 
xfont.pro, which allows you to browse the available hardware 
fonts. An example is diplayed in a window at the bottom of the 
Font Widget.

Text position: Click on the image to position the text, or enter 
the coordinates in the widget. Change size of the text by entering 
a different Char Size. Rotate the text around the selected 
position from 0-360 degrees.

Arrow: To draw an arrow, click the Select button, then on the 
image click where you want the tail to be, then click where you 
want the head to be. OR enter or change the coordinates in the 
widget. 

Color: You may change the color of the Text, the Text Background, 
or the Arrow by choosing a predefined color from the drop-down 
menu, or move the slider to select a color. If you call 
annotate_image with the /TRUECOLOR option, then you have the 
option of choosing colors for your annotations that are not in the 
8-bit color table.

Finish: To save your image, you must always first ACCEPT Annotation.
Default is to save the current annotation to all frames, or you can
specify which frames you want it applied to. After ACCEPT, you may
Save (current) Frame to File in PNG, JPEG, or GIF format. Then click
Done to return to the calling program. If called from scc_playmovie, 
run the movie to see your annotated frame(s). 

$Id: annotate_image_help.txt,v 1.3 2011/04/21 18:54:35 nathan Exp $
