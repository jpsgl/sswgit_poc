pro plotprep
;+
; $Id: plotprep.pro,v 1.2 2009/05/14 17:27:02 nathan Exp $
; NAME:
;       plotprep
;
; PURPOSE: set commonly used env for plotting: white background, 8-bit color
;   	   using tek_color
;
; AUTHOR: Nathan Rich
;
; CATEGORY: graphics plot color system environment
;
; CALLING SEQUENCE:
;
; KEYWORD PARAMETERS:
;
; COMMON BLOCKS:
;
;       None
;
; RESTRICTIONS:  
;
; $Log: plotprep.pro,v $
; Revision 1.2  2009/05/14 17:27:02  nathan
; gray background; make a procedore
;
; Revision 1.1  2007/09/21 18:56:37  nathan
; set X device for plotting
;
;-

device,decomposed=0,retain=2
tek_color
!p.background=220
!p.color=0
!x.style=1
!y.style=3

END ;-------------------------------------------------------------------------------
