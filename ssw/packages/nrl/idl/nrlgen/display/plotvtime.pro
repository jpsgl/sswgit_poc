;+
pro plotvtime,tm,vals, label, NOWINDOW=nowindow, COLOR=color, _Extra=_extra
;
; $Id: plotvtime.pro,v 1.4 2010/12/09 22:41:12 nathan Exp $
; Name:     plotvtime
;
; Purpose:  Generic routine for plotting value vs. time and labeling value
;
; Inputs:   tm	    Time array for utplot
;   	    vals    what to plot vs. time
;   	    label   string to label vals with
;
; Optional input keywords:
;   	/NOWINDOW   Use existing window for plot
;   	/MDY	    tm is format of MM/DD/YYYY
;   	Any plot or anytim2utc keywords
;
; Outputs:  none - plots values in window
;
; Common blocks:    timeplot	
;
; WRitten by N.Rich, NRL/I2
;-
; $Log: plotvtime.pro,v $
; Revision 1.4  2010/12/09 22:41:12  nathan
; make more robust
;
; Revision 1.3  2009/10/27 19:48:01  nathan
; fix COLOR keyword and anytim2utc call
;
; Revision 1.2  2009/05/14 17:29:39  nathan
; fix some things
;
; Revision 1.1  2009/04/13 20:31:45  nathan
; subroutines created for plotstereotemps.pro
;


common timeplot, keyx, keyy, clr

keyx=5
keyy=5
IF keyword_set(COLOR) THEN clr=color ELSE clr=0

ptype='time'
IF n_params() LT 3 THEN BEGIN
    ; see if this is a not-time plot
    IF datatype(vals) EQ 'STR' THEN BEGIN
    	ptype='xy' 
	label=vals
	vals=tm
    ENDIF ELSE BEGIN
    	label=''
    	read,'Please enter a label to identify data values',label
    ENDELSE
ENDIF

ymin=min(vals,max=ymax)

; start plots
plotprep
IF ptype EQ 'time' THEN clear_utplot
!x.style=1
;!x.margin=[6,3]
IF ymin Ge 100 or ymin LT .001 THEN !x.margin=[10,2] ELSE !x.margin=[6,2]
!y.margin=[6,3]

IF ~keyword_set(NOWINDOW) THEN window,0

IF ptype EQ 'time' THEN BEGIN
    ud=anytim2utc(tm, _Extra=_extra )
    utplot,ud,vals,color=clr, _Extra=_extra
ENDIF ELSE $
    plot,vals,color=clr, _Extra=_extra
xyouts,keyx,keyy,strupcase(label),color=clr,/device,size=2

; increment for next plot with oplotvtime.pro
clr=3	; skip red
keyx=keyx+strlen(label)*13+10

 end
