PRO mvi2mpg,FILE=file,GAMMA=gamma,LCT=lct,MPEG_NAME=mpeg_name,Full=Full, $
            SCALE=scale,REF=ref, USE_CURR_COLORS=use_curr_colors, $
	    TIMES=times, PNG=png
;
; KEYWORDS:
;  FILE		Filename of MVI file.
;  LCT		Set to value of wished color table (1..45) Default is to use
;		the color table of MVI file.
;  /USE_CURR_COLORS	Use color table in use when procedure is called
;  GAMMA	Set to desired gamma value for color table
;  MPEG_NAME	Desired name of output
;  /FULL	Set to output in full size: Default is half size
;  SCALE	Compression factor: 4,8,16,24 (4 is low compression)
;  REF		High quality='DECODED', Low quality='ORIGINAL'
;  /TIMES	Add time from MVI header to each frame
;  /PNG		Use png instead of gif
;
;
; MODIFICATIONS:
;  2/2/01, nbr - Change use of GAMMA, LCT keywords--use MVI color table as default
;  030714, jake - added PNG keyword
;
;- 

   IF (KEYWORD_SET(USE_CURR_COLORS)) THEN TVLCT, r, g, b, /GET

   filename = 'default.mvi'
   filepath = './'
   ftitle=''

   spawn,'pwd',path
   path = path + get_delim()
   ;** have user select movie file (.mvi)
   IF (DATATYPE(win_index) EQ 'UND') THEN BEGIN
      IF (file EQ '') THEN RETURN
      BREAK_FILE, file, a, dir, name, ext
      IF (dir EQ '') THEN win_index = path+file ELSE win_index = file
   ENDIF

   ;** read movie in from movie file (.mvi)
   IF (DATATYPE(win_index) EQ 'STR') THEN BEGIN
      IF (N_PARAMS() EQ 2) THEN do2=1 ELSE do2=0
      BREAK_FILE, win_index, a, dir, name, ext
      IF (do2 EQ 1) THEN BREAK_FILE, win_index2, a2, dir2, name2, ext2
      ftitle = name+ext
      filename = win_index
      filepath = dir
      IF (dir EQ '') THEN filepath = './'
      OPENR,lu,filename(0),/GET_LUN
      SCCREAD_MVI, lu, file_hdr, ihdrs, imgs
      IF (do2 EQ 1) THEN OPENR,lu2,win_index2,/GET_LUN
      IF (do2 EQ 1) THEN SCCREAD_MVI, lu2, file_hdr2, ihdrs2, imgs2

;      IF ( (file_hdr.ver GT 0) AND (file_hdr.sunxcen NE 0) AND (file_hdr.sunycen NE 0) AND $
;        (file_hdr.sec_pix NE 0) ) THEN BEGIN
;         xcen = file_hdr.sunxcen
;         ycen = file_hdr.sunycen
;         secs = file_hdr.sec_pix
;      ENDIF


      win_index = INTARR(file_hdr.nf)
      names = STRARR(file_hdr.nf)
      nx = file_hdr.nx
      ny = file_hdr.ny
      rnx = nx
      rny = ny
      IF KEYWORD_SET(IMG_REBIN) THEN BEGIN
         rnx = img_rebin(0)
         rny = img_rebin(1)
;         xcen = xcen * FLOAT(rnx)/nx
;         ycen = ycen * FLOAT(rny)/ny
;         secs = secs / FLOAT(rny)/ny
      ENDIF

      IF KEYWORD_SET(SKIP) THEN BEGIN
         skip = skip
      ENDIF ELSE skip=0
      IF KEYWORD_SET(START) THEN start=start ELSE start=0
      IF KEYWORD_SET(LENGTH) THEN length=start+length ELSE length=file_hdr.nf
      FOR i=start,length-1,skip+1 DO BEGIN
         PRINT, '%%WRUNMOVIE reading frame ', STRING(i+1,FORMAT='(I4)'), ' of ',STRING(file_hdr.nf,FORMAT='(I4)'), $
                ' from movie file ', filename
         WINDOW, XSIZE = rnx, YSIZE = rny*(do2+1), /PIXMAP, /FREE
         IF (DATATYPE(win_ind) EQ 'UND') THEN win_ind = !D.WINDOW ELSE win_ind = [win_ind, !D.WINDOW]
         image = imgs(i)
         IF KEYWORD_SET(IMG_REBIN) THEN $
            image = REBIN(image, rnx, rny)
         TV, image
         IF (do2 EQ 1) THEN BEGIN
            image = imgs2(i)
            IF KEYWORD_SET(IMG_REBIN) THEN $
               image = REBIN(image, rnx, rny)
            TV, image, 0, rny
         ENDIF
         ahdr = MVIHDR2STRUCT(ihdrs(i))
         ;IF ( swapflag EQ 1 ) THEN BYTEORDER, ahdr
         IF (i EQ start) THEN hdrs=ahdr ELSE hdrs=[hdrs,ahdr]
         IF KEYWORD_SET(TIMES) THEN $
            XYOUTS, 10, 10, hdrs(i).date_obs + ' ' + STRMID(hdrs(i).time_obs,0,5), CHARSIZE=1.5,/DEVICE
      ENDFOR
      win_index = win_ind
      names = hdrs.filename
      CLOSE,lu
      FREE_LUN,lu
   ENDIF

   ;** load movies from existing pixmaps
   WSET, win_index(0)
   hsize = !D.X_SIZE
   vsize = !D.Y_SIZE
   ;** get length of movie
   len = N_ELEMENTS(win_index)
   frames = STRARR(len)	;** array of movie frame names (empty)
   IF (KEYWORD_SET(names) NE 0) THEN frames = names


   IF NOT KEYWORD_SET(mpeg_name) THEN $
        mpeg_name = STRMID(file,0,RSTRPOS(file,'.')+1) + 'mpg'
   ;IF NOT KEYWORD_SET(gct) THEN gct = .15
   ;IF NOT KEYWORD_SET(lct) THEN lct = 9   ; GRN/WHT EXPONENTI
   IF NOT KEYWORD_SET(scale) THEN scale = 8
   IF NOT KEYWORD_SET(ref) THEN ref = 'DECODED'
   img_rebin = 0
   IF NOT KEYWORD_SET(full) THEN img_rebin = [hsize/2,vsize/2]

   IF keyword_set(USE_CURR_COLORS) THEN TVLCT, r, g, b
   IF keyword_set(LCT) THEN loadct,lct 
   IF keyword_set(GAMMA) THEN gamma_ct,gamma

   PIXMAPS2MPEG, mpeg_name, win_index, IMG_REBIN=img_rebin, $
                 SCALE=scale, REFERENCE=ref, PNG=png, /force_win_index

   FOR i=0, N_ELEMENTS(win_index)-1 DO WDELETE, win_index(i)
                

return
end
