;-
; $Id: showfits.pro,v 1.16 2018/03/13 19:40:59 nathan Exp $
;pro showfits,dir,h,im, XBIN=xbin, WAITSEC=waitsec, SUFFIX=suffix
; Name:     
;
; Purpose:  Monitor directory for new FITS files and display it when it arrives. 
;   	-or- Display a single FITS file image.
;
; Inputs: Directory to monitor
;   	-or- scalar or array of FITS filenames.
;
; Optional Keyword Inputs:
;   XBIN=   	Factor by which to rebin input for display (single image only)
;   WAITSEC=	Number of seconds to wait between checking for update; default=5
;   SUFFIX= 	Set to desired suffix to search for; default is "fits"
;   YWINSIZE=	Set number of pixels for display window, vertical dimension. (Default is 840 for 2 rows and 960 for 3 rows)
;    /SMALLER	Set to display 24 images instead of 8
;   USEWIN=  	Set to Window ID to display in. Default is 0.
;   ROWS=   	Display this number of rows per image; number displayed sized accordingly.
;
; Optional Outputs: 
;   h	Header structure
;   im	Image from (most recent) FITS file 
;
; Calls:    wnd.pro, mreadfits.pro, hist_equal.pro, break_file.pro
;
; Restrictions: Input directory must exist.
;
; WRitten by N.Rich, NRL
;-
; $Log: showfits.pro,v $
; Revision 1.16  2018/03/13 19:40:59  nathan
; add info on image, update wispr prints
;
; Revision 1.15  2017/03/22 08:31:40  nathan
; add /rows, utilize cgColor.pro for index display
;
; Revision 1.14  2016/11/19 05:04:15  nathan
; fix auto window size for 1 image
;
; Revision 1.13  2016/11/19 02:57:46  nathan
; add save option
;
; Revision 1.12  2016/10/24 20:04:34  nathan
; update solohi keywords
;
; Revision 1.11  2016/08/16 21:21:48  nathan
; change printed header values
;
; Revision 1.10  2016/02/08 19:34:07  nathan
; print wispr particulars
;
; Revision 1.9  2015/07/27 16:21:19  nathan
; remove requirement for input to be solohi
;
; Revision 1.8  2015/07/02 22:08:07  nathan
; add USEWIN option
;
; Revision 1.7  2015/07/02 18:45:01  nathan
; add YWINSIZE and SMALLER options
;
; Revision 1.6  2015/06/19 16:27:52  nathan
; do not display if constant value; xyout color=128; set up 3-d output array
;
; Revision 1.5  2015/05/07 16:00:34  nathan
; display up to 8 images in window
;
; Revision 1.4  2015/04/20 18:29:29  nathan
; add im,h output and print keywords
;
; Revision 1.3  2015/04/16 19:17:35  nathan
; add single image option
;
; Revision 1.2  2015/04/16 17:52:39  nathan
; already changes
;
; Revision 1.1  2015/04/16 17:50:44  nathan
; for displaying FITS files from solohi
;
pro showfitsimage, fname, xsz, ysz, h, im, i, LLC=llc, WWIN=wwin

common show, pixrows

IF datatype(i) EQ 'UND' THEN i=0

break_file,fname,di,pa,ro,sx
mreadfits,fname,h,im,/silent
sz=size(im)
IF (sz[0] eq 1) THEN im[*]=0
ypix=pixrows<(sz[2])
IF tag_exist(h,'INSTRUME') THEN BEGIN
    solohi=(h.instrume EQ 'SoloHI')
    wispr =(strmid(h.instrume,0,5) eq 'WISPR')
ENDIF

hm=histogram(im)
mdni = median(im)

IF n_elements(hm) GT 4 THEN dim=(hist_equal(im)+1)<255 ELSE dim=im
IF xsz LT 400 THEN charsz=1.5 ELSE charsz=2
IF keyword_set(LLC) THEN BEGIN
    wndfac=((float(ysz)/ypix)<(float(xsz)/sz[1]))<1.
    ; SoloHI DM special case:
    IF (solohi) THEN IF h.ccbincmd eq 61441 and h.naxis1 eq 2048 THEN im=reform(im[*,0:479],1024,960)
    x=llc[0]
    y=llc[1]
    IF (sz[0] eq 2) THEN tv,congrid(dim[*,0:ypix-1],sz[1]*wndfac,ypix*wndfac),x,y
    cgtext,x+5,y+5,trim(i),/device,charsize=charsz,color=0,charthi=2
    cgtext,x+5,y+5,trim(i)+' '+string(h.ispreg7,'(i3)')+' '+rstrmid(h.filename,0,25),/device,charsize=charsz,color=0


ENDIF ELSE BEGIN
    print,'Displaying '+ro+sx
;help,im
    mini = min(im,max=maxi)
    stdi=stddev(im)

    help,im
    print,'Max,Median,Min,Stdev:',maxi,',',mdni, ',',mini,',',stdi

    wndfac=xsz
    device,get_screen_size=ssz
    while (wndfac*sz[1] gt ssz[0] or wndfac*sz[2] gt ssz[1]) do wndfac=wndfac/2.
    	
    IF keyword_set(WWIN) THEN wid=wwin ELSE wid=0
    
    wnd,wid,dim,wndfac
ENDELSE

IF tag_exist(h,'FILENAME') THEN fn=h.filename ELSE fn=fname
per=strpos(fn,'.')
fichars=strmid(fn,per-4,4)
IF (solohi) THEN $
print,string(i,'(i3)'),'  ',rstrmid(fn,5,20),' STUDY_ID=',trim(h.study_id),' IMGTYPE=',trim(h.imgtype),' NAXIS1=',string(h.naxis1,'(i04)'),' NSUM=',trim(h.nsumexp),' MDN=',trim(mdni) $

ELSE IF (wispr) THEN $
print,string(i,'(i3)'),'  ',h.file_raw,'  ',fichars,'  ',strmid(h.date_obs,0,19), ' ',string(h.naxis1,'(i04)')+'x'+string(h.naxis2,'(i04)'),' ',string(h.study_id,'(z04)'),' ',string(h.ispreg7,'(i3)'),' BIN=',trim(fix(h.nbin)),' NSUM=',trim(h.nsumexp),' MDN=',trim(mdni) $

ELSE print,string(i,'(i3)'),'  ',fn

end

pro showfits,dir,h,im,XBIN=xbin, WAITSEC=waitsec, SUFFIX=suffix, YWINSIZE=ywinsize, SMALLER=smaller, $
    USEWIN=usewin, ROWS=rows
common show, pixrows

loadct,0
device,get_screen_size=z

IF keyword_set(SUFFIX) THEN srchstr='*.'+suffix ELSE srchstr='*.fits'
IF keyword_set(WAITSEC) THEN waittime=waitsec ELSE waittime=5
IF keyword_set(XBIN) THEN dispfac=xbin ELSE dispfac=1.
IF keyword_set(ROWS) THEN pixrows=rows ELSE pixrows=1920
IF keyword_set(USEWIN) THEN winid=usewin ELSE winid=0
n=n_elements(dir)
IF n GT 1 THEN BEGIN
; list of filenames, display 512x480 in 4x2 mosaic or 256x240 in 6x4 mosaic
; or if ROWS set, 4x(960/(rows/2)) or 6x(960/(rows/4))
   IF keyword_set(YWINSIZE) THEN ywin=ywinsize ELSE ywin=960
   IF keyword_set(SMALLER) THEN BEGIN
	xwin=1536.*ywin/960
	IF xwin GT z[0] THEN BEGIN
	    frac=float(z[0])/xwin
	    xwin=z[0]
	    ywin=fix(ywin*frac)
	ENDIF
	nrow=(ywin/(pixrows/4))>4
    	ysz=ywin/nrow
	xsz=xwin/6
    	ncol=6
    ENDIF ELSE BEGIN
	IF n_elements(dir) LE 4 THEN ncol=2 ELSE ncol=4
	xwin=ncol*512.*ywin/960
	IF xwin GT z[0] THEN BEGIN
	    frac=float(z[0])/xwin
	    xwin=z[0]
	    ywin=fix(ywin*frac)
	ENDIF
	
	nrow=(ywin/(pixrows/2))>2
    	ysz=ywin/nrow
	help,xwin
	xsz=xwin/ncol
    ENDELSE
    help,nrow
    window,winid,xsize=xwin, ysize=ywin
    textcolor=cgColor("Red",0)
    
    FOR i=0,n-1 DO BEGIN
	y= ((i/ncol) mod nrow)*ysz 
	x= (i mod ncol)*xsz
	var=''
	IF i GT 0 and x EQ 0 and y EQ 0 THEN BEGIN
	    read,'Enter to continue, s to save png, q to exit.',var
	    IF strlowcase(var) EQ 'q' or strlowcase(var) eq 'x' THEN return
	    IF strlowcase(var) eq 's' THEN x=ftvread(/png)
	    erase
	ENDIF
	;help,x,y
	showfitsimage,dir[i],xsz,ysz, hi,imi, i, LLC=[x,y]
	IF i EQ 0 THEN BEGIN
	    h=hi
	    sz=size(imi)
	    im=uintarr(sz[1],sz[2],n)
	    im[*,*,0]=imi
	ENDIF ELSE BEGIN
	    ;h=[h,hi]
	ENDELSE
	
    ENDFOR
loadct,0
    return
ENDIF

    
IF (file_test(dir,/regular)) THEN BEGIN
    showfitsimage,dir,dispfac,0,h,im
    return
ENDIF
    
fitslist=file_search(concat_dir(dir,srchstr),count=lastcount)
help,waittime
help,fitslist

while (1) do begin
    
    nlist=file_search(concat_dir(dir,srchstr), count=thiscount)
    FOR i=0,thiscount-1 DO BEGIN
    	w=where(fitslist eq nlist[i],nf)
	IF nf EQ 0 THEN BEGIN
	    showfitsimage,nlist[i],dispfac,0,h,im, WWIN=usewin
	    wait,waittime
	ENDIF
    ENDFOR
    IF lastcount EQ thiscount THEN wait,waittime
    fitslist=nlist
    lastcount=thiscount
ENDWHILE

loadct,0

 end
