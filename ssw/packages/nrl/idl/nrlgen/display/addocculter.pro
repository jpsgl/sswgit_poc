;+
;  $Id: addocculter.pro,v 1.1 2011/08/19 15:31:21 nathan Exp $
;
; PURPOSE:
;  One more prog to add an occulter in an image
;
; CATEGORY:
;   visualization, masking
;
; INPUTS:
;  im : input image
;  cntrsun : center of the sun in im pix 
;  cntrocc : center of the occulter in im pix
;  rsun : radius of the sun in im pix
;  rocc : radius of the occulter in im pix
;  nosuncont : set to disable drawing of the sun contour
;  outermask : set to the radius of the outer FOV
;
; OUTPUTS:
;  Image with the occulter 
;
;-

function addocculter,im,cntrsun,cntrocc,rsun,rocc,nosuncont=nosuncont,outermask=outermask

imsize=size(im,/dim)

; ---- sun circle mask
msksun=mkcirc(imsize[0],imsize[1],cntrsun[0],cntrsun[1],rsun*0.9)-mkcirc(imsize[0],imsize[1],cntrsun[0],cntrsun[1],rsun)
msun=where(msksun gt 0,cntsun)

; ---- occulter mask
mskocc=mkcirc(imsize[0],imsize[1],cntrocc[0],cntrocc[1],rocc)
mocc=where(mskocc gt 0,cntocc)

; -- add outer mask if requested
if n_elements(outermask) ne 0 then begin
	mskout=mkcirc(imsize[0],imsize[1],cntrocc[0],cntrocc[1],outermask)
	mout=where(mskout eq 0,cntout)
endif else cntout=0l



; -- minmax of the image
minim=min(im)
maxim=max(im)

imo=im
; ---- add mask in the images
if cntocc gt 0 then imo[mocc]=minim
if cntsun gt 0 and not keyword_set(nosuncont) then imo[msun]=maxim
if cntout gt 0 then imo[mout]=minim

return,imo
end
