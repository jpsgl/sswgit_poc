;+
;$Id: select_bytscl.pro,v 1.13 2015/05/27 19:09:02 colaninn Exp $
;
; Project     : SECCHI
;                   
; Name        : select_bytscl
;               
; Purpose     : interactively select a byte scale for an image
;               
; Explanation : this is a widget tool to selects bmin and bmax.
;                     
; Use         : IDL> byte_image=select_bytscl(image,bmin,bmax)
;
; Optional Inputs:
;   	bmin
;       bmax	
;
; output: bimage =bytescaled image
;         bmin/bmax =selected bmin/bmax
;
; KEYWORDS: noscl = disables the LOG/SQRT scale options for use with secchi movie tools	
;               
; Calls       : 
;
; Side effects: None.
;               
; Category    : Image Display.  Animation.
;               
; Written     : Lynn Simpson NRL 2010-04-19
;               
; See Also    : 
;	
;-            
;$Log: select_bytscl.pro,v $
;Revision 1.13  2015/05/27 19:09:02  colaninn
;changed variable image to image00 IDL8 compatible
;
;Revision 1.12  2012/01/03 17:16:39  mcnutt
;added label keyword
;
;Revision 1.11  2011/11/08 19:55:35  mcnutt
;sends img into save_frame if doxcolors is set
;
;Revision 1.10  2010/08/16 16:13:35  mcnutt
;move where scale is definded
;
;Revision 1.9  2010/08/12 15:18:14  mcnutt
;corrected image display size after scale
;
;Revision 1.8  2010/08/12 14:57:52  mcnutt
;added input scale and modal keyword
;
;Revision 1.7  2010/06/22 14:00:21  mcnutt
;set bmin and bmax to float
;
;Revision 1.6  2010/04/22 16:09:04  mcnutt
;added reset button
;
;Revision 1.5  2010/04/22 15:58:29  mcnutt
;adjusted window positions and added adj slider option
;
;Revision 1.4  2010/04/21 18:33:49  mcnutt
;resets window to previous window when done
;
;Revision 1.3  2010/04/20 15:34:15  mcnutt
;added keyword descriptions
;
;Revision 1.2  2010/04/20 15:17:48  mcnutt
;added scale option and removed factor select
;
;Revision 1.1  2010/04/19 18:36:10  mcnutt
;new widget tool to select bmin and bmax now an option in wscc_mkmovie
;
;Revision 1.51  2010/02/02 23:51:58  nathan
;tweaked some labels
;

pro select_bytscl_event, ev
COMMON selbytscl, selbyt

   if(where(tag_names(ev) eq 'VALUE') gt -1)then input=ev.Value
   WIDGET_CONTROL, ev.id, GET_UVALUE=test
   IF (DATATYPE(test) EQ 'STR') THEN input = test
   if input ne '' then begin
    CASE (input) OF

        'BMINT' : BEGIN 
                selbyt.bmin=ev.value
                WIDGET_CONTROL, selbyt.slidemin, SET_VALUE=selbyt.bmin;/selbyt.factor
	 END
        'SLIDE_MIN' : BEGIN 
                WIDGET_CONTROL, selbyt.slidemin, GET_VALUE=val
                selbyt.bmin=val;*selbyt.factor
                WIDGET_CONTROL, selbyt.bmint, SET_VALUE=strtrim(selbyt.bmin,2)

	 END
        'BMAXT' : BEGIN 
                selbyt.bmax=ev.value
                WIDGET_CONTROL, selbyt.slidemax, SET_VALUE=selbyt.bmax;/selbyt.factor
	 END
        'SLIDE_MAX' : BEGIN 
                WIDGET_CONTROL, selbyt.slidemax, GET_VALUE=val
                selbyt.bmax=val;*selbyt.factor
                WIDGET_CONTROL, selbyt.bmaxt, SET_VALUE=strtrim(selbyt.bmax,2)
	 END
        'SCALET' : begin
	         selbyt.scale=selbyt.scales(ev.index)  
                 IF selbyt.scale eq 'LOG' THEN image00=ALOG10(selbyt.image+3)>0 else $
                     IF selbyt.scale eq 'SQRT' THEN image00 = SQRT((selbyt.image)>0) else image00=selbyt.image
                 tmin=min(image00,/nan,max=tmax)
                 tmed=median(image00)
                 widget_control,selbyt.slidemax,set_value=[tmax,tmin,tmax]
                 widget_control,selbyt.slidemin,set_value=[tmin,tmin,tmax]
                 WIDGET_CONTROL, selbyt.slidemax, GET_VALUE=val
                 selbyt.bmax=val;*selbyt.factor
                 WIDGET_CONTROL, selbyt.slidemin, GET_VALUE=val
                 selbyt.bmin=val;*selbyt.factor
                 WIDGET_CONTROL, selbyt.bmint, SET_VALUE=strtrim(selbyt.bmin,2)
                 WIDGET_CONTROL, selbyt.bmaxt, SET_VALUE=strtrim(selbyt.bmax,2)
                 WIDGET_CONTROL, selbyt.info, SET_VALUE='max='+strtrim(tmax,2)+'  med='+strtrim(tmed,2)+' min='+strtrim(tmin,2)
	 END
        'ADJSLIDER' : begin
                 widget_control,selbyt.slidemax,set_value=[selbyt.bmax,selbyt.bmin,selbyt.bmax]
                 widget_control,selbyt.slidemin,set_value=[selbyt.bmin,selbyt.bmin,selbyt.bmax]
	 END
        'RESET' : begin
                IF selbyt.scale eq 'LOG' THEN image00=ALOG10(selbyt.image+3)>0 else $
                     IF selbyt.scale eq 'SQRT' THEN image00 = SQRT((selbyt.image)>0) else image00=selbyt.image
                 tmin=min(image00,/nan,max=tmax)
                 widget_control,selbyt.slidemax,set_value=[selbyt.bmax,tmin,tmax]
                 widget_control,selbyt.slidemin,set_value=[selbyt.bmin,tmin,tmax]
	 END

	'DONE' : BEGIN       ;** exit program
 	         WIDGET_CONTROL, /DESTROY, selbyt.selbase
 	         WIDGET_CONTROL, /DESTROY, selbyt.groupleader
		 wdelete,selbyt.selwin
                 return
	 END

   ENDCASE
  endif
   wset,selbyt.selwin
   IF selbyt.scale eq 'LOG' THEN image00=ALOG10(selbyt.image+3)>0 else $
                 IF selbyt.scale eq 'SQRT' THEN image00 = SQRT((selbyt.image)>0) else image00=selbyt.image
   tv,BYTE(((image00 > selbyt.bmin < selbyt.bmax)-selbyt.bmin)*(!D.TABLE_SIZE-1)/(selbyt.bmax-selbyt.bmin))
   if selbyt.label ne '' then xyouts,5,5,selbyt.label,color=255,charsize=2,charthick=2,/device
 


END

function select_bytscl,image_in,bmin,bmax,scale,noscl=noscl,modal=modal,label=label
COMMON selbytscl, selbyt

device,get_screen_size=ss
if total(ss) lt 2048 then xf1=512 else if total(ss) lt 4096 then xf1=1024 else xf1=2048
if n_elements(image_in(*,0)) gt ss(0) or  n_elements(image_in(0,*)) gt ss(1) then xf=n_elements(image_in(*,0))/xf1 else xf=1
if xf gt 1 then image00=congrid(image_in,n_elements(image_in(*,0))/xf,n_elements(image_in(0,*))/xf) else image00=image_in

tmin=min(image00,/nan,max=tmax)
tmed=median(image00)

if n_params() lt 2 then begin
  bmin=tmin & bmax=tmax
endif
if n_params() lt 3 then begin
  scale='Linear'
endif

if datatype(bmin) eq 'UND' then bmin=tmin
if datatype(bmax) eq 'UND' then bmax=tmax
scales=['Linear','LOG','SQRT']
if datatype(scale) eq 'UND' then scale=scales(0) 
if datatype(scale) ne 'STR' then scale=scales(scale)

bmin=float(bmin) & bmax=float(bmax)
curwin=!d.window

device,retain=2
WINDOW, XSIZE = n_elements(image00(*,0)), YSIZE = n_elements(image00(0,*)), /FREE,xpos=ss(0)-n_elements(image00(*,0)),ypos=ss(1)-n_elements(image00(0,*))
       selwin = !D.WINDOW
       IF scale eq 'LOG' THEN image00=ALOG10(image00+3)>0 else $
                 IF scale eq 'SQRT' THEN image00 = SQRT((image_in)>0) else image00=image00
       bimage= BYTE(((image00 > bmin < bmax)-bmin)*(!D.TABLE_SIZE-1)/(bmax-bmin))
       tv,bimage
       if keyword_set(label) then begin
          xyouts,5,5,label,color=255,charsize=2,charthick=2,/device
          label=label
       endif else label=''

groupleader = Widget_Base(Map=0)
Widget_Control, groupleader, /Realize
selbase = WIDGET_BASE(TITLE='Byte Scale select',/COLUMN,xsize=525,ysize=175,xoffset=ss(0)-n_elements(image00(*,0))-525>0,yoffset=ss(1)-n_elements(image00(0,*))-175>0,modal=keyword_set(modal),group_leader=groupleader)
    selrowa = WIDGET_BASE(selbase, /ROW)

        tmp = WIDGET_LABEL(selrowa, VALUE='Image before bytscl ')
        info = widget_text(selrowa, VALUE='max='+strtrim(tmax,2)+'  med='+strtrim(tmed,2)+' min='+strtrim(tmin,2), XSIZE=40, YSIZE=1, UVALUE='infoT')
        if ~keyword_set(noscl) then begin
    	  bscale = widget_droplist(selrowa, VALUE=scales, UVALUE='SCALET',title='Scale')
          WIDGET_CONTROL, bscale, SET_DROPLIST_SELECT=where(scales eq scale)
        endif
    selrow1 = WIDGET_BASE(selbase, /ROW)
    	bmint = CW_FIELD(selrow1, VALUE=strtrim(bmin,2), XSIZE=10, YSIZE=1, title='BMin', UVALUE='BMINT', /ALL_EVENTS)

        slidemin = CW_FSLIDER(selrow1,  VALUE=bmin, UVALUE='SLIDE_MIN', $
             /DRAG, /SCROLL, MIN=tmin, MAX=tmax,xsize=350,/SUPPRESS_VALUE)

    selrow2 = WIDGET_BASE(selbase, /ROW)
    	bmaxt = CW_FIELD(selrow2, VALUE=strtrim(bmax,2), XSIZE=10, YSIZE=1, title='BMax', UVALUE='BMAXT',  /ALL_EVENTS)
        slidemax = CW_FSLIDER(selrow2,  VALUE=bmax, UVALUE='SLIDE_MAX', $
             /DRAG, /SCROLL, MIN=tmin, MAX=tmax,xsize=350,/SUPPRESS_VALUE )


    selrow3 = WIDGET_BASE(selbase, /ROW)
	tmp=  CW_BGROUP(selrow3,['  Adjust Slider to current bmin and bmax   ',' RESET ',' DONE '],BUTTON_UVALUE = ['ADJSLIDER','RESET','DONE'],/ROW)

   selbyt={bmin:bmin,bmax:bmax,selwin:selwin,bmint:bmint,bmaxt:bmaxt,slidemin:slidemin,slidemax:slidemax,image:image00,selbase:selbase,$
    	    scale:scale,scales:scales, groupleader:groupleader, info:info, label:label}


  WIDGET_CONTROL, /REALIZE, selbase
  WIDGET_CONTROL, selbase, /SHOW
  XMANAGER, 'select_bytscl', selbase, EVENT_HANDLER='select_bytscl_event'

  wset,curwin
  wdel,selbyt.selwin
  bmin=selbyt.bmin
  bmax=selbyt.bmax
  scale=selbyt.scale
   IF selbyt.scale eq 'LOG' THEN image00=ALOG10(image_in+3)>0 else $
                 IF selbyt.scale eq 'SQRT' THEN image00 = SQRT((image_in)>0) else image00=image_in
  bimage= BYTE(((image00 > bmin < bmax)-bmin)*(!D.TABLE_SIZE-1)/(bmax-bmin))
return,bimage

END
