
FUNCTION hdrsinfo, nfiles, MEM = mem
; $Id: hdrsinfo.pro,v 1.1 2008/02/15 22:47:55 nathan Exp $
;
; $Log: hdrsinfo.pro,v $
; Revision 1.1  2008/02/15 22:47:55  nathan
; moved from lasco/idl/display because called by scclister.pro
;
@scchandle.com

IF N_ELEMENTS(nfiles) EQ 0 THEN RETURN,-1

n = N_ELEMENTS(nfiles)
hinfo = STRARR(n)

IF NOT KEYWORD_SET(mem) THEN mem = 1

CASE mem OF

1: BEGIN

     FOR i=0,n-1 DO BEGIN
       ghandle,i
       hinfo(i) = nfiles(i)+ $
        "  " + h.date_o + $
        "  " + h.time_o + $
        STRING(STRCOMPRESS(h.detect,/REMOVE_ALL),format='(a5)') + $
        STRING(STRCOMPRESS(h.exptime,/REMOVE_ALL),format='(a10)') + $
        " " + h.filter + $
        " " + h.polar + $
        STRING(STRCOMPRESS(h.wavel,/REMOVE_ALL),format='(a8)') + $
        (STRING(h.naxis1)) + $
        (STRING(h.naxis2)) + $
        STRING(STRCOMPRESS(h.r1col,/REMOVE_ALL),format='(a6)') + $
        STRING(STRCOMPRESS(h.r2col,/REMOVE_ALL),format='(a6)') + $
        STRING(STRCOMPRESS(h.sumrow,/REMOVE_ALL),format='(a4)') + $
        STRING(STRCOMPRESS(h.sumcol,/REMOVE_ALL),format='(a4)') 
    
     ENDFOR
    END
ELSE:
ENDCASE

RETURN,hinfo
END
