;+
pro viewlist, list, minv, maxv, DELAY=delay, LOOP=loop, PAN=pan, HALT=halt, $
    RDIFF=rdiff, DIFF=diff, SHOW_KEYWORD=show_keyword, HISTEQUAL=histequal, $
    MAXMIN=maxmin, CENTERS=centers, _EXTRA=_extra
;
; View a list of image files, loading 1 at a time.
;
; INPUT:
;  list		STRARR, list of files -or- datacube[*,*,n]
;
; OPTIONAL INPUT:
;  minv     min scale value
;  maxv     max scale value
;
; KEYWORDS:
;  DELAY	Set to number of seconds between frames
;  LOOP		Repeat display of sequence; if =1, then indefinitely
;  PAN		rebin/congrid by a factor or fraction
;  HALT		advance images with '.cont' at command line
;  RDIFF    	Running difference images
;  DIFF=n     	Difference with the nth image in list (first is default)
;  SHOW_KEYWORD Print value of given FITS keyword before displaying each image
;  HISTEQUAL	Display with histogram equalization
;  MAXMIN   	Print image statistics
;  CENTERS= 	Returns fltarr(2,n) with center determined by method of chords
;
; Written 11Jul2002 by N. Rich, NRL/Interferometrics
;
; 04.01.21, nbr - Add PAN
; 04.03.25, nbr - Add Halt
; 04.10.18, nbr - add diff and rdiff
; 07.08.29, nbr - Fix rdiff
;
;  @(#)viewlist.pro	1.4 08/29/07  LASCO NRL IDL Library
;
;-

sli=size(list)
IF sli[0] GT 2 THEN BEGIN
; input is a data cube
    n=sli[3]
    ext='data'
ENDIF ELSE BEGIN
    n=n_elements(list)

    break_file,list[0], lo, dir, root, ext
ENDELSE

IF keyword_set(LOOP) THEN looping=loop ELSE looping=1
IF keyword_set(PAN) THEN factor=pan ELSE factor=1
first=1
sliding=0
lastx=0
lasty=0
IF keyword_set(CENTERS) THEN centers=fltarr(2,n)

IF keyword_set(DIFF) THEN BEGIN
    	IF (diff eq 1) THEN diffi=0 ELSE diffi=diff
	IF ext EQ 'data' THEN diffim=reform(list[*,*,diffi]) ELSE $
   	print,'Reading diff image ',list[diffi]
	IF ext EQ '.tif' THEN BEGIN
		diffim = read_tiff(list[diffi],r,g,b, ORDER=ord)
		IF ord THEN diffim = rotate(diffim,7)
		IF first THEN tvlct,r,g,b
		ENDIF
	IF ext EQ '.fts' or ext EQ '.fit' or ext EQ '.fits' THEN BEGIN
		diffim = READFITS(list[diffi],hdr)
		ENDIF
	
ENDIF
WHILE looping DO BEGIN
   for i=0,n-1 do begin
    	IF ext EQ 'data' THEN im=reform(list[*,*,i]) ELSE $
   	print,'Reading ',list[i]
	IF ext EQ '.tif' THEN BEGIN
		im = read_tiff(list[i],r,g,b, ORDER=ord)
		IF ord THEN im = rotate(im,7)
		IF first THEN tvlct,r,g,b
		ENDIF
	IF ext EQ '.fts' or ext EQ '.fit' or ext EQ '.fits' THEN BEGIN
		im = READFITS(list[i],hdr)
		;nim = bytscl(im,0.7*mdn, 1.6*mdn)
		IF keyword_set(SHOW_KEYWORD) THEN BEGIN
		    print,' '
		    print, strupcase(show_keyword),": ",fxpar(hdr,show_keyword)
		    print,' '
		ENDIF
		ltz = where(im LT 0,nltz)
		IF nltz GT 1 THEN BEGIN
		    im=long(im)
		    im[ltz]=im[ltz] +32768 +32767
		ENDIF
	ENDIF
	;IF first THEN BEGIN
	IF keyword_set(MAXMIN) THEN maxmin,im
	sz=size(im)
	xsize=fix(sz[1]*factor)
	ysize=fix(sz[2]*factor)
        IF xsize LE 0 or ysize LE 0 THEN goto, skipit
	device,get_screen_size=ss
	;if (xsize gt 0.9*ss(0) OR ysize gt 0.9*ss(1)) THEN BEGIN
	;	slide_imagef, full_window=fwid, slide_window=swid, $
	;	xvis=(1000<xsize), yvis=(1000<ysize), $
	;	xsize=xsize, ysize=ysize
	;	sliding=1
	;ENDIF ELSE $
        IF xsize NE lastx or ysize NE lasty THEN window,xsize=xsize,ysize=ysize
	;ENDIF
	lastx=xsize
	lasty=ysize
	IF (first) THEN lastim=im
	IF n_params() LT 3 THEN BEGIN
	    nz = where(im gt 0,nnz)
	    if nnz gt 1 THEN BEGIN
	    	mdn = median(im[nz])
		minv=0.7*mdn
    	    	maxv=1.6*mdn
	    ENDIF ELSE BEGIN
	    	minv=min(im)
		maxv=max(im)
	    ENDELSE
	ENDIF
	IF keyword_set(DIFF)  THEN nim = float(im)-diffim ELSE $
	IF keyword_set(RDIFF) THEN nim = float(im)-lastim ELSE $
	nim=im
	lastim=im
	IF keyword_set(HIST_EQUAL) THEN nim = hist_equal(temporary(nim)) $
	ELSE nim = bytscl(nim,minv,maxv)
	IF sz[1] mod xsize EQ 0 THEN nim = rebin((nim),xsize,ysize) $
		ELSE nim = congrid((nim),xsize,ysize)
		
	IF sliding THEN BEGIN
		wset,fwid
		tv,congrid(nim,256,256)
		wset,swid
	ENDIF
	tv,nim
        
	IF keyword_set(CENTERS) THEN BEGIN
	    	IF sz[1] mod xsize EQ 0 THEN im = rebin((im),xsize,ysize) $
		ELSE im = congrid((im),xsize,ysize)
    	    	find_chord_ctr,im,xc,yc,/interp,_EXTRA=_extra
		centers[0,i]=xc
		centers[1,i]=yc
	ENDIF
	first=0
        skipit:
	IF keyword_set(DELAY) THEN wait, delay
	IF keyword_set(HALT) THEN stop
   endfor

   IF keyword_set(LOOP) THEN BEGIN
	IF loop EQ 1 THEN looping=1 
   ENDIF ELSE looping=looping-1

ENDWHILE

end
