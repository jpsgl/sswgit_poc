;-
; $Id: tvfit.pro,v 1.2 2009/09/01 19:12:24 nathan Exp $
; Name:     
;
; Purpose:  tvscl im into existing window, NaN=0 
;
; Inputs: any [n1,n2] or [1,n1,n2] number array
;
; Outputs:  none
;
; Restrictions: currently requires window dimensions to be integer factor of input dimensions
;
; WRitten by N.Rich, NRL/I2
;-
; $Log: tvfit.pro,v $
; Revision 1.2  2009/09/01 19:12:24  nathan
; should now work with any size window
;
; Revision 1.1  2009/08/14 19:44:41  nathan
; i would have used $SSW/yohkoh/ucon/idl/shimizu/tvf.pro but it does not handle NaN
;

pro tvfit, im0

im=reform(im0)
sim=size(im)
x0=sim[1]
y0=sim[2]
ar=float(x0)/y0
xd=!d.x_size
yd=!d.y_size

;--Rebin to fit into existing window
xr=float(x0)/xd
yr=float(y0)/yd
fctr=round(xr>yr)
while (x0 mod fctr GT 0) or (y0 mod fctr GT 0) do fctr=fctr+1
;
im1=rebin(im,x0/fctr,y0/fctr)
wm=where_missing(im1)
IF wm[0] GE 0 THEN im1[wm]=0.

tvscl,im1
 end
