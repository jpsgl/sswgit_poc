;+
pro oplotvtime,tm,vals, label, MDY=mdy, COLOR=color, _Extra=_extra

;; $Id: oplotvtime.pro,v 1.3 2010/12/09 22:41:12 nathan Exp $
; Name:     oplotvtime
;
; Purpose:  Generic routine for plotting value vs. time and labeling value
;   	    on an already existing plot created with utplot.pro
;
; Inputs:   tm	Time array for utplot
;   	    vals    what to plot vs. time
;   	    label   string to label vals with
;
; Optional input keywords:
;   	    Any oplot keywords
;
; Outputs:  none - plots values in window
;
; Common blocks:    timeplot	
;
; WRitten by N.Rich, NRL/I2
;-
; $Log: oplotvtime.pro,v $
; Revision 1.3  2010/12/09 22:41:12  nathan
; make more robust
;
; Revision 1.2  2009/10/27 19:48:00  nathan
; fix COLOR keyword and anytim2utc call
;
; Revision 1.1  2009/04/13 20:31:44  nathan
; subroutines created for plotstereotemps.pro
;


common timeplot, keyx, keyy, clr

IF keyword_set(COLOR) THEN clr=color

ptype='time'
IF n_params() LT 3 THEN BEGIN
    ; see if this is a not-time plot
    IF datatype(vals) EQ 'STR' THEN BEGIN
    	ptype='xy' 
	label=vals
	vals=tm
    ENDIF ELSE BEGIN
    	label=''
    	read,'Please enter a label to identify data values',label
    ENDELSE
ENDIF


ymin=min(vals,max=ymax)

IF ptype EQ 'time' THEN BEGIN
    ud=anytim2utc(tm,_EXTRA=_extra)
    outplot,ud,vals,color=clr, _Extra=_extra
ENDIF ELSE oplot,vals,color=clr, _Extra=_extra
    
xyouts,keyx,keyy,strupcase(label),color=clr,/device,size=2

; increment for next plot with oplotvtime.pro
clr=(clr+1)>3
keyx=keyx+strlen(label)*13+10

IF keyx GT !d.x_size-(strlen(label)*13+10) THEN BEGIN
    keyx=5
    keyy=keyy+18
ENDIF

end
