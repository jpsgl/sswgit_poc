;$Id: annotate_image.pro,v 1.25 2011/12/01 20:56:46 nathan Exp $
;
; Project     : SECCHI/LASCO
;                   
; Name        : annotate_image
;               
; Purpose     : widget tool to annotate a movie or image
;               
; Explanation : 
;
; Use         : annotate_image,win_index,/true
;
;
; Optional Inputs:
;   	    	win_index can be a filename or window index from another program;
;   	    	if undefined pickfile is called to locate input file
;   	
;
; KEYWORDS: 	/TRUECOLOR use true color format
;               IMG= to pass image(s) in from other programs
;               /TEMPLATE creates textframe stored in WSCC_MKMOVIE_COMMON used to add text when creating a movie
;               AHDRS=	array of header structures from MVI or FITS file
;   	    	/DEBUG	print debug messages
;
; Calls       : scc_movieout.pro
;
; Side effects: will write over an existing movie if name is the same
;               
; Category    : Image Display.  Animation.
;               
; Written     : Lynn Simpson, NRL June 2009.
;               
;	
;-            
;$Log: annotate_image.pro,v $
;Revision 1.25  2011/12/01 20:56:46  nathan
;use select_windows for windows compat
;
;Revision 1.24  2011/04/21 18:54:35  nathan
;re-congfigure bottom two rows to be more intuitive
;
;Revision 1.23  2010/12/09 22:40:20  nathan
;documentation
;
;Revision 1.22  2010/01/26 12:24:41  mcnutt
;added check to see if image read in is truecolor
;
;Revision 1.21  2010/01/21 20:45:00  nathan
;fix case where filename in fstate.hdrs is undefined; adjust criteria for MVI Options
;
;Revision 1.20  2009/12/08 20:42:44  nathan
;Allow add-times for any case where frame hdrs have time; remove .fff from default timestamp
;
;Revision 1.19  2009/12/04 22:06:23  nathan
;fix save frame filename
;
;Revision 1.18  2009/11/18 19:32:08  mcnutt
;uses date_obs for time is time_obs is blank (fts hdr being used from scc_mkmovie)
;
;Revision 1.17  2009/11/17 18:33:24  mcnutt
;shows status in text box when adding annoations
;
;Revision 1.16  2009/11/16 18:35:52  mcnutt
;corrected image output type label
;
;Revision 1.15  2009/10/29 16:01:46  nathan
;re-do last two rows on widget
;
;Revision 1.14  2009/10/21 12:11:51  mcnutt
;corrected single image msg
;
;Revision 1.13  2009/10/20 19:01:12  mcnutt
;removed extra issecchi lines
;
;Revision 1.12  2009/10/20 16:25:13  mcnutt
;moved save buttons and added notification if annotation not applied
;
;Revision 1.11  2009/10/20 15:37:02  mcnutt
;added SECCHI logo and time stamp options
;
;Revision 1.10  2009/10/19 22:28:08  nathan
;Started to implement scheme to dynamically compute width of control part of
;widget by computing width of each row; still need to determine margins of
;various widgets and apply result to width of image display window.
;
;Revision 1.9  2009/10/19 19:13:42  mcnutt
;adjusted widget size for larger movies
;
;Revision 1.8  2009/09/24 19:55:16  mcnutt
;removed second definition of comman block WSCC_MKMOVIE_COMMON
;
;Revision 1.7  2009/09/24 19:52:06  mcnutt
;defines textframe when called with keyword template
;
;Revision 1.6  2009/09/24 19:13:55  nathan
;fix length of frame slider
;
;Revision 1.5  2009/08/25 17:00:19  mcnutt
;corrected if statment for file_hdr type
;
;Revision 1.4  2009/07/20 22:09:11  nathan
;tweak comments
;
;Revision 1.3  2009/07/20 16:50:24  mcnutt
;add help buttom and added keyword free to showfont window
;
;Revision 1.2  2009/06/25 21:06:52  nathan
;Adjusted some text and layout of widget; added HELP function which still
;needs a button.
;
;Revision 1.1  2009/06/25 17:30:44  mcnutt
;new widget to annotate movie and images
;
;
pro annotate_image_preview,add=add
COMMON annotate_image_common, fontbase, groupleader, fonttext, fontarrow
COMMON WSCC_MKMOVIE_COMMON, swmoviev, debug_on, rgb, imgs, coordsab, findx, textframe
    WIDGET_CONTROL, fontbase, GET_UVALUE=fstate   ; get structure from UVALUE
;    stemp=size(textframe)
    timg=-1
;    SET_PLOT,'z'
;    DEVICE,SET_RESOLUTION=[fstate.xsz,fstate.ysz]
    device,get_decomposed=decom
    device,decomposed=1
    if fstate.txt ne '' then begin
       wset,fstate.txt_win
       timg=bytarr(fstate.xsz,fstate.ysz)+0
       tv,timg
       if fstate.bkgrd(0) ne -1 or (fstate.bkgrd(0) eq -1 and fstate.ufont eq 0) then orn=0 else orn=fstate.orn
       if fstate.ufont ne 0 then txt=fstate.bfnt+fstate.txt+'!X' else txt=fstate.txt
       xyouts,fstate.evx,fstate.evy,txt,font=fstate.ufont,charsize=fstate.sze,color=255,alignment=fstate.alg,orientation=orn,/device
       timg=tvrd()
       if (fstate.bkgrd(0) eq -1 and fstate.ufont eq 0) then timg = ROT(timg, fstate.orn*(-1), 1,fstate.evx,fstate.evy,/pivot)
       block=where(timg eq 255)
       if fstate.bkgrd(0) ne -1 and block(0) ne -1 then begin
          arr_ind=array_indices(timg,block)
          x2=max(arr_ind[0,*]+5) <fstate.xsz-1
          x1=min(arr_ind[0,*]-5) >0
          y2=max(arr_ind[1,*]+5) <fstate.ysz-1
          y1=min(arr_ind[1,*]-5) >0
          timg(x1:x2,y1:y2)=128
          timg(block)=255
          if (fstate.orn ne 0 and fstate.orn ne 360) then begin
            if fstate.alg eq 1 then xyp=[x2,y1] else if fstate.alg eq .5 then xyp=[x1+(x2-x1)/2,y1] else xyp=[x1,y1]
            timg = ROT(timg, fstate.orn*(-1), 1,xyp(0),xyp(1),/pivot)
          endif
        endif
        tv,timg
    endif
    if fstate.arrsel gt 0 and total(fstate.arxy(0:3)) gt 0 then begin
       wset,fstate.txt_win
       aimg=bytarr(fstate.xsz,fstate.ysz)+0
       tv,aimg
       ARROW,fstate.arxy(0),fstate.arxy(1),fstate.arxy(2),fstate.arxy(3),color=255,hsize=fstate.arxy(5),thick=fstate.arxy(4),/solid
       aimg=tvrd()
       aimg(where(aimg eq 255))=200
       if timg(0) ne -1 then timg(where(aimg eq 200))=200 else timg=aimg
    endif

;    SET_PLOT,'x'
    device,decomposed=decom

    if keyword_set(add) then xyframes=[fstate.f1,fstate.f2] else xyframes=[fstate.current,fstate.current]

    for frm=xyframes(0),xyframes(1) do begin
        if keyword_set(add) then widget_control,fstate.wtxt,set_value='******adding text to frame #'+string(frm)+'******'
	WSET,fstate.win_index(frm)
        if fstate.len gt 1 and fstate.true eq 1 then ttimg=imgs(*,*,*,frm) else $
              if fstate.len gt 1 and fstate.true eq 0 then ttimg=imgs(*,*,frm) else ttimg=imgs
        if frm ge fstate.f1 and frm le fstate.f2 then begin
         ck=where(timg eq 255)
	 if fstate.txt ne '' and ck(0) ne -1 then begin
            if fstate.true eq 0 then begin
               ttimg(where(timg eq 255))=fix(fstate.clr)
	       if fstate.bkgrd(0) ne -1 then ttimg(where(timg eq 128))=fix(fstate.bkgrd)
               if datatype(textframe) ne 'UND' and keyword_set(add) then begin
                   bimg=reform(textframe(0,*,*))
	           bimg(where(timg eq 255))=fix(fstate.clr)
                   if  fstate.bkgrd(0) ne -1 then bimg(where(timg eq 128))=fix(fstate.bkgrd)
		   textframe(0,*,*)=bimg
                   bimg=reform(textframe(1,*,*))
	           bimg(where(timg eq 255))=255
                   if  fstate.bkgrd(0) ne -1 then bimg(where(timg eq 128))=255
		   textframe(1,*,*)=bimg
              endif
               if  fstate.bkgrd(0) ne -1 and  datatype(textframe) ne 'UND'  and keyword_set(add) then begin
	           textframe(0,where(timg eq 128))=fix(fstate.bkgrd)
	           textframe(1,where(timg eq 128))=255
              endif
            endif
            if fstate.true eq 1 then begin
	         for nc = 0,2 do begin
                    bimg=reform(ttimg(nc,*,*))
		    bimg(where(timg eq 255))=fix(fstate.clr(nc))
		    ttimg(nc,*,*)=bimg
                 endfor
                 if  datatype(textframe) ne 'UND'  and keyword_set(add) then for nc = 0,3 do begin
                    bimg=reform(textframe(nc,*,*))
		    if nc lt 3 then bimg(where(timg eq 255))=fix(fstate.clr(nc)) else  bimg(where(timg eq 255))=255
		    textframe(nc,*,*)=bimg
                 endfor
	         if fstate.bkgrd(0) ne -1 then for nc = 0,2 do begin
                    bimg=reform(ttimg(nc,*,*))
		    bimg(where(timg eq 128))=fix(fstate.bkgrd(nc))
		    ttimg(nc,*,*)=bimg
                 endfor
                 if fstate.bkgrd(0) ne -1 and   datatype(textframe) ne 'UND'  and keyword_set(add) then for nc = 0,3 do begin
                    bimg=reform(textframe(nc,*,*))
		    if nc lt 3 then bimg(where(timg eq 128))=fix(fstate.bkgrd(nc)) else  bimg(where(timg eq 128))=255
		    textframe(nc,*,*)=bimg
                 endfor
	    endif
	  endif
	 if fstate.arrsel gt 0 then begin
            ck=where(timg eq 200)
            if ck(0) ne -1 then begin ;no error when select is setected with zero values
              if fstate.true eq 0 then ttimg(where(timg eq 200))=fix(fstate.arrclr)
              if fstate.true eq 0 and  datatype(textframe) ne 'UND'  and keyword_set(add) then begin
                   bimg=reform(textframe(0,*,*))
	           bimg(where(timg eq 200))=fix(fstate.arrclr)
		   textframe(0,*,*)=bimg
                   bimg=reform(textframe(1,*,*))
	           bimg(where(timg eq 200))=255
		   textframe(1,*,*)=bimg
              endif
              if fstate.true eq 1 then for nc = 0,2 do begin
                    bimg=reform(ttimg(nc,*,*))
		    bimg(where(timg eq 200))=fix(fstate.arrclr(nc))
		    ttimg(nc,*,*)=bimg
                 endfor
              if fstate.true eq 1 and   datatype(textframe) ne 'UND'  and keyword_set(add) then for nc = 0,3 do begin
                    bimg=reform(textframe(nc,*,*))
		    if nc lt 3 then bimg(where(timg eq 200))=fix(fstate.arrclr(nc)) else  bimg(where(timg eq 200))=255
		    textframe(nc,*,*)=bimg
                 endfor
	    endif
	  endif

          tv,ttimg,true=fstate.true
          if keyword_set(add) and fstate.true eq 1 then imgs(*,*,*,frm)=ttimg
          if keyword_set(add) and fstate.true eq 0 then imgs(*,*,frm)=ttimg

          if frm eq fstate.current then begin
             WSET,fstate.draw_win
	     DEVICE, COPY = [0, 0, fstate.xsz, fstate.ysz, 0, 0, fstate.win_index(frm)]
          endif
        endif else begin
           tv,ttimg,true=fstate.true
           WSET,fstate.draw_win
	   DEVICE, COPY = [0, 0, fstate.xsz, fstate.ysz, 0, 0, fstate.win_index(fstate.current)]
	endelse
	
     endfor
end

pro annotate_add_times,add=add,logo=logo
COMMON annotate_image_common, fontbase, groupleader, fonttext, fontarrow
COMMON WSCC_MKMOVIE_COMMON, swmoviev, debug_on, rgb, imgs, coordsab, findx, textframe
    WIDGET_CONTROL, fontbase, GET_UVALUE=fstate   ; get structure from UVALUE
;    stemp=size(textframe)
    timg=-1
;    SET_PLOT,'z'
;    DEVICE,SET_RESOLUTION=[fstate.xsz,fstate.ysz]
    device,get_decomposed=decom
    device,decomposed=1

    if keyword_set(logo) then begin
          timg=bytarr(fstate.xsz,fstate.ysz)+0
          if fix(fstate.sze) lt 2 then tlogo=fstate.logosm else if fix(fstate.sze) eq 2 then tlogo=fstate.logomd else tlogo=fstate.logo 
          x2=fstate.evx+n_elements(tlogo(*,0)) < fstate.xsz-1 > n_elements(tlogo(*,0))-1
          x1=x2-n_elements(tlogo(*,0))+1
          y2=fstate.evy+n_elements(tlogo(0,*)) < fstate.ysz-1 > n_elements(tlogo(0,*))-1
          y1=y2-n_elements(tlogo(0,*))+1
          timg(x1:x2,y1:y2)=tlogo
    endif

    if keyword_set(add) then xyframes=[fstate.f1,fstate.f2] else xyframes=[fstate.current,fstate.current]
    for frm=xyframes(0),xyframes(1) do begin
;     if fstate.txt ne '' then begin
       if keyword_set(add) then widget_control,fstate.wtxt,set_value='******adding text to frame #'+string(frm)+'******'
       wset,fstate.txt_win
       if ~keyword_set(logo) then begin
          timestmp=fstate.txt
          if strpos(fstate.txt,'YYYY') gt -1 then strput,timestmp,strmid(fstate.hdrs(frm).date_obs,0,4),strpos(fstate.txt,'YYYY')
          if strpos(fstate.txt,'MM') gt -1 then strput,timestmp,strmid(fstate.hdrs(frm).date_obs,5,2),strpos(fstate.txt,'MM')
          if strpos(fstate.txt,'DD') gt -1 then strput,timestmp,strmid(fstate.hdrs(frm).date_obs,8,2),strpos(fstate.txt,'DD')
          if fstate.hdrs(frm).time_obs ne '' then begin
            if strpos(fstate.txt,'hh') gt -1 then strput,timestmp,strmid(fstate.hdrs(frm).time_obs,0,2),strpos(fstate.txt,'hh')
            if strpos(fstate.txt,'mm') gt -1 then strput,timestmp,strmid(fstate.hdrs(frm).time_obs,3,2),strpos(fstate.txt,'mm')
            if strpos(fstate.txt,'ss') gt -1 then strput,timestmp,strmid(fstate.hdrs(frm).time_obs,6,2),strpos(fstate.txt,'ss')
            if strpos(fstate.txt,'fff') gt -1 then strput,timestmp,strmid(fstate.hdrs(frm).time_obs,9,3),strpos(fstate.txt,'fff')
          endif else begin
            if strpos(fstate.txt,'hh') gt -1 then strput,timestmp,strmid(fstate.hdrs(frm).date_obs,11,2),strpos(fstate.txt,'hh')
            if strpos(fstate.txt,'mm') gt -1 then strput,timestmp,strmid(fstate.hdrs(frm).date_obs,14,2),strpos(fstate.txt,'mm')
            if strpos(fstate.txt,'ss') gt -1 then strput,timestmp,strmid(fstate.hdrs(frm).date_obs,17,2),strpos(fstate.txt,'ss')
            if strpos(fstate.txt,'fff') gt -1 then strput,timestmp,strmid(fstate.hdrs(frm).date_obs,20,3),strpos(fstate.txt,'fff')
 	  endelse
          timg=bytarr(fstate.xsz,fstate.ysz)+0
          tv,timg
          if fstate.bkgrd(0) ne -1 or (fstate.bkgrd(0) eq -1 and fstate.ufont eq 0) then orn=0 else orn=fstate.orn
          if fstate.ufont ne 0 then txt=fstate.bfnt+timestmp+'!X' else txt=timestmp
          xyouts,fstate.evx,fstate.evy,txt,font=fstate.ufont,charsize=fstate.sze,color=255,alignment=fstate.alg,orientation=orn,/device
          timg=tvrd()
          if (fstate.bkgrd(0) eq -1 and fstate.ufont eq 0) then timg = ROT(timg, fstate.orn*(-1), 1,fstate.evx,fstate.evy,/pivot)
       endif
       block=where(timg eq 255)
       if fstate.bkgrd(0) ne -1 and block(0) ne -1 then begin
          arr_ind=array_indices(timg,block)
          x2=max(arr_ind[0,*]+5) <fstate.xsz-1
          x1=min(arr_ind[0,*]-5) >0
          y2=max(arr_ind[1,*]+5) <fstate.ysz-1
          y1=min(arr_ind[1,*]-5) >0
          timg(x1:x2,y1:y2)=128
          timg(block)=255
          if (fstate.orn ne 0 and fstate.orn ne 360) then begin
            if fstate.alg eq 1 then xyp=[x2,y1] else if fstate.alg eq .5 then xyp=[x1+(x2-x1)/2,y1] else xyp=[x1,y1]
            timg = ROT(timg, fstate.orn*(-1), 1,xyp(0),xyp(1),/pivot)
          endif
        endif
;        tv,timg

;    SET_PLOT,'x'
    device,decomposed=decom

        WSET,fstate.win_index(frm)
        if fstate.len gt 1 and fstate.true eq 1 then ttimg=imgs(*,*,*,frm) else $
              if fstate.len gt 1 and fstate.true eq 0 then ttimg=imgs(*,*,frm) else ttimg=imgs
        if frm ge fstate.f1 and frm le fstate.f2 then begin
         ck=where(timg eq 255)
	 if fstate.txt ne '' and ck(0) ne -1 then begin
            if fstate.true eq 0 then begin
               ttimg(where(timg eq 255))=fix(fstate.clr)
	       if fstate.bkgrd(0) ne -1 then ttimg(where(timg eq 128))=fix(fstate.bkgrd)
               if datatype(textframe) ne 'UND' and keyword_set(add) then begin
                   bimg=reform(textframe(0,*,*))
	           bimg(where(timg eq 255))=fix(fstate.clr)
                   if  fstate.bkgrd(0) ne -1 then bimg(where(timg eq 128))=fix(fstate.bkgrd)
		   textframe(0,*,*)=bimg
                   bimg=reform(textframe(1,*,*))
	           bimg(where(timg eq 255))=255
                   if  fstate.bkgrd(0) ne -1 then bimg(where(timg eq 128))=255
		   textframe(1,*,*)=bimg
              endif
               if  fstate.bkgrd(0) ne -1 and  datatype(textframe) ne 'UND'  and keyword_set(add) then begin
	           textframe(0,where(timg eq 128))=fix(fstate.bkgrd)
	           textframe(1,where(timg eq 128))=255
              endif
            endif
            if fstate.true eq 1 then begin
	         for nc = 0,2 do begin
                    bimg=reform(ttimg(nc,*,*))
		    bimg(where(timg eq 255))=fix(fstate.clr(nc))
		    ttimg(nc,*,*)=bimg
                 endfor
		 
                 if  datatype(textframe) ne 'UND'  and keyword_set(add) then for nc = 0,3 do begin
                    help,textframe
		    bimg=reform(textframe(nc,*,*))
		    if nc lt 3 then bimg(where(timg eq 255))=fix(fstate.clr(nc)) else  bimg(where(timg eq 255))=255
		    textframe(nc,*,*)=bimg
                 endfor
	         if fstate.bkgrd(0) ne -1 then for nc = 0,2 do begin
                    bimg=reform(ttimg(nc,*,*))
		    bimg(where(timg eq 128))=fix(fstate.bkgrd(nc))
		    ttimg(nc,*,*)=bimg
                 endfor
                 if fstate.bkgrd(0) ne -1 and   datatype(textframe) ne 'UND'  and keyword_set(add) then for nc = 0,3 do begin
                    bimg=reform(textframe(nc,*,*))
		    if nc lt 3 then bimg(where(timg eq 128))=fix(fstate.bkgrd(nc)) else  bimg(where(timg eq 128))=255
		    textframe(nc,*,*)=bimg
                 endfor
	    endif
	  endif

          tv,ttimg,true=fstate.true
          if keyword_set(add) and fstate.true eq 1 then imgs(*,*,*,frm)=ttimg
          if keyword_set(add) and fstate.true eq 0 then imgs(*,*,frm)=ttimg

          if frm eq fstate.current then begin
             WSET,fstate.draw_win
	     DEVICE, COPY = [0, 0, fstate.xsz, fstate.ysz, 0, 0, fstate.win_index(frm)]
          endif
        endif else begin
           tv,ttimg,true=fstate.true
           WSET,fstate.draw_win
	   DEVICE, COPY = [0, 0, fstate.xsz, fstate.ysz, 0, 0, fstate.win_index(fstate.current)]
	endelse
	
     endfor
end

PRO annotate_image_draw, ev
COMMON annotate_image_common

       WIDGET_CONTROL, fontbase, GET_UVALUE=fstate


       IF (((ev.press and 1) eq 1) and (fstate.arrsel le 1)) then begin
              fstate.evx=ev.x
              fstate.evy=ev.y
              widget_control,fstate.fntx,set_value=ev.x
              widget_control,fstate.fnty,set_value=ev.y
              WIDGET_CONTROL, fontbase, SET_UVALUE=fstate
              if fstate.addtime eq 1 then annotate_add_times else if fstate.addlogo eq 1 then annotate_add_times,/logo else annotate_image_preview
       ENDIF


       IF (((ev.press and 1) eq 1) and (fstate.arrsel eq 3)) then begin
              fstate.arxy(2)=ev.x
              fstate.arxy(3)=ev.y
              WIDGET_CONTROL, fstate.mvarrx2, SET_VALUE=ev.x
              WIDGET_CONTROL, fstate.mvarry2, SET_VALUE=ev.y 
              fstate.arrsel=1
	      WIDGET_CONTROL, fontbase, SET_UVALUE=fstate
             if fstate.addtime eq 1 then annotate_add_times else if fstate.addlogo eq 1 then annotate_add_times,/logo else annotate_image_preview
       ENDIF
       IF (((ev.press and 1) eq 1) and (fstate.arrsel eq 2)) then begin
              fstate.arxy(0)=ev.x
              fstate.arxy(1)=ev.y
              WIDGET_CONTROL, fstate.mvarrx1, SET_VALUE=ev.x
              WIDGET_CONTROL, fstate.mvarry1, SET_VALUE=ev.y 
              fstate.arrsel=3
	      WIDGET_CONTROL, fontbase, SET_UVALUE=fstate
              if fstate.addtime eq 1 then annotate_add_times else if fstate.addlogo eq 1 then annotate_add_times,/logo else annotate_image_preview
        ENDIF


END

pro annotate_image_event, ev
COMMON annotate_image_common
   WIDGET_CONTROL, fontbase, GET_UVALUE=fstate   ; get structure from UVALUE
   if(where(tag_names(ev) eq 'VALUE') gt -1)then input=ev.Value else WIDGET_CONTROL, ev.id, GET_UVALUE=input
   WIDGET_CONTROL, ev.id, GET_UVALUE=test
   IF (DATATYPE(test) EQ 'STR') THEN input = test
    CASE (input) OF

       'extout'  : fstate.ext=fstate.extouts(ev.index)
       'fntxt'   :  begin
                  fstate.txt=ev.value 
                  fstate.annsvd=0
          END
       'fntorn' : BEGIN   ;change current frame number
                WIDGET_CONTROL, fstate.fntorn, GET_VALUE=val
                fstate.orn=val
          END


       'fntsze'   : fstate.sze=ev.value 
       'fntx' : fstate.evx=ev.value
       'fnty' : fstate.evy=ev.value

;       'fnttxtclr' :  if fstate.true eq 1 then fstate.clr=reform(fstate.clrval(*,ev.value)) else fstate.clr=fstate.clrval(ev.value)
       'fnttxtclr' :  BEGIN
                        if fstate.true eq 1 then begin
                              fstate.clr=reform(fstate.clrval(*,ev.index))
                              WIDGET_CONTROL, fstate.rclr, SET_VALUE=fstate.clr(0)
                              WIDGET_CONTROL, fstate.gclr, SET_VALUE=fstate.clr(1)
                              WIDGET_CONTROL, fstate.bclr, SET_VALUE=fstate.clr(2)
			endif else begin
			      fstate.clr=fstate.clrval(ev.index)
                              WIDGET_CONTROL, fstate.rclr, SET_VALUE=fstate.clr
                        endelse 
                 END
        'rclr' : BEGIN   ;set red color value or color value if not true
                WIDGET_CONTROL, fstate.rclr, GET_VALUE=val
                if fstate.true eq 1 then fstate.clr(0)=val else fstate.clr=val
          END
        'gclr' : BEGIN   ;set red color value or color value if not true
                WIDGET_CONTROL, fstate.gclr, GET_VALUE=val
                fstate.clr(1)=val
          END
        'bclr' : BEGIN   ;set red color value or color value if not true
                WIDGET_CONTROL, fstate.bclr, GET_VALUE=val
                fstate.clr(2)=val
          END



       'fnttxtbk'  :  BEGIN; if fstate.true eq 1 then fstate.bkgrd=reform(fstate.bkgval(*,ev.value)) else fstate.bkgrd=fstate.bkgval(ev.value)
                        if fstate.true eq 1 then begin
                              fstate.bkgrd=reform(fstate.bkgval(*,ev.index))
                              if fstate.bkgrd(0) gt -1 then begin
                                WIDGET_CONTROL, fstate.brclr, SET_VALUE=fstate.bkgrd(0)
                                WIDGET_CONTROL, fstate.bgclr, SET_VALUE=fstate.bkgrd(1)
                                WIDGET_CONTROL, fstate.bbclr, SET_VALUE=fstate.bkgrd(2)
                              endif
                              if fstate.bkgrd(0) eq -2 then begin
                                WIDGET_CONTROL, fstate.brclr, GET_VALUE=val
				fstate.bkgrd(0)=val
                                WIDGET_CONTROL, fstate.bgclr, GET_VALUE=val
				fstate.bkgrd(1)=val
                                WIDGET_CONTROL, fstate.bbclr, GET_VALUE=val
				fstate.bkgrd(2)=val
                              endif
			endif else begin
			     fstate.bkgrd=fstate.bkgval(ev.index)
                              if fstate.bkgrd gt -1 then begin
                                WIDGET_CONTROL, fstate.brclr, SET_VALUE=fstate.bkgrd
                              endif
                              if fstate.bkgrd(0) eq -2 then begin
                                WIDGET_CONTROL, fstate.brclr, GET_VALUE=val
				fstate.bkgrd(0)=val
                              endif
                        endelse
                 END
        'brclr' : BEGIN   ;set red color value or color value if not true
                WIDGET_CONTROL, fstate.brclr, GET_VALUE=val
                if fstate.true eq 1 and fstate.bkgrd(0) ne -1 then fstate.bkgrd(0)=val else fstate.bkgrd=val
          END
        'bgclr' : BEGIN   ;set red color value or color value if not true
                WIDGET_CONTROL, fstate.bgclr, GET_VALUE=val
                if fstate.bkgrd(0) ne -1 then fstate.bkgrd(1)=val
          END
        'bbclr' : BEGIN   ;set red color value or color value if not true
                WIDGET_CONTROL, fstate.bbclr, GET_VALUE=val
                if fstate.bkgrd(0) ne -1 then fstate.bkgrd(2)=val
          END


       'fntf1'   :  fstate.f1=ev.value 
       'fntf2'   :  fstate.f2=ev.value 

       'fntalg'   : fstate.alg=fstate.algval(ev.value)

   	'hvfnt'   : BEGIN
                     if fstate.ufont eq 0 then device,set_font=''
	             fstate.nfnt=ev.value
                     fstate.bfnt=strmid(fstate.hvfonts(ev.value),0,strpos(fstate.hvfonts(ev.value),'-')-1)
                     fstate.ufont=-1
                   END
   	'ttfnt'   : BEGIN
                     if fstate.ufont eq 0 then device,set_font=''
	             fstate.nfnt=ev.value
                     fstate.bfnt=strmid(fstate.ttfonts(ev.value),0,strpos(fstate.ttfonts(ev.value),'-')-1)
                     fstate.ufont=1
                   END

   	'xbng' : BEGIN	;add text to current window
                    if fstate.ufont ne 0 then begin
                      fstate.ptxt=fstate.txt
                      fstate.txt=fstate.txt+strmid(fstate.bangs(ev.value),0,strpos(fstate.bangs(ev.value),'-')-1)
                      widget_control,fstate.wtxt,set_value=fstate.txt
                    endif
                 END

         'undo' : BEGIN
	            fstate.txt=fstate.ptxt
                    widget_control,fstate.wtxt,set_value=fstate.txt
                 END

       'MVarrx1' : fstate.arxy(0)=ev.value
       'MVarry1' : fstate.arxy(1)=ev.value
       'MVarrx2' : fstate.arxy(2)=ev.value
       'MVarry2' : fstate.arxy(3)=ev.value
       'Arrsel1' : begin
                  fstate.annsvd=0
                   fstate.arrsel=2
                end
       'Arrsel2' : fstate.arrsel=3
       'MVtxtarrclr' :  BEGIN
                        if fstate.true eq 1 then begin
                              fstate.arrclr=reform(fstate.clrval(*,ev.index))
                              WIDGET_CONTROL, fstate.arclr, SET_VALUE=fstate.arrclr(0)
                              WIDGET_CONTROL, fstate.agclr, SET_VALUE=fstate.arrclr(1)
                              WIDGET_CONTROL, fstate.abclr, SET_VALUE=fstate.arrclr(2)
			endif else begin
			      fstate.arrclr=fstate.clrval(ev.index)
                              WIDGET_CONTROL, fstate.arclr, SET_VALUE=fstate.arrclr
                        endelse
                 END
        'arclr' : BEGIN   ;set red color value or color value if not true
                WIDGET_CONTROL, fstate.arclr, GET_VALUE=val
                if fstate.true eq 1 then fstate.arrclr(0)=val else fstate.arrclr=val
          END
        'agclr' : BEGIN   ;set red color value or color value if not true
                WIDGET_CONTROL, fstate.agclr, GET_VALUE=val
                fstate.arrclr(1)=val
          END
        'abclr' : BEGIN   ;set red color value or color value if not true
                WIDGET_CONTROL, fstate.abclr, GET_VALUE=val
                fstate.arrclr(2)=val
          END



       'MVtxtarrsz' :  fstate.arxy(5)=ev.value
       'MVtxtarrth' :  fstate.arxy(4)=ev.value 
       'Arrclear' : BEGIN	
                     fstate.arxy=[0,0,0,0,fstate.arxy(4),fstate.arxy(5)]
                     fstate.arrsel =0
                     widget_control,fstate.mvarrx1,set_value=fstate.arxy(0)
                     widget_control,fstate.mvarry1,set_value=fstate.arxy(1)
                     widget_control,fstate.mvarrx2,set_value=fstate.arxy(2)
                     widget_control,fstate.mvarry2,set_value=fstate.arxy(3)
                 END
       'txtclear' : BEGIN
                    fstate.txt=''
                    widget_control,fstate.wtxt,set_value=fstate.txt
                 END



   	'SHOWFONT' : BEGIN	
                     if fstate.ufont eq -1 then fx=fstate.hvfonts(fstate.nfnt) else fx=fstate.ttfonts(fstate.nfnt)
		     if fstate.ufont ne 0 then begin
                       select_windows	;set_plot,'x'
                       if fstate.fnt_win gt -1 then  wdel,fstate.fnt_win
                       WINDOW, XSIZE = 700, YSIZE = 500, retain=2,/FREE
		       fstate.fnt_win = !D.WINDOW
                        if fstate.ufont eq 1 then begin
                             fin=strmid(fx,strpos(fx,'-')+2,strlen(fx)-strpos(fx,'-')+1)
                             showfont,fin,fin,/TT_font
			 endif else begin
                             fin=strmid(fx,1,strpos(fx,'-')-1)
                             fname=strmid(fx,strpos(fx,'-')+2,strlen(fx)-strpos(fx,'-')+1)
                             showfont,fin,fname
			 endelse
                     endif
                 END
   	'HWFONTS' : BEGIN	
                     hwfont=xfont()
                     if strlen(hwfont) gt 5 then begin
                      fstate.ufont=0
                      wset,fstate.txt_win
                      DEVICE, SET_FONT = hwfont
;                      if !d.window eq 0 then wdel,!d.window
                     endif
                 END


        'SLIDE_NUM' : BEGIN   ;change current frame number
                WIDGET_CONTROL, fstate.sliden, GET_VALUE=val
                fstate.current=val
          END
	  



	ELSE : BEGIN
                 PRINT, '%%annotate_image.  Unknown event.'
	     END


    ENDCASE

       WIDGET_CONTROL, fontbase, SET_UVALUE=fstate
       if fstate.addtime eq 1 then annotate_add_times else if fstate.addlogo eq 1 then annotate_add_times,/logo else annotate_image_preview


END


function annotate_image_func, ev
COMMON annotate_image_common
COMMON WSCC_MKMOVIE_COMMON
   WIDGET_CONTROL, fontbase, GET_UVALUE=fstate   ; get structure from UVALUE
   if(where(tag_names(ev) eq 'VALUE') gt -1)then input=ev.Value else WIDGET_CONTROL, ev.id, GET_UVALUE=input
   WIDGET_CONTROL, ev.id, GET_UVALUE=test
   IF (DATATYPE(test) EQ 'STR') THEN input = test
    CASE (input) OF


   	'APPLY' : BEGIN
	             if fstate.addtime eq 1 then annotate_add_times,/add else if fstate.addlogo eq 1 then annotate_add_times,/logo,/add else annotate_image_preview,/add
                    fstate.txt=''
                    widget_control,fstate.wtxt,set_value=fstate.txt
                     fstate.arxy=[0,0,0,0,fstate.arxy(4),fstate.arxy(5)]
                     fstate.arrsel =0
                     fstate.addlogo=0  
                     fstate.addtime=0  
                     if fstate.seopts ne 0 then widget_control,fstate.seopts,set_value=0
                     widget_control,fstate.mvarrx1,set_value=fstate.arxy(0)
                     widget_control,fstate.mvarry1,set_value=fstate.arxy(1)
                     widget_control,fstate.mvarrx2,set_value=fstate.arxy(2)
                     widget_control,fstate.mvarry2,set_value=fstate.arxy(3)
                     fstate.annsvd=1  
                     WIDGET_CONTROL, fontbase, SET_UVALUE=fstate
                  END
 
   	'DONE' : BEGIN	
                    if fstate.annsvd eq 0 then BEGIN
		    	popup_msg,[ 'Current annotation not saved.', $
		    	    	    'You must ACCEPT Annotation to keep your work.'], $
				    title='WARNING!',/modal
			
		    ENDIF ELSE BEGIN
			if fstate.fnt_win gt -1 then  wdel,fstate.fnt_win
			if fstate.local_win eq 1 then wdel,fstate.win_index     
			wdel,fstate.txt_win     
			if fstate.ufont eq 0 then DEVICE, SET_FONT = ''
			WIDGET_CONTROL, fontbase , /DESTROY   ; DONE
			WIDGET_CONTROL, groupleader, /DESTROY
			wdel,fstate.draw_win     
		    ENDELSE
                 END
 
    	'HELP' :    BEGIN   ;display help text
    	    	    which,'annotate_image',outfile=fi
		    break_file,fi,dl,dir,fn,ext
		    xdisplayfile,concat_dir(dir,'annotate_image_help.txt'),width=70,title='annotate_image Help'
    	    	    END

   	'CANCEL' : BEGIN	
                    if fstate.fnt_win gt -1 then  wdel,fstate.fnt_win
                    if fstate.local_win eq 1 then wdel,fstate.win_index     
                    wdel,fstate.txt_win     
                    if fstate.ufont eq 0 then DEVICE, SET_FONT = ''
		    WIDGET_CONTROL, fontbase , /DESTROY   ; DONE
	            WIDGET_CONTROL, groupleader, /DESTROY
                    wdel,fstate.draw_win     
                 END
        'SAVEFRM' : BEGIN  
                  if fstate.annsvd eq 0 then $
		    popup_msg,[ 'Current annotation not saved.', $
		    	    	'You must SAVE ANNOTATION to keep your work.'], $
				title='WARNING!',/modal
		  
		  IF datatype(fstate.hdrs) NE 'STC' THEN fname0=fstate.filename ELSE $
		    	fname0=fstate.hdrs[fstate.current].filename
		  break_file,fname0,vo,di,root0,su
		  IF root0 EQ '' THEN root0='X'
                 fname=Dialog_Pickfile(/Write, File=root0+fstate.ext,filter='*'+fstate.ext)                 
                 ;fname=strmid(fname,0,strpos(fname,fstate.ext))
		 
                 if fname ne '' then begin
                   if fstate.true eq 0 then begin
                     tvlct,r,g,b,/get
		     if fstate.len gt 1 then ttimg=imgs(*,*,fstate.current) else ttimg=imgs
                     if fstate.ext eq '.png' then write_png,fname,ttimg,r,g,b
                     if fstate.ext eq '.gif' then write_gif,fname,ttimg,r,g,b
                     if fstate.ext eq '.jpg' then begin
		    	       GIF2JPG24,ttimg,r,g,b,jpgimg
                               WRITE_JPEG, fname,jpgimg,TRUE=3
                     endif
                  endif else begin
                     wset,fstate.draw_win
		     if fstate.ext eq '.gif' then junk=fTVREAD(/gif, filename=fname,/noprompt)
		     if fstate.ext eq '.png' then junk=fTVREAD(/png, filename=fname,/noprompt)
		     if fstate.ext eq '.jpg' then junk=fTVREAD(/jpeg, filename=fname,/noprompt)
                 endelse
               endif
          END

        'SAVEMV' : BEGIN   ;change current frame number
                  if fstate.annsvd eq 0 then $
		    popup_msg,[ 'Current annotation not saved.', $
		    	    	'You must SAVE ANNOTATION to keep your work.'], $
				title='WARNING!',/modal
		  scc_movieout,fstate.win_index,fstate.file_hdr,hdrs=fstate.hdrs,filename=fstate.filename,len=fstate.len;  scc_movieout,base
          END

        'SETIMES' : BEGIN
               fstate.addlogo=0  
               fstate.addtime=1  
               fstate.txt='YYYY-MM-DD hh:mm:ss'
               fstate.annsvd=0
               widget_control,fstate.wtxt,set_value=fstate.txt
               WIDGET_CONTROL, fontbase, SET_UVALUE=fstate
	       annotate_add_times
             
	  END

	'SELOGO' : BEGIN
               fstate.addlogo=1  
               fstate.addtime=0  
               fstate.txt='ADDING SECCHI LOGO'
               fstate.annsvd=0
               widget_control,fstate.wtxt,set_value=fstate.txt
               IF ~(TAG_EXIST(fstate, 'logo')) THEN  begin
                  large_logo_filename = 'secchi_logo.sav'
                  path = concat_dir(getenv('SSW'),'stereo')
                  RESTORE, FILEPATH(large_logo_filename,ROOT_DIR=path,SUBDIRECTORY=['secchi','data'])
                  logo=scclogo
                  logo(where(logo eq 1))=255
                  logomd=rebin(logo,128,128)
                  block=where(logo eq 255)
                  arr_ind=array_indices(logo,block)
                  logo=logo(min(arr_ind[0,*]):max(arr_ind[0,*]),min(arr_ind[1,*]):max(arr_ind[1,*]))
                  block=where(logomd eq 255)
                  arr_ind=array_indices(logomd,block)
                  logomd=logomd(min(arr_ind[0,*]):max(arr_ind[0,*]),min(arr_ind[1,*]):max(arr_ind[1,*]))
                  fstate=add_tag(fstate,logo,'logo')
                  fstate=add_tag(fstate,logomd,'logomd')
    	          small_logo_filename = 'secchi_logo_small.sav'
	          path = concat_dir(getenv('SSW'),'stereo')
	          RESTORE, FILEPATH(small_logo_filename,ROOT_DIR=path,SUBDIRECTORY=['secchi','data'])
	          logosm=scclogo
                  logosm(where(logosm eq 1))=255
                  block=where(logosm eq 255)
                  arr_ind=array_indices(logosm,block)
                  logosm=logosm(min(arr_ind[0,*]):max(arr_ind[0,*]),min(arr_ind[1,*]):max(arr_ind[1,*]))
                  fstate=add_tag(fstate,logosm,'logosm')
               ENDIF
               WIDGET_CONTROL, fontbase, SET_UVALUE=fstate
	       annotate_add_times,/logo
	  END 


	'SENONE' : BEGIN
              fstate.addlogo=0  
              fstate.addtime=0  
              fstate.txt=''
              widget_control,fstate.wtxt,set_value=fstate.txt
              WIDGET_CONTROL, fontbase, SET_UVALUE=fstate
	  END 

	ELSE : BEGIN
                 PRINT, '%%annotate_image.  Unknown event.'
	     END

    ENDCASE


END


pro annotate_image ,win_index,  template=template, truecolor=truecolor, img=img, ahdrs=ahdrs, debug=debug
COMMON annotate_image_common
COMMON WSCC_MKMOVIE_COMMON
issecchi=0
issoho=0
if keyword_set(truecolor) then true=1 else true=0
if keyword_set(debug) then debug_on=1 else debug_on=0
if keyword_set(img) then begin
  win_index=(-1)
  imgs=img
ENDIF
IF (DATATYPE(win_index) EQ 'UND') THEN BEGIN
      filename = PICKFILE(filter='*.mvi,*.hdr,*.png,*.gif,*.jpg', /MUST_EXIST, TITLE='Select movie/image file to load',GET_PATH=path)
      IF (filename EQ '') THEN RETURN
      BREAK_FILE, filename, a, dir, name, ext
      IF (dir EQ '') THEN win_index = path+filename ELSE win_index = filename
ENDIF
IF (DATATYPE(win_index) EQ 'STR') then begin
  BREAK_FILE, win_index, a, dir, name, ext, /last
  ext = STRLOWCASE(ext)
	   if (ext EQ '.png') then imgt = READ_PNG(win_index,r,g,b)
           if (ext EQ '.gif') then READ_GIF, win_index, imgt, r, g, b
	   if (ext EQ '.jpg') then BEGIN
		READ_JPEG, win_index, imgt, ctable, COLORS=!D.N_COLORS-1
                r=ctable(*,0) & g=ctable(*,1) & b=ctable(*,2)
	   endif
           sz=size(imgt)
	   if sz(0) eq 3 then begin
             imgs=imgt
	     truecolor=1
           endif
           if keyword_set(truecolor) and (ext EQ '.png' or ext EQ '.gif' or ext EQ '.jpg') and sz(0) ne 3 then begin
                   imgs=bytarr(3,n_Elements(imgt(*,0)),n_Elements(imgt(0,*)))
                   imgs(0,*,*)=r(imgt) & imgs(1,*,*)=g(imgt) & imgs(2,*,*)=b(imgt)
	   endif 
           if ~keyword_set(truecolor) and (ext EQ '.png' or ext EQ '.gif' or ext EQ '.jpg') then begin
	          TVLCT, r,g,b  
                  imgs=imgt
           endif


	   if (ext EQ '.mvi' or ext EQ '.hdr') then BEGIN
               if strpos(win_index,'.hdr') gt -1 then pngmvi=win_index else pngmvi='0'
               OPENR,lu,win_index,/GET_LUN
               rgbvec=bytarr(!d.n_colors,3)
               SCCREAD_MVI, lu, file_hdr, ihdrs, imgsin, swapflag , rgbvec=rgbvec, pngmvi=pngmvi
               IF file_hdr.truecolor gt 0 or true eq 1 THEN imgs = bytarr(3,file_hdr.nx, file_hdr.ny, file_hdr.nf) else imgs = bytarr(file_hdr.nx, file_hdr.ny, file_hdr.nf) 

               for i=0,file_hdr.nf-1 do begin
    	          hdri=SCCMVIHDR2STRUCT(ihdrs[i],file_hdr.ver)
                  if i gt 0 then hdrs=[hdrs,hdri] else begin
		     hdrs=hdri
                     dindx=where(['C2','C3','EIT'] EQ hdri.detector,issoho)
                     sindx=where(['EUVI','COR2','COR1','HI1','HI2'] EQ hdri.detector, issecchi)
		  endelse
                  if (pngmvi ne '0') THEN begin  
                     filein=string(imgsin(i))
	             if strpos(filein,'.png') gt -1 then imgt=read_png(dir+string(imgsin(i)),r,g,b)
	             if strpos(filein,'.gif') gt -1 then read_gif,dir+string(imgsin(i)),imgt,r,g,b
	             IF (file_hdr.truecolor EQ 0) then begin
	                imgi=imgt 
	             endif else begin
                        imgi=bytarr(3,n_Elements(imgt(*,0)),n_Elements(imgt(0,*)))
                        imgi(0,*,*)=r(imgt) & imgi(1,*,*)=g(imgt) & imgi(2,*,*)=b(imgt)
	             ENDELSE
                  ENDIF ELSE BEGIN
	             IF (file_hdr.truecolor EQ 0) THEN imgi=imgsin[*,*,i] else imgi=imgsin[*,*,*,i]
                  ENDELSE
                  IF file_hdr.truecolor EQ 0 and true eq 1 then  begin
                     imgt=imgi & imgi= bytarr(3,file_hdr.nx, file_hdr.ny)
                     imgi(0,*,*)=rgbvec(imgt,0) & imgi(1,*,*)=rgbvec(imgt,1) & imgi(2,*,*)=rgbvec(imgt,2)
		  endif
	          IF (file_hdr.truecolor EQ 0) and true eq 0 THEN imgs(*,*,i)=imgi ELSE imgs(*,*,*,i)=imgi
                ENDFOR
                IF file_hdr.truecolor EQ 0 and true eq 1 then file_hdr.truecolor=true
	   endif
    filename=win_index
   win_index=(-1)
ENDIF

if datatype(file_hdr) eq 'UND' then file_hdr=''
if keyword_set(ahdrs) then begin
  hdrs=ahdrs
  dindx=where(['C2','C3','EIT'] EQ hdrs(0).detector,issoho)
  sindx=where(['EUVI','COR2','COR1','HI1','HI2'] EQ hdrs(0).detector, issecchi)
endif

ismvi=0
IF datatype(hdrs) NE 'UND' THEN BEGIN
    IF tag_exist(hdrs[0],'date_obs') THEN if strlen(hdrs[0].date_obs) GT 14 THEN ismvi=1
    IF tag_exist(hdrs[0],'time_obs') THEN if strlen(hdrs[0].time_obs) GT 2 THEN ismvi=1
ENDIF

if datatype(hdrs) eq 'UND' then hdrs=''
if datatype(ext) eq 'UND' then ext='.png'
if datatype(filename) eq 'UND' then filename=''


szimg=size(imgs)
if szimg(0) eq 4 then true=1
if szimg(0) eq 3 and szimg(1) eq 3 then true=1 
if true eq 1 then xsz=n_Elements(imgs(0,*,0)) else xsz=n_Elements(imgs(*,0))
if true eq 1 then ysz=n_Elements(imgs(0,0,*)) else ysz=n_Elements(imgs(0,*))

if keyword_set(template) and datatype(textframe) eq 'UND' and true eq 1 then textframe=bytarr(4,xsz,ysz)
if keyword_set(template) and datatype(textframe) eq 'UND' and true eq 0 then textframe=bytarr(2,xsz,ysz)


local_win=0 ;will delete windows if
IF (DATATYPE(win_index) EQ 'UND') or win_index(0) eq -1 THEN BEGIN
    if true eq 1 then nf1=n_Elements(imgs(0,0,0,*)) else nf1=n_Elements(imgs(0,0,*))
;    IF nf1 gt 1 then begin
        win_index=intarr(nf1)
        local_win=1
        FOR i=0,nf1-1 DO BEGIN
           WINDOW, XSIZE = xsz, YSIZE = ysz, /PIXMAP, /FREE
           win_index(i) = !D.WINDOW
 	   IF (true EQ 0) THEN tv,imgs[*,*,i] ELSE tv,imgs[*,*,*,i],true=true
       ENDFOR
;   ENDIF else win_index=0


ENDIF

len = N_ELEMENTS(win_index)


;Table H-33: Embedded Font Selection Commands 

hvfonts=[$
'!3 - Simplex Roman (default)',$
'!4 - Simplex Greek',$	
'!5 - Duplex Roman',$
'!6 - Complex Roman',$
'!7 - Complex Greek',$
'!8 - Complex Italic',$
'!9 - Math/special characters',$
'!10 - Special characters',$
'!11 - Gothic English',$
'!12 - Simplex Script',$
'!13 - Complex Script',$
'!14 - Gothic Italian',$
'!15 - Gothic German',$
'!16 - Cyrillic',$
'!17 - Triplex Roman',$
'!18 - Triplex Italic',$
'!20 - Miscellaneous']

ttfonts= [$
'!3 - Helvetica',$
'!4 - Helvetica Bold',$	
'!5 - Helvetica Italic',$
'!6 - Helvetica Bold Italic',$
'!7 - Times',$
'!8 - Times Italic',$
'!9 - Symbol',$
'!11 - Courier',$
'!12 - Courier Italic',$
'!13 - Courier Bold',$
'!14 - Courier Bold Italic',$
'!15 - Times Bold',$
'!16 - Times Bold Italic']

bangs=[$
'!N - Shift back to the normal level and original character size.',$
'!A - Shift above the division line.',$
'!B - Shift below the division line.',$
'!C - Carriage return.',$
'!D - Shift down to first level subscript, decrease size by 0.62.',$
'!E - Shift up to the exponent level and decrease size by 0.44.',$
'!I - Shift down to the index level and decrease size by 0.44.',$
'!L - Shift down to the second level subscript. Decrease size by 0.62.',$
'!R - Restore position from the top of the saved positions stack.',$
'!S - Save position to top of the saved positions stack.',$
'!U - Shift to upper subscript level. Decrease size by 0.62.',$
'!! - Display the ! symbol.',$
'!M - Math/special characters  or true type Symbol (1 character) ',$
'!X - Revert to the entry font']



;if ufont eq 0 then begin
;  print,'Program does not work with hardware fonts'
;  return
;endif else if ufont eq 1 then fonts=ttfonts


  ftitle='Annotate_Image'

        groupleader = Widget_Base(Map=0)
        Widget_Control, groupleader, /Realize

  fontbase = WIDGET_BASE(/row,  TITLE=ftitle , group_leader=groupleader)


  fontc = WIDGET_BASE(fontbase, /COLUMN)
     fontDRAW = WIDGET_BASE(fontc ,/row)


; ******KB***** this is where the scroll bar stuff begins 
device,get_screen_size=ss
w=define_widget_sizes()
widgysize=w.dsktop+w.scrbar+2*w.border+w.winhdr+44
widgxsize=w.scrbar+2*w.border+660

xscrlsize=(xsz < (ss[0]-widgxsize))
yscrlsize=(ysz < (ss[1]-widgysize))

  if (xsz gt xscrlsize OR ysz gt yscrlsize) then $
    draw_fx= WIDGET_DRAW(fontDRAW, XSIZE=xsz, YSIZE=ysz, $
                       X_SCROLL_SIZE=xscrlsize, $
                       Y_SCROLL_SIZE=yscrlsize, $  
                       EVENT_PRO='annotate_image_DRAW', /FRAME, $
                       /BUTTON_EVENTS, RETAIN=2) $ 
else $
 	draw_fx = WIDGET_DRAW(fontDRAW, XSIZE=xsz, YSIZE=ysz, EVENT_PRO='annotate_image_DRAW', $
               /FRAME, /BUTTON_EVENTS , RETAIN=2)


; ******KB**** end of scroll bar stuff	

;-Define horizontal margins for various widgets
labelm=2

frameslidelabel='Current Frame:'
fsllen=strlen(frameslidelabel)
if len ne 1 then begin
     fontrowa = WIDGET_BASE(fontc ,/row)
     mvtxt=widget_label (fontrowa,  value=frameslidelabel)
     sliden = WIDGET_SLIDER(fontrowa,  VALUE=0, UVALUE='SLIDE_NUM', $
             /DRAG, /SCROLL, MIN=0, MAX=len-1 ,xsize=xscrlsize-6*fsllen)  ; 6*14=width of label
endif else sliden=-1


;  fonttext = WIDGET_BASE(fontbase,/colum,  TITLE=ftitle+' text' )

  fnt1c = WIDGET_BASE(fontbase,/colum)

; keep track of row lengths
lab1='TEXT:'
f1sz=80
but1='Clear'
rowlens=strlen(lab1)+f1sz+strlen(but1)+2+3

       fnt1t = WIDGET_BASE(fnt1c,/colum,/frame)
       fnt1 = WIDGET_BASE(fnt1t,/row)
    	txt=''
    	wtxt= CW_FIELD(fnt1,  /ALL_EVENTS, value=txt, UVALUE='fntxt' , XSIZE=f1sz, YSIZE=1, title=lab1)
    	tmp=  CW_BGROUP(fnt1,[but1],BUTTON_UVALUE = ['txtclear'],/ROW)

; keep track of row lengths
lab1='Vector Fonts:'
f1sz=max(strlen(hvfonts))
lab2=' TrueType Fonts:'
f2sz=max(strlen(ttfonts))
but1='Show Font'
rowlens=[rowlens,strlen(lab1)+f1sz+strlen(lab2)+f2sz+strlen(but1)+2+5]

    	fnt1e = WIDGET_BASE(fnt1t,/ROW)
       tfnt =   WIDGET_LABEL(fnt1e, VALUE=lab1)
       tfnt =   CW_BSELECTOR2(fnt1e, hvfonts, SET_VALUE=nfnt, UVALUE='hvfnt')
       nfnt=0
       tfnt =   WIDGET_LABEL(fnt1e, VALUE=lab2)
       tfnt =   CW_BSELECTOR2(fnt1e, ttfonts, SET_VALUE=nfnt, UVALUE='ttfnt')
       nfnt=0
       tmp=  CW_BGROUP(fnt1e,[but1],BUTTON_UVALUE = ['SHOWFONT'],/ROW)
       
; keep track of row lengths
lab1='Insert Embedded Command:'
f1sz=max(strlen(bangs))
rowlens=[rowlens,strlen(lab1)+f1sz+2+2]

       fnt1a = WIDGET_BASE(fnt1t,/row)
       tfnt =   WIDGET_LABEL(fnt1a, VALUE=lab1)
       tfnt =   CW_BSELECTOR2(fnt1a, bangs, SET_VALUE=0, UVALUE='xbng')

; keep track of row lengths
lab1='(Embedded !-commands do not work with hardware fonts.)'
but1='Select a Hardware Font'
rowlens=[rowlens,strlen(lab1)+strlen(but1)+2+2]

       fnt2 = WIDGET_BASE(fnt1t,/ROW)
       tmp=  CW_BGROUP(fnt2,[but1],BUTTON_UVALUE = ['HWFONTS'],/ROW)
       tfnt =   WIDGET_LABEL(fnt2, VALUE=lab1)

       fnt3c = WIDGET_BASE(fnt1c,/colum,/frame)

algarr=['Left','Center','Right']
; keep track of row lengths
lab1='TEXT position: CLICK ON IMAGE -or-'
lab2='X:'
f1sz=4
lab3='Y:'
f2sz=4
lab4='Align:'
f3sz=max(strlen(algarr))
lab5='Char Size:'
f4sz=4
but1='Show Font'
rowlens=[rowlens,strlen(lab1)+strlen(lab2)+f1sz+strlen(lab3)+f2sz+strlen(lab4)+f3sz+strlen(lab5)+f4sz+2+10]

       fnt3 = WIDGET_BASE(fnt3c,/ROW)
    	
	fntx= WIDGET_LABEL(fnt3,value=lab1)
        fntx= CW_FIELD(fnt3,  /ALL_EVENTS, Value=0 , UVALUE='fntx' , XSIZE=f1sz, YSIZE=1, title=lab2)
        fnty= CW_FIELD(fnt3,  /ALL_EVENTS, Value=0 , UVALUE='fnty' , XSIZE=f2sz, YSIZE=1, title=lab3)
          algval=[0.0,0.5,1.0]
          alg=algval(0)
       mvtxtc =   WIDGET_LABEL(fnt3, VALUE=lab4)
       mvtxtc =   CW_BSELECTOR2(fnt3, algarr, SET_VALUE=0, UVALUE='fntalg')
       sze=3.0
       fntsze= CW_FIELD(fnt3,  /ALL_EVENTS, value=sze , UVALUE='fntsze' , XSIZE=f4sz, YSIZE=1, title=lab5)

; keep track of row lengths
lab1='Orientation:'
f1sz=360
rowlens=[rowlens,strlen(lab1)+f1sz/6.+2+2]
; convert scr_size to char equivalent
       fnt3t1 = WIDGET_BASE(fnt3c,/ROW)
       orn=0
       mvtxt=widget_label (fnt3t1,  value=lab1)
       fntorn = WIDGET_SLIDER(fnt3t1,  VALUE=0, UVALUE='fntorn', /DRAG, MIN=0, MAX=360,scr_xsize=f1sz,/scroll)

;  fontarrow = WIDGET_BASE(fontbase, /colum, /frame )

    arxy=[0,0,0,0,1,fix(xsz/64.0)]
    arrcol = WIDGET_BASE(fnt1c,/colum,/frame)

; keep track of row lengths
lab1='ARROW: '
but1='Select'
lab2='X1:'
f2sz=4
lab3='Y1:'
f3sz=4
lab4='X2:'
f4sz=4
lab5='Y2:'
f5sz=4
lab6='Thick:'
f6sz=4
lab7='Size:'
f7sz=4
but2='Clear'
rowlens=[rowlens,strlen(lab1)+strlen(lab2)+strlen(lab3)+strlen(lab4)+strlen(lab5)+strlen(lab6)+strlen(lab7) $
    	    	 +f2sz+f3sz+f4sz+f5sz+f6sz+f7sz+strlen(but1)+strlen(but2) +2+15]

       arrrow1 = WIDGET_BASE(arrcol,/row)
            mvtxtc =   WIDGET_LABEL(arrrow1, VALUE=lab1)
            tmp=  CW_BGROUP(arrrow1,[but1],BUTTON_UVALUE = ['Arrsel1'],/ROW)
            mvarrx1= CW_FIELD(arrrow1,  /ALL_EVENTS, Value=arxy(0) , UVALUE='MVarrx1' , XSIZE=f2sz, YSIZE=1, title=lab2)
            mvarry1= CW_FIELD(arrrow1,  /ALL_EVENTS, Value=arxy(1) , UVALUE='MVarry1' , XSIZE=f3sz, YSIZE=1, title=lab3)
;            tmp=  CW_BGROUP(arrrow1,['Select'],BUTTON_UVALUE = ['Arrsel2'],/ROW)
            mvarrx2= CW_FIELD(arrrow1,  /ALL_EVENTS, Value=arxy(2) , UVALUE='MVarrx2' , XSIZE=f4sz, YSIZE=1, title=lab4)
            mvarry2= CW_FIELD(arrrow1,  /ALL_EVENTS, Value=arxy(3) , UVALUE='MVarry2' , XSIZE=f5sz, YSIZE=1, title=lab5)

;        arrrow2 = WIDGET_BASE(arrcol,/row)
;            mvtxtc =   WIDGET_LABEL(text6, VALUE='Color:')
;            mvtxtc =   CW_BSELECTOR2(text6, clrarr, SET_VALUE=0, UVALUE='MVtxtarrclr')
            mvtxt= CW_FIELD(arrrow1,  /ALL_EVENTS, value=arxy(4) , UVALUE='MVtxtarrth' , XSIZE=f6sz, YSIZE=1, title=lab6)
            mvtxt= CW_FIELD(arrrow1,  /ALL_EVENTS, value=arxy(5) , UVALUE='MVtxtarrsz' , XSIZE=f7sz, YSIZE=1, title=lab7)
            tmp=  CW_BGROUP(arrrow1,[but2],BUTTON_UVALUE = ['Arrclear'],/ROW)


       fnt3cb1 = WIDGET_BASE(fnt1c,/ROW,/frame)
       if true eq 0 then begin
          clrarr=['White','Median','Black']
          clrval=[255,127,0]
          clr=clrval(0)          
          arrclr=clr
          bkgarr=['none','White','Median','Black','Select']
          bkgval=[-1,255,127,0,-2]
          bkgrd=bkgval(0)
          fnt3ba = WIDGET_BASE(fnt3cb1,/Colum,/frame,Title='Color')

; keep track of row lengths
lab1='Text Color:'
lab2='Background:'
lab3='Arrow Color:'
f1sz=max(strlen(clrarr))
f2sz=max(strlen(bkgarr))
f3sz=max(strlen(clrarr))
lab4='Val:'
f4sz=140
rowlens=[rowlens, (strlen(lab1)+strlen(lab2)+strlen(lab3)+f1sz+f2sz+f3sz+2+6), $
    	    	  (3*strlen(lab4)+3*f4sz/6.+2+6) ]
; convert scr_size to char equivalent

          mvtxtc =   widget_droplist(fnt3ba, VALUE=clrarr, UVALUE='fnttxtclr',title=lab1)
          fnt3bar1 = WIDGET_BASE(fnt3ba,/row)
          mvtxt=widget_label (fnt3bar1,  value=lab4)
          rclr = WIDGET_SLIDER(fnt3bar1,  VALUE=clrval(0), UVALUE='rclr', /DRAG, MIN=0, MAX=255,scr_xsize=f4sz,/scroll);, title='R:')
          fnt3bar2 = WIDGET_BASE(fnt3ba,/row)
 
          fnt3bb = WIDGET_BASE(fnt3cb1,/Colum,/frame,title='Background')
          mvtxtc =   widget_droplist(fnt3bb, VALUE=bkgarr, UVALUE='fnttxtbk',title=lab2)
          fnt3bbr1 = WIDGET_BASE(fnt3bb,/row)
          mvtxt=widget_label (fnt3bbr1,  value=lab4)
          brclr = WIDGET_SLIDER(fnt3bbr1,  VALUE=0, UVALUE='brclr', /DRAG, MIN=0, MAX=255,scr_xsize=f4sz,/scroll);, title='R:')

          fnt3bc = WIDGET_BASE(fnt3cb1,/Colum,/frame,Title='Arrow Color')
          mvtxtc =   widget_droplist(fnt3bc, VALUE=clrarr, UVALUE='MVtxtarrclr',title=lab3)
          fnt3bcr1 = WIDGET_BASE(fnt3bc,/row)
          mvtxt=widget_label (fnt3bcr1,  value=lab4)
          arclr = WIDGET_SLIDER(fnt3bcr1,  VALUE=clrval(0), UVALUE='arclr', /DRAG, MIN=0, MAX=255,scr_xsize=f4sz,/scroll);, title='R:')

          gclr=0 & bclr=0
          bgclr=0 & bbclr=0
          agclr=0 & abclr=0
;          mvtxtc =   WIDGET_LABEL(fnt3b, VALUE='Color: ')
;          mvtxtc =   CW_BSELECTOR2(fnt3b, clrarr, SET_VALUE=0, UVALUE='fnttxtclr')
;          mvtxtc =   WIDGET_LABEL(fnt3b, VALUE='Bkgnd: ')
;          mvtxtc =   CW_BSELECTOR2(fnt3b, bkgarr, SET_VALUE=0, UVALUE='fnttxtbk')
       endif else begin
          clrarr=['White',  'Red',    'Blue',   'Green',  'yellow', 'Black']
;          clrval=['ffffff'x,'0000ff'x,'ff0000'x,'00ff00'x,'38e5e5'x,'000000'x]
          clrval=[[255,255,255],[255,000,000],[000,000,255],[000,255,000],[229,229,056],[000,000,000]]
          clr=reform(clrval(*,0))
          arrclr=clr

          bkgarr=['none',    'White',      'Red',        'Blue',       'Green',     'yellow',       'Black',    'Select']
          bkgval=[[-1,-1,-1],[255,255,255],[255,000,000],[000,000,255],[000,255,000],[229,229,056],[000,000,000],[-2,-2,-2]]
          bkgrd=reform(bkgval(*,0))

          fnt3ba = WIDGET_BASE(fnt3cb1,/Colum,/frame,Title='Color')

; keep track of row lengths
lab1='Text Color:'
lab2='Background:'
lab3='Arrow Color:'
f1sz=max(strlen(clrarr))
f2sz=max(strlen(bkgarr))
f3sz=max(strlen(clrarr))
lab4='R:'
f4sz=140
rowlens=[rowlens,(strlen(lab1)+strlen(lab2)+strlen(lab3)+f1sz+f2sz+f3sz+2+6), $
    	    	  (3*strlen(lab4)+3*f4sz+2+6), (3*strlen(lab4)+3*f4sz+2+6), (3*strlen(lab4)+3*f4sz+2+6) ]

          mvtxtc =   widget_droplist(fnt3ba, VALUE=clrarr, UVALUE='fnttxtclr',title=lab1)
          fnt3bar1 = WIDGET_BASE(fnt3ba,/row)
          mvtxt=widget_label (fnt3bar1,  value=lab4)
          rclr = WIDGET_SLIDER(fnt3bar1,  VALUE=clrval(0,0), UVALUE='rclr', /DRAG, MIN=0, MAX=255,scr_xsize=f4sz,/scroll);, title='R:')
          fnt3bar2 = WIDGET_BASE(fnt3ba,/row)
          mvtxt=widget_label (fnt3bar2,  value='G:')
          gclr = WIDGET_SLIDER(fnt3bar2,  VALUE=clrval(1,0), UVALUE='gclr', /DRAG, MIN=0, MAX=255,scr_xsize=f4sz,/scroll);, title='G:')
          fnt3bar3 = WIDGET_BASE(fnt3ba,/row)
          mvtxt=widget_label (fnt3bar3,  value='B:')
          bclr = WIDGET_SLIDER(fnt3bar3,  VALUE=clrval(2,0), UVALUE='bclr', /DRAG, MIN=0, MAX=255,scr_xsize=f4sz,/scroll);, title='B:')

          fnt3bb = WIDGET_BASE(fnt3cb1,/Colum,/frame,title='Background')

          mvtxtc =   widget_droplist(fnt3bb, VALUE=bkgarr, UVALUE='fnttxtbk',title=lab2)
          fnt3bbr1 = WIDGET_BASE(fnt3bb,/row)
          mvtxt=widget_label (fnt3bbr1,  value=lab4)
          brclr = WIDGET_SLIDER(fnt3bbr1,  VALUE=0, UVALUE='brclr', /DRAG, MIN=0, MAX=255,scr_xsize=f4sz,/scroll);, title='R:')
          fnt3bbr2 = WIDGET_BASE(fnt3bb,/row)
          mvtxt=widget_label (fnt3bbr2,  value='G:')
          bgclr = WIDGET_SLIDER(fnt3bbr2,  VALUE=0, UVALUE='bgclr', /DRAG, MIN=0, MAX=255,scr_xsize=f4sz,/scroll);, title='G:')
          fnt3bbr3 = WIDGET_BASE(fnt3bb,/row)
          mvtxt=widget_label (fnt3bbr3,  value='B:')
          bbclr = WIDGET_SLIDER(fnt3bbr3,  VALUE=0, UVALUE='bbclr', /DRAG, MIN=0, MAX=255,scr_xsize=f4sz,/scroll);, title='B:')

          fnt3bc = WIDGET_BASE(fnt3cb1,/Colum,/frame,Title='Arrow Color')
          mvtxtc =   widget_droplist(fnt3bc, VALUE=clrarr, UVALUE='MVtxtarrclr',title=lab3)
          fnt3bcr1 = WIDGET_BASE(fnt3bc,/row)
          mvtxt=widget_label (fnt3bcr1,  value=lab4)
          arclr = WIDGET_SLIDER(fnt3bcr1,  VALUE=clrval(0,0), UVALUE='arclr', /DRAG, MIN=0, MAX=255,scr_xsize=f4sz,/scroll);, title='R:')
          fnt3bcr2 = WIDGET_BASE(fnt3bc,/row)
          mvtxt=widget_label (fnt3bcr2,  value='G:')
          agclr = WIDGET_SLIDER(fnt3bcr2,  VALUE=clrval(1,0), UVALUE='agclr', /DRAG, MIN=0, MAX=255,scr_xsize=f4sz,/scroll);, title='G:')
          fnt3bcr3 = WIDGET_BASE(fnt3bc,/row)
          mvtxt=widget_label (fnt3bcr3,  value='B:')
          abclr = WIDGET_SLIDER(fnt3bcr3,  VALUE=clrval(2,0), UVALUE='abclr', /DRAG, MIN=0, MAX=255,scr_xsize=f4sz,/scroll);, title='B:')
       endelse
;
       seopts=0
       mvioptlabels=['Not Selected','Add Time stamps']
       mvioptbutval=['SENONE','SETIMES']
       if issecchi then begin
    	    mvioptlabels=[mvioptlabels,'Add SECCHI Logo']
	    mvioptbutval=[mvioptbutval,'SELOGO']
       endif
       if ismvi then begin
           fnt3bd = WIDGET_BASE(fnt3cb1,/Colum,/frame,Title='MVI OPTIONS:')
           mvtxt=widget_label (fnt3bd,  value='MVI OPTIONS:')
    	   seopts = CW_BGROUP(fnt3bd, mvioptlabels, BUTTON_UVALUE=mvioptbutval, /EXCLUSIVE, /COLUMN, /FRAME, EVENT_FUNCT='annotate_image_func')
       endif

;
;--Bottom box has 2 rows. Separate var for each item in box.
;
     	fnt4a = WIDGET_BASE(fnt1c,/column,/frame)


; - First Row

; keep track of row lengths
labfr1=''
f1sz=3
labfr2='to'
f2sz=3
butacc='ACCEPT Annotation for frame(s):'

rowlens=[rowlens,strlen(labfr1)+strlen(labfr2)+f1sz+f2sz+strlen(butacc) +2*3+7]

    	fnt4  = WIDGET_BASE(fnt4a,/ROW)
    	tmp=  CW_BGROUP(fnt4,[butacc],BUTTON_UVALUE = ['APPLY'],/ROW,EVENT_FUNCT='annotate_image_func')
    	f1=0 & f2=len-1
    	if len gt 1 then begin
    	    mvtxt= CW_FIELD(fnt4,  /ALL_EVENTS, Value=f1, UVALUE='fntf1' , XSIZE=f1sz, YSIZE=1, title=labfr1)
    	    mvtxt= CW_FIELD(fnt4,  /ALL_EVENTS, Value=f2, UVALUE='fntf2' , XSIZE=f2sz, YSIZE=1, title=labfr2)
    	endif

; - Second Row
 
buthel=' Help '
butcan=' Cancel '
butdon=' Done '
butmvi=' Save MVI '
butfra=' Save Frame to File '
if len ne 1 and datatype(file_hdr) eq 'STC' then BEGIN
    bgrplab=[butmvi,butfra]
    bgrpval=['SAVEMV','SAVEFRM']
ENDIF ELSE BEGIN
    bgrplab=[butfra]
    bgrpval=['SAVEFRM']
ENDELSE
labtyp='Output type:'
f3sz=strlen(labtyp)
extouts=['.png','.jpg','.gif']
f4sz=4
rowlens=[rowlens,strlen(buthel)+strlen(butcan)+strlen(butdon) +total(strlen(bgrplab))+f3sz +f4sz +2*6+4]

        fnt5 = WIDGET_BASE(fnt4a,/ROW)
	tmp=  CW_BGROUP(fnt5,[buthel,butcan,butdon], BUTTON_UVALUE = ['HELP','CANCEL','DONE'], /ROW,EVENT_FUNCT='annotate_image_func')
    	tmp=  CW_BGROUP(fnt5, bgrplab, BUTTON_UVALUE = bgrpval, /ROW,EVENT_FUNCT='annotate_image_func')
    	extout = widget_droplist(fnt5, VALUE=extouts, UVALUE='extout',title=labtyp)
    	widget_control,extout, SET_DROPLIST_SELECT=where(extouts eq ext)
              
;   WIDGET_CONTROL, fonttext, MAP=0
;   WIDGET_CONTROL, /REAL, fonttext
;   WIDGET_CONTROL, fontarrow, MAP=0
;   WIDGET_CONTROL, /REAL, fontarrow


bfnt=strmid(hvfonts(0),0,strpos(hvfonts(0),'-')-1)
blank=bytarr(512,512)+0

   WIDGET_CONTROL, fontbase, MAP=1
   WIDGET_CONTROL, /REAL, fontbase
   WIDGET_CONTROL, draw_fx, GET_VALUE=draw_win

;device,retain=2
window,xsize=xsz,ysize=ysz, /PIXMAP, /FREE
txt_win=!d.window


fstate={        txt:txt, $
                nfnt:nfnt, $
                bfnt:bfnt, $
                ptxt:' ', $
                sze:sze, $
		orn:orn, $
		fntorn:fntorn, $
                wtxt:wtxt, $
                txt_win:txt_win, $
                draw_win:draw_win, $
                fnt_win:-1, $
                algval:algval, $
                alg:alg, $
                clr:clr, $
                bkgrd:bkgrd, $
                clrval:clrval, $
                bkgval:bkgval, $
                ufont:-1, $
                hvfonts:hvfonts, $
                ttfonts:ttfonts, $
                bangs:bangs, $
                evx:0, $
                evy:0, $
                fntx:fntx, $
                fnty:fnty, $
                f1:f1, $
                f2:f2, $
                xsz:xsz, $
                ysz:ysz, $
                true:true, $
                win_index:win_index, $ 
                filename:filename, $ 
                file_hdr:file_hdr, $ 
                hdrs:hdrs, $ 
                local_win:local_win, $ 
                len:len, $ 
                current:0, $ 
                showtxt:0, $ 
                showarr:0, $ 
                arxy:arxy, $ 
                arrclr:arrclr, $ 
                arrsel:0, $ 
                arclr:arclr, $ 
                agclr:agclr, $ 
                abclr:abclr, $ 
                brclr:brclr, $ 
                bgclr:bgclr, $ 
                bbclr:bbclr, $ 
                rclr:rclr, $ 
                gclr:gclr, $ 
                bclr:bclr, $ 
               mvarrx1:mvarrx1, $
               mvarry1:mvarry1, $
               mvarrx2:mvarrx2, $
               mvarry2:mvarry2, $
               ext:ext, $
               extouts:extouts, $
               addtime:0, $
               addlogo:0, $
               seopts:seopts, $
               sliden:sliden, $ 
               annsvd:1 $
            }
       

   WIDGET_CONTROL, fontbase, SET_UVALUE=fstate
   annotate_image_preview

IF debug_on THEN BEGIN
    print,'Screen_size=',ss
    help,widgysize,widgxsize,xscrlsize,yscrlsize
    wait,2
ENDIF


  XMANAGER, 'annotate_image', fontbase, EVENT_HANDLER='annotate_image_event'

 
END
