;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA_DEFAULTS
; PURPOSE:
;		This function contains defaults, including
;			1. a filename 
;			2. the corresponding pathname. A 2-element string array
;				is required, where the elements refer to linux and windows
;				pathnames, respectively. (Depending on which o/s DIVA is
;				running on, the other pathname may be set to the null-string)
;			3. initial size of main draw area. A 2-element array of the form
;				[xsize,ysize]
;			4. a string array containing a list of printers
;			5. a corresponding list of labels for a WIDGET_LIST
;		This file should be edited before starting DIVA for the first
;		time.
; OUTPUTS:
;		The items listed above
;-
;------------------------------------------------------------------------------;
PRO diva_defaults,def_path,def_file,tlb_drawwid_size,diva_printers, 		$
			diva_printers_list

;---------------------------------------------------------------------------;
;																			:
;	DEFAULT PATHNAME & FILENAME												;
;																			;
;---------------------------------------------------------------------------;
def_path    	    =	['/net/solardata/sd1/nathan/secchi/a/','D:\Temp\']
;def_path    	    =	['/home/ccdlab1/egse/data/images/','D:\Temp\']
;def_path    	    =	['/home/ccdlab2/egse/data/images/','D:\Temp\']
;def_file    	    =	'gain4.dat'
;def_file    	    =	'dan3.dat'
;def_file			=	'test4.fit'
def_file    	    =	'data.fit'
;def_file			=	'a_new_file_2.img'
;---------------------------------------------------------------------------;
;																			:
;	DEFAULT DRAW AREA SIZE													;
;																			;
;---------------------------------------------------------------------------;
;tlb_drawwid_size    =	[800,660] 	;usual size  
;tlb_drawwid_size    =	[1240,910]  ;full size on my linux box
tlb_drawwid_size    =	[600,660]

;---------------------------------------------------------------------------;
;																			:
;	LINUX PRINTERS															;
;																			;
;---------------------------------------------------------------------------;
;diva_printers		=	['hp8150dn-ps1','hp8150dn-ps2','phaser850dx']
diva_printers		=	['lasco-ptr4','hp8550']
diva_printers_list  	= diva_printers
;diva_printers_list	=	[	'HP8150DN-PS1 (b/w, default) R25 1.35',		$
;							'HP8150DN-PS2 (b/w) R25 1.35',				$
;							'PHASER850DX (colour) R25 1.35']

; IF you don't explicitly define any printers, then uncomment the 
;following lines, and the default printer will be used, as in...
;	lpr filename.ps

;diva_printers = ['']
;diva_printers_list = ['']

END
