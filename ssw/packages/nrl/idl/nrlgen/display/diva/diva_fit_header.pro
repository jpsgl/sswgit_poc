;------------------------------------------------------------------------------;
PRO diva_fit_header_ev, event
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
WIDGET_CONTROL, event.id, GET_UVALUE = eventval		
WIDGET_CONTROL, event.top,GET_UVALUE = fit_hdr_info

CASE eventval OF

"EXIT": 	BEGIN
				WIDGET_CONTROL, event.top, /DESTROY		
			END
"PRINT":	BEGIN
				header = fit_hdr_info.header

				openw,1,'header.txt';,/delete
					for i=0, n_elements(header)-1 do begin
						printf,1, format='(%"%s\r")', header[i]
					endfor
				close,1
				spawn,'lpr header.txt'
				spawn,'rm header.txt'
			END
ELSE: MESSAGE, "Event User Value Not Found"		
ENDCASE

END ;end of "diva_fit_header" event handler------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_fit_header,filename,header,diva_fit_headerbase,GROUP = GROUP
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;

IF(XRegistered("diva_fit_header") NE 0) THEN RETURN		

;-------------------create the main base---------------------------------------;
diva_fit_headerbase = WIDGET_BASE(TITLE = 'FITS HEADER:  "'+filename+'"',   $
    	    	    	group_leader=group,/floating,/col)

n_elems = n_elements(header)

IF (n_elems gt 30) THEN BEGIN
    diva_fit_h_text = widget_text(diva_fit_headerbase,xsize=90,ysize=30,    $
    	    	value=header,/scroll)
ENDIF ELSE BEGIN
    diva_fit_h_text = widget_text(diva_fit_headerbase,xsize=90,     	    $
    	    	ysize=n_elems + 1,value=header)
ENDELSE
;-------------------quit button------------------------------------------------;
printbutton=widget_button(diva_fit_headerbase, value='Print',uvalue='PRINT')
;-------------------quit button------------------------------------------------;
quitbutton=widget_button(diva_fit_headerbase, value='Quit',uvalue='EXIT')
;------------------------------------------------------------------------------;

;-------------------realise the widget-----------------------------------------;
WIDGET_CONTROL, diva_fit_headerbase, /REALIZE		

fit_hdr_info = {header : header}						
widget_control,diva_fit_headerbase,set_uvalue = fit_hdr_info

XManager, "diva_fit_header", diva_fit_headerbase, $		
		EVENT_HANDLER = "diva_fit_header_ev", $
		GROUP_LEADER = GROUP,/no_block		
						
END ;end of "diva_fit_header" main routine-------------------------------------;
;------------------------------------------------------------------------------;
