;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA_MAKE_FITS_FILE
; PURPOSE:
;		to make a FITS file
; INPUTS:
;		1. a "diva_image" structure
;		2. the image index of a multiple file (only for FITS data)
;		3. a string array, where each element will be added as 	  
;		   a comment to the FITS file  				    	  
;		4. the widget_id of the group_leader				    	  
;		5. 'func' is a string. It's values are 'bitshift',     	  
;		   'extract', 'diva_math'  					    	  
;		6. 'bsname' is a string to be added to the filename of 	  
;		   bitshifted images (eg: __bsn1)  			    	  
;		7. 'bshift_value' is an integer. The image will be     	  
;		   bitshifted this much, if func = bitshift	    	  
; PROCEDURES USED:
;		Functions:   SXPAR() 
;		Procedures:  SXADDPAR, SXDELPAR
;
; MODIFICATION HISTORY:
;		Original Version written by John A. Rainnie on 5th Feb 2003
;-
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_make_fits_file,diva_image,index,comments,wid_id,func,bsname,		$
						bshift_value

CASE func OF
	'bitshift': image = ISHFT(	(*diva_image.o_pimage),bshift_value)
	ELSE:		image = 		(*diva_image.o_pimage)
ENDCASE


image 		= reverse(image)
CASE diva_image.n_dim[0] OF
	3:	BEGIN
			b0  	=	rotate(image[*,*,0],2)
			b1  	=	rotate(image[*,*,1],2)
			b2  	=	rotate(image[*,*,2],2)
			image   =	[[[b0]],[[b1]],[[b2]]]
		END
	1:		image	= 	rotate(image,2)
ENDCASE

f			= diva_image.file_info.f

;get file format (FITS or IMG)
file_format = diva_image.file_info.file_format


CASE file_format OF
	'type_fit': BEGIN	;FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS
					file_exist = findfile(f,count=count)
					
					CASE count OF
						0:		mkhdr,header,image,/extend
						ELSE:	header  = headfits(f,ext=index)
					ENDCASE
					
					sxdelpar,header,'XTENSION'
					sxaddpar,header,'SIMPLE','T',' file does conform ' + 	$
							'to FITS standard',BEFORE='BITPIX'
					
					sxaddpar,header,'EXTEND','T',' FITS data may ' + 		$
							'contain extensions',BEFORE='BZERO'
					
					sxdelpar,header,'DATE-OBS'
					sxdelpar,header,'TIME-OBS'
					
					img_name = SXPAR(header,'IMG_NAME',count=count)
					IF (count eq 0) THEN img_name = diva_image.mfile_name
					
				END
	'type_img': BEGIN	;IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG
					
					;make a minimal FITs header
					mkhdr,header,image,/extend
					;discard 'O_BZERO' keyword and replace it with 'BZERO'
					
					IF (size(image,/type) eq 12) THEN BEGIN
						sxdelpar,header,'O_BZERO'
						sxdelpar,header,'BZERO'
						sxaddpar,header,'BZERO',32768,						$
									' Original Data is Unsigned Integer',	$
									after='EXTEND'
						;discard 'O_BSCALE' keyword and replace it with 'BSCALE'
						sxdelpar,header,'O_BSCALE'
						;add 'BSCALE' keyword
						sxaddpar,header,'BSCALE',1.,						$
									' Default scaling factor',after='BZERO'
					END
					CASE diva_image.diva_n_images OF
						1:		img_name = diva_image.file_info.file_str
						ELSE:	img_name = diva_image.mfile_name
					ENDCASE
				END
	ELSE:		stop
ENDCASE
				
;get current time and date and add to header		
get_date,date
sxaddpar,header,'DATE',date,' Creation UTC (CCCC-MM-DD) date ' + 			$
									'of FITS header',after='BSCALE'
t 			= systime()
this_time 	= strmid(t,11,8)
sxaddpar,header,'TIME',this_time,' Creation Time (HH-MM-SS) ' + 			$
									'of FITS header',after='DATE'

sxaddpar,header,'IMG_NAME',img_name,after='TIME'
sxdelpar,header,'FILENAME'


;remove 2-line comment about fits format....
sxdelpar,header,'COMMENT'
sxdelpar,header,'HISTORY'
sxaddpar,header,'HISTORY','developed by bigjohn and duncan at RAL', 		$
															before='END'

;add comments here!!!!!
sxaddpar,header,'COMMENT',' ********************************' + 			$
				'***************************************',before='HISTORY'


FOR i = 0, n_elements(comments) - 1 DO BEGIN
	sxaddpar,header,'COMMENT',comments[i],before='HISTORY'
ENDFOR


sxaddpar,header,'COMMENT',' ********************************' + 			$
				'***************************************',before='HISTORY'
;add usual 1-liner about FITS reference.....
sxaddpar,header,'COMMENT','FITS format is described in A&A ' +				$
							'Vol376, pg359; bibcode 2001A&A...376..' +		$
							'359H',before='HISTORY'

;OK, now let the user choose a filename
f_str 	= diva_image.file_info.file_str
p     	= diva_image.file_info.pathname
str   	= diva_image.mfile_name_str

CASE diva_image.diva_n_images OF
	1:		guess_name = p + f_str
	ELSE:	guess_name = p + f_str + '__' + str
ENDCASE

CASE func OF
	'bitshift': 	guess_name = guess_name + '__' + bsname + '.fit'
	'extract':		guess_name = guess_name + '.fit'
	'diva_math':	guess_name = guess_name + '.fit'
	'bitmask':		guess_name = guess_name + '_bitmask.fit'
	ELSE:			stop
ENDCASE

guess_name = strcompress(guess_name,/rem)

CASE func OF
	'diva_math':	get_f = guess_name
	ELSE:			BEGIN
						;call "diva_get_filename" FUNCTION to return 
						;user-selected "get_f"
						get_f 	= diva_get_filename(guess_name,p,wid_id)
					END 
ENDCASE	

;add "get_f" to FITS header (keyword = "FILENAME")	
file_nopath = strmid(get_f,strpos(get_f,'/',/reverse_search)+1)
sxaddpar,header,'FILENAME',file_nopath,' the filename',before='IMG_NAME'


;call 'diva_hdr_edit' function to add comments to header. If this 
;operation is aborted, then the returned value will be a 1 element
;string array.
new_header  = diva_hdr_edit(header,wid_id)
header		= *new_header
PTR_FREE,new_header
;check to see if returned value is 1 element string array
if (n_elements(header) eq 1) then RETURN


;OK, now (finally) write the FITS file.....whooohoooo!
writefits,get_f,image,header

IF (func eq 'bitmask') THEN Ptr_Free,diva_image.o_pimage

END;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
