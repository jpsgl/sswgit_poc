;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA_READ_DATA_BMP
; AUTHOR:
;		John A. Rainnie - RAL (j.a.rainnie@rl.ac.uk)
; PURPOSE:
;		This procedure queries and reads a BITMAP file. A 'diva_image'
;		structure is populated with appropriate values (cf: IMG and FITS
;		files).
; INPUTS:
;		tmp_diva_image_file - a 'diva_image' structure
; OUTPUTS:
;		tmp_diva_image_file - populated with appropriate values
; PROCEDURES USED:
;		FUNCTIONS:	DIVA_IMAGE_INIT (diva_image.pro)
;					QUERY_BMP,READ_BMP
;-
;------------------------------------------------------------------------------;
pro diva_read_data_bmp,tmp_diva_image_file
;------------------------------------------------------------------------------;
file_info	= tmp_diva_image_file.file_info

bmp_good 	= QUERY_BMP(file_info.f,info)

IF (bmp_good ne 1) 			THEN stop
IF (info.num_images ne 1) 	THEN stop

image = READ_BMP(file_info.f,r,g,b)

;I got the following from Liam E. Gumley's IDL procedure
;"showimage.pro" from.....
;http://cimss.ssec.wisc.edu/~gumley/idl/showimage.pro
;- Convert to 24-bit
image 			= ROTATE(REVERSE(image),2)
dims 			= SIZE(image, /dimensions)
nx  			= dims[0]
ny  			= dims[1]
true 			= BYTARR( nx, ny,3)
true[*, *,0]	= r[image]
true[*, *,1]    = g[image]
true[*, *,2]	= b[image]
image = TEMPORARY(true)
;- Reset the number of channels
info.channels = 3

n_dim	= info.channels

CASE info.pixel_type OF
	1	:	n_byt  = 1 
	ELSE:	stop
ENDCASE

tmp_diva_image_file.xsize			= info.dimensions[0]
tmp_diva_image_file.ysize			= info.dimensions[1]
tmp_diva_image_file.n_dim			= n_dim
tmp_diva_image_file.n_byt			= n_byt
tmp_diva_image_file.diva_n_images	= info.num_images

;call 'diva_image_init' function to populate 'diva_image' structure
diva_image = diva_image_init(tmp_diva_image_file,image)
;and update 'tmp_diva_image_file' structure
tmp_diva_image_file = diva_image

END
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA_GET_BMP_INFO
; AUTHOR:
;		John A. Rainnie - RAL (j.a.rainnie@rl.ac.uk)
; PURPOSE: 
;		This function returns a string array, which will be displayed in the
;		'FITS HEADER' gui. This is the BMP information derived from
;		the IDL function 'QUERY_BMP'
; INPUTS:
;		filename - the filename of the BMP
; OUTPUTS:
;		header - a string array
;-
FUNCTION diva_get_bmp_info,filename
;------------------------------------------------------------------------------;

bmp_good = QUERY_BMP(filename,binfo)

header  	 = strarr(n_tags(binfo) + 1)
header[0] = 'CHANNELS	'	  + strcompress(binfo.channels,/rem)
header[1] = 'XSIZE		'	  + strcompress(binfo.dimensions[0],/rem)
header[2] = 'YSIZE		'	  + strcompress(binfo.dimensions[1],/rem)
header[3] = 'HAS_PALETTE	' + strcompress(binfo.has_palette,/rem)
header[4] = 'IMAGE_INDEX	' + strcompress(binfo.image_index,/rem)
header[5] = 'NUM_IMAGES 	' + strcompress(binfo.num_images,/rem)
header[6] = 'PIXEL_TYPE 	' + strcompress(binfo.pixel_type,/rem)
header[7] = 'TYPE		'	  + strcompress(binfo.type,/rem)
						

RETURN,header

END;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
