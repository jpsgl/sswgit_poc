;------------------------------------------------------------------------------;
pro diva_draw_all_ev,event;----------------------------------------------------;
;------------------------------------------------------------------------------;
; Purpose:
;------------------------------------------------------------------------------;
WIDGET_CONTROL, event.id, GET_UVALUE = h_event_struct,/no_copy		

CASE h_event_struct.eventval OF

"EXIT":     	begin
    	    	    WIDGET_CONTROL, event.top, /DESTROY
		    Ptr_Free,h_event_struct.phist_info
		    return
    	    	end
"DRAW":     	begin
		     possibleEventTypes = [ 'DOWN', 'UP', 'MOTION', 'SCROLL' ]
		     thisEvent = possibleEventTypes(event.type)
		     ;if (thisevent eq 'MOTION') then print,event.x,event.y
		end
"BASE":     	begin
		    ;get what we need 
		    xsize_screen=(*h_event_struct.phist_info).xsize_screen
		    ysize_screen=(*h_event_struct.phist_info).ysize_screen
    	    	    xsize   	=(*h_event_struct.phist_info).xsize
		    ysize   	=(*h_event_struct.phist_info).ysize
		    n_dim   	=(*h_event_struct.phist_info).n_dim
		    diva_autoscale_on=(*h_event_struct.phist_info). 	    $
		    	    	    	    	    	    diva_autoscale_on
		    bitshift_val=(*h_event_struct.phist_info).bitshift_val
			
		    event.x = 0 > event.x < xsize_screen -20
		    event.y = 0 > event.y < ysize_screen -20
			    
;		    if StrUpCase(!Version.OS_Family) NE 'UNIX' THEN BEGIN
		    
		    	widget_control,     	    	    	    	    $
		    	    (*h_event_struct.phist_info).diva_draw_all_draw,$
			    draw_xsize=event.x,draw_ysize=event.y
		    	;print,event.x,event.y
			
		    	(*h_event_struct.phist_info).newx=event.x
		    	(*h_event_struct.phist_info).newy=event.y
			
		    erase
		    widget_control,(*h_event_struct.phist_info).$
			    	diva_draw_all_draw,get_value=all_index
    	    	    wset,all_index
		    
		    if (n_dim eq 3) then begin
		    	pimage=congrid((*(*h_event_struct.phist_info).	    $
			    	    pimage),event.x,event.y,3)
		    endif else begin
		    	pimage=congrid((*(*h_event_struct.phist_info).	    $
			    	    pimage),event.x,event.y)
    	    	    endelse		    
    	    	    
		    CASE diva_autoscale_on OF
			0:  BEGIN
				if (n_dim eq 1) then begin
				    tv,ishft(pimage,bitshift_val)
				endif else begin
				    tv,ishft(pimage,bitshift_val),true=3
				endelse
			    END
			1:  BEGIN
				if (n_dim eq 1) then begin
				    tvscl,pimage
				endif else begin
				    tvscl,pimage,true=3
				endelse
			    END
		    ENDCASE
			
		    widget_control,(*h_event_struct.phist_info).$
			    	diva_draw_all_base,TLB_Set_Title=   	    $
				'orig size = '	+   	    	    	    $
				strcompress(xsize,/remove)+'x'+     	    $ 
				strcompress(ysize,/remove)+ '   '+  	    $ 	
				'new size = '+	    	    	    	    $
				strcompress(event.x,/remove)+'x'+   	    $
				strcompress(event.y,/remove)
    	    	end		
		
ELSE: MESSAGE, "Event User Value Not Found"		
ENDCASE
WIDGET_CONTROL, event.id, sET_UVALUE = h_event_struct,/no_copy
END ;end of diva_draw_all event handling routine task--------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_draw_all,pimage,xsize,ysize,n_dim,diva_autoscale_on,bitshift_val,$
    	group=group
;------------------------------------------------------------------------------;
; Purpose:
;------------------------------------------------------------------------------;

diva_draw_all_base=widget_base(title='DIVA ALL',/col,tlb_size_events=1)

screen_size=get_screen_size(RESOLUTION=resolution)
xsize_screen=screen_size(0)
ysize_screen=screen_size(1)

aspect_ratio=float(ysize)/float(xsize)

phist_info=Ptr_New({ 	    	    	    	    	    	    	    $  
    	    	    diva_draw_all_base	:diva_draw_all_base,	    	    $
    	    	    diva_draw_all_draw	:0L,     	    	    	    $
		    xsize   	    	:800,     	    	    	    $
		    ysize   	    	:800,     	    	    	    $
		    n_dim   	    	:n_dim,     	    	    	    $
		    xsize_screen	:xsize_screen,			    $
		    ysize_screen	:ysize_screen,			    $
		    newx    	    	:0, 	    	    	    	    $
		    newy    	    	:0, 	    	    	    	    $
		    aspect_ratio    	:aspect_ratio,	    	    	    $
		    diva_autoscale_on	:diva_autoscale_on, 	    	    $
		    bitshift_val    	:bitshift_val,	    	    	    $
		    tlbxsize	    	:0L,	    	    	    	    $
		    tlbysize	    	:0L,	    	    	    	    $
		    pimage  	    	:pimage     	    	    	    $
	    	  })
		  
diva_draw_all_draw=widget_draw(diva_draw_all_base,xsize=800,ysize=800,  $
    	uvalue={eventval:"DRAW",phist_info:phist_info},/motion_events)
(*phist_info).diva_draw_all_draw=diva_draw_all_draw

;-------------------quit button------------------------------------------------;
quitbutton=widget_button(diva_draw_all_base, value='Quit',  	    	    $
    	    uvalue={eventval:'EXIT',phist_info:phist_info})
;------------------------------------------------------------------------------;


widget_control,diva_draw_all_base, /REALIZE

widget_control,diva_draw_all_base,TLB_GET_Size=newsize
(*phist_info).tlbxsize=newsize[0]
(*phist_info).tlbysize=newsize[1]

widget_control,diva_draw_all_draw,get_value=all_index
wset,all_index

    CASE diva_autoscale_on OF
    	0:  BEGIN
	    	if (n_dim eq 1) then begin
		    tv,ishft(congrid(*pimage,800,800),bitshift_val)
		endif else begin
		    tv,ishft(congrid(*pimage,800,800,n_dim),bitshift_val),true=3
		endelse
	    END
    	1:  BEGIN
	    	if (n_dim eq 1) then begin
		    tvscl,congrid(*pimage,800,800)
		endif else begin
		    tvscl,congrid(*pimage,800,800,n_dim),true=3
		endelse
	    END
    ENDCASE

widget_control,(*phist_info).diva_draw_all_base,set_uvalue= 	    	    $
    	{eventval:"BASE", phist_info:phist_info}    	

XManager,"diva_draw_all",diva_draw_all_base, 	    	    	    	    $	
		EVENT_HANDLER = "diva_draw_all_ev", 	    	    	    $
		GROUP_LEADER = GROUP

end;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
