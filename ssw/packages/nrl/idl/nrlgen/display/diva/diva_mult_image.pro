;------------------------------------------------------------------------------;
Pro diva_mult_image__define
;------------------------------------------------------------------------------;
;Purpose - 
;------------------------------------------------------------------------------;
    junk = {	    diva_mult_image,   	    	    	        $
    	    	    file_info	    	:   {diva_file},    	$
		    		mfile_name	    	:   '',     	    	$
		    		mfile_name_str    	:   '',     	    	$
		    		mfile_date	    	:   '',     	    	$
		    		mfile_time	    	:   '',     	    	$
		    		mfile_pos_from  	:   0L,     	    	$
		    		mfile_pos_to  		:   0L,     	    	$
		    		mfile_fits_ext  	:   0	    	    	$
			}
END;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_mult_image_init,diva_mult_image,mfile_pos_from,mfile_pos_to,	    $
    	    	    	mfile_name,mfile_date,mfile_time,mfile_fits_ext
;------------------------------------------------------------------------------;
; Purpose -
;------------------------------------------------------------------------------;
diva_mult_image.mfile_pos_from  =   mfile_pos_from
diva_mult_image.mfile_pos_to   	=   mfile_pos_to
diva_mult_image.mfile_name  	=   mfile_name
diva_mult_image.mfile_date  	=   mfile_date
diva_mult_image.mfile_time  	=   mfile_time
diva_mult_image.mfile_fits_ext	=   mfile_fits_ext

n = mfile_name
test = strpos(n,'.')
if (test ne -1) then begin
    diva_mult_image.mfile_name_str  =   strmid(n,0,strpos(n,'.'))
endif else begin
    diva_mult_image.mfile_name_str  =	n
endelse

end;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
