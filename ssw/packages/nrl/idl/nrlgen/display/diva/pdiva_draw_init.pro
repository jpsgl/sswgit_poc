;------------------------------------------------------------------------------;
;+
; NAME:
;		PDIVA_DRAW_INIT
; PURPOSE:
;		This function creates and initialises the "pdiva_draw" structure.
; INPUTS:
;		tlb - is DIVA's top level base widget id
;		tlb_mid_base	- is the widget id of the main panel
;		diva_draw_base - is the widget id of the widget base which
;		 	includes the main draw area. It is destroyed and re-created
;		 	when a new image is loaded
;		diva_tlb_drawwid	- is the widget id of the main draw area
;		diva_tlb_pixindex	- is the widget id of the main draw area pixmap
;		r,g,b	- are the returned RGB values from the internal
;		 	colour table, using "IDL> TVLCT,r,g,b,/GET"
;		os_name - is determined from !D.name
; AUTHOR:
;		John A. Rainnie - RAL (j.a.rainnie@rl.ac.uk)
;-
;------------------------------------------------------------------------------;
FUNCTION pdiva_draw_init,tlb,tlb_mid_base,diva_draw_base,  	    	    	$
    	    	    			diva_tlb_drawwid,diva_tlb_pixindex, 		$
								r,g,b,os_name,tlb_drawwid_size, 			$
								tlb_prev_drawwid,diva_printers, 			$
								diva_printers_list
;------------------------------------------------------------------------------;
plot_r    = r  	    &	plot_g    = g  	    &	plot_b    = b

plot_r[1] = 255     &	plot_g[1] = 0       &	plot_b[1] = 0      ;1 = red       
plot_r[2] = 0 	    &	plot_g[2] = 255     &	plot_b[2] = 0      ;2 = green     
plot_r[3] = 0 	    &	plot_g[3] = 0       &	plot_b[3] = 255	   ;3 = blue      
plot_r[4] = 0	    &	plot_g[4] = 255     &	plot_b[4] = 255	   ;4 = cyan      
plot_r[5] = 255     &	plot_g[5] = 255     &	plot_b[5] = 255	   ;5 = white     
plot_r[6] = 0	    &	plot_g[6] = 0       &	plot_b[6] = 0 	   ;6 = black     
plot_r[7] = 255     &	plot_g[7] = 255     &	plot_b[7] = 0 	   ;7 = yellow    

image_ct  = [     [r],     [g],     [b]]
plot_ct   = [[plot_r],[plot_g],[plot_b]]

pdiva_draw =  Ptr_New({  													$
				tlb	    	    			:	tlb,	    	    	    $
    	     	os_name	    	    	    :	os_name,    	    	    $
    	     	diva_current_image     	    :	{diva_image},	    	    $
				diva_current_prev_image 	:	{diva_image},	    	    $
	     		tlb_mid_base   	            :	tlb_mid_base,				$
	     		diva_draw_base 	            :	diva_draw_base, 			$
    	     	diva_tlb_drawwid			:	diva_tlb_drawwid,   	    $
	    		diva_tlb_pixindex			:	diva_tlb_pixindex,  	    $
	    		tlb_drawwid_size			:	tlb_drawwid_size,   	    $
				diva_prev_button_base		: 	0L, 						$
	    		tlb_prev_drawwid			:	tlb_prev_drawwid,   	    $
	    		tlb_prev_pixid 	    	    :	0L, 						$
	    		tlb_prev_coords	    	    :	[0,0,0,0,0,0],  	    	$
	    		tlb_prev_xy    	    	    :	[0,0],	    	    	    $
	    		tlb_prev_motion_evs    	    :	0,  	    	    	    $
				prev_draw_buttons_show		:	0,							$
	    		tlb_image_function			:	1,	    	    	    	$
	    		diva_autoscale				:	[1,0], 	    	    	    $
	    		tlb_top_base   	    	    :	0L, 	    	    	    $
	    		tlb_bshift_base	    	    :	0L, 	    	    	    $
	    		tlb_bshift_text	    	    :	0L, 	    	    	    $
	    		tlb_bshift_save 			:	0L, 						$
				tlb_info_base  	            :	0L,			    			$
	    		tlb_info_curs_t	            :	0L,			    			$
    	     	tlb_info_valu_t	            :	0L,			    		    $
	    		tlb_info_func_text  	    :	0L, 	    	    	    $
	    		tlb_info_size_text  	    :	0L, 	    	    	    $
	    		xs     	    	            :	0L,			                $
	    		ys     	    	            :	0L,			                $
	    		xd     	    	            :	0L,			                $
	    		yd     	    	            :	0L,			                $
	    		motion_events_on			:	0,			                $
	    		diva_line_info 	    	    :	Ptr_New(),  	    	    $
	    		diva_zoom_info 	    	    :	Ptr_New(),  	    	    $
	    		diva_roi_info  	    	    :	Ptr_New(),  	    	    $
				diva_image_stored 			:	Ptr_New(),					$
				diva_conc_info				:	Ptr_New(),					$
				diva_math_info				:	Ptr_New(),					$
				diva_whattoprint			:	0,			    			$
	    		diva_roi_irreg_xpts 		:	intarr(100),		        $
	    		diva_roi_irreg_ypts 		:	intarr(100),		        $
	    		diva_roi_irreg_retainx		:	intarr(100),		        $
	    		diva_roi_irreg_retainy		:	intarr(100),		        $
	    		diva_roi_irreg_counter 	    :	0,  	    	    	    $
	    		diva_rband_box 	    	    :	[2,1],	    	    	    $
				diva_histbase				:	0L,							$
    	     	image_ct					:	image_ct,				    $
	    		plot_ct	    	    	    :	plot_ct,     	    	    $
	    		diva_fit_size  	    	    :	0,   	    	    	    $
				diva_printers				:	diva_printers,				$
				diva_printers_list			:	diva_printers_list			$
    	    })

RETURN,pdiva_draw
	    
END;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
