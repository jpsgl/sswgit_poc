;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA_READ_DATA_IMG
; PURPOSE:
;		This procedure reads data files of (RAL's) IMG type.
; INPUTS:
;		tmp_diva_image_file - a "diva_image" structure
; OUTPUTS:
;		tmp_diva_image_file - a "diva_image" structure
; NOTES:
;		The first image is read, and an end of file check is made to 
;		find out whether the file contains multiple images (if this is
;		the case, then "DIVA_LOGFILE_READ" procedure is called).
;
;		All IMG format images are unsigned integer type
; PROCEDURES USED:
;		FUNCTIONS:	DIVA_IMAGE_INIT (diva_image.pro)
; AUTHOR:
;		John A. Rainnie - RAL (j.a.rainnie@rl.ac.uk)
;-
;------------------------------------------------------------------------------;
PRO diva_read_data_img,tmp_diva_image_file
;------------------------------------------------------------------------------;
file_info	= tmp_diva_image_file.file_info

openr,unit,file_info.f,/GET_LUN

file_size=FSTAT(unit)
file_size=file_size.size

tmp_diva_image_file.file_info.file_unit=unit

header	= UINTARR(4)
READU,unit,header

image	= UINTARR(header[0],header[1],header[2])
READU,unit,image

end_of_file=EOF(unit)

FREE_LUN,tmp_diva_image_file.file_info.file_unit
tmp_diva_image_file.file_info.file_unit =   0

;--------------------------------------------------------------------------;
;check for image files with extra 512 bytes of ascii text
so_far = LONG(header[0]) * LONG(header[1]) * 2 + 6
IF (end_of_file eq 0) AND (file_size - so_far eq 512) THEN end_of_file=1
;------------------------------------------------------------------------------;

tmp_diva_image_file.xsize	= header[0]
tmp_diva_image_file.ysize	= header[1]
tmp_diva_image_file.n_dim   = header[2]
tmp_diva_image_file.n_byt   = header[3]

CASE end_of_file OF
	0:	BEGIN	; MULTIPLE IMAGES
			logfile_name=strcompress(	file_info.pathname +				$
    	    	    					file_info.file_str + '.txt',/remove)
			;call 'diva_logfile_read' procedure
			diva_logfile_read,logfile_name,mfile_pos_from,    	    		$
				    mfile_pos_to,mfile_name,mfile_date,mfile_time
		
			
			;update structure with number of images
			mfile_pos_from  = LONG(mfile_pos_from)
			mfile_pos_to    = LONG(mfile_pos_to)
			
			tmp_diva_image_file.mfile_pos_from	=	mfile_pos_from[0]		 		
			tmp_diva_image_file.mfile_pos_to    =	mfile_pos_to[0] 		 		
			tmp_diva_image_file.mfile_name	    =	mfile_name[0]			 		
			tmp_diva_image_file.mfile_date	    =	mfile_date[0]			 		
			tmp_diva_image_file.mfile_time	    =	mfile_time[0]			 		
			tmp_diva_image_file.diva_n_images	=	N_ELEMENTS(mfile_name)   		
			
			n = tmp_diva_image_file.mfile_name
			tmp_diva_image_file.mfile_name_str	=	strmid(n,0,strpos(n,'.'))		
		END
	1:	BEGIN	; SINGLE IMAGE
			tmp_diva_image_file.mfile_pos_from	=   ['']
    		tmp_diva_image_file.mfile_pos_to	=   ['']
    		tmp_diva_image_file.mfile_name		=   ['']
    		tmp_diva_image_file.mfile_date		=   ['']
    		tmp_diva_image_file.mfile_time		=   ['']
    		tmp_diva_image_file.diva_n_images	=    1
		END
ENDCASE


;call 'diva_image_init' function to populate 'diva_image' structure
diva_image = diva_image_init(tmp_diva_image_file,image)
;and update 'tmp_diva_image_file' structure
tmp_diva_image_file = diva_image

END ;of "diva_read_data_img" procedure-----------------------------------------;
;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA_IMAGE_FILE_READ_MULTIPLE
; PURPOSE:
;		This procedure reads an IMG type multiple file.
; INPUTS:
;		tmp_diva_image_file - a "diva_image" structure
; OUTPUTS:
;		tmp_diva_image_file - a "diva_image" structure
; PROCEDURES USED:
;		FUNCTIONS:	READ_BINARY
;					DIVA_IMAGE_INIT
; AUTHOR:
;		John A. Rainnie - RAL (j.a.rainnie@rl.ac.uk)
;-
;------------------------------------------------------------------------------;
PRO diva_image_file_read_multiple,tmp_diva_image_file
;------------------------------------------------------------------------------;
read_from = tmp_diva_image_file.mfile_pos_from

file_info	= tmp_diva_image_file.file_info

openr,1,file_info.f

header=READ_BINARY(1,data_type=12,data_start=read_from,data_dims=4)

image = UINTARR(header[0],header[1],header[2])
;img format is always UINT type!!!	- IDL data type = 12
READU,1,image
close,1

tmp_diva_image_file.xsize	= header[0]
tmp_diva_image_file.ysize	= header[1]
tmp_diva_image_file.n_dim   = header[2]
tmp_diva_image_file.n_byt   = header[3]


;call 'diva_image_init' function to populate 'diva_image' structure
diva_image = diva_image_init(tmp_diva_image_file,image)
;and update 'tmp_diva_image_file' structure
tmp_diva_image_file = diva_image


end ;of 'diva_image_file_read_multiple' procedure------------------------------;
;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA_LOGFILE_READ
; PURPOSE:
;		This procedure reads an IMG type log (ascii) file. It consists
;		of an ascii table (1 line per image), containing information
;		about that particular image (see OUTPUTS)
; INPUTS:
;		logfile_name	- the name of the log file to read
; OUTPUTS:
;		mfile_pos_from	- first byte to read
;		mfile_pos_to	- last byte to read
;		mfile_name		- 
;		mfile_date		-
;		mfile_time		-
; AUTHOR:
;		John A. Rainnie - RAL (j.a.rainnie@rl.ac.uk)
;-
;------------------------------------------------------------------------------;
PRO diva_logfile_read,logfile_name,mfile_pos_from,    	    				$
    	    mfile_pos_to,mfile_name,mfile_date,mfile_time
;------------------------------------------------------------------------------;
dummy=''

openr,1,logfile_name

WHILE (NOT EOF(1)) DO BEGIN
    line=''
    READF,1,format='(%"%s")',line
    dummy=[dummy,line]
ENDWHILE

close,1
dummy=dummy[1:*]

mfile_pos_from  = strarr(n_elements(dummy))
mfile_pos_to    = strarr(n_elements(dummy))
mfile_name      = strarr(n_elements(dummy))
mfile_date      = strarr(n_elements(dummy))
mfile_time      = strarr(n_elements(dummy))


FOR i=0,N_ELEMENTS(dummy)-1 DO BEGIN
    dummy(i)=strcompress(dummy(i),/remove)

    mfile_pos_from[i]   = strmid(dummy[i],0,11)
    mfile_pos_to[i]     = strmid(dummy[i],11,11)
    mfile_time[i] 		= strmid(dummy[i],7,/reverse_offset)
    mfile_date[i] 		= strmid(dummy[i],15,8,/reverse_offset)
    mfile_name[i] 		= strmid(dummy[i],22)
    mfile_name[i]		= strmid(mfile_name[i],0,strlen(mfile_name[i])-16)
ENDFOR


END ;of 'diva_logfile_read' procedure -----------------------------------------;
;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA_LOGFILE_SIMPLE_READ
; PURPOSE:
;		This procedure
; INPUTS:
;		lf - the filename of the selected log file
; OUTPUTS:
;		str_array - a string array  
; PROCEDURE:
;		The log file is read until the end of the file is reached, and 
;		each line is appended into a string array 
; NOTE:
;		This procedure is called from "DIVA_EV": eventval = "PREV_VLOG", 
;		where file format is IMG type. The output ("str_array") is simply 
;		displayed in a GUI ("DIVA_FIT_HEADER").
; AUTHOR:
;		John A. Rainnie - RAL (j.a.rainnie@rl.ac.uk)
;-
;------------------------------------------------------------------------------;
PRO diva_logfile_simple_read,lf,str_array
;------------------------------------------------------------------------------;

dummy=''

openr,1,lf

WHILE (NOT EOF(1)) DO BEGIN
    line=''
    readf,1,format='(%"%s")',line
    dummy=[dummy,line]
ENDWHILE

close,1
str_array=dummy[1:*]

END ; of "diva_logfile_simple_read" procedure----------------------------------;
;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA_AD_GET_DATA_FILE
; AUTHOR:
;		John A. Rainnie - RAL (j.a.rainnie@rl.ac.uk)
; PURPOSE:
;		This function returns the filename of the data file
;		corresponding to the input log file name.
; INPUTS:
;		pick_log - the filename of the selected log file
; OUTPUTS:
;		pick_data - the filename of the corresponding data file 
; PROCEDURE:
;		The file extension ".txt" is removed and replaced with a wildcard.
;		A search (FINDFILE) is made to find other files of similar names.
;		The other filename (there should only ever be 1) is the one we want
; NOTE:
;		This function is only required when IMG format files are selected
;		in adapative mode. It is the log file that is selected - not the
;		data file!
;-
;------------------------------------------------------------------------------;
FUNCTION diva_ad_get_data_file,pick_log
;------------------------------------------------------------------------------;

;create file_specification (see IDL help "FINDFILE") to search for
pick_log_str	= strmid(pick_log,0,strpos(pick_log,'.',/reverse_search))+'.*'
;find all files
r				= findfile(pick_log_str)

;remove pathanmes - they confuse stupid Windoze
FOR i= 0, n_elements(r) - 1 DO r[i] =										$
					strmid(r[i],strpos(r[i],'\',/reverse_search)+1)

;do the same with 'pick_log'
p				= pick_log
p				= strmid(p,strpos(p,'\',/reverse_search)+1)

;get the pathame
pathname 		= strmid(pick_log,0,strpos(pick_log,'\',/reverse_search)+1)

c				=strmatch(r,p)
nr				=r(WHERE(c eq 0))

CASE n_elements(nr) OF
    0:	    pick_data	= ''	
    1:      pick_data	= strcompress(pathname+nr,/remove)
    ELSE:   stop
ENDCASE

RETURN,pick_data

END ; of "diva_ad_get_data_file" function--------------------------------------;
;------------------------------------------------------------------------------;
