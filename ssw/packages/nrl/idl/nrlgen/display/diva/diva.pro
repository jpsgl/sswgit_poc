;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA_EV
; AUTHOR:
;		John A. Rainnie - RAL (j.a.rainnie@rl.ac.uk)
; PURPOSE:
;		This procedure is the event handler of main DIVA GUI
; CATEGORY:
;		EVENT HANDLER
; CALLING SEQUENCE:
;		@d ("d.pro" is a batch file)
; INPUTS:
;		event --> "info" structure
; PROCEDURE:
;		obvious
; NOTES:
; PROCEDURES USED:
;		millions.....
; MODIFICATION HISTORY:
;		Original Version written in 2003 by John A. Rainnie at RAL
;-
;------------------------------------------------------------------------------;
PRO diva_ev, event

WIDGET_CONTROL, event.id, GET_UVALUE = eventval
widget_control,event.top,get_uvalue=info

diva_autoscale      	= (*(*info).pdiva_draw).diva_autoscale
diva_zoom_info	    	= (*(*info).pdiva_draw).diva_zoom_info
diva_line_info	    	= (*(*info).pdiva_draw).diva_line_info
diva_tlb_drawwid    	= (*(*info).pdiva_draw).diva_tlb_drawwid
diva_tlb_pixindex		= (*(*info).pdiva_draw).diva_tlb_pixindex
diva_draw_base	    	= (*(*info).pdiva_draw).diva_draw_base
diva_fit_size	    	= (*(*info).pdiva_draw).diva_fit_size
tlb_prev_drawwid    	= (*(*info).pdiva_draw).tlb_prev_drawwid
tlb_prev_pixid	    	= (*(*info).pdiva_draw).tlb_prev_pixid
plot_ct	 				= (*(*info).pdiva_draw).plot_ct
image_ct	 			= (*(*info).pdiva_draw).image_ct
 
if  (TAG_NAMES(event, /STRUCTURE_NAME) EQ 'WIDGET_CONTEXT') OR				$
	(TAG_NAMES(event, /STRUCTURE_NAME) EQ 'WIDGET_TIMER')	THEN BEGIN
 
if (TAG_NAMES(event, /STRUCTURE_NAME) EQ 'WIDGET_CONTEXT') THEN BEGIN
	
	prev_contextbase = (*info).prev_contextbase
	widget_displaycontextmenu, event.id, event.x, event.y, prev_contextbase
	
endif  ;of condition event.tag=widget_context



IF (TAG_NAMES(event, /STRUCTURE_NAME) EQ 'WIDGET_TIMER') THEN BEGIN
    if ((*info).diva_adapt_mode_on eq 1) then begin
    	if ((*info).diva_time_events_on eq 1) then begin
    	    
	    	widget_control,(*info).diva_ad_buttons[0],TIMER=	    	    $
	    	    	    	    	    	    (*info).tlb_ad_timer[0]
	    
	    	timer = (*info).tlb_ad_timer[1]
    	    CASE timer OF
    	    	0:  BEGIN
		    			(*info).tlb_ad_timer[1] = 1
						widget_control,(*info).tlb_adapt_animate,set_value=''
		    		END
    	    	1:  BEGIN
		    			(*info).tlb_ad_timer[1] = 0
						widget_control,(*info).tlb_adapt_animate,set_value='*'
		    		END
    	    ENDCASE

	    	number_images = (*info).number_images
	    
	    	f 	= (*info).diva_ad_pick_file[0]	;pick_log
	    	ext = strmid(f,  strpos(f,'.',/reverse_search)+1)
	    
	    	;IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_
			;IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_
			;IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_
	    	if (ext eq 'txt') then begin
				n_dummy=''
				openr,1,(*info).diva_ad_pick_file[0]
				WHILE (NOT EOF(1)) DO BEGIN
					line=''
					readf,1,format='(%"%s")',line
					n_dummy=[n_dummy,line]
				ENDWHILE
				close,1
				n_dummy=n_dummy(1:*)
	    	endif else begin
	    	;FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS
	    	;FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS
	    	;FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS
				diva_fits_help,f,get_fname,get_fdate,get_ftime
				n_dummy = get_fname
	    	endelse
	    
	    
	    	;BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH
	    	;BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH
	    	;BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH

	    	new_number_images=n_elements(n_dummy)
	    	n = new_number_images-number_images
	    	;the condition that a new image has been detected
	    	if (number_images ne new_number_images) then begin
				print,'*********************************************'
				print,'number of new images detected = ' + strcompress(n,/rem)
				print,'*********************************************'

				(*(*info).pdiva_draw).diva_autoscale = [1,0]
				
				
				(*info).number_images 	=    	   new_number_images
				mfile_pos_from  		=   strarr(new_number_images)
				mfile_pos_to			=   strarr(new_number_images)
				mfile_name				=   strarr(new_number_images)
				mfile_name_str  		=   strarr(new_number_images)
				mfile_date				=   strarr(new_number_images)
				mfile_time				=   strarr(new_number_images)
				mfile_fits_ext			=   intarr(new_number_images)

				;FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS
				;FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS
				;FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS
				if (ext eq 'fit') OR (ext eq 'fits') OR (ext eq 'fts') then begin
		    		;stop
		    		FOR i=0,n_elements(n_dummy)-1 DO BEGIN
		    			mfile_name[i]	    = get_fname[i]
		    			mfile_fits_ext[i]   = i
						mfile_date[i]	    = get_fdate[i]
						mfile_time[i]	    = get_ftime[i]
						mfile_pos_from[i]   = 0
						mfile_pos_to[i]     = 0

						IF (strpos(get_fname[i],'.') eq -1) 	THEN		$
							mfile_name_str[i] = get_fname[i] 	ELSE		$
							mfile_name_str[i] = 							$
							strmid(get_fname[i],0,strpos(get_fname[i],'.'))
					ENDFOR

				endif else begin
				;IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG
				;IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG
				;IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG
		    		for i=0,n_elements(n_dummy)-1 do begin
						n_dummy[i]		  =strcompress(n_dummy[i],/remove)

    					mfile_pos_from[i] = strmid(n_dummy[i],0,11)
    					mfile_pos_to[i]   = strmid(n_dummy[i],11,11)
    					mfile_time[i]     = strmid(n_dummy[i],7,/reverse_offset)
    					mfile_date[i]     = strmid(n_dummy[i],15,8,/reverse_offset)
						mfile_name[i]     = strmid(n_dummy[i],22)
    					mfile_name[i]     = strmid(mfile_name[i],0, 		$
		    	    	        	    		  strlen(mfile_name[i])-16)
						mfile_name_str[i] = strmid(mfile_name[i],0, 		$
	    	    	    				  	strpos(mfile_name[i],'.',		$
															/reverse_search))
						mfile_fits_ext[i] = 0
		    		endfor

				endelse

				;BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH
				;BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH
				;BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH


				for i=0,n_elements(n_dummy)-1 do							$
            	    	(*info).diva_ad_new_list[i]      =  mfile_time[i] + $
		    	    		'  ' + mfile_date[i] + '  ' + mfile_name[i]

				pick_log    =	(*info).diva_ad_pick_file[0]
				pick_data   =	(*info).diva_ad_pick_file[1]

				;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				;OK, let's create a 'diva_mult_image' structure for each new
				;image found. Then we'll append that onto our original
				diva_file   =	{diva_file}
				f   	    = 	pick_data
				diva_file_init,diva_file,f


				diva_mult_image     = REPLICATE({diva_mult_image},n)
				tmp_diva_mult_image = diva_mult_image

				FOR i=0,n - 1 DO BEGIN
		    		diva_mult_image_init,tmp_diva_mult_image,				$
		    	    			mfile_pos_from[i+number_images],    	    $
								mfile_pos_to[i+number_images],	    	    $
								mfile_name[i+number_images], 	    	    $
								mfile_date[i+number_images],	    	    $
								mfile_time[i+number_images],	    	    $
								mfile_fits_ext[i+number_images]

		    		tmp_diva_mult_image[i].file_info = diva_file[0]
		    		diva_mult_image[i] =  tmp_diva_mult_image[i]
				ENDFOR

				;OK, now let's append the new array image structures to the
				;existing array
				old_diva_mult_image = (*(*info).diva_mult_image)
				new_diva_mult_image = diva_mult_image
				;append here  
				diva_mult_image = [old_diva_mult_image,new_diva_mult_image]
				;update here
				(*(*info).diva_mult_image) = diva_mult_image

				d = (*info).diva_ad_new_list
				
				widget_control,(*info).tlb_adapt_list,update=0
				widget_control,(*info).tlb_adapt_list,set_value=d(where(d ne ''))

				;AUTO-LOAD stuff here!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				diva_ad_autoload_on = (*info).diva_ad_autoload_on

				if (diva_ad_autoload_on eq 1) then begin
		    		index = new_number_images-1
		    		(*info).diva_ad_list_index = index

		    		mult = (*(*info).diva_mult_image)[index]
		    		temp = (*(*info).pdiva_draw).diva_current_prev_image
					
		    		temp.file_info	    	= mult.file_info
			    	temp.mfile_pos_from 	= mult.mfile_pos_from
			    	temp.mfile_pos_to		= mult.mfile_pos_to
			    	temp.mfile_name 		= mult.mfile_name
			    	temp.mfile_name_str 	= mult.mfile_name_str
			    	temp.mfile_date 		= mult.mfile_date
			    	temp.mfile_time 		= mult.mfile_time
					
					num_images = n_elements((*(*info).diva_mult_image))
					
					;FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS
					;FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS
		    		;FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS
		    		IF (ext eq 'fit') OR (ext eq 'fits') OR (ext eq 'fts')  $
					THEN BEGIN

			    		num_images = n_elements((*(*info).diva_mult_image))
			    		;call procedure to read multiple image
			    		diva_read_data_fit,temp,index,num_images

		    		ENDIF ELSE BEGIN
					;IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG
					;IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG
		    		;IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG

		    			;call 'diva_image_file_read_multiple'
						diva_image_file_read_multiple,temp
					
						temp.diva_n_images  = num_images
						
		    		ENDELSE

		    		;BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_
					;BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_
					;BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_

		    		;... and update structure with new values
    	    		(*(*info).pdiva_draw).diva_current_prev_image 	= temp

					;OK: update 'diva_current_image' structure
					Ptr_Free,(*(*info).pdiva_draw).diva_current_image.pimage
					Ptr_Free,(*(*info).pdiva_draw).diva_current_image.o_pimage
					Ptr_Free,(*(*info).pdiva_draw).diva_current_image.p_pimage
					curr_prev 	= temp
					diva_image_copy, curr_prev, new_curr
					
					(*(*info).pdiva_draw).diva_current_image = new_curr

										
		    		;call 'diva_draw_prev' procedure to display preview image
		    		widget_control,tlb_prev_drawwid,get_value = prev_drawwid
		    		diva_draw_prev,prev_drawwid,tlb_prev_pixid,temp.n_dim,	$
		    					    		temp.p_pimage

		    		;update file size and dimensions info (label) widgets
		    		tmp = {t:[      strcompress(temp.mfile_name,/rem),		$
		    		    			strcompress(temp.mfile_size,/rem), 	$
		    		    			strcompress(temp.xsize,/rem),			$
		    		    			strcompress(temp.ysize,/rem),			$
		    		    			strcompress(temp.n_dim,/rem)]}

		    		;call 'diva_report_prev_info' to update info labels
		    		diva_report_prev_info,(*info).prev_info,tmp


					widget_control,(*info).tlb_prev_size_label_id,			$
						set_value =	'[Bytes: ' +							$
							strcompress(temp.file_info.filesize,/remove) +	$
							']  [Images: ' +								$
							strcompress(new_number_images,/remove)+']'
					
					
					;call 'diva_draw_create' to kill and recreate tlb draw area
		    		diva_draw_create,(*info).pdiva_draw
		    		;... then display image
		    		diva_draw_image, (*info).pdiva_draw
		    		;....and finally update tlb title
		    		diva_tlb_title,  (*info).pdiva_draw
		    		;call 'diva_draw_prev_viewport' procedure
		    		diva_draw_prev_viewport,(*info).pdiva_draw

		    		widget_control,(*info).diva_file_buttids[0],sensitive=1
		    		widget_control,(*info).diva_menus[0],sensitive=1
		    		widget_control,(*info).diva_menus[1],sensitive=1
		    		widget_control,(*info).diva_menus[2],sensitive=1
		    		widget_control,(*info).diva_menus[3],sensitive=1


				endif  ;of condition diva_ad_autoload is TRUE
				
				
				;make sure selected image entry remains highlighted
				widget_control,(*info).tlb_adapt_list,set_list_select = 	$
				    	    	    			(*info).diva_ad_list_index
				
				widget_control,(*info).tlb_prev_load_butt,sensitive = 0
					 
	    	endif  ;of condition that a new image has been detected
    	end  ;of condition are_time_evs_on = 1 (ie TRUE)
    end  ;of condition adaptive mode is ON
endif
ENDIF ELSE BEGIN  ;of condition event.tag=widget_timer

CASE eventval OF
;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DIVA_EXIT":	$   ;DIVA_EXIT_DIVA_EXIT_DIVA_EXIT_DIVA_EXIT_DIVA_EXIT_DIVA
BEGIN
    WIDGET_CONTROL, event.top, /DESTROY
    RETURN
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DIVA_OPEN":	$   ;DIVA_OPEN_DIVA_OPEN_DIVA_OPEN_DIVA_OPEN_DIVA_OPEN_DIVA
BEGIN
	CASE (*info).os_name OF
	    'X':    	path = (*info).def_path[0]
	    'WIN':  	path = (*info).def_path[1]
	ENDCASE
	;call "diva_file" procedure to diaplay an OPEN FILE(s) dialog.
	f = diva_file((*info).def_file,path,'preview')
	;if this was cancelled, then RETURN
	IF (f[0] eq '') THEN RETURN 
    ;**************************************************************************
	;clean up if adaptive mode was on.....
	(*info).diva_ad_new_list 	= strarr(500)
    (*info).diva_adapt_mode_on  = 0
    (*info).diva_time_events_on = 0
    ;....and reset to 'preview mode'
    widget_control,(*info).tlb_prev_mode_label_id,set_value =				$
			    						'******   PREVIEW  MODE   ******'
    widget_control,(*info).tlb_prev_name_label_id,set_value = 'none'
    ;**************************************************************************
	;call "diva_prev_list_sensitive" to sensitize preview lists
	doh = [1,1,0,0,1,0,0]
	diva_prev_list_sensitive,(*info).tlb_preview_list,doh

	widget_control,(*(*info).pdiva_draw).diva_prev_button_base,map = 0
	(*(*info).pdiva_draw).prev_draw_buttons_show = 0

	wset,tlb_prev_pixid & erase
	widget_control,tlb_prev_drawwid,get_value = prev_drawwid
	wset,prev_drawwid & erase


	tmp = {t:['none','0','0','0','0']}
	;call 'diva_report_prev_info' to update info labels
	diva_report_prev_info,(*info).prev_info,tmp

    ;define and initialise 'diva_file_info' structure
	diva_file_info = REPLICATE({diva_file},N_ELEMENTS(f))

	tmp_diva_file_info = {diva_file}
	FOR i=0,N_ELEMENTS(f)-1 DO BEGIN
	    diva_file_init, tmp_diva_file_info, f[i]
	    diva_file_info[i]=tmp_diva_file_info
	ENDFOR

	if (ptr_valid((*info).diva_file_info) eq 0) then BEGIN
	    (*info).diva_file_info = PTR_NEW(diva_file_info)
	endIF ELSE BEGIN
	    (*(*info).diva_file_info) =  [(*(*info).diva_file_info), 	    $
			    	    	    	    	diva_file_info]
	ENDELSE

	widget_control,(*info).tlb_prev_list,set_value	=     	    	    $
		    	    	(*(*info).diva_file_info)[*].filename

	widget_control,(*info).tlb_prev_size_label_id,set_value =			$
						'[Bytes: - ]  [Images: - ]'
	
	
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DIVA_EXPORT":	$
BEGIN
	curr 				= (*(*info).pdiva_draw).diva_current_image
	pimage  			= curr.pimage
	pathname			= curr.file_info.pathname
	diva_whattoprint 	= 0
	diva_autoscale  	= (*(*info).pdiva_draw).diva_autoscale
	
	diva_printers		= (*(*info).pdiva_draw).diva_printers
	diva_printers_list	= (*(*info).pdiva_draw).diva_printers_list
	image_ct			= (*(*info).pdiva_draw).image_ct
	plot_ct				= (*(*info).pdiva_draw).plot_ct
	
	
	CASE curr.diva_n_images OF
		1:		file_str = curr.file_info.file_str
		ELSE:	file_str = curr.file_info.file_str + '__' + curr.mfile_name
	ENDCASE
	
	;check for and remove extension
	check = strpos(file_str,'.')
	if (check ne -1) then file_str = 										$
					strmid(file_str, 0, strpos(file_str,'.',/reverse_search))
	
	
	;call "diva_export" procedure
	diva_export, pimage,pathname,file_str,diva_autoscale,   	    	    $
    	    	    	diva_whattoprint,diva_printers, 					$
						diva_printers_list,image_ct,plot_ct,				$
						group=(*info).tlb
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DIVA_HELP":    xdisplayfile,'diva_help.txt',width=60,	    	    	    $
		    	    	    	done_button='Quit',group=(*info).tlb
"DIVA_VERS":    diva_vers,GROUP = (*info).tlb
"DIVA_ABOUT":   diva_about,GROUP = (*info).tlb
"PREV_BACK":	$	;
BEGIN
    
    file_format = (*(*info).pdiva_draw).diva_current_prev_image.			$
													file_info.file_format
	
	CASE file_format OF
		'type_img': 	wot = [1,0,1,0,0,0,1]	;IMG
		'type_fit': 	wot = [0,0,1,1,0,0,1]	;FIT
		ELSE:			stop
	ENDCASE
	
	diva_prev_contextbase_sensitive,(*info).prev_context_list,wot
	diva_prev_buttons_sensitive,(*info).prev_buttons,wot
	
	(*info).diva_adapt_mode_on = 0
	widget_control,(*(*info).pdiva_draw).diva_prev_button_base,map = 0
	(*(*info).pdiva_draw).prev_draw_buttons_show = 0
	
	tmp = {t:['none','0','0','0','0']}
	;call 'diva_report_prev_info' to update info labels
	diva_report_prev_info,(*info).prev_info,tmp
    
	;call "diva_prev_list_sensitive" to sensitize preview lists
	doh = [1,1,0,0,1,0,0]
	diva_prev_list_sensitive,(*info).tlb_preview_list,doh	    
	
	widget_control,tlb_prev_drawwid,get_value = prev_drawwid
	wset,prev_drawwid	&   erase
	wset,tlb_prev_pixid & 	erase
	
	
	;if its there....kill widget table
	IF(XRegistered("diva_fit_header") eq 1) THEN BEGIN
		widget_control,(*info).diva_display_logbase,/destroy
	END
	
		    
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"PREV_VLOG":	$   ;PREV_VLOG_PREV_VLOG_PREV_VLOG_PREV_VLOG_PREV_VLOG_PREV
BEGIN
    ;if this widget exists, destroy it !!!!
	IF(XRegistered("diva_fit_header") eq 1) THEN 							$
						widget_control,(*info).diva_display_logbase,/destroy
	
    file_info	  = (*(*info).pdiva_draw).diva_current_prev_image.file_info
	f   		  = file_info.f
	lf			  = f
	file_format   = file_info.file_format
    diva_n_images = (*(*info).pdiva_draw).diva_current_prev_image.diva_n_images
	
    CASE file_format OF
		'type_img': BEGIN
						;in case of IMG type, f ne lf
						lf		= strmid(f,0,strpos(f,'.'))+'.txt'
						temp 	= (*(*info).diva_mult_image)
						diva_logfile_simple_read,lf,header
					END
		'type_fit': BEGIN
						CASE (*info).diva_adapt_mode_on OF
							1:		index = (*info).diva_ad_list_index
							ELSE:	IF (diva_n_images eq 1) THEN 			$
										index = 0 			ELSE  			$
										index = (*info).diva_prev_mult_index
						ENDCASE
						header  = headfits(lf,ext=index)
					END
		'type_jpg': header  = diva_get_jpg_info(lf)
		'type_bmp': header  = diva_get_bmp_info(lf)
	ENDCASE
	
	
	;call 'diva_fit_header' pro to realize the widget
	diva_fit_header,lf,header,base_id,GROUP = (*info).tlb
	;update structure
    (*info).diva_display_logbase = base_id
	
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DIVA_MULT":	$   ;DIVA_MULT_DIVA_MULT_DIVA_MULT_DIVA_MULT_DIVA_MULT_DIVA
BEGIN
    widget_control,/hourglass
	
	widget_control,(*(*info).pdiva_draw).diva_prev_button_base,map = 0
	(*(*info).pdiva_draw).prev_draw_buttons_show = 0
	
    (*info).diva_prev_mult_index = event.index

    temp = (*(*info).diva_mult_image)[event.index]

    tempii = (*(*info).pdiva_draw).diva_current_prev_image

    tempii.mfile_pos_from   = temp.mfile_pos_from
    tempii.mfile_pos_to     = temp.mfile_pos_to
    tempii.mfile_name	    = temp.mfile_name
    tempii.mfile_name_str   = temp.mfile_name_str
    tempii.mfile_time	    = temp.mfile_time
    tempii.mfile_date	    = temp.mfile_date
    tempii.mfile_size	    = temp.mfile_pos_to-temp.mfile_pos_from + 1 - 8

    PTR_FREE,tempii.pimage
    PTR_FREE,tempii.o_pimage
    PTR_FREE,tempii.p_pimage
    
    file_ext 	= tempii.file_info.file_ext
	file_format = tempii.file_info.file_format
    ;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ;split into 2 sections here, depending on whether file is IMG or FITS
    ;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    CASE file_format OF
		'type_img': BEGIN
						;call procedure to read multiple image
						diva_image_file_read_multiple,tempii
						wot = [1,1,0,0,1,1,0]
					END
		'type_fit':	BEGIN
						;call procedure to read multiple image
						num_images = tempii.diva_n_images
						diva_read_data_fit,tempii,event.index,num_images
						wot = [1,1,0,1,1,1,0]
						
						IF(XRegistered("diva_fit_header") eq 1) THEN 		$
							widget_control,(*info).diva_display_logbase,/destroy

					END
	ENDCASE

	
	;sensitize context list entries accordingly	
    diva_prev_contextbase_sensitive,(*info).prev_context_list,wot
	diva_prev_buttons_sensitive,(*info).prev_buttons,wot
	
	;update 'diva_current_prev_image' structure
    (*(*info).pdiva_draw).diva_current_prev_image = tempii

    tmp = {t:['__'+ strcompress(tempii.mfile_name,/rem),     	    		$
		    		strcompress(tempii.mfile_size,/rem),     	    		$
		    		strcompress(tempii.xsize,/rem),  	    	    		$
		    		strcompress(tempii.ysize,/rem),  	    	    		$
		    		strcompress(tempii.n_dim,/rem)]}
    
    
    ;call 'diva_report_prev_info' to update info labels
    diva_report_prev_info,(*info).prev_info,tmp
    ;set diva_preview draw widget as current
    widget_control,tlb_prev_drawwid,get_value = prev_drawwid
    
    ;call 'diva_draw_prev' procedure to display preview image
    diva_draw_prev,prev_drawwid,tlb_prev_pixid,tempii.n_dim,tempii.p_pimage
    
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DIVA_PREV":	$   ;DIVA_PREV_DIVA_PREV_DIVA_PREV_DIVA_PREV_DIVA_PREV_DIVA
BEGIN
    widget_control,/hourglass
	
	widget_control,(*(*info).pdiva_draw).diva_prev_button_base,map = 0
	(*(*info).pdiva_draw).prev_draw_buttons_show = 0
	
	widget_control,(*info).tlb_prev_load_butt,sensitive = 1
	
    (*info).diva_prev_index=event.index ;update index
    ;update selected file widget_label		
    f = (*(*info).diva_file_info)[event.index].f
    widget_control,(*info).tlb_prev_name_label_id,set_value = f

	
    tmp_diva_image_file 	      = (*(*info).pdiva_draw).diva_current_prev_image
    tmp_diva_image_file.file_info = (*(*info).diva_file_info)[event.index] 
	filesize 					  = tmp_diva_image_file.file_info.filesize
	
	
    ;this is where the file is read and is sorted into single/multiple files
    
	;get 'file_format' - check filetype for FITS or IMG format
	file_format = (*(*info).diva_file_info)[event.index].file_format	

	;IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG
    ;IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG
    ;IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG
    IF (file_format eq 'type_img') THEN BEGIN

    	PTR_FREE, (*(*info).pdiva_draw).diva_current_prev_image.pimage
    	PTR_FREE, (*(*info).pdiva_draw).diva_current_prev_image.o_pimage
    	PTR_FREE, (*(*info).pdiva_draw).diva_current_prev_image.p_pimage
		PTR_FREE, (*(*info).pdiva_draw).diva_current_prev_image.s_pimage
		
		;call 'diva_read_data_img' pro to read data
    	diva_read_data_img,tmp_diva_image_file

    	diva_n_images   				= tmp_diva_image_file.diva_n_images
		tmp_diva_image_file.ops_bshift  = [1,0]

    ENDIF;of condition data file extension is 'type_img' (img, dat or raw)
    
	;FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS
    ;FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS
    ;FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS
	IF (file_format eq 'type_fit') THEN BEGIN
		;call 'diva_fits_help' pro to get summary of data
    	diva_fits_help,f,get_fname
		diva_n_images	= n_elements(get_fname)
		
		;call 'diva_read_data_fit' pro to read data
		diva_read_data_fit,tmp_diva_image_file,index,diva_n_images
		
		tmp_diva_image_file.diva_n_images   = diva_n_images
	ENDIF ;of condition file is FITS format 'type_fit'

	;JPEG_JPEG_JPEG_JPEG_JPEG_JPEG_JPEG_JPEG_JPEG_JPEG_JPEG_JPEG_JPEG_JPEG
	;JPEG_JPEG_JPEG_JPEG_JPEG_JPEG_JPEG_JPEG_JPEG_JPEG_JPEG_JPEG_JPEG_JPEG
	;JPEG_JPEG_JPEG_JPEG_JPEG_JPEG_JPEG_JPEG_JPEG_JPEG_JPEG_JPEG_JPEG_JPEG
	IF (file_format eq 'type_jpg') THEN BEGIN
		
		;call 'diva_read_data_jpg' pro to read data
		diva_read_data_jpg,tmp_diva_image_file
		
		diva_n_images	= tmp_diva_image_file.diva_n_images
		
	ENDIF ;of condition file is JPEG format 'type_jpg'

	;BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_
	;BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_
	;BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_BMP_
	IF (file_format eq 'type_bmp') THEN BEGIN
		
		;call 'diva_read_data_jpg' pro to read data
		diva_read_data_bmp,tmp_diva_image_file
		
		diva_n_images	= tmp_diva_image_file.diva_n_images
		
	ENDIF ;of condition file is BMP format 'type_bmp'

	;BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH
	;BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH
	;BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH
	
    ;update 'diva_current_prev_image'
    (*(*info).pdiva_draw).diva_current_prev_image = tmp_diva_image_file

	widget_control,(*info).tlb_prev_size_label_id,set_value =				$
			'[Bytes: '+ strcompress(filesize,/rem) +  					$
			']  [Images: ' + strcompress(diva_n_images,/rem) + ']'

	
    widget_control,tlb_prev_drawwid,get_value = prev_drawwid
    wset,prev_drawwid
	;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!	

	IF (diva_n_images eq 1) THEN BEGIN  ;SINGLE IMAGE

    ;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!	
		temp = (*(*info).pdiva_draw).diva_current_prev_image

	    ;call 'diva_draw_prev' procedure to display preview image
	    diva_draw_prev,prev_drawwid,tlb_prev_pixid,temp.n_dim,temp.p_pimage

    	tmp = {t:[	strcompress(temp.file_info.filename,/rem),      		$
    		    	strcompress(temp.mfile_size,/rem),	    	    		$
    		    	strcompress(temp.xsize,/rem),		    				$
    		    	strcompress(temp.ysize,/rem),		    				$
    		    	strcompress(temp.n_dim,/rem)]}
		
		
		CASE file_format OF
			'type_img': wot = [0,1,1,0,0,1,0]		;type_img	IMG
			'type_fit': wot = [1,1,1,0,0,1,0]		;type_fit	FITS
			'type_jpg': wot = [1,1,1,0,0,1,0]		;type_jpg	JPEG
			'type_bmp': wot = [1,1,1,0,0,1,0]		;type_bmp	BMP
		ENDCASE 
		diva_prev_contextbase_sensitive,(*info).prev_context_list,wot
		diva_prev_buttons_sensitive,(*info).prev_buttons,wot
		
		widget_control,(*info).tlb_prev_load_butt,sensitive = 1
			
	;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	ENDIF ELSE BEGIN	    		;MULTIPLE IMAGE FILE

	;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    	
		;diva_prev_list_sensitive,(*info).tlb_preview_list,eventval
		
		widget_control,(*info).tlb_prev_list,sensitive=0
		widget_control,(*info).tlb_prev_list_mul,sensitive=1
		widget_control,(*info).tlb_prev_list_sin_base,map=0
		widget_control,(*info).tlb_prev_list_mul_base,map=1
		
		
    	;IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG
    	;IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG
    	;IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG
    	IF (file_format eq 'type_img') THEN BEGIN

			p   = tmp_diva_image_file.file_info.pathname
    	    fs  = tmp_diva_image_file.file_info.file_str
    	    logfile_name = strcompress(p + fs + '.txt',/remove)

    	    ;call 'diva_logfile_read' procedure
	    	diva_logfile_read,logfile_name,   	    	    $
    					    mfile_pos_from, mfile_pos_to,   				$
    					    mfile_name,mfile_date,mfile_time
			
			n_elems_dummy = n_elements(mfile_name)
			
    	    diva_mult_image = Replicate({diva_mult_image}, n_elems_dummy)
    	    tmp_diva_mult_image = diva_mult_image

    	    FOR i = 0,n_elems_dummy -1 DO BEGIN
    			diva_mult_image_init,tmp_diva_mult_image,mfile_pos_from[i], $
    					 				mfile_pos_to[i],mfile_name[i],	    $
    					  				mfile_date[i],mfile_time[i],0
    			diva_mult_image[i] = tmp_diva_mult_image[i]
    	    ENDFOR      

			stuff 			= diva_mult_image[0]

    	    mfile_pos_from  = stuff.mfile_pos_from
    	    mfile_pos_to    = stuff.mfile_pos_to
    	    mfile_size  	= stuff.mfile_pos_to - stuff.mfile_pos_from +1-8
			
			wot = [1,1,0,0,1,1,0]
		
		
		ENDIF;of condition data file extension is "type_img"

    	;FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS
    	;FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS
    	;FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS
		IF (file_format eq 'type_fit') THEN BEGIN
			;cf IMG, where log file is read to get list of image names,
	    	;times,dates and positions where the data starts/stops
	    	;However, we don't all that here. All we need is a list of
	    	;names ("get_fname" - already got this from "diva_fits_help")
	    	;procedure. We don't need all that other info to be able to
	    	;read parts of the image file, since we can use the extension
	    	;number ie; 
	    	;result = readfits('filename.fit',header,exten_no=exten_no)

	    	temp			= (*(*info).pdiva_draw).diva_current_prev_image
			file_info       = temp.file_info
	    	diva_n_images   = temp.diva_n_images

	    	diva_mult_image = Replicate({diva_mult_image}, diva_n_images)

	    	for i=0,diva_n_images - 1 do begin
				diva_mult_image[i].file_info = file_info
				
				diva_mult_image[i].mfile_name 	    = get_fname[i]

				if (strpos(get_fname[i],'.') eq -1) then begin
					diva_mult_image[i].mfile_name_str = get_fname[i]
				endif else begin
					diva_mult_image[i].mfile_name_str   =					$
						strmid(get_fname[i],0,strpos(get_fname[i],'.'))
				endelse
				diva_mult_image[i].mfile_fits_ext   = i
	    	endfor

			mfile_size = temp.xsize*temp.ysize*temp.n_dim*temp.n_byt

			wot = [1,1,0,1,1,1,0]
			
						
		ENDIF;of condition file is "type_fit"

		;BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH
		;BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH
		;BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH
		
		IF (Ptr_Valid((*info).diva_mult_image) eq 1) then BEGIN
			Ptr_Free,(*info).diva_mult_image
		END
		(*info).diva_mult_image = Ptr_NEW(diva_mult_image)
		;create a list of individual filenames
		widget_control,(*info).tlb_prev_list_mul,set_value =    	    		$
			    			(*(*info).diva_mult_image)[*].mfile_name
		
		(*(*info).pdiva_draw).diva_current_prev_image.mfile_size = mfile_size
		
		temp = (*(*info).pdiva_draw).diva_current_prev_image
    	;1. get image info
    	tmp = {t:['__'+     strcompress(temp.mfile_name,/rem),	    	$
						    strcompress(temp.mfile_size,/rem),	    	$
						    strcompress(temp.xsize,/rem),		    	$
    					    strcompress(temp.ysize,/rem),		    	$
						    strcompress(temp.n_dim,/rem)]}
		
		;2. set list selection to element zero
    	widget_control,(*info).tlb_prev_list_mul,set_list_select=0
    	;3. set diva_preview draw widget as current
	    widget_control,tlb_prev_drawwid,get_value = prev_drawwid
	    ;4. call 'diva_draw_prev' procedure to display preview image
	    diva_draw_prev,prev_drawwid,tlb_prev_pixid,temp.n_dim,	    	$
	    			    								temp.p_pimage

		diva_prev_contextbase_sensitive,(*info).prev_context_list,wot
		diva_prev_buttons_sensitive,(*info).prev_buttons,wot
			
	ENDELSE ;of single/multiple case
	;call 'diva_report_prev_info' to update info labels
	diva_report_prev_info,(*info).prev_info,tmp
	
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"PREV_EXTRACT":	$	;PREV_EXTRACT_	PREV_EXTRACT_PREV_EXTRACT_PREV_EXTRACT_
BEGIN	
	widget_control,/hourglass
	diva_current_prev_image=(*(*info).pdiva_draw).diva_current_prev_image
	f				= diva_current_prev_image.file_info.f
	diva_n_images	= diva_current_prev_image.diva_n_images
	index 			= (*info).diva_prev_mult_index
	wid_id			= (*info).tlb
	func			= 'extract'
	bsname 			= ''
	bshift_val		= 0
	
	comment_0	= ' Source file: ' + strcompress(f,/rem)
	comment_1	= ' This file was created by exctracting image #' + 		$
									 strcompress(index + 1,/rem) +			$
							' of ' + strcompress(diva_n_images,/rem)

	comments	= [comment_0,comment_1]
	
	;call "diva_make_fits_file" procedure to make a fits file
	diva_make_fits_file,diva_current_prev_image,index,comments, 			$
											wid_id,func,bsname,bshift_val
	
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"PREV_REMV":	$   ;PREV_REMV_PREV_REMV_PREV_REMV_PREV_REMV_PREV_REMV_PREV
BEGIN

	index 	    = (*info).diva_prev_index
	old_array   = (*(*info).diva_file_info)
	selected    = (*(*info).diva_file_info)[index]

	f 			=  selected.f
	cf  		= (*(*info).pdiva_draw).diva_current_image.file_info.f

    ;if removed item is current image!!!!!!!!!!!!!!!!!!!!
	if (f eq cf) then begin
	    diva_draw_no_image,(*info).pdiva_draw
	    ;desensitize stats/hist TOOL options
	    widget_control,(*info).diva_file_buttids[0],sensitive=0
	    widget_control,(*info).diva_menus[0],sensitive=0
	    widget_control,(*info).diva_menus[1],sensitive=0
	    widget_control,(*info).diva_menus[2],sensitive=0
	    widget_control,(*info).diva_menus[3],sensitive=0
	endif

	n_elems_old_array = n_elements(old_array)
	l = n_elems_old_array - 1   ;last element
	
	IF (n_elems_old_array gt 1) THEN BEGIN

		CASE index OF
				0:	new_array = [(*(*info).diva_file_info)[1:*]]
				l:	new_array = [(*(*info).diva_file_info)[0:l-1]]
	    	ELSE:	new_array = [(*(*info).diva_file_info)[0:index-1],		$
		     	    	 	 	 (*(*info).diva_file_info)[index+1:l] ]
		ENDCASE
		;update 'diva_image_file' array
		(*(*info).diva_file_info)=new_array
		;update list with entry removed
		widget_control,(*info).tlb_prev_list,set_value= 					$
    	    						(*(*info).diva_file_info)[*].filename
	ENDIF ELSE BEGIN
	    ;update list with entry removed
	    Ptr_FRee,(*info).diva_file_info
	    Ptr_Free,(*(*info).pdiva_draw).diva_current_prev_image.pimage
	    Ptr_Free,(*(*info).pdiva_draw).diva_current_prev_image.o_pimage
	    Ptr_Free,(*(*info).pdiva_draw).diva_current_prev_image.p_pimage
	    widget_control,(*info).tlb_rhs_base,sensitive=0
	    widget_control,(*info).tlb_prev_list,set_value = 'none'
	ENDELSE

	(*info).diva_prev_index = 0 ;reset index to zero
	;update filename/dimensions/size widget labels
	tmp = {t:['none','0','0','0','0']}
	;call 'diva_report_prev_info' to update info labels
	diva_report_prev_info,(*info).prev_info,tmp
	;erase preview draw area and pixmap
	widget_control,tlb_prev_drawwid,get_value = prev_drawwid
	wset,prev_drawwid 		& erase
	wset,tlb_prev_pixid 	& erase 

	widget_control,(*(*info).pdiva_draw).diva_prev_button_base,map = 0
	(*(*info).pdiva_draw).prev_draw_buttons_show = 0
	 widget_control,(*info).tlb_prev_size_label_id,set_value =  		 	$
						 '[Bytes: - ]  [Images: - ]'

END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"PREV_LOAD":	$   ;PREV_LOAD_PREV_LOAD_PREV_LOAD_PREV_LOAD_PREV_LOAD_PREV
BEGIN
    widget_control,/hourglass
	
	(*(*info).pdiva_draw).diva_fit_size 	= 0
	(*(*info).pdiva_draw).diva_autoscale 	= [1,0]
    
	
	if (XRegistered("diva_hist") eq 1) THEN 								$
			widget_control,(*(*info).pdiva_draw).diva_histbase,/destroy
	
	
    Ptr_Free,(*(*info).pdiva_draw).diva_current_image.pimage
    Ptr_Free,(*(*info).pdiva_draw).diva_current_image.o_pimage
    Ptr_Free,(*(*info).pdiva_draw).diva_current_image.p_pimage
    
    curr_prev 	= (*(*info).pdiva_draw).diva_current_prev_image
    
    diva_image_copy, curr_prev, new_curr
    
    ;make pdiva_draw.diva_current_image
    (*(*info).pdiva_draw).diva_current_image = new_curr
    
    ;call 'diva_draw_create' to kill and recreate tlb draw area
    diva_draw_create,(*info).pdiva_draw
    diva_draw_image, (*info).pdiva_draw;... then display image
    diva_tlb_title,  (*info).pdiva_draw;....and finally update tlb title
    ;call 'diva_draw_prev_viewport' procedure
    diva_draw_prev_viewport,(*info).pdiva_draw
    

    widget_control,(*info).diva_file_buttids[0],sensitive=1
    widget_control,(*info).diva_menus[0],sensitive=1
    widget_control,(*info).diva_menus[1],sensitive=1
    widget_control,(*info).diva_menus[2],sensitive=1
    widget_control,(*info).diva_menus[3],sensitive=1
    
	;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	;get data type, ISHFT only works for bytes, integers and longwords
	sz			= size((*(*(*info).pdiva_draw).diva_current_image.pimage))
	n 			= N_ELEMENTS(sz)
	data_type 	= sz[n-2]

	IF (data_type eq 4)  OR (data_type eq 5)  OR (data_type eq 6) 	$
	OR (data_type eq 7)  OR (data_type eq 8)  OR (data_type eq 9) 	$
	OR (data_type eq 10) OR (data_type eq 11) 						$ 
	THEN BEGIN 
		widget_control,(*(*info).pdiva_draw).tlb_top_base,sensitive=0
	ENDIF ELSE BEGIN
		widget_control,(*(*info).pdiva_draw).tlb_top_base,sensitive=1
    ENDELSE
	;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"PREV_ALL": 	BEGIN;---------------------------------------------------------;
	t 	    = (*(*info).pdiva_draw).diva_current_prev_image
	p 	    = t.pimage
	CASE t.n_dim OF
		1:  pimage = congrid(*p,400,400)
		3:  pimage = congrid(*p,400,400,t.n_dim)
	ENDCASE
	diva_draw_all,pimage,t.n_dim,1,group= (*info).tlb 
	;where auto = 1 indicates autoscaling condition is TRUE (it's ON) 
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DIVA_ALL": 	BEGIN;---------------------------------------------------------;
	t 	    = (*(*info).pdiva_draw).diva_current_image
	p 	    = t.pimage

	;get data type, ISHFT only works for bytes, integers and longwords
	sz			= size(*p)
	n 			= N_ELEMENTS(sz)
	data_type 	= sz[n-2]
	
	CASE t.n_dim OF
		1:	pimage =congrid(*p,400,400)
		3:	pimage =congrid(*p,400,400,t.n_dim)
	ENDCASE
	
	IF (data_type eq 4)  OR (data_type eq 5)  OR (data_type eq 6) 			$
	OR (data_type eq 7)  OR (data_type eq 8)  OR (data_type eq 9) 			$
	OR (data_type eq 10) OR (data_type eq 11) 								$ 
	THEN BEGIN 
		auto 	= 1 ;indicates autoscaling condition is TRUE (it's ON) 
	ENDIF ELSE BEGIN
		auto    = (*(*info).pdiva_draw).diva_autoscale[0]
		shft    = (*(*info).pdiva_draw).diva_autoscale[1]
		pimage  = ishft(pimage,shft)
		
	ENDELSE
	diva_draw_all,pimage,t.n_dim,auto,group= (*info).tlb 

END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"ADAPT_MODE":	$   ;ADAPTIVE_MODE_ADAPTIVE_MODE_ADAPTIVE_MODE_ADAPTIVE_MODE_
BEGIN
	CASE (*info).os_name OF
	    'X':    	path = (*info).def_path[0]
	    'WIN':  	path = (*info).def_path[1]
	ENDCASE
	;call "diva_file" procedure to diaplay an OPEN FILE(s) dialog.
	f = diva_file((*info).def_file,path,'adapt')
	;if this was cancelled, then RETURN
	IF (f[0] eq '') THEN BEGIN
		;call 'diva_error' to whinge...............
		diva_error,(*info).tlb,0
		RETURN
    END
    ;**************************************************************************
	pick_log 	= strcompress(f[0],/remove)
	pick_data 	= pick_log ;this will be overwritten for IMG type files
	
	widget_control,(*info).tlb_prev_name_label_id,set_value = pick_log
	
	;extract file extension
	ext = STRMID(pick_log,STRPOS(pick_log,'.',/reverse_search)+1)
	
	;IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG
	;IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG
	;IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG
	if (ext eq 'txt') then begin
		
		;call 'diva_logfile_read' procedure
		diva_logfile_read,pick_log, mfile_pos_from,							$
									mfile_pos_to,	    					$
					    			mfile_name, 							$
									mfile_date, 							$
									mfile_time
									
		;call FUNCTION to get data file name "pick_data"
		pick_data = diva_ad_get_data_file(pick_log)
		
	endif else begin
	;FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS
	;FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS
	;FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS

	    ;call 'diva_fits_help' pro to get summary of data
	    ;diva_fits_help,pick_log,get_fname,get_fdate,get_ftime
		diva_fits_help,pick_log,mfile_name,mfile_date,mfile_time
		
		n_elems 		= n_elements(mfile_name)
		mfile_pos_from	= STRARR(n_elems)
		mfile_pos_to	= STRARR(n_elems)
		
	endelse
	
	;BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH
	;BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH
	;BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH
	
	;OK, let's create a 'diva_mult_image' structure for each new
    ;image found. Then we'll append that onto our original
    diva_file	=   {diva_file}
    diva_file_init,diva_file,pick_data
    
	num 		= n_elements(mfile_name)


	diva_mult_image	= replicate({diva_mult_image},num)
	tmp_diva_mult_image = diva_mult_image

	FOR i = 0,num - 1 DO BEGIN
		diva_mult_image_init,tmp_diva_mult_image,							$
	    		    mfile_pos_from, 	    	    	    	    	    $
	    		    mfile_pos_to,		    	    	    	    		$
	    		    mfile_name[i],		    	    						$
	    		    mfile_date[i],		    	    						$
	    		    mfile_time[i],   	    	    	    				$
			    	i

		tmp_diva_mult_image[i].file_info = diva_file[0]
		diva_mult_image[i] =  tmp_diva_mult_image[i]
	
	ENDFOR
	
    new_list = STRARR(num)

	FOR i= 0 ,num - 1 DO 	new_list[i] = 	mfile_time[i] + '   ' + 		$
											mfile_date[i] + '   ' + 		$
											mfile_name[i]
	
	;free up 'diva_mult_image' pointer - and recreate with new structure
	Ptr_Free,(*info).diva_mult_image
	(*info).diva_mult_image     = Ptr_NEW(diva_mult_image)
	number_images				= n_elements(new_list)
	(*info).number_images       = number_images

	;update info widget with number of images and file size
	filesize	  = strcompress(diva_mult_image[0].file_info.filesize,/remove)
	number_images = strcompress(number_images,/remove)
	
	widget_control,(*info).tlb_prev_size_label_id,set_value =				$
			'[Bytes: ' + filesize +	']  [Images: ' + number_images+']'

	;update filenames of log and data files (the same for FITs) in "info"
	(*info).diva_ad_pick_file   = [pick_log,pick_data]

	FOR i= 0 ,n_elements(new_list) - 1 DO   	    	    	    	    $	
		    	    	(*info).diva_ad_new_list[i]=new_list[i]
	;update adaptive list with new_list
	widget_control,(*info).tlb_adapt_list,set_value=new_list

	;set timer going		    
	widget_control,(*info).diva_ad_buttons[0],TIMER =   	    	    	$
			    	    	    	    			(*info).tlb_ad_timer[0]

	;reset diva_ad_button to ON
	widget_control,(*info).diva_ad_buttons[0],set_button=1

	(*info).diva_adapt_mode_on  = 1
	(*info).diva_time_events_on = 1
	widget_control,(*info).tlb_prev_mode_label_id,set_value =				$
		    	    					'*****   ADAPTIVE MODE   *****'

	;call 'diva_report_prev_info' to update info labels
	tmp = {t:['none','0','0','0','0']}
	diva_report_prev_info,(*info).prev_info,tmp
	
	;unmap buttons in preview draw area
	widget_control,(*(*info).pdiva_draw).diva_prev_button_base,map = 0
	(*(*info).pdiva_draw).prev_draw_buttons_show = 0
	
	;call "diva_prev_list_sensitive" to sensitize preview lists
	doh = [1,0,0,0,0,0,1]
	diva_prev_list_sensitive,(*info).tlb_preview_list,doh
	
	;erase preview draw area
	widget_control,tlb_prev_drawwid,get_value = prev_drawwid
	wset,prev_drawwid   	& erase
	wset,tlb_prev_pixid 	& erase
	
	
	;erase main draw area
	widget_control,diva_tlb_drawwid,get_value = tlb_draw
	wset,tlb_draw	    	& erase
	wset,diva_tlb_pixindex  & erase
	widget_control,diva_tlb_drawwid,sensitive = 0
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"ADAPT_OFF":$	;ADAPTIVE_MODE_OFF_ADAPTIVE_MODE_OFF_ADAPTIVE_MODE_OFF_OFF_OFF
BEGIN
	(*info).diva_adapt_mode_on = 0
	widget_control,(*info).tlb_prev_mode_label_id,set_value =				$
			    						'******   PREVIEW  MODE   ******'
	widget_control,(*info).tlb_rhs_base,sensitive = 1
	
	widget_control,(*(*info).pdiva_draw).diva_prev_button_base,map = 0 
	(*(*info).pdiva_draw).prev_draw_buttons_show = 0
	
	index = (*info).diva_prev_index
	
	if (Ptr_Valid((*info).diva_file_info) eq 1) then BEGIN
		list	= (*(*info).diva_file_info)[*].f
		doh = [1,1,0,0,1,0,0]
		;widget_control,(*info).tlb_prev_list,sensitive=1
	endif else begin    ;ie-    no files selected
		;widget_control,(*info).tlb_prev_list,sensitive=0
		doh = [1,0,0,0,1,0,0]
		widget_control,(*info).tlb_prev_name_label_id,set_value='none'
	endelse
	
	;call "diva_prev_list_sensitive" to sensitize preview lists
	diva_prev_list_sensitive,(*info).tlb_preview_list,doh
	
	
	widget_control,(*info).tlb_prev_size_label_id,set_value =				$
												'[Bytes: - ]  [Images: - ]'
	
	;diva_draw_no_image,(*info).pdiva_draw
	tmp = {t:['0','0','0','0','0']}
	;call 'diva_report_prev_info' to update info labels
	diva_report_prev_info,(*info).prev_info,tmp
	;get preview draw area and make current
	widget_control,tlb_prev_drawwid,get_value = prev_drawwid
	wset,prev_drawwid   &  erase
	wset,tlb_prev_pixid &  erase
	diva_draw_no_image,(*info).pdiva_draw
	;OK, lets free up ALL the adaptive mode pointers
	(*info).diva_ad_new_list = strarr(500)
	
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"AD_LIST":  $	;AD_LIST_AD_LIST_AD_LIST_AD_LIST_AD_LIST_AD_LIST_AD_LIST_AD_LIS
BEGIN
    widget_control,/hourglass
    (*info).diva_ad_list_index = event.index
	
	widget_control,(*info).tlb_prev_load_butt,sensitive = 1
	
    pick_data	= (*info).diva_ad_pick_file[1]
    f 			= (*info).diva_ad_pick_file[0]
    ext 		= strmid(f,  strpos(f,'.',/reverse_search)+1)
    
	mult 		= (*(*info).diva_mult_image)[event.index]
    temp 		= (*(*info).pdiva_draw).diva_current_prev_image
    
	CASE mult.file_info.file_format OF
		'type_img': 	wot = [1,1,0,0,1,0,0]		;type_img	IMG
		'type_fit': 	wot = [1,1,0,1,1,0,0]		;type_fit	FIT
	ENDCASE 
	diva_prev_contextbase_sensitive,(*info).prev_context_list,wot
	diva_prev_buttons_sensitive,(*info).prev_buttons,wot

	temp.file_info      	= mult.file_info
	temp.mfile_pos_from		= mult.mfile_pos_from
	temp.mfile_pos_to		= mult.mfile_pos_to
	temp.mfile_name     	= mult.mfile_name
	temp.mfile_name_str 	= mult.mfile_name_str
	temp.mfile_date     	= mult.mfile_date
	temp.mfile_time     	= mult.mfile_time

    ;IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG
	;IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG
	;IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG
    if (ext eq 'txt') then begin

		;call 'diva_image_file_read_multiple'
		diva_image_file_read_multiple,temp
		
		temp.diva_n_images = n_elements((*(*info).diva_mult_image))

    endif else begin
    ;FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS
	;FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS
	;FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS
	
		num_images  			= n_elements((*(*info).diva_mult_image))
		;call procedure to read multiple image
		diva_read_data_fit,temp,event.index,num_images
    endelse


    ;BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH
	;BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH
	;BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH

	;and update "diva_current_prev_image" structure
	(*(*info).pdiva_draw).diva_current_prev_image = temp



    ;get preview draw area and make current
    widget_control,tlb_prev_drawwid,get_value = prev_drawwid
    ;call 'diva_draw_prev' procedure to display preview image
    diva_draw_prev,prev_drawwid,tlb_prev_pixid,temp.n_dim,temp.p_pimage

    tmp = {t:[      strcompress(temp.mfile_name,/rem), 	    				$
		    		strcompress(temp.mfile_size,/rem),	    				$
		    		strcompress(temp.xsize,/rem),  	    					$
		    		strcompress(temp.ysize,/rem),  	    					$
		    		strcompress(temp.n_dim,/rem)]}
    ;call 'diva_report_prev_info' to update info labels
    diva_report_prev_info,(*info).prev_info,tmp
    
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DIVA_XMTOOL":  xmtool, group= (*info).tlb 
"DIVA_XLOADCT": stop;xloadct,group= (*info).tlb
"AD_AUTO_ON":	if (event.select eq 1) then (*info).diva_ad_autoload_on = 1
"AD_AUTO_OFF":	if (event.select eq 1) then (*info).diva_ad_autoload_on = 0
;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DIVA_STATS":	BEGIN
		    temp    = (*(*info).pdiva_draw).diva_current_image
		    maxv    = total(temp.i_max)
		    meanv   = total(temp.i_mean)
		    diva_draw_base = (*(*info).pdiva_draw).diva_draw_base
		    
			if (maxv eq 0) AND (meanv eq 0) then begin
		    	diva_tlb_stats_calc,(*info).pdiva_draw,diva_draw_base
		    endif else begin
				xv  	= [0,temp.xsize-1]
				yv  	= [0,temp.ysize-1]
				stats 	= [xv,yv,temp.i_min,temp.i_max,temp.i_mean, 		$
			    							temp.i_variance,temp.i_stddev]
				;call 'diva_stats' pro to display widget table of stats!
				diva_stats,stats,0,temp.n_dim,group=diva_draw_base
		    endelse
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DIVA_HISTOG":	$   ;   HISTOGRAM__HISTOGRAM__HISTOGRAM__HISTOGRAM__HISTOGRAM
BEGIN
	if (XRegistered("diva_hist") eq 1) THEN return
	diva_hist,'current',(*info).pdiva_draw,hist_id,							$
								group=(*(*info).pdiva_draw).tlb_top_base
	
	(*(*info).pdiva_draw).diva_histbase = hist_id
	
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"FUNCTION": 	$	;
BEGIN
	t = (*(*info).pdiva_draw).tlb_info_func_text
	CASE event.value OF
		1:  BEGIN   ;regular ROI
    			(*(*info).pdiva_draw).tlb_image_function = 1
				widget_control,t,set_value = 'Regular ROI'
    		END
		2:  BEGIN   ;irregular ROI
    			(*(*info).pdiva_draw).tlb_image_function = 2
				widget_control,t,set_value = 'Irregular ROI'
    		END
		3:  BEGIN   ;circular ROI
				(*(*info).pdiva_draw).tlb_image_function = 3
				widget_control,t,set_value = 'Circular ROI'
    		END
		4:  BEGIN   ;ZOOM
    			(*(*info).pdiva_draw).tlb_image_function = 4
				widget_control,t,set_value = 'Zoom'
    		END
		6:  BEGIN   ;Horz/Vert Line profile 
    			(*(*info).pdiva_draw).tlb_image_function = 6
				widget_control,t,set_value = 'H/V Line Profile'
    		END
		7:  BEGIN   ;Entire Col Line profile 
    			(*(*info).pdiva_draw).tlb_image_function = 7
				widget_control,t,set_value = 'All Col Profile'
    		END
		8:  BEGIN   ;Entire Row Line profile 
    			(*(*info).pdiva_draw).tlb_image_function = 8
				widget_control,t,set_value = 'All Row Profile'
    		END
		9:  BEGIN   ;any Line profile
    			(*(*info).pdiva_draw).tlb_image_function = 9
				widget_control,t,set_value = 'Any Line Profile'
    		END
	ENDCASE
	(*(*info).pdiva_draw).diva_roi_irreg_xpts = intarr(100)
	(*(*info).pdiva_draw).diva_roi_irreg_ypts = intarr(100)
	(*(*info).pdiva_draw).diva_roi_irreg_counter = 0
	
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DIVA_SAVED_CURR":		$	;
BEGIN
	;call "diva_image_store" procedure	
	diva_image_store,(*info).pdiva_draw,'current'
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"PREV_STORE":		$
BEGIN
	;call "diva_image_store" procedure
	diva_image_store,(*info).pdiva_draw,'prev'
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"PREV_STORE_ALL":	$
BEGIN
	stop
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DIVA_MATH":	$   ;DIVA_MATH_DIVA_MATH_DIVA_MATH_DIVA_MATH_DIVA_MATH_DIVA_MAT
BEGIN
    IF (XREGISTERED("diva_math") NE 0) THEN RETURN
	
	diva_image_stored = (*(*info).pdiva_draw).diva_image_stored
    
	IF (PTr_VALID(diva_image_stored) eq 1) THEN BEGIN
	
		diva_math_info = diva_math_init(	(*info).def_path,				$
											diva_image_stored,				$
											(*info).tlb 	)
		
		(*(*info).pdiva_draw).diva_math_info = diva_math_info
		
		IF (XREGISTERED("diva_conc") NE 0) THEN BEGIN
			
			(*(*(*info).pdiva_draw).diva_math_info).diva_concbase = 		$
					(*(*(*info).pdiva_draw).diva_conc_info).diva_concbase
			
			(*(*(*info).pdiva_draw).diva_conc_info).diva_mathbase = 		$
					(*(*(*info).pdiva_draw).diva_math_info).diva_mathbase

		ENDIF
		
    ENDIF ELSE  diva_error,(*info).tlb,3
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DIVA_CONC":	$   ;DIVA_CONCATENATOR_DIVA_CONCATENATOR_DIVA_CONCATENATOR_DIVA
BEGIN
    IF (XREGISTERED("diva_conc") NE 0) THEN RETURN
	
    diva_image_stored = (*(*info).pdiva_draw).diva_image_stored
	
	IF (PTr_VALID(diva_image_stored) eq 1) THEN BEGIN
	
    	diva_conc_info = diva_conc_init( 	(*info).def_path,				$
											diva_image_stored,				$
											(*info).tlb 	)
		
		(*(*info).pdiva_draw).diva_conc_info = diva_conc_info
		
		IF (XREGISTERED("diva_math") NE 0) THEN BEGIN
			
			(*(*(*info).pdiva_draw).diva_conc_info).diva_mathbase = 		$
					(*(*(*info).pdiva_draw).diva_math_info).diva_mathbase

			(*(*(*info).pdiva_draw).diva_math_info).diva_concbase = 		$
					(*(*(*info).pdiva_draw).diva_conc_info).diva_concbase
			
		ENDIF
		
    ENDIF ELSE  diva_error,(*info).tlb,3
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DIVA_ROT_CW":	$   ;DIVA_ROTATE_CLOCKWISE________DIVA_ROTATE_CLOCKWISE________
BEGIN
    ;make temporary copy of 'diva_current_image'
    tmp_diva_current_image = (*(*info).pdiva_draw).diva_current_image
    ;pass it into 'diva_image_rotate'
    diva_image_rotate, tmp_diva_current_image, 3
    
    nsize   	= (*(*info).pdiva_draw).tlb_drawwid_size
    old_xsize   = tmp_diva_current_image.xsize
    old_ysize	= tmp_diva_current_image.ysize
    
    CASE diva_fit_size OF
    	0:  ;nothing to do here
	1:  diva_image_congrid,tmp_diva_current_image,nsize[0]   ,nsize[1]
	2:  diva_image_congrid,tmp_diva_current_image,old_xsize  ,nsize[1]-10
	3:  diva_image_congrid,tmp_diva_current_image,nsize[0]-10,old_ysize
    ENDCASE
    
    ;update 'diva_current_image' here
    (*(*info).pdiva_draw).diva_current_image = tmp_diva_current_image
    
    ;call 'diva_draw_create' to kill and recreate tlb draw area
    diva_draw_create,(*info).pdiva_draw
    diva_draw_image, (*info).pdiva_draw;... then display image
    diva_tlb_title,  (*info).pdiva_draw;....and finally update tlb title
    ;call 'diva_draw_prev_viewport' procedure
    diva_draw_prev_viewport,(*info).pdiva_draw
    
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DIVA_ROT_ACW": $   ;DIVA_ROTATE_ANTI-CLOCKWISE______DIVA_ROTATE_ANTI-CLOCKWISE
BEGIN
    ;make temporary copy of 'diva_current_image'
    tmp_diva_current_image = (*(*info).pdiva_draw).diva_current_image
    ;pass it into 'diva_image_rotate'
    diva_image_rotate, tmp_diva_current_image, 1
    
    nsize   	= (*(*info).pdiva_draw).tlb_drawwid_size
    old_xsize   = tmp_diva_current_image.xsize
    old_ysize	= tmp_diva_current_image.ysize
    
    CASE diva_fit_size OF
    	0:  ;nothing to do here
	1:  diva_image_congrid,tmp_diva_current_image,nsize[0]   ,nsize[1]
	2:  diva_image_congrid,tmp_diva_current_image,old_xsize  ,nsize[1]-10
	3:  diva_image_congrid,tmp_diva_current_image,nsize[0]-10,old_ysize
    ENDCASE
    
    ;update 'diva_current_image' here
    (*(*info).pdiva_draw).diva_current_image = tmp_diva_current_image
    
    ;call 'diva_draw_create' to kill and recreate tlb draw area
    diva_draw_create,(*info).pdiva_draw
    diva_draw_image, (*info).pdiva_draw;... then display image
    diva_tlb_title,  (*info).pdiva_draw;....and finally update tlb title
    ;call 'diva_draw_prev_viewport' procedure
    diva_draw_prev_viewport,(*info).pdiva_draw
    
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DIVA_ROT_F":	$   ;DIVA_ROTATE_180_DIVA_ROTATE_180_DIVA_ROTATE_180_DIVA_ROT
BEGIN
    
    ;make temporary copy of 'diva_current_image'
    tmp_diva_current_image = (*(*info).pdiva_draw).diva_current_image
    ;pass it into 'diva_image_rotate'
    diva_image_rotate, tmp_diva_current_image, 2
    
    ;update 'diva_current_image' here
    (*(*info).pdiva_draw).diva_current_image = tmp_diva_current_image
    
    ;call 'diva_draw_create' to kill and recreate tlb draw area
    diva_draw_create,(*info).pdiva_draw
    diva_draw_image, (*info).pdiva_draw;... then display image
    diva_tlb_title,  (*info).pdiva_draw;....and finally update tlb title
    ;call 'diva_draw_prev_viewport' procedure
    diva_draw_prev_viewport,(*info).pdiva_draw

END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DIVA_FIT_B":	$   ;DIVA_BEST_FIT_DIVA_BEST_FIT_DIVA_BEST_FIT_DIVA_BEST_FIT_
BEGIN
    (*(*info).pdiva_draw).diva_fit_size = 1
    
    nsize   	= (*(*info).pdiva_draw).tlb_drawwid_size
    ;make temporary copy of 'diva_current_image'
    tmp_diva_current_image = (*(*info).pdiva_draw).diva_current_image
    ;pass it into 'diva_image_congrid'
    diva_image_congrid,tmp_diva_current_image,nsize[0],nsize[1]
    
    ;update 'diva_current_image' here
    (*(*info).pdiva_draw).diva_current_image = tmp_diva_current_image
    
    ;call 'diva_draw_create' to kill and recreate tlb draw area
    diva_draw_create,(*info).pdiva_draw
    diva_draw_image, (*info).pdiva_draw;... then display image
    diva_tlb_title,  (*info).pdiva_draw;....and finally update tlb title
    ;call 'diva_draw_prev_viewport' procedure
    diva_draw_prev_viewport,(*info).pdiva_draw
    
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DIVA_FIT_H":	$   ;DIVA_FIT_HEIGHT_DIVA_FIT_HEIGHT_DIVA_FIT_HEIGHT_DIVA_FIT_
BEGIN
    (*(*info).pdiva_draw).diva_fit_size = 2

    nsize   = 	(*(*info).pdiva_draw).tlb_drawwid_size
    ;make temporary copy of 'diva_current_image'
    tmp_diva_current_image = (*(*info).pdiva_draw).diva_current_image
    old_xsize = tmp_diva_current_image.xsize

    ;pass it into 'diva_image_congrid'
    diva_image_congrid,tmp_diva_current_image,old_xsize,nsize[1]-10

    ;update 'diva_current_image' here
    (*(*info).pdiva_draw).diva_current_image = tmp_diva_current_image
    
    ;call 'diva_draw_create' to kill and recreate tlb draw area
    diva_draw_create,(*info).pdiva_draw
    diva_draw_image, (*info).pdiva_draw;... then display image
    diva_tlb_title,  (*info).pdiva_draw;....and finally update tlb title
    ;call 'diva_draw_prev_viewport' procedure
    diva_draw_prev_viewport,(*info).pdiva_draw

END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DIVA_FIT_W":	$   ;DIVA_FIT_WIDTH_DIVA_FIT_WIDTH_DIVA_FIT_WIDTH_DIVA_FIT_
BEGIN
    (*(*info).pdiva_draw).diva_fit_size = 3

    nsize   = 	(*(*info).pdiva_draw).tlb_drawwid_size
    ;make temporary copy of 'diva_current_image'
    tmp_diva_current_image = (*(*info).pdiva_draw).diva_current_image
    old_ysize = tmp_diva_current_image.ysize

    ;pass it into 'diva_image_congrid'
    diva_image_congrid,tmp_diva_current_image,nsize[0]-10,old_ysize

    ;update 'diva_current_image' here
    (*(*info).pdiva_draw).diva_current_image = tmp_diva_current_image
    
    ;call 'diva_draw_create' to kill and recreate tlb draw area
    diva_draw_create,(*info).pdiva_draw
    diva_draw_image, (*info).pdiva_draw;... then display image
    diva_tlb_title,  (*info).pdiva_draw;....and finally update tlb title
    ;call 'diva_draw_prev_viewport' procedure
    diva_draw_prev_viewport,(*info).pdiva_draw

END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DIVA_PTC": 	$   ;DIVA_PTC_DIVA_PTC_DIVA_PTC_DIVA_PTC_DIVA_PTC_DIVA_PTC_DIVA
BEGIN
    
	;This needs a lot of work!!!!!!!!
	;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	ifile = (*(*info).pdiva_draw).diva_current_prev_image.file_info.f

	pathname	= (*(*info).pdiva_draw).diva_current_prev_image.file_info.pathname
	filename	= (*(*info).pdiva_draw).diva_current_prev_image.file_info.filename
	file_str	= (*(*info).pdiva_draw).diva_current_prev_image.file_info.file_str
	file_ext	= (*(*info).pdiva_draw).diva_current_prev_image.file_info.file_ext
	filestuff	= [ifile,pathname,filename,file_str,file_ext]
	
	;FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_FITS_F
	if (file_ext eq 'fit') OR (file_ext eq 'fits') OR (file_ext eq 'fts') 	$
	THEN BEGIN
		
		diva_fits_help,ifile,get_fname,get_fdate,get_ftime
		number_images = n_elements(get_fname)
		if (number_images eq 2) then begin
			;stop
			image_1 = readfits(ifile,header,exten_no=0,/NOSCALE)
			image_2 = readfits(ifile,header,exten_no=1,/NOSCALE)
			
			xsize = sxpar(header,'NAXIS1')
			ysize = sxpar(header,'NAXIS2')
			
			h_1 = [xsize,ysize,1,2]
			h_1 = uint(h_1)
			
		endif else begin
			diva_error,(*info).tlb,6
			RETURN
		endelse
		
	ENDIF ELSE BEGIN
	;IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG_IMG
		openr,1,ifile
		file_size=FSTAT(1)
		file_size=file_size.size
		h_1 = uindgen(4)
		READU,1,h_1
		;check file is of correct PTC format (ie; 2 same-size images)
		h = LONG(h_1)
		calc_size =  ( ( (h[0]*h[1]) + (n_elements(h)) ) * h[3] ) * 2
		;print,file_size,calc_size
		IF (calc_size eq file_size) THEN BEGIN
	    	h_2 = uindgen(4)
	    	image_1  = uindgen(h_1[0],h_1[1],h_1[2])
	    	image_2  = uindgen(h_1[0],h_1[1],h_1[2])
	    	READU,1,image_1
	    	READU,1,h_2
	    	READU,1,image_2
	    	close,1
	    	image_1 = rotate(image_1,2)
	    	image_2 = rotate(image_2,2)
		ENDIF ELSE BEGIN
	    	close,1
	    	diva_error,(*info).tlb,6
			RETURN
		ENDELSE
    	;ENDIF ELSE diva_error,(*info).tlb,6
		
	ENDELSE
    ;BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_BOTH_
	
	diva_printers		= (*(*info).pdiva_draw).diva_printers
	diva_printers_list	= (*(*info).pdiva_draw).diva_printers_list
    plot_ct				= (*(*info).pdiva_draw).plot_ct
    image_ct			= (*(*info).pdiva_draw).image_ct

    diva_ptc, image_1,image_2,h_1,diva_tlb_drawwid,image_ct,plot_ct,		$
						filestuff,diva_printers,diva_printers_list
    
    
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DUMMY":		$
BEGIN

END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DIVA_NEG": 	$	;DIVA_NEGATIVE_DIVA_NEGATIVE_DIVA_NEGATIVE_DIVA_NEGATIVE_
BEGIN
	pimage  	= (*(*(*info).pdiva_draw).diva_current_image.pimage)
	maxv		= max(pimage)
	minv		= min(pimage)
	
	new_pimage	= (maxv - pimage) + minv
	
	(*(*(*info).pdiva_draw).diva_current_image.pimage)	= new_pimage
	
	diva_draw_image, (*info).pdiva_draw;... then display image
	
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DIVA_TEMPI":	$   ;
BEGIN
	widget_control,/hourglass
    pimage  	= (*(*(*info).pdiva_draw).diva_current_image.pimage)
    o_pimage  	= (*(*(*info).pdiva_draw).diva_current_image.o_pimage)
    p_pimage  	= (*(*(*info).pdiva_draw).diva_current_image.p_pimage)
    
    (*(*(*info).pdiva_draw).diva_current_image.pimage)	    = 	    	    $
    	    	    (*(*(*info).pdiva_draw).diva_current_image.pimage)/4.
    (*(*(*info).pdiva_draw).diva_current_image.o_pimage)    = 	    	    $
    	    	    (*(*(*info).pdiva_draw).diva_current_image.o_pimage)/4.
    (*(*(*info).pdiva_draw).diva_current_image.p_pimage)    = 	    	    $
    	    	    (*(*(*info).pdiva_draw).diva_current_image.p_pimage)/4.
    
    diva_draw_image, (*info).pdiva_draw;... then display image
    
	;recalculate stats
	diva_draw_base = (*(*info).pdiva_draw).diva_draw_base
	diva_tlb_stats_calc,(*info).pdiva_draw,diva_draw_base
    
END
;"DIVA_TEMPII":	$   ;
;BEGIN
;    widget_control,/hourglass
;	pimage  	= (*(*(*info).pdiva_draw).diva_current_image.pimage)
;    o_pimage  	= (*(*(*info).pdiva_draw).diva_current_image.o_pimage)
;    p_pimage  	= (*(*(*info).pdiva_draw).diva_current_image.p_pimage)
;    
;    (*(*(*info).pdiva_draw).diva_current_image.pimage)	    = 	    	    $
;    	    	    uint((*(*(*info).pdiva_draw).diva_current_image.pimage)*4)
;    (*(*(*info).pdiva_draw).diva_current_image.o_pimage)    = 	    	    $
;    	    	    uint((*(*(*info).pdiva_draw).diva_current_image.o_pimage)*4)
;    (*(*(*info).pdiva_draw).diva_current_image.p_pimage)    = 	    	    $
;    	    	    uint((*(*(*info).pdiva_draw).diva_current_image.p_pimage)*4)
;    
;    diva_draw_image, (*info).pdiva_draw;... then display image
;    
;END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"AUTOSC_ON":  	$   ;
BEGIN
    widget_control,/hourglass
    (*(*info).pdiva_draw).diva_autoscale = [1,0]
    ;... then display image
    diva_draw_image, (*info).pdiva_draw
    
    o = (*(*info).pdiva_draw).diva_current_image.ops_name
    t = strmid(o,0,strpos(o,'(',/reverse_search))+'(1,0)'
    (*(*info).pdiva_draw).diva_current_image.ops_name = t
    (*(*info).pdiva_draw).diva_current_image.ops_bshift = [1,0]
    
    widget_control,(*(*info).pdiva_draw).tlb_bshift_base,sensitive = 0
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"AUTOSC_OFF":  	$   ;
BEGIN
    widget_control,/hourglass
    ;widget_control,tlb_info_auto_text,set_value = 'Autoscale OFF'
    (*(*info).pdiva_draw).diva_autoscale = [0,0]
    widget_control,(*(*info).pdiva_draw).tlb_bshift_text,set_value = '0'
    ;... then display image
    diva_draw_image, (*info).pdiva_draw
    
    o = (*(*info).pdiva_draw).diva_current_image.ops_name
    t = strmid(o,0,strpos(o,'(',/reverse_search))+'(0,0)'
    (*(*info).pdiva_draw).diva_current_image.ops_name = t
    (*(*info).pdiva_draw).diva_current_image.ops_bshift = [0,0]

    widget_control,(*(*info).pdiva_draw).tlb_bshift_base,sensitive = 1
	widget_control,(*(*info).pdiva_draw).tlb_bshift_save,sensitive = 0
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"BSHFT_DOWN":	$   ;
BEGIN
    widget_control,/hourglass
    new_value = diva_autoscale[1] + 1
    (*(*info).pdiva_draw).diva_autoscale[1] = new_value
    widget_control,(*(*info).pdiva_draw).tlb_bshift_text,   	    	    $
    	    	    	    	    set_value = strcompress(new_value,/rem)
    ;... then display image
    diva_draw_image, (*info).pdiva_draw
    
    o = (*(*info).pdiva_draw).diva_current_image.ops_name
    t = strmid(o,0,strpos(o,'(',/reverse_search)) + 	    	    	    $
		    	    '(0,' + strcompress(new_value,/rem) + ')'
    (*(*info).pdiva_draw).diva_current_image.ops_name = t
    (*(*info).pdiva_draw).diva_current_image.ops_bshift =	  				$
		    	    	    	    [0,strcompress(new_value,/rem)]
									
	CASE new_value OF
		0:		sens = 0
		ELSE:	sens = 1
	ENDCASE
	widget_control,(*(*info).pdiva_draw).tlb_bshift_save,sensitive = sens
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"BSHFT_UP":		$   ;
BEGIN
    widget_control,/hourglass
    new_value = diva_autoscale[1] - 1
    (*(*info).pdiva_draw).diva_autoscale[1] = new_value
    widget_control,(*(*info).pdiva_draw).tlb_bshift_text,   	    	    $
    	    	    	    	    set_value = strcompress(new_value,/rem)
    ;... then display image
    diva_draw_image, (*info).pdiva_draw
    
    o = (*(*info).pdiva_draw).diva_current_image.ops_name
    t = strmid(o,0,strpos(o,'(',/reverse_search)) + 	    	    	    $
		    	    '(0,' + strcompress(new_value,/rem) + ')'
    (*(*info).pdiva_draw).diva_current_image.ops_name = t
    (*(*info).pdiva_draw).diva_current_image.ops_bshift =	  				$
		    	    	    	    [0,strcompress(new_value,/rem)]

	CASE new_value OF
		0:		sens = 0
		ELSE:	sens = 1
	ENDCASE
	widget_control,(*(*info).pdiva_draw).tlb_bshift_save,sensitive = sens
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"BSHFT_SAVE":	$
BEGIN
	widget_control,/hourglass
	diva_image		= (*(*info).pdiva_draw).diva_current_image
	f				= (*(*info).pdiva_draw).diva_current_image.file_info.f
	diva_n_images	= (*(*info).pdiva_draw).diva_current_image.diva_n_images
	bshift_val		= (*(*info).pdiva_draw).diva_autoscale[1]
	
	CASE diva_n_images OF
		1:		index 	= 0
		ELSE:	index	= (*info).diva_prev_mult_index
	ENDCASE
	
	wid_id			= (*info).tlb
	
	print,index
	
	comment_0	= ' Source image: ' + strcompress(f,/rem)
	comment_1	= ' ...and was image #' + strcompress(index + 1,/rem) +		$
					' of ' + strcompress(diva_n_images,/rem)
	comment_2	= ' This image was created by bitshifting the original' + 	$
							' image by: ' + strcompress(bshift_val,/rem)
	comments	= [comment_0,comment_1,comment_2]
	func		= 'bitshift'
	
	IF (bshift_val gt 0) THEN bsname = 'bsp' + strcompress(bshift_val,/rem)
	IF (bshift_val lt 0) THEN bsname = 'bsn' + 								$
							strcompress(FIX(ABS(bshift_val)),/rem)
	
	;call "diva_make_fits_file" procedure to make a fits file
	diva_make_fits_file,diva_image,index,comments,wid_id,func,				$
														bsname,bshift_val
	
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"FMOS_QUART":	$	;
BEGIN
	widget_control,/hourglass
	
	i_xsize = (*(*info).pdiva_draw).diva_current_image.xsize
	i_ysize = (*(*info).pdiva_draw).diva_current_image.ysize
	pimage	= (*(*(*info).pdiva_draw).diva_current_image.pimage)
	
	;make a new array (1/4 size of pimage x 4)
	pimage_q	= UINTARR((i_xsize/2),(i_ysize/2),4)
	
	;in order, llhs, ulhs, lrhs, urhs
	pimage_q[*,*,0]	= pimage[0:(i_xsize/2)-1,0:(i_ysize/2)-1]
	pimage_q[*,*,1]	= pimage[0:(i_xsize/2)-1,(i_ysize/2):i_ysize-1]
	pimage_q[*,*,2]	= pimage[(i_xsize/2):i_xsize-1,0:(i_ysize/2)-1]
	pimage_q[*,*,3]	= pimage[(i_xsize/2):i_xsize-1,(i_ysize/2):i_ysize-1]
	
	filename	= (*(*info).pdiva_draw).diva_current_image.file_info.file_str
	pathname	= (*(*info).pdiva_draw).diva_current_image.file_info.pathname
	guess_name	= pathname + filename + '__q.fit'
	wid_id		= (*info).tlb
	
	;call "diva_get_filename" FUNCTION to return user-selected "get_f"
	get_f 	= diva_get_filename(guess_name,pathname,wid_id)
	
	FOR i = 0,3 DO BEGIN
		index = i
		diva_conc_make_fits_file,pimage_q[*,*,i],index,get_f
	ENDFOR
	
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
					    					
ELSE: MESSAGE, "Event User Value Not Found"		
ENDCASE
ENDELSE

if  (Ptr_Valid(diva_zoom_info) eq 1) OR      	    	    	    	    $
    (Ptr_Valid(diva_line_info) eq 1) THEN BEGIN
    
    xsize	= (*(*info).pdiva_draw).diva_current_image.xsize
    ysize	= (*(*info).pdiva_draw).diva_current_image.ysize
    
    IF (Ptr_Valid(diva_zoom_info) eq 1) THEN BEGIN
    	(*(*(*info).pdiva_draw).diva_zoom_info).diva_autoscale = 	    	$
    	    	    	    	    (*(*info).pdiva_draw).diva_autoscale
    	;call 'diva_zoom_display' to update zoom image
    	diva_zoom_display,(*(*info).pdiva_draw).diva_zoom_info
    
		xz = (*diva_zoom_info).xz
		yz = (*diva_zoom_info).yz
    ENDIF
    
    IF (Ptr_Valid(diva_line_info) eq 1) THEN BEGIN
    	c = (*diva_line_info).coords
    ENDIF 
	
    widget_control,diva_tlb_drawwid, get_value=tlb_draw
    wset,tlb_draw
    
    Device,Copy=[0,0,xsize,ysize,0,0,(*(*info).pdiva_draw).diva_tlb_pixindex]
    tvlct,  (*(*info).pdiva_draw).plot_ct[*,0],			            		$
    	    (*(*info).pdiva_draw).plot_ct[*,1],			            		$
	    	(*(*info).pdiva_draw).plot_ct[*,2]
	
	
    IF (Ptr_Valid(diva_zoom_info) eq 1) THEN 	    	    	    	    $
					plots,xz,yz,/device,thick=1,color=2
    IF (Ptr_Valid(diva_line_info) eq 1) THEN 	    	    	    	    $
					plots,[c[0],c[2]],[c[1],c[3]],/device,color=2,psym=-6
	
    tvlct,  (*(*info).pdiva_draw).image_ct[*,0],    	    	    	    $
    	    (*(*info).pdiva_draw).image_ct[*,1],     	    	    	    $
	    	(*(*info).pdiva_draw).image_ct[*,2]    
endif



widget_control,event.top,set_uvalue=info,/no_copy
END ;end of "diva" event handler-----------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA
; CATEGORY:
;		widget
; AUTHOR:
;		John A. Rainnie - RAL (j.a.rainnie@rl.ac.uk)
; PURPOSE:
;		creation of main DIVA GUI
; CATEGORY:
;		image viewer and processor GUI
; CALLING SEQUENCE:
;		@d ("d.pro" is a batch file)
; INPUTS:
; OPTIONAL INPUTS:
; KEYWORD PARAMETERS:
; OUTPUTS:
; OPTIONAL OUTPUTS:
; COMMON BLOCKS:
;		none
; SIDE EFFECTS:
; RESTRICTIONS:
; PROCEDURE:
;		obvious
; NOTES:
; EXAMPLE:
; PROCEDURES USED:
; MODIFICATION HISTORY:
;		Original Version written in 2003 by John A. Rainnie at RAL
;-
PRO diva, GROUP = GROUP
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
IF(XRegistered("diva") NE 0) THEN RETURN		


device,retain=2,decomposed=0
On_Error, 1 	; On an error condition, return to the main level of IDL.
!x.style = 1  &  !y.style = 1  &  !z.style = 1

;call 'diva_defaults' procedure to get DEFAULTS
diva_defaults,def_path,def_file,tlb_drawwid_size,diva_printers, 			$
			diva_printers_list


;call 'diva_make_webpage' procedure to make DIVA's IDL Extended HELP
diva_make_webpage



diva_prev_xsize = 250
;-------------------create the main base---------------------------------------;
tlb = WIDGET_BASE(TITLE = "DIVA",TLB_Frame_Attr=1,mbar=bar,/row)
    ;--------file menu list----------------------------------------------------;
    file_menu	= WIDGET_BUTTON(bar, VALUE='File', /MENU)
    
    diva_file_open  = widget_button(file_menu,value='Open...',	    	    $
    	    	    	    uvalue='DIVA_OPEN')     

    diva_adapt_butt = widget_button(file_menu,value='Adaptive Mode...',  	$
    	    	    	    uvalue='ADAPT_MODE',/separator)

    diva_print_butt = widget_button(file_menu,value='Export...',	    	$
    	    	    	    uvalue='DIVA_EXPORT',sensitive=0,/separator)    

    diva_exit 		= widget_button(file_menu,value='Exit', 				$
							uvalue='DIVA_EXIT',/separator) 

    diva_file_buttids = [diva_print_butt]       
    ;--------------------------------------------------------------------------;
    ;--------edit menu list----------------------------------------------------;
    edit_menu 	= WIDGET_BUTTON(bar, VALUE='Edit' ,/MENU,sensitive=0)
			    
	diva_edit_saved = widget_button(edit_menu,value='Store image',			$
						uvalue='DIVA_SAVED_CURR') 
    ;--------------------------------------------------------------------------;
    ;--------view menu list----------------------------------------------------;
    view_menu	= WIDGET_BUTTON(bar, VALUE='View' ,/MENU,sensitive=0)
		
    diva_view_orig  = widget_button(view_menu,value='Revert to original',	$
    	    	    	    uvalue='PREV_LOAD')
    
	diva_view_neg   = widget_button(view_menu,value='Negative Image', 		$
    	    	    	    /separator,uvalue='DIVA_NEG')
    
	
	diva_view_rot   = widget_button(view_menu,value='Rotate Image', 		$
    	    	    	    /menu)
    	    ;rotation sub menu-------------------------------------------------;
			diva_rot_left 	= widget_button(diva_view_rot,value='+ CW', 	$
    	    	    	    	uvalue='DIVA_ROT_CW')

    	    diva_rot_right	= widget_button(diva_view_rot,value='- CW', 	$
    	    	    	    	uvalue='DIVA_ROT_ACW')

    	    diva_rot_flip	= widget_button(diva_view_rot,value='Flip', 	$
    	    	    	    	uvalue='DIVA_ROT_F')

    diva_view_fit   = widget_button(view_menu,value='Fit to',/menu)
			;size sub menu-----------------------------------------------------;
    	    diva_fit_best 	= widget_button(diva_view_fit,value='Best Fit', $
    	    	    	    	uvalue='DIVA_FIT_B')
		    
    	    diva_fit_height = widget_button(diva_view_fit,value='Height',   $
    	    	    	    	uvalue='DIVA_FIT_H')

    	    diva_fit_width	= widget_button(diva_view_fit,value='Width',	$
    	    	    	    	uvalue='DIVA_FIT_W')

    diva_view_all   = widget_button(view_menu,value='All image...',  	    $
    	    	    	    uvalue='DIVA_ALL',/separator)
    ;--------------------------------------------------------------------------;
    ;--------tool menu list----------------------------------------------------;
    tool_menu	= WIDGET_BUTTON(bar, VALUE='Tools' ,/MENU,sensitive=0)

    diva_tool_stats  	= widget_button(tool_menu,uvalue='DIVA_STATS',      $
    	    	    	    value='Statistics...')
    diva_tool_histog   	= widget_button(tool_menu,uvalue='DIVA_HISTOG',	    $
    	    	    	    value='Histogram...')
    diva_tool_math  	= widget_button(tool_menu,uvalue='DIVA_MATH',	    $
    	    	    	    value='Image Math...')
    diva_tool_ptc   	= widget_button(tool_menu,uvalue='DIVA_PTC',	    $
    	    	    	    value='2-image P.T.C. ...')
    diva_tool_conc  	= widget_button(tool_menu,uvalue='DIVA_CONC',	    $
    	    	    	    value='Concatenator...')    
    diva_tool_xmtool	= widget_button(tool_menu,uvalue='DIVA_XMTOOL',     $
    	    	    	    value='XMtool...',/separator)
    diva_tool_xloadct   = widget_button(tool_menu,uvalue='DIVA_XLOADCT',    $
    	    	    	    value='XLoadCT...')
    ;--------------------------------------------------------------------------;
    ;--------function menu list------------------------------------------------;
    func_menu	= widget_button(bar,value='ROI',/menu,sensitive=0)
    junk = {CW_PDMENU_S, flags:0, name:'' }
    desc = [ 	{ CW_PDMENU_S, 1, 'Area' },     	                        $ 
             	{ CW_PDMENU_S, 0, 'Regular' }, 	                            $ 
             	{ CW_PDMENU_S, 0, 'Irregular' },							$ 
	     		{ CW_PDMENU_S, 2, 'Circular' }, 							$ 
             	{ CW_PDMENU_S, 0, 'Zoom' },    	                            $ 
             	{ CW_PDMENU_S, 1, 'Line' },    	                            $ 
             	{ CW_PDMENU_S, 0, 'Horz/Vert' },							$ 
	     		{ CW_PDMENU_S, 0, 'All col' }, 	                            $ 
	     		{ CW_PDMENU_S, 0, 'All row' }, 	                            $ 
             	{ CW_PDMENU_S, 0, 'Any' }]
    
    menu =  CW_PDMENU(func_menu, desc,/mbar,uvalue='FUNCTION')
    ;--------------------------------------------------------------------------;
    
    diva_menus = [edit_menu,view_menu,tool_menu,func_menu]

    ;--------temp menu list----------------------------------------------------;
    temp_menu 	= WIDGET_BUTTON(bar, VALUE='Temp' ,/MENU)

    diva_tempi_butt     = widget_button(temp_menu,value='image/4',    		$
    	    	    	    uvalue='DIVA_TEMPI',sensitive=1)
    ;--------fmos menu list----------------------------------------------------;
    fmos_menu 	= WIDGET_BUTTON(bar, VALUE='FMOS' ,/MENU)

    diva_fmos_quart     = widget_button(fmos_menu,value='Quarter..',    	$
    	    	    	    uvalue='FMOS_QUART',sensitive=1)
    ;--------help menu list----------------------------------------------------;
    help_menu	= WIDGET_BUTTON(bar,VALUE='Help',/MENU,/help)

    diva_help_help	= widget_button(help_menu,value='Help...',				$
    	    				uvalue='DIVA_HELP')
    diva_help_vers	= widget_button(help_menu,value='Version...',  	    	$
    	    				uvalue='DIVA_VERS',/separator)
    diva_help_about = widget_button(help_menu,value='About...',  	    	$
    	    				uvalue='DIVA_ABOUT')
    ;--------------------------------------------------------------------------;
;---------------------------END OF FILE MENU BUTTONS---------------------------;
;------------------------------------------------------------------------------;

;------------------------------------------------------------------------------;
;																			   ;
;								CURRENT IMAGE PANE							   ;
;																			   ;
;------------------------------------------------------------------------------;
;------------tlb mid column base-----------------------------------------------;
tlb_mid_base 				= widget_base(tlb,/col,sensitive=1,frame=4)

diva_draw_base  			= widget_base(tlb_mid_base,/col)

;make top base - autoscaling/bitshifting
tlb_top_base 				= widget_base(diva_draw_base,/row,/frame,		$
									sensitive=0)
tlb_auto_base 				= widget_base(tlb_top_base,/row,/frame)

tlb_auto_label				= widget_label(tlb_auto_base,value='Autoscale:')
tlb_auto_base_exc			= widget_base(tlb_auto_base,/row,/exclusive)

diva_auto_on				= widget_button(tlb_auto_base_exc,				$
									value='On',								$
									uvalue='AUTOSC_ON')
diva_auto_off				= widget_button(tlb_auto_base_exc,				$
									value='Off',							$
									uvalue='AUTOSC_OFF')
widget_control,diva_auto_on,set_button=1

tlb_bshift_base 			= widget_base(tlb_top_base,/row,/frame, 		$
									sensitive=1)
tlb_bshift_label			= widget_label(tlb_bshift_base, 				$
									value='Bitshift:')
tlb_bshift_down 			= widget_button(tlb_bshift_base,				$
									value=' < ',     	    				$
    								uvalue='BSHFT_DOWN')
tlb_bshift_text 			= widget_text(tlb_bshift_base,xsize=4,			$
									value='0')	
tlb_bshift_up				= widget_button(tlb_bshift_base,				$
									value=' > ',     	    				$
    								uvalue='BSHFT_UP')

;make draw area 
diva_tlb_drawwid 			= widget_draw(diva_draw_base,/scroll,			$
    								xsize=tlb_drawwid_size[0],				$
									ysize=tlb_drawwid_size[1],				$
									frame=6,								$
									x_scroll_size=tlb_drawwid_size[0],		$
									y_scroll_size=tlb_drawwid_size[1])
									
;make pixmap window (and store its id)
window,/free,xsize = tlb_drawwid_size[0],ysize = tlb_drawwid_size[1],/pixmap
diva_tlb_pixindex = !d.window

;set DIVA title
widget_control,tlb,TLB_Set_Title = 'DIVA  --> no image selected'

;make information base - cursor position/value,etc
tlb_info_base				= widget_base(diva_draw_base,/row,/frame)

tlb_info_curs_t 			= widget_text(tlb_info_base,					$
									value='0,0',xsize=10)

tlb_info_valu_t 			= widget_text(tlb_info_base,					$
									value='0',xsize=6)

tlb_info_func_label 		= widget_label(tlb_info_base,					$
									value=' Function:')
tlb_info_func_text  		= widget_text(tlb_info_base,					$
									value = '',xsize = 16)			    

tlb_info_size_label 		= widget_label(tlb_info_base,value=' Size:')
tlb_info_size_text  		= widget_text(tlb_info_base,					$
									value = '',xsize = 10)			    

;------------------------------------------------------------------------------;
;																			   ;
;								PREVIEW PANE								   ;
;																			   ;
;------------------------------------------------------------------------------;
;------------tlb rhs column base-----------------------------------------------;
tlb_rhs_base 				= widget_base(tlb,/col,frame=4,sensitive=0)
tlb_prev_mode_label_id  	= widget_label(tlb_rhs_base,value=				$
									'******   PREVIEW  MODE   ******')
tlb_prev_name_label_id  	= widget_label(tlb_rhs_base,					$
									value='none',							$
    								xsize=diva_prev_xsize)
		
tlb_prev_size_label_id  	= widget_label(tlb_rhs_base,					$
									xsize=diva_prev_xsize,					$
									value = '[Bytes: - ]  [Images: - ]')


;------------preview mapped base-----------------------------------------------;
tlb_prev_list_mapped_base	= widget_base(tlb_rhs_base, 					$
									/align_center,							$
									xsize=diva_prev_xsize+10)
;------------single mapped base------------------------------------------------;
tlb_prev_list_sin_base		= widget_base(tlb_prev_list_mapped_base,		$
									map=1,/col,/frame,						$
									xsize=diva_prev_xsize+10,/align_center)
tlb_prev_list 				= widget_list(tlb_prev_list_sin_base,			$
									value='wot',xsize=37,ysize=21,			$
									uvalue='DIVA_PREV',/align_left, 		$
									/context_events)
		
	prev_contextbase 		= widget_base(tlb_prev_list_sin_base,			$
									/context_menu)
	
	value = [	'View header/log...'	,'All image...' ,'Remove entry',	$
				'View FITs summary...'	,'Extract image','Store image' ,	$
				'Store ALL images'	]
							
	b1 =widget_button(prev_contextbase,value =value[0],uvalue='PREV_VLOG' )
	b2 =widget_button(prev_contextbase,value =value[1],uvalue='PREV_ALL'  )
	b3 =widget_button(prev_contextbase,value =value[2],uvalue='PREV_REMV' )
	b4 =widget_button(prev_contextbase,value =value[3],uvalue='DUMMY'	  )
	b5 =widget_button(prev_contextbase,value =value[4],uvalue='PREV_EXTRACT')
	b6 =widget_button(prev_contextbase,value =value[5],uvalue='PREV_STORE')
	b7 =widget_button(prev_contextbase,value =value[6],uvalue='PREV_STORE_ALL')
	
	prev_context_list = [b1,b2,b3,b4,b5,b6,b7]
	
;------------multiple mapped base----------------------------------------------;
tlb_prev_list_mul_base		= widget_base(tlb_prev_list_mapped_base,		$
									map=0,									$
									/col,									$
									/frame,									$
									xsize=diva_prev_xsize+10,				$
									/align_center)
	
tlb_goback_butt 			= widget_button(tlb_prev_list_mul_base, 	    $
    								value='Back to Main list',				$
									uvalue='PREV_BACK', 					$
									/align_center, 							$
									xsize=diva_prev_xsize-15)

tlb_prev_list_mul 			= widget_list(tlb_prev_list_mul_base,			$
									value='back...',     					$
    								xsize=37,								$
									ysize=20,								$
									uvalue='DIVA_MULT', 					$
									/align_left,							$
									/context_events)
;------------adaptive mapped base----------------------------------------------;
tlb_adapt_list_base 		= widget_base(tlb_prev_list_mapped_base,		$
									map=0,/col,/frame,						$
									xsize=diva_prev_xsize+10,				$
									/align_center)

tlb_adapt_auto_onoff_base	= widget_base(tlb_adapt_list_base,/row,     	$
    								xsize=diva_prev_xsize-4,				$
									/frame,/align_left)
tlb_adapt_auto_onoff_label	= widget_label(tlb_adapt_auto_onoff_base,  	    $
    								value='Auto-Load:')

tlb_adapt_auto_exc_base 	= widget_base(tlb_adapt_auto_onoff_base,		$
									/row,/exclusive)
tlb_adapt_auto_on_button	= widget_button(tlb_adapt_auto_exc_base,		$
									value='On ', 							$
    								uvalue='AD_AUTO_ON')
tlb_adapt_auto_off_button	= widget_button(tlb_adapt_auto_exc_base,		$
									value='Off',							$
    								uvalue='AD_AUTO_OFF')


tlb_adapt_off_butt			= widget_button(tlb_adapt_list_base,			$
									uvalue='ADAPT_OFF',						$
									value='Quit Adaptive Mode', 			$
									/align_center, 							$
									xsize=diva_prev_xsize-15)



tlb_adapt_animate_lhs 		= widget_label(tlb_adapt_auto_onoff_base,		$
									value='   [')
tlb_adapt_animate 			= widget_label(tlb_adapt_auto_onoff_base,		$
									value='*')
tlb_adapt_animate_rhs 		= widget_label(tlb_adapt_auto_onoff_base,		$
									value=']')

tlb_adapt_list				= widget_list(tlb_adapt_list_base,				$
									xsize=37,								$
									ysize=17,    	    					$
    								uvalue='AD_LIST',						$
									/align_left,							$
									/context_events)

;this button is reset when entering adaptive mode
widget_control,tlb_adapt_auto_on_button,set_button=1
diva_ad_buttons = [tlb_adapt_auto_on_button]
;------------preview/adaptive mode info & button base--------------------------;
tlb_prev_info_base			= widget_base(tlb_rhs_base,/frame,/col, 		$
									/align_center,      					$
    								xsize=diva_prev_xsize+6)

tlb_prev_info_name			= widget_label(tlb_prev_info_base,				$
									value='none',    	    				$
    								xsize=diva_prev_xsize)
tlb_prev_info_size			= widget_label(tlb_prev_info_base, 	    	    $
    								value='0 bytes',						$
									xsize=diva_prev_xsize)
tlb_prev_info_dims			= widget_label(tlb_prev_info_base, 	    	    $
    								value='0 x 0 x 0 pixels',				$
									xsize=diva_prev_xsize)
									
prev_info = [tlb_prev_info_name,tlb_prev_info_size,tlb_prev_info_dims]
;------------load as current image button--------------------------------------;
tlb_prev_load_butt			= widget_button(tlb_rhs_base,					$
									sensitive=0,							$
									ysize=50,   	    					$
    								value='Load (as current image)',		$
									uvalue='PREV_LOAD', 	    	    	$
									/align_center,							$
									/frame, 								$
									xsize=diva_prev_xsize+6,				$
									RESOURCE_NAME='loadbutton')

tlb_preview_list 			= [ tlb_rhs_base,								$
								tlb_prev_list,								$
								tlb_prev_list_mul,							$
								tlb_prev_load_butt, 						$
								tlb_prev_list_sin_base, 					$
								tlb_prev_list_mul_base, 					$
								tlb_adapt_list_base 		]

;------------preview draw area + mapped button base----------------------------;
tlb_prev_draw_base  		= widget_base(tlb_rhs_base)

tlb_prev_drawwid			= widget_draw(tlb_prev_draw_base,				$
									xsize=diva_prev_xsize,					$
    								ysize=diva_prev_xsize)

;------------------------------------------------------------------------------;
;-------------------realise the widget-----------------------------------------;
WIDGET_CONTROL, tlb, /REALIZE		
loadct,0
tvlct,r,g,b,/get

;call 'pdiva_draw_init' function to initialise 'pdiva_draw' structure
pdiva_draw=pdiva_draw_init(tlb,tlb_mid_base,diva_draw_base,  	    	    $
    	    	    	diva_tlb_drawwid,diva_tlb_pixindex,r,g,b,			$
						!d.name,tlb_drawwid_size,tlb_prev_drawwid,			$
						diva_printers,diva_printers_list)
			
info = Ptr_New({															$
				tlb     	    	        : tlb,  						$
		  		diva_file_info   	        : Ptr_New(),					$
		  		diva_mult_image   	        : Ptr_New(),					$
		  		pdiva_draw					: pdiva_draw,    	    	    $
		        os_name 				    : !d.name,  					$
		        def_path				    : def_path, 					$
		        def_file				    : def_file, 					$
				tlb_preview_list			: tlb_preview_list, 			$
		        tlb_prev_list			    : tlb_prev_list,				$
		        tlb_prev_list_mul		    : tlb_prev_list_mul,			$
		        tlb_prev_list_sin_base      : tlb_prev_list_sin_base,		$
		        tlb_prev_list_mul_base      : tlb_prev_list_mul_base,		$
				tlb_prev_load_butt			: tlb_prev_load_butt,			$
		        tlb_adapt_list  		    : tlb_adapt_list,				$
		        tlb_adapt_list_base 	    : tlb_adapt_list_base,  		$
		        tlb_rhs_base			    : tlb_rhs_base, 				$
		        prev_buttons			    : [0L,0,0,0,0,0,0], 			$
		        prev_info				    : prev_info,					$
		        diva_prev_index 		    : 0,							$
		        diva_prev_mult_index	    : 0,							$
		        diva_display_logbase	    : 0L,							$
		        diva_adapt_mode_on  	    : 0,							$
		        tlb_prev_mode_label_id		: tlb_prev_mode_label_id,		$
		        tlb_prev_name_label_id		: tlb_prev_name_label_id,		$
				tlb_prev_size_label_id		: tlb_prev_size_label_id,		$
		        diva_ad_new_list		    : strarr(500),  				$
		        diva_ad_list_index  	    : 0,							$
		        diva_ad_pick_file		    : ['',''],  					$
		        diva_ad_buttons 		    : diva_ad_buttons,  			$
		        diva_time_events_on 	    : 1,							$
		        number_images			    : 0,							$
		        diva_ad_autoload_on 	    : 1,							$
		        tlb_ad_timer			    : [1.,0.],  					$
		        tlb_adapt_animate		    : tlb_adapt_animate,			$
		        diva_prev_xsize 		    : diva_prev_xsize,  			$
		        diva_file_buttids		    : diva_file_buttids,			$
		        diva_menus  			    : diva_menus,					$
				prev_contextbase 			: prev_contextbase,				$
				prev_context_list			: prev_context_list 			$
    	      })


widget_control,tlb_prev_draw_base,/destroy
tlb_prev_draw_base = widget_base(tlb_rhs_base)


tlb_prev_drawwid=widget_draw(tlb_prev_draw_base,xsize=diva_prev_xsize,		$
    	ysize=diva_prev_xsize,frame=6,Event_Pro='diva_prev_draw_events',    $
		/motion_events, /button_events, 									$
		uvalue={eventval:"DRAW_PREV_EVENT", pdiva_draw: pdiva_draw})
(*(*info).pdiva_draw).tlb_prev_drawwid = tlb_prev_drawwid

window,/free,xsize=diva_prev_xsize,ysize=diva_prev_xsize,/pixmap
(*(*info).pdiva_draw).tlb_prev_pixid=!d.window


diva_prev_button_base=widget_base(tlb_prev_draw_base,/col,map=0)
(*(*info).pdiva_draw).diva_prev_button_base = diva_prev_button_base

tlb_viewlog_butt = widget_button(diva_prev_button_base,	    				$
    	value=	'View header/log...',uvalue='PREV_VLOG')

tlb_prev_resz_butt=widget_button(diva_prev_button_base,						$
		value='All image...',uvalue='PREV_ALL')

tlb_prev_remv_butt=widget_button(diva_prev_button_base,sensitive=0,	    	$
    	value=' Remove entry ',uvalue='PREV_REMV')

tlb_prev_summ_butt=widget_button(diva_prev_button_base,sensitive=0,	    	$
    	value=' View FITs summary... ',uvalue='DUMMY')

tlb_prev_extr_butt=widget_button(diva_prev_button_base,sensitive=0,	    	$
    	value=' Extract image ',uvalue='PREV_EXTRACT')

tlb_prev_store_butt=widget_button(diva_prev_button_base,sensitive=0,	    	$
    	value=' Store image ',uvalue='PREV_STORE')

tlb_prev_storeall_butt=widget_button(diva_prev_button_base,sensitive=0,	    	$
    	value=' Store ALL images ',uvalue='PREV_STORE_ALL')

(*info).prev_buttons = [tlb_viewlog_butt	,tlb_prev_resz_butt,			$
						tlb_prev_remv_butt	,tlb_prev_summ_butt,			$
						tlb_prev_extr_butt	,tlb_prev_store_butt,			$
						tlb_prev_storeall_butt	]

;------------store 'info' structure--------------------------------------------;
widget_control,tlb,set_uvalue=info

;------------Register this program and set up the event loop-------------------;
XManager, "diva", tlb, EVENT_HANDLER = "diva_ev",   	    	    	    $
    	    group_leader=group,cleanup='diva_cleanup'
END ;end of "diva" main routine------------------------------------------------;
;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA_CLEANUP
; PURPOSE:
;		This procedure frees up all pointers when quitting DIVA. The final
;		act of this procedure is to issue a 'IDL> help,/heap' command. The
;		result of this shoud be that NO heap variables are left.
; INPUTS:
;		tlb - the widget id of the DIVA top level base
; AUTHOR:
;		John A. Rainnie - RAL (j.a.rainnie@rl.ac.uk)
;-
;------------------------------------------------------------------------------;
PRO diva_cleanup,tlb
;------------------------------------------------------------------------------;

widget_control,tlb,get_uvalue=info

IF (ptr_valid(info) eq 1) THEN BEGIN
    Ptr_Free,(*info).diva_file_info
    Ptr_Free,(*info).diva_mult_image
    ;current and current_prev image pointers
    Ptr_Free,(*(*info).pdiva_draw).diva_current_image.pimage
    Ptr_Free,(*(*info).pdiva_draw).diva_current_image.o_pimage
    Ptr_Free,(*(*info).pdiva_draw).diva_current_image.p_pimage
	Ptr_Free,(*(*info).pdiva_draw).diva_current_image.s_pimage
    Ptr_Free,(*(*info).pdiva_draw).diva_current_prev_image.pimage
    Ptr_Free,(*(*info).pdiva_draw).diva_current_prev_image.o_pimage
    Ptr_Free,(*(*info).pdiva_draw).diva_current_prev_image.p_pimage
	Ptr_Free,(*(*info).pdiva_draw).diva_current_prev_image.s_pimage

	;"diva_image_stored" image pointers
    if (Ptr_Valid((*(*info).pdiva_draw).diva_image_stored) eq 1) then BEGIN
    	how_many = n_elements((*(*(*info).pdiva_draw).diva_image_stored))
		for i=0,how_many -1 do begin
	    	Ptr_free,(*(*(*info).pdiva_draw).diva_image_stored)[i].pimage
	    	Ptr_free,(*(*(*info).pdiva_draw).diva_image_stored)[i].o_pimage
	    	Ptr_free,(*(*(*info).pdiva_draw).diva_image_stored)[i].p_pimage
			Ptr_free,(*(*(*info).pdiva_draw).diva_image_stored)[i].s_pimage
		ENDFOR
    	Ptr_FRee,(*(*info).pdiva_draw).diva_image_stored
    END

    ;line profile, zoom and roi info structure pointers
    if (Ptr_Valid((*(*info).pdiva_draw).diva_line_info) eq 1) then begin
    	stop
    end
    if (Ptr_Valid((*(*info).pdiva_draw).diva_zoom_info) eq 1) then begin
    	stop
    end
    if (Ptr_Valid((*(*info).pdiva_draw).diva_roi_info) eq 1) then begin
    	stop
    end
    if (Ptr_Valid((*(*info).pdiva_draw).diva_conc_info) eq 1) then begin
    	stop
    end
    if (Ptr_Valid((*(*info).pdiva_draw).diva_math_info) eq 1) then begin
    	stop
    end
    ;and finally, pdiva_draw and info pointers
    Ptr_free,(*info).pdiva_draw
    Ptr_Free,info
ENDIF else stop

help,/heap
END ;of 'diva_cleanup' procedure-----------------------------------------------;
;------------------------------------------------------------------------------;
