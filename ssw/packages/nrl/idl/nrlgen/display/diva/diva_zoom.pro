;------------------------------------------------------------------------------;
PRO diva_zoom_ev, event;-------------------------------------------------------;
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
WIDGET_CONTROL, event.id, GET_UVALUE = eventval
widget_control,event.top,get_uvalue=diva_zoom_info		

diva_zoombase			= (*diva_zoom_info).diva_zoombase
diva_zoom_report_cvalue = (*diva_zoom_info).diva_zoom_report_cvalue    
diva_zoom_report_vvalue = (*diva_zoom_info).diva_zoom_report_vvalue    
diva_zoom_size			= (*diva_zoom_info).diva_zoom_size  	       
diva_zoom_mag			= (*diva_zoom_info).diva_zoom_mag		       
zoom_factors			= (*diva_zoom_info).zoom_factors		       
xyc 					= (*diva_zoom_info).xyc 				       
image_ct				= (*diva_zoom_info).image_ct			       
plot_ct 				= (*diva_zoom_info).plot_ct 			       
tlb_stuff				= (*diva_zoom_info).tlb_stuff			       
n_dim					= (*diva_zoom_info).n_dim				       
diva_printers	  		= (*diva_zoom_info).diva_printers
diva_printers_list		= (*diva_zoom_info).diva_printers_list

CASE eventval OF

"EXIT": 		$
BEGIN
	;destroy widget & return
	WIDGET_CONTROL, event.top, /DESTROY
	RETURN		
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"ZOOM_F":           	    	    	    	    	    	    	    $
BEGIN
    diva_zoom_mag = zoom_factors[event.index]/2
    ;update 'diva_zoom_info' structure with new value of 'diva_zoom_mag' variable
    (*diva_zoom_info).diva_zoom_mag = diva_zoom_mag
    
    temp	    = (*diva_zoom_info)
    xd = xyc[0]   &   yd = xyc[1]
    diva_zoom = diva_zoom_size/diva_zoom_mag
    
    zoom_pimage = (*temp.pimage)[   xd-diva_zoom:xd+diva_zoom-1,	    $
	    			    yd-diva_zoom:yd+diva_zoom-1,*   	]
    
    CASE temp.n_dim OF
	    	1:  zoom_pimage =  rebin(zoom_pimage,320,320,  /sample)
	    	3:  zoom_pimage =  rebin(zoom_pimage,320,320,3,/sample)
    ENDCASE
    
    (*(*diva_zoom_info).zoom_pimage) = zoom_pimage
    
    ;get widget id of tlb draw area - and make current draw area
    widget_control,tlb_stuff[2],get_value=tlb_draw
    wset,tlb_draw
    ;redraw new bounding box here
    Device,Copy=[0,0,tlb_stuff[0],tlb_stuff[1],0,0,tlb_stuff[3]]
    tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2];change to PLOT colour table

    xz = [xd-diva_zoom,xd+diva_zoom,xd+diva_zoom,xd-diva_zoom,xd-diva_zoom ]
    yz = [yd+diva_zoom,yd+diva_zoom,yd-diva_zoom,yd-diva_zoom,yd+diva_zoom ]
    
    (*diva_zoom_info).xz = xz
    (*diva_zoom_info).yz = yz
    
    plots,xz,yz,/device,thick=1,color=2
    ;change back to IMAGE colour table
    tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2]

    ;call 'diva_zoom_display' procedure to display zoom image
    diva_zoom_display,diva_zoom_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"ZOOM_PRINT":	$
BEGIN	    	    	    	    	    	    	    
	pimage  		= (*diva_zoom_info).zoom_pimage
	diva_autoscale  = (*diva_zoom_info).diva_autoscale
	file_stuff		= (*diva_zoom_info).file_stuff
	
	filename 		= file_stuff[0]
	pathname		= file_stuff[1]
	file_str		= file_stuff[2]
	mfile_name		= file_stuff[3]
	
	IF (mfile_name ne '') then file_str = file_str + '__' + mfile_name
	
	diva_whattoprint = 2
	
	;call "diva_export" procedure
	diva_export, pimage,pathname,file_str,diva_autoscale,   	    	    $
		    	    	diva_whattoprint,diva_printers, 					$
						diva_printers_list,group=diva_zoombase
	
	
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"ZOOM_DRAW_EVENT":  	    	    	    	    	    	    	    $
BEGIN
    temp	    = (*diva_zoom_info)
    diva_zoom	= diva_zoom_size/diva_zoom_mag
    zoom_pimage = (*temp.zoom_pimage)
    
    possibleEventTypes = [ 'DOWN', 'UP', 'MOTION', 'SCROLL' ]
    thisEvent = possibleEventTypes(event.type)

    IF (thisevent eq 'MOTION') THEN BEGIN
		event.x = 0 > event.x < (320 - 1)
		event.y = 0 > event.y < (320 - 1)
		xz = event.x & yz = event.y
		
	
		xd = xyc[0]   &   yd = xyc[1]
	
		;report cursor position and value to zoom widget
		xv = strcompress((xz/(2*diva_zoom_mag))+ xd-diva_zoom,/remove)
		yv = strcompress((yz/(2*diva_zoom_mag))+ yd-diva_zoom,/remove)
		
		widget_control,diva_zoom_report_cvalue,set_value = xv + ',' + yv
		
		
		IF (size(zoom_pimage,/type) eq 1) THEN BEGIN
			CASE temp.n_dim OF
	    		1:	vv =  strcompress(zoom_pimage[xz,yz],/remove)
	    		3:	vv =  	  strcompress(FIX(zoom_pimage[xz,yz,0]),/rem)+  $
	    	    		',' + strcompress(FIX(zoom_pimage[xz,yz,1]),/rem)+  $
	    	    		',' + strcompress(FIX(zoom_pimage[xz,yz,2]),/rem)
			ENDCASE
		ENDIF ELSE BEGIN	
			CASE temp.n_dim OF
	    		1:	vv =  strcompress(zoom_pimage[xz,yz],/remove)
	    		3:	vv =  	  strcompress(zoom_pimage[xz,yz,0],/rem)+ 		$
	    	    		',' + strcompress(zoom_pimage[xz,yz,1],/rem)+ 		$
	    	    		',' + strcompress(zoom_pimage[xz,yz,2],/rem)
			ENDCASE
		ENDELSE	
			    
		widget_control,diva_zoom_report_vvalue,set_value = vv

	END    
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"ZOOM_STATS":	BEGIN
    temp	    	= (*diva_zoom_info)
    n_dim   	    = temp.n_dim
    diva_zoombase   = temp.diva_zoombase
    xd		    	= xyc[0]
    yd		    	= xyc[1]
    zoom_pimage     = (*temp.zoom_pimage)
    temp_zoom	    = diva_zoom_size/diva_zoom_mag

    x	    	    = [xd-temp_zoom,xd+temp_zoom] 
    y	    	    = [yd-temp_zoom,yd+temp_zoom] 
    ;call 'diva_stats_calc' procedure (in 'diva_stats.pro')
    diva_stats_calc,zoom_pimage,x,y,1,n_dim,diva_zoombase
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!		
"ZOOM_SURF":	BEGIN
	IF (XRegistered("diva_surf") eq 1) THEN RETURN
	
	zoom_pimage = (*diva_zoom_info).zoom_pimage
	file_stuff	= (*diva_zoom_info).file_stuff
						
	CASE n_dim OF
		1:      diva_surf,Ptr_new(*zoom_pimage),file_stuff,n_dim,			$
								diva_printers,diva_printers_list,			$
								diva_surf_info, 							$
								group = (*diva_zoom_info).diva_zoombase
		ELSE:   print,'not for rgb images......YET!!!!'
	ENDCASE

	;free up "diva_surf_info" pointer
	ptr_free,(*diva_zoom_info).diva_surf_info
		(*diva_zoom_info).diva_surf_info = Ptr_New(diva_surf_info)
	
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

ELSE: MESSAGE, "Event User Value Not Found"		
ENDCASE
END ;end of diva_zoom event handling routine task------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_zoom_info__define

junk =	{	diva_zoom_info, 												$
		    n_dim		    		    :0, 		    			        $
		    xsize   	    	        :0, 		    			        $
		    ysize   	    	        :0, 		    			        $
		    diva_zoombase   	        :0L,		    			        $
		    diva_zoom_size  	        :0, 		    			        $
		    diva_zoom_mag   	        :1, 		    			        $
		    zoom_motion_events_on       :0, 		    			        $
		    diva_zoom_draw	    	    :0L,		    			        $
		    zoom_factors    	        :intarr(5),     			        $
		    rgb_on		    		    :[0,0,0],	    			        $
		    diva_rgb_butts	    	    :[0L,0L,0L],    			        $
		    image_ct		    	    :bytarr(256,3), 			        $
		    plot_ct		    		    :bytarr(256,3), 			        $
		    diva_zoom_report_cvalue     :0L,		    			        $
		    diva_zoom_report_vvalue     :0L,		    			        $
		    pimage  	    	        :Ptr_new(),     			        $
		    zoom_pimage     	        :Ptr_New(),     			        $
		    diva_autoscale  	        :[0,0], 	    			        $
		    xyc     	    	        :[0L,0L],	    			        $
		    xz     	    	    	    :[0L,0,0,0,0],  			        $
		    yz     	    	    	    :[0L,0,0,0,0],  			        $
		    tlb_stuff	    	        :[0L,0,0,0,0],  			        $
		    file_stuff	    	        :['','','',''],  			        $
			diva_surf_info				:Ptr_NEW(),							$
			diva_zoom_surf_button		:0L, 								$
		    diva_printers				:strarr(3), 						$
			diva_printers_list			:strarr(3)							$
    	}
END;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
FUNCTION diva_zoom_init,n_dim,image_ct,plot_ct,diva_autoscale,	    	    $
    	    	pimage,tlb_stuff,file_stuff,diva_draw_base,diva_printers,	$
				diva_printers_list
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;

diva_zoom_info	    	    		=   {diva_zoom_info}
diva_zoom_info.n_dim	    		=   n_dim
diva_zoom_info.zoom_factors 		=   [2,4,8,16,32]
diva_zoom_info.diva_zoom_size		=   80L
diva_zoom_info.diva_zoom_mag		=   1
diva_zoom_info.image_ct     		=   image_ct
diva_zoom_info.plot_ct      		=   plot_ct
diva_zoom_info.diva_autoscale		=   [diva_autoscale[0],diva_autoscale[1]]
diva_zoom_info.pimage	    		=   pimage
diva_zoom_info.tlb_stuff    		=   tlb_stuff
diva_zoom_info.file_stuff   		=   file_stuff
diva_zoom_info.diva_printers    	=	diva_printers	 
diva_zoom_info.diva_printers_list	=	diva_printers_list
;-------------------create the main base---------------------------------------;
diva_zoombase 				= WIDGET_BASE(TITLE = "DIVA ZOOM",/col, 		$
    	    	    	    		group_leader=diva_draw_base,/floating)
diva_zoom_info.diva_zoombase = diva_zoombase
;-------------------diva zoom button base--------------------------------------;
diva_zoom_butt_1_base		= widget_base(diva_zoombase,/row,/frame)

diva_zoom_butt_mag_list 	= widget_droplist(diva_zoom_butt_1_base,	    $
    	    						value=['2x','4x','8x','16x','32x'], 	$
									title='Zoom Factor:',	    			$
	    							uvalue='ZOOM_F')
diva_zoom_stats 			= widget_button(diva_zoom_butt_1_base,			$
									value='Stats...',       				$
	    							uvalue='ZOOM_STATS')
diva_zoom_print 			= widget_button(diva_zoom_butt_1_base,			$
									value='Print...',       				$
	    							uvalue='ZOOM_PRINT')
;-------------------button2 base-----------------------------------------------;		
diva_zoom_butt_2_base		= widget_base(diva_zoombase,/row,/frame)

diva_zoom_surf_button 		= widget_button(diva_zoom_butt_2_base,			$
									value='Surface...', 					$
									uvalue='ZOOM_SURF')

;-------------------draw window------------------------------------------------;
diva_zoom_draw				= widget_draw(diva_zoombase,					$
									xsize=320,ysize=320,	    	    	$
									Event_Pro='diva_zoom_ev',  	    	    $
									uvalue="ZOOM_DRAW_EVENT",				$
    	    						Button_Events=1,						$
            						/motion_events, 						$
									/viewport_events)

diva_zoom_info.diva_zoom_draw = diva_zoom_draw		
;-------------------reporting widget-------------------------------------------;
diva_zoom_report_base		= widget_base(diva_zoombase,/row,/frame)

diva_zoom_report_clabel 	= widget_label(diva_zoom_report_base,   	    $
    	    						value='Cursor:')
diva_zoom_report_cvalue 	= widget_text(diva_zoom_report_base,    	    $
    	    						value='0,0',xsize=10)

diva_zoom_report_vlabel 	= widget_label(diva_zoom_report_base,   	    $
    	    						value='  Value:')
CASE n_dim OF
    1:	minmaxval = '0'
    3:	minmaxval = '0,0,0'
ENDCASE
diva_zoom_report_vvalue 	= widget_text(diva_zoom_report_base,    	    $
    	    						value=minmaxval,xsize=12)

diva_zoom_info.diva_zoom_report_cvalue = diva_zoom_report_cvalue
diva_zoom_info.diva_zoom_report_vvalue = diva_zoom_report_vvalue
;-------------------quit button------------------------------------------------;
quitbutton					= widget_button(diva_zoombase,value='Quit', 	$
									uvalue='EXIT',/frame)
;------------------------------------------------------------------------------;

WIDGET_CONTROL, diva_zoombase, /REALIZE		

pDiva_zoom_Info = Ptr_New(diva_zoom_info)

widget_control,diva_zoombase,set_uvalue=pDiva_zoom_Info
    
XManager, "diva_zoom", diva_zoombase,EVENT_HANDLER = "diva_zoom_ev",        $
		GROUP_LEADER = tlb_stuff[4],cleanup = 'diva_zoom_cleanup'
		
Return,pDiva_zoom_Info		
		
END ;==================== end of diva_zoom main routine =======================
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_zoom_cleanup,diva_zoombase
;------------------------------------------------------------------------------;
; Purpose - to cleanup diva_line widget - specifically to free it's pointer!
;------------------------------------------------------------------------------;
widget_control,diva_zoombase,get_uvalue=diva_zoom_info
print,'CLEANING UP DIVA ZOOM    *************************'
IF (Ptr_Valid(diva_zoom_info) eq 1) then begin
    Ptr_Free,(*diva_zoom_info).zoom_pimage
	Ptr_Free,(*diva_zoom_info).diva_surf_info
    Ptr_Free,diva_zoom_info
ENDIF
END;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_zoom_display,diva_zoom_info
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
n_dim	    	    	= (*diva_zoom_info).n_dim
diva_zoom_draw	    	= (*diva_zoom_info).diva_zoom_draw
diva_autoscale_on   	= (*diva_zoom_info).diva_autoscale[0]
bitshift_val	    	= (*diva_zoom_info).diva_autoscale[1]
zoom_pimage 	    	= (*(*diva_zoom_info).zoom_pimage)

widget_control,diva_zoom_draw,get_value=zoom_index
wset,zoom_index

CASE diva_autoscale_on OF
    0:  BEGIN
    	    if (n_dim eq 1) then tv,ishft(zoom_pimage,bitshift_val) 	    $
    	    ELSE    	    	 tv,ishft(zoom_pimage,bitshift_val),true=3
    	END
    1:  BEGIN
    	    if (n_dim eq 1) then tvscl,zoom_pimage  	    	    	    $
    	    ELSE    	    	 tvscl,zoom_pimage,true=3
    	END
ENDCASE

END;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
