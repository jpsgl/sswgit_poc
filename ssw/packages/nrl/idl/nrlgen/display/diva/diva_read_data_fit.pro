;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA_READ_DATA_FIT
; PURPOSE:
;		This procedure queries and reads a FITS file. A 'diva_image'
;		structure is populated with appropriate values.
; INPUTS:
;		tmp_diva_image_file - a 'diva_image' structure
; OUTPUTS:
;		tmp_diva_image_file - populated with appropriate values
; PROCEDURES USED:
;		FUNCTIONS:	DIVA_IMAGE_INIT (diva_image.pro)
;					READFITS, SXPAR
; AUTHOR:
;		John A. Rainnie - RAL (j.a.rainnie@rl.ac.uk)
;-
;------------------------------------------------------------------------------;
pro diva_read_data_fit,tmp_diva_image_file,index,diva_n_images
;------------------------------------------------------------------------------;
file_info	= tmp_diva_image_file.file_info

;call 'readfits' FUNCTION to return data and header
image   	= READFITS(file_info.f,header,exten_no=index,/SILENT)

ext_type 	= SXPAR(header,'XTENSION')
;IMAGE type data if (NAXIS ne 0) OR (XTEnsion eq 'Image')
CASE SIZE(ext_type,/type) OF
	2	:	;print,ext_type
	7	:	CASE strcompress(ext_type,/remove_all) OF
				'TABLE' 	:	print,ext_type
				'BINTABLE'	:	print,ext_type
				'IMAGE' 	:	print,ext_type
				ELSE		:	stop
			ENDCASE
	ELSE:
ENDCASE

bitpix  	= SXPAR(header,'BITPIX')
CASE bitpix OF 
	8	:	n_byt 	= 1
    16	:	n_byt 	= 2
    -32 :	n_byt 	= 2
	32	:	n_byt 	= 2
    ELSE:   stop
ENDCASE


naxis 		= SXPAR(header,'NAXIS')
CASE naxis OF
    2	:	n_dim 	= 1 	;ie, just xsize and ysize --> greyscale
    3	:	n_dim	= 3
	ELSE:   stop
ENDCASE

tmp_diva_image_file.xsize			= SXPAR(header,'NAXIS1')
tmp_diva_image_file.ysize			= SXPAR(header,'NAXIS2')
tmp_diva_image_file.n_dim			= n_dim
tmp_diva_image_file.n_byt			= n_byt
tmp_diva_image_file.diva_n_images	= diva_n_images

;call 'diva_image_init' function to populate 'diva_image' structure
diva_image = diva_image_init(tmp_diva_image_file,image)
;and update 'tmp_diva_image_file' structure
tmp_diva_image_file = diva_image


END ; of 'diva_read_data_fit'--------------------------------------------------;
;------------------------------------------------------------------------------;
