;------------------------------------------------------------------------------;
PRO diva_hdr_edit_ev, event
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
WIDGET_CONTROL, event.id, GET_UVALUE = eventval		
WIDGET_CONTROL, event.top,GET_UVALUE = diva_hdr_edit_info

CASE eventval OF

"EXIT": 		$
BEGIN
	(*diva_hdr_edit_info.new_header) = strarr(1)
	WIDGET_CONTROL, event.top, /DESTROY	
		
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"PRINT":		$
BEGIN
	header = diva_hdr_edit_info.header

	openw,1,'header.txt';,/delete
		for i=0, n_elements(header)-1 do begin
			printf,1, format='(%"%s\r")', header[i]
		endfor
	close,1
	spawn,'lpr header.txt'
	spawn,'rm header.txt'
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"ADD_COMMENT":	$
BEGIN
	widget_control,diva_hdr_edit_info.diva_mod_text,get_value = s_comment
	widget_control,diva_hdr_edit_info.diva_mod_text,set_value = ''
	new_header = (*diva_hdr_edit_info.new_header)
	
	sxaddpar,new_header,'COMMENT',s_comment[0],before='HISTORY'
	
	widget_control,diva_hdr_edit_info.diva_fit_h_text,set_value = new_header
	
	(*diva_hdr_edit_info.new_header) = new_header
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"REVERT":		$
BEGIN
	(*diva_hdr_edit_info.new_header) = (*diva_hdr_edit_info.orig_header)
	widget_control,diva_hdr_edit_info.diva_fit_h_text,set_value = 			$
										(*diva_hdr_edit_info.new_header)
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"ACCEPT":		$
BEGIN
	WIDGET_CONTROL, event.top, /DESTROY	
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

ELSE: MESSAGE, "Event User Value Not Found"		
ENDCASE

END ;end of "diva_hdr_edit" event handler--------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
FUNCTION diva_hdr_edit,header,tlb
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;

;IF(XRegistered("diva_hdr_edit") NE 0) THEN RETURN,''		

;-------------------create the main base---------------------------------------;
diva_hdr_editbase = WIDGET_BASE(TITLE = 'FITS HEADER EDITOR',   $
    	    	    	group_leader=tlb,/floating,/col,/MODAL)
;-------------------header - text widget---------------------------------------;
diva_fit_h_text = widget_text(diva_hdr_editbase,xsize=90,ysize=30,    $
    	    	value=header,/scroll)
;-------------------modification base------------------------------------------;
diva_mod_base=widget_base(diva_hdr_editbase,/row,/frame,/align_center)
diva_mod_label=widget_label(diva_mod_base,value='Add comment:')
diva_mod_text=widget_text(diva_mod_base,value='',/editable,xsize=71,		$
		uvalue='ADD_COMMENT')
;-------------------button base------------------------------------------------;
buttonbase=widget_base(diva_hdr_editbase,/row,/frame,/grid_layout)
;-------------------accept button----------------------------------------------;
acceptbutton=widget_button(buttonbase, value='Accept',uvalue='ACCEPT')
;-------------------revert to original button----------------------------------;
revertbutton=widget_button(buttonbase, value='Revert to Original',	$
		uvalue='REVERT')
;-------------------print button-----------------------------------------------;
printbutton=widget_button(buttonbase, value='Print',uvalue='PRINT')
;-------------------quit button------------------------------------------------;
quitbutton=widget_button(buttonbase, value='Abort',uvalue='EXIT')
;------------------------------------------------------------------------------;

;-------------------realise the widget-----------------------------------------;
WIDGET_CONTROL, diva_hdr_editbase, /REALIZE		

diva_hdr_edit_info =  {	orig_header 		: PTR_NEW(header),				$
						new_header			: PTR_NEW(header),				$
						diva_fit_h_text 	: diva_fit_h_text,				$
						diva_mod_text		: diva_mod_text 				$
					  }
				
widget_control,diva_hdr_editbase,set_uvalue = diva_hdr_edit_info

XManager, "diva_hdr_edit", diva_hdr_editbase, $		
		EVENT_HANDLER = "diva_hdr_edit_ev", $
		GROUP_LEADER = tlb,cleanup='diva_hdr_edit_cleanup'		

return,diva_hdr_edit_info.new_header
						
END ;end of "diva_hdr_edit" main routine-------------------------------------;
;----------------------------------------------------------------------------;
;----------------------------------------------------------------------------;
;----------------------------------------------------------------------------;
PRO diva_hdr_edit_cleanup,diva_hdr_editbase
;----------------------------------------------------------------------------;
widget_control,diva_hdr_editbase,GET_UVALUE = diva_hdr_edit_info
Ptr_FRee,diva_hdr_edit_info.orig_header
;Ptr_FRee,diva_hdr_edit_info.new_header
END
;----------------------------------------------------------------------------;
;----------------------------------------------------------------------------;
