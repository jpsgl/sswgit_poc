;+
; NAME:
;		D
; PURPOSE:
;		batch file to compile DIVA procedures and run "diva.pro"
; CATEGORY:
;		batch file
; CALLING SEQUENCE:
;		@d
; INPUTS:
; OPTIONAL INPUTS:
; KEYWORD PARAMETERS:
; OUTPUTS:
; OPTIONAL OUTPUTS:
; COMMON BLOCKS:
;		none
; SIDE EFFECTS:
; RESTRICTIONS:
; PROCEDURE:
;		obvious
; NOTES:
;		if the o/s is linux, then the path is extended to include
;		the './diva_fits' directory
; MODIFICATION HISTORY:
;		Original Version written in 2003 by John A. Rainnie at RAL
;-
retall
if (!d.name eq 'X') then !path = './diva_fits:' + !path
.r diva_image
.r diva_read_data_img
.r diva_read_data_jpg
.r diva_read_data_bmp
.r diva_file
.r diva_conc
.r diva_math
.r diva
.r pdiva_draw_init
.r diva_error
.r diva_hdr_edit
.r diva_defaults
.r diva_make_webpage
.r diva_export
.r diva_print
.r diva_fit_header
.r diva_ptc_plot
.r diva_ptc
.r diva_roi_profile
.r diva_make_fits_file
.r diva_circle
.r diva_line
.r diva_zoom
.r diva_hist
.r diva_stats
.r diva_surf
.r diva_roi
.r diva_draw
.r diva_report
.r diva_draw_all
.r diva_read_data_fit
.r diva_mult_image
.r diva_vers
.r diva_result
diva
