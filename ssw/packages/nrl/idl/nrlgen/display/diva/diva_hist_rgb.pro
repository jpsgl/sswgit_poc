;------------------------------------------------------------------------------;
PRO diva_hist_rgb_ev, event
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
WIDGET_CONTROL, event.id, GET_UVALUE = eventval	
widget_control,event.top,get_uvalue=diva_hist_info,/no_copy	

CASE eventval OF

"EXIT":     	BEGIN
    	    	    WIDGET_CONTROL, event.top, /DESTROY	
		    return	
	    	END
"PLOTLIN":   	BEGIN
    	    	    if (event.select eq 1) then begin
		    	(*diva_hist_info).linplot_on = 1
    	    	    	;call 'diva_hist_plot' to plot histogram
    	    	    	diva_hist_rgb_plot,diva_hist_info
		    end
		END
"PLOTLOG":   	BEGIN
    	    	    if (event.select eq 1) then begin
		    	(*diva_hist_info).linplot_on = 0
    	    	    	;call 'diva_hist_plot' to plot histogram
    	    	    	diva_hist_rgb_plot,diva_hist_info
		    end
		END
"EBINON":   	BEGIN
    	    	    if (event.select eq 1) then begin
		    	(*diva_hist_info).ebin_on = 1
    	    	    	;call 'diva_hist_plot' to plot histogram
    	    	    	diva_hist_rgb_plot,diva_hist_info
		    end
		END
"EBINOFF":   	BEGIN
    	    	    if (event.select eq 1) then begin
		    	(*diva_hist_info).ebin_on = 0
    	    	    	;call 'diva_hist_plot' to plot histogram
    	    	    	diva_hist_rgb_plot,diva_hist_info
		    end
		END
"SMOOTH":   	BEGIN
    	    	    new = (*diva_hist_info).smooth_list_list[event.index]
		    old = (*diva_hist_info).smooth_list_value
		    
		    if (old ne new) then begin
		    	(*diva_hist_info).smooth_list_value = new
    	    	    	;call 'diva_hist_plot' to plot histogram
    	    	    	diva_hist_rgb_plot,diva_hist_info
		    end
    	    	END
"FUNCTION":   	BEGIN
    	    	    new = (*diva_hist_info).func_list_list[event.index]
		    old = (*diva_hist_info).func_list_value
		    
		    if (old ne new) then begin
		    	print,new
		    	(*diva_hist_info).func_list_value = new
		    end
    	    	END
"HISTDRAW": 	BEGIN
		    diva_hist_index = (*diva_hist_info).diva_hist_index
		    widget_control,(*diva_hist_info).diva_hist_draw,	    $
		    	    	    	    	    get_value = hist_draw
		    wset,hist_draw
		    hist_motion_evs_on = (*diva_hist_info).hist_motion_evs_on
		    
		    possibleEventTypes = [ 'DOWN', 'UP', 'MOTION', 'SCROLL']
		    thisEvent = possibleEventTypes(event.type)
		    
		    event.x = 0 > event.x < (640 - 1)
		    event.y = 0 > event.y < (400 - 1)
		    coords = Convert_Coord(event.x, event.y, /Device, /To_Data)
		    xh = !x.crange(0) > coords(0) < !x.crange(1)
		    yh = !y.crange(0) > coords(1) < !y.crange(1)
		    widget_control,(*diva_hist_info).infoxy_id, 	    $
			      set_value=strcompress(round(xh),/rem) +      $
				  ',' + strcompress(round(yh),/rem)
		    
		    
		    if (thisevent eq 'DOWN') then begin
		    	(*diva_hist_info).hist_motion_evs_on = 1
			(*diva_hist_info).dhcoords[0] = xh
			(*diva_hist_info).dhcoords[1] = yh
		    endif ;of a DOWN event
		    
		    if (thisevent eq 'MOTION') then begin
			xhd = xh & yhd = yh
			Device,Copy=[0,0,640,400,0,0,diva_hist_index]
			if (hist_motion_evs_on eq 0) then begin
			    plots,[xhd,xhd],[!y.crange(0),!y.crange(1)]
			    plots,[!x.crange(0),!x.crange(1)],[yhd,yhd]
			endif else begin
    	    	    	    xhs = (*diva_hist_info).dhcoords[0]    
			    yhs = (*diva_hist_info).dhcoords[1]
			    oplot,[xhs,xhs],[!y.crange(0),!y.crange(1)]
			    oplot,[xhd,xhd],[!y.crange(0),!y.crange(1)]
			    arrow,xhs,yhs,xhd,yhs,/data,hsize=10,/solid,thick=2
			endelse
		    endif ;of MOTION events
		    if (thisevent eq 'UP') then begin
		    	(*diva_hist_info).hist_motion_evs_on = 0
			(*diva_hist_info).dhcoords[2] = xh
			(*diva_hist_info).dhcoords[3] = yh
			
			xhs = (*diva_hist_info).dhcoords[0]
			;reset condition
			if (abs(xhs-xh) lt 5) then begin
			    minmaxbins = (*diva_hist_info).minmaxbins
			    (*diva_hist_info).dhcoords[0] = 0
			    (*diva_hist_info).dhcoords[2] = minmaxbins[1]
			    minmaxbins = (*diva_hist_info).minmaxbins
;			    if ((*diva_hist_info).linplot_on eq 1) then	    $
;			    	!y.crange = [0,max((*diva_hist_info).bins)] $
;			    	ELSE !y.crange =[.1,max((*diva_hist_info).bins)]
;				;!y.crange = [0,minmaxbins[1]] $
;				;ELSE !y.crange =[.1,minmaxbins[1]]
			    ie = [0,0,0,0,0,0]
			    ;call 'diva_hist_sensitize' pro 
		    	    diva_hist_rgb_sensitize,(*diva_hist_info).go_buttons,ie
			endif else begin
			    ie = [1,1,1,1,1,1]
		    	    ;call 'diva_hist_sensitize' pro 
			    diva_hist_rgb_sensitize,(*diva_hist_info).go_buttons,ie
			endelse
			x_0 = (*diva_hist_info).dhcoords[0]
			x_1 = (*diva_hist_info).dhcoords[2]
			x = [x_0,x_1]
			x = x(sort(x))
			(*diva_hist_info).dhcoords[0] = x[0]
			(*diva_hist_info).dhcoords[2] = x[1]
			
			diva_hist_rgb_plot,diva_hist_info
		    endif ;of UP event
    	    	END
"GO_ALL_L": 	BEGIN
    	    	    x_0 = (*diva_hist_info).dhcoords[0]
		    x_1 = (*diva_hist_info).dhcoords[2]
		    dx = abs(x_0 - x_1)/2
		    (*diva_hist_info).dhcoords[0] = 0
		    (*diva_hist_info).dhcoords[2] = dx*2
    	    	    
		    ie = [0,0,1,1,1,1]
		    ;call 'diva_hist_sensitize' pro 
		    diva_hist_rgb_sensitize,(*diva_hist_info).go_buttons,ie
		    ;call 'diva_hist_plot' to plot histogram
    	    	    diva_hist_rgb_plot,diva_hist_info
		END
"GO_ALL_R": 	BEGIN
    	    	    x_0 = (*diva_hist_info).dhcoords[0]
		    x_1 = (*diva_hist_info).dhcoords[2]
		    dx = abs(x_0 - x_1)/2
		    maxv = max((*diva_hist_info).bins)
		    (*diva_hist_info).dhcoords[0] = maxv - (dx*2)
		    (*diva_hist_info).dhcoords[2] = maxv

    	    	    ie = [1,1,0,0,1,1]
		    ;call 'diva_hist_sensitize' pro 
		    diva_hist_rgb_sensitize,(*diva_hist_info).go_buttons,ie
    	    	    ;call 'diva_hist_plot' to plot histogram
    	    	    diva_hist_rgb_plot,diva_hist_info
    	    	END
"GO_L":     	 	BEGIN
    	    	    x_0 = (*diva_hist_info).dhcoords[0]
		    x_1 = (*diva_hist_info).dhcoords[2]
		    dx = abs(x_0 - x_1)/4
		    if (x_0 - dx gt 0) then begin
		    	(*diva_hist_info).dhcoords[0] = x_0 - dx
		    	(*diva_hist_info).dhcoords[2] = x_1 - dx
			ie = [1,1,1,1,1,1]
		    endif else begin
		    	(*diva_hist_info).dhcoords[0] = 0
			(*diva_hist_info).dhcoords[2] = dx*2
			ie = [0,0,1,1,1,1]
		    endelse
		    ;call 'diva_hist_sensitize' pro 
		    diva_hist_rgb_sensitize,(*diva_hist_info).go_buttons,ie
    	    	    ;call 'diva_hist_plot' to plot histogram
    	    	    diva_hist_rgb_plot,diva_hist_info
		END
"GO_R":     	BEGIN
    	    	    x_0 = (*diva_hist_info).dhcoords[0]
		    x_1 = (*diva_hist_info).dhcoords[2]
		    dx = abs(x_0 - x_1)/4
		    maxv = (*diva_hist_info).minmaxbins[1]
		    if (x_1 + dx lt maxv) then begin
		    	(*diva_hist_info).dhcoords[0] = x_0 + dx
		    	(*diva_hist_info).dhcoords[2] = x_1 + dx
			ie = [1,1,1,1,1,1]
		    endif else begin
		    	(*diva_hist_info).dhcoords[0] = maxv - (dx*2)
			(*diva_hist_info).dhcoords[2] = maxv
			ie = [1,1,0,0,1,1]
		    endelse
		    ;call 'diva_hist_sensitize' pro 
		    diva_hist_rgb_sensitize,(*diva_hist_info).go_buttons,ie
    	    	    ;call 'diva_hist_plot' to plot histogram
    	    	    diva_hist_rgb_plot,diva_hist_info
    	    	END
"R_DBLE":   	BEGIN
		    maxv = (*diva_hist_info).minmaxbins[1]
		    x_0 = (*diva_hist_info).dhcoords[0]
		    x_1 = (*diva_hist_info).dhcoords[2]
    	    	    
		    if (x_0 gt 0) OR (x_1 lt maxv) then begin
		    	dx = abs(x_0 - x_1)
			newx = [x_0 - (dx/2),x_1 + (dx/2)]
			
			if (newx[0] lt 1) then begin
			    newx[0] = 0
			    ie = [0,0,1,1,1,1]
			    ;call 'diva_hist_sensitize' pro 
			    diva_hist_rgb_sensitize,(*diva_hist_info).go_buttons,ie
			    ;stop
			endif
			if (newx[1] ge maxv) then begin
			    newx[1] = maxv
			    ie = [1,1,0,0,1,1]
			    ;call 'diva_hist_sensitize' pro 
			    diva_hist_rgb_sensitize,(*diva_hist_info).go_buttons,ie
			    ;stop
    	    	    	endif
			if (newx[0] lt 1) AND (newx[1] ge maxv) then begin
			    ie = [0,0,0,0,0,0]
			    ;call 'diva_hist_sensitize' pro 
			    diva_hist_rgb_sensitize,(*diva_hist_info).go_buttons,ie
			    ;stop
			endif
			if (newx[1]-newx[0] le 6) then begin
			    ie = [1,1,1,1,1,0]
			    ;call 'diva_hist_sensitize' pro 
			    diva_hist_rgb_sensitize,(*diva_hist_info).go_buttons,ie
			    ;stop
			endif
			
			ie = [1,1,1,1,1,1]
			;call 'diva_hist_sensitize' pro 
			diva_hist_rgb_sensitize,(*diva_hist_info).go_buttons,ie
			
    	    		catch,wot & if (wot ne 0) then stop
			(*diva_hist_info).dhcoords[0] = newx[0]
			(*diva_hist_info).dhcoords[2] = newx[1]

    	    		;call 'diva_hist_plot' to plot histogram
    	    	    	diva_hist_rgb_plot,diva_hist_info
		    endif else begin
		    	ie = [0,0,0,0,0,0]
			;call 'diva_hist_sensitize' pro 
			diva_hist_rgb_sensitize,(*diva_hist_info).go_buttons,ie
    	    		;call 'diva_hist_plot' to plot histogram
    	    	    	diva_hist_rgb_plot,diva_hist_info
		    endelse
			
    	    	END
"R_HALF":   	BEGIN
		    maxv = (*diva_hist_info).minmaxbins[1]
		    x_0 = (*diva_hist_info).dhcoords[0]
		    x_1 = (*diva_hist_info).dhcoords[2]
		    dx = (x_1 - x_0)
		    newx = [x_0 + (dx/4),x_1 - (dx/4)]
		    if (newx[1]-newx[0] ge 5) then begin 
			(*diva_hist_info).dhcoords[0] = newx[0]
		    	(*diva_hist_info).dhcoords[2] = newx[1]
			;call 'diva_hist_plot' to plot histogram
			diva_hist_rgb_plot,diva_hist_info
			ie = [1,1,1,1,1,1]
			;call 'diva_hist_sensitize' pro 
			diva_hist_rgb_sensitize,(*diva_hist_info).go_buttons,ie
		    endif else begin
		    	ie = [1,1,1,1,1,0]
		    	;call 'diva_hist_sensitize' pro 
			diva_hist_rgb_sensitize,(*diva_hist_info).go_buttons,ie
		    endelse
		END
"COL_R":    	BEGIN
    	    	    CASE event.select OF
		    	1:  (*diva_hist_info).rgb_on[0] = 1
			0:  (*diva_hist_info).rgb_on[0] = 0
		    ENDCASE
		    widget_control,(*diva_hist_info).col_butts[0],sensitive=1
		    widget_control,(*diva_hist_info).col_butts[1],sensitive=1
		    widget_control,(*diva_hist_info).col_butts[2],sensitive=1
		    ;call 'diva_hist_plot' to plot histogram
    	    	    diva_hist_rgb_plot,diva_hist_info
    	    	END				
"COL_G":    	BEGIN
    	    	    CASE event.select OF
		    	1:  (*diva_hist_info).rgb_on[1] = 1
			0:  (*diva_hist_info).rgb_on[1] = 0
		    ENDCASE
		    widget_control,(*diva_hist_info).col_butts[0],sensitive=1
		    widget_control,(*diva_hist_info).col_butts[1],sensitive=1
		    widget_control,(*diva_hist_info).col_butts[2],sensitive=1
		    ;call 'diva_hist_plot' to plot histogram
    	    	    diva_hist_rgb_plot,diva_hist_info
    	    	END				
"COL_B":    	BEGIN
    	    	    CASE event.select OF
		    	1:  (*diva_hist_info).rgb_on[2] = 1
			0:  (*diva_hist_info).rgb_on[2] = 0
		    ENDCASE
		    widget_control,(*diva_hist_info).col_butts[0],sensitive=1
		    widget_control,(*diva_hist_info).col_butts[1],sensitive=1
		    widget_control,(*diva_hist_info).col_butts[2],sensitive=1
		    ;call 'diva_hist_plot' to plot histogram
    	    	    diva_hist_rgb_plot,diva_hist_info
    	    	END				
ELSE: MESSAGE, "Event User Value Not Found"		
ENDCASE
;help,(*diva_hist_info),/str
widget_control,event.top,set_uvalue=diva_hist_info,/no_copy
END ;end of "diva_hist" event handler------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_hist_rgb, pdiva_draw,GROUP = GROUP
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;

IF(XRegistered("diva_hist") NE 0) THEN RETURN		

;-------------------create the main base---------------------------------------;
diva_histbase = WIDGET_BASE(TITLE = "DIVA IMAGE (RGB) HISTOGRAM",/col)

;------------------------------------------------------------------------------;
;-------------------topbase----------------------------------------------------;
topbase = widget_base(diva_histbase,/row,/frame)

plot_base=widget_base(topbase,/row,/frame)
plot_base_label=widget_label(plot_base,value='Plot:')
plot_base_exc=widget_base(plot_base,/exclusive,/row)
plot_base_lin=widget_button(plot_base_exc,value='lin',uvalue='PLOTLIN')
widget_control,plot_base_lin,set_button=1
plot_base_log=widget_button(plot_base_exc,value='log',uvalue='PLOTLOG')

ebin_base=widget_base(topbase,/row,/frame)
ebin_base_label=widget_label(ebin_base,value='Empty bins:')
ebin_base_exc=widget_base(ebin_base,/exclusive,/row)
ebin_base_off=widget_button(ebin_base_exc,value='off',uvalue='EBINOFF')
widget_control,ebin_base_off,set_button=1
ebin_base_on=widget_button(ebin_base_exc,value='on',uvalue='EBINON')

smooth_base=widget_base(topbase,/row,/frame)
smooth_list_list=['none','3','5','7']
smooth_list=widget_droplist(smooth_base,title='Smoothing:',uvalue='SMOOTH',  $
    	value=smooth_list_list)

func_base=widget_base(topbase,/row,/frame)
func_list_list=['none','threshold','pixel']
func_list=widget_droplist(func_base,title='Function:',uvalue='FUNCTION',    $
    	value=func_list_list)

;-------------------color base-------------------------------------------------;
colbase = widget_base(diva_histbase,/row,/frame)
colbase_rgb = widget_base(colbase,/row,/frame,/nonexclusive)

col_r_butt = widget_button(colbase_rgb,value=' R ',uvalue='COL_R')
col_g_butt = widget_button(colbase_rgb,value=' G ',uvalue='COL_G')
col_b_butt = widget_button(colbase_rgb,value=' B ',uvalue='COL_B')
widget_control,col_r_butt,set_button = 1
widget_control,col_g_butt,set_button = 1
widget_control,col_b_butt,set_button = 1
col_butts  = [col_r_butt,col_g_butt,col_b_butt]

;-------------------hist draw base---------------------------------------------;
diva_hist_draw = widget_draw(diva_histbase,xsize=640,ysize=400,frame=5,     $
    	    /motion_events,Button_Events=1,/viewport_events,uvalue='HISTDRAW')

window,/free,/pixmap,xsize=640,ysize=400
diva_hist_index=!d.window

;-------------------bottom base------------------------------------------------;
buttbase = widget_base(diva_histbase,/row,/frame)

infobase = widget_base(buttbase,/row,/frame)
info_label = widget_label(infobase,value='coords:')
infoxy_id = widget_text(infobase,value='0,0',xsize=12) 

minmaxbase=widget_base(buttbase,/row,/frame)
minmax_label = widget_label(minmaxbase,value='min/max:')
minmax_id = widget_text(minmaxbase,value='0:0,0:0,0:0',xsize=30) 

go_base = widget_base(buttbase,/row,/frame,/grid_layout)
go_all_left=widget_button(go_base,value=' << ',uvalue='GO_ALL_L',sensitive=0)
go_left=widget_button(go_base,value='<',uvalue='GO_L',sensitive=0)
go_right=widget_button(go_base,value='>',uvalue='GO_R',sensitive=0)
go_all_right=widget_button(go_base,value=' >> ',uvalue='GO_ALL_R',sensitive=0)

range_base = widget_base(buttbase,/row,/frame,/grid_layout)
range_dble = widget_button(range_base,value=' x2 ',uvalue='R_DBLE',sensitive=0)
range_half = widget_button(range_base,value=' /2 ',uvalue='R_HALF',sensitive=0)
go_buttons = [go_all_left,go_left,go_right,go_all_right,range_dble,range_half]
;-------------------quit button------------------------------------------------;
quitbutton=widget_button(diva_histbase, value='Quit',uvalue='EXIT')
;------------------------------------------------------------------------------;

;-------------------realise the widget-----------------------------------------;
WIDGET_CONTROL, diva_histbase, /REALIZE		

i_r=(*(*pdiva_draw).diva_current_image.o_pimage)[*,*,0]
i_g=(*(*pdiva_draw).diva_current_image.o_pimage)[*,*,1]
i_b=(*(*pdiva_draw).diva_current_image.o_pimage)[*,*,2]
minval = min((*(*pdiva_draw).diva_current_image.o_pimage))
maxval = max((*(*pdiva_draw).diva_current_image.o_pimage))
hist_r = [HISTOGRAM(i_r,min=minval,max=maxval),lonarr(10)]
hist_g = [HISTOGRAM(i_g,min=minval,max=maxval),lonarr(10)]
hist_b = [HISTOGRAM(i_b,min=minval,max=maxval),lonarr(10)]
bins   = FINDGEN(N_ELEMENTS(hist_r))
hist = fltarr(N_ELEMENTS(hist_r),3)
hist[*,0] = hist_r
hist[*,1] = hist_g
hist[*,2] = hist_b

minmaxbins = [min(bins),max(bins)]
dhcoords   = [0,0,minmaxbins[1],0]

diva_hist_info = Ptr_New({  pdiva_draw	    	:   pdiva_draw,     	    $
    	    	    	    linplot_on	    	:   1,	    	    	    $
    	    	    	    ebin_on 	    	:   0,	    	    	    $
			    smooth_list_list	:   smooth_list_list,	    $
			    smooth_list_value	:   'none',	    	    $
			    func_list_list    	:   func_list_list,   	    $
			    func_list_value   	:   'none',		    $
			    diva_hist_draw  	:   diva_hist_draw, 	    $
			    diva_hist_index 	:   diva_hist_index, 	    $
			    infoxy_id	    	:   infoxy_id,	    	    $
			    minmax_id	    	:   minmax_id,	    	    $
			    hist    	    	:   hist,   	    	    $
			    bins    	    	:   bins,    	    	    $
			    minmaxbins	    	:   minmaxbins,	    	    $
			    dhcoords  	    	:   dhcoords,   	    $
			    hist_motion_evs_on	:   0,	    	    	    $
			    go_buttons	    	:   go_buttons,	    	    $
			    col_butts	    	:   col_butts,	    	    $
			    rgb_on  	    	:   [1,1,1] 	    	    $
			})
;call 'diva_hist_plot' to plot histogram
diva_hist_rgb_plot,diva_hist_info


widget_control,diva_histbase,set_uvalue=diva_hist_info,/no_copy


XManager, "diva_hist", diva_histbase,EVENT_HANDLER = "diva_hist_rgb_ev",    $
		GROUP_LEADER = GROUP		
						
END ;end of "diva_hist" main routine-------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_hist_rgb_plot,diva_hist_info

x           = [(*diva_hist_info).dhcoords[0],(*diva_hist_info).dhcoords[2]]
bins        = (*diva_hist_info).bins[x[0]:x[1]]
rgb_on      = (*diva_hist_info).rgb_on
col_butts   = (*diva_hist_info).col_butts
i_mean      = (*(*diva_hist_info).pdiva_draw).diva_current_image.i_mean
i_stddev    = (*(*diva_hist_info).pdiva_draw).diva_current_image.i_stddev
hist 	    = (*diva_hist_info).hist
hist 	    = hist[x[0]:x[1],*]
minmaxbins  = (*diva_hist_info).minmaxbins
ebin_on     = (*diva_hist_info).ebin_on
linplot_on  = (*diva_hist_info).linplot_on
smooth_list_value = (*diva_hist_info).smooth_list_value
image_ct    = (*(*diva_hist_info).pdiva_draw).image_ct
plot_ct     = (*(*diva_hist_info).pdiva_draw).plot_ct 


if (x[0] lt 0) then x[0] = 0
if (x[1] gt minmaxbins[1]) then x[1] = minmaxbins[1]
if (x[0] eq x[1]) then x[1] = x[0] + 10
if (x[1]-x[0] lt 1) then x[1] = x[0] + 1
;reset condition
if (x[0] eq 0) AND (x[1] eq 0) then x[1] = max(bins)

;LOG bit!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
if (linplot_on eq 0) and (min(hist) eq 0) then begin
    hist=float(hist)
    hist(where(hist eq 0))=.1
endif
;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
;smoothing bit !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
if (smooth_list_value ne 'none') then begin
    hist_r = hist[*,0]
    hist_g = hist[*,1]
    hist_b = hist[*,2]
    CASE smooth_list_value OF
    	'3':	BEGIN
	    	    hist[*,0] = smooth(hist_r,3) 
	    	    hist[*,1] = smooth(hist_g,3) 
	    	    hist[*,2] = smooth(hist_b,3) 
    	    	END
    	'5':	BEGIN
	    	    hist[*,0] = smooth(hist_r,5) 
	    	    hist[*,1] = smooth(hist_g,5) 
	    	    hist[*,2] = smooth(hist_b,5) 
    	    	END
    	'7':	BEGIN
	    	    hist[*,0] = smooth(hist_r,7) 
	    	    hist[*,1] = smooth(hist_g,7) 
	    	    hist[*,2] = smooth(hist_b,7) 
    	    	END
    ENDCASE
endif	    
;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
;empty bins bit!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
;copy 'hist' to a new array, and set all elements to zero
if (ebin_on eq 1) then begin
    empty_bins_hist=hist
    empty_bins_hist(*)=!y.crange[0]
    ;need to check whether empty bin values were set to 0.1 (during) log plot
    if (min(hist) lt 1) then begin
	;find nonzero elements of 'hist', and give them a value of unity    
	nonzero_elements=where(hist eq min(hist))
	empty_bins_hist[nonzero_elements]=!y.crange[1]
    endif
end

tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2];load plot colour table

wset,(*diva_hist_info).diva_hist_index
;call 'diva_hist_do_plot' to plot data
diva_hist_rgb_do_plot,hist,bins,empty_bins_hist,linplot_on,ebin_on,x,       $
    	i_mean,i_stddev,minmaxbins,rgb_on



widget_control,(*diva_hist_info).diva_hist_draw,get_value = hist_draw
wset, hist_draw
;call 'diva_hist_do_plot' to plot data
diva_hist_rgb_do_plot,hist,bins,empty_bins_hist,linplot_on,ebin_on,x,       $
    	i_mean,i_stddev,minmaxbins,rgb_on
oplot,[minmaxbins[1]-9,   minmaxbins[1]-9],[!y.crange[0],!y.crange[1]],color=7

;revert back to image colour table
tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2]

if  (rgb_on[0] eq 1) AND (rgb_on[1] eq 0) AND (rgb_on[2] eq 0) then 	    $
    widget_control,col_butts[0],sensitive = 0
if  (rgb_on[0] eq 0) AND (rgb_on[1] eq 1) AND (rgb_on[2] eq 0) then 	    $
    widget_control,col_butts[1],sensitive = 0
if  (rgb_on[0] eq 0) AND (rgb_on[1] eq 0) AND (rgb_on[2] eq 1) then 	    $
    widget_control,col_butts[2],sensitive = 0

;report min and max values of new plot range
if (min(hist) eq .1)  THEN  minmax = '0,' + 	    	    	    	    $
    	strcompress(round(max(hist)),/rem) $
ELSE minmax = 	    strcompress(min(round(hist[*,0])),/rem) + ':'  + 	    $
    	    	    strcompress(max(round(hist[*,0])),/rem) +','   + 	    $
		    strcompress(min(round(hist[*,1])),/rem) + ':'  + 	    $
    	    	    strcompress(max(round(hist[*,1])),/rem) +','   + 	    $
		    strcompress(min(round(hist[*,2])),/rem) + ':'  + 	    $
    	    	    strcompress(max(round(hist[*,2])),/rem) 
widget_control,(*diva_hist_info).minmax_id,set_value = minmax


end; of 'diva_hist_plot' pro---------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_hist_rgb_do_plot,hist,bins,empty_bins_hist,linplot_on,ebin_on,x,   $
    	i_mean,i_stddev,minmaxbins,rgb_on
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;

;only 1 channel is being displayed
if (rgb_on[0] eq 1) AND (rgb_on[1] eq 0) AND (rgb_on[2] eq 0) then begin
    ymin = min(hist[*,0]) & ymax = max(hist[*,0])   
end
if (rgb_on[0] eq 0) AND (rgb_on[1] eq 1) AND (rgb_on[2] eq 0) then begin
    ymin = min(hist[*,1]) & ymax = max(hist[*,1])   
end
if (rgb_on[0] eq 0) AND (rgb_on[1] eq 0) AND (rgb_on[2] eq 1) then begin
    ymin = min(hist[*,2]) & ymax = max(hist[*,2])   
end
; if 2 channels are being displayed
if (rgb_on[0] eq 1) AND (rgb_on[1] eq 1) AND (rgb_on[2] eq 0) then begin
    ymin_r = min(hist[*,0]) & ymax_r = max(hist[*,0])   
    ymin_g = min(hist[*,1]) & ymax_g = max(hist[*,1])
    ymin_rg = [ymin_r,ymin_g]  & ymax_rg = [ymax_r,ymax_g]
    ymin = min(ymin_rg)  &  ymax = max(ymax_rg)
end
if (rgb_on[0] eq 1) AND (rgb_on[1] eq 0) AND (rgb_on[2] eq 1) then begin
    ymin_r = min(hist[*,0]) & ymax_r = max(hist[*,0])   
    ymin_b = min(hist[*,2]) & ymax_b = max(hist[*,2])   
    ymin_rb = [ymin_r,ymin_b]  & ymax_rb = [ymax_r,ymax_b]
    ymin = min(ymin_rb)  &  ymax = max(ymax_rb)
end
if (rgb_on[0] eq 0) AND (rgb_on[1] eq 1) AND (rgb_on[2] eq 1) then begin
    ymin_g = min(hist[*,1]) & ymax_g = max(hist[*,1])   
    ymin_b = min(hist[*,2]) & ymax_b = max(hist[*,2])
    ymin_gb = [ymin_g,ymin_b]  & ymax_gb = [ymax_g,ymax_b]
    ymin = min(ymin_gb)  &  ymax = max(ymax_gb)
end
;all 3 channels are being displayed
if (rgb_on[0] eq 1) AND (rgb_on[1] eq 1) AND (rgb_on[2] eq 1) then begin
    ymin = min(hist[*,*]) & ymax = max(hist[*,*])   
end


CASE linplot_on OF
    1:	BEGIN
    	    plot,bins,hist,xtitle='Pixel value',yr=[ymin,ymax],   	    $
    	    	charsize=1.3,ytit='Frequency',xr=[x[0],x[1]],/nodata
	END
    0:	BEGIN
    	    plot_io,bins,hist,xtitle='Pixel value',yr=[ymin,ymax],   	    $
	    	charsize=1.3,ytit='Frequency',xr=[x[0],x[1]],/nodata
	    !y.crange(0)=10^!y.crange(0) & !y.crange(1)=10^!y.crange(1)
	END
ENDCASE    

if (rgb_on[0] eq 1) then begin
    oplot,bins,hist[*,0],psym=10,color=1,thick=2
    oplot,[i_mean[0],i_mean[0]],[!y.crange[0],!y.crange[1]],thick=2,color=1
    oplot,[i_mean[0]+i_stddev[0],i_mean[0]+i_stddev[0]],    	    	    $
    	    	    	    	[!y.crange[0],!y.crange[1]],lines=2,color=1
    oplot,[i_mean[0]-i_stddev[0],i_mean[0]-i_stddev[0]],    	    	    $
    	    	    	    	[!y.crange[0],!y.crange[1]],lines=2,color=1
    if (ebin_on eq 1) then oplot,bins,empty_bins_hist[*,0],psym=10,color=1
end
if (rgb_on[1] eq 1) then begin
    oplot,bins,hist[*,1],psym=10,color=2,thick=2
    oplot,[i_mean[1],i_mean[1]],[!y.crange[0],!y.crange[1]],thick=2,color=2
    oplot,[i_mean[1]+i_stddev[1],i_mean[1]+i_stddev[1]],    	    	    $
    	    	    	    	[!y.crange[0],!y.crange[1]],lines=2,color=2
    oplot,[i_mean[1]-i_stddev[1],i_mean[1]-i_stddev[1]],    	    	    $
    	    	    	    	[!y.crange[0],!y.crange[1]],lines=2,color=2
    if (ebin_on eq 1) then oplot,bins,empty_bins_hist[*,1],psym=10,color=2
end
if (rgb_on[2] eq 1) then begin
    oplot,bins,hist[*,2],psym=10,color=3,thick=2
    oplot,[i_mean[2],i_mean[2]],[!y.crange[0],!y.crange[1]],thick=2,color=3
    oplot,[i_mean[2]+i_stddev[2],i_mean[2]+i_stddev[2]],    	    	    $
    	    	    	    	[!y.crange[0],!y.crange[1]],lines=2,color=3
    oplot,[i_mean[2]-i_stddev[2],i_mean[2]-i_stddev[2]],    	    	    $
    	    	    	    	[!y.crange[0],!y.crange[1]],lines=2,color=3
    if (ebin_on eq 1) then oplot,bins,empty_bins_hist[*,2],psym=10,color=3
end

oplot,[minmaxbins[1]-9,   minmaxbins[1]-9],[!y.crange[0],!y.crange[1]],     $
    	thick=9,color=7
oplot,[minmaxbins[1]-7,   minmaxbins[1]-7],[!y.crange[0],!y.crange[1]],     $
    	thick=9,color=7
oplot,[minmaxbins[1]-5,   minmaxbins[1]-5],[!y.crange[0],!y.crange[1]],     $
    	thick=9,color=7
oplot,[minmaxbins[1]-3,   minmaxbins[1]-3],[!y.crange[0],!y.crange[1]],     $
    	thick=9,color=7
oplot,[minmaxbins[1]-1,   minmaxbins[1]-1],[!y.crange[0],!y.crange[1]],     $
    	thick=9,color=7

				    				    
end
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_hist_rgb_sensitize,go_buttons,ie
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
widget_control,go_buttons[0],sensitive=ie[0]
widget_control,go_buttons[1],sensitive=ie[1]
widget_control,go_buttons[2],sensitive=ie[2]
widget_control,go_buttons[3],sensitive=ie[3]
widget_control,go_buttons[4],sensitive=ie[4]
widget_control,go_buttons[5],sensitive=ie[5]

end
