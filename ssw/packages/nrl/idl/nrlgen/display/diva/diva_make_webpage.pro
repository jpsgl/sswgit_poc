pro diva_make_webpage

available = [	'diva_defaults.pro',				$
				'd.pro',							$
				'diva.pro',							$
				'diva_file.pro',					$
				'diva_image.pro',					$
				'diva_make_fits_file.pro',			$
				'diva_draw.pro',					$
				'diva_read_data_jpg.pro',			$
				'diva_read_data_bmp.pro',			$
				'pdiva_draw_init.pro',				$
				'diva_read_data_img.pro',			$
				'diva_line.pro' 					$
			]

CASE !D.name OF
;	'X':	mk_html_help,available,'/home/jar24/idl/diva/diva_doc/' + $
	'X':	mk_html_help,available,'$stereo/dps/idl/diva/diva_doc/' + $
			    'diva_design_idl_headers.html', $
			    TITLE = 'DIVA - Extended IDL Help' 
	ELSE:	
ENDCASE

end
