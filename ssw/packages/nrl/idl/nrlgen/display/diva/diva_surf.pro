;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_surf_zoom_plot,diva_surf_info
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
temp 	    	= (*diva_surf_info)
diva_surf_draw  = temp.diva_surf_draw
ud_slider   	= temp.ud_slider
lr_slider   	= temp.lr_slider
surftype_toplot = temp.surftype_toplot
roi_image   	= (*temp.roi_pimage)

if (surftype_toplot eq 'Lego') then begin
    surface,roi_image,az=lr_slider,ax=ud_slider,/lego,charsize=1.9
endif
if (surftype_toplot eq 'Solid') then begin
    shade_surf,roi_image,az=lr_slider,ax=ud_slider,charsize=1.9
endif
if (surftype_toplot eq 'Mesh') then begin
    surface,congrid(roi_image,40,40),az=lr_slider,ax=ud_slider,charsize=1.9
endif

END ;of "diva_surf_zoom_plot" procedure----------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_surf_zoom,diva_surf_info
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
temp 	    	= (*diva_surf_info)
diva_surf_draw  = temp.diva_surf_draw
ud_slider   	= temp.ud_slider
lr_slider   	= temp.lr_slider
surftype_toplot = temp.surftype_toplot
roi_image   	= (*temp.roi_pimage)

wset,(*diva_surf_info).diva_surf_pix

;call "diva_surf_zoom_plot" procedure to plot surface
diva_surf_zoom_plot,diva_surf_info
    
widget_control,diva_surf_draw,get_value=surf_index
wset,surf_index
device, copy=[0,0,500,500,0,0,(*diva_surf_info).diva_surf_pix]	
END;of "diva_surf_zoom" procedure----------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_surf_ev, event;-------------------------------------------------------;
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
WIDGET_CONTROL, event.id, GET_UVALUE = eventval
widget_control,event.top,get_uvalue=diva_surf_info

diva_surf_rslider   =	(*diva_surf_info).diva_surf_rslider
diva_surf_bslider   =	(*diva_surf_info).diva_surf_bslider
diva_surfbase		=	(*diva_surf_info).diva_surfbase
diva_printers		= 	(*diva_surf_info).diva_printers
diva_printers_list	= 	(*diva_surf_info).diva_printers_list
		
CASE eventval OF

"EXIT": 		$
BEGIN
	;destroy widget
	WIDGET_CONTROL, event.top, /DESTROY
	RETURN		
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"UP_DOWN":      $
BEGIN
	widget_control,diva_surf_rslider,get_value=ud_slider
	(*diva_surf_info).ud_slider = ud_slider
	diva_surf_zoom,diva_surf_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"LEFT_RIGHT":	$
BEGIN
	widget_control,diva_surf_bslider,get_value=lr_slider
	(*diva_surf_info).lr_slider=lr_slider
	diva_surf_zoom,diva_surf_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"SURF_PRINT":   $
BEGIN
	os_name 	= !D.name
	file_stuff 	= (*diva_surf_info).file_stuff
	full_name 	= file_stuff[0]
	path_name 	= file_stuff[1]
	file_str  	= file_stuff[2]
	mfile_name  = file_stuff[3]
	
	file_in	= file_str + '_surf.ps'
	
	;call 'diva_print_what' FUNCTION to get filename and 
	result = diva_print_what(path_name,file_in,diva_printers, 	    		$
		    							diva_printers_list,diva_surfbase)
	
	if ((*result).do_print_yes eq 1) then begin
		filename 		= (*result).path+(*result).filename
		what_printer	= diva_printers[(*result).what_printer]
		do_save_yes 	= (*result).do_save_yes
		
		print,'printing to: "'+strcompress(filename)+'"'
	
		set_plot,'ps'
		device,filename=filename,/color
	
		;call 'diva_surf_zoom' to plot data
		diva_surf_zoom_plot,diva_surf_info
	
		device,/close
		CASE os_name OF
			'X':	BEGIN
						set_plot,'x'
						;call "diva_print_linux" pro to do printing!!!
						diva_print_linux,filename,do_save_yes,what_printer
					END
			'WIN':	    set_plot,'win'
		ENDCASE
	ENDIF
	;now free up the 'result' pointer
	Ptr_Free,result
	
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"SURF_DEF": 	$
BEGIN
	(*diva_surf_info).lr_slider = 30
	(*diva_surf_info).ud_slider = 30
	widget_control,diva_surf_rslider,set_value=30
	widget_control,diva_surf_bslider,set_value=30

	diva_surf_zoom,diva_surf_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"SURF_TYPE":    $
BEGIN
	temp    	    = (*diva_surf_info)
	surftype_list   = temp.surftype_list  
	surftype_toplot = temp.surftype_toplot  

	IF (surftype_list[event.index] eq surftype_toplot) THEN 				$
	RETURN ELSE (*diva_surf_info).surftype_toplot = surftype_list(event.index)
		    
	diva_surf_zoom,diva_surf_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"SURF_ZERO":	$
BEGIN
	CASE event.index OF
		0:	(*diva_surf_info).roi_pimage = (*diva_surf_info).roi_i_pimage
		1:	(*diva_surf_info).roi_pimage = (*diva_surf_info).roi_o_pimage
	ENDCASE
	diva_surf_zoom,diva_surf_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

ELSE: MESSAGE, "Event User Value Not Found"		
ENDCASE
END ;end of diva_surf event handling routine task------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_surf,roi_pimage,file_stuff,n_dim,diva_printers,					$
							diva_printers_list,diva_surf_info,GROUP = GROUP
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;

roi_image					= (*roi_pimage)
roi_size					= size(roi_image)
;get a copy of "roi_image" --> "roi_o_image" and remove any zero-values
;this is especially useful to rescale irregular & circular surfaces.
roi_o_image 				= (*roi_pimage)
non_zero_indicies 			= where(roi_image gt 0,complement = zero_indicies)

if (zero_indicies[0] ne -1) then BEGIN
	non_zero_min_value			= min(roi_image[non_zero_indicies])
	roi_o_image[zero_indicies]  = non_zero_min_value		
ENDIF

diva_surf_info=Ptr_New({													$
					roi_pimage  	    	 	:roi_pimage,		    	$
					roi_i_pimage  	    	 	:roi_pimage,				$
					roi_o_pimage  	    	 	:Ptr_New(roi_o_image),		$
    	    	    file_stuff    	    	 	:file_stuff,  		    	$
    	    	    diva_surfbase	         	:0L,	     		    	$
					diva_surf_draw	         	:0L,	     		    	$
					diva_surf_pix	         	:0L,	     		    	$
					surftype_list            	:['Mesh','Lego','Solid'],   $
					surftype_toplot	         	:'Mesh',	    	    	$
					diva_surf_surftype_list  	:0L,  	    	    	    $
					diva_surf_rslider   	 	:0L,   		    			$
					diva_surf_bslider   	 	:0L,   		    			$
					ud_slider   	         	:30,   		    			$
					lr_slider   	         	:30,   		    			$
					n_dim	    	         	:n_dim, 					$
					diva_printers				:diva_printers, 			$
					diva_printers_list			:diva_printers_list 		$
		    			})

;-------------------create the main base---------------------------------------;
diva_surfbase = WIDGET_BASE(TITLE = "DIVA ROI SURFACE",/col,	    	    $
    	    	    	group_leader=group,/floating)
(*diva_surf_info).diva_surfbase=diva_surfbase
;-------------------diva surf button base--------------------------------------;
diva_surf_button_base=widget_base(diva_surfbase,/row,/frame)

diva_surf_surftype_list=widget_droplist(diva_surf_button_base,	    	    $
    	value=(*diva_surf_info).surftype_list,title='Surface type:',	    $
	uvalue='SURF_TYPE')

diva_surf_def_butt=widget_button(diva_surf_button_base,value='Default',     $
    	uvalue='SURF_DEF',xsize=80)

diva_surf_print_butt=widget_button(diva_surf_button_base,value='Print',     $
    	uvalue='SURF_PRINT',xsize=80)

diva_surf_inhibit_list=widget_droplist(diva_surf_button_base,				$
		value=['Off','On'],uvalue='SURF_ZERO',title='Inhibit min-value:')
if (zero_indicies[0] eq -1) then widget_control,diva_surf_inhibit_list,sensitive=0
		
;-------------------draw & rhs base--------------------------------------------;
diva_surf_slider_draw_base=widget_base(diva_surfbase,/row)

diva_surf_lhs_base=widget_base(diva_surf_slider_draw_base,/col)
;-------------------up down slider---------------------------------------------;
diva_surf_rslider = widget_slider(diva_surf_lhs_base,ysize=500,/drag,	    $
    	value=(*diva_surf_info).ud_slider,/vertical,minimum=0,maximum=89,   $
	uvalue='UP_DOWN')
(*diva_surf_info).diva_surf_rslider=diva_surf_rslider
;-------------------rhs base---------------------------------------------------;
diva_surf_rhs_base=widget_base(diva_surf_slider_draw_base,/col)
;-------------------draw window------------------------------------------------;
diva_surf_draw=widget_draw(diva_surf_rhs_base,xsize=500,ysize=500)
(*diva_surf_info).diva_surf_draw=diva_surf_draw
window,/free,/pixmap,xsize=500,ysize=500
diva_surf_pix=!d.window
(*diva_surf_info).diva_surf_pix=diva_surf_pix
;-------------------left right slider------------------------------------------;
diva_surf_bslider = widget_slider(diva_surf_rhs_base,xsize=500,/drag,	    $
    	value=(*diva_surf_info).lr_slider,minimum=-179,maximum=179, 	    $
	uvalue='LEFT_RIGHT')
(*diva_surf_info).diva_surf_bslider=diva_surf_bslider
;-------------------quit button------------------------------------------------;
quitbutton=widget_button(diva_surfbase, value='Quit',uvalue='EXIT')
;-------------------realize the widget-----------------------------------------;
WIDGET_CONTROL, diva_surfbase, /REALIZE		

widget_control,diva_surf_draw,get_value=surf_index
wset,surf_index

if (roi_size(1) gt 40) or (roi_size(2) gt 40) then begin
    roi_image = congrid(roi_image,40,40)
    surface,roi_image,az=lr_slider,ax=ud_slider,thick=1,charsize=1.9
endif else begin
    (*diva_surf_info).surftype_list   = ['Lego','Mesh','Solid']
    (*diva_surf_info).surftype_toplot = 'Lego'
    widget_control,diva_surf_surftype_list,set_value=['Lego','Mesh','Solid']
    surface,roi_image,az=lr_slider,ax=ud_slider,thick=1,/lego,charsize=1.9
end

widget_control,diva_surfbase,set_uvalue= diva_surf_info

XManager, "diva_surf", diva_surfbase, EVENT_HANDLER = "diva_surf_ev",	    $
		GROUP_LEADER = GROUP,cleanup = 'diva_surf_cleanup'
END ;==================== end of diva_surf main routine =======================
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_surf_cleanup,diva_surfbase
;------------------------------------------------------------------------------;
; Purpose - to cleanup diva_surf widget - specifically to free it's pointer!
;------------------------------------------------------------------------------;
widget_control,diva_surfbase,get_uvalue=diva_surf_info
print,'CLEANING UP DIVA SURFACE    **********************'
IF (Ptr_Valid(diva_surf_info) eq 1) then begin
    Ptr_Free,(*diva_surf_info).roi_pimage
	Ptr_Free,(*diva_surf_info).roi_i_pimage
	Ptr_Free,(*diva_surf_info).roi_o_pimage
    Ptr_Free,diva_surf_info
ENDIF
END;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
