;------------------------------------------------------------------------------;
pro diva_roi_zoom,diva_roi_info;-----------------------------------------------;
;------------------------------------------------------------------------------;
; Purpose - rebin ROI image ('roi_pimage') and display it
;------------------------------------------------------------------------------;
temp	    	=   (*diva_roi_info)

IF(XRegistered("diva_hist") eq 1) THEN BEGIN
	widget_control,temp.hist_id,/destroy
END
 
bshift	    	=   temp.diva_autoscale[1]
diva_roi_mag	=   temp.diva_roi_mag
roi_o_pimage  	=   (*temp.roi_o_pimage)
n_dim	    	=   temp.n_dim



widget_control,temp.diva_roi_draw,/destroy  	;destroy ROI draw area 

;some other goodies we need
dx = temp.x[1] - temp.x[0] + 1    &   xsize =  dx * diva_roi_mag
dy = temp.y[1] - temp.y[0] + 1    &   ysize =  dy * diva_roi_mag

;OK, rebin 'roi_pimage' image array to new size
IF (n_dim eq 3) THEN roi_pimage = REBIN(roi_o_pimage,xsize,ysize,3,/sample) $
ELSE    	     	 roi_pimage = REBIN(roi_o_pimage,xsize,ysize,  /sample)
;Free up 'roi_pimage' pointer and create a new one for the rebinned version    
Ptr_Free,temp.roi_pimage
(*diva_roi_info).roi_pimage = Ptr_New(roi_pimage)
;(RE-)create ROI draw area - and then update it's widget id  
diva_roi_draw=widget_draw((*diva_roi_info).diva_roi_draw_base,	    	    $
    	xsize=xsize,ysize=ysize,Button_Events=1, Event_Pro='diva_roi_ev',   $
        /motion_events,/viewport_events,uvalue="ROI_DRAW_EVENT")
(*diva_roi_info).diva_roi_draw	=   diva_roi_draw
;display the new resized/rebinned 'roi_pimage' - as usual
CASE temp.diva_autoscale[0] OF
    0:  IF (n_dim eq 1) THEN        tv,ishft(roi_pimage,bshift)	    	    $
    	    ELSE	            	tv,ishft(roi_pimage,bshift),true=3
    1:  IF (n_dim eq 1) THEN        tvscl,roi_pimage 			    		$
    	    ELSE	            	tvscl,roi_pimage,true=3
ENDCASE

window,/free,xsize=xsize,ysize=ysize,/pixmap
(*diva_roi_info).diva_roi_draw_pix = !d.window
wset,!d.window
CASE temp.diva_autoscale[0] OF
    0:  IF (n_dim eq 1) THEN 	tv,ishft(roi_pimage,bshift)	    	    	$
    	    ELSE    	    	tv,ishft(roi_pimage,bshift),true=3
    1:  IF (n_dim eq 1) THEN 	tvscl,roi_pimage   	    	    	    	$
    	    ELSE    	    	tvscl,roi_pimage,true=3
ENDCASE

widget_control,diva_roi_draw,get_value=roi_draw
wset,roi_draw

if (XRegistered("diva_roi_profile") ne 0) then begin
    widget_control,(*temp.diva_roi_profile_info).diva_roi_profilebase,/destroy
    Ptr_free,temp.diva_roi_profile_info
endif

end; of 'diva_roi_zoom' procedure----------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_roi_ev, event;--------------------------------------------------------;
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
WIDGET_CONTROL, event.id, GET_UVALUE = eventval
widget_control,event.top,get_uvalue=diva_roi_info		

n_dim	    	    	=   (*diva_roi_info).n_dim
diva_roibase	    	=   (*diva_roi_info).diva_roibase	
image_ct    	    	=   (*diva_roi_info).image_ct
plot_ct     	    	=   (*diva_roi_info).plot_ct
diva_roi_profile_col 	=   (*diva_roi_info).diva_roi_profile_col
diva_roi_draw_pix   	=   (*diva_roi_info).diva_roi_draw_pix
diva_printers			=	(*diva_roi_info).diva_printers
diva_printers_list		=	(*diva_roi_info).diva_printers_list


widget_control,(*diva_roi_info).diva_roi_draw,get_value=roi_draw
wset,roi_draw

CASE eventval OF

"EXIT":     $	;EXIT_EXIT_EXIT_EXIT_EXIT_EXIT_EXIT_EXIT_EXIT_EXIT_EXIT_EXIT_EX
BEGIN
    WIDGET_CONTROL, event.top, /DESTROY     	    ;destroy widget
    return		
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"STATS":    $	;STATISTICS_STATISTICS_STATISTICS_STATISTICS_STATISTICS_STATIST
BEGIN
    temp    	    = 	(*diva_roi_info)
    roi_o_pimage    =	temp.roi_o_pimage
    x	    	    =	temp.x(sort(temp.x))
    y	    	    =	temp.y(sort(temp.y))
    ;call 'diva_stats_calc' procedure (in 'diva_stats.pro')
    diva_stats_calc,*roi_o_pimage,x,y,1,n_dim,diva_roibase
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"ROI_F":    $	;ROI_FACTORS_ROI_FACTORS_ROI_FACTORS_ROI_FACTORS_ROI_FACTORS_RO
BEGIN
    ;ok, get and update new 'diva_roi_mag' value
    (*diva_roi_info).diva_roi_mag = (*diva_roi_info).roi_factors[event.index]
    ;call 'diva_roi_zoom' procedure - which destroys the ROI
    ;draw area, and creates a new one (appropriately sized)
    diva_roi_zoom,diva_roi_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"SURF":     $	;ROI_SURFACE_ROI_SURFACE_ROI_SURFACE_ROI_SURFACE_ROI_SURFACE_RO
BEGIN
    if (XRegistered("diva_surf") eq 1) then return

	roi_o_pimage    = (*diva_roi_info).roi_o_pimage
    file_stuff	    = (*diva_roi_info).file_stuff 
    CASE n_dim OF
    	1:      diva_surf,Ptr_new(*roi_o_pimage),file_stuff,n_dim,			$
								diva_printers,diva_printers_list,			$
												group = diva_roibase
    	ELSE:   print,'not for rgb images......YET!!!!'
    ENDCASE

;	roi_data = (*(*diva_roi_info).roi_o_pimage)
;	data_size = size(roi_data)
;	roi_xsize = data_size[1]
;	roi_ysize = data_size[2]
;	oSurf =OBJ_NEW('IDLgrSurface',roi_data,style=10,color=[255,0,0], 		$
;							xcoord_conv=[-.5,1./roi_xsize],     			$
;							ycoord_conv=[-.5,1./roi_ysize],     			$
;							zcoord_conv=[-.5,1./max(roi_data)],/hidden_lines)
;	XOBJVIEW,oSurf,/modal,xs=800,ys=800,group=diva_roibase
	
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"ROI_PRINT":	$
BEGIN
	
	pimage			= (*diva_roi_info).roi_o_pimage
	diva_autoscale  = (*diva_roi_info).diva_autoscale
	file_stuff  	= (*diva_roi_info).file_stuff
	
	filename 		= file_stuff[0]
	pathname		= file_stuff[1]
	file_str		= file_stuff[2]
	mfile_name		= file_stuff[3]
	
	IF (mfile_name ne '') then file_str = file_str + '__' + mfile_name
	
	diva_whattoprint = 1
	
	;call "diva_export" procedure
	diva_export, pimage,pathname,file_str,diva_autoscale,   	    	    $
		    	    	diva_whattoprint,diva_printers, 					$
						diva_printers_list,image_ct,plot_ct,				$
						group=diva_roibase
	
	
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"ROI_DRAW_EVENT":   $	;ROI_DRAW_EVENTS_ROI_DRAW_EVENTS_ROI_DRAW_EVENTS_ROI_DR
BEGIN
    possibleEventTypes = [ 'DOWN', 'UP', 'MOTION', 'SCROLL']
    thisEvent = possibleEventTypes[event.type]
    ;some unpacking - need these values locally
    temp    	    = 	(*diva_roi_info)
    roi_pimage	    =	temp.roi_pimage
    diva_roi_mag    =	temp.diva_roi_mag
    xsize   	    =	((temp.x[1] - temp.x[0]) + 1) * diva_roi_mag
    ysize   	    =	((temp.y[1] - temp.y[0]) + 1) * diva_roi_mag
    diva_roi_profile_info   =	(temp.diva_roi_profile_info)
    
    event.x = 0 > event.x < (xsize - 1)     ;constrain coords to within 
    event.y = 0 > event.y < (ysize - 1)     ;the ROI draw area
	
	x_roi = event.x & y_roi = event.y
	
    ;report coords - dividing coord values by mag factor - in order to 
    ;take account of rebinning, and also add the x and y offsets
    xpos = strcompress((x_roi/diva_roi_mag)+temp.x[0],/remove)
    ypos = strcompress((y_roi/diva_roi_mag)+temp.y[0],/remove)
    widget_control,temp.diva_roi_report_cvalue,set_value = xpos +',' + ypos

    ;now find and display pixel value at this cursor position
	IF (size(*roi_pimage,/type) eq 1) THEN BEGIN
		CASE n_dim OF
	    	1:	vv =  	  strcompress(FIX((*roi_pimage)[x_roi,y_roi]),/remove)
	    	3:	vv =  	  strcompress(FIX((*roi_pimage)[x_roi,y_roi,0]),/rem)+  $
	    	    	',' + strcompress(FIX((*roi_pimage)[x_roi,y_roi,1]),/rem)+  $
	    	    	',' + strcompress(FIX((*roi_pimage)[x_roi,y_roi,2]),/rem)
		ENDCASE
	ENDIF ELSE BEGIN	
		CASE n_dim OF
	    	1:	vv =  	  strcompress((*roi_pimage)[x_roi,y_roi],/remove)
	    	3:	vv =  	  strcompress((*roi_pimage)[x_roi,y_roi,0],/rem)+ 		$
	    	    	',' + strcompress((*roi_pimage)[x_roi,y_roi,1],/rem)+ 		$
	    	    	',' + strcompress((*roi_pimage)[x_roi,y_roi,2],/rem)
		ENDCASE
	ENDELSE	
	widget_control,temp.diva_roi_report_vvalue,set_value= vv


    IF (XRegistered("diva_roi_profile") eq 1) AND 	    	    	    $ 
    (thisEvent eq 'DOWN') THEN BEGIN
		tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2]
		Device,Copy=[0,0,xsize-1,ysize-1,0,0,diva_roi_draw_pix]
	
		IF (diva_roi_profile_col eq 0) then BEGIN
	    	plots,[0,xsize-1],[y_roi,y_roi],/device,color=2
	     	widget_control,(*temp.diva_roi_profile_info).diva_roi_profile_draw, $
	    	    get_value=profile_draw
	    	wset,profile_draw
	    	oplot,(*temp.diva_roi_profile_info).result_x,(*roi_pimage)[*,y_roi]
	    	tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2]
		ENDIF ELSE BEGIN
	    	plots,[x_roi,x_roi],[0,ysize-1],/device,color=2
	    	widget_control,(*temp.diva_roi_profile_info).diva_roi_profile_draw, $
	    	    get_value=profile_draw
	    	wset,profile_draw
	    	oplot,(*temp.diva_roi_profile_info).result_x,(*roi_pimage)[x_roi,*]
	    	tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2]    
		ENDELSE
    ENDIF
    
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"ROI_COL":  	    $	;ROI_ROW_ROI_ROW_ROI_ROW_ROI_ROW_ROI_ROW_ROI_ROW_ROI_RO
BEGIN
    roi_pimage	=   (*(*diva_roi_info).roi_pimage)
    size_roi	=   size(roi_pimage)
    num_rows	=   float(size_roi[1])
    num_cols	=   float(size_roi[2])
    result  	=   fltarr(num_rows)
    file_stuff	=	(*diva_roi_info).file_stuff
	
    for i=0,num_rows-1 do begin
    	result[i] = total(float(roi_pimage[i,*]))/num_cols
    endfor
    
    x = (*diva_roi_info).x  	&   	x = x(sort(x))
    y = (*diva_roi_info).y  	&   	y = y(sort(y))
    
    result_x = indgen(n_elements(result))+x[0]
    
    IF (Ptr_Valid((*diva_roi_info).diva_roi_profile_info) eq 1) THEN BEGIN
    	;kill widget and free up the pointer
		widget_control,(*(*diva_roi_info).diva_roi_profile_info).   	    $
	    	    	    	    	    		diva_roi_profilebase,/destroy
    	Ptr_Free,(*diva_roi_info).diva_roi_profile_info					    
    ENDIF
	;call 'diva_roi_profile' procedure and return 'diva_roi_profile_info' ptr
	diva_roi_profile,result,result_x,diva_roi_profile_info,     	    	$
	    	    	    	    		image_ct,plot_ct,file_stuff,		$
										diva_printers,diva_printers_list,	$
										group=diva_roibase
	
	;update 'diva_roi_profile_info' pointer
	(*diva_roi_info).diva_roi_profile_info = diva_roi_profile_info
	
	(*diva_roi_info).diva_roi_profile_col = 0
    
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"ROI_ROW":  	    $	;ROI_COLUMN_ROI_COLUMN_ROI_COLUMN_ROI_COLUMN_ROI_COLUMN
BEGIN
    roi_pimage	=   (*(*diva_roi_info).roi_pimage)
    size_roi	=   size(roi_pimage)
    num_rows	=   float(size_roi[1])
    num_cols	=   float(size_roi[2])
    result  	=   fltarr(num_cols)
	file_stuff	=	(*diva_roi_info).file_stuff
    
    for i=0,num_cols-1 do begin
    	result[i] = total(float(roi_pimage[*,i]))/num_rows
    endfor

    x = (*diva_roi_info).x  	&   	x = x(sort(x))
    y = (*diva_roi_info).y  	&   	y = y(sort(y))
    
    result_x = indgen(n_elements(result))+y[0]

    IF (Ptr_Valid((*diva_roi_info).diva_roi_profile_info) eq 1) THEN BEGIN
    	;kill widget and free up the pointer
		widget_control,(*(*diva_roi_info).diva_roi_profile_info).			$
	    	    	    	    	    		diva_roi_profilebase,/destroy
    	Ptr_Free,(*diva_roi_info).diva_roi_profile_info					    
    ENDIF
	;call 'diva_roi_profile' procedure and return 'diva_roi_profile_info' ptr
	diva_roi_profile,result,result_x,diva_roi_profile_info, 				$
									image_ct,plot_ct,file_stuff,			$
									diva_printers,diva_printers_list,		$
									group=diva_roibase
	
	;update 'diva_roi_profile_info' pointer
	(*diva_roi_info).diva_roi_profile_info = diva_roi_profile_info
	
	(*diva_roi_info).diva_roi_profile_col = 1
    
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"ROI_HIST": 		$	;
BEGIN
	if (XRegistered("diva_hist") eq 1) THEN return
	
	;make a FAKE "pdiva_draw" containing...
	;....a fake "diva_image" containing....
	;....a fake "file_info". Hmmmmmmmmmmmmmmmmm???????
	;need to start in the reverse order.........	
	guess_name = (*diva_roi_info).guess_name
	print,guess_name
	;1. "file_info"
	diva_file = {diva_file}
	diva_file_init,diva_file,guess_name
	;2. "diva_image"
	diva_image  				= {diva_image}
	diva_image.file_info 		= diva_file
	diva_image.o_pimage	= Ptr_new((*(*diva_roi_info).roi_pimage))
	diva_image.i_mean			= [0.D,0.,0.]
	diva_image.i_stddev			= [0.D,0.,0.]
	diva_image.mfile_name_str	= ''
	diva_image.mfile_time		= ''
	diva_image.n_dim			= (*diva_roi_info).n_dim
	;3. "pdiva_draw"
	diva_roi_draw = (*diva_roi_info).diva_roi_draw
	diva_autoscale  = (*diva_roi_info).diva_autoscale
	pdiva_draw = Ptr_new({	image_ct			:	image_ct,				$
							plot_ct 			:	plot_ct,				$
							os_name 			:	!d.name,				$
							draw_area			:	diva_roi_draw,			$
							diva_image			:	diva_image,				$
							diva_printers		:	diva_printers,			$
							diva_printers_list	:	diva_printers_list,		$
							diva_roi_info		:	diva_roi_info,			$
							diva_autoscale		:	diva_autoscale			$
						})
	
	
	diva_hist,'roi_area',pdiva_draw,hist_id,								$
										group=(*diva_roi_info).diva_roibase
	
	(*diva_roi_info).hist_id = hist_id
	
	
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

ELSE: MESSAGE, "Event User Value Not Found"		
ENDCASE
END ;end of diva_roi event handling routine task-------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_roi_info__define

junk	=   {	diva_roi_info,	    	    	    	    	    		$
    	    	diva_roibase	    		:0L,							$
    	    	diva_roi_report_cvalue		:0L,        	    	        $
    	    	diva_roi_report_vvalue		:0L,        	    	        $
				diva_roi_draw	    		:0L,	    	    	        $
				diva_roi_draw_pix   		:0L,	    	    	        $
				diva_roi_draw_base  		:0L,	    	    	        $
		    	x   	    	    		:[0L,0L],	    	    	    $
				y   	    	    		:[0L,0L],	    	    	    $
				n_dim	    	    		:0,     	    	    	    $
				roi_o_pimage	    		:Ptr_New(), 	    	        $
				roi_pimage   	    		:Ptr_New(),	    	    	    $
				diva_roi_butt_mag_list		:0L,	    	    	        $
				roi_factors 	    		:intarr(9),        	    	    $
				image_ct					:bytarr(256,3),     	        $
				plot_ct 					:bytarr(256,3),     	        $
				diva_autoscale	    		:[0,0],	    	    	        $
				os_name     	    		:'', 	    	    	        $
				file_stuff	    			:strarr(4),  	    	        $
				diva_roi_mag	    		:0,  	    	    	        $
				diva_roi_profile_info		:Ptr_New(),	    	    		$
				diva_roi_profile_col		:0,  	    	    	    	$
				diva_printers				:strarr(3), 					$
				diva_printers_list			:strarr(3),						$
				guess_name					:'',							$
				hist_id 					:0L 							$
	    }
END;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
FUNCTION diva_roi_init,n_dim,image_ct,plot_ct,diva_draw_base,os_name,	    $
    	    	    diva_autoscale,roi_pimage,file_stuff,xc,yc,xs,ys,		$
					diva_printers,diva_printers_list
;------------------------------------------------------------------------------;
; Purpose - ROI widget. After a (regular) ROI has been selected, this widget
;   	    is created to display the selected ROI image. 
; Inputs  - 
;------------------------------------------------------------------------------;
diva_roi_info 	    	    	=   {diva_roi_info} 

x = [xc,xs] 	&   x = x(sort(x))  	&   delx = x[1] - x[0]
y = [yc,ys] 	&   y = y(sort(y))  	&   dely = y[1] - y[0]


diva_roi_info.n_dim 	    	=   n_dim
diva_roi_info.image_ct      	=   image_ct
diva_roi_info.plot_ct       	=   plot_ct
diva_roi_info.os_name	    	=   os_name
diva_roi_info.diva_autoscale	=   diva_autoscale
diva_roi_info.roi_o_pimage		=   Ptr_New(roi_pimage)
diva_roi_info.roi_pimage    	=   Ptr_New(roi_pimage)
diva_roi_info.diva_roi_mag  	=   1
diva_roi_info.x	    	    	=   x
diva_roi_info.y	    	    	=   y
diva_roi_info.diva_printers 	=	diva_printers
diva_roi_info.diva_printers_list=	diva_printers_list
screen_size 	    	    	=   get_screen_size(RESOLUTION = resolution) 
diva_roi_info.file_stuff    	=   file_stuff

IF (file_stuff[3] eq '') THEN guess_name = file_stuff[1] + file_stuff[2]	$
ELSE 	guess_name = file_stuff[1] + file_stuff[2] + '__' + file_stuff[3]
diva_roi_info.guess_name = strcompress(guess_name + '_roi.fit',/rem)


rf = [1,2,4,6,8,12,16,24,32]
if (delx ge dely) then begin
    nrf_elems = where(rf*delx lt screen_size[0] - 10)
    nrf=rf(where(rf*delx lt screen_size[0]))
endif else begin
    nrf_elems = where(rf*dely lt screen_size[1] - 200)
    nrf=rf(where(rf*dely lt screen_size[1]))
endelse
nrf_str=['1x','2x','4x','6x','8x','12x','16x','24x','32x']
nrf_str=nrf_str[nrf_elems]
diva_roi_info.roi_factors[0:n_elements(nrf)-1] = nrf

;-------------------create the main base---------------------------------------;
diva_roibase 				= WIDGET_BASE(TITLE = "DIVA ROI",/col,			$
    	    	    	    		group_leader=diva_draw_base,/floating)
diva_roi_info.diva_roibase  = diva_roibase				
;-------------------zoom factor base-------------------------------------------;
diva_roi_zoomfactor_base	= widget_base(diva_roibase,/row,/frame)

diva_roi_butt_mag_list		= widget_droplist(diva_roi_zoomfactor_base,	    $
    	    						value=nrf_str,							$
									title='Zoom Factor:',					$
									uvalue='ROI_F')
diva_roi_info.diva_roi_butt_mag_list = 	diva_roi_butt_mag_list    
;-------------------button base------------------------------------------------;
diva_roi_button_base		= widget_base(diva_roibase,/row,/frame, 		$
									/grid_layout)

	    
diva_roi_butt_surf			= widget_button(diva_roi_button_base,			$
									value='Surface...',	    				$
	    							uvalue='SURF')

diva_roi_butt_stats 		= widget_button(diva_roi_button_base,			$
									value='Stats...',    					$
	    							uvalue='STATS')
	    
diva_roi_butt_print 		= widget_button(diva_roi_button_base,			$
									value='Print...',    					$
	    							uvalue='ROI_PRINT')

diva_roi_butt_hist			= widget_button(diva_roi_button_base,			$
									value='Hist...',    					$
	    							uvalue='ROI_HIST')

	    
diva_roi_button2_base		= widget_base(diva_roibase,/row,/frame)	    
diva_roi_ave_label			= widget_label(diva_roi_button2_base,			$
									value = 'Line averaged profile:  ')
diva_roi_butt_ave_col		= widget_button(diva_roi_button2_base,			$
									value='  Column  ', 					$
    	    						uvalue='ROI_COL')
diva_roi_butt_ave_row		= widget_button(diva_roi_button2_base,			$
									value='   Row    ', 					$
    	    						uvalue='ROI_ROW')
	    	    
;-------------------draw area base---------------------------------------------;
diva_roi_draw_base			= widget_base(diva_roibase)
diva_roi_info.diva_roi_draw_base=diva_roi_draw_base
;-------------------draw window------------------------------------------------;
diva_roi_draw				= widget_draw(diva_roi_draw_base,				$
									xsize=delx,ysize=dely,	    			$
    	    						Button_Events=1,						$
									/motion_events, 						$
									/viewport_events,   	    			$
									uvalue="ROI_DRAW_EVENT")
diva_roi_info.diva_roi_draw = diva_roi_draw

window,/free,xsize=delx,ysize=dely,/pixmap
diva_roi_info.diva_roi_draw_pix = !d.window
;-------------------reporting widget-------------------------------------------;
diva_roi_report_base		= widget_base(diva_roibase,/row,/frame)

diva_roi_report_clabel		= widget_label(diva_roi_report_base,			$
									value='Cursor:')
diva_roi_report_cvalue		= widget_text(diva_roi_report_base, 			$
									value='0,0',xsize=10)

diva_roi_report_vlabel		= widget_label(diva_roi_report_base,			$
									value='  Value:')
CASE n_dim OF
    1:	minmaxval = '0'
    3:	minmaxval = '0,0,0'
ENDCASE
diva_roi_report_vvalue		= widget_text(diva_roi_report_base, 			$
									value='0',xsize=12)

diva_roi_info.diva_roi_report_cvalue=diva_roi_report_cvalue
diva_roi_info.diva_roi_report_vvalue=diva_roi_report_vvalue

;-------------------quit button------------------------------------------------;
quitbutton					=widget_button(diva_roibase,					$
									value='Quit',uvalue='EXIT',/frame)
;------------------------------------------------------------------------------;

WIDGET_CONTROL, diva_roibase, /REALIZE		

pdiva_roi_info = Ptr_New(diva_roi_info)						
    
widget_control,diva_roibase,set_uvalue = pdiva_roi_info

XManager, "diva_roi", diva_roibase,EVENT_HANDLER = "diva_roi_ev",   	    $
		GROUP_LEADER = diva_draw_base,cleanup = 'diva_roi_cleanup'
		
Return,	pdiva_roi_info
			
END ;end of diva_roi main routine----------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_roi_cleanup,diva_roibase
;------------------------------------------------------------------------------;
; Purpose - to cleanup diva_roi widget - specifically to free it's pointer!
;------------------------------------------------------------------------------;
widget_control,diva_roibase,get_uvalue=diva_roi_info
print,'CLEANING UP DIVA ROI        **********************'
IF (Ptr_Valid(diva_roi_info) eq 1) then begin
	
	IF(XRegistered("diva_hist") eq 1) THEN BEGIN
		;widget_control,(*diva_roi_info).hist_id,/destroy
	END
    Ptr_Free,(*diva_roi_info).roi_o_pimage
    Ptr_Free,(*diva_roi_info).roi_pimage
    Ptr_Free,diva_roi_info
ENDIF
END;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_roi_display,diva_roi_info
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
temp 	    	    = (*diva_roi_info)
diva_autoscale_on   = temp.diva_autoscale[0]
bshift	    	    = temp.diva_autoscale[1]
n_dim 	    	    = temp.n_dim
roi_pimage    	    = (*temp.roi_pimage)
diva_roi_draw_pix   = temp.diva_roi_draw_pix

widget_control,temp.diva_roi_draw,get_value=roi_index
wset,roi_index
CASE diva_autoscale_on OF
    0:  IF (n_dim eq 1) THEN 	tv,ishft(roi_pimage,bshift)	    	    $
    	    ELSE    	    	tv,ishft(roi_pimage,bshift),true=3
    1:  IF (n_dim eq 1) THEN 	tvscl,roi_pimage   	    	    	    $
    	    ELSE    	    	tvscl,roi_pimage,true=3
ENDCASE

wset,diva_roi_draw_pix
CASE diva_autoscale_on OF
    0:  IF (n_dim eq 1) THEN 	tv,ishft(roi_pimage,bshift)	    	    $
    	    ELSE    	    	tv,ishft(roi_pimage,bshift),true=3
    1:  IF (n_dim eq 1) THEN 	tvscl,roi_pimage   	    	    	    $
    	    ELSE    	    	tvscl,roi_pimage,true=3
ENDCASE

end;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
