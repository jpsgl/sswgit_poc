;------------------------------------------------------------------------------;
pro diva_adaptive
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;


end
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_adaptive_read_log,pick_log,pick_data,new_list,diva_mult_image
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;

;call 'diva_logfile_read' procedure
diva_logfile_read,pick_log,n_elems_dummy,mfile_pos_from,mfile_pos_to,	    $
					    mfile_name,mfile_date,mfile_time

number_images=n_elems_dummy

;call procedure to get data file name
diva_ad_get_data_file,pick_log,pick_data
f   	    = pick_data
;make and populate/initialise 'diva_file' structure
diva_file   = {diva_file}
diva_file_init,diva_file,f

diva_mult_image = replicate({diva_mult_image},number_images)
tmp_diva_mult_image = diva_mult_image

for i=0,number_images-1 do begin
    diva_mult_image_init,tmp_diva_mult_image,mfile_pos_from[i],     	    $
    	    	    	    	    	     mfile_pos_to[i],mfile_name[i], $
					     mfile_date[i],mfile_time[i]
    tmp_diva_mult_image[i].file_info = diva_file[0]
    diva_mult_image[i] =  tmp_diva_mult_image[i] 
endfor

new_list=strarr(number_images)
mfile_name_str=strarr(number_images)

for i=0,number_images-1 do begin
    new_list(i) = mfile_time(i)+'  ' + mfile_date(i)+'  '+ mfile_name(i)
    mfile_name_str[i] = strmid(mfile_name[i],0, 	    	    	    $
	    	    	    	strpos(mfile_name[i],'.',/reverse_search))
endfor

END
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_ad_get_data_file,pick_log,pick_data

;create file_specification (see IDL help "FINDFILE") to search for
pick_log_str=strmid(pick_log,0,strpos(pick_log,'.',/reverse_search))+'.*'

r=findfile(pick_log_str)

;remove pathanmes - they confuse stupid Windoze
for i=0,n_elements(r)  -1 do begin
	r[i] =	strmid(r[i],strpos(r[i],'\',/reverse_search)+1)
endfor

;do the same with 'pick_log'
p=pick_log
p=strmid(p,strpos(p,'\',/reverse_search)+1)

;get the pathame
pathname = strmid(pick_log,0,strpos(pick_log,'\',/reverse_search)+1)

c=strmatch(r,p)
nr=r(where(c eq 0))

CASE n_elements(nr) OF
    0:	    pick_data=''	
    1:      pick_data=strcompress(pathname+nr,/remove)
    ELSE:   stop
ENDCASE

end
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_ad_read_data,pick_data,temp,mult

temp.mfile_pos_from	= mult.mfile_pos_from
temp.mfile_pos_to	= mult.mfile_pos_to
temp.mfile_name     	= mult.mfile_name
temp.mfile_name_str 	= mult.mfile_name_str
temp.mfile_date     	= mult.mfile_date
temp.mfile_time     	= mult.mfile_time
temp.file_info      	= mult.file_info

openr,1,pick_data
header=read_binary(1,data_type=12,data_dims=4,data_start=temp.mfile_pos_from)
image = uintarr(header[0],header[1],header[2])
READU,1,image

file_size=FSTAT(1)
file_size=file_size.size
close,1

image=reverse(image)
    if (header[2] eq 3) then begin
	b0	    =	rotate(image[*,*,0],2)
	b1	    =	rotate(image[*,*,1],2)
	b2	    =	rotate(image[*,*,2],2)
	image   =	[[[b0]],[[b1]],[[b2]]]
    endif else begin
	image=rotate(image,2)
    endelse

p_pimage=congrid(image,300,300,header[2])

xsize   	=  header[0] 
ysize   	=  header[1]
n_dim	    	=  header[2]
i_file_size     =  temp.mfile_pos_to - temp.mfile_pos_from + 1 - 8

if (Ptr_Valid(temp.pimage) eq 1) then begin
    Ptr_Free,temp.pimage
    Ptr_Free,temp.o_pimage
    Ptr_Free,temp.p_pimage
end 
temp.pimage	    =  ptr_new(image)
temp.o_pimage	    =  temp.pimage
temp.p_pimage	    =  ptr_new(p_pimage)
temp.mfile_size     =  file_size
temp.xsize 	    =  xsize
temp.ysize 	    =  ysize

temp.n_dim 	    =  n_dim
temp.i_file_size    =  i_file_size
temp.diva_n_images  =  n_elements(mult)
temp.ops_name 	    =  temp.file_info.pathname + temp.file_info.file_str +  $
    	    	    	    	'__' + temp.mfile_name_str + '.' +  	    $
    	    	    	    	temp.file_info.file_ext + '(' +     	    $
    	    	    	    	strcompress(xsize,/rem) + ',' +     	    $
				strcompress(ysize,/rem) + ')(1,0)'

end ;--------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
