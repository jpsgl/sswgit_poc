;------------------------------------------------------------------------------;
pro diva_hilite_display,diva_hilite_info
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
pimage      	= (*(*diva_hilite_info).pimage)
hilite_value	= (*diva_hilite_info).hilite_value
hilite_range	= (*diva_hilite_info).hilite_range
hilite_color	= (*diva_hilite_info).hilite_color
image_ct        = (*diva_hilite_info).image_ct
plot_ct	    	= (*diva_hilite_info).plot_ct
hilite_num_id	= (*diva_hilite_info).hilite_num_id
hilite_ran_id	= (*diva_hilite_info).hilite_ran_id
hilite_oper 	= (*diva_hilite_info).hilite_oper


widget_control,/hourglass

    widget_control,(*diva_hilite_info).diva_tlb_drawwid, get_value=tlb_draw
    wset,tlb_draw
    
    display = BytScl(pimage, Top=!D.Table_Size-2)
    
    lower = (hilite_value - hilite_range)
    if (lower lt 0) then lower = 0
    
    upper = (hilite_value + hilite_range)
    if (upper gt max(pimage)) then upper = max(pimage)
    
    li=Where((pimage ge lower) and (pimage le upper),count)
;    print,  strcompress(lower,/rem) + '-->' +     	    $
;    	    strcompress(upper,/rem)
    
    if (hilite_range eq 0) then li=Where(pimage eq hilite_value,count)
    
    if (hilite_oper  eq 1) then begin
    	li=where(pimage lt hilite_value,count)
	lower = 0
	upper = hilite_value - 1
    endif
    if (hilite_oper  eq 2) then begin
    	li=where(pimage gt hilite_value,count)
	lower = hilite_value + 1
	upper = max(pimage)
    endif
    
    CASE hilite_color OF
    	'Yellow':   tvlct,255,255,0  , !D.Table_Size-1
	'White':    tvlct,255,255,255, !D.Table_Size-1
	'Black':    tvlct,0  ,0  ,0  , !D.Table_Size-1
	'Red':	    tvlct,255,0  ,0  , !D.Table_Size-1
	'Green':    tvlct,0  ,255,0  , !D.Table_Size-1
	'Blue':     tvlct,0  ,0  ,255, !D.Table_Size-1
	'Cyan':     tvlct,0  ,255,255, !D.Table_Size-1
    ENDCASE
    ;tvlct,255,0,0, !D.Table_Size-1
    if (count gt 0) then begin
    	display[li] = !D.Table_Size-1 
    	tv,display
    	widget_control,hilite_num_id,set_value=strcompress(count,/rem)
    endif else begin
    	tv,display
	widget_control,hilite_num_id,set_value='0'
    endelse
    tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2]

widget_control,hilite_ran_id,set_value =    	    	    	    	    $
    	    	    strcompress(lower,/rem) + ':' + strcompress(upper,/rem)

end ;of 'diva_hilite_display'--------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_hilite_ev, event
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
WIDGET_CONTROL, event.id, GET_UVALUE = eventval		
widget_control,event.top,get_uvalue=diva_hilite_info

max_pimage  	=   (*diva_hilite_info).max_pimage
hilite_input_id =   (*diva_hilite_info).hilite_input_id
hilite_range_id =   (*diva_hilite_info).hilite_range_id

CASE eventval OF

"EXIT":     	BEGIN
    	    	    WIDGET_CONTROL, event.top, /DESTROY	
		    return	
	    	END
"DO_HILITE":	$
BEGIN
    widget_control,(*diva_hilite_info).hilite_input_id,get_value=s_hilite_value
    hilite_value = long(s_hilite_value[0])
    
    if (hilite_value gt max_pimage) then begin
    	widget_control,hilite_input_id,set_value = strcompress(max_pimage,/rem)
	hilite_value = max_pimage
    endif
    (*diva_hilite_info).hilite_value = hilite_value
    
    diva_hilite_display,diva_hilite_info
    
END
"DO_RANGE":	$
BEGIN
    widget_control,(*diva_hilite_info).hilite_range_id,get_value=s_hilite_range
    hilite_range = long(s_hilite_range[0])
    
    (*diva_hilite_info).hilite_range = hilite_range
    
    diva_hilite_display,diva_hilite_info
END
"DO_COLOR":	$
BEGIN
    colors=['Yellow','White','Black','Red','Green','Blue','Cyan']
    (*diva_hilite_info).hilite_color = colors[event.index]
    diva_hilite_display,diva_hilite_info
END
"DO_OPER":	$
BEGIN
    (*diva_hilite_info).hilite_oper = event.index
    if (event.index eq 0) then  widget_control,hilite_range_id,sensitive=1  $
    ELSE    	    	    	widget_control,hilite_range_id,sensitive=0
    
    diva_hilite_display,diva_hilite_info
END

ELSE: MESSAGE, "Event User Value Not Found"		
ENDCASE
END ;end of "diva_hilite" event handler----------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_hilite, pimage,diva_tlb_drawwid,plot_ct,image_ct,GROUP = GROUP
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;

IF(XRegistered("diva_hilite") NE 0) THEN RETURN		

;-------------------create the main base---------------------------------------;
diva_hilitebase = WIDGET_BASE(TITLE = "DIVA PIXEL HIGHLIGHTER",/col,	    $
    	    group_leader=group,/floating,/frame)


topbase=widget_base(diva_hilitebase,/col)

hilite_value_base=widget_base(topbase,/col,/frame)
hilite_value_label=widget_label(hilite_value_base,value='Set value:',	    $
    	    /align_left)
hilite_input_id = widget_text(hilite_value_base,/editable,value='1000',     $
    	    uvalue='DO_HILITE',xsize=8)


hilite_oper_id=widget_droplist(hilite_VALUE_base,uvalue='DO_OPER',  	    $
    	    value=['  +/-  ','  < ','  > '],/align_left)



hilite_range_id = widget_text(hilite_VALUE_base,/editable,value='100',      $
    	    uvalue='DO_RANGE',xsize=8,sensitive=1)


hilite_color_label=widget_label(hilite_VALUE_base,value='Set coloUr:',	    $
    	    /align_left)
hilite_color =widget_droplist(hilite_VALUE_base,value=['Yellow','White',    $
    	    'Black','Red','Green','Blue','Cyan'],uvalue='DO_COLOR')

hilite_ran_label=widget_label(hilite_VALUE_base,value='Value range:',	    $
    	    /align_left)
hilite_ran_id=widget_label(hilite_VALUE_base,value='900:1100',xsize=100)	

hilite_num_label=widget_label(hilite_VALUE_base,value='No. of pixels:',     $
    	    /align_left)
hilite_num_id=widget_label(hilite_VALUE_base,value='0',xsize=100)		
;-------------------quit button------------------------------------------------;
quitbutton=widget_button(diva_hilitebase, value='Quit',uvalue='EXIT')
;------------------------------------------------------------------------------;

;-------------------realise the widget-----------------------------------------;
WIDGET_CONTROL, diva_hilitebase, /REALIZE		
						

diva_hilite_info=Ptr_New({  diva_hilitebase   	:   diva_hilitebase,	    $
    	    	    	    pimage  	    	:   pimage, 	    	    $
			    hilite_value    	:   1000L, 	    	    $
			    hilite_range    	:   100L,  	    	    $
			    hilite_color    	:   'Yellow',	    	    $
			    hilite_input_id     :   hilite_input_id, 	    $
			    hilite_range_id 	:   hilite_range_id,	    $
			    hilite_ran_id   	:   hilite_ran_id,  	    $
			    hilite_num_id   	:   hilite_num_id,  	    $
			    diva_tlb_drawwid	:   diva_tlb_drawwid,	    $
			    plot_ct 	    	:   plot_ct,	    	    $
			    image_ct	    	:   image_ct,	    	    $
			    max_pimage	    	:   max(*pimage),     	    $
			    hilite_oper    	:   0	    	    	    $
			})

widget_control,diva_hilitebase,set_uvalue= diva_hilite_info


XManager, "diva_hilite", diva_hilitebase, $		
		EVENT_HANDLER = "diva_hilite_ev", $
		GROUP_LEADER = GROUP,cleanup='diva_hilite_cleanup'		
						
END ;end of "diva_hilite" main routine-----------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
Pro diva_hilite_cleanup,diva_hilitebase
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
widget_control,diva_hilitebase,get_uvalue=diva_hilite_info
if (ptr_valid(diva_hilite_info) eq 1 ) then begin
    ;Ptr_Free,(*diva_hilite_info).pimage
    Ptr_Free,diva_hilite_info
ENDIF
end;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
