;------------------------------------------------------------------------------;
PRO diva_hist_ev, event
;------------------------------------------------------------------------------;
; Purpose -
;------------------------------------------------------------------------------;
WIDGET_CONTROL, event.id, GET_UVALUE = eventval
widget_control,event.top,get_uvalue=diva_hist_info

diva_printers		    = (*diva_hist_info).diva_printers
diva_printers_list	    = (*diva_hist_info).diva_printers_list
n_dim				    = (*diva_hist_info).n_dim
rgb_on				    = (*diva_hist_info).rgb_on
ebin_on 			    = (*diva_hist_info).ebin_on
linplot_on  		    = (*diva_hist_info).linplot_on
smooth_list_value	    = (*diva_hist_info).smooth_list_value
diva_side_hilite_base	= (*diva_hist_info).diva_side_hilite_base
diva_side_thresh_base	= (*diva_hist_info).diva_side_thresh_base
hilite_value		    = (*diva_hist_info).hilite_value
hilite_range		    = (*diva_hist_info).hilite_range
hilite_color		    = (*diva_hist_info).hilite_color
func_list_value 	    = (*diva_hist_info).func_list_value
hilite_fill_coords	    = (*diva_hist_info).hilite_fill_coords
hilite_oper 		    = (*diva_hist_info).hilite_oper
thresh_values		    = (*diva_hist_info).thresh_values
i_mean      	        = (*(*diva_hist_info).diva_image).i_mean
i_stddev    	        = (*(*diva_hist_info).diva_image).i_stddev


CASE eventval OF

"EXIT": 			$
BEGIN
	WIDGET_CONTROL, event.top, /DESTROY
	return
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"SMOOTH":			$
BEGIN
	new = (*diva_hist_info).smooth_list_list[event.index]
	old = (*diva_hist_info).smooth_list_value

	if (old ne new) then begin
		(*diva_hist_info).smooth_list_value = new
		;call 'diva_hist_display' to plot histogram
		diva_hist_display,diva_hist_info
	end
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"FUNCTION": 		$
BEGIN
	new = event.index
	old = (*diva_hist_info).func_list_value
	
	if (old eq new) then RETURN
	
	(*diva_hist_info).func_list_value = new
	
	diva_hist_display,diva_hist_info
	
	CASE new OF
		0:	BEGIN
				widget_control,diva_side_hilite_base,map=0
				widget_control,diva_side_thresh_base,map=0
			END	
		1:	BEGIN
				widget_control,diva_side_hilite_base,map=0
				widget_control,diva_side_thresh_base,map=1
				diva_hist_thresh_display,diva_hist_info
			END
		2:	BEGIN
				widget_control,diva_side_hilite_base,map=1
				widget_control,diva_side_thresh_base,map=0
				diva_hilite_display,diva_hist_info
			END
		3:	BEGIN
				widget_control,diva_side_hilite_base,map=0
				widget_control,diva_side_thresh_base,map=0
				stop
			END
	ENDCASE
	
	
	
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"HISTDRAW": 		$
BEGIN
	diva_hist_index = (*diva_hist_info).diva_hist_index
	widget_control,(*diva_hist_info).diva_hist_draw,get_value = hist_draw
	wset,hist_draw
	hist_motion_evs_on = (*diva_hist_info).hist_motion_evs_on

	possibleEventTypes = [ 'DOWN', 'UP', 'MOTION', 'SCROLL']
	thisEvent = possibleEventTypes(event.type)

	event.x = 0 > event.x < (500 - 1)
	event.y = 0 > event.y < (320 - 1)
	coords = Convert_Coord(event.x, event.y, /Device, /To_Data)
	xh = !x.crange(0) > coords(0) < !x.crange(1)
	yh = !y.crange(0) > coords(1) < !y.crange(1)
	widget_control,(*diva_hist_info).infoxy_id, 	    $
    	  set_value=strcompress(round(xh),/rem) +       $
		  ',' + strcompress(round(yh),/rem)


	IF (thisevent eq 'DOWN') THEN BEGIN
		(*diva_hist_info).hist_motion_evs_on = 1
		(*diva_hist_info).dhcoords[0] = xh
		(*diva_hist_info).dhcoords[1] = yh
	ENDIF ;of a DOWN event

	IF (thisevent eq 'MOTION') THEN BEGIN
		xhd = xh & yhd = yh
		Device,Copy=[0,0,500,320,0,0,diva_hist_index]
		if (hist_motion_evs_on eq 0) then begin
    		plots,[xhd,xhd],[!y.crange(0),!y.crange(1)]
    		plots,[!x.crange(0),!x.crange(1)],[yhd,yhd]
		endif else begin
			xhs = (*diva_hist_info).dhcoords[0]
			yhs = (*diva_hist_info).dhcoords[1]
    		oplot,[xhs,xhs],[!y.crange(0),!y.crange(1)]
    		oplot,[xhd,xhd],[!y.crange(0),!y.crange(1)]
    		arrow,xhs,yhs,xhd,yhs,/data,hsize=10,/solid,thick=2
		endelse
	ENDIF ;of MOTION events

	IF (thisevent eq 'UP') THEN BEGIN
		(*diva_hist_info).hist_motion_evs_on = 0
		
		xhs = (*diva_hist_info).dhcoords[0]
		
		(*diva_hist_info).dhcoords[2] = xh
		(*diva_hist_info).dhcoords[3] = yh

		;reset condition
		if (abs(xhs-xh) lt 5) then begin
    		(*diva_hist_info).dhcoords[0] = min((*(*diva_hist_info).bins))
    		(*diva_hist_info).dhcoords[2] = max((*(*diva_hist_info).bins))

    		ie = [0,0,0,0,0,0]
    		;call 'diva_hist_sensitize' pro
			diva_hist_sensitize,(*diva_hist_info).go_buttons,ie
		endif else begin
			ie = [1,1,1,1,1,1]
			;call 'diva_hist_sensitize' pro
    		diva_hist_sensitize,(*diva_hist_info).go_buttons,ie
		endelse

		;call 'diva_hist_display' to plot histogram
		diva_hist_display,diva_hist_info
	ENDIF ;of UP event
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"GO_ALL_L": 		$
BEGIN
	x_0 							= (*diva_hist_info).dhcoords[0]
	x_1 							= (*diva_hist_info).dhcoords[2]
	dx  							= abs(x_0 - x_1)/2
	(*diva_hist_info).dhcoords[0] 	= 0
	(*diva_hist_info).dhcoords[2] 	= dx*2

	ie = [0,0,1,1,1,1]
	;call 'diva_hist_sensitize' pro
	diva_hist_sensitize,(*diva_hist_info).go_buttons,ie
	;call 'diva_hist_display' to plot histogram
	diva_hist_display,diva_hist_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"GO_ALL_R": 		$
BEGIN
	x_0 = (*diva_hist_info).dhcoords[0]
	x_1 = (*diva_hist_info).dhcoords[2]
	dx = abs(x_0 - x_1)/2
	maxv = max( (*(*diva_hist_info).bins) )
	(*diva_hist_info).dhcoords[0] = maxv - (dx*2)
	(*diva_hist_info).dhcoords[2] = maxv

	ie = [1,1,0,0,1,1]
	;call 'diva_hist_sensitize' pro
	diva_hist_sensitize,(*diva_hist_info).go_buttons,ie
	;call 'diva_hist_display' to plot histogram
	diva_hist_display,diva_hist_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"GO_L": 			$
BEGIN
	x_0 = (*diva_hist_info).dhcoords[0]
	x_1 = (*diva_hist_info).dhcoords[2]
	dx  = abs(x_0 - x_1)/4
	if (x_0 - dx gt 0) then begin
		(*diva_hist_info).dhcoords[0] = x_0 - dx
		(*diva_hist_info).dhcoords[2] = x_1 - dx
		ie = [1,1,1,1,1,1]
	endif else begin
		(*diva_hist_info).dhcoords[0] = 0
		(*diva_hist_info).dhcoords[2] = dx*2
		ie = [0,0,1,1,1,1]
	endelse
	;call 'diva_hist_sensitize' pro
	diva_hist_sensitize,(*diva_hist_info).go_buttons,ie
	;call 'diva_hist_display' to plot histogram
	diva_hist_display,diva_hist_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"GO_R": 			$
BEGIN
	x_0 = (*diva_hist_info).dhcoords[0]
	x_1 = (*diva_hist_info).dhcoords[2]
	dx  = abs(x_0 - x_1)/4
	maxv = max( (*(*diva_hist_info).bins) )
	if (x_1 + dx lt maxv) then begin
		(*diva_hist_info).dhcoords[0] = x_0 + dx
		(*diva_hist_info).dhcoords[2] = x_1 + dx
		ie = [1,1,1,1,1,1]
	endif else begin
		(*diva_hist_info).dhcoords[0] = maxv - (dx*2)
		(*diva_hist_info).dhcoords[2] = maxv
		ie = [1,1,0,0,1,1]
	endelse
	;call 'diva_hist_sensitize' pro
	diva_hist_sensitize,(*diva_hist_info).go_buttons,ie
	;call 'diva_hist_display' to plot histogram
	diva_hist_display,diva_hist_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"R_DBLE":			$
BEGIN
	maxv 	= max( (*(*diva_hist_info).bins) )
	x_0 	= (*diva_hist_info).dhcoords[0]
	x_1 	= (*diva_hist_info).dhcoords[2]

	IF (x_0 gt 0) OR (x_1 lt maxv) THEN BEGIN
		dx = abs(x_0 - x_1)
		newx = [x_0 - (dx/2),x_1 + (dx/2)]

		if (newx[0] lt 1) then begin
			newx[0] = 0
			ie = [0,0,1,1,1,1]
			;call 'diva_hist_sensitize' pro
			diva_hist_sensitize,(*diva_hist_info).go_buttons,ie
		endif
		if (newx[1] ge maxv) then begin
			newx[1] = maxv
			ie = [1,1,0,0,1,1]
			;call 'diva_hist_sensitize' pro
			diva_hist_sensitize,(*diva_hist_info).go_buttons,ie
			;stop
		endif
		if (newx[0] lt 1) AND (newx[1] ge maxv) then begin
			ie = [0,0,0,0,0,0]
			;call 'diva_hist_sensitize' pro
			diva_hist_sensitize,(*diva_hist_info).go_buttons,ie
			;stop
		endif
		if (newx[1]-newx[0] le 6) then begin
			ie = [1,1,1,1,1,0]
			;call 'diva_hist_sensitize' pro
			diva_hist_sensitize,(*diva_hist_info).go_buttons,ie
			;stop
		endif

		ie = [1,1,1,1,1,1]
		;call 'diva_hist_sensitize' pro
		diva_hist_sensitize,(*diva_hist_info).go_buttons,ie

		catch,wot & if (wot ne 0) then stop
		(*diva_hist_info).dhcoords[0] = newx[0]
		(*diva_hist_info).dhcoords[2] = newx[1]

		;call 'diva_hist_display' to plot histogram
		diva_hist_display,diva_hist_info
	ENDIF ELSE BEGIN
		ie = [0,0,0,0,0,0]
		;call 'diva_hist_sensitize' pro
		diva_hist_sensitize,(*diva_hist_info).go_buttons,ie
		;call 'diva_hist_display' to plot histogram
		diva_hist_display,diva_hist_info
	ENDELSE

END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"R_HALF":			$
BEGIN
	maxv 	= max( (*(*diva_hist_info).bins) )
	x_0 	= (*diva_hist_info).dhcoords[0]
	x_1 	= (*diva_hist_info).dhcoords[2]
	dx  	= (x_1 - x_0)
	newx 	= [x_0 + (dx/4),x_1 - (dx/4)]
	IF (newx[1]-newx[0] ge 5) THEN BEGIN
		(*diva_hist_info).dhcoords[0] = newx[0]
		(*diva_hist_info).dhcoords[2] = newx[1]
		;call 'diva_hist_display' to plot histogram
		diva_hist_display,diva_hist_info
		ie = [1,1,1,1,1,1]
		;call 'diva_hist_sensitize' pro
		diva_hist_sensitize,(*diva_hist_info).go_buttons,ie
	ENDIF ELSE BEGIN
		ie = [1,1,1,1,1,0]
		;call 'diva_hist_sensitize' pro
		diva_hist_sensitize,(*diva_hist_info).go_buttons,ie
	ENDELSE
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"HIST_PS":			$
BEGIN
	b 			= (*(*diva_hist_info).n_bins)
	h 			= (*(*diva_hist_info).n_hist)

	plot_ct 	= (*(*diva_hist_info).pdiva_draw).plot_ct
	image_ct	= (*(*diva_hist_info).pdiva_draw).image_ct
    	    	    
	file_info 	= (*(*diva_hist_info).diva_image).file_info
		    
	path    	= file_info.pathname
	file_str	= file_info.file_str
	file_in		= file_str + '_hist.ps'
			
	;call 'diva_print_what' FUNCTION to get filename and 
	result = diva_print_what(path,file_in,diva_printers, 	    			$
		    			diva_printers_list,(*diva_hist_info).diva_histbase)
		    
	IF ((*result).do_print_yes eq 1) THEN BEGIN
			
		filename 		= (*result).path+(*result).filename
		what_printer	= diva_printers[(*result).what_printer]
		do_save_yes 	= (*result).do_save_yes
		    
		print,'printing to: "'+strcompress(filename)+'"'
		
		os_name = !d.name
		   
		set_plot,'ps'
		device,filename=filename,/color

		;change to PLOT colour table
		tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2]
				
		;call "diva_hist_ops" procedure 
		diva_hist_ops,h,linplot_on,smooth_list_value,n_dim,			$
													ebin_on,empty_bins_hist
				
				
		;call "diva_hist_plot" procedure 		
		diva_hist_plot,h,b,empty_bins_hist,linplot_on,ebin_on,				$
					i_mean,i_stddev,rgb_on,n_dim,hilite_value,				$ 
					hilite_range,hilite_color,func_list_value,				$
					hilite_fill_coords,hilite_oper,thresh_values

		;change back to IMAGE colour table
		tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2]

		device,/close
		CASE os_name OF
			'X':	BEGIN
						set_plot,'x'
						;call "diva_print_linux" pro to do printing!!!
						diva_print_linux,filename,do_save_yes,what_printer
					END
			'WIN':		set_plot,'win'
		ENDCASE
		

		
		;change to PLOT colour table
		tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2]
				
		;call "diva_hist_ops" procedure 
		diva_hist_ops,h,linplot_on,smooth_list_value,n_dim,			$
													ebin_on,empty_bins_hist
				
		;call "diva_hist_plot" procedure 		
		diva_hist_plot,h,b,empty_bins_hist,linplot_on,ebin_on,				$
					i_mean,i_stddev,rgb_on,n_dim,hilite_value,				$ 
					hilite_range,hilite_color,func_list_value,				$
					hilite_fill_coords,hilite_oper,thresh_values
		
		;change back to IMAGE colour table
		tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2]
		
		
	ENDIF
	;now free up the 'result' pointer
	Ptr_Free,result

END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"HIST_PRINT":		$
BEGIN
	b 		= (*(*diva_hist_info).bins)
	h 		= (*(*diva_hist_info).hist)

	f 		= fltarr(2,n_elements(b))
	f[0,*]  = b
	f[1,*]  = h

	mfile_name_str  = (*(*diva_hist_info).diva_image).mfile_name_str
	mfile_time      = (*(*diva_hist_info).diva_image).mfile_time
	file_info 		= (*(*diva_hist_info).diva_image).file_info

	IF (mfile_name_str eq '') THEN BEGIN
		file_str 	= file_info.file_str
		filename 	= strcompress(file_str,/rem) + '_hist.dat'
	ENDIF ELSE BEGIN
		CASE mfile_time OF
			'': 	r = 'unkown'
			ELSE:	BEGIN
						r = strsplit(mfile_time,':',/extract)
						r = r[0]+'_'+r[1]+'_'+r[2]
					END
		ENDCASE
		filename 	= strcompress(mfile_name_str,/rem) + '_' +  			$
											strcompress(r,/rem) + '_hist.dat'
	ENDELSE
	
	print,'***********   printing data to file [2,' +	    				$
		    	    	strcompress(n_elements(b),/rem) +   				$
						']  -->   ',filename

	openw,1,filename
	printf,1,f
	close,1

END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"LIN_LOG":			$
BEGIN
	if (event.index NE (*diva_hist_info).linplot_on) then RETURN
	CASE event.index OF
		0:	(*diva_hist_info).linplot_on = 1
		1:	(*diva_hist_info).linplot_on = 0
	ENDCASE
	;call 'diva_hist_display' to plot histogram
	diva_hist_display,diva_hist_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"E_BINS":			$
BEGIN
	if (event.index EQ (*diva_hist_info).ebin_on) then RETURN
	CASE event.index OF
		0:	(*diva_hist_info).ebin_on = 0
		1:	(*diva_hist_info).ebin_on = 1
	ENDCASE
	;call 'diva_hist_display' to plot histogram
	diva_hist_display,diva_hist_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"COL_R":			$
BEGIN
	 CASE event.select OF
		1:  (*diva_hist_info).rgb_on[0] = 1
		0:  (*diva_hist_info).rgb_on[0] = 0
	ENDCASE
	widget_control,(*diva_hist_info).col_butts[0],sensitive=1
	widget_control,(*diva_hist_info).col_butts[1],sensitive=1
	widget_control,(*diva_hist_info).col_butts[2],sensitive=1
	;call 'diva_hist_display' to plot histogram
    diva_hist_display,diva_hist_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"COL_G":			$
BEGIN
	 CASE event.select OF
		1:  (*diva_hist_info).rgb_on[1] = 1
		0:  (*diva_hist_info).rgb_on[1] = 0
	ENDCASE
	widget_control,(*diva_hist_info).col_butts[0],sensitive=1
	widget_control,(*diva_hist_info).col_butts[1],sensitive=1
	widget_control,(*diva_hist_info).col_butts[2],sensitive=1
	;call 'diva_hist_display' to plot histogram
    diva_hist_display,diva_hist_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"COL_B":			$
BEGIN
	 CASE event.select OF
		1:  (*diva_hist_info).rgb_on[2] = 1
		0:  (*diva_hist_info).rgb_on[2] = 0
	ENDCASE
	widget_control,(*diva_hist_info).col_butts[0],sensitive=1
	widget_control,(*diva_hist_info).col_butts[1],sensitive=1
	widget_control,(*diva_hist_info).col_butts[2],sensitive=1
	;call 'diva_hist_display' to plot histogram
    diva_hist_display,diva_hist_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

;PIXEL_HILITE_PIXEL_HILITE_PIXEL_HILITE_PIXEL_HILITE_PIXEL_HILITE_PIXEL_HILITE_
;PIXEL_HILITE_PIXEL_HILITE_PIXEL_HILITE_PIXEL_HILITE_PIXEL_HILITE_PIXEL_HILITE_
;PIXEL_HILITE_PIXEL_HILITE_PIXEL_HILITE_PIXEL_HILITE_PIXEL_HILITE_PIXEL_HILITE_
;PIXEL_HILITE_PIXEL_HILITE_PIXEL_HILITE_PIXEL_HILITE_PIXEL_HILITE_PIXEL_HILITE_
"DO_HILITE":		$
BEGIN
	widget_control,(*diva_hist_info).diva_hilite_input_value, 				$
													get_value=s_hilite_value
	hilite_value = long(s_hilite_value[0])
	
	maxv 	= max( (*(*diva_hist_info).bins) )
	IF (hilite_value gt maxv) THEN BEGIN
		widget_control,(*diva_hilite_info).diva_hilite_input_value, 		$
									set_value = strcompress(max_pimage,/rem)
		hilite_value = maxv
	ENDIF
	
	(*diva_hist_info).hilite_value = hilite_value
	
	
	(*diva_hist_info).hilite_fill_coords=[	hilite_value-hilite_range,		$
											hilite_value+hilite_range	]
	
	
	diva_hist_display,diva_hist_info
	diva_hilite_display,diva_hist_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DO_RANGE":			$
BEGIN
	widget_control,(*diva_hist_info).hilite_range_id,get_value=s_hilite_range
	hilite_range = long(s_hilite_range[0])
	
	(*diva_hist_info).hilite_range = hilite_range

	(*diva_hist_info).hilite_fill_coords=[	hilite_value-hilite_range,		$
											hilite_value+hilite_range	]
	
	diva_hist_display,diva_hist_info
	diva_hilite_display,diva_hist_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DO_COLOR":			$
BEGIN
	colors=['Yellow','White','Black','Red','Green','Blue','Cyan']
	(*diva_hist_info).hilite_color = colors[event.index]
	
	diva_hist_display,diva_hist_info
	diva_hilite_display,diva_hist_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DO_HILITE_OPER":	$
BEGIN
	(*diva_hist_info).hilite_oper = event.index
	maxv 	= max( (*(*diva_hist_info).bins) )
	
	CASE event.index OF
		0:	BEGIN
				widget_control,(*diva_hist_info).hilite_range_id,sensitive=1
				(*diva_hist_info).hilite_fill_coords =						$
										[	hilite_value-hilite_range,		$
											hilite_value+hilite_range	]
			END
		1:	BEGIN
				widget_control,(*diva_hist_info).hilite_range_id,sensitive=0
				(*diva_hist_info).hilite_fill_coords = [0,hilite_value]
			END
		2:	BEGIN
				widget_control,(*diva_hist_info).hilite_range_id,sensitive=0
				(*diva_hist_info).hilite_fill_coords = [hilite_value,maxv]
			END
	ENDCASE

	diva_hist_display,diva_hist_info
	diva_hilite_display,diva_hist_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"H_BLINK":			$
BEGIN
	(*diva_hist_info).h_blink_on = 10
	diva_hilite_display,diva_hist_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
;THRESHOLDING_THRESHOLDING_THRESHOLDING_THRESHOLDING_THRESHOLDING_THRESHOLDING
;THRESHOLDING_THRESHOLDING_THRESHOLDING_THRESHOLDING_THRESHOLDING_THRESHOLDING
;THRESHOLDING_THRESHOLDING_THRESHOLDING_THRESHOLDING_THRESHOLDING_THRESHOLDING
;THRESHOLDING_THRESHOLDING_THRESHOLDING_THRESHOLDING_THRESHOLDING_THRESHOLDING
"DO_THRESH_MIN":	$
BEGIN
	widget_control,(*diva_hist_info).diva_thresh_min_value, 				$
													get_value=s_thresh_min
	thresh_min = long(s_thresh_min[0])
	
	minv 		= min( (*(*diva_hist_info).bins) )
	thresh_max	= (*diva_hist_info).thresh_values[1]
	
	IF (thresh_min ge thresh_max) 	THEN thresh_min = thresh_max - 1
	IF (thresh_min le minv) 		THEN thresh_min = minv 
	
	
	widget_control,(*diva_hist_info).diva_thresh_min_value, 				$
							set_value=strcompress(LONG(thresh_min),/rem)
	
	(*diva_hist_info).thresh_values[0] = thresh_min
	
	diva_hist_display,diva_hist_info
	diva_hist_thresh_display,diva_hist_info

	
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DO_THRESH_MAX":	$
BEGIN
	widget_control,(*diva_hist_info).diva_thresh_max_value, 				$
													get_value=s_thresh_max
	thresh_max = long(s_thresh_max[0])
	
	maxv 		= max( (*(*diva_hist_info).bins) )
	thresh_min	= (*diva_hist_info).thresh_values[0]
	
	IF (thresh_max le thresh_min) 	THEN thresh_max = thresh_min + 1
	IF (thresh_max ge maxv) 		THEN thresh_max = maxv
	
	
	widget_control,(*diva_hist_info).diva_thresh_max_value, 				$
							set_value=strcompress(LONG(thresh_max),/rem)
	
	(*diva_hist_info).thresh_values[1] = thresh_max
	
	diva_hist_display,diva_hist_info
	diva_hist_thresh_display,diva_hist_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"SAVE_BITMASK": 	$
BEGIN
	(*diva_hist_info).save_bitmask_on = 1
	
	diva_hist_display,diva_hist_info
	diva_hist_thresh_display,diva_hist_info
	
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


				
ELSE: MESSAGE, "Event User Value Not Found"
ENDCASE
END ;end of "diva_hist" event handler------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_hist, wot_image,pdiva_draw,diva_histbase,GROUP = GROUP
;------------------------------------------------------------------------------;
; Purpose -
;------------------------------------------------------------------------------;
IF(XRegistered("diva_hist") NE 0) THEN RETURN

CASE wot_image OF
	'current':	BEGIN
					diva_image	 	= (*pdiva_draw).diva_current_image
					wot_drawarea	= (*pdiva_draw).diva_tlb_drawwid
				END
	'roi_area': BEGIN
					diva_image		= (*pdiva_draw).diva_image
					wot_drawarea	= (*pdiva_draw).draw_area
				END
	ELSE:		stop
ENDCASE

n_dim				= diva_image.n_dim
;calculate histogram(s) here. First, get image.....
o_pimage			= LONG(*diva_image.o_pimage)

;calculate the min and max values of the image
minval		= long(min(o_pimage[*]))
maxval		= long(max(o_pimage[*]))


;create array for "hist" - also calculate min/max pixel values 
hist 		= LONARR(maxval+1,n_dim)

minmax_f	= strarr(n_dim)

FOR i = 0,n_dim - 1 DO BEGIN
	hist[*,i] 	= HISTOGRAM(o_pimage[*,*,i],min=0L,max=maxval) 
	minmax_f[i] = strcompress(long(min(o_pimage[*,*,i])),/rem)  + ':' +		  $
				  strcompress(long(max(o_pimage[*,*,i])),/rem)
ENDFOR

bins 				= FINDGEN(N_ELEMENTS(hist[*,0]))

if (wot_image eq 'roi_area') THEN BEGIN
	diva_roi_mag = (*(*pdiva_draw).diva_roi_info).diva_roi_mag

	hist = hist / (diva_roi_mag)^2
END


dhcoords 			= [0,0,max(bins),0]

o_pimage_median 	= ROUND(MEDIAN(bins))
o_pimage_median_5	= ROUND(o_pimage_median * .01)



;-------------------create the main base---------------------------------------;
diva_histbase 				= WIDGET_BASE(TITLE = "DIVA IMAGE HISTOGRAM" +  $
									"   [ n_dim = " + 						$
									strcompress(n_dim,/rem) + 				$
									" ]", /col,group_leader=GROUP,/floating)

;------------------------------------------------------------------------------;
;																			   ;
;								TOP DROPLIST BASE 							   ;
;																			   ;
;------------------------------------------------------------------------------;
top_droplist_base			= widget_base(diva_histbase,/row,/frame)


;plot linear/log scaling droplist
plot_loglin_droplist 		= widget_droplist(top_droplist_base,			$
									value=['Linear','Log'], 				$
									uvalue = 'LIN_LOG',						$
									title = 'Plot:',						$
									/frame)

;plot empty bins/missing codes droplist
plot_emptybins_droplist 	= widget_droplist(top_droplist_base,			$
									value=['Off','On'], 					$
									uvalue = 'E_BINS',						$
									title = 'Empty Bins:',					$
									/frame)


;smooth the histogram droplist
smooth_list_list			= ['none','3','5','7']
smooth_list 				= widget_droplist(top_droplist_base,			$
									title='Smoothing:',						$
									uvalue='SMOOTH',  						$
    								value=smooth_list_list, 				$
									/frame)

;function selection droplist
func_list_list				= ['none','thresholding','pixel value', 		$
									'equalisation']
func_list					= widget_droplist(top_droplist_base,			$
									title='Function:',						$
									uvalue='FUNCTION',    					$
    								value=func_list_list,					$
									/frame)

IF (n_dim ne 1) THEN widget_control,func_list,sensitive = 0
;------------------------------------------------------------------------------;
;																			   ;
;								TOP COLOUR BASE 							   ;
;																			   ;
;------------------------------------------------------------------------------;
top_color_base				= widget_base(diva_histbase,/row,/frame)

colbase_rgb 				= widget_base(top_color_base,/row,/frame,		$
									/nonexclusive)

col_r_button				= widget_button(colbase_rgb,					$
									value=' R ',uvalue='COL_R')
col_g_button				= widget_button(colbase_rgb,					$
									value=' G ',uvalue='COL_G')
col_b_button				= widget_button(colbase_rgb,					$
									value=' B ',uvalue='COL_B')

widget_control,col_r_button,set_button = 1
widget_control,col_g_button,set_button = 1
widget_control,col_b_button,set_button = 1

col_butts  = [col_r_button,col_g_button,col_b_button]

if (n_dim eq 1) then widget_control,colbase_rgb,sensitive = 0
;------------------------------------------------------------------------------;
;																			   ;
;								DRAW & SIDE BASE							   ;
;																			   ;
;------------------------------------------------------------------------------;
draw_and_side_base			= widget_base(diva_histbase,/row,/frame)
;------------------------------------------------------------------------------;
;																			   ;
;									DRAW BASE								   ;
;																			   ;
;------------------------------------------------------------------------------;
diva_hist_draw_base 		= widget_base(draw_and_side_base,/col)

diva_hist_draw  			= widget_draw(draw_and_side_base,				$
									xsize=500,								$
									ysize=320,						        $
									frame=5,    					        $
    	    						/motion_events, 				        $
									Button_Events=1,				        $
									/viewport_events,				        $
									uvalue='HISTDRAW')
;make and store pixmap
window,/free,/pixmap,xsize=500,ysize=320
diva_hist_index = !d.window
;------------------------------------------------------------------------------;
;																			   ;
;									SIDE BASE								   ;
;																			   ;
;------------------------------------------------------------------------------;
diva_hist_side_base 		= widget_base(draw_and_side_base,/frame)

;------------------------------------------------------------------------------;
;																			   ;
;							   THRESHOLDING BASE							   ;
;																			   ;
;------------------------------------------------------------------------------;
diva_side_thresh_base		= widget_base(diva_hist_side_base,				$
									/col,map=0)

thresh_values 				= [ o_pimage_median - o_pimage_median_5,		$
								o_pimage_median + o_pimage_median_5	]

diva_thresh_label_min 		= widget_label(diva_side_thresh_base,			$
									value='Set min value:',/align_left)
									
diva_thresh_min_value 		= widget_text(diva_side_thresh_base,			$
									/editable,								$
									value=strcompress(thresh_values[0],/rem),$
    	    						uvalue='DO_THRESH_MIN',xsize=8)


diva_thresh_label_max 		= widget_label(diva_side_thresh_base,			$
									value='Set max value:',/align_left)
									
diva_thresh_max_value 		= widget_text(diva_side_thresh_base,			$
									/editable,								$
									value=strcompress(thresh_values[1],/rem),$
    	    						uvalue='DO_THRESH_MAX',xsize=8)

diva_thresh_count_label_1	= widget_label(diva_side_thresh_base,			$
									value='No. of pixels:',/align_left)

diva_thresh_count 			= widget_label(diva_side_thresh_base,			$
									value='?',xsize=100)								


diva_thresh_save_butt		= widget_button(diva_side_thresh_base,			$
									value = ' Save bitmask ', 				$
									uvalue = 'SAVE_BITMASK',ysize=50)
;------------------------------------------------------------------------------;
;																			   ;
;							 PIXEL HILITE BASE								   ;
;																			   ;
;------------------------------------------------------------------------------;
diva_side_hilite_base		= widget_base(diva_hist_side_base,				$
									/col,map=0)

diva_hilite_label_1 		= widget_label(diva_side_hilite_base,			$
									value='Set value:',/align_left)
									
diva_hilite_input_value 	= widget_text(diva_side_hilite_base,			$
									/editable,								$
									value=strcompress(o_pimage_median,/rem),$
    	    						uvalue='DO_HILITE',xsize=8)
									
hilite_oper_id				= widget_droplist(diva_side_hilite_base,		$
									uvalue='DO_HILITE_OPER',  	    		$
    	    						value=['  +/-  ','  < ','  > '],		$
									/align_left)									
									
hilite_range_id 			= widget_text(diva_side_hilite_base,			$
									/editable,								$
									value=strcompress(o_pimage_median_5,/rem),$
									uvalue='DO_RANGE',	$
									xsize=8,sensitive=1)
									
diva_hilite_label_2			= widget_label(diva_side_hilite_base,			$
									value='Set coloUr:',/align_left)

diva_hilite_color 			= widget_droplist(diva_side_hilite_base,		$
									value=['Yellow','White','Black',		$
									'Red','Green','Blue','Cyan'],			$
									uvalue='DO_COLOR')

diva_hilite_label_3			= widget_label(diva_side_hilite_base,			$
									value='Value range:',/align_left)

diva_hilite_label_4			= widget_label(diva_side_hilite_base,			$
									value='1000:1000',xsize=100)
									
diva_hilite_label_5			= widget_label(diva_side_hilite_base,			$
									value='No. of pixels:',/align_left)

diva_hilite_label_6			= widget_label(diva_side_hilite_base,			$
									value='?',xsize=100)
									
diva_hilite_label_ids		= [diva_hilite_label_4,diva_hilite_label_6]

diva_hilite_blink			= widget_button(diva_side_hilite_base,			$
									value='BLINK', 							$
									uvalue='H_BLINK')
;------------------------------------------------------------------------------;
;																			   ;
;								 BOTTOM BASE								   ;
;																			   ;
;------------------------------------------------------------------------------;
buttbase 					= widget_base(diva_histbase,/col)


;------------------------------------------------------------------------------;
;																			   ;
;							  NAVIGATION BASE								   ;
;																			   ;
;------------------------------------------------------------------------------;
navigation_base 			= widget_base(buttbase,/row)

infobase 					= widget_base(navigation_base,/row,/frame)
info_label  				= widget_label(infobase,value='coords:')
infoxy_id 					= widget_text(infobase,value='0,0',xsize=12)


go_base 					= widget_base(navigation_base,/row,/frame,		$
									/grid_layout)
go_all_left 				= widget_button(go_base,value=' << ',			$
									uvalue='GO_ALL_L', 						$
									sensitive=0)
go_left 					= widget_button(go_base,value='<',				$
									uvalue='GO_L',							$
									sensitive=0)
go_right					= widget_button(go_base,value='>',				$
									uvalue='GO_R',							$
									sensitive=0)
go_all_right				= widget_button(go_base,value=' >> ',			$
									uvalue='GO_ALL_R', 						$
									sensitive=0)

range_base  				= widget_base(navigation_base,/row,/frame,		$
									/grid_layout)
range_dble  				= widget_button(range_base,value=' x2 ',		$
									uvalue='R_DBLE',						$
									sensitive=0)
range_half  				= widget_button(range_base,value=' /2 ',		$
									uvalue='R_HALF',						$
									sensitive=0)

go_buttons  	= [go_all_left,go_left,go_right,go_all_right,				$
													range_dble,range_half]

;------------------------------------------------------------------------------;
;																			   ;
;								   INFO BASE								   ;
;																			   ;
;------------------------------------------------------------------------------;
info_base 					= widget_base(buttbase,/row)

minmaxbase					= widget_base(info_base,/row,/frame)
minmax_label 				= widget_label(minmaxbase,						$
									value='Frequency min:max')

minmax_id 					= widget_text(minmaxbase,xsize=30)

minmax_f_base				= widget_base(info_base,/row,/frame)
minmax_f_label 				= widget_label(minmax_f_base,					$
									value='Pixel Value min:max')

minmax_f_id 				= widget_text(minmax_f_base,xsize=20)

;------------------------------------------------------------------------------;
;																			   ;
;								 BUTTON BASE								   ;
;																			   ;
;------------------------------------------------------------------------------;
button_base 				= widget_base(buttbase,/row,/grid_layout,/frame)

print_button 				= widget_button(button_base,					$
									value='Print',							$
									uvalue='HIST_PS')
									
data_button 				= widget_button(button_base,					$
									value='Save data to file', 				$
									uvalue='HIST_PRINT')

;-------------------quit button------------------------------------------------;
quitbutton					= widget_button(diva_histbase,					$
									value='Quit',uvalue='EXIT',/frame)
;------------------------------------------------------------------------------;

;-------------------realise the widget-----------------------------------------;
WIDGET_CONTROL, diva_histbase, /REALIZE


;create "diva_hist_info" structure and reference it as a pointer
diva_printers		= (*pdiva_draw).diva_printers
diva_printers_list	= (*pdiva_draw).diva_printers_list

;report min/max pixel values 
minmax_f_str=minmax_f[0]
IF (n_dim gt 1) THEN BEGIN
	FOR i=1,n_dim-1 DO minmax_f_str = minmax_f_str + ',' + minmax_f[i]
ENDIF
widget_control,minmax_f_id,set_value = minmax_f_str

;-------------------initialise "diva_hist_info"--------------------------------;
CASE n_dim OF
	1:		rgb_on = [0,0,0]
	3:		rgb_on = [1,1,1]
ENDCASE


diva_hist_info = Ptr_New({  												$
					pdiva_draw	    		:   pdiva_draw,     	        $
					diva_image				:	Ptr_New(diva_image), 		$
					n_dim					:	n_dim,						$
    	    	    diva_histbase   		:   diva_histbase,  	        $
			    	linplot_on	    		:   1,	    	    	        $
    	    	    ebin_on 	    	    :	0,  					    $
			    	smooth_list_list	    :	smooth_list_list,		    $
			    	smooth_list_value	    :	'none', 				    $
			    	func_list_list    	    :	func_list_list, 		    $
			    	func_list_value   	    :	0, 				    		$
			    	diva_hist_draw  	    :	diva_hist_draw, 		    $
			    	diva_hist_index 	    :	diva_hist_index,		    $
			    	infoxy_id	    	    :	infoxy_id,  			    $
			    	minmax_id	    	    :	minmax_id,  			    $
					minmax_f_id 			:	minmax_f_id,				$
			    	hist    	    	    :	Ptr_New(hist),				$
			    	bins    	    	    :	Ptr_New(bins),				$
					n_hist					:	Ptr_New(),					$
					n_bins					:	Ptr_New(),					$
			    	dhcoords  	    	    :	dhcoords,				    $
			    	hist_motion_evs_on	    :	0,  					    $
			    	go_buttons	    	    :	go_buttons,  			    $
					diva_printers			:	diva_printers,			    $
					diva_printers_list		:	diva_printers_list,		    $
					rgb_on					:	rgb_on, 					$
					col_butts				:	col_butts,					$
					diva_side_hilite_base	:	diva_side_hilite_base,		$
					hilite_oper 			:	0, 							$
					hilite_range_id 		:	hilite_range_id, 			$
					hilite_color    		:   'Yellow',					$
					hilite_range			:	o_pimage_median_5,			$
					diva_hilite_label_ids	:	diva_hilite_label_ids,		$
					diva_hilite_input_value :	diva_hilite_input_value, 	$
					hilite_value    		:   o_pimage_median,			$
					hilite_fill_coords 	:		[0L,0], 					$
					h_blink_on				:	0,							$
					diva_side_thresh_base	:	diva_side_thresh_base,		$
					diva_thresh_min_value 	:	diva_thresh_min_value, 		$
					diva_thresh_max_value 	:	diva_thresh_max_value, 		$
					thresh_values			: 	thresh_values, 				$
					diva_thresh_count		:	diva_thresh_count,			$
					wot_image				:	wot_image,					$
					wot_drawarea			:	wot_drawarea,				$
					save_bitmask_on 		:	0							$
				})



;call 'diva_hist_display' to plot histogram
diva_hist_display,diva_hist_info

widget_control,diva_histbase,set_uvalue=diva_hist_info

XManager, "diva_hist", diva_histbase,EVENT_HANDLER = "diva_hist_ev", 	    $
		GROUP_LEADER = GROUP,cleanup='diva_hist_cleanup'

END ;end of "diva_hist" main routine-------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_hist_cleanup,diva_histbase

widget_control,diva_histbase,get_uvalue=diva_hist_info
if (Ptr_Valid(diva_hist_info) eq 1) then begin

	CASE (*diva_hist_info).wot_image OF
		'current':	BEGIN
						diva_draw_image,(*diva_hist_info).pdiva_draw
					END
		'roi_area': BEGIN
						Ptr_Free,(*(*diva_hist_info).pdiva_draw).			$
														diva_image.o_pimage
						Ptr_Free,(*diva_hist_info).pdiva_draw
					END
		ELSE:		stop
	ENDCASE
	
	Ptr_Free,(*diva_hist_info).hist
	Ptr_Free,(*diva_hist_info).bins
	Ptr_Free,(*diva_hist_info).n_hist
	Ptr_Free,(*diva_hist_info).n_bins
	Ptr_Free,(*diva_hist_info).diva_image
	Ptr_FRee,diva_hist_info
	
endif 
end ;end of diva_hist CLEANUP routine------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_hist_display,diva_hist_info
;------------------------------------------------------------------------------;
; Purpose -
;------------------------------------------------------------------------------;

temph 	    	    = (*diva_hist_info)
n_bins	    	    = (*temph.bins)
n_hist	    	    = (*temph.hist)
dhcoords			= temph.dhcoords
;OK, this is the complicated bit!!  - where data coords are converted into
;element indicies
nlinex	    	    = [dhcoords[0]-n_bins[0],dhcoords[2]-n_bins[0]]
nlinex	    	    = nlinex(sort(nlinex))
n_bins				= n_bins[nlinex[0]:nlinex[1]]
n_hist				= n_hist[nlinex[0]:nlinex[1],*]
;phew, glad that's over!!!!!


n_dim				= temph.n_dim
rgb_on      		= temph.rgb_on
col_butts   		= temph.col_butts
minmax_id			= temph.minmax_id
linplot_on  	    = temph.linplot_on
smooth_list_value   = temph.smooth_list_value
ebin_on     	    = temph.ebin_on

hilite_value 		= temph.hilite_value
hilite_range 		= temph.hilite_range
hilite_color 		= temph.hilite_color
hilite_oper 		= temph.hilite_oper
func_list_value 	= temph.func_list_value
hilite_fill_coords	= temph.hilite_fill_coords
thresh_values		= temph.thresh_values

tempd	    	    = (*temph.diva_image)
i_mean      	    = tempd.i_mean
i_stddev    	    = tempd.i_stddev


image_ct    	    = (*temph.pdiva_draw).image_ct
plot_ct     	    = (*temph.pdiva_draw).plot_ct

;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
;call "diva_hist_ops" procedure 
diva_hist_ops,n_hist,linplot_on,smooth_list_value,n_dim,ebin_on,		$
													empty_bins_hist

;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
;refresh pointers
Ptr_Free,(*diva_hist_info).n_bins
Ptr_Free,(*diva_hist_info).n_hist
(*diva_hist_info).n_bins  = Ptr_New(n_bins)
(*diva_hist_info).n_hist  = Ptr_New(n_hist)

;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
;load plot colour table
tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2]

;OK, we need to plot twice...one to the pixmap and one to the draw area
;1  - first the pixmap...........................
wset,(*diva_hist_info).diva_hist_index

;call 'diva_hist_plot' to plot data
diva_hist_plot,n_hist,n_bins,empty_bins_hist,linplot_on,ebin_on,   	    $
    	i_mean,i_stddev,rgb_on,n_dim,hilite_value,						$ 
		hilite_range,hilite_color,func_list_value,hilite_fill_coords,	$
		hilite_oper,thresh_values
		
		
		

;.....and secondly to the actual histogram draw arae
widget_control,(*diva_hist_info).diva_hist_draw,get_value = hist_draw
wset, hist_draw

;call 'diva_hist_plot' to plot data
diva_hist_plot,n_hist,n_bins,empty_bins_hist,linplot_on,ebin_on,   	    $
    	i_mean,i_stddev,rgb_on,n_dim,hilite_value,						$ 
		hilite_range,hilite_color,func_list_value,hilite_fill_coords,	$
		hilite_oper,thresh_values

;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
;revert back to image colour table
tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2]

;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
;report FREQUENCY min and max values of new plot range
minmax_str=strarr(n_dim)
FOR i=0,n_dim-1 DO BEGIN
	minmax_str[i] = strcompress(long(min(n_hist[*,i])),/rem)	+ ':' + 		$
					strcompress(long(max(n_hist[*,i])),/rem)
ENDFOR

minmax_mult_str=minmax_str[0]
IF (n_dim gt 1) THEN BEGIN
	FOR i=1,n_dim-1 DO minmax_mult_str = minmax_mult_str + ',' + minmax_str[i]
ENDIF

widget_control,minmax_id,set_value = minmax_mult_str

;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
;ensure at least one channel is selected - this last one is
;de-sensitized until another channel is selected
IF (fix(total(rgb_on)) eq 1) THEN BEGIN
	index = where(rgb_on ne 0)
	widget_control,col_butts[index],sensitive = 0
ENDIF


end; of 'diva_hist_display' pro------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_hist_plot,hist,bins,empty_bins_hist,linplot_on,ebin_on,	    $
    	i_mean,i_stddev,rgb_on,n_dim,hilite_value,						$ 
		hilite_range,hilite_color,func_list_value,hilite_fill_coords,	$
		hilite_oper,thresh_values
;------------------------------------------------------------------------------;
; Purpose -
;------------------------------------------------------------------------------;
CASE hilite_color OF
	'Yellow':	h_color = 7
	'White':    h_color = 5
	'Black':    h_color = 6
	'Red':	    h_color = 1
	'Green':    h_color = 2
	'Blue':     h_color = 3
	'Cyan':     h_color = 4
ENDCASE
;stop
;get ymin and ymax - plot this range
wot_size = FIX(TOTAL(rgb_on))
IF (wot_size lt 1) THEN wot_size = 1	;in the case of n_dim = 1

index = WHERE(rgb_on ne 0)
IF (TOTAL(index) lt 0) THEN index = 0	;in the case of n_dim = 1

yminmax = LONARR(wot_size,2)
FOR i	= 0,wot_size - 1 DO BEGIN
	yminmax[i,0]	=	MIN(hist[*,index[i]])
	yminmax[i,1]	=	MAX(hist[*,index[i]])
ENDFOR
ymin = MIN(yminmax[*,0]) 
ymax = MAX(yminmax[*,1])

IF (linplot_on eq 0) then BEGIN
	ymin = ymin > .1
	ymax = ymax > .1
ENDIF

CASE linplot_on OF
    1:	BEGIN
    	    plot,bins,hist,xtitle='Pixel value',yr=[ymin,ymax], 			$
    	    	charsize=1.3,ytit='Frequency',/nodata
		END
    0:	BEGIN
    	    plot_io,bins,hist,xtitle='Pixel value', yr=[ymin,ymax], 		$
	    		charsize=1.3,ytit='Frequency',/nodata
			
			!y.crange[0]=10^!y.crange[0] & !y.crange[1]=10^!y.crange[1]
		END
ENDCASE


CASE n_dim OF
	3:	BEGIN
			FOR i = 0, n_dim - 1 DO BEGIN
	
				IF (rgb_on[i] eq 1) THEN BEGIN
	    			IF (ebin_on eq 1) THEN oplot,bins,						$
													empty_bins_hist[*,i], 	$
													psym  = 10,				$
													color = i+1
					
					oplot,bins,hist[*,i],psym = 10,color = i+1,thick = 2
	    			oplot,[i_mean[i],i_mean[i]],[ymin,!y.crange[1]],		$
												thick = 2,color = i+1
	    			oplot,[i_mean[i]+i_stddev[i],i_mean[i]+i_stddev[i]],	$
	    	    	    	    				[ymin,!y.crange[1]],		$
												lines = 2,color = i+1
	    			oplot,[i_mean[i]-i_stddev[i],i_mean[i]-i_stddev[i]],   	$
	    	    	    	    				[ymin,!y.crange[1]],		$
												lines = 2,color = i+1
				END
	
			ENDFOR
		END
	1:	BEGIN
		  	IF (ebin_on eq 1) THEN											$
							oplot,bins,empty_bins_hist[*,0],psym=10,color=4
						
			oplot,bins,hist,psym=10,color=2,thick=2
		  	oplot,[i_mean,i_mean],[!y.crange[0],!y.crange[1]],thick=2
		  	oplot,[i_mean+i_stddev,i_mean+i_stddev],						$
										[ymin,!y.crange[1]],lines=2
		  	oplot,[i_mean-i_stddev,i_mean-i_stddev],						$
										[ymin,!y.crange[1]],lines=2
			
			
			
			CASE func_list_value OF
				1:	BEGIN
						oplot,[thresh_values[0],thresh_values[0]],			$
								[ymin,!y.crange[1]],						$
								color=7,thick=2
						oplot,[thresh_values[1],thresh_values[1]],			$
								[ymin,!y.crange[1]],						$
								color=7,thick=2
					END
				2:	BEGIN
						oplot,[hilite_value,hilite_value],					$
								[ymin,!y.crange[1]],						$
								color=h_color,thick=2
						IF (hilite_oper eq 0) THEN BEGIN
							oplot,[hilite_value-hilite_range,				$	
									hilite_value-hilite_range],				$	
									[ymin,!y.crange[1]],					$	
									color=h_color,thick=1
							oplot,[hilite_value+hilite_range,				$	
									hilite_value+hilite_range],				$	
									[ymin,!y.crange[1]],					$	
									color=h_color,thick=1
						ENDIF
						IF (hilite_range gt 0) THEN 						$
							polyfill,[hilite_fill_coords[0],				$
									hilite_fill_coords[0],					$
									hilite_fill_coords[1],					$
									hilite_fill_coords[1]],					$
		 							[ymin,!y.crange[1], 					$
									!y.crange[1],!y.crange[0]], 			$
									color=h_color,/line_fill,				$
									orientation=45,noclip=0

					END
				ELSE:	stop
			ENDCASE
										
		END
ENDCASE

END
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_hist_ops,n_hist,linplot_on,smooth_list_value,n_dim,ebin_on,		$
													empty_bins_hist
;------------------------------------------------------------------------------;
; Purpose -
;------------------------------------------------------------------------------;
;Linear/Log plot bit!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
if (linplot_on eq 0) then n_hist = float(n_hist) > .1
;Smoothing bit !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
IF (smooth_list_value ne 'none') THEN BEGIN
	FOR i=0,n_dim-1 DO BEGIN
		n_hist[*,i]	= smooth(n_hist[*,i],fix(smooth_list_value))
	ENDFOR
ENDIF
;Empty bins bit!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
;copy 'hist' to a new array, and set all elements to zero
if (ebin_on eq 1) then begin
    empty_bins_hist 	= n_hist
    empty_bins_hist[*]	= !y.crange[0]
    ;need to check whether empty bin values were set to 0.1 (during) log plot
    if (min(n_hist) lt 1) then begin
		;find nonzero elements of 'hist', and give them a value of unity
		nonzero_elements					= where(n_hist eq min(n_hist))
		empty_bins_hist[nonzero_elements]	= !y.crange[1]
    endif
end

END
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_hist_sensitize,go_buttons,ie
;------------------------------------------------------------------------------;
; Purpose - to sensitize/desensitize histogram's navigation buttons
;------------------------------------------------------------------------------;
FOR i=0,5 DO widget_control,go_buttons[i],sensitive=ie[i]
END;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_hilite_display,diva_hist_info
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;

temph 	    	        = (*diva_hist_info)
tempd	    	        = (*temph.diva_image)
image_ct    	    	= (*temph.pdiva_draw).image_ct
plot_ct     	    	= (*temph.pdiva_draw).plot_ct
autoscale				= (*temph.pdiva_draw).diva_autoscale
pimage				    = (*temph.diva_image).o_pimage
hilite_value		    = temph.hilite_value
hilite_range		    = temph.hilite_range
hilite_color		    = temph.hilite_color
hilite_oper 		    = temph.hilite_oper
diva_hilite_label_ids 	= temph.diva_hilite_label_ids
h_blink_on				= temph.h_blink_on
diva_hist_draw			= temph.diva_hist_draw
diva_hist_index			= temph.diva_hist_index
draw_area				= temph.wot_drawarea
wot_image				= temph.wot_image

widget_control,/hourglass


display = BytScl(*pimage, Top=!D.Table_Size-2)

lower 	= (hilite_value - hilite_range) > 0
upper 	= (hilite_value + hilite_range) < max(*pimage)

CASE hilite_oper OF
	0:	BEGIN
			li = where((*pimage ge lower) and (*pimage le upper),count)
			if (hilite_range eq 0) then li = 								$
									Where(*pimage eq hilite_value,count)	
		END
	1:	BEGIN
			li		= where(*pimage lt hilite_value,count)
			lower 	= 0
			upper 	= hilite_value - 1
		END
	2:	BEGIN
			li		= where(*pimage gt hilite_value,count)
			lower 	= hilite_value + 1
			upper 	= max(*pimage)
		END
ENDCASE



;call "diva_hilite_set_ct" procedure to set colour table
diva_hilite_set_ct,hilite_color



CASE count OF
	0:		BEGIN
				widget_control,diva_hilite_label_ids[1],set_value='0'
			END
	ELSE:	BEGIN
				display[li] = !D.Table_Size-1
				if (wot_image eq 'roi_area') THEN BEGIN
					
					diva_roi_mag = (*(*(*diva_hist_info).pdiva_draw).		$
												diva_roi_info).diva_roi_mag
					
					
					If (count le 4) AND (count gt 0) THEN count = 1 		$
					ELSE  count = count / (diva_roi_mag)^2
					
				ENDIF
				widget_control,diva_hilite_label_ids[1],					$
											set_value=strcompress(count,/rem)
			END
ENDCASE

widget_control,draw_area,get_value=draw_area
wset,draw_area
diva_hilite_plot,autoscale,display


FOR i=0,h_blink_on-1 DO BEGIN
	wait,.1
	tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2]
	diva_hilite_plot,autoscale,*pimage
	
	wait,.1
	;call "diva_hilite_set_ct" procedure to set colour table
	diva_hilite_set_ct,hilite_color
	diva_hilite_plot,autoscale,display
ENDFOR

(*diva_hist_info).h_blink_on = 0

tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2]

widget_control,diva_hilite_label_ids[0],									$
						set_value = strcompress(lower,/rem) + ':' + 		$
									strcompress(upper,/rem)

END
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_hilite_set_ct,hilite_color
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
CASE hilite_color OF
	'Yellow':   tvlct,255,255,0  , !D.Table_Size-1
	'White':    tvlct,255,255,255, !D.Table_Size-1
	'Black':    tvlct,0  ,0  ,0  , !D.Table_Size-1
	'Red':	    tvlct,255,0  ,0  , !D.Table_Size-1
	'Green':    tvlct,0  ,255,0  , !D.Table_Size-1
	'Blue':     tvlct,0  ,0  ,255, !D.Table_Size-1
	'Cyan':     tvlct,0  ,255,255, !D.Table_Size-1
ENDCASE

END
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_hilite_plot,autoscale,display
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;

CASE autoscale[0] OF
    0:  tv,ishft(display,autoscale[1])
    1:  tvscl,(display)
ENDCASE


END
;------------------------------------------------------------------------------;
PRO diva_hist_thresh_display,diva_hist_info

temph 	    	        = (*diva_hist_info)
tempd	    	        = (*temph.diva_image)
pimage				    = (*temph.diva_image).o_pimage
thresh_values			= temph.thresh_values
diva_thresh_count		= temph.diva_thresh_count
draw_area				= temph.wot_drawarea
save_bitmask_on 		= temph.save_bitmask_on
image_ct    	    	= (*temph.pdiva_draw).image_ct
display 				= UINT(*pimage)


tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2]

upper = where(	(*pimage ge thresh_values[0]) AND  							$
				(*pimage le thresh_values[1]), complement=lower,count)

IF (n_elements(lower) eq 1) AND (lower[0] eq -1) then BEGIN
	diva_error,diva_thresh_count,10
	return
ENDIF

IF (count eq 0) THEN BEGIN
	autoscale = tempd.diva_autoscale
	widget_control,draw_area,get_value = draw_area
	wset,draw_area
	diva_hilite_plot,autoscale,display
	
	widget_control,diva_thresh_count,set_value = 'NONE!'
	return
ENDIF


display[lower]	= 0
display[upper]	= 1

IF (save_bitmask_on eq 1) THEN BEGIN
	
	diva_image  				= {diva_image}
	diva_image.file_info 		= tempd.file_info 
	diva_image.o_pimage 		= Ptr_New(display)
	diva_image.n_dim			= tempd.n_dim
	diva_image.mfile_name		= tempd.mfile_name
	diva_image.diva_n_images	= tempd.diva_n_images
	diva_image.mfile_name_str	= tempd.mfile_name_str
	
	f			= tempd.file_info.f
	index 		= 0
	wid_id  	= temph.diva_histbase
	func		= 'bitmask'
	bsname		= ''
	bshift_val	= 0
	
	comment_0	= ' Source file: ' + strcompress(f,/rem)
	comment_1	= ' This file was created by bitmasking  image #' + 		$
									 strcompress(index + 1,/rem) +			$
							' of ' + strcompress(tempd.diva_n_images,/rem)
	
	comments	= [comment_0,comment_1]
	
	;call "diva_make_fits_file" procedure to make a fits file
	diva_make_fits_file,diva_image,index,comments, 							$
											wid_id,func,bsname,bshift_val
	
ENDIF ELSE BEGIN
	(*diva_hist_info).save_bitmask_on = 0
	
	widget_control,draw_area,get_value=draw_area
	wset,draw_area

	tvscl,display
	
	widget_control,diva_thresh_count,set_value = strcompress(count,/rem)
ENDELSE

(*diva_hist_info).save_bitmask_on = 0

END

