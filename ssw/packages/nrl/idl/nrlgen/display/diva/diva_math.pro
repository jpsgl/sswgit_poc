;------------------------------------------------------------------------------;
PRO diva_math_ev, event
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
WIDGET_CONTROL, event.id, GET_UVALUE = 	eventval
widget_control,event.top,get_uvalue=diva_math_info

diva_op1_list_id   		=   (*diva_math_info).diva_op1_list_id
diva_op2_list_id 		=   (*diva_math_info).diva_op2_list_id
diva_math_sum_first 	=   (*diva_math_info).diva_math_sum_first
diva_math_sum_second	=   (*diva_math_info).diva_math_sum_second
diva_math_sum_doit  	=   (*diva_math_info).diva_math_sum_doit
diva_math_opr_label 	=   (*diva_math_info).diva_math_opr_label
diva_math_constant  	=   (*diva_math_info).diva_math_constant
diva_constant_opr   	=   (*diva_math_info).diva_constant_opr
diva_constant_val   	=   (*diva_math_info).diva_constant_val
diva_math_function  	=   (*diva_math_info).diva_math_function
diva_math_butt_remove	=   (*diva_math_info).diva_math_butt_remove
diva_math_op1_draw		=   (*diva_math_info).diva_math_op1_draw
diva_math_op2_draw		=   (*diva_math_info).diva_math_op2_draw
diva_math_op1_minmax	=	(*diva_math_info).diva_math_op1_minmax
diva_math_op2_minmax	=	(*diva_math_info).diva_math_op2_minmax
index	    	    	=   (*diva_math_info).index
operand_2_base			=	(*diva_math_info).operand_2_base
diva_math_sum_base		=	(*diva_math_info).diva_math_sum_base

CASE eventval OF

"EXIT": 		$	;
BEGIN
	WIDGET_CONTROL, event.top, /DESTROY	
	return	
END ;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"CONSTANT": 	$	;
BEGIN
    widget_control,diva_math_constant,get_value = sdiva_constant_val
	IF (sdiva_constant_val[0] eq '') THEN value = 0.
	
	value = FLOAT(sdiva_constant_val[0])
	
	(*diva_math_info).diva_constant_val = value

	widget_control,diva_math_sum_second,set_value 	= strcompress(value,/rem)
	widget_control,diva_math_constant,set_value 	= strcompress(value,/rem)
    widget_control,diva_math_sum_doit,sensitive 	= 1
END ;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DO_SUM":		$	;
BEGIN
	widget_control,/hourglass				    
    
	first	= (*(*diva_math_info).diva_math_first_pimage)
	
    first  = FLOAT(first)
    ;get dimensions of 1st image......
    first_image_size    = SIZE(first)
    first_xsize	    	= first_image_size[1]
    first_ysize	    	= first_image_size[2]

	;get second operand - whether image or constant value
	CASE diva_constant_opr OF
		0:		BEGIN
					;get dimensions of 2nd image......
					second = (*(*diva_math_info).diva_math_second_pimage)
					second = FLOAT(second)
					second_image_size	= size(second)
					second_xsize		= second_image_size[1]
					second_ysize		= second_image_size[2]
					;compare, if not the same, then complain and return.....
					IF (first_xsize ne second_xsize) OR 			$
					   (first_ysize ne second_ysize) THEN BEGIN
						   diva_error,diva_math_op1_draw,2
						   RETURN				
					ENDIF  ;otherwise, carry on!
				END
		ELSE:	BEGIN
					second = FLOAT(diva_constant_val)
				END
	ENDCASE
	
	;OK, do calculation...........................				 
	CASE diva_math_function OF
		0:		result_image	= 	first + second			;add
		1:		result_image	= 	first - second			;subtract
		2:		result_image	= 	first * second			;multiply
		3:		result_image	= 	first / second			;divide
		4:		result_image	= ( first + second	) / 2 	;average
	ENDCASE

	;save "result_image" pointer
    Ptr_free,(*diva_math_info).result_image
    (*diva_math_info).result_image  = Ptr_New(result_image)

	;call "diva_result" procedure (GUI)
	diva_result,diva_math_info,group = (*diva_math_info).diva_mathbase

END ;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"REMALL_BUTT": 	$	;MATH_REMOVE_ALL_MATH_REMOVE_ALL_MATH_REMOVE_ALL_MATH_R
BEGIN
	;DIVA_CONC!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	IF (XREGISTERED("diva_conc") eq 1) THEN BEGIN
		widget_control,(*diva_math_info).diva_concbase,/destroy
	ENDIF
	
	old_array 	= (*(*diva_math_info).diva_image_stored)
	n_elems 	= N_ELEMENTS(old_array)
    FOR i = 0, n_elems -1 DO BEGIN
		Ptr_Free,old_array[i].pimage
		Ptr_Free,old_array[i].o_pimage
		Ptr_Free,old_array[i].p_pimage
		Ptr_Free,old_array[i].s_pimage
	ENDFOR
	Ptr_Free,(*diva_math_info).diva_image_stored

	WIDGET_CONTROL, event.top, /DESTROY
	RETURN	
	
END ;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"REM_BUTT": 	$	;
BEGIN
    ;DIVA_CONC!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	IF (XREGISTERED("diva_conc") eq 1) THEN BEGIN
		widget_control,(*diva_math_info).diva_concbase,/destroy
	ENDIF
	
	old_array 	= (*(*diva_math_info).diva_image_stored)
    n_elems 	= N_ELEMENTS(old_array)
    last 		= n_elems - 1  ;ie, index of last element
    CASE n_elems OF
    	;if there's only one list entry,then remove it from
    	;list and destroy the image_math widget!!!!!!!!!!
		1:  BEGIN
	    		Ptr_Free,old_array.pimage
				Ptr_Free,old_array.o_pimage
				Ptr_Free,old_array.p_pimage
				Ptr_Free,old_array.s_pimage
				Ptr_Free,(*diva_math_info).diva_image_stored
				WIDGET_CONTROL, event.top, /DESTROY 
				return	
	    	END
    ELSE:   BEGIN
	    		widget_control,diva_math_sum_first,  set_value = 'none' 
				widget_control,diva_math_sum_second, set_value = 'none' 
				widget_control,diva_math_butt_remove,sensitive = 0

				CASE index[0] OF
					0	:	new_array = [old_array[1:*]]
		 			last:	new_array = [old_array[0:last-1]]
		 			ELSE:	new_array = [old_array[0:index[0]-1],  	    	$
		    	    	    	 		 old_array[index[0]+1:last]]
				ENDCASE   
				
				Ptr_Free,old_array[index[0]].pimage
				Ptr_Free,old_array[index[0]].o_pimage
				Ptr_Free,old_array[index[0]].p_pimage
				Ptr_Free,old_array[index[0]].s_pimage
				
				;reset index to zero
				(*diva_math_info).index[0] = 0
				;update structure with entry removed
				(*(*diva_math_info).diva_image_stored) = new_array
				;update list with entry removed
				diva_math_list = new_array[*].ops_name

				FOR i=0,n_elements(diva_math_list)-1 DO BEGIN
		    		diva_math_list[i] = strcompress(i+1,/rem) +     	    $
		    	    	    	    	    		'. '+diva_math_list[i]
				ENDFOR

				diva_math_list_mini = STRARR(N_ELEMENTS(new_array))

				FOR i = 0, n_elements(diva_math_list) - 1 DO BEGIN
		    		diva_math_list_mini[i]=strcompress(i+1,/rem) +'. '+     $
		    	    		strmid(diva_math_list[i],   	    	    	$
		    	    		strpos(diva_math_list[i],'/',/reverse_search)+1)
				ENDFOR

				widget_control,diva_op1_list_id,set_value = diva_math_list
				widget_control,diva_op2_list_id,set_value = diva_math_list_mini

				widget_control,diva_math_sum_doit,sensitive = 0
				widget_control,diva_math_constant,sensitive = 0
				widget_control,diva_op2_list_id,sensitive = 0
				widget_control,diva_math_op1_draw,get_value=ops_index
				wset,ops_index & erase
	    	END
    ENDCASE
END ;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"OPR2_IMG": 	$	;
BEGIN
	widget_control,diva_math_constant,sensitive=0
	(*diva_math_info).diva_constant_opr=0
	widget_control,diva_op2_list_id,	sensitive = 1
	widget_control,diva_math_sum_doit,sensitive=0
	
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!	
"OPR2_CON": 	$	;
BEGIN
	widget_control,diva_math_constant,sensitive=1
	widget_control,diva_math_sum_doit,sensitive=0
	(*diva_math_info).diva_constant_opr=1
	widget_control,diva_op2_list_id,	sensitive = 0
	widget_control,diva_math_op2_draw,	get_value=ops_index
	wset,ops_index	&  ERASE
	xyouts,.25,.45,'NONE',/norm,charsize=3,charthick=2
	widget_control,diva_math_op2_minmax,set_value=''
	
	widget_control,diva_math_sum_second,set_value = '0'
	widget_control,diva_math_constant,set_value = '0'
	
	widget_control,diva_op2_list_id,set_list_select = -1
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!	
"MATH_SECOND_IMAGE":	$	;SECOND_IMAGE_SELECTION__SECOND_IMAGE_SELECTION
BEGIN
	widget_control,/hourglass
	(*diva_math_info).index[1] = event.index
	;extract "diva_image" structure for this selection
	diva_image = (*(*diva_math_info).diva_image_stored)[event.index]
	
	;extract image, then refresh pointer to it ("diva_math_second_pimage")
	pimage  	= (*diva_image.pimage)
	Ptr_Free,(*diva_math_info).diva_math_second_pimage
	(*diva_math_info).diva_math_second_pimage = ptr_new(pimage)
	
	;extract s_pimage
	s_pimage  	= (*diva_image.s_pimage)
	
	;get min & max pixel values and report them
	min_max = strcompress(min(pimage),/rem) + ',' +strcompress(max(pimage),/rem)
	widget_control,diva_math_op2_minmax,set_value = min_max

	;now display small image
	ops_bshift  = diva_image.ops_bshift
	widget_control,diva_math_op2_draw,get_value=ops_index
	wset,ops_index	&  ERASE
	CASE ops_bshift[0] OF
		0:  tv,ishft(s_pimage,ops_bshift[1])
		1:  tvscl,s_pimage
	ENDCASE
	
	;call "diva_math_plot_outline" to outline image
	diva_math_plot_outline,diva_image.s_pimage,ops_index;,					$
									;(*diva_math_info).pdiva_draw.image_ct,	$
									;(*diva_math_info).pdiva_draw.plot_ct
	
	;get ops_name and remove pathname.....
	ops_name = diva_image.ops_name
	;...and create the mini version				
	;1. strip off pathname.....
	ops_name_mini=strmid(ops_name,strpos(ops_name,'/',/reverse_search)+1)
	;2. ....then brackety stuff.....
	ops_name_mini=strmid(ops_name_mini,0,strpos(ops_name_mini,'('))
	;3. ...add image index....
	ops_name_mini= '[' + strcompress(event.index + 1,/rem) + '] ' + ops_name_mini
	;4. ....and finally update second image widget with mini name
	widget_control,diva_math_sum_second,set_value =	ops_name_mini
	widget_control,diva_math_sum_doit,sensitive=1

END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"MATH_FIRST_IMAGE":		$	;FIRST_IMAGE_SELECTION____FIRST_IMAGE_SELECTION
BEGIN
	widget_control,/hourglass
	(*diva_math_info).index[0] = event.index
	;extract "diva_image" structure for this selection
	diva_image = (*(*diva_math_info).diva_image_stored)[event.index]
	
	;extract image, then refresh pointer to it ("diva_math_first_pimage")
    pimage  	= (*diva_image.pimage)
    Ptr_FREE,(*diva_math_info).diva_math_first_pimage
    (*diva_math_info).diva_math_first_pimage = Ptr_New(pimage)

	;extract s_pimage
    s_pimage  	= (*diva_image.s_pimage)

	;get min & max pixel values and report them
	min_max = strcompress(min(pimage),/rem) + ',' +strcompress(max(pimage),/rem)
	widget_control,diva_math_op1_minmax,set_value = min_max
	
	;now display small image
	ops_bshift  = diva_image.ops_bshift
	widget_control,diva_math_op1_draw,get_value=ops_index
    wset,ops_index	&  ERASE
    CASE ops_bshift[0] OF
    	0:  tv,ishft(s_pimage,ops_bshift[1])
    	1:  tvscl,s_pimage
    ENDCASE

	;call "diva_math_plot_outline" to outline image
	diva_math_plot_outline,diva_image.s_pimage,ops_index;,					$
									;(*diva_math_info).pdiva_draw.image_ct,	$
									;(*diva_math_info).pdiva_draw.plot_ct

    ;get ops_name and remove pathname.....
    ops_name = diva_image.ops_name
    ;...and create the mini version				 
	;1. strip off pathname.....
	ops_name_mini=strmid(ops_name,strpos(ops_name,'/',/reverse_search)+1)
	;2. ....then brackety stuff.....
	ops_name_mini=strmid(ops_name_mini,0,strpos(ops_name_mini,'('))
	;3. ...add image index....
	ops_name_mini= '[' + strcompress(event.index + 1,/rem) + '] ' + ops_name_mini
	;4. ....and finally update first image widget with mini name
    widget_control,diva_math_sum_first,set_value =  ops_name_mini
    
	
	widget_control,diva_math_butt_remove,sensitive = 1
	widget_control,operand_2_base,sensitive=1
	IF (diva_constant_opr eq 0) then widget_control,diva_op2_list_id,sensitive=1
	widget_control,diva_math_sum_doit,sensitive=0
END ;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"OPR_LIST": 	$	;
BEGIN
	
	(*diva_math_info).diva_math_function = event.index
	CASE event.index OF
		0:	widget_control,diva_math_opr_label,set_value = ' + '
		1:	widget_control,diva_math_opr_label,set_value = ' - '
		2:	widget_control,diva_math_opr_label,set_value = ' * '
		3:	widget_control,diva_math_opr_label,set_value = ' / '
		4:	BEGIN
				widget_control,diva_math_opr_label,set_value = 'ave'
				widget_control,diva_math_constant,sensitive=0
			END
	ENDCASE
END ;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

ELSE: MESSAGE, "Event User Value Not Found"		
ENDCASE
END ;end of diva_math event handling routine task -----------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA_MATH_INFO__DEFINE
; PURPOSE:
;		This procedure defines the "diva_math_info" (named) structure
;		(see IDL help: "Automatic Stucture Definition").
; INPUTS:
;		None
; AUTHOR:
;		John A. Rainnie - RAL (j.a.rainnie@rl.ac.uk)
;-
;------------------------------------------------------------------------------;
PRO diva_math_info__define
;------------------------------------------------------------------------------;

junk = {	diva_math_info, 												$
			diva_mathbase				: 0L,			        			$
			diva_concbase				: 0L,								$
			diva_op1_list_id 	    	: 0L, 		        				$
			diva_op2_list_id	    	: 0L, 		        				$
			diva_math_op1_draw		    : 0L,		        				$
			diva_math_op2_draw		    : 0L,		        				$
			diva_math_op1_minmax	    : 0L, 	        					$
			diva_math_op2_minmax	    : 0L, 	        					$
			diva_math_constant 	        : 0L,		        				$
			diva_math_sum_base		    : 0L,		        				$
			diva_math_sum_first	        : 0L,  	        					$
			diva_math_sum_second	    : 0L, 	        					$
			diva_math_opr_label	        : 0L,  	        					$
			diva_math_sum_doit 	        : 0L,		        				$
			operand_2_base			    : 0L,			        			$
			diva_constant_opr 	        : 0,						        $
			diva_constant_val 	        : 0.,						        $
			diva_math_function 	        : 0,						        $
			diva_image_stored			: PTR_NEW(),						$
			diva_math_first_pimage	    : PTR_NEW(),						$
			diva_math_second_pimage     : PTR_NEW(),						$
			result_image			    : PTR_NEW(),						$
			diva_math_butt_remove	    : 0L,	        					$
			index     	    	        : [0,0] 					        $
		}	    	    	    

END ; of "diva_math_info__define" procedure------------------------------------;
;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA_MATH_INIT
; PURPOSE:
;		This function 
; INPUTS:
;		def_path	- default pathname
;		diva_image_stored	- a pointer
; OUTPUTS:
;		diva_math_info
; AUTHOR:
;		John A. Rainnie - RAL (j.a.rainnie@rl.ac.uk)
;-
;------------------------------------------------------------------------------;
FUNCTION diva_math_init,def_path,diva_image_stored,tlb
;------------------------------------------------------------------------------;

diva_math_list	    		= (*diva_image_stored)[*].ops_name
diva_math_list_mini 		= STRARR(N_ELEMENTS(diva_math_list))

FOR i = 0, N_ELEMENTS(diva_math_list) - 1 DO BEGIN
    diva_math_list[i] 		= '[' + strcompress(i+1,/rem) + '] ' +  		$
									diva_math_list[i]

    diva_math_list_mini[i]	= '[' + strcompress(i+1,/rem) + '] ' +			$
							strmid(diva_math_list[i],						$
    	    				strpos(diva_math_list[i],'/',/reverse_search)+1)
	
	diva_math_list[i] 		= strmid(diva_math_list[i],0,					$
								strpos(diva_math_list[i],'('))
ENDFOR

diva_math_info					= {diva_math_info}
diva_math_info.diva_image_stored = diva_image_stored
;-------------------main base--------------------------------------------------;
diva_mathbase 				= WIDGET_BASE(TITLE = "DIVA - IMAGE MATH",/col, $
									group_leader=tlb,/floating)
diva_math_info.diva_mathbase = diva_mathbase
;-------------------operand 1 base---------------------------------------------;
operand_1_base				= widget_base(diva_mathbase,/row,frame=6)


diva_op1_rhs_base			= widget_base(operand_1_base,/col)

diva_op1_rhs_top_base		= widget_base(diva_op1_rhs_base,/row,/grid_layout)
diva_op1_list_label			= widget_label(diva_op1_rhs_top_base,			$
									value='OPERAND #1:             ',		$
									/align_left)

diva_math_butt_remove		= widget_button(diva_op1_rhs_top_base,			$
									sensitive=0,value='Remove selection',	$
									uvalue='REM_BUTT')
diva_math_info.diva_math_butt_remove = diva_math_butt_remove

diva_math_butt_remove_all	= widget_button(diva_op1_rhs_top_base,			$
									value='Remove ALL (and quit)',	$
									uvalue='REMALL_BUTT')




diva_op1_list_id			= widget_list(diva_op1_rhs_base,ysize=10,		$
									xsize=70,value=diva_math_list,			$
									uvalue='MATH_FIRST_IMAGE',/align_left)
diva_math_info.	diva_op1_list_id = 	diva_op1_list_id						

diva_op1_lhs_base			= widget_base(operand_1_base,/col,/frame)

diva_math_op1_draw			= widget_draw(diva_op1_lhs_base,xsize=150,		$
									ysize=150,frame=4)
diva_math_info.diva_math_op1_draw = diva_math_op1_draw

diva_math_op1_label			= widget_label(diva_op1_lhs_base,				$
									value='min/max values:')
diva_math_op1_minmax		= widget_text(diva_op1_lhs_base,value='0,0',	$
									xsize=20)
diva_math_info.	diva_math_op1_minmax = diva_math_op1_minmax							
;-------------------operator base----------------------------------------------;
operator_base 				= widget_base(diva_mathbase,/row,frame=6)

diva_opr_list				= widget_droplist(operator_base,				$
									uvalue = 'OPR_LIST',					$
									value  = [ '+  addition',				$
										'-  subtraction',					$
										'*  multiplication',				$
									 	'/  division',						$
										'   average'],						$
										title = 'OPERATOR:')

;-------------------operand 2 base---------------------------------------------;
operand_2_base			= widget_base(diva_mathbase,/row,frame=6,sensitive=0)
diva_math_info.operand_2_base = operand_2_base

diva_op2_rhs_base		= widget_base(operand_2_base,/col)

diva_op2_rhs_top_base	= widget_base(diva_op2_rhs_base,/row)
diva_op2_list_label		= widget_label(diva_op2_rhs_top_base,				$
								value='OPERAND #2:',/align_left)


diva_math_opr2_base_exc = widget_base(diva_op2_rhs_top_base,/row,/exclusive)
diva_math_opr2_is_image = widget_button(diva_math_opr2_base_exc,			$
								value='image',uvalue='OPR2_IMG')
diva_math_opr2_is_con	= widget_button(diva_math_opr2_base_exc,			$
								value='constant',uvalue='OPR2_CON')
widget_control,diva_math_opr2_is_image,set_button=1
diva_math_constant		= widget_text(diva_op2_rhs_top_base,				$
								value='0',									$
								/editable,									$
								uvalue='CONSTANT',xsize=10,					$
								sensitive=0)
diva_math_info.diva_math_constant = diva_math_constant

diva_op2_list_id 		= widget_list(diva_op2_rhs_base,					$
    							value=diva_math_list_mini,					$
								ysize=10,xsize=70,							$
								uvalue='MATH_SECOND_IMAGE')
diva_math_info.diva_op2_list_id = diva_op2_list_id

diva_op2_lhs_base		= widget_base(operand_2_base,/col,					$
								/frame,sensitive=0)

diva_math_op2_draw		= widget_draw(diva_op2_lhs_base,xsize=150,			$
								ysize=150,frame=4)
diva_math_info.diva_math_op2_draw = diva_math_op2_draw

diva_math_op2_label		= widget_label(diva_op2_lhs_base,					$
								value='min/max values:')
								
diva_math_op2_minmax	= widget_text(diva_op2_lhs_base,value='0,0',xsize=20)
diva_math_info.diva_math_op2_minmax = diva_math_op2_minmax								
;-------------------sum base---------------------------------------------------;
diva_math_sum_base		= widget_base(diva_mathbase,/row,frame=6)
diva_math_info.diva_math_sum_base = diva_math_sum_base

diva_math_sum_label 	= widget_label(diva_math_sum_base,					$
								value='OPERATION:  ')

diva_math_sum_first 	= widget_text(diva_math_sum_base,value='none',		$
								xsize=35)
diva_math_info.diva_math_sum_first = diva_math_sum_first

diva_math_opr_label 	= widget_label(diva_math_sum_base,value='  +  ')
diva_math_info.diva_math_opr_label = diva_math_opr_label

diva_math_sum_second	= widget_text(diva_math_sum_base,value='none',		$
								xsize=35)
diva_math_info.diva_math_sum_second = diva_math_sum_second

diva_math_sum_doit		= widget_button(diva_math_sum_base,value='   =   ', $
								sensitive=0,uvalue='DO_SUM')
diva_math_info.diva_math_sum_doit = diva_math_sum_doit								
;-------------------quit button------------------------------------------------;
quitbutton				= widget_button(diva_mathbase, value='Quit',		$
								uvalue='EXIT')
;------------------------------------------------------------------------------;

WIDGET_CONTROL, diva_mathbase, /REALIZE		

pdiva_math_info = Ptr_New(diva_math_info)
						
widget_control,diva_mathbase,set_uvalue = pdiva_math_info

XManager, "diva_math", diva_mathbase,EVENT_HANDLER = "diva_math_ev",	    $
		GROUP_LEADER = tlb,cleanup = 'diva_math_cleanup'		

Return, pdiva_math_info

END ;end of diva_math main routine---------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_math_cleanup,diva_mathbase

widget_control,diva_mathbase,get_uvalue=diva_math_info
t = diva_math_info
if (Ptr_Valid(t) eq 1) then begin
    Ptr_Free,(*t).diva_math_first_pimage
    Ptr_Free,(*t).diva_math_second_pimage
    Ptr_Free,(*t).result_image
	;finally free up the top-level pointer for "DIVA_MATH"
    Ptr_FRee,t
endif 
end ;end of diva_math CLEANUP routine------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_math_plot_outline,pimage,wid_id;,image_ct,plot_ct
;------------------------------------------------------------------------------;
; Purpose -
;------------------------------------------------------------------------------;

wset,wid_id
save_size   = size(*pimage)
xsize		= save_size[1] - 1
ysize		= save_size[2] - 1

;tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2];PLOT colour table

plots,[0,0,xsize,xsize,0],[0,ysize,ysize,0,0],/device;,color=4

;tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2];IMAGE colour table

END ;of "diva_math_plot_outline" procedure-------------------------------------;
;------------------------------------------------------------------------------;
