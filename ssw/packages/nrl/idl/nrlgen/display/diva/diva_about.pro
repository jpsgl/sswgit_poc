;------------------------------------------------------------------------------;
PRO diva_about_ev, event;------------------------------------------------------;
;------------------------------------------------------------------------------;
;PURPOSE
;
;------------------------------------------------------------------------------;

WIDGET_CONTROL, event.id, GET_UVALUE = eventval	
						
CASE eventval OF

"DONE": WIDGET_CONTROL, event.top, /DESTROY	

ENDCASE
END ;end of diva_about event handler-------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_about, GROUP = GROUP;-------------------------------------------------;
;------------------------------------------------------------------------------;
;PURPOSE
;
;------------------------------------------------------------------------------;

IF(XRegistered("diva_about") NE 0) THEN RETURN

os_name = !d.name
CASE os_name OF
    "WIN":  thepixarehere = '.\diva_pics\'
    "X":    thepixarehere = './diva_pics/'
ENDCASE

read_jpeg,strcompress(thepixarehere,/remove)+'bigjohn.jpg',bigjohn,/true
bigjohn=congrid(bigjohn,3,300,200)
;stop
;-------------------main base--------------------------------------------------;
diva_aboutbase = WIDGET_BASE(TITLE = "ZIMAGER: ABOUT",/col,/frame) 
items=[      'ABOUT DIVA [Version 1.0 (c)]',$   	    	    ;0   	     
       '',$ 	    	    	    	    	     	    	    ;1
       ' 18 Febuary 2002 John Rainnie',$    	     	    	    ;2
       'All fRights pReserved',$		     	    	    ;3
       '+----------------------------------------------+',$ 	    ;4
       'DIVA is an image viewer',$	    	     	    	    ;5
       '']  	    	    	    	    	     	    	    ;6
       
messwin_base=widget_base(diva_aboutbase,frame=1,/col,/align_center)

messwin_0 =widget_label(messwin_base,value=items[0])	;VERSION
messwin_1 =widget_label(messwin_base,value=items[1])
messwin_2 =widget_label(messwin_base,value=items[2])
messwin_3 =widget_label(messwin_base,value=items[3])
messwin_4 =widget_label(messwin_base,value=items[4])
messwin_5 =widget_label(messwin_base,value=items[5])
messwin_6 =widget_label(messwin_base,value=items[6])

diva_about_draw=widget_draw(diva_aboutbase,xsize=300,ysize=200,frame=5)
				  
;-------------------quit button------------------------------------------------;
done=widget_button(diva_aboutbase,value='QUIT',uvalue='DONE',/dynamic_resize)

WIDGET_CONTROL, diva_aboutbase, /REALIZE	
widget_control,diva_about_draw,get_value=me_index
wset,me_index
tv,bigjohn,/true
						
XManager, "diva_about", diva_aboutbase, $	
		EVENT_HANDLER = "diva_about_ev", $
		GROUP_LEADER = GROUP		
						
END ;end of diva_about main routine--------------------------------------------;
;------------------------------------------------------------------------------;
