;------------------------------------------------------------------------------;
PRO diva_roi_profile_ev, event
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
WIDGET_CONTROL, event.id, GET_UVALUE = eventval	
widget_control,event.top,get_uvalue=	diva_roi_profile_info	

lyr 					= (*diva_roi_profile_info).yr[0]
uyr 					= (*diva_roi_profile_info).yr[1]
result					= (*diva_roi_profile_info).result
diva_roi_profile_draw 	= (*diva_roi_profile_info).diva_roi_profile_draw
diva_printers			= (*diva_roi_profile_info).diva_printers
diva_printers_list		= (*diva_roi_profile_info).diva_printers_list
diva_roi_profilebase	= (*diva_roi_profile_info).diva_roi_profilebase

widget_control,diva_roi_profile_draw,get_value=draw_area
wset,draw_area

CASE eventval OF

"EXIT": 	$
BEGIN
	WIDGET_CONTROL, event.top, /DESTROY
	return		
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"RESTORE":	BEGIN
	;call 'diva_roi_profile_display' procedure to plot result	
	diva_roi_profile_display,diva_roi_profile_info

END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"YR_UP":	BEGIN
	mid 	= (lyr+uyr)/2
	mid_n   =	mid - lyr
	lyr 	= mid - (mid_n * 2)
	uyr 	= mid + (mid_n * 2)
	(*diva_roi_profile_info).yr = [lyr,uyr]
	;call 'diva_roi_profile_display' procedure to plot result	
	diva_roi_profile_display,diva_roi_profile_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"YR_DOWN":	BEGIN
	mid 	= (lyr+uyr)/2
	mid_n   =	mid - lyr
	lyr 	= mid - (mid_n * .5)
	uyr 	= mid + (mid_n * .5)
	(*diva_roi_profile_info).yr = [lyr,uyr]
	;call 'diva_roi_profile_display' procedure to plot result	
	diva_roi_profile_display,diva_roi_profile_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"YR_DEF":	BEGIN
	(*diva_roi_profile_info).yr = [min(result),max(result)]
	;call 'diva_roi_profile_display' procedure to plot result	
	diva_roi_profile_display,diva_roi_profile_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DO_PRINT": 	$
BEGIN
	full_name 	= (*diva_roi_profile_info).file_stuff[0]
	path_name 	= (*diva_roi_profile_info).file_stuff[1]
	file_str  	= (*diva_roi_profile_info).file_stuff[2]
	mfile_name  = (*diva_roi_profile_info).file_stuff[3]

	file_in	= file_str + '_roi_profile.ps'

	;call 'diva_print_what' FUNCTION to get filename and 
	result = diva_print_what(path_name,file_in,diva_printers,				$
									diva_printers_list,diva_roi_profilebase)

	IF ((*result).do_print_yes eq 1) THEN BEGIN
		filename 		= (*result).path+(*result).filename
		what_printer	= diva_printers[(*result).what_printer]
		do_save_yes 	= (*result).do_save_yes
		
		print,'printing to: "'+strcompress(filename)+'"'

		os_name = !d.name

		set_plot,'ps'
		device,filename=filename,/color

		;call 'diva_roi_profile_display' procedure to plot result
		diva_roi_profile_display,diva_roi_profile_info

		device,/close
		CASE os_name OF
			'X':	BEGIN
						set_plot,'x'
						;call "diva_print_linux" pro to do printing!!!
						diva_print_linux,filename,do_save_yes,what_printer
					END
			'WIN':	    set_plot,'win'
		ENDCASE
	ENDIF
	;now free up the 'result' pointer
	Ptr_Free,result
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			
ELSE: MESSAGE, "Event User Value Not Found"		
ENDCASE
widget_control,event.top,set_uvalue=	diva_roi_profile_info
END ;end of "diva_roi_profile" event handler-----------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_roi_profile, result,result_x,diva_roi_profile_info,image_ct,	    $
    	plot_ct,file_stuff,diva_printers,diva_printers_list,GROUP = GROUP
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;

;-------------------create the main base---------------------------------------;
diva_roi_profilebase = WIDGET_BASE(TITLE = "DIVA ROI PROFILE",/col, 	    $
    	group_leader=group,/floating)

diva_roi_profile_top=widget_base(diva_roi_profilebase,/row,/frame)

d_r_p_lhs_base=widget_base(diva_roi_profile_top,/col,/frame)
diva_roi_profile_draw=widget_draw(d_r_p_lhs_base,xs=600,ys=400)

d_r_p_rhs_base=widget_base(diva_roi_profile_top,/col,/frame)
d_r_p_yrup=  widget_button(d_r_p_rhs_base,value=' + 100% ',uvalue='YR_UP')
d_r_p_yrdef= widget_button(d_r_p_rhs_base,value='Default',uvalue='YR_DEF')
d_r_p_yrdown=widget_button(d_r_p_rhs_base,value=' - 100% ',uvalue='YR_DOWN')
d_r_p_print =widget_button(d_r_p_rhs_base,value=' Print ',uvalue='DO_PRINT')

diva_roi_profile_restore=widget_button(diva_roi_profilebase,value='Restore',$
    	uvalue='RESTORE')
;-------------------quit button------------------------------------------------;
quitbutton=widget_button(diva_roi_profilebase, value='Quit',uvalue='EXIT')
;------------------------------------------------------------------------------;

;-------------------realise the widget-----------------------------------------;
WIDGET_CONTROL, diva_roi_profilebase, /REALIZE		


diva_roi_profile_info = Ptr_New ({   	    	    	    	    	    $
    	    	    	diva_roi_profilebase	: diva_roi_profilebase,     $
		            	result					: result,					$
		            	result_x				: result_x, 				$
		            	diva_roi_profile_draw	: diva_roi_profile_draw,	$
		            	image_ct				: image_ct, 				$
		            	plot_ct 				: plot_ct,  				$
		            	yr  					: [min(result),max(result)],$
						file_stuff				: file_stuff,				$
						diva_printers			: diva_printers,			$
						diva_printers_list		: diva_printers_list		$
		            			})

widget_control,diva_roi_profile_draw,get_value=draw_area
wset,draw_area

;call 'diva_roi_profile_display' procedure to plot result			
diva_roi_profile_display,diva_roi_profile_info

widget_control,diva_roi_profilebase,set_uvalue=	diva_roi_profile_info

XManager, "diva_roi_profile", diva_roi_profilebase, 	    	    	    $	
		EVENT_HANDLER = "diva_roi_profile_ev",      	    	    		$
		GROUP_LEADER = GROUP,cleanup = 'diva_roi_profile_cleanup'	
						
END ;end of "diva_roi_profile" main routine------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_roi_profile_cleanup,diva_roi_profilebase
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
widget_control,diva_roi_profilebase,get_uvalue=diva_roi_profile_info
if (Ptr_Valid(diva_roi_profile_info) eq 1) then begin
    Ptr_Free,diva_roi_profile_info
endif
END ;of "diva_roi_profile_cleanup"---------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_roi_profile_display,diva_roi_profile_info
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;

t   =	(*diva_roi_profile_info)

;widget_control,t.diva_roi_profile_draw,get_value=draw_area
;wset,draw_area
tvlct,t.plot_ct[*,0],t.plot_ct[*,1],t.plot_ct[*,2];change to PLOT colour table
plot,t.result_x,t.result,yr=[t.yr[0],t.yr[1]],/nodata
oplot,t.result_x,t.result,color=2,thick=2,psym=10
if (n_elements(t.result) lt 50) then 	    	    	    	    	    $
    oplot,t.result_x,t.result,color=1,psym=6,thick=2
;change back to IMAGE colour table
tvlct,t.image_ct[*,0],t.image_ct[*,1],t.image_ct[*,2]

end
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
