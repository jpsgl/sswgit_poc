;------------------------------------------------------------------------------;
PRO diva_report
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
stop
END;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_report_prev_info,prev_info,tmp
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
if (tmp.t[0] ne '') then begin
    widget_control,prev_info[0],set_value = tmp.t[0]
    widget_control,prev_info[1],set_value = tmp.t[1] + ' bytes'
    widget_control,prev_info[2],set_value = tmp.t[2] + ' x ' 	+   		$
    	    	    	    	    	    tmp.t[3] + ' x ' 	+   		$
					    					tmp.t[4] + ' pixels'
end

END;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_prev_list_sensitive,widget_ids,doh

for i = 0, 3 do widget_control,widget_ids[i],sensitive	= doh[i]	
for i = 4, 6 do widget_control,widget_ids[i],map		= doh[i]	
	
	
END
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_prev_buttons_sensitive,widget_ids,wot
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;

for i = 0, n_elements(wot)-1 do widget_control,widget_ids[i],sensitive=wot[i]

END
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_prev_contextbase_sensitive,widget_ids,wot
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;

for i = 0,n_elements(wot)-1  do widget_control,widget_ids[i],sensitive=wot[i]

END;of 'diva_prev_contextbase_sensitive' procedure-----------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_tlb_title,pdiva_draw
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
tlb 	    	    = (*pdiva_draw).tlb
diva_current_image  = (*pdiva_draw).diva_current_image

n_dim	    =	strcompress(diva_current_image.n_dim,/rem)
xsize	    =	strcompress(diva_current_image.xsize,/rem)
ysize	    =	strcompress(diva_current_image.ysize,/rem)
mfile_name  =	strcompress(diva_current_image.mfile_name,/rem)
f	    	=	strcompress(diva_current_image.file_info.f,/rem)
pathname    =	strcompress(diva_current_image.file_info.pathname,/rem)
file_str    =	strcompress(diva_current_image.file_info.file_str,/rem)

n_pixels    =	strcompress((*pdiva_draw).diva_current_image.xsize * 	    $
    	    	    	    (*pdiva_draw).diva_current_image.ysize *	    $
			    			(*pdiva_draw).diva_current_image.n_dim,/rem)
			    
bracks =  ' [ '+xsize+' x '+ ysize + ' x '+n_dim+' = '+n_pixels+' pixels ]'

if (mfile_name eq '') then begin    ;for single images
    widget_control,tlb,TLB_Set_Title= 'DIVA  --> image = '+ f + bracks
endif else begin    	    	    ;for multiple images
    widget_control,tlb,TLB_Set_Title='DIVA  --> image = '+    	    	    $
    	    				pathname + file_str + '__'+mfile_name + bracks
endelse

END;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
