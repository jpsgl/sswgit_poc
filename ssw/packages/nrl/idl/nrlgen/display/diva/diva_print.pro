;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA_PRINT_LINUX
; PURPOSE:
;		This procedure prints a file to either a specified or default
;		printer (depends on DIVA_DEFAULTS.PRO)
; INPUTS:
;		1. the filename
;		2. "do_save_yes", having possible values of 0, 1 and 2, corresponding
;			to 'just print', 'print & save' and 'just save'
;		3. "what_printer" - which is either specified 
;			(ie: lpr -Pprintername filename.ext), or the default
;			printer is used (ie: lpr filename.ext)
; OUTPUTS:
;		None
;-
;------------------------------------------------------------------------------;
PRO diva_print_linux,filename,do_save_yes,what_printer
;------------------------------------------------------------------------------;

file_str		= strcompress(filename,/rem)

IF (do_save_yes ne 2) THEN BEGIN
	CASE what_printer OF
		'': 	spawn,"lpr " 						 + file_str
		ELSE:	spawn,"lpr -P"  + what_printer + ' ' + file_str
	ENDCASE
	IF (do_save_yes eq 0) THEN spawn,"rm " + file_str
ENDIF

END
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA_EV
; PURPOSE:
;		This procedure is the event handler of PRINT widget
; CATEGORY:
;		EVENT HANDLER
;-
;------------------------------------------------------------------------------;
PRO diva_print_what_ev, event
;------------------------------------------------------------------------------;
WIDGET_CONTROL, event.id, GET_UVALUE = eventval		
widget_control,event.top,get_uvalue=diva_print_what_info

info = (*diva_print_what_info)

CASE eventval OF

"EXIT": 	$	;
BEGIN
	(*diva_print_what_info).do_print_yes = 0
	WIDGET_CONTROL, event.top, /DESTROY
	return
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"ACCEPT":	$	;
BEGIN
	(*diva_print_what_info).do_print_yes = 1
	WIDGET_CONTROL, event.top, /DESTROY
	return
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"NAME_INPUT":	$   ;
BEGIN
    widget_control,info.name_text_id,get_value = s_name
    name = strcompress(s_name[0],/rem)
    (*diva_print_what_info).filename = name
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"C_DIR":    	$   ;
BEGIN
    new_path = dialog_pickfile(/directory,path=info.path,    	    	    $
    	    	    	    	dialog_parent=info.diva_print_whatbase)

    IF (new_path eq '') THEN new_path = info.path
    (*diva_print_what_info).path = new_path
    widget_control,info.dir_label2,set_value=new_path
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"PRINTER_SELECT":	$	;
BEGIN
	(*diva_print_what_info).what_printer = event.index
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"SAVE_YES": 	$	;
BEGIN
	if (event.select eq 1) then begin
		(*diva_print_what_info).do_save_yes = 2
		widget_control,(*diva_print_what_info).name_base,sensitive = 1
		widget_control,(*diva_print_what_info).dir_base ,sensitive = 1
	end
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"SAVE_NO": 		$	;
BEGIN
	if (event.select eq 1) then begin
		(*diva_print_what_info).do_save_yes = 0
		widget_control,(*diva_print_what_info).name_base,sensitive = 0
		widget_control,(*diva_print_what_info).dir_base ,sensitive = 0
	end
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"SAVE_BOTH": 	$	;
BEGIN
	if (event.select eq 1) then begin
		(*diva_print_what_info).do_save_yes = 1
		widget_control,(*diva_print_what_info).name_base,sensitive = 0
		widget_control,(*diva_print_what_info).dir_base ,sensitive = 0
	end
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


ELSE: MESSAGE, "Event User Value Not Found"		
ENDCASE

END ;end of "diva_print_what" event handler------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
FUNCTION diva_print_what, path,filename,diva_printers,						$
											diva_printers_list,group_l
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;

;-------------------create the main base---------------------------------------;
diva_print_whatbase 		= WIDGET_BASE(TITLE = "DIVA PRINTDIALOG",		$
									/col,group_leader=group_l,				$
									/floating,/modal)

topbase 					= widget_base(diva_print_whatbase,/col,/frame)

;-------------------filename base----------------------------------------------;
name_base 					= widget_base(topbase,/row,/align_left, 		$
									sensitive=0)
name_label					= widget_label(name_base,value='Filename:    ')
name_text_id 				= widget_text(name_base,						$
									value=filename, 						$
									uvalue='NAME_INPUT',    				$
    	    	    				/editable,								$
									/all_events,							$
									xsize = 60)

;-------------------directory base---------------------------------------------;
dir_base 					= widget_base(topbase,/row,sensitive=0)

dir_label1					= widget_label(dir_base,value='Directory:   ',	$
									/align_right)

dir_label2					= widget_label(dir_base,value=path, 			$
									xsize=370,/align_left)
dir_button					= widget_button(dir_base,						$
									value=' Change... ',					$
									uvalue='C_DIR')

;--------------------save file base--------------------------------------------;
save_file_base				= widget_base(topbase,/row,/align_left)

save_file_label 			= widget_label(save_file_base,					$
									value='Save File:  ')

save_file_base_exc			= widget_base(save_file_base,/row,				$
									/align_left,/exclusive,					$
									/grid_layout)

save_file_no 				= widget_button(save_file_base_exc, 			$
									value = 'Just Print',					$
									uvalue='SAVE_NO')
widget_control,save_file_no,set_button=1

save_file_both				= widget_button(save_file_base_exc, 			$
									value = 'Print & Save',					$
									uvalue='SAVE_BOTH')

save_file_yes				= widget_button(save_file_base_exc, 			$
									value = 'Just Save',					$
									uvalue='SAVE_YES')

;--------------------what printer base-----------------------------------------;
IF  (n_elements(diva_printers) ge 1) 			AND 						$
	(where(diva_printers[0] ne '') ne -1)   								$
THEN BEGIN

	printer_base			= widget_base(topbase,/row,/align_left)

	printer_label			= widget_label(printer_base,					$
									value='Printer:   ')
	printer_list			= widget_droplist(printer_base, 				$
									uvalue='PRINTER_SELECT',				$
									value = diva_printers_list)
ENDIF
;-------------------ok / cancel base-------------------------------------------;
ok_cancel_base  			= widget_base(diva_print_whatbase,				$
									/row,/grid_layout,/frame)

okbutton					= widget_button(ok_cancel_base, 				$
									value='Print',							$
									uvalue='ACCEPT')
widget_control,diva_print_whatbase,default_button = okbutton


cancelbutton				= widget_button(ok_cancel_base, 				$
									value='Cancel', 						$
									uvalue='EXIT')
widget_control,diva_print_whatbase,cancel_button = cancelbutton
;------------------------------------------------------------------------------;

;-------------------realise the widget-----------------------------------------;
WIDGET_CONTROL, diva_print_whatbase, /REALIZE		
						
diva_print_what_info = Ptr_New({    	    	    	    	    	    $
    	    		diva_print_whatbase   	    : diva_print_whatbase,	    $
    	    		path	    	    	    : path,    	    	    	$
					filename	    	    	: filename,	    	    	$
					name_text_id        	    : name_text_id,  	    	$
					dir_label2  	    	    : dir_label2,    	    	$
					what_printer				: 0, 						$
					do_print_yes				: 1, 						$
					do_save_yes 				: 0,						$
					name_base					: name_base,				$
					dir_base					: dir_base					$
			    			   })

widget_control,diva_print_whatbase,set_uvalue=diva_print_what_info

XManager, "diva_print_what", diva_print_whatbase, 							$
		EVENT_HANDLER = "diva_print_what_ev",								$
		GROUP_LEADER  = group_l

return,diva_print_what_info

END ;end of "diva_print_what" main routine-------------------------------------;
;------------------------------------------------------------------------------;
