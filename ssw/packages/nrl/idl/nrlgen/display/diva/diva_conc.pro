;------------------------------------------------------------------------------;
PRO diva_conc_ev, event
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
WIDGET_CONTROL, event.id , GET_UVALUE = eventval		
widget_control, event.top, get_uvalue = diva_conc_info

diva_conc_list_id   =	(*diva_conc_info).diva_conc_list_id
diva_conc_draw      =	(*diva_conc_info).diva_conc_draw
diva_conc_move	    =	(*diva_conc_info).diva_conc_move
diva_conc_rem	    =	(*diva_conc_info).diva_conc_rem
diva_conc_rem_all   =	(*diva_conc_info).diva_conc_rem_all
index	    	    =	(*diva_conc_info).index
read_from_to	    =	(*diva_conc_info).read_from_to
diva_conc_file		=	(*diva_conc_info).diva_conc_file
diva_concbase		= 	(*diva_conc_info).diva_concbase

CASE eventval OF

"EXIT": 		$
BEGIN
	WIDGET_CONTROL, event.top, /DESTROY
	RETURN
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"CONC_LIST":	$   ;CONC_LIST_CONC_LIST_CONC_LIST_CONC_LIST_CONC_LIST_CONC_LIST
BEGIN
    ei      = event.index
	
	temp    = (*(*diva_conc_info).diva_image_stored)
    bshift  = temp[ei].ops_bshift
    pimage  = (*temp[ei].pimage)
    pimage  = congrid(pimage,300,300)
    
    (*diva_conc_info).index = ei
    
    widget_control,diva_conc_draw,get_value=conc_index
    wset,conc_index
    CASE bshift[0] OF
    	0:  tv,ishft(pimage,bshift[1])
    	1:  tvscl,pimage
    ENDCASE
    
    widget_control,diva_conc_rem,sensitive = 1  
    widget_control,diva_conc_rem_all,sensitive = 1  
    
    n_elems = n_elements(temp)
    last    = (n_elems - 1)
    
    IF (n_elems eq 1) THEN BEGIN
    	widget_control,diva_conc_move[0],sensitive = 0
		widget_control,diva_conc_move[1],sensitive = 0
    ENDIF ELSE BEGIN
    	CASE ei OF
	    0:	    BEGIN
	    	    	widget_control,diva_conc_move[0],sensitive = 0
    	    	    	widget_control,diva_conc_move[1],sensitive = 1
		    	END
    	    last:   BEGIN
	    	    	widget_control,diva_conc_move[0],sensitive = 1
    	    	    	widget_control,diva_conc_move[1],sensitive = 0
	    	    END
    	    ELSE:   BEGIN
	    	    	widget_control,diva_conc_move[0],sensitive = 1
    	    	    	widget_control,diva_conc_move[1],sensitive = 1
	    	    END
		ENDCASE
    ENDELSE
    widget_control,diva_conc_file,sensitive=1
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"CONC_UP":  	$   ;CONC_UP_CONC_UP_CONC_UP_CONC_UP_CONC_UP_CONC_UP_CONC_UP_CO
BEGIN
    old_array	= (*(*diva_conc_info).diva_image_stored)
    n_elems 	= n_elements(old_array)
    last 		= n_elems -1  ;ie, index of last element
    IF (n_elems eq 2) THEN BEGIN
		new_array = [old_array[1],old_array[0]]
		widget_control,diva_conc_move[0],sensitive = 0
    ENDIF ELSE BEGIN
    	sub 	= [old_array[index],old_array[index-1]]
    	CASE index OF
    	    1:	BEGIN
	    	    	new_array = [sub,old_array[index+1:*]]
					widget_control,diva_conc_move[0],sensitive = 0
		    	END
	    last:   new_array = [old_array[0:index-2],sub]
	    ELSE:   new_array = [old_array[0:index-2],sub,old_array[index+1:*]]
    	ENDCASE
    ENDELSE
    ;reset index to zero
    (*diva_conc_info).index = index - 1
    ;update structure with entry removed
    (*(*diva_conc_info).diva_image_stored) = new_array
    ;update list with entry removed
    diva_conc_list = new_array[*].ops_name
    
    FOR i=0,n_elements(diva_conc_list)-1 DO diva_conc_list[i] = $
    		    strcompress(i+1,/rem) + '. '+diva_conc_list[i]
    widget_control,diva_conc_list_id,set_value = diva_conc_list
    widget_control,diva_conc_list_id,set_list_select = index - 1
    widget_control,diva_conc_move[1],sensitive = 1

END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"CONC_DOWN":  	$   ;CONC_DOWN_CONC_DOWN_CONC_DOWN_CONC_DOWN_CONC_DOWN_CONC_DOW
BEGIN
    old_array 	= (*(*diva_conc_info).diva_image_stored)
    n_elems 	= n_elements(old_array)
    last 		= n_elems -1  ;ie, index of last element
    
    IF (n_elems eq 2) THEN BEGIN
		new_array = [old_array[1],old_array[0]]
		widget_control,diva_conc_move[1],sensitive = 0
    ENDIF ELSE BEGIN
    	sub = [old_array[index+1],old_array[index]]
    	CASE index OF
    	    0:	new_array	= [sub,old_array[index+2:*]]
	    last-1: BEGIN
	    	    	new_array 	= [old_array[0:index-1],sub]
					widget_control,diva_conc_move[1],sensitive = 0
		    	END
	    ELSE:   new_array 	= [old_array[0:index-1],sub,old_array[index+2:*]]	
    	ENDCASE
    ENDELSE
    ;reset index to zero
    (*diva_conc_info).index = index + 1
    ;update structure with entry removed
    (*(*diva_conc_info).diva_image_stored) = new_array
    ;update list with entry removed
    diva_conc_list = new_array[*].ops_name
    
    FOR i=0,n_elements(diva_conc_list)-1 DO diva_conc_list[i] = $
    		    strcompress(i+1,/rem) + '. '+diva_conc_list[i]
    widget_control,diva_conc_list_id,set_value = diva_conc_list
    widget_control,diva_conc_list_id,set_list_select = index + 1
    widget_control,diva_conc_move[0],sensitive = 1
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"CONC_REM_ALL": $   ;CONC_REMOVE_ALL_CONC_REMOVE_ALL_CONC_REMOVE_ALL_CONC_REMOV
BEGIN
	;DIVA_MATH!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	IF (XREGISTERED("diva_math") eq 1) THEN BEGIN
		widget_control,(*diva_conc_info).diva_mathbase,/destroy
	ENDIF
	
	old_array 	= (*(*diva_conc_info).diva_image_stored)
	n_elems 	= N_ELEMENTS(old_array)
    FOR i = 0, n_elems -1 DO BEGIN
		Ptr_Free,old_array[i].pimage
		Ptr_Free,old_array[i].o_pimage
		Ptr_Free,old_array[i].p_pimage
		Ptr_Free,old_array[i].s_pimage
	ENDFOR
	Ptr_Free,(*diva_conc_info).diva_image_stored

	WIDGET_CONTROL, event.top, /DESTROY
	RETURN	
	
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"CONC_REM":  	$   ;CONC_REMOVE_CONC_REMOVE_CONC_REMOVE_CONC_REMOVE_CONC_REMOV
BEGIN
	;DIVA_MATH!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	IF (XREGISTERED("diva_math") eq 1) THEN BEGIN
		widget_control,(*diva_conc_info).diva_mathbase,/destroy
	ENDIF

    old_array 	= (*(*diva_conc_info).diva_image_stored)
    n_elems 	= N_ELEMENTS(old_array)
    last 		= n_elems - 1  ;ie, index of last element
    CASE n_elems OF
    	;if there's only one list entry,then remove it from
    	;list and destroy the image_conc widget!!!!!!!!!!
    	1:  BEGIN
	    		Ptr_Free,old_array.pimage
				Ptr_Free,old_array.o_pimage
				Ptr_Free,old_array.p_pimage
				Ptr_Free,old_array.s_pimage
				Ptr_Free,(*diva_conc_info).diva_image_stored
				WIDGET_CONTROL, event.top, /DESTROY 
				RETURN	
	    	END
    ELSE:   BEGIN
    	    	CASE index OF
					0	:	new_array = [old_array[1:*]]
		 			last:	new_array = [old_array[0:last-1]]
		 			ELSE:	new_array = [old_array[0:index-1],				$
		    	    	     			 old_array[index+1:last]]
				ENDCASE   
				
				Ptr_Free,old_array[index[0]].pimage
				Ptr_Free,old_array[index[0]].o_pimage
				Ptr_Free,old_array[index[0]].p_pimage
				Ptr_Free,old_array[index[0]].s_pimage
				
				;reset index to zero
				(*diva_conc_info).index = 0
				;update structure with entry removed
				(*(*diva_conc_info).diva_image_stored) = new_array
				;update list with entry removed
				diva_conc_list = new_array[*].ops_name

				FOR i=0,n_elements(diva_conc_list)-1 DO diva_conc_list[i] = $
		    	    			strcompress(i+1,/rem) + '. '+diva_conc_list[i]

				widget_control,diva_conc_list_id,set_value = diva_conc_list
				widget_control,diva_conc_move[0],sensitive = 0
				widget_control,diva_conc_move[1],sensitive = 0
				widget_control,diva_conc_rem,sensitive = 0

				widget_control,diva_conc_draw,get_value=conc_index
				wset,conc_index     &   erase
	    END
    ENDCASE
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"CONC_FILE":  	$   ;CONC_FILE_CONC_FILE_CONC_FILE_CONC_FILE_CONC_FILE_CONC_FIL
BEGIN
	widget_control,/hourglass
	
	old_array   = (*(*diva_conc_info).diva_image_stored)
	f			= old_array[0].file_info.f
	
	;1. check if there is only one image - and it already exists. If so,return!
	n_elems 	= n_elements(old_array)
	IF (n_elems eq 1) THEN BEGIN
		existing = findfile(f,count = count)
		IF (count ge 1) AND (old_array[0].mfile_name eq '') THEN return
	ENDIF
	
	
	;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!;
	;																		;
	;	2. time to get a filename - offer 'path+"temp.fit"' 				;
	;																		;
	;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!;
	;get the pathname of the "first_image"
	path 	= old_array[0].file_info.pathname

	;name is based on time and date --> "DD_MM_YY__HH_MM_SS.fit"
	get_date,this_date
			day 	= strmid(this_date,1,/reverse_offset)
			month	= strmid(this_date,5,2)
			year	= strmid(this_date,0,4)

	t = systime()	&	this_time = strmid(t,11,8)
			hour	= strmid(this_time,0,2)
			minute	= strmid(this_time,3,2)
			second	= strmid(this_time,1,/reverse_offset)
	;make name
	time_name=  day  + '_' + month  + '_' + year + '__' +  					$
				hour + '_' + minute + '_' + second

	name 	= path + time_name + '.fit'

	;call "diva_get_filename" FUNCTION to return user-selected "get_f"
	get_f 	= diva_get_filename(name,path,diva_resultbase)
	
	;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!;
	;																		;
	;	3.	write first image as a FITS file								;
	;																		;
	;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!;
	FOR i = 0,n_elems - 1 DO BEGIN
		image = (*old_array[i].o_pimage)
		index = i
		diva_conc_make_fits_file,image,index,get_f
	ENDFOR
    
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"CONC_ANIM":  	$   
BEGIN
	
	old_array   = (*(*diva_conc_info).diva_image_stored)
	f			= old_array[0].file_info.f
	
	;1. check if there is only one image - and it already exists. If so,return!
	n_elems 	= n_elements(old_array)
	IF (n_elems eq 1) THEN RETURN

	widget_control,/hourglass

;	h	= BYTARR(256,256,n_elems)
;	
;	FOR i = 0,n_elems - 1 DO BEGIN
;		;image 		= BYTE(CONGRID(*old_array[i].o_pimage,256,256))
;		image 		= CONGRID(*old_array[i].o_pimage,256,256)
;		h[*,*,i]	= image
;	ENDFOR
;	
;	;stop
;	
;	; Initialize XINTERANIMATE
;	XINTERANIMATE, SET=[256, 256, n_elems], /SHOWLOAD;,rate = 10
;	
;	; Load the images into XINTERANIMATE
;	FOR i=0,n_elems - 1 DO XINTERANIMATE, FRAME = i,IMAGE = h[*,*,i]
;	
;	; Play the animation
;	XINTERANIMATE,group=diva_concbase
    
    widget_control,diva_conc_draw,get_value=conc_index
    wset,conc_index
	
	FOR i = 0, n_elems - 1 DO BEGIN
		
		(*diva_conc_info).index = i
		pimage	= CONGRID(*old_array[i].o_pimage,300,300)
		bshift  = old_array[i].ops_bshift
    	CASE bshift[0] OF
    		0:  tv,ishft(pimage,bshift[1])
    		1:  tvscl,pimage
    	ENDCASE
		widget_control,diva_conc_list_id,set_list_select = i
		wait,(*diva_conc_info).anim_time_value
		
	ENDFOR
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"ANIM_TIME":  	$   
BEGIN
	value	= ['.05','.10','.15','.20','.25','.30','.40']
	(*diva_conc_info).anim_time_value = FLOAT(value[event.index])
	
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

ELSE: MESSAGE, "Event User Value Not Found"		
ENDCASE
;widget_control,event.top,set_uvalue=diva_conc_info
END ;end of "diva_conc" event handler------------------------------------------;
;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA_CONC_INFO__DEFINE
; PURPOSE:
;		This procedure defines the "diva_conc_info" (named) structure
;		(see IDL help: "Automatic Stucture Definition").
; INPUTS:
;		None
; AUTHOR:
;		John A. Rainnie - RAL (j.a.rainnie@rl.ac.uk)
;-
;------------------------------------------------------------------------------;
PRO diva_conc_info__define
;------------------------------------------------------------------------------;

junk = {	diva_conc_info, 												$
			diva_concbase   	        : 0L,			        			$
			diva_mathbase				: 0L,								$
			diva_image_stored			: PTR_NEW(),						$
			diva_conc_list_id	        : 0L,		        				$
			diva_conc_draw  	        : 0L,			        			$
			diva_conc_move  	        : [0L,0],			        		$
			diva_conc_rem   	        : 0L,			        			$
			diva_conc_rem_all  	        : 0L,		        				$
			diva_conc_file		        : 0L,			        			$
			index   	    	        : 0,						        $
			os_name 	    	        : '',  				        		$
			def_path	    	        : ['',''], 				        	$
			read_from_to    	        : [0L,0L],  				        $
			anim_time_value 	        : 0.						        $
		}
		
END ; of "diva_conc_info__define" procedure------------------------------------;
;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA_CONC_INIT
; PURPOSE:
;		This function 
; INPUTS:
;		def_path	- default pathname
;		diva_image_stored	- a pointer
; OUTPUTS:
;		diva_conc_info
; AUTHOR:
;		John A. Rainnie - RAL (j.a.rainnie@rl.ac.uk)
;-
;------------------------------------------------------------------------------;
FUNCTION diva_conc_init,def_path,diva_image_stored,tlb
;------------------------------------------------------------------------------;

diva_conc_info					= {diva_conc_info}
diva_conc_info.os_name  		= !D.name
diva_conc_info.def_path 		= def_path
diva_conc_info.anim_time_value	= 0.1
diva_conc_info.diva_image_stored = diva_image_stored
;-------------------create the main base---------------------------------------;
diva_concbase 				= WIDGET_BASE(TITLE = "DIVA CONCATENATOR TOOL",	$
									/col,group_leader=tlb,/floating)
									
diva_conc_info.diva_concbase	= diva_concbase

diva_conc_topbase				= widget_base(diva_concbase,/row,frame=4)

diva_conc_list_base 			= widget_base(diva_conc_topbase,/col,/frame)

;make list
diva_conc_list	    = (*diva_image_stored)[*].ops_name
FOR i = 0, N_ELEMENTS(diva_conc_list) - 1 DO BEGIN
    diva_conc_list[i] = strcompress(i+1,/rem) +'. '+diva_conc_list[i]
ENDFOR

diva_conc_list_id 			= widget_list(diva_conc_list_base,				$
									value=diva_conc_list,   				$
    								uvalue='CONC_LIST', 					$
									ysize=13)

diva_conc_info.diva_conc_list_id = diva_conc_list_id
;-------------------move entry in list base------------------------------------;

diva_conc_move_base 		= widget_base(diva_conc_topbase,				$
									/col,/grid_layout,/frame)
diva_conc_move_up   		= widget_button(diva_conc_move_base,			$
									value='Up',   	    					$
    								uvalue='CONC_UP',sensitive=0)

diva_conc_move_down 		= widget_button(diva_conc_move_base,			$
									value='Down',   	    				$
    								uvalue='CONC_DOWN',sensitive=0)

diva_conc_move  			= [diva_conc_move_up,diva_conc_move_down]
diva_conc_info.diva_conc_move = diva_conc_move
;-------------------remove entry base------------------------------------------;
diva_conc_rem_base			= widget_base(diva_conc_list_base,				$
									/frame,/row,/grid_layout)

diva_conc_rem 				= widget_button(diva_conc_rem_base,				$
									value='Remove selection',   	    	$
    								uvalue='CONC_REM',						$
									sensitive=0)
diva_conc_info.diva_conc_rem = diva_conc_rem
									
diva_conc_rem_all 			= widget_button(diva_conc_rem_base, 			$
									value='Remove ALL',   					$
    								uvalue='CONC_REM_ALL',					$
									sensitive=1)
diva_conc_info.diva_conc_rem_all = diva_conc_rem_all

diva_conc_file  			= widget_button(diva_conc_rem_base,				$
									value='Save to File',    				$
    								uvalue='CONC_FILE', 					$
									sensitive=1)
diva_conc_info.diva_conc_file = diva_conc_file
;-------------------animation base---------------------------------------------;
diva_conc_anim_base 		= widget_base(diva_conc_list_base,				$
									/frame,/row)

diva_conc_anim_butt			= widget_button(diva_conc_anim_base,			$
									value='Animate',     					$
    								uvalue='CONC_ANIM', 					$
									sensitive=1)

value						= [' .05 ',' .10 ',' .15 ',' .20 ',' .25 ',		$
							    			   ' .30 ',' .40 ']
diva_conc_anim_list 		= widget_droplist(diva_conc_anim_base,			$
									value = value,							$
									uvalue = 'ANIM_TIME',					$
									title = '  Set time interval (s):')

widget_control,	diva_conc_anim_list,set_droplist_select = 1
;-------------------draw area base---------------------------------------------;
diva_conc_draw_base 		= widget_base(diva_conc_topbase,/col)
diva_conc_draw				= widget_draw(diva_conc_draw_base,				$
									xsize=300,ysize=300,frame=4)
diva_conc_info.diva_conc_draw = diva_conc_draw
;-------------------quit button------------------------------------------------;
quitbutton					= widget_button(diva_concbase,					$
									value='Quit',							$
									uvalue='EXIT',frame=4)
;------------------------------------------------------------------------------;

;-------------------realise the widget-----------------------------------------;
WIDGET_CONTROL, diva_concbase, /REALIZE		

pdiva_conc_info = Ptr_New(diva_conc_info)
						
widget_control,diva_concbase,set_uvalue = pdiva_conc_info

XManager, "diva_conc", diva_concbase,EVENT_HANDLER = "diva_conc_ev", $
		GROUP_LEADER = tlb,cleanup = 'diva_conc_cleanup'	
		
Return, pdiva_conc_info
						
END ;end of "diva_conc" main routine-------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_conc_cleanup,diva_concbase

widget_control,diva_concbase,get_uvalue=diva_conc_info
if (Ptr_Valid(diva_conc_info) eq 1) then begin
    Ptr_FRee,diva_conc_info
endif 
end ;end of diva_conc CLEANUP routine------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_conc_make_fits_file,image,index,get_f

image = rotate(reverse(image),2)	;reverse & flip image
data_type = size(image,/type)		;get data type

CASE index OF
	0:		BEGIN
				;make a minimal FITs header
				mkhdr,header,image,/extend
				;call "diva_conc_fits_header" procedure
				diva_conc_fits_header,header,data_type,get_f,index
				;finally create FITS file with first image
				writefits,get_f,image,header
			END
	ELSE:	BEGIN
				;make a minimal FITs header
				mkhdr,header,image,/image
				;call "diva_conc_fits_header" procedure
				diva_conc_fits_header,header,data_type,get_f,index
				;finally append FITS file with other images
				writefits,get_f,image,header,/APPEND
			END
ENDCASE

END;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_conc_fits_header,header,data_type,get_f,index

IF (data_type eq 12) THEN BEGIN
	;discard 'O_BZERO' keyword and replace it with 'BZERO'
	sxdelpar,header,'O_BZERO'
	sxdelpar,header,'BZERO'
	sxaddpar,header,'BZERO',32768.,' Original Data is Unsigned Integer',		$
														after='EXTEND'
ENDIF

;add 'BSCALE' keyword
sxaddpar,header,'BSCALE',1.,' Default scaling factor',after='BZERO'

;remove 2-line comment about fits format....
sxdelpar,header,'COMMENT'
sxaddpar,header,'HISTORY',' developed by bigjohn and duncan at RAL', 	$
														before='END'

sxaddpar,header,'COMMENT',' ********************************' + 		$
			'***************************************',before='HISTORY'

sxaddpar,header,'COMMENT',' Source file: ' + strcompress(get_f,/rem),		$
										before='HISTORY'
sxaddpar,header,'COMMENT',' ********************************' + 		$
			'***************************************',before='HISTORY'
;add usual 1-liner about FITS reference.....
sxaddpar,header,'COMMENT','FITS format is described in A&A ' +			$
							'Vol376, pg359; bibcode 2001A&A...376..' +	$
							'359H',before='HISTORY'

 t = systime()   &   this_time = strmid(t,11,8)
sxaddpar,header,'TIME',this_time,' Creation Time (HH-MM-SS) ' + 			$
								'of FITS header',after='DATE'


file_nopath = strmid(get_f,strpos(get_f,'/',/reverse_search)+1)
sxaddpar,header,'FILENAME',file_nopath,' the filename',after='TIME'
sxaddpar,header,'IMG_NAME','image'+strcompress(index,/rem),after='FILENAME'


;take off annoying trailing (empty lines)
	;get index of 'END'
	wot = where(strcompress(strmid(header,0,8),/rem) eq 'END') + 1
	n_hdr = strarr(wot)
	n_hdr = header[0:wot-1]
	;lovely!!!!!
header = n_hdr

END;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
