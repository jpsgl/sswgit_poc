Pro diva_circle, radius, xc, yc,x,y

irad 	= round(radius)
x   	= 0
y   	= irad 
d   	= 3 - 2 * irad

; Find the x and y coordinates for one eighth of a circle.
; The maximum number of these coordinates is the radius of
; the circle.

xHalfQuad    = Make_Array( irad + 1, /Int, /NoZero )
yHalfQuad    = xHalfQuad
path	     = 0

WHILE (x lt y) DO BEGIN
      xHalfQuad[path] = x
      yHalfQuad[path] = y
      path = path + 1
      IF (d lt 0) THEN d=d+(4*x)+6 ELSE BEGIN
           d = d + (4*(x-y)) + 10
           y = y - 1
      END
x = x + 1
END

IF x eq y THEN BEGIN ; Fill in last point
        xHalfQuad[path] = x
        yHalfQuad[path] = y
        path = path + 1
END ; Filling in last point

; Shrink the arrays to their correct size
xHalfQuad = xHalfQuad[ 0:path-1 ]
yHalfQuad = yHalfQuad[ 0:path-1 ]

; Convert the eighth circle into a quadrant
xQuad = [ xHalfQuad, Rotate(yHalfQuad, 5) ]
yQuad = [ yHalfQuad, Rotate(xHalfQuad, 5) ]

; Prepare for converting the quadrants into a full circle
xQuadRev = Rotate( xQuad[0:2*path-2], 5 )
yQuadRev = Rotate( yQuad[0:2*path-2], 5 )

; Create full-circle coordinates
x = [ xQuad,  xQuadRev, -xQuad[1:*], -xQuadRev ]
y = [ yQuad, -yQuadRev, -yQuad[1:*],  yQuadRev ]

End; diva_circle
