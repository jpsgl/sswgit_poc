;------------------------------------------------------------------------------;
PRO diva_stats_ev, event
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
WIDGET_CONTROL, event.id, GET_UVALUE = eventval		

CASE eventval OF

"EXIT":     	WIDGET_CONTROL, event.top, /DESTROY

ELSE: MESSAGE, "Event User Value Not Found"		
ENDCASE

END ;end of "diva_stats" event handler-----------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_stats,s,wot,n_dim,GROUP = GROUP
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
;if (wot eq 0) then IF (XRegistered("diva_stats") NE 0) THEN RETURN		

row_labels = ['bl coord','tr coord','area','min','max','mean',	    	    $
    	    	'variance','stddev']

bl  	= [strcompress(fix(s[0]),/rem)+','+strcompress(fix(s[2]),/rem)]
tr  	= [strcompress(fix(s[1]),/rem)+','+strcompress(fix(s[3]),/rem)]
area 	= strcompress(round((s[1]-s[0]+1)*(s[3]-s[2]+1)),/rem)
minv1 	= strcompress(round(s[4]),/remove)
minv2 	= strcompress(round(s[5]),/remove)
minv3 	= strcompress(round(s[6]),/remove)
maxv1 	= strcompress(round(s[7]),/remove)
maxv2 	= strcompress(round(s[8]),/remove)
maxv3 	= strcompress(round(s[9]),/remove)
meanv1  = strcompress(s[10],/remove)
meanv2  = strcompress(s[11],/remove)
meanv3	= strcompress(s[12],/remove)
varv1 	= strcompress(s[13],/remove)
varv2 	= strcompress(s[14],/remove)
varv3 	= strcompress(s[15],/remove)
stdv1 	= strcompress(s[16],/remove)
stdv2 	= strcompress(s[17],/remove)
stdv3 	= strcompress(s[18],/remove)

if (n_dim eq 3) then begin
    values=transpose([	[bl,tr,area,minv1,maxv1,meanv1,varv1,stdv1],$
    	    	    	[bl,tr,area,minv2,maxv2,meanv2,varv2,stdv2],$
	    	    	[bl,tr,area,minv3,maxv3,meanv3,varv3,stdv3] ])
    col_labels = ['r','g','b']
endif else begin
    values = transpose([bl,tr,area,minv1,maxv1,meanv1,varv1,stdv1])
    col_labels = ['Value']
endelse

;-------------------create the main base---------------------------------------;
diva_statsbase = WIDGET_BASE(TITLE = "DIVA STATS",/col,	    	    	    $
    	    	    	    	group_leader=group,/floating)
;-------------------create table widget to display stats-----------------------;
stat_table=widget_table(diva_statsbase,xsize=n_dim,ysize=8,     	    $
    	column_labels=col_labels,row_labels=row_labels,	    	    	    $
	alignment=1,column_widths=100,value=values)

;-------------------quit button------------------------------------------------;
quitbutton=widget_button(diva_statsbase, value='Quit',uvalue='EXIT')
;------------------------------------------------------------------------------;

;-------------------realise the widget-----------------------------------------;
WIDGET_CONTROL, diva_statsbase, /REALIZE		

XManager, "diva_stats", diva_statsbase, $		
		EVENT_HANDLER = "diva_stats_ev", $
		GROUP_LEADER = GROUP		
						
END ;end of "diva_stats" main routine------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_tlb_stats_calc,pdiva_draw,diva_draw_base
;------------------------------------------------------------------------------;
; Purpose -
;------------------------------------------------------------------------------;

pimage  = (*pdiva_draw).diva_current_image.pimage 
xsize	= (*pdiva_draw).diva_current_image.xsize
ysize	= (*pdiva_draw).diva_current_image.ysize
n_dim	= (*pdiva_draw).diva_current_image.n_dim
xv  	= [0,xsize -1]
yv  	= [0,ysize -1]

if (n_dim eq 1) then begin
    temp = (*pdiva_draw).diva_current_image
    
    temp.i_min     =	   min(*pimage)
    temp.i_max     =	   max(*pimage)
    temp.i_mean    =	   mean(*pimage)
    temp.i_variance =	   variance(*pimage)
    temp.i_stddev   =	   stddev(*pimage)
    
    (*pdiva_draw).diva_current_image = temp
endif
if (n_dim eq 3) then begin
    ir = (*pimage)[*,*,0]
    ig = (*pimage)[*,*,1]
    ib = (*pimage)[*,*,2]
    
    temp = (*pdiva_draw).diva_current_image
    
    temp.i_min         = [min(ir)     ,min(ig)	   ,min(ib)]	    
    temp.i_max         = [max(ir)     ,max(ig)	   ,max(ib)]	    
    temp.i_mean        = [mean(ir)    ,mean(ig)	   ,mean(ib)]	    
    temp.i_variance    = [variance(ir),variance(ig),variance(ib)]
    temp.i_stddev      = [stddev(ir)  ,stddev(ig)  ,stddev(ib)]  
    
    (*pdiva_draw).diva_current_image = temp
endif

stats = [xv,yv,temp.i_min,temp.i_max,temp.i_mean,temp.i_variance,temp.i_stddev]
;call 'diva_stats' pro to make STATISTICS gui
diva_stats,stats,0,n_dim,group = diva_draw_base    

END
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_stats_calc,pimage,xv,yv,wot,n_dim,groupid
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;

CASE n_dim OF
    1:	BEGIN
    	    minv    = [min(pimage)     ,0,0]
	    	maxv    = [max(pimage)     ,0,0]
	    	meanv   = [mean(pimage)    ,0,0]
	    	varv    = [variance(pimage),0,0]
	    	stddevv = [stddev(pimage)  ,0,0]
		END
    3:	BEGIN
    	    ir = pimage[*,*,0]
    	    ig = pimage[*,*,1]
    	    ib = pimage[*,*,2]
    	    minv    = [min(ir)     ,min(ig)     ,min(ib)     ]
	    	maxv    = [max(ir)     ,max(ig)     ,max(ib)     ]
	    	meanv   = [mean(ir)    ,mean(ig)    ,mean(ib)    ]
	    	varv    = [variance(ir),variance(ig),variance(ib)]
	    	stddevv = [stddev(ir)  ,stddev(ig)  ,stddev(ib)  ]
		END
ENDCASE
stats	= [ xv,yv,minv,maxv,meanv,varv,stddevv]
;call 'diva_stats' pro to make STATISTICS gui
diva_stats,stats,wot,n_dim,group=groupid     
end
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
