;------------------------------------------------------------------------------;
PRO diva_vers_ev, event;-----------------------------------------------------;
;------------------------------------------------------------------------------;
;PURPOSE
;
;------------------------------------------------------------------------------;

WIDGET_CONTROL, event.id, GET_UVALUE = eventval	
						
CASE eventval OF

"DONE": WIDGET_CONTROL, event.top, /DESTROY	

ENDCASE
END ;end of diva_vers event handler------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_vers, GROUP = GROUP;------------------------------------------------;
;------------------------------------------------------------------------------;
;PURPOSE
;
;------------------------------------------------------------------------------;

IF(XRegistered("diva_vers") NE 0) THEN RETURN	

;-------------------main base--------------------------------------------------;
diva_versbase = WIDGET_BASE(TITLE = "DIVA: VERSION",/col,/frame)  

items=['DIVA  --  VERSION 1.0 (c)',$    	     	    ;0
       '18 Feburary 2002 John A Rainnie',$     	     	    ;1
       'All fRights pReserved',$		     	    ;2
'+----------------------------------------------+',$ 	    ;3
'+----------------------------------------------+',$ 	    ;4
       'Imaging Systems Division',$		     	    ;5
       'Space Science & Technology Department',$     	    ;6
       'Rutherford Appleton Laboratory',$	     	    ;7
       'Chilton, Didcot',$			     	    ;8
       'OX11 0QX',$				     	    ;9
       'United Kingdom' ]			     	    ;10


messwin_base=widget_base(diva_versbase,frame=1,/col)

messwin_0 =widget_label(messwin_base,value=items(0))	;VERSION
messwin_1 =widget_label(messwin_base,value=items(1))
messwin_2 =widget_label(messwin_base,value=items(2))
messwin_3 =widget_label(messwin_base,value=items(3))

authors_base=widget_base(messwin_base,/row)
authors_lhs =widget_base(authors_base,/col)
authors_rhs =widget_base(authors_base,/col)

authors_lhs_1=widget_label(authors_lhs,value='Author:',/align_right)
authors_rhs_1=widget_label(authors_rhs,value='John A Rainnie',/align_left)

authors_lhs_1=widget_label(authors_lhs,value='Contributors:',/align_right)
authors_rhs_1=widget_label(authors_rhs,value='Philip J Kershaw',/align_left)
authors_rhs_1=widget_label(authors_rhs,value='Duncan L Drummond',/align_left)
authors_rhs_1=widget_label(authors_rhs,value='Nick R Waltham',/align_left)


messwin_4 =widget_label(messwin_base,value=items(4))
messwin_5 =widget_label(messwin_base,value=items(5))
messwin_6 =widget_label(messwin_base,value=items(6))
messwin_7 =widget_label(messwin_base,value=items(7))
messwin_8 =widget_label(messwin_base,value=items(8))
messwin_9 =widget_label(messwin_base,value=items(9))
messwin_10=widget_label(messwin_base,value=items(10))	;UK

;-------------------quit button------------------------------------------------;
done=widget_button(diva_versbase,value='Quit',uvalue='DONE',/dynamic_resize)

WIDGET_CONTROL, diva_versbase, /REALIZE	
						
XManager, "diva_vers", diva_versbase, $	
		EVENT_HANDLER = "diva_vers_ev", $
		GROUP_LEADER = GROUP		

END ;end of diva_vers main routine-------------------------------------------;
;------------------------------------------------------------------------------;
