;------------------------------------------------------------------------------;
PRO diva_export_ev, event
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
WIDGET_CONTROL, event.id, GET_UVALUE = eventval
widget_control,event.top,get_uvalue=diva_export_info	

diva_export_type		    = (*diva_export_info).diva_export_type
diva_export_dir_label2	    = (*diva_export_info).diva_export_dir_label2
diva_export_file_base	    = (*diva_export_info).diva_export_file_base
diva_export_dir_base	    = (*diva_export_info).diva_export_dir_base
what_printer 			    = (*diva_export_info).what_printer
diva_printers			    = (*diva_export_info).diva_printers
diva_printers_list		    = (*diva_export_info).diva_printers_list
diva_do_save_yes		    = (*diva_export_info).diva_do_save_yes
image_ct    	    		= (*diva_export_info).image_ct
plot_ct     	    		= (*diva_export_info).plot_ct
diva_jpeg_qual_base 		= (*diva_export_info).diva_jpeg_qual_base
;diva_jpeg_qual_text 		= (*diva_export_info).diva_jpeg_qual_text
diva_jpeg_slider			= (*diva_export_info).diva_jpeg_slider

CASE eventval OF

"DO_JPEG":			$
BEGIN
	if (event.select eq 1) then begin    
		(*diva_export_info).diva_export_type = 0

		f	= (*diva_export_info).export_filename
		;remove extension and replace with "tif"
		f 	= strmid(f,0,strpos(f,'.',/reverse_search)) + '.jpg'
		
		(*diva_export_info).export_fullfilename = 							$
						(*diva_export_info).pathname + f
						
		widget_control,(*diva_export_info).diva_export_file_text,    		$
			    	    	    set_value=f
								
		widget_control,diva_jpeg_qual_base,sensitive = 1
	endif
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DO_TIFF":			$
BEGIN
	if (event.select eq 1) then begin    
		(*diva_export_info).diva_export_type = 1

		f	= (*diva_export_info).export_filename
		;remove extension and replace with "tif"
		f 	= strmid(f,0,strpos(f,'.',/reverse_search)) + '.tif'
		
		(*diva_export_info).export_fullfilename = 							$
						(*diva_export_info).pathname + f
						
		widget_control,(*diva_export_info).diva_export_file_text,    		$
			    	    	    set_value=f
								
		widget_control,diva_jpeg_qual_base,sensitive = 0
	endif
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"DO_PS":  			$
BEGIN
	if (event.select eq 1) then begin    
		(*diva_export_info).diva_export_type = 2

		f	= (*diva_export_info).export_filename
		;remove extension and replace with "tif"
		f 	= strmid(f,0,strpos(f,'.',/reverse_search)) + '.ps'
		
		(*diva_export_info).export_fullfilename = 							$
						(*diva_export_info).pathname + f
						
		widget_control,(*diva_export_info).diva_export_file_text,    		$
			    	    	    set_value=f
								
		widget_control,diva_jpeg_qual_base,sensitive = 0
	endif
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"OK_EXPORT":		$
BEGIN
	;diva_do_save_yes	0	just print
	;					1	print and save
	;					2	just save
	
	pimage  			= (*(*diva_export_info).pimage)
	os_name 			= !D.name		
	diva_autoscale_on	= (*diva_export_info).diva_autoscale_on
	bitshift_val		= (*diva_export_info).bitshift_val
	n_dim 				= SIZE(pimage,/n_dimensions)
			
	if (diva_autoscale_on eq 0) then pimage=ishft(pimage,bitshift_val)		$
	ELSE pimage = bytscl(pimage)
	
	export_fullfilename=(*diva_export_info).export_fullfilename
	
	q = (*diva_export_info).diva_jpeg_qual_value
	
	
	CASE diva_export_type OF
		0:	BEGIN
				CASE n_dim OF
					2:		write_jpeg,export_fullfilename,pimage,			$
															quality = q
					3:		write_jpeg,export_fullfilename,pimage,			$
															quality = q,	$
															true = 3
					ELSE:	stop
				ENDCASE
			END
		1:	BEGIN
		    	pimage=reverse(pimage,2)
				CASE n_dim OF
					2:		write_tiff,export_fullfilename,pimage,1
					3:		write_tiff,export_fullfilename,pimage,			$
															planarconfig = 2
					ELSE:	stop
				ENDCASE
			END
		2:	BEGIN
    	    	set_plot,'ps'
				device,/landscape,filename=export_fullfilename,/color
				tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2]
				CASE n_dim OF
					2:		tvscl,pimage
					3:		tvscl,pimage,true=3
							
					ELSE:	stop
				ENDCASE
				tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2]
				device,/close
				set_plot,os_name
    		END
	ENDCASE
	
	IF (diva_do_save_yes ne 2) THEN BEGIN
		CASE os_name OF
			'X':	BEGIN
						;call "diva_print_linux" pro to do printing!!!
						diva_print_linux,export_fullfilename,				$
											diva_do_save_yes,				$
											diva_printers[what_printer]
					END
			ELSE:	diva_error,(*diva_export_info).diva_exportbase,0
		ENDCASE
	
	ENDIF
	
	WIDGET_CONTROL, event.top, /DESTROY
	Ptr_Free,diva_export_info
	RETURN		
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"CANCEL_EXPORT":	$
BEGIN
	WIDGET_CONTROL, event.top, /DESTROY
	Ptr_Free,diva_export_info
	RETURN		
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"GET_NAME": 		$	
BEGIN
	widget_control,(*diva_export_info).diva_export_file_text,	    		$
													get_value=s_get_filename
	(*diva_export_info).export_filename =  s_get_filename
	
	export_fullfilename = (*diva_export_info).pathname + s_get_filename
	;print,export_fullfilename
	(*diva_export_info).export_fullfilename = export_fullfilename
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"BROWSE":			$
BEGIN
	pathname=(*diva_export_info).pathname
	dir = dialog_pickfile(path=pathname,title='Save in directory...',		$
			/directory)
	(*diva_export_info).pathname = dir
	widget_control,diva_export_dir_label2,set_value = dir
	
	export_fullfilename = dir + (*diva_export_info).export_filename
	print,export_fullfilename
	(*diva_export_info).export_fullfilename = export_fullfilename
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"BOTH_SAVE": 		$
BEGIN
	if (event.select eq 1) then begin
		(*diva_export_info).diva_do_save_yes = 1
		widget_control,diva_export_file_base,sensitive = 1
		widget_control,diva_export_dir_base,sensitive = 1
	end
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"NO_SAVE":			$
BEGIN
	if (event.select eq 1) then begin
		(*diva_export_info).diva_do_save_yes = 0
		widget_control,diva_export_file_base,sensitive = 0
		widget_control,diva_export_dir_base,sensitive = 0
	end
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"YES_SAVE": 		$
BEGIN
	if (event.select eq 1) then begin
		(*diva_export_info).diva_do_save_yes = 2
		widget_control,diva_export_file_base,sensitive = 1
		widget_control,diva_export_dir_base,sensitive = 1
	end
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"PRINTER_SELECT":	$
BEGIN
	(*diva_export_info).what_printer = event.index
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"JPEG_QUAL":		$
BEGIN
	widget_control,diva_jpeg_slider,get_value = value
	(*diva_export_info).diva_jpeg_qual_value		= value 
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


ELSE: MESSAGE, "Event User Value Not Found"		
ENDCASE
END ;end of diva_export event handler procedure--------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_export, pimage,pathname,file_str,diva_autoscale,   	    	    $
    	    	    	diva_whattoprint,diva_printers,diva_printers_list,	$
						image_ct,plot_ct,GROUP = GROUP
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;

CASE diva_whattoprint OF
    0:	file_type = '_all'
    1:	file_type = '_roi'
    2:	file_type = '_zoom'
ENDCASE

export_filename 	= file_str+file_type +'.jpg'
export_filename_str = file_str+file_type
export_fullfilename = pathname + export_filename;+'.jpg'

;definition of 'diva_export_type' tag
;diva_export_type   =	0   --> JPEG
;   	    	    =	1   --> TIFF
;   	    	    =	2   --> PS


IF(XRegistered("diva_export") NE 0) THEN RETURN		
;-------------------create the main base---------------------------------------;
diva_exportbase 			= WIDGET_BASE(TITLE = "DIVA EXPORT DIALOG", 	$
									/col,group_leader=GROUP,				$
									/floating,/modal)	

;-------------------filename base----------------------------------------------;
diva_export_file_base 		= widget_base(diva_exportbase,/row,sensitive=0)

diva_export_file_label  	= widget_label(diva_export_file_base,			$
									value='Filename:   ')
diva_export_file_text		= widget_text(diva_export_file_base,			$
									/editable,  							$
									/all_events,							$
    								value=export_filename,					$
									uvalue='GET_NAME',xsize=60)

;-------------------directory base---------------------------------------------;
diva_export_dir_base 		= widget_base(diva_exportbase,/row,sensitive=0)
diva_export_dir_label1  	= widget_label(diva_export_dir_base, 			$
									value='Directory:  ',					$
									/align_right)
diva_export_dir_label2  	= widget_label(diva_export_dir_base, 			$
									value=pathname, 						$
									xsize = 370,/align_left)
diva_export_dir_button		= widget_button(diva_export_dir_base,			$
									uvalue='BROWSE',						$
									value=' Change... ')

;-------------------image format base------------------------------------------;
diva_export_format_base 	= widget_base(diva_exportbase,/row)

diva_export_format_label	= widget_label(diva_export_format_base,			$
									value='Format:    ')
diva_format_base_exc		= widget_base(diva_export_format_base,			$
									/row,/exclusive)

diva_format_jpeg_butt		= widget_button(diva_format_base_exc,			$
									value='JPEG',							$
    								uvalue='DO_JPEG')
widget_control,diva_format_jpeg_butt,set_button=1

diva_format_tiff_butt		= widget_button(diva_format_base_exc,			$
									value='TIFF',	    					$
    								uvalue='DO_TIFF')
diva_format_pscr_butt		= widget_button(diva_format_base_exc,			$
									value=' PS ',	    					$
    								uvalue='DO_PS')

diva_jpeg_qual_base 		= widget_base(diva_export_format_base,			$
									/row,/frame)
diva_jpeg_qual_label_1		= widget_label(diva_jpeg_qual_base, 			$
									value = '% Quality:')


diva_jpeg_slider			= widget_slider(diva_jpeg_qual_base,			$
									uvalue = 'JPEG_QUAL',					$
									value = 75, 							$
									minimum = 30,							$
									maximum = 100)

diva_jpeg_qual_label_2		= widget_label(diva_jpeg_qual_base, 			$
									value = 'Default = 75%')


;--------------------save file base--------------------------------------------;
diva_export_save_base		= widget_base(diva_exportbase,/row)

diva_export_save_label		= widget_label(diva_export_save_base,			$
									value='Save File: ')

diva_export_save_base_exc 	= widget_base(diva_export_save_base,/row, 		$
									/exclusive)

diva_export_save_no 		= widget_button(diva_export_save_base_exc, 		$
									value='Just Print',						$
									uvalue='NO_SAVE')
widget_control,diva_export_save_no,set_button=1

diva_export_save_both 		= widget_button(diva_export_save_base_exc, 		$
									value='Print & Save',					$
									uvalue='BOTH_SAVE')

diva_export_save_yes 		= widget_button(diva_export_save_base_exc, 		$
									value='Just Save',						$
									uvalue='YES_SAVE')

;--------------------what printer base-----------------------------------------;
IF  (N_ELEMENTS(diva_printers) ge 1) 			AND 						$
	(WHERE(diva_printers[0] ne '') ne -1)   								$
THEN BEGIN

	diva_export_printer_base	= widget_base(diva_exportbase,/row)
	diva_export_printer_label	= widget_label(diva_export_printer_base,	$
										value='Printer:  ')
	diva_export_printer_list	= widget_droplist(diva_export_printer_base, $
										uvalue='PRINTER_SELECT',			$
										value=diva_printers_list)
ENDIF
;--------------------accept/cancel base----------------------------------------;
diva_export_butt_base		= widget_base(diva_exportbase,/row, 			$
									/grid_layout,/frame)

diva_export_ok_butt 		= widget_button(diva_export_butt_base,	    	$
    								value='Print',							$
									uvalue='OK_EXPORT')
	
diva_export_cancel_butt 	= widget_button(diva_export_butt_base,	    	$
    								value='Cancel', 						$
									uvalue='CANCEL_EXPORT')

;------------------------------------------------------------------------------;
WIDGET_CONTROL, diva_exportbase, /REALIZE		

diva_export_info =	Ptr_New({  												$
        pimage  					    : pimage,				 		    $
        image_ct					    : image_ct, 			 		    $
        plot_ct 					    : plot_ct,  			 		    $
        diva_exportbase 			    : diva_exportbase,  	 		    $
        pathname					    : pathname, 			 		    $
        export_filename 			    : export_filename,  	 		    $
        export_fullfilename 		    : export_fullfilename,   		    $
        diva_export_file_text		    : diva_export_file_text, 		    $
        diva_export_type			    : 0,					 		    $
        diva_autoscale_on			    : diva_autoscale[0],	 		    $
        bitshift_val				    : diva_autoscale[1],	 		    $
        diva_whattoprint			    : diva_whattoprint, 	 		    $
        diva_do_save_yes			    : 0,					 		    $
        diva_printers				    : diva_printers,		 		    $
        diva_printers_list  		    : diva_printers_list,	 		    $
        what_printer				    : 0,					 		    $
        diva_export_file_base		    : diva_export_file_base, 		    $
        diva_export_dir_base		    : diva_export_dir_base,  		    $
        diva_export_dir_label2  	    : diva_export_dir_label2,		    $
        diva_jpeg_qual_base 		    : diva_jpeg_qual_base,   		    $
        diva_jpeg_slider			    : diva_jpeg_slider, 	 		    $
        diva_jpeg_qual_value		    : 75					 		    $
						})

						
widget_control,diva_exportbase,set_uvalue=diva_export_info

XManager, "diva_export", diva_exportbase, 									$		
		EVENT_HANDLER = "diva_export_ev", 									$
		GROUP_LEADER  = GROUP		
						
END ;end of diva_export main routine-------------------------------------------;
;------------------------------------------------------------------------------;
