;------------------------------------------------------------------------------;
PRO diva_ptc_plot_ev, event
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
WIDGET_CONTROL, event.id, GET_UVALUE = eventval		
widget_control,event.top,get_uvalue=diva_ptc_plot_info

diva_ptc_plot_draw  =	(*diva_ptc_plot_info).diva_ptc_plot_draw
diva_plot_pix	    =	(*diva_ptc_plot_info).diva_plot_pix
plot_size   	    =	(*diva_ptc_plot_info).diva_ptc_plot_size
diva_ptc_plot_pos   =	(*diva_ptc_plot_info).diva_ptc_plot_pos
motion_events_on    =	(*diva_ptc_plot_info).motion_events_on
plot_ct     	    =	(*diva_ptc_plot_info).plot_ct
image_ct    	    =	(*diva_ptc_plot_info).image_ct
xyc 	    	    =	(*diva_ptc_plot_info).xyc
diva_printers		=	(*diva_ptc_plot_info).diva_printers   
diva_printers_list	=	(*diva_ptc_plot_info).diva_printers_list


CASE eventval OF

"EXIT":     	BEGIN
    	    	    WIDGET_CONTROL, event.top, /DESTROY
		    return		
	    	END
"PLOT_DRAW_EVENT":  $	;DRAW_EVENTS_DRAW_EVENTS_DRAW_EVENTS_DRAW_EVENTS_DRA
BEGIN
    widget_control,diva_ptc_plot_draw,get_value=draw_id
    wset,draw_id
    ;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ;constrain coords to within the ROI draw area
    event.x = 0 > event.x < (plot_size[0] - 1)
    event.y = 0 > event.y < (plot_size[1] - 1)
    Device,Copy=[0,0,plot_size[0],plot_size[1],0,0,diva_plot_pix]
    ;convert from DEVICE coords to DATA coords
    coords=Convert_Coord(event.x,event.y,/Device,/To_Data)
    xl= !x.crange[0] > coords[0] < !x.crange[1]
    yl= !y.crange[0] > coords[1] < !y.crange[1]
    ;report cursor (x) position....and corresponding value
    widget_control,diva_ptc_plot_pos,set_value=    	    	    	    $
    	    	    	    	    strcompress(round(xl),/remove) + ',' +  $
				    strcompress(round(yl),/remove)
    ;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    possibleEventTypes = [ 'DOWN', 'UP', 'MOTION', 'SCROLL']
    thisEvent = possibleEventTypes(event.type)

    if (thisevent eq 'DOWN') then begin
	(*diva_ptc_plot_info).motion_events_on=1
	(*diva_ptc_plot_info).xyc[0] = xl
	(*diva_ptc_plot_info).xyc[1] = yl
    endif ;of DOWN mouse event
    
    if (thisevent eq 'MOTION') then begin
    	Device,Copy=[0,0,plot_size[0],plot_size[1],0,0,diva_plot_pix]
    	xd = xl  &  yd = yl
    	;change to PLOT colour table
    	tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2]
	if (motion_events_on eq 0) then begin
	    plots,[xd,xd],[!y.crange[0],!y.crange[1]],thick=1
	    plots,[!x.crange[0],!x.crange[1]],[yd,yd],thick=1
	endif else begin
	    xs=xyc[0]  &  ys=xyc[1]
	    oplot,[xs,xs],[!y.crange[0],!y.crange[1]],color=4,thick=2
	    oplot,[xd,xd],[!y.crange[0],!y.crange[1]],color=4,thick=2
	    arrow,xs,ys,xd,ys,/data,hsize=10,/solid,color=4,thick=2
	endelse
	;change back to IMAGE colour table
    	tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2]

    endif ;of MOTION mouse event
    
    if (thisevent eq 'UP') then begin
	(*diva_ptc_plot_info).motion_events_on=0
	(*diva_ptc_plot_info).xyc[2] = xl
	(*diva_ptc_plot_info).xyc[3] = yl
	plots,[xl,xl],[!y.crange[0],!y.crange[1]],thick=1
	plots,[!x.crange[0],!x.crange[1]],[yl,yl],thick=1
    	
	diva_ptc_plot_display,diva_ptc_plot_info
	
    endif ;of DOWN mouse event

END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"PTC_PSYM": 	$   ;PTC_PSYM_PTC_PSYM_PTC_PSYM_PTC_PSYM_PTC_PSYM_PTC_PSYM_
BEGIN    
    CASE event.index OF
    	0:  (*diva_ptc_plot_info).wot_psym = 3
	1:  (*diva_ptc_plot_info).wot_psym = 2
	2:  (*diva_ptc_plot_info).wot_psym = 1
	3:  (*diva_ptc_plot_info).wot_psym = 0
	4:  (*diva_ptc_plot_info).wot_psym = 10
    ENDCASE
    diva_ptc_plot_display,diva_ptc_plot_info
    
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"PTC_PRINT":	$   ;PTC_PRINT_PTC_PRINT_PTC_PRINT_PTC_PRINT_PTC_PRINT_PTC_
BEGIN
    temp    	= (*diva_ptc_plot_info)
    f 	    	= temp.filestuff
    x 	    	= [temp.xyc[0],temp.xyc[2]]
    os_name 	= !d.name
    file_in 	= f[3] + '_ptc.ps'
    path_name   = f[1]
    
    ;call 'diva_print_what' FUNCTION to get filename and 
    result = diva_print_what(path_name,file_in,diva_printers, 	    		$
		    					diva_printers_list,temp.diva_ptc_plotbase)
    
    IF ((*result).do_print_yes eq 1) THEN BEGIN
		
		filename 		= (*result).path+(*result).filename
		what_printer	= diva_printers[(*result).what_printer]
    	do_save_yes 	= (*result).do_save_yes
		
    	print,'printing to: "'+strcompress(filename)+'"'
		
    	set_plot,'ps'
    	device,filename=filename,/color
    
    	tvlct,temp.plot_ct[*,0],temp.plot_ct[*,1],temp.plot_ct[*,2]
    
    	plot,temp.e_bar,temp.row_variance_3/2,/nodata,xtit='E_bar  /ADUs',	$
    			charsize=1.2,xstyle=1,title='CCD Transfer Curve: '+ 		$
    			' 1/m = '+strcompress(1/temp.result[1],/rem) +  			$
				' !Ne!U-!N/ADU',ytit='2_variance/2  /ADUs', 				$
				xrange=[min(x),max(x)]
    	oplot,temp.e_bar,temp.row_variance_3/2,psym=temp.wot_psym,color=2
    	oplot,temp.e_bar,temp.yfit,color=1,thick=2
    	xyouts,.02,.02,/norm,f[0],charsize=.8
    
    	tvlct,temp.image_ct[*,0],temp.image_ct[*,1],temp.image_ct[*,2]
    
    	device,/close
    	CASE os_name OF		      
    		'X':	BEGIN
						set_plot,'x'
						;call "diva_print_linux" pro to do printing!!!
						diva_print_linux,filename,do_save_yes,what_printer
					END     
			'WIN':	    set_plot,'win'    
    	ENDCASE
		
		tvlct,temp.plot_ct[*,0],temp.plot_ct[*,1],temp.plot_ct[*,2]
		
		plot,temp.e_bar,temp.row_variance_3/2,/nodata,xtit='E_bar  /ADUs',	$
				charsize=1.2,xstyle=1,title='CCD Transfer Curve: '+ 		$
				' 1/m = '+strcompress(1/temp.result[1],/rem) +  			$
				' !Ne!U-!N/ADU',ytit='2_variance/2  /ADUs', 				$
				xrange=[min(x),max(x)]
		oplot,temp.e_bar,temp.row_variance_3/2,psym=temp.wot_psym,color=2
		oplot,temp.e_bar,temp.yfit,color=1,thick=2
		xyouts,.02,.02,/norm,f[0],charsize=.8
		
		tvlct,temp.image_ct[*,0],temp.image_ct[*,1],temp.image_ct[*,2]
	ENDIF
    ;now free up the 'result' pointer
    Ptr_Free,result
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      


ELSE: MESSAGE, "Event User Value Not Found"
ENDCASE

END ;end of "diva_ptc_plot" event handler--------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_ptc_plot,e_bar,row_variance_3,yfit,result,image_ct,plot_ct,	    $
    	filestuff,diva_ptc_plotbase,diva_printers,diva_printers_list,		$
		GROUP = GROUP
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;

diva_ptc_plot_size = [600,400]
;-------------------create the main base---------------------------------------;
diva_ptc_plotbase = WIDGET_BASE(TITLE="CCD Transfer Curve:  "+filestuff[0], $
    	    	    	    	    	/col,group_leader=group,/floating)

;-------------------draw base and widget/pixmap--------------------------------;
diva_ptc_plot_draw_base=widget_base(diva_ptc_plotbase)
diva_ptc_plot_draw=widget_draw(diva_ptc_plot_draw_base,     	    	    $
    	    	xsize=diva_ptc_plot_size[0],ysize=diva_ptc_plot_size[1],    $
    	    	Button_Events=1, Event_Pro='diva_ptc_plot_ev',frame=5,	    $
		/motion_events,/viewport_events,uvalue="PLOT_DRAW_EVENT")

window,/free,/pixmap,xsize=diva_ptc_plot_size[0],ysize=diva_ptc_plot_size[1]
diva_plot_pix = !D.window
;-------------------cursor information-----------------------------------------;
diva_ptc_plot_cursor_base=widget_base(diva_ptc_plotbase,/row,/frame)
diva_ptc_plot_pos=widget_text(diva_ptc_plot_cursor_base,value='0,0',xsize=10)

diva_ptc_plot_psym=widget_droplist(diva_ptc_plot_cursor_base,	    	    $
    	    	title='Points:',uvalue='PTC_PSYM',value=['period',   	    $
		'asterisk','plus sign','line','histogram'])
		
diva_ptc_plot_print=widget_button(diva_ptc_plot_cursor_base,value=' Print ',$
    	    	uvalue='PTC_PRINT')		
;-------------------quit button------------------------------------------------;
quitbutton=widget_button(diva_ptc_plotbase, value='Quit',uvalue='EXIT')
;------------------------------------------------------------------------------;

;-------------------realise the widget-----------------------------------------;
WIDGET_CONTROL, diva_ptc_plotbase, /REALIZE

xyc= [min(e_bar),min(row_variance_3),max(e_bar),max(row_variance_3)]

diva_ptc_plot_info = Ptr_New({  	    	    	    	    	    	$
		diva_ptc_plotbase   	:   diva_ptc_plotbase,	    	            $
		e_bar	    	    	:   e_bar,  	    	    	            $
		row_variance_3	    	:   row_variance_3,  	    	            $
		yfit	    	    	:   yfit,   	    	    	            $
		result	    	    	:   result, 	    	    	            $
		diva_ptc_plot_draw  	:   diva_ptc_plot_draw,	    	            $
		diva_plot_pix	    	:   diva_plot_pix,   	    	            $
		image_ct    	    	:   image_ct,	    	    	            $
		plot_ct     	    	:   plot_ct, 	    	    	            $
		diva_ptc_plot_size  	:   diva_ptc_plot_size,	    	            $
		diva_ptc_plot_pos   	:   diva_ptc_plot_pos,	    	            $
		motion_events_on    	:   0,	    	    	    	            $
		xyc 	    	    	:   xyc,     	    	    	            $
		wot_psym    	    	:   3,	    	    	    	            $
		filestuff   	    	:   filestuff,	    	    	            $
		diva_printers			:	diva_printers,							$
		diva_printers_list		:	diva_printers_list						$
		    	    		})

diva_ptc_plot_display,diva_ptc_plot_info

widget_control,diva_ptc_plotbase,set_uvalue=diva_ptc_plot_info

XManager, "diva_ptc_plot", diva_ptc_plotbase, 	    	    	    	    $
		EVENT_HANDLER = "diva_ptc_plot_ev", 	    	    	    $
		GROUP_LEADER = GROUP,cleanup='diva_ptc_plot_cleanup'		
						
END ;end of "diva_ptc_plot" main routine---------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_ptc_plot_cleanup,diva_ptc_plotbase
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
widget_control,diva_ptc_plotbase,get_uvalue=diva_ptc_plot_info
Ptr_Free,diva_ptc_plot_info
end
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_ptc_plot_display,diva_ptc_plot_info


temp = (*diva_ptc_plot_info)

x = [temp.xyc[0],temp.xyc[2]]	    &  x = x[sort(x)]	& delx = abs(x[1]-x[0])

if ((x[1]-x[0]) lt 0) then stop

if (delx lt 10) then xr = [min(temp.e_bar),max(temp.e_bar)] ELSE xr = [x[0],x[1]]

tvlct,temp.plot_ct[*,0],temp.plot_ct[*,1],temp.plot_ct[*,2]

widget_control,temp.diva_ptc_plot_draw,get_value=draw_id
wset,draw_id

plot,temp.e_bar,temp.row_variance_3/2,/nodata,title='CCD Transfer Curve: '+ $
    	' 1/m = '+strcompress(1/temp.result[1],/rem)+' !Ne!U-!N/ADU',       $
    	ytit='2_variance/2  /ADUs',charsize=1.2,xtit='E_bar  /ADUs',	    $
	xstyle=1,xrange=xr
oplot,temp.e_bar,temp.row_variance_3/2,psym=temp.wot_psym,color=2
oplot,temp.e_bar,temp.yfit,color=1,thick=2
xyouts,.02,.02,/norm,temp.filestuff[0],charsize=.8

wset,temp.diva_plot_pix
plot,temp.e_bar,temp.row_variance_3/2,/nodata,title='CCD Transfer Curve: '+ $
    	' 1/m = '+strcompress(1/temp.result[1],/rem)+' !Ne!U-!N/ADU',       $
    	ytit='2_variance/2  /ADUs',charsize=1.2,xtit='E_bar  /ADUs',	    $
	xstyle=1,xrange=xr
oplot,temp.e_bar,temp.row_variance_3/2,psym=temp.wot_psym,color=2
oplot,temp.e_bar,temp.yfit,color=1,thick=2
xyouts,.02,.02,/norm,temp.filestuff[0],charsize=.8

tvlct,temp.image_ct[*,0],temp.image_ct[*,1],temp.image_ct[*,2]
widget_control,temp.diva_ptc_plot_draw,get_value=draw_id
wset,draw_id

END
