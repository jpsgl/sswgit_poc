;------------------------------------------------------------------------------;
pro diva_draw_all_ev,event
;------------------------------------------------------------------------------;
; Purpose -
;------------------------------------------------------------------------------;
widget_control,event.top,get_uvalue=info,/no_copy
Case tag_names(event,/structure_name) Of

"WIDGET_DRAW":	$
Begin
	WSet, info.Drawing_Area_ID
    CASE info.auto OF
		0:	CASE info.n_dim OF
					1:  TV, congrid(info.pimage,info.base_size[0],			$
														info.base_size[1])
					3:  TV, congrid(info.pimage,info.base_size[0],			$
														info.base_size[1],	$
				    	    	    					info.n_dim),true=3
			ENDCASE
		1:	CASE info.n_dim OF
					1:  TVSCL, congrid(info.pimage,info.base_size[0],		$
														info.base_size[1])
					3:  TVSCL, congrid(info.pimage,info.base_size[0],		$
														info.base_size[1],	$
				    	    	    					info.n_dim),true=3
			ENDCASE
	ENDCASE
End;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"WIDGET_BASE":	$
Begin
	Widget_Control,event.top, TLB_Get_Size=New_Base_Size
	widget_control,event.top,update = 0
	widget_control,info.all_drawwid,/destroy

	info.all_drawwid = widget_draw(info.wView,/expose_events,retain=0,		$
        									xsize=info.base_size[0],	    $
        									ysize=info.base_size[1])

	widget_control,event.top,update = 1
	;Now update base size
	info.Base_Size = [Event.X, Event.Y]
End;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


ELSE: MESSAGE, "Event User Value Not Found"			
EndCase
widget_control,event.top,set_uvalue=info,/no_copy
END;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_draw_all,pimage,n_dim,auto,group=group
;------------------------------------------------------------------------------;
; Purpose -
;------------------------------------------------------------------------------;

all_tlb = Widget_base(title='DIVA ALL',tlb_size_events=1,/col,				$
				group_leader=group,/floating)

wView = Widget_Base(all_tlb)

all_drawwid = Widget_Draw(wView,/expose_events,retain=0,xsize=400,ysize=400)


widget_control, all_tlb, /realize
widget_control, all_drawwid, Get_Value=Drawing_Area_ID

Widget_Control, all_tlb, TLB_Get_Size=Base_Size

info =  {   all_drawwid     	:   all_drawwid,    	    	    	    $
	    	Base_Size	    	:   Base_Size,      	                    $
	    	Drawing_Area_ID 	:   Drawing_Area_ID, 	                    $
	    	pimage   	    	:   pimage,  	    	            	    $
	    	n_dim   	    	:   n_dim,  	    	    	    	    $
	    	wView   	    	:   wView,   	    	    	    	    $
			auto				:	auto									$
    	}

widget_control,all_tlb,set_uvalue=info,/no_copy

xmanager,'diva_draw_all',all_tlb, EVENT_HANDLER = "diva_draw_all_ev",	    $
    	    	GROUP_LEADER = GROUP
END;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;