;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA_READ_DATA_JPG
; AUTHOR:
;		John A. Rainnie - RAL (j.a.rainnie@rl.ac.uk)
; PURPOSE:
;		This procedure queries and reads a JPEG file. A 'diva_image'
;		structure is populated with appropriate values (cf: IMG and FITS
;		files).
; INPUTS:
;		tmp_diva_image_file - a 'diva_image' structure
; OUTPUTS:
;		tmp_diva_image_file - populated with appropriate values
; PROCEDURES USED:
;		FUNCTIONS:	DIVA_IMAGE_INIT (diva_image.pro)
;					QUERY_JPEG,READ_JPEG
;-
;------------------------------------------------------------------------------;
pro diva_read_data_jpg,tmp_diva_image_file
;------------------------------------------------------------------------------;
file_info	= tmp_diva_image_file.file_info

jpeg_good = QUERY_JPEG(file_info.f,info)

IF (jpeg_good ne 1) 		THEN stop
IF (info.num_images ne 1) 	THEN stop

CASE info.channels OF
	3	:	READ_JPEG,file_info.f,image,order=1,true=3
	1	:	READ_JPEG,file_info.f,image,order=1
	ELSE:	stop
ENDCASE

n_dim	= info.channels

CASE info.pixel_type OF
	1	:	n_byt  = 1 
	ELSE:	stop
ENDCASE

tmp_diva_image_file.xsize			= info.dimensions[0]
tmp_diva_image_file.ysize			= info.dimensions[1]
tmp_diva_image_file.n_dim			= n_dim
tmp_diva_image_file.n_byt			= n_byt
tmp_diva_image_file.diva_n_images	= info.num_images

;call 'diva_image_init' function to populate 'diva_image' structure
diva_image = diva_image_init(tmp_diva_image_file,image)
;and update 'tmp_diva_image_file' structure
tmp_diva_image_file = diva_image

END
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA_GET_JPG_INFO
; AUTHOR:
;		John A. Rainnie - RAL (j.a.rainnie@rl.ac.uk)
; PURPOSE: 
;		This function returns a string array, which will be displayed in the
;		'FITS HEADER' gui. This is the JPEG information derived from
;		the IDL function 'QUERY_JPEG'
; INPUTS:
;		filename - the filename of the JPEG
; OUTPUTS:
;		header - a string array
;-
FUNCTION diva_get_jpg_info,filename
;------------------------------------------------------------------------------;

jpeg_good = QUERY_JPEG(filename,jinfo)

header  	 = strarr(n_tags(jinfo) + 1)
header[0] = 'CHANNELS	'	  + strcompress(jinfo.channels,/rem)
header[1] = 'XSIZE		'	  + strcompress(jinfo.dimensions[0],/rem)
header[2] = 'YSIZE		'	  + strcompress(jinfo.dimensions[1],/rem)
header[3] = 'HAS_PALETTE	' + strcompress(jinfo.has_palette,/rem)
header[4] = 'IMAGE_INDEX	' + strcompress(jinfo.image_index,/rem)
header[5] = 'NUM_IMAGES 	' + strcompress(jinfo.num_images,/rem)
header[6] = 'PIXEL_TYPE 	' + strcompress(jinfo.pixel_type,/rem)
header[7] = 'TYPE		'	  + strcompress(jinfo.type,/rem)
						

RETURN,header

END;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
