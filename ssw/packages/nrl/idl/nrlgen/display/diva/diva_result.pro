;------------------------------------------------------------------------------;
PRO diva_result_ev, event
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
WIDGET_CONTROL, event.id, GET_UVALUE = eventval		
widget_control,event.top,get_uvalue=diva_result_info

diva_resultbase 			= (*diva_result_info).diva_resultbase
result_info_minmax_text 	= (*diva_result_info).result_info_minmax_text
result_info_type_label		= (*diva_result_info).result_info_type_label


CASE eventval OF

"EXIT": 		$	;
BEGIN
	WIDGET_CONTROL, event.top, /DESTROY		
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"SAVE_RESULT": 	$	;
BEGIN
	;OK, this is a biggie.......
	;1. get "result_image" and it's size
	;2. get user to select a filename - initially based on date/time
	;3. create a "diva_image" structure for the "result_image"
	;4. create a comments" array - this will be added to the FITS header
	;5. write a FITS file - using the procedure "diva_make_fits_file"
	;6. append new "diva_image" structure to "diva_stored_image" array

	;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!;
	;																		;
	;	1. get "result_image" and it's size									;
	;																		;
	;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!;
	result_image    = (*(*(*diva_result_info).diva_math_info).result_image)
	save_name_size  = size(result_image)
    xsize   	    = save_name_size[1]
    ysize   	    = save_name_size[2]
	
	index_1			= (*(*diva_result_info).diva_math_info).index[0]
	index_2			= (*(*diva_result_info).diva_math_info).index[1]
	first_image 	= (*(*(*diva_result_info).diva_math_info).				$
												diva_image_stored)[index_1]
	
	;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!;
	;																		;
	;	2. time to get a filename - offer 'path+"temp.fit"' 				;
	;																		;
	;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!;
	;get the pathname of the "first_image"
	path 	= first_image.file_info.pathname

	;name is based on time and date --> "DD_MM_YY__HH_MM_SS.fit"
	get_date,this_date
			day 	= strmid(this_date,1,/reverse_offset)
			month	= strmid(this_date,5,2)
			year	= strmid(this_date,0,4)

	t = systime()	&	this_time = strmid(t,11,8)
			hour	= strmid(this_time,0,2)
			minute	= strmid(this_time,3,2)
			second	= strmid(this_time,1,/reverse_offset)
	;make name
	time_name=  day  + '_' + month  + '_' + year + '__' +  					$
				hour + '_' + minute + '_' + second

	name 	= path + time_name + '.fit'

	;call "diva_get_filename" FUNCTION to return user-selected "get_f"
	get_f 	= diva_get_filename(name,path,diva_resultbase)

	save_name_mem   =	strcompress(get_f,/remove)	+ '('   	+ 			$
						strcompress(xsize,/remove)	+ ','   	+			$
						strcompress(ysize,/remove)	+ ')(1,0)'
	save_name_file  = 	strcompress(get_f,/remove)


	;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!;
	;																		;
	;	3. create a "diva_image" type structure								;
	;																		;
	;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!;
	diva_file						= {diva_file}
	diva_file_init,diva_file,save_name_file

	diva_image_result 				= {diva_image}
	diva_image_result.file_info 	= diva_file
	diva_image_result.xsize 		= xsize
	diva_image_result.ysize 		= ysize
	diva_image_result.n_dim 		= 1
	diva_image_result.i_max 		= max(result_image)
	diva_image_result.i_min 		= min(result_image)
	diva_image_result.ops_bshift	= [1,0]
	diva_image_result.ops_name  	= save_name_mem
	diva_image_result.diva_n_images = 1
	diva_image_result.pimage		= Ptr_New(result_image)
	diva_image_result.o_pimage		= diva_image_result.pimage
	;----------------------------------------------------------------;
	;create preview image--------------------------------------------;
	;----------------------------------------------------------------;
	diva_prev_image_congrid,diva_image_result.pimage,p_pimage,		$
							xsize,ysize,diva_image_result.n_dim[0], $
							prev_xsize,prev_ysize
	diva_image_result.p_pimage	 	= p_pimage
	diva_image_result.prev_xsize	= prev_xsize
	diva_image_result.prev_ysize	= prev_ysize
	;----------------------------------------------------------------;
	;create small image----------------------------------------------;
	;----------------------------------------------------------------;
	diva_small_image_congrid,diva_image_result.pimage,s_pimage, 	$
							xsize,ysize,diva_image_result.n_dim[0],	$
							prev_xsize,	prev_ysize
	diva_image_result.s_pimage	 	= s_pimage
	;----------------------------------------------------------------;

	;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!;
	;																		;
	;	4. create comments array											;
	;																		;
	;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!;
	file_info	= first_image.file_info
	mfile_name	= first_image.mfile_name

	
	comment_0 = ' This image is the result of the following calculation :-'
	
	if (mfile_name eq '') then begin
		comment_1 = ' Operand1     :  ' + file_info.f
		comment_4 = ' Operand1 img :  N/A' 
	endif else begin
		comment_1 = ' Operand1     :  ' + file_info.pathname + 				$
						file_info.file_str + '__' + 						$
						mfile_name  + '.' + file_info.file_ext
						
		comment_4 = ' Operand1 img :  ' + mfile_name		
	endelse
	
	comment_2 = ' Operand1 file:  ' + file_info.filename
	comment_3 = ' Operand1 path:  ' + file_info.pathname
	
	diva_math_function = (*(*diva_result_info).diva_math_info).diva_math_function
	CASE diva_math_function OF
		0:	comment_5 = '+'
		1:	comment_5 = '-'
		2:	comment_5 = '*'
		3:	comment_5 = '-'
		4:	comment_5 = 'average...'
		5:	comment_5 = 'blended with...'
	ENDCASE
	comment_5 = ' Operator     :  ' + comment_5
	
	diva_constant_opr = (*(*diva_result_info).diva_math_info).diva_constant_opr
	CASE diva_constant_opr OF
		0:	BEGIN
				;2 images
				second_image= (*(*(*diva_result_info).diva_math_info).		$
									diva_image_stored)[index_1]
												
				file_info	= second_image.file_info
				mfile_name	= second_image.mfile_name
				
				if (mfile_name eq '') then begin
					comment_6 = ' Operand2     :  ' + file_info.f
					comment_9 = ' Operand2 img :  N/A'
				endif else begin
					comment_6 = ' Operand2     :  ' + file_info.pathname +	$
									file_info.file_str + '__' + 			$
									mfile_name  + '.' + file_info.file_ext
					
					comment_9 = ' Operand2 img :  ' + mfile_name
				endelse
				comment_7 = ' Operand2 file:  ' + file_info.filename
				comment_8 = ' Operand2 path:  ' + file_info.pathname
			END
		1:	BEGIN
				;1 image and constant
				diva_constant_val = (*(*diva_result_info).diva_math_info).	$
														diva_constant_val
				wot = strcompress(diva_constant_val,/rem)
				comment_6 = ' Operand2     :  ' 		+ wot
				comment_7 = ' Operand2 file:  N/A'
				comment_8 = ' Operand2 path:  N/A'
				comment_9 = ' Operand2 img :  N/A'
				
			END
	ENDCASE
	
	comments = [comment_0,comment_1,comment_2,comment_3,comment_4,			$
				comment_5,comment_6,comment_7,comment_8,comment_9]


	;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!;
	;																		;
	;	5. write a FITS file												;
	;																		;
	;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!;
	;call "diva_make_fits_file" procedure to make a fits file
	wid_id			= diva_resultbase
	func			= 'diva_math'
	bsname 			= ''
	bshift_val		= 0
	
	diva_make_fits_file,diva_image_result,index,comments, 					$
											wid_id,func,bsname,bshift_val


	;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!;
	;																		;
	;	6. append new "diva_image" structure to "diva_stored_image" array	;
	;																		;
	;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!;
	
	old_diva_image_stored = (*(*(*diva_result_info).diva_math_info). 		$
														diva_image_stored)
	
	;concatenate 'diva_stored_image' with result structure
	new_diva_image_stored = [old_diva_image_stored,diva_image_result]
	;update 'diva_image_stored' array
	(*(*(*diva_result_info).diva_math_info).diva_image_stored) =  			$
														new_diva_image_stored

	;append lists with new entry
	diva_math_list = new_diva_image_stored[*].ops_name
	diva_math_list_mini = strarr(n_elements(diva_math_list))

	FOR i=0,n_elements(diva_math_list)-1 DO BEGIN
		diva_math_list[i] 		= '['  + strcompress(i+1,/rem) 		+		$
								  '] ' + diva_math_list[i]

		diva_math_list_mini[i]	= '['  + strcompress(i+1,/rem)  	+		$
								  '] ' + strmid(diva_math_list[i],			$
										 strpos(diva_math_list[i],			$
										'/',/reverse_search)+1)

		diva_math_list[i] 		= 		strmid(diva_math_list[i],0, 		$
										strpos(diva_math_list[i],'('))
	ENDFOR

	;update the list widgets
	widget_control,(*(*diva_result_info).diva_math_info).					$
							diva_op1_list_id,set_value = diva_math_list
	widget_control,(*(*diva_result_info).diva_math_info).					$
							diva_op2_list_id,set_value = diva_math_list_mini
	;sensitize/desensitize widgets accordingly
	widget_control,(*(*diva_result_info).diva_math_info).					$
										diva_math_sum_doit, sensitive = 0

	;re-highlight the 'first' image entry
	widget_control,(*(*diva_result_info).diva_math_info).					$
								diva_op1_list_id,set_list_select = index_1
	
	widget_control,(*(*diva_result_info).diva_math_info).					$
										diva_math_sum_second,set_value = ''  

	
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"CURR_RESULT": 	$	;
BEGIN
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"TYPE_CHANGE": 	$	;
BEGIN
	type_index = (*diva_result_info).type_index
	IF (type_index ne event.index) THEN BEGIN
	
		(*diva_result_info).type_index = event.index
		
		
		result_image = (*(*(*diva_result_info).diva_math_info).result_image)
		CASE event.index OF
			0:	result_image = FLOAT(result_image)
			1:	BEGIN
					l_indicies = where(result_image lt 0	,count)
					IF (count ne 0) THEN result_image[l_indicies] = 0
					
					u_indicies = where(result_image gt 65535,count)
					IF (count ne 0) THEN result_image[u_indicies] = 65535
					
					result_image = UINT(result_image)
				END
		ENDCASE
		;update 
		(*(*(*diva_result_info).diva_math_info).result_image) = result_image
		;get min/max values and display
		minv = min(result_image)
		maxv = max(result_image)
		widget_control,result_info_minmax_text, 				$
					set_value = 	strcompress(minv,/rem) + 	$
							',' + 	strcompress(maxv,/rem)
		
		;get data type and display
		data_type		= size(result_image,/type)
		
		CASE data_type OF
			4:		data_type_str	= 'floating point (32-bit)'
			12: 	data_type_str	= 'unsigned integer (16-bit)'
			ELSE:	data_type_str	= 'unknown (unknown)'
		ENDCASE
		widget_control,result_info_type_label,set_value = data_type_str
		
	ENDIF
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!




ELSE: MESSAGE, "Event User Value Not Found"		
ENDCASE

END ;end of "diva_result" event handler----------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_result,diva_math_info, GROUP = GROUP
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;

IF(XRegistered("diva_result") NE 0) THEN RETURN		

result_image	= (*(*diva_math_info).result_image)
;stop
minv			= min(result_image)
maxv			= max(result_image)

data_type		= size(result_image,/type)

CASE data_type OF
	4:		data_type_str	= 'floating point (32-bit)    '
	ELSE:	data_type_str	= 'unknown (unknown)          '
ENDCASE


;-------------------create the main base---------------------------------------;
diva_resultbase 			= WIDGET_BASE(TITLE = "DIVA - MATH RESULT", 	$
									/col,									$
									/floating, 								$
									/modal,									$
									group_leader=GROUP)


diva_result_topbase 		= widget_base(diva_resultbase,/row)	

;-------------------draw area--------------------------------------------------;
diva_result_draw			= widget_draw(diva_result_topbase,xs=400,ys=400)
;-------------------rhs base---------------------------------------------------;
diva_result_info_base		= widget_base(diva_resultbase,/row,/frame)


result_info_minmax_base 	= widget_base(diva_resultbase,/row,/frame)
result_info_minmax_label	= widget_label(result_info_minmax_base, 		$
									value = 'min/max values:')

result_info_minmax_text		= widget_text(result_info_minmax_base,			$
									value = strcompress(minv,/rem) + 		$
									',' + strcompress(maxv,/rem))




result_info_type_base		= widget_base(diva_resultbase,/row,/frame)
result_info_type_0			= widget_label(result_info_type_base,			$
									value = 'DATA TYPE:')

result_info_type_label		= widget_label(result_info_type_base,			$
									value = data_type_str)



result_info_type_droplist 	= widget_droplist(result_info_type_base,		$
									value = ['float','uint'],				$
									uvalue= 'TYPE_CHANGE',					$
									title = 'Change type:')


;-------------------button base------------------------------------------------;
diva_result_button_base 	= widget_base(diva_resultbase,/row,/frame,		$
									/grid_layout)

diva_result_save			= widget_button(diva_result_button_base,		$
									value = 'Save image (FITS)',			$
									uvalue = 'SAVE_RESULT')
diva_result_curr			= widget_button(diva_result_button_base,		$
									value = 'Make current image',			$
									uvalue = 'CURR_RESULT')
;-------------------quit button------------------------------------------------;
quitbutton					= widget_button(diva_resultbase,				$
									value='Quit',uvalue='EXIT')
;------------------------------------------------------------------------------;

;-------------------realise the widget-----------------------------------------;
WIDGET_CONTROL, diva_resultbase, /REALIZE		


diva_result_info = Ptr_New({												$
				diva_resultbase 			:	diva_resultbase,			$
				diva_math_info				:	diva_math_info,				$
				type_index					:	0,							$
				result_info_minmax_text 	:	result_info_minmax_text, 	$
				result_info_type_label		:	result_info_type_label		$
						  })


widget_control,diva_resultbase,set_uvalue = diva_result_info



widget_control,diva_result_draw,get_value = result_draw
wset,result_draw
tvscl,congrid(result_image,400,400)
						


XManager, "diva_result", diva_resultbase, $		
		EVENT_HANDLER = "diva_result_ev", $
		GROUP_LEADER = GROUP,cleanup='diva_result_cleanup'		
						
END ;end of "diva_result" main routine-----------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_result_cleanup,diva_resultbase

widget_control,diva_resultbase,get_uvalue=diva_result_info
Ptr_Free,diva_result_info

END
