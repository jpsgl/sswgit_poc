;------------------------------------------------------------------------------;
pro diva_draw_no_image,pdiva_draw
;------------------------------------------------------------------------------;
; Purpose - to destroy main DIVA draw area
;------------------------------------------------------------------------------;

tlb 	    	    =   (*pdiva_draw).tlb
tlb_mid_base	    =   (*pdiva_draw).tlb_mid_base
diva_draw_base      =   (*pdiva_draw).diva_draw_base
draw_size   	    =	(*pdiva_draw).tlb_drawwid_size

widget_control,tlb,update=0
widget_control,diva_draw_base,/destroy

diva_draw_base  			= widget_base(tlb_mid_base,/col)


;--------------------------------------------------------------------------;
; tlb_top_base - autoscale/bitshifting/etc---------------------------------;
;--------------------------------------------------------------------------;
tlb_top_base 				= widget_base(diva_draw_base,/row,/frame,		$
									sensitive=0)
(*pdiva_draw).tlb_top_base  = tlb_top_base

tlb_auto_base 				= widget_base(tlb_top_base,/row,/frame)

tlb_auto_label				= widget_label(tlb_auto_base,					$
									value='Autoscale:')

tlb_auto_base_exc			= widget_base(tlb_auto_base,/row,/exclusive)

diva_auto_on				= widget_button(tlb_auto_base_exc,				$
									value='On', 							$
									uvalue='AUTOSC_ON')
diva_auto_off				= widget_button(tlb_auto_base_exc,				$
									value='Off',							$
									uvalue='AUTOSC_OFF')
widget_control,diva_auto_on,set_button = 1

tlb_bshift_base 			= widget_base(tlb_top_base, 					$
									/row,/frame,sensitive=1)
tlb_bshift_label			= widget_label(tlb_bshift_base, 				$
									value='Bitshift:')
tlb_bshift_down 			= widget_button(tlb_bshift_base,				$
									value=' < ',     	    				$
    								uvalue='BSHFT_DOWN')
tlb_bshift_text 			= widget_text(tlb_bshift_base,xsize=4,			$
									value='0')	
tlb_bshift_up				= widget_button(tlb_bshift_base,				$
									value=' > ',     	    				$
    								uvalue='BSHFT_UP')
tlb_bshift_save 			= widget_button(tlb_bshift_base,				$
									value='Save',     	    				$
    								uvalue='BSHFT_SAVE')
;--------------------------------------------------------------------------;
; tlb draw areas 'diva_tlb_drawwid' and 'diva_tlb_pixindex'----------------;
;--------------------------------------------------------------------------;
diva_tlb_drawwid 			= widget_draw(diva_draw_base,/scroll,        	$
    								xsize=draw_size[0], 					$
									ysize=draw_size[1], 					$
									frame=6,	    	    	    		$
									x_scroll_size=draw_size[0], 			$
									y_scroll_size=draw_size[1])
(*pdiva_draw).diva_tlb_drawwid  	= diva_tlb_drawwid
	

window,/free,xsize=draw_size[0],ysize=draw_size[1],/pixmap
(*pdiva_draw).diva_tlb_pixindex = !d.window

widget_control,tlb,TLB_Set_Title = 'DIVA  --> no image selected'

;--------------------------------------------------------------------------;
; create info base---------------------------------------------------------;
;--------------------------------------------------------------------------;

tlb_info_base				= widget_base(diva_draw_base, 					$
									/row,/frame)

tlb_info_curs_t 			= widget_text(tlb_info_base,    	    		$
    	    	    	    		value='0,0',xsize=10)

tlb_info_valu_t 			= widget_text(tlb_info_base,    	    		$
    	    	    	    		value='0',xsize=8)

tlb_info_func_label 		= widget_label(tlb_info_base,					$
									value=' Function:')

tlb_info_func_text  		= widget_text(tlb_info_base,					$
									xsize = 16, 							$
    	    	    	    		value = '')	
							
tlb_info_size_label 		= widget_label(tlb_info_base,					$
									value=' Size:')
									
tlb_info_size_text  		= widget_text(tlb_info_base,					$
									xsize = 10,     						$
    	    	    	    		value = '')			    

widget_control,tlb,update=1

(*pdiva_draw).diva_draw_base 		= diva_draw_base
(*pdiva_draw).tlb_bshift_base 		= tlb_bshift_base
(*pdiva_draw).tlb_bshift_text 		= tlb_bshift_text
(*pdiva_draw).tlb_info_base 		= tlb_info_base
(*pdiva_draw).tlb_info_curs_t 		= tlb_info_curs_t
(*pdiva_draw).tlb_info_valu_t 		= tlb_info_valu_t
(*pdiva_draw).tlb_info_func_text 	= tlb_info_func_text
(*pdiva_draw).tlb_info_size_text	= tlb_info_size_text
end
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_draw_create,pdiva_draw
;------------------------------------------------------------------------------;
; Purpose -
;------------------------------------------------------------------------------;

tlb 	    	    =   (*pdiva_draw).tlb
tlb_mid_base	    =   (*pdiva_draw).tlb_mid_base
diva_draw_base      =   (*pdiva_draw).diva_draw_base
n_dim	    	    =   (*pdiva_draw).diva_current_image.n_dim
draw_size   	    =	(*pdiva_draw).tlb_drawwid_size
xsize	    	    =	(*pdiva_draw).diva_current_image.xsize
ysize	     	    =	(*pdiva_draw).diva_current_image.ysize
pimage      	    =   (*pdiva_draw).diva_current_image.pimage

widget_control,tlb,update = 0

wdelete,(*pdiva_draw).diva_tlb_pixindex

;ok - destroy 'diva_draw_base'
widget_control,diva_draw_base,/destroy

;and (1) make it again and (2) update into 'info'
diva_draw_base  			= widget_base(tlb_mid_base,/col)
;--------------------------------------------------------------------------;
; tlb_top_base - autoscale/bitshifting/etc---------------------------------;
;--------------------------------------------------------------------------;
tlb_top_base 				= widget_base(diva_draw_base,/row,/frame)
tlb_auto_base 				= widget_base(tlb_top_base,/row,/frame)

tlb_auto_label				= widget_label(tlb_auto_base,					$
									value='Autoscale:')

tlb_auto_base_exc			= widget_base(tlb_auto_base,/row,/exclusive)

diva_auto_on				= widget_button(tlb_auto_base_exc,				$
									value='On',uvalue='AUTOSC_ON')

diva_auto_off				= widget_button(tlb_auto_base_exc,				$
									value='Off',uvalue='AUTOSC_OFF')
widget_control,diva_auto_on,set_button = 1

tlb_bshift_base 			= widget_base(tlb_top_base, 					$
									/row,/frame,sensitive=0)
tlb_bshift_label			= widget_label(tlb_bshift_base, 				$
									value='Bitshift:')
tlb_bshift_down 			= widget_button(tlb_bshift_base,				$
									value=' < ',							$
									uvalue='BSHFT_DOWN')
tlb_bshift_text 			= widget_text(tlb_bshift_base,xsize=4,			$
									value='0')	
tlb_bshift_up				= widget_button(tlb_bshift_base,				$
									value=' > ',							$
									uvalue='BSHFT_UP')
tlb_bshift_save 			= widget_button(tlb_bshift_base,				$
									value='  Save  ',						$
    								uvalue='BSHFT_SAVE')

(*pdiva_draw).diva_draw_base 	= diva_draw_base
(*pdiva_draw).tlb_top_base  	= tlb_top_base
(*pdiva_draw).tlb_bshift_base 	= tlb_bshift_base
(*pdiva_draw).tlb_bshift_text 	= tlb_bshift_text
(*pdiva_draw).tlb_bshift_save 	= tlb_bshift_save

;--------------------------------------------------------------------------;
; tlb draw areas 'diva_tlb_drawwid' and 'diva_tlb_pixindex'----------------;
;--------------------------------------------------------------------------;
diva_tlb_drawwid			= widget_draw(diva_draw_base,					$
									xsize=xsize,ysize=ysize,  	    		$
        							/scroll,								$
									x_scroll_size=draw_size[0], 			$
									y_scroll_size=draw_size[1], 	    	$
        							Button_Events=1,						$
									Event_Pro='diva_draw_events',	    	$
        							/motion_events, 						$
									/viewport_events,						$
									frame=6,	    	    				$
        			uvalue={eventval:"DRAW_EVENT", pdiva_draw: pdiva_draw})
(*pdiva_draw).diva_tlb_drawwid = diva_tlb_drawwid
;and make pixmap window (store its id)
window,/free,xsize=xsize,ysize=ysize,/pixmap
(*pdiva_draw).diva_tlb_pixindex = !d.window

;--------------------------------------------------------------------------;
; create info base---------------------------------------------------------;
;--------------------------------------------------------------------------;
tlb_info_base				= widget_base(diva_draw_base,/row,/frame)

tlb_info_curs_t 			= widget_text(tlb_info_base,value='0,0',xsize=10)


CASE n_dim OF
    1:	tlb_info_valu_t 	= widget_text(tlb_info_base,    	    		$
    	    	    	    		value='0',xsize=8)
    3:	tlb_info_valu_t 	= widget_text(tlb_info_base,    	    		$
    	    	    	    		value='0,0,0',xsize=18)
ENDCASE			


tlb_info_func_label 		= widget_label(tlb_info_base,					$
									value=' Function:')

tlb_info_func_text  		= widget_text(tlb_info_base,xsize = 16,     	$
    	    	    	    		value = 'Regular ROI')			    


CASE (*pdiva_draw).diva_fit_size OF
    0:	size_text = 'Original'
    1:	size_text = 'Best Fit'
    2:	size_text = 'Height'
    3:	size_text = 'Width'
ENDCASE    
tlb_info_size_label 		= widget_label(tlb_info_base,value=' Size:')
tlb_info_size_text  		= widget_text(tlb_info_base,xsize = 10,     	$
    	    	    	    		value = size_text)			


(*pdiva_draw).tlb_info_base 		= tlb_info_base
(*pdiva_draw).tlb_info_curs_t 		= tlb_info_curs_t
(*pdiva_draw).tlb_info_valu_t 		= tlb_info_valu_t
(*pdiva_draw).tlb_image_function 	= 1			    
(*pdiva_draw).tlb_info_func_text 	= tlb_info_func_text
(*pdiva_draw).tlb_info_size_text 	= tlb_info_size_text
							    
;--------------------------------------------------------------------------;
;--------------------------------------------------------------------------;
			    
widget_control,tlb,update = 1
end;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_draw_image,pdiva_draw
;------------------------------------------------------------------------------;
; Purpose -
;------------------------------------------------------------------------------;
diva_tlb_drawwid    =	(*pdiva_draw).diva_tlb_drawwid
diva_tlb_pixindex   =	(*pdiva_draw).diva_tlb_pixindex
pimage	    	    =	(*pdiva_draw).diva_current_image.pimage
n_dim	    	    =	(*pdiva_draw).diva_current_image.n_dim
xsize	    	    =   (*pdiva_draw).diva_current_image.xsize
ysize	    	    =   (*pdiva_draw).diva_current_image.ysize
diva_autoscale_on   =	(*pdiva_draw).diva_autoscale[0]
bitshift_val	    =	(*pdiva_draw).diva_autoscale[1]


widget_control,diva_tlb_drawwid,get_value=tlb_draw
wset,tlb_draw

CASE diva_autoscale_on OF
    0:  IF (n_dim eq 1) then    tv,ishft(*pimage,bitshift_val)     	    	$
    	ELSE    	    		tv,ishft(*pimage,bitshift_val),true=3
    1:  IF (n_dim eq 1) then 	tvscl,*pimage	    	    	    	    $
    	ELSE	    	    	tvscl,*pimage,true=3
ENDCASE

wset,diva_tlb_pixindex

CASE diva_autoscale_on OF
    0:  IF (n_dim eq 1) then    tv,ishft(*pimage,bitshift_val)	    	    $
    	ELSE    	    		tv,ishft(*pimage,bitshift_val),true=3
    1:  IF (n_dim eq 1) then 	tvscl,*pimage	    	    	    	    $
    	ELSE	    	    	tvscl,*pimage,true=3
ENDCASE


;;stats will be calculated and updated when 'Statistics' has been selected

END ; of "diva_draw_image" procedure-------------------------------------------;
;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA_DRAW_PREV
; PURPOSE:
;		This procedure displays the preview image
; INPUTS:
;		prev_draw	- widget id of preview draw area
;		prev_pix	- widget id of preview draw pixmap
;		n_dim		- number of planes of image
;		p_pimage	- the preview image
; NOTES:
;		The preview image is always diaplyed with TVSCL
; AUTHOR:
;		John A. Rainnie - RAL (j.a.rainnie@rl.ac.uk)
;-
PRO diva_draw_prev,prev_draw,prev_pix,n_dim,p_pimage
;------------------------------------------------------------------------------;
wset,prev_draw  & erase
wset,prev_pix	& erase

wset,prev_draw	;preview draw area
IF (n_dim eq 1) THEN    tvscl,(*p_pimage)   ELSE    tvscl,(*p_pimage),true=3

wset,prev_pix	;preview pixmap
IF (n_dim eq 1) THEN    tvscl,(*p_pimage)   ELSE    tvscl,(*p_pimage),true=3

END ; of "diva_draw_prev" procedure--------------------------------------------;
;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA_DRAW_PREV_VIEWPORT
; INPUTS:
;		pdiva_draw
; PURPOSE:
;		This procedure draws a (cyan) rectangle on the preview image
;		representing the viewport. This will move after scrolling the main
;		image draw area.
; RESTRICTIONS:
;		This will only occur if the preview and current images are the same.
; AUTHOR:
;		John A. Rainnie - RAL (j.a.rainnie@rl.ac.uk)
;-
;------------------------------------------------------------------------------;
PRO diva_draw_prev_viewport,pdiva_draw
;------------------------------------------------------------------------------;
f				=	(*pdiva_draw).diva_current_image.file_info.f
curr_mfile_name =   (*pdiva_draw).diva_current_image.mfile_name
prev_mfile_name =   (*pdiva_draw).diva_current_prev_image.mfile_name
prev_f			= 	(*pdiva_draw).diva_current_prev_image.file_info.f

IF (curr_mfile_name eq prev_mfile_name) AND (f eq prev_f) then BEGIN

	;x and y size of image
	xsize 		= (*pdiva_draw).diva_current_image.xsize
	ysize 		= (*pdiva_draw).diva_current_image.ysize
	prev_xsize	= (*pdiva_draw).diva_current_image.prev_xsize
	prev_ysize	= (*pdiva_draw).diva_current_image.prev_ysize
	;x and y size of draw viewport
	tlb_drawwid_xsize = (*pdiva_draw).tlb_drawwid_size[0]
	tlb_drawwid_ysize = (*pdiva_draw).tlb_drawwid_size[1]

	if (xsize lt tlb_drawwid_xsize) then begin
    	tlb_drawwid_xsize=xsize
    	tlb_drawwid_ysize=tlb_drawwid_ysize+20
	end
	if (ysize lt tlb_drawwid_ysize) then begin
    	tlb_drawwid_ysize=ysize
    	tlb_drawwid_xsize=tlb_drawwid_xsize+20
	end

	x_ratio = float(prev_xsize)/xsize
	y_ratio = float(prev_ysize)/ysize

	;get current viewport (x and y at lower left corner)
	widget_control,(*pdiva_draw).diva_tlb_drawwid,get_draw_view=draw_view

	;lower lhs coords - PREVIEW 
	llhs = [draw_view[0]*x_ratio,draw_view[1]*y_ratio]
	;upper rhs coords - PREVIEW 
	urhs = [(draw_view[0]+tlb_drawwid_xsize-1)*x_ratio,			$
    		(draw_view[1]+tlb_drawwid_ysize-1)*y_ratio ]
	;make sure the urhs values are 249 at max
	urhs[0] = urhs[0] < 248
	urhs[1] = urhs[1] < 248

	llhs = round(llhs)
	urhs = round(urhs)

	delx = urhs[0]-llhs[0]
	dely = urhs[1]-llhs[1]

	(*pdiva_draw).tlb_prev_coords = [llhs[0],llhs[1],urhs[0],urhs[1],delx,dely]

	widget_control,(*pdiva_draw).tlb_prev_drawwid,get_value=prev_draw
	wset,prev_draw

	plot_ct     = (*pdiva_draw).plot_ct
	image_ct    = (*pdiva_draw).image_ct

	tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2];PLOT colour table
	Device,Copy=[0,0,250,250,0,0,(*pdiva_draw).tlb_prev_pixid]
	plots,      [llhs[0],llhs[0],urhs[0],urhs[0],llhs[0]],  		$
    	    	[llhs[1],urhs[1],urhs[1],llhs[1],llhs[1]],  		$
    	    	/device,color=4,thick=2
	tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2];IMAGE colour table
ENDIF
END ; of "diva_draw_prev_viewport" procedue------------------------------------;
;------------------------------------------------------------------------------;
;+;
; NAME:
;		DIVA_PREV_DRAW_EVENTS
; CATEGORY:
;		event handler (of preview image draw area)
; PURPOSE:
;		This procedure maps and unmaps buttons in the preview draw area
; INPUTS:
;		event --> "pdiva_draw"
; PROCEDURE:
;		A right mouse click while over the preview draw area will toggle
;		(map/unmap) buttons
; AUTHOR:
;		John A. Rainnie - RAL (j.a.rainnie@rl.ac.uk)
;-
;------------------------------------------------------------------------------;
pro diva_prev_draw_events,event
;------------------------------------------------------------------------------;
WIDGET_CONTROL, event.id , GET_UVALUE = event_struct
pdiva_draw = event_struct.pdiva_draw

prev_draw_buttons_show	= (*pdiva_draw).prev_draw_buttons_show

what_button_pressed=['NONE','LEFT','MIDDLE','NONE','RIGHT']
thisbutton = what_button_pressed(event.press)
IF (thisbutton eq 'RIGHT') THEN BEGIN
	CASE prev_draw_buttons_show OF
		0:	(*pdiva_draw).prev_draw_buttons_show = 1
		1:	(*pdiva_draw).prev_draw_buttons_show = 0
	ENDCASE
	
	widget_control,(*pdiva_draw).diva_prev_button_base, 					$
								map = (*pdiva_draw).prev_draw_buttons_show
ENDIF

END ; of "diva_prev_draw_events" procedure-------------------------------------;
;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA_DRAW_EVENTS
; CATEGORY:
;		event handler
; PURPOSE:
;		This procedure handles the events of the main draw area, including
;		reporting cursor position and pixel value and selecting regions of
;		interest using the cursor.
; INPUTS:
;		event --> "pdiva_draw"
; AUTHOR:
;		John A. Rainnie - RAL (j.a.rainnie@rl.ac.uk)
;-
;------------------------------------------------------------------------------;
PRO diva_draw_events,event
;------------------------------------------------------------------------------;
WIDGET_CONTROL, event.id , GET_UVALUE = event_struct
pdiva_draw = event_struct.pdiva_draw

tlb 	    	    =	(*pdiva_draw).tlb
xsize 	    	    = 	(*pdiva_draw).diva_current_image.xsize
ysize 	    	    = 	(*pdiva_draw).diva_current_image.ysize
prev_xsize			= 	(*pdiva_draw).diva_current_image.prev_xsize
prev_ysize			= 	(*pdiva_draw).diva_current_image.prev_ysize
n_dim	    	    =	(*pdiva_draw).diva_current_image.n_dim
pimage      	    = 	(*pdiva_draw).diva_current_image.pimage
motion_events_on    =	(*pdiva_draw).motion_events_on
diva_tlb_drawwid    =	(*pdiva_draw).diva_tlb_drawwid
diva_tlb_pixindex   = 	(*pdiva_draw).diva_tlb_pixindex
diva_autoscale_on   =   (*pdiva_draw).diva_autoscale[0]
bitshift_val	    =	(*pdiva_draw).diva_autoscale[1]
diva_draw_base	    =	(*pdiva_draw).diva_draw_base
xs  	    	    =	(*pdiva_draw).xs
ys  	    	    =	(*pdiva_draw).ys
xd  	    	    =	(*pdiva_draw).xd
yd  	    	    =	(*pdiva_draw).yd
diva_line_info	    =	(*pdiva_draw).diva_line_info
diva_zoom_info	    =	(*pdiva_draw).diva_zoom_info
diva_roi_info	    =	(*pdiva_draw).diva_roi_info
os_name     	    =	(*pdiva_draw).os_name
t					=	(*pdiva_draw).tlb_info_func_text
diva_printers		=	(*pdiva_draw).diva_printers
diva_printers_list	=	(*pdiva_draw).diva_printers_list

IF (PTR_VALID(diva_zoom_info) ne 0) THEN BEGIN
    diva_zoom_size      =   (*(*pdiva_draw).diva_zoom_info).diva_zoom_size
    diva_zoom_mag       =   (*(*pdiva_draw).diva_zoom_info).diva_zoom_mag
    diva_zoom	    	=   diva_zoom_size/diva_zoom_mag
ENDIF
;get colours
image_ct    	    = 	(*pdiva_draw).image_ct
plot_ct     	    = 	(*pdiva_draw).plot_ct
;rubberband box
rband_c     	    =	(*pdiva_draw).diva_rband_box[0]
rband_t     	    =	(*pdiva_draw).diva_rband_box[1]
;image function
tlb_image_function  = 	(*pdiva_draw).tlb_image_function
;1  -->     regular ROI
;2  -->     irregular ROI
;3  -->     circular ROI
;4  -->     ZOOM
;6  -->     Horz/Vert LINE profile
;7  -->     Entire ROW LINE profile
;8  -->     Entire COL LINE profile
;9  -->     ANY LINE profile
;file_stuff
pathname    = (*pdiva_draw).diva_current_image.file_info.pathname
f   	    = (*pdiva_draw).diva_current_image.file_info.f
file_str    = (*pdiva_draw).diva_current_image.file_info.file_str
mfile_str   = (*pdiva_draw).diva_current_image.mfile_name_str

file_stuff = [f,pathname,file_str,mfile_str]



widget_control,diva_tlb_drawwid, get_value=tlb_draw
wset,tlb_draw

CASE event_struct.eventval OF

"DRAW_EVENT":  $ 
BEGIN
    event.x = 0 > event.x < (xsize - 1)
    event.y = 0 > event.y < (ysize - 1)
    xc = event.x & yc = event.y
	
	
    widget_control,(*pdiva_draw).tlb_info_curs_t,set_value= 	    	    $
    	    	    	    	    	strcompress(xc,/rem) + ',' +        $
		    	    	    			strcompress(yc,/rem)

	IF (size(*pimage,/type) eq 1) THEN BEGIN
    	IF (n_dim eq 1) THEN widget_control,(*pdiva_draw).tlb_info_valu_t,	$	 
    	    		set_value = strcompress((*pimage)[xc,yc],/rem)	    	$	 
    	ELSE widget_control,(*pdiva_draw).tlb_info_valu_t,set_value =   	$	 
    	    	    		strcompress(FIX((*pimage)[xc,yc,0]),/rem) + 	$	 
	    			',' +   strcompress(FIX((*pimage)[xc,yc,1]),/rem) + 	$	 
	    			',' +   strcompress(FIX((*pimage)[xc,yc,2]),/rem)
	ENDIF ELSE BEGIN
    	IF (n_dim eq 1) THEN widget_control,(*pdiva_draw).tlb_info_valu_t,	$	 
    	    		set_value = strcompress((*pimage)[xc,yc],/rem)	    	$	 
    	ELSE widget_control,(*pdiva_draw).tlb_info_valu_t,set_value =   	$	 
    	    	    		strcompress((*pimage)[xc,yc,0],/rem) +	    	$	 
	    			',' +   strcompress((*pimage)[xc,yc,1],/rem) +	    	$	 
	    			',' +   strcompress((*pimage)[xc,yc,2],/rem)
	ENDELSE
    
	
	
	possibleEventTypes =[ 'DOWN', 'UP', 'MOTION', 'SCROLL' ]
    what_button_pressed=['NONE','LEFT','MIDDLE','NONE','RIGHT']
    thisEvent  = possibleEventTypes(event.type)
    thisbutton = what_button_pressed(event.press)
	
	;SCROLLING_SCROLLING_SCROLLING_SCROLLING_SCROLLING_SCROLLING_SCROLLING_S
    ;SCROLLING_SCROLLING_SCROLLING_SCROLLING_SCROLLING_SCROLLING_SCROLLING_S
    ;SCROLLING_SCROLLING_SCROLLING_SCROLLING_SCROLLING_SCROLLING_SCROLLING_S
    IF (thisevent eq 'SCROLL') THEN BEGIN
    	
		curr_mfile_name =  (*pdiva_draw).diva_current_image.mfile_name
		prev_mfile_name =  (*pdiva_draw).diva_current_prev_image.mfile_name
		
		prev_f			= 	(*pdiva_draw).diva_current_prev_image.file_info.f
		if (curr_mfile_name eq prev_mfile_name) AND (f eq prev_f) then BEGIN

			widget_control,diva_tlb_drawwid,get_draw_view = draw_view

			tlb_drawwid_xsize = (*pdiva_draw).tlb_drawwid_size[0]
			tlb_drawwid_ysize = (*pdiva_draw).tlb_drawwid_size[1]

			if (xsize lt tlb_drawwid_xsize) then begin
    	    		tlb_drawwid_xsize=xsize
	    		tlb_drawwid_ysize=tlb_drawwid_ysize+20
			end
			if (ysize lt tlb_drawwid_ysize) then begin
	    		tlb_drawwid_ysize=ysize
	    		tlb_drawwid_xsize=tlb_drawwid_xsize+20
			end

			x_ratio = float(prev_xsize)/xsize
			y_ratio = float(prev_ysize)/ysize

			;lower lhs coords - PREVIEW 
			llhs = [draw_view[0]*x_ratio,draw_view[1]*y_ratio]
			;upper rhs coords - PREVIEW 
			urhs = [(draw_view[0]+tlb_drawwid_xsize-1)*x_ratio, 			$
	    			(draw_view[1]+tlb_drawwid_ysize-1)*y_ratio ]
			;make sure the urhs values are 249 at max
			urhs[0] = urhs[0] < 248
			urhs[1] = urhs[1] < 248

			llhs = round(llhs)
			urhs = round(urhs)

			widget_control,(*pdiva_draw).tlb_prev_drawwid,get_value=prev_draw
			wset,prev_draw
			tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2];PLOT colour table
			Device,Copy=[0,0,250,250,0,0,(*pdiva_draw).tlb_prev_pixid]
			plots,	[llhs[0],llhs[0],urhs[0],urhs[0],llhs[0]],				$
	    			[llhs[1],urhs[1],urhs[1],llhs[1],llhs[1]],				$
				/device,color=4,thick=2
			tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2];IMAGE colour table
		ENDIF
    END ; of SCROLLING
    ;REGULAR_ROI_REGULAR_ROI_REGULAR_ROI_REGULAR_ROI_REGULAR_ROI_REGULAR_ROI
    ;REGULAR_ROI_REGULAR_ROI_REGULAR_ROI_REGULAR_ROI_REGULAR_ROI_REGULAR_ROI
    ;REGULAR_ROI_REGULAR_ROI_REGULAR_ROI_REGULAR_ROI_REGULAR_ROI_REGULAR_ROI
    IF (tlb_image_function eq 1)  then begin

		if (thisevent eq 'UP') then begin
	    	(*event_struct.pdiva_draw).motion_events_on = 0
	    	(*event_struct.pdiva_draw).xd=xc
	    	(*event_struct.pdiva_draw).yd=yc
			;IMAGE colour table
	    	tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2]
    	    ;RESET condition
			if (abs(xs-xc) gt 1) or (abs(ys-yc) gt 1) then begin

				if (Ptr_Valid(diva_roi_info) eq 1) THEN widget_control,     $
		    			(*(*pdiva_draw).diva_roi_info).diva_roibase,/destroy
		
				x = [xc,xs] 	&   x = x(sort(x))
				y = [yc,ys] 	&   y = y(sort(y))
		
				IF (n_dim eq 3) THEN 	roi_pimage  = 	    	    	    $
		    	    					(*pimage)(x[0]:x[1],y[0]:y[1],*)	$
				ELSE    roi_pimage  = 	(*pimage)[x[0]:x[1],y[0]:y[1]]
		
				(*pdiva_draw).diva_roi_info = diva_roi_init(n_dim,			$
    	    	    	    	image_ct,plot_ct,diva_draw_base,os_name,    $
								(*pdiva_draw).diva_autoscale,roi_pimage,    $
								file_stuff,xc,yc,xs,ys,diva_printers,		$
								diva_printers_list)

    	    	diva_roi_display,(*pdiva_draw).diva_roi_info

    	    endif ;of reset condition
    	endif ;of condition 'UP'


		if (thisevent eq 'DOWN') then begin
	    	(*event_struct.pdiva_draw).motion_events_on = 1
	    	(*event_struct.pdiva_draw).xs=xc
	    	(*event_struct.pdiva_draw).ys=yc
			tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2];PLOT colour table
		endif ;of condition 'DOWN'
	
		if (thisevent eq 'MOTION') then begin
	    	if (motion_events_on eq 1) then begin

				Device,Copy=[0,0,xsize,ysize,0,0,diva_tlb_pixindex]
				plots,[xs, xs, xc, xc, xs],[ys, yc, yc, ys, ys],			$
												/device,thick=1,color=rband_c
				plots,xs,ys,/device,psym=6,color=rband_c
				plots,xc,yc,/device,psym=6,symsize=2,color=rband_c
				IF (Ptr_Valid(diva_zoom_info) eq 1) THEN    	    	    $
	    	    	plots,(*diva_zoom_info).xz,(*diva_zoom_info).yz,	    $
	    									/device,thick=1,color=rband_c

			end ;of motion events on
			(*event_struct.pdiva_draw).xd=xc
			(*event_struct.pdiva_draw).yd=yc
		endif ;of condition 'MOTION'
			    	
    endif ;of condition image_function = 1 

    ;IRREGULAR_ROI_IRREGULAR_ROI_IRREGULAR_ROI_IRREGULAR_ROI_IRREGULAR_ROI
    ;IRREGULAR_ROI_IRREGULAR_ROI_IRREGULAR_ROI_IRREGULAR_ROI_IRREGULAR_ROI
    ;IRREGULAR_ROI_IRREGULAR_ROI_IRREGULAR_ROI_IRREGULAR_ROI_IRREGULAR_ROI
    if (tlb_image_function eq 2) 		then begin
		if (thisevent eq 'DOWN') 		then begin
	    	if (thisbutton eq 'LEFT') 	then begin

    	    	(*event_struct.pdiva_draw).diva_roi_irreg_counter = 	    $
		    	(*event_struct.pdiva_draw).diva_roi_irreg_counter + 1
				c = (*event_struct.pdiva_draw).diva_roi_irreg_counter
		
				(*event_struct.pdiva_draw).diva_roi_irreg_xpts[c-1]=xc
				(*event_struct.pdiva_draw).diva_roi_irreg_ypts[c-1]=yc
		
				Device,Copy=[0,0,xsize,ysize,0,0,diva_tlb_pixindex]
				tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2];PLOT colour table
				if (c gt 1) then begin
		    		prevx = (*event_struct.pdiva_draw).diva_roi_irreg_xpts[0:c-1]
		    		prevy = (*event_struct.pdiva_draw).diva_roi_irreg_ypts[0:c-1]
		    		 plots,[prevx,xc],[prevy,yc],/device,thick=1,			$
					 								color=rband_c,psym=-6
				endif else begin
		    		plots,[xc],[yc],/device,thick=1,psym=-6,color=rband_c
				endelse
    	    	IF (Ptr_Valid(diva_zoom_info) eq 1) THEN    	    	    $
	    	    	plots,(*diva_zoom_info).xz,(*diva_zoom_info).yz,	    $
	    									/device,thick=1,color=rband_c
				;IMAGE colour table
				tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2]
	    	end ;of condition 'thisbutton = LEFT'
	    
			if (thisbutton eq 'RIGHT') then begin
				c = (*event_struct.pdiva_draw).diva_roi_irreg_counter
				IF (c lt 3) then begin
		    		diva_error,(*event_struct.pdiva_draw).tlb,4
		    		return
				ENDIF
		
				(*event_struct.pdiva_draw).diva_roi_irreg_xpts[c] = 	    $
		    	    	(*event_struct.pdiva_draw).diva_roi_irreg_xpts[0]
				(*event_struct.pdiva_draw).diva_roi_irreg_ypts[c] = 	    $
		    	    	(*event_struct.pdiva_draw).diva_roi_irreg_ypts[0]
		
				;OK, store x,y positions in retainx & retainy......
				(*event_struct.pdiva_draw).diva_roi_irreg_retainx = 	    $
		    	    		(*event_struct.pdiva_draw).diva_roi_irreg_xpts
				(*event_struct.pdiva_draw).diva_roi_irreg_retainy = 	    $
		    	    		(*event_struct.pdiva_draw).diva_roi_irreg_ypts
    	    	;...get the coords of the points selected..... 
				prevx = (*event_struct.pdiva_draw).diva_roi_irreg_retainx[0:c]
				prevy = (*event_struct.pdiva_draw).diva_roi_irreg_retainy[0:c]
				;...then re-initialize xpts, ypts and the counter to zero
				(*event_struct.pdiva_draw).diva_roi_irreg_xpts = intarr(100)
				(*event_struct.pdiva_draw).diva_roi_irreg_ypts = intarr(100)
				(*event_struct.pdiva_draw).diva_roi_irreg_counter = 0
		
		
				tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2];PLOT colour table
				Device,Copy=[0,0,xsize,ysize,0,0,diva_tlb_pixindex]
				plots,[prevx],[prevy],/device,thick=1,psym=-6,color=rband_c
    	    	IF (Ptr_Valid(diva_zoom_info) eq 1) THEN    	    	    $
	    	    	plots,(*diva_zoom_info).xz,(*diva_zoom_info).yz,	    $
	    									/device,thick=1,color=rband_c
    	    	tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2];IMAGE colour table
		
				r=polyfillv(prevx,prevy,xsize,ysize)
				xr=r mod xsize
				yr=r/xsize
		
				minxr  = min(xr)	&  minyr  = min(yr)
				maxxr  = max(xr)	&  maxyr  = max(yr)
				nxsize = maxxr-minxr+1	&  nysize = maxyr-minyr+1
		
				nimg=uintarr(nxsize,nysize)
				nxr = xr - minxr
				nyr = yr - minyr
		
				nimg[nxr,nyr]=(*pimage)[xr,yr]
				roi_pimage = nimg
				
				;check if ROI GUI exists, if so then kill it
				if (Ptr_Valid(diva_roi_info) eq 1) THEN widget_control,     $
		    			(*(*pdiva_draw).diva_roi_info).diva_roibase,/destroy
				;and make it again
				(*pdiva_draw).diva_roi_info = diva_roi_init(n_dim,			$
    	    	    	    	image_ct,plot_ct,diva_draw_base,os_name,    $
								(*pdiva_draw).diva_autoscale,roi_pimage,    $
								file_stuff,maxxr,maxyr,minxr,minyr, 		$
								diva_printers,diva_printers_list)
    	    	
    	    	diva_roi_display,(*pdiva_draw).diva_roi_info

	    	end ;of condition 'thisbutton = RIGHT'
		end ;of condition 'thisevent = DOWN'
    end ;of condition 'tlb_image_function = 2'

    ;CIRCULAR_ROI__CIRCULAR_ROI__CIRCULAR_ROI__CIRCULAR_ROI__CIRCULAR_ROI
    ;CIRCULAR_ROI__CIRCULAR_ROI__CIRCULAR_ROI__CIRCULAR_ROI__CIRCULAR_ROI
    ;CIRCULAR_ROI__CIRCULAR_ROI__CIRCULAR_ROI__CIRCULAR_ROI__CIRCULAR_ROI
    if (tlb_image_function eq 3) then begin

		if (thisevent eq 'DOWN') then begin
	    	(*event_struct.pdiva_draw).motion_events_on = 1
	    	(*event_struct.pdiva_draw).xs=xc
	    	(*event_struct.pdiva_draw).ys=yc
		endif ;of condition 'DOWN'
		
		if (thisevent eq 'MOTION') then begin
	    	if (motion_events_on eq 1) then begin
	    		;PLOT colour table
				tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2]
				Device,Copy=[0,0,xsize,ysize,0,0,diva_tlb_pixindex]
				diva_circle,abs(xc - xs),xs,ys,x,y
				PlotS, x + xs, y+ ys, /DEVICE,color=rband_c
				arrow,xs,ys,xc,ys,/solid,hsize=10,color=rband_c
				plots,[xs],[ys],/device,color=rband_c,psym=-6
				IF (Ptr_Valid(diva_zoom_info) eq 1) THEN					$
	    	    		plots,(*diva_zoom_info).xz,(*diva_zoom_info).yz,	$
	    				/device,thick=1,color=rband_c
				tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2];IMAGE colour table
	    	endif ;of condition motion events are ON	

    	endif ;of condition 'MOTION'	    
		
		if (thisevent eq 'UP') then begin
	    	(*event_struct.pdiva_draw).motion_events_on = 0
	    	(*event_struct.pdiva_draw).xd=xc
	    	(*event_struct.pdiva_draw).yd=yc

	    	tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2];PLOT colour table
	    	Device,Copy=[0,0,xsize,ysize,0,0,diva_tlb_pixindex]
	    	diva_circle,abs(xc - xs),xs,ys,x,y
			PlotS, x + xs, y+ ys, /DEVICE,color=rband_c
	    	arrow,xs,ys,xc,ys,/solid,hsize=10,color=rband_c
	    	plots,[xs],[ys],/device,color=rband_c,psym=-6
			IF (Ptr_Valid(diva_zoom_info) eq 1) THEN			    		$
	    	    	plots,(*diva_zoom_info).xz,(*diva_zoom_info).yz,	    $
					/device,thick=1,color=rband_c
	    	tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2];IMAGE colour table

	    	x=x+xs 
	    	y=y+ys

	    	r=polyfillv(x,y,xsize,ysize)
	    	xr=r mod xsize
	    	yr=r/xsize

	    	minxr  = min(xr)  	    &  minyr  = min(yr)
	    	maxxr  = max(xr)  	    &  maxyr  = max(yr)
	    	nxsize = maxxr-minxr+1  &  nysize = maxyr-minyr+1

	    	nimg=uintarr(nxsize,nysize)
	    	nxr = xr - minxr
	    	nyr = yr - minyr

	    	nimg[nxr,nyr]=(*pimage)[xr,yr]
	    	roi_pimage = nimg

	    	;check if ROI GUI exists, if so then kill it
	    	if (Ptr_Valid(diva_roi_info) eq 1) THEN widget_control,     	$
		    	(*(*pdiva_draw).diva_roi_info).diva_roibase,/destroy
	    	;and make it again
	    	(*pdiva_draw).diva_roi_info = diva_roi_init(n_dim,          	$
    	    	    	    	image_ct,plot_ct,diva_draw_base,os_name,    $
			    				(*pdiva_draw).diva_autoscale,roi_pimage,    $
			    				file_stuff,maxxr,maxyr,minxr,minyr, 		$
								diva_printers,diva_printers_list)

			diva_roi_display,(*pdiva_draw).diva_roi_info

		endif ;of condition 'UP'    
	
    end ;of condition 'tlb_image_function = 3'
    
    ;ZOOM__ZOOM__ZOOM__ZOOM__ZOOM_ZOOM__ZOOM__ZOOM__ZOOM__ZOOM_ZOOM__ZOOM
    ;ZOOM__ZOOM__ZOOM__ZOOM__ZOOM_ZOOM__ZOOM__ZOOM__ZOOM__ZOOM_ZOOM__ZOOM
    ;ZOOM__ZOOM__ZOOM__ZOOM__ZOOM_ZOOM__ZOOM__ZOOM__ZOOM__ZOOM_ZOOM__ZOOM
    if (tlb_image_function eq 4) then begin
	
	IF (Ptr_Valid(diva_line_info) eq 1) THEN c = (*diva_line_info).coords

	if (thisevent eq 'DOWN') then begin
	    (*event_struct.pdiva_draw).motion_events_on = 1
	    
	    if (Ptr_Valid(diva_zoom_info) eq 0) then begin
	    	;if it doesn't already exist, then make it!!!!
		tlb_stuff=[xsize,ysize,diva_tlb_drawwid,diva_tlb_pixindex,  $
		    	    	    	    	    	diva_draw_base]
		(*pdiva_draw).diva_zoom_info = diva_zoom_init(n_dim,	    $
	    	    	    	image_ct,plot_ct,   	    	    	    $
							(*pdiva_draw).diva_autoscale,	    	    $
							pimage,tlb_stuff,file_stuff,diva_draw_base, $
							diva_printers,diva_printers_list)

		widget_control,(*pdiva_draw).diva_tlb_drawwid, get_value=tlb_draw
		wset,tlb_draw
	    endif
	    
	    diva_zoom_size = (*(*pdiva_draw).diva_zoom_info).diva_zoom_size
	    diva_zoom_mag  = (*(*pdiva_draw).diva_zoom_info).diva_zoom_mag
	    diva_zoom	   = diva_zoom_size/diva_zoom_mag
	    
	    xc = diva_zoom > xc < (xsize - diva_zoom - 1)
	    yc = diva_zoom > yc < (ysize - diva_zoom - 1)
	    xd = xc
	    yd = yc
	    (*event_struct.pdiva_draw).xd = xc
	    (*event_struct.pdiva_draw).yd = yc
	    xz = [xc-diva_zoom,xc+diva_zoom,xc+diva_zoom,   	    	    $
	    	    	    	    	    xc-diva_zoom,xc-diva_zoom]
	    yz = [yc+diva_zoom,yc+diva_zoom,yc-diva_zoom,   	    	    $
	    	    	    	    	    yc-diva_zoom,yc+diva_zoom]
	    
	    (*(*pdiva_draw).diva_zoom_info).xz = xz
	    (*(*pdiva_draw).diva_zoom_info).yz = yz
	    
    	    tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2];PLOT colour table
    	    Device,Copy=[0,0,xsize,ysize,0,0,diva_tlb_pixindex]
	    plots,xz,yz,/device,thick=1,color=rband_c
	    IF (Ptr_Valid(diva_line_info) eq 1) THEN	    	    	    $
	    	plots,[c[0],c[2]],[c[1],c[3]],/device,color=rband_c,psym=-6
    	    tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2];IMAGE colour table
	    
	    ;need to think about case where xd < size of new bounding box!!!!
	    if (xd+diva_zoom gt xsize-1)    then xd = (xsize-1)-diva_zoom
	    if (yd+diva_zoom gt ysize-1)    then yd = (ysize-1)-diva_zoom
	    if (xd lt diva_zoom)	    then xd = diva_zoom
	    if (yd lt diva_zoom)	    then yd = diva_zoom
	    
	    (*(*pdiva_draw).diva_zoom_info).xyc = [xd,yd]
	    zoom_pimage = (*pimage)[	xd-diva_zoom:xd+diva_zoom-1,	    $
	    				yd-diva_zoom:yd+diva_zoom-1,*	 ]
	    CASE n_dim OF
	    	1:  zoom_pimage =  rebin(zoom_pimage,320,320,  /sample)
	    	3:  zoom_pimage =  rebin(zoom_pimage,320,320,3,/sample)
	    ENDCASE
	    
	    Ptr_FRee,(*(*pdiva_draw).diva_zoom_info).zoom_pimage
	    (*(*pdiva_draw).diva_zoom_info).zoom_pimage = Ptr_New(zoom_pimage)
	    
	    ;call 'diva_zoom_display' to update zoom image
	    diva_zoom_display,	(*pdiva_draw).diva_zoom_info
    	
		;if surface plot is up, then update it
		if  (XRegistered("diva_surf") eq 1) then  BEGIN
			ptr_free,(*(*(*(*pdiva_draw).diva_zoom_info).					$
												diva_surf_info)).roi_pimage
			(*(*(*(*pdiva_draw).diva_zoom_info).diva_surf_info)).roi_pimage=$
													Ptr_New(zoom_pimage)
			diva_surf_zoom,(*(*(*pdiva_draw).diva_zoom_info).diva_surf_info)
		ENDIF

		    
	end ;of condition 'thisevent = DOWN'
	
	if (thisevent eq 'MOTION') then begin
	    if (motion_events_on eq 1) then begin

	    	diva_zoom_size = (*(*pdiva_draw).diva_zoom_info).diva_zoom_size
	    	diva_zoom_mag  = (*(*pdiva_draw).diva_zoom_info).diva_zoom_mag
	    	diva_zoom	   = diva_zoom_size/diva_zoom_mag
		xc = diva_zoom > xc < (xsize - diva_zoom - 1)
		yc = diva_zoom > yc < (ysize - diva_zoom - 1)
		(*event_struct.pdiva_draw).xd = xc
		(*event_struct.pdiva_draw).yd = yc
		xz = [xc-diva_zoom,xc+diva_zoom,xc+diva_zoom,	    	    $
		    	    	    	    	xc-diva_zoom,xc-diva_zoom]
		yz = [yc+diva_zoom,yc+diva_zoom,yc-diva_zoom,	    	    $
		    	    	    	    	yc-diva_zoom,yc+diva_zoom]
		(*(*pdiva_draw).diva_zoom_info).xz = xz
		(*(*pdiva_draw).diva_zoom_info).yz = yz

    	    	tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2];PLOT colour table
		Device,Copy=[0,0,xsize,ysize,0,0,diva_tlb_pixindex]
    	    	plots,xz,yz,/device,thick=1,color=rband_c
    	    	IF (Ptr_Valid(diva_line_info) eq 1) THEN	    	    $
    	    	    plots,[c[0],c[2]],[c[1],c[3]],/device,color=rband_c,    $
		    	    	    	    	    	    	psym=-6
		tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2];IMAGE colour table
		
		;need to think about case where xd < size of new bounding box!!!!
		if (xd+diva_zoom gt xsize-1) 	then xd = (xsize-1)-diva_zoom
		if (yd+diva_zoom gt ysize-1) 	then yd = (ysize-1)-diva_zoom
		if (xd lt diva_zoom) 	    	then xd = diva_zoom
		if (yd lt diva_zoom) 	    	then yd = diva_zoom
		
		(*(*pdiva_draw).diva_zoom_info).xyc = [xd,yd]
		zoom_pimage = (*pimage)[    xd-diva_zoom:xd+diva_zoom-1,    $
					    			yd-diva_zoom:yd+diva_zoom-1,*  ]
		CASE n_dim OF
		    1:	zoom_pimage =  rebin(zoom_pimage,320,320,  /sample)
		    3:	zoom_pimage =  rebin(zoom_pimage,320,320,3,/sample)
		ENDCASE
		
		Ptr_FRee,(*(*pdiva_draw).diva_zoom_info).zoom_pimage
		(*(*pdiva_draw).diva_zoom_info).zoom_pimage = Ptr_New(zoom_pimage)
		
		;call 'diva_zoom_display' to update zoom image
		diva_zoom_display,  (*pdiva_draw).diva_zoom_info
		
		;if surface plot is up, then update it
		if  (XRegistered("diva_surf") eq 1) then  BEGIN
			ptr_free,(*(*(*(*pdiva_draw).diva_zoom_info).					$
												diva_surf_info)).roi_pimage
			(*(*(*(*pdiva_draw).diva_zoom_info).diva_surf_info)).roi_pimage=$
													Ptr_New(zoom_pimage)
			diva_surf_zoom,(*(*(*pdiva_draw).diva_zoom_info).diva_surf_info)
		ENDIF
		
	    end ;of condition "motion_events_on eq 1"
	end ;of condition 'thisevent = MOTION'
	
	if (thisevent eq 'UP') then begin
	    (*event_struct.pdiva_draw).motion_events_on = 0
	end ;of condition 'thisevent = UP'
	
    end ;of condition 'tlb_image_function eq 4'
    
   ;LINE_LINE_LINE_LINE_LINE_LINE_LINE_LINE_LINE_LINE_LINE_LINE_LINE_LINE
   ;LINE_LINE_LINE_LINE_LINE_LINE_LINE_LINE_LINE_LINE_LINE_LINE_LINE_LINE
   ;LINE_LINE_LINE_LINE_LINE_LINE_LINE_LINE_LINE_LINE_LINE_LINE_LINE_LINE
   if (tlb_image_function eq 6) OR (tlb_image_function eq 9) then begin
    	
	if (thisevent eq 'DOWN') then begin
	    motion_events_on = 1
	    (*event_struct.pdiva_draw).xs=xc
	    (*event_struct.pdiva_draw).ys=yc
    	end ;of condition 'thisevent = DOWN'
	
	if (thisevent eq 'MOTION') then begin
	    if (motion_events_on eq 1) then begin
	    	delx=abs(xs-xc)  &   dely=abs(ys-yc)
	    
	    	if (tlb_image_function eq 6) then begin
		    if (delx gt dely) then yc=ys ELSE xc=xs
	    	endif 
	    
	    	Device,Copy=[0,0,xsize,ysize,0,0,diva_tlb_pixindex]
    	    	tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2];PLOT colour table
	    	plots,[xs,xc],[ys,yc],/device,thick=1,color=rband_c,psym=-6
    	    	IF (Ptr_Valid(diva_zoom_info) eq 1) THEN    	    	    $
		    	plots,(*diva_zoom_info).xz,(*diva_zoom_info).yz,    $
		    	    	    	    /device,thick=1,color=rband_c
		tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2];IMAGE colour table
		
		(*event_struct.pdiva_draw).xd=xc
		(*event_struct.pdiva_draw).yd=yc
	    end ;of condition 'motion_events = 1'
    	end ;of condition 'thisevent = MOTION'	
	
	if (thisevent eq 'UP') then begin
    	    motion_events_on = 0
	    ;xd = event.x & yd = event.y

	    delx=abs(xs-xc) & dely=abs(ys-yc)
	    
	    if (delx gt 2) OR (dely gt 2) then begin
	    
	    	if (tlb_image_function eq 6) then begin ;H/V condition
		    if (delx gt dely) then yc=ys ELSE xc=xs
	    	endif 
		
		x = [xs,xc]  	    &   y = [ys,yc]
		x = x(sort(x))	    &   y = y(sort(y))
	    	delx = x[1] - x[0]  &	dely = y[1] - y[0]
		
		(*event_struct.pdiva_draw).xs = x[0]
		(*event_struct.pdiva_draw).xd = x[1]
	    	(*event_struct.pdiva_draw).ys = y[0]
	    	(*event_struct.pdiva_draw).yd = y[1]
		
		if (tlb_image_function eq 6) then begin ;H/V condition
		    if (n_dim eq 1) then begin
		    	l_image = (*pimage)[x[0]:x[1],y[0]:y[1]]
		    endif else begin
			l_image =  (*pimage)[x(0):x[1],y[0]:y[1],*]
		    endelse
		    l_image =  reform(l_image)
		    if (dely eq 0) THEN l_axis = indgen(x[1]-x[0] +1)+x[0]
		    if (delx eq 0) THEN l_axis = indgen(y[1]-y[0] +1)+y[0]
		end
		if (tlb_image_function eq 9) then begin ;ANY condition
		    nPoints = ABS(x[1]-x[0]+1) > ABS(y[1]-y[0]+1)
		    xloc = x[0] + delx * findgen(nPoints)/(nPoints -1)
		    yloc = y[0] + dely * findgen(nPoints)/(nPoints -1)
		    if (n_dim eq 1) then begin
		    	l_image = interpolate(*pimage, xloc, yloc)
		    endif else begin
		    	l_image_r = interpolate((*pimage)[*,*,0],xloc,yloc)
			l_image_g = interpolate((*pimage)[*,*,1],xloc,yloc)
			l_image_b = interpolate((*pimage)[*,*,2],xloc,yloc)
			l_image = [[l_image_r],[l_image_g],[l_image_b]]
		    endelse
		    l_axis  = indgen(n_elements(l_image[*,0]))
		end
		
		;OK, now got arrays for x and y plot!!!!!!!!!!!!!!!!!!!!!!!!
    	    	if (delx ne dely) then begin
		    if (Ptr_Valid(diva_line_info) eq 0) then begin
		    	;if it doesn't already exist, then make it!!!!
			(*pdiva_draw).diva_line_info = diva_line_init(n_dim,$
			    	    	image_ct,plot_ct,diva_draw_base,    $
							os_name,file_stuff,diva_printers,	$
							diva_printers_list)
		    endif 
    	    	    ;However, if it does exist, we need to update stuff here
		    PTR_FREE,(*(*pdiva_draw).diva_line_info).x_arr
		    PTR_FREE,(*(*pdiva_draw).diva_line_info).y_arr
		    (*(*pdiva_draw).diva_line_info).x_arr = PTR_NEW(l_axis)
		    (*(*pdiva_draw).diva_line_info).y_arr = PTR_NEW(l_image)
		    
		    (*(*pdiva_draw).diva_line_info).xyc = [ min(l_axis),    $
		    	    	min(l_image),max(l_axis),max(l_image)	 ]

    	    	    (*(*pdiva_draw).diva_line_info).coords = [xs,ys,xd,yd]
		    
		    if (delx eq 0) then begin
		    	(*(*pdiva_draw).diva_line_info).const = xs
		    endif else begin
		    	(*(*pdiva_draw).diva_line_info).const = ys
		    endelse
		    
		    ;call 'diva_line_display' pro to plot line profile
		    diva_line_display,(*pdiva_draw).diva_line_info
		
		endif ;of condition a DIAGONAL
	    end ;of condition delx OR dely  > 2
	end ;of condition 'thisevent = UP'	
	
	(*event_struct.pdiva_draw).motion_events_on=motion_events_on
    end ;of condition 'tlb_image_function eq 6 and 9'

   ;ALL_LINE_ALL_LINE_ALL_LINE_ALL_LINE_ALL_LINE_ALL_LINE_ALL_LINE_ALL_LINE
   ;ALL_LINE_ALL_LINE_ALL_LINE_ALL_LINE_ALL_LINE_ALL_LINE_ALL_LINE_ALL_LINE
   ;ALL_LINE_ALL_LINE_ALL_LINE_ALL_LINE_ALL_LINE_ALL_LINE_ALL_LINE_ALL_LINE
    if (tlb_image_function eq 7) OR (tlb_image_function eq 8) then begin
		
    	if (thisbutton eq 'RIGHT') then begin
	    	CASE tlb_image_function OF
	    		7:  BEGIN
		    		tlb_image_function = 8
					widget_control,(*pdiva_draw).tlb_info_func_text,		$
			    	    	    		set_value = 'All Row Profile'
		    	END
	    		8:  BEGIN
		    		tlb_image_function = 7
					widget_control,(*pdiva_draw).tlb_info_func_text,		$
			    	    	    		set_value = 'All Col Profile'
		    	END
	    	ENDCASE
	    	(*pdiva_draw).tlb_image_function = tlb_image_function
		end ;of condition 'thisevent = RIGHT'
		
		if (thisevent eq 'DOWN') then begin
	    	(*event_struct.pdiva_draw).xs=xc
	    	(*event_struct.pdiva_draw).ys=yc
	    	Device,Copy=[0,0,xsize,ysize,0,0,diva_tlb_pixindex]
	    	tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2];PLOT colour table
	    	CASE tlb_image_function OF
	    		7:  BEGIN
		    			plots,[xc,xc],[0,ysize],/device,thick=1,    	    $
			    	    	    	    			color=rband_c,psym=-6
						IF (n_dim eq 1) THEN l_image =      	    	    $
									(*pimage)[xc:xc,0:ysize-1]      	    $
						ELSE l_image = (*pimage)[xc:xc,0:ysize-1,*]
						l_axis  = indgen(ysize)
						coords  = [xc,0,xc,ysize-1]
						const   = xc
		    		END
	    		8:  BEGIN
		    			plots,[0,xsize],[yc,yc],/device,thick=1,    	    $
			    	    	    	    			color=rband_c,psym=-6
		    			IF (n_dim eq 1) then l_image =      	    	    $
			    	    			(*pimage)[0:xsize-1,yc:yc]      	    $
						ELSE l_image = (*pimage)[0:xsize-1,yc:yc,*]
						l_axis  = indgen(xsize)
						coords  = [0,yc,xsize-1,yc]
						const   = yc
		    	END
	    	ENDCASE
			IF (Ptr_Valid(diva_zoom_info) eq 1) THEN			    		$
				plots,(*diva_zoom_info).xz,(*diva_zoom_info).yz,	    	$
	    							/device,thick=1,color=rband_c
	    	;IMAGE colour table
			tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2]

	    	l_image 	=   reform(l_image)

	    	if (Ptr_Valid(diva_line_info) eq 0) then begin
				;if it doesn't already exist, then make it!!!!
				(*pdiva_draw).diva_line_info = diva_line_init(n_dim,		$
			    				image_ct,plot_ct,diva_draw_base,    	    $
								os_name,file_stuff,diva_printers,			$
								diva_printers_list)
	    	endif 
			;However, if it does exist, we need to update stuff here
	    	Ptr_FRee,(*(*pdiva_draw).diva_line_info).x_arr
	    	Ptr_FRee,(*(*pdiva_draw).diva_line_info).y_arr
	    	(*(*pdiva_draw).diva_line_info).x_arr = ptr_new(l_axis)
	    	(*(*pdiva_draw).diva_line_info).y_arr = ptr_new(l_image)

	    	(*(*pdiva_draw).diva_line_info).xyc = [ min(l_axis),			$
		    	    								min(l_image),			$
													max(l_axis),			$
													max(l_image) ]
	    	(*(*pdiva_draw).diva_line_info).const  = const
	    	(*(*pdiva_draw).diva_line_info).coords = coords

	    	;call 'diva_line_display' pro to plot line profile
	    	diva_line_display,(*pdiva_draw).diva_line_info

		end ;of condition 'thisevent = DOWN'
    endif;of condition 'tlb_image_function eq 7 OR 8'

    
END ;of 'DRAW_EVENT' event value
ELSE:
EndCase
END;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
