PRO diva_ptc_doit,diva_ptc_info

widget_control,/hourglass

dc_coords   	    	=   (*diva_ptc_info).dc_coords_real
sig_coords   	    	=   (*diva_ptc_info).roi_coords_real
image_1     	    	=   (*(*diva_ptc_info).ptc_image_1)
image_2     	    	=   (*(*diva_ptc_info).ptc_image_2)
ptc_table_ids	    	=   (*diva_ptc_info).ptc_table_ids
image_ct    	    	=   (*diva_ptc_info).image_ct
plot_ct     	    	=   (*diva_ptc_info).plot_ct
diva_printers			=	(*diva_ptc_info).diva_printers   
diva_printers_list		=	(*diva_ptc_info).diva_printers_list

;DC_DC_DC_DC_DC_DC_DC_DC_DC_DC_DC_DC_DC_DC_DC_DC_DC_DC_DC_DC_DC_DC_DC_DC_DC_

dc_x = [dc_coords[0],dc_coords[2]]
dc_y = [dc_coords[1],dc_coords[3]]

dc_x = dc_x[sort(dc_x)]
dc_y = dc_y[sort(dc_y)]

dc_image_1 = image_1[dc_x[0]:dc_x[1],dc_y[0]:dc_y[1]]
dc_image_2 = image_2[dc_x[0]:dc_x[1],dc_y[0]:dc_y[1]]

dc_moment_1   	=   moment(dc_image_1,/double)
dc_mean_1   	=   dc_moment_1[0]
dc_var_1 		=   dc_moment_1[1]
dc_stddev_1   	=   sqrt(dc_moment_1[1])

dc_moment_2   	=   moment(dc_image_2,/double)
dc_mean_2   	=   dc_moment_2[0]
dc_var_2 		=   dc_moment_2[1]
dc_stddev_2   	=   sqrt(dc_moment_2[1])

widget_control,ptc_table_ids[0],set_value= 	    	    	    	    	$
    	[ [ strcompress(dc_mean_1,/remove),strcompress(dc_mean_2,/remove)], $
	  	  [ strcompress(dc_var_1,/rem)    ,strcompress(dc_var_2,/rem)],	    $
	  	  [ strcompress(dc_stddev_1,/rem) ,strcompress(dc_stddev_2,/rem)]  ]

;SIGNAL_SIGNAL_SIGNAL_SIGNAL_SIGNAL_SIGNAL_SIGNAL_SIGNAL_SIGNAL_SIGNAL_SIGNA

sig_x = [sig_coords[0],sig_coords[2]]
sig_y = [sig_coords[1],sig_coords[3]]

sig_x = sig_x[sort(sig_x)]
sig_y = sig_y[sort(sig_y)]

sig_image_1 = image_1[sig_x[0]:sig_x[1],sig_y[0]:sig_y[1]]
sig_image_2 = image_2[sig_x[0]:sig_x[1],sig_y[0]:sig_y[1]]

sig_moment_1   	=   moment(sig_image_1,/double)
sig_mean_1   	=   sig_moment_1[0]
sig_variance_1 	=   sig_moment_1[1]
sig_stddev_1   	=   sqrt(sig_moment_1[1])

sig_moment_2   	=   moment(sig_image_2,/double)
sig_mean_2   	=   sig_moment_2[0]
sig_variance_2 	=   sig_moment_2[1]
sig_stddev_2   	=   sqrt(sig_moment_2[1])

widget_control,ptc_table_ids[1],set_value = 	    	    	    	    $
    	[  [	strcompress(sig_mean_1,/remove), 	    	    	    $
	    	strcompress(sig_mean_2,/remove)],	    	    	    $
	    [	strcompress(sig_variance_1,/remove), 	    	    	    $
	    	strcompress(sig_variance_2,/remove)],   	    	    $
	    [	strcompress(sig_stddev_1,/remove), 	    	    	    $
	    	strcompress(sig_stddev_2,/remove)]  	]

;CALCULATE_RESULTS_CALCULATE_RESULTS_CALCULATE_RESULTS_CALCULATE_RESULTS_CALC

;calculate mean values of SIGNAL image_1 ROI
num_rows_1  	= n_elements(sig_image_1[0,*])
temp 	    	= fltarr(num_rows_1)
row_variance_3  = fltarr(num_rows_1)
;calculate E_BAR  and reverse
for i=0,num_rows_1 -1 do temp[i] = mean(sig_image_1[*,i],/double)
E_BAR = reverse(temp - dc_mean_1)

;subtract image_2 from image_1, and add 100 ADUs
sig_image_3 = long(abs(sig_image_1 - sig_image_2) + 100)

;calculate variance  and reverse
;for i=0,num_rows_1 -1 do row_variance_3[i] = variance(double(sig_image_3[*,i]))
;stop

;plot,sig_image_3[*,100]



for i=0,num_rows_1 -1 do row_variance_3[i] = 	    	    	    	    $
    	    variance(double(sig_image_3[where(sig_image_3[*,i] lt 1.e3),i]))
row_variance_3 = reverse(row_variance_3)

;OK, now perform a linear line-fit for this data (e_bar vs variance)
result = LINFIT(e_bar,row_variance_3/2.)
yfit=result[0]+result[1]*e_bar

;PLOT_RESULTS_PLOT_RESULTS_PLOT_RESULTS_PLOT_RESULTS_PLOT_RESULTS_PLOT_RESULT
window,4,xs=600,ys=500
!p.multi=[0,1,2]
;plot,e_bar,row_variance_3/2.,psym=3,xtit='E_bar  /ADUs',    	    	    $
;    	ytit='2_variance/2  /ADUs',charsize=2,xstyle=1,     	    	    $
;	xr=[0,max(e_bar)+.1*max(e_bar)],title='CCD Transfer Curve'
;oplot,e_bar,yfit
;xyouts,.25,.94,/norm,'1/m',charsize=1.5
;xyouts,.32,.94,/norm,'=',charsize=1.5
;xyouts,.36,.94,/norm,strcompress(1/(result[1]),/remove) +   	    	    $
;    	    	    	    	    	    '   !Ne!U-!N/ADU',charsize=1.5

plot,e_bar,ystyle=1,yr=[min(e_bar)-.1*min(e_bar),max(e_bar)+.1*max(e_bar)], $
    	charsize=1.2,tit='E-bar:  Mean Pixel Value  /ADUs',xstyle=1
plot,row_variance_3/2.,charsize=1.2,xstyle=1, 	    	    	    	    $
    	tit='2_variance/2:  Variance in [(image2-image1)/2]  /ADUs'

!p.multi=0

if (XRegistered("diva_ptc_plot") ne 0) then begin
    widget_control,(*diva_ptc_info).diva_ptc_plotbase,get_uvalue=info
    Ptr_Free,info
    widget_control,(*diva_ptc_info).diva_ptc_plotbase,/destroy
endif
diva_ptc_plot,e_bar,row_variance_3,yfit,result,image_ct,plot_ct,    	    $
    	    	(*diva_ptc_info).filestuff,diva_ptc_plotbase,	    	    $
				diva_printers,diva_printers_list,							$
		group = (*diva_ptc_info).diva_ptcbase

(*diva_ptc_info).diva_ptc_plotbase = diva_ptc_plotbase

END
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_ptc_ev, event
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
WIDGET_CONTROL, event.id, GET_UVALUE = eventval
widget_control,event.top,get_uvalue=diva_ptc_info	

image_ct    	    	=   (*diva_ptc_info).image_ct
plot_ct     	    	=   (*diva_ptc_info).plot_ct
diva_ptc_pix	    	=   (*diva_ptc_info).diva_ptc_pix
ptc_motion_events_on	=   (*diva_ptc_info).ptc_motion_events_on
dc_coords   	    	=   (*diva_ptc_info).dc_coords
roi_coords   	    	=   (*diva_ptc_info).roi_coords
header	    	    	=   (*diva_ptc_info).header
get_dc_roi  	    	=   (*diva_ptc_info).get_dc_roi
diva_ptc_drawsize   	=   (*diva_ptc_info).diva_ptc_drawsize
ptcdc_in    	    	=   (*diva_ptc_info).ptcdc_in
ptcsig_in   	    	=   (*diva_ptc_info).ptcsig_in
x_conv	    	    	=   float(header[0])/diva_ptc_drawsize
y_conv	    	    	=   float(header[1])/diva_ptc_drawsize
diva_ptc_cursor     	=   (*diva_ptc_info).diva_ptc_cursor

widget_control,(*diva_ptc_info).diva_ptc_draw,get_value=ptc_draw
wset,ptc_draw

CASE eventval OF

"EXIT":     	BEGIN
    	    	    WIDGET_CONTROL, event.top, /DESTROY
		    return		
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"PTC_DRAW_EVENT":   $	;PTC_DRAW_EVENTS_PTC_DRAW_EVENTS_PTC_DRAW_EVENTS_PTC_DR
BEGIN
    event.x = 0 > event.x < (diva_ptc_drawsize - 1)
    event.y = 0 > event.y < (diva_ptc_drawsize - 1)
    xc = event.x & yc = event.y
    
    x_real = strcompress(round((xc+1)*x_conv)-1,/rem)
    y_real = strcompress(round((yc+1)*y_conv)-1,/rem)
    widget_control,diva_ptc_cursor[0],set_value= x_real + ',' + y_real
    
    val = strcompress((*(*diva_ptc_info).ptc_image_prev)[xc,yc],/rem)
    widget_control,diva_ptc_cursor[1],set_value= val
    
    possibleEventTypes =[ 'DOWN', 'UP', 'MOTION', 'SCROLL' ]
    thisEvent  = possibleEventTypes(event.type)
    
    if (thisevent eq 'DOWN') then begin
    	(*diva_ptc_info).ptc_motion_events_on = 1
	tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2]
	if (get_dc_roi eq 0) then begin ;ie - signal
	    (*diva_ptc_info).roi_coords[0:1]=[xc,yc]
	endif else begin
	    (*diva_ptc_info).dc_coords[0:1]=[xc,yc]
    	endelse	
    endif ;of condition 'DOWN'

    if (thisevent eq 'MOTION') then begin
    	if (ptc_motion_events_on eq 1) then begin
	    Device,Copy=[0,0,diva_ptc_drawsize,diva_ptc_drawsize,0,0,diva_ptc_pix]
	    
	if (get_dc_roi eq 0) then begin ;ie - signal
	    (*diva_ptc_info).roi_coords[2:3]=[xc,yc]
	endif else begin
	    (*diva_ptc_info).dc_coords[2:3]=[xc,yc]
    	endelse	
	    
	    if (get_dc_roi eq 0) then begin ;ie - signal
		plots,[roi_coords[0],roi_coords[0],xc,xc,roi_coords[0]],    $
		      [roi_coords[1],yc,yc,roi_coords[1],roi_coords[1]],    $
		      /device,color=2
		plots,[dc_coords[0],dc_coords[0],dc_coords[2],dc_coords[2], $
		    	dc_coords[0] ],[dc_coords[1] ,dc_coords[3], 	    $
			dc_coords[3],dc_coords[1],dc_coords[1]],/device,color=7
	    endif else begin
		plots,[dc_coords[0] ,dc_coords[0] ,xc,xc,dc_coords[0] ],    $
		      [dc_coords[1] ,yc,yc,dc_coords[1], dc_coords[1] ],    $
		      /device,color=7
		plots,[roi_coords[0],roi_coords[0],roi_coords[2],   	    $
		    	roi_coords[2],roi_coords[0]],[roi_coords[1],	    $
			roi_coords[3],roi_coords[3],roi_coords[1],  	    $
			roi_coords[1]],/device,color=2
    	    endelse	
	end ;of motion events on
    endif ;of condition 'DOWN'
    
    if (thisevent eq 'UP') then begin
	if (get_dc_roi eq 0) then begin ;ie - signal
	    (*diva_ptc_info).roi_coords[2:3]=[xc,yc]
	    
	    roi_coords_real = (*diva_ptc_info).roi_coords

	    x = [roi_coords_real[0],roi_coords_real[2]]   &   x = x[sort(x)]
	    y = [roi_coords_real[1],roi_coords_real[3]]   &   y = y[sort(y)]
    	    
	    x = round((x+1)*x_conv)-1
	    y = round((y+1)*y_conv)-1
	    
	    roi_coords_real = [x[0],y[0],x[1],y[1]]
	    (*diva_ptc_info).roi_coords_real = roi_coords_real
	    for i=0,3 do widget_control,ptcsig_in[i],	    	    	    $
    	    	    	    set_value=strcompress(roi_coords_real[i],/rem)
	endif else begin
	    (*diva_ptc_info).dc_coords[2:3]=[xc,yc]
	    
	    dc_coords_real = (*diva_ptc_info).dc_coords

	    x = [dc_coords_real[0],dc_coords_real[2]]   &   x = x[sort(x)]
	    y = [dc_coords_real[1],dc_coords_real[3]]   &   y = y[sort(y)]
    	    
	    x = round((x+1)*x_conv)-1
	    y = round((y+1)*y_conv)-1
    	    
	    dc_coords_real = [x[0],y[0],x[1],y[1]]
	    (*diva_ptc_info).dc_coords_real = dc_coords_real
	    for i=0,3 do widget_control,ptcdc_in[i],	    	    	    $
    	    	    	    set_value=strcompress(dc_coords_real[i],/rem)
	    
    	endelse	
    	(*diva_ptc_info).ptc_motion_events_on = 0
	tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2]
    endif ;of condition 'DOWN'
    
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"SELECT_SIG":	$   ;
BEGIN
    if (event.select eq 1) then begin
    	(*diva_ptc_info).get_dc_roi = 0
    endif

END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"SELECT_DC":	$   ;
BEGIN
    if (event.select eq 1) then begin
    	(*diva_ptc_info).get_dc_roi = 1
    endif

END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"ROIDC_UX": 	$   ;
BEGIN
    widget_control,ptcdc_in[0],get_value=s_ux
    ux = fix(s_ux[0])
    
    (*diva_ptc_info).dc_coords_real[0]  = ux
    (*diva_ptc_info).dc_coords[0]   	= abs(round((ux-1)/x_conv)-1)
    
    ;call 'diva_ptc_display' procedure to display the image 
    diva_ptc_display,diva_ptc_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"ROIDC_UY": 	$   ;
BEGIN
    widget_control,ptcdc_in[1],get_value=s_uy
    uy = fix(s_uy[0])
    
    (*diva_ptc_info).dc_coords_real[1]  = uy
    (*diva_ptc_info).dc_coords[1]   	= abs(round((uy-1)/y_conv)-1)
    
    ;call 'diva_ptc_display' procedure to display the image 
    diva_ptc_display,diva_ptc_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"ROIDC_LX": 	$   ;
BEGIN
    widget_control,ptcdc_in[2],get_value=s_lx
    lx = fix(s_lx[0])
    
    (*diva_ptc_info).dc_coords_real[2]  = lx
    (*diva_ptc_info).dc_coords[2]   	= abs(round((lx-1)/x_conv)-1)
    
    ;call 'diva_ptc_display' procedure to display the image 
    diva_ptc_display,diva_ptc_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"ROIDC_LY": 	$   ;
BEGIN
    widget_control,ptcdc_in[3],get_value=s_ly
    ly = fix(s_ly[0])
    
    (*diva_ptc_info).dc_coords_real[3]  = ly
    (*diva_ptc_info).dc_coords[3]   	= abs(round((ly-1)/y_conv)-1)
    
    ;call 'diva_ptc_display' procedure to display the image 
    diva_ptc_display,diva_ptc_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"ROI_UX": 	$   ;
BEGIN
    widget_control,ptcsig_in[0],get_value=s_ux
    ux = fix(s_ux[0])
    
    (*diva_ptc_info).roi_coords_real[0]  = ux
    (*diva_ptc_info).roi_coords[0]   	 = abs(round((ux-1)/x_conv)-1)
    
    ;call 'diva_ptc_display' procedure to display the image 
    diva_ptc_display,diva_ptc_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"ROI_UY": 	$   ;
BEGIN
    widget_control,ptcsig_in[1],get_value=s_uy
    uy = fix(s_uy[0])
    
    (*diva_ptc_info).roi_coords_real[1]  = uy
    (*diva_ptc_info).roi_coords[1]   	 = abs(round((uy-1)/y_conv)-1)
    
    ;call 'diva_ptc_display' procedure to display the image 
    diva_ptc_display,diva_ptc_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"ROI_LX": 	$   ;
BEGIN
    widget_control,ptcsig_in[2],get_value=s_lx
    lx = fix(s_lx[0])
    
    (*diva_ptc_info).roi_coords_real[2]  = lx
    (*diva_ptc_info).roi_coords[2]   	 = abs(round((lx-1)/x_conv)-1)
    
    ;call 'diva_ptc_display' procedure to display the image 
    diva_ptc_display,diva_ptc_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"ROI_LY": 	$   ;
BEGIN
    widget_control,ptcsig_in[3],get_value=s_ly
    ly = fix(s_ly[0])
    
    (*diva_ptc_info).roi_coords_real[3]  = ly
    (*diva_ptc_info).roi_coords[3]   	 = abs(round((ly-1)/y_conv)-1)
    
    ;call 'diva_ptc_display' procedure to display the image 
    diva_ptc_display,diva_ptc_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"PTC_DEF":  	$   ;
BEGIN
    (*diva_ptc_info).dc_coords_real  =   [1,1400,45,1000]
    (*diva_ptc_info).roi_coords_real =   [600,2148,1600,100]
    
    dc_x = [(*diva_ptc_info).dc_coords_real[0],(*diva_ptc_info).dc_coords_real[2]]
    dc_y = [(*diva_ptc_info).dc_coords_real[1],(*diva_ptc_info).dc_coords_real[3]]
    dc_x = round((dc_x-1)/x_conv)
    dc_y = round((dc_y-1)/y_conv)
    
    (*diva_ptc_info).dc_coords = [dc_x[0],dc_y[0],dc_x[1],dc_y[1]]
    
    roi_x = [(*diva_ptc_info).roi_coords_real[0],(*diva_ptc_info).roi_coords_real[2]]
    roi_y = [(*diva_ptc_info).roi_coords_real[1],(*diva_ptc_info).roi_coords_real[3]]
    roi_x = round((roi_x-1)/x_conv)
    roi_y = round((roi_y-1)/y_conv)
    
    (*diva_ptc_info).roi_coords = [roi_x[0],roi_y[0],roi_x[1],roi_y[1]]
    

    for i=0,3 do widget_control,ptcsig_in[i],set_value =    	    	    $
    	    	    strcompress((*diva_ptc_info).roi_coords_real[i],/rem)

    for i=0,3 do widget_control,ptcdc_in[i],set_value =     	    	    $
    	    	    strcompress((*diva_ptc_info).dc_coords_real[i],/rem)

    ;call 'diva_ptc_display' procedure to display the image 
    diva_ptc_display,diva_ptc_info

    
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"PTC_DOIT": 	$   ;
BEGIN
    diva_ptc_doit,diva_ptc_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

ELSE: MESSAGE, "Event User Value Not Found"		
ENDCASE

END ;end of "diva_ptc" event handler-------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_ptc_info__define
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
junk	=   {	diva_ptc_info,     	    	    	    	    	    	$
    	    	ptc_image_1 	    	    :	Ptr_New(),  	    	    $
				ptc_image_2 	    	    :	Ptr_New(),  	    	    $
				ptc_image_prev	    	    :	Ptr_New(),  	    	    $
				header	    	    	    :	[0L,0,0,0], 	    	    $
    	    	diva_tlb_drawwid    	    :	0L,  	    	    	    $
				diva_ptcbase	    	    :	0L,  	    	    	    $
				diva_ptc_draw	    	    :	0L,  	    	    	    $
				diva_ptc_pix	    	    :	0L, 	    	    	    $
				ptc_motion_events_on   	    :	0,   	    	    	    $
				image_ct		    		:	bytarr(256,3),     	    	$
				plot_ct 		    		:	bytarr(256,3),     	    	$
				dc_coords   	    	    :	[0L,0,0,0], 	    	    $
				roi_coords  	    	    :	[0L,0,0,0],  	    	    $
				dc_coords_real 	    	    :	[0L,0,0,0], 	    	    $
				roi_coords_real	    	    :	[0L,0,0,0],  	    	    $
				get_dc_roi  	    	    :	0,   	    	    	    $
				diva_ptc_drawsize   	    :	0,   	    	    	    $
				ptcdc_in    	    	    :	[0L,0,0,0], 	    	    $
				ptcsig_in    	    	    :	[0L,0,0,0], 	    	    $
				diva_ptc_cursor     	    :	[0L,0],	    	    	    $
				ptc_table_ids	    	    :	[0L,0],	    	    	    $
				diva_ptc_plotbase   	    :	0L,  	    	    	    $
				filestuff  	    	    	:	strarr(5),   	    	    $
				diva_printers				:	strarr(3),					$
				diva_printers_list			:	strarr(3)					$
	    	}
END
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_ptc, image_1,image_2,header,diva_tlb_drawwid,image_ct,plot_ct,     $
    	    filestuff,diva_printers,diva_printers_list
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
diva_ptc_info = Ptr_New({diva_ptc_info})

diva_ptc_drawsize   	    	    = 500
(*diva_ptc_info).ptc_image_1   	    = Ptr_New(image_1)
(*diva_ptc_info).ptc_image_2   	    = Ptr_New(image_2)
(*diva_ptc_info).ptc_image_prev     = Ptr_New(congrid(image_1,	    	    $
    	    	    	    	    	diva_ptc_drawsize,diva_ptc_drawsize))
(*diva_ptc_info).header	    	    = header
(*diva_ptc_info).diva_tlb_drawwid   = diva_tlb_drawwid
(*diva_ptc_info).image_ct   	    = image_ct
(*diva_ptc_info).plot_ct    	    = plot_ct
(*diva_ptc_info).diva_ptc_drawsize  = diva_ptc_drawsize
(*diva_ptc_info).get_dc_roi 	    = 1
(*diva_ptc_info).filestuff  	    = filestuff
(*diva_ptc_info).diva_printers   	= diva_printers   
(*diva_ptc_info).diva_printers_list = diva_printers_list
;-------------------create the main base---------------------------------------;
diva_ptcbase = WIDGET_BASE(TITLE = "DIVA 2_IMAGE PTC:  "+filestuff[0],	    $
    	    	/col,/FLOATING,group_leader=diva_tlb_drawwid)
(*diva_ptc_info).diva_ptcbase = diva_ptcbase

diva_ptc_topbase = widget_base(diva_ptcbase,/row,/frame)
;-------------------draw area base---------------------------------------------;
diva_ptc_draw_base=widget_base(diva_ptc_topbase,/frame,/col)
diva_ptc_draw = widget_draw(diva_ptc_draw_base,xsize=diva_ptc_drawsize,     $
    	    	ysize=diva_ptc_drawsize,Button_Events=1, /motion_events,    $
		/viewport_events,uvalue="PTC_DRAW_EVENT")
(*diva_ptc_info).diva_ptc_draw = diva_ptc_draw

window,/free,xsize=diva_ptc_drawsize,ysize=diva_ptc_drawsize,/pixmap
(*diva_ptc_info).diva_ptc_pix = !d.window

diva_ptc_buttbase=widget_base(diva_ptcbase,/row,/frame)

;--------------------cursor base-----------------------------------------------;
diva_ptc_cursor_base=widget_base(diva_ptc_draw_base,/row)
diva_ptc_cursor_pos=widget_text(diva_ptc_cursor_base,value='0,0',xsize=12)
diva_ptc_cursor_val=widget_text(diva_ptc_cursor_base,value='0'  ,xsize=6)
(*diva_ptc_info).diva_ptc_cursor = [diva_ptc_cursor_pos,diva_ptc_cursor_val]




;-------------------info base--------------------------------------------------;
diva_ptc_info_base=widget_base(diva_ptc_topbase,/col)

;--------------------select DC/sig base----------------------------------------;
diva_ptc_select_base=widget_base(diva_ptc_info_base,/row,/frame)
diva_ptc_select_label=widget_label(diva_ptc_select_base,value='Select:')

diva_ptc_select_base_exc=widget_base(diva_ptc_select_base,/row,/exclusive)

diva_ptc_select_dc=widget_button(diva_ptc_select_base_exc,value ='DC',     $
    	uvalue='SELECT_DC')
widget_control,diva_ptc_select_dc,set_button=1
diva_ptc_select_sig=widget_button(diva_ptc_select_base_exc,value='Signal',  $
    	uvalue='SELECT_SIG')

diva_ptc_select_def=widget_button(diva_ptc_select_base,value='Default',     $
    	uvalue='PTC_DEF')	

;-------------------DC_DC_DC_DC_DC_DC_DC_--------------------------------------;
ptcdc_input_base=widget_base(diva_ptc_info_base,/col,frame=3)
ptcdc_input_label=widget_label(ptcdc_input_base,value =     	    	    $
    	'Input DC ROI coords (yellow) :')

ptcdc_in_upper_base=widget_base(ptcdc_input_base,/row)
ptcdc_in_u_label=widget_label(ptcdc_in_upper_base,value='upper (x,y):')
ptcdc_in_ux_value=widget_text(ptcdc_in_upper_base,value='0', 	    	    $
    	uvalue='ROIDC_UX',xsize=5,/editable)
ptcdc_in_uy_value=widget_text(ptcdc_in_upper_base,value='0', 	    	    $
    	uvalue='ROIDC_UY',xsize=5,/editable)

ptcdc_in_lower_base=widget_base(ptcdc_input_base,/row)
ptcdc_in_l_label=widget_label(ptcdc_in_lower_base,value='lower (x,y):')
ptcdc_in_lx_value=widget_text(ptcdc_in_lower_base,value='0', 	    	    $
    	uvalue='ROIDC_LX',xsize=5,/editable)
ptcdc_in_ly_value=widget_text(ptcdc_in_lower_base,value='0',  	    	    $
    	uvalue='ROIDC_LY',xsize=5,/editable)

(*diva_ptc_info).ptcdc_in = [ptcdc_in_ux_value,ptcdc_in_uy_value,   	    $
    	    	    	     ptcdc_in_lx_value,ptcdc_in_ly_value]
			     
ptcdc_table_id=widget_table(ptcdc_input_base,xsize=2,ysize=3,         	    $
    	row_labels=['Mean','Variance','StddeV'],    	    	    	    $
	value=[['0','0'],['0','0'],['0','0']],      	    	    	    $
	alignment=1,column_labels=['IMAGE 1','IMAGE 2'])
;-------------------SIGNAL_SIGNAL_SIGNAL---------------------------------------;
ptcsig_input_base=widget_base(diva_ptc_info_base,/col,frame=3)

ptcsig_input_label=widget_label(ptcsig_input_base,value =   	    	    $
    	'Input ROI coords (green) :')

ptcsig_upper_base=widget_base(ptcsig_input_base,/row)
ptcsig_u_label=widget_label(ptcsig_upper_base,value='upper (x,y):')

ptcsig_ux_value=widget_text(ptcsig_upper_base,uvalue='ROI_UX',xsize=5,	    $
    	/editable,value='0')
ptcsig_uy_value=widget_text(ptcsig_upper_base,uvalue='ROI_UY',xsize=5,	    $
    	/editable,value='0')

ptcsig_lower_base=widget_base(ptcsig_input_base,/row)
ptcsig_l_label=widget_label(ptcsig_lower_base,value='lower (x,y):')

ptcsig_lx_value=widget_text(ptcsig_lower_base,uvalue='ROI_LX',xsize=5,	    $
    	/editable,value='0')
ptcsig_ly_value=widget_text(ptcsig_lower_base,uvalue='ROI_LY',xsize=5,	    $
    	/editable,value='0')

(*diva_ptc_info).ptcsig_in = [ptcsig_ux_value,ptcsig_uy_value,   	    $
    	    	    	      ptcsig_lx_value,ptcsig_ly_value]

roi_table_id=widget_table(ptcsig_input_base,xsize=2,ysize=3,         	    $
    	row_labels=['Mean','Variance','StddeV'],    	    	    	    $
	value=[['0','0'],['0','0'],['0','0']],      	    	    	    $
	alignment=1,column_labels=['IMAGE 1','IMAGE 2'])
	

(*diva_ptc_info).ptc_table_ids = [ptcdc_table_id,roi_table_id]


diva_ptc_doit_button=widget_button(diva_ptc_info_base,value=' Do it ',	    $
    	uvalue='PTC_DOIT',ysize=50,frame=5)

;-------------------quit button------------------------------------------------;
quitbutton=widget_button(diva_ptcbase, value='Quit',uvalue='EXIT')
;------------------------------------------------------------------------------;

;-------------------realise the widget-----------------------------------------;
WIDGET_CONTROL, diva_ptcbase, /REALIZE	

;call 'diva_ptc_display' procedure to display the image 
diva_ptc_display,diva_ptc_info

widget_control,diva_ptcbase,set_uvalue = diva_ptc_info	
						

XManager, "diva_ptc", diva_ptcbase, $		
		EVENT_HANDLER = "diva_ptc_ev", $
		GROUP_LEADER = GROUP,cleanup='diva_ptc_cleanup'		
						
END ;end of "diva_ptc" main routine--------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_ptc_cleanup,diva_ptcbase
;------------------------------------------------------------------------------;
; Purpose - 
;------------------------------------------------------------------------------;
widget_control,diva_ptcbase,get_uvalue=diva_ptc_info
IF (Ptr_Valid(diva_ptc_info) eq 1) then begin
    Ptr_Free,(*diva_ptc_info).ptc_image_1
    Ptr_Free,(*diva_ptc_info).ptc_image_2
    Ptr_Free,(*diva_ptc_info).ptc_image_prev
    Ptr_Free,diva_ptc_info
ENDIF
END ;of 'diva_ptc_cleanup' procedure-------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_ptc_display,diva_ptc_info

diva_ptc_draw	    =	(*diva_ptc_info).diva_ptc_draw
diva_ptc_pix	    =	(*diva_ptc_info).diva_ptc_pix
image_1     	    =	(*diva_ptc_info).ptc_image_1
diva_ptc_drawsize   =	(*diva_ptc_info).diva_ptc_drawsize
image_ct    	    =   (*diva_ptc_info).image_ct
plot_ct     	    =   (*diva_ptc_info).plot_ct
dc_coords   	    =	(*diva_ptc_info).dc_coords
roi_coords  	    =	(*diva_ptc_info).roi_coords

widget_control,diva_ptc_draw,get_value=ptc_draw
wset,ptc_draw
tvscl,congrid(*image_1,diva_ptc_drawsize,diva_ptc_drawsize)

wset,diva_ptc_pix
tvscl,congrid(*image_1,diva_ptc_drawsize,diva_ptc_drawsize)

widget_control,diva_ptc_draw,get_value=ptc_draw
wset,ptc_draw

tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2]
Device,Copy=[0,0,diva_ptc_drawsize,diva_ptc_drawsize,0,0,diva_ptc_pix]
plots,[dc_coords[0],dc_coords[0],dc_coords[2],dc_coords[2], $
    	dc_coords[0] ],[dc_coords[1] ,dc_coords[3], 	    $
	dc_coords[3],dc_coords[1],dc_coords[1]],/device,color=7
plots,[roi_coords[0],roi_coords[0],roi_coords[2],roi_coords[2], $
    	roi_coords[0] ],[roi_coords[1] ,roi_coords[3], 	    $
	roi_coords[3],roi_coords[1],roi_coords[1]],/device,color=2
tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2]

END ;of 'diva_ptc_display' procedure-------------------------------------------;
;------------------------------------------------------------------------------;
