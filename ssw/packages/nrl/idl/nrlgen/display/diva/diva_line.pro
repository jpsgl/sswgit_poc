;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA_LINE_EV
; CATEGORY:
;		event handler
; PURPOSE:
;
; INPUTS:
;		event
; AUTHOR:
;		John A. Rainnie - RAL (j.a.rainnie@rl.ac.uk)
;-
;------------------------------------------------------------------------------;
PRO diva_line_ev, event;-------------------------------------------------------;
;------------------------------------------------------------------------------;
WIDGET_CONTROL, event.id , GET_UVALUE = eventval
widget_control, event.top, get_uvalue = diva_line_info

temp	    	    	=   (*diva_line_info)
line_motion_events_on 	=   temp.line_motion_events_on
 xyc 	    	    	=   temp.xyc
image_ct    	    	=   temp.image_ct
plot_ct     	    	=   temp.plot_ct
rgb_on	    	    	=   temp.rgb_on
diva_rgb_butts	    	=   temp.diva_rgb_butts
coords	    	    	=   temp.coords
const	    	    	=   temp.const
diva_linebase	    	=   temp.diva_linebase
n_dim	    	    	=   temp.n_dim
ymin	    	    	=   temp.yminmax[0]
ymax	    	    	=   temp.yminmax[1]
os_name     	    	=   temp.os_name

CASE eventval OF

"EXIT":     	    	    	    	    	    	    	    	    $
BEGIN
    WIDGET_CONTROL, event.top, /DESTROY     ;destroy widget
    return
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"LINE_DRAW_EVENT":	$
BEGIN
    diva_line_pix=temp.diva_line_pix

    widget_control,temp.diva_line_draw,get_value=line_draw
    wset,line_draw
    ;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ;constrain coords to within the ROI draw area
    event.x = 0 > event.x < (420 - 1)
    event.y = 0 > event.y < (320 - 1)
    Device,Copy=[0,0,420,320,0,0,diva_line_pix]
    ;convert from DEVICE coords to DATA coords
    coords=Convert_Coord(event.x,event.y,/Device,/To_Data)
    xl= !x.crange[0] > coords[0] < !x.crange[1]
    yl= !y.crange[0] > coords[1] < !y.crange[1]
    ;report cursor (x) position....and corresponding value
    widget_control,temp.diva_line_c_v,set_value=    	    	    	    $
    	    	    	    	    strcompress(round(xl),/remove) + ',' +  $
				    				strcompress(round(yl),/remove)
    ;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    possibleEventTypes = [ 'DOWN', 'UP', 'MOTION', 'SCROLL']
    thisEvent = possibleEventTypes(event.type)

    IF (thisevent eq 'DOWN') THEN BEGIN
		(*diva_line_info).line_motion_events_on=1
		(*diva_line_info).xyc[0] = xl
		(*diva_line_info).xyc[1] = yl
    ENDIF ;o;of condition MOUSE_EVENT = "DOWN"

    IF (thisevent eq 'MOTION') THEN BEGIN
    	Device,Copy=[0,0,420,320,0,0,diva_line_pix]
    	xd = xl  &  yd = yl
    	;change to PLOT colour table
    	tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2]
		
		IF (line_motion_events_on eq 0) THEN BEGIN
	    	plots,[xd,xd],[!y.crange[0],!y.crange[1]],thick=1
	    	plots,[!x.crange[0],!x.crange[1]],[yd,yd],thick=1
		ENDIF ELSE BEGIN
	    	xs=xyc[0]  &  ys=xyc[1]
	    	oplot,[xs,xs],[!y.crange[0],!y.crange[1]],color=4,thick=2
	    	oplot,[xd,xd],[!y.crange[0],!y.crange[1]],color=4,thick=2
	    	arrow,xs,ys,xd,ys,/data,hsize=10,/solid,color=4,thick=2
		ENDELSE
		;change back to IMAGE colour table
    	tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2]

    ENDIF ;;of condition MOUSE_EVENT = "MOTION"

    IF (thisevent eq 'UP') THEN BEGIN
		(*diva_line_info).line_motion_events_on=0
    	xs = xyc[0]  &  ys = xyc[1]
		xd = xl      &  yd = yl
		;OK, when mouse button is released, remove arrow/etc
		;and get back to displaying the full crosshair
		Device,Copy=[0,0,420,320,0,0,diva_line_pix]
    	;change to PLOT colour table
    	tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2]
		plots,[xd,xd],[!y.crange(0),!y.crange(1)],thick=1,color=2
		plots,[!x.crange(0),!x.crange(1)],[yd,yd],thick=1,color=2
		;change back to IMAGE colour table
    	tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2]

    	(*diva_line_info).xyc[2] = xd
    	
		IF (abs(xs - xd) lt 5) THEN BEGIN
	    	(*diva_line_info).xyc[0] = min(*temp.x_arr)
	    	(*diva_line_info).xyc[2] = max(*temp.x_arr)
		END
	
	diva_line_display,diva_line_info
    ENDIF;of condition MOUSE_EVENT = "UP"
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"LINE_STATS":		$
BEGIN
    linex = (*temp.linex) 	&   liney = (*temp.liney)

    CASE n_dim OF
    	1:  BEGIN
				s	    = moment(liney)
		        s_min	= [min(liney),0,0]
		        s_max	= [max(liney),0,0]
		        s_ave	= [s[0] 	 ,0,0]
		        s_var	= [s[1] 	 ,0,0]
		        s_std	= [sqrt(s[1]),0,0]
		        ;and collect them
		        (*diva_line_info).all_stats[4:*] =  						$
											[s_min,s_max,s_ave,s_var,s_std]
	    	END
		3:	BEGIN
    			s_r = moment(liney[*,0])
		        s_g = moment(liney[*,1])
		        s_b = moment(liney[*,2])
		        s_min = [min(liney[*,0]),min(liney[*,1]),min(liney[*,2])]
		        s_max = [max(liney[*,0]),max(liney[*,1]),max(liney[*,2])]
		        s_ave = [s_r[0],s_g[0],s_b[0]]
		        s_var = [s_r[1],s_g[1],s_b[1]]
		        s_std = [sqrt(s_r[1]),sqrt(s_g[1]),sqrt(s_b[1])]
		        ;and collect them
		        (*diva_line_info).all_stats[4:*] =  						$
											[s_min,s_max,s_ave,s_var,s_std]
			END
    ENDCASE
    all_stats =  (*diva_line_info).all_stats
    diva_stats,all_stats,1,temp.n_dim,group=diva_linebase

END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"LINE_PRINT":		$
BEGIN
    x = (*temp.linex) 	&   y = (*temp.liney)
    
    full_name 	= temp.file_stuff[0]
    path_name 	= temp.file_stuff[1]
    file_str  	= temp.file_stuff[2]
    mfile_name  = temp.file_stuff[3]
    
    file_in	= file_str + '_line.ps'
    
    ;call 'diva_print_what' FUNCTION to get filename and 
    result = diva_print_what(path_name,file_in,temp.diva_printers,			$
								temp.diva_printers_list,temp.diva_linebase)
    
	IF ((*result).do_print_yes eq 1) THEN BEGIN
    	filename 		= (*result).path+(*result).filename
		what_printer	= temp.diva_printers[(*result).what_printer]
		do_save_yes 	= (*result).do_save_yes
		
    	print,'printing to: "'+strcompress(filename)+'"'
    
    	set_plot,'ps'
    	device,filename=filename,/color

    	;change to PLOT colour table
    	tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2]
    	;call 'diva_line_plot' to plot data
    	diva_line_plot,x,y,n_dim,ymin,ymax,rgb_on
    	;change back to IMAGE colour table
    	tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2]

    	device,/close
    	
		CASE os_name OF
    		'X':	BEGIN
						set_plot,'x'
						;call "diva_print_linux" pro to do printing!!!
						diva_print_linux,filename,do_save_yes,what_printer
					END
			'WIN':	    set_plot,'win'
    	ENDCASE
			
		
		widget_control,temp.diva_line_draw,get_value=line_draw
		wset,line_draw
    	;change to PLOT colour table
    	tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2]
    	;call 'diva_line_plot' to plot data
    	diva_line_plot,x,y,n_dim,ymin,ymax,rgb_on
    	;change back to IMAGE colour table
    	tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2]
	ENDIF
    ;now free up the 'result' pointer
    Ptr_Free,result


END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
;!!!!!!!!!!! COLOUR OPTIONS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"COL_R":		$
BEGIN
    CASE event.select OF
    	1:  BEGIN
	    	(*diva_line_info).rgb_on[0] = 1
		widget_control,diva_rgb_butts[0],sensitive = 1
		widget_control,diva_rgb_butts[1],sensitive = 1
		widget_control,diva_rgb_butts[2],sensitive = 1
	    END
    	0:  BEGIN
	    	(*diva_line_info).rgb_on[0] = 0
		if (rgb_on[1] eq 0) and (rgb_on[2] eq 1) then 	    	    		$
		    	    			widget_control,diva_rgb_butts[2],sensitive=0
		if (rgb_on[1] eq 1) and (rgb_on[2] eq 0) then 	    	    		$
		    	    			widget_control,diva_rgb_butts[1],sensitive=0
	    END
    ENDCASE
    diva_line_display,diva_line_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"COL_G":		$
BEGIN
    CASE event.select OF
    	1:  BEGIN
	    	(*diva_line_info).rgb_on[1] = 1
		widget_control,diva_rgb_butts[0],sensitive = 1
		widget_control,diva_rgb_butts[1],sensitive = 1
		widget_control,diva_rgb_butts[2],sensitive = 1
	    END
    	0:  BEGIN
	    	(*diva_line_info).rgb_on[1] = 0
		if (rgb_on[0] eq 0) and (rgb_on[2] eq 1) then 	    	    $
		    	    widget_control,diva_rgb_butts[2],sensitive=0
		if (rgb_on[0] eq 1) and (rgb_on[2] eq 0) then 	    	    $
		    	    widget_control,diva_rgb_butts[0],sensitive=0
	    END
    ENDCASE
    diva_line_display,diva_line_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"COL_B":		$
BEGIN
    CASE event.select OF
    	1:  BEGIN
	    		(*diva_line_info).rgb_on[2] = 1
				widget_control,diva_rgb_butts[0],sensitive = 1
				widget_control,diva_rgb_butts[1],sensitive = 1
				widget_control,diva_rgb_butts[2],sensitive = 1
	    	END
    	0:  BEGIN
	    		(*diva_line_info).rgb_on[2] = 0
				if (rgb_on[0] eq 0) AND (rgb_on[1] eq 1) then 	    	    $
		    	    			widget_control,diva_rgb_butts[1],sensitive=0
				if (rgb_on[0] eq 1) AND (rgb_on[1] eq 0) then 	    	    $
		    	    			widget_control,diva_rgb_butts[0],sensitive=0
	    	END
    ENDCASE
    diva_line_display,diva_line_info
END;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

ELSE: MESSAGE, "Event User Value Not Found"
ENDCASE
END ;end of diva_roi event handling routine task-------------------------------;
;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA_LINE_INFO__DEFINE
; PURPOSE:
;		This procedure defines the "diva_line_info" (named) structure
;		(see IDL help: "Automatic Stucture Definition").
; INPUTS:
;		None
; AUTHOR:
;		John A. Rainnie - RAL (j.a.rainnie@rl.ac.uk)
;-
;------------------------------------------------------------------------------;
PRO diva_line_info__define
;------------------------------------------------------------------------------;

junk =	{	diva_line_info,    	    	    	    	    	    		$
    	    x_arr   	    	            :Ptr_New(), 					$
    	    y_arr   	    	            :Ptr_New(), 					$
		    linex   	    	            :Ptr_New(), 					$
		    liney   	    	            :Ptr_New(), 					$
		    n_dim		    		        :0, 							$
		    const   	    	            :0, 							$
		    all_stats	    	            :findgen(19),					$
		    diva_linebase   	            :0L,							$
    	    diva_line_c_v	    	        :0L,							$
		    diva_line_minmax_id             :0L,							$
		    line_motion_events_on			:0, 							$
		    diva_line_draw	    	        :0L,							$
		    diva_line_pix	    	        :0L,							$
		    rgb_on		    		        :[0,0,0],						$
		    yminmax 	    	            :[0L,0L],						$
		    diva_rgb_butts	    	        :[0L,0L,0L],					$
		    image_ct		    	        :bytarr(256,3), 				$
		    plot_ct		    		        :bytarr(256,3), 				$
		    coords  	    	            :[0,0,0,0], 					$
		    xyc 		    		        :[0,0,0,0], 					$
		    os_name 	    	            :'',							$
		    file_stuff	    	            :['','','',''], 				$
			diva_printers			        :strarr(3), 					$
			diva_printers_list		        :strarr(3)  					$
    	}
		
END;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;+
; NAME:
;		DIVA_LINE_INIT
; CATEGORY:
;		Widget
; PURPOSE:
;		This function assigns real values to "diva_line_info" (named)
;		structure
; INPUTS:
;		n_dim				- number of planes
;		image_ct			- image colour table (greyscale)
;		plot_ct 			- plotting colour table
;		diva_draw_base		-
;		os_name 			- the operating system
;		file_stuff			- a string array
;		diva_printers		- 
;		diva_printers_list	-
; NOTES:
;		The data may be single banded (greyscale images) or be RGB (3 bands)
; AUTHOR:
;		John A. Rainnie - RAL (j.a.rainnie@rl.ac.uk)
;-
;------------------------------------------------------------------------------;
FUNCTION diva_line_init,n_dim,image_ct,plot_ct,diva_draw_base,	    	    $
    	    os_name,file_stuff,diva_printers,diva_printers_list
;------------------------------------------------------------------------------;
diva_line_info	    	            =	{diva_line_info}
diva_line_info.n_dim	            =	n_dim
diva_line_info.image_ct             =	image_ct
diva_line_info.plot_ct              =	plot_ct
diva_line_info.os_name	            =	os_name
diva_line_info.file_stuff           =	file_stuff
diva_line_info.diva_printers		=	diva_printers
diva_line_info.diva_printers_list	=	diva_printers_list

CASE n_dim OF
	1:		diva_line_info.rgb_on 	= [0,0,0]
	3:		diva_line_info.rgb_on 	= [1,1,1]
ENDCASE

;-------------------create the main base---------------------------------------;
diva_linebase = WIDGET_BASE(TITLE = "DIVA LINE PROFILE",/col,	    	    $
    	    	    	    	group_leader=diva_draw_base,/floating)
diva_line_info.diva_linebase = diva_linebase
;-------------------diva line button base--------------------------------------;
diva_line_button_base=widget_base(diva_linebase,/row,/frame)

diva_line_stats=widget_button(diva_line_button_base,value='Stats...',       $
	    uvalue='LINE_STATS')
diva_line_print=widget_button(diva_line_button_base,value='Print...',       $
	    uvalue='LINE_PRINT')
;-------------------rgb colour base--------------------------------------------;
CASE n_dim OF
    3:	line_rgb_base = widget_base(diva_line_button_base,/row,             $
    	   			   /frame,/nonexclusive)
    1:	line_rgb_base = widget_base(diva_line_button_base,/row,             $
    	    	    	    	    	/frame,/nonexclusive,map=0)
ELSE :	stop
ENDCASE
    diva_line_rbutt = widget_button(line_rgb_base,value=' R ',uvalue='COL_R')
    diva_line_gbutt = widget_button(line_rgb_base,value=' G ',uvalue='COL_G')
    diva_line_bbutt = widget_button(line_rgb_base,value=' B ',uvalue='COL_B')
    widget_control,diva_line_rbutt,set_button = 1
    widget_control,diva_line_gbutt,set_button = 1
    widget_control,diva_line_bbutt,set_button = 1
    diva_line_info.diva_rgb_butts  =     	    	    	    	    $
    	    	    	    [diva_line_rbutt,diva_line_gbutt,diva_line_bbutt]
;-------------------plot window------------------------------------------------;
diva_line_info.diva_line_draw    =	widget_draw(diva_linebase,  	    $
    	    	    	    	    	    	    xsize=420,ysize=320,    $
    	    	Button_Events=1, Event_Pro='diva_line_ev',frame=5,  	    $
		/motion_events,/viewport_events,uvalue="LINE_DRAW_EVENT")

window,/free,/pixmap,xsize=420,ysize=320
diva_line_info.diva_line_pix = !D.window
;-------------------coords base------------------------------------------------;
diva_line_coords_base	 =widget_base(diva_linebase,/row,/frame)
diva_line_coords_c_l=widget_label(diva_line_coords_base,value=' coords:')
diva_line_info.diva_line_c_v=widget_text(diva_line_coords_base,  	    $
    	    	    	    	    	    	    value='0,0',xsize=12)

CASE n_dim OF
    1:	minmaxval = '0,0'
    3:	minmaxval = '0:0,0:0,0:0'
ENDCASE
diva_line_minmax=widget_label(diva_line_coords_base,value='  min/max:')
diva_line_info.diva_line_minmax_id=widget_text(diva_line_coords_base,       $
    	    	    	    	    	    	    value=minmaxval,xsize=30)

;-------------------quit button------------------------------------------------;
quitbutton=widget_button(diva_linebase, value='Quit',uvalue='EXIT')
;------------------------------------------------------------------------------;

WIDGET_CONTROL, diva_linebase, /REALIZE

pdiva_line_info = Ptr_New(diva_line_info)

widget_control,diva_linebase,set_uvalue = pdiva_line_info

XManager, "diva_line", diva_linebase,EVENT_HANDLER = "diva_line_ev",	    $
		GROUP_LEADER = diva_draw_base,cleanup = 'diva_line_cleanup'


Return, pdiva_line_info

END ;end of diva_line main routine---------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_line_cleanup,diva_linebase
;------------------------------------------------------------------------------;
; Purpose - to cleanup diva_line widget - specifically to free it's pointer!
;------------------------------------------------------------------------------;
widget_control,diva_linebase,get_uvalue=diva_line_info
print,'CLEANING UP DIVA LINE    *************************'
IF (Ptr_Valid(diva_line_info) eq 1) then begin
    Ptr_Free,(*diva_line_info).x_arr
    Ptr_Free,(*diva_line_info).y_arr
    Ptr_Free,(*diva_line_info).linex
    Ptr_Free,(*diva_line_info).liney
    Ptr_Free,diva_line_info
ENDIF
END;---------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
PRO diva_line_display,diva_line_info
;------------------------------------------------------------------------------;
; Purpose - to plot line profile of image
;------------------------------------------------------------------------------;
temp 	    	    = (*diva_line_info)
linex	    	    = (*temp.x_arr)
liney	    	    = (*temp.y_arr)
xyc 	    	    = temp.xyc
;OK, this is the complicated bit!!  - where data coords are converted into
;element indicies
nlinex	    	    = [xyc[0]-linex[0],xyc[2]-linex[0]]
nlinex	    	    = nlinex(sort(nlinex))
linex	    	    = linex[nlinex[0]:nlinex[1]]
liney	    	    = liney[nlinex[0]:nlinex[1],*]
;phew, glad that's over!!!!!
;stop
Ptr_Free,(*diva_line_info).linex
Ptr_Free,(*diva_line_info).liney
(*diva_line_info).linex  = Ptr_New(linex)
(*diva_line_info).liney  = Ptr_New(liney)


n_dim	    	    = temp.n_dim
rgb_on	    	    = temp.rgb_on
diva_rgb_butts	    = temp.diva_rgb_butts
image_ct    	    = temp.image_ct
plot_ct     	    = temp.plot_ct
diva_line_minmax_id = temp.diva_line_minmax_id
coords	    	    = temp.coords
const	    	    = temp.const


wot_size = fix(total(rgb_on))
IF (wot_size lt 1) THEN wot_size = 1	

index = where(rgb_on ne 0)
IF (total(index) lt 0) THEN index = 0	;in the case of n_dim = 1

yminmax = lonarr(wot_size,2)
FOR i	= 0,wot_size - 1 DO BEGIN
	yminmax[i,0]	=	min(liney[*,index[i]])
	yminmax[i,1]	=	max(liney[*,index[i]])
ENDFOR
ymin = min(yminmax[*,0])
ymax = max(yminmax[*,1])

(*diva_line_info).yminmax = [ymin,ymax]

tvlct,plot_ct[*,0],plot_ct[*,1],plot_ct[*,2];change to PLOT colour table

;get id of line widget draw area and set as current
widget_control,temp.diva_line_draw,get_value=line_draw
wset,line_draw
;call 'diva_line_plot' to plot data
diva_line_plot,linex,liney,n_dim,ymin,ymax,rgb_on

;get id of diva_line pixmap draw area, make it current and plot line profile
wset,temp.diva_line_pix
;call 'diva_line_plot' to plot data
diva_line_plot,linex,liney,n_dim,ymin,ymax,rgb_on

tvlct,image_ct[*,0],image_ct[*,1],image_ct[*,2];change back to IMAGE colour table

CASE n_dim OF
    1:	widget_control,diva_line_minmax_id,set_value =      	    	    $
						strcompress(min(liney),/rem)+ ',' + 				$
						strcompress(max(liney),/rem)
    3:	widget_control,diva_line_minmax_id,set_value =      	    	    $
    	    			strcompress(min(fix(liney[*,0])),/rem) + ':'  +   $
	    				strcompress(max(fix(liney[*,0])),/rem) +','   +   $
	    				strcompress(min(fix(liney[*,1])),/rem) + ':'  +   $
	    				strcompress(max(fix(liney[*,1])),/rem) +','   +   $
	    				strcompress(min(fix(liney[*,2])),/rem) + ':'  +   $
	    				strcompress(max(fix(liney[*,2])),/rem)
ENDCASE

;"const" is defined in "diva_draw"
if (n_elements(coords(where(coords eq const))) eq 2) then begin
    if (coords[1] eq const) AND (coords[3] eq const) then begin
    	;ie - a horizontal line
		(*diva_line_info).all_stats[0:3] = [min(linex),const,max(linex),const]
    endif else begin	;or a vertical line
    	(*diva_line_info).all_stats[0:3] = [const,min(linex),const,max(linex)]
    endelse
endif else begin
    
endelse

END ;of 'diva_line_display' procedure------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
;------------------------------------------------------------------------------;
pro diva_line_plot,linex,liney,n_dim,ymin,ymax,rgb_on
;------------------------------------------------------------------------------;
; Purpose -
;------------------------------------------------------------------------------;
l= linex
!p.charsize = 1.2 & !p.thick = 2
CASE n_dim OF
    1:	BEGIN
    	    plot, l,liney,ytit='Value',yr=[ymin,ymax],/nodata
    	    oplot,l,liney,psym=10,color=2
    	END
    3:	BEGIN
    	    plot, l,liney,ytit='Value',yr=[ymin,ymax],/nodata
    	    if (rgb_on[0] eq 1) then oplot,l,liney[*,0],psym=10,color=1
    	    if (rgb_on[1] eq 1) then oplot,l,liney[*,1],psym=10,color=2
    	    if (rgb_on[2] eq 1) then oplot,l,liney[*,2],psym=10,color=3
    	END
ENDCASE
!p.charsize = 1 & !p.thick = 1

END ;--------------------------------------------------------------------------;
;------------------------------------------------------------------------------;