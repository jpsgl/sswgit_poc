function nrgf,im00,ht0,indmn,av=av,st=st,hts=hts, $
		nopoly=nopoly,maxbin=maxbin, $
		htrange=htrange,pix_size=pix_size,crpix1=crpix1,crpix2=crpix2, $
		coeffav=coeffav,coeffst=coeffst,returnvalue=returnvalue

;+
; $Id: nrgf.pro,v 1.3 2010/04/20 14:08:42 mcnutt Exp $
; NAME:
; 	nrgf
;
; PURPOSE:
;  applies Normalizing Radial Graded Filter (nrgf) processing to image 
;  (see morgan, habbal & woo, solar physics 2006)
;
; CALLING SEQUENCE:
;   im=nrgf(im00,ht0,indmn [,av=av,st=st,hts=hts, $
;		nopoly=nopoly,maxbin=maxbin, $
;		coeffav=coeffav,coeffst=coeffst])
;
;
; INPUTS:
;	im00 = input image
;	ht0 = height of each pixel in image from Sun center
;	indmn = index of pixels to process
;	nopoly = keyword, if set do not use polynomial fit
;			to radial functions of mean and std dev
;	maxbin = set to maximum number of bins in
;			radial functions of mean and std dev
;
; KEYWORDS:
;   	htrange=[minheight,maxheight]
;   	pix_size=pixel_size
;    	crpix1=x center pixel
;   	crpix2=y center pixel
;   	coeffav=optional output
;   	coeffst=optional output
;   	returnvalue= ouput structure containing av,st,and hts
;
; OUTPUTS:
;  im = NRGF-processed output image
;  
; OPTIONAL OUTPUTS
;   	coeffav=poly_fit(hts,av,4,yfit=avf,status=status)
;   	coeffst=poly_fit(hts,st,4,yfit=stf,status=status)
;	av = profile of mean brightness as function of height
;	st = profile of standard deviation of brightness as function of height
;	hts = height array, used to determine av and st
;
; PROCEDURE:
;
;
;
; MODIFICATION HISTORY:
; $Log: nrgf.pro,v $
; Revision 1.3  2010/04/20 14:08:42  mcnutt
; added keyword discriptions
;
; Revision 1.2  2010/04/19 19:31:09  nathan
; added nrgf defn
;
;	Created 10/2008 - Huw Morgan hmorgan@ifa.hawaii.edu
;-

status=0

if n_params() eq 0 then begin
  print,'NRGF: Please provide image!!!'
  return,-1
endif

s=size(im00)
if n_params() lt 2 then begin;need to create height array
  if s[0] ne 2 then begin
    print,'NRGF: I can only handle arrays of arbitrary dimension '
    print,'      if you provide height of each pixel!!!'
    return,-1
  endif
  if not(keyword_set(crpix1)) then begin
    print,'NRGF: Height array not provided'
    print,'      Assume center of 2D image is Sun center'
  endif
  if not(keyword_set(pix_size)) then begin
    print,'NRGF: Pixel size not provided'
    print,'      Assume pix_size=1'
  endif
  crpix1=keyword_set(crpix1)?crpix1:s[1]*0.5
  crpix2=keyword_set(crpix2)?crpix2:s[2]*0.5
  pix_size=keyword_set(pix_size)?pix_size:1.
  x=(findgen(s[1])-crpix1)*pix_size
  y=(findgen(s[1])-crpix2)*pix_size
  xx=rebin(x,s[1],s[2])
  yy=rebin(reform(y,1,s[2]),s[1],s[2])
  ht0=sqrt(xx^2+yy^2)
endif

cntindmn=n_elements(indmn)
if cntindmn eq 0 then begin
  if not(keyword_set(htrange)) then begin
    print,'NRGF: Index of pixels to process not given, '
    print,'      nor desired height range specified, '
    print,'      so processing all pixels'
    indmn=where(finite(im00),cntindmn)
  endif else begin
    indmn=where(ht0 ge htrange[0] and ht0 le htrange[1] and $
                  finite(im00),cntindmn)           
    if cntindmn eq 0 then begin
      print,'NRGF: No finite pixels in given height range, returning -1'
      return,-1
    endif
  endelse
endif
if cntindmn eq 0 then begin
  print,'NRGF: No appropriate region to process, returning'
  return,-1
endif

sillypixelsdone=0
indforhtav=indmn
im0=im00*!values.f_nan
im=float(im00[indmn]) & ht=ht0[indmn]

again:
if not(keyword_set(maxbin)) then maxbin=100
hts=min(ht)+findgen(maxbin)*(max(ht)-min(ht))/(maxbin-1.)
dhts2=(hts[1]-hts[0])*0.5
rebin_huw,im00[indforhtav],ht0[indforhtav],hts,av,st
hts=hts+dhts2

ind=where(finite(av) eq 1 and finite(st) eq 1,countgood)
av=av[ind] & st=st[ind] & hts=hts[ind]

;do quick NRGF to identify silly pixels
if not(sillypixelsdone) then begin
  ims=im-interpol(av,hts,ht)
  ims=temporary(ims)/interpol(st,hts,ht)
  h=histogram(ims,nbin=101,/nan,reverse_ind=ri,loc=loc)
  t=total(h,/cum)/total(float(h))
  ind=where(t gt 1.e-2 and t lt (1-1.e-2))
  if ind(0) gt -1 then begin
    minval=min(loc[ind],max=maxval)
    indok=where(ims ge minval and ims le maxval)
    if indok(0) gt -1 then indforhtav=indforhtav[indok]
  endif
  sillypixelsdone=1
  goto,again
endif

if not(keyword_set(nopoly)) then begin
  
  mn=min(av,max=mx,/nan)
  range=mx-mn
  minconst=mn-0.01*range
  av=av-minconst
	av=alog10(av)
	st=alog10(st)

	ss=abs(st-median(st,7))
	m=sqrt((moment(ss,/nan))(1))
	inds=where(ss lt m*0.7,cnt)
	if cnt gt maxbin*0.5 then begin
		hts=hts[inds] & av=av[inds] & st=st[inds]
	endif
	ind=where(finite(av) eq 1 and finite(st) eq 1,cnt)
	if cnt eq 0 then begin
		print,'NRGF polyfit is not successful'
		print,'Doing NRGF without polyfit'
		nopoly=1
		goto,again
	endif else begin
		hts=hts[ind] & av=av[ind] & st=st[ind]
	endelse

	coeffav=poly_fit(hts,av,4,yfit=avf,status=status)
	coeffst=poly_fit(hts,st,4,yfit=stf,status=status)
	av=avf
	st=stf

	if status gt 0 then begin
		print,'NRGF polyfit is not successful'
		print,'Doing NRGF without polyfit'
		nopoly=1
		goto,again
	endif

	avv=interpol(av,hts,ht)
	avv=10^temporary(avv)
	avv=temporary(avv)+minconst
	stt=interpol(st,hts,ht)
	stt=10^temporary(stt)
	
	im=temporary(im)-avv
	im=temporary(im)/stt

	av=10^temporary(av)
	av=temporary(av)+minconst
	st=10^temporary(st)


endif else begin
	im=temporary(im)-interpol(av,hts,ht)
	im=temporary(im)/interpol(st,hts,ht)
endelse

returnvalue={ht:hts,av:av,st:st}
im0[indmn]=im

return,im0

end
