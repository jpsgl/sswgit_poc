;+
; $Id: setx.pro,v 1.2 2011/12/01 20:56:46 nathan Exp $
;
; PURPOSE:
;  set x display and reset plotting parameters
;
; CATEGORY:
;  plotting
;
; INPUTS:
;  none
;
; $Log: setx.pro,v $
; Revision 1.2  2011/12/01 20:56:46  nathan
; use select_windows for windows compat
;
;-
pro setx


select_windows	;set_plot,'x'
!p.thick=1
!p.charthick=1.
!p.charsize=1.
!p.font=-1
!x.thick=1.
!y.thick=1.

return
end
