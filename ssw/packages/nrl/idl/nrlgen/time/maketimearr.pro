function maketimearr, sttim, entim, increment, hours=hours, days=days
;+
; $Id: maketimearr.pro,v 1.2 2012/11/06 22:56:32 nathan Exp $
;Project   : STEREO SECCHI
;
;NAME:	maketimearr
;
;PURPOSE:   Build a CDS time structure array spanning the input times
;	with a settable increment step
;INPUT:
;	sttim	- The start time (nrlanytim format)
;	entim	- The end time
;	increment- The step size in seconds
;       hours   - The step size interpreted as hours 
;       days    - The step size interpreted as days 
;OUTPUT:
;	Returns an array of CDS time structures
;HISTORY:
; $Log: maketimearr.pro,v $
; Revision 1.2  2012/11/06 22:56:32  nathan
; allow nrlanytim format; adjust output to include end of last interval
;
; Revision 1.1  2008/02/11 19:06:12  nathan
; rewrite of build_timarr.pro for CDS output
;
;	original pro Written 27-Aug-93 by M.Morrison
;       5-Jul-00, made the offset variable double-precision (HSH)
;-
;
tai0 = utc2tai(nrlanytim2utc(sttim))
tai1 = utc2tai(nrlanytim2utc(entim))
dur  = tai1-tai0	;duration in seconds
nsec = increment
if (keyword_set(hours)) then nsec = increment*60.*60.
if (keyword_set(days)) then nsec = increment*60.*60.*24.
;
n = ceil(dur/nsec)
stop
taiarr=tai0+dindgen(n)*nsec
out = tai2utc(taiarr,/nocorrect)
;

return, out
end
