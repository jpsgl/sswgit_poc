
function mjd2utc,mjd
; $Id: mjd2utc.pro,v 1.2 2010/11/04 20:13:05 nathan Exp $
;
; Purpose:
;  mjd2utc function excepts an array of mjd times, converts them to utc, and 
;  returns them. It handles the modified julian days by adding 50000 to it 
;  when converting to utc. FInally, it subtracts 12 hrs from the mjd times
;  since mjd starts as 12:00 hr but UT starts at 00:00 hr. 
;
; Calling Sequence:
;  utc= mjd2utc(mjd)
;
; Input:
;  mjd: double array of mjd. Both modified mjd values, like 98.540861, and 
;       normal mjd values like, 50098.540861, are handled.
;
; Output:
;  utc: an array of {mjd:0L,time:0L} structures. 
;
; $Log: mjd2utc.pro,v $
; Revision 1.2  2010/11/04 20:13:05  nathan
; called by make_xyr_med_offsets.pro in dev/lasco/pointing
;
; Ed Esfandiari - Feb 1999.
;

 n= n_elements(mjd)
 utc= {mjd:0L,time:0L}
 utc= replicate(utc,n)
 for i=0D,n-1 do begin
   ; pick up mjd part of the mjd (mjd is double):
   utc(i).mjd= long(strmid(mjd(i),0,strpos(mjd(i),'.')))
   if(utc(i).mjd lt 50000L) then begin
     ;Modified JD. 50000 must be added to get mjd:
     utc(i).mjd= mjd(i)+50000L
   end
   ; Make sure all 6 decimal digits are picked up:
   smjd= strtrim(string(mjd(i),format='(d12.6)'),2)

   ; Change the fraction of the day to miliseconds and save as time:
   utc(i).time= float(strmid(smjd,strpos(smjd,'.'),10)) * 24. * 3600 * 1000

   ; Change from MJD to UT (subtract 12 hrs from MJD since mjd starts at 12
   ; but UT starts at 00 hr):

   utc(i).time= utc(i).time - (12.* 3600 * 1000)
   if(utc(i).time lt 0L) then begin
     utc(i).mjd= utc(i).mjd - 1L
     utc(i).time= utc(i).time + 86400000L
   end
 end 



  return,utc
end
