;+
; $Id: match_cadence.pro,v 1.1 2010/08/24 22:16:33 nathan Exp $
function match_cadence, li, cad
;
; Project   : STEREO SECCHI
;                   
; Name      : match_cadence
;               
; Purpose   : make a list from input at input cadence -or- matching input list
;               
; Explanation: 
;               
; Examples:   IDL> listout= match_cadence(list1,30)
;             IDL> listout= match_cadence(list1,listtomatch)
;    
; Inputs    :	li  	STRARR  List of filenames which start YYYYMMDD_HHMM
;   	    	cad 	FLOAT   Desired cadence of output in minutes -or-
;   	    	    	STRARR	List of filenames to which to match li 
;            
; Outputs   : STRARR
;
; Keywords  : 
;
; Calls from LASCO : 
;
; Common    : 
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : movie sorting analysis
;               
; Prev. Hist. : None.
;
; Written     : N.Rich, NRL/I2 Aug 2010
;               
; $Log: match_cadence.pro,v $
; Revision 1.1  2010/08/24 22:16:33  nathan
; for making input lists for tiled movies
;

n_in=n_elements(li)
nout0=n_elements(cad)

IF nout0 EQ 1 THEN BEGIN
    break_file,li[0],di,pa,ro,su
    ut0=yymmdd2utc(strmid(ro,0,13)+'00')
    break_file,li[n_in-1],di,pa,ro,su
    ut1=yymmdd2utc(strmid(ro,0,13)+'00')
    utcad=maketimearr(ut0,ut1,cad*60.)
    nout=n_elements(utcad)
ENDIF ELSE nout=nout0

timesin=strarr(n_in)
timesot=strarr(nout)
outli=strarr(nout)

for i=0,n_in-1 do begin
    break_file,li[i],di,pa,ro,su
    timesin[i]=strmid(ro,0,13)
endfor

IF nout0 EQ 1 THEN BEGIN
    for i=0,nout-1 do timesot[i]=utc2yymmdd(utcad[i],/hhmmss,/yyyy)
ENDIF ELSE BEGIN
    for i=0,nout-1 do begin
	break_file,cad[i],di,pa,ro,su
	timesot[i]=strmid(ro,0,13)
    endfor
ENDELSE

for i=0,nout-1 do begin
    ind=find_closest(timesot[i],timesin)
    outli[i]=li[ind]
endfor

return,outli

END
