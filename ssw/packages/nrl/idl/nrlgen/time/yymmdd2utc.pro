function yymmdd2utc,dates, _EXTRA=_extra
;
;+
;$Id: yymmdd2utc.pro,v 1.3 2010/11/18 22:33:15 nathan Exp $
; NAME:
;	YYMMDD2UTC
;
; PURPOSE:
;	This function converts a date string in the format [YY]YYMMDD[_][HHMMSS]
;       into a modified julian date structure
;
; CATEGORY: UTIL, time
;
; CALLING SEQUENCE:
;	Result = YYMMDD2UTC(Dates)
;
; INPUTS:
;	Dates:	Date string in the format yymmdd, yyyymmdd, or yyyymmdd_hhmmss,
;               or mjd or CDS time structure
;
; OUTPUTS:
;	This function returns a CDS date structure.  If the input is an array of
;	date strings, then the output will be an array of structures.
;
; MODIFICATION HISTORY:
; 	Written by:	RA Howard, 1995
; $Log: yymmdd2utc.pro,v $
; Revision 1.3  2010/11/18 22:33:15  nathan
; handle more types of input
;
; Revision 1.2  2009/07/29 21:43:48  nathan
; allow input to specify hhmm only
;
; Revision 1.1  2008/02/20 18:50:02  nathan
; moved from lasco/idl/util
;
;	V2:  RAH, Jun 2, 1997, input dates can be long, string or CDS time structure
;	V3:  RAH, Sep 22, 1997, corrected CDS time structure to be longs not integers
;   2005.03.17, nbr - allow/use _hhmmss in argument
;   2005.03.30, nbr - fix bug that modified argument
;
;	@(#)yymmdd2utc.pro	1.6 03/30/05 LASCO IDL LIBRARY
;-
sz=SIZE(dates)
dt = datatype(dates)
IF (dt EQ 'LON') THEN BEGIN
   dte={CDS_INT_TIME,mjd:0L,time:0L}
   IF (sz(0) EQ 1)  THEN dte=REPLICATE(dte,sz(1))
   dte.mjd = dates
   RETURN,dte
ENDIF
IF (dt EQ 'STC') THEN RETURN,dates
IF (dt NE 'STR') THEN BEGIN
   PRINT,'%ERROR: YYMMDD2UTC: Unrecognized datatype '+datatype(dates)
   RETURN,-1
ENDIF
;
;  Now process string array of dates
;
IF (sz(0) EQ 0) THEN dates2=make_array(1,value=dates) ELSE dates2=dates
   
len = STRLEN(dates2)
dte = dates2
w = WHERE(len EQ 6,nw)
IF (nw GT 0)  THEN BEGIN
    yy = FIX(STRMID(dates2(w),0,2)) 
    ww = WHERE (yy GE 70,nww)
    IF (nww GT 0)  THEN dte(w(ww)) = '19'+dates2(w(ww))
    ww = WHERE (yy LT 70,nww)
    IF (nww GT 0)  THEN dte(w(ww)) = '20'+dates2(w(ww))
ENDIF
dd = where(len EQ 6 or len EQ 8,ndd)    ; only date
hh = where(len EQ 11, nhh)  	    	; only HH
mm = where(len EQ 13, nmm)  	    	; only HHMM
IF nmm GT 0 THEN dte[mm]=dte[mm]+'00'
IF nhh GT 0 THEN dte[hh]=dte[hh]+'0000'
IF ndd GT 0 THEN dte[dd]=dte[dd]+'_000000'
w = where(len EQ 14,nw)     ; no underscore in argument
IF (nw GT 0) THEN dte[w]=strmid(dte[w],0,8)+'_'+strmid(dte[w],8,6)

dte =   STRMID(dte,0,4) +'-'+STRMID(dte,4,2)  +'-'+STRMID(dte,6,2) +'T' + $
        STRMID(dte,9,2) +':'+STRMID(dte,11,2) +':'+STRMID(dte,13,2)
        
IF (sz[0] EQ 0) THEN dte=dte[0]
RETURN,anytim2utc(dte, _EXTRA=_extra)
END
