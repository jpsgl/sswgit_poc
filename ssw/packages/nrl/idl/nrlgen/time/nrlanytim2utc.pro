;+
function nrlanytim2utc,inp,_EXTRA=_extra
; $Id: nrlanytim2utc.pro,v 1.3 2016/11/30 21:21:45 nathan Exp $
;
; NAME:
;	NRLanytim2utc
;
; PURPOSE:
;	This is an extension of anytim2utc.pro to include yymmdd format and YY-DOY- format.
;
; CATEGORY: UTIL time gen string
;
; CALLING SEQUENCE:
;	Result = NRLanytim2utc(inp)
;
; INPUTS:
;	inp:	Any time format accepted by anytim2utc.pro, PLUS yymmdd_hhmmss format
;
; OPTIONAL KEYWORDS:
;	Passed to anytim2utc
;
; OUTPUTS:
;	This function returns a CDS structure (see $SSW/gen/idl/time)
;
; CALLS:    yymmdd2utc.pro, anytim2utc.pro
;
; MODIFICATION HISTORY:
; 	Written by:	N.Rich, 7/2009
;
; $Log: nrlanytim2utc.pro,v $
; Revision 1.3  2016/11/30 21:21:45  nathan
; input YY-DOY (ITOS) format
;
; Revision 1.2  2011/02/01 19:50:10  nathan
; be sure to use _extra
;
; Revision 1.1  2009/07/29 21:43:20  nathan
; born
;
;-
dtin=datatype(inp)

IF dtin NE 'STR' THEN return,anytim2utc(inp,_EXTRA=_extra)

IF strpos(inp[0],'/') LT 0 and strpos(inp[0],'-') LT 0 THEN return, yymmdd2utc(inp,_EXTRA=_extra)

xx=strsplit(inp[0],'-',count=c)
IF c EQ 3 THEN IF (xx[1]-xx[0] EQ 3 and xx[2]-xx[1] EQ 4) THEN strput,inp,'T',6

return,anytim2utc(inp,_EXTRA=_extra)

END
