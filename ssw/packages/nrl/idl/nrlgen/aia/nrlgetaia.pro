function nrlgetaia, times, wave, cadence, HOURS=hours, DIRECTORY=directory
;+
; $Id: nrlgetaia.pro,v 1.5 2011/11/21 17:31:32 nathan Exp $
;
; Project   : AIA NRL
;                   
; Name      : 
;               
; Purpose   : 
;               
; Explanation: Tries to minimize search time by retrieving at most 1 hour's worth of files.
;               
; Use       : 
;    
; Inputs    :   times 	scalar (one day) or 2-elem array (start, end) or n-elem array of any anytim2utc format
;   	    	wave	AIA wavelength
;   	    	cadence time between images (minutes); one to match exactly times; zero for ALL between start and end

; Optional Inputs: 
;               
; Outputs   : prints 2 values

; Optional Outputs: 
;
; Keywords  :   
;   	/HOURS	cadence is units of hours
;   	DIRECTORY=  directory to search for all files in
;
; Category    : directory system archive query database files
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL/I2, Feb 09
;-               
; $Log: nrlgetaia.pro,v $
; Revision 1.5  2011/11/21 17:31:32  nathan
; fix input of 1 day
;
; Revision 1.4  2011/07/06 15:22:35  cooper
; changed some varaibles to long type to avoid overflow
;
; Revision 1.3  2011/06/29 16:34:17  nathan
; should work now
;
; Revision 1.2  2011/04/22 22:34:36  nathan
; count ti as seconds, test prevti
;
; Revision 1.1  2011/04/22 21:31:37  nathan
; first attempt
;


utc=anytim2utc(times)
IF n_elements(times) LT 2 THEN BEGIN
    utc=[utc,utc]
    utc[1].mjd=utc[0].mjd+1
ENDIF
strtimes=utc2yymmdd(utc,/yyyy,/hhmmss)
IF keyword_set(HOURS) THEN cadence=cadence*60
aiad=getenv_slash('aia')
inp=''
IF wave EQ 0 THEN read,'Please enter AIA wavelength(Ang): ',inp
IF inp NE '' THEN wave=fix(inp)
wav =string(wave,'(i4.4)')
nd=utc[1].mjd-utc[0].mjd+1
ti=utc[0].time/1000
utn=utc[0]
list=''
utd=utn
utd.time=0

if cadence GT 0 then begin 
    minpday=86400./60
    time1=utc2yymmdd(tai2utc( findgen( minpday/cadence+1)*60  +utc2tai(utd) ),/yyyy,/hhmmss)
    ; just interested in time
    catimes=double(strmid(time1,9,6))
    ;catimes= hhmmss
endif
hhpos=-1

FOR i=utc[0].mjd,utc[1].mjd DO BEGIN
    utn.mjd=i
    IF i EQ utc[1].mjd THEN lastti =hms2sec(long(strmid(strtimes[1],9,6))) ELSE lastti =long(86400)
    IF i EQ utc[0].mjd THEN firstti=hms2sec(long(strmid(strtimes[0],9,6))) ELSE firstti=long(0)
    ymd=utc2yymmdd(utn,/yyyy)
    IF keyword_set(DIRECTORY) THEN dir=directory ELSE BEGIN
	help,aiad
	IF strlen(aiad) LT 2 THEN read,'Please enter $aia; will search for files in $aia/YYYY/MM/DD/WAVE/: ',aiad
	yr=strmid(ymd,0,4)
	mo=strmid(ymd,4,2)
	da=strmid(ymd,6,2)
	dir=filepath(wav,root=aiad,subdir=[yr,mo,da])
    ENDELSE
    ; read in all files of wave in directory 
    spawn,'ls '+concat_dir(dir,'*'+trim(wav)+'_*'),fid,/sh
    ; probably going to get bad files with *wav_*, oh well.
    namesubs=strsplit(fid[0],'_') ; gives locations of subparts
    ; find _12345678_ and assume it is date followed by time
    wu=where(namesubs-shift(namesubs,1) eq 9,nwu)
    IF nwu EQ 1 THEN hhpos=namesubs[wu[0]] 
    IF hhpos LT 0 THEN BEGIN
    	message,'unrecognized filename '+fid[0],/info
	print,'set hhpos= position of first char of HHMMSS (from 0) in'
	print,fid[0]
	print,'Then .cont'
	stop
    ENDIF
    fitimes=hms2sec(long(strmid(fid,hhpos,6)))
    ; fitimes= seconds

    ti=firstti+cadence*60
    WHILE ti LE lastti DO BEGIN
    	inx=find_closest(ti,fitimes,/less)
	IF cadence LE 0 THEN BEGIN
	    inx2=find_closest(lastti,fitimes,/less)
	    list=[list,fid[inx:inx2]]
	    ti=lastti+1
	ENDIF ELSE $
    	    list=[list,fid[inx]]
    	ti=ti+cadence*60
    ENDWHILE

ENDFOR

getaiaresult=list[1:*]
help,getaiaresult
return,getaiaresult

end
