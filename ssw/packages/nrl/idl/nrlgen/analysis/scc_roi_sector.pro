function scc_ROI_SECTOR,hdr,r1,r2,t1_0,t2_0,PIXEL=pixel,DRAW=draw,ADJUST=adjust
;+
; $Id: scc_roi_sector.pro,v 1.6 2012/03/02 23:27:43 nathan Exp $
;
; NAME:
;	ROI_SECTOR
;
; PURPOSE:
;	This function returns the 1D list of indices contained within
;	the specified sector
;
; CATEGORY:
;	CME
;
; CALLING SEQUENCE:
;	Result = scc_ROI_SECTOR (Hdr,R1,R2,T1,T2)
;
; INPUTS:
;	Hdr:	Image header (LASCO header structure)
;	R1:	Inner radius of the sector (in solar radii), from sun center
;	R2:	Outer radius of the sector (in solar radii), from sun center
;	T1:	Left hand boundary of the sector (in degrees CCW from solar north)
;	T2:	Right hand boundary of the sector (in degrees CCW from solar north)
;
; KEYWORD PARAMETERS:
;	PIXEL:	If set, the inner and outer radii are specified in pixels
;	DRAW:	If set, the perimeter of the sector is drawn on the image
;       ADJUST: If set and also draw must be set, devide the x by adjust[0]
;               and y by adjust[1].
;
; OUTPUTS:
;	This function returns the indices of the pixels contained within
;	the sector as an array of long integers.
;
; COMMON BLOCKS:
;	None
;
; SIDE EFFECTS:
;	None
;
; RESTRICTIONS:
;
; PROCEDURE:
;	A sector is defined as the region bounded by two radial lines 
;	(from sun center) at two position angles and the annulus (centered 
;	on the sun) at two heights.
;
;	The pixel coordinates of the inner and outer boundaries of the 
;	annulus are computed at one degree increments from one position
;	angle to the next.  Correction is made if the sector spans the
;	North pole (where the position angle is 0/360).
;
;	POLYFILLV is used to fill in the indices from the definition of
;	the boundary.
;
;	The image header is used in determining the sun center and the 
;	number of pixels per radius.
;
; CALLED ROUTINES:
;	get_SUN_CENTER, POLYFILLV
;
; EXAMPLE:
;	To obtain the indices of a sector from 5 to 10 solar radii and
;	position angles 270 to 280.  Note that the left hand boundary
;	as viewed from the sun is 280 and the right hand boundary is 270.
;	The outline of the sector is drawn on the display
;
;	Result = ROI_SECTOR(hdr,5,10,280,270,/draw)
;
; MODIFICATION HISTORY:
; $Log: scc_roi_sector.pro,v $
; Revision 1.6  2012/03/02 23:27:43  nathan
; do not correct for roll if AZP
;
; Revision 1.5  2012/03/02 18:59:29  nathan
; clarify angle defn; call get_sun_center(/silent)
;
; Revision 1.4  2010/12/20 22:55:56  nathan
; allow for hdr.rsun to be undefined
;
; Revision 1.3  2010/12/15 20:05:56  nathan
; Add keyword ADJUST= to match roi_sector_ed.pro; correct ROI returned for
; roll angle in header (CROTA1).
;
;       Modified :      RCC, NRL, 10/31/08, adapted of SECCHI
; 	Written by:	RA Howard, NRL, 2 December 1997
;
;	@(#)roi_sector.pro	1.1 03/14/98 LASCO IDL LIBRARY
;-

COMMON cme_massimg,base,hbase,calbase,bname, debug

sun_cen = get_sun_center(hdr, ROLL=sroll, /deg, /silent)
IF (KEYWORD_SET(pixel)) THEN pixel_per_radius = 1. $
ELSE BEGIN
    IF tag_exist(hdr,'RSUN') THEN rsun=hdr.rsun ELSE rsun=0.
    IF rsun LE 0 THEN rsun=get_solar_radius(hdr)
    pixel_per_radius = rsun/hdr.cdelt1
ENDELSE
IF hdr.cunit1 EQ 'deg' THEN pixel_per_radius = pixel_per_radius/3600.
IF hdr.ctype1 EQ 'HPLN-AZP' THEN type='azp' ELSE type='tan'
IF type NE 'azp' THEN BEGIN
    ;
    ; Correct for roll of image
    ;
    t1=t1_0-sroll
    t2=t2_0-sroll
    IF abs(sroll) GT 1 THEN message,'Sector boundaries corrected by '+trim(sroll)+' degrees.',/info
ENDIF ELSE BEGIN
    t1=t1_0
    t2=t2_0
ENDELSE
;
;  Check if CME is over the North Pole
;
IF (t1 LT t2) THEN BEGIN
   th1 = t1+360
   th2 = t2
ENDIF ELSE BEGIN
   th1 = t1
   th2 = t2
ENDELSE
;
;  Compute sines and cosines of beginning and 
;  ending sector boundaries
;
c1 = COS(th1/!radeg)
c2 = COS(th2/!radeg)
s1 = SIN(th1/!radeg)
s2 = SIN(th2/!radeg)
;
;  At 1 degree increments, compute the outer boundary pixel coordinates
;
x = [r1*s1]
y = [r1*c1]
FOR i=th1,th2,-1 DO BEGIN
    ith = i
    x = [x,r2*sin(ith/!radeg)]
    y = [y,r2*cos(ith/!radeg)]
ENDFOR

;
;  If the ending angle doesn't include the final angle,
;  then add it
;
IF (ith NE th2)  THEN BEGIN
   x = [x,r2*sin(ith/!radeg)]
   y = [y,r2*cos(ith/!radeg)]
ENDIF
;
;  At 1 degree increments, compute the inner boundary pixel coordinates
;
len = n_elements(x)
FOR i=th2,th1 DO BEGIN
    ith = i
    x = [x,r1*sin(ith/!radeg)]
    y = [y,r1*cos(ith/!radeg)]
ENDFOR
;
;  If the ending angle doesn't include the final angle,
;  then add it to the inner radius 
;
IF (ith NE th2)  THEN BEGIN
   x = [x,r1*sin(ith/!radeg)]
   y = [y,r1*cos(ith/!radeg)]
ENDIF
;
;  Add the beginning point to the array, 
;
x = [x,r1*s1]
y = [y,r1*c1]

IF hdr.detector EQ 'HI1' THEN BEGIN
m1 = (y[1]-y[0])/(x[1]-x[0])
m2 = (y[len-1]-y[len])/(x[len-1]-x[len])

  n = n_elements(x)
  xx = [x[0],x[len:*]]
  m = max(abs(xx),mx)
  x[0] = xx[mx]
  x[len:*] = xx[mx]

  y[0] = m1*x[0] 
  y[n-1] = m1*x[n-1]
  y[len] = m2*x[len]

ENDIF
;
;Then adjust for the sun center and plate scale (solar radius)
;
x = sun_cen.xcen - x*pixel_per_radius
y = sun_cen.ycen + y*pixel_per_radius

;
;  Display the outline of the ROI, and compute the indices
;  bounded by the outline
;

IF (KEYWORD_SET(draw))  THEN BEGIN
  IF (KEYWORD_SET(adjust)) THEN           $
    PLOTS,x/adjust(0),y/adjust(1),/device $
  ELSE                                    $
    PLOTS,x,y,/device
ENDIF

RETURN,POLYFILLV(x,y,hdr.naxis1,hdr.naxis2)
END
