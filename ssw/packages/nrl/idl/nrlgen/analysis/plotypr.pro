pro plotypr, times, ypr, ARCSEC=arcsec, DEGREES=degrees, YPR2=ypr2, _Extra=_extra, ZM=zm, WID=wid
;+
; $Id: plotypr.pro,v 1.4 2018/11/08 17:34:00 nathan Exp $
;
; Project : STEREO SECCHI
;
; Name    : plotypr
;
; Purpose : Generate 3 plots of Yaw, Pitch and Roll in 1 window 
;
; Use     : IDL> 
;
; Input:    times	Array(n) of times acceptable to utplot.pro
;	    ypr		3xn array of yaw (arcsec), pitch(arcsec), roll(degrees)
;
; Output:   None
;
; Keyword Input/Output:
;	/ARCSEC	    Convert roll to arcsec before plotting
;   	/DEGREES    All input values are in degrees
;	/ZM	    Restrict plot to +/- stdev
;   	WID=	    IDL Window to use
;   	YPR2	    ypr array to overplot primary ypr input
;	Any keywords accepted by UTPLOT
;
; Common  :
;
; Restrictions: 
;
; Side effects: generates 3 plots in 1 window
;
; Category   : attitude, analysis, calibration, pointing
;
; Prev. Hist.:

; Written    : N.Rich, NRL/I2, 6/5/07
;
; $Log: plotypr.pro,v $
; Revision 1.4  2018/11/08 17:34:00  nathan
; add /DEGREES
;
; Revision 1.3  2010/12/09 22:37:11  nathan
; change color
;
; Revision 1.2  2008/02/20 17:06:09  nathan
; added ypr2 keyword for overplot
;
; Revision 1.1  2007/06/20 21:12:19  nathan
; draft
;
;-

yaw=ypr[0,*]
pitch=ypr[1,*]
roll=ypr[2,*]
ytimes=times
ptimes=times
rtimes=times

IF keyword_set(ARCSEC) THEN BEGIN
	roll=roll*3600.
	rollunit='Arcsec'
ENDIF ELSE BEGIN
	rollunit='Degrees'
ENDELSE

IF keyword_set(ZM) THEN BEGIN
	sigy=stdev(yaw,mny)*5
	sigp=stdev(pitch,mnp)*5
	sigr=stdev(roll,mnr)*2
	wy=where(yaw LE mny+sigy and yaw GE mny-sigy)
	wp=where(pitch LE mnp+sigp and pitch GE mnp-sigp)
	wr=where(roll LE mnr+sigr and roll GE mnr-sigr)
	yaw=yaw[wy]
	pitch=pitch[wp]
	roll=roll[wr]
	ytimes=times[wy]
	ptimes=times[wp]
	rtimes=times[wr]
ENDIF

miny=min(yaw,max=maxy)
minp=min(pitch,max=maxp)
n=n_elements(times)

IF keyword_set(YPR2) THEN BEGIN
    miny=(min(ypr2[0,*]))<miny
    minp=(min(ypr2[1,*]))<minp
    maxy=(max(ypr2[0,*]))>maxy
    maxp=(max(ypr2[1,*]))>maxp
ENDIF
IF maxy-miny GT 7200 THEN BEGIN
	yaw=yaw/3600.
	IF keyword_set(YPR2) THEN ypr2[0,*]=ypr2[0,*]/3600.
	yawu='Degrees'
ENDIF ELSE BEGIN
	yawu='Arcsec'
ENDELSE
IF keyword_set(DEGREES) THEN yawu='Degrees'
IF maxp-minp GT 7200 THEN BEGIN
	pitch=pitch/3600.
	IF keyword_set(YPR2) THEN ypr2[1,*]=ypr2[1,*]/3600.
	pitchu='Degrees'
ENDIF ELSE BEGIN
	pitchu='Arcsec'
ENDELSE
IF keyword_set(DEGREES) THEN pitchu='Degrees'

device,decomposed=0,retain=2
!y.style=3
!x.style=1
!p.color=2
!p.background=255
!p.multi=[0,0,3,0,0]
IF keyword_set(WID) THEN wind=wid ELSE wind=0
IF keyword_set(PSYM) THEN psym2=psym+1 ELSE psym2=4
window,wind,xsiz=512,ysize=1024
utplot,ytimes,yaw,title='Yaw',ytitle=yawu, yrange=[miny,maxy],charsize=2,timerang=[times[0],times[n-1]],_extra=_extra 
IF keyword_set(YPR2) THEN outplot,ytimes,ypr2[0,*],psym=psym2
utplot,ptimes,pitch,title='Pitch',ytitle=pitchu,yrange=[minp,maxp],charsize=2,timerang=[times[0],times[n-1]],_extra=_extra
IF keyword_set(YPR2) THEN outplot,ytimes,ypr2[1,*],psym=psym2
utplot,rtimes,roll,title='Roll',ytitle=rollunit,charsize=2,timerang=[times[0],times[n-1]],_extra=_extra
IF keyword_set(YPR2) THEN outplot,ytimes,ypr2[2,*],psym=psym2
!p.multi=0

end
