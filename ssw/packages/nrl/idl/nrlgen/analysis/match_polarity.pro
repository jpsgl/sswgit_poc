;+
; $Id: match_polarity.pro,v 1.4 2009/03/19 15:26:50 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : match_polarity
;               
; Purpose   : Given a single FITS file, find the other 2 files
;             containing the rest of the polarizations.
;               
; Explanation: Method used to find the other files is the
;            same for both the database and file methods.
;            The matching criteria is:
;   Files have the same 'obs_id'
;   Files have 'downlink' = 4
;   Files are for the same 'obsrvtory' (A or B)
;   Files are within 3 minutes of each other.
;   Files as a set use the 'imgseq' values of 0, 1, 2
;               
; Use       : IDL> flist=match_polarity(fname,/local)
;    
; Inputs    : fname = FITS file name, for which corresponding
;                     polarity files will be found.
;               
; Outputs   : returns a string(3) array listing all 3 polarization components,
;             in imgseq order 0,1,2.
;
; Keywords  : /local = (optional) do not use database, but try
;                     to find other polarity files by checking
;                     all FITS (*.fts) files in the same directory
;                     as the given FITS file name.
;
;             /keeppath = (optional) if using /local, includes full
;                     file path with returned names
;
;             /debug = (optional) display copious output on progress.
;               
; Calls from LASCO : none 
;
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : A,B
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRL, Jan 2007
;               
; Notes       : contains test case 'test_match_polarity'.
;
;-            


; routine to look in file directory for other polarity components
PRO match_polarity_file,fname,obs_id,imgseq,obsrvtry,results

   fdir=file_break(fname,/path); inverse of concat_dir

   sntype=strmid(fname,8,1,/reverse); 's' or 'n'

   if (sntype eq 'n') then $
     filelist=file_search(fdir,"*n4*.fts",count=icount) else $
     filelist=file_search(fdir,"*s4*.fts",count=icount)

;   print,'found ',n_elements(filelist)

   iele=(where(filelist eq fname))[0]; return lone match
;   print,'starting at ',iele
   loc=lonarr(3); tracks indices of found files

   for ipolar=0,2 do begin
;       print,'checking ',ipolar,imgseq
       if (imgseq eq ipolar and ipolar ne 0) then begin
           ; we already know our original file
           results[ipolar] = file_break(fname,/name)
;           print,ipolar,'input'
       end else begin

           if (ipolar lt imgseq) then begin
               ; search backwards
               floop0=iele[0]
               floop1=0
               fincr=-1
           end else begin
               ; search forwards
               floop0=iele[0]
               floop1=n_elements(filelist)-1
               fincr=1
           end
;           print,'looping ',floop0,floop1,fincr
           for floop=floop0,floop1,fincr do begin
               fn=filelist[floop]
               ignore=sccreadfits(fn,hdr,/nodata)
               ; little fix here for when image sequence not set, e.g. 'n'
;               imgseq=hdr.imgseq
               checkimgseq=floor(hdr.polar/120)
;               print,'polar is ',checkimgseq,ipolar
;               print,'obsrvtry is ',hdr.obsrvtry,obsrvtry
;               print,'obs id is ',hdr.obs_id,obs_id

; have to remove the obs_id check, no longer applies for the
; way we take Cor2 images.
;               if (hdr.obs_id eq obs_id and checkimgseq eq ipolar and $
;                   hdr.obsrvtry eq obsrvtry) then begin
               if (checkimgseq eq ipolar and $
                   hdr.obsrvtry eq obsrvtry) then begin
                   results[ipolar]=file_break(fn,/name)
;                   print,ipolar,'found ',fn,'(',results,')'
                   break        ; found it, stop searching
               end
           end

       end

   end

END

; uses sqdb to look up entry unless /local flag is given.
; /local tells it to just look in the directory where the
; given fname exists.
; If given /local and /keeppath, includes the directory in the return filenames

FUNCTION match_polarity,fname,local=local,debug=debug,keeppath=keeppath

   local=setyesno(local)
   debug=setyesno(debug)
   keeppath=setyesno(keeppath)

   if (file_exist(fname)) then begin
       ; fetch info from file itself

       ignore=sccreadfits(fname,hdr,/nodata)
       obs_id=hdr.obs_id        ; same for all relevant files
       date_obs=hdr.date_obs    ; reference date, for DB searches
       datedb=repchar(date_obs,'T',' ') ; format to DB standard
;       imgseq=hdr.imgseq        ; 0, 1 or 2
       imgseq=floor(hdr.polar/120)        ; 0, 1 or 2
       obsrvtry=hdr.obsrvtry
       if (obsrvtry eq 'STEREO_A') then AorB=1 else AorB=2

    ;print,'observatory: ',hdr.obsrvtry
   end else if (local) then begin
       if (debug) then print,"File '"+fname+"' does not exist, exiting"
       return,-1
   end else begin
       ; look up filename in database using sqdb
       fnq=file_break(fname,/name) ; inverse of concat_dir

       query = 'select obs_id,date_obs,imgseq,polar,obsrvtry,filename'
       query = query + ' from img_seb_hdr'
;       query = query + ' where filename LIKE "%' + fnq + '"'
       query = query + ' where filename = "' + fnq + '"'
       query = query + ' limit 1'

;works: q='select filename from img_seb_hdr where filename LIKE "%20061130_135450_n4euB.fts%" limit 10'
       if (debug) then print,query
       myset=sqdb('secchi_flight',query,quiet=1)
       if (DATATYPE(myset) eq 'STR') then begin
           print,'Error finding file information in db, exiting.'
           return,0
       end else begin
           obs_id=myset.obs_id
           datedb=myset.date_obs
;           imgseq=myset.imgseq
           imgseq=floor(myset.polar/120)
           obsrvtry=myset.obsrvtry
           AorB=myset.obsrvtry
           if (AorB eq 1) then obsrvtry='STEREO_A' else obsrvtry='STEREO_B'
       end

   end

   if (debug) then print,'debug: date_obs: ',datedb,', imgseq: ',imgseq
;   detector=hdr.detector; detector, for DB, but hdr is STR and db is INT
;   seb_prog=hdr.seb_prog
;   polar=fltarr(3)
;   polar[imgseq]=hdr.polar

   results=strarr(3)

   filedir=1

   if (local) then begin
       ; hunt around in directory for files
       match_polarity_file,fname,obs_id,imgseq,obsrvtry,results
;       print,'Results: ',results
   end else begin
       ; use sqdb
 
       datemid= tai2utc(utc2tai(str2utc(datedb)),/ecs)
       dateEarly= tai2utc(utc2tai(str2utc(datedb))-180D,/ecs)
       dateLate= tai2utc(utc2tai(str2utc(datedb))+180D,/ecs)

       for i=0,2 do begin

           if (i lt imgseq) then begin
               date1=dateEarly
               date2=datemid
               sortby = 'desc'
           end else begin
               date1=datemid
               date2=dateLate
               sortby = 'asc'
           end

           polarquads=['0','48','96']; correspond to polar=0,120,240

           query = "select filename,date_obs from img_seb_hdr"
           query = query +' where date_obs between "'+date1+'" and "'+date2+'"' ;
;           query = query + " and obs_id = " + strtrim(string(obs_id),2)
           query = query + " and downlink = 4" ; ignore space weather
;           query = query + " and imgseq = " + strtrim(string(i),2)
           query = query + " and polar_quad = " + polarquads[i]
;was strtrim(string(i*120),2)
           query = query + " and obsrvtry = " + strtrim(string(AorB),2)
           query = query + " order by date_obs " + sortby + " limit 1"

           if (i eq imgseq) then begin
               ; we already know this
               myset=file_break(fname,/name)
           end else begin
               ; query the database for nearby relevant file
               myset=sqdb('secchi_flight',query,quiet=1)
           end

           if (debug) then print,query

           if (debug) then print,n_elements(myset),DATATYPE(myset)
           if (debug) then help,myset,/str
           if (DATATYPE(myset) eq 'STR') then begin
               results[i]=myset
           end else begin
               results[i]=(myset[0]).filename
           end

       end

   end

   if (debug) then print,'imgseq was ',imgseq
   if (debug) then for i=0,n_elements(results)-1 do print,i,') ',results[i]

   path=file_dirname(fname,/mark_directory)
   if (keeppath) then for i=0,n_elements(results)-1 do $
     results[i]= path + results[i]

   return,results

END


pro test_match_polarity,debug=debug

  debug=setyesno(debug)

  file='/net/venus/secchi2/lz/cor2/L0/a/seq/20061214/20061214_151013_s4c2A.fts'

  print,' '
  print,'  ***  Testing file-based lookup ***'
  results=match_polarity(file,/local,debug=debug)
  print,results
  print,' '

  print,' '
  print,'  ***  Testing file+DB lookup ***'
  results=match_polarity(file,debug=debug)
  print,results
  print,' '

  print,' '
  print,'  ***  Testing pure DB lookup ***'
  fname=file_break(file,/name)
  results=match_polarity(fname,debug=debug)
  print,results
  print,' '

end

pro testhdr

fdir='/net/venus/secchi2/lz/cor2/L0/a/seq/20070112';

fnames=[$
         '20070112_000240_s4c2A.fts',$
         '20070112_000240_s7c2A.fts',$
         '20070112_000252_s4c2A.fts',$
         '20070112_000252_s7c2A.fts',$
         '20070112_000304_s4c2A.fts',$
         '20070112_000304_s7c2A.fts',$
         '20070112_010240_s4c2A.fts',$
         '20070112_010240_s7c2A.fts',$
         '20070112_010252_s4c2A.fts']

fc=n_elements(fnames)
hdrptr=ptrarr(fc)

for i=0,fc-1 do begin
    fn=fdir+'/'+fnames[i]
    ignore=sccreadfits(fn,hdr,/nodata)
    hdrptr[i]=ptr_new(hdr)
end

for i=0,fc-1 do begin
    hdr=*hdrptr[i]
    print,i+1,') ',fnames[i]
    print,'obssetid: ',hdr.obssetid
    print,'obs_id: ',hdr.obs_id
    print,'seb_prog: ',hdr.seb_prog
    print,'imgseq: ',hdr.imgseq
    print,'imgctr: ',hdr.imgctr
    print,'polar: ',hdr.polar
    print,'date_obs: ',hdr.date_obs
    print,'filename: ',hdr.filename
    print,'observatory: ',hdr.obsrvtry
end

end
