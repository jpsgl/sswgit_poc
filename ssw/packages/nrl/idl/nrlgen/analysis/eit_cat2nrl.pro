;+
;
; Project   : LASCO
;                   
; Name      : eit_cat2nrl
;               
; Purpose   : Converts SSWDB EIT catalog_lz entries to map to the NRL
;             file naming and directory structure so routines like 'EIT_PREP'
;             and Festival can be used with EIT data at NRL.
;               
; Explanation: This simply translates a catalog line into a directory
;              location, checking to see that a file for the desired
;              time exists.  It first checks the LZ data and, if no
;              directory for the given date exists, tries the QZ data.
;               
; Use       : IDL> eit_filename == eit_cat2nrl(singleline_from_catalog_lz)
;    
; Inputs    : a string in the format provided by eit_catrd
;               
; Outputs   : a filename with full path on the NRL disks, or -1 if no
;             file exists.
;
; Keywords  : (none)
;
; Calls from LASCO : none 
;
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: None, but requires the environment variables LZ_IMG
;               and QL_IMG be set and pointing to LASCO data
;               
; Category    : data anal
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRL, April 2008


; function that, given an EIT lz catalog string, finds the expected
; FITS file.
; catalog format is space-separated:
;  yyyy-Mmm-dd hh:mm:ss exptime ? wave::filter etc...
;
; Uses brute force 'hunt through files'
;

; test case:
;   f=eit_catrd('2007-01-01')
;   print,eit_cat2nrl(f[0])

FUNCTION eit_cat2nrl,catline

  items=strsplit(catline,' ',/extract)
  date=items[0]
  month=int2str((month_id(strmid(date,5,3)))[0],2)
  yymmdd=strmid(date,2,2) + month + strmid(date,9,2)
  t1=items[1]
  exptime=items[2]
  set2=strsplit(items[4],'::',/extract)
  wavelength=set2[0]
  filter=set2[1]

  ; now, using time, find the file

  lzloc=getenv('LZ_IMG') + '/level_05/' + yymmdd + '/c4/
  qlloc=getenv('QL_IMG') + '/level_05/' + yymmdd + '/c4/


  if (file_exist(lzloc)) then mydir=lzloc else $
    if (file_exist(qlloc)) then mydir=qlloc

  fn=-1

  if (n_elements(mydir) ne 0) then begin

      filelist=file_search(mydir,"*.fts",count=icount)

      for iloop=0,icount-1 do begin
          fn=filelist[iloop]
;          ignore=sccreadfits(fn,hdr,/nodata)
          hdr=lasco_fitshdr2struct(headfits(fn))
          t2=strmid(hdr.time_obs,0,8)
          if (t2 eq t1) then break
      end

  end

  return,fn

END
