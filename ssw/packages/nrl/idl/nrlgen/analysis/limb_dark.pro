FUNCTION LIMB_DARK,Lambda,MEANFLUX=meanflux,ALLEN=allen
;+
; $Id: limb_dark.pro,v 1.1 2008/02/07 22:22:57 nathan Exp $
; NAME:
;	LIMB_DARK
;
; PURPOSE:
;	This function returns the limb darkening coefficient given the wavelength
;
; CATEGORY:
;	DATA ANALYSIS
;
; CALLING SEQUENCE:
;	Result = LIMB_DARK (Lambda)
;
; INPUTS:
;	Lambda:	Wavelength in nanometers
;
; KEYWORDS
;	MEANFLUX	If present, the limb darkening is the ratio of the mean flux
;			to the intensity at the center of the disk.  The default is
;			the ratio of the intensity at the limb to the intensity at
;			the center.
;	ALLEN		If present the values from Allen "Astrophysical Quantities"
;			are returned rather than from Pierce and Allen.
;
; OUTPUTS:
;	This function returns the limb darkening for the input wavelength(s).
;
; RESTRICTIONS:
;	The range of wavelengths can be between 305 nm and 995 nm.  If any
;	input values are outside of this range, the program will exit without
;	computing anything and will return a value of -1.
;
; PROCEDURE:
;	The limb darkening values derived from Table 2 of Pierce and Allen,
;	"The Solar Spectrum Between 0.3 and 10 micron", pp169-192, in "The
;	Solar Output and its Variation" O.R. White, ed, Colorado University
;	Press, Boulder, 1977.
;
;	Column 6 of Table 2 gives a limb darkening function as the ratio of the
;	average solar flux to the intensity at disk center.  We want the ratio
;   	of the intensity at the limb to the intensity at the center of the disk
;   	Allen, "Astrophysical Quantities" p170 relates the limb darkening
;   	coefficient, u, to the ratio of the total flux to the intensity at
;   	disk center as approximately:
;
;		u = 3*(1-[F/I((0)])
;
;   	where, F/I(0) is the mean to center ratio given in Pierce and Allen.
;
;	Calls the IDL routine INTERPOL to perform the interpolations.
;
; EXAMPLE:
;	To obtain the limb darkening at 730 nm (7300 Angstrom):
;
;		u = LIMB_DARK(730.)
;
; MODIFICATION HISTORY:
; 	Written by:	R.A. Howard, NRL, September, 1999
; $Log: limb_dark.pro,v $
; Revision 1.1  2008/02/07 22:22:57  nathan
; moved from lasco/data_anal
;
; @(#)limb_dark.pro	1.4 02/07/08 :LASCO IDL LIBRARY
;
;-
;
IF KEYWORD_SET(ALLEN) THEN BEGIN
   wl = 10.*[30,32,35,37,38,40,45,50,55,60,80,100]
   meantocenter = [648,685,705,710,710,718,755,782,803,817,862,886]/1000.
ENDIF ELSE BEGIN
   wl = 305.+10*INDGEN(70)
   u300 = [.636,.657,.674,.685,.694,.703,.691,.680,.687,.698]
   u400 = [.710,.718,.726,.734,.744,.753,.759,.765,.769,.775]
   u500 = [.779,.784,.788,.793,.798,.802,.805,.809,.812,.815]
   u600 = [.817,.820,.823,.825,.828,.830,.834,.836,.838,.841]
   u700 = [.843,.845,.847,.849,.851,.853,.855,.857,.859,.861]
   u800 = [.863,.864,.865,.867,.868,.869,.870,.872,.873,.874]
   u900 = [.875,.876,.877,.878,.879,.880,.881,.882,.883,.884]
   meantocenter = [u300,u400,u500,u600,u700,u800,u900]
ENDELSE
;PLOT,wl,meantocenter,/ynozero
ntab = N_ELEMENTS(meantocenter)
w = WHERE (lambda LT wl(0) OR lambda GT wl(ntab-1),nw)
IF (nw GT 0)   THEN BEGIN
   PRINT,'ERROR LIMB_DARK.  Input wavelength outside of table range',lambda(w)
   RETURN,-1
ENDIF
IF KEYWORD_SET(MEANFLUX) THEN u=INTERPOL(meantocenter,wl,lambda) $
                         ELSE u=3*(1.-INTERPOL(meantocenter,wl,lambda))
;PLOT,wl,u,/ynozero
RETURN,u
END

