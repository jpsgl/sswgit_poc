function fcorona,img,hdr,exclude=exclude,model=model,fcorona
;
;+
; NAME:
;	FCORONA
;
; PURPOSE:
;	This function returns a K-coronal image from the total B image by
;	subtracting off the F-coronal model, optionally returning the
;       F-corona model image as well.  Works for most SOHO and STEREO data.
;
; CATEGORY:
;	ANALYSIS
;
; CALLING SEQUENCE:
;	Result = FCORONA(Img,Hdr)
;
; INPUTS:
;	Img:	Array containing an image of total brightness.  It is the output
;    		from level 1 processing (for polarizer=clear)
;	Hdr:	FITS image header
;
;       exclude: Optional flag, if set excludes K-corona at R<Rsun
;
;       model: Optional model to set, defaults to 'kl' for Koutchmy-Lamy
;
;       fcorona: Optional return variable if user also wants fcorona returned
;
; OUTPUTS:
;	This routine returns an image of the K-brightness, and
;	optionally returns the fcorona as well if given the 'fcorona' parameter
;
; MODIFICATION HISTORY:
; 	Written by:	R.A. Howard, NRL, 6 Nov 1996
;       original was called 'bk' and remains in LASCO data_anal
;          but does not work (and may have never worked).
;       Modified and renamed to 'fcorona' by: S. Antunes, NRL, 5 Jan 2007
;          and moved into secchi 'nrlgen' tree.
;
;	@(#)bk.pro	1.1 10/04/96 LASCO IDL LIBRARY
;-
;

if (n_elements(model) eq 0) then model='kl'

naxis1 = fxpar(hdr,'NAXIS1')
naxis2 = fxpar(hdr,'NAXIS2')
coord=fltarr(4)
;coord(0) = fxpar (hdr,'SUNCOL'); column center of sun, CRPIX1
;coord(1) = fxpar (hdr,'SUNROW'); row center of sun, CRPIX2
;coord(2) = fxpar (hdr,'SOLROL'); roll angle of solar north, CROTA1, 2
;coord(3) = fxpar (hdr,'PIXRAD')

sat= strtrim(fxpar(hdr,'TELESCOP'),2)

;if (sat eq 'SOHO') then begin

;  detector= strtrim(fxpar(hdr,'DETECTOR'),2)
;  k0=t_param(detector,'CENTER')
;  coord(0)=k0(0)
;  coord(1)=k0(1)
;  coord(2) = fxpar (hdr,'CROTA1'); roll angle of solar north, CROTA1, 2

;  coord(3) = fxpar (hdr,'PLATESCL'); number of pixels per radius, e.g.
   ;  PLATESCL is arcsec/pixel and sun is 16 arcmin=960 arcsec radius
   ; so this coord(3) is  960.0/PLATESCL or around 100, so 1024 pixels
   ; is around 10 R_sun.
;  coord(3) = 960.0/coord(3)

; REMOVED above SOHO code, it failed for C2 512x512 images.
; Now everything uses the more reasonable generic method below.

;end else begin

  ; best guess, works for STEREO
  coord(0) = fxpar (hdr,'CRPIX1'); column center of sun, CRPIX1
  coord(1) = fxpar (hdr,'CRPIX2'); row center of sun, CRPIX2
  coord(2) = fxpar (hdr,'CROTA'); roll angle of solar north

  coord(3) = fxpar (hdr,'CDELT1'); number of pixels per radius, e.g.
   ;  CDELT1 is arcsec/pixel and sun is 16 arcmin=960 arcsec radius
   ; so this coord(3) is  960.0/CDELT1 or around 100, so 1024 pixels
   ; is around 10 R_sun.
  coord(3) = 960.0/coord(3)

;end

;print,coord
;sample coord =  [512, 512, 0, 960.0/11.4]

sundist,coord,dist,angle,xsize=naxis1,ysize=naxis2
; dist are in units of solar radius

case model of
  'kl':   fcorona=fcor_kl(dist,angle)  ; kl, also the default model
  else:  fcorona=fcor_kl(dist,angle)  ; kl, the default model
endcase

if (n_elements(exclude) ne 0) then begin
  ; exclude f-corona contribution below R<Rsun
   below=WHERE(dist lt 1.0)
  fcorona[below]=0.0; set corona to zero over region where BK is not defined
end

return,(img-fcorona)>0
end

PRO test_fcorona
  ; simple test of the above 'fcorona' ingest and results

  fname='/home/sandy/dev/sandyutil/data_old/s22003802.fts'; SOHO
  fname='/net/venus/secchi2/rt/cor2/L0/a/seq/20061204/20061204_141055_n2c2A.fts'; STEREO
  img=readfits(fname,hdr)
  img2=fcorona(img,hdr,f,/exclude)
  tv_many,[[[img]],[[img2]],[[f]]],/slice,/add,res=0.1,/local
  dimg=img-img2
;  print,min(dimg),max(dimg)
END

PRO testplot_fcorona
  ; Reproduces the plot from Koutchmy-Lamy paper.

  dist=make_array(/float,80,/index); array from R=0 to R=79
  dist=dist+1; run from 1 to 80
  equator=dist*0.0 + 0.0; array of same size, angle=0
  pole=dist*0.0 + 90.0; array of same size, angle=90
  kecliptic=fcor_kl(dist,equator)
  kpole=fcor_kl(dist,pole)
  plot,dist,kecliptic,xtitle='R/Rsun',ytitle='log B',/ylog,/xlog,$
    yrange=[5e-14,1e-6],ystyle=1,xrange=[0.9,600],xstyle=1,$
    title='fcor_kl reproduction of Koutchmy & Lamy Fig. 3'
  xyouts,dist[40],kecliptic[40],'ecliptic',/data

  oplot,kpole
  xyouts,dist[40],kpole[40],'pole',/data,alignment=1.0

END
