function SCC_CALC_CME_MASS,Img,Hdr,Box,FNAME=FNAME,CONT=CONT,POS=POS, $
                  ROI=ROI,ALL=ALL,ONLY_NE=ONLY_NE,MEDVAL=MEDVAL,  $
		MAXVAL=MAXVAL,PB=PB,CMELONLAT=CMELONLAT
;+
; NAME:
;	CALC_CME_MASS
;
; PURPOSE:
;	Computes the CME mass in an image given a box defining the area
;
; CATEGORY:
;	CME 
;
; CALLING SEQUENCE:
;	Result = SCC_CALC_CME_MASS (Img, Hdr, Box)
;
; INPUTS:
;	Img:	The 2-D difference image containing the CME.  The units are
;		in mean solar brightness units
;	Hdr:	The lasco header structure of the image
;	Box:	An array containing the coordinates of the region of interest
;
; KEYWORD PARAMETERS:
;	FNAME:	If present, this string defines the name of a file
;		to store the mass value in.  The information will be appended
;		to an existing file or will create a new file.  The default
;		is not to save the information.
;	CONT:	If set this parameter indicates that a continuing CME sequence
;		is being computed and various parameters will be not be 
;		computed. The default is to compute the parameters.
;	POS:	If present, this allows the angle from the plane of the sky to
;		be specified.  The default is to set the angle to 20 degrees.
;	ROI:	If present, then box contains the ROI indices rather than coordinates
;	ALL:	If present, then the entire image is processed
;	ONLY_NE:If present, electron density is returned, rather than mass
;	MAXVAL:	If present, the maximum value in the region is computed
;	MEDVAL:	If present, the median value in the region is computed
;	PB:	If present, the input image is a pB image
;
; OUTPUTS:
;	This function returns the mass contained within the ROI box in
;	grams.
;
; COMMON BLOCKS:
;	CME_MASS,Dist,Angle,B,Conv
;		Dist = Distance of pixel in solar radii from sun center
;		Angle = Angle of pixel in degrees from solar north
;		B = brightness array of one electron
;		Conv = Conversion factor from MSB to grams
;
;		This common block is used to store a previous computation
;		of the distance matrix to save time.
;
; SIDE EFFECTS:
;	None
;
; RESTRICTIONS:
;	The coordinates of the sun center must be in the header.
;
; PROCEDURE:
;	An array in which the elements are the distance of that pixel from
;	sun center is computed.  Then ELTHEORY is called to compute the
;	brightness and polarization properties of a single electron.
;
; MODIFICATION HISTORY:
; 	Written by:	R.A. Howard, NRL, 18 September 1996
;	RAH 22 Mar 1997, Added Keyword POS and corrected mass/e
;	RAH 16 May 1997, Changed header from FITS to header structure
;	RAH 19 Sep 1997, Added Keyword ONLY_NE, added function of date
;	RAH 18 Apr 1999, Put Ne to mass conversion into separate routine
;	RAH 28 Sep 1999, Put POS (Plane of sky) angle to 0 instead of 20
;	RAH 03 Oct 1999, Added capability for pB image
;
;
; @(#)calc_cme_mass.pro	1.9 10/03/99 :NRL Solar Physics
;
;-
COMMON CME_MASS,Dist,Angle,B,Conv
;coord = telescope_pointing(hdr)
sunc = scc_sun_center(hdr)
coord =[sunc.xcen,sunc.ycen,hdr.crota*!dtor,hdr.rsun/hdr.cdelt1]
;
;   determine of the distance and brightness per electon information needs to be
;   computed
;
IF KEYWORD_SET (CONT) THEN BEGIN
   sz = SIZE (DIST) ;from common block
   IF (sz(0) NE 0) THEN compute=0 ELSE compute=1
ENDIF ELSE compute=1
IF (compute eq 1) THEN BEGIN
   ;   
   ;  convert header structure to wcs structure   
   ;
   wcs = fitshead2wcs(hdr)  
   ;
   ;   create a distance array
   ;
   dist=wcs_get_coord(wcs)
   IF hdr.cunit1 eq 'deg' then dist = dist*3600.
   dist =reform(sqrt(dist[0,*,*]^2 + dist[1,*,*]^2))/hdr.rsun
   ;
   ;  compute the brightness per electron, assume 0 degrees 
   ;  behind (in front of) the limb if not specified
   ;
   IF KEYWORD_SET(POS) THEN pos_angle=pos ELSE pos_angle=0
   IF KEYWORD_SET(CMELONLAT) THEN BEGIN
     cmelon=cmelonlat(0);sgui.lon*!radeg
     wcs=  fitshead2wcs(hdr)
     if cmelon gt wcs.position.crln_obs then $; 
       if cmelon-wcs.position.crln_obs gt 180 then $;
         plon=cmelon-wcs.position.crln_obs-270 else $ 	;east limb
           plon=(-1*(cmelon-wcs.position.crln_obs))+90     ;west limb

     if cmelon lt wcs.position.crln_obs then $ 
       if (360-wcs.position.crln_obs)+cmelon gt 180 then $;
          plon=((360-wcs.position.crln_obs)+cmelon)-270 else $ 	;east limb
               plon=-1*((360-wcs.position.crln_obs)+cmelon)+90     ;west limb

     cmelat=cmelonlat(1)
     pos_angle=asin((sin(plon/!radeg))/(tan(cmelat/!radeg)^2+1))*!radeg
   ENDIF

   ELTHEORY,dist,float(pos_angle),r,b,bt,br,pol
   wb = WHERE (b EQ 0,nb)
   IF (nb GT 0) then b(wb) = 1
ENDIF
;
;  Now compute various conversion factors
;
;  Get the size of a pixel in the sky
;  The solar radius is 6.96e10 cm 
;  The angular radius is nominally 0.2666 degrees (at 1AU).
;  It should be nominally 1% bigger at L1
;  The apparent radius should be computed for the day of year.
;
;d = hdr.date_obs
;yymmdd = STRMID(d,0,4)+STRMID(d,5,2)+STRMID(d,8,2)
;tel = hdr.telescop
;IF (tel EQ 'SOHO')  THEN SOLAR_EPHEM,yymmdd,radius=solar_radius,/soho $
;                    ELSE SOLAR_EPHEM,yymmdd,radius=solar_radius
;solar_radius = solar_radius*3600.		; in arc seconds
solar_radius = hdr.rsun
cm_per_arcsec = 6.96e10/solar_radius
;cm2_per_pixel = (cm_per_arcsec*SUBTENSE(hdr.detector))^2
IF hdr.cunit1 EQ 'deg' THEN cm2_per_pixel= (cm_per_arcsec*hdr.cdelt1*3600.)^2 $ 
 ELSE cm2_per_pixel = (cm_per_arcsec*hdr.cdelt1)^2 
;
;  Should electron density or mass be computed?
;
IF (KEYWORD_SET(ONLY_NE))  $
   THEN conv = cm2_per_pixel $
   ELSE conv = NE2MASS(1.) * cm2_per_pixel
;
;  If pB image then divide by Bt-Br per electron else by Btotal per electron
;  Compute the mass (or electron density)
;
IF (KEYWORD_SET(PB))  THEN mass=img/(bt-br) ELSE mass=img/b
mass = conv *  mass 		; mass array (in grams)
;
;   get total mass in the region of interest
;
IF (KEYWORD_SET(ROI))  THEN BEGIN
   mass = mass(box)
ENDIF ELSE IF (NOT KEYWORD_SET(ALL)) THEN BEGIN
   p1col = box(0,0)
   p1row = box(0,1)
   p2col = box(1,0)-1
   p2row = box(1,1)-1
   mass = mass(p1col:p2col,p1row:p2row)
ENDIF
totmass = TOTAL(mass)
IF (KEYWORD_SET(MEDVAL))  THEN medval=MEDIAN(mass)
IF (KEYWORD_SET(MAXVAL))  THEN maxval=MAX(mass)
;
;  write to a file if desired
;
IF KEYWORD_SET (FNAME) THEN BEGIN
   OPENU,lu,fname,/get_lun,/append
;   PRINTF,lu,FXPAR(hdr,'DATE_OBS'),totmass,box,format='(a20,e15.4,3x,4i5)'
   PRINTF,lu,hdr.DATE_OBS,totmass,box,format='(a20,e15.4,3x,4i5)'
   CLOSE,lu
   FREE_LUN,lu
ENDIF
;
;
IF KEYWORD_SET(all) THEN RETURN,mass ELSE RETURN,totmass
END
