function sharpen,img,bkg,factor,PF=PF, BOX_SIZE=box_size, NO_RATIO=no_ratio
;+
; $Id: sharpen.pro,v 1.1 2008/02/19 19:48:42 nathan Exp $
; NAME:
;	SHARPEN
;
; PURPOSE:
;	Sharpens a ratio image by adding in a small amount of an edge enhanced image
;
; CATEGORY:
;	LASCO UTIL
;
; CALLING SEQUENCE:
;	Result = SHARPEN(Img,Bkg,factor)
;
; INPUTS:
;	Img:	Input image in DN/sec
;	Bkg:	Background image in DN/sec
;	Factor:	Factor of edge enhanced image to add to original image, Default is .015
;
; KEYWORD PARAMETERS:
;	PF:	Point filter factor, default is 4
;	BOX_SIZE:	Size of box to use in unsharp mask.  Default is 11 points
;	NO_RATIO:	Return straight edge-enhanced image
;
; OUTPUTS:
;	This function returns the edge enhance ratio image as a real number.
;
; SIDE EFFECTS:
;
; RESTRICTIONS:
;
; PROCEDURE:
;	The procedure to enhance an image adds a little edge enhancement to the original image.
;
;	First the image (in DN/sec) is point filtered to remove the stars and cosmic rays.
;
;	Then the unsharp mask image is formed with the original image and the background
;	image.  The edge enhanced image is the difference between the unsharp mask of
;	the original image and the background image.  The difference is performed to remove
;	any artifacts such as stray light arcs that are in both the original and background images.
;
;	The ratio image is computed and the missing blocks are set to 1.0.
;
;	The edge enhanced image is computed as:
;		(Img/Bkg) + factor*edge_enhanced_image 
;
; EXAMPLE:
;
; MODIFICATION HISTORY:
;
; $Log: sharpen.pro,v $
; Revision 1.1  2008/02/19 19:48:42  nathan
; moved from lasco/idl/util
;
; 	Written by:	RAH, 20 Apr 98
;	99/10/27, N Rich	Add NO_RATIO keyword
;
;	%W% %H% LASCO IDL LIBRARY
;-
;
;  Remove some of the cosmic rays and stars to reduce their impact 
;  on the unsharp masking.  Do two iterations
;
IF KEYWORD_SET(pf)  THEN pointfac=pf ELSE pointfac=4.
IF KEYWORD_SET(BOX_SIZE)  THEN bs=box_size ELSE bs=11
IF (factor EQ 0)  THEN fac=.015 ELSE fac=factor
POINT_FILTER,img,5,pointfac,2,img,outputs
;
;  Form the unsharp masks of the original image and the background image
;
unsh = img-SMOOTH(img,bs)
;tvscl,unsh
unshbkg = bkg-SMOOTH(bkg,bs)
;tvscl,unshbkg
;
;  Subtract off the background unsharp mask to remove the stray light artifacts
;
edge = unsh-unshbkg
IF keyword_set(NO_RATIO) THEN RETURN, edge
;
;  Add some of the edge enhancements back into the original image
;
a = img/(bkg>1)
w = WHERE (bkg LE 0,nw)
IF (nw GT 0)  THEN a(w)=1
RETURN,a+fac*edge
END
