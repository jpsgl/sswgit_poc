function smoothpeaks,img0,boxsz,factor, DISPLAY=display, WHERESUB=wheresub, DARK=dark, BRIGHT=bright, DEBUG=debug
;+
; $Id: smoothpeaks.pro,v 1.4 2013/11/20 17:40:45 nathan Exp $
; NAME:
;	SMOOTHPEAKS
;
; PURPOSE:
;	remove discontinuities from image above and/or below a median-filter threshold
;
; CATEGORY:
;	secchi util
;
; CALLING SEQUENCE:
;	Result = smoothfilter(Img,boxsz,factor)
;
; INPUTS:
;	Img:	Input array
;	boxsz:	input to smooth
;	Factor:	factor to test roughness; it is the same scale as input
;
; REQUIRED KEWYORD PARAMETERS:
;   	/DARK	Replace dark pixels
;   	    and/or
;   	/BRIGHT Replace bright pixels
;
; KEYWORD PARAMETERS:
;   	/DISPLAY    Show which pixels are selected
;   	WHERESUB=   returns subscripts where discontinuities exceed a threshold
;	/DEBUG	Go slow, also implies /DISPLAY
;
; OUTPUTS:
;	img with some pixels replaced with smooth img
;
; SIDE EFFECTS:
;
; RESTRICTIONS:
;
; PROCEDURE: get filter_image() after extrapolating edges. Replace pixels above a threshold
;
; MODIFICATION HISTORY:
;
; $Log: smoothpeaks.pro,v $
; Revision 1.4  2013/11/20 17:40:45  nathan
; implement extend_edges.pro
;
; Revision 1.3  2012/05/02 15:08:57  nathan
; print filter_image duration
;
; Revision 1.2  2012/03/22 15:44:23  secchia
; add debug option to do wait
;
; Revision 1.1  2012/03/07 21:37:08  nathan
; used in scc_mk_daily_med.pro
;
;-

factor=abs(factor)
IF keyword_set(DEGUG) THEN message,'smoothfilter(,'+trim(boxsz)+','+trim(fac)+')',/info

sz=size(img0)
nx0=sz[1]
ny0=sz[2]
nex=boxsz*2
ximg=extend_edges(img0,nex)

t0=systime(1)

xsma=filter_image(ximg,median=boxsz,/all_pixels)
sma = xsma[nex:nex+nx0-1,nex:nex+ny0-1]

IF keyword_set(DEBUG) THEN BEGIN
    print,''
    print,'filter_image took ',trim(systime(1)-t0),' seconds.'
    print,''
ENDIF
dif=img0-sma

wd=where(dif LT -factor,nwd)
wb=where(dif GT factor,nwb)
IF keyword_SET(DARK) and nwd GT 0 THEN wheresub=wd ELSE $
IF keyword_set(BRIGHT) and nwb GT 0 THEN wheresub=wb
IF keyword_set(DARK) and keyword_set(BRIGHT) THEN wheresub=(union(wd,wb))>0
img=img0
nwn=n_elements(wheresub)
IF nwn GT 1 THEN BEGIN
    IF keyword_set(DISPLAY) or keyword_set(DEBUG) THEN BEGIN
	sz=size(img0)
	bim=bytarr(sz[1],sz[2])
	bim[wheresub]=1
	wnd,4,bim,/noresize
	help,boxsz,factor
    ENDIF
    img[wheresub]=sma[wheresub]
ENDIF ELSE $
    message,'img returned unchanged.',/info

if keyword_set(DEBUG) THEN wait,2    
return,img

END
