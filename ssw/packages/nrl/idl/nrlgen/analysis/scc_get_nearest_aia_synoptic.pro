;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_GET_NEAREST_AIA_SYNOPTIC
;
; Purpose     :	Retrieve nearest AIA 193 image to specified time.
;
; Category    :	
;
; Explanation :	This routine searches the AIA synoptic website to look for the
;               closest image to the requested time.  Called from
;               SSC_GET_NEAREST_AIA when it fails to find anything through the VSO.
;
; Syntax      :	SSC_GET_NEAREST_AIA_SYNOPTIC, DATE, HEADER, IMAGE, FOUND=FOUND
;
; Examples    :	See ssc_get_nearest_aia
;
; Inputs      :	DATE    = Date/time to look for.
;
; Opt. Inputs :	None.
;
; Outputs     :	HEADER  = FITS header of AIA image.
;               IMAGE   = Image array
;
; Opt. Outputs:	None.
;
; Keywords    :	WAVELENGTH = Wavelength to search for.  Default is 193.  If
;                            passed as 195, then automatically converted to 193.
;
;               FOUND   = Returns 1 if an image was found, 0 otherwise.
;
; Calls       :	ANYTIM2UTC, UTC2TAI, CHECK_EXT_TIME, CONCAT_DIR, ANYTIM2TAI,
;               BOOST_ARRAY, BREAK_FILE, MK_TEMP_DIR, FXREAD
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 17-Feb-2011, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
pro scc_get_nearest_aia_synoptic, date, header, image, wavelength=wavelength, found=found,wladj=wladj, nameonly=nameonly
;
common aialist,aiafiles,aiatimes,aiapath,cday,cwave

if ~keyword_set(wavelength) then wavelength = 195 else wavelength=wavelength

if datatype(cday) eq 'UND' then cday=''
if datatype(cwave) eq 'UND' then cwave=-1
aia_wls=[0094,0131,0171,0193,0211,0304,0335,1600]

wave=find_closest(wavelength,aia_wls)

if strlen(date) gt 8 then yyyymmdd=strmid(date,0,4)+strmid(date,5,2)+strmid(date,8,2) else yyyymmdd=date
if cday ne yyyymmdd or cwave ne wave then begin
  aiapath = getenv('AIA_SYNOPTIC')+'/'+strmid(yyyymmdd,0,4)+'/'+strmid(yyyymmdd,4,2)+'/'+strmid(yyyymmdd,6,2)+'/'+string(aia_wls(wave),'(i4.4)')+'/'   ;'http://jsoc.stanford.edu/data/aia/synoptic/'
  aiafiles=file_search(aiapath+'*')
  if aiafiles(0) ne '' then begin
    aiatimes=long(strmid(aiafiles,strpos(aiafiles(0),'.fits')-6,6))
    cday=yyyymmdd
    cwave=wave
  endif else goto,notfound
endif 

selcloset:
if strlen(date) ge 19 then seltime=long(strmid(date,11,2)+strmid(date,14,2)+strmid(date,17,2)) else seltime=120000l
select=find_closest(seltime,aiatimes)
print,'Reading '+aiafiles(select)

;check for hdr only files
hdronly=0
mreadfits,aiafiles(select),fitshdr,/nodata
if fitshdr.naxis eq 0 then begin
   good=indgen(n_elements(aiatimes))
   good=good(where(good ne select))
   aiatimes=aiatimes(good)
   aiafiles=aiafiles(good)
   hdronly=1
endif
if hdronly then goto,selcloset

if keyword_set(nameonly) then image=aiafiles(select) else begin
  mreadfits,aiafiles(select),fitshdr,image,/dash2underscore,/silent
  header = convert2secchi(fitshdr)
  if keyword_set(wladj)then begin
        image = image / header.exptime
        case wavelength of
            171: image = image * 1.1
            195: image = image / 1.9
            284: image = image / 3.4
            304: image = image * ssc_get_aia_304_factor(date)
            else:
        endcase
  endif
endelse
goto,foundit
notfound:
found=0 
if keyword_set(nameonly) then image=''
foundit:
;
return
end
