pro time_avg_diff, flist, times, datacube, COORDS=coords, NCOORDS=ncoords, SCALE=scale, BOTH=both, NOCORR=noCorr
    
;+
; $Id: time_avg_diff.pro,v 1.7 2012/10/09 17:36:04 nathan Exp $
; NAME:
;	time_avg_diff
;
; PURPOSE:
;	Computes time-averaged difference of input FITS files 
;
; CATEGORY: movie analysis  
;
; CALLING SEQUENCE:
;	
;
; INPUTS:
;   flist   STRARR[n]  List of FITS files. EUVI, EIT, MDI, AIA or HMI. May be sub-fields.
;   
; KEYWORD inputs:
;   COORDS= 	FLTARR[4] having vertices of square [x1,x2,y1,y2] in units of carrington longitude (x) and latitude (y)
;   SCALE= 	Carrington degrees per pixel default = .25
;   BOTH=     if 1 use synced image from other SC if 2 use previous image from other SC (SECCHI only)
;   NOCORR=   average not subtracted from datacube
;
; OUTPUTS:
;   times   STRARR[n]  Times of each input image
;   datacube	DBLARR[len,wid,n]   Difference of ROI in constant lat/lon projection 
;
; KEYWORD outputs:
;   NCOORDS=	DBLARR[2,len,wid]   Coordinates of each pixel in output
;
; COMMON:   
;
; Calls from LASCO :  
;
; SIDE EFFECTS:
;	None
;
; RESTRICTIONS:
;
; PROCEDURE:
;   Each image is processed as appropriate for that instrument. The ROI is put in a
;   constant lon-lat projection. Over all images, an average is computed for each 
;   pixel in the projected ROI. Then this average is subtracted from each image
;   and returned.
;
; MODIFICATION HISTORY:
;
; $Log: time_avg_diff.pro,v $
; Revision 1.7  2012/10/09 17:36:04  nathan
; update output description
;
; Revision 1.6  2011/03/31 15:35:28  nathan
; change format of coords input
;
; Revision 1.5  2011/03/08 17:14:36  mcnutt
; added check for valid points
;
; Revision 1.4  2011/03/04 13:39:14  mcnutt
; set carrington key word
;
; Revision 1.3  2011/03/01 19:15:21  mcnutt
; added first run of program synopic mapping base on scc_euvi_synoptic.pro
;
; Revision 1.2  2011/02/25 16:08:59  nathan
; specification
;
; Revision 1.1  2011/02/25 16:04:24  nathan
; specification
;
;-

       mreadfits,flist(0),h00,image,/dash2underscore,/silent
       instrume=h00.INSTRUME
       isse= (instrume eq 'SECCHI')
       IF isse THEN fichar=reform(byte(strmid(h00.filename,16,1))) ELSE fichar=0
       IF (fichar LE 57 AND fichar GE 48) or keyword_set(level1) THEN isl1=1 else isl1=0
       IF not isse or isl1 THEN h0=convert2secchi(h00,/NOCORRECTION) else h0=h00
       cam=strlowcase(h0.detector)
       IF isse THEN aorb=STRMID(h0.obsrvtry,7,1) ELSE aorb=h0.TELESCOP
       IF aorb EQ 'B' THEN ab=1 ELSE ab=0
	    iseu= (cam eq 'euvi')
	    isai= (cam eq 'aia')
	    isei= (h0.INSTRUME eq 'EIT')

       if keyword_set(scale) then scale=scale else scale=.25
       if keyword_set(coords) then coords=coords else coords=[0.,360.,-90.,90.]
       if coords[0] gt coords[1] then coords[1]=coords[1]+360.
       if coords[2] gt coords[3] then coords[2:3]=[coords[3],coords[2]]

       nx = 1 + (coords[1]-coords[0]) / scale
       lon = scale*findgen(nx) +coords[0]
       ny = 1 + (coords[3]-coords[2]) / scale
       lat = scale*findgen(ny) + coords[2]

       w=where(lon gt 360.,count)
       if count gt 0 then lon[w] = lon[w]-360.

       lon = rebin(reform(lon,nx,1), nx, ny)
       lat = rebin(reform(lat,1,ny), nx, ny)


       len=n_Elements(flist)
       datacube=dblarr(nx,ny,len)
       ncoords=fltarr(2,nx,ny)
       ncoords(0,*,*)=lon
       ncoords(1,*,*)=lat
       times=strarr(len)
       base=dblarr(nx,ny) +0
       divisor=intarr(nx,ny) +0
 
  
       for i=0,n_Elements(flist)-1 do begin
          if iseu then secchi_prep, flist(i), hdr, image, /silent
          if isei then begin 
	      EIT_PREP,  flist(i), fitshdr, image, /RESP, /NRL, /FLOAT, /FILTER, /NO_ROLL, $
    	    	     NO_CALIBRATE=1, FILL=fltarr(32,32), VERBOSE=~quiet
		; always do roll correction with scc_roll_image.pro later
		hdr = convert2secchi(fitshdr)
		image=image/hdr.exptime
		;IF ~keyword_set(NOEXPCORR) and (NOCALIMGVAL) THEN image=image/hdr.exptime
          endif
          if ~iseu and ~isei then begin  ;see scc_euvi_synoptic for more AIA options such as ssc_get_nearest_aia and 171 correction factors to apply.
                 mreadfits,flist(i),fitshdr,image,/dash2underscore,/silent
		hdr = convert2secchi(fitshdr)
	  endif
          times(i)=hdr.date_obs
          wcs= fitshead2wcs(hdr)

          wcs_convert_to_coord, wcs, coord, 'hg', lon, lat, /carrington;=carrington
          pixel = wcs_get_pixel(wcs, coord)

          map = interpolate(image, reform(pixel[0,*,*]), reform(pixel[1,*,*]), missing=!values.f_nan, _extra=_extra)
          if iseu and keyword_set(both) then begin
	       name2 = scc_find_synced(flist(i))
	       IF name2 EQ '' and both gt 1 THEN name2=name2prev  ; use previous image if matching image is missing
               IF name2 NE '' then begin
                 secchi_prep, name2, hdro, image, /silent
                 wcs= fitshead2wcs(hdro)
                 wcs_convert_to_coord, wcs, coordo, 'hg', lon, lat, /carrington;=carrington
                 pixel = wcs_get_pixel(wcs, coordo)
                 map_o = interpolate(image, reform(pixel[0,*,*]), reform(pixel[1,*,*]), missing=!values.f_nan, _extra=_extra)
                 w = where_missing(map)
                 map[w] = map_o[w]
                 dist1 = total((coord/hdr.rsun)^2, 1)
                 dist2 = total((coordo/hdro.rsun)^2, 1)
                 w = where(dist2 lt dist1, count)
                 if count gt 0 then map[w] = map_o[w]
                 name2prev=name2
               ENDIF
          endif      
          w =  where_not_missing(map, count)
          if count gt 0 then begin
            base(w)=base(w)+map(w)
            divisor(w)=divisor(w)+1
          endif
          datacube(*,*,i) =map
      endfor 

      if ~keyword_set(nocorr) then begin
         base=base/divisor
         for n=0,len-1 do begin
            map=datacube(*,*,n)
            w=where_not_missing(map)
            map(w)=map(w)-base(w)
	    datacube(*,*,n)=map
         endfor
      endif

end
