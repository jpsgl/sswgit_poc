;+
; $Id: value_to_index.pro,v 1.1 2008/08/11 14:24:11 nathan Exp $
; NAME:
;
;    VALUE_TO_INDEX
;
; PURPOSE:
;
;    Given a (1D) ARRAY and a scalar VALUE, determine the array INDEX
;    corresponding to the element of the array that is closest in
;    magnitude to the specified value.
;
; CALLING SEQUENCE:
;
;   Index=VALUE_TO_INDEX(ARRAY, VALUE)
; 
; INPUTS:
;
;   ARRAY = 1D array of values
;   
;   VALUE = scalar value
;
; EXAMPLE:
;
;  ARRAY=findgen(100)/99.*5          ; create an array of 100 pts from 0 to 5.
;
;  Index=VALUE_TO_INDEX(ARRAY,3.125) ; find the element of ARRAY whose value
;                                    ; is closest to 3.125.
;                                   
;  In this case, Index=62 (i.e., ARRAY(62)=3.13131)
; 
; MODIFICATION HISTORY:
; $Log: value_to_index.pro,v $
; Revision 1.1  2008/08/11 14:24:11  nathan
; copied from $NRL_LIB/windt for image_profiles.pro
;
;   David L. Windt, Bell Labs, March 1997.
;   windt@bell-labs.com
;-
function value_to_index,array,value
on_error,2
acceptance=(value/1.e4) > 1.e-6 ; choose an acceptance criteria
                                ; that's small enough.
if n_elements(value) ne 1 then message,'VALUE must be a scalar.'
sz=size(array)
if sz(0) ne 1 then message,'ARRAY must be a 1-D array.'
if value ge max(array) then return,!c
if value le min(array) then return,!c
repeat begin                    ; loop until a point is found.
    index=where(abs(value-array) lt acceptance)
    acceptance=acceptance*2.    ; increase acceptance region.
endrep until total([index,index]) ge 0  

return,index(0)                 ; make sure there's only one and it's a scalar.
end
