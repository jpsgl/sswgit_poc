function extrapolate,inp0,add
;+
; $Id: extrapolate.pro,v 1.1 2013/11/21 22:18:14 nathan Exp $
; NAME:
;
; PURPOSE:  extrapolate a line
;
; CATEGORY: secchi util image processing
;
; CALLING SEQUENCE:
;	Result = fix_edges(vector, scalar)
;
; INPUTS:
;	inp:	1-D array
;	add:	number of values to append to end of inp
;
; REQUIRED KEWYORD PARAMETERS:
;
; KEYWORD PARAMETERS:
;
; OUTPUTS:
;	1-D array of length n_elements(inp)+add
;
; SIDE EFFECTS:
;
; RESTRICTIONS: Currently linear interp only; could add other functions as desired.
;
; PROCEDURE:
;
; MODIFICATION HISTORY:
;
; $Log: extrapolate.pro,v $
; Revision 1.1  2013/11/21 22:18:14  nathan
; used in smoothpeaks used in scc_mk_daily_med.pro
;
;-

nin=n_elements(inp0)
inp=inp0
out=reform(inp0)
; remove outliers, 2 on each side?
s=sort(inp)
inp[s[0]]=!values.f_nan
inp[s[1]]=!values.f_nan
inp[s[nin-1]]=!values.f_nan
inp[s[nin-2]]=!values.f_nan
g=where_not_missing(inp)

IF n_elements(g) GT 3 THEN BEGIN
    fit=poly_fit(g,inp[g],1)
    FOR i=nin,nin+add-1 do out=[out,fit[0]+fit[1]*i]
ENDIF ELSE $
        FOR i=nin,nin+add-1 do out=[out,!values.f_nan]

return,out

END
