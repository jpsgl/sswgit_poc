function extend_edges,im,x, DEBUG=debug
;+
; $Id: extend_edges.pro,v 1.3 2013/11/21 22:24:08 nathan Exp $
; NAME:
;	SMOOTHPEAKS
;
; PURPOSE:
;	extrapolate edges of image with linear fit
;
; CATEGORY:
;	secchi util
;
; CALLING SEQUENCE:
;	Result = extend_edges(array, x)
;
; INPUTS:
;	array:	Input array
;	x:  	number of pixels to extend edges
;
; REQUIRED KEWYORD PARAMETERS:
;
; KEYWORD PARAMETERS:
;   	/DISPLAY    Show which pixels are selected
;	/DEBUG	Go slow, also implies /DISPLAY
;
; OUTPUTS:
;	array with dimensions extended by 2*x in each dimension
;
; SIDE EFFECTS:
;
; RESTRICTIONS:
;
; PROCEDURE:
;
; MODIFICATION HISTORY:
;
; $Log: extend_edges.pro,v $
; Revision 1.3  2013/11/21 22:24:08  nathan
; fix errors
;
; Revision 1.2  2013/11/20 17:40:45  nathan
; implement extend_edges.pro
;
; Revision 1.1  2013/11/20 16:13:04  nathan
; stub
;
;
;-

sz=size(im)
nx0=sz[1]
ny0=sz[2]
newarr=dblarr(sz[1]+2*x,sz[2]+2*x)
newarr[x,x]=im

FOR i=0,nx0-1 DO BEGIN
; top
    nin=30
    vec=im[i,ny0-nin:ny0-1]
    nvec=extrapolate(vec,x)
    newarr[x+i,x+ny0-nin:x+x+ny0-1]=nvec
    
; bottom
    vec=reverse(reform(im[i,0:nin-1]))
    nvec=extrapolate(vec,x)
    newarr[x+i,0:x+nin-1]=reverse(nvec)
    
ENDFOR
    
FOR i=0,ny0-1 DO BEGIN
; right
    nin=30
    vec=im[nx0-nin:nx0-1,i]
    nvec=extrapolate(vec,x)
    newarr[x+nx0-nin:x+x+nx0-1,x+i]=nvec
; left
    vec=reverse(im[0:nin-1,i])
    nvec=extrapolate(vec,x)
    newarr[0:x+nin-1,x+i]=reverse(nvec)
ENDFOR
    
; corners are still zero

return, newarr

END
