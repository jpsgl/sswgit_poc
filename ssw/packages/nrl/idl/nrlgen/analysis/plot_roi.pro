pro plot_roi, roi, nx,ny

; input:
;   roi     1-d Array of Region of interest of image 
;   nx, ny  dimensions of image
;

szr=size(roi)
one2two, roi, [nx, ny], cols, rows 
    

mnx=min(cols,max=mxx)
mny=min(rows,max=mxy)
help,mnx,mny,mxx,mxy

w1=where(cols eq mnx)
w2=where(rows eq mny)
w3=where(cols eq mxx)
w4=where(rows eq mxy)

xx=[mnx,cols[w2[0]],mxx,cols[w4[0]],mnx]
yy=[rows[w1[0]],mny,rows[w3[0]],mxy,rows[w1[0]]]

PLOTS,xx,yy,/device,color=255

end
