function local_slope,x,y,nx
;+
; $Id: local_slope.pro,v 1.1 2013/01/24 23:30:29 nathan Exp $
; NAME:
;
; PURPOSE:  compute slope of moving window
;
; CATEGORY: secchi util math curve analysis
;
; CALLING SEQUENCE:
;	Result = local_slope(x,y,n)
;
; INPUTS:
;   	x   Independent variable (such as time)
;   	y   Dependent variable. May be discontinuous.
;   	nx  Number of x to use to compute slope (size of window)
;
; REQUIRED KEWYORD PARAMETERS:
;
; KEYWORD PARAMETERS:
;
; OUTPUTS:
;	fltarr of same number elements as y
;
; SIDE EFFECTS:
;
; RESTRICTIONS: x and y must be same size; slope must be always positive; if 
;   	nx is even, nx-1 will be used
;
; PROCEDURE: start and end of segment will have nx/2 values = result at nx/2 from start/end; 
;   	    if x has less than nx elements, value will be computed for the elements present.
;
; MODIFICATION HISTORY:
;
; $Log: local_slope.pro,v $
; Revision 1.1  2013/01/24 23:30:29  nathan
; used for computing actual write rate on SSR1
;
;-

nin=n_elements(x)
if nin lt nx THEN nx=nin
if (nx mod 2) EQ 0 THEN nx=nx-1
xm=nx/2
xp=nx/2
IF nx EQ 1 THEN xp=1	; and xm=0
res =fltarr(nin)
res[*]=-99.
nr0 =nin-xm-xp
lastdx=0
for i=0,nin-1 do begin
; figure out interval to check
    newm=1
    x1=i
    x2=i+nx-1
    ; check for end of array
    if x2 gt nin-1 THEN BEGIN
    	x2=nin-1
	newm=0
    ENDIF
    j=x1+1
     
    ; check for end of segment/ discontinuity	
    while j le x2 do begin
    	if y[j]-y[j-1] lt 0 THEN BEGIN
	    x2=j-1 
	; do not compute new slope
	    newm=0
	   ; stop
	ENDIF ELSE j=j+1
    endwhile
	
    ; now get local slope	
    dx=x[x1:x2]
    dy=y[x1:x2]
   ; help,dx
    if n_elements(dx) GT 1 THEN IF (newm) or lastdx EQ 1 then bm=linfit(dx,dy)
    lastdx=n_elements(dx)
    
    ; figure which value in array we are at
    inx=(i+xp)<x2
    res[inx]=bm[1]
    
    ; check for unfilled values so far
    w = where(res[0:inx] eq -99.,nw)
    IF nw GT 0 THEN res[w]=bm[1]
ENDFOR


return,res
END
