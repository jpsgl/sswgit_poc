FUNCTION fCIRCLE, xcenter, ycenter, radius
;
; Modifications:
;  2004.02.05, nbr - rename for SSW compatability
;
points = (2 * !PI / 99.0) * FIndGen(100)
x = xcenter + radius * Cos(points)
y = ycenter + radius * Sin(points)
RETURN, Transpose([[x],[y]])
END
