;+
; PROJECT:
;  SOHO-LASCO
;
; NAME:
;  lstreadstruct
;
; PURPOSE:
;  Read a tree structure in a binary file saved with lstsavestruct.pro
;
; CATEGORY:
;  data handling, io
;
; DESCRIPTION: 
;
; CALLING SEQUENCE:
;
; INPUTS:
;  lun : logical unit of the file where to read the data
;
; OUTPUTS:
;  s       : structure read from the file
;  fldname : give the tag names of the structure
;  sonsize : give the size of the son of the tags
;  strname : give the name of the structures and sub structures, NULL
;            string if anonymous 
;
; INPUT KEYWORDS:
;
; OUTPUT KEYWORDS:
;
; HISTORY:
;  coded by A.Thernisien on 20/08/2002
;
; CVSLOG:
;  Revision 1.1  2002/08/20 15:46:05  arnaud
;  *** empty log message ***
; $Id: lstreadstruct.pro,v 1.2 2007/11/27 22:36:25 thernis Exp $
;-
pro lstreaddesc,lun,fldname,sonsize,strname,ntags,s,ignorestructnames=ignorestructnames

; ---- init tmp var for reading
strtmp=string('',form='(A16)')
sname=string('',form='(A16)')
longtmp=0L

; ---- read the structure name
readu,lun,sname
if keyword_set(ignorestructnames) then sname='' else sname=strtrim(sname,2)
strname=[strname,sname]

first=1b
for i=0,ntags-1 do begin
    ; ---- read the tag name
    readu,lun,strtmp
    fldname=[fldname,strtrim(strtmp,2)]
    ; ---- read the number of son
    readu,lun,longtmp
    nbson=longtmp
    ; ---- read the size
    readu,lun,longtmp
    nbsz=longtmp+2
    sz=lonarr(nbsz)
    readu,lun,sz

    sonsize=[sonsize,nbson,longtmp,sz]  ; -- nbtags arraydim arrayinfo

    ; -- descent the tree if several sons
    if nbson gt 0 then begin
        lstreaddesc,lun,fldname,sonsize,strname,nbson,ssub,ignorestructnames=ignorestructnames
        if first then begin
            s=create_struct(strtrim(strtmp,2),$
                            make_array(dimension=sz(0:nbsz-3),value=ssub))
            first=0B
        endif else if i eq ntags-1 and sname ne '' then $
          s=create_struct(name=sname,s,strtrim(strtmp,2),$
                          make_array(dimension=sz(0:nbsz-3),value=ssub)) else $
          s=create_struct(s,strtrim(strtmp,2),$
                          make_array(dimension=sz(0:nbsz-3),value=ssub))
    endif else begin
        if first then begin
            if longtmp eq 0 then begin
                s=create_struct(strtrim(strtmp,2),$
                                (make_array(1,type=sz(nbsz-2)))(0)) 
            endif else begin
                s=create_struct(strtrim(strtmp,2),$
                                make_array(size=[longtmp,sz]))
            endelse
            first=0B
        endif else begin
            if longtmp eq 0 then begin
                if i eq ntags-1 and sname ne '' then $
                  s=create_struct(name=sname,s,strtrim(strtmp,2),$
                                  (make_array(1,type=sz(nbsz-2)))(0)) else $
                  s=create_struct(s,strtrim(strtmp,2),$
                                  (make_array(1,type=sz(nbsz-2)))(0))
            endif else begin
                if i eq ntags-1 and sname ne '' then $
                  s=create_struct(name=sname,s,strtrim(strtmp,2),$
                                  make_array(size=[longtmp,sz])) else $
                  s=create_struct(s,strtrim(strtmp,2),$
                                  make_array(size=[longtmp,sz]))
            endelse
        endelse
        ; -- format the length of the strings
        if sz(nbsz-2) eq 7 then if longtmp eq 0 then $
          s.(i)=string('',form='(A'+strtrim(sz(nbsz-1),2)+')') else s.(i)[*]=string('',form='(A'+strtrim(sz(nbsz-1),2)+')')
        ;begin
        ;    for j=0,n_elements(s.(i))-1 do $
        ;      s.(i)[j]=string('',form='(A'+strtrim(sz(nbsz-1),2)+')')
        ;endelse
    endelse    
endfor

return
end



pro lstreadstruct,s,lun,fldname,sonsize,strname,ignorestructnames=ignorestructnames

; ------ read the structure description
; ---- init tmp var for reading
strtmp=string('',form='(A16)')
longtmp=0L

; ---- read the first element of the structure description: must be ROOT
readu,lun,strtmp
if strtrim(strtmp,2) ne 'ROOT' then message,'Cannot read the structure descrition!'
fldname=strtrim(strtmp,2)

; -- read the number of tags
readu,lun,longtmp
ntags=longtmp

; -- read the size
readu,lun,longtmp
nbsz=longtmp+2
sz=lonarr(nbsz)
readu,lun,sz

sonsize=[ntags,longtmp,sz]      ; -- nbtags arraydim arrayinfo
strname=''

if ntags gt 0 then begin
    lstreaddesc,lun,fldname,sonsize,strname,ntags,s,ignorestructnames=ignorestructnames
    s=make_array(dimension=sz(0:nbsz-3),value=s)
endif else begin
    print,'The structure description is void : I assume that it is an array...'
    if longtmp eq 0 then $
      s=create_struct(strtrim(strtmp,2),(make_array(1,type=sz(nbsz-2)))(0)) else $
      s=create_struct(strtrim(strtmp,2),make_array(size=[longtmp,sz]))
endelse

; ---- now read the data
readu,lun,s

strname=strname(1:*)

return
end
