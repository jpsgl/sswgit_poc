pro showkwords, hdrs, filename=filename, _extra=_extra, subscript=subscript

;+
; $Id: showkwords.pro,v 1.5 2016/11/20 01:15:40 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : showkwords
;               
; Purpose   : print a table of keyword values
;               
; Explanation: 
;               
; Use       : IDL> 
;    
; Inputs    :   hdrs      STCARR    array of structures

; Optional Inputs: 
;               
; Outputs   : print a table

; Optional Outputs: print to a file
;
; Keywords  :   FILENAME = 'name_of_file_to_output_to'
;               /ANY_TAG_NAME
;               SUBSCRIPT   set to print subscript value of input array
;
; Category    : structure utility database fits
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL/I2, Apr 05
;               
; $Log: showkwords.pro,v $
; Revision 1.5  2016/11/20 01:15:40  nathan
; allow /filename
;
; Revision 1.4  2005/10/17 19:03:14  nathan
; make more general (non-image-header)
;
; Revision 1.3  2005/07/29 19:11:44  nathan
; adjust output formatting
;
; Revision 1.2  2005/07/19 18:56:13  nathan
; make general for any structure and keywords
;
; Revision 1.1  2005/04/21 19:26:16  nathan
; no comment
;
;-            

nh=n_elements(hdrs)
hdr1=hdrs[0]
if n_elements(_extra) GT 0 THEN pattern=['FILENAME',tag_names(_extra)] ELSE pattern=['FILENAME']
nk=n_elements(pattern)

IF tag_exist(hdr1,'FILENAME') THEN BEGIN
    ;      20050406_172656_n4c1A.fts   *****   143   exposfps.img  
    strng='FILENAME                  ' 
    sprtr='========================= '
    IF keyword_set(SUBSCRIPT) THEN BEGIN
	    strng=strng+' '+'  SUBSCR'
	    sprtr=sprtr+' '+'========'
    ENDIF
ENDIF
for i=1,nk-1 do begin
    kword=pattern[i]
    kwordlen=strlen(kword)
    stat=execute('kwordval=hdr1.'+kword)
    kwordtype=datatype(kwordval)
    IF strmid(kword,0,4) EQ 'DATE' AND kwordtype EQ 'STR' THEN BEGIN
    	strng=strng+' '+string(kword,format='(a19)') 
        sprtr=sprtr+' '+'==================='
    ENDIF ELSE IF kwordtype EQ 'STR'	THEN BEGIN
    	strng=strng+' '+string(kword,format='(a12)') 
        sprtr=sprtr+' '+'============'
    ENDIF ELSE BEGIN
    	strng=strng+' '+string(kword,format='(a8)')
        sprtr=sprtr+' '+'========'
    ENDELSE
endfor
print,strng
print,sprtr
if keyword_set(filename) THEN BEGIN
    if datatype(filename) eq 'STR' then begin 
    openw,lun,filename,/get_lun
    printf,lun,strng
    printf,lun,sprtr
    endif else filename=0
endif

for i=0,nh-1 do begin
	hdr=hdrs[i]
	IF tag_exist(hdr,'FILENAME') THEN strng = rstrmid(hdr.filename,0,25)+' '
	IF keyword_set(SUBSCRIPT) THEN strng = strng+' '+string(i,format='(i8)')
        for j=1,nk-1 do begin
		kword=pattern[j]
		stat=execute('kwordval=hdr.'+kword)
		kwordtype=datatype(kwordval)
		IF    strmid(kword,0,4) EQ 'DATE' AND kwordtype EQ 'STR' THEN BEGIN
			strng=strng+' '+string(kwordval,format='(a19)') 
		ENDIF ELSE IF kwordtype EQ 'STR'	THEN BEGIN
			strng=strng+' '+string(kwordval,format='(a12)') 
		ENDIF ELSE IF kwordtype EQ 'DOU'	THEN BEGIN
                	IF kwordval LT 1 THEN $
                        strng=strng+' '+string(float(kwordval),format='(f8.6)') ELSE $
			; format for TAI values
			strng=strng+' '+string(float(kwordval),format='(f16.4)')
		ENDIF ELSE IF kwordtype EQ 'FLO'	THEN BEGIN
                	IF kwordval LT 1 THEN $
                        strng=strng+' '+string(float(kwordval),format='(f8.6)') ELSE $
			strng=strng+' '+string(kwordval,format='(g8)')
		ENDIF ELSE IF kwordtype EQ 'INT'	THEN BEGIN
			strng=strng+' '+string(kwordval,format='(i8)')
		ENDIF ELSE IF kwordtype EQ 'LON'	THEN BEGIN
			strng=strng+' '+string(kwordval,format='(i8)')
		ENDIF ELSE IF kwordtype EQ 'UIN'	THEN BEGIN
			strng=strng+' '+string(kwordval,format='(i8)')
		ENDIF
        endfor
        
	print,strng
	IF keyword_set(filename) THEN printf,lun,strng
endfor

if keyword_set(filename) THEN BEGIN
	close,lun
	free_lun,lun
endif
end
