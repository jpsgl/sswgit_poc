;+
; PROJECT:
;  SOHO-LASCO
;
; NAME:
;  lstsavestruct
;
; PURPOSE:
;  Save a tree structure in a binary file
;
; CATEGORY:
;  data handling, io
;
; DESCRIPTION: 
;  See lststruct for the exploration of the trees
;
; CALLING SEQUENCE:
;
; INPUTS:
;  s : structure to save
;  lun : logical unit of the file where to save the data
;
; OUTPUTS:
;  fldname : give the tag names of the structure
;  sonsize : give the size of the son of the tags
;  strname : give the name of the structures and sub structures, NULL
;            string if anonymous 
;
; INPUT KEYWORDS:
;
; OUTPUT KEYWORDS:
;
; HISTORY:
;  coded by A.Thernisien on 20/08/2002
;
; CVSLOG:
;  Revision 1.1  2002/08/20 15:46:05  arnaud
;  *** empty log message ***
; $Id: lstsavestruct.pro,v 1.1 2006/09/11 21:10:44 nathan Exp $
;-
pro lstsavestruct,s,lun,fldname,sonsize,strname

; ---- get the structure description of the array
lststruct,s,fldname,sonsize,strname

idxsonsize=0                    ; -- pointer for the sonsize array
idxstrname=0                    ; -- pointer for structure name array
for i=0,n_elements(fldname)-1 do begin

    ; -- write the field name
    writeu,lun,string(fldname(i),form='(A16)') ; -- limit to 16 chars
    ; -- compute the shift of the pointer according the size of the field
    shiftsonsize=sonsize(idxsonsize+1)+4
    ; -- write the nbson and the size of the field
    writeu,lun,sonsize(idxsonsize:idxsonsize+shiftsonsize-1)
    ; -- test if the son is a struct and write the struct name if yes
    if sonsize(idxsonsize+shiftsonsize-2) eq 8 then begin
            writeu,lun,string(strname(idxstrname),form='(A16)')
            idxstrname=idxstrname+1
    endif
    ; -- point to the next field size
    idxsonsize=idxsonsize+shiftsonsize

endfor

; ---- write the stucture itself with its data
writeu,lun,s

return
end

