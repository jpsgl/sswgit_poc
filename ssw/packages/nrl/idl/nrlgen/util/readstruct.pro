;+
; $Id: readstruct.pro,v 1.3 2007/11/27 22:36:25 thernis Exp $
;
; PURPOSE:
;  read a structure in an xdr file created with savestruct.pro
;
; CATEGORY:
;  io, data handling
;
; INPUTS:
;  file : filename
;
; OUTPUTS:
;  lun : file logical unit number
;  return : the structure extracted from the file
;
;-

function readstruct,file,lun=lun,ignorestructnames=ignorestructnames

len = strlen(file)
ext = strlowcase(strmid(file,len-3,3))
gzip = ext eq '.gz'


openr,lun,file,/xdr,/get_lun,compress=(gzip ? 1b : 0b)
lstreadstruct,s,lun,ignorestructnames=ignorestructnames
free_lun,lun

return,s
end
