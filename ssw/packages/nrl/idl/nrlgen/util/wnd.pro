;+
;  $Id: wnd.pro,v 1.4 2012/03/22 15:44:42 secchia Exp $
;
; PURPOSE:
;      open an IDL display window to the good dimentions
;
; CATEGORY:
;   visualization	
;
; CALLING SEQUENCE:
;	WND,0,image
;
; INPUTS:
;	num : index of the window you want to open
;	x,y : two cases :
;		x : can be an image (2D array) then the window fit the dimention af this array
;		x,y : (both integer) dim of the win 
;     
; KEYWORD INPUT:
;	nodata : don't display the image: only open the window to the right size
;       title : title of the window
;       sample : sample keyword of the rebin function
;       tv : use tv instead of tvscl
;	/NORESIZE	Ignore size of window to display, if already present
;
; OPTIONAL INPUTS PARAMETERS:
;  f : zoom factor : if < 1 must be an integer division
;
;
; HISTORY:
;  A.Thernisien on 01/10/99
;
;-
pro wnd,num,x,y,NODATA=nodata,title=title,sample=sample,tv=tv, $
	NORESIZE=noresize

; -- display depending on the parameter size
s=size(x)

case 1 of
    ; ---- the second parameter is an image
    (s(0) eq 2) :  begin
        
        if n_elements(y) eq 0 then f=1 else f=y
        
        if f ne 1 then begin
            if s[1]*f ne fix(s[1]*f) or s[2]*f ne fix(s[2]*f) then begin
                print,'Use congrid instead of rebin' 
                xx=congrid(x,s[1]*f,s[2]*f,/interp,/center)
            endif else xx=rebin(x,s[1]*f,s[2]*f,sample=sample)
        endif else xx=x
	IF datatype(xx) NE 'BYT' THEN BEGIN
	    wnan=wherenan(xx,nnan)
	    help,wnan
	ENDIF
	;IF nnan GT 0 THEN xx[wnan]=0
        s=size(xx) 
        ; -- get the current opened window state
        device,window_state=ws
        
        ; -- select the window if available
        if ws[num] then begin
            wset,num
            ; -- resize if not the same size
            if (s[1] ne !d.x_size or s[2] ne !d.y_size) and $
		not keyword_set(NORESIZE) then $
              window,num,xsize=s[1],ysize=s[2],retain=2,title=title
        endif else $
          window,num,xsize=s[1],ysize=s[2],retain=2,title=title
        if not keyword_set(nodata) then $
          if keyword_set(tv) then tv,xx else tvscl,xx
    end
    ; ---- the 2nd and 3rd params are size
    (n_elements(y) eq 1 and n_elements(x) eq 1) : begin
        ; -- get the current opened window state
        device,window_state=ws
        ; -- select the window if available
        if ws[num] then begin
            wset,num
            ; -- resize if not the same size
            if x ne !d.x_size or y ne !d.y_size then $
              window,num,xsize=x,ysize=y,retain=2,title=title
        endif else $
          window,num,xsize=x,ysize=y,retain=2,title=title
    end
    ; -- bad call
    else : message,'Bad input',/inform
endcase

return
end
