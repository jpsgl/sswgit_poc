FUNCTION int2str,input,forcesize
;
; Project   : NRLLIB
;                   
; Name      : int2str
;               
; Purpose   : casts an integer to a string with, optionally, a
;             specified number of digits to provid leading zeros
;               
; Explanation: IDL lacks an intrinsic 'itoa' function.
;               
; Use       : IDL> intstr=int2str(intvalue,[numdigits])
;    
; Inputs    : input: integer to cast
;             optional forcesize: force this size by adding leading zeroes
;               
; Outputs   : a string representation of the input integer
;
; Keywords  : (none)
;               
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : util
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRL, Dec 2006
;               

  str=strtrim(string(input),2)

  ; pad with leading zeroes, if asked
  if (n_elements(forcesize) ne 0) then $
    for ilen=strlen(str),forcesize-1 do str='0'+str

  return,str

END
