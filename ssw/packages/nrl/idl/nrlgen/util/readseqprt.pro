function readseqprt,filein, columnheadings, DELIM=delim, HEADING=heading, $
    COLUMNS=columns, OMIT_PARTIAL=omit_partial, TEP=tep

;+
; $Id: readseqprt.pro,v 1.3 2018/06/12 19:02:22 nathan Exp $
;
; Project   : SOLOHI
;                   
; Name      : readseqprt
;               
; Purpose   : generic pro for reading in ITOS seq prints
;               
; Explanation: 
;               
; Use       : IDL> 
;    
; Inputs    : 
;       filein	Name of seqprt file
;               
; Outputs   : 
;   	STRARR= 	nlines by ncol of data, not including header row

; Optional Outputs: 
;   	columnheadings	STRARR	First row of input file which uses at least 1 delimiter
;
; Keywords  : 
;   	DELIM=	    Column delimiter, default is comma.
;   	HEADING=    Row which has column headings (start at 0)
;   	COLUMNS=    Number of columns in output, in case it is more than first line
;   	/OMIT_PARTIAL	Screen out rows with nothing in first field
; Calls:    
;
; Common    : 
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : database hk utility plotting
;               
; Prev. Hist. : readlist.pro
;
; $Log: readseqprt.pro,v $
; Revision 1.3  2018/06/12 19:02:22  nathan
; read in TEP
;
; Revision 1.2  2016/03/30 18:53:04  nathan
; add COLUMNS=
;
; Revision 1.1  2015/11/04 18:30:12  nathan
; generic pro for reading in seqprt from ITOS
;
;-            

str = ''

exist = FILE_EXIST(filein)
IF ~(exist) THEN BEGIN
	li=findfile(filein)
	IF strlen(li[0]) GT 1 THEN exist=1
ENDIF
IF NOT(exist) THEN BEGIN
   print,filein,' not found - returning.'
   return,''
ENDIF

OPENR, IN, filein, /GET_LUN

flag=2
all = ''
first=1
WHILE NOT(EOF(IN)) DO BEGIN
    READF, IN, str
    ;str = STRTRIM(str,flag)
    IF str EQ '' or strlen(trim(str)) LT 20 THEN BEGIN
    	IF first THEN message,'Omitting empty lines.',/info
	first=0
    ENDIF ELSE all = [all,str]

ENDWHILE

CLOSE, IN
FREE_LUN, IN

IF n_elements(all) LT 2 THEN BEGIN
   print,filein,' is empty - returning.'
   return,''
ENDIF

all = all(1:*)
num = N_ELEMENTS(all)

IF keyword_set(DELIM) THEN dlim=delim ELSE dlim=',' 
isrgx=0
IF dlim EQ ' ' or keyword_set(TEP) THEN BEGIN
    isrgx=1
    dlim ='[[:space:]]+'
ENDIF
np0=0
i=0
IF keyword_set(HEADING) THEN BEGIN
    parts=strsplit(all[heading],dlim,/extract, REGEX=isrgx)
    i=heading+1
    np0=n_elements(parts)
ENDIF ELSE BEGIN
    WHILE np0 LT 2 DO BEGIN
    	parts=strsplit(all[i],dlim,/extract, REGEX=isrgx)
    	;print,parts
    	np0=n_elements(parts)
	i=i+1
    ENDWHILE
ENDELSE
columnheadings=parts

np=np0
IF keyword_set(COLUMNS) THEN np=columns
IF keyword_SET(TEP) THEN BEGIN
    parts=strsplit(all[2],dlim,/extract, REGEX=isrgx)
    np=n_elements(parts)
ENDIF

; now extract data
allout=strarr(np,num-i)

FOR j=i,num-1 DO BEGIN
    parts=strsplit(all[j],dlim,/extract, REGEX=isrgx)
    ;print,parts
    allout[0,j-i]=trim(parts)
ENDFOR    	

times=allout[0,*]

; is time using doy or not?

ta=strsplit(allout[0,2],'-')
IF total(ta) EQ 10 THEN BEGIN
    strput,times,'T',6
    allout[0,*]='20'+times
ENDIF
ta2=strsplit(allout[1,2],'-')
times2=allout[1,*]
IF total(ta2) EQ 10 THEN BEGIN
    strput,times2,'T',6
    allout[1,*]='20'+times2
ENDIF

; TEP special case
IF parts[2] EQ 'IDLE' or keyword_set(TEP) THEN BEGIN
    allout0=allout
    nt = (np-2)/2
    allout=strarr(2+nt,num-1)
    allout[0,*]=allout0[0,*]
    allout[1,*]=allout0[1,*]
    j=2
    FOR i=2,np-1,2 do BEGIN
    	columnheadings=[columnheadings,parts[i]]
	allout[j,*]=allout0[i+1,*]
	j=j+1
    ENDFOR
    np0=2+nt
ENDIF
	
for i=0,np0-1 do print,i,'  ',columnheadings[i]

RETURN, allout

end
