pro sort_file, fname, sortsta, sortnc, uqsta, uqnc, OVERWRITE=overwrite
;+
; $Id: sort_file.pro,v 1.1 2012/11/30 21:10:06 nathan Exp $
;
; Project     : STEREO
;
; Name        : sort_file
;
; Purpose     : read in input file, sort, make sure each line is uniq for optional substring
;
; Explanation : 
;
; Use         : 
;
; Example     : 
;
; Inputs      : fname	STRING	file to read in, sort, and replace
;
; Optional inputs:  
;   	sortsta INT start character in each row to use for sort
;   	sortnc	INT number of characters to use from x1
;   	UQSTA   INT start character in each row to use for uniq (else use x1)
;   	UQNC    INT number of characters to use from uqsta for sort (else use nc)
;
; Outputs     : file fname.new or overwrite input
;
; Keywords    : /OVERWRITE  Overwrite input filename
;
; Calls       : readlist
;
; Category    : system, files, admin
;
; Prev. Hist. : None.
;
; MODIFICATION HISTORY:
; $Log: sort_file.pro,v $
; Revision 1.1  2012/11/30 21:10:06  nathan
; for fixing img_hdr.txt files
;

rows=readlist(fname,nrows)
help,rows

IF n_params() GT 1 THEN ssta=sortsta ELSE ssta=0
IF n_params() GT 2 THEN snc =sortnc  ELSE snc =strlen(rows[0])
IF n_params() GT 3 THEN usta=uqsta ELSE usta=ssta
IF n_params() GT 4 THEN unc =uqnc  ELSE unc =snc

ustr=strmid(rows[0],usta,unc)
print,'Using >',ustr,'< for uniq.'


; Do uniq; Always keep the last entry (LASCO img_hdr.txt)
newli=rows[0]
FOR i=1,nrows-1 DO BEGIN
    ustri=strmid(rows[i],usta,unc)
    w=where(ustr EQ ustri,nw)
    IF nw GT 0 THEN newli[w[0]]=rows[i] ELSE BEGIN
    	ustr=[ustr,ustri]
	newli=[newli,rows[i]]
    ENDELSE
ENDFOR
; now each row is uniq, need to sort

sstr=strmid(newli,ssta,snc)
print,'Using >',sstr[0],'< for sort.'
srows=newli[sort(sstr)]


nrowsf=n_elements(srows)
IF keyword_set(OVERWRITE) THEN outname=fname ELSE outname=fname+'.new'

openw,luo,outname,/get_lun
FOR i=0,nrowsf-1 DO printf,luo,srows[i]
free_lun,luo

end
