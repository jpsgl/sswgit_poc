pro ps_setup,param,printer,COLOR_PRT=color_prt,LANDSCAPE=landscape,ENCAP=encap,GIF=gif
;+
; $Id: ps_setup.pro,v 1.2 2011/12/01 17:01:04 nathan Exp $
; NAME:			ps_setup
;
; PURPOSE:		setup Postscript printer
;
; CATEGORY:
; CALLING SEQUENCE:	ps_setup,param,printer
; INPUTS:		param = 0 to change the idl plot device to the printer
;			param =	1 to return the plot device back to the screen
;				  and to transfer the plot file to the printer
; 			param = 2 to read current window and send to Postscript
;		  		  This may be faster for plots with lots of 
;				  points.  This is a screen dump, if you 
;				  suddenly decide a plot is worth printing.
;
; 			param = 3 Save to a file
;
; 			param = 4 Same as 2 but color table is inverted first
;
; OPTIONAL INPUTS:	printer = ascii string designating the printer to be
;				  used
;			color_prt = specify color postscript printer
;
;			encap = output encapsulated Postscript
;
;                       gif = output gif image (only for ps_setup,2)
;
;
; OUTPUTS:
; OPTIONAL OUTPUT PARAMETERS:
; COMMON BLOCKS:
; SIDE EFFECTS:
; RESTRICTIONS:
; PROCEDURE:
;		  Example:
;		    ps_setup,0
;		    plot,x
;		    plot,y
;		    ps_setup,1,'A13'
;		  Example:
;		    ps_setup,0,/color_prt
;		    tvscl,img
;		    ps_setup,1,'lasco_phaser'
;		  Example:
;		    plot,huge_array
;		    ps_setup,2
;
; MODIFICATION HISTORY:
;	WRITTEN BY:	RA Howard, NRL, 1990
; $Log: ps_setup.pro,v $
; Revision 1.2  2011/12/01 17:01:04  nathan
; use select_windows
;
; Revision 1.1  2008/05/15 17:11:42  nathan
; moved from lasco/idl/util
;
;	2000/07/18, NBR - Use printer input for naming GIF file
; 
;	07/18/00 @(#)ps_setup.pro	1.10 LASCO IDL LIBRARY
;
;-
;
common plot_setup,f,lastdevice
common colors,ro,go,bo,red,green,blue
count = 0
tvlct,red,green,blue,/get
;help,lastdevice
if param eq 0 then begin
	fi = 'idlplot.ps'
        if keyword_set(encap) then begin
         fi = 'idlplot.eps'
        endif
	lastdevice=!d.name
	repeat begin
                 count = count+1
		f=fi+string( format='(I3.3)',count)
		nf=findfile(f) 
		s = size(nf)
	endrep until s(0) eq 0
	set_plot,'ps',/interpolate
        if keyword_set(encap) then begin
         device,/ENCAPSULATED
         print,'Encapsulated Postscript'
        endif
        if keyword_set(landscape) then begin
	 if (n_params() lt 3) then device,/landscape,filename=f $
	 else device,/landscape,/color,filename=f,bits_per_pixel=8
        endif else begin
	 if (n_params() lt 3) then device,filename=f $
	 else device,/color,filename=f,bits_per_pixel=8
        endelse
        tvlct,red,green,blue
endif else $
if param eq 2 or param eq 4 then begin
	select_windows
	x = tvrd()
        if param eq 4 then x = BYTE(255 - x)
        IF KEYWORD_SET(GIF) THEN BEGIN
           fi=STRCOMPRESS(printer,/REMOVE_ALL)+'.gif'
	   nf = findfile(fi)
	   s = size(nf)
	   IF s(0) NE 0 THEN BEGIN
	      repeat begin
                 count = count+1
		f=fi+string( format='(I3.3)',count)
		nf=findfile(f) 
		s = size(nf)
	      endrep until s(0) eq 0
	   ENDIF ELSE f = fi
           print,'Saving GIF file: '+f
           write_gif,f,x,red,green,blue
        ENDIF ELSE BEGIN
	fi = 'idlplot.ps'
	lastdevice=!d.name
	repeat begin
                 count = count+1
		f=fi+string( format='(I3.3)',count)
		nf=findfile(f) 
		s = size(nf)
	endrep until s(0) eq 0
	set_plot,'ps'
	if n_params() lt 3 then device,/landscape,filename=f $
	else begin
		device,/landscape,/color,filename=f
		tvlct,red,green,blue
	endelse
	tv,x
	x=1
	device,/close
	select_windows
	if n_params() lt 2 then $
	spawn,'lp -o nobanner '+f $
	else spawn,'lp -o nobanner -d'+printer+' '+f
        ENDELSE          ; Not a GIF
endif else begin
	device,/close
	;set_plot,lastdevice
        select_windows
	if( param ne 3) then BEGIN
	 if n_params() lt 2 then $
	  spawn,'lp -o nobanner '+f $
	 else spawn,'lp -o nobanner -d'+printer+' '+f
        END
endelse
return
end
