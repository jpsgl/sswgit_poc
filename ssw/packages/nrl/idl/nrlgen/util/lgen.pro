;+
; $Id: lgen.pro,v 1.1 2006/09/11 21:10:44 nathan Exp $
;
; PURPOSE:
;  Create a findgen like array but with user defined range
;
; CATEGORY:
;  data handling
;
; INPUTS:
;  nbp : number of elements
;  x11,x22 : range min and max of the array. Note that if x11 is a 2
;            elements array then the 2nd elements of x11 is considered
;            to be x22.
;  double : switch to generate double type instead of float
;
; OUTPUTS:
;  return : the findgen like array from x11 to x22 with nbp elements
;
;-

function lgen,nbp,x11,x22,double=double

if n_elements(x11) eq 2 then begin
    x1=x11[0]
    x2=x11[1]
endif else begin
    x1=x11
    x2=x22
endelse

if not keyword_set(double) then l=findgen(nbp)/float(nbp-1)*(x2-x1)+x1 else l=dindgen(nbp)/double(nbp-1)*(x2-x1)+x1

return,l
end
