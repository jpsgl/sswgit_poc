;+
;$Id: webstats.pro,v 1.2 2010/12/09 23:02:04 nathan Exp $
;
; NAME:
;       webstats 
;
; PURPOSE:
;       This pro reads the input access_log file and generates statistics in a report
;
; CATEGORY:
;       UTIL 
;
; CALLING SEQUENCE:
;       webstats,input_filename 
;
; INPUTS:
;       log file with format like:
;   	72.71.213.195 - - [01/Jan/2007:11:48:39 -0500] "GET /secchi2.css HTTP/1.1" 200 2696
;
; KEYWORD PARAMETERS:
;
; OUTPUTS:
;       Writes report broken down by month and total and new downloads 
;
; SIDE EFFECTS:
;       None
;
; RESTRICTIONS:
;
; PROCEDURE: just reads in file, sorts, and counts
;
; EXAMPLE:
;
; MODIFICATION HISTORY:
;
; $Log: webstats.pro,v $
; Revision 1.2  2010/12/09 23:02:04  nathan
; test for logf
;
; Revision 1.1  2007/10/29 22:38:46  nathan
; first draft
;
pro webstats, logf

restore, getenv('SCC_DATA')+'/templates/weblogtemplate.sav'
IF file_exist(logf) THEN fn=logf ELSE fn='/net/ares/opt/local/apache2/logs/ares_access_log'
s=read_ascii(fn, template=weblogtemplate)

;IDL[iapetus]>help,/str,s
;** Structure <a1ffeac>, 8 tags, length=1518296, data length=1518296, refs=1:
;   IPADDRESS       STRING    Array[14599]
;   DATE            STRING    Array[14599]
;   TARGET          STRING    Array[3, 14599]
;   FIELD09         LONG      Array[14599]
;   FIELD10         LONG      Array[14599]

mmm=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']

years =strmid(s.date,8,4)
uniqy =uniq(years,sort(years))
ny=n_elements(uniqy)
yarr=years[uniqy]
help,yarr,s

FOR y=0,ny-1 DO BEGIN
    
    iny=where(years EQ yarr[y])
    
    ipadd=s.ipaddress[iny]
    
    months=strmid(s.date[iny],4,3)

    ; Non-NRL IP addresses

    notnrl=where(strmid(ipadd,0,9) NE '132.250.1', nnotnrl)

    ; Unique visitors

    newvis=uniq(ipadd,sort(ipadd))

    ; Compute numbers; need all in order to scale graphic

    mnots=lonarr(12)
    munqs=lonarr(12)
    
    FOR i=0,11 DO BEGIN
    	mon=mmm[i]
	mnot=where(months[notnrl] EQ mon,nmnot)
	mnots[i]=nmnot
	munq=where(months[newvis] EQ mon,nmunq)
	munqs[i]=nmunq	
    ENDFOR

; Scale graphs
    maxh=max(mnots)
    hbin=maxh/20
    maxu=max(munqs)
    ubin=maxu/20
    help,hbin,ubin
    
; Start report

    ;get_utc,now
    ;yymmdd=utc2yymmdd(now)
    openw,1,getenv('SCC_DATA')+'/statistics/webstats'+yarr[y]+'.txt'
    printf,1,''
    printf,1,'Raw web statistics for '+trim(s.target[1,0])+' in '+yarr[y]
    printf,1,''
    printf,1,'Generated ',systime()
    printf,1,''
    printf,1,'Month   Views from Non-NRL IP addr   Views from New IP addresses'
    printf,1,'======  ===========================  ==========================='
    
    FOR i=0,11 DO BEGIN 
    	
	; Compute graphics
	hgr=''
	htiks=mnots[i]/hbin
	IF htiks GT 0 THEN FOR j=0,htiks-1 DO hgr=hgr+'*'
	ugr=''
	utiks=munqs[i]/ubin
	IF utiks GT 0 THEN FOR j=0,utiks-1 DO ugr=ugr+'*'
	
	printf,1,mmm[i]+' '+strmid(yarr[y],2,2)+'  '+ $
	    	 string(mnots[i],'(I6)')+' '+string(hgr,'(a-20)')+ $
		 string(munqs[i],'(I8)')+' '+string(ugr,'(a-20)')
    ENDFOR
    close,1
ENDFOR	; year
    
end
