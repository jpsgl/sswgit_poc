pro update_fits, li
; $Id: update_fits.pro,v 1.2 2017/12/19 23:03:22 nathan Exp $
;
; generic routine for updating FITS headers
;
;
; $Log: update_fits.pro,v $
; Revision 1.2  2017/12/19 23:03:22  nathan
; current SSW versions
;
; Revision 1.1  2011/04/25 21:01:01  nathan
; specific for incorrect roll on LASCO 2011/04/22-25
;

n=n_elements(li)

for i=0,n-1 do begin
    nm=li[i]
    help,nm
    im=lasco_readfits(nm,h,/no_)
    ;
    ; START modify keyword
    ;
    newval=get_roll_or_xy(h,'roll',/degrees)
    
    fxhmodify,nm,'CROTA1',newval,'manually updated'
    fxhmodify,nm,'CROTA2',newval,'manually updated'
    ;
    ; END modify keyword
    ;
 endfor
 
 end
