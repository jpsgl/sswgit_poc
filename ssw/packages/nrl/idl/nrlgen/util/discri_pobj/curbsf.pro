;+
;NAME:
;     curbsf.pro
;
;PURPOSE:
;      To determine parameters of local curvature.  
;CATEGORY:
; Image processing, mathematics, cosmics, feature extraction 
;CALLING SEQUENCE
;     curbsf,ima_in,ck1,ck2,statk1,statk2,cutsk1,cutsk2
;              
;INPUTS
;   ima_in            ; image_in
;
;OUTPUTS:
;   ck1               ; k1 curbure
;   ck2               ; k2 curbure
;
;PROCEDURE:
;
;HISTORY:
;    written by A.LL. april 1996
; 
; CVSLOG:
;  $Log: curbsf.pro,v $
;  Revision 1.1  2008/05/07 19:09:12  thernis
;  First commit
;
;  Revision 1.2  2002/07/11 07:24:17  arnaud
;  Insertion of the Log in each header
;
;
;-
pro curbsf,ima_in,ck1,ck2,statk1,statk2,cutsk1,cutsk2,blabla=blabla
;
; initial filter
IF  N_PARAMS() LT 3  THEN BEGIN
   print,' define ima_in,ck1 and ck2 please!'
   return
ENDIF ELSE BEGIN
   toim = size(ima_in)
   ndim = toim(0)
   IF (ndim NE 2) THEN BEGIN
     print,' ima_in is not 2 Dim array!'
     return
   ENDIF
   nc = toim(1)
   nl = toim(2)
   ima = float(ima_in)
   ck1 = fltarr(nc,nl)
   ck2 = fltarr(nc,nl)
END
;
; verify if type of image if float
;
statk1 = fltarr(2)
statk2 = fltarr(2)
cutsk1 = fltarr(2)
cutsk2 = fltarr(2)
;
filh = fltarr(nc,nl)
filv = fltarr(nc,nl)
;
;help, /memory
;passage des parametres a la sub fortran
;
;        base = '/home/antoine/lasco/workidl'
;        base =getenv('BASE_CURVATURE')
;        sherl = base+'/spcurbc.so'
rtinitenv
	sherl=getenv('RT_PATH')+path_sep()+getenv('RT_SOSUBPATH')+path_sep()+'spcurbc.'+getenv('RT_SOEXTENSION')
        entry = 'spcurbc'
if n_elements(blabla) ne 0 then print,'entree dans spcurbc'
;
dum = call_external(sherl, entry, $
       nc,nl, $
       ima_in,ck1,ck2,filh,filv, $
       statk1,statk2,cutsk1,cutsk2)
;
return
end
;
