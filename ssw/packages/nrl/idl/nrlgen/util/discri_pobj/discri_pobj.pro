function discri_pobj,imagein,mask_list,$
                     THRES=thres,BIAS=bias,inmask=inmask,blabla=blabla,rate=rate,maskonly=maskonly
;+
; NAME: discri_pobj.pro
;
;
;
; PURPOSE: discrimination of point like objects in lasco-c2 images
;          using surface curvature (source: A&A, "Discrimination of
;          point-like objects in astronomical images using surface
;          curvature", A. Llebaria P. Lamy & P. Malburet).
;
; CATEGORY: image processing, cosmics, stars, enhancement, pipeline
;
;
; CALLING SEQUENCE: imageout=discri_pobj(imagein)
; 
;
; 
; INPUTS:
;
;         imagein : 2 dim array, image to be processed
;
;
; OPTIONAL INPUTS:
;
;       
; INPUT KEYWORDS:
;
;         thres : float, min threshold for the curvature/ regions where the
;                 local curvature is greater than this threshold will be 
;                 processed (defaulted to 0.07) 
;
;         bias :  float, little bias added to the image before calculating its
;                 logarithm in order to limit the effects of weak noise
;                 (defaulted to 0.7) 
;          
;         inmask : binary mask to disable cosmic correction in some
;                  zones
;         blabla : set to display progression messages
;         maskonly : don't apply interpolation stage. If set the
;                    returned value is the binary mask.
;
; OUTPUTS:
;
;          imageout : 2 dim array, processed image
;
;
; OUTPUT KEYWORDS:
;          rate : approximative cosmic rate (the MB are not masked)
;
;
; COMMON BLOCKS:
;
;
;
; SIDE EFFECTS:
;
;
;
; RESTRICTIONS:
;
;
;
; PROCEDURE: cica.pro, curbsf.pro, erode
;
;
;
; EXAMPLE:
;
;
;
; MODIFICATION HISTORY:
;     IDL version, D. VIBERT, april 1997
;     07/02/2002, modif by AT to avoid crash (see the code)
;     25/02/2002, add of the inmask keyword
;     03/04/2002, check the level of cosmic before interpolation
; CVSLOG:
;  $Log: discri_pobj.pro,v $
;  Revision 1.1  2008/05/07 19:09:12  thernis
;  First commit
;
;  Revision 1.2  2002/07/11 07:24:17  arnaud
;  Insertion of the Log in each header
;
;
;-


IF  N_PARAMS() LT 1  THEN  $
   message,'bad number of parameters, usage: discri_pobj(imagein)'
simagein= size(imagein)
ndim=simagein(0)
IF (ndim NE 2) THEN $
  message,'bad parameter, imagein is not 2 dim array'
nc=simagein(1)
nl=simagein(2)
imagein=float(imagein)

if n_elements(thres) eq 0 then $
  thres = .07 $
else $
  thres = float(thres)

if n_elements(bias) eq 0 then $
  bias = .7 $
else $
  bias = float(bias)

if n_elements(blabla) ne 0 then print,'% Discrimination of point-like objets'

hlog= alog10((imagein+bias) > 1e-8)

curbsf,hlog,ck1,ck2,blabla=blabla
k=sqrt(ck1*ck1 + ck2*ck2)

km=where(k ge thres,count)      ; -- 07/02/2002 : add of the count
mask = replicate(1b,nc,nl)
if count gt 0 then mask(km) = 0b ; -- 07/02/2002 : test if count if not null

if n_elements(inmask) gt 0 then mask=mask or byte(abs(byte(inmask > 0 < 1)-1))


mask = erode(mask,replicate(1b,3,3))
mask_list = where(mask eq 0b,nbcosmpix)

; ---- check the level of cosmics before calling the cica function to avoid
;      core dump when there are too much cosmics

rate=float(nbcosmpix)/n_elements(mask)

if keyword_set(maskonly) then begin
    imageout=mask
    return,mask
endif
if rate lt 0.88 then cica,imagein,mask_list,imageout,blabla=blabla else begin
    print
    print,'Too much cosmic impacts: cannot perform correction !'
    imageout=-1
endelse

return,imageout
end

