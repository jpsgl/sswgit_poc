;+
;NAME:
;     cica.pro
;
;PURPOSE:
;      To interpolate local patches in a image 
;CATEGORY:
; Interpolation, cosmics, pipeline    
;CALLING SEQUENCE
;     cica,ima_in,pixel_list,ima_out
;              
;INPUTS
;   ima_in            ; image_in
;   pixel_list        ; map of bad pixels
;
;OUTPUTS:
;   ima_out           ; interpolated image
;
;PROCEDURE:
;
;HISTORY:
;    written by A.LLebaria april 1996
; 
; CVSLOG:
;  $Log: cica.pro,v $
;  Revision 1.1  2008/05/07 19:09:12  thernis
;  First commit
;
;  Revision 1.2  2002/07/11 07:24:17  arnaud
;  Insertion of the Log in each header
;
;
;-
pro cica,ima_in,pixel_list,ima_out,out_range,blabla=blabla
;
   on_error,1
; initial filter
IF  N_PARAMS() LT 3  THEN BEGIN
   message,' Define ima_in,pixel_list and ima_out please!'
   return
ENDIF ELSE BEGIN
   IF  N_PARAMS() LT 4 THEN out_range = -1. ELSE out_range=float(out_range)
   toim = size(ima_in)
   ndim = toim(0)
   IF (ndim NE 2) THEN BEGIN
     print,' ima_in is not 2 Dim array!'
     return
   ENDIF
   nc = toim(1)
   nl = toim(2)
   itype = toim(3)
   CASE 1 OF
     itype eq 0: Message, 'ima_in is not defined!' 
     itype eq 7: Message, 'ima_in can not be a string'
     itype eq 5: Message, 'ima_in can not be a double precision array'
     itype eq 6: Message, 'ima_in can not be a complex array'
     itype eq 8: Message, 'ima_in can not be a structure'
     itype eq 9: Message, 'ima_in can not be a double precision complex array'
     ELSE:
   ENDCASE 
   isize = toim(4)
;   ima = float(ima_in)
   ima_out = fltarr(nc,nl)
   mask = float(ima_in)
END
;
; verify if pixel list remains insaid of input image
;
   infind = min(pixel_list,max=supind)
   if ( infind lt 0 or supind gt isize ) then begin
         message,' Pixel_list is invalid, verify it, please!'
         return
   endif
   on_error,0
;
; mask discarded pixels
;
   infpix = min(mask)
   mask( pixel_list ) = out_range
;
; mapping auxiliary frames
;
   ntab = long( (isize)/3 )
   nls  = (nl+1)/2
   ncs  = (nc+1)/2
   nca  = long( ncs+1 )
   nla  = long( nls+(nls+2)/3+1 )
   temp = fltarr(nc)
   aux  = fltarr(nca,nla)
   ierr = long(0)
;  
;help, /memory
;passage des parametres a la sub fortran
;
;        base = '/usr/users/antoine/lasco/workidl'
;        base= '/usr/users/didier/curvature'
;        base=getenv('BASE_CURVATURE')
;        sherl = base+'/spcicac.so'
rtinitenv
	sherl=getenv('RT_PATH')+path_sep()+getenv('RT_SOSUBPATH')+path_sep()+'spcicac.'+getenv('RT_SOEXTENSION')
        entry = 'spcicac'
        if n_elements(blabla) ne 0 then print,'entree dans spcicac'
;


dum = call_external(sherl, entry, $
       nl,nc,nla,nca, $
       ntab,ierr,out_range, $
       mask, ima_out, temp, aux)
;
case 1 of
  itype eq 1: ima_out = byte(ima_out)
  itype eq 2: ima_out = fix(ima_out)
  itype eq 3: ima_out = long(ima_out)
  ELSE:
endcase
;
return
end
;







