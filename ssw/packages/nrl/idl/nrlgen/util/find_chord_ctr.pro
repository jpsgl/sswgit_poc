;  pro FIND_CHORD_CTR,Img,Xcen,Ycen,Xsig,Ysig,Inten=Inten,Interp=Interp, 
;  pylon=pylon, DEBUG=debug, HDR=hdr
;
;+
; NAME:
;	FIND_CHORD_CTR
;
; PURPOSE:
;	Find the center of the occulting disk by the method of chords
;   HINT: for computing centers of multiple images, use viewlist, CENTERS=
;
; CATEGORY:
;	LASCO Analysis
;
; CALLING SEQUENCE:
;	FIND_CHORD_CTR, Img, Xcen, Ycen
;
; INPUT PARAMETERS:
;	Img:	The 2-D image
;
; KEYWORDS:
;	INTEN:	Threshold intensity (Default = 1000)
;	INTERP:	Flag to perform interpolation (Should be set)
;	PYLON:	Flag to avoid pylon (should be used for C2/C3)
;	HDR:	Use header to determine starting center
;
; OUTPUT PARAMETERS:
;	Xcen:	The value of the center in X
;	Ycen:	The value of the center in Y
;
; OPTIONAL OUTPUT PARAMETERS: 	None
;
; COMMON BLOCKS:
;	Used for debug
;
; SIDE EFFECTS:
;	Draws chords and computed center on current window
;
; RESTRICTIONS:
;
; PROCEDURE:
;	Finds the occulter boundary by finding the row and column 
;	numbers where the image exceeds a threshold intensity.
;
;	Uses the center of the image as the initial center of the occulter
;
;	If the keyword, Inten, is not present, the default is 1000
;
;	For each row, the column numbers for the left and right boundaries 
;	of the chord are computed.  If the optional input parameter, Interp, 
;	is present then linear interpolation is used to find a fractional 
;	column number.  Otherwise the pixel number of the first pixel 
;	exceeding the threshold is used.  The column center is then the 
;	midpoint of the chord.  The process is stopped when two boundaries, 
;	separated by at least 50 pixels, cannot be found. Then the average 
;	of all rows is computed to find the average column number.
;
;	The process is repeated for each column to find the average row number.
;
; MODIFICATION HISTORY:		Written RA Howard, NRL, 20 December 1995
;	V1	RAH	Initial Release
;	V2	RAH	16 Feb 96, Added rejection of outliers to average
;	V3	RAH	18 Mar 96, Corrected 1 pixel error in right/top edge 
;	nbr, 1/30/01 - Fix PYLON keyword; change Debug output; plots on open window
;
;	@(#)find_chord_ctr.pro	1.3 02/01/10 LASCO IDL LIBRARY
;-
;
;  
pro FIND_COL_BOUNDARIES,img,xcen,row,inten,w1,w2,Interp

    w = where(img(xcen:*,row) gt inten)  
    sw = size(w)  
    if (sw(0) eq 0) then w2 = -1 else begin
;
; interpolate to find right edge
;
       w2 = w(0)+xcen		; column of 1st pixel > inten
       if (interp eq 1) then begin
          i1=img(w2-1,row)		; intensity of 1st pixel < inten
          i2=img(w2,row)		; intensity of 1st pixel > inten
          if (i2 ne i1) then w2 = w2-1+(inten-i1)/(i2-i1)
       endif
    endelse
    w = where(img(0:xcen,row) gt inten)  
    sw = size(w)  
    if (sw(0) eq 0) then w1 = -1 else begin
;
; interpolate to find left edge
;
       w1 = w(sw(1)-1)		; column of 1st pixel > inten
       if (interp eq 1) then begin
          i1=img(w1,row)		; intensity of 1st pixel > inten
          i2=img(w1+1,row)		; intensity of 1st pixel < inten
          if (i2 ne i1) then w1 = w1+(inten-i1)/(i2-i1)
       endif
    endelse
return
end

pro FIND_ROW_BOUNDARIES,img,ycen,col,inten,w1,w2,interp
;
    w = where(img(col,ycen:*) gt inten)  
    sw = size(w)  
    if (sw(0) eq 0) then w2 = -1 else begin
;
; interpolate to find upper edge
;
       w2 = w(0)+ycen		; column of 1st pixel > inten
       if (interp eq 1) then begin
          i1=img(col,w2-1)		; intensity of 1st pixel < inten
          i2=img(col,w2)		; intensity of 1st pixel > inten
          if (i2 ne i1) then w2 = w2-1+(inten-i1)/(i2-i1)
       endif
    endelse
    w = where(img(col,0:ycen) gt inten)  
    sw = size(w)  
    if (sw(0) eq 0) then w1 = -1 else begin
;
; interpolate to find lower edge
;
       w1 = w(sw(1)-1)		; column of 1st pixel > inten
       if (interp eq 1) then begin
          i1=img(col,w1)		; intensity of 1st pixel > inten
          i2=img(col,w1+1)		; intensity of 1st pixel < inten
          if (i2 ne i1) then w1 = w1+(inten-i1)/(i2-i1)
       endif
    endelse
RETURN
END

;***************************************************************************
;***************************************************************************

pro FIND_CHORD_CTR,Img,Xcen,Ycen,Xsig,Ysig,Inten=Inten,Interp=Interp,pylon=pylon, DEBUG=debug, HDR=hdr

COMMON find_ctr_debug,dbg,ludebug

IF keyword_set(DEBUG) THEN BEGIN
   dbg=1 
   print,'Opening ./debug_find_chord_ctr.log'
   openw,ludebug,'./debug_find_chord_ctr.log',/get_lun
ENDIF ELSE dbg=0
   help,dbg,ludebug
sz = size(dbg)
;IF (sz(0) EQ 0) THEN dbg=0
sz = size(img)  
IF keyword_set(HDR) THEN BEGIN
   c=occltr_cntr(hdr)
   xcen = c[0] & ycen = c[1]
ENDIF ELSE BEGIN
   xcen = fix(sz(1)/2)
   ycen = fix(sz(2)/2)
   ;ycen=525
ENDELSE
IF ( NOT KEYWORD_SET(inten)) THEN inten=1000
inten=float(inten)
IF KEYWORD_SET(Interp) THEN BEGIN
   print,'Interpolation will be used'
ENDIF ELSE BEGIN
   print,'No Interpolation will be used'
   interp = 0
ENDELSE
help,inten
;
;        find column center
; 
row = ycen
nctr=0  
;
;    first find column center for rows > ycen'
;

repeat begin  
    find_col_boundaries,img,xcen,row,inten,w1,w2,interp
IF dbg THEN print,'w1',w1,'   w2',w2
    IF (dbg EQ 1) THEN BEGIN
	printf,ludebug,'    first find column center for rows > ycen'
       IF ((w1 GE 0) AND (w2 GE 0)) THEN BEGIN
          aw1 = float(w1)
          aw2 = float(w2)
          ar  = float(row)
          ai1 = img(fix(w1),fix(row))
          ai2 = img(fix(w2),fix(row))
          ai3 = img(fix(w1)+1,fix(row))
          ai4 = img(fix(w2)+1,fix(row))
          c = 0.5*(aw1+aw2)
          PRINTF,ludebug,c,ar,aw1,ar,aw2,ar,ai1,ai3,ai2,ai4,format='(6F9.2,4I6)'
          PRINT,c,ar,aw1,ar,aw2,ar,ai1,ai3,ai2,ai4,format='(6F9.2,4I6)'
	  plots,[w1,w2],[row,row],/dev

       ENDIF
    ENDIF
;
;   average left and right chord boundaries to get column average
;   force chord length to be at least 50 pixels
;
    if ((w1 ge (w2-50)) or (w1 eq -1) or (w2 eq -1)) then row=sz(2) else begin  
       avg = 0.5*(w1+w2)  
       if (nctr eq 0) then xctr=[avg] else xctr=[xctr,avg]  
       nctr = nctr+1 
       row = row+1  
    endelse  
endrep until (row eq sz(2))  
;
;    now find column center for rows < ycen
;
IF NOT(keyword_set(PYLON))  THEN BEGIN
row = ycen-1
repeat begin  
    find_col_boundaries,img,xcen,row,inten,w1,w2,interp
    IF (dbg EQ 1) THEN BEGIN
	printf,ludebug,'    now find column center for rows < ycen'
       IF ((w1 GE 0) AND (w2 GE 0)) THEN BEGIN
          aw1 = float(w1)
          aw2 = float(w2)
          ar  = float(row)
          ai1 = img(fix(w1),fix(row))
          ai2 = img(fix(w2),fix(row))
          ai3 = img(fix(w1)+1,fix(row))
          ai4 = img(fix(w2)+1,fix(row))
          c = 0.5*(aw1+aw2)
          PRINTF,ludebug,c,ar,aw1,ar,aw2,ar,ai1,ai3,ai2,ai4,format='(6F9.2,4I6)'
	  plots,[w1,w2],[row,row],/dev,color=240
       ENDIF
    ENDIF
;
;   average left and right chord boundaries to get column average
;   force chord length to be at least 50 pixels
;
    if ((w1 ge (w2-50)) or (w1 eq -1) or (w2 eq -1)) then row=1 else begin  
       avg = 0.5*(w1+w2)  
       if (nctr eq 0) then xctr=[avg] else xctr=[xctr,avg]  
       nctr = nctr+1 
       row = row-1  
    endelse  
endrep until (row eq 1) 
ENDIF		; check for keyword pylon
if (nctr le 1) then begin		; return if error
    xctr = -1
    yctr = -1
    xsig = 0
    ysig = 0
    return
endif
sd=stdev(xctr,mn) 
xcen=mn 
;print,'Col ctr = ',xcen,'  Std dev = ',sd,' Num pts = ',nctr
;
;   reject outliers
;
nrep = 0
repeat begin
   w = where(abs(xctr-mn) lt 3*sd)
   siz = size(w)
   if (siz(0) eq 0) then begin		; return if error
      xctr = -1
      yctr = -1
      xsig = 0
      ysig = 0
      return
   endif
   xctr = xctr(w)
   nctr=n_elements(xctr)
   if (nctr gt 1) then sd= stdev(xctr,mn) else sd=0
   xcen=mn 
   print,'Col ctr = ',xcen,'  Std dev = ',sd,' Num pts = ',nctr
   nrep = nrep+1
endrep until ((nrep eq 10) or (sd lt 0.3))
Xsig = sd
;
;        find row center
; 
col = fix(xcen)
nctr=0  
;
;    first find row center for cols > xcen
;
repeat begin  
    find_row_boundaries,img,ycen,col,inten,w1,w2,interp
    IF (dbg EQ 1) THEN BEGIN
	printf,ludebug,'    first find row center for cols > xcen'
       IF ((w1 GE 0) AND (w2 GE 0)) THEN BEGIN
          aw1 = float(w1)
          aw2 = float(w2)
          ac  = float(col)
          ai1 = img(fix(col),fix(w1))
          ai2 = img(fix(col),fix(w2))
          ai3 = img(fix(col),fix(w1)+1)
          ai4 = img(fix(col),fix(w2)+1)
          r = 0.5*(aw1+aw2)
          PRINTF,ludebug,ac,r,aw1,ar,aw2,ar,ai1,ai3,ai2,ai4,format='(6F9.2,4I6)'
	  plots,[col,col],[w1,w2],/dev,color=220
       ENDIF
    ENDIF
;
;   average upper and lower chord boundaries to get row average
;   force chord length to be at least 50 pixels
;
    if ((w1 ge (w2-50)) or (w1 eq -1) or (w2 eq -1)) then col=sz(1) else begin  
       avg = 0.5*(w1+w2)  
       if (nctr eq 0) then yctr=[avg] else yctr=[yctr,avg]  
       nctr = nctr+1 
       col = col+1  
    endelse  
endrep until (col eq sz(1))  
col = fix(xcen)-1
;
;    now find row center for cols < xcen
;
IF NOT(keyword_set(PYLON))  THEN BEGIN
repeat begin  
    find_row_boundaries,img,ycen,col,inten,w1,w2,interp
    IF (dbg EQ 1) THEN BEGIN
	printf,ludebug,'    now find row center for cols < xcen'
       IF ((w1 GE 0) AND (w2 GE 0)) THEN BEGIN
          aw1 = float(w1)
          aw2 = float(w2)
          ac  = float(col)
          ai1 = img(fix(col),fix(w1))
          ai2 = img(fix(col),fix(w2))
          ai3 = img(fix(col),fix(w1)+1)
          ai4 = img(fix(col),fix(w2)+1)
          r = 0.5*(aw1+aw2)
          PRINTF,ludebug,ac,r,aw1,ar,aw2,ar,ai1,ai3,ai2,ai4,format='(6F9.2,4I6)'
	  plots,[col,col],[w1,w2],/dev,color=200
       ENDIF
    ENDIF
;
;   average upper and lower chord boundaries to get row average
;   force chord length to be at least 50 pixels
;
    if ((w1 ge (w2-50)) or (w1 eq -1) or (w2 eq -1)) then col=1 else begin  
       avg = 0.5*(w1+w2)  
       if (nctr eq 0) then yctr=[avg] else yctr=[yctr,avg]  
       nctr = nctr+1 
       col = col-1  
    endelse  
endrep until (col eq 1) 
ENDIF		; check for keyword pylon
if (nctr le 1) then begin		; return if error
   xctr = -1
   yctr = -1
      xsig = 0
      ysig = 0
   return
endif
sd=stdev(yctr,mn) 
ycen=mn 
;
;   reject outliers
;
nrep = 0
repeat begin
   w = where(abs(yctr-mn) lt 3*sd)
   siz = size(w)
   if (siz(0) eq 0) then begin		; return if error
      xctr = -1
      yctr = -1
      xsig = 0
      ysig = 0
      return
   endif
   yctr = yctr(w)
   nctr=n_elements(yctr)
   if (nctr gt 1) then sd=stdev(yctr,mn) else sd=0
   ycen=mn 
   print,'Row ctr = ',ycen,'  Std dev = ',sd,' Num pts = ',nctr
   nrep = nrep+1
endrep until ((nrep eq 10) or (sd lt 0.3))

plots,[xcen-40,xcen+40],[ycen,ycen],color=128, /dev
plots,[xcen,xcen],[ycen-40,ycen+40],color=128, /dev
tvcircle,120,xcen,ycen,color=100
Ysig = sd

IF keyword_set(DEBUG) THEN BEGIN
   close,ludebug
   free_lun,ludebug
ENDIF
  
end  
