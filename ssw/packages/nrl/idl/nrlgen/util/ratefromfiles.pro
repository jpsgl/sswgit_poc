FUNCTION ratefromfiles, li
;+
; $Id: ratefromfiles.pro,v 1.2 2010/12/09 22:59:05 nathan Exp $
;
; PROJECT: STEREO/SECCHI
;
; NAME:
;	ratefromfiles
;
; PURPOSE:
;	compute a data rate based on input filename sizes and timestamps
;
; CATEGORY: util file system os downlink
;
; CALLING SEQUENCE:
;
;	rate = ratefromfiles(list_of_files)
;
; INPUTS:
;	list_of_files	STRARR	list of files with paths
;
; KEYWORD PARAMETERS:
;
; OUTPUTS:
;	This function returns a rate in bytes per second
;
; PROCEDURE:
;
; EXAMPLE:
;
; MODIFICATION HISTORY:
; $Log: ratefromfiles.pro,v $
; Revision 1.2  2010/12/09 22:59:05  nathan
; doc correction
;
; Revision 1.1  2009/08/14 22:40:29  nathan
; born
;
;-

n=n_elements(li)
v=fltarr(n)
t=lonarr(n)
openr,1,li[0]
s=fstat(1)
close,1
v[0]=s.size
t[0]=s.mtime


for i=1,n-1 do begin
    openr,1,li[i]
    s=fstat(1)
    close,1
    v[i]=v[i-1]+s.size
    t[i]=s.mtime
endfor

srt=sort(t)
v=v[srt]
t=t[srt]
t=t-t[0]

dt=t-shift(t,1)

cutoff=400  ;seconds
gaps=where(dt gt cutoff,ng)

p=1
i0=0
while gaps[i0]-p LT 2 do begin
    p=gaps[i0]
    i0=i0+1
endwhile

bm=linfit(t[p: gaps[i0]-1], v[p: gaps[i0] -1])

bytps=bm[1]

i0=i0+1
For i=i0,ng-1 do begin
    x0=gaps[i]
    IF i EQ ng-1 THEN x1 = n-1 ELSE x1=gaps[i+1]-1
    IF x1-x0 GT 1 THEN BEGIN
    	plot,t[x0:x1], v[x0:x1],psym=1
    	bm=linfit(t[x0:x1], v[x0:x1])
	oplot,[t[x0],t[x1]],[bm[0]+bm[1]*t[x0],bm[0]+bm[1]*t[x1]]
	print,bm
	wait,1
	bytps=[bytps,bm[1]]
    ENDIF
ENDFOR

nb=n_elements(bytps)
plot,bytps
mdn=median(bytps)
oplot,[0,nb-1],[mdn,mdn]

return,mdn
END
