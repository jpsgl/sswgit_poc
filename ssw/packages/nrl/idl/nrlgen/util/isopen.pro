;+
; $Id: isopen.pro,v 1.2 2011/02/10 17:41:42 nathan Exp $
; NAME:
;       isopen
; PURPOSE:
;       check if a file unit is open
; CALLING SEQUENCE:
;       chk=is_open(filein)
; INPUTS:
;       file = file to check
; OUTPUTS:
;       chk = 1 (open) or 0 (closed)
;   	filein	= name of open file specified by unit=
;   	unit	= LUN of open file specified by filein
; KEYWORDS:
;       UNIT = file unit number to check, if input
;   	--or--	LUN of filein
; PROCEDURE:
;       uses fstat (now uses HELP)
; HISTORY:
; $Log: isopen.pro,v $
; Revision 1.2  2011/02/10 17:41:42  nathan
; fix erroneous set of unit
;
; Revision 1.1  2011/02/08 20:03:14  nathan
; re-wrote is_open.pro to accept file OR unit as input
;
;       Written Jun'94 (DMZ,ARC)
;       Modified, Zarro (SM&A/GSFC), 8 Oct 1999
;        -- more accurate use of grep
;-

function isopen,filein,unit=unit

chk=0b
IF keyword_set(unit) THEN unitin=unit ELSE unitin=-1
;if is_blank(file) then return,chk

ver5=idl_release(lower=5,/inc)
if ver5 then begin
 call_procedure,'help',/files,out=out
 
 out=strtrim(out,2)
 full_name=chklog(filein,/pre)
 nout=n_elements(out)
 if (nout gt 0) and (out(0) ne '') then $
  for i=0,nout-1 do begin
    temp=strcompress(out(i))
    temp=str2arr(temp,delim=' ')
    IF is_number(temp[0]) THEN uniti=fix(temp[0]) ELSE uniti=0
    nte=n_elements(temp)
    filein=temp[nte-1]
    IF uniti EQ unitin OR full_name EQ filein then BEGIN
    	unit=uniti
	return,1b
    ENDIF
  endfor
 
endif else begin
 if unitin GT 0 then begin
  status=fstat(unitin)
  if strpos(strlowcase(file),strlowcase(status(0).name)) ge 0 then chk=1b
 endif
endelse

return,chk 
end
