function intersection, ar1, ar2, dumm=dumm, IND1=ind1, IND2=ind2, tolerance=tolerance, $
    	OVERLAP=overlap
;+
; $Id: intersection.pro,v 1.4 2013/03/26 18:33:00 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : intersection
;               
; Purpose   : returns the intersection of 2 input arrays
;               
; Explanation: 
;               
; Use       : IDL> 
;    
; Inputs    : 2 arrays of same type; must be number or Date-string
;               
; Outputs   : 1 array; if no intersection, then scalar -1
;
; Keywords  : 
;   	IND1 = returns indices of intersection in ar1
;   	IND2 = returns indices of intersection in ar2
;
;   	TOLERANCE=  set to bin size to use (base 10)
;   	If input is string time, TOLERANCE='min', 'sec', or 'hour'
;   	/OVERLAP    assumes input is sorted, IND1 and IND2 are indices of range where there is overlap
;   	    	    and output is ar1[ind1]
;
; Category    : array, math
;               
; Prev. Hist. : None.
;
; Written     : N.Rich, NRL/Interferometrics, May 2007
;               
; $Log: intersection.pro,v $
; Revision 1.4  2013/03/26 18:33:00  nathan
; change /OVERLAP output
;
; Revision 1.3  2013/03/26 17:39:13  nathan
; add /OVERLAP
;
; Revision 1.2  2010/11/04 18:21:25  nathan
; added IND1=, IND2=, TOLERANCE=
;
; Revision 1.1  2007/05/10 22:27:27  nathan
; used in getsccarchiveinfo.pro
;
;-            

n1=n_elements(ar1)
n2=n_elements(ar2)

ni1=0
IF keyword_set(OVERLAP) THEN BEGIN
    mn1=min(ar1,max=mx1)
    mn2=min(ar2,max=mx2)
    IF mn2 LT mx1 and mx2 GT mn1 THEN BEGIN
    	ind1=where(ar1 GE mn2,ni1)
	ind2=where(ar2 LE mx1,ni2)
    ENDIF 
    IF ni1 GT 0 THEN $
    	return, ar1[ind1] $
    ELSE BEGIN
    	message,'No overlap found.',/info
	return,-1
    ENDELSE
ENDIF

IF keyword_set(TOLERANCE) THEN BEGIN
    IF datatype(ar1) EQ 'STR' THEN BEGIN
    ; assume is type date-time string
    ; and remove subseconds
    	print,'Truncating ar1...'
    	FOR i=0l,n1-1 DO BEGIN
	    IF tolerance EQ 'sec' THEN dot=strpos(ar1[i],'.') ELSE $
	    IF tolerance EQ 'min' THEN dot=rstrpos(ar1[i],':') ELSE $
	    IF tolerance EQ 'hour' THEN dot=strpos(ar1[i],':')
	    IF dot GT 0 THEN ar1[i]=strmid(ar1[i],0,dot)
	ENDFOR
    	print,'Truncating ar2...'
    	FOR i=0l,n2-1 DO BEGIN
	    IF tolerance EQ 'sec' THEN dot=strpos(ar2[i],'.') ELSE $
	    IF tolerance EQ 'min' THEN dot=rstrpos(ar2[i],':') ELSE $
	    IF tolerance EQ 'hour' THEN dot=strpos(ar2[i],':')
	    IF dot GT 0 THEN ar2[i]=strmid(ar2[i],0,dot)
	ENDFOR
    ENDIF ELSE BEGIN
    	ar1=long(ar1/float(tolerance))
	ar2=long(ar2/float(tolerance))
    ENDELSE
    print,'Tolerance rebinning done.'
ENDIF

IF n1 GT n2 THEN BEGIN
    biga=ar1
    smaa=ar2
ENDIF ELSE BEGIN
	biga=ar2
	smaa=ar1
ENDELSE
n=n1<n2

outa=ar1[0]
first=1
FOR i=0,n-1 DO BEGIN
	w=where(biga EQ smaa[i],nw)
	
	IF nw GT 0 THEN BEGIN
	    outa=[outa,smaa[i]]
	    IF (first) THEN BEGIN
	    	indbig=w[0]
		indsma=i
	    ENDIF ELSE BEGIN
	    	indbig=[indbig,w[0]]
	    	indsma=[indsma,i]
	    ENDELSE
	    first=0
	ENDIF
ENDFOR
nout=n_elements(outa)
if nout GT 1 THEN outa=outa[1:nout-1] ELSE outa=-1
IF nout LE 1 THEN BEGIN
    message,'No intersection found.',/info
    return,outa
ENDIF
IF n1 GT n2 THEN BEGIN
    ind1=indbig
    ind2=indsma
ENDIF ELSE BEGIN
    ind1=indsma
    ind2=indbig
ENDELSE

return,outa

END
