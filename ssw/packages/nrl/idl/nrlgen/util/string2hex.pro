FUNCTION string2hex,input,forcesize
;
; Project   : SoloHI
;                   
; Name      : string2hex
;               
; Purpose   : casts a string to sequence of 2-dig hex byte equivalent with, optionally, a
;             specified number of total chars to provide trailing zeros (null bytes)
;               
; Explanation: For SCOS string command arguments
;               
; Use       : IDL> intstr=int2str(intvalue,[numdigits])
;    
; Inputs    : input: string to cast
;             optional forcesize: force this size by adding trailing zeroes
;               
; Outputs   : a string 
;
; Keywords  : (none)
;               
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : util
;               
; Prev. Hist. : None.
;
; Written     : N.Rich, NRL, 2016
;               

nin=strlen(input)

str=arr2str(to_hex(byte(input)),delim='')

  ; pad with leading zeroes, if asked
if (n_elements(forcesize) ne 0) then $
    for ilen=strlen(str),2*forcesize-1 do str=str+'0'

  return,str

END
