FUNCTION FILE_DATE_MOD, File,DATE_ONLY=date_only, UTC=utc, SIZE=fsize
;+
; $Id: file_date_mod.pro,v 1.3 2018/04/16 21:46:07 nathan Exp $
; NAME:
;	FILE_DATE_MOD
;
; PURPOSE:
;	This function returns the UT modification time
;
; CATEGORY:
;	LASCO UTILITY
;
; CALLING SEQUENCE:
;
;	Result = FILE_DATE_MOD(File)
;
; INPUTS:
;	File:	The name of the file to be processed
;
; KEYWORD PARAMETERS:
;	DATE_ONLY	If set, return date only as a string
; 	UTC		If set, returns GMT/UTC time (Redundant)
;   	SIZE=var    	Will return file size from ls -l
;
; OUTPUTS:
;	This function returns the date the file was modified as TAI
;
; PROCEDURE:
;	The procedure spawns a command to the OS to get the modification date
;
; EXAMPLE:
;	date = FILE_DATE_MOD(sample_file.dat,/date_only)
;		returns the date as a string such as 1996/10/15
;
; MODIFICATION HISTORY:
; $Log: file_date_mod.pro,v $
; Revision 1.3  2018/04/16 21:46:07  nathan
; was broken on linux so replaced all with idl fstat.pro
;
; Revision 1.2  2017/06/16 17:41:38  nathan
; 15 Jun 2017, A. Vourlidas - replaced ls with stat call to simplify code
;
; Revision 1.1  2008/04/17 20:44:50  nathan
; moved from lasco/idl/util
;
; 	10 Feb 1999 - Nathan Rich, NRL
;			fixed year rollover problem  e.g Dec 21 was producing a
; 			file mod of Dec 21, 1999 since the ls command produced
;			just Dec 21 and not Dec 21 1998
;
; 			From the ls man page
; 			If  the  time  of last modification is greater than six
; 			months ago, it is shown in the format `month date year'
; 			for the POSIXlocale.  When the LC_TIME locale category
; 			is not set to the POSIX locale, a different format of
; 			the time field may  be  used.  Files modified within
; 			six months show 'month date time'.
;
; 			Still not sure if this is 6 months to the day or six
; 			months by months.  We will try six months by months - DW
;
;  	7 Oct 1999 - Fixed error in determining and using mon_index - NBR
;    	Jan 2000 - Correct definition of mm and dd; fix year determination (again) - NBR
; 	14 Aug 2000, NBR - Added /SH to spawn call
;  	2 Apr 2001, NBR - Return date-only in ECS format
; 	30 Nov 2001, NBR - Add mods for Windows compatibility for GSV
; 	21 Jun 2002, NBR - Use /bin/ls instead of /usr/ucb/ls and change method of parsing string
;  	9 Jun 2005, NBR - Add UTC option
;	5 Oct 2006,	F. Auch�re - replace close, file1 by free_lun, file1 (frees the allocated unit)
;   7 Oct 2006, F. Auchere - more robust way to extract date from spawn output under windows

;	04/09/07 @(#)file_date_mod.pro	1.12 LASCO IDL LIBRARY
;-

IF ~(file_exist(File)) THEN BEGIN
    PRINT, '%%FILE_DATE_MOD: Error file not accessible: ', file
    RETURN, -1
ENDIF

openr,9,File
s=fstat(9)
close,9

fsize=s.size
ut = unixtai2utc(s.mtime)
IF keyword_set(DATE_ONLY) THEN $
return, utc2str(ut, /DATE_ONLY, /ECS) ELSE $
return, utc2tai(ut)
 
END
