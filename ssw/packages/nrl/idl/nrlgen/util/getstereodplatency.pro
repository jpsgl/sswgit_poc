function getstereodplatency,dir,versions=ver0, NOPLOT=noplot
; $Id: getstereodplatency.pro,v 1.5 2013/03/26 17:38:16 nathan Exp $
;
; Project     : STEREO SECCHI
;                   
; Name        : 
;               
; Purpose     : determine latency of _???_6_02.* files based on filename and mod date of file
;               
; Explanation : 
;               
; Use         : 
;    
; Inputs      : dir 	Directory of files to be tested
;               
; Outputs     : FLTARR(nfiles,2)	[*,0] = DOY of observation, 
;   	    	    	    	    	[*,1] = File timestamp in Hours after UT midnight of DOY of observation
;
; Keywords :	VERSIONS = array	Version numbers of files read returned
;   	    	/NOPLOT     Set to not put up plot of day vs. latency - 24 hr
;               
; Calls from LASCO : file_date_mod.pro
;
; Common      : 
;               
; Restrictions: assumes filename time is UT
;   	    	current year only
;		STEREO/APL naming convention
;               
; Side effects: 
;               
; Category    : system, file access, time, telemetry
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL/I2, Dec 2006
;               
; Modified    :
;
; $Log: getstereodplatency.pro,v $
; Revision 1.5  2013/03/26 17:38:16  nathan
; clarify output
;
; Revision 1.4  2010/12/09 22:57:27  nathan
; update ver numbers
;
; Revision 1.3  2007/08/28 21:08:08  nathan
; add /noplot
;
; Revision 1.2  2007/05/22 18:56:26  nathan
; optimize for level-0 telemetry
;
; Revision 1.1  2007/04/09 19:36:35  nathan
; generic routine for determining file latency
;

cd,dir
get_utc,strnow,/ecs
yyyy=strmid(strnow,0,4)
f01=file_search('*_'+yyyy+'_???_6_02.*')
nf01=n_elements(f01)
hlate=fltarr(nf01,2)  	; [*,1] is hours first file available after UT midnight
  			; [*,0] is DOY
ver0 =intarr(nf01)	; will contain version of first nonzero file
FOR i=0,nf01-1 DO BEGIN
    fname=f01[i]
    verp=strpos(fname,'02.')
    doyp=strpos(fname,yyyy+'_')
    doy=fix(strmid(fname,doyp+5,3))
    hlate[i,0]=doy
    dtai=utc2tai(doy2utc(doy+1,fix(yyyy)))
    ver=2
    print,'Statting ',fname
    ftai=file_date_mod(fname,/utc, size=fsize)
    IF fsize LE 0 OR ftai-dtai LT 0 THEN BEGIN
	strput,fname,'03',verp
	print,'Statting ',fname
	ftai=file_date_mod(fname,/utc, size=fsize)
	ver=3
	IF fsize LE 0 OR ftai-dtai LT 0 THEN BEGIN
		strput,fname,'04',verp
		print,'Statting ',fname
		ftai=file_date_mod(fname,/utc, size=fsize)
		ver=4
	ENDIF
    ENDIF
    hlate[i,1]=((ftai-dtai)/3600.)>0
    ver0[i]=ver
    ;stop
ENDFOR

!x.style=3
!y.style=3
window
plot,hlate[*,0],hlate[*,1]-24,title='UT time available on day 2',psym=4
return,hlate
end
