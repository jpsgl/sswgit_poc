FUNCTION hex2string,input,forcesize
; $Id: hex2string.pro,v 1.1 2018/06/25 20:29:14 nathan Exp $
; Project   : SoloHI
;                   
; Name      : hex2string
;               
; Purpose   : casts a 2-dig hex byte string to ASCII equivalent 
;               
; Explanation: For SCOS string command arguments
;               
; Use       : IDL> ascii = string2hex('72616D2F6C643274726272682E627366')
;    
; Inputs    : input: string
;               
; Outputs   : a string 
;
; Keywords  : (none)
;               
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : util
;               
; Prev. Hist. : None.
;
; $Log: hex2string.pro,v $
; Revision 1.1  2018/06/25 20:29:14  nathan
; complement to string2hex.pro
;
; Written     : N.Rich, NRL, 2016
;               

nin=strlen(input)/2
arr=strarr(nin)
FOR i=0,nin-1 DO BEGIN
    x = strmid(input,i*2,2)
    hex2dec,x,d,/quiet
    arr[i]=string(byte(d))
ENDFOR

return,trim(arr2str(arr,delim=''))

END
