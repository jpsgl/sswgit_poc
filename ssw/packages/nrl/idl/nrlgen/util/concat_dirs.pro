;; $Id: concat_dirs.pro,v 1.2 2018/01/24 17:59:46 nathan Exp $
;; ----------------------------------------------------------------------
;;
;; concat_dirs - uses concat_dir to concatenate a series of
;; directory names into a path. For example:
;;   IDL> path = concat_dirs(['$HINODE','xrt','quicklook','2007'])
;; 
;; $Log: concat_dirs.pro,v $
;; Revision 1.2  2018/01/24 17:59:46  nathan
;; update comments
;;
;; Revision 1.1  2018/01/24 17:59:06  nathan
;; copy of eis_concat_dirs.pro (for general use)
;;
;; ----------------------------------------------------------------------

function concat_dirs,dirs

if n_elements(dirs) eq 0 then begin
  message,"Input a list of directories to concatenate together",/info
  message,"USAGE: outdir = concat_dirs([dir1,dir2,dir3]",/info
  return,''
endif

output = dirs[0]
for n=1,n_elements(dirs)-1 do output = concat_dir(output,dirs(n))

return,output
end
