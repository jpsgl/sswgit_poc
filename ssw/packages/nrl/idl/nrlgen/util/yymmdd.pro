function yymmdd,ff
;+
; $Id: yymmdd.pro,v 1.1 2008/02/15 22:49:57 nathan Exp $
; NAME:
;	YYMMDD
; PURPOSE:
;       YYMMDD returns a list of "numerical" directories (e.g., '970803/')
;       It rejects other directories (e.g. assorted/, bad_dates/)
; CALLING SEQUENCE:
;	yymmdd, ff
; INPUTS:
;       ff -- a list of directories
; OUTPUTS:
;       Returns only "numerical" directories  
; SIDE EFFECTS:
;
; RESTRICTIONS:
;       Uses  the CDS routine VALID_NUM to check for numerical directories
; PROCEDURE:
;
; MODIFICATION HISTORY:
;
; $Log: yymmdd.pro,v $
; Revision 1.1  2008/02/15 22:49:57  nathan
; moved from lasco/idl/display because called by scclister.pro
;
;       Written by:     A. Vourlidas  9/12/97
;-

  n = n_elements(ff)
  tmp= LONARR(n)                           ;Temporary array 
  FOR i=0,n-1 DO tmp(i) = VALID_NUM(ff(i)) ;Check for valid  directories

  RETURN,ff(WHERE(tmp GT 0))
end
