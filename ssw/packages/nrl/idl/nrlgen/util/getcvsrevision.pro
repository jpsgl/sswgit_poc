;+
; PROJECT:
;  SOHO-LASCO
;
; NAME:
;  GETCVSREVISION
;
; PURPOSE:
;  extract only the usefull part of the CVS revision number
;
; CATEGORY:
;  data handling
;
; DESCRIPTION: 
;
; CALLING SEQUENCE:
;  revision=getcvsrevision('$Revision: 1.1 $'[,/rmonedot])
;
; INPUTS:
;  rev : revision number string
;
; OUTPUTS:
;  return : only the revision number 
;
; INPUT KEYWORDS:
;  rmonedot : remove the un-usefull 1.
;
; OUTPUT KEYWORDS:
;
; CVSLOG:
;  $Id: getcvsrevision.pro,v 1.1 2006/09/11 21:10:44 nathan Exp $
;
; HISTORY:
;  coded by A.Thernisien on 29/08/2002
;
;-
function getcvsrevision,rev,rmonedot=rmonedot

if n_elements(rmonedot) eq 0 then $
  r=strmid(rev,11,strlen(rev)-1) else $
  r=strmid(rev,13,strlen(rev)-1)  ; -- remove the unsefull 1. 

r=strmid(r,0,strlen(r)-2)

return,r
end
