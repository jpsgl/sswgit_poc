FUNCTION READLIST, list, num, TRIMflag=trimflag, SPLIT=split, OMIT_COMMENTS=omit_comments
; $Id: readlist.pro,v 1.5 2018/04/16 21:48:04 nathan Exp $ 
; PURPOSE:
;  Open text file and read in each line as a string and return a string array
;
; INPUTS:
;  list		STRING	Name of file to read
;
; Keywords:
;   TRIMFLAG= 	0 (removes trailing and leading whitespace), 1 (removes leading whitespace), 2 (removes trailing whitespace)
;   SPLIT=  Char which is used as delimiter to split each row into substrings 
;   	    [default is ' ,']; User is prompted to confirm or change Nfields
;   /OMIT_COMMENTS	Omit rows which begin with # or ;
;
; OUTPUTS:
;  Result of function is a 1xNrows STRARR; if /SPLIT then dim is NfieldsxNrows
;  num		If passed, contains number of rows in array
;
; SCCS variables for IDL use
; 
; @(#)readlist.pro	1.2 03/13/01 :NRL Solar Physics
;
; $Log: readlist.pro,v $
; Revision 1.5  2018/04/16 21:48:04  nathan
; add /OMIT_COMMENTS
;
; Revision 1.4  2017/08/02 18:27:50  mcnutt
; reduced line to check for parts if nlines is less then 5
;
; Revision 1.3  2015/04/24 21:48:58  nathan
; if file_exist does not work, try findfile test
;
; Revision 1.2  2010/12/09 22:59:48  nathan
; add /TRIMFLAG and SPLIT=
;
; Revision 1.1  2008/03/04 19:37:15  nathan
; moved from lasco/idl/inout
;
; MODS
;   3/13/01, nbr - Add FILE_EXIST and empty file check
;

str = ''

exist = FILE_EXIST(list)
IF ~(exist) THEN BEGIN
	li=findfile(list)
	IF strlen(li[0]) GT 1 THEN exist=1
ENDIF
IF NOT(exist) THEN BEGIN
   print,list,' not found - returning.'
   return,''
ENDIF

OPENR, IN, list, /GET_LUN

flag=2
IF keyword_set(TRIMFLAG) THEN flag=2-trimflag
all = ''
first=1
WHILE NOT(EOF(IN)) DO BEGIN
    READF, IN, str
    str = STRTRIM(str,flag)
    str0=strmid(str,0,1)
    IF str EQ '' THEN BEGIN
    	IF first THEN message,'Omitting empty lines.',/info
	first=0
    ENDIF ELSE $
    IF (str0 EQ ';' or str0 EQ '#') and keyword_set(OMIT_COMMENTS) THEN BEGIN
    	print,'Omitting ',str
    ENDIF ELSE all = [all,str]
ENDWHILE

CLOSE, IN
FREE_LUN, IN

IF n_elements(all) LT 2 THEN BEGIN
   print,list,' is empty - returning.'
   return,''
ENDIF

all = all(1:*)
num = N_ELEMENTS(all)

IF keyword_set(SPLIT) THEN BEGIN
    IF datatype(SPLIT) EQ 'STR' THEN delim=split ELSE delim=' ,'
    parts=strsplit(all[0],delim,/extract)
    print,parts
    np=n_elements(parts)
    if n_Elements(all) lt 5 then zal=n_Elements(all)-1 else zal=5
    FOR i=1,zal DO BEGIN
    	parts=strsplit(all[i],delim,/extract)
	print,parts
    	np=[np,n_elements(parts)]
    ENDFOR    	
    np0=max(np)
    inp=''
    read,'Enter max number of fields per row in file: ['+trim(np0)+']',inp
    IF inp EQ '' THEN nf=np0 ELSE nf=fix(inp)
    allout=strarr(nf,num)
    FOR i=0,num-1 DO BEGIN
    	parts=strsplit(all[i],delim,/extract)
	np=n_elements(parts)
	allout[0:np-1,i]=parts
    ENDFOR    
ENDIF ELSE allout=all

RETURN, allout

END
