;+
; PURPOSE: check if a string is a number
; INPUTS:
;  str : string to test
; OUTPUTS:
;  return : true if it is a number
;-
function is_a_number,str
return,stregex(str,'^[0-9]*\.?[0-9]*$',/boolean)
end

; $Log: is_a_number.pro,v $
; Revision 1.1  2011/08/03 20:38:38  thernis
; First commit
;