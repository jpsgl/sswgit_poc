;+
; $Id: savestruct.pro,v 1.2 2007/11/27 16:23:31 thernis Exp $
;
; PURPOSE:
;  save a structure in an xdr file. Can be read with readstruct.pro
;
; CATEGORY:
;  io, data handling
;
; INPUTS:
;  file : filename
;  s : structure to save
;  
; OUTPUTS:
;  append : append the structure to an already opened file
;  lun : file logical unit number
;
;-

pro savestruct,file,s,append=append,lun=lun,gzip=gzip

if not keyword_set(append) then openw,lun,file,/xdr,/get_lun
lstsavestruct,s,lun
if not keyword_set(append) then free_lun,lun

if keyword_set(gzip) then spawn,'nohup gzip -q -f '+file+' &'

return
end
