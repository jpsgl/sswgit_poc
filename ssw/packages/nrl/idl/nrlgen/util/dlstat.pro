;+
;$Id: dlstat.pro,v 1.3 2010/12/09 22:56:40 nathan Exp $
;
; NAME:
;       DLSTAT 
;
; PURPOSE:
;       This pro reads the downloads log file and displays various information and statistics
;       for SECCHI and LASCO WWW download.
;
; CATEGORY:
;       UTIL 
;
; CALLING SEQUENCE:
;       DLSTAT 
;
; INPUTS:
;       None 
;
; KEYWORD PARAMETERS:
;       /lasco : LASCO statistics  (Default = SECCHI statistics)
;
; OUTPUTS:
;       None
;
; SIDE EFFECTS:
;       None
;
; RESTRICTIONS:
;
; PROCEDURE:
;
; EXAMPLE:
;       dlstat 
;
; MODIFICATION HISTORY:
;       Written by:     Ed Esfandiari, NRL, 10/25/1999 
;                       Ed Esfandiari, NRL, 10/26/1999  Added hardcopy option for plot.
;                       Ed Esfandiari, NRL, 11/03/1999  Added direct-input option.
;                       Ed Esfandiari, NRL, 06/09/2000  added code to handle the (% STRING: Explicitly
;                                                       formatted output truncated at limit of 1024 lines)
;                                                       problem.
;
;                       Ed Esfandiari, NRL, 08/29/2000  Made the host display field wider.
;                       Ed Esfandiari, NRL, 12/05/2000  Added patern matching (*patern*) for direct-input.
;                                                       Also added dowload-size and number-of-downloads
;                                                       plot choice. One can toggle between them.
;                       Ed Esfandiari, NRL, 01/26/2001  Change the size format from 6.2 to 7.2 to handle
;                                                       one or more GB FTP download sizes.
;                       Ed Esfandiari, NRL, 07/24/2007  Added SECCHI statistics.
;
; 

pro plot_data,dates,sizes,num_dl_dates,num_downloads,lsize,hsize,ptype,ps_plot=ps_plot
  ;!ymin=lsize
  ;!ymax=hsize

 !p.multi= 0

; print,'plot_type= ',ptype

 WIDGET_CONTROL, /hour
 if(keyword_set(ps_plot)) then begin
   set_plot,'ps'
   device,file='dlstat.ps'
   device,/landscape
   if(ptype eq 'Sizes of Downloads') then begin
     if(n_elements(sizes) gt 1) then utplot,dates,sizes,psym=2,color=1 
     xyouts,0,0,"                               Total downloads: "+ $
            string(n_elements(sizes),'(i5)'),/dev,color=1
     xyouts,",  Total size: "+string(total(sizes),'(f9.2)')+" MB",/dev,color=1
     xyouts,",  Average: "+string(total(sizes)/n_elements(sizes),'(f7.2)')+" MB",/dev,color=1
   endif else begin
     if(n_elements(num_downloads) eq 1) then erase
     if(n_elements(num_downloads) gt 1) then begin 
       !ymin=0 & !ymax= max(num_downloads)+1
       utplot,num_dl_dates,num_downloads,psym=10,ytitle='Number of Daily Downloads',color=1 
       !ymin=0 & !ymax=0
     endif
     xyouts,0,0,"    Total downloads: "+string(n_elements(sizes),'(i5)'),/dev,color=1 
   endelse
   device,/close_file
   set_plot,'x'
 endif else begin
   if(ptype eq 'Sizes of Downloads') then begin
     if(n_elements(sizes) gt 1) then begin
       utplot,dates,sizes,/nodata,color=1
       outplot,dates,sizes,psym=2,color=5
     end
     xyouts,90,215,"Total downloads: "+string(n_elements(sizes),'(i9)'),/dev,color=3
     xyouts,90,200,"Total size:      "+string(total(sizes),'(f10.2)')+" MB",/dev,color=3
     xyouts,90,185,"Average:        "+string(total(sizes)/n_elements(sizes),'(f10.2)')+" MB",/dev,color=3
   endif else begin
     if(n_elements(num_downloads) eq 1) then erase
     if(n_elements(num_downloads) gt 1) then begin 
       !ymin=0 & !ymax= max(num_downloads)+1
       utplot,num_dl_dates,num_downloads,/nodata,ytitle='Number of Daily Downloads',color=1
       outplot,num_dl_dates,num_downloads,psym=10,color=5
       !ymin=0 & !ymax=0
     endif
     xyouts,90,215,"Total downloads: "+string(n_elements(sizes),'(i9)'),/dev,color=3
   endelse
 end

 return
end

pro number_of_downloads,dates,num_dl_dates,num_downloads
     days= strmid(dates,0,strpos(dates(0),'T'))
     sdays=shift(days,-1)
     dldays=where(days ne sdays,dlcnt)
     if(dlcnt gt 0) then begin
       each_day=days(dldays)
       sdldays=shift(dldays,1)
       each_day_dl= dldays-sdldays
       each_day_dl(0)= dldays(0)+1

       utc=str2utc(days)   
       mjd=utc.mjd
       all_mjds= indgen(mjd(n_elements(mjd)-1)-mjd(0)+1)+mjd(0)
       nmjd= n_elements(all_mjds)
       num_dl_dates= strarr(nmjd)
       num_downloads= intarr(nmjd)
       j=0
       for i=0,nmjd-1 do begin
         num_dl_dates(i)= utc2str({mjd:all_mjds(i),time:0L})
         if(j lt dlcnt and strpos(num_dl_dates(i),each_day(j)) ge 0) then begin
           num_downloads(i)= each_day_dl(j)
           j= j+1
         endif 
       end
     endif else begin
       num_dl_dates= dates(0)
       num_downloads= n_elements(dates)
     endelse
   return
end


pro dlstat_event,event
 common dldata, start_date,end_date,sdate,edate,dates,sizes,hosts,addrs,emails,mtds, printer_name, $
                rhost, raddr, rmail, rmtd, host_list,addr_list,email_list,mtd_list, r2r2, windex, direct
 common ctable, ored,ogreen,oblue
 common sizes, low_size,hi_size,lsize,hsize
 common plots, plot_list,ptype
 common ndata, ndates,nsizes,num_dl_dates,num_downloads


 WIDGET_CONTROL, event.id, GET_UVALUE=uval
 WIDGET_CONTROL, event.top, GET_UVALUE=struct   ; get structure from UVALUE

 case (uval) of
  'done': begin
            tvlct,ored,ogreen,oblue
            !mtitle=''
            !xtitle=''
            !ytitle=''
            !x.style= 0
            !y.style= 0
            !ymin=0
            !ymax=0
            !xmin=0
            !xmax=0
            tool_names='exit'
            wait,3
            openw,unit,'dlstat.ps',/get_lun,/delete
            close,unit
            free_lun,unit
            WIDGET_CONTROL, /DESTROY, struct.base
          end
  'sdt': begin
              sdate= strtrim(start_date(event.index),2)
              ;print,'selected start date= ',sdate
         end
  'edt': begin
              edate= strtrim(end_date(event.index),2)
              add_one_month,edate
              ;print,'selected end date= ',edate
         end
  'lsz': begin
              lsize= low_size(event.index)
              ;print,'selected low size= ',lsize
         end
  'hsz': begin
              hsize= hi_size(event.index)
              ;print,'selected hi size= ',hsize 
          end
  'host': begin
              rhost= strtrim(host_list(event.index))
              ;print,'selected host= ',rhost
          end
  'addr': begin
              raddr= strtrim(addr_list(event.index),2)
              ;print,'selected addr= ',raddr 
          end
  'email': begin
              rmail= strtrim(email_list(event.index),2) 
              ;print,'selected email= ',rmail
           end
  'mtd': begin
             rmtd= strtrim(mtd_list(event.index),2)
             ;print,'selected mtd= ',rmtd
         end
  'plot_type': begin
                 new_ptype= strtrim(plot_list(event.index),2)
                 ;print,'selected plot_type= ',new_ptype
                 if(new_ptype ne ptype) then begin
                   ptype= new_ptype
                   plot_data,ndates,nsizes,num_dl_dates,num_downloads,lsize,hsize,ptype
                   plot_data,ndates,nsizes,num_dl_dates,num_downloads,lsize,hsize,ptype,/ps_plot
                 end
               end
  'plt': begin
         end
  'list': begin
          end
  'dirct': begin
            WIDGET_CONTROL, event.id ,GET_VALUE= direct 
            direct= strtrim(direct(0),2)
           end
  'hcopy': begin
            IF printer_name[0] EQ '' THEN msg= "Using default printer. Proceed?" ELSE $
	    msg='Printer to be used is "'+printer_name(0)+'". Proceed?'
            res=widget_message(msg,/question,title='Confirm')
            if(res eq 'Yes') then begin
              WIDGET_CONTROL, /hour
              IF printer_name[0] EQ '' THEN ptt='' ELSE ptt='-d '+printer_name[0]
	      spawn,'lp -o nobanner '+ptt+' dlstat.ps',/sh
            end; confirm
           end
  'lp_name': begin
               WIDGET_CONTROL, event.id ,GET_VALUE= printer_name
               printer_name= strtrim(printer_name,2)
             end
  'exec': begin
             WIDGET_CONTROL, /hour
             ndates= dates & nhosts= hosts & naddrs= addrs & nemails= emails & nmtds= mtds & nsizes= sizes

             bad_dates= ''
             ind= where(strtrim(ndates,2) ge sdate and strtrim(ndates,2) le edate, cnt)
             if(cnt eq 0) then bad_dates= ' (check the dates)'
             change_arrays,ndates,nhosts,naddrs,nemails,nmtds,nsizes,cnt,ind

             bad_sizes= ''
             if(cnt gt 0) then begin
               ind= where(nsizes ge fix(lsize) and nsizes le fix(hsize),cnt)
               if(cnt eq 0) then bad_sizes= ' (check the sizes)'
               change_arrays,ndates,nhosts,naddrs,nemails,nmtds,nsizes,cnt,ind
             end

             if(cnt gt 0) then begin 
               if(rhost ne host_list(0)) then begin
                 ind= where(strtrim(nhosts,2) eq rhost, cnt) 
                 change_arrays,ndates,nhosts,naddrs,nemails,nmtds,nsizes,cnt,ind
               end 
             end

             if(cnt gt 0) then begin
               if(raddr ne addr_list(0)) then begin
                 ind= where(strtrim(naddrs,2) eq raddr, cnt)
                 change_arrays,ndates,nhosts,naddrs,nemails,nmtds,nsizes,cnt,ind
               end
             end

             if(cnt gt 0) then begin
               if(rmail ne email_list(0)) then begin
                 ind= where(strtrim(nemails,2) eq rmail, cnt) 
                 change_arrays,ndates,nhosts,naddrs,nemails,nmtds,nsizes,cnt,ind
               end 
             end

             if(cnt gt 0) then begin
               if(rmtd ne mtd_list(0)) then begin
                 ind= where(strtrim(nmtds,2) eq rmtd, cnt)
                 change_arrays,ndates,nhosts,naddrs,nemails,nmtds,nsizes,cnt,ind
               end 
             end

             bad_direct= ''
             bad_direct_add= ''
             if(cnt gt 0) then begin
               if(direct ne '') then begin
                 if(strpos(direct,'*') ge 0) then begin ; do patern match
                   paterns= str_sep(direct,'*')
                   lens= strlen(paterns)
                   p_ind= where(lens gt 0, pnum)
                   if(pnum eq 1) then begin
                     patern= paterns(p_ind(0))
                     pos= strpos(strtrim(nemails,2),patern) 
                     ind= where(pos ge 0, cnt1)
                     if(cnt1 eq 0) then begin
                       pos= strpos(strtrim(nhosts,2),patern)
                       ind= where(pos ge 0, cnt1)
                     endif
                     if(cnt1 eq 0) then begin
                       pos= strpos(strtrim(naddrs,2),patern)
                       ind= where(pos ge 0, cnt1)
                     endif  
                   endif else begin
                     cnt1= 0
                     bad_direct_add= ' - Only ONE patern (i.e. *patern*) is allowed'
                   endelse
                 endif else begin ; do exact match
                   ind= where(strtrim(nemails,2) eq direct, cnt1)
                   if(cnt1 eq 0) then $ 
                     ind= where(strtrim(nhosts,2) eq direct, cnt1)
                   if(cnt1 eq 0) then $
                     ind= where(strtrim(naddrs,2) eq direct, cnt1)
                 endelse
                 cnt= cnt1
                 if(cnt gt 0) then begin 
                   change_arrays,ndates,nhosts,naddrs,nemails,nmtds,nsizes,cnt,ind
                 endif else bad_direct= ' (Check the "Direct-Input" - Blank is default)'+bad_direct_add
               end
             end

             if(cnt gt 0) then begin
               ; display and plot the data: 
               update_display,ndates,nsizes,nhosts,naddrs,nemails,nmtds,r2r2 
               erase
               number_of_downloads,ndates,num_dl_dates,num_downloads
               plot_data,ndates,nsizes,num_dl_dates,num_downloads,lsize,hsize,ptype
               plot_data,ndates,nsizes,num_dl_dates,num_downloads,lsize,hsize,ptype,/ps_plot
             endif else begin
               widget_control,r2r2,set_value= "      NO MATCHES FOR THE ABOVE SELECTIONS"+bad_dates+$
                                              bad_sizes+bad_direct+"." 
               erase
             endelse
          end
 endcase

end



pro change_arrays,ndates,nhosts,naddrs,nemails,nmtds,nsizes,cnt,ind
  if(cnt gt 0) then begin
    ndates= ndates(ind) & nhosts= nhosts(ind) & naddrs= naddrs(ind) 
    nemails= nemails(ind) & nmtds= nmtds(ind) & nsizes= nsizes(ind)
  end
  return
end


pro add_one_month,edate
   yr= fix(strmid(edate,0,4))
   mo= fix(strmid(edate,5,2))
   if(mo lt 12) then $
     mo= mo+1 $
   else begin
     mo= 1
     yr= yr+1
   endelse
   edate= string(yr,'(i4.4)')+'-'+string(mo,'(i2.2)') 
   
   return
end



pro reformat_data,dates,hosts,addrs,emails,mtds,sizes,tlabel

; do the following to left-justify format of hosts, addrs, emails and mtds strings on output:

; Data:

  mdates= max(strlen(dates))
  mhosts= max(strlen(hosts))
  maddrs= max(strlen(addrs))
  memails= max(strlen(emails))
  mmtds= max(strlen(mtds))

  blanks='                                                '
  hosts =hosts+blanks
  addrs= addrs+blanks
  emails= emails+blanks
  mtds= mtds+blanks

  hosts= strmid(hosts,0,mhosts)
  addrs= strmid(addrs,0,maddrs)
  emails= strmid(emails,0,memails)
  mtds= strmid(mtds,0,mmtds)

; Titles:

  tdate= 'Date'+blanks
  thost= 'Remote-Host'+blanks
  taddr= 'Remote-Address'+blanks
  temail= 'Email-Address'+blanks
  tmtd= 'Download-Mtd'+blanks
  tsize='Size(MB)'+blanks

  tdate= strmid(tdate,0,mdates)
  thost= strmid(thost,0,mhosts)
  taddr= strmid(taddr,0,maddrs)
  temail= strmid(temail,0,memails)
  tmtd= strmid(tmtd,0,mmtds)
  tsize= strmid(tsize,0,8)

  tlabel= tdate+'  '+tsize+'  '+thost+'  '+taddr+'  '+temail+'  '+tmtd+'       '
  return
end



pro update_display,dates,sizes,hosts,addrs,emails,mtds,r2r2 
  t=''
  tot= n_elements(dates)
  for i= 0, tot-1 do begin
    temp= dates(i)+' '+string(sizes(i),'(f7.2)')+'  '+hosts(i)+'  '+addrs(i)+'  '+emails(i)+'  '+mtds(i)
    t= [t,temp]
  end
  t= t(1:*)
  widget_control,r2r2,set_value= t
 return
end




pro dlstat,lasco=lasco
 common dldata, start_date,end_date,sdate,edate,dates,sizes,hosts,addrs,emails,mtds, printer_name, $
                rhost, raddr, rmail, rmtd, host_list,addr_list,email_list,mtd_list, r2r2, windex, direct 
 common ctable, ored,ogreen,oblue
 common sizes, low_size,hi_size,lsize,hsize
 common plots, plot_list,ptype
 common ndata, ndates,nsizes,num_dl_dates,num_downloads

;fn= "/net/louis14/data/local/apache/share/htdocs/lasco-www/downloads.log"
dir='/net/louis14/export/home/apache/share/htdocs/lasco-www'
IF ~file_exist(dir) THEN dir='.'
fn=dir+"/secchi_downloads.log"
inst='SECCHI'
if keyword_set(lasco) then begin
  fn=dir+"/downloads.log"
  inst='LASCO'
endif 

help,inst,fn

openr,dlun,fn,/get_lun

dates='' & hosts='' & addrs='' & emails='' & mtds='' & sizes= 0.0

line=''
while not (eof(dlun)) do begin
 readf,dlun,line
 if(strpos(line,"Date:") eq 0) then begin
   dates= [dates,strtrim(strmid(line,strpos(line,"Date: ")+6,strlen(line)),2)]
   readf,dlun,line
   ;hosts= [hosts,strtrim(strmid(line,strpos(line,"REMOTE_HOST: ")+13,strlen(line)),2)]
   hst= strtrim(strmid(line,strpos(line,"REMOTE_HOST: ")+13,strlen(line)),2)
   if(strpos(hst,'Non-existent') ge 0) then hst= 'Non-existent host/domain'
   hosts= [hosts,hst]
   readf,dlun,line
   if strpos(line,"REMOTE_ADDR: ") lt 0 then readf,dlun,line
   addrs= [addrs,strtrim(strmid(line,strpos(line,"REMOTE_ADDR: ")+13,strlen(line)),2)]
   readf,dlun,line
   emails= [emails,strtrim(strmid(line,strpos(line,"Email Addr: ")+12,strlen(line)),2)]
   toks= str_sep(strtrim(strmid(line,strpos(line,"Email Addr: ")+12,strlen(line)),2),'.')
   readf,dlun,line
   if(strlen(strtrim(line,2)) eq 0) then readf,dlun,line  ; in case return key was pressed after email.
   amtd= strtrim(strmid(line,strpos(line,"Download Method= ")+17,strlen(line)),2)
   if(strpos(amtd,'(') gt 0) then amtd= strmid(amtd,0,strpos(amtd,'(')-1) 
   mtds= [mtds,amtd]  
   while (strpos(line,"Download Raw Size") lt 0) do readf,dlun,line
   line= strmid(line,0,strpos(line,"MB"))
   sizes= [sizes,float(strtrim(strmid(line,strpos(line,"Raw Size=")+9,strlen(line)),2))]
 end
end
 
free_lun,dlun
close,dlun


dates= dates[1:*]
hosts= hosts[1:*]
addrs= addrs[1:*]
emails= emails[1:*]
mtds= mtds[1:*]
sizes= sizes[1:*]
tot= n_elements(dates)

months= strmid(dates,4,3)

; Note: if dates contain more than 1024 elements, following statement generates this error
;    % STRING: Explicitly formatted output truncated at limit of 1024 lines.
; and truncates days 20 1024:

; days=   string(strmid(dates,8,2),format='(i2.2)')

; So, I did the reformat only on single digit days (1-9) instead of the whole array to
; resolve the problem. If the single digits array also become more than 1024, then must break
; it apart to segments of size 1024 and a fraction:
;
;mx= 1024
;days= string(strmid(dates,8,2))
;fdays= fix(days)
;sfdays= where(fdays lt 10,cnt)
;;if(cnt gt 0) then days(sfdays)= string(days(sfdays),format='(i2.2)') ; this only works for cnt <= 1024.
;whole= cnt/mx
;frac= cnt mod mx 
;for l= 0,whole-1 do begin
; days(sfdays(l*mx:(l+1)*mx-1))= string(days(sfdays(l*mx:(l+1)*mx-1)),format='(i2.2)')
;end
;if (frac gt 0) then begin
;  if(whole eq 0) then l= l-1
;  days(sfdays(l*mx:l*mx+frac-1))= string(days(sfdays(l*mx:l*mx+frac-1)),format='(i2.2)')
;end

days=   string(strmid(dates,8,2),format='(i2.2)')  ; new versions of ID now handle this problem 

times=  strmid(dates,11,8)
years=  strmid(dates,20,4)

m=where(months eq 'Jan',cnt) & if(cnt gt 0) then months(m)= '01'
m=where(months eq 'Feb',cnt) & if(cnt gt 0) then months(m)= '02'
m=where(months eq 'Mar',cnt) & if(cnt gt 0) then months(m)= '03'
m=where(months eq 'Apr',cnt) & if(cnt gt 0) then months(m)= '04'
m=where(months eq 'May',cnt) & if(cnt gt 0) then months(m)= '05'
m=where(months eq 'Jun',cnt) & if(cnt gt 0) then months(m)= '06'
m=where(months eq 'Jul',cnt) & if(cnt gt 0) then months(m)= '07'
m=where(months eq 'Aug',cnt) & if(cnt gt 0) then months(m)= '08'
m=where(months eq 'Sep',cnt) & if(cnt gt 0) then months(m)= '09'
m=where(months eq 'Oct',cnt) & if(cnt gt 0) then months(m)= '10'
m=where(months eq 'Nov',cnt) & if(cnt gt 0) then months(m)= '11'
m=where(months eq 'Dec',cnt) & if(cnt gt 0) then months(m)= '12'

dates=  years+'-'+months+'-'+days+'T'+times

reformat_data,dates,hosts,addrs,emails,mtds,sizes,tlabel

;wtitle='LASCO Download Statistics'
wtitle=inst+' Download Statistics'

disp= getenv('DISPLAY')
if(disp eq '132.250.160.215:0.0') then disp='susim-n'

disp='susim-n'

;base= widget_base(title=wtitle,xsize=915,ysize=680,xoffset=10,/frame)
;base= widget_base(title=wtitle,xsize=918,ysize=715,xoffset=10,/frame)
if(disp eq 'susim-n') then $
  base= widget_base(title=wtitle,xsize=950,ysize=715,xoffset=10,/frame) $
else $
  base= widget_base(title=wtitle,xsize=1120,ysize=715,xoffset=10,/frame)

;r1= widget_base(base,/ROW)       ; devide base into rows; pickup row r1 
;c1= widget_base(r1,/COL,/FRAME)  ; get c1 column from r1
;c2= widget_base(r1,/COL,/FRAME)  ; get c2 column from r1 

c0= widget_base(base,/COL)       ; make base into a column (c0)
r1= widget_base(c0,/ROW)         ; pick up a row from c0 (r1)
r2= widget_base(c0,/ROW)         ; pick up another row from c0 (r2)
c1= widget_base(r1,/COL,/FRAME)  ; divide r1 into column c1
;c2= widget_base(r1,/COL,/FRAME)  ; and into column c2 
c2= widget_base(r1,/COL)
c2r= widget_base(r2,/COL,/FRAME)

months= strmid(dates,0,7)
smonths= shift(months,-1)
keep= where(months ne smonths,num)
if(num eq 0) then begin
  print,"no data"
  return
end

start_date= [months(keep)]
end_date= start_date
for i= 0,num-1 do end_date(i)= start_date(num-1-i)
c1r1= widget_base(c1,/ROW)
sdate= start_date(0)
lab= widget_label(c1r1,value='              ')
sdate_id= cw_bselector(c1r1, UVALUE='sdt',start_date,label_top="Start Date") 
lab= widget_label(c1r1,value='  ') 
edate= end_date(0)
add_one_month,edate
edate_id= cw_bselector(c1r1, UVALUE='edt',end_date,label_top="End Date")

hst= hosts(sort(hosts))
shosts= shift(hst,-1)
keep= where(hst ne shosts,num)
host_list= strtrim(['All',hst(keep)],2)
c1r2= widget_base(c1,/ROW)
rhost= host_list(0)
;host_id= cw_bselector(c1r2, UVALUE='host',host_list,label_left="Remote Hosts: ")
;lab= widget_label(c1r2,value='Remote Hosts:')
;host_id= widget_list(c1r2, UVALUE='host',value=host_list) ; must make new selection & press on mouse,too. 
host_id= widget_droplist(c1r2, UVALUE='host',value=host_list,title='Remote Hosts:')

adr= addrs(sort(addrs))
sadr= shift(adr,-1)
keep= where(adr ne sadr)
addr_list= strtrim(['All',adr(keep)],2)
c1r3= widget_base(c1,/ROW)
raddr= addr_list(0)
;addr_id= cw_bselector(c1r3, UVALUE='addr',addr_list,label_left="Remote Addresses: ")
addr_id= widget_droplist(c1r3, UVALUE='addr',value=addr_list,title='Remote Addresses:')

mail= emails(sort(emails))
smail= shift(mail,-1)
keep= where(mail ne smail,num)
email_list= strtrim(['All',mail(keep)],2)
c1r3= widget_base(c1,/ROW)
rmail= email_list(0)
;email_id= cw_bselector(c1r3, UVALUE='email',email_list,label_left="Email Addresses: ")
email_id= widget_droplist(c1r3, UVALUE='email',value=email_list,title='Email Addresses:')

mtd= mtds(sort(mtds))
smtd= shift(mtd,-1)
keep= where(mtd ne smtd,num)
mtd_list=['All',mtd(keep)]
c1r4= widget_base(c1,/ROW)
rmtd= mtd_list(0)
mtd_id= cw_bselector(c1r4, UVALUE='mtd',mtd_list,label_left="Download Methods: ")

low_size= string([0,5,10,20,50,100,200,300,400,500])
if(max(sizes) gt 500) then low_size= [low_size,string(fix(max(sizes)+1))]
hi_size= low_size 
num= n_elements(low_size)
for i= 0,num-1 do hi_size(i)= low_size(num-1-i)
c1r41= widget_base(c1,/ROW)
lsize= low_size(0)
lsize_id= cw_bselector(c1r41, UVALUE='lsz',low_size,label_left="Sizes (MB):  From") 
hsize= fix(max(sizes)+1) 
hsize_id= cw_bselector(c1r41, UVALUE='hsz',hi_size,label_left="To")

c1r43= widget_base(c1,/ROW)
direct= ''
lab= WIDGET_LABEL(c1r43,VALUE=' Direct-Input (use Email, Host, Address, or a *patern*)')
;select= WIDGET_TEXT(c1r43,UVALUE='dirct',VALUE=direct,/EDITABLE,xsize=15, $
;                    /ALL_EVENTS)

c1r45= widget_base(c1,/ROW)
lab= WIDGET_LABEL(c1r45,VALUE='       ')

select= WIDGET_TEXT(c1r45,UVALUE='dirct',VALUE=direct,/EDITABLE,xsize=35, $
                    /ALL_EVENTS)
lab= WIDGET_LABEL(c1r45,VALUE='Blank=Default')

c1r5= widget_base(c1,/ROW)
lab= WIDGET_LABEL(c1r5,VALUE='    ')
gid=  WIDGET_BUTTON(c1r5, VALUE='Get Statistics (using the above settings)',UVALUE='exec',/frame)

c2r0= widget_base(c2,/ROW)
if(disp eq 'susim-n') then $
  lab= WIDGET_LABEL(c2r0,VALUE='                                          ') $
else $
  lab= WIDGET_LABEL(c2r0,VALUE='                                                                      ')
exit=  WIDGET_BUTTON(c2r0, VALUE='Exit',UVALUE='done')

edg= 2
c2r1= widget_base(c2,/ROW)
lab= WIDGET_LABEL(c2r1,VALUE='  ')
if(disp eq 'susim-n') then begin
  draw= widget_draw(c2r1,xsize=500+edg,ysize=250+edg,retain=2,/motion_events,$
                    /button_events,/viewport_events,UVALUE='plt',/frame)
endif else begin
  draw= widget_draw(c2r1,xsize=580+edg,ysize=280+edg,retain=2,/motion_events,$
                    /button_events,/viewport_events,UVALUE='plt',/frame)
endelse
c2r2= widget_base(c2,/ROW)
c2r3= widget_base(c2,/ROW)
if(disp eq 'susim-n') then begin 
  lab= WIDGET_LABEL(c2r2,VALUE='                        ') 
  lab= WIDGET_LABEL(c2r3,VALUE='                     ') 
endif else begin
  lab= WIDGET_LABEL(c2r2,VALUE='                                   ')
  lab= WIDGET_LABEL(c2r3,VALUE='                                ')
endelse

plot_list= ['Sizes of Downloads','Number of Downloads']
ptype= plot_list(0)
plot_id= widget_droplist(c2r2, UVALUE='plot_type',value=plot_list,title='Plot Type:')

select= WIDGET_BUTTON(c2r3, UVALUE='hcopy',VALUE='Make Hardcopy',FONT=font)

printer_name= 'hp4-247'
lab= WIDGET_LABEL(c2r3,VALUE='-> printer:')
select= WIDGET_TEXT(c2r3,UVALUE='lp_name',VALUE='hp4-247',/EDITABLE,xsize=15, $
                    /ALL_EVENTS)

;label= ' Date               Size(MB)  Remote-Host                         Remote-Address   Email-Address                   Download-Method'
;label= label+'                  '

if(disp eq 'susim-n') then begin 
  label= ' Date               Size(MB)  Remote-Host                                   Remote-Address   Email-Address                      Download-Method'
  label= label+'            '
endif else begin
  label= ' Date                         Size(MB)  Remote-Host                                                      Remote-Address   Email-Address                                  Download-Method'
  label= label+'                '
endelse

label= tlabel

r2r1= widget_base(c2r,/ROW)
lab= WIDGET_LABEL(r2r1,VALUE= label)

r2r2= widget_text(c2r,uvalue='list',xsize=90,ysize=20,/wrap,/all_event,/scroll)

update_display,dates,sizes,hosts,addrs,emails,mtds,r2r2 

device,decomposed=0 ; use 8-bit color (for 24-bit systems) - IDL limitation.

widget_control,/realize,base
widget_control,draw,get_value= windex
wset,windex

;q= cw_defroi(draw)
;print,q

;!mtitle='Individual raw download sizes (MB)'
;!mtitle='LASCO Database .FTS Downloads'
!mtitle=inst+' Database .FTS Downloads'
!ytitle='Raw Download Sizes (Mega Bytes)'
!x.style= 1
!y.style= 1
!ymin=0 & !ymax=0
!xmin=0 & !xmax=0

WIDGET_CONTROL, /hour

;  Get the current color table and save it:
   tvlct,/get,ored,ogreen,oblue
;  Create a color table of Black, White, Red, Green, Blue, Yellow, Cyan, and
;  Magenta and load it into the display device:
;         B,W,R,G,B,Y,C,M
       r=[0,1,1,0,0,1,0,1]
       g=[0,1,0,1,0,1,1,0]
       b=[0,1,0,0,1,0,1,1]
       tvlct,255*r,255*g,255*b

ndates= dates
nsizes= sizes

number_of_downloads,dates,num_dl_dates,num_downloads
plot_data,dates,sizes,num_dl_dates,num_downloads,lsize,hsize,ptype
plot_data,dates,sizes,num_dl_dates,num_downloads,lsize,hsize,ptype,/ps_plot

struct={base:base}
widget_control,base, SET_UVALUE=struct
xmanager,'dlstat',base,EVENT_HANDLER='dlstat_event', GROUP_LEADER= GROUP

return
end
