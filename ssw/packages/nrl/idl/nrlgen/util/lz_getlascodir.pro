function lz_getlascodir ,month,day,year,camera,SILENT=silent
;+
; $Id: lz_getlascodir.pro,v 1.1 2010/06/17 17:16:12 nathan Exp $
;
;NAME: LZ_GETLASCODIR

;PURPOSE:
;       Tells user which directory contains a given days worth of level zero
;	LASCO data for a particular camera.

;CALLING SEQUENCE:
;	dir=lz_getlascodir(1,6,1996,'C1')
;     or
;	dir=lz_getlascodir(1,6,96,'c1')
;
;INPUTS:
;	month:	an integer betwen 1 (January) and 12 (December)
;	day:	day of the month
;	year:	either the last two digits of the year (e.g. 96) or all 4 digits
;	camera:	a two-character string describing which camera is desired.
;		Acceptable choices for camera are: "c1","c2","c3", or "c4"
;		camera is case-insensitive

;KEYWORDS: 
;	SILENT: Supress output of all error messages except lower-level IDL
;		and system messages. 

;OUTPUTS:
;      A string specifiying the directory in which desired images can be found.

;AUTHOR: Scott Hawley, NRL, June 27, 1996
;
;MODIFIED: SHH 7/12/96  Generates directory names for cplex2
;	9/23/96 by N. Rich	modified printed output
;	961007 by N. Rich	fixed findfile call
;	970827 by N. Rich	use LZ_IMG for directory
;       990126 Ed Esfandiari    Fixed code for Y2K problem.
; 
; SCCS variables for IDL use
; 
; @(#)lz_getlascodir.pro	1.7 08/27/97 :NRL Solar Physics
;
;-
;$Log: lz_getlascodir.pro,v $
;Revision 1.1  2010/06/17 17:16:12  nathan
;*** empty log message ***
;


errstr=''

;if (year gt 1900) then year=year-1900

if(year lt 100) then year= year+1900
if(year lt 2000) then begin
  year=year-1900
endif else begin
  year=year-2000
end

camera=strlowcase(camera)

daydir = strtrim(string(year,format='(I2.2)'),2)+strtrim(string(month,format='(I2.2)'),2) $
              +strtrim(string(day,format='(I2.2)'),2)+'/'

;missing data

if ((month eq 1) and (year eq 96) and ((day eq 17) or (day eq 7) or (day eq 8))) then begin
   if not (KEYWORD_SET(silent)) then print,'Sorry, no data for ',daydir
   return,p 
endif
 
If (camera ne 'c4') THEN dirname=getenv('LZ_IMG')+'/level_05/' $
	ELSE BEGIN
           IF not (KEYWORD_SET(silent)) THEN $
		print,"lz_getlascodir: EIT isn't ready yet. You're on your own"
           return,errstr
	ENDELSE

;print,'directory= ',dirname+daydir+camera
check=findfile(dirname+daydir+camera,count=numfiles)
if (numfiles eq 0) then begin
  if not (KEYWORD_SET(silent)) then $
	print,'Sorry, no LZ data available for ',daydir+camera
  return,errstr 
endif

return,STRCOMPRESS(dirname+daydir+camera+'/',/REMOVE_ALL)

end

