;+
; NAME:
;       FIXWRAP
;
;
; PURPOSE:
;       This function removes the overflow values of an image
;
;
; CATEGORY:
;  
;
;
; CALLING SEQUENCE:
;       out = FIXWRAP(in)
;
; 
; INPUTS:
;       in = image with overflows
;
;
; OPTIONAL INPUTS:
;      NONE
;
;	
; KEYWORD PARAMETERS:
;      NONE
;
;
; OUTPUTS:
;      out = a long image with overflows removed
;
;
; OPTIONAL OUTPUTS:
;      NONE
;
; COMMON BLOCKS:
;      NONE
;
; SIDE EFFECTS:
;      The output image is LONG type
;
; RESTRICTIONS:
;      NONE
;
; PROCEDURE:
;
;
; EXAMPLE:
;      in = LASCO_READFITS(file, h)
;      out = FIXWRAP(in)
;
; MODIFICATION HISTORY:
;      Written by:         A. Vourlidas, NRL, 9/30/97
;-
FUNCTION fixwrap,  in
;
out =  in + (in LT 0)*'ffff'xL
out =  out +  (out lT 0)*'ffff'xL
return,  out
END


