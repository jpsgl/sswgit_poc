;+
; PROJET
;     SECCHI
;
; NAME:
;
; PURPOSE:
;  Remove wraps due to exceeding max value (such as a packet counter)
;
; CATEGORY:
;  Mathematics
;
; CALLING SEQUENCE:
;   
; DESCRIPTION:
;
; INPUTS:   1-D array
;
; INPUT KEYWORD:
;
; OUTPUTS: Array same dim as input
;
; PROCEDURE: 
;  
; CALLED ROUTINES: 
;
;  $Log: word2dec.pro,v $
function unwrapctr, arr

sz=size(arr)
maxa=max(arr)
out=arr[0]
base=0
for i=1L,sz[1]-1 DO BEGIN
    IF arr[i] LT arr[i-1] THEN base=base+maxa
    out=[out,arr[i]+base]
endfor

return,out

end
