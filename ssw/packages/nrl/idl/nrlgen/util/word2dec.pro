;+
; PROJET
;     SECCHI
;
; NAME:
;
; PURPOSE:
;  Convert an array of integers into a decimal value. Input should be BIG endian.
;
; CATEGORY:
;  Mathematics
;
; CALLING SEQUENCE:
;   
; DESCRIPTION:
;
; INPUTS:   1-D or 2-d byte or int array (or scalar); 2nd dimension is size of returned array
;
; INPUT KEYWORD:
;   /SIGNED 	Returns signed instead of unsigned values of dimension 2nd dimension of input
;
; OUTPUTS:
;
; PROCEDURE: Returns unsigned INT or LONG depending on size of input. 
;  
; CALLED ROUTINES: hex2dec64.pro
;
;  $Log: word2dec.pro,v $
;  Revision 1.4  2018/12/26 16:16:47  mcnutt
;  changed hex formats to be compatable with GDL
;
;  Revision 1.3  2017/12/19 23:03:23  nathan
;  current SSW versions
;
;  Revision 1.2  2015/08/06 16:25:36  nathan
;  use 64 bit hex2dec
;
;  Revision 1.1  2015/04/28 20:47:30  nathan
;  for SoloHI
;
function word2dec, arr, SIGNED=signed, SILENT=silent

sz=size(arr)
IF sz[0] EQ 0 THEN BEGIN
    n=1
    typ=sz[1] 
    na=1
ENDIF ELSE $
IF sz[0] EQ 1 THEN BEGIN
    typ=sz[2]
    n=sz[1]
    na=1
ENDIF ELSE BEGIN
    typ=sz[3]
    n=sz[1]
    na=sz[2]
ENDELSE

hhx=0
FOR j=0L,na-1 DO BEGIN
IF typ EQ 1 THEN BEGIN
; byte input
    hx=string(arr[0,j],'(z02)')
    FOR i=1,n-1 DO hx=hx+string(arr[i,j],'(z02)')
ENDIF ELSE BEGIN
    hx=string(arr[0,j],'(z04)')
    FOR i=1,n-1 DO hx=hx+string(arr[i,j],'(z04)')
ENDELSE

IF (not(keyword_set(SILENT))) THEN help,hx
nx=strlen(hx)

hex2dec64,hx,dec,/quiet

IF keyword_set(SIGNED) THEN BEGIN
    IF nx LE 4 THEN x=fix(dec)
    IF nx GT 4 THEN x=dec
ENDIF ELSE BEGIN 
    IF nx LE 4 THEN x=uint(dec)
    IF nx GT 4 THEN x=ulong(dec)
ENDELSE
hhx=[hhx,x]
ENDFOR

return,hhx[1:na]
end
