;+
; $Id: pixccd2pixim.pro,v 1.1 2006/09/11 21:10:45 nathan Exp $
;
; PURPOSE:
;  change coordinate system from pixel image to pixel CCD
;
; CATEGORY:
;  image processing
;
; DESCRIPTION:
;  This program is useful when measuring features in images of
;   different resolution. It permits to deal with the shift induced by
;   the rebining of images. The method is to convert image pixel
;   position into physical CCD pixel position.
;
;  pixel image : pixel position in an image, centered on the center 
;                of the pixel. It's given in units of pixels. 
;                The pixel 0,0 is at the center of the
;                bottom left pixel.
;
;  pixel CCD : pixel position on the CCD. It's centered on the bottom
;              left corner of each pixel. It's given in units of 
;              distance. The position 0,0 is at the bottom left
;              corner of the bottom left pixel of the CCD.
;  pixsidesize : size of the pixel size in distance units 
;               (mm for example)
;
; INPUTS:
;  pixccd: pixel position on the CCD
;  pixsidesize : size of the pixel size in distance units (mm for example)
;
; OUTPUTS:
;  return : the pixel position in image coordinate
;
;-
function pixccd2pixim,pixccd,pixsidesize

return,pixccd/pixsidesize-0.5
end
