pro printmedsdev, inp
;+
; $Id: printmedsdev.pro,v 1.1 2009/03/04 22:13:11 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : printmeanstdev
;               
; Purpose   : print Median and Standard Deviation of input array
;               
; Explanation: 
;               
; Use       : 
;    
; Inputs    :   inp 	any Array of numerical type

; Optional Inputs: 
;               
; Outputs   : prints 2 values

; Optional Outputs: 
;
; Keywords  :   
;
; Category    : math 
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL/I2, Feb 09
;-               
; $Log: printmedsdev.pro,v $
; Revision 1.1  2009/03/04 22:13:11  nathan
; simple utility
;
; Revision 1.1  2008/05/09 19:13:23  nathan
; calls compare_struct.pro
;

print,median(inp),stdev(inp)

end
