PRO SETFONT, BIG=big,SMALL=small
; $Id: setfont.pro,v 1.2 2010/12/09 23:00:37 nathan Exp $
; 
; Purpose: permanantly change default hardware font (until it is changed again)
;
; $Log: setfont.pro,v $
; Revision 1.2  2010/12/09 23:00:37  nathan
; doc
;
; Revision 1.1  2009/04/28 21:30:08  nathan
; Move from lasco/idl/util
;
; @(#)setfont.pro	1.5 10/02/02 :LASCO IDL LIBRARY
;
 !P.FONT=0

;device,set_font='-adobe-helvetica-*-r-normal*',get_fontnames=fn
;wbig=where(fn EQ 

IF keyword_set(BIG) THEN $
   DEVICE, FONT='-adobe-helvetica-bold-r-normal--34-240-100-100-p-182-iso8859-1' $
ELSE IF keyword_set(SMALL) THEN $
   DEVICE, FONT='-adobe-helvetica-medium-r-normal--18-180-75-75-p-98-iso8859-1' $
ELSE $
   DEVICE, FONT='-adobe-helvetica-medium-r-normal--24-240-75-75-p-130-iso8859-1'
END
