;+
; PROJECT:  STEREO/SECCHI
;
; NAME:     binfound.pro
;
; PURPOSE:
;  test for executable in $path of shell
;
; CATEGORY: operating system file shell utilities
;
; CALLING SEQUENCE:
;   	flag=binfound('pwd')
;   
; Use:
;   	IF binfound('pwd') THEN do something
;
; INPUTS:
;   	    exe STRING	binary executable
;
; INPUT KEYWORD:
;
; OUTPUTS:
;   	    STRING
;
; CALLED ROUTINES: spawn
;
; HISTORY:
;  $Log: binfound.pro,v $
;  Revision 1.2  2011/12/01 15:57:40  nathan
;  return 0 if not unix
;
;  Revision 1.1  2011/01/05 18:26:52  nathan
;  used in pixmaps2mpeg and scc_movieout
;

function binfound, inp

IF !version.os_family NE 'unix' THEN return,0
; in IDL versions LE 5.3, no 2nd argument to spawn is allowed
SPAWN, ['which',inp], result, /NOSHELL
result = result[0]

IF (STRPOS(result, 'not found') GE 0 OR $
    STRPOS(result, 'no '+inp) GE 0) or $
    strlen(result) EQ 0 THEN $
    return,0 ELSE $
    return,1
    
end
