;+
; PROJECT:  STEREO/SECCHI
;
; NAME:     bin_data.pro
;
; PURPOSE:
;  separate input into bins of size specified
;
; CATEGORY: array statistics time analysis housekeeping telemetry
;
; CALLING SEQUENCE:
;   	bin_data, indx, data, indxo, datao, days=1 [or other binsize spec.]
;   
; Use:
;   	
;
; INPUTS:
;   	indx	1-d vector, increasing, time or not
;   	data	1-d vector of same size of indx
;
; INPUT KEYWORD:
;   	SECONDS=, MINUTES=, HOURS=, DAYS=   Specify bin size if indx is times
;   	BINSIZE=    Specify bin size in unit of indx (not time)
;   	/FILL	include value for every dx in indxo (data0=0)
;
; OUTPUTS:
;   	indxo	values corresponding to start of bin where elements in bin is GT 0
;   	    	CDS time structure or unit of indx
;   	datao	number of data in each bin
;
; CALLED ROUTINES: anytim2tai, tai2utc
;
; HISTORY:
;  $Log: bin_data.pro,v $
;  Revision 1.1  2011/02/11 17:25:49  nathan
;  called by plotmemscrub.pro
;

pro bin_data, indx, data, indxo, datao, SECONDS=seconds, MINUTES=minutes, HOURS=hours, DAYS=days, $
    BINSIZE=binsize, FILL=fill

IF keyword_set(BINSIZE) THEN BEGIN
; indx is not time
    dx=binsize
    x=indx
ENDIF ELSE $
; convert indx to seconds
    x=anytim2tai(indx)
    
nx=n_elements(x)

IF keyword_set(SECONDS) THEN dx=seconds ELSE $
IF keyword_set(MINUTES) THEN dx=minutes*60. ELSE $
IF keyword_set(HOURS) THEN dx=hours*3600. ELSE $
IF keyword_set(DAYS) THEN dx=days+86400.

xend=x[nx-1]
xn=x[0]
wn=where(x GE xn and x LT xn+dx,nw)
IF nw GT 0 THEN datao=total(data[wn]) ELSE datao=data[0]
indxo=xn
xn=xn+dx

WHILE (xn LT xend) DO BEGIN
    wn=where(x GE xn and x LT xn+dx,nw)
    IF nw GT 0 THEN BEGIN
    	datao=[datao,total(data[wn])]
	indxo=[indxo,xn]
    ENDIF ELSE $
    if keyword_set(FILL) THEN BEGIN
    	datao=[datao,0]
	indxo=[indxo,xn]
    ENDIF
    xn=xn+dx
ENDWHILE
IF not keyword_set(BINSIZE) THEN indxo=tai2utc(indxo)

end
