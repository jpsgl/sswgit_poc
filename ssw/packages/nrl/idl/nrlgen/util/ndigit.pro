;+
; $Id: ndigit.pro,v 1.2 2007/08/27 20:34:00 nathan Exp $
;
; PURPOSE:
;  Returns number of digits to the left of the decimal.
;
; CATEGORY:  data handling format string
;
; INPUTS:
;  x	: a number
;
; OUTPUTS:
;  n 	: number of digits in x
;
;-

function ndigit, x

xx=x
n=0
while xx gt 1 do begin
    n=n+1
    xx=xx/10.
    ;help,xx
endwhile
return,n>1 
end
