;+
; PROJECT:
;  SOHO-LASCO
;
; NAME:
;  lststruct
;
; PURPOSE:
;  explore and give a description of a tree structure
;
; CATEGORY:
;  data handling, io
;
; DESCRIPTION: 
;  The tree structure is explored in pre-order
;
; CALLING SEQUENCE:
;  lststruct,s,fldname,sonsize,strname,/test
;
; INPUTS:
;  s : structure to find the description
;
; OUTPUTS:
;  fldname : give the tag names of the structure
;  sonsize : give the size of the son of the tags
;  strname : give the name of the structures and sub structures, NULL
;            string if anonymous 
;
; INPUT KEYWORDS:
;  test : set to run in test mode: only display info while running
;
; OUTPUT KEYWORDS:
;
; HISTORY:
;  coded by A.Thernisien on 20/08/2002
;
; CVSLOG:
;  Revision 1.1  2002/08/20 15:46:05  arnaud
;  *** empty log message ***
; $Id: lststruct.pro,v 1.1 2006/09/11 21:10:44 nathan Exp $
;-
pro lstsub,s,fldname,sonsize,strname,test=test

tagnames=tag_names(s)
sname=tag_names(s,/str)
ntags=n_tags(s)

if test then print,'STRNAME: '+sname
strname=[strname,sname]

for i=0,ntags-1 do begin

    if test then begin
        print,'FLDNAME: '+tagnames(i)
        print,'NBSON  : '+strtrim(n_tags(s.(i)),2)
    endif
    sz=size(s.(i))
    nbsz=n_elements(sz)
    ; -- if it's a string, get the len
    if sz(nbsz-2) eq 7 then sz(nbsz-1)=strlen((s(0).(i))(0))

    if test then begin
        sztxt='SIZE   : '
        for j=0,nbsz-1 do sztxt=sztxt+strtrim(sz(j),2)+' ; '
        print,sztxt
    endif

    fldname=[fldname,tagnames(i)]
    sonsize=[sonsize,n_tags(s.(i)),sz]

    if sz(nbsz-2) eq 8 then lstsub,(s(0).(i))(0),fldname,sonsize,strname,test=test

endfor

return
end



pro lststruct,s,fldname,sonsize,strname,test=test

if n_elements(test) ne 0 then test=1B else test=0B

tagnames=tag_names(s)
ntags=n_tags(s)
sz=size(s)
nbsz=n_elements(sz)
strname=''

if test then begin
    print,'ROOT'
    print,'NBSON  : '+strtrim(ntags,2)
    sztxt='SIZE   : '
    for j=0,nbsz-1 do sztxt=sztxt+strtrim(sz(j),2)+' ; '
    print,sztxt
endif
fldname='ROOT'
sonsize=long(ntags)
sonsize=[sonsize,sz]

lstsub,s(0),fldname,sonsize,strname,test=test

strname=strname(1:*)

return
end
