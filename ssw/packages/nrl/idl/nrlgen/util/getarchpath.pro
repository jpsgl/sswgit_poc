function getarchpath, rootdir, binfile
;+
; $Id: getarchpath.pro,v 1.1 2008/10/27 17:40:59 nathan Exp $
;
; PROJECT:  STEREO/SECCHI
;
; NAME:     GETARCHPATH
;
; PURPOSE:  Returns binfile with path to match architecture
;
; CATEGORY: operating system, utility, file
;
; EXAMPLE:
;	Result = getarchpath(getenv('SECCHI_LIB'),'xyzsun')
;
; INPUTS:
;	Rootdir = directory which contains subdirs of OS's
;   	binfile = name of binary file that you want to execute
;
; OUTPUTS:
;	Result = path + filename
;
; PROCEDURE:
;
;
; MODIFICATION HISTORY:
; $Log: getarchpath.pro,v $
; Revision 1.1  2008/10/27 17:40:59  nathan
; path utility
;
;-
;
sl=get_delim() 

return,concat_dir(rootdir,!version.os_name+sl+!version.arch+sl+binfile)
end
