;+
; $Id: wcschangeunits.pro,v 1.2 2007/11/27 16:26:34 thernis Exp $
;
; PURPOSE:
;  convert a variable from one wcs angle unit to one another
;
; CVSLOG:
;  $Log: wcschangeunits.pro,v $
;  Revision 1.2  2007/11/27 16:26:34  thernis
;  Check if var is passed or not to avoid crash if not.
;
;  Revision 1.1  2007/07/25 15:33:08  thernis
;  first commit
;
;-

function wcschangeunits,unitin0,unitout0,var,factor=factor

if n_elements(var) eq 0 then var=1.

if size(unitin0,/n_dim) eq 0 then begin
    unitin=replicate(unitin0,n_elements(var))
endif else unitin=unitin0
if size(unitout0,/n_dim) eq 0 then begin
    unitout=replicate(unitout0,n_elements(var)) 
endif else unitout=unitout0

unitinout=strlowcase(strtrim([[unitin],[unitout]],2))

nbunit=n_elements(unitin)
if nbunit ne n_elements(unitout) then message,'unitin and unitout should have the same number of elements.'

f=fltarr(2,nbunit)
factor=fltarr(nbunit)

for j=0,nbunit-1 do begin
    for i=0,1 do begin
        case unitinout[j,i] of
            'deg' : f[i,j]=!dtor
            'arcsec' : f[i,j]=!dtor/3600
            'rad' : f[i,j]=1.
            else : message,'Unknown units or not implemented'
        endcase
    endfor
    factor[j]=f[0,j]/f[1,j]
endfor

return,var*factor
end
