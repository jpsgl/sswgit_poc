
function form_qry,table,fields,f_ind,conds,tab_cnt,req_pck_time
; $Id: form_qry.pro,v 1.1 2008/02/22 20:45:01 nathan Exp $
;    tablename= table
;
;    qry= {field:intarr(n_elements(fields)), cond:strarr(n_elements(fields))}
;
; $Log: form_qry.pro,v $
; Revision 1.1  2008/02/22 20:45:01  nathan
; moved from lasco/idl/database
;

 qry_cmnd=''

 s= size(f_ind)

 if(s(0) ne 0 or tab_cnt gt 1) then begin ; atleast one field was selected
                                          ; and/or have a join and only a
                                          ; condition (no field) may be given.
   qry_cmnd='select '
   if(s(0) ne 0) then begin ; atleast one filed was selected
     for i=0, n_elements(f_ind)-1 do qry_cmnd= qry_cmnd+fields(f_ind(i))+','
     qry_cmnd= strmid(qry_cmnd,0,strlen(qry_cmnd)-1) ; remove the last comma
   end
   qry_cmnd= qry_cmnd+' from '+table

   conds= strtrim(conds,2) ; remove leading/trailing blanks OR 
                                 ; conditions fields that are all blanks

   c_ind= where(conds ne '')
   s= size(c_ind)
   if(s(0) ne 0) then begin ; atleast one condition was given
     qry_cmnd= qry_cmnd+' where '
     prev_len=strlen(qry_cmnd)
     for i=0, n_elements(c_ind)-1  do begin
       if(not(tab_cnt gt 1 and fields(c_ind(i)) eq 'pck_time')) then $
         qry_cmnd= qry_cmnd+fields(c_ind(i))+' '
       cond= conds(c_ind(i))
       pos= strpos(cond,'&') ; separate tokens that have & (ie day1&day2)
       if(pos ne -1) then begin
         tok1= strmid(cond,0,pos)
         tok2= strmid(cond,pos+1,strlen(cond)-1)
         if(not(tab_cnt gt 1 and fields(c_ind(i)) eq 'pck_time')) then $
           qry_cmnd= qry_cmnd+'between '+tok1+' and '+tok2+' and '
         if(fields(c_ind(i)) eq 'pck_time') then $
           req_pck_time= 'between '+tok1+' and '+tok2
       endif else begin
         if(not(tab_cnt gt 1 and fields(c_ind(i)) eq 'pck_time')) then $
           qry_cmnd= qry_cmnd+cond+' and '
         if(fields(c_ind(i)) eq 'pck_time') then begin
           rpt= strmid(cond,1,strlen(cond))
           if(strpos(rpt,'=') eq 0) then rpt=strmid(rpt,1,strlen(rpt))
           if(strpos(rpt,'>') eq 0) then rpt=strmid(rpt,1,strlen(rpt))
           if(strpos(rpt,'<') eq 0) then rpt=strmid(rpt,1,strlen(rpt))
           req_pck_time= rpt 
         end
       endelse
     end ;for loop
     if(strlen(qry_cmnd) gt prev_len) then $
       qry_cmnd= strmid(qry_cmnd,0,strlen(qry_cmnd)-5) ; remove the last ' and '
   end
 end

; print,'qry_cmnd= ',qry_cmnd
; print,'req_pck_time= ',req_pck_time

RETURN, qry_cmnd
END


