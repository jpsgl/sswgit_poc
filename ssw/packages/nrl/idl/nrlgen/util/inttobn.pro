;+
; PROJET
;     SECCHI
;
; NAME:
;  INTTOB34
;
; PURPOSE:
;  Convert a integer to a base n number
;
; CATEGORY:
;  Mathematics
;
; CALLING SEQUENCE:
;   
; DESCRIPTION:
;
; INPUTS:
;
; INPUT KEYWORD:
;
; OUTPUTS:
;
; PROCEDURE:
;  
; CALLED ROUTINES:
;
; HISTORY:
;	V1 A.Thernisien 10/07/2001
; CVSLOG:
;  $Log: inttobn.pro,v $
;  Revision 1.1  2007/05/03 14:31:55  mcnutt
;  copied from inttob32.pro add n for base value
;
;  Revision 1.2  2002/07/11 07:24:12  arnaud
;  Insertion of the Log in each header
;
;
;-
function inttobn,in,n,nbdigit

sze=size(in)

if sze(0) eq 0 then begin
    sx=1 
    out=''
endif else begin
    sx=sze(1)
    out=strarr(sx)
endelse

for i=0,sx-1 do begin
    div=in(i)
    repeat begin
        res=div mod n
        out(i)=string(byte(res+48+7*(res gt 9)))+out(i)
        div=div / n
    endrep until (div eq 0)

    if n_elements(nbdigit) ne 0 then begin
        l=strlen(out(i))
        case 1 of
            (l lt nbdigit) : repeat out(i)='0'+out(i) until (strlen(out(i)) ge nbdigit)
            (l gt nbdigit) : out(i)=strjoin(replicate('*',nbdigit))
            else:
        endcase
    endif

endfor

return,out
end
