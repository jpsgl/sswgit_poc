pro refresh_link,ff
;+
; $Id: refresh_link.pro,v 1.1 2010/07/02 17:07:04 nathan Exp $
; NAME:
;	refresh_link
; PURPOSE:
;       re-create symbolic link so timestamp is current
;
; CALLING SEQUENCE:
;	refresh_link, ff
; INPUTS:
;       ff  STR or STRARR   symbolic link file
; OUTPUTS:
;       none
;  
; SIDE EFFECTS:
;
; RESTRICTIONS:
;       
; PROCEDURE: removes input iff it is a symbolic link and replace it with same value
;
; $Log: refresh_link.pro,v $
; Revision 1.1  2010/07/02 17:07:04  nathan
; born
;
;-

n = n_elements(ff)
dlm=get_delim()
for i=0,n-1 do begin
    ; remove trailing slash if any
    li=ff[i]
    IF rstrmid(li,0,1) EQ dlm THEN li=strmid(li,0,strlen(li)-1)
    spawn,['ls','-l',li],res,/noshell
    parts=strsplit(res[0],/extract)
    np=n_elements(parts)
    IF parts[np-2] EQ '->' THEN BEGIN
    	spawn,['/bin/rm','-f',li],/noshell
	spawn,['ln','-s',parts[np-1],parts[np-3]],/noshell
    ENDIF ELSE print,li,' not a link; skipping'
endfor
end
