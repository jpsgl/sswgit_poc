pro set_delimiter
; $Id: set_delimiter.pro,v 1.1 2008/02/15 22:49:57 nathan Exp $
;
; PRO SET_DELIMITER
; 
; Defines !delimiter system variable.
;
; Modified:
;
; $Log: set_delimiter.pro,v $
; Revision 1.1  2008/02/15 22:49:57  nathan
; moved from lasco/idl/display because called by scclister.pro
;
; 02/06/19 by N. Rich - Cover linux case.
;
; 06/19/02 @(#)set_delimiter.pro	1.1
;
;-

IF !VERSION.OS EQ 'windows' THEN DEFSYSV,'!DELIMITER', '\' ELSE $      ; Windows
IF !VERSION.OS EQ 'Win32'   THEN DEFSYSV,'!DELIMITER', '\' ELSE $      ; Win95, Win NT
IF !VERSION.OS EQ 'MacOS'   THEN DEFSYSV,'!DELIMITER', ':' ELSE $     ; Macintoshes
IF !VERSION.OS EQ 'vms'     THEN DEFSYSV,'!DELIMITER', ']' ELSE $     ; VMS machines
DEFSYSV,'!DELIMITER', '/'	; all other cases
;IF !VERSION.OS EQ 'OSF'     THEN DEFSYSV,'!DELIMITER', '/'      ; OSF machines
;IF !VERSION.OS EQ 'sunos'   THEN DEFSYSV,'!DELIMITER', '/'      ; SUN machines

return
end
