;+
;
; NAME:
;  SAMPLE_CIRC
;
; PURPOSE:
;  Extract a circular profile from an image
;
; CATEGORY:
; Image Processing, Feature Extraction
;
;
; INPUTS:
;  xc,yc : center of the circle in pix
;  ima : image
;  radius : radius of the circle in pix
;  dr : radial number of steps
;  dtheta : angular step : 1. gives 360 samples
;
;
; OUTPUTS:
;  profile : the profile
;  trace : the mask of the circle trace in the image
;  ierr : error flag: error if 1
;  imaout : image with the trace of the circle
;
; HISTORY:
;
;
; CVSLOG:
;  Revision 1.2  2002/07/11 07:24:11  arnaud
;  Insertion of the Log in each header
;
; $Id: sample_circ.pro,v 1.1 2009/01/30 17:57:43 nathan Exp $
;-
function fsmpe, x, alpha
;
;    Computes four coefficients for interpolation
;
ax = abs(x)
isax = size(ax)
if isax(0) eq 0 then begin
   case 1 of
   ax lt 1. : r = (2.*ax+1.)*(ax-1.)^2 + alpha*(ax-1.)*ax^2
   ax eq 1. : r = 0.
   ax gt 1. and ax lt 2. : r = alpha*(ax-1.)*(ax-2.)^2
   else: r = 0.
   endcase
endif else begin
   r = 0.*ax
   ipos = where( ax lt 1.,nipos )
   kpos = where( ax gt 1. and ax lt 2., nkpos )
   if nipos gt 0 then begin
     bx = ax(ipos)
     r(ipos) = (2.*bx+1.)*(bx-1.)^2 + alpha*(bx-1.)*bx^2
   endif
   if nkpos gt 0 then begin
     bx = ax(kpos)
     r(kpos) = alpha*(bx-1.)*(bx-2.)^2
   endif
endelse
;
return,r
end
;
pro sample_circ,xc,yc,ima,radius,dr,dtheta,profile,trace,ierr,imaout,pmask=pmask
;
nval = 360./dtheta
xp = fltarr(nval)
yp = fltarr(nval)
isze = size(ima)
nx = long(isze(1))
ny = long(isze(2))
trace = intarr(nx,ny)
;
rad_sze = size(radius)
if rad_sze(0) eq 0 then begin
   if rad_sze(1) eq 0 then begin
      message,' radius is not defined',/inform
      return
   endif else begin
      profile = fltarr(nval)
      nz = 1
   endelse
endif else begin
   nz = rad_sze(1)
   profile = fltarr(nval,nz)
endelse
;print,'-------------------------->', size(profile)
angl = findgen(nval)*dtheta*!dtor
cosa = cos(angl)  &  sina = sin(angl)
;
niter = fix(dr > 1.)
ierr = 0
if niter eq 0 then begin
    message,'dr  must be  ge  1.',/inform
    ierr = 1
    return 
endif
;
nr = 0
while nr lt nz do begin
prfl = fltarr(nval)
pmask=replicate(0b,nval)
rdius = radius[nr]
;
xp = (rdius+dr)*cosa+xc
yp = (rdius+dr)*sina+yc

for drr = 0,dr-1 do  begin
;
xp = (rdius+drr)*cosa+xc
yp = (rdius+drr)*sina+yc
ixp = long(xp)
iyp = long(yp)
rxp = xp-float(ixp)
ryp = yp-float(iyp)

mmm=where(ixp gt 0 and iyp gt 0 and ixp lt (nx-2) and iyp lt (ny-2),cnt)
if cnt eq 0 then continue
ixp=ixp[mmm]
iyp=iyp[mmm]
rxp=rxp[mmm]
ryp=ryp[mmm]
pmask[mmm]=1b
;
ipos00 = (ixp-1)+nx*(iyp-1) & ipos01 = (ixp-1)+nx*iyp  
ipos02 = (ixp-1)+nx*(iyp+1) & ipos03 = (ixp-1)+nx*(iyp+2)
;
ipos10 = ixp+nx*(iyp-1)     & ipos11 = ixp+nx*iyp
ipos12 = ixp+nx*(iyp+1)     & ipos13 = ixp+nx*(iyp+2)
;
ipos20 = (ixp+1)+nx*(iyp-1) & ipos21 = (ixp+1)+nx*iyp
ipos22 = (ixp+1)+nx*(iyp+1) & ipos23 = (ixp+1)+nx*(iyp+2)
;
ipos30 = (ixp+2)+nx*(iyp-1) & ipos31 = (ixp+2)+nx*iyp
ipos32 = (ixp+2)+nx*(iyp+1) & ipos33 = (ixp+2)+nx*(iyp+2)
;
trace(ipos11) = 1
;
coefx0 = fsmpe( rxp+1.,-.5)
coefx1 = fsmpe( rxp,-.5)
coefx2 = fsmpe(-rxp+1.,-.5)
coefx3 = fsmpe(-rxp+2.,-.5)
coefy0 = fsmpe( ryp+1.,-.5)
coefy1 = fsmpe( ryp,-.5)
coefy2 = fsmpe(-ryp+1.,-.5)
coefy3 = fsmpe(-ryp+2.,-.5)
;   
out     = coefy0*(ima[ipos00]*coefx0+ima[ipos01]*coefx1+ima[ipos02]*coefx2+ima[ipos03]*coefx3) $
         +coefy1*(ima[ipos10]*coefx0+ima[ipos11]*coefx1+ima[ipos12]*coefx2+ima[ipos13]*coefx3) $
         +coefy2*(ima[ipos20]*coefx0+ima[ipos21]*coefx1+ima[ipos22]*coefx2+ima[ipos23]*coefx3) $
         +coefy3*(ima[ipos30]*coefx0+ima[ipos31]*coefx1+ima[ipos32]*coefx2+ima[ipos33]*coefx3)
prfl[mmm] = out + prfl[mmm]
endfor
;
;
profile[*,nr] = prfl/niter
nr = nr+1
endwhile
;
imaout = ima*(1-trace)
;
return
end
