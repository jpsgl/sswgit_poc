; $Id: db_help_func.pro,v 1.1 2008/02/22 20:45:01 nathan Exp $
;
; Ed Esfandiari, Nov 95
; This function interfaces with a C routine that uses sybase for retrieving
; names of data bases, table names within a database, or column names within
; a table. Input to this pro is either no parameter, one parameter (database 
; name), or two parameters (database name and table name).
; Output is an array of strings containing databases, tables, or fields names.
; This function is the same as db_help procedure; As it is obvious, it returns
; the output instead of just printing it to the screen.

; Ed Esfandiari, Nov 96
; added code to recognize LAS's OSF and use lascos in rsh.

; Ed Esfandiari, Oct 01
; Changed code to eliminate the use of rsh. Instead of using the
; sybsrv account on corona, we are now using a copy of db_info
; that I have placed in $NRL_LIB/lasco/C/lib.


; Examples:
; s=db_help()                       outputs names of existing databases
; s=db_help('solwind')              outputs names of tables in solwind database
; s=db_help('solwind','transient')  outputs names of columns in transient table 
;                                   of solwind database
; $Log: db_help_func.pro,v $
; Revision 1.1  2008/02/22 20:45:01  nathan
; moved from lasco/idl/database
;

function db_help_func,dbase,table 

 cmnd='sp_help '

 arg= n_params() ; get number of input parameters
 if(arg eq 0) then begin 
;   dbase='hk_test'
   dbase='master' ; use master (as dummy) because it will always be there.
   cmnd= 'sp_helpdb'
 end
 if(arg le 1) then table= ''
 if(arg eq 2) then cmnd= cmnd+table 

  valid= 0

; rnum= randomu(seed,10) ; generate 10 random numbers 
; out_file= "op"+string(rnum(7),'(f6.4)')+".dat" ; use the 7th to name the file
; strput,out_file,'_',strpos(out_file,'.')

; get PID of IDL session for unique temporary file name:

; do the following if idl is run from either a VMS or a UNIX operating system:

  machine= 'unknown'
  opsys= strupcase(!version.os)
  if(opsys eq 'OSF') then begin  ; VMS/OSF at LAS
    machine= 'lascos'
    spawn,'show process',result
    idl_pid= STRCOMPRESS(strmid(result(where(strmid(result,8,2) eq '-d')),3,5))
  endif else begin
    if(opsys eq 'VMS') then begin  ; VMS
      machine= 'unknownVMS'
      spawn,'show process',result & result = result(1)
      idl_pid = STRCOMPRESS(strmid(result,strpos(result,"ID:")+3, $
                          strlen(result)),/REMOVE_ALL)
    endif else begin  ; UNIX
      machine= 'corona'
      cmd = '/usr/bin/ps | grep idl | grep -v grep'
      spawn, cmd, /sh, result & result = result(0)
      idl_pid = STRCOMPRESS(STRMID(result, 0, 6),/REMOVE_ALL)
    endelse
  endelse

  out_file= "op"+idl_pid+".dat"
;  print,'out_file= ',out_file

;generate the spawn command and run it:
; spawn_cmnd="rsh -l sybsrv "+machine+" db_info "+dbase+" "+cmnd+" > "+out_file

 spawn_cmnd=GETENV('NRL_LIB')+"/lasco/C/lib/db_info "+dbase+" "+cmnd+" > "+out_file

;print,'command= ',spawn_cmnd

 spawn,spawn_cmnd

;open the text file created by calling the above C routine and format and
;output the table or column names:

 tmp= ''
 openr,unit,out_file,/get_lun,/delete
 while (not eof(unit)) do begin ; read last line to get number of valid entries
   readf,unit,tmp
 end
 if(strpos(tmp,'Unable to connect: SQL Server is unavailable') ne -1) then begin
   print,''
   print,'Sorry, SQL Server is unavailable. Try again at a later time.' 
   print,''
   free_lun,unit
   return,['no data']
 end
 valid= fix(tmp)  ; change to integer 
 point_lun,unit,0 ; rewind the file
 print,''
 if(valid gt 0) then begin
   field=strarr(valid)
   tmp=''
   for i=0,valid-1 do begin
     readf,unit,tmp
     tmp=strtrim(tmp,2)
     field(i)= tmp+'                                  '
   end
   if(arg eq 2) then begin
     for i=valid,valid*2 - 1 do begin
       readf,unit,tmp
       tmp=strtrim(tmp,2)
       field= [field,tmp] 
     end
   end
   free_lun,unit
;   print,format='(2a35)',field
 endif else begin
   if(arg eq 1) then begin
     print,'*** Invalid database name. Correct the problem and try again.'
   end
   if(arg eq 2) then begin
     print,'*** Invalid database and/or table name(s). Correct the problem and try again.'
   end
   free_lun,unit
   field=['no data']
 end
 print,''

 return,field

 end



