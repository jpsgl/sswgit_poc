; $Id: check_permission.pro,v 1.1 2008/03/10 19:01:55 nathan Exp $
;
; check that user has write permission for file or directory
;
; $Log: check_permission.pro,v $
; Revision 1.1  2008/03/10 19:01:55  nathan
; moved from lasco/idl/util
;
FUNCTION CHECK_PERMISSION, file, SILENT=silent

print, file
   BREAK_FILE, file, a, dir, name, ext
   FILE_ACC, dir, exist, read, write, execute, filetype

   IF (exist EQ 0) THEN BEGIN
      IF NOT(KEYWORD_SET(SILENT)) THEN PRINT, 'Error: directory does not exist: ', dir
      RETURN, 0
   ENDIF
   IF (write EQ 0) THEN BEGIN
      IF NOT(KEYWORD_SET(SILENT)) THEN PRINT, 'Error: no write permission for directory: ', dir
      RETURN, 0
   ENDIF

   FILE_ACC, file, exist, read, write, execute, filetype
   IF (exist EQ 1) AND (write EQ 0) THEN BEGIN
      IF NOT(KEYWORD_SET(SILENT)) THEN PRINT, 'Error: no write permission for file: ', file
      RETURN, 0
   ENDIF

   RETURN, 1

END
