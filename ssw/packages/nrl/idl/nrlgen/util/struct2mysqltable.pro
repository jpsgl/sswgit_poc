;+
; $Id: struct2mysqltable.pro,v 1.1 2019/01/15 20:59:00 nathan Exp $
;
; Project     : PSP WISPR
;                   
; Name        : struct2mysqltable
;               
; Purpose     : To convert IDL structure to MySQL table definition 
;               
; Use         : IDL> struct2mysqltable, s
;    
; Inputs      : s		; structure variable
;               
; Outputs     : file in current directory
;               
; Calls       : 
;
; Common      : None.
;               
; Restrictions: Must have write permission for the current directory.
;               
; Side effects: Creates one file in the current directory named:
;		structure_name_struct.sql	;MySQL table definition script
;               
; Category    : Database, Pipeline 
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, June 2018
;               
; Modified    :
;
; $Log: struct2mysqltable.pro,v $
; Revision 1.1  2019/01/15 20:59:00  nathan
; init
;
;-            

;__________________________________________________________________________________________________________
;


PRO struct2mysqltable, s

COMMON cpp_common, idl_out, in

   s_name = rtag_names(s,/structure_name )
   s_tags = rtag_names(s)
   n=n_elements(s_tags)
   out_file_idl = s_name + '_struct.sql'
   PRINT, '%%: Creating output file: ', out_file_idl

      OPENW, IDL_OUT, out_file_idl, /GET_LUN
  
   today = SYSTIME()
   printf, IDL_OUT, '/*  $Id: struct2mysqltable.pro,v 1.1 2019/01/15 20:59:00 nathan Exp $'
   PRINTF, IDL_OUT, ' *  MySQL table definition created by struct2mysqltable.pro'
   PRINTF, IDL_OUT, ' *  on ' + today
   PRINTF, IDL_OUT, ' */'
   printf, IDL_OUT, 'CREATE TABLE `',s_name,'` ('

for i=0,n-1 do begin
    typ=datatype(s.(i))
    case (typ) of
	'BYT': mnval="SMALLINT UNSIGNED "
	'LON': mnval="INT "
	'FLO': mnval="FLOAT "
	'DOU': mnval="DOUBLE "
	'ULN': mnval="INT UNSIGNED "
	'L64': mnval="BIGINT "
	'U64': mnval="BIGINT UNSIGNED "
	'INT': mnval="SMALLINT "
	'UIN': mnval="SMALLINT UNSIGNED "
	'STR': mnval="VARCHAR(80) "
	else:
    endcase

    IF strmid(s_tags[i],0,4) EQ 'DATE' THEN mnval="DATETIME(3) "
    
    printf,IDL_OUT,' `'+s_tags[i]+'` '+mnval+'NOT NULL,'
endfor
printf,IDL_OUT,')'

   CLOSE, IDL_OUT & FREE_LUN, IDL_OUT

END
