;+
; PROJECT:  STEREO/SECCHI
;
; NAME:     resolve_path.pro
;
; PURPOSE:
;  expand shell logicals into full text so that string can be used without shell
;
; CATEGORY: operating system file shell utilities
;
; CALLING SEQUENCE:
;   	fullpath=resolve_path('~/dir1/$ENVVAR/dir2')
;   
; DESCRIPTION:
;   	accepts '/' or '\' or ':' as delimiters; returns correct 
;   	delimiters for current OS
;
; INPUTS:
;   	    dir     STRING
;
; INPUT KEYWORD:
;
; OUTPUTS:
;   	    STRING
;
; CALLED ROUTINES: break_file
;
; HISTORY:
;  $Log: resolve_path.pro,v $
;  Revision 1.4  2011/12/01 21:19:05  nathan
;  fix . and .. cases
;
;  Revision 1.3  2011/12/01 20:02:23  nathan
;  changes for windows compat
;
;  Revision 1.2  2010/12/09 23:00:14  nathan
;  test for parts
;
;  Revision 1.1  2009/07/17 18:13:10  nathan
;  written for use in scc_movieout.pro
;

function resolve_path, inp0

break_file, inp0, di, pa, rfn, sfx

dlm=get_delim()

char1=strmid(inp0,0,1)

; Expand ~
IF char1 EQ '~' THEN pa=expand_tilde(pa)

; Case where input is . or ..
IF strmid(rfn,0,1) eq '.' THEN BEGIN
    pa=sfx+dlm
    sfx=''
ENDIF
char1=strmid(pa,0,1)

parts=strsplit(pa,dlm,/extract)
wp=where(parts,wpn)
IF wpn GT 0 THEN BEGIN
    parts=parts(wp)  
    IF char1 NE dlm and char1 NE '$' and char1 NE '.' THEN parts=['.',parts]
ENDIF ELSE parts=['.']

wd=where(strmid(parts,0,1) EQ '$',nwd)
IF nwd GT 0 THEN for i=0,nwd-1 DO BEGIN
    item=parts[wd[i]]
    len=strlen(item)
    newitem=getenv(strmid(item,1,len-1))
    IF newitem NE '' THEN parts[wd[i]]=newitem
ENDFOR

path=parts[0]

IF strmid(path,0,1) EQ '.' THEN path=file_expand_path(parts[0])
np=n_elements(parts)
IF strmid(path,0,1) NE dlm THEN path=dlm+path
FOR i=1,np-1 DO path=path+dlm+parts[i]

return,di+path+dlm+rfn+sfx
end
