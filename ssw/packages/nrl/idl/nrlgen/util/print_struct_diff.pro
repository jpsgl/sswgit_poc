pro print_struct_diff, h1, h2
;+
; $Id: print_struct_diff.pro,v 1.1 2008/05/09 19:13:23 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : print_struct_diff
;               
; Purpose   : print keyword values of same keyword which are different in two input structures
;               
; Explanation: 
;               
; Use       : 
;    
; Inputs    :   h1, h2	INput structures

; Optional Inputs: 
;               
; Outputs   : prints a table

; Optional Outputs: print to a file
;
; Keywords  :   
;
; Calls from LASCO : 
;
; Common    : 
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : structure utility database fits header
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL/I2, Mar 08
;-               
; $Log: print_struct_diff.pro,v $
; Revision 1.1  2008/05/09 19:13:23  nathan
; calls compare_struct.pro
;

d=compare_struct(h1,h2)
nd=n_elements(d)
IF nd GT 0 THEN BEGIN
    print,'Keyword           S1             S2'
    FOR i=0,nd-1 DO $
    	print,string(d[i].field,'(a-9)'),'= ',h1.(d[i].tag_num_a),' ',h2.(d[i].tag_num_b)
ENDIF ELSE print, 'No differences'
end
