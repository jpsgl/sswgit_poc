;+
; $Id: hex2dec64.pro,v 1.3 2015/08/06 16:26:50 nathan Exp $
;
; Project     : SoloHI     
;                   
; Name        : HEX2DEC64
;               
; Purpose     : Convert hexadecimal representation to decimal integer.
;               
; Explanation : A hexadecimal string is converted to a decimal integer and 
;               can be displayed or returned or both or neither. 
;   	    	Inputs longer than 4 bytes require 64 bit math to come out right.
;               
; Use         : IDL> hex2dec, hex [, decimal, /quiet]
;    
; Inputs      : hex - hexadecimal string
;
; Opt. Inputs : None
;               
; Outputs     : See below
;               
; Opt. Outputs: decimal - the decimal integer equivalent of the input.
;               
; Keywords    : quiet - unless given the decimal number is printed to the
;                       terminal
;
; Calls       : None
;               
; Restrictions: Input must be a string.
;               
; Side effects: None
;               
; Category    : Utils, Numerical
;               
; Prev. Hist. : hex2dec.pro
; 
; $Log: hex2dec64.pro,v $
; Revision 1.3  2015/08/06 16:26:50  nathan
; remove debug msg
;
; Revision 1.2  2015/08/06 16:24:41  nathan
; update to handle values GT 32 bits
;              
;-            

pro hex2dec64,inp,out,quiet=quiet

;
;  trap invalid input
;
if datatype(inp) ne 'STR' then begin
   print,'Error: input must be string.'
   return
endif

;  
;  initialise output etc
;
out = 0L
n = strlen(inp)

;
;  convert each character in turn
;
for i=n-1,0,-1 do begin
  c = strupcase(strmid(inp,i,1))
  case c of
   'A': c = 10
   'B': c = 11
   'C': c = 12
   'D': c = 13
   'E': c = 14
   'F': c = 15
   'X': c = 0
   'H': c = 0
  else: begin
         if not valid_num(c,/integer) then begin
           print,'Invalid character **',c,'**'
           out = 0
           return
         endif
        end
  endcase
  next=ulong64(c)*16LL^(n-1-i)
  ;print,i,fix(c),next
  out = out + next
endfor

;
;  if not silenced, print result
;
if not keyword_set(quiet) then print,out

end
