function fitsquery, dates, keyword, values, SCANDIR=scandir, FILES=files
; $Id: fitsquery.pro,v 1.2 2016/12/13 00:11:40 nathan Exp $
;
; Project   : Solar Orbiter - SoloHI
;                   
; Name      : fitsquery.pro
;               
; Purpose   : returns list of FITS filenames matching specified input
;               
; Explanation: 
;
; Example   : IDL> li=fitsquery([20161128,20161129],'drb_t',[-40,-35],scandir='.')
;    
; Inputs    : 
;   dates   Scalar or vector of values in format YYYYMMDD (can be integer)
;   keyword FITS keyword to search
;   values= range of values to search on, inclusive (scalar or 2-element array) 
;
; Optional Input:
;               
; Outputs   : 	filenames	including path
;
; Keywords  : 
;   SCANDIR= 	directory to use to scan for subdirectories in dates input
;   /FILES  	dates is a List of FITS files to search
;
; Calls     : 
;
; Category    : analysis
;               
; Prev. Hist. : None.
;
; Written     : N. Rich / NRL
;               
; $Log: fitsquery.pro,v $
; Revision 1.2  2016/12/13 00:11:40  nathan
; updates
;
; Revision 1.1  2016/12/12 23:50:04  nathan
; do search on FITS headers
;
;-

shidir=concat_dir(getenv('SOLOHI'),'fits')
IF keyword_set(SCANDIR) THEN shidir=scandir  
IF n_elements(values) eq 1 THEN values=[values,values]
IF keyword_set(FILES) THEN nd=1 ELSE nd=n_elements(dates)
flist=''
FOR i=0,nd-1 DO BEGIN
    IF keyword_set(FILES) THEN li=dates ELSE BEGIN
    	li=findfile(concat_dir(shidir,trim(dates[i])+'/*.fits'))
    	print,'Searching ',trim(n_elements(li)),' files in ',dates[i]
    ENDELSE
    shireadfits,li,h,/nodata
    IF datatype(indx) EQ 'UND' THEN indx=tag_index(h[0],keyword)
    w=where(h.(indx) GE values[0] and h.(indx) LE values[1],nw)
    IF nw GT 0 THEN flist=[flist,li[w]]
ENDFOR

nf=n_elements(flist)
IF (nf LE 1) THEN BEGIN
    message,'No files match criteria',/info
    return,'None'
    
ENDIF ELSE return,flist[1:nf-1]
end
