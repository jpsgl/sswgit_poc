;+
; $Id: pickfiles.pro,v 1.1 2008/02/15 22:49:57 nathan Exp $
;
; Project     : SOHO - LASCO/EIT
;
; Name	      : PICKFILES
;
; Purpose     : 
;       This function allows the user to interactively pick a files.  A files
;       selection tool with a graphical user interface is created.  Files
;       can be selected from the current directory or other directories.
;
; Category    : Widgets
;
; Explanation : 
;
; Use         : result = pickfiles()
;
; Examples    : result = pickfiles()
;
; Inputs      : 
;
; Opt. Inputs : 
;
; Outputs     : 
;       PICKFILES returns a string array that contains the names of the files selected.
;       If no file(s) is selected, PICKFILES returns a null string.
;
; Opt. Outputs:
;
; Keywords    : 
;
;       FILTER: A string value for filtering the files in the file list.
;               The user can modify the default filter value: "*.fts *.mvi". 
;
;       PATH:   The initial path to select files from.  If this keyword is
;               not set, the current directory is used.
;
; Common      :
;
; Restrictions: None.
;
; Side effects: None.
;
; History     :  16-feb-1996,Borut Podlipnik, MPAe,Written
;
; Contact     : BP, borut@lasco1.mpae.gwdg.de
;
;-
; $Log: pickfiles.pro,v $
; Revision 1.1  2008/02/15 22:49:57  nathan
; moved from lasco/idl/display because called by scclister.pro
;



PRO pickfiles_event,event

COMMON pick_block, PFBase, files_f, files_s, files, new_path
 WIDGET_CONTROL,event.id,GET_UVALUE = uval
 WIDGET_CONTROL,event.top,GET_UVALUE = suv

 CASE uval OF

  "NEW_FILTER": BEGIN

                 WIDGET_CONTROL,suv.nf, GET_VALUE=new_filter
                 new_filter = STRCOMPRESS(new_filter(0))

                 WIDGET_CONTROL,suv.np, GET_VALUE=new_path
                 new_path = STRCOMPRESS(new_path(0),/REMOVE_ALL)
                 IF strmid(new_path,strlen(new_path)-1,1) NE !delimiter THEN $
                 new_path = new_path + !delimiter
               
                ; check new_path!

                 CD, new_path, CURRENT = old_dir
                 files_f = findfile(new_filter)
           
                 WIDGET_CONTROL,suv.wl1, SET_VALUE=files_f
                 CD, old_dir
                END

  "NEW_PATH": BEGIN
               WIDGET_CONTROL,suv.np, GET_VALUE=new_path
               new_path = STRCOMPRESS(new_path(0),/REMOVE_ALL)
               IF strmid(new_path,strlen(new_path)-1,1) NE !delimiter THEN $
               new_path = new_path + !delimiter
               

               WIDGET_CONTROL,suv.nf, GET_VALUE=new_filter
               new_filter = STRCOMPRESS(new_filter(0))

               IF new_path EQ !delimiter THEN BEGIN
                 tmp = PICKFILE(GET_PATH=new_path,PATH=GETENV('IMAGES'))
                 WIDGET_CONTROL,suv.np, SET_VALUE=new_path
               ENDIF ELSE BEGIN
                 chk = chk_dir(new_path)
                 IF chk EQ 0 THEN BEGIN
                    msg = WIDGET_MESSAGE("Directory not found!")
                 ENDIF ELSE BEGIN

                    WIDGET_CONTROL,suv.wl1, SET_VALUE=''

                    CD, new_path, CURRENT = old_dir

                    files_f = findfile(new_filter)

                    IF files_f(0) EQ '' THEN msg = WIDGET_MESSAGE("No files found!")

                    WIDGET_CONTROL,suv.wl1, SET_VALUE=files_f
                    CD, old_dir

                 ENDELSE
               ENDELSE
              END

  "SELECT1": BEGIN
               tmp = files_f(event.index)
               tmp = new_path + tmp
               IF files_s(0) EQ '' THEN BEGIN
                 files_s = tmp
               ENDIF ELSE BEGIN
                 ind = WHERE(files_s EQ tmp)
                 IF ind(0) EQ -1 THEN files_s = [files_s,tmp]
               ENDELSE
              
               WIDGET_CONTROL,suv.wl2, SET_VALUE=files_s
               
             END

  "SELECT2": BEGIN
               files_s(event.index) = ''
               ind = WHERE(files_s GT '')
               
               IF ind(0) EQ -1 THEN BEGIN
                 files_s = ''
               ENDIF ELSE BEGIN
                 files_s = files_s(ind)
               ENDELSE

               WIDGET_CONTROL,suv.wl2, SET_VALUE=files_s
    
             END

 "SELALL": BEGIN
             files_s = new_path + files_f
             WIDGET_CONTROL,suv.wl2, SET_VALUE=files_s           
           END

   "SAVE" : BEGIN
              IF STRLEN(files_s(0)) EQ 0 THEN BEGIN
                wmessage,title='Warning',text='No files selected!', $
                         xsize=25,ysize=2
               RETURN
              ENDIF

              wd = GETENV('WORK')+!delimiter+GETENV('USER')+!delimiter+'list'+!delimiter
              IF chk_dir(wd) EQ 0 THEN wd = GETENV('HOME') + !delimiter

              fname = "working.lst"
              xvaredit, fname
 
              wdn= wd + fname
              
              wrt_asc,wdn,files_s
            END

   "CLEAR":  BEGIN

             files_s = ['']

             WIDGET_CONTROL,suv.wl2, SET_VALUE=files_s

            END

        "Done":  WIDGET_CONTROL, event.top, /DESTROY
                                  
	"HELP": BEGIN
                  wmessage,title='Help on Pickfiles',text='Coming soon...'

		END

       "CANCEL": BEGIN
                  
                  WIDGET_CONTROL,event.top,/DESTROY
                 END

ELSE: donothing = 0
ENDCASE


END


FUNCTION pickfiles,FILTER=FILTER, PATH=PATH

COMMON pick_block, PFBase, files_f, files_s, files,new_path

IF (XRegistered("pickfiles") NE 0) THEN RETURN, -1

IF NOT KEYWORD_SET(FILTER) THEN filter = "*.fts *.mvi"

new_filter = filter
cd, CURRENT = new_path

files_s = ''
 
IF KEYWORD_SET(PATH) THEN new_path = path

files = ''

xs = 40

IF n_elements(new_path) EQ 0 then cd, CURRENT = new_path

PFBase = WIDGET_BASE(TITLE = "Pickfiles ",/FRAME,/COLUMN)

bl1 = WIDGET_BASE(PFBase,/COLUMN)
bl2 = WIDGET_BASE(bl1,/ROW)
li2 = WIDGET_LABEL(bl2,VALUE = "Input File and Path:")
r3  = WIDGET_BASE(bl1, /FRAME,/COLUMN)
r7  = WIDGET_BASE(r3, /ROW)
r1  = WIDGET_BASE(r7, /ROW)
l1  = WIDGET_LABEL(r1,VALUE = 'Filter: ') 
w5  = WIDGET_BASE (r1, /ROW) 
nf  = WIDGET_TEXT (w5,XSIZE=xs,/EDITABLE, VALUE=new_filter,UVALUE='NEW_FILTER') 

r2  = WIDGET_BASE(r3, /ROW)
l2  = WIDGET_LABEL(r2,VALUE = 'Path  : ')
w5  = WIDGET_BASE (r2, /ROW)
np  = WIDGET_TEXT (w5,XSIZE=xs,/EDITABLE, VALUE=new_path,UVALUE='NEW_PATH')

	
lcol4    = WIDGET_BASE(PFBase, /FRAME, /ROW)
int_base = WIDGET_BASE(lcol4,/COLUMN)

l3  = WIDGET_LABEL(int_base,VALUE = 'Files Found  : ')
wl1 = WIDGET_LIST(int_base,UVALUE='SELECT1', XSIZE=50, YSIZE=10)
l3  = WIDGET_LABEL(int_base,VALUE = 'Selection  : ')
wl2 = WIDGET_LIST(int_base,UVALUE='SELECT2', XSIZE=50, YSIZE=10)                 

sel_base = WIDGET_BASE(int_base,/ROW)
p_ss = WIDGET_BUTTON(sel_base,VALUE='Save selection',UVALUE='SAVE')
;p_sel2 = WIDGET_BUTTON(sel_base,VALUE='Insert',UVALUE='INSERT')
p_go = WIDGET_BUTTON(sel_base,VALUE="   Clear    ",UVALUE='CLEAR')
p_go = WIDGET_BUTTON(sel_base,VALUE="    All     ",UVALUE='SELALL')


sel_base = WIDGET_BASE(PFBase,/ROW,/FRAME)
p_sel0 = WIDGET_BUTTON(sel_base,VALUE="     Ok     ",UVALUE='Done')
p_sel2 = WIDGET_BUTTON(sel_base,VALUE="   Cancel   ",UVALUE='CANCEL')
p_sel3 = WIDGET_BUTTON(sel_base,VALUE="    Help    ",UVALUE='HELP')
;p_sel4 = WIDGET_BUTTON(sel_base,VALUE='Select',UVALUE='S_SEL')


WIDGET_CONTROL, PFBase, /REALIZE

wiuv = { PFBase:PFBase, $
         wl1:wl1,         $
         wl2:wl2,       $  
         nfilter:new_filter, $
         nf:nf, $
         np:np $
       } 

WIDGET_CONTROL, PFBase, SET_UVALUE = wiuv

CD, new_path, CURRENT = old_dir
new_path = new_path + !delimiter

tmp = 'ls ' +  new_filter

spawn,tmp,files_f

IF files_f(0) EQ '' THEN files_f = ''

WIDGET_CONTROL,wl1, SET_VALUE=files_f

CD, old_dir

XMANAGER,"pickfiles",PFBase, $               
                EVENT_HANDLER = "pickfiles_event", $
                GROUP_LEADER = GROUP, $
                /MODAL

RETURN,files_s
END








