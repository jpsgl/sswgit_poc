
;+
; $Id: setyesno.pro,v 1.1 2007/04/06 17:35:26 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : 
;               
; Purpose   : 
;               
; Explanation: 
;               
; Use       : IDL> 
;    
; Inputs    : optional infile: filename to process
;               
; Outputs   : file T 
;
; Keywords  : A, B
;
;   A     is a
;               
; Calls from LASCO : none 
;
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : A,B
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRL, Nov-Dec 2005
;               
; $Log: setyesno.pro,v $
; Revision 1.1  2007/04/06 17:35:26  antunes
; Supplemental file needed to move 'match_polarity' from dev to nrlgen.
;
; Revision 1.1  2006/04/11 15:55:18  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.4  2006/03/20 16:49:09  antunes
; New 'rendering rotation test' routine complete.
; 'dragger' now can be embedded in other applications, though it
; still uses evil common blocks and requires additional event
; handlers if embedded.
;
; Revision 1.3  2006/03/09 20:10:40  antunes
; Better graphic handling.
;
; Revision 1.2  2006/01/09 17:14:42  antunes
; More commented added.
; New 'super3drv' driver routine for getting user specs for reconstruction
; process now checked in.  Just type 'super3drv' to test.
;
;-            
; takes input of 0,1,yes,no, etc and returns 0 (0/no) or 1 (all others)

FUNCTION setyesno,input

  if (keyword_set(input)) then input=1
  if (n_elements(input) eq 0) then input='no'
  ret=1
  if (datatype(input) eq 'STR') then begin 
      if (strcmp(input,'0',1)) then ret=0
      if (strcmp(input,'n',1,/fold_case)) then ret=0
  end else if (input eq 0) then begin
      ret=0
  end

  return,ret

END
