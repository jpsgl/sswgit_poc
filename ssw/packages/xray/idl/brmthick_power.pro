;+
;
; NAME: brmthick_power.pro
;
;
; PURPOSE:
;            compute  nonthermal electron energy flux at ONE time interval
;            The input  model parameters are of bremsstrahlung thick-target model
; OUTPUT:
;               returne  the  nonthermal energy flux at one time interval
;
;
; CATEGORY:
;       SPECTRA, XRAYS
;
; CALLING SEQUENCE:
;       brmthick_power, electronflux, lowind, brkeng, highind, lowcut, highcut
;
; CALLS:
;       brm_distrn.pro, QROMB.pro
;
; INPUTS:   6 fitting parametes for the thick-target model which includes:
;             eletronflux; integrated electron flux/1.0e35     (electron s^-1)
;                   lowind:       lower power-law index
;                   brkeng:       break energy                               (keV)
;                   highind:      higher power-law index
;                   lowcut:       Low-energy cutoff                      (keV)
;                   highcut;       High-energy cutoff                     (keV)
;
; History:
;                Linhui Sui (linhui.sui@gsfc.nasa.gov), 09/2005
; Modified:
; 10-Dec-2008, Kim Tolbert.  Added eps keyword.  Since input args to qromb are double precision,
;   epsilon in qromb defaults to 1.e-12.  This small epsilon sometimes causes a crash with the
;   message: QROMB: Too many steps in routine qromb.  Probably only need epsilon=1.e-6.
; 18-Dec-2008, Kim Tolbert.  Changed order of calculation of coeff to avoid overflow.
; 22-Aug-2012, Kim Tolbert.  Limit break energy and high energy cutoff to 3.2e4 keV before calling
;   QROMB (higher values don't contribute anything, but can cause QROMB to crash with 'too many iterations')
;
;-

;integra of nonthermal energy
Function total_nontherm, gamma

common thickindex, p, q, gamlow, gambrk, gamhigh

brm_distrn, gamma, gamlow, gambrk, gamhigh, p, q, fcn

 return, fcn * (gamma - 1.0)
 ; return, fcn * (gamma - 1.0) * sqrt(1.0 - 1.0/gamma^2)  ;for density distribution

end


Function brmthick_power, electronflux, lowind, brkeng, highind, lowcut, highcut, eps=eps

common thickindex, low, high, gamlow, gambrk, gamhigh

low = lowind
high = highind

;constants
mc2 = 510.98d+00
clight = 2.9979d+10
kev2erg = 1.6d-9

brk_new = brkeng < 3.2e4
high_new = highcut < 3.2e4

;convert input parameter to useful form
gamlow = lowcut / mc2 + 1.0d0
gambrk = brk_new / mc2 + 1.0d0
gamhigh = high_new / mc2 + 1.0d0

lowengflux = QROMB('total_nontherm', gamlow, gamhigh, eps=eps)

 coeff =  1.0e35 * kev2erg * mc2 * electronflux 
 ; coeff =  na * 1.0e25 * kev2erg * mc2 * clight   ;for density distribution

totengflux = coeff * lowengflux    ;energy flux

return, totengflux

end

