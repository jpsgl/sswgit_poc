;+
;
; NAME:
; 		F_MULTI_THERM_ABUN_EXP
;
; PURPOSE:
; This function returns the photon spectrum seen at the Earth
; for a multithermal model (optically thin thermal bremsstrahlung function,
; normalized to the Sun-Earth distance)
; The differential emission measure has an exponential dependence on temperature.
; Same as f_multi_therm_exp except relative abundance for all low-fip elements are 
; controlled by separate parameters.
;
; CATEGORY:
;       SPECTRA, XRAYS
;
; CALLING SEQUENCE:
;       Flux = f_multi_therm_abun_exp( eph, a )
;
; CALLED BY: f_mth_exp_bpow
;
; CALLS:
;       f_multi_therm
;
; INPUTS:
; eph - energy vector in keV, 2N edges or N mean energies
; a -   model parameters defined below
;   a(0) = differential emission measure at T = 2 keV in units of 10^49 cm^(-3) keV^(-1)
;   a(1) = minimum plasma temperature in keV
;   a(2) = maximum plasma temperature in keV
;   a(3) = temperature scale length in keV for calculating the differential emission measure
;          at temperature T:  DEM(T) = a(0) * exp( (2. - T) / a(3) )
;   a[4]  Relative abundance for Fe and Ni
;   a[5]  Relative abundance for Ca
;   a[6]  Relative abundance for S
;   a[7]  Relative abundance for Si
;            Abundances are relative to coronal abundance for Chianti
;            Relative to solar abundance for mewe
;           (unless user selects a different abundance table manually)
;
; WRITTEN: Linhui Sui, 2003/08/28
; Modifications:
; 18-Jul-2006, RAS. Made this a wrapper for f_multi_therm.  See f_multi_therm
;   for more detailed documentation.
; 12-May-2008, Kim.  Renamed ..._abun... because now has separate parameters for
;	abundance of Fe/Ni, Ca, S, Si, and call f_multi_therm with /sep_abun.
;
;-

function f_multi_therm_abun_exp, eph, a, rel_abun=rel_abun, _extra=_extra

return, f_multi_therm(/exponential, eph, a, rel_abun=rel_abun, /sep_abun, _extra=_extra)
end
