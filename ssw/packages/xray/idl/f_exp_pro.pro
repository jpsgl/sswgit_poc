;+
; Name: f_exp_pro
; Procedure version of f_exp function
; Previously called f_exp, renamed to f_exp_pro since wanted f_exp to be a function.
; Written: Richard ?
; Modifications: Renamed to f_exp_pro, Kim Tolbert, 11-Dec-2009
;- 
pro f_exp_pro_test
x=findgen(100)+1.
a = [.05,.01,0]
f_exp_pro, x, a, z
y = [z[2:8],z[80:85]]
e = [x[2:8],x[80:85]]
y[3]= .07
y[0]= .055
y[6]=.04
w = curvefit (e,y,fltarr(13)+1., a, chisq=chi, fita=[1,1,0],function_name='f_exp_pro', status=status)
plot,x,z
oplot, e,w,psym=2
oplot,e,y,psym=4
f_exp_pro, x, a, full
linecolors
oplot,x,full,color=2

print,a
print,chi,status

stop
end

pro f_exp_pro, x, a, f, pder

if (size(x))(0) eq 2 then edge_products, x, mean=xm else xm=x
if n_elements(a) eq 2 then a = [a,0.]

exp_a1x = exp(xm * a[1])
f = a[0] * exp_a1x + a[2]

if n_params() eq 4 then pder = [ [exp_a1x], [a[0] * xm * exp_a1x], [replicate(1., n_elements(xm))] ]

end
