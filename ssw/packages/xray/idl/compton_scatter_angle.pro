;+
; Name: Compton_scatter_angle
; 
; Purpose: This function computes the maximum value of the cosine of the Compton scattering angle
;   which corresponds to the minumum scattering of the photon for the given low energy limit
; 
; Usage: 
;   scatter_angle = compton_scatter_angle( photon_energy, electron_energy, valid = valid, nvalid = nvalid )
;   
; Inputs:
;   photon_energy - Energy of Compton scattered incident photons in keV, may be a vector
;   electron_energy - Energy of Comton scattered electron, may be a vector
;     If photon_energy and electron_energy are vectors they must be the same shape
; Keyword Inputs:
;   CLIP- Set value of first high energy electron energy where the result is lt -1 to -1 and remove
;      all higher energy returns.  Default CLIP is 1
; Keyword Outputs:
;   VALID - indices of returned result that are valid, i.e. gt -1, if VALID is -1 there are no valid returns
;     and NVALID will be 0
;   NVALID - number of valid indices
; Return:
;   returns the cosine of the scatter angle, values less than -1 are not valid
; Restrictions - electron energy must be monotonically increasing  
; History: 
;   Based on photon/electron kinematics
;   17-oct-2014, richard.schwartz@nasa.gov
;-
function compton_scatter_angle, photon_energy, electron_energy, valid = valid, nvalid = nvalid, $
  clip=clip

default, clip, 1
mu = 1.0 - f_div( electron_energy , ( photon_energy - electron_energy ) * ( photon_energy / 511.0 ) )
z = where( photon_energy eq electron_energy, nz)
if nz eq 1 then mu[z] = 1.0e6
if keyword_set( clip ) then begin
  valid = where( mu ge -1.0 and mu le 1.0001, nvalid)
  if mu[ nvalid - 1 ]  gt -1.0 and n_elements( mu ) gt nvalid then begin
      nvalid++
      valid = lindgen( nvalid )
      mu[ nvalid -1 ] = -1
  endif
  return, mu[ valid ]
  endif
sel = where( photon_energy le electron_energy, nsel)
if nsel ge  1 then mu[sel] = -1.e6 ;scattered electron energy can't equal incident photon energy
valid = where( mu ge -1, nvalid)
return, mu
end