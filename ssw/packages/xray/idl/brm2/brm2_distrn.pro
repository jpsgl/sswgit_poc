;+
; PROJECT:
;	HESSI
;
; NAME:
;	Brm2_Distrn
;
; PURPOSE:
;   This routine calculates the value of the electron distribution function
;   at electron energy eel.  
;   The distribution function is normalized so that the integral from 
;   eelow to eehigh is 1.  
;
; CATEGORY:
;	HESSI, Spectra, Modeling
;
; CALLING SEQUENCE:
;    Brm2_Distrn, eel, p, q, eelow, eebrk, eehigh, fcn
;
; CALLS:
;	none
;
; INPUTS:
;
;   eel  - Two-dimensional array of abscissas calculated in
;          Pro Brm_GauLeg[53 or 54].
;
;   p    - Power-law index of the electron distribution function below
;          eebrk. 
;
;   q    - Power-law index of the electron distribution function above
;          eebrk. 
;
;   eelow      -   Low energy cut-off. 
;
;   eebrk      -   Break energy. 
;
;   eehigh     -   High energy cut-off. 
;   
;
; OPTIONAL INPUTS:
;	none
;
; OUTPUTS:
;	fcn		-	Array containing the normalized electron distribution function
;               for each element of the input array eel.
;
; OPTIONAL OUTPUTS:
;	none
;
; KEYWORDS:
;	none
;
; COMMON BLOCKS:
;	none
;
; SIDE EFFECTS:
;
;
; RESTRICTIONS:
;
;
; PROCEDURE:
;   A normalized, double power law electron distribution function is computed.
;   p is the lower power-law index, between eelow and eebrk, and q is the
;   upper power-law index, between eebrk and eehigh.
;
;
; MODIFICATION HISTORY:
;   Version 1, Gordon.D.Holman@nasa.gov, 23 July 2001
;   IDL version:  Sally House, summer intern
;	Documentation for the Fortran version of this code can be found at
;   http://hesperia.gsfc.nasa.gov/hessi/modelware.htm
;
;;   Version 2:
;   04/20/2009  Yang Su      Update this subroutine for ThinTarget Version 2. 
;                            Changed from function of relativistic gammas to function of energies (in keV).
;   05/12/2009  Gordon Holman  Updated documentation, revised normalization coefficient
;   13-Jan-2010, Kim Tolbert. Remove first If test - not necessary and bug (F_E should be Fcn)                                          
;                                           
;-

Pro Brm2_Distrn, eel, p, q, eelow, eebrk, eehigh, fcn

            ;   Obtain normalization coefficient, norm.
                    
            n0 = (q-1.d0) / (p-1.d0) * eebrk^(p-1) * eelow^(1-p)
            n1 =  n0 - (q-1.d0) / (p-1.d0) 
            n2 = (1.d0- eebrk^(q-1) * eehigh^(1-q)) 
    
            norm = 1.d0/(n1+n2)

            Fcn=make_array(size(eel,/dimensions),/double,value=0.d0)
;            If (Where(eel lt eelow))[0] ne -1 then  $
;                    F_E[Where (eel lt eelow)] = 0.d0
            If (Where((eel lt eebrk) and (eel ge eelow)))[0] ne -1 then  $
                    Fcn[Where((eel lt eebrk) and (eel ge eelow))] = norm * n0 * (p-1.d0) * eel[Where((eel lt eebrk) and (eel ge eelow))]^(-p) * eelow^(p-1.d0)
            If (Where((eel le eehigh) and (eel ge eebrk)))[0] ne -1 then  $
                    Fcn[Where((eel le eehigh) and (eel ge eebrk))] = norm * (q-1.d0) * eel[Where((eel le eehigh) and (eel ge eebrk))]^(-q) * eebrk^(q-1.d0)

END