;+
; Name: plot_electron_distribution
; 
; Purpose: Plot the electron distribution (also called electron spectrum) for thick target models in a plotman window
; 
; Inputs:
;   energy - array of energy edges (2xn) to calculate distribution for
;   par - array of thick target parameters (see f_thick... routines for description)
;   
; Input Keywords:
;   func - name of function (for label on plot), optional
;   interval - fir interval number (for plotman window description), optional
;   
; Input/Output Keywords:
;   plotman_obj - if passed in will use that plotman instance.  If not will create one and pass it out.
;   
; Written: Kim Tolbert, 26-Jun-2014
; Modifications:
; 
;-

pro plot_electron_distribution, energy, par, plotman_obj=plotman_obj, func=func, interval=interval

edf = calc_electron_distribution(energy, par)
xy_obj = obj_new('xyplot', energy, edf)
id = 'Electron Distribution'
xtitle = 'Energy (keV)
ytitle = '1.e35 electrons s!u-1!n keV!u-1!n'
xy_obj->set, id=id, data_unit=ytitle, /xlog, /ylog
desc = id + (exist(interval) ? ' Interval ' + trim(interval) : '')
label = exist(func) ? func + ' ' : ''
label = label + arr2str(trim(par, '(g9.3)'), ',')
xy_obj->plotman, plotman_obj=plotman_obj, xtitle=xtitle, /replace, nodup=0, desc=desc, label=label, legend_loc=2
destroy,xy_obj

end