;+
; PROJECT:
;	HESSI
;
; NAME:
;	Brm2_Frc_Distrn_vnorm
;
; PURPOSE:
;	This routine provides the integral of an electron flux distribution function 
;    of the form (eel + eebrk)^(-p) (inner integral of the brm2 thick-target
;    bremsstrahlung calculation) from eel to eehigh. 
; The distribution function is normalized to the electron spectral flux 
;    at reference energy eeref, Fref = F(eeref). The value of eeref is specified 
;    in this routine to be 50 keV.  
;    If eelow < eeref < eehigh, the normalization is the
;    electron spectral flux (electrons per sec per keV) at eeref. 
;    Outside this range, it is the flux of the extension of the distribution
;    function to electron energy eeref.  
;    NOTE: This subroutine gives a spectral flux of one at eeref.  The value
;    of the normalization (Fref) is determined in another routine.  This subroutine
;    differs from Brm2_F_Distrn, which is normalized to the integrated electron
;    flux (electrons per sec).  
;This distribution function is flat at low energies (eel << eebrk) and a single
;    power law at high energies (eel >> eebrk).  It is the distribution function
;    in the presence of return-current losses in the simple 1D model with 
;    collisional losses dominating in the thick-target region and return-current
;    losses dominating above the thick-target region described in 
;    Holman, G. D. 2012, "Understanding the Impact of Return-Current Losses on 
;    the X-Ray Emission from Solar Flares, Ap. J., 745, 52.  In this model, 
;    eebrk = V, the value of the potential drop from the location a single-
;    power-law electron distribution with a sharp low-energy cutoff, E_c, is 
;    injected, to the top of the thick-target region.  In this model, E_c = eelow + V
;    and the high-energy cutoff of the injected electron distribution is eehigh + V.
;    
;    The integrated electron flux, F_0, is given by
;    F_0 = Fref*(eeref+eebrk)^p * ((eelow+eebrk)^(1-p) - (eehigh+eebrk)^(1-p))/(p-1)
;    
; CATEGORY:
;	HESSI, Spectra, Modeling
;
; CALLING SEQUENCE:
;	Brm2_Frc_Distrn_vnorm, eel, p, q, eelow, eebrk, eehigh, F_E
;
; CALLS:
;	none
;	
;	CALLED BY:
;	brm2_fouter.pro
;
; INPUTS:
;
;   eel  - Two-dimensional array of abscissas calculated in
;            Pro Brm_GauLeg[53 or 54].
;
;   p    - Power-law index of the electron distribution function. 
;   
;   q    - Not used in this routine.
;
;   eelow      -   Sharp low-energy cut-off. 
;
;   eebrk      -   Break energy between upper power law and lower flat distribution. 
;
;   eehigh      -   Sharp high-energy cut-off. 
;   
;
; OPTIONAL INPUTS:
;	none
;
; OUTPUTS:
;	F_E		-	Array containing the integrated electron flux distribution function
;				for each element of the input array eel.
;
; OPTIONAL OUTPUTS:
;	none
;
; KEYWORDS:
;	none
;
; COMMON BLOCKS:
;	none
;
; SIDE EFFECTS:
;
;
; RESTRICTIONS:
;
;
; PROCEDURE:
;   The integral of a normalized, single-power-law electron distribution function
;   that becomes flat well below eebrk, proportional to (eel + eebrk)^{-p}, is computed.
;   p is the power-law index, between eelow and eehigh, and eebrk is the
;   transition energy between the upper, power-law and the lower, flat distribution.
;
;
; MODIFICATION HISTORY:
;   Version 1 of this subroutine, Gordon.D.Holman@nasa.gov, 08 February 2013
;                                           
;-

Pro Brm2_Frc_Distrn_vnorm, eel, p, q, eelow, eebrk, eehigh, F_E

            eeref = 50.  ;The value of the reference electron energy in keV.
            
            fnorm = (eeref + eebrk)^p / (p-1.d0)
            
            F_E = make_array(size(eel,/dimensions),/double,value=0.d0)
            
            If (Where(eel lt eelow))[0] ne -1 then  $
                    F_E[Where(eel lt eelow)] = (eelow + eebrk)^(1.d0 - p) - (eehigh + eebrk)^(1.d0 - p)

            If (Where((eel le eehigh) and (eel ge eelow)))[0] ne -1 then  $
                    F_E[Where((eel le eehigh) and (eel ge eelow))] = (eel[Where((eel le eehigh) and (eel ge eelow))] + eebrk)^(1.d0 - p) - (eehigh + eebrk)^(1.d0 - p)
                    
            F_E = fnorm * F_E

END