;+
; PROJECT:
;	HESSI
;
; NAME:
;	Brm2_Fouter
;
; PURPOSE:
;	Returns the integrand for the outer bremsstrahlung integration.
;
; CATEGORY:
;	HESSI, Spectra, Modeling
;
; CALLING SEQUENCE:
;	Result = Brm2_Fouter(eel, eph, eelow,  eebrk, eehigh, p, q, z)
;
; CALLS:
;	Brm_BremCross
;	Brm_ELoss
; Brm2_F_Distrn
;
; INPUTS:
;
;	  eel		-	Two-dimensional array of abscissas calculated in
;				    Pro Brm_GauLeg[53 or 54].
;
;	  eph		-	Array of photon energies.  Each element corresponds to a column
;				    of the two-dimensional array eel.

;   eelow      -   Low energy cut-off.  This input is not
;               used in Fouter, but is passed to Brm2_F_Distrn.
;
;   eebrk      -   Break energy.  This input is not
;               used in Fouter, but is passed to Brm2_F_Distrn.
;
;   eehigh      -   High energy cut-off.  This input is not
;               used in Fouter, but is passed to Brm2_F_Distrn.
;
;   p    - Power-law index of the electron distribution function below
;          eebrk. This input is not used in Fouter, but is passed to Brm2_F_Distrn.
;
;   q    - Power-law index of the electron distribution function above
;          eebrk. This input is not used in Fouter, but is passed to Brm2_F_Distrn.
;
;	  z		 - Mean atomic number of the target plasma.  This input is not
;				   used in Fouter, but is passed to Brm_BremCross.
;				
;
; OPTIONAL INPUTS:
;	none
;
; OUTPUTS:
;   Fouter	-	Array of integrands (for inner integration)
;					corresponding to input array eel.
;
;
; OPTIONAL OUTPUTS:
;	none
;
; KEYWORDS:
;	
;	  independent_norm   -   Normalization specified at a fixed photon energy, provided in ref_energy
;	                         keyword, usually 50 keV.
;	                         If set, Brm2_F_Distrn_vnorm is called instead of Brm2_F_Distrn.
;	  
;	  return_current   -   If set, Brm2_Frc_Distrn_vnorm is called instead of Brm2_F_Distrn.
;	  
;	  ref_energy  -        Reference energy for normalization in keV (used if independent_norm is set)
;
; COMMON BLOCKS:
;	none
;
; SIDE EFFECTS:
;
;
; RESTRICTIONS:
;
;
; PROCEDURE:
;
;
; MODIFICATION HISTORY:
;   Version 1, holman@stars.gsfc.nasa.gov, 23 July 2001
;   IDL version:  Sally House, summer intern
;   Documentation is provided in the file $SSW/packages/xray/doc/brm_thick_doc.pdf
;   (Adobe Acrobat Reader required).
;
;   Version 2:     This is used in the new, faster version the thick target model functions.
;   12/02/2009 Yang Su       Change the code for the purpose of reversing the order of integral
;   04/14/2009  Yang Su      Change the integral into it's logarithm form to 
;                                           make it converge much faster. Nothing is changed in this subroutine.
;   04/17/2009  Yang Su      Split the integral into 3 parts. Nothing is changed in this subroutine.
;   05/26/2011  Kim Tolbert  Added independent_norm keyword, if set call Brm2_F_Distrn_vnorm (for thick2_vnorm)  
;   08-Feb-2013 Kim Tolbert  Added return_current keyword. If set call Brm2_Frc_Distrn_vnorm (for thick2_rc)
;   19-Mar-2013 Gordon Holman Added to header doc.  
;   26-Jun-2014, Kim Tolbert.  Added ref_energy input keyword, and pass through if /independent_norm
;   09-Jul-2014, Kim Tolbert.  Pass ref_energy pass through if /return_current                            
;-

Function Brm2_Fouter, eel, eph, eelow, eebrk, eehigh, p, q, z, $
  independent_norm=independent_norm, ref_energy=ref_energy, return_current=return_current, _extra=_extra

    l = Where(eel)
    
    mc2 = 510.98d+00
    clight = 2.9979d+10

    gamma = (eel/mc2) + 1.D0
    
    Brm_BremCross, eel, eph(l MOD N_Elements(eph)), z, cross
    
    Brm_ELoss, eel, dedt
    
    pc = SqRt(eel*(eel+2.d0*mc2))
    
    ; inner integration either with independent normalization or not
    case 1 of
      keyword_set(return_current):   Brm2_Frc_Distrn_vnorm, eel, p, q, eelow, eebrk, eehigh, F_E, ref_energy=ref_energy
      keyword_set(independent_norm): Brm2_F_Distrn_vnorm, eel, p, q, eelow, eebrk, eehigh, F_E, ref_energy=ref_energy      
      else: Brm2_F_Distrn, eel, p, q, eelow, eebrk, eehigh, F_E 
    endcase

    Fouter =F_E * cross * pc / dedt / gamma    
    
    Return, Fouter

END