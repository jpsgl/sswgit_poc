;+
; PROJECT:
;	HESSI
;
; NAME:
;	Brm2_F_Distrn_vnorm
;
; PURPOSE:
;	This routine provides the integral of the double power-law electron 
;    flux distribution function (inner integral) from eel to eehigh. 
; The distribution function is normalized to the electron spectral flux 
;    at reference energy eeref.  If eelow < eeref < eebrk, the normalization
;    is the electron spectral flux (electrons per sec per keV) at eeref. 
;    Outside this range, it is the flux of the extension of the lower
;    power-law (index p) to electron energy eeref.  If eebrk < eeref < eehigh,
;    the spectral flux at eeref is (eebrk/eeref)^(q-p) times the value of the 
;    normalization.  
;NOTE: This subroutine gives the unnormalized distribution function.  The value
;    of the normalization is determined in another routine.  This subroutine
;    differs from Brm2_F_Distrn, which is normalized to the integrated electron
;    flux (electrons per sec).  
;    
; CATEGORY:
;	HESSI, Spectra, Modeling
;
; CALLING SEQUENCE:
;	Brm2_F_Distrn, eel, p, q, eelow, eebrk, eehigh, F_E
;
; CALLS:
;	none
;
; INPUTS:
;
;   eel  - Two-dimensional array of abscissas calculated in
;            Pro Brm_GauLeg[53 or 54].
;
;   p    - Power-law index of the electron distribution function below
;          eebrk. 
;
;   q    - Power-law index of the electron distribution function above
;          eebrk. 
;
;   eelow      -   Low energy cut-off. 
;
;   eebrk      -   Break energy. 
;
;   eehigh      -   High energy cut-off. 
;   
; INPUT KEYWORDS:
;   ref_energy - reference energy for thick2_vnorm function
;   
; OPTIONAL INPUTS:
;	none
;
; OUTPUTS:
;	F_E		-	Array containing the integrated electron flux distribution function
;				for each element of the input array eel.
;
; OPTIONAL OUTPUTS:
;	none
;
; KEYWORDS:
;	none
;
; COMMON BLOCKS:
;	none
;
; SIDE EFFECTS:
;
;
; RESTRICTIONS:
;
;
; PROCEDURE:
;   The integral of a normalized, double power law electron distribution function is computed.
;   p is the lower power-law index, between eelow and eebrk, and q is the
;   upper power-law index, between eebrk and eehigh.
;
;
; MODIFICATION HISTORY:
;   Version 1 of this subroutine, Gordon.D.Holman@nasa.gov, 23 May 2011
;   26-Jun-2014, Kim Tolbert.  Added ref_energy input keyword. Previously hard-coded to 50.
;                                           
;-

Pro Brm2_F_Distrn_vnorm, eel, p, q, eelow, eebrk, eehigh, F_E, ref_energy=ref_energy

            eeref = exist(ref_energy) ? ref_energy : 50.  ;The value of the reference electron energy in keV.
            
            If (eebrk gt eehigh) then eebrk = eehigh
            If (eebrk lt eelow) then eebrk = eelow
            
            ;   Normalize all energies to eeref
            een = eel/eeref
            eln = eelow/eeref
            ebn = eebrk/eeref
            ehn = eehigh/eeref
            
            F_E = make_array(size(eel,/dimensions),/double,value=0.d0)
            
            If (Where(eel lt eelow))[0] ne -1 then  $
                    F_E[Where(eel lt eelow)] = ((eln^(1-p) - ebn^(1-p))/(p-1.d0)) + (ebn^(q-p))*(ebn^(1-q) - ehn^(1-q))/(q-1.d0)
            If (Where((eel lt eebrk) and (eel ge eelow)))[0] ne -1 then  $
                    F_E[Where((eel lt eebrk) and (eel ge eelow))] = ((een^(1-p) - ebn^(1-p))/(p-1.d0)) + (ebn^(q-p))*(ebn^(1-q) - ehn^(1-q))/(q-1.d0)
            If (Where((eel le eehigh) and (eel ge eebrk)))[0] ne -1 then  $
                    F_E[Where((eel le eehigh) and (eel ge eebrk))] = (ebn^(q-p))*(een^(1-q) - ehn^(1-q))/(q-1.d0)
                    
            F_E = eeref * F_E

END