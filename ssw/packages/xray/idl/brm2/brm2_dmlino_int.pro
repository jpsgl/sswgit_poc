;+
; PROJECT:
; HESSI
;
; NAME:
; Brm2_DmlinO_int
;
; PURPOSE:
; Performs the integration for the routine brm2_dmlino.  See brm2_dmlin0 header
; for more details.
;
; CATEGORY:
; HESSI, Spectra, Modeling
;
; CALLING SEQUENCE:
; Result = Brm2_DmlinO(maxfcn, rerr, eph, eelow, eebrk, eehigh, p, q, z, a_lg, b_lg, l, intsum, ier, _extra=_extra)
;
; CALLS:
; Brm_GauLeg
; Brm2_Fouter
;
; INPUTS:
;  Note: all of the arguments up to a_lg are the same as for brm2_dmlino, and in the same order
;  
; maxfcn  - Maximum number of points for the Gaussian quadrature
;       integration.
;
; rerr  - Desired relative error for evaluation of the integrals.  For
;       example, rerr = 0.01 indicates that the estimate of the
;       integral is to be correct to one digit, whereas rerr = 0.001
;       calls for two digits to be correct.
;
; eph   - Array of photon energies for which the flux is calculated.
;
;   eelow      -   Low energy cut-off. 
;
;   eebrk      -   Break energy. 
;
;   eehigh      -   High energy cut-off. 
;
;   p    - Power-law index of the electron distribution function below
;          eebrk. This input is not used in Brm2_DmlinO, but is passed to Function Brm2_Fouter.
;
;   q - Power-law index of the electron distribution function above
;          eebrk. This input is not used in Brm2_DmlinO, but is passed to Function Brm2_Fouter.
;
; z   - Mean atomic number of the target plasma.  This input is not
;       used in Brm2_DmlinO, but is passed to Function Brm2_Fouter.
;       
; a_lg - Array containing log of lower limits of integration.
; 
; b_lg - Array containing log of upper limits of integration.
; 
; l    - (that's the letter L) indices to use
; 
; _extra - keywords to pass on to Brm2_Fouter
;
; OPTIONAL INPUTS:
; none
;
; OUTPUTS:
;
; intsum - Array of estimates of the integrals corresponding to the photon
;         energies and limits of integration specified.
;
; ier   - Array of error flags.  The numerical integration converged
;         where ier= 0, and did not converge where ier = 1.
;
;
; OPTIONAL OUTPUTS:
; none
;
; KEYWORDS:
; none
;
; COMMON BLOCKS:
; none
;
; MODIFICATION HISTORY:
;   05-Mar-2013 Kim Tolbert   This code was extracted from brm2_dmlino and made into this
;                             separate subroutine.                           
;                                                                     
;-

pro brm2_dmlino_int, maxfcn, rerr, eph, eelow, eebrk, eehigh, p, q, z, a_lg, b_lg, l, intsum, ier, _extra=_extra

 ;  Maximum possible order of the Gaussian quadrature integration is 2^nlim.
nlim = 12

FOR ires = 2, nlim DO BEGIN
    npoint = 2l^(ires)
    IF (npoint GT maxfcn) THEN GOTO, noconv
    eph1 = eph[l]

  ; Create arrays for abscissas and weights.
    xI = DblArr(N_Elements(l),npoint)
    wI = DblArr(N_Elements(l),npoint)

  ; Use GauLeg to perform the outer integration.
    Brm_GauLeg, a_lg[l], b_lg[l], npoint, xi, wi     ; use lg(aa) and lg(bb) instead of aa and bb

    lastsum = intsum

    ; add 10.^xi*Alog(10.d0) because of the change of logarithm form; use 10.^xi instead of xi for same reason
    intsum[l] =(10.d0^xi*Alog(10.d0)*wi*Brm2_Fouter(10.d0^xi, eph1, eelow, eebrk, eehigh, p, q, z, _extra=_extra)) $
                  #(Make_Array(npoint, Value=1))
          
  ; Use last two calculations to check for convergence.
  l1 = Abs(intsum-lastsum)
  l2 = rerr*Abs(intsum)
  l= Where(l1 GT l2)
  
  IF (l[0] EQ -1) THEN return
ENDFOR
    
; ier= 1 indicates no convergence.
noconv: ier[l] = 1                
      
end              