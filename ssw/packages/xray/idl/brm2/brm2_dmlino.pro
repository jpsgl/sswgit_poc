;+
; PROJECT:
;	HESSI
;
; NAME:
;	Brm2_DmlinO
;
; PURPOSE:
;	To integrate a function via the method of Gaussian quadrature.  Repeatedly
;	doubles the number of points evaluated until convergence, specified by the
;	input rerr, is obtained, or the maximum number of points, specified by the
;	input maxfcn, is reached.  If integral convergence is not achieved, this
;	function returns the error code ier = 1 when either the maximum number of
;	function evaluations is performed or the number of Gaussian points to be
;	evaluated exceeds maxfcn.  Maxfcn should be less than or equal to 2^nlim,
;	or 4096 with nlim = 12.
;
; CATEGORY:
;	HESSI, Spectra, Modeling
;
; CALLING SEQUENCE:
;	Result = Brm2_DmlinO( a, b, maxfcn, rerr, eph, eelow, eebrk, eehigh, p, q, z, ier)
;
; CALLS:
;	Brm_GauLeg
;	Brm2_Fouter
;
; INPUTS:
;
;	a		-	Array containing the lower limits of integration for the
;				corresponding photon energies in the input array eph.
;
;	b		-	Array containing the upper limits of integration for the
;				corresponding photon energies in the input array eph.
;
;	maxfcn	-	Maximum number of points for the Gaussian quadrature
;				integration.
;
;	rerr	-	Desired relative error for evaluation of the integrals.  For
;				example, rerr = 0.01 indicates that the estimate of the
;				integral is to be correct to one digit, whereas rerr = 0.001
;				calls for two digits to be correct.
;
;	eph		-	Array of photon energies for which the flux is calculated.
;
;   eelow      -   Low energy cut-off. 
;
;   eebrk      -   Break energy. 
;
;   eehigh      -   High energy cut-off. 
;
;   p    - Power-law index of the electron distribution function below
;          eebrk. This input is not used in Brm2_DmlinO, but is passed to Function Brm2_Fouter.
;
;   q - Power-law index of the electron distribution function above
;          eebrk. This input is not used in Brm2_DmlinO, but is passed to Function Brm2_Fouter.
;
;	z		-	Mean atomic number of the target plasma.  This input is not
;				used in Brm2_DmlinO, but is passed to Function Brm2_Fouter.
;
; OPTIONAL INPUTS:
;	none
;
; OUTPUTS:
;
;	DmlinO	-	Array of estimates of the integrals corresponding to the photon
;					energies and limits of integration specified.
;
;	ier		-	Array of error flags.  The numerical integration converged
;					where ier= 0, and did not converge where ier = 1.
;
;
; OPTIONAL OUTPUTS:
;	none
;
; KEYWORDS:
;	none
;
; COMMON BLOCKS:
;	none
;
; SIDE EFFECTS:
;
;
; RESTRICTIONS:
;
;
; PROCEDURE:
;	This function is based on the routine xgauleg found in "Numerical Recipes
;	Example Book (FORTRAN)," Second Edition, Cambridge University Press, p. 48.
;
; MODIFICATION HISTORY:
;   Version 1, Gordon.D.Holman@nasa.gov, 23 July 2001
;   IDL version:  Sally House, summer intern
;   Documentation is provided in the file $SSW/packages/xray/doc/brm_thick_doc.pdf
;   (Adobe Acrobat Reader required).
;
;   Version 2:     This is used in the new, faster version the thick target model functions.It is roughly parallel to
;                  the old brm_dmlino.pro
;   12/02/2009 Yang Su       Updated the Version 1 code for the purpose of reversing the order of integration
;   04/14/2009  Yang Su      Change the integral into it's logarithmic form to 
;                                           make it converge much faster. 
;   04/17/2009  Yang Su      Split the integral into 3 parts: below the eelow, 
;                                           between eelow and eebrk, and above eebrk.  
;                                           change starting npoint from 2 to 4.
;   05/08/2009  Gordon Holman  Revised error messages to be more specific.  Removed EFD.
;   05/26/2011  Kim Tolbert   Added independent_norm keyword, and pass to Brm2_Fouter (for thick2_vnorm)
;   08-Feb-2013 Kim Tolbert   Use _extra instead of spelling out keywords that are passed through (currently 
;                             independent_norm and now return_current) to Brm2_Fouter
;   05-Mar-2013 Kim Tolbert   Extracted the integration code (which was the same for the three parts)
;                             into a separate subroutine called brm2_dmlino_int.                               
;                                                                     
;-

Function Brm2_DmlinO, a, b, maxfcn, rerr, eph, eelow, eebrk, eehigh, p, q, z, ier, _extra=_extra

    mc2 = 510.98d+00
    clight = 2.9979d+10

        ;----------------------Part 1, below eelow-------------------------
            ;	Create arrays for integral sum and error flags.
            intsum1 = DblArr(N_Elements(a))
            ier1=DblArr(N_Elements(a))

            P1=where(a lt eelow)
 
            If P1[0] ne -1 then begin

                a_lg=alog10(a[p1])
                b_lg=alog10( make_array(n_elements(a_lg),/double,value=eelow) )    
                
                l=P1
                
                brm2_dmlino_int, maxfcn, rerr, eph, eelow, eebrk, eehigh, p, q, z, a_lg, b_lg, l, intsum1, ier1, _extra=_extra
                                
                ;	ier= 1 indicates no convergence.
                if total(ier1) gt 0 then MESSAGE,'!!! ERROR --- brm2_DmlinO: Part 1 integral did not converge for some photon energies.', /continue
                     
            Endif
                
                
        ;----------------------------------Part 2, between eelow and eebrk---------------------------------
   
            intsum2 = DblArr(N_Elements(a))
            ier2=DblArr(N_Elements(a))
            aa=a
            P2=where(a lt eebrk)
            
            If (P2[0] ne -1) and (eebrk ne eelow) then begin
                if P1[0] ne -1 then aa[p1]=eelow
                a_lg=alog10(aa[P2])
                b_lg=alog10( make_array(n_elements(a_lg),/double,value=eebrk) )
       
                l=p2
                
                brm2_dmlino_int, maxfcn, rerr, eph, eelow, eebrk, eehigh, p, q, z, a_lg, b_lg, l, intsum2, ier2, _extra=_extra
 
                if total(ier1) gt 0 then MESSAGE,'!!! ERROR --- brm2_DmlinO: Part 2 integral did not converge for some photon energies.',/continue
                
            Endif
    ;----------------------------Part 3: between eebrk and eehigh-----------------------

            intsum3 = DblArr(N_Elements(a))
            ier3=DblArr(N_Elements(a))
            aa=a
            P3=where(a le eehigh)

            If (P3[0] ne -1) and (eehigh ne eebrk) then begin
                
                If P2[0] ne -1 then aa[p2]=eebrk
                a_lg=alog10(aa[P3])
                b_lg=alog10( make_array(n_elements(a_lg),/double,value=eehigh) )
   
                l=p3
                
                brm2_dmlino_int, maxfcn, rerr, eph, eelow, eebrk, eehigh, p, q, z, a_lg, b_lg, l, intsum3, ier3, _extra=_extra
 
                if total(ier3) gt 0 then MESSAGE,'!!! ERROR --- brm2_DmlinO: Part 3 integral did not converge for some photon energies.',/continue
                
            Endif

        ;----------------------------------------------------------------------------

        DmlinO  = (intsum1+intsum2+intsum3)*(mc2/clight)   
        ier=ier1+ier2+ier3
    Return, DmlinO

END