;+
; PROJECT:
;   HESSI
;
; NAME:
;   Brm2_ThickTarget
;
; PURPOSE:
;   This Function computes the thick-target bremsstrahlung x-ray/gamma-ray
;   spectrum from an isotropic electron distribution function provided in   
;   Pro Brm2_F_distrn.  The units of the computed flux is photons per second per
;   keV per square centimeter.
;   
;   The electron flux distribution function is a double power law in electron energy
;   with a low-energy cutoff and a high-energy cutoff.  
;
;   Note: If you want to plot the derivative of the flux, or the spectral index of the photon 
;               spectrum as a function of photon energy, you should set RERR to 1.d-6, 
;               because it is more sensitive to RERR than the flux. 
;               Then: 
;       Flux = Brm2_ThickTarget(energy, A) or Flux=F_Thick2(energy, A)
;       index = -Deriv(Alog10(energy),Alog10(Flux))
;
; CATEGORY:
;   HESSI, Spectra, Modeling
;
; CALLING SEQUENCE:
;   Flux = Brm2_ThickTarget(eph, a)
;
; CALLS:
;   Brm2_DmlinO
;
; INPUTS:
;   eph - Array of photon energies (in keV) at which to compute the
;         photon flux.
;   a - parameters describing the nonthermal electron broken power-law, where:
;   a(0) - Total integrated electron flux, in units of 10^35 electrons sec^-1.
;          This normalization parameter is not used in this routine.
;   a(1) - Power-law index of the electron distribution function below
;          eebrk.
;   a(2) - Break energy in the electron distribution function (in keV)
;   a(3) - Power-law index of the electron distribution function above
;          eebrk.
;   a(4) - Low energy cutoff in the electron distribution function
;          (in keV).
;   a(5) - High energy cutoff in the electron distribution function (in keV).

;
; OPTIONAL INPUTS:
;
; OUTPUTS:
;
;   Array of photon fluxes (in photons s^-1 keV^-1 cm^-2),
;   when multiplied by a[0] * 1.0d+35,
;   corresponding to the photon energies in the input array eph.
;   The detector is assumed to be 1 AU from the source.
;
; OPTIONAL OUTPUTS:
;   none
;
; KEYWORDS:
;
;
; COMMON BLOCKS:
;   none
;
; SIDE EFFECTS:
;
;
; RESTRICTIONS:
;
;
; PROCEDURE:
;
;
; MODIFICATION HISTORY:
;   Version 1, Gordon.D.Holman@nasa.gov, 23 July 2001
;   IDL version:  Sally House, summer intern
;   Documentation is provided in the file $SSW/packages/xray/doc/brm_thick_doc.pdf
;   (Adobe Acrobat Reader required).
;   03/19/2002 Linhui sui    set EFD = 1 to get electron flux distribution
;   12/02/2004 Linhui Sui    update the documentation
;   
;   Version 2:    Gordon.D.Holman@nasa.gov 05/19/2009, This is used in the new, faster version the 
;                 thick target model functions.  It is roughly parallel to the old brm_bremthick.
;   12/02/2009 Yang Su       Following Gordon Holman's suggestion, 
;                                           reverse the order of integral for faster speed.  
;   04/14/2009 Yang Su      Change the integral into it's logarithm form to 
;                                           make it converge much faster. The only changes are in Brm2_DmlinO.pro.                                 
;   04/17/2009  Yang Su      Split the integral into 3 parts: below the eelow, 
;                                               between eelow and eebrk, above eebrk.  
;                                           The relative error is set to 1.d-4 (was 1.d-3).
;                                           Version 2 is at least 10-100 times faster than Version 1 with RERR = 1.d-3.
;  05/08/2009  Gordon Holman    Documentation and error message modified. Removed reference to EFD.
;  05/26/2011  Kim Tolbert      Added independent_norm keyword (for thick2_vnorm fcn), and pass to Brm2_dmlino 
;  08-Feb-2013 Kim Tolbert      Use _extra instead of spelling out keywords that are passed through (currently 
;                               independent_norm and now return_current) to Brm2_DmlinO                                    
;                                           
;-

Function Brm2_ThickTarget, eph, a, _extra=_extra

    ;set parameters
    p = double(a[1])
    eebrk = double(a[2])
    q = double(a[3])
    eelow = double(a[4])
    eehigh = double(a[5])
    
    IF eebrk lt eelow then eebrk=eelow
    IF eebrk gt eehigh then eebrk=eehigh

    mc2 = 510.98d+00
    clight = 2.9979d+10
    au = 1.496d+13
    r0 = 2.8179d-13

    ; Maximum number of points for the Gaussian quadrature integration, maxfcn.
    
    maxfcn = 2048

    ; Average atomic number (z).
    
    z = 1.2d0
    
    ; Specify the relative error.
    
    rerr = 1.d-4

    ; Compute the numerical coefficient for the photon flux.
    
    fcoeff = (1.d0/(4.d0*!pi*au^2))*(clight^2/mc2^4)
    
    ; Compute the numerical coefficient for the energy loss rate in keV/sec.
    ; This should be multiplied by the target plasma number density, but the
    ; number density has been left out because it cancels out of the
    ; thick-target calculation.
    
    decoeff = 4.d0*!pi*r0^2*clight   ; remove *mc2 because now the F_E is calculated over E in keV not over gamma
    
    ; Create arrays for the photon flux and error flags.
    
    flux = DblArr(N_Elements(eph))
    iergq = IntArr(N_Elements(eph))
    
    
    l = Where( (eph LT eehigh) and (eph gt 0) )
    
    If l[0] ne -1 then begin
        flux[l] = Brm2_DmlinO( $
                       double(eph[l]), $
                       make_array(n_elements(l), /double,value=eehigh), $
                       maxfcn, $
                       rerr, $
                       eph[l],$
                       eelow, $
                       eebrk, $
                       eehigh, $
                       p, $
                       q, $
                       z, $
                       ier, $
                       _extra=_extra)

        iergq[l] = ier

        flux = (fcoeff/decoeff)*flux

    endif else begin
        Message,'!!! ERROR --- The photon energies are higher than the highest electron energy or not greater than zero.'
    endelse

    RETURN, flux

END
