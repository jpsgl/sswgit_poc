;+
;Name: Setup_Chianti_Cont_Hessi
;
;Purpose:
;	This main program is the setup file for the chianti continuum database file
;
;History:
;	2000, richard.schwartz@nasa.gov
;	30-aug-2012, automated using the chianti version in the filename and documented
;	17-mar-2015, richard.schwartz@nasa.gov - raise temp to 10^9 Kelvin
;		 ioneq set to 'CHIANTI_V7', ntemp changed to 750;-
kmin=1. ;min energy keV
kmax=250.0 ;max energy
temp_range = [1.0, 1000.0]*1e6 ;[1., 100.]*1e6 ; from 1-1000 MegaKelvin
ntemp = 300 ;200
nedge = 500
nelem = 15 ;top 15 abundances
ioneq= 'CHIANTI' ;'MAZZOTTA'
abund= 'sun_coronal_1992_feldman_ext.abund'
chianti_dbase= concat_dir('SSW_CHIANTI','dbase')
ioneq_file = loc_file(path=concat_dir(chianti_dbase,'ioneq'),'*.ioneq')
select = where( strpos(STRLOWCASE(ioneq_file),STRLOWCASE(ioneq)) ne -1)
ioneq_name = ioneq_file[ select[0] ]
abund_file = loc_file(path=concat_dir(chianti_dbase,'abundance'),'*.abund')
select = where( strpos(STRLOWCASE(abund_file),STRLOWCASE(abund)) ne -1)
abund_file = abund_file[ select[0] ]

    ;---> Call: PRO isothermal, wmin,wmax,wavestep,temp,lambda,spectrum,list_wvl,list_ident,$
    ;pressure=pressure,edensity=edensity,photons=photons, ergs=ergs, $
    ;sngl_ion=sngl_ion, abund_name= abund_name , ioneq_name=ioneq_name, $
    ;noverbose=noverbose, min_abund=min_abund, cont=cont, $
    ;masterlist=masterlist, noprot=noprot, radtemp=radtemp, $
    ;rphot=rphot, em=em

isothermal, 3.,4., .01, 2e7, lambda, spectrum, edens=1e11,$
    ioneq_name=ioneq_name,abund_name=abund_file
;automated using the chianti version in the filename, ras, 30-aug-2012
fileout = setup_chianti_cont(ioneq, kmin,kmax,$
    temp_range, ntemp=ntemp, nedge=nedge, nelem=nelem, $
    genxfile='chianti_cont_1_250_v'+strtrim(fix(float(chianti_version())*10),2)+'.geny', /overwrite)
;restgenx, zindex,  gtotcont, gtotcont_lo, edge_str, ctemp, chianti_doc, file=fileout
end

