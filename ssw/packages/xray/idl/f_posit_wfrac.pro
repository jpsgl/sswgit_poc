;+
; NAME:
;   F_POSIT_WFRAC
;
; PURPOSE:
;   Function wrapper for posit_wfrac procedure.  Calculates 511 keV
;   line with positronium continuum.
;
; CALLING SEQUENCE:
;   result = f_posit_wfrac( energy_bins, parameters )
;
; INPUTS:
;   e - energy bins on which the function is evaluated.  If a vector,
;       these are interpreted as the mean values of the bins and the
;       bins are calculated to be the mean values plus or minus the
;       widths of the bins.  If a 2 x N array, the e[0,*] are the
;       lower bounds and the e[1,*] are the upper bounds.
;   a - vector of parameters:
;     a[0] - 511 keV line flux
;     a[2] - Positronium continuum fraction
;     a[3] - 511 keV line FWHM
;     a[4] - 511 keV centroid
; OUTPUTS:
;   result - 511 line with positronium continuum.
;
; RESTRICTIONS:
;   If e is a vector input, the minimum bin boundary value will be at
;   least 1e-20.  Negative or zero energy values are not allowed in
;   this case.  If e is a 2 x N array, no such check is performed.
;
; CALLS:
;   posit_wfrac
;
; WRITTEN:
;   Paul Bilodeau, 21-aug-2002, paul.bilodeau@gsfc.nasa.gov
;
;-
;
FUNCTION f_posit_wfrac, e, a

IF (size(e))(0) EQ 1 THEN BEGIN
    ;; assume these are the mean values - need the bin boundaries
    e2 = Fltarr(2) # e
    delta = e - shift(e,1)
    ;; First delta is negative - correct this by duplicating the
    ;; second delta value.
    delta[0] = delta[1]
    ;; The minimum energy bin boundary value is 1e-20 - 0 or negative
    ;; are not allowed.
    e2[0,*] = ( e - delta/2. ) > 1e-20
    e2[1,*] = e + delta/2.
ENDIF ELSE e2 = e

posit_wfrac, e2, 0, f, 0, a, [0,0], 0, 0

RETURN, f

END
