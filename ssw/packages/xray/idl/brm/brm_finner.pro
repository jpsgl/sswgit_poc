;+
; PROJECT:
;	HESSI
;
; NAME:
;	Brm_FInner
;
; PURPOSE:
;	Returns the integrand for the (inner) bremsstrahlung integration.
;
; CATEGORY:
;	HESSI, Spectra, Modeling
;
; CALLING SEQUENCE:
;	Result = Brm_FInner(eel, eph, z)
;
; CALLS:
;	Brm_BremCross
;	Brm_ELoss
;
; INPUTS:
;
;	eel		-	Two-dimensional array of abscissas calculated in
;				Pro Brm_GauLeg[53 or 54].
;
;	eph		-	Array of photon energies.  Each element corresponds to a column
;				of the two-dimensional array eel.
;
;	z		-	Mean atomic number of the target plasma.  This input is not
;				used in Brm_FInner, but is passed to Pro Brm_BremCross.
;
;
; OPTIONAL INPUTS:
;	none
;
; OUTPUTS:
;   Brm_FInner	-	Array of integrands (for inner numerical integration)
;					corresponding to input array eel.
;
;
; OPTIONAL OUTPUTS:
;	none
;
; KEYWORDS:
;	none
;
; COMMON BLOCKS:
;	none
;
; SIDE EFFECTS:
;
;
; RESTRICTIONS:
;
;
; PROCEDURE:
;
;
; MODIFICATION HISTORY:
;   Version 1, holman@stars.gsfc.nasa.gov, 23 July 2001
;   IDL version:  Sally House, summer intern
;	Documentation for the Fortran version of this code can be found at
;   http://hesperia.gsfc.nasa.gov/hessi/flarecode/bremthickdoc.pdf
;
;-

Function Brm_FInner, eel, eph, z

l = Where(eel)

mc2 = 510.98d+00

Brm_BremCross, eel, eph(l MOD N_Elements(eph)), z, cross

Brm_ELoss, eel, dedt

pc = SqRt(eel*(eel+2.d0*mc2))

Brm_FInner = cross*pc/dedt

Return, Brm_FInner

END