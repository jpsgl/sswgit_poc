;+
; PROJECT:
;	HESSI
;
; NAME:
;	Brm_BremThick
;
; PURPOSE:
;	This Function computes the thick-target bremsstrahlung x-ray/gamma-ray
;	spectrum from an isotropic electron distribution function provided in
;	Pro Distrn.  The units of the computed flux is photons per second per
;	keV per square centimeter.
;
; CATEGORY:
;	HESSI, Spectra, Modeling
;
; CALLING SEQUENCE:
;	Brm_BremThick (eph, a)
;
; CALLS:
;	Brm_DmlinO
;
; INPUTS:
;   eph - Array of photon energies (in keV) at which to compute the
;         photon flux.
;   a - parameters describing the nonthermal electron broken power-law, where:
;   a(0) - normalization factor, i.e. nonthermal electron density * area
;          densty = number density of nonthermal electrons (cm^-3);
;          area = area of the radiating source region (cm^2).;
;   a(1) - Power-law index of the electron distribution function below
;          eebrk.
;   a(2) - Break energy in the electron distribution function (in keV)
;   a(3) - Power-law index of the electron distribution function above
;          eebrk.
;   a(4) - Low energy cutoff in the electron distribution function
;          (in keV).
;   a(5) - High energy cutoff in the electron distribution function (in keV).
;
;
; OPTIONAL INPUTS:
;   none
;
; OUTPUTS:
;
;	Array of photon fluxes (in photons s^-1 keV^-1 cm^-2)
;	corresponding to the photon energies in the input array eph.
;	The detector is assumed to be 1 AU from the source.
;
; OPTIONAL OUTPUTS:
;	none
;
; KEYWORDS:
;   none
;
; COMMON BLOCKS:
;	none
;
; SIDE EFFECTS:
;
;
; RESTRICTIONS:
;
;
; PROCEDURE:
;
;
; MODIFICATION HISTORY:
;   Version 1, holman@stars.gsfc.nasa.gov, 23 July 2001
;   IDL version:  Sally House, summer intern
;   Documentation is provided in the file brm_thick_doc.pdf
;   (Adobe Acrobat Reader required).
;	Documentation for the Fortran Version of this code
;   http://hesperia.gsfc.nasa.gov/hessi/flarecode/bremthickdoc.pdf
;
;-

Function Brm_BremThick, eph, a

;set parameters
p = a[1]
eebrk = a[2]
q = a[3]
eelow = a[4]
eehigh = a[5]

;use electron density distribution
EFD = 0

mc2	= 510.98d+00
clight = 2.9979d+10
au = 1.496d+13
r0 = 2.8179d-13

; Maximum number of points for the Gaussian quadrature integration, maxfcn.

maxfcn = 2048

; Average atomic number (z).

z = 1.2d0

; Specify the relative error.

rerr = 0.001

; Compute electron gammas corresponding to eelow, eebrk, and eehigh.

gamlow = (eelow/mc2)+1.d0
gambrk = (eebrk/mc2)+1.d0
gamhigh = (eehigh/mc2)+1.d0

; Compute the numerical coefficient for the photon flux.

fcoeff = (1.0/(4.d0*!pi*au^2))*(clight^2/mc2^4)

; Compute the numerical coefficient for the energy loss rate in keV/sec.
; This should be multiplied by the target plasma number density, but the
; number density has been left out because it cancels out of the
; thick-target calculation.

decoeff = 4.d0*!pi*r0^2*clight*mc2

; Create arrays for the photon flux and error flags.

flux = DblArr(N_Elements(eph))
iergql = IntArr(N_Elements(eph))
iergqh = IntArr(N_Elements(eph))
iergqi = IntArr(N_elements(eph))

; The integration is performed in two steps because of the discontinuity
; at gambrk.

l = Where(eph LT eebrk)
h = Where(eph GE eebrk)

IF (l(0) NE -1) THEN BEGIN

    emin = eph
    IF((Where(eph LT eelow))(0) NE -1) THEN $
      emin(Where(eph LT eelow)) = eelow
    
    fluxl = Brm_DmlinO( $
                       emin(l), $
                       eebrk, $
                       maxfcn, $
                       rerr, $
                       eph(l), $
                       eebrk, $
                       gamlow, $
                       gambrk, $
                       gamhigh, $
                       p, $
                       q, $
                       z, $
                       EFD, $
                       ier1, $
                       ier2)
    
    iergql(l) = ier1
    iergqi(l) = ier2
    
    fluxh = Brm_DmlinO( $
                       DblArr(N_Elements(l))+eebrk, $
                       eehigh, $
                       maxfcn, $
                       rerr, $
                       eph(l), $
                       eebrk, $
                       gamlow, $
                       gambrk, $
                       gamhigh, $
                       p, $
                       q, $
                       z, $
                       EFD, $
                       ier1, $
                       ier2)
    
    iergqh(l) = ier1
    iergqi(l) = iergqi(l)+ier2
    
    flux(l) = fluxl+fluxh
    
ENDIF

IF (h(0) NE -1) THEN BEGIN
    
    flux(h) = Brm_DmlinO( $
                         eph(h), $
                         eehigh, $
                         maxfcn, $
                         rerr, $
                         eph(h), $
                         eebrk, $
                         gamlow, $
                         gambrk, $
                         gamhigh, $
                         p, $
                         q, $
                         z, $
                         EFD, $
                         ier1, $
                         ier2)
    
    iergqh(h) = ier1
    iergqi(h) = ier2
    
ENDIF

flux = (fcoeff/decoeff)*flux

RETURN, flux

END
