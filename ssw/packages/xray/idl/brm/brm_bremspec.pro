;+
; PROJECT:
;   HESSI
;
; NAME:
;   Brm_BremSpec
;
; PURPOSE:
;   This Function computes the thin-target bremsstrahlung x-ray/gamma-ray
;   spectrum from an isotropic electron distribution function provided in
;   Pro Brm_Distrn.  The units of the computed flux is photons per second per
;   keV per square centimeter.
;
; CATEGORY:
;   HESSI, Spectra, Modeling
;
; CALLING SEQUENCE:
;   Brm_BremSpec, eph, a
;
; CALLS:
;   Brm_Dmlin
;
; INPUTS:
;
;   eph - Array of photon energies (in keV) at which to compute the
;         photon flux.
;   a - parameters describing the nonthermal electron broken power-law, where:
;   a(0) - normalization factor in units of 1.0d55 cm-2 sec-1,
;             i.e. plasma density * volume of source * integrated nonthermal electron flux density
;   a(1) - Power-law index of the electron distribution function below eebrk.
;   a(2) - Break energy in the electron distribution function (in keV)
;   a(3) - Power-law index of the electron distribution function above eebrk.
;   a(4) - Low energy cutoff in the electron distribution function (in keV).
;   a(5) - High energy cutoff in the electron distribution function (in keV).
;
;
; OPTIONAL INPUTS:
;   none
;
;
; OUTPUTS:
;
;   Array of photon fluxes (in photons s^-1 keV^-1 cm^-2)
;   corresponding to the photon energies in the input array eph.
;   The detector is assumed to be 1 AU from the source.
;
; OPTIONAL OUTPUTS:
;   none
;
; KEYWORDS:
;   none
;
; COMMON BLOCKS:
;   none
;
; SIDE EFFECTS:
;
;
; RESTRICTIONS:
;
;
; PROCEDURE:
;
;
; MODIFICATION HISTORY:
;   Version 1, holman@stars.gsfc.nasa.gov, 23 July 2001
;   IDL version:  Sally House, summer intern
;   Documentation is provided in the file brm_doc.pdf
;   (Adobe Acrobat Reader required).
;   Documentation for the Fortran Version of this code
;   can be found at http://hesperia.gsfc.nasa.gov/hessi/flarecode/bremdoc.pdf
;   Version 2, 11/20/2002, Changed from procedure to function
;                          Set EFD = 1, use electron flux distribution
;                         Normalization factor is mutiplied at f_vth_thin.pro
;                  instead of within the script
; 10-May-2005 Linhui Sui, correct description of a(0)
; 24-Nov-2009, Kim Tolbert. Decrease relative error from .001 to .0001. Should help with the problem
;   that some sets of parameters produce a bump in the spectrum.  Will be slower, but can use thin2.
;-

Function Brm_BremSpec, eph, a

;   set parameters
p = a[1]
eebrk = a[2]
q = a[3]
eelow = a[4]
eehigh = a[5]

;   use electron flux distribution
EFD = 1

mc2 = 510.98d+00
clight = 2.9979d+10
au = 1.496d+13

;   Maximum number of points for the Gaussian quadrature integration, maxfcn.

maxfcn = 2048

;   Average atomic number (z).

z = 1.2d0

;   Specify relative error.

rerr = 0.0001

;   Compute electron gammas corresponding to eelow, eebrk, and eehigh.

gamlow = (eelow/mc2)+1.d0
gambrk = (eebrk/mc2)+1.d0
gamhigh = (eehigh/mc2)+1.d0

;   Compute the numerical coefficient for the photon flux.

fcoeff = (clight/(4.d0*!dpi*(au^2)))/mc2^3

;   Create arrays for the photon flux and error flags.

flux = DblArr(N_Elements(eph))
iergql = IntArr(N_Elements(eph))
iergqh = IntArr(N_Elements(eph))

;   The integration is performed in two steps because of the discontinuity
;   at gambrk.

l = Where(eph LT eebrk)
h = Where(eph GE eebrk)

IF (l(0) NE -1) THEN BEGIN

    emin = eph
    IF((Where(eph LT eelow))(0) NE -1) THEN emin(Where(eph LT eelow)) = eelow

    fluxl = Brm_Dmlin(emin(l), eebrk, maxfcn, rerr, $
       eph(l), gamlow, gambrk, gamhigh, p, q, z, EFD, ier)
    iergql(l) = ier

    fluxh = Brm_Dmlin(DblArr(N_Elements(l))+eebrk, eehigh, maxfcn, rerr, $
       eph(l), gamlow, gambrk, gamhigh, p, q, z, EFD, ier)
    iergqh(l) = ier

    flux(l) = fluxl+fluxh

ENDIF

IF (h(0) NE -1) THEN BEGIN

    flux(h) = Brm_Dmlin(eph(h), eehigh, maxfcn, rerr, $
       eph(h), gamlow, gambrk, gamhigh, p, q, z, EFD, ier)
    iergqh(h) = ier

ENDIF

flux = flux*fcoeff

return, flux

END