;+
; PROJECT:
;	HESSI
;
; NAME:
;	Brm_FThin
;
; PURPOSE:
;	Returns the integrand for the bremsstrahlung integration.
;
; CATEGORY:
;	HESSI, Spectra, Modeling
;
; CALLING SEQUENCE:
;	Result = Brm_FThin(eel, eph, gamlow, gambrk, gamhigh, p, q, z, EFD)
;
; CALLS:
;	Brm_BremCross
;	Brm_Distrn
;
; INPUTS:
;
;	eel		-	Two-dimensional array of abscissas calculated in
;				Pro Brm_GauLeg[53 or 54].
;
;	eph		-	Array of photon energies.  Each element corresponds to a column
;				of the two-dimensional array eel.
;
;	gamlow	-	Electron gamma corresponding to the low energy cutoff.  This
;				input is not used in Brm_FThin, but is passed to
;				Pro Brm_Distrn.
;
;	gambrk	-	Electron gamma corresponding to the break energy.  This input
;				is not used in Brm_FThin, but is passed to Pro Brm_Distrn.
;
;	gamhigh	-	Electron gamma corresponding to the high energy cutoff.  This
;				input is not used in Brm_FThin, but is passed to
;				Pro Brm_Distrn.
;
;	p		-	Power-law index below the break energy.  This input is not used
;				in Brm_FThin, but is passed to Pro Brm_Distrn.
;
;	q		-	Power-law index above the break energy.  This input is not used
;				in Brm_FThin, but is passed to Pro Brm_Distrn.
;
;	z		-	Mean atomic number of the target plasma.  This input is not
;				used in Brm_FThin, but is passed to Pro Brm_BremCross.
;
;	EFD		-	Keyword.  If set, the electron flux distribution is calculated.
;				Otherwise, the electron density distribution is calculated.
;
;
; OPTIONAL INPUTS:
;	none
;
; OUTPUTS:
;   Brm_FThin	-	Array of integrands corresponding to input array eel.
;
; OPTIONAL OUTPUTS:
;	none
;
; KEYWORDS:
;
;
; COMMON BLOCKS:
;	none
;
; SIDE EFFECTS:
;
;
; RESTRICTIONS:
;
;
; PROCEDURE:
;
;
; MODIFICATION HISTORY:
;   Version 1, holman@stars.gsfc.nasa.gov, 23 July 2001
;   IDL version:  Sally House, summer intern
;	Documentation for the Fortran Version of this code
;   can be found at http://hesperia.gsfc.nasa.gov/hessi/flarecode/bremdoc.pdf
;
;-

Function Brm_FThin, eel, eph, gamlow, gambrk, gamhigh, p, q, z, EFD

l = Where(eel)

mc2 = 510.98d+00
clight = 2.9979d+10

gamma = (eel/mc2)+1.d0

Brm_BremCross, eel, eph(l MOD N_Elements(eph)), z, cross

Brm_Distrn, gamma, gamlow, gambrk, gamhigh, p, q, fcn

IF (EFD) THEN Brm_FThin = fcn*cross*(mc2/clight) $
	ELSE Brm_FThin = fcn*cross*SqRt(eel*(eel+2.d0*mc2))

Return, Brm_FThin

END