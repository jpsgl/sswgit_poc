
pro chianti_kev_line_common_load, file_in, reload=reload,_extra=_extra


common chianti_kev_lines, zindex, out, ion_info, file

if exist(file_in) or keyword_set(reload) or not keyword_set(file) then begin

	default, file, ''


	default, file_in, getenv('CHIANTI_LINES_FILE')
	sswdb_xray_chianti = concat_dir('$SSWDB_XRAY','chianti')
	file_in = loc_file( path=sswdb_xray_chianti, file_in)
	;31-AUG-2005, using .sav file with lines dbase, specified directly
	;by $CHIANTI_LINES_FILE
	if not file_exist( file_in) and not exist(file)  then $
	    file_in = loc_file(path=sswdb_xray_chianti,'chianti_lines.geny')
	default, reload, 0

	If not (file[0] eq file_in[0]) or not exist(zindex) or reload then begin
	    ;check ending
	    break_file, file_in, disk,dir, fname, ext
	    case 1 of
	        strlowcase(ext) eq '.geny':restgenx, zindex,  out, ion_info, file=file_in
	       else: restore, file_in
	       endcase
	       endif

	default, esort, 0
	;sort the out.lines array by energy in keV
	;ras, 24-mar-2006
		ord = sort( 1./ out.lines.wvl)
		out.lines = out.lines[ord]


	file = file_in


endif
end

pro chianti_kev_cont_common_load, file_in, reload=reload,_extra=_extra


common chianti_kev_cont, zindex, totcont, totcont_lo, edge_str, ctemp, chianti_doc, file
if exist(file_in) or keyword_set(reload) or not keyword_set(file) then begin
	default, file, ''

	default, file_in, getenv('CHIANTI_CONT_FILE')
	sswdb_xray_chianti = concat_dir('$SSWDB_XRAY','chianti')
	file_in = loc_file( path=sswdb_xray_chianti, file_in)
	if not file_exist( file_in) and not exist(file)  then $
	    file_in = loc_file(path=sswdb_xray_chianti,'chianti_cont.geny')

	default, reload, 0
	If not (file[0] eq file_in[0]) or not exist(zindex) or reload then begin
	    ;check ending
	    break_file, file_in, disk,dir, fname, ext
	    case 1 of
	        strlowcase(ext) eq '.geny':restgenx, zindex, totcont, totcont_lo, $
	        edge_str, ctemp, chianti_doc, file=file_in
	       else: restore, file_in
	       endcase
	    endif
	file = file_in
endif
end

;+
 ;  PROJECT:
 ;    SSW/XRAY
 ;  NAME:
 ;    CHIANTI_KEV_COMMON_LOAD
 ;  PURPOSE:
 ;    This procedure is called to load the common blocks that support the chianti_kev... functions.
 ;
 ;  CALLING SEQUENCE
 ;    chianti_kev_common_load [, linefile=linefile_in,	contfile=contfile_in]
 ;


 ;
 ;  Optionl Keyword INPUTS:
 ;    LINEFILE - full descriptor of line database file, see chianti_kev_line_common_load
 ;    CONTFILE - full descriptor of continuum database file, see chianti_cont_line_common_load

 ;	  RELOAD - force reloading of all database files
 ;	  NO_ABUND - don't call xr_rd_abundance()
 ;
 ;
 ;  OPTIONAL INPUT KEYWORDS:
 ;
 ;  RESTRICTIONS:
 ;    N.B. If both edges aren't specified in WAVE, then the bins of WAVE must
 ;   be equally spaced.
 ;  METHOD:
 ;    Reads in database files and store the results in common
 ;	  Files are only read if /RELOAD is called or the
 ;	   filenames are different from the saved filenames
 ;  COMMON BLOCKS:
 ;	 These common blocks are in the helper routines called by this procedurel
 ;
 ;   CHIANTI_KEV_LINES holds the line database obtained from CHIANTI.
 ;   chianti_kev_cont  holds the continuum database from CHIANTI
 ;  MODIFICATION HISTORY:
 ;  25-apr-2006, richard.schwartz - abundance doesn't take input except for RELOAD
 ;		File and choice of dist type from two environ var., XR_AB_FILE, XR_AB_TYPE
 ;			see xr_rd_abundace, added NO_ABUND keyword
 ;	24-mar-2006, richard.schwartz mod to save line info in ascending energy order
 ;	21-mar-2006, richard.schwartz@gsfc.nasa.gov, uses chianti_kev_load_common to
 ;		manage database files. Default database files updated to work down to 1 keV
 ;  richard.schwartz@gsfc.nasa.gov, 8-feb-2005
 ;      29-aug-2005 to used compressed .sav file
  ;-


pro chianti_kev_common_load, $
	linefile=linefile_in, $
	contfile=contfile_in, $
	no_abund=no_abund, $
	reload=reload, $
	_extra=_extra



chianti_kev_line_common_load, linefile_in, reload=reload, _extra=_extra
chianti_kev_cont_common_load, contfile_in, reload=reload,_extra=_extra
abundance=xr_rd_abundance(reload=reload) ;uses environment variables only, XR_AB_FILE & XR_AB_TYPE



end













