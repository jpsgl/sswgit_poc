
;+
; NAME:
;   F_BPOW_NLINE
;
; PURPOSE: broken power-law function with/without discontinuities in the
;   derivatives + gaussian line function
;
; CALLING SEQUENCE: result = f_bpow_nline(x, a)
;
; INPUTS:
;   x - independent variable, nominally energy in keV under SPEX
;   a - parameters describing the broken power-law and gaussian
;     a(4+3n) - integrated line intensity of line n(<3)
;     a(5+3n) - line centroid
;     a(6+3n) - line sigma
;
;     a(0) - normalization at epivot
;     a(1) - negative power law index below break
;     a(2) - break energy
;     a(3) - negative power law index above break
;
; OUTPUTS:
;   result of function, a broken power law + gaussian line
; OPTIONAL OUTPUTS:
;
; KEYWORDS:
;   NLINE - number of gaussian lines in model, defaults to 5
;
; PROCEDURE:
;
; CALLS:   F_bpow and f_gauss_intg or gaussian
;
; COMMON BLOCKS:
;   f_bpow_nline
;
; RESTRICTIONS:
;   when used in SPEX, limited to 8 lines on 21-jan-1996
;
; MODIFICATION HISTORY:
;   ras,  21-jan-1996
;   richard.schwartz@gsfc.nasa.gov, 11-jan-2001, support arbitrary number of lines.
;   richard.schwartz@gsfc.nasa.gov, 30-jun-2003, rewritten to use f_vthc_bpow_nline
;   richard.schwartz@gsfc.nasa.gov, 5-aug-2003, fixed bug from 30-jun-2003 in call
;     to f_vthc_bpow_nline
;
;-
;

function f_bpow_nline, x, a, nline=nline



sizex = size(x)
nx = sizex(sizex(0))
line = fltarr(nx)
npar = n_elements(a)
if (npar-4) mod 3 ne 0 then message,'Invalid parameter array. Must have 4 + 3*nline elements in a.'

nline = (npar - 4)/3


return,  f_vthc_bpow_nline( x, [0.0, 1.0, a] ) + line
end

