;+
; PROJECT:
;   RHESSI/INVERSION
;
; NAME:
;   inv_get_f_thin_ospex
;
; PURPOSE:
;   Grabs all data from OSPEX for inversion
;
; CATEGORY:
;   HESSI, Spectra, Inversion
;
; CALLING SEQUENCE:
;  inv_get_f_thin_ospex,obj,ee,f_thin_model
;
; CALLS:
;   none
;
; INPUTS:
;
;   obj      - OSPEX object name
; efit_range - range of indexes for the inversion
;
; OPTIONAL INPUTS:
;   none
;
; OUTPUTS:
;
;   ee     - photon energy bins 2D array
;   eph    - count  energy bins 2D array
;   Photon - photon flux photons/kev/s/cm^2
;   Ephoton- photon errors photons/kev/s/cm^2
;
; OPTIONAL OUTPUTS:
;   none
;
; KEYWORDS:
;   none
;
; COMMON BLOCKS:
;   none
;
; SIDE EFFECTS:
;
;
; RESTRICTIONS:
;
;
; MODIFICATION HISTORY:
;   eduard(at)astro.gla.ac.uk, 28 October, 2005
;
;-

pro inv_get_f_thin_ospex,obj,efit_range,e_el,e_photons,F_thin_model,I_thin

;@spex_commons

ph_model1 = obj-> getdata(class='fit_function')
I_thin = ph_model1(efit_range)
; photon flux in thin-target model

model = obj -> getdata(class='fit_function')

;f_model=spex_current('f_model',/incommon)
f_model = obj -> get(/fit_function)
IF (f_model NE 'vth+thin') THEN message, 'ERROR: The fit model should be F_VTH_THIN !'

;I_thin = F_VTH_THIN(e_photons,apar)
; photon flux by thin target model

;************************************************************************

me=9.1d-28
;electron mass

ee=e_el

;apar=spex_current('apar',/incommon)
apar = obj -> get(/fit_comp_params)

IF (N_elements(apar) GT 8) THEN message,'Error: only one time interval can be used !'
;*********************************

EM    = apar(0)*1d-6; cm^-3
kT    = apar(1) ; keV
coef  = apar(2) ;*1.d+55
p     = apar(3)
eebrk = apar(4) ; keV
q     = apar(5)
eelow = apar(6) ; keV
eehigh= apar(7) ; keV

T=kT*1.6d-9 ; (kT in erg)


; Convert energies to gammas:

mc2 = 510.98d+00

gamlow = (eelow/mc2)+1.d0
gambrk = (eebrk/mc2)+1.d0
gamhigh = (eehigh/mc2)+1.d0

; Normalization, k, so that Int(dGAMMA)*FCN = 1:

denoma = ((gambrk-1.0d0)^(1.0d0-p))-((gamlow-1.0d0)^(1.0d0-p))
denoma = denoma*(1.0d0-q)
denomb = ((gamhigh-1.0d0)^(1.0d0-q))-((gambrk-1)^(1.0d0-q))
denomb = denomb*(1.0d0-p)*((gambrk-1.0d0)^(q-p))
denom = denoma+denomb
k = (1.0d0-q-p+(p*q))/denom

gamma=(ee+510.98)/510.98 ; Lorentz factor
enererg=ee*1.6d-09
fcn=dblarr(N_elements(ee))
fcnth=dblarr(N_elements(ee))

fcn = (gamma GE gamlow AND gamma LE gambrk)*((gamma-1.0d+00)^(-p))
fcn = fcn+(gamma GT gambrk AND gamma LE gamhigh)*((gambrk-1.0d+00)^(q-p) $
         * (gamma-1.0d+00)^(-q))

fcn = k*fcn
fcn = coef*fcn/510.98
;
; thermal part

fcnth=(2.^(1.5)/sqrt(!pi*me))*em*enererg*t^(-1.5)*exp(-enererg/t)*1.6d-09
;
fcntot=fcn+fcnth


;plot_oo,ee(6:287),Fcntot(6:287),xrange=[10,200]
;m_coef=2.5e+29
;I_thin = F_VTH_THIN(e_photons,apar)

F_thin_model=fcntot

end
