;+
; Name: get_rc_electron_vals
; 
; Purpose: Calculate evolved spectrum due to RC losses and referece flux for injected electrons for f_thick2_rc function
; 
; Input Arguments:
;  energy - energy values to calculate spectrum at in keV
;  par - thick2_rc parameters (see f_thick2_rc for description) 
;  
; Input Keywords:
;  fref_inj - if set, return flux value for injected electrons (the single power law) at reference energy.  This is higher than
;    the evolved spectrum's referece flux at ref energy, since the evolved flux curves down at lower energies because of RC losses.
;    
; Output:
;  If fref_inj is 0 (default), returns evolved spectrum at energy values provided.
;  If fref_inj is set, returns scalar value of injected flux at ref energy.
;  
; Written: Kim Tolbert, 11-Jul-2014
;  
;-

function get_rc_electron_vals, energy, par, fref_inj=fref_inj

fref = par[0]
p = par[1]
ebrk = par[2]
elo = par[4]
ehi = par[5]
eref = par[6]

fref_injected = fref * (1. + ebrk/eref)^p
if keyword_set(fref_inj) then return, fref_injected

frc = fref * ( (energy+ebrk)/(eref+ebrk) )^(-p)
q = where(energy lt elo or energy gt ehi, count)
if count gt 0 then frc[q] = 0.

return, frc
end