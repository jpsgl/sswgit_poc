;+
;
; NAME: 
;	BREM_49
;
; PURPOSE:
;	This function calculates the optically thin continuum thermal bremsstrahlung 
;	photon flux incident on
;	the Earth from an isothermal plasma on the Sun.  Normalization is for an emission
;	measure on the Sun of 1.e49 cm-3
;
; CATEGORY:
;	SPECTRA
;
; CALLING SEQUENCE:
;	Flux = BREM_49( Energy, Kt )
;
; CALLS:
;	ACGAUNT (Gaunt factor function)
;
; INPUTS:
;       Energy - Energy vector in keV
;	Kt     - Plasma temperature in keV, a scalar.
;
; OPTIONAL INPUTS:
;	none
;
; OUTPUTS:
;       Returns the differential photon flux in units of 
;	photons/(cm2 s keV) per (1e49 cm-3 emission measure)
;
; OPTIONAL OUTPUTS:
;	none
;
; KEYWORDS:
;	VERBOSE - If set, prints a message.
; COMMON BLOCKS:
;	none
;
; SIDE EFFECTS:
;	none
;
; RESTRICTIONS:
;	Use at temperatures above line energies.  Never valid for kT lt 0.1 keV.
;
; PROCEDURE:
;	bremsstrahlung radiation function
;	differential spectrum seen at Earth in units of photon/(cm2 s keV)
;
; MODIFICATION HISTORY:
;	Identical with the SMM/HXRBS group's DCP 1VTH for an emission measure of 1.e49 cm-3
;	Version 2, Documented, RAS, 19-May-1996
;	VERSION 3, 21-MAY-1996, Changed to call acgaunt instead of tegfc(obsolete)
;	VERSION 4, 23-oct-1996, Made to return row vector
;-

function brem_49,e,kt , verbose=verbose

if keyword_set(verbose) then print, 'Differential Bremsstrahlung '+$
	'spectrum at Earth for emission measure of 1.e49.'
;
kt0 =( kt(0) > 0.1) ;protect against vectors for kt
result = (1.e8/9.26) * acgaunt(12.3985/E, KT0/.08617) *exp(-(E/KT0 < 50)) /E / KT0^.5

return, result(*)
end
