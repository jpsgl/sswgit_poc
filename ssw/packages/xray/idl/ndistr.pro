; NAME: NDISTR
;
; PURPOSE: This function returns n-distribution of particle
; density normalised to 1, i.e. integral of f(E)dE = 1
; 
; CATEGORY: 
;       SPECTRA
; 
; CALLING SEQUENCE:
;          ndistr = ndistr(en, par)
; 
; INPUTS:
; en - energy in keV
; par - model parametres
; par: par[0] : temperature (in K)
;      par[1] : n-index, must be equal to or larger than 1, in the limit
;               n_index -> 1 it corresponds to Maxwell distribution
;      par[2]: high-energy cutoff
;
; MODIFICATION HISTORY:
; Jana Kasparova, 2-Nov-2009
; Jana Kasparova, 3-Feb-2011 normalisation uses the high-energy cutoff value
;-
function ndistr,en,par

  k_b = 8.617d-8 ; Boltzmann constant (in keV / K)
  ;const = 1d0 / gamma(par[1] / 2d0 + 1d0) / (k_b * par[0])^1.5d0 
  norm = igamma(par[1] / 2d0 + 1d0, par[2]) * gamma(par[1] / 2d0 + 1d0)
  const = 1d0 / norm /(k_b * par[0])^1.5d0 

  ndistr = (en / k_b / par[0])^((par[1] - 1d0)/2d0) * sqrt(en) * exp(-en / k_b / par[0])
  ndistr = ndistr * const
return, ndistr
end
