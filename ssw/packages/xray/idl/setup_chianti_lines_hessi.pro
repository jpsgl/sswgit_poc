kmin=1.0 ;minimum energy in keV for lines
kmax=10.0 ;maximum energy in keV for lines
temp_range = [1., 100.]*1e6 ;range of valid temperatures
ntemp = 500 ; number of valid temperatures
;ntemp = 50 ; for test
ioneq = 'MAZZOTTA'
nelem = 15
;abundance determined after the fact
fileout = setup_chianti_lines(ioneq,  kmin,kmax,$
    temp_range, ntemp=ntemp, nelem=nelem, $
    savfile='chianti_lines_1_10_v50.sav')
    ;savfile='chianti_lines_3_10_v50_test.sav')


end

