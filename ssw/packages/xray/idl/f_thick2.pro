;+
;
; NAME: F_THICK
;
; PURPOSE: This function returns ?
;
; CATEGORY:
;       SPECTRA, XRAYS
;
; CALLING SEQUENCE:
;       Flux = F_THICK2( E, A )
;
; CALLS:
;       F_VTH_THICK2
;
; INPUTS:
; E -energy vector in keV, 2XN edges or N mean energies
; A -model parameters defined below
;   a(0) - Total integrated electron flux, in units of 10^35 electrons sec^-1.
;   a(1) - Power-law index of the electron distribution function below
;          eebrk.
;   a(2) - Break energy in the electron distribution function (in keV)
;   a(3) - Power-law index of the electron distribution function above
;          eebrk.
;   a(4) - Low energy cutoff in the electron distribution function
;          (in keV).
;   a(5) - High energy cutoff in the electron distribution function (in keV).
;
; MODIFICATION HISTORY:
; Kim Tolbert 19-May-2009. Copied from f_thick.  Calls parallel but much faster routines
;   (modifications for speed by Yang Su)
;-

function f_thick2, e, a, _extra=_extra

return, f_vth_thick2 (e, a, /no_vth, _extra=_extra)

end