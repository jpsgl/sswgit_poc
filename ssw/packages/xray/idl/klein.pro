
;+
; Name        :
;	Klein
;               
; Purpose     : 
;	This function returns the Klein-Nishina differential Compton scattering cross-section 
;	in cm^2 electron^(-1) sr^(-1).
;               
; Category    : Spectra
;               
; Explanation : Klein-Nishina formula from Zombeck
;               
; Use         : result = KLEIN( photon_energy, mu )
;    
; Inputs      : 
;	photon_energy - Energy in keV of the incident photon
;	Mu- cosine of the angle between the incident and scattered photons
;               
; Opt. Inputs : None
;               
; Outputs     : None
;
; Opt. Outputs: None
;               
; Keywords    : 
;	TOT - total Compton xsec cm^2/electron
;	ENEW- Energy of Compton scattered photon
; Calls       :
;	Xsec
;
; Common      : None
;               
; Restrictions: Either "photon_energy" or "Mu" may be vectors, but not both.
;	Need more work to reconcile the differential and total cross-sections.
;	ras, 21-May-1996               
; Side effects: None.
;               
; Prev. Hist  : Taken from SPEX directory
;
; Modified    : 
;	Version 1, ras 2-Mar-1996
;	Version 2, ras 29-oct-2014, extensively revised to support vectors for the photon_energy or mu,
;	  the cosine of the scattering angle
;
;-            
function klein, photon_energy, mu, $
  tot = tot , $
  enew=enew, $
  electron_max = electron_max, $
  mu_max = mu_max
 

r0 = 2.82e-13 ;cm classical electron radius
nphot = n_elements( photon_energy )
nmu   = n_elements( mu )

alpha  =  reproduce( photon_energy / 511.0, nmu )
if n_elements( alpha ) gt 1 then alpha = transpose( alpha )
mus   = reproduce( mu, nphot )
;a  = e/511.0
;dsigma = r0^2 * (1./(1.+a*(1-mu)))^3 * ((1+mu^2)/2.)* $
;  (1.+ a^2*(1.-mu)^2/(1.+mu^2)/(1.+a*(1.-mu)))
;enew = 511. / ( 1. - mu + (1/a))
;Replacing the former expressions with these

mmu      = 1.0 - mus ;used a lot in the next lines
alphammu = alpha * mmu
one_alphammu = 1.0 + alphammu
one_mu2  = 1.0 + mus^2
dsigma   = r0^2 * (1.0 / one_alphammu )^3 * ( one_mu2 /2.0 ) * $
           (1.0 + alphammu^2 / one_mu2 / one_alphammu )
enew     = 511.0  / ( mmu + (1.0 /alpha))
electron_max = photon_energy / ( 1.0 + 255.499/ photon_energy ) 
;mu_max 
;here we get the total cross-section for one electron by using germanium to get the
;total Compton xsection and then dividing by the number of electrons
;While there are small variations, you get nearly the same value using Z's from 20-40
tot = xsec( photon_energy, 32, 'si', /barn) /32.0 / 1e24  ;total Compton xsec cm^2/electron

return, reform( dsigma )

end

;Invariance of total Compton xsection per electron using xsec
;IDL> for j=20,40 do print, xsec(150., j, 'si',/barn)/j/1e24
;4.35040e-025
;4.33730e-025
;4.32789e-025
;4.32072e-025
;4.30094e-025
;4.33784e-025
;4.32466e-025
;4.31940e-025
;4.31653e-025
;4.31716e-025
;4.33417e-025
;4.32923e-025
;4.33561e-025
;4.29863e-025
;4.26735e-025
;4.25021e-025
;4.24601e-025
;4.27386e-025
;4.28655e-025
;4.27443e-025
;4.26159e-025