;+
; Name        : Compton_max_electron_energy
;
; Purpose     :
; This function returns the the maximum possible kinetec energy of the scattered electron
; based solely on kinematics, i.e. conservation of energy and momentum. See many, eg Zombeck, for details
;
; Category    : Spectra
;
; Explanation : 
;
; Use         : max_electron_energy = Compton_max_electron_energy( photon_energy )
;
; Inputs      :
; photon_energy - Energy in keV
;
;
; Opt. Inputs : None
;
; Outputs     : Function returns the kinetic energy of the scattered electron in keV
;
; Opt. Outputs: None
;
; Keywords    :              
; Calls       :
; 
;
; Common      : None
;
; Restrictions:
; Side effects: None.
;
; Prev. Hist  :
;
; Modified    :
; Version 1, ras 23-oct-2014
;
;-
function compton_max_electron_energy, photon_energy

alpha = photon_energy/ 511.0

max_electron_energy = photon_energy / ( 1.0 / 2.0 / alpha + 1.0 )
return, max_electron_energy
end