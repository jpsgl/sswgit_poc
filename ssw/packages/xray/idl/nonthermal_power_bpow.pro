;+
; PROJECT:
;   HESSI
;
; NAME:
;   NONTHERMAL_POWER_BPOW
;
; PURPOSE:
;   This routine returns the non-thermal power present in an accelerated (thick-target)
;   electron flux distribution with a broken power-law shape, as used in brm_bremthick.pro.
;   Be careful of the parameter order: they are not exactly the same as in brm_bremthick.pro,
;   and there is of course no thermal component.
;
; CATEGORY:
;   HESSI, Spectra, Modeling
;
; CALLING SEQUENCE:
;   nonthermal_power_bpow(a)
;
; CALLS:
;   -
;
; INPUTS:
;
;	a[0]: total electron flux in 1d35 e-/s
;	a[1]: low-energy cutoff [keV]
;	a[2]: spectral index below the break energy
;	a[3]: break energy [keV]
;	a[4]: spectral index above the break energy
;	a[5]: high-energy cutoff [keV]
;
; OPTIONAL INPUTS:
;   none
;
; OUTPUTS:
;
;   A non-thermal power in keV/s, or erg/s if the /erg keyword is set.
;
; OPTIONAL OUTPUTS:
;   none
;
; KEYWORDS:
;   /erg : if set, routine will return nonthermal power in erg/s. Default is keV/s.
;
; COMMON BLOCKS:
;   none
;
; SIDE EFFECTS:
;
;
; RESTRICTIONS:
;   THe input parameters are not checked for inconsistencies, such as low-energy cutoff < Ebreak and so on.
;
; PROCEDURE:
;
;
; EXAMPLE:
;	PRINT, nonthermal_power_bpow([1,10,4,50,4,300],/erg)
;
;
; MODIFICATION HISTORY:
;	2007/11/12: P. Saint-Hilaire, shilaire@ssl.berkeley.edu
;
;-
FUNCTION nonthermal_power_bpow, a, erg=erg

	tmp=a[1]^a[2] * ( (a[1]^(1.-a[2])-a[3]^(1.-a[2]))/(a[2]-1.) + $
		a[3]^(a[4]-a[2]) * (a[3]^(1.-a[4])-a[5]^(1.-a[4]))/(a[4]-1.) )
	AE1=a[0]*1d35/tmp	;normalizing factor of distribution at low-energy cutoff a[1]
	Pnth=AE1*a[1]^a[2] * ( (a[1]^(2.-a[2])-a[3]^(2.-a[2]))/(a[2]-2.) + $
		a[3]^(a[4]-a[2]) * (a[3]^(2.-a[4])-a[5]^(2.-a[4]))/(a[4]-2.) )
	IF KEYWORD_SET(erg) THEN Pnth*=1.6d-9

	RETURN, Pnth
END
