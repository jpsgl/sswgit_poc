;+
; Name: F_TEMPLATE_INTERPOLATE
;
; Purpose: This function interpolates the template from the template energy bins
;	to the user energy bins, edges2, that normally come from the input (photon) side of the
;	detector response
;	30-jan-2015, RAS, always use interp2integ as that is always more robust
;-
function  f_template_interpolate, edges2,  wkev,  enucl, fnucl, wnucl, flux_ekev, $
	minmax_ratio=minmax_ratio
default, minmax_ratio, 1e12
enucl[0] = enucl[0] > 1.001
nnucl = n_elements( fnucl)
  is_wnucl = exist( wnucl)
  ;identify or determine bin widths
  if ~is_wnucl then begin
  	  nnucl = n_elements(fnucl)
	  wnucl = enucl[1:*]-enucl
	  lwnucl = alog( enucl[1:*]/enucl)
	  ;is it log(really non-stepwise linear indicating smooth function) or stepwise linear
	  linlog = stddev( wnucl[0:9]) lt stddev( lwnucl[0:9] )
	  ;if it is linear, then adjust boundaries as needed
	  if linlog then begin ;stepwise fixed bins
	  	z = where( ((wnucl[1:*] - wnucl) gt .1) and ((wnucl[2:*] - wnucl[1:*]) gt .1), nz) +1
	  	if nz ge 1 then wnucl[z] = wnucl[z-1]
	  	;18-aug-2011, ras, move the "wnucl = ..." expression to after this endif
	  	;and not the one after
	  	;change from this
	  	;endif
	  ;endif else wnucl = get_edges( [enucl, enucl[nnucl-1]^2/enucl[nnucl-2]],/width)
	  ;to this:
	  	endif else wnucl = get_edges( [enucl, enucl[nnucl-1]^2/enucl[nnucl-2]],/width)
	  endif
   ;Extend the template to ensure any extrapolation above the last energy is 0.0
  enucl = [enucl, last_item(enucl)+10.]
  wnucl = [wnucl, 10.]
  fnucl = [fnucl, 0.0]
  ;Normalize fluxes to unity, just in case, flux is normalized over the enucl range
  tflux = total( fnucl * wnucl )
  if (abs(tflux-1) gt .1) then begin
  	fnucl = fnucl/tflux ;wasn't normalized, so normalize over enucl range
  	message, /info, 'Template flux renormalized over enucl range to 1 photon/cm2'
  	endif
  ;For linear case we need to do careful interpolation so use interp2integ
  if ~exist(linlog) then begin
  	 lwnucl = alog( enucl[1:*]/enucl)
	  ;is it log or stepwise linear
	  linlog = stddev( wnucl[0:9]) lt stddev( lwnucl[0:9] )
	  endif
	;30-jan-2015, always use interp2integ as that is always more robust
  ;if linlog then begin
  if 1 then begin
	tflux = interp2integ( edges2, enucl, fnucl) >0
	flux_ekev = f_div(tflux,wkev) ;/ total(tflux)
	endif else flux_ekev = interpol( fnucl, enucl, get_edges(edges2,/gmean))
	mxflux     = max(flux_ekev)
	mnflux     = mxflux / minmax_ratio
	flux_ekev = (flux_ekev > mnflux) > 0.0
	return, flux_ekev
end

;+
; Name: F_TEMPLATE
;
; Purpose:  This routine interpolates a template-defined function of energy onto arbitrary energy edges
;
; Method:  A template file is supplied either through a shortcut name keyword or a file name.  For .sav files
;  the file is restored, the flux in the file is interpolated to the current edges, divided by energy bin
;  widths, and divided by the total of the flux.  The interpolated edges and flux are stored in a data_cache
;  object so that that operation doesn't have to be repeated again for the same edges during the current
;  idl session (the data_cache obj stores each set of arrays in common along with an identifer which contains
;  the file name and number of interpolated edges).
;  For .txt files, eval_line_complex is called.
;
; Inputs:
;
;	EkeV: 2xN energy edges in keV
;	A:  Normalization - default is in 1 photon /cm2 at the Earth for A[0] of 1.0
;	Keywords:
;	File: Supply name of template file to use OR use shortcut keywords listed below.
;	  Complete filename or filename w/o extension or directory, must be idl save or text file
;		If no extension adds ".sav"
;		Looks first on current directory and then SSWDB_XRAY
;		If it can't find a ".sav" file it tries a .txt extension.
;		Defaults to 'brd+nar_ap022_s400_theta60.sav', unless you specify one of the shortcut keywords
;		.sav file should have two variables, enucl and fnucl which are the energy bins and fluxes
;		.txt files should be a text file, each line has the relative flux, line center, and line width
;		NOTE: If file is set and one of the following shortcuts is set, file takes precedence.
;	BRD_NUC shortcut keyword - specifies file = BRD_ap022_s400_theta60.sav
;	NAR_NUC shortcut keyword - specifies file = NAR_ap022_s400_theta60.sav
;	BRD_NAR_NUC shortcut keyword - specifies file = BRD+NAR_ap022_s400_theta60.sav
;	PION_S30 shortcut keyword - specifies file = pi_ap01_s30_b300_nh1e15.sav
;	PION_S35 shortcut keyword - specifies file = pi_ap01_s35_b300_nh1e15.sav
;	PION_S40 shortcut keyword - specifies file = pi_ap01_s40_b300_nh1e15.sav
;	PION_S45 shortcut keyword - specifies file = pi_ap01_s45_b300_nh1e15.sav
;	PION_S50 shortcut keyword - specifies file = pi_ap01_s50_b300_nh1e15.sav
;	NUC1: shortcut keyword - specifies file= 'nuclear_template.sav'
;	NUC2  shortcut keyword - specifies file = 'nuclear_template1.sav'
;	VERN  shortcut keyword - specifies file = 'vern_50e3.sav'
;	ALPHA  shortcut keyword - specifies file = 'alpha_dniso_35_60.sav'
;	FLINE  shortcut keyword - specifies file = 'sline_full_smm.txt'
;	BLINE  shortcut keyword - specifies file = 'sline_broad_smm.txt'
;	NLINE  shortcut keyword - specifies file = 'sline_narrow_smm.txt'
;	USER   shortcut keyword - specifies user-supplied file named 'user_template.sav' or 'user_template.txt'
;
;	MINMAX_RATIO - resultant function is always gt then the max(F)/minmax_ratio
;		where F is the function defined on the range given by EkeV
;
; History
;	ras, 11-oct-2007, Based on Nuclear_Table_load
;	Kim  16-Oct-2007, added shortcut names for input files
;	Kim  Sep-2009, Use data_cache obj to store interpolated file results for each file read, so unless a new
;	     file is requested, don't have to read and interpolate on every call
;	Kim  26-Sep-2009. Destroy cache obj.  Added catch to catch all error cases and return an array of 0s.
;	     Added #edges to name of cache id, so if number of edges changes but filename doesn't, it knows it's different.
; Kim 14-Oct-2009. 	Ensure the template extrapolates to 0. at high energies by adding an extra energy to enucl
;      and an extra value of 0. to fnucl
; Kim 12-Nov-2009.  DMZ changed data_cache obj to return empty string instead of undefined variable when no data. Changed test.
;	ras, 6-jun-2011, normalization is now over the range of the template and so is fixed regardless of the
;		range of the drm photon bins.  Normalizations are normally 1 ph/cm2 for templates that come from Ron Murphy, NRL
; Kim 15-Aug-2012, added brd_nuc,nar_nuc,brd_nar_nuc, and the 5 pion templates with spectral index 3 to 5, and changed
;   default to brd_nar_nuc.  Left the old shortcut templates (nuc1,nuc2,vern,alpha,fline,bline,nline) as is, but they are all 
;   old - the ones just added are more complete.
; RAS 30-jan-2015, always use interp2integ as that is always more robust inside interpolation routine from template to user bins
;-
function f_template, ekev, a, $
	file=file, error=error, minmax_ratio = minmax_ratio, $
	brd_nuc=brd_nuc, nar_nuc=nar_nuc, brd_nar_nuc=brd_nar_nuc, $
	pion_s30=pion_s30, pion_s35=pion_s35, pion_s40=pion_s40, pion_s45=pion_s45, pion_s50=pion_s50, $
	nuc1=nuc1, nuc2=nuc2, vern=vern, alpha=alpha, _extra=_extra, $
	fline=fline, bline=bline, nline=nline, user=user

if not keyword_set(file) then begin
	case 1 of
	
	  keyword_set(brd_nuc): file = 'brd_ap022_s400_theta60.sav'
	  keyword_set(nar_nuc): file = 'nar_ap022_s400_theta60.sav'
	  keyword_set(brd_nar_nuc): file = 'brd+nar_ap022_s400_theta60.sav'
	  keyword_set(pion_s30): file = 'pi_ap01_s30_b300_nh1e15.sav'
	  keyword_set(pion_s35): file = 'pi_ap01_s35_b300_nh1e15.sav'
	  keyword_set(pion_s40): file = 'pi_ap01_s40_b300_nh1e15.sav'
	  keyword_set(pion_s45): file = 'pi_ap01_s45_b300_nh1e15.sav'
	  keyword_set(pion_s50): file = 'pi_ap01_s50_b300_nh1e15.sav'
	
		keyword_set(nuc1): file = 'nuclear_template.sav'
		keyword_set(nuc2): file = 'nuclear_template1.sav'
		keyword_set(vern): file = 'vern_50e3.sav'
		keyword_set(alpha): file = 'alpha_dniso_35_60.sav'
		keyword_set(fline): file = 'sline_full_smm.txt'
		keyword_set(bline): file = 'sline_broad_smm.txt'
		keyword_set(nline): file = 'sline_narrow_smm.txt'
		keyword_set(user):  file = 'user_template'
		
	endcase
endif
extnsn = ['.sav','.txt']
error = 0

default, file, 'brd+nar_ap022_s400_theta60.sav'

break_file, file, disk, dir, fnam, ext

ext = keyword_set(ext) ? ext : extnsn
diskdir = keyword_set(disk) or keyword_set(dir) ? disk + dir : getenv('SSWDB_XRAY')

fnam = fnam + ext
test = file_exist(fnam)
; if exists in current dir, use that, otherwise use diskdir
filename = total(test)<1 ? fnam : concat_dir(diskdir, fnam)

edge_products, ekev, edges_1=edges,  width=wkev, edges_2=edges2
nedges = n_elements(wkev)

catch, error_catch
if error_catch ne 0 then begin
  message, /cont, !error_state.msg
  message, /cont, ' Returning array of 0s.'
  destroy, cache  ; won't complain if cache doesn't exist yet
  error = 1
  return,  fltarr(nedges)
  catch, /cancel
endif

if not (total(test)<1) then begin
	test = file_exist(filename)
	; if none of files exist, this will jump up to catch and return an array of 0s
	if not total(test)<1 then message, 'File match not found for '+file
endif

; Interpolate the flux onto the new bins, integrate, and divide by the
; bin width.
default, minmax_ratio, 1e12

;check for sav in first file that exists
sel = where(test)
filename = filename[sel[0]]

;message, 'Using file ' + filename, /cont

if stregex(filename,/fold,'.sav',/boolean) then begin
  ; if already read this file and calculated flux_ekev with the current
  ; edges, then it's stored in cache.  Just restore and return * a[0]
  cache = obj_new('data_cache')
  cache->set,name=filename+trim(nedges); , /verbose
  cache->restore,struct
  if is_struct(struct) then begin
    if same_data(struct.edges2, edges2) then begin
      obj_destroy, cache
;      print,'restored from ' + filename+trim(nedges)
      return, a[0] * struct.flux_ekev
    endif
  endif
	restore, filename  ;returns Energy edges, enucl, in keV for fluxes, fnucl, ph/cm2/keV,  normalized to 1 photon/cm2
	; if restore doesn't have enucl or fnucl, then bad file.  Jump to catch and return 0s
	if not exist(enucl) or not exist(fnucl) then message, filename + ' is not a nuclear template file.'
  ;interpolate fnucl onto edges2 from enucl
  ;;;;
  flux_ekev = f_template_interpolate(edges2,  wkev,  enucl, fnucl, wnucl,  minmax_ratio=minmax_ratio)
  ; midpoint of first channel must be > 1.0 keV

	;;;;;
	cache->save, {edges2: edges2, flux_ekev: flux_ekev}
	obj_destroy, cache

endif else flux_ekev =  eval_line_complex( ekev, filename, err_msg=err_msg, err_code=err_code)

error = 0
return, a[0] * flux_ekev
end