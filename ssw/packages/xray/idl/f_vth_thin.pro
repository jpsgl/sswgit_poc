;+
;
; NAME:
; Flux model of thermal bremsstrahlung plus thin-target broken
; power-law electron distribution function (GHolman)
;
; PURPOSE:
; This function returns the differential photon spectrum seen at the Earth
; for a two component model comprised of a thermal
; bremsstrahlung function plus a thin-target,
; non-thermal broken power-law function with low- and high-energy cutoffs.
;
; CATEGORY:
;       SPECTRA, XRAYS
;
; CALLING SEQUENCE:
;       Flux = F_VTH_THIN( E, A )
;
; CALLS:
;       F_VTH, BRM_BREMSPEC,  EDGE_PRODUCTS, CHECKVAR
;
; INPUTS:
; E -energy vector in keV, 2XN edges or N mean energies
; A -model parameters defined below
; For the thermal function using F_VTH.PRO:
;   a(0)= emission measure in units of 10^49 cm-3
;   a(1)= KT   plasma temperature in keV
;
;
; For the "non-thermal" function it uses the broken power law
; function, F_BREMSPEC.PRO:
;   a(2) - normalization factor in units of 1.0d55 cm-2 sec-1,
;             i.e. plasma density * volume of source * integrated nonthermal electron flux
;   a(3) - Power-law index of the electron distribution function below
;          a(4) (=eebrk).
;   a(4) - Break energy in the electron distribution function (in keV)
;   a(5) - Power-law index of the electron distribution function above
;          eebrk.
;   a(6) - Low energy cutoff in the electron distribution function
;          (in keV).
;   a(7) - High energy cutoff in the electron distribution function (in keV).
;
; COMMON BLOCKS:
; FUNCTION_COM
;
; HISTORY:
; VERSION 1 2002/11/19 Adapted from f_vth_thick.pro for compuation of
;                electron mean flux distribution from photon spectra
;     Lin   2002/12/12 Fix infinity and nan output, spetral index 1.0 is avoided
;   26-mar-2003, richard.schwartz@gsfc.nasa.gov - call f_vth with 2xN energy edges.
;   24-Mar-2004, Kim Tolbert - added no_vth and _extra keywords.  If no_vth set,
;     then 6 params are for brm_bremspec, otherwise first two are for vth, next
;     six are for brm_bremspec.
; 2004/04/16, Linhui Sui, to speed up the fitting with OSPEX, check the normalization factor a[2],
;             if eq 0 then do not call thick target function brm_bremspec.pro
;-
function f_vth_thin, e, a, no_vth=no_vth, _extra=_extra


@function_com
;FUNCTION_COM - Common and common source for information on fitting functions
;
;common function_com, f_model, Epivot, a_cutoff

if (size(e))(0) eq 2 then edge_products, e, mean=em else em=e

if keyword_set(no_vth) then begin
    maxpar = 6
    offset = 0
endif else begin
    maxpar = 8
    offset = 2
endelse

checkvar,apar,dblarr(maxpar)

npar = n_elements(a)
apar(0) = a(0: (npar-1)< (n_elements(apar)-1) )
if total(abs(apar(4+offset:5+offset))) eq 0.0 then apar(4+offset)=a_cutoff

;spectral index 1.0 will cause NaN flux
if (apar[1+offset] eq 1.0) then apar[1+offset] = 1.01
if (apar[3+offset] eq 1.0) then apar[3+offset] = 1.01

if apar[0+offset] eq 0. then ans = 0. else $
ans = apar[0+offset]* 1.0d55 * brm_bremspec(em,apar(0+offset:5+offset))
if not keyword_set(no_vth) then ans = ans + f_vth(e, apar(0:1))

;If flux is NaN or infinity, set them to small numbers
NaNInd = where((finite(ans, /infinity) eq 1) or (finite(ans, /nan) eq 1))
if (NaNInd[0] ge 0) then ans[NaNInd] = 1.0e-27

return,ans
end