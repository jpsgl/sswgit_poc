;+
; PROJECT:
;   XRAY
; NAME:
;   XR_RD_ABUNDANCE
;
; PURPOSE:
;   This returns the abundances written in the xray_abun_file.genx
;   The abundances are taken from CHIANTI and MEWE.  The source filenames are:
;   cosmic sun_coronal sun_coronal_ext sun_hybrid sun_hybrid_ext sun_photospheric mewe_cosmic mewe_solar
;   The first six come fron Chianti, the last two from Mewe.  They are:
;   cosmic sun_coronal sun_coronal_ext sun_hybrid sun_hybrid_ext sun_photospheric mewe_cosmic mewe_solar

;
; CATEGORY:
;
;
; CALLING SEQUENCE:
;   abundance = xr_rd_abundance( [filnam,] [/coronal,] [/photospheric,] [/mewe,][/cosmic,]
;     [/hybrid,][/ext])
;
; CALLS:
;   none
;
; INPUTS:
;       Filnam - optional, give it one of the eight filenames (string)
;      From Chianti
;      1. cosmic
;      2. sun_coronal - default abundance
;      3. sun_coronal_ext
;      4. sun_hybrid
;      5. sun_hybrid_ext
;      6. sun_photospheric
;      7. mewe_cosmic
;      8. mewe_solar - default for mewe_kev
;
; OPTIONAL INPUTS:
;   none
;
; OUTPUTS:
;       none explicit, only through commons;
;
; OPTIONAL OUTPUTS:
;   none
;
; KEYWORDS:
;     VERBOSE - If set, display the filnam used.
;     CORONAL - USE ONE OF CHIANTI CORONAL ABUNDANCES
;     HYBRID - SELECT ONE OF TWO HYBRID CHIANTI ABUNDANCES
;     EXT - SELECT ONE OF THE TWO EXTENDED CHIANTI ABUNDANCES
;     PHOTOSPHERIC - SELECT THE CHIANTI PHOTOSPHERIC ABUNDANCE
;     COSMIC - SELECT ONE OF THE TWO COSMIC ABUNDANCES
;     MEWE - SELECT ONE OF THE TWO MEWE ABUNDANCES
;     MEWE HAS PRECEDENCE, THEN COSMIC, THEN CORONAL, THEN HYBRID
;      EXT MUST BE USED IN COMBINATION WITH CORONAL OR HYBRID

; COMMON BLOCKS:
;   none
;
; SIDE EFFECTS:
;   none
;
; RESTRICTIONS:
;   none
;
; PROCEDURE:
;   none
;
; MODIFICATION HISTORY:
;   Version 1, richard.schwartz@gsfc.nasa.gov, 8-FEB-2005
;	16-feb-2006, richard.schwartz@gsfc.nasa.gov, added sun_photospheric to case statement
;
;-
function xr_rd_abundance, filnam,  $
    coronal=coronal, $
    photospheric=photospheric, $
    hybrid=hybrid,$
    ext=ext, $
    mewe=mewe, $
    cosmic=cosmic,$
    verbose=verbose

chianti_ab_file = getenv('CHIANTI_AB_FILE')
if file_exist( chianti_ab_file ) then file = chianti_ab_file else begin
    xray_chianti = concat_dir('SSWDB_XRAY','chianti')
    file = loc_file( path=[curdir(),xray_chianti],'xray_abun_file.genx')
    endelse


restgen, ab, file=file[0]

default,filnam, ''

select = where( strlowcase(filnam) eq ab.filnam, count)

if count then begin
    if keyword_set(verbose) then print, filnam
    return, ab[select[0]].abund
    endif
if count eq 0 and filnam ne '' then message,/continue,'Invalid abundance filename'

;No selection yet
photospheric = keyword_set( photospheric )
ext = keyword_set( ext )
hybrid = keyword_set( hybrid )
coronal = keyword_set( coronal )
mewe = keyword_set( mewe)
cosmic = keyword_set( cosmic )
default, ext, 0
default, hybrid, 0
default, coronal, 0
default, mewe, 0
default, cosmic, 0
photospheric = 1 - coronal


case 1 of
    mewe: filnam = cosmic ?'mewe_cosmic' : 'mewe_solar'
    cosmic: filnam = 'cosmic'
    coronal: filnam = ext ? 'sun_coronal_ext' : 'sun_coronal'
    hybrid: filnam = ext ? 'sun_hybrid_ext'  : 'sun_hybrid'
    photospheric: filnam = 'sun_photospheric'
    else: filnam = 'sun_coronal
    endcase


select = where( filnam eq ab.filnam, count)

if count then begin
    if keyword_set(verbose) then print, filnam
    return, ab[select[0]].abund
    endif

end
