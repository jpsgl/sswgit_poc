;+
;
; NAME: F_THICK2_VNORM
;
; PURPOSE: Thick Target Bremsstrahlung Version 2 with independent normalization
; 
; METHOD: This function is the same as F_THICK2 except 
;       1.  the normalization and low energy cutoff
;       parameters are independent (in F_THICK2 they are highly coupled).
;       2.  The user can set the reference energy (should be between the low
;       energy cutoff and the break energy), but should not fit on this parameter.
;       3. There is an extra parameter (the reference energy)
;
; CATEGORY:
;       SPECTRA, XRAYS
;
; CALLING SEQUENCE:
;       Flux = F_THICK2_VNORM( E, A )
;
; CALLS:
;       F_VTH_THICK2
;
; INPUTS:
; E -energy vector in keV, 2XN edges or N mean energies
; A -model parameters defined below
;   a(0) - Electron flux at reference energy (a[6]), in units of 10^35 electrons (sec kev)^(-1)
;   a(1) - Power-law index of the electron distribution function below
;          eebrk.
;   a(2) - Break energy in the electron distribution function (in keV)
;   a(3) - Power-law index of the electron distribution function above
;          eebrk.
;   a(4) - Low energy cutoff in the electron distribution function
;          (in keV).
;   a(5) - High energy cutoff in the electron distribution function (in keV).
;   a(6) - Reference energy (in keV) at which function is normalized (KEEP FIXED DURING FIT)
;
; MODIFICATION HISTORY:
; Kim Tolbert 19-May-2009. Copied from f_thick.  Calls parallel but much faster routines
;   (modifications for speed by Yang Su)
; Kim Tolbert, 26-May-2011, Copied from f_thick2.  Same except calls f_vth_thick2 with /independent_norm
; Kim Tolbert, 26-Jun-2014, Pass ref_energy keyword with value of a[6] through (previously couldn't change from 50.)
;-

function f_thick2_vnorm, e, a, _extra=_extra

return, f_vth_thick2 (e, a, /no_vth, /independent_norm, ref_energy=a[6], _extra=_extra)

end