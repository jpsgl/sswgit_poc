;+
;Name: CHIANTI_KEV_VERSION
;
;Purpose: Report on Chianti version used to compile line and continuum databases
; for CHIANTI_KEV
;
;History: 21-mar-2006, richard.schwartz@gsfc.nasa.gov

function chianti_kev_version
	chianti_kev_common_load
	common chianti_kev_lines, zindex, out, ion_info, file
return, out.version
end
