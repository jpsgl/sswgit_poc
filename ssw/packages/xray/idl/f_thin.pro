;+
;
; NAME: F_THIN
;
; PURPOSE: This function returns ?
;
; CATEGORY:
;       SPECTRA, XRAYS
;
; CALLING SEQUENCE:
;       Flux = F_THIN( E, A )
;
; CALLS:
;       F_VTH_THIN
;
; INPUTS:
; E -energy vector in keV, 2XN edges or N mean energies
; A -model parameters defined below
;   a(0) - normalization factor(in 1.0d55),
;             i.e. plasma density *	electron density * volumn of source * electron velocity
;   a(1) - Power-law index of the electron distribution function below
;          eebrk.
;   a(2) - Break energy in the electron distribution function (in keV)
;   a(3) - Power-law index of the electron distribution function above
;          eebrk.
;   a(4) - Low energy cutoff in the electron distribution function
;          (in keV).
;   a(5) - High energy cutoff in the electron distribution function (in keV).
;
; MODIFICATION HISTORY:
; Kim Tolbert 24-Mar-2004
;-

function f_thin, e, a, _extra=_extra

return, f_vth_thin (e, a, /no_vth, _extra=_extra)

end