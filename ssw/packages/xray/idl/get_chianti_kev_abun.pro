;+
; PROJECT:
;   XRAY PACKAGE
; NAME:
;   GET_CHIANTI_KEV_ABUN
;
; PURPOSE:
;   This returns the abundances in use for the first 50 elements that
;   are currently used in chianti_kev().
;
; CATEGORY:
;   XRAY
;
; CALLING SEQUENCE:
;   abundance = get_chianti_kev_abun( )
;
; CALLS:
;   none
;
; INPUTS:
;       none explicit, only through commons
;
; KEYWORD INPUTS:
;
; OUTPUTS:
;       Returns an array of abundances relative to the abundance of Hydrodgen
;
; OPTIONAL OUTPUTS:
;   none
;
; KEYWORDS:
;   none
; COMMON BLOCKS:
;   CHIANTI_KEV_abundance, CHIANTI_KEV_cont
;
; SIDE EFFECTS:
;   none
;
; RESTRICTIONS:
;
;
; PROCEDURE:
;   Reads values out of common blocks.
;
; MODIFICATION HISTORY:
;   Version 1, richard.schwartz@gsfc.nasa.gov, 31-JAN-2005
;
;-



function get_chianti_kev_abun

common chianti_kev_abundance, abundance, abfilnam, rel_abun_com

common chianti_kev_cont, zindex, totcont, edge_str, ctemp, chianti_doc, file

out = abundance
mask = abundance
mask[zindex] = 0
out = out - mask


return, out
end