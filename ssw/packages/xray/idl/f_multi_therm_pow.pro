;+
;
; NAME:
; 		F_MULTI_THERM_POW
;
; PURPOSE:
; This function returns the photon spectrum seen at the Earth
; for a multithermal model (optically thin thermal bremsstrahlung function,
; normalized to the Sun-Earth distance)
; The differential emission measure has a power-law dependence on temperature.
;
; CATEGORY:
;       SPECTRA, XRAYS
;
; CALLING SEQUENCE:
;       Flux = f_multi_therm_pow( eph, a )
;
; CALLED BY: f_mth_pow_bpow
;
; CALLS:
;       f_therm_dem_pow
;       brm_gauleg
;
; INPUTS:
; eph - energy vector in keV, 2N edges or N mean energies
; a -   model parameters defined below
;   a(0) = differential emission measure at T = 2 keV in units of 10^49 cm^(-3) keV^(-1)
;   a(1) = minimum plasma temperature in keV
;   a(2) = maximum plasma temperature in keV
;   a(3) = power-law index for calculating the differential emission measure at temperature T:
;          DEM(T) = a(0) * (2./T)^a(3)
;   a(4)  Relative abundance for Iron and Nickel
;            Relative to coronal abundance for chianti
;            Relative to solar abundance for mewe
;           (unless user selects a different abundance table manually)
;   If rel_abun keyword is used, apar(4) is set to keyword value.
;
; KEYWORD INPUTS:
;   REL_ABUN - 2x2 array giving Fe, Ni abundance [ 26,x],[28,x] ],  If rel_abun keyword not used,
;     the value of x is taken from apar(4) to make 2x2 array.  If that's not there either, x is 1.
;   In _extra, can pass /full, /continuum, /lines, /mewe, /chianti keywords (see f_vth)
;
; OUTPUTS:
; Array of photon fluxes at photon energies determined by eph
;
; PROCEDURE:
; Thermal bremsstrahlung fluxes are computed by integrating the isothermal bremsstrahlung
; from plasma in the temperature range a(1) (t_min) to a(2) (t_max) with differential
; emission measure DEM(T).  The DEM and bremsstrahlung emissivity are provided in F_THERM_DEM_POW.
; The integration is performed using Gaussian Quadrature.  The Gaussian Quadrature abscissas
; and weights are provided by BRM_GAULEG.
;
; WRITTEN: Linhui Sui, 2003/08/28
;
; REVISED: Gordon Holman, 2003/09/04, Enhanced documentation.
; Linhui Sui, 2004/03/04: check a[0] before start computation. If a[0]= 0., then
;						  output zero fluxes at all energy bands
;
; Kim Tolbert, 2004/03/04 - added _extra so won't crash if keyword is used in call
; Kim Tolbert, 2006/03/21 - pass _extra through to f_therm_dem_pow
; Kim, 19-Apr-2006.  Now has 5 params - added abundance (for Fe,Ni) as 5th param.
;-

function f_multi_therm_pow, eph, a, rel_abun=rel_abun, _extra=_extra

emission = a[0]
t_min = a[1]
t_max = a[2]
alpha = a[3]

abun = n_elements(a) eq 4 ? 1. : a[4]
rel_abun = keyword_set(rel_abun) ? rel_abun : reform( [26,abun,28,abun], 2,2)

;print,'rel_abun in f_multi_therm_pow = ',reform(rel_abun,4)


estep = (size(eph))[2]

;	Create arrays for integral sum

intsum = FltArr(estep)


;check whether a[0] eq 0, if so, return zero flux
if a[0] eq 0. then goto, conv

if t_min ge t_max then begin
   message,'Minimum temperature is > maximum temperature.', /cont
   goto, conv
endif


tlowarr = fltarr(estep) + t_min

;   Specify the maximum order of the Gaussian quadrature integration
;   before ending without convergence.  MAXFCN should be less than
;   2^(NLIM).

maxfcn = 2048

;   Specify the maximum relative error allowed for convergence

rerr = 0.001

l = Where(tlowarr)

;	Maximum possible order of the Gaussian quadrature integration is 2^(nlim).

nlim = 12
FOR ires = 1, nlim DO BEGIN

	npoint = 2L^ires
	IF (npoint GT maxfcn) THEN GOTO, noconv

	eph1 = eph[*, l]

	;	Create arrays for abscissas and weights.

	x = DblArr(N_Elements(l),npoint)
	w = DblArr(N_Elements(l),npoint)

	;	Use Brm_GauLeg to provide the abscissas and weights.

	Brm_GauLeg, tlowarr(l), t_max, npoint, x, w


    ;   Save the last approximation to the integral.

	lastsum = intsum

    ;   Obtain the numerical approximation to the integral.

	intsum(l) = (w * f_therm_dem_pow(eph1, x,  emission, alpha, $
	                  rel_abun=rel_abun, _extra=_extra)) $
		         # (Make_Array(npoint, Value=1))

	;	Use last two calculations to check for convergence.

	l1 = Abs(intsum-lastsum)
	l2 = rerr*Abs(intsum)

    ;   Obtain the indices of those integrals that have not yet converged.

	l = Where(l1 GT l2)

    ;  Return the computed fluxes when all of the integrals have converged.

	IF (l(0) EQ -1) THEN GOTO, conv

ENDFOR


noconv:

	print, '!!!!!!!!!!!!!!! no convergence'

conv:

	mmflux = intsum

RETURN, mmflux

END






