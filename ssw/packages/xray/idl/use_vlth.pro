;+
; Name:
;	 USE_VLTH
; Purpose:
; 	This procedure sets the logical scalar USE_VLTH in the common block f_vth_com
;
; 	if set, then thermal spectrum includes line spectrum from MEWE_SPEC
; Category:
;	spectra, xrays
; Inputs:
;	/set	Use Mewe_spec to generate thermal bremsstrahlung model
;       /noset  Just use continuum spectrum from Brem_49.pro
;       default is just the continuum spectrum
;
; Calls:
;	FCHECK
; common block
;	f_vth_com
; History:
;	ras, 18-may-94
;-
pro use_vlth, set=set, noset=noset

common f_vth_com, use_vlth

if fcheck( set, 0) then use_vlth=1 else use_vlth=0

if fcheck( noset, 0) then use_vlth=0

end
