;+
;
; NAME:
;	FLUORESCENCE
;
; PURPOSE:
;	Calculate X-ray fluorescence from the escape of K-alpha,beta  photons
;	from a detector surface
; CATEGORY:
;
;
; CALLING SEQUENCE:
;	result = fluorescence( E,kp,ke,detector=det,error=error)
;
; CALLED BY:
;
;
; CALLS:
;	xsec
;
; INPUTS:
;	E		Input energy vector
;	detector	only 'Ge','NaI', 'CsI', 'Si', 'CDTE' or 'SXS'
;			SXS refers to the Yohkoh Soft Xray Spectrometer (Xe and CO2)
;			default is Germanium
; OPTIONAL INPUTS:
;	theta - 	angle of incidence of the beam to the normal, degrees
;			default is 0 degrees
; OUTPUTS:
;	kp		Probability of fluorescence at ke
;	ke		Vector of escape energies
;
; OPTIONAL OUTPUTS:
;	ERROR - Set to 1 if detector isn't from list.
;
; COMMON BLOCKS:
;	none
;
; SIDE EFFECTS:
;	none
;
; RESTRICTIONS:
;	only selected detector materials allowed
;
; PROCEDURE:
;	none
;
; MODIFICATION HISTORY:
;	ras, 20-dec-94
;	RAS, 13-NOV-2012, ADDED CDTE
;-


pro fluorescence,e,kp,ke,detector=det, theta=theta, error=error
;calculate the fluorescence K escape fractions in one of 5 detectors
;materials, Germanium, CsI, Si, SXS (Xe and CO2), or NaI

costhet = cos( fcheck( theta,0.)/!radeg )
error = 0
checkvar,det, 'GE'
dt=strupcase(det)
;WHAT DETECTOR MATERIAL?
arg = ['GE','NAI','CSI','SXS','SI','CDTE']
detector = where( dt eq arg,count)

;NONE FOUND
if count eq 0 then begin ;error return
	error=1
	print,'SELECT DETECTOR FROM ',arg
	return
endif

num = n_elements(E) ;

if dt eq 'GE' then begin

	mu1  = xsec(E,32.,'pe')/5.323 ;pe cross-section at E in gm/cm^2
	edge = [11.1,11.1]
	ke   = [9.87,10.98] ;energy of k shell emission
	w    = [[.471+fltarr(num)],[.0691+fltarr(num)]] ;Fluor. prob. at kp
	mu0  = xsec(ke,32.,'pe')/5.323
endif

if dt eq 'SI' then begin

	mu1  = xsec(E,14.,'pe')/2.33 ;pe cross-section at E in gm/cm^2
	edge = [1.839]
	ke   = [1.839-.118] ;energy of k shell emission
	w    = [.03+fltarr(num)] ;Fluor. prob. at kp
	mu0  = xsec(ke,14.,'pe')/2.33
endif

if dt eq 'NAI' then begin

	mu  = [[xsec(E,11.,'pe',/cm2)*23.0],[xsec(E,53.,'pe',/cm2)*126.9]] $
		/(23.0+126.9)/3.67
	mu1 = mu[*,0] + mu[*,1] ;add the xsections together
	w   = (0.882+fltarr(num,1))*mu[*,1]/mu1 ;prob. that pe results in fluor. in I
	edge= [33.2]
	ke  = [29.0]
	mu0 = 	(xsec(ke,11.,'pe',/cm2)*23.0+xsec(ke,53.,'pe',/cm2)*126.9) $
		/(23.0+126.9)/3.67

endif

if dt eq 'CSI'   then begin

	mu = [[xsec(E,55.,'pe',/cm2)*132.9],[xsec(E,53.,'pe',/cm2)*126.9]] / $
		(132.9+126.9)/4.51
	mu1 = mu[*,0] + mu[*,1] ;add the xsections together
	w   = [[0.90*mu[*,0]/mu1],[0.882*mu[*,1]/mu1]]
	edge= [36.0,33.2]
	ke  = [30.2,29.0]
	mu0 = 	(xsec(ke,55.,'pe',/cm2)*132.9+xsec(ke,53.,'pe',/cm2)*132.9) $
		/(132.9+126.9)/4.51

endif
if  dt eq 'CDTE' then begin
	gmcm3 = 5.85 ;g cm-3
;CD 48 186.6 22.9841(53.2) 23.1736(100) 26.09(27.3) 26.684(5.3)  0.84
;IDL> print, coef[48].edge
;     0.000000     0.825000      4.23800      27.9400      10000.0
;Te 52 211.9 27.2017(53.7) 27.4723(100) 31.0(28.2) 31.7(5.8) 0.875
;IDL> print, coef[51].edge
;     0.000000      1.00600      4.93900      31.8130      10000.0
	mu = [[xsec(E,48.,'pe',/cm2)*186.6],[xsec(E,52.,'pe',/cm2)*211.9]] / $
		(186.6+211.9)/5.85
	w48 = [53.2, 100., 27.3, 5.3]
	w48 = w48 / total(w48)
	w52 = [53.7, 100., 28.2, 5.8]
	w52 = w52 / total(w52)
	mu1 = mu[*,0] + mu[*,1] ;add the xsections together
	w   = [[(0.84*mu[*,0]/mu1)#w48],[(0.875*mu[*,1]/mu1)#w52]]/(0.84+.875) ;probability for each fluor line
	;for E gt Edge
	edge= [fltarr(4)+27.94,fltarr(4)+31.813]
	ke  = [22.9841, 23.1736, 26.09, 26.684,27.2, 27.47, 31.0, 31.7]
	mu0 = 	(xsec(ke,48.,'pe',/cm2)*186.6+xsec(ke,52.,'pe',/cm2)*211.9) $
		/(186.6+211.9)/5.85

endif
if dt eq 'SXS' then begin

	;use the SXS gas volume parameters, not a general Xe detector
	;90% Xe and 10% CO2
	mu = [[xsec(E,54.,'pe',/cm2)*0.9*131.3], $
		[xsec(E,6.,'pe',/cm2)*0.1*12.],$
		[xsec(E,8.,'pe',/cm2)*0.1*32.]]
	mu1 = mu[*,0] + mu[*,1] + mu[*,2]
	w   = [0.90*mu[*,0]]/mu1
	edge= [34.58]
	ke  = [29.6]
	mu0 = xsec(ke,54.,'pe',/cm2)*0.9*131.3 + $
	      xsec(ke,6.,'pe',/cm2)*0.1*12.+ xsec(ke,8.,'pe',/cm2)*0.1*32.
endif

kp = fltarr(num,n_elements(KE)) ;fluor. energies by incid. energy

for i=0, n_elements(KE)-1 do begin
	;mu1 is total xsec at incident E
	;mu0 is total xsec at each ke
	ratio = (mu1/costhet) / mu0[i]
	kp[*,i] = w[*,i]/2. * (1. - (1./ratio[*])*alog(1.+ratio[*]))
	;CAN'T HAVE FLUORESCENCE, IF E LESS THAN K SHELL
	wfl = where(E lt edge[i],count)
	if count gt 0 then	kp[wfl,i] = 0.0
endfor

END
