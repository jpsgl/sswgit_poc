;+
;Name: Mod_rate_density
;
; Lazy man's averaging over a sine curve
;
; Arate - Average count rate
; Modamp - ranges 0-1, depth of modulation, 1 is complete
;	modulation, 0, is none
;
;-
function mod_rate_density, arate, modamp, delta, binning=binning

;delta is a fraction less than one
;break cosine modulation function up into 1/delta*100 bins
modamp0 = modamp >0.01<1.0
default, binning, 100
rdelta = 1/delta * 100
x = findgen(rdelta)/(rdelta+1) + 1./2./rdelta
y = (1.-modamp0/2.) + modamp0/2.*cos(x*2*!pi)
hbins =1./delta*binning
h = histogram(y/delta*binning,min=0,max=hbins)

avgx = total(h*findgen(hbins))/total(h)/hbins

peakrate = arate/avgx

idlta= 1./delta
xh = findgen(hbins)
rate = fltarr(idlta)
ix = 0
for i=0,idlta-1 do begin
	ix = lindgen(binning) + i*binning
	rate[i]=f_div(total(h[ix]*xh[ix]),total(h[ix]))/hbins * peakrate
	endfor

return, rate
end
;+
;Name: Mod_Pilecoef
;
; Adjust pileup coefficients based on modulation
; depth of count rate.
; For RHESSI, MODAMP  should be close to 1 for
; the coarse grids and 0.1 for the finest.
;
;
;
;-



pro mod_pilecoef, r, tc, pc, norder, modamp


rate = mod_rate_density( r, modamp, .01, binning=100)

z = where(rate gt 0, nz)

rate = rate[z]

pc = fltarr(norder,nz)

for i=0,nz-1 do begin
	pilecoef, rate[i], tc, pci, norder
	pc[0,i] = pci
	endfor
pc = nz gt 1 ? avg(pc,1):pc
end

;+
; Name:
;	Pileup_countrate_Build_Piler
;
; Purpose:
;	This function convolves a variable amplitute gaussian
;	with a pulse-height distribution simulating the pulse-height
;	distribution of the second photon in a pileup event
;
; Restrictions:
;	Not to be used stand-alone.  To be used only with Pileup_Countrate
;
;-

function Pileup_countrate_build_piler, eff, ecrt, crt, $
	sigma = sigma, $
	crtmin=crtmin, maxeff=maxeff
;ecrt starts at 0.0

edge_products, ecrt, edges_1 = ecrt1, mean=ecrtm

default, crtmin, .01
default, maxeff, 1.0
default, eff, 1.0
default, sigma, 1.0
sigma = sigma > 0.1

eff = eff < maxeff ;must be less than 1.0
piler = crt > 0
;if eff lt 1.0 then begin ;shrink piler crt
	piler = interpol( piler, ecrtm*eff, ecrtm ) / eff ;divide to preserve counts
	;endif
sigma = sigma < 10.0
if sigma gt 0.2 then begin
	nxx = long(10*sigma)/2 * 8 + 1
	xx  = findgen(nxx)/10. - (nxx-1)/20.
	kernel = gaussian(xx, [1,0,sigma])

	mx = max(crt,n)
	;locate spectral point 3 orders of magnitude below peak at higher energy
;	umx= where( crt[n:*] lt mx/1000., numx)
;	if numx ge 1 then begin &$
;		spiler= piler[n+umx[0]+1:*] &$
;		piler = piler[0:n+umx[0]] & endif

	opiler  = convol(piler, kernel, /norm, /edge_zero,/center)

	opiler[n:*] = opiler[n:*] < crt[n:*]
	opiler  = total(crt)/total(opiler)*opiler
;	if numx gt 1 then opiler = [opiler, spiler]
if 0 then begin
	plot, ecrtm,piler>10, xran=[0,10]
	oplot, ecrtm, crt,col=5
	oplot, ecrtm, opiler, col=9
	kernel[nxx/2:*]=1
	kernel2 = kernel
	opiler2  = convol(piler, kernel2, /norm, /edge_zero,/center)
	oplot, ecrtm, kernel	*1e5, col=9
	oplot, ecrtm, kernel2 * 1e5, col=7
	oplot, ecrtm, opiler2, col=7
	endif
endif else opiler=piler

;;;;;;;;;;;;

return, opiler
end
;+
; NAME:
;       Pileup_countrate_PULSE_CONVOLVE
;
; Purpose:
;		Convolve piler pulse height with base pulse height
;
; Restrictions:
;		To be used with pileup_countrate.  Not to be
;		used standalone without the user totally understanding the
;		actions of the CONVOL function
;-

function Pileup_countrate_pulse_convolve,  pilee, piler
;piler and plilee both extend to 0 and have the same energy binning
base = [pilee*0.0, pilee]
npulse = n_elements(pilee)
return, (convol( base, piler, center=0, /norm))[npulse:*]
end

;+
;
; NAME:
;       PILEUP_COUNTRATE
;
; PURPOSE:
;	This procedure computes the countrate after pileup
;
; CATEGORY:
;       XRAY
;
; CALLING SEQUENCE:
; 	pcountrate = pilup_countrate( truecountrate, ct_edges,$
;	 prob_gain, pilerfrac [,tau=tau] [,norder=norder])
;
;
; CALLS:
;	none
;
; INPUTS:
;
;	truecountrate - true countrate spectrum in cts/sec/bin, incident on detector
;	ct_edges  - energy bin edges, either 1-d or 2xN formulation
;   Parameters of pilup model in param_str
	;	param_str = {pileup_str,
	;	Tau0	  : 1.0,  $;in usec
	;	sigma     : 1.0,  $;convolution of piler in keV, sigma
	;	pilerfrac : 0.999, $;average fractional energy of piled up photon
	;	Ecut      : 10.0, $  ;upper limit of second region
	;	Ratio     : 3.0,$;effectiveness ratio for pileup for energy loss less than Ecut
	;	offset    : 0.3,$;positive energy offset, gain related
	;	modfrac   : 0.5 } ;sine-modulated fraction of flux


; KEYWORDS:
;
;
; OPTIONAL INPUTS:
;	none
;
; OUTPUTS:
;       Returns pileup countrate
;
; OPTIONAL OUTPUTS:
;	none
;

;	none
; COMMON BLOCKS:
;	none
;
; SIDE EFFECTS:
;	none
;
; RESTRICTIONS:
;	none
;
; PROCEDURE:
;	This routine implements a simple model of pileup on a countrate spectrum in the
;	forward direction.  Takes an input countrate and detector electronic info and
;	computes the probability of multiple orders of pileup.  Rapidly creates multiple orders
;	of pileup through successive convolutions of the piler function profile.  Piler is computed by
;	convolving a gaussian with the pulseheight distribution, where the gaussian is at an energy
;	fraction given by pilerfrac.  Poisson probability function is used to compute the probability of
;	the various orders of pileup.  After the convolutions with the piler, the various waveforms are
;	weighted with their probability and the whole is summed for the final piled up countrate spectrum or
;	pulse-height distribution
;
; MODIFICATION HISTORY:
;	24-oct-2007, ras
;	5-oct-2006, ras
;	1-may-2008, ras, protect tau2 from being undefined, either 0 or defined
;_
function pileup_countrate, $
	truecountrate, $
	ct_edges, $
	param_str, $
	out   = out, $
	pdist = pdist, $
	e10 = e10


;
edge_products, ct_edges, edges_1= edges1, width=ewidth, mean=edgm
tau      = param_str.tau0 * 1e-6
sigma    = param_str.sigma
pilerfrac= param_str.pilerfrac
etau     = param_str.ecut
;ratio - effectiveness ratio for pileup for energy loss less than Ecut
if etau gt edges1[0] then tau2 = tau * param_str.ratio
tau2     = etau gt edges1[0] ? tau * param_str.ratio :0.0

offset   = param_str.offset ;0.3 positive energy offset, gain related
modfrac  = param_str.modfrac ; sine modulated fraction of flux, 0-1.
default, norder, 5



crate  = total( truecountrate ) ;number of counts/sec

if crate eq 0.0 then return, truecountrate
cspec  = truecountrate / ewidth
;limits on rebinned spectrum to use in convolution
elim = [0.0, last_item(edges1)] ; spectrum must be extended to 0 to
;work with build_piler and pulse_convolve
;rebin onto 0.1 keV bins
de    = (elim[1]- elim[0])
e10   = interpol( elim, 10*de+1.0)
e10m  = get_edges(/mean, e10)
crt10 = interpol( cspec, edgm+offset, e10m)
;
crt10[0:19] = 0.0 ;terminate 0-2 keV, force flux to zero
;modify crt10, count rate in .1 keV bins to give greater weight to region of higher pileup prob
tau_eff = tau

;Compute norder
pilecoef, crate*1.d0, tau_eff, pc, 50
test_order = where(pc ge 1e-5, norder)
norder = norder > 2

if tau2 gt 0 then begin
	itau2 = value_locate( e10m, etau)
	piler = crt10
	piler[0:itau2] = piler[0:itau2] * tau2 / tau
	tau_eff = total(piler)/total(crt10) * tau
	endif

default, piler, crt10
;create the piler function, convolution of a gaussian with pulseheight distro
;piler = Pileup_countrate_build_piler( pilerfrac, e10, crt10, crtmin=0.02, maxeff=maxeff, sigma=sigma, pdist=pdist)
piler = Pileup_countrate_build_piler( pilerfrac, e10, piler, crtmin=0.02, maxeff=maxeff, sigma=sigma)

;make different orders of pileup, by reconvolving with piler
piled = reproduce( crt10, norder)

for i=1,norder-1 do piled[0,i] = Pileup_countrate_pulse_convolve( piled[*,i-1], piler)
mod_pilecoef, crate, tau_eff, pc, norder, modfrac
pc = transpose(reproduce( pc, n_elements(piler)))
crtp10 = total( pc * piled, 2)

ssw_rebinner, crtp10*.1, e10, result,edges1
if arg_present(out) then begin
	out = fltarr(n_elements(edges1)-1,norder)
	for i=1,norder-1 do begin
		ssw_rebinner, piled[*,i]*.1, e10, temp, edges1
		out[0,i]=pc[0,i]*temp
		endfor

	endif


return, result
end

