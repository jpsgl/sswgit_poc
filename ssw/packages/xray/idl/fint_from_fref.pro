;+
; PROJECT:
;	HESSI
;
; NAME:
;	Fint_from_Fref
;
; PURPOSE:
;	This function computes the integrated electron flux, fint (electrons per second) 
;    using the electron distribution function normalization (electrons per second per keV)
;    at a reference energy eeref. 
; The distribution function is assumed to be normalized to the electron spectral flux 
;    at reference energy eeref.  If eelow < eeref < eebrk, the normalization
;    is the electron spectral flux (electrons per sec per keV) at eeref. 
;    Outside this range, it is the flux of the extension of the lower
;    power-law (index p) to electron energy eeref.  If eebrk < eeref < eehigh,
;    the spectral flux at eeref is (eebrk/eeref)^(q-p) times the value of the 
;    normalization.  
;    
; CATEGORY:
;	HESSI, Spectra, Modeling
;
; CALLING SEQUENCE:
;	Fint_from_Fref( p, q, eelow, eebrk, eehigh, eeref, fref )
;
; CALLS:
;	none
;
; INPUTS:
;
;   p    - Power-law index of the electron distribution function below
;          eebrk. 
;
;   q    - Power-law index of the electron distribution function above
;          eebrk. (not used for rc function)
;
;   eelow      -   Low energy cut-off in keV. 
;
;   eebrk      -   Break energy in keV. (or for rc function, potential drop in kV)
;
;   eehigh      -   High energy cut-off in keV. 
;   
;   eeref - The reference energy at which fref is determined in keV
;   
;   fref  - The value of the normalization coeffecient at eeref (electrons per sec per keV)
;   
;
; OPTIONAL INPUTS:
;   func  - function name (i.e. 'thick2_vnorm', 'thick2_rc') (Required if func is thick2_rc)
;
; OUTPUTS:
;	Fint		-	The integrated electron flux (electrons per second)
;
; OPTIONAL OUTPUTS:
;	none
;
; KEYWORDS:
;	none
;
; COMMON BLOCKS:
;	none
;
; SIDE EFFECTS:
;
;
; RESTRICTIONS:
;
;
; PROCEDURE:
;   The integral of a double power law electron distribution function is computed.  The 
;   distribution function is normalized so that its value is fref at energy eeref if 
;   eelow < eeref < eebrk.  
;   p is the lower power-law index, between eelow and eebrk, and q is the
;   upper power-law index, between eebrk and eehigh.  The distribution function is integrated
;   from eelow to eehigh to obtain the total electron flux. 
;
;
; MODIFICATION HISTORY:
;   Version 1 of this subroutine, Gordon.D.Holman@nasa.gov, 26 May 2011
;   10-Jul-2014, Kim Tolbert. Added func keyword, and changes for thick2_rc function.   
;   14-Jul-2014, Kim. For thick2_rc, use fref for injected spectrum (calculated from get_rc_electron_vals) instead
;     of a[0] which is fref for evolved spectrum
;                                           
;-

Function Fint_from_Fref, fref, p, ebrk, q, elow, ehigh, eeref, func=func

checkvar, func, 'thick2_vnorm'

; Protect input args from changing
ffref = fref
eelow = elow
eebrk = ebrk
eehigh = ehigh

; For thick2_rc model, eebrk is really the potential drop, not the break energy
;   so add it to low and high energy.  And since it's really a single power-law, set break
;   energy to high energy.
if func eq 'thick2_rc' then begin
  ffref = get_rc_electron_vals(energy=energy, par=[fref,p,ebrk,q,elow,ehigh,eeref], /fref_inj)
  eelow = eelow + eebrk
  eehigh = eehigh + eebrk
  eebrk = eehigh
endif
            
If (eebrk gt eehigh) then eebrk = eehigh
If (eebrk lt eelow) then eebrk = eelow

; Normalize all energies to eeref

eln = eelow/eeref
ebn = eebrk/eeref
ehn = eehigh/eeref

fintn = ((eln^(1-p) - ebn^(1-p))/(p-1.d0)) + (ebn^(q-p))*(ebn^(1-q) - ehn^(1-q))/(q-1.d0)

fint = ffref * eeref * fintn

RETURN, fint

END