;+
; Name        : resp_calc_compton_scatter
;
; Purpose     :
; This function the probability of a compton_scatter from the photon_energy to the 
; channel_energy bin
;
; Category    : Spectroscopy
;
; Explanation : Integrates the klein-nishina cross-section for Compton Scatter through the output
;   channel_edges for each photon_energy and so builds up the probability density
;
; Use         : 
;
; Inputs      :
; channel_edges - output energy-loss bins in keV
; photon_energy - photon Energy in keV
; cmp_xray      - probability of Compton scatter for input photon energy in detector
;
;
; Opt. Inputs : None
;
; Outputs     : Function returns the channel integrated Compton cross-section
;
; Opt. Outputs: None
;
; Keywords    :
; Calls       :
;
;
; Common      : None
;
; Restrictions:
; Side effects: None.
;
; Prev. Hist  :
;
; Modified    :
; Version 1, ras 23-oct-2014, adapted from earlier versions of resp_calc
;
;-
function resp_calc_compton_scatter, channel_edges, photon_edges, cmp_xray

emin = get_edges( photon_edges, /gmean )
edge_products, channel_edges, gmean = emout, wid=wout, edges_1 = eout1d, edges_2 = eout
nflux = n_elements( emin )
nout  = n_elements( wout )
compton = fltarr( nout, nflux ) 
default, cmp_xray, fltarr( nflux ) + 1.0

max_electron_energy = compton_max_electron_energy( emin )
limited_max_energy  = max_electron_energy <  eout[ 1, nout - 1]
;Which channel(out) bin do these fall in
q_max = value_locate( eout1d, max_electron_energy ) < ( nout - 1)
valid = where( max_electron_energy gt eout1d[0], nvalid )
if nvalid ge 1 then for ii = valid[0], NFLUX - 1L do begin ;loop over input photon energies
  ;the centroid channel energy needs to be adjusted for the last
  ;bin because the central energy isn't given by the channel_edges but by the maximum
  ;scattered electron energy
  emout_i = emout
  wout_i  = wout
  if q_max[ii] ge 0 then begin
     emout_i[ q_max[ii] ] = sqrt( eout[0, q_max[ii]] * limited_max_energy[ii] )
     wout_i[ q_max[ii] ]  = max_electron_energy[ii] - eout[0, q_max[ii]] 
  endif
  ;                          PHOTON,ELECTRON energies
  ;Inside klein_elctrn we change variables for the differential cross-section from 
  ;per cos(scatt angle) to per scatter energy.  The chain rule derivative is used inside that function
  ;first compute the xsection at 100 energies from 0 to max_electron_energy
  energies = exp( interpol( [ alog(.1 ), alog( max_electron_energy[ ii ]*1.02) ], 101 ) )
  edge_products, energies, width = wenergies, gmean = emnergies 
  dsk_base = klein_elctrn( emin[ii], emnergies )
  tot = transpose( wenergies ) # dsk_base
  ;dsk = klein_elctrn( emin[ii], emout_i, wmu = wmu )
  dsk    = fltarr( nout )
  dsk[0] = interpol( dsk_base, emnergies, emout_i[0:q_max[ii]] )

  ;                          PHOTON,ELECTRON energies
  
  ;if wmu[0] eq -1 then goto, NOVALID ; energy too low for Compton
  ;diff. xs * delta Kinetic /total xs * Prob_of_compton
  dsk = (dsk/tot[0]) * wout_i * cmp_xray[ii]  ;changed win to wout, ras, 16-oct-2014
  ;help, i
  compton[0, ii] = dsk  
endfor

return, compton
end