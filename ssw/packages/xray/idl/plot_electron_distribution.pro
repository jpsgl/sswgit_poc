;+
; Primary routine, plot_electron_distribution, is below.
; This routine plot_electron_distribution_addons is called to add features to the electron distribution plot.
; Don't call plot_electron_distribution_addons directly.
; It is called in the xyplot object via the addplot_name and addplot_arg mechanism in xyplot.
; Features added:
;  low power law extended
;  high power law extended
;  marks at low energy cutoff, break energy, high energy cutoff, and reference energy
;  legend
;-
;

pro plot_electron_distribution_addons, energy=energy, par=par, func=func, edf=edf, fint=fint, color=color

  stat = check_func_electron(func, vnorm=vnorm, single_pow=single_pow, athin=athin, rc=rc)
  
  p = par[1]     ; index of elec. distr. below ebrk
  q = par[3]     ; index of elec. distr. above ebrk
  
  ; all in keV
  elo = par[4]  ; low energy cutoff
  ebrk = par[2] ; break energy
  ehi = par[5]  ; high energy cutoff
  
  eref = vnorm ? par[6] : 50.
  
  ; Calculate values on edf curve at low, break, high energies. Don't use edf=0 values below elo
  ; for interpolation.
  ee = [elo, ebrk, ehi]   ; low, break, high
  qgood = where(edf gt 0, count)
  zee = 10.^interpol(alog10(edf[qgood]), alog10(energy[qgood]), alog10(ee)) > 0.
  
  ; if return current function, then oplot the return current electron spectrum 
  if rc then begin
    frc = get_rc_electron_vals(energy, par)
    rccol = color[3]
    oplot, energy, frc, col=rccol
    ; get values of flux from evolved spectrum at elo, ebrk, ehi (won't use the ebrk one)
    zee = get_rc_electron_vals(ee, par) 
  endif else rccol = 0

  ; if function is normalized at a ref energy, add ref energy and normalization value to ee and zee arrays
  if vnorm then begin
    ee = [ee, eref]
    zee = [zee, par[0]]
  endif
    
  ; Calculate single power law with low delta. Normalization and ref energy are first non-zero point of edf 
  pow1 = f_1pow(energy, [ edf[qgood[0]], p, energy[qgood[0]] ])
  pow1col = color[2]
    
  pthick = !p.thick > 1.
  if !d.name eq 'PS' then pthick = pthick*3
  
  ; Overplot edf thicker, then oplot the two separate power laws
  oplot, energy, edf, thick=pthick*3
  oplot, energy, pow1, col=pow1col, lines=2, psym=0, thick=pthick
  if single_pow then begin
    zee[1] = !values.f_nan  ; don't want to put symbol for break energy for single power-law
    pow2col = 0
  endif else begin
    ; Calculate second single power law with high delta. Normalization is value of edf at break energy and
    ; ref energy is value of break energy.
    pow2 = f_1pow(energy, [zee[1], q, ebrk])
    pow2col = color[3]
    oplot, energy, pow2, col=pow2col, lines=2, psym=0, thick=pthick     
  endelse 
  
  ; Draw symbols for low, break, high, ref energies (if available)
  ppsym = [4,2,7,6]  ; diamond,asterisk, X, square
  symsize = [2.,2.,2.,2.5]
  zcol = [color[0], color[0], color[0], color[1]] ; red, red, red, green
  for i=0,n_elements(ee)-1 do plots, ee[i], zee[i], color=zcol[i], psym=ppsym[i], symsize=symsize[i], thick=pthick
  
  g93 = '(g9.3)'
  expon = athin ? '55' : '35'  ; for thin functions, unit is 1.e55, for thick 1.e35
  unit = 10.D^fix(expon)

  ; Set up legend for all options, and then remove below if not needed
  leg = ['Integrated Electron Flux ' + trim(unit*fint, g93) + ' electrons s!u-1!n', $
         'Potential Drop from Injection Point to Thick Target ' + trim(ebrk, g93) + ' kV', $
         'Flux units: 10!u' + expon + '!n electrons s!u-1!n keV!u-1!n', $
         'Injected Electron Spectrum', $
         'Evolved Spectrum due to RC Losses', $ 
         'Low Delta ' + trim(p, g93) + ' Extended', $
         'High Delta ' + trim(q, g93) + ' Extended', $
         'Low Energy Cutoff ' + trim(elo, g93) + ' keV, Flux ' + trim(zee[0],g93), $
         'Break Energy ' + trim(ebrk, g93) + ' keV, Flux ' + trim(zee[1],g93), $
         'High Energy Cutoff ' + trim(ehi, g93) + ' keV, Flux ' + trim(zee[2],g93), $
         'Normalization Flux = ' + trim(par[0],g93) + ' at ' + trim(eref, g93) + ' keV' ]         
  
  bw = !d.name eq 'PS' ? 0 : 255 ; black or white for main trace legend depends on PS
  linest = [-99, -99, -99, 0, 0, 2, 2, 1, 1, 1, 1]
  sym = [0, 0, 0, 0, 0, 0, 0, ppsym]
  lcolor = [255, 255, 255, bw, rccol, pow1col, pow2col, zcol]
  
  ; Remove unneeded items from legend params for functions with no ref energy or single power-law
  ; Do this in reverse order, so index to remove doesn't change as we remove!
  if ~vnorm then remove, 10, leg, linest, sym, lcolor
  if single_pow then remove, [6,8], leg, linest, sym, lcolor
  if ~rc then remove, [1,4], leg, linest, sym, lcolor
  
  ssw_legend, leg, linest=linest, psym=sym, /left, /bottom, box=0, color=lcolor, thick=pthick
  
end

;+
; Name: plot_electron_distribution
; 
; Purpose: Plot the electron distribution (also called electron spectrum) for thick or thin 
;   target models
; 
; Input keywords:
;   energy - array of energies in keV to calculate distribution for. N mean energies or 2xN edges. Default is 100 logarithmically 
;         spaced values from 1 to 300 keV.
;   par - array of model parameters (see f_thick... and f_thin... routines for description). Default is default 
;         values for thick2_vnorm function
;   func - Model name (e.g. 'thick2_rc')  Default is 'thick2_vnorm'
;   no_plotman - if set, plot in simple plot window.  Otherwise plot in plotman interactive window. Default is 0.
;   interval - fit interval number (for plotman window description), optional
;   _extra - additional plot parameters to pass to plot routine
;   
; Input/Output Keywords:
;   plotman_obj - if passed in will use that plotman instance.  If not, will create one and pass it out.
;   
; Examples:
;   plot_electron_distribution  ; uses all of the defaults
;   plot_electron_distribution, energy, par, func='thick2_vnorm'
; 
; Written: Kim Tolbert, 26-Jun-2014
; Modifications: 8-Jul-2014, Kim. Added features to overplot via plot_electron_distribution_addons routine above.
; 
;-

pro plot_electron_distribution, energy=energy, par=par, func=func, $
  plotman_obj=plotman_obj, no_plotman=no_plotman, $
  interval=interval, times=times, _extra=_extra

checkvar, energy, make_xarr(xlo=1., xhi=300., nx=100, /log)
checkvar, func, 'thick2_vnorm'
checkvar, par, [1.,4.,150.,6.,20.,3200.,50.]

no_plotman = keyword_set(no_plotman)

euse = energy ; protect input arg
if size(euse, /n_dim) eq 2 then euse = get_edges(euse, /mean)
; modify energy array to add a point at the break energy so we won't interpolate across it
euse = [euse, par[2]]
euse = euse[sort(euse)]

edf = calc_electron_distribution(energy=euse, par=par, func=func, fint=fint)
if edf[0] eq -1 then return

stat = check_func_electron(func, vnorm=vnorm, single_pow=single_pow, athin=athin)

expon = athin ? '55' : '35'

xy_obj = obj_new('xyplot', euse, edf)
id = 'Electron Spectrum'
xtitle = 'Energy (keV)
ytitle = '10!u' + expon + '!n electrons s!u-1!n keV!u-1!n'
xy_obj->set, id=id, data_unit=ytitle, /xlog, /ylog
desc = id + (exist(interval) ? ' Interval ' + trim(interval) : '') + (exist(func) ? ' '+func : '')
if exist(times) then label = format_intervals(times,/ut) 
flabel = exist(func) ? 'Function: ' + func + ' ' : ''
label = append_arr(label,flabel)
;label = label + arr2str(trim(par, '(g9.3)'), ',')
legend_loc = 2 

; Plot either in plotman or not. In either case, set up name of routine and structure with arguments for 
; the plot_electron_distribution_addons routine which will be called in the xyplot object
; Only the color settings are different for plotman or non-plotman.
addplot_name = 'plot_electron_distribution_addons'
addplot_arg = {energy:euse, par:par, func: func, edf:edf, fint:fint, color: intarr(4)} 

if no_plotman then begin
  linecolors
  addplot_arg.color = [2,7,9,12]  ; red, green, cyan, magenta
  xy_obj->set, addplot_name=addplot_name, addplot_arg=addplot_arg
  xy_obj->plot, xtitle=xtitle, label=label, legend_loc=legend_loc, thick=3, psym=0, _extra=_extra
endif else begin
  addplot_arg.color = [243,248,250,253]  ; red, green, cyan, magenta 
  xy_obj->set, addplot_name=addplot_name, addplot_arg=addplot_arg
  xy_obj->plotman, plotman_obj=plotman_obj, xtitle=xtitle, /replace, nodup=0, desc=desc, $
    label=label, legend_loc=legend_loc, psym=0, _extra=_extra  
endelse
destroy,xy_obj

end