;+
;
; NAME: f_vth_thick2
; Flux model of a thermal bremsstrahlung plus thick-target broken
; power-law electron distribution function (GHolman)
;
; PURPOSE:
; This function returns the differential photon spectrum seen at the Earth
; for a two component model comprised of a thermal
; bremsstrahlung function plus a thick-target,
; non-thermal, broken power-law function with low- and high-energy cutoffs.
;
; CATEGORY:
;       SPECTRA, XRAYS
;
; CALLING SEQUENCE:
;       Flux = F_VTH_THICK2( E, A )
;
; CALLS:
;       F_VTH, F_BPOW, EDGE_PRODUCTS, CHECKVAR
;
; INPUTS:
; E -energy vector in keV, 2XN edges or N mean energies
; A -model parameters defined below
; For the thermal function using F_VTH.PRO:
;   a(0)= emission measure in units of 10^49 cm-3
;   a(1)= KT   plasma temperature in keV
;   a(2) - Total integrated electron flux, in units of 10^35 electrons sec^-1.
;   a(3) - Power-law index of the electron distribution function below
;          a(4) (=eebrk).
;   a(4) - Break energy in the electron distribution function (in keV)
;   a(5) - Power-law index of the electron distribution function above
;          eebrk.
;   a(6) - Low-energy cutoff in the electron distribution function
;          (in keV).
;   a(7) - High-energy cutoff in the electron distribution function (in keV).
;
; COMMON BLOCKS:
; FUNCTION_COM
;
; HISTORY:
; 19-May-2009, Kim.  Copied from f_vth_thick.  Calls parallel but much faster routines
;   rooted at brm2_thicktarget instead of brm_bremthick (modifications for speed by Yang Su)
; 08-Jul-2011, Kim.  Added independent_norm keyword for use with thick2_vnorm function
; 08-Feb-2013, Kim. Use _extra instead of spelling out keywords that are passed through (currently 
;  independent_norm and now return_current) to brm2_thicktarget
;-
function f_vth_thick2, e, a, no_vth=no_vth, _extra=_extra

@function_com
;FUNCTION_COM - Common and common source for information on fitting functions
;
;common function_com, f_model, Epivot, a_cutoff

if (size(e))(0) eq 2 then edge_products, e, mean=em else em=e

if keyword_set(no_vth) then begin
    maxpar = 6
    offset = 0
endif else begin
    maxpar = 8
    offset = 2
endelse

checkvar,apar,fltarr(8)

npar = n_elements(a)
apar(0) = a(0: (npar-1)< (n_elements(apar)-1) )
if total(abs(apar(4+offset:5+offset))) eq 0.0 then apar(4+offset)=a_cutoff

;spectral index 1.0 will cause NaN flux
if (apar[1+offset] eq 1.0) then apar[1+offset] = 1.01
if (apar[3+offset] eq 1.0) then apar[3+offset] = 1.01

if apar[0+offset] eq 0. then ans = fltarr(n_elements(em)) else $
  ans = 1.0e35 * brm2_thicktarget(em,apar(0+offset:5+offset), _extra=_extra) * apar[0+offset]

if not keyword_set(no_vth) then ans = ans + f_vth(e, apar(0:1))

;If output is infinity or nan, set them to small number
NaNInd = where((finite(ans, /infinity) eq 1) or (finite(ans, /nan) eq 1))
if (NaNInd[0] ge 0) then ans[NaNInd] = 1.0e-27

return,ans
end





