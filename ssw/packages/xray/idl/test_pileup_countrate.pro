;test pileup_countrate

norder = 6

use_vird = bytarr(18)
use_vird[3]=1b
hsi_rd_ct_edges, 14,ct_edges
ct_edges = [1.+findgen(70)/3, ct_edges[22:*]]
hessi_build_srm, ct_edges, use_vird, srm, area, /sep_det,/sep_vird, atten_state=0, $
	time_wanted='20-feb-02 11:10:00',/pha_on_row, all_simplify=0

ph_edges=ct_edges
edge_products, ph_edges, edges_2=phedg2, width=dph
tcspec = area*srm#( f_vth(phedg2, [1.0, 2.]) * dph)
dct = get_edges(ct_edges, /width)
tcrt = tcspec * dct
cedgm = get_edges( ct_edges,/mean)
plot, cedgm, tcrt/dct, xrang=[0,30], psym=10
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;These are the two free parameters for the pileup_mod function
prob_gain = 1.0
pilerfrac = 0.8
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;tcrt - is the predicted count rate in counts/sec, we don't need
;the counter livetime.  PCRT is the output of apply_drm
pcrt = pileup_countrate( tcrt, ct_edges, prob_gain, pilerfrac, tau=1.0e-6, $
	piled0=tcrt0, norder=norder, sigma=sigma)

linecolors
oplot, cedgm, pcrt/dct, thick=2, psy=10
;oplot, cedgm, tcrt0/dct, col=7, psy=10


end