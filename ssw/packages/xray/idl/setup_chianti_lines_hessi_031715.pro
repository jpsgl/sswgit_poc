;+
;Name: Setup_Chianti_Lines_Hessi
;
;Purpose:
;	This main program is the setup file for the chianti lines database file
;
;History:
;	2000, richard.schwartz@nasa.gov
;	30-aug-2012, automated using the chianti version in the filename and documented
;	17-mar-2015, richard.schwartz@nasa.gov - raise temp to 10^9 Kelvin
;		kmax set to 12.0 keV, ioneq set to 'CHIANTI_V7', ntemp changed to 750
;-
kmin=1.0 ;minimum energy in keV for lines
;kmax=10.0 ;maximum energy in keV for lines
kmax = 12.0 ;maximum energy in keV for lines
;temp_range = [1., 100.]*1e6 ;range of valid temperatures
temp_range = [1.0, 1000.0]*1e6 ;range of valid temperatures
;ntemp = 500 ; number of valid temperatures
ntemp = 750 ; number of valid temperatures
;ntemp = 50 ; for test
;ioneq = 'MAZZOTTA'
ioneq = 'CHIANTI'
nelem = 15
savfile='chianti_lines_1_10_v'+strtrim(fix(float(chianti_version())*10),2)+'.sav'
;abundance determined after the fact
fileout = setup_chianti_lines(ioneq,  kmin,kmax,$
    temp_range, ntemp=ntemp, nelem=nelem, $
    savfile=savfile)
    ;savfile='chianti_lines_3_10_v50_test.sav')


end

