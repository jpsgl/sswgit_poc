;; NAME:
; 		F_MULTI_THERM_GAUSS
;
; PURPOSE:
; This function returns the photon spectrum at the Earth, d(Flux)(eph)/dt,
; from a differential emission measure distribution which has a Gaussian dependence on temperature.
; 
;
; CATEGORY:
;       SPECTRA, XRAYS
;
; CALLING SEQUENCE:
;       Flux = f_multi_therm_gauss(eph, apara)
;
; CALLS:
;       f_vth.pro
;
; INPUTS:
;       eph -      array of photon energy bands in
;                  keV. This is a (2xN) array.
;       apar -    parameter array which should consist of:
;                  a[0] = emission measure (in 10^49 cm-3) at the maximum
;                  point in the Gaussian DEM distribution, i.e. at tmax
;                  a[1] = the temperature where the peak of the
;                  Gaussian DEM distribution is located, in units of
;                  log T (K). e.g. a[1] = 7.0 for 10 MK.
;                  a[2] = The width of the DEM Gaussian distribution
;                  in log T (K) units. e.g. a[2] = 0.3.
;
; WRITTEN: Andrew Inglis, 2012/07/11
;
; HISTORY: Andrew Inglis, 2012/07/23 - tidied up code and documentation
;          Andrew Inglis, 2012/07/31 - fixed error where wrong value
;                                      of emission measure was input
;                                      to f_vth.
;	   Andrew Inglis, 2012/08/13 - bug fix. The gaussian envelope is now calculated
;				       over the range 0.086 keV - 8.6 keV, the same range
;				       of valid inputs to f_vth.
;
;
function f_multi_therm_gauss, eph, apar, _extra = _extra
;
;
emission=apar[0]
tmax=apar[1]
sigma=apar[2]

;temperature input into f_vth is valid only between 0.086 and 8.6 keV. Only search for EM contributions within this range
 
;generates an array of temperatures between log T (K) = 6 - 8 (0.086 - 8.6 keV). The Gaussian envelope will be calculated over this range.
t=findgen(200)*0.01 + 6.0

;calculate the gaussian envelope for the DEM distribution within the range log T (K) = 6 - 8.
gauss= exp(-(t-tmax)^2/((2.*sigma)^2))

;input into f_vth needs to be in keV units. Convert here.
t_keV=(10^t)/1e6 * 0.08617

;call f_vth to get brehmsstrahlung profile for a temperature T
;repeat for all Ts, but multiply by the Gaussian DEM profile
;so that Ts further away from Tmax contribute less 

flux = f_vth(eph, [emission, 0., 1], $
   multi_temp=t_kev, _extra=_extra); 

fsize=size(flux)
flux_tot=fltarr(fsize[1])
flux_tot[*]=0.

; multiply flux results by the gaussian envelope
for i=0, fsize[2]-1 do begin

   flux[*,i]=flux[*,i] * gauss[i]
   q=finite(flux[*,i])       
   index=where (q gt 0,count,complement=index_c)
   IF (n_elements(index_c) gt 1) THEN BEGIN
   flux[index_c,i]=0.
   ENDIF
   flux_tot[*]=flux_tot + flux[*,i]

endfor
;stop
return, flux_tot
end
