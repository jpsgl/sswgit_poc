
pro xlinflx,Te6,wave,Flux,Line,Trans,          $
       photon=photon,erg=erg,wave_range=wave_range,  $
       rel_abun=rel_abun,           $
       elem=elem,abun=abun,cosmic=cosmic,       $
       file_in=file_in, qstop=qstop
;+
;  NAME:
;   xlinflx
;  PURPOSE:
;   Compute the Mewe line spectrum for EM=1.e44 cm^-3
;  CALLING SEQUENCE:
;    xlinflx,Te6,wave,Flux               ; ph s-1
;    xlinflx,Te6,wave,Flux,/photon       ; ph s-1
;    xlinflx,Te6,wave,Flux,/erg          ; erg s-1
;    xlinflx,Te6,wave,Flux,wave_range=wave_range
;    xlinflx,Te6,wave,Flux,/cosmic  ; Use cosmic abudances
;    xlinflx,Te6,wave,Flux,Line,Trans   ; Return line information
;    xlinflx,Te6,wave,Flux,rel_abun = [26, 0.8] ;Fe abundance 80% of nominal.
;
;  INPUTS:
;    Te6        = Electron Temperature in MK
;  OUTPUTS:
;    wave       = Wavelengths of lines
;    Flux       = Fluxes of lines.  If Te6 is
;                 a vector, then Flux = fltarr(N_elements(Te6),N_elements(wave))
;  OPTIONAL INPUT KEYWORDS:
;    photon     = If set, calculation is made in ph s-1 (default)
;    erg        = If set, calculation is made in erg s-1
;    wave_range = A 2-element vector with the desired lower and upper wavelength
;                 limits (Ang).  For example, wave_range = [2.,60.] will
;                 calculate only those lines between 2 and 60 A.
;    cosmic = If set, read the cosmic abundance file
;    file_in    = To explicitly specify the Mewe line-list file.
;    rel_abun      = A 2XN array, where the first index gives the atomic number
;     of the element and the second gives its relative abundance
;     to its nominal value given by ABUN.
;  OPTIONAL OUTPUTS:
;    Line   = Character string with ion information
;    Trans  = Character string with transition informaiton
;  OPTIONAL OUTPUT KEYWORDS:
;    Abun   = Abundances used for calculation
;    elem   = Elements corresponding to the abundances
;
;  METHOD:
;    Reads $DIR_SXT_SENSITIVE/mewe_solar.genx
;       or $DIR_SXT_SENSITIVE/mewe_cosmic.genx if /cosmic switch is specified.
;
;    Note:      If Line argument is present, then picklambda is not called
;           to sum up lines at the same wavelength
;
;  MODIFICATION HISTORY:
;    29-oct-92, Written, J. R. Lemen, LPARL
;    25-jan-93, JRL  -- change call to picklambda
;    25-feb-93, JRL  -- Mewe file converted to genx file.
;     8-apr-93, JRL  -- Added optional output parameter
;     7-jul-93, JRL  -- Added file_in keyword
;    18-may-94, JRL  -- Fixed up check to prevent unnecessary file reads.
;    21-jun-95, JRL  -- Minor change to make 171A line calculation work with
;      with the SPEX85 data files.  And change to be able
;      work with the SPEX95 data files.
;      Changed the units to return in terms of 1.e44
;    20-aug-1997, richard.schwartz@gsfc, switched spl_interp from spline to
;   increase speed by a factor of 4 on a call with T a scalar.  Unsure
;   when spl_interp was defined so protect version lt 4.0 with spline.
;   Saved spl_init setup in new structure in common, mewe_spl.
;   For single temperature input identify valid transitions both on
;   wavelength range and temperature range of ion.  Identify prior
;   to loop for additional 30% increase.  New version takes 15% of time
;   old version for single temperature input at 10 MegaK for wavelengths
;   shorter than 12.4 Angstroms on a Digital Unix. Also REL_ABUN
;   keyword allows changes in abundances from nominal.  Information on
;   abundances used after calculation of remaining lines, so this isn't
;   the way to code this to run for a single ion.  Also, noted that
;   Argon is encoded as AR for a species but as A for an elem and
;   called that to the attention of the parties responsible under SSW.
;    12-sep-1997, richard.schwartz@gsfc, fixed element sorting bug with use of rel_abun.
;    10-nov-1998, richard.schwartz@gsfc, use DIR_GEN_SPECTRA, changed to xlinflx from linflx.
;    1-dec-1998, richard.schwartz added ".genx" to loc_file call. Proc won't work without.
;    19-jul-2003, richard.schwartz, changed common block name to avoid conflict with linflx
;    29-aug-2003, richard schwartz, use reform instead of transpose to make more robust
;   Changed most prints to message,/info.  Turn off messages with !quiet set to 1.
;    19-aug-2004, kim.tolbert@gsfc.nasa.gov.  Fix of 29-aug needed tweak - if one element
;      still needs to be array, not scalar for reform to work.
;-

if keyword_set(cosmic) then begin
        mewe_file = loc_file(path=['DIR_GEN_SPECTRA','DIR_SXT_SENSITIVE'],'mewe_cosmic.genx')
endif else begin
        mewe_file = loc_file(path=['DIR_GEN_SPECTRA','DIR_SXT_SENSITIVE'],'mewe_solar.genx')
        cosmic = 0
endelse

if n_elements(file_in) ne 0 then begin
    mewe_file = file_in
    cosmic = 8
endif

; With no calling arguments, display the calling sequence:

if n_params() eq 0 then begin
  doc_library,'xlinflx'
  return
endif

if keyword_set(photon) and keyword_set(erg) then begin
  print,' **** Error in xlinflx ****',string(7b),string(7b)
  print,'      You cannot specify both /photon and /erg'
  return
endif else if keyword_set(erg) then Units=1 else Units=0

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; Keep the Mewe data in a common block
; mewe_nval = Vector containing number of non-zero alpha's
; mewe_start= Vector containing index of first non-zero alpha

common xmewe_com,mewe_data,mewe_elem,mewe_abun,mewe_nval,mewe_start,mewe_abun_cal,$
    mewe_file_prev, mewe_spl

if n_elements(mewe_data) eq 0 then read_file = 1 else $
   if mewe_file_prev ne mewe_file then read_file = 1 else read_file = 0

if read_file then begin
  message,/info,'Reading the Mewe data file = '+mewe_file
  restgen,file=mewe_file,mewe_data,abunstr,text=text
  mewe_data = mewe_data(where(mewe_data.wv gt 0.))  ; Eliminate null cases
  mewe_nval = intarr(n_elements(mewe_data))
  mewe_start= intarr(n_elements(mewe_data))
; Missing flag in Spex95 file is -99.
  if min(mewe_data.intens) le -90. then qnew = 1 else qnew = 0
  TMK = 4.+indgen(51)*.1         ; Part of the temporary Fix
  nmiss = 0           ; Part of the temporary Fix
  for i=0,n_elements(mewe_data)-1 do begin   ; Get # of intens values
;-------begin-temporary-fix-------------------------
; This next section of code takes care of the fact
; that some lines in the SPEX95 version have missing
; temperature values
     if qnew then begin
        k = where(mewe_data(i).intens gt -90.,nn)
        if nn ge 2 then begin
          if max(k(1:*)-k) ne 1 then begin
       print,'Missing Values: ',mewe_data(i).linnum,'  ',mewe_data(i).species,mewe_data(i).tr,mewe_data(i).wv
                j0=round((mewe_data(i).T0-TMK(0))*10)
;print,'-----------------'       ;***
;print,mewe_data(i).intens       ;***
                mewe_data(i).intens(k(0):k(0)+max(k)) =     $
         dspline(TMK(j0+k),mewe_data(i).intens(k),TMK(j0:j0+max(k)),interp=0)
;print,'-----------------'       ;***
;print,mewe_data(i).intens       ;***
;     stop          ;***
       nmiss = nmiss + 1
          endif
        endif      ; nc ge 2
     endif       ; qnew
;-------end-temporary-fix-----------------------
     if qnew then k = where(mewe_data(i).intens gt -90.,nn) $   ; SPEX95 files
             else k = where(mewe_data(i).intens ne 0,nn)    ; SPEX85 files
     mewe_nval(i) = nn
     mewe_start(i) = k(0)
  endfor
  if nmiss gt 0 then message,'Number problem T cases = '+strtrim(nmiss,2),/info ; Part of the temporary Fix
  nwave = n_elements(mewe_data)
  mewe_spl= replicate( {mewe_spline, nval:0L, tmax:0.0, xspline:fltarr(35), $
    yspline:fltarr(35), spl_init:fltarr(35)}, nwave)
  tempgen = 0.1 * findgen(35)
  release = idl_release(lower=4.0)
  for j=0,nwave-1 do begin
     xx = mewe_data(j).t0 + 0.1 * mewe_start(j) + tempgen(0:mewe_nval(j)-1)
     yy = mewe_data(j).intens(mewe_start(j):mewe_start(j)+mewe_nval(j)-1)
     if release and mewe_nval(j) gt 2 then zz = spl_init(xx,yy) else zz=xx*0.0
     mewe_spl(j).tmax = xx(mewe_nval(j)-1)
     mewe_spl(j).xspline(0:mewe_nval(j)-1) = xx + fltarr(mewe_nval(j))
     mewe_spl(j).yspline(0:mewe_nval(j)-1) = yy + fltarr(mewe_nval(j))
     mewe_spl(j).spl_init(0:mewe_nval(j)-1) = zz + fltarr(mewe_nval(j))
     endfor
  mewe_abun = abunstr.abun
  mewe_elem = abunstr.elem
  mewe_abun_cal = cosmic         ; Flag which data base this is
  mewe_file_prev = mewe_file         ; Save name of file that was read last
endif
abun = mewe_abun
elem = mewe_elem          ; Return elemental abundances
; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


if n_elements(wave_range) lt 2 then begin
   nwave = n_elements(mewe_data)
   jj = indgen(nwave)
endif else jj = where((mewe_data.wv ge min(wave_range)) and     $
             (mewe_data.wv le max(wave_range)),nwave)

if nwave eq 0 then begin
  wave = 0. &  Flux = 0.       ; Return something
  message,/info,' **** Warning in xlinflx ****'+string(7b)
  message,/info,'      No lines in requested wavelength range'
  return
endif

Temp = alog10(Te6*1.e6)
if n_elements(Temp) eq 1 then begin
  kk = 0
  j2 = where( mewe_spl(jj).xspline(0) le temp and mewe_spl(jj).tmax ge temp, nj2)
  if nj2 eq 0 then begin
    wave = 0. &  Flux = 0.   ; Return something
    message,/info,' **** Warning in xlinflx ****'+string(7b)
    message,/info,'      No lines with flux in requested wavelength range'
    return
    endif
  jj = jj(j2)
  nwave = nj2
  endif else kk = sort(Temp)
Ntemp = n_elements(Temp)
Flux  = fltarr(Ntemp,nwave)
Wave  = mewe_data(jj).wv       ; Wavelengths
if n_params() ge 4 then begin     ; Return line information
  Line = mewe_data(jj).species   ; Ion information
  Trans= mewe_data(jj).tr     ; Transition information
endif

release = idl_release(lower=4.0)
il = mewe_nval-1
for i=0,nwave-1 do begin
   j = jj(i)
   ilj = il(j)
   if mewe_nval(j) gt 1 then begin
     aa = fltarr(Ntemp)
     if ntemp gt 1 then $
    k = where((Temp(kk) ge mewe_spl(j).xspline(0)) and (Temp(kk) le $
    mewe_spl(j).xspline(ilj)),nk) $
    else begin
       k = 0       ;for 1 temperature all jj are valid
       nk = 1
    endelse
     if nk gt 0 then begin
     if mewe_nval(j) gt 2 then if release then $
       aa(kk(k)) = spl_interp(mewe_spl(j).xspline(0:ilj),mewe_spl(j).yspline(0:ilj), $
       mewe_spl(j).spl_init(0:ilj), Temp(kk(k))) $
       else aa(kk(k)) = spline(mewe_spl(j).xspline(0:ilj),mewe_spl(j).yspline(0:ilj),$
       Temp(kk(k))) $
       else aa(kk(k)) = interpol(mewe_spl(j).yspline(0:ilj), $
    mewe_spl(j).xspline(0:ilj),Temp(kk(k)))
        Flux(0,i) = aa
     endif
   endif          ; mewe_nval(j) gt 1
endfor

;       Calculate Line flux (in erg s-1 or ph s-1)

;            [ Flux = 1.e-23 * 10**(-int_fac)           ! erg cm+3 s-1 ]
;            [ 44. => EM=1.e44 cm-3 ]

k = where(Flux ne 0.,nk)
if nk gt 0 then Flux(k) = 10^(44. - 23. - Flux(k))  ; erg cm+3 s-1
;Change from transpose(wave) to reform(wave,1,Nwave) which works for one element array
if n_elements(wave) eq 1 then wave = [wave]
if units eq 0 then Flux =  Flux / 1.98648e-8 * rebin(reform(wave,1,Nwave),Ntemp,Nwave)

; Call picklambda to eliminate duplicate wavelengths
; picklambda wants a 2-d array - First subscript=Temp, 2nd=wavelength
nrel = n_elements(rel_abun)
if nrel ge 2 and (nrel mod 2 eq 0) then begin


  z_lbl  = ['H ', 'HE', 'LI', 'BE', 'B ', 'C ', 'N ', 'O ', 'F ', 'NE', 'NA', $
    'MG', 'AL', 'SI', 'P ', 'S ', 'CL', 'AR', 'K ', 'CA', 'SC', 'TI', 'V ',$
    'CR', 'MN', 'FE', 'CO', 'NI', 'CU', 'ZN']
  wa = where( mewe_elem eq 'A ',nwa)
  if nwa then mewe_elem(wa)= 'AR'
  zmewe =where_arr(z_lbl, mewe_elem) +1

  rel_abun = reform( rel_abun, 2, nrel/2)
  wabun = where_arr(zmewe, rel_abun(0,*), nabun)
  ;
  ;zmewe is ordered by Z, so rel_abun(0,*) must be ordered, too.
  ;
  if nabun ge 2 then rabun = rel_abun(*,sort(rel_abun(0,*))) else rabun = rel_abun
  if nabun ge 1 then for i=0,nabun-1 do begin
    wline = where(strmid(line,0,2) eq mewe_elem(wabun(i)), nmod)
    if nmod ge 1 then flux(*,wline) = flux(*,wline) * rabun(1,i)
    endfor

  endif

; Call picklambda to eliminate duplicate wavelengths
; picklambda wants a 2-d array - First subscript=Temp, 2nd=wavelength

if n_params() le 3 then        $
  picklambda,wave,flux,wave,flux    ; Eliminate duplicate wavelengths

if keyword_set(qstop) then stop
return
end
