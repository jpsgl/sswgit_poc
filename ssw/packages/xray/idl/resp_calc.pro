;+
; Name:
;	RESP_CALC (response calculation)
;
; Category:
;	SPECTRA, XRAYS, DETECTOR RESPONSE, X-ray analysis
;
; Call:
;       resp_calc, detector,area,func,func_par,d,z,gmcm,nflux,elo,ehi, $;inputs
;	   eloss_mat, pls_ht_mat, ein, smatrix 			        ;outputs
;	   eout=eout, linearpha=linearpha, nosigma=nosigma, theta=theta,$
;          verbose=verbose, error=error
;
; INPUTS:
;
;       NFLUX   - number of input energy bins, log. spacing,
;		  May also be output bin number if EOUT not input as a keyword.
;       ELO     - lower energy limit of matrix in keV
;       EHI     - upper energy limit of matrix in keV
;       AREA    - detector geometric area in cm^2
;       DETECTOR- detector composition e.g. 'CSI' as allowed in DET_XSEC.PRO or
;		  XSEC.PRO
;       FUNC	- 'NAME' of function which returns FWHM in keV as a
;               function of Energy in keV
;       FUNC_PAR- Vector of four constants used in FUNC_NAME
;       D       - detector thickness in cm
;       Z	- atomic numbers of up to four elements in the detector window
;       GMCM	- thickness of each element in gm/cm^2
;
; Optional Inputs:
;
;	EDGES_IN - 1d or 2d specification of photon energy edges.
;	EDGES_OUT - 1d or 2d specification of channel energy edges.
;	NOSIGMA - if set then there is no resolution broadening calculated
;	THETA   - angle of incidence, degrees, default is zero degrees from normal
;	VERBOSE - if set, then print a message about the output
;	EXTEND_EDGES_OUT - internally extend the output edges before clipping them for the result
;	EFF   - photopeak efficiency
; OUTPUTS:
;       EIN - channel energy edges for photon input, fltarr(2,nflux), lo and hi edges
;	in keV
;       ELOSS_MAT- energy loss matrix
;       PLS_HT_MAT- pulse-height response matrix, ELOSS_MAT convolved with
;               energy resolution broadening
;	SMATRIX - Also pulse-height response matrix, except it's normalized
;		to units of 1/keV
;	error - returns 1 if there was an error in the inputs to det_xsec.pro
; KEYWORDS
;	EOUT- May be used to specify the output PHA edges, 2Xnumber_output_bins, or
;	number_output_bins+1, in keV.  Will be log spaced unless LINEARPHA is set
;	LINEARPHA- If set and EOUT isn't specified, then PHA channels will be
;	linear from ELO to EHI
; Procedure:
;	This is a simple model of the response of a detector to an X-ray flux
;	in the regime where the photo-electric response dominates.  It is
;	most suitable for the simple geometry of an infinite planar detector,
;	because no finite size effects (edge effects) are considered.  It allows
;	the  inclusion of attenuation by detector windows and housings.  It includes
;	the effect of single Compton scattering assuming escape of the scattered
;	photon, the effect of escape by fluorescent photons produced by atomic
;	de-excitation in the detector material from the photo-electric capture of
;	photons above the K-edge.  It can include the effect of resolution
;	broadening using an arbitrary gaussian function whose width is a function
;	of the energy and up to 4 parameters.  There are several possible detector
;	materials allowed for at present, 'Si', 'Ge','NaI', 'CsI', and 'SXS' where the
;	last refers to the Soft X-Ray Spectrometers in the Yohkoh WBS.  This model
;	can be made to accommodate such standard detector materials as Si and CdZnTe
;	without too much difficulty provided DET_XSEC and FLUORESCENCE are modified.
;	Another area of needed modification is consideration of the probability of
;	Compton escape vs subsequent photo-electric capture.
; Calls to:
;	det_xsec, xsec, klein_elctrn, fluorescence, pulse_spread, edge_products
; Restrictions:
;	As currently implemented the input output bins are identical.  To change
;	the output bins to detector channels, the matrix must be interpolated
;	and/or integrated into the new channels.
;
; History - RAS, 92/12/01
;	   ras, 9 May 1995
;          Version 3, 16 Aug 1996, optimized pulse shape calculation
;          Version 4, 18 nov 1996, changed EDGES to EIN, added output vector
;	   for pulse-height bins, EOUT, made linear option for EOUT
;          Version 5, 19 Nov 1996, using PULSE_SPREAD.pro for resolution broadening
;          Version 6, 20 Nov 1996, fixed energy interpolation (IDIAG* and WEIGHT*)
;    8-jun-2013, ras, converted to square brackets, added EDGES_IN and EDGES_OUT keywords
;	;18-feb-2014, ras, added > 0 protection to fluorescent bin calculation
;	5-mar-2014, ras, added extend_edges_out and final clipping, only valid when edges_out is set, 
;	  added Eff, relative photopeak efficiency. 0-1 where 1 would be a perfect photopeak over the whole geometric area
;	29-oct-2014, ras, replace Compton scatter spectrum elements with new routine that integrates over the
;	output energy bins unlike the former version that integrated over the input energy bins  
;-
pro resp_calc, detector,area,func,func_par,d,z,gmcm,nflux,elo,ehi, $;inputs
	eloss_mat, pls_ht_mat, ein, smatrix,  		   $;outputs
	nosigma=nosigma, theta=theta, verbose=verbose, $
	pulse_shape=pulse_shape, eout=eout, linearpha=linearpha, edges_in=edges_in, $
	edges_out = edges_out, extend_edges_out = extend_edges_out, eff=eff

checkvar, theta, 0.0
costheta = cos( theta/!radeg )

if keyword_set(verbose) then begin
	chkarg,'resp_calc',proc
	doc_end = where( strpos(proc,';-') ne -1)
	more, proc[0:doc_end[0]]
endif


ein = keyword_set( edges_in) ? edges_in : exp( findgen(nflux+1) * alog( ehi/elo ) / nflux) * elo
edge_products, ein, edges_2=ein, mean=emin, width=win
nflux = n_elements(win)
if not keyword_set(eout) then $
if keyword_set(linearpha) then eout = findgen(nflux+1) * (ehi-elo)/nflux + elo $
  else eout = ein
default, extend_edges_out, 0
valid_out = keyword_set( edges_out )
extend_edges_out = valid_out * extend_edges_out
eout = valid_out ? edges_out : eout
edge_products, eout, edges_2=eout, edges_1 = eout1d, mean=emout, width= wout

nout=n_elements(wout)
if extend_edges_out then begin
	;use nout edges at the end, now add bins to top of ein
	de = wout[ nout-1 ]
	eout = [get_edges( eout, /edges_1), findgen( (max(ein) - eout[1, nout-1]) / de) * de + eout[1, nout-1] ]
	edge_products, eout, edges_2=eout, mean=emout, width= wout
	nout0 = nout
	nout  = n_elements( wout )
	endif

eloss_mat = fltarr(nout, nflux)
iout = findgen(nout)
;indices to the diagonal energy positions in eloss_mat
idiag = interpol(iout, emout, emin)>0+ findgen(nflux)*nout
;
; Construct weights over two adjacent output energies to obtain the original photopeak's centroid
;
idiag0 = floor(idiag)
idiag1 = idiag0+1 < (nflux*nout-1)
weight0= 1.0 - (idiag-idiag0)
weight1= 1-weight0

;DETECTOR INTERACTION CROSS-SECTIONS

s_pe  = det_xsec( emin, det = detector, TYP='PE', error = error )  ;PHOTOELECTRIC XSEC IN 1/CM
s_cmp = det_xsec( emin, det = detector, TYP='SI', error = error ) ;COMPTON XSEC  IN 1/CM

s_tot = s_pe + s_cmp ;TOTAL CROSS-SECTION FOR ENERGY ABSORBING INTERACTION

pe_xray  = (1.-exp(-( (s_tot * D / COSTHETA) < 40.))) * s_pe / s_tot ;PROBABILITY OF PHOTO ELEC.
cmp_xray = s_cmp / s_pe * pe_xray ; PROBABILITY OF COMPTON SCATTER
;

;WINDOW ATTENUATION
windows = where(z ne 0,nwind) ;use only defined windows
atten= 1./ exp(  (xsec(emin,Z[0],'AB',/cm2perg, error=error) * $
	 gmcm[0]/costheta) < 40. )
if nwind gt 1 then $
	for i=1,nwind-1 do atten = $
	 atten / exp(  (xsec(emin,z[I],'AB',/cm2perg, error=error) * $
	   gmcm[i]/costheta) < 40. )

;FLUORESCENT ESCAPE
;
fluorescence, emin, kp, ke, det=detector, theta=theta

;EMIN - INCIDENT PHOTON ENERGY IN KEV
;KP  - PROBABILITY OF FL. ESCAPE AT ENERGY KE IN KEV
;KE  - ENERGIES OF ESCAPE PHOTONS
;n_escape - number of escape energies

n_escape = n_elements(ke)
kp_tot = rebin(kp,nflux,1) * n_escape

;PHOTOPEAKS INCLUDING FLUORESCENT LOSS

eff=(1.-kp_tot) * atten * pe_xray
eloss_mat[idiag0]      = eff*weight0
eloss_mat[idiag1]      = eff*weight1

;FLUORESCENT ESCAPE PEAKS
for i = 0L, n_escape - 1L do begin; put in escape peaks at each escape energy

	escape=emin-ke[i]
	fluo = ( interpol(iout, emout, escape) > 0 ) + ( findgen(nflux) * nout ) ;18-feb-2014, added > 0 protection
	wfluo=where( fluo ge 0 and fluo le ( 1L * nflux * nout - 1 ), nfluo )
	if nfluo ge 1 then begin
	  ifluo0 = floor( fluo )
	  ifluo1 = ifluo0+1 < ( nflux*nout-1 )
	  weight0= 1.0 - ( fluo - ifluo0 )
	  weight1= 1 - weight0

	  eloss_mat[ ifluo0[ wfluo ] ] = weight0 * ( kp[ wfluo,i ] * eff[ wfluo ]/( 1-kp_tot[ wfluo ] ) + $
				eloss_mat[ ifluo0[ wfluo ] ] )
	  eloss_mat[ ifluo1[ wfluo ] ] = weight1 * ( kp[ wfluo,i ] * eff[ wfluo ]/( 1-kp_tot[ wfluo ] ) + $
				eloss_mat[ ifluo1[ wfluo ] ] )
	endif
endfor

;

;COMPTON SCATTERING - SINGLE INTERACTION
cscatt     = resp_calc_compton_scatter( eout, ein, cmp_xray )
eloss_mat += cscatt

pls_ht_mat = 0.0
smatrix    = 0.0

if not keyword_set(nosigma) then begin
RESOLUTION: ; BROADENING
;The function named in FUNC is the FWHM in keV as a function of energy in keV.
;Calculate the standard deviation of the resolution broadening function
;as a function of energy.

ps_input = { ein:ein, eout:eout, func:func, func_par:func_par }

pulse_spread, ps_input, pulse_shape, eloss_mat, pls_ht_mat

;
pls_ht_mat = pls_ht_mat * area * costheta

smatrix =  pls_ht_mat / rebin(reform( wout, nout, 1 ), nout, nflux)
;output rows of SMATRIX are in units of counts/keV/photon

endif
;MULTIPLY BY THE DETECTOR AREA
eloss_mat = eloss_mat * area * costheta
if extend_edges_out then begin
	smatrix = smatrix[ 0 : nout0-1, * ]
	eloss_mat = eloss_mat[ 0 : nout0-1, * ]
	pls_ht_mat = pls_ht_mat[ 0 : nout0-1, * ]
	pulse_shape = pulse_shape[ 0 : nout0-1, 0 : nout0-1 ]

	endif
end
