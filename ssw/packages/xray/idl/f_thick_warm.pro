;+
;
; NAME: f_thick_warm
;
; PURPOSE: This function returns photon flux from a warm thick-target model 
;  Uses simplified form of Equations 24 and 25 from Kontar et al, ApJ 2015
;  http://adsabs.harvard.edu/abs/2015arXiv150503733K
;
; CATEGORY:
;       SPECTRA, XRAYS
;
; CALLING SEQUENCE:
;       Flux = f_thick_warm( E, A )
;
; CALLS:
;       f_vth
;       f_thick2
;       
; INPUTS:
; E -energy vector in keV, 2XN edges or N mean energies
; A -model parameters defined below
;   a(0) - Total integrated electron flux, in units of 10^35 electrons sec^-1.
;   a(1) - Power-law index of the electron distribution function below
;          eebrk.
;   a(2) - Break energy in the electron distribution function (in keV)
;   a(3) - Power-law index of the electron distribution function above
;          eebrk.
;   a(4) - Low energy cutoff in the electron distribution function
;          (in keV).
;   a(5) - High energy cutoff in the electron distribution function (in keV).
;   
;   *********************(above parameters are as in standard TT model)
;   
;   a(6) - Warm plasma density [10^10 cm ^-3]
;   a(7) - Warm plasma temperature [keV] 
;   a(8) - Warm plasma length [Mm], 1Mm =10^8 cm
;   a(9)  - relative abundance of elements, default = 1 relative to CHIANTI coronal
;           /basically done as in f_vth/
;
; MODIFICATION HISTORY:
; eduard(at)astro.gla.ac.uk, July 18, 2015 written
; eduard                              uses thick target v2 for speeed
; 24-Jul-2015, Kim. Renamed f_thick_warm from f_warm_thick to be parallel to other thick2 functions
; 25/07/2015, eduard@glasgow added warning for low density 
; 15/08/2015 eduard@glasgow added new parameter - element abundance added to accomodate Brian's wish


function f_thick_warm, e, a, _extra=_extra

FTT= f_vth_thick2(e, a[0:5], /no_vth, _extra=_extra)

me_keV=511. ;[keV]
cc=2.99d10  ; speed of light [cm/s]
KK=2.6d-18
np =a[6]*1e10
Tloop=a[7]
L= a[8]*1e8

ll=Tloop^2/(2.D0*KK*np) ; collisional stoping distance for electrons of Tloop energy

Emin=Tloop*3.*(5.d0*ll/L)^4

EM_add=3.*!PI/2./KK/cc*SQRT(me_keV/8.)*Tloop^2/sqrt(Emin)*a[0]*1d35

EM49  =EM_add*1d-49 ; EM in units of 10^49 cm^(-3)

;print,'Emin =', Emin, 'Additional EM =', EM49

;mean free path of the lowest energy in the injected spectrum
L_min = a[4]^2/(2.*KK*np)/3.
IF (L_min GT L) THEN message,/info, 'WARNING: min mean free path > loop length: Emin/kT ='+trim(Emin/Tloop)
; For low density /or large low energy cut off in the injected spectrum/
; thermalization is less important

IF (Emin GT 0.1 ) THEN BEGIN 
  message,/info, 'WARNING: Emin > 0.1 keV,  Setting Emin to 0.1'
  Emin = 0.1
  ; approximation breaks for large Emin 
ENDIF

Fvth_add=f_vth(e, [EM49,Tloop,a[9]])

return, FTT+Fvth_add

end