;+
; NAME:
; F_EXP
;
; PURPOSE: negative exponential function
;
; CALLING SEQUENCE: result = f_exp(e, a)
;
; CATEGORY: SPEX, spectral fitting
;
; INPUTS:
; e - independent variable, nominally energy in keV in SPEX
;     If a 1-d array, these are interpreted as the mean.
;     If a 2xN array, these are interpreted as edges and
;     and the arithmetic mean is calculated and used
; a - parameters describing the exponential
; a[0] - normalization for exponential
; a[1] - pseudo temperature for exponential, divides e in arg for exp
;
; INPUT KEYWORD:
;   DO_CALC - if set to 0, return an array of zeroes of the correct size (default is 1)
;
; OPTIONAL INPUTS:

; OUTPUTS:
; result of function, an exponential
; OPTIONAL OUTPUTS:
;
; PROCEDURE:
;		result = exp(	( ( a[0] - em / a[1] ) > (-80.0) )  < 80. )

; CALLS:   exp, EDGE_PRODUCTS
;
; COMMON BLOCKS:
; none
;
; MODIFICATION HISTORY:
; Kim Tolbert, 17-Sep-2009
; 11-Apr-2014, Kim. Added do_calc keyword. Needed for nodrm version of this routine.
; 12-may-2014, richard.schwartz@nasa.gov, changed a[0] to exp(a[0]) and protect against $
;	over/underflow with an arg of the exponential from -70 to 70
;-
;
function f_exp, e, a, pder, do_calc=do_calc, _extra=_extra

checkvar, do_calc, 1

if (size(e))[0] eq 2 then edge_products, e, mean=em else em=e

if keyword_set(do_calc) then begin
	arg =   ( ( a[0] - em / a[1] ) > (-80.0) )  < 80.
	result = exp( arg )
	endif else result = fltarr( n_elements( em ) )

return, result

;pder = [[bx], [a[0]*(-x)*bx]]
end