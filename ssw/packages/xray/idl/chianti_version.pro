;+
; PROJECT:
;   SSW
; NAME:
;   Returns the current chianti version number. warns
;   if chianti needs to be loaded into ssw via ssw_packages,/chianti
;
; PURPOSE:
;
;
; CATEGORY:
;   XRAY
;
; CALLING SEQUENCE:
;
;
; CALLS:
;   none
;
; INPUTS:
;       none explicit, only through commons;
;
; OPTIONAL INPUTS:
;   none
;
; OUTPUTS:
;       none explicit, only through commons;
;
; OPTIONAL OUTPUTS:
;   none
;
; KEYWORDS:
;   none
; COMMON BLOCKS:
;   none
;
; SIDE EFFECTS:
;   none
;
; RESTRICTIONS:
;   none
;
; PROCEDURE:
;   none
;
; MODIFICATION HISTORY:
;   11-feb-2005, richard.schwartz@gsfc.nasa.gov
;
;-



function chianti_version,  var

defsysv, '!xuvtop', exists=valid

if not valid then begin
    print, 'CHIANTI package not included in SSW.  Try IDL> ssw_packages, /chianti'
    return, ''
    endif

return, (rd_text(findfile(concat_dir(!xuvtop,'VERSION'))))[0]
end