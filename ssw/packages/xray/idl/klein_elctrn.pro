;+
; Name        :
;	KLEIN_ELCTRN
;               
; Purpose     : 
;	This function returns the Klein-Nishina differential Compton scattering cross-section 
;	in cm^2 electron^(-1) sr^(-1) as a function of the kinetic energy of the 
;	scattered electron.
;               
; Category    : Spectra
;               
; Explanation : Klein-Nishina formula from Zombeck
;               
; Use         : result = KLEIN_ELECTRN( photon_energy, electron_energy )
;    
; Inputs      : 
;	photon_energy - Energy in keV
;	electron_energy - vector of kinetic energies of scattered electrons
;               
; Opt. Inputs : None
;               
; Outputs     : None
;
; Opt. Outputs: None
;               
; Keywords    :              tot=tot, mu=mu, wmu=wmu, enew=enew
;	TOT - total Compton xsec cm^2/electron
;	ENEW- energy of scattered photon (photon_energy-electron_energy)
;	WMU - indices of valid kinetic energies, electron_energy
;	MU  - Compton scattering angle, relative to incident photon direction
;
; Calls       :
;	KLEIN
;
; Common      : None
;               
; Restrictions: 
; Side effects: None.
;               
; Prev. Hist  : 
;
; Modified    : 
;	Version 1, ras 2-Mar-1996
;	Version 2, ras, 29-oct-2014, changed the variable names to be more descriptive
;
;-            

function klein_elctrn, photon_energy, electron_energy, tot=tot, mu=mu, wmu=wmu, enew=enew

alpha  = photon_energy / 511.0 ;photon energy in units of the electron rest mass
dsigma = electron_energy * 0.0

mu  = electron_energy*0.0 + 2.0 ;Out of range for cosine is default.
wok = where( photon_energy gt electron_energy, nok)
if nok ge 1 then mu[ wok ]  = 1. - electron_energy[ wok ]/(photon_energy - electron_energy[ wok ]) / alpha

wmu = where( abs(mu) le 1., ncount) ;allowed values of the cosine
if ncount ge 1 then begin
	mu  = mu[wmu]
	dsigma_dmu = 2.* !pi * klein( photon_energy, mu, tot=tot, enew=enew) 
	dsigma[ wmu ] =  dsigma_dmu * ( 1.0 + alpha * ( 1 - mu ))^2 / alpha / photon_energy
endif

return, dsigma
end





                                                       






                                                       





                                                       
