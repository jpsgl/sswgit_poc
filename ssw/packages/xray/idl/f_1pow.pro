;+
; NAME:
; F_1POW
;
; PURPOSE: single power-law function with epivot as a parameter
;
; CALLING SEQUENCE: result = f_1pow(e, a)
;
; CATEGORY: SPEX, spectral fitting
;
; INPUTS:
; e - independent variable, nominally energy in keV in SPEX
;     If a 1-d array, these are interpreted as the mean.
;     If a 2xN array, these are interpreted as edges and
;     and the arithmetic mean is calculated and used
; a - parameters describing the single power-law
; a(0) - normalization at epivot, photon flux of first power-law
;        at epivot
; a(1) - negative power law index
; a(2) - epivot (keV)
; OPTIONAL INPUTS:

; OUTPUTS:
; result of function, a power law
; OPTIONAL OUTPUTS:
;
; PROCEDURE:

; CALLS:   F_DIV, EDGE_PRODUCTS
;
; COMMON BLOCKS:
; none
;
; RESTRICTIONS: power-law cannot be flatter then x^(-.01)
;
; MODIFICATION HISTORY:
; Kim Tolbert, 17-Sep-2009
;-
;

function f_1pow, e, a, _extra=_extra

if (size(e))[0] eq 2 then edge_products, e, mean=em else em=e

return, a[0]* f_div(a[2], em)^a[1]

end
