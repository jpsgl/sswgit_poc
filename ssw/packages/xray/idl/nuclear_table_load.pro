;+
; NAME:
;	NUCLEAR_TABLE_LOAD
;
; PURPOSE:
;	Loads the table into F_NUCLEAR
;
; CATEGORY:
;	XRAY, GAMMA_RAYS, SPECTRUM, SPEX
;
; CALLING SEQUENCE:  Nuclear_table_load, table_file
;
; CALLS:

; INPUTS:
;   Table_file -
;		nuclear template file (see f_nuclear)
;		includes full path if not in current directory or $SSWDB_XRAY
;		if no value passed, default file is used:
;		$SSWDB_XRAY/nuclear_template.sav
; COMMON BLOCKS:
; F_NUCLEAR_COM
;
; MODIFICATION HISTORY:
;	25-feb-03, richard.schwartz@gsfc.nasa.gov
;-

pro nuclear_table_load, table_file, error=error

error = 0

common f_nuclear_com, enucl, fnucl

	default = 'nuclear_template.sav'
	checkvar, table_file, default
    template = loc_file(path=[curdir(),'$SSWDB_XRAY'],table_file, $
      count=ncount)
    if ncount eq 0 then BEGIN
        error = 1
        if table_file eq default then $
        printx, default+' file not installed on SSWDB_XRAY'
        error = 1
        return
    endif
    readme = ''
    ; midpoints for 2550 channels
    enucl = fltarr(2550)

    ; flux normalized to 1 photon/cm2
    fnucl = fltarr(2550)
    restore, template[0]

    ; midpoint of first channel must be > 1.0 keV
    enucl[0] = enucl[0] > 1.001
    ;Extend the template to ensure any extrapolation above
    ;the last energy is 0.0
    enucl = [enucl, last_item(enucl)+10.]
    fnucl = [fnucl, 0.0]

END
