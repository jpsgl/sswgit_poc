;+
; NAME:
;	F_NUCLEAR
;
; PURPOSE:
;	This function returns a template nuclear line spectral model
;	including independent broken power-law, 2230 keV line, and 511
;	keV line.
;
; CATEGORY:
;	XRAY, GAMMA_RAYS, SPECTRUM, SPEX
;
; CALLING SEQUENCE:  spectrum = f_nuclear( ekev, par)
;
; CALLS: f_bpow
;        edge_products
;	 f_div
;	 concat_dir
; INPUTS:
;   ekev - photon energies in keV, 2xN, upper and lower energy
;          bounds for N channels
;
;   parameters  - model parameters:
;       parameters[0] - nuclear line template flux, normalized to 1
;                photon/cm2, from file 'nuclear_template.sav'
;       parameters[1] - Annihilation line  flux, normalized to 1 photon/cm^2
;       parameters[2] - Positronium flux
;       parameters[3] - Annihilation line sigma
;       parameters[4] - Annihilation line centroid
;       parameters[5] - 2.223 MeV line flux, normalized to 1 photon/cm^2
;       parameters[6] - 2.223 MeV line centroid
;       parameters[7] - 2.223 MeV sigma
;       parameters[8->8+ntemp-1] - flux values for line templates,
;                       normalized to 1 photon/cm^2.  Number of
;                       parameters ( ntemp ) is number of line
;                       templates defined in LINE_COMPLEX_COM.
;       parameters[8+ntemp -> 8+ntemp+5 ] - f_bpow parameters
;       parameters[8+ntemp] - normalization, flux at Epivot (Epivot defaults
;                       to 50 keV - see function_com.pro)
;       parameters[8+ntemp+1] - power law index below break energy
;       parameters[8+ntemp+2] - break energy
;       parameters[8+ntemp+3] - power law index above break energy
;       parameters[8+ntemp+4] - low energy cutoff
;       parameters[8+ntemp+5] - power law index below low energy
;                        cutoff, 1<Eco<2, default value is 1.5
;
; If the break energy is set to zero, the break is ignored and the power
; law is calculated over the full range.
;
; OUTPUTS:
;  flux - photons/cm2/keV for each bin
;
; OUTPUT KEYWORDS:
;  N_LINE - Number of line templates in LINE_COMPLEX_COM
;  ERROR - Set to [ 0 / 1 ] if an error [ did not / did ] occur.
;
; KEYWORDS:
;  Any additional keywords are passed by reference to LINE_COMPLEX.
;
; COMMON BLOCKS:
; F_NUCLEAR_COM
;
; MODIFICATION HISTORY:
;	ras, 17-jun-94, based on line template from Ron Murphy and Gerry Share
;	Version 2, ras, 6-oct-94
;	Version 3, ras, 8-apr-97, add SSWDB_XRAY to data file search.
;       Version 4, richard.schwartz@gsfc.nasa.gov, 24-jul-1997,
;       removed PERM_DATA.
; 	31-May-2001, Paul Bilodeau - now accepts 1 or 2-d arrays of
;       energy bounds for input. First element of enucl read in from
;       template file is at least 1.001 keV.
; 	14-June-2001, Paul Bilodeau - added positronium component and
;       two gaussian complexes.  Reorganzied list of parameters.
; 	15-June-2001, Paul Bilodeau - added GAUSS_TEMPLATE keyword.
;       19-August-2002, Paul Bilodeau - Rewrote to use line_complex
;         code, changed keywords and keyword handling,  and updated
;         documentation.
;       30-sep-2002, Paul Bilodeau - changed from posit_wfrac to
;         f_positronium.  Allow annihilation line (511 keV) centroid
;         as a parameter to facilitate fitting.
;       12-nov-2002, Paul Bilodeau and RAS, use interp2integ for
;         nuclear_template.sav rebinning.
;	13-nov-02, ras, protect against negative extension of
;	extrapolation
;       27-nov-2002, Paul.Bilodeau@gsfc.nasa.gov - fix doc error
;          regarding 2.223 MeV line width and update positronium line
;          width description.
;-

function f_nuclear, ekev, $
                    parameters, $
                    error=error, $
                    N_LINE=n_line, $
                    _REF_EXTRA=_ref_extra

error = 0

common f_nuclear_com, enucl, fnucl

if n_elements(fnucl) eq 0 then begin
    template = loc_file(path=[curdir(),'SSWDB_XRAY'],'nuclear_template.sav', $
      count=ncount)
    if ncount eq 0 then BEGIN
        error = 1
        printx, 'nuclear_template.sav file not installed on SSWDB_XRAY'
        return, 0.0
    endif
    readme = ''
    ; midpoints for 2550 channels
    enucl = fltarr(2550)

    ; flux normalized to 1 photon/cm2
    fnucl = fltarr(2550)
    restore, template(0)

    ; midpoint of first channel must be > 1.0 keV
    enucl[0] = enucl[0] > 1.001
    ;Extend the template to ensure any extrapolation above
    ;the last energy is 0.0
    enucl = [enucl, last_item(enucl)+10.]
    fnucl = [fnucl, 0.0]
endif

;get mean energy and widths
edge_products, ekev, edges_1=edges, mean=em, width=wkev, edges_2=edges2

; Interpolate the flux onto the new bins, integrate, and divide by the
; bin width.
flux_ekev = interp2integ( edges2, enucl, fnucl )
flux_ekev = parameters[0] * f_div( flux_ekev, wkev )

; Add 511 line with positronium component and 2.223 MeV line.
flux_ekev = Temporary( flux_ekev ) + $
            f_positronium( edges2, parameters[1:4] ) + $
            f_nline( edges2, parameters[5:7], _EXTRA=_ref_extra )

; Line complexes from line_complex
n_line = 0
IF N_Elements( parameters ) GT 8 THEN BEGIN
    flux_ekev = Temporary( flux_ekev ) + $
                line_complex( $
                  edges2, $
                  parameters[8:*], $
                  N_LINE=n_line, $
                  _EXTRA=_ref_extra )

    flux_ekev = Temporary( flux_ekev ) + f_bpow( em, parameters[8+n_line:*] )
ENDIF

RETURN, flux_ekev

END
