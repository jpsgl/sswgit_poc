
;+
 ;  PROJECT:
 ;    SSW/XRAY
 ;  NAME:
 ;    CHIANTI_KEV
 ;  PURPOSE:
 ;    This function returns a thermal spectrum (line + continuum) for EM=1.e44 cm^-3
 ;    Uses a database of line and continua spectra obtained from the CHIANTI distribution
 ;
 ;  CALLING SEQUENCE:
 ;    Flux = Chianti_kev(Te6, energy, /kev, /earth, /photon, /edges)       ; ph cm-2 s-1 keV-1 at the Earth
 ;
 ;  INPUTS:
 ;    Temp    = Electron Temperature in MK (may be a vector)
 ;    Energy   = Array of 2XN energies in keV, if 1D then they are assumed to be contiguous lower
 ;   and upper energy edges
 ;
 ;  CALLS:
 ;
 ;
 ;  OUTPUTS:
 ;    Flux   = Fluxes in ph s-1 or erg s-1
 ;      Fluxes = fltarr(N_elements(Te6),N_elements(wave))
 ;
 ;  OPTIONAL INPUT KEYWORDS:
 ;    KEV        = Units of wave are in keV, output units will be ph keV-1
 ;     If KEV is set, assumes EDGES is set so EDGES must be 2XN
 ;    EARTH      = calculate flux in units cm-2 s-1 (bin unit, ang or keV)-1
 ;    DATE       = optional date for calculation of earth flux, def='2-apr-92'
 ;    REL_ABUN   = A 2XN array, where the first index gives the atomic number
 ;     of the element and the second gives its relative abundance
 ;     to its nominal value given by ABUN.
 ; ENVIRONMENT VARIABLE SETUPS:
 ;    LINE_FILE    = explicit file name for extracted CHIANTI line dbase
 ;    CONT_FILE  = explicit file name for extracted CHIANTI continuum dbase
 ;    XR_AB_TYPE - A string designating the type of stored abundance distribution
 ;		to use
 ;   corresponding to the XR_AB_TYPE argument in xr_rd_abundance(),eg "cosmic", "sun_coronal", ...
 ;
;      1. cosmic
;      2. sun_coronal - default abundance
;      3. sun_coronal_ext
;      4. sun_hybrid
;      5. sun_hybrid_ext
;      6. sun_photospheric
;      7. mewe_cosmic
;      8. mewe_solar - default for mewe_kev
;
;  RESTRICTIONS:
;    N.B. If both edges aren't specified in WAVE, then the bins of WAVE must
;   be equally spaced.
;  METHOD:
;    Reads in a database from $SSWDB_XRAY, nominally 'chianti_setup.geny'
;  COMMON BLOCKS:
;   CHIANTI_KEV holds the line and continuum database obtained from CHIANTI.
;  MODIFICATION HISTORY:
;  richard.schwartz@gsfc.nasa.gov, 6-dec-2003
;  richard.schwartz@gsfc.nasa.gov, 24-jan-2005
;  24-mar-2006, ras, supports temp array
;  4-Apr-2006, Kim, fixed bug - for multi temp noline, wasn't initializing contspectrum
;    correctly - needs correctly dimensioned array of 0s.  Added nocont keyword
;	24-apr-2006, richard.schwartz@gsfc.nasa.gov,
;		reoved  ab_filename, see xr_rd_abundance and setup.xray_env
;-

function chianti_kev, temp, energy, kev=kev,  $

    tcont=tcont, reload=reload, earth=earth,$
    linespectrum=linespectrum, $
    contspectrum=contspectrum,$

    nocont=nocont, noline=noline,_extra=_extra
;photons/cm2/sec/kev



default, reload, 0

;chianti_kev_common_load - this is always done in chianti_kev_lines and _cont

ntemp = n_elements(temp)
temparr = ntemp eq 1 ? temp[0] : temp
nen = n_elements(energy[0,*])

linespectrum = 0.
contspectrum = fltarr(nen, ntemp)

if not keyword_set(noline) then $
   linespectrum = chianti_kev_lines(temparr, energy, $
      reload=reload, earth=earth, kev=kev, $
       _extra=_extra )

if not keyword_set(nocont) then $
   for i=0, ntemp-1 do $
      contspectrum[0,i]  = chianti_kev_cont(temp[i], energy, $
        reload=reload, earth=earth, kev=kev, tcont=tcont, $
         _extra=_extra )

return, linespectrum + contspectrum


end