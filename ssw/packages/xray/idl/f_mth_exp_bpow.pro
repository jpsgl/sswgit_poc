;+
; NAME: F_MTH_EXP_BPOW
;
; PURPOSE: Multithermal function plus a broken power-law function for fitting hard X-ray spectra.
;          The differential emission measure has an exponential dependence on temperature.
;
; CALLING SEQUENCE: result = f_mth_exp_bpow(e, a)
;
; INPUTS:
;   e - independent variable, nominally energy in keV under SPEX
;   a - parameters describing the function:
;
;       Multi-thermal parameters:
;       a(0) - differential emission measure at T = 2 keV in units of 10^49 cm^(-3) keV^(-1)
;       a(1) - minimum plasma temperature in keV
;       a(2) - maximum plasma temperature in keV
;       a(3) - temperature scale length in keV for caculating differential emission measure
;              at temperature T:
;              DEM = a(0) * exp( (2. - T) / a(3) )
;
;       Broken power law parameters:
;       a(4) - normalization at 50 keV (or epivot)
;       a(5) - negative power-law index below break
;	    a(6) - break energy
;	    a(7) - negative power-law index above break
;
; OUTPUTS:
;   Sum of multi-thermal and broken power law functions.
;
; PROCEDURE:
;   See f_multi_therm_exp and f_bpow.
;
; CALLS:
;   f_multi_therm_exp and f_bpow
;
; RESTRICTIONS:
;   The normalization for the differential emission measure, a(0), is fixed
;   at T = 2 keV in F_THERM_DEM_EXP.
;
; WRITTEN:
;   Paul Bilodeau, 24-February-2003
;
; REVISED:
;   Gordon Holman, 14 August 2003
;   Linhui Sui, 28 August 2003, write separate multi-thermal model functions for power-law and
;               exponential dependence of DEM on temperature
;
;-
;------------------------------------------------------------------------------
FUNCTION f_mth_exp_bpow, e, a

checkvar, a, Fltarr( 8 )

RETURN, f_multi_therm_exp(e, a[0:3] ) + f_bpow( e, a[4:*] )

END
