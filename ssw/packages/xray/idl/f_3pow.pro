function threepow, x, a
@function_com

apar=[1.0, a]


npar = n_elements(apar)
apar = apar(0:npar<5)

if npar lt 6 then apar = [apar, fltarr(6-npar)]
apar(4) = apar(4) > apar(2)
apar(1) = apar(1) >.01
if apar(3) eq 0.0 then apar(3)=apar(1)
if apar(5) eq 0.0 then apar(5)=apar(3)

apar(3) = apar(3) >.01
apar(5) = apar(5) >.01
ans = x*0.0

n1 = (Epivot / apar(2))^(apar(1)-apar(3))
n2 = n1 * (Epivot / apar(4))^(apar(3)-apar(5))

w0=where(x le apar(2), nw0)
if nw0 ge 1 then ans(w0) = apar(0) * (Epivot/x(w0))^apar(1)
w1=where(x gt apar(2) and x le apar(4), nw1)
if nw1 ge 1 then ans(w1) = n1 * (Epivot/x(w1))^apar(3)
w2=where(x gt apar(4), nw2)
if nw2 ge 1 then ans(w2) = n2 * (Epivot/x(w2))^apar(5)



return,ans
end

;+
; NAME:
;	F_3POW
;
; PURPOSE: triple broken power-law function with/without discontinuities in the
;	derivatives
;
; CALLING SEQUENCE: result = f_3pow(x, a)
;
; CATEGORY: SPEX, spectral fitting
;
; INPUTS:
;	x - independent variable, nominally energy in keV under SPEX
;	    if a 2xN array is passed, these are interpreted as edges
;	    and the value is calculated at the arithmetic mean
;	a - parameters describing the broken power-law
;	a(0) - normalization at epivot, photon flux of first power-law
;	       at epivot
;	a(1) - negative power law index below break energy1
;	a(2) - break energy1
;	a(3) - negative power law index above break energy1, below break energy2
;	a(4) - break energy2
;	a(5) - negative power law index above break energy2
; OUTPUTS:
;	result of function, a triple broken power law
; OPTIONAL OUTPUTS:
;
; PROCEDURE:	passes calculation onto threepow.pro
;
; CALLS:   threepow
;
; COMMON BLOCKS:
;	none
;
; RESTRICTIONS: power-law cannot be flatter then x^(-.01)
;
; MODIFICATION HISTORY:
;	ras, 1 nov 1995
; Kim Tolbert, 2004/07/16 - added _extra so won't crash if keyword is used in call
;-
;

function f_3pow,x,a, fname, _extra=_extra

if (size(x))(0) eq 2 then edge_products, x, mean=xm else xm = x
return, a(0)* threepow( xm, a(1:*) )


end
