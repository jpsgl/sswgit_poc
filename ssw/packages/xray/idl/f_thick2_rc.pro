;+
;
; NAME: F_THICK2_RC
;
; PURPOSE: Thick Target Bremsstrahlung Version 2 with independent normalization with return-current losses (see
;   Brm2_Frc_Distrn_vnorm header for more information)
; 
; METHOD: This function is the same as F_THICK2_VNORM except that it calls f_vth_thick2 with /return_current, which
;   gets passed through and tells brm2_fouter to call Brm2_Frc_Distrn_vnorm instead of Brm2_F_Distrn_vnorm.
;   This has the same parameters as F_THICK2_VNORM, except that a[3] (higher energy power-law index) isn't used.  It's still
;   there just as a placeholder for consistency in the parameter meanings.
;   F_THICK2_VNORM is the same as F_THICK2 except 
;       1.  the normalization and low energy cutoff
;       parameters are independent (in F_THICK2 they are highly coupled).
;       2.  The user can set the reference energy (should be between the low
;       energy cutoff and the break energy), but should not fit on this parameter.
;       3. There is an extra parameter (the reference energy)
;
; CATEGORY:
;       SPECTRA, XRAYS
;
; CALLING SEQUENCE:
;       Flux = F_THICK2_RC( E, A )
;
; CALLS:
;       F_VTH_THICK2
;
; INPUTS:
; E -energy vector in keV, 2XN edges or N mean energies
; A -model parameters defined below
;   a(0) - Electron flux at reference energy (a[6]), in units of 10^35 electrons (sec kev)^(-1)
;   a(1) - Power-law index of the electron distribution function below
;          eebrk.
;   a(2) - Break energy in the electron distribution function (in keV)
;   a(3) - Not used.
;   a(4) - Low energy cutoff in the electron distribution function
;          (in keV).
;   a(5) - High energy cutoff in the electron distribution function (in keV).
;   a(6) - Reference energy (in keV) at which function is normalized (KEEP FIXED DURING FIT)
;
; MODIFICATION HISTORY:
; Kim Tolbert 8-Feb-2013. Copied from f_thick2_vnorm.  Same except calls f_vth_thick2 with /return_current
; Kim Tolbert 09-Jul-2014, Pass ref_energy keyword with value of a[6] through (previously couldn't change from 50.)
;
;-

function f_thick2_rc, e, a, _extra=_extra

return, f_vth_thick2 (e, a, /no_vth, /independent_norm, /return_current, ref_energy=a[6], _extra=_extra)

end