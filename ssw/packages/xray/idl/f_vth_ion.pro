;+
;
; NAME:
; F_VTH_ION
;
; PURPOSE:
; Flux model of a thermal bremsstrahlung plus non-uniform target ionization.
;
; CATEGORY:
;       SPECTRA, XRAYS
;
; CALLING SEQUENCE:
;       Flux = F_VTH_ION( E, A )
;
; CALLS:
;       F_VTH, EDGE_PRODUCTS, CHECKVAR, ION_FIT
;
; INPUTS:
; E -energy vector in keV, 2XN edges or N mean energies
; A -model parameters defined below
; For the thermal function using F_VTH.PRO:
;   a(0)= emission measure in units of 10^49 cm-3
;   a(1)= KT   plasma temperature in keV
;
; For the non-uniform target ionization, ION_FIT is used.
;   a(2) - normalization
;   a(3) - spectral index of the electron flux
;   a(4) - Break energy in the electron distribution function (in keV)
;
; COMMON BLOCKS:
; FUNCTION_COM
;
; WRITTEN:
;   20-nov-2002, Paul.Bilodeau@gsfc.nasa.gov
;
;-
FUNCTION f_vth_ion, e, a

IF (size(e))(0) EQ 2 THEN edge_products, e, mean=em ELSE em=e

checkvar, apar, Fltarr(5)

npar = n_elements(a)

apar(0) = a(0: (npar-1)< (n_elements(apar)-1) )

ans = f_vth(em, apar[0:1] ) + f_ion( em, apar[2:4] )

RETURN, ans

END
