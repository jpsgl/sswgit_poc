;+
 ;  PROJECT:
 ;    SSW/XRAY
 ;  NAME:
 ;    CHIANTI_KEV_UNITS
 ;  PURPOSE:
 ;   This procedure manages the unit conversions for CHIANTI_KEV_LINES
 ;   and CHIANTI_KEV_CONT.
 ;
 ;  CALLING SEQUENCE:
 ;    chianti_kev_units, energy, spectrum, funits, kev=kev, earth=earth, date=date)
 ;
 ;  INPUTS:
 ;    Energy   = Array of 2XN energies in keV, if 1D then they are assumed to be contiguous lower
 ;   and upper energy edges
 ;    Spectrum - spectrum with default units as input, new units applied to funits
 ;		may be an array, num_energies x number_spectra
 ;
 ;  CALLS:
 ;
 ;
 ;  OUTPUTS:
 ;    funits - unit conversion factor applied to spectrum
 ;
 ;  OPTIONAL INPUT KEYWORDS:
 ;    PHOTON = If set, calculation is made in ph s-1 (default)
 ;    ERG    = If set, calculation is made in erg s-1
 ;    KEV        = Units of wave are in keV, output units will be ph keV-1
 ;    EARTH_FLUX = calculate flux in units cm-2 s-1 (bin unit, ang or keV)-1
 ;    DATE       = optional date for calculation of earth flux, def='2-apr-92'
 ;  RESTRICTIONS:
 ;
 ;  METHOD:
 ;    Reads in a database from $SSWDB_XRAY, nominally 'chianti_setup.geny'
; Explanation of Chianti units & emission measure (Ken Phillips, June 17,
;2004):
;
;Output of Chianti ch_ss units are in photons (or ergs) cm-2 s-1 A-1 sr-1, i.e. the output is a specific intensity (not a flux as it is per solid angle). Suppose specific intensity at some wavelength is F_lam for a
;*surface* emission measure = 10^27 cm^-5 (this is the ch_ss default).
;
;Let a flare on the sun have area A (cm^2). Its solid angle at earth is A/(au)^2.
;
;Therefore flux_lam = F_lam * A / (au)^2
;
;The flare *volume* emission measure corresponding to the 10^27 surface EM is A * 10^27 cm^-3.
;
;So flux per unit volume EM (Ne^2 V = 1) is
;
;F_lam * A/(au)^2 * 1/(10^27 A) = F_lam / (10^27 [au]^2)
;
;					= 4.44e-54 * F_lam
;(log(4.44e-54) = -53.35)
;
;Note the A's cancel out.
;
;So if you want to generate a *volume* EM = 10^49 cm^-3 from ch_ss, put in a value of log(*surface* EM) = 27.0 + 49.0 -  53.35 = 22.648
;
;The units of the spectrum will then be standard flux units, i.e. photons
;cm^2 s^-1 A^-1, without any steradian units.
;
;You can then convert fluxes to ph. Cm^2 s^-1 keV-1 by just multiplying by (lambda in A)^2 / 12.399 [or 12.399/(energy in keV)^2] and wavelength to energy by wavelength = 12.399/energy in keV

 ;  COMMON BLOCKS:
 ;  MODIFICATION HISTORY:
 ;  24-mar-2006 richard.schwartz - modified to work with 1 or more spectra
 ;  richard.schwartz@gsfc.nasa.gov,18-mar-2004
  ;-

pro chianti_kev_units,  spectrum, funits, $
    wedg=wedg, kev=kev, earth=earth, date=date


if keyword_set(kev) then begin
 if keyword_set(earth) then begin
    thisdist = 1.49627e13
    if keyword_set(date) then begin
     date = anytim( fcheck(date,'4-apr-92'),/yohkoh)
     radius = 6.9598e10
     arcsec = 180./!pi*3600.
     thisdist = radius/sin((get_rb0p(date))[0]/arcsec)
     endif
     funits = thisdist^2 ;unlike mewe_kev  don't use 4pi, chianti is per steradian
     endif

    funits = f_div(1d44/funits, wedg)
    ;Nominally 1d44/funits is 4.4666308e17 and alog10(4.4666e17) is 17.64998
    ;That's for emisson measure of 1d44cm-3, so for em of 1d49cm-3 we have a factor whos log10 is 22.649, just like kjp
    spectrum = float(spectrum * rebin(funits,size(/dim, spectrum)))
    ;tcont = float(tcont * funits)
    endif

end