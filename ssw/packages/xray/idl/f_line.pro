;+
; PROJECT:
;       HESSI
; NAME:
;	F_LINE
;
; PURPOSE:
;	This function returns a single Gaussian function (a0 is normalized)
;
; CATEGORY:
;	E2 - CURVE AND SURFACE FITTING.
; CALLS:
;	F_GAUSS_INTG
; CALLING SEQUENCE:
;	out = F_LINE(X,A)
;
; INPUTS:
;	X = VALUES OF INDEPENDENT VARIABLE, 2 X N, LO AND HI EDGES
;	A = PARAMETERS OF GAUSSIAN FUNCTION
;       A[0] - integrated intensity
;       A[1] - centroid
;       A[2] - sigma
;       (see F_GAUSS_INTG, GAUSS_INTG for more explanation)
; OUTPUTS:
;	Function values
;
; OPTIONAL OUTPUT PARAMETERS:
;
; COMMON BLOCKS:
;	NONE.
; SIDE EFFECTS:
;	NONE.
; RESTRICTIONS:
;	NONE.
; WRITTEN: Kim Tolbert 30-Jan-2004
;-
function f_line, x, a, _extra=_extra

; normalize a[0] to the width of the gaussian
a0 = f_div (a[0] / sqrt(2*!pi), a[2])

return, f_gauss_intg( x, [a0, a[1:2]] )

end