;+
; Name:  calc_nontherm_electron_energy_flux in erg / sec
; 
; Purpose: Calculate the nonthermal electron energy flux for a thick target model
; 
; Input arguments:
;  params - thick target parameters as follows:
;    params[0] - total integrated electron flux (1.e35 electrons/sec) if independent_norm not set
;                electron flux at ref energy params[6], 10^35 electrons (sec keV)^(-1)
;    params[1] - low delta
;    params[2] - break energy (keV)
;    params[3] - high delta
;    params[4] - low energy cutoff (keV)
;    params[5] - high energy cutoff (keV)
;    params[6] - reference energy (keV) if independent_norm is set
;    
;  Input Keywords:
;    independent_norm - if set then params[0] is electron flux at ref energy, otherwise it's integrated electron flux
;    func - name of function, e.g. 'thick2_vnorm'.  Func is required if your function is thick2_rc.
;    Note: if pass func, don't need to pass independent_norm, will figure it out.  
;    
;  Example: 
;    print,calc_nontherm_electron_energy_flux([1.2e-3, 4.8, 32000.,5., 25., 32000., 50.], /indep)
;        1.1939001e+027
;        
;  Output: Returns the the nonthermal electron energy flux in erg / sec
;  Written: Kim Tolbert 30-Jun-2014
;  Modifications:
;  
;-

function calc_nontherm_electron_energy_flux, params, independent_norm=independent_norm, func=func

par = params

if keyword_set(func) then begin
  status = check_func_electron(func, vnorm=vnorm, athin=athin)
  if ~status or athin then begin 
    message, /cont, 'Cannot calculate nonthermal electron energy flux for this function: ' + func[0]
    return, -1
  endif
  independent_norm = vnorm
endif

if keyword_set(independent_norm) then $
  ; first calculate total integrated electron flux (1.e35 electrons/sec)
  par[0] = Fint_from_Fref(par[0],par[1],par[2],par[3],par[4],par[5],par[6], func=func)

return, brmthick_power(par[0],par[1],par[2],par[3],par[4],par[5],eps=1.e-6)

end