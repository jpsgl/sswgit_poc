;+
; Name: calc_electron_distribution
;
; Purpose: Calculate the electron distribution (electron spectrum) for the thick and thin target models
; 
; Input Keywords:
; energy - array of energies in keV to calculate distribution for. N mean energies or 2xN edges. Default is 100 logarithmically 
;         spaced values from 1 to 300 keV.
;   par - array of model parameters (see f_thick... and f_thin... routines for description). Default is default
;         values for thick2_vnorm function
;   func - Model name (e.g. 'thick2_rc')  Default is 'thick2_vnorm'                  
; 
; Output Keywords:
;   fint - Total integrated electron flux in electrons sec^(-1)
;
; OUTPUTS:
;  Function value returned is electron spectrum in units of 1.e35 electrons s^(-1) kev^(-1) for thick target models,
;    1.e55 electrons s^(-1) kev^(-1) for thin target models.
;  Also returns integrated electron flux in keyword fint.
;  
; Written: 25-Jun-2014, Kim Tolbert
; Modifications: 
;   8-Jul-2014, Kim. Change calling args to keywords and set some defaults. 
;  14-Jul-2014, Kim. Don't call get_rc_electron_vals to get new fref for rc func.  That's now taken care of in fint_from_fref.
; 
;-


function calc_electron_distribution, energy=energy, par=par, func=func, fint=fint

  checkvar, energy, make_xarr(xlo=1., xhi=300., nx=100, /log) ; default is 100 log spaced energies from 1 to 300 
  checkvar, func, 'thick2_vnorm'
  checkvar, par, [1.,4.,150.,6.,20.,3200.,50.]
  
  e = energy ; protect input arg
  if size(e, /n_dim) eq 2 then e = get_edges(e, /mean)
  
  a = par  ; protect input argument from changing
  
  ; Check whether we can calc elect dist from function specified, and return flags for type of function 
  if ~check_func_electron(func, vnorm=vnorm, single_pow=single_pow, athin=athin, rc=rc) then begin
    message, /cont, 'Can not calculate electron distribution for ' + func + '. Aborting.'
    return, -1
  endif

  p = a[1]     ; index of elec. distr. below ebrk
  q = a[3]     ; index of elec. distr. above ebrk
  
  ; all in keV
  elo = a[4]  ; low energy cutoff
  ebrk = a[2] ; break energy
  ehi = a[5]  ; high energy cutoff  
  
  ; units of edf are keV^(-1)
  thinsm = 0  ; disable for now until figure out the normalization issue
  if thinsm then begin
    widtrans = a[6]
    brm2sm_distrn, e, p, q, elo, ebrk, ehi, widtrans, edf
    fint = fint_from_fref(1., p, ebrk, q, elo, ehi, 100.)  ; use energies in keV here
    ed = fint * edf
    ed = edf
  endif else begin
    brm2_distrn, e, p, q, elo, ebrk, ehi, edf, func=func
    
    ; Get fint, the total integrated electron flux to multiply edf by.  If this function was normalized at ref energy, then 
    ; calculate total integrated electron flux from flux at eref.  Otherwise, use a[[0].  Units of fint are electrons sec^(-1)
    if vnorm then begin
      fref = a[0]  ; electron flux at ref energy
      eref = a[6] ; reference energy
      fint = fint_from_fref(fref, p, ebrk, q, elo, ehi, eref, func=func)  ; use energies in keV here
    endif else fint = a[0]
    ed = fint * edf
  endelse 
   
;  print,'fint = ', fint
  return,  ed
  
end
