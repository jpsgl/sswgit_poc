;+
; NAME:
; EVAL_LINE_COMPLEX
;
; PURPOSE:
; This function returns a line template complex read from the input template
; file.
;
; CATEGORY:
; XRAY, GAMMA_RAYS, SPECTRUM, SPEX
;
; CALLING SEQUENCE:
; Meant to be called via line_complex.
;
; INPUTS:
;   xlo - lower bounds of energy bins
;   xhi - upper bounds of energy bins
;   template_file - template file to read parameters from.  Each file should
;     be a text file with one line per row, with each line having the
;     relative flux, line center,  and line width.
;
; INPUT KEYWORDS:
;   SIGTHRESH - number of sigmas within the midpoint to integrate each 
;     gaussian.
;
; OUTPUTS:
;   result - photons/keV for each bin.  The final result is normalized to
;            1 photon/keV.
;
; OUTPUT KEYWORDS:
;   ERR_MSG - string containing error message.  Null if no error occurred.
;   ERR_CODE - [0 / 1] if an error [did / did not] occurr.
;
; COMMON BLOCKS:
; LINE_COMPLEX_COM
;
; CALLS:
; rd_tfile
;
; WRITTEN:
; Paul Bilodeau, NASA/GSFC-RITSS, 22-June-2001 - documentation and initial 
; debugging.
;
; MODIFICATION HISTORY:
;  20-aug-2002, Paul Bilodeau - rewrote for speed, efficiency, and
;    correctness of normalization factor.  Added SIGTHRESH keyword.
;
;-
;------------------------------------------------------------------------------
FUNCTION eval_line_complex, xlo, $
                            xhi, $
                            template_file, $
                            SIGTHRESH=sigthresh, $
                            ERR_MSG=err_msg, $
                            ERR_CODE=err_code, $
                            _EXTRA=_extra

err_msg = ''
err_code = 1
result = 0.

CATCH, err
IF err NE 0 THEN BEGIN
    err_msg = !err_string
    RETURN, 0.
ENDIF

; Only bother to calculate the integrals within the user-specified
; number of sigmas.
checkvar, sigthresh, 6.

result = xlo * 0.
 
params = rd_tfile( template_file, 3, /CONVERT )

n_templates = N_Elements( params[0,*] )
use = Where( params[2,*] GT 0., n_use )

IF n_use EQ 0 THEN BEGIN
    err_msg = 'No sigmas are positive - cannot evaluate.'
    RETURN, 0.
ENDIF

FOR i=0, n_use-1 DO BEGIN
    idx = use[i]
    zlo = ( xlo - params[1,idx] ) / params[2,idx]
    zhi = ( xhi - params[1,idx] ) / params[2,idx]
    zuse = Where( Abs(zlo) LE sigthresh AND Abs(zhi) LE sigthresh, n_zuse )
    ez = zlo * 0.
    IF n_zuse GT 0 THEN BEGIN
        delta_g = Gaussint( zhi[ zuse ] ) - Gaussint( zlo[ zuse ] )
        delta_z = zhi[ zuse ] - zlo[ zuse ]
        ez[ zuse ] = f_div( delta_g, delta_z )
    ENDIF
    result = Temporary( result ) + params[0,idx] * ez / params[2,idx]
ENDFOR

result = f_div( result, Total( params[0,use] ) )

err_code = 0

RETURN, result

END
