;+
; NAME:
; LINE_COMPLEX
;
; PURPOSE:
; This function returns nuclear line templates weighted by input parameters.
; Based on common block variable status and keyword settings, it will look
; for template files to read.  The template files, energy edges, and
; realization of the templates on the energy edges are all stored in the
; common block LINE_COMPLEX_COM.
;
; CATEGORY:
; XRAY, GAMMA_RAYS, SPECTRUM, SPEX
;
; CALLING SEQUENCE:
; complex = line_complex( edges, [1e-3,1e-2], $
;   FILE=['template1.txt','template2.txt] )
;
; INPUTS:
; edges - 2xN energy array - bins to evaluate models on.
; ref_flux - integrated value of each complex
;
; INPUT KEYWORDS:
; FILE - name(s)/pattern(s) of template file(s) to read.
; PATH - path(s) to search for template file(s).
; UPDATE - force check on line template files.
;
; OUTPUTS:
; result - photons for each bin
;
; OUTPUT KEYWORDS:
; err_msg - string containing error message.  Null if no error occurred.
; err_code - [0 / 1] if an error [did / did not] occurr.
;
; COMMON BLOCKS:
; LINE_COMPLEX_COM
;
; CALLS:
; edge_products
; eval_line_complex
; same_data
; update_line_templates
;
; WRITTEN:
; Paul Bilodeau, NASA/GSFC-RITSS, 22-June-2001.
;
; Modification History:
;   19-aug-2002, Paul Bilodeau - changed return value to 0. in case of
;     error.  N_LINE is now calculated in update_line_templates. 
;   21-aug-2002, Paul Bilodeau - error is not returned if the function
;     is called without edges or ref_flux.  This corresponds to the
;     user wanting to know the number of line templates, forcing an
;     update, or both.
;
;-
;------------------------------------------------------------------------------
FUNCTION line_complex, edges, $
                       ref_flux, $
                       N_LINE=n_line, $
                       FILE=file, $
                       PATH=path, $
                       UPDATE=update, $
                       ERR_MSG=err_msg, $
                       ERR_CODE=err_code, $
                       _EXTRA=_extra

COMMON line_complex_com, complex_def_files, line_complexes, complex_ebins

err_msg = ''
err_code = 1

CATCH, err
IF err NE 0 THEN BEGIN
    err_msg = !err_string
    RETURN, 0.
ENDIF

n_line = N_Elements( complex_def_files )

IF n_line GT 0 THEN $
  IF complex_def_files[0] EQ '' THEN n_line = 0

need_update = Keyword_Set( update ) OR $
              n_line EQ 0 OR $
              Size( file, /TYPE ) EQ 7 OR $
              Size( path, /TYPE ) EQ 7

IF need_update THEN BEGIN
    update_line_templates, $
      FILE=file, $
      PATH=path, $
      N_LINE=n_line, $
      ERR_MSG=err_msg, $
      ERR_CODE=err_code

    IF err_code THEN RETURN, 0. ELSE err_code = 1
ENDIF

; If no input edges or reference flux, just return
IF N_Elements( edges ) EQ 0L AND $
  N_Elements( ref_flux ) EQ 0L THEN BEGIN
    err_code = 0
    RETURN, 0.
ENDIF

new_edges = N_Elements( edges ) GT 0
IF new_edges THEN BEGIN
    edge_products, edges, MEAN=em, WIDTH=ew, EDGES_2 = e2
    new_edges = 1 - same_data( e2, complex_ebins )
ENDIF ELSE edge_products, complex_ebins, MEAN=em, WIDTH=ew

IF need_update OR new_edges THEN BEGIN
    n_edges = N_Elements( em )
    elo = em - ew/2.
    ehi = em + ew/2.
    line_complexes = Fltarr( n_line, n_edges )
    FOR i=0, n_line-1 DO BEGIN
        tmp = eval_line_complex( elo, ehi, complex_def_files[i], $
          ERR_MSG=err_msg, ERR_CODE=err_code, _EXTRA=_extra )
        IF err_code THEN RETURN, 0.
        line_complexes[ i, * ] = tmp
    ENDFOR
    IF new_edges THEN complex_ebins = e2
ENDIF

checkvar, line_complexes, 0.

result = Reform( line_complexes ## ref_flux[0L:n_line-1L] )

err_code = 0

RETURN, result

END
