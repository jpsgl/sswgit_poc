;+
; NAME:
; UPDATE_LINE_TEMPLATES
;
; PURPOSE:
; This procedure looks for files that contain line templates
;
; CATEGORY:
; XRAY, GAMMA_RAYS, SPECTRUM, SPEX
;
; CALLING SEQUENCE:
; update_line_templates, FILE=['template1.txt','template2.txt]
;
; INPUT KEYWORDS:
; FILE - name(s)/pattern(s) of template file(s) to read.
; PATH - path(s) to search for template file(s).
;
; OUTPUT KEYWORDS:
; err_msg - string containing error message.  Null if no error occurred.
; err_code - [0 / 1] if an error [did / did not] occurr.
;
; COMMON BLOCKS:
; LINE_COMPLEX_COM
;
; CALLS:
; concat_dir
; curdir
; get_logenv
; loc_file
;
; WRITTEN:
; Paul Bilodeau, NASA/GSFC-RITSS, 22-June-2001 - documentation and initial 
; debugging finished.
;
; MODIFICATION HISTORY:
;   23-July-2002, Paul Bilodeau - added checking for empty strings in
;     complex_def_files variable
;
;-
;------------------------------------------------------------------------------
PRO update_line_templates, FILE=file, $
                           PATH=path, $
                           ERR_MSG=err_msg, $
	                   ERR_CODE=err_code

err_msg = ''
err_code = 1

CATCH, err
IF err NE 0 THEN BEGIN
    err_msg = !err_string
    RETURN
ENDIF

COMMON line_complex_com, complex_def_files, line_complexes, complex_ebins

check_files = N_Elements( complex_def_files ) EQ 0
IF NOT( check_files ) THEN check_files = complex_def_files[0] EQ ''

IF N_Elements( file ) GT 0 OR $
  N_Elements( path ) GT 0 OR $
  check_files THEN BEGIN
    def_path = [ get_logenv('SSWDB_XRAY'), $
                 concat_dir( $
                   concat_dir( $
                   concat_dir( get_logenv('SSW'), 'packages', /DIR ), $
                   'xray', /DIR), $
                   'dbase' ), $
                 curdir() ]

    checkvar, path, def_path 

    checkvar, file, 'line*.txt'

    complex_def_files = loc_file(file, PATH=path, COUNT=n_line, /ALL )
ENDIF

IF complex_def_files[0] EQ '' THEN BEGIN
    err_msg = 'UPDATE_LINE_TEMPLATES: No line template files found.'
    RETURN
ENDIF 

n_line = N_Elements( complex_def_files )

FOR i=0L, n_line-1L DO BEGIN
    params = rd_tfile( complex_def_files[i], 3, /CONVERT )
    params_type = Size( params, /TYPE )
    IF N_Elements( params ) MOD 3L NE 0L OR params_type LT 4 OR $
      params_type GT 5 THEN BEGIN
        err_msg = 'UPDATE_LINE_TEMPLATES: Error reading parameter list' + $
          'from file - ' + complex_def_files[i]
        RETURN
    ENDIF
ENDFOR

err_code = 0

END
