
;+
; PROJECT:
;	SDAC
; NAME: 
;	Vlth	
; PURPOSE:
;	This function returns a thermal line x-ray spectrum integrated over
;	wider channels suitable for continuum detectors
;	as a function of temperature in keV for an emission measure
;	of 1e49 cm-3 for a detector at the earth with the source
;	at the Sun.  It uses interpolation to obtain the results
;	after an initializing call.
; CATEGORY:
;	XRAY, SPECTRA
; CALLING SEQUENCE:
;	flux = vlth( energy, kt[, te6=te6, edges=edges]	
; INPUTS:
;	energy - photon energy in keV
;	kt     - plasma temperature in keV
;	
;	VLTH must first be initialized with a set of energy edges
;	
;	(the default edges are take for sxs1 using sxs_ecal, edges)
;
;	edges  - 2xn channel edges in keV, set up for a particular detector
;	te6    - optional grid of temperatures in MegaKelvin
; KEYWORDS:
;	CONT_ONLY -If set, then don't include the line fluxes in the output.
;
; OUTPUTS:
;	The function returns the flux in units of  photons/cm2/s/keV at the 
;	earth for a source at the Sun with an emission measure of 1e49 cm-3.
; OPTIONAL OUTPUTS:
;
; CALLS:
;	MEWE_KEV, LOC_FILE, CHECKVAR, EDGE_PRODUCTS
; COMMON BLOCKS:
;	VLTH
; PROCEDURE:
;	Initialize an array of photon flux as a function of temperature
;	and energy band suitable for a particular detectors.  Then interpolate
;	on the grid in future calls until the energy grid is changed.
;	Uses mewe_kev which calls mewe_spec for standard solar abundances
; RESTRICTIONS:
;
; MODIFICATION HISTORY:
;	ras, 25-apr-94
;	ras, 10-mar-95,
;	ras, 3-jun-1996, changed 11.6=>(1./.08617)
;	ras, 7-jul-1996, added protection for out of range index,
;	ras, 15-aug-1996, protect interpolation from going below zero
;	ras, 23-sep-1996, added cont_only keyword
;	VERSION 7, richard.schwartz@gsfc.nasa.gov, 13-aug-1997, improved documentation.
;-
;
function vlth, e, kt, te6=te6, edges=edges, cont_only=cont_only

common vlth, te6c, em, tflux, cont_flux
; te6c and tflux stored alog10
checkvar, cont_only, 0

if keyword_set(edges) or keyword_set(te6) or n_elements(tflux) eq 0 then begin
 	
	lsxs = loc_file('sxs_ecal',count=found_ecal)
	if found_ecal ge 1 then call_procedure, 'sxs_ecal', echan1
	checkvar, edges, echan1, .1*findgen(201)
	checkvar,te6, 10^( findgen(100)*.024+5.7 -6.0)
	tflux = float(mewe_kev(te6, edges, /kev,/earth,cont=cont_flux, /photon,/edges))
	for j=0,n_elements(tflux(*,0))-1 do begin
	wmin  = where( tflux(j,*) lt (1e-30*max(tflux(j,*))>1e-30), nmin )
	if nmin ge 1 then begin
		tflux(j,wmin(0):*) = -30.0 
		cont_flux(j,wmin(0):*) =-30.0 
	endif
	endfor
	tflux(where(tflux gt 0.0)) = 5 + alog10(tflux(where(tflux gt 0.0)))

	cont_flux(where(cont_flux gt 0.0)) = 5 + alog10(cont_flux(where(cont_flux gt 0.0)))
	te6c= alog10(te6)
	edge_products, edges, mean=em
	return,0.0
endif

lkt = alog10(kt*(1./.08617))
mkt = min(abs(te6c- lkt(0)), nkt)
nkt = fix(nkt)

f0 = (tflux(nkt,*)*(1.0-cont_only)+cont_only*cont_flux(nkt,*))
f1 = (tflux(nkt-1 > 0,*)*(1.0-cont_only)+cont_only*cont_flux(nkt-1 > 0,*))

f  = f0 - (f1-f0)/(te6c(nkt)-te6c(nkt-1>0)) * (lkt(0)-te6c(nkt))

;stop
result = interpol( 10^f(*), em(*), e(*)) > 0.0

return, result

end
