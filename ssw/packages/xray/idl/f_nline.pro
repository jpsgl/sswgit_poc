;+
; NAME:
;   F_NLINE
;
; PURPOSE:
;   Calculate the sum of several normalized gaussian functions, based
;   on the input parameter vector.
;
; CALLING SEQUENCE:
;   result = f_nline( energy_bins, [1e3,250.,15.,1e4,400.,10.], nline=nline )
;
; INPUTS:
;   energy_bins - energy values on which the function is evaluated.  If a 2 x N
;       array, the e[0,*] are the lower bounds and the e[1,*] are the
;       upper bounds of the bins, and the mean values are calculated.
;   parameters - vector of line parameters:
;     The index i varies from 0 to the number of lines-1, where the number of
;     lines is equal to the number of input parameters divided by 3.
;     parameters[i*3]   - amplitudes of gaussian lines.
;     parameters[i*3+1] - centroids of gaussian lines.
;     paramaters[i*3+2] - sigmas of gaussian lines.
; 
; OUTPUTS:
;   result - sum of the gaussian lines.
;
; OUTPUT KEYWORDS:
;   NLINE - number of gaussian lines used in the calculation.
;
; CALLS:
;   f_div
;   f_gauss_intg
;
; WRITTEN:
;   Paul Bilodeau, 21-aug-2002, paul.bilodeau@gsfc.nasa.gov - based on
;     code from f_bpow_nline by Richard Schwartz.
;
; MODIFICATION HISTORY:
;  Paul Bilodeau, 5-nov-2002 - replaced gaussian with f_gauss_intg
;
;-
;
;------------------------------------------------------------------------------
FUNCTION f_nline, energy_bins, parameters, NLINE=nline, _REF_EXTRA=_ref_extra

npar = n_elements( parameters )

nline = 0

IF NOT( npar MOD 3 EQ 0 AND npar GT 0 ) THEN BEGIN
    MESSAGE, 'Invalid parameter array. ' + $
             'Must have 3*nline elements in parameters.', $
             /CONTINUE
    RETURN, 0.
ENDIF

n_energy_bins = Size( energy_bins, /N_DIMENSIONS ) EQ 2 ? $
                N_Elements( energy_bins ) / 2 : $
                N_Elements( energy_bins )

line = Fltarr( n_energy_bins )

nline = npar / 3

w2 = Indgen(2)
FOR i=0,nline-1 DO BEGIN
    IF parameters[i*3] NE 0.0 THEN BEGIN
        line = Temporary( line ) + $
               f_gauss_intg( energy_bins, $
                             [ f_div( parameters[i*3]/sqrt(2*!pi), $
                               parameters[2+i*3]),  $
                               parameters[1+i*3+w2 ] ], $
                             _EXTRA=_ref_extra $
                           )
    ENDIF
ENDFOR

RETURN, line

END
