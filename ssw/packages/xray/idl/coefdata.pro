;+
; PROJECT:
;	XRAY
;
; NAME:
;	COEFDATA
;
;
; PURPOSE:
;	This procedures reads in coefficients needed to calculate xray xsections
;	from 1-1000 keV.  Coefficients are taken from McMaster et al 1968.
;
; CATEGORY:
;	SPECTRA, XRAYS, XRAY RESPONSE, xray analysis,
;
;
; CALLING SEQUENCE:
;	COEFDATA, Coef, $
;		ZNAMES=znames, $
;		xcom_obj = xcom_obj, $
;		ERROR=ERROR, RESET=RESET
;
;
; CALLED BY: xcross
;
;
; CALLS:
;	none
;
; INPUTS:
;       none explicit, only through commons;
;
; OPTIONAL INPUTS:
;
;
; OUTPUTS:

;
; OPTIONAL KEYWORD OUTPUTS:
;	coef - structure with needed coefficients see explanation of tag fields below
;		From the 1st extension in $SSWDB_XRAY/xray_coef.fits
;		First extension taken from
;		McMaster W H, Del Grande N K, Mallett J H and Hubbell J H 1969, 1970 Compilation of x-ray cross sections
;		Lawrence Livermore National Laboratory Report UCRL-50174 (section I 1970, section II 1969, section III
;		1969 and section IV 1969)
;	znames - structure with element Z, 2 letter names, and full names, see explanation below
;		From the 2nd extension in $SSWDB_XRAY/xray_coef.fits
;		Source - http://physics.nist.gov/PhysRefData/XrayMassCoef/tab1.html
;	xcom_obj - Object for retrieving xcom data structures for cross-sections from 1 keV to 1e5 MeV (100 GeV)
; EXPLANATION FOR TAG_NAMES
;	Z:	ATOMIC NUMBER
;	AT_DEN:	ATOMIC DENSITY - DENSITY IN UNITS OF 10^24 ATOMS/CC
;	CONV:	CONVERSION CONSTANT - CROSS_SECTION(CM2/GM)*CONV=CS(BARNES/ATOM)
;	EDGE:	SHELL EDGES IN KEY
;	PE  :   COEFFICIENTS FOR PHOTOELECTRIC CROSS SECTION
;	SCC :	COEFFICIENTS FOR COHERENT SCATTERING (RAYLEIGH)
;	SCI :	COEFFICIENTS FOR INCOHERENT SCATTERING (COMPTON)
; Explanation of tag names in Znames
;** Structure <782a278>, 6 tags, length=40, data length=38, refs=1:
;   Z               INT              1      ;Atomic Number
;   NAME            STRING    'H '			;1 or 2 letter name of element
;   FULL_NAME       STRING    'Hydrogen    ';full name of element
;   Z_OVER_A        FLOAT          0.992120 ;ratio of Atomic Numuber to Atomic Mass Number
;   XCIT_ENERGY     FLOAT           19.2000 ;excitation energy
;   DENS            FLOAT      8.37500e-005 ;
; XCOM_OBJ - interact through READ_MAT_XCOM
; 	or
;		xcom_obj = obj_new('xcom') ;done internally here
;		then
;		list = xcom_obj->Get(/list_material) ;126 compound/element names with x-section info
;		xcom_data = xcom_obj->GetData( LIST[I] ) ; get the xsection table and density for one of the 126 elements
;		help, xcom_data,/st
;		xcom_data= xcom_obj->getdata(mat='lead')
;		help, xcom_data,/st
;		** Structure <252fca8>, 3 tags, length=15688, data length=15688, refs=3:
;		   XSUBS           BYTE      Array[4]
;		   XDENS           FLOAT           11.3400
;		   XDAT            FLOAT     Array[490, 8]

; KEYWORDS:
;	ERROR - If set can't find or read data file.
; COMMON BLOCKS:
;	xrcoef,n,z,ad,conversion,edge,pearr,scc,sci
;
; SIDE EFFECTS:
;	none
;
; RESTRICTIONS:
;	87 elements from 1-94
;	missing z=[83,85,87,88,89,91,93]
; PROCEDURE:
;	none
; REFERENCES:
;   For a review of x and gamma ray x-section compilations:
;		INSTITUTE OF PHYSICS PUBLISHING PHYSICS IN MEDICINE AND BIOLOGY
;		Phys. Med. Biol. 51 (2006) R245�R262 doi:10.1088/0031-9155/51/13/R15
;		REVIEW
;		Review and history of photon cross section
;		calculations*
;		J H Hubbell
;		National Institute of Standards and Technology, Ionizing Radiation Division, Mail Stop 8463,
;		100 Bureau Drive, Gaithersburg, MD 20899-8463, USA
;		E-mail: john.hubbell@nist.gov
;		Received 22 February 2006, in final form 12 April 2006
;		Published 20 June 2006
;		Online at stacks.iop.org/PMB/51/R245
; MODIFICATION HISTORY:
;	documented, ras, 20-dec-94
;	ras, 12-jan-95, include conv_vax_unix for unix
;	ras, 16-jan-96, include conv_vax_unix for unix for n
;	Version 4, ras, 8-apr-97, add SSWDB_XRAY to data file search and ERROR.
;       Version 5, richard.schwartz@gsfc.nasa.gov, 24-jul-1997, removed PERM_DATA.
;       Version 6, richard.schwartz@gsfc.nasa.gov, 8-feb-1999, add call to use_vax_float.
;	Need a more permanent solution, e.g. a FITS file!
;	Version 7, richard.schwartz@gsfc.nasa.gov, 15-may-2000, added explanation
;	of missing datafile problem.
;	27-dec-2011, richard.schwartz@nasa.gov, moved data to FITS file, xray_coef.fits for
;		standardization and cross-platform reliability
;-

PRO COEFDATA, COEF, $
	ZNAMES=znames, $
	xcom_obj = xcom_obj, $
	ERROR=ERROR, RESET=RESET
;
;READ IN THE XRAY CROSS SECTION COEFFICIENTS FROM MCMASTER ET AL
;
common xrcoef, n, z, ad, conversion, edge, pearr, scc, sci, znamesav, xcom_obj_sav
;
;A={COEF,Z:0.0,AT_DEN:0.0,CONV:0.0,EDGE:FLTARR(5),PE:FLTARR(4,4),$
;	SCC:FLTARR(4), SCI:FLTARR(4)}
;EXPLANATION FOR TAG_NAMES
;	Z:	ATOMIC NUMBER
;	AT_DEN:	ATOMIC DENSITY - DENSITY IN UNITS OF 10^24 ATOMS/CC
;	CONV:	CONVERSION CONSTANT - CROSS_SECTION(CM2/GM)*CONV=CS(BARNES/ATOM)
;	EDGE:	SHELL EDGES IN KEY
;	PE  :   COEFFICIENTS FOR PHOTOELECTRIC CROSS SECTION
;	SCC :	COEFFICIENTS FOR COHERENT SCATTERING (RAYLEIGH)
;	SCI :	COEFFICIENTS FOR INCOHERENT SCATTERING (COMPTON)

;file = loc_file(path=[curdir(),'SSWDB_XRAY'],'coefdata.dat', count=count)
if n_elements(coefsav) eq 0 or keyword_set(reset) then begin
	file = loc_file(path=[curdir(),'SSWDB_XRAY'],'xray_coef.fits', count=count)
	error = 1
	if count  eq 0 then begin
		print, '!!!!!!!!!!!!!!!! Warning. xray_coef.fits not found.'
		print, '!!!!!!!!!!!!!!!! This file should be found in SSWDB_XRAY'
		print, '!!!!!!!!!!!!!!!! Your definition of SSWDB_XRAY is ',getenv('SSWDB_XRAY')
		print, '!!!!!!!!!!!!!!!! The normal location of SSWDB_XRAY is $SSW/packages/xray/dbase or $SSW_XRAY/dbase.'
		return
		endif

	;openr,lu1,/get, file(0)
	;n=0
	;readu,lu1,n
	;coef=replicate(a,conv_vax_unix(n))
	;readu,lu1,coef
	;free_lun,lu1
	;coef = conv_vax_unix(use_vax_float(/old2new,coef))

	coefsav = mrdfits( file[0],1,/silent)
	znamesav = mrdfits( file[0],2,/silent)
	xcom_obj_sav = obj_new('xcom')
	;;LOAD THE COMMON BLOCK

	z =	 	coefsav.z
	ad =	 	coefsav.at_den
	conversion = 	coefsav.conv
	edge =   	coefsav.edge
	pearr = 	coefsav.pe
	scc = 		coefsav.scc
	sci = 		coefsav.sci

	n = n_elements(coefsav)
	endif
coef = coefsav
znames = znamesav
xcom_obj = xcom_obj_sav
error = 0
END
