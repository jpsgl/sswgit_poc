;+
; PROJECT:
;       HESSI
; NAME:
;	F_LINE_ASYM
;
; PURPOSE:
;	This function returns a single asymmetric Gaussian function (a0 is normalized)
;
; CATEGORY:
;	E2 - CURVE AND SURFACE FITTING.
; CALLS:
;	F_GAUSS_INTG
; CALLING SEQUENCE:
;	out = F_LINE_ASYM(X,A)
;
; INPUTS:
;	X = VALUES OF INDEPENDENT VARIABLE, 2 X N, LO AND HI EDGES
;	A = PARAMETERS OF GAUSSIAN FUNCTION
;       A[0] - integrated intensity
;       A[1] - x value of peak
;       A[2] - sigma for x < A[1]
;       A[3] - sigma for x > A[1]
;       (see F_GAUSS_INTG, GAUSS_INTG for more explanation)
;       
; INPUT KEYWORD:       
;   DO_CALC - if set to 0, return an array of zeroes of the correct size (default is 1)
;   
; OUTPUTS:
;	Function values
;
; OPTIONAL OUTPUT PARAMETERS:
;
; COMMON BLOCKS:
;	NONE.
; SIDE EFFECTS:
;	NONE.
; RESTRICTIONS:
;	NONE.
; WRITTEN: Kim Tolbert 11-Apr-2014 
;-
function f_line_asym, x_in, a, do_calc=do_calc, _extra=_extra

checkvar, do_calc, 1

x = get_edges(x_in, /edges_2)  ; if edges don't come in as 2xn, make them 2xn

if keyword_set(do_calc) then begin
  ; normalize a[0] to the width of the gaussian
  a0 = f_div (a[0] / sqrt(2*!pi), avg([a[2],a[3]]) )
  a1 = a[1]

  nx = n_elements(x[0,*])
  f = fltarr(nx)

  ; q1, k1 will be indices and number of elements for x<peak, use first sigma
  ; q2, k2 will be indices and number of elements for x<peak, use second sigma
  ; qp will be index of x bin containing peak value
  qp = value_locate(x[0,*], a1)
  q1 = -1 & k1 = 0
  q2 = -1 & k2 = 0
  case 1 of
    qp eq -1: begin  ; all values of x are > a1, so use second sigma
      q2 = indgen(nx)
      k2 = nx
      end
    qp eq (nx-1): begin  ; all values of x are < a1, so use first sigma
      q1 = indgen(nx)
      k1 = nx
      end
    else: begin          ; find indices before and after x=a1
      q1 = indgen(qp)
      k1 = n_elements(q1)
      q2 = qp + 1 +indgen(nx-qp-1)
      k2 = n_elements(q2)
      end
  endcase
  
  if k1 gt 0 then f[q1] = f_gauss_intg( x[*,q1], [a0, a1, a[2]] )
  if k2 gt 0 then f[q2] = f_gauss_intg( x[*,q2], [a0, a1, a[3]] )
  
  ; if k1 or k2 is 0, we've already calculated f for all indices with appropriate sigma, so return
  if k1 eq 0 or k2 eq 0 then return, f

  ; For the bin containing a1, break into two bins, one from beginning of bin to a1, and one from a1 to end of bin,
  ; calculate f_gauss_intg for each with first and second sigma, then combine the two by forming weighted average
  xp = x[*,qp]
  fp1 = f_gauss_intg( [xp[0],a1], [a0, a1, a[2]] )
  fp2 = f_gauss_intg( [a1,xp[1]], [a0, a1, a[3]] )
  fp = ( (a1-xp[0]) * fp1 + (xp[1]-a1) * fp2 ) / (xp[1]-xp[0])

  ; combine arrays from 3 sections to form result 
  return, [f[q1], fp, f[q2]]
  
endif else return, fltarr(n_elements(x[0,*]))  ; return array of zeroes

end