;+
; NAME:  
;             thick
; PURPOSE:
;             compute nonthermal thick-target electron power above a 
;             low-energy cutoff
; CATEGORY:  
;             spectra, xrays
; CALLING SEQUENCE: 
;             pow=thick(a1,a2,da1,da2,dpow,b1,b2,em,cut=cut,step=step)
; INPUTS:    
;             a1 = fitted power-law amplitude of HXR photon spectrum
;             a2 = spectral index of photon spectrum
; OPTIONAL INPUTS:
;             da1 = uncertainty on a1
;             da2 = uncertainty on a2
;             em  = mean energy of photons [I = a1*(e/em)^(-a2) ]
; KEYWORDS:
;             cut = electron low-energy cutoff (def = 20 keV)
;             step = % step size for computing numerical derivatives
; OUTPUTS:     
;             pow = total power of electrons above cutoff (10^20 ergs/s)
; OPTIONAL OUTPUTS:
;             b1 = log_10(amplitude of electron power-law spectrum)
;             b2 = spectral index of electrons 
;             dpow = uncertainty of pow
; PROCEDURE:                         
;             uses well-known formulae of Brown (1971) etc.
; MODIFICATION HISTORY:                           
;             written DMZ (ARC) Aug'91
;             added abundance factor of 1.6 and removed double
;             precision
;             ras and dmz 12-apr-03
;-

function thick,a1,a2,da1,da2,dpow,b1,b2,em,cut=cut,step=step

if n_elements(cut) eq 0 then cut=20.
if n_elements(em) eq 0 then em=50.           
if n_elements(step) eq 0 then step=1.
em=float(em) & cut=float(cut) & a2=float(a2) & a1=float(a1) 
   
z=beta(a2-.5,.5)
b1=alog10(1.6*3.e33)+alog10(a1)+a2*alog10(em)+alog10(a2*(a2-1)^2*z)
b2=a2+1.                        
log_pow=b1+(2.-b2)*alog10(cut)-alog10(b2-2.)+alog10(1.6e-9)
pow=10.^(log_pow-20.)

;-- use recursion to compute errors on power (no guarantee that this is right!)

if (n_elements(da1) ne 0) and (n_elements(da2) ne 0) then begin
 h=step/100.
 dpow_da1=(thick(a1+a1*h,a2,cut=cut)-thick(a1-a1*h,a2,cut=cut))/2./a1/h
 dpow_da2=(thick(a1,a2+a2*h,cut=cut)-thick(a1,a2-a2*h,cut=cut))/2./a2/h
 dpow=abs(dpow_da1*da1+dpow_da2*da2)
endif

return,pow & end

