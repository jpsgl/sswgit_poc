;+
;
; NAME: F_THICK_NUI
;
; PURPOSE: This function returns the nonthermal bremsstrahlung X-ray spectrum from a thick target with step-function or 
;  linear-function ionization profiles. Relativistic energy loss and full cross section are included. 
;  Based on Thick2 function
;
; CATEGORY:
;       SPECTRA, XRAYS
;
; CALLING SEQUENCE:
;       Flux = F_THICK_NUI( E, A )
;
; CALLS:
;      Brm2_NUI
;
; INPUTS:
; E -energy vector in keV, 2XN edges or N mean energies
; A -model parameters defined below
;   a(0) - Total integrated electron flux, in units of 10^35 electrons sec^-1.
;   a(1) - Power-law index of the electron distribution function.
;   a(2) - E* in step-function model or E1 in linear-function model (in keV);  set 0 for fully ionized thick target.
;   a(3) - E0-E1 in linear-function model (in keV); set 0 for step-function model.
;   a(4) - Low energy cutoff in the electron distribution function (in keV).
;   a(5) - High energy cutoff in the electron distribution function (in keV).
;
; MODIFICATION HISTORY:
; Kim Tolbert 24-Mar-2004
; July 2011,   Yang Su.      Modified f_nui.pro for NUI.
;-

function f_thick_nui, e, a, _extra=_extra
if (size(e))(0) eq 2 then edge_products, e, mean=em else em=e
return, 1.d35*Brm2_NUI(em, a)

end