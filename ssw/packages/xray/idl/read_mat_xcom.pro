;+
; PROJECT:
;  SDAC
; NAME:
;   Read_mat_xcom.pro
; CALLED PROCEDURES:
;	LOC_FILE, CURDIR
; CATEGORY:
;	XRAY RESPONSE
; PURPOSE:
;   Fast access to Partial Interaction/Total Attenuation Coefficients for
;   96 elements and various compounds commonly used as detectors created
;   by the SDAC version of XCOM software by J. Berger.  Programs accesses
;   the idl xdr, stream file perm_data:xcom_info.dat which is created by
;   running the idl program xcom_data.
;
; INPUT KEYWORD PARAMETERS:
;   IN_MAT -  The variable you are looking for (STRING)
;   xfile - For testing purposes, will look at a user specified xcom_info.dat
; OUTPUTS:
;   xdens - The density of the searched for substance
;   xsubs - The string of the searched for substance
;   xdat - the array created for the searched substance by XCOM
;          xdat is generally an ~550 x 8 floating point array
;          where:
;          Xdat(*,0) = Photon Energy at which coefficients are calculated (MeV)
;          xdat(*,1 & 2) = Coherent and Incoherent Scattering coeff. (cm^2/g)
;          Xdat(*,3) = photoelectric absorption (cm^2/g)
;          Xdat(*,4 & 5) = Pair production in nuclear and electron field (cm^2/g)
;          Xdat(*,6 & 7) = Total attenuation with and without coherent
;                          scattering (cm^2/g)
;
; OUTPUT KEYWORDS:
;   materials - The string array containing all the substances for which
;               xcom currently has a .SAV file
;   finderror - 1 if read_mat_xcom fails to find a substance
;   ERROR - If set, can't find database file.
; SIDE EFFECTS:
;   none at present
; RESTRICTIONS:
;   The SDAC has created save files for 96 elements and 30 compounds
;   The "xdat" array data for other compounds can be obtained by running
;   SDAC's version of XCOM:
;   Start and IDL session, at the IDL> prompt run:
;   Ixcom,X=xdat,dens=xdens,xsubs
;   IXCOM will provide a menu with instruction after that point.
;
;   Protoactinium (91), Neptunium (93), Berkelium (97), Einsteinium (99),
;    and all elements past 100 are not available interactively or through
;    this program.
;
; MODIFICATION HISTORY
;   Written by Richard Schwartz on June 20, 1995
;   EAC changed spelling of Praseodymium.sav to correct, had to change
;   size of bytarr to 12 x 126.
;   Version 3, RAS, 8-apr-97, added error and used loc_file with SSWDB_XRAY
;   Version 4, RAS, 15-apr-97, added free_lun!
;   Version 5, richard.schwartz@gsfc.nasa.gov, 24-jul-1997, removed PERM_DATA.
;	Version 6, ras, 18-nov-2006, uses FITS file created from original database
;	Kim Tolbert, 1-Feb-2007, Change file to infile, for use in error message
;-

Pro read_mat_xcom, in_mat=in_mat,  materials=materials, finderror=finderror,$
                   xdens,  xsubs, xdat, xfile=xfile, error=error


; open file xcom_info.dat which contains the info for all 126
; substances, created by xcom_data.pro
finderror = 0

if keyword_set(xfile) then begin
	infile = xfile
	file = loc_file(infile, count=count)
endif else begin
	infile = 'xcom.fits'
	file = loc_file(path=[curdir(),'SSWDB_XRAY'],infile,count=count)
endelse

if count eq 0 then begin
	error=1
     	message, /cont, 'Error.  Cannot find ' + infile
     	print, "Check that you've included xray as an SSW instrument and SSWDB_XRAY is set correctly."
	return
endif
error = 0
common xcom_common, bmaterial

if n_elements(bmaterial) ne 126 then begin
	bmaterial =mrdfits( file[0],/silent) ;basic data array, materials in extensions

; bmaterial contains the ALL CAPS titles of .SAV files (i.e., BORON.SAV,
; ZNSE.SAV), .SAV is not included in bmaterial

	bmaterial = strcompress(bmaterial,/remove)
	endif
materials = bmaterial

checkvar,in_mat,'TUNGSTEN'

wmat = (where( strupcase(in_mat) eq strupcase(materials), nmat))(0)

if nmat eq 1 then begin   ;read into the file until desired substance
	out = mrdfits( file[0],wmat[0]+1,/silent)
	xsubs = strcompress(out.xsubs,/rem)
	xdat  = out.xdat
	xdens = out.xdens
endif else begin
  Message,/cont,'Program has not found a match, please check your specified substance'
  finderror = 1
endelse
end
