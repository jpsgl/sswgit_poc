;+
; NAME: 
;   F_BPOS_NLINE
;	
; PURPOSE: 
;   broken power-law function with/without discontinuities in the
;   derivatives + gaussian line function
;	
; CALLING SEQUENCE: 
;   result = f_bpos_nline(x, a)
;		
; INPUTS:
;   x - independent variable, nominally energy in keV under SPEX
;   a - parameters describing the broken power-law, 511 keV line
;       with positronium fraction, and n gaussians
;
;       a[0] - normalization at epivot
;       a[1] - negative power law index below break
;       a[2] - break energy
;       a[3] - negative power law index above break
;
;       a[4] - 511 keV line flux, normalized to 1 photon/cm^2
;       a[5] - Positronium fraction
;       a[6] - 511 keV line FWHM
;	a[7] - 511 Kev line centroid
;
;       a[8+3n] - integrated line intensity of line n(<3)
;       a[9+3n] - line centroid
;       a[10+3n] - line sigma
;
; OUTPUTS:
;   result of function, a broken power law, 511 line with positronium 
;   continuum, and gaussian lines
;
; OUTPUT KEYWORDS:
;   NLINE - number of gaussian lines in model
;
; CALLS:
;   checkvar
;   f_bpow
;   f_posit_wfrac
;   f_nline
;	
; WRITTEN:
;   Paul Bilodeau, 20-aug-2002 - based of f_bpow_nline by Richard Schwartz, 
;     reorganized with new procedures, particularly f_posit_wfrac.	
;-
;
FUNCTION f_bpos_nline, x, a, NLINE=nline

nline = 0

checkvar, a, Fltarr(8)

IF N_Elements(a) GT 8 THEN $
  RETURN, f_bpow( x, a[0:3] ) + $
          f_posit_wfrac( x, a[4:7] )  + $
          f_nline( x, a[8:*], NLINE=nline )

RETURN,  f_bpow( x, a[0:3] ) + f_posit_wfrac( x, a[4:7] )

END
