;+
;
; NAME:
;	xsec
;
; PURPOSE:
;	This function calculates xray xsections from 1-1000 keV.
;
;
; CATEGORY:
;	spectra, xrays, detector response
;
; CALLING SEQUENCE:
;        xray_cross_section = xsec(energy,z,type, cm2pergram=cm2per, barns_atom=barns)
;
; CALLED BY:
;
;
; CALLS:
;	xcross
;
; INPUTS:
;       energy - an array or scalar from 1-1000. in units of keV
;	z      - a scalar from 1-94, the atomic number of the element for which
;		 the cross-section is desired
;		 missing z=[83,85,87,88,89,91,93]
;	type   - a string used to choose the specific cross-section
;		 'pe' - photoelectric cross-section
;		 'si' - Inelastic Scattering Cross-section, Compton Scattering
;		 'sc' - Rayleigh Scattering
;		 'ab' - Total Absorption, some of all 3 above
;
; OPTIONAL INPUTS:
;	cm2pergram - if set, then cross section returned in units of cm^2/gm
;       barns_atom - if set, then cross section returned in units of barns/atom
;
; OUTPUTS:
;       xray cross section in units of 1/cm
;
; OPTIONAL OUTPUTS:
;	error - if an invalid argument is given, this is set to 1
;
; COMMON BLOCKS:
;	xrcoef - removed 28-dec-2011, ras, access coef data structure through coefdata
; CALLS:
;	COEFDATA, XCROSS
; SIDE EFFECTS:
;	none
;
; RESTRICTIONS:
;	energy is restricted to gt 1 and lt 1000 keV or an error is generate.
; PROCEDURE:
;	none
;
; MODIFICATION HISTORY:
;	documented, ras, 20-dec-94
;	28-dec-2011, ras, modernized with call to coefdata and use of [] instead of ()
;-
function xsec,energy,z,type, cm2pergram=cm2per, barns_atom=barns, error=error

on_error,2
error = 1

;xsec in cm-1 at energy e for el.,type='pe,sc,si,ab'
;return in cm2 per gram if keyword cm2 is set

;common xrcoef, n, z, ad, conversion, edge, pearr, scc, sci ;commented 28-dec-2011
;all reference to data in common block is now through call to coefdata
coefdata, coef

e=energy
we    = where( energy le 1. or energy ge 1000., num_energies)
if num_energies ge 1 then begin
	message,'ERROR, Energy out of range.  Restricted to >1 to <1000 keV'
	return,0
endif
sizee=size(e) &if sizee[0] eq 0 then e=e+fltarr(1);array or constant

ans=fltarr(n_elements(e))

nz=where(coef.z eq z, num)
cf = coef[nz]
if num ne 1 then begin
	print,'Z = ', z
	message,'ERROR, atomic number must be a scalar, 1-94 exclusive of 83,85,87,88,89,91,93.'
	return,0
endif


utype = strupcase(type)
wtype = where( utype[0] eq ['AB','PE','SC','SI'], ntype)
if ntype ne 1 then begin
	print,'Type = ',utype
	message,'ERROR, Type must be PE, AB, SC, or SI.'
	return, 0
endif

if ((utype eq 'PE') or (utype eq 'AB')) then begin
  ;determine energy ranges
  for i=0,3 do begin
  we=where( (e ge cf.edge[i]) and (e lt cf.edge[i+1]), nume)
  if nume gt 0 then  ans[we]=xcross(e[we],cf.pe[*,i])+ans[we]
  endfor
endif
if ((utype eq 'SC') or (utype eq 'AB')) then ans=xcross(e,cf.scc)+ans
if ((utype eq 'SI') or (utype eq 'AB')) then ans=xcross(e,cf.sci)+ans

if keyword_set(cm2per) then ans=ans/cf.conv else $
	if keyword_set(barns) then ans=ans else ans=ans* cf.at_den

if sizee[0] eq 0  then ans=ans[0]  	;if energy comes in a scalar, the result is a scalar
return, ans
end

