;+
; PROJECT:
;	XRAY
;
; NAME:
;	XRAY_KEDGE
;
;
; PURPOSE:
;	Return the K-edge energy in keV of the chosen element of the 87
;		available from McMaster et al 1969 (see coefdata)
; CATEGORY:
;	xray analysis
;
;
; CALLING SEQUENCE:
;	Kedge = xray_kedge( arg )
;
; EXAMPLES:
;	Kedge = xray_kedge( 32 )
;	Kedge = xray_kedge( 'ge' )
;	Kedge = xray_kedge( 'germanium' )
;	print, kedge
;		11.1040 ;in keV
;
; CALLED BY:
;
;
; CALLS:
;	none
;
; INPUTS:
;       none explicit, only through commons;
;
; OPTIONAL INPUTS:
;
;
; OUTPUTS:

;
; OPTIONAL OUTPUTS:
; KEYWORDS:
;	ERROR - If set can't find the element Z or string
; COMMON BLOCKS:
;
; SIDE EFFECTS:
;	none
;
; RESTRICTIONS:
;	87 elements from 1-94
;	missing z=[83,85,87,88,89,91,93]
; PROCEDURE:
;	Uses structures COEF and ZNAMES from coefdata.pro to identify the
;	the structure element in COEF containing the element and therefore the kedge value as well.
; REFERENCES:
;		K edges taken from
;		McMaster W H, Del Grande N K, Mallett J H and Hubbell J H 1969, 1970 Compilation of x-ray cross sections
;		Lawrence Livermore National Laboratory Report UCRL-50174 (section I 1970, section II 1969, section III
;		1969 and section IV 1969)

;   For a review of x and gamma ray x-section compilations:
;		INSTITUTE OF PHYSICS PUBLISHING PHYSICS IN MEDICINE AND BIOLOGY
;		Phys. Med. Biol. 51 (2006) R245�R262 doi:10.1088/0031-9155/51/13/R15
;		REVIEW
;		Review and history of photon cross section
;		calculations*
;		J H Hubbell
;		National Institute of Standards and Technology, Ionizing Radiation Division, Mail Stop 8463,
;		100 Bureau Drive, Gaithersburg, MD 20899-8463, USA
;		E-mail: john.hubbell@nist.gov
;		Received 22 February 2006, in final form 12 April 2006
;		Published 20 June 2006
;		Online at stacks.iop.org/PMB/51/R245
; MODIFICATION HISTORY:

;	27-dec-2011, richard.schwartz@nasa.gov,
;-
function xray_kedge, z, error=error
error = 1
zi = z[0]
coefdata, c, zname=zname
if ~is_number(zi) then begin
	tz = where( stregex(/fold,/boo, (strlen(zi) le 2 ? zname.name : zname.full_name), zi), nz)
	if nz eq 0 then begin
		message,/conti,'Element not found'
		return, 0.0
		endif
	zi = zname[tz[0]].z
	endif

tz = where( c.z eq zi, nz)
if nz eq 0 then begin
	message,/conti,'Z not found'
	return, 0.0
	endif
error = 0
return, c[tz[0]].edge[3]
end