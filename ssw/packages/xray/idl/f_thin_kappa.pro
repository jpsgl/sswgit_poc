;+
;
; NAME: F_THIN_KAPPA
;
; PURPOSE: This function returns thin-target bremsstrahlung X-ray
; spectrum seen at the Earth assuming electron distribution in 
; a form of kappa distribution. It is based on brm_bremspec.pro,
; brm_dmlin.pro, brm_fthin.pro.
;
;
; CATEGORY:
;       SPECTRA, XRAYS
;
; CALLING SEQUENCE:
;       Flux = F_THIN_KAPPA( energy_edges, params )
;
; CALLS:
;       EKAPPA, BRM_GAULEG, BRM_BREMCROSS
;
; INPUTS:
; energy_edges - energy vector in keV, 2XN edges or N mean energies
; params - model parameters defined below
; params: params[0] : "emission measure" (in 1d49 cm^-3, i.e. ambient proton density * volume * electron density in kappa distribution )
;         params[1] : temperature (in keV)
;         params[2] : kappa index 
;         params[3] : high-energy cutoff (in keV)
;
; MODIFICATION HISTORY:
; Jana Kasparova, 16-Jan-2009
;-
function f_thin_kappa,energy_edges,params,_extra = _extra
  if ((size(energy_edges))[0] eq 2) then edge_products,energy_edges, mean = en else en = energy_edges

  ; constants
  mc2 = 510.98d+00
  clight = 2.9979d+10
  au = 1.496d+13
  z = 1.2d0 ; average atomic number
  ; conversion from keV to K
  keV2K = 1.16d7
  f1 = params[0]
  t = params[1] * keV2K
  kappa_index = params[2]
  ehigh = params[3]
  
  
  maxfcn = 2048 ; maximum number of points for the Gaussian quadrature integration

  fcoeff = (clight/(4.d0*!dpi*(au^2)))/mc2^2;  numerical coefficient for the photon flux
  ; 1/mc2 is due to cross-section
  ; clight/mc2 is due to BETA (= speed / clight) multiplication

  rerr = 1d-4                   ;relative error
  nen = n_elements(en)
  intsum = dblarr(nen)          ; integral sum
  nlim = 12 ; maximum possible order of the Gaussian quadrature integration is 2^nlim.
  conv = 0b ; convergence flag
  for ires = 1, nlim do begin
    npoint = 2L^ires
    
; abscissas and weights
    x = dblarr(nen,npoint)
    w = dblarr(nen,npoint)
    ; brm_gauleg
    brm_gauleg,en,ehigh,npoint,x,w
    lastsum = intsum

    ph_en_array = rebin(en,nen,npoint)
; cross section is 2D array according to x,w
    brm_bremcross,x,ph_en_array,z,cross
; kappa distribution - particle density, not flux !

    fcn = ekappa(x,[t,kappa_index])
    gamma = x / mc2 + 1.d0

; multiplication by BETA = speed / clight
    integrand = fcn * cross * sqrt(x * (x + 2.d0 * mc2)) / gamma
; integration
    intsum = total(w * integrand,2,/double)
; compute relative error 
    diff = abs(intsum - lastsum) / intsum
    ind = where(diff gt rerr)
    if (ind[0] eq -1) then begin
      conv = 1b
      break
    endif
  endfor

if (conv ne 1b) then begin
  print,'f_thin_kappa: Not converged'
endif

flux = 1d49 * f1 * intsum * fcoeff
return, flux
end
