;+
; NAME:
;   F_POSITRONIUM
;
; PURPOSE:
;   Function wrapper for positronium procedure.  Calculates 511 keV
;   line with positronium continuum.
;
; CALLING SEQUENCE:
;   result = f_positronium( energy_bins, parameters )
;
; INPUTS:
;   energy_bins - energy bins on which the function is evaluated.  If a vector,
;       these are interpreted as the mean values of the bins and the
;       bins are calculated to be the mean values plus or minus the
;       widths of the bins.  If a 2 x N array, the e[0,*] are the
;       lower bounds and the e[1,*] are the upper bounds.
;   parameters - vector of parameters:
;     parameters[0] - Annihilation line flux
;     parameters[1] - Positronium continuum flux
;     parameters[2] - Annihilation line sigma
;     parameters[3] - Annihilation line centroid
;
; OUTPUTS:
;   positronium_spectrum - Annihilation line with positronium continuum.
;
; CALLS:
;   positronium
;
; WRITTEN:
;   Paul Bilodeau, 22-sept-2002, paul.bilodeau@gsfc.nasa.gov
;
; MODIFIFCATION HISTORY:
;   27-nov-2002, Paul.Bilodeau@gsfc.nasa.gov - doc update.  Positronium now
;     requires sigma for line width parameter.
;-
;
FUNCTION f_positronium, energy_bins, parameters

;; Positronium needs the bin boundaries.
;; This treatment of the energy bins is consistent with f_bpow,
;; f_bpos_nline, f_nline, etc.
edge_products, energy_bins, EDGES_2=energy_bins2

positronium, energy_bins2, 0, positronium_spectrum, 0, parameters, [0,0], 0, 0

RETURN, positronium_spectrum

END
