;+
;
; NAME:
; 		F_THERM_DEM_EXP
;
; PURPOSE:
; This function returns the differential photon spectrum at the Earth, d(Flux)(eph)/dt,
; from plasma with temperature t.
; The differential emission measure has an exponential dependence on temperature.
; This is the integrand for the numerical integration in F_MULTI_THERM_EXP.
;
; CATEGORY:
;       SPECTRA, XRAYS
;
; CALLING SEQUENCE:
;       Flux = F_THERM_DEM_EXP(eph, t, emission, alpha)
;
; CALLED BY: f_multi_therm_exp
;
; CALLS:
;       f_vth.pro
;
; INPUTS:
;       eph -      array of photon energies or energy bands in keV
;       t -        temperature in keV
;       emission - differential emission measure (DEM) at t = 2 keV
;       alpha -    exponential decay scale in keV for computation of
;                  DEM(t) = a(0) * exp( (2. - t)/alpha )
;
;
; WRITTEN: Linhui Sui, 2003/08/28
;
; REVISED: Gordon Holman, 2003/09/04, Expanded documentation.
;
;-


function f_therm_dem_exp, eph, t, emission, alpha

; The t dimension is array of [E_dim, t_dim]

tsize = size(t)
flux = fltarr(tsize[1], tsize[2])

for i = 0,  tsize[2] - 1 do begin
	para = [emission, t[0,i]]
	flux[*, i] = f_vth(eph, para) * exp( (2.0 - t[0, i])/alpha )
endfor

return, flux

end
