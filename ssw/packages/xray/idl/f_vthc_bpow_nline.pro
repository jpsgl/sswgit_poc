
;+
; NAME:
;	F_VTHC_BPOW_NLINE
;
; PURPOSE: thermal continuum (nolines) + broken power-law function with/without
;	discontinuities in the
;	derivatives + gaussian line function
;
; CALLING SEQUENCE: result = F_VTHC_BPOW_NLINE(x, a)
;
; INPUTS:
;	x - independent variable, nominally energy in keV under SPEX, 2xN
;	a - parameters describing the broken power-law and gaussian
;		a[6+3n] - integrated line intensity of line n(<3)
;		a[7+3n] - line centroid
;		a[8+3n] - line sigma
;
;		Thermal Continuum from Mewe_kev
;		a[0] - Emission measure in units of 1e49 cm^(-3)
;		a[1] - Temperature in keV
;		Broken Power Law
;		a[2] - normalization at epivot
;		a[3] - negative power law index below break
;		a[4] - break energy
;		a[5] - negative power law index above break
;
; OUTPUTS:
;	result of function, a broken power law + gaussian line
; OPTIONAL OUTPUTS:
;
; KEYWORDS:
; 	NLINE - number of gaussian lines in model, defaults to 5
;
; PROCEDURE:
;
; CALLS:   F_bpow and f_gauss_intg or gaussian
;
; COMMON BLOCKS:
;	F_VTHC_BPOW_NLINE
;
; RESTRICTIONS:
;	when used in SPEX, limited to 8 lines on 21-jan-1996
;
; MODIFICATION HISTORY:
;	ras,  21-jan-1996
;	richard.schwartz@gsfc.nasa.gov, 11-jan-2001, support arbitrary number of lines.
;	25-jun, richard.schwartz@gsfc.nasa.gov, included thermal continuum
;-
;

function F_VTHC_BPOW_NLINE, x, a, nline=nline



sizex = size(x)
nx = sizex(sizex[0])
line = fltarr(nx)
npar = n_elements(a)
n6 = 6 ;number of components other than the gaussians
if (npar-n6) mod 3 ne 0 then message,'Invalid parameter array. Must have 6 + 3*nline elements in a.'

nline = (n_elements(a) - 6)/3

line = f_nline( x, a[6:*])


return,  f_vth(x, a[0:1],/noline)+ f_bpow( x,  a[2:5] ) + line
end

