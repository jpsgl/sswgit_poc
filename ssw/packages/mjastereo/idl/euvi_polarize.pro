pro euvi_polarize,loopfile,polfile,poles

;read loop coordinates____________________________________________________
readcol,loopfile,iloop_,ix_,iy_,iz_,ih_,ih_raw,dh_,nsm_,wave_,chi2_,q1_,q2_
iloop_old=iloop_
euvi_loop_renumerate,iloop_,wave_
nloop   =long(max(iloop_)+1)
print,'Number of loops    NLOOP   =',nloop

;display field lines______________________________________________________
openw,2,polfile
close,2
window,0,xsize=1024,ysize=1024
loadct,5
!x.range=minmax(ix_)
!y.range=minmax(iy_)
!x.style=1
!y.style=1
!p.font=-1
plot,[0,0],[0,0]
for iloop=0,nloop-1 do begin
 ind    =where(iloop_ eq iloop,ns)
 if (ns ge 1) then begin
  oplot,ix_(ind),iy_(ind),color=50
 endif
endfor
read,'Enter number of magnetic poles : ',npole
poles=fltarr(npole,3)
for ipole=0,npole-1 do begin
 print,'click position with cursor : '
 cursor,x0,y0
 poles(ipole,0)=x0
 poles(ipole,1)=y0
 wait,0.2
 read,'Enter polarity [+1,-1] : ',sign_pole
 poles(ipole,2)=sign_pole
 if (sign_pole eq +1) then xyouts,[x0,x0],[y0,y0],'+',size=5
 if (sign_pole eq -1) then xyouts,[x0,x0],[y0,y0],'-',size=5
endfor
for iloop=0,nloop-1 do begin
 ind    =where(iloop_ eq iloop,ns)
 if (ns ge 1) then begin
  oplot,ix_(ind),iy_(ind),thick=3
  xyouts,ix_(ind(0)),iy_(ind(0)),string(iloop,'(i3)')
  dpole1=sqrt((ix_(ind(0))   -poles(*,0))^2+(iy_(ind(0))   -poles(*,1))^2)
  dpole2=sqrt((ix_(ind(ns-1))-poles(*,0))^2+(iy_(ind(ns-1))-poles(*,1))^2)
  dist_min1=min(dpole1,ipole1)
  dist_min2=min(dpole2,ipole2)
  polar	  =poles(ipole1,2)
  if (dist_min2 lt dist_min1) then polar=-poles(ipole2,2) ;end of loop
  if (polar eq -1) then iplus=ind(ns-1)
  if (polar eq +1) then iplus=ind(0)
  if (polar eq -1) then iminus=ind(0)
  if (polar eq +1) then iminus=ind(ns-1)
  xyouts,ix_(iplus),iy_(iplus),'+',size=2,color=100
  xyouts,ix_(iminus),iy_(iminus),'-',size=2,color=200
  for is=0,ns-1 do begin
   if (polar eq -1) then i=ind(ns-1-is)
   if (polar eq +1) then i=ind(is)
   openw,2,polfile,/append
   printf,2,iloop_old(i),ix_(i),iy_(i),iz_(i),ih_(i),ih_raw(i),$
    dh_(i),nsm_(i),wave_(i),chi2_(i),q1_(i),q2_(i),format='(i6,6f8.1,2i6,3f6.2)'
   close,2
  endfor
 endif
;read,'continue?',yes
endfor
close,2
print,'File written with corrected polarities = ',polfile

end

