pro euv_dimming_cat,dir,iev,vers,input,goesfile
;+
; Project     : AIA/SDO, HMI/SDO, IBIS/DST, ROSA/DST
;
; Name        : EUV_CAT
;
; Category    : Catalog of time sequence of a flare event
;
; Explanation : generates event catalog file with times and
;               heliographic positions
;
; Syntax      : IDL>euv_cat,dir,goesfile,input,iev,vers
;
; Inputs      : input	= input parameters
;             : dir	= data file directory
;	      ; goesfile=GOES times and positions catalog
;	      ; iev     =flare number
;             ; vers    =version label of run
;
; Outputs     ; catalog file with times and heliographic positions
;
; History     : 21-Dec-2015, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

print,'===================EUV_CAT============================'
tmargin1=input.tmargin1
tmargin2=input.tmargin2
cadence =input.cadence
instr	=input.instr
fov0	=input.fov0
nbin	=input.nbin
wave8	=input.wave8

;_________________________GOES TIME INTERVALS____________________
readcol,goesfile,event_,tstart_,tpeak_,tend_,goes_,noaa_,helpos_,$
       format='(I,A,A,A,A,A,A)',/silent
ind    =where(event_ eq iev,nind)   ;IEV=selected flare event
if (nind le 0) then stop,'NLFFF_EVENT : NO MATCHING EVENT FOUND'
tf_start    =tstart_(ind(0))
tf_peak     =tpeak_(ind(0))
tf_end      =tend_(ind(0))
sec_start   =anytim(tf_start,/sec)
sec_peak    =anytim(tf_peak,/sec)
sec_end     =anytim(tf_end,/sec)
rise  	    =(sec_peak-sec_start)           ;[s] rise time
decay       =(sec_end-sec_peak)             ;[s] decay time
noaa        =noaa_(ind(0))
helpos      =helpos_(ind(0))
duration    =tmargin1+rise+decay+tmargin2
nt	    =(long(duration/cadence + 1) > 1) < 200

input.tstart='20'+anytim(sec_start,/hxrbs)  ;YYMMDD_HHMMSS
input.rise  =rise
input.decay =decay
input.noaa  =noaa
input.helpos=helpos
input.goes  =goes_(ind(0))

;__________________________WAVELENGTH STRINGS______________________
nwave	=n_elements(wave8)
wavestr	=''
for i=0,nwave-1 do begin
 if (wave8(i) le 9999) then string0,4,wave8(i),wave_str
 if (wave8(i) le  999) then string0,3,wave8(i),wave_str
 if (wave8(i) le   99) then string0,2,wave8(i),wave_str
 if (wave8(i) le    9) then string0,1,wave8(i),wave_str
 wavestr=wavestr+' '+wave_str
endfor

;____________________HELIOGRAPHIC POSITION________________________
;(input.helpos defines position at time input.tstart,
;the actual position in the event catalog file is corrected
;for differential rotation 
north   =strmid(helpos,0,1)
west    =strmid(helpos,3,1)
if (north eq 'N') then lat=+long(strmid(helpos,1,2))
if (north eq 'S') then lat=-long(strmid(helpos,1,2))
if (west  eq 'W') then lon=+long(strmid(helpos,4,2))
if (west  eq 'E') then lon=-long(strmid(helpos,4,2))
t_syn   =27.2753*86400. 	;mean synodic period of rotation [s]
dlon_frame=360.*(cadence/t_syn) ;longitude change per time cadence

;____________________WRITES EVENT CATALOG_________________________
string0,3,iev,iev_str
catfile ='dimming_'+iev_str+vers+'.txt'
openw,1,dir+catfile
t0_sec	=anytim(input.tstart,/sec)
dateobs_=strarr(nt)
for it=0,nt-1 do begin
 a	=' '
 nr	=string(it+1,'(I4)')
 t_sec  =(t0_sec-tmargin1)+it*cadence
 timestr='20'+anytim(t_sec,/yymmdd) 	;assume after 2000		
 yy	=strmid(timestr, 0,4)
 mm	=strmid(timestr, 5,2)
 dd	=strmid(timestr, 8,2)
 hh	=strmid(timestr,12,2)
 min	=strmid(timestr,15,2)
 ss	=strmid(timestr,18,2)
 dateobs=yy+mm+dd+'_'+hh+min+ss
 dateobs_(it)=dateobs
 lon2	=lon+it*dlon_frame		;longitude differential rotation
 string0,2,abs(lon2),lon2_str
 if (lon2 lt 0) then helpos2=strmid(helpos,0,3)+'E'+lon2_str 
 if (lon2 ge 0) then helpos2=strmid(helpos,0,3)+'W'+lon2_str 
 fov	=string(fov0,'(F4.2)')
 line	=a+nr+a+dateobs+a+noaa+a+helpos2+a+instr+a+wavestr+a+fov
 printf,1,line
endfor
close,1
print,'New catalog file created : '+dir+catfile

;________________SAVE PARAMETERS______________________________
string0,3,iev,iev_str
for i=0,nt-1 do begin
 it	=i+1
 string0,3,it,it_str
 savefile ='dimming_'+iev_str+'_'+it_str+vers+'.sav'
 save,filename=dir+savefile,input
endfor
end
