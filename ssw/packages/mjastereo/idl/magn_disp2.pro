pro magn_disp2,modelfile,runfile,para,io
;+
; Project     : SOHO/MDI, SDO/HMI, STEREO
;
; Name        : MAGN_DISP2
;
; Category    : display of magnetic data modeling
;
; Explanation : displays magnetogram and observed and fitted magnetic
;		field lines in top-view and side-view projection.
;
; Syntax      : IDL>magn_disp2,modelfile,runfile,para,io
;
; Inputs      : modelfile = input filename *_loops.sav
;               runfile   = input filename *_coeff.dat, *_fit.sav
;               para      = input parameters
;                          [ds,ngrid,nmag,nsm,qloop,nseg,acc,amax,hmax,dpix,
;                           niter,qzone,eps,qv,thresh,q_mag,halt,iopt]
;		io	  = ;0=X-window, 1=ps, 2=eps, 3=color ps
;
; Outputs     ; postscript file <runfile>.ps
;
; History     : 16-May-2011, Version 1 written by Markus J. Aschwanden
;               30-Jan-2012, Version 2 released to SSW
;
; Contact     : aschwanden@lmsal.com
;-

ds      =para[0]
ngrid   =para[1]
nmag    =para[2]
nsm     =para[3]
qloop   =para[4]
nseg    =para[5]
acc     =para[6]
amax    =para[7]
hmax    =para[8]
dpix    =para[9]
niter   =para[10]
qzone   =para[11]
eps     =para[12]
qv      =para[13]
thresh  =para[14]
q_mag	=para[15]
halt	=para[16]

;____________________READ DATA_____________________________________
savefile1=modelfile+'_loops.sav'	;-->lowlou_field.sav
restore,savefile1 ;-->bzmap,x,y,field_lines,alpha,q_scale,angle,cpu,merit
field_lines0=field_lines
alpha0	=alpha
alpha0_med=median(alpha0(*,0))
help,field_lines0
help,'dev_loop',dev_loop
;statistic,dev_loop
q_scale	=1.				;AD HOC (2012 AUG 27)

savefile2=runfile+'_fit.sav'		;-->*_field.sav
restore,savefile2 ;-->bzmap,x,y,field_lines,alpha,q_scale,angle,cpu,merit
help,field_lines

coeff_file=runfile+'_coeff.dat'
readcol,coeff_file,m1,x1,y1,z1,a1
imag    =n_elements(m1)

;____________________DISPLAY________________________________________
form    =1      ;0=landscape, 1=portrait
char    =1.0    ;character size
fignr	=''
ref     =''
dlat    =5
unit	=0
fig_open,io,form,char,fignr,runfile,unit
loadct,0

;_____________________DISPLAY MAGNETOGRAM______________________________
nx	=n_elements(x)
ny	=n_elements(y)
aspect	=1.
x1_     =0.5*(1.-0.75*aspect)
x2_     =0.5*(1.+0.75*aspect)
y1_     =0.03
y2_     =0.60
!p.title=' '
!p.position=[x1_,y1_,x2_,y2_]
!x.range=[x[0],x[nx-1]]
!y.range=[y[0],y[ny-1]]
!x.title=' '
!y.title='y-axis'
!x.style=1
!y.style=1
if (io eq 0) then begin
 nxw   =long(!d.x_vsize*(x2_-x1_)+0.5)
 nyw   =long(!d.y_vsize*(y2_-y1_)+0.5)
 image =congrid(bzmap,nxw,nyw)
endif
if (io ne 0) then image=bzmap
plot,[0,0],[0,0]
c2      =max(abs(bzmap))
c1      =-c2*2.
tv,bytscl(image,min=c1,max=c2),x1_,y1_,xsize=(x2_-x1_),ysize=(y2_-y1_),/normal
coordsphere,1,0,0,0,0,dlat
!noeras=1

;______________________DISPLAY FIELD LINES_____________________________
loadct,5
red	=110
yellow	=200
blue	=50
dim	=size(field_lines0)
if (dim[0] le 2) then nf0=dim[2]
if (dim[0] ge 3) then nf0=dim[3]
xf0	=fltarr(nf0)
yf0	=fltarr(nf0)
zf0	=fltarr(nf0)
nf0	=dim(3)
if (dim(0) eq 2) then nf0=1
for k=0,nf0-1 do begin
 ind	=where(field_lines0(*,2,k) ne 0,ns) 
 if (ns ge 1) then begin 
  xl	=field_lines0(0:ns-1,0,k)
  yl	=field_lines0(0:ns-1,1,k)
  zl	=field_lines0(0:ns-1,2,k)
  is	=long(qloop*(ns-1))
  xf0[k]=xl[is]
  yf0[k]=yl[is]
  zf0[k]=zl[is]
  if (alpha0(k,0) le alpha0_med) then color=red
  if (alpha0(k,0) gt alpha0_med) then color=red ;yellow
  oplot,xl,yl,color=color,thick=3
 endif
endfor
oplot,xf0,yf0,psym=4

;_______________________DISPLAY FITTED LINES____________________________
dim	=size(field_lines)
nf	=dim(3)
if (dim(0) eq 2) then nf=1
zmin	=1.
zmax	=1.
for k=0,nf-1 do begin
 ind	=where(field_lines(*,2,k) ne 0,ns) 
 if (ns ge 1) then begin 
  xl	=field_lines(0:ns-1,0,k)
  yl	=field_lines(0:ns-1,1,k)
  zl	=field_lines(0:ns-1,2,k)
  oplot,xl,yl,color=blue,thick=2
  zmin	=zmin<min(zl)
  zmax	=zmax>max(zl)
 endif
endfor
zmid	=(zmin+zmax)/2.
zrange	=(max(x)-min(x))*0.55		;aspect ratio yrange/xrange=0.55

;________________________DISPLAY VERTICAL PROJECTION______________________
y1_     =0.63
y2_     =0.95
!p.position=[x1_,y1_,x2_,y2_]
if (exist(merit) eq 0) then merit=[-1,-1,-1]
!p.title=runfile+' : div-free='+string(merit[0],'(f10.6)')+','+$
                   ' force-free='+string(merit[1],'(f10.6)')
!y.range=zmid+(zrange/2.)*[-1,1]
!y.title='z-axis'
phi     =2.*!pi*findgen(361)/360.
plot,cos(phi),sin(phi)
coordsphere,1,0,0,-90,0,dlat
is      =0
for k=0,nf0-1 do begin
 ind	=where(field_lines0(*,2,k) ne 0,ns) 
 if (ns ge 1) then begin 
  xl	=field_lines0(0:ns-1,0,k)
  yl	=field_lines0(0:ns-1,1,k)
  zl	=field_lines0(0:ns-1,2,k)
  if (alpha0(k,0) le alpha0_med) then color=red
  if (alpha0(k,0) gt alpha0_med) then color=red ;yellow
  oplot,xl,zl,color=color,thick=3
 endif
endfor
oplot,xf0,zf0,psym=4

;_______________________DISPLAY FITTED LINES____________________________
for k=0,nf-1 do begin
 ind	=where(field_lines(*,2,k) ne 0,ns) 
 if (ns ge 1) then begin 
  xl	=field_lines(0:ns-1,0,k)
  yl	=field_lines(0:ns-1,1,k)
  zl	=field_lines(0:ns-1,2,k)
  oplot,xl,zl,color=blue,thick=2
 endif
endfor
xyouts_norm,0.7,0.95,'misalign= '+string(angle,'(f5.1)'),char
xyouts_norm,0.7,0.90,'nfield = '+string(nf,'(I5)'),char
xyouts_norm,0.7,0.85,'dpix   = '+string(dpix,'(f10.6)'),char
xyouts_norm,0.7,0.80,'ds     = '+string(ds,'(f10.6)'),char
xyouts_norm,0.7,0.75,'ngrid  = '+string(ngrid,'(i5)'),char
xyouts_norm,0.7,0.70,'thresh = '+string(thresh,'(f6.1)')+' G',char
xyouts_norm,0.7,0.65,'imag/nmag='+string(imag,'(I3)')+'/'+string(nmag,'(I3)'),char
xyouts_norm,0.7,0.60,'nsm    = '+string(nsm,'(I5)'),char
xyouts_norm,0.7,0.55,'qloop  = '+string(qloop,'(f5.2)'),char
xyouts_norm,0.7,0.50,'nseg   = '+string(nseg,'(I5)'),char
xyouts_norm,0.7,0.45,'acc    = '+string(acc,'(f8.4)'),char
xyouts_norm,0.7,0.40,'amax   = '+string(amax,'(f8.1)'),char
xyouts_norm,0.7,0.35,'iter/niter='+string(iter+1,'(i3)')+'/'+string(niter,'(i3)'),char
xyouts_norm,0.7,0.30,'qzone  = '+string(qzone,'(f5.3)'),char
xyouts_norm,0.7,0.25,'qv     = '+string(qv,'(f6.1)'),char
xyouts_norm,0.7,0.20,'cpu    = '+string(cpu,'(f6.1)')+' s',char
fig_close,io,fignr,ref
end

