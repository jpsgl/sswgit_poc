pro nlfff_prof_step,time,efree,nloop,t5,dtol,t,s,ind_step,e1,e2,te1,te2,err

;time profile of free energy is obtained from median and boxcar smoothing,
;and the energy decrease from E1 to E2 with error ERR
;at times TE1,TE2 ;during time range TIME[IND_STEP] 

;_______________________ENERGY DISSIPATION______________________
t3_hr	=t5[0]
t1_hr	=t5[1]
t0_hr	=t5[2]
t2_hr	=t5[3]
t4_hr	=t5[4]

;_______________________MEDIAN__________________________________
dt	=time(1)-time(0)
nmed	=long(2.*0.1/dt) > 3
nmed2	=long(nmed/2)
ind	=where((efree gt 0) and (time ge t3_hr) and (time le t4_hr),nt)
t	=time(ind)
e	=efree(ind)
emed	=fltarr(nt)
for it=0,nt-1 do begin
 it1	=(it-nmed2) > 0
 it2	=(it+nmed2) < (nt-1)
 emed(it)=median(e(it1:it2))
endfor  

;________________________SMOOTHING WITH CONSTANT EDGE_____________
nsm	=nmed < (nt-1) 
edge1	=fltarr(nsm)+emed(0)
edge2	=fltarr(nsm)+emed(nt-1)
emed_	=[edge1,emed,edge2]
esmo 	=smooth(emed_,nsm)
s 	=esmo(nsm:nsm+nt-1)

;________________________PEAK AND VALLEY___________________________
ind1	=where((e gt 0) and (t le t0_hr) and (t ge t1_hr-dtol))
ind2	=where((e gt 0) and (t gt t0_hr) and (t le t2_hr+dtol))
e1	=max(s(ind1),ie1)
e2	=min(s(ind2),ie2)
te1	=t(ind1(ie1))
te2	=t(ind2(ie2))
it1	=ind1(ie1)
it2	=ind2(ie2)
ind	=findgen(nt)
ind_step=ind(it1:it2)

;________________________ERROR ESTIMATE___________________________
ediff   =e2-e1
nobs	=avg(nloop)
sig1	=e1/sqrt(nobs)
sig2	=e2/sqrt(nobs)
err	=sqrt(sig1^2+sig2^2)

help,time,efree,nloop,t5,dtol,t,s,ind_step,e1,e2,te1,te2,err
end
