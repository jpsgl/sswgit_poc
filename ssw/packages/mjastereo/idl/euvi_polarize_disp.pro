pro euvi_polarize_disp,polfile,para,poles

;read loop coordinates____________________________________________________
readcol,polfile,iloop_,ix_,iy_,iz_,ih_,ih_raw,dh_,nsm_,wave_,chi2_,q1_,q2_
euvi_loop_renumerate,iloop_,wave_
nloop   =long(max(iloop_)+1)
print,'Number of loops    NLOOP   =',nloop
hmin	=0.025
nn	=4
rsun_pix=para.rsun/para.cdelt1	;solar radius in pixels
nx	=(max(ix_)-min(ix_))
ny	=(max(iy_)-min(iy_))
rad	=nx/20.
zoom	=2
window	=0

;display field lines______________________________________________________
loadct,5
for io=0,3,3 do begin
if (io eq 0) then begin
 xsize  =zoom*nx
 ysize  =zoom*ny
 window,window,xsize=xsize,ysize=ysize
 set_plot,'x'
 !p.font=-1
 char   =2.0
 x1_    =0.0
 y1_    =0.0
 x2_    =1.0
 y2_    =1.0
endif
if (io ge 1) then begin
 form    =1     ;landscape format
 char    =1.0     ;smaller character size
 fignr='polar'
 ipos    =strpos(polfile,'polar')
 plotname=strmid(polfile,0,ipos)
 unit    =0
 ref     =''
 fig_open,io,form,char,fignr,plotname,unit
 x1_    =0.05
 x2_    =0.95
 y1_    =0.5-0.5*(x2_-x1_)*0.7*float(ny)/float(nx)
 y2_    =0.5+0.5*(x2_-x1_)*0.7*float(ny)/float(nx)
endif

!p.position=[x1_,y1_,x2_,y2_]
!x.range=minmax(ix_)+nx*[-0.05,0.05]
!y.range=minmax(iy_)+ny*[-0.05,0.05]
!x.style=1
!y.style=1
!p.font=-1
plot,[0,0],[0,0]
dim	=size(poles)
npoles	=dim(1)
phi	=2.*!pi*findgen(100)/101.
for ip=0,npoles-1 do begin
 oplot,poles(ip,0)+rad*[-0.5,0.5],poles(ip,1)+[0.,0.],thick=5
 if (poles(ip,2) eq +1) then $
  oplot,poles(ip,0)+rad*[0.,0.],poles(ip,1)+rad*[-0.5,0.5],thick=5
 oplot,poles(ip,0)+rad*cos(phi),poles(ip,1)+rad*sin(phi),thick=2
endfor
for iloop=0,nloop-1 do begin
 ind    =where(iloop_ eq iloop,ns)
 loopcol=50.*wave_(ind(0))
 euvi_spline,para,ix_(ind),iy_(ind),iz_(ind),x,y,z,d,r,r_n
 h	=r/rsun_pix-1.
 n	=n_elements(x)
 oplot,x,y,color=loopcol
 jmax=long(n/nn)
 for j=0,jmax do begin
  ind	=j*nn+findgen(nn+1)
  if (j eq jmax) then ind=n-1-nn+findgen(nn+1)
  ha    =avg(h(ind))
  th	=long(10.*exp(-ha/hmin))
  oplot,x(ind),y(ind),thick=th,color=loopcol
 endfor
;xyouts,x(0),y(0),string(iloop,'(i3)')
 iplus =0
 iminus=n-1
 hplus	=r(iplus)/rsun_pix-1. 
 hminus	=r(iminus)/rsun_pix-1. 
 if (hplus  le hmin) then xyouts,x(iplus), y(iplus), '+',size=2
 if (hminus le hmin) then xyouts,x(iminus),y(iminus),'-',size=2
endfor

if (io ge 1) then fig_close,io,fignr,ref
endfor

end

