pro nlfff_dem_plot,dir_dem,efile,iev,dateobs_,fov_,tmargin,dt,vers,io,nbin
;+
; Project     : AIA/SDO
;
; Name        : TEST_FLARE_DEM
;
; Category    : Data analysis   
;
; Explanation : calculates DEM and Te 
;
; Syntax      : IDL>test_flare_dem
;
; Inputs      : nr	 = flare number in catalog
;		cat_file = flare catalog file
;
; Outputs     : images strored in savefile
;
; History     : 7-Nov-2012, Version 1 written by Markus J. Aschwanden
;		            (see ~/IDL_STEREO/AIA_TEEM_MAP.PRO)
;
; Contact     : aschwanden@lmsal.com
;-

print,'______________NLFFF_DEM_PLOT_________________________
plotname=dir_dem+dateobs_[0]+'_'+vers+'_dem'
ipos	=strpos(vers,'run6')
if (ipos ge 0) then plotname=dir_dem+dateobs_[0]+'_'+vers+'_dem
savefile=plotname+'.sav'
restore,savefile ;->,dateobs_,wave_,nt,time_t,flux_t,time_t,texp_t,telog,dem_t,$
    ;-->emlog_prof,te_prof,ne_prof,eth_prof,chi_prof,rad_prof,area_prof,vol_prof,$
    ;-->flux_prof,fit_prof
print,'read file = ',savefile
restore,'goes_prof.sav'	;-->event_,time_goes_,flux_goes_,deriv_goes_
readcol,efile,ev_nr,tstart_,tpeak_,tend_,goes_,noaa_,pos_,$
        format='(I,A,A,A,A,A,A)'

nwave	=n_elements(wave_)
ind	=where(event_ eq iev)
ind2	=where(ev_nr eq iev)
if (ind[0] ne ind2[0]) then stop,'NLFFF_DEM_PLOT: INCONSISTENCY EV_NR'
i	=ind[0]
ind_t  =where(time_goes_(*,i) ne 0)
time_goes =time_goes_(ind_t,i)
flux_goes =flux_goes_(ind_t,i)
tstart  =tstart_(i)
tpeak   =tpeak_(i)
tend    =tend_(i)
class   =goes_(i)
noaa    =noaa_(i)

t1      =anytim(tstart,/sec)
tp      =anytim(tpeak,/sec)
t2      =anytim(tend,/sec)
t3      =t1-tmargin*3600.
t4      =t2+tmargin*3600.
tbase   =(t1-(t1 mod 86400.))           ;sec since start of day
t1_hr   =(t1-tbase)/3600.
t2_hr   =(t2-tbase)/3600.
tp_hr   =(tp-tbase)/3600.
t3_hr   =(t3-tbase)/3600.
t4_hr   =(t4-tbase)/3600.
t_hr	=t3_hr+dt*findgen(nt)

;____________________PLOT___________________________________
form    =1      ;0=landscape, 1=portrait
char    =1 	;character size
ref     =''
fignr	=''
ref     =''     ;label at bottom of Fig.
unit    =0      ;window number
col     =[50,75,100,125,145,155,175,200]
ct	=5

;______________________DISPLAY GOES light curves___________________
fig_open,io,form,char,fignr,plotname,unit
loadct,0
!p.position=[0.1,0.80,0.5,0.96]
!p.title=''
!x.title='Time [hrs]'
!y.title='GOES flux [W m!U-2!N]'
!x.range=[t3_hr,t4_hr]
!x.style=1
!y.style=1
fmax	=max(flux_goes)
!y.range=[0,fmax>1.e-6]
plot,time_goes,flux_goes,thick=3
oplot,t1_hr*[1,1],!y.crange,linestyle=2
oplot,tp_hr*[1,1],!y.crange,linestyle=0
oplot,t2_hr*[1,1],!y.crange,linestyle=2
xyouts_norm,0.02,0.9,dateobs_[0]+' (Event #'+string(iev,'(i3)')+')',char
xyouts_norm,0.02,0.8,'GOES '+class+'-class, 1-8 A',char
xyouts_norm,0.02,0.7,'NOAA/AR '+noaa,char
xyouts_norm,0.02,0.6,'New version='+vers,char
xyouts_norm,0.05,0.05,'(a)',char*2
!noeras=1

;____________________DISPLAY FLUX OF FLARE________________
!p.position=[0.1,0.60,0.5,0.76]
!y.title='AIA flux [normalized]'
!p.title=' '
!y.range=[0,1.1]
!y.style=1
loadct,0
plot, t1_hr*[1,1],!y.range,linestyle=2
oplot,tp_hr*[1,1],!y.range,linestyle=0
oplot,t2_hr*[1,1],!y.range,linestyle=2
loadct,ct
xyouts_norm,0.02,0.9,'AIA/SDO',char
for iw=0,nwave-1 do begin
 ind	=where(t_hr ge t3_hr)
 f2	=max(flux_prof[ind,iw])
 oplot,t_hr[ind],flux_prof[ind,iw]/f2,thick=3,linestyle=0,color=col[iw] 
 xyouts_norm,0.02,0.9-0.1*(1+iw),wave_[iw]+' A',char,1,col[iw]
endfor
xyouts_norm,0.05,0.05,'(b)',char*2

;___________________DISPLAY OF PARAMETER PROFILES________________
!p.position=[0.1,0.40,0.5,0.56]
!y.title='EM, Te, L, ne, Eth'
!p.title=' '
!y.range=[0,2.]
!y.style=1
loadct,0
plot, t1_hr*[1,1],!y.range,linestyle=2
oplot,t2_hr*[1,1],!y.range,linestyle=2
loadct,ct
xyouts_norm,0.02,0.9,'EM',char,0,col[0]
xyouts_norm,0.02,0.8,'Te [MK]',char,0,col[2]
xyouts_norm,0.02,0.7,'L  [cm]',char,0,col[3]
xyouts_norm,0.02,0.6,'ne [cm-3]',char,0,col[4]
xyouts_norm,0.02,0.5,'E!Dth!N [erg]',char,0,col[6]
xyouts_norm,0.02,0.4,'!9s!3!Ddev!N ',char,0
indm	=where((t_hr gt t3_hr) and (t_hr le t4_hr))
emlog_peak=max(emlog_prof(indm),imax)		;<<<<<EM peak<<<<<<<<<<<<<<
im	=indm(imax)
oplot,t_hr[im]*[1,1],!y.range,thick=1
oplot,!x.range,[1,1],linestyle=1
oplot,t_hr[ind],10.^(emlog_prof[ind]-emlog_prof(im)),thick=3,linestyle=0,color=col[0]
oplot,t_hr[ind],te_prof[ind]/te_prof(im),thick=3,linestyle=0,color=col[2]
oplot,t_hr[ind],rad_prof[ind]/rad_prof[im],thick=3,linestyle=0,color=col[3]
oplot,t_hr[ind],ne_prof[ind]/ne_prof[im],thick=3,linestyle=0,color=col[4]
oplot,t_hr[ind],eth_prof[ind]/eth_prof[im],thick=3,linestyle=0,color=col[6]
oplot,t_hr[ind],chi_prof[ind]/chi_prof[im],thick=3,linestyle=2
xyouts_norm,0.05,0.05,'(c)',char*2

;___________________DISPLAY DEM_________________________________
!p.position=[0.6,0.40,0.95,0.65]
emlog	=alog10(dem_t)
ind	=where(dem_t gt 0)
!x.range=[5.,8.] ;minmax(telog)
!y.range=minmax(emlog(ind))
!x.title='Temperature  log(T[K])'
!y.title='Emission Measure EM[cm!U-3!N K!U-1!N]'
!x.style=1
!y.style=0
plot,[0,0],[0,0]
ind	=where(t_hr ge t1_hr,nind)
for i=0,nind-1 do begin
 it	=ind(i)
 oplot,telog,emlog(*,it)
 emax	=max(emlog(*,it),im)
 xyouts,telog(im),emlog(im,it),string(it,'(I2)'),size=char*0.7
endfor

;___________________FLARE PEAK TIME____________________________
emax	=max(emlog_prof,im)
eth0	=eth_iso(im)
eth1	=eth_prof(im)
emlog0	=emlog_prof(im)
te0	=te_prof(im)
tew0	=tew_prof(im)
r0	=rad_prof(im)
ne0	=ne_prof(im)
oplot,telog,emlog(*,im),thick=3
xyouts_norm,0.05,0.05,'(f)',char*2
statistic,chi_prof,chi_avg,chi_sig
oplot,alog10(te0)*[1,1],!y.crange,linestyle=2
oplot,alog10(tew0)*[1,1],!y.crange,linestyle=1
print,'CHI2=',chi_prof
print,'CHI_AVG,CHI_SIG=',chi_avg,chi_sig

;___________________EM MAP_____________________________________
x1_	=0.6
x2_	=0.95
y1_	=0.70
y2_	=0.95
!p.position=[x1_,y1_,x2_,y2_]
dim	=size(em_map[*,*,im])
if (dim(0) eq 1) then begin &nxx=1      &nyy=1      &endif
if (dim(0) eq 2) then begin &nxx=dim(1) &nyy=dim(2) &endif
x	=findgen(nxx)
y	=findgen(nyy)
!x.style=1
!x.style=1
!x.range=minmax(x)
!y.range=minmax(y)
!p.title='log(EM) map, N!Dbin!N='+string(nbin,'(I4)')
!y.title='N-S Macropixels'
!x.title='E-W Macropixels'
plot,[1,1],[1,1]
z	=alog10(em_map[*,*,im])>1
if (io eq 0) then begin
 nxw   =long(!d.x_vsize*(x2_-x1_)+0.5)
 nyw   =long(!d.y_vsize*(y2_-y1_)+0.5)
 z     =congrid(z,nxw,nyw)>1
endif
ind	=where(z gt 0)
statistic,z(ind),zavg,zsig
c1	=zavg-3*zsig
c2	=zavg+3*zsig
tv,bytscl(z,min=c1,max=c2),x1_,y1_,xsize=(x2_-x1_),ysize=(y2_-y1_),/normal
if (nxx gt 2) and (nyy gt 2) then begin
 em_max	=max(em_map[*,*,im])
 level	=[em_thresh,em_max*0.1]
 if (em_max*0.1 le em_thresh) then level=[em_max*0.09,em_thresh]
 contour,em_map[*,*,im],x,y,level=level
endif
scale	=r0/(pix*nbin)		;length scale in units of macro pixels
zmax	=max(em_map[*,*,im],imax)
x0	=imax mod nxx
y0	=imax/nxx
xa	=x0-scale/2
xb	=x0+scale/2
ya	=y0-scale/2
yb	=y0+scale/2
oplot,[xa,xb,xb,xa,xa],[ya,ya,yb,yb,ya],thick=3
xyouts_norm,0.05,0.05,'(e)',char*2

;___________________PHASE PLOT TE vs Ne_________________________
!p.position=[0.1,0.05,0.45,0.35]
!p.title=' '
!y.range=[6.0, 8.0]
!x.range=[9.0,12.0]
!x.style=0
!y.style=0
!x.title='Electron density log[ne]'
!y.title='Temperature log[Te]'
loadct,0
if (nind le 1) then goto,skip_phase
plot,alog10(ne_prof(ind)),alog10(te_prof(ind)),thick=1	;log(Te) vs log(ne)
ind1	=where((t_hr[*,0] le tp_hr),n1) 
ind2	=where((t_hr[*,0] ge tp_hr),n2) 
loadct,ct
if (n1 ge 1) then begin
 xx	=alog10(ne_prof(ind1))
 yy	=alog10(te_prof(ind1))
 oplot,xx,yy,thick=3,color=col[0]
 xyouts,xx[0],yy[0],'Start',size=char,color=col[0]
endif
if (n2 ge 2) then begin
 xx	=alog10(ne_prof(ind2))
 yy	=alog10(te_prof(ind2))
 oplot,xx,yy,thick=3,color=col[3]
 xyouts,xx[n2-1],yy[n2-1],'End',size=char,color=col[3]
endif
SKIP_PHASE:
ne_	=10.^!x.range
te_	=te0*(ne_/ne0)^(1./2)
oplot,alog10(ne_),alog10(te_),linestyle=2		;RTV law
xyouts_norm,0.05,0.05,'(d)',char*2

;___________________PARAMETERS___________________________________
!p.position=[0.5,0.05,0.95,0.35]
!x.style=5
!y.style=5
!x.range=[0,1]
!y.range=[0,1]
loadct,0
plot,[0,0],[0,0]
xyouts_norm,.0,.88,'Emission measure EM!Dp!N = 10!A'+string(emlog0,'(f5.2)')+'!N cm!U-3!N',char 
xyouts_norm,.0,.80,'Electron temperature T!Dp!N = '+string(te0/1.e6,'(f5.2)')+$
   ' MK, T!De,w!N='+string(tew0/1.e6,'(f5.2)'),char
xyouts_norm,.0,.72,'Length scale  L = '+string(r0/1.e8,'(f5.1)')+' Mm',char
xyouts_norm,.0,.64,'Electron density n!Dp!N = '+string(ne0,'(e8.2)')+' cm!U-3!N',char 
xyouts_norm,.0,.56,'Thermal energy E!Dth!N = '+string(eth1,'(e8.2)')+$ 
                        ' erg, q!Diso!N = '+string(eth1/eth0,'(F5.1)'),char 
xyouts_norm,.0,.48,'DEM chi-square !9c!3!U2!N = '+string(chi_avg,'(f5.2)')+' _',char
xyouts_norm,.0,.48,'DEM chi-square !9c!3!U2!N = '+string(chi_avg,'(f5.2)')+' +'+$
                          string(chi_sig,'(f5.2)'),char 
for iw=0,nwave-1 do begin 
 f1	=fit_prof[im,iw]
 f2	=flux_prof[im,iw]
 str	=''
 if (f2 gt 0) then begin
  qfit	=f1/f2
  str	=string(f1,'(e8.2)')+' DN/s, f!Dfit!N/f!Dobs!N = '+string(qfit,'(f8.2)')
 endif
 xyouts_norm,.0,0.37-0.08*iw,'Flux '+wave_(iw)+' A = '+str,char
endfor
fig_close,io,fignr,ref
end

