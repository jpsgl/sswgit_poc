pro euvi_hydro_project,loopfile,hydrofile,para,crota2,blong,blat,dlat,ct,hmax,w_km,wave,sc,io,model,xrange,yrange,w_res
;+
; Project     : STEREO
;
; Name        : EUVI_HYDRO_PROJECT 
;
; Category    : Graphics 
;
; Explanation : This routine produces a plot with the 3D coordinates of
;		a loop projected in any arbitrary direction.
;
; Syntax      : IDL> euvi_projection,loopfile,para,crota2,blong,blat,dlat,ct,hmax
;
; Inputs      : loopfile = datafile containing (x,y,z)-coordinates of loops (e.g.,'loop_A.dat')
;		para	= structure containing image parameters
;		crota2  =0.  ;rotation angle (deg) relative to STEREO-A image
;		blong   =0.  ;longitude diff (deg) relative to STEREO-A image
;		blat    =90. ;latitude diff (deg) relative to STEREO-A image
;	 	dlat    =1.  ;spacing (deg) of coordinate grid
;		ct	=5   ;IDL colortable
;		hmax	=0.1 ;height limit of stereoscopic correlation
;
; Outputs     : postscript file of plot *.ps
;
; History     : Oct/Nov 2007, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

;_______________INPUT PARAMETERS________________________________________
cdelt1	=para.cdelt1
crpix1	=para.crpix1
crpix2	=para.crpix2
rad   	=para.rsun
rad_pix	=rad/cdelt1
dh_max	=0.05*696.	;Mm
hchr    =2./(cdelt1*0.725) ;Mm
pix	=cdelt1*0.725 ;pixel-->Mm

;_______________READ HYDROFILE__________________________________________
restore,hydrofile
	;save,filename=hydrofile,image0,image1,para_model,fov,para,nmulti,$
        ;nloopmax,nk,em44_min,w_km,temin,temax,frac_min
tel_	=para_model(*,0)
h0_	=para_model(*,1)
lam_	=para_model(*,2)
h1_	=para_model(*,3)
lam1_	=para_model(*,4)
em44_	=para_model(*,5)
l_	=para_model(*,6)
htop_	=para_model(*,7)
w_	=para_model(*,8)
ds_	=para_model(*,9)
loop_model_=para_model(*,10)
nloop_model=n_elements(loop_model_)
dim	=size(image1)
nx	=dim(1)
ny	=dim(2)
model	=fltarr(nx,ny)
i1      =fov(0)
j1      =fov(1)
i2      =fov(2)
j2      =fov(3)

;______________PLOT PREVIOUS LOOPS______________________________________
file_exist=findfile(loopfile,count=nfiles)
if (nfiles eq 0) then print,'No previous loopfile found';
print,'Datafile with loop coordinates found  LOOPFILE=',loopfile 

idat	=strpos(loopfile,'.dat')
isav	=strpos(loopfile,'.sav')
if (idat gt 0) then readcol,loopfile,iloop_,ix_,iy_,iz_,ih_,iz_raw,dh_,nsm_,wave_,chi2_,q1_,q2_
if (isav gt 0) then restore,loopfile ;-->iloop_,ix_,iy_,iz_
nloop  =max(iloop_)+1 
print,'Number of previously traced loops    NLOOP   =',nloop
xx	=(ix_-crpix1)/rad_pix	;xx normalized to solar radius
yy	=(iy_-crpix2)/rad_pix	;yy normalized to solar radius
zz	=(iz_)/rad_pix		;zz normalized to solar radius
rotate1,xx,yy,xrot,yrot,0.,0.,crota2 
rotate1,xrot,zz,xrot2,zrot,0.,0.,blong 
rotate1,zrot,yrot,zrot2,yrot2,0.,0.,blat 
xmid    =((max(xrot2)+min(xrot2))/2.)*rad_pix+crpix1
ymid    =((max(yrot2)+min(yrot2))/2.)*rad_pix+crpix2
xrange  =xmid+[-0.5,+0.5]*float(nx)
yrange  =ymid+[-0.5,+0.5]*float(ny)
xmodel	=xrange(0)+(xrange(1)-xrange(0))*findgen(nx)/float(nx-1)
ymodel	=yrange(0)+(yrange(1)-yrange(0))*findgen(ny)/float(ny-1)

;_______________________DEFINES POINT SPREAD FUNCTION____________________
w       =(w_km/725.)/cdelt1                     ;width in pixels
w_obs   =sqrt(w^2+w_res^2)                      ;observed width
sig_gauss=w_obs/(2.*sqrt(2*alog(2.)))           ;gaussian half width in pixels
sig_gauss2=sig_gauss*4.
kernel  =fltarr(nk,nk)
for i=0,nk-1 do begin 
 for j=0,nk-1 do begin 
  kernel(i,j)=0.9*exp(-((i-nk/2)^2+(j-nk/2)^2)/(2.*sig_gauss^2))$ 
             +0.1*exp(-((i-nk/2)^2+(j-nk/2)^2)/(2.*sig_gauss2^2)) 
 endfor
endfor
kernel  =kernel/total(kernel)                   ;normalization

;_______________________READ EUVI RESPONSE FUNCTION____________________
nte_    =100
te_     =3.*findgen(nte_)/float(nte_)    ;MK loop temperature
euvi_temp_resp,wave,sc,te_,resp_iwave,1 ;EUVI response function

for il=0,nloop_model-1 do begin 
 if (il mod 100) eq 0 then print,'Loop reconstruction #',il
 iloop	=long(loop_model_(il))
 ind	=where(iloop_ eq iloop,n) 
 if (n ge 3) then begin
  xp     =xrot2(ind)*rad_pix+crpix1		;in pixels
  yp     =yrot2(ind)*rad_pix+crpix2
  zp     =zrot2(ind)*rad_pix
  euvi_spline,para,xp,yp,zp,x,y,z,d,r,r_n
  n      =n_elements(x)
  s      =fltarr(n)
  for is=1,n-1 do s(is)=s(is-1)+sqrt((x(is)-x(is-1))^2+$
         (y(is)-y(is-1))^2+(z(is)-z(is-1))^2)
  ns     =long(max(s)/ds_(il))
  s_     =ds_(il)*findgen(ns)			 ;step ds=1 pixel
  if (ns le 3) then goto,endloop
  xs     =interpol(x,s,s_)			;xp_rot interpolated to ds=1
  ys     =interpol(y,s,s_)		        ;yp_rot interpolated to ds=1
  rs     =interpol(r,s,s_)
  hs     =rs-rad_pix				;hs in pixels
  ix     =long(xs-xrange(0)+0.5)
  iy     =long(ys-yrange(0)+0.5)
  ind_ij =where((ix ge 0) and (ix le nx-1) and (iy ge 0) and (iy le ny-1),nind_ij)
  if (nind_ij ge 1) then ij=ix(ind_ij)+iy(ind_ij)*nx

  h      =hs>0
  s      =s_
  htop   =max(hs,itop)
  tchr   =0.01                           ;[MK]
  rsun   =6.96                           ;solar radius [100 Mm]
  hchr   =2./(cdelt1*0.725)
  te     =tel_(il) >tchr                 ;loop top temp [MK]
  h0     =(h0_(il)>hchr)<htop            ;loop footpoint height [pix]
  lam    =(lam_(il)<rsun)>(-rsun)        ;scale height [100 Mm]
  h1     =(h1_(il)>hchr)<htop            ;loop footpoint height [pix]
  lam1   =(lam1_(il)<rsun)>(-rsun)       ;scale height [100 Mm]
  l      =(s(itop)-s(0))>(s(ns-1)-s(itop))
  lambda =(lam/rsun)*rad_pix             ;scale height [pixels]
  lambda1=(lam1/rsun)*rad_pix            ;scale height [pixels]
  s_l    =(s-hchr)/(l-hchr)
  eps    =(tchr/te)^(7./2.)
  temp_model=tchr+(te-tchr)*( (s_l*(2.-s_l)) > eps )^(2./7.)
  ind2   =where(s gt s(itop),nind2)
  if (nind2 ge 1) then $
  temp_model(ind2)=tchr+(te-tchr)*( (s_l(ind2)*(2.-s_l(ind2))) > eps )^(2./7.)
  p1_h   =exp(-(h-hchr)/lambda)
  p1_top =exp(-(htop-hchr)/lambda)
  if (nind2 ge 1) then p1_h(ind2)=p1_top*exp((htop-h(ind2))/lambda1)
  ind3   =where((s le s(itop)) and (h lt h0),nind3)
  ind4   =where((s gt s(itop)) and (h lt h1),nind4)
  if (nind3 ge 1) then p1_h(ind3)=0.
  if (nind4 ge 1) then p1_h(ind4)=0.
  n1_h	 =p1_h/temp_model
  em1_h  =n1_h^2
  resp   =interpol(resp_iwave,te_,temp_model)>0
  em44	 =em44_(il)
  flux_model=em44*em1_h*resp

  if (nind_ij ge 1) then begin
   image_slice=fltarr(nx,ny)
   image_convol=fltarr(nx,ny)
   image_slice(ij)=image_slice(ij)+reform(flux_model(ind_ij))
   zmax =total(image_slice)
   if (zmax gt 0) then begin
    image_convol=convol(image_slice,kernel)
    model=model+image_convol
   endif
  endif
 endif
 endloop:
endfor

;________________________PLOT________________________________________
ipos     =strpos(loopfile,'trace')
fignr	  ='proj'
if (ipos le 0) then begin
 ipos    =strpos(loopfile,'field')
 fignr	  ='proj_model'
endif
plotname=strmid(loopfile,0,ipos)
form    =1     ;landscape format
char    =1     ;smaller character size
unit    =0     
ref     =''     
x1_    =0.05
y1_    =0.15 
x2_    =0.95
y2_    =0.85 
set_plot,'x'

print,'io,form,char,fignr,plotname,unit='
print,io,form,char,fignr,plotname,unit
fig_open,io,form,char,fignr,plotname,unit
if (ct ge 0) then loadct,ct
!p.position=[x1_,y1_,x2_,y2_]
!p.title=strmid(loopfile,ipos-9,8)
dx	=(xrange(1)-xrange(0))*0.001
!x.range=xrange+dx*[-1,1]
!y.range=yrange+dx*[-1,1]
!x.title='EW direction (solar radii)'
!y.title='NS direction (solar radii)'
!x.style=1
!y.style=1
plot,[0,0],[0,0]

;___________________plot model image________________________
if (io eq 0) then begin
 nxw   =long(!d.x_vsize*(x2_-x1_)+0.5)
 nyw   =long(!d.y_vsize*(y2_-y1_)+0.5)
 z     =congrid(model,nxw,nyw)
endif
if (io ne 0) then z=model
statistic,model,avg,dev
z1=avg-2*dev
z2=avg+3*dev
inv     =-1.
if (inv eq +1.) then zz=z
if (inv eq -1.) then zz=z2-z
tv,bytscl(zz,min=z1,max=z2),x1_,y1_,xsize=(x2_-x1_),ysize=(y2_-y1_),/normal
oplot,[xrange(0),xrange(1),xrange(1),xrange(0),xrange(0)],$
      [yrange(0),yrange(0),yrange(1),yrange(1),yrange(0)],thick=2

rad	=rad_pix
pos	=0.
x0	=crpix1
y0	=crpix2
blong0	=blong
blat0	=0.
crota0	=0.
dlat0	=dlat(0)
!p.color=0
coordsphere2,rad,pos,crota0,blat0,blong0,dlat0,x0,y0
;coordsphere,rad,pos,crota0,blat0,blong0,dlat0
phi	=2.*!pi*findgen(1001)/1000.
oplot,rad_pix*(1.+hmax)*cos(phi)+crpix1,rad_pix*(1.+hmax)*sin(phi)+crpix2,linestyle=2
oplot,rad_pix*cos(phi)+crpix1,rad_pix*sin(phi)+crpix2,thick=2
fig_close,io,fignr,ref
print,xrange,yrange
end
