pro aia_mag_fov,fileset,fov,image_mag,x,y,nx,ny,dpix

;read MDI file____________________________________________________
magfile =fileset+'_magn.fits'
image   =readfits(magfile,index_mag)
dateobs =sxpar(index_mag,'DATE_OBS')
naxis1  =sxpar(index_mag,'NAXIS1')
naxis2  =sxpar(index_mag,'NAXIS2')
crpix1  =sxpar(index_mag,'CRPIX1')
crpix2  =sxpar(index_mag,'CRPIX2')
cdelt1  =sxpar(index_mag,'CDELT1')
cdelt2  =sxpar(index_mag,'CDELT2')
rsun    =sxpar(index_mag,'R_SUN')               ;solar radius in pixels
pos0    =sxpar(index_mag,'SOLAR_P0')
datamin =sxpar(index_mag,'D_MIN0')
datamax =sxpar(index_mag,'D_MAX0')
p0      =pos0*!pi/180.
dpix    =1./rsun
x1      =fov(0)
x2      =fov(2)
y1      =fov(1)
y2      =fov(3)
nx      =long((x2-x1)*rsun+0.5)
ny      =long((y2-y1)*rsun+0.5)
image_mag=fltarr(nx,ny)

for j=0,ny-1 do begin
 x      =x1+(x2-x1)*findgen(nx)/float(nx)
 y      =y1+(y2-y1)*float(j)/float(ny)
 x_rot  = x*cos(p0)+y*sin(p0)
 y_rot  =-x*sin(p0)+y*cos(p0)
 i_     =long(rsun*x_rot+crpix1+0.5)
 j_     =long(rsun*y_rot+crpix2+0.5)
 stripe =image(i_,j_)
 ind    =where((stripe ge datamin) and (stripe le datamax),nind)
 if (nind ge 1) then image_mag(ind,j)=stripe(ind)
endfor

x      =x1+(x2-x1)*findgen(nx)/float(nx)
y      =y1+(y2-y1)*findgen(ny)/float(ny)
end
