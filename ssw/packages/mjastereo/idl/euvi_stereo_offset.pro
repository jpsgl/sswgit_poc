pro euvi_stereo_offset,xp,yp,xp2,yp2,xp_b,yp_b,dx_error
;+
; Project     : STEREO
;
; Name        : EUVI_STEREO_HEIGHT
;
; Category    : Data analysis
;
; Explanation : This routine interpolates coordinates [xp_b,yp_b] from curve [xp2,yp2] 
;		in spacecraft B ;at identical y-coordinates yp_b=yp as curve [xp,yp] 
;		in spacecraft A ;and stereoscopic error
;		This routine is called from EUVI_STEREO.PRO
;
; Syntax      : IDL> euvi_stereo_offset,xp,yp,xp2,yp2,xp_b,yp_b,dx_error
;
; Input:
;               xp   =x-coordinate of loop spline point in image A
;               yp   =y-coordinate of loop spline point in image A
;               xp2  =x-coordinate of loop spline point in image B
;               yp2  =y-coordinate of loop spline point in image B
; Output:
;               xp_b =x-coordinate of loop spline point in image B
;               yp_b =y-coordinate of loop spline point in image B (with yp_b=yp)
;
; History     : Oct/Nov 2007, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

;average offset
dx_avg	=avg(xp2)-avg(xp)
xp2_coal=xp2-dx_avg

;interpolation of B
spline_p,xp2_coal,yp2,x_2,y_2
spline_p,x_2,y_2,x2,y2
n	=n_elements(x2)

;interpolation
np	=n_elements(xp)
xp_b	=fltarr(np)
yp_b	=fltarr(np)
dx_error=fltarr(np)
for i=0,np-1 do begin
 dmin	=min((xp(i)-x2)^2+(yp(i)-y2)^2,in)
 in	=in<(n-2)
 yp_b(i)=yp(i)
 if (x2(in+1) eq x2(in)) then slope=1.e6
 if (x2(in+1) ne x2(in)) then slope=(y2(in+1)-y2(in))/(x2(in+1)-x2(in))
 xp_b(i)=x2(in)		;+(yp(i)-y2(in))/slope
 dx_error(i)=0.5*sqrt(1.+1./slope^2)
endfor

;resetting offset
xp_b	=xp_b+dx_avg
end
 
