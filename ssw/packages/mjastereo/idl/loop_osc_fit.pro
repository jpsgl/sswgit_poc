pro loop_osc_fit,t,y_obs,t_fit,y_fit,coeff,period

;fit of damped oscillation
common euvi_osc_powell_common,x,y,y_model 
x	=t
y	=y_obs
statistic,y,a0,a1
p	=period
tau	=period*2.
t0	=x(0)
coeff  =[a0,a1,p,tau,t0]
npar   =n_elements(coeff)
xi     =fltarr(npar,npar)
for i=0,npar-1 do xi(i,i)=1.
ftol   =1.e-4
powell,coeff,xi,ftol,fmin,'euvi_osc_powell'
nt	=n_elements(x)
nn	=nt*10
x	=x(0)+(x(nt-1)-x(0))*findgen(nn)/float(nn-1)
dev	=euvi_osc_powell(coeff) ;-->x,y_model
t_fit	=x
y_fit	=y_model
end
