pro euvi_summary,datadir,date,tstart,tend,type,instr,sc,datasum,datafiles

;+
; Project     : STEREO
;
; Name        : EUVI_SUMMARY
;
; Category    : Database management
;
; Explanation : This routine reads the EUVI summary file 
;		and extracts an observation summary (filenames, times,
;		filter, wavelength, exposure time, image size, etc.)
;		according to the selected observing time interval
;		specified by DATE, TSTART, TEND, INSTR, SPACECRAFT, TYPE
;               and returns a structure DATASUM and string array DATAFILES.
;		Beacon data are included in the summary.
;
; Syntax      : IDL> euvi_summary,datadir,date,tstart,tend,instr,sc,type,datasum,datafiles 
;
; Inputs      : datadir	  ='/net/kokuten/archive/stereo/lz/L0/'
;		date      ='20061231' ; YYYYMMDD
;		tstart    ='070000'   ; HHMMSS [UT]
;		tend      ='100000'   ; HHMMSS [UT]
;		instr     ='euvi'
;		sc	  ='a'        ; spacecraft 'a','b'
;		type      ='img'      ;'img','cal', 'seq'
;
; Outputs     : datasum   = structure with information of catalog entries
;             ; datafiles = string array of file names
;
; History     : 21-Feb-2007, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

;format conversions_________________________________________________
if (sc eq 'a') then sc_='A'
if (sc eq 'b') then sc_='B'
sumfile=datadir+sc+'/summary/scc'+sc_+strmid(date,0,6)+'.'+type
mon      =['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
date_str =strmid(date,6,2)+'-'+mon(long(strmid(date,4,2))-1)+'-'+strmid(date,2,2)
date_str1=date_str+' '+strmid(tstart,0,2)+':'+strmid(tstart,2,2)+':'+strmid(tstart,4,2)
date_str2=date_str+' '+strmid(tend,0,2)+':'+strmid(tend,2,2)+':'+strmid(tend,4,2)
date2_str=[date_str1,date_str2]
print,date2_str

;reading summary of entire month___________________________________________
;datasum = SCC_READ_SUMMARY(sumfile)            ;entire month

;reading summary of a full day, both A+B spacecraft________________________
;datasum = SCC_READ_SUMMARY(DATE=date_str)      ;entire day, both A+B

;reading summary of a time range [TSTART,TEND], for both A+B spacecraft____
;datasum = SCC_READ_SUMMARY(DATE=date2_str)

;reading summary for specified input [TSTART,TEND,SPACECRAFT,INSTR]________
 datasum  =SCC_READ_SUMMARY(DATE=date2_str,TYP=type,spa=sc,tel=instr)
 datafiles=datadir+sc+'/'+type+'/'+instr+'/'+date+'/'+datasum.filename

;display output____________________________________________________________
 print,'SPACECRAFT = ',sc
 help,datasum,/str
 nfiles = n_elements(datasum)
 for i=0,nfiles-1 do print,datasum(i)
 print,'Number of files found: ',nfiles

end 
