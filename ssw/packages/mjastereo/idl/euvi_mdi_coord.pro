pro euvi_mdi_coord,fileset
;+
; Project     : STEREO
;
; Name        : EUVI_MDI_TRANS 
;
; Category    : Database management
;
; Explanation : reads loop tracing file from STEREO/A, interpolates loop
;		coordinates, and transforms into STEREO/B coordinates
;		and Earth coordinates (e.g. for SOHO/MDI)
;
; Syntax      : IDL> euvi_mdi_coord,fileset
;
; Inputs      : savefile	: contains image parameters
;		loopfile	; contains low-resolution loop spline points
;
; Outputs     : loopfile_a	; high-resolution loop coordinates STEREO/A
;             : loopfile_b	; high-resolution loop coordinates STEREO/B
;             : loopfile_c	; high-resolution loop coordinates Earth, MDI
;
; History     : Oct 2011, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

;_______________MAGNETOGRAM__________________________________________
fileset0=strmid(fileset,0,17)
file_mdi   =fileset+'_magn.fits'
image_mdi=readfits(file_mdi,index_mdi)

;_______________EARTH COORDINATE SYSTEM_____________________________
xyzfile    =fileset0+'_a_xyz.dat'         	 ;coord STEREO/A
readcol,xyzfile,iloop_,ip_,x_,y_,z_,s_,h_
savefile   =fileset0+'_a_image.sav'		 ;image_pair,index_a,index_b
restore,savefile				
sb_mja2pts,xyzfile,a=index_a(0),b=index_b(0),pts=pts
wcs_mdi=fitshead2wcs(index_mdi)
hg      =pts.rval[0:1,*]                         ;heliographic Carrington
hc      =sb_hg2hc(hg,pos=wcs_mdi.position,/carr) ;heliocentric coordinates
iloop   =-pts.numf                               ;loop number
rc      =1.+h_/696.
xc      =reform(hc(0,*))*rc
yc      =reform(hc(1,*))*rc
zc      =reform(hc(2,*))*rc
np	=n_elements(iloop)

;_______________WRITE COORDINATE FILE STEREO/B______________________
coordfile  =fileset0+'_a_coord.dat'        ;coord MDI
openw,1,coordfile
for i=0,np-1 do printf,1,iloop(i),xc(i),yc(i),zc(i),format='(i6,3f12.6)'
close,1
print,'loop coordinates Earth,MDI: '+coordfile
end
