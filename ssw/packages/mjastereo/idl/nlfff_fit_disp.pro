pro nlfff_fit_disp,x_fit,s_fit,k,para,coeff

hmin    =para[0]
nsmax   =para[7]
hmax    =para[12]
ds      =para[13]
nseg	=long(para[8])

;extract loop____________________________________________________
ifit    =nseg*k+findgen(nseg)
imid    =nseg*k+(nseg/2)
sl_	=s_fit(ifit)
xl_     =x_fit(ifit,0)
yl_     =x_fit(ifit,1)
zl_     =x_fit(ifit,2)
rl_	=sqrt(xl_^2+yl_^2+zl_^2)
hl_	=rl_-1.
scale	=0.1

;field line______________________________________________________
xm    =xl_(nseg/2)
ym    =yl_(nseg/2)
zm    =zl_(nseg/2)
nlfff_fieldline,xm,ym,zm,ds,hmax,coeff,field,np,nsmax
np    =np < nsmax
xfield=field(0:np-1,0)
yfield=field(0:np-1,1)
zfield=field(0:np-1,2)
rfield=sqrt(xfield^2+yfield^2+zfield^2)
sfield=fltarr(np)
for ip=1,np-1 do sfield(ip)=sfield(ip-1)+$
 sqrt((xfield(ip)-xfield(ip-1))^2+(yfield(ip)-yfield(ip-1))^2)

;plot____________________________________________________________
window,1,xsize=700,ysize=1400
clearplot
loadct,5
!p.position=[0.1,0.55,0.95,0.95]
!p.title='Loop number ='+string(k,'(I5)')
!x.range=xl_(nseg/2)+[-1.,1.]*scale/2.
!y.range=0.999+[0,scale]
!x.style=1
!y.style=1
plot ,xl_,rl_,psym=-1,thick=2,xtitle='x',ytitle='altitude h'
oplot,!x.crange,[1,1]
oplot,xfield,rfield,color=125
!noeras=1

!p.position=[0.1,0.05,0.95,0.45]
!y.range=yl_(nseg/2)+[-1.,1.]*scale/2.
plot,xl_,yl_,psym=-1,thick=2,xtitle='x',ytitle='y'
oplot,xfield,yfield,color=125,thick=2
dlat=1.0
coordsphere,1.,0,0,0,0,dlat

read,'continue?',yes
if (yes eq 0) then stop

end
