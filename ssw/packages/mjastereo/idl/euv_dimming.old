pro euv_dimming,dir,savefile,test    
;+
; Project     : SDO/AIA,  
;
; Name        : EUV_DIMMING 
;
; Category    : data analysis of EUV dimming and CME mass, speed, and energy
;
; Explanation : Reads AIA images and performs EUV dimming analysis
;
; Syntax      : IDL>euv_dimming,dir,savefile,test
;
; Inputs      : dir,savefile,test
;
; Outputs     ; IDL save file: *.sav
;
; History     : 24-Feb-2016, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

print,'__________________EUV DIMMING_____________________________
restore,dir+savefile    ;--> PARA
filename=strmid(savefile,0,15)
instr	=input.instr
fov0    =input.fov0	;width of position angle range
nbin	=input.nbin
helpos	=input.helpos	;flare position heliographic coordinates
wave_disp=input.wave_disp
wave_	=para.wave_
x1_sun  =para.x1_sun
x2_sun  =para.x2_sun
y1_sun  =para.y1_sun
y2_sun  =para.y2_sun
fluxmax =para.fluxmax

;________________WAVELENGTH LOOP_____________________________
nwave	=n_elements(wave_)
exptime_=fltarr(nwave)
first_wave=0
for iwave=0,nwave-1 do begin
 wave	=wave_[iwave]
 imagefile=dir+filename+'_'+instr+'_'+wave+'.fits'

;print,'__________READ FULL EUV IMAGE_______________________________
 search =findfile(imagefile,count=file_found)
 if (file_found le 0) then begin
  print,'FILE NOT FOUND : ',imagefile
  print,'first_wave=',first_wave
  goto,end_wave
 endif
 first_wave=first_wave+1
 data	=readfits(imagefile,h,/silent)
 dateobs=sxpar(h,'DATE_OBS')
 naxis1	=sxpar(h,'NAXIS1')
 naxis2	=sxpar(h,'NAXIS2')
 cdelt1 =sxpar(h,'CDELT1')
 cdelt2	=sxpar(h,'CDELT2')
 crpix1	=sxpar(h,'CRPIX1') &if (crpix1 gt naxis1) then crpix1=naxis1/2
 crpix2	=sxpar(h,'CRPIX2') &if (crpix2 gt naxis2) then crpix2=naxis2/2
 crval1	=sxpar(h,'CRVAL1')
 crval2	=sxpar(h,'CRVAL2')
 exptime=sxpar(h,'EXPTIME')
 exptime_(iwave)=exptime
 eph	=get_sun(dateobs)
 rsun 	=eph(1)				;rsun=index.rsun_obs in arcseconds 
 rpix   =rsun/cdelt1			;solar radius in number of pixels
 dpix_euv=1./rpix			;pixel size in units of solar radius
 i0_sun =long(crpix1-crval1/cdelt1)
 j0_sun =long(crpix2-crval2/cdelt2)
 if (first_wave eq 1) then begin
  nx     =long(1+(x2_sun-x1_sun)/dpix_euv)
  ny     =long(1+(y2_sun-y1_sun)/dpix_euv)
 endif
 image1 =fltarr(nx,ny)
 i1     =long(i0_sun+x1_sun/dpix_euv) > 0
 i2     =(i1+nx)                      < (naxis1-1)
 j1     =long(j0_sun+y1_sun/dpix_euv) > 0
 j2     =(j1+ny)		      < (naxis2-1)
 image1	=data(i1:i2,j1:j2)

;_________________DISPLAY EUV SCANS_______________________________
 if (test ge 1) and (wave eq wave_disp) then begin
  nx_disp=1024
  ny_disp=1024
  window,0,xsize=nx_disp,ysize=ny_disp
  loadct,1
  x1_	=0.0
  x2_	=1.0
  y1_	=0.0
  y2_	=1.0
  !p.position=[x1_,y1_,x2_,y2_]
  r0	=1.3
  !x.range=[-1.,1.]*r0
  !y.range=[-1.,1.]*r0
  !x.style=1
  !y.style=1
  plot,[x1_sun,x2_sun,x2_sun,x1_sun,x1_sun],[y1_sun,y1_sun,y2_sun,y2_sun,y1_sun]
  oplot,[0,0],[-1.3,1.3],linestyle=2
  oplot,[-1.3,1.3],[0,0],linestyle=2
  nphi   =361
  phi    =2.*!pi*findgen(nphi)/float(nphi-1)
  oplot,cos(phi),sin(phi)
  rsun   =1
  pos    =0.
  crota2 =0.
  blong  =0.
  blat	 =0.
  dlat   =10.
  coordsphere,rsun,pos,crota2,blat,blong,dlat
  !noeras=1

  x1_    =(r0+x1_sun)/(2.*r0)
  x2_    =(r0+x2_sun)/(2.*r0)
  y1_    =(r0+y1_sun)/(2.*r0)
  y2_    =(r0+y2_sun)/(2.*r0)
  !p.position=[x1_,y1_,x2_,y2_]
  !x.range=[x1_sun,x2_sun]
  !y.range=[y1_sun,y2_sun]
  loadct,3
  plot,[x1_sun,x2_sun,x2_sun,x1_sun,x1_sun],[y1_sun,y1_sun,y2_sun,y2_sun,y1_sun]
  nxw    =long(!d.x_vsize*(x2_-x1_)+0.5)
  nyw    =long(!d.y_vsize*(y2_-y1_)+0.5)
  z      =congrid(image1,nxw,nyw)
  ind0   =where(image1 ne 0)
  statistic,image1(ind0),zavg,zsig
  c1     =zavg-3*zsig
  c2     =zavg+3*zsig
  tv,bytscl(z,min=c1,max=c2),x1_,y1_,xsize=(x2_-x1_),ysize=(y2_-y1_),/normal
 endif

;print,'__________FLUX GRID_______________________________________
 if (first_wave eq 1) then begin
  nxx	=long(nx/nbin)
  nyy	=long(ny/nbin)
  dpix_bin=dpix_euv*nbin
  x_grid	=x1_sun+dpix_bin*findgen(nxx)
  y_grid	=y1_sun+dpix_bin*findgen(nyy)
  flux_grid=fltarr(nxx,nyy,nwave)
 endif
 image2	=float(image1)/exptime 		;normalized to exposure time of 1 s
 if (nbin eq 1) then flux_grid(*,*,iwave)=image2(0:nx-1,0:ny-1)
 if (nbin ge 2) then $
 flux_grid(*,*,iwave)=rebin(image2(0:nxx*nbin-1,0:nyy*nbin-1),nxx,nyy)
 END_WAVE:
endfor
print,'exposure times =',exptime_

;________________SAVE PARAMETERS______________________________
para.dpix_euv=dpix_euv 			;pixel size in units of solar radius
rsun_cm =6.96e10                        ;cm
dx_grid_cm=nbin*dpix_euv*rsun_cm      	;grid pixel size [cm]
save,filename=dir+savefile,input,para,exptime_,flux_grid,x_grid,y_grid,dx_grid_cm
print,'parameters saved in file = ',savefile
end
