pro stereo_loop_fov,index1,data1,index2,data2,savefile 
;+
; Project     : SECCHI/STEREO
;
; Name        : STEREO_LOOP_FOV()
;
; Category    : 3D Reconstruction, Stereoscopy, Data Analsis 
;
; Explanation : The user selects a field-of-view that contains a loop
;		in the first of a STEREO image pair. The FOV parameters
;		x_range,y_range is stored in savefile	
;
; Syntax      : IDL> stereo_loop_fov,index1,data1,index2,data2,savefile 
;
; Inputs      : index1	- structure containing descriptors of data1 
;		data1 	- image at time t1 (reference time)  
;               index2	- structure containing descriptors of data2 
;		data2 	- image at time t2 (e.g.from following or previous day) 
;		savefile- string filename (e.g. 'test_001.sav')
;
; Outputs     : output parameters are written in savefile
;
; History     : 16-Aug-2006, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

;---------------------------------------------------------------;
;		CHECK INPUT 					;
;---------------------------------------------------------------;
print,'INPUT : index1,data1,index2,data2'
help,index1,data1,index2,data2
dim1	=size(data1)
dim2	=size(data2)
if (dim1(0) eq 0) or (dim2(0) eq 0) then stop,'No image input!'

;---------------------------------------------------------------;
;		INPUT PARAMETERS 				;
;---------------------------------------------------------------;
naxis1_ =fltarr(3)  &naxis1_(1)=index1.naxis1  &naxis1_(2)=index2.naxis1    
naxis2_ =fltarr(3)  &naxis2_(1)=index1.naxis2  &naxis2_(2)=index2.naxis2    
cdelt1_ =fltarr(3)  &cdelt1_(1)=index1.cdelt1  &cdelt1_(2)=index2.cdelt1    
cdelt2_ =fltarr(3)  &cdelt2_(1)=index1.cdelt2  &cdelt2_(2)=index2.cdelt2    
xcen_   =fltarr(3)  &xcen_(1)  =index1.xcen    &xcen_(2)  =index2.xcen    
ycen_   =fltarr(3)  &ycen_(1)  =index1.ycen    &ycen_(2)  =index2.ycen    
solar_r =fltarr(3)  &solar_r(1)=index1.solar_r &solar_r(2)=index2.solar_r    
pos_    =fltarr(3)     &pos_(1)=index1.solar_p    &pos_(2)=index2.solar_p    
crota2_ =fltarr(3)  &crota2_(1)=index1.crota2  &crota2_(2)=index2.crota2    
blong_  =fltarr(3)   &blong_(1)=index1.solar_l0 &blong_(2)=index2.solar_l0    
blat_   =fltarr(3)    &blat_(1)=index1.solar_b0  &blat_(2)=index2.solar_b0    
dateobs_=strarr(3)  &dateobs_(1)=index1.date_obs &dateobs_(2)=index2.date_obs
print,'NAXIS1=',naxis1_
print,'NAXIS2=',naxis2_
print,'CDELT1=',cdelt1_
print,'CDELT2=',cdelt2_
print,'XCEN  =',xcen_
print,'YCEN  =',ycen_

pa      =pos_(1)
p0a     =crota2_(1)
l0a     =blong_(1)
b0a     =blat_(1)
cdelt1	=cdelt1_(1)
cdelt2	=cdelt2_(1)
rsun    =696.;Mm
hfoot   =2.5 ;Mm
rmin    =1.+(hfoot/rsun)

;---------------------------------------------------------------;
;		SELECTING FIELD OF VIEW 			;
;---------------------------------------------------------------;
dim	=size(data1)
nx	=dim(1)
ny	=dim(2)
clearplot
set_plot,'x'
window,0,xsize=nx,ysize=ny,retain=2
zmax	=max(data1)
zmin	=min(data1)
loadct,3
!p.position=[0,0,1,1]
!x.range=[0,nx]
!y.range=[0,ny]
!x.style=5
!y.style=5
plot,[0,nx,nx,0,0],[0,0,ny,ny,0]
FOV_PLOT:
tv,bytscl(float(data1),min=zmin,max=zmax)
print,' '
print,'DISPLAY IMAGE 1: ____________________ min,max = ',zmin,zmax
c1	=0
c2	=0
read, 'Change color range    (0 0=no change)   z1,z2 =',c1,c2
if (c1 ne 0) or (c2 ne 0) then begin
 zmin=c1
 zmax=c2
 goto,fov_plot
endif

print,' '
print,'SELECTION OF FIELD OF VIEW (FOV) _______________'
print,'Click with mouse on LEFT-BOTTOM corner of FOV'
cursor,i1,j1
wait,0.5
print,'Click with mouse on RIGHT-TOP corner of FOV'
cursor,i2,j2
wait,0.5
oplot,[i1,i2,i2,i1,i1],[j1,j1,j2,j2,j1],color=255
xyouts,i1,j2+5,'Selected field of view'
ic1	=(i1+i2)/2.
jc1	=(j1+j2)/2.
fov_x	=(i2-i1)*cdelt1
fov_y	=(j2-j1)*cdelt2
print,'fov_x,fov_y=',fov_x,fov_y

;---------------------------------------------------------------;
;		FOV COALIGNMENT 				;
;---------------------------------------------------------------;
;heliographic coordinates of FOV centers of images 1,2
radii	=1.0
coord_cart_helio,index1,radii,ic1,jc1,xc1,yc1,l1,b1
r_fov	=sqrt(xc1^2+yc1^2)
print,'r_fov,solar_r=',r_fov,solar_r(1)
if (r_fov lt solar_r(1)) then begin
 coord_helio_cart,index2,radii,l1,b1,ic2,jc2,xc2,yc2
endif else begin
 print,'Center of FOV outside limb !'
 coord_cart_helio,index1,radii,ic1,jc1,xc1,yc1,l1,b1
 xc2=xc1
 yc2=yc1
 ic2=ic1
 jc2=jc1
endelse

;coordinate ranges of images 1,2
x_range=fltarr(2,4)
y_range=fltarr(2,4)
x_range(*,0)=[xc1-fov_x/2.,xc1+fov_x/2.]
y_range(*,0)=[yc1-fov_y/2.,yc1+fov_y/2.]
x_range(*,1)=[xc2-fov_x/2.,xc2+fov_x/2.]
y_range(*,1)=[yc2-fov_y/2.,yc2+fov_y/2.]
x_range(*,2)=x_range(*,0)
y_range(*,2)=y_range(*,0)
x_range(*,3)=x_range(*,1)
y_range(*,3)=y_range(*,1)

;---------------------------------------------------------------;
;	save results 						;
;---------------------------------------------------------------;
save,filename=savefile,x_range,y_range
print,'FOV parameters written in file = '+savefile
end 
