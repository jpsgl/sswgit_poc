pro euvi_cmetrack,fov_file,fov,index,ct,iseq,vect,coeff,fignrs,nrebin

restore,fov_file ;-->omages_fov(nx,ny,nt)
t	=anytim(index.date_obs,/sec)
dim	=size(images_fov)
nx	=dim(1)
ny	=dim(2)
nt	=dim(3)
nseq	=n_elements(iseq)
nrow	=long(sqrt(nseq-1)+0.99)
form    =1      ;0=landscape, 1=portrait
char    =1.0    ;character size
ref     =''	;label at bottom of Fig.
nsm	=1
inv	=1
i1	=fov(0)
j1	=fov(1)
i2	=fov(2)
j2	=fov(3)
xcen	=index(0).crpix1
ycen	=index(0).crpix2
cdelt1	=index(0).cdelt1
rsun	=index(0).rsun
x0	=xcen
y0	=ycen
r0	=rsun/cdelt1
n	=1000
phi	=2.*!pi*findgen(n)/float(n-1)
xlimb	=x0+r0*cos(phi)
ylimb	=y0+r0*sin(phi)
v	=float(vect)+[i1,j1,i1,j1]
l_km	=sqrt((v(0)-v(2))^2+(v(1)-v(3))^2)*cdelt1*725.
it0	=coeff(0)
tstart	=t(iseq(0))-t(0)
tend	=t(max(iseq))-t(0)
cad	=t(1)-t(0)
nt	=long((tend-tstart)/cad+1)    ;timestep=cadence
nh	=long(l_km/1000.)             ;height increment=1 Mm
h_slice =findgen(nh)		       ;Mm
r_slice =findgen(nh)
t_slice =tstart+cad*findgen(nt)
slice	=fltarr(nt,nh)     
slice0	=fltarr(nt,nh)     
print,'cadence=',cad
nxx	=128
nyy	=128

if (ct eq 0) then io1=1
if (ct ge 1) then io1=3
for io=0,io1,io1 do begin
 fignr    =fignrs(0)
 unit    =0      ;window number
 plotname='f'
 fig_open,io,form,char,fignr,plotname,unit
 loadct,ct
 for i=0,nseq-1 do begin
  it	=iseq(i)
  image	=images_fov(*,*,it)
  if (i eq 0) then begin
   image_t0=image
   image_old=image
   goto,endloop
  endif
  diff	=image-image_old
  image_old=image

  icol	=((i-1) mod nrow)
  irow	=long((i-1)/nrow)
  dx	=0.9/float(nrow)
  dy	=0.7/float(nrow)
  x1_	=0.05+dx*icol
  x2_	=x1_+dx*0.95
  y2_	=0.9-dy*irow
  y1_	=y2_-dy*0.95
  !p.position=[x1_,y1_,x2_,y2_]
  !x.range=[i1,i2]
  !y.range=[j1,j2]
  !x.style=5
  !y.style=5
  dim	=size(diff)
  nxx	=dim(1)/nrebin
  nyy	=dim(2)/nrebin
  diff_	=rebin(diff(0:nrebin*nxx-1,0:nrebin*nyy-1),nxx,nyy)
  if (io eq 0) then begin
   nxw  =long(!d.x_vsize*(x2_-x1_)+0.5)
   nyw  =long(!d.y_vsize*(y2_-y1_)+0.5)
   z    =congrid(diff_,nxw,nyw)
  endif
  if (io ne 0) then z=diff_
  plot,[0,0],[0,0]
  statistic,z,avg,dev
  z1	=avg-dev
  z2	=avg+dev
  tv,bytscl(z,min=z1,max=z2),x1_,y1_,xsize=x2_-x1_,ysize=y2_-y1_,/normal
  !noeras=1

;fitting circular CME shape
  oplot,[v(0),v(2)],[v(1),v(3)],thick=3
  oplot,xlimb,ylimb,thick=2
  dt	=t(it)-t(it0)
  h_km	=coeff(1)+coeff(2)*dt+coeff(3)*dt^2 ;km
  v_km_s =coeff(2)+coeff(3)*dt
  h_c	=h_km/2.
  xc	=v(0)+(v(2)-v(0))*h_c/l_km
  yc	=v(1)+(v(3)-v(1))*h_c/l_km
  rc	=(h_c/725.)/cdelt1
  xcme	=xc+rc*cos(phi)
  ycme	=yc+rc*sin(phi)
  oplot,xcme,ycme,linestyle=1
  xyouts_norm,0.01,0.05,'h='+strtrim(string(h_km/1.e3,'(i4)'),2)+' Mm, v='+$
       strtrim(string(v_km_s,'(i4)'),2)+' km/s',char
;height-time plot
  t_diff =t(iseq(i))-t(0)
  dtmin	=min(abs(t_slice-t_diff),itnext)
  for j=0,nh-1 do begin
   h_km	=float(j)*1.e3
   i_pix =long(vect(0)+(vect(2)-vect(0))*h_km/l_km+0.5)
   j_pix =long(vect(1)+(vect(3)-vect(1))*h_km/l_km+0.5)
   r_slice(j)=sqrt((i1+i_pix-x0)^2+(j1+j_pix-y0)^2) ;dist from Sun center in pixel
   slice(itnext,j)=diff(i_pix,j_pix)
   slice0(itnext,j)=image(i_pix,j_pix)/image_t0(i_pix,j_pix)
  endfor

  endloop:
 endfor
 fig_close,io,fignr,ref
endfor

for io=0,io1,io1 do begin
fignr   =fignrs(1);figure number
unit    =1      ;window number
plotname='f'
fig_open,io,form,char,fignr,plotname,unit
for k=0,1 do begin
if (k eq 0) then slice_=slice0
if (k eq 1) then slice_=slice 
if (k eq 0) then flux='Flux [DN/s]'
if (k eq 1) then flux='Flux running diff [DN/s]'
dt	=t-t(it0)
h_km	=coeff(1)+coeff(2)*dt+coeff(3)*dt^2 ;km
for i=0,nt-1 do begin
 if (slice_(i,0) eq 0) then slice_(i,*)=slice_(i+1,*)
 slice_(i,*)=smooth(slice_(i,*),11)
endfor
x1_	=0.10
x2_	=0.45
y1_	=0.55-k*0.5
y2_	=0.95-k*0.5
if (io eq 0) then begin
 nxw  =long(!d.x_vsize*(x2_-x1_)+0.5)
 nyw  =long(!d.y_vsize*(y2_-y1_)+0.5)
 z    =congrid(slice_,nxw,nyw)
endif
if (io ne 0) then z=slice_
statistic,slice_,avg,dev
z1	=avg-dev*(1-0.5*k)
z2	=avg+dev*(1-0.5*k) 
!p.position=[x1_,y1_,x2_,y2_]
!x.style=1
!y.style=1
!x.range=minmax(t_slice)+[-0.5,0.5]*cad
!y.range=[0,l_km/1.e3]
!x.title='Time t [s]'
!y.title='Height  h [Mm]'
plot,[0,0],[0,0]
tv,bytscl(z,min=z1,max=z2),x1_,y1_,xsize=x2_-x1_,ysize=y2_-y1_,/normal
oplot,t-t(0),h_km/1.e3,thick=3,color=0
oplot,t-t(0),h_km/1.e3
!noeras=1

x1_	=0.55
x2_	=0.90
offset	=(max(slice_)-min(slice_))/4.
!p.position=[x1_,y1_,x2_,y2_]
!x.range=minmax(h_slice)
!y.range=[-1,nseq+1]*offset
!y.title=flux
!x.title='Height  h [Mm]'
ind	=where(r_slice gt r0)
ind	=where(r_slice gt 0)
plot,[0,0],[0,0]
for i=0,nseq-1 do begin
 oplot,h_slice(ind),reform(slice_(i,ind)-1.)+offset*i
 if (k eq 0) then begin
  dimming=1.-min(slice_(i,ind))
  xyouts,!x.crange(1),offset*i,string(-dimming,'(f5.2)'),size=char
  dt	=t(i)-t(it0)
  h_km	=coeff(3)*dt^2 ;km
  if (sc eq 'a') then z_km=6.e5
  if (sc eq 'b') then z_km=3.e5
  h_limb=h_slice(ind)-h_slice(ind(0))
; oplot,h_slice(ind),-(h_km/z_km)*exp(-(h_limb/1.5e2)^2)+offset*i,color=128
 endif
endfor
endfor
fig_close,io,fignr,ref
endfor ;for io-0,3,3 
end
