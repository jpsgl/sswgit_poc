pro euvi_fov,images,color,fov,images_fov,index,ncorr

;+
; Project     : STEREO
;
; Name        : EUVI_FOV
;
; Category    : Data graphics
;
; Explanation : This routine allows to specify a field-of-view (interactively)
;		and to extract subimages of this FOV
;
; Syntax      : IDL>euvi_fov,images,color,fov,images_fov,index
;
; Inputs      : images	  =dblarr(*,*,nfiles)
;		color	   0=automated movie play, 1=interactive stepping of images
;               fov	  =[i1,j1,i2,j2] pixel coordinates of field-of-view
;			   interactive FOV selection if fov=[0,0,0,0]  
;               index     =structure containing FITS header to array of input images
;		ncorr	   0=no correction, 
;                         >1=number of pixels in cross-correlation 
;
; Outputs     : images_fov(*,*,nfiles) = array of n subimages 
;	        index(nfiles) =  index structure
;
; History     : 21-Feb-2007, Version 1 written by Markus J. Aschwanden
;	        13-Feb-2008, pass index structure (use pointing offset)
;	        14-Feb-2008, misalignment correction by cross-correlation
;	        14-Feb-2008, correct pointing info (EUVI_POINT,index)
;                7-Nov-2008, solar rotation tracking
;
; Contact     : aschwanden@lmsal.com
;-

method	=1					;FOV prescribed
if (total(abs(fov)) eq 0) then method=0		;FOV interactive selection
if (n_params(0) le 5) then ncorr=0

;euvi_point,index                 ;corrects pointing information

dim	=size(images)
nx	=dim(1)
ny	=dim(2)
if (dim(0) le 2) then nfiles=1
if (dim(0) eq 3) then nfiles=dim(3)
for ifile=0,nfiles-1 do begin
 image	=images(*,*,ifile)
 inonzero=where(image gt 0,nind)
 if (nind le 0) then print,'ERROR: Image has no non-zero values'
 if (nind ge 1) then begin
  if (ifile eq 0) then begin 
   statistic,image(inonzero),zav,zsig 
   z1	=zav-2.*zsig
   z2	=zav+2.*zsig
   z2	=max(image(inonzero))
  endif
  change_color:
  if (ifile eq 0) then begin
   window,0,xsize=nx<1600,ysize=ny<1600
   loadct,3
   !x.range=[0,nx]
   !y.range=[0,ny]
   !x.style=1
   !y.style=1
  endif
  x1_	=0.0
  x2_	=1.0
  y1_	=0.0
  y2_	=1.0
  io	=0
  if (io eq 0) then begin
   nxw  =long(!d.x_vsize*(x2_-x1_)+0.5)
   nyw  =long(!d.y_vsize*(y2_-y1_)+0.5)
   z    =congrid(image,nxw,nyw)
  endif
  if (io ne 0) then z=image

  !p.position=[x1_,y1_,x2_,y2_]
  !x.range=[0,nx]
  !y.range=[0,ny]
  plot,[0,0],[0,0]
  tv,bytscl(alog10(z),min=alog10(z1),max=alog10(z2)),x1_,y1_,xsize=(x2_-x1_),ysize=(y2_-y1_),/normal
  if (color eq 1) then begin
   read,'Enter z-range z1,z2 [0,0=no]= ',z1,z2
   if (z1 ne 0) or (z2 ne 0) then goto,change_color
  endif
  if (ifile eq 0) then begin
   if (method eq 0) then begin
    print,'Mark Bottom Left Corner of FOV : '
    cursor,x1,y1
    wait,0.5
    i1=long(x1)
    j1=long(y1)
    print,'x,y=',i1,j1
    oplot,[i1,i1],[j1,j1],psym=1
    print,'Mark Top Right Corner of FOV : '
    cursor,x2,y2
    wait,0.5
    i2=long(x2)
    j2=long(y2)
    print,'x,y=',i2,j2
    oplot,[i2,i2],[j2,j2],psym=1
    oplot,[i1,i2,i2,i1,i1],[j1,j1,j2,j2,j1],color=255
    fov	=[i1,j1,i2,j2]
   endif
  endif
  i1	=fov(0)
  i2	=fov(2)
  j1	=fov(1)
  j2	=fov(3)

;calculates heliographic longitude and latitude of FOV center from Earth
   euvi_pos_inv,index(ifile),fov,pos,lon,lat 

;relative offset of sun center 
  di    =index(ifile).crpix1-index(0).crpix1
  dj    =index(ifile).crpix2-index(0).crpix2
  i0	=(i1+i2)/2.
  j0	=(j1+j2)/2.
  ic	=index(ifile).crpix1
  jc	=index(ifile).crpix2
  ir    =index(ifile).rsun/index(ifile).cdelt1		;solar radius in pixel
  ix	=i0-ic
  iy	=j0-jc
  nxx	=fov(2)-fov(0)+1
  nyy	=fov(3)-fov(1)+1
  i2	=i1+nxx-1
  j2	=j1+nyy-1
  if (ifile eq 0) then images_fov=dblarr(nxx,nyy,nfiles)

;correction with cross-correlation
  if (ifile eq 0) then begin
   image1=image(i1:i2,j1:j2)
   i1new=i1
   j1new=j1
  endif
  cc0=-1.
  if (ifile eq 0) then images_fov(*,*,ifile)=image1
  if (ifile ge 1) and (ncorr eq 0) then images_fov(*,*,ifile)=image(i1:i2,j1:j2)
  if (ifile ge 1) and (ncorr ge 1) then begin
   for i3=i1new-ncorr,i1new+ncorr do begin
    i4=i3+(i2-i1)
    for j3=j1new-ncorr,j1new+ncorr do begin
     j4=j3+(j2-j1)
     image2=image(i3:i4,j3:j4)
     ccc=correlate(image1,image2)
     if (ccc ge cc0) then begin
      cc0=ccc
      i1best=i3
      j1best=j3
      images_fov(*,*,ifile)=image2
     endif
    endfor
   endfor
   i1new=i1best
   j1new=j1best
   print,'CCC shift =',i1best-i1,j1best-j1
  endif
  oplot,[i1,i2,i2,i1,i1],[j1,j1,j2,j2,j1],color=255
 endif
endfor
end
