pro helio_loopcoord2,p,p0,l0,b0,rsun,az,th,x,y,r1,xl,yl,zl
;transforms image coordinates [x,y] with loop plane orientation az,th  
;into heliographic coordinates [l,b,r]
;INPUT
;p           = position angle (N-->E)
;p0          = rotation (roll) angle of image (N-->E)
;	       (p0=-p for images rotated to solar North)
;l0,b0       = heliografic longitude and latitude of disk center.
;rsun        = solar radius [in length units]  
;az	     = azimuth angle of loop footpoint baseline to EW (deg)	
;th	     = inclination angle of loop plane to vertical (deg)
;x[n],y[n]   = image coordinates of loop (in same length units as RSUN)
;r1          = distance of loop footpoints from sun center (in same length units as RSUN)     
;		(e.g. R1=RSUN*(1+2.5/696.) for chromospheric height
;OUTPUT:
;xl[n],zl[n] = cartesian coordinates in loop plane [in samge length units as RSUN]      
;yl[n]=0 	for coplanar loops
;M.J.Aschwanden, March 1998, UMd

common powell_var,p_,p0_,l0_,b0_,rsun_,az_,th_,l1,b1,r1_,yl_,xi,yi

n	=n_elements(x)
xl	=fltarr(n)				;start    at footpoint 1
yl	=fltarr(n)				;start    at footpoint 1 
zl	=fltarr(n)				;start    at footpoint 1 
p_	=p
p0_	=p0
l0_	=l0
b0_	=b0
rsun_	=rsun
r1_	=r1
az_	=az
th_	=th
helio_trans,p,p0,l0,b0,x(0),y(0),r1,l1,b1,rsun	;r1,l1,b1 at footpoint 1
for i=1,n-1 do begin
 xi	=x(i)
 yi	=y(i)
 yl_	=yl(i)
 coeff	=[xl(i-1),zl(i-1)]			;starting point 
 vv	=fltarr(2,2) &vv(0,0)=1. &vv(1,1)=1.
 ftol	=1.0e-4
 powell,coeff,vv,ftol,dev,'helio_powell'
 xl(i)	=coeff(0)
 zl(i)	=coeff(1)
;print,i,xl(i),yl(i),zl(i),dev,format='(i8,4f8.2)' ;test output
endfor
end
