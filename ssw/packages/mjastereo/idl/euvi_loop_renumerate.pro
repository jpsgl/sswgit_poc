pro euvi_loop_renumerate,iloop_,wave_

;renumerates loops in case of multiple wavelength sets

nloop   =0
if (min(wave_) le 1) and (max(wave_) ge 1) then begin
 ind1   =where(wave_ eq 1)
 nloop  =long(max(iloop_(ind1)))+1
endif
if (min(wave_) le 2) and (max(wave_) ge 2) then begin
 ind2   =where(wave_ eq 2)
 iloop_(ind2)=iloop_(ind2)+nloop
 nloop  =long(max(iloop_))+1
endif
if (min(wave_) le 3) and (max(wave_) ge 3) then begin
 ind3   =where(wave_ eq 3)
 iloop_(ind3)=iloop_(ind3)+nloop
 nloop  =long(max(iloop_))+1
endif
if (min(wave_) le 4) and (max(wave_) ge 4) then begin
 ind4   =where(wave_ eq 4)
 iloop_(ind4)=iloop_(ind4)+nloop
 nloop  =long(max(iloop_))+1
endif
end

