pro nlfff_fieldline,xm,ym,zm,ds,hmax,coeff,field,ns,nsmax
;+
; Project     : SOHO/MDI, SDO/HMI, STEREO
;
; Name        : MAGN_FIELDLINE
;
; Category    : Magnetic data modeling
;
; Explanation : Subroutine in support of MAGN_FIELD.PRO,
;               calculting a single field line
;
; Syntax      : IDL>nlfff_fieldline,xm,ym,zm,ds,hmax,coeff,field,ns
;
; Inputs      : xm	= x-coordinate of start of field_line
;               ym	= y-coordinate of start of field_line
;               zm	= z-coordinate of start of field_line
;               ds      = spatial resolution along field line
;		hmax    = maximum altitude of field line (in solar radii) 
;               coeff   = magnetic model coefficients [b1,x1,y1,z1,a1]
;
; Outputs     ; field[NS,5] = field line coordinates
;		ns	= number of points along field line
;
; History     : 16-May-2011, Version 1 written by Markus J. Aschwanden
;               30-Jan-2012, Version 2 released to SSW
;
; Contact     : aschwanden@lmsal.com
;-

;____________________FIELD LINE HEIGHT LOOP_________________________
f   	=fltarr(nsmax,3)
f(0,0)	=xm
f(0,1)	=ym
f(0,2)	=zm
polar	=1.
for is=1,nsmax-1 do begin
 x1_	=f[is-1,*]
 nlfff_vector,coeff,x1_,b_
 b_tot  =sqrt(b_[0,0]^2+b_[0,1]^2+b_[0,2]^2)
 x2_	=x1_+ds*(b_[0,*]/b_tot)*polar
 r2	=sqrt(x2_[0,0]^2+x2_[0,1]^2+x2_[0,2]^2)
 b_tot	=sqrt(b_[0,0]^2+b_[0,1]^2+b_[0,2]^2)
 if (r2 le 1.) or (r2 ge (1.+hmax)) or (is ge nsmax-1) then goto,first_footpoint
 f[is,*]=x2_[0,*]
endfor
FIRST_FOOTPOINT:
nf1	=is < (nsmax/2-1)
if (nf1 ge 2) then begin
 for j=0,2 do f[0:nf1-1,j]=reverse(f[0:nf1-1,j])
endif
polar	=-1.
for is=nf1,nsmax-1 do begin
 x1_	=f(is-1,*)
 nlfff_vector,coeff,x1_,b_
 b_tot  =sqrt(b_[0,0]^2+b_[0,1]^2+b_[0,2]^2)
 x2_	=x1_+ds*(b_[0,*]/b_tot)*polar
 r2	=sqrt(x2_[0,0]^2+x2_[0,1]^2+x2_[0,2]^2)
 if (r2 lt 1.) or (r2 gt (1.+hmax)) or (is ge nsmax-1) then goto,end_fieldline
 f[is,*]=x2_[0,*]
endfor
END_FIELDLINE:
ns	=is < (nsmax-1)
field   =fltarr(ns,3)
for j=0,2 do field[*,j]=f[0:ns-1,j]
end

