function euvi_dimming_powell,coeff

common powell_par,t3,t1_,t2_,mc_,m1,m2,m3,m1obs,m2obs,m3obs

m_cme	=coeff(0)
t_cme	=(coeff(1)>t1_(0))<t2_(2)
dt_cme	=coeff(2)
n	=1000
te	=t3*findgen(n)/float(n-1)
dte	=te(1)-te(0)
m_te	=m_cme*exp(-(te-t_cme)^2/(2.*dt_cme^2))
ind1	=where((te ge t1_(0)) and (te le t2_(0)),n1)
ind2	=where((te ge t1_(1)) and (te le t2_(1)),n2)
ind3	=where((te ge t1_(2)) and (te le t2_(2)),n3)
m1	=total(m_te(ind1)*dte)
m2	=total(m_te(ind2)*dte)
m3	=total(m_te(ind3)*dte)
m1obs	=mc_(0)*(t2_(0)-t1_(0))
m2obs	=mc_(1)*(t2_(1)-t1_(1))
m3obs	=mc_(2)*(t2_(2)-t1_(2))
g1	=m1obs
g2	=m2obs
g3	=m3obs
dev	=sqrt(total(g1*(m1-m1obs)^2+g2*(m2-m2obs)^2+g3*(m3-m3obs)^2)/total(g1+g2+g3))
return,dev
end
