pro nlfff_timing,t1,t2,dt,nt,days_,dateobs_
;+
; Project     : SOHO/MDI, SDO/HMI, STEREO 
;
; Name        : NLFFF_TIMING
;
; Category    : Magnetic data modeling 
;
; Explanation : Produces a filename in format YYYYMMDD_HHMMSS
;		from the start and end time of a time sequence
;		with a given time step DT and sequence number IT
;
; Syntax      : IDL>nlfff_timing,t1,t2,dt,nt,days_,dateobs_
;
; Inputs      : t1='12-Feb-2011 00:00:00'
; 		t2='17-Feb-2011 00:00:00'
;		dt=1.0	;hours 
;
; Outputs     ; nt	=120		  ;number of timesteps
;		days_   =[12.0,...,17.00] ;time in units of days
;		dateobs =['20110212_000000',...,'20110217_000000']
;
; History     : 20-Aug-2013, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

print,'___________________NLFFF_TIMING_________________________'
year_str=strmid(t1,7,4)
m=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
month_let=strmid(t1,3,3)
for im=0,11 do if (month_let eq m(im)) then goto,month_found
MONTH_FOUND:
string0,2,im+1,month_str
day1    =strmid(t1,0,2)
day2    =strmid(t2,0,2)
hour1   =strmid(t1,12,2)
hour2   =strmid(t2,12,2)
min1    =strmid(t1,15,2)
min2    =strmid(t2,15,2)
t3      =(long(day1)-12.)*24.+long(hour1)+long(min1)/60.
t4      =(long(day2)-12.)*24.+long(hour2)+long(min2)/60.
nt      =(t4-t3)/dt+1
days_	=fltarr(nt)
dateobs_=strarr(nt)
for it=0,nt-1 do begin
 day    =long(day1)+long(hour1)/24.+long(min1)/(24.*60.)+(dt*it)/24.
 day	=day+0.5*(1./86400.)	;rounding
 string0,2,long(day),day_str
 hour   =day*24 mod 24
 string0,2,hour,hour_str
 minute=(hour-long(hour))*60.
 string0,2,minute,min_str
 seconds=(minute-long(minute))*60.
 string0,2,seconds,sec_str
 days_(it)=day
 dateobs_(it)=year_str+month_str+day_str+'_'+hour_str+min_str+sec_str
endfor

end
