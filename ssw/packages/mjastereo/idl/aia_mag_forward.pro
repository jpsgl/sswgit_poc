pro aia_mag_forward,bm,xm,ym,zm,bz,xp,yp,zp,wp,rho_p,beta

;calculates observables [bz,xp,yp,zp,wp] from model [bm,xm,ym,zm]

rho_m	=sqrt(xm^2+ym^2)
alpha	=abs(atan(rho_m/zm))
rm	=sqrt(rho_m^2+zm^2)
dm	=1.-rm
if (alpha eq 0) then beta=0.
if (alpha gt 0) then beta=atan((sqrt(9.+8.*(tan(alpha))^2.)-3.)/(4.*tan(alpha)))
if (alpha ge !pi/2.) then beta=atan(1./sqrt(2.))
bz      =bm*cos(beta)^2*cos(alpha-beta)
rho_p   =rho_m+dm*sin(alpha-beta)/cos(beta)
beta2   =acos((cos(beta)^3./2.)^(1./3.))
wp      =dm*tan(beta2)*cos(alpha)*(1.-0.1*alpha)
if (xm eq 0.) then gamma=!pi/2.
if (xm ne 0.) then gamma=atan(ym/xm)
xp	=rho_p*cos(gamma)
yp	=rho_p*sin(gamma)
zp	=sqrt(1.-rho_p^2)
end 
