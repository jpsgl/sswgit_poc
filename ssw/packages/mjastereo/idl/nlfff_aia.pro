pro nlfff_aia,dir,savefile,test,wave_
;+
; Project     : AIA, IRIS, IBIS
;
; Name        : NLFFF_READ
;
; Category    : read AIA, IRIS, IBIS images 
;
; Explanation : reads image from archive at LMSAL 
;               and write uncompressed FITS files
;
; Syntax      : IDL>nlfff_read,dir,savefile,test,wave_
;
; Inputs      : dir	 =data directory 
;		savefile 
;
; Outputs     ; saves AIA FITS file in dir
;
; History     : 16-Oct-2015, Version 1 written by Markus J. Aschwanden
;		10-Feb-2016, ssw_jsoc_time2data, aia_prep (Sam Freeland, Dave Jess)
;
; Contact     : aschwanden@lmsal.com
;-

restore,dir+savefile	;--> PARA
filename=strmid(savefile,0,15)
instr	=input.instr
wave8	=input.wave8
indw	=where(wave8 ne 0,nwave)
wave_	=strtrim(string(wave8[indw],'(I5)'),2)
if (instr ne 'AIA') then goto,end_proc


print,'__________________NLFFF_AIA_________________________________'
nwave	=n_elements(wave_)
nfiles_wave=lonarr(nwave)
for iwave=0,nwave-1 do begin
 search=dir+filename+'_'+instr+'_*'+wave_(iwave)+'.fits' 
 file	=findfile(search,count=count)
 if (count eq 1) then begin
  print,'File already exists : ',file
  nfiles_wave[iwave]=1
 endif
endfor
if (total(nfiles_wave) eq nwave) then begin
 print,'All files exist'
 goto,end_proc
endif

;print,'__________TIME WINDOW______________________________________
cadence	=12	 ;[seconds] AIA cadence for 7 wavelength fitlers
year    =strmid(filename,0,4)
month   =strmid(filename,4,2)
day     =strmid(filename,6,2)
hour    =strmid(filename,9,2)
minu    =strmid(filename,11,2)
seco    =strmid(filename,13,2)
t1_file =year+'-'+month+'-'+day+' '+hour+':'+minu+':'+seco
t1_sec	=anytim(t1_file,/sec)
t2_sec	=t1_sec+cadence	 ;12 s AIA cadence for 7 wavelength fitlers
t1	=anytim(t1_sec,/hxrbs)
t2	=anytim(t2_sec,/hxrbs)
print,'Start time = ',t1
print,'End   time = ',t2

;READ AIA DATA_______________________________________________
if (instr eq 'AIA') then begin
;aia=sdo_time2files(t1,t2,/aia,waves=wave_,img_type='LIGHT',level=1.5)		;VERSION V1
 ssw_jsoc_time2data,t1,t2,waves=wave_,oindex,odata,/use_shared,/silent,/noshell,/comp_delete,/uncomp_delete;,max_files=6
 if (N_ELEMENTS(odata[*,0,0]) ne 4096) then begin
  nwave  =0
  wave_	=['none']
  print,'No corresponding AIA image found'
  goto,end_proc
 endif
 nwave	=n_elements(odata[0,0,*])	;reduce to existing files
 wave_	=strarr(nwave)
 ;read_sdo,aia,index,data		;-->data(4096,4096,7)			;VERSION V1
 aia_prep, oindex, odata, index, data
 help,index,data
 for iwave=0,nwave-1 do begin
  wave=long(index[iwave].wavelnth)
  if (wave le   99) then ndigit=2
  if (wave ge  100) then ndigit=3
  if (wave ge 1000) then ndigit=4
  string0,ndigit,wave,wave_str
  wave_(iwave)=wave_str
 endfor
endif

;DISPLAY IMAGE______________________________________________
if (test eq 1) then begin
 loadct,0,/silent
 !p.background=255
 !p.color=0
 window,0,xsize=1024,ysize=1024
 for iwave=0,nwave-1 do begin
  ;print,'read file = ',aia(iwave),' wavelength=',wave_(iwave)
   print,'read file = '+dir+filename+'_'+instr+'+'+wave_(iwave)+'.fits'
  statistic,data(*,*,iwave),zavg,zsig
  tv,bytscl(rebin(data(*,*,iwave),1024,1024),min=0,max=3*zsig)
 endfor
endif

;FITS FILE________________________________________
for iwave=0,nwave-1 do begin
 aiafile_wave=dir+filename+'_'+instr+'_'+wave_(iwave)+'.fits' 
 mwritefits,index(iwave),data(*,*,iwave),outfile=aiafile_wave	;FITS file
 print,'FITS file written = ',aiafile_wave
endfor
END_PROC: 
end
