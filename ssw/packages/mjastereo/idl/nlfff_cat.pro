pro nlfff_cat,input,catfile,dir
;+
; Project     : AIA/SDO, HMI/SDO, IBIS/DST, ROSA/DST
;
; Name        : NLFFF_CAT
;
; Category    : Magnetic data modeling 
;
; Explanation : generates event catalog file with times and
;               heliographic positions
;
; Syntax      : IDL>nlfff_cat,input,catfile
;
; Inputs      : input	= input parameters
;             : catfile = filename of catalog file
;
; Outputs     ; catalog file with times and heliographic positions
;
; History     : 21-Dec-2015, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

print,'===================NLFFF_CAT============================'
catfile	=input.catfile
tstart  =input.tstart
cadence =input.cadence
duration=input.duration
noaa	=input.noaa
helpos	=input.helpos
instr	=input.instr
wave8	=input.wave8
fov0	=input.fov0
nsm1	=input.nsm1
nmag_p	=input.nmag_p
nmag_np	=input.nmag_np
amis	=input.amis
nitmin	=input.nitmin
nstruc	=input.nstruc
prox_min=input.prox_min
lmin_wid=input.lmin_wid
rmin_wid=input.rmin_wid
qthresh1=input.qthresh1
qthresh2=input.qthresh2
nt	=long(duration/cadence + 1) > 1
if (nt gt 2000) then begin
 print,'Excessive number of time steps nt =',nt
 nt	=nt < 2000
 print,'Number of time steps reduced to nt = ',nt
endif

t0_sec	=anytim(tstart,/sec)
nwave	=n_elements(wave8)
wavestr	=''
for i=0,nwave-1 do begin
 if (wave8(i) le 9999) then string0,4,wave8(i),wave_str
 if (wave8(i) le  999) then string0,3,wave8(i),wave_str
 if (wave8(i) le   99) then string0,2,wave8(i),wave_str
 if (wave8(i) le    9) then string0,1,wave8(i),wave_str
 wavestr=wavestr+' '+wave_str
endfor

;____________________HELIOGRAPHIC POSITION________________________
;(input.helpos defines position at time input.tstart,
;the actual position in the event catalog file is corrected
;for differential rotation 
north   =strmid(helpos,0,1)
west    =strmid(helpos,3,1)
if (north eq 'N') then lat=+long(strmid(helpos,1,2))
if (north eq 'S') then lat=-long(strmid(helpos,1,2))
if (west  eq 'W') then lon=+long(strmid(helpos,4,2))
if (west  eq 'E') then lon=-long(strmid(helpos,4,2))
t_syn   =27.2753*86400. ;mean synodic period of rotation [s]
dlon_frame=360.*(input.cadence/t_syn) ;longitude change per time cadence


;____________________WRITES EVENT CATALOG_________________________
openw,1,dir+catfile
for it=0,nt-1 do begin
 a	=' '
 nr	=string(it+1,'(I4)')
 t_sec  =t0_sec+it*cadence
 yymmdd =anytim(t_sec,/yymmdd)		;assume after year 2000
 yy	='20'+strmid(yymmdd,0,2)
 mm	=strmid(yymmdd,3,2)
 dd	=strmid(yymmdd,6,2)
 hh	=strmid(yymmdd,10,2)
 min	=strmid(yymmdd,13,2)
 ss	=strmid(yymmdd,16,2)
 dateobs=yy+mm+dd+'_'+hh+min+ss
 lon2	=lon+it*dlon_frame		;longitude differential rotation
 string0,2,abs(lon2),lon2_str
 if (lon2 lt 0) then helpos2=strmid(helpos,0,3)+'E'+lon2_str 
 if (lon2 ge 0) then helpos2=strmid(helpos,0,3)+'W'+lon2_str 
 fov	=string(fov0,'(F4.2)')
 nsm	=string(nsm1,'(I2)')
 nmagp	=string(nmag_p,'(I3)')
 nmagnp	=string(nmag_np,'(I3)')
 amis	=string(amis,'(I2)')
 nitm   =string(nitmin,'(I2)')
 nstr	=string(nstruc,'(I5)')
 prox	=string(prox_min,'(F4.1)')
 lmin	=string(lmin_wid,'(I1)')
 rmin	=string(rmin_wid,'(I1)')
 q1	=string(qthresh1,'(F3.1)')
 q2	=string(qthresh2,'(F3.1)')
 line	=a+nr+a+dateobs+a+noaa+a+helpos2+a+instr+a+wavestr+a+$
    fov+a+nsm+a+nmagp+a+nmagnp+a+nstr+a+amis+a+nitm+a+lmin+a+rmin+a+q1+a+q2
 printf,1,line
endfor
close,1

print,'New catalog file is created : '+dir+catfile
END_CAT:

end
