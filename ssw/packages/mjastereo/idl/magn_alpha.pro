pro magn_alpha,runfile,para
;+
; Project     : SOHO/MDI, SDO/HMI, STEREO
;
; Name        : MAGN_ALPHA
;
; Category    : Magnetic data modeling
;
; Explanation : Calculating nolinear force-free parameter ALPHA[NK,4]
;               averaged for every loop with two methods.
;
; Syntax      : IDL>magn_alpha,runfile,para
;
; Inputs      : runfile   = input file *_cube.sav, *_fit.sav, *_coeff.dat
;               para      = input parameters
;                           [ds,ngrid,nmag,nsm,qloop,nseg,acc,amax,hmax,dpix,
;                           niter,qzone,eps,qv,thresh,q_mag,halt,iopt]
;
; Outputs     ; runfile   = output file *_fit.sav
;			-->bzmap,x,y,field_lines,alpha,q_scale,angle,cpu,merit,iter
;
; History     : 16-May-2011, Version 1 written by Markus J. Aschwanden
;               30-Jan-2012, Version 2 released to SSW
;		 6-Feb-2012, cube-calculated method alpha (c)
;
; Contact     : aschwanden@lmsal.com
;-

print,'_________________MAGN_ALPHA__________________________'
ds	=para[0]
amax	=para[7]

cubefile=runfile+'_cube.sav'
restore,cubefile        ;&help,x,y,z,bx,by,bz,b,a
nx	=n_elements(x)
ny	=n_elements(y)
nz	=n_elements(z)

savefile=runfile+'_fit.sav'
restore,savefile      ;-->bzmap,x,y,field_lines,q_scale,angle,cpu,merit
dim	=size(field_lines)
if (dim[0] le 2) then nf=1
if (dim[0] ge 3) then nf=dim[3]

coeff_file=runfile+'_coeff.dat'
readcol,coeff_file,m1,x1,y1,z1,a1
coeff   =[[m1],[x1],[y1],[z1],[a1]]

;______________________FIELD LINES______________________________'
polar	=1.
alpha	=fltarr(nf,2)
for k=0,nf-1 do begin
 ind	=where(field_lines(*,2,k) ne 0,ns) 
 alpha_ =fltarr(ns)
 if ((k mod 10) eq 0) or (k eq nf-1) then print,'field line k=',k,'   ns=',ns
 for i=0,ns-1 do begin
  x0	=field_lines(i,0,k)
  y0	=field_lines(i,1,k)
  z0	=field_lines(i,2,k)
  bb	=field_lines(i,3,k)
  al	=field_lines(i,4,k)	;theoretical alpha
  x1	=x0-ds
  x2 	=x0+ds
  y1 	=y0-ds
  y2 	=y0+ds
  z1 	=z0-ds
  z2 	=z0+ds
  magn_vector,x0,y0,z0,ds,coeff,polar,xv,yv,zv,bx_  ,by_  ,bz_  ,b_  ,endv
  magn_vector,x1,y0,z0,ds,coeff,polar,xv,yv,zv,bx_x1,by_x1,bz_x1,b_x1,endv
  magn_vector,x2,y0,z0,ds,coeff,polar,xv,yv,zv,bx_x2,by_x2,bz_x2,b_x2,endv
  magn_vector,x0,y1,z0,ds,coeff,polar,xv,yv,zv,bx_y1,by_y1,bz_y1,b_y1,endv
  magn_vector,x0,y2,z0,ds,coeff,polar,xv,yv,zv,bx_y2,by_y2,bz_y2,b_y2,endv
  magn_vector,x0,y0,z1,ds,coeff,polar,xv,yv,zv,bx_z1,by_z1,bz_z1,b_z1,endv
  magn_vector,x0,y0,z2,ds,coeff,polar,xv,yv,zv,bx_z2,by_z2,bz_z2,b_z2,endv
  dby_dx =(by_x2-by_x1)/(x2-x1)
  dbz_dx =(bz_x2-bz_x1)/(x2-x1)
  dbx_dy =(bx_y2-bx_y1)/(y2-y1)
  dbz_dy =(bz_y2-bz_y1)/(y2-y1)
  dbx_dz =(bx_z2-bx_z1)/(z2-z1)
  dby_dz =(by_z2-by_z1)/(z2-z1)
  jx	=(dbz_dy-dby_dz)
  jy	=(dbx_dz-dbz_dx)
  jz	=(dby_dx-dbx_dy)
  wx	=bx_^2					;weighted by B^2
  wy	=by_^2
  wz	=bz_^2
  alpha_(i)=total(wx*(jx/bx_)+wy*(jy/by_)+wz*(jz/bz_))/total(wx+wy+wz)
 endfor
 statistic,alpha_,a_avg,a_sig			;cube-calculated alpha
 alpha(k,0)=a_avg	
 alpha(k,1)=a_sig 	

test	=0
if (test eq 1) then begin
 clearplot
 window,2,xsize=300,ysize=900
 !p.position=[0.1,0.7,0.95,0.95]
 plot,alpha__,yrange=amax*[-1,1]
 plot,alpha_,linestyle=2
 oplot,!x.crange,[0,0],linestyle=1
 xyouts_norm,0.5,0.9,string(a_avg,'(f5.2)')+'+'+string(a_sig,'(f5.2)'),char
 !noeras=1

 !p.position=[0.1,0.4,0.95,0.65]
 ind	=where(field_lines(*,2,k) ne 0,ns) 
 ineg	=where(alpha_ lt 0,ns) 
 plot,field_lines(ind,0,k),field_lines(ind,2,k),xrange=[-0.1,0.1],yrange=[0.9,1.1]
 if (ns ge 1) then oplot,field_lines(ineg,0,k),field_lines(ineg,2,k),thick=3

 !p.position=[0.1,0.1,0.95,0.35]
 plot,field_lines(ind,0,k),field_lines(ind,1,k),xrange=[-0.1,0.1],yrange=[-0.1,0.1]
 if (ns ge 1) then oplot,field_lines(ineg,0,k),field_lines(ineg,1,k),thick=3

 read,'continue?',yes
 if (yes eq 0) then stop
;wait,0.3
endif

endfor

;_______________________SAVE ALPHA PARAMETER_________________________
save,filename=savefile,bzmap,x,y,field_lines,alpha,q_scale,angle,cpu,merit,iter,dev_loop
print,'alpha parameters saved in file = ',savefile

end

