pro euvi_corr,image_pair,para,nsm,fov_small,dcont,outfile,ct
;+
; Project     : STEREO
;
; Name        : EUVI_CORR 
;
; Category    : Data analysis  
;
; Explanation : This routine displays a selected field-of-view (FOV_SMALL) 
;		of a stereoscopic image pair (IMAGE_PAIR).
;		The user localizes manually a corresponding point-like structure
;		in the two images and the routine performs the stereoscopic
;		height calculation and stores the (x,y,z) values in OUTFILE. 
;
; Syntax      : IDL>euvi_corr,image_pair,para,nsm,fov_small,dcont,outfile,ct
;
; Inputs      : images_pair(2,*,*) = stereo image pair B,A
;		para               = index structure with image parameters 
;		nsm		   = smoothing parameter for highpass filter
;		fov_small	   = [i1,j1,i2,j2] small field-of-view in image pixels 
;               dcont              : lowest contour level (DN), dcont*2^i (logarithmic higher levels)
;               ct                 : 0, ..., 38 IDL color table
;
; Outputs     : outfile 	    = output: iloop,x,y,z,...
;
; History     : Oct/Nov 2007, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

;________________________________________________________________
;			SUBIMAGES  				|
;________________________________________________________________
rsun_m  =0.696*1.e9
cdelt1	=para.cdelt1
cdelt2	=para.cdelt2
crpix1	=para.crpix1
crpix2	=para.crpix2
da	=para.da/rsun_m
db	=para.db/rsun_m
rsun   	=para.rsun
a_sep	=para.sep_deg*(!pi/180.)
hmax	=0.1
rad_pix	=rsun/cdelt1
r_max   =1.0+hmax
i1	=fov_small(0)
j1	=fov_small(1)
i2	=fov_small(2)
j2	=fov_small(3)
nx	=(i2-i1+1)
ny	=(j2-j1+1)
ism	=(nsm-3)/2			;nsm=3,5,7-->ism=0,1,2
r_n	=1.0
im	=(i1+i2)/2.
jm	=(j1+j2)/2.
rm	=sqrt((im-crpix1)^2+(jm-crpix2)^2)
im_b	=im
jm_b	=jm
if (rm lt rad_pix) then euvi_stereo_rot,im,jm,para,r_n,im_b,jm_b,zm_b
i1_b	=im_b-(im-i1)
i2_b	=i1_b+(i2-i1)
xrange1 =[i1  ,i2  ]
xrange2 =[i1_b,i2_b]
images_fov=fltarr(nx,ny,2)		;2 spacecraft(A,B)  
image_a	=float(reform(image_pair(1,i1:i2,j1:j2)))
image_b	=float(reform(image_pair(0,i1_b:i2_b,j1:j2)))
for it=0,1 do begin
 if (it eq 0) then image0 =image_a
 if (it eq 1) then image0 =image_b
 if (nsm ge 3) then images_fov(*,*,it)=(image0-smooth(image0,nsm))
endfor
panel2	=[0.0,0.0,0.5,1.]
panel1	=[0.5,0.0,1.0,1.]
nloop	=0
loopcol	=150
loopcol2= 50
zoom	=long((2000/(2*nx)) < (1600/(ny)))
print,'Use zoom factor of ',zoom
di	=1
ni	=di*2+1

print,'__________________________________________________________________'
print,'|		PLOT STEREO IMAGE PAIR  			|'
print,'__________________________________________________________________'
io	=0
xsize	=zoom*nx*2
ysize	=zoom*ny
window,0,xsize=xsize,ysize=ysize
set_plot,'x'
!p.font=-1
loadct,ct
for it=0,1 do begin
 if (it eq 0) then panel=panel1 
 if (it eq 1) then panel=panel2 
 x1_	=panel(0)
 y1_	=panel(1)
 x2_	=panel(2)
 y2_	=panel(3)
 image  =images_fov(*,*,it)
 !p.position=[x1_,y1_,x2_,y2_]
 if (it eq 0) then !x.range=xrange1
 if (it eq 1) then !x.range=xrange2
 !y.range=[j1,j2]		&yrange12=!y.range
 !x.style=1
 !y.style=1
 plot,[0,0],[0,0]
 xx	=!x.range(0)+findgen(nx) 
 yy	=!y.range(0)+findgen(ny) 
 zz	=dcont*2^findgen(10)
 contour,image,xx,yy,level=zz
 if (it eq 0) then sc='A'
 if (it eq 1) then sc='B'
 xyouts_norm,0.02,0.02,sc,3
 !noeras=1
endfor		;stereo image pair
oplot,[i1,i1],[j1,j2],thick=3

print,'__________________________________________________________________'
print,'|		PLOT PREVIOUS LOOPS 				|'
print,'__________________________________________________________________'
loadct,3
nloop=0
file_exist=findfile(outfile,count=nfiles)
if (nfiles eq 0) then print,'No previous loopfile found';
if (nfiles ge 1) then begin 
 readcol,outfile,iloop_,ix_,iy_,iz_,ih_pix,dh_pix,ih_mm,dh_mm,nsm_
 print,'Statistics of height measurements:'
 statistic,ih_mm
 if (n_elements(iloop_) lt 1) then stop,'loopfile empty: ',outfile
 nloop	=long(max(iloop_))+1
 print,'Datafile with loop coordinates found  LOOPFILE=',outfile 
 print,'Number of loops found                NLOOP   ='+string(nloop,'(i3)')
 for iloop=0,nloop-1 do begin 
  ind	=where(iloop_ eq iloop,np) 
  xp	=ix_(ind)
  yp	=iy_(ind)
  zp	=iz_(ind)
  euvi_stereo_rot,xp,yp,para,1.,xb,yb,zb

  !p.position=panel1		;STEREO A projection of field lines
  !x.range=xrange1
  !y.range=yrange12
  char	=1.0
  plot,[xp,xp],[yp,yp],psym=1,symsize=2,color=loopcol
  xyouts,xp,yp,string(iloop,'(I3)'),size=char*2,color=loopcol
  ip	=long(xp+0.5)
  jp	=long(yp+0.5)
  oplot,ip(0)-di+[0.,ni-1,ni-1,0.,0.],jp(0)+[-1.,-1.,1.,1.,-1.],color=loopcol

  !p.position=panel2 		;STEREO B projection of field lines
  !x.range=xrange2
  plot,[xb,xb],[yb,yb],psym=1,symsize=2,color=loopcol
  xyouts,xb,yb,string(iloop,'(I3)'),size=char*2,color=loopcol  
  ip2	=long(xb+0.5)
  jp2	=long(yb+0.5)
  oplot,ip2(0)-di+[0.,ni-1,ni-1,0.,0.],jp2(0)+[-1.,-1.,1.,1.,-1.],color=loopcol
 endfor
endif

print,'__________________________________________________________________'
print,'|		DRAWING NEW LOOP IN STEREO A			|'
print,'__________________________________________________________________'
!p.position=panel1
!x.range=xrange1
!y.range=[j1,j2]
plot,[0,0],[0,0]
iloop   =nloop
print,'Trace point #'+string(iloop+1,'(i4)')
print,' Click continuous positions in A [right panel]'
cursor,xp,yp
wait,0.2
ip	=long(xp+0.5)
jp	=long(yp+0.5)
print,'read coordinates x,y='+string(ip,'(f6.1)')+','+string(jp,'(f6.1)')+' pixels'
oplot,[ip,ip],[jp,jp],psym=1
x_a	=ip-di+findgen(ni)
f_a	=reform(images_fov(ip-i1-di:ip-i1+di,jp-j1,0))
for dj=-1,1 do f_a=f_a+reform(images_fov(ip-i1-di:ip-i1+di,jp-j1+dj,0))
ymax	=max(f_a,im)
xmax	=ip-di+im
if (di eq 1) then parabol_max,x_a,f_a,xmax_a,ymax_a,xx_a,yy_a
if (di ge 2) then begin
 dres	=0.1
 nni	=long(ni/dres)
 xx_a	=ip-di+dres*findgen(nni)
 yfit	 =spline(x_a,f_a,xx_a)
 ymax	=max(yfit,im)
 xmax_a=ip-di+dres*im
endif
oplot,ip(0)-di+[0.,ni-1,ni-1,0.,0.],jp(0)+[-1.,-1.,1.,1.,-1.],color=loopcol
print,'A centroid = ',xmax,'-->',xmax_a
xp	=xmax_a
plot,[xp,xp],[yp,yp],psym=1,symsize=2,thick=2,color=loopcol

print,'__________________________________________________________________'
print,'|		TRACING CORRESPONDING LOOP IN STEREO B		|'
print,'__________________________________________________________________'
!p.position=panel2
!x.range=xrange2
plot,[0,0],[0,0]
euvi_stereo_rot,xp,yp,para,1.,xb,yb,zb
oplot,[xb,xb],[yb,yb],psym=4,color=loopcol
print,' Click continuous positions in B [left panel] (end outside)'
cursor,xp2,yp2
wait,0.2
ip2	=long(xp2+0.5)
jp2	=jp 	;force same j-row
print,'read coordinates x,y='+string(ip2,'(f6.1)')+','+string(jp2,'(f6.1)')+' pixels'
f_b0	=reform(images_fov(ip2-i1_b-di:ip2-i1_b+di,jp2-j1,0))
f_max	=max(f_b0,imax)
ip2	=ip2+(imax-1)
oplot,[ip2,ip2],[jp2,jp2],psym=1
x_b	=ip2-di+findgen(ni)
f_b	=reform(images_fov(ip2-i1_b-di:ip2-i1_b+di,jp2-j1,0))
for dj=-1,1 do f_b=f_b+reform(images_fov(ip2-i1_b-di:ip2-i1_b+di,jp2-j1+dj,0))
ymax	=max(f_b,im)
xmax	=ip2-di+im
if (di eq 1) then parabol_max,x_b,f_b,xmax_b,ymax_b,xx_b,yy_b
if (di ge 2) then begin
 dres	=0.1
 nni	=long(ni/dres)
 xx_b	=ip2-di+dres*findgen(nni)
 yfit	 =spline(x_b,f_b,xx_b)
 ymax	=max(yfit,im)
 xmax_b =ip2-di+dres*im
endif
oplot,ip2(0)-di+[0.,ni-1,ni-1,0.,0.],jp2(0)+[-1.,-1.,1.,1.,-1.],color=loopcol
print,'B centroid = ',xmax,'-->',xmax_b
xp2	=xmax_b
plot,[xp2,xp2],[yp2,yp2],psym=1,symsize=2,thick=2,color=loopcol

window,1,xsize=512,ysize=512
!p.position=[0.1,0.1,0.5,0.9]
!y.range=minmax(f_a)*[1,1.2]
!x.range=minmax(x_a)+[-1,+1]
plot,x_a,f_a,psym=1,thick=3,symsize=3
oplot,xx_a,yy_a
oplot,xmax_a*[1,1],[0,ymax_a],linestyle=2
!noeras=1
!p.position=[0.55,0.1,0.95,0.9]
!y.range=minmax(f_b)*[1,1.2]
!x.range=minmax(x_b)+[-1,+1]
plot,x_b,f_b,psym=1,thick=3,symsize=3
oplot,xx_b,yy_b
oplot,xmax_b*[1,1],[0,ymax_b],linestyle=2

print,'__________________________________________________________________'
print,'|		STEREOSCOPIC HEIGHT MEASUREMENT 		|'
print,'__________________________________________________________________'
euvi_stereo_rot,xp,yp,para,1.   ,x_b0,y_b0,z_b0
euvi_stereo_rot,xp,yp,para,r_max,x_b1,y_b1,z_b1
dx_error=0.5
h_alt   =((r_max-1.0)*(xp2-x_b0)/(x_b1-x_b0)) ;interpol [1.0,1.1]*R_sun
dh_alt  =((r_max-1.0)*dx_error/(x_b1-x_b0))<hmax ;error (in units of R_sun)
rp      =(1.0+h_alt)*rad_pix  
hp	=h_alt*rad_pix
dhp 	=dh_alt*rad_pix  
zp	=sqrt(rp^2-(xp-crpix1)^2-(yp-crpix2)^2)
print,'Altitude h+/-dh = '+string(h_alt*696.,'(f6.1)')+'+'$
                          +string(dh_alt*696.,'(f6.1)')+' Mm'
read,'Save loop ? [0,1] = ',save

print,'__________________________________________________________________'
print,'|		SAVE LOOP COORDINATES 				|'
print,'__________________________________________________________________'
if (save eq 1) then begin
 openw,1,outfile,/append 
 printf,1,iloop,xp,yp,zp,hp,dhp,h_alt*696.,dh_alt*696.,nsm,format='(i6,7f8.1,i6)'
 close,1
 print,'nloop = '+string(iloop+1,'(i3)')+'  outfile = '+outfile
endif

end
