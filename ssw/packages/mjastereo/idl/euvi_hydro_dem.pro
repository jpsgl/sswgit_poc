pro euvi_hydro_dem,loopfile,hydrofile,para,sc,fluxobs,fluxdem,te_log,dem_log
;+
; Project     : STEREO
;
; Name        : EUVI_HYDRO_PROJECT 
;
; Category    : Graphics 
;
; Explanation : This routine produces a plot with the 3D coordinates of
;		a loop projected in any arbitrary direction.
;
; Syntax      : IDL> 
;
; Inputs      : 
;
; Outputs     : 
;
; History     : Oct/Nov 2007, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

;_______________INPUT PARAMETERS________________________________________
cdelt1	=para.cdelt1
crpix1	=para.crpix1
crpix2	=para.crpix2
rad   	=para.rsun
rad_pix	=rad/cdelt1
dh_max	=0.05*696.	;Mm
hchr    =2./(cdelt1*0.725) ;Mm
pix	=cdelt1*0.725 ;pixel-->Mm
fluxobs	=fltarr(3)
fluxdem	=fltarr(3)
if (sc eq 'a') then isc=0
if (sc eq 'b') then isc=1
nbin	=n_elements(te_log)
dem44	=fltarr(nbin)
dem_log	=fltarr(nbin)

;_______________READ HYDROFILE__________________________________________
restore,hydrofile
	;save,filename=hydrofile,image0,image1,para_model,fov,para,nmulti,$
        ;nloopmax,nk,em44_min,w_km,temin,temax,frac_min
for iwave=0,2 do fluxobs(iwave)=total(image0(*,*,iwave))
tel_	=para_model(*,0)
h0_	=para_model(*,1)
lam_	=para_model(*,2)
h1_	=para_model(*,3)
lam1_	=para_model(*,4)
em44_	=para_model(*,5)
l_	=para_model(*,6)
htop_	=para_model(*,7)
w_	=para_model(*,8)
ds_	=para_model(*,9)
loop_model=para_model(*,10)
nloop_model=n_elements(loop_model)

;______________PLOT PREVIOUS LOOPS______________________________________
file_exist=findfile(loopfile,count=nfiles)
if (nfiles eq 0) then print,'No previous loopfile found';
print,'Datafile with loop coordinates found  LOOPFILE=',loopfile 

idat	=strpos(loopfile,'.dat')
isav	=strpos(loopfile,'.sav')
if (idat gt 0) then readcol,loopfile,iloop_,ix_,iy_,iz_,ih_,iz_raw,dh_,nsm_,wave_,chi2_,q1_,q2_
if (isav gt 0) then restore,loopfile ;-->iloop_,ix_,iy_,iz_
nloop  =max(iloop_)+1 
print,'Number of previously traced loops    NLOOP   =',nloop

;_______________________BACKGROUND_____________________________________
backgr  =fltarr(3)
for iwave=0,2 do begin
 backgr(iwave)=min(image0(*,*,iwave))
endfor
dim	=size(image0)
nx	=dim(1)
ny	=dim(2)
image1	=fltarr(nx,ny,3)
fluximage=fltarr(3)
for iwave=0,2 do begin
 fluxdem(iwave)=backgr(iwave)*float(nx)*float(ny)
 fluximage(iwave)=backgr(iwave)
endfor

;_______________________READ EUVI RESPONSE FUNCTION____________________
nte_    =100
te_     =3.*findgen(nte_)/float(nte_)    ;MK loop temperature
wave1	='171'
wave2	='195'
wave3	='284'
euvi_temp_resp,wave1,sc,te_,resp171,1 ;EUVI response function
euvi_temp_resp,wave2,sc,te_,resp195,1 ;EUVI response function
euvi_temp_resp,wave3,sc,te_,resp284,1 ;EUVI response function

for il=0,nloop_model-1 do begin 
  iloop	=long(loop_model(il))
  ind	=where(iloop_ eq iloop,np) 
  if (np le 3) then goto,endloop
  xp     =ix_(ind)	;in pixels
  yp     =iy_(ind)
  zp     =iz_(ind)
  euvi_spline,para,xp,yp,zp,x,y,z,d,r,r_n
  n      =n_elements(x)
  s      =fltarr(n)
  for is=1,n-1 do s(is)=s(is-1)+sqrt((x(is)-x(is-1))^2+$
         (y(is)-y(is-1))^2+(z(is)-z(is-1))^2)
  ds	 =ds_(il)
  ns     =long(max(s)/ds)
  s_     =ds*findgen(ns)			 ;step ds=1 pixel
  xs     =interpol(x,s,s_)			;xp_rot interpolated to ds=1
  ys     =interpol(y,s,s_)		        ;yp_rot interpolated to ds=1
  rs     =interpol(r,s,s_)
  hs     =rs-rad_pix				;hs in pixels
  h      =hs>0
  s      =s_
  htop   =max(hs,itop)
  tchr   =0.01                           ;[MK]
  rsun   =6.96                           ;solar radius [100 Mm]
  hchr   =2./(cdelt1*0.725)
  te     =tel_(il) >tchr                  ;loop top temp [MK]
  h0     =(h0_(il)>hchr)<htop            ;loop footpoint height [pix]
  lam    =(lam_(il)<rsun)>(-rsun)        ;scale height [100 Mm]
  h1     =(h1_(il)>hchr)<htop            ;loop footpoint height [pix]
  lam1   =(lam1_(il)<rsun)>(-rsun)       ;scale height [100 Mm]
  l      =(s(itop)-s(0))>(s(ns-1)-s(itop))
  lambda =(lam/rsun)*rad_pix             ;scale height [pixels]
  lambda1=(lam1/rsun)*rad_pix            ;scale height [pixels]
  s_l    =(s-hchr)/(l-hchr)
  eps    =(tchr/te)^(7./2.)
  temp_model=tchr+(te-tchr)*( (s_l*(2.-s_l)) > eps )^(2./7.)
  ind2   =where(s gt s(itop),nind2)
  if (nind2 ge 1) then $
  temp_model(ind2)=tchr+(te-tchr)*( (s_l(ind2)*(2.-s_l(ind2))) > eps )^(2./7.)
  p1_h   =exp(-(h-hchr)/lambda)
  p1_top =exp(-(htop-hchr)/lambda)
  if (nind2 ge 1) then p1_h(ind2)=p1_top*exp((htop-h(ind2))/lambda1)
  ind3   =where((s le s(itop)) and (h lt h0),nind3)
  ind4   =where((s gt s(itop)) and (h lt h1),nind4)
  if (nind3 ge 1) then p1_h(ind3)=0.
  if (nind4 ge 1) then p1_h(ind4)=0.
  n1_h	 =p1_h/temp_model
  em1_h  =n1_h^2
  resp1  =interpol(resp171,te_,temp_model)>0
  resp2  =interpol(resp195,te_,temp_model)>0
  resp3  =interpol(resp284,te_,temp_model)>0
  w_sm	 =7<(ns-1)
  fluxprof1=smooth(em1_h*resp1,w_sm)	
  fluxprof2=smooth(em1_h*resp2,w_sm)	
  fluxprof3=smooth(em1_h*resp3,w_sm)	
  em44	 =em44_(il)
  flux_model=fltarr(ns,3)
  flux_model(*,0)=em44*fluxprof1
  flux_model(*,1)=em44*fluxprof2
  flux_model(*,2)=em44*fluxprof3
  for iwave=0,2 do fluxdem(iwave)=fluxdem(iwave)+total(flux_model(*,iwave))

  temp_log =alog10(temp_model)+6.0
  w	=w_(il)
  vol24 =ds*w^2*pix^3                 ;voxel volume [Mm^3]
  htop8 =htop_(il)*pix
  hchr8 =hchr*pix
  lam8	=lam*100.
  lam18	=lam1*100.
  ne1   =sqrt(10.^(alog10(em44)+44.-alog10(vol24)-24.)) ;density [cm-3]
  ne0   =ne1*exp(-(htop8-hchr8)/(lam8))
  ne2   =((ne0*exp(+(htop8-hchr8)/(lam18)))>1.e8)<1.e11
  ne10  =ne1*n1_h/1.e10

  for it=0,nbin-2 do begin
   ind	=where((temp_log ge te_log(it)) and (temp_log lt te_log(it+1)),nind)
   if (nind ge 1) then dem44(it)=dem44(it)+total(ne10(ind)^2*vol24)
  endfor

  if (il mod 100) eq 0 then print,'loop#, flux, DEM : ',$
      il,fluxdem/fluxobs,max(dem44),format='(a,i8,3f8.3,e10.2)'
endloop:
endfor
te	=10.^te_log
dte	=shift(te,-1)-te
dte(nbin-1)=dte(nbin-2)^2/dte(nbin-3)	
dem_log	=alog10(dem44/dte)+44.
end
