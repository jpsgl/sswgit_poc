pro nlfff_prof_step2,time,efree,nloop,t5,dtol,t,s,e1,e2,te1,te2,err2

;time profile of free energy is obtained from median and boxcar smoothing,
;and the energy decrease from E1 to E2 with error ERR
;at times TE1,TE2 ;during time range TIME[IND_STEP] 

;_______________________ENERGY DISSIPATION______________________
t3_hr	=t5[0]
t1_hr	=t5[1]
t0_hr	=t5[2]
t2_hr	=t5[3]
t4_hr	=t5[4]
dt	=time(1)-time(0)
nmed	=long(2.*0.1/dt) > 5
nmed2	=long(nmed/2)
ind	=where((efree gt 0),nt)
if (nt ge 5) then begin
 t	=time(ind)
 e	=efree(ind)
 emed	=fltarr(nt)
 for it=0,nt-1 do begin
  it1	=(it-nmed2) > 0
  it2	=(it+nmed2) < (nt-1)
  emed(it)=median(e(it1:it2))
 endfor  
 emed(0:1)=emed(2)
 emed(nt-2:nt-1)=emed(nt-3)
 nsm	=(nmed < (nt-1) )>1
 s 	=smooth(emed,nsm)
 ind1	=where((e gt 0) and (t le t0_hr) and (t ge t1_hr-dtol))
 ind2	=where((e gt 0) and (t gt t0_hr) and (t le t2_hr+dtol))
 e1	=max(s(ind1),ie1)
 e2	=min(s(ind2),ie2)
 te1	=t(ind1(ie1))
 te2	=t(ind2(ie2))
 it1	=ind1(ie1)
 it2	=ind2(ie2)
 ind	=findgen(nt)

 dy	=abs(s-e)
 isort	=sort(dy)
 isig	=long(nt*0.67)
 err2	=dy(isort(isig))
endif

if (nt lt 5) then begin
 t	=time(0:nt-1)
 e	=efree(0:nt-1)
 emed	=fltarr(nt)
 s	=fltarr(nt)
 e1	=0.
 e2	=0.
 te1	=time(0)
 te2	=time(1)
 err2	=0.
endif
end
