pro string0,ndigit,nr,str
;converts nr into string format and fills up zeros to ndigit

str	=strtrim(string(nr,'(i20)'),2)
n	=strlen(str)
if (ndigit gt n) then for i=0,ndigit-n-1 do str='0'+str 
end
