pro euvi_contour,image_pair,para,nsm,fov,hmax,loopfile,filter,window,sc,cont,dcont,ct,gamma,inv

;+
; Project     : STEREO
;
; Name        : EUVI_CONTOUR 
;
; Category    : Graphics 
;
; Explanation : This routine displays the xy-coordinates of loops (from LOOPFILE)
;		onto a colorscale or contourplot of the STEREO images (IMAGE_PAIR).
;
; Syntax      : IDL>euvi_contour,image_pair,para,nsm,fov,hmax,loopfile,filter,window,sc,cont,dcont,ct
;
; Inputs      : image_pair(2,*,*)  : stereo image pair B,A
;		para               : index structure with image parameters 
;		nsm		   : smoothing parameter for highpass filter
;		fov		   : [i1,j1,i2,j2] field-of-view in image pixels 
;		hmax   		   : 0.1 ;range of alitudes (in solar radii)
;		loopfile	    : 'loop_A.dat' datafile containing x,y,z coordinates
;		filter		    : 0=highpass filter, -1=linear, -2=log scale of flux
;		window		   : 0,1,2,3 IDL screen nr
;		sc		   : 'a' or 'b', STEREO spacecraft
;		cont		   : 0=greyscale plot, 1=contour plot
;		dcont		   : lowest contour level 
;		ct		   : 0, ..., 38 IDL color table 
;               gamma		   : =0.5 (slope of color table)
;               inv		   : inversion of color table if inv=-1
;
; Outputs     : postscript file of plot with name 'cont_col.ps' or 'over_col.ps'
;
; History     : Oct/Nov 2007, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-
if n_params(0) le 12 then gamma=0.5
if n_params(0) le 13 then inv  =0.

;________________________________________________________________
;			SUBIMAGES  				|
;________________________________________________________________
rsun_m  =0.696*1.e9
cdelt1	=para.cdelt1
cdelt2	=para.cdelt2
crpix1	=para.crpix1
crpix2	=para.crpix2
da	=para.da/rsun_m
db	=para.db/rsun_m
rsun   	=para.rsun
a_sep	=para.sep_deg*(!pi/180.)
rad_pix	=rsun/cdelt1
r_max   =1.0+hmax
i1	=fov(0)
j1	=fov(1)
i2	=fov(2)
j2	=fov(3)
nx	=(i2-i1+1)
ny	=(j2-j1+1)
ism	=(nsm-3)/2			;nsm=3,5,7-->ism=0,1,2
r_n	=1.0
im	=(i1+i2)/2.
jm	=(j1+j2)/2.
rm	=sqrt((im-crpix1)^2+(jm-crpix2)^2)
im_b	=im
jm_b	=jm
if (rm lt rad_pix) then euvi_stereo_rot,im,jm,para,r_n,im_b,jm_b,zm_b
i1_b	=im_b-(im-i1)
i2_b	=i1_b+(i2-i1)
if (sc eq 'a') then xrange=[i1  ,i2  ]+[-0.5,+0.5]
if (sc eq 'b') then xrange=[i1_b,i2_b]+[-0.5,+0.5]
print,'xrange=',xrange
yrange	=[j1,j2]
if (sc eq 'a') then image0=float(reform(image_pair(1,i1:i2,j1:j2)))
if (sc eq 'b') then image0=float(reform(image_pair(0,i1:i2,j1:j2)))
euvi_imagefilter,filter,nsm,image0,image,gamma,inv
zoom	=long((2000/(nx)) < (1600/(ny)))
th	=3+zoom/2


print,'__________________________________________________________________'
print,'|		PLOT STEREO IMAGE PAIR  			|'
print,'__________________________________________________________________'
for io=0,3,3 do begin
if (io eq 0) then begin
 xsize	=zoom*nx
 ysize	=zoom*ny
 window,window,xsize=xsize,ysize=ysize
 set_plot,'x'
 !p.font=-1
 char	=2.0
 x1_	=0.0
 y1_	=0.0
 x2_	=1.0
 y2_	=1.0
endif
ipos	 =strpos(loopfile,'trace')
if (io ge 1) then begin
 form    =1     ;landscape format
 char    =1.0     ;smaller character size
 if (cont eq 0) then fignr='over'
 if (cont eq 1) then fignr='cont'
 plotname=strmid(loopfile,0,ipos)
 unit    =0
 ref     =''
 fig_open,io,form,char,fignr,plotname,unit
 x1_	=0.05
 y1_	=0.15
 x2_	=0.95
 y2_	=0.85
endif

!p.position=[x1_,y1_,x2_,y2_]
!x.range=xrange
!y.range=yrange		
!x.style=1
!y.style=1
!p.title=loopfile
!x.title='EW direction [pixel]'
!y.title='NS direction [pixel]'
plot,[0,0],[0,0]

if (cont eq 0) then begin
 loadct,(ct>0)
 if (io eq 0) then begin
  nxw  =long(!d.x_vsize*(x2_-x1_)+0.5)
  nyw  =long(!d.y_vsize*(y2_-y1_)+0.5)
  ima  =congrid(image,nxw,nyw)
 endif
 if (io ne 0) then ima=image
 simage =smooth(image,21)
 z1	=min(simage)
 z2	=max(simage)
 tv,bytscl(ima,min=z1,max=z2),x1_,y1_,xsize=(x2_-x1_),ysize=(y2_-y1_),/normal
endif

if (cont eq 1) then begin
 loadct,0
 xx	=!x.range(0)+findgen(nx)
 yy	=!y.range(0)+findgen(ny)
 zz2	=max(image)
 zz1	=dcont>min(image)
 zz	=zz1+(zz2-zz1)*findgen(11)/10.
 contour,float(image),xx,yy,level=zz
 print,'minmax(image)=',minmax(image)
 print,'contour levels = ',zz
endif
if (sc eq 'a') then sc_cap='A'
if (sc eq 'b') then sc_cap='B'
xyouts_norm,0.02,0.93,sc_cap,3,0
x1	=!x.range(0)
x2	=!x.range(1)-0.1
y1	=!y.range(0)
y2	=!y.range(1)-0.1
oplot,[x1,x2,x2,x1,x1],[y1,y1,y2,y2,y1]
!noeras=1

print,'__________________________________________________________________'
print,'|		PLOT PREVIOUS LOOPS 				|'
print,'__________________________________________________________________'
file_exist=findfile(loopfile,count=nfiles)
if (nfiles eq 0) then stop,'No previous loopfile found';

readcol,loopfile,iloop_,ix_,iy_,iz_,ih_,ih_raw,dh_,nsm_,wave_,chi2_,q1_,q2_
euvi_loop_renumerate,iloop_,wave_
nloop	=long(max(iloop_))+1
if (nloop eq 0) then stop,'loopfile empty: ',loopfile
print,'Datafile with loop coordinates found  LOOPFILE=',loopfile 
print,'Number of loops found                NLOOP   ='+string(nloop,'(i3)')
loadct,5
for iloop=0,nloop-1 do begin 
 ind	=where(iloop_ eq iloop,np) 
 xp	=ix_(ind)
 yp	=iy_(ind)
 zp	=iz_(ind)
 loopcol=50.*wave_(ind(0))
 euvi_spline,para,xp,yp,zp,x,y,z,d,r,r_n
 n	=n_elements(x)
 euvi_stereo_rot,x,y,para,r_n,x_b,y_b,z_b
 r0_n	=1.0
 euvi_stereo_rot,x,y,para,r0_n,x0_b,y0_b,z0_b
 r1_n	=1.0+hmax
 euvi_stereo_rot,x,y,para,r1_n,x1_b,y1_b,z1_b

 if (sc eq 'a') then begin
  oplot,x,y,color=loopcol,thick=th
  rmax=max(r,im)
  xyouts,x(im),y(im),string(iloop+1,'(I3)'),color=loopcol,size=char*0.7
 endif
 if (sc eq 'b') then begin
  oplot,x_b,y_b,color=loopcol,thick=th
  oplot,x0_b,y0_b,color=loopcol,thick=2
  oplot,x1_b,y1_b,color=loopcol,thick=2,linestyle=2
  rmax=max(r,im)
  xyouts,x_b(im),y_b(im),string(iloop+1,'(I3)'),color=loopcol,size=char*0.7
 endif
endfor
if (io ge 1) then fig_close,io,fignr,ref
endfor
end
