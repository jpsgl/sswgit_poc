pro helio_loopcoord,xl,yl,zl,rsun,az,th,l1,b1,r1,l,b,r

;transforms loop plane coordinates [xl,yl,zl] 
;into heliographic coordinates [l,b,r]

;xl[n],yl[n],zl[n = orthogonal distances in loop plane (in same length units as RSUN)
;rsun	     = solar radius in length units      
;az	     = azimuth angle of loop footpoint baseline to EW (deg)	
;th	     = inclination angle of loop plane to vertical (deg)
;l1,b1       = heliographic longitude and latitude of [xl=0,yl=0,zl=0]
;l[n],b[n]   = heliographic longitude and latitude coordinates
;r[n] 	     = distance from Sun center (in same units as RSUN)

;M.J.Aschwanden, March 1998, UMd

n	=n_elements(xl)
;conversion deg-->rad
q	=(!pi/180.)
th_	=replicate(th,n)*q
az_	=replicate(az,n)*q
if (n_elements(l1) eq 1) then l1_=replicate(l1,n)
if (n_elements(l1) gt 1) then l1_=l1
;rotation by inclination angle 
x1	= xl
y1	= yl*cos(th_)+zl*sin(th_)
z1	=-yl*sin(th_)+zl*cos(th_)
;rotation by azimuth angle 
x2	=x1*cos(az_)-y1*sin(az_)
y2	=xl*sin(az_)+y1*cos(az_)
z2	=z1
;heliographic coordinates
l	=l1_+atan(x2/(r1+z2))/q
b	=b1+atan(y2/(r1+z2))/q
r	=z2+r1
end
