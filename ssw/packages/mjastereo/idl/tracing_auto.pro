pro tracing_auto,image1,image2,loopfile,para,output,test
;+
; Project     : SOHO, TRACE, STEREO, HINODE, SDO
;
; Name        : TRACING_AUTO
;
; Category    : Automated 2D Pattern Recognition
;
; Explanation : An input image (IMAGE1) is highpass-filtered (IMAGE2)
;		and automatically traced for curvi-linear structures,
;		whose [x,y] pixel coordinates are stored in LOOPFILE.
;		The algorithm iteratively starts to trace a loop from
;		the position of the absolute flux maximum in the image,
;		tracks in a bi-directional way by oriented directivity,
;		based on the direction of the ridge with maximum flux.
;
; Syntax      : IDL> tracing_auto,image1,image2,loopfile,para,test
;
; Inputs      : image1   - input image 
;               para     - [nsm,len0,nlen,wid,nsig,qfill,nmax]
;			   contains algorithm control parameters, such as:
;			     nsm  = highpass filter
;			     len0  = minimum length of loop segments
;			     nlen = size of moving box [pixels]
;			     wid  = half width of loop [pixels]
;			     nsig = threshold for starting detection of loop 
;		             qfill =minimum filling ratio (flux_avg/flux_peak)
;			     nmax = maximum number of analyzed structures
;               test     - flag for no test (0) or interactive test output (1)
;
; Outputs     : image2	 - output image of highpass-filtered image
;               loopfile - output file containing loop coordinates
;                          with consecutive loop numbering
;
; Procedures called inside this routine:
;		TRACING_DIRECTION.PRO
;		TRACING_STEP.PRO
;
; History     : 16-Oct-2009, OCCULT-1 version written by Markus J. Aschwanden
;             :  8-May-2013, OCCULT-2 version 
;
; Contact     : aschwanden@sag.lmsal.com
;-

;UNPACK CONTROL PARAMETERS________________________________________________
nsm	=para(0)
len0	=para(1)
rmin	=para(2)
nsig	=para(3)
qfill	=para(4)
nmax	=para(5)
wid     =long((nsm-3)/2)
nlen    =long(sqrt(8*rmin*wid))

;MINIMUM FLUX CORRECTION__________________________________________________
dim     =size(image1)
nx      =dim(1)
ny      =dim(2)
fluxmed =median(image1)
fluxmin =min(image1)
fluxmax =max(image1)
qarea	=1.00

;HIGHPASS FILTER AND THRESHOLD_____________________________________________
image2  =image1-smooth(image1,nsm) 
statistic,image2,avg,dev_sigma
thresh0	=avg+nsig*dev_sigma
print,'Threshold flux = ',thresh0
openw,2,loopfile
close,2

;LOOP TRACING______________________________________________________________
count	=0
residual=image2
for nloop=0,nmax-1 do begin
 zmax	=max(residual,im) 
 if (nloop mod 100) eq 0 then begin
  ipix	=where(residual ge thresh0,npix)
  qpix	=float(npix)/(float(nx)*float(ny))
  print,'Struc#'+string(nloop,'(I7)')+' Loop#'+string(count,'(I5)')+$
   ' Flux='+string(zmax,'(f10.2)')+' F/Th='+string(zmax/thresh0,'(f6.1)')+$
   ' Remaining='+string(qpix,'(f6.4)') 
 endif
 jstart	=long(im/nx)
 istart	=long(im mod nx)
 tracing_step,residual,istart,jstart,nlen,ang,thresh0,qfill,test,xloop,yloop,zloop,wid,count
 np	=n_elements(xloop)
 if (np le 1) then looplen=0.
 if (np eq 2) then looplen=sqrt((xloop(1)-xloop(0))^2+(yloop(1)-yloop(0))^2)
 if (np ge 3) then looplen=total(sqrt((xloop(1:np-1)-xloop(0:np-2))^2+$
                                      (yloop(1:np-1)-yloop(0:np-2))^2))
 x0	=(max(xloop)+min(xloop))/2.
 y0	=(max(yloop)+min(yloop))/2.
 dx	=(max(xloop)-min(xloop))/2.
 dy	=(max(yloop)-min(yloop))/2.
 dd	=(dx>dy)>(nlen/2)
 i1	=long(x0-dd*1.5)>0
 i2	=long(x0+dd*1.5)<(nx-1)
 j1	=long(y0-dd*1.5)>0
 j2	=long(y0+dd*1.5)<(ny-1)
 subimage=residual(i1:i2,j1:j2)
 z0	=max(subimage)>1.
 xfov   =i1+findgen(i2-i1+1)
 yfov   =j1+findgen(j2-j1+1)
  if (count ge test) then begin
   print,'Loop start: #,i,j,np=',nloop,istart,jstart,np
   window,2,xsize=1024,ysize=1024
   clearplot
   !x.range=[i1,i2]
   !y.range=[j1,j2]
   !x.style=1
   !y.style=1
   !p.title='Loop #'+string(nloop,'(i4)')
   plot,[i1,i2],[j1,j2]
   zmax=max(subimage)
   contour,subimage,xfov,yfov,level=thresh0+(zmax-thresh0)*(findgen(11)/10.)
   oplot,istart*[1,1],jstart*[1,1],psym=1,symsize=10,color=128,thick=2
   oplot,xloop,yloop,thick=3,color=128
   oplot,xloop,yloop,thick=3,color=128,psym=1,symsize=2
   xyouts,xloop(np-1),yloop(np-1),string(nloop,'(i3)'),size=char
   read,'continue? [0,1] : ',yes
   if (yes eq 0) then stop
  endif
 statistic,xloop,xav,xdev
 statistic,yloop,yav,ydev
 if (xdev lt 1) then looplen=0    ;vertical column artifact
 if (ydev lt 1) then looplen=0    ;horizontal row  artifact
 if (looplen ge len0) then begin
  openw,2,loopfile,/append
  nskip	=4
  for ip=0,np-2,nskip do printf,2,count,xloop(ip),yloop(ip),zloop(ip)
                         printf,2,count,xloop(np-1),yloop(np-1),zloop(np-1)
  close,2
  count=count+1
  if (zmax lt thresh0) then goto,end_trace
 endif				  ;if (looplen ge len0) then begin

;ERASE LOOP IN RESIDUAL IMAGE____________________________________________
 i3	=(istart-wid)>0
 i4	=(istart+wid)<(nx-1)
 j3	=(jstart-wid)>0
 j4	=(jstart+wid)<(ny-1)
 residual(i3:i4,j3:j4)=0.		;in case of no valid loop
 nn	=n_elements(xloop)
 if (nn ge 2) then begin
  xloop1	=2.*xloop(0)-xloop(1)
  xloop2	=2.*xloop(nn-1)-xloop(nn-2)
  yloop1	=2.*yloop(0)-yloop(1)
  yloop2	=2.*yloop(nn-1)-yloop(nn-2)
  spline_p,[xloop1,xloop,xloop2],[yloop1,yloop,yloop2],xx,yy  ;increase res by 8
  ns	=n_elements(xx)					      ;because step=4
  nw	=(wid*2+1)
  array_curve,residual,xx,yy,nw,xw,yw,zw 
  for is=0,ns-1 do begin
   for iw=0,nw-1 do begin
    i_	=(long(xw(is,iw)+0.5)<(nx-1))>0 
    j_	=(long(yw(is,iw)+0.5)<(ny-1))>0 
    residual(i_,j_)=0.
   endfor
  endfor
 endif

;TEST OUTPUT_______________________________________________________________
 if (count ge test) then begin
  window,3,xsize=1024,ysize=1024
  clearplot
  !x.range=minmax(xfov)
  !y.range=minmax(yfov)
  subimage=residual(i1:i2,j1:j2)
  plot,[0,0],[0,0]
  zmax=max(subimage)
  contour,subimage,xfov,yfov,level=thresh0+(zmax-thresh0)*(findgen(11)/10.)
  oplot,istart*[1,1],jstart*[1,1],psym=1,symsize=10,color=128,thick=2
  oplot,xloop,yloop,thick=3,color=128
  oplot,xloop,yloop,thick=3,color=128,psym=1,symsize=2
  read,'continue? [0,1] : ',yes
  if (yes eq 0) then stop
 endif
endfor
end_trace:
output  =[wid,nlen,fluxmin,fluxmax,fluxmed,qarea,thresh0]
end
