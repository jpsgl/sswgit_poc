pro euvi_event_para,id,parfile,nfilemax,nxx,dtstart,dur,date,t1,t2,sc,pos,sep,fov_,ibad 

;reads flare parameter catalog file____________________________________
nsel	=0
ibad	=lonarr(2,3,nfilemax)
openr,1,parfile
while not eof(1) do begin
 line_read=' '
 readf,1,line_read
 id_=strmid(line_read,0,3)
 if (long(id_) eq id) then begin
  nsel	=nsel+1
  if (nsel eq 1) then line=line_read
  if (nsel ge 2) then begin
   sc  =strmid(line_read,68,1)
   wave=strmid(line_read,70,3)
   iframe=long(strmid(line_read,74,3))
   if (sc eq 'A') then isc=0
   if (sc eq 'B') then isc=1
   if (wave eq '171') then iwave=0
   if (wave eq '195') then iwave=1
   if (wave eq '284') then iwave=2
   ibad(isc,iwave,iframe)=1
   print,'SC=',sc,' WAVE=',wave,' IT_BAD=',iframe
  endif
 endif
endwhile
close,1

;extracts parameters from string_______________________________________
yy	=strmid(line,5,4)
month	=strmid(line,10,3)
month_	=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
im	=where(month eq month_)
if (im le 8) then mm='0'+string(im(0)+1,'(i1)')
if (im ge 9) then mm=string(im(0)+1,'(i2)')
dd	=strmid(line,14,2)
date	=yy+mm+dd
t1	=strmid(line,18,2)+strmid(line,21,2)
t2	=strmid(line,24,2)+strmid(line,27,2)
ab	=strmid(line,45,2)
sc	=' '
if (ab eq 'A ') then sc='a'
if (ab eq ' B') then sc='b'
if (ab eq 'AB') then sc='ab'
pos	=strtrim(strmid(line,29,10),2)

;pixel range of FOV______________________________________________
datadir='$secchi/lz/L0/'
expmax	=10
movie	=0
color	=-1	;no display of images
wave	='171'
euvi_files,datadir,date,t1,t2,'a',wave,file_a,nfilemax,expmax
euvi_files,datadir,date,t1,t2,'b',wave,file_b,nfilemax,expmax
euvi_display,file_a(0),color,movie,image_a,index_a
euvi_display,file_b(0),color,movie,image_b,index_b
euvi_pos,parfile,index_a,id,nxx,pos_a,fov_a
euvi_pos,parfile,index_b,id,nxx,pos_b,fov_b
fov_	=[[fov_a],[fov_b]]

;spacecraft separation angle_____________________________________
euvi_stereoangle,date,sep

;extended time interval__________________________________________
t1_min  =long(strmid(t1,2,2))+60*long(strmid(t1,0,2))
t2_min  =long(strmid(t2,2,2))+60*long(strmid(t2,0,2))
t3_min  =long(t1_min+dtstart)
t4_min  =long(t3_min+dur)
string0,2,t3_min/60,t3_hh
string0,2,t3_min mod 60,t3_mm
string0,2,t4_min/60,t4_hh
string0,2,t4_min mod 60,t4_mm
t1      =t3_hh+t3_mm
t2      =t4_hh+t4_mm
end
