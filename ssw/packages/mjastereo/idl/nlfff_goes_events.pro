pro nlfff_goes_events,epoch_start,epoch_end,efile,above,iev1

;	Purpose: generates GOES event catalog in selected epoch
;		 with GOES class above some level (M1, X1)
;
;	Input:	 epoch_start='2014-Mar-29 00:00'
;		 epoch_end  ='2014-Mar-30 00:00'
;		 efile      ='goes_noaa_M1.txt'
;		 above	    ='M1'

gev	=get_gev(epoch_start,epoch_end)
print,'Number of all GOES flares : '
help,gev

decode_gev,gev,fstart,fstop,fpeak,class=class,above=above,noaa_ar=noaa,location=loc
n	=2000
t3	=epoch_start
t4	=epoch_end
search  =['FRM_NAME=SSW Latest Events','FL_GOESCLS>M']
q	=ssw_her_make_query(t3,t4,/fl,search=search,result_limit=n)
her	=ssw_her_query(q)
help,her.fl 
keys	='event_starttime,fl_goescls,event_coord1,event_coord2'
info	=get_infox(her.fl,keys,/more)
ew	=get_infox(her.fl,'event_coord1')
ns	=get_infox(her.fl,'event_coord2')
ts	=get_infox(her.fl,'event_starttime')

;more,fstart+'  '+fpeak+'  '+fstop+'  '+class+'   '+loc
n	=n_elements(fstart)
pos	='      '
openw,2,efile
for i=0,n-1 do begin
 iev	=iev1+i
 str	=string(iev,'(I4)')
 t1	=fstart(i)
 t0	=fpeak(i)
 t2	=fstop(i)
 goes	=class(i)
 ar0	='     '
 if (noaa(i) ne '') then ar0=noaa(i)
 if (noaa(i) ne '') then ar =noaa(i)
 pos0	='      '
 if (loc(i) ne '') then pos0=loc(i)
 if (loc(i) ne '') then pos =loc(i)

;Substitute heliographi position from SSW LATEST EVENTS'
 ind	=where(ts eq t1,nind)
 ii	=ind(0)
 if (ii ne -1) then begin
  string0,2,abs(ns(ii)),ns_str
  string0,2,abs(ew(ii)),ew_str
  if (ns(ii) ge 0) then posa='N'+ns_str
  if (ns(ii) lt 0) then posa='S'+ns_str
  if (ew(ii) ge 0) then posb='W'+ew_str
  if (ew(ii) lt 0) then posb='E'+ew_str
  pos2	=posa+posb
  if (pos eq '      ') then pos=pos2
 endif
 printf,2,str+'  '+t1+'  '+t0+'  '+t2+'  '+goes+'  '+ar+'  '+pos+'  '+ar0+'  '+pos0
 print   ,str+'  '+t1+'  '+t0+'  '+t2+'  '+goes+'  '+ar+'  '+pos+'  '+ar0+'  '+pos0
endfor
close,2
print,'Flare catalog file written = '+efile
end
