pro euvi_loopdem_plot,io,filename,isc,iloop,backgr,w_pix,fracmin

;_______________________INPUT______________________________
wave_	=['171','195','284'] ;Angstrom
sc_	=['A','B']
imagefiles=filename+wave_+'.sav'
loopfile =filename+'xyz.dat'  
s_iloop =strtrim(string(iloop,'(I3)'),2)
if (backgr eq -2) then s_backgr='__'
if (backgr eq -1) then s_backgr='_'
if (backgr ge 0) then s_backgr=string(backgr,'(I1)')
s_w     =strtrim(string(w_pix,'(I3)'),2)
plotname=filename+'dem_'
fignr0	 =s_iloop+s_w+s_backgr
outfile	 =plotname+fignr0+'.sav'
outfiles =findfile(outfile,count=nfound)
if (nfound eq 0) then euvi_loopdem,imagefiles,loopfile,iloop,backgr,w_pix,outfile
restore,outfile

;________________________PLOT_______________________________
form    =1      ;0=landscape, 1=portrait
char    =1.     ;character size
fignr	 =fignr0+sc_(isc)+'a'
ref     =''	;(Aschwanden et al. 2007)'  ;label at bottom of Fig.
unit    =0+2*isc;window number
ct	=3	;color table
nlev	=20	;number of contour levels
fig_open,io,form,char,fignr,plotname,unit
loadct,ct
q	=fltarr(3,2)
for iw=0,2 do begin
 ss	=s/l
 im	=nw/2
 ff	=f(*,im,isc,iw)
 bb	=b(*,im,isc,iw)
 q(iw,0)=total(ff)
 q(iw,1)=total(ff-bb)

 x1_	=0.07
 x2_	=x1_+0.4
 y1_	=0.7-0.3*iw
 y2_	=y1_+0.25
 !p.position=[x1_,y1_,x2_,y2_]
 !p.title=' '
 !x.range=[0,1]
 !y.range=[0,max(ff)]
 !x.title=''
 !y.title=''
 if (iw eq 0) then !x.title='position along loop  s/L'
 !y.title='flux  f(s) [DN/s]'
 plot,[0,0]
 polyfill,[ss,reverse(ss)],[bb,reverse(ff)],color=200-100*isc
 oplot,ss,ff,thick=3
 oplot,ss,bb
 dy	=!y.range(1)*0.05
 xyouts_norm,0.03,0.90,sc_(isc)+' '+wave_(iw),char*2
 xyouts,0.02,dy,'Background = '+method+' Method',size=char
 xyouts,0.02,3*dy,'flux ratio f/f_total='+string(q(iw,1)/q(iw,0),'(f5.3)'),size=char
 !noeras=1
endfor
flux_frac=total(q(*,1))/total(q(*,0))
xyouts,0.02,5*dy,'(f1+f2+f3)/f!Dtotal!N='+string(flux_frac,'(f5.3)'),size=char*1.4
print,'flux fraction=',flux_frac

;TEMPERATURE PROFILE______________________________________________
 tmin	=0.5 ;MK
 tmax	=2.5 ;MK
 te	=t(*,isc)
 dte	=dt(*,isc)
 ind	=where((frac(*,isc) ge fracmin) and (te gt tmin) and (te lt tmax),nind)
 qfrac	=float(nind)/float(ns)
 statistic,te(ind),t_av,t_sig
 statistic,dte(ind),dt_av,dt_sig
 lambda_t=47.*t_av

 x1_	=0.58
 x2_	=x1_+0.4
 y1_	=0.7-0.3*0
 y2_	=y1_+0.25
 !y.title='Temperature  T[MK]'
 !x.title='position along loop  s/L'
 !x.title=''
 !y.range=[0.,3.]
 !p.position=[x1_,y1_,x2_,y2_]
 plot,ss(ind),te(ind),thick=3,psym=1
 errplot,ss(ind),te(ind)-dte(ind),te(ind)+dte(ind)
 oplot,[0,1],[t_av,t_av],thick=3
 xyouts_norm,0.05,0.9,'T!Bp!N='+string(t_av,'(f4.2)')+'+'+string(t_sig,'(f4.2)')+' MK'
 xyouts_norm,0.05,0.8,'!9s!3!BDEM!N='+string(dt_av,'(f4.2)')+'+'+string(dt_sig,'(f4.2)')+' MK'
 xyouts_norm,0.65,0.9,'!9l!3!BT!N='+string(lambda_t,'(f5.1)')+' Mm'
 xyouts_norm,0.65,0.8,'q!Bfrac!N='+string(qfrac,'(f5.2)')
 xyouts_norm,0.65,0.7,'(f/b)>'+string(fracmin,'(f5.3)')
 print,t_av,t_sig,dt_av,dt_sig

;DENSITY PROFILE___________________________________________________
 n0_fit	 =avg(n(ind,isc)/exp(-h(ind)/lambda_t))
 nt_fit	 =n0_fit*exp(-h/lambda_t)
 qn	=n(ind,isc)/nt_fit(ind)
 statistic,qn,qn_av,qn_sig

 y1_	=0.7-0.3*1
 y2_	=y1_+0.25
 !y.title='Electron density  n!De!N [cm!U-3!N]'
 !x.title='position along loop  s/L'
 !y.range=[0,max(2*n)>1.e9]
 !x.style=0
 !y.style=0
 !p.position=[x1_,y1_,x2_,y2_]
 plot,ss(ind),n(ind,isc),thick=3,psym=1
 for is=0,ns-1 do $
  if (min(frac(is,*)) ge fracmin) and (min(t(is,*)) gt tmin) and $
   (max(t(is,*)) lt tmax) then oplot,ss(is)*[1,1],[n(is,0),n(is,1)]
 oplot,ss,nt_fit,thick=3
 xyouts_norm,0.05,0.9,'w='+string(w8(isc),'(f6.1)')+' Mm'
 xyouts_norm,0.05,0.8,'l!Bfull,proj!N='+string(l2,'(f6.1)')+' Mm'
 xyouts_norm,0.05,0.7,'l!Bfull!N='+string(l,'(f6.1)')+' Mm'
 xyouts_norm,0.05,0.6,'l!Bhalf!N='+string(l/2.,'(f6.1)')+' Mm'
 xyouts_norm,0.50,0.9,'n!Be!N='+string(n0_fit/1.e9,'(f5.2)')+' 10!A9!N cm!U-3!N'
 xyouts_norm,0.50,0.8,'n!Be!N/n!Bmod!N='+string(qn_av,'(f5.2)')+'+'+string(qn_sig,'(f5.2)')
 xyouts_norm,0.50,0.7,'ns='+string(ns,'(i3)')+', nw='+string(nw,'(i3)')

;PARTIAL MAP______________________________________________________
 iw	=0
 i1	=long(min(xs(*,*,isc))-nw)
 i2	=long(max(xs(*,*,isc))+nw)
 j1	=long(min(ys(*,*,isc))-nw)
 j2	=long(max(ys(*,*,isc))+nw)
 xx	=i1+findgen(i2-i1+1)
 yy	=j1+findgen(j2-j1+1)
 restore,imagefiles(iw)  ;-->images_pair(2,nx,ny),para,index_a,index_b
 image  =reform(image_pair(1-isc,i1:i2,j1:j2))
 c1	=alog10(min(image)>1.)
 c2	=alog10(max(image))
 col1	=min(f(*,*,isc,0))
 col2	=max(f(*,*,isc,0))

 y1_	=0.7-0.3*2
 y2_	=y1_+0.25
 !p.position=[x1_,y1_,x2_,y2_]
 !x.range=[i1-0.5,i2+0.5]
 !y.range=[j1-0.5,j2+0.5]
 !x.title='pixel parallel to AB plane'
 !y.title='pixel perpendicular to AB plane'
 !x.style=1
 !y.style=1
 if (io eq 0) then begin
  nxw  =long(!d.x_vsize*(x2_-x1_)+0.5)
  nyw  =long(!d.y_vsize*(y2_-y1_)+0.5)
  ima  =congrid(image,nxw,nyw)
 endif
 if (io ne 0) then ima=image
 plot,[0,0],[0,0]
 tv,bytscl(alog10(ima),min=alog10(col1),max=alog10(col2)),x1_,y1_,xsize=(x2_-x1_),ysize=(y2_-y1_),/normal
 !noeras=1
 contour,image,xx,yy,level=10.^(c1+(c2-c1)*findgen(nlev)/float(nlev)),/over
 xyouts_norm,0.02,0.02,sc_(isc)+' '+wave_(iw),char*2
 oplot,xs(*,nw/2,isc),ys(*,nw/2,isc),thick=3,linestyle=2
 oplot,xs(*,0   ,isc),ys(*,0   ,isc),thick=3
 oplot,xs(*,nw-1,isc),ys(*,nw-1,isc),thick=3
 oplot,xs(0,*   ,isc),ys(0,*   ,isc),thick=3
 oplot,xs(ns-1,*,isc),ys(ns-1,*,isc),thick=3

xyouts,0.1,0.97,'Loop #'+string(iloop,'(i2)')+'/'+sc_(isc),size=char*2,/normal
xyouts,0.5,0.97,'Background = '+method+' Method',size=char,/normal

if (io eq 0) then begin
openw,3,'background.dat',/append
printf,3,iloop,backgr,isc,ns,qfrac,t_av,t_sig,dt_av,dt_sig,flux_frac,w8(isc),n0_fit/1.e8,$
	format='(4i4,8f8.2)'
close,3
print,'Parameters saved in file=background.dat'
endif

fig_close,io,fignr,ref
end
