pro nlfff_iris,dir,savefile,test
;+
; Project     : SDO, IRIS 
;
; Name        : NLFFF_IRIS
;
; Category    : read IRIS data 
;
; Explanation : reads IRIS image from archive at LMSAL 
;               and write uncompressed FITS file
;
; Syntax      : IDL>nlfff_iris,dir,savefile,test
;
; Inputs      : dir	 =temporary data directory 
;		filename =filename 
;   		wave_    =['1330','1400','2796','2832']
;
; Outputs     ; saves IRIS FITS file in dir
;		wave_=wavelengths found
;
; History     : 19-Nov-2014, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

restore,dir+savefile    ;--> PARA
filename=strmid(savefile,0,15)
wave_   =para.wave_
instr   =input.instr
if (instr ne 'IRIS') then goto,end_proc

print,'__________________NLFFF_IRIS_____________________________'
nw	=n_elements(wave_)
nfiles_wave=lonarr(nw)
for iwave=0,nw-1 do begin
 irisfile=filename+'_IRIS_'+wave_(iwave)+'.fits' 
 file	=findfile(dir+irisfile,count=count)
 if (count eq 1) then begin
  print,'IRIS file already exists : ',irisfile
  nfiles_wave[iwave]=1
 endif
endfor
if (total(nfiles_wave) eq nw) then begin
 print,'All IRIS files exist'
 goto,end_proc
endif

;_____________________SELECT EVENT AND TIME_____________________________
dt       =0.05                  ;dt=0.1 hrs, cadence
year    =strmid(filename,0,4)
month   =strmid(filename,4,2)
day     =strmid(filename,6,2)
hour    =strmid(filename,9,2)
minu    =strmid(filename,11,2)
seco    =strmid(filename,13,2)
t1_file =year+'-'+month+'-'+day+' '+hour+':'+minu+':'+seco
t0sec   =anytim(t1_file,/sec)
t1sec   =t0sec-(3600.*dt/2.)  			;minus half cadence 
t2sec   =t0sec+(3600.*dt/2.)  			;plus  half cadence 
t1      =anytim(t1sec,/hxrbs)
t2      =anytim(t2sec,/hxrbs)
print,'Start time = ',t1
print,'End   time = ',t2

;_____________________FIND IRIS FILES OF SAME DAY________________________
t0      =strmid(t1,0,9)+' 00:00:00.000'
t3      =strmid(t2,0,9)+' 23:59:00.000'
files   =iris_time2files(t0,t3,/sji,level=2)    ;--> level 2 data
nf      =n_elements(files)
print,'Number of files of day = ',nf
print,files

;_____________________FIND IRIS OBSERVING SEQUENCE_______________________
tl      =iris_time2timeline(t0,t3)
ntl     =n_elements(tl)
print,'Number of observing sequences of day = ',ntl
isel    =-1
for it=0,ntl-1 do begin
 dateobs=tl(it).DATE_OBS
 dateend=tl(it).DATE_END
 t3sec  =anytim(dateobs)
 t4sec  =anytim(dateend)
 label=''
 if (t3sec le t2sec) and (t4sec ge t1sec) then begin
  isel=it
  label=' <==== '
  obsid=strtrim(tl(it).OBSID,2)
 endif
 print,dateobs,'-->',dateend,label
endfor
if (isel eq -1) then goto,end_proc
tl_sel  =tl(isel)
print,'Selected observing sequence # = ',isel
print,'Observing sequence ID #       = ',obsid
print,'DATE_OBS   = ',tl_sel.DATE_OBS
print,'DATE_END   = ',tl_sel.DATE_END

;__________________________FIND FILENAME_____________________________
ind_files=lonarr(nf)
for it=0,nf-1 do begin
 ipos1  =strpos(files(it),obsid)
 for iw =0,nw-1 do begin
  ipos2  =strpos(files(it),wave_[iw])
  if (ipos2 ne -1) then goto,next_seq
 endfor
 NEXT_SEQ:
 label=''
 if (ipos1 ne -1) and (ipos2 ne -1) then begin &ind_files(it)=1 &label=' <=== ' &endif
endfor
ifiles=where(ind_files eq 1,nfsel)
print,'# of selected fiels = ',ifiles
files_sel=files(ifiles)

;___________________________READ IMAGES____________________________
;compile first: IDL>.run $SSW/iris/idl/lmsal/util/read_iris_l22.pro
;==================================================================
for isel=0,nfsel-1 do begin
 sjifile =files_sel[isel]
 print,sjifile
 print,'timerange t1=',t1
 print,'timerange t2=',t2
 read_iris_l2,sjifile,index ;,data,imagen=indgen(1) 
 ns	=n_elements(index)
 print,'sequence start=',index[0].date_obs
 print,'sequence end  =',index[ns-1].date_obs
 tseq	=anytim(index.date_obs,/sec)
 ind	=where((tseq ge t1sec) and (tseq le t2sec),nseq)
 print,'Number of images in selected time interval = ',nseq
 if (nseq le 1) then goto,end_image
 read_iris_l2,sjifile,index,data,timerange=[t1,t2],/keep_null
 help,index,data
 dim	=size(data)
 ndim	=dim(0)
 nx	=dim(1)
 ny	=dim(2)
 nframe =dim(3)
 for iwave=0,nw-1 do begin
  ipos	=strpos(sjifile,'SJI_'+wave_[iwave])
  if (ipos ge 0) then goto,end_wave
 endfor
 END_WAVE:

;___________________________REMOVE EDGES__________________________
 image	=reform(data(*,*,0))
 zmed	=median(image)
 for j=0,ny-1 do begin
  ind0	=where(image(*,j) le 0,n0)
  if (n0 ge 1) then image(ind0,j)=zmed
 endfor

;___________________________DISPLAY IMAGE_________________________
 window,0,xsize=nx,ysize=ny
 loadct,3
 statistic,image,zavg,zsig
 c1	=zavg-2*zsig
 c2	=zavg+2*zsig
 tv,bytscl(image,min=c1,max=c2)

;___________________________WRITE FITS FILE_______________________
 irisfile_wave=dir+filename+'_IRIS_'+wave_(iwave)+'.fits' 
 mwritefits,index[0],image,outfile=irisfile_wave   ;FITS file
 print,'IRIS FITS file written = ',irisfile_wave
 END_IMAGE:
endfor
END_PROC: 
end
