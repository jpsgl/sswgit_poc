pro euvi_catalog,evefile,catfile

;generates event catalog list in one-line format

;________________________GENERATE CATALOG_______________
 start =0
 status=0
 count =0

 openr,1,evefile
 openw,2,catfile
 printf,2,'  #  DATE         START  END POSITION   CAD  SC  GOES  RHESSI  CTS  COMMENTS  CME'
 while not eof(1) do begin

 line=' '
 readf,1,line

 if (strmid(line,0,3) eq '___') then begin 	
  if (start ge 1) then begin
   text	=nr_+euvi+goes+kev+comments+cme 		;previous event
   if (strmid(text,0,3) ne '   ') then begin
    printf,2,text
    count=count+1
   endif
   if (status ge 2) then begin
    class=strmid(goes,0,1)
   endif
  endif
  goes='      '
  kev='             '
  allcomments=''
  comments=''
  cme=''
  status=1   					;marker read 
  start=1
 endif

 if (status eq 1) then begin
  ipos=strpos(line,'#')
  if (ipos lt 0) then begin
    status=0
    nr_='     '
  endif
  if (ipos ge 0) then begin 
   nr =strmid(line,ipos+1,3)				
   if (long(nr) lt 500) then begin
    nr_=nr+'  ' 				;flare nr
    status=2					;flare event read
   endif
  endif
 endif

 if (status eq 2) then begin
  century=strmid(line,0,2)
  if (century eq '20') then begin
   dateobs=strmid(line,0,11)
   timeobs=strmid(line,12,11)
   posobs =strmid(line,25,6)
   ipos   =strpos(line,'C=') &cadence=strmid(line,ipos+2,4) 
   if (strmid(cadence,3,1) eq ' ') then cadence=' '+strmid(line,ipos+2,3)
   if (strmid(cadence,2,1) eq ' ') then cadence='  '+strmid(line,ipos+2,2)
   ipos=strpos(line,'EUVI')
   sc='AB'
   if (strmid(line,ipos+4,2) eq '[A') then sc='A '
   if (strmid(line,ipos+4,2) eq '[B') then sc=' B'
   euvi=dateobs+'  '+timeobs+'  '+posobs+'  '+cadence+'  '+sc+'  '
   ipos1  =strpos(line,'EUVI[') 
   ipos2  =strpos(line,']')
   nlen   =ipos2-(ipos1+5)
   allcomments=strmid(line,ipos1+5,nlen)
   if (strpos(allcomments,'I') ge 0) then comments=comments+'I'
   if (strpos(allcomments,'P') ge 0) then comments=comments+'P'
   if (strpos(allcomments,'E') ge 0) then comments=comments+'E'
   if (strpos(allcomments,'D') ge 0) then comments=comments+'D'
   if (strpos(allcomments,'O') ge 0) then comments=comments+'O'
   if (strpos(allcomments,'W') ge 0) then comments=comments+'W'
   if (strpos(allcomments,'T') ge 0) then comments=comments+'T'
   nlen=strlen(comments)
   for i=nlen,7 do comments=comments+' '
   if (strpos(allcomments,'L') ge 0) then cme=cme+'L'
   if (strpos(allcomments,'S') ge 0) then cme=cme+'S'
   nlen=strlen(cme)
   for i=nlen,4 do cme=cme+' '
   status=3					;EUVI line read
  endif
 endif

 if (status eq 3) then begin
  ipos=strpos(line,'GOES')
  if (ipos ge 0) then begin
   goes=strmid(line,ipos+5,4)+'  '
   if (strmid(goes,3,1) eq ' ') then goes=strmid(line,ipos+5,3)+'   '
   if (strmid(goes,2,1) eq ' ') then goes=strmid(line,ipos+5,2)+'    '
  endif
 endif

 if (status eq 3) then begin
  ipos=strpos(line,'RHESSI')
  if (ipos ge 0) then begin
   kev=strmid(line,ipos+7,6)+' '
   if (strmid(kev,6,1) eq ' ') then kev=strmid(line,ipos+7,6)+' '
   if (strmid(kev,5,1) eq ' ') then kev=strmid(line,ipos+7,5)+'  '
   if (strmid(kev,4,1) eq ' ') then kev=strmid(line,ipos+7,4)+'   '
   if (strmid(kev,3,1) eq ' ') then kev=strmid(line,ipos+7,3)+'    '
   if (strmid(kev,2,1) eq ' ') then kev=strmid(line,ipos+7,2)+'     '
   if (strmid(kev,1,1) eq ' ') then kev=strmid(line,ipos+7,1)+'      '
   ipos2=strpos(line,'P=')
   ipos3=strpos(line,'cts/s')
   npos =ipos3-ipos2-2
   peak=strtrim(strmid(line,ipos2+2,npos),2)
   nlen=strlen(peak)
   if (nlen eq 1) then kev=kev+'   '+peak+'  '
   if (nlen eq 2) then kev=kev+'  '+peak+'  '
   if (nlen eq 3) then kev=kev+' '+peak+'  '
   if (nlen eq 4) then kev=kev+peak+'  '
  endif
 endif

 endwhile
 printf,2,'Count of events = '+string(count,'(i5)')
 close,1
 close,2
 print,'Files generated = ',catfile ,' counts=',count

end
