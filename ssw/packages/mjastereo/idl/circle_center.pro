pro circle_center,xa,ya,xb,yb,xc,yc,xcen,ycen,rad

;circle through 3 points

xcen	=0.
ycen	=0.
rad	=1.
a	=(xb-xa)
b	=(yb-ya)
c	=(xb-xc)
d	=(yb-yc)
e	=-(xb^2+yb^2-xa^2-ya^2)/2.
f	=-(xb^2+yb^2-xc^2-yc^2)/2.
xcen	=(b*f-d*e)/(a*d-b*c)
ycen	=(c*e-a*f)/(a*d-b*c)
rad	=sqrt((xa-xcen)^2+(ya-ycen)^2)
end

