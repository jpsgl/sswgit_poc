pro helio_trans,p,p0,l0,b0,x,y,r,l,b,rsun
;transforms image coordinates [x,y,r] into heliographic coordinates [l,b] 
;	p              = position angle (N-->E) 
;	p0	       = rotation (roll) angle of image (N-->E)
;	l0,b0	       = heliografic longitude and latitude of disk center.
;spatial scales X,Y,R are specified by (photospheric) solar radius RSUN 
;	x[n],y[n]      = array of image coordinates (in same units as RSUN) 
;	r[n]	       = distance from Sun center (in same units as RSUN)
;	l[n],b[n]      = array of [l,b] heliographic coordinates
;M.J.Aschwanden, March 1998, UMd

;scaling
if (n_params(0) le 9) then rsun=1.	;in units of solar radii (default in old version) 
;normalization into solar radii
xx	=x/rsun
yy	=y/rsun
rr	=r/rsun
;conversion deg-->rad
q	=(!pi/180.)
p_	=p *q
p0_	=p0*q
l0_	=l0*q
b0_	=b0*q
;rotation by position angle 
x1 	= xx*cos(p_+p0_)+yy*sin(p_+p0_)
y1 	=-xx*sin(p_+p0_)+yy*cos(p_+p0_)
z1	= sqrt(rr^2-xx^2-yy^2)
;rotation by latitude angle 
x2	= x1
y2	= y1*cos(b0_)+z1*sin(b0_)
z2	=-y1*sin(b0_)+z1*cos(b0_)
;rotation by longitude angle 
x3	= x2*cos(l0_)+z2*sin(l0_)
y3	= y2
z3	=-x2*sin(l0_)+z2*cos(l0_)
;latitude and latitude angle
b_	=asin(y3/rr)
n	=n_elements(x3)
if (n eq 1) then arctan,z3,x3,l_,l_deg
if (n ge 2) then begin
 l_	=fltarr(n)
 for i=0,n-1 do begin 
  arctan,z3(i),x3(i),l_i,l_ideg 
  l_(i)=l_i 
 endfor
endif
l	=l_/q
b	=b_/q
end
