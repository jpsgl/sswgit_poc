pro helix_para_coord,base,h0,rhelix,nturn,rsun,xl,yl,zl
;+
; Project       : Loop 3D geometry fitting
;
; Name          : LOOP_PARA_COORD 
;
; Category      : Data modeling
;
; Explanation   : computes loop coordinates in XZ-loop plane (n=180)
;		  XL(0,...,N-1), YL(0,...,n-1)=0, ZL(0,...,N-1)       
;		  from 3 loop parameters (base,h0,hmin) in same length
;		  units as solar radius RSUN (preferably in arcsecs). 
;
; Syntax	: IDL>loop_para_coord,base,h0,hmin,th,rsun,xl,yl,zl
;
; Input		: base    =footpoint separation (arcsec)
;		  h0      =offset of circular loop center loop baseline (arcsec)
;		  hmin	  =height of footpoints	(arcsecs)		
;		  th	  =inclination angle of loop plane (deg)
;	          rsun    =solar radius (arcsecs)

half	=base/2.
phi0	=atan(h0/half)
rloop   =sqrt(h0^2+half^2)
n	=180
yl 	=fltarr(n)
l	=rloop*(!pi+2.*phi0)
s	=l*findgen(n)/float(n-1)
psi	=nturn*2.*!pi*(s/l)		;helical twist angle
r	=rhelix*sin(psi)		;radial displacement
yl	=rhelix*cos(psi)		;transverse displacement
phi	=-phi0+(!pi+2.*phi0)*findgen(n)/float(n-1)
xl  	=(rloop+r)*cos(phi)
zl  	=(rloop+r)*sin(phi)+h0
xf1	=xl(0)
yf1	=yl(0)
xf2	=xl(n-1)
yf2	=yl(n-1)
newbase	=sqrt((xf2-xf1)^2+(yf2-yf1)^2)
xx	=(xl-(xl(0)+xl(n-1))/2.)*base/newbase
yy	=(yl-(yl(0)+yl(n-1))/2.)*base/newbase
zz	=(zl-(zl(0)+zl(n-1))/2.)*base/newbase
a	=atan((yy(n-1)-yy(0))/(xx(n-1)-xx(0)))
xl	= xx*cos(a)+yy*sin(a)
yl	=-xx*sin(a)+yy*cos(a)
end
