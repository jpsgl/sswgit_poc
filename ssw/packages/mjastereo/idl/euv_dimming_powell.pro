function euv_dimming_powell,coeff

common dimming,model,time,a_t,v_t,x_t,q_t,q_obs,q_sig,h0,ind_fit

rsun_cm	=6.96e10 			;cm
t	=time				;s
a0	=coeff(0)*1.e5 			;cm/s^2
t0	=coeff(1)*1.e3			;s
qmin	=coeff(2) > 0 
tau	=120.				;s (acceleration < time resolution)
ta	=t0+tau				;s

ind0	=where(t ge t0,n0)
ind1	=where((t ge t0) and (t le ta),n1)
ind2	=where(t ge ta,n2)
tt	=t-t0
nt	=n_elements(t)
a_t	=fltarr(nt)
v_t	=fltarr(nt)
x_t	=fltarr(nt)
q_t	=fltarr(nt)
if (model lt 1) or (model gt 4) then stop,'Wrong model number'

if (model eq 1) then begin
 a_t(ind1)=a0
 for i=0,n1-1 do v_t(ind1(i))=a0*(t(ind1(i))-t0) 
 va	=a0*tau
 if (n2 ge 1) then for i=0,n2-1 do v_t(ind2(i))=va 
 x_t	=x_t + h0
 x_t(ind1)=(1./2.)*a0*(t(ind1)-t0)^2 + h0
 xa     =  (1./2.)*a0*tau^2 + h0
 if (n2 ge 1) then for i=0,n2-1 do x_t(ind2(i))=xa + va*(t(ind2(i))-ta) 
endif

if (model eq 2) then begin
 a_t(ind1)=a0*(1.-tt(ind1)/tau)
 v_t(ind1)=a0*tt(ind1)-(a0/2.)*(tt(ind1)^2/tau)
 va	  =(a0/2.)*tau
 if (n2 ge 1) then for i=0,n2-1 do v_t(ind2(i))=va
 xa	=(a0/3.)*tau^2 + h0
 x_t	=x_t + h0
 x_t(ind1)=(a0/2.)*tt(ind1)^2 - (a0/6.)*tt(ind1)^3/tau + h0
 if (n2 ge 1) then for i=0,n2-1 do x_t(ind2(i))=xa + va*(t(ind2(i))-ta)
endif

if (model eq 3) then begin
 a_t(ind1)=a0*(1.-tt(ind1)/tau)^2
 v_t(ind1)=(a0/3.)*tau*(1.-(1.-tt(ind1)/tau)^3)
 va	  =(a0/3.)*tau
 if (n2 ge 1) then for i=0,n2-1 do v_t(ind2(i))=va
 xa	=(a0/4.)*tau^2 + h0
 x_t	=x_t + h0
 x_t(ind1)=(a0/3.)*(tt(ind1)*tau)+(a0/12.)*tau^2*((1.-tt(ind1)/tau)^4 - 1.)+h0
 if (n2 ge 1) then for i=0,n2-1 do x_t(ind2(i))=xa + va*(t(ind2(i))-ta)
endif

if (model eq 4) then begin
 tt	=t(ind0)-t0
 a_t(ind0)=a0*exp(-tt/tau)
 v_t(ind0)=a0*tau*(1.-exp(-(tt/tau)))
 x_t	=x_t+h0
 x_t(ind0)=h0 + a0*tau*tt - a0*tau^2*(1.-exp(-tt/tau))
endif

q    	=((rsun_cm+h0)^3-rsun_cm^3)/((rsun_cm+x_t)^3-rsun_cm^3)
q_t	=qmin+(1.-qmin)*q
nfree	=n_elements(coeff)
nind	=n_elements(ind_fit)
n	=(nind-nfree) > 1
chi	=sqrt(total((q_t(ind_fit)-q_obs(ind_fit))^2./q_sig(ind_fit)^2)/float(n))

return,chi
end
