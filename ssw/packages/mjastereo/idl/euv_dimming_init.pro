pro euv_dimming_init,dir,iev,vers,test
;+
; Project     : AIA/SDO, HMI/SDO, IBIS/DST, ROSA/DST
;
; Name        : NLFFF_INIT
;
; Category    : Magnetic data modeling 
;
; Explanation : read event start time, wavelengths, and heliographic 
;		position and defines a unique filename and field-of-view
;		coordinates (in units of solar radii from Sun center).
;
; Syntax      : IDL>nlfff_event,dir,iev,vers,test
;
; Inputs      : dir	=directory for datafiles
;		iev	=flare number
;		vers	=version
;		test
;
; Outputs     ; savefile
;
; History     : 30-Oct-2015, Version 1 written by Markus J. Aschwanden
;               25-Nov-2015, define structure PARA
;
; Contact     : aschwanden@lmsal.com
;-

print,'===================EUV_INIT============================'
string0,3,iev,iev_str
catfile ='dimming_'+iev_str+vers+'.txt'
readcol,dir+catfile,event_,dateobs_,noaa_,helpos_,instr_,$
  w1_,w2_,w3_,w4_,w5_,w6_,w7_,w8_,fov_,$
  format='(I,A,A,A,A,A,A,A,A,A,A,A,A,A)',/silent
nt	=n_elements(event_)	&print,'Number of time steps nt= ',nt

;_____________________TIME SEQUENCE_____________________________
for j=0,nt-1 do begin
 it	=j+1
 ind	=where(event_ eq it,nev)
 if (nev le 0) then stop,'NLFFF_EVENT : NO MATCHING EVENT FOUND'
 i 	=ind(0)			;selected entry 
 wave8	=[w1_(i),w2_(i),w3_(i),w4_(i),w5_(i),w6_(i),w7_(i),w8_(i)]
 indw	=where(wave8 ne 0,nwave)
 wave_	=strtrim(string(wave8(indw),'(I5)'),2)

;____________________SAVEFILE___________________________________
 it	=j+1
 string0,3,it,it_str  
 savefile_t='dimming_'+iev_str+'_'+it_str+vers+'.sav'
 restore,dir+savefile_t			;-->input 

;____________________DATEOBS FORMAT_____________________________'
			;standard format 2010-08-03T15:23:00
 if (strmid(dateobs_(i),10,1) eq 'T') then dateobs=dateobs_(i)
			;dateobs format 20100803_152300
 if (strmid(dateobs_(i),8,1) eq '_') then begin
  year	=strmid(dateobs_(i),0,4)
  mon	=strmid(dateobs_(i),4,2)
  day	=strmid(dateobs_(i),6,2)
  hh	=strmid(dateobs_(i),9,2)
  mm	=strmid(dateobs_(i),11,2)
  ss	=strmid(dateobs_(i),13,2)
  dateobs=year+'-'+mon+'-'+day+'T'+hh+':'+mm+':'+ss
 endif

;'________________FIELD-OF-VIEW COORDINATES_____________________'
 fov0	=input.fov0
 north	=strmid(helpos_(i),0,1)
 west	=strmid(helpos_(i),3,1)
 if (north eq 'N') then lat=+long(strmid(helpos_(i),1,2))
 if (north eq 'S') then lat=-long(strmid(helpos_(i),1,2))
 if (west  eq 'W') then lon=+long(strmid(helpos_(i),4,2))
 if (west  eq 'E') then lon=-long(strmid(helpos_(i),4,2))
 t_syn   =27.2753*86400. ;mean synodic period of rotation [s]
 t_sec   =anytim(dateobs,/sec)
 t_year  =t_sec mod long(365.2422*86400.)
 t_b0    =(5*30.+6)*86400.                       ;June 6, B0=0 deg
 b0      =7.24*sin(2.*!pi*(t_year-t_b0))
 xc_sun	=sin(lon*!pi/180.)*cos(lat*!pi/180.)
 yc_sun  =sin((lat-b0)*!pi/180.)
 x1_sun	=xc_sun-fov0/2.
 x2_sun	=xc_sun+fov0/2.
 y1_sun	=yc_sun-fov0/2.
 y2_sun	=yc_sun+fov0/2.

;_________________________PARAMETER STRUCTURE_____________________
 para	={event:event_(i),dateobs:dateobs_(i),wave_:wave_,$
          x1_sun:x1_sun,x2_sun:x2_sun,y1_sun:y1_sun,y2_sun:y2_sun,$
          dpix_euv:0.0,fluxmax:findgen(nwave)}
 savefile_t='dimming_'+iev_str+'_'+it_str+vers+'.sav'
 save,filename=dir+savefile_t,input,para
endfor
end
