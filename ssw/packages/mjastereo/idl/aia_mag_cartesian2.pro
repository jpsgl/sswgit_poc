pro aia_mag_cartesian2,xx,yy,zz,x,y,z,xcen,ycen

b0	=asin(ycen)
l0	=asin(xcen)
b	=b0+yy
l	=l0+xx
x	=sin(l)
y	=sin(b)
r	=zz
z	=sqrt(r^2-x^2-y^2)
end
