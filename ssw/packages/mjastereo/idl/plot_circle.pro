pro plot_circle,x,y,r,thick,fill,color,linestyle

if (n_params(1) le 4) then fill=-1
if (n_params(1) le 5) then color=!p.color
if (n_params(1) le 6) then linestyle=0
phi	=2.*!pi*findgen(361)/360.
xx	=x+r*cos(phi)
yy	=y+r*sin(phi)
if (fill eq -1) then oplot,xx,yy,thick=thick,color=color,linestyle=linestyle
if (fill ge  0) then polyfill,xx,yy,color=color

end
