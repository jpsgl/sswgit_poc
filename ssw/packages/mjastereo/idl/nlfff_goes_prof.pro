pro nlfff_goes_prof,savefile,efile,dt,tmargin
;
;	Purpose:	created GOES time profiles and saves in SAVEFILE
;	dt      =0.1    ;hrs
;	tmargin =0.5    ;hrs
;	savefile='goes_prof.sav'      
;	efile='goes_noaa_M1.txt'

;_________________________READ LIST AND SELECT EVENTS____________________
readcol,efile,event_,tstart_,tpeak_,tend_,goes_,noaa_,pos_,$
       format='(I,A,A,A,A,A,A)'
nev     =n_elements(event_)
nbin	=30			;2 s * 30 = 30 s = 0.5 min
nt_max	=1000			;1000 * 0.5 min = 500 min = 8.3 hrs
time_goes_=fltarr(nt_max,nev)
flux_goes_=fltarr(nt_max,nev)
deriv_goes_=fltarr(nt_max,nev)

for i=0,nev-1 do begin		;extract from event file
 iev	 =event_(i)		
 print,'Read GOES event #',iev 
 ind     =where(event_ eq iev,nind)
 tstart	 =tstart_(ind(0))
 tpeak   =tpeak_(ind(0))
 tend    =tend_(ind(0))
 goes    =goes_(ind(0))
 noaa    =noaa_(ind(0))
 t0      =anytim(tpeak,/sec)
 t1      =anytim(tstart,/sec)
 t2      =anytim(tend,/sec)   
 t3      =t1-tmargin*3600.   
 t4      =t2+tmargin*3600.    
 t3_str  =anytim(t3,/hxrbs)
 t4_str  =anytim(t4,/hxrbs)
 tbase	 =(t0-(t0 mod 86400.))	;sec since start of day
 t0_hr   =(t0-tbase)/3600.
 t1_hr   =(t1-tbase)/3600.
 t2_hr   =(t2-tbase)/3600.
 t3_hr   =(t3-tbase)/3600.
 t4_hr   =(t4-tbase)/3600.

;print,'______________________READ GOES____________________________'
 rd_gxd,t3_str,t4_str,gdata,status=status
 time	=((float(gdata.time)/1000.) mod 86400)/3600.  ;time in hours 
 tgoes0=min(time,i0)
 ng	=n_elements(time)
 if (i0 ge 1) and (i0 lt ng/2) then $
  time(0:i0-1)=time(0:i0-1)-24.
 if (i0 ge 1) and (i0 gt ng/2) then $
  time(i0:ng-1)=time(i0:ng-1)+24.
 flux	=gdata.lo > 1.e-7				;1-8 A low energy chan
 nt	=long(ng/nbin) < nt_max
 time_goes_(0:nt-1,i)=rebin(time(0:nt*nbin-1),nt)
 flux_goes_(0:nt-1,i)=rebin(flux(0:nt*nbin-1),nt)

;______________________________PLOT GOES DERIVATIVE___________________'
 deriv_goes_(1:nt-2,i)=(flux_goes_(2:nt-1,i)-flux_goes_(0:nt-3,i))/2.
 deriv_goes_(0,i)=deriv_goes_(1,i)
 deriv_goes_(nt-1,i)=deriv_goes_(nt-2,i)
endfor

save,filename=savefile,event_,time_goes_,flux_goes_,deriv_goes_
print,'file written = ',savefile
end
