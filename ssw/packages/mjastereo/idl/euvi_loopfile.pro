pro euvi_loopfile,savefile,tracefile,tablefile,loopfile

;+
; Project     : STEREO
;
; Name        : EUVI_LOOPFILE 
;
; Category    : Database management
;
; Explanation : reads loop datafile and creates Table with loop parameters 
;		in file TABLEFILE, as well as interpolates the loop 3D coordinates 
;		with higher resolution into file loopfile 
;
; Syntax      : IDL> euvi_loopfile,savefile,tracefile,tablefile,loopfile
;
; Inputs      : savefile	: e.g. 'loop_A.sav' contains image parameters
;		tracefile	; e.g. 'loop_A.dat' contains low-resolution loop spline points
;
; Outputs     : tablefile	; e.g. 'table2.dat' contains Table
;		loopfile	; e.g. 'loop_A2.dat' contains high-resolution loop coordinates
;
; History     : Oct/Nov 2007, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

restore,savefile
rsun_m  =0.696*1.e9
cdelt1  =para.cdelt1
cdelt2  =para.cdelt2
crpix1  =para.crpix1
crpix2  =para.crpix2
da      =para.da/rsun_m
db      =para.db/rsun_m
rsun    =para.rsun
a_sep   =para.sep_deg*(!pi/180.)
rad_pix =rsun/cdelt1
hmax	=0.1
r_max   =1.0+hmax

readcol,tracefile,iloop_,ix_,iy_,iz_,ih_,ih_raw,dh_,nsm_,wave_,chi2_,q1_,q2_
euvi_loop_renumerate,iloop_,wave_
nloop  =long(max(iloop_))+1
print,'Datafile with loop coordinates found  LOOPFILE=',tracefile
print,'Number of loops found                NLOOP   ='+string(nloop,'(i3)')

if (tablefile ne '') then openw,2,tablefile
openw,3,loopfile
for iloop=0,nloop-1 do begin
 ind    =where(iloop_ eq iloop,np)
 xp     =ix_(ind)
 yp     =iy_(ind)
 zp     =iz_(ind)
 hp	=ih_(ind)
 hr	=ih_raw(ind)
 dhp	=dh_(ind)
 nfree	=np-3
 x0	=(xp(0)-crpix1)/rad_pix
 y0	=(yp(0)-crpix2)/rad_pix
 z0	= zp(0)/rad_pix
 qa	=q1_(ind(0))
 qb	=q2_(ind(0))
 chi2   =sqrt(total((hp-hr)^2/dhp^2)/float(nfree))
 
 euvi_spline,para,xp,yp,zp,x,y,z,d,r,r_n	;d[pixels],r[pixels],r_n[solar radii]
 n	=n_elements(x)
 h	=r-rad_pix
 dh	=interpol(dhp,hp,h)
 iwave	=wave_(ind(0))
 for i=0,n-1 do begin
  ip	=i+1
  xi	=(x(i)-crpix1)/rad_pix			;x[solar radii]
  yi	=(y(i)-crpix2)/rad_pix			;y[solar radii]
  zi	= z(i)/rad_pix				;z[solar radii]
  hi	=(r(i)/rad_pix-1.)*696.>0.	;Mm	;h[Mm]
  dhi	=(dh(i)/rad_pix-1.)*696.>0.	;Mm	;dh[Mm]
  si	=(d(i)/rad_pix)*696.			;d[Mm]
  printf,3,iloop,ip,xi,yi,zi,si,hi,iwave,format='(2i6,3f12.6,2f8.1,i4)'
 endfor
 
 len	=(max(d)/rad_pix)*696.	;Mm
 h0	=min(r_n-1.,imin)*696.>0.;Mm
 h1	=max(r_n-1.,imax)*696.	;Mm
 w	=1./dh
 dhw	=total(dh*w)/total(w)
 print,   iloop,np,x0,y0,z0,format='(2i4,3f10.5)'
 if (tablefile ne '') then printf,2,iloop,' &',np,' &',x0,' &',y0,' &',z0,$
        ' &',len,' &',h0,' &',h1,' &',dhw,' &',qa,' &',qb,' &',chi2,' \\',$
	format='(2(i4,a),3(f10.5,a),(f8.1,a),3(f6.1,a),3(f8.2,a))'

endfor
if (tablefile ne '') then close,2
close,3
print,'loop coordinates xyz in '+tablefile
print,'interpolated points  in '+loopfile
end
