pro euvi_hydro_temp,loopfile,hydrofile,para,crota2,blong,blat,dlat,ct,hmax,w_km,io,model,xrange,yrange,res
;+
; Project     : STEREO
;
; Name        : EUVI_HYDRO_PROJECT 
;
; Category    : Graphics 
;
; Explanation : This routine produces a plot with the 3D coordinates of
;		a loop projected in any arbitrary direction.
;
; Syntax      : IDL> euvi_projection,loopfile,para,crota2,blong,blat,dlat,ct,hmax
;
; Inputs      : loopfile = datafile containing (x,y,z)-coordinates of loops (e.g.,'loop_A.dat')
;		para	= structure containing image parameters
;		crota2  =0.  ;rotation angle (deg) relative to STEREO-A image
;		blong   =0.  ;longitude diff (deg) relative to STEREO-A image
;		blat    =90. ;latitude diff (deg) relative to STEREO-A image
;	 	dlat    =1.  ;spacing (deg) of coordinate grid
;		ct	=5   ;IDL colortable
;		hmax	=0.1 ;height limit of stereoscopic correlation
;
; Outputs     : postscript file of plot *.ps
;
; History     : Oct/Nov 2007, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

;_______________INPUT PARAMETERS________________________________________
cdelt1	=para.cdelt1
crpix1	=para.crpix1
crpix2	=para.crpix2
rad   	=para.rsun
rad_pix	=rad/cdelt1
dh_max	=0.05*696.	;Mm
hchr    =2./(cdelt1*0.725) ;Mm
pix	=cdelt1*0.725 ;pixel-->Mm

;_______________READ HYDROFILE__________________________________________
restore,hydrofile
	;save,filename=hydrofile,image0,image1,para_model,fov,para,nmulti,$
        ;nloopmax,nk,em44_min,w_km,temin,temax,frac_min
tel_	=para_model(*,0)
h0_	=para_model(*,1)
lam_	=para_model(*,2)
h1_	=para_model(*,3)
lam1_	=para_model(*,4)>10. ;>10,000 km
em44_	=para_model(*,5)
l_	=para_model(*,6)
htop_	=para_model(*,7)
w_	=para_model(*,8)
ds_	=para_model(*,9)/res
loop_model_=para_model(*,10)
nloop_model=n_elements(loop_model_)
dim	=size(image0)
nx	=dim(1)
ny	=dim(2)
nx2	=dim(1)*res
ny2	=dim(2)*res
model	=fltarr(nx2,ny2)+0.01 ;MK

;______________PLOT PREVIOUS LOOPS______________________________________
file_exist=findfile(loopfile,count=nfiles)
if (nfiles eq 0) then print,'No previous loopfile found';
print,'Datafile with loop coordinates found  LOOPFILE=',loopfile 

idat	=strpos(loopfile,'.dat')
isav	=strpos(loopfile,'.sav')
if (idat gt 0) then readcol,loopfile,iloop_,ix_,iy_,iz_,ih_,iz_raw,dh_,nsm_,wave_,chi2_,q1_,q2_
if (isav gt 0) then restore,loopfile ;-->iloop_,ix_,iy_,iz_
nloop  =max(iloop_)+1 
print,'Number of previously traced loops    NLOOP   =',nloop
xx	=(ix_-crpix1)/rad_pix	;xx normalized to solar radius
yy	=(iy_-crpix2)/rad_pix	;yy normalized to solar radius
zz	=(iz_)/rad_pix		;zz normalized to solar radius
rotate1,xx,yy,xrot,yrot,0.,0.,crota2 
rotate1,xrot,zz,xrot2,zrot,0.,0.,blong 
rotate1,zrot,yrot,zrot2,yrot2,0.,0.,blat 
xmid    =((max(xrot2)+min(xrot2))/2.)*rad_pix+crpix1
ymid    =((max(yrot2)+min(yrot2))/2.)*rad_pix+crpix2
xrange  =xmid+[-0.5,+0.5]*float(nx)
yrange  =ymid+[-0.5,+0.5]*float(ny)
xmodel	=xrange(0)+(xrange(1)-xrange(0))*findgen(nx)/float(nx-1)
ymodel	=yrange(0)+(yrange(1)-yrange(0))*findgen(ny)/float(ny-1)

;_______________________DEFINES POINT SPREAD FUNCTION____________________
w       =(w_km/725.)/cdelt1                     ;width in pixels
w_res   =0.0                                   ;resolution in pixels
w_obs   =sqrt(w^2+w_res^2)                      ;observed width
sig_gauss=w_obs/(2.*sqrt(2*alog(2.)))           ;gaussian half width in pixels
sig_gauss2=sig_gauss*4.
kernel  =fltarr(nk,nk)
for i=0,nk-1 do begin 
 for j=0,nk-1 do begin 
  kernel(i,j)=exp(-((i-nk/2)^2+(j-nk/2)^2)/(2.*sig_gauss^2))
 endfor
endfor
kernel  =kernel/total(kernel)                   ;normalization

;_______________________READ EUVI RESPONSE FUNCTION____________________
print,nloop_model
for il=0,nloop_model-1 do begin 
 if (il mod 100) eq 0 then print,'Loop reconstruction #',il,avg(model)
 iloop	=long(loop_model_(il))
 ind	=where(iloop_ eq iloop,n) 
 if (n ge 3) then begin
  xp     =xrot2(ind)*rad_pix+crpix1		;in pixels
  yp     =yrot2(ind)*rad_pix+crpix2
  zp     =zrot2(ind)*rad_pix
  euvi_spline,para,xp,yp,zp,x,y,z,d,r,r_n
  n      =n_elements(x)
  s      =fltarr(n)
  for is=1,n-1 do s(is)=s(is-1)+sqrt((x(is)-x(is-1))^2+$
         (y(is)-y(is-1))^2+(z(is)-z(is-1))^2)
  ns     =long(max(s)/ds_(il))
  s_     =ds_(il)*findgen(ns)			 ;step ds=1 pixel
  if (ns le 3) then goto,endloop
  xs     =interpol(x,s,s_)			;xp_rot interpolated to ds=1
  ys     =interpol(y,s,s_)		        ;yp_rot interpolated to ds=1
  rs     =interpol(r,s,s_)
  hs     =rs-rad_pix				;hs in pixels
  ix     =long(res*(xs-xrange(0))+0.5)
  iy     =long(res*(ys-yrange(0))+0.5)
  ind_ij =where((ix ge 0) and (ix le nx2-1) and (iy ge 0) and (iy le ny2-1),nind_ij)
  if (nind_ij lt 1) then goto,endloop
  ij=ix(ind_ij)+iy(ind_ij)*nx2

  h      =hs>0
  s      =s_
  htop   =max(hs,itop)
  tchr   =0.01                           ;[MK]
  rsun   =6.96                           ;solar radius [100 Mm]
  hchr   =2./(cdelt1*0.725)
  te     =tel_(il) >tchr                 ;loop top temp [MK]
  h0     =(h0_(il)>hchr)<htop            ;loop footpoint height [pix]
  lam    =(lam_(il)<rsun)>(-rsun)        ;scale height [100 Mm]
  h1     =(h1_(il)>hchr)<htop            ;loop footpoint height [pix]
  lam1   =(lam1_(il)<rsun)>(-rsun)       ;scale height [100 Mm]
  l      =(s(itop)-s(0))>(s(ns-1)-s(itop))
  lambda =(lam/rsun)*rad_pix             ;scale height [pixels]
  lambda1=(lam1/rsun)*rad_pix            ;scale height [pixels]
  s_l    =(s-hchr)/(l-hchr)
  eps    =(tchr/te)^(7./2.)
  temp_model=tchr+(te-tchr)*( (s_l*(2.-s_l)) > eps )^(2./7.)
  ind2   =where(s gt s(itop),nind2)
  if (nind2 ge 1) then $
  temp_model(ind2)=tchr+(te-tchr)*( (s_l(ind2)*(2.-s_l(ind2))) > eps )^(2./7.)
  if (nind_ij ge 1) then model(ij)=model(ij)>reform(temp_model(ind_ij))
 endif
 endloop:
endfor
end
