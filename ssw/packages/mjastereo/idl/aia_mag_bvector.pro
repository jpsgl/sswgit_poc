pro aia_mag_bvector,bx,by,bz,x0,y0,z0,ds,polar,xa_,xb_,ba_,end_field

end_field=0
xb_	=xa_
ba_	=[0,0,0,0]
dim	=size(bx)
nx	=dim[1]
ny	=dim[2]
nz	=dim[3]
x1	=xa_[0]
y1	=xa_[1]
z1	=xa_[2]
xa 	=(x1-x0)/ds
ya      =(y1-y0)/ds
za      =(z1-z0)/ds
ia    	=long(xa)
ja    	=long(ya)
ka    	=long(za)
if (ia lt 0) or (ja lt 0) or (ka lt 0) or $
 (ia ge nx-1) or (ja ge ny-1) or (ka ge nz-1) then begin
 end_field=1
 goto,end_fieldline
endif
u       =xa-ia
v       =ya-ja
w       =za-ka
bxa     =(1-u)*(1-v)*(1-w)*bx(ia,ja,ka)+u*(1-v)*(1-w)*bx(ia+1,ja,ka)+$
         (1-u)*v*(1-w)*bx(ia,ja+1,ka)  +u*v*(1-w)*bx(ia+1,ja+1,ka)+$
         (1-u)*(1-v)*w*bx(ia,ja,ka+1)  +u*(1-v)*w*bx(ia+1,ja,ka+1)+$
         (1-u)*v*w*bx(ia,ja+1,ka+1)    +u*v*w*bx(ia+1,ja+1,ka+1)
bya     =(1-u)*(1-v)*(1-w)*by(ia,ja,ka)+u*(1-v)*(1-w)*by(ia+1,ja,ka)+$
         (1-u)*v*(1-w)*by(ia,ja+1,ka)  +u*v*(1-w)*by(ia+1,ja+1,ka)+$
         (1-u)*(1-v)*w*by(ia,ja,ka+1)  +u*(1-v)*w*by(ia+1,ja,ka+1)+$
         (1-u)*v*w*by(ia,ja+1,ka+1)    +u*v*w*by(ia+1,ja+1,ka+1)
bza     =(1-u)*(1-v)*(1-w)*bz(ia,ja,ka)+u*(1-v)*(1-w)*bz(ia+1,ja,ka)+$
         (1-u)*v*(1-w)*bz(ia,ja+1,ka)  +u*v*(1-w)*bz(ia+1,ja+1,ka)+$
         (1-u)*(1-v)*w*bz(ia,ja,ka+1)  +u*(1-v)*w*bz(ia+1,ja,ka+1)+$
         (1-u)*v*w*bz(ia,ja+1,ka+1)    +u*v*w*bz(ia+1,ja+1,ka+1)
ba      =sqrt(bxa^2+bya^2+bza^2)
if (ba ne 0) then begin
 x2	=x1+ds*(bxa/ba)*polar
 y2     =y1+ds*(bya/ba)*polar
 z2     =z1+ds*(bza/ba)*polar
 xb_	=[x2,y2,z2]
 ba_	=[bxa,bya,bza,ba]
endif
if (z2 le 1.0) then end_field=1		;cartesian coordinates
END_FIELDLINE:
end
