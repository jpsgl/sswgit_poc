pro euvi_pos_inv,index,fov,pos,lon,lat
;+
; Project     : STEREO
;
; Name        : EUVI_POS_INV
;
; Category    : Coordinate transformations
;
; Explanation : From input field-of-view with corners FOV=[I1,J1,I2,J2]
;	        the heliographic position POS is inverted for image
;		with structure INDEX 
;
; Syntax      : IDL> euvi_pos_inv,index,fov,pos,lon,lat
;
; Inputs      : index     =structure containing FITS header of image
;               fov	  =[i1,j1,i2,j2] pixel of FOV corners
;
; Outputs     ;	pos       ='S06E76' heliographic latitude and longitude
;               lon       =heliographic longitude (-76 deg)
;               lat       =heliographic latitude  ( -6 deg)
;
; History     : 13-Nov-2008, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-


;_____________________INVERTS HELIOGRAPHIC COORDINATES___________________
;FITS header
 ic     =index.crpix1	;pixels of Sun center in image
 jc     =index.crpix2
 dx	=index.cdelt1
 dy	=index.cdelt2
 l0	=index.hgln_obs
 b0	=index.hglt_obs
 r_sun  =index.rsun
 a11	=index.pc1_1
 a12	=index.pc1_2
 a21	=index.pc2_1
 a22	=index.pc2_2
;corners of FOV
 i1	=fov(0)
 j1	=fov(1)
 i2	=fov(2)
 j2	=fov(3)
 nxx	=i2-i1+1
;center of FOV
 i0	=i1+nxx/2
 j0	=j1+nxx/2
;(x,y) coordinates in arcsec from FOV center
 xfov 	=float(i0-ic)*dx
 yfov 	=float(j0-jc)*dy
;forcing FOV inside solar disk
 r_fov =sqrt(xfov^2+yfov^2)
 if (r_fov ge r_sun) then xfov=xfov*(r_sun/r_fov)*0.9999
 if (r_fov ge r_sun) then yfov=yfov*(r_sun/r_fov)*0.9999
;rotation of image into ecliptic plane
 x_a 	=xfov*a11+yfov*a12 
 y_a 	=xfov*a21+yfov*a22 
;latitude difference to solar subpoint A
 x_rot	=x_a 
 lat_a	=(180./!pi)*asin((y_a/r_sun)/sqrt(1.-(x_a/r_sun)^2))
 lat	=lat_a+b0
 r_lat	=r_sun*cos(lat*!pi/180.)
 y_rot2=(r_sun^2-r_lat^2)
 if (y_rot2 le 0.) then y_rot=0.
 if (y_rot2 gt 0.) and (y_a gt 0.) then y_rot=+sqrt(y_rot2)
 if (y_rot2 gt 0.) and (y_a lt 0.) then y_rot=-sqrt(y_rot2)
 sin_a	=(x_rot/r_lat)
 if (sin_a ge +1.) then sin_a=+1.
 if (sin_a le -1.) then sin_a=-1.
 lon_a	=(180./!pi)*asin(sin_a)
 lon	=lon_a+l0 
;heliographic longitude and latitude
 if (lon ge 0) then west='W'
 if (lon lt 0) then west='E'
 if (lat ge 0) then north='N'
 if (lat lt 0) then north='S'
 if (abs(lat)+0.5 ge 10) then lat_deg=string(abs(lat)+0.5,'(i2)')
 if (abs(lon)+0.5 ge 10) then lon_deg=string(abs(lon)+0.5,'(i2)')
 if (abs(lat)+0.5 lt 10) then lat_deg='0'+string(abs(lat)+0.5,'(i1)')
 if (abs(lon)+0.5 lt 10) then lon_deg='0'+string(abs(lon)+0.5,'(i1)')
 pos=north+lat_deg+west+lon_deg
end
