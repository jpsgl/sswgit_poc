pro nlfff_prof_sun,dir,iev,day,t5,dtol,time,efree,ind_step,e1,e2,te1,te2,err

;_______________________READ SUN DATA2 AR11158 (2011 FEB 12-17)_______________
if (strpos(dir,'paper_nlfff') ne -1) then begin	
 restore,dir+'test_sun_ef2.sav'         	;12 min cadence
 time2_sun=15.+t/24.				;time in units of days
 ep2_sun =e_p*100.		        	;conversion of 10^32 to 10^30 erg
 en2_sun =e_n*100.
 ef2_sun =(e_n-e_p)*100.

 ;Xudong Sun data
 thour_sun=(time2_sun-day)*24.
 indx	 =where((thour_sun ge t5(0)) and (thour_sun le t5(4)),nt)
 time    =thour_sun(indx)
 efree   =ef2_sun(indx)
endif

;_______________________READ SUN DATA2 (X-FLARES)______________________________
if (strpos(dir,'work_global') ne -1) then begin	
 iev_    =[12,37,66,67,147,148,220,344,349,351,384]
 di	=min(abs(iev-iev_),imin)
 iev	=iev_(imin)				;Sun data exist for this flare
 readcol,dir+'emag_xu_evol.txt',nrx,dateobs0,efree0,epot0,enp0,c,efree1,ep1,enp1,$
   skip=1,format='(I,A,F,F,F,F,F,F,F)'
 ind_x  =where(nrx eq iev,nt)
 hour   =strmid(dateobs0(ind_x),9,2)
 minu   =strmid(dateobs0(ind_x),11,2)
 time   =hour+minu/60.
 ep     =ep1(ind_x)/1.e30
 efree  =efree1(ind_x)/1.e30
endif

;________________________PEAK AND VALLEY_________________________________________
t3_hr   =t5[0]
t1_hr   =t5[1]
t0_hr   =t5[2]
t2_hr   =t5[3]
t4_hr   =t5[4]
ind1    =where((efree gt 0) and (time le t0_hr) and (time ge t1_hr-dtol))
ind2    =where((efree gt 0) and (time gt t0_hr) and (time le t2_hr+dtol))
e1      =max(efree(ind1),ie1)
e2      =min(efree(ind2),ie2)
te1     =time(ind1(ie1))
te2     =time(ind2(ie2))
it1     =ind1(ie1)
it2     =ind2(ie2)
ind     =findgen(nt)
ind_step=ind(it1:it2)

;________________________ERROR ESTIMATE___________________________
ediff   =e2-e1
err	=median(abs(efree(1:nt-1)-efree(0:nt-2)))

end
