pro euvi_spline1,xp,yp,zp,x,y,z,d,r
;+
; Project     : STEREO
;
; Name        : EUVI_SPLINE
;
; Category    : Data analysis
;
; Explanation : This routine provides a spline interpolation of 3D
;		spline points (xp,yp,zp) with finer resolution (x,y,z),
;		and computes distance along field line (d), distance
;		from Sun center (r), all in pixel units, as well as
;		the distance normalized to units or solar radii (r_n).
;		Difference to EUVI_SPLINE.PRO
;		-no input structure PARA is needed, but [xp,yp,zp]
;		 are assumed to be normalized to solar radii with
;	         Sun center of [0,0,0]
;
; Syntax      : IDL>euvi_spline,para,xp,yp,zp,x,y,z,d,r
;
; Inputs      : xp,yp,zp= 3D coordinates of spline points (in pixels)
;
; Outputs     : x,y,z	= 3D coordinates interpolated (in solar radii)
;		d	= distance along field line (insolar radiis)
;		r	= distance to Sun center (in solar radii)
;
; History     : 30-Jan-2009, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com

np     =n_elements(xp)
dp     =fltarr(np)
for i=1,np-1 do dp(i)=dp(i-1)+sqrt((xp(i)-xp(i-1))^2+(yp(i)-yp(i-1))^2)
rp     =sqrt(xp^2+yp^2+zp^2)
hp     =(rp-1.)*696.           ;in units of Mm

spline_p,xp,yp,x,y
n     =n_elements(x)
d     =fltarr(n)
for i=1,n-1 do d(i)=d(i-1)+sqrt((x(i)-x(i-1))^2+(y(i)-y(i-1))^2)
if (np eq 2) then z=interpol(zp,dp,d)
if (np eq 3) then z=interpol(zp,dp,d,/quadratic)
if (np ge 4) then z=interpol(zp,dp,d,/spline)
r       =sqrt(x^2+y^2+z^2)
end
