pro magn_fieldline,xm,ym,zm,ds,hmax,coeff,field,ns,nm
;+
; Project     : SOHO/MDI, SDO/HMI, STEREO
;
; Name        : MAGN_FIELDLINE
;
; Category    : Magnetic data modeling
;
; Explanation : Subroutine in support of MAGN_FIELD.PRO,
;               calculting a single field line
;
; Syntax      : IDL>magn_fieldline,xm,ym,zm,ds,hmax,coeff,field,ns,nm
;
; Inputs      : xm	= x-coordinate of start of field_line
;               ym	= y-coordinate of start of field_line
;               zm	= z-coordinate of start of field_line
;               ds      = spatial resolution along field line
;		hmax    = maximum altitude of field line (in solar radii) 
;               coeff   = magnetic model coefficients [b1,x1,y1,z1,a1]
;
; Outputs     ; field[NS,5] = field line coordinates
;		ns	= number of points along field line
;		nm	= point index from 1st footpoint to starting point
;
; History     : 16-May-2011, Version 1 written by Markus J. Aschwanden
;               30-Jan-2012, Version 2 released to SSW
;
; Contact     : aschwanden@lmsal.com
;-

;____________________FIELD LINE HEIGHT LOOP_________________________
nfmax   =1000
f   	=fltarr(nfmax,5)
f(0,0)	=xm
f(0,1)	=ym
f(0,2)	=zm
polar	=1.
for is=1,nfmax-1 do begin
 x1	=f(is-1,0)
 y1	=f(is-1,1)
 z1	=f(is-1,2)
 magn_vector,x1,y1,z1,ds,coeff,polar,x2,y2,z2,bx,by,bz,b_tot,endv,alpha
 r2	=sqrt(x2^2+y2^2+z2^2)
 if (r2 le 1.) or (r2 ge (1.+hmax)) or (is ge nfmax-1) or (endv eq 1) then $
  goto,first_footpoint
 f(is,0)=x2
 f(is,1)=y2
 f(is,2)=z2
 f(is,3)=b_tot
 f(is,4)=alpha
endfor
FIRST_FOOTPOINT:
nf1	=is < (nfmax-1)
nm	=nf1-1
f(0,4)  =f(1,4)
if (nf1 ge 2) then begin
 f[nf1-1,3]=f[nf1-2,3]
 f[nf1-1,4]=f[nf1-2,4]
 for j=0,4 do f[0:nf1-1,j]=reverse(f[0:nf1-1,j])
endif
polar	=-1.
for is=nf1,nfmax-1 do begin
 x1	=f(is-1,0)
 y1	=f(is-1,1)
 z1	=f(is-1,2)
 magn_vector,x1,y1,z1,ds,coeff,polar,x2,y2,z2,bx,by,bz,b_tot,endv,alpha
 r2	=sqrt(x2^2+y2^2+z2^2)
 if (r2 lt 1.) or (r2 gt (1.+hmax)) or (is ge nfmax-1) or (endv eq 1) then $
  goto,end_fieldline
 f(is,0)=x2
 f(is,1)=y2
 f(is,2)=z2
 f(is,3)=b_tot
 f(is,4)=alpha
endfor
END_FIELDLINE:
ns	=is < (nfmax-1)
field   =fltarr(ns,5)
for j=0,4 do field[*,j]=f[0:ns-1,j]
end

