pro euvi_display,files,color,movie,images,index

;+
; Project     : STEREO
;
; Name        : EUVI_DISPLAY 
;
; Category    : Data management and graphics
;
; Explanation : This routine reads a sequence of EUVI files specified
;		in FILES and displays them sequentially in an IDL window.
;
; Syntax      : IDL>euvi_display,files,color,movie,images,index
;
; Inputs      ; files 	   : string array of file names
;		color     : 0=automated min/max, 1=interactive
;		movie     : 0=automated stepping, 1=interactive stepping
;
; Outputs     : images(*,*,n) = array of n subimages 
;		index(n)      = array of n image index structures
;
; History     : Feb 2007, Version 1 written by Markus J. Aschwanden
;		1-May 2008, image array DBLARR --> INTARR
;
; Contact     : aschwanden@lmsal.com
;-

nfiles	=n_elements(files)	
help,files,index
for i=0,nfiles-1 do print,files(i)
;data	=readfits(files,index)
 data	=sccreadfits(files,index)	;multiple files
 secchi_prep,file,index,data
for ifile=0,nfiles-1 do begin
 print,'reading datafile = ',files(ifile)
 image	=data(*,*,ifile)
 index_ifile=index(ifile)
;euvi_point,index_ifile			;corrects pointing information
 sb_icerdiv2,index_ifile,image
 dateobs=index_ifile.date_obs
 texp	=index_ifile.exptime
 wave	=string(index_ifile.wavelnth,'(I3)')+' A'
 compr	=index_ifile.comprssn
 compf	=index_ifile.compfact
 crpix1 =index_ifile.crpix1 &if (ifile eq 0) then x0=crpix1
 crpix2 =index_ifile.crpix2 &if (ifile eq 0) then y0=crpix2
 dim	=size(image)
 nx	=dim(1)
 ny	=dim(2)
 inonzero=where(image gt 0,nind)
 if (nind le 0) then print,'ERROR: Image has no non-zero values'
 if (nind ge 1) then begin
  if (ifile eq 0) then begin
   z1	=min(image(inonzero))
   z2	=max(image(inonzero))
  endif
  print,'Image sequence # = ',ifile
  print,'File             = ',files(ifile)
  print,'Wavelength       = ',wave
  print,'image size nx,ny = ',nx,ny,    format='(a,2i8)'
  print,'color range [DN] = ',z1,z2,    format='(a,2i8)'
  print,'compression      = ',compr,compf,format='(a,2i8)'
  print,'exposure timr    = ',texp
  change_color:
 if (color ge 0) then begin
  if (ifile eq 0) then begin
   window,0,xsize=nx<1600,ysize=ny<1600
   loadct,3
   !x.range=[0,nx]
   !y.range=[0,ny]
   !x.style=1
   !y.style=1
   texp0=texp
  endif
  q	=texp0/texp
  image =z1+(image-z1)*q
  x1_	=0.0
  x2_	=1.0
  y1_	=0.0
  y2_	=1.0
  io	=0
  if (io eq 0) then begin
   nxw  =long(!d.x_vsize*(x2_-x1_)+0.5)
   nyw  =long(!d.y_vsize*(y2_-y1_)+0.5)
   z    =congrid(image,nxw,nyw)
  endif
  if (io ne 0) then z=image

  !p.position=[x1_,y1_,x2_,y2_]
  if (ifile eq 0) then plot,[0,0],[0,0]
  tv,bytscl(alog10(z),min=alog10(z1),max=alog10(z2)),x1_,y1_,xsize=(x2_-x1_),ysize=(y2_-y1_),/normal
  xyouts,10,ny-20,dateobs+' '+wave,size=5
  if (color eq 1) then begin
   read,'Enter lower bound of color range [DN] = ',z1
   read,'Enter upper bound of color range [DN] = ',z2
   color=0	;for next time no color change again
   goto,change_color
  endif
  action=1
  if (movie eq 1) then read,'continue ? [0=stop, 1=next image] ',action
  if (action eq 0) then goto,out_of_here 
endif

 endif
;if (ifile eq 0) then images=dblarr(nx,ny,nfiles)
 if (ifile eq 0) then images=intarr(nx,ny,nfiles)
;images(*,*,ifile)=image
 images(*,*,ifile)=fix(image)
endfor
out_of_here:
end
