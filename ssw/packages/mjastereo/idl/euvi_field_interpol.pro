pro euvi_field_interpol,loopfile,para,nxfield,nyfield,fov_foot,hmax,dmax,dran,fieldfile

;+
; Project     : STEREO
;
; Name        : EUVI_FIELD_INTERPOL
;
; Category    : 3D stereoscopic data analysis and modeling 
;
; Explanation : This algorithm generates a 3D field aligned with
;		the skeleton of stereoscopically triangulated loops
;		whose 3D coordinates are stored in the dataifle LOOPFILE.
;
; Syntax      : IDL> euvi_field_interpol,loopfile,para,nxfield,nyfield,$
;                       fov_foot,hmax,dmax,fieldfile
;
; Inputs      : loopfile   ='~/work_stereo_euvi/070509_A_trace.dat'
;		para	  ;structure containing image parameters
;		nxfield    =50 ;number of field line footpoints in x-direction
;		nyfield    =50 ;number of field line footpoints in y-direction
;		fov_field  =[0.,0.,1.,1.] ;field-of-view relative to FOV
;		dmax      =10 ;footpoint proximity range [pixels]
;		dran      =100 ;maximum field line range [pixels]
;		nops      =0   ;0=create ps-file, -1=no ps-file
;
; Outputs     ; fieldfile   =output file with (x,y,z) coordinates of field lines
;
; History     : May-2008, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

;___________________needed parameters____________________________________
crpix1	=para.crpix1
crpix2	=para.crpix2
cdelt1	=para.cdelt1
rsun	=para.rsun
rad_pix =rsun/cdelt1

;___________________stereoscopic loop coordinates_________________________
readcol,loopfile,iloop_,ix_,iy_,iz_,ih_,ih_raw,dh_,nsm_,wave_,chi2_,q1_,q2_
euvi_loop_renumerate,iloop_,wave_
nloop	=long(max(iloop_)+1)
print,'Number of loops    NLOOP   =',nloop

;___________________x,y,z-coordinates of 3D field line footpoints__________
;=====================box in line-of-sight================================
x1c	=min(ix_)-crpix1
x2c	=max(ix_)-crpix1
y1c	=min(iy_)-crpix2
y2c	=max(iy_)-crpix2
;=====================box in heliographic longitude/latitude==============
;x_	=ix_-crpix1
;y_	=iy_-crpix2
;r_	=ih_+rad_pix
;helio_trans,0.,0.,0.,0.,x_,y_,r_,lon,lat,rad_pix
;if (max(lon) ge 180) then begin
; ind1	=where(lon ge 180)
; lon(ind1)=lon(ind1)-360.
;endif
;lon0	=min(lon) 
;lon1	=max(lon) 
;lat0	=min(lat)
;lat1	=max(lat)
;helio_trans2,0.,0.,0.,0.,lon0,lat0,rad_pix,x1c,y1c,z1,rad_pix
;helio_trans2,0.,0.,0.,0.,lon1,lat1,rad_pix,x2c,y2c,z2,rad_pix
;=========================================================================
x1	=x1c+crpix1
x2	=x2c+crpix1
y1	=y1c+crpix2
y2	=y2c+crpix2
x_	=x1+abs(x2-x1)*[fov_foot(0),fov_foot(2)]
y_	=y1+abs(y2-y1)*[fov_foot(1),fov_foot(3)]
xfoot   =x_(0)+(x_(1)-x_(0))*(findgen(nxfield)+0.5)/float(nxfield)
yfoot   =y_(0)+(y_(1)-y_(0))*(findgen(nyfield)+0.5)/float(nyfield)
zfoot   =fltarr(nxfield,nyfield)
for i=0,nxfield-1 do begin
 for j=0,nyfield-1 do begin
  zfoot(i,j)=sqrt(rad_pix^2-(xfoot(i)-crpix1)^2-(yfoot(j)-crpix2)^2)
 endfor
endfor

;___________________copy loop coordinates from input to output file________
idat	=strpos(fieldfile,'.dat')
isav	=strpos(fieldfile,'.sav')
if (idat gt 0) then begin
 openw,2,fieldfile
 n	=n_elements(iloop_)
 for i=0,n-1 do printf,2,iloop_(i),ix_(i),iy_(i),iz_(i),ih_(i),ih_raw(i),$
  dh_(i),nsm_(i),poly_(i),chi2_(i),q1_(i),q2_(i),format='(i6,6f8.1,2i6,3f6.2)'
endif
if (isav gt 0) then begin
 ifield_=iloop_ 
 xfield_=ix_
 yfield_=iy_
 zfield_=iz_
 hfield_=ih_
 dfield_=dh_
endif

;_____________________compute field vectors of stereoscopic loops___________
for iloop=0,nloop-1 do begin
 ind	 =where(iloop_ eq iloop,ns)
;x       =ix_(ind)    
;y       =iy_(ind)   
;z       =iz_(ind)         
;============================================
 xp      =ix_(ind)    
 yp      =iy_(ind)   
 zp      =iz_(ind)         
 euvi_spline,para,xp,yp,zp,x,y,z,d,r,r_n	
 ns	 =n_elements(x)
;===========================================
 dx 	 =x(1:ns-1)-x(0:ns-2)   ;x-component of vector
 dy 	 =y(1:ns-1)-y(0:ns-2)	
 dz 	 =z(1:ns-1)-z(0:ns-2)
 if (iloop eq 0) then begin
  xx	=x
  yy	=y
  zz	=z
  dxx	=[dx,dx(ns-2)]
  dyy	=[dy,dy(ns-2)]
  dzz	=[dz,dz(ns-2)]
 endif
 if (iloop ge 1) then begin
  xx	=[xx,x]
  yy	=[yy,y]
  zz	=[zz,z]
  dxx	=[dxx,dx,dx(ns-2)]
  dyy	=[dyy,dy,dy(ns-2)]
  dzz	=[dzz,dz,dz(ns-2)]
 endif
endfor
len	=sqrt(dxx^2+dyy^2+dzz^2)
dxx	=dxx/len
dyy	=dyy/len
dzz	=dzz/len
np	=n_elements(iloop_)		  ;number of points

;________________________field line reconstruction___________________________
nsfield  =lonarr(nxfield,nyfield)
nsmax	=long(!pi*hmax*rad_pix)		;maximum number of loop coordinates
iloop	=nloop				;# of first interpolated field line
for iy=0,nyfield-1 do begin
 for ix=0,nxfield-1 do begin
  ds0	=fltarr(np)+1.0			 ;positive pixel step (skeleton loops)
  start=0
restart:
  xfield =fltarr(nsmax)			  ;new field line coordinate arrays
  yfield =fltarr(nsmax)
  zfield =fltarr(nsmax)
  xfield(0)=xfoot(ix)			 ;footpoint coordinates
  yfield(0)=yfoot(iy)
  zfield(0)=zfoot(ix,iy)
  h0	=sqrt((xfield(0)-crpix1)^2+(yfield(0)-crpix2)^2+zfield(0)^2)/rad_pix-1.
  for is=1,nsmax-1 do begin
   x0	=xfield(is-1)			 ;starting point of field line vector
   y0	=yfield(is-1)
   z0	=zfield(is-1)
   dd	=sqrt((xx-x0)^2+(yy-y0)^2+(zz-z0)^2)>0.001 	;distance
   if (is eq 1) and (min(dd) ge dmax) then goto,nextloop
   ww	=1./dd^2			;weight = 1/distance^2
   ind	=where(dd le dran,nind)
   if (nind le 0) then goto,endloop
   dx	=total(ds0(ind)*dxx(ind)*ww(ind))/total(ww(ind)) ;weighted vector comp
   dy	=total(ds0(ind)*dyy(ind)*ww(ind))/total(ww(ind))
   dz	=total(ds0(ind)*dzz(ind)*ww(ind))/total(ww(ind))
   len	=sqrt(dx^2+dy^2+dz^2)		;length of vector
   x1	=x0+dx/len			;normalization of vector component	
   y1	=y0+dy/len
   z1	=z0+dz/len
   h1	=sqrt((x1-crpix1)^2+(y1-crpix2)^2+z1^2)/rad_pix-1.	;height
   if (is eq 1) and (start eq 0) and (h1 lt h0) then begin
    ds0=-ds0				;reverse interpolation direction
    start=start+1
    goto,restart
   endif
   r_cen=rad_pix*(1.+h1)
   x_cen=x1-crpix1
   y_cen=y1-crpix2
   helio_trans,0.,0.,0.,0.,x_cen,y_cen,r_cen,lon,lat,rad_pix
;  if (lon ge 180) then lon=lon-360.
;  if (lon lt lon0) or (lon gt lon1) or $			;outside range
;     (lat lt lat0) or (lat gt lat1) or $
   if (x1 le x_(0)) or (x1 ge x_(1)) or $
      (y1 le y_(0)) or (y1 ge y_(1)) or $
      (h1  lt 0.  ) or (h1  gt hmax) then begin			;full loops
;     (h1  lt 0.  ) or (h1  gt hmax) or (h1 lt h0) then begin	;half loops
    goto,endloop
   endif
   xfield(is)=x1			 ;continuing field line
   yfield(is)=y1
   zfield(is)=z1
   h0	=h1				;new height
  endfor
  endloop:
  ns	=is				;number of field line positions
  if (ns ge 3) then begin
   r	=sqrt(xfield^2+yfield^2+zfield^2)
   h	=(r-rad_pix)
   h_mm =h*cdelt1*0.725
   h_raw=h_mm
   dh	=0.
   nsm	=0
   i_poly=0
   chi2	=0.
   q1	=0.
   q2	=0.
   if (idat gt 0) then begin		;saving field line coord in ASCI file
    for is=0,ns-1 do printf,2,iloop,xfield(is),yfield(is),zfield(is),$
      h_mm(is),h_raw(is),dh,nsm,i_poly,chi2,q1,q2,format='(i6,6f8.1,2i6,3f6.2)'
   endif
   if (isav gt 0) then begin
    ifield_=[ifield_,intarr(ns)+iloop] 
    xfield_=[xfield_,xfield(0:ns-1)]
    yfield_=[yfield_,yfield(0:ns-1)]
    zfield_=[zfield_,zfield(0:ns-1)]
    hfield_=[hfield_,h_mm(0:ns-1)]
    dfield_=[dfield_,fltarr(ns)+dh]
   endif
  endif
  if (iloop mod 100) eq 0 then print,'Loop #',iloop,' ix,iy,ns=',ix,iy,ns
  iloop=iloop+1
  nextloop:
 endfor
endfor
if (idat gt 0) then close,2

;__________________________saving field line coord in IDL savefile___________
if (isav gt 0) then begin
 iloop_ =ifield_
 ix_	=xfield_
 iy_	=yfield_
 iz_	=zfield_
 ih_	=hfield_
 dh_	=dfield_
 save,filename=fieldfile,iloop_,ix_,iy_,iz_,ih_,ih_,dh_
endif
print,'Field line coordinates stored in file = ',fieldfile
end
