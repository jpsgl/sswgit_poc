pro euvi_dimming_stereo,coord,sep,alt_sun,lon_deg,lat_deg

dim	=size(coord)
npar	=dim(1)
nsc	=dim(2)
nwave	=dim(3)
alt_sun	=fltarr(nwave)
lon_deg	=fltarr(nwave)
lat_deg	=fltarr(nwave)
sep_a	=sep(0)*!pi/180.
sep_b	=sep(1)*!pi/180.
dsep	=sep_a-sep_b
print,'Spacecraft separation angle   dsep = ',sep(0)-sep(1)
for iwave=0,nwave-1 do begin
 xa	=coord(0,0,iwave)			;in solar radii
 xb	=coord(0,1,iwave)
 ya	=coord(1,0,iwave)
 yb	=coord(1,1,iwave)
 phia	=coord(2,0,iwave)*!pi/180.
 phib	=coord(2,1,iwave)*!pi/180.
 pa	=coord(3,0,iwave)*!pi/180.
 pb	=coord(3,1,iwave)*!pi/180.
 da	=sqrt(xa^2+ya^2)
 db	=sqrt(xb^2+yb^2)
 x_a	=da*cos(phia+pa)
 x_b	=db*cos(phib+pb)
 y_a	=da*sin(phia+pa)
 y_b	=db*sin(phib+pb)
 y	=(y_a+y_b)/2.
 rho	=sqrt(x_a^2+((x_b-x_a*cos(dsep))/sin(dsep))^2)
 r	=sqrt(rho^2+y^2)
 lon_a	=asin(x_a/rho)
 lat	=asin(y/rho)
 lon	=lon_a+sep_a				;longitude from Earth
 lon_deg(iwave)=lon*180./!pi
 lat_deg(iwave)=lat*180./!pi
 alt_sun(iwave)=r-1.
endfor
end
 
