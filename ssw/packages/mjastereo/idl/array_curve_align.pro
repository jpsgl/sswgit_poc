pro array_curve_align,xw,yw,zw,xp,yp,xpnew,ypnew,test
;+
; Project     : STEREO
;
; Name        : ARRAY_CURVE_ALIGN 
;
; Category    : Auxiliary subroutine 
;
; Explanation : aligns spline points [xp,yp]-->[xpnew,ypnew] to ridge (s-axis)
;		of curved array [xw,yw]. This subroutine is called from 
;		EUVI_STEREO.PRO
;
; Syntax      : IDL> array_curve_align,xw,yw,zw,xp,yp,xpnew,ypnew,test
;
; Inputs      : xw(ns,nw)	= x-coordinates of curved 2D array (s=length, w=width)	
;               yw(ns,nw)	= y-coordinates of curved 2D array 
;               zw(ns,nw)	= values z(x,y)	at coordinate points [xw,yw]
;		xp(np)		= x-coordinates of spline points at s-axis of [ns,nw]
;		yp(np)		= y-coordinates of spline points at s-axix of [nx,nw]
;	        test		= 1 for test output on screen (0 for silent)
;
; Outputs     ; xpnew(np)	= corrected x-coordinates aligned with ridge 
;		ypnew(np)	= corrected y-coordinates aligned with ridge 
;
; Other routines called : PARABOL_MAX.PRO from library IDL_GEN 
;
; History     : Oct/Nov 2007, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

dim	=size(zw)
ns	=dim(1)
nw	=dim(2)
np	=n_elements(xp)
dis	=long(float(ns)*(1./float(np-1)))
iw0	=nw/2
xs	=0+dis*findgen(np)
wcorr	=fltarr(np)
xpnew	=fltarr(np)
ypnew	=fltarr(np)
for ip=0,np-1 do begin
 is1	=long(float(ns)*(float(ip-0.5)/float(np-1)))>0
 if (ip eq np-1) then is1=(ns-1)-dis
 is2	=is1+dis
 z_w	=fltarr(3)
 for is=is1,is2 do begin
  dpeak	=zw(is,iw0)-(zw(is,iw0-1)+zw(is,iw0+1))/2.
  if (dpeak gt 0) then z_w=z_w+reform(zw(is,iw0-1:iw0+1))
 endfor
 x	=[-1,0,1]
 y	=z_w(0:2)
 parabol_max,x,y,xmax,ymax,xx,yy
 if (test ge 1) then print,xmax
 wcorr(ip)=xmax
 is0	=long((is1+is2)/2)
 dx	=xw(is0+1,iw0)-xw(is0,iw0)
 dy	=yw(is0+1,iw0)-yw(is0,iw0)
 daxis	=sqrt(dy^2+dx^2)
 sina	=dy/daxis
 cosa	=dx/daxis
 xpnew(ip)=xp(ip)+wcorr(ip)*sina
 ypnew(ip)=yp(ip)-wcorr(ip)*cosa
endfor

if (test eq 1) then begin
 window,1,xsize=1024,ysize=256
 clearplot
 color=128
 contour,zw,level=1+2*findgen(16)
 oplot,xs,(nw-1)/2.+wcorr,thick=3,color=color
endif
if (test ge 2) then begin
 color=172
 contour,zw,level=1+2*findgen(16),/overplot,color=color
 oplot,xs,(nw-1)/2.+wcorr,thick=3,color=color
endif
end
 
