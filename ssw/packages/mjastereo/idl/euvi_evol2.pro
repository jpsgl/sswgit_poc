pro euvi_evol2,fovfile,plotname,ct,pos
;+
; Project     : STEREO
;
; Name        : EUVI_EVOL 
;
; Category    : Data graphics 
;
; Explanation : This routine plots the GOES and EUV light curves 
;		for spacecraft A and B, 4 FOV images with difference images, 
;		and creates a postscript file.
;
; Syntax      : IDL> euvi_evol,fovile
;
; Inputs      : fovfile = '~/euvi/20071214/20071214_1400_171_a_fov.sav'
;
; Outputs     : postscript file of plot *.ps
;
; Restrictions: The fovfiles of spacecraft 'a' and 'b' must exist,
;	        requiring to previously run EUVI_EVOL for sc='a' and sc='b'
;
; History     : 10-Nov-2008, written by Markus Aschwanden 
;
; Contact     : aschwanden @ lmsal.com
;-

;INPUT SAVE FILES____________________________________________________
;fovfile = '~/euvi/20071214/20071214_1400_171_a_fov.sav'
;fovfile:date,t1,t2,sc,wave,images_fov,index,id,pos,fov
;evolfile:time_goes,goes_high,goes_low,hxr_norm,t_i,flare,nflare,flare_dn,qflare
ipos	=strpos(fovfile,'fov.sav')
fovfiles =strmid(fovfile,0,ipos-2)+['a','b']+'_fov.sav'
evolfiles=strmid(fovfile,0,ipos-2)+['a','b']+'_evol.sav'
if (n_params(0) le 1) then plotname=strmid(fovfile,0,ipos-2)+'ab'
if (n_params(0) le 3) then pos=' '

;CREATES PLOT________________________________________________________
form	=1		;portrait
char	=1
fignr	=''
unit	=0
ref	=''
if (ct eq 0) then io1=1
if (ct ge 1) then io1=3
for io=0,io1,io1 do begin 
 fig_open,io,form,char,fignr,plotname,unit
 ab_	=['A','B']
 if (ct eq 0) then color=[0,0]
 if (ct ge 1) then color=[100,200]
 xrange =[0,0]

for isc=0,1 do begin

 file_exist=findfile(fovfiles(isc),count=nfiles)
 if (nfiles le 0) then begin
  if (io eq 0) then print,'FILE DOES NOT EXIST =',fovfiles(isc)
  goto,endloop
 endif
 if (io eq 0) then print,'READ SAVEFILE =',fovfiles(isc)
 restore,fovfiles(isc)

 file_exist=findfile(evolfiles(isc),count=nfiles)
 if (nfiles le 0) then begin
  if (io eq 0) then print,'FILE DOES NOT EXIST =',evolfiles(isc)
  goto,endloop
 endif
 if (io eq 0) then print,'READ SAVEFILE =',evolfiles(isc)
 restore,evolfiles(isc)

 ;GOES class
 if (id ge 1) then begin
  catfile='~/euvi/euvi_catalog.txt'
  euvi_event,catfile,id,line,date,t1,t2,sc
  goes	=strtrim(strmid(line,49,5),2)
 endif
 goes2=' '
 maxflux  =max(alog10(goes_low))
 if (maxflux ge -9) then goes2='Z'+strtrim(string(10.^(maxflux+9),'(f4.1)'),2)
 if (maxflux ge -8) then goes2='A'+strtrim(string(10.^(maxflux+8),'(f4.1)'),2)
 if (maxflux ge -7) then goes2='B'+strtrim(string(10.^(maxflux+7),'(f4.1)'),2)
 if (maxflux ge -6) then goes2='C'+strtrim(string(10.^(maxflux+6),'(f4.1)'),2)
 if (maxflux ge -5) then goes2='M'+strtrim(string(10.^(maxflux+5),'(f4.1)'),2)
 if (maxflux ge -4) then goes2='X'+strtrim(string(10.^(maxflux+4),'(f4.1)'),2)
 if (strlen(goes) eq 0) then goes=goes2

 nte	 =n_elements(flare)
 for i=0,nte-1 do begin
  ia=i-2  
  if (i lt 2) then ia=0 
  if (i gt nte-3) then ia=nte-5
  ib=ia+4
  fmed	=median(flare(ia:ib))
  if (flare(i) le fmed/1.5) or (flare(i) ge fmed*1.5) then flare(i)=fmed
 endfor
 flare_min=min(flare)
 flare_max=max(flare)
 flare_dn =flare_max-flare_min
 qflare   =flare_dn/flare_max
 flare_norm=(flare-flare_min)/(flare_max-flare_min)

 dim	 =size(images_fov)
 nx      =dim(1)
 ny      =dim(2)
 if (dim(0) le 2) then nte=1
 if (dim(0) eq 3) then nte=dim(3)
 mon=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
 date_str=strmid(date,6,2)+'-'+mon(long(strmid(date,4,2))-1)+'-'+strmid(date,2,2)
 midnight=date_str+' 00:00:00'
 r_sc	=index(0).dsun_obs  ;1.3...1.7*10^11 m
 r_au	=1.495978e11  ;m
 c	=2.99792458e8 ;m/s
 dt_light=(r_sc-r_au)/c		;light travel correction time
 t_i	=t_i-dt_light
 ind0	=where(time_goes gt 0,ngoes) 
 if (total(xrange) eq 0) then xrange=minmax(time_goes(ind0))
 if (ngoes gt 2) then ind0=where((time_goes ge xrange(0)+180.) and (time_goes le xrange(1)-180.))

 !p.position=[0.07,0.75,0.97,0.95]
 id_	=strtrim(string(id,'(i6)'),2)
 title=''
 if (goes ne ' ') then title=' GOES '+goes+' '
 if (id ge 0) then title='Event #'+id_+' '+pos+title
 label=ab_(isc)+': F='+string(flare_dn/1.e6,'(f5.2)')+' 10!U6!NDN/s, ('+string(qflare*100,'(f4.1)')+' %)'
 if (isc eq 0) then !p.title=title
 !x.range=xrange
 !y.range=[0,1.2]
 !x.title=''
 !y.title='Flux (normalized)'
 !x.style=1
 !y.style=1
 setutbase,midnight
 utplot,time_goes(ind0),goes_high(ind0)/max(goes_high)
 ymax=max(goes_high,itm)
 !p.linestyle=2
 outplot,time_goes(ind0),goes_low(ind0)/max(goes_low)
 !p.linestyle=0
 ymax=max(goes_low,itm)
 outplot,time_goes(ind0),hxr_norm(ind0),thick=4
 ymax=max(hxr_norm,itm)
 outplot,t_i,flare_norm,thick=2,linestyle=3
 outplot,t_i,flare_norm,psym=4+isc
 ymax=max(flare_norm,itm)
;xyouts_norm,0.7,0.9-0.1*isc,label,char,0,color(isc)
;goto,skip
 xpos	=0.9	&ypos=0.8
 xyouts_norm,xpos,0.8,'HXR',char
 xyouts_norm,xpos,0.7,'GOES/Hi',char
 xyouts_norm,xpos,0.6,'GOES/Lo',char
 xyouts_norm,xpos,0.5,'EUVI/A',char
 xyouts_norm,xpos,0.4,'EUVI/B',char
 oplot_norm,xpos-[0.1,0.02],ypos-[0.0,0.0],0,4
 oplot_norm,xpos-[0.1,0.02],ypos-[0.1,0.1],0,2
 oplot_norm,xpos-[0.1,0.02],ypos-[0.2,0.2],2,2
 oplot_norm,xpos-[0.1,0.02],ypos-[0.3,0.3],3,2
 oplot_norm,xpos-[0.1,0.02],ypos-[0.3,0.3],2,2
 oplot_norm,xpos-[0.1,0.02],ypos-[0.4,0.4],3,2
 oplot_norm,xpos-[0.1,0.02],ypos-[0.4,0.4],2,2
;skip:
 !noeras=1
 
;automated selection of image sequence
 if (nte le 1) then iseq=[0]
 if (nte ge 2) and (nte le 4) then iseq=long(findgen(nte))
 if (nte ge 5) then iseq=long(float(nte-1)*float(1+findgen(4))/4.)

;plot EUVI times 
 nseq	=n_elements(iseq)
 for it=0,nte-1 do begin
  if (min(abs(it-iseq)) eq 0) then oplot,t_i(it)*[1,1],!y.crange
 endfor
 
 dx	=1.0/(nseq>4)
 dy	=0.35
 asp_fov =float(ny)/float(nx)
 asp_plot=(float(dy)/float(dx))*0.75
 qv	=asp_fov/asp_plot
 nyy	=ny<long(ny/qv)
 nxx    =nx<long(nx*qv)
 i0	=0.
 j0	=0.
 if (qv ge 1.) then j0=long((ny-nyy)/2.)
 if (qv lt 1.) then i0=long((nx-nxx)/2.)
 flux_med=median(images_fov)
 for it=0,nseq-1 do begin
  x1_	=0.0+it*dx
  x2_	=x1_+dx-0.002 
  y1_	=(dy/2.)*(3-2.05*isc)
  y2_	=(dy/2.)*(4-2.05*isc) 
  image	=images_fov(i0:i0+nxx-1,j0:j0+nyy-1,iseq(it))
  z1	=min(image)
  z2	=max(image)
  dim	=size(image)
  nx_	=dim(1)/2
  ny_	=dim(2)/2
  image_=rebin(image(0:2*nx_-1,0:2*ny_-1),nx_,ny_)
  if (io eq 0) then begin
   nxw  =long(!d.x_vsize*(x2_-x1_)+0.5)
   nyw  =long(!d.y_vsize*(y2_-y1_)+0.5)
   z    =congrid(image_,nxw,nyw)
  endif
  if (io ne 0) then z=image_
  !p.position=[x1_,y1_,x2_,y2_]
  !p.title=' '
  !x.range=[0,nxx]
  !y.range=[0,nyy]
  !x.style=5
  !y.style=5
  plot,[0,0],[0,0]
  tv,bytscl(alog10(z),min=alog10(z1),max=alog10(z2)),x1_,y1_,xsize=(x2_-x1_),ysize=(y2_-y1_),/normal
; dateobs=index(iseq(it)).date_d$obs
  dateobs=index(iseq(it)).date_obs
  wavelen=index(iseq(it)).wavelnth
  xyouts_norm,0.01,0.02,strmid(dateobs,11,8)+' UT',1,0,255
  xyouts_norm,0.01,0.90,string(wavelen,'(i3)')+' A',1,0,255
  if (sc eq 'a') then sc_='AHEAD'
  if (sc eq 'b') then sc_='BEHIND'
  if (it eq 0) then xyouts_norm,0.01,0.80,'SC='+sc_,1,0,255
  !noeras=1

  it2	=iseq(it)
  if (it eq 0) then it1=0
  if (it ge 1) then it1=iseq(it-1)
 if (it1 ge 0) then begin
  ima	=images_fov(i0:i0+nxx-1,j0:j0+nyy-1,it1)
  imb	=images_fov(i0:i0+nxx-1,j0:j0+nyy-1,it2)
  za	=ima(sort(ima))
  zb	=imb(sort(imb))
  coeff =linfit(za,zb)
  diff	=imb-(coeff(0)+coeff(1)*ima)
  dim	=size(diff)
  nx_	=dim(1)/2
  ny_	=dim(2)/2
  diff_ =rebin(diff(0:2*nx_-1,0:2*ny_-1),nx_,ny_)
  if (io eq 0) then z=congrid(diff_,nxw,nyw)
  if (io ne 0) then z=diff_
  y3_	=(dy/2.)*(2-2.05*isc)
  y4_	=(dy/2.)*(3-2.05*isc)
  !p.position=[x1_,y3_,x2_,y4_]
  plot,[0,0],[0,0]
  d1	=-flux_med/2
  d2	=+flux_med/2
  tv,bytscl(z,min=d1,max=d2),x1_,y3_,xsize=(x2_-x1_),ysize=(y4_-y3_),/normal
 endif
 endfor ;for iseq=0,3

 endloop:
 endfor ;for isc=0,1
 fig_close,io,fignr,ref
endfor ;for iplot=0,3

end
