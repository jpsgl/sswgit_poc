pro nlfff_tracing_disp,imagefile,vers,para2,fov_noise,output,io

;______________CONSTANTS_________________________________________________
form    =1      ;0=landscape, 1=portrait
char    =1.0    ;character size
ref     =''     ;label at bottom of Fig.
unit    =0      ;window number
ipos    =strpos(imagefile,'.fits')
filename=strmid(imagefile,0,ipos)
plotname=filename
fignr	='_'+vers+'0'
nsm1    =long(para2(0))
rmin    =long(para2(1))
lmin    =long(para2(2))
nstruc  =long(para2(3))
qmed    =para2(4)
nsm2	=nsm1+2
flux0   =70     ;lower loop limit for cumulative loop count criterion
flux1   =100    ;lower loop limit for powerlaw fit
wid     =output(0)
fluxmin =output(1)
fluxmax =output(2)
thresh  =output(3)
base    =output(4)
nsm2    =output(5)
nlen    =output(6)
na      =output(7)
nb      =output(8)

;---------------TRACING----------------------------------------------------
loopfile0=strmid(imagefile,0,ipos)+'_manu_2d_pix.dat'
loopfile =strmid(imagefile,0,ipos)+'_auto_2d_pix.dat'
image1  =readfits(imagefile,header) 
image1	=float(image1)
dim	=size(image1)
nx	=dim(1)
ny	=dim(2)
fov 	=[0,0,nx-1,ny-1]	;field of view in pixels [x1,y1,x2,y2] 
image1	=float(image1)
if (nsm1 le 2) then image2=image1-smooth(image1,nsm2)
if (nsm1 ge 3) then image2=smooth(image1,nsm1)-smooth(image1,nsm2)

;----------------PLOT------------------------------------------------------
fig_open,io,form,char,fignr,plotname,unit
loadct,0,/silent
x1_	=0.05
x2_	=0.95
y1_	=0.25
y2_	=0.95
aspect	=float(ny)/float(nx)
if (aspect le 1.) then y2_=y1_+0.7*aspect 
if (aspect gt 1.) then x2_=x1_+0.9*aspect 
!p.position=[x1_,y1_,x2_,y2_]
!p.title=filename
!x.title=''
!y.title=''
!x.style=1
!y.style=1
!x.range=[fov(0),fov(2)]
!y.range=[fov(1),fov(3)]
plot,[0,0],[0,0]
if (io eq 0) then begin
 nxw   =long(!d.x_vsize*(x2_-x1_)+0.5)
 nyw   =long(!d.y_vsize*(y2_-y1_)+0.5)
 z     =congrid(image2,nxw,nyw)
endif
if (io ne 0) then z=image2
statistic,z,zavg,zsig
c2	= zsig*0.7
c1	=-zsig
tv,bytscl(z,min=c1,max=c2),x1_,y1_,xsize=(x2_-x1_),ysize=(y2_-y1_),/normal
in1     =fov_noise[0]
in2     =fov_noise[2]
jn1     =fov_noise[1]
jn2     =fov_noise[3]
oplot,[in1,in2,in2,in1,in1],[jn1,jn1,jn2,jn2,jn1]
xyouts,in1+5,jn1+5,'Noise area',size=char
!noeras=1

loadct,5,/silent
nflux	=[0,0]
readcol,loopfile,iloop,x,y,f,s
nloop	=max(iloop)+1
th	=2
if (io ge 1) then th=4
for i=0,nloop-1 do begin 
 ind=where(i eq iloop,ns)
 oplot,x(ind)+fov(0),y(ind)+fov(1),thick=th,color=128
endfor

;________________________SIZE DISTRIBUTION_______________________
!p.position=[0.7,0.04,0.97,0.22]
!x.range=[1.e1,1.e3]
!y.range=[1.,1.e3]
!p.title=' '
!x.title='Loop length L [pixels] '
!y.title='' 
!x.style=1
!y.style=1
plot_oo,[1,1]
xyouts_norm,0.05,0.9,'N(>L)',char,0,128
title_	=['Manual','OCCULT-2']
lm	=fltarr(2)
nflux	=fltarr(2)
slope	=fltarr(2)
for k=1,0,-1 do begin
 if (k eq 0) then file=loopfile0
 if (k eq 1) then file=loopfile
 file_exst=findfile(file,count=nfiles)
 if (nfiles eq 0) then goto,skip 
 readcol,file,iloop,x,y,f,s
 nloop	=max(iloop)+1
 print,'reading file=',file,' nloop=',nloop
 len	=fltarr(nloop)
 for i=0,nloop-1 do begin 
  ind   =where(i eq iloop,ns)
  xi	=x(ind)
  yi	=y(ind)
  dist =total(sqrt((xi(1:ns-1)-xi(0:ns-2))^2+(yi(1:ns-1)-yi(0:ns-2))^2))
  if (k eq 1) then dpix=sqrt((xi(1)-xi(0))^2+(yi(1)-yi(0))^2)
  len(i)=dist
 endfor
 len	=len/dpix
 isort=sort(len)
 len_sort=len(isort)
 n_len=findgen(nloop)+1
 if (k eq 0) then oplot,reverse(len_sort),n_len,thick=2
 if (k eq 1) then oplot,reverse(len_sort),n_len,thick=2,color=128
 lm[k]  =max(len)
 ind    =where((len ge flux0),nf)
 nflux(k)=nf
 oplot,[flux0,flux0],[1,nf],linestyle=1

 x1=alog10(flux1)
 x2=alog10(max(len))
 xx=alog10(reverse(len_sort))
 yy=alog10(n_len)
 ind=where((xx ge x1) and (yy le x2),nind)
 if (nind ge 1) then begin
  coeff=linfit(xx(ind),yy(ind),sigma=sig)
  x_     =[x1,x2]
  if (k eq 0) then oplot,10.^x_,10.^(coeff(0)+coeff(1)*x_)
  if (k eq 1) then oplot,10.^x_,10.^(coeff(0)+coeff(1)*x_),color=128
  slope(k)=coeff(1)
 endif
 !noeras=1
 skip:
endfor

ENDPLOT:
;________________________TEXT____________________________________
xyouts,0.05,0.20,'Image flux min/max',size=char,/normal
xyouts,0.35,0.20,'FMIN ='+string(fluxmin,'(I6)'),size=char,/normal
xyouts,0.50,0.20,'FMAX ='+string(fluxmax,'(I6)'),size=char,/normal
xyouts,0.05,0.18,'Image threshold, FOV noise',size=char,/normal
xyouts,0.35,0.18,'THRESH ='+strtrim(string(thresh,'(F8.2)'),2),size=char,/normal
xyouts,0.50,0.18,'FOV ='+strtrim(string(in1,'(I4)'),2)+','$
                        +strtrim(string(jn1,'(I4)'),2)+','$
                        +strtrim(string(in2,'(I4)'),2)+','$
                        +strtrim(string(jn2,'(I4)'),2),size=char,/normal
xyouts,0.05,0.16,'Loop lowpass/highpass',size=char,/normal
xyouts,0.35,0.16,'NSM1 = '+string(nsm1,'(I4)'),size=char,/normal
xyouts,0.50,0.16,'NSM2 = '+string(nsm2,'(I4)'),size=char,/normal
xyouts,0.05,0.14,'Minimum curvature radius',size=char,/normal
xyouts,0.35,0.14,'RMIN = '+string(rmin,'(I4)'),size=char,/normal
xyouts,0.50,0.14,'NLEN = '+string(nlen,'(I4)'),size=char,/normal
xyouts,0.05,0.12,'Loop width, min length',size=char,/normal
xyouts,0.35,0.12,'WID ='+string(wid,'(F4.1)'),size=char,/normal
xyouts,0.50,0.12,'LMIN ='+string(lmin,'(I4)'),size=char,/normal
xyouts,0.05,0.10,'Number of angles, curvatures',size=char,/normal
xyouts,0.35,0.10,'NA = '+string(na,'(I4)'),size=char,/normal
xyouts,0.50,0.10,'NB = '+string(nb,'(I4)'),size=char,/normal
xyouts,0.05,0.08,'Number of loops',size=char,/normal
xyouts,0.35,0.08,'NMAX = '+string(nstruc,'(I5)'),size=char,/normal
xyouts,0.50,0.08,'NLOOP = '+string(nloop,'(I4)'),size=char,/normal
xyouts,0.05,0.04,title_(0)+':',size=char,/normal
str     ='L!Dm!N='+string(lm(0),'(I4)')
xyouts,0.20,0.04,str,size=char,/normal
str     ='N(L>'+string(flux0,'(i3)')+')='+string(nflux(0),'(i3)')
xyouts,0.35,0.04,str,size=char,/normal
xyouts,0.50,0.04,'slope='+string(slope(0),'(f7.2)'),size=char,/normal
xyouts,0.05,0.02,title_(1)+':',size=char,/normal,color=128
xyouts,0.20,0.02,'L!Dm!N='+string(lm(1),'(I4)'),size=char,/normal,color=128
xyouts,0.35,0.02,'N(L>'+string(flux0,'(i3)')+')='+string(nflux(1),'(i3)'),size=char,/normal,color=128
xyouts,0.50,0.02,'slope='+string(slope(1),'(f7.2)'),size=char,/normal,color=128
xyouts,0.70,0.97,'Code = '+title_(1),size=char*1.5,/normal
fig_close,io,fignr,ref
end

