pro euvi_projection3,fov,loopfile,para,ct,hmax,win
;+
; Project     : STEREO
;
; Name        : EUVI_PROJECTION3
;
; Category    : Graphics 
;
; Explanation : This routine displays three projections of loop coordinates
;		in [x,y], [x,z], and [y,z] plane.
;
; Syntax      : IDL> euvi_projection3,fov,loopfile,para,ct,hmax
;
; Inputs      : fov	= [i1,j1,i2,j2] field-of-view in image pixels 
;		loopfile = datafile containing (x,y,z) coordinates of loops (e.g., 'loop_A.dat')
;		para    = index structure with image parameters 
;		ct	= IDL color table
;		hmax    = maximum height of stereoscopic correlation (e.g. 0.1 solar radii)
;               win     = IDL window
;
; Outputs     : postscript file called 'projection3_col.ps'
;
; History     : Oct/Nov 2007, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

if n_params(0) le 5 then win=0
;________________________________________________________________
;			SUBIMAGES  				|
;________________________________________________________________
rsun_m  =0.696*1.e9
cdelt1	=para.cdelt1
cdelt2	=para.cdelt2
crpix1	=para.crpix1
crpix2	=para.crpix2
da	=para.da/rsun_m
db	=para.db/rsun_m
rsun   	=para.rsun
a_sep	=para.sep_deg*(!pi/180.)
rad_pix	=rsun/cdelt1
r_max   =1.0+hmax
i1	=fov(0)
j1	=fov(1)
i2	=fov(2)
j2	=fov(3)
nx	=(i2-i1+1)
ny	=(j2-j1+1)
r_n	=1.0
dy	=0.2
panel1	=[0.1,0.0+dy,0.5,0.50]
panel2	=[0.5,0.0+dy,0.9,0.50]
panel3	=[0.1,0.5,0.5,1.00-dy]
panel4	=[0.5,0.5,0.9,1.00-dy]
pos	=0.
crota2	=0.
blong	=0
blat	=0.
dlat	=2.
phi	=2.*!pi*findgen(361)/360.

print,'__________________________________________________________________'
print,'|		PLOT STEREO IMAGE PAIR  			|'
print,'__________________________________________________________________'
for io=0,3,3 do begin
if (io eq 0) then begin
 window,win,xsize=1024,ysize=1024*1.4
 set_plot,'x' 
 !p.font=-1
 char   =0.7  
endif           
ipos     =strpos(loopfile,'trace')
if (io ge 1) then begin 
 form    =1     ;landscape format
 char    =0.7   ;smaller character size
 plotname=strmid(loopfile,0,ipos)
 fignr    ='pro3'
 unit    =0     
 ref     =''     
 fig_open,io,form,char,fignr,plotname,unit
endif   
if (ct ge 0) then loadct,ct

file_exist=findfile(loopfile,count=nfiles)
if (nfiles eq 0) then stop,'No previous loopfile found';
readcol,loopfile,iloop_,ix_,iy_,iz_,ih_,ih_raw,dh_,nsm_,wave_,chi2_,q1_,q2_
euvi_loop_renumerate,iloop_,wave_
nloop	=long(max(iloop_))+1
if (nloop eq 0) then stop,'Empty file with no loops'
print,'Datafile with loop coordinates found  LOOPFILE=',loopfile 
print,'Number of loops found                NLOOP   ='+string(nloop,'(i3)')

 !p.position=panel1		;STEREO A projection of field lines
 !x.style=1
 !y.style=1
 !x.range=[i1,i2]
 !y.range=[j1,j2]
 !p.title=' '
 !x.title='X direction (pixel)'
 !y.title='Y direction (pixel)'
 plot,[i1,i2,i2,i1,i1],[j1,j1,j2,j2,j1],thick=2
 for iloop=0,nloop-1 do begin 
  ind	=where(iloop_ eq iloop,np) 
 if (np ge 3) then begin
  euvi_spline,para,ix_(ind),iy_(ind),iz_(ind),x,y,z,d,r,r_n
  n	=n_elements(x)
  wavecol=50*wave_(ind(0))
  if (ct ge 0) then oplot,x,y,color=wavecol,thick=3
  if (ct lt 0) then oplot,x,y
  r_max=max(r,im)
  if (ct ge 0) then xyouts,x(im),y(im),string(iloop+1,'(I3)'),color=wavecol
  if (ct lt 0) then xyouts,x(im),y(im),string(iloop+1,'(I3)')
 endif
 endfor
 xyouts_norm,0.05,0.92,'A',char
 coordsphere2,rad_pix,pos,crota2,blat,blong,dlat,crpix1,crpix2
 oplot,crpix1+rad_pix*cos(phi),crpix2+rad_pix*sin(phi),thick=3
 !noeras=1

 !p.position=panel3 			;vertical projection A
 !p.title=' '
 !x.range=[i1,i2]
 z1=avg(iz_)-0.5*(j2-j1) 	
 z2=avg(iz_)+0.5*(j2-j1) 	
 !y.range=[z1,z2]
 !x.title=' '
 !y.title='Z direction (pixel)'
 plot,[i1,i2,i2,i1,i1],[z1,z1,z2,z2,z1],thick=2
 for iloop=0,nloop-1 do begin 
  ind	=where(iloop_ eq iloop,np) 
 if (np ge 3) then begin
  euvi_spline,para,ix_(ind),iy_(ind),iz_(ind),x,y,z,d,r,r_n
  n	=n_elements(x)
  wavecol=50*wave_(ind(0))
  oplot,x,z,color=wavecol,thick=3
  r_max=max(r,im)
  xyouts,x(im),z(im),string(iloop+1,'(I3)'),color=wavecol
 endif
 endfor
 blong	=0
 blat	=-90.
 crpix3=0
 coordsphere2,rad_pix,pos,crota2,blat,blong,dlat,crpix1,crpix3
 oplot,crpix1+rad_pix*cos(phi),crpix3+rad_pix*sin(phi),thick=3

 !p.position=panel2 			;horizontal projection 
 z1=avg(iz_)-0.5*(i2-i1) 	
 z2=avg(iz_)+0.5*(i2-i1) 	
 !x.range=[z1,z2]
 !y.range=[j1,j2]
 !y.title=' '
 !x.title='Z direction (pixel)'
 plot,[z1,z2,z2,z1,z1],[j1,j1,j2,j2,j1],thick=2
 for iloop=0,nloop-1 do begin 
  ind	=where(iloop_ eq iloop,np) 
 if (np ge 3) then begin
  euvi_spline,para,ix_(ind),iy_(ind),iz_(ind),x,y,z,d,r,r_n
  n	=n_elements(x)
  wavecol=50*wave_(ind(0))
  oplot,z,y,color=wavecol,thick=3
  r_max=max(r,im)
  xyouts,z(im),y(im),string(iloop+1,'(I3)'),color=wavecol
 endif
 endfor
 blong	=0.
 blat	=0.
 crpix3=0.
 coordsphere2,rad_pix,pos,crota2,blat,blong,dlat,crpix3,crpix2
 oplot,crpix3+rad_pix*cos(phi),crpix2+rad_pix*sin(phi),thick=3

 !p.position=panel4 			;full-Sun projection 
 !p.title='20'+strmid(loopfile,ipos-9,8)
 !x.style=5
 !y.style=5
 x1=-rad_pix*1.2
 x2=+rad_pix*1.2
 !x.range=[x1,x2]
 !y.range=[x1,x2]
 d_x=(x2-x1)
 !x.title=' '
 plot,[i1,i2,i2,i1,i1]-crpix1,[j1,j1,j2,j2,j1]-crpix2,thick=2
 for iloop=0,nloop-1 do begin 
  ind	=where(iloop_ eq iloop,np) 
 if (np ge 3) then begin
  euvi_spline,para,ix_(ind),iy_(ind),iz_(ind),x,y,z,d,r,r_n
  n	=n_elements(x)
  wavecol=50*wave_(ind(0))
  oplot,x-crpix1,y-crpix2,color=wavecol,thick=3
  oplot,x-crpix1,z,color=wavecol,thick=3
  oplot,z,y-crpix2,color=wavecol,thick=3
 endif
 endfor
;oplot,[i1,i2,i2,i1,i1]-crpix1,[z1,z1,z2,z2,z1],thick=2
;oplot,[z1,z2,z2,z1,z1],[j1,j1,j2,j2,j1]-crpix2,thick=2
 xyouts,i2-crpix1-d_x*0.03,j1-crpix2-d_x*0.03,'x',size=char*1.5
 xyouts,i1-crpix1-d_x*0.03,j2-crpix2-d_x*0.03,'y',size=char*1.5
;xyouts,i2-crpix1+20,z1,'X',size=char*2
;xyouts,i1-crpix1,z2+20,'Z',size=char*2
;xyouts,z2+20,j1-crpix2,'Z',size=char*2
;xyouts,z1,j2-crpix2+20,'Y',size=char*2
 i3	=(i1+i2)/2-crpix1
 j3	=(j2-crpix2+z1)/2.
;plot_arrow,i3,j2-crpix2+50,i3,z1-50,0.1,0.1,2
;xyouts,i3+50,j3,'Rotate 90!U0!N N',size=char
 i4	=i2-crpix1
 j4	=(j1-crpix2+j2-crpix2)/2.
 im	=(i2-crpix1+z1)/2.
;plot_arrow,i4+50,j4,z1-50,j4,0.06,0.06,2
;xyouts,im,j4+50,'Rotate 90!U0!N W',size=char
 blong	=0.
 blat	=0.
 crpix3=0.
 dlat	=10.
 coordsphere2,rad_pix,pos,crota2,blat,blong,dlat,crpix3,crpix3
 oplot,crpix3+rad_pix*cos(phi),crpix3+rad_pix*sin(phi),thick=3

 if (min(wave_) le 1) and (max(wave_) ge 1) then begin
  oplot,rad_pix*[0.7,0.8],rad_pix*[1.1,1.1],color= 50,thick=3
  xyouts,rad_pix*0.85,rad_pix*1.1,'171 A',color= 50,size=char
 endif
 if (min(wave_) le 2) and (max(wave_) ge 2) then begin
  oplot,rad_pix*[0.7,0.8],rad_pix*[1.0,1.0],color=100,thick=3
  xyouts,rad_pix*0.85,rad_pix*1.0,'195 A',color=100,size=char
 endif
 if (min(wave_) le 3) and (max(wave_) ge 3) then begin
  oplot,rad_pix*[0.7,0.8],rad_pix*[0.9,0.9],color=150,thick=3
  xyouts,rad_pix*0.85,rad_pix*0.9,'284 A',color=150,size=char
 endif

if (io ge 1) then fig_close,io,fignr,ref
endfor
end
