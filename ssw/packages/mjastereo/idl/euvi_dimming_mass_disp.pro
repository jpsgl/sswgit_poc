pro euvi_dimming_mass_disp,massfiles,io,labels

dim     =size(massfiles)
nsc     =dim(1)
if (dim(0) eq 1) then ncase=1
if (dim(0) ge 2) then ncase=dim(2)
dx_plot = 0.9/float(nsc)
dy_plot =(0.9/float(ncase))<0.2
form    =1
char    =1.0
fignr   ='_'
unit    =2
ref     =''
ipos    =strpos(massfiles(0,0),'_mass.sav')
plotname=strmid(massfiles(0,0),0,13)+'_mass'
fig_open,io,form,char,fignr,plotname,unit
for isc=0,1 do begin
 for icase=0,ncase-1 do begin
  restore,massfiles(isc,icase) ;-->t1_,t2_,mc_,wd_,m_peak,m_cme,t_cme,dt_cme,
                               ;   te,ygauss,obs,m1obs,m2obs,m3obs
  t3    =max(te)
  x1_   =0.1+dx_plot*isc
  x2_   =x1_+dx_plot*0.8
  y1_   =0.05+dy_plot*(ncase-1-icase)
  y2_   =y1_+dy_plot*0.8
  !p.position=[x1_,y1_,x2_,y2_]
  !p.title=strmid(massfiles(isc,icase),0,19)+' '+obs
  !x.range=[0,t3] ;MK
  !y.range=[0,10.]
  !x.title=' '
  !y.title=' '
  if (icase eq ncase-1) then !x.title   ='Temperature T[MK]'
  if (icase eq ncase/2) then !y.title   ='CME mass dm/dT [10!U15!N g/MK]'
  plot,[0,0],[0,0]
  for i=0,2 do begin
   oplot,[0,t1_(i),t1_(i),t2_(i),t2_(i),t3],[0,0,mc_(i),mc_(i),0,0]
   polyfill,[0,t1_(i),t1_(i),t2_(i),t2_(i),t3],[0,0,mc_(i),mc_(i),0,0],spacing=0.1,orientation=45*(i+1)
  endfor
  oplot,te,ygauss,thick=3
  xyouts_norm,0.5,0.9,'m!D171!N= '+string(m1obs,'(f5.2)')+' 10!U15!N g',char
  xyouts_norm,0.5,0.8,'m!D195!N= '+string(m2obs,'(f5.2)')+' 10!U15!N g',char
  xyouts_norm,0.5,0.7,'m!D284!N= '+string(m3obs,'(f5.2)')+' 10!U15!N g',char
  xyouts_norm,0.5,0.5,'m!DCME!N= '+string(m_cme,'(f5.2)')+' 10!U15!N g',char
  xyouts_norm,0.5,0.4,'T!D0!N  = '+string(t_cme,'(f5.2)')+' MK',char
  xyouts_norm,0.5,0.3,'!9s!3!DT!N= '+string(dt_cme,'(f5.2)')+' MK',char
; xyouts_norm,0.1,0.8,'#'+string(labels(icase),'(I1)'),char*2
  !noeras=1
 endfor
endfor
fig_close,io,fignr,ref
end

