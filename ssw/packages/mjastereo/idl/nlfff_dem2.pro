pro nlfff_dem2,dir,efile,iev,dateobs_,fov_,wave_,teem_table,vers,test1,nbin,backgr,em_thresh,dt,tmargin,qnoise
;+
; Project     : AIA/SDO
;
; Name        : NLFF_DEM
;
; Category    : Emission measure and temperature analysis   
;
; Explanation : reads AIA images, measures flare area, DEM analysis
;		pixel-wise DEM modeling
;
; Syntax      : IDL>nlfff_dem2,dir,efile,iev,dateobs_,fov_,wave_,
;		               teem_table,vers,test1,nbin,backgr,em_thresh
;
; Inputs      : DIR_TMP         STRING    = '~/DATA_TMP/'
;		DIR_DEM         STRING    = '~/work_global/NLFFF_DEM4/'
;		EFILE           STRING    = '~/work_global/goes_noaa_M1.txt'
;		IEV             INT       =        1
;		DATEOBS_        STRING    = Array[16]
;		FOV_            FLOAT     = Array[4, 16]
;		WAVE_           STRING    = Array[6]
;		TEEM_TABLE      STRING    = '~/work_global/teem_table.sav'
;		VERS            STRING    = 'run4'
;		TEST1           INT       =       -1
;		NBIN            INT       =        4
;		BACKGR          INT       =        0
;		EM_THRESH       FLOAT     =   1.00000e+23
;
; Outputs     : 
;
; History     : 11-Mar-2014, Version 1 written by Markus J. Aschwanden
;		23-Oct-2012  major revision
;
; Contact     : aschwanden@lmsal.com
;-


print,'__________________NLFFF DEM____________________________________
nwave   =n_elements(wave_)
dim	=size(dateobs_)
nt	=dim(1)
nbin	=nbin > 1
first	=0
kb	=1.3807e-16 	;erg/K		;Boltzmann constant
filename  =dateobs_[0]
plotname=dir+dateobs_[0]+'_'+vers+'_dem
ipos	=strpos(plotname,'run6')
if (ipos ge 0) then $
plotname=dir+dateobs_[0]+'_dem'+strtrim(string(nbin,'(i4)'),2)+'_run6'

savefile=plotname+'.sav'
restore,teem_table      ;-->wave_,tsig_,resp,telog,te_range,flux,dte

time_t =strarr(nt,nwave)
texp_t =fltarr(nt,nwave)
area_t =fltarr(nt,nwave)
valid  =lonarr(nt,nwave)
pix_cm_=fltarr(nt,nwave)
for iw=0,nwave-1 do begin
 wave   =wave_[iw]
 for it=0,nt-1 do begin
  x1	=fov_[0,it]
  y1	=fov_[1,it]
  x2	=fov_[2,it]
  y2	=fov_[3,it]
  filename  =dateobs_[it]
  imagefile=dir+filename+'_'+wave_[iw]+'_euv.fits'
  file	 =findfile(imagefile,count=count)
  if (count eq 0) then nlfff_AIA,dir,filename,wave_[iw],test1
  file	 =findfile(imagefile,count=count)
  if (count eq 0) then goto,skip_read			;file does not exist
  print,it,iw,'   reading : ',imagefile
  valid(it,iw)=1
  data   =readfits(imagefile,h,/silent)
  dateobs=sxpar(h,'DATE_OBS')   &time_t[it,iw]=dateobs 
  texp	 =sxpar(h,'EXPTIME')	&texp_t[it,iw]=texp
  cdelt1 =sxpar(h,'CDELT1')
  crpix1 =sxpar(h,'CRPIX1')
  crpix2 =sxpar(h,'CRPIX2')
  eph    =get_sun(dateobs)
  rsun   =eph(1)                 			;rsun=index.rsun_obs in arcsec
  rpix   =rsun/cdelt1
  dpix   =1./rpix
  pix_cm_(it,iw)=dpix*6.96e10				;pixel size in cm
  if (first eq 0) then begin				;fix first image size
   i1     =long(crpix1+x1/dpix+0.5)
   j1     =long(crpix2+y1/dpix+0.5)
   i2     =long(crpix1+x2/dpix+0.5)
   j2     =long(crpix2+y2/dpix+0.5)
   nx     =(i2-i1+1)
   ny     =(j2-j1+1)
   nxx	  =long(nx/nbin)				;rebinned size
   nyy	  =long(ny/nbin)
   nx0	  =nxx*nbin
   ny0	  =nyy*nbin
   flux_xyt=fltarr(nxx,nyy,nt,nwave)
   unce_xyt=fltarr(nxx,nyy,nt,nwave)
   first  =1						;first frame read 
  endif
  i2	  =i1+nx-1
  j2	  =j1+ny-1
  image   =float(data(i1:i2,j1:j2))/texp		;DN/s
  flux_xyt(*,*,it,iw)=rebin(image(0:nx0-1,0:ny0-1),nxx,nyy)   ;rebinning 
  SKIP_READ:
 endfor 	;fot it=0,nt-1 
 for i=0,nxx-1 do begin
  for j=0,nyy-1 do begin
   flux	=flux_xyt[i,j,*,iw] 
   ind	=where(flux gt 0)
   if (backgr eq 0) then back=min(flux(ind))
   if (backgr eq 1) then back=0.
   peak	=max(flux(ind))
   flux_xyt(i,j,*,iw)=(flux-back)   		;minimum DN/s
   unce_xyt(i,j,*,iw)=(qnoise*peak) 
  endfor
 endfor
endfor	;for iw=0,nwave-1
pix_cm	=median(pix_cm_)

;________________DEM+TE ANALYSIS____________________________________
restore,teem_table      ;-->wave_,tsig_,resp,telog,te_range,flux
te	 =10.^telog
nte      =n_elements(telog)
nsig     =n_elements(tsig_)
ne_prof  =fltarr(nt)
te_prof  =fltarr(nt)
tew_prof =fltarr(nt)
tiso_prof=fltarr(nt)
emlog_prof=fltarr(nt)
chi_prof =fltarr(nt)
eth_prof =fltarr(nt)
eth_iso  =fltarr(nt)
rad_prof =fltarr(nt)
area_prof=fltarr(nt)
vol_prof =fltarr(nt)
flux_prof=fltarr(nt,nwave)
fit_prof =fltarr(nt,nwave)
te_map   =fltarr(nxx,nyy,nt)
em_map   =fltarr(nxx,nyy,nt)
sig_map  =fltarr(nxx,nyy,nt)
dem_t	 =fltarr(nte,nt)
telog3	 =alog10(te_range[2]) 
telog4	 =alog10(te_range[3]) 
ind_te	 =where((telog ge telog3) and (telog le telog4),ind_te)
ite1	 =min(ind_te)
ite2	 =max(ind_te)
nvar     =3.

for it=0,nt-1 do begin
 print,'time step = ',it,nt-1 
 flux_obs_tot=fltarr(nwave)
 unce_obs_tot=fltarr(nwave)
 flux_fit_tot=fltarr(nwave)
 flux_fit_best=0.
 for j=0,nyy-1 do begin
  for i=0,nxx-1 do begin
   flux_obs=reform(flux_xyt[i,j,it,*])             ;flux [DN/s macropix]
   unce_obs=reform(unce_xyt[i,j,it,*])             ;flux [DN/s macropix]
   chi_best=1.e10
   em_best =0.
   telog_best=0.
   sig_best=0.1
   chi_best=1.e10
   indw   =where((flux_obs gt 0),nindw)
   nfree  =float(nindw-nvar) > 1.
   if (nindw lt nvar+1) then goto,skip
   for k=ite1,ite2 do begin
    for l=0,nsig-1 do begin
     flux_fit1=reform(flux(k,l,*))			  ;model flux 6 wavelen
     em1=median(flux_obs/flux_fit1)			  ;median ratio		
     flux_fit=flux_fit1*em1
     chi =sqrt(total((flux_obs[indw]-flux_fit[indw])^2/unce_obs[indw]^2)/nfree)
     if (chi le chi_best) then begin
      chi_best   =chi
      em_best    =em1				  	  ;[cm-5 K-1]
      telog_best =telog(k)				  
      sig_best   =tsig_(l)
      flux_fit_best=flux_fit
     endif
    endfor
   endfor
   em_map(i,j,it)=em_best
   te_map(i,j,it)=10.^telog_best
   sig_map(i,j,it)=sig_best				   
   dem_kelvin=em_best*exp(-(telog-telog_best)^2/(2.*sig_best^2))
   dem_t(*,it)=dem_t(*,it)+dem_kelvin
   flux_obs_tot=flux_obs_tot+flux_obs
   unce_obs_tot=unce_obs_tot+unce_obs 
   flux_fit_tot=flux_fit_tot+flux_fit_best
   SKIP:
  endfor
 endfor

 ind	=where(unce_obs_tot gt 0,nind)
 if (nind ge 1) then begin
  dflux	=(flux_obs_tot-flux_fit_tot)
  chi_prof(it)=sqrt(total((dflux(ind))^2/unce_obs_tot(ind)^2)/nfree)
 endif
 for iw=0,nwave-1 do begin
  flux_prof(it,iw)=flux_obs_tot(iw)
  fit_prof(it,iw)=flux_fit_tot(iw)
 endfor
endfor	;for it=0,nt-1 


;____________________DEM BACKGROUND SUBTRACTION__________________________
If (backgr eq 1) then begin
 dem_min=1.e23
 dem_t_bg=fltarr(nte)
 for it=0,nt-1 do begin
  if (it eq 0) then dem_t_bg=dem_t(*,it)
  if (it ge 1) then dem_t_bg=dem_t_bg < dem_t(*,it)
 endfor
endif

;____________________FLARE GEOMETRY_________________________________
area_pix     =fltarr(nt)
area_min     =1 				;macropixel
for it=0,nt-1 do begin
 em_max	     =max(em_map)			;abs max peak EM_MAP[*,*,NT]
 thr	     =em_thresh < (em_max*0.5)
 em_max	     =max(em_map(*,*,it))
 em_min	     =median(em_map(*,*,it))
 ind_area    =where(em_map[*,*,it] ge thr,nind_area)
 area_pix[it]=float(nind_area > area_min)*float(nbin)^2
endfor
area_max     =fltarr(nt)+max(area_pix)		 	   ;median flare area
rad_pix      =sqrt(area_max)		 		   ;area [pixels]
rad_prof     =rad_pix*pix_cm				   ;cm
area_prof    =rad_prof^2.				   ;cm^2
vol_prof     =rad_prof^3.				   ;cm^3
vol_log	     =alog10(vol_prof)

; EMISSION MEASURE_______________________________
for it=0,nt-1 do begin
 dem_te	       =reform(dem_t(*,it))			   ;cm-5  K-1 pix-1
 dem_bin       =reform(dem_t(*,it)*dte)			   ;cm-5  pix-1 Tebin-1 
 dem_tot       =total(dem_bin)				   ;cm-5
 dem_max       =max(dem_te,itemp)			   
 te_prof(it)   =10.^telog(itemp)			   ;T  [K]
 emlog_prof(it)=alog10(dem_tot)+2.*alog10(nbin*pix_cm)     ;log(EM [cm-3])
 demlog_bin    =alog10(dem_bin)+2.*alog10(nbin*pix_cm)     ;log(EM [cm-3] K-1)
 dem30	       =dem_bin/1.e30
 tew_prof(it)  =total(te*dem30)/total(dem30)

; DENSITY ADN THERMAL ENERGY____________________________
 ne_prof(it) =10.^(0.5*(emlog_prof[it]-vol_log[it]))
 ne_bin      =10.^(0.5*(demlog_bin    -vol_log[it]))
 eth_iso(it) =3.*kb*vol_prof(it)*ne_prof(it)*te_prof(it)
 eth_prof(it)=3.*kb*vol_prof(it)*total(ne_bin*te) 
 tiso_prof(it)=eth_prof(it)/(3.*kb*vol_prof(it)*ne_prof(it))
 print,'ETH = ',eth_iso(it),eth_prof(it)/eth_iso(it)

; TIME GAP CORRECTION__________________________________
 flux_tot=total(flux_prof[it,*])
 if (flux_tot le 0.) and (it ge 1) then begin
  flux_prof[it,*]=flux_prof[it-1,*]
  fit_prof[it,*] =fit_prof[it-1,*]
  chi_prof[it]   =chi_prof[it-1]
  rad_prof[it]   =rad_prof[it-1]
  area_prof[it]  =area_prof[it-1]
  vol_prof[it]   =vol_prof[it-1]
  emlog_prof[it] =emlog_prof[it-1]
  te_prof[it]    =te_prof[it-1]
  tew_prof[it]   =tew_prof[it-1]
  tiso_prof[it]  =tiso_prof[it-1]
  ne_prof[it]    =ne_prof[it-1]
  eth_prof[it]   =eth_prof[it-1]
  eth_iso[it]    =eth_iso[it-1]
 endif
endfor	;for it=0,nt-1 

;________________SAVE PRIMARY PARAMETERS________________________
pix	=pix_cm
save,filename=savefile,dateobs_,wave_,nt,time_t,texp_t,em_map,tiso_prof,$
    te_map,sig_map,emlog_prof,telog,dem_t,te_prof,tew_prof,ne_prof,$
    eth_prof,eth_iso,chi_prof,rad_prof,area_prof,vol_prof,flux_prof,$
    fit_prof,pix,nbin,em_thresh
print,'Parameters saved in file = ',savefile
end
