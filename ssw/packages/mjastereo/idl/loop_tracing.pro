pro loop_tracing,image_fov,index,fov,nzoom,ct,nsp,isc,x_pix,y_pix,x_sun,y_sun
;+
; Project	: Loop 3D geometry fitting
;
; Name		: LOOP_TRACING
;
; Category	: Data modeling
;
; Explanation   : This routine allows interactive tracing of loop points
;		  in a given IMAGE(NX,NY) with the FITS image header info
;		  contained in the structure INDEX. The INDEX needs to
;		  contain info of the pixels CRPIX, CRPIX2 of the Sun 
;		  center position in the image, the solar radius RSUN
;		  in arcseconds, and the pixel size CDELT1, CDELT2 in
;		  units of arcseconds. The output coordinate of the
;		  interactively chosen loop coordinates (typically 3-10 points)
;		  is stored in X_PIX,Y_PIX in units of image pixels, 
;		  or in X_SUN,Y_SUN in units of solar radii with respect
;		  to Sun center. The loop curve can be interpolated in 2D
;		  with 8 times higher resolution using SPLINE_P,X,Y,XX,YY
;
; Syntax:	: IDL> loop_tracing,image,index,fov,nzoom,ct,nsp,isc,$
;                      x_pix,y_pix,x_sun,y_sun
;
; Input:	: image_fov= subimage image[i1:i2,j1:j2] defined by FOV
;		  index = structure containing FITS header info
;		  fov   = [i1,j1,i2,j2] gives pixel ranges of display 
;	                  with (i1,j1) the bottom left corner,
;		          and  (i2,j2) the top right corner, or
;	                  (i1,i2) the x-axis range and (j1,j2) the y-axis range
;		  nzoom = zooming factor for enlargment (e.g., 1,2,3...)
;                 ct    = color table for display 
;		  nsp	= number of loop points to manually select 
;	          isc	=0 if both coordinates XSP and YSP are to be traced
;                       =1 if only coordinate XSP is to be traced,
;                          while YSP is given in input (e.g. for epipolar
;                          planes in STEREO/B, using YSP from STEREO/A.)
;		  
; Output:	: x_pix = array of x-coordinates in image pixel units
;		  y_pix = array of y-coordinates in image pixel units
;		  x_sun = array of x-coordinates in units of solar radii rel. to Sun center
;		  y_sun = array of y-coordinates in units of solar radii rel. to Sun center
;
; History:	: 2008-Dec-18, Version 1 written by Markus J. Aschwanden
;               ; 2009-Jan-22, modify output variables x_sun,y_sun
;               ; 2011-May-16, include all other instruments than SECCHI
;
; Contact:	: aschwanden@lmsal.com
;-

;EXTRACT CONSTANTS_______________________________________________________
crpix1	=index.crpix1		;pixel of Sun center
crpix2	=index.crpix2
cdelt1	=index.cdelt1		;pixel size in units of arcseconds
cdelt2	=index.cdelt2
if (index.instrume eq 'SECCHI') then rsun=index.rsun	;in units of arcseconds
if (index.instrume ne 'SECCHI') then begin
 dateobs=index.date_obs
 eph	=get_sun(dateobs)
 rsun	=eph(1)
 l0	=eph(10)
 b0	=eph(11)
endif
radpix1	=rsun/cdelt1		;solar radius in pixels
radpix2	=rsun/cdelt2
i1	=fov(0)
j1	=fov(1)
i2	=fov(2)
j2	=fov(3)
nx	=i2-i1+1
ny	=j2-j1+1
;image_fov=image(i1:i2,j1:j2)
n	=1000
phi	=2.*!pi*findgen(n)/float(n-1)
xlimb	=crpix1+radpix1*cos(phi)
ylimb	=crpix2+radpix2*sin(phi)

;DISPLAY IMAGE____________________________________________________________
window,0,xsize=nx*nzoom,ysize=ny*nzoom
x1_   =0.
x2_   =1.
y1_   =0.
y2_   =1.
loadct,ct
!p.position=[x1_,y1_,x2_,y2_]
!x.range=[i1,i2]      ;in units of image pixels 
!y.range=[j1,j2]
!x.style=1
!y.style=1
nxw  =long(!d.x_vsize*(x2_-x1_)+0.5)
nyw  =long(!d.y_vsize*(y2_-y1_)+0.5)
z    =congrid(image_fov,nxw,nyw)
plot,[0,0],[0,0]
statistic,image_fov,avg,dev
z1    =avg-dev
z2    =avg+dev
tv,bytscl(z,min=z1,max=z2),x1_,y1_,xsize=x2_-x1_,ysize=y2_-y1_,/normal
oplot,xlimb,ylimb
oplot,x_pix,y_pix,psym=1,symsize=2,color=128
if (x_pix(0) ne x_pix(nsp-1)) and (y_pix(0) ne y_pix(nsp-1)) then begin
 x_old	=x_pix
 y_old	=y_pix
 spline_p,x_old,y_old,xx,yy
 oplot,xx,yy		;diplays previous tracing
endif
!noeras=1

;TRACE LOOP___________________________________________________________
x_pix   =fltarr(nsp)
if (isc eq 0) then y_pix=fltarr(nsp)
print,'Number of loop positions   nsp=',nsp
dx	=(!x.range(1)-!x.range(0))/10.
for i=0,nsp-1 do begin
 print,'click cursor on spline point #'+string(i,'(i3)')
 if (isc eq 1) then begin
  oplot,x_old(i)*[1,1],y_old(i)*[1,1],psym=4,symsize=nzoom
  oplot,x_old(i)+dx*[-1,1],y_old(i)*[1,1]
 endif
 cursor,x,y
 x_pix(i)=x
 if (isc eq 0) then y_pix(i)=y
 oplot,x_pix(i)*[1,1],y_pix(i)*[1,1],psym=1,symsize=nzoom
 wait,0.2
 print,'X='+string(x,'(f8.1)')+' y='+string(y,'(f8.1)')
endfor

;UNITS OF SOLAR RADII__________________________________________________
x_sun	=(x_pix-crpix1)/radpix1
y_sun	=(y_pix-crpix2)/radpix2
print,'Distance from Sun center = ',minmax(sqrt(x_sun^2+y_sun^2)),' solar radii'

;LOOP INTERPOLATION____________________________________________________
oplot,x_pix,y_pix,psym=1,symsize=nzoom,thick=nzoom*3,color=0
oplot,x_pix,y_pix,psym=1,symsize=nzoom,thick=nzoom
spline_p,x_pix,y_pix,xspline,yspline
oplot,xspline,yspline,thick=nzoom
end


