pro euvi_response

dir	='/net/notung/Users/nitta/EUVI/response/'
restgenx,srea,file=dir+'ahead_sre_chianti1_feldman_mazzotta_000.geny' 
restgenx,sreb,file=dir+'behind_sre_chianti1_feldman_mazzotta_000.geny'

help,srea,sreb,/str
print,srea.name

gain	=15.
t_mk	=srea.temp/1e6
r1_a	=srea.elec(*,0)/gain
r2_a	=srea.elec(*,1)/gain
r3_a	=srea.elec(*,2)/gain
r4_a	=srea.elec(*,3)/gain
r1_b	=sreb.elec(*,0)/gain
r2_b	=sreb.elec(*,1)/gain
r3_b	=sreb.elec(*,2)/gain
r4_b	=sreb.elec(*,3)/gain

window,0,xsize=512,ysize=512
!x.title='Temperature  T [MK]'
!y.title='Photons/s [for EM=10!U44!N cm!U-3!N]'
plot_io,t_mk,r1_a,xsty=1,yty=1,xra=[0.3,3],thick=2
oplot,  t_mk,r2_a,linestyle=2,thick=2
oplot,  t_mk,r3_a,linestyle=1,thick=2
oplot,  t_mk,r4_a,linestyle=0,thick=1
oplot,  t_mk,r1_b,linestyle=0
oplot,  t_mk,r2_b,linestyle=2,thick=2
oplot,  t_mk,r3_b,linestyle=1,thick=2
oplot,  t_mk,r4_b,linestyle=0,thick=1

nt	=n_elements(t_mk)
respfile ='euvi_response.dat'
openw,2,respfile
for i=0,nt-1 do printf,2,t_mk(i),r1_a(i),r2_a(i),r3_a(i),r4_a(i),$
	         r1_b(i),r2_b(i),r3_b(i),r4_b(i),format='(9f10.3)'
close,2
print,'EUVI response in file = '+respfile

;FILTER RATIOS______________________________________________________
nt      =n_elements(t_mk)
dt      =shift(t_mk,-1)-t_mk
dt(nt-1)=dt(nt-2)
n_sig   =20
t_sig   =(findgen(n_sig)+1)/float(n_sig)
tmin	=0.7 ;MK
tmax	=3.0 ;MK

filterfile='euvi_filterratio.dat'
openw,2,filterfile
for idt=0,n_sig-1 do begin
 sigt   =t_sig(idt)
 q1     =fltarr(nt,2)
 q2     =fltarr(nt,2)
 for it=0,nt-1 do begin
  t0    =t_mk(it)
  em44_t=exp(-(t_mk-t0)^2/(2.*sigt^2))
  f1a   =total(em44_t*r1_a*dt)
  f2a   =total(em44_t*r2_a*dt)
  f3a   =total(em44_t*r3_a*dt)
  f1b   =total(em44_t*r1_b*dt)
  f2b   =total(em44_t*r2_b*dt)
  f3b   =total(em44_t*r3_b*dt)
  q1(it,0)=f2a/f1a
  q1(it,1)=f2b/f1b
  q2(it,0)=f3a/f2a
  q2(it,1)=f3b/f2b
  if (t0 ge tmin) and (t0 le tmax) and (sigt ge 0.2) then $
  printf,2,t0,sigt,q1(it,0),q2(it,0),q1(it,1),q2(it,1)
 endfor
endfor
close,2
print,'filter ratios in file = '+filterfile

end

