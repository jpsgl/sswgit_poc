pro vector_product,a,b,c,c_norm,angle

;calculates vector product c = a x b, with normalization c_norm=c/|c|

c	=fltarr(3)
c(0)	=a(1)*b(2)-a(2)*b(1)
c(1)	=a(2)*b(0)-a(0)*b(2)
c(2)	=a(0)*b(1)-a(1)*b(0)
c2	=total(c^2)
c_norm	=[0.,0.,0.]
angle 	=0.
if (c2 gt 0.) then begin
 c_abs	=sqrt(total(c2))
 c_norm	=c/c_abs
 a_abs	=sqrt(total(a^2))
 b_abs	=sqrt(total(b^2))
 sin_a	=c_abs/(a_abs*b_abs)
 angle	=asin((sin_a > (-1.))<1.)
 angle_sign=total(a*b)
;if (angle_sign lt 0.) then angle=-angle
 if (angle_sign lt 0.) then angle=!pi-angle	;CORRECTED 2012-JAN-30
endif
end
