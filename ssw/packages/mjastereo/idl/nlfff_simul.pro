pro nlfff_simul,dir,filename,vers,para
;+
; Project     : SOHO/MDI, SDO/HMI 
;
; Name        : MAGN_SIMUL
;
; Category    : magnetic field moedling
;
; Explanation : simulates nonlinear force-free magnetic field models
;
; Syntax      : IDL> nlfff_simul,dir,filename,para
;
; Inputs      : filename = *._coeff.dat, *_cube.sav, *_field.sav,   
;		para     = input parameters
;
; Outputs     ; saves parameters in file *_coeff.dat
;		--> m1,x1,y1,z1,a1		
;		saves parameters in file *_cube.sav:
;               --> x,y,z,bx,by,bz,b,a
;               saves parameters in file *_field.sav:
;               --> bzmap,x,y,field_lines,alpha,q_scale,cpu,merit
;
; History     : 28-Nov-2011, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

print,'__________INPUT PARAMETERS__________________________
nsmax   =long(para[7])
hmax    =para[12]
ds      =para[13]
fov_x1  =para[17]
fov_y1  =para[18]
fov_x2  =para[19]
fov_y2  =para[20]
stereo  =0  
dpix    =ds
rsun	=1./dpix
scale	=fov_x2
nx	=long(2*scale/dpix)
ny	=nx
x       =fov_x1+dpix*(findgen(nx)+0.5)
y       =fov_y1+dpix*(findgen(ny)+0.5)
xcenter =min(abs(x),crpix1)
ycenter =min(abs(y),crpix2)
mag     =1000.
inr	=long(strmid(filename,10,4))   
n	=((inr-1) mod 6)*2
if (inr eq 1) or (inr eq 7) then n=1
print,'Number of magnetic components = ',n

;______________________MAGNETIC INPUT PARAMETERS_______________'
b1_	=[ 1.00,-1.00, 0.90,-0.90, 0.80,-0.80, 0.70,-0.70, 0.60,-0.60]
x1_	=[ 0.05, 0.03, 0.00,-0.02,-0.08,-0.03, 0.03, 0.08, 0.06, 0.08]
y1_	=[ 0.00, 0.06,-0.01, 0.04,-0.07,-0.05,-0.07,-0.06, 0.08, 0.07]
r1_	=0.995
z1_	=sqrt(r1_^2-x1_^2-y1_^2)
a1_	=[-8.00,-8.00,-4.00,-4.00,+8.00,+8.00,-2.00,-2.00, 2.00, 2.00]*2
if (inr lt 7) then a1_=fltarr(10) 		;potential field
coeff	=[[b1_[0:n-1]],[x1_[0:n-1,0]],[y1_[0:n-1]],[z1_[0:n-1]],[a1_[0:n-1]]]
coeff(*,0)=coeff(*,0)*mag
help,coeff

;________________STORE SIMULATION PARAMETERS________________
coeff_file =dir+filename+'_coeff.dat'
coeff_model=dir+filename+'_coeff0.dat'
dim	=size(coeff)
nmag	=dim[1]
openw,2,coeff_file
openw,3,coeff_model
for is=0,nmag-1 do begin
 m1     =coeff(is,0)
 x1     =coeff(is,1)
 y1     =coeff(is,2)
 z1     =coeff(is,3)
 a1     =coeff(is,4)
 print   ,m1,x1,y1,z1,a1,format='(5f12.5)'
 printf,2,m1,x1,y1,z1,a1,format='(5f12.5)'
 printf,3,m1,x1,y1,z1,a1,format='(5f12.5)'
endfor
close,2
close,3
print,'Coefficient file written : ',coeff_model
print,'Coefficient file written : ',coeff_file

;________________SIMULATE MAGNETOGRAM_________________________
aia_mag_map,coeff,1.,x,y,bzmap

;________________CALCULATE FIELD LINES________________________
qloop   =0.5
ngrid	=5
nbound	=4
if (inr eq 1) then ngrid=10
nxx	=long(nx/ngrid-nbound+1)
nyy	=long(ny/ngrid-nbound+1)
nf	=nxx*nyy
print,'Number of field simulated lines = ',nf
np_	=lonarr(nf)
field_lines=fltarr(nsmax,5,nf)
k	=0
for i=0,nxx-1 do begin
for j=0,nyy-1 do begin
 xm     =x[(i+nbound/2)*ngrid]
 ym     =y[(j+nbound/2)*ngrid]
 zm    =sqrt(1.-xm^2-ym^2)
 nlfff_fieldline,xm,ym,zm,ds,hmax,coeff,field,ns,nsmax
 imm	=long(qloop*(ns-1))
 xmm	=field(imm,0)
 ymm	=field(imm,1)
 zmm    =field(imm,2)
 nlfff_fieldline,xmm,ymm,zmm,ds,hmax,coeff,field,ns,nsmax
 ns     =ns < nsmax
 np_[k] =ns
 field_lines(0:ns-1,0,k)=field(0:ns-1,0)
 field_lines(0:ns-1,1,k)=field(0:ns-1,1)
 field_lines(0:ns-1,2,k)=field(0:ns-1,2)
 k	=k+1
endfor
endfor

;________________SAVE LOOP COORDINATES________________________
loopfile=dir+filename+'_loop_'+vers+'.dat'
zero	=0.
stereo	=0
openw,1,loopfile
for k=0,nf-1 do begin
 for is=0,np_[k]-1 do begin
  xl	=field_lines(is,0,k)
  yl	=field_lines(is,1,k)
  zl	=field_lines(is,2,k)
  flux	=1000./np_[k]
  if (stereo eq 0) then  printf,1,k,xl,yl,flux,zero
  if (stereo eq 1) then  printf,1,k,xl,yl,zl,zero
 endfor
endfor
close,1
help,field_lines
print,'loopfile written = ',loopfile

;________________MAGNETIC MAP__________________________________
magfile =dir+filename+'_magn.fits'
writefits,magfile,bzmap
image=readfits(magfile,header)
sxaddpar,header,'DATE_OBS',filename
sxaddpar,header,'NAXIS',2
sxaddpar,header,'NAXIS1',nx
sxaddpar,header,'NAXIS2',ny
sxaddpar,header,'CRPIX1',crpix1
sxaddpar,header,'CRPIX2',crpix2
sxaddpar,header,'CDELT1',dpix
sxaddpar,header,'CDELT2',dpix
sxaddpar,header,'SOLAR_P0',0
sxaddpar,header,'R_SUN',rsun
writefits,magfile,bzmap,header
print,'Magnetic field file written = ',magfile
end
