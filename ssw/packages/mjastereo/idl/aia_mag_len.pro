pro aia_mag_len,x,y,z,ns,slen,s
;+
; Project     : AIA/SDO
;
; Name        : AIA_MAG_LEN
;
; Category    : Data analysis   
;
; Explanation : calculates length of a curved array in 3D
;               from midpoint position [xm,ym,zm] to both footpoints
;               [xf1,yf1,0] and [xf2,yf2,0]
;
; Syntax      : IDL>aia_loop_fieldline,xm,ym,zm,np,hmax,coeff,curr,field
;
; Inputs      : x       = x-coordinate of loop midpoint (solar radii)
;               y       = y-coordinate of loop midpoint 
;               z       = z-coordinate of loop midpoint 
;
; Outputs     : ns
;		slen
;
; History     : 1-Jun-2011, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

ns	=n_elements(x)
slen	=0.
s	=fltarr(ns)
for is=1,ns-1 do $
 s(is)=s(is-1)+sqrt((x[is]-x[is-1])^2+(y[is]-y[is-1])^2+(z[is]-z[is-1])^2)
slen	=s(ns-1)
end

