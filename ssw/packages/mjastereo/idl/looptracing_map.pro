pro looptracing_map,nx,ny,wid_gauss,datfile,image

;creates a map of loops by convolving the loop coordinates with a
;2D gaussian kernel function (with gaussian width WID_GAUSS)

image	=fltarr(nx,ny)
readcol,datfile,iloop_,xloop_,yloop_,zloop_,sloop_
iloop1  =min(iloop_)
iloop2  =max(iloop_)
if (iloop2 le 100) then size=1.0
if (iloop2 gt 100) then size=0.7
nloop	=0
for iloop=iloop1,iloop2 do begin
 ind    =where(iloop_ eq iloop,np)
 if (np ge 1) then begin
  iloop =iloop_(ind(0))
  xp    =xloop_(ind)
  yp    =yloop_(ind)
  zp    =zloop_(ind)
  spline_p,xloop_(ind),yloop_(ind),xx,yy
  sp	=fltarr(np)
  for ip=1,np-1 do sp(ip)=sp(ip-1)+sqrt((xp(ip)-xp(ip-1))^2+(yp(ip)-yp(ip-1))^2)
  nn    =n_elements(xx)
  ss	 =fltarr(nn)
  for ii=1,nn-1 do ss(ii)=ss(ii-1)+sqrt((xx(ii)-xx(ii-1))^2+(yy(ii)-yy(ii-1))^2)
  zz	=interpol(zp,sp,ss) 
  for ii=0,nn-1 do begin
   i0	=long(xx(ii)+0.5)
   j0	=long(yy(ii)+0.5)
   ix	=i0-5+findgen(11)
   for ja=j0-5,j0+5 do begin
    ind0=where((ix ge 0) and (ix le nx-1) and (j0 ge 5) and (j0 le ny-6),nind)
    if (nind ge 1) then image(ix(ind0),ja)=$
     image(ix(ind0),ja) > zz(ii)*exp(-((ix(ind0)-i0)^2+(ja-j0)^2)/(2.*wid_gauss^2))
   endfor
  endfor
  nloop =nloop+1
 endif
endfor
end
