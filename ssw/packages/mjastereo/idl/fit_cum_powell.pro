function fit_cum_powell,coeff

common powerlaw_var,xh_,yh_,ind_fit_,yfit_,x0_,noise_,dev_

n0	=10.^coeff(0)
a	=coeff(1) 
nmax	=max(yh_)
ind_fit_=where((yh_ ge 1.) and (xh_ ge x0_),nf)
ncoeff	=n_elements(coeff)
nh	=n_elements(xh_)
x1	=xh_(0)
x2	=xh_(nh-1)
yfit_	=1.+(n0-1.)*((x2+x0_)^(1.-a)-(xh_+x0_)^(1.-a))/((x2+x0_)^(1.-a)-(x0_+x0_)^(1.-a))
noise_  =sqrt(yh_)
obs     =yh_(ind_fit_)
fit     =yfit_(ind_fit_)
err     =noise_(ind_fit_)
dev_	=fltarr(nh)
dev_(ind_fit_)=(fit-obs)/err
w       =fltarr(nf)+1.             ;weights
chi2    =total((w*(fit-obs)/err)^2)/total(w)
chi     =sqrt(chi2*float(nf)/float(nf-ncoeff))
return,chi
end
