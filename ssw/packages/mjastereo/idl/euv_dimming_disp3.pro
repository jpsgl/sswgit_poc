pro euv_dimming_disp3,input,dir,catfile,run,iflare,io,vers,model
;+
; Project     : SDO/AIA,  
;
; Name        : EUV_DIMMING_DISP 
;
; Category    : data analysis of EUV dimming and CME mass, speed, and energy
;
; Explanation : Reads AIA images and performs EUV dimming analysis
;
; Syntax      : IDL>euv_dimming_disp,input,catfile,dir,iflare
;
; Inputs      : dir,savefile
;
; Outputs     ; IDL save file: *.sav
;
; History     : 24-Feb-2016, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

;________________READ PARAMETERS______________________________
string0,3,iflare,nr_str
savefile2='dimming_'+nr_str+run+'.sav'
restore,dir+savefile2	
print,'read parameters from = ',dir+savefile2
;save,filename=dir+savefile2,input,para,exptime_,flux_grid,x_grid,y_grid,$
;        flux_prof,emtot_prof,telog_prof,tsig_prof,chi_prof,wave_,time,$
;        x_lasco,angle,area,vol,em_tot,dem_t,nel,te_mk,telog,mass,time,$
;        v_lasco,t_lasco,a_model,v_model,x_model,x_model_grid,ekin_model
tmargin1=input.tmargin1                 ;s
tmargin2=input.tmargin2
rise    =input.rise
decay   =input.decay
duration=rise+decay
cadence =input.cadence
it_start=long(tmargin1/cadence)
it_peak=long((tmargin1+rise)/cadence)
it_end =long((tmargin1+rise+decay)/cadence)
tstart_hr=time(it_start)
tpeak_hr=time(it_peak)
tend_hr=time(it_end)

readcol,'lasco.dat',flare,dateobs_lasco,h_lasco,v1_lasco,v2_lasco,v3_lasco,$
	m_lasco,e_lasco,c,skipline=1,format='(I,A,F,I,I,I,F,F,A)'
ind0	=where(flare eq iflare,n)
i	=ind0(0)
vv_lasco=[v1_lasco(i),v2_lasco(i),v3_lasco(i)]
ind3	=where(vv_lasco ne 0)
statistic,vv_lasco(ind3),vlasco_avg,vlasco_sig
hh_lasco=strmid(dateobs_lasco(i),11,2)
mm_lasco=strmid(dateobs_lasco(i),14,2)
ss_lasco=strmid(dateobs_lasco(i),17,2)
t_hrs_lasco=hh_lasco+mm_lasco/60.+ss_lasco/3600.
if (t_hrs_lasco lt min(time)) then t_hrs_lasco=t_hrs_lasco+24.
if (t_hrs_lasco ge max(time)) then t_hrs_lasco=t_hrs_lasco-24.
mass_lasco=m_lasco(i)
ekin_lasco=e_lasco(i)
onset_lasco=strmid(dateobs_lasco(i),11,8)
comment	=c(i)

;_________________DISPLAY________________________________________
form    =1      ;0=landscape, 1=portrait
char	=1.
char2	=1.2
if (io ne 0) then char=0.8    ;character size
plotname='dimming_'
ref     =''     ;label at bottom of Fig.
unit    =0      ;window number
ct	=5
nt	=n_elements(time)
nwave	=n_elements(wave_)
rsun	=6.96e10 ;cm
string0,3,iflare,fig_str
fignr	=fig_str+vers

;________________________AIA WAVELENGTH FLUXES_________________
fig_open,io,form,char,fignr,plotname,unit
loadct,ct
x1_	=0.1
x2_	=0.5
y2_	=0.96
y1_	=0.80
!p.position=[x1_,y1_,x2_,y2_]
!p.title=' '
ymax0=max(flux_prof)
!x.range=minmax(time)			;FOV coordinates
!y.range=[0,ymax0]
!x.style=1
!y.style=0
!x.title=''
!y.title='AIA flux [DN/s]'
plot,time,fltarr(nt)
xyouts,tpeak_hr-0.01,0,' Flare peak',size=char,orient=90
xyouts,tstart_hr-0.01,0,' Flare start',size=char,orient=90
xyouts,tend_hr-0.01,0,' Flare end',size=char,orient=90
color	=[50,75,100,125,150,175,200]
for iwave=0,nwave-1 do begin
 wave=wave_(iwave)
 ymax=max(flux_prof(*,iwave))
 col	=color(iwave)
 factor	=1.0
 if (ymax lt ymax0/10.) then factor=10.0
 oplot,time,flux_prof(*,iwave)*factor,color=col
 oplot,tstart_hr*[1,1],!y.crange
 oplot,tpeak_hr*[1,1],!y.crange
 oplot,tend_hr*[1,1],!y.crange
 str=string(wave_(iwave),'(I4)')+' A x'+string(factor,'(I2)')
 xyouts_norm,0.7,0.9-0.1*iwave,str,char,0,col
 !noeras=1
endfor

;________________________ACCELERATION_________________
y2_	=0.77
y1_	=0.61
!p.position=[x1_,y1_,x2_,y2_]
!y.range=[0,max(a_model)*1.2] 
plot,time,a_model,ytitle='a [cm/s!U2!N]'
oplot,time,a_model
oplot,(t_lasco/3600.)*[1,1],!y.crange,color=128
v_str='a!Dmax!N = '+string(max(a_model),'(E7.1)')+' cm/s!U2!N'
xyouts_norm,0.02,0.9,v_str,char,0,128
t_lasco_hrs=(t_lasco/3600. mod 24)
hour    =long(t_lasco_hrs)
minute  =(t_lasco_hrs-hour)*60.
string0,2,hour,hh
string0,2,minute,mm
xa	=t_lasco/3600.
xyouts,xa-0.01,0,' LASCO/C2',size=char,orient=90,color=128

;________________________VELOCITY_________________
y2_	=0.58
y1_	=0.42
!p.position=[x1_,y1_,x2_,y2_]
!p.title=' '
!x.title=' '
!y.range=[0,max(v_model*1.2/1.e5) > vlasco_avg]
plot,time,v_model/1.e5,ytitle='v [km/s]'
oplot,t_lasco/3600.*[1,1],!y.crange,color=128
oplot,!x.crange,[1,1]*v_lasco/1.e5,color=128
oplot,time,v_model/1.e5
v_str='v!DLASCO!N = '+string(v_lasco/1.e5,'(I6)')+' km/s'
xyouts_norm,0.02,0.9,v_str,char,0,128
ya	=!y.crange(0)
xyouts,xa-0.01,0,' LASCO/C2',size=char,orient=90,color=128
oplot,t_hrs_lasco(0)*[1,1],vlasco_avg+vlasco_sig*[-1,1],thick=5,color=128
oplot,t_hrs_lasco(0)*[1,1],vlasco_avg*[-1,1],psym=4,thick=5,color=128
xyouts,t_hrs_lasco(0),vlasco_avg,' LASCO Detection',color=128

;________________________DISTANCE_________________
y2_	=0.39
y1_	=0.23
!p.position=[x1_,y1_,x2_,y2_]
!y.range=[0,max(x_model/rsun)*1.2 > h_lasco(0)]
plot,time,x_model/rsun,ytitle='x [R!Dsun!N]'
oplot,(t_lasco/3600.)*[1,1],!y.crange,color=128
oplot,!x.crange,[1,1]*x_lasco/rsun,color=128
r0	=6.96e10 ;cm
v_str='x!DLASCO!N = '+string(x_lasco/r0,'(F4.2)')+' R!Dsun!N'
xyouts_norm,0.02,0.9,v_str,char,0,128
xyouts,xa-0.01,0,' LASCO/C2',size=char,orient=90,color=128
oplot,t_hrs_lasco(0)*[1,1],h_lasco(0)+0.1*[-1,1],thick=5,color=128
oplot,t_hrs_lasco(0)*[1,1],h_lasco(0)*[1,1],psym=4,thick=5,color=128
xyouts,t_hrs_lasco(0),h_lasco(0),' LASCO Detection',color=128
oplot,!x.crange,(d0/r0)*[1,1],linestyle=2

;________________________EMISSION MEASURE______________
y2_	=0.20
y1_	=0.04
!p.position=[x1_,y1_,x2_,y2_]
!x.title='Time  t[hrs]'
!y.range=[0,max(emtot_prof)*1.2]
plot,time,emtot_prof,ytitle='EM [cm!U-3!N]'
oplot,time,emtot_prof
qnorm=max(emtot_prof)/max(em_model)
oplot,time,em_model*qnorm,psym=10	
oplot,(t_lasco/3600.)*[1,1],!y.crange,color=128
v_str='EM!Dmax!N = '+string(max(emtot_prof),'(E7.1)')+' erg'
xyouts_norm,0.02,0.9,v_str,char,0,128
xyouts,xa-0.01,0,' LASCO/C2',size=char,orient=90,color=128

;________________________DEM_____________________________
x1_	=0.60
x2_	=0.95
y2_	=0.96
y1_	=0.61
!p.position=[x1_,y1_,x2_,y2_]
demlog=alog10(dem_t)
plot,telog,demlog(*,0),xrange=minmax(telog),yrange=minmax(demlog),$
     xtitle='log(T)',ytitle='log(EM [cm!U-5!N K!U-1!N])'
for it=1,nt do oplot,telog,demlog(*,it-1)
telog_med=median(telog_prof)
oplot,telog_med*[1,1],!y.crange

;________________________PARAMETERS________________________
!p.position=[0.6,0.05,0.95,0.55]
!x.range=[0,1]
!y.range=[-0.5,2.0]
!x.style=5
!y.style=5
plot,[0,0],[0,0]
xyouts,0.0,2.0,'Flare # '+nr_str,size=char2
xyouts,0.0,1.9,'Version = '+vers,size=char2
xyouts,0.0,1.8,strmid(input.tstart,0,20)+' UT',size=char2
xyouts,0.0,1.7,'GOES = '+input.goes,size=char2
xyouts,0.0,1.6,input.helpos,size=char2
xyouts,0.0,1.5,'FOV ='+string(input.fov0,'(F4.2)')+' R!Dsun!N',size=char2
xyouts,0.0,1.4,'NBIN='+string(input.nbin,'(I4)'),size=char2
xyouts,0.0,1.3,'L='+string(l_cm/1.e8,'(I4)')+' Mm, ('+$
       string(angle,'(I3)')+'!U0!N)',size=char2
xyouts,0.0,1.2,'V='+string(vol,'(E7.1)')+' cm!U3!N',size=char2
xyouts,0.0,1.1,'EM='+string(em_tot,'(E7.1)')+' cm!U-3!N',size=char2
xyouts,0.0,1.0,'n!De!N='+string(nel,'(E7.1)')+' cm!U-3!N',size=char2
xyouts,0.0,0.9,'T='+string(te_mk,'(F5.2)')+' MK',size=char2
xyouts,0.0,0.8,'dt='+string(time(1)-time(0),'(F5.2)')+' hrs',size=char2
xyouts,0.0,0.7,'m='+string(mass,'(E7.1)')+' g [AIA]',size=char2
xyouts,-0.0,0.6,'m='+string(mass_lasco,'(E7.1)')+' g [LASCO]',size=char2,color=128
statistic,x_lasco,x_avg,x_sig
xyouts,0.0,0.5,'x!DLASCO!N='+string(x_avg/rsun,'(F5.2)'),size=char2
statistic,v_lasco,v_avg,v_sig
xyouts,0.0,0.4,'v!DLASCO!N='+string(v_avg/1.e5,'(I5)')+'_',size=char2
xyouts,0.0,0.4,'v!DLASCO!N='+string(v_avg/1.e5,'(I5)')+'+'+$
	string(v_sig/1.e5,'(I5)')+' km/s',size=char2
xyouts,0.0,0.3,'v!DLASCO!N='+string(vlasco_avg,'(I5)')+'_',size=char2,color=128
xyouts,0.0,0.3,'v!DLASCO!N='+string(vlasco_avg,'(I5)')+'+'+$
	string(vlasco_sig,'(I5)')+' km/s',size=char2,color=128

;________________________DIMMING START TIME__________________________
t0_onset=(tstart_hr/3600. mod 24)
hour	=long(t0_onset)
minute	=long((t0_onset-hour)*60.)
string0,2,hour,hh
string0,2,minute,mm
str	='t!Ddimm!N = '+hh+':'+mm+' UT, '
xyouts,0.0,0.2,str,size=char2

;______________________LASCO ONSET TIME______________________________
t_lasco_hrs=(t_lasco/3600. mod 24)
hour	=long(t_lasco_hrs)
minute	=(t_lasco_hrs-hour)*60.
string0,2,hour,hh
string0,2,minute,mm
str	='t!DLASCO!N = '+hh+':'+mm+' UT, '
xyouts,0.0,0.1,str,size=char2
xyouts,0.0,0.0,'t!DLASCO!N = '+onset_lasco,size=char2,color=128

;______________________KINETIC ENERGY_________________________________
statistic,ekin_model/1.e30,ekin_avg,ekin_sig
xyouts,0.0,-0.1,'E!Dkin!N='+string(ekin_avg,'(F7.2)')+'+',size=char2
xyouts,0.0,-0.1,'E!Dkin!N='+string(ekin_avg,'(F7.2)')+'_'+string(ekin_sig,'(F7.2)')+' 10!U30!N erg',size=char2
xyouts,0.0,-0.2,'E!Dkin!N='+string(ekin_lasco/1.e30,'(F7.2)')+$
	' 10!U30!N erg',size=char2,color=128

xyouts,0.0,-0.4,'LASCO/C2 event: '+comment,size=char2
xyouts,0.0,-0.5,'v-ratio='+string((v_avg/1.e5)/vlasco_avg,'(f5.2)'),size=char2
fig_close,io,fignr,ref
end
