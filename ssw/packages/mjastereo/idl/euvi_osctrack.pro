pro euvi_osctrack,fov_file,fov,index,ct,iseq,vect,coeff,fignrs,nrebin,plot

restore,fov_file ;-->images_fov(nx,ny,nt)
t	=anytim(index.date_obs,/sec)
dim	=size(images_fov)
nx	=dim(1)
ny	=dim(2)
nt	=dim(3)
nseq	=n_elements(iseq)
nrow	=long(sqrt(nseq-1)+0.99)
form    =1      ;0=landscape, 1=portrait
char    =1.0    ;character size
ref     =''	;label at bottom of Fig.
nsm	=1
inv	=1
i1	=fov(0)
j1	=fov(1)
i2	=fov(2)
j2	=fov(3)
ii1	=plot(0)
jj1	=plot(1)
ii2	=plot(2)
jj2	=plot(3)
xcen	=index(0).crpix1
ycen	=index(0).crpix2
cdelt1	=index(0).cdelt1
rsun	=index(0).rsun
x0	=xcen
y0	=ycen
r0	=rsun/cdelt1
n	=1000
phi	=2.*!pi*findgen(n)/float(n-1)
xlimb	=x0+r0*cos(phi)
ylimb	=y0+r0*sin(phi)
v	=float(vect)+[i1,j1,i1,j1]
l_km	=sqrt((v(0)-v(2))^2+(v(1)-v(3))^2)*cdelt1*725.
tstart	=t(iseq(1))-t(0)
tend	=t(max(iseq))-t(0)
cad	=t(1)-t(0)
nt	=long((tend-tstart)/cad+1)    ;timestep=cadence
nh	=long(l_km/1000.)             ;height increment=1 Mm
h_slice =findgen(nh)		       ;Mm
t_slice =tstart+cad*findgen(nt)
slice	=fltarr(nt,nh)     
print,'cadence=',cad
nxx	=128
nyy	=128
flux_med =median(images_fov(ii1:ii2,jj1:jj2,*))

if (ct eq 0) then io1=1
if (ct ge 1) then io1=3
for io=0,io1,io1 do begin
 fignr    =fignrs(0)
 unit    =0      ;window number
 plotname='f'
 fig_open,io,form,char,fignr,plotname,unit
 loadct,ct
 image_t0=images_fov(ii1:ii2,jj1:jj2,0)
 for i=1,nseq-1 do begin
  it	=iseq(i)
  image =images_fov(ii1:ii2,jj1:jj2,it)
  image0=images_fov(ii1:ii2,jj1:jj2,it-1)
  diff	=image-image_t0
  runn  =smooth(image,3)-smooth(image0,3)

  icol	=((i-1) mod nrow)
  irow	=long((i-1)/nrow)
  dx	=0.9/float(nrow)
  dy	=0.7/float(nrow)
  x1_	=0.05+dx*icol
  x2_	=x1_+dx*0.95
  y2_	=0.9-dy*irow
  y1_	=y2_-dy*0.95
  !p.position=[x1_,y1_,x2_,y2_]
  !x.range=[i1+ii1,i1+ii2]
  !y.range=[j1+jj1,j1+jj2]
  !x.style=5
  !y.style=5
  dim	=size(image)
  nxx	=dim(1)/nrebin
  nyy	=dim(2)/nrebin
  runn_	=rebin(runn(0:nrebin*nxx-1,0:nrebin*nyy-1),nxx,nyy)
  if (io eq 0) then begin
   nxw  =long(!d.x_vsize*(x2_-x1_)+0.5)
   nyw  =long(!d.y_vsize*(y2_-y1_)+0.5)
   z    =congrid(runn_,nxw,nyw)
  endif
  if (io ne 0) then z=runn_
  plot,[0,0],[0,0]
  d1	=-flux_med*0.05
  d2	=+flux_med*0.05
  tv,bytscl(z,min=d1,max=d2),x1_,y1_,xsize=x2_-x1_,ysize=y2_-y1_,/normal
  oplot,[v(0),v(2)]+ii1,[v(1),v(3)]+jj1,thick=8,color=0
  oplot,[v(0),v(2)]+ii1,[v(1),v(3)]+jj1,thick=4,color=255
  xyouts_norm,0.02,0.02,strmid(index(it).date_obs,11,8),char*1.4
  !noeras=1

;height-time plot
  t_diff =t(iseq(i))-t(0)
  dtmin	=min(abs(t_slice-t_diff),itnext)
  for j=0,nh-1 do begin
   h_km	=float(j)*1.e3
   i_pix =long(vect(0)+(vect(2)-vect(0))*h_km/l_km+0.5)
   j_pix =long(vect(1)+(vect(3)-vect(1))*h_km/l_km+0.5)
   slice(itnext,j)=diff(i_pix,j_pix)
  endfor
  endloop:
 endfor
 fig_close,io,fignr,ref
endfor

for io=0,io1,io1 do begin
fignr   =fignrs(1);figure number
unit    =1      ;window number
plotname='f'
fig_open,io,form,char,fignr,plotname,unit
flux='Flux [DN/s]'
ih_max	=lonarr(nt)
amp_km	=fltarr(nt)
for i=0,nt-1 do begin
 if (slice(i,0) eq 0) then slice(i,*)=slice(i+1,*)
 slice(i,*)=smooth(slice(i,*),3)
 ymax	=max(slice(i,*),im)
 ih_max(i)=im
 amp_km(i)=h_slice(im)
endfor
err_km	=(h_slice(1)-h_slice(0))/2.

;fit of damped oscillation
common euvi_osc_powell_common,x,y,y_model 
x	=t_slice
y	=amp_km
statistic,y,a0,a1
p	=(t_slice(1)-t_slice(0))*4.
tau	=(max(t_slice)-min(t_slice))/2.
t0	=t_slice(0)
phi	=0.
coeff  =[a0,a1,p,tau,t0,phi]
npar   =n_elements(coeff)
xi     =fltarr(npar,npar)
for i=0,npar-1 do xi(i,i)=1.
ftol   =1.e-4
powell,coeff,xi,ftol,fmin,'euvi_osc_powell'
n	=nt*10
x	=t_slice(0)+(t_slice(nt-1)-t_slice(0))*findgen(n)/float(n-1)
dev	=euvi_osc_powell(coeff) ;-->x,y_model

x1_	=0.10
x2_	=0.45
y1_	=0.05
y2_	=0.50
if (io eq 0) then begin
 nxw  =long(!d.x_vsize*(x2_-x1_)+0.5)
 nyw  =long(!d.y_vsize*(y2_-y1_)+0.5)
 z    =congrid(slice,nxw,nyw)
endif
if (io ne 0) then z=slice
statistic,slice,avg,dev
z1	=avg-dev
z2	=avg+dev 
!p.position=[x1_,y1_,x2_,y2_]
!x.style=1
!y.style=1
!x.range=minmax(t_slice)+[-0.5,0.5]*cad
!y.range=minmax(h_slice)
!x.title='Time t [s]'
!y.title='Amplitude  a [Mm]'
plot,[0,0],[0,0]
z1	=median(z)
z2	=max(z)
tv,bytscl(z,min=z1,max=z2),x1_,y1_,xsize=x2_-x1_,ysize=y2_-y1_,/normal
oplot,t_slice,amp_km,psym=4,color=0
for i=0,nt-1 do oplot,t_slice(i)*[1,1],amp_km(i)+err_km*[-1,1],color=0
oplot,x,y_model,color=0,thick=3
!noeras=1

x1_	=0.55
x2_	=0.90
offset	=(max(slice)-min(slice))/3.
!p.position=[x1_,y1_,x2_,y2_]
!x.range=minmax(h_slice)
!y.range=[-1,nseq+1]*offset
!y.title=flux
!x.title='Amplitude  a [Mm]'
plot,[0,0],[0,0]
for i=0,nt-1 do begin
 x	=h_slice
 y	=reform(slice(i,*))-1.
 oplot,x,y+(i*offset)
 im	=ih_max(i)
 oplot,x(im)*[1,1],y(im)*[1,1]+(i*offset),psym=4
 if (i ge 1) then begin
  im0	=ih_max(i-1)
  oplot,[xold,x(im)],[yold,y(im)]+[i-1,i]*offset,thick=2
 endif
 xold	=x(im)
 yold	=y(im)
endfor
fig_close,io,fignr,ref
endfor ;for io-0,3,3 
end
