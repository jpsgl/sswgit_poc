pro nlfff_field,dir,savefile,test
;+
; Project     : SOHO/MDI, SDO/HMI, STEREO
;
; Name        : NLFFF_FIELD
;
; Category    : Magnetic data modeling
;
; Explanation : Calculating NF magnetic field lines FIELD_LINES[NS,5,NF] 
;		magnetic field coefficients [B1,X1,Y1,Z1,A1] 
;
; Syntax      : IDL>nlfff_field,dir,savefile,test
;
; Inputs      : dir,savefile,test
;
; Outputs     ; updates savefile
;
; History     : 16-May-2011, Version 1 written by Markus J. Aschwanden
;               30-Jan-2012, Version 2 released to SSW
;               29-JAN-2015, im --> np/2 (footpoint --> midpoint)
;
; Contact     : aschwanden@lmsal.com
;-


print,'__________________NLFFF_FIELD_________________________________'
restore,dir+savefile    ;--> PARA
fov0    =input.fov0      ;field-of-view
filename=strmid(savefile,0,15)
x1_sun  =para.x1_sun
x2_sun  =para.x2_sun
y1_sun  =para.y1_sun
y2_sun  =para.y2_sun
hmax    =0.05     		;maximum altitude (solar radii)
nseg    =9			;number of loop segments (misalignment angles)
ds      =0.002        		;spatial resolution along loop
nloop   =10000        		;loop array limit
nsmax   =long(fov0/ds)  
dim     =size(bzmap)
nx      =dim(1)
ny      =dim(2)
dpix    =float(x2_sun-x1_sun)/float(nx-1)
x       =x1_sun+dpix*findgen(nx)
y       =y1_sun+dpix*findgen(ny)
nf	=n_elements(ns_loop)

;______________________ALL FIELD LINES____________________________________'
dev_loop=fltarr(nf)
for k=0,nf-1 do begin
 ifit	=k*nseg+findgen(nseg)
 dev_loop(k)=median(dev_deg(ifit))
endfor
field_lines=fltarr(nsmax,3,nf)
ns_field=lonarr(nf)

;____________MAGNETIC FIELD LINES THROUGH LOOP MIDPOINT_________________
nf_sel	=nf
isort	=sort(dev_loop)
k_sel	=isort(0:nf_sel-1)
for k_sel=0,nf_sel-1 do begin
 k	=isort(k_sel)
 if ((k_sel mod 100) eq 0) or (k_sel eq nf-1) then print,'field line #',k_sel
 ns	=ns_loop[k]
 ind	=where(field_loop(*,0,k) ne 0,ns)
;im	=0 					;loop footpoint
 im	=ns_loop(k)/2 				;loop midpoint
 xm	=field_loop(im,0,k)
 ym	=field_loop(im,1,k)
 zm	=field_loop(im,2,k)
 rm	=sqrt(xm^2+ym^2+zm^2)
 nlfff_fieldline,xm,ym,zm,ds,hmax,coeff,field,np,nsmax
 np	=np < nsmax
 field_lines(0:np-1,0,k)=field(0:np-1,0)
 field_lines(0:np-1,1,k)=field(0:np-1,1)
 field_lines(0:np-1,2,k)=field(0:np-1,2)
 ns_field[k]=np
endfor

;_______________________SAVE PARAMETERS________________________
save,filename=dir+savefile,input,para,bzmap,bzfull,bzmodel,coeff,$
     field_loop_det,ns_loop_det,wave_loop_det,$
     field_loop,ns_loop,wave_loop,dev_deg,dev_deg2,$
     field_lines,ns_loop,ns_field 
print,'parameters saved in file = ',savefile
end
