pro nlfff_fov,dir,savefile,test
;+
; Project     : SDO, STEREO, SOHO, AIA
;
; Name        : NLFFF_FOV
;
; Category    : imaging data analysis
;
; Explanation : displays selected field-of-view
;
; Syntax      : IDL>nlfff_fov,dir,savefile,test
;
; Inputs      : dir	 =directory 
;		savefile =output savefile
;               test	 =0, 1, or 2 (display test graphics)
;
; Outputs     ; 
;
; History     : 17-Jan-2014, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

print,'__________________NLFFF_FOV____________________________'
restore,dir+savefile    ;--> INPUT,PARA
filename=strmid(savefile,0,15)
wave_	=para.wave_
instr   =input.instr
x1_sun	=para.x1_sun
x2_sun	=para.x2_sun
y1_sun	=para.y1_sun
y2_sun	=para.y2_sun

;'__________________CHECK EXISTENCE OF FILE_________________'
iw	=0
search	=dir+filename+'_'+instr+'_'+wave_(iw)+'.fits'
file    =findfile(search,count=count)
print,'Search file = ',search
if (count eq 0) then begin
 print,'No file found !'
 goto,skip
endif
if (count eq 1) then begin 		
 print,'File already exists : ',file
 data =readfits(file,header) 
 index=fitshead2struct(header)
endif

;_______________________EXTRACT HEADER______________________
 naxis1	=index.naxis1
 naxis2	=index.naxis2
 cdelt1	=index.cdelt1
 cdelt2	=index.cdelt2
 crval1 =index.crval1
 crval2 =index.crval2
 crpix1 =index.crpix1
 crpix2 =index.crpix2
 dateobs=index.date_obs
 eph    =get_sun(dateobs)
 rsun   =eph(1)                 ;rsun=index.rsun_obs in arcseconds
 blat	=eph(11)		&print,'solar latitude B0=',blat
 rpix   =rsun/cdelt1            ;solar radius in number of pixels
 dpix_euv=1./rpix               ;pixel size in units of solar radius
 i0_sun =long(crpix1-crval1/cdelt1)
 j0_sun =long(crpix2-crval2/cdelt2)
 nx     =long(1+(x2_sun-x1_sun)/dpix_euv)
 ny     =long(1+(y2_sun-y1_sun)/dpix_euv)
 image1 =fltarr(nx,ny)
 i1     =long(i0_sun+x1_sun/dpix_euv)
 i2     =long(i0_sun+x2_sun/dpix_euv)
 j1     =long(j0_sun+y1_sun/dpix_euv)
 j2     =long(j0_sun+y2_sun/dpix_euv)
 for j=j1,j2 do begin
  if (j ge 0) and (j le naxis2-1) and (j-j1 le ny-1) then begin
   ii1  =(i1 > 0) < (naxis1-1)
   ii2  =(i2 > 0) < (naxis1-1)
   nnx  =ii2-ii1+1
   ii3  =(ii1-i1)>0
   ii4  =ii3+nnx-1
   if (ii2-1 gt ii1) then $		
   image1(ii3:ii4-1,j-j1)=float(data(ii1:ii2-1,j))
  endif
 endfor
 for j=1,ny-1 do begin
  max1=max(image1(*,j-1))
  max2=max(image1(*,j))
  if (max1 gt 0) and (max2 eq 0) then image1(*,j)=image1(*,j-1)
 endfor
 for j=ny-2,0,-1 do begin
  max1=max(image1(*,j+1))
  max2=max(image1(*,j))
  if (max1 gt 0) and (max2 eq 0) then image1(*,j)=image1(*,j+1)
 endfor

;__________________________DISPLAY IMAGE_____________________________
if (test eq 1) then begin
 window,0,xsize=1024,ysize=1024
 !p.position=[0,0,1,1]
 !x.range=[-1.,1.]
 !y.range=[-1.,1.]
 !x.style=1
 !y.style=1
 plot,[x1_sun,x2_sun,x2_sun,x1_sun,x1_sun],[y1_sun,y1_sun,y2_sun,y2_sun,y1_sun]
 oplot,[0,0],[-1,1],linestyle=2
 oplot,[-1,1],[0,0],linestyle=2
 nphi	=361
 phi	=2.*!pi*findgen(nphi)/float(nphi-1)
 oplot,cos(phi),sin(phi)
 rsun	=1
 pos	=0.
 crota2 =0.
 blong	=0.
 dlat	=10.
 coordsphere,rsun,pos,crota2,blat,blong,dlat
 !noeras=1

 x1_	=(1.+x1_sun)/2.
 x2_	=(1.+x2_sun)/2.
 y1_	=(1.+y1_sun)/2.
 y2_	=(1.+y2_sun)/2.
 !p.position=[x1_,y1_,x2_,y2_]
 !x.range=[x1_sun,x2_sun]
 !y.range=[y1_sun,y2_sun]
 loadct,3
 plot,[x1_sun,x2_sun,x2_sun,x1_sun,x1_sun],[y1_sun,y1_sun,y2_sun,y2_sun,y1_sun]
 nxw  	=long(!d.x_vsize*(x2_-x1_)+0.5)
 nyw  	=long(!d.y_vsize*(y2_-y1_)+0.5)
 z    	=congrid(image1,nxw,nyw)
 ind0	=where(image1 ne 0)
 statistic,image1(ind0),zavg,zsig
 c1	=zavg-3*zsig
 c2	=zavg+3*zsig
 tv,bytscl(z,min=c1,max=c2),x1_,y1_,xsize=(x2_-x1_),ysize=(y2_-y1_),/normal
endif

;'________________SAVE DATA______________________________________'
para.dpix_euv=dpix_euv
save,filename=dir+savefile,input,para
print,'Data saved in file = ',savefile
SKIP:
end
