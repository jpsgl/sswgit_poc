pro stereo_test

;LOOP PARAMETERS______________________________________
base	=200	&print,'loop base      base = ',base
h0	=-50.	&print,'loop center    h0   = ',h0
hmin	=4.	&print,'min height     hmin = ',hmin
az	=45.	&print,'aximuth        az   = ',az
th	=-30.	&print,'inclination    th   = ',th
l1	=90.	&print,'longitude      l1   = ',l1
b1	=20.	&print,'latitude       b1   = ',b1
;EPHEMERIDES__________________________________________
rsun	=960.   &print,'solar radius rsun   = ',rsun
l0	=45.	&print,'long.disk center l0 = ',l0
b0	=10.	&print,'lat.disk center  b0 = ',b0
p 	=15.	&print,'pos.angle        p  = ',p 
p0	=-15.	&print,'rot.angle        p0 = ',p0 
grid	= 5.	&print,'grid           grid = ',grid

;DISPLAY______________________________________________
io	=0
form	=0
char	=1
fignr	='0'
plotname='test_loop'
unit	=0
ref	=''
fig_open,io,form,char,fignr,plotname,unit
!p.multi=[0,3,2,0,0]

;LOOP PARAMETERS --> LOOP PLANE COORDINATES___________________
loop_para_coord,base,h0,hmin,th,rsun,xl,yl,zl
rloop	=sqrt(h0^2+(base/2.)^2)
!x.range=[-1.1,1.1]*rloop
!y.range=[-1.1,1.1]*rloop
vl	=zl*cos(th*!pi/180.)
plot,xl,vl,thick=3
phi	=2.*!pi*findgen(361)/360.
oplot,rsun*cos(phi),rsun*(sin(phi)-1.)
oplot,rsun*cos(phi),rsun*(sin(phi)-1.)+hmin,linestyle=1

;LOOP PLANE COORDINATES --> HELIOGRAPHIC COORDINATES___________
rmin	=rsun+hmin
helio_loopcoord,xl,yl,zl,rsun,az,th,l1,b1,rmin,l,b,r
!x.range=[-1,1]*rloop*(180./!pi)/960.
!y.range=[-1,1]*rloop*(180./!pi)/960.
dl	=l-l1
db	=b-b1
plot,dl,db,thick=3
rsun_lb =(180./!pi)
p_lb	=0.
p0_lb   =0.
l1_lb	=0.
b1_lb	=0.
coord_sphere,rsun_lb,p_lb,p0_lb,l1_lb,b1_lb,grid

;TRANSFORMATION IN IMAGE COORDINATES___________________
helio_trans2,p,p0,l0,b0,l,b,r,x,y,z,rsun
xav	=(min(x)+max(x))/2.
yav	=(min(y)+max(y))/2.
!x.range=xav+[-1,1]*rloop
!y.range=yav+[-1,1]*rloop
plot,x,y,thick=3
coord_sphere,rsun,p,p0,l0,b0,grid

;INVERSION OF LOOP PARAMETERS__________________________
base	=0
h0	=0
az	=0
th	=0
loop_coord_para,x,y,hmin,rsun,p,p0,l0,b0,l1,b1,az,th,base,h0
print,'loop base    base = ',base
print,'loop center  h0   = ',h0
print,'aximuth      az   = ',az
print,'inclination  th   = ',th
print,'longitude    l1   = ',l1
print,'latitude     b1   = ',b1
fig_close,io,fignr,ref
end

