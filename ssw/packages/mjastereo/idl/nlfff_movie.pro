pro nlfff_movie,moviedir
;+
; Project     : SOHO/MDI, SDO/HMI, STEREO 
;
; Name        : NLFFF_MOVIE 
;
; Category    : Magnetic data modeling 
;
; Explanation : Making movie from gif or jpeg files in MOVIEDIR
;
; Syntax      : IDL>nlfff_movie,modelfile,runfile,para
;
; Inputs      : moviedir = directory with movie files
;
; Outputs     ; runfile   = output files *_coeff.dat, *_fit.sav
;
; History     : 2-Mar-2012, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

;___________________MAKE MOVIE GIF OR JPG FILES_______________________
;filelist	=findfile(moviedir+'*.gif',count=nfiles)
 filelist	=findfile(moviedir+'*.jpeg',count=nfiles)
 if (nfiles eq 0) then stop,'No gif or jpeg files found'
 if (nfiles ge 1) then for i=0,nfiles-1 do print,i,'-->',filelist(i)
;image2movie,filelist,/mpeg,/nodelete
 print,'Movie images produced '
end
