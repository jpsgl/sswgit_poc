pro fit_dif,x,xh,dh,yh,yfit,ind_fit,x0,a,da,chi_dif,noise,dev,nbin_dif,opt
;+
; Project     : all data
;
; Name        : POWERLAWFIT
;
; Category    : Event statistics
;
; Explanation : Fitting of powerlaw function with threshold effects
;
; Syntax      : IDL>powerlawfit,x,
;
; Inputs      : x=fltarr(n)=array of values of n events  
;
; Outputs     ; a	=powerlaw slope 
;		x0	=threshold value
;
; History     : 20-Apr-2015, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

common powerlaw_var,xh_,yh_,dh_,ind_fit_,yfit_,x0_,noise_,dev_

;_______________________HISTOGRAM_________________________________
x1	=min(x)
x2	=max(x)
x1log	=alog10(x1)
dhlog	=1./float(nbin_dif)
nh	=long((alog10(x2)-alog10(x1))/dhlog+1)
xh	=10.^(x1log+dhlog*findgen(nh))
yh	=fltarr(nh)
dh	=fltarr(nh)
for i=0,nh-1 do begin
 if (i le nh-2) then dh(i)=xh(i+1)-xh(i)
 if (i eq nh-1) then dh(i)=dh(i-2)^2/dh(i-3)
 ind	=where((x ge xh(i)) and (x lt xh(i)+dh(i)),nbin)
 yh(i)	=nbin/dh(i)
endfor

;_______________________POWELL OPTIMIZATION__________________________
xh_	=xh
yh_	=yh
dh_	=dh
y0	=max(yh*dh,im)			;max number of events per bin
x0	=xh(im)
n0	=y0*dh(im)
ind_fit =where((xh ge x0) and (yh*dh ge 1.),nf)
x1log	=alog10(xh(ind_fit(0)))
x2log	=alog10(xh(ind_fit(nf-1)))
y1log	=alog10(yh(ind_fit(0)))
y2log	=alog10(yh(ind_fit(nf-1)))
a	=-(y2log-y1log)/(x2log-x1log)
nev	=total(yh(ind_fit)*dh(ind_fit))
coeff   =[alog10(n0),a]   	  	      ;initial guess 
ncoeff  =n_elements(coeff)
xi      =fltarr(ncoeff,ncoeff)
for i=0,ncoeff-1 do xi(i,i)=0.1
ftol    =1.e-4
ind_fit_=ind_fit
x0_	=x0
if (opt eq 1) then powell,coeff,xi,ftol,chi_dif,'fit_dif_powell'
chi_dif	=fit_dif_powell(coeff)		;-->updates yfit_,noise_,dev_,chi_dif
yfit 	=yfit_
ind_fit	=ind_fit_
x0 	=x0_
n0      =10.^coeff(0)
a       =coeff(1)
da	=a/sqrt(nev)
noise   =noise_
dev	=dev_

end
