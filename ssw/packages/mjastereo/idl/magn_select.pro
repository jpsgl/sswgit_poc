pro magn_select,modelfile,para
;+
; Project     : AIA
;
; Name        : MAGN_SELECT 
;
; Category    : SOHO/MDI, SDO/HMI data analysis
;
; Explanation : selects a subset of magnetic field lines
;		to be used as coronal loop constraints 
;		in forward-fitting of nonlinear force-free field models.
;
; Syntax      : IDL> mag_select,modelfile,para
;
; Inputs      : modelfile = input filename *_field.sav
;               para     = input parameters
;                          [ds,ngrid,nmag,nsm,qloop,nseg,acc,amax,hmax,dpix,
;                           niter,qzone,eps,qv,thresh,q_mag,halt,iopt]
;
; Outputs     ; saves parameters in file *_loops.sav
;		--> bzmap,x,y,field_lines,alpha,q_scale,cpu
;
; History     : 16-May-2011, Version 1 written by Markus J. Aschwanden
;		30-Jan-2012, Version 2 released to SSW 
;
; Contact     : aschwanden@lmsal.com
;-

print,'_______________MAGN_SELECT________________________'
ngrid	=para[1]
dpix	=para[9]
thresh	=para[14]

savefile=modelfile+'_field.sav'
restore,savefile   ;-->bzmap,x,y,field_lines,alpha,q_scale
dim     =size(field_lines)
ns	=dim(1)
if (dim(0) le 2) then nf=1
if (dim(0) eq 3) then nf=dim(3)
nx	=n_elements(x)
ny	=n_elements(y)
x1	=x[0]
x2	=x[nx-1]
y1	=y[0]
y2	=y[ny-1]
fov	=[x1,y1,x2,y2]
field_loops=field_lines
alpha_all  =alpha

;_____________________FIELDLINE SELECTION____________________
mask	=lonarr(nx,ny)
isel_	=lonarr(nf)
gauss_	=fltarr(nf)
for k=0,nf-1 do begin
 ind    =where(field_loops(*,2,k) ne 0,np)
 ix1    =long((field_loops(0,0,k)-x1)/dpix)
 iy1    =long((field_loops(0,1,k)-y1)/dpix)
 ix2    =long((field_loops(np-1,0,k)-x1)/dpix)
 iy2    =long((field_loops(np-1,1,k)-y1)/dpix)
 if (ix1 ge 0) and (ix1 le nx-1) and (ix2 ge 0) and (ix2 le nx-1) and $
    (iy1 ge 0) and (iy1 le ny-1) and (iy2 ge 0) and (iy2 le ny-1) then begin
 b1	=abs(bzmap(ix1,iy1))
 b2	=abs(bzmap(ix2,iy2))
 if ((mask(ix1,iy1) eq 0) and (b1 ge thresh)) or $
    ((mask(ix2,iy2) eq 0) and (b2 ge thresh)) then begin
  isel_(k)=1
  gauss_(k)=b1>b2
  i1	=(ix1-long(ngrid/2))>0
  i2	=(i1+ngrid-1)<(nx-1)
  j1	=(iy1-long(ngrid/2))>0
  j2	=(j1+ngrid-1)<(ny-1)
  i3	=(ix2-long(ngrid/2))>0
  i4	=(i3+ngrid-1)<(nx-1)
  j3	=(iy2-long(ngrid/2))>0
  j4	=(j3+ngrid-1)<(ny-1)
  mask(i1:i2,j1:j2)=1
  mask(i3:i4,j3:j4)=1
 endif
 endif
endfor

ind	=where(isel_ eq 1,nsel)
print,'Number of loops selected = ',nsel
field_lines=fltarr(ns,5,nsel)
alpha	=fltarr(nsel,2)
isort	=sort(-gauss_(ind))
for k=0,nsel-1 do begin
 i	=ind(isort(k))
 field_lines(*,0,k)=field_loops(*,0,i)
 field_lines(*,1,k)=field_loops(*,1,i)
 field_lines(*,2,k)=field_loops(*,2,i)
 alpha(k,0)=alpha_all(i,0)
 alpha(k,1)=alpha_all(i,1)
endfor 

;_____________________DISPLAY________________________________
window,0,xsize=nx,ysize=ny
loadct,0
!p.position=[0,0,1,1]
plot,[0,0],[0,0],xrange=[x1,x2],yrange=[y1,y2],xstyle=1,ystyle=1
tv,bytscl(bzmap)
loadct,5
for i=0,nf-1 do begin
 ind    =where(field_loops(*,2,i) ne 0,ns)
 oplot,field_loops(0:ns-1,0,i),field_loops(0:ns-1,1,i),color=128
endfor
for i=0,nsel-1 do begin
 ind    =where(field_lines(*,2,i) ne 0,ns)
 oplot,field_lines(0:ns-1,0,i),field_lines(0:ns-1,1,i),color=200,thick=3
endfor

;_____________________SAVE PARAMETERS________________________
savefile=modelfile+'_loops.sav'
save,filename=savefile,bzmap,x,y,field_lines,alpha,q_scale,cpu
print,'parameters saved in file = ',savefile
end

