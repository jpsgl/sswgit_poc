pro euvi_stereo_rot2,x_pix,y_pix,r_alt,para,xb_pix,yb_pix,zb_pix
;+
; Project     : STEREO
;
; Name        : EUVI_STEREO_ROT
;
; Category    : Data analysis
;
; Explanation : This routine computes xb position in STEREO B image from x position 
;		in STEREO A image for a co-aligned pair of STEREO images. 
;
; Syntax      : IDL> euvi_stereo_rot,x_pix,y_pix,para,r_alt,xb_pix,yb_pix,zb_pix
;
; Input:
;               x_pix   =x-pixel position in image A
;               y_pix   =x-pixel position in image A
;               para    =structure with image parameters
;		r_alt	=radius of source altitude (r_alt=1.0 for solar surface)
; Output:
;		xb_pix	=x-pixel position in image B
;		yb_pix	=x-pixel position in image B
;		zb_pix	=x-pixel position in image B
;		 (assuming crpix1 is identical in images A and B)
;
; History     : May 2007, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

crpix1	=para.crpix1
crpix2	=para.crpix2
cdelt1	=para.cdelt1
cdelt2	=para.cdelt2
a_sep	=para.sep_deg*(!pi/180.)
r_arc	=para.rsun			;solar radius in arcsec
r_sun   =r_arc/cdelt1			;solar radius in pixels
r_pix	=r_sun*r_alt			;distance from Sun center [pixels}

xa	=(x_pix-crpix1)/r_pix
ya	=(y_pix-crpix2)/r_pix
euvi_stereo_rot1,xa,ya,r_alt,a_sep,xb
yb	=ya
zb	=sqrt(r_alt^2-xb^2-yb^2)
xb_pix	=xb*r_pix+crpix1
yb_pix	=yb*r_pix+crpix2
zb_pix	=zb*r_pix
end
