function euvi_hydro_fit_powell,coeff

common powell_para,s_,hs,flux_obs,te_,resp_,flux_model,temp_model,area_h,par_model,temin,temax,hmax,rad_pix,frac_max,n1_h

h		=hs>0
s		=s_
ns		=n_elements(s)
htop		=max(hs,itop) 
tchr		=0.01				;[MK]
rsun		=6.96				;solar radius [100 Mm]
cdelt1		=1.6
hchr		=2./(cdelt1*0.725)
te		= coeff(0) >tchr 	 	;loop top temp [MK]
h0		=(coeff(1)>hchr)<htop  		;loop footpoint height [pix]
lam		=(coeff(2)<rsun)>(-rsun)	;scale height [100 Mm]
h1		=(coeff(3)>hchr)<htop  		;loop footpoint height [pix]
lam1		=(coeff(4)<rsun)>(-rsun)	;scale height [100 Mm]
l		=(s(itop)-s(0))>(s(ns-1)-s(itop))
lambda 		=(lam/rsun)*rad_pix 		;scale height [pixels]
lambda1 	=(lam1/rsun)*rad_pix 		;scale height [pixels]
s_l		=(s-hchr)/(l-hchr)
eps		=(tchr/te)^(7./2.)
temp_model	=tchr+(te-tchr)*( (s_l*(2.-s_l)) > eps )^(2./7.) 
ind2		=where(s gt s(itop),nind2)
if (nind2 ge 1) then $
 temp_model(ind2)=tchr+(te-tchr)*( (s_l(ind2)*(2.-s_l(ind2))) > eps )^(2./7.) 
p1_h		=exp(-(h-hchr)/lambda) 	 
p1_top		=exp(-(htop-hchr)/lambda)
if (nind2 ge 1) then p1_h(ind2)=p1_top*exp((htop-h(ind2))/lambda1) 
ind3		=where((s le s(itop)) and (h lt h0),nind3)
ind4		=where((s gt s(itop)) and (h lt h1),nind4)
if (nind3 ge 1) then p1_h(ind3)=0.
if (nind4 ge 1) then p1_h(ind4)=0.
n1_h		=p1_h/temp_model	
em1_h		=n1_h^2 
flux_profile	  =fltarr(ns,6)
fluxmin	 	 =0.001
w_sm		=7<(ns-1)
for iw=0,5 do begin
 resp  		=interpol(resp_(*,iw),te_,temp_model)>0 
 flux_profile(*,iw)=smooth(em1_h*resp,w_sm) 
endfor
noise		=1.
em		=min((flux_obs>noise)/(flux_profile>fluxmin))
flux_model	 =em*flux_profile
frac6		=fltarr(6)
for iw=0,5 do begin
 frac6(iw)=total(flux_model(*,iw)>fluxmin)/total(flux_obs(*,iw)>fluxmin)
endfor
frac_max	=avg(frac6)
dev	        =1.-frac_max
par_model	=[te,h0,lam,h1,lam1,em,l,htop]
return,dev
end
