pro euvi_evol,date,tstart,tend,index,images_fov,iseq,sc,wave,nmin,id,nsme,area,pos,dir,filename,ct,nrebin
;+
; Project     : STEREO
;
; Name        : EUVI_EVOL 
;
; Category    : Data graphics 
;
; Explanation : This routine reads the GOES light curve for a 
;		specified time interval (<1 day) and plots
;		the FOV images, along with difference images,
;		and creates a plot in postscript format.
;
; Syntax      : IDL> euvi_evol,date,tstart,tend,index,images_fov,iseq,sc,wave,nmin
;
; Inputs      : date      ='20061231' ; YYYYMMDD
;               tstart    ='070000'   ; HHMMSS [UT]
;               tend      ='100000'   ; HHMMSS [UT]
;		index(n)  = array of image index structures	  
;		images_fov(*,*,n) = array of subimages
;		iseq	  = number of selected images
;		sc	  = spacecraft 'a', 'b', or ['a','b']
;		wave	  = wavelength label
;		nmin	  = GOES smoothing boxcar (3 or 5 min)
;	        id	  = EUVI event #
;	        nsme	  = smoothing boxcar for EUV background light curve
;	        area	  = area for EUV light curve [i3,j3,i4,j4]
;               pos       = heliographic position (label)
;		dir       = directory name of output file
;
; Outputs     : postscript file of plot *.ps
;
; History     : 23-Feb-2007, Version 1 written by Markus J. Aschwanden
;	        14-Feb-2007, change contrast definition into percentages
;               10-Nov-2008, add two-spacecraft display A+B 
;
; Contact     : aschwanden@lmsal.com
;-

if (n_params(0) le 13) then pos=' '
if (n_params(0) le 14) then dir=' '
if (n_params(0) le 15) then ct =3
if (n_params(0) le 16) then nrebin=2

;read GOES data_____________________________________________________
if (tend eq '240000') then tend='235959'
mon      =['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
date_str =strmid(date,6,2)+'-'+mon(long(strmid(date,4,2))-1)+'-'+strmid(date,2,2)
time1  =date_str+' '+strmid(tstart,0,2)+':'+strmid(tstart,2,2)+':'+strmid(tstart,4,2)
time2  =date_str+' '+strmid(tend,0,2)+':'+strmid(tend,2,2)+':'+strmid(tend,4,2)
euvi_goes_read,time1,time2,time_goes,goes_low,goes_high
maxflux	 =max(alog10(goes_low))

nt	=n_elements(time_goes)
if (nt gt 2) then begin
 dt	=time_goes(1)-time_goes(0)
 nsm	=long(nmin*60./dt)<nt
 nsm2	=long(0.5*60./dt)<nt
 print,'GOES smoothing dt,nsm=',dt,nsm
 dt_goes =time_goes(1)-time_goes(0)
 smooth_extra,alog10(goes_low),smooth_log_goes_low,nsm
 smooth_extra,alog10(goes_high),smooth_log_goes_high,nsm
 goes_low=10.^smooth_log_goes_low
 goes_high=10.^smooth_log_goes_high
 flux	 =goes_low
 hxr	=(flux-shift(flux,+1))/(time_goes-shift(time_goes,1))>0.
 hxr(0)	=hxr(1)
 hxr(nt-1)=hxr(nt-2)
 hxr	=smooth(hxr,nsm2)
 ti	=findgen(nt)
 indx	=where((ti gt nsm2/2) and (ti lt nt-1-nsm2/2)) 
 hxr1	=min(hxr(indx))
 hxr2	=max(hxr(indx))
 hxr_norm=(hxr-hxr1)/(hxr2-hxr1)
endif
if (nt le 2) then begin
 ti	 =time_goes
 hxr_norm=[0.,0.]
 indx	 =[0,1]
endif

;GOES class
goes=' '
if (maxflux ge -9) then goes='Z'+strtrim(string(10.^(maxflux+9),'(f4.1)'),2)
if (maxflux ge -8) then goes='A'+strtrim(string(10.^(maxflux+8),'(f4.1)'),2)
if (maxflux ge -7) then goes='B'+strtrim(string(10.^(maxflux+7),'(f4.1)'),2)
if (maxflux ge -6) then goes='C'+strtrim(string(10.^(maxflux+6),'(f4.1)'),2)
if (maxflux ge -5) then goes='M'+strtrim(string(10.^(maxflux+5),'(f4.1)'),2)
if (maxflux ge -4) then goes='X'+strtrim(string(10.^(maxflux+4),'(f4.1)'),2)

;EUV image sequence__________________________________________________ 
if (n_params(0) le 11) then nsme=0
dim	=size(images_fov)
nx	=dim(1)
ny	=dim(2)
if (n_params(0) le 12) then area=[0,0,nx-1,ny-1]
if (dim(0) le 2) then nte=1
if (dim(0) eq 3) then nte=dim(3)
t_i	=anytim(index.date_obs) mod 86400

flare	 =fltarr(nte)
i3	=area(0)*(nx-1)
j3	=area(1)*(ny-1)
i4	=area(2)*(nx-1)
j4	=area(3)*(ny-1)
for it=0,nte-1 do flare(it)=total(images_fov(i3:i4,j3:j4,it)/index(it).exptime)  ;DN/s
print,'max flare flux = ',max(flare)

;correction for mavericks
for i=0,nte-1 do begin
  ia=i-2 
  if (i lt 2) then ia=0
  if (i gt nte-3) then ia=nte-5
  ib=ia+4
  fmed  =median(flare(ia:ib))
  if (flare(i) le fmed/1.5) or (flare(i) ge fmed*1.5) then flare(i)=fmed
 endfor

flare_min=min(flare)
flare_max=max(flare)
flare_dn =flare_max-flare_min
qflare	 =flare_dn/flare_max
flare_norm=(flare-flare_min)/(flare_max-flare_min)

;creates plot_______________________________________________________
form=1 ;portrait
char	=1
fignr	 =''
plotname=dir+filename
if (strmid(filename,0,2) eq '20') then fignr='evol'
unit	=0
ref	=''
midnight=strmid(time1,0,10)+'00:00:00'
if (ct eq 0) then io1=1
if (ct ge 1) then io1=3
for io=0,io1,io1 do begin 
 fig_open,io,form,char,fignr,plotname,unit
 !p.position=[0.07,0.44,0.97,0.6]
 id_	=strtrim(string(id,'(i6)'),2)
 title=''
 if (goes ne ' ') then title=' GOES '+goes+', '
 if (id ge 0) then title='Event #'+id_+' '+pos+title
 frac	=string(qflare*100,'(f5.1)')+' %'
 title=title+'Flux='+string(flare_dn/1.e6,'(f5.1)')+' 10!U6!NDN/s ('+frac+')'
 if (n_params(0) ge 11) then !p.title=title
 !x.range=minmax(time_goes)
 !y.range=[0,1.2]
 !x.title=''
 !y.title='Flux (normalized)'
 !x.style=1
 !y.style=1
 !p.thick=2
 setutbase,midnight
 utplot,time_goes,(goes_high-min(goes_high))/(max(goes_high)-min(goes_high))
 ymax=max(goes_high,itm)
 !p.linestyle=2
 outplot,time_goes,(goes_low-min(goes_low))/(max(goes_low)-min(goes_low))
 !p.linestyle=0
 ymax=max(goes_low,itm)
 outplot,time_goes(indx),hxr_norm(indx),thick=4
 ymax=max(hxr_norm(indx),itm)
 outplot,t_i,flare_norm,thick=2,linestyle=3
 outplot,t_i,flare_norm,thick=2,psym=4
 ymax=max(flare_norm,itm)
goto,skip
 xpos	=0.8	&ypos=0.8
 xpos	=0.7	&ypos=0.9	;Fig 7 in EUVI paper (2009)
 xyouts_norm,xpos,ypos-0.0,'HXR',char
 xyouts_norm,xpos,ypos-0.1,'GOES/Hi',char
 xyouts_norm,xpos,ypos-0.2,'GOES/Lo',char
 xyouts_norm,xpos,ypos-0.3,'EUVI',char
 oplot_norm,[-0.1,-0.02]+xpos,[ 0.0, 0.0]+ypos,0,4
 oplot_norm,[-0.1,-0.02]+xpos,[-0.1,-0.1]+ypos,0,2
 oplot_norm,[-0.1,-0.02]+xpos,[-0.2,-0.2]+ypos,2,2
 oplot_norm,[-0.1,-0.02]+xpos,[-0.3,-0.3]+ypos,3,2
 oplot_norm,[-0.1,-0.02]+xpos,[-0.3,-0.3]+ypos,3,2,4
skip:
 !noeras=1
 
 file=dir+filename+'evol.sav'
 nflare=nx*ny
 save,filename=file,time_goes,goes_high,goes_low,hxr_norm,t_i,flare,nflare,flare_dn,qflare
 print,'data saved in file = ',file

;automated selection of image sequence
if (iseq(0) eq -1.) then begin
 if (nte le 1) then iseq=[0]
 if (nte ge 2) and (nte le 5) then iseq=long(1+findgen(nte-1))
 if (nte gt 5) then iseq=long(1+float(nte-2)*float(findgen(4))/3.)
 print,'Total number of images  = ',nte
 print,'Selected image sequence = ',iseq
endif

;plot EUVI times 
 !p.thick=1
 nseq	=n_elements(iseq)
 for it=0,nte-1 do begin
; oplot,t_i(it)*[1,1],[0,flare_norm(it)]
  if (min(abs(it-iseq)) eq 0) then oplot,t_i(it)*[1,1],!y.range
 endfor
 
 dx	=0.25
 dy	=0.2 
 i0	=0.
 j0	=0.
 flux_med=median(images_fov)
 for it=0,nseq-1 do begin
  x1_	=0.0+it*dx
  y1_	=dy
  x2_	=x1_+dx-0.002 
  y2_	=2.*dy 
  image	=images_fov(i0:i0+nx-1,j0:j0+ny-1,iseq(it))
  z1	=min(image)
  z2	=max(image)
  dim	=size(image)
  nx_	=dim(1)/nrebin
  ny_	=dim(2)/nrebin
  image_=rebin(image(0:nrebin*nx_-1,0:nrebin*ny_-1),nx_,ny_)
  if (io eq 0) then begin
   nxw  =long(!d.x_vsize*(x2_-x1_)+0.5)
   nyw  =long(!d.y_vsize*(y2_-y1_)+0.5)
   z    =congrid(image_,nxw,nyw)
  endif
  if (io ne 0) then z=image_
  !p.position=[x1_,y1_,x2_,y2_]
  !p.title=' '
  !x.range=[0,nx]
  !y.range=[0,ny]
  !x.style=5
  !y.style=5
  plot,[0,0],[0,0]
  tv,bytscl(alog10(z),min=alog10(z1),max=alog10(z2)),x1_,y1_,xsize=(x2_-x1_),ysize=(y2_-y1_),/normal
  oplot,[i3-0.5,i4+0.5,i4+0.5,i3-0.5,i3-0.5],[j3-0.5,j3-0.5,j4+0.5,j4+0.5,j3-0.5]
; dateobs=index(iseq(it)).date_d$obs
  dateobs=index(iseq(it)).date_obs
  wavelen=index(iseq(it)).wavelnth
  xyouts_norm,0.01,0.02,strmid(dateobs,11,8)+' UT',1,0,255
  xyouts_norm,0.01,0.90,string(wavelen,'(i3)')+' A',1,0,255
  if (sc eq 'a') then sc_='AHEAD'
  if (sc eq 'b') then sc_='BEHIND'
  if (it eq 0) then xyouts_norm,0.01,0.80,'SC='+sc_,1,0,255
  !noeras=1

  it2	=iseq(it)
  it1	=it2-1
 if (it1 ge 0) then begin
  ima	=images_fov(i0:i0+nx-1,j0:j0+ny-1,it1)
  imb	=images_fov(i0:i0+nx-1,j0:j0+ny-1,it2)
  za	=ima(sort(ima))
  zb	=imb(sort(imb))
  coeff =linfit(za,zb)
  diff	=imb-(coeff(0)+coeff(1)*ima)
  dim	=size(diff)
  nx_	=dim(1)/nrebin
  ny_	=dim(2)/nrebin
  diff_ =rebin(diff(0:nrebin*nx_-1,0:nrebin*ny_-1),nx_,ny_)
  if (io eq 0) then z=congrid(diff,nxw,nyw)
  if (io ne 0) then z=diff
  y3_	=0.00
  y4_	=dy
  !p.position=[x1_,y3_,x2_,y4_]
  plot,[0,0],[0,0]
  d1	=-flux_med/4
  d2	=+flux_med/4
  tv,bytscl(z,min=d1,max=d2),x1_,y3_,xsize=(x2_-x1_),ysize=(y4_-y3_),/normal
 endif
 endfor
 fig_close,io,fignr,ref
endfor

end
