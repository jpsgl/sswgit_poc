pro aia_mag_bpot,x1,y1,z1,coeff,bx,by,bz
;+
; Project     : AIA/SDO
;
; Name        : AIA_LOOP_BPOT
;
; Category    : Data modeling 
;
; Explanation : Potential magnetic field vector
;
; Syntax      : IDL>aia_loop_bpot,x1,y1,z1,coeff,bx,by,bz
;
; Inputs      : x1       = x-coordinate of loop midpoint (solar radii)
;               y1       = y-coordinate of loop midpoint 
;               z1       = z-coordinate of loop midpoint 
;               coeff    = coefficients of magnetic potential field model
;
; Outputs     : bx	= magnetic field component B_x(x1,y1,z1)
;               by	= magnetic field component B_y(x1,y1,z1)
;               bz	= magnetic field component B_z(x1,y1,z1)
;
; History     : 19-Sep-2011, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

;_____________________POTENTIAL FIELD INPUT_________ __________________
dim     =size(coeff)
ncomp   =dim(1)
magn    =coeff(*,0)
xmag    =coeff(*,1)
ymag    =coeff(*,2)
zmag    =coeff(*,3)                     ;negative height
hmag    =zmag-1.			;cartesian coordinates 
pol     =magn/abs(magn)                 ;magnetic polarity
bx   	=0.
by   	=0.
bz   	=0.
for ic=0,ncomp-1 do begin
 d_mag=sqrt((x1-xmag(ic))^2+(y1-ymag(ic))^2+(z1-zmag(ic))^2)
 cos_x =(x1-xmag(ic))/d_mag 
 cos_y =(y1-ymag(ic))/d_mag
 cos_z =(z1-zmag(ic))/d_mag
 b0_p  =magn(ic)*(hmag(ic)/d_mag)^2
 bx    =bx+b0_p*cos_x
 by    =by+b0_p*cos_y
 bz    =bz+b0_p*cos_z
endfor
end

