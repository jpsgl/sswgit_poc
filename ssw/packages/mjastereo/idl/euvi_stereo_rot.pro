pro euvi_stereo_rot,x_pix,y_pix,para,r_alt,xb_pix,yb_pix,zb_pix
;+
; Project     : STEREO
;
; Name        : EUVI_STEREO_ROT
;
; Category    : Data analysis
;
; Explanation : This routine computes xb position in STEREO B image from x position 
;		in STEREO A image for a co-aligned pair of STEREO images. 
;
; Syntax      : IDL> euvi_stereo_rot,x_pix,y_pix,para,r_alt,xb_pix,yb_pix,zb_pix
;
; Input:
;               x_pix   =x-pixel position in image A
;               y_pix   =x-pixel position in image A
;               para    =structure with image parameters
;		r_alt	=radius of source altitude (r_alt=1.0 for solar surface)
; Output:
;		xb_pix	=x-pixel position in image B
;		yb_pix	=x-pixel position in image B
;		zb_pix	=x-pixel position in image B
;		 (assuming crpix1 is identical in images A and B)
;
; History     : May 2007, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

crpix1	=para.crpix1
crpix2	=para.crpix2
cdelt1	=para.cdelt1
cdelt2	=para.cdelt2
r_m	=0.696*1.e9
da	=para.da/r_m
db	=para.db/r_m
a_sep	=para.sep_deg*(!pi/180.)
rsun	=para.rsun
rad_pix	=rsun/cdelt1

alpha_a	=(x_pix-crpix1)*(cdelt1/3600.)*(!pi/180.)		;rad
delta_a	=(y_pix-crpix2)*(cdelt2/3600.)*(!pi/180.)		;rad
t2      =tan(alpha_a)^2+tan(delta_a)^2
z2	=(r_alt>1.)^2*(1.+t2)-float(da)^2*t2
z	=da*t2/(1.+t2)				;if (z2 le 0)
ind	=where(z2 gt 0,nind)
if (nind ge 1) then z(ind)=(da*t2(ind)+sqrt(z2(ind)))/(1.+t2(ind))
ind0	=where(z2 le 0,nind0)
if (nind0 ge 1) then r_alt(ind0)=da*sqrt(t2(ind0)/(1.+t2(ind0)))
x   	=(da-z)*tan(alpha_a)
y       =(da-z)*tan(delta_a)
xb0     =db*sin(a_sep)
zb      =sqrt(db^2-xb0^2)
gamma_b =atan((zb-z)/(xb0+x))				;sign !!! 
alpha_b =!pi/2.-gamma_b-a_sep
delta_b =atan(y/(db-z))
q	=(db/da)				;coalignment factor
xb_pix	=crpix1+q*alpha_b*(3600./cdelt1)*(180./!pi)
yb_pix	=crpix2+q*delta_b*(3600./cdelt2)*(180./!pi)
zb_pix	=z*rad_pix
end
