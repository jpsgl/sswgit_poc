pro loop_plot_image,image,z1,z2,ct,io

x1_	=!p.position(0)
y1_	=!p.position(1)
x2_	=!p.position(2)
y2_	=!p.position(3)
if (io ne 0) then z=image
if (io eq 0) then begin
 nxw  =long(!d.x_vsize*(x2_-x1_)+0.5)
 nyw  =long(!d.y_vsize*(y2_-y1_)+0.5)
 z    =congrid(image,nxw,nyw)
endif
plot,[0,0],[0,0]
tv,bytscl(z,min=z1,max=z2),x1_,y1_,xsize=x2_-x1_,ysize=y2_-y1_,/normal
!noeras=1
end


