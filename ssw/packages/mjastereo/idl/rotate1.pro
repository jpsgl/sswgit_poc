pro rotate1,x,y,xrot,yrot,x0,y0,angle
;
; PURPOSE:
;	rotation of x,y-coordinates by angle a
;
; INPUT:
;	x 	= array of x-corrdinates
;	y 	= array of y-coordinates
;	x0	= x-coordinate of rotation center
;	y0	= y-coordinate of rotation center
;	angle	= rotation angle clounterclockwise [deg]
;
; OUTPUT:
;	xrot	= x-coordinates of rotated array
;	yrot	= y-coordinates of rotated array
;
; HISTORY:
;	aschwanden@lmsal.com
;
;
a       =angle*!pi/180.
xrot    =x0+(x-x0)*cos(a)-(y-y0)*sin(a)
yrot    =y0+(x-x0)*sin(a)+(y-y0)*cos(a)
end

