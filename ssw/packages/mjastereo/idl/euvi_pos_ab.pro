pro euvi_pos_ab,pos,para,ia,ja,ib,jb,image_pair
;+
; Project     : STEREO
;
; Name        : EUVI_FOV_AB
;
; Category    : Coordinate transformations
;
; Explanation : This routine allows to determine image pixel position
;		corresponding to a heliographic position POS
;		in a stereoscopic image pair coaligned to epipolar
;		coordinates (using EUVI_IMAGE_PAIR)
;
; Syntax      : IDL>euvi_fov_ab,pos,index_b,para,ia,ja,ic,jc
;
; Input       : pos       =heliographic position, e.g. 'W30N10'
;		para	  =parameter structure defined by EUVI_STEREOPAIR
; 
; Output      : ia	  =x-pixel position in image STEREO/A
;		ja        =y-pixel position in image STEREO/A
;		ib        =x-pixel position in image STEREO/B
;		jb        =y-pixel position in image STEREO/B
;
; History     : 3-Sep-2009, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

l	=float(strmid(pos,1,2))
b	=float(strmid(pos,4,2))
north	=strmid(pos,0,1)
west	=strmid(pos,3,1)
if (north ne 'N') and (north ne 'S') and (west ne 'W') and (west ne 'E') then $
 stop,'Heliographic position ill-defined : ',pos
if (north eq 'N') then lat=+b
if (north eq 'S') then lat=-b
if (west  eq 'W') then lon=+l
if (west  eq 'E') then lon=-l
sep_deg	=para.sep_deg
ab_deg  =para.ab_deg
crpix1	=para.crpix1
crpix2	=para.crpix2
cdelt1	=para.cdelt1
cdelt2	=para.cdelt2
rsun	=para.rsun
rpix	=rsun/cdelt1
lon_a	=lon-sep_deg/2.
lon_b	=lon+sep_deg/2.

;pixels in heliographic coordinates
la	=((-90.>lon_a)<90.)*!pi/180.
lb	=((-90.>lon_b)<90.)*!pi/180.
b	=lat*!pi/180.
xa	=rpix*sin(la)*cos(b)
xb	=rpix*sin(lb)*cos(b)
ya	=rpix*sin(b)
yb	=rpix*sin(b)

;pixels in AB images
ab	=ab_deg*!pi/180.
ia	=crpix1+xa*cos(ab)-ya*sin(ab)
ja	=crpix2+xa*sin(ab)+ya*cos(ab)
ib	=crpix1+xb*cos(ab)-yb*sin(ab)
jb	=ja
stop
end
