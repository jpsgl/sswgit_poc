pro aia_mag_cartesian,x,y,z,xx,yy,zz,xcen,ycen

b0	=asin(ycen)
l0	=asin(xcen)
b	=asin(y)
l	=asin(x)
xx	=(l-l0)
yy	=(b-b0)
r	=sqrt(x^2+y^2+z^2)
zz	=r
end
