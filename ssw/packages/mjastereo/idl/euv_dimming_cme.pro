pro euv_dimming_cme,dir,iev,vers,test
;+
; Project     : SDO/AIA,  
;
; Name        : EUV_DIMMING_CME 
;
; Category    : data analysis of EUV dimming and CME mass, speed, and energy
;
; Explanation : Reads AIA images and performs EUV dimming analysis
;
; Syntax      : IDL>euv_dimming_disp,dir,iev,vers,test
;
; Inputs      : dir,iev,vers,test
;
; Outputs     ; IDL save file: *.sav
;
; History     : 24-Feb-2016, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

common dimming,model,time,a_t,v_t,x_t,q_t,q_obs,q_sig,h0,ind_fit

print,'__________________EUV_DIMMING_CME_______________________________
string0,3,iev,iev_str
catfile ='dimming_'+iev_str+vers+'.txt'
readcol,dir+catfile,event_,dateobs_,noaa_,helpos_,instr_,$
  w1_,w2_,w3_,w4_,w5_,w6_,w7_,w8_,fov_,$
  format='(I,A,A,A,A,A,A,A,A,A,A,A,A,A)',/silent
nt	=n_elements(event_)

;________________________RESTORE SAVED PARAMETERS______________________
savefile='dimming_'+iev_str+vers+'.sav'
restore,dir+savefile    ;-->input,para,exptime_,flux_grid,x_grid,y_grid,$
			;flux_prof,emtot_prof,telog_prof,tsig_prof,chi_prof,$
			;wave_,time,dem_grid,dem_t,te_mk,telog
print,'read parameter file = ',savefile
dim    =size(flux_grid)
nx     =dim(1)
ny     =dim(2)
nwave  =dim(3)
cadence =input.cadence
helpos	=input.helpos
nbin	=input.nbin
lon	=long(strmid(helpos,4,2))
lat	=long(strmid(helpos,1,2))
l	=lon*!pi/180.
b	=lat*!pi/180.
rho	=acos(cos(l)*cos(b))
r0	=6.96e10 ;cm


;___________________LOOP OF CORRELATED TIME PROFILES____________________
np	  =4			;number of profiles 
a_model   =fltarr(nt,np)
v_model   =fltarr(nt,np)
x_model   =fltarr(nt,np)
em_model  =fltarr(nt,np)
em_obs	  =fltarr(nt)
a0_model  =fltarr(np)
t0_model  =fltarr(np)
ta_model  =fltarr(np)
td_model  =fltarr(np)
qe_model  =fltarr(np)
ekin_model=fltarr(np)
chi_model =fltarr(np)

;_________________SELECTED LOCATION OF MAXIMUM DIMMING________________
ccc_min  =0.9				;variable degree of correlation
dem_max =max(dem_grid,ijt_max)		;location and time of maximum
dem_norm=dem_grid/dem_max			;normalize EM to unity
ix	=ijt_max mod nx
iy	=(ijt_max mod (nx*ny))/nx
it_max=ijt_max/(nx*ny)
dem_xym =reform(dem_norm(ix,iy,*))		;time array at max EM
ccc	=fltarr(nx*ny)
cc2	=fltarr(nx,ny)
dem_sum=fltarr(nt)
isum	=0
for i=0,nx-1 do begin
 for j=0,ny-1 do begin
  k	=j*nx+i
  dem_xy=reform(dem_norm(i,j,*))		;time array EM(t)
  ccc(k)=correlate(dem_xy(it_max:nt-1),dem_xym(it_max:nt-1))
  cc2(i,j)=ccc(k)
  if (ccc(k) ge ccc_min) then dem_sum=dem_sum+dem_xy
 endfor
endfor
dem_xy	  =dem_sum

;__________________INTERPOLATE LOW_EM OUTLIERS__________________
q_out	=0.5
for it=1,nt-2 do begin
 em_avg=(dem_xy(it-1)+dem_xy(it+1))/2.
 if (dem_xy(it) lt em_avg/2.) then begin
  dem_xy(it)=em_avg
  dem_t(*,it)=(dem_t(*,it-1)+dem_t(*,it+1))/2.
 endif
endfor

;___________________SELECT TIME RANGE FOR FITTING________________
qpeak	=0.90					
qtail	=0.05					
dem_max=max(dem_xy,it_max) &it_max=it_max > 1
dem_min=min(dem_xy(it_max+1:nt-1),it_min)
it_	=findgen(nt)
dem_tail=dem_min+(dem_max-dem_min)*qtail				
dem_peak=dem_min+(dem_max-dem_min)*qpeak			
ind_fit=where((dem_xy lt dem_peak) and (dem_xy gt dem_tail) and (it_ ge it_max),nfit)
if (nfit le 5) then begin
 nfit	=6 < (nt-it_max)
 ind_fit=it_max+findgen(nfit)
endif

q_obs	=dem_xy/dem_max
q_sig  =q_obs*0.1/sqrt(np)			;EM accuracy (SS method)
em_max	=max(emtot_prof)
em_obs =dem_xy*(em_max/dem_max) 

;_________________CME VELOCITY AND ACCELERATION___________
for m=0,np-1 do begin
 model	  =m+1

 dt     =time(1)-time(0)
 t0     =time(it_max)           		;s
 it     =findgen(nt)
;tau    =1000.                                  ;s
 h0     =4.7e9*te_mk     		       ;[cm] density scale height
 d0	=(r0+h0)*sin(rho)	                  ;distance from disk center
 v0     =3.0e7                  		;cm/s
 a0     =0.1*v0/dt
 qmin	=0.1
 coeff  =[a0/1.e5,t0/1.e3,qmin]
 ncoeff =n_elements(coeff)
 xi     =fltarr(ncoeff,ncoeff)
 for k=0,ncoeff-1 do xi(k,k)=1.0
 ftol   =1.e-4
 powell,coeff,xi,ftol,fmin,'euv_dimming_powell'
 chi    =euv_dimming_powell(coeff)   ;-->a_model,v_model,x_model,q_t

;____________________POWELL OPTIMIZATION________________________________
 acc     =coeff(0)*1.e5
 t0      =coeff(1)*1.e3                         ;s
 qmin	 =coeff(2) > 0
 tau	 =120.					;s (acceleration < time resolution)
 d0      =(r0+h0)*sin(rho)	                ;distance from disk center
 a_model(*,m)=a_t
 v_model(*,m)=v_t
 x_model(*,m)=x_t+d0
 em_model(*,m)=em_max*q_t
 a0_model(m)=acc
 t0_model(m)=t0
 ta_model(m)=(t0+tau)		                 ;s
 chi_model(m)=chi
 qhalf	=qmin+(1.-qmin)*0.5			;dimming half time [s]
 ind	=where(q_t ge qhalf,nind)
 it1	=max(ind) < (nt-2)
 it2	=it1+1 
 td_model(m)=time(it1)+(time(it2)-time(it1))*(qhalf-q_t(it1))/(q_t(it2)-q_t(it1))
endfor		;for m=0,np-1 do 

;_________________DIMMING AREA_________________________________
image1  =reform(dem_grid(*,*,it_max))
image2  =reform(dem_grid(*,*,it_min))
for it=0,nt-1 do begin
 image   =reform(dem_grid(*,*,it))
 if (it le it_max) then image1=image1 > image
 if (it gt it_max) then image2=image2 < image
endfor
diff    =(image1-image2)>0.
area	=fltarr(nx,ny)
zmed	=median(image1)
for i=0,ny-1 do begin
 for j=0,ny-1 do begin
  if (diff(i,j) ge 2.*zmed) then area(i,j)=1.  
 endfor
endfor
ind_dimm=where(area eq 1,npix_dimm)
dx_grid_cm=r0*(x_grid(1)-x_grid(0))	;grid scale in units of cm
area_dimm=npix_dimm*dx_grid_cm^2	;area in cm^2
a       =cos(rho)
b       =h0*sin(rho)
c       =-area_dimm
l_cm    =(-b+sqrt(b^2-4*a*c))/(2.*a)
vol     =l_cm^2*h0
angle   =90.*(l_cm/(r0*!pi/2.))		;angular width [deg]

;_________________DIMMING MASS AND KINETIC ENERGY________________
dem_da	=max(emtot_prof)
em_tot  =dem_da
nel	=sqrt(dem_da*area_dimm/vol)
mp	=1.67e-24 			;proton mass
mass	=mp*nel*vol
v_avg	=avg(v_model(nt-1,*))
ekin_model=(1./2.)*mass*v_avg^2

;_________________GRAVITATIONAL ENERGY________________________
g	=6.672e-8	;dyne cm&2 g^-2	gravitational constant
m_sun	=1.99e33	;g solar mass
e_grav	=g*(m_sun/r0)*mass	;gravitational energy
v_esc	=sqrt(2.*g*(m_sun/r0))  ;escape velocity [cm/s]

;________________SAVE PARAMETERS______________________________
save,filename=dir+savefile,input,para,exptime_,flux_grid,x_grid,y_grid,$
        flux_prof,emtot_prof,telog_prof,tsig_prof,chi_prof,wave_,time,$
        dem_grid,dem_t,te_mk,telog,area_dimm,v_avg,$
        l_cm,rho,angle,area,vol,em_tot,em_obs,dem_t,nel,mass,em_model,$
	d0,x_lasco,v_lasco,t_lasco,a_model,v_model,x_model,ekin_model,$
        q_t,q_obs,a0_model,t0_model,ta_model,td_model,chi_model,ind_fit,h0,$
        e_grav,v_esc
print,'parameters saved in file = ',savefile
end
