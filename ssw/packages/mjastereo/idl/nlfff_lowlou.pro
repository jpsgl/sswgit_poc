pro nlfff_lowlou,dir,filename,vers,para
;+
; Project     : SOHO/MDI, SDO/HMI 
;
; Name        : MAGN_SIMUL
;
; Category    : magnetic field moedling
;
; Explanation : gets data of Low & Lou (1990) from Malanushenko 
;
; Syntax      : IDL> nlfff_lowlou,dir,filename,vers,para
;
; Inputs      : filename = *._coeff.dat, *_cube.sav, *_field.sav,   
;		para     = input parameters
;
; Outputs     ; saves parameters in file *_coeff.dat
;		--> m1,x1,y1,z1,a1		
;		saves parameters in file *_cube.sav:
;               --> x,y,z,bx,by,bz,b,a
;               saves parameters in file *_field.sav:
;               --> bzmap,x,y,field_lines,alpha,q_scale,cpu,merit
;
; History     : 28-Nov-2011, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

print,'___________NLFFF_LOWLOU_____________________________'
name3	=strmid(filename,0,3)
nr	=strmid(filename,12,1)
if (name3 ne 'LOW') then goto,skip_end 
dir1	='/sanhome/anny/4Markus/low_lou_a_0.6_n_2.0_unsign'
dir2	='/sanhome/anny/4Markus/low_lou_a_0.02_n_1/'
if (nr eq '1') then dir_anna=dir1
if (nr eq '2') then dir_anna=dir2
nsmax	=long(para[7])
ds      =para[13]
x1	=para[20]
y1	=para[21]
x2	=para[22]
y2	=para[23]
stereo	=0

;________________MAGNETIC MAP__________________________________
restore,dir_anna+'/photosphere.sav' ;-->b_3d.bx,by,bz,b,x,y,z,alpha
bx      =b_3d.bx
by      =b_3d.by
bz      =b_3d.bz
bzmap   =bz(*,*,0)
xx      =b_3d.x
yy      =b_3d.y
zz      =b_3d.z
a       =b_3d.alpha
nx      =n_elements(xx)
ny      =n_elements(yy)
nz      =n_elements(zz)
scale	=(x2-x1)/(max(xx)-min(xx))
x	=xx*scale
y	=yy*scale
z	=zz*scale+1.	;tangent to solar surface
dpix	=x[1]-x[0]
rsun	=1./dpix
xcenter =min(abs(x),crpix1)
ycenter =min(abs(y),crpix2)

magfile =dir+filename+'_magn.fits'
writefits,magfile,bzmap
image=readfits(magfile,header)
sxaddpar,header,'DATE_OBS',filename
sxaddpar,header,'NAXIS',2
sxaddpar,header,'NAXIS1',nx
sxaddpar,header,'NAXIS2',ny
sxaddpar,header,'CRPIX1',crpix1
sxaddpar,header,'CRPIX2',crpix2
sxaddpar,header,'CDELT1',dpix
sxaddpar,header,'CDELT2',dpix
sxaddpar,header,'SOLAR_P0',0
sxaddpar,header,'R_SUN',rsun
writefits,magfile,bzmap,header
print,'Magnetic field file written = ',magfile

;________________LOCATE FIELDLINE FILES____________________
search  =dir_anna+'/fl_*.sav'
print,'searchfile = ',search
files   =findfile(search,count=nff)
print,'Number of field lines = ',nff
help,files

nlenmax  =0
nf	=nff/4
field_lines=fltarr(nsmax,4,nf)
np_	=lonarr(nf)
k	=0
for kk=0,nff-1,4 do begin
 restore,files(kk)
 nlf    =strlen(files(kk))
 dim    =size(f)
 nlen   =dim(2)
 nlenmax=nlenmax>nlen
 if (nlen le nsmax) then ind=findgen(nlen)
 if (nlen gt nsmax) then begin
  q	=float(nlen)/float(nsmax)
  ind	=long(q*findgen(nsmax))<(nlen-1)
 endif
 nind	=n_elements(ind)
 np_[k]=nind
 field_lines(0:nind-1,0,k)=x[0]+dpix*(reform(f[0,ind]))	;x-coordinate
 field_lines(0:nind-1,1,k)=y[0]+dpix*(reform(f[1,ind]))	;y-coordinate
 field_lines(0:nind-1,2,k)=z[0]+dpix*(reform(f[2,ind]))	;z-coordinate
 nl	=strlen(dir_anna)
 if (kk eq 0) or ((kk mod 100) eq 0) or (kk eq nf-1) then $
 print,'file ',kk,k,' ',strmid(files(kk),nl,nlf-nl),' nlen=',nlen,nlenmax
 k	=k+1
 if (k eq nf) then goto,endloop
endfor
endloop:

;________________SAVE LOOP COORDINATES________________________
if (stereo eq 0) then loopfile=dir+filename+'_loop_'+vers+'.dat'
if (stereo eq 1) then loopfile=dir+filename+'_loop_3d.dat'
zero	=0.
openw,1,loopfile
for k=0,nf-1 do begin
 for is=0,np_[k]-1 do begin
  xl	=field_lines(is,0,k)
  yl	=field_lines(is,1,k)
  zl	=field_lines(is,2,k)
  if (stereo eq 0) then  printf,1,k,xl,yl,zero,zero
  if (stereo eq 1) then  printf,1,k,xl,yl,zl,zero
 endfor
endfor
close,1
print,'loopfile written = ',loopfile
SKIP_END:
end
