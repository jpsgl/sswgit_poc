function euvi_dimming_powell2,coeff

common powell_par2,x_,y_

y0	=coeff(0)
tau	=coeff(1)
tstart	=coeff(2)
y	=y0*(1.-exp(-((x_-tstart)/tau)^2/2.))
ind	=where(x_ lt tstart,nind)
if (nind ge 1) then y(ind)=0.
n	=n_elements(x_)
dev	=sqrt(total(abs(y-y_)))/float(n)
return,dev
end
