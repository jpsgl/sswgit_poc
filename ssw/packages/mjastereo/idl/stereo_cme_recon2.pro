;STEREO_RECON_______________________________________________
;3D-RECONSTRUCTION
niter	=5
nx	=100
ny	=100
angle	=30
for iter=0,niter-1 do begin
 recon  =fltarr(nx,ny)
 a      =(angle/180.)*!pi
 if (iter eq 0) then stereoz=stereob
 if (iter ge 1) then begin
  diff  =max(abs(recb-stereob),i0)
  diffpeak=recb(i0)-stereob(i0)
  sign  =diffpeak/abs(diffpeak)
  corr  =-(0.5*sign*diff*exp(-(x-x(i0))^2/(2.*wid^2))>0)
  stereoz=stereob+corr
 endif
 for i=0,nx-1 do begin
  ishift=float(i-nx/2.)/tan(a)
  recon(i,*)=stereoa(i)*shift(stereoz/total(stereoz),-ishift)
 endfor
 recon=recon*total(model)/total(recon)  ;renormalization

 reca =fltarr(nx)
 recb =fltarr(nx)
 for j=0,ny-1 do reca(*)=reca(*)+recon(*,j)
 if (angle mod 180) eq 0 then recb=reca
 if (angle mod 180) ne 0 then begin
  reconb        =rot(recon,angle)
  for i=0,nx-1 do recb(i)=total(reconb(i,*))
 endif
 diffabs=total(abs(recon-model))/total(model)
 xyouts,0.6,0.9-0.03*iter,'Diff='+string(diffabs),/normal
;oplot,x,recb
;oplot,x,stereoz,linestyle=2
endfor

;STEREO_BACKPROJ________________________________________________
;3D-RECONSTRUCTION
recon   =fltarr(nx,ny)
a       =(angle/180.)*!pi
y       =findgen(ny)
yy      =ny/2+(findgen(ny)-ny/2)/tan(a)
stretchb=interpol(stereob,y,yy)
for i=0,nx-1 do begin
 ishift=float(i-nx/2.)/tan(a)
 recon(i,*)=stereoa(i)*shift(stretchb/total(stretchb),-ishift)
 ibound=(abs(ishift)<(ny-1))>1
 if (i le nx/2) then recon(i,0:ibound)=0
 if (i ge nx/2) then recon(i,ny-ibound:ny-1)=0
endfor
recon=recon*total(model)/total(recon)  ;renormalization
end

