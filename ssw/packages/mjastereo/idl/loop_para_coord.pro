pro loop_para_coord,base,h0,hmin,th,rsun,xl,yl,zl
;+
; Project       : Loop 3D geometry fitting
;
; Name          : LOOP_PARA_COORD 
;
; Category      : Data modeling
;
; Explanation   : computes loop coordinates in XZ-loop plane (n=180)
;		  XL(0,...,N-1), YL(0,...,n-1)=0, ZL(0,...,N-1)       
;		  from 3 loop parameters (base,h0,hmin) in same length
;		  units as solar radius RSUN (preferably in arcsecs). 
;
; Syntax	: IDL>loop_para_coord,base,h0,hmin,th,rsun,xl,yl,zl
;
; Input		: base    =footpoint separation (arcsec)
;		  h0      =offset of circular loop center loop baseline (arcsec)
;		  hmin	  =height of footpoints	(arcsecs)		
;		  th	  =inclination angle of loop plane (deg)
;	          rsun    =solar radius (arcsecs)

half	=base/2.
phi0	=atan(h0/half)
rloop   =sqrt(h0^2+half^2)
n	=180
xl 	=fltarr(n)
yl 	=fltarr(n)
zl 	=fltarr(n)
for i=0,n-1 do begin
 phi	=-phi0+(!pi+2.*phi0)*float(i)/float(n-1)
 xl(i)  =rloop*cos(phi)
 zl(i)  =rloop*sin(phi)+h0
endfor
end
