pro euvi_stereo_rot1,xa,ya,r,a_sep,xb,yb,zb
;+
; Project     : STEREO
;
; Name        : EUVI_STEREO_ROT1
;
; Category    : Data analysis
;
; Explanation : This routine computes xb position in STEREO B image from x position 
;		in STEREO A image for a co-aligned pair of STEREO images. 
;
; Syntax      : IDL> euvi_stereo_rot,x_pix,y_pix,para,r_alt,xb_pix,yb_pix,zb_pix
;
; Input:
;               xa      =x-position in image A (in solar radii)
;               ya      =y-position in image A
;               za      =z-position in image A
; Output:
;		xb	=x-position in image B
;		yb	=y-position in image B
;		zb	=z-position in image B
;
; History     : May 2007, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

lat	=asin(ya/r)
cos_lat =(cos(lat)<1.)>(-1.)
rlat_a	=xa/(r*cos_lat)
lon_a	=asin(rlat_a)
lon_b	=lon_a+a_sep
xb	=r*sin(lon_b)*cos(lat)
end
