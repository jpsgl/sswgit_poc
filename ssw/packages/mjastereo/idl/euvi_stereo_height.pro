pro euvi_stereo_height,x_pix,y_pix,xb_pix,yb_pix,para,x,y,z,r
;+
; Project     : STEREO
;
; Name        : EUVI_STEREO_HEIGHT
;
; Category    : Data analysis
;
; Explanation : This routine computes height from stereoscopic displacements 
;		in image A and B for a co-aligned pair of STEREO images.
;
; Syntax      : IDL> euvi_stereo_height,x_pix,y_pix,xb_pix,yb_pix,para,x,y,z,r
;
; Input:
;		x_pix	=x-pixel position in image A
;		y_pix	=x-pixel position in image A
;		xb_pix	=x-pixel position in image B
;		yb_pix	=x-pixel position in image B
;		para	=structure with image parameters
; Output:
;		x	=cartesian x-coordinate from Sun center (in solar radii)
;		y	=cartesian y-coordinate from Sun center (in solar radii)
;		z	=cartesian z-coordinate from Sun center (in solar radii)
;		r	=distance from Sun center (in solar radii)
;
; History     : Oct/Nov 2007, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

crpix1	=para.crpix1
crpix2	=para.crpix2
cdelt1	=para.cdelt1
cdelt2	=para.cdelt2
r_m	=0.696*1.e9
da	=para.da/r_m
db	=para.db/r_m
a_sep	=para.sep_deg*(!pi/180.)

db	=da	;because alpha_b is already corrected to same distance

alpha_a	=(x_pix-crpix1)*(cdelt1/3600.)*(!pi/180.)		;rad
delta_a	=(y_pix-crpix2)*(cdelt2/3600.)*(!pi/180.)		;rad
alpha_b	=(xb_pix-crpix1)*(cdelt1/3600.)*(!pi/180.)		;rad
delta_b	=(yb_pix-crpix2)*(cdelt2/3600.)*(!pi/180.)		;rad
gamma_a	=(!pi/2.)-alpha_a
gamma_b	=(!pi/2.)-alpha_b-a_sep
x_a	=da*tan(alpha_a)
x_b	=db*sin(alpha_b)/sin(gamma_b)
x	=(x_b*tan(gamma_b)-x_a*tan(gamma_a))/(tan(gamma_b)-tan(gamma_a))
z       =(x_a-x)*tan(gamma_a)
y       =(da-z)*tan(delta_a)
r	=sqrt(x^2+y^2+z^2)
end
