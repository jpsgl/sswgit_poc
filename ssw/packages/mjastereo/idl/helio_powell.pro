function helio_powell,coeff
;optimization function called from helio_loopcoord2.pro

common powell_var,p_,p0_,l0_,b0_,rsun_,az_,th_,l1,b1,r1_,yl_,xi,yi

xl_	=coeff(0)
zl_	=coeff(1)
helio_loopcoord,xl_,yl_,zl_,rsun_,az_,th_,l1,b1,r1_,l_,b_,r_
helio_trans2,p_,p0_,l0_,b0_,l_,b_,r_,x_,y_,z_,rsun_
dev	=sqrt((x_-xi)^2+(y_-yi)^2)	
return,dev
end

