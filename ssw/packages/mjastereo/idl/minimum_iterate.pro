pro minimum_iterate,x,y,x1,x2,it,qstep
;+
; Project     : GENERAL
;
; Name        : MINIMUM_ITERATE
;
; Category    : Numerical algorithm : minimum finding
;
; Explanation : Finding of the minimum x of an iterative function f(x)
;		by halfing existing interval.
;		This procedure supports MAGN_FIT.PRO 
;
; Syntax      : IDL>minimum_iterate,x,y,x1,x2,it,qstep
;
; Inputs      : x	  = abscissa values of an array
; 		y	  = ordinate values of an array y(x)
;		it	  = iteration step
;		qstep	  = fraction of diminuishing interval
;
; Outputs     ; 
;		x1	  = new value of x (next guess of minimum)
;		y1	  = new value of y
;
; History     : 16-May-2011, Version 1 written by Markus J. Aschwanden
;               30-Jan-2012, Version 2 released to SSW
;
; Contact     : aschwanden@lmsal.com
;-

xmin	=min(x)
xmax	=max(x)
half	=(xmax-xmin)/2.
dx	=half*qstep^(1+it)
ymin	=min(y,imin)
x0	=x[imin]
x1	=(x0-dx)>xmin  
x2	=(x0+dx)<xmax 
end
