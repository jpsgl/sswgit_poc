pro euvi_dimming2,fov_file,fov,index,io,dimfile,ibad,nsm,nsm0,sep,coord4,thresh

common powell_par2,x_,y_

;read images________________________________________________________
restore,fov_file ;-->images_fov(nx,ny,nt)
t	=anytim(index.date_obs,/seconds) mod 86400
dim	=size(images_fov)
nx	=dim(1)
ny	=dim(2)
nt	=dim(3)
i1	=fov(0)
j1	=fov(1)
i2	=fov(2)
j2	=fov(3)
wave	=index(0).wavelnth
obs	=index(0).obsrvtry
i0	=index(0).crpix1
j0	=index(0).crpix2
cdelt1	=index(0).cdelt1
cdelt2	=index(0).cdelt2
naxis1	=index(0).naxis1
naxis2	=index(0).naxis2
rsun	=index(0).rsun
t1	=index(0).date_obs
t2	=index(nt-1).date_obs
r0	=rsun/cdelt1
n	=360
phi	=2.*!pi*findgen(n)/float(n-1)
grid	=10

print,'Replacing bad images & normalize by exposure time_____________'
if (obs eq 'STEREO_A') then isc=0
if (obs eq 'STEREO_B') then isc=1
if (wave eq '171') then iwave=0
if (wave eq '195') then iwave=1
if (wave eq '284') then iwave=2
texp	=index(0).exptime  ;EUVI_DISPLAY normalizes to exptime of 1st image
for it=0,nt-1 do begin
 images_fov(*,*,it)=(images_fov(*,*,it))/texp
 if (ibad(isc,iwave,it) eq 1) then images_fov(*,*,it)=images_fov(*,*,it-1)
endfor

print,'Cleaning spikes_______________________________________________'
spike_thresh=1.5
datacube=float(images_fov)
trace_clean,datacube,images_fov,spike_thresh

print,'Temperature response__________________________________________'
respfile=concat_dir('$SSW_MJASTEREO','idl/euvi_response.dat')
readcol,respfile,t_mk,r1_a,r2_a,r3_a,r4_a,r1_b,r2_b,r3_b,r4_b
if (wave eq '171') then r_a=r1_a
if (wave eq '195') then r_a=r2_a
if (wave eq '284') then r_a=r3_a
spline_p,t_mk,r_a,te,resp
rmax	=max(resp,itm)
temp	=te(itm)
q_eq	=exp(-!pi/4.)	;=0.455938  equivalent width for gaussian 
itemp	=where(resp ge rmax*q_eq,ntemp)
tmin	=min(te(itemp))
tmax	=max(te(itemp))
r44	=total(resp(itemp))/float(ntemp)  ;average response function
print,'Wavelength         wave       =',wave,' A'
print,'Temperature range  T1,T0,T2   =',tmin,temp,tmax,' MK'
print,'Response functipn  R_max      =',rmax,' DN/s for EM=10^44 cm-3'
print,'Average response   R_avg/R_max=',r44/rmax

print,'Brightest position in AR______ ___________________________'
image0	=reform(images_fov(*,*,0))
statistic,image0,z_avg,z_sig
smooth_flatedge,image0,image0_smo,nsm0
zmax	=max(image0_smo,im)
j	=long(float(im)/nx)
i	=im-j*nx
;................................................................
imin	=i1+i
jmin	=j1+j
arctan,imin-i0,jmin-j0,phi0,phi0_deg    ;position angle of darkest dimming 
dpos	=(1./r0)*(180./!pi)	        ;azimuth angle per pixel
rdim	=sqrt((imin-i0)^2+(jmin-j0)^2)  ;distance of dimming to Sun center
r1	=r0*(1.+(47./2.)*temp/696.)	;half scale height
angle	=180.*asin((rdim<r1)/r1)/!pi	;angle of dimming to Sun center
if (imin lt i0) then angle=-angle	;East side

print,'Heliographic coordinates_____________________________________'
ix	=imin
iy	=jmin
ir	=sqrt((ix-i0)^2+(iy-j0)^2)
xa	=(ix-i0)/r0			;normalize to solar radius	
ya	=(iy-j0)/r0
crota_deg=index(nt-1).crota		;rotation angle of image to ecliptic 
crota	=crota_deg*!pi/180.
da	=sqrt(xa^2+ya^2)
xa_ecl	=da*cos(phi0+crota)
ya_ecl	=da*sin(phi0+crota)
ra	=(rdim>r1)/r0			;EUV altitude (in solar radii)
la	=asin(xa_ecl/ra)
ba	=asin(ya_ecl/ra)
lon	=la*(180./!pi)+sep(isc)
lat	=ba*(180./!pi)
;-----------------------WCS transformation for h=0------------------------
ix	=i0+(imin-i0)/ra 		;correct to photospheric height
iy	=j0+(jmin-j0)/ra 
wcs	=fitshead2wcs(index(nt-1))
coord	=wcs_get_coord(wcs,[ix,iy])
wcs_convert_from_coord,wcs,coord,'HG',lon1,lat1
;-----------------------WCS transformation for h=0------------------------
nw	=long((nx<ny)/sqrt(2.))
ns	=nw
x_pix	=findgen(nw)-nw/2.		
x_deg	=x_pix*dpos			;azimuth angle in pixel
;-----------------------background column depth---------------------------
x	=rdim+findgen(ns)-ns/2
euvi_columndepth,x,r0,temp,dz_,dz_pix,lam_pix
dz8_back=dz_pix*(696./r0)		;column depth in Mm 

print,'proj distance Sun center   = ',rdim/r0,format='(a,f7.4)'
print,'Theo. altitude of centroid = ',r1/r0,format='(a,f7.4)'
print,'angle to disk center   (SC)= ',angle,' deg',format='(a,f6.1,a)'
print,'angle to disk center(EARTH)= ',angle+sep(isc),' deg',format='(a,f6.1,a)'
print,'heliogr long,lat    (EARTH)= ',lon1,lat1,' deg(WCS)',format='(a,2f6.1,a)'
print,'heliogr long,lat   (EARTH) = ',lon,lat,' deg',format='(a,2f6.1,a)'
print,'Rotation angle     (CROTA) = ',crota_deg,' deg',format='(a,f6.1,a)'
print,'pos angle dimming   (PHI0) = ',phi0_deg,' deg',format='(a,f6.1,a)'
print,'Column depth     dz/lambda = ',dz_pix/lam_pix,format='(a,f6.2,a)'

print,'Display images_______________________________________________'
form    =1      ;0=landscape, 1=portrait
char    =1.0    ;character size
plotname=strmid(dimfile,0,23)+'_diff'
fignr	='_'
unit    =0      ;window number
ref     =''	
ct	=3
ncol	=long(0.7*sqrt(nt-1)+0.99)
nrow    =long(nt/ncol+0.99)>(ncol+1)
nrebin	=4

fig_open,io,form,char,fignr,plotname,unit
loadct,ct
coeff_cor=fltarr(4)
coeff_dim=fltarr(4,nt)
for it=0,nt-1 do begin
 image 	=reform(images_fov(*,*,it))
 diff 	=image-image0
 icol	=(it mod ncol)
 irow	=long(it/ncol)
 dx	=0.80/float(ncol)
 dy	=0.98/float(nrow)
 x1_	=0.1+dx*icol
 x2_	=x1_+dx*0.95
 y2_	=0.99-dy*irow
 y1_	=y2_-dy*0.95
 !p.position=[x1_,y1_,x2_,y2_]
 !p.title=' '
 dy	=long((ny-nx)/2.)
 !x.range=[i1,i2]
 !y.range=[j1+dy,j2-dy]
 !x.style=5
 !y.style=5
 dim	=size(diff)
 nxx	=nx/nrebin
 nyy	=ny/nrebin
 diff_	=rebin(diff(0:nrebin*nxx-1,0:nrebin*nyy-1),nxx,nyy)
 if (io eq 0) then begin
  nxw  =long(!d.x_vsize*(x2_-x1_)+0.5)
  nyw  =long(!d.y_vsize*(y2_-y1_)+0.5)
  z    =congrid(diff_,nxw,nyw)
 endif
 if (io ne 0) then z=diff_
 plot,[0,0],[0,0]
 tv,bytscl(z,min=-z_sig,max=z_sig),x1_,y1_,xsize=x2_-x1_,ysize=y2_-y1_,/normal
 xyouts_norm,0.1,0.9,string(it,'(I3)'),char
 oplot,i0+r0*cos(phi),j0+r0*sin(phi),linestyle=2	;plot limb
 oplot,imin*[1,1],jmin*[1,1],psym=1,symsize=3,color=255 ;darkest positition
 oplot,i0+rdim*[0,cos(phi0)],j0+rdim*[0,sin(phi0)]	;plot radial direction
 !noeras=1

;Dimming profile_____________________________________________
 if (it eq 0) then begin
  flux 	=fltarr(nw,nt)
  dimm 	=fltarr(nw,nt)
  xw	=fltarr(nw,ns)
  yw	=fltarr(nw,ns)
  zw	=fltarr(nw,ns)
  area	=lonarr(nw,ns)
  icen	=long((i1+i2)/2.)
  jcen	=long((j1+j2)/2.)
  rcen	=sqrt((icen-i0)^2+(jcen-j0)^2)
  arctan,icen-i0,jcen-j0,phi1,phi1_deg    ;position angle of darkest dimming 
  icurv	=fltarr(nw)			  ;no curvature correction at limb
  if ((rcen+ns/2) gt r0) then begin
   icurv=rcen-sqrt(rcen^2-(findgen(nw)-nw/2)^2)
   print,'curvature correction'
  endif 
  for is=0,ns-1 do begin
   imid	   =icen-i1+(is-ns/2-icurv)*cos(phi1(0))
   jmid	   =jcen-j1+(is-ns/2-icurv)*sin(phi1(0))
   xw(*,is)=imid+(findgen(nw)-nw/2)*sin(phi1(0)) ;subimage coordinates
   yw(*,is)=jmid-(findgen(nw)-nw/2)*cos(phi1(0)) ;subimage coordinates
  endfor
  dmin	=min(abs((xw-(imin-i1))^2+(yw-(jmin-j1))^2),ijm)
  is_dim =long(float(ijm)/nw)
  iw_dim =ijm-is_dim*nw
 endif
 if (it eq 0)    then zw0=bilinear(image,xw,yw)
 if (it eq nt-1) then zw1=bilinear(diff ,xw,yw)

 smooth_flatedge,image,image_smo,nsm	
 smooth_flatedge,diff ,diff_smo ,nsm	
 zw	 =bilinear(image_smo,xw,yw)		;subimage coordinates
 zw_data =bilinear(image    ,xw,yw)		;original data	
 zw_diff =bilinear(diff_smo ,xw,yw)		;smoothed
 zw_diff2=bilinear(diff    ,xw,yw)		;not smnoothed
 oplot,i1+xw(0,*)   ,j1+yw(0,*)     
 oplot,i1+xw(nw-1,*),j1+yw(nw-1,*)
 oplot,i1+xw(*,0)   ,j1+yw(*,0)    
 oplot,i1+xw(*,ns-1),j1+yw(*,ns-1)
 is1	=(is_dim-ns/4)>0
 is2	=(is_dim+ns/4)<(ns-1)
 for is=is1,is2 do flux(*,it)=flux(*,it) > zw(*,is)

;Gaussian fit to total flux cross-section_____________________________
 if (it eq 0) then begin
  x1min	=x_pix(nw/4)
  x1max	=x_pix(nw*3/4)
  w1max	=(x1max-x1min)/2.35
  for iter=0,3 do begin
   iw1	=long(iter*nw/10.)
   ind	=iw1+findgen(nw-iw1*2)
   b1	=min(flux(ind,0))
   x_flux=x_pix(ind)
   y_flux=flux(ind,0)-b1
   y1	 =max(y_flux,im)
   x1	 =x_pix(im+iw1)					;gaussian center
   w1	 =(total(y_flux)/y1)/2.35
   coeff =[y1,x1,w1,b1]
   yfit_cor=gaussfit(x_flux,y_flux,coeff,nterms=3)
   y1	 =coeff(0)
   x1	 =coeff(1)
   w1	 =coeff(2)
   print,'coeff=y1,x1,w1,b1=',y1,x1,w1,b1,format='(a,4f8.2)'
   coeff2=linfit(x_flux,y_flux)
   yfit_lin=coeff2(0)+coeff2(1)*x_flux
   dev_gauss=total(abs(yfit_cor-y_flux))
   dev_lin  =total(abs(yfit_lin-y_flux))
   if (y1 gt 0) and (x1 gt x1min) and (x1 lt x1max) and $
      (w1 lt w1max) and (dev_gauss lt dev_lin) then goto,fitok
   y1	=0.	;in case fit fails
  endfor
  fitok:
  coeff_cor=[coeff,b1]
  yfit_gauss=y1*exp(-((x_pix-x1)/w1)^2/2.)
  qdim  =0.
  qdim_max=(y1+b1)/y1
 endif

;dimming cross_section________________________________________________'
 nsig	=3.
 zmax	=max(zw_data)
 zmin	=min(zw_data)
 dark	=zmin+(zmax-zmin)*thresh
 if (it ge 1) then begin
  dimm(*,it) =dimm(*,it-1)	    		;dimm(*,*) is negative
  for is=is1,is2 do begin
   ind_dark=where(zw_data(*,is) le dark,ndark)
   if (ndark ge 1) then dimm(ind_dark,it)=$
     dimm(ind_dark,it) < reform(zw_diff(ind_dark,is))
  endfor
  ymin	=max(yfit_gauss)*exp(-1.)
  ind1	=where((dimm(*,it) ne 0) and (yfit_gauss le ymin),nind1)
  if (nind1 eq 0) then ind1=where((dimm(*,it) ne 0),nind1)
  statistic,dimm(ind1,it),dim_avg,dim_sig
  ind	=where((-(dimm(*,it)-dim_avg) ge nsig*dim_sig) and (yfit_gauss gt ymin),nind)
  if (nind ge 1) then qdim=max(-(dimm(ind,it)-dim_avg)/(yfit_gauss(ind)))
  qdim=(qdim < qdim_max)>0.
  coeff_dim(0,it)=qdim*coeff_cor(0)
  coeff_dim(1:2,it)=coeff_cor(1:2)
  coeff_dim(3,it)=-dim_avg
 endif
endfor
fig_close,io,fignr,ref

print,'Plot dimming curves____________________________________________'
unit	=1
fignr	='_'
plotname=strmid(dimfile,0,23)+'_prof'
fig_open,io,form,char,fignr,plotname,unit
x1_	=0.08
x2_	=0.28
y1_	=0.80
y2_	=0.95
!p.position=[x1_,y1_,x2_,y2_]
!p.title='Initial flux'
!x.title='Azimuthal pixel'
!y.title='Radial pixel'
!x.range=[0,nw]
!y.range=[0,ns]
!x.style=1
!y.style=1
plot,[0,0],[0,0]
if (io eq 0) then begin
 nxw  =long(!d.x_vsize*(x2_-x1_)+0.5)
 nyw  =long(!d.y_vsize*(y2_-y1_)+0.5)
 z0   =congrid(zw0,nxw,nyw)
 z1   =congrid(zw1,nxw,nyw)
endif
if (io ne 0) then z0=zw0
if (io ne 0) then z1=zw1
plot,[0,0],[0,0]
statistic,z0,z_avg,z_dev
c1	=z_avg-z_dev
c2	=z_avg+z_dev
tv,bytscl(z0,min=c1,max=c2),x1_,y1_,xsize=x2_-x1_,ysize=y2_-y1_,/normal
oplot,iw_dim*[1,1],is_dim*[1,1],psym=4,color=0
oplot,!x.range,is_dim*[1,1],color=0,thick=3
!noeras=1

y1_	=0.35
y2_	=0.50
!p.position=[x1_,y1_,x2_,y2_]
!p.title='Final Dimming'
plot,[0,0],[0,0]
tv,bytscl(z1,min=-z_sig,max=+z_sig),x1_,y1_,xsize=x2_-x1_,ysize=y2_-y1_,/normal
oplot,iw_dim*[1,1],is_dim*[1,1],psym=4,color=255
oplot,!x.range,is_dim*[1,1],color=255,thick=3

y1_	=0.60
y2_	=0.75
!p.position=[x1_,y1_,x2_,y2_]
!p.title=' '
!x.title='Azimuth angle  P [deg]'
!y.title='Flux [DN/s]'
!x.range=minmax(x_deg)
!x.style=1
!y.style=1
!y.range=[0,max(flux(*,0)+b1)*1.1]
plot,[0,0],[0,0]
plot,x_deg,flux(*,0)
oplot,x_deg,yfit_gauss+b1,thick=3
oplot,x_deg,fltarr(nw)+b1,linestyle=1

y1_	=0.15
y2_	=0.30
y1	=coeff_dim(0,nt-1)
x1	=coeff_dim(1,nt-1)
w1	=coeff_dim(2,nt-1)
b1	=coeff_dim(3,nt-1)   ;negative
yfit_dim=y1*exp(-((x_pix-x1)/w1)^2/2.)+b1
!p.position=[x1_,y1_,x2_,y2_]
!p.title=' '
ymax	=max(yfit_dim) > max(-dimm(*,nt-1))
!y.range=[-ymax*1.2,0]
plot,!x.range,[0,0],linestyle=1
oplot,x_deg,dimm(*,nt-1)		;smoothed profile
polyfill,[!x.range(0),x_deg,!x.range(1)],[0,dimm(*,nt-1),0],orientation=45,spacing=0.1
oplot,x_deg,-yfit_dim,thick=3

x1_	=0.33
x2_	=0.53
y1_	=0.05
y2_	=0.95
!p.position=[x1_,y1_,x2_,y2_]
!p.title='EUV Dimming'
!y.title=' '
dz	=ymax
!y.range=[-nt*dz,dz*0.25]
plot,[0,0],[0,0]
for it=0,nt-1 do begin
 dzi	=-dz*(it)
 oplot,x_deg,dimm(*,it)+dzi
 xyouts,x_deg(0),dimm(0,it)+dzi,string(it,'(I3)'),size=char
 y1	=coeff_dim(0,it)
 x1	=coeff_dim(1,it)
 w1	=coeff_dim(2,it)
 b1	=coeff_dim(3,it)
 yfit_dim=y1*exp(-(x_pix-x1)^2/(2.*w1^2))+b1	;dimming curve <0
 oplot,x_deg,-yfit_dim+dzi,thick=3
endfor

;CME mass calculation_________________________________________
q_fwhm  =2.*sqrt(2.*alog(2.))		;factor FWHM/gaussian width 
q_den	=sqrt(2.) 			;factor density width/flux width
f0_cor	=coeff_cor(0)  			;flux of corona [DN/s]
b0_cor  =coeff_cor(3)			;background flux of corona [DN/s]
w0_cor  =coeff_cor(2)*q_fwhm*q_den 	;FWHM corona (pixels)
f_dim	=reform(coeff_dim(0,*)) > 0. 	;dimming flux of CME [DN/s] (positive)
w_dim   =coeff_dim(2,*)*q_fwhm*q_den 	;FWHM dimming region (pixels)
w0_dim  =w_dim(nt-1) 			;best width of dimming (deg)
w0_dim_deg=w0_dim*dpos           	;FWHM dimming region (deg)
lam8	=47.*temp       		;Mm

;exponential curve fitted to dimming evolution 
f0_dim	=0.
tau	=0.
tstart	=0.
ifit	=where(f_dim ne 0,nfit)
if (max(f_dim) gt 0) and (nfit ge nt/2) then begin
 x_	=[t(ifit)-t(0)]
 y_	=[f_dim(ifit)]        ;positive
 coeff	=[f_dim(0),1000.,1000.]
 xi	=fltarr(3,3)
 for i=0,2 do xi(i,i)=1.
 ftol	=1.e-5
 powell,coeff,xi,ftol,fmin,'euvi_dimming_powell2'
 f0_dim	=coeff(0)  
 tau	=coeff(1)
 tstart	=coeff(2)+t(0)
endif
hours	=tstart/3600.
hour	=long(hours)
print,'Fitted starting time of dimming  tstart=',hour,' hrs'
minute  =(tstart-(hour*3600.))/60.
tstart_ut=string(hour,'(I2)')+':'+string(minute,'(I2)')
t_fit	=t(0)+(t(nt-1)-t(0))*findgen(101)/100.
f_fit	=f0_dim*(1.-exp(-((t_fit-tstart)/tau)^2/2.))   ;positive
ind	=where(t_fit lt tstart,nind)
if (nind ge 1) then f_fit(ind)=0.
q_dim	=fltarr(nt)+1.
q_fit	=fltarr(101)+1.
if (f0_cor ne 0) then begin
 q_dim	=(1.-f_dim/(f0_cor+b0_cor))
 q_fit	=(1.-f_fit/(f0_cor+b0_cor))
endif 
 
;coronal background density
pix8	=696.*(1./r0)			;pixel size in Mm
n_back  =sqrt(b0_cor/(r44*pix8^2*dz8_back))

;active region density
a_rad	=abs(angle*!pi/180.)
dz8_cor =lam8*cos(a_rad)+w0_cor*sin(a_rad)
print,'AR column depth  dz8_cor/lam8	= ',dz8_cor/lam8
print,'AR column depth  dz8_cor/w0_cor	= ',dz8_cor/w0_cor
n10_cor	=n_back
if (f0_cor gt 0) then n10_cor=sqrt(n_back^2+(f0_cor/(r44*pix8^2*dz8_cor)))

;CME mass
dz8_dim	=dz8_cor
mp24	=1.6726 			;1e-24 g
n10_dim	=fltarr(nt)
n10_fit	=fltarr(nt)
ind	=where(f_dim gt 0,nind)
if (nind ge 1) then n10_dim(ind)=n10_cor*sqrt(f_dim(ind)/(f0_cor))
n10_fit =n10_cor*sqrt(f_fit/f0_cor)
v24_dim	=(!pi/4.)*(w0_dim*pix8)^2*lam8		;cylindrical volume
m15_dim =mp24*n10_dim*(1.e10/1.e15)*v24_dim
m15_fit =mp24*n10_fit*(1.e10/1.e15)*v24_dim
m15	=max(m15_fit)

;plot profiles________________________________________________
x1_	=0.60
x2_	=0.95
y1_	=0.70
y2_	=0.95
!p.position=[x1_,y1_,x2_,y2_]
!p.title=' '
!x.title='Time t [hr]'
!y.title='Flux ratio  F(t)/F(0) [DN/s]'
!x.range=minmax(t)/3600.
!y.range=[0.,1.1]
plot,t/3600.,q_dim,psym=10
if (m15 gt 0) then begin
 oplot,t_fit/3600.,q_fit,thick=4
 oplot,[0,t(nt-1)]/3600.,[1,1],linestyle=1
 oplot,[0,t(nt-1)]/3600.,[1,1]*(1.-f0_cor/(f0_cor+b0_cor)),linestyle=1
 oplot,[1,1]*tstart/3600.,!y.range,linestyle=1
endif
!noeras=1

y1_	=0.37
y2_	=0.63
!p.position=[x1_,y1_,x2_,y2_]
!p.title=' '
!x.title='Time t [hr]'
!y.title='Mass flow  m(t) [10!U15!N g]'
!y.range=[0,(max(m15_dim)>1.)*1.1]
plot,t/3600.,m15_dim,psym=10
if (m15 gt 0) then begin
 oplot,t_fit/3600.,m15_fit,thick=4
 oplot,[1,1]*tstart/3600.,!y.range,linestyle=1
endif

if (io eq 0) then char=1.5
xyouts_norm,0,-0.3,strmid(dimfile,0,19)+' '+obs,char
xyouts_norm,0,-0.4,'Dimming start time ='+tstart_ut+' UT',char
xyouts_norm,0,-0.5,'Distance Sun center ='+string(rdim/r0,'(f6.3)'),char
xyouts_norm,0,-0.6,'f!Dcor!N='+string(f0_cor  ,'(F7.1)')+' DN/s',char
xyouts_norm,0,-0.7,'f!Ddim!N='+string(f0_dim  ,'(F7.1)')+' DN/s',char
xyouts_norm,0,-0.8,'Q!Ddim!N='+string(-100.*(1.-min(q_fit)),'(f6.1)')+' %',char
xyouts_norm,0,-0.9,'w!Ddim!N='+string(w0_dim_deg  ,'(F5.1)')+' deg, ('$
                               +string(w0_dim*pix8,'(I4)')+' Mm)',char
xyouts_norm,0,-1.0,'!9t!3!Dcor!N='+string(tau,'(I6)')+' s',char
xyouts_norm,0,-1.1,'n!De!N='+string(n10_cor*10.,'(f5.2)')+' 10!U9!N cm!U-3!N',char
xyouts_norm,0,-1.2,'n!DB!N='+string(n_back*10.,'(f5.2)')+' 10!U9!N cm!U-3!N',char
xyouts_norm,0,-1.3,'m!DCME!N='+string(m15,'(f5.2)')+' 10!U15!N g',char
fig_close,io,fignr,ref

coord4	=[(ix-i0)/r0,(iy-j0)/r0,phi0_deg,crota]
save,filename=dimfile,fov,index,t,q_fwhm,f0_cor,b0_cor,w0_cor,f_dim,$
	w_dim,w0_dim,w0_dim_deg,f0_dim,tau,t_fit,f_fit,q_dim,q_fit,lam8,$
	pix8,n_back,n10_cor,dz8_back,n10_dim,n10_fit,m15_dim,m15_fit,m15,$
	temp,tmin,tmax,ix,iy,i0,j0,r0,phi0_deg,crota,zw0,zw1,$
	coeff_cor,coeff_dim,flux,dimm,is_dim,iw_dim
print,'savefile written = ',dimfile
end
