seed1	=1234
seed2	=seed1*2
seed3	=seed1*3
seed4	=seed1*4
seed5	=seed1*5
seed6	=seed1*6
n	=10
ax	=-1.+2.*randomu(seed1,n)
ay	=-1.+2.*randomu(seed2,n)
az	=-1.+2.*randomu(seed3,n)
bx	=-1.+2.*randomu(seed4,n)
by	=-1.+2.*randomu(seed5,n)
bz	=-1.+2.*randomu(seed6,n)
a_	=[[ax],[ay],[az]]
b_	=[[bx],[by],[bz]]
vector_product_array,a_,b_,c_,cnorm_,angle_
for i=0,n-1 do begin 
 a	=[ax[i],ay[i],az[i]]
 b	=[bx[i],by[i],bz[i]]
 vector_product,a,b,c,c_norm,angle
 print,'(single) c=',c    ,' angle=',angle,format='(a,3f8.4,a,f8.4)'
 print,'(array)  c=',c_[i,0],c_[i,1],c_[i,2],' angle=',angle_[i],$
  format='(a,3f8.4,a,f8.4)'
 print,'_________________________________'
endfor
end
