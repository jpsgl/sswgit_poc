pro euvi_goes_read,time1,time2,time_goes,goes_low,goes_high

;+
; Project     : STEREO
;
; Name        : EUVI_GOES
;
; Category    : Database management
;
; Explanation : This routine reads GOES data or returns default start/end arrays 
;		if GOES data not found.
; 		This routine is called by EUVI_GOES.PRO
;
; Syntax      : IDL> euvi_goes_read,time1,time2,time_goes,goes_low,goes_high
;
; Inputs      : time1	= start of time interval, format: time1='04-DEC-06 00:00:00'
;		time2	= end   of time interval, format: time2='04-DEC-06 23:59:59'
;
; Outputs     : time_goes = time array
;		goes_low  = GOES flux from low  energy channel 1-8 A
;		goes_high = GOES flux from high energy channel 0.5-4 A
;
; History     : 21-Feb-2007, Version 1 written by Markus J. Aschwanden
;	        14-Feb-2007, add keyword inheritance _extra to rd_gxd
;
; Contact     : aschwanden@lmsal.com
;-

time_goes=[anytim(time1,/seconds),anytim(time2,/seconds)] mod 86400
goes_low =[0.,0.] ;if GOES data do not exist or not found
goes_high=[0.,0.]

rd_gxd,time1,time2,gdata,status=status
;print,status &help,gdata
;if (status ne 0) then rd_gxd,time1,time2,gdata,status=status,/goes10
;if (status ne 0) then rd_gxd,time1,time2,gdata,status=status,/goes11
;if (status ne 0) then rd_gxd,time1,time2,gdata,status=status,/goes12
;if (status ne 0) then rd_gxd,time1,time2,gdata,status=status,/goes13
;if (status ne 0) then rd_gxd,time1,time2,gdata,status=status,/goes14
;if (status ne 0) then rd_gxd,time1,time2,gdata,status=status,/goes15
 print,status &help,gdata
 if (status eq 0) then begin
 time_goes=(gdata.time)/1000. mod 86400
 goes_low=gdata.lo
 goes_high=gdata.hi
 endif
end
