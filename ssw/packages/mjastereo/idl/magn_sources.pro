pro magn_sources,modelfile,runfile,para
;+
; Project     : AIA
;
; Name        : MAGN_SOURCES
;
; Category    : SOHO/MDI and SDO/HMI data analysis
;
; Explanation : reads HMI magnetogram image and decomposes into 
;		NS Gaussian components of unipolar magnetic charges
;		and stores coefficients in file *_coeff.dat
;
; Syntax      : IDL> mag_sources,modelfile,runfile,para
;
; Inputs      : modelfile = filename of magnetogram in *_field.sav
; 		            --> bzmap,x,y,field_lines,alpha,q_scale
;
; Outputs     ; runfile   = output file *_coeff.dat
;                           --> b1[ns],x1[ns],y1[ns],z1[ns],a1[ns]
;
; History     : 16-May-2011, Version 1 written by Markus J. Aschwanden
;		30-Jan-2012, Version 2 released to SSW
;
; Contact     : aschwanden@lmsal.com
;-

common powell_var,image_local,x_local,y_local,nlocal,coeff,icomp

print,'________________MAGN_SOURCES________________'
nmag    =para[2]
nsm     =para[3]
q_mag	=para[15]
iopt	=para[17]

savefile2=modelfile+'_field.sav'
restore,savefile2   ;-->bzmap,x,y,field_lines,alpha,q_scale
nx	=n_elements(x)
ny	=n_elements(y)
x1	=x[0]
x2	=x[nx-1]
y1	=y[0]
y2	=y[ny-1]
fov	=[x1,y1,x2,y2]
dpix	=x[1]-x[0]

;Gaussian width of max magnetic field component..................
bmax	=max(abs(bzmap),im)
iy	=long(im/long(nx))	
result  =gaussfit(x,abs(bzmap(*,iy)),a,nterms=3)
nwpix	=abs(a(2)/dpix)
wfit	=nx/4
if (wfit le nx/4) then wfit=(long(nwpix+0.5)>2) 	

;smoothing of magnetogram........................................
if (nsm lt 2) then image_smo=bzmap
if (nsm ge 2) then image_smo=smooth(bzmap,nsm)
image_mdi_sub=float(image_smo)

;Gaussian integral...............................................
nsig    =wfit
sig_    =0.5*(1+findgen(nsig))
qsig_   =fltarr(nsig)
nfit    =wfit*2+1
for isig=0,nsig-1 do begin
 qsig_(isig)=0.
 for ix=-wfit,wfit do begin
  for iy=-wfit,wfit do begin
   qsig_(isig)=qsig_(isig)+exp(-(ix^2+iy^2)/(2.*sig_(isig)^2))/float(nfit)^2
  endfor
 endfor
endfor

;Decomposition of MDI subimage..............................
coeff	=fltarr(nmag,4)
polar	=[+1.,-1]
;print,'  IS       IPOL     M       X       Y       Z      -H '
magn	=max(abs(image_mdi_sub))		;max field strength
image_sub=float(image_mdi_sub)			
qsig	=sqrt(2.*alog(2.))			
icomp	=0
ns	=long(nmag-1)/2
ipix	=lonarr(nmag,7)
for is=0,ns do begin
npol	=2
if (nmag eq 1) then npol=1
for ipol=0,npol-1 do begin
 if (ipol eq 0) then b0=max(image_sub,im)
 if (ipol eq 1) then b0=min(image_sub,im)
 iy	=long(im/long(nx))	
 ix	=im-iy*long(nx)
 bz	=abs(image_sub(ix,iy))
 i3	=(ix-wfit)>0
 i4	=(ix+wfit)<(nx-1)
 j3	=(iy-wfit)>0
 j4	=(iy+wfit)<(ny-1)
 n34	=(i4-i3+1)*(j4-j3+1)
 ipix[icomp,*]=[ix,i3,i4,iy,j3,j4,n34]
 xp	=x[ix]  	
 yp	=y[iy] 		
 zp	=sqrt(1.-xp^2-yp^2)
 polar	=image_sub(ix,iy)/abs(image_sub(ix,iy))
 qint   =total(image_sub(i3:i4,j3:j4))/(bz*float(n34)*polar)
 sig	=interpol(sig_,qsig_,qint)		;interpolation 
 wp	=sig*qsig*dpix			        ;HW in solar radii 		
 aia_mag_inversion,bz,xp,yp,zp,wp,bm,xm,ym,zm
 bm	=bm*polar
 coeff(icomp,0)=bm
 coeff(icomp,1)=xm
 coeff(icomp,2)=ym
 coeff(icomp,3)=zm
 aia_mag_map,coeff[icomp,*],1,x,y,model_coeff
 image_sub=image_sub-model_coeff
 icomp	=icomp+1
 bres	=max(abs(image_sub))
 qres	=bres/bmax
 if (qres le q_mag) then goto,end_decomp
endfor
endfor
END_DECOMP:
nmag	=icomp
coeff_new=fltarr(nmag,4)
for j=0,3 do coeff_new(0:nmag-1,j)=coeff(0:nmag-1,j)
coeff	=coeff_new

;---------------------POWELL OPTIMIZATION--------------------------
if (iopt ge 1) then begin
 for iter=0,iopt-1 do begin
 print,'POWELL OPTIMIZATION Iteration = ',iter+1,'/',iopt
 for icomp=0,nmag-1 do begin
  ix	=ipix[icomp,0]
  i3	=ipix[icomp,1]
  i4	=ipix[icomp,2]
  iy	=ipix[icomp,3]
  j3	=ipix[icomp,4]
  j4	=ipix[icomp,5]
  n34	=ipix[icomp,6]
  image_local=float(image_mdi_sub[i3:i4,j3:j4])
  x_local=x[i3:i4]
  y_local=y[j3:j4] 
  nlocal=n34
  am	=0.0
  coeff_m=fltarr(4)
  for i=0,3 do coeff_m[i]=reform(coeff[icomp,i])
  vv	=fltarr(4,4)
  vv[0,0]=bm*0.01
  for i=1,3 do vv[i,i]=dpix
  ftol	=1.0e-5
  powell,coeff_m,vv,ftol,dev,'magn_sources_powell'
   coeff(icomp,0)=coeff_m[0]
   coeff(icomp,1)=coeff_m[1]
   coeff(icomp,2)=coeff_m[2]
   coeff(icomp,3)=coeff_m[3]
  if (coeff_m[3] lt 0.0) or (coeff_m[3] ge 1.0) then $
    coeff[icomp,*]=coeff_new[icomp,*]	   ;reset to old values if z is bad
 endfor
 endfor
endif

;iterative superposition of Gaussian (unipolar/dipole) magnetic charges
aia_mag_map,coeff,1.,x,y,model
diff_image=bzmap-model
statistic,diff_image,zav,zsig
print,'Number of magnetic components = ',nmag
print,'Residual statistic            = ',zav,zsig

;write file with components______________________________________________
coeff_file=runfile+'_coeff.dat'
openw,2,coeff_file
for is=0,nmag-1 do begin
 m1	=coeff(is,0)
 x1	=coeff(is,1)
 y1	=coeff(is,2)
 z1	=coeff(is,3)
 a1	=0.
 printf,2,m1,x1,y1,z1,a1,format='(5f12.5)'
;print   ,m1,x1,y1,z1,a1,format='(5f12.5)'
endfor
close,2

;display HMI FOV image__________________________________________________
zoom	=long(600/(2.*nx))>1
window,0,xsize=nx*2*zoom,ysize=ny*2*zoom
gauss	=magn*0.5
loadct,0
tv,bytscl(rebin(bzmap,nx*zoom,ny*zoom) ,min=-gauss,max=+gauss),0,ny*zoom
tv,bytscl(rebin(model,nx*zoom,ny*zoom),min=-gauss,max=+gauss),0,0
tv,bytscl(rebin(diff_image,nx*zoom,ny*zoom),min=-gauss,max=+gauss),nx*zoom,ny*zoom
!noeras=1
!p.position=[0.,0.5,0.5,1.]
!p.title='East-West scan'
!x.style=1
!y.style=1
!x.range=[fov(0),fov(2)]
!y.range=[fov(1),fov(3)]
!p.position=[0.55,0.05,0.95,0.45]
!x.range=minmax(x)
!y.range=[-2,2]*magn
plot,!x.range,[0,0],linestyle=1
zmax	=max(bzmap,im)
iy	=long(im/long(nx))	
image_model=model(*,iy)
oplot,x,image_model,thick=3
oplot,x,bzmap(*,iy),color=150
end

