pro euvi_columndepth,x,r0,temp,dz_,dz,lam

;estimates column depth for positions x for corona with scale height temp

n	=n_elements(x)
lam     =r0*(47.*temp/696.) 				;scale height in pixel
ind1	=where(abs(x) lt r0,n1)
ind2	=where((abs(x) ge r0) and (abs(x) le r0+lam),n2)
dz_	=fltarr(n)
r1	=r0+lam
if (n1 ge 1) then dz_(ind1)=sqrt(r1^2-x(ind1)^2)-sqrt(r0^2-x(ind1)^2) 
if (n2 ge 1) then dz_(ind2)=2.*sqrt(r1^2-x(ind2)^2)
dz	=total(dz_)/float(n)
end
