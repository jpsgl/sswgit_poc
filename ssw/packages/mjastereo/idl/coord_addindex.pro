pro coord_addindex,index
;+
; Project     : ALL 
;
; Name        : COORD_ADDINDEX
;
; Purpose     : complete solar ephemerides in INDEX
;
; Category    : Coordinate systems, Utility 
;
; Explanation : Adds ephemerides R0, L0, B0, P into
;		fields .solar_r, solar_l0, solar_b0, solar_p
;
; Syntax      : IDL> fig_open,io,form,char 
;
; Examples    : IDL>coord_addindex,index
;
; Inputs      : index	-  structure 
;
; Opt. Inputs : None
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Common      : None
;
; Restrictions: Millenium crisis in ephemeris values at 2000-Jan-01
;		(probably created by subroutine GET_SUN)
;
; Side effects: None
;
; History     : Version 1,  9-OCT-1998,  Markus Aschwanden.  Written
;
; Contact     : aschwand@lmsal.com
;-

;converts index into solarsoft standard format
index=struct2ssw(index)


timedate=index.date_obs
eph     =get_sun(timedate)
r       =eph(1)         ;solar radius in arcseconds
l0      =eph(10)        ;heliographic longitude of solar disk
b0      =eph(11)        ;heliographic latitude of solar disk
p       =eph(12)        ;position angle of solar axis
crota2  =-p 		;assumed to be rotated to solar north

index=add_tag(index,r ,'solar_r')
index.solar_r =r
print,'COORD_ADDINDEX: add solar radius, INDEX.SOLAR_R = ',r

index=add_tag(index,l0,'solar_l0')
index.solar_l0=l0
print,'COORD_ADDINDEX: add heliogr.long. INDEX.SOLAR_L0 = ',l0

index=add_tag(index,b0,'solar_b0')
index.solar_b0=b0
print,'COORD_ADDINDEX: add heliogr.lat   INDEX.SOLAR_B0 = ',b0

index=add_tag(index,p ,'solar_p')
index.solar_p =p
print,'COORD_ADDINDEX: add position angle  INDEX.SOLAR_P = ',p

index=add_tag(index,crota2,'crota2')
index.crota2=crota2
print,'COORD_ADDINDEX: add roll rotation angle  INDEX.CROTA2 = ',crota2
print,'  (assuming that the image is rotated to solar North)'

end
