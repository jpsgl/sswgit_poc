pro aia_loop_autodisp,fileset,vers,te_range,chimax,io
;+
; Project     : AIA/SDO
;
; Name        : AIA_LOOP_AUTO
;
; Category    : Data analysis   
;
; Explanation : Statistics display of automated loop EM and Te modeling
;
; Syntax      : IDL>aia_loop_autodisp,cubefile,te_range,chimax,io
;
; Inputs      : fileset  = filename containing EM and Te values
;               vers     = label in filename (e.g., vers='a')
;               te_range(2) = valid DEM temperature ragne
;               chimax   = maximum acceptable chi-square
;		io	 = output format (0=screen, 1=postscript file)
;
; Outputs     : postscript file: auto_loop_autodisp.ps
;
; History     : 09-Mar-2011, Version 1 written by Markus J. Aschwanden
;               11-Jun-2011, add FILESET and VERS to define filename
;
; Contact     : aschwanden@lmsal.com
;-

;____________________LOOP SEGMENT COUNTER___________________________
cubefile=fileset+vers+'_teem_cube.sav'
restore,cubefile		;-->cube,loop_par
n	=n_elements(loop_par(*,0))
log_em =loop_par(*,2) 		;log(EM)
log_te =loop_par(*,3) 		;log(T)
log_sig=loop_par(*,4) 		;log(sig_T)
chi    =loop_par(*,5)		;chi2
w_cm   =loop_par(*,6)		;wid[cm]
ne_cm3 =loop_par(*,7)		;ne[cm-3]
log_ne =alog10(ne_cm3)
igood  =where((chi ne 0) and (chi le chimax),ngood)

;_______________________DISPLAY_____________________________________
form    =1      ;0=landscape, 1=portrait
char    =1.0    ;character size
fignr	=''
plotname='aia_loop_autodisp'
ref     =''     ;label at bottom of Fig.
unit    =0      ;window number
fig_open,io,form,char,fignr,plotname,unit

!p.position=[0.08,0.70,0.48,0.95]
plot,log_te(igood),log_ne(igood),psym=1,symsize=0.5,xrange=[5.5,7.0],yrange=[8,11],xtitle='Temperature  log(T)',ytitle='Density  n(cm!U-3!N)'

;RTV model
log_te_=5.5+2.0*findgen(100)/100
te0	=1.e6
l1	=1.e9
l2	=1.e10
kb	=1.38e-16
ne1	=(te0^2/l1)/(2.*1400.^3.*kb)
ne2	=(te0^2/l2)/(2.*1400.^3.*kb)
te	=10.^log_te_
log_ne1	=alog10(ne1*(te/te0)^2)
log_ne2	=alog10(ne2*(te/te0)^2)
oplot,log_te_,log_ne1
oplot,log_te_,log_ne2
xyouts,5.55,8.3,'L=10!U9!N cm',orient=40
xyouts,5.95,8.1,'L=10!U10!N cm',orient=40
xyouts,6.10,8.1,'(RTV)',orient=40
!noeras=1

!p.position=[0.55,0.70,0.95,0.95]
x	=chi
x1	=0.0
x2	=5.0
nh	=25
dh	=(x2-x1)/float(nh)
xh	=x1+dh*(findgen(nh)+0.5)
yh	=histogram(x,min=x1,max=x2,bin=dh)
plot,xh,yh,psym=10,xtitle='Goodness-of-fit  !9c!3!U2!N',ytitle='Number'
oplot,[1,1],!y.crange,linestyle=1
oplot,[1,1]*chimax,!y.crange
xyouts_norm,0.6,0.9,'N='+string(n,'(I6)'),char
statistic,chi,avg,sig
xyouts_norm,0.6,0.8,'!9c!3!U2!N='+string(avg,'(f6.1)')+'+',char
xyouts_norm,0.6,0.8,'!9c!3!U2!N='+string(avg,'(f6.1)')+'_'+$
                             string(sig,'(f6.1)'),char

!p.position=[0.08,0.40,0.48,0.65]
x	=log_te(igood)
x1	=5.5
x2	=7.0
nh	=15
dh	=(x2-x1)/float(nh)
xh	=x1+dh*(findgen(nh)+0.5)
yh	=histogram(x,min=x1,max=x2,bin=dh)
plot,xh,yh,psym=10,xtitle='Temperature  log(T)',ytitle='Number'
xyouts_norm,0.6,0.9,'N='+string(ngood,'(I6)'),char

!p.position=[0.55,0.40,0.95,0.65]
x	=log_ne(igood)
x1	=8.0
x2	=12.0
nh	=24
dh	=(x2-x1)/float(nh)
xh	=x1+dh*(findgen(nh)+0.5)
yh	=histogram(x,min=x1,max=x2,bin=dh)
plot,xh,yh,psym=10,xtitle='Electron density  log(n)',ytitle='Number'
xyouts_norm,0.6,0.9,'N='+string(ngood,'(I6)'),char
statistic,x,avg,sig
xyouts_norm,0.6,0.8,'n!De!N='+string(avg,'(f5.2)')+'+',char
xyouts_norm,0.6,0.8,'n!De!N='+string(avg,'(f5.2)')+'_'+$
                             string(sig,'(f5.2)'),char

!p.position=[0.08,0.10,0.48,0.35]
x	=log_sig(igood)
x1	=0.0
x2	=1.0
nh	=10
dh	=(x2-x1)/float(nh)
xh	=x1+dh*(findgen(nh)+0.5)
yh	=histogram(x,min=x1,max=x2,bin=dh)
plot,xh,yh,psym=10,xtitle='Temperature Gaussian width  log(!9s!3!DT!N)',ytitle='Number'
xyouts_norm,0.6,0.9,'N='+string(ngood,'(I6)'),char
statistic,x,avg,sig
xyouts_norm,0.6,0.8,'!9s!3!DT!N='+string(avg,'(f5.2)')+'+',char
xyouts_norm,0.6,0.8,'!9s!3!DT!N='+string(avg,'(f5.2)')+'_'+$
                             string(sig,'(f5.2)'),char

!p.position=[0.55,0.10,0.95,0.35]
x	=w_cm(igood)/1.e8
x1	=0.0
x2	=10.0
nh	=20
dh	=(x2-x1)/float(nh)
xh	=x1+dh*(findgen(nh)+0.5)
yh	=histogram(x,min=x1,max=x2,bin=dh)
plot,xh,yh,psym=10,xtitle='Loop width  w[Mm]',ytitle='Number'
xyouts_norm,0.6,0.9,'N='+string(ngood,'(I6)'),char
oplot,[1,1]*0.6*0.916*2.0,!y.crange,linestyle=1
statistic,x,avg,sig
xyouts_norm,0.6,0.8,'w='+string(avg,'(f6.1)')+'+',char
xyouts_norm,0.6,0.8,'w='+string(avg,'(f6.1)')+'_'+$
                             string(sig,'(f6.1)'),char

fig_close,io,fignr,ref
end
