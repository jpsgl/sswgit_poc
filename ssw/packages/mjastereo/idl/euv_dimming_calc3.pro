pro euv_dimming_calc3,input,dir,catfile,run,iflare,test,dem
;+
; Project     : SDO/AIA,  
;
; Name        : EUV_DIMMING_DISP 
;
; Category    : data analysis of EUV dimming and CME mass, speed, and energy
;
; Explanation : Reads AIA images and performs EUV dimming analysis
;
; Syntax      : IDL>euv_dimming_disp,input,catfile,dir,iflare
;
; Inputs      : dir,savefile
;
; Outputs     ; IDL save file: *.sav
;
; History     : 24-Feb-2016, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

print,'__________________EUV_DIMMING_CALC3_________________________
readcol,dir+catfile,event_,dateobs_,noaa_,helpos_,instr_,$
  w1_,w2_,w3_,w4_,w5_,w6_,w7_,w8_,fov_,$
  format='(I,A,A,A,A,A,A,A,A,A,A,A,A,A)',/silent
nt	=n_elements(event_)
i	=0
wave8   =[w1_(i),w2_(i),w3_(i),w4_(i),w5_(i),w6_(i),w7_(i),w8_(i)]
indw    =where(wave8 ne 0,nwave)
wave_   =strtrim(string(wave8(indw),'(I5)'),2)
tmargin1=input.tmargin1                 ;s
tmargin2=input.tmargin2
rise    =input.rise
decay   =input.decay
duration=rise+decay
cadence =input.cadence
it_start=long(tmargin1/cadence)
it_peak =long((tmargin1+rise)/cadence)
it_end  =long((tmargin1+rise+decay)/cadence)
helpos	=input.helpos
lon	=long(strmid(helpos,4,2))
lat	=long(strmid(helpos,1,2))
l	=lon*!pi/180.
b	=lat*!pi/180.
angle	=sqrt(l^2+b^2)
r0	=6.96e10 			;cm
d0	=r0*sin(angle)			;distance of CME lauch from disk center

;________________________AIA RESPONSE FUNCTION____________________
teem_table='teem_table.sav'
te_range=[0.5,20]*1.e6		;[K] valid temperature range of DEM 
tsig    =0.1*(1+findgen(10))	;values of Gaussian log temp widths
tresp   =aia_get_response(/temp,/full,/dn)
telog_  =tresp.logte
telog1  =alog10(te_range(0))
telog2  =alog10(te_range(1))
ind_te  =where((telog_ ge telog1) and (telog_ le telog2),nte)
telog   =telog_(ind_te)
ichan_  =fltarr(nwave)
resp    =fltarr(nte,nwave)
for iw=0,nwave-1 do begin
 filter ='A'+wave_(iw)
 if (wave_(iw) eq '094') or (wave_(iw) eq '94') then filter='A94'
 ichan  =where(tresp.channels eq filter)
 resp_  =tresp.tresp(*,ichan)
 resp(*,iw)=resp_(ind_te)
endfor

;_____________________CALCULATES LOOPUP TABLES____________________
if (dem eq 1) then begin
 dte1    =10.^telog(1:nte-1)-10.^telog(0:nte-2)
 dte     =[dte1(0),dte1]
 em1     =1.
 nsig    =n_elements(tsig)
 flux    =fltarr(nte,nsig,nwave)
 for i=0,nte-1 do begin
  for j=0,nsig-1 do begin
   em_te =em1*exp(-(telog-telog(i))^2/(2.*tsig(j)^2))
   for iw=0,nwave-1 do flux(i,j,iw)=total(resp(*,iw)*em_te*dte) 
  endfor
 endfor
 save,filename=teem_table,wave_,resp,telog,dte,tsig,flux
 print,'Lookup table created : ',teem_table
endif
restore,teem_table      	;-->wave_,resp,telog,dte,tsig,flux

;__________________FLUX GRID DATA_________________________
time	  =fltarr(nt)
flux_prof =fltarr(nt,nwave)
emtot_prof=fltarr(nt)
telog_prof=fltarr(nt)
tsig_prof =fltarr(nt)
chi_prof  =fltarr(nt)
wid_prof  =fltarr(nt)
for it=0,nt-1 do begin
 print,'time step = ',it,nt-1
 nr_it	=it+1 
 ind     =where((event_) eq nr_it,nind)
 if (nind le 0) then stop,'NLFFF_EVENT : NO MATCHING EVENT FOUND'
 i       =ind(0)                 ;selected entry
 string0,3,nr_it,nr_str
 hh	 =strmid(dateobs_(i), 9,2)
 mm	 =strmid(dateobs_(i),11,2)
 ss	 =strmid(dateobs_(i),13,2)
 time(it)=hh+mm/60.+ss/3600.
 if (it ge 1) then $			;midnight jump 
  if (time(it) lt time(it-1)) then time(it)=time(it)+24.
 savefile     =dateobs_(i)+'_'+instr_(i)+'_'+nr_str+'_'+run+'.sav'

;__________________FLUX PROFILES____________________________________
 restore,dir+savefile    	;--> PARA,FLUX_GRID(NX,NY,NWAVE)
 for iwave=0,nwave-1 do flux_prof(it,iwave)=avg(flux_grid(*,*,iwave))
 if (it eq 0) then begin
  dim	=size(flux_grid)
  nx	=dim(1)
  ny	=dim(2)
  em_xy	  =fltarr(nx,ny,nt)
  telog_xy=fltarr(nx,ny,nt)
  tsig_xy =fltarr(nx,ny,nt)
  chi2	  =fltarr(nx,ny,nt)
 endif

;________________DEM ANALYSIS________________________________
 dim    =size(flux)		;precalculated TE+EM loopup table
 nte    =dim[1]
 nsig   =dim[2]
 nwave  =dim[3]
 nfree  =3
 for i=0,nx-1 do begin			;x,y grid
  for j=0,ny-1 do begin
   em_best =0.
   telog_best=0.
   tsig_best=0.
   flux_obs=reform(flux_grid(i,j,*)) 		;flux per second
   counts=flux_obs*exptime_
   noise =sqrt(counts/exptime_)
   chi_best=9999.
   for k=0,nte-1 do begin
    for l=0,nsig-1 do begin
     flux_dem1=reform(flux(k,l,*))
     em1 =total(flux_obs)/total(flux_dem1)
     flux_dem=flux_dem1*em1
     ind	=where(flux_obs ne 0.,nind)
     chi =sqrt(total((flux_obs(ind)-flux_dem(ind))^2/noise(ind)^2)/(nind-nfree))
     if (chi le chi_best) then begin
      em_best    =em1
      telog_best =telog(k)
      tsig_best  =tsig(l)
      chi_best   =chi
     endif
    endfor
   endfor
   em_xy(i,j,it)   =em_best
   telog_xy(i,j,it)=telog_best
   tsig_xy(i,j,it) =tsig_best
   chi2(i,j,it)    =chi_best
  endfor						;x location loop
 endfor							;y location loop
endfor							;time step loop 
 
;_________________SPATIAL SYNTHESIS OF DEM____________________________________
dem_t  =fltarr(nte,nt)
dem_grid=fltarr(nx,ny,nt)
ngrid  =float(nx*ny)
chi_med	=fltarr(nt)
for it=0,nt-1 do begin
 for j=0,ny-1 do begin
  for i=0,nx-1 do begin
   em_best    =em_xy(i,j,it)
   telog_best =telog_xy(i,j,it)
   tsig_best  =tsig_xy(i,j,it) 
   em_kelvin  =em_best*exp(-(telog-telog_best)^2/(2.*tsig_best^2))/ngrid
   dem_grid(i,j,it)=total(em_kelvin*dte)		;DEM localized x,y
   dem_t(*,it)=dem_t(*,it)+em_kelvin		        ;DEM summed x,y	
  endfor
 endfor
 emtot_prof(it)=total(dem_t(*,it)*dte)			;EM [cm-5]
 dem_max=max(dem_t(*,it),imax)				;EM [cm-5 K-1]
 telog_prof(it)=telog(imax)				;T[MK]
 chi_med(it)=median(chi2(*,*,it))
endfor

;__________________ELIMINATE WORST DEM FITS_____________________
chi_bad	=5.0
ind_bad	=where(chi_med ge chi_bad,nbad)
if (nbad ge 1) then begin
 for i=0,nbad-1 do begin
  it	  =ind_bad(i)
  it1     =(it-1) 	&if (it eq 0   ) then it1=1 
  it2     =(it+1)       &if (it eq nt-1) then it2=nt-2
  emtot_prof(it)  =(emtot_prof(it1)+emtot_prof(it2))/2.
  dem_grid(*,*,it)=(dem_grid(*,*,it1)+dem_grid(*,*,it2))/2.
  dem_t(*,it)=(dem_t(*,it1)+dem_t(*,it2))/2.
  telog_xy(*,*,it)=(telog_xy(*,*,it1)+telog_xy(*,*,it2))/2.
 endfor
 print,'Eliminate time with bad fit  it_bad=',it
endif

;_________________LOCALIZED DIMMING____________________________
qmin   =0.15
epeak  =max(emtot_prof,it_peak)
x_model_grid=fltarr(nx,ny,nt)
x_model=fltarr(nt)
em_model=fltarr(nt)
npeak_t =fltarr(nt)
mask_xy =fltarr(nx,ny)
for j=0,ny-1 do begin
 for i=0,nx-1 do begin
  emax_grid=max(dem_grid(i,j,*),it_max)
  emin_grid=min(dem_grid(i,j,it_max:nt-1),dit_min)
  npeak_t(it_max)=npeak_t(it_max)+1
  mask_xy(i,j)=it_max
  te_mk =10.^(median(telog_xy(i,j,0:it_max)))/1.e6
  h0	=4.7e9*te_mk						;[cm] density scale height 
  q	=(reform(dem_grid(i,j,*)-emin_grid)/(emax_grid-emin_grid)) > 1.e-3  
  x_model_grid(i,j,0:it_max)=h0 
  for it=it_max+1,nt-1 do begin
   xq1	=r0*((((1.+h0/r0)^3-1.)/q(it)+1.)^(1./3.)-1.)
   xq0	=2.0*x_model_grid(i,j,it-1)-x_model_grid(i,j,it-2)	;1st order extrapolation
   x_model_grid(i,j,it)=xq1 > xq0
   if (q(it) lt qmin) then x_model_grid(i,j,it)=xq0
  endfor
 endfor
endfor
npeakmax=max(npeak_t(it_start+2:it_end),itm)
it_peak	=it_start+2+itm
window,1,xsize=512,ysize=512
clearplot
plot,time,npeak_t,psym=10
oplot,time(it_peak)*[1,1],!y.crange,thick=3

ngood   =0
it_tol	=3		;tolerance of time steps for EM peak
for j=0,ny-1 do begin
 for i=0,nx-1 do begin
  emax_grid=max(dem_grid(i,j,*),it_max)
  if (it_max ge it_peak-it_tol) and (it_max le it_peak+it_tol) then begin
   x_model =x_model  + reform(x_model_grid(i,j,*))
   em_model=em_model + reform(dem_grid(i,j,*))
   ngood=ngood+1
  endif
 endfor
endfor
x_model=x_model/float(ngood)
em_model=em_model/float(ngood)
x_model=x_model+d0					;flare location offset
print,'Number of averaged EM profiles = ',ngood,nx*ny,float(ngood)/float(nx*ny)
it_max	=it_peak

;_________________CME VELOCITY AND ACCELERATION___________________
a_model	  =fltarr(nt)
v_model	  =fltarr(nt)
dt	=(time(1)-time(0))*3600.			;[s] time step
for it=1,nt-2 do begin
 v_model(it)=(x_model(it+1)-x_model(it-1))/(2.*dt)
endfor
v_model(nt-1)=v_model(nt-2)
v_model(0)=2.*v_model(1)-v_model(2)
for it=1,nt-2 do begin
 a_model(it)=(v_model(it+1)-v_model(it-1))/(2.*dt)
endfor
a_model(nt-1)=2.*a_model(nt-2)-a_model(nt-3)
a_model(0)=2.*a_model(1)-a_model(2)

;__________________LASCO/C2 TIME AND VELOCITY_____________________
readcol,'lasco.dat',flare,dateobs_lasco,h_lasco,v1_lasco,v2_lasco,v3_lasco,$
        m_lasco,e_lasco,c,skipline=1,format='(I,A,F,I,I,I,F,F,A)'
ind0    =where(flare eq iflare,n)
i       =ind0(0)
d_lasco =h_lasco(i)
x_lasco =r0*h_lasco(i)				;cm
ind1    =where(x_model ge x_lasco,n_lasco)	
if (max(x_model) lt x_lasco) then ind1=[nt-1]
v_lasco =v_model(ind1(0))			;cm/s
t_lasco =time(ind1(0))*3600.			;s
 
;_________________DIMMING AREA_________________________________
emax	=max(emtot_prof,it_max)
emin	=min(emtot_prof(it_max:nt-1),dit_min)
em_tot  =emax-emin 
e1_xy   =em_xy(*,*,it_max)
e2_xy	=em_xy(*,*,nt-1)
de_med	=median(e1_xy-e2_xy)
e3_xy	=e1_xy-de_med*2.
ind_dimm=where(e2_xy lt e3_xy,npix_dimm)
q_dimm	=float(npix_dimm)/float(nx*ny)
wid_dimm=sqrt(float(npix_dimm))		;width in units of grid bins 
l_cm 	=wid_dimm*dx_grid_cm		;cm 
area	=l_cm^2				;equivalent area [cm>2]
vol	=l_cm^3				;volume [cm3]
angle   =90.*(l_cm/(r0*!pi/2.))		;angular width [deg]

;_________________DIMMING MASS AND KINETIC ENERGY________________
nel	=sqrt(em_tot/l_cm)
mp	=1.67e-24 			;proton mass
mass	=mp*nel*vol
ekin_model=(1./2.)*mass*v_lasco^2

;________________SAVE PARAMETERS______________________________
string0,3,iflare,nr_str
savefile2='dimming_'+nr_str+run+'.sav'
save,filename=dir+savefile2,input,para,exptime_,flux_grid,x_grid,y_grid,$
	flux_prof,emtot_prof,telog_prof,tsig_prof,chi_prof,wave_,time,dem_grid,$
        l_cm,angle,area,vol,em_tot,dem_t,nel,te_mk,telog,mass,time,em_model,$
	d0,x_lasco,v_lasco,t_lasco,a_model,v_model,x_model,x_model_grid,ekin_model
print,'parameters saved in file = ',savefile2
end
