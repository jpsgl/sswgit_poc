function euvi_hydro_fit_init,coeff

common powell_para,s_,hs,flux_obs,te_,resp_,flux_model,temp_model,area_h,par_model,temin,temax,hmax,rad_pix,frac_max

h		=hs>0
s		=s_
ns		=n_elements(s)
htop		=max(hs,itop) 
tchr		=0.01				;[MK]
rsun		=6.96				;solar radius [100 Mm]
te		= coeff(0) >tchr 	 	;loop top temp [MK]
lam		=(coeff(1)<rsun)>(-rsun)	;scale height [100 Mm]
l		=(s(itop)-s(0))>(s(ns-1)-s(itop))
lambda 		=(lam/rsun)*rad_pix 		;scale height [pixels]
cdelt1		=1.6
hchr		=2./(cdelt1*0.725)
s_l		=(s-hchr)/(l-hchr)
eps		=(tchr/te)^(7./2.)
temp_model	=tchr+(te-tchr)*( (s_l*(2.-s_l)) > eps )^(2./7.) 
em1_h		=exp(-2.*(h-hchr)/lambda) 	 
flux_profile	  =fltarr(ns,6)
fluxmin	 	 =0.001
w_sm		=7<(ns-1)
for iw=0,5 do begin
 resp  		=interpol(resp_(*,iw),te_,temp_model)>0 
 flux_profile(*,iw)=smooth(em1_h*resp,w_sm) 
endfor
noise		=2.
em		=min((flux_obs>noise)/(flux_profile>fluxmin))
flux_model	 =em*flux_profile
frac6		=fltarr(6)
for iw=0,5 do begin
 frac6(iw)=total(flux_model(*,iw)>fluxmin)/total(flux_obs(*,iw)>fluxmin)
endfor
frac_max	=max(frac6)
dev	        =1.-frac_max
return,dev
end
