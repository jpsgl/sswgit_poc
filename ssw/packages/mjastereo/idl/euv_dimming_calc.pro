pro euv_dimming_calc,input,dir,catfile,run,iflare,test,dem,single
;+
; Project     : SDO/AIA,  
;
; Name        : EUV_DIMMING_DISP 
;
; Category    : data analysis of EUV dimming and CME mass, speed, and energy
;
; Explanation : Reads AIA images and performs EUV dimming analysis
;
; Syntax      : IDL>euv_dimming_disp,input,catfile,dir,iflare
;
; Inputs      : dir,savefile
;
; Outputs     ; IDL save file: *.sav
;
; History     : 24-Feb-2016, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

common dimming,model,time,a_t,v_t,x_t,q_t,q_obs,h0,ind_fit,isum

print,'__________________EUV_DIMMING_CALC3_________________________
readcol,dir+catfile,event_,dateobs_,noaa_,helpos_,instr_,$
  w1_,w2_,w3_,w4_,w5_,w6_,w7_,w8_,fov_,$
  format='(I,A,A,A,A,A,A,A,A,A,A,A,A,A)',/silent
nt	=n_elements(event_)
i	=0
wave8   =[w1_(i),w2_(i),w3_(i),w4_(i),w5_(i),w6_(i),w7_(i),w8_(i)]
indw    =where(wave8 ne 0,nwave)
wave_   =strtrim(string(wave8(indw),'(I5)'),2)
tmargin1=input.tmargin1                 ;s
tmargin2=input.tmargin2
rise    =input.rise
decay   =input.decay
duration=rise+decay
cadence =input.cadence
it_start=long(tmargin1/cadence)
it_peak =long((tmargin1+rise)/cadence)
it_end  =long((tmargin1+rise+decay)/cadence)
helpos	=input.helpos
nbin	=input.nbin
lon	=long(strmid(helpos,4,2))
lat	=long(strmid(helpos,1,2))
l	=lon*!pi/180.
b	=lat*!pi/180.
angle	=sqrt(l^2+b^2)
r0	=6.96e10 			;cm
string0,3,iflare,iflare_str

;________________________AIA RESPONSE FUNCTION____________________
teem_table='teem_table.sav'
te_range=[0.5,20]*1.e6		;[K] valid temperature range of DEM 
tsig    =0.1*(1+findgen(10))	;values of Gaussian log temp widths
tresp   =aia_get_response(/temp,/full,/dn)
telog_  =tresp.logte
telog1  =alog10(te_range(0))
telog2  =alog10(te_range(1))
ind_te  =where((telog_ ge telog1) and (telog_ le telog2),nte)
telog   =telog_(ind_te)
ichan_  =fltarr(nwave)
resp    =fltarr(nte,nwave)
for iw=0,nwave-1 do begin
 filter ='A'+wave_(iw)
 if (wave_(iw) eq '094') or (wave_(iw) eq '94') then filter='A94'
 ichan  =where(tresp.channels eq filter)
 resp_  =tresp.tresp(*,ichan)
 resp(*,iw)=resp_(ind_te)
endfor

;_____________________CALCULATES LOOPUP TABLES____________________
if (dem eq 1) then begin
 dte1    =10.^telog(1:nte-1)-10.^telog(0:nte-2)
 dte     =[dte1(0),dte1]
 em1     =1.
 nsig    =n_elements(tsig)
 flux    =fltarr(nte,nsig,nwave)
 for i=0,nte-1 do begin
  for j=0,nsig-1 do begin
   em_te =em1*exp(-(telog-telog(i))^2/(2.*tsig(j)^2))
   for iw=0,nwave-1 do flux(i,j,iw)=total(resp(*,iw)*em_te*dte) 
  endfor
 endfor
 save,filename=teem_table,wave_,resp,telog,dte,tsig,flux
 print,'Lookup table created : ',teem_table
endif
restore,teem_table      	;-->wave_,resp,telog,dte,tsig,flux

;__________________FLUX GRID DATA_________________________
time	  =fltarr(nt)
flux_prof =fltarr(nt,nwave)
emtot_prof=fltarr(nt)
telog_prof=fltarr(nt)
tsig_prof =fltarr(nt)
chi_prof  =fltarr(nt)
wid_prof  =fltarr(nt)
for it=0,nt-1 do begin
 print,'time step = ',it,nt-1
 nr_it	=it+1 
 ind     =where((event_) eq nr_it,nind)
 if (nind le 0) then STOP,'NLFFF_EVENT : NO MATCHING EVENT FOUND'
 i       =ind(0)                 ;selected entry
 string0,3,nr_it,nr_str
 hh	 =strmid(dateobs_(i), 9,2)
 mm	 =strmid(dateobs_(i),11,2)
 ss	 =strmid(dateobs_(i),13,2)
 time(it)=hh*3600.+mm*60.+ss		;[s]
 if (it ge 1) then $			;midnight jump 
  if (time(it) lt time(it-1)) then time(it)=time(it)+24.*3600.
 savefile =dateobs_(i)+'_'+instr_(i)+'_'+nr_str+'_'+run+'.sav'

;__________________FLUX PROFILES____________________________________
 restore,dir+savefile     	;--> PARA,FLUX_GRID(NX,NY,NWAVE)
 dim	=size(flux_grid)
 nx	=dim(1)
 ny	=dim(2)
 nwave	=dim(3)
 for iwave=0,nwave-1 do flux_prof(it,iwave)=avg(flux_grid(*,*,iwave))
 if (it eq 0) then begin
  dim	=size(flux_grid)
  nx	=dim(1)
  ny	=dim(2)
  em_xy	  =fltarr(nx,ny,nt)
  telog_xy=fltarr(nx,ny,nt)
  tsig_xy =fltarr(nx,ny,nt)
  chi2	  =fltarr(nx,ny,nt)
 endif

;________________DEM ANALYSIS________________________________
 dim    =size(flux)		;precalculated TE+EM loopup table
 nte    =dim[1]
 nsig   =dim[2]
 nwave  =dim[3]
 for i=0,nx-1 do begin			;x,y grid
  for j=0,ny-1 do begin
   em_best =0.
   telog_best=0.
   tsig_best=0.
   flux_obs=reform(flux_grid(i,j,*)) 		;flux per second
   counts=flux_obs*exptime_*nbin^2
   noise =sqrt(counts/exptime_)
   chi_best=9999.
   for k=0,nte-1 do begin
    for l=0,nsig-1 do begin
     flux_dem1=reform(flux(k,l,*))
     em1 =total(flux_obs)/total(flux_dem1)
     flux_dem=flux_dem1*em1
     ind	=where((flux_obs ne 0.),nind)
     npar       =3 
     nfree	=(nind-npar)>3
     chi =sqrt(total((flux_obs(ind)-flux_dem(ind))^2/noise(ind)^2)/nfree)
     if (chi le chi_best) then begin
      em_best    =em1
      telog_best =telog(k)
      tsig_best  =tsig(l)
      chi_best   =chi
     endif
    endfor
   endfor
   em_xy(i,j,it)   =em_best
   telog_xy(i,j,it)=telog_best
   tsig_xy(i,j,it) =tsig_best
   chi2(i,j,it)    =chi_best
  endfor						;x location loop
 endfor							;y location loop
endfor							;time step loop 
 
;_________________SPATIAL SYNTHESIS OF DEM____________________________________
dem_t  =fltarr(nte,nt)
dem_grid=fltarr(nx,ny,nt)
ngrid  =float(nx*ny)
chi_med	=fltarr(nt)
for it=0,nt-1 do begin
 for j=0,ny-1 do begin
  for i=0,nx-1 do begin
   em_best    =em_xy(i,j,it)
   telog_best =telog_xy(i,j,it)
   tsig_best  =tsig_xy(i,j,it) 
   em_kelvin  =em_best*exp(-(telog-telog_best)^2/(2.*tsig_best^2))/ngrid
   dem_grid(i,j,it)=total(em_kelvin*dte)		;DEM localized x,y
   dem_t(*,it)=dem_t(*,it)+em_kelvin		        ;DEM summed x,y	
  endfor
 endfor
 emtot_prof(it)=total(dem_t(*,it)*dte)			;EM [cm-5]
 dem_max=max(dem_t(*,it),imax)				;EM [cm-5 K-1]
 telog_prof(it)=telog(imax)				;T[MK]
 chi_med(it)=median(chi2(*,*,it))
endfor

;__________________INTERPOLATE LOW_EM OUTLIERS__________________
q_out	=0.5
for it=1,nt-2 do begin
 emtot_avg=(emtot_prof(it-1)+emtot_prof(it+1))/2.
 if (emtot_prof(it) lt emtot_avg/2.) then emtot_prof(it)=emtot_avg
endfor

;__________________ELIMINATE WORST DEM FITS_____________________
chi_bad	=5.0
ind_bad	=where(chi_med ge chi_bad,nbad)
if (nbad ge 1) then begin
 for i=0,nbad-1 do begin
  it	  =ind_bad(i)
  it1     =(it-1) 	&if (it eq 0   ) then it1=1 
  it2     =(it+1)       &if (it eq nt-1) then it2=nt-2
  emtot_prof(it)  =(emtot_prof(it1)+emtot_prof(it2))/2.
  dem_grid(*,*,it)=(dem_grid(*,*,it1)+dem_grid(*,*,it2))/2.
  dem_t(*,it)=(dem_t(*,it1)+dem_t(*,it2))/2.
  telog_xy(*,*,it)=(telog_xy(*,*,it1)+telog_xy(*,*,it2))/2.
 endfor
 print,'Eliminate time with bad fit  it_bad=',it
endif
te_mk0  =10.^(median(telog_xy(*,*,0)))/1.e6
te_mk	=(te_mk0 > 1.0) < 2.5	;[1.0 MK] minimum preflare temperature
h0	=4.7e9*te_mk		;[cm] density scale height 

;_________________SELECTED LOCATION OF MAXIMUM DIMMING________________
n	=nx*ny
em_sum	=fltarr(nt)
em_all	=fltarr(nt)
em_peak	=max(emtot_prof,im)
dtime	=2.*(time(1)-time(0))
isum	=0
em_best =0.
for j=0,ny-1 do begin
 for i=0,nx-1 do begin
  dem_xy=reform(dem_grid(i,j,*))
  empeak=max(dem_xy,ip)
  em_all=em_all+dem_xy
  if (time(ip) ge time(im)-dtime) and (time(ip) le time(im)+dtime) then begin
   em_sum=em_sum+dem_xy
   isum	=isum+1
  endif
  if (max(dem_xy) ge em_best) then begin
   i_best=i
   j_best=j
   em_best=max(dem_xy)
  endif
 endfor
endfor
if (single eq 1) then em_sum=reform(dem_grid(i_best,j_best,*))

;__________________INTERPOLATE LOW_EM OUTLIERS__________________
q_out	=0.5
for it=1,nt-2 do begin
 em_avg=(em_sum(it-1)+em_sum(it+1))/2.
 if (em_sum(it) lt em_avg/2.) then em_sum(it)=em_avg
endfor

;___________________SELECT FITTING RANGE________________________
em_max	=max(em_sum,it_max)
em_min	=min(em_sum(it_max:nt-1),di)
it_min	=it_max+di
tfit	=900.	;1800.			;min 0.5 hour 
nfit	=long(it_min-it_max) > (tfit/cadence)
ind_fit	=long(it_max+findgen(nfit))
q_obs	=(em_sum-em_min)/(em_max-em_min)
em_obs	=em_min+(em_max-em_min)*q_obs
if (em_min eq em_max) then print,'EM_MIN=EM_MAX=',em_min

;_________________CME VELOCITY AND ACCELERATION___________
n_model	  =4				;4 model functions
a_model   =fltarr(nt,n_model)
v_model   =fltarr(nt,n_model)
x_model   =fltarr(nt,n_model)
em_model  =fltarr(nt,n_model)
a0_model  =fltarr(n_model)
t0_model  =fltarr(n_model)
ta_model  =fltarr(n_model)
qe_model  =fltarr(n_model)
ekin_model=fltarr(n_model)
chi_model =fltarr(n_model)
x_lasco   =fltarr(n_model)
v_lasco   =fltarr(n_model)
t_lasco   =fltarr(n_model)
dt        =time(1)-time(0)
t0        =time(it_max)           		;s
it        =findgen(nt)
for m=0,n_model-1 do begin
 model	=1 + (m mod 4) 
 tau    =1000.                                  ;s
 d0	=(r0+h0)*sin(angle)	                  ;distance from disk center
 v0     =3.0e7                  		;cm/s
 a0     =0.1*v0/dt
 qmin	=0.01
 coeff  =[a0/1.e5,tau/1.e3,t0/1.e3,qmin]
 ncoeff =n_elements(coeff)
 xi     =fltarr(ncoeff,ncoeff)
 for i=0,ncoeff-1 do xi(i,i)=1.0
 ftol   =1.e-4
 if (em_max gt em_min) then begin
  powell,coeff,xi,ftol,fmin,'euv_dimming_powell'
  chi    =euv_dimming_powell(coeff)   ;-->a_model,v_model,x_model,q_t
  acc 	=coeff(0)*1.e5
  tau    =coeff(1)*1.e3
  t0     =coeff(2)*1.e3                           ;s
  d0	=(r0+h0)*sin(angle)	                  ;distance from disk center
  a_model(*,m)=a_t
  v_model(*,m)=v_t
  x_model(*,m)=x_t+d0
  em_model(*,m)=em_min+(em_max-em_min)*q_t
  a0_model(m)=acc
  t0_model(m)=t0
  ta_model(m)=(t0+tau)		                 ;s
  chi_model(m)=chi
 endif
endfor

;__________________LASCO/C2 TIME AND VELOCITY_____________________
lasco_file='~/work_global/ecme_lasco.dat'
readcol,lasco_file,flare,dateobs_lasco,start_goes,start_lasco,dt_lasco,h_lasco,$
 v1_lasco,v2_lasco,v3_lasco,m_lasco,e_lasco,skipline=3,format='(I,A,A,A,F,F,I,I,I,F,F)'
ind0    =where(flare eq iflare,n)
i       =ind0(0)
d_lasco =h_lasco(i)
x_lasco =r0*h_lasco(i)				;cm
for m=0,n_model-1 do begin
 ind1    =where(x_model(*,m) ge x_lasco,n_lasco)	
 if (max(x_model(*,m)) lt x_lasco) then ind1=[nt-1]
 v_lasco(m)=v_model(ind1(0),m)			;cm/s
 t_lasco(m)=time(ind1(0))			;s
 print,m,v_lasco(m)/1.e5,' km/s'
endfor
 
;_________________DIMMING AREA_________________________________
em_tot  =em_max-em_min 
e1_xy   =em_xy(*,*,it_max)
e2_xy	=em_xy(*,*,nt-1)
de_med	=median(e1_xy-e2_xy)
e3_xy	=e1_xy-de_med*2.
ind_dimm=where(e2_xy lt e3_xy,npix_dimm)
q_dimm	=float(npix_dimm)/float(nx*ny)
wid_dimm=sqrt(float(npix_dimm))		;width in units of grid bins 
l_cm 	=wid_dimm*dx_grid_cm		;cm 
area	=l_cm^2				;equivalent area [cm>2]
vol	=l_cm^3				;volume [cm3]
angle   =90.*(l_cm/(r0*!pi/2.))		;angular width [deg]

;_________________DIMMING MASS AND KINETIC ENERGY________________
nel	=sqrt(em_tot/l_cm)
mp	=1.67e-24 			;proton mass
mass	=mp*nel*vol
ekin_model=(1./2.)*mass*v_lasco^2

;________________SAVE PARAMETERS______________________________
string0,3,iflare,iflare_str
savefile2='dimming_'+iflare_str+run+'.sav'
save,filename=dir+savefile2,input,para,exptime_,flux_grid,x_grid,y_grid,$
	flux_prof,emtot_prof,telog_prof,tsig_prof,chi_prof,wave_,time,dem_grid,$
        l_cm,angle,area,vol,em_tot,dem_t,nel,te_mk,telog,mass,em_model,$
	d0,x_lasco,v_lasco,t_lasco,a_model,v_model,x_model,ekin_model,$
        q_t,q_obs,em_obs,a0_model,t0_model,ta_model,chi_model,ind_fit
print,'parameters saved in file = ',savefile2
end
