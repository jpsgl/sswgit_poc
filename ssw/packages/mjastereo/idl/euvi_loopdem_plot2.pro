pro euvi_loopdem_plot2,io,filename,isc,iloop,backgr,w_pix,fracmin

;_______________________INPUT______________________________
wave_	=['171','195','284'] ;Angstrom
sc_	=['A','B']
imagefiles=filename+wave_+'.sav'
loopfile =filename+'xyz.dat'  
s_iloop =strtrim(string(iloop,'(I3)'),2)
if (backgr eq -2) then s_backgr='__'
if (backgr eq -1) then s_backgr='_'
if (backgr ge 0) then s_backgr=string(backgr,'(I1)')
s_w    =strtrim(string(w_pix,'(I3)'),2)
fignr0	 =s_iloop+s_w+s_backgr
plotname=filename+'dem_'
outfile	 =plotname+fignr0+'.sav'
outfiles =findfile(outfile,count=nfound)
if (nfound eq 0) then euvi_loopdem,imagefiles,loopfile,iloop,backgr,w_pix,outfile
restore,outfile

;________________________PLOT_______________________________
form    =1      ;0=landscape, 1=portrait
char    =1.     ;character size
fignr	 =fignr0+sc_(isc)+'b'
ref     =''	;(Aschwanden et al. 2007)'  ;label at bottom of Fig.
unit    =1+2*isc;window number
ct	=3	;color table
fig_open,io,form,char,fignr,plotname,unit
loadct,ct
for iw=0,2 do begin
 ff     =reform(f(*,*,isc,iw))
 gg     =reform(g(*,*,isc,iw))
 bb     =reform(b(*,*,isc,iw))
 ff	=ff>bb
 di     =((max(ff)-min(ff))/5.)>0.5
 ffmin	=min(ff)+(ns-1)*di
 ffmax	=max(ff)
 for is=0,ns-1 do begin
  ffmin=ffmin<(min(ff(is,*)+(is-1)*di))
  ffmax=ffmax>(max(ff(is,*)+(is+1)*di))
 endfor
 !p.position=[0.25+0.2*iw,0.1,0.4+0.2*iw,0.9]
 !p.title=wave_(iw)+' A'
 !x.title='pixels'
 !y.title=' '
 !x.range=[0,nw-1]
 !y.range=[ffmin,ffmax]
 !x.style=1
 !y.style=1
 plot,[1,1]*nw/2,!y.range,linestyle=1
 for is=ns-1,0,-1 do begin
  iiw   =ib1+findgen(ib2-ib1+1)
  ffw	=reform(ff(is,ib1:ib2))
  bbw	=reform(bb(is,ib1:ib2))
  if (frac(is,isc) ge fracmin) then $
   polyfill,[iiw,reverse(iiw)],[ffw,reverse(bbw)]+is*di,color=200-100*isc
  oplot,reform(ff(is,*))+is*di,thick=1
  oplot,reform(bb(is,*))+is*di,thick=1,linestyle=2
  if (backgr eq 4) then oplot,xww,reform(gg(is,*))+is*di,thick=3
  oplot,[ib1,ib2],[ff(is,ib1),ff(is,ib2)]+is*di,psym=4
 endfor
 !noeras=1
endfor
xyouts,0.1,0.95,'Loop #'+string(iloop,'(i2)')+'/'+sc_(isc),size=char*2,/normal
xyouts,0.5,0.95,'Background = '+method+' Method',size=char,/normal
fig_close,io,fignr,ref
end
