pro euvi_lightcurves,date,t1s,t2s,dt,datafiles_,instr_,wave_,area_,t_m_,eh_m_,theat_,l_,s_h_,pdiv_,w_,fignr,io,ct
;+
; Project     : STEREO
;
; Name        : EUVI_LIGHTCURVES 
;
; Category    : Data modeling 
;
; Explanation : This routine simulates an observed time profile
;		based on a loop model with impulsive heating and 
;		subsequent conductive and radiative cooling.
;
; Syntax      : IDL>euvi_lightcurves,instr_,wave_,area_,dir,filename,sc,index,$
;                                    t1,t2,dt,t_m,eh_m,theat,l,s_h,pdiv,w,dur
;
; Input       : date	=string in format YYYYMMDD, e.g., '20071211'
;	        t1s	=start time in sec 
;	        t2s	=end   time in sec 
;		dt	=time step in sec, e.g., dt=3.0
;	        datafiles_=strarray of data filenames 
;		instr_	=strarr, e.g., ['GOES','XRT','EIS','EUVI',...]
;		wave_	=strarr, e.g., ['0.5-4',...'
;			 GOES: ['0.5-4','1-8']
;			 XRT:  ['Al-poly']
;			 EIS:  ['195','202','258','262','264','275']
;			 EUVI: ['171','195','284','304']
;		area_	=lonarr, e.g., [1,...], number of pixels of averaged flux
;		t_m	=time of maximum heating in sec (relative to start time) 
;		eh_m	=maximum heating rate in erg cm-2 s-1 
;		theat	=heating time scale in sec
;		l	=loop length in cm
;		s_h	=heating time scale in cm
;		pdiv	=divergence of loop, e.g., pdiv=0.0 for constant diamter
;		w	=width of loop in cm
;
; Outputs     : postscript file of plot *.ps
;
; History     : 2008, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

print,'INPUT__________________________________________________________'
ninstr	=n_elements(instr_)
th	=0.
sat     =10	;GOES
pixel	=1.6*0.725e8
psi	=2.2
te_0	=1.0e6
ne_0	=1.0e8
dur	=(t2s-t1s)
nt	=long(dur/dt) 
t	=t1s+dt*findgen(nt)	;s
goes_low=[-10.]	;default

print,'READ OBSERVED TIME PROFILES________________________________________'
goes	=0
euvi	=0
flux	=fltarr(nt,ninstr)
time	=fltarr(nt,ninstr)
back    =fltarr(nt,ninstr)
nte	=1000
resp	=fltarr(nte,ninstr)
te_resp	=fltarr(nte,ninstr)
tem_resp=fltarr(ninstr)
cad	=fltarr(ninstr)
calib	=lonarr(ninstr)+1

for in=0,ninstr-1 do begin
 wave	=wave_(in)
 instr	=instr_(in)
 obsfile=datafiles_(in)

 if (instr eq 'GOES') then begin
  goes=1
  restore,obsfile
  if (wave eq '0.5-4') then flux_goes=goes_high
  if (wave eq '1-8'  ) then flux_goes=goes_low
  n_t	=n_elements(flux_goes)
  if (n_t le nt) then begin
   flux(0:n_t-1,in)=flux_goes>0.
   time(0:n_t-1,in)=time_goes
  endif
  if (n_t gt nt) then begin
   flux(*,in)=interpol(flux_goes,time_goes,t)>0.
   time(*,in)=t
  endif
  cad(in)=median(time_goes(1:n_t-1)-time_goes(0:n_t-2))
  te_6	=10.^(te_log-6.)
  em_49 =fltarr(nte)+10.^(44.-49.)	;EM in units 10^44
  dateobs=strmid(obsfile,0,8)
  goes_fluxes,te_6,em_49,flong,fshort,date=dateobs,satellite=sat
  if (wave eq '1-8'  ) then resp(*,in)=flong
  if (wave eq '0.5-4') then resp(*,in)=fshort
 endif

 if (instr eq 'EUVI') then begin
  euvi=1
  restore,obsfile
  obs	=index(0).obsrvtry
  if (obs eq 'STEREO_A') then sc='a'
  if (obs eq 'STEREO_B') then sc='b'
  c	=2.9799e8 ;m/s
  delay =(1.5e11-index(0).dsun_obs)/c  ;STEREO A light time correction 
  n_t	=n_elements(t_i)
  for it=1,n_t-2 do begin
   fgap	=(flare(it-1)+flare(it+1))/2.
   if (flare(it) lt fgap/2.) then flare(it)=fgap
  endfor
  if (n_t le nt) then begin
   flux(0:n_t-1,in)=flare>0.
   time(0:n_t-1,in)=t_i
  endif
  if (n_t gt nt) then begin
   flux(*,in)=interpol(flare,t_i-delay,t)>0.
   time(*,in)=t
  endif
  cad(in)=median(t_i(1:n_t-1)-t_i(0:nt-2))
  dir_euvi ='~/idl_stereo/'
  respfile=concat_dir('$SSW_MJASTEREO','idl/euvi_response.dat')
  readcol,respfile,t_mk,r1_a,r2_a,r3_a,r4_a,r1_b,r2_b,r3_b,r4_b
  n      =n_elements(t_mk)
  temp_log=alog10(t_mk)+6.0
  if (sc eq 'a') and (wave eq '171') then resp_euvi=r1_a 
  if (sc eq 'a') and (wave eq '195') then resp_euvi=r2_a 
  if (sc eq 'a') and (wave eq '284') then resp_euvi=r3_a 
  if (sc eq 'b') and (wave eq '171') then resp_euvi=r1_b 
  if (sc eq 'b') and (wave eq '195') then resp_euvi=r2_b 
  if (sc eq 'b') and (wave eq '284') then resp_euvi=r3_b 
  resp(*,in)=resp_euvi
  te_resp(*,in)=temp_log
 endif

 if (instr eq 'XRT') then begin
  if (strpos(obsfile,'.sav'))  ge 1 then begin
   restore,obsfile
   t_i=times
  endif
  if (strpos(obsfile,'.genx')) ge 1 then begin
   restgen,times,int,line_id,file=obsfile
   n_t	=n_elements(times)
   t_i  =anytim(times,/second) mod 86400.
   data=int
   nlen=strlen(line_id)
   wave=strmid(line_id,0,1)
   for i=1,nlen-1 do begin 
    ch	=strmid(line_id,i,1)
    if (ch eq '_') then ch='-'
    wave=wave+ch
   endfor
   if (strmid(wave,0,5) eq 'Open/') then wave=strmid(wave,5,nlen-5)
   if (strmid(wave,nlen-5,5) eq '/Open') then wave=strmid(wave,0,nlen-5)
   wave_(in)=wave
  endif 
  if (n_t le nt) then begin
   flux(0:n_t-1,in)=(data/area_(in))>0.
   time(0:n_t-1,in)=t_i
  endif
  if (n_t gt nt) then begin
   ind	=where((t ge min(t_i)) and (t le max(t_i)))
   flux(ind,in)=(interpol(data,t_i,t(ind))>0.)/area_(in)
   time(ind,in)=t(ind)
  endif
  cad(in)=median(t_i(1:n_t-1)-t_i(0:n_t-2))
  ind	=where(t ge max(t_i),nind)
  if (nind ge 1) then flux(ind,in)=flux(ind(0)-1,in)
  resp_xrt=calc_xrt_temp_resp()
  ichan=where(resp_xrt.channel_name eq wave)
  if (ichan eq -1) then print,'No XRT channel found for ',wave
  resp_wave=resp_xrt(ichan).temp_resp   ;DN/pixel cm-5 s-1 
  temp_log =resp_xrt(ichan).temp
  nte	=n_elements(temp_log)
  em44	=resp_wave*1.e24     ;vol=(1.e8 cm)^3, n=10^10 cm-3==>EM=10^44 cm-6
  resp(0:nte-1,in)=em44
  te_resp(0:nte-1,in)=temp_log
 endif

 if (instr eq 'EIS') then begin
  if (strpos(obsfile,'.sav'))  ge 1 then begin
   restore,obsfile
   t_i=times
  endif 
  if (strpos(obsfile,'.genx')) ge 1 then begin
   restgen,times,int,line_id,file=obsfile
   t_i=anytim(times,/second) mod 86400.
   data=int
   wave=line_id
  endif
  n_t	=n_elements(t_i)
  wave_(in)=wave
  if (n_t le nt) then begin
   flux(0:n_t-1,in)=(data/area_(in))>0.
   time(0:n_t-1,in)=t_i
  endif
  if (n_t gt nt) then begin
   ind	=where((t ge min(t_i)) and (t le max(t_i)))
   flux(ind,in)=(interpol(data,t_i,t(ind))>0.)/area_(in)
   time(ind,in)=t(ind)
  endif
  cad(in)=median(t_i(1:n_t-1)-t_i(0:n_t-2))
  ind	=where(t ge max(t_i),nind)
  if (nind ge 1) then flux(ind,in)=flux(ind(0)-1,in)
  dir_eis='~/HINODE/'
  wave3	=wave
  ipos	=strpos(wave,'.')	
  if (ipos ge 1) then wave3=strmid(wave,ipos-3,3)
  if (wave3 eq '269') then wave3='268'
  search = dir_eis+'*'+wave3+'*.genx'
  respfile=findfile(search,count=count)
  if (count eq 1) then restgen,log_t,log_n0,emiss,lambda,abund,nH_ne,abund_ref,file=respfile(0)
stop
  if (count ne 1) then begin
   print,'WARNING: none or no unique respfile'
   calib(in)=0
  endif
  em44	=emiss*10.^(44-2.*log_n0)
  nte	=n_elements(log_t)
  resp(0:nte-1,in)=em44		
  te_resp(0:nte-1,in)=log_t
 endif

;back(*,in)=min(flux)
 resp_max=max(resp(*,in),imax)
 print,'reading obsfile = ',obsfile,' T=',tem_resp(in)/1.e6,' MK'
endfor



print,'GOES classsification__________________________________________________'
fm	=max(alog10(goes_low))
class	=['X','M','C','B','A','Z']
goesclass=' '
for i=-9,-4 do begin
 ic	=-i-4		;Z,A,....,X
 factor =10.^(fm-i)
 if (factor ge 1) then goesclass=class(ic)+strtrim(string(factor,'(f4.1)'),2)
endfor

print,'LIGHTCURVE MODELING_________________________________________'
ncomp	=n_elements(eh_m_)	;number of model components
print,'ncomp=',ncomp
t_p_	=fltarr(ncomp)
nemin	=1.e7
lightcurves=fltarr(nt,ninstr,ncomp+1)
for icomp=0,ncomp-1 do begin
 t_m	=t_m_(icomp)
 theat	=theat_(icomp)
 eh_m	=eh_m_(icomp)
 l	=l_(icomp)
 s_h	=s_h_(icomp)
 pdiv	=pdiv_(icomp)
 w	=w_(icomp)
 if (w eq 0) then w=1.e8 
 w0	=w
 hydro_approx1,eh_m,theat,l,s_h,t_m,t,eh_ana,te_ana,ne_ana,pe_ana,$
        t_p,t_e,te_p,ne_p,tcond,trad,tcool
 t_p_(icomp)=t_p
 print,'Maximum temperature  max(T)=',max(te_ana)
 print,'Peak density         max(n)=',max(ne_ana)
 print,'Conductive cooling time    =',tcond
 print,'Radiative cooling time     =',trad
 ind	=where(ne_ana gt 0)
 t_e	=t(max(ind))
 t_s	=t(min(ind))
 ind	=where((t ge t_s) and (t le t_e))
 it1	=min(ind)
 it2	=max(ind)
 te_ana =te_ana>te_0
 ne_ana =ne_ana>ne_0
 ns	=1000
 te_ana2=fltarr(ns,nt)
 ne_ana2=fltarr(ns,nt)
 itm	=long(t_m/dt)
 for it=0,nt-1 do begin 
  te_l	=te_ana(it)
  ne_l	=ne_ana(it)
  hydro_approx2,l,s_h,te_l,ne_l,th,ns,s,te_s,ne_s,p_s
  te_ana2(*,it)=te_s
  ne_ana2(*,it)=ne_s
 endfor
 for in=0,ninstr-1 do begin
  for it=it1,it2 do begin
   ne_s	=reform(ne_ana2(*,it))
   te_log=alog10(reform(te_ana2(*,it)))	;[K]
   ind	=where(te_resp(*,in) gt 0,nind)
   ne_	=interpol(ne_s,te_log,te_resp(ind,in))>nemin		
   s_	=interpol(s   ,te_log,te_resp(ind,in))		
   ds_	=(shift(s_,-1)-s_) &ds_(nind-1)=ds_(nind-2)   
   wid_  =w0*sin((!pi/2.)*(s_/l))^pdiv 	              ;loop divergence
   vol_log=alog10(2.*ds_)+2.*alog10(wid_) 
   em_log =2.*alog10(ne_)+vol_log
   em44   =10.^(em_log-44.)
   dem	=em44*resp(*,in)
   lightcurves(it,in,icomp)=total(dem)		
   lightcurves(it,in,ncomp)=total(dem)+lightcurves(it,in,ncomp)		
  endfor
 endfor
endfor

print,'DISPLAY____________________________________________________________'
form    =1      ;0=landscape, 1=portrait
char    =0.8    ;character size
plotname='f'    ;filename
ref     =''     ;label at bottom of Fig.
unit    =0      ;window number
nplot	=(ninstr+3)>(2*3)
iplot_	=findgen(nplot)
nrow	=(nplot+1)/2
icol_	=long(iplot_/nrow)
irow_	=long(iplot_ mod ((nplot+1)/2))
x1_	=0.1+0.48*icol_
x2_	=x1_+0.35
dy	=0.9/nrow
y2_	=0.93-dy*irow_
y1_	=0.93-dy*(irow_+0.75)
if (io eq 0) then thi=3
if (io ne 0) then thi=6
fig_open,io,form,char,fignr,plotname,unit
loadct,ct
t_hrs	=t/3600.
col_	=100+150*(findgen(ncomp+1)+0.5)/float(ncomp+1)

for in=0,ninstr-1 do begin
 yy	=lightcurves(*,in,ncomp)
 for icomp=0,ncomp-1 do begin
  color	=col_(icomp)
  t_m	=t_m_(icomp)
  t_p	=t_p_(icomp)
  instr	=instr_(in)
  wave	=wave_(in)
  tit	=instr+' '+wave_(in)
  units	='DN/s'
  if (instr eq 'GOES') then units='W m!U-2!N' 
  y	=lightcurves(*,in,icomp)
  b	=back(*,in)
  ind0	=where(time(*,in) ne 0)
  f	=flux(ind0,in)
  q	=max(f)/max(yy)
  time_hrs=time(ind0,in)/3600.

;plotting time profiles
  y1=min(f)
  y2=max(f) ;>max(y+b)
  !p.position=[x1_(in),y1_(in),x2_(in),y2_(in)]
  !p.charsize=char
  !p.title=tit
  !y.range=[y1,y2+0.3*(y2-y1)]
  !y.title=units
  !x.title=' '
  !y.style=0
  !x.style=5
  if (y1_(in) le 0.1) then begin
   !x.title='Time t-t!D0!N [s]' 
   !x.style=1
  endif
  !x.range=minmax(t_hrs)
  plot,time_hrs,f
  oplot,!x.crange,!y.crange(0)*[1,1]
  oplot,!x.crange,!y.crange(1)*[1,1]*0.99
  oplot,time_hrs,f,psym=10
  oplot,t_hrs,(y+b)*q,thick=thi,color=color
  oplot,t_hrs,(yy+b)*q,thick=3,color=col_(ncomp)
  oplot,t_hrs,b,color=color
  oplot,t_m*[1,1]/3600.,!y.crange,color=color
  oplot,t_p*[1,1]/3600.,!y.crange,color=color,linestyle=2
  xyouts_norm,0.70,0.8,'q!Dw!N='+string(sqrt(q),'(f4.2)'),char*1.5,0,color
  !noeras=1
 endfor
endfor

i	=ninstr>nrow
iplot	=1
!x.range=minmax(t_hrs)
!p.position=[x1_(i),y1_(i),x2_(i),y2_(i)]
!p.title=' '
eh_max=max(eh_ana)
yrange_h=[0,eh_max*1.2]
!y.range=yrange_h
!x.style=1
!y.style=0
!x.title=' '
!y.title='Heating rate E!DH!N[erg cm!U-3!D s!U-1!N]'
!y.title=' '
for icomp=0,ncomp-1 do begin
 plot,t_hrs,eh_ana,thick=thi
 oplot,t_hrs,eh_ana,thick=thi,color=color
 oplot,t_m*[1,1]/3600.,!y.crange,color=color
 oplot,t_p*[1,1]/3600.,!y.crange,color=color,linestyle=2
 xl	=0.05+0.65*iplot
 xyouts_norm,xl,0.88,'E!DH!N='+string(eh_m,'(f5.2)')+' erg cm!U-3!N s!U-1!N',char,0,color
 xyouts_norm,xl,0.76,'t!Dheat!N='+string(theat,'(i4)')+' s',char,0,color
 shlabel=string(s_h/1.e8,'(i4)')+' Mm'
 if (s_h gt l) then shlabel='uniform'
 xyouts_norm,xl,0.64,'s!DH!N='+shlabel,char,0,color
 xyouts_norm,xl,0.52,'L ='+string(l/1.e8,'(i4)')+' Mm',char,0,color
 xyouts_norm,xl,0.40,'w ='+string(w/1.e8,'(f5.1)')+'+',char,0,color
 xyouts_norm,xl,0.28,'p!Ddiv!N='+string(pdiv,'(f5.2)'),char,0,color
 xyouts_norm,xl,0.16,'th ='+string(th,'(i2)'),char,0,color
endfor

i	=i+1
!p.position=[x1_(i),y1_(i),x2_(i),y2_(i)]
te_max=max(te_ana)
yrange_t=[0.,1.2*te_max/1.e6]
!y.range=yrange_t
if (iplot eq 0) then !y.title='Temperature T[MK]'
if (iplot gt 0) then !y.title=' '
plot,t_hrs,te_ana/1.e6
oplot,t_hrs,te_ana/1.e6,thick=thi,color=color
oplot,t_m*[1,1]/3600.,!y.crange,color=color
oplot,t_p*[1,1]/3600.,!y.crange,color=color,linestyle=2
xyouts_norm,xl,0.88,'T!Dm!N='+string(max(te_ana)/1.e6,'(f5.1)')+' MK',char,0,color
xyouts_norm,xl,0.76,'t!Dm!N='+string(t_m/3600.,'(f8.3)')+' hrs',char,0,color
xyouts_norm,xl,0.64,'t!Dp!N='+string(t_p/3600.,'(f8.3)')+' hrs',char,0,color
xyouts_norm,xl,0.50,'t!De!N='+string(t_e/3600.,'(f8.3)')+' hrs',char,0,color

i	=i+1
!p.position=[x1_(i),y1_(i),x2_(i),y2_(i)]
ne_max=max(ne_ana)
yrange_n=[0,ne_max*1.2]/1.e10
!y.range=yrange_n
!x.style=1
if (iplot eq 0) then !y.title='Density n!De!N[10!U10!N cm!U-3!N]'
if (iplot gt 0) then !y.title=' '
!x.title='Time t-t!D0!N [s]' 
plot,t_hrs,ne_ana/1.e10
oplot,t_hrs,ne_ana/1.e10,thick=thi,color=color
oplot,t_m*[1,1]/3600.,!y.crange,color=color
oplot,t_p*[1,1]/3600.,!y.crange,color=color,linestyle=2
ne_max	=max(ne_ana) &p_ne=alog10(ne_max)     &q_ne=ne_max/10.^(long(p_ne))
em_max	=(ne_max/1.e10)^2*(w/1.e8)^2*(l/1.e8)
p_em=alog10(em_max)+44. &q_em=em_max/10.^(long(p_em-44.))
xyouts_norm,xl,0.88,'n!Dp!N ='+string(q_ne,'(f4.2)')+' 10!U'+string(long(p_ne),'(I2)')+'!N cm!U-3!N',char,0,color
xyouts_norm,xl,0.76,'EM!Dp!N='+string(q_em,'(f4.2)')+' 10!U'+string(long(p_em),'(I2)')+'!N cm!U-3!N',char,0,color
xyouts_norm,xl,0.64,'t!Dcond!N='+string(tcond,'(i6)')+' s',char,0,color
xyouts_norm,xl,0.50,'t!Drad!N ='+string(trad,'(i6)')+' s',char,0,color

fig_close,io,fignr,ref
end

