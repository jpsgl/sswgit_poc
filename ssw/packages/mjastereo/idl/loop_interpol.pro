pro loop_interpol,dpix,x,y,xx,yy
;interpolates loop coordinates [x,y] with step dpix[Mm] to [xx,yy]

spline_p,x,y,xs,ys
nl   =n_elements(xs)
dl   =sqrt((xs(1:nl-1)-xs(0:nl-2))^2+(ys(1:nl-1)-ys(0:nl-2))^2) ; Mm
sl   =fltarr(nl)
for i=1,nl-1 do sl(i)=sl(i-1)+dl(i-1)
len  =total(dl)
ns   =long(len/dpix) ;number of pixels
s    =len*findgen(ns)/float(ns-1)
xx   =interpol(xs,sl,s)
yy   =interpol(ys,sl,s)
end
