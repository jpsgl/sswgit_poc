pro nlfff_sim,dir,filename,parafile,vers,para
;+
; Project     :  
;
; Name        : MAGN_SIM
;
; Category    : magnetic field moedling
;
; Explanation : simulates nonlinear force-free magnetic field models
;
; Syntax      : IDL> nlfff_sim,dir,filename,vers,para
;
; Inputs      : filename = *._coeff.dat, *_cube.sav, *_field.sav,   
;		para     = input parameters
;
; Outputs     ; saves parameters in file *_coeff.dat
;		--> m1,x1,y1,z1,a1		
;
; History     : 16-Apr-2014, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

print,'__________INPUT PARAMETERS__________________________
readcol,parafile,nr_,imag_,mag_,lon_,lat_,r_,skip=1,/silent
nr	=long(strmid(vers,4,3))
ind	=where(nr_ eq nr,nind)
if (nind eq 0) then stop,'NLFFF_SIM : No matching run number'
nmag0   =max(imag_(ind))+1
b1_	=mag_(ind)
r1_	=r_(ind)
y1_	=r1_*sin(lat_(ind)*!pi/180.)
x1_	=r1_*sin(lon_(ind)*!pi/180.)*cos(lat_(ind)*!pi/180.)
z1_	=sqrt(r1_^2-x1_^2-y1_^2)
print,'Number of simulation run   = ',nr
print,'NUmber of magnetic charges = ',nmag0
print,'B-field                    = ',b1_(0)
print,'Longitude                  = ',lon_(ind(0))
print,'Latitude                   = ',lat_(ind(0))
print,'x-position                 = ',x1_(0)
print,'y-position                 = ',y1_(0)
print,'z-position                 = ',z1_(0)
print,'radius of magn source      = ',r1_(0)

nx	=256	       
ny	=nx
dfoot   =0.015          ;max distance from next magnetic source with B>B_foot
dsmag   =0.0005*3       ;pixelsize magnetogram (0.0005, 0.001, 0.002, 0.004)
nsmax   =200            ;maximum number of points per loop
hmax    =0.15           ;maximum altitude for field extrapolation
ds      =0.002          ;spatial resolution of gridpoint (solar radii)
n_nlfff	=nmag0		;number of NLFFF magnetic charges
fov_x1  =x1_(0)-dsmag*(nx/2)  
fov_y1  =y1_(0)-dsmag*(ny/2)  
fov_x2  =x1_(0)+dsmag*(nx/2)  
fov_y2  =y1_(0)+dsmag*(ny/2)  
fov	=[fov_x1,fov_y1,fov_x2,fov_y2]
para	=fltarr(25)
para    =[n_nlfff,nmag0,dfoot,dsmag,0,0,0,nsmax,0,0,0,0,hmax,ds,0,0,0,fov,0,0,0]
dpix    =dsmag
rpix	=1./dpix
x       =fov_x1+dpix*(findgen(nx))
y       =fov_y1+dpix*(findgen(ny))
crpix1	=-x(0)/dpix
crpix2	=-y(0)/dpix
n	=nmag0
a1_	=fltarr(n)
coeff	=[[b1_[0:n-1]],[x1_[0:n-1,0]],[y1_[0:n-1]],[z1_[0:n-1]],[a1_[0:n-1]]]

;________________STORE SIMULATION PARAMETERS________________
coeff_file =dir+filename+'_coeff.dat'
coeff_model=dir+filename+'_coeff0.dat'
dim	=size(coeff)
nmag	=dim[1]
openw,2,coeff_file
openw,3,coeff_model
for is=0,nmag-1 do begin
 m1     =coeff(is,0)
 x1     =coeff(is,1)
 y1     =coeff(is,2)
 z1     =coeff(is,3)
 a1     =coeff(is,4)
;print   ,m1,x1,y1,z1,a1,format='(5f12.5)'
 printf,2,m1,x1,y1,z1,a1,format='(5f12.5)'
 printf,3,m1,x1,y1,z1,a1,format='(5f12.5)'
endfor
close,2
close,3
print,'Coefficient file written : ',coeff_model
print,'Coefficient file written : ',coeff_file

;________________SIMULATE MAGNETOGRAM_________________________
aia_mag_map,coeff,1.,x,y,bzmap

;________________CALCULATE FIELD LINES________________________
qloop   =0.5
ngrid	=20
nbound	=4
nxx	=long(nx/ngrid-nbound+1)
nyy	=long(ny/ngrid-nbound+1)
nf	=nxx*nyy
print,'Number of field simulated lines = ',nf
np_	=lonarr(nf)
field_lines=fltarr(nsmax,5,nf)
k	=0
for i=0,nxx-1 do begin
for j=0,nyy-1 do begin
 xm     =x[(i+nbound/2)*ngrid]
 ym     =y[(j+nbound/2)*ngrid]
 zm    =sqrt(1.-xm^2-ym^2)
 nlfff_fieldline,xm,ym,zm,ds,hmax,coeff,field,ns,nsmax
 imm	=long(qloop*(ns-1))
 xmm	=field(imm,0)
 ymm	=field(imm,1)
 zmm    =field(imm,2)
 nlfff_fieldline,xmm,ymm,zmm,ds,hmax,coeff,field,ns,nsmax
 ns     =ns < nsmax
 np_[k] =ns
 field_lines(0:ns-1,0,k)=field(0:ns-1,0)
 field_lines(0:ns-1,1,k)=field(0:ns-1,1)
 field_lines(0:ns-1,2,k)=field(0:ns-1,2)
 k	=k+1
endfor
endfor

;________________SAVE LOOP COORDINATES________________________
loopfile=dir+filename+'_loop_'+vers+'.dat'
zero	=0.
openw,1,loopfile
for k=0,nf-1 do begin
 for is=0,np_[k]-1 do begin
  xl	=field_lines(is,0,k)
  yl	=field_lines(is,1,k)
  zl	=field_lines(is,2,k)
  flux	=1000./np_[k]
  printf,1,k,xl,yl,zl,zero
 endfor
endfor
close,1
help,field_lines
print,'loopfile written = ',loopfile

;________________MAGNETIC MAP__________________________________
magfile =dir+filename+'_magn.fits'
writefits,magfile,bzmap
image=readfits(magfile,header)
sxaddpar,header,'DATE_OBS',filename
sxaddpar,header,'DATAMIN',min(bzmap)
sxaddpar,header,'DATAMAX',max(bzmap)
sxaddpar,header,'NAXIS',2
sxaddpar,header,'NAXIS1',nx
sxaddpar,header,'NAXIS2',ny
sxaddpar,header,'CRPIX1',crpix1
sxaddpar,header,'CRPIX2',crpix2
sxaddpar,header,'CDELT1',dpix
sxaddpar,header,'CDELT2',dpix
sxaddpar,header,'SOLAR_P0',0
sxaddpar,header,'R_SUN',rpix
print,'rpix,dpix=',rpix,dpix
writefits,magfile,bzmap,header
print,'Magnetic field file written = ',magfile
end
