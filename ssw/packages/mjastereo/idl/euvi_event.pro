pro euvi_event,catfile,id,line,date,t1,t2,sc,pos 

;reads flare catalog
if (id ge 1) then begin
 test	=0	;1=test output
 openr,1,catfile
 while not eof(1) do begin
  line=' '
  readf,1,line
  if (strmid(line,5,1) eq '2') then begin
   id_=strmid(line,0,3)
   if (long(id_) eq id) then goto,endfile
  endif
 endwhile
 close,1
 stop,'ERROR: ID ='+string(id,'(i3)')+' not found in catalog file ',catfile
 endfile:
 close,1

 yy	=strmid(line,5,4)
 month	=strmid(line,10,3)
 month_	=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
 im	=where(month eq month_)
 if (im le 8) then mm='0'+string(im(0)+1,'(i1)')
 if (im ge 9) then mm=string(im(0)+1,'(i2)')
 dd	=strmid(line,14,2)
 date	=yy+mm+dd
 t1	=strmid(line,18,2)+strmid(line,21,2)
 t2	=strmid(line,24,2)+strmid(line,27,2)
 ab	=strmid(line,45,2)
 sc	=' '
 if (ab eq 'A ') then sc='a'
 if (ab eq ' B') then sc='b'
 if (ab eq 'AB') then sc='ab'
 pos	=strtrim(strmid(line,29,10),2)
endif
end
