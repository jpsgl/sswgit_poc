pro aia_mag_map,coeff,mag,x,y,image
;+
; Project     : AIA
;
; Name        : AIA_MAG_MAP
;
; Category    : simulations 
;
; Explanation : simulates artificial magnetogram image based on coeff
;
; Syntax      : IDL> aia_mag_sim,coeff,magn,x,y,image
;
; Inputs      : coeff[ncomp,4]	;coefficients of magnetic components
;               mag		;maximum magnetic field strength {Gauss)
;		x		;x-coordinates of map
;		y		;y-coordinates of map
;
; Outputs     : image[nx,ny]	;simulated magnetic field map
;
; History     : 21-Apr-2014, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

;simulates MDI file____________________________________________________
nx	=n_elements(x)
ny	=n_elements(y)
image	=fltarr(nx,ny)
dim	=size(coeff)
ncomp	=dim(1)
for icomp=0,ncomp-1 do begin
 bm     =coeff(icomp,0)
 xm     =coeff(icomp,1)
 ym     =coeff(icomp,2)
 zm     =coeff(icomp,3)
 rm     =sqrt(xm^2+ym^2+zm^2)                   ;distance from Sun center
 dm     =1.-rm                                  ;depth in solar radii
 for j=0,ny-1 do begin
  z	=sqrt(1.-x^2-y(j)^2)			;photosphere
  rho	=sqrt(x^2+y(j)^2)
  ind	=where(rho le 1.,nind)
  if (nind ge 1) then begin
   d     =sqrt((x-xm)^2+(y(j)-ym)^2+(z-zm)^2)
   b 	=mag*bm*(dm/d)^2*(z-zm)/d
   image(ind,j)=image(ind,j)+b(ind)
  endif
 endfor
endfor
end
