pro nlfff_geo,x_fit,v_fit,nseg,nf,hmin,qapex,phi1,phi2,field_loops,ns_loop,len
;+
; Project     : SDO/AIA, HMI
;
; Name        : NLFFF_GEO
;
; Category    : Magnetic data modeling 
;
; Explanation : Generates projected circular loop segment geometries
;
; Syntax      : IDL>nlfff_geo,x_fit,v_fit,nseg,hapex,phi1,phi2
;
; Inputs      : x_fit	  =fltarr[nfit,3]   coordinates of nfit loop positions
;               v_fit	  =fltarr[nfit,3]   vectors at nfit loop positions
;               nseg      =number of segments nfit=nseg*nf 
;		hapex	  =loop apex height
;		phi1	  =first footpoint loop azimuth
;		phi2	  =second footpoint loop azimuth
;
; Outputs     ; x_fit, v_fit
;
; History     : 11-Jun-2014, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

;print,'__________________NLFFF_GEO_____________________________'
nfit	=nseg*nf
dim	=size(x_fit)
if (dim[1] ne nfit) then stop,'NLFFF_GEO: dimension discrepancy nfit=nseg*nf'

for k=0,nf-1 do begin
 ns=ns_loop[k]
 xl=field_loops[0:ns-1,0,k]
 yl=field_loops[0:ns-1,1,k]
 phi=phi1+(phi2-phi1)*(findgen(nseg)+0.5)/float(nseg)
 hapex=qapex*len(k)
 sin_min=min(sin(phi))
 sin_max=max(sin(phi))
 hl=hmin+(hapex-hmin)*(sin(phi)-sin_min)/(sin_max-sin_min)
 vl=hmin+(hapex-hmin)*cos(phi)                ;derivative
 rl=hl+1.
 zl=sqrt(rl^2-xl^2-yl^2)
 ifit=k*nseg+findgen(nseg)
 iseg=long(1+(ns-3)*findgen(nseg)/float(nseg-1)) < (ns-2)
 x_fit[ifit,2]=zl
 v_fit[ifit,2]=vl
endfor
end
