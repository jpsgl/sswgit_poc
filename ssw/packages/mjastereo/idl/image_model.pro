
;LOOP PARAMETERS______________________________________
d2_	=fltarr(11,4)
for iloc=0,3 do begin
for irun=0,10 do begin
nloop	=2^irun	;number of loops
nx	=512	;imagesize
pixel	=1.0	;arcsec
backgr	=5	;brightness of background
bright	=5	;brightness of loop
ns	=200	;max. number of coordinate points per loop 
seed1	=12345
seed2	=seed1*2
seed3	=seed1*3
seed4	=seed1*4
seed5	=seed1*5
y_offset= 10.*(1.+10.0*randomn(seed1,nloop))
base_	= 80.*(1.+0.2*randomn(seed2,nloop))
h0_	=-10.*(1.+0.5*randomn(seed3,nloop))
th_	=-30.*(1.+0.1*randomn(seed4,nloop))
hmin	=4.	
az	=85.	
l1	=0.	
b1	=10
;EPHEMERIDES__________________________________________
rsun	=960.   
l0	=-60.+30.*iloc	;0,-30,-60,	
print,'l0=',l0,'____________________________'
b0	=0.	
p 	=15.	
p0	=-15.
grid	= 10.

;IMAGE_MODEL__________________________________________
x_	=fltarr(ns,nloop)
y_	=fltarr(ns,nloop)
z_	=fltarr(ns,nloop)
ns_	=fltarr(nloop)
rmin	=rsun+hmin
for is=0,nloop-1 do begin
 base	=base_(is)
 h0	=h0_(is)
 th	=th_(is)
 loop_para_coord,base,h0,hmin,th,rsun,xl,yl,zl 		;LOOP PARAMETERS --> LOOP PLANE COORD
 yl	=yl+y_offset(is)
 helio_loopcoord,xl,yl,zl,rsun,az,th,l1,b1,rmin,l,b,r  	;LOOP PLANE COORD --> HELIOGR COORD
 helio_trans2,p,p0,l0,b0,l,b,r,x,y,z,rsun 		;TRANSFORMATION IN IMAGE COORD
 ns	=n_elements(x)
 ns_(is)=ns
 x_(0:ns-1,is)=x
 y_(0:ns-1,is)=y
 z_(0:ns-1,is)=z
 if (is eq 0) then begin
  xmin	=min(x)
  xmax	=max(x)
  ymin	=min(y)
  ymax	=max(y)
 endif
 if (is ge 1) then begin
  xmin	=xmin < min(x)
  xmax	=xmax > max(x)
  ymin	=ymin < min(y)
  ymax	=ymax > max(y)
 endif
endfor

;IMAGE_CREATION_______________________________________
naxis1	=nx
naxis2	=nx
crpix1	=(naxis1+1)/2
crpix2	=(naxis2+1)/2
crval1	=(xmin+xmax)/2.
crval2	=(ymin+ymax)/2.
cdelt1	=pixel
cdelt2	=pixel
ctype1	='SOLAR_X'
ctype2	='SOLAR_Y'
crota1	=0.
crota2	=0.
xcen	=crval1+cdelt1*((naxis1+1)/2-crpix1)
ycen	=crval2+cdelt2*((naxis2+1)/2-crpix2)
cunit1	='arcsec'
cunit2	='arcsec'

image	=lonarr(nx,nx)+backgr
x	=xcen+cdelt1*(findgen(nx)-(nx+1)/2.)
y	=ycen+cdelt2*(findgen(nx)-(nx+1)/2.)
for is=0,nloop-1 do begin
 ns	=ns_(is)
 ix	=long((x_(0:ns-1,is)-x(0))/pixel)
 iy	=long((y_(0:ns-1,is)-y(0))/pixel)
 image(ix,iy)=image(ix,iy)+bright*replicate(1,ns)
endfor

xrange	=(xmax-xmin)
yrange	=(ymax-ymin)
n2	=long( (alog(xrange)/alog(2.))/2. + (alog(yrange)/alog(2.))/2. + 0.5)
n2	=7
nx2	=2^n2
print,'size of subimage=',nx2
subimage=lonarr(nx2,nx2)
ix1 	=(nx/2)-nx2/2
ix2	=ix1+nx2-1
subimage=image(ix1:ix2,ix1:ix2)

;DISPLAY______________________________________________
io	=0
form	=0
char	=1
fignr	='0'
plotname='image_model'
unit	=0
ref	=''

fig_open,io,form,char,fignr,plotname,unit
!p.position=[0.1,0.05,0.75,0.95]
!x.range=minmax(x)
!y.range=minmax(y)
!x.style=1
!y.style=1
plot,[0,0],[0,0]
coord_sphere,rsun,p,p0,l0,b0,grid
for is=0,nloop-1 do begin
 ns	=ns_(is)
 x	=x_(0:ns-1,is)
 y	=y_(0:ns-1,is)
 z	=z_(0:ns-1,is)
 plot,x,y,thick=3
endfor

yes=1
;read,'display image? [0,1] ',yes
if (yes eq 1) then tv,bytscl(image,min=0,max=max(image))

;read,'display subimage? [0,1] ',yes
if (yes eq 1) then begin
 clearplot
 erase
 tv,bytscl(subimage,min=0,max=max(image))
endif


dim	=size(subimage)
nx	=dim(1)
ny	=dim(2)
nscale	=long(alog(nx)/alog(2))
zmax	=max(subimage)
zmin	=min(subimage)
gap	=10
zlev	=zmin
print,'nx,nscale=',nx,nscale
for iscale=nscale-2,0,-1 do begin  
 nxx	=nx/(2^iscale)
 im	=rebin(subimage,nxx,nxx)
 ncol	=long(512/nx)
 icol	=(nx*iscale) mod 512
 irow	=nx*long(iscale/ncol)
 tv,bytscl(im,min=0,max=zmax),icol,irow

 nbox	=n_elements(im)
 ind	=where(im gt zlev,area) 
 area	=area>1
 len	=nxx
 d	=alog(area)/alog(len)
 d1	=alog(area+sqrt(area))/alog(len)
 a3     =float(nloop*avg(base_/2.)*!pi)
 d3	=alog(a3)/alog(nx)
 d3_pred=d+0.03*tan((d-1.)*!pi/2.)
 nl_pred=(nx^(d3_pred-1.))>1	
 
 print,iscale,nx,nxx,'  A=',area,'  L=',len,' D=',d,' + ',abs(d1-d),' D3_pred,D3=',d3_pred,d3,$
	' NL_pred,NL=',nl_pred,nloop,format='(3i6,a,i6,a,i6,a,f4.2,a,f4.2,a,2f5.2,a,2i6)'
endfor
fig_close,io,fignr,ref

d2_(irun,iloc)=d
wait,2
endfor
endfor
end

