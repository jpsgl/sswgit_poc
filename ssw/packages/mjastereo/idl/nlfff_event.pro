pro nlfff_event,efile,iev,dt,tmargin,nt,dateobs_,fov0,fov_,goes,noaa,helpos,ang
;+
; Project     : GOES, AIA/SDO, HMI/SDO
;
; Name        : NLFFF_EVENT
;
; Category    : Magnetic data modeling 
;
; Explanation : read event start time, end time, and heliographic 
;		position and produces an array of NT times DATEOBS_ 
;		with a cadence DT and field-of-view positions FOV_
;		in units of solar radii from Sun center.
;
; Syntax      : IDL>nlfff_event,efile,iev,t1,t2,dt,nt,days_,dateobs_,fov_
;
; Inputs      : efile		='goes_noaa_M1.txt'
;		iev		=1 	;event nr from list (1...155)
;		dt		=0.1	;6-min cadence  
;		tmargin		=0.5	;30-min margin before start and after end
;		fov0		=0.4	;field-of-view [solar radii]
;
; Outputs     ; nt		=number of timesteps
;		dateobs_[NT,0] 	=['20110612_003000',...]
;		dateobs_[NT,1] 	=['2011-06-12 00:30:00',...]
;		fov_[4,NT]	=[[x1,y1,x2,y2,0]  ,...,[x1,y1,x2,y2,nt-1]]
;		goes		=GOES flare class, e.g. 'M2.0'
;		noaa		=NOAA active region nr,  e.g., '11081'
;		helpos		=heliographic position, e.g., 'N23W47'
;		ang		=heliographic angle from disk center
;
; History     : 14-Jan-2014, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

;print,'___________________NLFFF EVENT _____________________'
readcol,efile,event_,tstart_,tpeak_,tend_,goes_,noaa_,helpos_,$
       format='(I,A,A,A,A,A,A)',/silent
nev	=n_elements(event_)
ind	=where(event_ eq iev,nind)
if (nind le 0) then stop,'NLFFF_EVENT : NO MATCHING EVENT FOUND'
tstart	=tstart_(ind(0))
tpeak	=tpeak_(ind(0))
tend	=tend_(ind(0))
goes	=goes_(ind(0))
noaa	=noaa_(ind(0))
helpos	=helpos_(ind(0))
;print,'EVENT NR = ',event_nr
;print,'START    = ',tstart
;print,'PEAK     = ',tpeak
;print,'TEND     = ',tend
;print,'GOES     = ',goes
;print,'NOAA AR  = ',noaa
;print,'HELIO POS= ',helpos

;print,'________________TIME CADENCE ________________________'
t_syn	=27.2753*86400.	;mean synodic period of rotation [s]	
tp_sec	=anytim(tpeak,/sec)
t_year	=tp_sec mod long(365.2422*86400.)
t_b0	=(5*30.+6)*86400.			;June 6, B0=0 deg
b0	=7.24*sin(2.*!pi*(t_year-t_b0))
t1_sec	=anytim(tstart,/sec)-tmargin*3600.
t2_sec	=anytim(tend,/sec)  +tmargin*3600.
dt_sec	=dt*3600
nt	=long((t2_sec-t1_sec)/dt_sec+1)
;print,'CADENCE  = ',dt,' hours'
;print,'TIMESTEPS= ',nt
north	=strmid(helpos,0,1)
west	=strmid(helpos,3,1)
if (north eq 'N') then lat=+long(strmid(helpos,1,2))
if (north eq 'S') then lat=-long(strmid(helpos,1,2))
if (west  eq 'W') then lon=+long(strmid(helpos,4,2))
if (west  eq 'E') then lon=-long(strmid(helpos,4,2))
dateobs_=strarr(nt,2)
fov_	=fltarr(4,nt)
for it=0,nt-1 do begin
 t	=t1_sec+it*dt_sec
 dt_rot	=(t-tp_sec)*cos(lat*!pi/180.)
 timestr=anytim(t,/hxrbs)
 yy	=strmid(timestr,0,2)
 mo	=strmid(timestr,3,2)
 dd	=strmid(timestr,6,2)
 hh	=strmid(timestr,10,2)
 mm	=strmid(timestr,13,2)
 ss	=strmid(timestr,16,2)
 dateobs_(it,0)='20'+yy+mo+dd+'_'+hh+mm+ss
 dateobs_(it,1)='20'+yy+'-'+mo+'-'+dd+' '+hh+':'+mm+':'+ss
 dx_rot	=2.*!pi*(dt_rot/t_syn)
 x0	=sin(lon*!pi/180.)*cos(lat*!pi/180.)+dx_rot
;y0	=sin(lat*!pi/180.)
 y0	=sin((lat-b0)*!pi/180.)
 dx	=fov0/2.
 dy	=fov0/2.
 fov_[*,it]=[x0-dx,y0-dy,x0+dx,y0+dy]
;print,it,' --> ',dateobs_(it),'  ',fov_[*,it],format='(i4,3a,4f8.3)'
endfor
ang	=abs(lon)		;heliogrpahic longitude from disk center
end
