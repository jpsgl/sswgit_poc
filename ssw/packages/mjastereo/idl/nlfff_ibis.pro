pro nlfff_ibis,dir,savefile,test
;+
; Project     : AIA, IRIS, IBIS
;
; Name        : NLFFF_READ
;
; Category    : read AIA, IRIS, IBIS images 
;
; Explanation : reads image from archive at LMSAL 
;               and write uncompressed FITS file
;
; Syntax      : IDL>nlfff_read,dir,filename,instr,wave_
;
; Inputs      : dir	 =data directory 
;		savefile 
;
; Outputs     ; saves AIA FITS file in dir
;
; History     : 16-Oct-2015, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

restore,dir+savefile    ;--> PARA
filename=strmid(savefile,0,15)
wave8   =input.wave8
indw	=where(wave8 ne 0,nwave)
wave_	=wave8[indw]
instr   =input.instr
if (instr ne 'IBIS') then goto,end_proc

print,'__________________NLFFF_IBIS________________________________'
nwave   =n_elements(wave_)
nfiles_wave=lonarr(nwave)
for iwave=0,nwave-1 do begin
 wave=strtrim(string(wave_(iwave),'(I5)'),2)
 search=dir+filename+'_'+instr+'_*'+wave+'.fits'
 file   =findfile(search,count=count)
 if (count eq 1) then begin
  print,'File already exists : ',file
  nfiles_wave[iwave]=1
 endif
endfor
if (total(nfiles_wave) eq nwave) then begin
 print,'All files exist'
 goto,end_proc
endif

;________________________PREPARE FILE_______________________________'
if (count eq 0) then begin
 print,'File not found : '+datafile
 if (instr eq 'IBIS') and (wave_[0] eq '8542') and (dateobs eq '2010-08-03T15:23:00') then begin
  infile ='Ca8542.mosaic.spk.+0.04A.v3.2.fits'
  file	=findfile(dir+infile,count=orig)
  if (orig eq 1) then print,'Original file exists : ',file
 endif
endif

if (orig eq 1) then begin
   dx    =0.0976     ;arcsec per pixel, Kevin Reardon, 2015 Sep 17
   x1    =-168.8     ;arcsec   ;-0.178 solar radii
   x2    =  74.8     ;arcasec  ; 0.079 solar radii
   y1    =  59.3     ;arcsec   ; 0.063 solar radii
   y2    = 301.7     ;arcsec   ; 0.319 solar radii

   fits_read,file,data,h
   help,data,h
   dim   =size(data)
   nx    =dim(1)
   ny    =dim(2)
   naxis1=nx
   naxis2=ny
   cdelt1=dx
   cdelt2=dx
   crval1=0.
   crval2=0.
   crpix1=-x1/cdelt1             ;pixel nr of Sun center
   crpix2=-y1/cdelt2             ;pixel nr of Sun center

   sxaddpar,h,'DATE_OBS',dateobs
   sxaddpar,h,'NAXIS1',naxis1
   sxaddpar,h,'NAXIS2',naxis2
   sxaddpar,h,'CDELT1',cdelt1
   sxaddpar,h,'CDELT2',cdelt2
   sxaddpar,h,'CRVAL1',crval1
   sxaddpar,h,'CRVAL2',crval2
   sxaddpar,h,'CRPIX1',crpix1
   sxaddpar,h,'CRPIX2',crpix2
   writefits,dir+datafile,data,h
   fits_read,dir+datafile,data,h
   print,'datafile written = ',datafile
 endif

;DISPLAY IMAGE______________________________________________
DISPLAY:
if (test eq 1) then begin
 fits_read,dir+datafile,data,h
 help,data,h
 dim   =size(data)
 nx    =dim(1)
 ny    =dim(2)
 window,0,xsize=nx/2,ysize=ny/2
 loadct,3
 statistic,data,zavg,zsig
 c1	=zavg-3*zsig
 c2	=zavg+3*zsig
 tv,bytscl(rebin(data,nx/2,ny/2),min=c1,max=c2)
endif
END_PROC:
end
