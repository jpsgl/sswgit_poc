pro aia_mag_weight,x1,y1,z1,coeff,nrank,coeff_rank
;+
; Project     : AIA/SDO
;
; Name        : AIA_LOOP_FIELDLINE
;
; Category    : Data analysis   
;
; Explanation : Ranks magnetic field components for field line
;               passing through position [x1,y1,z1]
;
; Syntax      : IDL>aia_loop_weight,x1,y1,z1,coeff,nrank,coeff_rank
;
; Inputs      : xm       = x-coordinate of loop midpoint (solar radii)
;               ym       = y-coordinate of loop midpoint 
;               zm       = z-coordinate of loop midpoint 
;               coeff    = coefficients of magnetic potential field model
;		nrank	 = number of selected components according to rank
;
; Outputs     : irank	 = rank order of importance of magnetic field comp
;               coeff_rank[nrank,4] = selected coefficients
;
; History     : 20-Sep-2011, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

;____________________magn charge coefficients__________________________
dim     =size(coeff)
ncomp   =dim(1)
fmag    =coeff(*,0)
xmag    =coeff(*,1)
ymag    =coeff(*,2)
zmag    =coeff(*,3)                     ;negative height
rmag    =sqrt(xmag^2+ymag^2+zmag^2)
hmag    =rmag-1.
weight	=fltarr(ncomp)

;____________________potential field___________________________________
for ic=0,ncomp-1 do begin
 d_mag=sqrt((x1-xmag(ic))^2+(y1-ymag(ic))^2+(z1-zmag(ic))^2)
 b0_p  =fmag(ic)*(hmag(ic)/d_mag)^2
 weight(ic)=b0_p
endfor

;____________________ranking magnetic field compontnets________________
isort	=sort(weight)
irank	=reverse(isort)
isel	=irank[0:nrank-1]
coeff_rank=[[fmag[isel]],[xmag[isel]],[ymag[isel]],[zmag[isel]]]
end

