pro rotate2,x1,y1,z1,x2,y2,z2,az,ax

;two rotations about z-axis (rotation in xy-plane) and x-axis (inclination)

xx	=x1*cos(az)-y1*sin(az)
yy	=x1*sin(az)+y1*cos(az)
zz	=z1
x2	=xx
y2	=yy*cos(ax)-zz*sin(ax)
z2	=yy*sin(ax)+zz*cos(ax)
end

