pro euvi_pos,catfile,index,id,nxx,pos,fov
;+
; Project     : STEREO
;
; Name        : EUVI_POS
;
; Category    : Coordinate transformations
;
; Explanation : reads flare position POS from catalog CATFILE 
;               for event number ID caluclates image pixel coordinates 
;		of the field-of-view with corners FOV=[I1,J1,I2,J2]
;	        and size NXX
;
; Syntax      : IDL> euvi_pos,catfile,index,id,nxx,pos,fov
;
; Inputs      : catfile	   ='~/euvi/euvi_catalog.txt'
;		index     =structure containing FITS header of image
;		id	  =1,...,184 (event identification number in catalog)
;               nxx       =256 pixel size of subimage
;	        pos       ='S06E76' heliographic latitude and longitude
;
; Outputs     ; fov	  =[i1,j1,i2,j2] pixel of FOV corners
;
; History     : 21-Feb-2007, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

;_________________________READ POS FROM CATALOG_______________________________
openr,1,catfile
while not eof(1) do begin
 line=' '
 readf,1,line
 if (strmid(line,5,1) eq '2') then begin
  id_=strmid(line,0,3)
  pos=strmid(line,31,6)
  if (long(id_) eq id) then goto,endfile
 endif
endwhile
print,'EUVI_POS > WARNING: event# not contained in catalog --> no POS, FOV'
goto,endproc	;avoid supersiding FOV in case of a non-catalog event
endfile:

;__________________________COMPUTE PIXEL COORDINATES OF FOV___________________
;heliographic longitude and latitude
  lat_abs=strmid(pos,1,2)
  lon_abs=strmid(pos,4,2)
  if (strmid(pos,0,1) eq 'N') then lat=+lat_abs
  if (strmid(pos,0,1) eq 'S') then lat=-lat_abs
  if (strmid(pos,3,1) eq 'W') then lon=+lon_abs
  if (strmid(pos,3,1) eq 'E') then lon=-lon_abs
;rotation from Earth subsolar point to STEREO/A subsolar point in longitude
  lon_a =lon-index.hgln_obs
  lat_a =lat-index.hglt_obs
  r_sun =index.rsun                 ;solar radius in arcsec
  r_lat =r_sun*cos(lat*!pi/180.)       ;distance from solar axis at latitude
  z_rot =r_lat*cos(lon_a*!pi/180.)
  x_rot =r_lat*sin(lon_a*!pi/180.)
  y_rot2=(r_sun^2-r_lat^2)
  if (y_rot2 le 0.) then y_rot=0.
  if (y_rot2 gt 0.) and (lat gt 0.) then y_rot=+sqrt(y_rot2)
  if (y_rot2 gt 0.) and (lat lt 0.) then y_rot=-sqrt(y_rot2)
;rotation from Earth subsolar point to STEREO/A subsolar point in latitude
  x_a   =x_rot
  y_a   =sqrt(y_rot^2+z_rot^2)*sin(lat_a*!pi/180.)
;position angle rotation from solar North to STEREO/A image North
  xfov = x_a*index.pc1_1-y_a*index.pc1_2
  yfov =-x_a*index.pc2_1+y_a*index.pc2_2
  ic   =index.crpix1
  jc   =index.crpix2
  i0   =ic+xfov/index.cdelt1
  j0   =jc+yfov/index.cdelt2
  i1   =i0-nxx/2
  i2   =i1+(nxx-1)
  j1   =j0-nxx/2
  j2   =j1+(nxx-1)
  nxx  =(i2-i1+1)
  fov  =[i1,j1,i2,j2]
  fov  =long(fov+0.5)	;rounding to integers
endproc:
close,1
end
