pro euvi_table

;generates table of event catalog list in one-line format

;________________________GENERATE CATALOG_______________
catfile	 ='~/euvi/euvi_catalog.txt'
tablefile='~/euvi/euvi_table.txt'
m=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
nev	=0
cgoes	=0
mgoes	=0
nrhessi	=0
ncme	=0
nimp	=0
npost	=0
ndimm	=0
nocc	=0
nerup	=0
nwave	=0

openr,1,catfile
openw,2,tablefile
while not eof(1) do begin
 line=' '
 readf,1,line
 if (strmid(line,0,3) eq '  #') or (strmid(line,0,3) eq 'Cou') then goto,next
 nr	=string(long(strmid(line,0,3)),'(I3)')
 date	=strmid(line,5,11)
 time	=strmid(line,18,11)
 pos	=strmid(line,31,6)
 year	=strmid(line,5,4)
 m_str	=strmid(line,10,3)
 day	=strmid(line,14,2)
 hour	=strmid(line,18,2)
 min	=strmid(line,21,2)
 goes	=strmid(line,49,4)
 kev	=strmid(line,55,6) 
 cts	=strmid(line,61,5) &if (strlen(strtrim(cts,2)) eq 0) then cts='...'
 comment=strmid(line,68,7) &if (strlen(strtrim(comment,2)) eq 0) then comment='...'
 cme    =strmid(line,76,3) &if (strlen(strtrim(cme,2)) eq 0) then cme='...'
 im	=where(m_str eq m)
 if (im le 8) then month='0'+string(im+1,'(I1)')
 if (im ge 9) then month=string(im+1,'(I2)')
 datetime=year+'-'+month+'-'+day+'T'+hour+':'+min+':00'
 ymd =strmid(year,2,2)+'/'+month+'/'+day
 a_rad	=get_stereo_sep_angle(datetime,'A','B')
 a_deg	=a_rad*180./!pi
 angle  =string(a_deg,'(f5.1)')
 ipos   =strpos(kev,'-')
 kev2	=strmid(kev,0,ipos)
 if (kev2 eq '') then kev2='...'
 yymmdd=year+month+day
 tstart =strmid(time,0,2)+strmid(time,3,2)
 file = '~/euvi/'+yymmdd+'/'+yymmdd+'_'+tstart+'_171_'
 euvi_a=' ....'
 euvi_b=' ....'
 sc=['a', 'b']
 for isc=0,1 do begin
  fovfile=file+sc(isc)+'_evol.sav'
  exist=findfile(fovfile,count=nfiles)
  if (nfiles ge 1) then begin
   restore,fovfile
   f_med=median(flare)
   iout    =where((flare lt f_med*0.5) or (flare gt f_med*1.5),nout)
   if (nout ge 1) then flare(iout)=(flare(iout-1)+flare(iout+1))/2.
   nte=n_elements(flare)
   flare_min=min(flare)
   flare_max=max(flare)
   flare_dn =flare_max-flare_min
   if (isc eq 0) then euvi_a=string(flare_dn/1.e5,'(f5.1)')
   if (isc eq 1) then euvi_b=string(flare_dn/1.e5,'(f5.1)')
   test=strpos(comment,'T')
   if ((test ge 0) or (nte le 2)) and (isc eq 0) then euvi_a='?'
   if ((test ge 0) or (nte le 2)) and (isc eq 1) then euvi_b='?'
  endif
 endfor

  text	=nr+' & '+ymd+' & '+time+' & '+pos+' &'+angle+' & '+goes+' & '+$
  kev2+' &'+cts+' &'+euvi_a+' &'+euvi_b+' & '+comment+' & '+cme+' \\'
 printf,2,text
 nev	=nev+1
 ipos=strpos(goes,'M') &if (ipos ge 0) then mgoes=mgoes+1
 ipos=strpos(goes,'C') &if (ipos ge 0) then cgoes=cgoes+1
 ipos=strpos(cts,'...') &if (ipos ge 0) then nrhessi=nrhessi+1
 ipos=strpos(cme,'...') &if (ipos ge 0) then ncme=ncme+1
 ipos=strpos(comment,'I') &if (ipos ge 0) then nimp=nimp+1
 ipos=strpos(comment,'P') &if (ipos ge 0) then npost=npost+1
 ipos=strpos(comment,'D') &if (ipos ge 0) then ndimm=ndimm+1
 ipos=strpos(comment,'O') &if (ipos ge 0) then nocc=nocc+1
 ipos=strpos(comment,'E') &if (ipos ge 0) then nerup=nerup+1
 ipos=strpos(comment,'W') &if (ipos ge 0) then nwave=nwave+1
 next:
endwhile
close,1
close,2
print,'Table file generated = ',tablefile 

print,'Statistics=================================================='
ngoes	=cgoes+mgoes
nrh	=nev-nrhessi
ncm	=nev-ncme
print,'Number of events           N = ',nev
print,'Number of GOES >C1 events  N = ',ngoes,(100.*ngoes/nev),' %'
print,'RHESSI >12 keV events      N = ',nrh,(100.*nrh/nev),' %'
print,'Events with impulsive EUV  N = ',nimp,(100.*nimp/nev),' %'
print,'Events EUV postflare loops  N = ',npost,(100.*npost/nev),' %'
print,'Number of CME events       N = ',ncm,(100.*ncm/nev),' %'
print,'Events EUV occulting       N = ',nocc,(100.*nocc/nev),' %'
print,'Events EUV dimming         N = ',ndimm,(100.*ndimm/nev),' %'
print,'Events EUV waves & osc     N = ',nwave,(100.*nwave/nev),' %'
print,'Number of GOES >M1 events  N = ',mgoes,(100.*mgoes/nev),' %'
print,'Events EUV eruption        N = ',nerup,(100.*nerup/nev),' %'
end
