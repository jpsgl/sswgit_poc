pro nlfff_hmi,dir,savefile,test
;+
; Project     : AIA/SDO, HMI/SDO, IBIS/DST, ROSA/DST
;
; Name        : NLFFF_HMI
;
; Category    : magnetic field moedling
;
; Explanation : reads HMI magnetogram data from Stanford JSOC
;               and writes uncompressed FITS file
;
; Syntax      : IDL>nlfff_hmi,dir,savefile,test
;
; Inputs      : dir	 =directory 
;		savefile =output filename 
;
; Outputs     ; HMI FITS file in dir
;
; History     : 21-Sep-2012, Version 1 written by Markus J. Aschwanden
;               10-Feb-2016, ssw_jsoc_time2data, read_sdo, hmi_prep 
;		             (updates from Sam Freeland and Dave Jess)
;
; Contact     : aschwanden@lmsal.com
;-

print,'__________________NLFFF_HMI_________________________________
filename=strmid(savefile,0,15)
magfile =filename+'_HMI.fits' 
file	=findfile(dir+magfile,count=count)
if (count eq 1) then begin
 print,'HMI file already exists : ',magfile
 goto,skip
endif
year	=strmid(filename,0,4)
month	=strmid(filename,4,2)
day	=strmid(filename,6,2)
hour	=strmid(filename,9,2)
minu	=strmid(filename,11,2)
seco 	=strmid(filename,13,2)
h1	=long(hour)  &h2=h1
m1	=long(minu)  &m2=m1
s1	=long(seco)  
s2	=(s1+13) mod 60
if ((s1+12) ge 60.) then m2=(m1+1) mod 60
if ((m1+ 1) ge 60.) then m2=m2+1
if ((h1+ 1) ge 60.) then h2=h2+1
string0,2,h2,hour2
string0,2,m2,minu2
string0,2,s2,seco2
t1	=year+'-'+month+'-'+day+' '+hour+':'+minu+':'+seco
t2	=year+'-'+month+'-'+day+' '+hour2+':'+minu2+':'+seco2
print,'Start time = ',t1
print,'End   time = ',t2

;READ DATA_______________________________________________
;ssw_jsoc_time2data,t1,t2,index,data,ds='hmi.M_45s'
;ssw_jsoc_time2data,t1,t2,index,files,/FILES_ONLY,ds='hmi.M_45s',/silent
 ssw_jsoc_time2data,t1,t2,ds='hmi.M_45s',index,data,/use_shared,/silent,$
                    /noshell,/comp_delete,/uncomp_delete;,max_files=1
;read_sdo,files,index,data,/USE_INDEX,/noshell,/uncomp_delete,/use_shared

;DISPLAY IMAGE___________________________________________
if (test eq 1) then begin
 loadct,0,/silent
 !p.background=255
 !p.color=0
 window,0,xsize=1024,ysize=1024
 tv,bytscl(rebin(data(*,*),1024,1024),min=-100,max=100)
;tvim,bytscl(rebin(data(*,*),1024,1024),min=-100,max=100),/noframe,title='HMI magnetogram'
endif

;WRITE FITS FILE________________________________________
mwritefits,index,data,outfile=dir+magfile
print,'FITS file written = ',dir+magfile
SKIP:
end
