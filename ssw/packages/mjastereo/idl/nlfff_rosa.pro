pro nlfff_rosa,dir,savefile,test,elist
;+
; Project     : ROSA
;
; Name        : NLFFF_ROSA

; Category    : image processing
;
; Explanation : makes ROSA (Dunn Solar Telescope) FITS image
;
; Syntax      : IDL>nlfff_rosa,dir,savefile,...
;
; Inputs      : dir	 =data directory 
;
; Outputs     ; saves AIA FITS file in dir
;
; History     : 16-Oct-2015, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

restore,dir+savefile    ;--> PARA
filename=strmid(savefile,0,15)
wave_   =para.wave_
instr   =input.instr
if (instr ne 'ROSA') then goto,end_proc

print,'__________________NLFFF_ROSA________________________________'
restore,dir+savefile	;--> para,dateobs,noaa,helpos,instr,wave_
nwave	=n_elements(wave_)
for iwave=0,nwave-1 do begin
 ipos	=strpos(savefile,'ROSA')
 filename=strmid(savefile,0,15)
 datafile=filename+'_ROSA_'+wave_[iwave]+'.fits'
 file_found=findfile(dir+datafile,count=count)
 if (count eq 0) then print,'File not found : '+datafile
 if (count eq 1) then begin
  print,'File already exists : ',datafile
  goto,display
 endif

;________________________PREPARE FILE FROM DAVE JESS__________________'
 if (elist eq 'event_jess.txt') Then begin
  saveset='Halpha_coaligned_HMI_map_'+strmid(savefile,0,8)+'.sav'
  restore,dir+saveset
  wave    ='6563' 			;H-alpha
;..........2014-Aug-24..................2014-Aug-30..................
  data    =halpha_fit_map.data		;halpha_map.data
  xc      =halpha_fit_map.xc		;halpha_map.xc
  yc      =halpha_fit_map.yc		;halpha_map.yc
  dx      =halpha_fit_map.dx		;halpha_map.dx
  dy      =halpha_fit_map.dy		;halpha_map.dy
  date_obs=halpha_fit_map.time		;halpha_map.time
  dim     =size(data)
  nx      =dim(1)
  ny      =dim(2)
  image   =data
  naxis1  =nx
  naxis2  =ny
  cdelt1  =dx
  cdelt2  =dx
  crval1  =0.
  crval2  =0.
  crpix1  =(naxis1+1)/2.-(xc-crval1)/cdelt1
  crpix2  =(naxis2+1)/2.-(yc-crval2)/cdelt2
  mkhdr,h,image				;makes FITS header
 endif

;________________________PREPARE FILE FROM KEVIN REARDON_____________'
 if (elist eq 'event_reardon.txt') then begin
  origfile='H6563.mosaic.spk.wv-0.1A.v3.2.fits'
  fits_read,dir+origfile,data,h
  help,data,h
  dx    =0.0976     ;arcsec per pixel, Kevin Reardon, 2015 Sep 17
  x1    =-168.8     ;arcsec   ;-0.178 solar radii
  x2    =  74.8     ;arcasec  ; 0.079 solar radii
  y1    =  59.3     ;arcsec   ; 0.063 solar radii
  y2    = 301.7     ;arcsec   ; 0.319 solar radii

  dim   =size(data)
  nx    =dim(1)
  ny    =dim(2)
  naxis1=nx
  naxis2=ny
  cdelt1=dx
  cdelt2=dx
  crval1=0.
  crval2=0.
  crpix1=-x1/cdelt1             ;pixel nr of Sun center
  crpix2=-y1/cdelt2             ;pixel nr of Sun center
 endif

 help,naxis1,naxis2,cdelt1,cdelt2,crpix1,crpix2
 sxaddpar,h,'DATE_OBS',dateobs
 sxaddpar,h,'NAXIS1',naxis1
 sxaddpar,h,'NAXIS2',naxis2
 sxaddpar,h,'CDELT1',cdelt1
 sxaddpar,h,'CDELT2',cdelt2
 sxaddpar,h,'CRVAL1',crval1
 sxaddpar,h,'CRVAL2',crval2
 sxaddpar,h,'CRPIX1',crpix1
 sxaddpar,h,'CRPIX2',crpix2
 writefits,dir+datafile,data,h
 print,'datafile written = ',dir+datafile

DISPLAY: ;_______________________________________________
 if (test eq 1) then begin
  fits_read,dir+datafile,data,h
  help,data,h
  dim   =size(data)
  nx    =dim(1)
  ny    =dim(2)
  window,0,xsize=nx/2,ysize=ny/2
  loadct,3
  statistic,data,zavg,zsig
  c1	=zavg-3*zsig
  c2	=zavg+3*zsig
  nxx	=nx/2
  nyy	=ny/2
  tv,bytscl(rebin(data(0:nxx*2-1,0:nyy*2-1),nxx,nyy),min=c1,max=c2)
  wait,3
 endif
endfor	;for iwave=0,nwave-1 do begin
END_PROC: 
end
