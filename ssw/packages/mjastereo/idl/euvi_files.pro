pro euvi_files,datadir,date,tstart,tend,sc,wave,files,nfilemax,expmax

;+
; Project     : STEREO 
;
; Name        : EUVI_FILES
;
; Category    : Database management
;
; Explanation : This routine finds the filenames of EUVI image data
;		on the disk DATADIR that correspond to the time range
;		specified by OBSDATE, TSTART, TEND, from spacecraft
;		SC=A or B, in wavelength filter WAVE. The array of
;		selected filenames is output in array DATAFILES. 
;	 	Beacon data are excluded in the data selection.
;
; Syntax      : IDL> euvi_files,datadir,date,tstart,tend,sc,wave,files
;
; Inputs      : datadir   ='/net/kokuten/archive/stereo/lz/L0/'
;               date      ='20061231' ; YYYYMMDD
;               tstart    ='070000'   ; HHMMSS [UT]
;               tend      ='100000'   ; HHMMSS [UT]
;               sc	  ='a'        ; spacecraft 'a','b'
;		wave      ='171'      ; wavelength '171','195', '284','304'
;		nfilemax   =100        ; maximum number of files
;
; Outputs     ; files = string array of file names
;
; History     : 21-Feb-2007, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

if (n_params(0) le 7) then nfilemax=100 ;max file number 
if (n_params(0) le 8) then begin
 expmax=10. ;max exposure time = 10 s
 if (wave eq '284') then expmax=20.
 if (wave eq '195') then expmax=20.
endif

;finds summary file____________________________________________________
dir	=datadir+sc+'/summary/'
yearmo	=strmid(date,0,6)
search  =dir+'*'+yearmo+'.img'
sumfile	=findfile(search,count=nfiles)
if (nfiles eq 0) then begin			;modified May 15, 2007
 search =dir+'*'+yearmo+'.img.eu'
 sumfile=findfile(search,count=nfiles)
endif
if (nfiles eq 0) then print,'no summary file found'
print,'Summary file found = ',sumfile

;reads content of summaryfile__________________________________________
t1	=date+'_'+tstart
t2	=date+'_'+tend
image_files=strarr(1000)
line	=''
i	=0
t_old	=0.
cadence =[0.]
openr,1,sumfile
while not eof(1) do begin
 readf,1,line 
;image_time=strmid(line,0,17) 
 image_time=strmid(line,0,13) 
 image_wave=strmid(line,88,3)
 image_mode=strmid(line,17,1)	;7=space weather mode
 str_exptime=strmid(line,56,6)
 if (str_exptime eq 'xptime') or (str_exptime eq '======') then $
  exptime=9999. else exptime=float(str_exptime)
 if (image_time ge t1) and (image_time le t2) and $
    (image_wave eq wave) and (image_mode ne '7') and $
    (exptime le expmax) then begin
  image_files(i)=strmid(line,0,25)
  hour   =strmid(line,9,2)
  min	 =strmid(line,11,2)
  sec    =strmid(line,13,2)
  t_sec  =3600.*long(hour)+60.*long(min)+float(sec)
  cadence=[cadence,t_sec-t_old]
  t_old  =t_sec
; print,string(i,'(i4)')+' --> '+strmid(line,0,93) 
  i=i+1
  if (i ge 1000) then begin
   close,1
   stop,'STOP: Excessive number of files (>1000)'+$
      '   Reduce time interval [tstart,tend] !'
  endif
 endif
endwhile
nfile=i
close,1
if (nfile eq 0) then stop,'No files found'

;creates filenames______________________________________________________
ind	=findgen(nfile)
if (nfile ge nfilemax) then begin
 ind	=long(findgen(nfilemax)*float(nfile-1)/float(nfilemax-1))
 print,'nfile    =',nfile
 print,'nfilemax =',nfilemax
 print,'selected=',ind
 nfile	=nfilemax
endif
print,'cadence =',median(cadence), 's'
files=datadir+sc+'/img/euvi/'+date+'/'+image_files(ind)
;for i=0,nfile-1 do print,i,'  ',files(i)
print,'Number of datafiles selected = ',nfile
end
