pro euvi_goes,date,tstart,tend,type,instr,dir
;+
; Project     : STEREO
;
; Name        : EUVI_GOES 
;
; Category    : Database management
;
; Explanation : This routine reads the EUVI summary file
;               and extracts all images from spacecraft A and B in
;		all wavelengths and displays the observing schedule
;		along with a GOES light curve for a daily time
;		interval. A plot is automatically created.
;
; Syntax      : IDL> euvi_goes,date,tstart,tend,type,instr
;
; Input       : date      ='20061231' ; YYYYMMDD
;               tstart    ='000000'   ; HHMMSS [UT]
;               tend      ='240000'   ; HHMMSS [UT]
;               type      ='img'      ;'img','cal', 'seq'
;               instr     ='euvi'
;		dir	  ='~/work/'  ;directory name of output file
;
; Outputs     : postscript file of plot *.ps
;
; History     : 21-Feb-2007, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-


;read EUVI summary file____________________________________________
if (tend eq '240000') then tend='235959'
mon      =['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
date_str =strmid(date,6,2)+'-'+mon(long(strmid(date,4,2))-1)+'-'+strmid(date,2,2)
date_str1=date_str+' '+strmid(tstart,0,2)+':'+strmid(tstart,2,2)+':'+strmid(tstart,4,2)
date_str2=date_str+' '+strmid(tend,0,2)+':'+strmid(tend,2,2)+':'+strmid(tend,4,2)
date2_str=[date_str1,date_str2]
datasum_a= SCC_READ_SUMMARY(DATE=date2_str,TYP=type,spa='a',tel=instr)
datasum_b= SCC_READ_SUMMARY(DATE=date2_str,TYP=type,spa='b',tel=instr)

;read GOES data_____________________________________________________
;time1	='04-DEC-06 00:00:00'
;time2	='04-DEC-06 23:59:59'
time1  =date_str+' '+strmid(tstart,0,2)+':'+strmid(tstart,2,2)+':'+strmid(tstart,4,2)
time2  =date_str+' '+strmid(tend,0,2)+':'+strmid(tend,2,2)+':'+strmid(tend,4,2)
euvi_goes_read,time1,time2,time_goes,goes_low,goes_high

;creates plot_______________________________________________________
form	=0
char	=1
plotname=strmid(date,2,6)+'_'
if (n_params(0) ge 6) then plotname=dir+plotname
fignr    ='goes'
unit	=0
ref	=''
midnight=strmid(time1,0,10)+'00:00:00'
for io=0,1 do begin 
 fig_open,io,form,char,fignr,plotname,unit
 !p.position=[0.1,0.65,0.9,0.95]
 !x.range=minmax(time_goes)
 !y.range=[-9,-3]
 !x.title=date
 !y.title='GOES flux [W/m!U2!N]'
 !x.style=1
 !y.style=1
 setutbase,midnight 		;print,'midnight=',midnight
 utplot,time_goes,alog10(goes_high)
 outplot,time_goes,alog10(goes_low)
 t2	=!x.crange(1)
 t1	=!x.crange(0)
 for i=-8,-4 do oplot,[t1,t2],[i,i],linestyle=2
 xyouts,t2,-4,' X',size=1
 xyouts,t2,-5,' M',size=1
 xyouts,t2,-6,' C',size=1
 xyouts,t2,-7,' B',size=1
 xyouts,t2,-8,' A',size=1
 xyouts,t2+(t2-t1)*0.05,-7,'GOES Class',orientation=90,size=1.0
 !noeras=1

 spacecraft=['A','B']
 for k=0,1 do begin
  if (k eq 0) then datasum=datasum_a
  if (k eq 1) then datasum=datasum_b
  dim	=size(datasum)
  if (dim(0) eq 0) then goto,skip
  !p.position=[0.1,0.375-0.3*k,0.9,0.575-0.3*k]
  !y.range=[0,4]
  !y.title='EUVI Filter'
  image_time =anytim(datasum.date_obs) mod 86400.
  n	=n_elements(image_time)
  image_wave =datasum.value 
  image_file =datasum.filename
  utplot,time_goes,time_goes*0.
  for i=0,4 do oplot,!x.crange,[i,i]
  wave_	=[304., 284., 195.,171.]
  for iw=0,3 do begin
   ind	=where(image_wave eq wave_(iw),nind) 
   if (nind ge 1) then begin
    for it=0,nind-1 do begin
     beacon=strmid(image_file(ind(it)),17,1)
     if (beacon ne '7') then oplot,image_time(ind(it))*[1,1],[iw,iw+1]
    endfor
   endif
   xyouts,!x.crange(1),iw,string(wave_(iw),'(i5)')+' A'
  endfor
  xyouts_norm,0.02,0.05,spacecraft(k),char*3
  SKIP:
 endfor
 fig_close,io,fignr,ref
endfor

end
