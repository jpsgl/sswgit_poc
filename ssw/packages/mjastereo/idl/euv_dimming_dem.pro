pro euv_dimming_dem,dir,iev,vers,test  
;+
; Project     : SDO/AIA,  
;
; Name        : EUV_DIMMING_DEM 
;
; Category    : data analysis of EUV dimming and CME mass, speed, and energy
;
; Explanation : Reads AIA images and performs EUV dimming analysis
;
; Syntax      : IDL>euv_dimming_dem,dir,iev,vers,test
;
; Inputs      : dir,iev,vers,test
;
; Outputs     ;input,para,exptime_[nwave],flux_grid[nx,ny,nwave],
;		x_grid[nx],y_grid[ny],flux_prof[nt,nwave],emtot_prof[nt]
;		telog_prof[nt],tsig_prof[nt],chi_prof[nt],wave_,time[nt]
;		dem_grid[nx,ny,nt],dem_t[nte,nt],te_mk,telog[nte]
;
; History     : 24-Feb-2016, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

print,'__________________EUV_DIMMING_DEM_________________________
string0,3,iev,iev_str
catfile ='dimming_'+iev_str+vers+'.txt'
readcol,dir+catfile,event_,dateobs_,noaa_,helpos_,instr_,$
  w1_,w2_,w3_,w4_,w5_,w6_,w7_,w8_,fov_,$
  format='(I,A,A,A,A,A,A,A,A,A,A,A,A,A)',/silent
nt	=n_elements(event_)
i	=0
wave8   =[w1_(i),w2_(i),w3_(i),w4_(i),w5_(i),w6_(i),w7_(i),w8_(i)]
indw    =where(wave8 ne 0,nwave)
wave_   =strtrim(string(wave8(indw),'(I5)'),2)


;________________________AIA RESPONSE FUNCTION____________________
teem_table='teem_table.sav'
te_range=[0.5,20]*1.e6		;[K] valid temperature range of DEM 
tsig    =0.1*(1+findgen(10))	;values of Gaussian log temp widths
tresp   =aia_get_response(/temp,/full,/dn)
telog_  =tresp.logte
telog1  =alog10(te_range(0))
telog2  =alog10(te_range(1))
ind_te  =where((telog_ ge telog1) and (telog_ le telog2),nte)
telog   =telog_(ind_te)
ichan_  =fltarr(nwave)
resp    =fltarr(nte,nwave)
for iw=0,nwave-1 do begin
 filter ='A'+wave_(iw)
 if (wave_(iw) eq '094') or (wave_(iw) eq '94') then filter='A94'
 ichan  =where(tresp.channels eq filter)
 resp_  =tresp.tresp(*,ichan)
 resp(*,iw)=resp_(ind_te)
endfor

;_____________________CALCULATES LOOPUP TABLES____________________
tablefile=findfile(teem_table,count=count)
if (count eq 0) then begin
 dte1    =10.^telog(1:nte-1)-10.^telog(0:nte-2)
 dte     =[dte1(0),dte1]
 em1     =1.
 nsig    =n_elements(tsig)
 flux    =fltarr(nte,nsig,nwave)
 for i=0,nte-1 do begin
  for j=0,nsig-1 do begin
   em_te =em1*exp(-(telog-telog(i))^2/(2.*tsig(j)^2))
   for iw=0,nwave-1 do flux(i,j,iw)=total(resp(*,iw)*em_te*dte) 
  endfor
 endfor
 save,filename=teem_table,wave_,resp,telog,dte,tsig,flux
 print,'Lookup table created : ',teem_table
endif
restore,teem_table      	;-->wave_,resp,telog,dte,tsig,flux

;__________________FLUX GRID DATA_________________________
time	  =fltarr(nt)
flux_prof =fltarr(nt,nwave)
emtot_prof=fltarr(nt)
telog_prof=fltarr(nt)
tsig_prof =fltarr(nt)
chi_prof  =fltarr(nt)
for it=0,nt-1 do begin
 print,'time step = ',it,nt-1
 nr_it	 =it+1 
 ind     =where((event_) eq nr_it,nind)
 if (nind le 0) then STOP,'NLFFF_EVENT : NO MATCHING EVENT FOUND'
 i       =ind(0)                 ;selected entry
 string0,3,nr_it,it_str
 hh	 =strmid(dateobs_(i), 9,2)
 mm	 =strmid(dateobs_(i),11,2)
 ss	 =strmid(dateobs_(i),13,2)
 time(it)=hh*3600.+mm*60.+ss		;[s]
 if (it ge 1) then $			;midnight jump 
  if (time(it) lt time(it-1)) then time(it)=time(it)+24.*3600.

;__________________FLUX PROFILES____________________________________
 savefile_t='dimming_'+iev_str+'_'+it_str+vers+'.sav'
 restore,dir+savefile_t     	;--> PARA,FLUX_GRID(NX,NY,NWAVE)
 dim	=size(flux_grid)
 nx	=dim(1)
 ny	=dim(2)
 nwave	=dim(3)
 for iwave=0,nwave-1 do flux_prof(it,iwave)=avg(flux_grid(*,*,iwave))
 if (it eq 0) then begin
  dim	=size(flux_grid)
  nx	=dim(1)
  ny	=dim(2)
  em_xy	  =fltarr(nx,ny,nt)
  telog_xy=fltarr(nx,ny,nt)
  tsig_xy =fltarr(nx,ny,nt)
  chi2	  =fltarr(nx,ny,nt)
 endif
 nbin	=input.nbin

;________________DEM ANALYSIS________________________________
 dim    =size(flux)		;precalculated TE+EM loopup table
 nte    =dim[1]
 nsig   =dim[2]
 nwave  =dim[3]
 for i=0,nx-1 do begin			;x,y grid
  for j=0,ny-1 do begin
   em_best =0.
   telog_best=0.
   tsig_best=0.
   flux_obs=reform(flux_grid(i,j,*)) 		;flux per second
   counts=flux_obs*exptime_*nbin^2
   noise =sqrt(counts/exptime_)
   chi_best=9999.
   for k=0,nte-1 do begin
    for l=0,nsig-1 do begin
     flux_dem1=reform(flux(k,l,*))
     em1 =total(flux_obs)/total(flux_dem1)
     flux_dem=flux_dem1*em1
     ind	=where((flux_obs ne 0.),nind)
     npar       =3 
     nfree	=(nind-npar)>3
     chi =sqrt(total((flux_obs(ind)-flux_dem(ind))^2/noise(ind)^2)/nfree)
     if (chi le chi_best) then begin
      em_best    =em1
      telog_best =telog(k)
      tsig_best  =tsig(l)
      chi_best   =chi
     endif
    endfor
   endfor
   em_xy(i,j,it)   =em_best
   telog_xy(i,j,it)=telog_best
   tsig_xy(i,j,it) =tsig_best
   chi2(i,j,it)    =chi_best
  endfor						;x location loop
 endfor							;y location loop
endfor							;time step loop 
 
;_________________SPATIAL SYNTHESIS OF DEM____________________________________
dem_t  =fltarr(nte,nt)
dem_grid=fltarr(nx,ny,nt)
ngrid  =float(nx*ny)
chi_med	=fltarr(nt)
for it=0,nt-1 do begin
 for j=0,ny-1 do begin
  for i=0,nx-1 do begin
   em_best    =em_xy(i,j,it)
   telog_best =telog_xy(i,j,it)
   tsig_best  =tsig_xy(i,j,it) 
   em_kelvin  =em_best*exp(-(telog-telog_best)^2/(2.*tsig_best^2))/ngrid
   dem_grid(i,j,it)=total(em_kelvin*dte)		;DEM localized x,y
   dem_t(*,it)=dem_t(*,it)+em_kelvin		        ;DEM summed x,y	
  endfor
 endfor
 emtot_prof(it)=total(dem_t(*,it)*dte)			;EM [cm-5]
 dem_max=max(dem_t(*,it),imax)				;EM [cm-5 K-1]
 telog_prof(it)=telog(imax)				;T[MK]
 chi_med(it)=median(chi2(*,*,it))
endfor

;__________________INTERPOLATE LOW_EM OUTLIERS__________________
q_out	=0.5
for it=1,nt-2 do begin
 emtot_avg=(emtot_prof(it-1)+emtot_prof(it+1))/2.
 if (emtot_prof(it) lt emtot_avg/2.) then emtot_prof(it)=emtot_avg
endfor

;__________________ELIMINATE BAD DEM FITS_____________________
chi_bad	=5.0
ind_bad	=where(chi_med ge chi_bad,nbad)
if (nbad ge 1) then begin
 for i=0,nbad-1 do begin
  it	  =ind_bad(i)
  it1     =(it-1) 	&if (it eq 0   ) then it1=1 
  it2     =(it+1)       &if (it eq nt-1) then it2=nt-2
  emtot_prof(it)  =(emtot_prof(it1)+emtot_prof(it2))/2.
  dem_grid(*,*,it)=(dem_grid(*,*,it1)+dem_grid(*,*,it2))/2.
  dem_t(*,it)=(dem_t(*,it1)+dem_t(*,it2))/2.
  telog_xy(*,*,it)=(telog_xy(*,*,it1)+telog_xy(*,*,it2))/2.
 endfor
 print,'Eliminate time with bad fit  it_bad=',it
endif
dem_max	=max(dem_t(*,0),imax)
te_mk0  =(10.^telog(imax))/1.e6
te_mk	=(te_mk0 > 1.) < 5.	;1-5 MK preflare temperature

;________________SAVE PARAMETERS______________________________
savefile='dimming_'+iev_str+vers+'.sav'
save,filename=dir+savefile,input,para,exptime_,flux_grid,x_grid,y_grid,$
	flux_prof,emtot_prof,telog_prof,tsig_prof,chi_prof,wave_,time,$
	dem_grid,dem_t,te_mk,telog
print,'parameters saved in file = ',savefile
end
