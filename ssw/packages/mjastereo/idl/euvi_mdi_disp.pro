pro euvi_mdi_disp,io,fignr,code,model,nsm,fileset,fov

form    =1      ;0=landscape, 1=portrait
char    =0.7    ;character size
plotname='f' 	;filename
ref     =''     ;file label
unit    =0      ;window number
dlat	=5	;longitude/latitude grid

;__________________READ STEREO/EUVI SUBIMATES_________________________
euvi_mdi_fov,fileset,para,fov,fov_a,fov_b,image_a,image_b

;__________________READ MAGNETOGRAM___________________________________
search	=fileset+'_magn.fits'
file_m	=findfile(search,count=nfiles_m)
if (nfiles_m eq 0) then print,'EUVI_MDI_DISP: file not found : '+search
if (nfiles_m eq 1) then aia_mag_fov,fileset,fov,image_mag,x,y,nx,ny,dpix

;__________________READ MAGNETIC FIELD MODEL__________________________
search	=fileset+'_coeff.dat'
file_c	=findfile(search,count=nfiles_c)
if (nfiles_c eq 0) then print,'EUVI_MDI_DISP: file not found : '+search
if (nfiles_c eq 1) then begin
 readcol,file_c(0),fmag,xmag,ymag,zmag
 ncomp   =n_elements(fmag)
endif

;__________________READ MAGNETIC FIELD LINES__________________________
fieldfile=fileset+'_loop_'+model+code+'.sav'
fieldfile='MDI'+strmid(fileset,0,13)+'Aa100_fit.sav'	;MODIFY MARCH 10, 2012
restore,fieldfile       ;-->field_lines,field_select,dev_loop
print,'Read field lines from file : ',fieldfile

;____________________READ LOOPFILES_________________________________
fileset0=strmid(fileset,0,17)
readcol,fileset0+'_a_trace.dat',iloopa,xa,ya,za,r,xb,yb,zb 
readcol,fileset0+'_a_coord.dat',iloopc,xc,yc,zc
nloop   =long(max(iloopa))+1
print,'Number of traced loops  NLOOP=',nloop 

;_____________________DISPLAY STEREO/EUVI-A IMAGE______________________
fig_open,io,form,char,fignr,plotname,unit
blue    =50     	
red	=110		
orange	=160
yellow	=200
sc_	 =['B','A']
 for iplot=0,1 do begin
  for isc=0,1 do begin
   if (iplot eq 0) then loadct,3
   if (iplot eq 1) then loadct,0
   if (isc eq 0) then im=image_b
   if (isc eq 1) then im=image_a
   if (iplot eq 0) then im=alog10(im > 1)
   if (iplot eq 1) then im=im-smooth(im,nsm)
   x1_	=0.02+0.5*isc 
   x2_	=x1_+0.46
   y1_	=0.68-iplot*0.33
   y2_	=y1_+0.31
   if (isc eq 0) then fov_ab=fov_b
   if (isc eq 1) then fov_ab=fov_a
   x1   =fov_ab[0]
   x2   =fov_ab[2]
   y1   =fov_ab[1]
   y2   =fov_ab[3]
   !p.title=' '
   !p.position=[x1_,y1_,x2_,y2_]  
   !x.range=[x1,x2]
   !y.range=[y1,y2]
   !x.title=' '
   !y.title=' '
   !x.style=1
   !y.style=1
   if (io eq 0) then begin
    nxw   =long(!d.x_vsize*(x2_-x1_)+0.5)
    nyw   =long(!d.y_vsize*(y2_-y1_)+0.5)
    image =congrid(im,nxw,nyw)
   endif
   if (io ne 0) then image=im
   plot,[0,0],[0,0]
   statistic,im,zavg,zsig
   nsig	=[[-3,+3],[-2,+1]]
   c1	=zavg+nsig(0,iplot)*zsig
   c2	=zavg+nsig(1,iplot)*zsig
   tv,bytscl(image,min=c1,max=c2),x1_,y1_,xsize=(x2_-x1_),ysize=(y2_-y1_),/normal
   coordsphere,1,0,0,0,0,dlat
   xyouts_norm,0.05,0.05,'STEREO/EUVI '+sc_(isc),char*2,0,255
   !noeras=1

;plot loops
   loadct,5
   if (isc eq 0) then begin
    for il=0,nloop-1 do begin
     ind =where(il eq iloopa,ns)	
     xl	 =xb[ind]
     yl	 =yb[ind]
     oplot,xl,yl,thick=5,color=blue
;    xyouts,xl(ns-1),yl(ns-1),string(il,'(I3)'),size=char
    endfor
   endif
   if (isc eq 1) then begin
    for il=0,nloop-1 do begin
     ind   =where(il eq iloopa,ns)	
     xl	=xa[ind]
     yl	=ya[ind]
     oplot,xl,yl,thick=5,color=blue
;    xyouts,xl(ns-1),yl(ns-1),string(il,'(I3)'),size=char
    endfor
   endif

  endfor
 endfor
 !noeras=1

;_____________________DISPLAY MAGNETOGRAM______________________________
image_mag=bzmap
;if (nfiles_m eq 1) then begin
 x1_	=0.52 
 x2_	=0.98
 y1_	=0.02
 y2_	=0.32
 loadct,0
 x1     =fov[0]
 x2     =fov[2]
 y1     =fov[1]
 y2     =fov[3]
 !p.title=' '
 !p.position=[x1_,y1_,x2_,y2_]  
 !x.range=[x1,x2]
 !y.range=[y1,y2]
 if (io eq 0) then begin
  nxw   =long(!d.x_vsize*(x2_-x1_)+0.5)
  nyw   =long(!d.y_vsize*(y2_-y1_)+0.5)
  image =congrid(image_mag,nxw,nyw)
 endif
 if (io ne 0) then image=image_mag
 plot,[0,0],[0,0]
 statistic,image_mag,zavg,zsig
 c1	=zavg-6*zsig
 c2	=zavg+2*zsig
 tv,bytscl(image,min=c1,max=c2),x1_,y1_,xsize=(x2_-x1_),ysize=(y2_-y1_),/normal
 coordsphere,1,0,0,0,0,dlat
 xyouts_norm,0.05,0.05,'SOHO/MDI',char*2
;endif

;_____________________DISPLAY LOOPS______________________________________
 loadct,5
 dim	=size(field_lines)
 nf	=dim[3]

;plot loops 
 for il=0,nloop-1 do begin
  ind =where(il eq iloopc,ns)	
  xl	 =xc[ind]
  yl	 =yc[ind]
  oplot,xl,yl,thick=5,color=blue
 endfor

 for k=0,nf-1 do begin
  ind    =where(field_lines(*,2,k) ne 0,ns)
  if (ns ge 1) then begin
   xl    =field_lines(0:ns-1,0,k)
   yl    =field_lines(0:ns-1,1,k)
   zl    =field_lines(0:ns-1,2,k)
   oplot,xl,yl,color=red,thick=2
  endif
 endfor

;_____________________DISPLAY FIELD LINES IN VERTICAL PROJECTION______________
 x1_	=0.02
 x2_	=0.48
 z1	=min(zc)
 z2	=max(zc)
 aspect	=0.5
 !p.position=[x1_,y1_,x2_,y2_]  
 !p.title=' '
 !y.range=(z1+z2)/2.+aspect*(x2-x1)*[-1,1]	
 !y.title=' '
 phi	=2.*!pi*findgen(361)/360.
 plot,cos(phi),sin(phi)
 coordsphere,1,0,0,-90,0,dlat

;plot loops 
 for il=0,nloop-1 do begin
  ind =where(il eq iloopc,ns)	
  xl	 =xc[ind]
  zl	 =zc[ind]
  oplot,xl,zl,thick=5,color=blue
 endfor

;plot field lines
for k=0,nf-1 do begin
 ind    =where(field_lines(*,2,k) ne 0,ns)
 if (ns ge 1) then begin
  xl    =field_lines(0:ns-1,0,k)
  yl    =field_lines(0:ns-1,1,k)
  zl    =field_lines(0:ns-1,2,k)
  oplot,xl,zl,color=red,thick=2
 endif
endfor

fig_close,io,fignr,ref
end
