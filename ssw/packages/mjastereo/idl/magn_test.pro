;_________________________PARAMETER SELECTION_______________________
inr	=0		;case=0(Low & Lou), 1-6=pot, 7-12=nonpot simul 
imeth	=0		;meth=0,1,2,3
label	='a'		;identification label in file name
ncomp_	=[10,1,2,4,10,10,10,1,2,4,10,10,10]
meth_	=['A','B','C','D']
nmag_	=[2,4,6,8,10,16,20,30,40,50,75,100]
nmag	=1*ncomp_[inr] 	;maximum number of magnetic charges
meth	=meth_[imeth]	;method of optimization
niter	=20   		;minimum number of iterations (0=none)
thresh	=0.		;threshold of magnetic field [G]
ngrid	=8		;8 pixel grid for loop footppoints 
qv	=0.0	 	;0<qv<1, weighting factor of vector B-data
q_mag	=0.001		;residual limit b/bmax of magnetic decomposition
acc	=0.001 	 	;accuracy in alpha optimization
eps	=0.1 		;convergence criterion (delta misalign per iter) 
amax	=100. 		;maximum force-free alpha  
io      =0      	;0=X-window, 1=ps, 2=eps, 3=color ps
dpix	=0.1*(1./29.5)	;pixel size of grid  
ds	=dpix		;spatial resolution of gridpoint (solar radii)
nsm	=0 		;smoothing of magnetogram
qloop	=0.5		;loop position (0.0 < qloop < 1.0)
nseg	=10		;number of loop segments (misalignment angles)
hmax	=dpix*3.5	;maximum coronal altitude (minimum=3*ds)
halt	=0.15		;maximum altitude for field extrapolation
qzone	=0.5		;0.5 zone diminuation factor
iopt	=4		;0,1=no/with Powell optimization of magn sources
para	=[ds,ngrid,nmag,nsm,qloop,nseg,acc,amax,hmax,dpix,niter,qzone,eps,qv,thresh,q_mag,halt,iopt]
if (inr le 9)  then nr='0'+string(inr,'(I1)')
if (inr ge 10) then nr=string(inr,'(I2)')
modelfile='model'+nr				;model00   ... model12
runfile  ='run'+nr+meth+label			;run00Aa ... run12Cz

;______________TASKS______________________________________
;goto,plot 
 if (inr eq 0) then magn_lowlou,modelfile,para
 if (inr ge 1) then magn_simul, modelfile,para,inr
 magn_select,  modelfile,para
 magn_sources, modelfile,runfile,para	    
 magn_fit,     modelfile,runfile,para,meth		
 magn_field,   runfile,para
 magn_cube,    runfile,para
 magn_merit,   runfile,para
 magn_alpha,   runfile,para
PLOT:
 magn_disp2,   modelfile,runfile,para,io    &wait,3 
 magn_disp4,   modelfile,runfile,para,io    &wait,3 
 magn_disp3,   modelfile,runfile,para,io    &wait,3  
end
