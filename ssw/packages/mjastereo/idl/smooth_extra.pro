pro smooth_extra,z,s,w

; PURPOSE:
;	smoothing function which extrapolated edge 
;
; INPUT:
;	z	= array 
;	w	= number of pixels of ramp (to be set flat)
;
; OUTPUT:
;	s	= smoothed array
;
; HISTORY:
;	aschwand@lmsal.com

n	=n_elements(z)
z1	=total(z(0:w-1))/float(w)
z2	=total(z(n-w:n-1))/float(w)
zz	=[fltarr(w)+z1,z,fltarr(w)+z2]
ss	=smooth(zz,w)
s	=ss(w:w+n-1)
end
