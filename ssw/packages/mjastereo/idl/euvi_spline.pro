pro euvi_spline,para,xp,yp,zp,x,y,z,d,r,r_n
;+
; Project     : STEREO
;
; Name        : EUVI_SPLINE
;
; Category    : Data analysis
;
; Explanation : This routine provides a spline interpolation of 3D
;		spline points (xp,yp,zp) with finer resolution (x,y,z),
;		and computes distance along field line (d), distance
;		from Sun center (r), all in pixel units, as well as
;		the distance normalized to units or solar radii (r_n).
;
; Syntax      : IDL>euvi_spline,para,xp,yp,zp,x,y,z,d,r,r_n
;
; Inputs      : para               = index structure with image parameters
;               xp,yp,zp	   = 3D coordinates of spline points (in pixels)
;
; Outputs     : x,y,z	   	   = 3D coordinates interpolated (in pixels)
;		d		   = distance along field line (in pixels)
;		r		   = distance to Sun center (in pixels)
;		r_n		   = distance normalized to solar radii 
;
; History     : 10-Sept-2007, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com

crpix1  =para.crpix1
crpix2  =para.crpix2
cdelt1  =para.cdelt1
cdelt2  =para.cdelt2
rsun    =para.rsun
rad_pix =rsun/cdelt1

np     =n_elements(xp)
dp     =fltarr(np)
for i=1,np-1 do dp(i)=dp(i-1)+sqrt((xp(i)-xp(i-1))^2+(yp(i)-yp(i-1))^2)
rp     =sqrt((xp-crpix1)^2+(yp-crpix2)^2+zp^2)
hp     =(rp/rad_pix-1.)*696.           ;in units of Mm

spline_p,xp,yp,x,y
n     =n_elements(x)
d     =fltarr(n)
for i=1,n-1 do d(i)=d(i-1)+sqrt((x(i)-x(i-1))^2+(y(i)-y(i-1))^2)
if (np eq 2) then z=interpol(zp,dp,d)
if (np eq 3) then z=interpol(zp,dp,d,/quadratic)
if (np ge 4) then z=interpol(zp,dp,d,/spline)
r       =sqrt((x-crpix1)^2+(y-crpix2)^2+z^2)
r_n	=r/rad_pix
end
