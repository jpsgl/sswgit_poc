pro plot_arrow,x1,y1,x2,y2,q1,q2,double,linestyle,color     
;arrow start at  (x1,y1) and arrow end at (x2,y2) 
;q1=thickness/length   q2=arrow-head/length
;x-scale and y-scale assumed to be equal
;double=0 (one-sided arrow), double=2 (double-sided arrow)
;linestyle

if (n_params(0) lt 5) then q1=0.1
if (n_params(0) lt 6) then q2=0.1
if (n_params(0) lt 7) then double=0
if (n_params(0) lt 8) then linestyle=0
if (n_params(0) lt 9) then color=!p.color

if (x1 ne x2) then a=atan((y2-y1)/(x2-x1))
if (x1 eq x2) and (y2 gt y1) then a=+!pi/2.
if (x1 eq x2) and (y2 lt y1) then a=-!pi/2.
if (x2 lt x1) then a=a+!pi
l	=sqrt((y2-y1)^2+(x2-x1)^2)
if (double eq 0) or (double eq 1) then begin
 u1	=-l*q1/2.
 u2	=u1
 u3	=-l*q1
 u4	=0.
 u5	=+l*q1
 u6	=+l*q1/2.
 u7	=u6
 v1	=0.
 v2	=l*(1.-q2)
 v3	=v2
 v4	=l
 v5	=v2
 v6	=v2
 v7	=0.
 xx	=[u1,u2,u3,u4,u5,u6,u7,u1]
 yy	=[v1,v2,v3,v4,v5,v6,v7,v1]
endif
if (double eq 2) then begin
 u1	=0.
 u2	=-l*q2
 u3     =-l*q1/2.
 u4     =u3
 u5     =u2
 u6     =0.
 u7     =+l*q2
 u8     =+l*q1/2.
 u9 	=u8
 u10	=u7
 v1     =0.
 v2	=l*q2
 v3	=v2	
 v4     =l*(1.-q2)
 v5     =v4
 v6     =l
 v7     =v5
 v8     =v5
 v9	=v3
 v10	=v3
 xx     =[u1,u2,u3,u4,u5,u6,u7,u8,u9,u10,u1]
 yy     =[v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v1]
endif
aa	=!pi/2.-a
x	= xx*cos(aa)+yy*sin(aa)
y	=-xx*sin(aa)+yy*cos(aa)
x	=x+x1
y	=y+y1
oplot,x,y,linestyle=linestyle,color=color
end
