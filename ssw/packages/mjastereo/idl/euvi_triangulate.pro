pro euvi_triangulate,index,para,xa,xb,ya,za,zb,footpoints
;+
; Project     : STEREO
;
; Name        : EUVI_TRIANGULATE 
;
; Category    : 3D Modeling 
;
; Explanation : This routine triangulates locations in two
;		coaligned STEREO images. A point (e.g. of a loop)
;	        has the image coordinates (xa,ya) in EUVI/A,
;		and the coordinates (xb,yb) in EUVI/B,
;		where x is coaligned with epipolar planes (yb=ya).
;               The triangulation yields the third cartesian
;		coordinate (za) in EUVI/A and (zb) in EUVI/B.
;               The (x,y) coordinates can be traced with the
;		routine LOOP_TRACING.PRO
;		All coordinates (x,y,z) are in units of solar radii, 
;		with respect to Sun center, za aligned with 
;		line-of-sight of observed in EUVI/A to Sun center.
;
; Syntax      : IDL> euvi_triangulate,index,para,xa,xb,ya,za,zb
;
; Inputs      : index	  = structure (FITS header of image EUVI/A)
;		para      = structure containing coalingment param.
;			    (see EUVI_STEREOPAIR)
;		xa	  = x-coordinates of point in EUVI/A
;		ya	  = y-coordinates of point in EUVI/A
;		xb	  = x-coordinates of point in EUVI/B
;
; Outputs     ; za	  = z-coordinate of point in EUVI/A
;		zb	  = z-coordinate of point in EUVI/B 
;	        footpoints= [lf1,bf1,lf2,bf2] longitudes (lf1,lf2)
;                           and latitudes (lb1,lb2) of footpoints  
;                           in Stoneyhurst coord (deg) for STEREO/A,
;	                    (l,b)=(0,0) at Sun cetner, aligned 
;                           with epipolar plane of spacecraft A,B)
;
; History     : 26-Jan-2009, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

yb	=ya
n	=n_elements(xa)
if (total(ya-yb) ne 0) then stop,'ya not equal to yb!)
asep	=para.sep_deg
al	=asep*(!pi/180.)
if (sin(al) eq 0.) then za=fltarr(n)
if (sin(al) ne 0.) then begin
 za	=sqrt(((xa-xb*cos(al))/sin(al))^2+(xb^2-xa^2))
 ind0	=where(xb eq xa*cos(al),nind0)
 ind	=where(xb lt xa*cos(al),nind)
 if (nind0 ge 1) then za(ind0)=0.
 if (nind  ge 1) then za(ind) =-za(ind)
endif
r	=sqrt(xa^2+ya^2+za^2)
zb	=sqrt(r^2 -xb^2-yb^2)
h	=r-1.
for i=0,n-1 do print,'Altitude h='+string(h(i)*696.,'(f6.1)')+' Mm'

;footpoint longitude and latitudes
q	=(180./!pi)
lf1	=q*asin(xa(0)/sqrt(xa(0)^2+za(0)^2))
bf1	=q*asin(ya(0)/r(0))
lf2	=q*asin(xa(n-1)/sqrt(xa(n-1)^2+za(n-1)^2))
bf2	=q*asin(ya(n-1)/r(n-1))
if (za(0)   lt 0) then lf1=270.-(lf1-270.)
if (za(n-1) lt 0) then lf2=270.-(lf2-270.)
lf1	=lf1 mod 360.
lf2	=lf2 mod 360.
if (lf1 ge 180.) then lf1=lf1-360.
if (lf2 ge 180.) then lf2=lf2-360.
footpoints=[lf1,bf1,lf2,bf2]
end
