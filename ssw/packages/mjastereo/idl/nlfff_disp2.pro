pro nlfff_disp2,dir,catfile,iflare,it,run,overlay,ngrid,bmin
;+
; Project     : SOHO/MDI, SDO/HMI, STEREO
;
; Name        : NLFFF_DISP2
;
; Category    : display of magnetic data modeling
;
; Explanation : displays of choice: magnetogram or filtered EUV image,
;		overlay of field lines or loop tracings
;
; Syntax      : IDL>nlfff_disp2,dir,catfile,it,run,overlay,ngrid,bmin
;
; Inputs      : dir	  = work directory
;		catfile	  = name of catalog file
;		it	  = time step (1,...,nt)
;               run       = label to mark particular run
; 		overlay   = 171=flux log, -171=filter, 0=magnetogram
; 		ngrid     = magnetic field footpoint grid, (0=without grid)
; 		bmin      = [Gauss] min. magnetic field for gridpoints
;
; Outputs     ; postscript file <imagefile>.ps
;
; History     : 16-May-2011, Version 1 written by Markus J. Aschwanden
;                8-Oct-2016, new version 
;             ;  1-May-2017, hmin=input.hmin, hmax=input.hmax
;
; Contact     : aschwanden@lmsal.com
;-

print,'===================NLFFF_DISP====================='
readcol,dir+catfile,event_,dateobs_,noaa_,helpos_,instr_,$
  w1_,w2_,w3_,w4_,w5_,w6_,w7_,w8_,w9_,w10_,w11_,$
  format='(I,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A)',/silent
ind     =where(event_ eq it,nind)
if (nind le 0) then stop,'NLFFF_EVENT : NO MATCHING EVENT FOUND'
i       =ind(0)                 ;selected entry
string0,3,it,it_nr
savefile =dateobs_(i)+'_'+instr_(i)+'_'+it_nr+'_'+run+'.sav'

;_________________________READ MAGNETIC DATA___________________________
restore,dir+savefile    ;--> input,para,bzmap,bzfull,bzmodel,coeff,$
                        ;--> field_loop_det,ns_loop_det,wave_loop_det,$
                        ;--> field_loop,ns_loop,wave_loop,dev_deg,dev_deg2,$
                        ;--> field_lines,ns_loop,ns_field,e_h,e_p,e_np,b2_free,$
                        ;--> field_lines_grid,ns_field_grid
instr	=input.instr
fov0    =input.fov0	;field-of-view [solar radii]
nmag_p  =input.nmag_p
wave_	=para.wave_
filename=strmid(savefile,0,15)
dpix_mag=para.dpix_mag
x1_sun  =para.x1_sun
x2_sun  =para.x2_sun
y1_sun  =para.y1_sun
y2_sun  =para.y2_sun
hmax	=input.hmax
ds	=0.002		;step aplong field line
nsmax   =long(fov0/ds)  
nseg    =9
nwave	=n_elements(wave_)
dim     =size(bzfull)
nx      =dim(1)
ny      =dim(2)
x       =x1_sun+dpix_mag*findgen(nx)
y       =y1_sun+dpix_mag*findgen(ny)
if (overlay eq 0) then image_disp=bzfull
ct	=1
blue	=50
purple	=75
red	=125
yellow	=200 

;________________________READ EUV DATA______________________________
wave	=abs(overlay)
string0,3,wave,wave_str
if (overlay ne 0) then begin
 search=dir+filename+'_'+instr+'_*'+wave_str+'_fov.fits'
 imagefile=findfile(search,count=count)
 if (count eq 0) then STOP,'imagefile does not exist : '+imagefile
 print,'reading FITS file = ',imagefile(0)
 nsm1	=3
 nsm2	=5
 image_lin=readfits(imagefile(0),h,/silent)
 z0	=max(image_lin)*1.e-3
 if (overlay gt 0) then image_disp=alog10(image_lin > z0)
 if (overlay lt 0) then image_disp=smooth(image_lin,nsm1)-smooth(image_lin,nsm2)
endif

;____________________DISPLAY IMAGE_____________________________________
for io=0,3,3 do begin        ;0=screen, 3=color postscript file
 form    =1      ;0=landscape, 1=portrait
 char    =1      ;character size
 fignr	 =''
 ref     =''
 dlat    =5
 unit	 =2
 if (io eq 0) then thick=2
 if (io ne 0) then thick=5
 ipos	=strpos(savefile,'.sav')
 plotname=dir+strmid(savefile,0,ipos)+'2'		;to distinguish from first plot
 fig_open,io,form,char,fignr,plotname,unit
 loadct,ct,/silent
 x1_     =0.05
 x2_     =0.95
 y1_     =0.15
 y2_     =0.85
;!p.title=filename+' '+instr+' '+wave_str+' A'
 !p.title=filename+', time step='+strtrim(string(it,'(I4)'),2)+', Catalog='+catfile
 !p.position=[x1_,y1_,x2_,y2_]
 !x.range=[x[0],x[nx-1]]
 !y.range=[y[0],y[ny-1]]
 !x.title=''
 !y.title=''
 !x.style=1
 !y.style=1
 if (io eq 0) then begin
  nxw   =long(!d.x_vsize*(x2_-x1_)+0.5)
  nyw   =long(!d.y_vsize*(y2_-y1_)+0.5)
  image =congrid(image_disp,nxw,nyw)			;magnetogram 
 endif
 if (io ne 0) then image=image_disp 			;bzmap
 plot,[0,0],[0,0],charsize=0.8
 statistic,image,c0,csig
 if (overlay eq 0) then nsig_=[-5,1]
 if (overlay ne 0) then nsig_=[-2,2]
 c1      =c0+nsig_[0]*csig
 c2      =c0+nsig_[1]*csig
 tv,bytscl(image,min=c1,max=c2),x1_,y1_,xsize=(x2_-x1_),ysize=(y2_-y1_),/normal
 coordsphere,1,0,0,0,0,dlat
 !noeras=1
 
;______________________DISPLAY TRACED LOOPS AND FIELD LINES_______________________
 if (ngrid eq 0) then begin
  nf	=n_elements(ns_loop)
  loadct,5,/silent
  for k=0,nf-1 do begin
   np	=ns_field[k]
   xl	=field_lines(0:np-1,0,k)
   yl	=field_lines(0:np-1,1,k)
   oplot,xl,yl,color=red,thick=thick
   ns	=ns_loop[k]
   xl	=field_loop(0:ns-1,0,k)
   yl	=field_loop(0:ns-1,1,k)
   zl	=field_loop(0:ns-1,2,k)
   oplot,xl,yl,color=yellow,thick=thick
  endfor
 endif

;_____________DISPLAY OF GRID OF FOOTPOINTS OF MAGNETIC FIELD___________________
 if (ngrid ge 1) then begin
  if (io eq 0) then begin
   nf	=ngrid^2
   field_lines_grid=fltarr(nsmax,3,nf)
   ns_field_grid=lonarr(nf)
   dxgrid=nx/float(ngrid)
   dygrid=ny/float(ngrid)
   rm	=1.+ds/2.
   kk	=0
   for j=0,ngrid-1 do begin
    ym	=y1_sun+(y2_sun-y1_sun)*(j+0.5)/float(ngrid)
    iy	=long((j+0.5)*dygrid)
    iy1	=long(    j*dygrid)
    iy2	=long((j+1)*dygrid)<(ny-1)
    for i=0,ngrid-1 do begin
     xm	=x1_sun+(x2_sun-x1_sun)*(i+0.5)/float(ngrid)
     ix	=long((i+0.5)*dxgrid)
     ix1=long(    i*dxgrid)
     ix2=long((i+1)*dxgrid)<(nx-1)
     if (max(abs(bzfull(ix1:ix2,iy1:iy2))) ge bmin) then begin
      rm	=1.+ds/2.
      zm	=sqrt(rm^2-xm^2-ym^2)
      nlfff_fieldline,xm,ym,zm,ds,hmax,coeff,field,np,nsmax
      np	=np < nsmax
      field_lines_grid(0:np-1,0,kk)=field(0:np-1,0)
      field_lines_grid(0:np-1,1,kk)=field(0:np-1,1)
      field_lines_grid(0:np-1,2,kk)=field(0:np-1,2)
      ns_field_grid[kk]=np
      kk	=kk+1
     endif
    endfor
    print,'field line grid at row = ',j,ngrid 
   endfor
   m1      =coeff(*,0)
   x1      =coeff(*,1)
   y1      =coeff(*,2)
   z1      =coeff(*,3)
   r1      =sqrt(x1^2+y1^2+z1^2)
   d1      =1.-r1                                  ;source depth
   nphi    =20
   phi     =2.*!pi*findgen(nphi)/float(nphi-1)
  endif

  loadct,5
  for im=nmag_p-1,0,-1 do begin
   oplot,x1(im)+d1(im)*cos(phi),y1(im)+d1(im)*sin(phi),color=yellow,thick=thick
  endfor
  nf      =n_elements(ns_field_grid)
  for k=0,nf-1 do begin
   ns    =ns_field_grid[k]
   xl    =field_lines_grid(0:ns-1,0,k)
   yl    =field_lines_grid(0:ns-1,1,k)
   zl    =field_lines_grid(0:ns-1,2,k)
   oplot,xl,yl,color=red,thick=thick
  endfor
 endif

 fig_close,io,fignr,ref
endfor	;io=0 screen, io=3 postscript file

end

