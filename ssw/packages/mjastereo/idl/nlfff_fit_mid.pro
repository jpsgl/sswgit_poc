pro nlfff_fit_mid,dir,filename,vers,para,fileprev,fovprev
;+
; Project     : SOHO/MDI, SDO/HMI, STEREO 
;
; Name        : NLFFF_FIT
;
; Category    : Magnetic data modeling 
;
; Explanation : Fitting of twisted field lines (nonlinear force-free
;		field) NLFFF-model to observed loop projections.
;
; Syntax      : IDL>nlfff_fit,dir,filename,vers,para,fileprev,fovprev
;
; Inputs      : dir	  = directory of input/output files
;		filename  = generic filename containing date and time of obs
;		vers      = individual laber for particular run
;               para      = input parameters
;               fileprev  = filename of previous time frame
;		filefov   = field-of-view of previous time frame
;
; Outputs     ; runfile   = output files *_coeff.dat, *_fit.sav
;
; History     : 16-May-2011, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

print,'__________________NLFFF_FIT_____________________________'
t1     =systime(0,/seconds)
dz_mid	=para[0]
dfoot   =para[2]
dsmag   =para[3]
nseg    =long(para[8])
niter   =long(para[9])
nitmin	=long(para[10])
hmax    =para[12]
ds      =para[13]
da0	=para[14]
lmin	=para[16]
x1_	=para[17]
y1_     =para[18]
x2_     =para[19]
y2_     =para[20]
fov	=para[17:20]
iter	=0		;in case of NITER=0
blue	=50

;___________________READ MAGNETOGRAM AND LOOP 2D DATA_____________
savefile=dir+filename+'_field_'+vers+'.sav'
;print,'reading savefile = ',savefile
restore,savefile		;-->para,x,y,bzmap,field_loops,ns_loop
nf	=n_elements(ns_loop)	;number of loops
print,'Number of all loops             NF    =',nf
if (nf eq 0) then goto,save_data

;___________________READ MAGNETIC SOURCES COEFFICIENTS_____________
coeff_file=dir+filename+'_coeff_'+vers+'.dat'	    ;potential field comp
readcol,coeff_file,m1,x1,y1,z1,a1,/silent
nmag	=n_elements(m1)
coeff	=[[m1],[x1],[y1],[z1],[a1]]

;__________________NLFFF COMPONENTS_______________________________
set_nlfff=fltarr(nmag)
set_nlfff[0]=1					    ;first NFFFF source
for im=1,nmag-1 do begin
 dmin	=sqrt(min((x1(0:im-1)-x1(im))^2+(y1(0:im-1)-y1(im))^2))
 if (dmin ge dfoot) then set_nlfff[im]=1
endfor
ind_nlfff=where((set_nlfff eq 1),n_nlfff)
print,'Number of NLFFF sources = ',n_nlfff
print,'#',ind_nlfff,format='(A,100I3)'

;___________________EXTRACT LOOP SEGMENT POSITIONS____________________ 
nfit    =nf*nseg
x_fit   =fltarr(nfit,3)
v_fit   =fltarr(nfit,3)
xm_fit  =fltarr(nf,3)
vm_fit  =fltarr(nf,3)
len	=fltarr(nf)
z_mid	=fltarr(nf)
for k=0,nf-1 do begin    	                    ;number of loops
 ns     =ns_loop[k]
 xl     =field_loops[0:ns-1,0,k]
 yl     =field_loops[0:ns-1,1,k]
 len[k] =ds					    ;projected loop length
 for i=1,ns-1 do len(k)=len(k)+sqrt((xl(i)-xl(i-1))^2+(yl(i)-yl(i-1))^2)
;r_mid	=1.0+len(k)/2.				    ;height of circular loop
 r_mid	=1.0+len(k)/4.				    ;half height of circular loop
;r_mid	=1.0+ds					    ;start with minimum height
 i_mid	=ns/2					    ;midpoint
 z_mid(k)=sqrt(r_mid^2-xl(i_mid)^2-yl(i_mid)^2)     ;z-coordinate at loop midpoint
 oplot,xl,yl,color=blue,thick=2
 for iseg=0,nseg-1 do begin
  im    =long(1+(ns-3)*float(iseg)/float(nseg-1))
  if (iseg eq nseg-1) then im=(ns-2)
  im1   =((im-1)>0)<(ns-1)
  im2   =((im+1)>0)<(ns-1)
  ifit  =k*nseg+iseg
  x_fit[ifit,0]=xl[im]
  x_fit[ifit,1]=yl[im]
  x_fit[ifit,2]=z_mid(k)			    ;all segm have altitude 
  v_fit[ifit,0]=xl[im2]-xl[im1]			    ;  of loop midpoint
  v_fit[ifit,1]=yl[im2]-yl[im1]			    ;v_fit[*,2] not used in 2D
  imid	=k*nseg+nseg/2
  xm_fit[k,0]=x_fit[imid,0]
  xm_fit[k,1]=x_fit[imid,1]
  xm_fit[k,2]=x_fit[imid,2]			    ;altitude at loop midpoint
  vm_fit[k,0]=v_fit[imid,0]
  vm_fit[k,1]=v_fit[imid,1]
 endfor						    ;misalignment angle 
endfor

;________________INITIAL VALUE ESTIMATES___________
if (filename eq fileprev) then print,'NO PREVIOUS ALPHA VALUES (A_i=0)'

;________________USE PREVIOUS VALUES FOR A1[IND_NLFFF]___________
if (filename ne fileprev) then begin
 print,'USE PREVIOUS VALUES OF ALPHA'
 dx_rot	=(fov(0)-fovprev(0))			    ;solar rotation during dt 
 rsun_km =696000.
 print,'SOLAR ROTATION = ',dx_rot*rsun_km,' km'
 coeff_prev=dir+fileprev+'_coeff_'+vers+'.dat'	    ;nonpotential field comp
 file	=findfile(coeff_prev,count=nfile)
 if (nfile eq 0) then goto,skip_prev
 readcol,coeff_prev,m0,x0,y0,z0,a0,/silent
 for im=0,n_nlfff-1 do begin                        ;new NLFFF components
  d2	=((x0-(x1(im)+dx_rot))^2+(y0-y1(im))^2)     ;squared distance
  d2 	=d2 > dsmag^2				    ;avoid zero distance
  w0	=1./d2					    ;weighting factor
  a1(im)=total(a0*w0)/total(w0) 		    ;weighted alpha
 endfor  
SKIP_PREV:
endif

;___________________________ITERATION START________________________ 
angle   =90.
dev_old =90.
iter_best=0
coeff_best=coeff
if (niter eq 0) then goto,end_iter
for iter=0,niter-1 do begin

;_________________________ALPHA GRADIENT OPTIMIZATION_____________
 if (iter ge 1) then begin			;after first altitude adjustment
  grad_alpha=fltarr(nmag)
  for i=0,n_nlfff-1 do begin
   im	=ind_nlfff(i)
   coeff2=coeff
   coeff2(im,4)=coeff(im,4)+da0
   nlfff_misalign,para,coeff2,x_fit,v_fit,dev_deg,dev_med   ;x_fit[imid,2]=altitude
   grad_alpha[im]=angle-dev_med
  endfor
  grad_norm=max(abs(grad_alpha))		;uniform weighting
  grad_alpha=grad_alpha/grad_norm
  coeff(ind_nlfff,4)=coeff(ind_nlfff,4)+da0*grad_alpha(ind_nlfff)
 endif

;________________________INTERMEDIATE MISALIGNMENT ANGLE_______________
 nlfff_misalign,para,coeff,x_fit,v_fit,dev_deg,dev_med
 dev_inter=dev_med

;________________________ALTITUDE GRADIENT OPTIMIZATION________________
 grad_zmid=fltarr(nf)
 for k=0,nf-1 do begin
  xm_fit[k,2]=z_mid[k]+dz_mid
  nlfff_vector,coeff,xm_fit,bfff_                	;B-field at pos x_fit
  vector_product_array2,vm_fit,bfff_(*,0:2),v3,v3_norm,angle_rad  ;2D mis angle
  dev_rad =angle_rad < (!pi-angle_rad)          	;180-deg ambiguity
  dev_deg =(180./!pi)*dev_rad                   	;radian into degrees
  dev_med =median(dev_deg)
  grad_zmid[k]=dev_inter-dev_med
 endfor  
 grad_zmid=grad_zmid/max(abs(grad_zmid))
 z_mid	=z_mid+dz_mid*grad_zmid
 r_chrom=1.+ds
 z_chrom=sqrt(r_chrom^2-xm_fit[*,0]^2-xm_fit[*,1]^2)
 z_mid	=z_mid > z_chrom 				;chromospheric boundary
 for k=0,nf-1 do begin
  xm_fit[k,2]=z_mid[k]					;new coordinate
  ifit  =k*nseg+findgen(nseg)
  x_fit[ifit,2]=z_mid(k)			        ;all segm have altitude 
 endfor

;______________________NEW_MISALIGNMENT ANGLE_2_________________
 nlfff_misalign,para,coeff,x_fit,v_fit,dev_deg,dev_med
 if (dev_med lt angle) or (iter eq 0) then begin    
  angle     =dev_med
  dev_all   =dev_deg
  iter_best =iter 
  coeff_best=coeff
 endif
 print,'NITER, MISALIGNMENT ANGLE --> ',iter,dev_inter,dev_med,format='(a,I4,2f8.2)'
 dev_old=dev_med
 if ((iter-iter_best) ge nitmin) then goto,end_iter

;_______________________END OF ITERATION LOOP_____________________
endfor  		;for iter=0,niter-1
END_ITER:
coeff	=coeff_best
dev_deg=dev_all 

;_______________________SAVE COEFFICIENTS_____________________
coeff_file2=dir+filename+'_coeff_'+vers+'.dat'
openw,2,coeff_file2
for is=0,nmag-1 do begin
 m1     =coeff(is,0)
 x1     =coeff(is,1)
 y1     =coeff(is,2)
 z1     =coeff(is,3)
 a1     =coeff(is,4)
 printf,2,m1,x1,y1,z1,a1
endfor
close,2

;_______________________CPU time_________________________________
t2     =systime(0,/seconds)
cpu    =t2-t1
print,'FORWARD FITTING : CPU time        = ',cpu,format='(a,f8.1)'

;_______________________SAVE PARAMETERS________________________
SAVE_DATA:
save,filename=savefile,para,bzmap,bzmodel,field_loops,ns_loop,dx_euv,nmag,$
      angle,dev_deg,iter,cpu,qb_rebin,qb_model,n_nlfff,ind_nlfff,z_mid
;print,'parameters saved in file = ',savefile
end
