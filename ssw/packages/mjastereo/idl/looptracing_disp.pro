pro looptracing_disp,filename,fov,cutoff,fitrange,para,io,unit

form    =1      ;0=landscape, 1=portrait
char    =1      ;character size
if (io ne 0) then char=0.7
ref     =''	;label at bottom of Fig.        
ct      =[3,3,4,3,0,0]	;color table
nsig	=3	;number of sigmas dynamic range

;____________________READ DATAFILE____________________________
datafiles=filename+['.fits','F.fits','M.fits','L.fits']
loopfile =filename+'.dat1'
savefile =filename+'.sav1'
plotname =filename
fignr	=''
i1	=fov(0)
j1	=fov(1)
i2	=fov(2)
j2	=fov(3)
restore,savefile
aspect	=0.9*(j2-j1)/(i2-i1)
xshift	=(1.-aspect)*0.5

;PLOT____________________________________________________________
fig_open,io,form,char,fignr,plotname,unit
for iplot=0,5 do begin
 loadct,ct(iplot)
 if (iplot le 3) then image=readfits(datafiles(iplot),header)
;im=image(i1:i2,j1:j2)
 im=image
 icol	=iplot mod 2
 irow	=iplot/2
 if (icol eq 0) then begin &x2_=0.49 &x1_=x2_-0.49*aspect &endif
 if (icol eq 1) then begin &x1_=0.50 &x2_=x1_+0.49*aspect &endif
 y2_	=0.98-0.33*irow
 y1_	=y2_-0.32
 !p.title=' '
 if (io eq 0) then begin
  nxw   =long(!d.x_vsize*(x2_-x1_)+0.5)
  nyw   =long(!d.y_vsize*(y2_-y1_)+0.5)
  z	=congrid(im,nxw,nyw)
 endif
 if (io ne 0) then z=im
 statistic,im,avg,dev
 z2	=avg+nsig*dev
 z1	=avg-nsig*dev
 if (iplot eq 2) then begin &z1=0 &z2=3 &endif
 !p.position=[x1_,y1_,x2_,y2_]
 !x.style=5
 !y.style=5
 !x.range=[i1,i2]
 !y.range=[j1,j2]
 plot,[0,0],[0,0]
 if (iplot le 3) then tv,bytscl(z,min=z1,max=z2),x1_,y1_,$
  xsize=(x2_-x1_),ysize=(y2_-y1_),/normal
 !noeras=1

 if (iplot eq 4) then begin
  oplot,[i1,i2-1,i2-1,i1,i1],[j1,j1,j2-2,j2-2,j1]
  for i=0,1000,100 do oplot,[i,i],!y.crange,linestyle=1
  for j=0,1000,100 do oplot,!x.crange,[j,j],linestyle=1
  readcol,loopfile,iloop_,xloop_,yloop_
  iloop1  =min(iloop_)
  iloop2  =max(iloop_)
  nloop	=0
  for iloop=iloop1,iloop2 do begin
   ind    =where(iloop_ eq iloop,np)
   if (np ge 1) then begin
    iloop =iloop_(ind(0))
    xp    =xloop_(ind)
    yp    =yloop_(ind)
    spline_p,xloop_(ind),yloop_(ind),xp,yp
    np    =n_elements(xp)
    len	=0
    for ip=1,np-1 do len=len+sqrt((xp(ip)-xp(ip-1))^2+(yp(ip)-yp(ip-1))^2)
    if (len ge cutoff) then oplot,xp+i1,yp+j1,thick=2
    nloop=nloop+1
   endif
  endfor
  xyouts_norm,0.85,0.95,'N='+string(nloop,'(I4)'),char
 endif

 if (iplot eq 5) then begin
  restore,savefile ;-->para
  rmin	=para(0)
  wid	=para(1)
  nsig	=para(2)
  qfill	=para(3)
  reso	=para(4)
  nstruc=para(5)
  !p.position=[0.55,0.03,0.55+0.43*aspect,0.30]
  !x.range=[10,1000]
  !y.range=[1 , 400]
  !p.title=filename
  !x.title='Loop length L [pixels] '
  !y.title='Number of loops with length >L'
  !x.style=1
  !y.style=1
  plot_oo,[0,0],[0,0]
  label	=['Man','Auto']
  for k=0,1 do begin
   line	=0
   if (k eq 0) then datfile=filename+'.dat0'
   if (k eq 1) then datfile=filename+'.dat1'
   if (k eq 0) then line=2
   readcol,datfile,iloop,x,y 
   len	=fltarr(1000)
   is1	=min(iloop)
   is2	=max(iloop)
   nloop=0 
   for is=is1,is2 do begin
    ind	=where(iloop eq is,np)
    if (np gt 1) then begin
     xi	=x(ind)
     yi	=y(ind)
     dist=total(sqrt( (xi(1:np-1)-xi(0:np-2))^2 + (yi(1:np-1)-yi(0:np-2))^2))
     len(nloop)=dist 
     nloop=nloop+1
    endif
   endfor
   len	=len(0:nloop-1)
   isort=sort(len)
   len_sort=len(isort)
   n_len=findgen(nloop)+1
   x_sort=reverse(len_sort)
   oplot,x_sort,n_len,thick=1+k,linestyle=line
   xyouts,x_sort(nloop-1),n_len(nloop-1),label(k),size=char
   ind	=where((len ge cutoff),nflux)
   minlen=min(len(ind))
   maxlen=max(len(ind))
   medlen=median(len(ind))
   totlen=total(len(ind))
   x1	=alog10(fitrange(0))
   x2	=alog10(fitrange(1))
   xx	=alog10(reverse(len_sort))
   yy	=alog10(n_len)
   ind	=where((xx ge x1) and (yy le x2),nind)
   coeff =linfit(xx(ind),yy(ind),sigma=sig)
   x_	=[x1,x2]
   oplot,10.^x_,10.^(coeff(0)+coeff(1)*x_),linestyle=2-2*k,color=128

   xyouts_norm,0.05,       0.34,'L!Dcutoff!N=',char
   xyouts_norm,0.05,       0.28,'L!Dmax!N=',char
   xyouts_norm,0.05,       0.22,'L!Dmed!N=',char
   xyouts_norm,0.05,       0.16,'N!loop!N=',char
   xyouts_norm,0.05,       0.10,'slope=',char
   xyouts_norm,0.2+0.15*k,0.34,string(cutoff,'(i4)'),char
   xyouts_norm,0.2+0.15*k,0.28,string(maxlen,'(i4)'),char
   xyouts_norm,0.2+0.15*k,0.22,string(medlen,'(i4)'),char
   xyouts_norm,0.2+0.15*k,0.16,string(nflux,'(i4)'),char
   xyouts_norm,0.2+0.15*k,0.10,string(coeff(1),'(f5.2)'),char
   if (k eq 0) then l2=float(maxlen)
   if (k eq 0) then me=float(medlen)
   if (k eq 0) then nl=float(nflux)
   print,'Maximum loop length ratio = '+string(max(len)/l2,'(f4.2)')
   print,'Median loop length ratio  = '+string(medlen/me,'(f4.2)'),char
   print,'Number of loop ratio      = '+string(nflux/nl,'(f4.2)'),char
  endfor
  oplot,cutoff*[1,1],!y.range,linestyle=1
  xyouts_norm,0.2+0.15*2,0.28,string(max(len)/l2,'(f4.2)'),char
  xyouts_norm,0.2+0.15*2,0.22,string(medlen/me,'(f4.2)'),char
  xyouts_norm,0.2+0.15*2,0.16,string(nflux/nl,'(f4.2)'),char
  xyouts_norm,0.2+0.15*0,0.40,'MANU',char
  xyouts_norm,0.2+0.15*1,0.40,'AUTO',char
  xyouts_norm,0.2+0.15*2,0.40,'RATIO',char

  xyouts_norm,0.05,0.78,'w!D!N='+string(wid,'(i4)'),char
  xyouts_norm,0.05,0.72,'r!Dcurv!N ='+string(rmin,'(i4)'),char
  xyouts_norm,0.05,0.66,'n!DT!N='+string(nsig,'(f4.1)'),char
  xyouts_norm,0.05,0.60,'i1-i2='+string(i1,'(I4)')+'-'+string(i2,'(I4)'),char
  xyouts_norm,0.05,0.54,'j1-j2='+string(j1,'(I4)')+'-'+string(j2,'(I4)'),char
  xyouts_norm,0.05,0.48,'t!DCPU!N='+string(tcpu,'(f4.1)')+' s',char
 endif
endfor
fig_close,io,fignr,ref
end
