pro aia_mag_derivative,bi_xy,nx,ny,ds,dbi_dx,dbi_dy

;	bi_xy[nx,ny]	;2D array
;	dbi_dx[nx,ny]	;derivative dBi/dx
;	dbi_dy[nx,ny]	;derivative dBi/dy

bi_sm	=bi_xy

dbi_dx	=fltarr(nx,ny)
for j=0,ny-1 do begin
 y1	=bi_sm(0:nx-3,j)
 y2	=bi_sm(2:nx-1,j)
 dbi_dx(1:nx-2,j)=(y2-y1)/(2.*ds)
 dbi_dx(0,j)=2.*dbi_dx(1,j)-dbi_dx(2,j)		;linear extrapolation
 dbi_dx(nx-1)=2.*dbi_dx(nx-2,j)-dbi_dx(nx-3,j)	;linear extrapolation
endfor

dbi_dy	=fltarr(nx,ny)
for j=1,ny-2 do begin
 y1	=bi_sm(*,j-1)
 y2	=bi_sm(*,j+1)
 dbi_dy(*,j)=(y2-y1)/(2.*ds)
endfor
dbi_dy(*,0)   =2.*dbi_dy(*,1)-dbi_dy(*,2)	;linear extrapolation
dbi_dy(*,ny-1)=2.*dbi_dy(*,ny-2)-dbi_dy(*,ny-3)	;linear extrapolation
end

