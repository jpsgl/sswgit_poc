pro fit_dif_plot,title,x,xh,dh,yh,yfit,ind_fit,noise,dev,x0,a,da,bg,chi_dif,char

!p.title=title
!x.title=''
!y.title=''
if (!p.position(1) le 0.2) then !x.title='Size  x'
if (!p.position(0) le 0.2) then !y.title='Frequency  N(x)'
red	=125
orange	=150

;DIFFERENTIAL SIZE DISTRIBUTION
;fit_dif,x,xh,dh,yh,yfit,ind_fit,x0,a,da,chi_dif,nbin_dif,opt
nev0    =n_elements(x)
nh      =n_elements(xh)
nf      =n_elements(ind_fit)
nev     =total(yh(ind_fit)*dh(ind_fit))
slope   =-(alog10(yfit(2:nh-1))-alog10(yfit(0:nh-3)))/(alog10(xh(2:nh-1))-alog10(xh(0:nh-3)))
slope   =[slope(0),slope,slope(nh-3)]
dslope	=min(abs(slope-a),ip)
npow    =yfit(ip)*(xh/xh(ip))^(-a)
!y.range=[min(yh(ind_fit)),max(yh)]
!x.range=minmax(xh)
!x.style=0
!y.style=0
plot_oo,xh,yh,psym=10
for i=0,nh-1 do oplot,xh(i)*[1,1],yh(i)+noise(i)*[-1,1]
for i=0,nf-1 do if (abs(dev(ind_fit(i))) ge 2) then $
;xyouts,xh(ind_fit(i)),yh(ind_fit(i))*2,string(dev(ind_fit(i)),'(I2)'),size=0.7
oplot,xh,yfit,thick=3,color=red,linestyle=2
oplot,xh(ind_fit),yfit(ind_fit),thick=3,color=red
oplot,xh,npow,thick=2,color=orange,linestyle=2
oplot,xh(ind_fit(0))*[1,1],10.^!y.crange,linestyle=2,color=red
oplot,x0*[1,1],10.^!y.crange,linestyle=1

xyouts_norm,0.05,0.39,'BG = '+string(bg,'(i6)'),char
xyouts_norm,0.05,0.32,'N = '+string(nev0,'(i6)'),char
if (nev lt 1.e5) then str=string(nev,'(i6)')
if (nev ge 1.e5) then str=string(nev,'(E8.2)')
xyouts_norm,0.05,0.25,'N!DF!N = '+str,char,0,red
xyouts_norm,0.05,0.18,'a = '+string(a,'(f5.3)')+'_',char
xyouts_norm,0.05,0.18,'a = '+string(a,'(f5.3)')+'+'+string(da,'(f5.3)'),char
xyouts_norm,0.05,0.11,'x!D0!N = '+string(x0,'(e8.2)'),char
xyouts_norm,0.05,0.04,'chi = '+string(chi_dif(0),'(f7.2)'),char
print,'nev0,nev=',nev0,nev,str
!noeras=1
end


