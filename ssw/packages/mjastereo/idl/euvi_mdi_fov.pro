pro euvi_mdi_fov,fileset,para,fov,fov_a,fov_b,image_a,image_b

;read EUVI image pair (epipolar coaligned) and extracts
;field-of-views [fov_a],[fov_b] corresponding to Earth/MDI [fov]

;________________________________________________________________
savefile=fileset+'_image.sav'
restore,savefile			;-->image_pair,para
dim     =size(image_pair)               ;fltarr[2,2048,2048]
naxis1  =dim(2)
naxis2  =dim(3)
rsun_m  =0.696*1.e9
cdelt1  =para.cdelt1
cdelt2  =para.cdelt2
crpix1  =para.crpix1
crpix2  =para.crpix2
da      =para.da/rsun_m
db      =para.db/rsun_m
rsun    =para.rsun
a_sep   =para.sep_deg*(!pi/180.)
rad_pix =rsun/cdelt1
;.................rotation from Earth FOV to STEREO/A....................
nx      =long((fov(2)-fov(0))*rad_pix)>1                ;pixel per FOV
ny      =long((fov(3)-fov(1))*rad_pix)>1
xa0     =(fov(0)+fov(2))/2.                             ;Earth FOV center
ya      =(fov(1)+fov(3))/2.
lat     =asin((ya<1.)>(-1.))
rlat_a  =(xa0/cos(lat)<1.)>(-1.)
lon_a0  =asin(rlat_a)
lon_a   =lon_a0-a_sep/2.
xa      =sin(lon_a)*cos(lat)
im      =crpix1+xa*rad_pix
jm      =crpix2+ya*rad_pix
r_alt   =1.0
;.................rotation from STEREO/A to B...........................
euvi_stereo_rot2,im,jm,r_alt,para,im_b,jm_b,zm_b           ;MODIFY OCT 2011
i1_a    =long(im-nx/2)>0
nx      =long((im-i1_a)*2)
i2_a    =i1_a+(nx-1)
i1_b    =long(im_b-nx/2)>0
i2_b    =i1_b+(nx-1)
j1      =long(jm-ny/2)>0
ny      =long((jm-j1)*2)
j2      =j1+(ny-1)
;.................subimage extraction....................................
image_a =float(reform(image_pair(1,i1_a:i2_a,j1:j2)))
image_b =float(reform(image_pair(0,i1_b:i2_b,j1:j2)))
print,'EUVI/A pixel range i1,i2,j1,j2=',i1_a,i2_a,j1,j2
print,'EUVI/B pixel range i1,i2,j1,j2=',i1_b,i2_b,j1,j2
x1a	=(i1_a-crpix1)/rad_pix
x2a	=(i2_a-crpix1)/rad_pix
x1b	=(i1_b-crpix1)/rad_pix
x2b	=(i2_b-crpix1)/rad_pix
y1	=(j1-crpix2)/rad_pix
y2	=(j2-crpix2)/rad_pix
fov_a	=[x1a,y1,x2a,y2]
fov_b	=[x1b,y1,x2b,y2]
end
