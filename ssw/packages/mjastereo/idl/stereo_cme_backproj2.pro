io	=0	;0=X-window, 1=ps, 2=eps, 3=color ps
form	=1	;0=landscape, 1=portrait
char	=0.6	;character size 
fignr	='9c'  	;figure number
plotname='oxnard_f'	;filename
ref	=' (Aschwanden et al. 2002)'  ;label at bottom of Fig.	
unit	=0	;window number
angle	=50

;Density distribution model
nx	=512
ny	=nx
wid	=nx/10.
x	=findgen(nx)
y	=findgen(ny)
model	=fltarr(nx,ny)
ns	=5
x0	=nx*[0.1,0.3,0.5,0.6,0.65]
y0	=ny*[0.7,0.65,0.6,0.4,0.2]
a0	=1.*[0.5,0.8,1.0,0.85,0.75]
wx	=nx*[0.1,0.12,0.1,0.08,0.05]
wy	=nx*[0.1,0.12,0.1,0.09,0.08]
for is=0,ns-1 do begin 
 for j=0,ny-1 do model(*,j)=model(*,j)+a0(is)*exp(-((x-x0(is))^2/(2.*wx(is)^2)+(y(j)-y0(is))^2/(2.*wy(is)^2)))
endfor

;STEREO-A,B view 
STEREO:
stereoa =fltarr(nx)
for j=0,ny-1 do stereoa(*)=stereoa(*)+model(*,j)	;projection in direction of STEREO-A (y-axis)

stereob =fltarr(nx)
if (angle mod 180) eq 0 then stereob=stereoa
if (angle mod 180) ne 0 then begin
 modelb	=rot(model,angle)
 x_	=fltarr(nx)+x(ny/2)
 y_	=y
 rotate1,x_,y_,xb,yb,nx/2,ny/2,angle
 for i=0,nx-1 do stereob(i)=total(modelb(i,*))		;projection in direction of STEREO-B 
endif

;3D-RECONSTRUCTION
recon	=fltarr(nx,ny)
a	=(angle/180.)*!pi
nq	=20	;number of stacked ellipticals
i1a	=fltarr(nq)
i2a	=fltarr(nq)
i1b	=fltarr(nq)
i2b	=fltarr(nq)
xa	=fltarr(nq)
xb	=fltarr(nq)
wa	=fltarr(nq)
wb	=fltarr(nq)
ymaxa	=max(stereoa)
ymaxb	=max(stereob)
q	=(findgen(i)+0.5)/float(nq)
ya	=fltarr(nq)
yc	=float(ny)/2
xc	=float(nx)/2
for i=0,nq-1 do begin
 i1a(i)	=min(where(stereoa ge ymaxa*q(i)))
 i2a(i)	=max(where(stereoa ge ymaxa*q(i)))
 i1b(i)	=min(where(stereob ge ymaxb*q(i)))
 i2b(i)	=max(where(stereob ge ymaxb*q(i)))
 xa(i) =(x(i1a(i))+x(i2a(i)))/2.
 xb(i) =(x(i1b(i))+x(i2b(i)))/2.
 wa(i) =(x(i2a(i))-x(i1a(i)))/2.
 wb(i) =(x(i2b(i))-x(i1b(i)))/2.
 ya(i) =yc+((xb(i)-xc)/cos(a)+xc-xa(i))*tan(!pi/2.-a)
endfor

for is=0,nq-1 do begin 
 a0	=1.
 ww	=(wa+wb)/2.
 for j=0,ny-1 do recon(*,j)=recon(*,j)+a0*exp(-((x-xa(is))^2/(2.*ww(is)^2)+(y(j)-ya(is))^2/(2.*ww(is)^2)))
endfor
;recon=recon*total(model)/total(recon)  ;renormalization
recon=recon*max(model)/max(recon)  ;renormalization

;DISPLAY
fig_open,io,form,char,fignr,plotname,unit 
!p.position=[0.1,0.7,0.45,0.95]
!p.title='Model density distribution n(x,z)'
!p.charsize=char
!x.range=[0,nx]
!y.range=[0,ny]
!x.style=1
!y.style=1
nlevel	=10
level	=max(model)*(findgen(nlevel)+1.)/float(nlevel)
contour,model,x,y,level=level
oplot,[1,1]*(nx/2),[0,ny]
xyouts,nx/2.+10,10,'A',size=char
oplot,xb,yb
xyouts,xb(0),yb(0),'B',size=char
oplot,xa,ya,psym=4
!noeras=1

!p.position=[0.1,0.4,0.45,0.65]
!p.title='STEREO A LOS_integration  n(x)'
!y.range=[0,max(stereoa)]
plot,x,stereoa,thick=3
oplot,[1,1]*(nx/2),!y.range
xyouts,nx/2.+10,10,'A',size=char
for i=0,nq-1 do oplot,xa(i)+wa(i)*[-1.,+1.],[stereoa(i1a(i)),stereoa(i2a(i))],psym=1
for i=0,nq-1 do oplot,xa(i)*[1.,1.],q(i)*ymaxa*[1,1],psym=4

!p.position=[0.55,0.4,0.9,0.65]
!p.title='STEREO B LOS_integration  n(x_rot)'
!y.range=[0,max(stereob)]
plot,x,stereob,thick=3
oplot,[1,1]*(nx/2),!y.range
xyouts,nx/2.+10,10,'B',size=char
for i=0,nq-1 do oplot,xb(i)+wb(i)*[-1.,+1.],[stereob(i1b(i)),stereob(i2b(i))],psym=1
for i=0,nq-1 do oplot,xb(i)*[1.,1.],q(i)*ymaxb*[1,1],psym=4

!p.position=[0.1,0.1,0.45,0.35]
!p.title='Reconstructed density distribution n(x,z)'
!p.charsize=char
!x.range=[0,nx]
!y.range=[0,ny]
!x.style=1
!y.style=1
contour,recon,x,y,level=level

!p.position=[0.55,0.1,0.9,0.35]
!p.title='Difference [recon-model]'
!p.charsize=char
!x.range=[0,nx]
!y.range=[0,ny]
!x.style=1
!y.style=1
contour,recon-model,x,y,level=level
contour,model-recon,x,y,level=level,c_linestyle=1

fig_close,io,fignr,ref
end
