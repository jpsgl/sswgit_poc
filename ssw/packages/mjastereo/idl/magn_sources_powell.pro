function magn_sources_powell,coeff_m

; function supporting the Powell optimization in MAGN_SOURCES.PRO 

common powell_var,image_local,x_local,y_local,nlocal,coeff,icomp

for i=0,3 do coeff[icomp,i]=coeff_m[i]
aia_mag_map,coeff,1.,x_local,y_local,model_map
image_diff=image_local-model_map
dev	=sqrt(total(image_diff^2)/nlocal)
return,dev
end
