pro aia_mag_dec,fileset,fov,ns,nsm
;+
; Project     : AIA
;
; Name        : AIA_MAG_DEC
;
; Category    : SDO/HMI data analysis
;
; Explanation : reads HMI magnetogram image and decomposes into 
;		NS Gaussian components of unipolar magnetic charges
;		and stores coefficients in file COEFF_FILE.
;
; Syntax      : IDL> aia_mag_dec,fileset,fov,ns,detph,nsm
;
; Inputs      : fileset   = filename of magnetogram fits file
;               fov       = field-of-view [x1,y1,x2,y2] in solar radii
;		ns	  = number of unipolar magnetic charges
;		nsm	  = smoothing of magnetogram
;
; Outputs     ; coeff_file= output file containing [m,x,y,z]   	
;
; History     : 16-May-2011, Version 1 written by Markus J. Aschwanden
;		11-Aug-2011, modified 
;
; Contact     : aschwanden@lmsal.com
;-

;read MDI file____________________________________________________
aia_mag_fov,fileset,fov,image_mag,x,y,nx,ny,dpix
x1      =fov(0)
x2      =fov(2)
y1      =fov(1)
y2      =fov(3)

ind_pos	=where(image_mag gt 0,npos)
ind_neg	=where(image_mag lt 0,nneg)
if (npos ge 1) then flux_pos=total(image_mag(ind_pos))
if (nneg ge 1) then flux_neg=total(image_mag(ind_neg))

;Gaussian width of max magnetic field component..................
bmax	=max(abs(image_mag),im)
iy	=long(im/long(nx))	
result  =gaussfit(x,abs(image_mag(*,iy)),a,nterms=3)
wfit	=(long(a(2)/dpix+0.5)>2) 	

;subimage with zero boundaries...................................
iborder	=wfit
if (nsm lt 2) then image_smo=image_mag
if (nsm ge 2) then image_smo=smooth(image_mag,nsm)
image_mdi_sub=float(image_smo)
for iw=0,iborder-1 do begin
 image_mdi_sub(*,iw) =0
 image_mdi_sub(*,ny-1-iw)=0
 image_mdi_sub(iw,*) =0
 image_mdi_sub(nx-1-iw,*)=0
endfor

;Gaussian integral..........................................
nsig    =wfit
sig_    =0.5*(1+findgen(nsig))
qsig_   =fltarr(nsig)
nfit    =wfit*2+1
for isig=0,nsig-1 do begin
 qsig_(isig)=0.
 for ix=-wfit,wfit do begin
  for iy=-wfit,wfit do begin
   qsig_(isig)=qsig_(isig)+exp(-(ix^2+iy^2)/(2.*sig_(isig)^2))/float(nfit)^2
  endfor
 endfor
endfor

;Decomposition of MDI subimage..............................
x      =x1+(x2-x1)*findgen(nx)/float(nx)
y      =y1+(y2-y1)*findgen(ny)/float(ny)
coeff	=fltarr(ns,4)
polar	=[+1.,-1]
print,'  IS       IPOL     M       X       Y       Z      -H '
magn	=max(abs(image_mdi_sub))		;max field strength
print,'MAXIMUM MAGNETIC FIELD STRENGTH + ',magn,' Gauss'
image_sub=float(image_mdi_sub)			
qsig	=sqrt(2.*alog(2.))			
icomp	=0
for is=0,ns/2-1 do begin
for ipol=0,1 do begin
 if (ipol eq 0) then b0=max(image_sub,im)
 if (ipol eq 1) then b0=min(image_sub,im)
 iy	=long(im/long(nx))	
 ix	=im-iy*long(nx)
 bz	=abs(image_sub(ix,iy))
 i3	=(ix-wfit)>0
 i4	=(ix+wfit)<(nx-1)
 j3	=(iy-wfit)>0
 j4	=(iy+wfit)<(ny-1)
 n34	=(i4-i3+1)*(j4-j3+1)
;.................................................
;fit_x  =gaussfit(x[i3:i4],abs(image_sub[i3:i4,iy]),ax,nterms=3)
;fit_y  =gaussfit(y[j3:j4],abs(image_sub[ix,j3:j4]),ay,nterms=3)
;.................................................
 xp	=x[ix]  	;ax[1]
 yp	=y[iy] 		;ay[1]
 zp	=sqrt(1.-xp^2-yp^2)
 polar	=image_sub(ix,iy)/bz
 qint   =total(image_sub(i3:i4,j3:j4))/(bz*float(n34)*polar)
 sig	=interpol(sig_,qsig_,qint)		;interpolation 
 wp	=sig*qsig*dpix			        ;HW in solar radii 		
 aia_mag_inversion,bz,xp,yp,zp,wp,bm,xm,ym,zm
;print,icomp,ipol,bm,xm,ym,zm,format='(2i6,4f10.6)'
 bm	=bm*polar
 coeff(icomp,0)=bm
 coeff(icomp,1)=xm
 coeff(icomp,2)=ym
 coeff(icomp,3)=zm
 aia_mag_map,coeff[icomp,*],1.,x,y,model_coeff
 image_sub=image_sub-model_coeff
 icomp	=icomp+1
endfor
endfor
statistic,image_sub,zav,zsig
print,'Residual statistic (average and standard deviation) = ',zav,zsig

;iterative superposition of Gaussian (unipolar/dipole) magnetic charges
aia_mag_map,coeff,1.,x,y,model

print,'Magnetogram map         - min/max = ',minmax(image_mag)
print,'Magnetogram map created - min/max = ',minmax(model)
ind_pos	=where(model gt 0,npos)
ind_neg	=where(model lt 0,nneg)
fluxratio=[0.,0.]
if (npos ge 1) then begin
 rec_pos=total(model(ind_pos))
 fluxratio[0]=rec_pos/flux_pos
 print,'Total positive flux (input/output)= ',flux_pos,rec_pos,fluxratio[0]
endif
if (nneg ge 1) then begin
 rec_neg=total(model(ind_neg))
 fluxratio[1]=rec_neg/flux_neg
 print,'Total negative flux (input/output)= ',flux_neg,rec_neg,fluxratio[1]
endif

;write file with components______________________________________________
coeff_file=fileset+'_coeff.dat'
openw,2,coeff_file(0)
for is=0,ns-1 do begin
 m1	=coeff(is,0)
 x1	=coeff(is,1)
 y1	=coeff(is,2)
 z1	=coeff(is,3)
 printf,2,m1,x1,y1,z1
endfor
close,2

;display HMI FOV image__________________________________________________
diff_image=image_mag-model
window,0,xsize=nx*4,ysize=ny*4
gauss	=magn*0.5
loadct,0
tv,bytscl(rebin(image_mag,nx*2,ny*2) ,min=-gauss,max=+gauss),0,ny*2
tv,bytscl(rebin(model,nx*2,ny*2),min=-gauss,max=+gauss),0,0
tv,bytscl(rebin(diff_image,nx*2,ny*2),min=-gauss,max=+gauss),nx*2,ny*2
!noeras=1
!p.position=[0.,0.5,0.5,1.]
!x.style=1
!y.style=1
!x.range=[fov(0),fov(2)]
!y.range=[fov(1),fov(3)]
!p.position=[0.55,0.05,0.95,0.45]
!x.range=minmax(x)
!y.range=[-1,1]*magn
plot,!x.range,[0,0],linestyle=1
zmax	=max(image_mag,im)
iy	=long(im/long(nx))	
image_model=model(*,iy)
;image_model=interpol(model(*,iy),x,x-0.5*dpix)
oplot,x,image_model,thick=3
oplot,x,image_mag(*,iy),color=150
;oplot,x,image_mag(*,iy)-image_model,color=200
end

