pro nlfff_profiles,dir,catfile,label,time,efree,epot,epot_corr,amis,nloop,ndet,date

;extract time profiles

readcol,dir+catfile,event_,dateobs_,noaa_,helpos_,instr_,$
   w1_,w2_,w3_,w4_,w5_,w6_,w7_,w8_,w9_,w10_,w11_,$
   format='(I,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A)',/silent
nt	=n_elements(event_)
savefiles=findfile(dir+'*'+label+'.sav',count=count)
print,'Number of time steps      = ',nt
nt	=nt < count
time   =fltarr(nt)
efree  =fltarr(nt)
amis   =fltarr(nt)
nloop  =fltarr(nt)
ndet   =fltarr(nt)
epot   =fltarr(nt)
dev_deg =fltarr(nt)
e_p    =0.
e_np   =0.
day_start  =strmid(dateobs_(0),6,2)
day_corr=0.
for it=0,nt-1 do begin
 restore,savefiles(it)
 print,it,nt,'   ',savefiles(it)
 dd	  =strmid(dateobs_(it),6,2)
 hh       =strmid(dateobs_(it),9,2)
 mm       =strmid(dateobs_(it),11,2)
 ss       =strmid(dateobs_(it),13,2)
 if (dd eq day_start+1) then day_corr=24.
 time(it) =day_corr+hh+mm/60.+ss/3600.
 efree(it)=(e_np-e_p)/1.e30
 epot(it) =e_p/1.e30
 nloop(it)=n_elements(wave_loop)
 ndet(it) =para.ndet
 amis(it) =median(dev_deg)
 if (it eq 0) then date=strmid(dateobs_(it),0,8)
endfor

;correct potential energy fluctuations (linear regression fit)
if (nt ge 2) then begin
 emed	=epot
 for it=1,nt-2 do emed(it)=median(epot(it-1:it+1))
 emed(0)=emed(1)
 emed(nt-1)=emed(nt-2)
ind	=where((emed gt 0) and (time ne 0))
 x	=time-time(0)
 c	=linfit(x(ind),emed(ind))
 epot_corr=c(0)+c(1)*x
 ind	=where(epot gt 0)
 efree(ind)=efree(ind)*(epot_corr(ind)/epot(ind))
endif
end

