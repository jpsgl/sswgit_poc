pro nlfff_disp_trace,catfile,iev,run,dir,io
;+
; Project     : SOHO/MDI, SDO/HMI, STEREO
;
; Name        : NLFFF_DISP1
;
; Category    : display of magnetic data modeling
;
; Explanation : displays magnetogram and observed and fitted magnetic
;		field lines in top-view and side-view projection.
;
; Syntax      : IDL>nlfff_disp,imagefile,vers,para,io
;
; Inputs      : imagefile = input filename *_loops.sav
;               para      = input parameters
;		io	  = ;0=X-window, 1=ps, 2=eps, 3=color ps
;
; Outputs     ; postscript file <imagefile>.ps
;
; History     : 16-May-2011, Version 1 written by Markus J. Aschwanden
;               30-Jan-2012, Version 2 released to SSW
;
; Contact     : aschwanden@lmsal.com
;-

print,'===================NLFFF_DISP_TRACE====================='
readcol,dir+catfile,event_,dateobs_,noaa_,helpos_,instr_,$
  w1_,w2_,w3_,w4_,w5_,w6_,w7_,w8_,w9_,w10_,w11_,$
  format='(I,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A)',/silent
ind     =where(event_ eq iev,nind)
if (nind le 0) then stop,'NLFFF_EVENT : NO MATCHING EVENT FOUND'
i       =ind(0)                 ;selected entry
string0,3,iev,iev_nr
savefile =dateobs_(i)+'_'+instr_(i)+'_'+iev_nr+'_'+run+'.sav'

;_________________________READ DATA___________________________
restore,dir+savefile    ;--> INPUT,PARA
instr   =input.instr
fov0    =input.fov0      ;field-of-view [solar radii]
nmag_p	=input.nmag_p	;magnetic components
nmag_np	=input.nmag_np	;magnetic components
nsm1    =input.nsm1      ;lowpass filter

event   =para.event
dateobs =para.dateobs
nsm2	=nsm1+2
wave_   =para.wave_
filename=strmid(savefile,0,15)
x1_sun  =para.x1_sun
x2_sun  =para.x2_sun
y1_sun  =para.y1_sun
y2_sun  =para.y2_sun
blue	=50
red	=110
yellow	=200
orange	=150
ct	=3
dlat    =5
set_plot,'X'

;___________________MAGNETIC CHARGES____________________________
m1	=coeff(*,0)
x1	=coeff(*,1)
y1	=coeff(*,2)
z1	=coeff(*,3)
r1	=sqrt(x1^2+y1^2+z1^2)
d1	=1.-r1					;source depth
nphi	=20
phi	=2.*!pi*findgen(nphi)/float(nphi-1)

;_______________________PLOT IMAGE_______________________________
ind0	=where(long(wave_) ne 0,nw)
wave_	=wave_(ind0)
window,0,xsize=512*4,ysize=512*2
for iw=0,nw-1 do begin
 wave	=wave_[iw]
 search=dir+filename+'_'+instr+'_*'+wave+'_fov.fits'
 imagefile=findfile(search,count=count)
 if (count eq 0) then goto,skip_wave
 print,'reading FITS file = ',imagefile(0)
 image1 =readfits(imagefile(0),h,/silent)
 image2 =smooth(image1,nsm1)-smooth(image1,nsm2)

 loadct,0
 icol	=iw mod 4
 irow	=iw/4
 x1_    =float(icol)/4.
 x2_    =x1_+0.24
 y1_	=0.5-0.5*irow
 y2_	=y1_+0.49
 !p.position=[x1_,y1_,x2_,y2_]
 !x.range=[x1_sun,x2_sun]
 !y.range=[y1_sun,y2_sun]
 !x.style=5
 !y.style=5
 !x.title=''
 !y.title=''
 nxw    =long(!d.x_vsize*(x2_-x1_)+0.5)
 nyw   	=long(!d.y_vsize*(y2_-y1_)+0.5)
 im     =congrid(image2,nxw,nyw)
 statistic,im,c0,csig
 c1      =c0-1.*csig
 c2      =c0+1.*csig
 plot,[0,0],[0,0]
 tv,bytscl(im,min=c1,max=c2),x1_,y1_,xsize=(x2_-x1_),ysize=(y2_-y1_),/normal
 xyouts_norm,0.01,0.92,string(wave_[iw],'(I4)')+' A',3,0,255
 !noeras=1

;____________________PLOT FIELD LINES_______________________________
 nf      =n_elements(ns_loop)
 loadct,5
 qloop	=0.5
 for k=0,nf-1 do begin
  ns	=ns_loop[k]
  xl	=field_loop(0:ns-1,0,k)
  yl	=field_loop(0:ns-1,1,k)
  zl	=field_loop(0:ns-1,2,k)
  is	=long(qloop*(ns-1))
  if (wave_loop(k) eq long(wave)) then oplot,xl,yl,color=blue,thick=2
 endfor

;___________________PLOT MAGNETIC CHARGES____________________________
 for im=nmag_p-1,0,-1 do begin
  if (im le nmag_np-1) then color=red
  if (im gt nmag_np-1) then color=blue
  oplot,x1(im)+d1(im)*cos(phi),y1(im)+d1(im)*sin(phi),color=color,thick=3
 endfor
SKIP_WAVE:
endfor
end

