pro vector_product_array2,a,b,c,c_norm,angle

;VECTORIZED version of procedure VECTOR_PRODUCT.PRO 
;
;calculates vector product c = a x b, with normalization c_norm=c/|c|
;a[*,3]  =vector array with x=a[*,0], y=a[*,1], z=a[*,2]
;b[*,3]  =vector array with x=b[*,0], y=b[*,1], z=b[*,2]
;c[*,3]  =vector array with x=c[*,0], y=c[*,1], z=c[*,2]
;angle[*]=vector array of angles betwee a and b in units radians [0,...,pi]
;
;History: 21-Feb-2012 written by Markus J. Aschwanden

dima	=size(a)	&na=dima[1]
dimb	=size(b)	&nb=dimb[1]
if (na ne nb) then print,'VECTOR_PRODUCT_ARRAY2: a,b not equal length'

cx      =fltarr(na)
cy      =fltarr(na)
cz      =a[*,0]*b[*,1]-a[*,1]*b[*,0]
c 	=[[cx],[cy],[cz]]
a_abs 	=sqrt(a[*,0]^2+a[*,1]^2+a[*,2]^2)
b_abs 	=sqrt(b[*,0]^2+b[*,1]^2+b[*,2]^2)
c_abs 	=sqrt(c[*,0]^2+c[*,1]^2+c[*,2]^2)
c_norm	=fltarr(na,3)
c_norm[*,0]=cx/c_abs
c_norm[*,1]=cy/c_abs
c_norm[*,2]=cz/c_abs
sin_c	=fltarr(na)
ind 	=where((a_abs ne 0.) and (b_abs ne 0.),nind)
sin_c(ind)=c_abs(ind)/(a_abs(ind)*b_abs(ind))
angle	=asin((sin_c > (-1.)) < 1.)
angle_sign=a[*,0]*b[*,0]+a[*,1]*b[*,1]+a[*,2]*b[*,2]
ind	=where(angle_sign lt 0.,nind)
if (nind ge 1) then angle(ind)=!pi-angle(ind)
end
