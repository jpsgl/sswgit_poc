pro loop_model_fit,model,asep,xa,xb,coeff,xa_,xb_,deva,devb,iter,footpoints

common powell_par,model0,asep0,coeff0,xa0,xb0,xa_0,xb_0,dev_a0,dev_b0,foot0

if (n_params(0) le 10) then footpoints=[0.,0.,0.,0.]
foot0	=footpoints
model0	=model    ;1=circular, 2=circular/footpt, 3=helical
asep0	=asep
xa0	=xa
xb0	=xb
s1	=0.
s2	=0.
th	=coeff(5)	;starting values
h0	=coeff(3)
rhelix  =coeff(6)
nturn	=coeff(7)
ftol	=1.0e-6
if (model eq 1) then var=[th,h0]
if (model eq 2) then var=[th,h0,s1,s2]
if (model eq 3) then var=[th,h0,rhelix,nturn]
nvar	=n_elements(var)
vv	=fltarr(nvar,nvar)
for i=0,nvar-1 do vv(i,i)=1.
if (iter le 0) then dev=loop_powell(var)
stop
if (iter ge 1) then powell,var,vv,ftol,dev,'loop_powell',itmax=iter
coeff	=coeff0		;[l1,b1,base,h0,az,th,rhelix,nturn]
xa_	=xa_0
xb_	=xb_0
deva	=dev_a0
devb	=dev_b0

;z-coordinate reconstructed with curvature maximization method
xspline	=xa(*,0)
yspline	=xa(*,1)
xcirc	=xa_(*,0)
ycirc	=xa_(*,1)
zcirc	=xa_(*,2)
ns      =n_elements(xspline)
nc      =n_elements(xcirc)
s_c     =fltarr(nc)
for i=1,nc-1 do begin
 ds     =sqrt((xcirc(i)-xcirc(i-1))^2+(ycirc(i)-ycirc(i-1))^2)
 s_c(i) =s_c(i-1)+ds
endfor
len_c  =s_c(nc-1)
ns     =n_elements(xspline)
d1     =sqrt((xcirc(0)-xspline(0))^2+(ycirc(0)-yspline(0))^2)
d2     =sqrt((xcirc(0)-xspline(ns-1))^2+(ycirc(0)-yspline(ns-1))^2)
if (d2 le d1) then s_c=len_c-s_c
s_loop =fltarr(ns)
for i=1,ns-1 do begin
 ds    =sqrt((xspline(i)-xspline(i-1))^2+(yspline(i)-yspline(i-1))^2)
 s_loop(i)=s_loop(i-1)+ds
endfor
len_loop=s_loop(ns-1)
for i=0,ns-1 do begin
 dist_c=s_loop(i)*(len_c/len_loop)
 dmin  =min(abs(s_c-dist_c),j)
 xa(i,2)=zcirc(j)
endfor

;spacecraft B
xspline	=xb(*,0)
yspline	=xb(*,1)
xcirc	=xb_(*,0)
ycirc	=xb_(*,1)
zcirc	=xb_(*,2)
ns      =n_elements(xspline)
nc      =n_elements(xcirc)
s_c     =fltarr(nc)
for i=1,nc-1 do begin
 ds     =sqrt((xcirc(i)-xcirc(i-1))^2+(ycirc(i)-ycirc(i-1))^2)
 s_c(i) =s_c(i-1)+ds
endfor
len_c  =s_c(nc-1)
ns     =n_elements(xspline)
d1     =sqrt((xcirc(0)-xspline(0))^2+(ycirc(0)-yspline(0))^2)
d2     =sqrt((xcirc(0)-xspline(ns-1))^2+(ycirc(0)-yspline(ns-1))^2)
if (d2 le d1) then s_c=len_c-s_c
s_loop =fltarr(ns)
for i=1,ns-1 do begin
 ds    =sqrt((xspline(i)-xspline(i-1))^2+(yspline(i)-yspline(i-1))^2)
 s_loop(i)=s_loop(i-1)+ds
endfor
len_loop=s_loop(ns-1)
for i=0,ns-1 do begin
 dist_c=s_loop(i)*(len_c/len_loop)
 dmin  =min(abs(s_c-dist_c),j)
 xb(i,2)=zcirc(j)
endfor
end

