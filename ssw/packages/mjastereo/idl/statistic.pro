pro statistic,x,xav,sigma,median,sig1,sig2
;average, variance, standard deviation (sigma), median, 
;and range [sig1,sig2] that includes central 67% of distribution 

N	=N_ELEMENTS(X)
XMIN	=MIN(X)
XMAX	=MAX(X)
IF (N EQ 0) THEN BEGIN
 XAV	=0.
 SIGMA	=0.
 MEDIAN	=0.
 SIG1	=0.
 SIG2	=0.
ENDIF
IF (N EQ 1) THEN BEGIN
 XAV	=X(0)
 SIGMA	=0.
 MEDIAN	=X(0)
 SIG1	=X(0)
 SIG2	=x(0)
ENDIF
IF (N GE 2) THEN BEGIN
 XAV	=TOTAL(X)/FLOAT(N)
 VARIANCE=TOTAL((X-XAV)^2)/FLOAT(N-1)
 SIGMA	=SQRT(VARIANCE)
 ISORT	=SORT(X)
 XSORT	=X[ISORT]
 N2	=N/2
 IF (2*N2 NE N) THEN median= XSORT(N2)			;odd number
 IF (2*N2 EQ N) THEN median=(XSORT(N2-1)+XSORT(N2))/2.	;even number
 isig1	=long(n*0.17)
 isig2	=long(n*0.83)
 sig1	=xsort(isig1)
 sig2	=xsort(isig2)
ENDIF
IF (N EQ 2) THEN SIGMA=abs(X(1)-X(0))/2.

if (N_PARAMS(0) EQ 1) THEN BEGIN
lines	=strarr(6)
lines(0)='Number of elements       ='+string(N)
lines(1)='Minimum, maximum of X    ='+string(XMIN)+string(XMAX)
lines(2)='Mean, average            ='+string(XAV)
lines(3)='Sigma                    ='+string(SIGMA)
lines(4)='Median                   ='+string(median)
lines(5)='Median (67%) range       ='+string(sig1)+string(sig2)
for i=0,5 do print,lines(i)
endif 
end 
