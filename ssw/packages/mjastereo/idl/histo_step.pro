pro histo_step,x,y,xh,yh,ymin
;converts function [X,Y] in histogram step function [XH,YH]

if (n_params(1) le 4) then ymin=0.
DIM	=SIZE(X)
N	=DIM(1)
NN	=N*2+2
XH	=FLTARR(NN)
YH	=FLTARR(NN)
XH(0)	=X[0]-(X[1]-X[0])/2.
YH(0)	=ymin
XH(1)	=X[0]-(X[1]-X[0])/2.
YH(1)	=Y[0]
FOR I=1,N-1 DO BEGIN
 XH(I*2)    =(X[I]+X[I-1])/2.
 XH(I*2+1)  =XH[I*2]
 YH(I*2)    =Y[I-1]
 YH(I*2+1)  =Y[I]
ENDFOR
XH(NN-2)  =X[N-1]+(X[N-1]-X[N-2])/2.
YH(NN-2)  =Y[N-1]
XH(NN-1)  =XH[NN-2]
YH(NN-1)  =ymin
END
