pro nlfff_disp_comb,dir,savefile,test,io,iev,elist,run
;+
; Project     : SOHO/MDI, SDO/HMI, STEREO
;
; Name        : NLFFF_DISP1
;
; Category    : display of magnetic data modeling
;
; Explanation : displays magnetogram and observed and fitted magnetic
;		field lines in top-view and side-view projection.
;
; Syntax      : IDL>nlfff_disp,imagefile,vers,para,io
;
; Inputs      : imagefile = input filename *_loops.sav
;               para      = input parameters
;		io	  = ;0=X-window, 1=ps, 2=eps, 3=color ps
;
; Outputs     ; postscript file <imagefile>.ps
;
; History     : 16-May-2011, Version 1 written by Markus J. Aschwanden
;               30-Jan-2012, Version 2 released to SSW
;
; Contact     : aschwanden@lmsal.com
;-

print,'===================NLFFF_DISP_COMB====================='
restore,dir+savefile    ;--> PARA
event   =para.event
dateobs =para.dateobs
fov0    =para.fov0      ;field-of-view [solar radii]
nsm1    =para.nsm1      ;lowpass filter
wave_   =para.wave_
instr   =para.instr
filename=para.filename
x1_sun  =para.x1_sun
x2_sun  =para.x2_sun
y1_sun  =para.y1_sun
y2_sun  =para.y2_sun
blue	=50
red	=110
yellow	=200
ct	=3
nsm2	=nsm1+2
dlat    =5
set_plot,'X'
loadct,ct
nn	=400
window,0,xsize=nn*2,ysize=nn*3

;_______________________PLOT IMAGE_______________________________
nw	=n_elements(wave_)
for iw=0,nw-1 do begin
 imagefile=dir+filename+'_'+instr+'_'+wave_[iw]+'_fov.fits'
 print,'reading FITS file = ',imagefile
 image1 =readfits(imagefile,h)
 dateobs=sxpar(h,'DATE_OBS')    &print,'AIA time = ',dateobs
 image2 =smooth(image1,nsm1)-smooth(image1,nsm2)

 icol	=iw mod 2
 irow	=iw/2
 x1_    =0.0+0.5*icol
 x2_    =x1_+0.49
 y2_	=1.0-0.33*irow
 y1_	=y2_-0.32
 !p.position=[x1_,y1_,x2_,y2_]
 !x.range=[x1_sun,x2_sun]
 !y.range=[y1_sun,y2_sun]
 !x.style=5
 !y.style=5
 !x.title=''
 !y.title=''
 nxw    =long(!d.x_vsize*(x2_-x1_)+0.5)
 nyw   	=long(!d.y_vsize*(y2_-y1_)+0.5)
 im     =congrid(image1,nxw,nyw)
 statistic,im,c0,csig
 c1      =c0-2*csig
 c2      =c0+2*csig
 plot,[0,0],[0,0]
 tv,bytscl(im,min=c1,max=c2),x1_,y1_,xsize=(x2_-x1_),ysize=(y2_-y1_),/normal
 xyouts_norm,0.01,0.92,string(wave_[iw],'(I4)')+' A',3,0,255
 !noeras=1

;____________________PLOT FIELD LINES_______________________________
 nf      =n_elements(ns_loop)
 loadct,5
 qloop	=0.5
 for k=0,nf-1 do begin
  ns	=ns_loop[k]
  xl	=field_loops(0:ns-1,0,k)
  yl	=field_loops(0:ns-1,1,k)
  zl	=field_loops(0:ns-1,2,k)
  is	=long(qloop*(ns-1))
  oplot,xl,yl,color=blue,thick=2
 endfor
endfor

;____________________MAGNETOGRAM____________________________________
loadct,0
x1_     =0.51
x2_     =1.00
y2_	=1.0-0.33
y1_	=y2_-0.32
!p.position=[x1_,y1_,x2_,y2_]
!p.title=' '
plot,[0,0],[0,0]
nxw   =long(!d.x_vsize*(x2_-x1_)+0.5)
nyw   =long(!d.y_vsize*(y2_-y1_)+0.5)
image3=congrid(bzmap,nxw,nyw)                  ;magnetogram
plot,[0,0],[0,0],charsize=0.8
statistic,image3,c0,csig
c1      =c0-4*csig
c2      =c0+2*csig
tv,bytscl(image3,min=c1,max=c2),x1_,y1_,xsize=(x2_-x1_),ysize=(y2_-y1_),/normal
coordsphere,1,0,0,0,0,dlat
xyouts_norm,0.82,0.92,'HMI',3,0,255

;_______________________DISPLAY FITTED LINES____________________________
loadct,5
for k=0,nf-1 do begin
 np	=ns_field[k]
 xl	=field_lines(0:np-1,0,k)
 yl	=field_lines(0:np-1,1,k)
 zl	=field_lines(0:np-1,2,k)
 rl	=sqrt(xl^2+yl^2+zl^2)
 col	=yellow
 th	=1
 if (rl(0) le 1.01) and (rl(np-1) le 1.01) then begin &col=red &th=2 &endif
 oplot,xl,yl,thick=th,color=col
endfor

;____________________FREE ENERGY___________________________________
loadct,5
x1_     =0.5
x2_     =1.0
y1_	=0.00
y2_	=0.32
!p.position=[x1_,y1_,x2_,y2_]
!p.title=' '
!x.range=[x1_sun,x2_sun]
!y.range=[y1_sun,y2_sun]
plot,[0,0],[0,0]
nxw   =long(!d.x_vsize*(x2_-x1_)+0.5)
nyw   =long(!d.y_vsize*(y2_-y1_)+0.5)
image3=congrid(b2_free,nxw,nyw)                  ;free energy map 
plot,[0,0],[0,0],charsize=0.8
cc1      =0
cc2	=0.	;reset color level
if (cc2 eq 0) then cc2=max(b2_free)*2.
tv,bytscl(image3,min=cc1,max=cc2),x1_,y1_,xsize=(x2_-x1_),ysize=(y2_-y1_),/normal
coordsphere,1,0,0,0,0,dlat
xyouts_norm,0.02,0.02,filename,3,0,255
xyouts_norm,0.52,0.92,'Free energy',3,0,255

;__________________GOES LIGHT CURVE AND FREE ENERGY_______________
restore,'goes_prof.sav'  ;-->event_,time_goes_,flux_goes_,deriv_goes_
ind     =where(event_ eq iev)
iev_goes=ind(0)

readcol,efile,event_,tstart_,tpeak_,tend_,goes_,noaa_,helpos_,$
       format='(I,A,A,A,A,A,A)',/silent
ind     =where(event_ eq iev,nind)
if (nind le 0) then stop,'NLFFF_EVENT : NO MATCHING EVENT FOUND'
tstart  =tstart_(ind(0))
tpeak   =tpeak_(ind(0))
tend    =tend_(ind(0))
t0_hr   =(anytim(tpeak ,/sec) mod 86400)/3600.
t1_hr   =(anytim(tstart,/sec) mod 86400)/3600.
t2_hr   =(anytim(tend  ,/sec) mod 86400)/3600.
t3_hr   =t1_hr-tmargin-dt/2.
t4_hr   =t2_hr+tmargin+dt/2.
time_str=strmid(tstart,0,10)+' '+strmid(tstart,11,5)+' UT'
ptitle=instr+' #'+string(iev,'(I4)')+': '+time_str

;______________________EVOL DATA__________________________
restore,instr+'_evol.sav'
ind      =where(iev_ eq iev,nind)
iev0     =ind(0)
nt       =nt_(iev0)
ind      =where(hours_(0:nt-1,iev0) ne 0,nind)
hours    =hours_(ind,iev0)
efree    =efree_avg(0:nt-1,iev0,0)
sfree    =efree_avg(0:nt-1,iev0,1)
ediss    =ediss_avg[0:nt-1,iev0,0]
sdiss   =ediss_avg[0:nt-1,iev0,1]
erate    =fdiss_avg[0:nt-1,iev0,0]
srate    =fdiss_avg[0:nt-1,iev0,1]
len      =len_avg(0:nt-1,iev0,0)
slen     =len_avg(0:nt-1,iev0,1)
eneg     =max(ediss)-ediss
sneg     =srate

x1_	=0.05
x2_	=0.45
y1_	=0.28*2./3.
y2_	=0.48*2./3.
 !p.position=[x1_,y1_,x2_,y2_]
 !p.title=ptitle
 !y.title='E!Dfree!N [10!U30!N erg]'
 !x.title=''
 !x.range=[t3_hr,t4_hr]
 !y.range=[0.,max(efree)*1.2]
 !x.style=1
 !y.style=1
 ind    =where((hours ne 0) and (efree ne 0))
 plot, t1_hr*[1,1],!y.crange,linestyle=2
 oplot,t0_hr*[1,1],!y.crange,linestyle=0,thick=2
 oplot,t1_hr*[1,1],!y.crange,linestyle=2
 oplot,t2_hr*[1,1],!y.crange,linestyle=2
 oplot,!x.range,!y.crange(1)*[1,1]*0.99
 oplot,hours(ind),efree(ind)
 errplot,hours(ind),efree(ind)-sfree(ind),efree(ind)+sfree(ind)
 oplot,hours(it)*[1,1],!y.crange,thick=5,color=red
 !noeras=1

;______________________________PLOT GOES FLUX_________________________'
 loadct,5
 y1_	=0.04*2./3.
 y2_	=0.24*2./3.
 !p.position=[x1_,y1_,x2_,y2_]
 !p.title=''
 !y.title='GOES flux [10!U30!N erg]' 
 !y.range=max(flux_goes_(*,iev_goes))*[1.e-4,1.]
 !x.title='Time [hrs]'
 time_goes=time_goes_(*,iev_goes)
 flux_goes=flux_goes_(*,iev_goes)
 deriv_goes=deriv_goes_(*,iev_goes)
 ind	=where(time_goes lt time_goes(0),n0)
 if (n0 gt 0) then time_goes(ind)=time_goes(ind)+24.
 plot_io,time_goes,flux_goes,thick=3
 df_norm=(deriv_goes/max(deriv_goes))
 ymin   =10.^!y.crange(0)
 df_dt  =ymin+(max(flux_goes)-ymin)*df_norm
 oplot,time_goes,df_dt,color=red,thick=3
 polyfill,[t1_hr,time_goes,t2_hr],[ymin,df_dt>ymin,ymin],spac=0.1,orient=45,color=red
 oplot,[t1_hr,t1_hr],10^!y.crange,linestyle=2
 oplot,[t0_hr,t0_hr],10^!y.crange,linestyle=0
 oplot,[t2_hr,t2_hr],10^!y.crange,linestyle=2

;_________________________MAKE GIF_FILE______________________________________
 image_tv=tvrd(true=3)
 string0,3,it,it_str
 moviefile=dir+'frame_'+it_str
;write_gif,moviefile+'.gif',image_tv
 write_jpeg,moviefile+'.jpeg',image_tv,true=3
;print,'Movie file written : '+moviefile+'.gif'
 print,'Movie file written : '+moviefile+'.jpeg'
SKIP:
end

