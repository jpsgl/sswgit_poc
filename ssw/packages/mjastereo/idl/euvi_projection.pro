pro euvi_projection,loopfile,para,crota2,blong,blat,dlat,ct,h_error,hmax,window,plotname,nops,loopcolor,title
;+
; Project     : STEREO
;
; Name        : EUVI_PROJECTION 
;
; Category    : Graphics 
;
; Explanation : This routine produces a plot with the 3D coordinates of
;		a loop projected in any arbitrary direction.
;
; Syntax      : IDL> euvi_projection,loopfile,para,crota2,blong,blat,dlat,ct,h_error,hmax,plotname,nops
;
; Inputs      : loopfile = datafile containing (x,y,z)-coordinates of loops (e.g.,'loop_A.dat')
;		para	= structure containing image parameters
;		crota2  =0.  ;rotation angle (deg) relative to STEREO-A image
;		blong   =0.  ;longitude diff (deg) relative to STEREO-A image
;		blat    =90. ;latitude diff (deg) relative to STEREO-A image
;	 	dlat    =1.  ;spacing (deg) of coordinate grid
;		ct	=5   ;IDL colortable
;		h_error =1   ;0=default, 1=with error bars of altitude
;		hmax	=0.1 ;height limit of stereoscopic correlation
;		window  =0   ;IDL window nr.
;		plotname ='euvi_projection'   ;output filename
;		nops	=0   ;0=creates ps-file, -1=no ps-file (movie mode)
;
; Outputs     : postscript file of plot *.ps
;
; History     : Oct/Nov 2007, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

if (n_params(1) le 9)  then window=0
if (n_params(1) le 10) then plotname='euvi_projection'
if (n_params(1) le 11) then nops  =0
if (n_params(1) le 12) then loopcolor=lonarr(1000,2) 
if (n_params(1) le 13) then title=loopfile

;________________________________________________________________
;			INPUT PARAMETERS			|
;________________________________________________________________
cdelt1	=para.cdelt1
crpix1	=para.crpix1
crpix2	=para.crpix2
rad   	=para.rsun
rad_pix	=rad/cdelt1
dh_max	=0.05*696.	;Mm

print,'__________________________________________________________________'
print,'|		PLOT PREVIOUS LOOPS 				|'
print,'__________________________________________________________________'
file_exist=findfile(loopfile,count=nfiles)
if (nfiles eq 0) then print,'No previous loopfile found';
print,'Datafile with loop coordinates found  LOOPFILE=',loopfile 

idat	=strpos(loopfile,'.dat')
isav	=strpos(loopfile,'.sav')
if (idat gt 0) then readcol,loopfile,iloop_,ix_,iy_,iz_,ih_,iz_raw,dh_,nsm_,wave_,chi2_,q1_,q2_
if (isav gt 0) then restore,loopfile ;-->iloop_,ix_,iy_,iz_
nloop  =max(iloop_)+1 
print,'Number of previously traced loops    NLOOP   =',nloop
xx	=(ix_-crpix1)/rad_pix	;xx normalized to solar radius
yy	=(iy_-crpix2)/rad_pix	;yy normalized to solar radius
zz	=(iz_)/rad_pix		;zz normalized to solar radius
rotate1,xx,zz,xrot,zrot,0.,0.,blong 
rotate1,zrot,yy,zrot2,yrot,0.,0.,blat 
rotate1,xrot,yrot,xrot2,yrot2,0.,0.,crota2 
xrange=max(xrot2)-min(xrot2)
yrange=max(yrot2)-min(yrot2)
range =(xrange>yrange)
xmid  =(max(xrot2)+min(xrot2))/2.
ymid  =(max(yrot2)+min(yrot2))/2.

if (h_error eq 1) then begin
 xxa	=xx		
 yya	=yy		
 zza	=zz-dh_/rad_pix	
 rotate1,xxa,zza,xrota,zrota,0.,0.,blong 
 rotate1,zrota,yya,zrot2a,yrota,0.,0.,blat 
 rotate1,xrota,yrota,xrot2a,yrot2a,0.,0.,crota2 
 xxb	=xx		
 yyb	=yy		
 zzb	=zz+dh_/rad_pix		
 rotate1,xxb,zzb,xrotb,zrotb,0.,0.,blong 
 rotate1,zrotb,yyb,zrot2b,yrotb,0.,0.,blat 
 rotate1,xrotb,yrotb,xrot2b,yrot2b,0.,0.,crota2 
endif

if (nops eq  0) then begin &io2=3 &iostep=3 &endif
if (nops eq -1) then begin &io2=0 &iostep=1 &endif
for io=0,io2,iostep do begin
if (io eq 0) then begin
 clearplot
 erase
 set_plot,'X' 
 window,window,xsize=1024,ysize=1024
 !p.font=-1
 char   =1.2  
 x1_    =0.05
 y1_    =0.05 
 x2_    =0.95
 y2_    =0.95    
endif           
fignr	  =''
if (io ge 1) then begin
 form    =1     ;landscape format
 char    =1.2   ;smaller character size
 unit    =0     
 ref     =''     
 fig_open,io,form,char,fignr,plotname,unit
 x1_    =0.05
 y1_    =0.15 
 x2_    =0.95
 y2_    =0.85 
endif

!p.position=[x1_,y1_,x2_,y2_]
!p.charsize=1.2
!p.title=title
!x.range=xmid+[-0.52,0.52]*range
!y.range=ymid+[-0.52,0.52]*range
!x.title='EW direction (solar radii)'
!y.title='NS direction (solar radii)'
!x.style=1
!y.style=1
if (ct ge 0) then loadct,ct
plot,[0,0],[0,0]
rad	=1.0
pos	=0.
x0	=0.
y0	=0.
dlat0	=dlat(0)
coordsphere2,rad,pos,crota2,-blat,blong,dlat0,x0,y0
;coordsphere,rad,pos,crota2,-blat,blong,dlat0
phi	=2.*!pi*findgen(1001)/1000.
oplot,(1.+hmax)*cos(phi),(1.+hmax)*sin(phi),linestyle=2

len	=fltarr(nloop)
for iloop=0,nloop-1 do begin 
 ind	=where(iloop_ eq iloop,n) 
 if (n ge 1) then begin 
  xs	=ix_(ind)
  ys	=iy_(ind)
  zs	=iz_(ind)
  rs	=sqrt((xs-crpix1)^2+(ys-crpix2)^2+zs^2) 
  ns	=n_elements(xs)
  len(iloop)=total(sqrt((xs(1:ns-1)-xs(0:ns-2))^2+(ys(1:ns-1)-ys(0:ns-2))^2))
  rtop	=max(rs,itop)
  if (itop eq 0) or (itop eq ns-1) then len(iloop)=-1.
 endif
endfor

xmin	=min(ix_)
xmax	=max(ix_)
ymin	=min(iy_)
ymax	=max(iy_)
y1	=ymin-0.1*(ymax-ymin)
y2	=ymax+0.1*(ymax-ymin)
for iloop=0,nloop-1 do begin 
 ind	=where(iloop_ eq iloop,n) 
 if (n ge 3) then begin
  yloop	=(iy_(ind(0))-y1)/(y2-y1) 
  loopcol=loopcolor(iloop,0)
  loopthick=loopcolor(iloop,1)
  xp_rot=xrot2(ind)
  yp_rot=yrot2(ind)
  spline_p,xp_rot,yp_rot,x,y
  if (ct ge 0) then begin
   if (min(dh_(ind)) gt 0) then begin
    oplot,x,y,color=loopcol,thick=loopthick
    y_max	=max(y,im)
;   xyouts,x(im),y(im),string(iloop+1,'(I3)'),color=loopcol,size=char-0.1*io
    xyouts,x(im),y(im),string(iloop+1,'(I3)'),color=loopcol,size=char
   endif
   if (min(dh_(ind)) eq 0) then oplot,x,y,color=loopcol
  endif
  if (ct lt 0) then begin
   if (min(dh_(ind)) gt 0) then oplot,x,y,thick=5
   if (min(dh_(ind)) eq 0) then oplot,x,y
  endif 

  if (h_error eq 1) and (min(dh_(ind)) gt 0.) then begin
   xp_rota=xrot2a(ind)
   yp_rota=yrot2a(ind)
   xp_rotb=xrot2b(ind)
   yp_rotb=yrot2b(ind)
   oplot,xp_rot,yp_rot,psym=1
   for i=0,n-1 do begin
    line=0
    if (dh_(ind(i)) ge dh_max) then line=2
    if (ct ge 0) then oplot,[xp_rota(i),xp_rotb(i)],[yp_rota(i),yp_rotb(i)],color=loopcol,line=line
    if (ct lt 0) then oplot,[xp_rota(i),xp_rotb(i)],[yp_rota(i),yp_rotb(i)]
   endfor
  endif
 endif
endfor
if (io ge 1) then fig_close,io,fignr,ref
endfor
end
