function fit_dif_powell,coeff

common powerlaw_var,xh_,yh_,dh_,ind_fit_,yfit_,x0_,noise_,dev_

n0	=10.^coeff(0)
a	=    coeff(1) 
ncoeff	=n_elements(coeff)
nh	=n_elements(xh_)
yfit_ 	=n0*(xh_+dh_/2+x0_)^(-a)
noise_	=sqrt(yh_*dh_)/dh_
num	=yh_*dh_
ind_fit_=where((xh_ ge x0_) and (num ge 1.),nf)
obs	=yh_(ind_fit_)
fit	=yfit_(ind_fit_)
err	=noise_(ind_fit_)
dev_	=fltarr(nh)
dev_(ind_fit_)=(fit-obs)/err
w	=fltarr(nf)+1.		;weights
chi2    =total((w*(fit-obs)/err)^2)/total(w)
chi	=sqrt(chi2*float(nf)/float(nf-ncoeff))
return,chi 
end
