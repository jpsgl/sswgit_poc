pro stereo_loop_trace,index1,data1,index2,data2,savefile,nsm 
;+
; Project     : SECCHI/STEREO
;
; Name        : STEREO_LOOP_TRACE()
;
; Category    : 3D Reconstruction, Stereoscopy, Data Analsis
;
; Explanation : The user traces spline points along aloop.
;               The loop spline coordinates XP,YP are stored in savefile
;
; Syntax      : IDL> stereo_loop_trace,index1,data1,index2,data2,savefile
;
; Inputs      : index1  - structure containing descriptors of data1
;               data1   - image at time t1 (reference time)
;               index2  - structure containing descriptors of data2
;               data2   - image at time t2 (e.g.from following or previous day)
;               savefile- string filename (e.g. 'test_001.sav')
;		nsm     - number of pixels for smoothing/highpass-filtering
;
; Outputs     : output parameters are written in savefile
;
; History     : 16-Aug-2006, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com

;---------------------------------------------------------------;
;		CHECK INPUT 					;
;---------------------------------------------------------------;
print,'INPUT : index1,data1,index2,data2'
help,index1,data1,index2,data2
dim1	=size(data1)
dim2	=size(data2)
if (dim1(0) eq 0) or (dim2(0) eq 0) then stop,'No image input!'
restore,savefile	;-->x_range,y_range
fov_x	=(x_range(1,0)-x_range(0,0))
fov_y	=(y_range(1,0)-y_range(0,0))
zmax    =max(data1)
zmin    =min(data1)
dateobs_=strarr(3)  &dateobs_(1)=index1.date_obs &dateobs_(2)=index2.date_obs

;---------------------------------------------------------------;
;		IMAGE EROSION 		    			;
;---------------------------------------------------------------;
smooth_flatedge,float(data1),lowpass1,nsm
smooth_flatedge,float(data2),lowpass2,nsm
filter1=float(data1)-lowpass1
filter2=float(data2)-lowpass2

;---------------------------------------------------------------;
;		DISPLAY STEREO IMAGES				;
;---------------------------------------------------------------;
ysize	=800
xsize	=(ysize*(fov_x/fov_y))
window,0,xsize=xsize,ysize=ysize,retain=2
io	=0
ncol	=2
nrow	=2
nimages	=ncol*nrow
qgap	=0.1
grid	=5.
index_ 	=concat_struct(concat_struct(concat_struct(index1,index2),index1),index2)
data_	=[[[data1]],[[data2]],[[filter1]],[[filter2]]]
x_label ='EW [arcseconds]'
y_label ='NS [arcseconds]'
z_range =fltarr(2,nimages)
for i=0,1 do z_range(*,i)=[zmin,zmax]
for i=2,3 do z_range(*,i)=[-2,+4]*nsm
fig_multi_pos,x_range,y_range,ncol,nrow,qgap,p_position
fig_multi_tv,index_,data_,x_range,y_range,z_range,p_position,ncol,nrow,io,$
        x_label,y_label,grid
xyouts,0.1,0.97,dateobs_(1),/normal,size=1.2 
xyouts,0.6,0.97,dateobs_(2),/normal,size=1.2 

;---------------------------------------------------------------;
;                trace new loop 					;
;---------------------------------------------------------------;
!p.position=p_position(*,2)
!x.range=x_range(*,2)
!y.range=y_range(*,2)
indx=[0,1,1,0,0]
indy=[0,0,1,1,0]
plot,x_range(indx,2),y_range(indy,2),thick=5
if (io eq 0) then begin
 print,'Click loop (>7) positions with cursor in following order:'
 print,'x(1)=footpoint 1, x(2),...,x(n-1)=detected loop segment, x(n)=footpoint 2'
 print,'[ending by clicking outside of FOV]'
 for ip=0,100 do begin 
  print,'x,y  i=',ip
  cursor,x,y 
  wait,0.5
  if (x lt !x.range(0)) or (x gt !x.range(1)) or $
     (y lt !y.range(0)) or (y gt !y.range(1)) then goto,endloop
  oplot,[x,x],[y,y],psym=1
  if (ip eq 0) then begin &xp=[x]    &yp=[y] &endif
  if (ip ge 1) then begin &xp=[xp,x] &yp=[yp,y] &endif
 endfor
 endloop:
endif
np	=n_elements(xp)
spline_p,xp(1:np-2),yp(1:np-2),xs,ys
oplot,xs,ys,thick=3
xs	=[xp(0),xs,xp(np-1)]
ys	=[yp(0),ys,yp(np-1)]
oplot,[xp(0),xp(np-1)],[yp(0),yp(np-1)],linestyle=2

;---------------------------------------------------------------;
;       save results                                            ;
;---------------------------------------------------------------;
save,filename=savefile,x_range,y_range,xs,ys 
print,'FOV parameters written in file = '+savefile
end
