pro magn_simul,modelfile,para,inr
;+
; Project     : SOHO/MDI, SDO/HMI 
;
; Name        : MAGN_SIMUL
;
; Category    : magnetic field moedling
;
; Explanation : simulates nonlinear force-free magnetic field models
;
; Syntax      : IDL> magn_lowlou,filename,para,inr
;
; Inputs      : filename = *._coeff.dat, *_cube.sav, *_field.sav,   
;		para     = input parameters
;			   [ds,ngrid,nmag,nsm,qloop,nseg,acc,amax,hmax,dpix,
;                           niter,qzone,eps,qv,thresh,q_mag,halt,iopt]
;
; Outputs     ; saves parameters in file *_coeff.dat
;		--> m1,x1,y1,z1,a1		
;		saves parameters in file *_cube.sav:
;               --> x,y,z,bx,by,bz,b,a
;               saves parameters in file *_field.sav:
;               --> bzmap,x,y,field_lines,alpha,q_scale,cpu,merit
;
; History     : 28-Nov-2011, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

print,'________________MAGN_SIMUL__________________'
ds      =para[0]
qloop	=para[4]
halt    =para[16]
dpix    =para[9]
thresh  =para[14]

mag     =1000.
scale	=0.1
xm      =0.25*scale	;source location offset from disk center	
xm      =0.5*scale	;source location offset from disk center	
x0	=0.00
x1	=0.00
y0      =0.00
y1      =0.5*xm
r0      =1.-xm/2.
r1      =1.-xm/2.
z0      =sqrt(r0^2-x0^2-y0^2)
z1      =sqrt(r1^2-x1^2-y1^2)
ntwist  =-0.5
ntwist2 =-1.0
l       =0.05*!pi
a0      =0.
a1      =2.*!pi*ntwist/l
a2      =2.*!pi*ntwist2/l
nx	=long(2*scale/dpix)
ny	=nx
nz	=3
x       =-scale+dpix*findgen(nx)
y       =-scale+dpix*findgen(ny)
z       =   1.0+dpix*findgen(nz)
if (inr eq 1) then coeff=[[1.],[x0],[y0],[z0],[a0]]
if (inr eq 7) then coeff=[[1.],[x0],[y0],[z0],[a1]]
if (inr eq 2) then coeff=[[1.,-1.],[xm,-xm],[y0,y0],[z0,z0],[a0,a0]]
if (inr eq 8) then coeff=[[1.,-1.],[xm,-xm],[y0,y0],[z0,z0],[a1,a1]]
if (inr eq 3) then coeff=[[1.,-1.,1,-1],[xm,xm/2.,-xm/2,-xm],$
        [y0,y1,y0,y1],[z0,z0,z1,z1],[a0,a0,a0,a0]]
if (inr eq 9) then coeff=[[1.,-1.,1,-1],[xm,xm/2.,-xm/2,-xm],$
        [y0,y1,y0,y1],[z0,z0,z1,z1],[a1,a1,a2,a2]]
if ((inr ge 4) and (inr le 6)) or ((inr ge 10) and (inr le 12)) then begin
 nm     =10
 if (inr eq 4) or (inr eq 10) then s=7
 if (inr eq 5) or (inr eq 11) then s=1
 if (inr eq 6) or (inr eq 12) then s=5
 seed1  =123*s
 seed2  =234*s
 seed3  =345*s
 seed4  =456*s
 seed5  =567*s
 r1     =randomu(seed1,nm)
 r2     =randomu(seed2,nm)
 r3     =randomu(seed3,nm)
 r4     =randomu(seed4,nm)
 r5     =randomu(seed5,nm)
 coeff  =fltarr(nm,5)
 coeff(*,0)=-1.0+2.0*r1
 coeff(*,1)=(-1.5+3.0*r2)*scale/2.
 coeff(*,2)=(-1.5+3.0*r3)*scale/2.
 coeff(*,3)=0.95+0.02*r4
 if (inr ge 10) then coeff(*,4)=0.
 if (inr ge 10) then begin
  coeff(*,4)=-4.*!pi*ntwist*3.*r5/l
  for i=0,nm/2-1 do begin 	;adjust conjugate dipoles to constant alphas
   ipos	=2*findgen(nm/2)
   ineg	=ipos+1
   x1	=coeff(ipos,1) 
   y1	=coeff(ipos,2) 
   z1	=coeff(ipos,3) 
   x2	=coeff(ineg,1) 
   y2	=coeff(ineg,2) 
   z2	=coeff(ineg,3) 
   dist  =sqrt((x1-x2(i))^2+(y1-y2(i))^2+(z1-z2(i))^2)
   dmin  =min(dist,imin)
   coeff(ineg(i),4)=coeff(ipos(imin),4)
   print,'COEFF neg#=',ineg(i),' pos#',ipos(imin),' dmin=',dmin
  endfor
 endif 
endif

;________________MIMNIC LOW+LOU MODEL_______________________
if (inr eq 13) then begin
 readcol,'low01Aa_coeff.dat',m1,x1,y1,z1,a1
 a0	=-25.
 r	=sqrt(x1^2+y1^2)
 w	=0.03
 a1	=a1*0.+a0*exp(-r^2/(2.*w^2))
 coeff	=[[m1/mag],[x1],[y1],[z1],[a1]]
endif
 
;________________SCALING OF MAGNETIC FIELD_________________
coeff(*,0)=coeff(*,0)*mag

;________________STORE SIMULATION PARAMETERS________________
coeff_file=modelfile+'_coeff.dat'
openw,2,coeff_file
dim	=size(coeff)
nmag	=dim[1]
for is=0,nmag-1 do begin
 m1     =coeff(is,0)
 x1     =coeff(is,1)
 y1     =coeff(is,2)
 z1     =coeff(is,3)
 a1     =coeff(is,4)
 printf,2,m1,x1,y1,z1,a1,format='(5f12.5)'
 print   ,m1,x1,y1,z1,a1,format='(5f12.5)'
endfor
close,2
print,'Coefficient file written : ',coeff_file

;________________SIMULATE 3D B_FIELD CUBE___________________
print,'Datacube dimensions = nx,ny,nz,',nx,ny,nz
bx      =fltarr(nx,ny,nz)
by      =fltarr(nx,ny,nz)
bz      =fltarr(nx,ny,nz)
b       =fltarr(nx,ny,nz)
polar   =1.
for iz=0,nz-1 do begin
 bmax   =0.
 for iy=0,ny-1 do begin
  for ix=0,nx-1 do begin
   x1   =x[ix]
   y1   =y[iy]
   z1   =z[iz]
   magn_vector,x1,y1,z1,ds,coeff,polar,x2,y2,z2,bx2,by2,bz2,b2,endv
   bx(ix,iy,iz)=bx2
   by(ix,iy,iz)=by2
   bz(ix,iy,iz)=bz2
   b(ix,iy,iz) =b2
   bmax=bmax > abs(b2)
  endfor
 endfor
 print,'Magnetic field 3D cube iz/nz=',iz,nz,' bmax=',bmax
endfor

;_____________________ALPHA-CUBE___________________________________
a       =fltarr(nx,ny,nz)
for iz=1,nz-2 do begin
 for iy=1,ny-2 do begin
  for ix=1,nx-2 do begin
   dbx_dy=(bx(ix,iy+1,iz)-bx(ix,iy-1,iz))/(2.*dpix)
   dbx_dz=(bx(ix,iy,iz+1)-bx(ix,iy,iz-1))/(2.*dpix)
   dby_dx=(by(ix+1,iy,iz)-by(ix-1,iy,iz))/(2.*dpix)
   dby_dz=(by(ix,iy,iz+1)-by(ix,iy,iz-1))/(2.*dpix)
   dbz_dx=(bz(ix+1,iy,iz)-bz(ix-1,iy,iz))/(2.*dpix)
   dbz_dy=(bz(ix,iy+1,iz)-bz(ix,iy-1,iz))/(2.*dpix)
   jx   =(dbz_dy-dby_dz)
   jy   =(dbx_dz-dbz_dx)
   jz   =(dby_dx-dbx_dy)
   bx_	=bx(ix,iy,iz)
   by_	=by(ix,iy,iz)
   bz_	=bz(ix,iy,iz)
   wx   =bx_^2                                  ;weighted by B^2
   wy   =by_^2
   wz   =bz_^2
   a(ix,iy,iz)=total(wx*(jx/bx_)+wy*(jy/by_)+wz*(jz/bz_))/total(wx+wy+wz)
  endfor
 endfor
endfor
print,'a_map=',minmax(a(*,*,1))

;________________SIMULATE MAGNETOGRAM_________________________
aia_mag_map,coeff,1.,x,y,bzmap

;________________CALCULATE FIELD LINES________________________
ind	=where(abs(bzmap) ge thresh,nf)
print,'Number of field lines = ',nf
j0	=long(ind/nx)
i0	=ind-j0*nx
nsmax	=1000
field_lines=fltarr(nsmax,5,nf)
alpha	=fltarr(nf,4)
for k=0,nf-1 do begin
 xm     =x[i0(k)]
 ym     =y[j0(k)]
 zm     =sqrt(1.-xm^2-ym^2)
 magn_fieldline,xm,ym,zm,ds,halt,coeff,field,ns_,nm
 im	=long(qloop*(ns_-1))
 xm	=field(im,0)
 ym	=field(im,1)
 zm     =field(im,2)
 magn_fieldline,xm,ym,zm,ds,halt,coeff,field,ns_,nm
 ns_    =ns_ < nsmax
 field_lines(0:ns_-1,0,k)=field(0:ns_-1,0)
 field_lines(0:ns_-1,1,k)=field(0:ns_-1,1)
 field_lines(0:ns_-1,2,k)=field(0:ns_-1,2)
 field_lines(0:ns_-1,3,k)=field(0:ns_-1,3)      ;b

 alpha_ =fltarr(ns_)
 alpha__=fltarr(ns_)
 for i=0,ns_-1 do begin
  x0    =field_lines(i,0,k)
  y0    =field_lines(i,1,k)
  z0    =field_lines(i,2,k)
  bb    =field_lines(i,3,k)
  al    =field_lines(i,4,k)     ;theoretical alpha
  x1    =x0-ds
  x2    =x0+ds
  y1    =y0-ds
  y2    =y0+ds
  z1    =z0-ds
  z2    =z0+ds
  magn_vector,x0,y0,z0,ds,coeff,polar,xv,yv,zv,bx_  ,by_  ,bz_  ,b_  ,endv
  magn_vector,x1,y0,z0,ds,coeff,polar,xv,yv,zv,bx_x1,by_x1,bz_x1,b_x1,endv
  magn_vector,x2,y0,z0,ds,coeff,polar,xv,yv,zv,bx_x2,by_x2,bz_x2,b_x2,endv
  magn_vector,x0,y1,z0,ds,coeff,polar,xv,yv,zv,bx_y1,by_y1,bz_y1,b_y1,endv
  magn_vector,x0,y2,z0,ds,coeff,polar,xv,yv,zv,bx_y2,by_y2,bz_y2,b_y2,endv
  magn_vector,x0,y0,z1,ds,coeff,polar,xv,yv,zv,bx_z1,by_z1,bz_z1,b_z1,endv
  magn_vector,x0,y0,z2,ds,coeff,polar,xv,yv,zv,bx_z2,by_z2,bz_z2,b_z2,endv
  dby_dx =(by_x2-by_x1)/(x2-x1)
  dbz_dx =(bz_x2-bz_x1)/(x2-x1)
  dbx_dy =(bx_y2-bx_y1)/(y2-y1)
  dbz_dy =(bz_y2-bz_y1)/(y2-y1)
  dbx_dz =(bx_z2-bx_z1)/(z2-z1)
  dby_dz =(by_z2-by_z1)/(z2-z1)
  jx    =(dbz_dy-dby_dz)
  jy    =(dbx_dz-dbz_dx)
  jz    =(dby_dx-dbx_dy)
  wx    =bx_^2                                  ;weighted by B^2
  wy    =by_^2
  wz    =bz_^2
  alpha_(i)=total(wx*(jx/bx_)+wy*(jy/by_)+wz*(jz/bz_))/total(wx+wy+wz)
  alpha__(i)=al                                 ;theoretical alpha
 endfor
 field_lines(0:ns_-1,4,k)=alpha_(0:ns_-1)       ;alpha
 statistic,alpha_,a_avg,a_sig                   ;cube-calculated alpha
 alpha(k,0)=a_avg
 alpha(k,1)=a_sig
endfor

;________________SAVE PARAMETERS______________________________
file1	=modelfile+'_cube.sav'
save,filename=file1,x,y,z,bx,by,bz,b,a
print,'parameters saved in file = ',file1

file2	=modelfile+'_field.sav'
angle	=0.
cpu	=0.
q_scale =1.
merit	=[0.,0.,0.]
save,filename=file2,bzmap,x,y,field_lines,alpha,q_scale,cpu,merit
print,'parameters saved in file = ',file2
end
