pro aia_loop_auto,fileset,wave_,fov,para,test,cutoff,vers
;+
; Project     : AIA/SDO
;
; Name        : AIA_LOOP_AUTO
;
; Category    : Data analysis   
;
; Explanation : Automated loop tracing
;
; Syntax      : IDL>aia_loop_auto,fileset,wave_,fov
;
; Inputs      : fileset	 = beginning of filename 
;		wave_[6] = wavelengths
;	        fov[4]   = [x1,y1,x2,y2] field-of-view in solar radii
;               vers	 = label in filename (e.g., vers='a'
;
; Outputs     : filterfile = highpass-filtered images
;		loopfile   = x,y coordinates of traced loops
;
; History     : 09-Mar-2011, Version 1 written by Markus J. Aschwanden
;               18-May-2011, change units of FOV from pixels to solar radii
;		11-Jun-2011, add VERS to specify filename
;
; Contact     : aschwanden@lmsal.com
;-

;____________________READ DATA_______________________________________
 if (max(abs(fov)) gt 1.3) then STOP,'Specify FOV in units of solar radii'
 nwave	=n_elements(wave_)
 nsig	=para[2]
 loopfiles=strarr(nwave)
 for iwave=0,nwave-1 do begin
  wave	   =wave_(iwave)
  loopfile  =fileset+vers+'_'+wave+'_auto.dat'
  loopfiles[iwave]=loopfile
  searchfile=fileset+'*'+wave_[iwave]+'.fits'
  datafiles=file_search(searchfile)
  datafile=datafiles[0]
  read_sdo,datafile,index0,data0
  aia_prep,index0,data0,index,data
  cdelt1  =index.cdelt1
  crpix1  =index.crpix1
  crpix2  =index.crpix2
  rsun    =index.rsun_obs
  rpix    =rsun/cdelt1
  x1	  =fov[0]
  x2	  =fov[2]
  y1	  =fov[1]
  y2	  =fov[3]
  i1      =long(x1*rpix+crpix1+0.5)
  i2      =long(x2*rpix+crpix1+0.5)
  j1      =long(y1*rpix+crpix2+0.5)
  j2      =long(y2*rpix+crpix2+0.5)
  print,'FOV in pixels [i1,j1,i2,j1]=',i1,j1,i2,j2
  image1   =data[i1:i2,j1:j2]
  nx	   =i2-i1
  ny	   =j2-j1

;_____________________LOOP TRACING___________________________________
  help,image1,fov,para,cutoff,image2,loopfile,test
  looptracing_auto,image1,fov,para,cutoff,image2,loopfile,test

;_____________________WRITE FILTERFILE_______________________________
; filterfile=strmid(datafile,0,ipos)+'F.fits'
; writefits,filterfile,image2

;_____________________DISPLAY_________________________________________
  readcol,loopfile,iloop,x,y,z,s
  n     =max(iloop)+1
  statistic,image2,zav,zdev
  z1    =zav-zdev
  z2    =zav+zdev

  clearplot
  window,0,xsize=nx,ysize=ny
  loadct,0
  !p.position=[0,0,1,1]  
  !x.range=[x1,x2]
  !y.range=[y1,y2]
  !p.title=' '
  !x.title=' '
  !y.title=' '
  !x.style=5
  !y.style=5
  plot,[0,0],[0,0]
  tv,bytscl(image2,min=z1,max=z2),0,0,xsize=1,ysize=1,/normal
  if (iwave eq 1) then image171=image2
  col    =40*(1+iwave)
  !noeras=1

;__________________DISPLAY TRACE LOOPSS______________________
  loadct,5
  xyouts_norm,0.02,0.9,wave_[iwave]+' A',2,0,col
  readcol,loopfile,iloop,x,y,z,s
  for i=0,n-1 do begin
   ind   =where(i eq iloop,np)
   spline_p,x[ind],y[ind],xx,yy
   oplot,xx,yy,thick=5,color=col
  endfor
 endfor

;_______________________SYNTHESIZED DISPLAY___________________________
loadct,0
statistic,image171,zav,zdev
z1    =zav-zdev
z2    =zav+zdev
tv,bytscl(image171,min=z1,max=z2),0,0,xsize=1,ysize=1,/normal
!noeras=1
loadct,5
for iwave=0,nwave-1 do begin
 readcol,loopfiles[iwave],iloop,x,y,z,s
 nloop   =max(iloop)+1
 print,wave_[iwave],nloop,' loop structures'
 for i=0,nloop-1 do begin
  ind   =where(i eq iloop,np)
  spline_p,x[ind],y[ind],xx,yy
  oplot,xx,yy,thick=5,color=40*(1+iwave)
 endfor
 xyouts_norm,0.05+0.1*iwave,0.95,wave_[iwave],2,0,40*(1+iwave)
endfor

end
