pro nlfff_dem2,dir_tmp,dir_dem,efile,iev,dateobs_,fov_,tmargin,dt,wave_,teem_table,vers,test1,nbin,em_thr
;+
; Project     : AIA/SDO
;
; Name        : NLFF_DEM
;
; Category    : Data analysis   
;
; Explanation : reads AIA images, measures flare area, DEM analysis
;		pixel-wise DEM modeling
;
; Syntax      : IDL>nlfff_dem,
;
; Inputs      : dir_tmp	 = AIA image data directory 
;               dir_dem	 = DEM data directory
;
; Outputs     : 
;
; History     : 11-Mar-2014, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

print,'__________________NLFFF DEM____________________________________
nwave   =n_elements(wave_)
nt	=n_elements(dateobs_)
it1	=long(tmargin/dt)		;time step at start, background subtr
nbin	=nbin > 1
first	=0
kb	=1.3807e-16 	;erg/K		;Boltzmann constant
qnoise  =0.1

time_t =strarr(nt,nwave)
texp_t =fltarr(nt,nwave)
area_t =fltarr(nt,nwave)
valid  =lonarr(nt,nwave)
for iw=0,nwave-1 do begin
 wave   =wave_[iw]
 for it=0,nt-1 do begin
  x1	=fov_[0,it]
  y1	=fov_[1,it]
  x2	=fov_[2,it]
  y2	=fov_[3,it]
  filename  =dateobs_[it]
  imagefile=dir_tmp+filename+'_'+wave_[iw]+'_euv.fits'
  file	 =findfile(imagefile,count=count)
  if (count eq 0) then nlfff_AIA,dir_tmp,filename,wave_[iw],test1
  file	 =findfile(imagefile,count=count)
  if (count eq 0) then goto,skip_read			;file does not exist
  print,it,iw,'   reading : ',imagefile
  valid(it,iw)=1
  data   =readfits(imagefile,h,/silent)
  dateobs=sxpar(h,'DATE_OBS')   &time_t[it,iw]=dateobs 
  texp	 =sxpar(h,'EXPTIME')	&texp_t[it,iw]=texp
  cdelt1 =sxpar(h,'CDELT1')
  crpix1 =sxpar(h,'CRPIX1')
  crpix2 =sxpar(h,'CRPIX2')
  eph    =get_sun(dateobs)
  rsun   =eph(1)                 	;rsun=index.rsun_obs in arcsec
  rpix   =rsun/cdelt1
  dpix   =1./rpix
  if (first eq 0) then begin				;fix first image size
   i1     =long(crpix1+x1/dpix+0.5)
   j1     =long(crpix2+y1/dpix+0.5)
   i2     =long(crpix1+x2/dpix+0.5)
   j2     =long(crpix2+y2/dpix+0.5)
   nx     =(i2-i1+1)
   ny     =(j2-j1+1)
   nxx	  =long(nx/nbin)				;rebinned size
   nyy	  =long(ny/nbin)
   nx0	  =nxx*nbin
   ny0	  =nyy*nbin
   flux_xyt=fltarr(nxx,nyy,nt,nwave)
   unce_xyt=fltarr(nxx,nyy,nt,nwave)
   back    =float(data(i1:i2,j1:j2))/texp		;DN/s
   first  =1						;first frame read 
  endif
  i2	  =i1+nx-1
  j2	  =j1+ny-1
  image   =float(data(i1:i2,j1:j2))/texp		;DN/s
  if (it le it1) then back = image < back
  if (it eq it1) then back_rebin=rebin(back(0:nx0-1,0:ny0-1),nxx,nyy)*nbin^2  	
  if (it ge it1) then begin
   image_subtr= (image - back) > 1.e-6			;DN/s
   flux_xyt(*,*,it,iw)=rebin(image_subtr(0:nx0-1,0:ny0-1),nxx,nyy)*nbin^2 ;rebinning
  endif
SKIP_READ:
 endfor ;fot it=0,nt-1 
 for i=0,nxx-1 do begin
  for j=0,nyy-1 do begin
   unce_xyt(i,j,*,iw)=qnoise*max(flux_xyt[i,j,*,iw])
  endfor
 endfor
endfor	;for iw=0,nwave-1

;________________DEM+TE ANALYSIS____________________________________
restore,teem_table      ;-->wave_,q94,area,resp_corr,telog,dte,tsig,flux,pix
nte      =n_elements(telog)
nsig     =n_elements(tsig)
ne_prof  =fltarr(nt)
te_prof  =fltarr(nt)
dte_prof =fltarr(nt)
em_prof  =fltarr(nt)
chi_prof =fltarr(nt)
eth_prof =fltarr(nt)
rad_cm   =fltarr(nt)
vol_cm   =fltarr(nt)
flux_prof=fltarr(nt,nwave)
unce_prof=fltarr(nt,nwave)
fit_prof =fltarr(nt,nwave)
flux_t	 =fltarr(nt,nwave)
unce_t	 =fltarr(nt,nwave)
te_map   =fltarr(nxx,nyy,nt)
em_map   =fltarr(nxx,nyy,nt)
sig_map  =fltarr(nxx,nyy,nt)
dem_t	 =fltarr(nte,nt)
nfree    =3

for it=it1,nt-1 do begin
 print,'time step = ',it,nt-1 
 for j=0,nyy-1 do begin
  for i=0,nxx-1 do begin
   flux_obs=reform(flux_xyt[i,j,it,*])                    ;flux [DN/s pix]
   unce_obs=reform(unce_xyt[i,j,it,*])                    ;flux [DN/s pix]
   indw   =where((flux_obs gt 0),nindw)
   if (nindw lt nwave) then goto,skip_time                ;skip this time frame
   flux_prof[it,*]=flux_prof[it,*]+flux_obs               ;total flux [DN/s]
   unce_prof[it,*]=unce_prof[it,*]+unce_obs        	  ;total flux [DN/s]
   for k=0,nte-1 do begin
    for l=0,nsig-1 do begin
     flux_dem1=reform(flux(k,l,*))			  ;model flux 6 wavelen
     em1=median(flux_obs/flux_dem1)			  ;median ratio		
     flux_dem=flux_dem1*em1
     chi =sqrt(total((flux_obs-flux_dem)^2/unce_obs^2)/(nwave-nfree))
     if (k eq 0) and (l eq 0) then chi_best=chi
     if (chi le chi_best) then begin
      chi_best   =chi
      em_best    =em1				  	  ;[cm-5 K-1]
      telog_best =telog(k)				  
      sig_best   =tsig(l)
     endif
    endfor
   endfor
   em_map(i,j,it)=em_best
   te_map(i,j,it)=10.^telog_best
   sig_map(i,j,it)=sig_best
   dem_t(*,it)=dem_t(*,it)+em_best*exp(-(telog-telog_best)^2/(2.*sig_best^2))
  endfor
 endfor

;____________________TOTAL EMISSION MEASURE_______________________________
 flux_dem    =fltarr(nwave)
 for iw=0,nwave-1 do flux_dem(iw)=total(dem_t(*,it)*resp_corr(*,iw)*dte)
 flux_obs    =reform(flux_prof[it,*])                      ;flux [DN/s macropix]
 qem	     =median(flux_obs/flux_dem)			   ;median ratio	
 flux_dem    =flux_dem*qem
 fit_prof[it,*]=flux_dem
 unce_obs    =reform(unce_prof[it,*])                      ;flux [DN/s marcopix]
 chi_prof(it)=sqrt(total((flux_obs-flux_dem)^2/unce_obs^2)/(nwave-nfree))
 dem_max     =max(dem_t(*,it),itemp)			   ;EM [cm-5 K-1 pix-1]
 te_prof(it) =10.^telog(itemp)				   ;T  [K]
 dte_prof(it)=(dte(itemp)/1.e6)/2.			   ;dT  [K]
 dem 	     =total(dem_t(*,it)*dte)			   ;EM [cm-5 pix-1]
 em_prof(it) =alog10(dem)+2.*alog10(pix)	   	   ;log EM [cm-3]
SKIP_TIME:
endfor	;for it=it1,nt-1 

;____________________FLARE GEOMETRY_________________________________
em_med	=fltarr(nxx,nyy,nt)
te	=10.^telog
for it=0,nt-1 do begin
 if (it eq 0) or   (it eq nt-1) then em_med[*,*,it]=em_map[*,*,it]
 if (it ne 0) and  (it ne nt-1) then begin
  for i=0,nxx-1 do begin
   for j=0,nyy-1 do begin
    em3	=em_map[i,j,it-1:it+1]
    indm=where(em3 ne 0,nmed)
    if (nmed ge 1) then em_med[i,j,it]=median(em3(indm))
   endfor
  endfor
 endif
 em_max      =max(em_med[*,*,it])
 em_min      =em_thr < (em_max*0.5) 			   ;used for RUN4
 ind_area    =where(em_med[*,*,it] ge em_min,nind_area)
 rad_pix     =sqrt(nind_area)*nbin			   ;area [pixels]
 rad_cm[it]  =rad_pix*pix					   ;cm
 vol_cm[it]  =rad_cm[it]^3.				   ;cm^3
 vol_log     =alog10(vol_cm(it))
 ne_prof(it) =10.^(0.5*(em_prof(it)-vol_log))
 eth_old     =3.*ne_prof(it)*kb*te_prof(it)*vol_cm(it)
 em_te       =total(sqrt((dem_t(*,it)/1.e30)*pix^2*te^2*dte))
 eth_prof(it)=3.*kb*sqrt(vol_cm(it)/1.e30)*em_te*1.e30
 print,'ETH = ',eth_old,eth_prof(it),eth_prof(it)/eth_old

 flux_tot=total(flux_prof[it,*])
 if ((nindw lt nwave) or (flux_tot le 0.)) and (it ge 1) then begin
  flux_prof[it,*]=flux_prof[it-1,*]
  fit_prof[it,*] =fit_prof[it-1,*]
  em_prof[it]    =em_prof[it-1]
  te_prof[it]    =te_prof[it-1]
  dte_prof[it]   =dte_prof[it-1]
  chi_prof[it]   =chi_prof[it-1]
  rad_cm[it]     =rad_cm[it-1]
  vol_cm[it]     =vol_cm[it-1]
  ne_prof[it]    =ne_prof[it-1]
  eth_prof[it]   =eth_prof[it-1]
 endif
endfor	;for it=it1,nt-1 

;________________SAVE PRIMARY PARAMETERS________________________
filename  =dateobs_[0]
savefile=dir_dem+filename+'_dem_'+vers+'.sav'
save,filename=savefile,dateobs_,wave_,nt,time_t,flux_t,texp_t,em_map,em_med,$
    te_map,sig_map,em_prof,telog,dem_t,te_prof,dte_prof,ne_prof,eth_prof,$
    chi_prof,rad_cm,vol_cm,flux_prof,fit_prof,pix,nbin,em_thr,em_min
print,'Parameters saved in file = ',savefile
end
