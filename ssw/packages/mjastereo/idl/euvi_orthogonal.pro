pro euvi_orthogonal,loopfile,loopnr,para
;+
; Project     : STEREO
;
; Name        : EUVI_ORTHOGONAL 
;
; Category    : Graphics 
;
; Explanation : This routine displays 3D loop coordinates in the loop plane
;		and in the other orthognal planes.
;
; Syntax      : IDL> euvi_orthogonal,loopfile,loopnr,para
;
; Inputs      : loopfile	= datafile with loop 3D coordinates
;	        loopnr	       = range of loop numbers, e.g., [1,7] for 1-7
;		para           = index structure with image parameters 
;
; Outputs     : postcript file named 'ortho.ps'
;
; History     : Oct/Nov 2007, Version 1 written by Markus J. Aschwanden
;		Dec 12, 2008, correct for negative yyy (David Kim, NRL)
;
; Contact     : aschwanden@lmsal.com
;-

;________________________________________________________________
;			SUBIMAGES  				|
;________________________________________________________________
form    =1     ;landscape format
char    =0.8   ;smaller character size
ipos     =strpos(loopfile,'trace')
plotname=strmid(loopfile,0,ipos)
fignr	 ='ortho'
unit    =0     
ref     =''     
rsun_m  =0.696*1.e9
cdelt1	=para.cdelt1
cdelt2	=para.cdelt2
crpix1	=para.crpix1
crpix2	=para.crpix2
da	=para.da/rsun_m
db	=para.db/rsun_m
rsun   	=para.rsun
a_sep	=para.sep_deg*(!pi/180.)
rad_pix	=rsun/cdelt1

file_exist=findfile(loopfile,count=nfiles)
if (nfiles eq 0) then stop,'No previous loopfile found';
readcol,loopfile,iloop_,ix_,iy_,iz_,ih_,ih_raw,dh_,nsm_,poly_,chi2_,q1_,q2_
if (n_elements(iloop_) lt 1) then stop,'loopfile empty: ',loopfile
nloop	=long(max(iloop_))+1
print,'Datafile with loop coordinates found  LOOPFILE=',loopfile 
print,'Number of loops found                NLOOP   ='+string(nloop,'(i3)')

for io=0,1 do begin
fig_open,io,form,char,fignr,plotname,unit
for iloopnr=loopnr(0),loopnr(1) do begin
iloop	=iloopnr-1
ind	=where(iloop_ eq iloop,np) 
euvi_spline,para,ix_(ind),iy_(ind),iz_(ind),x,y,z,d,r,r_n
n	=n_elements(x)
p	=0.
p0	=0.
l0	=0.
b0	=0.
x_n	=(x-crpix1)/rad_pix
y_n	=(y-crpix2)/rad_pix
z_n	=z/rad_pix

;midpoint baseline
x0	=(x_n(0)+x_n(n-1))/2.
y0	=(y_n(0)+y_n(n-1))/2.
z0	=(z_n(0)+z_n(n-1))/2.

;inclination angle
r_     =(r_n-1.)
hmax   =max(r_,im)                             ;hmax
xh     =x_n(im)
yh     =y_n(im)
zh     =z_n(im)
radius =sqrt((xh-x0)^2+(yh-y0)^2+(zh-z0)^2)    ;loop radius
theta  =(180./!pi)*acos(hmax/radius)           ;loop inclination

;first rotation
dy	=(y_n(0)-y0)
dx	=(x_n(0)-x0)
d	=sqrt(dx^2+dy^2)
cosa	=dx/d
sina	=dy/d
xx	= (x_n-x0)*cosa+(y_n-y0)*sina
yy	=-(x_n-x0)*sina+(y_n-y0)*cosa
zz	= (z_n-z0)
alpha	=(180./!pi)*acos(cosa)

;second rotation
dz	=zz(n-1)-zz(0)
dx	=xx(n-1)-xx(0)
d	=sqrt(dx^2+dz^2)
cosb	=dx/d
sinb	=dz/d
xxx	= xx*cosb+zz*sinb 
yyy	= yy
zzz	=-xx*sinb+zz*cosb
beta	=(180./!pi)*asin(sinb)

;third rotation
;ymax	=max(yyy,im)		;corrected Dec 12, 2008
ymax	=max(abs(yyy),im)	;corrected Dec 12, 2008 (for negative yyy)
if (im ne 0) then begin
 dy	=yyy(im)-yyy(0)
 dz	=zzz(im)-zzz(0)
 d	=sqrt(dy^2+dz^2)
 sinc	=dz/d
 cosc	=dy/d
 x4	=xxx
 y4	= yyy*cosc+zzz*sinc
 z4	=-yyy*sinc+zzz*cosc
 gamma	=(180./!pi)*asin(sinc)
endif
if (im eq 0) then begin          ;no rotation (precludes sinc=NAN, cosc=NAN) 
 x4	=xxx
 y4	=yyy
 z4	=zzz
 gamma	=0.
endif

;aspect ratios
d	=0.14	
dy	=0.11
dgap	=0.09
row	=0.90-iloopnr*dy*1.2
rad	=(abs(x4(n-1)-x4(0))/2.)
dd	=rad>max(y4)
q	=1.2
phi	=2.*!pi*findgen(101)/100.
xmin	=min(abs(x4),imin)
h0	=y4(imin)
half	=abs(x4(n-1)-x4(0))/2.
rad	=(half^2+h0^2)/(2.*h0)
hc	=h0-rad			;height or circle center

!p.position=[0,row,d,dy+row]+dgap
!p.title=strmid(loopfile,ipos-9,8)
!x.range=[-dd/2,dd/2]*q
!y.range=[ 0,dd]*q 	
!x.title=' '
if (iloopnr eq loopnr(1)) then !x.title='Loop perpendicular'
!y.title='Loop plane'
!x.style=0
!y.style=0
plot,z4,y4,thick=3,xticks=2
oplot,[0,0],!y.range,linestyle=1
xyouts_norm,0.1,0.9,'Loop #'+string(iloopnr,'(i2)'),char
!noeras=1

!p.title=' '
!p.position=[d+dgap,row,3*d+dgap,dy+row]+dgap
!x.range=[-dd,dd]*q
!y.range=[ 0,dd]*q	
if (iloopnr eq loopnr(1)) then !x.title='Loop baseline'
!y.title='Loop plane'
plot,x4,y4,thick=3
oplot,rad*cos(phi),rad*sin(phi)+(h0-rad),linestyle=2
oplot,[0,0],!y.range,linestyle=1
dev	=sqrt(x4^2+(y4-hc)^2)/rad
xyouts_norm,0.05,0.9,'max(R)='+string(max(dev),'(f4.2)'),char
xyouts_norm,0.05,0.8,'min(R)='+string(min(dev),'(f4.2)'),char
xyouts_norm,0.55,0.9,'r!Dcurv!N='+string(rad*696.,'(f5.1)')+' Mm',char
xyouts_norm,0.55,0.8,'h!Dcurv!N='+string(hc*696.,'(f5.1)')+' Mm',char

!p.position=[3*d+2*dgap,row,5*d+2*dgap,dy+row]+dgap
!x.range=[-dd,dd]*q
!y.range=[-dd/2,dd/2]*q
if (iloopnr eq loopnr(1)) then !x.title='Loop baseline'
!y.title='Loop perpendicular'
plot,x4,z4,thick=3
oplot,!x.range,[0,0],linestyle=1
oplot,[0,0],!y.range,linestyle=1
dev	=abs(z4/rad)
xyouts_norm,0.05,0.9,'Nonplanarity ='+string(max(dev),'(f4.2)'),char
xyouts_norm,0.05,0.8,'Inclination  ='+string(theta   ,'(f6.1)')+'!U0!N',char
endfor

if (io ge 1) then fig_close,io,fignr,ref
endfor
end
