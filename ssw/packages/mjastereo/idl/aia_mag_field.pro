pro aia_mag_field,fileset,fov,ds,hmin,hmax
;+
; Project     : AIA/SDO
;
; Name        : AIA_MAG_LOOP
;
; Category    : SDO/AIA data modeling 
;
; Explanation : Fitting of twisted field lines to observed loop projections
;
; Syntax      : IDL>aia_mag_loop,fileset,fov,ds,hmax,model,nseg,test
;
; Inputs      : fileset	 = filename 
;	        fov[4]   = [x1,y1,x2,y2] field-of-view (in solar radii)
;		ds	 = step size 
;		hmax	 = height range of magnetic field model
;		model	 : 'p'=potential, 'n'=nonpotential
;               nseg     = number of segments (misalignment angle)
;		test	 = display initial-guess values 
;
; Outputs:      *_field.sav = savefile containing fieldlines
;
; History     : 12-May-2011, Version 1 written by Markus J. Aschwanden
;                6-Sep-2011, modifications
;
; Contact     : aschwanden@lmsal.com
;-

print,'_____________READ MAGNETIC FIELD MODEL_________________________'
coeff_file=file_search(fileset+'_coeff.dat')
readcol,coeff_file(0),fmag,xmag,ymag,zmag 
coeff	=[[fmag],[xmag],[ymag],[zmag]]
ncomp	=n_elements(fmag)
print,'NUMBER OF MAGNETIC COMPONENTS = ',ncomp

print,'____________READ LOOP COORDINATES______________________________'
loopfile=fileset+'_loop.dat'
readcol,loopfile,iloop,x,y,z,s
nloop   =long(max(iloop)+1)
print,'NUMBER OF LOOPS            = ',nloop

print,'_____________LOOP FOOTPOINTS + MAGNETIC CHARGES________________'
hfoot	=2*ds
eps	=1.e-4
nfoot   =2
footp   =fltarr(nloop,nfoot,3)
for il=0,nloop-1 do begin
 ind   =where(il eq iloop,ns)
 xl     =x[ind]
 yl     =y[ind]
 zl     =z[ind]
 rl	=sqrt(xl^2+yl^2+zl^2)
 for ifoot=0,nfoot-1 do begin
  if (ifoot eq 0) then begin &is1=0    &is2=1    &endif
  if (ifoot eq 1) then begin &is1=ns-1 &is2=ns-2 &endif
  if (rl(is1) le (1.+hfoot)) then begin
   xf   =xl(is1)
   yf   =yl(is1)
   zf	=sqrt(1.-xf^2-yf^2)
   if (abs(rl(is1)-1.) ge eps) then begin
    q    =(rl[is2]-1.)/(rl[is1]-1.)
    xf   =(xl[is1]*q-xl[is2])/(q-1.)
    yf   =(yl[is1]*q-yl[is2])/(q-1.)
    zf	=sqrt(1.-xf^2-yf^2) 
   endif
   footp[il,ifoot,0]=xf
   footp[il,ifoot,1]=yf
   footp[il,ifoot,2]=zf
  endif
 endfor
endfor

print,'_____________POTENTIAL FIELD BX,BY,BZ__________________________'
x1	=fov[0]
x2	=fov[2]
y1	=fov[1]
y2	=fov[3]
nx	=long((x2-x1)/ds)
ny	=long((y2-y1)/ds)
print,'coordinate grid nx x ny = ',nx,ny
x	=x1+ds*findgen(nx)
y	=y1+ds*findgen(ny)
bx_p	=fltarr(nx,ny)
by_p	=fltarr(nx,ny)
bz_p	=fltarr(nx,ny)
amis0	=0.
polar 	=1
for j=0,ny-1 do begin
 for i=0,nx-1 do begin
  xa	=x[i]
  ya	=y[j]
  za	=sqrt(1.-xa^2-ya^2)
  aia_mag_vector,xa,ya,za,ds,hmin,hmax,coeff,curr,amis0,polar,xb,yb,zb,b_tot
  bx_p(i,j)=b_tot*(xb-xa)/(ds*polar)
  by_p(i,j)=b_tot*(yb-ya)/(ds*polar)
  bz_p(i,j)=b_tot*(zb-za)/(ds*polar)
 endfor
endfor

print,'_____________NON-POTENTIAL FIELD VECTORS_______________________'
code    ='h'    ;helical flux tube fitting
model	='n'	;non-potential model
fieldfile=fileset+'_loop_'+model+code+'.sav'
restore,fieldfile		;-->field_ ,dev_loop,cpu
print,'Field lines restored from file : ',fieldfile

bx_np	=bx_p
by_np	=by_p
bz_np	=bz_p
ix_loop =fltarr(nloop,nfoot)
iy_loop =fltarr(nloop,nfoot)
for il=0,nloop-1 do begin
 ind	=where(field_(*,il,0) ne 0,ns)
 if (ns ge 1) then begin
 n	=ns-1
 d1	=sqrt((field_[0,il,0]-footp[il,0,0])^2+(field_[0,il,1]-footp[il,0,1])^2)
 d2	=sqrt((field_[n,il,0]-footp[il,0,0])^2+(field_[n,il,1]-footp[il,0,1])^2)
 if (d1 le d2) then is_=[0,ns-1]
 if (d1 gt d2) then is_=[ns-1,0]
 for ifoot=0,1 do begin
  is	=is_[ifoot]  
  xm	=footp[il,ifoot,0] 
  ym	=footp[il,ifoot,1]
  zm	=footp[il,ifoot,2]
  dx_b	=field_[is,il,0]
  dy_b	=field_[is,il,1]
  dz_b	=field_[is,il,2]
  ds_b  =sqrt(dx_b^2+dy_b^2+dz_b^2)
  b_tot =field_[0,il,3]
  ix	=(long((dx_b-x1)/ds)>0) < (nx-1)
  iy	=(long((dy_b-y1)/ds)>0) < (ny-1)
  ix_loop[il,ifoot]=ix
  iy_loop[il,ifoot]=iy
  bx_np(ix,iy)=b_tot*(dx_b/ds_b)
  by_np(ix,iy)=b_tot*(dy_b/ds_b)
  bz_np(ix,iy)=b_tot*(dz_b/ds_b)
 endfor
 endif
endfor

print,'_____________RENORMALIZATION OF LOS-COMPONENT__________________'
ind	=where(bz_np ne 0)
q_ind	=bz_p(ind)/bz_np(ind)
bx_np(ind)=bx_np*q_ind
by_np(ind)=by_np*q_ind
bz_np(ind)=bz_np*q_ind
b	  =sqrt(bx_np^2+by_np^2+bz_np^2)

print,'_____________ELECTRIC CURRENTS_________________________________'
rsun	 =6.96e10		;cm
c	 =3.0e10		;cm/s	speed of light
jz       =fltarr(nx,ny)
alpha	 =fltarr(nx,ny)
jcurr	 =fltarr(nx,ny)
alpha_cgs=fltarr(nloop,2)
jcurr_cgs=fltarr(nloop,2)
for il=0,nloop-1 do begin
 for ifoot=0,1 do begin
  ix	=ix_loop[il,ifoot]
  iy	=iy_loop[il,ifoot]
  if (ix ne 0) and (iy ne 0) then begin
   alpha_cgs[il,ifoot]=alpha_loop[il]/rsun
   jcurr_cgs[il,ifoot]=c*alpha_cgs[il]*b(ix,iy)
   alpha(ix,iy)=alpha_cgs[il,ifoot]/rsun
   jcurr(ix,iy)=jcurr_cgs[il,ifoot]
  endif
 endfor
endfor

print,'_____________SAVE FIELDLINES___________________________________'
fieldfile=fileset+'_field_'+model+code+'.sav'
save,filename=fieldfile,bx_p,by_p,bz_p,bx_np,by_np,bz_np,$
  alpha,jcurr,alpha_cgs,jcurr_cgs,ix_loop,iy_loop,footp
print,'Field lines saved in file : ',fieldfile
end

