function euv_dimming_function2,coeff

common dimming,model,t_hrs,a_t,v_t,x_t,em_t,em_obs,em1,em2,h0,t0_hrs

rsun_cm	=6.96e10 ;cm
t	=t_hrs*3600.			;s
a0	=coeff(0)*1.e4 
tau1_hrs=coeff(1)	 		;hrs
tau2_hrs=coeff(2)			;hrs
qem	=coeff(3)
tau1	=tau1_hrs*3600.			;s
tau2	=tau2_hrs*3600.			;s
t0	=t0_hrs*3600.			;s
t1	=t0-tau1			;s
t2	=t0+tau2			;s
em2	=em1*qem

ind0	=where((t le t1),n0)
ind1	=where((t gt t1) and (t le t2),n1)
ind2	=where((t gt t2),n2)
nt	=n_elements(t)

;_____________________________acceleration profile_______________________________
a_t	=fltarr(nt)
a_t(ind1)=a0

;_____________________________velocity profile___________________________________
v_t	=fltarr(nt)
v_t(ind1)=a0*(t(ind1)-t1)
v0	 =a0*(t2-t1)
v_t(ind2)=v0

;_____________________________distance profile___________________________________
x_t	=fltarr(nt)
x_t(ind0)=h0
x_t(ind1)=(a0/2.)*(t(ind1)-t1)^2 + h0
x_t(ind2)=(a0/2.)*(t2-t1)^2 + v0*(t(ind2)-t2) + h0

;_____________________________emission measure profile___________________________
em_t	=fltarr(nt)
q	=((rsun_cm+h0)^3-rsun_cm^3)/((rsun_cm+x_t)^3-rsun_cm^3)
ind3	=where((t le t0),n3)
ind4	=where((t gt t0),n4)
em_t(ind3)=em2+(em1-em2)*exp(-(t(ind3)-t0)^2/(2.*(t1-t0)^2))	;filling
em_t(ind4)=em2+(em1-em2)*q(ind4)				;dimming

nfree	=2
n	=nt-nfree
y_obs	=em_obs/em1
y_model	=em_t/em1
y_sigma =0.05*y_obs
chi	=sqrt(total((y_model-y_obs)^2./y_sigma^2)/float(n))

return,chi
end
