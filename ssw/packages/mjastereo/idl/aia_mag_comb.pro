pro aia_mag_comb,fileset,vers,wave_,segmin
;+
; Project     : AIA/SDO
;
; Name        : AIA_MAG_LOOP
;
; Category    : SDO/AIA data modeling 
;
; Explanation : Fitting of potential magnetic field model parameterized with
;		unipolar charges to observed loop projections
;
; Syntax      : IDL>aia_mag_loop,fileset,wave,fov,hmax,nh,segmin,rope
;
; Inputs      : fileset	 = beginning of filename 
;               vers     = label in filename (e.g., vers='a')
;		wave_    = wavelengths
;		segmin   = minimum field line length
;
; Outputs:      outfile *_0006_loop.dat containing selected fieldfiles
;
; History     : 2-Jun-2011, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

;wavelength-dependent files...........................................
nwave	=n_elements(wave_)
outfile =fileset+vers+'_006_auto.dat'
nsel	=0
ntotal	=0
openw,2,outfile
for iw=0,nwave-1 do begin
 wave	=wave_(iw)
 loopfile  =file_search(fileset+vers+'_'+wave+'_auto.dat')
 readcol,loopfile[0],iloop,x,y,z,s
 nloop   =max(iloop)+1
 ntotal	 =ntotal+nloop
 for il=0,nloop-1 do begin
  ind	=where(iloop eq il,np)
  if (max(s(ind)) ge segmin) then begin
   for ip=0,np-1 do begin
    ii	=ind(ip)
    printf,2,nsel,x(ii),y(ii),z(ii),s(ii)
   endfor
   nsel	=nsel+1
  endif
 endfor
endfor
close,2

print,'Total number of loops in 6 wavelengths : ',ntotal
print,'Number of selected loops : ',nsel
print,'Selected field lines saved in file : ',outfile
end
