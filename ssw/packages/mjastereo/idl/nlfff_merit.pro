pro nlfff_merit,dir,savefile,test
;+
; Project     : SOHO/MDI, SDO/HMI, STEREO
;
; Name        : NLFFF_MERIT 
;
; Category    : Magnetic data modeling
;
; Explanation : Calculates figure of merits of a NLFFF solution:
;		divergence-freeness and force-freeness:	MERIT=[ld,lf,s_j]
;
; Syntax      : IDL>nlfff_merit,dir,savefile,test
;
; Inputs      : dir,savefile,test
;
; Outputs     ; updates savefile -->bzmap,x,y,field_lines,q_scale,angle,cpu,merit,iter
;
; History     : 28-Nov-2011, Version 1 written by Markus J. Aschwanden
;               30-Jan-2012, Version 2 released to SSW
;
; Contact     : aschwanden@lmsal.com
;-

print,'__________________NLFFF_MERIT___________________'
restore,dir+savefile    ;--> PARA
fov0    =input.fov0      ;field-of-view
filename=strmid(savefile,0,15)
x1_sun  =para.x1_sun
x2_sun  =para.x2_sun
y1_sun  =para.y1_sun
y2_sun  =para.y2_sun
dim     =size(bzmap)
nx      =dim(1)
ny      =dim(2)
dpix    =float(x2_sun-x1_sun)/float(nx)
x       =x1_sun+dpix*findgen(nx)
y       =y1_sun+dpix*findgen(ny)
nz      =n_elements(z)

;_____________________CURRENTS_____________________ 
jx	=fltarr(nx,ny,nz)
jy	=fltarr(nx,ny,nz)
jz	=fltarr(nx,ny,nz)
div_b	=fltarr(nx,ny,nz)
for iz=1,nz-2 do begin
 for iy=1,ny-2 do begin
  dbx_dy=(bx(1:nx-2,iy+1,iz)-bx(1:nx-2,iy-1,iz))/2.
  dbx_dz=(bx(1:nx-2,iy,iz+1)-bx(1:nx-2,iy,iz-1))/2.
  dby_dx=(by(2:nx-1,iy,iz  )-by(0:nx-3,iy,iz  ))/2.
  dby_dz=(by(1:nx-2,iy,iz+1)-by(1:nx-2,iy,iz-1))/2.
  dbz_dx=(bz(2:nx-1,iy,iz  )-bz(0:nx-3,iy,iz  ))/2.
  dbz_dy=(bz(1:nx-2,iy+1,iz)-bz(1:nx-2,iy-1,iz))/2.
  jx(1:nx-2,iy,iz)=dbz_dy-dby_dz
  jy(1:nx-2,iy,iz)=dbx_dz-dbz_dx
  jz(1:nx-2,iy,iz)=dby_dx-dbx_dy
  dbx_dx=(bx(2:nx-1,iy  ,iz)-bx(0:nx-3,iy  ,iz  ))/2.
  dby_dy=(by(1:nx-2,iy+1,iz)-by(1:nx-2,iy-1,iz  ))/2.
  dbz_dz=(bz(1:nx-2,iy,iz+1)-bz(1:nx-2,iy  ,iz-1))/2.
  div_b(1:nx-2,iy,iz)=dbx_dx+dby_dy+dbz_dz
 endfor
endfor

;_____________________CURL B = j x B_____________________________ 
jxb1=fltarr(nx,ny,nz)
jxb2=fltarr(nx,ny,nz)
jxb3=fltarr(nx,ny,nz)
for iz=1,nz-2 do begin
 for iy=1,ny-2 do begin
  jxb1(1:nx-2,iy,iz)=jy(1:nx-2,iy,iz)*bz(1:nx-2,iy,iz)-jz(1:nx-2,iy,iz)*by(1:nx-2,iy,iz)
  jxb2(1:nx-2,iy,iz)=jz(1:nx-2,iy,iz)*bx(1:nx-2,iy,iz)-jx(1:nx-2,iy,iz)*bz(1:nx-2,iy,iz)
  jxb3(1:nx-2,iy,iz)=jx(1:nx-2,iy,iz)*by(1:nx-2,iy,iz)-jy(1:nx-2,iy,iz)*bx(1:nx-2,iy,iz)
 endfor
endfor
abs_jxb =sqrt(jxb1^2+jxb2^2+jxb3^2)
abs_j   =sqrt(jx^2+jy^2+jz^2)
s_j	=total(abs_jxb(1:nx-2,1:ny-2,1:nz-2)/b(1:nx-2,1:ny-2,1:nz-2))/$
         total(abs_j(1:nx-2,1:ny-2,1:nz-2))

;____________________Force-freeness______________________________
vol	=float(nx-2)*float(ny-2)*float(nz-2)
lf	=total(abs_jxb(1:nx-2,1:ny-2,1:nz-2)^2/b(1:nx-2,1:ny-2,1:nz-2)^4)/vol

;____________________Divergence-freeness_________________________
ld	=total(div_b(1:nx-2,1:ny-2,1:nz-2)^2/b(1:nx-2,1:ny-2,1:nz-2)^2)/vol

;_____________________FIGURE OF MERIT____________________________
print,' Maximum magnetic field = ',max(abs(b)) 
print,' Divergence freeness    = ',ld
print,' Force freeness         = ',lf
print,' Weighted current       = ',s_j

;____________________SAVE MERIT PARAMETER__________________________
merit	=[ld,lf,s_j]
;____________________SAVE DATACUBE____________________________________
para.merit=merit 
save,filename=dir+savefile,input,para,bzmap,bzfull,bzmodel,coeff,$
     field_loop_det,ns_loop_det,wave_loop_det,$
     field_loop,ns_loop,wave_loop,dev_deg,dev_deg2,$
     field_lines,ns_loop,ns_field,x,y,z,bx,by,bz,b,a
print,'parameters saved in file = ',savefile
end
