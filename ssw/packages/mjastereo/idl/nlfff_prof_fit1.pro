pro nlfff_prof_fit1,time,efree,nloop,t5,dtol,t,e,e1,e2,te1,te2,err2,chi2

;time profile of free energy is obtained from median and boxcar smoothing,
;and the energy decrease from E1 to E2 with error ERR
;at times TE1,TE2 ;during time range TIME[IND_STEP] 

;_______________________ENERGY DISSIPATION______________________
nt	=n_elements(time)
emed	=fltarr(nt)
for it=1,nt-2 do emed(it)=median(efree(it-1:it+1))
emed(0)=emed(1)
emed(nt-1)=emed(nt-2)
e_	=fltarr(nt)
nvar	=2
nbin	=nt-nvar
dt1	=min(abs(time-t5(2)),it1)
dt2	=min(abs(time-t5(2)),it2)
dt3	=min(abs(time-(t5(1)-dtol)),it3)
dt4	=min(abs(time-(t5(3)+dtol)),it4)
t3	=time(it3)
t4	=time(it4)
sigma	=fltarr(nt)
ind	=where(emed gt 0)
sigma(ind)=emed(ind)/sqrt(nloop(ind))
chimin	=9999.
for i1=it3,it1 do begin
 for i2=it2,it4 do begin
  te1_	=time(i1)
  te2_	=time(i2)
  ind1	=where((time le te1_),n1)
  ind0	=where((time gt te1_) and (time lt te2_),n0)
  ind2	=where((time ge te2_),n2)
  statistic,emed(ind1),e1_,sig1
  statistic,emed(ind2),e2_,sig2
  e_(ind1)=e1_(ind1)
  e_(ind0)=e1_+(e2_-e1_)*(time(ind0)-te1_)/(te2_-te1_)
  e_(ind2)=e2_(ind2)
  err2	=sqrt(sig1^2/n1+sig2^2/n2)
  chi2_	=sqrt((1./nbin)*total((e_-emed)^2/sigma^2))
  if (chi2_ lt chimin) then begin
   te1 =te1_
   te2 =te2_
   e1  =e1_
   e2  =e2_
   t   =time(ind)
   e   =e_(ind)
   chi2=chi2_
   chimin=chi2_
  endif
 endfor
endfor
end
