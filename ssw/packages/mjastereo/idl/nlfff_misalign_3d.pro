pro nlfff_misalign_3d,x_fit,v_fit,nf,coeff,dev,dev_loop,para,len
;+
; Project     : SOHO/MDI, SDO/HMI, STEREO
;
; Name        : NLFFF_MISALIGN 
;
; Category    : Magnetic data modeling
;
; Explanation : Subroutine in support of MAGN_FIT.PRO,
;		which calcultes the misalignment angle between an observed
;		coronal loop FIELD_LINES and a theoretical NLFFF model.
;               The fitting constraints can be stereoscopic
;               loop coordinates 
;
; Syntax      : IDL>nlfff_misalign,...
; 
; Inputs      : field_lines[NS,3,NF] = 3D coordinates of loop fieldlines
;		nf	= number of field lines
;		coeff	= magnetic model coefficients [b1,x1,y1,z1,a1]
;
; Outputs     ; dev	= average misalignment angle of all loops 
;		dev_loop[nf] = average misalignment angles per loop 
;
; History     : 16-May-2011, Version 1 written by Markus J. Aschwanden
;               30-Jan-2012, Version 2 released to SSW
;
; Contact     : aschwanden@lmsal.com
;-


;_________________2D MISALIGNMENT LOOP/FIELD FOR BEST ALTITUDE_______________
nlfff_vector,coeff,x_fit,bfff_			;B-field at position x_fit
vector_product_array,v_fit,bfff_,v3,v3_norm,angle_rad  ;3D misalign angle
dev_rad =angle_rad < (!pi-angle_rad)		;180-deg ambiguity
dev_deg =(180./!pi)*dev_rad      		;radian into degrees
dev_med =median(dev_deg)
end 

