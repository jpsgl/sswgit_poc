pro nlfff_magn,dir_hmi,dir,filename,vers,para
;+
; Project     : SDO, STEREO, SOHO
;
; Name        : MAGN_MDIDATA
;
; Category    : magnetic field moedling
;
; Explanation : reads MDI magnetogram data in chosen field-of-view (FOV)
;		reads and selects loop file (3D coordinates, x,y,z), 
;               and stores parameters in savefile
;
; Syntax      : IDL>nlfff_input,dir,filename,vers,para
;
; Inputs      : filename =image file
;		vers	 =version of output file
;               para     =input parameters
;
; Outputs     ; saves parameters in file <filename>_field_<vers>.sav:  
;               --> para,bzma
;
; History     : 27-Feb-2014, Version 1 written by Markus J. Aschwanden
;		22-Jan-2015: change wmax=dpix*iwmax	
;		28-Jan-2015: iwmax=5  ;pixels
;
; Contact     : aschwanden@lmsal.com
;-

print,'__________________NLFFF_MAGN__________________________
nmag0   =long(para[1])
dsmag	=para[3]  	  
x1      =para[17]
y1      =para[18]
x2      =para[19]
y2      =para[20]
iwmax	=1				;pixels of local peak
iwrange	=20				;pixels of depth range

;________________READ MAGNETIC DATA________________________
magfile =dir_hmi+filename+'_magn.fits'
image   =readfits(magfile,index_mag,/silent)
dateobs =sxpar(index_mag,'DATE_OBS')
datamin =sxpar(index_mag,'DATAMIN')
datamax =sxpar(index_mag,'DATAMAX')
nx      =sxpar(index_mag,'NAXIS1')
ny      =sxpar(index_mag,'NAXIS2')
crpix1  =sxpar(index_mag,'CRPIX1')
crpix2  =sxpar(index_mag,'CRPIX2')
cdelt1  =sxpar(index_mag,'CDELT1')
cdelt2  =sxpar(index_mag,'CDELT2')
pos0	=sxpar(index_mag,'SOLAR_P0')
rpix	=sxpar(index_mag,'R_SUN')
if (pos0 eq 0.) then pos0=sxpar(index_mag,'CROTA2')
if (rpix eq 0.) then rpix=sxpar(index_mag,'RSUN_OBS')/cdelt1
dpix    =1./rpix			;pixel size in solar radii

;________________EXTRACT MAGNETOGRAM_________________________
p0      =pos0*!pi/180.
i1      =long(crpix1+x1/dpix) < (nx-1)
i2      =long(crpix1+x2/dpix) < (nx-1)
j1      =long(crpix2+y1/dpix) < (ny-1)
j2      =long(crpix2+y2/dpix) < (ny-1)
ni	=(i2-i1+1)
nj	=(j2-j1+1)
x	=x1+dpix*findgen(ni)
y	=y1+dpix*findgen(nj)
bzmap	=image(i1:i2,j1:j2)

;________________ROTATION OF MDI IMAGE______________________
if (p0 ne 0.) then begin
 zmin	=min(bzmap)
 for j=0,nj-1 do begin
  y_     =fltarr(ny)+y(j)
  x_rot  = x*cos(p0)+y_*sin(p0)
  y_rot  =-x*sin(p0)+y_*cos(p0)
  i_     =long(rpix*x_rot+crpix1) 
  j_     =long(rpix*y_rot+crpix2) 
  stripe =image(i_,j_)
  ind0   =where(stripe eq zmin,nind0)
  if (nind0 ge 1) then stripe(ind0)=0.
  r	 =sqrt(x^2+y(j)^2)
  indr	 =where(r ge 0.999,nindr)
  if (nindr ge 1) then stripe(indr)=0.
  bzmap(*,j)=stripe
 endfor
endif

;________________REBIN MAP____________________________________
nbin	=long(dsmag/dpix+0.5)>1
nxx	=ni
nyy	=nj
bzfull  =bzmap			;full resolution map
xfull	=x
yfull	=y
if (nbin eq 1) then begin
 x	=x1+dpix*findgen(nxx)
 y	=y1+dpix*findgen(nyy)
endif
if (nbin ge 2) then begin
 nxx	=(ni/nbin)
 nyy	=(nj/nbin)
 bzmap  =rebin(bzfull(0:nxx*nbin-1,0:nyy*nbin-1),nxx,nyy)
 x	=rebin(xfull[0:nxx*nbin-1],nxx) 
 y	=rebin(yfull[0:nyy*nbin-1],nyy) 
endif
dim	=size(bzmap)
nx	=dim(1)
ny	=dim(2)

;__________________DCOMPOSITION OF MAGN SOURCES_________________________'
;print,'  IS       IPOL     M       X       Y       Z      -H '
coeff	=fltarr(nmag0,4)
ipix	=lonarr(6,nmag0)
magn	=max(abs(bzmap))		;max field strength
bz_obs	=fltarr(nmag0)
ix_obs	=fltarr(nmag0)
iy_obs	=fltarr(nmag0)
eps	=1.e-6
icomp	=0
qm	=0.5				;RUN4
qm	=0.25				;RUN5
wmax	=dpix*iwmax			;solar radii
wrange	=dpix*iwrange			;solar radii
wmin	=dpix*0.5			;solar radii
nw	=100
image_res=float(bzmap)			
for is=0,nmag0-1 do begin
 bzabs	=max(abs(image_res),im)
 iy	=long(im/long(nx))	
 ix	=im-iy*long(nx)
 bz	=image_res(ix,iy)
 bz_obs(icomp)=bz
 ix_obs(icomp)=ix
 iy_obs(icomp)=iy
 xprof  =image_res(*,iy)
 yprof  =reform(image_res(ix,*))
 i3min	=(ix-iwmax)>0
 i4max	=(ix+iwmax)<(nx-1)
 j3min	=(iy-iwmax)>0
 j4max	=(iy+iwmax)<(ny-1)
 for i3=ix,i3min,-1 do if (abs(xprof(i3)) le abs(xprof(ix))*qm) then goto,left
LEFT:
 for i4=ix,i4max do if (abs(xprof(i4)) le abs(xprof(ix))*qm) then goto,right
RIGHT:
 for j3=iy,j3min,-1 do if (abs(yprof(j3)) le abs(yprof(iy))*qm) then goto,bottom
BOTTOM:
 for j4=iy,j4max-1 do if (abs(yprof(j4)) le abs(yprof(iy))*qm) then goto,top
TOP:
 i3	=((i3 < ix-1) >     0 ) 
 i4	=((i4 > ix+1) < (nx-1)) 
 j3	=((j3 < iy-1) >     0 ) 
 j4	=((j4 > iy+1) < (ny-1)) 
 nx_model=i4-i3+1			;local map
 ny_model=j4-j3+1
 n_model =nx_model*ny_model
 ic_model=ix-i3				;local map center 
 jc_model=iy-j3
 ipix[*,is]=[i3,j3,i4,j4,ix,iy]
 x_model=x[i3:i4]
 y_model=y[j3:j4]
 image_local=image_res[i3:i4,j3:j4]
 xp	=x[ix]  	
 yp	=y[iy] 		
 zp	=sqrt((1.-xp^2-yp^2) > eps)
 am	=0.
 for iw=0,nw-1 do begin
  wp	=wmin+(wrange-wmin)*(float(iw)/float(nw-1))^2 
  aia_mag_inversion,bz,xp,yp,zp,wp,bm,xm,ym,zm
  coeff_local=[[bm],[xm],[ym],[zm],[am]]
  aia_mag_map,coeff_local,1.,x_model,y_model,model_local
  dev	=abs(total(image_local-model_local))
  if (iw eq 0) then devmin=dev
  rm	=sqrt(xm^2+ym^2+zm^2)
  if (dev le devmin) and (rm lt 0.999) then begin
   devmin=dev
   coeff(icomp,0)=bm				
   coeff(icomp,1)=xm
   coeff(icomp,2)=ym
   coeff(icomp,3)=zm
;TEST DISPLAY______________________________________
   test=1
   if (test eq 1) then begin
    dim=size(model_local)
    c2	=max(abs(image_local))
    c1	=-c2
    diff_image=model_local-image_local
    tv,bytscl(rebin(image_local,dim(1)*16,dim(2)*16),min=c1,max=c2)
    tv,bytscl(rebin(model_local,dim(1)*16,dim(2)*16),min=c1,max=c2),dim(1)*16.,0
    tv,bytscl(rebin( diff_image,dim(1)*16,dim(2)*16),min=c1,max=c2),dim(1)*32.,0
    print,'iw,wp,dev=',iw,wp/dpix,dev
    erase
    clearplot
    !p.position=[0.1,0.1,0.9,0.45]
    plot,image_local(*,iy-j3),thick=3
    oplot,model_local(*,iy-j3)
    !noeras=1
    !p.position=[0.1,0.55,0.9,0.9]
    plot,image_local(ix-i3,*),thick=3
    oplot,model_local(ix-i3,*)
    wait,0.1
   endif
;___________________________________________________
  endif
 endfor
 if (test eq 1) then begin
  read,'continue?',yes
  if (yes eq 0) then stop
 endif
 aia_mag_map,coeff[icomp,*],1,x,y,model_map
 image_res=image_res-model_map 
;________________________erase core of structure_______________________
 ind	=where(abs(model_map) ge 0.5*max(abs(model_map)))
 image_res(ind)=0.
;______________________________________________________________________
;if ((is mod 10) eq 0) then print,is,long(coeff[icomp,0]),' Gauss'
 icomp	=icomp+1
END_DECOMP:
endfor
nmag	=icomp
coeff	=coeff[0:nmag-1,*]

;________________MAGNETIC FLUX NORMALIZATION______________________
aia_mag_map,coeff,1.,x,y,bzmodel	  ;---> B map
qb_rebin=sqrt(total(bzfull^2)/total((bzmap*nbin)^2))
qb_model=total(abs(bzmodel))/total(abs(bzmap))  
coeff[*,0]=coeff[*,0]/qb_model		  ;magnetic flux normalization   
aia_mag_map,coeff,1.,x,y,bzmodel	  ;---> renomralized B map
qn_model=total(abs(bzmodel))/total(abs(bzmap))  
qe_model=total(bzmodel^2)/total(bzmap^2)  ;energy conservation

print,'Number of magnetic sources  = ',nmag,' /',nmag0
print,'Maximum magnetic field bzmap= ',magn
print,'Maximum magnetic field corff= ',max(abs(coeff[*,0]))
print,'Magnetic field rebin factor = ',qb_rebin,format='(a,f6.3)'
print,'Magnetic field model factor = ',qb_model,format='(a,f6.3)'
print,'Magnetic field renormalized = ',qn_model,format='(a,f6.3)'
print,'Magnetic energy conservation= ',qe_model,format='(a,f6.3)'

;________________WRITE FILE WITH MODEL MAGNETIC COEFFICIENTS______
coeff_file=dir+filename+'_coeff_'+vers+'.dat'
openw,2,coeff_file
for is=0,nmag-1 do begin
 m1	=coeff(is,0)
 x1	=coeff(is,1)
 y1	=coeff(is,2)
 z1	=coeff(is,3)
 a1	=0.
 printf,2,m1,x1,y1,z1,a1,format='(5f12.5)'
;print,is,m1,x1,y1,z1,a1,format='(I4,I12,4F12.5)'
endfor
close,2

;______________DISPLAY OF HMI FOV IMAGE_____________________________
zoom	=2
loadct,3,/silent
window,1,xsize=nx*zoom*2,ysize=ny*zoom*2
diff_image=bzmap-bzmodel
diff_image0=diff_image
tv,bytscl(rebin(bzmap,nx*zoom,ny*zoom) ,min=-magn,max=+magn),0,ny*zoom
tv,bytscl(rebin(bzmodel,nx*zoom,ny*zoom),min=-magn,max=+magn),0,0

!p.position=[0.5,0.5,1,1]
!x.range=minmax(x)
!y.range=minmax(y)
!x.style=5
!y.style=5
plot,[0,0],[0,0]
tv,bytscl(rebin(diff_image0,nx*zoom,ny*zoom),min=-magn,max=+magn),nx*zoom,ny*zoom
if (nmag le 10) then begin
for is=0,nmag-1 do begin
 i3	=x[ipix[0,is]]
 j3	=y[ipix[1,is]]
 i4	=x[ipix[2,is]]
 j4	=y[ipix[3,is]]
 oplot,[i3,i4,i4,i3,i3],[j3,j3,j4,j4,j3],color=255
 xyouts,i3,j4,string(is,'(I3)'),color=255
endfor
endif
!noeras=1

!p.position=[0.55,0.05,0.95,0.45]
!p.title='East-West scan'
!x.style=1
!y.style=0
!x.range=minmax(x)
!y.range=magn*[-1,1]
is	=0
i3	=ipix[0,is]
i4	=ipix[2,is]
ix	=ipix[4,is]
iy	=ipix[5,is]
plot,!x.range,[0,0],linestyle=1
oplot,x,bzmodel[*,iy],thick=2
oplot,x[i3:i4],bzmodel[i3:i4,iy],thick=5
oplot ,x,bzmap[*,iy],color=150,thick=2

;________________SAVE PARAMETERS______________________________
savefile =dir+filename+'_field_'+vers+'.sav'
save,filename=savefile,para,bzmap,bzmodel,qb_rebin,qb_model,nmag 
;print,'parameters saved in file = ',savefile
end
