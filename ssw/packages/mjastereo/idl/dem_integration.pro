pro dem_integration,telog,demlog,len,tep_log,tew_log,emp_log,emt_log,eth_log
;+
; Project     : SDO, AIA
;
; Name        : DEM_INTEGRATION 
;
; Category    : DEM analysis
;
; Explanation : calculates DEM parameters by integration over a given
;		logarithmid DEM/DT 
;
; Syntax      : IDL>dem_integration,telog,demlog,tep40,tew40,emp40,emtot40,eth30
;
; Inputs      : telog	=log temperature array [K]
;		demlog	=log emission measure  [cm-3 K-1]
;		len	=length scale [cm]
;
; Outputs     ; tep_log	=log peak temperature [MK]
;               tew_log =log emission measured weighted temperature [MK]
;		emp_log	=log peak  emission measure [10^40 cm-3 K-1]
;		emt_log	=log total emission measure [10^40 cm-3 K-1]
;		eth_log	=log thermal energy [10^30 erg]
;
; History     : 16-Mar-2012, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

;test...............................................................
nte	=n_elements(telog)
nem	=n_elements(demlog)
if (nte ne nem) then stop,'DEM_INTEGRATION: unequal arrays TELOG, DEMLOG'
if (min(telog) lt 3.) or (max(telog) gt 9) then $
 stop,'DEM_INTEGRATION: invalid temperature range'
demlog	=demlog > 0 
if (min(demlog) lt  0) then stop,'DEM_INTEGRATION: negative emission measures'
if (max(demlog) gt 60) then stop,'DEM_INTEGRATION: excessive emission measure'

;peak temperature and emission measure..............................
emp_log =max(demlog,im)
tep_log	=telog(im)

;emission-measure weighted temperature..............................
te	=10.^telog
dte	=[te[1:nte-1]-te[0:nte-2],0.]
dem40	=10.^(demlog-40.)
dem40_bin=dem40*dte
tew	=total(te*dem40_bin)/total(dem40_bin)
tew_log	=alog10(tew)

;total emission measure.............................................
emt_log	=alog10(total(dem40_bin))+40.

;thermal energy.....................................................
kb      =1.3807e-16     ;erg/K
vol	=len^3
eth20	=3.*kb*sqrt(vol)*total(te*dem40_bin^(1./2.))
eth_log	=alog10(eth20)+20.
end

