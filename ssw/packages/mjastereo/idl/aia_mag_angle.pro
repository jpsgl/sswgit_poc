pro aia_mag_angle,bx_p,by_p,bz_p,x0,y0,z0,ds,xl,yl,zl,nseg,angle_,dev_,dev

polar  =1.
dev_   =fltarr(nseg)
angle_ =fltarr(3,3,nseg)
ns     =n_elements(xl)
im_    =long(ns*(1+findgen(nseg))/float(nseg+1))<(ns-2)
for iseg=0,nseg-1 do begin
 im    =im_(iseg)
 xa_   =[xl[im],yl[im],zl[im]]
 aia_mag_bvector,bx_p,by_p,bz_p,x0,y0,z0,ds,polar,xa_,xb_,ba_,boundary
 v1    =[xl[im+1]-xl[im],yl[im+1]-yl[im],zl[im+1]-zl[im]]
 v2    =[xb_[0]-xa_[0],xb_[1]-xa_[1],xb_[2]-xa_[2]]
 v1len =sqrt(total(v1*v1))
 v2len =sqrt(total(v2*v2))
 dev1  =acos((total(v1*v2)/(v1len*v2len))<1.)        ;scalar product
 dev_rad=dev1 < (!pi-dev1)
 dev_(iseg)=(180./!pi)*dev_rad                       ;misalignment angle
 angle_(*,0,iseg)=[xl(im-1),xa_[0],xb_[0]]
 angle_(*,1,iseg)=[yl(im-1),xa_[1],xb_[1]]
 angle_(*,2,iseg)=[zl(im-1),xa_[2],xb_[2]]
endfor
dev    =median(dev_)
end 

