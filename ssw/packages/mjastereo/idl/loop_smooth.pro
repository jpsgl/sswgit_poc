pro loop_smooth,xp,yp

;smoothing
np     =n_elements(xp)
ind    =findgen(np)
ind1   =where((ind mod 3) eq 0,n1)
ind2   =where((ind mod 3) eq 1,n2)
ind3   =where((ind mod 3) eq 2,n3)
ind1   =[0,ind1,np-1] &if ((np-1) mod 3) eq 0 then ind1(n1)=ind1(n1)-1
ind2   =[0,ind2,np-1] &if ((np-2) mod 3) eq 0 then ind2(n2)=ind2(n2)-1
ind3   =[0,ind3,np-1] &if ((np  ) mod 3) eq 0 then ind3(n3)=ind3(n3)-1
ind1(1)=1
spline_p,xp(ind1),yp(ind1),xs1,ys1
spline_p,xp(ind2),yp(ind2),xs2,ys2
spline_p,xp(ind3),yp(ind3),xs3,ys3
for i=0,np-1 do begin
 dmin1=min((xs1-xp(i))^2+(ys1-yp(i))^2,i1)
 dmin2=min((xs2-xp(i))^2+(ys2-yp(i))^2,i2)
 dmin3=min((xs3-xp(i))^2+(ys3-yp(i))^2,i3)
 xp(i)=(xs1(i1)+xs2(i2)+xs3(i3))/3.
 yp(i)=(ys1(i1)+ys2(i2)+ys3(i3))/3.
endfor
end

