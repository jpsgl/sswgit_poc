pro euvi_coalign,image_pair,coaligned_pair,para,roll_max,roll,dx,dy
;+
; Project     : STEREO
;
; Name        : EUVI_COALIGN 
;
; Category    : Data analysis 
;
; Explanation : This routine performs an empirical finetuning
;		of the imperfect coalignment of a STEREO image pair,
;		based on limb edge detection and correlation of
;		photospheric features.
;
; Syntax      : IDL> euvi_coalign,image_pair,coaligned_pair,para,roll_max,roll,dx,dy
;
; Inputs      : IMAGE_PAIR	=UINT image array (2,nx,ny)
;		PARA 		=structure with image parameters
;		ROLL_MAX	=maximum range of roll angle variation
;
; Outputs     : COALIGNED_PAIR	=fltarr(2,nx,ny)
;		ROLL		=correction in roll angle (clockwise)
;		DX		=correction shift in x-axis
;		DY		=correction shift in y-axis
;
; History     : 30-Aug-2007, Version 1 written by Markus J. Aschwanden
;		17-Nov-2008  insert: coaligned_pair(1,*,*)=image_roll(*,*)
;
; Contact     : aschwanden@lmsal.com
;-

imagea	=float(reform(image_pair(0,*,*)))
imageb	=float(reform(image_pair(1,*,*)))
dim	=size(image_pair)
n2	=dim(1)
nx	=dim(2)
ny	=dim(3)
jj1	=ny/7
jj2	=ny-ny/7
nyy	=jj2-jj1+1
crpix1	=para.crpix1
crpix2	=para.crpix2
cdelt1	=para.cdelt1
cdelt2	=para.cdelt2
rsun	=para.rsun
sep_deg	=para.sep_deg
rpix1	=rsun/cdelt1
rpix2	=rsun/cdelt2
phi	=2.*!pi*findgen(361)/360.
print,'Sun center position   CRPIX1,CRPIX2 = ',crpix1,crpix2
print,'Pixel size            CDELT1,CDELT2 = ',cdelt1,cdelt2
print,'Solar radius A        RSUN          = ',rsun

window,0,xsize=nx,ysize=nyy
set_plot,'x'
!p.position=[0,0,1,1]
!x.range=[0,nx]
!y.range=[jj1,jj2]
!x.style=1
!y.style=1
plot,[0,nx,nx,0,0],[jj1,jj1,jj2,jj2,jj1]
diff_image=(imagea(*,jj1:jj2)-imageb(*,jj1:jj2))
diff_med=median(diff_image)
z1	=diff_med-1000
z2	=diff_med+1000
tv,bytscl(diff_image,min=z1,max=z2)
oplot,crpix1+rpix1*cos(phi),crpix2+rpix2*sin(phi),linestyle=2
oplot,[0,nx,nx,0,0],[jj1,jj1,jj2,jj2,jj1]

;coalignment in x-direction 
dx_	=fltarr(3)
dy_	=fltarr(3)
for ibox=0,2 do begin
nshift	=10*(1+ibox)
nshift2	=100+50*(1+ibox)
zdiff	=fltarr(nshift*2+1,4)
i1	=crpix1-rpix1-nshift
i2	=crpix1-rpix1+nshift
i3	=crpix1+rpix1-nshift
i4	=crpix1+rpix1+nshift
i5	=crpix1-nshift2
i6	=crpix1+nshift2
j1	=crpix2-rpix1-nshift
j2	=crpix2-rpix1+nshift
j3	=crpix2+rpix1-nshift
j4	=crpix2+rpix1+nshift
j5	=crpix2-nshift2
j6	=crpix2+nshift2
oplot,[i1,i2,i2,i1,i1],[j5,j5,j6,j6,j5]
oplot,[i3,i4,i4,i3,i3],[j5,j5,j6,j6,j5]
oplot,[i5,i6,i6,i5,i5],[j1,j1,j2,j2,j1]
oplot,[i5,i6,i6,i5,i5],[j3,j3,j4,j4,j3]
for i=-nshift,nshift do begin
 ii	=i+nshift
 zdiff(ii,0)=avg(abs(imagea(i1+i:i2+i,j5:j6)-imageb(i1:i2,j5:j6)))
 zdiff(ii,1)=avg(abs(imagea(i3+i:i4+i,j5:j6)-imageb(i3:i4,j5:j6)))
 zdiff(ii,2)=avg(abs(imagea(i5:i6,j1+i:j2+i)-imageb(i5:i6,j1:j2)))
 zdiff(ii,3)=avg(abs(imagea(i5:i6,j3+i:j4+i)-imageb(i5:i6,j3:j4)))
endfor 
x	=findgen(nshift*2+1)-nshift
zx	=zdiff(*,0)+zdiff(*,1)
zy	=zdiff(*,2)+zdiff(*,3)
zmin1	=min(zx,imin)
zmin2	=min(zy,jmin)
ix      =imin-nshift
iy      =jmin-nshift
imin	=(imin>1)<(nshift*2-1)
jmin	=(jmin>1)<(nshift*2-1)
cx	=poly_fit(x(imin-1:imin+1),zx(imin-1:imin+1),2)
cy	=poly_fit(x(imin-1:imin+1),zy(imin-1:imin+1),2)
dx	=-cx(1)/(2.*cx(2))
dy	=-cy(1)/(2.*cy(2))
print,'Coalignment shift in pixels(integer) : dx,dy= ',ix,iy
print,'Coalignment shift in pixels(poly_fit) : dx,dy= ',dx,dy
dx_(ibox)=dx
dy_(ibox)=dy
endfor
statistic,dx_,dx_avg,dx_sig
statistic,dy_,dy_avg,dy_sig
dx	=[dx_avg,dx_sig]
dy	=[dy_avg,dy_sig]
print,'shift in x = ',dx(0),'+',dx(1)
print,'shift in y = ',dy(0),'+',dy(1)

;shifting image B
x	=findgen(nx)
y	=findgen(ny)
for j=0,ny-1 do imageb(*,j)=interpol(imageb(*,j),x,x-dx(0))
for i=0,nx-1 do imageb(i,*)=interpol(imageb(i,*),y,y-dy(0))
coaligned_pair=image_pair
coaligned_pair(1,*,*)=long(imageb)

;display coaligned (shifted) image
imagea	=float(reform(coaligned_pair(0,*,*)))
imageb	=float(reform(coaligned_pair(1,*,*)))
diff_image=(imagea(*,jj1:jj2)-imageb(*,jj1:jj2))
tv,bytscl(diff_image,min=z1,max=z2)
oplot,crpix1+rpix1*cos(phi),crpix2+rpix2*sin(phi),linestyle=2
oplot,[0,nx,nx,0,0],[jj1,jj1,jj2,jj2,jj1]

;roll Sun
d_roll	 =0.1*roll_max
n_roll   =long(2.*roll_max/d_roll+1)
diff_roll=fltarr(n_roll)
deg_roll =-roll_max+d_roll*findgen(n_roll)
for iroll=0,n_roll-1 do begin
 a_roll	   =deg_roll(iroll)*!pi/180.
 image_roll=imageb
 for j=0,ny-1 do begin
  ind	=where(sqrt((x-crpix1)^2+(y(j)-crpix2)^2) lt rpix1,nind)
  if (nind ge 1) then begin
   jnd	=fltarr(nind)+y(j)
   i_roll=long(crpix1+(ind-crpix1)*cos(a_roll)-(jnd-crpix2)*sin(a_roll)+0.5)
   j_roll=long(crpix2+(ind-crpix1)*sin(a_roll)+(jnd-crpix2)*cos(a_roll)+0.5)
   image_roll(ind,j)=imageb(i_roll,j_roll)
  endif
 endfor

 ;rotate by STEREO separation angle
 sep_rad	=sep_deg*!pi/180.
 image_rot	=image_roll
 for j=0,ny-1 do begin
  ind	=where(sqrt((x-crpix1)^2+(y(j)-crpix2)^2) lt rpix1,nind)
  if (nind ge 1) then begin
   lat	=asin((y(j)-crpix2)/rpix2)
   r_lat	=rpix2*cos(lat)
   long	=acos((ind-crpix1)/r_lat)
   ind_rot=long(crpix1+r_lat*cos(long+sep_rad)+0.5)
   image_rot(ind,j)=image_roll(ind_rot,j)
  endif
 endfor

 diff_image=(imagea(*,jj1:jj2)-image_rot(*,jj1:jj2))
 tv,bytscl(diff_image,min=z1,max=z2)
 diff_roll(iroll)=avg(abs(diff_image)) 
 print,'Roll angle : ',deg_roll(iroll),diff_roll(iroll)
endfor

;interpolate rollangle
dmin	=min(diff_roll,imin)
c	=poly_fit(deg_roll(imin-1:imin+1),diff_roll(imin-1:imin+1),2)
roll0	=-c(1)/(2.*c(2))
roll	=[roll0,d_roll/2.]
print,'Interpolated roll angle = ',roll(0),'+',roll(1)
a_roll	   =roll0*!pi/180.
image_roll=imageb
for j=0,ny-1 do begin
 ind	=findgen(nx)
 jnd	=fltarr(nx)+y(j)
 i_roll=long(crpix1+(ind-crpix1)*cos(a_roll)-(jnd-crpix2)*sin(a_roll)+0.5)
 j_roll=long(crpix2+(ind-crpix1)*sin(a_roll)+(jnd-crpix2)*cos(a_roll)+0.5)
 image_roll(ind,j)=imageb(i_roll,j_roll)
endfor
image_rot =image_roll
for j=0,ny-1 do begin
 ind	=where(sqrt((x-crpix1)^2+(y(j)-crpix2)^2) lt rpix1,nind)
 if (nind ge 1) then begin
  lat	=asin((y(j)-crpix2)/rpix2)
  r_lat	=rpix2*cos(lat)
  long	=acos((ind-crpix1)/r_lat)
  ind_rot=long(crpix1+r_lat*cos(long+sep_rad)+0.5)
  image_rot(ind,j)=image_roll(ind_rot,j)
 endif
endfor
diff_image=(imagea(*,jj1:jj2)-image_rot(*,jj1:jj2))
tv,bytscl(diff_image,min=z1,max=z2)

;store coaligned image
coaligned_pair(1,*,*)=long(image_roll(*,*))
end
