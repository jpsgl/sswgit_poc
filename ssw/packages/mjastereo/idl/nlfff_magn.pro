pro nlfff_magn,dir,savefile,test
;+
; Project     : SDO, STEREO, SOHO
;
; Name        : MAGN_MDIDATA
;
; Category    : magnetic field moedling
;
; Explanation : reads MDI magnetogram data in chosen field-of-view (FOV)
;		reads and selects loop file (3D coordinates, x,y,z), 
;               and stores parameters in savefile
;
; Syntax      : IDL>nlfff_input,dir,savefile,test
;
; Inputs      : dir,savefile,test
;
; Outputs     ; saves parameters in file 
;
; History     : 27-Feb-2014, Version 1 written by Markus J. Aschwanden
;		29-Jan-2015: iwrange=20  ;pixels
;		10-Dec-2015: magnetic separation
;		13-Jan-2016: redefine energy corr: qe_rebin,qe_corr 
;
; Contact     : aschwanden@lmsal.com
;-

print,'__________________NLFFF_MAGN__________________________
restore,dir+savefile    ;--> PARA
filename=strmid(savefile,0,15)
x1_sun  =para.x1_sun
x2_sun  =para.x2_sun
y1_sun  =para.y1_sun
y2_sun  =para.y2_sun
qe_rebin=para.qe_rebin
qe_model=para.qe_model
nmag    =input.nmag_p	;number of magnetic charges
magwid  =1		;half width magnetic Gaussian
iwrange	=20		;pixels of depth range
nrebin0 =3  		;rebinning magnetogram

;________________READ MAGNETIC DATA________________________
magfile =dir+filename+'_HMI.fits'
image   =readfits(magfile,index_mag,/silent)
dateobs =sxpar(index_mag,'DATE_OBS')
datamin =sxpar(index_mag,'DATAMIN')
datamax =sxpar(index_mag,'DATAMAX')
nx      =sxpar(index_mag,'NAXIS1')
ny      =sxpar(index_mag,'NAXIS2')
crpix1  =sxpar(index_mag,'CRPIX1')
crpix2  =sxpar(index_mag,'CRPIX2')
cdelt1  =sxpar(index_mag,'CDELT1')
cdelt2  =sxpar(index_mag,'CDELT2')
pos0	=sxpar(index_mag,'SOLAR_P0')
rpix	=sxpar(index_mag,'R_SUN')
if (pos0 eq 0.) then pos0=sxpar(index_mag,'CROTA2')
if (rpix eq 0.) then rpix=sxpar(index_mag,'RSUN_OBS')/cdelt1
dpix_mag=1./rpix			;pixel size in solar radii

;________________EXTRACT MAGNETOGRAM_________________________
p0      =pos0*!pi/180.
i1      =long(crpix1+x1_sun/dpix_mag) < (nx-1)
i2      =long(crpix1+x2_sun/dpix_mag) < (nx-1)
j1      =long(crpix2+y1_sun/dpix_mag) < (ny-1)
j2      =long(crpix2+y2_sun/dpix_mag) < (ny-1)
ni	=(i2-i1+1)
nj	=(j2-j1+1)
x	=x1_sun+dpix_mag*findgen(ni)
y	=y1_sun+dpix_mag*findgen(nj)
bzmap	=image(i1:i2,j1:j2)

;________________ROTATION OF MDI IMAGE______________________
if (p0 ne 0.) then begin
 zmin	=min(bzmap)
 for j=0,nj-1 do begin
  y_     =fltarr(ny)+y(j)
  x_rot  = x*cos(p0)+y_*sin(p0)
  y_rot  =-x*sin(p0)+y_*cos(p0)
  i_     =long(rpix*x_rot+crpix1) 
  j_     =long(rpix*y_rot+crpix2) 
  stripe =image(i_,j_)
  ind0   =where(stripe eq zmin,nind0)
  if (nind0 ge 1) then stripe(ind0)=0.
  r	 =sqrt(x^2+y(j)^2)
  indr	 =where(r ge 0.999,nindr)
  if (nindr ge 1) then stripe(indr)=0.
  bzmap(*,j)=stripe
 endfor
endif

;________________REBIN MAP____________________________________
nbin	=long(nrebin0+0.5)>1
nxx	=ni
nyy	=nj
bzfull  =float(bzmap)			;full resolution map
bz2full	=float(bzfull)^2		;full resolution magnetic energy 
xfull	=x
yfull	=y
if (nbin eq 1) then begin
 x	=x1_sun+dpix_mag*findgen(nxx)
 y	=y1_sun+dpix_mag*findgen(nyy)
endif
if (nbin ge 2) then begin
 nxx	=(ni/nbin)
 nyy	=(nj/nbin)
 bzmap  =rebin(bzfull(0:nxx*nbin-1,0:nyy*nbin-1),nxx,nyy)
 bz2map =rebin(bz2full(0:nxx*nbin-1,0:nyy*nbin-1),nxx,nyy) ;energy = B^2
 x	=rebin(xfull[0:nxx*nbin-1],nxx) 
 y	=rebin(yfull[0:nyy*nbin-1],nyy) 
endif
dim	=size(bzmap)
nx	=dim(1)
ny	=dim(2)

;__________________MAGNETIC ENERGY CORRECTION DUE TO REBINNING__________' 
qe_rebin=total(bzmap^2)/total(bz2map) 	;rebinning energy factor 

;__________________DCOMPOSITION OF MAGN SOURCES_________________________'
;print,'  IS       IPOL     M       X       Y       Z      -H '
coeff	=fltarr(nmag,5)
ipix	=lonarr(6,nmag)
magn	=max(abs(bzmap))		;max field strength
bz_obs	=fltarr(nmag)
ix_obs	=fltarr(nmag)
iy_obs	=fltarr(nmag)
eps	=1.e-6
icomp	=0
qm	=0.5				;RUN4
qm	=0.25				;RUN5
wrange	=dpix_mag*iwrange		;solar radii
wmin	=dpix_mag*0.5			;solar radii
nw	=100
image_res=float(bzmap)			
mask	=lonarr(nx,ny)
for is=0,nmag-1 do begin
 bzabs	=max(abs(image_res),im)
 iy	=long(im/long(nx))	
 ix	=im-iy*long(nx)
 bz	=image_res(ix,iy)
 bz_obs(icomp)=bz
 ix_obs(icomp)=ix
 iy_obs(icomp)=iy
 xprof  =image_res(*,iy)
 yprof  =reform(image_res(ix,*))
 i3	=(ix-magwid)>0
 i4	=(ix+magwid)<(nx-1)
 j3	=(iy-magwid)>0
 j4	=(iy+magwid)<(ny-1)
 nx_model=i4-i3+1			;local map
 ny_model=j4-j3+1
 n_model =nx_model*ny_model
 ic_model=ix-i3				;local map center 
 jc_model=iy-j3
 ipix[*,is]=[i3,j3,i4,j4,ix,iy]
 x_model=x[i3:i4]
 y_model=y[j3:j4]
 image_local=image_res[i3:i4,j3:j4]
 mask[i3:i4,j3:j4]=1			;mask of magnetic coverage
 xp	=x[ix]  	
 yp	=y[iy] 		
 zp	=sqrt((1.-xp^2-yp^2) > eps)
 am	=0.
 for iw=0,nw-1 do begin
  wp	=wmin+(wrange-wmin)*(float(iw)/float(nw-1))^2 
  aia_mag_inversion,bz,xp,yp,zp,wp,bm,xm,ym,zm
  coeff_local=[[bm],[xm],[ym],[zm],[am]]
  aia_mag_map,coeff_local,1.,x_model,y_model,model_local
  dev	=abs(total(image_local-model_local))
  if (iw eq 0) then devmin=dev
  rm	=sqrt(xm^2+ym^2+zm^2)
  if (dev le devmin) and (rm lt 0.999) then begin
   devmin=dev
   coeff(icomp,0)=bm				
   coeff(icomp,1)=xm
   coeff(icomp,2)=ym
   coeff(icomp,3)=zm
;TEST DISPLAY______________________________________
   if (test ge 2) then begin
    dim=size(model_local)
    c2	=max(abs(image_local))
    c1	=-c2
    diff_image=model_local-image_local
    tv,bytscl(rebin(image_local,dim(1)*16,dim(2)*16),min=c1,max=c2)
    tv,bytscl(rebin(model_local,dim(1)*16,dim(2)*16),min=c1,max=c2),dim(1)*16.,0
    tv,bytscl(rebin( diff_image,dim(1)*16,dim(2)*16),min=c1,max=c2),dim(1)*32.,0
    print,'iw,wp,dev=',iw,wp/dpix_mag,dev
    erase
    clearplot
    !p.position=[0.1,0.1,0.9,0.45]
    plot,image_local(*,iy-j3),thick=3
    oplot,model_local(*,iy-j3)
    !noeras=1
    !p.position=[0.1,0.55,0.9,0.9]
    plot,image_local(ix-i3,*),thick=3
    oplot,model_local(ix-i3,*)
   endif
;___________________________________________________
  endif
 endfor
 if (test eq 3) then begin
  read,'continue?',yes
  if (yes eq 0) then test=1
 endif
 aia_mag_map,coeff[icomp,*],1,x,y,model_map
 image_res=image_res-model_map 
;________________________erase core of structure_______________________
 ind	=where(abs(model_map) ge 0.5*max(abs(model_map)))
 image_res(ind)=0.
;______________________________________________________________________
 icomp	=icomp+1
END_DECOMP:
endfor
nmag	=icomp
coeff	=coeff[0:nmag-1,*]

;________________MAGNETIC ENERGY NORMALIZATION______________________
aia_mag_map,coeff,1.,x,y,bzmodel	  ;---> B map
qe_model=total(bzmodel^2)/total(bzmap^2) ;magnetic energy of model

print,'Magnetic field minmax(bzmap)= ',minmax(bzmap)
print,'Number of magnetic sources  = ',nmag
print,'Maximum magnetic field bzmap= ',magn
print,'Maximum magnetic field coeff= ',max(abs(coeff[*,0]))
print,'Magnetic energy corr rebin  = ',qe_rebin
print,'Magnetic energy corr model  = ',qe_model

;______________DISPLAY OF HMI FOV IMAGE_____________________________
if (test ge 1) then begin
 if (nx le 256) then zoom=2
 if (nx gt 256) then zoom=1
 loadct,3,/silent
 window,1,xsize=nx*zoom*2,ysize=ny*zoom*2
 diff_image=bzmap-bzmodel
 diff_image0=diff_image
 tv,bytscl(rebin(bzmap,nx*zoom,ny*zoom) ,min=-magn,max=+magn),0,ny*zoom
 tv,bytscl(rebin(bzmodel,nx*zoom,ny*zoom),min=-magn,max=+magn),0,0

 !p.position=[0.5,0.5,1,1]
 !x.range=minmax(x)
 !y.range=minmax(y)
 !x.style=5
 !y.style=5
 plot,[0,0],[0,0]
 tv,bytscl(rebin(diff_image0,nx*zoom,ny*zoom),min=-magn,max=+magn),nx*zoom,ny*zoom
 if (nmag le 10) then begin
 for is=0,nmag-1 do begin
  i3	=x[ipix[0,is]]
  j3	=y[ipix[1,is]]
  i4	=x[ipix[2,is]]
  j4	=y[ipix[3,is]]
  oplot,[i3,i4,i4,i3,i3],[j3,j3,j4,j4,j3],color=255
  xyouts,i3,j4,string(is,'(I3)'),color=255
 endfor
 endif
 !noeras=1

 !p.position=[0.55,0.05,0.95,0.45]
 !p.title='East-West scan'
 !x.style=1
 !y.style=0
 !x.range=minmax(x)
 !y.range=magn*[-1,1]
 is	=0
 i3	=ipix[0,is]
 i4	=ipix[2,is]
 ix	=ipix[4,is]
 iy	=ipix[5,is]
 plot,!x.range,[0,0],linestyle=1
 oplot,x,bzmodel[*,iy],thick=2
 oplot,x[i3:i4],bzmodel[i3:i4,iy],thick=5
 oplot ,x,bzmap[*,iy],color=150,thick=2
endif

;________________SAVE PARAMETERS______________________________
para.dpix_mag=dpix_mag
para.qe_rebin=qe_rebin
para.qe_model=qe_model
save,filename=dir+savefile,input,para,bzmap,bzfull,bzmodel,coeff
print,'parameters saved in file = ',savefile
end
