pro looptracing_auto,image1,fov,para,cutoff,image2,loopfile,test
;+
; Project     : SOHO, TRACE, STEREO, HINODE, SDO
;
; Name        : LOOPTRACING_AUTO
;
; Category    : Automated 2D Pattern Recognition
;
; Explanation : An input image (IMAGE1) is highpass-filtered (IMAGE2)
;		and automatically traced for curvi-linear structures,
;		whose [x,y] coordinates in solar radii are stored in LOOPFILE.
;		The algorithm iteratively starts to trace a loop from
;		the position of the absolute flux maximum in the image,
;		tracks in a bi-directional way by oriented directivity,
;		based on the direction of the ridge with maximum flux.
;
; Syntax      : IDL> looptracing_auto,image1,image2,loopfile,para,text
;
; Inputs      : image1   - input image 
;               fov[4]   - [x1,y1,x2,y2] field-of-view of image in solar radii
;               para     - [rmin,wid,nsig,qfill,nstruc]
;			   contains algorithm control parameters, such as:
;                            rmin   = minimum curvature radius [pixels]
;			     wid    = half width of loop [pixels]
;			     nsig   = significance level of flux threshold 
;			     reso   = resolution of output loop coord [pixels]
;			     qfill  = minimum filling ratio 
;			     nstruc = maximum number of structures
;	        cutoff   - minimum loop length [in solar radii]
;               test     - option for display of traced structures
;
; Outputs     : image2	 - output image of highpass-filtered image
;               loopfile - output file containing loop coordinates 
;			   in units of solar radii, 
;                          with consecutive loop numbering
;
; History     : 22-Oct-2009, Version 1 written by Markus J. Aschwanden
;               26-Oct-2009, modification of input and control parameters 
;               16-May-2009, loop coordinates from pixels into solar radii
;               16-May-2009, CUTOFF from pixels into solar radii
;
; Contact     : aschwanden@sag.lmsal.com
;-


;CONTROL PARAMETERS________________________________________________________
rad	=180./!pi
rmin	=para(0)                	&print,'min. curvature radius = ',rmin
wid 	=para(1)			&print,'loop half width       = ',wid
nsig 	=para(2)			&print,'significance level    = ',nsig
qfill	=para(3)			&print,'minimum filling factor= ',qfill
reso	=para(4)		        &print,'ouput resolution      = ',reso
nstruc	=para(5)			&print,'max. structures       = ',nstruc
nlen	=long(sqrt(8*wid*rmin))		&print,'tracing length        = ',nlen
step	=1                              &print,'tracing step          = ',step
nsm	=long(3+2*wid)			&print,'higphass smoothing    = ',nsm
da	=asin(sqrt(2.*wid/rmin)<1.)/2.	&print,'tracing half angle    = ',da*rad
npmax	=1000                           
r1	=long(step/2-nlen/2)
r	=r1+findgen(nlen)
if (n_params(0) le 5) then cutoff=nlen

;HIGHPASS FILTER AND THRESHOLD_____________________________________________
image1	=float(image1)
image0	=image1
if (nsm ge 5) then image0=smooth(image1,3)
image2  =image0-smooth(image0,nsm) 
dim	=size(image1)
nx	=dim(1)
ny	=dim(2)
dpix	=(fov(2)-fov(0))/float(nx)	;solar radii per pixel
i1_fov	=fov(0)/dpix			;pixels from Sun center 
j1_fov	=fov(1)/dpix			;pixels from Sun center
cutoff_pix=cutoff/dpix
statistic,image2
statistic,image2,avg,dev_sigma
thresh0	=avg+nsig*dev_sigma
openw,2,loopfile
close,2

;LOOP TRACING START AT MAXIMUM FLUX POSITION_______________________________
iloop=0
iloop_last=-1
residual=image2		
for istruc=0,nstruc-1 do begin
 zstart =max(residual,im) 
 if (istruc mod 100) eq 0 then begin
  ipix	=where(residual ge thresh0,npix)
  qpix	=float(npix)/(float(nx)*float(ny))
  print,'Struc#'+string(istruc,'(I7)')+'  Loop#'+string(iloop,'(I5)')+$
        '  Flux/Thresh='+string(zstart/thresh0,'(f6.1)')+$
        '  Area='+string(qpix,'(f6.4)') 
  if (iloop eq iloop_last) then goto,end_trace
  iloop_last=iloop
 endif
 jstart	=long(im/nx)
 istart	=long(im mod nx)

;TRACING LOOP STRUCTURE STEPWISE_____________________________________________
 da1	=da
 da2	=da
 turn	=0
 for idir=0,1 do begin
  xl	=fltarr(npmax)
  yl	=fltarr(npmax)
  zl	=fltarr(npmax)
  al	=fltarr(npmax)
  xl(0) =istart
  yl(0) =jstart
  zl(0) =zstart

;DIRECTION FINDING___________________________________________________________
  for ip=0,npmax do begin          		;maximum loop length
   if (ip eq 0) and (idir eq 0) then begin &a1=0. &a2=!pi &endif
   if (ip eq 0) and (idir eq 1) then begin &a1=alpha0-da1 &a2=alpha0+da2 &endif
   if (ip ge 1) then begin &a1=al(ip-1)-da1 &a2=al(ip-1)+da2 &endif
   nphi	=long((a2-a1)*(180/!pi))
   phi	=a1+(a2-a1)*findgen(nphi)/float(nphi)
   flux_phi=fltarr(nphi)
   peak_phi=fltarr(nphi)
   for i=0,nphi-1 do begin
    x_	=xl(ip)+r*cos(phi(i))
    y_	=yl(ip)+r*sin(phi(i))
    flux=residual(long(x_+0.5),long(y_+0.5))
    flux_phi(i)=total(flux)/nlen
    peak_phi(i)=max(flux)
   endfor
   flux_avg =max(flux_phi,im) >0.
   flux_peak=peak_phi(im) >0.
   qf       =flux_avg/flux_peak
   alpha    =phi(im)

   if (ip ge 1) then begin 				   ;180-deg ambuiguity
    alpha_	=alpha+[0.,-!pi,!pi]
    dalpha	=min(abs(alpha_-al(ip-1)),im)
    alpha	=alpha_(im)
   endif
   al(ip)=alpha
   if (idir eq 0) and (ip eq 0) then alpha0=alpha	   ;save initial dir

;NEXT LOOP POSITION ALONG RIDGE DIRECTION__________________________
   xl(ip+1)=xl(ip)+step*cos(alpha+!pi*idir)         	   ;new position
   yl(ip+1)=yl(ip)+step*sin(alpha+!pi*idir)
   zl(ip+1)=flux_avg

;END LOOP CRITERION________________________________________________
   if (qf lt qfill) then goto,endloop
  endfor		;for ip=0,npmax
  endloop:

;RE-ORDERING LOOP COORDINATES______________________________________ 
  if (idir eq 0) then begin			;first half of loop
   xloop	=reverse(xl(0:ip))
   yloop	=reverse(yl(0:ip))
   zloop	=reverse(zl(0:ip))
  endif
  if (idir eq 1) and (ip ge 1) then begin	;second half of loop 
   xloop	=[xloop,xl(1:ip)]
   yloop	=[yloop,yl(1:ip)]
   zloop	=[zloop,zl(1:ip)]
  endif
 endfor

;REMOVE HORIZONTAL OR VERTICAL ARTIFACTS______________________________
 statistic,xloop,xav,xdev
 statistic,yloop,yav,ydev
 if (xdev lt 1) then looplen=0    ;vertical column artifact
 if (ydev lt 1) then looplen=0    ;horizontal row  artifact

;LOOP COMPLETED - LOOP LENGTH_________________________________________
 np	=n_elements(xloop)
 s	=fltarr(np)			;loop length coordinate
 looplen=0
 if (np ge 2) then for ip=1,np-1 do s(ip)=s(ip-1)+$
   sqrt((xloop(ip)-xloop(ip-1))^2+(yloop(ip)-yloop(ip-1))^2)
 looplen=s(np-1) 			;number of pixels for full loop length 
;ns	=long(looplen)>1
 ns	=long(looplen)>3		;modification 2011
 ss	=findgen(ns)

;STORE LOOP COORDINATES_______________________________________________
 if (looplen ge cutoff_pix) then begin
  nn	=long(ns/reso+0.5)
  ii	=findgen(nn)*reso
  xx	=interpol(xloop,s,ii)				;interpolate
  yy	=interpol(yloop,s,ii)
  ff	=interpol(zloop,s,ii)				;flux average
  x_rsun=(xx+i1_fov)*dpix
  y_rsun=(yy+j1_fov)*dpix
  s_rsun=ii*dpix						;loop length
  openw,2,loopfile,/append
  for ip=0,nn-1 do printf,2,iloop,x_rsun(ip),y_rsun(ip),ff(ip),s_rsun(ip)
  close,2
  iloop=iloop+1
  if (zstart lt thresh0) then goto,end_trace
 endif				  ;if (looplen ge cutoff_pix) then begin

;TEST DISPLAY_________________________________________________________
if (istruc ge test) and (looplen ge cutoff_pix) then begin
 x0     =(max(xloop)+min(xloop))/2.
 y0     =(max(yloop)+min(yloop))/2.
 dx     =(max(xloop)-min(xloop))/2.
 dy     =(max(yloop)-min(yloop))/2.
 dd     =(dx>dy)>(step/2)
 i1     =long(x0-dd*1.5)>0
 i2     =long(x0+dd*1.5)<(nx-1)
 j1     =long(y0-dd*1.5)>0
 j2     =long(y0+dd*1.5)<(ny-1)
 subimage=residual(i1:i2,j1:j2)
 z0     =max(subimage)>1.
 xfov   =i1+findgen(i2-i1+1)
 yfov   =j1+findgen(j2-j1+1)
 window,0,xsize=1024,ysize=1024
 clearplot
 loadct,3
 !x.range=[i1,i2]
 !y.range=[j1,j2]
 !x.style=1
 !y.style=1
 !p.title='Structure #'+string(istruc,'(i4)')
 plot,[i1,i2],[j1,j2]
 contour,subimage,xfov,yfov,level=thresh0/2.+(zstart-thresh0/2.)*findgen(11)/10.
 oplot,istart*[1,1],jstart*[1,1],psym=4,symsize=2,color=128,thick=2
 oplot,xloop,yloop,thick=3,color=128
 oplot,xloop,yloop,thick=3,color=128,psym=1,symsize=2
 for ip=0,np-1 do xyouts,xloop(ip),yloop(ip),string(ip,'(i3)'),size=1.5,color=200
 read,'continue? [0,1] : ',yes
 if (yes eq 0) then stop
endif

;ERASE LOOP IN RESIDUAL IMAGE____________________________________________
 i3	=(istart-wid)>0
 i4	=(istart+wid)<(nx-1)
 j3	=(jstart-wid)>0
 j4	=(jstart+wid)<(ny-1)
 residual(i3:i4,j3:j4)=0.		;in case of no valid loop
 nn	=n_elements(xloop)
 if (nn ge 3) then begin
  xx	=interpol(xloop,s,ss)				;interpolate
  yy	=interpol(yloop,s,ss)
  nw	=(wid*2+1)
  array_curve,residual,xx,yy,nw,xw,yw,zw 
  for is=0,ns-1 do begin
   for iw=0,nw-1 do begin
    i_	=(long(xw(is,iw)+0.5)<(nx-1))>0 
    j_	=(long(yw(is,iw)+0.5)<(ny-1))>0 
    residual(i_,j_)=0.
   endfor
  endfor
 endif

endfor	;for istruc=0,nstruc-1 do begin
end_trace:
end
