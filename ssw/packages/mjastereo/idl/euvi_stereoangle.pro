pro euvi_stereoangle,date,sep2

datafile	='~/EUVI/stereo_angle.dat'
readcol,'~/EUVI/stereo_angle.dat',year,month,day,sep_b,sep_a
time	=year+(month-1)/12.+(day-1)/365.24
yy	=strmid(date,0,4)
mm	=strmid(date,4,2)
dd	=strmid(date,6,2)
time2	=yy+(mm-1)/12.+(dd-1)/365.24	
sep_a2	=interpol(sep_a,time,time2)
sep_b2	=interpol(sep_b,time,time2)
sep2	=[sep_a2,-sep_b2]
;print,'EUVI_STEREOANGLE '+date+' A,B=',sep2
end




