;_______RUN SPECIFICATION_________________________________
 iflare =592		;1,...,nev = flare ID number (from any catalog)
 it 	=1              ;1,...,nt = time step (nt=duration/cadence)
 cube	=0		;0=no B-cube    1=B-cube  
 test	=0  ;1		;0=none, 1=test, 2=detailed 
 io	=0		;0=screen, 1=ps, 2=eps, 3=col.ps
 run	='runA'	  	;name of run	
 dir	='~/work_nlfff_tutorial/'  ;work directory name	
 catfile='event_tutorial_'+run+'.txt'  ;catalog file name
;goto,DISP 		;shortcut to display results (from previous run)
;goto,DISP2 		;shortcut to display field lines 

;_______INPUT PARAMETERS__________________________________
 input={catfile:catfile,$                       ;catalog file name
    	tstart:'2014-03-29T17:05:00',$		;start time (UT)
    	cadence:360.,$				;time cadence (s)
    	duration:4500.,$				;total time duration (s)
    	noaa:'12017',$ 				;active region label
    	helpos:'N10W33',$			;heliogrpahic position
    	instr:'AIA',$				;instrument
    	wave8:[94,131,171,193,211,335,0,0],$	;wavelengths (A)
    	fov0:0.10,$				;field-of-view (solar radii)
    	nsm1:1,$				;hiphass filter (pixel)
    	nmag_p:30,$				;number of potential sources
    	nmag_np:30,$				;number of nonpotential sources
    	amis:20,$				;misalignment angle limit (deg)
    	nitmin:40,$				;min number of iterations
    	nstruc:1000,$				;structures per wavelength
    	prox_min:10.0,$				;proximity loop to magn sources
    	lmin_wid:5.0,$				;min loop length (pixels)
    	rmin_wid:8.0,$				;min curv radius (pixels)
    	qthresh1:0,$				;threshold flux (medians)
    	qthresh2:0}				;threshold filter (medians)

;_______TASKS______________________________________________
 nlfff_cat,    input,catfile,dir
 nlfff_init,   input,catfile,it,run,dir,savefile
 nlfff_hmi,    dir,savefile,test
 nlfff_aia,    dir,savefile,test,wave_ 
 nlfff_iris,   dir,savefile,test 
 nlfff_fov,    dir,savefile,test
 nlfff_magn,   dir,savefile,test
 nlfff_tracing,dir,savefile,test
 nlfff_fit,    dir,savefile,test
 nlfff_field,  dir,savefile,test
 nlfff_cube,   dir,savefile,test,cube
 nlfff_merit,  dir,savefile,test
 nlfff_energy, dir,savefile,test

;________DISPLAY RESULTS___________________________________
DISP:
 nlfff_disp,   dir,catfile,iflare,it,run,io
STOP

;________DISPLAY FIELD LINES_______________________________
DISP2:
 savefile='20140329_170500_AIA_001_runA.sav'
 bmin	=10   ;G
 ngrid	=20
 io	=0
 nlfff_field2,dir,savefile,bmin,ngrid,io
STOP
;___________________________________________________________
end

