pro euvi_imagefilter,filter,nsm,image0,image1,gamma,inv

;eliminate spikes
zmin	=min(smooth(image0,11))
zmax	=max(smooth(image0,11))
image0	=(image0>zmin)<zmax

if (filter lt 1) or  (nsm lt 3) then image1=image0

if (filter ge 1) and (nsm ge 3) then begin
 image1=image0*0.
 for ism=3,nsm,2 do begin
  w_ism=(1.+(nsm-ism)/2.)^2
  image1=image1+w_ism*(image0-smooth(image0,ism))
 endfor
 image1=image1>0.
endif

z1      =min(image1)
z2      =max(image1)
image1   =z1+(z2-z1)*((image1-z1)/(z2-z1))^gamma
if (inv eq -1.) then image1=z2-image1
end

