pro looptracing_overlap,nx,ny,datfile0,datfile1,image_mask,q1,q2,q3

;creates a maskfile with overlapping loop trajectories
;from manually traced loops (DATFILE0) and automated detection (DATFILE1)

mask	=lonarr(nx,ny,2)
for iset=0,1 do begin
 if (iset eq 0) then readcol,datfile0,iloop_,xloop_,yloop_
 if (iset eq 1) then readcol,datfile1,iloop_,xloop_,yloop_,zloop_,sloop_
 iloop1  =min(iloop_)
 iloop2  =max(iloop_)
 nloop   =0
 for iloop=iloop1,iloop2 do begin
  ind    =where(iloop_ eq iloop,np)
  if (np ge 3) then begin
   iloop =iloop_(ind(0))
   xp    =xloop_(ind)  
   yp    =yloop_(ind)
   spline_p,xloop_(ind),yloop_(ind),x,y
   spline_p,x,y,xx,yy
   ns	 =n_elements(xx)
   for is=0,ns-1 do begin
    i0	=long(xx(is)+0.5)
    j0	=long(yy(is)+0.5)
    if (i0 ge 1) and (i0 le nx-2) and (j0 ge 1) and (j0 le ny-2) then begin
     for ii=i0-1,i0+1 do begin
      for jj=j0-1,j0+1 do begin
       mask(ii,jj,iset)=1+iset
      endfor
     endfor
    endif
   endfor
  endif
 endfor
endfor
image_mask=mask(*,*,0)+mask(*,*,1)
ind1	=where(mask(*,*,0) eq 1,n1)
ind2	=where(mask(*,*,1) eq 2,n2)
ind3	=where(image_mask(*,*)  eq 3,n3)
q1	=float(n2)/float(n1)
q2	=float(n3)/float(n1)
q3	=float(n3)/float(n2)
end
