pro secchi_euvi_flarecat, t0, t1, cat, search=search, $
    count=count, tcount=tcount,out_style=out_style, init=init

;+
;   Name: secchi_euvi_flarecat
;
;   Purpose: ssw wrapper for Markus's EUVI flare catalog
;	     for detailed description see:
;         Aschwanden,M.J., Wuelser, J.-P., Nitta, N. V., Lemen, J. R., 
;         Freeland, S., & Thompson, W. T. 2014, Solar Physics 289/3, 919-938
;         URL1="http://www.lmsal.com/~aschwand/eprints/2013_euvicatalog.pdf" 
;         "STEREO/Extreme Ultraviolet Imager (EUVI) Event Catalog 2006-2012"
;
;   Calling Sequence:
;      IDL> secchi_euvi_flarecat,CAT  ; full catalog if no times supplied 
;      -or-
;      IDL> secchi_euvi_flarecat,t0,t1,CAT ; catalog within user time range
;
;   Input Parameters:
;      t0,t1 - optional time range - default is full catalog
;
;   Output Parameters:
;      cat - the catalog (structure vector)
;
;   Keyword Parameters:
;      search - optional search array (-> struct_where.pro)
;      tcount - (outpu) - number of records in time range
;      count - (output) - number of recrods returned (=tcount before SEARCH)
;
;   History:
;      24-feb-2013 - S.L.Freeland
;      10-apr-2013 - S.L.Freeland - update URL & structure template to mach new catalog
;       4-jun-2013 - S.L.Freeland - doc header only 
;      28-jul-2014 - M.J.Aschwanden - implemented in SSW
;
;-
common secchi_euvi_flarecat_blk, fullcat, ctimes

caturl='http://www.lmsal.com/~aschwand/EUVI/euvi_events_catalog.txt
caturl='http://secchi.lmsal.com/EUVI/euvi_autodetection/euvi_events.txt'
init=keyword_set(init) or n_elements(fullcat) eq 0 
;template={date_obs:'',cadence:0,sc:'',number:0,wavelnth:0,fb:0,fp_fb:0,goes_class:'',goes_delay:0.,l0:0.,b0:0.,l_sc:0.,b_sc:0.,l:0,b:0.,r:0.}
template={date_obs:'',cadence:0,sc:'',number:0,wavelnth:0,fbl:0.,fp_fbl:0.,fb2_fbl:0.,fluence:0.,rise_time:0.,decay_time:0.,goes_delay:0.,goes_class:'',l0:0.,b0:0.,l_sc:0.,b_sc:0.,l:0,b:0.,r:0.}
if init then begin 
   sock_list,caturl,catlist
   catlist=strtrim(catlist,2)
   ss0=(where(strpos(catlist,'2006') eq 0))(0)
   cattable=catlist[ss0:*]
   cols=str2cols(cattable,' ',/trim)
   strtab2vect,cols,yy,mon,dd,hh,mm
   dates=yy+'/'+mon+'/'+dd+' ' +hh+':'+mm
   if n_elements(out_style) eq 0 then out_style='ccsds'
   adates=anytim(dates,out_style=out_style)
   nc=n_elements(cattable)
   fullcat=replicate(template,nc)
   fullcat.date_obs=adates
   ntags=n_tags(template)
if get_logenv('checkcat') ne '' then stop,'fullcat, cattable'
   for t=1,ntags-1 do begin
      temp=fullcat.(t)
      reads,reform(cols[t+4,*]),temp
      fullcat.(t)=temp
   endfor
endif

cat=fullcat
count=n_elements(cat)
tcount=count
if n_params() ge 3 then begin 
   sst=where(anytim(cat.date_obs) ge anytim(t0) and anytim(cat.date_obs) le anytim(t1),tcount)
   if tcount gt 0 then cat=cat[sst] else cat=-1
endif

if n_elements(search) gt 0 then begin 
   sss=struct_where(cat,search=search,count)
   if count gt 0 then cat=cat[sss] else cat=-1
endif

if n_params() eq 1 then t0=cat

if count eq 0 then box_message,'no records match'

return

end

