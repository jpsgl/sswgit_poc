pro aia_mag_bfieldline,nf,imid,xl,yl,zl,bx,by,bz,x0,y0,z0,ds,field

field	=fltarr(nf,3)
field[0,0]=xl[imid]
field[0,1]=yl[imid]
field[0,2]=zl[imid]
polar  =1.
for is=1,nf/2 do begin
 xa_   =reform(field[is-1,*])
 aia_mag_bvector,bx,by,bz,x0,y0,z0,ds,polar,xa_,xb_,ba_,boundary
 for j=0,2 do field[is,j]=xb_[j]
 if (boundary eq 1) then goto,first_footpoint
endfor
FIRST_FOOTPOINT:
nf1    =is < (nf-1)
field[0:nf1,0]=reverse(field[0:nf1,0])
field[0:nf1,1]=reverse(field[0:nf1,1])
field[0:nf1,2]=reverse(field[0:nf1,2])
polar  =-1.
for is=nf1+1,nf-1 do begin
 xa_   =reform(field[is-1,*])
 aia_mag_bvector,bx,by,bz,x0,y0,z0,ds,polar,xa_,xb_,ba_,boundary
 for j=0,2 do field[is,j]=xb_[j]
 if (boundary eq 1) then goto,end_fieldline
endfor
END_FIELDLINE:
nfl    =is < (nf-1)
end

