pro nlfff_trace,dir,filename,wave
;+
; Project     : SDO, STEREO, SOHO, AIA
;
; Name        : NLFFF_AIA
;
; Category    : read TRACE images 
;
; Explanation : reads TRACE image from archive at LMSAL 
;               and write uncompressed FITS file
;
; Syntax      : IDL>nlfff_trace,dir,filename,wave
;
; Inputs      : dir	 =directory 
;		filename =filename
;		wave     =wavelength, eg '171'
;
; Outputs     ; saves TRACE FITS file in dir
;
; History     : 27-May-2013, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

print,'__________NLFFF_AIA_________________________________
year	=strmid(filename,0,4)
month	=strmid(filename,4,2)
day	=strmid(filename,6,2)
hour	=strmid(filename,9,2)
minu	=strmid(filename,11,2)
sec	='00'
string0,2,long(minu)+1,minu2		;one hour later
string0,2,long(hour)+1,hour2		;one hour later
string0,2,long(minu)+1,minu2		;one minute later
string0,2,long(hour)+1,hour2		;one minute later
im	=long(month)-1
months	=['Jan','Feb','Mar','Apr','May','Jun',$
          'Jul','Aug','Sep','Oct','Nov','Dec']
tstart	=year+'-'+months(im)+'-'+day+' '+hour+':'+minu+':'+sec
;tend	=year+'-'+months(im)+'-'+day+' '+hour+':'+minu2+':'+sec
 tend	=year+'-'+months(im)+'-'+day+' '+hour2+':'+minu+':'+sec
print,'Start time = ',tstart
print,'End   time = ',tend

;READ DATA_______________________________________________
;trace_cat,start,tend,cat 
;print,cat
;trace_cat2data,cat(0),index,data
;help,index,data
;print,'Number of files found = ',n_elements(cat)
;help,index,data

parent='/archive1/trace/level1'   ; top of chronologically arranged archive
cat	=ssw_time2filelist(tstart,tend,parent=parent)
help,cat
more,cat
nfiles	=n_elements(cat)
if (cat eq '') then print,'No TRACE files found !'
for i=0,nfiles-1 do begin 
 search=wave			  ;print,search
 ipos=strpos(cat(i),search)       ;print,ipos
 if (ipos ge 0) then goto,wavelength_found
endfor
wavelength_found:
data	=readfits(cat(i),header)
help,image,header

;DISPLAY IMAGE___________________________________________
dim	=size(data(*,*,0))
nx	=dim(1)
ny	=dim(2)
window,0,xsize=nx,ysize=ny
loadct,0
statistic,data,zavg,zsig
tv,bytscl(data(*,*,0),min=0,max=3*zsig)

;FITS FILE________________________________________
euvfile=dir+filename+'_euv.fits' 
;mwritefits,index,data,outfile=euvfile		;write FITS file
;spawn,'cp '+cat(0)+' '+euvfile			;copy FITS FILE
 writefits,euvfile,data,header
print,'FITS file copied = ',euvfile
end
