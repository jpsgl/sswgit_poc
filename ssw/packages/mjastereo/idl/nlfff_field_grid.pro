pro nlfff_field_grid,files,vers,para,ngrid,bmin,alpha
;+
; Project     : SOHO/MDI, SDO/HMI, STEREO
;
; Name        : NLFFF_FIELD
;
; Category    : Magnetic data modeling
;
; Explanation : Calculating NF magnetic field lines FIELD_LINES[NS,5,NF] 
;		magnetic field coefficients [B1,X1,Y1,Z1,A1] 
;
; Syntax      : IDL>nlfff_field,runfile,para
;
; Inputs      : runfile   = input file *_coeff.dat
;               para      = input parameters
;
; Outputs     ; runfile   = output files *_fit.sav
;
; History     : 16-May-2011, Version 1 written by Markus J. Aschwanden
;               30-Jan-2012, Version 2 released to SSW
;               29-JAN-2015, im --> np/2 (footpoint --> midpoint)
;
; Contact     : aschwanden@lmsal.com
;-


print,'__________________NLFFF_FIELD_________________________________'
nsmax	=long(para[7])
nseg	=para[8]
hmax	=para[12]
ds      =para[13]
xf1	=para[17]
yf1	=para[18]
xf2	=para[19]
yf2	=para[20]

;______________________READ DATA_____________________________________
restore,files(3)   ;-->para,bzmap,bzmodel,qb_rebin,qb_model,nmag
dim     =size(bzmap)
nx      =dim(1)
ny      =dim(2)
dpix    =float(xf2-xf1)/float(nx-1)
xaxis   =xf1+dpix*findgen(nx)
yaxis   =yf1+dpix*findgen(ny)

readcol,files(4),m1,x1,y1,z1,a1,/silent
na	=n_elements(alpha)
for ia=0,na-1 do a1(ia)=alpha(ia)
coeff	=[[m1],[x1],[y1],[z1],[a1]]

;______________________FOOTPOINT GRID________________________________
nf	=ngrid*ngrid 
ix_grid =long(nx*(0.5+findgen(ngrid))/float(ngrid))
iy_grid =long(ny*(0.5+findgen(ngrid))/float(ngrid))
field_lines=fltarr(nsmax,3,nf)
ns_field=lonarr(nf)

;____________MAGNETIC FIELD LINES THROUGH LOOP MIDPOINT_________________
window,2,xsize=1024,ysize=1024
!p.position=[0.05,0.05,0.95,0.95]
!x.range=minmax(xaxis)
!y.range=minmax(yaxis)
!x.style=1
!y.style=1
plot,[1,1],[1,1]
k	=0
for j=0,ngrid-1 do begin
 for i=0,ngrid-1 do begin
  bfoot =bzmap(ix_grid(i),iy_grid(j))
  if (abs(bfoot) ge bmin) then begin
   print,j,i,k,bfoot,bmin
   xm	=xaxis(ix_grid(i))
   ym	=yaxis(iy_grid(j))
   zm	=sqrt((1.+ds)^2-xm^2-ym^2)
   nlfff_fieldline,xm,ym,zm,ds,hmax,coeff,field,np,nsmax
   np	=np < nsmax
   field_lines(0:np-1,0,k)=field(0:np-1,0)
   field_lines(0:np-1,1,k)=field(0:np-1,1)
   field_lines(0:np-1,2,k)=field(0:np-1,2)
   ns_field[k]=np
   oplot,field(0:np-1,0),field(0:np-1,1)
   k	=k+1
  endif
 endfor
endfor
nf	=k
print,'Number of field lines = ',nf
if (nf eq 0) then stop,'No field line with B_foot > B_min'
field=fltarr(nsmax,3,nf)
field(*,*,0:nf-1)=field_lines(*,*,0:nf-1)
ns_field=ns_field(0:nf-1)

;_______________________SAVE PARAMETERS________________________
save,filename=files(5),para,bzmap,xaxis,yaxis,ns_field,field
print,'parameters saved in file = ',files(0)
end
