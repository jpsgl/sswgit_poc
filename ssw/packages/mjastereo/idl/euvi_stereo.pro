pro euvi_stereo,image_pair,para,nsm,fov,poly,hmax,loopfile,filter,window,ct,wave,gamma,inv 
;+
; Project     : STEREO
;
; Name        : EUVI_STEREO 
;
; Category    : Data analysis  
;
; Explanation : This routine displays a selected FOV of a stereoscopic image pair.
;		The user traces manually a corresponding curvi-linear structure
;		in the two images and the routine performs the stereoscopic
;		height calculation and stores the (x,y,z) values in LOOPFILE. 
;
; Syntax      : IDL> euvi_stereo,image_pair,para,nsm,fov,poly,hmax,loopfile,filter,window,ct
;
; Inputs      : images_pair(2,*,*) = stereo image pair B,A
;		para    = index structure with image parameters 
;		nsm	= 3, or 5; smoothing parameter for highpass filter
;		fov	= [i1,j1,i2,j2] field-of-view in image pixels 
;		poly    = 2 ;0=raw heights; poly=1,2,3,... degree polynom fitting h(s)
;		hmax    = 0.1 ;range of alitudes (in solar radii)
;		filter   = 1   (highpass filter); 0=no filter 
;		window  = 0   IDL display window
; 		ct      = 3   color table 0,3,5,16  (inverted black/white if -1)
;	        wave    = '171','195','284','304' wavelength
;		bw	= black/white contrast (100% = 1 sigma standard dev)
;
; Outputs     : loopfile 	    = output: iloop,x,y,z,...
;
; History     : Oct/Nov 2007, Version 1 written by Markus J. Aschwanden
;		May 1/2008, weighted multi-scale highpass-filter ism=3,5,..,nsm
;		May 5/2008, debug option POLY=0, (ZP=ZP_RAW, CHI2=0)
;		May 5/2008, lower limit of height h=0 (rp_raw=rp_raw>rad_pix)
;
; Contact     : aschwanden@lmsal.com
;-

if (n_params(0) le 10) then wave='171'
if (n_params(0) le 11) then gamma=0.5
if (n_params(0) le 12) then inv=-1
iwave	=0
if (wave eq '171') then iwave=1
if (wave eq '195') then iwave=2
if (wave eq '284') then iwave=3
if (wave eq '304') then iwave=4

;________________________________________________________________
;			SUBIMAGE EXTRACTION 			|
;________________________________________________________________
start	=0
fov0	=fov
START:
rsun_m  =0.696*1.e9
cdelt1	=para.cdelt1
cdelt2	=para.cdelt2
crpix1	=para.crpix1
crpix2	=para.crpix2
da	=para.da/rsun_m
db	=para.db/rsun_m
rsun   	=para.rsun
a_sep	=para.sep_deg*(!pi/180.)
rad_pix	=rsun/cdelt1
r_max   =1.0+hmax
i1	=fov0(0)
j1	=fov0(1)
i2	=fov0(2)
j2	=fov0(3)
nx	=(i2-i1+1)
ny	=(j2-j1+1)
ism	=(nsm-3)/2			;nsm=3,5,7-->ism=0,1,2
r_n	=1.0
im	=(i1+i2)/2.
jm	=(j1+j2)/2.
rm	=sqrt((im-crpix1)^2+(jm-crpix2)^2)
im_b	=im
jm_b	=jm
if (rm lt rad_pix) then euvi_stereo_rot,im,jm,para,r_n,im_b,jm_b,zm_b
i1_b	=im_b-(im-i1)
i2_b	=i1_b+(i2-i1)
xrange1 =[i1  ,i2  ]
xrange2 =[i1_b,i2_b]
yrange  =[j1,j2]		
z0	=sqrt(rad_pix^2-(jm-crpix2)^2)
jrange	=[-0.5,+1.5]*((fov0(2)-fov0(0))/2.)
zrange  =z0+jrange
images_fov=fltarr(nx,ny,2)		;2 spacecraft(A,B)  
print,'EUVI/A pixel range i1,i2,j1,j2=',i1,i2,j1,j2
print,'EUVI/B pixel range i1,i2,j1,j2=',i1_b,i2_b,j1,j2
image_a	=float(reform(image_pair(1,i1:i2,j1:j2)))
image_b	=float(reform(image_pair(0,i1_b:i2_b,j1:j2)))
help,image_a,image_b
for it=0,1 do begin
 if (it eq 0) then image0 =image_a
 if (it eq 1) then image0 =image_b
 euvi_imagefilter,filter,nsm,image0,image1,gamma,inv
 images_fov(*,*,it)=image1
endfor
panel1	=[0.5,0.0,1.0,1.00]
panel2	=[0.0,0.0,0.5,1.00]
nloop	=0
loopcol	=150
loopcol2= 50
zoom	=(long((2000/(2*nx)) < (1600/(2*ny))))>1
print,'Use zoom factor of ',zoom
phi	=2*!pi*findgen(361)/360.
xlimb	=crpix1+rad_pix*cos(phi)
ylimb	=crpix2+rad_pix*sin(phi)

print,'__________________________________________________________________'
print,'|		PLOT STEREO IMAGE PAIR  			|'
print,'__________________________________________________________________'
io	=0
char	=1
xsize	=zoom*nx*2
ysize	=zoom*ny
window,window,xsize=xsize,ysize=ysize
set_plot,'x'
!p.font=-1
loadct,ct
for it=0,start do begin
 if (it eq 0) then panel=panel1 
 if (it eq 1) then panel=panel2 
 x1_	=panel(0)
 y1_	=panel(1)
 x2_	=panel(2)
 y2_	=panel(3)
 image  =images_fov(*,*,it)
 statistic,image,avg,dev
 z2	=avg+2*dev
 z1	=avg-2*dev
 if (io eq 0) then begin
  nxw  =long(!d.x_vsize*(x2_-x1_)+0.5)
  nyw  =long(!d.y_vsize*(y2_-y1_)+0.5)
  ima  =congrid(image,nxw,nyw)
 endif
 if (io ne 0) then ima=image
 !p.position=[x1_,y1_,x2_,y2_]
 if (it eq 0) then !x.range=xrange1
 if (it eq 1) then !x.range=xrange2
 !y.range=yrange
 !x.style=1
 !y.style=1
 plot,[0,0],[0,0]
 tv,bytscl(ima,min=z1,max=z2),x1_,y1_,xsize=(x2_-x1_),ysize=(y2_-y1_),/normal
 oplot,xlimb,ylimb
 if (it eq 0) then sc='A'
 if (it eq 1) then sc='B'
 xyouts_norm,0.02,0.02,sc,3
 !noeras=1
endfor		;stereo image pair
oplot,[i1,i1],[j1,j2],thick=4

print,'__________________________________________________________________'
print,'|		PLOT PREVIOUS LOOPS 				|'
print,'__________________________________________________________________'
loadct,3
file_exist=findfile(loopfile,count=nfiles)
if (nfiles ge 1) then begin 
 readcol,loopfile,iloop_,ix_,iy_,iz_,ih_raw,ih_,dh_,nsm_,wave_,chi2_
 euvi_loop_renumerate,iloop_,wave_
 nloop	=long(max(iloop_))+1
 if (nloop lt 1) then stop,'loopfile empty: ',loopfile
 zrange =avg(iz_)+jrange
 print,'Datafile with loop coordinates found  LOOPFILE=',loopfile 
 print,'Number of loops found                NLOOP   ='+string(nloop,'(i3)')
 for iloop=0,nloop-1 do begin 
  ind	=where(iloop_ eq iloop,np) 
  xp	=ix_(ind)
  yp	=iy_(ind)
  zp	=iz_(ind)
  euvi_spline,para,xp,yp,zp,x,y,z,d,r,r_n
  n	=n_elements(x)
  euvi_stereo_rot,x,y,para,r_n,x_b,y_b,z_b

  !p.position=panel1		;STEREO A projection of field lines
  !x.range=xrange1
  !y.range=yrange
  iwave_old=wave_(ind(0))
  th=4
  if (iwave_old lt iwave) then th=2
  plot,x,y,color=loopcol,thick=th
  if (x(n-1) ge xrange1(0)) and (x(n-1) le xrange1(1)) and $
     (y(n-1) ge yrange(0))  and (y(n-1) le yrange(1)) then $
     xyouts,x(n-1),y(n-1),string(iloop,'(I3)'),color=0 	;color=loopcol

  !p.position=panel2 		;STEREO B projection of field lines
  !x.range=xrange2
  plot,x_b,y_b,color=loopcol,thick=2
  xyouts,x_b(n-1),y_b(n-1),string(iloop,'(I3)'),color=loopcol 

 endfor
endif
if (nfiles eq 0) then print,'No previous loopfile found';

if (start eq 0) then begin
 print,'__________________________________________________________________'
 print,'|		NARROW DOWN FIELD OF VIEW			|'
 print,'__________________________________________________________________'
 !p.position=panel1
 !x.range=xrange1
 !y.range=[j1,j2]
 plot,[0,0],[0,0]
 iloop   =nloop
 print,'Narrow down field-of-view for loop (Click on BL and TR corners of a box)'
 cursor,x1fov,y1fov
 wait,0.2
 oplot,x1fov*[1,1],y1fov*[1,1],psym=1,thick=4
 cursor,x2fov,y2fov
 wait,0.2
 oplot,x2fov*[1,1],y2fov*[1,1],psym=1,thick=4
 oplot,[x1fov,x2fov,x2fov,x1fov,x1fov],[y1fov,y1fov,y2fov,y2fov,y1fov],color=255
 wait,1
 fov0=long([x1fov<x2fov,y1fov<y2fov,x1fov>x2fov,y1fov>y2fov])
 start=1
 goto,start
endif

print,'__________________________________________________________________'
print,'|		DRAWING NEW LOOP IN STEREO A			|'
print,'__________________________________________________________________'
!p.position=panel1
!x.range=xrange1
!y.range=[j1,j2]
plot,[0,0],[0,0]
iloop   =nloop
print,'Trace loop #'+string(iloop+1,'(i4)')
print,' Click continuous positions in A [right panel] (end in left panel)'
npos    =1000
xp      =fltarr(npos)   ;new input for loop positions x(i),i=0,...,npos-1
yp      =fltarr(npos)   ;new input for loop positions y(i),i=0,...,npos-1
for i=0,npos-1 do begin
 cursor,xp0,yp0
 wait,0.2
 if (xp0 gt i2) or (xp0 lt i1) or (yp0 lt j1) or (yp0 ge j2) then goto,endloop
 xp(i)	=xp0
 yp(i)  =yp0
 print,'read coordinates x,y='+string(xp0,'(f6.1)')+','+string(yp0,'(f6.1)')+' pixels'
 oplot,xp(0:i),yp(0:i),psym=1
 oplot,xp(0:i),yp(0:i),linestyle=1
endfor
endloop:
np      =i
xp	=xp(0:np-1)
yp	=yp(0:np-1)
spline_p,xp,yp,x,y
n       =n_elements(x)
oplot,x,y,color=loopcol,thick=4
xyouts,x(n-1),y(n-1),string(iloop,'(i3)'),size=1,color=loopcol
read,'1=Open loop (start 1st footpoint), 2=Closed loop (end at 2nd footpoint) : ',open  

print,'__________________________________________________________________'
print,'|		CHECK ON DISK VS. OFF LIMB			|'
print,'__________________________________________________________________'
dist_center=max(sqrt((x-crpix1)^2+(y-crpix2)^2))/rad_pix
print,'Maximum distance from Sun center = ',dist_center

print,'__________________________________________________________________'
print,'|		TRACING CORRESPONDING LOOP IN STEREO B		|'
print,'__________________________________________________________________'
!p.position=panel2
!x.range=xrange2
plot,[0,0],[0,0]
euvi_stereo_rot,x,y,para,1.   ,xb0,yb0,zb0
;euvi_stereo_rot,x,y,para,r_max,xb1,yb1,zb1
if (open eq 1) then begin
 r_1=1.+0.25*(r_max-1.)*sin((!pi/2)*findgen(n)/float(n-1))
 r_2=1.+0.5*(r_max-1.)*sin((!pi/2)*findgen(n)/float(n-1))
 r_3=1.+1.0*(r_max-1.)*sin((!pi/2)*findgen(n)/float(n-1))
 r_4=1.+1.5*(r_max-1.)*sin((!pi/2)*findgen(n)/float(n-1))
endif
if (open eq 2) then begin
 r_1=1.+0.25*(0.5*r_max-1.)*sin(!pi*findgen(n)/float(n-1))
 r_2=1.+0.5*(0.5*r_max-1.)*sin(!pi*findgen(n)/float(n-1))
 r_3=1.+1.0*(1.0*r_max-1.)*sin(!pi*findgen(n)/float(n-1))
 r_4=1.+1.5*(r_max-1.)*sin(!pi*findgen(n)/float(n-1))
endif
euvi_stereo_rot,x,y,para,r_1  ,xb1,yb1,zb1
euvi_stereo_rot,x,y,para,r_2  ,xb2,yb2,zb2
euvi_stereo_rot,x,y,para,r_3  ,xb3,yb3,zb3
euvi_stereo_rot,x,y,para,r_4  ,xb4,yb4,zb4
oplot,xb0,yb0,color=255,thick=1
oplot,xb1,yb1,color=200,thick=1
oplot,xb2,yb2,color=200,thick=1
oplot,xb3,yb3,color=200,thick=1
oplot,xb4,yb4,color=200,thick=1
xyouts,xb0(n-1),yb0(n-1),'h=0',size=char
xyouts,xb1(n-1),yb1(n-1),'h=0.025',size=char
xyouts,xb2(n-1),yb2(n-1),'h=0.05',size=char
xyouts,xb3(n-1),yb3(n-1),'h=0.10',size=char
xyouts,xb4(n-1),yb4(n-1),'h=0.15',size=char
if (open ne 2) then begin
xyouts,xb0(n/2),yb0(n/2),'h=0',size=char
xyouts,xb1(n/2),yb1(n/2),'h=0.025',size=char
xyouts,xb2(n/2),yb2(n/2),'h=0.05',size=char
xyouts,xb3(n/2),yb3(n/2),'h=0.10',size=char
xyouts,xb4(n/2),yb4(n/2),'h=0.15',size=char
endif

print,'Click epipolar x-positions in image B [on horizontal bars in left panel]'
euvi_stereo_rot,xp,yp,para,1.   ,xb0,yb0,zb0
euvi_stereo_rot,xp,yp,para,r_max,xb1,yb1,zb1
xyouts,xb0(0),yb0(0)+1,'h=0',size=char*2,color=loopcol
xyouts,xb1(0),yb1(0)+1,'h='+string(hmax,'(f3.1)'),size=char*2,color=loopcol
hp_raw	=fltarr(np)
xph_raw =fltarr(np)
for i=0,np-1 do begin
 oplot,!x.range,[yb0(i),yb1(i)],color=255
 oplot,[xb0(i),xb1(i)],[yb0(i),yb1(i)],thick=8,color=0
 oplot,[xb0(i),xb1(i)],[yb0(i),yb1(i)],thick=4,color=255
 cursor,xph,yph
 wait,0.2
 if (yph gt yrange(1)) then goto,endproc
 xph_raw(i)=xph
 oplot,xph*[1,1],yph*[1,1],psym=4,color=loopcol
 oplot,xph_raw(0:i),yb0(0:i),thick=8,color=loopcol
 hp_raw(i)=(rad_pix*hmax*(xph-xb0(i))/(xb1(i)-xb0(i))) > 0. ;lin interp on disk
 if (a_sep ne 0) and (dist_center ge 1) then begin ;stereoscopic interpolation
  xa	=(xp(i)     -crpix1)
  xb	=(xph_raw(i)-crpix1)                           
  hp_raw(i)=(sqrt(xa^2+((xa*cos(a_sep)-xb)/sin(a_sep))^2)-rad_pix) > 0.  
 endif
 hp_Mm    =(hp_raw(i)/rad_pix)*696. ;Mm
 print,'Stereoscopic height  h/r_sun=',hp_raw(i)/rad_pix,' r=',hp_Mm,' Mm',$
  format='(a,f8.3,a,f6.1,a)
 oplot,xph*[1,1],xph*[1,1],psym=4,color=loopcol
endfor

print,'__________________________________________________________________'
print,'|		POLYNOM FITTING h(s)		  		|'
print,'__________________________________________________________________'
dhp	=fltarr(np)
dp	=fltarr(np)
for i=0,np-1 do begin
 ii	=i>1
 dp(i)  =dp(ii-1)+sqrt((xp(ii)-xp(ii-1))^2+(yp(ii)-yp(ii-1))^2)
 tan_th	=abs(xp(ii)-xp(ii-1))/(abs(yp(ii)-yp(ii-1))>0.5) ;direct. slope dx/dy
 dxp	=(1./2.)*sqrt(1.+tan_th^2)	          ;x-parallax error in pixels
 dhp(i) =(rad_pix*hmax)*dxp/abs(xb1(i)-xb0(i))   ;height error in pixel
 print,'Altitude = ',hp_raw(i),'+/-',dhp(i)
endfor
dhp(0)=dhp(1)
for i_poly=(2<poly),poly do begin
 coeff	=poly_fit(dp,hp_raw,i_poly)	;poly=degree of polynom
 hp	=fltarr(np)+coeff(0)
 for ip=1,i_poly do hp=hp+coeff(ip)*dp^ip
 nfree	=np-(i_poly+1)
 chi2	=sqrt(total((hp-hp_raw)^2/dhp^2)/float(nfree))
 print,'i_poly,poly,np,nfree=',i_poly,poly,np,nfree,' chi2=',chi2,$
  format='(a,4i5,a,f7.2)'
 if (chi2 le 1.5) or (nfree le 2) then goto,end_polyfit
endfor
end_polyfit:
rp	=rad_pix+hp
zp      =sqrt((rp^2-(xp-crpix1)^2-(yp-crpix2)^2)>0.01)
euvi_spline,para,xp,yp,zp,x,y,z,d,r,r_n
euvi_stereo_rot,x,y,para,r_n,x_b,y_b,z_b
oplot,x_b,y_b,color=loopcol

read,'Save loop ? [0,1] = ',save

print,'__________________________________________________________________'
print,'|		SAVE LOOP COORDINATES 				|'
print,'__________________________________________________________________'
if (save eq 1) then begin
 q1	=0.
 q2	=0.
 openw,1,loopfile,/append
 for i=0,np-1 do printf,1,iloop,xp(i),yp(i),zp(i),hp(i),hp_raw(i),dhp(i),$
    nsm,iwave,chi2,q1,q2,format='(i6,6f8.1,2i6,3f6.2)'
 close,1
 print,'nloop = '+string(iloop+1,'(i3)')+'  loopfile = '+loopfile
endif
endproc:
end
