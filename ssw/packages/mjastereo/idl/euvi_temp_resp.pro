pro euvi_temp_resp,wave,sc,temp,resp,method
;+
; Project     : STEREO
;
; Name        : EUVI_TEMP_RESP 
;
; Category    : EUVI instrument
;
; Explanation : This routine produces a temperature response function
;
; Syntax      : IDL> euvi_temp_resp,...
;
; Input       : wave       ='171', '195', '284' wavelength string
;		sc	   ='a','b' spacecraft
;		temp(nte)  =array with temperature values [MK]	
;	        method     =0 (gaussian approximation), 1=EUVI preliminary
;
; Outputs     : resp(nte)  =array with response function 
;			    in units of photons/s [for EM=10^44 cm-3] 
;	        (see Fig.2 in Aschwanden et al. 2008, ApJ 680, 1477)
;
; History     : Jun 2008, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

;Approximative EUVI response function_____________________________
if (method eq 0) then begin
 wave_   =['171','195','284']
 tpeak_  =[0.90,1.50,2.20] ;MK
 dtpeak_ =[0.30,0.40,0.50] ;MK
 rpeak_  =[3500.,1500.,150.]
 iwave   =where(wave eq wave_)
 tpeak   =tpeak_(iwave(0))
 dtpeak  =dtpeak_(iwave(0))
 rpeak	=rpeak_(iwave(0))
 resp    =rpeak*exp(-((temp-tpeak)/dtpeak)^2/2.)   ;Gaussian approximation
endif

;Preliminary EUVI response function (Nitta/Wuelser 2008)____________
if (method eq 1) then begin
 respfile=concat_dir('$SSW_MJASTEREO','idl/euvi_response.dat')
 readcol,respfile,t_mk,r1_a,r2_a,r3_a,r4_a,r1_b,r2_b,r3_b,r4_b
 if (sc eq 'a') and (wave eq '171') then res=r1_a ;STEREO A
 if (sc eq 'a') and (wave eq '195') then res=r2_a ;STEREO A
 if (sc eq 'a') and (wave eq '284') then res=r3_a ;STEREO A
 if (sc eq 'a') and (wave eq '304') then res=r4_a ;STEREO A
 if (sc eq 'b') and (wave eq '171') then res=r1_b ;STEREO B
 if (sc eq 'b') and (wave eq '195') then res=r2_b ;STEREO B
 if (sc eq 'b') and (wave eq '284') then res=r3_b ;STEREO B
 if (sc eq 'b') and (wave eq '304') then res=r4_b ;STEREO B
 resp	=interpol(res,t_mk,temp)
endif
end

