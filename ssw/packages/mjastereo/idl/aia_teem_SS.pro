pro aia_teem_SS,qnoise,dx_cm,nbin,flux_aia,teem_table,saveset2
;+
; Project     : AIA/SDO
;
; Name        : AIA_TEEM_SS
;
; Category    : AIA Temperature and Emission measure analysis
;
; Explanation : DEM inversion from AIA flux maps AIA_FLUX[NX,NY]
;               based on single-Gaussian DEM fits in each macropixel NBIN
;		and integration over entire image area
;
; Syntax      : IDL>aia_teem_SS,dx,nbin,teem_table,aia_flux,saveset2
;
; Inputs      : dx_cm		= pixel size [cm]
;		nbin		= macro pixel size (number of pixels)
;		teem_table	= filename of response function file	
;		flux_aia[nx,ny,nwave] = AIA flux maps in all wavelengths
;		saveset2	= output parameters:
;
; History     : 23-Oct-2014, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

;________________TEMPERATURE BINNING AND RESPONSE FUNCTION_______________
restore,teem_table ;-->wave_,tsig_,resp,telog,dte,flux
;		wave_(newave)   AIA wavelength set [Angstroem]
;               tsig_(nsig)     Gaussian half width [0.01,...,1.00]
;               resp(nte,nwave) Response function
;               telog(nte)      logarithmic temperature range
;               dte(nte)        logarithmic temperature bins
;               flux(nte,nsig,nwave) AIA fluxes for unity emission measure
te	=10.^telog
nte     =n_elements(telog)
nwave	=n_elements(wave_)
nsig    =n_elements(tsig_)
dem_t	=fltarr(nte)
flux_obs_tot=fltarr(nwave)
flux_dem_tot=fltarr(nwave)
flux_dem_best=0.
if (qnoise le 0.) then begin &qnoise=0.1 &print,'SET qnoise=0.1' &endif

;________________REBINNING INTO MACROPIXELS______________________________
nfree   =3
dim	=size(flux_aia)
nx	=dim[1]
ny	=dim[2]
nxx     =long(nx/nbin)                                ;rebinned size
nyy     =long(ny/nbin)
flux_nbin=rebin(flux_aia,nxx,nyy,nwave)
em_map	=fltarr(nxx,nyy)
te_map	=fltarr(nxx,nyy)
sig_map	=fltarr(nxx,nyy)
telog3   =alog10(te_range[2])
telog4   =alog10(te_range[3])
ind_te   =where((telog ge telog3) and (telog le telog4),ind_te)
ite1     =min(ind_te)
ite2     =max(ind_te)

;_________________TEEM ANALYSIS IN MACROPIXELS__________________________
for j=0,nyy-1 do begin
 for i=0,nxx-1 do begin
  flux_obs=reform(flux_nbin[i,j,*])                      ;flux [DN/s pix]
  unce_obs=qnoise*flux_obs                               ;flux [DN/s pix]
  em_best   =0.
  telog_best=0.
  sig_best  =0.1
  chi_best  =1.e10
  indw   =where((flux_obs gt 0),nindw)
  if (nindw lt nwave) then goto,skip                      ;skip this time frame
  for k=ite1,ite2 do begin
   for l=0,nsig-1 do begin
    flux_dem1=reform(flux(k,l,*))			  ;model flux 6 wavelen
    em1=median(flux_obs/flux_dem1)			  ;median ratio		
    flux_dem=flux_dem1*em1
    chi =sqrt(total((flux_obs-flux_dem)^2/unce_obs^2)/(nwave-nfree))
    if (chi le chi_best) then begin
     chi_best   =chi
     em_best    =em1				  	  ;[cm-5 K-1]
     telog_best =telog(k)				  
     sig_best   =tsig_(l)
     flux_dem_best=flux_dem
    endif
   endfor
  endfor
  em_map(i,j)=em_best
  te_map(i,j)=10.^telog_best
  sig_map(i,j)=sig_best
  em_kelvin=em_best*exp(-(telog-telog_best)^2/(2.*sig_best^2))
  dem_t(*)=dem_t(*)+em_kelvin
  flux_obs_tot=flux_obs_tot+flux_obs
  flux_dem_tot=flux_dem_tot+flux_dem_best
skip:
 endfor
endfor

;___________________GOODNESS OF FIT______________________________________
unce_obs_tot=qnoise*flux_obs_tot
ind    =where(flux_obs_tot gt 0,nind)
chi_best =sqrt(total((flux_obs_tot(ind)-flux_dem_tot(ind))^2/unce_obs_tot(ind)^2)/(nfree))
print,'CHI-BEST=',chi_best

;___________________VOLUME EMISSION MEASURE______________________________
emlog_inv=alog10(dem_t)+2.*alog10(nbin*dx_cm)     ;log(EM [cm-3] K-1)
emlog_inv0=emlog_inv

;___________________SAVE OUTPUT DATA_____________________________________
emlog_inv_err=emlog_inv*0.
save,filename=saveset2,emlog_inv,emlog_inv_err,chi_best,em_map,te_map,sig_map 
end
