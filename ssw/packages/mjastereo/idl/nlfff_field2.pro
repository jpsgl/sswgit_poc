pro nlfff_field2,dir,savefile,bmin,ngrid
;+
; Project     : SOHO/MDI, SDO/HMI, STEREO
;
; Name        : NLFFF_FIELD
;
; Category    : Magnetic data modeling
;
; Explanation : Calculating NF magnetic field lines FIELD_LINES[NS,5,NF]
;               from magnetic field coefficients [B1,X1,Y1,Z1,A1]
;		in a grid of footpoints
;
; Syntax      : IDL>nlfff_field2,dir,savefile,bmin,ngrid
;
; Inputs      : dir,savefile
;		bmin     = [G] minimum magnetic field strength
;		ngrid    = grid dimension for magnetc field footpoints
;
; Outputs     ; runfile   = output files *_fit.sav
;
; History     : 17-JUL-2015, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

print,'__________________NLFFF_FIELD2_________________________________'
savefiles=findfile(dir+savefile,count=nfiles)
if (nfiles le 0) then begin
 savefiles=findfile(dir+'*.sav',count=nfiles)
 if (nfiles eq 0) then stop,'No file exists: *.sav'
endif
isel	=0
if (nfiles ge 2) then begin
 for i=0,nfiles-1 do print,i,'  ',savefiles(i)
 read,'Enter number of savefile = ',isel
endif
dir_savefile=savefiles(isel)

;_________________________READ SAVEFILE_______________________________
restore,dir_savefile
print,'reading savefile = ',dir_savefile

fov0    =input.fov0      ;field-of-view
x1_sun  =para.x1_sun
x2_sun  =para.x2_sun
y1_sun  =para.y1_sun
y2_sun  =para.y2_sun
hmax    =0.05                   ;maximum altitude (solar radii)
nseg    =9                      ;number of loop segments (misalignment angles)
ds      =0.002                  ;spatial resolution along loop
nloop   =10000                  ;loop array limit
nsmax   =long(fov0/ds)
dim     =size(bzfull)
nx      =dim(1)
ny      =dim(2)
dpix    =float(x2_sun-x1_sun)/float(nx-1)
x       =x1_sun+dpix*findgen(nx)
y       =y1_sun+dpix*findgen(ny)
nf      =n_elements(ns_loop)
dim     =size(bzfull)
nx      =dim(1)
ny      =dim(2)
dpix    =float(x2_sun-x1_sun)/float(nx-1)
x       =x1_sun+dpix*findgen(nx)
y       =y1_sun+dpix*findgen(ny)

;____________MAGNETIC FIELD FROM GRID OF FOOTPOINTS___________________
nf	=ngrid^2
field_lines_grid=fltarr(nsmax,3,nf)
ns_field_grid=lonarr(nf)
dxgrid	=nx/float(ngrid)
dygrid	=ny/float(ngrid)
rm	=1.+ds/2.
kk	=0
for j=0,ngrid-1 do begin
 ym	=y1_sun+(y2_sun-y1_sun)*(j+0.5)/float(ngrid)
 iy	=long((j+0.5)*dygrid)
 for i=0,ngrid-1 do begin
  xm	=x1_sun+(x2_sun-x1_sun)*(i+0.5)/float(ngrid)
  ix	=long((i+0.5)*dxgrid)
  if (abs(bzfull(ix,iy)) ge bmin) then begin
   rm	=1.+ds/2.
   zm	=sqrt(rm^2-xm^2-ym^2)
   nlfff_fieldline,xm,ym,zm,ds,hmax,coeff,field,np,nsmax
   np	=np < nsmax
   field_lines_grid(0:np-1,0,kk)=field(0:np-1,0)
   field_lines_grid(0:np-1,1,kk)=field(0:np-1,1)
   field_lines_grid(0:np-1,2,kk)=field(0:np-1,2)
   ns_field_grid[kk]=np
   kk	=kk+1
  endif
 endfor
 print,'field line grid at row = ',j,ngrid 
endfor
nf	=kk

;_______________________SAVE PARAMETERS________________________
save,filename=dir_savefile,input,para,bzmap,bzfull,bzmodel,coeff,$
     field_loop_det,ns_loop_det,wave_loop_det,$
     field_loop,ns_loop,wave_loop,dev_deg,dev_deg2,$
     field_lines,ns_loop,ns_field,e_h,e_p,e_np,b2_free,$
     field_lines_grid,ns_field_grid
print,'parameters saved in file = ',dir_savefile
end

