pro dist_perp_curve,x0_,x,y,z,xn_,dist,is_next

;calculated perpendiculat shortest distance DIST of a point X0_=[X0,Y0,Z0]
;from a 3D curve [X(s),Y(s),Z(s)] parameterized as a function of the
;loop length coordinate (s). XN_(3) are the 3D coordinates of the nearest point.

ns	=n_elements(x)
nsy	=n_elements(y)
nsz	=n_elements(z)
if (ns ne nsy) and (ns eq nsz) then stop,'DIST_PERP_CURVE : inconsistent coord' 
if (ns le 3) then stop,'DIST_PERP_CURVE : curve with < 3 points)
x0	=x0_(0)
y0	=x0_(1)
z0	=x0_(2)

;distances to different points in curve
d	=sqrt((x-x0)^2+(y-y0)^2+(z-z0)^2)

;distance to next curve point
dmin	=min(d,imin)

;parabolic interpolation
if (imin ge 1) and (imin le ns-2) then begin
 i0	=imin 
 i1	=i0-1
 i2	=i0
 i3	=i0+1
 d1	=d(i0-1)
 d2	=d(i0)
 d3	=d(i0+1)
 parabol2,i1,i2,i3,d1,d2,d3,is_next,dist,slope,ier
 is_next=(is_next > 0) < (ns-1)
endif
if (imin eq 0) or (imin eq ns-1) then begin
 dist	=dmin
 is_next=imin
 i1	=is_next < (ns-2)
 i2	=i1+1
endif

;coordinate of nearest point
xn_	=fltarr(3)
q	=(is_next-i1)/(i2-i1)
xn_(0)	=x[i1]+(x[i2]-x[i1])*q
xn_(1)	=y[i1]+(y[i2]-y[i1])*q
xn_(2)	=z[i1]+(z[i2]-z[i1])*q
end
