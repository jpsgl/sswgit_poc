pro euvi_dimming_mass,dimfiles3,savefile

common powell_par,t3,t1_,t2_,mc_,m1,m2,m3,m1obs,m2obs,m3obs

t1_	=fltarr(3)
t2_	=fltarr(3)
mc_	=fltarr(3)
wd_	=fltarr(3)
for ifile=0,2 do begin
 restore,dimfiles3(ifile) 
 t1_(ifile)=tmin
 t2_(ifile)=tmax
 mc_(ifile)=m15/(tmax-tmin)
 wd_(ifile)=w0_dim
endfor
obs	=index(0).obsrvtry

wd_med	=median(wd_)
t3	=4. ;MK
m0_cme	=max(mc_)
t0_cme	=1.5
dt0_cme	=0.5
m1obs	=mc_(0)/(t2_(0)-t1_(0))
m2obs	=mc_(1)/(t2_(1)-t1_(1))
m3obs	=mc_(2)/(t2_(2)-t1_(2))
coeff	=[m0_cme,t0_cme,dt0_cme]
npar	=n_elements(coeff)
xi	=fltarr(npar,npar)
for i=0,npar-1 do xi(i,i)=1.
ftol	=1.e-4
ind	=where(mc_ gt 0,nind) 
if (nind eq 0) then begin
 m_peak	=0.
 t_cme	=1.
 dt_cme =1.
endif
if (nind eq 1) then begin
 m_peak =mc_(ind(0))
 t_cme	=(t2_(ind(0))+t1_(ind(0)))/2.
 dt_cme	=(t2_(ind(0))-t1_(ind(0)))/2.
endif
if (nind eq 2) then begin
 m_peak =(mc_(ind(0))+mc_(ind(1)))/2.
 t_cme	=(t2_(ind(1))+t1_(ind(0)))/2.
 dt_cme	=(t2_(ind(1))-t1_(ind(0)))/2.
endif
if (nind eq 3) and (m2obs le (m1obs>m3obs))  then begin
 statistic,[m1obs,m2obs,m3obs],m_peak,msigma
 t_cme	=(t2_(2)+t1_(0))/2.
 dt_cme	=t2_(2)-t1_(0)
endif
if (nind eq 3) and (m2obs gt (m1obs>m3obs))  then begin
 powell,coeff,xi,ftol,fmin,'euvi_dimming_powell'
 m_peak	=coeff(0)
 t_cme	=(coeff(1)>t1_(0))<t2_(2)
 dt_cme	=abs(coeff(2))
endif
mass1	=sqrt(2.*!pi)*m_peak*dt_cme	;Gaussian integral 
n	=1000
te	=t3*findgen(n)/float(n-1)
ygauss	=m_peak*exp(-(te-t_cme)^2/(2.*dt_cme^2))
ind0	=where((te lt min(t1_)) or (te gt max(t2_)))
ygauss(ind0)=0.
m_cme	=total(ygauss)*(te(1)-te(0))
save,filename=savefile,t1_,t2_,mc_,wd_,m_peak,m_cme,t_cme,dt_cme,$
	te,ygauss,obs,m1obs,m2obs,m3obs
end
