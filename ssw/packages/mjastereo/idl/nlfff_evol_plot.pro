pro nlfff_evol_plot,efile,io,ev_nr,tmargin,dt,instr

;________________________INPUT SELECTION________________________
unit    =2	;window number
form    =1      ;0=landscape, 1=portrait
char    =0.8    ;character size
plotname=instr+'_evol'
string0,3,ev_nr,fignr
ref     =''     
ct	=5	;color table
blue	=50
red	=125
orange	=150
yellow	=200

;______________________GOES DATA_________________________________
restore,'goes_prof.sav'  ;-->event_,time_goes_,flux_goes_,deriv_goes_
ind	=where(event_ eq ev_nr)
iev_goes=ind(0)

readcol,efile,event_,tstart_,tpeak_,tend_,goes_,noaa_,helpos_,$
       format='(I,A,A,A,A,A,A)',/silent
ind     =where(event_ eq ev_nr,nind)
if (nind le 0) then stop,'NLFFF_EVENT : NO MATCHING EVENT FOUND'
tstart  =tstart_(ind(0))
tpeak   =tpeak_(ind(0))
tend    =tend_(ind(0))
t0_hr   =(anytim(tpeak ,/sec) mod 86400)/3600.
t1_hr   =(anytim(tstart,/sec) mod 86400)/3600.
t2_hr   =(anytim(tend  ,/sec) mod 86400)/3600.
t3_hr	=t1_hr-tmargin-dt/2.
t4_hr	=t2_hr+tmargin+dt/2.
time_str=strmid(tstart,0,10)+' '+strmid(tstart,11,5)+' UT'
ptitle=instr+' #'+string(ev_nr,'(I4)')+': '+time_str
class	=goes_(ind(0))

;______________________EVOL DATA__________________________
restore,instr+'_evol.sav'
ind	 =where(iev_ eq ev_nr,nind)
iev	 =ind(0)
nt	 =nt_(iev)
ind	 =where(hours_(0:nt-1,iev) ne 0,nind)
hours	 =hours_(ind,iev)
efree	 =efree_avg(0:nt-1,iev,0)
sfree	 =efree_avg(0:nt-1,iev,1)
ediss	 =ediss_avg[0:nt-1,iev,0]
sdiss    =ediss_avg[0:nt-1,iev,1]
erate	 =fdiss_avg[0:nt-1,iev,0]
srate	 =fdiss_avg[0:nt-1,iev,1]
len	 =len_avg(0:nt-1,iev,0)		
slen	 =len_avg(0:nt-1,iev,1)         
eneg	 =max(ediss)-ediss
sneg	 =srate

;_________________________PLOT FREE ENERGY____________________
fig_open,io,form,char,fignr,plotname,unit
loadct,ct
 x1_	=0.1+0.3*1
 x2_	=x1_+0.25
 y2_	=0.95
 y1_	=0.80
 !p.position=[x1_,y1_,x2_,y2_]
 !p.title=ptitle
 !y.title='E!Dfree!N [10!U30!N erg]' 
 !x.title=''
 !x.range=[t3_hr,t4_hr]
 !y.range=[0.,max(efree)*1.2]
 !x.style=5
 ind	=where((hours ne 0) and (efree ne 0))
 plot, t1_hr*[1,1],!y.crange,linestyle=2
 oplot,t0_hr*[1,1],!y.crange,linestyle=0,thick=2
 oplot,t1_hr*[1,1],!y.crange,linestyle=2
 oplot,t2_hr*[1,1],!y.crange,linestyle=2
 oplot,!x.range,!y.crange(1)*[1,1]*0.99
 oplot,hours(ind),efree(ind)
 errplot,hours(ind),efree(ind)-sfree(ind),efree(ind)+sfree(ind)
 !noeras=1

;_________________________PLOT NEGATIVE ENERGY____________________
 y2_	=y1_
 y1_	=y2_-0.15
 !p.position=[x1_,y1_,x2_,y2_]
 !p.title=''
 !y.title='E!Dneg!N [10!U30!N erg]' 
 !x.title=''
 !y.range=[0.,max(eneg)*1.2]
 ind	=where((hours ne 0) and (eneg ne 0))
 plot,t1_hr*[1,1],!y.crange,linestyle=2
 oplot,t0_hr*[1,1],!y.crange,linestyle=0,thick=2
 oplot,t1_hr*[1,1],!y.crange,linestyle=2
 oplot,t2_hr*[1,1],!y.crange,linestyle=2
 oplot,!x.range,!y.crange(1)*[1,1]*0.99
 oplot,hours(ind),eneg(ind)
 errplot,hours(ind),eneg(ind)-sneg(ind),eneg(ind)+sneg(ind)
 xyouts_norm,0.05,0.85,'E ='+string(ene(iev),'(I4)')+' 10!U32!N erg',char*1.5

;_________________________PLOT ENERGY DISSIPATION____________________
 y2_	=y1_
 y1_	=y2_-0.15
 !p.position=[x1_,y1_,x2_,y2_]
 !p.title=''
 !y.title='E!Ddiss!N [10!U30!N erg]' 
 !x.title=''
 !y.range=[0.,max(ediss)*1.2]
 ind	=where((hours ne 0) and (ediss ne 0))
 plot, t1_hr*[1,1],!y.crange,linestyle=2
 oplot,t0_hr*[1,1],!y.crange,linestyle=0,thick=2
 oplot,t2_hr*[1,1],!y.crange,linestyle=2
 oplot,!x.range,!y.crange(1)*[1,1]*0.99
 oplot,hours(ind),ediss(ind)
 errplot,hours(ind),ediss(ind)-sdiss(ind),ediss(ind)+sdiss(ind)

;_________________________PLOT DISSIPATED ENERGY RATE____________________
 y2_	=y1_
 y1_	=y2_-0.15
 !p.position=[x1_,y1_,x2_,y2_]
 !p.title=''
 !y.title='F!Ddiss!N [10!U30!N erg/0.1 hr]'
 !x.title=''
 !y.range=[0.,max(erate)*1.2]
 ind	=where((hours ne 0) and (erate ne 0))
 plot ,t1_hr*[1,1],!y.crange,linestyle=2
 oplot,t0_hr*[1,1],!y.crange,linestyle=0,thick=2
 oplot,t1_hr*[1,1],!y.crange,linestyle=2
 oplot,t2_hr*[1,1],!y.crange,linestyle=2
 oplot,!x.range,!y.crange(1)*[1,1]*0.99
 oplot,hours(ind),erate(ind)
 oplot,hours(ind),erate(ind),thick=5,color=red
 polyfill,[t3_hr,hours(ind),t4_hr],[0,erate(ind),0],spacing=0.1,orientation=45,color=red 
 xyouts_norm,0.05,0.85,'P ='+string(peak(iev),'(I4)')+' 10!U32!N erg',char*1.5
 xyouts_norm,0.05,0.75,'D ='+string(dur(iev),'(F4.1)')+' hrs',char*1.5

;______________________PLOT GOES FLUX_________________________'
 y2_	=y1_
 y1_	=y2_-0.15
 ymin	=1.e-7
 ind_t	=where(time_goes_(*,iev_goes) ne 0)
 time_goes=time_goes_(ind_t,iev_goes)
 ind	=where(hours ne 0,nind)
 if (max(time_goes) lt t3_hr) then time_goes=time_goes+24.
 if (min(time_goes) gt t4_hr) then time_goes=time_goes-24.
 flux_goes=flux_goes_(ind_t,iev_goes)
 deriv_goes=deriv_goes_(ind_t,iev_goes)
 !p.position=[x1_,y1_,x2_,y2_]
 !p.title='#'+string(ev_nr,'(I4)')+': '+strmid(tstart,0,10)+$
     ' '+strmid(tstart,11,5)+' UT'
 !x.title=''
 !y.title='GOES flux [10!U30!N erg]'
 !y.range=[ymin,max(flux_goes)]
 !y.style=0
 plot_io,time_goes,flux_goes,thick=3
 oplot,!x.crange,(10.^!y.crange(1))*[1,1]*0.95
 oplot,!x.range,!y.crange(0)*[1,1]
 oplot,t0_hr*[1,1],10.^!y.crange,linestyle=0,thick=2
 oplot,t1_hr*[1,1],10.^!y.crange,linestyle=2
 oplot,t2_hr*[1,1],10.^!y.crange,linestyle=2
 xyouts_norm,0.05,0.9,class,char*1.5
 df_norm=(deriv_goes/max(deriv_goes))
 df_dt	=max(flux_goes)*df_norm
 oplot,time_goes,df_dt>ymin,col=blue,thick=4
 polyfill,[t3_hr,time_goes,t4_hr],[ymin,df_dt>ymin,ymin],$
  spac=0.1,orient=45,color=blue

;______________________PLOT FLARE LENGTH SCALG________________________'
 y2_	=y1_
 y1_	=y2_-0.15
 !p.position=[x1_,y1_,x2_,y2_]
 !p.title=''
 !y.title='Length  L[Mm]' 
 !x.title='Time  t[hrs]'
 !x.style=1
 !y.range=[0.,max(len(1:nt-1))*1.5 > 20]
 ind	=where((hours ne 0) and (len ne 0))
 plot ,t1_hr*[1,1],!y.crange,linestyle=2
 oplot,t0_hr*[1,1],!y.crange,linestyle=0,thick=2
 oplot,t1_hr*[1,1],!y.crange,linestyle=2
 oplot,t2_hr*[1,1],!y.crange,linestyle=2
 oplot,!x.range,!y.crange(1)*[1,1]*0.99
 oplot,hours(ind),len(ind)
 errplot,hours(ind),len(ind)-slen(ind),len(ind)+slen(ind)

fig_close,io,fignr,ref
end


