pro nlfff_prof_goes,efile,gfile,iev,tmargin,dt,t5,time_goes,flux_goes,deriv_goes,class

;______________________READ GOES FLUX_________________________'
status  =['missing','exist']
exist  =findfile(efile,count=count)
print,'File '+status(count),' --> ',efile
exist  =findfile(gfile,count=count)
print,'File '+status(count),' --> ',gfile

restore,gfile  ;-->event_,time_goes_,flux_goes_,deriv_goes_
ind     =where(event_ eq iev)
iev_goes=ind(0)
readcol,efile,event_,tstart_,tpeak_,tend_,goes_,noaa_,helpos_,$
       format='(I,A,A,A,A,A,A)',/silent
ind     =where(event_ eq iev,nind)
if (nind le 0) then stop,'NLFFF_EVENT : NO MATCHING EVENT FOUND'
tstart  =tstart_(ind(0))
tpeak   =tpeak_(ind(0))
tend    =tend_(ind(0))
t1      =anytim(tstart,/sec)
t0      =anytim(tpeak ,/sec) 
t2      =anytim(tend  ,/sec) 
t3	=t1-tmargin*3600.
t4	=t2+tmargin*3600.
t1_hr   =(anytim(tstart,/sec) mod 86400)/3600.
t0_hr	=t1_hr+(t0-t1)/3600.
t2_hr	=t1_hr+(t2-t1)/3600.
t3_hr	=t1_hr+(t3-t1)/3600.
t4_hr	=t1_hr+(t4-t1)/3600.
time_str=strmid(tstart,0,10)  ;+' '+strmid(tstart,11,5)+' UT'
class   =goes_(ind(0))
ind_t   =where(time_goes_(*,iev_goes) ne 0)
time_goes=time_goes_(ind_t,iev_goes)
flux_goes=flux_goes_(ind_t,iev_goes)
deriv_goes=deriv_goes_(ind_t,iev_goes)
t5	=[t3_hr,t1_hr,t0_hr,t2_hr,t4_hr]
end
