dim	=size(subimage)
nx	=dim(1)
ny	=dim(2)
nscale	=long(alog(nx)/alog(2))
zmax	=max(subimage)
zmin	=min(subimage)
gap	=10
zlev	=zmin+1.
print,'nx,nscale=',nx,nscale
for iscale=0,nscale-1 do begin  
 nxx	=nx/(2^iscale)
 im	=rebin(subimage,nxx,nxx)
 ncol	=long(512/nx)
 icol	=(nx*iscale) mod 512
 irow	=nx*long(iscale/ncol)
 tv,bytscl(im,min=0,max=zmax),icol,irow

 nbox	=n_elements(im)
 ind	=where(im ge zlev,area) 
 len	=nxx
 dimen	=alog(area)/alog(nxx)
 
 print,iscale,nx,nxx,' area=',area,' len=',len,' D=',dimen
endfor
end
