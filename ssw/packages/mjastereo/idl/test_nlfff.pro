coeff	=[[1.,-0.5],[0.1,-0.1],[0.1,-0.1],[0.95,0.95],[0.0,0.0]]
coeff	=[[1.,-0.5],[0.1,-0.1],[0.1,-0.1],[0.95,0.95],[10.,10.]]
mag	=1000.
nx	=20
ny	=nx
z0	=1.0
x	=-0.2+0.4*findgen(nx)/float(nx-1)
y	=-0.2+0.4*findgen(ny)/float(ny-1)
z	=fltarr(nx)+z0
ds	=(max(x)-min(x))/float(nx)
aia_mag_map,coeff,mag,x,y,image

window,0,xsize=512,ysize=512
loadct,5
plot,[0,0],[0,0]
z1	=min(image)
z2	=max(image)
nlev	=20
level	=z1+(z2-z1)*findgen(nlev)/float(nlev-1)
contour,image,x,y,level=level
for i=0,1 do oplot,coeff[i,1]*[1,1],coeff[i,2]*[1,1],psym=4,thick=4

for j=0,ny-1 do begin
 xx	=x
 yy	=fltarr(nx)+y(j)
 zz 	=fltarr(nx)+z0
 x_	=[[xx],[yy],[zz]]
 nlfff_vector,coeff,x_,bfff_
 bx	=bfff_[*,0]
 by	=bfff_[*,1]
 bz	=bfff_[*,2]
 b 	=bfff_[*,3]
 for i=0,nx-1 do begin
  oplot,x[i]+ds*[0,bx[i]/b[i]],y[j]+ds*[0,by[i]/b[i]],thick=4
  polar =1.
  magn_vector,xx(i),yy(i),zz(i),ds,coeff,polar,x2,y2,z2,bx2,by2,bz2,b2,e,a
  oplot,[x(i),x2],[y(j),y2],color=128,thick=2
 endfor
endfor
end
