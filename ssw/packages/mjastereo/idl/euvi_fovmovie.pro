pro euvi_fovmovie,fov_file,zoom,ct,moviedir,filter,gamma,inv0,multi

restore,fov_file ;-->images_fov(nx,ny,nframe)
images_fov1	=images_fov
double	=1
diff	=1
if (multi ge 3) then diff=2
if (multi mod 2) eq 0 then begin
 double	=2
 fov_file2=fov_file
 ipos=strpos(fov_file,'a_fov.sav')
 fov_file2=strmid(fov_file,0,ipos)+'b_fov.sav'
 if (ipos eq -1) then begin
  ipos=strpos(fov_file,'b_fov.sav')
  fov_file2=strmid(fov_file,0,ipos)+'a_fov.sav'
 endif
 restore,fov_file2 ;-->images_fov(nx,ny,nframe)
 images_fov2	=images_fov
endif

dim	=size(images_fov1)
nx	=dim(1)
ny	=dim(2)
nframe	=dim(3)
filelist =strarr(nframe)
for iframe=0,nframe-1 do begin
 nxx	=nx*zoom
 nyy	=ny*zoom
 image0	=rebin(images_fov1(*,*,iframe),nxx,nyy)
 if (iframe eq 0) then begin
  zmin	=min(image0)
  zmax	=max(image0)
 endif
 nsm	=1
 inv	=1
 euvi_imagefilter,filter,nsm,image0,image,gamma,inv
 if (inv0 eq -1) then image=zmax-image
 if (double eq 1) and (diff eq 1) then image_gif=fltarr(nxx,nyy)
 if (double eq 2) and (diff eq 1) then image_gif=fltarr(nxx*2+1,nyy)
 if (double eq 1) and (diff eq 2) then image_gif=fltarr(nxx,nyy*2+1)
 if (double eq 2) and (diff eq 2) then image_gif=fltarr(nxx*2+1,nyy*2+1)
 image_gif(0:nxx-1,0:nyy-1)=image
 if (multi mod 2) eq 0 then begin
  image1=rebin(images_fov2(*,*,iframe),nxx,nyy)
  euvi_imagefilter,filter,nsm,image1,image2,gamma,inv
  if (inv0 eq -1) then image2=zmax-image2
  image_gif(nxx+1:2*nxx,0:nyy-1)=image2
 endif
 if (iframe ge 1) and (diff eq 2) then begin
  zmed	=median(image)
; diff1=zmed+10*(image-image_old)
; diff2=zmed+10*(image2-image2_old)
  coeff	=linfit(image_old,image)
  diff1 =zmed+10.*(image-(coeff(0)+coeff(1)*image_old))
  coeff	=linfit(image2_old,image2)
  diff2 =zmed+10.*(image2-(coeff(0)+coeff(1)*image2_old))
  image_gif(0:nxx-1,nyy+1:2*nyy)=diff1
  image_gif(nxx+1:2*nxx,nyy+1:2*nyy)=diff2
 endif

 set_plot,'X'
 window,0,xsize=nxx*double+1,ysize=nyy*diff+1,retain=2
 loadct,ct
 tv,bytscl(image_gif,min=zmin,max=zmax)
 if (double eq 2) then tv,bytscl(image2,min=zmin,max=zmax),nx*zoom+1,0
 o=tvrd(iframe,true=3)
 if (iframe lt  10) then outfile='frame_00'+string(iframe,'(i1)')+'.jpg'
 if (iframe ge  10) then outfile='frame_0'+string(iframe,'(i2)')+'.jpg'
 if (iframe ge 100) then outfile='frame_'+string(iframe,'(i3)')+'.jpg'
 write_jpeg,moviedir+outfile,o,true=3
 if (iframe lt  10) then outfile='frame_00'+string(iframe,'(i1)')+'.gif'
 if (iframe ge  10) then outfile='frame_0'+string(iframe,'(i2)')+'.gif'
 if (iframe ge 100) then outfile='frame_'+string(iframe,'(i3)')+'.gif'
 write_gif,moviedir+outfile,bytscl(image_gif,min=zmin,max=zmax)
 filelist(iframe)=outfile
 image_old=image
 image2_old=image2
endfor
image2movie,moviedir+filelist,/mpeg ;,/nodelete
end
