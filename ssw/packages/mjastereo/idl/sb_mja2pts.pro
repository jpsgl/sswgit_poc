; function that transforms cartesian point coordinates with
; x in Ahead/Behind/sun plane, z from suncenter towards Ahead, y perpend.
; (in solrad) to Carrington lon,lat,rad (in deg,deg,solrad)

function mja2rval,xyz,awcs,bwcs

  ap = awcs.position
  bp = bwcs.position

  ; 1.  determine position angle of sun-Behind vector as seen from Ahead
  ; 1.1 vector to Behind in system with z=solarN, Ahead in -y/z plane
  bz  = sin(bp.solar_b0/!radeg)
  bxy = cos(bp.solar_b0/!radeg)
  b_a = (bp.hgln_obs-ap.hgln_obs) / !radeg
  bxyz1 = [bxy*sin(b_a),-bxy*cos(b_a),bz]
  ; 1.2 rotate about x so that z points to Ahead:
  bxyz2 = bxyz1
  ang = (ap.solar_b0-90.0)/!radeg
  bxyz2[1] = bxyz1[1]*cos(ang)-bxyz1[2]*sin(ang)
  bxyz2[2] = bxyz1[1]*sin(ang)+bxyz1[2]*cos(ang)
  pang = atan(-bxyz2[1],-bxyz2[0])

  ; 2. rotate points about z by pang
  xyz1 = xyz
  xyz1[0,*] = xyz[0,*]*cos(pang)-xyz[1,*]*sin(pang)
  xyz1[1,*] = xyz[0,*]*sin(pang)+xyz[1,*]*cos(pang)

  ; 3. rotate points about x by Ahead -b0
  a_b0 = (-ap.solar_b0/!radeg)
  xyz2 = xyz1
  xyz2[1,*] = xyz1[1,*]*cos(a_b0)-xyz1[2,*]*sin(a_b0)
  xyz2[2,*] = xyz1[1,*]*sin(a_b0)+xyz1[2,*]*cos(a_b0)

  ; xyz2 are now points in cartesian heliocentric with z to Ahead
  ; 4. convert cartesians to rval (lon,lat,rad)
  rval = xyz2
  rr = sqrt(total(xyz2*xyz2,1))
  rxz = sqrt(xyz2[0,*]*xyz2[0,*]+xyz2[2,*]*xyz2[2,*])
  rval[0,*] = !radeg*atan(xyz2[0,*],xyz2[2,*])
  rval[1,*] = !radeg*atan(xyz2[1,*],rxz)
  rval[2,*] = rr

  ; 5. convert Ahead centered longitude to Carrington longitude
  rval[0,*] = rval[0,*] + ap.hgln_obs + ap.carr_earth

  return,rval

end

;+
; NAME:
;        sb_mja2pts
; PURPOSE:
;        Convert M.Aschwanden's loop coord. file into sbrowser tie point format
;        Image display for STEREO-SECCHI
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        sb_mja2pts,'mja_file',a='ahead.fts',b='behind.fts',save='tptfile.sav'
; INPUTS:
;        mja_file: string. Name of M.Aschwanden's loop coordinate file
; KEYWORDS (INPUT):
;        ahead:  string.  Name of EUVI-A FITS file used for finding loops
;        behind: string.  Name of EUVI-B FITS file used for finding loops
;           Note: ahead and behind are required!  (for S/C position)
;        savefile: string.  Save ptsdata in file "savefile", if present.
;        color: bytarr(3) or bytarr(3,n_elements(nstep)).  Color of field lines
; OUTPUTS:
;        pts : vector of tie point data structure.  1 element if new,
;              else one element is added to existing vector.
;              See sb_mkptsdata.pr for tag details
; COMMON BLOCKS:
;        pfss_data_block : ptr,ptph,ptth,nstep,now
; SIDE EFFECTS:
;        Creates file if savefile present
; RESTRICTIONS:
;        Crashes if pfss_data_block common block not properly defined!
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 5/20/2008
;-

pro sb_mja2pts,mja_file,ahead=ahead,behind=behind,savefile=sfil, $
                       color=col,nostatic=nostatic,pts=tpt_data

if n_elements(col0) eq 3 then col=byte(col0) else col=[255B,255B,255B]

; get the ahead and behind wcs structures
siz = size(ahead)
if siz[siz[0]+1] eq 7 then begin
  if strmid(ahead,0,2) eq '20' then begin    ; not a full path
     fa = file_search(ahead)                 ; in local directory?
     if fa eq '' then begin                  ; or in archive?
        path = scc_data_path('a',tel='euvi',type='img')
        fa = concat_dir(concat_dir(path,strmid(ahead,0,8)),ahead+'*')
        fa = file_search(fa)
     endif
  endif else fa=file_search(ahead)
  if fa eq '' then begin
     print,'Ahead file not found'
     return
  endif
  hh = headfits(fa)
  awcs = fitshead2wcs(hh)
endif else begin                              ; assuming it's a structure
  if tag_exist(ahead,'position') then awcs=ahead $
                                 else awcs=fitshead2wcs(ahead)
endelse
siz = size(behind)
if siz[siz[0]+1] eq 7 then begin
  if strmid(behind,0,2) eq '20' then begin   ; not a full path
     fa = file_search(behind)                ; in local directory?
     if fa eq '' then begin                  ; or in archive?
        path = scc_data_path('b',tel='euvi',type='img')
        fa = concat_dir(concat_dir(path,strmid(behind,0,8)),behind+'*')
        fa = file_search(fa)
     endif
  endif else fa=file_search(behind)
  if fa eq '' then begin
     print,'Behind file not found'
     return
  endif
  hh = headfits(fa)
  bwcs = fitshead2wcs(hh)
endif else begin                              ; assuming it's a structure
  if tag_exist(behind,'position') then bwcs=behind $
                                  else bwcs=fitshead2wcs(behind)
endelse

; read the mja file
readcol,mja_file,lnum,pnum,x_i,y_i,z_i,s_i,h_i
npnt = n_elements(lnum)
;lnumu = lnum[uniq(lnum)]
;nfeat = n_elements(lnumu)
xyz = fltarr(3,npnt)
xyz[0,*] = x_i
xyz[1,*] = y_i
xyz[2,*] = z_i

; convert xyz to rval
rval = mja2rval(xyz,awcs,bwcs)

sb_mkptsdata,tptd0
tpt_data = replicate(tptd0,npnt)
tpt_data.tt = anytim(wcs_get_time(awcs))
tpt_data.carr = 1    ; Carrington coords.
tpt_data.stat = 2    ; status: rval valid, xy not valid
tpt_data.rval = rval
if keyword_set(nostatic) then tpt_data.numf = lnum $
                        else tpt_data.numf = -lnum
tpt_data.nump = pnum
tpt_data.col  = col

fsiz = size(sfil)
if fsiz[fsiz[0]+1] eq 7 then save,tpt_data,file=sfil

return

end
