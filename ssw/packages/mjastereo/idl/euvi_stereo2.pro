pro euvi_stereo2,fileset,nsm,fov,hmax,tracefile,wave,linear
;+
; Project     : STEREO
;
; Name        : EUVI_STEREO 
;
; Category    : Data analysis  
;
; Explanation : This routine displays a selected FOV of a stereoscopic image pair.
;		The user traces manually a corresponding curvi-linear structure
;		in the two images and the routine performs the stereoscopic
;		height calculation and stores the (x,y,z) values in LOOPFILE. 
;
; Syntax      : IDL> euvi_stereo,fileset,nsm,fov,hmax,tracefile,wave
;
; Inputs      : images_pair(2,*,*) = stereo image pair B,A
;		para    = index structure with image parameters 
;		nsm	= 3, or 5; smoothing parameter for highpass filter
;		fov	= [i1,j1,i2,j2] field-of-view in image pixels 
;		hmax    = 0.1 ;range of alitudes (in solar radii)
;	        wave    = '171','195','284','304' wavelength
;		linear	= linear regression z(y) 
;
; Outputs     : tracefile 	    = output: iloop,x,y,z,...
;
; History     : Oct/Nov 2007, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

;________________________________________________________________
;			SUBIMAGE EXTRACTION 			|
;________________________________________________________________
euvi_mdi_fov,fileset,para,fov,fov_a,fov_b,image_a,image_b
dim	=size(image_a)		
nx	=dim(1)
ny	=dim(2)
crpix1  =para.crpix1
crpix2  =para.crpix2
cdelt1  =para.cdelt1
cdelt2  =para.cdelt2
a_sep   =para.sep_deg*(!pi/180.)
r_arc   =para.rsun                      ;solar radius in arcsec
r_sun   =r_arc/cdelt1                   ;solar radius in pixels
nloop	=0
loopcol	=150
loopcol2=220
zoom	=(long((2000/(2*nx)) < (1600/(2*ny))))>1
print,'Use zoom factor of ',zoom
phi	=2*!pi*findgen(361)/360.
xlimb	=cos(phi)
ylimb	=sin(phi)

print,'__________________________________________________________________'
print,'|		PLOT STEREO IMAGE PAIR  			|'
print,'__________________________________________________________________'
io	=0
char	=1
xsize	=zoom*nx*2
ysize	=zoom*ny
window,0,xsize=xsize,ysize=ysize
set_plot,'x'
!p.font=-1
ct	= 0		;IDL colortable
loadct,ct
for it=0,1 do begin
 x1_	=0.5*it
 x2_	=x1_+0.5
 y1_	=0.0
 y2_	=1.0
 if (it eq 0) then image=image_b-smooth(image_b,nsm)
 if (it eq 1) then image=image_a-smooth(image_a,nsm)
 statistic,image,avg,dev
 z2	=avg+1*dev
 z1	=avg-2*dev
 if (io eq 0) then begin
  nxw  =long(!d.x_vsize*(x2_-x1_)+0.5)
  nyw  =long(!d.y_vsize*(y2_-y1_)+0.5)
  ima  =congrid(image,nxw,nyw)
 endif
 if (io ne 0) then ima=image
 !p.position=[x1_,y1_,x2_,y2_]
 if (it eq 1) then !x.range=[fov_a(0),fov_a(2)]
 if (it eq 0) then !x.range=[fov_b(0),fov_b(2)]
 !y.range=[fov_a(1),fov_a(3)]
 !x.style=1
 !y.style=1
 plot,[0,0],[0,0]
 tv,bytscl(ima,min=z1,max=z2),x1_,y1_,xsize=(x2_-x1_),ysize=(y2_-y1_),/normal
 oplot,xlimb,ylimb
 if (it eq 0) then sc='A'
 if (it eq 1) then sc='B'
 xyouts_norm,0.02,0.02,sc,3
 !noeras=1
endfor		

print,'__________________________________________________________________'
print,'|		PLOT PREVIOUS LOOPS 				|'
print,'__________________________________________________________________'
loadct,3
file_exist=findfile(tracefile,count=nfiles)
if (nfiles ge 1) then begin 
 readcol,tracefile,iloop_,xa_,ya_,za_,r_,xb_,yb_,zb_
 nloop	=long(max(iloop_))+1
 print,'Number of loops  NLOOP   ='+string(nloop,'(i3)')
 for iloop=0,nloop-1 do begin 
  ind	=where(iloop_ eq iloop,np) 
  xa	=xa_(ind)
  ya	=ya_(ind)
  za	=za_(ind)
  xb	=xb_(ind)
  yb	=yb_(ind)
  zb	=zb_(ind)
  spline_p,xa,ya,xxa,yya
  spline_p,xb,ya,xxb,yyb

  !x.range=[fov_a(0),fov_a(2)]			;STEREO A loops 
  !p.position=[0.5,0,1.,1] 
  plot,xxa,yya,color=loopcol,thick=4 		 

  !x.range=[fov_b(0),fov_b(2)]			;STEREO B loops 
  !p.position=[0,0,0.5,1] 
  plot,xxb,yyb,color=loopcol,thick=2
 endfor
endif
if (nfiles eq 0) then print,'No previous tracefile found';

print,'__________________________________________________________________'
print,'|		DRAWING NEW LOOP IN STEREO A			|'
print,'__________________________________________________________________'
!p.position=[0.5,0,1,1]
!x.range=[fov_a(0),fov_a(2)]
!y.range=[fov_a(1),fov_a(3)]
plot,[0,0],[0,0]
iloop   =nloop
print,'Trace loop #'+string(iloop+1,'(i4)')
print,' Click loop positions in A [right panel] (mark end in left panel)'
npos    =100
xa      =fltarr(npos)   
ya      =fltarr(npos)   
for i=0,npos-1 do begin
 cursor,xa0,ya0
 wait,0.2
 if (xa0 lt fov_a(0)) then goto,endloop
 xa(i)	=xa0
 ya(i)  =ya0
 oplot,xa(0:i),ya(0:i),psym=1
 print,'read coordinates x,y=',xa0,ya0
endfor
endloop:
np      =i
xa	=xa(0:np-1)
ya	=ya(0:np-1)
spline_p,xa,ya,x,y
n       =n_elements(x)
oplot,x,y,color=loopcol,thick=4
xyouts,x(n-1),y(n-1),string(iloop,'(i3)'),size=1,color=loopcol

print,'__________________________________________________________________'
print,'|		TRACING CORRESPONDING LOOP IN STEREO B		|'
print,'__________________________________________________________________'
!p.position=[0,0,0.5,1]
!x.range=[fov_b(0),fov_b(2)]
plot,[0,0],[0,0]
print,'Click epipolar x-positions in image B [left panel] :'
za	=fltarr(np)
r  	=fltarr(np)
xb	=fltarr(np)
yb	=fltarr(np)
zb	=fltarr(np)
dh	=0.0005				;altitude resolution = 0.5 arcsec
oplot,!x.range,min(ya)*[1,1],color=loopcol2
oplot,!x.range,max(ya)*[1,1],color=loopcol2
for i=0,np-1 do begin
 rmin	=(sqrt(xa(i)^2+ya(i)^2)+dh/2.) > 1. 
 rmax	=(1.+hmax)>(rmin+100*dh) 
 nh	=long((rmax-rmin)/dh)>1		;number of altitude steps
 r_	=rmin+dh*findgen(nh)
 xb_h	=fltarr(nh)
 for ih=0,nh-1 do begin
  euvi_stereo_rot1,xa(i),ya(i),r_(ih),a_sep,xb_ih
  xb_h(ih)=xb_ih	
 endfor
 oplot,xb_h,fltarr(nh)+ya(i),color=loopcol2,thick=3
 oplot,xb_h(0)*[1,1],ya(i)*[1,1],color=loopcol2,psym=1,thick=3,symsize=3
 cursor,xbi,ybi
 wait,0.2
 xb(i) =xbi
 yb(i) =ya(i)
 dmin  =min(abs(xb_h-xbi),imin)
 r(i)  =r_(imin)
 za(i) =sqrt(r(i)^2-xa(i)^2-ya(i)^2)
 zb(i) =sqrt(r(i)^2-xb(i)^2-yb(i)^2)
 oplot,xb(i)*[1,1],yb(i)*[1,1],psym=4,symsize=3,thick=3,color=loopcol
 if (i ge 1) then oplot,xb(0:i),yb(0:i),thick=2,linestyle=2,color=loopcol
 print,'Stereoscopic height  h=',(r(i)-1.)*696.,' Mm'
endfor
spline_p,xb,yb,x,y
oplot,x,y,color=loopcol,thick=4
read,'Save loop ? [0,1] = ',save

print,'__________________________________________________________________'
print,'|		SAVE LOOP COORDINATES 				|'
print,'__________________________________________________________________'
if (save eq 1) then begin

 if (linear eq 1) then begin		;linear regression of z(y)-coordinate
  c      =linfit(ya,za)
  za     =c(0)+c(1)*ya
  r      =sqrt(xa^2+ya^2+za^2)
  zb     =sqrt(r^2-xb^2-yb^2)
 endif

 openw,1,tracefile,/append
 for i=0,np-1 do printf,1,iloop,xa(i),ya(i),za(i),r(i),xb(i),yb(i),zb(i),$
  format='(i6,7f10.4)'
 close,1
 print,'nloop = '+string(iloop+1,'(i3)')+' file1 = '+tracefile

 ipos=strpos(tracefile,'trace.dat')
 xyzfile=strmid(tracefile,0,ipos)+'xyz.dat'
 openw,2,xyzfile,/append	;used in sb_mja2pts.pro 
 si	=0.
 for i=0,np-1 do begin
  hi	=(r(i)-1.)*r_sun
  if (i ge 1) then si=s0+sqrt((xa(i)-x0)^2+(ya(i)-y0)^2+(za(i)-z0)^2)*696.
  printf,2,iloop,i,xa(i),ya(i),za(i),si,hi,format='(2i6,5f10.4)'
  x0	=xa(i)
  y0	=ya(i)
  z0	=za(i)
  s0	=si
 endfor
 close,2
 print,'nloop = '+string(iloop+1,'(i3)')+' file2 = '+xyzfile
endif
end
