pro tracing_direction,subimage,nlen,step,idir,k,i1,j1,x0,y0,alpha,ifit,jfit,ang0,a1,a2,alpha00,qfill

;	This routine is called by TRACING_STPE.PRO and TRACING_AUTO.PRO 

;range of directivity vector_____________________________________
alpha0	=alpha					;old alpha value endif 
x	=x0-i1
y	=y0-j1
if (idir eq 0) then begin
 ir1	=-(nlen/2-step)
 ir2	=nlen/2
endif
if (idir eq 1) then begin
 ir1	=-nlen/2
 ir2	=nlen/2-step
endif
nr	=ir2-ir1+1
r	=ir1+findgen(nr)

;polarization direction__________________________________________
a1	=0.
a2	=!pi
da	=!pi*(ang0/180.)
if (k ge 1) then begin
 a1	=alpha0+(alpha0-alpha00)-da/2.	;second order extrapolation
 a2	=alpha0+(alpha0-alpha00)+da/2.
endif
nphi	=long((a2-a1)*(180/!pi))
phi	=a1+(a2-a1)*findgen(nphi)/float(nphi)
flux_phi=fltarr(nphi)
peak_phi=fltarr(nphi)
for i=0,nphi-1 do begin
 x_	=x+r*cos(phi(i))
 y_	=y+r*sin(phi(i))
 flux   =subimage(long(x_+0.5),long(y_+0.5))
 flux_phi(i)=total(flux)/nr
 peak_phi(i)=max(flux)>0.
endfor
flux_avg=max(flux_phi,im) >0.
flux_peak=peak_phi(im) >0.
qfill	=0.
if (flux_peak gt 0) then qfill=flux_avg/flux_peak
alpha	=phi(im)

;180-degree ambuiguity____________________________________________
if (k ge 1) or (idir eq 1) then begin
 alpha_	=alpha+[0.,-!pi,!pi]
 dalpha	=min(abs(alpha_-alpha0),im)
 alpha	=alpha_(im)
endif
ifit	=x+r*cos(alpha)
jfit	=y+r*sin(alpha)
y0	=y+j1
end

