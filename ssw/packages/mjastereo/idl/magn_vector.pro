pro magn_vector,x,y,z,ds,coeff,polar,x2,y2,z2,bx,by,bz,b_tot,endv,alpha
;+
; Project     : SOHO/MDI, SDO/HMI, STEREO
;
; Name        : MAGN_VECTOR 
;
; Category    : Magnetic data modeling
;
; Explanation : Subroutine in support of MAGN_FIELDLINE.PRO,
;               calculting a single magnetic field vector at position [X,Y,Z]
;
; Syntax      : IDL>magn_vector,x,y,z,ds,coeff,polar,$
;		x2,y2,z2,bx,by,bz,b_tot,endv,alpha
;
; Inputs      : x       = x-coordinate of start of field vector 
;               y       = y-coordinate of start of field vector
;               z       = z-coordinate of start of field vector
;               ds      = spatial resolution along field line
;               coeff   = magnetic model coefficients [b1,x1,y1,z1,a1]
;		polar	= magnetic polarity or sign of direction [-1,+1]
;
; Outputs     ; x2	= second x-coordinate of 3D field vector [X,X2]
;               y2	= second y-coordinate of 3D field vector [Y,Y2]
;               z2	= second y-coordinate of 3D field vector [Z,Z2]
;               bx	= Bx component at position [X,Y,Z]
;               by	= By component at position [X,Y,Z]
;               bz	= Bz component at position [X,Y,Z]
;               b_tot	= absolute value of magnetic field strength B at [X,Y,Z]
;		endv	= 1=end of vector condition (below photosphere) 
;		alpha   = analytica expression of force-free alpha parameter 
;			  at [X,Y,Z]
;
; History     : 1-Jun-2011, Version 1 written by Markus J. Aschwanden
;               30-Jan-2012, Version 2 released to SSW
;		 6-Feb-2012, More accurate term for alpha
;
; Contact     : aschwanden@lmsal.com
;-

;____________________Potential field................................
dim     =size(coeff)
ncomp   =dim(1)
endv	=0.
bx   	=0.
by   	=0.
bz   	=0.
eps	=1.e-6
ab_sum	=0.
b_sum	=0.
for j=0,ncomp-1 do begin
 b_j    =coeff(j,0)
 x_j    =coeff(j,1)
 y_j    =coeff(j,2)
 z_j    =coeff(j,3)                     ;negative height
 rm     =sqrt(x_j^2+y_j^2+z_j^2)
 d_j    =1.-rm
 rj_	=[x-x_j,y-y_j,z-z_j]
 rj 	=sqrt(total(rj_*rj_))
 b_p    =b_j*(d_j/rj)^2

;....................nonpotential component.........................
 a_j	=coeff(j,4)
 b	=a_j/2.
 v_axis	=[x_j,y_j,z_j]
 vector_product,rj_,v_axis,r_phi,cos_phi,theta
 cos_r	=rj_/rj
 term	=b*rj*sin(theta)
 b_r	=b_p/(1.+term^2)
 b_phi	=b_r*term 				
 bx     =bx+b_r*cos_r(0)+b_phi*cos_phi(0)
 by     =by+b_r*cos_r(1)+b_phi*cos_phi(1)
 bz     =bz+b_r*cos_r(2)+b_phi*cos_phi(2)
 cos_th	=cos(theta)
 al	=-2.*b*cos_th/(1.+term^2)		;negative sign ???
 if (abs(b_r) lt abs(b_phi)) then al=al*(cos_th^2+term^2) ;Febr 6, 2012
 ab_sum	=ab_sum+b_p*al
 b_sum	=b_sum +b_p
endfor
b_	=[bx,by,bz]
b_tot	=0.
b2      =total(b_^2)
if (b2 gt 0.) then b_tot=sqrt(b2)
alpha	=(ab_sum/b_sum)

;......................incremental step along field line............
x2	=x
y2	=y
z2	=z
if (b_tot ne 0.) then begin
 x2	=x + ds*(bx/b_tot)*polar 
 y2	=y + ds*(by/b_tot)*polar 
 z2	=z + ds*(bz/b_tot)*polar 
 r2	=sqrt(x2^2+y2^2+z2^2)
 if (r2 le 1.) then endv=1
endif
end

