pro nlfff_disp2,dir,savefile,io
;+
; Project     : SDO/HMI, AIA
;
; Name        : NLFFF_FIELD2
;
; Category    : Magnetic data modeling
;
; Explanation : Display magnetogram and field lines FIELD_LINES_NEW[NS,5,NF]
;		in a grid of footpoints
;
; Syntax      : IDL>nlfff_field2,dir,savefile,io
;
; Inputs      : dir,savefile
;		io      =0 screen, 1=ps-file, 2=eps-file, 3=color file
;
; Outputs     ; postscript file (io=1,2,3)
;
; History     : 17-JUL-2015, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

print,'__________________NLFFF_DISP2_________________________________'
savefiles=findfile(dir+savefile,count=nfiles)
if (nfiles le 0) then begin
 savefiles=findfile(dir+'*.sav',count=nfiles)
 if (nfiles eq 0) then stop,'No file exists: *.sav'
endif
isel    =0
if (nfiles ge 2) then begin
 for i=0,nfiles-1 do print,i,'  ',savefiles(i)
 read,'Enter number of savefile = ',isel
endif
dir_savefile=savefiles(isel)
nlen	=strlen(dir_savefile)
plotname=strmid(dir_savefile,0,nlen-4)+'_field'

print,'__________________NLFFF_FIELD2_________________________________'
restore,dir+savefile
print,'reading savefile = ',dir+savefile

fov0    =input.fov0      ;field-of-view
x1_sun  =para.x1_sun
x2_sun  =para.x2_sun
y1_sun  =para.y1_sun
y2_sun  =para.y2_sun
hmax    =0.05                   ;maximum altitude (solar radii)
nseg    =9                      ;number of loop segments (misalignment angles)
ds      =0.002                  ;spatial resolution along loop
nloop   =10000                  ;loop array limit
nsmax   =long(fov0/ds)
dim     =size(bzfull)
nx      =dim(1)
ny      =dim(2)
dpix    =float(x2_sun-x1_sun)/float(nx-1)
x       =x1_sun+dpix*findgen(nx)
y       =y1_sun+dpix*findgen(ny)
nf      =n_elements(ns_loop)

dim     =size(bzfull)
nx      =dim(1)
ny      =dim(2)
dpix    =float(x2_sun-x1_sun)/float(nx-1)
x       =x1_sun+dpix*findgen(nx)
y       =y1_sun+dpix*findgen(ny)

PLOT: ;_______________________PLOT____________________________________
x1      =para.x1_sun
x2      =para.x2_sun
y1      =para.y1_sun
y2      =para.y2_sun
dpix_mag=para.dpix_mag
dim     =size(bzfull)
nx      =dim(1)
ny      =dim(2)
x       =x1+dpix_mag*findgen(nx)
y       =y1+dpix_mag*findgen(ny)
nf      =n_elements(ns_loop)

;DISPLAY MAGNETOGRAM
form    =1      ;0=landscape, 1=portrait
char    =1.     ;character size
fignr	=''
ref     =''     ;label at bottom of Fig.
unit    =0      ;window number
ct      =5      ;color table
blue    =50     ;blue color
red     =110    ;red color
yellow  =200    ;yellow color

fig_open,io,form,char,fignr,plotname,unit
x1_   =0.1
x2_   =0.9
y2_   =0.80
y1_   =0.20
!p.position=[x1_,y1_,x2_,y2_]
!p.title=savefile
!x.range=[x[0],x[nx-1]]
!y.range=[y[0],y[ny-1]]
!x.title='East-West distance [solar radii]'
!y.title='North-South distance [solar radii]'
!x.style=1
!y.style=1
if (io eq 0) then begin
 nxw   =long(!d.x_vsize*(x2_-x1_)+0.5)
 nyw   =long(!d.y_vsize*(y2_-y1_)+0.5)
 image =congrid(bzfull,nxw,nyw)                 ;magnetogram full res
;image =congrid(bzmap,nxw,nyw)                  ;magnetogram low res
endif
if (io ne 0) then image=bzfull                  ;bzmap full res
;if (io ne 0) then image=bzmap                  ;bzmap low res
loadct,1
plot,[x(0),x(nx-1),x(nx-1),x(0),x(0)],[y(0),y(0),y(ny-1),y(ny-1),y(0)]
statistic,bzfull,c0,csig
c1      =c0-5*csig
c2      =c0+1*csig
tv,bytscl(image,min=c1,max=c2),x1_,y1_,xsize=(x2_-x1_),ysize=(y2_-y1_),/normal
dlat  =5
coordsphere,1,0,0,0,0,dlat
!noeras=1

;______________________DISPLAY FIELD LINES_______________________
loadct,5,/silent
if (io eq 0) then th=1
if (io gt 0) then th=2
nf_grid      =n_elements(ns_field_grid)
for k=0,nf_grid-1 do begin
 np     =ns_field_grid[k]
 xl     =field_lines_grid(0:np-1,0,k)
 yl     =field_lines_grid(0:np-1,1,k)
 oplot,xl,yl,color=red,thick=th
endfor
fig_close,io,fignr,ref
end

