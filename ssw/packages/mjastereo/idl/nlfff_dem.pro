pro nlfff_dem,dir,efile,iev,dateobs_,fov_,tmargin,dt,wave_,teem_table,vers,test1
;+
; Project     : AIA/SDO
;
; Name        : NLFF_DEM
;
; Category    : Data analysis   
;
; Explanation : reads AIA images, measures flare area, DEM analysis
;
; Syntax      : IDL>nlfff_dem,
;
; Inputs      : dir	 = work directory 
;
; Outputs     : 
;
; History     : 11-Mar-2014, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

print,'__________________NLFFF DEM____________________________________
nwave   =n_elements(wave_)
nt	=n_elements(dateobs_)
it1	=long(tmargin/dt)		;time step at start, background subtr

time_t	=strarr(nt,nwave)
texp_t	=fltarr(nt,nwave)
flux_t	=fltarr(nt,nwave)
area_t	=fltarr(nt,nwave)
for iw=0,nwave-1 do begin
 wave   =wave_[iw]
 first	=0
 for it=0,nt-1 do begin
  x1	=fov_[0,it]
  y1	=fov_[1,it]
  x2	=fov_[2,it]
  y2	=fov_[3,it]
  filename  =dateobs_[it]
  imagefile=dir+filename+'_'+wave_[iw]+'_euv.fits'
  file	 =findfile(imagefile,count=count)
  if (count eq 0) then  nlfff_AIA,dir,filename,wave_[iw],test1
  file	 =findfile(imagefile,count=count)
  print,it,iw,'   reading : ',imagefile
  if (count eq 0) then goto,skip_read
  data   =readfits(imagefile,h,/silent)
  dateobs=sxpar(h,'DATE_OBS')
  texp	 =sxpar(h,'EXPTIME')
  cdelt1 =sxpar(h,'CDELT1')
  crpix1 =sxpar(h,'CRPIX1')
  crpix2 =sxpar(h,'CRPIX2')
  eph    =get_sun(dateobs)
  rsun   =eph(1)                 ;rsun=index.rsun_obs in arcseconds
  rpix   =rsun/cdelt1
  dpix   =1./rpix
  dx_euv =cdelt1/rsun           ;pixel size in solar radii
  i1     =long(crpix1+x1/dpix+0.5)
  i2     =long(crpix1+x2/dpix+0.5)
  j1     =long(crpix2+y1/dpix+0.5)
  j2     =long(crpix2+y2/dpix+0.5)
  nx     =(i2-i1+1)
  ny     =(j2-j1+1)
  image  =float(data(i1:i2,j1:j2))/texp			;DN/s
  if (it eq   0) or (first eq 0) then begin
   back   =image
   backmax=image
   first  =1
  endif
  if (it le it1) then begin
   back   =back < image
   backmax=backmax > image 
  endif
  if (it ge it1) then begin
   nsm   =5
   image_smo=smooth(image-back,nsm)
   peak  =max(image_smo)				
   thresh=peak/2.					;FWHM
   inda	 =where((image-back) ge thresh,npix_area)
   flux_t[it,iw]=total(image-back) > 0
   area_t[it,iw]=npix_area         > 1
  endif
  time_t[it,iw]=dateobs 
SKIP_READ:
 endfor
endfor

;____________________FLARE GEOMETRY_________________________________
restore,teem_table      ;-->wave_,q94,area,resp_corr,telog,dte,tsig,flux,pix
rad	=fltarr(nt)
area	=fltarr(nt)
vol	=fltarr(nt)
for it=it1,nt-1 do begin
 area[it]=median(area_t(it,*))			;median area in pixels
 rad[it]=sqrt(area[it])
 vol[it]=area[it]^(3./2.)
endfor

;________________DEM+TE ANALYSIS____________________________________
qnoise	=0.1
qfree    =float(nwave)/float(nfree)
nte     =n_elements(telog)
nsig    =n_elements(tsig)
ntot    =nte*nsig
te_prof =fltarr(nt)
em_prof =fltarr(nt)
sig_prof=fltarr(nt)
chi_prof=fltarr(nt)
flux_prof=fltarr(nt,nwave)
unce_prof=fltarr(nt,nwave)
fit_prof =fltarr(nt,nwave)
nfree   =3
istep	=0
for it=it1,nt-1 do begin
 if (area[it] le 0) then goto,skip_time
 flux_obs=(reform(flux_t[it,*])/area[it])>0.            ;total flux [DN/s pix]
 unce_obs=flux_obs*qnoise*qfree 
 flux_prof[it,*]=flux_obs*area[it]  
 unce_prof[it,*]=unce_obs*area[it]  
 te_	=fltarr(nte,nsig)
 tsig_	=fltarr(nte,nsig)
 chi_	=fltarr(nte,nsig)
 indw	=where((flux_obs gt 0) and (wave_ ne 304),nindw)
 if (nindw le nfree) then goto,skip_time
 chi_best=99.
 for k=0,nte-1 do begin
  te_k	=10.^telog(k) 
  for l=0,nsig-1 do begin
   flux_dem1=reform(flux(k,l,*))			;flux in 7 wavelengths
;  em1	=median(flux_obs/flux_dem1)			
   em1	=total(flux_obs)/total(flux_dem1)			
   flux_dem=flux_dem1*em1
   flux_total=flux_dem*area[it]
   te_(k,l)=te_k
   tsig_(k,l)=tsig(l)
   f_model=flux_total(indw)
   f_obs  =reform(flux_prof[it,indw])
   f_sig  =reform(unce_prof[it,indw])
   chi_(k,l)=sqrt(total(((f_model-f_obs)/f_sig)^2)/(nindw-nfree))  
   if (chi_(k,l) lt chi_best) then begin
    k_best=k
    l_best=l
    te_best=te_k
    sig_best=tsig(l)
    chi_best=chi_(k,l)
   endif
  endfor
 endfor

;CORRESPONDING values in EM and CHI......................................
 flux_dem1=reform(flux(k_best,l_best,*))		;flux in 7 wavelengths
 em1	  =median(flux_obs/flux_dem1)			
 flux_dem =flux_dem1*em1
 flux_total=flux_dem*area[it]
 em_best=em1*area[it]
 sig_dte=sqrt(2.*!pi)*te_best*(10.^sig_best-1.)  

;TIME PROFILE of Te, Tsig, EM, Chi.......................................
 em_prof(it)=alog10(em_best)+alog10(sig_dte)+2.*alog10(pix)     ;log(EM)
 te_prof(it)=te_best					;weighted mean TE
 sig_prof(it)=sig_best					;Gaussian width T [K]
 chi_prof(it)=chi_best
 fit_prof[it,0:nwave-1] =flux_dem[0:nwave-1]*area[it]
 istep	=istep+1
 SKIP_TIME:
endfor

;____________________ELECTRON DENSITY___________________________
ne_prof =fltarr(nt)
kb	=1.3807e-16 	;erg/K
rad_cm 	=rad*pix	;cm
vol_cm	=rad_cm^3.
ind	=where((em_prof gt 0) and (vol_cm gt 0))
ne_prof(ind) =10.^(0.5*(em_prof(ind)-alog10(vol_cm(ind))))

;____________________THERMAL ENERGY_____________________________
eth_prof =fltarr(nt)
kb	=1.3807e-16 	;erg/K
eth_prof =3.*ne_prof*kb*te_prof*vol_cm
print,'Maximum thermal energy  E_th=',max(eth_prof),' erg'

;________________SAVE PRIMARY PARAMETERS________________________
filename  =dateobs_[0]
savefile=dir+filename+'_dem_'+vers+'.sav'
save,filename=savefile,$
    dateobs_,wave_,nt,time_t,flux_t,time_t,texp_t,em_prof,te_prof,$
    ne_prof,eth_prof,sig_prof,chi_prof,rad_cm,vol_cm,flux_prof,fit_prof
print,'Parameters saved in file = ',savefile
end
