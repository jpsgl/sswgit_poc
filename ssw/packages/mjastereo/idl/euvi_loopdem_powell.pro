function euvi_loopdem_powell,coeff

common powell_para,f1,f2,f3,dfit,dx0,dxg

nw	=n_elements(f1)
x0	=(coeff(0)>(nw/2-dx0))<(nw/2+dx0) 
xg	=(coeff(1)>0.5)<dxg
y1	=coeff(2)>0
y2	=coeff(3)>0
y3	=coeff(4)>0
b1	=coeff(5)>0
b2	=coeff(6)>0
b3	=coeff(7)>0
db1	=coeff(8)
db2	=coeff(9)
db3	=coeff(10)

x	=findgen(nw)
back1	=b1+db1*(x-x0)
back2	=b2+db2*(x-x0)
back3	=b3+db3*(x-x0)
flux1	 =back1+y1*exp(-(x-x0)^2/(2.*xg^2))
flux2	 =back2+y2*exp(-(x-x0)^2/(2.*xg^2))
flux3	 =back3+y3*exp(-(x-x0)^2/(2.*xg^2))
ib1	=nw/2-dfit
ib2	=nw/2+dfit
f_model	=[flux1(ib1:ib2),flux2(ib1:ib2),flux3(ib1:ib2)]
f_obs	=[  f1(ib1:ib2),  f2(ib1:ib2),  f3(ib1:ib2)]
s_obs	=sqrt(f_obs)
chi2	=total(((f_model-f_obs)/s_obs)^2)
nobs	=n_elements(f_obs)
ncoeff	=n_elements(coeff)
n_free	=nobs-ncoeff
chi2_red=chi2/n_free
dev	=sqrt(chi2_red)
return,dev
end
