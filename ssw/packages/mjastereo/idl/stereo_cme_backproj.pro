io	=0	;0=X-window, 1=ps, 2=eps, 3=color ps
form	=1	;0=landscape, 1=portrait
char	=0.6	;character size 
fignr	='9c'  	;figure number
plotname='oxnard_f'	;filename
ref	=' (Aschwanden et al. 2002)'  ;label at bottom of Fig.	
unit	=0	;window number
angle	=10

;Density distribution model
nx	=512
ny	=nx
wid	=nx/10.
x	=findgen(nx)
y	=findgen(ny)
model	=fltarr(nx,ny)
ns	=5
x0	=nx*[0.1,0.3,0.5,0.6,0.65]
y0	=ny*[0.7,0.65,0.6,0.4,0.2]
a0	=1.*[0.5,0.8,1.0,0.85,0.75]
wx	=nx*[0.1,0.12,0.1,0.08,0.05]
wy	=nx*[0.1,0.12,0.1,0.09,0.08]
for is=0,ns-1 do begin 
 for j=0,ny-1 do model(*,j)=model(*,j)+a0(is)*exp(-((x-x0(is))^2/(2.*wx(is)^2)+(y(j)-y0(is))^2/(2.*wy(is)^2)))
endfor

;STEREO-A,B view 
STEREO:
stereoa =fltarr(nx)
for j=0,ny-1 do stereoa(*)=stereoa(*)+model(*,j)

stereob =fltarr(nx)
if (angle mod 180) eq 0 then stereob=stereoa
if (angle mod 180) ne 0 then begin
 modelb	=rot(model,angle)
 x_	=fltarr(nx)+x(ny/2)
 y_	=y
 rotate1,x_,y_,xb,yb,nx/2,ny/2,angle
 for i=0,nx-1 do stereob(i)=total(modelb(i,*))
endif

;3D-RECONSTRUCTION
recon	=fltarr(nx,ny)
a	=(angle/180.)*!pi
y	=findgen(ny)
yy	=ny/2+(findgen(ny)-ny/2)/tan(a)
stretchb=interpol(stereob,y,yy)
for i=0,nx-1 do begin
 ishift=float(i-nx/2.)/tan(a)
 recon(i,*)=stereoa(i)*shift(stretchb/total(stretchb),-ishift)
 ibound=(abs(ishift)<(ny-1))>1
 if (i le nx/2) then recon(i,0:ibound)=0
 if (i ge nx/2) then recon(i,ny-ibound:ny-1)=0
endfor
recon=recon*total(model)/total(recon)  ;renormalization

stereoa_ =fltarr(nx)
for j=0,ny-1 do stereoa_(*)=stereoa_(*)+recon(*,j)

stereob_ =fltarr(nx)
if (angle mod 180) eq 0 then stereob_=stereoa_
if (angle mod 180) ne 0 then begin
 modelb_=rot(recon,angle)
 x_	=fltarr(nx)+x(ny/2)
 y_	=y
 rotate1,x_,y_,xb,yb,nx/2,ny/2,angle
 for i=0,nx-1 do stereob_(i)=total(modelb_(i,*))
endif

;DISPLAY
fig_open,io,form,char,fignr,plotname,unit 
!p.position=[0.1,0.7,0.45,0.95]
!p.title='Model density distribution n(x,z)'
!p.charsize=char
!x.range=[0,nx]
!y.range=[0,ny]
!x.style=1
!y.style=1
nlevel	=10
level	=max(model)*(findgen(nlevel)+1.)/float(nlevel)
contour,model,x,y,level=level
oplot,[1,1]*(nx/2),[0,ny]
xyouts,nx/2.+10,10,'A',size=char
oplot,xb,yb
xyouts,xb(0),yb(0),'B',size=char
!noeras=1

!p.position=[0.1,0.4,0.45,0.65]
!p.title='STEREO A LOS_integration  n(x)'
!y.range=[0,max(stereoa)]
plot,x,stereoa,thick=3
oplot,x,stereoa_,thick=3,linestyle=1
oplot,[1,1]*(nx/2),!y.range
xyouts,nx/2.+10,10,'A',size=char

!p.position=[0.55,0.4,0.9,0.65]
!p.title='STEREO B LOS_integration  n(x_rot)'
!y.range=[0,max(stereob)]
plot,x,stereob,thick=3
oplot,x,stereob_,thick=3,linestyle=1
oplot,[1,1]*(nx/2),!y.range
xyouts,nx/2.+10,10,'B',size=char

!p.position=[0.1,0.1,0.45,0.35]
!p.title='Reconstructed density distribution n(x,z)'
!p.charsize=char
!x.range=[0,nx]
!y.range=[0,ny]
!x.style=1
!y.style=1
contour,recon,x,y,level=level

!p.position=[0.55,0.1,0.9,0.35]
!p.title='Difference [recon-model]'
!p.charsize=char
!x.range=[0,nx]
!y.range=[0,ny]
!x.style=1
!y.style=1
contour,recon-model,x,y,level=level
contour,model-recon,x,y,level=level,c_linestyle=1

fig_close,io,fignr,ref
end
