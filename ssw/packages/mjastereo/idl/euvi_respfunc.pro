pro euvi_respfunc,instr_,wave_

print,'READ OBSERVED TIME PROFILES________________________________________'
ninstr	=n_elements(instr_)
goes	=0
euvi	=0
xrt	=0
eis	=0
nte	=101
te_log	=5.00+0.02*findgen(nte)
te_	=10.^te_log
resp	=fltarr(nte,ninstr)
te_resp	=fltarr(ninstr)
resp_max=fltarr(ninstr)
sat	=10
sc	='a'

for in=0,ninstr-1 do begin
 wave	=wave_(in)
 instr	=instr_(in)

 if (instr eq 'GOES') then begin
  goes=1
  te_6	=10.^(te_log-6.)
  em_49 =fltarr(nte)+10.^(44.-49.)	;EM in units 10^44
  goes_fluxes,te_6,em_49,flong,fshort,date=dateobs,satellite=sat
  if (wave eq '1-8'  ) then resp(*,in)=flong
  if (wave eq '0.5-4') then resp(*,in)=fshort
 endif

 if (instr eq 'EUVI') then begin
  euvi=1
  dir_euvi ='~/idl_stereo/'
  respfile=concat_dir('$SSW_MJASTEREO','idl/euvi_response.dat')
  readcol,respfile,t_mk,r1_a,r2_a,r3_a,r4_a,r1_b,r2_b,r3_b,r4_b
  n      =n_elements(t_mk)
  temp_log=alog10(t_mk)+6.0
  if (sc eq 'a') and (wave eq '171') then resp_euvi=r1_a 
  if (sc eq 'a') and (wave eq '195') then resp_euvi=r2_a 
  if (sc eq 'a') and (wave eq '284') then resp_euvi=r3_a 
  if (sc eq 'b') and (wave eq '171') then resp_euvi=r1_b 
  if (sc eq 'b') and (wave eq '195') then resp_euvi=r2_b 
  if (sc eq 'b') and (wave eq '284') then resp_euvi=r3_b 
  ind_te=where(resp_euvi gt 0) &resp_min=min(resp_euvi(ind_te))
  resp(*,in)=interpol(resp_euvi,temp_log,te_log)>resp_min
 endif

 if (instr eq 'XRT') then begin
  xrt=1
  resp_xrt=calc_xrt_temp_resp()
  ichan=where(resp_xrt.channel_name eq wave)
  if (ichan eq -1) then print,'No XRT channel found for ',wave
  resp_wave=resp_xrt(ichan).temp_resp   ;DN/pixel cm-5 s-1 
  temp_log =resp_xrt(ichan).temp
  ind_te=where(resp_wave gt 0) &resp_min=min(resp_wave(ind_te))
  em44	=resp_wave*1.e24  ;vol=(1.e8 cm)^3, n=10^10 cm-3==>EM=10^44 cm-6
  resp(*,in)=interpol(em44(ind_te),temp_log(ind_te),te_log)>resp_min
 endif

 if (instr eq 'EIS') then begin
  eis=1
  dir_eis='~/paper_hinode/EIS'
  search = dir_eis+'/*'+wave+'*.genx'
  respfile=findfile(search)
  restgen,log_t,log_n0,emiss,lambda,abund,nH_ne,abund_ref,file=respfile(0)
  ind_te=where(emiss gt 0) &resp_min=min(emiss(ind_te))
  em44	=emiss*10.^(44-2.*log_n0)
  resp(*,in)=interpol(em44(ind_te),log_t(ind_te),te_log)>resp_min
 endif

 resp_max(in)=max(resp(*,in),imax)
 te_resp(in)=te_(imax)
endfor

print,'DISPLAY____________________________________________________________'
for io=0,3,3 do begin
form    =1      ;0=landscape, 1=portrait
char    =1.     ;character size
if (io ge 1) then char=0.7
plotname='respfunc' ;filename
fignr	 =''
ref     =''     ; (Aschwanden et al. 2008)'  ;label at bottom of Fig.
unit    =0      ;window number
ct	=5
offset	=[0,-1.e6,0,0,0,0,0,-1.e5,0,0,0]

fig_open,io,form,char,fignr,plotname,unit
loadct,ct
!p.position=[0.1,0.3,0.9,0.7]
!x.title='Temperature T[K]'
!x.range=[1.e5,1.e7]
!y.range=[1.e-4,10]
plot_oo,[1,1]
xlabel	=!x.range(0)
for in=0,ninstr-1 do begin
 col	=255*float(1+in)/float(ninstr+4)
 instr	=instr_(in)
 wave	=wave_(in)
 if (strmid(instr,0,3) ne 'XRT') then wave=wave+' A'
 tit	=instr+' '+wave
 units	='DN/s'
 if (instr eq 'GOES') then units='W m!U-2!N' 

 oplot,te_,(resp(*,in)/resp_max(in))*(1./in),color=col,thick=3
 xlabel=te_resp(in)+offset(in)
 xyouts,xlabel,(1./in)*1.1,instr+' '+wave,orientation=90,color=col
 !noeras=1
endfor
fig_close,io,fignr,ref
endfor

end
