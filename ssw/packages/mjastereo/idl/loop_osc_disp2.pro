pro loop_osc_disp2,io,fignr,plotname,coordfiles,fov,qmid,delta,qfov,period,ifile,ct

form	=1			;0=landscape, 1=portrait
char	=1.0			;charsize
unit	=0			;window
ref	=''

;READ DATA AND FITS__________________________________________________
nt	=n_elements(coordfiles)
restore,coordfiles(0)  ;-->index,coeff,xa,xb,xa_,xb_,xa_sun,ya_sun,xb_sun
xsun	=xa(*,0)
ysun	=xa(*,1)
zsun	=xa(*,2)
xcirc	=xa_(*,0)
ycirc	=xa_(*,1)
zcirc	=xa_(*,2)
range	=(max(xsun)-min(xsun))>(max(ysun)-min(ysun))
x0	=(min(xsun)+max(xsun))/2.
y0	=(min(ysun)+max(ysun))/2.
z0	=(min(zsun)+max(zsun))/2.
rsun	=1.
pos	=0.
crota2  =0.
blong	=0.
blat	=0.
dlat	=5.
l1	=coeff(0)	
for j=0,2 do if (l1 ge 180.) then l1=l1-360.
b1	=coeff(1)
base	=coeff(2)*696. ;Mm
h0	=coeff(3)*696. ;Mm
az	=coeff(4)
th	=coeff(5)
rloop	=sqrt((base/2.)^2+h0^2)
tosc	=fltarr(nt)
xosc	=fltarr(nt)
yosc	=fltarr(nt)
zosc	=fltarr(nt)
dateobs =index.date_obs
t0	=anytim(dateobs,/second) mod 86400
thi	=1
if (nt eq 1) then thi=5

;(X,Y)-PROJECTION ________________PANEL 1____________________________
fig_open,io,form,char,fignr,plotname,unit
loadct,ct
x1_	=0.1
x2_	=0.5
y1_	=0.2
y2_	=0.5
!p.position=[x1_,y1_,x2_,y2_]
!p.title=' '
!x.title='x [solar radii]'
!y.title='y [solar radii]'
!x.range=x0+range*[-1,1]*qfov
!y.range=y0+range*[-1,1]*qfov
!x.style=1
!y.style=1
plot ,xsun,ysun,thick=thi
oplot,xcirc,ycirc,linestyle=2
coordsphere,rsun,pos,crota2,blat,blong,dlat
xyouts_norm,0.05,0.90,'b  = ',char
xyouts_norm,0.05,0.85,'r  = ',char
xyouts_norm,0.05,0.80,'h0 = ',char
xyouts_norm,0.05,0.75,'az = ',char
xyouts_norm,0.05,0.70,'th = ',char
xyouts_norm,0.05,0.65,'l1 = ',char
xyouts_norm,0.05,0.60,'b1 = ',char
xyouts_norm,0.25,0.90,string(base,'(i4)')+' Mm',char
xyouts_norm,0.25,0.85,string(rloop,'(i4)')+' Mm',char
xyouts_norm,0.25,0.80,string(h0,'(i4)')+' Mm',char
xyouts_norm,0.25,0.75,string(az,'(i4)')+'!U0!N',char
xyouts_norm,0.25,0.70,string(th,'(i4)')+'!U0!N',char
xyouts_norm,0.25,0.65,string(l1,'(i4)')+'!U0!N',char
xyouts_norm,0.25,0.60,string(b1,'(i4)')+'!U0!N',char
print,nt
if (nt ge 2) then begin
for it=0,nt-1 do begin
 restore,coordfiles(it) 
 euvi_spline1,xa_sun,ya_sun,za_sun,x,y,z,d,r
 imid	=where((d ge qmid(0,0)*max(d)) and (d le qmid(1,0)*max(d)))
 jmid	=where((d ge qmid(0,1)*max(d)) and (d le qmid(1,1)*max(d)))
 kmid	=where((d ge qmid(0,2)*max(d)) and (d le qmid(1,2)*max(d)))
 dateobs=index.date_obs
 t=anytim(dateobs,/second) mod 86400
 tosc(it)=t-t0
 xosc(it)=avg(x(imid))
 yosc(it)=avg(y(jmid))
 zosc(it)=avg(z(kmid))
 if (it eq ifile) then begin
  oplot ,x,y,thick=3,color=128
  oplot,x,y,color=128
  oplot,x(imid),y(imid),thick=5
 endif
endfor
endif
!noeras=1

;(X,Z)-PROJECTION ________________PANEL 2____________________________
y1_	=0.55
y2_	=0.85
!p.position=[x1_,y1_,x2_,y2_]
!y.title='z [solar radii]'
!y.range=z0+range*[-1,1]*qfov
plot ,xsun,zsun,thick=thi
oplot,xcirc,zcirc,linestyle=2
coordsphere,rsun,pos,crota2,blat,blong,dlat
restore,coordfiles(ifile) 
euvi_spline1,xa_sun,ya_sun,za_sun,x,y,z,d,r
imid	=where((d ge qmid(0,0)*max(d)) and (d le qmid(1,0)*max(d)))
oplot,x,z,color=128,thick=3
oplot,x(imid),z(imid),thick=5

;(Z,Y)-PROJECTION ________________PANEL 3____________________________
x1_	=0.59
x2_	=0.99
y1_	=0.2
y2_	=0.5
!p.position=[x1_,y1_,x2_,y2_]
!x.title='z [solar radii]'
!y.title='y [solar radii]'
!x.range=z0+range*[-1,1]*qfov
!y.range=y0+range*[-1,1]*qfov
plot ,zsun,ysun,thick=thi
oplot,zcirc,ycirc,linestyle=2
coordsphere,rsun,pos,crota2,blat,blong,dlat
restore,coordfiles(ifile) 
euvi_spline1,xa_sun,ya_sun,za_sun,x,y,z,d,r
kmid	=where((d ge qmid(0,2)*max(d)) and (d le qmid(1,2)*max(d)))
oplot,z,y,color=128,thick=3
oplot,z(kmid),y(kmid),thick=5

;_________________________________PANEL 4____________________________
if (nt ge 2) then begin
y1_	=0.73
y2_	=0.85
!p.position=[x1_,y1_,x2_,y2_]
!p.title='East-west amplitude  dx(t)'
!x.title=' '
!y.title='Ampl. dx[Mm]'
!x.range=minmax(tosc)
!y.range=[-1,1]*696.*delta/4.
x	=xosc*696. ;Mm
c	=linfit(tosc,x)
xlin	=c(0)+c(1)*tosc
plot,tosc,x-xlin,thick=1
oplot,tosc,x-xlin,psym=4
oplot,tosc,x*0,linestyle=2
loop_osc_fit,tosc,x-xlin,tfit,xfit,cc,period
oplot,tfit(0:ifile*10-1),xfit(0:ifile*10-1),thick=3
a	=abs(cc(1))
p	=cc(2)
tau	=cc(3)
t0	=cc(4)/p
if (cc(1) lt 0) then t0=t0+0.5
text	='A='+string(a,'(f3.1)')+' Mm, P='+string(p,'(i3)')+$
         ' s, t!D0!N/P='+string(t0,'(f5.2)')
if (tau lt 1.e4) then text=text+ ', !9t!3!Ddamp!N='+string(tau,'(I5)')
xyouts_norm,0.02,0.02,text,char

;_________________________________PANEL 5____________________________
y1_	=0.55
y2_	=0.67
!p.position=[x1_,y1_,x2_,y2_]
!p.title='Line-of-sight amplitude  dz(t)'
!x.title='Time  t-t!D0!N [s]'
!y.title='Ampl. dz[Mm]'
!x.range=minmax(tosc)
!y.range=[-1,1]*696.*delta/4.
y	=yosc*696.
c	=linfit(tosc,y)
ylin	=c(0)+c(1)*tosc
plot,tosc,y-ylin,thick=1
oplot,tosc,y-ylin,psym=4
oplot,tosc,y*0,linestyle=2
loop_osc_fit,tosc,y-ylin,tfit,xfit,cc,period
oplot,tfit(0:ifile*10-1),xfit(0:ifile*10-1),thick=3
a	=abs(cc(1))
p	=cc(2)
tau	=cc(3)
t0	=cc(4)/p
if (cc(1) lt 0) then t0=t0+0.5
text	='A='+string(a,'(f3.1)')+' Mm, P='+string(p,'(i3)')+$
         ' s, t!D0!N/P='+string(t0,'(f5.2)')
if (tau lt 1.e4) then text=text+ ', !9t!3!Ddamp!N='+string(tau,'(I5)')
xyouts_norm,0.02,0.02,text,char
endif

fig_close,io,fignr,ref
end


