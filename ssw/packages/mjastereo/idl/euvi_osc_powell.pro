function euvi_osc_powell,coeff

common euvi_osc_powell_common,x,y,y_model

a0	=coeff(0)
a1	=coeff(1)
p	=coeff(2)
tau	=coeff(3)
t0	=coeff(4)
t	=x
y_model =a0+a1*cos(2.*!pi*(t-t0)/p)*exp(-(t-t0)/tau)
n	=n_elements(y)
dev	=sqrt(total((y_model-y)^2/float(n)))
return,dev
end


