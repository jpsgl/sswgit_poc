pro nlfff_vector,coeff,x_,bfff_
;+
; Project     : SOHO/MDI, SDO/HMI, STEREO
;
; Name        : NLFFF_VECTOR 
;
; Category    : Magnetic data modeling
;
; Explanation : Subroutine in support of NLFFF_FIELDLINE.PRO,
;               calculting a single magnetic field vector at position [X,Y,Z]
;
; Syntax      : IDL>nlfff_vector,coeff,x_,bfff_
;
; Inputs      : coeff      = magnetic model coefficients [b1,x1,y1,z1,a1]
;               x_[*,3]    = x,y,z coordinates of field vector
;
; Outputs     ; bfff_[*,4] = force-free field vectors at X_[*,3]
;
; History     : 21-Feb-2012, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

dimc    =size(coeff)
dimx    =size(x_)
nc      =dimc[1]
nx      =dimx[1]
bx   	=fltarr(nx)
by   	=fltarr(nx)
bz   	=fltarr(nx)
v_axis	=fltarr(nx,3)
rj_	=fltarr(nx,3)
cos_r	=fltarr(nx,3)
bfff_	=fltarr(nx,3)
for j=0,nc-1 do begin
;..........................Potential field........................
 b_j	=coeff(j,0)
 x_j	=coeff(j,1)
 y_j	=coeff(j,2)
 z_j	=coeff(j,3)
 rm	=sqrt(x_j^2+y_j^2+z_j^2)
 d_j	=1.-rm
 rj_[*,0]=x_[*,0]-x_j
 rj_[*,1]=x_[*,1]-y_j
 rj_[*,2]=x_[*,2]-z_j
 rj      =sqrt(rj_[*,0]^2+rj_[*,1]^2+rj_[*,2]^2)
 b_p	=b_j*(d_j/rj)^2
;........................Non-potential field.........................
 v_axis(*,0)=x_j
 v_axis(*,1)=y_j
 v_axis(*,2)=z_j
 vector_product_array,rj_,v_axis,r_phi,cos_phi,theta
 a_j	=coeff(j,4)
 b	=a_j/2.
 term	=b*rj*sin(theta)
 b_r	=b_p/(1.+term^2)
 b_phi	=b_r*term 				
;........................Magnetic field components output............
 cos_r[*,0]=rj_[*,0]/rj
 cos_r[*,1]=rj_[*,1]/rj
 cos_r[*,2]=rj_[*,2]/rj
 bx     =bx+b_r*cos_r[*,0]+b_phi*cos_phi[*,0]
 by     =by+b_r*cos_r[*,1]+b_phi*cos_phi[*,1]
 bz     =bz+b_r*cos_r[*,2]+b_phi*cos_phi[*,2]
endfor
bfff_(*,0)=bx
bfff_(*,1)=by
bfff_(*,2)=bz
end

