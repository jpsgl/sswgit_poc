pro loop_model_disp,io,fignr,plotname,savefiles,ifile,versnr,disp,fov,nsm,ct,range 
;
;DESCRITPTION:		Displays STEREO images A and B with loop models
;
;INPUT : io		=0=screen, -1=ps, 2=eps  3=color
;	 fignr		='6'
;	 plotname	='f'
;	 savefiles      =['~/euvi/20070627/20070627_1821_171_ab_image.sav,...]
;	 ifile		=1,...
;	 versnr		=1
;	 disp		=0=image, 2=highpass filter, 3=running difference
;	 fov		=[i1,j1,i2,j2]
;	 nsm		=5
;	 ct		=3


;PLOT PREPARATION_________________________________________________ 
form    =1      ;0=landscape, 1=portrait
char    =1.0    ;character size
ref     =''     ;label at bottom of Fig.
unit	=0
range	=float(range)
xrange	=range(*,0)
yrange	=range(*,1)
zarange	=range(*,2)
zbrange	=range(*,3)
new	=0
if (total(range) eq 0) then new=1

;READ DATA________________________________________________________
ipos	 =strpos(savefiles(ifile),'image.sav')
vers	 ='v'+strtrim(string(versnr,'(I3)'),2)
filename0=strmid(savefiles(ifile-1),0,ipos)
filename =strmid(savefiles(ifile)  ,0,ipos)
savefile0=filename0+'image.sav'
savefile =filename+'image.sav'
fitfile  =filename+vers+'_coord.sav'
restore,savefile0	;-->image_pair,para,index_a,index_b
image_pair0=image_pair
restore,savefile	;-->image_pair,para,index_a,index_b
restore,fitfile   	;-->index,para,xa_sun,xb_sun,yb_sun,za_sun,zb_sun,xa,xb,xa_,xb_
yb_sun	=ya_sun
ra	=sqrt(xa(*,0)^2+xa(*,1)^2) 
rb	=sqrt(xb(*,0)^2+xb(*,1)^2) 
ia      =where((ra ge 1) or (xa(*,2) gt 0),nia)
ib      =where((rb ge 1) or (xb(*,2) gt 0),nib)
oa      =where((ra le 1) and (xa(*,2) le 0),noa)
ob      =where((rb le 1) and (xb(*,2) le 0),nob)
dev	=[deva,devb]
sc__	=['A','B']
l1      =coeff(0)
b1      =coeff(1)
base    =coeff(2)
h0      =coeff(3)
az      =coeff(4)
th      =coeff(5)
i1	=fov(0)
j1	=fov(1)
i2	=fov(2)
j2	=fov(3)
rsun	=1.
p	=0.
p0	=0.
l0	=0.
b0	=0.
grid	=5.	;deg spacing of heliographic grid
l1      =coeff(0)
b1      =coeff(1)
base    =coeff(2)
h0      =coeff(3)
az      =coeff(4)
th      =coeff(5)
rloop  =sqrt(h0^2+(base/2.)^2)
phi0    =0.
if (rloop gt 0) then phi0=asin(h0/rloop)
l      =rloop*(!pi+2.*phi0)

DISPLAY: ;________________________________________________________
fig_open,io,form,char,fignr,plotname,unit
for isc=0,1 do begin
 image0	=reform(image_pair0(1-isc,*,*))
 image1	=reform(image_pair(1-isc,*,*))
 if (disp eq 0) then image2=image1                      ;original image
 if (disp eq 1) then image2=image1-smooth(image1,nsm)   ;highpass filter
 if (disp eq 2) then image2=smooth(image1-image0,nsm)	;running difference
 image	=image2(i1:i2,j1:j2)  			        ;subimage 
 statistic,image,avgz,sig
 z1    =avgz-sig
 z2    =avgz+sig
 x1_   =0.1
 x2_   =0.5
 y1_   =0.15+0.38*(1-isc)
 y2_   =0.45+0.38*(1-isc)
 !p.position=[x1_,y1_,x2_,y2_]
 !p.title='STEREO/'+sc__(isc)+' '+index.date_obs
 !x.title='EW  x[solar radii]'
 !y.title='NS  y[solar radii]'
 if (new eq 1) then xrange=([i1,i2]-index.crpix1)*index.cdelt1/index.rsun 
 if (new eq 1) then yrange=([j1,j2]-index.crpix2)*index.cdelt2/index.rsun
 !x.range=xrange
 !y.range=yrange
 !x.style=1
 !y.style=1
 loadct,ct
 loop_plot_image,image,z1,z2,ct,io
 coord_sphere,rsun,p,p0,l0,b0,grid
 loadct,3
 if (isc eq 0) then begin
  oplot,xa_sun,ya_sun,psym=1,symsize=2,thick=3,color=128 ;tracing points
  oplot,xa(ia,0),xa(ia,1),thick=3,color=128                ;interpolated 
  if (noa gt 0) then oplot,xa(oa,0),xa(oa,1),thick=3,color=128,linestyle=2
  oplot,xa_(*,0),xa_(*,1),color=200,linestyle=2,thick=3  ;circular model
 endif
 if (isc eq 1) then begin
  oplot,xb_sun,yb_sun,psym=1,symsize=2,thick=3,color=128 ;traced points
  oplot,xb(ib,0),xb(ib,1),thick=3,color=128                ;interpolated 
  if (nob gt 0) then oplot,xb(ob,0),xb(ob,1),thick=3,color=128,linestyle=2
  oplot,xb_(*,0),xb_(*,1),color=200,linestyle=2,thick=3  ;circular model
 endif
 xyouts_norm,0.05,0.90,'dev='+string(dev(isc)*696.,'(f6.1)')+' Mm',char
endfor
!noeras=1

;(Z,Y)-PROJECTION ________________PANEL 3____________________________
x1_    =0.60
x2_    =0.95
y1_    =0.15+0.38
y2_    =0.45+0.38
x0     =(max(xa_sun)+min(xa_sun))/2.
z0     =(max(za_sun)+min(za_sun))/2.
if (new eq 1) then zarange=xrange+(z0-x0)
!p.position=[x1_,y1_,x2_,y2_]
!x.range=zarange
!x.title='LOS  z[solar radii]'
!p.title='Projection from East'
plot,[0,0],[0,0]
oplot,xa_(*,2),xa_(*,1),color=200,linestyle=2,thick=3 ;circular model
oplot,xa(*,2),xa(*,1),thick=2,color=200		      ;maximum curvature model
oplot,za_sun,ya_sun,psym=1,symsize=2,thick=3,color=128 ;triangulation
spline_p,za_sun,ya_sun,za_tri,ya_tri                  ;spline fit
oplot,za_tri,ya_tri,thick=3,color=128 		      ;triangulation
coord_sphere,rsun,p,p0,l0,b0,grid
xyouts_norm,0.05,0.95,'r    = ',char
xyouts_norm,0.05,0.90,'L    = ',char
xyouts_norm,0.05,0.85,'base = ',char
xyouts_norm,0.05,0.80,'h0 = ',char
xyouts_norm,0.05,0.75,'az = ',char
xyouts_norm,0.05,0.70,'th = ',char
xyouts_norm,0.05,0.65,'l1 = ',char
xyouts_norm,0.05,0.60,'b1 = ',char
xyouts_norm,0.25,0.95,string(rloop*696,'(f6.1)')
xyouts_norm,0.25,0.90,string(l*696,'(f6.1)')
xyouts_norm,0.25,0.85,string(base*696,'(f6.1)')
xyouts_norm,0.25,0.80,string(h0*696,'(f6.1)')
xyouts_norm,0.25,0.75,string(az,'(f6.1)')
xyouts_norm,0.25,0.70,string(th,'(f6.1)')
xyouts_norm,0.25,0.65,string(l1,'(f6.1)')
xyouts_norm,0.25,0.60,string(b1,'(f6.1)')

;(Z,Y)-PROJECTION ________________PANEL 4____________________________
x1_    =0.60
x2_    =0.95
y1_    =0.15
y2_    =0.45
x0     =avg(xb_sun)
z0     =avg(zb_sun)
if (new eq 1) then zbrange=xrange+(z0-x0)
!p.position=[x1_,y1_,x2_,y2_]
!x.range=zbrange
!x.title='LOS  z[solar radii]'
!p.title='Projection from East'
plot,[0,0],[0,0]
oplot,zb_sun,yb_sun,psym=1,symsize=2,thick=3,color=128 ;triangulation
oplot,xb(*,2),xb(*,1),color=200,thick=3               ;maximum curvature model
oplot,xb_(*,2),xb_(*,1),color=200,linestyle=2,thick=3 ;circular model
spline_p,zb_sun,yb_sun,zb_tri,yb_tri                  ;spline fit
oplot,zb_tri,yb_tri,thick=3,color=128 		      ;triangulation
coord_sphere,rsun,p,p0,l0,b0,grid

if (new eq 1) then begin
 range(*,0)=xrange
 range(*,1)=yrange
 range(*,2)=zarange
 range(*,3)=zbrange
endif
fig_close,io,fignr,ref
end

