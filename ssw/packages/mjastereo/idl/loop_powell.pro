function loop_powell,var

;optimization function called from loop_model_fit.pro

common powell_par,model0,asep0,coeff0,xa0,xb0,xa_0,xb_0,dev_a0,dev_b0,foot0

l0	=0.
b0	=0.
p	=0.
p0	=0.
rsun	=1.0
hmin	=0.0
rmin	=1.0
q	=0.999
th	=var(0) 
h0	=var(1)
s1	=0.
s2	=0.
rhelix	=0.
nturn	=0.
if (model0 eq 2) then begin
 s1=(0>var(2))<1. 
 s2=(0>var(3))<1. 
endif
if (model0 eq 3) then begin
 rhelix=var(2) 
 nturn =var(3) 
endif
xa	=reform(xa0(*,0))
ya	=reform(xa0(*,1))
za	=reform(xa0(*,2))
xb	=reform(xb0(*,0))
yb	=reform(xb0(*,1))
zb	=reform(xb0(*,2))
na	=n_elements(xa)
nb	=n_elements(xb)

;footpoint coordinates____________________________________
if (total(abs(foot0)) ne 0) then begin
 lf1	=foot0(0)
 bf1	=foot0(1)
 lf2	=foot0(2)
 bf2	=foot0(3)
endif
if (total(abs(foot0)) eq 0) then begin
 xf1	=xa(0)+(xa(1)-xa(0))*s1
 yf1	=ya(0)+(ya(1)-ya(0))*s1
 xf2	=xa(na-1)-(xa(na-2)-xa(na-1))*s2
 yf2	=ya(na-1)+(ya(na-2)-ya(na-1))*s2
 rf1	=1.		;sqrt(xa(0)^2+ya(0)^2+za(0)^2)
 rf2	=1.		;sqrt(xa(na-1)^2+ya(na-1)^2+za(na-1)^2)
 helio_trans,p,p0,l0,b0,xf1,yf1,rf1,lf1,bf1,rsun
 helio_trans,p,p0,l0,b0,xf2,yf2,rf2,lf2,bf2,rsun
 lf1	=lf1(0)
 lf2	=lf2(0)
 bf1	=bf1(0)
 bf2	=bf2(0)
endif
if (za(0)    lt 0) then lf1=270.-(lf1-270.)
if (za(na-1) lt 0) then lf2=270.-(lf2-270.)
l1	=(lf1+lf2)/2.	
b1	=(bf1+bf2)/2.	

;azimuth angle of loop baseline_____________________________________________
if (abs(lf1-lf2) ge 180) then begin
 l20=lf2
 if (lf1 gt l20) then lf2=lf2+360.
 if (lf1 le l20) then lf2=lf2-360.
endif
arctan,(lf2-lf1),(bf2-bf1),az_rad,az	

;footpoint baseline ___________________________________________________
d_long  =(lf2-lf1)*(!pi/180.)
d_lat   =(bf2-bf1)*(!pi/180.)
base_rad=acos(cos(d_long)*cos(d_lat))   ;spherical triangle
base    =2.*rmin*sin(base_rad/2.)	
coeff0	=[l1,b1,base,h0,az,th,rhelix,nturn]

;coordinate transformation from loop parameters__________________________
if (model0 le 2) then loop_para_coord,base,h0,hmin,th,rsun,xl,yl,zl
if (model0 eq 3) then helix_para_coord,base,h0,rhelix,nturn,rsun,xl,yl,zl
helio_loopcoord,xl,yl,zl,rsun,az,th,l1,b1,rmin,l,b,r
helio_trans2,p,p0,l0,b0,l,b,r,xa_,ya_,za_,rsun
xa_0	=[[xa_],[ya_],[za_]]

;difference to observed position_________________________________________ 
for i=1,na-2 do begin
 dist   =sqrt((xa_-xa(i))^2+(ya_-ya(i))^2)
 d      =min(dist,inext)
 if (i eq 1) then dev_a=d
 if (i ge 2) then dev_a=[dev_a,d]
endfor
dev_a0	=avg(dev_a)

;second spacecraft_______________________________________________________ 
l1b	=l1+asep0
helio_loopcoord,xl,yl,zl,rsun,az,th,l1b,b1,rmin,l,b,r
helio_trans2,p,p0,l0,b0,l,b,r,xb_,yb_,zb_,rsun
xb_0	=[[xb_],[yb_],[zb_]]
for i=1,nb-2 do begin
 dist   =sqrt((xb_-xb(i))^2+(yb_-yb(i))^2)
 d      =min(dist,inext)
 if (i eq 1) then dev_b=d
 if (i ge 2) then dev_b=[dev_b,d]
endfor
dev_b0	=avg(dev_b)

;simultaneous fit to STEREO/A and B______________________________________
if (asep0 eq 0) then dev=dev_a0			 ;single spacecraft fit
if (asep0 ne 0) then dev=sqrt(dev_a0^2+dev_b0^2)  ;both spacecraft fit
return,dev
end
