pro nlfff_deriv,nseg,x_fit,v_fit

;calculated numerical derivatives of vectors v_fit[nfit,3] 
;from x_fit[nfit,3] with nfit=nf*nseg a sequence of piecewise loops,
;assuming nseg > 3

v_fit	=x_fit*0.
dim	=size(x_fit)
nfit	=dim[1]
nf	=nfit/nseg
for iseg=1,nseg-2 do begin			;intermediate vectors
 ifit	=nseg*findgen(nf)+iseg
 ifit1	=nseg*findgen(nf)+iseg-1
 ifit2	=nseg*findgen(nf)+iseg+1
 v_fit[ifit,0]=x_fit[ifit2,0]-x_fit[ifit1,0] 
 v_fit[ifit,1]=x_fit[ifit2,1]-x_fit[ifit1,1] 
 v_fit[ifit,2]=x_fit[ifit2,2]-x_fit[ifit1,2] 
endfor

if (nseg eq 3) then begin
 ifit	=nseg*findgen(nf)			;first vector
 ifit1	=nseg*findgen(nf)+1
 v_fit[ifit,0]=v_fit[ifit1,0] 
 v_fit[ifit,1]=v_fit[ifit1,1] 
 v_fit[ifit,2]=v_fit[ifit1,2]
 ifit	=nseg*findgen(nf)+nseg-1		;last vector
 ifit1	=nseg*findgen(nf)+nseg-2
 v_fit[ifit,0]=v_fit[ifit1,0] 
 v_fit[ifit,1]=v_fit[ifit1,1] 
 v_fit[ifit,2]=v_fit[ifit1,2] 
endif

if (nseg gt 3) then begin
 ifit	=nseg*findgen(nf)			;first vector
 ifit1	=nseg*findgen(nf)+1
 ifit2	=nseg*findgen(nf)+2
 v_fit[ifit,0]=2*v_fit[ifit1,0]-v_fit[ifit2,0] 
 v_fit[ifit,1]=2*v_fit[ifit1,1]-v_fit[ifit2,1] 
 v_fit[ifit,2]=2*v_fit[ifit1,2]-v_fit[ifit2,2] 
 ifit	=nseg*findgen(nf)+nseg-1		;last vector
 ifit1	=nseg*findgen(nf)+nseg-2
 ifit2	=nseg*findgen(nf)+nseg-3
 v_fit[ifit,0]=2*v_fit[ifit1,0]-v_fit[ifit2,0] 
 v_fit[ifit,1]=2*v_fit[ifit1,1]-v_fit[ifit2,1] 
 v_fit[ifit,2]=2*v_fit[ifit1,2]-v_fit[ifit2,2] 
endif

vx	=v_fit[*,0]				;normalization to unity vector
vy	=v_fit[*,1]
vz	=v_fit[*,2]
v	=sqrt(vx^2+vy^2+vz^2)
v_fit[*,0]=v_fit[*,0]/v
v_fit[*,1]=v_fit[*,1]/v
v_fit[*,2]=v_fit[*,2]/v
end
