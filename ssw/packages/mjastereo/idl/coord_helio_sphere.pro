pro coord_helio_sphere,index,grid
;+
; Project     : SOLAR 
;
; Name        : COORD_HELIO_SPHERE()
;
; Purpose     : Overplots heliographic coordinate grid onto an image plot
;
; Category    : Coordinate systems, Graphics
;
; Explanation : The heliographic coordinate system is referenced to the
;		ephemeris parameters in structure INDEX
;
; Syntax      : IDL> coord_sphere,index,grid
;
; Examples    : IDL> grid=5.	;5 degree spacing
;		IDL> !x.range=[-1000,1000]
;		IDL> !y.range=[-1000,1000]
;		IDL> plot,[0,0],[0,0]
;		IDL> coord_helio_sphere,index,grid
;
; Inputs      : index 	-  structure containing descriptors of data1 
;
;		grid	-  spacing of heliographic grid in degrees
;
; Outputs     : None
;
; Opt. Inputs : None 
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Common      : None
;
; Restrictions: structure INDEX is required to have some minimal solar
;		keywords, such as 
;			.solar_r	solar radius in arcseconds
;			.solar_l0	heliograph.longitude of disk center
;			.solar_b0	heliograph.latitude  of disk center
;			.solar_p	solar position angle
;			.crota2 	roll angle correction 
;
; Side effects: None
;
; History     : Version 1,   6-Oct-1998, Markus J. Aschwanden. Written
;
; Contact     : aschwanden@lmsal.com
;-


;extracting ephemerides from structure INDEX
;p	=index.solar_p
;p0	=index.crota2-p
;rad	=index.solar_r	
;l0	=index.solar_l0
;b0	=index.solar_b0
radii	=1.0

dateobs =index.date_obs
eph     =get_sun(dateobs)
rad     =eph(1)
l0      =eph(10)
b0      =eph(11)
p       =eph(12)
p0      =-p

;overplots coordinate grid
!noeras	=1
!p.linestyle=0
phi	=2.*!pi*findgen(361)/360.
oplot,rad*cos(phi),rad*sin(phi)

!p.linestyle=1
x0	=0.
y0	=0.
ilat	=fix(89./grid)
nlat	=2.*ilat+1
lat	=grid*(-ilat+findgen(nlat))
l	=findgen(361)
for i=0,nlat-1 do begin
 b	=l*0.+lat(i)
 coord_helio_cart,index,radii,l,b,ix,iy,x,y,z
 ind	=where(z gt 0,nind)
 if (nind ge 2) then begin
  igap	=where((ind(1:nind-1)-ind(0:nind-2)) ne 1,ngap)
  if (ngap ge 1) then ind=[ind(igap(0)+1:nind-1),ind(0:igap(0))]
  oplot,x(ind),y(ind)
 endif
endfor

nlon	=fix(360./grid)
lon	=grid*findgen(nlon)
b	=findgen(361)
for i=0,nlon-1 do begin
 l	=b*0.+lon(i)
 coord_helio_cart,index,radii,l,b,ix,iy,x,y,z
 ind	=where(z gt 0,nind)
 if (nind ge 2) then begin
  igap  =where((ind(1:nind-1)-ind(0:nind-2)) ne 1,ngap)
  if (ngap ge 1) then ind=[ind(igap(0)+1:nind-1),ind(0:igap(0))]
  oplot,x(ind),y(ind)
 endif
endfor
!p.linestyle=0
end
