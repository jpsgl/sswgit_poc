pro coordsphere,rad,pos,crota2,blat,blong,dlat
;
;overplots spherical coordinate grid onto contourplot of sun
;where X-axis and Y-axis are given in degrees relative to the suncenter
;RAD	=radius of optical sun (in degrees)
;POS	=Position angle of solar North (from map North to East)
;CROTA2	=rotation angle of image (e.g. CROTA2=-POS if image previously
;	 has already been oriented to solar North, otherwise CROTA2=0)
;BLAT	=heliographic latitude of solar disk center
;BLONG  =heliographic longitude of solar disk center
;DLAT 	=spacing of spherical grid in degrees (e.g. 10 or 15 deg)
;	[1,10] for 2 linesytles
;The parameters POS,BLAT,BLONG can be found in "Nautical Almanach" and
;are given in degrees.
;(Programmed by Markus Aschwanden, NASA/GSFC, Code 602.6, e-mail:ISIS::MARKUS)

dim	=size(dlat)
nlinestyle=dim(0)
nlat0	=dlat(0) 
if (nlinestyle eq 1) then nlat0=long(dlat(1)/dlat(0))
PI	=ACOS(-1.)	
R	=RAD					;conversion into degrees
DPOS	=POS+CROTA2
P	=DPOS*PI/180.				;position angle in radian
L	=BLAT*PI/180.				;latitude of center in radian
PH	=2*PI*FINDGEN(500)/499.
PH1	= +PI*FINDGEN(250)/249.-L
PH2	= -PI*FINDGEN(250)/249.+L
COSPH	=COS(PH)	&SINPH =SIN(PH)
COSPH1	=COS(PH1)	&SINPH1=SIN(PH1)
COSPH2	=COS(PH2)	&SINPH2=SIN(PH2)
COSP	=COS(P)		&SINP	=SIN(P)
COSL	=COS(L)		&SINL	=SIN(L)

!NOERAS	=1					;plot horizon
OPLOT,R*COSPH,R*SINPH			

ILAT	=FIX(89/DLAT(0))				;plot latitudes
NLAT	=2*ILAT+1
LATDEG	=BLAT+DLAT(0)*(-ILAT+FINDGEN(NLAT))
LAT	=LATDEG*PI/180.
FOR I=0,NLAT-1 DO BEGIN
 R_EL	=R*COS(LAT(I))
 Q_EL	=SINL
 C_EL	=R*SIN(LAT(I))*COSL
 X_EL	=R_EL*COSPH2
 Y_EL	=R_EL*SINPH2*Q_EL+C_EL
 line	=1
 if (nlinestyle eq 1) and ((i mod nlat0) eq 0) then line=0
 OPLOT,X_EL*COSP-Y_EL*SINP,X_EL*SINP+Y_EL*COSP,linestyle=line	;rotation by position angle
ENDFOR

NLON	=FIX(180./DLAT(0))-1
LONG1	=BLONG-90.				;EAST limb
LON1	=DLAT(0)*(FIX(LONG1/DLAT(0))+1)		;first meridian modulo DLAT
LONDEG	=(LON1-LONG1-90.)+DLAT(0)*(FINDGEN(NLON))	;relative to disk center
LON	=LONDEG*PI/180.				;plot longitudes
FOR I=0,NLON-1 DO BEGIN
 X1	=R*SINPH1*SIN(LON(I))
 Y0	=R*COSPH1
 IF (LON(I) NE 0) THEN Z1=X1/TAN(LON(I))
 IF (LON(I) EQ 0) THEN Z1=SQRT(R^2-Y0^2)
 R1_2	=Y0^2+Z1^2
 Y1	=Y0*COSL-SQRT(ABS(R1_2-Y0^2))*SINL 	;inclining by angle L
 line	=1
 if (nlinestyle eq 1) and ((i mod nlat0) eq 0) then line=0
 OPLOT,X1*COSP-Y1*SINP,X1*SINP+Y1*COSP,linestyle=line		;rotation by position angle
ENDFOR
END
