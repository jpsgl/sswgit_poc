pro aia_mag_fieldangle,xloop,yloop,zloop,ds,hmin,hmax,coeff,curr,a_mis,nseg,dev,dev_
;+
; Project     : AIA/SDO
;
; Name        : AIA_LOOP_FIELDLINE
;
; Category    : Data analysis   
;
; Explanation : Magnetic field line extrapolation
;               from midpoint position [xm,ym,zm] to both footpoints
;               [xf1,yf1,0] and [xf2,yf2,0]
;
; Syntax      : IDL>aia_loop_fieldline,xm,ym,zm,np,hmax,coeff,curr,field
;
; Inputs      : xm       = x-coordinate of loop midpoint (solar radii)
;               ym       = y-coordinate of loop midpoint 
;               zm       = z-coordinate of loop midpoint 
;               ds       = 3D loop increment 
;               hmax     = maximum height of (open) field line
;               coeff    = coefficients of magnetic potential field model
;
; Outputs     : field[ns,3] = coordinates of full loop field line
;
; History     : 1-Jun-2011, Version 1 written by Markus J. Aschwanden
;
; Contact     : aschwanden@lmsal.com
;-

;____________________ANGLE AT LOOP NP POINTS_____________________________ 
polar	=1.0
ns	=n_elements(xloop)
dev	=90.
dim	=size(curr)
ncurr	=dim[1]
if (ncurr ge 2) and (ns ge 2) then begin
 dev_	=fltarr(nseg)+dev
 for istep=0,nseg-1 do begin 
  im	=long((ns-1)*(1+istep)/float(nseg+1))<(ns-2)
  x1	=xloop(im)
  y1	=yloop(im)
  z1	=zloop(im)
  r1	=sqrt(x1^2+y1^2+z1^2)
  aia_mag_vector,x1,y1,z1,ds,hmin,hmax,coeff,curr,a_mis,polar,x2,y2,z2,b,endv
  v1	=[xloop[im+1]-xloop[im],yloop[im+1]-yloop[im],zloop[im+1]-zloop[im]]
  v2	=[x2-x1,y2-y1,z2-z1]
  vector_product,v1,v2,v3,v3_norm,angle
  dev_rad =angle < (!pi-angle)
  dev_(istep)=(180./!pi)*dev_rad			;misalignment angle
 endfor
 ind	=where(dev_ ne 0,nind)
 if (nind ge 1) then dev=median(abs(dev_(ind)))
endif
end

