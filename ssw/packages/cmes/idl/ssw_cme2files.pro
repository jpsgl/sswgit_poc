function ssw_cme2files, cmes, cdaw=cdaw, cactus=cactus, relative=relative, $
   c2_diff=c2_diff, yhtf=yhtf, $
  linear_fit=linear_fit , second_order=second_order, allfit=allfit, htp=htp ,$
  c2_nrl=c2_nrl, c3_nrl=c3_nrl, eit_nrl=eit_nrl, eit195_nrl=eit195_nrl, $
  sephtx=sephtx, dsthtx=dsthtx, c2eitx=c2eitx, c2deitx=c2deitx, daily=daily, $
  univiseral=universal
;
;+
;   Name: ssw_cme2files
;
;   Purpose: map from cme structures per ssw_getcme_list -> urls (cdaw/nrl)
;
;   Input Parameters:
;      cmes - cme structures per ssw_getcme_list.pro output
;
;   Output:
;      function returns structure vector 1:1 w/input with url info
;
;   Keyword Parameters:
;      cdaw - if set, cdaw.gsfc.nasa.gov list (default)
;      cactus - if set, assume CACTUS from sidc.oma.be
;      c2_diff - return C2 Difference jsmovie URLS
;      yhtf - return YHT (ascii file) urls
	
;      lfit - return linear fit graphics urls
;      second_order - return second order fit graphics ursl
;      allfit - return lfit/2nd order html urls 
;      htp - synonym for allfit (ie, html return)
;      c2_nrl, c3_nrl, eit_nrl(eit_195) - nrl daily mpg urls
;      sephtx, dsthtx - sep/dst + ht + xr graphics files
;      universal - monthly index
;      
;   History:
;      21-sep-2005 - S.L.Freeland
;      27-sep-2005 - tweaked fmt overflow file names ('*' -> '0')
;      28-sep-2005 - add UNIVERSAL 
;
;   Restrictions:
;      LASCO list by Gopalswamy et al, cdaw.gsfc.nasa.gov only for today'
;-
;  
if not required_tags(cmes,'cpa,width,lspeed,event_name') then begin
   box_message,'Expect structures output from <ssw_getcme_list.pro>, returning...'
   return,-1
endif

cdaw=keyword_set(cdaw)
cactus=keyword_set(cactus)
cdawtop='http://cdaw.gsfc.nasa.gov/CME_list/UNIVERSAL/'
case 1 of 
   cdaw: topdir=cdawtop
   cactus: box_message,'Sorry, CDAW list only for now..'
   else:
endcase

if not keyword_set(topdir) then topdir=cdawtop
if keyword_set(relative) then topdir=''       ; relative paths

ename=gt_tagval(cmes,/event_name,missing='')

; enames=YYYYMMDD.HHMMSS.pXXXn

yyyymm=strmid(ename,0,4) + '_'+ strmid(ename,4,2) + '/' ; cdaw subdir convention
 
allfit=keyword_set(allfit) or keyword_set(htp)
linear=keyword_set(linear_fit)
second=keyword_set(second_order)
hpng=allfit or linear or second 

c2=keyword_set(c2_nrl)
c3=keyword_set(c3_nrl)
e195=keyword_set(eit_nrl) or keyword_set(eit195_nrl)
nrl=c2 or c3 or e195   

sep=keyword_set(sephtx)
dst=keyword_set(dsthtx)

daily=keyword_set(daily)
dailym=keyword_set(c2eitx)  or keyword_set(c2deitx) or daily

universal=keyword_set(universal)


case 1 of 
   keyword_set(c2_diff): retval= $
      topdir+yyyymm + 'jsmovies/' + yyyymm + ename + '/c2_rdif.html'
   keyword_set(yhtf): begin
      wtype=(['n','p'])(cmes.width ge 120)
      ssh=where(cmes.width eq 360,hcnt)
      if hcnt gt 0 then wtype(ssh)='h'
      retval= $
      topdir + yyyymm + 'yht/' + strmid(ename,0,15)         +  $
         '.w'+ fstring(cmes.width, format='(I3.3)') + wtype +  $
         '.v'+ fstring(cmes.lspeed,format='(I4.4)')         +  $       
         strmid(ename,15,6) + '.yht'
      retval=str_replace(retval,'*','0')
   endcase
   hpng: begin
      pngdir=topdir + yyyymm + 'htpng/' 
      case 1 of 
         allfit: retval=pngdir+ename+'.htp.html'
         linear: retval=pngdir+ename+'.ht1.png'
         second: retval=pngdir+ename+'.ht2.png'
         else: begin
            box_message,'Unexpected message..'
            retval=''
         endcase
      endcase
   endcase
   nrl: begin
      yymmdd=strmid(ename,2,6)  +'_' ; nrl fname convention
      nrltop='http://lasco-www.nrl.navy.mil/daily_mpg/'
      retval=nrltop + yyyymm + yymmdd + $
         (['','c2'])(c2) + (['','c3'])(c3) + (['','eit_195'])(e195) +'.mpg'
   endcase 
   sep or dst: begin 
      dailydir=str_replace( topdir,'UNIVERSAL','daily_plots')
      type=(['dsthtx','sephtx'])(sep)
      retval=dailydir+type+'/'+yyyymm+type+'.'+strmid(ename,0,8)+'.png'
   endcase
   dailym: begin 
      dailydir=str_replace(topdir,'UNIVERSAL','daily_movies')
      retval=dailydir+anytim(cmes,/ecs,/date_only) 
      dnam=''
      case 1 of
         daily:
         keyword_set(c2eitx): dnam='c2eit_gxray.html'
         keyword_set(c2deitx): dnam='c2rdif_gxray.html'
         else: box_message,'Unknown daily movie keyword'
      endcase
      retval=temporary(retval)+ '/' + dnam
   endcase
   universal: retval=topdir+ yyyymm + 'univ'+str_replace(yyyymm,'/','.')+'html'
   else:     
endcase

return,retval
end

