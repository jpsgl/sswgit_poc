pro www_getcme_list, summary=summary, qfile=qfile, debug=debug
;+
;   Name: www_getcme_list
;
;   Purpose: WWW interface to ssw_getcme_list.pro
;
;   Keyword Parameters:
;      qfile - WWW Post Query filename  
;
;   History:
;      25-Sep-2005 - S.L.Freeland - written
;      28-Sep-2005 - S.L.Freeland - add #match statistics and limit cme->html
;
;   Restrictions:
;      generally only called from an sswidl server
;
;-
summary=keyword_set(summary)
htemp=get_logenv('path_http')
ttemp=get_logenv('top_http')

set_logenv,'path_http','/net/diapason/www1/htdocs/solarsoft/cme'
set_logenv,'top_http','http://www.lmsal.com/solarsoft/cme

debug=keyword_set(debug)

box_message,'Running www_getcme_list'

; --------------- read and decode the FORM input -----------
break_file,qfile,log,paths,qfil,qver,qext
;qfile=concat_dir(concat_dir(get_logenv('path_http'),'text_client'),qfil+qver+qext)
outfil=qfile + '.html'
box_message,[qfile,outfil]
qstr=url_decode(qfile=qfile)
strtext=str2cols(str_help(qstr,/nobox),':')
; --------------- -----------------------------------------

; ----------  derive HTML output document ---------------------
htmlfile=str_replace(outfil,'post','html')

mess=['www_getcme_list:  run at ' + systime(), $
      'Query File: ' + strrep_logenv(qfile,'path_http')] ;, $
;      'HTML  File: ' + strrep_logenv(htmlfile,'path_http'), $
;      'HTML  URL:  ' + http_names(htmlfile)]
; ------------------------------------------------

prstr,mess,/nomore

; ------- extract start and stop times and do some sanity checks ------
startt=url_query2time(qstr,/start,/nodef)
stopt =url_query2time(qstr,/stop,/nodef)
adjust_times, startt, stopt, attext, status=atstatus


; ------------------ filter on everything else ------------------

  form_qstr_valid, qstr, tag, value, count, confarr=confarr
;	    merge_list='wave_len', range_list=['xcen,ewfov','ycen,nsfov']

cmes=ssw_getcme_list(startt,stopt,search=confarr, count=count, tcount=tcount)
help,cmes
; ----------- call listing routine w/optional html select --------
llimit=200               ; live html listing limit

; ---------------- generate html output ------------------- 
thead=['Time Range','Number CMES In Range']
tdata=arr2str(anytim([startt,stopt],/ecs,/trunc),' <em>-to-</em>') 
tdata=[tdata,strtrim(tcount,2)]

if n_elements(confarr) gt 0 then begin  
   thead=[thead,'Auxilary Search Criteria','Number Matching CMES']
   tdata=[tdata,arr2str(confarr,' AND '),strtrim(count,2)]
endif

smess=html_highlight([[thead],[tdata]],1,([1,3])(n_elements(confarr) ne 0), $
   /red,rsize=2)

htlimit=400
if count gt htlimit then begin 
   box_message,'CME->HTML limit exceeded'
   ch1=ssw_cme2html(cmes(0:(htlimit/2)-1),/cgi_summary)
   ch2=ssw_cme2html(last_nelem(cmes,htlimit/2),/cgi_summary)
   cmehtml=['<center> <font color=orange> Over ' + strtrim(htlimit,2) + $
     ' Matches; Limiting HTML Listing to that number </font></center>', ch1, $ 
       '<center><font color=orange> <h3> (HTML Truncated - ' + $
       strtrim(count-htlimit,2) , $ 
       ' CMES omitted</font></center>',ch2]
endif else cmehtml=ssw_cme2html(cmes,/cgi_summary)
html_doc,htmlfile,/header
file_append,htmlfile,['<center><font color=orange>', $
       strtab2html(smess,/row0),'</font></center>']
file_append,htmlfile,cmehtml
if not summary then begin 
endif

html_doc,htmlfile,/trailer
; ----------------------------------------------------------------
box_message,['URL:',http_names(htmlfile)]

if debug then stop

set_logenv,'path_http',htemp
set_logenv,'top_http',ttemp       
return
end

