pro www_getcme_summary, summary=summary, qfile=qfile, debug=debug
;+
;   Name: www_getcme_list
;
;   Purpose: WWW interface to ssw_getcme_list.pro
;
;   Keyword Parameters:
;      qfile - WWW Post Query filename  
;  
;   History:
summary=1
htemp=get_logenv('path_http')
ttemp=get_logenv('top_http')

set_logenv,'path_http','/net/diapason/www1/htdocs/solarsoft/cme'
set_logenv,'top_http','http://www.lmsal.com/solarsoft/cme

debug=keyword_set(debug)

box_message,'Running www_getcme_list'

; --------------- read and decode the FORM input -----------
break_file,qfile,log,paths,qfil,qver,qext
;qfile=concat_dir(concat_dir(get_logenv('path_http'),'text_client'),qfil+qver+qext)
outfil=qfile + '.html'
box_message,[qfile,outfil]
qstr=url_decode(qfile=qfile)
strtext=str2cols(str_help(qstr,/nobox),':')
; --------------- -----------------------------------------

; ----------  derive HTML output document ---------------------
htmlfile=str_replace(outfil,'post','html')

mess=['www_getcme_list:  run at ' + systime(), $
      'Query File: ' + strrep_logenv(qfile,'path_http')] ;, $
;      'HTML  File: ' + strrep_logenv(htmlfile,'path_http'), $
;      'HTML  URL:  ' + http_names(htmlfile)]
; ------------------------------------------------

prstr,mess,/nomore

; ------------------ filter on everything else ------------------

  form_qstr_valid, qstr, tag, value, count, confarr=confarr
;	    merge_list='wave_len', range_list=['xcen,ewfov','ycen,nsfov']

cmes=ssw_getcme_list(string_fields='event_name',search=confarr)
help,cmes
; ----------- call listing routine w/optional html select --------
llimit=200               ; live html listing limit

; ---------------- generate html output -------------------
cmehtml=ssw_cme2html(cmes,/summary)
html_doc,htmlfile,/header
file_append,htmlfile,cmehtml
if not summary then begin 
endif

html_doc,htmlfile,/trailer
; ----------------------------------------------------------------
box_message,['URL:',http_names(htmlfile)]

if debug then stop

set_logenv,'path_http',htemp
set_logenv,'top_http',ttemp       
return
end

