function ssw_cme2yht,cmes, append=append, truncate=truncate, $
   noyht_data=noyht_data
;
;   Name: ssw_cme2yht
;
;   Purpose: generate structure representation of the YHT ascii data@cdaw
;
;   Input Parameters:
;      cmes - one or more CME structures (output from ssw_getcme_list.pro)
;
;   Output:
;      Function returns YHT structure 1:1 input CMES; option, append to CMES 
;      {version:0,date_obs:'',onset1:'',onset2:'',feat_code'',quality:0,$
;           ytd_time:strarr(32),yht_height:fltarr(32)}
;
;   Keyword Parameters:
;      append - if set, return yht structures appended to input CME structures
;      truncate - if set, truncate ytd_time & yht_height to max elements
;                 in input yht data files (default is padded to 32 elements,
;                 the theoretcial maximum)
;      noyht_data - if set, only include "header" inc. ONSET1,ONSET2
;                    but omit the HT table data
;
;   Calling Sequence:
;      yht=ssw_cme2yht(cmes [,/append] [,/truncate])
;
;   History:
;      6-Oct-2005 - S.L.Freeland 
;
;-
if not required_tags(cmes,'cpa,lspeed,event_name') then begin 
   box_message,'Require input CME structures (output from ssw_getcme_list'
   return,-1
endif

if required_tags(cmes,'onset1,onset2,yht_time') then begin 
   box_message,'Input CME structures already include YHT info...?
   return,cmes
endif

nc=n_elements(cmes)

incyht=1-keyword_set(noyht_data)

;                 |--------------------- tags to auto copy --------------|
yhts={date_obs:'',version:0,onset1:'',onset2:'',feat_code:'',quality_index:0}
if incyht then $
  yhts=join_struct(yhts, {yht_time:strarr(32),yht_height:fltarr(32)})
atags=(tag_names(yhts))(1:5)
 
retval=replicate(yhts,nc)

yhtf=ssw_cme2files(cmes,/yht) ; CMES -> yht_data urls@cdaw.nascom.nasa.gov

for i=0,nc-1 do begin
   delvarx,yhtdata
   sock_list,yhtf(i),yhtdata
   ssv=where(strpos(yhtdata,'#VERSION') ne -1,vcnt)
   if vcnt gt 0 then yhtdata(ssv(0))=str_replace(yhtdata(ssv(0)),'=',':')
   if n_elements(yhtdata) lt 10 then $
     box_message,'Problem with access> ' + yhtf(i) else begin
    yhtdata=strtrim(yhtdata,2)
    hss=where(strpos(yhtdata,'#') eq 0 ,hcnt)
    htss=where(is_number(strmid(yhtdata,0,1)) and strlen(yhtdata) gt 40,htcnt)
    head=str_replace(ssw_strsplit(yhtdata(hss),':',tail=yhtd),'#','')
    yhtd=strtrim(yhtd,2)
    for at=0,n_elements(atags)-1 do begin 
       retval(i).(tag_index(retval,atags(at)))= $
         yhtd(where(head eq atags(at)))
    endfor
    if incyht then begin 
       dtab=str2cols(yhtdata(htss),/unal)
       strtab2vect,dtab,height,date,time
       nvalid=n_elements(date)
       retval(i).yht_height(0:nvalid-1)=float(height)
       retval(i).yht_time(0:nvalid-1)=strtrim(date + ' ' + time,2)
    endif
  endelse

endfor

if keyword_set(truncate) and incyht then begin 
   if n_elements(retval) gt 1 then $
      val=n_elements(where(total(retval.yht_height,2))) else $
      val=n_elements(where(retval.yht_height gt 0))
   newval=retval.yht_height(0:val-1)
   retval=rep_tag_value(temporary(retval),newval,'yht_height')
   newval=retval.yht_time(0:val-1)
   retval=rep_tag_value(temporary(retval),newval,'yht_time')
endif

return,retval
end
