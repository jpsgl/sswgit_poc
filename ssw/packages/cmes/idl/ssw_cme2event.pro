pro ssw_cme2event, events, genx=genx
;
;   Name: ssw_cme2event
;
;   Purpose: map cdaw cmes->event names -> $SSWDB/genx
;
;   Input Parameters:
;      NONE
; 
;   Keyword Parameters:
;      genx - if set, generate updated index -> SSWDB
;
;   Motivation:
;     cdaw event names (and derived URLS) not deterministic from event parameters
;
;   History:
;     21-Sep-2005 - S.L.Freeland
;-
genx=keyword_set(genx) or n_params() eq 0 
topdir='http://cdaw.gsfc.nasa.gov/CME_list/'

sock_list,topdir,tlist ; top level CDAW url

ss=where(strpos(tlist,'href="UNIVERSAL') ne -1) 

murls=strextract(tlist(ss),'href="','">')
mdirs=topdir+murls
nms=n_elements(mdirs)
events=strarr(50000)
pnt=0
cmes=ssw_getcme_list(/refresh,/cdaw)
icmes=anytim(cmes,/int)
badtimes=''
for i=0,nms-1 do begin 
   box_message,mdirs(i)
   delvarx,mlist
   sock_list,mdirs(i),mlist
help,mlist
   sse=where(strpos(mlist,'htpng') ne -1 and strpos(mlist,'htp.html') ne -1,ecnt)
   if ecnt eq 0 then begin 
      box_message,'No events for month??'
   endif else begin 
      sse=sse(where(strlen(mlist(sse)) eq max(strlen(mlist(sse))),ecnt))
      mevents=all_vals(mlist(sse))
      ecnt=n_elements(mevents)
      mevents=strextract(mevents,'"htpng/','.htp')
      bevt=where(strlen(mevents) ne strlen('19960501.084146.p070g'),bcnt)
      if bcnt gt 0 then stop,'bad event 
      events(pnt)=mevents ; insert
      etimes=file2time(mevents,out='int')
      ss=tim2dset(icmes,etimes,delta=dts)
      bss=where(dts ne 0,bdts)
      if bdts gt 0 then begin 
         box_message,['Time mismatch',mevents(bss)]
         badtimes=[temporary(badtimes),mevents(bss)]
      endif
      pnt=pnt+ecnt
   endelse
endfor

events=events(where(events ne ''))
stop,'events'

if keyword_set(genx) then begin 
      etimes=file2time(events,out='int')
      ss=tim2dset(icmes,etimes,delta=dts)
      cmes(ss).event_name=events
      ssb=where( cmes.event_name eq '',bcnt)
      if bcnt gt 0 then begin 
         box_message,'patching enames'
         bnames=time2file(cmes(ssb),delim='.',/sec) + '.p'+ $
            fstring(cmes(ssb).mpa,format='(I3.3)')
         rcar='s'
         for bn=0,bcnt-1 do begin 
            lcar=strmid(cmes(ssb(bn)-2:ssb(bn)+2).event_name,20,1)
            lss=where(lcar ne '',lcnt)
            if lcnt gt 0 then begin 
               hcar=histogram(byte(lcar(lss))) ; high frequency, adjacent
               rcar=string(byte(where(hcar eq max(hcar))))
            endif else box_message,'No obvious patch character
          endfor
          cmes(ssb).event_name=bnames+rcar
      endif 
      outdir=concat_dir('$SSWDB','soho/lasco/cmes')
      savegenx,file=concat_dir(outdir,'cdaw_cme_index'),cmes,/over
stop,'saved
endif
return
end
