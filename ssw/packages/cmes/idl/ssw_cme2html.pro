function ssw_cme2html,cmes, summary=summary, cgi_summary=cgi_summary
;+
;   Name: ssw_cme2html
;
;   Purpose: generate html table or summary graphic(s) from input 'cmes'
;
;   Input Paramters:
;      cmes - cme structures per 'ssw_getcme_list.pro' output
;
;   Output:
;      function returns html suitable for framing
;
;   Keyword Parameters:
;      summary - if set, generate/return summary page(s), not table
;
;   History:
;      23-sep-2005 - S.L.Freeland
;      26-sep-2005 - S.L.Freeland - corrected 23-sep hiccups..
;      11-oct-2005 - S.L.Freeland - add SXT Legacy links
;-
if not required_tags(cmes,'event_name,width,lspeed') then begin 
   box_message,'Expect input structures from <ssw_getcme_list.pro>'
   return,''
endif


c2diff=ssw_cme2files(cmes,/c2_diff)  ; urls
yht=ssw_cme2files(cmes,/yht) 
fithtml=ssw_cme2files(cmes,/allfit)
c2n=ssw_cme2files(cmes,/c2_nrl)
c3n=ssw_cme2files(cmes,/c3_nrl)
eitn=ssw_cme2files(cmes,/eit_nrl)
phtx=ssw_cme2files(cmes,/sephtx)
dstx=ssw_cme2files(cmes,/dsthtx)
c2dx=ssw_cme2files(cmes,/c2deitx) ; difference + goes plot
lfit=ssw_cme2files(cmes,/linear) 
daily=ssw_cme2files(cmes,/daily)


if keyword_set(summary) then begin 
   retval=''
   for i=0,n_elements(cmes)-1 do begin 
      stab=get_infox(cmes(i),'cpa,width,lspeed,accel')
      yhtdat='' & onset=''
      sock_list,yht(i),yhtdat
      onsetss=where(strpos(yhtdat,'#ONSET1') ne -1,ocnt)
      if ocnt gt 0 then onset=ssw_strsplit(yhtdat(onsetss),': ',/tail)
      stab=[anytim(cmes(i),/ecs),onset,str2cols(stab)]
      stab(2)=([stab(2),'Halo'])(cmes(i).cpa eq -9999)
      delvarx,imglist
      imglist=ssw_jsurl2imagelist(c2dx(i))
      if imglist(0) eq '' then begin 
         retvalx=['Problem accessing images within:',c2dx(i)]
      endif else begin 
      tims=strmids(imglist,strlen(imglist)-18,strpos(imglist,'.png'))
      itimes=file2time(strmid(tims,0,8) + '_'+ strmid(tims,8,6),out='int')
      dt=abs(anytim(itimes) - anytim(cmes(i)))
      order=sort(dt)
      ss2=(where(strpos(imglist(order),'c2') ne -1))(0)
      ssx=(where(strpos(imglist(order),'xray') ne -1))(0)
      closess=[order(ss2),order(ssx)]
      hname=str_replace(gt_tagval(cmes(i),/event_name),'.','_')
      head=['C2 Difference','GOES XRay','Linear Fit']
      images='<img src="' + [imglist(closess),lfit[i]] + '">'
      univ=ssw_cme2files(cmes(i),/univ)
      shead=['First C2 Appearance','First Onset at 1Rs','CPA [deg]','Width [deg]','Speed [km/s]',$
             'Accel [m/s<sup>2</sup>]']
      retvalx=['<center>', $
              strtab2html([[shead],[stab]],/row0,/center), '</center><p>', $
              strtab2html([[head],[images]],/row0), $
              '<p>Details:  ' + str2html(univ)]
      endelse
      retval=[temporary(retval),retvalx]
   endfor
endif else begin 
   header=['First C2 Appearance', $     ; > c2_diff
           'Height-Time',         $     ; > yht
           'Central PA [deg]', $
          'Angular Width [deg]',$
        'Linear Fit Speed [km/s]',$    ;> linear 
        '2nd Order Fit Speed (Final) [km/s]',           $    ;> linear
        '2nd Order Fit Speed (20 Rs) [km/s]', $  ;> linear
        'Accel [m/s<sup>2</sup>]',    $            
        'Mass [gram]',     $
        'Kinetic Energy [erg]', $
        'MPA (deg)','Daily Movies & Plots', $;> c2,c3,195,phtx,dst,jm 
        'Remarks']
   if keyword_set(cgi_summary) then header=[header,'Event Summary']
   ssyo=where(anytim('1-nov-1991') le cmes.anytim_dobs and $
              anytim('15-dec-2001') ge cmes.anytim_dobs,ysscnt)
   sxtlinks=strarr(n_elements(cmes))
   if ysscnt gt 0 then begin 
      smquery='http://www.lmsal.com/cgi-diapason/www_cme2sxt_movie.sh?' + $
          'event_name='+strtrim(string(cmes(ssyo).event_name),2)
      sxtlinks(ssyo)='<A HREF="' + smquery+ '">SXT,</A>'
   endif
   retval=strarr(n_elements(header),n_elements(cmes)+1)
   retval(0,1:*)=str2html(c2diff,link=anytim(cmes,/ecs),/nopar)
   retval(1,1:*)=str2html(yht,link_text='HT Data')
   retval(2,1:*)=strtrim(string(cmes.cpa),2)
   hss=where(retval(2,*) eq -9999,hcnt)
   if hcnt gt 0 then retval(2,hss) ='Halo'
   retval(3,1:*)=strtrim(string(cmes.width),2)
   retval(4,1:*)=str2html(fithtml,link=strtrim(string(cmes.lspeed),2))
   retval(5,1:*)=str2html(fithtml,link=strtrim(string(cmes.qspeed_final),2))
   retval(6,1:*)=str2html(fithtml,link=strtrim(string(cmes.qspeed_2or),2))
   retval(7,1:*)=strtrim(fstring(cmes.accel,format='(f10.2)'),2)
   ivss=where(cmes.accel eq -9999,sscnt)
   if sscnt gt 0 then retval(7,ivss+1)='--'
   retval(8,1:*)=strtrim(fstring(cmes.mass, format='(e20.2)'),2)
   ivss=where(cmes.mass eq -9999,sscnt)
   if sscnt gt 0 then retval(8,ivss+1)='--'
   retval(9,1:*)=strtrim(fstring(cmes.kenergy,format='(e20.2)'),2)
   ivss=where(cmes.kenergy eq -9999,sscnt)
   if sscnt gt 0 then retval(9,ivss+1)='--'
   retval(10,1:*)=strtrim(string(cmes.mpa),2)
   retval(11,1:*)=reform(str2html(c2n,link='C2') + ', ' + str2html(c3n,link='C3') + ', ' + $
                        str2html(eitn,link='EIT_195') + ', ' + $
                        sxtlinks                             + $		       
                        str2html(phtx,link='PHTX')    + ', ' + $
                        str2html(dstx,link='Dst')     + ', ' + $
                        str2html(daily,link='Daily_JS_Movies') )
   retval(12,1:*)=cmes.remarks
   if keyword_set(cgi_summary) then begin 
      if data_chk(cgi_summary,/string) then cgi=cgi_summary else $
         cgi='http://www.lmsal.com/cgi-diapason/www_getcme_summary.sh'
      query=cgi+'?event_name='+strtrim(string(cmes.event_name),2)
      retval(13,1:*)='<A HREF="' + query +'">' + strtrim(string(cmes.event_name),2) +'</A>'

   endif
   retval(*,0)=header
   retval=strtab2html(retval,/row0)
endelse
return,retval
end

