************************ Q = CHI^2/DOF *****************************

We now use the definition of:
   Q = Chi^2/DoF, good fits end when Q<<1.0, Q always greater than 0


A typical Pixon run outputs statistics like this:

Fit/DOF           0       37980

 Iter            LLF    Convergence              Q      Chi^2/DOF
    0   2.272829e+09   0.000000e+00   8.246452e+06   5.984280e+04
    1   3.423304e+08   1.930499e+09   1.241952e+06   9.013439e+03
    2   4.418489e+07   2.981455e+08   1.601798e+05   1.163373e+03
    3   2.422127e+07   1.996362e+07   8.774511e+04   6.377375e+02
    4   1.659190e+07   7.629376e+06   6.006317e+04   4.368588e+02
    5   1.702744e+07  -4.355480e+05   6.164348e+04   4.483266e+02
    6   2.154654e+06   1.487279e+07   7.680004e+03   5.673128e+01
    7   6.842554e+05   1.470399e+06   2.344904e+03   1.801620e+01
    8   2.366654e+05   4.475899e+05   7.208975e+02   6.231317e+00
    9   1.129645e+05   1.237009e+05   2.720690e+02   2.974316e+00
   10   5.742607e+04   5.553845e+04   7.055688e+01   1.512008e+00
   11   2.190267e+04   3.552341e+04  -5.833397e+01   5.766895e-01
   12   1.419378e+04   1.343124e+04  -8.630441e+01   3.737172e-01

And the fetched quantities for the above, used in our examples here, are:

    Chi2 = 14193.77
    Dof = 37980 (N*N*Nviews less the wt=0 pixels)
    varChi2 = 75960

** Pixon 'Q' is not Q

Pixon's "Q" is actually (Chi2 - DoF)/sqrt(varChi2) (= -86.30)

** Legacy 'Q' is not quite Q

Our legacy 'Q' from Paul is:
    Qpaul = (Chi2 - DoF) / (2* DoF) (= -0.313)

The 'Chi^2/DoF' value above is 0.373

So we store 'Q History' which is actually the 'Chi^2/DoF' value,
and now generate our final Q using the same formulation.

************************ Q = CHI^2/DOF *****************************
