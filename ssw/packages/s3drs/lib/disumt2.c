
/*
   disumt2.c
   Implementation of DSFT sum with linear interpolation and
   variable voxel image size
*/

#include "stdio.h"
#include "math.h"

long disumt(argc,argv)
int argc;
void *argv[];

{
long   N;
double *h;
double *i;
double *j;
float  *img;
double *f;
double v;

long   im,i0,ip,jm,j0,jp,Njm,Nj0,Njp;
double dim,di0,dip,di,djm,dj0,djp,dj;
long   k;
double q,w;

N   = *(long *)argv[0];
h   = (double *)argv[1];
i   = (double *)argv[2];
j   = (double *)argv[3];
img = (float *)argv[4];
f   = (double *)argv[5];
v   = *(double *)argv[6];  /* length of edge of voxel image */

q = 0.5*(v-1);
w = 1/v;

for(k=0;k<N*N;k++)
{
  i0 = floor(i[k]+0.5);
  di = i[k]-i0;

  j0 = floor(j[k]+0.5);
  dj = j[k]-j0;
  
  ip = i0+1;
  im = i0-1;
  jp = j0+1;
  jm = j0-1;
  
  Nj0 = N*j0;
  Njm = Nj0-N;
  Njp = Nj0+N;
  
  if((i0 >= 0) && (i0 < N) && (j0 >= 0) && (j0 < N))
  {
    dim=(q-di)*w; if(dim<0)dim=0;
    dip=(q+di)*w; if(dip<0)dip=0;
    di0=1-dim-dip;
    
    djm=(q-dj)*w; if(djm<0)djm=0;
    djp=(q+dj)*w; if(djp<0)djp=0;
    dj0=1-djm-djp;
    
                   img[k]+=h[Nj0+i0]*f[k]*di0*dj0;
       if(jm >= 0) img[k]+=h[Njm+i0]*f[k]*di0*djm;
       if(jp <  N) img[k]+=h[Njp+i0]*f[k]*di0*djp;
    
    if(im >= 0)
    {              img[k]+=h[Nj0+im]*f[k]*dim*dj0;
       if(jm >= 0) img[k]+=h[Njm+im]*f[k]*dim*djm;
       if(jp <  N) img[k]+=h[Njp+im]*f[k]*dim*djp;
    }
    
    if(ip < N)
    {              img[k]+=h[Nj0+ip]*f[k]*dip*dj0;
       if(jm >= 0) img[k]+=h[Njm+ip]*f[k]*dip*djm;
       if(jp <  N) img[k]+=h[Njp+ip]*f[k]*dip*djp;
    }
  }
  
}

return(-20);

}
