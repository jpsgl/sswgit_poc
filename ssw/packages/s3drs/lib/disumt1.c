
/*
   disumt1.c
   Implementation of DSFT sum with linear interpolation and
   variable voxel image size
*/

#include "stdio.h"
#include "math.h"

long disumt(argc,argv)
int argc;
void *argv[];

{
long   N;
double *h;
double *i;
double *j;
float  *im;
double *f;

long k;

long i0,j0,id,jd,isi,isj;
double di,dj,adi,adj,a0i,a0j;

N   = *(long *)argv[0];
h   = (double *)argv[1];
i   = (double *)argv[2];
j   = (double *)argv[3];
im  = (float *)argv[4];
f   = (double *)argv[5];

for(k=0;k<N*N;k++)
{
  i0  = floor(i[k]+0.5);
  di  = i[k]-i0;
  adi = fabs(di);

  j0  = floor(j[k]+0.5);
  dj  = j[k]-j0;
  adj = fabs(dj);
  
  id  = i0+((di>0)?1:-1);
  jd  = j0+((dj>0)?1:-1);
  a0i = 1-adi;
  a0j = 1-adj;

  if((i0 >= 0) && (i0 < N))
  { if((j0 >= 0) && (j0 < N))im[k] += h[N*j0+i0]*f[k]*a0i*a0j;
    if((jd >= 0) && (jd < N))im[k] += h[N*jd+i0]*f[k]*a0i*adj;
  }
  
  if((id >= 0) && (id < N))
  { if((j0 >= 0) && (j0 < N))im[k] += h[N*j0+id]*f[k]*adi*a0j;
    if((jd >= 0) && (jd < N))im[k] += h[N*jd+id]*f[k]*adi*adj;
  }
}

return(-10);

}
