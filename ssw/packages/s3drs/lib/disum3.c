
/*
implement trapezoid spreading. This is only exact for infinite observer
who is on the xy plane. A square of side 1 with constant emission,
viewed from infinity, will project to a trapezoid. If the center of the
trapezoid is at zero, the top of the trapezoid will be 1/c high, and
extend from -d to d, and the base of the trapezoid will extend from
-e to e, where d=(c-s)/2 and e=(c+s)/2, s=sin(g), c=cos(g), where g
is the viewing angle, or the angle by which the square is rotated.
(It is assumed that 0<=g,=45 deg. If not, a suitable g can be found
which gives the same results and does obey this constraint)
*/

#include "stdio.h"
#include "math.h"

double ITrap();

double s,c,e,d,A0;      /* Global variables */

/**************************************************************/

long disum(argc,argv)
int argc;
void *argv[];

{
long   N;
double *dat;
double *i;
double *j;
double *ff;

long k;

long   im,i0,ip;
long   j0,jd;
double di,ami,a0i,api;
double dj,adj,a0j;

double g,gg;

N   = *(long *)argv[0];   /* Number of pixels in a data array edge */
dat = (double *)argv[1];  /* data array */
i   = (double *)argv[2];  /* i index */
j   = (double *)argv[3];  /* j index */
ff  = (double *)argv[4];  /* multiplier */
g   = *(double *)argv[5]; /* view angle (gamma) */

gg = fabs(g);
while(gg>45.0)gg=gg-90.0;
gg = fabs(gg)*3.1415926535/180.;
s  = sin(gg);
c  = sqrt(1-s*s);
d  = 0.5*(c-s);
e  = 0.5*(c+s);
A0 = s/(2*c);  /* Area under one of the sloping parts of the trapezoid */

for(k=0;k<N*N;k++)
{

  i0  = floor(i[k]+0.5);
  im  = i0-1;
  ip  = i0+1;
  di  = i[k]-i0;

  ami = ITrap(-di-1.5,-di-0.5);
  a0i = ITrap(-di-0.5,-di+0.5);
  api = ITrap(-di+0.5,-di+1.5);

  j0  = floor(j[k]+0.5);
  dj  = j[k]-j0;
  adj = fabs(dj);

  jd  = j0+((dj>0)?1:-1);  /* (dj>0)?1:-1 is the sign of dj */
  a0j = 1-adj;

  if((im >= 0) && (im < N))
  { if((j0 >= 0) && (j0 < N))dat[N*j0+im] += ff[k]*ami*a0j;
    if((jd >= 0) && (jd < N))dat[N*jd+im] += ff[k]*ami*adj;
  }

  if((i0 >= 0) && (i0 < N))
  { if((j0 >= 0) && (j0 < N))dat[N*j0+i0] += ff[k]*a0i*a0j;
    if((jd >= 0) && (jd < N))dat[N*jd+i0] += ff[k]*a0i*adj;
  }

  if((ip >= 0) && (ip < N))
  { if((j0 >= 0) && (j0 < N))dat[N*j0+ip] += ff[k]*api*a0j;
    if((jd >= 0) && (jd < N))dat[N*jd+ip] += ff[k]*api*adj;
  }

/*  debug
if((i0==28) && (j0==42))
{ printf("\n1: i0=%d i=%f di=%f adi=%f si=%d id=%d a0i=%f",i0,i[k],di,adi,si,id,a0i);
  printf("\n   j0=%d j=%f dj=%f adj=%f sj=%d jd=%d a0j=%f",j0,j[k],dj,adj,sj,jd,a0j);
  printf("\n   dat[N*j0+i0]=%f dat[N*j0+id]=%f dat[N*jd+i0]=%f dat[N*jd+id]=%fi\n",
  dat[N*j0+i0],dat[N*j0+id],dat[N*jd+i0],dat[N*jd+id]);
  return(N);
}
*/

/*
if((a0i<0)||(a0j<0)||(adi<0)||(adj<0))
printf("\n disum.c error: %f %f %d %d",i[k],j[k],i0,j0);
*/

}

return(10);

}

/****************************************************************/

double ITrap(x0,x1)
/* Integrate the trapezoid from x0 to x1 */
double x0,x1;

{ double J;
  double a0m,a0p,a1m,a1p;

/*
a0m is the fraction of A0 covered by the triangle with one vertex [-e,0] and the
opposite side at x0. a0p is for the other side of the trapezoid and a1m and a1p
are for x1 instead of x0 respectively
*/

  if(s==0)
  { a0m=0;
    a0p=0;
    a1m=0;
    a1p=0;
  }
  else
  { a0m=(e-x0)/s;
    a0p=(e+x0)/s;
    a1m=(e-x1)/s;
    a1p=(e+x1)/s;
    a0m=a0m*a0m;
    a0p=a0p*a0p;
    a1m=a1m*a1m;
    a1p=a1p*a1p;
  }

/*;;*/

/*
printf("\nx0=%f x1=%f",x0,x1);
printf("\na0m=%f a0p=%f a1m=%f a1p=%f",a0m,a0p,a1m,a1p);
*/

          if(x0<-e)
  {                  if(x1<-e)  J = 0;
     else if((x1>=-e)&&(x1<-d)) J = a1p*A0;
     else if((x1>=-d)&&(x1< d)) J = A0 + (x1+d)/c;
     else if((x1>= d)&&(x1< e)) J = 1 - a1m*A0;
     else                       J = 1;
   }

   else   if((x0>=-e)&&(x0<-d))
   {      if((x1>=-e)&&(x1<-d)) J = (a1p-a0p)*A0;
     else if((x1>=-d)&&(x1< d)) J = (1-a0p)*A0 + (x1+d)/c;
     else if((x1>= d)&&(x1< e)) J = 1-(a0p+a1m)*A0;
     else                       J = 1 - a0p*A0;
   }

   else   if((x0>=-d)&&(x0<d))
   {      if((x1>=-d)&&(x1<d)) J = (x1-x0)/c;
     else if((x1>= d)&&(x1<e)) J = (d-x0)/c + (1-a1m)*A0;
     else                      J = (d-x0)/c + A0;
   }

   else if((x0>=d)&&(x0<e))
   {    if((x1>=d)&&(x1<e))    J = (a0m-a1m)*A0;
     else                      J = a0m*A0;
   }

   else J=0;

return J;

}


