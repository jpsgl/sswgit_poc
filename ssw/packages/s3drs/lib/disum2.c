
/*
   disum2.c
   Implementation of DSF sum with linear interpolation and
   variable voxel image size
*/

#include "stdio.h"
#include "math.h"

long disum(argc,argv)
int argc;
void *argv[];

{
long   N;
double *dat;
double *i;
double *j;
double *ff;
double v;

long   im,i0,ip,jm,j0,jp,Njm,Nj0,Njp;
double dim,di0,dip,di,djm,dj0,djp,dj;
long   k;
double q,w;

N   = *(long *)argv[0];   /* Number of pixels in a data array edge */
dat = (double *)argv[1];  /* data array */
i   = (double *)argv[2];  /* i index */
j   = (double *)argv[3];  /* j index */
ff  = (double *)argv[4];  /* multiplier */
v   = *(double *)argv[5]; /* length of edge of voxel image */

q = 0.5*(v-1);
w = 1/v;

for(k=0;k<N*N;k++)
{
  i0 = floor(i[k]+0.5);
  di = i[k]-i0;

  j0 = floor(j[k]+0.5);
  dj = j[k]-j0;
  
  ip = i0+1;
  im = i0-1;
  jp = j0+1;
  jm = j0-1;
  
  Nj0 = N*j0;
  Njm = Nj0-N;
  Njp = Nj0+N;

  if((i0 >= 0) && (i0 < N) && (j0 >= 0) && (j0 < N))
  {
    dim=(q-di)*w; if(dim<0)dim=0;
    dip=(q+di)*w; if(dip<0)dip=0;
    di0=1-dim-dip;
    
    djm=(q-dj)*w; if(djm<0)djm=0;
    djp=(q+dj)*w; if(djp<0)djp=0;
    dj0=1-djm-djp;
    
                   dat[Nj0+i0]+=ff[k]*di0*dj0;
       if(jm >= 0) dat[Njm+i0]+=ff[k]*di0*djm;
       if(jp <  N) dat[Njp+i0]+=ff[k]*di0*djp;
    
    if(im >= 0)
    {              dat[Nj0+im]+=ff[k]*dim*dj0;
       if(jm >= 0) dat[Njm+im]+=ff[k]*dim*djm;
       if(jp <  N) dat[Njp+im]+=ff[k]*dim*djp;
    }
    
    if(ip < N)
    {              dat[Nj0+ip]+=ff[k]*dip*dj0;
       if(jm >= 0) dat[Njm+ip]+=ff[k]*dip*djm;
       if(jp <  N) dat[Njp+ip]+=ff[k]*dip*djp;
    }
  }
  
}

return(20);

}
