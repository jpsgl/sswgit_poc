;+
;
; Project   : renderer wrappers
;                   
; Name      : stub for disumt2_inc
;               
; Purpose   : wrappers for Thomson scattering rendering
;               
; Explanation: part of Pixon and stand-alone renderer
;               
; Use       : see documentation in code
;    
; Inputs    : (see code)
;               
; Outputs   : (see code)
;
; Keywords  : (see code)
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : display, rendering, physics
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

; disumt_inc.pro

; ----- check types and stop if there's a problem

if(1 eq 0)then begin

  type_N = (size(N))[1]
  type_h = (size(h))[3]
  type_i = (size(i))[3]
  type_j = (size(j))[3]
  type_f = (size(fizzix))[3]

  if(type_N ne 3)then stop,'ERROR dsumt_inc.pro type_N ='+string(type_N)
  if(type_h ne 5)then stop,'ERROR dsumt_inc.pro type_h ='+string(type_h)
  if(type_i ne 5)then stop,'ERROR dsumt_inc.pro type_i ='+string(type_i)
  if(type_j ne 5)then stop,'ERROR dsumt_inc.pro type_j ='+string(type_j)
  if(type_f ne 5)then stop,'ERROR dsumt_inc.pro type_f ='+string(type_f)

end

; ----- convert types

if(1 eq 1)then begin
  N = long(N)
  h = double(h)
  i = double(i)
  j = double(j)
  fizzix = double(fizzix)
end

; -----

im=float(image[*,*,la])

; disum2: psp=variable, source disum2.com and disumt2.com
ret=call_external('disumt2.so','disumt',N,h,i,j,im,fizzix,psp)
if(ret ne -20)then stop,'inc error'

image[*,*,la]=im


