;+
;
; Project   : renderer wrappers
;                   
; Name      : dsumtidl
;               
; Purpose   : stand-alone backproject for Thomson scattering rendering
;               
; Explanation: part of Pixon and stand-alone renderer
;               
; Use       : see documentation in code
;    
; Inputs    : (see code)
;               
; Outputs   : (see code)
;
; Keywords  : (see code)
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : display, rendering, physics
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; 'no pixel interpolation' and 'pixel interpolation'
;  from pxndata__dsft_default.pro
; (later rewritten into disumt.c)

PRO dsumtidl,interpolate,N,i,j,la,image,fizzix,h

    if (interpolate) then begin
        ; pixel interpolation

        for a=0,N-1 do for b=0,N-1 do begin       

            i0=round(i[a,b])
            si=i[a,b]-i0
            di=abs(si)
            si=round(si/di)

            j0=round(j[a,b])
            sj=j[a,b]-j0
            dj=abs(sj)
            sj=round(sj/dj)

            f = fizzix[a,b] 

            ii=i0
            jj=j0
            if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
              image[a,b,la]=image[a,b,la]+h[ii,jj]*f*(1-di)*(1-dj)

            ii=i0+si
            jj=j0
            if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
              image[a,b,la]=image[a,b,la]+h[ii,jj]*f*di*(1-dj)

            ii=i0
            jj=j0+sj
            if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
              image[a,b,la]=image[a,b,la]+h[ii,jj]*f*(1-di)*dj

            ii=i0+si
            jj=j0+sj
            if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
              image[a,b,la]=image[a,b,la]+h[ii,jj]*f*di*dj

        end

    end else begin
        ; no pixel interpolation

        for a=0,N-1 do for b=0,N-1 do begin
            i0=round(i[a,b])
            j0=round(j[a,b])
            if((i0 ge 0) and (i0 lt N) and                      $
               (j0 ge 0) and (j0 lt N))then                     $
              image[a,b,la]=image[a,b,la]+h[i0,j0]*fizzix[a,b]
        end

    end

END
