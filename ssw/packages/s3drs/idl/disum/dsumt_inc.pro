;+
;
; Project   : renderer wrappers
;                   
; Name      : stub for dsumt_inc
;               
; Purpose   : wrappers for Thomson scattering rendering
;               
; Explanation: part of Pixon and stand-alone renderer
;               
; Use       : see documentation in code
;    
; Inputs    : (see code)
;               
; Outputs   : (see code)
;
; Keywords  : (see code)
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : display, rendering, physics
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

; dsumt_inc.pro

ri=round(i)
rj=round(j)

; ----- check types and stop if there's a problem

if(1 eq 0)then begin

  type_N  = (size(N) )[1]
  type_h  = (size(h) )[3]
  type_ri = (size(ri))[3]
  type_rj = (size(rj))[3]
  type_f  = (size(fizzix))[3]

  if(type_N  ne 3)then stop,'ERROR dsumt_inc.pro type_N ='+string(type_N)
  if(type_h  ne 5)then stop,'ERROR dsumt_inc.pro type_h ='+string(type_h)
  if(type_ri ne 3)then stop,'ERROR dsumt_inc.pro type_ri='+string(type_ri)
  if(type_rj ne 3)then stop,'ERROR dsumt_inc.pro type_rj='+string(type_rj)
  if(type_f  ne 5)then stop,'ERROR dsumt_inc.pro type_f ='+string(type_f)

end

; ----- convert types

if(1 eq 1)then begin
  N  = long(N)
  h  = double(h)
  ri = long(ri)
  rj = long(rj)
  fizzix  = double(fizzix)
end

; -----

; this works (dsumt.c has im double)
;im=double(image[*,*,la])
;stop,'dsumt 1'
;ret=call_external('dsumt.so','dsumt',N,h,ri,rj,im,fizzix)
;stop,'dsumt 2'
;image[*,*,la]=im

; ======================================

; this works (dsumt.c has im float)

if((dbg eq 1) and (la eq dbgla))then begin
  q=where(h ne 0,count)
  if((q[0] eq dbgij) and (id eq dbgid))then begin
    fmat="('dsft: code = ',i3,' ',i3,' xx',i3)"
    print,q[0],id,la,format=fmat
    stop,' dsft bef'
  end
end

im=float(image[*,*,la])
ret=call_external('dsumt.so','dsumt',N,h,ri,rj,im,fizzix)
if(ret ne -1)then stop,'inc error'
image[*,*,la]=im

if((dbg eq 1) and (la eq dbgla))then begin
  q=where(h ne 0,count)
  if((q[0] eq dbgij) and (id eq dbgid))then begin
    fmat="('dsft: code = ',i3,' ',i3,' xx',i3)"
    print,q[0],id,la,format=fmat
    stop,' dsft aft'
  end
end

; ======================================


; this works (dsumt.c has im float)
;image=float(image)
;im=image[*,*,la]
;ret=call_external('dsumt.so','dsumt',N,h,ri,rj,im,fizzix)
;image[*,*,la]=im

; ==========

; this works sort of (dsumt.c has im float) (doesn't give same Q's)
; it stops when image becomes double somehow
;sz=size(image[*,*,la])
;if(sz(3) ne 4)then stop
;im=image[*,*,la]; or im=reform(image[*,*,la])
;ret=call_external('dsumt.so','dsumt',N,h,ri,rj,im,fizzix)
;image[*,*,la]=im

; this does not work (dsumt.c has im float)(pixon count le 0, etc)
;ret=call_external('dsumt.so','dsumt',N,h,ri,rj,image[*,*,la],fizzix)

; this does not work (dsumt.c has im float)(pixon count le 0, etc)
;image=float(image)
;ret=call_external('dsumt.so','dsumt',N,h,ri,rj,image[*,*,la],fizzix)

; this does not work (dsumt.c has im float) (segmentation fault)
;ret=call_external('dsumt.so','dsumt',N,h,ri,rj,image[0,0,la],fizzix)

; this does not work (dsumt.c has im float) (segmentation fault)
;image=float(image)
;ret=call_external('dsumt.so','dsumt',N,h,ri,rj,image[0,0,la],fizzix)


