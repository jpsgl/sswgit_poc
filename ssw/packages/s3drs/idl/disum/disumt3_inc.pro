;+
;
; Project   : renderer wrappers
;                   
; Name      : disumt3_inc
;               
; Purpose   : wrappers for Thomson scattering rendering
;               
; Explanation: part of Pixon and stand-alone renderer
;               
; Use       : see documentation in code
;    
; Inputs    : (see code)
;               
; Outputs   : (see code)
;
; Keywords  : (see code)
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : display, rendering, physics
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

pro disumt3_inc,image,N,h,i,j,fizzix,la,machine,g
; version 05

; This program implements a C program which back-projects a footprint
; around each pixel in
; the h array into the image array and adds the projected value to
; the image array.
;
; input   image[N,N,N]  
;         N             size of arrays
;         h[N,N]        data array to be back projected onto image
;         i[N,N],j[N,N] the center of voxel ff[m,n] is projected to
;                         dat[i[m,n],j[m,n]] (note: i and j are floating point,
;                         since the center of ff[m,n] may not be projected to
;                         the center of a dat pixel.
;         fizzix
;-------------------------------------

; ----- check types and stop if there's a problem

;if(1 eq 0)then begin
;
;  type_N = (size(N))[1]
;  type_h = (size(h))[3]
;  type_i = (size(i))[3]
;  type_j = (size(j))[3]
;  type_f = (size(fizzix))[3]
;
;  if(type_N ne 3)then stop,'ERROR dsumt_inc.pro type_N ='+string(type_N)
;  if(type_h ne 5)then stop,'ERROR dsumt_inc.pro type_h ='+string(type_h)
;  if(type_i ne 5)then stop,'ERROR dsumt_inc.pro type_i ='+string(type_i)
;  if(type_j ne 5)then stop,'ERROR dsumt_inc.pro type_j ='+string(type_j)
;  if(type_f ne 5)then stop,'ERROR dsumt_inc.pro type_f ='+string(type_f)
;
;end

; ----- convert types (PIXON sometimes changes them)

;if(1 eq 1)then begin
  N = long(N)               ; Number of pixels in data array edge
  h = double(h)             ; data array
  i = double(i)             ; i index
  j = double(j)             ; j index
  fizzix = double(fizzix)   ; physical parameter
;end

; -----

im=float(image[*,*,la])	; ??? can this be done as part of call?

; disum1: psp=1 automatically, source disum1.com and disumt3.com

;s_disumt3='/net/corona/data/cplex3/reiser/pixon_10a/disumt3.so'
;s_disumt3='/net/mercury/data1/pixxon/Tomography/old/disumt3.so'
if (getenv('disumlib') eq '') then disum_so_loc,'disumt3.so'
;  setenv,'pixonlib=/net/mercury/data1/pixxon/Tomography/old/'
s_disumt3=getenv('disumlib')+'disumt3.so'


;;stop,'calling disumt3'

;if (machine eq 'pixxon1' or machine eq 'pixxon2' or machine eq 'pixxon3') then begin
;    ret=call_external(s_disumt3,'disumt',N,h,i,j,im,fizzix,g) ; red hat cc compiler    
;end else if (machine eq 'nike') then begin
;    ret=call_external('disumt3n.so','disumt',N,h,i,j,im,fizzix) ; ares gcc compiler
;end else begin
;    print,'Warning, in disumt3_inc.pro machine '+machine+' not known, guessing'
;    ret=call_external(s_disumt3,'disumt',N,h,i,j,im,fizzix,g) ; red hat cc compiler    
;end

 ret=call_external(s_disumt3,'disumt',N,h,i,j,im,fizzix,g)

if(ret ne -10)then stop,'inc error'

;;stop,'returned from disumt3'

image[*,*,la]=im	; ??? can this be done as part of call?

return
end

