;+
;
; Project   : renderer wrappers
;                   
; Name      : stub for disum2_inc
;               
; Purpose   : wrappers for Thomson scattering rendering
;               
; Explanation: part of Pixon and stand-alone renderer
;               
; Use       : see documentation in code
;    
; Inputs    : (see code)
;               
; Outputs   : (see code)
;
; Keywords  : (see code)
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : display, rendering, physics
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

; disum_inc.pro

ff=image[*,*,la]*fizzix

; ----- check types and stop if there's a problem

if(1 eq 0)then begin

  type_N   = (size(N)  )[1]
  type_dat = (size(dat))[3]
  type_i   = (size(i)  )[3]
  type_j   = (size(j)  )[3]
  type_ff  = (size(ff) )[3]

  if(type_N   ne 3)then stop,'ERROR dsum_inc.pro type_N   ='+string(type_N)
  if(type_dat ne 5)then stop,'ERROR dsum_inc.pro type_dat ='+string(type_dat)
  if(type_i   ne 5)then stop,'ERROR dsum_inc.pro type_i   ='+string(type_i)
  if(type_j   ne 5)then stop,'ERROR dsum_inc.pro type_j   ='+string(type_j)
  if(type_ff  ne 5)then stop,'ERROR dsum_inc.pro type_ff  ='+string(type_ff)

end

;=====

;N=2
;dat=[[0,0],[0,0]]
;ff=[[1,0],[0,0]]
;i=dat
;j=dat

;=====

; ----- convert types

if(1 eq 1)then begin
  N   = long(N)
  dat = double(dat)
  i   = double(i)
  j   = double(j)
  ff  = double(ff)
end

; -----

;q=where(ff ne ff,count)
;if(count gt 0)then stop,'NaNs in ff'

; disum2: psp=variable, source disum2.com and disumt2.com
;ret=call_external('disum2.so','disum',N,dat,i,j,ff,psp)

s_disum='/net/corona/data/cplex3/reiser/pixon_10a/disum2.so'

;case machine of
;'pixxon':  ret=call_external(s_disum,'disum',N,dat,i,j,ff,psp)   ; red hat cc compiler
;'pixxon2': ret=call_external(s_disum,'disum',N,dat,i,j,ff,psp)   ; red hat cc compiler
;'pixxon3': ret=call_external(s_disum,'disum',N,dat,i,j,ff,psp)   ; red hat cc compiler
;else:        stop,'Error disum1_inc.pro machine '+machine+' not known'
;end
ret=call_external(s_disum,'disum',N,dat,i,j,ff,psp) 
if(ret ne 20)then stop,'inc error'


;q=where(dat ne dat,count)
;if(count gt 0)then print,'count=',count,' dat[',q[0],']=Nan'
;if(count eq N*N)then stop,'dat is all NaNs'

