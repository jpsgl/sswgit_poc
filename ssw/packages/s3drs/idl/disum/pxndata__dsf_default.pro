;+
;
; Project   : renderer wrappers
;                   
; Name      : pxndata::DSF
;               
; Purpose   : updated version of Pixon data object
;               
; Explanation: part of Pixon and stand-alone renderer
;               
; Use       : see documentation in code
;    
; Inputs    : (see code)
;               
; Outputs   : (see code)
;
; Keywords  : (see code)
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : display, rendering, physics
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

FUNCTION PXNdata::DSF            $
                , image          $
                ,_REF_EXTRA = e    ; Extra variables (by reference)

;data sampling function electron density 3d
; uses small memory modifications, extended sun, finite observer
; cartesian image

common de_com,fa,fb,fc,tau,tau2
common debugs,interp,phys,tdsf,tdsft

dbg=0		; 0: no debug  1: stop for code
dbgid=0
dbgabc=2304
dbgla=9

t0=systime(-1)

; -----

id       = self.id
fprf     = (*self.user).fprf
sat      = (*self.user).sat             ; satellite number (0,1, etc)
polar    = (*self.user).polar           ; the polarization (0:tangential 1:radial 2:sum)
lla0     = (*self.user).lla0              ; rotated sun center
rho2     = (*self.user).rho2            ; rho^2 array
rhos2    = (*self.user).rhos2	        ; the rho_s^2 array
psLs     = (*self.user).psLs            ; the ps*Ls array
psLs2    = (*self.user).psLs2           ; the (ps*Ls)^2 array
mu       = (*self.user).mu
nu       = (*self.user).nu
i        = (*self.user).i
jc       = (*self.user).jc
K2       = (*self.user).K2
u        = (*self.user).u
d2       = (*self.user).d2

N  = n_elements(image[*,0,0])

; ----- 

dat=dblarr(N,N)

case polar of

    't': begin                  ; tangential polarization

        case sat of
   
            0: begin            ; satellite 0  id=0
                for la = 0,N-1 do begin
                    z2  = d2*(la-lla0[2])^2
                    r2  = round((rho2+z2)/d2)
                    rs2 = rhos2+tau2*z2
                    j   = jc+la/K2
                    fizzix = (fa[r2]/rs2)*phys + (1-phys)

;---------------------------

                    case interp of

                        0: begin ; no pixel interpolation
                            
                            for a=0,N-1 do for b=0,N-1 do begin             
                                i0=round(i[a,b])
                                j0=round(j[a,b])
                                if((i0 ge 0) and (i0 lt N) and            $
                                   (j0 ge 0) and (j0 lt N))then           $
                                  dat[i0,j0]=dat[i0,j0]+                 $
                                  image[a,b,la]*fizzix[a,b]
                            end
                        end

                        1: begin ; pixel interpolation 

                            for a=0,N-1 do for b=0,N-1 do begin

                                i0=round(i[a,b])
                                si=i[a,b]-i0
                                di=abs(si)
                                si=round(si/di)
                                
                                j0=round(j[a,b])
                                sj=j[a,b]-j0
                                dj=abs(sj)
                                sj=round(sj/dj)

                                f = image[a,b,la]*fizzix[a,b]
                                
                                ii=i0
                                jj=j0
                                if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $ 
                                  dat[ii,jj]=dat[ii,jj]+f*(1-di)*(1-dj)
                                
                                ii=i0+si
                                jj=j0
                                if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $ 
                                  dat[ii,jj]=dat[ii,jj]+f*di*(1-dj)
                                
                                ii=i0
                                jj=j0+sj
                                if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $ 
                                  dat[ii,jj]=dat[ii,jj]+f*(1-di)*dj
                                
                                ii=i0+si
                                jj=j0+sj
                                if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $ 
                                  dat[ii,jj]=dat[ii,jj]+f*di*dj
                                
                            end
                        end
                        
                        2: begin ; C program - no interpolation
@dsum_inc
                        end
                        
                        3: begin ; C program - with interpolation (v=1 or v=?)
@disum_inc
                        end
                        
                        else: stop,'gack'
                    end
                    
                    if((dbg eq 1) and (la eq dbgla))then begin
                        q=where(image ne 0)
                        if((id eq dbgid) and (q[0] eq dbgabc))then begin
                            fmat="('dsf: code = x',i3,' ',i3)"
                            print,id,q[0],format=fmat
                            stop,' dsf'
                        end
                    end
                    
;---------------------------
                    
                end
            end
            
            1: begin            ; satellite 1  id=2
                for la = 0,N-1 do begin
                    z2  = d2*(la-lla0[2])^2
                    r2  = round((rho2+z2)/d2)
                    rs2 = rhos2+tau2*z2
                    j   = jc+la/K2
                    fizzix = (fa[r2]/rs2)*phys + (1-phys)
                    
;---------------------------
                    
                    case interp of
                        
                        0: begin ; no pixel interpolation
                            
                            for a=0,N-1 do for b=0,N-1 do begin             
                                i0=round(i[a,b])
                                j0=round(j[a,b])
                                if((i0 ge 0) and (i0 lt N) and            $
                                   (j0 ge 0) and (j0 lt N))then           $
                                  dat[i0,j0]=dat[i0,j0]+                 $
                                  image[a,b,la]*fizzix[a,b]
                            end
                        end
                        
                        1: begin ; pixel interpolation
                            
                            for a=0,N-1 do for b=0,N-1 do begin       
                                
                                i0=round(i[a,b])
                                si=i[a,b]-i0
                                di=abs(si)
                                si=round(si/di)
                                
                                j0=round(j[a,b])
                                sj=j[a,b]-j0
                                dj=abs(sj)
                                sj=round(sj/dj)
                                
                                f = image[a,b,la]*fizzix[a,b]
                                
                                ii=i0
                                jj=j0
                                if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
                                  dat[ii,jj]=dat[ii,jj]+f*(1-di)*(1-dj)
                                
                                ii=i0+si
                                jj=j0
                                if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
                                  dat[ii,jj]=dat[ii,jj]+f*di*(1-dj)
                                
                                ii=i0
                                jj=j0+sj
                                if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
                                  dat[ii,jj]=dat[ii,jj]+f*(1-di)*dj
                                
                                ii=i0+si
                                jj=j0+sj
                                if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
                                  dat[ii,jj]=dat[ii,jj]+f*di*dj
                                
                            end
                        end
                        
                        2: begin ; C program
@dsum_inc
                        end
                        
                        3: begin ; C program - with interpolation
@disum_inc
                        end
                        
                        else: stop,'gack'
                    end
                    
                    if((dbg eq 1) and (la eq dbgla))then begin
                        q=where(image ne 0)
                        if((id eq dbgid) and (q[0] eq dbgabc))then begin
                            fmat="('dsf: code = x',i3,' ',i3)"
                            print,id,q[0],format=fmat
                            stop,' dsf'
                        end
                    end
                    
;---------------------------
                    
                end
            end
            
        end
    end
    
    'r': begin                  ; radial polarization
        
        case sat of
            
            0: begin            ; satellite 0  id=1
                for la=0,N-1 do begin
                    z2  = d2*(la-lla0[2])^2
                    r2  = rho2+z2
                    rs2 = rhos2+tau2*z2
                    cx2 = (r2*tau2-2*tau*psLs+psLs2/r2)/rs2       
                    r2  = round(r2/d2)
                    j   = jc+la/K2
                    fizzix=((cx2*fb[r2]+fc[r2])/rs2)*phys + (1-phys)
                    
;---------------------------
                    
                    case interp of
                        
                        0: begin ; no pixel interpolation
                            
                            for a=0,N-1 do for b=0,N-1 do begin             
                                i0=round(i[a,b])
                                j0=round(j[a,b])
                                if((i0 ge 0) and (i0 lt N) and            $
                                   (j0 ge 0) and (j0 lt N))then           $
                                  dat[i0,j0]=dat[i0,j0]+                 $
                                  image[a,b,la]*fizzix[a,b]
                            end
                        end
                        
                        1: begin ; pixel interpolation
                            
                            for a=0,N-1 do for b=0,N-1 do begin       
                                
                                i0=round(i[a,b])
                                si=i[a,b]-i0
                                di=abs(si)
                                si=round(si/di)
                                
                                j0=round(j[a,b])
                                sj=j[a,b]-j0
                                dj=abs(sj)
                                sj=round(sj/dj)
                                
                                f = image[a,b,la]*fizzix[a,b]
                                
                                ii=i0
                                jj=j0
                                if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
                                  dat[ii,jj]=dat[ii,jj]+f*(1-di)*(1-dj)
                                
                                ii=i0+si
                                jj=j0
                                if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
                                  dat[ii,jj]=dat[ii,jj]+f*di*(1-dj)
                                
                                ii=i0
                                jj=j0+sj
                                if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
                                  dat[ii,jj]=dat[ii,jj]+f*(1-di)*dj
                                
                                ii=i0+si
                                jj=j0+sj
                                if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
                                  dat[ii,jj]=dat[ii,jj]+f*di*dj
                                
                            end
                        end
                        
                        2: begin ; C program
@dsum_inc
                        end
                        
                        3: begin ; C program - with interpolation
@disum_inc
                        end
                        
                        else: stop,'gack'
                    end
                    
                    if((dbg eq 1) and (la eq dbgla))then begin
                        q=where(image ne 0)
                        if((id eq dbgid) and (q[0] eq dbgabc))then begin
                            fmat="('dsf: code = x',i3,' ',i3)"
                            print,id,q[0],format=fmat
                            stop,' dsf'
                        end
                    end
                    
;---------------------------
                    
                end
            end
            
            1: begin            ; satellite 1  id=3
                for la=0,N-1 do begin
                    z2  = d2*(la-lla0[2])^2
                    r2  = rho2+z2
                    rs2 = rhos2+tau2*z2
                    cx2 = (r2*tau2-2*tau*psLs+psLs2/r2)/rs2       
                    r2  = round(r2/d2)
                    j   = jc+la/K2
                    fizzix=((cx2*fb[r2]+fc[r2])/rs2)*phys + (1-phys)
                    
;---------------------------
                    
                    case interp of
                        
                        0: begin ; no pixel interpolation
                            
                            for a=0,N-1 do for b=0,N-1 do begin             
                                i0=round(i[a,b])
                                j0=round(j[a,b])
                                if((i0 ge 0) and (i0 lt N) and            $
                                   (j0 ge 0) and (j0 lt N))then           $
                                  dat[i0,j0]=dat[i0,j0]+                 $
                                  image[a,b,la]*fizzix[a,b]
                            end
                        end
                        
                        1: begin ; pixel interpolation
                            
                            for a=0,N-1 do for b=0,N-1 do begin       
                                
                                i0=round(i[a,b])
                                si=i[a,b]-i0
                                di=abs(si)
                                si=round(si/di)
                                
                                j0=round(j[a,b])
                                sj=j[a,b]-j0
                                dj=abs(sj)
                                sj=round(sj/dj)
                                
                                f = image[a,b,la]*fizzix[a,b]
                                
                                ii=i0
                                jj=j0
                                if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
                                  dat[ii,jj]=dat[ii,jj]+f*(1-di)*(1-dj)
                                
                                ii=i0+si
                                jj=j0
                                if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
                                  dat[ii,jj]=dat[ii,jj]+f*di*(1-dj)
                                
                                ii=i0
                                jj=j0+sj
                                if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
                                  dat[ii,jj]=dat[ii,jj]+f*(1-di)*dj
                                
                                ii=i0+si
                                jj=j0+sj
                                if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
                                  dat[ii,jj]=dat[ii,jj]+f*di*dj
                                
                            end
                        end
                        
                        2: begin ; C program
@dsum_inc
                        end
                        
                        3: begin ; C program - with interpolation
@disum_inc
                        end
                        
                        else: stop,'gack'
                    end
                    
                    if((dbg eq 1) and (la eq dbgla))then begin
                        q=where(image ne 0)
                        if((id eq dbgid) and (q[0] eq dbgabc))then begin
                            fmat="('dsf: code = x',i3,' ',i3)"
                            print,id,q[0],format=fmat
                            stop,' dsf'
                        end
                    end
                    
;---------------------------
                    
                end
            end
            
        end
    end
    
end

; ----- convolution

fdat = self->rfft(dat)
dat  = self->H(fdat,fprf)

; -----

tdsf=tdsf+(systime(-1)-t0)

return, dat 
end

