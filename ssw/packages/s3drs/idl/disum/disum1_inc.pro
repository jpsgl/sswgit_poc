;+
;
; Project   : renderer wrappers
;                   
; Name      : disum1_inc
;               
; Purpose   : wrappers for Thomson scattering rendering
;               
; Explanation: part of Pixon and stand-alone renderer
;               
; Use       : see documentation in code
;    
; Inputs    : (see code)
;               
; Outputs   : (see code)
;
; Keywords  : (see code)
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : display, rendering, physics
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

pro disum1_inc,ff,N,dat,i,j,machine
; version 06

; This program implements a C program which projects each voxel in
; the ff array onto the dat array and adds the projected value to
; the dat array at a "footprint" about that point.
;
; input   ff[N,N]       array being projected
;         N             size of arrays
;         dat[N,N]      array onto which ff will be projected and summed
;         i[N,N],j[N,N] the center of voxel ff[m,n] is projected to
;                         dat[i[m,n],j[m,n]] (note: i and j are floating point,
;                         since the center of ff[m,n] may not be projected to
;                         the center of a dat pixel.
;-------------------------------------

; ----- check types and stop if there's a problem

;if(1 eq 0)then begin
;
;  type_N   = (size(N)  )[1]
;  type_dat = (size(dat))[3]
;  type_i   = (size(i)  )[3]
;  type_j   = (size(j)  )[3]
;  type_ff  = (size(ff) )[3]
;
;  if(type_N   ne 3)then stop,'ERROR dsum_inc.pro type_N   ='+string(type_N)
;  if(type_dat ne 5)then stop,'ERROR dsum_inc.pro type_dat ='+string(type_dat)
;  if(type_i   ne 5)then stop,'ERROR dsum_inc.pro type_i   ='+string(type_i)
;  if(type_j   ne 5)then stop,'ERROR dsum_inc.pro type_j   ='+string(type_j)
;  if(type_ff  ne 5)then stop,'ERROR dsum_inc.pro type_ff  ='+string(type_ff)
;
;end

; ----- convert types (PIXON sometimes changes them)

;if(1 eq 1)then begin
  N   = long(N)
  dat = double(dat)
  i   = double(i)
  j   = double(j)
  ff  = double(ff)
;end

; -----

; disum1: psp=1 automatically, source disum1.com and disumt1.com

;s_disum1='/net/corona/data/cplex3/reiser/pixon_10a/disum1.so'
;s_disum1='/net/mercury/data1/pixxon/Tomography/old/disum1.so'
if (getenv('disumlib') eq '') then disum_so_loc,'disum1.so'
;  setenv,'disumlib=/net/mercury/data1/pixxon/Tomography/old/'
s_disum1=getenv('disumlib')+'disum1.so'

;case machine of
;'pixxon1': ret=call_external(s_disum1,'disum',N,dat,i,j,ff)   ; red hat cc compiler
;'pixxon2': ret=call_external(s_disum1,'disum',N,dat,i,j,ff)   ; red hat cc compiler
;'pixxon3': ret=call_external(s_disum1,'disum',N,dat,i,j,ff)   ; red hat cc compiler
;'nike':      ret=call_external('disum1n.so','disum',N,dat,i,j,ff)  ; ares gcc compiler
;else:        stop,'Error disum1_inc.pro machine '+machine+' not known'
;end
 ret=call_external(s_disum1,'disum',N,dat,i,j,ff) 

if(ret ne 10)then stop,'inc error' ; verify the right c program has been called

return
end

