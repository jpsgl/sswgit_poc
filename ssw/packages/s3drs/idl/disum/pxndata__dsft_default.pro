;+
;
; Project   : renderer wrappers
;                   
; Name      : PXNdata::DSFT
;               
; Purpose   : updated version of Pixon data object
;               
; Explanation: part of Pixon and stand-alone renderer
;               
; Use       : see documentation in code
;    
; Inputs    : (see code)
;               
; Outputs   : (see code)
;
; Keywords  : (see code)
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : display, rendering, physics
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

FUNCTION PXNdata::DSFT                $
                ,data                 $ ;data-like object
                ,_REF_EXTRA = e         ;extra variables

; uses small memory modifications

common de_com,fa,fb,fc,tau,tau2
common debugs,interp,phys,tdsf,tdsft

dbg=0
dbgij = 00
dbgid = 0
dbgla = 8

t0=systime(-1)

; -----

image = MAKE_ARRAY(SIZE = *self.isz)

id       = self.id
fprf     = (*self.user).fprf
sat      = (*self.user).sat             ; satellite number (0,1, etc.)
polar    = (*self.user).polar           ; the polarization (0:tangential 1:radial 2:sum)
lla0     = (*self.user).lla0            ; indices of sun center
rho2     = (*self.user).rho2            ; rho^2 array
rhos2    = (*self.user).rhos2	        ; the rho_s^2 array
psLs     = (*self.user).psLs            ; the ps*Ls array
psLs2    = (*self.user).psLs2           ; the (ps*Ls)^2 array
mu       = (*self.user).mu
nu       = (*self.user).nu
i        = (*self.user).i
jc       = (*self.user).jc
K2       = (*self.user).K2
u        = (*self.user).u
d2       = (*self.user).d2

N  = n_elements(data[*,0,0])

stop

; ----- de-convolution

h = self->HT(data,fprf)

;if((size(data))[0] eq 3)then h=reform(data[*,*,id]) else h=data

; ----- de-rotate and multiply by radial and angular functions

case polar of

't': begin		; tangential polarization

   case sat of
   
   0: begin                    ; satellite 0 id=0
      for la=0,N-1 do begin
        z2  = d2*(la-lla0[2])^2
        r2  = round((rho2+z2)/d2)
        rs2 = rhos2+tau2*z2
        j   = jc+la/K2
        fizzix=(fa[r2]/rs2)*phys + (1-phys)

; ---------------------------

case interp of

0: begin			; no pixel interpolation
for a=0,N-1 do for b=0,N-1 do begin
  i0=round(i[a,b])
  j0=round(j[a,b])
  if((i0 ge 0) and (i0 lt N) and                      $
     (j0 ge 0) and (j0 lt N))then                     $
     image[a,b,la]=image[a,b,la]+h[i0,j0]*fizzix[a,b]
end
end

1: begin			; pixel interpolation

for a=0,N-1 do for b=0,N-1 do begin       

  i0=round(i[a,b])
  si=i[a,b]-i0
  di=abs(si)
  si=round(si/di)

  j0=round(j[a,b])
  sj=j[a,b]-j0
  dj=abs(sj)
  sj=round(sj/dj)

  f = fizzix[a,b] 

  ii=i0
  jj=j0
  if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
    image[a,b,la]=image[a,b,la]+h[ii,jj]*f*(1-di)*(1-dj)

  ii=i0+si
  jj=j0
  if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
    image[a,b,la]=image[a,b,la]+h[ii,jj]*f*di*(1-dj)

  ii=i0
  jj=j0+sj
  if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
    image[a,b,la]=image[a,b,la]+h[ii,jj]*f*(1-di)*dj

  ii=i0+si
  jj=j0+sj
  if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
    image[a,b,la]=image[a,b,la]+h[ii,jj]*f*di*dj

end
end

2: begin   ; C program - no interpolation
@dsumt_inc
   end

3: begin   ; C program - with interpolation (v=1 or v=?)
@disumt_inc
   end

else: stop,'gack'
end

if((dbg eq 1) and (la eq dbgla))then begin
  q=where(h ne 0,count)
  if((q[0] eq dbgij) and (id eq dbgid))then begin
    fmat="('dsft: code = ',i3,' ',i3,' xx',i3)"
    print,q[0],id,la,format=fmat
    stop,' dsft'
  end
end

; ---------------------------

      end
      end

   1: begin                    ; satellite 1  id=2
      for la=0,N-1 do begin
        z2  = d2*(la-lla0[2])^2
        r2  = round((rho2+z2)/d2)
        rs2 = rhos2+tau2*z2
        j   = jc+la/K2
        fizzix=(fa[r2]/rs2)*phys + (1-phys)

; ---------------------------

case interp of

0: begin                        ; no pixel interpolation
for a=0,N-1 do for b=0,N-1 do begin
  i0=round(i[a,b])
  j0=round(j[a,b])
  if((i0 ge 0) and (i0 lt N) and                      $
     (j0 ge 0) and (j0 lt N))then                     $
     image[a,b,la]=image[a,b,la]+h[i0,j0]*fizzix[a,b] 
end
end

1: begin                        ; pixel interpolation

for a=0,N-1 do for b=0,N-1 do begin       

  i0=round(i[a,b])
  si=i[a,b]-i0
  di=abs(si)
  si=round(si/di)

  j0=round(j[a,b])
  sj=j[a,b]-j0
  dj=abs(sj)
  sj=round(sj/dj)

  f = fizzix[a,b]

  ii=i0
  jj=j0
  if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
    image[a,b,la]=image[a,b,la]+h[ii,jj]*f*(1-di)*(1-dj)

  ii=i0+si
  jj=j0
  if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
    image[a,b,la]=image[a,b,la]+h[ii,jj]*f*di*(1-dj)

  ii=i0
  jj=j0+sj
  if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
    image[a,b,la]=image[a,b,la]+h[ii,jj]*f*(1-di)*dj

  ii=i0+si
  jj=j0+sj
  if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
    image[a,b,la]=image[a,b,la]+h[ii,jj]*f*di*dj

end
end

2: begin   ; C program
@dsumt_inc
   end

3: begin   ; C program - with interpolation
@disumt_inc
   end

else: stop,'gack'
end

if((dbg eq 1) and (la eq dbgla))then begin
  q=where(h ne 0,count)
  if((q[0] eq dbgij) and (id eq dbgid))then begin
    fmat="('dsft: code = ',i3,' ',i3,' xx',i3)"
    print,q[0],id,la,format=fmat
    stop,' dsft'
  end
end

; ---------------------------

      end
      end
      
   else: stop,'ERROR in dsf: sat = '+string(sat)
   end
   end

'r': begin		; radial polarization

   case sat of
   
   0: begin                    ; satellite 0  id=1
      for la=0,N-1 do begin
        z2  = d2*(la-lla0[2])^2
        r2  = rho2+z2
        rs2 = rhos2+tau2*z2
        j   = jc+la/K2
        cx2 = (r2*tau2-2*tau*psLs+psLs2/r2)/rs2
        r2  = round(r2/d2)
        fizzix=((cx2*fb[r2]+fc[r2])/rs2)*phys + (1-phys)

; ---------------------------

case interp of

0: begin                        ; no pixel interpolation
for a=0,N-1 do for b=0,N-1 do begin
  i0=round(i[a,b])
  j0=round(j[a,b])
  if((i0 ge 0) and (i0 lt N) and                  $
     (j0 ge 0) and (j0 lt N))then                 $
     image[a,b,la]=image[a,b,la]+h[i0,j0]*fizzix[a,b]
end
end

1: begin                        ; pixel interpolation

for a=0,N-1 do for b=0,N-1 do begin       

  i0=round(i[a,b])
  si=i[a,b]-i0
  di=abs(si)
  si=round(si/di)

  j0=round(j[a,b])
  sj=j[a,b]-j0
  dj=abs(sj)
  sj=round(sj/dj)

  f = fizzix[a,b]

  ii=i0
  jj=j0
  if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
    image[a,b,la]=image[a,b,la]+h[ii,jj]*f*(1-di)*(1-dj)

  ii=i0+si
  jj=j0
  if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
    image[a,b,la]=image[a,b,la]+h[ii,jj]*f*di*(1-dj)

  ii=i0
  jj=j0+sj
  if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
    image[a,b,la]=image[a,b,la]+h[ii,jj]*f*(1-di)*dj

  ii=i0+si
  jj=j0+sj
  if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
    image[a,b,la]=image[a,b,la]+h[ii,jj]*f*di*dj

end
end

2: begin   ; C program
@dsumt_inc
   end

3: begin   ; C program - with interpolation
@disumt_inc
   end

else: stop,'gack'
end

if((dbg eq 1) and (la eq dbgla))then begin
  q=where(h ne 0,count)
  if((q[0] eq dbgij) and (id eq dbgid))then begin
    fmat="('dsft: code = ',i3,' ',i3,' xx',i3)"
    print,q[0],id,la,format=fmat
    stop,' dsft'
  end
end

; ---------------------------

      end
      end

   1: begin                    ; satellite 1  id=3
      for la=0,N-1 do begin
        z2  = d2*(la-lla0[2])^2
        r2  = rho2+z2
        rs2 = rhos2+tau2*z2
        j   = jc+la/K2
        cx2 = (r2*tau2-2*tau*psLs+psLs2/r2)/rs2
        r2  = round(r2/d2)
        fizzix=((cx2*fb[r2]+fc[r2])/rs2)*phys + (1-phys)

; ---------------------------

case interp of

0: begin                        ; no pixel interpolation
for a=0,N-1 do for b=0,N-1 do begin
  i0=round(i[a,b])
  j0=round(j[a,b])
  if((i0 ge 0) and (i0 lt N) and                  $
     (j0 ge 0) and (j0 lt N))then                 $
     image[a,b,la]=image[a,b,la]+h[i0,j0]*fizzix[a,b]
end
end

1: begin                        ; pixel interpolation

for a=0,N-1 do for b=0,N-1 do begin       

  i0=round(i[a,b])
  si=i[a,b]-i0
  di=abs(si)
  si=round(si/di)

  j0=round(j[a,b])
  sj=j[a,b]-j0
  dj=abs(sj)
  sj=round(sj/dj)

  f = fizzix[a,b]

  ii=i0
  jj=j0
  if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
    image[a,b,la]=image[a,b,la]+h[ii,jj]*f*(1-di)*(1-dj)

  ii=i0+si
  jj=j0
  if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
    image[a,b,la]=image[a,b,la]+h[ii,jj]*f*di*(1-dj)

  ii=i0
  jj=j0+sj
  if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
    image[a,b,la]=image[a,b,la]+h[ii,jj]*f*(1-di)*dj

  ii=i0+si
  jj=j0+sj
  if((ii ge 0) and (ii lt N) and (jj ge 0) and (jj lt N))then $
    image[a,b,la]=image[a,b,la]+h[ii,jj]*f*di*dj

end
end

2: begin   ; C program
@dsumt_inc
   end

3: begin   ; C program - with interpolation
@disumt_inc
   end

else: stop,'gack'
end

if((dbg eq 1) and (la eq dbgla))then begin
  q=where(h ne 0,count)
  if((q[0] eq dbgij) and (id eq dbgid))then begin
    fmat="('dsft: code = ',i3,' ',i3,' xx',i3)"
    print,q[0],id,la,format=fmat
    stop,' dsft'
  end
end

; ---------------------------

      end
      end

   else: stop,'ERROR in dsf: sat = '+string(sat)
   end
   end

else: stop,'ERROR in dsf: polar = '+string(polar)
end

; -----

tdsft=tdsft+(systime(-1)-t0)

RETURN,image 
END

