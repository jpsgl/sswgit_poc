;+
;
; Project   : renderer wrappers
;                   
; Name      : stub for dsum_inc
;               
; Purpose   : wrappers for Thomson scattering rendering
;               
; Explanation: part of Pixon and stand-alone renderer
;               
; Use       : see documentation in code
;    
; Inputs    : (see code)
;               
; Outputs   : (see code)
;
; Keywords  : (see code)
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : display, rendering, physics
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

; dsum_inc.pro

ri=round(i)
rj=round(j)
ff=image[*,*,la]*fizzix

; ----- check types and stop if there's a problem

if(1 eq 0)then begin

  type_N   = (size(N)  )[1]
  type_dat = (size(dat))[3]
  type_ri  = (size(ri) )[3]
  type_rj  = (size(rj) )[3]
  type_ff  = (size(ff) )[3]

  if(type_N   ne 3)then stop,'ERROR dsum_inc.pro type_N ='+string(type_N)
  if(type_dat ne 5)then stop,'ERROR dsum_inc.pro type_dat ='+string(type_dat)
  if(type_ri  ne 3)then stop,'ERROR dsum_inc.pro type_ri='+string(type_ri)
  if(type_rj  ne 3)then stop,'ERROR dsum_inc.pro type_rj='+string(type_rj)
  if(type_ff  ne 5)then stop,'ERROR dsum_inc.pro type_ff='+string(type_ff)

end

; ----- convert types

if(1 eq 1)then begin
  N=long(N)  
  dat=double(dat)
  ri=long(ri)
  rj=long(rj)
  ff=double(ff)
end

; -----

if((dbg eq 1) and (la eq dbgla))then begin
  q=where(image ne 0)
  if((id eq dbgid) and (q[0] eq dbgabc))then begin
    fmat="('dsf: code = x',i3,' ',i3)"
    print,id,q[0],format=fmat
    stop,' dsf bef'
  end
end

ret=call_external('dsum.so','dsum',N,dat,ri,rj,ff)
if(ret ne 1)then stop,'inc error'

if((dbg eq 1) and (la eq dbgla))then begin
  q=where(image ne 0)
  if((id eq dbgid) and (q[0] eq dbgabc))then begin
    fmat="('dsf: code = x',i3,' ',i3)"
    print,id,q[0],format=fmat
    stop,' dsf aft'
  end
end


