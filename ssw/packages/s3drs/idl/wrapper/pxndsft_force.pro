;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : PXNdata:DSFT
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;
; This is our modification of the Pixon object routine for DSFT
; (backprojection), modded to use our whitelight versions.
;               
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            

FUNCTION PXNdata::DSFT                $
                ,data                 $ ;data-like object
                ,_REF_EXTRA = e         ;extra variables

common debugs,phys,tdsf,tdsft,dbg
; Sandy hates using common blocks, but put this one in
; so that we avoid re-re-re-re-allocating storage for the
; often-reassigned often-mundged copy of the base image
common pixonstorage, imagetemp

; 'fast' is now extracted from the self structure
;fast=1; 1 = fast but sloppy, 0 = 4x slower but more accurate

; debug flags
dbgij = 0
dbgid = 0
dbgla = 0

; -----

N  = n_elements(data[*,0,0])

; note we need only make this once, saves on memory usage in large N cases
if (n_elements(imagetemp) eq 0) then begin
;    print,'does not exist, creating'
    imagetemp = MAKE_ARRAY(SIZE = *self.isz)
end else if (n_elements(imagetemp[*,0,0]) ne N) then begin
    ; common exists but is wrong size, delete
;    print,'wrong size, recreating'
    imagetemp = MAKE_ARRAY(SIZE = *self.isz)
end else begin
;    print,'reusing'
    imagetemp=imagetemp*0
end

id       = self.id
fprf     = (*self.user).fprf
roco     = (*self.user).roco            ; for near z axis stuff
fast     = (*self.user).fast		; to use fast or slowerbutaccurate rot
polar    = (*self.user).polar           ; the polarization (t,r,p, or b)
c2z      = (*self.user).c2z             ; cos(zeta) squared (for polar=p)
s2z      = (*self.user).s2z             ; sin(zeta) squared (for polar=p)
lla0     = (*self.user).lla0            ; indices of sun center
rho2     = (*self.user).rho2            ; rho^2 array
rhos2    = (*self.user).rhos2	        ; the rho_s^2 array
psLs     = (*self.user).psLs            ; the ps*Ls array
psLs2    = (*self.user).psLs2           ; the (ps*Ls)^2 array
i        = (*self.user).i
jc       = (*self.user).jc
K2       = (*self.user).K2
d2       = (*self.user).d2
fa       = (*self.user).fa
fb       = (*self.user).fb
fc       = (*self.user).fc
rmin2    = (*self.user).rmin2
tau      = (*self.user).tau
tau2     = (*self.user).tau2
interp   = (*self.user).interp
machine  = (*self.user).machine
g        = (*self.user).g
theta    = (*self.user).theta ; Sandy's implemenation of spherical coords
gamma = g ; easier to grep on

t0=systime(-1)
psp=2

; rotate (for close to z axis stuff). No need to rotate image, it doesn't
; contain anything yet.

if(roco eq 1)then lla0=[0.5*(N-1)-lla0[2],lla0[1],lla0[0]]

; usually assumes sun-center=optical-center=image-center,
;if (theta ne 0) then lla0=lla0

; ----- de-convolution

h = self->HT(data,fprf)

;if((size(data))[0] eq 3)then h=reform(data[*,*,id]) else h=data

; ----- de-rotate and multiply by radial and angular functions

case strlowcase(polar) of

    'n': begin     ; no Thompson scattering-- test case only

        for la=0,N-1 do begin
            z2  = d2*(la-lla0[2])^2
            r2  = rho2+z2
            rs2 = rhos2+tau2*z2
            j   = jc+la/K2
            cx2 = (r2*tau2-2*tau*psLs+psLs2/r2)/rs2
            r2  = round((r2-rmin2)/d2)
            fizzix=((fa[r2]+cx2*fb[r2]+fc[r2])/rs2)*phys + (1-phys)
            fizzix=fizzix*0.0+1.0; throw it away and just go flat
            
            ; native IDL version of '2', no interpolation
            dsumtidl,0,N,i,j,la,imagetemp,fizzix,h

        end

    end

; =============== PLANE POLARIZATION ===================

    'p': begin                  ; plane
        
        ht=h*c2z
        hr=h*s2z
        
        for la=0,N-1 do begin
            z2  = d2*(la-lla0[2])^2
            r2  = rho2+z2
            rs2 = rhos2+tau2*z2
            j   = jc+la/K2
            cx2 = (r2*tau2-2*tau*psLs+psLs2/r2)/rs2
            r2  = round((r2-rmin2)/d2)
            
            if(interp ne 3 and interp ne 1)then stop,'dsft error'
            
            fizzixt=(fa[r2]/rs2)*phys + (1-phys) ; tangential
            if (interp eq 3) then begin
                ; C program - with interpolation v=1
                disumt1_inc,imagetemp,N,ht,i,j,fizzixt,la,machine
            end else begin
                ; native IDL version of '3', interpolation v=1
                dsumtidl,1,N,i,j,la,imagetemp,fizzixt,ht
            end
            ; disumt1_inc,imagetemp,N,ht,i,j,fizzixt,la,machine

            fizzixr=((cx2*fb[r2]+fc[r2])/rs2)*phys + (1-phys) ;radial
            if (interp eq 3) then begin
                ; C program - with interpolation v=1
                disumt1_inc,imagetemp,N,hr,i,j,fizzixr,la,machine
            end else begin
                ; native IDL version of '3', interpolation v=1
                dsumtidl,1,N,i,j,la,imagetemp,fizzixr,hr
            end
            ; disumt1_inc,imagetemp,N,hr,i,j,fizzixr,la,machine
            
; ---------------------------
            
; debug
            if((dbg eq 1) and (la eq dbgla))then begin
                q=where(h ne 0,count)
                if((q[0] eq dbgij) and (id eq dbgid))then begin
                    fmat="('dsft: code = ',i3,' ',i3,' xx',i3)"
                    print,q[0],id,la,format=fmat
                    stop,' dsft'
                end
            end
            
; ---------------------------
            
        end
    end 
    
; =============== TANGENTIAL POLARIZATION ===========
    
    't': begin                  ; tangential polarization
        
        for la=0,N-1 do begin
            z2  = d2*(la-lla0[2])^2
            r2  = round((rho2+z2-rmin2)/d2)
            rs2 = rhos2+tau2*z2
            j   = jc+la/K2
            fizzix=(fa[r2]/rs2)*phys + (1-phys)
            
; ---------------------------
            
            case interp of
                
                0: begin ; native IDL version of '2', no interpolation
                    dsumtidl,0,N,i,j,la,imagetemp,fizzix,h
                end
                
                1: begin ; native IDL version of '3', interpolation v=1
                    dsumtidl,1,N,i,j,la,imagetemp,fizzix,h
                end
                
                2: begin        ; C program - no interpolation
@dsumt_inc
                end
                
                3: begin        ; C program - with interpolation v=1
                    disumt1_inc,imagetemp,N,h,i,j,fizzix,la,machine
                end
                
                4: begin   ; C program - with interpolation v=variable
@disumt2_inc
                end
                
                5: begin        ; C program - trapezoid spreading
                    disumt3_inc,imagetemp,N,h,i,j,fizzix,la,machine,g
                end
                
                else: stop,'gack'
            end
            
; debug
;if((dbg eq 1) and (la eq dbgla))then begin
;  q=where(h ne 0,count)
;  if((q[0] eq dbgij) and (id eq dbgid))then begin
;    fmat="('dsft: code = ',i3,' ',i3,' xx',i3)"
;    print,q[0],id,la,format=fmat
;    stop,' dsft'
;  end
;end
            
; ---------------------------
            
        end
    end
    
; =============== RADIAL POLARIZATION ===============
    
    'r': begin                  ; radial polarization
        
        for la=0,N-1 do begin
            z2  = d2*(la-lla0[2])^2
            r2  = rho2+z2
            rs2 = rhos2+tau2*z2
            j   = jc+la/K2
            cx2 = (r2*tau2-2*tau*psLs+psLs2/r2)/rs2
            r2  = round((r2-rmin2)/d2)
            fizzix=((cx2*fb[r2]+fc[r2])/rs2)*phys + (1-phys)
            
; ---------------------------
            
            case interp of
                
                0: begin ; native IDL version of '2', no interpolation
                    dsumtidl,0,N,i,j,la,imagetemp,fizzix,h
                end
                
                1: begin ; native IDL version of '3', interpolation v=1
                    dsumtidl,1,N,i,j,la,imagetemp,fizzix,h
                end
                
                2: begin        ; C program
@dsumt_inc
                end
                
                3: begin        ; C program - with interpolation v=1
                    disumt1_inc,imagetemp,N,h,i,j,fizzix,la,machine
                end
                
                4: begin   ; C program - with interpolation v=variable
@disumt2_inc
                end
                
                5: begin        ; C program - trapezoid spreading
                    disumt3_inc,imagetemp,N,h,i,j,fizzix,la,machine,g
                end
                
                else: stop,'gack'
            end
            
; debug
;if((dbg eq 1) and (la eq dbgla))then begin
;  q=where(h ne 0,count)
;  if((q[0] eq dbgij) and (id eq dbgid))then begin
;    fmat="('dsft: code = ',i3,' ',i3,' xx',i3)"
;    print,q[0],id,la,format=fmat
;    stop,' dsft'
;  end
;end
            
; ---------------------------
            
        end
    end 
    
; =============== NO POLARIZATION ===================
    
    'b': begin                  ; brightness
        
        for la=0,N-1 do begin
            z2  = d2*(la-lla0[2])^2
            r2  = rho2+z2
            rs2 = rhos2+tau2*z2
            j   = jc+la/K2
            cx2 = (r2*tau2-2*tau*psLs+psLs2/r2)/rs2
            r2  = round((r2-rmin2)/d2)
            fizzix=((fa[r2]+cx2*fb[r2]+fc[r2])/rs2)*phys + (1-phys)
            
; ---------------------------
            
            case interp of
                
                0: begin ; native IDL version of '2', no interpolation
                    dsumtidl,0,N,i,j,la,imagetemp,fizzix,h
                end
                
                1: begin ; native IDL version of '3', interpolation v=1
                    dsumtidl,1,N,i,j,la,imagetemp,fizzix,h
                end
                
                2: begin        ; C program
@dsumt_inc
                end
                
                3: begin        ; C program - with interpolation v=1
                    disumt1_inc,imagetemp,N,h,i,j,fizzix,la,machine
                end
                
                4: begin   ; C program - with interpolation v=variable
@disumt2_inc
                end
                
                5: begin        ; C program - trapezoid spreading
                    disumt3_inc,imagetemp,N,h,i,j,fizzix,la,machine,g
                end
                
                else: stop,'gack'
            end
            
; debug
;if((dbg eq 1) and (la eq dbgla))then begin
;  q=where(h ne 0,count)
;  if((q[0] eq dbgij) and (id eq dbgid))then begin
;    fmat="('dsft: code = ',i3,' ',i3,' xx',i3)"
;    print,q[0],id,la,format=fmat
;    stop,' dsft'
;  end
;end
            
; ---------------------------
            
        end
    end 
    
; ===================================================
    
    else: stop,'ERROR in dsft: polar = '+string(polar)
end

; reverse the rotation. No need to do lla0, its not returned

if(roco eq 1)then for nu=0,N-1 do $
  imagetemp[*,nu,*]=rotate(reform(temporary(imagetemp[*,nu,*])),3)

;print,'dsft: ',gamma,theta
if(roco eq -1 and (theta ne 0 or gamma ne 0))then begin
;    if (theta ne 0) then print,'rotating theta'
    if (fast) then begin
;;        if (theta ne 0) then cuberot3d,imagetemp,[0.0,0.0-theta,0.0],lla0
;    if (gamma ne 0) then print,'rotating gamma'
    ; note gamma should be CCW, so reverse is CW
;;        if (gamma ne 0) then cuberot3d,imagetemp,[0.0,0.0,gamma],lla0
;;        cuberot3d,imagetemp,[0.0,0.0-theta,gamma],lla0
        cuberot3d,imagetemp,[0.0,0.0-theta,gamma]
    end else begin
;;        imagetemp=turn_3d(imagetemp,0.0,0.0-theta,0.0-gamma,/resize,$
;;                         xyzcenter=lla0,/pivot,/interp)
        imagetemp=turn_3d(imagetemp,0.0,0.0-theta,0.0-gamma,/resize,$
                         /pivot,/interp)
    end

end

; -----

tdsft=tdsft+(systime(-1)-t0)

;print,'return from dsft'

return,imagetemp 
end


pro pxndsft_force
; just forces the compilation of the above object
end
