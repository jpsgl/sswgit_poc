;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : load_cor
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;
; This is basically a wrapper that calls either secchi_prep or
; the various lasco_prep routines so as to ingest data.  It is
; cumbersome and may need to be adjusted for different work sites.
;               
; note that besides reading in data, this routine sets the 'hdr'
; variable in case future backgrounds are needed.
; it also returns the given filename
;
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; note that besides reading in data, this routine sets the 'hdr'
; variable in case future backgrounds are needed.
; it also returns the given filename
;
; default is to use secchi_prep, set /pregen read the pre-made L1.0 DN files
;
; Although designed for cor2, it turns out this does fine for cor1
; files too, if given /cor1.
;
; By default, it assumes you are giving it an exact date, but if
; you specify /findnearest, it will load the nearest file to
; that date by calling 'find_cdata' first (also overwriting 'cmedate'
; in the process, intentially, to indicate the 'more accurate' time found).
;
;
; It can also make a difference image or a background-subtracted
; image.  Just pass it 'subtract='back' or 'subtract=priordate' to
; do this in one pass.
;
; If you do not give it a value of 'N', it returns the native full resolution
;
; If told /polar, it returns the 3 individual polarities
;
; If given indypair=1,2 or 3, returns a single polarization as
; follows:   1= A frame 0 or B frame 1 (both 270 degrees),
;            2= A frame 1 or B frame 2 (both 30 degrees),
;            3= A frame 2 or B frame 0 (both 150 degrees)
;
; Giving /force forces secchi_prep even if a previously cached
; version exists, otherwise it will look for the appropriate tB/rad/tang
; file in the current working directory first.
;
; If given /local, it looks for origin FITS files in the local directory
; instead of the default secchi path.
;
; If given /massimg, generates mass images instead of brightness images.
; Note that /massimg with the /rtpair it will fail (has no meaning).
;

FUNCTION load_cor,cmedate,ab,N=N,hdr=hdr,fname=fname,pregen=pregen,cor1=cor1,$
                  subtract=subtract,pointer=pointer,force=force,$
                  pb=pb,radial=radial,tangential=tangential,rtpair=rtpair,$
                  polar=polar,indypair=indypair,massimg=massimg,$
                  findnearest=findnearest,local=local

  ab=strlowcase(ab)
  pointer=KEYWORD_SET(pointer)

  local=KEYWORD_SET(local)
  force=KEYWORD_SET(force)
  polar=KEYWORD_SET(polar)
  pb=KEYWORD_SET(pb)
  rtpair=KEYWORD_SET(rtpair)
  radial=KEYWORD_SET(radial)
  tangential=KEYWORD_SET(tangential)
  massimg=KEYWORD_SET(massimg)
  if (massimg and rtpair) then print,'Error, no mass img created for rtpair.'

  if (n_elements(indypair) eq 0) then indypair=0
  if (polar+radial+rtpair+tangential+indypair+pb eq 0) then tb=1 else tb=0

  pregen=KEYWORD_SET(pregen); do not use secchi_prep but hunt up Nathan's set
  if (pregen and tb) then pregen=1 else pregen=0

  cor1=KEYWORD_SET(cor1)
  cor2=~cor1; if not /cor1, default is cor2 (only relevant for 'find_cdate')

  findnearest=KEYWORD_SET(findnearest)

  ;print,'Input date for ',ab,' is ',cmedate; debug

  ;if (ab eq 'a') then local=1; hack from when Cor1A data was missing

  cmebck=cmedate
  if (local eq 0 and findnearest eq 1 and cmedate ne 'back' and $
      cmedate ne 'raw') then $
    cmedate=find_cdata(cmedate,cor1=cor1,cor2=cor2,ab=ab,/justtime)

  if (cmedate eq '-1') then begin
      ; try to see if files are anywhere 
      local=1 
      cmedate=cmebck
  end

;  if (findnearest) then print,'Found nearest date as ',cmedate; debug

  if (cor1) then begin
      tag='cor1'
      stub='s4c1'
      stubalt='n4c1'
      cal='0B4c1'
  end else begin
      tag='cor2'
      stub='s4c2'
      stubalt='n4c2'
      cal='0B4c2'
  end

  cpieces=strsplit(cmedate,'_',/extract)

  if (cmedate eq 'raw') then begin

      data=fltarr(2048,2048)*0.0

  end else if (cmedate eq 'back') then begin

      data=scc_getbkgimg(hdr,/match,/totalb,/interpolate,outhdr=outhdr)
      data=data*get_msb2dn(hdr); scc_getbkgimg always returns DN, we want MSB
      wcs = fitshead2wcs(hdr) ; sanity check for buggy scc_roll_image
      if (tag_exist(wcs,'roll_angle') ne 0) then $
        scc_roll_image,hdr,data,SYSTEM='HAE',/interp,/roll
      ie=where(finite(data,/nan)); remove NaN
      if (ie[0] ne -1) then data[ie]=0

  end else if (pregen) then begin

      ; Nathan's pre-made tB images
      tbdir=getenv('SECCHI_P0')+'/'+ab+'/' + tag + '/'
      fname = tbdir + cpieces[0] + '/' + cmedate + $
        '_' + cal +strupcase(ab)+'.fts'
      if (file_exist(fname) eq 0) then begin
          print,'Missing input file CME for ',tag,' ,',cmedate
          return,-1
      end

      data=sccreadfits(fname,hdr)

      if (ab eq 'b') then data=data*2.8/2.7

      wcs = fitshead2wcs(hdr) ; sanity check for buggy scc_roll_image
      if (tag_exist(wcs,'roll_angle') ne 0) then $
        scc_roll_image,hdr,data,SYSTEM='HAE',/interp
      ie=where(finite(data,/nan)); remove NaN
      if (ie[0] ne -1) then data[ie]=0

  end else begin

      ; first check if FITS file was already created
      if (cor1) then onetwo='1' else onetwo='2'
      polarname = cmedate + '_14c' + onetwo + strupcase(ab) + '.fts'
      if (polar and file_exist(polarname)) then $
        print,'Cannot use pre-gen polars yet, prepping'

      checkfname = cmedate + '_1B4c' + onetwo + strupcase(ab) + '.fts' ; tB
      checkpname = cmedate + '_1P4c' + onetwo + strupcase(ab) + '.fts' ; tB

      tb_fname='tB_'+checkfname
      pb_fname='pB_'+checkpname

; add in checking for polars, matching, etc (do polars overwrite tBs?)

      yesfile=0
      if (tb and file_exist(tb_fname)) then yesfile=1
      ;print,tb,tb_fname,file_exist(tb_fname),yesfile
      ;print,'tb check: ',tb,file_exist(tb_fname),tb_fname
      if (pb and file_exist(pb_fname)) then yesfile=1
      ;print,'pb check: ',pb,file_exist(pb_fname),pb_fname
      if ((radial or tangential or rtpair) and $
          file_exist(tb_fname) and file_exist(pb_fname)) then yesfile=1
      ;print,'rad/tang check: ',radial,tangential
      if (force) then yesfile=0; recreate anyway if /force was given
      ; note polar trios always have to recreate, it's easier to track

      if (yesfile) then begin

          print,'Ingesting local pre-made secchi_prep FITS file'

          if (radial or tangential or rtpair) then begin

              tbdata=sccreadfits(tb_fname,hdr)
              pbdata=sccreadfits(pb_fname,hdr)

              if (rtpair) then begin
                  data=[[[(tbdata-pbdata)/2.0]],[[(tbdata+pbdata)/2.0]]]
                  hdr=[hdr,hdr]
                  hdr[0].polar=[2001]
                  hdr[1].polar=[2002]
              end else if (radial) then begin
                  data=(tbdata-pbdata)/2.0
                  hdr.polar=2001
              end else if (tangential) then begin
                  data=(tbdata+pbdata)/2.0
                  hdr.polar=2002
              end else begin
                  print,'Error, could not figure out what you wanted'
              end

          end else if (pb) then begin

              data=sccreadfits(pb_fname,hdr)

          end else begin

              ; must be tB
              data=sccreadfits(tb_fname,hdr)

          end


; checked roll, we should not adjust for it (based on comparison
; of using this technique with Feb 01 2007 images, indicating that
; roll is already being accounted for in image processing somewhere.
;          angle=0.0 - get_stereo_roll(hdr.date_obs,ab,SYSTEM='HAE')
;          sunk0=scc_sun_center(hdr)
;;          dd=hdr.date_obs
;;          save,file='sample_later'+ab+'.sav',angle,sunk0,data,dd
;          data = rot(data,angle,1.0,sunk0.xcen,sunk0.ycen,/pivot)

      end else begin

          print,'Running secchi_prep to create data.'

         ; generate L1.0 from L0.5 trios

      ; we could in theory use this, but filenames are already deducible
      ;List = SCC_READ_SUMMARY(DATE=['31-Dec-2007 01','31-Dec-2007 02'],$
      ;                        TELESCOPE='COR2',TYPE='seq',POLAR='0.0',$
      ;                        SPACECRAFT=ab,SOURCE='lz',/NOBEACON,/CHECK)
      ;fname=List[0].filename

          if (local) then tbdir='./' else $
           tbdir=getenv('secchi')+'/lz/L0/'+ab+'/seq/'+tag+'/'+cpieces[0] + '/'
          fname=tbdir + cmedate + '_' + stub + strupcase(ab) + '.fts'
          fnamealt=tbdir + cmedate + '_' + stubalt + strupcase(ab) + '.fts'
          fnamelocal='./' + cmedate + '_' + stub + strupcase(ab) + '.fts'
          fnamealtlocal='./' + cmedate + '_' + stubalt + strupcase(ab) + '.fts'

          flist=match_polarity(fname,/local,/keeppath)
;          print,fname,' A: ',flist
          if (n_elements(flist) ne 3) then $
            flist=match_polarity(fnamealt,/local,/keeppath)
;          print,fnamealt,' B: ',flist
          if (n_elements(flist) ne 3) then $
            flist=match_polarity(fnamelocal,/local,/keeppath)
;          print,fnamelocal,' C: ',flist
          if (n_elements(flist) ne 3) then $
            flist=match_polarity(fnamealtlocal,/local,/keeppath)
;          print,fnamealtlocal,' D: ',flist

;          print,'FLIST is ',flist

          if (n_elements(flist) le 1) then return,-1

          if (polar or indypair gt 0) then begin

              if (indypair gt 0) then begin
                  flist=[flist,flist[0]] ; add 4th element for B cases
                  ; indices for indypair 1->A0,B1; 2->A1,B2; 3->A2,B0 aka B3
                  if (ab eq 'a') then flist=flist[indypair-1] else $
                    flist=flist[indypair]
                  polangle=['n/a','270','30','150']
                  print,'Doing indiv. polar angle ',polangle[indypair]
              end else begin
                  print,'Doing full set of individual polarizations'
              end

              ; always recreate polars, it's easier to track and load
              ;print,'polar/indypair: ',flist
              secchi_prep,flist,hdr,data,/write_fts,/silent,/discri_pobj_on,$
                /bkgimg_off

              if (n_elements(hdr) eq 0) then return,-1 ; ERROR

          end else begin

              if (pb or radial or tangential or rtpair) then begin

                  ;print,'pb or radial or tan or rtpair: ',flist
                  secchi_prep,flist,hdr,pbdata,/write_fts,/silent,$
                    /pb,/polariz_on,/discri_pobj_on,/bkgimg_off

                  if (n_elements(hdr) eq 0) then return,-1; ERROR

                  file_move,checkpname,pb_fname,/overwrite

                  if (pb) then begin

                      data=pbdata

                  end else begin
                     ; radial or tangential or rtpair all require tB also

                      ;print,'radial or tan or rtpair: ',flist
                      secchi_prep,flist,hdr2,tbdata,/write_fts,/silent,$
                        /polariz_on,/discri_pobj_on,/bkgimg_off

                      if (n_elements(hdr) eq 0) then return,-1 ; ERROR

                      file_move,checkfname,tb_fname,/overwrite

                  ; solve: It = (tB+pB)/2, Ir=(tB-pB)/2
                      if (radial) then data=(tbdata-pbdata)/2.0 else $
                        if (tangential) then data=(tbdata+pbdata)/2.0 else $
                        data=[[[(tbdata-pbdata)/2.0]],[[(tbdata+pbdata)/2.0]]]

                      if (rtpair) then hdr=[hdr,hdr2]

                      if (radial) then hdr.polar=2001 else $
                        if (tangential) then hdr.polar=2002 else $
                        hdr.polar=[2001,2002]

                  end

              end else begin

                  ; just tB
                  secchi_prep,flist,hdr,data,/write_fts,/silent,$
                    /polariz_on,/discri_pobj_on,/bkgimg_off; tB

                  if (n_elements(hdr) eq 0) then return,-1; ERROR

                  file_move,checkfname,tb_fname,/overwrite

              end

          end
          
          ;help,hdr,polar,data

; do not need this, see earlier example in previous code block for comments
;          angle=0.0 - get_stereo_roll(hdr.date_obs,ab,SYSTEM='HAE')
;          sunk0=scc_sun_center(hdr)
;          for id=0,n_elements(data[0,0,*])-1 do $
;            data[*,*,id] = rot(data[*,*,id],angle[id],1.0,$
;                               sunk0[id].xcen,sunk0[id].ycen,/pivot)


      end

;;;;;;      scc_roll_image,hdr,data,SYSTEM='HAE'
      ;fname=hdr.filename

      ;print,'Apply spacecraft roll'
      ;help,hdr
      for i=0,n_elements(hdr)-1 do begin
          dat=data[*,*,i]
          ;help,hdr[i]
          wcs = fitshead2wcs(hdr[i]); sanity check for buggy scc_roll_image
          if (tag_exist(wcs,'roll_angle') ne 0) then $
            scc_roll_image,hdr[i],dat,SYSTEM='HAE',/interp,/roll
          data[*,*,i]=dat
      end

      ie=where(finite(data,/nan)); remove NaN
      if (ie[0] ne -1) then data[ie]=0

  end


  if (massimg) then data = calc_cme_mass2(data,hdr,box,/all)

  if (n_elements(N) ne 0) then begin
      Z=n_elements(data[0,0,*])
      data=congrid(data,N,N,Z,/inter)
  end

  ; always trim off corners, which some secchi_prep and tB do but others don't
  corners=p_getmask('corners',N)
  for i=0,n_elements(data[0,0,*])-1 do $
    data[*,*,i]=data[*,*,i]*corners


  if (n_elements(subtract) ne 0) then begin
      ; recursive call, as we'll want a background
      origtime=hdr.exptime
      hdr.exptime=1             ; required for scc_getbkgimg
      back=load_cor(subtract,ab,N=N,hdr=hdr,prep=prep,cor1=cor1,polar=polar)
      hdr.exptime=origtime
      data=data-back
  end

  if (pointer) then return,ptr_new(data) else return,data

END
