;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : p_recenter
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;               
; slides the data to get around an implementation choice in Paul's
; renderer, that the sun is in the center of the array.
;
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; slides the data to get around an implementation choice in Paul's
; renderer, that the sun is in the center of the array.
; Puts the value -9999 into any undefined pixel.
; If given an optional 'extradat' of the same size (e.g. a
; noise or mask array), produces the identical shift on that as well.
;
; Can also manually give a 'sunctr' to skip using the pixon wrapper
;

PRO test_p_recenter,N=N

   if (n_elements(N) eq 0) then N=64

   prep_3drv,pxwrapper,gamma=[0,0,0],N=N
   datum=make_array(N,N,3,/index,/float)
   pxwrapper.sr.k0[1]=ptr_new([N/4,(N-1)/2.0]); shift in X only
   pxwrapper.sr.k0[2]=ptr_new([N/4,N/4]); shift in both axes
   datum[(N-1)/2.0,(N-1)/2.0,0]=max(datum); show center as bright point
   datum[N/4,(N-1)/2.0,1]=max(datum); show center as bright point
   datum[N/4,N/4,2]=max(datum); show center as bright point
   tv_multi,datum>0,header='original',/show,title=int2str(N),fixed=128,/raw

   p_recenter,pxwrapper,datum
   tv_multi,datum>0,header='shifted',/show,title=int2str(N),fixed=128,/raw

END

; handy routine to return everywhere the data was set to -9999 by p_recenter
FUNCTION p_recenter_mask,datum,iele=iele
   if (n_elements(iele) ne 0) then masks=datum[*,*,iele]*0.0+1 else $
     masks=datum*0+1
   ie=where(round(datum) eq -9999)
   if (ie[0] ne -1) then masks[ie]=0
   return,masks
END

PRO p_recenter,datum,pxwrapper,extradat=extradat,iele=iele,$
               show=show,masks=masks,sunctr=sunctr

  show=KEYWORD_SET(show)

  Nd=n_elements(datum[0,0,*])
  if (n_elements(iele) ne 0) then begin
      istart=iele
      iend=iele
  end else begin
      istart=0
      iend=Nd-1
  end

  for id=istart,iend do begin

      Ndata=n_elements(datum[0,*,0])
      ctr=[(Ndata-1)/2.0,(Ndata-1)/2.0]

      if (n_elements(sunctr) eq 0) then sunctr=p_getk0(pxwrapper,id,/sun)
      ; newsunk0 = ctr = sunk0-offset
      offset=round(sunctr - ctr); best integer shift

      ;print,id,' sunctr is ',sunctr,' and offset is ',offset

      if (offset[0] ne 0 or offset[1] ne 0) then begin

          if (n_elements(pxwrapper) ne 0) then begin
              k0=p_getk0(pxwrapper,id)
              newk0=k0-offset
              *(pxwrapper.sr.k0[id])=k0-offset
              *(pxwrapper.sr.sunk0[id])=sunctr-offset
          end

          dnew=fltarr(Ndata,Ndata) - 9999 ; fills with undefined

          ix1=MAX([0,offset[0]])
          ix2=MIN([Ndata-1,Ndata-1+offset[0]])
          iy1=MAX([0,offset[1]])
          iy2=MIN([Ndata-1,Ndata-1+offset[1]])
          
          jx1=MAX([0,0-offset[0]])
          jx2=MIN([Ndata-1,Ndata-1-offset[0]])
          jy1=MAX([0,0-offset[1]])
          jy2=MIN([Ndata-1,Ndata-1-offset[1]])
      
      ;print,offset,Ndata
      ;print,ix1,ix2,iy1,iy2,ix2-ix1,iy2-iy1
      ;print,jx1,jx2,jy1,jy2,jx2-jx1,jy2-jy1
          dnew[jx1:jx2,jy1:jy2]=datum[ix1:ix2,iy1:iy2,id]
          datum[*,*,id]=dnew

          if (n_elements(extradat) ne 0) then begin
              dnew=dnew*0.0 - 9999 ; refill as undefined
              dnew[jx1:jx2,jy1:jy2]=extradat[ix1:ix2,iy1:iy2,id]
              extradat[*,*,id]=dnew
          end
      
      end else begin
          ;print,'no offset required'
      end

  end

  if (show) then tv_multi,datum,fixed=128,header='Shifted data',/log,oom=2

  masks=p_recenter_mask(datum)

END
