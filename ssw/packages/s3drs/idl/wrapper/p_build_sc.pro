FUNCTION p_build_sc,gname,term,note,doubleprec=doubleprec,fast=fast
;+
; $Id: p_build_sc.pro,v 1.1 2009/04/22 19:22:18 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : p_build_sc.pro
;               
; Purpose   : Populates the Pixon 'sc' structure
;               
; Explanation: Is a wrapper to put user values into the Pixon structures.
;              It puts in the user parameters and also sets
;              'reasonable' defaults for the rest of the structure
;              variables, and also initializes many structure values.
;               
; Use       : IDL> p_build_sc,gname,term,sat_name,note
;    
; Inputs    : 
;             gname : generic file name
;             term : terminal type
;             note : any note you want to include
;             /doubleprec: toggle if you want to use doubles for img/data,
;                not floats
;               
; Outputs   : the 'sc' structure
;
; Keywords  : none
;
; Calls from LASCO : none 
;
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : Simulation
;               
; Prev. Hist. : See Paul Reiser's code and notes.
;
; Written     : Sandy Antunes, NRL, Nov-Dec 2005
;               
; $Log: p_build_sc.pro,v $
; Revision 1.1  2009/04/22 19:22:18  antunes
; code relocation part 2 of 3.
;
; Revision 1.15  2009/03/31 14:57:26  antunes
; Re-org prepatory to checking into SSW.
;
; Revision 1.14  2008/12/03 19:34:27  antunes
; Neater output.
;
; Revision 1.13  2008/03/11 19:07:41  antunes
; Added flyby functionality.
;
; Revision 1.12  2008/01/24 20:14:41  antunes
; More diagnostics and tests.
;
; Revision 1.11  2008/01/15 16:50:08  antunes
; Better units, more tests.
;
; Revision 1.10  2008/01/09 20:42:14  antunes
; Added toggle and code for slower but more accurate rotations.
;
; Revision 1.9  2007/09/14 13:40:05  antunes
; Improved scaling.
;
; Revision 1.8  2007/06/11 16:30:25  antunes
; Added some test cases, cleaned up my cvs a bit.
;
; Revision 1.7  2007/03/19 20:27:21  antunes
; Minor updates.
;
; Revision 1.6  2007/02/23 14:04:57  antunes
; Succeeded in N=256 2-image reconstruction!
;
; Revision 1.5  2007/02/21 16:43:19  antunes
; Memory mods to allow for higher N (N=256 now works!)
;
; Revision 1.4  2006/12/06 19:13:34  antunes
; Little ingest improvements and fixes.
;
; Revision 1.3  2006/11/16 18:27:33  antunes
; Did a rollback then an update to allow pixon wrapper to handle
; heterogeneous images.
;
; Revision 1.1  2006/04/11 15:55:05  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.3  2005/12/27 17:48:24  antunes
; Commented half of the routines using SSW style.  Also added a new
; axes test case 'arrowcube'.
; Checked in the Tomography/old/mikic_img.pro routine here because it
; is a dependency for the general image-prep routine 'get_timage.pro'
;
;-            
                             ; ----- clerical stuff

doubleprec=KEYWORD_SET(doubleprec)

   t00=double(systime(-1))

fast=KEYWORD_SET(fast)
fast=1; for now, until we debug further
err=0
;if(n_elements(sat_name) ne Nd)then err=6
if(err ne 0)then begin                  &$
   print,'Error ',err,' - build_input ' &$
   stop                                 &$
  end

;sat_name:   sat_name,        $ ; [Nd] satellite names, moved to sr

sc= {$
gname:      gname,           $ ; [] generic file name 
term:       term,            $ ; [] terminal type (batch or dtterm)
note:       note,            $ ; [] any text you want
doubleprec: doubleprec,      $ ; [] toggle to use double not (default) float
t00:        t00,             $ ; [] start time (seconds)
totaltime:  t00*0,           $ ; [] total time (seconds)
tdsf:       t00*0,           $ ; time spent in dsf (seconds)
tdsft:      t00*0,           $ ; time spent in dsft (seconds)
fast:       fast,            $ ; toggle to use fast(1) or more accurate(0)
version:    '',              $ ; software version (set in stereo_pixon)
machine:    '',              $ ; machine name (set in stereo_osetup)
date:       '',              $ ; [] date (set in stereo_osetup)
tag:        '',              $ ; unique string identifier (set in stereo_osetup)
xyzmask:    0,               $ ; flag tells about initial data munging
zmask:      0,               $ ; flag tells about initial data munging
sidemask:   0,               $ ; flag tells about initial data munging
nozero:     0,               $ ; flag tells about initial data munging
excerpt:    0,               $ ; flag tells about initial data munging
occult:     0,               $ ; flag tells about initial data munging
brightpixels: 0,             $ ; flag tells about initial data munging
nadjust:    0,               $ ; value tells about initial data munging
looptime:   0,               $ ; holds time to run 1 pixon step, in sec
datacut:    double(0),       $ ; lists optional datacut, if any
endcause:   '(not yet run)', $ ; string describing why run ended
Q:          0.0d0,           $ ; placeholder to compute Q value from solution
chisq:      0.0d0,           $ ; placeholder to track chisq value from solution
pcount:     long(0),         $ ; placeholder to track pixon count from solution
iterations: long(0),         $ ; placeholder to track number of iterations run
runstats:   ptrarr(4)        $ ; keeps entire log of iterations,pcount,chisq,Q
}

 return,sc
END
