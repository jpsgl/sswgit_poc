;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : 
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;
; This is the second half of the 'quickcme' suite, this part assumes
; you ran the first one to prepare the entire problem.  This is the
; part that then calls up Pixon or other algorithms and tries to
; create a reconstruction.
;               
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; The second half of the 'quickcme' suite

FUNCTION quickcme_run,pxwrapper,diff,ndiff,mset,jumpstart,iend,$
                      presolution=presolution,pseudo=pseudo,checksn=checksn,$
                      mxit=mxit

  jumpstart=KEYWORD_SET(jumpstart)
  pseudo=KEYWORD_SET(pseudo)
  checksn=KEYWORD_SET(checksn)
  if (n_elements(iend) eq 0) then iend=1

  if (n_elements(presolution) ne 0) then import=1 else import=0

  if (n_elements(mxit) ne 0) then pxwrapper.sp.mxit=mxit

  pixon_env,'c',/local; ensure we are using local, latest version

  p_generate_pixon,'classic',pxwrapper,oPixon

  p_insert_data,pxwrapper,diff,oPixon,sigma=ndiff,masks=mset

  if (import) then begin

      Ncube=pxwrapper.sr.Ncube
      presol=congrid(presolution,Ncube,Ncube,Ncube)
      oPixon->replace,image=presol

  end else if (jumpstart) then begin

      ; generate a 'best guess' to start with all A and B frames (no LASCO)
      ;sn=ratio(diff[*,*,0:iend],ndiff[*,*,0:iend])*mset[*,*,0:iend] > 1.0

      jset=diff[*,*,0:iend]*mset[*,*,0:iend]

     ; project s/n plots
;      startimage=simple3drv(sn,pxwrapper=pxwrapper)
      startimage=simple3drv(jset,pxwrapper=pxwrapper,/back,/usemax,/norm,/carve)
    ; normalize
    ;  startimage = startimage * max(diff)/max(pr_render(pxwrapper,startimage))
    ; carve out center
    ;  m=p_getmask('COR2',pxwrapper.sr.ncube,rpixels=rpixels)
    ;  mask3D = cube_sphere(pxwrapper.sr.ncube,rpixels[0],r_outer=rpixels[1])
    ;  startimage = startimage * mask3D
    ; and insert
      if (checksn) then threeview,startimage,/log,/pixon,oom=2
;    save,startimage,file='startimage.sav'
      oPixon->replace,image=startimage

  end

  print,'Now starting Pixon reconstruction.'
  
  if (jumpstart or import) then begin
      if (pseudo) then oPixon->pseudo else oPixon->full
  end else begin
      if (pseudo) then oPixon->pseudo,/restart else oPixon->full,/restart
  end

  p_update_runstats,pxwrapper,oPixon

  return,oPixon

END
