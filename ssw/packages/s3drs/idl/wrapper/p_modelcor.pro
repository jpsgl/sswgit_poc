;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : p_modelcor
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;               
; Sets up instrument parameters suitable for Cor2A or C3
;
; Instr can be 'cor2', 'C3', others to be added via 'p_instrspec'
;
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; Sets up instrument parameters suitable for Cor2A or C3
;
; Instr can be 'cor2', 'C3', others to be added via 'p_instrspec'
;
; Assumes input data will be 'DN' unless given '/msb' flag.
; for pxwrapper: detector, L0tau, tau, Istd
;
; Typical usage is:
;      model2cor2a,pxwrapper
;      p_insert_data,pxwrapper,datum,oPixon,sigma=noise
;
; Can set 'iele' to just change one pxwrapper item, otherwise
; it sets them all as Cor2A simulations.
;

PRO p_modelcor,pxwrapper,dn=dn,exptime=exptime,iele=iele,instr=instr

  dn=KEYWORD_SET(dn)
;  if (msb) then imodel=4 else imodel=5
  if (dn) then imodel=11 else imodel=10
;  c3=KEYWORD_SET(c3)
;  cor2=KEYWORD_SET(cor2)
;  if (c3 eq 0) then cor2=1; cor2 is default

  fov_radius_in_rsun=p_instrspec('fov',instr)
  detector=p_instrspec('name',instr)
  DNperPhotons=p_instrspec('dnperphoton',instr)
  dn2msb=p_instrspec('dn2msb',instr)

  biasdev = 686.023; in DN
  biasdev = 0.027; not used yet
  if (n_elements(exptime) eq 0) then exptime = 6

  if (n_elements(iele) eq 0) then begin
      ia=0
      ib=pxwrapper.sr.Nd-1
  end else begin
      ia=iele
      ib=iele
  end

  ; Crucial line here, makes cube perfectly sized for that detector
  pxwrapper.sr.dvox=fov_radius_in_rsun/(0.5*pxwrapper.sr.Ncube)

  for i=ia, ib do begin

      pxwrapper.sr.detector[i]=detector
      pxwrapper.sr.L0tau=fov_radius_in_rsun/(0.5*pxwrapper.sr.Ncube)
      pxwrapper.sr.tau[i]=$
        fov_radius_in_rsun/(pxwrapper.sr.Ncube*pxwrapper.sr.L0)
      pxwrapper.sr.MSBperDN[i]=dn2msb
      pxwrapper.sr.DNperPhoton[i]=DNperPhotons
      pxwrapper.sr.exptime[i]=exptime
      pxwrapper.sr.biasdev[i]=biasdev
      pxwrapper.sr.imodel[i]=imodel
;      pxwrapper.sr.origres[i]=2048

      dvoxfrac=pxwrapper.sr.dvox/pxwrapper.sr.L0tau[i]
      ;o_flimb,pxwrapper.sr.chl,pxwrapper.sr.Ncube,1.0/pxwrapper.sr.L0tau[i],$
      ;  dvoxfrac,pxwrapper.sr.lla0,fa,fb,fc,rmin2,an,ImI0
      ;   note we do not need o_flimb here, only in the later
      ;   prep and rendering when we actually store the intermediate
      ;   quantities this produces.
      pxwrapper.sr.an=[0.300, 0.930, -0.230] ; an for polynomial ld (chl=2)
      pxwrapper.sr.limbdark[i]=0.8050 ; ImI0 for polynomial ld (chl=2)
      pxwrapper.sr.Istd[i]  =  $
        get_istd_Nd(imodel,pxwrapper.sr.L0,pxwrapper.sr.n0,$
                    pxwrapper.sr.L0tau[i],ImI0 = ImI0)

  end
  pxwrapper.sr.normd=1.0

END

;  dlevel05 = dmsb * exptime / dn2msb; convert to DN
;  fe = (sqrt(dlevel05 * DNperPhotons)) / DNperPhotons; sqrt of photon noise
;  i0=where(dlevel05 ne 0)
;  frac=fe*0.0; just to set array size
;  frac[i0] = fe[i0]/dlevel05[i0]; handles zeroes exception
;
;  nfe = dmsb * frac; total signal noise in MSB units
;  bnoise2 = (sqrt(biasdev * DNperPhotons))/DNperPhotons; add in bias noise
;  bnoisemsb = bnoise2*dn2msb
;  noise = sqrt( (nfe^2 + bnoisemsb^2))

;  print,'Cor2A minmax data, sigma (normd): ',$
;    minmax(dmsb),minmax(noise),normd
