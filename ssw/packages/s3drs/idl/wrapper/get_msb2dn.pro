;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : get_msb2dn
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;               
; *sigh* a wrapper for a wrapper of wrappers.
; Returns MSB per DN (e.g. 2e-12 msb/dn)
; Uses 'get_calfac' for SECCHI
;      'c2_calfactor' or 'c3_calfactor' for LASCO
;
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; *sigh* a wrapper for a wrapper of wrappers.
; Returns MSB per DN (e.g. 2e-12 msb/dn)
; Uses 'get_calfac' for SECCHI
;      'c2_calfactor' or 'c3_calfactor' for LASCO

FUNCTION get_msb2dn,hdr

    if (hdr.instrume eq 'SECCHI') then begin


        ; we use a faked hdr here so that you can pass it a
        ; secchi hdr read in via any fits reader, not just sccreadfits.
        fakehdr={SECCHI_HDR_STRUCTTEMP,$
                 IPSUM:hdr.ipsum,$
                 CCDSUM:hdr.ccdsum,$
                 DETECTOR:hdr.detector,$
                 OBSRVTRY:hdr.obsrvtry,$
                 WAVELNTH:hdr.wavelnth $
                }

        msb2dn = get_calfac(fakehdr)

    end else if (hdr.instrume eq 'LASCO') then begin

        if (hdr.detector eq 'C2') then begin
            msb2dn = c2_calfactor(hdr) 
        end else if (hdr.detector eq 'C3') then begin
            msb2dn = c3_calfactor(hdr)
        end else begin
            print,'Cannot recognize telescope, assuming msb2dn=1'
            msb2dn=1
        end
    end else if (tag_exist(hdr,'MSB2DN')) then begin
        ; often for sims, the conversion factor is recorded in the hdr
        msb2dn=hdr.msb2dn
    end else begin
        print,'Cannot recognize telescope, assuming msb2dn=1'
        msb2dn=1
    end

    return,msb2dn

END
