;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : p_roitool
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;
; This is a widget that returns a user-drawn mask for the given data.
; default is to return a mask blotting out everything except the
; region, but you can use /inverse to return a mask blotting out
; the selected ROI instead.
;               
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon, widgets
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; returns a mask for the given data
; default is to return a mask blotting out everything except the
; region, but you can use /inverse to return a mask blotting out
; the selected ROI instead.
;

FUNCTION p_roitool,data,inverse=inverse

  inverse=KEYWORD_SET(inverse)

  id=n_elements(data[0,0,*])
  for i=0,id-1 do begin

      d2=data[*,*,i]
      Ndata=n_elements(data[*,0,i])

      ; cap off top outlyers
      dmax=max(d2)
      dcap=percentiles(d2,value=[0.99])
      dmax=min([dmax,dcap])
      d2red=d2 < dmax

      d2red=congrid(d2,384,384)
      d2red=d2red/dmax

      XROI, alog10(d2red), Regions_Out=thisROI, /Block
      if (obj_valid(thisROI)) then begin
          mask = thisROI -> ComputeMask(Dimensions=Size(d2red, /Dimensions), $
                                        Mask_Rule=2)

          if (inverse) then mask = mask LE 0 else mask = mask GT 0

          Obj_Destroy, thisROI

          mask=congrid(mask,Ndata,Ndata)

      end else begin
          ; no regions were selected, so everything is allowed
          mask=d2*0.0+1.0; everything is allowed
      end

      push,masks,mask

  end

  return,masks

END


