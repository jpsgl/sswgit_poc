;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : pr_sum
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;
; given an image cube, rotates it by rho,phi and returns total along
; that line.
;               
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; given an image cube, rotates it by rho,phi and returns total along
; that line.


FUNCTION pr_sum,timage,rho,phi,pxwrapper=pxwrapper,iele=iele,pixon=pixon

  pixon=KEYWORD_SET(pixon)

  N=n_elements(timage[*,0,0])

  if (n_elements(pxwrapper)) then begin
      ; extract rho, phi from wrapper
      rho=pxwrapper.sr.rho
      phi=pxwrapper.sr.phi
  end else begin
      ; default if not given anything is our Pixon xyz
      if (n_elements(rho) eq 0) then rho=[0.0,90.0,0.0]
      if (n_elements(phi) eq 0) then phi=[0.0,0.0,90.0]
  end

  ; note use of 'set' to avoid overwriting input variables
  if (n_elements(iele) eq 0) then begin
      ; do as many items are provided
      iele=-1
      rhoset=rho; aka 'gamma', angle along rotational plane
      phiset=phi; aka 'theta', angle above rotational plane
      istart=0
      iend=n_elements(rhoset)-1
  end else begin
      rhoset=rho[iele]
      phiset=phi[iele]
      istart=0
      iend=0
  end

  if (iele eq -1) then dset=fltarr(N,N,n_elements(rhoset))
  help,dset

  for iframe=istart,iend do begin
      image=timage; always use a copy
      rang=rhoset[iframe]
      pang=phiset[iframe]

      if (pixon) then begin
          image=transpose(image,[1,2,0]);; yes!
      end

      ; note rotation is CCW
;      print,'rho: ',rang,', phi: ',pang
      cuberot3d,image,[pang,0.0-rang,0.0],/reverse
      dat=fltarr(N,N)

      for ix=0,N-1 do for jy=0,N-1 do dat[ix,jy]=total(image[ix,jy,*])
      if (iele eq -1) then dset[*,*,iframe]=dat; only accumulate for multiples
  end

  if (iele eq -1) then return,dset else return,dat

END
