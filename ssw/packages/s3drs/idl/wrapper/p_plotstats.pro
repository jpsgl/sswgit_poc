;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : p_plotstats
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;
; plots just the runtime stats
; plot runtime convergence, if such was saved (i.e. check ptr is valid)
;               
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; plots just the runtime stats
;
; plot runtime convergence, if such was saved (i.e. check ptr is valid)
;
; order is always iterations, pcount, chisq, q
;
; Can give 'solo' to just plot a single item:
;    solo='q','c'(hisq), or 'p'ixoncount aka pcount, or 'd' for default
;
; Can plot subranges,using 'istart=' and/or 'iend='
;

PRO p_plotstats,pxwrapper,fontsize=fontsize,filename=filename,save=save,$
                winsize=winsize,solo=solo,istart=istart,iend=iend,ps=ps,$
                header=header,log=log

  if (ptr_valid(pxwrapper.sc.runstats[0]) eq 0) then return

  save=KEYWORD_SET(save)
  ps=KEYWORD_SET(ps)
  log=KEYWORD_SET(log)
  if (n_elements(fontsize) eq 0) then fontsize=1.0
  if (n_elements(filename) eq 0) then filename='stats.ps'

  if (n_elements(winsize) eq 0) then winsize=[480,320]

  nele=n_elements( *(pxwrapper.sc.runstats[0]) )
  if (nele le 1) then return; case of no run data yet, so do not plot
  if (n_elements(istart) eq 0) then istart=0
  if (n_elements(iend) eq 0) then iend=nele-1
  istart=min([istart,nele-1])
  iend=max( [min([nele-1,iend]),istart] )

  if (ps) then begin
      current_device=!d.name
      set_plot,'PS'
      device,filename=filename,/color,bits_per_pixel=8,/landscape
  end else begin
      window,/free,xsize=winsize[0],ysize=winsize[1],retain=2
  end

  if (n_elements(header) eq 0) then header='Q History'

  ; default is plot them all
  if (n_elements(solo) eq 0) then solo='a'; default is to plot all

  ; some syntaxtical sugar here
  if (solo eq '1') then solo='p'
  if (solo eq '2') then solo='c'
  if (solo eq '3') then solo='q'

  if (strmatch('apcq','*'+solo+'*',/fold_case) eq 0) then solo='a'

  its=*(pxwrapper.sc.runstats[0])
  pd=*(pxwrapper.sc.runstats[1])
  cd=*(pxwrapper.sc.runstats[3])
  qd=*(pxwrapper.sc.runstats[3])

  if (log) then begin
      pd=alog10(pd)
      cd=alog10(cd)
      qd=alog10(cd)
      ynozero=1
  end else begin
      ynozero=0
  end

  if (solo eq 'a') then !P.MULTI = [0, 0, 3]

  if (solo eq 'p' or solo eq 'a') then $
    plot,its[istart:iend],pd[istart:iend],$
    title='Pixon Count',ynozero=1,yticks=2,charsize=fontsize

  if (solo eq 'c' or solo eq 'a') then $
    plot,its[istart:iend],cd[istart:iend],$
    title='Chisq',ynozero=ynozero,yticks=2,charsize=fontsize

  if (solo eq 'q' or solo eq 'a') then $
    plot,its[istart:iend],qd[istart:iend],$
    title='Q (Chisq/DOF)',ynozero=ynozero,yticks=2,charsize=fontsize

  if (save) then write_gif,gif5,tvrd()

  if (solo eq 'a') then !P.MULTI = 0; reset back to normal

  if (save) then write_gif,filename,tvrd()

  if (ps) then begin
      device,/CLOSE_FILE
      set_plot,current_device
  end


END
