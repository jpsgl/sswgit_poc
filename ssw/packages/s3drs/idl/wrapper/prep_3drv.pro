;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : prep_3drv
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;
; This is what makes 'pxwrapper', used by so many of our routines.
;
; A wrapper of wrappers, this majordomo calls the various
; structure-building routines to make a good first guess at all the
; instrument and geometry specifications.
;               
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; $Log: prep_3drv.pro,v $
; Revision 1.1  2009/04/22 19:22:39  antunes
; code relocation part 2 of 3.
;
; Revision 1.33  2009/04/22 14:52:25  antunes
; Added ability to totally ignore the SECCHI routines and use for any
; mission.
;
; Revision 1.32  2009/03/31 14:57:49  antunes
; Re-org prepatory to checking into SSW.
;
; Revision 1.31  2009/02/17 19:59:15  antunes
; Minor tweaks
;
; Revision 1.30  2008/12/03 19:34:28  antunes
; Neater output.
;
; Revision 1.29  2008/11/13 16:23:45  antunes
; Checkin after long hiatus.
;
; Revision 1.28  2008/07/01 19:22:24  antunes
; Checking in Pixon, finally.
;
; Revision 1.27  2008/06/16 18:22:57  antunes
; Minor mods.
;
; Revision 1.26  2008/06/06 18:40:24  antunes
; More tests and better plotting.
;
; Revision 1.25  2008/01/31 14:47:31  antunes
; Fully stable multi-FOV code for Pixon, also more test cases, plus top masking.
;
; Revision 1.24  2008/01/25 16:10:38  antunes
; Major refactoring of code to allow differing FOVs.  Note Pixon
; structures here are now incompatible with earlier versions!
;
; Revision 1.23  2008/01/24 20:14:41  antunes
; More diagnostics and tests.
;
; Revision 1.22  2008/01/17 20:36:12  antunes
; Major fix to alignment problems!  Also nicer plotting.
;
; Revision 1.21  2008/01/09 20:42:14  antunes
; Added toggle and code for slower but more accurate rotations.
;
; Revision 1.20  2008/01/09 19:24:28  antunes
; Finally fixed the data scaling unit (bug=legacy code used AU not Rsun),
; also added more tests.
;
; Revision 1.19  2008/01/04 20:16:49  antunes
; Final units (hopefully), also more tests and tools.
;
; Revision 1.18  2007/11/27 19:57:40  antunes
; Better plotting, more test cases.
;
; Revision 1.17  2007/11/21 23:12:25  antunes
; Updates to test scripts and diagnostics.
;
; Revision 1.16  2007/10/12 19:03:39  antunes
; Minor update, not perfect.
;
; Revision 1.15  2007/09/25 18:53:01  antunes
; Added hshell, also pixon->fits handling.
;
; Revision 1.14  2007/09/17 19:04:47  antunes
; More improvements.
;
; Revision 1.13  2007/08/29 18:55:09  antunes
; Greatly improved plotting and visualizations.
;
; Revision 1.12  2007/06/13 18:37:30  antunes
; Added autoscaling, instructions.
;
; Revision 1.11  2007/06/12 19:02:44  antunes
; Added Pixon autoscaling to resolve normalization difficulties.
;
; Revision 1.10  2007/05/31 21:02:51  antunes
; More tests, better FITS handling, documentation of geometry.
;
; Revision 1.9  2007/05/29 19:02:18  antunes
; Full process ready for scaled and cropped data with noise.
;
; Revision 1.8  2007/02/23 14:04:57  antunes
; Succeeded in N=256 2-image reconstruction!
;
; Revision 1.7  2006/11/30 20:03:57  antunes
; Updated pixon and other printing and output routines.
;
; Revision 1.6  2006/11/17 19:02:33  antunes
; Updates to Pixon structures.
;
; Revision 1.5  2006/11/16 18:27:33  antunes
; Did a rollback then an update to allow pixon wrapper to handle
; heterogeneous images.
;
; Revision 1.3  2006/05/11 13:52:06  antunes
; Better handling of args, memory.
;
; Revision 1.2  2006/05/03 18:45:33  antunes
; Improved geometry/view handling
;
; Revision 1.1  2006/04/11 15:55:06  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.2  2006/03/24 17:53:20  antunes
; Reconciled c and t environments, created test cases and profiling
; checks, working towards generalized tetra object.
;
; Revision 1.1  2006/03/08 19:33:11  antunes
; Yet another refactoring.
;
; Revision 1.15  2006/03/08 17:52:23  antunes
; Abstracted pixon structures into an aggregate structure in order
; to be encapsulate Pixonisms from general rendering and reconstruction
; routines.
;
; Revision 1.14  2006/02/28 19:48:16  antunes
; Lots of code, some buggy-- interim checkin.
;
; Revision 1.13  2006/02/21 19:36:03  antunes
; Bug fixes and streamlining.
;
; Revision 1.12  2006/02/17 19:48:16  antunes
; Incorporated refactored TPixon code into generalized schema,
; also now they can share an environment.  They now fork in
; the 'p_generate_pixon' call.
;
; Revision 1.11  2006/02/15 18:04:59  antunes
; Updated Old Pixon environment to force call of proper versions of Pixon.
;
; Revision 1.10  2006/02/03 18:50:06  antunes
; GUI updates.
;
; Revision 1.9  2006/01/09 17:14:42  antunes
; More commented added.
; New 'super3drv' driver routine for getting user specs for reconstruction
; process now checked in.  Just type 'super3drv' to test.
;
;-            
; Note we almost always want to use dvox=L0tau, i.e. the typical single
; voxel size is the average L0tau, so that 1 (average) pixel projects
; into 1 voxel.  If dcube=2, then 2 pixels project per voxel, if
; dcube=0.5, then each pixel covers two voxels.  However, we have not
; yet tested and verified when Ncube != Ndata
;
; You can give it initial positions as angles, with several variant names:
;   gamma/rho: angle around orbital plane, required
;   theta/phi: angle above orbital plane
;   z0 = height above x-y plane in scaled (to 1.0) units
;   LL = distance from observer to sun in scaled (to 1.0) units
;
; /cor2 means it will set constants appropriate to Cor2A in MSB using
;  the supplemental routine 'p_modelcor'.  Ditto for /c3 or /cor1
;
; Usually we run in single precision, /doubleprec will make all
; Pixon and data and image items be double precision instead.
; Our tests show no gain in improvement, so we recommend remaining
; in single precision floats for better performance.
;

PRO prep_3drv,pxwrapper,views=views,model=model,ftol=ftol,N=N,$
              enorm=enorm,dvox=dvox,dnorm=dnorm,autorange=autorange,tag=tag,$
              gamma=gamma,rho=rho,theta=theta,phi=phi,z0=z0,LL=LL,$
              cor2=cor2,c3=c3,cor1=cor1,otherinstr=otherinstr,$
              polar=polar,doubleprec=doubleprec

;common pixonrun,sr,sc,sp,sn,stuff
; these commons are empty structures upon entry to this routine,
; and get populated and returned back to the routine which called
; this.  Typically, one does a Pixon run by typing:
;   @init_pixon  (sets up paths and compiles Pixon code)
;   N=128
;   prep_[model],N  (loads given model defaults into Pixon structures)
;   p_run_pixon,sr,sc,sp,sn,N,stuff  (does Pixon run and saves result)
;

; we pass pixon-isms within a wrapper, despite how this hides
; data, to simply GUI and data saving issues
;sr=pxwrapper.sr
;sc=pxwrapper.sc
;sp=pxwrapper.sp
;sn=pxwrapper.sn
;stuff=pxwrapper.stuff

;version='stereo_pixon: 06'

cor2=KEYWORD_SET(cor2)
c3=KEYWORD_SET(c3)
cor1=KEYWORD_SET(cor1)

if (n_elements(tag) eq 0) then tag='recon'
if (n_elements(model) eq 0) then model=tag
if (n_elements(enorm) eq 0) then n0=1.0 else n0=enorm; use 'enorm' to avoid too many N
if (n_elements(dnorm) eq 0) then dnorm=1.0
doubleprec=keyword_set(doubleprec)
if (n_elements(autorange) eq 0) then autorange=-1; -1 turns off autoranging
if (n_elements(dvox) eq 0) then dvox=-1

;print,'preparing variables for ',model

if (n_elements(views) eq 0) then views=1

if (n_elements(N) eq 0) then begin
  N=32
  ; defines size of Pixon cube, N^3
;  print,'Using default N=128'
end

;ftol=0.005
;dfac=0.95

; ----- sr
;tau=0 ; typically 0 or 30.0/N/214.943 or 60.0/N/214.943 or 1.0e-10
ch  = model
if (n_elements(stuff) eq 0) then stuff=-1 ; model-specific parameters to include
if (n_elements(gamma) eq 0 and n_elements(rho) ne 0) then gamma=rho

if (n_elements(gamma) ne 0) then begin
    views = n_elements(gamma)
end else if (views eq 1) then begin
    gamma=[0.0]
end else if (views eq 2) then begin
    gamma=[0.0,90.0]                ; varies for different models
end else if (views eq 3) then begin
    gamma=[0.0,90.0,0.0]              ; varies for different models
end else begin
    if (model ne 'null') then begin
        print,'Cannot automatically decide on views >3, provide gamma=gamma'
    end
    gamma=make_array(views,/double,value=0.0)
end
;L0tau = 60./double(N)  ; 12/N is C2, 60/N is C3
;n0    = 1.0e8 ; typically 1.0e8 or simply 1
;n0    = 1 ; typically 1.0e8 or simply 1

if (n_elements(theta) eq 0 and n_elements(phi) ne 0) then theta=phi
if (n_elements(theta) eq 0) then theta=make_array(views,/double,value=0.0)
if (n_elements(z0) eq 0) then z0=make_array(views,/double,value=0.0)
if (n_elements(LL) eq 0) then LL=make_array(views,/double,value=1.0)

;sr=p_build_sr(N,ch,tau,gamma,n0)
sr=p_build_sr(N,gamma,model=model,n0=n0,dvox=dvox,normd=dnorm,$
             theta=theta,z0=z0,LL=LL,polar=polar)

;----- sp

anneal_specs=p_anneal_defaults()
reltol  =  1.0e-3; 1.0e-9 ; Pixon cutoff for relative change
abstol  =  1.0e-3; 1.0e-5 ; Pixon cutoff for absolute change
mxit = 400; minimization steps, i.e. max number of Pixon iterations per cycle
;npo   =  4 ;  number of polarizations
npo   =  2 ; number of PIXON kernels per octave, def=2
noct  =  4 ; number of PIXON kernel octaves, def=4

sp=p_build_sp(reltol,abstol,mxit,anneal_specs,npo=npo,noct=noct,$
              autorange=autorange)

; ----- sn
;; assumes data max ~ 1
noisemod = 'Amplifier and photon noise' ; Noise model to call

Smax=40
Smin=1

if (n_elements(ftol) ne 0) then begin
    ftol = [ftol,1/float(Smin)^2] ; array of sigma spec. for noise
end else begin
    ftol = [1/float(Smax)^2,1/float(Smin)^2] ; array of sigma spec. for noise
end

;print,'ftol is ',ftol

fsig=ftol
;       ftol contains the following parameters:
; ftol[0] is 1/smax^2 where smax is the signal to noise ratio
;         at data maximum. In other words, ftol[0]*(max data)
;         is g, where N=data/g is # of photons.
; ftol[1] is the data value corresponding ta Na*g, the background noise.
; ftol[2] is the data maximum. If zero, use the actual data maximum.
; fsig    is the same as ftol[0] if actual noise consistent with ftol
;         is to be added to the data. It can be set to zero to
;         eliminate any noise being actually added to the data.


sn=p_build_sn(noisemod,ftol,fsig)

; ----- sc
gname = tag ; the generic name
term = 'batch' ; batch or dtterm

if (views eq 1) then begin
    sat_name = ['s']; just a placeholder, usually, for 1 view
end else if (views eq 2) then begin
    sat_name = ['1','2']   ; usually 1,2 or xview,yview,zview or x,y,z
end else if (views eq 3) then begin
  sat_name = ['x','y','z'] ; usually 1,2 or xview,yview,zview or x,y,z
end else begin
;  print,'Cannot automatically decide on views other than 1, 2 or 3, fix'
  sat_name=make_array(views,/string,value='n')
  for i=0,views-1 do sat_name[i] = strtrim(string(i),2)
end

note = model ; can be any text desired
;sc=p_build_sc(gname,term,sat_name,note,gamma)
; note that 'slow' rotation uses N*1.5 cubes, so turn on 'fast'
; instead for performance or (automatic default here) if N>256
if (N le 128) then fast=0 else fast=1
sc=p_build_sc(gname,term,note,fast=fast,doubleprec=doubleprec)

;get_timage,sr,sc,timage,stuff,stuff1
;timage=pixonmodels(pxwrapper,stuff,stuff1)

; note later structures will get added
;sf, so, journ, userstruct

; now stuff it into a the wrapper for returning
pxwrapper = { $
              sr: sr, $
              sc: sc, $
              sp: sp, $
              sn: sn, $
              stuff: stuff, $
              object: 'no' $
}

;if (model eq 'null') then begin
;    print,"If you wish to now load an alternative model, load it into 'timage''
;    print,"  e.g.  timage=fake_ne_cube(",strtrim(N,1),",generate='cube',lit=3)"
;    print,'Then, after setting timage, type:'
;end else begin
;    print,'now type:'
;end

;print,' Now type either of the following 2 lines:'
;print,"  p_generate_pixon,'tetra',pxwrapper,oPixon,image=timage"
;print,'     or'
;print,"  p_generate_pixon,'classic',pxwrapper,oPixon,image=timage"

if (n_elements(otherinstr) ne 0) then p_modelcor,pxwrapper,instr=otherinstr
if (cor2) then p_modelcor,pxwrapper,instr='Cor2'; assumes MSB unless given /dn
if (c3) then p_modelcor,pxwrapper,instr='C3'; assumes MSB unless given /dn
if (cor1) then p_modelcor,pxwrapper,instr='cor1'; assumes MSB unless given /dn
;if (cor2) then model2cor2a,pxwrapper

END
