;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : pr_render
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;               
; stand-alone whitelight renderer combined from pxndsf_force and p_osetup
; optionally returns 'istd' as a seperate variable
;
; Use       : mydata=pr_render(pxwrapper,image,id,istd=istd,fast=fast)
;    
; Inputs    : pxwrapper = geometry and instrument structures
;             image = density cube to render from
;             id = (optional) frame id in the structure to render,
;                otherwise it renders all available frames in structure.
;
; Outputs   : returns the render
;             istd = (optional) return the standard intensity relative to I0
;
; Keywords  : /fast uses a faster but sloppier algorithm
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon, whitelight, renderer
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; stand-alone renderer combined from pxndsf_force and p_osetup
; optionally returns 'istd' as a seperate variable

PRO test_pr_render

   N=32
   prep_3drv,pxwrapper,N=N,gamma=[0,90]
   timage=pixonmodels(N,'fluxrope',pxwrapper)
   render1=pr_render(pxwrapper,timage,0)
   render2=pr_render(pxwrapper,timage,1)
   tv_multi,[[[render1]],[[render2]]],fixed=128,/log

END


FUNCTION pr_render,pxwrapper,image,id,istd=istd,fast=fast

  Ncube  = n_elements(image[*,0,0])
  if (datatype(pxwrapper) eq 'INT') then prep_3drv,pxwrapper,views=1,N=Ncube

  ; fast=0;
  ; 1 = fast but sloppy, 0 = 4x slower and more memory but more accurate
  if (n_elements(fast) eq 0 and tag_exist(pxwrapper,'sc')) then $
    fast=pxwrapper.sc.fast else fast=KEYWORD_SET(fast)

  if (n_elements(phys) eq 0) then phys=1
  
  if (n_elements(id) eq 0) then id=-1
  if (id eq -1) then begin
                                ; do all frames
      istart=0
      iend=pxwrapper.sr.Nd-1;  n_elements(pxwrapper.sr.gamma)-1
  end else begin
      istart=id
      iend=id
  end
  
; handles legacy or stand-alone structure as well as Pixon's wrapper
  if (tag_exist(pxwrapper,'sr')) then sr=pxwrapper.sr else sr=pxwrapper
  
;Ncube=sr.Ncube
;  Nd=n_elements(sr.gamma)
  Nd=sr.Nd

;;  lla0=sr.lla0; usually just (N-1)/2.0, but now we allow offset cubes too
  
  istd=fltarr(Nd)
  limbdark=fltarr(Nd)
  
; always work with a copy of the cube-- inefficient, but safe
  Ndata=sr.Ndata[0]
  
  datum=fltarr(Ndata,Ndata,Nd)
  
  FOR iframe=istart,iend do begin

      dat=fltarr(Ndata,Ndata)

      if (tag_exist(sr,'d')) then dvoxfrac=pxwrapper.sr.d else $
        dvoxfrac=sr.dvox/sr.L0tau[iframe]
      o_flimb,sr.chl,Ndata,1/sr.L0tau[iframe],dvoxfrac,sr.lla0,fa,fb,fc,$
        rmin2,an,ImI0
      sr.an=an
      limbdark[iframe]=ImI0
      istd[iframe]  =  $
        get_istd_Nd(sr.imodel[iframe],sr.L0,sr.n0,sr.L0tau[iframe],ImI0 = ImI0)
      
      ffunc,sr,iframe,ps,mu,nu,is,jc,K2,u,x,y,rhos2,c2z,s2z
      pxwrapper.sr=sr; ffunc alters some params in sr: trW,trRRs,trP,trVs

      rho2   =  x*x+y*y
      psLs   =  ps*pxwrapper.sr.LL[iframe]
      psLs2  =  psLs^2
      
      fprf=pr_fprf(1,[Ndata,Ndata])
      
      roco=sr.roco[iframe]
      polar=sr.polar[iframe]
;    c2z=
;    s2z=
      lla0=sr.lla0
;    rho2=rho2
;    rhos2=
;    psLs=psLs
;    psLs2=psLs2
      i=is
;    jc=
;    K2=

      if (tag_exist(sr,'d')) then dvoxfrac=pxwrapper.sr.d else $
        dvoxfrac=sr.dvox/sr.L0tau[iframe]

      d2=dvoxfrac^2
      fa=fa*dvoxfrac^3
      fb=fb*dvoxfrac^3
      fc=fc*dvoxfrac^3
;    rmin2=
      tau=sr.tau[iframe]
      tau2=sr.tau[iframe]*sr.tau[iframe]
      interp=sr.interp
      if (getenv('disumlib') eq '') then interp=1;switch to IDL if no C-version

      machine=getenv('HOST')
      g=sr.rho[iframe]
      theta=sr.theta[iframe]
      
      gamma=g                   ; easier to grep on
      
      t0=systime(-1)
      psp=2
      
      if (Ncube ne Ndata) then begin
          timage=congrid(image,Ndata,Ndata,Ndata)
      end else begin
          timage=image
      end
      
; rotate (for close to z-axis stuff)
      
      if(roco eq 1)then begin
                                ; legacy code, unlikely to be used
          for nu=0,Ndata-1 do $
            timage[*,nu,*]=rotate(reform(temporary(timage[*,nu,*])),1) 
          lla0=[0.5*(Ndata-1)-lla0[2],lla0[1],lla0[0]] 
      end
      
      if (roco eq -1 and (theta ne 0 or gamma ne 0)) then begin
                                ; note gamma should be CCW
          if (fast) then begin
;        print,'using fast',gamma,theta
              cuberot3d,timage,[0.0,theta,0.0-gamma],/reverse
          end else begin
;        print,'using slow'
;;              timage=turn_3d(timage,0.0,theta,gamma,/reverse,/resize,$
;;                            xyzcenter=lla0,/pivot,/interp)
              timage=turn_3d(timage,0.0,theta,gamma,/reverse,/resize,$
                            /pivot,/interp)
          end
          
; note we require the image center be the sun center be the cube
; center, for now
;    lla0=[]
      end                       ; end roco loop
      
      
; ----- 
      
      case strlowcase(polar) of
          
          'n': begin         ; no Thompson scattering-- test case only

              for la=0,Ndata-1 do begin
;                  z2  = d2*(la-lla0[2])^2
;                  r2  = rho2+z2
;                  rs2 = rhos2+tau2*z2
;                  cx2 = (r2*tau2-2*tau*psLs+psLs2/r2)/rs2       
;                  r2  = round((r2-rmin2)/d2)
                  j   = jc+la/K2

                  fizzix=fltarr(Ndata,Ndata)+1.0
                  ; native IDL version of '2', w/wout interpolation
                  dsumidl,interp,Ndata,i,j,la,timage,fizzix,dat
              end
              
          end ; closes 'n'
          
          
;=================== PLANE POLARIZATION =====================
          
          'p': begin            ; plane
              
              tdat=dblarr(Ndata,Ndata)
              rdat=dblarr(Ndata,Ndata)
              
              for la=0,Ndata-1 do begin
                  
                  z2  = d2*(la-lla0[2])^2
                  r2  = rho2+z2
                  rs2 = rhos2+tau2*z2
                  cx2 = (r2*tau2-2*tau*psLs+psLs2/r2)/rs2       
                  r2  = round((r2-rmin2)/d2)
                  j   = jc+la/K2
                  
                  if(interp ne 3 and interp ne 1)then stop,'dsf error'
                  
                  fizzixt = (fa[r2]/rs2)*phys + (1-phys) ;tangential
                  if (interp eq 3) then begin
                                ; C program - with interpolation v=1
                      disum1_inc,timage[*,*,la]*fizzixt,Ndata,tdat,i,j,machine
                  end else begin
                                ; native IDL version of '3', interpolation v=1
                      dsumidl,1,Ndata,i,j,la,timage,fizzixt,tdat; ,6 deltavoxel
                  end
                  
                  fizzixr=((cx2*fb[r2]+fc[r2])/rs2)*phys + (1-phys) ;radial
                  if (interp eq 3) then begin
                                ; C program - with interpolation v=1
                      disum1_inc,timage[*,*,la]*fizzixr,Ndata,rdat,i,j,machine
                  end else begin
                                ; native IDL version of '3', interpolation v=1
                      dsumidl,1,Ndata,i,j,la,timage,fizzixr,rdat
                  end
                  
;---------------------------
                  
              end
              
              dat = tdat*c2z + rdat*s2z
              
          end; closes 'p'
          
;================== TANGENTIAL POLARIZATION ==============
          
          't': begin            ; tangential polarization
              
              for la = 0,Ndata-1 do begin
                  z2  = d2*(la-lla0[2])^2
                  r2 = round((rho2+z2-rmin2)/d2)
                  rs2 = rhos2+tau2*z2
                  j   = jc+la/K2
                  fizzix = (fa[r2]/rs2)*phys + (1-phys)
                  
;---------------------------
                  
                  case interp of
                      
                      0: begin ; native IDL version of '2', no interpolation
                          dsumidl,0,Ndata,i,j,la,timage,fizzix,dat
                      end
                      
                      1: begin ; native IDL version of '3', interpolation v=1
                          dsumidl,1,Ndata,i,j,la,timage,fizzix,dat
                      end
                      
                      2: begin  ; C program - no interpolation
                          @dsum_inc
                      end
                
                      3: begin  ; C program - with interpolation v=1
                          disum1_inc,timage[*,*,la]*fizzix,Ndata,$
                            dat,i,j,machine
                      end
                
                      4: begin ; C program - with interpolation v=variable
                          @disum2_inc
                      end
                
                      5: begin  ; C program - trapezoid spreading
                          disum3_inc,timage[*,*,la]*fizzix,Ndata,$
                            dat,i,j,machine,g
                      end
                
                      else: stop,'gack'
                  end
            
;---------------------------
              end
          end                   ;   closes 't'
        
;================== RADIAL POLARIZATION ==================
        
          'r': begin            ; radial polarization
            
            for la=0,Ndata-1 do begin
                z2  = d2*(la-lla0[2])^2
                r2  = rho2+z2   ; r2 is distance from sun
                rs2 = rhos2+tau2*z2
                cx2 = (r2*tau2-2*tau*psLs+psLs2/r2)/rs2       
                r2  = round((r2-rmin2)/d2)
                j   = jc+la/K2
                fizzix=((cx2*fb[r2]+fc[r2])/rs2)*phys + (1-phys)
                
;---------------------------
                
                case interp of
                    
                    0: begin ; native IDL version of '2', no interpolation
                        dsumidl,0,Ndata,i,j,la,timage,fizzix,dat
                    end
                    
                    1: begin ; native IDL version of '3', interpolation v=1
                        dsumidl,1,Ndata,i,j,la,timage,fizzix,dat
                    end
                    
                    2: begin    ; C program - no interpolation
                        @dsum_inc
                    end
                
                    3: begin    ; C program - with interpolation v=1
                        disum1_inc,timage[*,*,la]*fizzix,Ndata,dat,i,j,machine
                    end
                
                    4: begin ; C program - with interpolation v=variable
                        @disum2_inc
                    end
                
                    5: begin    ; C program - trapezoid spreading
                        disum3_inc,timage[*,*,la]*fizzix,Ndata,dat,$
                          i,j,machine,g
                    end
                
                    else: stop,'gack'
                end
            
;---------------------------
            
            end
          end                     ; closes 'r'
        
;=================== NO POLARIZATION =====================
        
          'b': begin            ; brightness
            
              for la=0,Ndata-1 do begin
                  z2  = d2*(la-lla0[2])^2
                  r2  = rho2+z2
                  rs2 = rhos2+tau2*z2
                  cx2 = (r2*tau2-2*tau*psLs+psLs2/r2)/rs2       
                  r2  = round((r2-rmin2)/d2)
                  j   = jc+la/K2
                  fizzix=((fa[r2]+cx2*fb[r2]+fc[r2])/rs2)*phys + (1-phys)
                  
;---------------------------
                  
                  case interp of
                      
                      0: begin ; native IDL version of '2', no interpolation
                          dsumidl,0,Ndata,i,j,la,timage,fizzix,dat
                      end
                      
                      1: begin ; native IDL version of '3', interpolation v=1
                          dsumidl,1,Ndata,i,j,la,timage,fizzix,dat
                      end
                      
                      2: begin  ; C program - no interpolation
                          @dsum_inc
                      end
                    
                      3: begin  ; C program - with interpolation v=1
                          disum1_inc,timage[*,*,la]*fizzix,Ndata,dat,$
                            i,j,machine
                      end
                    
                      4: begin ; C program - with interpolation v=variable
                          @disum2_inc
                      end
                    
                      5: begin  ; C program - trapezoid spreading
                          disum3_inc,timage[*,*,la]*fizzix,Ndata,dat,$
                            i,j,machine,g
                      end
                    
                      else: stop,'gack'
                  end
                
;---------------------------
                
              end
          end                   ; closes 'b'
        
;============================================================
        
          else: begin
              help,polar
              stop,'ERRORx in dsf: polar = '+string(polar)
          end
      end                       ; closes 'case polar'
    
; ----- convolution
    
;fdat = self->rfft(dat)
;dat  = self->H(fdat,fprf)
    
; -----
    
; note we do not need to reverse the rotation since we rotate a copy,
; not the original input image.
    
      dat=dat * istd[iframe] ; convert to physically meaningful units
    
;  tv_multi,res=4,/log,dat,header=strtrim(string(iframe),2)
    
      datum[*,*,iframe]=dat
    
      pxwrapper.sr.istd[iframe]=istd[iframe] ; update what was given here
      pxwrapper.sr.limbdark[iframe]=limbdark[iframe] ; ditto
    
  end                           ; end of the per-frame loop

  if (id eq -1) then return,datum else return,dat

END
