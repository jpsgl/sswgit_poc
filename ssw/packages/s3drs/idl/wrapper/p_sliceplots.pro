;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : p_sliceplots
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;               
; Generates plots showing data slices for data or image.
; plots slice at midpt for input and output images and data,
; optional /bracket includes sigma range for data
;
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; plots slice at midpt for input and output images and data,
; optional /bracket includes sigma range for data

PRO p_sliceplots,pxwrapper,oPixon,timage,id=id,tag=tag,noscreen=noscreen,$
                 bracket=bracket,saved=saved,midpoint=midpoint,$
                 appendps=appendps

  if (n_elements(id) eq 0) then id=1
  if (n_elements(tag) eq 0) then tag=''
  noscreen=KEYWORD_SET(noscreen)
  if (noscreen) then istart=1 else istart=0
  bracket=KEYWORD_SET(bracket)
  appendps=KEYWORD_SET(appendps)

  N=pxwrapper.sr.Ncube
  if (n_elements(midpoint) eq 0) then midpt=N/2 else midpt=long(midpoint)
  midptstr=strtrim(string(midpt),2)

  img=stereo_fetch(pxwrapper,oPixon,/im)

  threeview,img,imgset,/pixon,/noplot

  ; note if we have no timage, this plot isn't terribly useful
  if (n_elements(timage) ne 0) then $
    threeview,timage,timgset,/pixon,/noplot else $
    timgset=imgset

  xi=imgset[*,*,0]
  xt=timgset[*,*,0]

  ie=where(xi ge 0)
  if (ie[0] ne -1) then imgmin=min(xi[ie]) else imgmin=0.0
  ie=where(xi le 0)
  if (ie[0] ne -1) then xi[ie]=1; HACK imgmin
  
  r=(make_array(/float,/index,N)-((N-1)/2.0))*pxwrapper.sr.l0tau[0]

; imgslice

  data=stereo_fetch(pxwrapper,oPixon,/data)
  render=stereo_fetch(pxwrapper,oPixon,/render)
  sigma=stereo_fetch(pxwrapper,oPixon,/sigma)
  qmap=stereo_fetch(pxwrapper,oPixon,/qmap)

  ie=where(data gt 0)
  if (ie[0] ne -1) then dmin=min(data[ie])
  ie=where(data lt 0)
  dataplus=data
  if (ie[0] ne -1) then dataplus[ie]=dmin*0.1
  dmin=dmin*1000.0; HACK
  dmax=max(dataplus)
;  qmap2=qmap*dmin

  dup=dataplus[*,midpt]+sigma[*,midpt]
  ddown=dataplus[*,midpt]-sigma[*,midpt]
  ie=where(ddown le 0)
  if (ie[0] ne -1) then ddown[ie]=dmin

  for ichoice=istart,1 do begin

      if (ichoice eq 1) then begin
          if (appendps) then begin
              ERASE; starts a new page on a presumed open postscript device
              fname='';
          end else begin
              fname=tag+'_'+strtrim(string(id),2)+'_slices.ps'
              current_device=!d.name
              set_plot,'PS'
              device,filename=fname,/landscape
          end
      end else begin
          ;onscreen
          window,/free,xsize=600,ysize=800,retain=2
      end

      !P.MULTI=[0,0,3]

;      ylow=min(finite([xt[*,midpt],xi[*,midpt]]))
;      yhigh=max(finite([xt[*,midpt],xi[*,midpt]]))
;;      ylow1=min(xt[*,midpt])
;;      ylow2=min(xi[*,midpt])
;;      ylow=min([ylow1,ylow2])
;            ylow=min([xt[*,midpt],xi[*,midpt]]))
;;      yhigh=max([xt[*,midpt],xi[*,midpt]])
      ylow=1E3
      yhigh=1E6

;      help,xt,xi,midpt,r

      plot,r,xi[*,midpt],title='Image slice (input=line, output=pt) at y='+midptstr,charsize=2.0,$
        /ynozero,xtitle='Rsun',ytitle='log(Ne)',/ylog,yrange=[1e4,1e10]
; ,ystyle=1,xstyle=1
      oplot,r,xt[*,midpt],psym=5
      ; now add in occulter limits for reference
      oplot,[-2.0,-2.0,2.0,2.0],[1E-17,1E17,1E17,1E-17],thick=0.1,linestyle=1
      oplot,[0,0],[1E-17,1E17],thick=0.1,linestyle=1
  
;      ydatalow=max([finite(data[*,midpt]),0.0])
;      ylow=min([ydatalow,render[*,midpt]])
;      ylow=min(finite([data[*,midpt] >0.0,render[*,midpt]]))
;      yhigh=max(finite([data[*,midpt],render[*,midpt]]))

;      print,minmax(data[*,midpt])
;      print,minmax(render[*,midpt])
;      print,ylow,yhigh

;      if (bracket) then begin
;          ylow=min([ylow, min(finite(data[*,midpt]-sigma[*,midpt]))])
;          yhigh=min([yhigh, min(finite(data[*,midpt]+sigma[*,midpt]))])
;      end

      plot,r,dataplus[*,midpt],/ylog,charsize=2.0, $
        title='Data slice (input lines with += sigma, output=pts)',$
        xtitle='Rsun',ytitle='log(B/B0)',yrange=[dmin,dmax];,ystyle=1,xstyle=1
      oplot,r,render[*,midpt],psym=5

      oplot,r,dup,linestyle=3
      oplot,r,ddown,linestyle=1

      ; now add in occulter limits for reference
      oplot,[-2.0,-2.0,2.0,2.0],[1E-20,1E17,1E17,1E-20],thick=0.1,linestyle=1
      oplot,[0,0],[1E-20,1E17],thick=0.1,linestyle=1

      plot,r,qmap[*,midpt],charsize=2.0,title='Q Map plot';,yrange=[0,4]

;      if (bracket) then begin
          ; add in +-1 sigma to data
;          oplot,r,(dataplus[*,midpt]+sigma[*,midpt]),linestyle=1
;          oplot,r,(dataplus[*,midpt]-sigma[*,midpt]),linestyle=1
;      end


;      plot,r,dup,/ylog,charsize=2.0,linestyle=3,$
;        title='Slice, Render and +- Sigma limits',$
;        xtitle='Rsun',ytitle='log(B/B0)'
;      oplot,r,render[*,midpt],psym=5
;      oplot,r,ddown,linestyle=1

      ; now add in occulter limits for reference
      oplot,[-2.0,-2.0,2.0,2.0],[1E-20,1E17,1E17,1E-20],thick=0.1,linestyle=1
      oplot,[0,0],[1E-20,1E17],thick=0.1,linestyle=1

      !P.MULTI=0

       if (ichoice eq 1 and appendps eq 0) then begin
          device,/CLOSE_FILE
          set_plot,current_device
      end
  end

  if (n_elements(saved) ne 0) then saved=[saved,fname] else saved=[fname]

END
