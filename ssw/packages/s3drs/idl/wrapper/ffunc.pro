pro ffunc,sr,s,ps,mu,nu,i,jc,K2,u,x,y,rhos2,c2z,s2z
;+
; $Id: ffunc.pro,v 1.1 2009/04/22 19:22:13 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : ffunc.pro
;               
; Purpose   : calculate intermediate Pixon parameters
;               
; Explanation: Pixon legacy code
;               
; Use       : IDL> ffunc,sr,s,ps,mu,nu,i,jc,K2,u,x,y,rhos2,c2z,s2z
;    
; Inputs: sr	render structure
;	 s      data frame number
;
; Outputs:
;       ps
;       mu,nu
;       i
;       jc
;       k2
;       u
;       x,y
;       rhos2
;       c2z      cosine^2 of angle between pixel, sun, and filter axis.
;       sr.trW, .trRRs, .trP, and .trVs
;	etc.
;
; Keywords  : 
;
; Calls from LASCO : none 
;
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : Simulation
;               
; Prev. Hist. : see Paul Reiser's code and notes
;
; Written     : Paul Reiser, imported by Sandy Antunes, NRL, Nov-Dec 2005
;               
; $Log: ffunc.pro,v $
; Revision 1.1  2009/04/22 19:22:13  antunes
; code relocation part 2 of 3.
;
; Revision 1.9  2008/01/31 14:47:31  antunes
; Fully stable multi-FOV code for Pixon, also more test cases, plus top masking.
;
; Revision 1.8  2008/01/25 16:10:37  antunes
; Major refactoring of code to allow differing FOVs.  Note Pixon
; structures here are now incompatible with earlier versions!
;
; Revision 1.7  2008/01/04 20:16:49  antunes
; Final units (hopefully), also more tests and tools.
;
; Revision 1.6  2007/02/12 18:05:17  antunes
; Modified for better handling of optical and sun center.
; Also generalized some earlier pixon legacy code.
;
; Revision 1.5  2006/11/30 20:03:57  antunes
; Updated pixon and other printing and output routines.
;
; Revision 1.4  2006/11/16 19:33:06  antunes
; Successfully replaced k0 with p_getk0 to allow for pointers
; and thus dynamic image loading.
;
; Revision 1.3  2006/11/16 18:27:32  antunes
; Did a rollback then an update to allow pixon wrapper to handle
; heterogeneous images.
;
; Revision 1.1  2006/04/11 15:55:05  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.2  2005/12/27 17:48:23  antunes
; Commented half of the routines using SSW style.  Also added a new
; axes test case 'arrowcube'.
; Checked in the Tomography/old/mikic_img.pro routine here because it
; is a dependency for the general image-prep routine 'get_timage.pro'
;
;-            

; version 07

; use some parameters in sr structure to calculate
; other parameters that will be needed

; input: sr	render structure
;	 s      data frame number
;
; output:
;       ps
;       mu,nu
;       i
;       jc
;       k2
;       u
;       x,y
;       rhos2
;       c2z      cosine^2 of angle between pixel, sun, and filter axis.
;       sr.trW, .trRRs, .trP, and .trVs
;	etc.
;-----------------------------------------

N = sr.Ncube

nn  = (N-1)/2.
; 'roco=-1' relies on rotating the cube, not creating a
;   rotation matrix for the view, so we temporarily use a gamma=0
;   for that case.  roco!=-1 means use the planar geometry given here.
if (sr.roco[s] eq -1) then gamma=0.0 else gamma = sr.gamma[s]

;tau  = sr.tau
tau  = sr.tau[s]
lla0 = sr.lla0
;k0   = sr.k0[*,s]
k0   = p_getk0(sr,s,/sun)

mu0=lla0[0]
nu0=lla0[1]
la0=lla0[2]

is0 = k0[0]
js0 = k0[1] 

z0 = sr.z0[s] 
;d  = sr.d        ; length of voxel edge in L0*tau units
if (tag_exist(sr,'d')) then dvoxfrac=sr.d else $
  dvoxfrac=sr.dvox/sr.L0tau[s]; length of voxel edge in L0*tau units

eps = sr.eps[s]

Ls = sr.LL[s]

; ----- calculate sines and cosines

ff=!pi/180.

cg  = cos(gamma*ff)        ; cos(gamma)
sg  = sin(gamma*ff)        ; sin(gamma)

se = sin(eps*ff) 
ce = cos(eps*ff) 

sq = sg*ce+se*cg
cq = cg*ce-sg*se

; ----- calculate the coordinate transformation operators

W = [                    $
[   1,    0,    0,   0], $
[   0,    1,    0,   0], $
[   0,    0,    1,   0], $
[-mu0, -nu0, -la0, 1/dvoxfrac]]

RRs03 = -Ls*sr.set[s]

RRs=[                          $
[   sq,   0, -cq*tau, 0], $
[  -cq,   0, -sq*tau, 0], $
[    0,   1,       0, 0], $
[RRs03, -z0,   Ls*ce, 1]]

P=[      $
[1,0,0], $
[0,1,0], $
[0,0,1], $
[0,0,0]]

; ?????? changed to natural viewpoint=IDL
;Vs=[           $
;[  1,   0, 0], $
;[  0,   1, 0], $
;[is0, js0, 1]]

Vs=[           $
[ -1,   0, 0], $
[  0,   1, 0], $
[is0, js0, 1]]

sr.trW = W
sr.trRRs[*,*,s]=RRs
sr.trP = P
sr.trVs[*,*,s]=Vs

; ----- calculate 2-d arrays

mu = dblarr(N,N)     ; first voxel index
nu = dblarr(N,N)     ; second voxel index (la is third)
u  = bytarr(N,N)+1

for k=0,N-1 do begin
  mu[k,*]=k
  nu[*,k]=k
end

; position of la=0 voxels in image coordinates

Lam=dblarr(4,N,N)    
Lam[0,*,*]=mu
Lam[1,*,*]=nu
Lam[2,*,*]=0
Lam[3,*,*]=1

; position of voxel in normed solar coordinates
          
;r  = W#Lam
r=dblarr(4,N,N)
for ia=0,3 do for ib=0,3 do for ic=0,N-1 $
do for id=0,N-1 do $
r[ia,ic,id]=r[ia,ic,id]+W[ia,ib]*Lam[ib,ic,id]

x  = reform(r[0,*,*]/r[3,*,*])
y  = reform(r[1,*,*]/r[3,*,*])
ps = x*cg+y*sg

; position of voxel in normed satellite coordinates

;Rs = RRs#r          
Rs=dblarr(4,N,N)
for ia=0,3 do for ib=0,3 do for ic=0,N-1 $
do for id=0,N-1 do $
Rs[ia,ic,id]=Rs[ia,ic,id]+RRs[ia,ib]*r[ib,ic,id]

xs = reform(Rs[0,*,*]/Rs[3,*,*])
zs = reform(Rs[2,*,*]/Rs[3,*,*])
rhos2 = (tau*xs)^2+zs^2

; position of pixel (from projected voxel) in normed data coordinates

;K  = Vs#P#Rs        
Q=Vs#P
K=dblarr(3,N,N)
for ia=0,2 do for ib=0,3 do for ic=0,N-1 $
do for id=0,N-1 do $
K[ia,ic,id]=K[ia,ic,id]+Q[ia,ib]*Rs[ib,ic,id]

i  = reform(K[0,*,*]/K[2,*,*])
jc = reform(K[1,*,*]/K[2,*,*])
K2 = reform(K[2,*,*])

;; ffunc_03b does not do this part

; calculate [i0,j0] - data indices of sun center
lla0 = [sr.lla0,1]
K    = Vs#P#RRs#W#lla0
i0   = K[0]/K[2]
j0   = K[1]/K[2]

; use mu and nu as i and j

zeta=atan(nu-j0,mu-i0)
c2z=cos(zeta-sr.fan[s]*!pi/180.)^2
s2z=1-c2z

;stop,'ready for viewplane,n,la,i,jc,k2 or viewline,n,ii,jj,i,jc'

return
end
