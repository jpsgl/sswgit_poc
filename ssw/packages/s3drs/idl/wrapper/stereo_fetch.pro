;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : stereo_fetch
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;
; Use this routine to extract any item from the Pixon objects,
; as it applies all the necessary de-normalizations and unit
; conversions to give you back physically meaningful data and image items.
;               
; wrapper for ->get(/data,/nop) to ensure we always remember
; to multiply by Istd (as does stereo_project) to get real units.
;
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;
; Default is /data, instead can return appropriate /wt or /sigma or
; /sn (signal-to-noise ratio) or /residuals or even
; /image-- note that images are not modified by Istd as they return e-
;
; If given /addmask, creates a mask from the object wt then applies it
; to the returned data item (unless item is an image, of course).
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; wrapper for ->get(/data,/nop) to ensure we always remember
; to multiply by Istd (as does stereo_project) to get real units.
;
;
;

function stereo_fetch,pxwrapper,oPixon,wt=wt,sigma=sigma,data=data,$
                      render=render,image=image,mask=mask,sn=sn,$
                      residuals=residuals,qmap=qmap,addmask=addmask

  data=KEYWORD_SET(data)
  wt=KEYWORD_SET(wt)
  sigma=KEYWORD_SET(sigma)
  render=KEYWORD_SET(render)
  image=KEYWORD_SET(image)
  sn=KEYWORD_SET(sn)
  residuals=KEYWORD_SET(residuals)
  qmap=KEYWORD_SET(qmap)
  mask=KEYWORD_SET(mask)
  addmask=KEYWORD_SET(addmask)

  ; ensures proper pixon env is loaded, if needed
  pixon_env,'c',/local

  if (wt) then begin
      retdata = oPixon->get(/wt,/nop)
  end else if (sigma) then begin
      ; note we prefer 1/wt over sigma, since Pixon ignores sigma per se
      retdata = ratio(1.0,oPixon->get(/wt,/nop))
  end else if (render) then begin
      retdata = stereo_project(pxwrapper,oPixon)
  end else if (sn) then begin
      signalD = stereo_fetch(pxwrapper,oPixon,/data)
      noiseD = stereo_fetch(pxwrapper,oPixon,/sigma)
      retdata=ratio(signalD,noiseD)
      ; note s/n is a ratio and do need to be normalized
  end else if (residuals or qmap) then begin
      inputD = stereo_fetch(pxwrapper,oPixon,/data)
      outputD = stereo_fetch(pxwrapper,oPixon,/render)
      noiseD = stereo_fetch(pxwrapper,oPixon,/sigma)
      if (qmap) then begin
          retdata=ratio((abs(inputD-outputD)),noiseD)
      end else begin
          retdata=ratio((inputD-outputD),0.5*(inputD+outputD))
      end
      ; note residuals are already scaled appropriately here
  end else if (image) then begin
      retdata = oPixon->get(/im,/nop)
  end else begin
      data=1
      retdata = oPixon->get(/data,/nop)
  end

  if (wt or sigma or data) then begin
      for m=0,n_elements(retdata[0,0,*])-1 do $
        retdata[*,*,m]=pxwrapper.sr.Istd[m]*retdata[*,*,m]/pxwrapper.sr.normd
  end else if (image) then begin
      retdata=retdata/pxwrapper.sr.normd
  end

  if (mask) then retdata=wt2mask(oPixon)

  ; can optionally mask masked region before returning
  if (addmask) then begin
      if (wt or sigma or data or render) then $
        retdata=retdata*wt2mask(oPixon)
  end

  return,retdata

end
