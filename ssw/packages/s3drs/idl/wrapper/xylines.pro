;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : xylines
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: Given an image and a set of rotations e.g. corresponding to
; the ecliptic positions of STEREO A and B relative to 0,
; overlays lines from the center to those positions, derotates
; the image, and ultimately returns the altered image in plaec
; of the original.
;               
; Use       : typically called from 's3drs.pro' or various plotting wrappers
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : display, widget
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; Given an image and a set of rotations e.g. corresponding to
; the ecliptic positions of STEREO A and B relative to 0,
; overlays lines from the center to those positions, derotates
; the image, and ultimately returns the altered image in plaec
; of the original.
;
; e.g. threeview,solution_image,/pixon,/noplot,three
;      zimg=three[*,*,2]
;      xylines,zimg,pxwrapper.sr.rho
;      tv_multi,zimg


PRO xylines,frame,angles

  N=n_elements(frame[0,*])

;   ; old method, introduced many rotation errors, now redundant
;  for i=0,n_elements(angles)-1 do begin
;      ;print,'rotating for angle ',angles[i]
;      frame=rot(/INTERP, frame,angles[i],1.0,/pivot)
;      frame[N/2,0:N/2]=max(frame)
;      frame=rot(/INTERP, frame,0.0-angles[i],1.0,/pivot)
;  end

  for i=0,n_elements(angles)-1 do begin
      overlay=frame*0.0
      overlay[0:N/2,N/2]=max(frame)
      overlay=rot(/INTERP, overlay,0.0-angles[i],1.0,/pivot)
      frame=frame + overlay
  end

END
