;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : find_cdata
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;
; little file to hunt in known locations for the closest match
; to the given time.  Works for C2, C3, Cor2, and Cor1.
;
; if given /justtime, returns the nearest time, otherwise returns
;  the actual file name.  Note that for C2 and C3, you need the
;  filenames, whereas for Cor2 and Cor3, the filenames contain the
;  times.
;               
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; little file to hunt in known locations for the closest match
; to the given time.  Works for C2, C3, Cor2, and Cor1.
;
; if given /justtime, returns the nearest time, otherwise returns
;  the actual file name.  Note that for C2 and C3, you need the
;  filenames, whereas for Cor2 and Cor3, the filenames contain the
;  times.

FUNCTION find_cdata,datetime,c2=c2,cor1=cor1,cor2=cor2,ab=ab,c3=c3,$
                   justtime=justtime

   c2=KEYWORD_SET(c2)
   c3=KEYWORD_SET(c3)
   cor1=KEYWORD_SET(cor1)
   cor2=KEYWORD_SET(cor2)
   if (c2 eq 0 and cor1 eq 0 and cor2 eq 0) then c3=1; default
   if (n_elements(ab) eq 0) then ab='a'
   justtime=KEYWORD_SET(justtime)

   if (cor1) then tag='cor1' else if (cor2) then tag='cor2' else $
     if (c2) then tag='c2' else tag='c3'

   items=strsplit(datetime,'_',/extract)
   date=strmid(items[0],2)
   t1hr=long(strmid(items[1],0,2))
   t1min=long(strmid(items[1],2,2))

;   print,'looking ',date,',',t1hr,':',t1min

   lascodir='/net/corona/lasco/lz/level_05/'
   secchidir=getenv('secchi')+'/lz/L0/'
   if (cor1 or cor2) then godir=secchidir+ab+'/seq/'+tag+'/'+items[0] + '/' $
   else godir = lascodir + date + '/' + tag + '/'

;   print,'godir is ',godir

   filelist=file_search(godir,"*.fts",count=icount)
;   print,filelist[1]

   delta=long(24*60); start with a large delta, i.e. wrong day
   answer=-1
;   print,icount,',',godir,delta

;   print,ab,' checking directory ',godir; debug

   for iloop=0,icount-1 do begin
       fn=filelist[iloop]
       if (c2 or c3) then begin
           hdr=lasco_fitshdr2struct(headfits(fn))
           timestr=hdr.time_obs
           filter=hdr.filter
           polar=hdr.polar
           ; only want unfiltered Lasco data
           if (filter ne 'Clear' or polar ne 'Clear') then timestr='00:00:00.000'
       end else begin
           ; cor1 or cor2
           ignore=sccreadfits(fn,hdr,/nodata)
           if (hdr.polar eq 0) then begin
               times=strsplit(hdr.date_obs,'T',/extract)
               timestr=times[1]
;               print,'found timestr ',timestr,' in file ',fn; debug
           end
       end

       t2hr=long(strmid(timestr,0,2))
       t2min=long(strmid(timestr,3,2))
       newdelta=(t2hr-t1hr)*60 + (t2min-t1min)

       ; I used to format the time based on the FITS header, but
       ; it turns out sometimes the FITS filename and the time in
       ; the FITS header are mismatched, e.g. 20070201_180240_s7c2B.fts
       ; has '180240' in filename but '180249' in the header.
       ; So now I just use the time string from the file name
       ;fmttime=strmid(timestr,0,2)+strmid(timestr,3,2)+strmid(timestr,6,2)
       fmttime=strmid(fn,15,6,/reverse)
       if (abs(newdelta) lt delta) then begin
           if (justtime) then answer=items[0]+'_'+fmttime else answer=fn
           delta=abs(newdelta)
;           print,'Saving ',answer; debug
       end

;       print,fn,' ',items[1],' vs ',hdr.time_obs,' = delta ',newdelta,' vs old ',delta

       if (newdelta gt delta) then break; we are now getting past it in time
   end

;   print,'Answer for ',ab,' is ',answer; debug
   return,answer

END
