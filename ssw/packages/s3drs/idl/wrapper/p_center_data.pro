;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : p_center_data
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;
; center_data_06 with modifications for latest sr structures.
; backward compatibility with original sr structures kept in
;               
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; center_data_06 with modifications for latest sr structures.
; backward compatibility with original sr structures kept in
;
; Also added /check flag, without it we do not check the validity
; of the transform.
;

pro p_center_data,sr,Ns,nn,check=check

  check=KEYWORD_SET(check)

;print,'*** fix center_data_06.pro !!! ***'
; ??? this subroutine just slides the data array around on the data plane
; to get the center of the image cube onto the center of the data array.
; It also ensures the sun center (which may be diff from optical
; center) is transformed identically.
;
; Paul sez: I think what we should be doing is keeping the optical center
; in the center of the data array (sr.k0 = [1,1]*(N-1)/2 for all frames)
; and altering the pointing (sr.z0, sr.eps, sr.set)

; input  s - stereo_in structure
; output sr.k0 will be altered such that the center of the image cube falls on
; the center of the data array, and sr.sunk0 will get the same
; transform as sr.k0
;
; Mildy modified so it is the same as is needed in 'pixonmodels.pro'
;

if (tag_exist(sr,'sr')) then mysr=sr.sr else mysr=sr

if (tag_exist(mysr,'Ncube')) then N=mysr.Ncube else N=mysr.N

if (n_elements(nn) eq 0) then nn = 0.5*(N-1)
if (n_elements(Ns) eq 0) then Ns = n_elements(mysr.gamma)

;stop,'read this'
; uses ffunc_03b which doesnt seem right when there is a 
; ffunc_06 available. However, the arguements to 06
; are different

for kk=0,Ns-1 do begin       ; reset k0 so that image center is data center

;  ffunc_03b,mysr,kk             ; get operators
  ffunc,mysr,kk             ; get operators

  W   = mysr.trW
  RR  = mysr.trRRs[*,*,kk]
  P   = mysr.trP
  Vs  = mysr.trVs[*,*,kk]
  lam = [nn,nn,nn,1]         ; image center
  k   = Vs#P#RR#W#lam        ; calculate [is,js] of image center
  ix = k[0]/k[2]
  jx = k[1]/k[2]
;  mysr.k0[0,kk] = nn-ix+mysr.k0[0,kk]      ; reset k0
;  mysr.k0[1,kk] = nn-jx+mysr.k0[1,kk]

  ; since ffunc uses sun center, we are shifting our sun center
  k00 = nn-ix+p_getk0(mysr,kk,0,/sun)      ; reset k0
  k01 = nn-jx+p_getk0(mysr,kk,1,/sun)
  if (tag_exist(mysr,'sunk0')) then mysr.sunk0[kk] = ptr_new([k00,k01])

  ; shift our optical center by the same amount
  k00 = nn-ix+p_getk0(mysr,kk,0)      ; reset k0
  k01 = nn-jx+p_getk0(mysr,kk,1)
  if (datatype(mysr.k0) eq 'DOU') then begin
      mysr.k0[*,kk]=[k00,k01]
  end else begin
      mysr.k0[kk] = ptr_new([k00,k01])
  end

end

; ------ NOW CHECK IT
if (check) then begin
  for kk=0,Ns-1 do begin       ; reset k0
;      ffunc_03b,mysr,kk           ; get operators
      ffunc,mysr,kk           ; get operators
      W   = mysr.trW
      RR  = mysr.trrrs[*,*,kk]
      P   = mysr.trP
      Vs  = mysr.trVs[*,*,kk]
      lam = [nn,nn,nn,1]
      k   = Vs#P#RR#W#lam
      ix = k[0]/k[2]
      jx = k[1]/k[2]
      if(abs(ix-nn) gt 1.0e-10)then stop,'center_data_06.pro: error 1 '+string(kk)
      if(abs(jx-nn) gt 1.0e-10)then stop,'center_data_06.pro: error 2 '+string(kk)
  end
end

if (tag_exist(sr,'sr')) then sr.sr=mysr; cast back if needed

return
end

