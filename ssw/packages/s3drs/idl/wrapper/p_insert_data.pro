;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : p_insert_data
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;               
; puts data arrays into the Pixon object.
;
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; Simple sanity check here to make sure sizes fit expectation
; Note we only check a) data and b) first data item if it's a pointer
;   array, so you can still seg-fault if you pass it bad sigma, masks,
;   or an array of pointers containing data of different sizes.

FUNCTION p_sizecheck,datum,Ndata,iele

  if (n_elements(iele) eq 0) then iele=-1

  sizes=size(datum)
; is 0, ... for a single ptr,
;    1, ... for an array of pointers,
;    2,N,N for a single data,
;    3,N,N for an array of data
  if (sizes[0] lt 2) then sizes=size(*(datum[0]))
  if (iele eq -1) then icheck=0 else icheck=iele
  if (sizes[1] ne sizes[2] or sizes[1] ne Ndata[icheck]) then $
    return,0 else return,1

END

pro p_insert_data,pxwrapper,datum,oPixon,iele=iele,$
                  sigma=sigma,poisson=poisson,masks=masks,nonorm=nonorm,$
                  autoscale=autoscale,retro=retro,image=image,check=check
;
;+
; $Id: p_insert_data.pro,v 1.1 2009/04/22 19:22:24 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : 
;             
; Purpose   : inserts datum into a pixon object, optionally also
;             inserting a provided noise weight &/or mask (wt = mask/sigma)
;               
; Explanation: 
;              
;               
; Use       : IDL> p_insert_data,pxwrapper,datum,oPixon
;    
; Inputs    : 
;               
; Outputs   : 
;
; Keywords  : 
;
; Calls from LASCO : none 
;
; Common    : none 
;               
; Restrictions: none
;               
; Category    : Simulation
;               
; Prev. Hist. : See Paul Reiser's code and notes.
;
; Written     : Sandy Antunes, NRL, Nov-Dec 2005
;               
; Inserts datum and/or sigma into oPixon as 'data' and 'wt', respectively.
;
; if given (iele), inserts the ptr into the object at that position,
; otherwise the default is to insert the dataset as a whole.
; When inserting a single item, pass only the single datum, not
; an array of datum.  When inserting a set, obviously you have
; to pass in an array of datum.
; Note the pixon object must already exist and be of the
; appropriate size-- ptr array of data has proper number of elements.
; So you may run into trouble inserting single data items until you've
; first put in a full set... we have not tested if this is the case or not.
;
; Note that, in general, we multiply our input data by sr.normd
; before inserting.  However, if you set /nonorm, this will be skipped.
;
; /retro uses the earlier Reiser method, which does not allow for pointers
;
; /check prints out some noise summary for verification.
;

check=KEYWORD_SET(check)
nonorm=KEYWORD_SET(nonorm)
if (n_elements(iele) eq 0) then iele=-1
if (iele eq -1) then single=0 else single=1
if (iele eq -1) then Nd=pxwrapper.sr.Nd else Nd=1

; ability to call earlier version for tetra, unlikely to ever be used
if (pxwrapper.object ne 'c') then begin
    print,'Using earlier, less tested tetra/retro p_insert code.'
    p_insert_retro,pxwrapper,datum,oPixon,iele=iele,sigma=sigma,masks=masks,$
      nonorm=nonorm
    return
end

if (nonorm) then normd=1.0 else normd=pxwrapper.sr.normd

; Note that 'force_ptrs' can apply a normalization that ends up
;   applying to the returned data but does not alter the original
;   data, so in general 'force_ptrs' actually returns a copy of
;   the original data, for safety.

if (n_elements(datum) ne 0) then begin

    if (p_sizecheck(datum,pxwrapper.sr.Ndata,iele) eq 0) then $
      stop,'data size mismatched to wrapper expectation, aborting'

    force_precision,datum,pxwrapper.sc.doubleprec
    dptr=force_ptrs(datum,Nd,norm=normd/pxwrapper.sr.Istd)

    oData = oPixon->get(/oData)
    if (iele eq -1) then begin
        for m=0,Nd-1 do oData[m]->Set, data=dptr[m]; a set
    end else begin
        oData[iele]->Set, data=dptr[0]; a single
    end
end

if (n_elements(sigma) ne 0) then begin

    if (p_sizecheck(sigma,pxwrapper.sr.Ndata,iele) eq 0) then $
      stop,'sigma size mismatched to wrapper expectation, aborting'

    psig=force_ptrs(sigma,norm=normd/pxwrapper.sr.Istd)
    if (n_elements(masks) ne 0) then begin

        if (p_sizecheck(masks,pxwrapper.sr.Ndata,iele) eq 0) then $
          stop,'mask size mismatched to wrapper expectation, aborting'

        msig=force_ptrs(masks)
    end else begin
        msig=ptrarr(Nd)
        for i=0,Nd-1 do msig[i]=ptr_new( *(psig[i]) * 0.0 + 1.0)
    end

    wt=ptrarr(Nd)
    for m=0,Nd-1 do begin
        mywt=ratio(*(msig[m]),*(psig[m]))
;        fwt=where(mywt gt 0.2)
;        if (fwt[0] ne -1) then mywt[fwt]=0.2; cap it off
        wt[m] = ptr_new(mywt)
    end
;    help,wt,wt[0],*wt[0]

;    help,*psig[0],*wt[0],*msig[0]

    if (iele eq -1) then begin
        oPixon->set,wt=wt; a set
    end else begin
        wtset=oPixon->get(/wt)
        wtset[iele]=wt[0]
        oPixon->set,wt=wtset; a single
    end

    if (check) then begin
        print,'check:'
        sigout=opixon->get(/sigma,/nop)
        wtout=opixon->get(/wt,/nop)
        ie=where (wtout > 0)
        print,'input sigma: ',minmax(sigma*pxwrapper.sr.normd)
        print,'output 1.0/wt: ',1.0/minmax(wtout[ie])
        print,'output sigma: ',minmax(sigout)
        print,'wt is ',minmax(wtout)
    end

end

END
