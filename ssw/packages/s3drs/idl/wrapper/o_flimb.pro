pro o_flimb,ch,N,RR,dvoxfrac,lla0,fa,fb,fc,rmin2,an,ImI0
;+
; $Id: o_flimb.pro,v 1.1 2009/04/22 19:22:17 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : o_flimb.pro
;               
; Purpose   : calculate limb darkening function for classic Pixon
;               
; Explanation: 
; This subroutine returns the tangential and radial functions fa and fb
; multiplied by (N/RR)^2
; to be used in the dsf and dsft functions for calculating electron density.
; For radii smaller than 1.1 solar radii, the functions are set to the value
; at 1.1 solar radii.
;               
; Use       : IDL> o_flimb,ch,N,RR,dvoxfrac,lla0,fa,fb,fc,rmin2,an
;    
; Inputs    : 
;         ch    - choice of limb darkening model
;         N     - number of pixels on one side of the image cube
;         RR    - normalized solar radius (L0*tau units) 
;         dvoxfrac     - normalized length of edge of voxel (L0*tau units)
;         lla0  - image coordinates of sun center
;               
; Outputs   : 
;         fa,fb,fc - extended sun functions Sigma_A, B, and C
;                    fa[i] is the value of fa at radius r (in image coordinates)
;                    where i=(r^2-rmin2)/(dvoxfrac*dvoxfrac)
;         rmin2    - minimum value of r^2
;         ImI0     - the ratio Im/I0 where Im is mean solar intensity,
;                    and I0 is solar intensity at disc center.
;
; Keywords  : none
;               
; Calls from LASCO : none 
;
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : Simulation
;               
; Prev. Hist. : See Paul Reiser's code and notes.
;
; Written     : Paul Reiser, imported by Sandy Antunes, NRL, Nov-Dec 2005
;               
; $Log: o_flimb.pro,v $
; Revision 1.1  2009/04/22 19:22:17  antunes
; code relocation part 2 of 3.
;
; Revision 1.7  2008/11/13 16:23:36  antunes
; Checkin after long hiatus.
;
; Revision 1.6  2008/03/11 19:07:41  antunes
; Added flyby functionality.
;
; Revision 1.5  2008/01/25 16:10:38  antunes
; Major refactoring of code to allow differing FOVs.  Note Pixon
; structures here are now incompatible with earlier versions!
;
; Revision 1.4  2007/06/26 18:56:18  antunes
; Got Pixon to work with real SECCHI FITS data!
;
; Revision 1.3  2006/11/16 18:28:06  antunes
; Added these back here, they were temporarily deleted.
;
; Revision 1.1  2006/04/11 15:55:05  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.2  2005/12/27 17:48:24  antunes
; Commented half of the routines using SSW style.  Also added a new
; axes test case 'arrowcube'.
; Checked in the Tomography/old/mikic_img.pro routine here because it
; is a dependency for the general image-prep routine 'get_timage.pro'
;
;-            


; 
;
;-----------------------------------------

; find iimin and iimax

if (ch eq -1) then begin
  ; skip calculation entirely, for testing and non-Pixon
    an=[1.0,1.0,1.0]
    ImI0=1.0
    return      
end

mu0=lla0[0]
nu0=lla0[1]
la0=lla0[2]

mu=lonarr(N,N)
nu=lonarr(N,N)
for i=0,N-1 do begin
  mu[i,*]=i
  nu[*,i]=i
end


for la=0,N-1 do begin
  ii = (mu-mu0)^2+(nu-nu0)^2+(la-la0)^2
  newmin=min(ii)
  newmax=max(ii)
  if(la eq 0)then iimin=newmin else if(iimin gt newmin)then iimin=newmin
  if(la eq 0)then iimax=newmax else if(iimax lt newmax)then iimax=newmax
end

rmin2 = iimin*dvoxfrac*dvoxfrac

; -----

NN = iimax-iimin+1	; number of elements in fa, fb, fc

fa  = dblarr(NN)
fb  = dblarr(NN)
fc  = dblarr(NN)

Ro  = double(RR)
Ro2 = Ro^2

case ch of

0: begin	; no limb darkening, point sun

   a0 = 1
   a1 = 0
   a2 = 0
   an = [a0,a1,a2]
   for ii=0l,NN-1 do begin
     r2     = dvoxfrac*dvoxfrac*(double(ii) + iimin)
     fa[ii] = Ro2/r2
     fb[ii] = Ro2/r2
     fc[ii] = 0
     if(r2 le 2) then begin
       fa[ii]=2
       fb[ii]=2
     end
   end
   ImI0=1.0
   end

1: begin	; polynomial limb darkening, point sun

   a1 =  0.93
   a2 = -0.23
   a0 = 1-a1-a2
   an = [a0,a1,a2]
   ImI0 = 1-a1/3-a2/2
   v  = ImI0*Ro2
   
   for ii=0l,NN-1 do begin
     r2 = dvoxfrac*dvoxfrac*(double(ii)+iimin)
     fa[ii] = v/r2
     fb[ii] = v/r2
     fc[ii] = 0
   end

   end

2: begin	; polynomial limb darkening, extended sun

   a1 =  0.93
   a2 = -0.23
   a0 = 1-a1-a2
   an = [a0,a1,a2]
   ImI0 = 1-a1/3-a2/2

   iimax=long(Ro2/(dvoxfrac*dvoxfrac)-iimin+1)
   if(iimax lt 0)then iimax=0

   for ii=long(iimax),NN-1 do begin
   
     r2 = dvoxfrac*dvoxfrac*(double(ii)+iimin)
     s2 = Ro2/r2
     s  = sqrt(s2)
     c2 = 1-s2
     c  = sqrt(c2)
     c3 = c*c2
     q  = alog((1+s)/c)
     p  = (1-c)^2

     aa0 = (4-3*c-c3)/3
     aa1 = (5+s2-c2*(5-s2)*q/s)/8
     aa2 = 2*p*(c3+2*c2+8*c+4)/(15*s2)
     fa[ii] = a0*aa0+a1*aa1+a2*aa2
     
     bb0 = c*s2
     bb1 = -(1-3*s2-c2*(1+3*s2)*q/s)/8
     bb2 = 2*p*(3*c3+6*c2+4*c+2)/(15*s2)
     fb[ii] = a0*bb0+a1*bb1+a2*bb2

     fc[ii] = fa[ii]-fb[ii]
 
   end

   end

3: begin        ; junk: testing polynomial limb darkening, extended sun

   hs2=fa*0

   a1 =  0.93
   a2 = -0.23
   a0 = 1-a1-a2
   an = [a0,a1,a2]
   ImI0 = 1-a1/3-a2/2

   iimax=long(Ro2/(dvoxfrac*dvoxfrac)-iimin+1)
   if(iimax lt 0)then iimax=0

   for ii=iimax,NN-1 do begin

     r2 = dvoxfrac*dvoxfrac*(double(ii)+iimin)
     s2 = Ro2/r2
     hs2[ii]=s2
     s  = sqrt(s2)
     c2 = 1-s2
     c  = sqrt(c2)
     c3 = c*c2
     q  = alog((1+s)/c)
     p  = (1-c)^2

     aa0 = (4-3*c-c3)/3
     aa1 = (5+s2-c2*(5-s2)*q/s)/8
     aa2 = 2*p*(c3+2*c2+8*c+4)/(15*s2)
     fa[ii] = a0*aa0+a1*aa1+a2*aa2

     bb0 = c*s2
     bb1 = -(1-3*s2-c2*(1+3*s2)*q/s)/8
     bb2 = 2*p*(3*c3+6*c2+4*c+2)/(15*s2)
     fb[ii] = a0*bb0+a1*bb1+a2*bb2

     fc[ii] = fa[ii]-fb[ii]

;;
;fb[ii]=ImI0*s2
;fb[ii]=(a0+2*a1/3+a2/2)*s2 - (a0/4+4*a1/15+a2/6)*s2*s2
;fc[ii]=0

   end

erra = (fa-ImI0*hs2)/fa
errb = (fb-ImI0*hs2)/fb
errc = (fc-hs2*hs2*(a0/2+4.*a1/15+a2/6))/fc
q=where(fa ne 0)
erra = erra[q]
errb = errb[q]
errc = errc[q]
r=sqrt(1/hs2)
r=r[q]
;stop,'flimb'

   end


else: stop,'ERROR flimb_06- ch = '+string(ch)
end


return
end

