;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : pixoncoords
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;
; These are the coordinate converters for Pixon, to convert HAE x,y,z
; to the specific units and scaling required by Pixon, or back again.
;
; They are bundled under a wrapper, 'pixoncoords', so that I can
; include both forward and reverse transformations in the same file,
; for easier checking that they are the same transformation.
;
; Usage: pixoncoords,[x,y,z],rho,phi,LL,z0,ecliptic
;   As given, converts [x,y,z] to rho,phi,LL,z0,ecliptic
;   Use /reverse to convert rho,phi,LL to [x,y,z] (z0 and ecliptic ignored)
; /verbose prints result to screen as well
; /au indicates [x,y,z] are or should return AU instead of default meters
; /radians or /degrees indicate rho,phi are or should return rad or deg (def)
; L0=L0 sets an L0 other than the default unit scaling 1=1AU
;               
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; These are the coordinate converters for Pixon, to convert HAE x,y,z
; to the specific units and scaling required by Pixon, or back again.
;
; They are bundled under a wrapper, 'pixoncoords', so that I can
; include both forward and reverse transformations in the same file,
; for easier checking that they are the same transformation.
;
; Usage: pixoncoords,[x,y,z],rho,phi,LL,z0,ecliptic
;   As given, converts [x,y,z] to rho,phi,LL,z0,ecliptic
;   Use /reverse to convert rho,phi,LL to [x,y,z] (z0 and ecliptic ignored)
; /verbose prints result to screen as well
; /au indicates [x,y,z] are or should return AU instead of default meters
; /radians or /degrees indicate rho,phi are or should return rad or deg (def)
; L0=L0 sets an L0 other than the default unit scaling 1=1AU
;
;
;
; x,y,z are HAE coordinates, in meters (if in AU, give the /au flag)
; rho,phi,LL are equivalent of spherical rho,phi,d
; rho,phi,z0 are cylindrical units
;
; Default for rho,phi is to accept or return degrees, unless /radian
; is set.  You can also specify /degrees (even though it is the
; default) for clarity.  If you specify none, it uses /degrees.  If
; you specify both /degrees and /radians, it uses /radians.
;
; Note that Pixon expects LL and z0 to use normalized units.
; Defaults to a Pixon scale where d=1.0=1AU in size.
; Note Pixon's L0 is a scaling factor used heavily and is the average
; sun-satellite distance, usually '1 au in solar radii', L0=214.943
; All units tend to be scaled to this so the normal distance unit is '1.0'
; So LL(final) = LL(solar radii)/L0
; or, if LL is in AU, LL(final) = LL(AU) (technically, LL(AU)*214.943/L0)
; and likewise z0(final) = z0(AU) ,or z0(solar radii)/L0 or z0(AU)*214.943/L0
;
; So we assume that the scaling L0 sets everything to 1AU, but if L0
; is something other than AU (other than L0=214.943), you _must_
; provide L0=L0 in the invocation to get out the proper units!  It
; never is a problem to specify L0, even if it's the default scaling,
; but again, if you neglect it we default to assuming d=1.0AU and L0=214.943.
;

pro test_ztop
  ; special test for checking 'top down' rho=90 degree case.

  rho=90.0
  pixoncoords,xyz,0.0,rho,1.00,/reverse,/au,/verbose

  pixoncoords,xyz,rho,phi,LL,z0,ecliptic,/au,/verbose

end

pro test_xyz2pixon
  ; generalized test script to make sure the transforms are consistent.

  ; uses an HAE x,y,z from a real problem and checks transforms

  ; in meters, xyz=-1.4431252e+11  -6.8996228e+09  -1.6542070e+08
  ; rho=-177.26275,phi=-0.065601302,z0=-0.0011057681,LL=0.96577135,ec=2.15105

  x=-1.4431252e+11
  y=-6.8996228e+09
  z=-1.6542070e+08
  rho=-177.26275
  phi=-0.065601302
  z0=-0.0011057681
  LL=0.96577135
  ec=2.15105

  xyz2pixon,[x,y,z],rhoB,phiB,LLB,z0B,eclipticB

  xyzB=sphere2xyz(rhoB,phiB,LLB)

  xyz2pixon,xyzB,rhoC,phiC,LLC,z0C,eclipticC

  print,'input x,y,z: ',x,y,z
  print,'known rho/phi/LL/z0/ec: ',rho,phi,LL,z0,ec
  print,'input gives us rho/phi/LL/z0/ec: ',rhoB,phiB,LLB,z0B,eclipticB
  print,'back transforms to x,y,z: ',xyzB
  print,'which gives rho/phi/LL/z0/ec: ',rhoC,phiC,LLC,z0C,eclipticC

  print,'     And same thing via wrappers: '

;  xyz2pixon,[x,y,z],rhoB,phiB,LLB,z0B,eclipticB
  pixoncoords,[x,y,z],rhoB,phiB,LLB,z0B,eclipticB
;  xyzB=sphere2xyz(rhoB,phiB,LLB)
  pixoncoords,xyzB,rhoB,phiB,LLB,/reverse
;  xyz2pixon,xyzB,rhoC,phiC,LLC,z0C,eclipticC
  pixoncoords,xyzB,rhoC,phiC,LLC,z0C,eclipticC

  print,'input x,y,z: ',x,y,z
  print,'known rho/phi/LL/z0/ec: ',rho,phi,LL,z0,ec
  print,'input gives us rho/phi/LL/z0/ec: ',rhoB,phiB,LLB,z0B,eclipticB
  print,'back transforms to x,y,z: ',xyzB
  print,'which gives rho/phi/LL/z0/ec: ',rhoC,phiC,LLC,z0C,eclipticC

end


; converts a desired spherical rho (orbital angle), phi (angle above) and d
; (in AU) to Pixon-ready x,y,z.
; This x,y,z output can then be fed into 'xyz2pixon' to get the full
; Pixon coordinate set needed, e.g.:
;   xyz2pixon,sphere2xyz(rho,phi,d),/au,rho,phi,LL,z0

; You _must_ use either /degrees or /radians to indicate your units,
; /degrees is the default.  If both given, /radians overrides.
;
; Use /au to return an xyz in AU, otherwise returns meters

function sphere2xyz,rho,phi,d,au=au,degrees=degrees,radians=radians,$
                    verbose=verbose,L0=L0
  ; rho is equiv of longitude aka angle in x-y plane angle
  ; phi is equivalent of latitude aka angle up off x-y plane

  au2m=149598000.0 * 1000.0;

  au=KEYWORD_SET(au)

  if (n_elements(L0) eq 0) then L0=-1
  degrees=KEYWORD_SET(degrees)
  radians=KEYWORD_SET(radians)
  verbose=KEYWORD_SET(verbose)

  if (radians) then begin
      rhoop = rho
      phiop = phi
  end else begin
      ; convert from degrees to radians
      rhoop = rho / !radeg
      phiop = phi / !radeg
  end

  ; returns meters unless the /au flag is set
  if (au) then auconv=1.0 else auconv=au2m

  if (L0 gt 0) then begin
      au2rsun = 214.943
      d = d * L0/au2rsun
  end

  x = d * cos(phiop) * cos(rhoop) * auconv
  y = d * cos(phiop) * sin(rhoop) * auconv
  z = d * sin(phiop) * auconv

  if (verbose) then print,'rho/phi/d: ',rho,phi,d
  if (verbose) then print,'coords x/y/z: ',x,y,z,'

  return,[x,y,z]

end


; converts X,Y,Z (presumed HAE in meters)
; to ecliptic Pixon rho,phi,LL,z0 (in AU)
;
; assumes X,Y,Z in meters unless given /au flag
; the returned 'z0' is in AU (by default)
;
; rho is equiv of longitude aka angle in x-y plane angle
; phi is equivalent of latitude aka angle up off x-y plane
;

pro xyz2pixon,xyz,au=au,verbose=verbose,L0=L0,rho,phi,LL,z0,ecliptic,$
              radians=radians,degrees=degrees

  au2m=149598000.0 * 1000.0;

  if (n_elements(xyz) ne 3) then stop,'usage: xyz2pixon,[x,y,z],rho,phi,LL,z0'

  degrees=KEYWORD_SET(degrees)
  radians=KEYWORD_SET(radians)
  au=KEYWORD_SET(au)
  if (n_elements(L0) eq 0) then L0=-1
  verbose=KEYWORD_SET(verbose)

  x=xyz[0]
  y=xyz[1]
  z=xyz[2]

  ; convert from m to AU unless /au was used to indicate input is in AU
  if (au) then auconv=1.0 else auconv=au2m
  xAU=x/auconv
  yAU=y/auconv
  zAU=z/auconv

  ; print,x,y,z

  rho = atan(yAU,xAU)
  phi = atan(zAU, sqrt(xAU^2+yAU^2))
  LL = sqrt(xAU^2+yAU^2+zAU^2)
  z0 = LL * sin(phi)            ; distance above x-y plane

  ; As noted in comments, we are assuming LL and z0 are using our
  ; most-common Pixon scale where Pixon distance 1.0 = 1 AU
  ; (and 1AU = 214.943 solar radii = L0).  However, if the user did
  ; give us a specific L0, we do the conversion to proper scaling here.

  if (L0 gt 0) then begin
      au2rsun = 214.943
      LL = LL * au2rsun/L0
      z0 = z0 * au2rsun/L0
  end

  ; So LL(final) = LL(solar radii)/L0
  ; or, if LL is in AU, LL(final) = LL(AU) (technically, LL(AU)*214.943/L0)
  ; and likewise z0(final) = z0(AU) ,or z0(solar radii)/L0 or z0(AU)*214.943/L0

  ; now that we are done the trig, convert to degrees for readability
  if (radians eq 0) then begin
      ; only convert to degrees if the /radian option was not chosen
      rho = rho * !radeg
      phi = phi * !radeg
  end

 ; Also include angle between solar north and ecliptic north,
 ; in CCW from ecliptic north
 ; Routine taken from festival_ecl_solar_north and embedded in here
 ; so we can use Pixon even if Festival is not installed.

  r = SQRT(x^2.0d + y^2.0d + z^2.0d)
  ecl_lat = ASIN(z/r)
  ecl_lon = ATAN(y, x)
  beta = !dtor*82.746294d
  lambda = !dtor*345.59337d
  sx = COS(beta)*SIN(lambda - ecl_lon)
  sy = COS(ecl_lat)*sin(beta) - SIN(ecl_lat)*COS(lambda - ecl_lon)
  ecliptic = ATAN(sx, sy) * !radeg



  if (verbose) then print,'coords x/y/z: ',x,y,z,'
  if (verbose) then print,'rho/phi/z0/LL: ',rho,phi,z0,LL

end

pro pixoncoords,xyz,rho,phi,LL,z0,ecliptic,$
                au=au,verbose=verbose,L0=L0,reverse=reverse,$
                degrees=degrees,radians=radians

  reverse=KEYWORD_SET(reverse)
  verbose=KEYWORD_SET(verbose)
  au=KEYWORD_SET(au)
  radians=KEYWORD_SET(radians)
  degrees=KEYWORD_SET(degrees)
  if (n_elements(L0) eq 0) then L0=-1

  if (reverse) then begin
      ; reverse transform: pixon2xyz
      xyz=sphere2xyz(rho,phi,LL,au=au,degrees=degrees,radians=radians,$
                    verbose=verbose,L0=L0)

  end else begin
      ; normal transform: xyz2pixon
      xyz2pixon,xyz,au=au,verbose=verbose,L0=L0,rho,phi,LL,z0,ecliptic,$
        degrees=degrees,radians=radians
  end

end
