;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : p_update_runstats
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;               
; updates the pxwrapper.sc runtime stats section from the pixon object
; Used to ensure pxwrapper info is in sync with that stored in the object.
;
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; updates the pxwrapper.sc runtime stats section from the pixon object
;
; If given /alt, gives an alternative mask-imposed formulation of q
;

PRO p_update_runstats,pxwrapper,oPixon,istep,alt=alt

   alt=KEYWORD_SET(alt)

   if (n_elements(istep) ne 0) then $
     pxwrapper.sc.iterations = pxwrapper.sc.iterations + istep

   dof = oPixon->Get(/dof)
   pxwrapper.sc.chisq = oPixon->Get(/chi2,/total,var=chi2var)
;   pxwrapper.sc.q = (pxwrapper.sc.chisq - dof)/(2.0*dof)
   ; note Pixon's running Q is slightly different from our original Q
   ; we actually use what Pixon calles Chi^2/DOF
   pxwrapper.sc.q = pxwrapper.sc.chisq/(dof > 0.0)

   if (pxwrapper.object eq 't') then begin
           ; tetra lacks a pxncnt
       pxwrapper.sc.pcount = 0
   end else begin
       pxwrapper.sc.pcount = oPixon->Get(/pxncnt)
   end

   pxwrapper.sc.totaltime=oPixon->get(/runtime)
   pxwrapper.sc.iterations=oPixon->get(/itercount)
   if (tag_exist(pxwrapper.sc,'endcause')) then $
       pxwrapper.sc.endcause=oPixon->get(/endcause) else $
       endcause='unknown'
 ;  history=oPixon->get(/history)
 ;  endcause=oPixon->get(/endcause)

   if (alt) then begin
       data=stereo_fetch(pxwrapper,oPixon,/data)
       result=stereo_fetch(pxwrapper,oPixon,/ren)
       sigma=stereo_fetch(pxwrapper,oPixon,/sig)
       masks=stereo_fetch(pxwrapper,oPixon,/mask)
       ; ict=n_elements(masks)
       ict=n_elements(where (masks gt 0))

       qmap = ratio((data-result)^2,sigma^2) * masks
       q = total(qmap)/double(ict)
       ;reduced_q1 = total(sqrt(qmap)) / double(ict)
       pxwrapper.sc.q = q
   end

END
