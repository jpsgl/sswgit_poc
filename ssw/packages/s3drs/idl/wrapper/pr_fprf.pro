;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : pr_fprf
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;               
; general routines needed by our stand-alone render and back projection
; primarily to implement the previous Pixon routines 'gprf' & 'prepfprf'
;
; Use       : typically compiled by other routines
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; general routines needed by our stand-alone render and back projection
; primarily to implement the previous Pixon routines 'gprf' & 'prepfprf'

FUNCTION pr_subarray,a,lo,hi
  if (size(a,/n_dimensions) eq 0) then return,0
  ll=LONARR(7)
  if (n_elements(lo) ne 0) then ll=[LONARR(size(a,/n_dimensions))+lo,ll]
  if (n_elements(hi) ne 0) then ur=[LONARR(size(a,/n_dimensions))+hi,$
                                    LONARR(7)] else $
    ur=[size(a,/dimensions)-1,LONARR(7)]
  out = a[ll[0]:ur[0], ll[1]:ur[1], ll[2]:ur[2], ll[3]:ur[3], ll[4]:ur[4], $
          ll[5]:ur[5], ll[6]:ur[6], ll[7]:ur[7]]
  return,out
END

; returns new array shifted along each dimension by corresponding s
FUNCTION pr_ndshift,a,s
  adim=size(a,/dimensions)
  t=pr_subarray([s + intarr(size(a,/n_dimensions)),INTARR(6)],0,6)
  tdim=pr_subarray([adim,REPLICATE(1,6)],0,6)
  a=reform(a,tdim,/overwrite)
  out=shift(a,t[0],t[1],t[2],t[3],t[4],t[5],t[6])
  a=reform(a,adim,/overwrite)
  return,out
END


FUNCTION pr_wrap,a
  return, pr_Ndshift(a, (size(a,/dimensions) + 1)/2)
END

FUNCTION pr_rfft,a,_REF_EXTRA=e

   ndim = size(a,/n_dimensions)
   new = size(a,/dimensions)
   new[0] = new[0]/2 + 1

   out = FFT(a, 1, DIMENSION=1, _EXTRA=e)
   out = pr_SubArray(TEMPORARY(out), 0, new-1)
   for n=2,ndim do out = FFT(TEMPORARY(out),1,DIMENSION=n,/OVERWRITE,_EXTRA=e)

   return,out
END

; This pr_prepfprf is no longer used, just placed inline when needed
; (compiles with fewer complaints that way)
;FUNCTION pr_prepFPRF, psf,_REF_EXTRA=e
;
;   temp = pr_rfft(pr_Wrap(psf))
;   temp = temp/temp[0]
;   return,ptr_new(temp)
;
;END


FUNCTION pr_rfftwrap, a, _REF_EXTRA=e
   return,pr_rfft(pr_Wrap(a), _EXTRA=e)
END

FUNCTION pr_irfft,fa

   out=fa
   fandim=size(fa,/n_dimensions)
   for n=2,fandim do begin
       out = fft(temporary(out),-1,dimension=n,/OVERWRITE,_extra=e)
   endfor

   fadim=size(fa,/dimensions)
   n = fadim[0] - 2
   if n gt 0 then begin
       temp = CONJ(out[n-LINDGEN(n),*,*,*,*,*,*])
       out = [TEMPORARY(out), TEMPORARY(temp)]
   endif

   out = FFT(TEMPORARY(out),-1,dimension=1,/OVERWRITE,_EXTRA=e)
   out = REAL_PART(TEMPORARY(out))
   return, out

END


PRO test_pr_mintest

   on_error,2
   N=32
   prep_3drv,pxwrapper,N=N,gamma=[0,90]
   nix=fltarr(32,32,32)+0.9
   render=pr_render(pxwrapper,nix,0)
   tv_multi,fixed=256,render,/log

   flat=fltarr(32,32)+1.0
   image=pr_backproject(pxwrapper,flat,0)
   threeview,image,fixed=256,/log

END

; stripped down version of Amos Yahil's example code 'gprf',
; used here for the pr_render and pr_backproject stand-alone
; routines.

function pr_gprf, dim, fwhm, center=center, sigma=sigma

   if (n_elements(center) eq 0) then center=dim/2

   if (n_elements(fwhm) ne 0) then sigma=fwhm/sqrt(8.0*alog(2.0)) else $
     if (n_elements(sigma) eq 0) then sigma=1.0

   nele = n_elements(dim)
   center = center + FLTARR(nele)
   sigma = sigma + FLTARR(nele)
   x = PTRARR(nele)
                                ; 1-D PRFs, computed in double precision
   FOR n=0,nele-1 DO BEGIN
      x[n] = PTR_NEW((DINDGEN(dim[n]+1) - 0.5 - center[n])/sigma[n])
      *x[n] = GAUSS_PDF(TEMPORARY(*x[n]))
      *x[n] = (*x[n])[1:*] - (*x[n])[0:dim[n]-1]
   ENDFOR
                                ; The function is the outer product of the 1-D
                                ; PRFs
   out = TEMPORARY(*x[0])
   npxl = LONG(dim[0])
   FOR n=1,nele-1 DO BEGIN
      npxl = npxl*dim[n]
      out = REFORM(TEMPORARY(out) # TEMPORARY(*x[n]), npxl, /OVERWRITE)
   ENDFOR
   PTR_FREE, x
   out = REFORM(out, dim, /OVERWRITE)

   RETURN, float(out)
END


; This is a very awkward wrapper for two completely
; different routines, provided here to make use of
; the above shared functions.
;
; subset of prep_fprf and pgprf needed for rendering
; subset of 'rconv' needed for back projection

function pr_fprf,option,aset,bset

  if (option eq 0) then begin

      ; back projection request, aka 'rconv'

      aset = CONJ(aset)
      rconv = pr_rfft(bset)*aset
      rconv = pr_irfft(TEMPORARY(rconv))
      return,rconv

  end else begin

      ; render request, aka 'prepfprf/gprf'

      prf2d=ptr_new(pr_gprf(aset))
      temp = pr_rfft(pr_wrap(*(prf2d)))
      fprfset = ptr_new(temp/temp[0])
      fprf=fprfset[0]

      return,fprf

  end


end
