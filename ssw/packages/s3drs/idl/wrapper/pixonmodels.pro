;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : pixonmodels
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;
; This creates a lot of sample models for building synthetic data.
;               
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; minor update, now references sr.L0tau[0] i.e. L0tau for
; the first specified data frame, as this element is no longer
; a scalar.  Ditto for sr.tau[0].

FUNCTION pixonmodels,N,modelname,pxwrapper,stuff,stuff1,debug=debug
;+
; $Id: pixonmodels.pro,v 1.1 2009/04/22 19:22:35 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : get_timage.pro
;               
; Purpose   : Generate a chosen model as an N^3 cube
;               
; Explanation: put true image into timage
;              stuff is parameters that are specific to the particular ch
;               
; Use       : IDL> get_timage,sr,sc,timage,stuff,stuff1
;    
; Inputs    : sr : Pixon structure
;             sc : Pixon
;             stuff : model-specific parameters needed
;             stuff1 : more model-specific parameters needed
;               
; Outputs   : timage : N^3 array
;
; Keywords  : none
;               
; Calls from LASCO : none 
;
; Common    : debugs : set of commons Pixon uses to store timing information
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : Simulation
;               
; Prev. Hist. : See Paul Reiser's code and notes.
;
; Written     : Paul Reiser, imported by Sandy Antunes, NRL, Nov-Dec 2005
;               
; $Log: pixonmodels.pro,v $
; Revision 1.1  2009/04/22 19:22:35  antunes
; code relocation part 2 of 3.
;
; Revision 1.17  2009/04/21 19:09:30  antunes
; Updated up through 'r' with SSW headers.  Also minor John bug fixes.
;
; Revision 1.16  2009/04/02 19:08:24  antunes
; GUI 1.91, feature-complete.
;
; Revision 1.15  2009/03/25 16:52:19  antunes
; GUI 1.31, synthetics now working.
;
; Revision 1.14  2008/01/31 14:47:31  antunes
; Fully stable multi-FOV code for Pixon, also more test cases, plus top masking.
;
; Revision 1.13  2008/01/25 16:10:38  antunes
; Major refactoring of code to allow differing FOVs.  Note Pixon
; structures here are now incompatible with earlier versions!
;
; Revision 1.12  2008/01/17 20:36:12  antunes
; Major fix to alignment problems!  Also nicer plotting.
;
; Revision 1.11  2008/01/15 16:50:08  antunes
; Better units, more tests.
;
; Revision 1.10  2008/01/09 19:24:28  antunes
; Finally fixed the data scaling unit (bug=legacy code used AU not Rsun),
; also added more tests.
;
; Revision 1.9  2008/01/04 20:16:49  antunes
; Final units (hopefully), also more tests and tools.
;
; Revision 1.8  2007/11/21 23:12:25  antunes
; Updates to test scripts and diagnostics.
;
; Revision 1.7  2007/10/03 19:01:31  antunes
; Better default handling.
;
; Revision 1.6  2007/10/03 18:44:21  antunes
; Implemented spherical coordinates in cpixon, major update!
;
; Revision 1.5  2007/06/29 13:50:57  antunes
; Checkin of many docs and test cases, for thoroughness.
;
; Revision 1.4  2007/02/12 18:05:18  antunes
; Modified for better handling of optical and sun center.
; Also generalized some earlier pixon legacy code.
;
; Revision 1.3  2006/11/30 20:03:57  antunes
; Updated pixon and other printing and output routines.
;
; Revision 1.2  2006/11/16 19:33:06  antunes
; Successfully replaced k0 with p_getk0 to allow for pointers
; and thus dynamic image loading.
;
; Revision 1.1  2006/11/16 18:38:14  antunes
; Final checkin before I re-attempt the k0 ptr mod (pixon wrappers only
;
; Revision 1.2  2006/05/03 18:45:33  antunes
; Improved geometry/view handling
;
; Revision 1.1  2006/03/08 19:33:11  antunes
; Yet another refactoring.
;
; Revision 1.7  2005/12/27 17:48:23  antunes
; Commented half of the routines using SSW style.  Also added a new
; axes test case 'arrowcube'.
; Checked in the Tomography/old/mikic_img.pro routine here because it
; is a dependency for the general image-prep routine 'get_timage.pro'
;
;-

; version 07

common debugs,phys,tdsf,tdsft,dbg

  debug=KEYWORD_SET(debug)

; put true image into timage
; stuff is parameters that are specific to the particular ch
; used
; -----------------------------------------------------------

  disableshm=1; turned off shm here because it confuses RTWL

  if (n_elements(pxwrapper) eq 0) then begin
  ; load a default geometry assuming 2 views, as rest of structures
  ; are necessary
      if (debug) then print,'preparing pixon structures.'
      prep_3drv,pxwrapper,views=2,N=N,/cor2
  end

;shmcut=128
  shmcut=1

  if (n_elements(pxwrapper) ne 0) then begin
      sr=pxwrapper.sr
      sc=pxwrapper.sc

; update any existing structures with proper N
      sr.Ncube=N
;sr.k0 = 0.5*(N-1)

      sr.ch=modelname
      sc.gname=modelname
      sc.note=modelname
  end


;N  = sr.n			; number of voxels in edge of image
  ch = modelname

  if(N le shmcut or disableshm)then begin
      if (debug) then print,"***** direct allocation"
      timage=fltarr(N,N,N)
  end else begin
      if (debug) then print,"***** using shared memory"
;  shmmap,/float,get_name=segname,dim=[N,N,N]
;  timage=shmvar(segname)
;   shmmap,'map_name',[N,N,N],/BYTE,/FLOAT

;   shmmap,'map_name',[N,N,N],/FLOAT
;   timage=shmvar('map_name')
;  filename=FILEPATH('sandy_scratch',/TMP)
;  openw,unit,filename,/GET_LUN
;  writeu, unit, FINDGEN(N*N*N)
;  close,unit
;  shmmap,/FLOAT,DIMENSION=[N,N,N],GET_NAME=segname,FILENAME=filename,OS_HANDLE='sandy_scratch'
      shmmap,/FLOAT,DIMENSION=[N,N,N],GET_NAME=segname
      timage=shmvar(segname)

  end

  nn   = (N-1)*0.5d0
  Ns   = n_elements(sr.gamma)	; number of satellites
  lla0 = sr.lla0

  if (debug) then print,'timaging case ',ch

  case strlowcase(ch) of

      'shell': begin            ; spherical shell dot in center 
          
          rmin=0.15d0
          rmax=0.16d0

          rmin=0.15
          rmax=0.20
      
          for mu=0,N-1 do for nu=0,N-1 do for la=0,N-1 do begin
              r2  = (mu-lla0[0])^2+(nu-lla0[1])^2+(la-lla0[2])^2
              if(r2 gt (rmin*n)^2 and r2 lt (rmax*n)^2)then $
                timage[mu,nu,la]=1 else timage[mu,nu,la]=0
          end

      end

      'hshell2': begin ; offset half spherical shell with arbitrary orientation
          
          if (n_elements(stuff) lt 2) then stuff=[45.0,45.0,1]; default

          tt = stuff[0]*!pi/180 ; rotation about z axis (phi)
          ff = stuff[1]*!pi/180 ; rotation towards z axis (90-theta)

          rmin = 0.15d0
          rmax = 0.25d0

          tt = double(tt)
          ff = double(ff)

          lla0=[0,nn,nn]        ; reset lla0

          for mu=0,N-1 do begin
              for nu=0,N-1 do for la=0,N-1 do begin
                  r2  = (mu-nn)^2+(nu-nn)^2+(la-nn)^2
                  if((r2 gt (rmin*n)^2) and (r2 lt (rmax*n)^2))then $
                    timage[mu,nu,la]=10^5.5 else timage[mu,nu,la]=0
                  if(mu lt nn)then timage[mu,nu,la]=0
;       print,mu,nu,la,sqrt(r2),rmin*N,rmax*N,timage[mu,nu,la]
              end
          end

          ro = [nn,nn,nn]

   ; rotate timage about z axis
          th1= tt*180/!pi
          for i=0,n-1 do begin
              timage[*,*,i]=rot(timage[*,*,i],th1,1.,n/2,n/2,/pivot,/INTERP,missing=0.)
          end
                                ; rotate rc about z axis
          t1 = th1*!pi/180.
          m=[[cos(t1),-sin(t1),0],[sin(t1),cos(t1),0],[0,0,1]]
          lla0=m#(lla0-ro)+ro

   ; rotate timage about y axis
          th2=ff*180/!pi
          for i=0,n-1 do begin
              timage[*,i,*]=rot(reform(timage[*,i,*]),th2,1.,n/2,n/2,/pivot,/INTERP,missing=0.)
          end
   ; rotate rc about y axis
          t2 = th1*!pi/180.
          m=[[cos(t2),0,-sin(t2)],[0,1,0],[sin(t2),0,cos(t2)]]
          lla0=m#(lla0-ro)+ro

; ====

;   put in image of sun and image cube center, and fill cube with
;   a small amount of electron density. Turn off physics so the
;   dots show up.
          if(0 eq 1)then begin
              phys=0
              timage[lla0[0],lla0[1],lla0[2]]=0.3*N
              timage[nn,nn,nn]= 0.3*N
              timage=timage+0.01*max(timage)
          end

          if (n_elements(pxwrapper) ne 0) then begin
              sr.lla0 = lla0    ; reset sun center
              sr.z0   = 0       ; pointing at sun
              p_center_data,sr,Ns,nn
          end


          ; add optional offset
          if (n_elements(stuff) eq 3) then begin

              Nm=N/2
              shellB=congrid(timage,Nm,Nm,Nm)
              offset=N/8
              timage=timage*0.0
              timage[N/2-offset:(N/2)+Nm-1-offset,$
                     offset:offset+Nm-1,offset:offset+Nm-1]=shellB

          end


      end

      'grid': begin
          for i=0,N-1,4 do for j=0,n-1,4 do for k=0,n-1,4 do timage[i,j,k]=1
          ic = 10
          M = N-1
          timage[0,0,0]=ic
          timage[0,0,M]=ic
          timage[0,M,0]=ic
          timage[0,M,M]=ic
          timage[M,0,0]=ic
          timage[M,0,M]=ic
          timage[M,M,0]=ic
          timage[M,M,M]=ic
      end

      'dots': begin
          timage=timage*0
          timage[nn,nn+N/8,nn]=1 
          timage[nn,nn-N/8,nn]=1
      end

      'solid': begin            ; all 1's
          timage=timage*0+1
      end

      'testcube3x3': begin ; test case, bright center pixel, + 1 pixel cube
          Nmid=N/2
          Nlow=Nmid-1
          Nhigh=Nmid+1
          timage[Nlow:Nhigh,Nlow:Nhigh,Nlow:Nhigh]=10^5.5
          timage[Nmid,Nmid,Nmid]=10^6.5
          if (debug) then print,'built testcube, center ',Nmid,' spanning ',Nlow,Nhigh
      end

      'ncube': begin ; test case, bright center pixel, + Nrange pixel cube
          if (n_elements(stuff) eq 0) then stuff=[N/8]; default
          nrange=stuff[0]
          Nmid=N/2
          Nlow=Nmid-nrange
          Nhigh=Nmid+nrange
          timage[Nlow:Nhigh,Nlow:Nhigh,Nlow:Nhigh]=10E6
          timage[Nmid,Nmid,Nmid]=1
          if (debug) then print,'built testcube, center ',Nmid,' spanning ',Nlow,Nhigh
      end

      'testcube1x1': begin	; test case, bright off-center pixel
                                ; default is to put it in middle
          if (n_elements(stuff) lt 3) then begin
              Nx=N/2
              Ny=N/2
              Nz=N/2
          end else begin
              Nx=stuff[0]
              Ny=stuff[1]
              Nz=stuff[2]
          end
          timage[Nx,Ny,Nz]=1
          if (debug) then print,'built testcube 1x1, center ',Nx,Ny,Nz
      end

      'testcube2x2': begin ; test case, bright 2x2 centered pixel cube
          Nmid=N/2
          Nlow=Nmid-1
          timage[Nlow:Nmid,Nlow:Nmid,Nlow:Nmid]=1E6
          if (debug) then print,'built 2x2 centered testcube'
      end

      'superv': begin        ; supervoxels - cubes of constant density
          nsv=n_elements(stuff[0,*])

          for i=0,nsv-1 do begin

;    center of supervoxel in image coordinates

              mu = N/2
              nu = N/2
              la = N/2

;    position of sun in image coordinates

              if (n_elements(pxwrapper) ne 0) then begin
                  sr.lla0[0]=-stuff[0,i]/(sr.dvox)+mu
                  sr.lla0[1]=-stuff[1,i]/(sr.dvox)+nu
                  sr.lla0[2]=-stuff[2,i]/(sr.dvox)+la
              end

              Nv=round(stuff[3,i])
              np=fix(0.5*Nv)
              nm=Nv-np-1
              nfill=float(stuff[4,i]) ; electron density

              timage[mu-nm:mu+np,nu-nm:nu+np,la-nm:la+np]=nfill

          end

          if (n_elements(pxwrapper) ne 0) then begin
              p_center_data,sr  ; alter sr.k0 to put image cube center
          end

      end

      'plume2': begin           ; canonical 2 plumes

;   sr.lla0  = [0,0,-2*N]	; sun center (proposal)
          lla0=[nn,nn,-nn/3]
          if (n_elements(pxwrapper) ne 0) then begin
              sr.lla0  = [nn,nn,-nn/3] ; sun center (paris)
          end

          x=31.49*n/64
          y=53.13*n/64
          radius = 6.15*n/64

;   nel=5.0	; proposal
          nel=1.0               ; paris

          sun_plume_00,timage,n,x,y,radius,nel,lla0
          
          x=54.87*n/64
          y=21.11*n/64
          radius = 4.22*n/64

;   nel=6.5	; proposal
          nel=1.0               ; paris
   
          sun_plume_00,timage,n,x,y,radius,nel,lla0
   
          p_center_data,sr,Ns,nn
   
;  zero out edges of image cube
          if(1 eq 1)then begin
              q=n/16
              timage[0:q,*,*]=0
              timage[*,0:q,*]=0
              timage[*,*,0:q]=0
              timage[N-1-q:N-1,*,*]=0
              timage[*,N-1-q:N-1,*]=0
              timage[*,*,N-1-q:N-1]=0
          end
          
          timage=timage
;          sr.z0=0
          
          if(n_elements(stuff1) ne 0)then begin
              restore,stuff1
              timage=timage
          end
          
      end
      
      'shell3': begin           ;  spherical shell
          
          if(n_elements(stuff) eq 0)then rmin = 0.15d0 else rmin=stuff[2]
          if(n_elements(stuff) eq 0)then rmax = 0.25d0 else rmax=stuff[3]
          
          lla0=[0,nn,nn,1]      ; reset lla0
          
          for mu=0,N-1 do begin
              for nu=0,N-1 do for la=0,N-1 do begin
                  r2  = (mu-nn)^2+(nu-nn)^2+(la-nn)^2
                  if((r2 gt (rmin*n)^2) and (r2 lt (rmax*n)^2))then $
                    timage[mu,nu,la]=1 else timage[mu,nu,la]=0
              end
          end
          
          if (n_elements(pxwrapper) ne 0) then begin

              sr.lla0=[lla0[0],lla0[1],lla0[2]]
              sr.z0=0           ; pointing at sun
              
              p_center_data,sr,Ns,nn
          end
          
      end
      
      'hshell3': begin ; half spherical shell with arbitrary orientation
          
          if (n_elements(stuff) eq 0) then stuff=[0,0]

          tt = double(stuff[0]*!pi/180) ; rotation about y axis (theta)
          ff = double(stuff[1]*!pi/180) ; rotation about z axis (-phi)
          
          if(n_elements(stuff) ne 4)then rmin = 0.15d0 else rmin=stuff[2]
          if(n_elements(stuff) ne 4)then rmax = 0.25d0 else rmax=stuff[3]
          
          for mu=0,N-1 do begin
              for nu=0,N-1 do for la=0,N-1 do begin
                  r2  = (mu-nn)^2+(nu-nn)^2+(la-nn)^2
                  if((r2 gt (rmin*n)^2) and (r2 lt (rmax*n)^2))then $
                    timage[mu,nu,la]=1 else timage[mu,nu,la]=0
                  if(mu lt nn)then timage[mu,nu,la]=0
              end
          end
          
; ----- this is amos' Ne but without using N/2 as center
          
          if(n_elements(stuff1) ne 0)then begin 
              if(stuff1[0] eq 'amos')then begin
                  if (debug) then print,'Approximating Amos Ne'
                  for mu=0,N-1 do begin
                      for nu=0,N-1 do for la=0,N-1 do begin
                          r  = sqrt((mu-nn)^2+(nu-nn)^2+(la-nn)^2)
                          if((r ge rmin*n) and (r le rmax*n))then $
                            timage[mu,nu,la]=1 else timage[mu,nu,la]=0
                          if(nu le nn)then timage[mu,nu,la]=0
                      end
                  end
              end else if(stuff1[0] eq '')then begin
                  stop,'ERROR hshell3 get_timage_06.pro'
              end else begin 
                  if (debug) then print,'RESTORING ',stuff1[0]
                  restore,stuff1[0]
              end
          end
          
;  put a blob in if stuff[4]=1
          
          if(n_elements(stuff) ge 5)then begin
              if(stuff[4] eq 1)then begin
                  q0=[0.15625,0.3125,0] ; location of center of blob (width of cube=1)
                  qr=.05        ; radius of blob
                  for mu=0,N-1 do for nu=0,N-1 do for la=0,N-1 do begin
                      hh=N/2
                      xx=(double(mu)-hh)/N
                      yy=(double(nu)-hh)/N
                      zz=(double(la)-hh)/N
                      xx=q0[0]-xx
                      yy=q0[1]-yy
                      zz=q0[2]-zz
                      
;         if(xx^2+yy^2+zz^2 lt qr^2)then begin
;           timage[mu,nu,la]=2
;           print,mu,nu,la,xx,yy,zz
;         end
                      
                      timage[mu,nu,la]=timage[mu,nu,la]+2.5*exp(-(xx*xx+yy*yy+zz*zz)/2/qr^2)
                  end
              end
;do3,timage,mag=256/N
;stop,'blob inserted into timage'
          end
          
;
;tval=50
;nna=28
;nnb=35
;timage[nna,nna,nna]=tval
;timage[nna,nna,nnb]=tval
;timage[nna,nnb,nna]=tval
;timage[nna,nnb,nnb]=tval
;timage[nnb,nna,nna]=tval
;timage[nnb,nna,nnb]=tval
;timage[nnb,nnb,nna]=tval
;timage[nnb,nnb,nnb]=tval
;nna=20
;nnb=43
;timage[nna,nna,nna]=tval
;timage[nna,nna,nnb]=tval
;timage[nna,nnb,nna]=tval
;timage[nna,nnb,nnb]=tval
;timage[nnb,nna,nna]=tval
;timage[nnb,nna,nnb]=tval
;timage[nnb,nnb,nna]=tval
;timage[nnb,nnb,nnb]=tval
;nna=12
;nnb=51
;timage[nna,nna,nna]=tval
;timage[nna,nna,nnb]=tval
;timage[nna,nnb,nna]=tval
;timage[nna,nnb,nnb]=tval
;timage[nnb,nna,nna]=tval
;timage[nnb,nna,nnb]=tval
;timage[nnb,nnb,nna]=tval
;timage[nnb,nnb,nnb]=tval
;nna=0
;nnb=63
;timage[nna,nna,nna]=tval
;timage[nna,nna,nnb]=tval
;timage[nna,nnb,nna]=tval
;timage[nna,nnb,nnb]=tval
;timage[nnb,nna,nna]=tval
;timage[nnb,nna,nnb]=tval
;timage[nnb,nnb,nna]=tval
;timage[nnb,nnb,nnb]=tval
          
; form a box
;tval=50
;nna=N/4
;nnb=3*N/4-1
;timage[nna,nna,nna:nnb]=tval
;timage[nna,nnb,nna:nnb]=tval
;timage[nnb,nna,nna:nnb]=tval
;timage[nnb,nnb,nna:nnb]=tval
;timage[nna,nna:nnb,nna]=tval
;timage[nna,nna:nnb,nnb]=tval
;timage[nnb,nna:nnb,nna]=tval
;timage[nnb,nna:nnb,nnb]=tval
;timage[nna:nnb,nna,nna]=tval
;timage[nna:nnb,nna,nnb]=tval
;timage[nna:nnb,nnb,nna]=tval
;timage[nna:nnb,nnb,nnb]=tval
          
; dot at center and la=32
;tval=50
;nna=N/2
;nnb=N/2-1
;timage[nna,nna,nna]=tval
;timage[nna,nna,nnb]=tval
;timage[nna,nnb,nna]=tval
;timage[nna,nnb,nnb]=tval
;timage[nnb,nna,nna]=tval
;timage[nnb,nna,nnb]=tval
;timage[nnb,nnb,nna]=tval
;timage[nnb,nnb,nnb]=tval
;
;timage[nna,nna,32]=tval
;timage[nna,nnb,32]=tval
;timage[nnb,nna,32]=tval
;timage[nnb,nnb,32]=tval
          
; -----
          
          ro = [nn,nn,nn]
          
          if (n_elements(stuff) eq 0) then stuff=[0,0]

          rot3d,timage,'y',stuff[0],RR,/init ; rotate timage about y axis
          rot3d,timage,'z',stuff[1],RR ; rotate timage about z axis

          lla0=[sr.lla0,1]
          lla0=RR#lla0          ; calculate new sun position
          
; ====
          
;   put in image of sun and image cube center, and fill cube with
;   a small amount of electron density. Turn off physics so the
;   dots show up.
          if(0 eq 1)then begin
              phys=0
              timage[lla0[0],lla0[1],lla0[2]]=0.3*N
              timage[nn,nn,nn]= 0.3*N
              timage=timage+0.01*max(timage)
          end
          
          if (n_elements(pxwrapper) ne 0) then begin
              sr.lla0=[lla0[0],lla0[1],lla0[2]]
          
;   sr.z0=0                              ; pointing at sun
          
; see p_center_data.pro
          
              if (debug) then print,''
              if (debug) then print,'---'
              if (debug) then print,'before center_data sr.k0=',sr.k0,' sr.lla0=',sr.lla0
          
              p_center_data,sr,Ns,nn
          end

;   if(n_elements(stuff1) ne 0)then begin
;     restore,stuff1
;     timage=timage
;     stop,'???' 
;   end
          
;do3,timage,mag=256/N
;stop,'end of timage'
          
      end
      
      'saito':  begin ; saito's Ne (Saito, K. 1972, Ann. Tokyo Astron. Obs. XII, 53,120
                                ; Taken from Allens Astrophysical Quantities, 4th Ed.
; A Non-Spherical Axisymmetric Model of The Solar K Corona of the Minimum Type,
; Ann. Tokyo Astron. Obs., vol. XIII ... 
; for points not in the equator or on pole, an ellipse connects equal density points
; on equator and pole
          
          rr_e= [1.003,1.005,1.01,1.03,1.06,1.10,1.2,1.4,1.6,2.0,2.5,3.0,4.0,5.0,10.0]
          ne_e=[9,8.8,8.7,8.6,8.4,8.25,7.8,7.35,7.05,6.5,5.95,5.5,5.05,4.75,4.05]
          
          rr_p= [1.003,1.005,1.01,1.03,1.06,1.10,1.2,1.4 ,1.6, 1.8, 2.0,2.2, 2.5,3.0,4.0,5.0]
          ne_p=[ 9,     8.6,  8.3,8.12,7.98,7.81,7.3,6.64,6.13,5.78,5.5,5.25,5.0,4.7,4.3,4.0]
;          rr_p=[1.01,1.10,1.2,1.4,1.6,2.0,2.5,3.0,4.0,5.0]
;          ne_p=[8.0,7.5,7.1,6.25,5.95,5.0,4.75,4.5,4.2,4.0]   

          if (n_elements(stuff) eq 0) then const=1.0 else const=stuff[0]
          
          for mu=0,N-1 do for nu=0,N-1 do for la=0,N-1 do begin
;              rho2  = (mu-lla0[0])^2+(nu-lla0[1])^2
;              z2    = (la-lla0[2])^2
;              timage[mu,nu,la]=
              r=sqrt ((mu-lla0[0])^2+(nu-lla0[1])^2+(la-lla0[2])^2)

              ang=!radeg * asin( abs(la-lla0[2]) / r ); angle above equator

              r=r*sr.L0tau[0] 

              espmcoeff=interpol(ne_e,rr_e,r)
              pspmcoeff=interpol(ne_p,rr_p,r)

; ; note original formalism, ratioing the emission roughly
;              espm=10.0^espmcoeff
;              pspm=10.0^pspmcoeff
;              ; interpolate polar and equatorial ratios
;              tspm=abs(((90.0-ang)/90.0))*espm + abs((ang/90.0))*pspm

;note alternative formalism, ratioing the coefficients then 10^ them
;both methods yield similar figures, but this one is football-shaped
;while the other formulation is peanut-shaped.
             tspm=abs(((90.0-ang)/90.0))*espmcoeff + abs((ang/90.0))*pspmcoeff
             tspm=10^tspm

;              if (mu eq 55 and nu eq 64 and la eq 64) then $
;                print,mu,nu,la,lla0,sr.l0tau[0],r,espm,pspm,const,tspm

              if (r le 1.0) then tspm=0.0

              timage[mu,nu,la]=tspm
              timage[mu,nu,la]=timage[mu,nu,la]/const
;              z2    = (la-lla0[2])^2
          end
      end

; ----------------------------------------------

      'saito2':  begin ; saito's Ne
          
          for mu=0,N-1 do for nu=0,N-1 do for la=0,N-1 do begin
              r=sqrt ((mu-lla0[0])^2+(nu-lla0[1])^2+(la-lla0[2])^2)

              ang=!radeg * asin( abs(la-lla0[2]) / r ); angle above equator

              r=r*sr.L0tau[0] 
              espm=spm_ne(r,0)
              pspm=spm_ne(r,1)

              ; interpolate polar and equatorial ratios
              tspm=abs(((90.0-ang)/90.0))*espm + abs((ang/90.0))*pspm

              if (r le 1.0) then tspm=0.0

              timage[mu,nu,la]=tspm
              timage[mu,nu,la]=timage[mu,nu,la]

          end
      end
      
; ----------------------------------------------
      
      'chen': begin             ; use Jim Chen's flux rope density
          
;  restore,'/net/corona/data/cplex3/reiser/Tomography/chen_timage.dat'
          restore,'/net/corona/data/cplex3/reiser/Tomography/chen_timage_25.dat'
          
          note = note+' chen25 image'
          
          image=image*10        ; just to get stuff on order of 1
          dd   = 20.0/127.0     ; voxel edge in solar radii
          lla0 = [10,1,10]/dd   ; sun center in voxel edge units
          d    = dd/sr.L0tau[0]	; voxel edge/L0tau  
          
; ----- zero blanket for fourier transforms
          
          read,'Enter imbed ratio e.g. 1 (=none), 1.5, 2, etc. - ',f
          
          imbed,image,N,f,lla0,d,tau,L0tau
          note=note+' (imbed)'
          
          timage = image
          if (n_elements(pxwrapper) ne 0) then begin
              sr.lla0  = lla0
;  sr.d     = d
          
              if (debug) then print,''
              if (debug) then print,'NEW CHEN - before center_data'
              if (debug) then print,''
              if (debug) then help,image
              if (debug) then print,'lla0  = ',sr.lla0
              if (debug) then print,'d     = ',sr.dvox
              if (debug) then print,'tau   = ',sr.tau[0]
              if (debug) then print,'L0    = ',sr.L0
              if (debug) then print,'L0tau = ',sr.L0tau[0]
          
              p_center_data,s   ; alter sr.k0 to put image cube center
          end
      end
      
; ----------------------------------------------
      
      'liewer': begin           ; Paulette Liewer's CME
          
; restore image, ds, and r0. ds is length of edge of voxel in solar radii,
; r0 is sun center in image coordinates. Size is 128^3
          
          restore,'/net/corona/data/cplex3/reiser/Tomography/3d_cme_den_2hr_128.datx'
          d   = ds/sr.L0tau[0]	; voxel edge/L0tau 
          
          me = 9.1093897e-28    ; mass of electron in grams 
          image=image/(me*sr.n0) ; convert from gm/cm^3 to normalized electron density
          
          q=where(image gt 1000) ; zero out sun
          image[q]=0
          
; imbed the image cube inside a larger one (Np^3)
          
          Np=0
          read,'Enter imbed size e.g. 128(=none),160,256,384 - ',Np
          if(Np ne 128)then begin
              df=(Np-128)/2
              ximage=fltarr(Np,Np,Np)
              ximage[df:df+127,df:df+127,df:df+127]=image
              r0=r0+(Np-128)/2.
              sr.tau[0]   = sr.tau[0]*N/Np
              sr.L0tau[0] = sr.L0tau[0]*N/Np
              if (debug) then print,'** edges zeroed Np = ',Np
          end else begin
              ximage=image
              if (debug) then print,'** edges NOT zeroed **'
          end
          
          image=rebin(ximage,N,N,N,/sample)
          
          timage = image
          sr.lla0  = r0         ; sun center in image coordinates
;  sr.d     = 			; voxel edge/L0tau
          p_center_data,s      ; alter sr.k0 to put image cube center
      end
      
; ----------------------------------------------
      
      'mikic': begin            ; Zoran Mikic's CME
          
; img is the 128^3 image, r is radius
          
          image = mikic_img(fnum=stuff,rr=r)
          r0=[63.5,63.5,63.5]
          
          Np=0
          read,'Enter imbed size e.g. 128(=none),160,256,384 - ',Np
          if(Np ne 128)then begin
              df=(Np-128)/2
              ximage=fltarr(Np,Np,Np)
              ximage[df:df+127,df:df+127,df:df+127]=image
              r0=r0+(Np-128)/2.
              sr.tau[0]   = sr.tau[0]*N/Np
              sr.L0tau[0] = sr.L0tau[0]*N/Np
              if (debug) then print,'** edges zeroed Np = ',Np
          end else begin
              ximage=image
              if (debug) then print,'** edges NOT zeroed **'
          end
          
          image=rebin(ximage,N,N,N,/sample)
          fff = float(N)/float(Np)
          r0  = (r0+0.5)*fff-0.5
          
          timage = image
          sr.lla0  = r0         ; sun center in image coordinates
          p_center_data,s      ; alter sr.k0 to put image cube center
      end
      
; ----------------------------------------------
      
      'arnaud': begin           ; Arnaud Thernisien sheet
          
          model = stuff
          
          
          if (debug) then print,'debug, doing arnaud sheet without proper memory handling'
          
          timage=fltarr(N,N,N)
          xx=timage
          yy=timage
          zz=timage
          
          for i=0,N-1 do begin
              xx[i,*,*]=i-sr.lla0[0]
              yy[*,i,*]=i-sr.lla0[1]
              zz[*,*,i]=i-sr.lla0[2]
          end
          
          fac = sr.dvox  ;sr.d*sr.L0tau[0] ; solar radii per voxel edge
          rr=sqrt(xx*xx+yy*yy+zz*zz)
          xx=xx*fac
          yy=yy*fac
          zz=zz*fac
          rrr=sqrt(xx*xx+yy*yy+zz*zz)
          
          th = 128.39*!pi/180.
          sth = sin(th)
          cth = cos(th)
          
          case model of
              1: begin
                  slabmodel='$PIXON/results/arnaud/slabmodel.so'
                  stop,'NOT USED'
              end
              2: begin
                  aa=myreadfits('slabprofile2.fts',h)
                  slabmodel='$PIXON/results/arnaud/slabmodel2.so'
              end
              3: begin          ; IDL version
                  aa=myreadfits('slabprofile2.fts',h)
              end
              4: begin
                  aa=myreadfits('slabprofile2.fts',h)
                  slabmodel='$PIXON/results/arnaud/slabmodel2.so'
              end
              
              else: stop,'ERROR get_timage'
          end
          
          hr=xx*0
          htheta=hr
          hphi=hr
          hq=hr
          hpx=hr
          hpy=hr
          hpz=hr
          
          for i=0L,n_elements(xx)-1 do begin
              xxx= xx[i]
              yyy= yy[i]*cth+zz[i]*sth
              zzz=-yy[i]*sth+zz[i]*cth
              px = float(xxx)
              py = float(yyy)
              pz = float(zzz)
              nel= float(0.0)
              
              case model of     ; red hat cc compiler
                  1: stop,'NOT USED' ;ret=call_external(slabmodel,'slabmodel',px,py,pz,nel)
                  2: begin
                      ret=call_external(slabmodel,'slabmodel2',px,py,pz,nel,aa)
                  end
                  3: begin;;
                      q=slabmodel2(px,py,pz,nel,aa,r,theta,phi,ii)
                      hq[i]=q
                      hr[i]=r
                      htheta[i]=theta
                      hphi[i]=phi
                      hpx[i]=px
                      hpy[i]=py
                      hpz[i]=pz
                  end
                  4: begin      ; zero out nel for r<n/2
                      if(rr[i] lt n/2)then begin
                          stop,'ready to call'
                          ret=call_external(slabmodel,'slabmodel2',px,py,pz,nel,aa)
                      end else begin 
                          nel=0
                      end
                  end
                  
                  else: stop,'ERROR get_timage'
              end
              
              timage[i]=nel
              
          end
          
      end
      
      'helix': begin
          
; a helix along the y axis of radius r. dy is the spatial period of
; the helix. Any slice through an x-z plane gives a 2-d gaussian of
; width t. The amplitude is amin modified by knots. Each knot 
; modifies the amplitude up and has an extension along the y axis of
; w
          
          r  = stuff[0]		; radius of helix
          dy = stuff[1] ; interval along y axis for one cycle of helix
          t  = stuff[2]		; thickness of helix
          amin = stuff[3]       ; minimum density
          amax = stuff[4]       ; maximum density
          nk   = stuff[5]       ; number of knots 
          w    = stuff[6]       ; width of knots (alon y axis)
          seed = stuff[7]       ; random number seed
          
          if (debug) then print,'debug: doing helix without proper memory handling'
          shmmap,DIMENSION=[N,N,N],get_name=segname,/FLOAT
          timage=shmvar(segname)
;  timage = fltarr(N,N,N)
          f   = indgen(N/2)     ; parametric in f
          ang = 2*!pi*f/dy
          xh = r*cos(ang-!pi/2) ; x position of center of helix
          zh = r*sin(ang-!pi/2) ; z position of center of helix
          yk = randomu(seed,nk)*0.5*N ; y-position of knots
          ak = randomu(seed,nk)*(amax-amin)+amin ; amplitude of knots
          
          x=fltarr(N,N)
          z=x
          for i=0,N-1 do begin
              x[i,*]=i-N/2
              z[*,i]=i-N/2
          end 
          
          for i=0,N/2-1 do begin ; step along y
              
              y=N/2+i
              
              a = amin 
              for m=0,nk-1 do begin
                  a = a + ak[m]*gaus(i,yk[m],w) 
              end
              
              timage[*,y,*]=a*gaus(x,xh[i],t) * gaus(z,zh[i],t)
              
          end
          
      end
      
      'fluxrope': begin         ; Russ's flux rope model
                                ; note this terrible error, preallocating a possibly wrong size
          shmmap,DIMENSION=[256,256,256],get_name=segname,/INT
          timg=shmvar(segname)
          if (file_exist('ftimage_256.dat')) then begin
              restore,'ftimage_256.dat'
          end else begin
              falt='/net/corona/cplex3/reiser/a/ftimage_256.dat'
              if (file_exist(falt)) then restore,falt else $
                  timage=make_array(N,N,N,/float,/index); lame default return
          end
;          print,'cheating here, ',SIZE(timg)
          timage=rebin(timg,N,N,N)*256./float(N)
      end
      
      'null': begin
; keep it blank for later use
      end
      
      else: stop,'bad ch'
  end
  
  if (n_elements(pxwrapper) ne 0) then begin
      pxwrapper.sr=sr
      pxwrapper.sc=sc
  end
  
  return,timage
  
END
