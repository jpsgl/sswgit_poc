;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : p_qplots
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;
; Plots the fitting history as improvement in 'Q' versus time.
;               
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
PRO p_qplots,oPixon,saved,id=id,noscreen=noscreen,tag=tag,appendps=appendps,$
             mxit=mxit

  noscreen=KEYWORD_SET(noscreen)
  if (n_elements(tag) eq 0) then tag='qplot'
  if (n_elements(id) eq 0) then id=0
  appendps=KEYWORD_SET(appendps)

  if (noscreen) then istart=1 else istart=0

  history=oPixon->get(/history)
  if (n_elements(mxit) eq 0) then mxit=n_elements(history[*,0])

  for ichoice=istart,1 do begin

      if (ichoice eq 1) then begin
          if (appendps) then begin
              ERASE; starts a new postscript page on a presumed existing device
              fname=''
          end else begin
              fname=tag+'_'+strtrim(string(id),2)+'_stats.ps'
              current_device=!d.name
              set_plot,'PS'
              device,filename=fname,/landscape
          end
      end else begin
          ;onscreen
          window,/free,xsize=400,ysize=400,retain=2
      end

      minq=0.1; min(history[*,0])*0.9

      maxq=1000.0; minq*10.0

      title='Q History'

      !P.MULTI=[0,0,2]
      plot,(abs(history[*,0])),title=title,font=7,xstyle=1,/ylog,$
        xtitle='Iteration',ytitle='Q (log scale)',yrange=[minq,maxq],$
        xrange=[0,mxit];,ystyle=1
      plot,(abs(history[*,1])),title='Change in LLF per step',/ylog,$
        ynozero=1,font=7,xstyle=1,$
        xtitle='Iteration',ytitle='delta LLF (log scale)',yrange=[1,1e6],$
        xrange=[0,mxit]
      !P.MULTI=0

      if (ichoice eq 1 and appendps eq 0) then begin
          device,/CLOSE_FILE
          set_plot,current_device
      end

  end

  if (n_elements(saved) eq 0) then saved=[fname] else saved=[saved,fname]

END
