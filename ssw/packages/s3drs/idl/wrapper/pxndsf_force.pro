;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : PXNdata::DSF
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;
; This is a modification of the Pixon-standard DSF, set to call our
; set of whitelight routines.
;               
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : common debugs,phys,tdsf,tdsft,dbg
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            

FUNCTION PXNdata::DSF            $
                , image          $
                ,_REF_EXTRA = e    ; Extra variables (by reference)

common debugs,phys,tdsf,tdsft,dbg

; Sandy hates using common blocks, but put this one in
; so that we avoid re-re-re-re-allocating storage for the
; often-reassigned often-mundged copy of the base image
common pixonstorage, imagetemp

; 'fast' is now extracted from the self structure
;fast=1; 1 = fast but sloppy, 0 = 4x slower but more accurate

;help,(*self.user)
;print,(*self.user).fprf

; debug flags
dbgid=0
dbgabc=0
dbgla=0
id       = self.id
fprf     = (*self.user).fprf
roco     = (*self.user).roco		; first execute rotate(image,1)
; note legacy use of save files prior to adding the /fast option
if (tag_exist((*self.user),'fast') eq 0) then fast=1 else $
  fast   = (*self.user).fast  ; to use fast or slowerbutaccurate rot
polar    = (*self.user).polar           ; the polarization (t,r,p, or b)
c2z      = (*self.user).c2z             ; cos(zeta) squared (for polar=p)
s2z      = (*self.user).s2z             ; sin(zeta) squared (for polar=p)
lla0     = (*self.user).lla0            ; sun center
rho2     = (*self.user).rho2            ; rho^2 array
rhos2    = (*self.user).rhos2	        ; the rho_s^2 array
psLs     = (*self.user).psLs            ; the ps*Ls array
psLs2    = (*self.user).psLs2           ; the (ps*Ls)^2 array
i        = (*self.user).i		
jc       = (*self.user).jc
K2       = (*self.user).K2
d2       = (*self.user).d2
fa       = (*self.user).fa
fb       = (*self.user).fb
fc       = (*self.user).fc
rmin2    = (*self.user).rmin2
tau      = (*self.user).tau
tau2     = (*self.user).tau2
interp   = (*self.user).interp
machine  = (*self.user).machine
g        = (*self.user).g               ; view angle gamma
theta    = (*self.user).theta ; Sandy's implementation of spherical coords
gamma=g; easier to grep on

 roco=-1
; print,'roco eq ',roco
;    print,'set 1: ',fprf,roco,polar,lla0,d2,rmin2
;    print,'set 2: ',tau,tau2,interp,machine,g,theta
;    help,tau,tau2
; print,'arrays: '
; print,minmax(c2z),minmax(s2z),minmax(rho2),minmax(rhos2),minmax(psls),minmax(psls2),minmax(i),minmax(jc),minmax(k2),minmax(fa),minmax(fb),minmax(fc)


t0=systime(-1)
psp=2

N  = n_elements(image[*,0,0])

; note that 'imagetemp' is a persistent common so this only
; assigns memory once, and after that is just a quick copy.
; We do this since we typically do a lot of rotations etc that we
; later have to reverse, instead we decided to just use a copy of
; the image for speed.
imagetemp=image

;if(debug_04)then stop

; rotate (for close to z-axis stuff)

if(roco eq 1)then begin
  for nu=0,N-1 do imagetemp[*,nu,*]=$
    rotate(reform(temporary(imagetemp[*,nu,*])),1) 
  lla0=[0.5*(N-1)-lla0[2],lla0[1],lla0[0]] 
end

if (roco eq -1 and (theta ne 0 or gamma ne 0)) then begin
    ; note gamma should be CCW

  if (fast) then begin
;;      if (gamma ne 0) then cuberot3d,imagetemp,[0.0,0.0,0.0-gamma],lla0
;;      if (theta ne 0) then cuberot3d,imagetemp,[0.0,theta,0.0],lla0
;;      cuberot3d,imagetemp,[0.0,theta,0.0-gamma],lla0,/reverse
      cuberot3d,imagetemp,[0.0,theta,0.0-gamma],/reverse
  end else begin
;;      imagetemp=turn_3d(image,0.0,theta,gamma,/reverse,/resize,$
;;                       xyzcenter=lla0,/pivot,/interp)
      imagetemp=turn_3d(image,0.0,theta,gamma,/reverse,/resize,$
                       /pivot,/interp)
  end
; note we require the image center be the sun center be the cube
; center, for now
;    lla0=[]
end


; ----- 

dat=dblarr(N,N)

case strlowcase(polar) of

    'n': begin                  ; no Thompson scattering-- test case only

        for la=0,N-1 do begin
            z2  = d2*(la-lla0[2])^2
            r2  = rho2+z2
            rs2 = rhos2+tau2*z2
            cx2 = (r2*tau2-2*tau*psLs+psLs2/r2)/rs2       
            r2  = round((r2-rmin2)/d2)
            j   = jc+la/K2
            fizzix=((fa[r2]+cx2*fb[r2]+fc[r2])/rs2)*phys + (1-phys)
            fizzix=fizzix*0.0+1.0; throw it away and just go flat

            ; native IDL version of '2', no interpolation
            dsumidl,0,N,i,j,la,imagetemp,fizzix,dat

        end

    end

;=================== PLANE POLARIZATION =====================

    'p': begin                  ; plane

        tdat=dblarr(N,N)
        rdat=dblarr(N,N)

        for la=0,N-1 do begin

            z2  = d2*(la-lla0[2])^2
            r2  = rho2+z2
            rs2 = rhos2+tau2*z2
            cx2 = (r2*tau2-2*tau*psLs+psLs2/r2)/rs2       
            r2  = round((r2-rmin2)/d2)
            j   = jc+la/K2

            if(interp ne 3 and interp ne 1)then stop,'dsf error'

            fizzixt = (fa[r2]/rs2)*phys + (1-phys) ;tangential
            if (interp eq 3) then begin
                ; C program - with interpolation v=1
                disum1_inc,imagetemp[*,*,la]*fizzixt,N,tdat,i,j,machine
            end else begin
                ; native IDL version of '3', interpolation v=1
                dsumidl,1,N,i,j,la,imagetemp,fizzixt,tdat
            end
            ; disum1_inc,imagetemp[*,*,la]*fizzixt,N,tdat,i,j,machine

            fizzixr=((cx2*fb[r2]+fc[r2])/rs2)*phys + (1-phys) ;radial
            if (interp eq 3) then begin
                ; C program - with interpolation v=1
                disum1_inc,imagetemp[*,*,la]*fizzixr,N,rdat,i,j,machine
            end else begin
                ; native IDL version of '3', interpolation v=1
                dsumidl,1,N,i,j,la,imagetemp,fizzixr,rdat
            end
            ; disum1_inc,imagetemp[*,*,la]*fizzixr,N,rdat,i,j,machine

;---------------------------
; debug
            if((dbg eq 1) and (la eq dbgla))then begin
                q=where(imagetemp ne 0)
                if((id eq dbgid) and (q[0] eq dbgabc))then begin
                    fmat="('dsf: code = x',i3,' ',i3)"
                    print,id,q[0],format=fmat
                    stop,' dsf'
                end
            end

;---------------------------

        end

        dat = tdat*c2z + rdat*s2z

    end
    
;================== TANGENTIAL POLARIZATION ==============

    't': begin                  ; tangential polarization

        for la = 0,N-1 do begin
            z2  = d2*(la-lla0[2])^2
            r2 = round((rho2+z2-rmin2)/d2)
            rs2 = rhos2+tau2*z2
            j   = jc+la/K2
            fizzix = (fa[r2]/rs2)*phys + (1-phys)

;if(la eq 32)then stop,'stop la=32 in dsf'

;---------------------------

            case interp of

                0: begin        ; native IDL version of '2', no interpolation
                    dsumidl,0,N,i,j,la,imagetemp,fizzix,dat
                end

                1: begin        ; native IDL version of '3', interpolation v=1
                    dsumidl,1,N,i,j,la,imagetemp,fizzix,dat
                end

                2: begin        ; C program - no interpolation
                    @dsum_inc
                end

                3: begin        ; C program - with interpolation v=1
                    disum1_inc,imagetemp[*,*,la]*fizzix,N,dat,i,j,machine
                end

                4: begin   ; C program - with interpolation v=variable
                    @disum2_inc
                end

                5: begin        ; C program - trapezoid spreading
                    disum3_inc,imagetemp[*,*,la]*fizzix,N,dat,i,j,machine,g
                end

                else: stop,'gack'
            end

; debug
;if((dbg eq 1) and (la eq dbgla))then begin
;  q=where(image ne 0)
;  if((id eq dbgid) and (q[0] eq dbgabc))then begin
;    fmat="('dsf: code = x',i3,' ',i3)"
;    print,id,q[0],format=fmat
;    stop,' dsf'
;  end
;end

;---------------------------

        end
    end

;================== RADIAL POLARIZATION ==================

    'r': begin                  ; radial polarization

        for la=0,N-1 do begin
            z2  = d2*(la-lla0[2])^2
            r2  = rho2+z2       ; r2 is distance from sun
            rs2 = rhos2+tau2*z2
            cx2 = (r2*tau2-2*tau*psLs+psLs2/r2)/rs2       
            r2  = round((r2-rmin2)/d2)
            j   = jc+la/K2
            fizzix=((cx2*fb[r2]+fc[r2])/rs2)*phys + (1-phys)

;---------------------------

            case interp of

                0: begin        ; native IDL version of '2', no interpolation
                    dsumidl,0,N,i,j,la,imagetemp,fizzix,dat
                end

                1: begin        ; native IDL version of '3', interpolation v=1
                    dsumidl,1,N,i,j,la,imagetemp,fizzix,dat
                end

                2: begin        ; C program - no interpolation
                    @dsum_inc
                end

                3: begin        ; C program - with interpolation v=1
                    disum1_inc,imagetemp[*,*,la]*fizzix,N,dat,i,j,machine
                end

                4: begin   ; C program - with interpolation v=variable
                    @disum2_inc
                end

                5: begin        ; C program - trapezoid spreading
                    disum3_inc,imagetemp[*,*,la]*fizzix,N,dat,i,j,machine,g
                end
                
                else: stop,'gack'
            end

; debug
;if((dbg eq 1) and (la eq dbgla))then begin
;  q=where(image ne 0)
;  if((id eq dbgid) and (q[0] eq dbgabc))then begin
;    fmat="('dsf: code = x',i3,' ',i3)"
;    print,id,q[0],format=fmat
;    stop,' dsf'
;  end
;end

;---------------------------

        end
    end

;=================== NO POLARIZATION =====================

    'b': begin                  ; brightness

        for la=0,N-1 do begin
            z2  = d2*(la-lla0[2])^2
            r2  = rho2+z2
            rs2 = rhos2+tau2*z2
            cx2 = (r2*tau2-2*tau*psLs+psLs2/r2)/rs2       
            r2  = round((r2-rmin2)/d2)
            j   = jc+la/K2
            fizzix=((fa[r2]+cx2*fb[r2]+fc[r2])/rs2)*phys + (1-phys)

;---------------------------

            case interp of

                0: begin        ; native IDL version of '2', no interpolation
                    dsumidl,0,N,i,j,la,imagetemp,fizzix,dat
                end

                1: begin        ; native IDL version of '3', interpolation v=1
                    dsumidl,1,N,i,j,la,imagetemp,fizzix,dat
                end

                2: begin        ; C program - no interpolation
                    @dsum_inc
                end

                3: begin        ; C program - with interpolation v=1
                    disum1_inc,imagetemp[*,*,la]*fizzix,N,dat,i,j,machine
                end

                4: begin   ; C program - with interpolation v=variable
                    @disum2_inc
                end

                5: begin        ; C program - trapezoid spreading
                    disum3_inc,imagetemp[*,*,la]*fizzix,N,dat,i,j,machine,g
                end

                else: stop,'gack'
            end

;;stop,'IN pxndata__dsf_06.pro'

; debug
;if((dbg eq 1) and (la eq dbgla))then begin
;  q=where(image ne 0)
;  if((id eq dbgid) and (q[0] eq dbgabc))then begin
;    fmat="('dsf: code = x',i3,' ',i3)"
;    print,id,q[0],format=fmat
;    stop,' dsf'
;  end
;end

;---------------------------

        end
    end
    
;============================================================

    else: begin
        help,polar
        stop,'ERRORx in dsf: polar = '+string(polar)
    end
end

; ----- convolution

;fdat = self->rfft(dat)
;dat  = self->H(fdat,fprf)

; -----

; reverse the rotation so that image is returned unchanged. No need
; to rotate lla0, its not returned.

; !!! no longer needed since we now operate on a copy of the original image!
;if(roco eq 1)then for nu=0,N-1 do $
;  image[*,nu,*]=rotate(reform(temporary(image[*,nu,*])),3)

;;;print,'dsf: ',gamma,theta
;if (roco eq -1 and (theta ne 0 or gamma ne 0)) then begin
;;;    im1=image
;;;    if (theta ne 0) then print,'rotating theta'
;    if (fast) then begin
;        if (theta ne 0) then cuberot3d,image,[0.0,0.0-theta,0.0]
;;;    if (gamma ne 0) then print,'rotating gamma'
    ; note gamma should be CCW, so reverse is CW
;        if (gamma ne 0) then cuberot3d,image,[0.0,0.0,gamma]
;    end else begin
;        image=turn_3d(image,0.0,0.0-theta,0.0-gamma,/resize)
;    end
;;;    im2=image
;;;    save,file='im.sav',im1,im2
;end

tdsf=tdsf+(systime(-1)-t0)

;;print,'return from dsf'

return,dat 
end


pro pxndsf_force
; just forces the compilation of the above object
end
