;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : p_nozero
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;               
; alters a data (and, if given, accompanying noise) so that any
; 'zero' values are set to dmin, while the noise is set to
; 2*dmin if an existing noise is not there or is less than 2*dmin
;
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; alters a data (and, if given, accompanying noise) so that any
; 'zero' values are set to dmin, while the noise is set to
; 2*dmin if an existing noise is not there or is less than 2*dmin
;

PRO p_nozero,data,noise,dmin=dmin

  if (n_elements(dmin) eq 0) then begin
      ie=where(data gt 0)
      if (ie[0] eq -1) then dmin=0.0 else dmin=min(data[ie])
  end

  ; now do actual munging

  ; note we compare against 'dmin' not zero, as this allows us to
  ; recurvively call p_nozero to set noise to an appropriate minimum
  ; in case the intrinsic noise isn't there (a rare case).
  ie=where(data le dmin)
  if (ie[0] ne -1) then data[ie]=dmin*0.1

  ; also do noise as a min of 2*dmin
  if (n_elements(noise) ne 0) then p_nozero,noise,dmin=0.1*dmin

END
