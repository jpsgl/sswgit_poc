;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : stereo_project
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;
; Acts as an interface between the Pixon object and the user.
; Use this to get renders from the Pixon object.  Called by
; 'stereo_fetch', which is safer than calling this directly.
;               
; Use       : renders=stereo_project(pxwrapper,oPixon)
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            

;               
; $Log: stereo_project.pro,v $
; Revision 1.1  2009/04/22 19:22:44  antunes
; code relocation part 2 of 3.
;
; Revision 1.8  2009/03/31 14:57:56  antunes
; Re-org prepatory to checking into SSW.
;
; Revision 1.7  2008/01/04 20:16:50  antunes
; Final units (hopefully), also more tests and tools.
;
; Revision 1.6  2007/10/26 19:02:37  antunes
; Updated difference image needs. (Also made first cut at a back projection
; wrapper).
;
; Revision 1.5  2007/06/21 18:29:35  antunes
; Finished test cases and got issues reconciled.
;
; Revision 1.4  2007/06/13 18:37:30  antunes
; Added autoscaling, instructions.
;
; Revision 1.3  2006/12/15 19:34:27  antunes
; Better pretty pictures.
;
; Revision 1.2  2006/11/30 20:03:58  antunes
; Updated pixon and other printing and output routines.
;
; Revision 1.1  2006/04/11 15:55:06  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.7  2006/03/08 19:33:12  antunes
; Yet another refactoring.
;
; Revision 1.6  2006/03/08 17:52:23  antunes
; Abstracted pixon structures into an aggregate structure in order
; to be encapsulate Pixonisms from general rendering and reconstruction
; routines.
;
; Revision 1.5  2006/02/21 19:36:03  antunes
; Bug fixes and streamlining.
;
; Revision 1.4  2006/02/21 18:10:19  antunes
; Improved shared code for C and T pixon.
;
; Revision 1.3  2006/01/09 17:14:42  antunes
; More commented added.
; New 'super3drv' driver routine for getting user specs for reconstruction
; process now checked in.  Just type 'super3drv' to test.
;
;-            

; here is a little translation wrapper between Paul's original
; stereo_project and the arguments needed for ours.  Just drop
; in 'stereo_project_reiser' where he calls 'stereo_project'
; and this handles the rest
function stereo_project_reiser,sr,sc,image,oPixon,userstruct
  data=stereo_project(sr,oPixon,image=image,/retro)
  return,data
end

function stereo_project,pxwrapper,oPixon,ptr=ptr,norescale=norescale,$
                        image=image,retro=retro

; input    pixontype     'c'lassic or 't'etra
;          sr            rendering parameters
;          sc            clerical parameters
;          image	 image to be projected (only for cPixon)
;          oPixon        oPixon object
;          userstruct    user structure (input to dsf, dsft)
;
; output   sc.tdsf       time spent in dsf
;          sc.tdsft      time spent in dsft
;          sc.totaltime  time since sc.t00
;          data          data rendered from image
;
;-------------------------------------
;
; Optional argument /ptr makes it return pointers to the data
; rather than data arrays.  This option gets forced if the data
; are of different sizes (not the same Ndata x Ndata size).
;
; Routine checks data sizes first before processing, to allow
; for heterogenous data sizes.
;
; The /retro flag invokes the simplier non-pointer-supporting
; legacy 'stereo_project_06' method, for backwards testing against
; Paul's earlier test cases.
;

common debugs,phys,tdsf,tdsft,dbg

ptr=KEYWORD_SET(ptr); optional flag to return pointer to data, not data
norescale=KEYWORD_SET(norescale);
retro=KEYWORD_SET(retro)

if (retro) then begin
    ; we redirect to our earlier 'retro' method
    ; the next two lines are for backwards compatability
    if (tag_exist(pxwrapper,'sr')) then sr=pxwrapper.sr else sr=pxwrapper
    if (tag_exist(sr,'N')) then N=sr.N else N=sr.Ncube; backwards compat
    Nd = N_elements(sr.gamma); number of data frames
    if (n_elements(image) eq 0) then image=oPixon->Get(/im,/nop)
    oData=oPixon->get(/oData)
    data=dblarr(N,N,Nd)
    for m=0,Nd-1 do data[*,*,m]=oData[m]->dsf(image)
    return,data
end

pixontype = pxwrapper.object

sr=pxwrapper.sr
sc=pxwrapper.sc
;userstruct=pxwrapper.userstruct


; -----

;if (n_elements(pixontype) eq 0) then stop,'Must specify pixon type in stereo_project'

Nd = N_elements(sr.gamma)	; number of data frames

homogenous=1; tracks whether all data are the same size, assume yes...
if (tag_exist(sr,'N')) then begin
    N=sr.N; old method, safe but does not allow different data types
end else begin
    N=sr.Ndata[0] ; uses 1st data image as required sized, possibly off, so...
    for i=0,Nd-1 do if (sr.Ndata[i] ne N) then homogenous=0
end

;if (homogenous) then print,'homo' else print,'hetero'
if (homogenous eq 0 and ptr eq 0) then begin
    print,'Note data are of uneven size, forcing return of pointers to data'
    ptr=1
end

;idims = [N,N,N]
;ddims = [N,N]
if(sc.term ne 'batch')then loadct,0

; ----- Construct real data

; get transformed data from image 

;print,'*** stereo_project before get oData'
;help,/mem

if (ptr) then data = ptrarr(Nd) else data  = dblarr(N,N,Nd)

if (pixontype eq 'c') then begin

    if (n_elements(image) eq 0) then image=oPixon->get(/im,/nop)

    oData = oPixon->get(/oData)
    if (ptr) then begin
        ; return pointers
        for m=0,Nd-1 do data[m] = ptr_new(oData[m]->dsf(image))
    end else begin
        ; return data arrays
        for m=0,Nd-1 do data[*,*,m] = oData[m]->dsf(image)
    end

end else if (pixontype eq 't') then begin

    oData = oPixon->get(/dm)
    if (ptr) then begin
        ; return pointers
        for m=0,Nd-1 do data[m] = oData[m]
    end else begin
        ; return data arrays
        for m=0,Nd-1 do data[*,*,m] = *oData[m]
    end

end else begin
    print,'could not figure out pixon type, ending early.'
    stop
end

sc.tdsf=tdsf
sc.tdsft=tdsft
;sc.totaltime = systime(-1)-sc.t00

pxwrapper.sc=sc

; rescale by default, unless otherwise told... I wish IDL had 'unless'
if (norescale) then rescale=0 else rescale=1

if (rescale) then begin

    normd=1.0
    ; a little backwards compatibility here for pre-normd structures
    if (tag_exist(pxwrapper.sr,'normd')) then normd=pxwrapper.sr.normd
    rescalefactors=pxwrapper.sr.Istd/pxwrapper.sr.normd

    for m=0,Nd-1 do data[*,*,m]=data[*,*,m]*rescalefactors[m]

end else begin
    rescalefactors=1.0
end

return,data
end

