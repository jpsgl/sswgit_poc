;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : quickcme
;               
; Purpose   : stand-alone recon, part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;
; This is the major reconstruction routine, as it sets up the problem
; and calls everything.  The s3drs GUI just sits on top of this, this
; is the routine that does all the heavy lifting.  It can also be used
; as a command-line reconstruction routine.  You just give it dates
; and instrument names and off it goes.  It has many more flags,
; toggles and options than the s3drs GUI allows, so feel free to
; experiment with different methods.
;
; This sets things up, and its companion 'quickcme_run' will then do
; the actual reconstruction and return the results.
;               
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;
; envelope=TIMAGE brings in a FM to use as a constraining zmask
; Alternately, you can just specify a 2D array, which is then assumed
; to be the z-view view already good to go.
; If you use a 3D image and also set 'envviews' to an array of angles,
; it will create additional masks using those angles as planar
; constraining masks.
; Just providing an envelope= yields a Z-mask, the envviews allow
; ecliptic masking as well.  I skipped allowing just ecliptic (no
; Z) under the presumption that a Z mask is the least intrusive
; masking possible.  Ecliptic shapes are more nuanced and presume
; a specific halo-type orientation from some view, while the Z
; view simply limits the top region of space that is otherwise
; underfined by ecliptically-located satellites.

; /concavemask plus occultleft/right/top/bottom makes those masks
; concave curves centered on 0, rather than flat half-screen masks.
;
; /trimc3top is now obselete since implementing the automatic 'badC3' mask
; to eliminate bad areas of the data frame.
;
; /force forces secchi_prep to recreate rather than used cached earlier files
;
; /jumpstart makes a 'restricted minimum volume' guess at the solution,
; good for well-formed cases to help avoid Pixon oddities.
; presolution=cube lets you bring in a prior solution as a starter,
; either for continuing with different parameters or adding instruments.
; If you set 'presolution' then it pre-empts /jumpstart.  Any import can
; have a different cube size then the problem at hand, since we rebin it.
;
; /extendedmaskiszero makes the non-occulter portions of any given
; masks actually be altered so the _data_ gets set to 0 and the mask
; to one, enforcing a concept that those regions have no relevant
; signal and thus we want to fit them as voids.  Experimental.
;
; /customroi calls up an interactive widget to add custom masks to data
;
; /maxocculter uses the biggest given occulter for all images
; or, try
; specificocculter=NAME to make all use a specific occulter, e.g.
;   specificocculter='C3' makes all use a C3 occulter,
;   specificocculter='none' means no occulters get placed at all.
;   specificocculter='ignore' or empty means this field is ignored.
;
; New, added 'nsn=X', where it only applies the 'nadjust' weight
; to those pixels which are above the nsn cutoff S/N ratio
;
; /raw uses only the first date, with no subtraction.  For experiments.
; Note it's awkward, you still have to give a 'priordate' (if only
; 'background' or the same as cmedate), even though said priordate
; data is not actually used.
;
; New /x2 mode takes an existing 'solved.sav' solution, lets you
; define a z-mask using an ROI tool, then reruns the reconstruction
; using our zmask techniques, allowing for recursive solutions.
;
; occultmasks='r' or ['r','r'] uses 'r','l','t','b' to indicate which
; half of the image to mask for each image (or for all, if not given
; as an array).  Replaces earlier (depreciated) /occult[left|right|top|bottom].
;
; /simnoise lets you test real data as if it were a simulated image,
; with no background or prior frame contribution.  If given 'raw'
; as a second frame, has no effect (is the same operation).
;
; Now enabled this with /simple as a wrapper for 'simple3drv', so it runs
; up to /setuponly (no Pixon) but runs simple3drv and creates results.
;
; Also has /massimg as a flag to create/use mass images, though this
; requires you also set the renderer type to not use WL scattering in
; order to have proper physics.  Mostly experimental.
;
; Optional 'retstr' can be populated with the setup input (diff,ndiff,mset,px)
;
; The /gui flag changes the default naming scheme to a shorter,
; more generic one, as the GUI allows on-the-fly changing of
; some parameters that are otherwise hard-coded when you call
; this using the command line.
;
; You can add in other instruments with 'otherinstr=', as long as you create
; an appropriate routine 'p_load_otherinstr', and edit 'p_modelcor' and
; 'p_getmask' to include 'otherinstr'.  Also, if you are using the GUI,
; add that instrument to s3drs_init's s3drs.instr to have it automatically
; be available.
;
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; current status: not tested is whether indices clash if you do
; envelope modeling + z/xyz masking

; note /zmask and /jumpstart presume Cor2... probably not a problem,
; but worth looking into

; sample:
;quickcme,64,'20071231_025220','20071231_022220',/c3
;
; use of /c3 means it will look for the c3 file closest to the given
; times.  Ditto for /c2 and /cor1 and /cor2
; Note you _have_ to specify either a model or, for real data, a detector!
;
; The datacut=1e-11 (e.g.) flag cuts the data just before inserting
; (but after noise is calculated using unadultered data) to explore
; signal issues.
;

PRO quickcme_parse,N=N,cmedate=cmedate,priordate=priordate

  while (n_elements(N) eq 0) do begin
      print,'Input a value for N of 64, 128, or 256 (256 is risky)'
      read,'N = ? ',Nval
      if (Nval eq 64 or Nval eq 128 or Nval eq 256) then N=floor(Nval)
  end
  
  if (n_elements(cmedate) eq 0) then begin
      cmedate=""
      print,'Enter the datestamp of the CME, e.g. 20071104_222220'
      read,cmedate
  end

;  cpieces=strsplit(cmedate,'_',/extract)

  if (n_elements(priordate) eq 0) then begin  
      priordate=""
      print,'To run as a difference image, provide the datestamp of the'
      print,' immediately prior frame (for a running difference, e.g.'
      print,' 20071104_212220) or of an earlier quiet frame (for a'
      print,' difference image, e.g. 20071104_022220).  If you wish to'
      print,' instead just subtract a standard background, type "background"'
      read,priordate
  
  end

  if (strmid(priordate,0,4) eq 'back') then priordate='back'
  if (strmid(priordate,0,4) eq 'raw') then priordate='raw'

END

FUNCTION quickcme_excerpt,dfull,sunc=sunc

  if (datatype(sunc) eq 'STC') then mysunc=[sunc.xcen,sunc.ycen] else $
    mysunc=sunc

  ;help,sunc,mysunc,datatype(sunc)

   ; subselect item
   ; assume C3, aka FOV=2*Cor2
   ; so 0-1023 becomes 0-511
  No=n_elements(dfull[0,*]); usually 1024
  Np= No/2 ; usually 512
  delta=mysunc-[(No-1)/2.0,(No-1)/2.0]
  ;print,sunc,delta
  ;delta=[0,0]; test here of nixing recentering when excerpting
  dhalf=dblarr(Np,Np)
  ;help,dfull
  ;print,0,Np-1,delta[0]+No/4,delta[0]+(3*No/4),Np-1,delta[1]+(3*No/4)-1-delta[1]-No/4
  dhalf[0:Np-1,0:Np-1]= $
    dfull[delta[0]+No/4:delta[0]+(3*No/4)-1,delta[1]+No/4:delta[1]+(3*No/4)-1]
  return,dhalf

END



PRO quickCME,N,cmedate,priordate,zmask=zmask,xyzmask=xyzmask,$
             checksn=checksn,dn=dn,excerpt=excerpt,c2=c2,c3=c3,cor1=cor1,$
             sidemask=sidemask,nozero=nozero,test=test,cor2=cor2,$
             mxit=mxit,occulttop=occulttop,occultbottom=occultbottom,$
             occultleft=occultleft,occultright=occultright,pseudo=pseudo,$
             maskratio=maskratio,nadjust=nadjust,setuponly=setuponly,$
             maskbright=maskbright,scrubbright=scrubbright,sncrop=sncrop,$
             sncutoff=sncutoff,force=force,standardplots=standardplots,$
             nsn=nsn,best=best,model=model,debug=debug,x2=x2,$
             modelgamma=modelgamma,modeltheta=modeltheta,$
             polar=polar,indypair=indypair,rtpair=rtpair,pb=pb,$
             radonly=radonly,tanonly=tanonly,raw=raw,massimg=massimg,$
             trimc3top=trimc3top,tag=tag,jumpstart=jumpstart,roi=roi,$
             datacut=datacut,resume=resume,envelope=envelope,$
             envviews=envviews,concavemask=concavemask,recenter=recenter,$
             lascowt=lascowt,presolution=presolution,maskcheck=maskcheck,$
             extendedmaskiszero=extendedmaskiszero,customroi=customroi,$
             maxocculter=maxocculter,specificocculter=specificocculter,$
             occultmasks=occultmasks,simnoise=simnoise,simple=simple,$
             retstr=retstr,gui=gui,otherinstr=otherinstr

  on_error,2
;  pixon_env,'c',/local; ensure we are using local, latest version

  msbperdn=2.7E-12

  standardplots=KEYWORD_SET(standardplots)
  c2=KEYWORD_SET(c2)
  c3=KEYWORD_SET(c3)
  cor1=KEYWORD_SET(cor1)
  cor2=KEYWORD_SET(cor2)

  trimc3top=KEYWORD_SET(trimc3top)
  if (n_elements(model) ne 0) then modelflag=1 else modelflag=0
  if (n_elements(modelgamma) eq 0) then modelgamma=[0,90]
  if (n_elements(modeltheta) eq 0) then modeltheta=modelgamma*0.0
  nmodels=n_elements(modelgamma)

  if (n_elements(envelope) ne 0) then envelopeflag=1 else envelopeflag=0
  if (n_elements(envviews) eq 0) then lopes=0 else lopes=n_elements(envviews)

  test=KEYWORD_SET(test)
  debug=KEYWORD_SET(debug)
  gui=KEYWORD_SET(gui)
  x2=KEYWORD_SET(x2)
  checksn=KEYWORD_SET(checksn)
  setuponly=KEYWORD_SET(setuponly)
  simple=KEYWORD_SET(simple)
  recenter=KEYWORD_SET(recenter)
  jumpstart=KEYWORD_SET(jumpstart)
  if (n_elements(presolution) ne 0) then import=1 else import=0
  ; note that presolution=cube pre-empts jumpstarting
  if (jumpstart and import) then jumpstart=0
  force=KEYWORD_SET(force)
  dn=KEYWORD_SET(dn)
  pseudo=KEYWORD_SET(pseudo)
  massimg=KEYWORD_SET(massimg)
  extendedmaskiszero=KEYWORD_SET(extendedmaskiszero)
  maxocculter=KEYWORD_SET(maxocculter)
  customroi=KEYWORD_SET(customroi)
  maskbright=KEYWORD_SET(maskbright)
  scrubbright=KEYWORD_SET(scrubbright)
  zmask=KEYWORD_SET(zmask)  
  xyzmask=KEYWORD_SET(xyzmask)  
  if (xyzmask) then zmask=0; redundant
  sidemask=KEYWORD_SET(sidemask)
  nozero=KEYWORD_SET(nozero)
  excerpt=KEYWORD_SET(excerpt)
  if (n_elements(sncutoff) eq 0) then sncutoff=-1
  if (n_elements(nsn) eq 0) then nsn=0
  if (n_elements(sncrop) eq 0) then sncrop=-1
  occultleft=KEYWORD_SET(occultleft)
  occultright=KEYWORD_SET(occultright)
  occulttop=KEYWORD_SET(occulttop)
  occultbottom=KEYWORD_SET(occultbottom)
  concavemask=KEYWORD_SET(concavemask)
  trimc3top=KEYWORD_SET(trimc3top)
  rtpair=KEYWORD_SET(rtpair)
  radonly=KEYWORD_SET(radonly)
  tanonly=KEYWORD_SET(tanonly)
  if (rtpair) then radonly=0
  if (rtpair) then tanonly=0
  pb=KEYWORD_SET(pb)
  raw=KEYWORD_SET(raw)
  polar=KEYWORD_SET(polar)
  if (n_elements(indypair) eq 0) then indypair=0
  simnoise=KEYWORD_SET(simnoise)
  resume=KEYWORD_SET(resume)

  best=KEYWORD_SET(best)
  if (best) then begin
      nozero=1
      xyzmask=1
  end

  if (test) then begin
      ; set some defaults here just for testing
      if (n_elements(N) eq 0) then N=64
      if (n_elements(cmedate) eq 0) then cmedate='20071231_022220'
      if (n_elements(priordate) eq 0) then priordate='20071231_015220'
      checksn=1
      cor2=1
  end else begin
      quickcme_parse,N=N,cmedate=cmedate,priordate=priordate
  end

;  if (n_elements(nadjust) eq 0) then if (N eq 64) then nadjust=10.0 else $
;    if (N eq 128) then nadjust=100.0 else nadjust=1.0
  if (n_elements(nadjust) eq 0) then nadjust=1.0

;    if (N eq 128) then nadjust=100.0 else nadjust=1.0

  savestem=cmedate+'_'+int2str(N)+'_'+priordate
  if (n_elements(otherinstr) ne 0) then $
    savestem += '_other'+int2str(n_elements(otherinstr))
  if (cor2) then savestem += '_cor2'
  if (pb) then savestem += '_pb'
  if (polar) then savestem += '_polar'
  if (rtpair) then savestem += '_radtan'
  if (radonly) then savestem += '_rad'
  if (tanonly) then savestem += '_tan'
  if (indypair gt 0) then savestem += '_pol='+polangle[indypair]
  if (c2) then savestem += '_c2'
  if (c3) then $
    if (excerpt) then savestem += '_c3ex' else $
    savestem += '_c3'
  if (cor1) then savestem += '_cor1'

  if (n_elements(specificocculter) eq 0) then specificocculter='ignore'
  if (specificocculter ne 'ignore') then $
    savestem += '_occ=' + specificocculter

  gui_name=savestem; save shorter name for other uses

  ; more detailed naming
  if (envelopeflag) then savestem += '_envelope'
  if (pseudo) then savestem += '_cg'
  if (lopes ne 0) then savestem += '+'+strjoin(int2str(envviews),'_')
  
  if (extendedmaskiszero) then savestem += '_exmask'
  if (maxocculter) then savestem += '_mx'
  if (customroi) then savestem += '_customroi'
  if (zmask) then savestem += '_zmask'
  if (xyzmask) then savestem += '_xyzmask'
  if (sidemask) then savestem += '_sidemask'
  if (nozero) then savestem += '_nozero'
  if (jumpstart) then savestem += '_j'
  if (simnoise) then savestem += '_sim'
  if (massimg) then savestem += '_mass'
  if (import) then savestem += '_presolution'
  if (recenter) then savestem += '_recenter'
  if (n_elements(tag) ne 0) then savestem += '_'+tag
  if (n_elements(datacut) ne 0) then savestem += '_cut'
  if (n_elements(roi) ne 0) then savestem += '_roi'
  if (n_elements(lascowt) ne 0) then savestem += '_lascowt='+int2str(lascowt)
  if (nadjust gt 1.0) then savestem += '_wt='+int2str(nadjust)
  if (nadjust lt 1.0) then savestem += '_wt=fractional'

  if (n_elements(occultmasks) ne 0) then begin

      ; new per-element way

      occultmasks=strlowcase(occultmasks)
      nm=n_elements(occultmasks)
      occultleft=make_array(nm,/byte)*0
      occultright=make_array(nm,/byte)*0
      occulttop=make_array(nm,/byte)*0
      occultbottom=make_array(nm,/byte)*0
      if (total(STRMATCH(occultmasks,'*r*')) ne 0) then $
        occultright[where(STRMATCH(occultmasks,'*r*') eq 1)]=1
      if (total(STRMATCH(occultmasks,'*l*')) ne 0) then $
        occultleft[where(STRMATCH(occultmasks,'*l*') eq 1)]=1
      if (total(STRMATCH(occultmasks,'*t*')) ne 0) then $
        occulttop[where(STRMATCH(occultmasks,'*t*') eq 1)]=1
      if (total(STRMATCH(occultmasks,'*b*')) ne 0) then $
        occultbottom[where(STRMATCH(occultmasks,'*b*') eq 1)]=1

  end

  occultbitmask = $
    floor(total(occultleft+2*occultright+3*occulttop+5*occultbottom))

  if (occultbitmask ne 0) then begin
      savestem += '_ma=' + int2str(occultbitmask)
      if (concavemask) then savestem += 'c'
  end

  if (sncutoff gt 0) then savestem += '_sncutoff='+int2str(sncutoff)
  if (nsn gt 0) then savestem += '_nsn='+int2str(nsn)
  if (sncrop gt 0) then savestem += '_sncrop='+int2str(sncrop)

  ; recursive use of solution as mask for next run
  if (x2) then begin

      restore,/v,savestem + '_solved.sav'
      threeview,solution_image,/noplot,trio,/pixon
      tempmask=trio[*,*,2]>1e4
      envelope=p_roitool(tempmask)

      savestem += '_x2env'
      envelopeflag=1;

  end


  ; note we cannot resume if the save file does not yet exist
  if (gui) then savefile = gui_name + '_inputdata.sav' else $
    savefile = savestem + '_inputdata.sav'
  if (resume and file_exist(savefile) eq 0) then resume=0

  if (resume eq 0) then begin

      print,'Setting up for ',cmedate,' - ',priordate
      
  ; FIRST, PREP PXWRAPPER
      iele=0
      if (n_elements(otherinstr) ne 0) then begin
          iele=n_elements(otherinstr)
      end
      if (cor2) then begin
          if (polar) then iele=iele+6 else $
            if (rtpair) then iele=iele+4 else iele=iele+2
      end
      if (c3) then ++iele
      if (c2) then ++iele
      if (cor1) then begin
          if (rtpair) then iele=iele+4 else iele=iele+2
      end
      gamma=make_array(/float,iele,value=0.0)
      theta=gamma*0.0
      if (modelflag) then begin
                                ; front-place input elements
          gamma=[modelgamma,gamma]
          theta=[modeltheta,theta]
          iele=iele+nmodels
      end

      ict=iele              ; keep track of 'data' versus 'masks' here

      if (envelopeflag) then begin
          gamma=[gamma,0.0]
          theta=[theta,90.0]
          envelopeid=iele
          iele=iele+1
          ; add in additional planar envelope masks, if any (lopes>0)
          for icte=0,lopes-1 do begin
              gamma=[gamma,envviews[icte]]
              theta=[theta,0.0]
              iele=iele+1
          end
      end


      izct=iele              ; keep track of 'data' versus 'masks' here

      if (zmask or xyzmask) then begin
          gamma=[gamma,0.0]     ; add Z
          theta=[theta,90.0]    ; add Z
          if (xyzmask) then gamma=[gamma,90.0,0.0] ; also add XY
          if (xyzmask) then theta=[theta,0.0,0.0] ; also add XY
      end
      
      if (n_elements(otherinstr) ne 0) then $
        prep_3drv,pxwrapper,N=N,theta=theta,gamma=gamma,$
        otherinstr=otherinstr[0] else $
        if (cor2) then $
        prep_3drv,pxwrapper,N=N,theta=theta,gamma=gamma,/cor2 else $
        if (cor1) then $
        prep_3drv,pxwrapper,N=N,theta=theta,gamma=gamma,/cor1 else $
        prep_3drv,pxwrapper,N=N,theta=theta,gamma=gamma,/cor2

      if (massimg) then begin
          pxwrapper.sr.polar='n'
          pxwrapper.sr.imodel=9
      end
      
      pxwrapper.sc.gname = savestem
      
     ; store the specifics of this setup, for record-keeping
      pxwrapper.sc.xyzmask=xyzmask
      pxwrapper.sc.zmask=zmask
      pxwrapper.sc.sidemask=sidemask
      pxwrapper.sc.nozero=nozero
      pxwrapper.sc.excerpt=excerpt
      pxwrapper.sc.occult=occultbitmask
      pxwrapper.sc.brightpixels=maskbright+2*scrubbright ; 0, 1, 2 or 3
      pxwrapper.sc.nadjust=nadjust
      
      if (dn) then pxwrapper.sr.imodel=5 ; for input in DN
      
      if (n_elements(datacut) ne 0) then pxwrapper.sc.datacut=datacut

                                ; ** SECCHI FILENAMES AND DIFF-DATA
      
      rawdata=fltarr(N,N)       ; just a junk element, for the moment
      priors=rawdata            ; ditto
      
      iele=0
      if (modelflag) then begin
          timage=howmodel(N,model)
          for i=0,nmodels-1 do begin
              ren=pr_render(pxwrapper,timage,i)
              rawdata=[[[rawdata]],[[ren]]]
              priors=[[[priors]],[[ren*0.0]]]
              ++iele
          end
      end
      
      if (n_elements(otherinstr) ne 0) then begin

          ; ability to load any number of frames of another instrument

          for io=0,n_elements(otherinstr)-1 do begin

              ; need handler here to read in your particular data
              p_otherdata,cmedate,otherinstr[io],mycme,myhdr

              if (n_elements(myhdr) eq 0) then return ; early exit/abort

              ; and also difference frame or background, if any
              if (priordate eq 'back' or priordate eq 'background') then begin
                  p_otherdata,priordate,otherinstr[io],myprior,testhdr,/back
                  if (n_elements(testhdr) eq 0) then return ; early exit/abort
              end else if (priordate eq 'raw') then begin
                  myprior=mycme*0.0
              end else begin
                  p_otherdata,myprior,otherinstr[io],myprior,testhdr
                  if (n_elements(testhdr) eq 0) then return ; early exit/abort
              end

              ; parse header to store into geometry structure
              p_scchdr,myhdr,N=N,pxwrapper=pxwrapper,iele=iele
          
              ; now add to stack just as usual
              mycme=congrid(mycme,N,N,/inter)
              myprior=congrid(myprior,N,N,/inter)
          
              rawdata=[[[rawdata]],[[mycme]]]
              priors=[[[priors]],[[myprior]]]

              ++iele

          end

      end

      if (cor2) then begin
          
          ;cmecheck=find_cdata(cmedate,/cor2,ab='a',/justtime)
          ;if (priordate eq 'back') then priorcheck='back' else $
          ;  priorcheck=find_cdata(priordate,/cor2,ab='a',/justtime)
          cmecheck=cmedate
          if (priordate eq 'back' or priordate eq 'background') then $
            priorcheck='back' else $
            if (priordate eq 'raw') then priorcheck='raw' else $
            priorcheck=priordate
          dcA=load_cor(cmecheck,'a',N=N,hdr=hdrACor2,fname=cmeA,force=force,$
                       pb=pb,polar=polar,rtpair=rtpair,$
                       radial=radonly,tang=tanonly,indypair=indypair,$
                       /findnear,massimg=massimg)

          if (n_elements(hdrAcor2) eq 0) then return; early exit/abort
          
          for ip=0,n_elements(hdrAcor2)-1 do begin
              p_scchdr,hdrAcor2[ip],N=N,pxwrapper=pxwrapper,iele=iele
              if (polar eq 0) then pxwrapper.sr.exptime[iele] *= 3.0
              ++iele
          end

          if (priorcheck eq 'back' or priorcheck eq 'raw') then $
            hdr=hdrACor2        ; feed into getbkg
          dpA=load_cor(priorcheck,'a',N=N,hdr=hdr,force=force,pb=pb,$
                       polar=polar,rtpair=rtpair,radial=radonly,tang=tanonly,$
                       indypair=indypair,/findnear,massimg=massimg)

          if (n_elements(hdr) eq 0) then return; early exit/abort

          for ip=0,n_elements(hdrAcor2)-1 do $
            print,'Cor2A polar: ',hdrAcor2[ip].polar,hdr[ip].polar
          
          ; note we do a seperate check for B, since for some earlier
          ; dates, 'B' times do not exactly match 'A' times, *sigh*
          ;cmecheck=find_cdata(cmedate,/cor2,ab='b',/justtime)
          ;if (priordate eq 'back') then priorcheck='back' else $
          ;  priorcheck=find_cdata(priordate,/cor2,ab='b',/justtime)

          dcB=load_cor(cmecheck,'b',N=N,hdr=hdrBCor2,fname=cmeB,force=force,$
                       pb=pb,polar=polar,rtpair=rtpair,$
                       radial=radonly,tang=tanonly,$
                       indypair=indypair,/findnear,massimg=massimg)

          if (n_elements(hdrBcor2) eq 0) then return; early exit/abort

          for ip=0,n_elements(hdrBCor2)-1 do begin
              p_scchdr,hdrBcor2[ip],N=N,pxwrapper=pxwrapper,iele=iele
              if (polar eq 0) then pxwrapper.sr.exptime[iele] *= 3.0
              ++iele
          end
          if (priorcheck eq 'back' or priorcheck eq 'raw') then $
            hdr=hdrBCor2        ; feed into getbkg
          dpB=load_cor(priorcheck,'b',N=N,hdr=hdr,force=force,pb=pb,$
                       polar=polar,rtpair=rtpair,radial=radonly,tang=tanonly,$
                       indypair=indypair,/findnear,massimg=massimg)

          if (n_elements(hdr) eq 0) then return; early exit/abort

          for ip=0,n_elements(hdrBCor2)-1 do $
            print,'Cor2B polar pair: ',hdrBCor2[ip].polar,hdr[ip].polar
          
                                ; if no files, abort early
          if (n_elements(dcA) eq 1 or n_elements(dcB) eq 1 or $
              n_elements(dpA) eq 1 or n_elements(dpB) eq 1) then return
          
;          if (pregen eq 0) then begin
                                ; units are in DN, shift to MSB
;              dcA=dcA*2.7E-12
;              dcB=dcB*2.8E-12
;              dpA=dpA*2.7E-12
;              dpB=dpB*2.8E-12
;          end
          
;          save,dcA,dcB,file='dcab.sav'

          rawdata=[[[rawdata]],[[dcA]],[[dcB]]]
          priors=[[[priors]],[[dpA]],[[dpB]]]
          
      end
      
                                ; ** LASCO FILENAMES
      
      if (c3) then begin
          
          cmec3=find_cdata(cmedate)
          if (DATATYPE(cmec3) eq 'INT') then return ; early exit/abort

          file_copy,cmec3,file_basename(cmec3),/overwrite
;          lasco_prep,file_basename(cmec3),hdrLC3,dcL
          data=lasco_readfits(cmec3,hdrLC3)
          dcL=c3_calibrate(data,hdrLC3)

          p_scchdr,hdrLC3,N=N,pxwrapper=pxwrapper,iele=iele
          c3iele=iele
          ++iele
          
          if (priordate eq 'back' or priordate eq 'background') then begin
              dpL=getbkgimg(hdrLC3,/ffv)
          end else if (priordate eq 'raw') then begin
              dpL=fltarr(1024,1024)*0.0
          end else begin
              priorc3=find_cdata(priordate)
              if (DATATYPE(priorc3) eq 'INT') then return ; early exit/abort

              file_copy,priorc3,file_basename(priorc3),/overwrite
;              lasco_prep,file_basename(priorc3),hdrLignore,dpL

              data=lasco_readfits(priorc3,hdrLignore)
              dpL=c3_calibrate(data,hdrLignore)
          end
          
          ;tv_multi,fixed=128,/log,dcL,header='dcL in'
          ;tv_multi,fixed=128,/log,dpL,header='dpL in'
          ;tv_multi,fixed=128,/log,dcL-dpL,header='diff in'

          ; note in both rotating and excerpting, we lose our optical center

          if (excerpt) then begin

              ; excerpting also recenters on sun center, for ease of view
              dcL=quickcme_excerpt(dcL,sunc=get_sun_center(hdrLC3))
              dpL=quickcme_excerpt(dpL,sunc=get_sun_center(hdrLignore))

              ; also update headers (both Pixon and FITS)
              pxwrapper.sr.l0tau[c3iele]=pxwrapper.sr.l0tau[c3iele]*0.5
              pxwrapper.sr.tau[c3iele]=pxwrapper.sr.tau[c3iele]*0.5

              pxwrapper.sr.sunk0[c3iele]=ptr_new([(N-1)/2.0,(N-1)/2.0])

             ; dvoxfrac, flibm and istd will be recomputed in p_osetup later

              ;tv_multi,fixed=128,/log,dcL,header='dcL excerpt'
              ;tv_multi,fixed=128,/log,dpL,header='dpL excerpt'
              ;tv_multi,fixed=128,/log,dcL-dpL,header='diff excerpt'

          end

          ; rotate image to HAE, from solar north to ecliptic north
          p_lasco_to_hae,dcL,hdrLC3,/rectify,/recentered
          p_lasco_to_hae,dpL,hdrLignore,/rectify,/recentered

          ; often LASCO data has black bars, so remove them
          badC3 = byte(dcL le 0)
          badC3 = byte(badC3 + (dpL le 0))
          badC3 =~ badC3
          
          badC3=congrid(badC3,N,N,/inter)

          dcL=congrid(dcL,N,N,/inter)
          dpL=congrid(dpL,N,N,/inter)

          ;tv_multi,fixed=128,/log,dcL,header='dcL rot-regrid'
          ;tv_multi,fixed=128,/log,dpL,header='dpL rot-regrid'
          ;tv_multi,fixed=128,/log,dcL-dpL,header='diff rot-regrid'
          ;tv_multi,badC3,header='badC3 mask'

          rawdata=[[[rawdata]],[[dcL]]]
          priors=[[[priors]],[[dpL]]]
          
      end
      
      if (c2) then begin
          cmec2=find_cdata(cmedate,/c2)
          if (DATATYPE(cmec2) eq 'INT') then return ; early exit/abort

          file_copy,cmec2,file_basename(cmec2),/overwrite
          lasco_prep,file_basename(cmec2),hdrLC2,dcLfull
          p_scchdr,hdrLC2,N=N,pxwrapper=pxwrapper,iele=iele
          
          if (priordate eq 'back' or priordate eq 'background') then begin
              dpLfull=getbkgimg(hdrLC2,/ffv)
          end else if (priordate eq 'raw') then begin
              dpLfull=fltarr(1024,1024)*0.0
          end else begin
              priorc2=find_cdata(priordate,/c2)
              if (DATATYPE(priorc2) eq 'INT') then return ; early exit/abort

              file_copy,priorc2,file_basename(priorc2),/overwrite
              lasco_prep,file_basename(priorc2),hdrLignore,dpLfull
          end
          
          ; rotate image to HAE, from solar north to ecliptic north, & rectify
          p_lasco_to_hae,dcL,hdrLC2,/rectify
          p_lasco_to_hae,dpL,hdrLignore,/rectify

          dcL=congrid(dcLfull,N,N,/inter)
          dpL=congrid(dpLfull,N,N,/inter)
          
          rawdata=[[[rawdata]],[[dcL]]]
          priors=[[[priors]],[[dpL]]]

          ++iele

      end
      
      if (cor1) then begin
          
          ;cmec1date=find_cdata(cmedate,/cor1,ab='a',/justtime)
          ;if (priordate eq 'back') then priorc1date='back' else $
          ;  priorc1date=find_cdata(priordate,/cor1,ab='a',/justtime)
          cmec1date=cmedate
          if (priordate eq 'back' or priordate eq 'background') then $
            priorc1date='back' else $
            if (priordate eq 'raw') then priorc1date='raw' else $
            priorc1date=priordate
          
          dcc1A=load_cor(cmec1date,'a',N=N,hdr=hdrACor1,fname=cmec1A,$
                         force=force,/cor1,pb=pb,$
                         rtpair=rtpair,radial=radonly,tang=tanonly,$
                         indypair=indypair,/findnearest,massimg=massimg)

          if (n_elements(hdrAcor1) eq 0) then return; early exit/abort

          for ip=0,n_elements(hdrAcor1)-1 do begin
              p_scchdr,hdrAcor1[ip],N=N,pxwrapper=pxwrapper,iele=iele
              ;if (polar eq 0) then pxwrapper.sr.exptime[iele] *= 3.0
              ++iele
          end

          if (priorc1date eq 'back' or priorc1date eq 'raw') then $
            hdr=hdrACor1        ; feed into getbkg
          dpc1A=load_cor(priorc1date,'a',N=N,hdr=hdr,force=force,/cor1,$
                         rtpair=rtpair,radial=radonly,tang=tanonly,pb=pb,$
                         indypair=indypair,/findnearest,massimg=massimg)

          if (n_elements(hdr) eq 0) then return; early exit/abort

          for ip=0,n_elements(hdrAcor1)-1 do $
            print,'Cor1B polar pair: ',hdrACor1[ip].polar,hdr[ip].polar


          ; note we do a seperate check for B, since for some earlier
          ; dates, 'B' times do not exactly match 'A' times, *sigh*
          ;cmec1date=find_cdata(cmedate,/cor1,ab='b',/justtime)
          ;if (priordate eq 'back') then priorc1date='back' else $
          ;  priorc1date=find_cdata(priordate,/cor1,ab='b',/justtime)

          dcc1B=load_cor(cmec1date,'b',N=N,hdr=hdrBCor1,fname=cmec1B,$
                         rtpair=rtpair,radial=radonly,tang=tanonly,pb=pb,$
                         force=force,/cor1,indypair=indypair,/findnearest,$
                         massimg=massimg)

          if (n_elements(hdrBCor1) eq 0) then return; early exit/abort

          for ip=0,n_elements(hdrBcor1)-1 do begin
              p_scchdr,hdrBcor1[ip],N=N,pxwrapper=pxwrapper,iele=iele
              ;if (polar eq 0) then pxwrapper.sr.exptime[iele] *= 3.0
              ++iele
          end
          if (priorc1date eq 'back' or priorc1date eq 'raw') then $
            hdr=hdrBCor1        ; feed into getbkg
          dpc1B=load_cor(priorc1date,'b',N=N,hdr=hdr,force=force,/cor1,$
                         rtpair=rtpair,radial=radonly,tang=tanonly,pb=pb,$
                         indypair=indypair,/findnearest,massimg=massimg)

          if (n_elements(hdr) eq 0) then return; early exit/abort

          for ip=0,n_elements(hdrBcor1)-1 do $
            print,'Cor1B polar pair: ',hdrBcor1[ip].polar,hdr[ip].polar
          
          rawdata=[[[rawdata]],[[dcc1A]],[[dcc1B]]]
          priors=[[[priors]],[[dpc1A]],[[dpc1B]]]
          
      end
      
      ;we put a junk element in the beginning as a placeholder, here we remove it
      rawdata=rawdata[*,*,1:*]
      priors=priors[*,*,1:*]
      if (raw) then diff=rawdata else diff=rawdata-priors
      
      if (debug) then begin
          print,'rawdata min, max',minmax(rawdata)
          print,'priors min, max',minmax(priors)
          print,'diff min, max',minmax(diff)
      end
      
      ; handle missing data
      ;ie=where(rawdata le 0 or priors gt 0)
      ;if (ie[0] ne -1) then diff[ie]=0.0
      
      ; find dmin for later possible munging
      ie=where(diff gt 0)
      if (ie[0] eq -1) then dmin=0.0 else dmin=min(diff[ie])
      
      ; handle negative numbers in difference images
      ie=where (diff lt 0)
      if (ie[0] ne -1) then diff[ie]=0.0
      
      
      ; ** NOISE
      
      if (simnoise) then begin

          ; test case, sim noise of just 'image' not data+back
          ndiff=p_simplenoise(diff,pxwrapper)/nadjust

      end else begin

          ; usual case
          ndiff=p_simplenoise(rawdata,pxwrapper,priors)/nadjust

      end

      if (nsn gt 0) then begin
          ; only adjust the noise weighting for those noise
          sn=ratio(diff,ndiff)
          ie=where(sn ge nsn)
          if (ie[0] ne -1) then ndiff[ie]=ndiff[ie]/nadjust
      end

      ; optionally can add more 'wiggle' to C3 data when adding it in
      if (c3 and n_elements(lascowt) ne 0) then $
        ndiff[*,*,c3iele]=ndiff[*,*,c3iele] * lascowt


      ; ** OPTIONAL RECENTERING

;      help,pxwrapper,diff,ndiff

      ;mset=p_getmask(sr=pxwrapper.sr,ratio=maskratio)
      ;tv_multi,diff*mset,/log,/local,header='orig'

      ;print,p_getk0(pxwrapper,/sun)
      if (recenter) then p_recenter,diff,pxwrapper,extradat=ndiff,masks=rcmasks
      
     ; *** MASKING

      if (maxocculter) then begin

          ; first list detectors in increasing size of r_occ
          fovs=p_instrspec('fov',/s3drs)
          fovname=p_instrspec('name',/s3drs)
          fovindices=sort(fovs)
          cascade=fovname[fovindices]

          ;cascade=['C1', 'C2', 'COR1', 'COR2', 'C3']

          iorig=where (cascade eq strupcase(pxwrapper.sr.detector[0]))
          for i=0,ict-1 do begin
              ipos=where (cascade eq strupcase(pxwrapper.sr.detector[i]))
              if (ipos gt iorig) then iorig=ipos
          end
          detector=cascade[iorig] ; our biggest detector

          mset=p_getmask(detector,sr=pxwrapper.sr,ratio=maskratio)

      end else if (specificocculter ne 'ignore') then begin
          ; force a single specific occulter for all obs, or use
          ; 'none' to have no occulters used at all
          print,'using specific ',specificocculter
          mset=p_getmask(specificocculter,sr=pxwrapper.sr,ratio=maskratio)
          ;tv_multi,mset

      end else begin

          mset=p_getmask(sr=pxwrapper.sr,ratio=maskratio)

      end

;      if (recenter) then mset=mset * p_recenter_mask(diff)
      ;tv_multi,diff*mset,/log,/local,header='new'

      ; allows inputing external ROI to these masks, but be careful--
      ; it does no input checking and assumes exact matches
      if (n_elements(roi) ne 0) then begin
          for iroi=0,n_elements(roi[0,0,*])-1 do begin
              mset[*,*,iroi] = mset[*,*,iroi] * roi[*,*,iroi]
          end
      end

     ; extremely obscure option, cuts off right side of both A & B with mask
          ;if (occultright) then mset[N/2:*,*,*]=0.0
          ;if (occultleft) then mset[0:N/2,*,*]=0.0
          ;if (occulttop) then mset[*,N/2:*,*]=0.0
          ;if (occultbottom) then mset[*,0:N/2,*]=0.0
      if (occultbitmask ne 0) then begin

          ; accommodates older depreciated way and newer mask array spec
          if (n_elements(occultmasks) ne 0) then begin
              ; new method
              nm=n_elements(occultmasks)
              mtype='array'
          end else begin
              ; older, depreciated method
              nm=n_elements(mset[0,0,*])
              mtype='byte'
          end

          for im=0,nm-1 do begin

              tempmask=bytarr(N,N) + 1

              ; older depreciated way just has occult* entities as
              ; single integers, while newer uses an array of them,
              ; so we clumsily handle those two cases here
              if (mtype eq 'array') then imt=im else imt=0

              if (occultright[imt]) then begin
                  if (concavemask) then $
                    tempmask *= p_getmask('corners',N,offshift=1,rocc=0) $
                  else tempmask[N/2:*,*]=0
              end
              if (occultleft[imt]) then begin
                  if (concavemask) then $
                    tempmask *= p_getmask('corners',N,offshift=2,rocc=0) $
                  else tempmask[0:N/2,*]=0
              end
              if (occulttop[imt]) then begin
                  if (concavemask) then $
                    tempmask *= p_getmask('corners',N,offshift=3,rocc=0) $
                  else tempmask[*,N/2:*]=0
              end
              if (occultbottom[imt]) then begin
                  if (concavemask) then $
                    tempmask *= p_getmask('corners',N,offshift=4,rocc=0) $
                  else tempmask[*,0:N/2]=0
              end

              mset[*,*,im]=mset[*,*,im]*tempmask

          end
      end


      if (customroi) then mset=mset*p_roitool(diff)

      ; note we place this here, after region maskes added, but before
      ; we do artifact, hot pixel and other removals that we want to keep
      if (extendedmaskiszero) then begin
          ; we want just the inner occulter for this
          ; modify data (cor2, cor1, c2, c3) only
          if (maxocculter) then $
            mtemp=p_getmask(sr=pxwrapper.sr,routside=214) else $
            mtemp=p_getmask(sr=pxwrapper.sr,routside=214)
          ; set data to zero where old mask was 0
          diff[*,*,0:ict-1]=diff[*,*,0:ict-1]*mset[*,*,0:ict-1]
          ; and replace masks with occulter-only masks
          mset[*,*,0:ict-1]=mtemp[*,*,0:ict-1]
      end

      if (recenter) then mset=mset * rcmasks

      ; allow semi-automated ingest of extra masks for special
      ; data cases (that have artifacts), named by date
      extramasks=[cmedate+'_extramasks.sav',priordate+'_extramasks.sav']
      print,'checking for ',extramasks
      for i=0,1 do if (file_exist(extramasks[i])) then begin
          restore,/v,extramasks[i]; brings in item 'extramasks'
          N=n_elements(mset[*,0,0])
          for j=0,n_elements(extramasks[0,0,*])-1 do $
            mset[*,*,j]=mset[*,*,j]*congrid(extramasks[*,*,j],N,N)
      end

      if (c3) then mset[*,*,c3iele]=mset[*,*,c3iele] * badC3

      if (c3 and trimc3top) then begin
          if (excerpt) then mset[*,3*N/4:*,c3iele]=0.0 else $
            mset[*,4*N/7:*,c3iele]=0.0
      end
      
      ; to add a sigma and value to side masks is tricky
      ; mask is 1 for good data, 0 for bad
      ; so we want to:
      ;   1) where mask=0, set data=dmin*0.1
      ;   2) where mask=0, set sigma=dmin*10
      ;   3) set mask=1 everywhere
      if (sidemask) then begin
          ie=where(mset eq 0)
          if (ie[0] ne -1) then diff[ie]=dmin*0.1
          if (ie[0] ne -1) then ndiff[ie]=dmin*10.0
          mset=mset*0.0+1.0
      end
      
      ; for xyz/z/envelope masking, the data is the mask, mask=inv data

      if (envelopeflag) then begin
          ; lopes=0 if just zenvelope, larger if additional views, do all
          ; remember icte=0,0 will run at least once (for the main zmask),
          ; and if there are additional envviews, will handle them as well
          for icte=0,lopes do begin
              if (icte eq 0) then $
                print,'Adding envelope as z-view' else $
                print,'Adding additional envelope at angle ',envviews[icte-1]

              if ((size(envelope))[0] eq 2) then begin
                  ; it's already a mask
                  faked=envelope
              end else begin
                  faked=pr_render(pxwrapper,envelope,envelopeid+icte)
              end
              faked=faked/max(faked) ; normalize as 0-1
              emask=faked*0.0
              ie=where(faked le 0.0)
              if (ie[0] ne -1) then emask[ie]=1 ; enforce zeroness
              mset[*,*,envelopeid+icte]=emask

              datumZ=faked*0.0 + dmin*10.0 ; low everywhere
              print,'DMin is ',dmin
              sigmaZ= 0.5 * datumZ ; S/N ~ 2
              diff=[[[diff]],[[datumZ]]]
              ndiff=[[[ndiff]],[[sigmaZ]]]
          end
;          tv_multi,[[[datumZ]],[[sigmaZ]],[[emask]]],/local,/log
;          print,'elements is ',envelopeid,pxwrapper.sr.gamma[envelopeid],pxwrapper.sr.theta[envelopeid]
      end

      if (zmask or xyzmask) then begin
          if (n_elements(otherinstr) ne 0) then maskme=otherinstr[0] else $
            if (cor1) then maskme='COR1' else maskme='COR2'
          blank=p_getmask(maskme,N,rocc=-1)
          datumZ=blank*0.0 + dmin*0.1 ; low everywhere
          maskZ= ~blank
          sigmaZ= datumZ * 10   ; wildly noisy
          
          pxwrapper.sr.detector[izct]='z mask'
          mset[*,*,izct]=maskZ
          diff=[[[diff]],[[datumZ]]]
          ndiff=[[[ndiff]],[[sigmaZ]]]
          if (xyzmask) then begin
              pxwrapper.sr.detector[izct+1]='y mask'
              pxwrapper.sr.detector[izct+1]='x mask'
              mset[*,*,izct+1]=maskZ
              mset[*,*,izct+2]=maskZ
              diff=[[[diff]],[[datumZ]],[[datumZ]]]
              ndiff=[[[ndiff]],[[sigmaZ]],[[sigmaZ]]]
          end
          
      end
      
      ; primitive hot pixel remover:
      ; 'maskbright' adds to mask and doesn't change data,
      ; 'scrubbright' actually modifies the data
      ; both only operate on data, not xyzmasks
      if (maskbright or scrubbright) then begin
          for iframe=0,ict-1 do begin
              dd=diff[*,*,iframe]
              mm=mset[*,*,iframe]
              dcap=percentiles(dd,value=[0.99])
              ibright=where(dd gt dcap)
              for i=0,n_elements(ibright)-1 do begin
                  myi=ibright[i]
                  if (myi gt 0 and myi ne N-1) then $
                    if (dd[myi] gt dd[myi-1] + dd[myi+1]) then begin
                      if (maskbright) then mm[myi]=0 else dd[myi]=0
                  end
              end
              if (maskbright) then mset[*,*,iframe]=mm else diff[*,*,iframe]=dd
          end
      end
      
      ; SN CUTOFF/FEATURES ONLY mod
      ; mod to extract only features and zero out data where SN<cutoff
      ; by masking the other regions so Pixon is free to do what it wishes
      ; sncutoff is similar to a high-pass filter
      ; sncrop is similar to a low-pass filter
      if (sncutoff gt 0 or sncrop gt 0) then begin
          for iframe=0,ict-1 do begin
              dd=diff[*,*,iframe]
              mm=mset[*,*,iframe]
              if (sncutoff gt 0) then begin
                  isn=where (ratio(diff,ndiff) le sncutoff)
                  if (isn[0] ne -1) then mm[isn]=0.0
              end
              if (sncrop gt 0) then begin
                  isn=where (ratio(diff,ndiff) ge sncrop)
                  if (isn[0] ne -1) then mm[isn]=0.0
              end
              mset[*,*,iframe]=mm
          end
      end

      ; *** false zero mod, to remove actual data zeroes
      if (nozero) then p_nozero,diff,ndiff
      
      ; switch to DN if asked
      if (dn) then begin
          diff=diff/msbperdn
          ndiff=ndiff/msbperdn
      end
      
      if (n_elements(mxit) ne 0) then pxwrapper.sp.mxit=mxit
      
      if (n_elements(datacut) ne 0) then begin
      ; a typical difference image may want datacut=1e-11 or so
          ied=where (diff le datacut)
          if (ied[0] ne -1) then diff[ied]=0.0
          ; to avoid knocking out the envelope, a bit cumbersome
          ; work-around to restore the envelope(s).
          if (envelopeflag) then diff[*,*,envelopeid]=dmin*10.0
          for icte=0,lopes-1 do diff[*,*,envelopeid+icte]=dmin*10.0
      end

      save,pxwrapper,diff,ndiff,mset,rawdata,priors,$
        file=savefile

      ; ** CHECK S/N
      
      if (checksn) then p_checksn,diff,ndiff,mset,maskcheck=maskcheck


  end else begin

      restore,/v,savefile

  end

  retstr={diff:ptr_new(diff),ndiff:ptr_new(ndiff),mset:ptr_new(mset),$
          px:pxwrapper}
      
  if (simple) then begin
      if (massimg) then backproject=0 else backproject=1
      alt=simple3drv(diff*mset,pxwrapper=pxwrapper,backproject=backproject,$
                     /usemax,/norm,/carve,contour=checksn,/save)
  end

  if (setuponly or simple) then return

;  ;no longer need to call env here, it's in quickcme_run
;  pixon_env,'c',/local; ensure we are using local, latest version

  if (polar) then iend=5 else if (rtpair) then iend=3 else iend=1
  oPixon = quickcme_run(pxwrapper,diff,ndiff,mset,jumpstart,iend,$
                        import,presolution=presolution,pseudo=pseudo,$
                        checksn=checksn)

  ;solution_image=oPixon->get(/im,/nop)
  solution_image=stereo_fetch(pxwrapper,oPixon,/im)
  solution_renders=stereo_fetch(pxwrapper,oPixon,/render)
  ;input_data=oPixon->get(/data,/nop)
  input_data=stereo_fetch(pxwrapper,oPixon,/data)

  save,pxwrapper,oPixon,solution_image,solution_renders,input_data,$
    file=savestem + '_solved.sav'
  
  print,savestem + '_solved.sav' 

  if (standardplots) then begin
      if (polar) then begin
          ; polar has too many elements, need to split them
          Nd=pxwrapper.sr.Nd
          p_standardplots,/best,pxwrapper,oPixon,/mega,cor1=cor1,c3=c3,$
            mxit=80,Nstart=0,Nend=Nd/2,multips=1
          p_standardplots,/best,pxwrapper,oPixon,/mega,cor1=cor1,c3=c3,$
            mxit=80,Nstart=Nd/2,Nend=Nd,multips=2
      end else begin
          p_standardplots,/best,pxwrapper,oPixon,/mega,cor1=cor1,c3=c3,mxit=80
      end
  end

  ; now free things
  oPixon->free
  ;delvar,solution_image
  ;if (n_elements(startimage) ne 0) then delvar,startimage
  ;if (n_elements(mask3D) ne 0) then delvar,mask3D
  ;delvar,diff,ndiff,mset,rawdata,priors,solution_renders,input_data,pxwrapper
  heap_gc; hopefully frees any remaining items

END
