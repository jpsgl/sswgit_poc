;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : p_osetup
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;
; Crucial code that instantiates the Pixon object, necessary for
; any Pixon work.  Assume Pixon is in your path and loadable.
;               
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
PRO p_osetup,pxwrapper,timage,oPixon

data_ptr  =  PTRARR(pxwrapper.sr.Nd)

user=p_userstruct(pxwrapper)

if (pxwrapper.sc.doubleprec) then begin

    for m=0,pxwrapper.sr.Nd-1 do $
      data_ptr[m]=ptr_new(dblarr(pxwrapper.sr.Ndata[m],pxwrapper.sr.Ndata[m]))

    shmmap,/DOUBLE,$
      DIMENSION=[pxwrapper.sr.Ncube,pxwrapper.sr.Ncube,pxwrapper.sr.Ncube],$
      GET_NAME=segname

end else begin

    for m=0,pxwrapper.sr.Nd-1 do $
      data_ptr[m]=ptr_new(fltarr(pxwrapper.sr.Ndata[m],pxwrapper.sr.Ndata[m]))

    shmmap,/FLOAT,$
      DIMENSION=[pxwrapper.sr.Ncube,pxwrapper.sr.Ncube,pxwrapper.sr.Ncube],$
      GET_NAME=segname

end

tphi=ptr_new(shmvar(segname),/NO_COPY)

oPixon = OBJ_NEW('Pixon', $
                 data   = data_ptr,        $
                 sigma  = 0,           $
                 phi    = tphi,             $
                 user   = user,            $
                 mxit   = pxwrapper.sp.mxit,            $
                 noshm  = pxwrapper.sp.noshm           $
                )

END
