FUNCTION p_build_sn,noisemod,ftol,fsig
;+
; $Id: p_build_sn.pro,v 1.1 2009/04/22 19:22:18 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : p_build_sn.pro
;               
; Purpose   : Populates the Pixon 'sn' noise structure
;               
; Explanation: Is a wrapper to put user values into the Pixon structures.
;              It puts in the user parameters and also sets
;              'reasonable' defaults for the rest of the structure
;              variables, and also initializes many structure values.
;               
; Use       : IDL> p_build_sn,noisemod,ftol,fsig
;    
; Inputs    : 
;             noisemod : noise model to use
;             ftol : noise tolerance level
;             fsig : noise sigma
;               
; Outputs   : the 'sn' structure
;
; Keywords  : none
;
; Calls from LASCO : none 
;
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : Simulation
;               
; Prev. Hist. : See Paul Reiser's code and notes.
;
; Written     : Sandy Antunes, NRL, Nov-Dec 2005
;               
; $Log: p_build_sn.pro,v $
; Revision 1.1  2009/04/22 19:22:18  antunes
; code relocation part 2 of 3.
;
; Revision 1.1  2006/04/11 15:55:05  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.2  2005/12/27 17:48:24  antunes
; Commented half of the routines using SSW style.  Also added a new
; axes test case 'arrowcube'.
; Checked in the Tomography/old/mikic_img.pro routine here because it
; is a dependency for the general image-prep routine 'get_timage.pro'
;

;-            
; ----- noise specification
seed = 0

sn= {$
noisemod:   noisemod,        $ ; [] Noise model
seed:       seed,            $ ; [] random number seed for noise. -1 means no noise
fsig:       fsig,            $ ; [] if(seed ne -1) then noise added is fsig*max(data)
ftol:       ftol             $ ; [?] Noise model parameter(s)
}

  return,sn
END

