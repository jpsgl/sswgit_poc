;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : p_getk0
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;               
; This is a wrapper to make updating older pixon routines
; easier.  The original pixon structures had data element
;  k0 = [2,Nd], type=double  (where Nd = number of frames).
; The newer pixon structure uses
;  k0 = [Nd], type=pointer, where each element points to
;             a double array [2]
;  sunk0 = [Nd], type=pointer, where each element points to
;             a double array [2]
; Where 'k0' is the optical center and 'sunk0' is the
;   sun reference pixel.
;
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            

; This is a wrapper to make updating older pixon routines
; easier.  The original pixon structures had data element
;  k0 = [2,Nd], type=double  (where Nd = number of frames).
; The newer pixon structure uses
;  k0 = [Nd], type=pointer, where each element points to
;             a double array [2]
;  sunk0 = [Nd], type=pointer, where each element points to
;             a double array [2]
; Where 'k0' is the optical center and 'sunk0' is the
;   sun reference pixel.
;
; By default, this returns 'k0', or you can say
; '/sun' to get the sun center.
;
; So, use this routine instead of specifically calling
;   either k0[2,i] or *k0[i] and it will intelligently
;   handle both older and newer pixon structures.
; e.g. use:
;   k0 = p_getk0(sr,index)
; You can also return the specific element (either 0 or 1)
;   k00 = p_getk0(sr,index,0)
;   k01 = p_getk0(sr,index,1)
;
; You can also add an offset or shift when return it, handy
; for altering k0, e.g.
; for m=0,Nd-1 do sr.k0[m] = ptr_new( p_getk0(sr,m,shift=-0.25) )
;
; The routine can take an incoming structure that contains a
; 'k0' array of pointers or 2D array, or also just an incoming
; 'k0' item (already pulled from the structure)
; Remember it just returns 1 or 2 double valued scalars, no matter
;   what the input (2 if given just an 'index', 1 if also given 'opt...')
;

function p_getk0,sr,index,optional_subelement,shift=shift,sun=sun

  sun=KEYWORD_SET(sun)

  if (n_elements(index) eq 0) then index=-1; returns all elements

  ; allows pxwrapper or pxwrapper.sr or just sr
  if (tag_exist(sr,'sr')) then mysr=sr.sr else mysr=sr

  ; check if we gave structurein.k0 or just k0 itself
  if (datatype (sr) eq 'STC') then begin

      if (sun and tag_exist(mysr,'sunk0')) then begin
          kset = mysr.sunk0
      end else begin
          kset=mysr.k0
      end

  end else begin
      kset=sr; they already passed it the k0/sunk0 they need
  end

  if (datatype(kset) eq 'PTR') then begin
      if (n_elements(kset) eq 1) then index=0; case of single element is odd
      if (index eq -1) then begin
          ; all elements
          nume=n_elements(kset)
          k0=make_array(2,nume,/float)
          for i=0,nume-1 do k0[*,i]=*(kset[i])
      end else begin
          ; just the chosen one
          k0=*(kset[index])
      end
  end else begin
      if (index eq -1) then begin
          ; all elements
          k0=kset
      end else begin
          ; just the chosen one
          k0=kset[*,index]
      end
  end

  if (n_elements(shift) ne 0) then k0 = k0+shift

  if (n_elements(optional_subelement) ne 0) then begin
      k0 = k0[optional_subelement]; note array gets cast to double scaler!
  end

;  print,'k0 returned is: ',k0

  return,k0

end

pro test_getk0
  ; a test case used to verify the above
  k0=make_array(2,3,/double,/index)
  sr = {k0: k0}
  print,'double test: dumping k0: '
  print,sr.k0
  help,sr.k0
  print,'k0 middle item is: ',p_getk0(sr,1)
  print,'k0 element 1 of middle item is: ',p_getk0(sr,1,1)

  k0=make_array(3,/ptr)
  k0[0] = ptr_new([0.,1.])
  k0[1] = ptr_new([2.,3.])
  k0[2] = ptr_new([4.,5.])
  sr = {k0: k0}
  print,*k0[0],*k0[1],*k0[2]
  help,sr.k0
  print,'pointer test: dumping k0 in sr: '
  print,'k0 middle item is: ',p_getk0(sr,1)
  print,'k0 element 1 of middle item is: ',p_getk0(sr,1,1)

  print,'pointer test structure-free: dumping sr.k0: '
  print,'k0 middle item is: ',p_getk0(sr.k0,1)
  print,'k0 element 1 of middle item is: ',p_getk0(sr.k0,1,1)

  print,'pointer shift test:'
  for m=0,n_elements(sr.k0)-1 do sr.k0[m]=ptr_new( p_getk0(sr,m,shift=1) )

  print,'k0 middle item is now +1: ',p_getk0(sr,1)
  print,'k0 element 1 of middle item is now +1: ',p_getk0(sr,1,1)

end
