;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : p_userstruct
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;               
; Split off Paul's 'user' structure from the rest of pixon's object
; The 'userstruct' is the part of the Pixon object that we are allowed
; to define and modify.  It is then accessible to routines we write
; such as the renderer.  We use it to store all relevant rendering and
; geometry info, since Pixon has no inherent geometry or reference frame.
;
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; Split off Paul's 'user' structure from the rest of pixon's object


FUNCTION p_userstruct,pxwrapper

;common debugs,phys,tdsf,tdsft,dbg

Nd=pxwrapper.sr.Nd

;dbug=0
;dbg=0
;tdsf = 0
;tdsft = 0

; need sr and sp for sure

Ncube          =  pxwrapper.sr.Ncube
Ndata          =  pxwrapper.sr.Ndata; array of Ns
fwhm_2d    =  0
fwhm_3d    =  0
tdsf       =  pxwrapper.sc.tdsf
tdsft      =  pxwrapper.sc.tdsft

machine    =  getenv('HOST')
;pxwrapper.sc.machine =  machine
pxwrapper.sc.date    =  systime()


;if(n_elements(phys) eq 0)then phys = 1

;idims  =  [Ncube,Ncube,Ncube]

for m=0,Nd-1 do begin

    dvoxfrac=pxwrapper.sr.dvox/pxwrapper.sr.L0tau[m]
    o_flimb,pxwrapper.sr.chl,Ncube,1/pxwrapper.sr.L0tau[m], $
      dvoxfrac,pxwrapper.sr.lla0,fa,fb,fc,rmin2,an,ImI0
;    pxwrapper.sr.an=an
;    pxwrapper.sr.limbdark[m]=ImI0
;    pxwrapper.sr.Istd[m]  =  $
;      get_istd_Nd(pxwrapper.sr.imodel[m],pxwrapper.sr.L0,pxwrapper.sr.n0, $
;                  pxwrapper.sr.L0tau[m],ImI0 = ImI0)
end

; ----- Create Pixon Object 

;prf2d  =  gprf(ddims, fwhm = fwhm_2d)
prf2d=PTRARR(Nd)
for m=0,Nd-1 do prf2d[m]  =  ptr_new(gprf([Ndata[m],Ndata[m]], fwhm = fwhm_2d))

tau2=make_array(Nd,/double)
tau2 = pxwrapper.sr.tau*pxwrapper.sr.tau

user  =  PTRARR(pxwrapper.sr.Nd)

for k = 0,pxwrapper.sr.Nd-1 do begin

    ; note ffunc modifies 'pxwrapper.sr' (trW,trRRs,trP,trVs)
    ; as well as returning values
  ffunc,pxwrapper.sr,k,ps,mu,nu,is,jc,K2,u,x,y,rhos2,c2z,s2z

  rho2   =  x*x+y*y
  psLs   =  ps*pxwrapper.sr.LL[k]
  psLs2  =  psLs^2

  ; prepFPRF returns a ptrarr(ndata,nchan)
  fprfset=PrepFPRF(*(prf2d[k]), $
                   ddim=[Ndata[k],Ndata[k]], $
                   idim=[Ndata[k],Ndata[k]],/FREE )
  fprf = fprfset[0]

  dvoxfrac=pxwrapper.sr.dvox/pxwrapper.sr.L0tau[k]

  interp=pxwrapper.sr.interp
;  print,'initially interp is ',interp
  if (getenv('disumlib') eq '') then interp=1;switch to IDL if no C-version
;  print,'then interp is ',interp
;  print,getenv('disumlib')


  mystruct  =  { $
                 fprf:    fprf,                               $
                 roco:    pxwrapper.sr.roco[k],               $
                 fast:    pxwrapper.sc.fast,                  $
                 theta:   pxwrapper.sr.theta[k],              $
                 polar:   pxwrapper.sr.polar[k],              $
                 c2z:     c2z,                                $
                 s2z:     s2z,                                $
                 lla0:    pxwrapper.sr.lla0,                  $
                 rho2:    rho2,                               $
                 rhos2:   rhos2,                              $
                 psLs:    psLs,                               $
                 psLs2:   psLs2,                              $
                 i:       is,                                 $
                 jc:      jc,                                 $
                 K2:      K2,                                 $
                 d2:      dvoxfrac^2,                         $
                 fa:      fa*dvoxfrac^3,                      $
                 fb:      fb*dvoxfrac^3,                      $
                 fc:      fc*dvoxfrac^3,                      $
                 rmin2:   rmin2,                              $
                 tau:     pxwrapper.sr.tau[k],                $
                 tau2:    tau2[k],                            $
                 interp:  interp,                             $
                 machine: machine,                            $
                 g:       pxwrapper.sr.rho[k]                 $
               }

  user[k] = PTR_NEW(mystruct)

end

return,user

end

