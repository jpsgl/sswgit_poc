FUNCTION p_build_sp,reltol,abstol,mxit,anneal_specs, $
                    npo=npo,noct=noct,noshm=noshm,autorange=autorange
;+
; $Id: p_build_sp.pro,v 1.1 2009/04/22 19:22:19 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : p_build_sp.pro
;               
; Purpose   : Populates the Pixon 'sp' anneal structure
;               
; Explanation: Is a wrapper to put user values into the Pixon structures.
;              It puts in the user parameters and also sets
;              'reasonable' defaults for the rest of the structure
;              variables, and also initializes many structure values.
;               
; Use       : IDL> p_build_sp,reltol,abstol,mxit,npo,anneal_specs
;    
; Inputs    : 
;             reltol: cutoff relative change
;             abstol: Pixon: cutoff absolute change
;             mxit: Pixon: maximum number of iterations per cycle
;             anneal-specs : Pixon: anneal factor
;             noshm: flag whether to use shm or not (default = use shm)
;             autorange: value to normalize data to, i.e. what value
;                      the highest max(data) should have.  Default is
;                      -1 aka "do not use", common values are 1 (for
;                      all data to fall between 0 and 1) and 1000 (for
;                      good Pixon performance, data between 0 and 1000
;                      is a safe bet).  Used by some the wrapper scripts to
;                      set the global value sr.normd.
;               
; Outputs   : the 'sp' structure
;
; Keywords  : none
;
; Calls from LASCO : none 
;
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : Simulation
;               
; Prev. Hist. : See Paul Reiser's code and notes.
;
; Written     : Sandy Antunes, NRL, Nov-Dec 2005
;               
; $Log: p_build_sp.pro,v $
; Revision 1.1  2009/04/22 19:22:19  antunes
; code relocation part 2 of 3.
;
; Revision 1.6  2009/03/31 14:57:26  antunes
; Re-org prepatory to checking into SSW.
;
; Revision 1.5  2007/11/27 19:57:40  antunes
; Better plotting, more test cases.
;
; Revision 1.4  2007/06/12 19:02:44  antunes
; Added Pixon autoscaling to resolve normalization difficulties.
;
; Revision 1.3  2007/02/23 14:04:57  antunes
; Succeeded in N=256 2-image reconstruction!
;
; Revision 1.2  2006/11/30 20:03:57  antunes
; Updated pixon and other printing and output routines.
;
; Revision 1.1  2006/04/11 15:55:05  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.2  2005/12/27 17:48:24  antunes
; Commented half of the routines using SSW style.  Also added a new
; axes test case 'arrowcube'.
; Checked in the Tomography/old/mikic_img.pro routine here because it
; is a dependency for the general image-prep routine 'get_timage.pro'
;
;-            
 starter  =  '0'

 noshm=KEYWORD_SET(noshm)

 if (n_elements(noct) eq 0) then noct=4; PIXON default
 if (n_elements(npo) eq 0) then npo=2; PIXON default
 if (n_elements(autorange) eq 0) then autorange=-1; turns off autorangeing
; print,noct,npo
 npxn  =  long(noct*npo+1)

sp= {$
anneal_3d: anneal_specs.anneal_3d,       $ ; [] Pixon: anneal factor
reltol:   reltol,          $ ; [] Pixon: cutoff relative change in
abstol:   abstol,          $ ; [] Pixon: cutoff absolute change in
autorange: autorange,      $ ; [] desired max(data) for Pixon, e.g. 1 or 1000
mxit:     mxit,            $ ; [] Pixon: maximum number of iterations per cycle
jsched:   anneal_specs.jsched, $ ; [?] schedule of pixon sizes for anneal loop
msched:   anneal_specs.msched, $ ; [?] schedule of mxit values for anneal loop
rsched:   anneal_specs.rsched, $ ; [?] schedule of reltol values for anneal loop
starter:  starter,         $ ; [] starter image name
noshm:    noshm,           $ ; toggle for using shm nor not, default=use shm
npo:      long(npo),       $
noct:     long(noct),      $
npxn:     npxn             $
}

  return,sp
END

