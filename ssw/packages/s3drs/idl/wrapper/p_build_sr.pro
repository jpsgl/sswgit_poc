; polar can be empty, 'b'rightness', 'n'one, 't'angential, 'r'adial,
; or a string angle value for plane sets
;

FUNCTION p_build_sr,Ncube,gamma,model=model,n0=n0,dvox=dvox,normd=normd,$
                    theta=theta,z0=z0,LL=LL,polar=polar
;+
; $Id: p_build_sr.pro,v 1.1 2009/04/22 19:22:19 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : p_build_sr.pro
;               
; Purpose   : Populates the Pixon 'sr' spatial/observer structure,
;              using a default STEREO A/Cor2 model.
;
;               
; Explanation: Is a wrapper to put user values into the Pixon structures.
;              It puts in the user parameters and also sets
;              'reasonable' defaults for the rest of the structure
;              variables, and also initializes many structure values.
;               
; Use       : IDL> p_build_sr,Ncube
;    
; Inputs    : 
;           Ncube : size of image array
;           model : model name
;           gamma : array of view angles, in degrees
;           n0 : standard electron density (electrons/cc)
;           normd: global normalization for data, i.e. data*normd=what we use
;           (optional) theta _or_ z0: angle above or height above x-y plane
;               
; Outputs   : the 'sr' structure
;
; Keywords  : none
;
; Calls from LASCO : none 
;
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : Simulation
;               
; Prev. Hist. : See Paul Reiser's code and notes.
;
; Written     : Sandy Antunes, NRL, Nov-Dec 2005
;               
; $Log: p_build_sr.pro,v $
; Revision 1.1  2009/04/22 19:22:19  antunes
; code relocation part 2 of 3.
;
; Revision 1.31  2009/04/09 16:40:06  antunes
; GUI 2.0, now handles different installations.
;
; Revision 1.30  2008/11/13 16:23:37  antunes
; Checkin after long hiatus.
;
; Revision 1.29  2008/06/16 18:22:57  antunes
; Minor mods.
;
; Revision 1.28  2008/01/25 16:10:38  antunes
; Major refactoring of code to allow differing FOVs.  Note Pixon
; structures here are now incompatible with earlier versions!
;
; Revision 1.27  2008/01/18 20:03:45  antunes
; More prettification, also one major structure pxwrapper.sr name change.
;
; Revision 1.26  2008/01/15 16:50:08  antunes
; Better units, more tests.
;
; Revision 1.25  2008/01/09 19:24:28  antunes
; Finally fixed the data scaling unit (bug=legacy code used AU not Rsun),
; also added more tests.
;
; Revision 1.24  2008/01/04 20:16:49  antunes
; Final units (hopefully), also more tests and tools.
;
; Revision 1.23  2007/12/07 19:59:12  antunes
; Better tests, plotting, and numerics.
;
; Revision 1.22  2007/11/21 23:12:23  antunes
; Updates to test scripts and diagnostics.
;
; Revision 1.21  2007/10/26 19:02:27  antunes
; Updated difference image needs. (Also made first cut at a back projection
; wrapper).
;
; Revision 1.20  2007/10/03 18:44:17  antunes
; Implemented spherical coordinates in cpixon, major update!
;
; Revision 1.19  2007/09/25 18:52:57  antunes
; Added hshell, also pixon->fits handling.
;
; Revision 1.18  2007/09/04 17:36:27  antunes
; Fixed some machine-dependent configuration issues.
;
; Revision 1.17  2007/08/31 22:10:56  antunes
; Some interesting improvements, and 2 pixon bug fixes.
;
; Revision 1.16  2007/08/29 18:55:08  antunes
; Greatly improved plotting and visualizations.
;
; Revision 1.15  2007/06/12 19:02:44  antunes
; Added Pixon autoscaling to resolve normalization difficulties.
;
; Revision 1.14  2007/05/31 21:02:51  antunes
; More tests, better FITS handling, documentation of geometry.
;
; Revision 1.13  2007/05/29 19:02:18  antunes
; Full process ready for scaled and cropped data with noise.
;
; Revision 1.12  2007/05/17 18:35:28  antunes
; Now handles solar/ecliptic rotation diff between secchi and soho.
;
; Revision 1.11  2007/02/12 18:05:18  antunes
; Modified for better handling of optical and sun center.
; Also generalized some earlier pixon legacy code.
;
; Revision 1.10  2007/01/11 21:09:49  antunes
; Updated the plotting.
;
; Revision 1.9  2007/01/09 20:06:03  antunes
; Test now works with pixon tetra
;
; Revision 1.8  2006/12/06 20:01:15  antunes
; More tweaks, this time adding histograms and better timing.
;
; Revision 1.7  2006/11/30 20:03:57  antunes
; Updated pixon and other printing and output routines.
;
; Revision 1.6  2006/11/17 19:02:32  antunes
; Updates to Pixon structures.
;
; Revision 1.5  2006/11/16 19:33:06  antunes
; Successfully replaced k0 with p_getk0 to allow for pointers
; and thus dynamic image loading.
;
; Revision 1.4  2006/11/16 18:27:33  antunes
; Did a rollback then an update to allow pixon wrapper to handle
; heterogeneous images.
;
; Revision 1.1  2006/04/11 15:55:05  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.5  2006/03/16 14:44:13  antunes
; Axes improvements and better handling of model params.
;
; Revision 1.4  2006/02/16 20:18:54  antunes
; Refactored Reiser TPixon routines into procedures that work with
; my 'generic' Pixon preparation routines.
;
; Revision 1.3  2005/12/27 17:48:24  antunes
; Commented half of the routines using SSW style.  Also added a new
; axes test case 'arrowcube'.
; Checked in the Tomography/old/mikic_img.pro routine here because it
; is a dependency for the general image-prep routine 'get_timage.pro'
;
;-            

if (n_elements(n0) eq 0) then n0=1.0
if (n_elements(chl) eq 0) then chl = 2
if (n_elements(model) eq 0) then model='model'
if (n_elements(normd) eq 0) then normd=1.0

lla0=[1,1,1]*0.5*(Ncube-1)  ; sun at center of image cube, default
L0    = 214.943           ; 1au in solar radii, default
interp = 3; only 1 or 3 work for plane polarized.  The others should in theory work fine for brightness images.  Only 3 and 5 have been tested.  1 is the IDL version of 3 (3 being a compiled C version).
;if (getenv('disumlib') eq '') then interp=1; switch to IDL version if no C lib
psp   = -1
;Istd  = 0.0


; ----- check for problems

Nd=n_elements(gamma)       ; number of data frames
if (n_elements(polar) eq 0) then begin
    polar = make_array(Nd,/string,value='b')
    fan = make_array(Nd,/long,value=0); initialize
end else begin
  ; polar can be singular or array, can be empty, 'b'rightness', 'n'one,
  ; 't'angential, 'r'adial, or a numeric value for plane sets
  ; only processing needed is to extract angles if it is a plane
    fan = make_array(Nd,/long,value=0); initialize
    if (n_elements(polar) eq 1) then $
      polar=make_array(Nd,/string,value=polar)
    for i=0,n_elements(polar)-1 do begin
        if (polar[i] eq '0' or polar[i] eq '120' or $
            polar[i] eq '240') then begin
            fan[i]=long(polar[i])
            polar[i]='p'
        end
    end
end
  

; we use a rotation in our usual rendering, so we'll need to track
; the sun position in the rotated frame for cases where we are
; not using a sun-centered cube. So for now, default to sun-centered:
rot_lla0=ptrarr(Nd)
for i=0,Nd-1 do rot_lla0[i]=ptr_new(lla0)

; values for roco are 0 (no z-axis rotation and no gamma/theta shift,
; so only in-plane images allowed), 1 (direct z-axis view, a Paul hack),
; or -1 (use full spherical geometry at some performance cost, allows
; theta not in plane.)

roco  = make_array(Nd,/long,value=-1) ; only in OPIXON

eps   = make_array(Nd,/long,value=0)
;;set   = make_array(Nd,/long,value=0)
as    = make_array(Nd,/long,value=1)

; we use 'mytheta' here as a local copy in order to assure
; theta and gamma always match in terms of order.
if (n_elements(theta) eq 0) then $
  mytheta = make_array(Nd,/double,value=0.0) else mytheta=theta[0:Nd-1]

if (n_elements(z0) eq 0) then $
  z0    = make_array(Nd,/double,value=0.0)
if (n_elements(LL) eq 0) then $
  LL    = make_array(Nd,/double,value=1.0); satellite distances

;k0    = make_array(2,Nd,value = 0.5*(Ncube-1),/double)
k0    = make_array(Nd,/ptr)
sunk0    = make_array(Nd,/ptr)
for m=0,Nd-1 do k0[m] = ptr_new([0.5*(Ncube-1),0.5*(Ncube-1)])
for m=0,Nd-1 do sunk0[m] = ptr_new([0.5*(Ncube-1),0.5*(Ncube-1)])
;for m=0,Nd-1 do k0[m] = ptr_new([0.5*(Ncube-1),0.5*(Ncube-1) + z0[m]])
;for m=0,Nd-1 do sunk0[m] = ptr_new([0.5*(Ncube-1),0.5*(Ncube-1) + z0[m]])

Ndata = make_array(Nd,/long,value=Ncube); defaults to data N = cube N

fov_radius_in_rsun=15.0
;tau=0 ; typically 0 or 30.0/N/214.943 or 60.0/N/214.943 or 1.0e-10
L0tau = make_array(Nd,/double,value=fov_radius_in_rsun/(0.5*Ncube))
tau = make_array(Nd,/double,value=fov_radius_in_rsun/(Ncube*L0));
; default is Voxel size matches 1 Pixel FOV (dvox and L0tau both are
; in units of solar radii)
if (n_elements(dvox) eq 0) then dvox  =  L0tau[0]
if (dvox le 0) then dvox=L0tau[0]

; note that eps is a shift, in degrees, away from the sun along
; the x-y plane (no vertical component)
set = sin(eps)/tau
;print,'theta: ',theta
;print,'tau: ',tau
;print,'eps: ',eps
;print,'set: ',set

; temporary placeholders
exptime=make_array(Nd,/float,value=1)
ecliptic=make_array(Nd,/float,value=1)
align=make_array(Nd,/string,value='ecliptic')
MSBperDN=make_array(Nd,/float,value=1)
DNperPhoton=make_array(Nd,/float,value=1)
biasdev=make_array(Nd,/float,value=0.0)
tele_name = make_array(Nd,/string)
for i=0,Nd-1 do tele_name[i]=string(i+1)
sat_name = make_array(Nd,/string,value='')
detector = make_array(Nd,/string,value='')
date_obs = make_array(Nd,/string,value=systime())
;xyz_coords = ptr_new(make_array(3,/float,value=0.0))
hae_coords = make_array(Nd,/ptr)


; even at this point, put in good guess at HAE coords
for m=0,Nd-1 do begin
    pixoncoords,xyz,gamma[m],mytheta[m],LL[m],/au,/reverse
;    print,gamma[m],theta[m],LL[m],xyz
    hae_coords[m] = ptr_new(xyz)
end

imodel = make_array(Nd,/long,value=10); 0=generic, 1=rough core1 model, 2=C3 model (undefined for now), 10=pure geometry
;imodel = 2      
;n0    = 1;1.0e8
;Istd  = 0.0
Istd = make_array(Nd,/float,value=0.0);
;as    = [1,1]

scaling = make_array(Nd,/float,value=1.0); used later when loading data

err=0
if(n_elements(LL)    ne Nd)then err=1
if(n_elements(z0)    ne Nd)then err=2
if(n_elements(eps)   ne Nd)then err=3
if(n_elements(set)   ne Nd)then err=4
if(n_elements(polar) ne Nd)then err=5
;if(n_elements(k0[0,*])  ne Nd)then err=7
if(n_elements(fan)   ne Nd)then err=8
if(n_elements(roco)  ne Nd)then err=9
;q=where((roco ne 0) and (roco ne 1),c)
;if(c gt 0)then err=10
;if(n_elements(dvox) ne 1)then err=11

q=where(finite(z0) eq 0,count)
if(count gt 0) then err=11

if(err ne 0)then begin                  &$
   print,'Error ',err,' - build_input ' &$
   stop                                 &$
end

if((interp ne 4) and (n_elements(psp) eq 0))then psp=-1

; ----- set variables to double or long

;Ncube      = long(Ncube)
;Ndata      = long(Ndata)
lla0   = double(lla0)
LL     = double(LL)
;tau    = double(tau)

roco   = long(roco)

gamma  = double(gamma)
rho  = double(gamma)
mytheta = double(mytheta)
phi = double(mytheta)

;k0     = double(k0)
z0     = double(z0)
dvox   = double(dvox)
L0     = double(L0)
;L0tau  = double(L0tau)
eps    = double(eps)
set    = double(set)
n0     = double(n0)
psp    = double(psp)
;wccd   = double(wccd)
fan    = double(fan)
as     = double(as)

origres=2048; default to SECCHI

limbdark=fltarr(Nd)+1.0; just a placeholder for now

; ----- load input structure. comments indicate nature of the variable.
;       [] means scalar, [?] means variable no. of elements

; ----- Physics and Geometry

sr= {$
Ncube:      long(Ncube),           $ ; [] Image array is NxNxN
Nd:         long(Nd),              $ ; [] Number of data frames
chl:        chl,             $ ; [] sun model
ch:         model,           $ ; [] true image specification
dvox:       dvox,            $ ; [] length of voxel edge in Solar Radii (change from Paul's 'd' distance/(L0*tau) 
L0:         L0,              $ ; [] average sun-satellite distance (Solar Radii)
interp:     interp,          $ ; [] 2: no pixel interp 3: with pixel interp psp=1 4: same,use psp, 0=no interp and done in IDL not C, 1=interp in IDL not C
psp:        psp,             $ ; [] data psf: only used if interp=4: (search on disum2 and disumt2)
n0:         n0,              $ ; [] standard electron density (electrons/cc), converts from gm/cm^3 to normalized e- density
normd:      normd,           $ ; [] global norm, data*normd = what we use
lla0:       lla0,            $ ; [3] sun center in image coordinates, N-dependent, ignored for RTWL
an:         dblarr(3),       $ ; [3] Polynomial coefficients for limb darkening 
Ndata:      long(Ndata),           $ ; [Nd] Data array is NxNxNd
scaling:    scaling,         $ ; [Nd] factor scaling original data to our Ndata
k0:         k0,              $ ; [Nd] pointers to optical center of detector in data coordinates (formerly was k0=[2,Nd])
sunk0:      sunk0,           $ ; [Nd] pointers to sun in data coordinates
tau:        tau,             $ ; [Nd]  width of a pixel divided by pinhole-ccd distance 
L0tau:      L0tau,           $ ; [Nd] distance subtended by a pixel at L0 (Solar Radii), N-dependent, ignored for RTWL
imodel:     imodel,          $ ; [Nd] instrument model for Istd (see get_istd_07.pro)
Istd:       Istd,            $ ; [Nd] Multiply norm data by this to get intensity at detector (photons/sec/cm^2/sr/A)
limbdark:   limbdark,        $ ; [Nd] Optional storage of limb darkening ratio used
polar:      polar,           $ ; [Nd] polarization for each satellite
fan:        fan,             $ ; [Nd] filter angles for plane polarization data frames 
LL:         LL,              $ ; [Nd] sun-satellite distance/L0
gamma:      gamma,           $ ; [Nd] angle of satellite from x-axis
rho:        rho,             $ ; [Nd] better name for 'gamma'
theta:      mytheta,           $ ; [Nd] polar theta angle of satellite ONLY TPIXON OR CPIXON_roco=-1
phi:        phi,             $ ; [Nd] better name for 'theta'
roco:       roco,            $ ; [Nd] in dsf, dsft first execute - ONLY OPIXON
z0:         z0,              $ ; [Nd] height of satellite above xy plane/(L0*tau)
rot_lla0:   rot_lla0,        $ ; [Nd] ptr array of rotated sun centers
eps:        eps,             $ ; [Nd] pointing angle from sun center
set:        set,             $ ; [Nd] sin(eps)/tau
ecliptic:   ecliptic,        $ ; [Nd] angle between solar north and ecliptic north, CCW from ecliptic north, in degrees.
align:      align,           $ ; [Ns] either 'ecliptic' or 'solar', useful for aligning disparate images to ecliptic north.
exptime:    exptime,         $ ; [Nd] exposure time for data, in seconds
MSBperDN:   MSBperDN,        $ ; [Nd] conversion units if avail for noise calc
DNperPhoton:DNperPhoton,     $ ; [Nd] conversion units if avail for noise calc
biasdev:     biasdev,        $ ; [Nd] amplifier noise, or 0
tele_name:  tele_name,       $ ; [Nd] telescope name, e.g. SOHO or SECCHI, useful for case and switch stat
detector:   detector,        $ ; [Nd] instrument names, esp. for noise models and masks
date_obs:   date_obs,        $ ; [Nd] reference date from data
hae_coords: hae_coords,      $ ; [Nd] from data, saved here for reference
sat_name:   sat_name,        $ ; [Nd] satellite names, formerly in sc
as:         as,              $ ; [Nd] aperture area divided by pixel area 
trW:        dblarr(4,4),     $ ; [4,4] normalized W matrix
trRRs:      dblarr(4,4,Nd),  $ ; [4,4,Nd] normalized RRs matrices
trP:        dblarr(3,4),     $ ; [3,4] normalized P matrix
trVs:       dblarr(3,3,Nd)  $ ; [3,3,Nd] normalized Vs matrix
}

; ----- PIXON fit specification

 return,sr
END
