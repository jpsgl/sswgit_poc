;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : p_report
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;               
; reconstruction run report, generating a text summary of the
; various run choices and parameters.  Useful for cover pages.
;
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; reconstruction run report
;
; Prints output to a file.
; If /echo, also echos it to onscreen.
; Can also sets an optional string array variable, returntext
;
; Optional '/savedata' plus oPixon also saves the pxwrapper and oPixon
; object (if no oPixon given, will not save)
;
; /dump also dumps the entire pxwrapper.sr and .sp structures to
; show the full set of all parameters for the given run.
;
; /nosave is used if you do not want to actually save the results,
; just view them.  For example, "p_report,pxwrapper,/dump,/echo,/nosave"
; will echo all the pxwrapper geometry and other structures to screen.
;
; infile=FILENAME is a strange beastie that, if given, restores
; that save file (presumed to contain a pxwrapper and, optionally,
; oPixon, perhaps as made by a /savedave use), then produces the report.
; It will gracefully fail if given a pxwrapper entity, to avoid
; overwriting info.  A sample usage to just peek at a saved file:
;    p_report,/dump,/echo,/nosave,infile='recon_32_May31_2007_v01.dat'
;
; /longline returns the report with longer lines and few line breaks,
; thus being more suitable for figure captions
;
; /terse returns just a short subset, useful for identifying plots
;
; if given filename=filename, writes the text report to that filename
; in addition to all usual and extra outputs.  If /nosave is included,
; it will still write to 'filename' as /nosave means to ignore the
; default versioned save.
;
; Can give it individual noise adjustments (e.g. from GUI), otherwise
; it assumes the global structure one is valid.
;

PRO p_report,pxwrapper,oPixon,$
             runname=runname,echo=echo,returntext=sq,filename=filename,$
             version=version,savedata=savedata,dump=dump,nosave=nosave,$
             infile=infile,fontsize=fontsize,longline=longline,terse=terse,$
             noises=noises
             

   echo=KEYWORD_SET(echo)
   dump=KEYWORD_SET(dump)
   longline=KEYWORD_SET(longline);
   terse=KEYWORD_SET(terse)
   nosave=KEYWORD_SET(nosave)
   if (nosave) then save=0 else save=1; default is 'save'
   savedata=KEYWORD_SET(savedata)

   if (n_elements(infile) ne 0) then begin
       if (n_elements(pxwrapper) eq 0) then begin
           if (file_exist(infile)) then begin
               restore,/verbose,infile
           end else begin
               print,'* No action taken, file does not exist: ',infile
               return
           end
       end else begin
           print,$
"* p_report cannot take both a 'pxwrapper' and an 'infile', no action taken."
           return
       end
   end

   if (savedata eq 1 and n_elements(oPixon) eq 0) then savedata=0; ditto

   if (n_elements(pxwrapper) eq 0) then begin
     print,'* No data to report on, returning gracefully with no action taken.'
     return
   end

; End of input checking, time to begin actual reporting.
   

   ; translation from o_flimb.pro
   sunmodels = ['no limb darkening, point sun',$
                'polynomial limb darkening, point sun',$
                'polynominal limb darkening, extended sun','new test model']

  if (n_elements(runname) eq 0) then runname = 'recon'

  Ncube=pxwrapper.sr.Ncube
  Ndata=pxwrapper.sr.Ndata
  n0=pxwrapper.sr.n0
  
  if (n_elements(version) ne 0) then begin
      ; user wants to force a specific version number, so go with it
      txtname = p_encodename(Ncube,runname,force=version,stem='.txt')
      datafile = p_encodename(Ncube,runname,force=version,stem='.dat')
      getversion=version
  end else begin
      ; establish our own version, then use it for the rest of the files
      txtname = p_encodename(Ncube,runname,getversion=getversion,stem='.txt')
      datafile = p_encodename(Ncube,runname,force=version,stem='.dat')
  end

  R = pxwrapper.sr.LL
  phi = pxwrapper.sr.phi
  rho = pxwrapper.sr.rho
  z0 = pxwrapper.sr.z0
  Nviews = pxwrapper.sr.Nd
;  Nviews = n_elements(R)
  Polarizations = pxwrapper.sr.polar
  if (tag_exist(pxwrapper.sr,'sat_name')) then begin
      ; standard format
      satellites = pxwrapper.sr.sat_name
      telescopes = pxwrapper.sr.tele_name
      detectors = pxwrapper.sr.detector
  end else if (tag_exist(pxwrapper.sc,'sat_name')) then begin
      ; earlier Paul format
      satellites = pxwrapper.sc.sat_name
      telescopes = ''
      detectors = ''
  end else begin
      ; failsafe
      satellites=make_array(Nviews,/string,/index)
      telescopes = ''
      detectors = ''
  end

  date = systime()
  runtime = pxwrapper.sc.totaltime/60.0; in minutes
  machine = pxwrapper.sc.machine
  gname = pxwrapper.sc.gname
  note = pxwrapper.sc.note

  if (tag_exist(pxwrapper.sc,'chisq')) then begin
      ; standard format
      chisq = pxwrapper.sc.chisq
      Q = pxwrapper.sc.q        ; Q is just (chisq - dof)/(2.0*dof)
      pxncnt=pxwrapper.sc.pcount
      niterations = pxwrapper.sc.iterations
  end else begin
      ; earlier Paul format
      chisq = -1
      Q = -1
      pxncnt=0
      niterations = pxwrapper.sp.mxit
  end

  noisemod = pxwrapper.sn.noisemod
  noise1=pxwrapper.sn.ftol[0]
  noise2=pxwrapper.sn.ftol[1]

  isunmodel = pxwrapper.sr.chl
  sunmodel=sunmodels[isunmodel]

  nlines=26+Nviews; 20 generic lines, 1 line per view, plus padding
  if (longline) then nlines=10+Nviews; only 9 generic lines plus 1/view and pad
  if (dump) then nlines=nlines + 4 + (3*Nviews) + $
    n_tags(pxwrapper.sr)+n_tags(pxwrapper.sp)
  if (terse) then nlines=3

  ; now store it all
  sq = strarr(nlines)

  if (terse) then begin
      ; special terse format
      sq[0] = string(Ncube,Nviews,strtrim(string(floor(runtime)),2),$
         format='("Ncube: ",i-3," Nviews: ",i-2," runtime: ",a6," minutes"/)')
      sq[0] = sq[0] + '   Polarizations: '
      sq[1] = ''
      for m=0,Nviews-1 do begin
          prho=string(rho[m],format='(" (",d6.2,",")')
          pphi=string(phi[m],format='(d-6.2,")  ")')
          sq[1]= sq[1] + prho + pphi
          if (Polarizations[m] eq 'b') then Polarizations[m]='tB'
          ppolar=string(Polarizations[m],format='(" ",a3," ")')
          sq[0] = sq[0] + ppolar
      end
      sq[2] = string(niterations,chisq,Q,pxncnt,$
                     format=$
 '(i-6," steps, chisq = ",e-11.3," Q = ",e-12.4," Pixon count = ",f-10.2/)')
      sqc=3-1; index of last useful element

  end else begin

      sqc=0
      sq[sqc] = 'Filename: ' + pxwrapper.sc.gname
      ++sqc
      sq[sqc] = "Run: " +pxwrapper.sr.date_obs[0] + '    '
      sq[sqc] = sq[sqc] + string(date,format='("Today: ",a/)')
      if (longline eq 0) then ++sqc
      sq[sqc] = sq[sqc] + $
        string(Ncube,Nviews,format='("   Ncube: ",i-4,"   Nviews: ",i-2/)')
      ++sqc

;      sq[sqc++]=''; stores blank then increments
;      for m=0,Nviews-1 do begin
;          sq[sqc]=sq[sqc] + string(m+1,Ndata[m],$
;                   format='("Frame ",i-2," has ndata: ",i-4,/)')
;          if (longline eq 0) then ++sqc
;      end

      sq[sqc++]=''              ; stores blank then increments

      names = satellites + '/' + telescopes + '/' + detectors

      if (longline eq 0) then begin
          sq[sqc] = string("View:  ")
          for m=0,Nviews-1 do $
            sq[sqc] = sq[sqc]+string(names[m],format='(a-17," ")')
          ++sqc
          sq[sqc++]=''          ; stores blank then increments
          sq[sqc] = string("Ndata: ")
          for m=0,Nviews-1 do $
            sq[sqc] = sq[sqc]+string(Ndata[m],format='(d-17.1," ")')
          ++sqc
          sq[sqc] = string("R:     ")
          for m=0,Nviews-1 do $
            sq[sqc] = sq[sqc]+string(R[m],format='(d-17.2," ")')
          ++sqc
          sq[sqc] = string("Rho:   ")
          for m=0,Nviews-1 do $
            sq[sqc] = sq[sqc]+string(rho[m],format='(d-17.2," ")')
          ++sqc
          sq[sqc] = string("Phi:   ")
          for m=0,Nviews-1 do $
            sq[sqc] = sq[sqc]+string(phi[m],format='(d-17.2," ")')
          ++sqc
;          sq[sqc] = string("(z0):  ")
;          for m=0,Nviews-1 do $
;            sq[sqc] = sq[sqc]+string(z0[m],format='(d-17.1," ")')
;          ++sqc
          sq[sqc] = string("Polar: ")
          for m=0,Nviews-1 do sq[sqc] = $
            sq[sqc]+string(Polarizations[m],format='(a-17," ")')
          ++sqc
      end else begin
                                ; alternative format, 1 line per obs
          sq[sqc]=$
'Viewpoint         Ndata    R(AU)   Rho        Phi            Polar'
          ++sqc
          for m=0,Nviews-1 do begin
              pviewpoint=string(names[m],format='(a15," ")')
              pndata=string(Ndata[m],format='(i5,"     ")')
              pr=string(R[m],format='(d5.2,"   ")')
              prho=string(rho[m],format='(d7.2,"   ")')
              pphi=string(phi[m],format='(d7.2,"   ")')
;              pz0=string(z0[m],format='(d7.4,"   ")')
              ppolar=string(Polarizations[m],format='(a6," ")')
;              sq[sqc] = pviewpoint + pndata + pr + prho + pphi + pz0
;              + ppolar
              sq[sqc] = pviewpoint + pndata + pr + prho + pphi + ppolar
              ++sqc
          end
      end
      
      sq[sqc++]=''              ; stores blank then increments
      
      sq[sqc] = string(niterations,chisq,$
                format='("Iterations: ",i-8,", Chisq = ",e-11.3/)')
      ++sqc
      sq[sqc] = sq[sqc] + $
        string(Q,pxncnt,format='("Q = ",e-12.4,", Pixon count = ",f-10.2/)')
      ++sqc
      
      sq[sqc++]=''              ; stores blank then increments
;      sq[sqc] = string(noisemod,noise1,noise2,$
;                      format='("Noise model: ",a,", parameters: ",2(" ",f)/)')
      sq[sqc] = string(noisemod,format='("Noise model: ",a/)')
      ++sqc
      
      sq[sqc] = sq[sqc] + string(sunmodel,format='("Sun model: ",a/)')
      ++sqc

      sq[sqc++]=''              ; stores blank then increments
      
      sq[sqc] = string(machine,runtime,$
                     format='("Machine: ",a12,4x,"runtime: ",a10," minutes"/)')
      if (longline eq 0) then ++sqc
      
      sq[sqc++]=''              ; stores blank then increments

      sq[sqc] = sq[sqc] + string("Notes: ",gname)
      if (longline eq 0) then ++sqc
      sq[sqc] = sq[sqc] + string("          ",note)

      ++sqc
      sq[sqc] = 'Noise scaling is '
      if (n_elements(noises) eq 0) then noises=pxwrapper.sc.nadjust
      for icn=0,n_elements(noises)-1 do sq[sqc] += int2str(noises[icn])+' '

      ; 'if' added here for backwards compatibility with earlier versions
      ; we also parse the full explanation into a more human-readable sentence
      if (tag_exist(pxwrapper.sc,'endcause')) then begin
          ++sqc
          case1='reached Q < 1'
          case2='was no longer successively improving Q'
          case3='gradient is 0'
          case4='reached maximum iterations'
          if (strmatch(pxwrapper.sc.endcause,'*qtol*')) then $
            endcause=case1 else $
            if (strmatch(pxwrapper.sc.endcause,'*gradient*')) then $
            endcause=case3 else $
            if (strmatch(pxwrapper.sc.endcause,'*err*')) then $
            endcause=case2 else endcause=case4
          for i=0,n_elements(causes)-1 do $
            if (causes[i] eq 'err') then endcause += 'not successively improving Q'
          sq[sqc] = 'Ended because it ' + endcause
      end

;  print,'num lines = ',sqc

      if (dump) then begin
          sq[sqc++]='  '
          sq[sqc++]='  '
          sq[sqc++]='*** SR ***'
          tags=tag_names(pxwrapper.sr)
          for i=0,n_elements(tags)-1 do $
            if (datatype(pxwrapper.sr.(i)) ne 'PTR' and $
                stregex(tags[i],'^TR') eq -1) then $
            sq[sqc++]=string(tags[i] + " = ",trim(string(pxwrapper.sr.(i)),2))
          for i=0,n_elements(pxwrapper.sr.k0)-1 do $
            sq[sqc++]=string('K0 = ',*pxwrapper.sr.k0[i])
          for i=0,n_elements(pxwrapper.sr.sunk0)-1 do $
            sq[sqc++]=string('SUNK0 = ',*pxwrapper.sr.sunk0[i])
          for i=0,n_elements(pxwrapper.sr.hae_coords)-1 do $
            sq[sqc++]=string('HAE_COORDS = ',*pxwrapper.sr.hae_coords[i])
          sq[sqc++]='*** SP ***'
          tags=tag_names(pxwrapper.sp)
          for i=0,n_elements(tags)-1 do $
            sq[sqc++]=tags[i]+" = "+trim(string(pxwrapper.sp.(i)),2)
      end

  end
      
  if (save) then begin
      openw,out,txtname,/get_lun
      printf,out,sq
      free_lun,out
      print,'Report is in ',txtname
  end

  if (n_elements(filename) ne 0) then begin
      openw,out,filename,/get_lun
      printf,out,sq
      free_lun,out
      print,'Report is in ',filename
  end

  if (echo) then for m=0,sqc do print,strtrim(sq[m])

  if (savedata) then begin
      save,file=datafile,pxwrapper,oPixon
      print,'Data saved to ',datafile
  end
      
END
