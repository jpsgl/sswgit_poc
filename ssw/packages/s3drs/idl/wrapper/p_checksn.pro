;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : p_checksn
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;               
; simple routine to plot some S/N ratios
;
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; simple routine to plot some S/N ratios

PRO p_checksn,data,noise,masks,brackets=brackets,header=header,ps=ps,snr=snr,$
              max=max,maskcheck=maskcheck,wsize=wsize,nonoise=nonoise

    brackets=KEYWORD_SET(brackets)
    nonoise=KEYWORD_SET(nonoise)
    maskcheck=KEYWORD_SET(maskcheck)
    force=KEYWORD_SET(force)
    if (n_elements(snr) eq 0) then snr=1.0
    if (n_elements(wsize) eq 0) then wsize=128

    if (n_elements(masks) eq 0) then masks=data*0.0+1.0

    Nd=n_elements(data[0,0,*])

    sn=ratio(data,noise)*masks > snr

    if (maskcheck) then begin

        ; white = good, either masked or data is non-zero
        ; black = bad, data is 0 but not masked

        ; replace mask with an overlay showing the mask clearly
        m2 = ~masks; where mask exists is now set to 1
        m2 = m2 + 1; where mask exists is set to 2
        tv_multi,m2,header='masks'

        m3=m2*0 + 1; make a new blank entity
        ie=where(data le 0)
        if (ie[0] ne -1) then m3[ie]=0; where data is <0 is set to 0
        tv_multi,m3,header='zero data'
        ;m2 = m2 + 5*max(sn)
        ;mset=sn+m2
        mset=m2+m3; very white = good data masked, grey = zero is masked, black = bad because data is zero but not masked
        tv_multi,mset,header='reverse'
    end else begin
        mset=masks
    end

    tsetd=make_array(/string,Nd,value='Data')
    tsetn=make_array(/string,Nd,value='Noise')
    tsetm=make_array(/string,Nd,value='Masks')
    tsets=make_array(/string,Nd,value='S/N')
    
    if (nonoise) then tset=[tsets,tsetm,tsetd] else $
      tset=[tsets,tsetm,tsetn,tsetd]

    if (nonoise) then dset=[[[sn]],[[mset]],[[data]]] else $
      dset=[[[sn]],[[mset]],[[noise]],[[data]]]
          
    if (brackets) then begin
        tsetep = make_array(/string,Nd,value='S - Noise')
        tsetem = make_array(/string,Nd,value='S + Noise')
        tset=[tsetep,tsetem,tsets,tsetd]
        dset=[[[data-noise]],[[data+noise]],[[sn]],[[data]]]
    end

    tv_multi,title='data/noise/masks/sn',labels=tset,header=header,$
      dset,fixed=wsize,/raw,/log,/unique,line=Nd,/showrange,oom=7,$
      ps=ps,max=max

END
