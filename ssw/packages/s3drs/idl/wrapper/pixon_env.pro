;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : pixon_env
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;
; Sets up the Pixon runtime environment so all routines are findable
; and loadable.
;               
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; Now changed so /local is default unless told /original
; Much of the software is checked into /dev so minimal paths are needed.
;
;
; WARNING!  If you want to use both CPixon and TPixon in the same
; session, you must either initially load both here (/both) OR
; make sure that TPixon is loaded _before_ CPixon.  Otherwise,
; CPixon will fail.
; You can always just load '/both' to avoid clashes.
;
; If you use the /local flag, it will use Sandy's local copy
; rather than the NRL-installed version.  After we get a new
; license, this can be changed (but note that any code changes
; in the new code must then be hand-loaded.)
;
; You can optionally give 'disumlib=' with the path to the compiled
; forms of the renderer and back projection, if the 'disumlib'
; environment is not yet set to point to these on your system.
;
; I have stripped down the earlier one (archived below) to what should
; be the only set needed, running classic pixon locally.
;

PRO pixon_env,disumlib=disumlib,$
              pixontype,force=force,both=both,local=local,original=original


; We have fast compiled versions of the rendering and backproject.
; We try to find the libraries if they are in the path.
; Or put in path here for compiled objects 'disum1.so','disumt1.so',
;   'disum3.so','disumt3.so'.  If these do not exist, you'll
;   need to use the IDL-only routines (which are slower).
;
  if (getenv('disumlib') eq '') then begin
      if (n_elements(disumlib) eq 0) then disum_so_loc
      if (getenv('disumlib') eq '') then $
        setenv,'disumlib=/net/mercury/data1/pixxon/Tomography/old/'
  end

  pixontype='c'
  if (n_elements(local) eq 0) then local=1

; first, check if we need to add this env in
  loadme=0                      ; presume false
  defsysv,'!pixontype_loaded',exist=exist

;print,'existing environment exists? ',exist
  if (exist eq 0) then begin

    ; first time run = guaranteed that we have to set the env

      defsysv,'!pixontype_loaded',pixontype
      loadme=1                  ; actually set environment
      
      common debugs,phys,tdsf,tdsft,dbg

      dbg=-1.0
      tdsf=0
      tdsft=0
      phys=1

      if (pixoncheck() eq 0) then begin
          print,'Warning, cannot find licensed Pixon in your path.'
          ; USING LOCAL UNPGPED COPY
          antuneshome='/home/antunes/pixonsrc/oopixon'
          antuneslic=antuneshome+'/pxnlicense/'+PIXONCHECK+'.pro'
          if (file_exist(antuneslic)) then begin
              add_path,antuneshome,/expand ; for 'c'
              print,'Using antunes developer copy'
          end
      end

      if (local) then begin
        ; path uses source code, no compiled image needed
      end else begin
          restore,'/net/mercury/data1/pixxon/oopixon_11.3/oopixon_11.3_6.2.sav'
          resolve_routine,'uniq',/is_function ; necessary since the
           ; above restore overwrites the 'uniq' needed by fitshead2struct
;;; REMOVE THIS ONCE LICENSE UPDATED
          restore,'/net/mercury/data1/pixxon/temp/pxnlicense__init.sav'
      end

      pxndsf_force              ; forces compilation of object
      pxndsft_force             ; forces compilation of object

    ;'/home/antunes/pixonsrc/oopixon/pxnlicense/pxnlicense__init.pro'
    ; minor hack here to update license if using *.sav version
      if (local eq 0) then $
        resolve_routine,'pxnlicense::init',/is_function

  end

END


PRO pixon_env_older,pixontype,force=force,both=both,local=local,$
                    original=original,disumlib=disumlib

; Project   : developer area for STEREO SECCHI
;                   
; Name      : pixon_env
;               
; Purpose   : Sets up shared Pixon environments
;               
; Explanation: still in debugging stage
;               
; Use       : IDL> newpixon_env
;    
; Inputs    : pixontype
;               
; Outputs   : none
;
; Keywords  : pixontype : 'c' or 't' (required)
;             /force : forces path update even if previously set
;             /both : loads both 'c' and 't' paths so allow sharing
;            (note that /both is the same as calling with 'c/t',
;              then again with 't/c',/force)
;               
; Calls from LASCO : none 
;
; Common    : establishes the common blocks used by other Pixon routines
;               debugs,phys,tdsf,tdsft,dbg
;               pixonrun,sr,sc,sp,sn,stuff
;             Also, its own common is defined to check this only is
;             called once
;               pixonreentrant,loaded
; Restrictions: none
;               
; Side effects: changes environment and path, sets some common variables
;               
; Category    : Simulation
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRL, Nov-Dec 2005
;               

both=KEYWORD_SET(both)
force=KEYWORD_SET(force)
;local=KEYWORD_SET(local)
original=KEYWORD_SET(original)
local=~original

; We have fast compiled versions of the rendering and backproject.
; We try to find the libraries if they are in the path.
; Or put in path here for compiled objects 'disum1.so','disumt1.so',
;   'disum3.so','disumt3.so'.  If these do not exist, you'll
;   need to use the IDL-only routines (which are slower).
;
if (getenv('disumlib') eq '') then begin
    if (n_elements(disumlib) eq 0) then disum_so_loc
    if (getenv('disumlib') eq '') then $
      setenv,'disumlib=/net/mercury/data1/pixxon/Tomography/old/'
end

if (n_elements(pixontype) eq 0) then pixontype='n'
if (both eq 0 and pixontype ne 't' and pixontype ne 'c' and $
                 pixontype ne 'b') then begin
    print,"Must call pixon_env,'c' or pixon_env,'t' or pixon_env,/both"
    stop
end

; 'loaded' can be 'c', 't' or 'b' (both)
; three cases: load 'c' if not 'c' or 'b', load 't' if not 't' or 'b',
;   load both 'b' if not 'b'

; first, check if we need to add this env in
loadme=0; presume false
defsysv,'!pixontype_loaded',exist=exist

;print,'existing environment exists? ',exist
if (exist eq 0) then begin

    ; first time run = guaranteed that we have to set the env
    defsysv,'!pixontype_loaded',pixontype
    if (both) then !pixontype_loaded = 'b'
    loadme=1; actually set environment

end else if (!pixontype_loaded ne 'b') then begin

    ; one environment is loaded, see if we need to load another
    if (!pixontype_loaded ne pixontype) then begin
        ; we have one set and need the other, so load
        !pixontype_loaded = 'b'
        loadme=1
    end

end else begin

    ; do nothing
    ;print,'no-action type is ',!pixontype_loaded,loadme,force

end

if (force eq 1) then loadme=1; final condition, if force do it anyway
;if (loadme) then print,'Loading ',pixontype,',',!pixontype_loaded else print,'Not loading ',pixontype,',',!pixontype_loaded

; only run if environment has not yet been set

if (loadme) then begin

    common debugs,phys,tdsf,tdsft,dbg
;    common pixonrun,sr,sc,sp,sn,stuff

    dbg=-1.0
    tdsf=0
    tdsft=0
    phys=1

    if (local) then begin
        ; USING LOCAL UNPGPED COPY
        if (pixontype eq 'c' or both eq 1) then $
          add_path,'/home/antunes/pixonsrc/oopixon',/expand ; for 'c'
;          add_path,'/net/mercury/data1/antunes/oopixon',/expand ; for 'c'

        if (pixontype eq 't' or both eq 1) then $
          add_path,'/home/antunes/pixonsrc/toopixon',/expand ; for 't'
    end else begin
        if (pixontype eq 'c' or both eq 1) then $
          add_path,'/net/mercury/data1/pixxon/oopixon_11.3',/expand ; for 'c'
        if (pixontype eq 't' or both eq 1) then $
          add_path,'/net/mercury/data1/pixxon/toopixon_2.0pre4',/expand;for 't'
    end

; we put both 'c' and 't' paths for simplicity here, rather than via switch
;;;; removed Tomography/old in the hopes that we now have
;;;; all those routines necessary in our default disum/ pathway.
;;;;    add_path,'/net/mercury/data1/pixxon/Tomography/old' ; for both

;    add_path,'/net/mercury/data1/pixxon/NRLcode',/expand ; for both
;   also should be in the default 'dev' path by now

    ;setenv,'PIXON  =/net/mercury/data1/pixxon'

;    print,getenv('PIXHERE')

    if (pixontype eq 't' or both eq 1) then begin

        thome=getenv('HOME')
        tempdir = thome+'/temp' ;
        if (file_test(tempdir,/dir)) then thome=tempdir

        setenv,'PIXHERE='+thome ; where put main files 
        setenv,'PIXSAVE='+thome ; where to put detailed

; tetrahedral ('new') pixon requires the next lines

        add_path,'/net/mercury/data1/pixxon/oopixon_11.3/util' ; for 't'

      ; first restore saved TPixon image, then replace specific routines
        if (local) then begin
            ; path uses source code, no compiled image needed
        end else begin
          restore,'/net/mercury/data1/pixxon/toopixon_2.0pre4/toopixon_2.0pre4_6.2.sav'
;;; REMOVE THIS ONCE LICENSE UPDATED
            restore,'/net/mercury/data1/pixxon/temp/tpxnlicense__init.sav'
      end

     ; force compilation of Paul's tpxnproj__jpol object
;      resolve_routine,'tpxn_render',/COMPILE_FULL_FILE
        tpxn_render             ;
        coord_pxn        ; force compilation of pixon-specific 'coord'

    end

    if (pixontype eq 'c' or both eq 1) then begin

; older object-oriented classic pixon requires the next four lines

        if (local) then begin
            ; path uses source code, no compiled image needed
        end else begin
            restore,'/net/mercury/data1/pixxon/oopixon_11.3/oopixon_11.3_6.2.sav'
            resolve_routine,'uniq',/is_function; necessary since the
                    ; above restore overwrites the 'uniq' needed
                    ; by fitshead2struct
;;; REMOVE THIS ONCE LICENSE UPDATED
            restore,'/net/mercury/data1/pixxon/temp/pxnlicense__init.sav'
    end

        pxndsf_force            ; forces compilation of object
        pxndsft_force           ; forces compilation of object

    end

    ;'/home/antunes/pixonsrc/oopixon/pxnlicense/pxnlicense__init.pro'
    ; minor hack here to update license if using *.sav version
    if (local eq 0) then $
      resolve_routine,'pxnlicense::init',/is_function

end

END

