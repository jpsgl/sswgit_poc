;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : wt2mask
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;
; quickie to get a 0/1 mask from an oPixon 'wt' object
;               
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; quickie to get a 0/1 mask from an oPixon 'wt' object

function wt2mask,opixon

  wt=opixon->get(/wt,/nop)
  mask=wt*0.0
  ie=where(wt ne 0)
  mask[ie]=1

  return,mask

end
