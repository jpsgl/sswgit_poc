;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : voxelcloudadd
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;
; This was an early stab at a kernel-based solver.  It works in that
; it doesn't crash, but it does not yield good results.  Could be
; useful as a template for a better kernel-based method, and to
; show how to invent a new solver.
;               
; given Pixon FOVs calculated for an ideal L0=1, here is
; Cloud interpolation allowing one voxel to deliver to more
; than one pixel, as opposed to the current schema, which traces
; back each voxel to only one pixel.)
;
; Adds signal 'signal' proportionally to region around central voxel
; i,j based on the FOV extent versus actual VOX FOV 'deltavox',
; inserting it into item 'dat', enforcing limits 0,N-1.
;
; handles either incoming dat as array or pointer equally well.
;
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; given Pixon FOVs calculated for an ideal L0=1, here is
; Cloud interpolation allowing one voxel to deliver to more
; than one pixel, as opposed to the current schema, which traces
; back each voxel to only one pixel.)
;
;
; Adds signal 'signal' proportionally to region around central voxel
; i,j based on the FOV extent versus actual VOX FOV 'deltavox',
; inserting it into item 'dat', enforcing limits 0,N-1.
;
; handles either incoming dat as array or pointer equally well.
;

PRO voxelcloudadd,deltavox,signal,i,j,N,dat,debug=debug

  debug=KEYWORD_SET(debug)

;  deltavox=1.0/pxwrapper.sr.LL or pxwrapper.sr.dvox/pxwrapper.sr.L0tau

  dshift=long(deltavox)
  dpartial=deltavox-dshift

  ; make floating array centered on voxel of interest and extending out
  ; i.e. if center is i,j, cover i-del+border, i+del+boarder
  ; if dpartial is 0, our border gets set to 0 but we keep it anyway
  ; note if dshift=0 and dpartial=0, this is just a 3x3 array (dsize=3)
  ; with the center=1 and the rest=0
  dsize=dshift*2+3
  dmask=fltarr(dsize,dsize) + dpartial
  dmask[1:dsize-2,1:dsize-2]=1.0
  dmask=dmask*signal

  ; now co-add it in to i,j (which corresponds to position dshift+1,dshift+1)
  ilow=i-dshift-1
  ihigh=i+dshift+1
  jlow=j-dshift-1
  jhigh=j+dshift+1

  idlow=max([0-ilow,0])
  jdlow=max([0-jlow,0])

  if (ihigh gt N-1) then idup= ihigh-(N-1) else idup=0
  if (jhigh gt N-1) then jdup= jhigh-(N-1) else jdup=0

  if (ilow le N-1 and ihigh ge 0 and jlow le N-1 and jhigh ge 0) then $
    if (datatype(dat) eq 'PTR') then $
    (*dat)[ilow+idlow:ihigh-idup,jlow+jdlow:jhigh-jdup] += $
    dmask[idlow:dsize-idup-1,jdlow:dsize-jdup-1] else $
    dat[ilow+idlow:ihigh-idup,jlow+jdlow:jhigh-jdup] += $
    dmask[idlow:dsize-idup-1,jdlow:dsize-jdup-1]


  if (debug) then begin
  ; print diagnostics if you want to see what this method does
      print,'delta is ',deltavox,' dsize is ',dsize
      print,'Mask indices are: ',0,dshift+1,n_elements(dmask[*,0])-1
      print,'Data indices are: ',ilow,i,ihigh,' (N=',N,')'
      print,'Mask values are: ',dmask[0,dshift+1],dmask[dshift+1,dshift+1],$
        dmask[n_elements(dmask[*,0])-1,dshift+1]
      print,'  Bottom: del=',idlow,', mask i=',0+idlow,', data i=',ilow+idlow
      print,'  Top: del=',idup,', mask i=',dsize-idup-1,', data i=',ihigh-idup
  end

END


PRO voxelcloudtest

  for i=0,10 do begin
      deltavox=i/2.0
      voxelcloudadd,deltavox,1.0,3,4,8,/debug
  end

END
