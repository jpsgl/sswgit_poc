;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : get_istd_nd
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;               
; modified get_istd to accept input params for a single
; image rather than presuming global values.
;
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; modified get_istd to accept input params for a single
; image rather than presuming global values.

; models =
; 0: "Paris", at 555e-7cm, returns photons/sec/cm^2/sr/A
; 1: FWHM=1000 centered at 7000A and 0.2 efficiency, photons/sec/cm^2/sr/A
; 2: 100% efficiency at 7000A (1A wide), photons/sec/cm^2/sr/A
; 3: erg/sec/cm^2/sr/A (not photons)
; 4: B/B0 (not photons)
; 5: Cor2A DN (akin to B/B0 less the dn2msb 2.7E-12 factor)
; 6: variant of 2, photons-per
; 7: variant of 3, ergs-per
;

; optional 'w0' (in A) provides an alternative central wavelength than 7000A
;   for models 2 and 3

;function get_istd_Nd,model,stereo_in,ImI0=ImI0,w0=w0
function get_istd_Nd,model,L0,n0,L0tau,ImI0=ImI0
; version 05 modified by Sandy Antunes

; Calculate Istd

; input   model - instrument model   
;         stereo_in input structure
;         ImI0     ratio of Im/I0 as calculated by the flimb (for model 0 only)


; Cox, Arthur N., "Allen's Astrophysical
; Quantities/editor, Arthur Cox", Springer-Verlag, 14th ed., NY, 2000.
;--------------------------------------------------------------------------------

; some shorter names for input data

;L0    = stereo_in.L0	; average satellite distance
;L0tau = stereo_in.L0tau ; L0*plate scale
;n0    = stereo_in.n0	; standard electron density (electrons/cc)

; ----- Physical Constants

e2 = 23.070796e-20   ; elementary charge squared (esu)         [Allen]
m  = 9.109389754e-28 ; electron mass             (g)           [Allen]
c  = 2.99792458e10   ; speed of light            (cm/sec)      [Allen]
ss = (e2/(m*c*c))^2  ; Thompson Cross section    (cm^2/sr), roughly 7.94e-26
AU = 1.495979e13     ; 1 AU                      (cm)          [Allen]
h  = 6.6260755e-27   ; Planck's constant         (erg-sec)          [Allen]
Im = 2.009e10        ; mean solar intensity      (erg/sec/cm^2/sr)  [Allen]
rsun = 6.65997e10    ; solar radius in cm        (cm)          [Allen]

; ----- duplicate Allen's data

; wavelength (A)

wA = [20,22,24,26,28,30,32,34,36,37,38,39,40,41,42,43,44,45,46,48,50,55,$
      60,65,70,75,80,90,100,$
      110,120,140,160,180,200,250,300,400,500]*.01
wA=wA*1.0e4

; central intensity (erg/sec/cm^2/sr/A)

IwA = [1.4,13,13,37,60,134,167,189,196,228,216,208,297,338,345,312,361,387,$
       395,384,361,343,317,281,246,218,194,157,125,101,84,56,40,27,18,8.1,$
       4.1,1.35,.57]*0.01
IwA=IwA*1.0e6

IwAerg=IwA; save for later photon-free alternative values

; convert IwA to photons  (photons/sec/cm^2/sr/A)

IwA = IwA*wA*1.0e-8/(h*c)

; ----- calculate proportionality constant K

; whoops, Paul erred by using AU, not rsun, as the unit factor here
;;K = !pi*ss*L0tau^3*n0*AU/(2*L0^2)
;;K = K*L0^2/L0tau^2

K = (!pi/2.0) * l0tau * rsun * ss * n0


; ----- resample w and Iw to a finer grid

 dw = 1.0d0
; w = scale_tomo(min(wA),max(wA),dw)
 w = scale(min(wA),max(wA),dw)
 Iw = interpol(IwA,wA,w)
 Iwerg = interpol(IwAerg,wA,w)

; ----- Implement Model

case model of

    0: begin                    ; Paris, Palo Alto
        wl = 555.0e-7           ; typical solar wavelength  (cm)
        if (n_elements(ImI0) eq 0) then ImI0=1.0
        Istd = K*0.1*(Im/ImI0)  ; (erg/sec/cm^2/sr/A) 

        Istd = Istd*wl/(h*c) ; convert to photons (photon/sec/cm^2/sr/A) 
    end

    1: begin                  ; 1000A width @ 7000A and 0.2 efficiency
        
        FWHM=1000.              ; width of instrument function
        w0 = 7000.              ; central wavelength
        Q0 = 0.2                ; peak efficiency

        v = FWHM/(2*sqrt(2*alog(2))) ; variance
        
        Q=Q0*exp(-(w-w0)^2/(2*v*v)) ; efficiency function
        Istd = K*total(Q*Iw)*dw

    end

    2: begin                 ; 1A 100% efficiency, returns photons per
   
        if (n_elements(w0) eq 0) then w0 = 7000. ; central wavelength
        index= floor(mean(where (wA ge w0*0.95 and wA le w0*1.05)))
        Istd = K * IwA[index]

    end

    3: begin                    ; returns ergs per, not photons per

        if (n_elements(w0) eq 0) then w0 = 7000. ; central wavelength
        index= floor(mean(where (wA ge w0*0.95 and wA le w0*1.05)))
        Istd = K * IwAerg[index] ; (erg/sec/cm^2/sr/A) 

    end

    4: begin                    ; for proportionality work
        Istd = K
    end

    5: begin                    ; assumes Cor2A DN
        Istd = K / 2.7E-12
    end

    6: begin                  ; 1A width @ 7000A and 1.0 efficiency, photons
        ; should give the same answer as 2


        FWHM=1.              ; width of instrument function
        w0 = 7000.              ; central wavelength
        Q0 = 1.0                ; peak efficiency

        v = FWHM/(2*sqrt(2*alog(2))) ; variance
        
        Q=Q0*exp(-(w-w0)^2/(2*v*v)) ; efficiency function
        Istd = K*total(Q*Iw)*dw

    end

    7: begin                  ; 1A width @ 7000A and 1.0 efficiency, ergs
        ; should give the same answer as 3


        FWHM=1.              ; width of instrument function
        w0 = 7000.              ; central wavelength
        Q0 = 1.0                ; peak efficiency

        v = FWHM/(2*sqrt(2*alog(2))) ; variance
        
        Q=Q0*exp(-(w-w0)^2/(2*v*v)) ; efficiency function
        Istd = K*total(Q*Iwerg)*dw

    end

    8: begin   ; no conversion (returns Pixon's 'dimensionless' units)
        Istd=1.0
    end

    9: begin   ; spatial conversion only
        Istd= l0tau * rsun
    end

    10: begin   ; geometric value:
                ; area subtended (in pi/2 * cm) * cross section (in cm^2/sr)
        ; Istd= (!pi/2.0) * l0tau * rsun * ss
        Istd = K
    end

    11: begin   ; geometric value as per 10, but assuming DN
                ; area subtended (in pi/2 * cm) * cross section (in cm^2/sr)
        ; Istd= (!pi/2.0) * l0tau * rsun * ss / 2.7E-12
        Istd = K/2.7E-12
    end

    else: stop,'bad model = '+string(model)
end

return, Istd
end
