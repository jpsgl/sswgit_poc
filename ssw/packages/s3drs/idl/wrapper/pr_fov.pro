;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : pr_fov
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;
; given a pxwrapper (or save file containing one), returns
; the back projection threeview showing the image
; coverage by the different views.
;               
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; given a pxwrapper (or save file containing one), returns
; the back projection threeview showing the image
; coverage by the different views.
;

FUNCTION pr_fov,pxwrapper,file=file

  if (n_elements(file) ne 0) then begin
      restore,/v,file
  end

  Nd=pxwrapper.sr.Nd
  N=pxwrapper.sr.Ncube
  flats=make_array(N,N,Nd,value=1.0,/float)
  pxwrappercopy=pxwrapper
  pxwrappercopy.sr.polar='n'; turns on flat system
  im=pr_backproject(pxwrappercopy,flats)

  return,im

END
