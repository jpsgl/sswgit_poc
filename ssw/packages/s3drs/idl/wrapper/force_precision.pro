;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : force_precision
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;               
; utility that, given either an array of pointers or
; an array of data images, casts them to type /doubleprec or /floatprec
; Note it fundamentally changes the data, does not just return it.
;
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; utility that, given either an array of pointers or
; an array of data images, casts them to type /doubleprec or /floatprec
; Note it fundamentally changes the data, does not just return it.

PRO force_precision,data,doubleprec

  doubleprec=KEYWORD_SET(doubleprec)

  if (doubleprec) then matchme='DOU' else matchme='FLO'

  dt=datatype(data)

  if (dt eq 'PTR') then begin
      ; handles a single ptr or an array
      dt=datatype(*data[0])
      nele=n_elements(data)

      if (dt ne matchme) then begin
          dnew=ptrarr(nele)
          for i=0,nele-1 do begin
              if (matchme eq 'DOU') then dnew[i]=ptr_new(double(*(data[i]))) $
              else dnew[i]=ptr_new(float(*(data[i])))
          end
          for i=0,nele-1 do ptr_free,data[i]
          data=dnew
      end

  end else if (dt ne matchme) then begin
      if (matchme eq 'DOU') then data=double(data) else data=float(data)
  end

END

PRO test_force_precision

  farr=make_array(6,6,/float,/index)
  darr=make_array(6,6,/double,/index)

  print,'*** Float data, should yield flt, flt, dbl, flt'
  f1=farr
  help,f1
  force_precision,f1,0
  help,f1
  force_precision,f1,1
  help,f1
  force_precision,f1,0
  help,f1

  print,'*** Float data, should yield dbl, flt, dbl, flt'
  d1=darr
  help,d1
  force_precision,d1,0
  help,d1
  force_precision,d1,1
  help,d1
  force_precision,d1,0
  help,d1

  print,'*** Float ptr, should yield flt, flt, dbl, flt'
  p1=ptrarr(3)
  for i=0,2 do p1[i]=ptr_new(farr)
  help,p1,*p1[0],*p1[1],*p1[2]
  force_precision,p1,0
  help,p1,*p1[0],*p1[1],*p1[2]
  force_precision,p1,1
  help,p1,*p1[0],*p1[1],*p1[2]
  force_precision,p1,0
  help,p1,*p1[0],*p1[1],*p1[2]


  print,'*** Double ptr, should yield dble, flt, dbl, flt'
  p1=ptrarr(3)
  for i=0,2 do p1[i]=ptr_new(darr)
  help,p1,*p1[0],*p1[1],*p1[2]
  force_precision,p1,0
  help,p1,*p1[0],*p1[1],*p1[2]
  force_precision,p1,1
  help,p1,*p1[0],*p1[1],*p1[2]
  force_precision,p1,0
  help,p1,*p1[0],*p1[1],*p1[2]

  print,'*** Ptr array of float data, should yield flt, flt, dbl, flt'
  p1=ptrarr(3)
  for i=0,2 do p1[i]=ptr_new(f1)
  help,p1,*p1[0],*p1[1],*p1[2]
  force_precision,p1,0
  help,p1,*p1[0],*p1[1],*p1[2]
  force_precision,p1,1
  help,p1,*p1[0],*p1[1],*p1[2]
  force_precision,p1,0
  help,p1,*p1[0],*p1[1],*p1[2]

  print,'*** Ptr array of double data, should yield dbl, flt, dbl, flt'
  p1=ptrarr(3)
  for i=0,2 do p1[i]=ptr_new(d1)
  help,p1,*p1[0],*p1[1],*p1[2]
  force_precision,p1,0
  help,p1,*p1[0],*p1[1],*p1[2]
  force_precision,p1,1
  help,p1,*p1[0],*p1[1],*p1[2]
  force_precision,p1,0
  help,p1,*p1[0],*p1[1],*p1[2]


END
