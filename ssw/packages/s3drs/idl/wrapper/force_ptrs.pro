;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : force_ptrs
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;
; utility that, given either an array of pointers or
; an array of data images, returns an array of pointers.
; Used to ensure datatypes of unknown type are cast into
; an array of pointers
;
; Note that if /single, it assumes the input is a
; single item and returns a single pointer, not an array
; of pointers (useful if you have an item that may be a
; single data or a pointer and you require a pointer).
;
; If given a value of 'norm' (other than 1.0), it multiplies
; the incoming data by that normalization before returning it.
; Norms can be a single value or an array of per-data norms.
;               
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; utility that, given either an array of pointers or
; an array of data images, returns an array of pointers.
; Used to ensure datatypes of unknown type are cast into
; an array of pointers
;
; Note that if /single, it assumes the input is a
; single item and returns a single pointer, not an array
; of pointers (useful if you have an item that may be a
; single data or a pointer and you require a pointer).
;
; If given a value of 'norm' (other than 1.0), it multiplies
; the incoming data by that normalization before returning it.
; Norms can be a single value or an array of per-data norms.
;

FUNCTION force_ptrs,indata,nele,single=single,norm=norm

  single=KEYWORD_SET(single)
  if (n_elements(norm) eq 0) then norm=1.0; default is no change

  if (datatype(indata) ne 'PTR') then begin
      if (single) then nele=1
      if (n_elements(nele) eq 0) then nele=n_elements(indata[0,0,*]) ; guess
      parray=ptrarr(nele)
      if (n_elements(norm) eq nele) then norms=norm else $
        norms=make_array(/float,nele,value=norm)
      for m=0,nele-1 do parray[m]=ptr_new(indata[*,*,m] * norms[m])
  end else begin
      if (norm eq 1) then begin
          parray=indata
      end else begin
          ; when normalizing existing pointers, we have to copy the
          ; data to prevent changing the original copy
          nele=n_elements(indata)
          parray=ptrarr(nele)
          if (n_elements(norm) eq nele) then norms=norm else $
            norms=make_array(/float,nele,value=norm)
          for m=0,nele-1 do parray[m]=ptr_new(*(indata[m]) * norms[m])
      end
  end

  if (single) then parray=parray[0]; ugly but works

  return,parray

END
