;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : pr_backproject
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;
; stand alone back projection combined from pxndsft_force and p_osetup
;               
; Use       : mycube = pr_backproject(pxwrapper,datum)
;    
; Inputs    : pxwrapper, datum
;             (optional) id, image
;               
; Outputs   : returns the backprojection cube, also can populate pxwrapper
;
; Keywords  : /fast uses a faster, sloppier method
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon, visualization, whitelight
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; stand alone back projection combined from pxndsft_force and p_osetup

PRO test_pr_backproject,pixon=pixon

   pixon=KEYWORD_SET(pixon)

   N=32
   prep_3drv,pxwrapper,N=N,gamma=[0,90]

   flat=fltarr(32,32)+1.0

   fproject=pr_backproject(pxwrapper,flat,0)
   fproject=pr_backproject(pxwrapper,flat,1,image=fproject)

   threeview,fproject,fixed=256
   threeview,fproject,fixed=256,/log

   timage=pixonmodels(N,'fluxrope',pxwrapper)
   render1=pr_render(pxwrapper,timage,0)
   render2=pr_render(pxwrapper,timage,1)

   tproject=pr_backproject(pxwrapper,[[[render1]],[[render2]]])

   threeview,tproject,fixed=256
   threeview,tproject,fixed=256,/log

   ; compare with pixon
   if (pixon) then begin
       p_generate_pixon,'classic',pxwrapper,oPixon
       p_insert_data,pxwrapper,[[[render1]],[[render2]]],oPixon
       oData = oPixon->get(/odata)
       im=odata[0]->dsft(render1)
       im=odata[1]->dsft(render2)

       threeview,im,fixed=256
       threeview,im,fixed=256,/log
   end

END

FUNCTION pr_backproject,pxwrapper,datum,id,image=image,fast=fast

;common debugs,phys,tdsf,tdsft,dbg
;pr_library,'init'; loads some necessary pr_* routines

  fast=0          ; 1 = fast but sloppy, 0 = 4x slower but more accurate
  if (n_elements(fast) eq 0 and tag_exist(pxwrapper,'sc')) then $
    fast=pxwrapper.sc.fast else fast=KEYWORD_SET(fast)

  if (n_elements(phys) eq 0) then phys=1
  
  if (n_elements(id) eq 0) then begin
                                ; do all frames
      istart=0
      iend=pxwrapper.sr.Nd-1  ; n_elements(pxwrapper.sr.gamma)-1
  end else begin
      istart=id
      iend=id
  end
  
; handles legacy or stand-alone structure as well as Pixon's wrapper
  if (tag_exist(pxwrapper,'sr')) then sr=pxwrapper.sr else sr=pxwrapper
  
  Ncube=sr.Ncube
  Nd=sr.Nd;   n_elements(sr.gamma)

;;  lla0=sr.lla0; usually just (N-1)/2.0, but now we allow offset cubes too

; initalize if needed
  if (n_elements(image) eq 0) then $
    image = MAKE_ARRAY(/float,Ncube,Ncube,Ncube,value=0.0)
  
;common debugs,phys,tdsf,tdsft,dbg
  
; debug flags
  dbgij = 0
  dbgid = 0
  dbgla = 0
  
; -----
  
  FOR iframe=istart,iend do begin
;    print,'iframe is ',iframe
                                ; handles incoming pointers or arrays
      if (size(datum,/n_dimensions) eq 1) then begin
          data=*(datum[iframe])
      end else if (size(datum,/n_dimensions) eq 3) then begin
          data=datum[*,*,iframe]
      end else begin
          data=datum
      end
      
      Ndata=sr.Ndata[iframe]
      
      if (tag_exist(sr,'d')) then dvoxfrac=sr.d else $
        dvoxfrac=sr.dvox/sr.L0tau[iframe]
      o_flimb,sr.chl,Ncube,1/sr.L0tau[iframe],dvoxfrac,sr.lla0,fa,fb,fc,rmin2,an,ImI0
      sr.an=an
      sr.Istd[iframe]  =  $
        get_istd_Nd(sr.imodel[iframe],sr.L0,sr.n0,sr.L0tau[iframe],ImI0 = ImI0)
      
      data=data/sr.Istd[iframe] ; convert to render units expected
      
      ; note ffunc alters sr.trW,trRRs,trP,trVs as well as returning values
      ffunc,sr,iframe,ps,mu,nu,is,jc,K2,u,x,y,rhos2,c2z,s2z
      pxwrapper.sr=sr
      
      rho2   =  x*x+y*y
      psLs   =  ps*pxwrapper.sr.LL[iframe]
      psLs2  =  psLs^2
      
;;    prf2d=ptr_new(pr_gprf([Ndata,Ndata], fwhm = fwhm_2d))
;    prf2d=ptr_new(pr_gprf([Ndata,Ndata],fwhm_2d))
      
                                ; prepFPRF returns a ptrarr(ndata,nchan)
;;    fprfset=PrepFPRF(*(prf2d),ddim=[Ndata,Ndata],idim=[Ndata,Ndata],/FREE )
;    temp = pr_rfft(pr_Wrap(*(prf2d)))
;    fprfset = ptr_new(temp/temp[0])
;    fprf=fprfset[0]
      
      fprf=pr_fprf(1,[Ndata,Ndata])
      
      roco=sr.roco[iframe]
      polar=sr.polar[iframe]
;    c2z=
;    s2z=
      lla0=sr.lla0
;    rho2=rho2
;    rhos2=
;    psLs=psLs
;    psLs2=psLs2
      i=is
;    jc=
;    K2=
      if (tag_exist(sr,'d')) then dvoxfrac=sr.d else $
        dvoxfrac=sr.dvox/sr.L0tau[iframe]
      d2=dvoxfrac^2
      fa=fa*dvoxfrac^3
      fb=fb*dvoxfrac^3
      fc=fc*dvoxfrac^3
;    rmin2=
      tau=sr.tau[iframe]
      tau2=sr.tau[iframe]*sr.tau[iframe]
      interp=sr.interp
      if (getenv('disumlib') eq '') then interp=1;switch to IDL if no C-version

      machine=getenv('HOST')
      g=sr.rho[iframe]
      theta=sr.theta[iframe]
      
      gamma=g                   ; easier to grep on
      
      
      psp=2
      
      N  = n_elements(data[*,0,0])
      
; rotate (for close to z axis stuff). No need to rotate image, it doesn't
; contain anything yet.
      
      if(roco eq 1)then lla0=[0.5*(N-1)-lla0[2],lla0[1],lla0[0]]
      
; for now, assume sun-center=optical-center=image-center
      if (theta ne 0) then lla0=lla0
      
; ----- de-convolution
      
;;;;    h = self->HT(data,fprf)
; copied relevant call (1 line) from pxndata::ht to here to
; replace the above self->ht invocation:
;      h = RConv(*fprf, data, /CONJ)
; then rewrote it so this does not require the pixon library
;;       h = pr_rconv(*fprf, data)
      h = pr_fprf(0,*fprf,data)
      
;if((size(data))[0] eq 3)then h=reform(data[*,*,id]) else h=data
      
; ----- de-rotate and multiply by radial and angular functions
      
      case strlowcase(polar) of
          
          'n': begin         ; no Thompson scattering-- test case only
              
              for la=0,N-1 do begin
                  z2  = d2*(la-lla0[2])^2
                  r2  = rho2+z2
                  rs2 = rhos2+tau2*z2
                  j   = jc+la/K2
                  cx2 = (r2*tau2-2*tau*psLs+psLs2/r2)/rs2
                  r2  = round((r2-rmin2)/d2)
                  fizzix=((fa[r2]+cx2*fb[r2]+fc[r2])/rs2)*phys + (1-phys)
                  fizzix=fizzix*0.0+1.0 ; throw it away and just go flat
                  
                                ; native IDL version of '2', no interpolation
                  dsumtidl,0,N,i,j,la,image,fizzix,h
                  
              end
              
          end
          
          
; =============== PLANE POLARIZATION ===================
          
          'p': begin            ; plane
              
              ht=h*c2z
              hr=h*s2z
              
              for la=0,N-1 do begin
                  z2  = d2*(la-lla0[2])^2
                  r2  = rho2+z2
                  rs2 = rhos2+tau2*z2
                  j   = jc+la/K2
                  cx2 = (r2*tau2-2*tau*psLs+psLs2/r2)/rs2
                  r2  = round((r2-rmin2)/d2)
                  
                  if(interp ne 3 and interp ne 1)then stop,'dsft error'
                  
                  fizzixt=(fa[r2]/rs2)*phys + (1-phys) ; tangential
                  if (interp eq 3) then begin
                                ; C program - with interpolation v=1
                      disumt1_inc,image,N,ht,i,j,fizzixt,la,machine
                  end else begin
                                ; native IDL version of '3', interpolation v=1
                      dsumtidl,1,N,i,j,la,image,fizzixt,ht
                  end
                                ; disumt1_inc,image,N,ht,i,j,fizzixt,la,machine
                  
                  fizzixr=((cx2*fb[r2]+fc[r2])/rs2)*phys + (1-phys) ;radial
                  if (interp eq 3) then begin
                                ; C program - with interpolation v=1
                      disumt1_inc,image,N,hr,i,j,fizzixr,la,machine
                  end else begin
                                ; native IDL version of '3', interpolation v=1
                      dsumtidl,1,N,i,j,la,image,fizzixr,hr
                  end
                                ; disumt1_inc,image,N,hr,i,j,fizzixr,la,machine
                  
; ---------------------------
                  
; debug
                  if((dbgij eq 1) and (la eq dbgla))then begin
                      q=where(h ne 0,count)
                      if((q[0] eq dbgij) and (id eq dbgid))then begin
                          fmat="('dsft: code = ',i3,' ',i3,' xx',i3)"
                          print,q[0],id,la,format=fmat
                          stop,' dsft'
                      end
                  end
                  
; ---------------------------
                  
              end
          end 
          
; =============== TANGENTIAL POLARIZATION ===========
          
          't': begin            ; tangential polarization
              
              for la=0,N-1 do begin
                  z2  = d2*(la-lla0[2])^2
                  r2  = round((rho2+z2-rmin2)/d2)
                  rs2 = rhos2+tau2*z2
                  j   = jc+la/K2
                  fizzix=(fa[r2]/rs2)*phys + (1-phys)
                  
; ---------------------------
                  
                  case interp of
                      
                      0: begin ; native IDL version of '2', no interpolation
                          dsumtidl,0,N,i,j,la,image,fizzix,h
                      end
                      
                      1: begin ; native IDL version of '3', interpolation v=1
                          dsumtidl,1,N,i,j,la,image,fizzix,h
                      end
                      
                      2: begin  ; C program - no interpolation
                          @dsumt_inc
                      end
                      
                      3: begin  ; C program - with interpolation v=1
                          disumt1_inc,image,N,h,i,j,fizzix,la,machine
                      end
                      
                      4: begin ; C program - with interpolation v=variable
                          @disumt2_inc
                      end
                      
                      5: begin  ; C program - trapezoid spreading
                          disumt3_inc,image,N,h,i,j,fizzix,la,machine,g
                      end
                      
                      else: stop,'gack'
                  end
                  
; debug
;if((dbg eq 1) and (la eq dbgla))then begin
;  q=where(h ne 0,count)
;  if((q[0] eq dbgij) and (id eq dbgid))then begin
;    fmat="('dsft: code = ',i3,' ',i3,' xx',i3)"
;    print,q[0],id,la,format=fmat
;    stop,' dsft'
;  end
;end
                  
; ---------------------------
                  
              end
          end
          
; =============== RADIAL POLARIZATION ===============
          
          'r': begin            ; radial polarization
              
              for la=0,N-1 do begin
                  z2  = d2*(la-lla0[2])^2
                  r2  = rho2+z2
                  rs2 = rhos2+tau2*z2
                  j   = jc+la/K2
                  cx2 = (r2*tau2-2*tau*psLs+psLs2/r2)/rs2
                  r2  = round((r2-rmin2)/d2)
                  fizzix=((cx2*fb[r2]+fc[r2])/rs2)*phys + (1-phys)
                  
; ---------------------------
                  
                  case interp of
                      
                      0: begin ; native IDL version of '2', no interpolation
                          dsumtidl,0,N,i,j,la,image,fizzix,h
                      end
                      
                      1: begin ; native IDL version of '3', interpolation v=1
                          dsumtidl,1,N,i,j,la,image,fizzix,h
                      end
                      
                      2: begin  ; C program
                          @dsumt_inc
                      end
                      
                      3: begin  ; C program - with interpolation v=1
                          disumt1_inc,image,N,h,i,j,fizzix,la,machine
                      end
                      
                      4: begin ; C program - with interpolation v=variable
                          @disumt2_inc
                      end
                      
                      5: begin  ; C program - trapezoid spreading
                          disumt3_inc,image,N,h,i,j,fizzix,la,machine,g
                      end
                      
                      else: stop,'gack'
                  end
                  
; debug
;if((dbg eq 1) and (la eq dbgla))then begin
;  q=where(h ne 0,count)
;  if((q[0] eq dbgij) and (id eq dbgid))then begin
;    fmat="('dsft: code = ',i3,' ',i3,' xx',i3)"
;    print,q[0],id,la,format=fmat
;    stop,' dsft'
;  end
;end
                  
; ---------------------------
                  
              end
          end 
          
; =============== NO POLARIZATION ===================
          
          'b': begin            ; brightness
              
              for la=0,N-1 do begin
                  z2  = d2*(la-lla0[2])^2
                  r2  = rho2+z2
                  rs2 = rhos2+tau2*z2
                  j   = jc+la/K2
                  cx2 = (r2*tau2-2*tau*psLs+psLs2/r2)/rs2
                  r2  = round((r2-rmin2)/d2)
                  fizzix=((fa[r2]+cx2*fb[r2]+fc[r2])/rs2)*phys + (1-phys)
                  
; ---------------------------
                  
                  case interp of
                      
                      0: begin ; native IDL version of '2', no interpolation
                          dsumtidl,0,N,i,j,la,image,fizzix,h
                      end
                      
                      1: begin ; native IDL version of '3', interpolation v=1
                          dsumtidl,1,N,i,j,la,image,fizzix,h
                      end
                      
                      2: begin  ; C program
                          @dsumt_inc
                      end
                      
                      3: begin  ; C program - with interpolation v=1
                          disumt1_inc,image,N,h,i,j,fizzix,la,machine
                      end
                      
                      4: begin ; C program - with interpolation v=variable
                          @disumt2_inc
                      end
                      
                      5: begin  ; C program - trapezoid spreading
                          disumt3_inc,image,N,h,i,j,fizzix,la,machine,g
                      end
                      
                      else: stop,'gack'
                  end
                  
; debug
;if((dbg eq 1) and (la eq dbgla))then begin
;  q=where(h ne 0,count)
;  if((q[0] eq dbgij) and (id eq dbgid))then begin
;    fmat="('dsft: code = ',i3,' ',i3,' xx',i3)"
;    print,q[0],id,la,format=fmat
;    stop,' dsft'
;  end
;end
                  
; ---------------------------
                  
              end
          end 
          
; ===================================================
          
          else: stop,'ERROR in dsft: polar = '+string(polar)
      end
      
; reverse the rotation. No need to do lla0, its not returned
      
      if(roco eq 1)then for nu=0,N-1 do $
        image[*,nu,*]=rotate(reform(temporary(image[*,nu,*])),3)
      
;print,'dsft: ',gamma,theta
      if(roco eq -1 and (theta ne 0 or gamma ne 0))then begin
;    if (theta ne 0) then print,'rotating theta'
          if (fast) then begin
;    if (gamma ne 0) then print,'rotating gamma'
                                ; note gamma should be CCW, so reverse is CW
              cuberot3d,image,[0.0,0.0-theta,gamma]
          end else begin
;;              image=turn_3d(image,0.0,0.0-theta,0.0-gamma,/resize,$
;;                           xyzcenter=lla0,/pivot,/interp)
              image=turn_3d(image,0.0,0.0-theta,0.0-gamma,/resize,$
                            /pivot,/interp)
          end
      end
      
; -----
      
;    tdsft=tdsft+(systime(-1)-t0)
      
;;print,'return from dsft'
      
  END
  
  return,image 
end
