;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : pxwrapper_update
;               
; Purpose   : part of Pixon, part of the s3drs GUI
;               
; Explanation: part of the Pixon wrappers used to access and
; manipulate the various arcane Pixon structures and objects.
;               
; a little wrapper that, given subelements intended for one
; of our canonical Pixon structures, puts them into the top-level
; 'pxwrapper' structure.
;
; Use       : typically called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (various)
;
; Keywords  : (various)
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : reconstruction, pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2005-2009
;               
;-            
; a little wrapper that, given subelements intended for one
; of our canonical Pixon structures, puts them into the top-level
; 'pxwrapper' structure.

PRO pxwrapper_update,pxwrapper,subname,substruct,iele

  case subname of
      'sc': mystruct=pxwrapper.sr
      'sn': mystruct=pxwrapper.sr
      'sp': mystruct=pxwrapper.sr
      'sr': mystruct=pxwrapper.sr
      'sf': mystruct=pxwrapper.sr
      'userstruct': mystruct=pxwrapper.userstruct
      else: stop,'no such structure, exiting'
  end

  update_struct_arrays,mystruct,substruct,iele

  pxwrapper=addrep_struct(pxwrapper,subname,mystruct)

END
