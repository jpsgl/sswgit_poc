;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; sample:
;quickcme,64,'20071231_025220','20071231_022220',/c3
;
; use of /c3 means it will look for the c3 file closest to the given
; times.  Ditto for /c2 and /cor1 and /cor2
; Note you _have_ to specify either a model or, for real data, a detector!
;
; If you give /pretty, it also saves a full-res version of the
; main input (difference, running diff, or background-subtracted)
; Use /pretty and /setuponly to just make these 'pretty pictures'
; without getting into any Pixon objects/work at all.
;
; The datacut=1e-11 (e.g.) flag cuts the data just before inserting
; (but after noise is calculated using unadultered data) to explore
; signal issues.
;

PRO quickcme_parse,N=N,cmedate=cmedate,priordate=priordate

  while (n_elements(N) eq 0) do begin
      print,'Input a value for N of 64, 128, or 256 (256 is risky)'
      read,'N = ? ',Nval
      if (Nval eq 64 or Nval eq 128 or Nval eq 256) then N=floor(Nval)
  end
  
  if (n_elements(cmedate) eq 0) then begin
      cmedate=""
      print,'Enter the datestamp of the CME, e.g. 20071104_222220'
      read,cmedate
  end

;  cpieces=strsplit(cmedate,'_',/extract)

  if (n_elements(priordate) eq 0) then begin  
      priordate=""
      print,'To run as a difference image, provide the datestamp of the'
      print,' immediately prior frame (for a running difference, e.g.'
      print,' 20071104_212220) or of an earlier quiet frame (for a'
      print,' difference image, e.g. 20071104_022220).  If you wish to'
      print,' instead just subtract a standard background, type "background"'
      read,priordate
  
  end

  if (strmid(priordate,0,4) eq 'back') then priordate='back'

END

FUNCTION quickcme_excerpt,dfull

   ; subselect item
   ; assume C3, aka FOV=2*Cor2
   ; so 0-1023 becomes 0-511
  No=1024
  Np=512
  dhalf=dblarr(Np,Np)
  dhalf[0:Np-1,0:Np-1]=dfull[No/4:(3*No/4)-1,No/4:(3*No/4)-1]
  return,dhalf

END

PRO quickcme_rotate,pxwrapper,iele,diff,ndiff,mset,verbose=verbose

  verbose=setyesno(verbose)

      ; change solar north to ecliptic north
  DEFSYSV, "!DRADEG", 180.0d/!DPI
  DEFSYSV, "!DDTOR", !DPI/180.0d

  xyz=*(pxwrapper.sr.hae_coords[iele])
  angle = festival_ecl_solar_north(xyz[0], xyz[1], xyz[2])

  xy=p_getk0(pxwrapper.sr,iele,/sun)

  diff[*,*,iele]=rot(diff[*,*,iele],angle,1.0,xy[0],xy[1],/pivot)
  ndiff[*,*,iele]=rot(ndiff[*,*,iele],angle,1.0,xy[0],xy[1],/pivot)
  mset[*,*,iele]=rot(mset[*,*,iele],angle,1.0,xy[0],xy[1],/pivot)
  
  ; also offset our optical center by our solar rotation
  k0=p_getk0(pxwrapper.sr,iele)
  zerok0= (k0-xy)
  newk0=zerok0*0.0
  newk0[0] = cos(angle*!dtor)*zerok0[0] - sin(angle*!dtor)*zerok0[1]
  newk0[1] = sin(angle*!dtor)*zerok0[0] + cos(angle*!dtor)*zerok0[1]
  newk0=newk0+xy
  pxwrapper.sr.k0[iele]=ptr_new(newk0)

  if (verbose) then print,'rotated by ',angle
END


PRO quickCME,N,cmedate,priordate,zmask=zmask,xyzmask=xyzmask,$
             checksn=checksn,dn=dn,excerpt=excerpt,c2=c2,c3=c3,cor1=cor1,$
             sidemask=sidemask,nozero=nozero,test=test,cor2=cor2,$
             mxit=mxit,occulttop=occulttop,occultbottom=occultbottom,$
             occultleft=occultleft,occultright=occultright,pseudo=pseudo,$
             maskratio=maskratio,nadjust=nadjust,setuponly=setuponly,$
             maskbright=maskbright,scrubbright=scrubbright,$
             sncutoff=sncutoff,prep=prep,standardplots=standardplots,$
             flatnoise=flatnoise,best=best,model=model,debug=debug,$
             modelgamma=modelgamma,modeltheta=modeltheta,pretty=pretty,$
             trimc3top=trimc3top,tag=tag,jumpstart=jumpstart,polar=polar,$
             datacut=datacut,resume=resume

  on_error,2
  pixon_env,'c',/local; ensure we are using local, latest version

  msbperdn=2.7E-12

  standardplots=setyesno(standardplots)
  flatnoise=setyesno(flatnoise)
  c2=setyesno(c2)
  c3=setyesno(c3)
  cor1=setyesno(cor1)
  cor2=setyesno(cor2)
  trimc3top=setyesno(trimc3top)
  if (n_elements(model) ne 0) then modelflag=1 else modelflag=0
  if (n_elements(modelgamma) eq 0) then modelgamma=[0,90]
  if (n_elements(modeltheta) eq 0) then modeltheta=modelgamma*0.0
  nmodels=n_elements(modelgamma)

  test=setyesno(test)
  debug=setyesno(debug)
  pretty=setyesno(pretty)
  checksn=setyesno(checksn)
  setuponly=setyesno(setuponly)
  jumpstart=setyesno(jumpstart)
  prep=setyesno(prep)
  dn=setyesno(dn)
  pseudo=setyesno(pseudo)
  maskbright=setyesno(maskbright)
  scrubbright=setyesno(scrubbright)
  zmask=setyesno(zmask)  
  xyzmask=setyesno(xyzmask)  
  if (xyzmask) then zmask=0; redundant
  sidemask=setyesno(sidemask)
  nozero=setyesno(nozero)
  excerpt=setyesno(excerpt)
  if (n_elements(sncutoff) eq 0) then sncutoff=-1
  occultleft=setyesno(occultleft)
  occultright=setyesno(occultright)
  occulttop=setyesno(occulttop)
  occultbottom=setyesno(occultbottom)
  trimc3top=setyesno(trimc3top)
  polar=setyesno(polar)
  resume=setyesno(resume)

  best=setyesno(best)
  if (best) then begin
      nozero=1
      xyzmask=1
  end

  if (test) then begin
      ; set some defaults here just for testing
      if (n_elements(N) eq 0) then N=64
      if (n_elements(cmedate) eq 0) then cmedate='20071231_022220'
      if (n_elements(priordate) eq 0) then priordate='20071231_015220'
      checksn=1
      cor2=1
  end else begin
      quickcme_parse,N=N,cmedate=cmedate,priordate=priordate
  end

  if (n_elements(nadjust) eq 0) then if (N eq 64) then nadjust=10.0 else if (N eq 128) then nadjust=100.0 else nadjust=1.0

  Nfactor=2048/float(N)         ; e.g. 1/16 for N=128 so Nfactor=16
  

  savestem=cmedate+'_'+priordate+'_'+int2str(N)
  if (cor2) then savestem=savestem+'_cor2'
  if (polar) then savestem=savestem+'_polar'
  if (c2) then savestem=savestem+'_c2'
  if (c3) then $
    if (excerpt) then savestem=savestem+'_c3ex' else $
    savestem=savestem+'_c3'
  if (cor1) then savestem=savestem+'_cor1'
  if (zmask) then savestem=savestem+'_zmask'
  if (xyzmask) then savestem=savestem+'_xyzmask'
  if (sidemask) then savestem=savestem+'_sidemask'
  if (nozero) then savestem=savestem+'_nozero'
  if (jumpstart) then savestem=savestem+'_jumpstart'
  if (n_elements(datacut) ne 0) then savestem=savestem+'_datacut'
  if (n_elements(tag) ne 0) then savestem=savestem+'_'+tag
  
  if (resume eq 0) then begin
      
      
      print,'Setting up for ',cmedate,' - ',priordate
      
  ; FIRST, PREP PXWRAPPER
      iele=0
      if (cor2) then begin
          if (polar) then iele=iele+6 else iele=iele+2
      end
      if (c3) then ++iele
      if (c2) then ++iele
      if (cor1) then iele=iele+2
      if (pretty) then pset=ptrarr(iele)
      if (pretty) then pcount=0
      gamma=make_array(/float,iele,value=0.0)
      theta=gamma*0.0
      if (modelflag) then begin
                                ; front-place input elements
          gamma=[modelgamma,gamma]
          theta=[modeltheta,theta]
          iele=iele+nmodels
      end
      ict=iele              ; keep track of 'data' versus 'masks' here
      if (zmask or xyzmask) then begin
          gamma=[gamma,0.0]     ; add Z
          theta=[theta,90.0]    ; add Z
          if (xyzmask) then gamma=[gamma,90.0,0.0] ; also add XY
          if (xyzmask) then theta=[theta,0.0,0.0] ; also add XY
      end
      
      if (cor2) then $
        prep_3drv,pxwrapper,N=N,theta=theta,gamma=gamma,/cor2 else if (cor1) then $
        prep_3drv,pxwrapper,N=N,theta=theta,gamma=gamma,/cor1 else $
        prep_3drv,pxwrapper,N=N,theta=theta,gamma=gamma,/cor2
      
      
      if (dn) then pxwrapper.sr.imodel=5 ; for input in DN
      
                                ; store the specifics of this setup, for record-keeping
      pxwrapper.sc.xyzmask=xyzmask
      pxwrapper.sc.zmask=zmask
      pxwrapper.sc.sidemask=sidemask
      pxwrapper.sc.nozero=nozero
      pxwrapper.sc.excerpt=excerpt
      pxwrapper.sc.occult=occultleft+occultright+occulttop+occultbottom
      pxwrapper.sc.brightpixels=maskbright+2*scrubbright; 0, 1, 2 or 3
      pxwrapper.sc.nadjust=nadjust
      
      pxwrapper.sc.gname=cmedate+'_'+int2str(N)+'_'+priordate
      if (cor2) then pxwrapper.sc.gname += '_cor2'
      if (polar) then pxwrapper.sc.gname += '_polar'
      if (c3) then $
        if (excerpt) then pxwrapper.sc.gname += '_c3ex' else $
        pxwrapper.sc.gname += '_c3'
      if (c2) then pxwrapper.sc.gname += '_c2'
      if (cor1) then pxwrapper.sc.gname += '_cor1'
      if (zmask) then pxwrapper.sc.gname += '_zmask'
      if (xyzmask) then pxwrapper.sc.gname += '_xyzmask'
      if (sidemask) then pxwrapper.sc.gname += '_sidemask'
      if (nozero) then pxwrapper.sc.gname += '_nozero'
      if (jumpstart) then pxwrapper.sc.gname += '_jumpstart'
      if (n_elements(tag) ne 0) then pxwrapper.sc.gname += '_'+tag
      if (n_elements(datacut) ne 0) then pxwrapper.sc.gname += '_datacut'
      if (n_elements(datacut) ne 0) then pxwrapper.sc.note += $
        'using datacut of '+int2str(datacut)
      
                                ; ** SECCHI FILENAMES AND DIFF-DATA
      
      rawdata=fltarr(N,N)       ; just a junk element, for the moment
      priors=rawdata            ; ditto
      
      if (modelflag) then begin
          timage=howmodel(N,model)
          for i=0,nmodels-1 do begin
              ren=pr_render(pxwrapper,timage,i)
              rawdata=[[[rawdata]],[[ren]]]
              priors=[[[priors]],[[ren*0.0]]]
          end
      end
      
      if (cor2) then begin
          
          cmecheck=find_cdata(cmedate,/cor2,ab='a',/justtime)
          if (priordate eq 'back') then priorcheck='back' else $
            priorcheck=find_cdata(priordate,/cor2,ab='a',/justtime)
          dcA=load_cor(cmecheck,'a',N=N,hdr=hdrACor2,fname=cmeA,$
                       prep=prep,polar=polar)
          
;print,'cmeA: ',cmeA
;if (file_exist(cmeA)) then print,' exists' else print,' does not exist'
          
          dpA=load_cor(priorcheck,'a',N=N,hdr=hdrACor2,prep=prep,polar=polar)
;help,dcA
;print,minmax(dcA)
;help,dpA
;print,minmax(dpA)
;stop
; debug, help, problem, fix
          
          dcB=load_cor(cmecheck,'b',N=N,hdr=hdrBCor2,fname=cmeB,$
                       prep=prep,polar=polar)
          dpB=load_cor(priorcheck,'b',N=N,hdr=hdrBCor2,prep=prep,polar=polar)
          
                                ; if no files, abort early
          if (n_elements(dcA) eq 1 or n_elements(dcB) eq 1 or $
              n_elements(dpA) eq 1 or n_elements(dpB) eq 1) then return
          
          if (prep eq 0) then begin
                                ; units are in DN, shift to MSB
              dcA=dcA*2.7E-12
              dcB=dcB*2.8E-12
              dpA=dpA*2.7E-12
              dpB=dpB*2.8E-12
          end
          
;          save,dcA,dcB,file='dcab.sav'
          
          rawdata=[[[rawdata]],[[dcA]],[[dcB]]]
          priors=[[[priors]],[[dpA]],[[dpB]]]
          
          if (pretty) then begin
                                ; also make a pretty picture set (inefficient duplication here, but
                                ; performance is not a worry for prep
              pset(pcount++)=load_cor(cmecheck,'a',hdr=hdr,prep=prep,$
                                      subtract=priorcheck,/pointer,polar=polar)
              pset(pcount++)=load_cor(cmecheck,'b',hdr=hdr,prep=prep,$
                                      subtract=priorcheck,/pointer,polar=polar)
          end
          
      end
      
                                ; ** LASCO FILENAMES
      
      if (c3) then begin
          
          cmec3=find_cdata(cmedate)
          file_copy,cmec3,file_basename(cmec3),/overwrite
          lasco_prep,file_basename(cmec3),hdrLC3,dcL
          calfac=c3_calfactor(hdrLC3)
          
          if (priordate eq 'back') then begin
              dpL=getbkgimg(hdrLC3,/ffv)
          end else begin
              priorc3=find_cdata(priordate)
              file_copy,priorc3,file_basename(priorc3),/overwrite
              lasco_prep,file_basename(priorc3),hdrLignore,dpL
          end
          
          propalter=calfac ; /msbperdn ; normalize DN for later bulk convert
          
                                ; rotate image
          DEFSYSV, "!DRADEG", 180.0d/!DPI
          DEFSYSV, "!DDTOR", !DPI/180.0d
          if (tag_exist(hdrLC3,'simdate')) then begin
              obsdate=hdrLC3.simdate
          end else begin
              get_fits_time,hdrLC3,obsdate
          end
          o1 = get_orbit(obsdate)
          tempxyz = [o1.gse_x,o1.gse_y,o1.gse_z]
          CONVERT_STEREO_COORD, obsdate, tempxyz, 'GSE', 'HAE' 
          tempxyz = tempxyz * 1000.0 ; convert LASCO GSE km to our meters
          angle=festival_ecl_solar_north(tempxyz[0],tempxyz[1],tempxyz[2])
          print,'angles: ',angle,hdrLC3.crota1
          tangle=angle          ;  +hdrLC3.crota1
          sunc=get_sun_center(hdrLC3)
          
          if (hdrLC3.CROTA1 EQ 180) then begin
              center=511.5      ;  (1024-1)/2.
              delx = center - sunc.xcen
              dely = center - sunc.ycen
              sunc.xcen = center + delx
              sunc.ycen = center + dely
          end
          
          sunk0=[sunc.xcen,sunc.ycen]
          
;      tv_multi,/log,dcL-dpL,fixed=196,title='LASCO diff, raw'
          dcL = rot(dcL,tangle,1.0,sunk0[0],sunk0[1],/pivot)
          dpL = rot(dpL,tangle,1.0,sunk0[0],sunk0[1],/pivot)
;      tv_multi,/log,dcL-dpL,fixed=196,title='LASCO diff, rotated'
          
          
                                ; end of rotation/rectification
          
          if (excerpt) then begin
              dcL=quickcme_excerpt(dcL)
              dpL=quickcme_excerpt(dpL)
          end
          
          if (pretty) then begin
                                ; also make a pretty picture set, do this before binning down data
              pset(pcount++)=ptr_new(dcL-dpL)
          end
          
;      dcL=1.7*dcL
;      dpL=1.7*dpL
;      dcL=2.0*dcL
;      dpL=2.0*dpL
          
          dcL=congrid(dcL,N,N,/inter)*propalter
          dpL=congrid(dpL,N,N,/inter)*propalter
          
          rawdata=[[[rawdata]],[[dcL]]]
          priors=[[[priors]],[[dpL]]]
          
      end
      
      if (c2) then begin
          cmec2=find_cdata(cmedate,/c2)
          file_copy,cmec2,file_basename(cmec2),/overwrite
          lasco_prep,file_basename(cmec2),hdrLC2,dcLfull
          calfac=c2_calfactor(hdrLC2)
          
          if (priordate eq 'back') then begin
              dpLfull=getbkgimg(hdrLC2,/ffv)
          end else begin
              priorc2=find_cdata(priordate,/c2)
              file_copy,priorc2,file_basename(priorc2),/overwrite
              lasco_prep,file_basename(priorc2),hdrLignore,dpLfull
          end
          
          propalter=calfac ;/msbperdn ; normalize DN for later bulk convert
          
                                ; rotate image
          DEFSYSV, "!DRADEG", 180.0d/!DPI
          DEFSYSV, "!DDTOR", !DPI/180.0d
          if (tag_exist(hdrLC2,'simdate')) then begin
              obsdate=hdrLC2.simdate
          end else begin
              get_fits_time,hdrLC2,obsdate
          end
          o1 = get_orbit(obsdate)
          tempxyz = [o1.gse_x,o1.gse_y,o1.gse_z]
          CONVERT_STEREO_COORD, obsdate, tempxyz, 'GSE', 'HAE' 
          tempxyz = tempxyz * 1000.0 ; convert LASCO GSE km to our meters
          angle=festival_ecl_solar_north(tempxyz[0],tempxyz[1],tempxyz[2])
          print,'angles: ',angle,hdrLC2.crota1
          tangle=angle          ;  +hdrLC2.crota1
          sunc=get_sun_center(hdrLC2)
          
          if (hdrLC2.CROTA1 EQ 180) then begin
              center=511.5      ;  (1024-1)/2.
              delx = center - sunc.xcen
              dely = center - sunc.ycen
              sunc.xcen = center + delx
              sunc.ycen = center + dely
          end
          
          sunk0=[sunc.xcen,sunc.ycen]
          
          dcLfull = rot(dcLfull,tangle,1.0,sunk0[0],sunk0[1],/pivot)
          dpLfull = rot(dpLfull,tangle,1.0,sunk0[0],sunk0[1],/pivot)
          
                                ; end of rotation/rectification
          
          if (pretty) then begin
                                ; also make a pretty picture set, do this before binning down data
              pset(pcount++)=ptr_new(dcL-dpL)
          end
          
          dcL=congrid(dcLfull,N,N,/inter) * propalter
          dpL=congrid(dpLfull,N,N,/inter) * propalter
          
          rawdata=[[[rawdata]],[[dcL]]]
          priors=[[[priors]],[[dpL]]]
      end
      
      if (cor1) then begin
          
          cmec1date=find_cdata(cmedate,/cor1,ab='a',/justtime)
          if (priordate eq 'back') then priorc1date='back' else $
            priorc1date=find_cdata(priordate,/cor1,ab='a',/justtime)
          
          dcc1A=load_cor(cmec1date,'a',N=N,hdr=hdrACor1,fname=cmec1A,/prep,/cor1)
          dpc1A=load_cor(priorc1date,'a',N=N,hdr=hdr,/prep,/cor1)
          dcc1B=load_cor(cmec1date,'b',N=N,hdr=hdrBCor1,fname=cmec1B,/prep,/cor1)
          dpc1B=load_cor(priorc1date,'b',N=N,hdr=hdr,/prep,/cor1)
          
          rawdata=[[[rawdata]],[[dcc1A]],[[dcc1B]]]
          priors=[[[priors]],[[dpc1A]],[[dpc1B]]]
          
          if (pretty) then begin
                                ; also make a pretty picture set (inefficient duplication here, but
                                ; performance is not a worry for prep
              pset(pcount++)=load_cor(cmecheck,'a',hdr=hdr,/prep,/cor1,$
                                      subtract=priorcheck,/pointer)
              pset(pcount++)=load_cor(cmecheck,'b',hdr=hdr,/prep,/cor1,$
                                      subtract=priorcheck,/pointer)
          end
          
      end
      
                                ; we put a junk element in the beginning as a placeholder, here we remove it
      rawdata=rawdata[*,*,1:*]
      priors=priors[*,*,1:*]
      diff=rawdata-priors
      
      if (debug) then begin
          print,'rawdata min, max',minmax(rawdata)
          print,'priors min, max',minmax(priors)
          print,'diff min, max',minmax(diff)
      end
      
                                ; ** DATA IN
      
                                ; handle missing data
                                ;ie=where(rawdata le 0 or priors gt 0)
                                ;if (ie[0] ne -1) then diff[ie]=0.0
      
                                ; find dmin for later possible munging
      ie=where(diff gt 0)
      if (ie[0] eq -1) then dmin=0.0 else dmin=min(diff[ie])
      
                                ; handle negative numbers in difference images
      ie=where (diff lt 0)
      if (ie[0] ne -1) then diff[ie]=0.0
      
                                ; ** FITS INGEST
      iele=0
      if (modelflag) then begin
          for i=0,nmodels-1 do ++iele
      end
      if (cor2) then begin
          if (polar) then inhdr=hdrBCor2[0] else inhdr=hdrBCor2
          p_ingest_fits,cmeA,N=N,pxwrapper=pxwrapper,iele=iele,inhdr=inhdr
                                ; compensate for SECCHI frames that are actually 2x3=6sec
          if (long(pxwrapper.sr.exptime[iele]) eq 2) then $
            pxwrapper.sr.exptime[iele]=6.0
          if (polar) then pxwrapper.sr.polar[iele]='p'
          ++iele
          
          if (polar) then begin
                                ; very inefficient, we just want to duplicate elements in pxwrapper
                                ; but the easiest way is just reread the same file
              p_ingest_fits,cmeA,N=N,pxwrapper=pxwrapper,iele=iele,$
                inhdr=hdrACor2[1]
                                ; compensate for SECCHI frames that are actually 2x3=6sec
              if (long(pxwrapper.sr.exptime[iele]) eq 2) then $
                pxwrapper.sr.exptime[iele]=6.0
              pxwrapper.sr.polar[iele]='p'
              pxwrapper.sr.fan[iele]=120.0
              ++iele
              p_ingest_fits,cmeA,N=N,pxwrapper=pxwrapper,iele=iele,$
                inhdr=hdrACor2[2]
                                ; compensate for SECCHI frames that are actually 2x3=6sec
              if (long(pxwrapper.sr.exptime[iele]) eq 2) then $
                pxwrapper.sr.exptime[iele]=6.0
              pxwrapper.sr.polar[iele]='p'
              pxwrapper.sr.fan[iele]=240.0
              ++iele
          end
          
          if (polar) then inhdr=hdrBCor2[0] else inhdr=hdrBCor2
          p_ingest_fits,cmeB,N=N,pxwrapper=pxwrapper,iele=iele,inhdr=inhdr
                                ; compensate for SECCHI frames that are actually 2x3=6sec
          if (long(pxwrapper.sr.exptime[iele]) eq 2) then $
            pxwrapper.sr.exptime[iele]=6.0
          if (polar) then pxwrapper.sr.polar[iele]='p'
          ++iele
          
          if (polar) then begin
                                ; very inefficient, we just want to duplicate elements in pxwrapper
                                ; but the easiest way is just reread the same file
              p_ingest_fits,cmeB,N=N,pxwrapper=pxwrapper,iele=iele,$
                inhdr=hdrBCor2[1]
                                ; compensate for SECCHI frames that are actually 2x3=6sec
              if (long(pxwrapper.sr.exptime[iele]) eq 2) then $
                pxwrapper.sr.exptime[iele]=6.0
              pxwrapper.sr.polar[iele]='p'
              pxwrapper.sr.fan[iele]=120.0
              ++iele
              p_ingest_fits,cmeB,N=N,pxwrapper=pxwrapper,iele=iele,$
                inhdr=hdrBCor2[2]
                                ; compensate for SECCHI frames that are actually 2x3=6sec
              if (long(pxwrapper.sr.exptime[iele]) eq 2) then $
                pxwrapper.sr.exptime[iele]=6.0
              pxwrapper.sr.polar[iele]='p'
              pxwrapper.sr.fan[iele]=240.0
              ++iele
          end
          
      end
      if (c3) then begin
          c3iele=iele
          p_ingest_fits,cmec3,N=N,pxwrapper=pxwrapper,iele=iele,inhdr=hdrLC3
          ++iele
      end
      if (c2) then begin
          cor2iele=iele
          p_ingest_fits,cmec2,N=N,pxwrapper=pxwrapper,iele=iele,inhdr=hdrLC2
          ++iele
      end
      if (cor1) then begin
          p_ingest_fits,cmec1A,N=N,pxwrapper=pxwrapper,iele=iele,inhdr=hdrACor1
          p_ingest_fits,cmec1B,N=N,pxwrapper=pxwrapper,iele=iele+1,inhdr=hdrBCor1
      end
      
      if (excerpt and c3) then begin
                                ; subsection alteration
          pxwrapper.sr.l0tau[c3iele]=pxwrapper.sr.l0tau[c3iele]*0.5
          pxwrapper.sr.tau[c3iele]=pxwrapper.sr.tau[c3iele]*0.5
                                ; also compensate for fact that ctr is overbinned if we excerpted
          k0=p_getk0(pxwrapper,c3iele)
          k0=2*k0 - ( (N-1)/2.0 )
          pxwrapper.sr.k0[c3iele]=ptr_new(k0)
          sunk0=p_getk0(pxwrapper,c3iele,/sun)
          sunk0=2*sunk0 - ( (N-1)/2.0 )
          pxwrapper.sr.sunk0[c3iele]=ptr_new(sunk0)
                                ; dvoxfrac, flibm and istd will be recomputed in p_osetup later
      end
      
      
                                ; ** NOISE
                                ;  noise(DN_sec)=noise(DN_tot)/6sec=noise(p*15)/6sec
                                ;  noise(p) = sqrt(p) = sqrt(DN_tot/15) = sqrt(DN_sec*6sec/15)
                                ; so noise(DN_sec)=sqrt(DN_sec*6/15)/6sec = sqrt(6/15)/6.0 * sqrt(DN_sec)
                                ; so noise(DN_sec)=0.105409 * sqrt(DN_sec)
                                ; Also noise total is sqrt of sum of 2 frames, roughly 1.4 each frame:
                                ;   noise(DN_sec_diff) ~= 0.15 sqrt(DN_sec_raw)
                                ;
                                ; Since DN_diff = Q * DN_raw and noise(DN_sec_cme)=0.15 sqrt(DN_sec_raw)
                                ; then  S/N = DN_sec_diff/noise(DN_sec_raw)=Q*DN_sec_raw/0.15 sqrt(DN_sec_raw)
                                ;    so S/N = (Q/0.15) sqrt(DN_sec_raw),
                                ; so S/N > 1 if sqrt(DN_sec_raw > (0.15/Q)
      
      if (flatnoise) then begin
                                ; units are currently in DN, so noise is
                                ; sqrt(DN/15.0) * 15.0 * (N/2048.) * 1.0/sqrt(exp) for 2 frames
                                ; = sqrt(DN) * N * (15/ [sqrt(15) * 2048 * sqr(6.0)] ) * sqrt(2)
                                ; = 0.00109183 * N * sqrt(DN)
          ndiff=sqrt(diff) * 0.00109183 * N/nadjust
      end else begin
                                ; better noise treatment
;      ndiff=p_quicknoise(diff,priors,pxwrapper=pxwrapper,/ingestdn,debug=debug)/nadjust
          print,'**** Levels are ',minmax(rawdata),pxwrapper.sr.msbperdn
          ndiff=p_simplenoise(rawdata,pxwrapper,priors)/nadjust
      end
      
; SN CUTOFF/FEATURES ONLY mod
; mod to extract only features and zero out data where SN<cutoff
      if (sncutoff gt 0) then begin
          isn=where (ratio(diff,ndiff) le sncutoff)
          if (isn[0] ne -1) then diff[isn]=0.0
      end
      
      
      
; *** MASKING
      
      mset=p_getmask(sr=pxwrapper.sr,ratio=maskratio)
                                ;tv_multi,mset,res=3,/ax,title='in quickcme'
                                ; extremely obscure option, cuts off right side of both A & B with mask
      
      if (occultright) then mset[N/2:*,*,*]=0.0
      if (occultleft) then mset[0:N/2,*,*]=0.0
      if (occulttop) then mset[*,N/2:*,*]=0.0
      if (occultbottom) then mset[*,0:N/2,*]=0.0
      
      if (trimc3top) then mset[*,2*N/3:*,c3iele]=0.0
      
                                ; to add a sigma and value to side masks is tricky
                                ; mask is 1 for good data, 0 for bad
                                ; so we want to:
                                ;   1) where mask=0, set data=dmin*0.1
                                ;   2) where mask=0, set sigma=dmin*10
                                ;   3) set mask=1 everywhere
      if (sidemask) then begin
          ie=where(mset eq 0)
          if (ie[0] ne -1) then diff[ie]=dmin*0.1
          if (ie[0] ne -1) then ndiff[ie]=dmin*10.0
          mset=mset*0.0+1.0
      end
      
                                ; for zmasking, the data is the mask, mask=inv data
      if (zmask or xyzmask) then begin
          blank=p_getmask('COR2',N,rocc=-1)
          datumZ=blank*0.0 + dmin*0.1 ; low everywhere
          maskZ= ~blank
          sigmaZ= datumZ * 10   ; wildly noisy
          
          pxwrapper.sr.detector[ict]='z mask'
          mset[*,*,ict]=maskZ
          diff=[[[diff]],[[datumZ]]]
          ndiff=[[[ndiff]],[[sigmaZ]]]
          if (xyzmask) then begin
              pxwrapper.sr.detector[ict+1]='y mask'
              pxwrapper.sr.detector[ict+1]='x mask'
              mset[*,*,ict+1]=maskZ
              mset[*,*,ict+2]=maskZ
              diff=[[[diff]],[[datumZ]],[[datumZ]]]
              ndiff=[[[ndiff]],[[sigmaZ]],[[sigmaZ]]]
          end
          
      end
      
                                ; primitive hot pixel remover:
                                ; 'maskbright' adds to mask and doesn't change data,
                                ; 'scrubbright' actually modifies the data
                                ; both only operate on data, not xyzmasks
      if (maskbright or scrubbright) then begin
          for iframe=0,ict-1 do begin
              dd=diff[*,*,iframe]
              mm=mset[*,*,iframe]
              dcap=percentiles(dd,value=[0.99])
              ibright=where(dd gt dcap)
              for i=0,n_elements(ibright)-1 do begin
                  myi=ibright[i]
                  if (myi ne 0 and myi ne N-1) then $
                    if (dd[myi] gt dd[myi-1] + dd[myi+1]) then begin
                      if (maskbright) then mm[myi]=0 else dd[myi]=0
                  end
              end
              if (maskbright) then mset[*,*,iframe]=mm else diff[*,*,iframe]=dd
          end
      end
      
; rotation to ecliptic is now done in p_ingest_fits automatically, when needed
;  if (c3) then quickcme_rotate,pxwrapper,c3iele,diff,ndiff,mset,verbose=verbose
;  if (c2) then quickcme_rotate,pxwrapper,cor2iele,diff,ndiff,mset,verbose=verbose
      
                                ; *** false zero mod, to remove actual data zeroes
      if (nozero) then p_nozero,diff,ndiff
      
                                ; switch to DN if asked
      if (dn) then begin
          diff=diff/msbperdn
          ndiff=ndiff/msbperdn
      end
      
      if (n_elements(mxit) ne 0) then pxwrapper.sp.mxit=mxit
      
      if (n_elements(datacut) ne 0) then begin
      ; a typical difference image may want datacut=1e-11 or so
          ied=where (diff le datacut)
          if (ied[0] ne -1) then diff[ied]=0.0
      end

      save,pxwrapper,diff,ndiff,mset,rawdata,priors,$
        file=savestem+'_inputdata.sav'
      
      if (n_elements(pset) ne 0) then save,pset,file=savestem+'_pretty.sav'
      
                                ; ** CHECK S/N
      
      if (checksn) then begin
          
          sn=ratio(diff,ndiff)*mset > 1.0

          tsetd=make_array(/string,pxwrapper.sr.Nd,value='Data')
          tsetn=make_array(/string,pxwrapper.sr.Nd,value='Noise')
          tsetm=make_array(/string,pxwrapper.sr.Nd,value='Masks')
          tsets=make_array(/string,pxwrapper.sr.Nd,value='S/N')
          
          tset=[tsets,tsetm,tsetn,tsetd]
          
          tv_multi,title='data/noise/masks/sn',labels=tset,$
            [[[sn]],[[mset]],[[ndiff]],[[diff]]],$
            fixed=128,/raw,/log,/unique,line=pxwrapper.sr.Nd,/showrange,oom=7

;      print,'models (and Istds) are: ',$
;        pxwrapper.sr.imodel,'(',pxwrapper.sr.istd,')'
      end

;  tv_multi,res=3,diff,title='one',/local,/log

  end else begin

      restore,/v,savestem+'_inputdata.sav'

  end

  if (setuponly) then return

  p_generate_pixon,'classic',pxwrapper,oPixon
;  help,diff,ndiff,mset,pxwrapper.sr.Nd

  p_insert_data,pxwrapper,diff,oPixon,sigma=ndiff,masks=mset

  if (jumpstart) then begin
      sn=ratio(diff,ndiff)*mset > 1.0
    startimage=simple3drv(sn,pxwrapper=pxwrapper)
    startimage = startimage * max(diff)/max(pr_render(pxwrapper,startimage))
    threeview,startimage,/log,/pixon,oom=2
;    save,startimage,file='startimage.sav'
    oPixon->replace,image=startimage
  end

  print,'Now starting Pixon reconstruction.'
  
  if (jumpstart) then oPixon->full else if (pseudo) then oPixon->pseudo,/restart else oPixon->full,/restart
 
  solution_image=oPixon->get(/im,/nop)
  solution_renders=stereo_project(pxwrapper,oPixon)
  input_data=oPixon->get(/data,/nop)

  p_update_runstats,pxwrapper,oPixon

  save,pxwrapper,oPixon,solution_image,solution_renders,input_data,$
    file=savestem+'_solved.sav'
  
  print,savestem+'_solved.sav' 

  if (standardplots) then p_standardplots,/best,pxwrapper,oPixon,/mega,mxit=400

END
