;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
pro quickmovie_showme,diffsetfile,iloops=iloops,istart=istart,iend=iend,$
                      dmax=dmax,slides=slides,wait=wait,winid=winid,quiet=quiet

  quiet=setyesno(quiet)

  restore,diffsetfile
  print,diffsetfile

  if (n_elements(iloops) eq 0) then iloops=0
  if (n_elements(istart) eq 0) then istart=0
  if (n_elements(iend) eq 0) then iend=n_elements(dpset)-1
  if (n_elements(wait) eq 0) then wait=0.5
  slides=setyesno(slides)

  N= n_elements((*dpset[0])[*,0,0])
  mask=p_getmask('COR2',N,rocc=4.0)
  maskpair=[[[mask]],[[mask]]]

  if (n_elements(dmax) eq 0) then begin
      dmax=0
      for i=istart+1,iend do begin
          if (ptr_valid(dpset[i])) then begin
              data=*(dpset[i])
              if (n_elements(data) ne 0) then begin
                  dm=max(*(dpset[i]) * maskpair)
                  dmax=max([dmax,dm])
              end
          end
      end
  end

  flip_colors
  for j=0,iloops do begin

      for i=istart,iend do begin
          if (ptr_valid(dpset[i])) then begin
              data=*(dpset[i]) * maskpair
              if (n_elements(data) ne 0) then begin
                  if (quiet eq 0) then print,titles[i],i
                  if (slides) then begin
                      tv_multi,data,/log,oom=4,wipe=wait,$
                        header=diffsetfile+' '+titles[i],$
                        fixed=256,winid=winid,max=dmax
                  end else begin
                      tv_multi,data,max=dmax,/log,winid=0,oom=2,$
                        header=titles[i],fixed=256,title=titles[i],wipe=wait
                  end
              end
          end
      end
 
  end
  flip_colors

end


pro quickmovie,startdate,numframes,N=N,type=type

  if (n_elements(type) eq 0) then type='running'; or 'background'

  if (n_elements(startdate) eq 0) then begin 
      cmedate=""
      print,'Enter the datestamp to start with, e.g. 20071104_222220'
      read,startdate
 end

;  if (n_elements(numframes) eq 0) then begin
;      numframes=10
;      print,'Enter the number of frames to use, e.g. 10'
;      read,numframes
;  end
  if (n_elements(numframes) eq 0) then numframes=48; max of 48 frames/day

  if (n_elements(N) eq 0) then N=256

  daypair=strsplit(startdate,'_',/extract)
  day=daypair[0]
  if (n_elements(daypair) gt 1) then cmetime=daypair[1]

  tbdira=getenv('SECCHI_P0')+'/a/cor2/'
  tbdirb=getenv('SECCHI_P0')+'/b/cor2/'

  dcA=0.0
  dcB=0.0

  dpset=ptrarr(numframes)

  cmedirA= tbdira + day + '/'; + day + '_' + cmetime + '_0B4c2A.fts'
  cmedirB= tbdirb + day + '/'; + day + '_' + cmetime + '_0B4c2B.fts'
  if (n_elements(cmetime) ne 0) then begin
      cmeA= cmedirA + day + '_' + cmetime + '_0B4c2A.fts'
      cmeB= cmedirB + day + '_' + cmetime + '_0B4c2B.fts'
  end

  filelistA=file_search(cmedirA,"*B4*.fts",count=icount)
  iele=0
  if (n_elements(cmetime) ne 0) then iele=(where(filelistA eq cmeA))[0]
  filelistB=file_search(cmedirB,"*B4*.fts",count=icount)

  iday=long(strmid(cmedirA,2,2,/reverse))
  iday=iday+1
  newday=strtrim(string(iday),2)
  if (strlen(newday) lt 2) then newday='0'+newday
  cmedirA2=strmid(cmedirA,0,strlen(cmedirA)-3) + newday + '/'
  cmedirB2=strmid(cmedirB,0,strlen(cmedirA)-3) + newday + '/'
  filelistA=[filelistA,file_search(cmedirA2,"*B4*.fts",count=icount)]
  filelistB=[filelistB,file_search(cmedirB2,"*B4*.fts",count=icount)]

  jele=min([numframes,n_elements(filelistA)-1-iele])
  print,numframes,n_elements(filelistA),iele,jele

  titles=strarr(n_elements(filelistA))

  ict=0
  for i=iele,iele+numframes-1 do begin

      cmeA=filelistA[i]
      cmeB=filelistB[i]
      if (file_exist(cmeA)) then print,cmeA
;      if (file_exist(cmeB)) then print,cmeB
  
      cmetrack=strmid(cmeA,16,6,/reverse)

      print,'Checking for ',cmetrack

      if (file_exist(cmeA) eq 1 and file_exist(cmeB) eq 1) then begin

          print,'Ingesting ',cmetrack

          titles[ict]=cmetrack

          dpA=dcA
          dpB=dcB
          dcA=rebin(sccreadfits(cmeA,hdrA),N,N)
          dcB=rebin(sccreadfits(cmeB,hdrB),N,N)*2.8/2.7
  
          hdrA.exptime=1        ; required for scc_getbkgimg
          hdrB.exptime=1        ; required for scc_getbkgimg
    
          if (type eq 'background' or i eq 0) then begin

              dpA=rebin(scc_getbkgimg(hdrA,/match,/totalb),N,N)
              dpB=rebin(scc_getbkgimg(hdrB,/match,/totalb),N,N)

          end

          ie= where (dcA lt 0)
          if (ie[0] ne -1) then dcA[ie]=0.0
          ie= where (dcB lt 0)
          if (ie[0] ne -1) then dcB[ie]=0.0
          ie= where (dpA lt 0)
          if (ie[0] ne -1) then dpA[ie]=0.0
          ie= where (dpB lt 0)
          if (ie[0] ne -1) then dpB[ie]=0.0
  
          diff=[[[dcA-dpA]],[[dcB-dpB]]]

          dpset[ict]=ptr_new(diff)

;          tv_multi,diff,/log,/cap
  
          ++ict
      end

  end

  save,dpset,titles,file=startdate+'.set'
  print,'saved diffs in ',startdate+'.set'

end
