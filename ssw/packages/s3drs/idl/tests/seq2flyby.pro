;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; silly little wrapper

PRO seq2flyby,seq,N=N,regen=regen,fixed=fixed,oom=oom

  seq=deriveseq(seq)

  regen=setyesno(regen)

  if (n_elements(N) eq 0) then N='128' else N=int2str(N)
  if (n_elements(oom) eq 0) then oom=4

  for i=0,n_elements(seq)-1 do begin

      fname=seq[i] + '_'+ N + '_solved.sav'
      if (file_exist(fname)) then begin
          restore,/v,fname
          ffile=seq[i]+'flyby'
          if (regen eq 1 or file_exist(ffile) eq 0) then begin
              img=stereo_fetch(pxwrapper,oPixon,/im)
              flyby,img,/grand,savefile=ffile,/mass,/add,angleper=5
          end
;      loadct,1
;      loadct,5
;      show_flyby,ffile,/movie,/log,/cap,oom=oom,fixed=fixed
      end

  end

END
