;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; Premise: with Paul's renderer, supposably the data centers do not
;    need to be anywhere in particular with in the image, only the
;    image cube center has to be the sun-center (due to our rotations
;    for determining LOS).
;
; goal-- first create a 64x64 shell sim and recon.
;        Then, take the data, shift one data frame by 4 pixels left
;        in X and shift the corresponding sun and optical center,
;        and recon again.  Since all the data is in one quandrant,
;        the two recons should be identical if Paul's render is
;        doing what we expect.
;


PRO offcentertest

  p_modeltests,'shell',64,gamma=[0,90,0],theta=[0,0,90]

  rename,'shell64_0,90,0_noise1.00000_mscale1.00000__datum.sav','center.sav'

  restore,/v,'shell64_0,90,0_noise1.00000_mscale1.00000__datum.sav'

  ; extract
  dat=datum[*,*,0]
  sig=noise[*,*,0]

  ; shift data by ishift

  ishift=4

  dat2=dat*0.0
  sig2=sig*0.0

  for i=0,63-ishift do dat2[i,*]=dat[i+ishift,*]
  for i=0,63-ishift do sig2[i,*]=sig[i+ishift,*]

  ; replace
  datum[*,*,0]=dat2
  noise[*,*,0]=sig2

  ; shift center reference by ishift
  ctr=p_getk0(pxwrapper,0)
  sun=p_getk0(pxwrapper,0,/sun)

  print,ctr,sun

  ctr[0]=ctr[0]-ishift
  sun[0]=sun[0]-ishift

  pxwrapper.sr.k0[0]=ptr_new(ctr)
  pxwrapper.sr.sunk0[0]=ptr_new(sun)

  print,p_getk0(pxwrapper),p_getk0(pxwrapper,/sun)

  ; now generate and run

  p_generate_pixon,'classic',pxwrapper,oPixon
  p_insert_data,pxwrapper,datum,oPixon,sigma=noise,masks=masks

  oPixon->full,/restart

  save,pxwrapper,oPixon,file='offcenter.sav'

  p_standardplots,pxwrapper,oPixon,/best,/quick

  oc_pxwrapper=pxwrapper
  oc_oPixon=oPixon

  restore,/v,'center.sav'
  p_standardplots,pxwrapper,oPixon,/best,/quick

  c_pxwrapper=pxwrapper
  c_oPixon=oPixon

END

