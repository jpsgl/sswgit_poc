;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; can do /back, /preframe aka diff, or defaults to running difference

PRO simpleplotcms,file
  if (file_exist(file)) then begin
      restore,/v,file
      !P.MULTI=[0,0,3]
      plot,cmset[*,0],title='CM X'
      plot,cmset[*,1],title='CM Y'
      plot,cmset[*,2],title='CM Z'
      !P.MULTI=0
  end

END

PRO simplerapidfire,N,back=back,seq=seq,checksn=checksn,preframe=preframe,$
                    ksize=ksize,gradient=gradient,renorm=renorm,noplot=noplot,$
                    startdate=startdate,enddate=enddate,nokernel=nokernel

  if (n_elements(N) eq 0) then N=64

  nokernel=setyesno(nokernel)
  noplot=setyesno(noplot)

  ; run cmerapidfire to prepare files
  cmerapidfire,N,back=back,seq=seq,checksn=checksn,preframe=preframe,$
    setuponly=1,startdate=startdate,enddate=enddate

  print,'Simple recon on this sequence: ',seq

  cmset=fltarr(n_elements(seq),3)

  ; now run our simple recon
  for i=1,n_elements(seq)-1 do begin
      fname=seq[i] + '_' + int2str(N) + '_inputdata.sav'
      if (file_exist(fname)) then begin

          restore,/v,fname
          diff=diff[*,*,0:1]
          ndiff=ndiff[*,*,0:1]
          mset=mset[*,*,0:1]
          pxwrapper.sr.Nd=2

          if (nokernel) then begin

              sol=simple3drv(diff*mset,pxwrapper=pxwrapper,/usemax)

          end else begin

              sol=simple3dsolve(pxwrapper,diff*mset,ndiff,nokernel=nokernel,$
                                ksize=ksize,gradient=gradient,renorm=renorm)

          end
          

          cm=centroid3d(sol)
          print,seq[i],cm
          cmset[i,*]=cm

          save,sol,file=seq[i]+'_simplesol.sav'

          if (noplot eq 0) then begin
              threeview,fixed=128,sol,/log,/pixon,oom=1,/cm,threed,/noplot

              m=p_getmask(sr=pxwrapper.sr,iele=0,ratio=3.0)
              m3=[[[m]],[[m]],[[m]]]
              tv_multi,threed*m3,fixed=128,/log,oom=1
              write_gif,seq[i]+'_simplesol.gif',tvrd()
          end

      end

  end

  save,cmset,seq,file=startdate+'_cms.sav'
  print,'CMs saved as ',startdate+'_cms.sav'

END
