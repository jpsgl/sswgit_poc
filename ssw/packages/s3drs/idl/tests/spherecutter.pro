; test of sphere cutting

restore,/v,'fluxrope128_-45,0,45_noise1.00000_mscale1.00000__solved.sav
N=pxwrapper.sr.ncube
m=p_getmask('COR2',N,rpixels=rpixels)
mask3D = cube_sphere(N,rpixels[0],r_outer=rpixels[1])
img=stereo_fetch(pxwrapper,opixon,/im)
img2=img*mask3D

threeview,/pixon,mask3d,title='masks',/ps,file='masks.ps'
threeview,/pixon,/log,img,title='sol',/ps,file='sol.ps' 
threeview,/pixon,/log,img2,title='cut',/ps,file='cut.ps'
threeview,/pixon,/log,img-img2,title='sol - cut sol',/ps,file='sol-cut.ps'
