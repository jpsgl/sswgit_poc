;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
PRO sample_model_renders

; load a half shell and render as shell+saito+fcor, shell, saito, fcor
; then ditto for FMed fluxrope

  p_modeltests,/compile; 'howmodel' is in p_modeltests

  shell=howmodel(128,'shell')
  saito=howmodel(128,'saito')

; load a fluxrope model in correct position, timage
  fluxrope=howmodel(128,'fluxrope') ; is automatically in Dec 31 position

  mask=p_getmask('COR2',128)

  shellsaito=shell+saito
  fluxsaito=fluxrope+saito

  prep_3drv,N=128,gamma=[90],/cor2,pxwrapper


  shellview=pr_render(pxwrapper,shell)*mask
  shellsaitoview=pr_render(pxwrapper,shellsaito)*mask
  fluxview=pr_render(pxwrapper,fluxrope)*mask
  fluxsaitoview=pr_render(pxwrapper,fluxsaito)*mask
  saitoview=pr_render(pxwrapper,saito)*mask

  lset=['Shell','Saito','Shell+Saito','Fluxrope','Saito','Fluxrope+Saito']
  dset=[[[shellview]],[[saitoview]],[[shellsaitoview]],$
        [[fluxview]],[[saitoview]],[[fluxsaitoview]]]

  tv_multi,/log,line=3,dset,labels=lset,header='Model Rendering',$
    file='model_renders.ps',/ps,oom=6,/land,$
    rowtitle=['Log intensity B/B0 [1.60E-14,1.60E-08]',$
              'Log intensity B/B0 [1.60E-14,1.60E-08]']

END
