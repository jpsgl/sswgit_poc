;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; makes some slide-like input summary plots, assumes input was generated
; via 'quickcme' as that makes the appropriate file names and gnames expected.
;
; Usage: cmesummary,N,seq=seq,/screen,[/justinput,/justoutput]
;

FUNCTION defaultcmeseq

  seq=['20071231_015220',$
       '20071231_022220',$
       '20071231_025220',$
       '20071231_032220',$
       '20071231_035220',$
       '20071231_042220',$
       '20071231_045220',$
       '20071231_052220',$
       '20071231_055220',$
       '20071231_015220']
  return,seq

END


PRO cmeinputsummary,N,seq=seq,screen=screen,fixed=fixed,oom=oom

  if (n_elements(N) eq 0) then N=128; a guess
  if (n_elements(seq) eq 0) then seq=defaultcmeseq() else $
    if (n_elements(seq) eq 1) then seq=deriveseq(seq)
  if (n_elements(oom) eq 0) then oom=2

  screen=setyesno(screen)

  for i=0,n_elements(seq)-1 do begin

     fname=seq[i] + '_' + int2str(N) + '_inputdata.sav'
;     print,fname
     if (file_exist(fname)) then begin
         restore,fname
         if (stregex(pxwrapper.sc.gname,'lasco',/boolean)) then nd=3 else nd=2
         data=diff[*,*,0:nd-1]
         noise=ndiff[*,*,0:nd-1]
         sn=ratio(data,noise) > 1 < 10
         dset=[[[data]],[[sn]]]
         l1=['A diff','B diff']
         if (nd eq 3) then l1=[l1, 'Lasco diff']
         l2=['A, S/N > 1 < 10','B, S/N > 1 < 10']
         if (nd eq 3) then l2=[l1, 'Lasco, S/N > 1']
         lset=[l1,l2]

         if (screen) then begin
             tv_multi,labels=lset,/log,/raw,title=pxwrapper.sc.gname,dset,$
               line=nd,/showrange,/reverse,fixed=fixed,/unique,oom=oom
             fname2=seq[i]+'_summary.gif'
             write_gif,fname2,tvrd()
             print,'wrote ',fname2
         end

         psfile=pxwrapper.sc.gname + '_input.ps'
         tv_multi,labels=lset,/log,/raw,title=pxwrapper.sc.gname,dset,$
           line=nd,/showrange,/reverse,/ps,file=psfile; /unique
     end

  end

END

PRO cmeoutputsummary,N,seq=seq,screen=screen,fixed=fixed,$
                     noxyz=noxyz,justxyz=justxyz,oom=oom,max=max

  if (n_elements(N) eq 0) then N=128; a guess
  if (n_elements(seq) eq 0) then seq=defaultcmeseq() else $
    if (n_elements(seq) eq 1) then seq=deriveseq(seq)
  screen=setyesno(screen)
  noxyz=setyesno(noxyz)
  justxyz=setyesno(justxyz)

  if (n_elements(oom) eq 0) then oom=2

  pixon_env,'c',/local

  for i=0,n_elements(seq)-1 do begin

     fname=seq[i] + '_' + int2str(N) + '_solved.sav'

     if (file_exist(fname)) then begin
         restore,fname
         
         p_update_runstats,pxwrapper,oPixon

         if (stregex(pxwrapper.sc.gname,'lasco',/boolean)) then nd=3 else nd=2

         threeview,solution_image,solview,/noplot,/pixon

         data=(stereo_fetch(pxwrapper,oPixon,/data))[*,*,0:nd-1]
         renders=(stereo_fetch(pxwrapper,oPixon,/ren))[*,*,0:nd-1]
         masks=(stereo_fetch(pxwrapper,oPixon,/mask))[*,*,0:nd-1]

         data=data*masks
         renders=renders*masks

         blankd=data[*,*,0]*0.0 + max(data)
         blankr=renders[*,*,0]*0.0 + max(renders)
         if (nd eq 2 and noxyz eq 0) then data=[[[data]],[[blankd]]]
         if (nd eq 2 and noxyz eq 0) then renders=[[[renders]],[[blankr]]]

         if (noxyz) then dset=[[[renders]],[[data]]] else if (justxyz) then $
           dset=solview else dset=[[[solview]],[[renders]],[[data]]]

         l1=['A input','B input']
         if (nd eq 3) then l1=[l1, 'Lasco input'] else if (noxyz eq 0) then $
           l1=[l1,'(placeholder)']
         l2=['A output','B output']
         if (nd eq 3) then l2=[l1, 'Lasco output'] else if (noxyz eq 0) then $
           l2=[l2,'(placeholder)']
         l3=['Image X','Image Y','Image Z']

         if (noxyz) then lset=[l2,l1] else if (justxyz) then $
           lset=l3 else lset=[l3,l2,l1]      ; note reversed

         title='Q = ' + int2str(pxwrapper.sc.q) + ', ' + pxwrapper.sc.gname

         if (noxyz) then nper=nd else nper=3

         if (screen) then begin
             tv_multi,labels=lset,/log,/raw,title=title,dset,$
               line=nper,/showrange,oom=oom,/cap,fixed=fixed,max=max; /unique
             fname2=seq[i]+'_out.gif'
             write_gif,fname2,tvrd()
             print,'wrote ',fname2
         end

         psfile=pxwrapper.sc.gname + '_out.ps'
         tv_multi,labels=lset,/log,/raw,title=title,dset,$
           line=3,/showrange,oom=oom,/ps,file=psfile,/cap,max=max; /unique


     end

  end

END

 

PRO cmesummary,N,seq=seq,screen=screen,fixed=fixed,$
               justinput=justinput,justoutput=justoutput,$
               noxyz=noxyz,justxyz=justxyz,oom=oom,max=max

  if (n_elements(N) eq 0) then N=128; a guess
  if (n_elements(seq) eq 0) then seq=defaultcmeseq() else $
    if (n_elements(seq) eq 1) then seq=deriveseq(seq)

  screen=setyesno(screen)
  justinput=setyesno(justinput)
  justoutput=setyesno(justoutput)

  if (justoutput eq 0) then $
    cmeinputsummary,N,seq=seq,screen=screen,fixed=fixed,oom=oom

  if (justinput eq 0) then $
    cmeoutputsummary,N,seq=seq,screen=screen,fixed=fixed,$
    noxyz=noxyz,justxyz=justxyz,oom=oom,max=max

END
