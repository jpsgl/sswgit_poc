;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; This test script loads up the Dec31st CME, uses manually inputed
; values (created by looking at the plots this throws up) to rotate a
; fluxrope model to roughly match, then sticks the fluxrope into
; a Pixon structure and tries to reconstruct it from those actual
; STEREO viewpoints.  This helps us determine whether STEREO can
; indeed resolve a fluxrope shape at its current separation.
;
; Note that flag /ninety makes it do a typical 90-degree sim result
;
; The /saito method adds saito for the _noise_ calculation while
; keeping it away from the image for the actual data (which is
; therefore presumed to be a difference image, where the saito
; drops out).
;
; /lascoadd includes a middle view for comparing STEREO vs STEREO+LASCO
;

PRO fakedec31,nadjust=nadjust,ninety=ninety,saito=saito,flatnoise=flatnoise,$
              nomask=nomask,lascoadd=lascoadd

; note this is for N=128

  if (n_elements(nadjust) eq 0) then nadjust=1.0
  ninety=setyesno(ninety)
  saito=setyesno(saito)
  nomask=setyesno(nomask)
  flatnoise=setyesno(flatnoise)
  lascoadd=setyesno(lascoadd)

; open SECCHI pxwrapper to get pxwrapper

  restore,'../arch/dec31seq128/20071231_032220_128_solved.sav',/v
  pxsecchi=pxwrapper

; get view, position of current solution
;;  threeview,res=2,/log,stereo_fetch(pxsecchi,oPixon,/im),/pixon,oom=2

; load a fluxrope model in correct position, timage
  p_modeltests,/compile
  synthetic=howmodel(128,'fluxrope') ; is automatically in Dec 31 position
;;  restore,/v,$
;;    '../fluxrope/fluxrope128_0,90,0_noise1.00000_mscale1.00000__datum.sav
;threeview,/log,res=1,timage,/pixon,oom=2
; move it to correct position and check vs 
;;  synthetic=timage
;;  cuberot3d,synthetic,[0.0,20.0,-90.0],/reverse
;;  cuberot3d,synthetic,[90.0,0.0,0.0]
;;  cuberot3d,synthetic,[0.0,35.0,0.0]
;;  threeview,/log,res=1,synthetic,/pixon,oom=2

; add optional saito
  if (saito) then $
    restore,/v,'../saito/saito64_0,90,0_noise1.00000_mscale1.00000__datum.sav'
  if (saito) then saitoimg=rebin(timage,128,128,128) else $
    saitoimg=fltarr(128,128,128)*0.0

; munge and prep

  gamma=pxsecchi.sr.rho[0:1]; e.g.   120.11215       76.140512
  if (ninety) then gamma=[53.0,143.0]
  theta=pxsecchi.sr.phi[0:1]
  LL=pxsecchi.sr.LL[0:1]

  midLL=total(LL)/2.0
  midtheta=total(theta)/2.0
  midgamma=total(gamma)/2.0
  if (lascoadd) then begin
      LL=[LL,midLL]
      theta=[theta,midtheta]
      gamma=[gamma,midgamma]
  end

  prep_3drv,px2,N=128,gamma=gamma,theta=theta,LL=LL,/cor2

; render it ala SECCHI and view

  saitodata=pr_render(px2,saitoimg)

  d1=pr_render(pxsecchi,synthetic,0)
  d2=pr_render(pxsecchi,synthetic,1)
  if (lascoadd) then d3=pr_render(px2,synthetic,2)

  if (lascoadd) then data=[[[d1]],[[d2]],[[d3]]] else data=[[[d1]],[[d2]]]

  m1=p_getmask(sr=pxsecchi.sr,iele=0)
  m2=p_getmask(sr=pxsecchi.sr,iele=1)
  if (lascoadd) then m3=p_getmask(sr=px2.sr,iele=1)

  if (lascoadd) then masks=[[[m1]],[[m2]],[[m3]]] else masks=[[[m1]],[[m2]]]
  if (nomask) then masks=(masks*0.0) + 1.0

;;  tv_multi,data*masks,/log,res=2,/cap,oom=2

; get noise

  if (flatnoise) then $
    noise=((sqrt((data+saitodata)/2.7E-14))*2.7E-14)/nadjust else $
    noise=p_quicknoise(data,pxwrapper=pxsecchi)/nadjust
;    noise=p_quicknoise(data+saitodata,pxwrapper=pxsecchi)/nadjust

;;;;  p_nozero,data,noise

  tv_multi,ratio(data,noise)*masks>1,/log

; now save and run

  save,file='temp.sav',px2,data,noise,masks,synthetic

  p_generate_pixon,'classic',px2,pxPixon
  p_insert_data,px2,data,pxPixon,sigma=noise,masks=masks

  pxPixon->full,/restart

  save,px2,pxPixon,file='sol.sav'

; plot (vs synthetic)

  p_standardplots,px2,pxPixon,synthetic=synthetic,/best; ,/flyby

; ship as PDF to John

;;  psmerge -ofakedec31.eps *.ps 
;;  ps2pdf fakedec31.eps fakedec31.pdf

END
