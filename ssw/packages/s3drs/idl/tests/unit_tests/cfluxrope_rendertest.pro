;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; takes the save file of the previous cflux, puts it
; into our structures, then compares renderers
;

on_error,2
fin='cfluxrope_070615152144.dat'

restore,/verbose,fin
; data is the original data
; mdata is the solution
; image is the solution image
; timage is the original sim image
; sigma is the sigma array
; userstruct, sr, sp, sn, sc, so are the structures
;

Nd=2
N=32
prep_3drv,pxwrapper,views=Nd,N=N,gamma=[0.0,90.0]

pxwrapper.sr.chl=sr.chl
pxwrapper.sr.an=sr.an
pxwrapper.sr.polar=sr.polar
pxwrapper.sr.fan=sr.fan
pxwrapper.sr.LL=sr.ll
pxwrapper.sr.tau=replicate(sr.tau,2)
pxwrapper.sr.roco=sr.roco
pxwrapper.sr.lla0=sr.lla0
for i=0,1 do *(pxwrapper.sr.k0[i])=sr.k0[*,i]
for i=0,1 do *(pxwrapper.sr.sunk0[i])=*(pxwrapper.sr.k0[i])
pxwrapper.sr.z0=sr.z0
pxwrapper.sr.dvox=sr.d/sr.l0tau
pxwrapper.sr.l0=sr.l0
pxwrapper.sr.l0tau=replicate(sr.l0tau,2)
pxwrapper.sr.eps=sr.eps
pxwrapper.sr.set=sr.set
pxwrapper.sr.trw=sr.trw
pxwrapper.sr.trrrs=sr.trrrs
pxwrapper.sr.trp=sr.trp
pxwrapper.sr.trvs=sr.trvs
pxwrapper.sr.interp=sr.interp
pxwrapper.sr.psp=sr.psp
pxwrapper.sr.n0=sr.n0
pxwrapper.sr.istd=replicate(sr.istd,2)
pxwrapper.sr.as=sr.as

pxwrapper.sp.anneal_3d=sp.anneal_3d
pxwrapper.sp.reltol=sp.reltol
pxwrapper.sp.abstol=sp.abstol
pxwrapper.sp.mxit=sp.mxit
pxwrapper.sp.reltol=sp.reltol
pxwrapper.sp.jsched=sp.jsched
pxwrapper.sp.msched=sp.msched
pxwrapper.sp.rsched=sp.msched
pxwrapper.sp.starter=sp.starter
pxwrapper.sp.npo=sp.npo
pxwrapper.sp.npxn=sp.npxn

pxwrapper.sn.noisemod=sn.noisemod
pxwrapper.sn.seed=sn.seed
pxwrapper.sn.fsig=sn.fsig
pxwrapper.sn.ftol=sn.ftol

pxwrapper.sr.SAT_NAME=sc.SAT_NAME

pxwrapper.sc.GNAME=sc.GNAME
pxwrapper.sc.TERM=sc.TERM
pxwrapper.sc.NOTE=sc.NOTE
pxwrapper.sc.T00=sc.T00
pxwrapper.sc.TOTALTIME=sc.TOTALTIME
pxwrapper.sc.TDSF=sc.TDSF
pxwrapper.sc.TDSFT=sc.TDSFT
pxwrapper.sc.VERSION=sc.VERSION
pxwrapper.sc.MACHINE=sc.MACHINE
pxwrapper.sc.DATE=sc.DATE
pxwrapper.sc.TAG=sc.TAG

p_generate_pixon,'classic',pxwrapper,oPixon,image=image,/raw

sandydata=stereo_project(pxwrapper,oPixon)
scaleddata=sandydata*(max(mdata)/max(sandydata))
minus=mdata-scaleddata

!p.multi=[0,1,4]
plot,mdata,font=11,title='Paul Render '+pxwrapper.sr.ch
plot,sandydata,font=11,title='Sandy Render'
plot,scaleddata,font=11,title='Sandy Render, scaled to Paul norm'
plot,minus,font=11,title='Paul-Sandy_scaled'
write_gif,'render_plot_'+pxwrapper.sr.ch+'.gif',tvrd()
!p.multi=0

dset=[[[minus]],[[scaleddata]],[[mdata]]]
l1=['Paul-Sandy_scaled']  
l2=['Sandy render, scaled']
l3='Paul render'
for i=1,Nd-1 do l1=[l1,l1[0]]
for i=1,Nd-1 do l2=[l2,l2[0]]
for i=1,Nd-1 do l3=[l3,l3[0]]
labels=[l1,l2,l3]
tv_multi,dset,res=4,/log,line=Nd,label=labels,$
  title='Render comparison '+pxwrapper.sr.ch,ct=3
write_gif,'render_comp_'+pxwrapper.sr.ch+'.gif',tvrd()
