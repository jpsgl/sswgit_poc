;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
PRO tinkering

pixon_env,'c'
restore,/v,'../dialme/saito.img'; saito
restore,/v,'opixondiff.sav'; pxwrapper, opixondiff

saito=rebin(saito,64,64,64)

pxwrapper.sr.interp=0
prsaito=pr_render(pxwrapper,saito)

wt=opixondiff->get(/wt,/nop)
ie=where(wt eq 0)
masks=wt*0.0+1.0
masks[ie]=0.0

odata=opixondiff->get(/data,/nop)

scale=7.0E-11

test=masks* (odata + prsaito*scale)

tv_multi,fixed=256,test

istop=20
plot,(prsaito*scale*masks)[0:istop,32],yrange=[-10,50]
oplot,test[0:istop,32],psym=2
oplot,odata[0:istop,32]*masks,psym=4

END

PRO flooring

  on_error,2

  pixon_env,'c'
  restore,/v,'cme_minus_pre_solution.sav'; pxwrapper, opixondiff

  wt=opixondiff->get(/wt,/nop)
  ie=where(wt eq 0)
  masks=wt*0.0+1.0
  masks[ie]=0.0

  desiredlevel=5.0; in this case, want to raise difference image by 5.0

  f=fltarr(64,64,3)*0.0 + desiredlevel

  fm=f*masks
;  tv_multi,fixed=256,fm

  flatimg=pr_backproject(pxwrapper,fm)

  threeview,/pixon,fixed=256,flatimg

  odata=opixondiff->get(/data,/nop)

  odata=odata+fm

  tv_multi,fixed=256,odata*masks
  plot,odata*masks,yrange=[-1,22]

  p_insert_data,pxwrapper,odata,oPixondiff
  pxwrapper.sc.gname=pxwrapper.sc.gname+'_floor'+$
    strtrim(string(floor(desiredlevel)),2)


  opixondiff->full,/restart

  save,opixondiff,pxwrapper,file='diff_floor5_solved.sav'



END
