;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; just stuck this into a procesure for archival value

PRO n0test_worked

; test of resolution issues with n0

.reset
on_error,2 

; datum is 0 to 4e4
restore,'temp.sav'; restores 2 default fluxrope datum, timage

n0=1 ; works
;n0=max(timage) ;fails
;n0=max(datum) ; fails
;n0=1e7  ; works but low pixon count
;n0=1e4  ; works
;timage=timage/n0
;datum=datum/n0

print,'max(timage)=',max(timage)
print,'max(datum)=',max(datum)
print,'n0=',n0

prep_3drv,pxwrapper,views=2,N=32,enorm=n0
print,'pxwrapper.sr.n0=',pxwrapper.sr.n0
p_generate_pixon,'classic',pxwrapper,oPixon
p_insert_data,pxwrapper,datum,oPixon

oPixon->Full,/restart

img=oPixon->get(/im,/nop)
render=stereo_project(pxwrapper,oPixon)
print,'max solution=',max(img),' vs max timage=',max(timage)
print,'max renders=',max(render),' vs max datum=',max(datum)


; just stuck this into a procesure for archival value

END

PRO sigmatest_worked

.reset
on_error,2 
N=32
nviews=2
prep_3drv,pxwrapper,views=nviews,N=N
p_generate_pixon,'classic',pxwrapper,oPixon

; datum is 0 to 4e4
restore,'temp.sav',/verb; restores 2 default fluxrope datum

;wt=opixon->get(/wt)
;plot,*wt[0]

;p_insert_data,pxwrapper,datum,oPixon,sigma=sqrt(datum),mask=datum*0.0+1.0; works
p_insert_data,pxwrapper,datum,oPixon; works
opixon->set,sigma=1 ; if not giving a specific noise, use this; works

;wt=opixon->get(/wt)
;plot,*wt[0]
sigma=opixon->get(/sigma,/nop)
print,'sigma is: '
help,sigma
;plot,*sigma[0]

oPixon->Replace,mxit=20
oPixon->Full,/restart
;oPixon->Pseudo,/restart


img=oPixon->Get(/im,/nop)
plot,img

result=stereo_project(pxwrapper,oPixon)
tv_multi,result,res=6

;sigma=opixon->get(/sigma,/nop)
;help,sigma
;plot,sigma
;wt=opixon->get(/wt,/nop)
;help,wt
;plot,wt

END
