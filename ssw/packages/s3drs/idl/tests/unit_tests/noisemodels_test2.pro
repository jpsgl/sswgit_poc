;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
on_error,2

;  signal=3375;  e.g. do 16DN or 64DN
  signal=16;  e.g. do 16DN or 64DN
  set_exptime=1;  e.g. do 1sec or 10sec

  fdataorig = 'cor105.fts'; a sample Level 0.5 SECCHI file
  fdata05 = 'cor105.fts'; a sample Level 0.5 SECCHI file
  fdata10 = 'data10.fts'
  outfname='20061106_131400_15c1A.fts'; what secchi_prep creates
  fnoisein = 'noisein.fts'
  fnoiseout = 'noiseout.fts'

  ; ingest our data L05 file
  data_temp=sccreadfits(fdataorig,hdr05)

; MUNGE DUE TO BAD SAMPLE DATA!
  data_temp=nint (data_temp*0) + signal + hdr05.biasmean

  save_secchi05,data_temp, hdr05, fdata05; NEW


  ; now fetch back to make sure we are okay
  data_L05=sccreadfits(fdata05,hdr05)

  print,'running secchi_prep on L05 data'
  secchi_prep,fdata05,/write_fts
  rename,outfname,fdata10
  data_L10=sccreadfits(fdata10,hdr10)

  ; soho offsetbias = 332; LASCO C1=332, C2=470, C3=319, 
;  exptime = hdr10.exptime

;  dn2photons = 15                ; placeholder or best guess
;  dn2msb = get_calfac(hdr10)
;  flatfield='cor1flatfield.fts'
;  data_flattemp=readfits(flatfield); or should I use sccreadfits?
;  data_flat=data_flattemp[50:2097,50:2097]

  ; MAKE A L05 NOISE FILE

;  rawDN = data_L05 - hdr05.biasmean
;  num_photons = rawDN * dn2photons

;  sigmaPhoton = sqrt(num_photons)
;  sigmaDN = sigmaPhoton / dn2photons

  fractional_error = fractional_noise(data_L05, hdr=hdr05, /subtractbias)
  noise_method1 = data_L10 * fractional_error

;
; Method 2: use secchi_prep directly, likely more accurate and consistent
;  noise = sigmaMSB = secchi_prep{sigmaDN + offsetbias}
;
  ; NOISEPREP METHOD
;  noisedata = uint(sigmaDN + hdranon.biasmean)
;  noisedata = float(sigmaDN)
;  save_secchi05,noisedata, hdr05, fnoisein; NEW

  noise_method2 = secchi_noiseprep(data_L05, hdr05, /subtractbias, /cleanup)

  ; run secchi_prep on noise
;  print,'running secchi_prep on noise'
;  secchi_prep,fnoisein
;  rename,outfname,fnoiseout
;  noise_method2=sccreadfits(fnoiseout,hdrn)
;  noise_method2 = noise_method2 - hdrn.biasmean, never subtract
;                                  biasmean from Level 1.0?

;
; now run a simulated secchi_prep on the noise data to check
;
  ; SIMNOISEPREP METHOD
; noise_method3 = ( (noisedata - hdrn.biasmean) * dn2msb / exptime) * data_flat
;
  sigmaDN = fractional_noise(data_L05, hdr=hdr05, /subtractbias, /sigma)
  noise_method3 = fake_secchi_prep(sigmaDN, hdrn, /use_calimg)

;; now all my output stuff

  print,'exposure time = ',exptime,' sec'
  print,'biases are: L05=',hdr05.biasmean,', L10=',hdr10.biasmean,$
    ' noisedata=',hdrn.biasmean

  print,' '

  print,'data_L05[512,512] is ',data_L05[512,512]
  print,'data_L10[512,512] is ',data_L10[512,512]
  print,'rawDN[512,512] is ',rawDN[512,512]
  print,'dn2photons = ',dn2photons,' (constant)'
  print,'num_photons = ',num_photons[512,512],$
    ' (rawDN * dn2photons)'
  print,'sigmaPhotons = ',sigmaPhoton[512,512],$
    ' (sqrt(num_photons))'
  print,'sigmaDN[512,512] is ',sigmaDN[512,512],$
    ' (sigmaPhoton/dn2photons)'
  print,'flatfield[512,512] is ',data_flat[512,512]
  print,'frac_err[512,512] is ',fractional_error[512,512],$
    ' (sigmaDN/rawDN)*flat'
  print,'noise1[512,512] is ',noise_method1[512,512],$
    ' (data_L10 * frac_err)'
  print,'noisedata[512,512] is ',noisedata[512,512],$
    ' (sigmaDN + bias)'
  print,'noise2[512,512] is ',noise_method2[512,512],$
    ' (secchi_prep(sigmaDN+bias))'
  print,'noise3[512,512] is ',noise_method3[512,512],$
    ' (sigmaDN * dn2msb * flat / exptime)'

  print,'  '

 print,'min/mean/max/sample pixel 512,512 of:'
 print,'L05: ',min(data_L05),mean(data_L05),max(data_L05),$
   data_L05[512,512]
 print,'rawDN: ',min(rawDN),mean(rawDN),max(rawDN),$
   rawDN[512,512]
 print,'L10: ',min(data_L10),mean(data_L10),max(data_L10),$
   data_L10[512,512]
 print,'frac error: ',$
   min(fractional_error),mean(fractional_error),max(fractional_error),$
   fractional_error[512,512]
 print,'noise1=L10 * frac_err: ',$
   min(noise_method1),mean(noise_method1),max(noise_method1),$
   noise_method1[512,512]
 print,'sigmaDN: ',min(sigmaDN),mean(sigmaDN),max(sigmaDN),$
   sigmaDN[512,512]
 print,'noisedata=sigmaDN+offsetbias: ',$
   min(noisedata),mean(noisedata),max(noisedata),$
   noisedata[512,512]
 print,'noise2=secchi_prep(noisedata): ',$
   min(noise_method2),mean(noise_method2),max(noise_method2),$
   noise_method2[512,512]
 print,'noise3=sim_secchi_prep(noisedata): ',$
   min(noise_method3),mean(noise_method3),max(noise_method3),$
   noise_method3[512,512]

 print,' '
 print,'if these three match, our methods and code work:'
 print,noise_method1[512,512],noise_method2[512,512],noise_method3[512,512]

  if (plot eq 0) then stop

; NOW PLOT IT

 ymin=min(noise_method1[1023,*])
 ymin=min([ymin,min(noise_method2[1023,*])])
 ymin=min([ymin,min(noise_method3[1023,*])])
 ymax=max(noise_method1[1023,*])
 ymax=max([ymax,max(noise_method2[1023,*])])
 ymax=max([ymax,max(noise_method3[1023,*])])

 noise_ratio23=noise_method2/noise_method3
 noise_ratio12=noise_method1/noise_method2

 rmin=min(noise_ratio23[1023,*])
 rmin=min([rmin,min(noise_ratio12[1023,*])])
 rmax=max(noise_ratio23[1023,*])
 rmax=max([rmax,max(noise_ratio12[1023,*])])

;,yrange=[rmin,rmax]
 !p.multi=[0,3,2]
 plot,noise_method1[1023,*],title='method 1',yrange=[ymin,ymax]
 plot,noise_method2[1023,*],title='method 2',yrange=[ymin,ymax]
 plot,noise_method3[1023,*],title='method 3',yrange=[ymin,ymax]
 plot,noise_ratio23[1023,*],title='ratio 2 over 3'
 plot,noise_ratio12[1023,*],title='ratio 1 over 2'
 plot,data_L10[1023,*],title='original data'
 !p.multi=0
 write_gif,gif1,tvrd()

; dset=[[[noise_method1]],[[noise_method2]],[[noise_method3]],$
;  [[noise_ratio23]],[[noise_ratio12]]]
; tv_multi,dset,res=0.10,/log
 dset_noise=[[[noise_method1]],[[noise_method2]],[[noise_method3]]]
 dset_ratio=[[[noise_ratio23]],[[noise_ratio12]]]
 tv_multi,dset_noise,res=0.10,/log,title='noise via 3 methods'
 write_gif,gif2,tvrd()
 tv_multi,dset_ratio,res=0.10,/log,title='ratio of noises'
 write_gif,gif3,tvrd()
