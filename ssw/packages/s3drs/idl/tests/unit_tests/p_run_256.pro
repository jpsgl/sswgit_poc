;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; very stripped down minimal test of N=256, requires the 32x2.dat file

  ; early easy way to do restarts here
  savefile='256prep.sav'
  if (file_exist(savefile)) then oPixon->Full,/restart
  if (file_exist(savefile)) then stop

  pregenfile='32x2.dat'
  if (file_exist(pregenfile) eq 0 ) then stop

  restore,/verb,pregenfile
  datum=float(rebin(datum,256,256,2))

  prep_3drv,pxwrapper,views=2,N=256

  pxwrapper.sr.interp=0; 0 = force native IDL for big runs
  pxwrapper.sp.noshm=-1; -1 = sometimes use shm
  noct=2; half as many, check memory usage for large N
  npo=1; half as many, check memory usage for large N
  pxwrapper.sp=p_tweak_kernel(pxwrapper.sp,noct,npo,/debug)

  p_generate_pixon,'classic',pxwrapper,oPixon

  p_insert_data,pxwrapper,datum,oPixon

  save,file=savefile,datum,oPixon; for future runs

;  restore,savefile
;  pixon_env,'c'
;  p_pixonloop,pxwrapper,oPixon,1,10,/full,/init,/endsave
