;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
PRO p_simprofiler

  nele=[32,64,128]
  fele=[1.0,0.001,0.00001]

  for Nrun=0,n_elements(nele)-1 do $
    for frun=0,n_elements(fele)-1 do $
     originaltest,nele[Nrun],fele[frun]

;  for Nrun=0,n_elements(nele)-1 do $
;    for frun=0,n_elements(fele)-1 do $
;     p_runsimdata,nele[Nrun],fele[frun]

END

PRO originaltest,N,ftol

  nviews=2 

  if (n_elements(N) eq 0) then N=128
  if (n_elements(ftol) eq 0) then ftol=0.000625

  modelname='fluxrope'
  timage=pixonmodels(N,modelname,pxwrapper,stuff,stuff1)
  prep_3drv,pxwrapper,views=nviews,N=N
  p_generate_pixon,'classic',pxwrapper,oPixon,image=timage
  datum=stereo_project(pxwrapper,oPixon)
  p_insert_data,pxwrapper,datum,oPixon

  pxwrapper.sn.ftol=ftol

  print,N,' ',ftol,', Loop 1: 2x20: ',systime()
  p_pixonloop,pxwrapper,oPixon,2,20,/init,/full
  p_report,pxwrapper,oPixon,/save

  print,N,' ',ftol,', Loop 2: 1x40: ',systime()
  p_pixonloop,pxwrapper,oPixon,1,40,/init,/full
  p_report,pxwrapper,oPixon,/save

  print,N,' ',ftol,', Long run: 400: ',systime()
  p_pixonloop,pxwrapper,oPixon,1,400,/init,/full
  p_report,pxwrapper,oPixon,/save

END



PRO p_runsimdata,N,ftol

  if (n_elements(N) eq 0) then N=128
  if (n_elements(ftol) eq 0) then ftol=0.000625

  msb2dn=2.7E-12; A Cor2

  f1='/home/antunes/dev/128x128sim_0.fts'
  f2='/home/antunes/dev/128x128sim_1.fts'

  datum=ptrarr(2)
  datum[0]=ptr_new(readfits(f1,hdr1))
  datum[1]=ptr_new(readfits(f2,hdr2))

  Norig=128
  nviews=2
  modelname='fluxrope'
  gamma=[0,45]

  if (N ne Norig) then for i=0,nviews-1 do $
    *datum[i]=rebin(*datum[i],N,N)*Norig/N

  prep_3drv,pxwrapper,views=nviews,N=N,gamma=gamma
  p_generate_pixon,'classic',pxwrapper,oPixon

  p_insert_data,pxwrapper,datum,oPixon

  pxwrapper.sn.ftol=ftol


  print,N,' ',ftol,', Loop 1: 2x20: ',systime()
  p_pixonloop,pxwrapper,oPixon,2,20,/init,/full
  p_report,pxwrapper,oPixon,/save

  print,N,' ',ftol,', Loop 2: 1x40: ',systime()
  p_pixonloop,pxwrapper,oPixon,1,40,/init,/full
  p_report,pxwrapper,oPixon,/save

END


; makes a 128x128 Cor2 fluxrope from 90 degree viewing
; in a SECCHI STEREO_A Level 0.5 format.
; It assumes the 'clean' image is a Level 1.0 calibrated
; final image, and backtraces it to Level 0.5.
; Then, we can ingest it as Level 0.5 so the proper noise
; etc is created.
;

PRO p_simdatagen

  meters2au = 1.5E11
  msb2dn=2.7E-12; A Cor2

  N=128
  nviews=2
  modelname='fluxrope'
  gamma=[0,45]

  fsave='128x128fluxrope.sav'
  if (file_exist(fsave)) then begin
      restore,fsave,/verbose
  end else begin
  ; create it
      prep_3drv,pxwrapper,views=nviews,N=N,gamma=gamma
      timage=pixonmodels(N,modelname,pxwrapper,stuff,stuff1)
      p_generate_pixon,'classic',pxwrapper,oPixon,image=timage
      datapair=stereo_project(pxwrapper,oPixon)
;      datapair=datapair * msb2dn; sets new max
      save,file=fsave,datapair
  end

  fstem='128x128sim_'
  for i=0,n_elements(gamma)-1 do begin

      simhdr={SECCHI_HDR_STRUCTSIM,$
              OBSRVTRY:'STEREO_A',$
              INSTRUME:'SIM',$
              DETECTOR:'COR2',$
              EXPTIME:1,$
              BIASMEAN:0,$
              IPSUM:0,$
              BUNIT: 'MSB',$
              RSUN: 0,$
              CRLN_OBS: gamma[i],$
              CRLT_OBS: 0.0,$
              DSUN_OBS: 1.0 / meters2AU,$
              POLAR: 0,$
              CRPIX1: N/2,$
              CRPIX2: N/2,$
              CRVAL1: N/2,$
              CRVAL2: N/2,$
              MSB2DN: msb2dn,$
              DATE: systime(/utc)$
             }

      data05=floor(secchi_unprep(datapair[*,*,i],simhdr))
      fname=fstem + trim(string(i),2) + '.fts'

      writefits,fname,data05,struct2fitshead(simhdr)

  end

END
