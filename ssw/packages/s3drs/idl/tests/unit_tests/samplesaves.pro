;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
;               
; $Log: samplesaves.pro,v $
; Revision 1.1  2009/04/22 18:33:55  antunes
; first half of relocation.
;
; Revision 1.1  2008/03/03 19:21:54  antunes
; Reorganized test directory for clarity and archiving.
;
; Revision 1.1  2006/04/11 15:55:00  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.2  2006/01/09 17:14:42  antunes
; More commented added.
; New 'super3drv' driver routine for getting user specs for reconstruction
; process now checked in.  Just type 'super3drv' to test.
;
;-            
PRO samplesaves,iP,iR,flag

  if (n_elements(flag) eq 0) then flag=3

  if (flag eq 1 or flag eq 3) then begin
;  restore,'image.sav0'
      print,'Loading raytrace data from file'
      restore,'rtsave.sav'
      iR=image.sbtot.im
  end

  if (flag eq 2 or flag eq 3) then begin
      print,'Loading pixon data from file'
      restore,'pixonsave.sav'
      iP=data[*,*,1]
  end

END
