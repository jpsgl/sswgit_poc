;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; Given an incoming data item of size Nold, e.g. [A,B]
; and an expanded destination expecting size Nnew,
; e.g. 6 expecting [A,A,A,B,B,B]
; interweaves the old into the new as:
;  A A A B B B
;
; The alternative transfer, shown here but not provided in this
; routine, is easier but results in [A,B,A,B,A,B]
; z0=stereo_in.z0
; if (trans ne 0) then for i=1,trans-1 do z0=[z0,stereo_in.z0]
; pxwrapper.sr.z0=z0
;
; typically 'old'=stereo_in.TAG and 'new'=pxwrapper.sr.TAG

function interweave,old,new

  Nd=(size(new))[3]
  Nele=n_elements(old)
  if (Nele ne Nd) then trans=Nd/Nele else trans=0

  if (trans ne 0) then for i=0,Nd-1 do new[i]=old[i/trans] else new=old

  return,new

end
