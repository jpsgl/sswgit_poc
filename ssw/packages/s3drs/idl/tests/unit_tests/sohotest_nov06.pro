;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; using p_ingest_fits to read in original 0.5 SOHO data

pro minitest

  ;###############
  ;# setup

  on_error,2

  fname1='/home/antunes/data_old/22003802.fts'
  fname2='/home/antunes/data_old/22003872.fts'


  ;###############
  ;# get data, mask, and data info

  p_ingest_fits,fname1,data1,mask1,srjr1
  p_ingest_fits,fname2,data2,mask2,srjr2

  ;# instantiate wrapper
  prep_3drv,pxwrapper,views=1

  ;###############
  ;# put data info into our 'sr' structure

  sr=pxwrapper.sr

  help,sr
  help,srjr1
  help,srjr2
  update_struct_arrays,sr,srjr1,0
  update_struct_arrays,sr,srjr2,1

  ; now do global updates needed specifically for sr-- 3 items only
  sr.Nd=2
  trRRs=dblarr(4,4,sr.Nd)
  sr=addrep_struct(sr,'trRRs',trRRs)
  trVs=dblarr(3,3,sr.Nd)
  sr=addrep_struct(sr,'trVs',trVs)

  pxwrapper=addrep_struct(pxwrapper,'sr',sr)

  ;###############
  ;# now, in theory, we can generate our pixon item using our new sr

  p_generate_pixon,'classic',pxwrapper,oPixon
  p_insert_data,pxwrapper,datum,oPixon
  oPixon->Set,wt=masks
  stereo_noise,datum,pxwrapper.sn.sigma ; CHECK THIS!
  oPixon->Replace,sigma=sigma,mxit=10

  ;###############
  ;# and run it
  p_pixonloop,pxwrapper,oPixon,2,5


end
