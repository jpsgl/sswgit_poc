;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; This file also contains test routines for making objects.


; Does a timing test to make just the 'vrtx' object in tPixon
; for a given 'dim'
; You can run this script to see where tPixon object creation slows down.
PRO tpixon_timetest,N=N

  on_error,2

  profiler, /system & profiler

  pixon_env,'t'
  if (n_elements(N) eq 0) then N=16
  dim=long(N); syntaxical sugar to make this match the original make?_tpxn.pro
; fast
;data=make_array(dim,dim,/float,/index)
;sigma=make_array(dim,dim,/float,value=1)
;oTPixon = obj_new('TPixon',data=data,sigma=sigma)

  idim = REPLICATE(dim, 3); eg long(3) with each element value 'dim'

; note bug in 'vrtx', requires 'idim' be long if n>trivial!!!!
  vrtx = pxn_coord(long(idim));
  vrtx = vrtx - dim/2                       ; vertex at all N^3 points

; slow
  ddim = REPLICATE(dim, 2)
  ndata=1 ; make of 1 data set per satellite
  nview=2 ; number of satellites
  data  = PTRARR(ndata,nview)
  data[0,0]= PTR_NEW(RANDOMN(seed, ddim), /NO_COPY)      ; noise
  data[0,1]= PTR_NEW(RANDOMN(seed, ddim), /NO_COPY)      ; noise

  theta=[0,90]
  dtor = !DPI/180

  ihat = PTRARR(nview)
  ihat[0] = PTR_NEW([- SIN(dtor*theta[0]), COS(dtor*theta[0]), 0])
  ihat[1] = PTR_NEW([- SIN(dtor*theta[1]), COS(dtor*theta[1]), 0])

  khat = PTRARR(nview)
  khat[0] = PTR_NEW([COS(dtor*theta[0]), SIN(dtor*theta[0]), 0])
  khat[1] = PTR_NEW([COS(dtor*theta[1]), SIN(dtor*theta[1]), 0])

  pos = PTRARR(nview)

  IF N_ELEMENTS(npersp) EQ 0 THEN		npersp = 5
  persp = npersp*dim
  pos[0] = PTR_NEW(-persp*(*khat[0]))
  pos[1] = PTR_NEW(-persp*(*khat[1]))
 

  oTPixon = obj_new('TPixon',data=data,sigma=1.0,vrtx=vrtx,$
                   ihat=ihat, khat=khat, pos=pos)

  profiler,/report

  profiler,/clear,/system
  profiler,/clear,/reset

END

;; here is a very quick test to run a tiny tpixon to verify paths, etc
PRO tpixon_tinytest
  pixon_env,'t',/force
  prep_3drv,pxwrapper,views=2
  timage=fake_ne_cube(16)
  p_generate_pixon,'tetra',pxwrapper,oPixon,image=timage
  datum=stereo_project(pxwrapper,oPixon)
  tv_multi,datum,rescale=4,title='tiny tpixon test'
END

;; here is a very quick test to run a tiny cpixon to verify paths, etc
PRO cpixon_tinytest
  pixon_env,'c',/force
  prep_3drv,pxwrapper,views=2
  timage=fake_ne_cube(16)
  p_generate_pixon,'classic',pxwrapper,oPixon,image=timage
  datum=stereo_project(pxwrapper,oPixon)
  tv_multi,datum,rescale=4,title='tiny cpixon test'
END

;;; here is some test code to make objects of varying sizes, and save them

PRO tpixon_reinsert_test,N=N
;; TEST: Insert a different 'timage' into a tpixon object
;;;Test:
;;
;;;'Make otPixon object, an arrow
  if (n_elements(N) eq 0) then N=32
  rescale=long(96/N)
  prep_3drv,pxwrapper,views=2
  timage=arrowcube(N)
  threeview,timage,rescale=rescale,title='1. arrow'
;;
;;; save tObject
  p_generate_pixon,'tetra',pxwrapper,oPixon,image=timage
  datum=stereo_project(pxwrapper,oPixon)
  tv_multi,datum,rescale=rescale,title='2. orig arrow'
  save,oPixon,filename='tpixon.sav'
;;
;;; now exit, then start a new IDL session with a different 'cube' timage,
;;; same N
  prep_3drv,pxwrapper,views=2,N=N
  newimage=fake_ne_cube(N,generate='cube',lit=5)
  restore,'tpixon.sav',/verbose
  oPixon->Replace,image=ptr_new(newimage),/NO_COPY
  pixontype='t'
  pixon_env,pixontype
  pxwrapper.object='t'
  datum=stereo_project(pxwrapper,oPixon)
  tv_multi,datum,rescale=rescale,title='3. inserted cube'
;;
;;; compare with a new IDL session doing the 'cube' from scratch
  prep_3drv,pxwrapper,views=2
  timage=fake_ne_cube(N,generate='cube',lit=5)
  p_generate_pixon,'tetra',pxwrapper,oPixon,image=timage
  datum=stereo_project(pxwrapper,oPixon)
  tv_multi,datum,rescale=rescale,title='4. new cube'
;;
;;
END

PRO tpixon_generic_object,N=N

  if (n_elements(N) eq 0) then N=16
  print,'making tPixon saved object of size ',N
  savefile=strtrim(string(N),2)+'tpixon.sav'

;; Make sample objects and save:
  prep_3drv,pxwrapper,views=2
  timage=fake_ne_cube(N,generate='cube',lit=1)
  p_generate_pixon,'tetra',pxwrapper,oPixon,image=timage
  save,oPixon,filename=savefile

  print,'saved as ',savefile

;N=16
;timage=fake_ne_cube(N,generate='cube',lit=1)
;p_generate_pixon,'tetra',pxwrapper,oPixon,image=timage
;save,oPixon,filename='16tpixon.sav'
;
;N=32
;timage=fake_ne_cube(N,generate='cube',lit=1)
;p_generate_pixon,'tetra',pxwrapper,oPixon,image=timage
;save,oPixon,filename='32tpixon.sav'
;
;N=64
;timage=fake_ne_cube(N,generate='cube',lit=1)
;p_generate_pixon,'tetra',pxwrapper,oPixon,image=timage
;save,oPixon,filename='64tpixon.sav'
;
;N=96
;timage=fake_ne_cube(N,generate='cube',lit=1)
;p_generate_pixon,'tetra',pxwrapper,oPixon,image=timage
;save,oPixon,filename='96tpixon.sav'
;
;N=128
;timage=fake_ne_cube(N,generate='cube',lit=1)
;p_generate_pixon,'tetra',pxwrapper,oPixon,image=timage
;save,oPixon,filename='128tpixon.sav'
;
;N=192
;timage=fake_ne_cube(N,generate='cube',lit=1)
;p_generate_pixon,'tetra',pxwrapper,oPixon,image=timage
;save,oPixon,filename='192tpixon.sav'
;
;N=256
;timage=fake_ne_cube(N,generate='cube',lit=1)
;p_generate_pixon,'tetra',pxwrapper,oPixon,image=timage
;save,oPixon,filename='256tpixon.sav'
;
END


; TEST CASE showing switching back and forth between 'c','t','rtwl'
PRO pixon_coexist_test,N=N

  if (n_elements(N) eq 0) then N=16
  on_error,2
  n_views=2; e.g.
  prep_3drv,pxwrapper,views=n_views,N=N
  rescale=long(96/N)

  pixon_env,'c',/both

  timage=arrowcube(N)
  ;threeview,timage,rescal=4

  ;modparam=timage2modparam(timage) 
  ;rdatum=rtwl_wrapper(pxwrapper,modparam)
  ;tv_multi,rdatum,rescal=8

  ;pimage=rearrange(timage,[2,3,-1])
  print,'running cpixon'
  p_generate_pixon,'classic',pxwrapper,ocPixon,image=timage
  cdatum=stereo_project(pxwrapper,ocPixon)
  tv_multi,cdatum,rescale=rescale,title='1. classic'

  pxwrapper.object='t'
  print,'running tpixon'
  p_generate_pixon,'tetra',pxwrapper,otPixon,image=timage
  tdatum=stereo_project(pxwrapper,otPixon)
  tv_multi,tdatum,rescale=rescale,title='2. tetra'

  print,'running cpixon'
  pxwrapper.object='c'
  cdatum2=stereo_project(pxwrapper,ocPixon)
  tv_multi,cdatum2,rescale=rescale,title='3. classic'

  print,'running tpixon'
  pxwrapper.object='t'
  tdatum2=stereo_project(pxwrapper,otPixon)
  tv_multi,tdatum2,rescale=rescale,title='4. tetra'

  print,'done alternating runs'

END

PRO pixon_tests
  print,'tests available are:'
  print,'  tpixon_generic_object,N=N'
  print,'    Creates and saves a very simple tPixon object with VRTX'
  print,'    but no geometry, of size N, then saves it.  Such a save'
  print,'    file should be able to be loaded/restored and then geometry'
  print,'    added for further tPixon runs.'
  print,'  pixon_coexist_test'
  print,'    Runs a simple test creating both cPixon and tPixon objects,'
  print,'    then rendering each twice with their respective renderers,'
  print,'    showing they can co-exist in the same path/environment.'
  print,'  tpixon_timetest,N=N'
  print,'    For a given N, creates the VRTX tPixon object and prints'
  print,'    time/profiling results showing how long it took.'
  print,'  tpixon_reinsert_test,N=N'
  print,'    Runs a simple test creating a tPixon object, saving it,'
  print,'    loading the save file, inserting a new timage, then rendering,'
  print,'    showing that the timage of a saved tPixon object can be altered.'
  print,'  tpixon_tinytest OR cpixon_tinytest'
  print,'    simple N=16 tests just to check paths, runtime, etc.'
END
