;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
PRO ignore

render=stereo_project(pxwrapper,oPixon)
;plot,render[*,64,0]*pxwrapper.sr.istd[0],yrange=[0,1e8]
plot,render[*,64,0],yrange=[0,5]
; is 0.234 rsun per pixel


N=4

on_error,2
timage=fltarr(N,N)
prep_3drv,views=2,N=N,pxwrapper
pxwrapper.sr.polar='b'
timage[0,0,0]=1.0
rend=pr_render(pxwrapper,timage)


end

; try this.. restore a data file to get a valid timage
pro resolution_dependence,N
  pixon_env,'c'
  restore,'fluxy_solved.sav'
  timage=opixon->get(/im,/nop)
;N=32
  timage2=congrid(timage,N,N,N)*0.0+1.0
  prep_3drv,views=2,N=N,pxwrapper2
  pxwrapper2.sr.polar='b'
  rend=pr_render(pxwrapper2,timage2,istd=istd)
;  print,pxwrapper2.sr.istd
;  print,N,' tB ',minmax(rend),minmax(rend)/float(N)
  print,N,' tB ',minmax(rend),mean(rend,/nan)
  pxwrapper2.sr.polar='n'
  rend=pr_render(pxwrapper2,timage2,istd=istd)
  print,N,' no ',minmax(rend),mean(rend,/nan)
;  print,N,' no ',minmax(rend),minmax(rend)/float(N)
;  print,pxwrapper2.sr.istd
end

