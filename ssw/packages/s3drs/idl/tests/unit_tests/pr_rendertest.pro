;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; simple routine to test our stand-alone front and back
; projections, pr_render and pr_backproject.

PRO pr_rendertest

   on_error,2
   N=32
   prep_3drv,pxwrapper,N=N,gamma=[0,90]
   nix=fltarr(32,32,32)+0.9
   render=pr_render(pxwrapper,nix,0)

   flat=fltarr(32,32)+1.0
   image=pr_backproject(pxwrapper,flat,0)

   tv_multi,fixed=256,render,/log
   threeview,image,fixed=256,/log

END
