;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; header test
fsecchi = 'sandyutil/data_old/20060604_165104_14c2A.fts'
fsoho = 'sandyutil/data_old/22003802.fts'
hsecchi = headfits(fsecchi)
hsoho = headfits(fsoho)

h1a = fitshdr2struct(hsecchi)
h1b = fitshdr2struct(hsoho)
h2a = fitshead2struct(hsecchi)
h2b = fitshead2struct(hsoho)
h3a = lasco_fitshdr2struct(hsecchi)
h3b = lasco_fitshdr2struct(hsoho)
