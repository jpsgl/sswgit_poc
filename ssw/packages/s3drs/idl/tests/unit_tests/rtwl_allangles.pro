;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; simple little routine to render using RTWL and return a set
; of 'datum' images

; for now, simple, later I will add other parameters

FUNCTION rtwl_allangles,N,modparam

  datum=fltarr(N,N,64)

  iframe=0
  for i=0,3 do begin
      xang=90*i

      for j=0,3 do begin
          yang=90*j

          for k=0,3 do begin
              zang=90*j

;              print,iframe,i,j,k

              raytracewl,sbt,imsize=[N,N],losnbp=long(N),losrange=[-20,20], $
                modelid=25,modparam=modparam,$
                neang=[xang,yang,zang]*!dtor,/cor2
      
              datum(*,*,iframe)=sbt.im
              iframe=iframe+1

          end

      end

  end

  return,datum

END
