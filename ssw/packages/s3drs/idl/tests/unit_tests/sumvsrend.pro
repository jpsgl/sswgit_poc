;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; in xyz64
on_error,2
restore,/v,'fluxy_datum.sav'; gets timage

prep_3drv,N=64,pxwrapper,gamma=[0.0,90.0,0.0],theta=[0.0,0.0,90.0]
pxwrapper.sr.interp=0
pxwrapper.sr.roco=-1
pxwrapper.sr.gamma=pxwrapper.sr.gamma*0.0

.com pr_sum

prsum=pr_sum(timage,pxwrapper=pxwrapper,/pixon)
prren=pr_render(pxwrapper,timage)

;tv_multi,fixed=256,/log,title='Sum',prsum
;tv_multi,fixed=256,/log,title='Pixon',prren
labels=['Sum 0,0','Sum 90,0','Sum 0,90','Pixon','Pixon','Pixon']
tv_multi,labels=labels,line=3,/log,fixed=128,[[[prsum]],[[prren]]],/local,/inv



;rotation checks:

xang=0.0; always 0
yang=0.0; height angle above plane
zang=0.0-rho; CCW around z

image1=timage
cuberot3d,image1,[xang,yang,zang]

image2=turn_3d(timage,zang,yang,zang)

image3=transform_volume(image,rotation=[xang,yang,zang])

image4=timage
interpolate3d,image,[xang,yang,zang]

image5=rot3dmatrix([xang,yang,zang]*!dtor) ## image
