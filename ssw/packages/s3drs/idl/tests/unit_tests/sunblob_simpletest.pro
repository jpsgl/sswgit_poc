;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; TEST CASE FOR A SIMPLE SUN-LIKE BLOB

on_error,2
N=32
nviews=2
prep_3drv,pxwrapper,views=nviews,N=N,gamma=[0,45]
pxwrapper.sr.n0=1e9
timage=make_array(N,N,N,/float,value=0.0)
rsun=N/6
for z= (N/2)-rsun,(N/2)+rsun do $
  for x= (N/2)-rsun,(N/2)+rsun do $
  for y= (N/2)-rsun,(N/2)+rsun do $
  if ( (z-N/2)^2 + (y-N/2)^2 + (x-N/2)^2 le rsun) then timage[x,y,z]=1.0
threeview,timage,res=2

p_generate_pixon,'classic',pxwrapper,oPixon,image=timage
cleandatum=stereo_project(pxwrapper,oPixon)
Nd=n_elements(pxwrapper.sr.gamma)
noise = max(cleandatum)*0.1 * (randomu(seed,N,N,Nd)-.5)/2
noisydatum=sqrt(cleandatum^2 + noise^2)

; OPTIONAL NOISE, CHOOSE WHICH LINE TO KEEP
p_insert_data,pxwrapper,noisydatum,oPixon,sigma=noise
;  p_insert_data,pxwrapper,cleandatum,oPixon


p_pixonloop,pxwrapper,oPixon,10,60,hours=0.1,/init,/flush
p_report,pxwrapper,oPixon,/echo,/save,runname='idiocypure'

d_new=stereo_project(pxwrapper,oPixon)
tv_multi,d_new,res=4
img=oPixon->Get(/im,/nop)
threeview,img,res=4


tv_multi,cleandatum,res=4,oom=2,/log,title='Log plot, original Sun'
write_jpeg,'suntest.jpg',tvrd(/true),true=1,quality=100
tv_multi,d_new,res=4,oom=2,/log,title='Log plot, solution'
write_jpeg,'sunsoltest.jpg',tvrd(/true),true=1,quality=100
threeview,img,res=4,/log,oom=2,title='Cube projections'
write_jpeg,'sunsolcube.jpg',tvrd(/true),true=1,quality=100
