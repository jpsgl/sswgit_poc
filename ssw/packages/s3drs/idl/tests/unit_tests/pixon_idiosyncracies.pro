;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; tests Pixon idiocyncracies, on different results
; depending on the mix of pointers and data, and the
; order items are placed into the Pixon object
;
; Pixon data must be pointers.  What happens if we mix it
; with sigma as data vs pointers, if we swap the order we
; load sigma and data, and if we mix up sigma-data with
; images that are either data or pointers?
;

PRO idiosyncracies,inherit=inherit

  dfile='32x2.dat'
  if (file_exist(dfile) eq 0) then return

  inherit=setyesno(inherit)

  N=16
  L1=[2,2,4,1,4,2,4,1,4]
  L2=[4,4,2,4,1,4,2,4,1]
  IL=[1,1,1,1,1,0,0,0,0]

  names=['sp d id','sp d id','d sp id','sd d id','d sd id',$
         'sp d ip','d sp ip','sd d ip','d sd ip']

  restore,/v,dfile
  datum=rebin(datum[*,*,0:1],N,N,2)
  prep_3drv,pxwrapper,N=N,views=2,gamma=[0.0,90.0]

  sigma=sqrt(datum)

  image=make_array(/float,N,N,N,value=1.0/float(N))

  if (inherit) then ins=' (inherits previous oPixon)' else $
    ins=' (reset each time)'
  report=['run name, Chi2, Pixon count'+ins]

  if (inherit) then p_generate_pixon,'classic',pxwrapper,oPixon


  for i=0,n_elements(L1)-1 do begin

      if (inherit eq 0) then p_generate_pixon,'classic',pxwrapper,oPixon
      oPixon->set,mxit=20

      oData=oPixon->get(/oData)

      if (L1[i] eq 1) then oPixon->set,sigma=sigma
      if (L1[i] eq 2) then oPixon->set,sigma=ptr_new(sigma)
      if (L1[i] eq 3) then for m=0,1 do oData[m]->Set,data=datum[*,*,m]
      if (L1[i] eq 4) then for m=0,1 do $
        oData[m]->Set,data=ptr_new(datum[*,*,m])

      if (L2[i] eq 1) then oPixon->set,sigma=sigma
      if (L2[i] eq 2) then oPixon->set,sigma=ptr_new(sigma)
      if (L2[i] eq 4) then for m=0,1 do $
        oData[m]->Set,data=ptr_new(datum[*,*,m])

      if (IL[i] eq 0) then oPixon->set,image=image
      if (IL[i] eq 1) then oPixon->set,image=ptr_new(image)

      oPixon->set,sigma=ptr_new(sigma)

      oPixon->full,/restart
      chi2 = trim(string(oPixon->get(/chi2,/total)),2)
      pc = trim(string(oPixon->get(/pxncnt)),2)
      help,oPixon->get(/sigma,/nop),output=shelp
      str=names[i] + '  Chi2: ' + chi2 + ' Pcount: ' + pc + ' sigma: '+shelp
;      print,str
      report=[report,str]
  end

  for i=0,n_elements(report)-1 do print,report[i]

END
