;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
PRO fovtest,N=N,munge=munge,plots=plots

  if (n_elements(N) eq 0) then N=64
  munge=setyesno(munge); yes=use differing FOVs
  plots=setyesno(plots)

  if (munge) then title='xz=Cor2,y=C3' else $
    title='xyz=Cor2'
  title=int2str(N)+'_'+title

  pixon_env,'c',/local
; bring in datum, noise, pxwrapper, timage

  restore,/v,'shell.img'
  synthetic=rebin(shell,N,N,N)

  prep_3drv,pxwrapper,gamma=[0,90,0],theta=[0,0,90],/cor2,N=N

  pxwrapper.sc.gname=title

  if (munge) then pxwrapper.sr.l0tau[1]=pxwrapper.sr.l0tau[1]*2.0
  if (munge) then pxwrapper.sr.tau[1]=pxwrapper.sr.tau[1]*2.0
  if (munge) then pxwrapper.sr.detector[1]='C3'

  datum=pr_render(pxwrapper,synthetic)

  mask=p_getmask(sr=pxwrapper.sr)
  masks=[[[mask]],[[mask]],[[mask]]]
;  tv_multi,fixed=196,/log,datum*masks

  conv=2.7e-18*15.0/6.0
  noise=conv*sqrt(datum/conv)
  print,max(noise),max(datum)

  p_generate_pixon,'classic',pxwrapper,oPixon
  p_insert_data,pxwrapper,datum,oPixon,sigma=noise,masks=masks

  oPixon->full,/restart

  render=stereo_fetch(pxwrapper,oPixon,/render)
  labels=['r1','r2','r3','d1','d2','d3']
  tv_multi,line=3,fixed=196,/raw,/log,/cap,oom=2,lab=labels,title=title,$
    [[[render]],[[datum]]]
  image=stereo_fetch(pxwrapper,oPixon,/im)
  threeview,fixed=196,/raw,/log,oom=2,/pixon,image

  save,file=title+'.sav',title,pxwrapper,oPixon,labels,$
    synthetic,image,render,datum,noise,masks

  if (plots) then p_standardplots,pxwrapper,oPixon,synthetic=synthetic,$
    oom=2,/cap,/noscreen,/noslice

END
