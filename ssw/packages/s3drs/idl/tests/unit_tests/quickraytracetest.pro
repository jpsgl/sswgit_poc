;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
;               
; $Log: quickraytracetest.pro,v $
; Revision 1.1  2009/04/22 18:33:51  antunes
; first half of relocation.
;
; Revision 1.1  2008/03/03 19:21:54  antunes
; Reorganized test directory for clarity and archiving.
;
; Revision 1.1  2006/04/11 15:55:00  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.4  2006/02/28 19:48:16  antunes
; Lots of code, some buggy-- interim checkin.
;
; Revision 1.3  2006/01/09 17:14:42  antunes
; More commented added.
; New 'super3drv' driver routine for getting user specs for reconstruction
; process now checked in.  Just type 'super3drv' to test.
;
;-            
PRO quickraytracetest,N,iP,iR
; generate a chen fluxrope in pixon and wlraytrace

prep_cfluxrope,N,sr,sc,sp,sn,stuff,pixonrun

p_generate_pixon,sr,sc,sp,userstruct,timage,oPixon
iP=data[*,*,1]
save,data,filename='pixonsave.sav'

raytracewl, sbt1,sbp1,sne1,imsize=[N,N],losrange=[-30,30],$
 losnbp=128L,modelid=33,neang=[0.,-90.0,90]*!dtor,/c3,/fake,$
 modparam=[0.7,2.55,30.,10,.2]

;wnd,0,alog10(sbt1.im > 1e-14)
iR=sbt1.im

; save the raytrace one here just for later use
image={sbtot:sbt1}
save,image,filename='rtsave.sav'

print,"datum saved in cfluxrope.sav and rtsave.sav"
print,"datum can be reloaded later using 'samplesaves,iP,iR,3'"
print,"now type 'pixon_vs_raytrace,iP,iR,dPR,[scaling]'"

END
