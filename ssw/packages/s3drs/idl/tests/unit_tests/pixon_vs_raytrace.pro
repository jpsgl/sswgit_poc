;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
;               
; $Log: pixon_vs_raytrace.pro,v $
; Revision 1.1  2009/04/22 18:33:49  antunes
; first half of relocation.
;
; Revision 1.1  2008/03/03 19:21:54  antunes
; Reorganized test directory for clarity and archiving.
;
; Revision 1.1  2006/04/11 15:55:00  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.5  2006/01/09 17:14:42  antunes
; More commented added.
; New 'super3drv' driver routine for getting user specs for reconstruction
; process now checked in.  Just type 'super3drv' to test.
;
; Revision 1.4  2005/12/27 17:48:24  antunes
; Commented half of the routines using SSW style.  Also added a new
; axes test case 'arrowcube'.
; Checked in the Tomography/old/mikic_img.pro routine here because it
; is a dependency for the general image-prep routine 'get_timage.pro'
;
;-            
pro pixon_vs_raytrace,iP,iR,dPR,scaling,imP,imR

  if (n_elements(scaling) eq 0) then scaling=1
  
; load up 'iP' and 'iR' as original data with intrinsic resolutions
  if (n_elements(iR) eq 0) then samplesaves,iP,iR,1
  if (n_elements(iP) eq 0) then samplesaves,iP,iR,2
  
;  restore,'image.sav0'
;  raytrace_image=image.sbtot.im

; restore,'128cfluxrope.sav'
;  pixon_image=data[*,*,1]

; quick copy of images to manipulate
  imP=iP
  imR=iR

; extract instrinsic resolutions
  basesP=size(imP)
  baseP=basesP[1]
  basesR=size(imR)
  baseR=basesR[1]

  base=max([baseP,baseR])

; rescale smallest to largest if necessary
  if (baseP ne baseR) then begin
      print,'Pixon and Raytrace sizes do not match:',baseP,baseR
      print,'rescaling to newsize'
      imP=congrid(imP,base,base)
      imR=congrid(imR,base,base)
  end

; now apply scaling factor (even if scaling=1, to create copy of images)
  newsize=scaling*base
  print,'resolution is ',base,' scaled to ',newsize
  imP=congrid(imP,newsize,newsize)
  imR=congrid(imR,newsize,newsize)
  
  ; normalize
  imP=imP/norm(imP)
  imR=imR/norm(imR)

  diffimg = imP - imR

  ; add an 0.0001% offset to divisor to avoid dividing by zero
  avoidzero=0.00001*min(imR[where(abs(imR) ne 0)])
  dPR = imP/(imR+avoidzero)

  cP=centroid(imP)
  cR=centroid(imR)
  offset=(cP-cR)
  print,'offset of centroids is: ',offset

;  mydevice=!D.NAME
;  SET_PLOT,'PS'
;  DEVICE,FILENAME='myfile.ps',/LANDSCAPE


  plot1=(imP>avoidzero)
  plot2=(imR>avoidzero)
  plot3=(dPR>avoidzero)

  ; log scale them
  plot1=alog(plot1)
  plot2=alog(plot2)
  plot3=alog(plot3)

  ; invert
  plot1=0.0-plot1
  plot2=0.0-plot2
  plot3=0.0-plot3

  spacing=max([10,round(newsize/20)])

  window,5,/free,xsize=4*spacing+3*newsize,ysize=spacing+newsize*2
  tvscl,0.0-imP,spacing,spacing+newsize
  tvscl,0.0-imR,2*spacing+newsize,spacing+newsize
  tvscl,0.0-dPR,3*spacing+2*newsize,spacing+newsize

  tvscl,plot1,spacing,0
  tvscl,plot2,2*spacing+newsize,0
  tvscl,plot3,3*spacing+2*newsize,0

  ; sets up geometry for labels so we know what is going up
  labels=['Pixon','Raytrace','Diff']
  xs=[0.5*spacing+newsize*0.5,spacing+newsize*1.5,1.5*spacing+newsize*2.5]
  ; we add the -2 bit just to better justify the text in smaller plots
  ys=[0.5*spacing+newsize-2,0.5*spacing+newsize-2,0.5*spacing+newsize-2]
  xyouts,xs,ys,labels,alignment=0.5,text_axes=0,/DEVICE

  labels=['linear','log']
  xs=[0.5*spacing,0.5*spacing]
  ys=[1.5*newsize,0.5*newsize]
  xyouts,xs,ys,labels,alignment=0.5,orientation=90.0,text_axes=0,/DEVICE

;  DEVICE,/CLOSE
;  SET_PLOT,mydevice
;  plot,imP,color=99999
;  oplot,imR

  print,'normalized inv data are in imP, imR, diff is in dPR'

end
