;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; make sure our rotation method and placement of centers for Pixon
; allows for proper symmetry rotation
;

PRO rotation_test

  for extra=0,1 do begin
      for Nexp=3,6 do begin

          N=2^Nexp

          prep_3drv,pxwrapper,N=N,gamma=[0.0,90.0,180.0],/cor2

          N2=N/2

          img=fltarr(N,N,N)
          img[N2-1-extra:N2+extra,N2-1-extra:N2+extra,N2-1-extra:N2+extra]=$
            10^6.5

          pr=pr_render(pxwrapper,img)
      
          print,minmax(pr)

          tv_multi,fixed=128,/raw,labels=lset,/log,pr,/show
      end

  end
END


; compare Pixon and RTWL's symmetry


PRO symmetry_test

  on_error,2

  N=8
  extra=1                       ; or 1, for larger N so it's visible

  for extra=0,1 do begin
      for Nexp=3,7 do begin

          N=2^Nexp


          prep_3drv,pxwrapper,N=N,gamma=[0.0],/cor2

          N2=N/2

          img=fltarr(N,N,N)
          img[N2-1-extra:N2+extra,N2-1-extra:N2+extra,N2-1-extra:N2+extra]=$
            10^6.5

          pr=pr_render(pxwrapper,img)
          rtwl=rtwl_wrapper(pxwrapper,img)

          print,minmax(pr),minmax(rtwl)

;threeview,fixed=256,img,/raw

          dset=[[[pr]],[[rtwl]]]
          lset=['Pixon','RTWL']
          tv_multi,fixed=128,/raw,labels=lset,/log,dset,line=2,/show

      end
  end

END
