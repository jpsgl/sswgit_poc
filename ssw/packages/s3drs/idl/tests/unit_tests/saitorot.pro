;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
on_error,2
N=128
saito=pixonmodels(N,'saito')

;cubeize it
; 2x2 would be on 63:64 aka size=1, (N/2)-size,(N/2)-1+size
; 3x3 is impossible, 4x4 is 62:65 aka size=2
cube=saito*0.0
size=16
n1=N/2-size
n2=N/2-1+size
cube[n1:n2,n1:n2,n1:n2]=10^6.5

timage=cube; or saito

prep_3drv,N=N,pxwrapper,gamma=[0,180,360],/cor2
pr=pr_render(pxwrapper,timage)

threeview,/raw/fixed=128,/log,timage
tv_multi,/raw,fixed=128,/log,pr,/axes
tv_multi,/raw,fixed=256,/log,pr,/axes
prdiff=pr[*,*,0]-pr[*,*,2]
tv_multi,/raw,fixed=256,/log,prdiff,/axes
