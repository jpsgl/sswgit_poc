;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; little routine to quickly make a pair of sample data for
; use in other pixon testing routines.

PRO makequick2data
      nviews=2 
      prep_3drv,pxwrapperA,views=nviews,N=32,gamma=[0,30]
; we skip timage to make a really big fluxrope instead
      restore,'/net/corona/cplex3/reiser/a/ftimage_256.dat',/verbose
      timage = timg[129-N/2:128+N/2,129-N/2:128+N/2,129-N/2:128+N/2]
      p_generate_pixon,'classic',pxwrapperA,oPixonA,image=timage
      d_orig=stereo_project(pxwrapperA,oPixonA)

  ; normalize it
      dmax=max(d_orig)

      d_orig = d_orig * (1.0/dmax) * 1e8

      save,d_orig,'2samplesgamma0,30.sav'
END
