;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
pro football

  saito=pixonmodels(64,'saito')
  threeview,fixed=196,/raw,/log,saito,saito3,/nop,/pixon

  prep_3drv,pxwrapper,N=64,views=3,/cor2
  masks=p_getmask(sr=pxwrapper.sr)
  masks=[[[masks]],[[masks]],[[masks]]]
  saito4=saito3*masks  
  tv_multi,fixed=196,/raw,/log,saito4,/cap,oom=2

end
