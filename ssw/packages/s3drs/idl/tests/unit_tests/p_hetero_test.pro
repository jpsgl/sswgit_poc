;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; test case loading in an N=16 and N=32 image over an N=32 cube.

; currently debugging p_osetup.pro and p_insert_data.pro, but
; the real bug seems to be in the actual object creation call,
; not in my prepatory handlers.

PRO p_hetero_test,ndata1=ndata1,ndata2=ndata2,ncube=ncube

;   .reset
   on_error,2 

  if (n_elements(ndata1) eq 0) then ndata1=32
  if (n_elements(ndata2) eq 0) then ndata2=32
  if (n_elements(ncube) eq 0) then ncube=32

  datafile='2samplesgamma0,30.sav'

  if (file_exist(datafile) eq 0) then makequick2data

  restore,datafile,/verbose

  ; make our two images
  d_one= congrid(d_orig[*,*,0],ndata1,ndata1)
  d_two = congrid(d_orig[*,*,1],ndata2,ndata2)

  help,d_one,d_two

  ; now make the pixon object we need, at size ncube
  prep_3drv,pxwrapper,views=nviews,N=ncube,gamma=[0,30]
  pxwrapper.sr.Ncube=ncube; force to be safe
  p_generate_pixon,'classic',pxwrapper,oPixon

  img=oPixon->Get(/im)
  help,*img[0]

  p_insert_data,pxwrapper,d_one,oPixon,iele=0
  p_insert_data,pxwrapper,d_two,oPixon,iele=1

  ; double check ratios were set properly
  ratid=oPixon->Get(/ratid)
  help,*ratid[0]
  print,*ratid[0]
  print,*ratid[1]
  print,pxwrapper.sr.ncube,pxwrapper.sr.ndata

  ; test it by simply refetching old data
  odata=oPixon->Get(/data)
  help,*odata[0]
  help,*odata[1]
  img=oPixon->Get(/im)
  help,*img[0]

  ; add noise
  sigma_one = fractional_noise(d_one,/sigma)
  sigma_two = fractional_noise(d_two,/sigma)
  sigma=ptrarr(2)
  sigma[0]=ptr_new(sigma_one)
  sigma[1]=ptr_new(sigma_two)

  ; this next line core dumps with ndata1=ndata2=32, ncube=16
  oPixon->Replace,sigma=sigma

  sigmatest = oPixon->Get(/sigma)
  help,*sigmatest[0]
  help,*sigmatest[1]


  ; try and fetch back the data
  d_test=stereo_project(pxwrapper,oPixon,/ptr)
  help,*d_test[0]
  help,*d_test[1]


  p_pixonloop,pxwrapper,oPixon,1,2,/init,/flush

  d_new=stereo_project(pxwrapper,oPixon,/ptr)
  help,*d_test[0]
  help,*d_test[1]

  p_report,pxwrapper,oPixon

   ; now test plotting
  tv_multi,d_new


END
