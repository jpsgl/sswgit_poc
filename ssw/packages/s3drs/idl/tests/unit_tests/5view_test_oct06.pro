;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; script, run with @5-view-render_sample.pro
; Does a quick sample run of 5 views.

;; INGEST
on_error,2
.com '$SSW/soho/mdi/idl/cal/circle_mask.pro'
.com fits2pixon
;;N=64
N=dialog_list(['32','64'],title='Resolution of reconstruction')
print,N
;N=128
test_real_3dr,pxwrapper,oPixon,datum,N=N,/ingest,ftol=0.0000006

sat_los_view,pxwrapper.sr.gamma,/label,wsize=400
;;tv_multi,(datum < 5000 > 3000),/log,/border,title='original datum',res=196/pxwrapper.sr.N

;; RECON
p_pseudoloop,pxwrapper,oPixon,20,2

; or
oPixon->Replace,mxit=0
oPixon->Full,/restart
p_pixonloop,pxwrapper,oPixon,200,50,/plot

;; VIEWING
;tv_multi,(datum < 5000 > 3000),/log,/border,title='original datum'
img=oPixon->Get(/image,/nop)
render_rot_gui,img,threshold=1e5
;Ns=strtrim(string(N),2)
;save,pxwrapper,oPixon,datum,file='5recon_'+Ns+'.sav'
p_report,pxwrapper,oPixon
