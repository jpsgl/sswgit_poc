;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
on_error,2

; first get a Cor2 header

fname='sandyutil/data_old/20060604_165104_14c2A.fts'
fsim = 'p_fluxrope0.fits'
fclean = 'simdata_clean1.fts'
fnoise = 'simdata_noise1.fts'
fsigma = 'simdata_sigmaDN1.sav'
plotout = 'sigmaDNtest1.gif'

sdata=sccreadfits(fname,shdr)
dn2msb = get_calfac(shdr)
N=shdr.naxis1

; and get our fake data
p_ingest_fits,fsim,dataset,mask,srjr
; data values are 0 to 0.15
fdata = dataset[*,*,0]/dn2msb
print,'scaling up to ',N
fdata = congrid(fdata,N,N)

save_secchi05,fdata,shdr,fclean

; now read it in, to be safe
cleandata=sccreadfits(fclean,chdr)

; add noise
sigmaDN = fractional_noise(cleandata,hdr=chdr,/sigma)
noisedata = cleandata + sigmaDN

; save noise just for later
save,sigmaDN,filename=fsigma

; save noisey file
save_secchi05,noisedata,chdr,fnoise

; read in noisey file
noisedata2=sccreadfits(fclean,nhdr)

; extract noise
sigmaDN2 = fractional_noise(noisedata2,hdr=nhdr,/sigma)

; compare noises
!p.multi=[0,2,2]
plot,sigmaDN,title='sigmaDN added'
plot,sigmaDN2,title='sigmaDN extracted'
plot,sigmaDN-sigmaDN2,yrange=[-0.01,0.01],$
  title='sigmaDN Noise added minus Cor2 calculated noise',$
  xtitle='index',ytitle='added - extracted'
plot,cleandata,title='(original noise-free data, for comparison)'
!p.multi=0

write_gif,plotout,tvrd()
