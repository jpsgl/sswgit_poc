;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
on_error,2
dirA='/home/antunes/project_management/cor2a/'
dirB='/home/antunes/project_management/cor2b/'
f1=dialog_pickfile(path=dirA)
f2=dialog_pickfile(path=dirB)

; choose a file from one, mirror it for the second.

data=ptrarr(2)
masks=ptrarr(2)
noise=ptrarr(2)

p_ingest_fits,f1,data,masks,srjr,/verbose,/pointer,index=0
p_ingest_fits,f2,data,masks,srjr,/verbose,/pointer,index=1

help,data[0],*data[0]
help,data[1],*data[1]

f=[$
'/home/antunes/project_management/cor2b/20061220/20061220_000230_s4c2B.fts',$
'/home/antunes/project_management/cor2b/20061220/20061220_000240_s7c2B.fts',$
'/home/antunes/project_management/cor2b/20061220/20061220_000250_s4c2B.fts']

secchi_prep,f,hdrs,imgs,/polariz_on,/debug

;tv_multi,(*(data[0]) * (*(masks[0]))),res=0.15,/log,title='ACor2'
tv_multi,(*(data[1]) * (*(masks[1]))),res=0.15,/log,title='BCor2'

