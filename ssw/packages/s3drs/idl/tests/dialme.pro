;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
pro makedials,offset=offset,justoffset=justoffset

  N=128

  justoffset=setyesno(justoffset)

  Nm=N/2
  stuff=[45.0,45.0]

  ; also do a shell closer in
  if (n_elements(offset) eq 0) then offset=N/8

  saito=pixonmodels(N,'saito')
;  threeview,/pixon,res=3,saito,/log,/surpressaxes
  save,file='saito.img',saito

  timage=pixonmodels(Nm,'hshell2',ignore,stuff)
  shell=saito*0.0
  shell[N/2:(N/2)+Nm-1,0:Nm-1,0:Nm-1]=timage
  threeview,/pixon,res=3,shell,/surpressaxes
  save,file='shell.img',shell

  shellB=saito*0.0
  shellB[N/2-offset:(N/2)+Nm-1-offset,$
         offset:offset+Nm-1,offset:offset+Nm-1]=timage
  threeview,/pixon,res=3,shellB,/surpressaxes
  save,file='shellB'+strtrim(string(floor(offset)),2)+'.img',shellB

  if (justoffset) then return

  fluxy=pixonmodels(N,'fluxrope',ignore,stuff)
  threeview,/pixon,res=3,fluxy,/log,/surpressaxes
  save,file='fluxy.img',fluxy

  fluxyB=fluxy
  cuberot3d,fluxyB,[30.0,0.0,0.0]
  threeview,/pixon,res=3,fluxyB,/log,/surpressaxes
  save,file='fluxyB.img',fluxyB

  timage=pixonmodels(Nm,'fluxrope',ignore,stuff)
  fluxrope=saito*0.0
;  fluxrope[N/2:(N/2)+Nm-1,0:Nm-1,0:Nm-1]=timage
  fluxrope[0:Nm-1,N/2:(N/2)+Nm-1,0:Nm-1]=timage
  threeview,/pixon,res=3,fluxrope,/log,/surpressaxes
  save,file='fluxrope.img',fluxrope

  save,file='dialme.imgs',shell,fluxrope,saito,fluxy,shellB,fluxyB

end

; optionally returns 'timage'

pro dialme,model,scaling,timage,noiselevel=noiselevel,nosave=nosave

  common images,fluxrope,shell,saito,fluxy,shellB,fluxyB

  nosave=setyesno(nosave)

  if (n_elements(scaling) eq 0) then scaling=0.0
  if (n_elements(noiselevel) eq 0) then noiselevel=0.0

  if (n_elements(fluxrope) eq 0) then $
    restore,'/home/antunes/RUNS/data/dialme/dialme.imgs'

;  restore,/v,'shellB12.img'

  case model of
      'fluxrope': timage=fluxrope
      'fluxy': timage=fluxy
      'fluxyB': timage=fluxyB
      'shell': timage=shell
      'shellB': timage=shellB
      'shellB4': timage=shellB
      'shellB8': timage=shellB
      'shellB12': timage=shellB
      'shellB16': timage=shellB
      else: timage=saito
  endcase

  print,'max of ',model,' is: ',max(timage)
  print,'max of raw saito is: ',max(saito)

  ; saito already is saito, and does not need saito added to it
  if (model ne 'saito') then $
    timage = (timage/scaling) + saito

  ; add optional noise
  N=n_elements(timage[0,0,*])
  timage = timage + sqrt(timage)*noiselevel*randomn(seed,N,N,N)

  if (nosave) then return

;  prep_3drv,pxwrapper,N=128,gamma=[0,90,0],theta=[0,0,90]

;  threeview,/pixon,res=3,/log,timage,/surpressaxes

  fname=model+'+saito_noise'+strtrim(string(floor(noiselevel)),2)+$
    'saitoscl='+strtrim(string(scaling),2)+'.img'
  save,timage,file=fname
  print,'saved ',fname

end

