;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
FUNCTION rot3dmatrix,alpha,inverse=inverse

  IF keyword_set(inverse) THEN return,rot3dmatrix_angles(alpha)

  alpha = double(alpha)
  ca = cos(alpha)
  sa = sin(alpha)

  mx = [[     1,     0,      0],$
        [     0, ca(0), -sa(0)],$
        [     0, sa(0),  ca(0)]]

  my = [[ ca(1),     0,  sa(1)],$
        [     0,     1,      0],$
        [-sa(1),     0,  ca(1)]]

  mz = [[ ca(2),-sa(2),      0],$
        [ sa(2), ca(2),      0],$
        [     0,     0,      1]]

  return,mz ## (my ## mx)
END

