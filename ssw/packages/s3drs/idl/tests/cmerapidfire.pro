;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; by default, does running difference.
; if /back, uses the daily median backgrounds
; if /preframe, uses the first frame for a cumulative evolution difference
; instead of 'seq' you can just give it a start and end date and it
;  will derive the intermediate ones as well.  Neglect an 'enddate'
;  and it will simple go to the end of that calendar day.

PRO cmerapidfire,N,back=back,seq=seq,checksn=checksn,$
                 preframe=preframe,setuponly=setuponly,$
                 startdate=startdate,enddate=enddate,$
                 occultright=occultright,occulttop=occulttop,$
                 occultleft=occultleft,occultbottom=occultbottom,zmask=zmask,$
                 nadjust=nadjust,mxit=mxit,flatnoise=flatnoise,best=best,$
                 c3=c3,excerpt=excerpt,standardplots=standardplots,prep=prep,$
                 polar=polar


  back=setyesno(back)
  checksn=setyesno(checksn)
  preframe=setyesno(preframe)
  setuponly=setyesno(setuponly)
  zmask=setyesno(zmask)
  best=setyesno(best)

  if (n_elements(N) eq 0) then N=64

  if (n_elements(startdate) ne 0) then begin
      ; if enddate, use defined interval, otherwise use startdate to get seq
      if (n_elements(enddate) ne 0) then $
        seq=deriveseq(startdate,enddate) else $
        seq=deriveseq(startdate)
  end

  if (n_elements(seq) eq 0) then return
;    seq=['20071231_002220',$
;         '20071231_005220',$
;         '20071231_012220',$
;         '20071231_015220',$
;         '20071231_022220',$
;         '20071231_025220',$
;         '20071231_032220',$
;         '20071231_035220',$
;         '20071231_042220']

  cme=seq[0]
print,seq
  for i=1,n_elements(seq)-1 do begin
      if (back) then prior='back' else prior=cme
      if (preframe) then prior=seq[0]
      cme=seq[i]
      if (setuponly) then print,'Prepping ',cme,' minus ',prior else $
        print,'Running ',cme,' minus ',prior
      quickcme,N,cme,prior,/cor2,c3=c3,excerpt=excerpt,$
        checksn=checksn,setuponly=setuponly,occultright=occultright,$
        occultleft=occultleft,occultbottom=occultbottom,occulttop=occulttop,$
        mxit=mxit,nadjust=nadjust,flatnoise=flatnoise,best=best,zmask=zmask,$
        standardplots=standardplots,prep=prep,polar=polar,/jump,datacut=1e-11
  end

END
