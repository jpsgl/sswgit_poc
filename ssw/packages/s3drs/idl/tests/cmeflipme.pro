;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
PRO showflipme,savemovie=savemovie,density=density,winid=winid,ps=ps,$
               prob=prob,oom=oom,fixed=fixed

  density=setyesno(density)
  savemovie=setyesno(savemovie)
  ps=setyesno(ps)
  prob=setyesno(prob)

  if (prob) then restore,'cmeseqprob.sav' else restore,'cmeseq.sav'

  ni=n_elements(slidene[*,0])

;  if (density) then loadct,15 else loadct,3; 9 is also good
  if (density) then loadct,5 else loadct,3; 9 is also good

  if (density) then for i=0,ni-1 do $
    tv_multi,fixed=fixed,labels=['Earth','Halo','Polar'],slidene(i,*),oom=oom,$
    winid=winid,/slide,max=1e6,ps=ps,filename=int2str(i)+'_ne.ps',/land,/log $
  else for i=0,ni-1 do $
    tv_multi,fixed=fixed,labels=['Earth','Halo','Polar'],slideren(i,*),$
    oom=oom,winid=winid,/slide,max=1e-11,title='Dec 31 CME, Cor2',ps=ps,/log,$
    filename=int2str(i)+'_pr.ps',/land

  if (savemovie) then begin
      for i=0,ni-1 do begin
          tv_multi,fixed=fixed,labels=['Earth','Halo','Polar'],slideren(i,*),$
            winid=winid,/slide,max=1e-11,title='Dec 31 CME, Cor2',oom=oom,/log
          fname='cme_'+int2str(i)+'.gif'
          write_gif,fname,tvrd()
      end
;    gifs2mpeg,'cmeflip.mpg','*.gif','.',/whirlgif
  end



END

PRO cmeflipme,N,seq=seq,prob=prob,nframes=nframes

  on_error,2
  pixon_env,'c',/local

  prob=setyesno(prob)

  if (n_elements(N) eq 0) then N=128

  if (n_elements(seq) eq 0) then seq=['20071231_015220',$
       '20071231_022220',$
       '20071231_025220',$
       '20071231_032220',$
       '20071231_035220',$
       '20071231_042220',$
       '20071231_045220',$
       '20071231_052220',$
       '20071231_055220',$
       '20071231_015220'] else if (n_elements(seq) eq 1) then $
    seq=deriveseq(seq)

  ni=n_elements(seq)
  slidene=ptrarr(ni,3)
  slideren=ptrarr(ni,3)

  prep_3drv,pxrender,N=128,gamma=[90,0,0],theta=[0,0,90],/cor2

;  print,seq

  ict=0
  for i=0,ni-1 do begin

      fname=seq[i]+'_'+int2str(N)+'_solved.sav'
;      print,'checking ',fname
      if (file_exist(fname)) then begin
;          print,'found ',ict
          restore,fname

          if (prob) then begin
              if (n_elements(nframes) eq 0) then nframes=pxwrapper.sr.Nd
              data=stereo_fetch(pxwrapper,oPixon,/dat)
              if (nframes ne pxwrapper.sr.Nd) then data=data[*,*,0:nframes-1]
              img=prob_cube(pxwrapper,data,nframes=nframes)
          end else begin
              img=stereo_fetch(pxwrapper,oPixon,/im)
          end

          threeview,img,/pixon,/noplot,imgtrio
          slidene(ict,0)=ptr_new(imgtrio[*,*,1]) ; Earth
          slidene(ict,1)=ptr_new(imgtrio[*,*,0]) ; Halo
          slidene(ict,2)=ptr_new(imgtrio[*,*,2]) ; Polar

          renders=pr_render(pxrender,img)
          slideren(ict,0)=ptr_new(renders[*,*,0]) ; Earth
          slideren(ict,1)=ptr_new(renders[*,*,1]) ; Halo
          slideren(ict,2)=ptr_new(renders[*,*,2]) ; Polar
          ++ict
      end

  end

  ; trim unused items
  slidene=slidene[0:ict-1,*]
  slideren=slideren[0:ict-1,*]

  if (prob) then begin
      save,slidene,slideren,file='cmeseqprob.sav'
      print,'Now type showflipme,/prob to view'
  end else begin
      save,slidene,slideren,file='cmeseq.sav'
      print,'Now type showflipme to view'
  end

END
