;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; given input data and header, returns rotated data
; plus the location of the sun center it found.
;
; if given /recentered flag, uses array ctr not hdr center
; (typically for when the data has been excerpted around the sun ctr)
;
; if told /rectify, rectified data that has crota=180, otherwise assumes data
; that has crota1=180 has not been rectified yet (user must then
; rectify it later in their code)
;

PRO p_lasco_to_hae,data,hdr,rectify=rectify,recentered=recentered

   rectify=setyesno(rectify)
   recentered=setyesno(recentered)

   tangle=get_lasco_solar_north(hdr)

                                ;if (hdr.crota1 eq 180) then
                                ;tangle=0-tangle; whoops, removed-- rotation is
                                ;always the same even if its unrectified

   if (recentered) then begin
       N2=0.5*(n_elements(data[0,*])-1)
       sunk0=[N2,N2]
   end else begin
       sunk0=get_sun_center(hdr)
   end

   data = rot(data,tangle,1.0,sunk0[0],sunk0[1],/pivot)

   if (hdr.crota1 eq 180 and rectify) then data=rotate(data,2)

END
