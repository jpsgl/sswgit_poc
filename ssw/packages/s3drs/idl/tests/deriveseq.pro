;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; accepts start dates of form '20080131_012220' or just '20080131'

FUNCTION deriveseq,startdate,enddate

  s1=strsplit(startdate,'_',/extract)
  if (n_elements(enddate) eq 0) then enddate=s1[0]+'_235220'
  s2=strsplit(enddate,'_',/extract)

  if (n_elements(s1) eq 1) then starthour=0 else $
    starthour=long(strmid(s1[1],0,2))
  endhour=long(strmid(s2[1],0,2))


  for i=starthour,endhour do begin
      if (i lt 10) then mid='_0' else mid='_'
      newtime1=s1[0] + mid + int2str(i) + '2220'
      newtime2=s1[0] + mid + int2str(i) + '5220'
      if (i eq starthour and 2220 lt s1[1]) then seq2=[newtime2] else $
      if (n_elements(seq2) eq 0) then seq2=[newtime1,newtime2] else $
        seq2=[seq2,newtime1,newtime2]
  end

  seq2=seq2[uniq(seq2[sort(seq2)])]

  return,seq2

END
