;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
FUNCTION ptrtocube,ptrset

  Nd=n_elements(ptrset)
  N=n_elements((*ptrset[0])[0,*])
  cube=fltarr(N,N,Nd)
  for i=0,Nd-1 do cube[*,*,i]=*ptrset[i]
  return,cube

END
