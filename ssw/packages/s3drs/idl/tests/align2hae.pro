;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; obsolete due to Bill's better scc_roll_image,hdr,data,SYSTEM='HAE'!

; given a stereo data and header, rotates it so up = HAE north
; (presently, up=spacecraft up only)
; Modifies both inputs.
; If you only give it a header, not a data, it modifies just that header

PRO align2HAE,hdr,data

    if (hdr.obsrvtry eq 'STEREO_A') then ab='A' else ab='B'

    angle=get_stereo_roll(hdr.date_obs,ab,SYSTEM='HAE')
    k0=[hdr.CRPIX1-1.0, hdr.CRPIX2-1.0] ; optical center [2]
    crvalarcsec=[hdr.CRVAL1, hdr.CRVAL2] ; offset in arcsec
    crval=crvalarcsec/hdr.cdelt1 ; convert to pixels
    sunk0=k0-crval

    if (n_elements(data) ne 0) then $
      data=rot(data,angle,1.0,sunk0[0],sunk0[1],/pivot)

    ; also move optical center based on this rotation and fill header
    newcrval=crval*0.0
    newcrval[0]=cos(angle*!dtor)*crval[0] - sin(angle*!dtor)*crval[1]
    newcrval[1]=sin(angle*!dtor)*crval[1] + cos(angle*!dtor)*crval[0]
    k0=sunk0+newcrval
    newcrvalarcsec=newcrval*hdr.cdelt1 ; convert back to arcsec
    hdr.CRVAL1=newcrvalarcsec[0]
    hdr.CRVAL2=newcrvalarcsec[1]
    hdr.CRPIX1=k0[0]+1.0
    hdr.CRPIX2=k0[1]+1.0

END
