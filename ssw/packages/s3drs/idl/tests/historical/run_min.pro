;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; see also test_real_3dr,pxwrapper,oPixon,datum,N=N,/ingest,ftol=0.0000006
on_error,2
if (n_elements(ptype) eq 0) then ptype='c'; 'c' or 't'
profile=0
rescale=4

; a little syntaxtical sugar
if (ptype eq 'c') then c=1 else c=0
if (ptype eq 't') then t=1 else t=0
if (c) then print,'Classic Pixon test'
if (t) then print,'Tetra Pixon test'

print,'... loading data, object, structures'
; load pre-existing datum
fname='n32_2fluxrope_datum.sav'
restore,filename=fname,/verbose
; load pre-existing pixon object
if (c) then fname='n32_cpixonobject.sav'; CLASSIC
if (t) then fname='n32_tpixonobject.sav'; TETRA
restore,filename=fname,/verbose
; load pre-existing pixon structures
if (c) then fname='n32_cpxwrapper.sav'; CLASSIC
if (t) then fname='n32_tpxwrapper.sav'; TETRA
restore,filename=fname,/verbose

; add weighting
weight =  make_array(N,N,Nd,value=1); a flat field for now
;weight[0:N/2,0:N/2,*]=0; ignore one quarter of each datum
oPixon->Set,wt=weight

; set up environment
N = n_elements(datum[*,0,0])
Nd = n_elements(datum[0,0,*])
if (c) then pixon_env,'c',/force; CLASSIC
if (t) then pixon_env,'t',/force; TETRA

print,'... recovering original image'
; recover original image, just for comparison later
timage=oPixon->Get(/image,/nop)
; need to plot it here, before it overwrites
threeview,timage,title='original image',rescale=rescale

print,'... inserting data'
; insert data, assuming positions are as given in original oPixon object
if (c) then oData = oPixon->get(/oData); CLASSIC
if (c) then for m=0,nd-1 do oData[m]->Set, data=PTR_NEW(datum[*,*,m]); CLASSIC
if (t) then data  = PTRARR(1,nd); TETRA
if (t) then for m=0,nd-1 do data[0,m]=ptr_new(datum[*,*,m]); TETRA
if (t) then oPixon->Replace,data=data; TETRA

; view it
sat_los_view,pxwrapper.sr.gamma,/label,wsize=400

;print,'... setting noise sigma, for classic'
if (c) then ftol=pxwrapper.sn.ftol
if (c) then sigma=ftol*max(datum); simple gaussian noise
;if (c) then oPixon->Replace,sigma=sigma
if (c) then print,'ftol: ',ftol
if (c) then print,'sigma: ',sigma
if (t) then print,'sigma is NxN array, all of value 1'

print,'... create sigma as poisson6 noise'
Smax=10
Smin=1
Dfmin=0
Dfmax=max(data)
a = (Dfmax-Dfmin)/(Smax^2-Smin^2)
Nb = (Dfmax*Smin^2-Dfmin*Smax^2)/(Dfmax-Dfmin)
sigma= sqrt(a*(data+a*Nb))
; alternately, could add noise to data instead (skip for now)
;DN2photons = 47
;sigma = sqrt( data * DN2photons)

oPixon->Replace,sigma=sigma

print,'... running reconstruction (pseudo)'
; run reconstruction
; oPixon->Replace,qtol=qtol
oPixon->Replace, mxit=100

if (profile) then profiler, /system & profiler
t1=systime(/seconds)
print,'starting: ',systime()

oPixon->pseudo
; or use p_pseudoloop,pxwrapper,oPixon,20,2

print,'ending: ',systime()
deltaT=(systime(/seconds) - t1)
deltaMin = fix(deltaT/60)
deltaSec = deltaT-(60*deltaMin)
print,'runtime = ',deltaMin,':',deltaSec
if (profile) then profiler,/report
if (profile) then profiler,/clear,/system
if (profile) then profiler,/clear,/reset

print,'... fetching back original data'
; get original data
pdata = oPixon->Get(/data)
data=dblarr(n,n,nd);
for i=0,nd-1 do data[*,*,i]=*pdata[i]

print,'... getting reconstruction solution data'
; get the reconstructed images
image = oPixon->Get(/image,/nop); CLASSIC
mdata=dblarr(n,n,nd);
if (c) then for m=0,nd-1 do mdata[*,*,m] = oData[m]->dsf(image); CLASSIC
;if (t) then dm = REFORM(oPixon -> Get(/DM), 1, nd, /OVERWRITE); TETRA
;if (t) then for i=0,nd-1 do mdata[*,*,i]=*dm[0,i]; TETRA
if (t) then dm = oPixon -> Get(/DM); TETRA
if (t) then for i=0,nd-1 do mdata[*,*,i]=*dm[i]; TETRA

print,'... getting residuals'
pres = oPixon->Get(/res)                ; get residuals
nd=n_elements(pres)
res=dblarr(n,n,nd)
for i=0,nd-1 do res[*,*,i]=*pres[i]

print,'... getting Q'
;pxncnt=oPixon->Get(/pxncnt) ; get number of pixons, CLASSIC
;print,pxncnt
q = oPixon->Get(/qval)      ; get q value
print,q

; some plots
threeview,image,title='fit image',rescale=rescale
tv_multi,data,title='original data',rescale=rescale
tv_multi,mdata,title='fit data',rescale=rescale
tv_multi,res,title='fit residuals',rescale=rescale

; save info
p_report,pxwrapper,oPixon

; rerun
;oPixon->Setimage,image=fake_ne_cube(N);
;oPixon->pseudo
