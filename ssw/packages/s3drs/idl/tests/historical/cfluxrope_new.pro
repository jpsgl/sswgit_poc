;+
; $Id: cfluxrope_new.pro,v 1.1 2009/04/22 18:33:24 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : cfluxrope_new.pro
;               
; Purpose   : script to run tetrahedreal (new) Pixon cfluxrope routines
;               
; Explanation: soon to be obsolete in favor of generalized environment
;               
; Use       : IDL> @cfluxrope_new
;    
; Inputs    : none
;               
; Outputs   : output of Pixon
;
; Keywords  : none
;               
; Calls from LASCO : none 
;
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: changes environment and path
;               
; Category    : Simulation
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRL, Nov-Dec 2005
;               
; $Log: cfluxrope_new.pro,v $
; Revision 1.1  2009/04/22 18:33:24  antunes
; first half of relocation.
;
; Revision 1.1  2008/03/03 19:21:50  antunes
; Reorganized test directory for clarity and archiving.
;
; Revision 1.1  2006/11/17 19:02:22  antunes
; Test cases for Pixon.
;
; Revision 1.2  2005/12/27 17:48:23  antunes
; Commented half of the routines using SSW style.  Also added a new
; axes test case 'arrowcube'.
; Checked in the Tomography/old/mikic_img.pro routine here because it
; is a dependency for the general image-prep routine 'get_timage.pro'
;
;-            

common debugs,phys,tdsf,tdsft,dbg

.run /net/mercury/data1/pixxon/toopixon_2.0pre3/tpxnproj/tpxnproj__jpol_02	; use new version

.run ffunc_07
.run transform_data_07
.run convert_image_07
.run get_timage_07
.run stereo_rsetup_07
.run flimb_07

.run get_istd_07

.run stereo_noise_07


;@diagn6_compile 

note='cfluxrope'
if(n_elements(term) eq 0)then term='batch'

dfac=0.95

; -----

;N=16
N=32
;N=64
;N=128
RN=double(N)

if(n_elements(tau) eq 0)then tau=0;60.0/N/214.943

lla0=[1,1,1]*0.5*(N-1)  ; sun at center of image cube

; ----- sr

print,N,lla0,tau
RN=double(N)
if(n_elements(chl)   eq 0)then chl = 2
if(n_elements(ch)    eq 0)then ch  = 'fluxrope'
stuff=[-90,0]
if(n_elements(polar) eq 0)then polar = ['b','b'];!!!!!!! reset to simple sum in jpol
fan   = [0,0]
L0    = 214.943           ; 1au in solar radii
LL    = [1,1]               ; satellite distances
if(n_elements(gamma) eq 0)then gamma=[0.001,90.001]
theta  = [90,90]
k0    = make_array(2,2,value = 0.5*(N-1),/double)
z0    = [0,0]
L0tau = 60./RN  ; 12/RN is C2, 60/RN is C3
if(n_elements(d) eq 0)then d  =  1.00
eps   = [0,0]
set   = [0,0]
if(n_elements(interp) eq 0)then interp = 5
psp   = -1
n0    = 1.0e8
imodel = 2	; 1=rough core1 model  2=C3 model
Istd  = 0.0
as    = [1,1]

; ----- sc

gname = 'cfluxrope'

if(n_elements(term) eq 0)then term = 'batch'
sat_name = ['1','2']
t00 = 0
note = 'cfluxrope'

;----- sp

anneal_3d  =  0

jsched = [-1,0,0,0,0]
msched = [50,100,100,100,100]
rsched = [1,1,1,1,1]*1.0e-5
starter  =  '0'

if(n_elements(reltol) eq 0)then reltol  =  1.0e-5 ;
if(n_elements(abstol) eq 0)then abstol  =  1.0e-5 ;

if(n_elements(mxit) eq 0)then mxit  =  50
npo   =  4
npxn  =  3*npo+1

; ----- sn

Smax=40
Smin=1

ftol = [1/float(Smax)^2,1/float(Smin)^2]  
fsig = ftol
;noisemod = 'poisson - in run'
noisemod = 'poisson6'
seed = 0

; -----

@build_input_07

jname = sc.gname+'.jou'
journal,jname

print,'****************************'
get_lun,lun
openr,lun,'run2_zshell.pro'
line=''
on_ioerror,IOE &$
while(1) do begin & readf,lun,line & print,line & end &$
IOE: close,lun
print,'****************************'
print,''

print,'**********************************'
print,'N      = ',N
print,'tau    = ',tau
print,'gamma  = ',gamma
print,'**********************************'

; =====

; ----- get true image

print,'Calling get_timage'
get_timage,sr,sc,timage,stuff,stuff1

; ----- center the data

print,'Calling center_data'
center_data_06,sr
sr.k0=sr.k0+[1,1]*.0001

;; ----- setup

print,'Calling stereo_rsetup'
stereo_rsetup,sr,sp,sc

;fname=sc.gname+'_'+sc.tag               ; define generic file name
;
;jname = fname+'.jou'                    ; rename journal file
;spawn,'mv '+sc.gname+'.jou '+jname
;
;;save,filename=fname+'t.dat',timage       ; save timage
;
;; ----- get the data
;
;;true data
;get_data,N,1.00,datat,sigmat,aa=aa,bb=bb,cc=cc
;datat=datat/0.2		; norm to datamax ~ 1
;stereo_noise,datat,sn,sigmat
;
;;fake data
;timage=timage/1.0e6/0.2		; norm to datamax ~ 1
;dataf=stereo_project(sr,sc,timage,oPixon,userstruct)
;stereo_noise,dataf,sn,sigmaf
;
;; zshell's fake data
;data1=readfits('moduslabTB01.fts',h)
;data1=rebin(data1,N,N)
;data2=readfits('moduslabTB02.fts',h)
;data2=rebin(data2,N,N)
;dataa=[[[data1]],[[data2]]]
;dataa=dataa/max(dataa)
;dataa=dataa*max(dataf)/3.

; ----- Run TPIXON programs

mxit=500

;@run_zshell_set
 ndata=1
 npersp=0;214.943/60.
 npol=1
 nview=2
 seed0=0
 theta=sr.gamma
 ;wait=0.
 display=0
 dim = sr.n


recsave=1
seed0=0
display=0
.run zshell_make2_tpxn
hnote=note

qadd=3
qrem=2
recsave=1
disp=0
lstop=0
niter=100
note=hnote	; so notes dont accumulate for multiple runs on one make
runfname='run_zshell.pro'
.run zshell_run2_tpxn

