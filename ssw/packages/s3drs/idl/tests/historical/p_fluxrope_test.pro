;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            

; extremely simple test case for Pixon, create and render a fluxrope
; 'batch' just means 'run but do not plot'
; Default is 3 views, but a 2-view case (views=2) is also set up.
; Values of 'views' other than 2 or 3 are meaningless and default to 3.

PRO p_fluxrope_test,N=N,iterations=iterations,hours=hours,batch=batch,$
                    pixontype=pixontype,interp=interp,anneal=anneal,$
                    views=views

;   .reset
   on_error,2 
   batch=setyesno(batch)
   if (n_elements(N) eq 0) then N=32; default case is tiny and fast
   if (n_elements(iterations) eq 0) then iterations=10; just a quick fit
   if (n_elements(hours) eq 0) then hours=0; optional hours flag for fitting
   if (n_elements(pixontype) eq 0) then pixontype='classic'; the default
   if (n_elements(anneal) eq 0) then anneal=0; default case is to not anneal
   if (n_elements(views) eq 0) then views=3; default case is 3 views
   if (views ne 2 and views ne 3) then views=3

   rname='fluxropesim_'+trim(string(views),2)+'view_'
   if (anneal) then rname=rname+'A'
   rname=rname+strmid(pixontype,0,1)

;  views=3 
   if (views eq 2) then gamma=[90,180] else gamma=[0,30,60]
;  prep_3drv,pxwrapper,views=views,N=N,gamma=[0,30,60]
  prep_3drv,pxwrapper,views=views,N=N,gamma=gamma

 ; flag to test non-C IDL dsf/dsft aka interp=0 or 1 instead of def. 5
  if (n_elements(interp) ne 0) then pxwrapper.sr.interp=interp

  modelname='fluxrope'
;  timage=pixonmodels(N,modelname,pxwrapper,stuff,stuff1)
; we skip timage to make a really big fluxrope instead
  restore,'/net/corona/cplex3/reiser/a/ftimage_256.dat',/verbose
  timage = timg[129-N/2:128+N/2,129-N/2:128+N/2,129-N/2:128+N/2]

  timage=timage/max(timage); norm to datamax ~ 1
  pxwrapper.sr.n0=1; 1e8

  p_generate_pixon,pixontype,pxwrapper,oPixon,image=timage
;   img=oPixon->Get(/im,/nop)
;   threeview,image,res=6
;   p_generate_pixon,'tetra',pxwrapper,oPixon,image=timage
   d_orig=stereo_project(pxwrapper,oPixon)

   print,"image and data max, and n0: ",max(timage),max(d_orig),pxwrapper.sr.n0

   ; normalize it
;   dmax=max(d_orig)
   d_orig = (d_orig /max(d_orig) ) * pxwrapper.sr.n0

   p_insert_data,pxwrapper,d_orig,oPixon

   ; add noise
   sigma = fractional_noise(d_orig,/sigma)
   oPixon->Replace,sigma=sigma
;   tv_multi,d_orig,/axes,/border
   p_pixonloop,pxwrapper,oPixon,2,iterations/2,/init,/flush,hours=hours,$
     anneal=anneal

   ; extra save here of intermediate items for this test case
   savename=p_encodename(pxwrapper.sr.Ncube,rname,getversion=version)
   save,file=savename,pxwrapper,oPixon

   d_new=stereo_project(pxwrapper,oPixon)
   if (pixontype eq 't' or pixontype eq 'tetra') then begin
       ; residuals with /nop fails in tetra for some pixon reason
       ; so we do this pointer-to-data hack instead
       d_res = ptrs2cube(oPixon->Get(/res))
   end else begin
       d_res=oPixon->Get(/res,/nop)
   end

    dset = [[[d_res]],[[d_new]],[[d_orig]]]
   labels = ['','','','recon','recon','recon','A','LASCO','B']

;   relsize=long(128/N)
;   tv_multi,dset,/addslice,yrow=N/2,linebreak=3,/axes,/border, $
;     labelset=labels,title='Fluxrope 3-view Sim',/unique,/log,res=relsize
;   write_gif,'fluxrope_3view_sim.gif',tvrd()

   ; extra save here of intermediate items for this test case
   savename=p_encodename(pxwrapper.sr.Ncube,rname,getversion=version)
   save,file=savename,pxwrapper,oPixon,d_orig,d_new,d_res,sigma,rname,labels
   print,'data is saved in: ',savename

   if (batch) then begin
       p_report,pxwrapper,oPixon,runname=rname,version=version
   end else begin
       p_report,pxwrapper,oPixon,runname=rname,version=version
   end

END
