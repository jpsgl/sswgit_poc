;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; using a real CME event on Dec 30th around 06:01:30 to try a
; 3D reconstruction


;### COR 1

on_error,2

;list=scclister()
;img=sccreadfits(dir_cor2a+fname_cor2a_000,hdr,/nodata)
;print,hdr.polar

dir_cor2a='/net/venus/secchi2/lz/cor2/L0/a/seq/20061230/'
;fname_cor2a_000='20061230_060130_s4c2A.fts'
;fname_cor2a_120='20061230_060143_s4c2A.fts'
;fname_cor2a_240='20061230_060156_s4c2A.fts'
fname_cor2a_000='20061230_060220_s4c2A.fts'
fname_cor2a_120='20061230_060233_s4c2A.fts'
fname_cor2a_240='20061230_060246_s4c2A.fts'
;fname_cor2a_000='20061230_063130_s4c2A.fts'
;fname_cor2a_120='20061230_063143_s4c2A.fts'
;fname_cor2a_240='20061230_063156_s4c2A.fts'

dir_cor2b='/net/venus/secchi2/lz/cor2/L0/b/seq/20061230/'

;fname_cor2b_000='20061230_060240_s4c2B.fts'
fname_cor2b_000='20061230_060240_s7c2B.fts'
;fname_cor2b_120='20061230_060250_s4c2B.fts'
fname_cor2b_120='20061230_060250_s7c2B.fts'
;fname_cor2b_240='20061230_060300_s4c2B.fts'
fname_cor2b_240='20061230_060300_s7c2B.fts'
;fname_cor2b_000='20061230_063240_s4c2B.fts'
;fname_cor2b_000='20061230_063240_s7c2B.fts'
;fname_cor2b_120='20061230_063250_s4c2B.fts'
;fname_cor2b_120='20061230_063250_s7c2B.fts'
;fname_cor2b_240='20061230_063300_s4c2B.fts'
;fname_cor2b_240='20061230_063300_s7c2B.fts'

fnames2a=[fname_cor2a_000,fname_cor2a_120,fname_cor2a_240]
fnames2a=dir_cor2a+fnames2a
fnames2b=[fname_cor2b_000,fname_cor2b_120,fname_cor2b_240]
fnames2b=dir_cor2b+fnames2b

dir_cor1a='/net/venus/secchi2/lz/cor1/L0/a/seq/20061230/'
;fname_cor1a_000='20061230_060100_s4c1A.fts'
;fname_cor1a_120='20061230_060110_s4c1A.fts'
;fname_cor1a_240='20061230_060120_s4c1A.fts'
fname_cor1a_000='20061230_063100_s4c1A.fts'
fname_cor1a_120='20061230_063110_s4c1A.fts'
fname_cor1a_240='20061230_063120_s4c1A.fts'

dir_cor1b='/net/venus/secchi2/lz/cor1/L0/b/seq/20061230/'
;fname_cor1b_000='20061230_060140_s4c1B.fts'
;fname_cor1b_120='20061230_060149_s4c1B.fts'
;fname_cor1b_240='20061230_060158_s4c1B.fts'
;fname_cor1b_000='20061230_061140_s4c1B.fts'
;fname_cor1b_120='20061230_061149_s4c1B.fts'
;fname_cor1b_240='20061230_061158_s4c1B.fts'
;fname_cor1b_000='20061230_062140_s4c1B.fts'
;fname_cor1b_120='20061230_062149_s4c1B.fts'
;fname_cor1b_240='20061230_062158_s4c1B.fts'
fname_cor1b_000='20061230_063140_s4c1B.fts'
fname_cor1b_120='20061230_063149_s4c1B.fts'
fname_cor1b_240='20061230_063158_s4c1B.fts'
;fname_cor1b_000='20061230_064140_s4c1B.fts'
;fname_cor1b_120='20061230_064149_s4c1B.fts'
;fname_cor1b_240='20061230_064158_s4c1B.fts'
;fname_cor1b_000='20061230_065140_s4c1B.fts'
;fname_cor1b_120='20061230_065149_s4c1B.fts'
;fname_cor1b_240='20061230_065158_s4c1B.fts'

fnames1a=[fname_cor1a_000,fname_cor1a_120,fname_cor1a_240]
fnames1a=dir_cor1a+fnames1a
fnames1b=[fname_cor1b_000,fname_cor1b_120,fname_cor1b_240]
fnames1b=dir_cor1b+fnames1b

secchi_prep,fnames1a,header1a,image1a,/polariz_on; outsize=64; /pB  FAILS
secchi_prep,fnames1b,header1b,image1b,/polariz_on; outsize=64; /pB
writefits,'1b.fts',image1b,struct2fitshead(header1b)
p_ingest_fits,'1b.fts',data1b,mask1b,srjr1b,/verbose
secchi_prep,fnames2a,header2a,image2a,/polariz_on; outsize=64; /pB
secchi_prep,fnames2b,header2b,image2b,/polariz_on; outsize=64; /pB

testimg=sccreadfits(dir_cor1a+fname_cor1a_000,hdr)


;# MANUALLY ADDING FRAMES

testimg1b1=sccreadfits(dir_cor2b+fname_cor2b_000)
testimg1b2=sccreadfits(dir_cor2b+fname_cor2b_120)
testimg1b3=sccreadfits(dir_cor2b+fname_cor2b_240)
testimage1b=testimg1b1+testimg1b2+testimg1b3

on_error,2
fnameAcor1=[$
         '20061230_050100_s4c1A.fts',$
         '20061230_050110_s4c1A.fts',$
         '20061230_050120_s4c1A.fts']
fnameBcor1=[$
         '20061230_050140_s4c1B.fts',$
         '20061230_050149_s4c1B.fts',$
         '20061230_050158_s4c1B.fts']

secchi_prep,fnameAcor1,headerAcor1,imageAcor1,/polariz_on; outsize=64; /pB
tv_multi,imagea,res=0.5

secchi_prep,fnameBcor1,headerBcor1,imageBcor1,/polariz_on; outsize=64; /pB
hdr=headerBcor1
mask=p_getmask('COR1',hdr.NAXIS1,[hdr.CRPIX1,hdr.CRPIX2],HDR.cdelt1/HDR.rsun)
tv_multi,imageBcor1

tA1_1=sccreadfits('20061230_050100_s4c1A.fts',hdr)
tA1_2=sccreadfits('20061230_050110_s4c1A.fts',hdr)
tA1_3=sccreadfits('20061230_050120_s4c1A.fts',hdr)
tsumA=tA1_1+tA1_2+tA1_3
tv_multi,tsumA


tB1_1=sccreadfits('20061230_050140_s4c1B.fts',hdr)
tB1_2=sccreadfits('20061230_050149_s4c1B.fts',hdr)
tB1_3=sccreadfits('20061230_050158_s4c1B.fts',hdr)
tsumB=tB1_1+tB1_2+tB1_3
mask=p_getmask('COR1',hdr.NAXIS1,[hdr.CRPIX1,hdr.CRPIX2],HDR.cdelt1/HDR.rsun)
tv_multi,tsumB


;### COR 2


on_error,2

tA2_1=sccreadfits('20061230_050220_s4c2A.fts',hdr)
tA2_2=sccreadfits('20061230_050233_s4c2A.fts',hdr)
tA2_3=sccreadfits('20061230_050246_s4c2A.fts',hdr)
tsumA=tA2_1+tA2_2+tA2_3
tv_multi,tsumA

a2names=['20061230_050220_s4c2A.fts',$
         '20061230_050233_s4c2A.fts',$
         '20061230_050246_s4c2A.fts']
secchi_prep,a2names,hdr,imageAcor2,/polariz_on; outsize=64; /pB
tv_multi,imageAcor2,res=0.5

mask=p_getmask('COR2',hdr.NAXIS1,[hdr.CRPIX1,hdr.CRPIX2],HDR.cdelt1/HDR.rsun)


tB2_1=sccreadfits('20061230_050240_s4c2B.fts',hdr)
tB2_2=sccreadfits('20061230_050250_s4c2B.fts',hdr)
tB2_3=sccreadfits('20061230_050300_s4c2B.fts',hdr)
tsumB=tB2_1+tB2_2+tB2_3
tv_multi,tsumB

b2names=['20061230_050240_s4c2B.fts',$
         '20061230_050250_s4c2B.fts',$
         '20061230_050300_s4c2B.fts']
secchi_prep,b2names,hdr,imageBcor2,/polariz_on; outsize=64; /pB
tv_multi,imageBcor2,res=0.5


;### FCORONA test

imageBcor2sub=fcorona(imageBcor2,struct2fitshead(hdrstr),fdata)

tv_multi,(imageBcor2*mask < 1.5e-9),res=0.15,/log,title='BCor2'
tv_multi,(imageBcor2*mask < 1.5e-9),res=0.15,/log 
plot,(imageBcor2*mask < 1.5e-9),title='BCor2'

tv_multi,fdata*mask,res=0.15,title='Fcorona'
tv_multi,fdata*mask,res=0.15
plot,fdata*mask,title='Fcorona'

tv_multi,(imageBcor2sub*mask),res=0.15,title='BCor2 - Fcorona'
tv_multi,(imageBcor2sub*mask),res=0.15
plot,(imageBcor2sub*mask < 1.5e-9),title='BCor2-Fcorona'


;### pB test
on_error,2

b2names=['20061230_050240_s4c2B.fts',$
         '20061230_050250_s4c2B.fts',$
         '20061230_050300_s4c2B.fts']
secchi_prep,b2names,hdr,Bcor2tB,/polariz_on; outsize=64; /pB
secchi_prep,b2names,hdr,Bcor2pB,/polariz_on,/pB; outsize=64; /pB
mask=p_getmask('COR2',hdr.NAXIS1,[hdr.CRPIX1,hdr.CRPIX2],HDR.cdelt1/HDR.rsun)
pbdiff = Bcor2tB-Bcor2pB
pset=[[[Bcor2tB]],[[Bcor2pB]],[[pbdiff]]]
tv_multi,pset,res=0.15,/log,/slice,/add,/dec,title='BCor2: tB, pB, tB-pB'
mset=pset
for i=0,n_elements(mset[0,0,*])-1 do mset[*,*,i]=mset[*,*,i]*mask
tv_multi,mset,res=0.15,/slice,/add,/dec,title='BCor2 masked: tB, pB, tB-pB'
