;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; loads in paul's image and renders, then renders with Pixon renderer
; to simply compare renders

; pick one of these three inputs

;fin='/home/antunes/dev/tests/earlier/out_mikic__032_04a_04.datx'
;fin='/home/antunes/dev/tests/earlier/out_shell__016_05_01.datx'
fin='/home/antunes/dev/tests/earlier/out_chen__032_02_04.datxxx'

restore,fin,/verb

N=(size(data))[1]
Nd=(size(data))[3]
;Nele=n_elements(stereo_in.z0)

;if (Nele ne Nd) then trans=Nd/Nele else trans=0

 gamma=fltarr(Nd)
 gamma=interweave(stereo_in.gamma,gamma)

prep_3drv,pxwrapper,views=Nd,N=N,gamma=gamma

 if (n_elements(istd) ne 0) then pxwrapper.sr.istd=istd

 pxwrapper.sr.interp=stereo_in.interp

 if (pxwrapper.sr.interp eq 3) then pxwrapper.sr.interp=1; bug in finding C

 pxwrapper.sr.psp=stereo_in.psp

 pxwrapper.sr.rho=pxwrapper.sr.gamma
; pxwrapper.sr.theta=?? ; only for tpixon
; pxwrapper.sr.phi=pxwrapper.sr.theta; only for tpixon

 pxwrapper.sr.lla0=stereo_in.lla0

; ;whoops, this interveaves, does not replicate as needed
; z0=stereo_in.z0
; if (trans ne 0) then for i=1,trans-1 do z0=[z0,stereo_in.z0]
; pxwrapper.sr.z0=z0

; ;now routine-ized
; z0=stereo_in.z0
; newz0=pxwrapper.sr.z0
; if (trans ne 0) then for i=0,Nd-1 do newz0[i]=z0[i/trans] else newz0=z0
; pxwrapper.sr.z0=newz0
 pxwrapper.sr.z0=interweave(stereo_in.z0,pxwrapper.sr.z0)

 ; special case idiosyncratic polar
 if (stereo_in.polar[0] eq 'yes') then stereo_in.polar='b'
 pxwrapper.sr.polar=interweave(stereo_in.polar,pxwrapper.sr.polar)

 if (tag_exist(stereo_in,'fan')) then $
   pxwrapper.sr.fan=interweave(stereo_in.fan,pxwrapper.sr.fan)

 ; note k0 is an (2xNdish) array destined for a set of Nd [2] pointers
 tempk0=fltarr(Nd)
 kold=stereo_in.k0[0,*]
 k0a=interweave(kold,tempk0)
 kold=stereo_in.k0[1,*]
 k0b=interweave(kold,tempk0)

 for i=0,Nd-1 do pxwrapper.sr.k0[i] = ptr_new([k0a[i],k0b[i]])
; for i=0,Nd-1 do *(pxwrapper.sr.k0[i])=stereo_in.k0[*,i]

 for i=0,Nd-1 do *(pxwrapper.sr.sunk0[i])=*(pxwrapper.sr.k0[i])

 pxwrapper.sr.d=stereo_in.d
 pxwrapper.sr.L0tau=replicate(stereo_in.l0tau,Nd)

 pxwrapper.sr.LL=interweave(stereo_in.LL,pxwrapper.sr.LL)

 pxwrapper.sr.tau=replicate(stereo_in.tau,Nd)

 sat_name=stereo_in.sat_name
 pxwrapper.sr.sat_name=interweave(stereo_in.sat_name,pxwrapper.sr.sat_name)

 pxwrapper.sr.tele_name=['','','','','','']
 pxwrapper.sr.detector=['','','','','','']
 pxwrapper.sr.n0=stereo_in.n0
 pxwrapper.sr.roco=stereo_in.roco
 pxwrapper.sr.ch=stereo_in.ch
 pxwrapper.sr.chl=stereo_in.chl
 pxwrapper.sn.seed=stereo_in.seed
 pxwrapper.sn.fsig=[stereo_in.fsig,1.0]
 pxwrapper.sn.ftol=[stereo_in.ftol,1.0]
 if (tag_exist(stereo_in,'anneal')) then $
   pxwrapper.sp.anneal_3d=stereo_in.anneal
 pxwrapper.sn.noisemod=stereo_in.noisemod
 pxwrapper.sc.date=stereo_in.date
 pxwrapper.sc.note=stereo_in.note
 pxwrapper.sc.t00=stereo_in.t00
 pxwrapper.sp.mxit=stereo_in.mxit

 pxwrapper.sc.gname='comparison test'

p_generate_pixon,'classic',pxwrapper,oPixon,image=image

sandydata=stereo_project(pxwrapper,oPixon)

scaleddata=sandydata*(max(mdata)/max(sandydata))
minus=mdata-scaleddata

!p.multi=[0,1,4]
plot,mdata,font=11,title='Paul Render '+pxwrapper.sr.ch
plot,sandydata,font=11,title='Sandy Render'
plot,scaleddata,font=11,title='Sandy Render, scaled to Paul norm'
plot,minus,font=11,title='Paul-Sandy_scaled'
write_gif,'render_plot_'+pxwrapper.sr.ch+'.gif',tvrd()
!p.multi=0

dset=[[[minus]],[[scaleddata]],[[mdata]]]
l1=['Paul-Sandy_scaled']  
l2=['Sandy render, scaled']
l3='Paul render'
for i=1,Nd-1 do l1=[l1,l1[0]]
for i=1,Nd-1 do l2=[l2,l2[0]]
for i=1,Nd-1 do l3=[l3,l3[0]]
labels=[l1,l2,l3]
tv_multi,dset,res=4,/log,line=Nd,label=labels,$
  title='Render comparison '+pxwrapper.sr.ch,ct=3
write_gif,'render_comp_'+pxwrapper.sr.ch+'.gif',tvrd()
