;+
; $Id: cfluxrope_classic_update.pro,v 1.1 2009/04/22 18:33:23 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : cfluxrope_classic.pro
;               
; Purpose   : script to run classic Pixon cfluxrope routines
;               
; Explanation: soon to be obsolete in favor of generalized environment
;               
; Use       : IDL> @cfluxrope_classic
;    
; Inputs    : none
;               
; Outputs   : output of Pixon
;
; Keywords  : none
;
; Calls from LASCO : none
;
; Common    : none
;               
; Restrictions: none
;               
; Side effects: changes environment and path
;               
; Category    : Simulation
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRL, Nov 2005
;               
; $Log: cfluxrope_classic_update.pro,v $
; Revision 1.1  2009/04/22 18:33:23  antunes
; first half of relocation.
;
; Revision 1.1  2008/03/03 19:21:50  antunes
; Reorganized test directory for clarity and archiving.
;
; Revision 1.1  2006/11/17 19:02:22  antunes
; Test cases for Pixon.
;
; Revision 1.1  2006/03/07 16:30:35  antunes
; Env fixes.
;
; Revision 1.2  2005/12/27 17:48:23  antunes
; Commented half of the routines using SSW style.  Also added a new
; axes test case 'arrowcube'.
; Checked in the Tomography/old/mikic_img.pro routine here because it
; is a dependency for the general image-prep routine 'get_timage.pro'
;
;-            

common debugs,phys,tdsf,tdsft,dbg

LINKIMAGE,'VARRAY','/home/antunes/varray.so',1,'varray',min_args=1,max_args=10,/keywords

phys=0

@allstart
@startup_113

.run /net/mercury/data1/pixxon/Tomography/old/pxndata__dsf_06.pro
.run /net/mercury/data1/pixxon/Tomography/old/pxndata__dsft_06.pro
.run disumt1_inc_06

.run ffunc_06
.run transform_data_06
.run convert_image_03b
.run get_large_timage_07
.run flimb_06
.run disum1_inc_06
.run disumt1_inc_06
.run disum3_inc_06
.run disumt3_inc_06
.run get_istd_06

.run stereo_noise_06
.run stereo_rsetup_06
.run stereo_project_06
.run stereo_pixon_06

;@diagn6_compile 

note0='streamer belt'
if(n_elements(term) eq 0)then term='batch'

;mypath = getenv('IDL_PATH')
;print,'IDL_PATH = ',mypath

ftol=0.005
dfac=0.95

; ----------

version='stereo_pixon: 06'

dbg=-1.0

tdsf=0
tdsft=0

; ----- debugs common

if(n_elements(phys) eq 0)then phys=1

N=32
;N=16
;N=32
;N=64
;N=128
;N=256
;N=512
RN=double(N)

old_debug = SHMDEBUG(0) ; 1=Enable debug mode 


if(n_elements(tau) eq 0)then tau=0;30.0/N/214.943

lla0=[1,1,1]*0.5*(N-1)  ; sun at center of image cube

; ----- sr


print,N,lla0,tau
RN=double(N)
if(n_elements(chl)   eq 0)then chl = 2
if(n_elements(ch)    eq 0)then ch  = 'fluxrope'
;if(n_elements(ch)    eq 0)then ch  = 'solid'
stuff=-1
if(n_elements(polar) eq 0)then polar = ['b','b']
fan   = [0,0]
L0    = 214.943           ; 1au in solar radii
LL    = [1,1]               ; satellite distances
gamma=[0,90]
roco  = [0,0]
k0    = make_array(2,2,value = 0.5*(N-1),/double)
z0    = [0,0]
L0tau = 60./RN  ; 12/RN is C2, 60/RN is C3
if(n_elements(d) eq 0)then d  =  1.00
eps   = [0,0]
set   = [0,0]
if(n_elements(interp) eq 0)then interp = 5
psp   = -1
n0    = 1.0e8
Istd  = 0.0
as    = [1,1]

; ----- sc

gname = 'cfluxrope'

if(n_elements(term) eq 0)then term = 'batch'
sat_name = ['1','2']
note = 'cfluxrope'
t00 = 0

;----- sp

anneal_3d  =  0

jsched = [-1,0,0,0,0]
msched = [50,100,100,100,100]
rsched = [1,1,1,1,1]*1.0e-5
starter  =  '0'

if(n_elements(reltol) eq 0)then reltol  =  1.0e-5 ;
if(n_elements(abstol) eq 0)then abstol  =  1.0e-5 ;

if(n_elements(mxit) eq 0)then mxit  =  20
npo   =  4
npxn  =  3*npo+1

; ----- sn

;; assumes data max ~ 1

Smax=40
Smin=1
ftol = [1/float(Smax)^2,1/float(Smin)^2]
fsig = ftol
noisemod = 'poisson6'
seed = 0

; -----

@build_input_06


jname = sc.gname+'.jou'
journal,jname

;print,'****************************'
;get_lun,lun
;openr,lun,'run_cfluxrope.pro'
;line=''
;on_ioerror,IOE &$
;while(1) do begin & readf,lun,line & print,line & end &$
;IOE: close,lun
;print,'****************************'
;print,''

print,'**********************************'
print,'N      = ',N
print,'tau    = ',tau
print,'gamma  = ',gamma
print,'interp = ',interp
print,'phys   = ',phys
print,'**********************************'

; =====

; ----- get true image

help,/mem
print,'Calling get_timage'
get_timage,sr,sc,timage,stuff,stuff1
help,/mem

;print,minmax(timage)
;stop,'STOP after get_timage'

; ----- center the data

help,/mem
print,'Calling center_data'
center_data_06,sr
help,/mem
sr.k0=sr.k0+[1,1]*.0001

; ----- setup


help,/mem
print,'Calling stereo_rsetup'
stereo_rsetup,sr,sp,sc,oPixon,userstruct
help,/mem


fname=sc.gname+'_'+sc.tag               ; define generic file name

jname = fname+'.jou'                    ; rename journal file
spawn,'mv '+sc.gname+'.jou '+jname

;save,filename=fname+'t.dat',timage       ; save timage

;; ----- get the data

print,'*********** phys=',phys

timage=timage/1.0e6/0.2         ; norm to datamax ~ 1
help,/mem
data=stereo_project(sr,sc,timage,oPixon,userstruct)
help,/mem
stereo_noise,data,sn,sigma
help,/mem

; ----- run the Pixon solution

mdata=data

case starter of  &$
'0': image=timage*0+1/double(N) &$
'T': image=timage &$
else: stop,'ERROR2 in run_cfluxrope'  &$
end

help,/mem
stereo_pixon,sp,sc,oPixon,userstruct,image,sigma,mdata,so,journ
help,/mem

;save,filename=fname+'s.dat',image

outfname=sc.gname+'_'+sc.tag+'.dat'
save,filename=outfname,sr,sp,sn,sc,so,userstruct,sigma,$
timage,image,data,mdata,journ
help,/mem
print,'PIXON data saved to '+outfname


;print,'unmapping shared memory, if any'
;shmunmap,'map_name'
;dodat,data,mdata,mag=256/N,ch=1.04

