;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; This runs 'cfluxrope' using our new refactored code, accurately
; reproducing the earlier Paul Reiser 'cfluxrope_classic' results.
;

on_error,2
fin='cfluxrope_070615152144.dat'

restore,/verbose,fin
; data is the original data
; mdata is the solution
; image is the solution image
; timage is the original sim image
; sigma is the sigma array
; userstruct, sr, sp, sn, sc, so are the structures
;

Nd=2
N=32
prep_3drv,pxwrapper,views=Nd,N=N,gamma=[0.0,90.0]

simpletest=0

; simpletest just compares solutions using the most basic
; geometry, no other factors

if (simpletest) then p_generate_pixon,'classic',pxwrapper,oPixon
if (simpletest) then p_insert_data,pxwrapper,data*1000,oPixon;,sigma=sigma*1000
if (simpletest) then p_pixonloop,pxwrapper,oPixon,4,100,/init
if (simpletest) then sdata=stereo_project(pxwrapper,oPixon,/retro)/1000.0
if (simpletest) then sset=[[[sdata]],[[mdata]],[[data]]]
if (simpletest) then tv_multi,sset,fixed=128,/log,line=2
if (simpletest) then tv_multi,sset,fixed=128,line=2,/unique
if (simpletest) then stop

; part here is a more complex test, using the full structure definitions

pxwrapper.sr.chl=sr.chl
pxwrapper.sr.an=sr.an
pxwrapper.sr.polar=sr.polar
pxwrapper.sr.fan=sr.fan
pxwrapper.sr.LL=sr.ll
pxwrapper.sr.tau=replicate(sr.tau,2)
pxwrapper.sr.roco=sr.roco
pxwrapper.sr.lla0=sr.lla0
for i=0,1 do *(pxwrapper.sr.k0[i])=sr.k0[*,i]
for i=0,1 do *(pxwrapper.sr.sunk0[i])=*(pxwrapper.sr.k0[i])
pxwrapper.sr.z0=sr.z0
pxwrapper.sr.d=sr.d
pxwrapper.sr.l0=sr.l0
pxwrapper.sr.l0tau=replicate(sr.l0tau,2)
pxwrapper.sr.eps=sr.eps
pxwrapper.sr.set=sr.set
pxwrapper.sr.trw=sr.trw
pxwrapper.sr.trrrs=sr.trrrs
pxwrapper.sr.trp=sr.trp
pxwrapper.sr.trvs=sr.trvs
pxwrapper.sr.interp=sr.interp
pxwrapper.sr.psp=sr.psp
pxwrapper.sr.n0=sr.n0
pxwrapper.sr.istd=replicate(sr.istd,2)
pxwrapper.sr.as=sr.as

pxwrapper.sp.anneal_3d=sp.anneal_3d
pxwrapper.sp.reltol=sp.reltol
pxwrapper.sp.abstol=sp.abstol
pxwrapper.sp.mxit=sp.mxit
pxwrapper.sp.reltol=sp.reltol
pxwrapper.sp.jsched=sp.jsched
pxwrapper.sp.msched=sp.msched
pxwrapper.sp.rsched=sp.rsched
pxwrapper.sp.starter=sp.starter
pxwrapper.sp.npo=sp.npo
pxwrapper.sp.npxn=sp.npxn

pxwrapper.sn.noisemod=sn.noisemod
pxwrapper.sn.seed=sn.seed
pxwrapper.sn.fsig=sn.fsig
pxwrapper.sn.ftol=sn.ftol

pxwrapper.sr.SAT_NAME=sc.SAT_NAME

pxwrapper.sc.GNAME=sc.GNAME
pxwrapper.sc.TERM=sc.TERM
pxwrapper.sc.NOTE=sc.NOTE
pxwrapper.sc.T00=sc.T00
pxwrapper.sc.TOTALTIME=sc.TOTALTIME
pxwrapper.sc.TDSF=sc.TDSF
pxwrapper.sc.TDSFT=sc.TDSFT
pxwrapper.sc.VERSION=sc.VERSION
pxwrapper.sc.MACHINE=sc.MACHINE
pxwrapper.sc.DATE=sc.DATE
pxwrapper.sc.TAG=sc.TAG


;fimage=image*0.0+0.03125;  matches Paul rescaling

regen=0; 0 = do pure, 1 = steal Paul's object
pixon_env,'c'

if (regen) then restore,/verb,'paulOpixon.sav' else $
  p_generate_pixon,'classic',pxwrapper,oPixon

altfill=3; 0 = normal, 1 = debugging check, 2-3 = no starter image, 4 = none
; note altfill=3 does ->full not anneal
; now reinsert, including sigmas
if (altfill eq 0) then p_insert_data,pxwrapper,data,oPixon,sigma=sigma,$
  image=image*0+1/double(N)
if (altfill eq 1) then p_insert_data,pxwrapper,data,oPixon,sigma=sigma,$
  image=image*0+1/double(N),/retro
if (altfill eq 2) then p_insert_data,pxwrapper,data,oPixon,sigma=sigma
if (altfill eq 3) then p_insert_data,pxwrapper,data,oPixon,sigma=sigma
if (altfill eq 4) then stop,'testing'
; and run solver

;opixon->set,mxit=5
;opixon->pseudo
;stop

;p_pixonloop,pxwrapper,oPixon,/anneal,/init

if (altfill eq 2) then p_pixonloop,pxwrapper,oPixon,/anneal,/flush
if (altfill eq 3) then p_pixonloop,pxwrapper,oPixon,/full,/flush
if (altfill eq 0 or altfill eq 1) then p_pixonloop,pxwrapper,oPixon,/anneal

fsaved='recon_32_Jun15_2007_v01.txt'; just in case we need it again

; save just about everything, with descriptive names, so we
; can analyze later if needed.

paul_image=image
sandy_image=oPixon->Get(/im,/nop)
orig_data=data
sandy_render=stereo_project(pxwrapper,oPixon)
paul_render=mdata
paul_q=so.q
paul_pxncnt=so.pxncnt
sandy_q=pxwrapper.sc.q
sandy_pxncnt=pxwrapper.sc.pcount
sandy_mdata=stereo_project(pxwrapper,oPixon,/retro)
minus_render=sandy_render-paul_render

;dset=[[[minus_render]],[[sandy_render]],[[paul_render]],[[orig_data]]]
;tv_multi,dset,fixed=128,/log,line=2
dset=[[[sandy_render]],[[paul_render]],[[orig_data]]]
if (altfill eq 3) then title='Anneal vs ->Full' else title='Paul vs Sandy'
tv_multi,dset,fixed=128,/log,line=2,title=title
write_gif,'fitmatch.gif',tvrd()
tv_multi,dset,fixed=128,line=2,title=title
write_gif,'fitmatchl.gif',tvrd()

!P.MULTI=[0,1,2]
plot,sandy_render
plot,paul_render
!P.MULTI=0
write_gif,'fitmatchg.gif',tvrd()


save,file='cfluxrope_redux.sav',pxwrapper,oPixon,sandy_image,paul_image,$
  orig_data,paul_render,sandy_render,paul_q,paul_pxncnt,sandy_q,$
  sandy_pxncnt,sigma,timage,sandy_mdata

; rerun without annealing
extra=0;
if (extra) then p_pixonloop,pxwrapper,oPixon,4,100,/endsave,/init
