;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
PRO robintest

on_error,2

; get trio of polarized Cor2 A images
da='/net/earth/earth2/secchi/lz/cor2/L0/a/seq/20070321/'
fa='/home/antunes/20070321_232700_s4c2A.fts'
flist=match_polarity(fa)
for i=0,2 do flist[i]=da+flist[i]
print,flist

; Grab one of the polarized images, for comparison
onepol=sccreadfits(flist[0])

; Prep them into tB image
secchi_prep,flist,phdr,secchipreptb,/polariz_on,/smask
; remove the less-than-0 ones because they screw up the plotting
si=where (secchipreppb lt 0)
secchipreptb[si]=0

; Also Prep them into pB image
secchi_prep,flist,phdr,secchipreppb,/polariz_on,/smask,/pB

; Now get Nathan's tB image
ftb='/net/earth/secchi/P0/a/cor2/20070321/20070321_232700_0B4c2A.fts'
nathantb=sccreadfits(ftb,tbhdr); DN/s
; grab background, even though we do not use it, just as an example
bkimg=scc_getbkgimg(tbhdr,outhdr=outhdr)

; And plot them
!P.MULTI=[0,2,4]
plot,nathantb
plot,alog10(nathantb)
plot,secchipreptb
plot,alog10(secchipreptb)
plot,secchipreppb
plot,alog10(secchipreppb)
plot,onepol
plot,alog10(onepol)
!P.MULTI=0
write_gif,'poltest.gif',tvrd()
l=['Nathan tB','secchi_prep tB','secchi_prep pB','polar=0']
tv_multi,fixed=128,/log,/uni,line=1,labels=l,$
  [[[nathantb]],[[secchipreptb]],[[secchipreppb]],[[onepol]]]


END

pro really,checkpoint
  ; checkpoint 1 = prep files, 2 = ingest and save, 3 = run

  on_error,2


  if (checkpoint eq 1) then begin

      print,'Prepping'

      da='/net/earth/earth2/secchi/lz/cor2/L0/a/seq/20070619/'
      db='/net/earth/earth2/secchi/lz/cor2/L0/b/seq/20070619/'

      fa='20070619_225230_s4c2A.fts'
      fb='20070619_225230_s4c2B.fts'

      flista=match_polarity(fa)
      for i=0,2 do flista[i]=da+flista[i]

      flistb=match_polarity(fb)
      for i=0,2 do flistb[i]=db+flistb[i]

      secchi_prep,flista,hdrA,dataA,/polariz_on,/smask,/pB,/write_fts
      secchi_prep,flistb,hdrB,dataB,/polariz_on,/smask,/pB,/write_fts

      pfa='20070619_225230_1P4c2A.fts'
      pfb='20070619_225230_1P4c2B.fts'

      p_noisewrapper,4,pfa,/savefits,/forcemsb,outfile=outa,noise=noiseA
      p_noisewrapper,4,pfb,/savefits,/forcemsb,outfile=outb,noise=noiseB

   ; now have dataA/hdrA, noiseA, dataB/hdrB, noiseB (all full-frame 2048x2048)

      Ncube=32
      Nd=2
      prep_3drv,pxwrapper,views=Nd,N=Ncube,autorange=0.2

      save,file='checkpoint1.sav',dataA,dataB,hdrA,hdrB,noiseA,noiseB,$
        Ncube,Nd,pxwrapper

  end else if (checkpoint eq 2) then begin

      print,'Ingesting'

      pixon_env,'c'
      restore,'checkpoint1.sav',/verb

;  for iele=0,Nd-1 p_ingest_fits,cropfiles[iele],datum,srjr,masks,$
;    pxwrapper=pxwrapper,iele=iele,N=Ncube,/pointer
      fnull='hello'
      ignore=1.0
      p_ingest_fits,fnull,ignore,maskA,$
        indata=dataA,inhdr=hdrA,pxwrapper=pxwrapper,iele=0,N=Ncube
      p_ingest_fits,fnull,ignore,maskB,$
        indata=dataB,inhdr=hdrB,pxwrapper=pxwrapper,iele=1,N=Ncube
      dataA=rebin(dataA,Ncube,Ncube)*(n_elements(dataA[0,*])/float(Ncube))
      noiseA=rebin(noiseA,Ncube,Ncube)*(n_elements(noiseA[0,*])/float(Ncube))
      dataB=rebin(dataB,Ncube,Ncube)*(n_elements(dataB[0,*])/float(Ncube))
      noiseB=rebin(noiseB,Ncube,Ncube)*(n_elements(noiseB[0,*])/float(Ncube))

;  for iele=0,Nd-1 do noise[iele]=p_cropscale(noisefiles[i],$
;     center=crops[0:1,iele],cropfactor=crops[2:3,iele],N=Ncube,/pointer)

      data=[[[dataA]],[[dataB]]]
      sigma=[[[noiseA]],[[noiseB]]]
      masks=[[[maskA]],[[maskB]]]

      md=0.2/max(data)
      data=data*md
      sigma=sigma*md

      save,file='checkpoint2.sav',dataA,dataB,noiseA,noiseB,maskA,maskB,$
        data,sigma,masks,Nd,pxwrapper

  end else if (checkpoint eq 3) then begin

      print,'Fitting'

      pixon_env,'c'
      restore,'checkpoint2.sav',/verb

      p_generate_pixon,'classic',pxwrapper,oPixon

      print,max(data),max(sigma)

;  restore,'cfluxrope.dat',/verb; data,mdata,image,timage,sigma,userstruct,s*

      data=data*masks

      p_insert_data,pxwrapper,data,oPixon,sigma=sigma

      print,max(data),max(sigma)

      p_pixonloop,pxwrapper,oPixon,/anneal,/flush

;  p_insert_data,pxwrapper,data,oPixon,sigma=sigma,mask=masks,/autoscale

;  p_pixonloop,pxwrapper,oPixon,1,20,/full,/flush

     save,file='checkpoint3.sav',pxwrapper,oPixon,data,sigma,masks

 end else if (checkpoint eq 4) then begin

     pixon_env,'c'
     restore,'checkpoint3.sav',/verb

     odata=oPixon->Get(/data,/nop)
     sdata=stereo_project(pxwrapper,oPixon)
     l=['orig','orig','recon','recon']
     tv_multi,[[[odata]],[[sdata]]],labels=l,fixed=256,line=2,/log


 end

end
