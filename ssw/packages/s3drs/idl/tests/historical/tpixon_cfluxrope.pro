;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; flux rope
; BE SURE TO RUN newpixon_env

pro tpixon_cfluxrope

prep_model,8,2,pxwrapper,timage,model='fluxrope'

stereo_tsetup,pxwrapper

usenoise = 0; added to handle Paul scripts blocking of noise

tpxn_make,pxwrapper,timage,oTPixon

tpxn_run,pxwrapper,oTPixon

;;d1=*dm[0]
;;d2=*dm[1]
;;plot_pixondatum,[[[d1]],[[d2]]],rescale=16

end
