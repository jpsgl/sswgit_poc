; This is documentation and procedures for reproducing the
; Paul Reiser 'cfluxrope' test.  Notes on debugging and other
; steps are included here, while the non-procedure part that
; starts this file is the final working test case.
;
;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            



.reset
@cfluxrope_recontest

;restore,'stopgap.sav',/verb; kinda worked, 7992 vs 8430 pixons at ->Map
; BUT... by adding the following 'image=' line, we get a 100% match as
; long as we also restored the oPixon object in the previous line!!!
;image=image*0.0+0.03125; only do for my cfluxrope, not the Paul case
;p_insert_data,pxwrapper,data,oPixon,sigma=sigma,/retro,$
;  image=image*0+1/double(32)

;p_pixonloop,pxwrapper,oPixon,/anneal
;threeview,fixed=128,/log,opixon->get(/im,/nop),title='Paul'

; All the above matches this bit below

;;;.reset
;;;earlystop=1
;;;@cfluxrope_classic
;;;
;;; THEN EITHER THIS:
;;;stereo_pixon,sp,sc,oPixon,userstruct,image,sigma,mdata,so,journ
;;;threeview,fixed=128,/log,opixon->get(/im,/nop),title='Paul'
;;; OR THIS:
;;;pxwrapper={sr:sr,sc:sc,sp:sp,sn:sn,object:'c'}
;;;p_insert_data,pxwrapper,data,oPixon,sigma=sigma,/retro,$
;;;  image=image*0+1/double(32)
;;;p_pixonloop,pxwrapper,oPixon,/anneal

; HERE is what we want to match
;ANNEAL STEP   3 of   4 scale= 0 mxit= 100
;
;Pixon count      7429.92
;
;Pixon count      7497.48
;pxnidx          min-map     min-pxnidx
;       0      0.00000      0.00000
;
;Pixon count      7497.48
;
;Fit/DOF           0        4096
;
; Iter            LLF    Convergence              Q      Chi^2/DOF
;    0   5.807109e+03   0.000000e+00   1.890527e+01   1.417751e+00
;    1   4.392720e+03   1.414389e+03   3.278326e+00   1.072441e+00
;    2   3.922143e+03   4.705776e+02  -1.920871e+00   9.575543e-01
;DONE


PRO test_p_anneal

;;  .reset
;;  @cfluxrope_classic

;;  .reset
;;  @cfluxrope_recontest

; if I just run without using the Paul object in the Sandy run,
; I get Terrible-- 796 pixons vs 8430 pixons

; Oddly enough, restoring the Paul object at this point does not help(!)
;save,image,file='stopgap.sav'; Terrible-- 796 pixons vs 8430 pixons
;restore,'stopgap.sav',/verb; Terrible-- 796 pixons vs 8430 pixons

  jsched=[-1,0,0,0,0]
  msched=[50,100,100,100,100]
  rsched=[1.0E-05,1.0E-05,1.0E-05,1.0E-05,1.0E-05]
  npxn=13
  N=32
  Nd=2

;save,oPixon,file='stopgap.sav'; kinda worked, 7992 vs 8430 pixons at ->Map
restore,'stopgap.sav',/verb; kinda worked, 7992 vs 8430 pixons at ->Map
; BUT... by adding the following 'image=' line, we get a 100% match as
; long as we also restored the oPixon object in the previous line!!!
image=image*0.0+0.03125; only do for my cfluxrope, not the Paul case

idims = [N,N,N]
ddims = [N,N]
oData = oPixon->get(/oData)
oPixon->Setimage,image=image   ; insert starter image
oPixon->Set,sigma=sigma        ; insert sigma
for m=0,Nd-1 do oData[m]->Set, data=PTR_NEW(data[*,*,m])

;save,oPixon,file='stopgap.sav'; worked, identical, 100% perfect
;restore,'stopgap.sav',/verb; worked, identical, 100% perfect


scales = FINDGEN(npxn)
pxnidx = FLTARR(N,N,N)+npxn-1
  oPixon->Set, pxnidx=pxnidx
  oPixon->Set, mxit=msched[0]
  oPixon->Set, reltol=rsched[0]

;save,oPixon,file='stopgap.sav'; worked, identical, 100% perfect
;restore,'stopgap.sav',/verb; worked, identical, 100% perfect

  oPixon->pseudo
  annealsteps='LS'
  q=qval(oPixon)
  print,q
mxits   = msched[0]
reltols = rsched[0]
nj=n_elements(jsched)

;save,oPixon,file='stopgap.sav'; worked, identical, 100% perfect
;restore,'stopgap.sav'; worked, identical, 100% perfect

j=1
;j=2
;j=3
;j=4
  oPixon->Map

; TYPICALLY, if ->Map matches here, everything is identical afterwards.

  oPixon->PixonSmooth, /pxnidx
  pxnmap=oPixon->Get(/pxnidx, /nop)
  pxnidx=pxnmap > scales[jsched[j]]
  oPixon->Set, pxnidx = pxnidx
  oPixon->Set, mxit   = msched[j]
  oPixon->Set, reltol = rsched[j]
  oPixon->pseudo
  q=qval(oPixon)
  print,q,j

;j=1
j=2
;j=3
;j=4
  oPixon->Map
  oPixon->PixonSmooth, /pxnidx
  pxnmap=oPixon->Get(/pxnidx, /nop)
  pxnidx=pxnmap > scales[jsched[j]]
  oPixon->Set, pxnidx = pxnidx
  oPixon->Set, mxit   = msched[j]
  oPixon->Set, reltol = rsched[j]
  oPixon->pseudo
  q=qval(oPixon)
  print,q,j

;j=1
;j=2
j=3
;j=4
  oPixon->Map
  oPixon->PixonSmooth, /pxnidx
  pxnmap=oPixon->Get(/pxnidx, /nop)
  pxnidx=pxnmap > scales[jsched[j]]
  oPixon->Set, pxnidx = pxnidx
  oPixon->Set, mxit   = msched[j]
  oPixon->Set, reltol = rsched[j]
  oPixon->pseudo
  q=qval(oPixon)
  print,q,j

;j=1
;j=2
;j=3
j=4
  oPixon->Map
  oPixon->PixonSmooth, /pxnidx
  pxnmap=oPixon->Get(/pxnidx, /nop)
  pxnidx=pxnmap > scales[jsched[j]]
  oPixon->Set, pxnidx = pxnidx
  oPixon->Set, mxit   = msched[j]
  oPixon->Set, reltol = rsched[j]
  oPixon->pseudo
  q=qval(oPixon)
  print,q,j

threeview,oPixon->get(/im,/nop),/log,fixed=128


END

PRO p_anneal_test_plot

;  restore,/verb,'sandy_sol.sav'
  sandy_sol=oPixon->get(/im,/nop)
  restore,/verb,'paul_sol.sav'
  minus_sol=sandy_sol-paul_sol

  sandy_set=fltarr(32,32,3)
  paul_set=sandy_set
  minus_set=sandy_set
  orig_set=sandy_set

  sandy_set[*,*,0]=ijkproj(sandy_sol,'ij')
  sandy_set[*,*,1]=ijkproj(sandy_sol,'ik')
  sandy_set[*,*,2]=ijkproj(sandy_sol,'jk')
  paul_set[*,*,0]=ijkproj(paul_sol,'ij')
  paul_set[*,*,1]=ijkproj(paul_sol,'ik')
  paul_set[*,*,2]=ijkproj(paul_sol,'jk')
  minus_set[*,*,0]=ijkproj(minus_sol,'ij')
  minus_set[*,*,1]=ijkproj(minus_sol,'ik')
  minus_set[*,*,2]=ijkproj(minus_sol,'jk')
  orig_set[*,*,0]=ijkproj(timage,'ij')
  orig_set[*,*,1]=ijkproj(timage,'ik')
  orig_set[*,*,2]=ijkproj(timage,'jk')


  dset=[[[minus_set]],[[sandy_set]],[[paul_set]],[[orig_set]]]
  labels=['Sandy-Paul','Sandy-Paul','Sandy-Paul','Sandy','Sandy','Sandy', $
          'Paul','Paul','Paul','Orig','Orig','Orig']
  tv_multi,dset,labels=labels,/log,line=3,fixed=128,ct=3,$
    title='Comparison of cfluxrope sim: orig Paul run vs Sandy refactored code'

END
