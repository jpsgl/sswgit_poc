;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
PRO realcme_scalar,N=N,run=run,cap=cap

  if (n_elements(N) eq 0) then N=128; default size
  run=setyesno(run)
  cap=setyesno(cap)
  
; Takes existing tB pairs (pre-and-CME, A&B) to make
; difference files with a flat cutoff of signal<0
; and runs a recon
  
; quick full one with flat flooring
  QA='../CME0708/tb_fits/20070708_182230_1B4c2A.fts'
  QB='../CME0708/tb_fits/20070708_182230_1B4c2B.fts'
  A2='../CME0708/tb_fits/20070708_222230_1B4c2A.fts'
  B2='../CME0708/tb_fits/20070708_222230_1B4c2B.fts'
  
  qad=sccreadfits(QA)
  qbd=sccreadfits(QB)
  a2d=sccreadfits(A2,hdra)
  b2d=sccreadfits(B2,hdrb)
  
  d1=a2d-qad
  d2=b2d-qbd
  
;p_noisewrapper,4,A2,noise=noise1,Nforce=N,data10=d1
;p_noisewrapper,4,B2,noise=noise2,Nforce=N,data10=d2
;p_noisewrapper,4,A2,noise=noise1,Nforce=N,data10=a2d
;p_noisewrapper,4,B2,noise=noise2,Nforce=N,data10=b2d
  
  d1r=rebin(d1,N,N)
  d2r=rebin(d2,N,N)
  orig=[[[rebin(a2d,N,N)]],[[rebin(b2d,N,N)]]]
  ie=where (orig lt 0)
  if (ie[0] ne -1) then orig[ie]=0.0
;d1sub=((d1 > (-2e-11) ) < 5e-11)
;d2sub=((d2 > (-2e-11) ) < 5e-11)
;datum=[[[d2sub]],[[d1sub]]]
;datum=rebin(datum,N,N,2)
  
;;sigma=rebin([[[noise1]],[[noise2]]],N,N,2)
  
  prep_3drv,pxwrapper,views=2,N=N
  
  if (cap) then capstr='_capped' else capstr=''
  pxwrapper.sc.gname='realcme_scalar' + capstr

  pxwrapper.sr.normd=5.7e+13
  
  quicknoiseA = sqrt(a2d*pxwrapper.sr.normd)/pxwrapper.sr.normd
  quicknoiseB = sqrt(b2d*pxwrapper.sr.normd)/pxwrapper.sr.normd
  sigma=rebin([[[quicknoiseA]],[[quicknoiseB]]],N,N,2)
  
  datum=[[[d1r]],[[d2r]]]
  
  p_ingest_fits,A2,dA,maskA,srjrA,/verbose,N=N,pxwrapper=pxwrapper,iele=0,$
    /rawcor2B
  p_ingest_fits,B2,dB,maskB,srjrB,/verbose,N=N,pxwrapper=pxwrapper,iele=1,$
    /rawcor2B
  
  maskA=p_getmask(hdr=hdra,ratio=2.0)
  maskB=p_getmask(hdr=hdrB,ratio=2.0)
  masks=rebin([[[maskA]],[[maskB]]],N,N,2)
  invmasks=abs(masks-1)
  
  p_generate_pixon,'classic',pxwrapper,oPixon
  
; make it flat
;sigmaN=((sigma*pxwrapper.sr.normd) > 0)
;datumN=((datum*pxwrapper.sr.normd) > 0)
  
;sigmamask=invmasks * max(sigmaN)
;sigmaN2=sigmaN+sigmamask
  
  
  sigma=sqrt(orig)
  datummin=abs(min(datum))
  
;  sigmaN=(( (sigma) *pxwrapper.sr.normd) > 0)
;  datumN=(( (datum+datummin) *pxwrapper.sr.normd) > 0)
  sigmaN= sigma * pxwrapper.sr.normd
  datumN= (datum+datummin) *pxwrapper.sr.normd

; cap sigma...
  sigmaN2=sigmaN
  for i=0,N-1 do for j=0,N-1 do for k=0,1 do $
    if (sigmaN2[i,j,k] gt datumN[i,j,k]) then $
    sigmaN2[i,j,k]=max([datumN[i,j,k]-(datummin*pxwrapper.sr.normd),datummin])

  if (cap eq 0) then sigmaN2=sigmaN; ignore the capping if asked
  
  p_insert_data,pxwrapper,datumN,oPixon,sigma=sigmaN2,/nonorm,masks=masks
  
  print,minmax(opixon->get(/data,/nop)),minmax(opixon->get(/sigma,/nop))
  
  fname=trim(string(N),2)+'realcme_scalar_in.sav'
  save,datumN,sigmaN2,pxwrapper,oPixon,file=fname
  print,'saved data in ',fname
  
  if (run) then p_pixonloop,pxwrapper,oPixon,/best,/init

  if (run) then p_standardplots,pxwrapper,oPixon,/no
  
END
