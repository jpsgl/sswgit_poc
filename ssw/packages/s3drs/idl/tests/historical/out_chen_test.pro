; This routine loads in an earlier Paul run and then reruns it
; using our current Pixon.  It also allows different settings
;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : (various test routines)
;               
; Purpose   : part of the s3drs test kit
;               
; Explanation: (archived here for completeness, not intended for users use)
;               
; Use       : (self-documented, see notes in code)
;    
; Written     : Sandy Antunes, NRC, 2005-2009
;               
;-            
; to explore the headspace, and includes a basic renderer test too.
;
; To simply compare our rendering with Paul's (to verify geometry):
;     out_chen_test
;
; To test that we can rerun Paul's case with convergence, first try:
;     out_chen_test,irecon=1
; Note this currently _fails_!  So instead guess that his 'f0' is
; the desired scaling factor (f0=6.8754723e+12)
;     out_chen_test,irecon=1,headspace=6.8754723e+12
; From this we learn that we can only rescale the data by f0, not n0,
; because n0 is much greater than the max(data).
;
; To rerun fully to reproduce Paul's fit:
;     out_chen_test,irecon=1,headspace=6.8754723e+12,mxit=2500,/anneal
;     'recon_32_Jun6_2007_v28.dat'
;     out_chen_test,irecon=1,headspace=6.8754723e+12,mxit=2500
;     'recon_32_Jun6_2007_v26.dat'

; In general, to test the effect of different headspaces/data ranges:
;     out_chen_test,irecon=1,headspace=TESTVALUE
; For example,
;     out_chen_test,irecon=1,mxit=40,headspace=1e-2,forcen0=1e-4
;     'recon_32_Jun6_2007_v45.dat' 'recon_32_Jun6_2007_v46_oom.gif'
;     out_chen_test,irecon=1,mxit=40,headspace=1,forcen0=1e-4
;     'recon_32_Jun6_2007_v47.dat' 'recon_32_Jun6_2007_v48_oom.gif'
;     out_chen_test,irecon=1,mxit=40,headspace=1e1,forcen0=1e-4
;     'recon_32_Jun6_2007_v43.dat' 'recon_32_Jun6_2007_v44_oom.gif'
;     out_chen_test,irecon=1,mxit=40,headspace=1e3,forcen0=1e-4
;     'recon_32_Jun6_2007_v34.dat' 'recon_32_Jun6_2007_v39_oom.gif'
;     out_chen_test,irecon=1,mxit=40,headspace=1e5,forcen0=1e-4
;     'recon_32_Jun6_2007_v37.dat' 'recon_32_Jun6_2007_v40_oom.gif'
;     out_chen_test,irecon=1,mxit=40,headspace=1e8,forcen0=1e-4
;     'recon_32_Jun6_2007_v35.dat' 'recon_32_Jun6_2007_v41_oom.gif'
;     out_chen_test,irecon=1,mxit=40,headspace=1e15,forcen0=1e-4
;     'recon_32_Jun6_2007_v36.dat' 'recon_32_Jun6_2007_v42_oom.gif'
;(tried 1e20, got Inf,-NaN)
;
;  'scaling  '  max(data)  n0    Pxncnt   Q        Q/scaling
;
;    1e-2        2.5e-3   1e-6     64    9.5e7     9.5e9
;    1           0.25     1e-4     23    2.0e4     2.0e4
;    1e1         2.5      1e-3   7968    7.1e3     7.1e2
;    1e3         2.5e2    0.1    2152    1.9e4     1.9e1
;    1e5         2.5e4    1e2    1800    1.7e4     0.17
;    1e8         2.5e7    1e5   16601    4.0e7     0.4
;    1e15        2.5e14   1e11  17171    5.9e21    5.9e6

; Arguments:
;   irecon (def 0); 0 = just compare renders, 1 = do reconstruction
;   mxit (def 20); Paul sim used mxit=2500
;   headspace (def 1); scaling to get Pixon to converge
;   forcen0 (def -1); if >0, sets n0 to other than Paul's default


; Notes on 'headspace':
;    Pixon requires max(data)>>n0 and also max(data)>1
;    one or both of these seems to determine the 'headspace', or
;    which data elements actually contribute to the solution.
;  'get_Istd_Nd' also may factor in.
; So I'm still not sure what Paul's sim normalizations are, but
; for real data, it should be: for data in MSB, n0 is the conversion
; factor to photons, e.g. Cor 2 is typically Bsun/DN=6.5e-11 and
; DN/photons=15 so n0=6.5e-11 * 15 =~ 1e-9.  The real data ends
; up being around <= e-6.  So we need to rescale, for example
; by 1e+10 so  data <= e+4 and n0=1e1. 
;

PRO out_chen_test,irecon=irecon,mxit=mxit,headspace=headspace,$
                  anneal=anneal,forcen0=forcen0

  if (n_elements(irecon) eq 0) then irecon=0
  if (n_elements(mxit) eq 0) then mxit=20
  if (n_elements(headspace) eq 0) then headspace=1
  if (n_elements(forcen0) eq 0) then forcen0=-1
  anneal=setyesno(anneal)

  fname='out_chen__032_04a_04.datx'

  restore,fname,/verbose
; restores DATA, XDATA, IMAGE, RES, MDATA, XMDATA, SIGMA, PXNCNT, Q,
;   TOTALTIME, TDSF, TDSFT, MACHINE, VERSION, STEREO_IN, USERSTRUCT,
;   F0, I0, ANNEALSTEPS, NPXN, JOURN
; examination of stereo_in
;   data = 6 data input files
;   image = solution
;   mdata = answer
;   totaltime = 1081
;   version = stereo_test_04a
;   f0=6.8754723e+12, i0=8.1318449e+17
;   nxpn=13
;   fsig=0.0, ftol=1e-05, noisemod='poisson3', reltol=abstol=ftol
;   mxit=2500
;   chl=2, ch='chen'
;   polar[3]='yes' 'yes' 'yes'
;   rro=1e30, tau=0.0, d=0.95, L0=214.94299, L0tau=0.62500, N0=1e8
;   ll[3]=1.0,1.0,1.0
;   gamma[3]=90,180,180  z0[3]=0,0,0  roco=0,0,1
;   lla0[3]=15.5,8.35625,15.5
;   k0[2,3]=15.5,15.5,  22.286562, 15.5,  22.286562,15.5
;

;;tags=tag_names(stereo_in)
;;for i=0,n_elements(tags)-1 do if (tags[i] ne 'IMAGE') then $
;;  print,tags[i]+' = ',stereo_in.(i)

; p_ingest_fits in the end requires rho,phi,LL,z0
; polar='t' and 'r'

  prep_3drv,pxwrapper,views=6,N=32,gamma=[90,90,180,180,180,180]

  pxwrapper.sr.gamma=[90,90,180,180,180,180]
  pxwrapper.sr.phi=pxwrapper.sr.gamma
  pxwrapper.sr.lla0=[15.5,8.35625,15.5]
  pxwrapper.sr.z0=[0,0,0,0,0,0]
  pxwrapper.sr.polar=['t','r','t','r','t','r']

  *(pxwrapper.sr.k0[0])=[15.5,15.5]
  *(pxwrapper.sr.k0[1])=[15.5,15.5]
  *(pxwrapper.sr.k0[2])=[22.286562,15.5]
  *(pxwrapper.sr.k0[3])=[22.286562,15.5]
  *(pxwrapper.sr.k0[4])=[22.286562,15.5]
  *(pxwrapper.sr.k0[5])=[22.286562,15.5]
  for i=0,5 do *(pxwrapper.sr.sunk0[i])=*(pxwrapper.sr.k0[i])

  pxwrapper.sr.theta=pxwrapper.sr.phi
  pxwrapper.sr.rho=pxwrapper.sr.gamma
  pxwrapper.sr.d=0.9500
  pxwrapper.sr.L0tau=[0.625,0.625,0.625,0.625,0.625,0.625]
  pxwrapper.sr.tau=[0.0,0.0,0.0,0.0,0.0,0.0]
  pxwrapper.sr.sat_name=['1','1','2','2','3','3']
  pxwrapper.sr.tele_name=['','','','','','']
  pxwrapper.sr.detector=['','','','','','']
  pxwrapper.sr.n0=1e+08
  pxwrapper.sr.roco=[0,0,0,0,1,1]
  pxwrapper.sr.ch='chen'
  pxwrapper.sn.seed=-1
  pxwrapper.sn.fsig=[0.0,1.0]
  pxwrapper.sn.ftol=[1.0e-05,1.0]
  pxwrapper.sp.anneal_3d=0.10
  pxwrapper.sn.noisemod='poisson3'
  pxwrapper.sc.date='Fri Aug 30 15:04:36 2002'
  pxwrapper.sc.note='Jim Chen Flux Rope * polarized  1 step reltol=0.00001 '
  pxwrapper.sc.note=pxwrapper.sc.note+$
    'noisemod=poisson3 mxit=2500 infinite observer xyz view'
  pxwrapper.sc.t00='1.0307343e+09'
;pxwrapper.sp.npxn=13; forcing this may be bad... pxwrapper default is 9

;pxwrapper.sr.interp=3  ; setting this to 3, not 5, breaks our installation???

  pxwrapper.sp.mxit=mxit

  pxwrapper.sc.gname='comparison test'

; reproduce solution

  threeview,image,res=4,/log,title='Paul solution image'
  tv_multi,res=4,mdata,/log,title='Paul solution render'

; reset limits
  print,'Orig max,min and n0: ',max(data),min(data),pxwrapper.sr.n0
  if (forcen0 gt 0) then pxwrapper.sr.n0=forcen0
  if (forcen0 gt 0) then print,'*** Forcing n0 to be: ',pxwrapper.sr.n0
  if (max(data) gt pxwrapper.sr.n0) then $
    pxwrapper.sr.n0=headspace*pxwrapper.sr.n0 else if (headspace ne 1) then $
    print,'*** Warning! Not scaling n0 to avoid it being higher than max(data)'
  data=headspace*data
  print,'New max,min and n0: ',max(data),min(data),pxwrapper.sr.n0

; difference is whether to load starter image or not
  if (irecon) then timage=fake_ne_cube(pxwrapper.sr.Ncube) else timage=image

;  pxwrapper.sr.n0=1.0

  p_generate_pixon,'classic',pxwrapper,oPixon,image=timage

  print,'max of Paul image: ',max(image)
  print,'max of oPixon stored image: ',max(oPixon->get(/im,/nop))

  if (irecon) then p_insert_data,pxwrapper,data,oPixon
  if (irecon) then p_pixonloop,pxwrapper,oPixon,mxit/4,4,/full,/plot,$
    anneal=anneal

  mydata=stereo_project(pxwrapper,oPixon)

  if (irecon) then title='Sandy recon' else title='Sandy rendertest'
  tv_multi,res=4,mydata,/log,title=title

  if (irecon) then myimage=oPixon->Get(/im,/nop)
  if (irecon) then threeview,myimage,res=4,/log,title='Sandy solution image'

  print,'Data, Mdata, Xdata, Mydata and Mydata/headspace maxes:'
  print,max(data),max(mdata),max(xdata),max(mydata),max(mydata)/headspace
  
  N3=pxwrapper.sr.Ncube^3
  if (irecon) then print,'Paul PXNCNT:  ',pxncnt
  if (irecon) then print,'Paul Q, Q/N^3: ',q,q/N3
  if (irecon) then print,'Sandy PXNCNT: ',pxwrapper.sc.pcount
  if (irecon) then print,'Sandy Q, Q/N^3: ',pxwrapper.sc.q,pxwrapper.sc.q/N3

  p_report,pxwrapper,oPixon,/save


END
