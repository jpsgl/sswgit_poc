;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : update_struct_arrays
;               
; Purpose   : general utility
;               
; Explanation: to add to or update arrays within an existing structure.
;               
; Use       : update_struct_arrays,sr,srjr,iele
;    
; Inputs    : sr, srjr, iele
;               
; Outputs   : sr (modified)
;
; Keywords  : none
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : utils, i/o
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; Routine to add to or update arrays within an existing structure.
; Inserts a single set of values into an struct that has the
;  appropriate number of elements or, if 'iele' is higher than
;  the existing number of elements, adds the appropriate item
;  to the arrayed item at the desired index (inserting duplicate
;  items as needed until the structure's array is the proper size
;  so that the indices match up).
; It does not handle inserting an array, only inserting a single
;  value into an existing array (or single value) item.  To handle
;  arrays of arrays, please use pointers for your initial structure instead.
; Note that it _will_ update a single (non-array) value without
;  trouble, if the item in the structure is not yet an array and
;  you are inserting into 'iele' 0
; Routine does not update/add elements that do not already existing
;  in the target structure, but instead simply ignores them.

PRO update_struct_arrays,sr,srjr,iele

  tags=tag_names(srjr)
  for i=0,n_elements(tags)-1 do begin
      tname=tags[i]
      origloc=get_tag_index(sr,tname)
      newloc=get_tag_index(srjr,tname)

;      print,'doing ',tname
      if (tag_exist(sr,tname)) then begin
          inum=n_elements(sr.(origloc))-1

          if (iele gt inum) then begin
              ; add new item
              tdata=sr.(origloc)
              for j=inum+1,iele do begin
;                  print,'  adding ',srjr.(newloc),' as ',j,' of ',n_elements(sr.(origloc)),' ',tname
                  tdata=[tdata,srjr.(newloc)]
              end
              sr = addrep_struct(sr,tname,tdata)
          end else begin
              ; insert the item into the existing space
;              print,'  replacing ',srjr.(newloc),' as ',iele,' of ',inum,' ',tname
              sr.(origloc)[iele]=srjr.(newloc)
          end
      end
  end

END

