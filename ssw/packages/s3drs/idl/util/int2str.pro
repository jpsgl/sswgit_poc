;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : int2str
;               
; Purpose   : general utility
;               
; Explanation: simple reformatter that converts an integer to a
; string, allowing you to specify a forced size using leading zeros
; if necessary.  Also can accept floats, casts them to integers.
;               
; Use       : print,int2str(myinteger)
;    
; Inputs    : input = an integer
;             forcesize = (optional) a string size to output
;               
; Outputs   : a string
;
; Keywords  : 
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : utils, i/o
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
FUNCTION int2str,input,forcesize

  str=strtrim(string(floor(input)),2)

  ; pad with leading zeroes, if asked
  if (n_elements(forcesize) ne 0) then $
    for item=0,n_elements(str)-1 do $
    for ilen=strlen(str[item]),forcesize-1 do str[item]='0'+str[item]

  return,str

END
