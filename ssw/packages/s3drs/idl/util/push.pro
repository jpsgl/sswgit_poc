;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : push
;               
; Purpose   : general utility
;               
; Explanation: adds an element onto the end of stack of NDIM 1, 2 or
; 3, creating the stack if it does not yet exist.
; If given /front, puts it before the other elements instead
;               
; Use       : push,stack,item
;    
; Inputs    : (optional) stack
;             (required) item to put on it
;               
; Outputs   : stack
;
; Keywords  : /front = puts the new item at the front, not end, of array
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : utils, i/o
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; adds an element onto the end of stack of NDIM 1, 2 or 3, creating the stack
; if it does not yet exist.
; If given /front, puts it before the other elements instead
;

PRO test_push

     push,dates,'Mon'
     print,dates
     push,dates,'Tues'
     print,dates
     push,dates,'Sun',/front
     print,dates

     im1=fltarr(6,6)+1.0
     im2=im1+2
     im3=im2+2
     print,'maxes 1,2,3 are: ',max(im1),max(im2),max(im3)
     push,imstack,im1
     help,imstack
     print,max([imstack[*,*,0]])
     push,imstack,im2
     help,imstack
     print,max([imstack[*,*,0]])
     push,imstack,im3,/front
     help,imstack
     print,max([imstack[*,*,0]])

END

PRO push,stack,item,front=front

   front=KEYWORD_SET(front)

   if (n_elements(stack) eq 0) then begin
       ; just create it
       stack=item
   end else begin
       nn=size(item,/n_dimensions)+1
       case nn of
           1: if (front) then stack=[item,stack] else stack=[stack,item]
           2: if (front) then stack=[[item],[stack]] else stack=[[stack],[item]]
           3: if (front) then stack=[[[item]],[[stack]]] else stack=[[[stack]],[[item]]]
       end

   end

END
