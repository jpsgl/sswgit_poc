;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : addrep_struct
;               
; Purpose   : general utility
;               
; Explanation:  wrapper for the
; http://lasco-www.nrl.navy.mil/cds_pros/help_struct.html
; routines that does a 'safe' add/replace to a structure
;               
; Use       : addrep_struct, str, tag, data
;    
; Inputs    : str, tag, data
;               
; Outputs   : (modifies the input structure by inserting the data at tag)
;
; Keywords  : 
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : utils, i/o
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; wrapper for the
; http://lasco-www.nrl.navy.mil/cds_pros/help_struct.html
; routines that does a 'safe' add/replace to a structure
; (adds if not exists, replaces if does exist)
; Works on anonymous structures only-- named structures
; become anonymous if 'add' is done, but retain their name
; if 'upon return.

FUNCTION addrep_struct,str,tag,data

  strname = tag_names(str,/structure_name)
  if (tag_exist(str,tag)) then begin
      iloc=get_tag_index(str,tag)
      if (strname ne '' and n_elements(str.(iloc)) ne n_elements(data)) then begin
          ; same as new tag, changes struct to anonymous
          str2=create_struct(str,name='')
          str2=rem_tag2(str2,tag)
          str2=add_tag2(str2,data,tag)
          print,'warning, named structure ',strname,' is now anonymous'
          return,str2
      end else begin
          str.(iloc)=data
          return,str
      end
  end else begin
    ; new tag, now add it
      str=add_tag2(str,data,tag)
      if (strname ne '') then begin
      ; it was a named structure, so warn
          print,'warning, named structure ',strname,' is now anonymous'
      end
      return,str
  end 

END

;.com '../util/addrep_struct'

pro test_addrep_struct
  str={test3,items:1,name:'vole'}
  v={name:'go'}
  i={items: 3}

  print,'initial'
  help,str
  help,str,/str

  pause

  print,'addrep'
  str=addrep_struct(str,'items',2)
  help,str
  help,str,/str

  pause

  print,'changing'
  update_struct_arrays,str,i,0
  help,str
  help,str,/str

  pause

  print,'adding'
  update_struct_arrays,str,i,1
  help,str
  help,str,/str


end
