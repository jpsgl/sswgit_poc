;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : get_roto_angle
;               
; Purpose   : general utility
;               
; Explanation: given a starting data and a subsequent LASCO-like FITS file,
; returns the equivalent rotational tomography angle based purely
; on the date, in degrees.
;               
; Use       : angle=get_roto_angle(startdate, enddate, fitsfname=fitsname)
;    
; Inputs    : startdate, enddate
;             (optional) fitsfname
;               
; Outputs   : angle
;
; Keywords  : 
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : utils, i/o, lasco, tomography
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; given a starting data and a subsequent LASCO-like FITS file,
; returns the equivalent rotational tomography angle based purely
; on the date, in degrees.
;
; Assumes a LASCO-like FITS files, uses the first file date given
; as a reference point to artificially rotated a tomographic image,
; where the rotation between images is given as a single 'theta' angle
; based on the solar rotation over the time difference of the files.
;
; Rotational angle is implemented as the time difference times the
; solar equatorial sidereal rotation (25.38 deg/day) minus the Earth's
; sidereal rotation during that period (0.9856 deg/day), for a net
; 'theta' shift of 13.1988 deg/day.
;
; The date is obtained from DATE_OBS.
;
; Issues ignored include, in order of likely least to likely largest:
; 1) SOHO drift around L1, presumed small
; 2) Difference in the location of the sun in the image centers,
;    given as (CRPIX1+CRVAL1 and CRPIX2+CRVAL2), typically under 1 pixel
; 3) Size in arcsec of the sun in the image (RSUN), also under 1 pixel
; 4) Difference between Earth's ecliptic versus the solar rotation axis
; 5) Differential solar rotation rates
; 6) Evolution of features over the times (days to weeks) given
;
; Within our pixon structures, we can use it as:
; drho = get_roto_angle(sr.date_obs[0],sr.date_obs[1])
;

FUNCTION get_roto_angle, startdate, enddate, fitsfname=fitsfname

   shiftrate = 13.1988             ; in deg/day

   if (n_elements(fitsfname) ne 0) then begin
       enddate=get_fits_time(head2stc(headfits(fitsfname))) ; one method
;   filedate=fxpar(headfits(fitsfname),'DATE'); also works
   end else if (n_elements(enddate) eq 0) then begin
       print,'warning, no date or file provided for comparison, exiting.'
       enddate=startdate
   end

;   if (stregex(startdate,' ',/boolean)) then begin
;       ; date format akin to 'systime'
;       juldate,bin_date(startdate),startjd; gives reduced julian date
;       startjd=startjd+2400000
;   end else if (datatype(startdate) eq 'STR') then begin
       ; common FITS 'date_obs' form
       jd=anytim2jd(startdate)
       startjd = jd.int+jd.frac
;   end

;   if (stregex(enddate,' ',/boolean)) then begin
;       ; date format akin to 'systime'
;       juldate,bin_date(enddate),endjd; gives reduced julian date
;       endjd=endjd+24000000
;   end else if (datatype(enddate) eq 'STR') then begin
       ; common FITS 'date_obs' form
       jd=anytim2jd(enddate)
       endjd = jd.int+jd.frac
;   end
   ; no 'else', else presumes they are already floating dates of some form
;   print,startdate,' ,',enddate,' , ',startjd,' , ',endjd
   theta = (endjd-startjd) * shiftrate;

   ; get theta within the range of 0.0-360.0
   modtheta = long(theta)/360
   theta = theta - (modtheta*360.0)

   return,theta

END
