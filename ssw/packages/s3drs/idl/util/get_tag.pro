;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : get_tag
;               
; Purpose   : general utility
;               
; Explanation: returns a string  of form yy/mm/dd/hr/min/sec
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : returns the tag
;
; Keywords  : time
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : utils, i/o
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

function get_tag,dummy
; returns a string  of form yy/mm/dd/hr/min/sec
; useful for time-stamping

caldat,systime(/julian),mon,day,yr,hr,min,sec
fm = "(i2.2)"
tag  =  string(yr-2000,fm) + string(mon,fm) + string(day,fm)       +$
         string(hr,fm)      + string(min,fm) + string(round(sec),fm)
return,tag
end

