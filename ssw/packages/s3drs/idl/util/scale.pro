;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : scale
;               
; Purpose   : general utility
;               
; Explanation:  forms an array beginning within dx about x0, ending 
; within dx about xx, containing xf exactly, with increments of dx
;               
; Use       : myarray=scale(hx0,hxx,hdx,xf=hxf,npts=npts)
;    
; Inputs    : hx0,hxx,hdx,xf,npts
;               
; Outputs   : returns array
;
; Keywords  : 
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : utils, i/o
;               
; Prev. Hist. : None.
;
; Written     : unknown
;               
;-            
function scale,hx0,hxx,hdx,xf=hxf,npts=npts

; forms an array beginning within dx about x0, ending 
; within dx about xx, containing xf exactly, with increments of dx
;
; in general, all these conditions cant be simultaneously satisfied.
; when keywords (xf and npts) are used and they introduce ambiguity,
; the keywords will be satisfied at the expense of x0 and xx.
; 
; in the following table, "*" means defined, "0" means undefined
;
; ch  dx  xf  npts 		xf	xlow	xhigh	dx
;
; 0   0   0   0     error
; 1   0   0   *             x0      x0      xx      (xx-x0)/(npts-1)
; 2   0   *   0     error
; 3   0   *   *             xf      (NE)    (NE)
; 4   *   0   0             x0      x0      (NE)    dx
; 5   *   0   *     error
; 6   *   *   0             xf      (NE)    (NE)    dx
; 7   *   *   *     error

;--------------------------------------------------------------------------

ch=(n_elements(npts) ne 0)+2*(n_elements(hxf) ne 0)+4*(n_elements(hdx) ne 0)
err=-1

x0 = double(hx0)
xx = double(hxx)

case ch of

0: begin	; (none)
   err=0
   end

1: begin	; npts

   dx = (xx-x0)/(npts-1)
   x=dindgen(npts)*dx+x0

   end

2: begin	; xf
   err=2
   end

3: begin	; xf, npts

   xf=double(hxf)
   dx = (xx-x0)/(npts-1)
   x=dindgen(npts)*dx+x0
   d=abs(x-xf) 
   q=(where(d eq min(d)))(0) 
   x=x+(xf-x(q))
 
   end

4: begin	; dx 
  
   dx=double(hdx)
   xf=x0

   if((xx-x0)*dx lt 0)then begin
     h  = xx
     xx = x0
     x0 = h
   end

   np = round((xx-xf)/dx)+1
   if(np lt 0)then err=4.0
   nm = round((xf-x0)/dx)+1
   if(nm lt 0)then err=4.1
   nx = abs(np+nm)-1
   x = findgen(nx)*dx + xf - (nm-1)*dx

   end

5: begin	; dx, npts 
   err=5
   end
  
6: begin	; dx, xf

   dx=double(hdx)
   xf=double(hxf)

   if((xx-x0)*dx lt 0)then begin
     h  = xx
     xx = x0
     x0 = h
   end

   np = round((xx-xf)/dx)+1
   if(np lt 0)then err=6.0
   nm = round((xf-x0)/dx)+1
   if(nm lt 0)then err=6.1
   nx = abs(np+nm)-1
   x = findgen(nx)*dx + xf - nm*dx

   end

7: begin	; dx, xf, npts
   err=7
   end

else: stop,'scale GACK'
end 

if(err ne -1)then begin
  print,'scale.pro ERROR ',err
  stop
end else begin
  return,x
end

end
