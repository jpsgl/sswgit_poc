;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : is_idlsave
;               
; Purpose   : general utility
;               
; Explanation: checks if given file is an idl savefile or not, and
; as a bonus returns the number of data items in the savefile.
; Obviously, returning '0' means no data/not an IDL save file
;               
; Use       : ami=is_idlsave(myfile)
;    
; Inputs    : filename
;               
; Outputs   : returns 0 (is not) or 1 (is an IDL save file)
;
; Keywords  : none
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : utils, i/o
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; checks if given file is an idl savefile or not, and
; as a bonus returns the number of data items in the savefile.
; Obviously, returning '0' means no data/not an IDL save file

FUNCTION is_idlsave,filename

  status=0

  catch, catcherr
  if catcherr EQ 0 then begin
;      restore, filename
      sObj = OBJ_NEW('IDL_Savefile',filename)
      sContents = sObj->Contents() ;
      status = sContents.N_VAR
      obj_destroy,sObj
  end else begin
      catch, /cancel
      status=0
;      message, 'This is not a SAVE file'
  end
  catch, /cancel 

  return,status

END
