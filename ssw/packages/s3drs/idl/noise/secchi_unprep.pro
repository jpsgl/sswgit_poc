;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : secchi_unprep
;               
; Purpose   : tools for examining and mimicing SECCHI-type data noise
;               
; Explanation: 
;               
; Use       : useful but generally not used
;    
; Inputs    : 
;               
; Outputs   : 
;
; Keywords  : 
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; Given a SECCHI Level 1.0 B/B0 dataset and header, reverses many
; of the secchi_prep processing to creating an approximation of
; that data's original Level 0.5 DN state.  Note this is based on
; an early secchi_prep of October 2006, and thus is not a very
; accurate reverse of calibration.  It is useful for 'back of the
; envelope' calculations of original noise levels and similar.
;
; If /use_calimg, adds a proper flatfield as per secchi_prep.
;
; Example, given level 10 data and hdr
; data_05 = secchi_unprep(data_10, hdr10, /use_calimg)
; save_secchi05,data_05, hdr10, filename
;

FUNCTION secchi_unprep, data10, hdr, use_calimg=use_calimg

;  dn2msb = get_calfac(hdr)
  dn2msb = get_msb2dn(hdr)
  exptime = hdr.exptime

  use_calimg=KEYWORD_SET(use_calimg)
  if (use_calimg) then flatfield = GET_CALIMG(hdr) else flatfield=1.0

  biasmean = hdr.biasmean

  data05 = ( (data10 / flatfield) * exptime / dn2msb ) + biasmean

  return, data05

END
