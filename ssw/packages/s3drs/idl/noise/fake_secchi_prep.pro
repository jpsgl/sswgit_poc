;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : fake_secchi_prep
;               
; Purpose   : tools for examining and mimicing SECCHI-type data noise
;               
; Explanation: given an L0.5 header and DN data, fakes secchi_prep for it
;               
; Use       : useful but generally not used
;    
; Inputs    : 
;               
; Outputs   : 
;
; Keywords  : 
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; Given a SECCHI Level 0.5 header and appropriate DN data,
; mimics a simple 'secchi_prep()'.  Mostly used for testing
; purposes or when a flatfield is not available.
; If file is in raw DN (without the biasmean yet subtracted),
;   use the /subtractbias flag.  If data is already in actual
;   DN (typically when using simulated data or noise calculations
;   or if the data has already had the biasmean subtracted), do
;   not use the /subtractbias flag.

FUNCTION fake_secchi_prep, data, hdr, use_calimg=use_calimg, $
                           subtractbias=subtractbias

;  dn2msb = get_calfac(hdr)
  dn2msb = get_msb2dn(hdr)
  exptime = hdr.exptime
  subtractbias=KEYWORD_SET(subtractbias)
  if (subtractbias) then biasmean = hdr.biasmean else biasmean=0

  use_calimg=KEYWORD_SET(use_calimg)
  if (use_calimg) then flatfield = GET_CALIMG(hdr) else flatfield=1.0

  outdata = ( (data - biasmean) * dn2msb / exptime) * flatfield

  return, outdata

END
