;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : secchi_to_photons
;               
; Purpose   : tools for examining and mimicing SECCHI-type data noise
;               
; Explanation: 
;               
; Use       : useful but generally not used
;    
; Inputs    : 
;               
; Outputs   : 
;
; Keywords  : 
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; This is an earlier stab at making real noise routines and is
; kept for historical reasons only.  We do not recommend using it.
;
; Eventually we used this for testing then split the different
; noise methods into their seperate programs, which have since
; developed further than this earlier one.
;

function secchi_to_photons,hdr,L05=L05,L10=L10, $
                           noiseDN=noiseDN, biasnoise=biasnoise, $
                           fractional=fractional, $
                           simprep=simprep,secchiprep=secchiprep, $
                           fakeprep=fakeprep,unprep=unprep

; routine to take a SECCHI Level 1 file and make an
;   estimate of the equivalent photons incident on detector
;   it used.  It can return either the photons or a noise
;   measurement.  By default, it returns the photons.  Instead,
;   you can specify /noise to return the noise (in DN).
;
; REQUIRES: the FITS header, and either Level 0.5 or 1.0 data via keywords:
;         secchi_to_photons(hdr,L05=<data>) or
;         secchi_to_photons(hdr,L10=<data>)
; (Both L05 and L10 can be provided without conflict.)
;
; It uses Level 1 data and 'fakes' 0.5 from it.  If given 0.5 data,
;   it does not need Level 1 unless the /noiseDN and /fractional
;   flags are both set.
;   Although it can accept a Level 0.5 file and generate the noise
;   or even a nominal minimally calibrated Level 1.0 file, we
;   recommend you use 'secchi_prep' for all your Level 1.0 needs.
;
; The result is always either an (0.5) Photons data or a Level 1.0 noise.
;
; Input flags that are noise related are given as follows:
;    /noiseDN, returns the data's noise in DN instead of returning raw photons
;        This requires one of the following 3 flags be set (default=/simprep)
;    /simprep, mimics secchi_prep internally (default).
;    /secchiprep, tells us to actually run secchi_prep rather than
;       using our simulated routines here
;    /fractional, returns a fractional noise rather than a prepped noise
; Also,
;    /biasnoise, adds the noise from the offset bias found in header
;       (only relevant if the 'noiseDN' flag is set).  Default is to
;       not do this.
;
; Two alternative functions are available.  These are used when this
; routine serves as a 'fake secchi_prep' or as a 'secchi_unprep'
;
;   /fakeprep = given L05 data, returns L10 data.  Ignores all other flags.
;   /unprep = given L10 data, returns L05 data.  Ignores all other flags.
;
;
; This is based on the Nov 2006 secchi_prep, where the following
; operations are done.  Later secchi_prep may not match, but
; this is considered a very simplistic backward conversion routine
; and therefore cautionaries are in effect.

; What secchi prep does is:
;  data_L10 = ( (data_L0_5 - offsetbias) * dn2msb / exptime) * flatfield
; therefore,
;  data_L05 = ((data_L1_0 / flatfield) * exptime / dn2msb) + offsetbias
;
; We define:
;   dn2msb = constant, e.g. 7.1E-11 for Cor1, from get_calfac
;   dn2photons = constant, e.g. 15 (for each photon makes 15 DN)
;   offsetbias = from FITS header, usually the keyword 'biasmean'
;
;   rawDN = data_L0_5 - offsetbias
;   num_photons = rawDN / dn2photons.
;   sigmaPhoton = sqrt(num_photons)
;   sigmaDN = sigmaPhoton * dn2photons
;   fractional_noise = (sigmaDN/rawDN) * flatfield
;
; So ultimately we can get:
;   L1noise = data_L1_0 * fractional_noise
; or, an alternative formulations,
;   noisedata = sigmaDN + offsetbias   ; the L0_5 equiv of a noise image
;   L1noise = ( (noisedata - offsetbias) * dn2msb / exptime) * flatfield
;

  noiseDN=KEYWORD_SET(noiseDN)
  secchiprep=KEYWORD_SET(secchiprep)
  biasnoise=KEYWORD_SET(biasnoise)
  simprep=KEYWORD_SET(simprep)
  fractional=KEYWORD_SET(fractional)
  fakeprep=KEYWORD_SET(fakeprep)
  unprep=KEYWORD_SET(unprep)

  if (datatype(hdr) ne 'STC') then begin
      hdrstr=fitshead2struct(hdr)
  end else begin
      hdrstr=hdr
  end

  offsetbiasraw = hdr.biasmean
  dn2photons = 15                ; placeholder or best guess
  dn2msb = get_calfac(hdr)

; quick check to see what our input data is-- L0.5 or L1.0?

  if (n_elements(L10) eq 0 and n_elements(L05) eq 0) then begin
      print,'No data sent, usage is: pho=secchi_to_photons(hdr,L10=<data>) or'
      print,'                        pho=secchi_to_photons(hdr,L05=<data>)'
      return,-1
  end

  if (n_elements(L10) ne 0) then data_L1_0 = ptr_new(L10)
  if (n_elements(L05) ne 0) then data_L0_5 = ptr_new(L05)
  
; first check if the necessary 0.5 data exists

  ; all methods require level 0.5 data exist
  if (n_elements(data_L0_5) eq 0) then begin
      ; no 05 means 10 must exist (because we error if neither does)
      data_L0_5 = ptr_new(((*data_L1_0/flatfield)*exptime/dn2msb)+offsetbias) 
      if (unprep) then begin
          ; alternative function, return unprepped data
          print,'Simulating unprep of secchi_prep, returning 0.5ish data'
          return,*data_L0_5
      end
  end


; now we check if we need Level 1.0 data at all

  if (n_elements(data_L1_0) eq 0 and simprep) then begin
     ; alternative function, return prepped data
      print,'Simulating secchi_prep, returning 1.0ish data'
     ; pseudo secchi_prep
      data_L1_0 = $
        ptr_new(((*data_L0_5-offsetbias)*dn2msb/exptime)*flatfield)
      return,*data_L1_0
  end

  if (n_elements(data_L1_0) eq 0 and noiseDN and fractional) then begin
      ; only the /noiseDN + /fractional method needs the 1.0 data

      if (secchiprep) then begin
          ; run secchi_prep
          L1hdr=hdr
          L1hdr.filename='temp' + L1hdr.filename
          writefits,L1hdr.filename,data,struct2fitshead(L1hdr)
          secchi_prep,L1hdr.filename,/write_fts
          ; figure out the filename secchi_prep used
          prepname = L1hdr.filename
          strput,prepname,'1',16
          data_L1_0 = sccreadfits(prepname)
      end else begin
          ; pseudo secchi_prep, same as in if/end set in previous block
          data_L1_0 = $
            ptr_new(((*data_L0_5-offsetbias)*dn2msb/exptime)*flatfield)
      end
  end


; #### Now for the actual photon and noise routines.

  rawDN = *data_L0_5 - offsetbias
  num_photons = rawDN / dn2photons

  if (noiseDN) then begin
      ; return noise

      sigmaPhoton = sqrt(num_photons)
      sigmaDN = sigmaPhoton * dn2photons

      if (biasnoise) then sigmaDN=biasnoise(sigmaDN,hdr)

      if (fractional) then begin
          ; "fractional noise" approach
          fractional_noise = (sigmaDN/rawDN) * flatfield
          L1noise = *data_L1_0 * fractional_noise
      end else begin
          ; "process noise data" approach-- run secchi_prep on noisedata
          noisedata = sigmaDN + offsetbias ; the L0_5 equiv of a noise image

          if (secchiprep) then begin
              ; run secchi_prep
              noisehdr=hdr
              noisehdr.filename='noise' + noisehdr.filename
              writefits,fname,noisedata,struct2fitshead(noisehdr)
              secchi_prep,fname,/write_fts
              ; figure out the filename secchi_prep used
              prepname = noisehdr.filename
              strput,prepname,'1',16
              L1noise = sccreadfits(prepname)
          end else begin
              ; run pseudo secchi_prep, aka 'simprep' flag
              L1noise = ((noisedata-offsetbias)*dn2msb/exptime)*flatfield
          end

      end
     
      return, L1noise

  end else begin
      ; return photons
      return,num_photons
  end


end
