;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : fractional_noise
;               
; Purpose   : tools for examining and mimicing SECCHI-type data noise
;               
; Explanation: one method of computing photon noise from secchi data
;               
; Use       : useful but generally not used
;    
; Inputs    : 
;               
; Outputs   : 
;
; Keywords  : 
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; Given a dataset of SECCHI 'DN detected', calculates the
; fractional noise level assuming sqrt(photons) as noise and
; returns the data array of these noise values.
; Optionally will do bias subtraction if asked with
;   /subtractbias (only ask if you are passing raw FITS
;   values rather than raw DN!)
;
; By default, returns fractional noise  sigmaDN/rawDN.
; If given /sigma flag, will return the sigmaDN instead.
;
; Do not forget to pass any normalization values, e.g.
;   norm=sr.n0

FUNCTION fractional_noise, rawDN, hdr=hdr, dn2photons=dn2photons, $
                           norm=norm, subtractbias=subtractbias,sigma=sigma

  if (n_elements(dn2photons) eq 0) then dn2photons = 15 ; best guess
  if (n_elements(norm) eq 0) then norm=1.0; no norm = no normalization

  dn2photons = dn2photons * norm

  sigma = KEYWORD_SET(sigma)

  subtractbias = KEYWORD_SET(subtract_bias)
  if (subtractbias ne 0 and n_elements(hdr) ne 0) then begin
      ; rawDN is actually rawFITS and needs converting
      biasmean=hdr.biasmean
  end else begin
      ; do not subtract a bias
      biasmean=0.0
  end

  ; note use of loop so we can use either data(N,N,Nd) or data(N,N)
  if (n_elements(dn2photons) gt 1) then begin

      num_photons = rawDN; just to get array size and type right
      for i=0,n_elements(dn2photons)-1 do $
        num_photons[*,*,i] = dn2photons[i] * (rawDN[*,*,i] - biasmean)

      sigmaPhoton = sqrt(num_photons)
      sigmaDN = rawDN; just to get array size and type right
      for i=0,n_elements(dn2photons)-1 do $
        sigmaDN[*,*,i] = sigmaPhoton[*,*,i] / dn2photons[i]

  end else begin

      num_photons = dn2photons * (rawDN - biasmean)
      sigmaPhoton = sqrt(num_photons)
      sigmaDN = sigmaPhoton / dn2photons

  end

  if (sigma) then begin

      return,sigmaDN

  end else begin

    ; FRACTIONAL METHOD
    ;  fractional_noise = (sigmaDN/rawDN)
    ;  noise = sigmaMSB = im_L1_0 * fractional_error
      fractional_noise = (sigmaDN/rawDN);  * data_flat
      return, fractional_noise

  end

END
