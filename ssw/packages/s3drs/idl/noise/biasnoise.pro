;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : biasnoise
;               
; Purpose   : tools for examining and mimicing SECCHI-type data noise
;               
; Explanation: adds in noise due to offset bias
;               
; Use       : useful but generally not used
;    
; Inputs    : 
;               
; Outputs   : 
;
; Keywords  : 
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; given an array of signal noise, addes the offset
; bias noise from the header to it, in quadrature:
;   total_noise = sqrt(signal_noise^2 + offsetnoise^2)
; The offset bias noise is assumed to be in DN.  If not,
;   this routine needs to be rewritten.
; The default assumes the noise array is in DN but can
;   also handle noise in MSB or other units.  Simple add
; dn2msb=<value> to the call to convert the offset bias into
;   the same units as the noise array.
;
; For example, given an instrument factor of dn2msb=7.1E-11,
; For a noise array in DN:  noise=biasnoise(noise,hdr)
; For a noise array in MSB: noise=biasnoise(noise,hdr,dn2msb=7.1E-11)
;

FUNCTION biasnoise,noise,hdr,dn2msb=dn2msb

  if (n_elements(dn2msb) eq 0) then dn2msb=1.0

  if (datatype(hdr) ne 'STC') then begin
      biasmean = fxpar(hdr,'BIASMEAN')
  end else if (tag_exist(hdr,'biasmean')) then begin
      biasmean=hdr.biasmean
  end else begin
      biasmean=0
  end

  ; convert this signal to the same units as the noise array
  ; note noise=sqrt(data) so noise^2 just = data
  biasnoise_squared=biasmean*dn2msb

  if (biasmean gt 0) then begin
      noise = sqrt( TEMPORARY(noise^2) + biasnoise_squared)
  end else begin
      print,'No relevant bias mean value from header, not adding bias noise.'
  end

  return,noise

END
