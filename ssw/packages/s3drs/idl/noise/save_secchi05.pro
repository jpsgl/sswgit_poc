;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : save_secchi05
;               
; Purpose   : tools for examining and mimicing SECCHI-type data noise
;               
; Explanation: saves sim or modified data in L0.5 format
;               
; Use       : useful but generally not used
;    
; Inputs    : 
;               
; Outputs   : 
;
; Keywords  : 
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; Given a proper SECCHI hdr structure and a data array of
; DN values of accurate size, saves the DN values as a SECCHI
; Level 0.5 file (suitable for processing via secchi_prep)
; (including applying the biasmean)
;
; Note it alters the original FITS keyword filename to match
; the filename you save.  Typically, secchi_prep of an 0.5
; file will produce a filename of:
;  strput, filename,'1',16
;

PRO save_secchi05, data, hdr, foutname
  
  hdranon=create_struct(hdr,name='')

  hdranon=rep_tag_name(hdranon,'EXTEND','SIMPLE')
  hdranon=rep_tag_value(hdranon,0,'BIASMEAN')
  hdranon=rep_tag_value(hdranon,0,'BZERO')
  hdranon=rep_tag_value(hdranon,1,'BSCALE')
  hdranon=rep_tag_value(hdranon,'1','SIMPLE')

  hdrstring = struct2fitshdr(hdranon)

  writefits,foutname,data,hdrstring

END
