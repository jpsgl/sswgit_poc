;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : secchi_noiseprep
;               
; Purpose   : tools for examining and mimicing SECCHI-type data noise
;               
; Explanation: 
;               
; Use       : useful but generally not used
;    
; Inputs    : 
;               
; Outputs   : 
;
; Keywords  : 
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; Given a dataset of SECCHI Level 0.5 data, computes the
;   noise in DN, saves that DN noise as a Level 0.5 file,
;   then runs secchi_prep on it to return a properly calibrated
;   noise in B/B0
; The optional /subtractbias flag should only be set if the
;   incoming data is raw FITS data and not bias-subtracted DN.
; If /cleanup is given, it deletes the intermediate Level0.5 and
;   Level1.0 files it made (We recommend using /cleanup)
; Note that we use a biasmean=0 for our noise files, so that the
;   issue of whether or not the biasmean is subtracted is not
;   a problem for subsequent calculations-- the noise data is
;   always already "bias subtracted" in its intermediate files.

FUNCTION secchi_noiseprep, data, hdr, subtractbias=subtractbias, $
                         cleanup=cleanup

  cleanup=KEYWORD_SET(cleanup)

  fnoisefile05='tempnoise05xxxxx.fts'; a temporary file to make

  sigmaDN = fractional_noise(data, hdr=hdr, /sigma, subtractbias=subtractbias)
  sigmaDN = float(TEMPORARY(sigmaDN))
  save_secchi05,sigmaDN, hdr, fnoisefile05

  secchi_prep,fnoisefile05,hdr,noise
  if (cleanup) then file_delete,fnoisefile05

;  fnoisefile10 = hdr.filename
;  strput, fnoisefile10,'1',16; how an L1.0 filename differs from L0.5
;  noise = sccreadfits(fnoisefile10)
;  if (cleanup) then file_delete,fnoisefile10

  return,noise
END

