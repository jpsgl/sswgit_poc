;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : p_instrspec
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: Returns various instrument parameters, we use this so
; that all instrument additions and updates are in the same place.
;               
; Use       : used by 's3drs.pro' and 'quickcme' wrappers
;    
; Inputs    : option,instr,s3drs=s3drs
;             option = 'name', 'fov', 'occulter', 'dnphotons', 'dn2msb'
;             instr = any known instrument name (list is in option='name')
;               
; Outputs   : returns either a single value or a list of values
;
; Keywords  : s3drs
;             s3drs: toggle limiting list return to just those that
;             work in the current build of the GUI.  As you add and
;             test instruments and verify they work, you can set them
;             as valid here and the GUI will automatically pick them up.
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : gui, s3drs
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            


; moved all of the instrument-specific specs here to allow easy
; generalized addition or modification of instruments.
;
; default is to return the full list, but if you give an 'instr',
; then it returns just that item.
;
; Optional flag /s3drs makes it only return the values for items
; defined in s3drs, which typically means only those items tested
; and working with either the native s3drs handlers or via the
; 'p_otherdata' data ingester.  So as instruments get added to
; the data ingest, you can put them in and test via this script.
; For example, we never put in EUVI or EIT into s3drs, but if you
; wanted to add them to 'p_otherdata' then you can just add them
; here and they will get automatically picked up by the s3drs GUI.
; Note the /s3drs flag has no meaning if you specify an 'instr',
; since in that case you are just fetching a parameter, not a full list.
;
; See the 'LOCAL' type in p_otherdata.pro for a sample of how to add
; an instrument data handler to s3drs.
;

FUNCTION p_instrspec,option,instr,s3drs=s3drs

  s3drs=KEYWORD_SET(s3drs)

  instrlist=         ['none', 'Cor2',  'Cor1',  'C2',  'C3', 'EIT','EUVI','C1','Local']
  s3drsvalid=        [  0,       1,       1,     1,     1,     0,    0,    0  , 1]
  fov_radius_in_rsun=[15.0,    15.0,     4.0,    6.0,  30.0,  0.5,  0.5,  3.0 ,10.0]
  occulter=          [0.0,      2.0,     1.5,    1.5,   3.5,  0.0,  0.0,  1.1 , 5.0]
  DNperPhoton=       [1.0,    15.29,   15.29,  15.29,  15.29, 1.0,  1.0, 15.29, 1.0]
  DN2MSB=            [1.0,  2.7E-12,2.7E-12,2.7E-12,2.7E-12,1.0,  1.0, 2.7E-12, 2.7E-12]

  case strmid(strupcase(option),0,3) of
      'NAM': retme=instrlist
      'FOV': retme=fov_radius_in_rsun
      'OCC': retme=occulter
      'DNP': retme=DNperPhoton
      'DN2': retme=DN2MSB
      else: retme=instrlist ; default is to return names
  end

  if (n_elements(instr) ne 0) then begin
      ; instead of the full list, they just want the specific element

      ifound=0; default is 'none' with default values
      for ic=0,n_elements(instrlist)-1 do $
        if (strupcase(instr) eq strupcase(instrlist[ic])) then ifound=ic
      retme=retme[ifound]

  end else if (s3drs) then begin

      ; sort out only those items valid for the s3drs GUI
      for ic=0,n_elements(retme)-1 do $
        if (s3drsvalid[ic] eq 1) then push,retme2,retme[ic]
      retme=retme2

  end

  return,retme

END
