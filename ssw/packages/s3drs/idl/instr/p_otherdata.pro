;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : p_otherdata
;               
; Purpose   : generic stub for loading data, part of the s3drs GUI
;               
; Explanation: wrapper that lets you easily add other instruments for
; 3D reconstruction
;               
; Use       : called from 's3drs.pro' and 'quickcme.pro'
;    
; Inputs    : datadate = date to find data for
;             instr = instrument type, for case statement
;               
; Outputs   : dataout = returned data array
;             hdrout = FITS header structure from data
;
; Keywords  : /background sets the fork to return a background data,
; not event data
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : fits, data
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; stub for adding new instruments to S3DRS reconstruction.
; This routine expects the input date (in the format
; YYYYMMDD_HHMM) and returns the data plus the FITS
; header.  The optional /background flag tells it to return
; the appropriate background file for that date instead of
; that day's data.
;
; Routine populates 'dataout' with HAE-oriented data and also
; populates 'hdrout' with FITS header.
;
; Routine does not yet handle failure well, since we actually want
; this to fail if it cannot load or find data!  The 'quickcme'
; wrapper uses the lack of existence of 'hdrout' as a sign to
; gracefully warn the user and continue appropriately, so in essence
; I've moved the error checking to that routine.
;
; Below I have one sample case put in, 'local', which expects a
; FITS file in the working directory named as [date].fts or
; (for backgrounds) [date]background.fts.
; It is provided mostly as an example, but is entirely valid if you
; just want to import data from an external generator using this.
;
; Note we use a case statement that forces the instrument name to
; upper case (for less ambiguous matches), so make sure your case
; syntax in this routine uses upper case (as with the 'LOCAL' example).
;

PRO p_otherdata,datadate,instr,dataout,hdrout,background=background

  background=KEYWORD_SET(background)

  print,'Processing data for ',instr

  if (background) then begin

      ; fetch instr-appropriate background
      case strupcase(instr) of

          'LOCAL': begin

              checkf = DIALOG_PICKFILE(/MUST_EXIST,/READ,$
                 TITLE='Please select a background file (FITS format only)',$
                 FILTER=['*.fts','*.fits','*.FTS','*.FITS','*.Fits','*.Fts'])
              ;checkf=datadate+'background.fts'
              if (checkf ne "") then begin
                  if (file_exist(checkf)) then begin
                      dataout=readfits(checkf,hdr)
                      hdrout=fitshead2struct(hdr)
                  end
              end

          end

          else: ; do nothing

      end

  end else begin

      ; fetch instr-appropriate HAE (ecliptic)-oriented data
      ; fetch instr-appropriate background
      case strupcase(instr) of

          'LOCAL': begin

              checkf = DIALOG_PICKFILE(/MUST_EXIST,/READ,$
                 TITLE='Please select an input data file (FITS format only)',$
                 FILTER=['*.fts','*.fits','*.FTS','*.FITS','*.Fits','*.Fts'])
              ;checkf=datadate+'.fts'
              if (checkf ne "") then begin
                  if (file_exist(checkf)) then begin
                      dataout=readfits(checkf,hdr)
                      hdrout=fitshead2struct(hdr)
                  end
              end

          end

          else: ; do nothing

      end



  end

END

