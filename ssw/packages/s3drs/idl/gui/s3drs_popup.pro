FUNCTION s3drs_popup,ev
;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs_popup
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: initial splash screen when invoking 's3drs'
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : none
;               
; Outputs   : none (creates license key file for future runs)
;
; Keywords  : 
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

   lickey=getenv('HOME')+'/.pixon'
   ;print,lickey

   if (file_exist(lickey)) then pixonneeded=0 else pixonneeded=1

   txt=[s3drs_text_credits(1),"",s3drs_text_ack(1)]

   ; also add pixon pre-accpetance terms
   if (pixonneeded or n_elements(ev) ne 0) then txt=[txt,s3drs_text_pixon()]

   answer=YES_NO_MENU(txt,/binary,title='Use Agreement',/big,$
                        optionalquestion='I agree to the above terms of use:')

   ; if they choose not to accept Pixon, go with this:
   if (pixonneeded eq 1 and answer eq 0) then begin
       ; okay, they turned down Pixon.  Take two
       ; disable Pixon in code
       pixonneeded=-1; turn off automatic license generation
       txt2="Pixon is diabled for this run."
       answer=YES_NO_MENU(txt2,/binary,title='Continue?')
       if (answer eq 0) then stop
   end else if (answer eq 0 and n_elements(ev) eq 0) then begin
       ; even without Pixon, they do not agree, so stop
       stop
   end

   if (answer eq 1 and pixonneeded eq 1) then begin
       print,'Creating license file here: ',lickey
       openw,lic,lickey,/get_lun
       printf,lic,s3drs_text_ack()
       free_lun,lic
   end

   return,pixonneeded

END
