;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs_modelparam_draw, s3drs_modelparam
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: creates and updates variable-sized widget of model params
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : event (not used)
;               
; Outputs   : (changes GUI state)
;
; Keywords  : 
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
PRO s3drs_modelparam_draw,params,ppair,subpanelID,fix,uname,values

  for i=0,n_elements(params)-1 do begin

      ; UVALUE=2 used for tracking, are often destroyed and recreated

      if (fix) then begin
          subpanelAlt = subpanelID
      end else begin
          subpanelAlt=subpanelID
      end
      params[i]=strmid(params[i],0,5)
      pitem = CW_FIELD(subpanelAlt,/FLOAT,XSIZE=5,TITLE=params[i],$
                       UNAME=uname+'p'+int2str(i),UVALUE=2,$
                       VALUE=values[i])
      if (fix) then $
        pitem2 = WIDGET_DROPLIST(subpanelAlt,VALUE=ppair,$
                                 UVALUE=2,UNAME=uname+'f'+int2str(i))
      
  end

END

PRO s3drs_modelparam,event,parent=parent

  common s3drs,s3drs

  ; sanity check
  if (n_elements(event) eq 0 and n_elements(parent) eq 0) then return

  ; The choice is in the DropList,
  ;   the widgets to destroy/create are in the SubPanel
  ; the uname is stored as UVALUE in both DropList and SubPanel
  ; the official uname of DropList is uname+'model',
  ; the official uname of SubPanel is uname+'sub',
  ; the titleID uname is uname+'title', and holds UVALUE of 'fix'
  ;
  ; If you are passed an event, that is from the DropList and you
  ; need to extract the SubPanel id, plus the DropList model choice.
  ; If you are passed parent=, that is the SubPanel id and you
  ; need to get the uvalue so you can get the modelchoice from the DropList
  ; In both cases, we also get the uname

  if (n_elements(parent) ne 0) then begin
      ; usually being initialized
      ; first get id of the DropList
      subpanelID = parent
      WIDGET_CONTROL,subpanelID,GET_UVALUE=unames
      uname=unames[0]
      ;droplistID = WIDGET_INFO(s3drs.wTLB,FIND_BY_UNAME=uname+'model')
  end else begin
      ; from event
      droplistID=event.id
      WIDGET_CONTROL,droplistID,GET_UVALUE=unames
      uname=unames[0]
      subpanelID = WIDGET_INFO(s3drs.wTLB,FIND_BY_UNAME=uname+'sub')
  end

  ; find model choice (string) from DropList by name
  modelname=(widget_shorthand(uname+'model',/MODELLIST)).name

  wtitle = WIDGET_INFO(s3drs.wTLB,FIND_BY_UNAME=uname+'title')
  if (wtitle ne 0) then WIDGET_CONTROL,wtitle,GET_UVALUE=fix else fix=0

  ; now process model and populate

  ; clear out old ones
  wchildren=WIDGET_INFO(subpanelID,/ALL_CHILDREN)
  for ic=0,n_elements(wchildren)-1 do begin
      if (wchildren[ic] ne 0) then begin
          WIDGET_CONTROL,wchildren[ic],GET_UVALUE=uvalue
          if (n_elements(uvalue) ne 0) then $
            if (uvalue eq 2) then $
            WIDGET_CONTROL,wchildren[ic],/DESTROY
      end
  end

  ; put in new ones
  modelid=s3drs_arnaud(modelname)

  if (modelid gt 0) then begin

      ; not all Arnaud models are parameterized or working
      i=modelid; for terseness
      if (i ne 32 and i ne 67 and i ne 25 and i ne 26 and i ne 30 and i ne 35 and i ne 36 and i ne 37 and i ne 40) then begin

          getmoddefparam,modelid,ss

          if (tag_exist(ss.s,'keyword') ne 0) then begin

              params=ss.s.keyword
              if (n_elements(params) gt 2) then begin
                  ignore=pop(params,/trim) ; Arnaud uses 0 indexing
                  values=parsemoddefparam(ss)
                  ppair=['vary','freeze']
                  s3drs_modelparam_draw,params,ppair,subpanelID,$
                    fix,uname,values
              end
              
          end
      end

  end

END

