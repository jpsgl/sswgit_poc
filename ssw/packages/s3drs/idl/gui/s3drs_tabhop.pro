; wrapper to just hop to next, useful as an event handler in GUI
PRO s3drs_tabhop,ignoreevent,skip=skip
;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs_tabhop
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: wrapper to just hop around tabs in either direction
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : event (ignored)
;               
; Outputs   : none (changes state)
;
; Keywords  : skip=skip (how many to go, including direction +-)
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

  if (n_elements(skip) eq 0) then skip=1; number of hops to make
  if (skip lt 0) then $
    for i=skip,-1 do s3drs_tabswitch,/PREV else $
    for i=1,skip do s3drs_tabswitch,/NEXT

END
