; used to switch around tab state for FM
PRO s3drs_fmtoggle,event
;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs_fmtoggle
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: toggles tabs in main GUI
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : event
;               
; Outputs   : (changes states in GUI)
;
; Keywords  : 
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

  common s3drs,s3drs

  reconchoice=widget_shorthand('reconmethod',/DROPLIST)
  if (reconchoice.index eq 2) then begin
      WIDGET_CONTROL,s3drs.tabs.fmID,SENSITIVE=1
      reconbutton=WIDGET_INFO(s3drs.tabs.reconID,FIND_BY_UNAME='reconbutton')
      WIDGET_CONTROL,reconbutton,SENSITIVE=0,SET_VALUE='(go to FM screen)'
      if (s3drs.tabhop) then $
        s3drs_tabhop
  end else begin
      WIDGET_CONTROL,s3drs.tabs.fmID,SENSITIVE=0
      reconbutton=WIDGET_INFO(s3drs.tabs.reconID,FIND_BY_UNAME='reconbutton')
      WIDGET_CONTROL,reconbutton,SENSITIVE=1,SET_VALUE='Run reconstruction!'
  end

END
