; Main event-handler routine 
PRO s3drs_handler_event, ev 
;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs_handler_event, s3drs_handler
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: s3drs_handler_event is the event handler for the GUI,
; while s3drs_handler just forces it to precompile
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : event
;               
; Outputs   : changes state of GUI
;
; Keywords  : 
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

  common s3drs,s3drs

  if (TAG_NAMES(ev,/STRUCTURE_NAME) eq 'WIDGET_KILL_REQUEST') then begin
      status=s3drs_exit()
      if (status eq 1) then return
  end

  if (ev.id eq WIDGET_INFO(s3drs.wTLB,FIND_BY_UNAME='nviewpoints')) then begin
      s3drs_synspec; puts up variable number of entry widgets as appropriate
      ignore=s3drs_synoff(ev); resets buttons/tabs to await new data
      return; nothing more to do
  end

  ; Retrieve the anonymous structure contained in the user value of 
  ; the top-level base widget.  
  WIDGET_CONTROL, ev.TOP, GET_UVALUE=stash 
 
  ; Retrieve the total number of tabs in the tab widget and 
  ; the index of the current tab. 
  numTabs = WIDGET_INFO(stash.TopTab, /TAB_NUMBER) 
  thisTab = WIDGET_INFO(stash.TopTab, /TAB_CURRENT) 

  ; If the current tab is the first tab, desensitize the 'Previous' button. 
  IF (thisTab EQ 1 and s3drs.wrap eq 0) THEN BEGIN 
    WIDGET_CONTROL, stash.bPrev, SENSITIVE=0 
  ENDIF ELSE BEGIN 
    WIDGET_CONTROL, stash.bPrev, SENSITIVE=1 
  ENDELSE 
 
  ; If the current tab is the last tab, desensitize the 
  ; 'Next' button. 
  IF (thisTab EQ numTabs-2 and s3drs.wrap eq 0) THEN BEGIN 
    WIDGET_CONTROL, stash.bNext, SENSITIVE=0 
  ENDIF ELSE BEGIN 
    WIDGET_CONTROL, stash.bNext, SENSITIVE=1 
  ENDELSE 
 
  ; If the user clicked either the 'Next' or 'Previous' button, 
  ; cycle through the tabs by calling the 'TWE1_SwitchTab' 
  ; procedure. 
  IF (ev.ID EQ stash.bNext) THEN $ 
    s3drs_tabswitch, thisTab, numTabs, stash, /NEXT 
  IF (ev.ID EQ stash.bPrev) THEN $ 
    s3drs_tabswitch, thisTab, numTabs, stash, /PREV 
 
  ; If the user clicked the 'Done' button, print out the values of 
  ; the various widgets, as contained in the 'retStruct' 
  ; structure. In a real application, this step would probably 
  ; adjust settings in the application to reflect the changes made 
  ; by the user. Finally, destroy the widgets. 
  IF (ev.ID EQ stash.bDone) THEN BEGIN 
    PRINT, 'BGroup1 selected indices: ', stash.retStruct.BGROUP1 
    PRINT, 'BGroup2 selected index: ', stash.retStruct.BGROUP2 
    PRINT, 'Slider value: ', stash.retStruct.SLIDER 
    PRINT, 'Text value: ', stash.retStruct.TEXT 
    result=DIALOG_MESSAGE('Saved-- continue to Exit?',/QUESTION)
    if (result eq 'Yes') then WIDGET_CONTROL, ev.TOP, /DESTROY 
  ENDIF 

  ; If the user clicked the 'Cancel' button, print out a message 
  ; and destroy the widgets. In a real application, this step would 
  ; allow the user to discard any changes made via the tabbed 
  ; interface before sending them to the application. 
  IF (ev.ID EQ stash.bCancel) THEN BEGIN 
      result=DIALOG_MESSAGE('QUIT WITHOUT SAVING?',/QUESTION,/DEFAULT_NO)
      if (result eq 'Yes') then WIDGET_CONTROL, ev.TOP, /DESTROY 
  ENDIF 

  if (tag_exist(ev,'x') and tag_exist(ev,'y')) then begin

     ; resizes windows so scrollbars take effect
     ; arrives when top gets TLB_SIZE_EENTS
      minx=MIN([s3drs.winx,ev.x])
;      miny=MIN([s3drs.winy*2,ev.y])
      miny=MIN([s3drs.winy,ev.y])
      WIDGET_CONTROL,s3drs.wTLB,XSIZE=minx,YSIZE=miny
      tags=tag_names(s3drs.tabs)
      for it=0,n_elements(tags)-1 do $
        WIDGET_CONTROL,s3drs.tabs.(it),XSIZE=minx-50,YSIZE=miny-50
      ; special handling for the bigger windows

  end


END 

PRO s3drs_handler
  ; just to initialize
END
