; to initiate a freeze/popup, use:
;    s3drs_busy(1,waiting,message)
; to return control to the function, use:
;    s3drs_busy(0,waiting)
;

; to generally recover from an error, use the intermediate routine
; 's3drs_restore'

PRO s3drs_restore

  common s3drs,s3drs

  s3drs_busy,0,s3drs.waiting

END

PRO s3drs_busy,onoff,waiting,message

;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs_busy
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: puts on or off a widget and freezes/unfreezes the screen
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : onoff (toggle), message (optional)
;               
; Outputs   : waiting (the ID of the freeze widget)
;
; Keywords  : 
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

  common s3drs,s3drs

  if (n_elements(onoff) eq 0) then onoff=0; default is turn everything back on

  if (onoff) then begin

      ; block-and-busy while creating data

      if (n_elements(message) eq 0) then $
        message=['Please wait while we',$
             'prep and load the data',$
             'according to your chosen',$
             'values.']

      WIDGET_CONTROL,s3drs.wTLB,SENSITIVE=0
      INFOWIDGET,message,/keepalive,id=waiting,/NO_BLOCK

  end else begin

      ; turn off block-and-busy

      WIDGET_CONTROL,s3drs.wTLB,SENSITIVE=1
      WIDGET_CONTROL,waiting,/DESTROY
      waiting=0

  end

  s3drs.waiting=waiting

END
