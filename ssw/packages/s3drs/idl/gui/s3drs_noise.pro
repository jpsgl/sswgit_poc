; updates noise when slider does something

PRO s3drs_noise,event
;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs_noise
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: updates noise data when slider does something
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : event
;               
; Outputs   : (changes underlying s3drs structure)
;
; Keywords  : 
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

  common s3drs,s3drs

  if (s3drs.link and n_elements(event) ne 0) then begin

      ; as one, so do they all
      for iv=0,s3drs.nFrames-1 do begin
          push,allarray,event.value
          wid=WIDGET_INFO(s3drs.tabs.noiseID,FIND_BY_UNAME='snratio'+int2str(iv))
          WIDGET_CONTROL,wid,SET_VALUE=event.value
      end

  end else begin

      ; get individual ones

      for iv=0,s3drs.nFrames-1 do begin
          snratio = widget_shorthand('snratio'+int2str(iv))
          push,allarray,snratio.value
      end

  end

  if (ptr_valid(s3drs.snratio)) then ptr_free,s3drs.snratio
  s3drs.snratio=ptr_new(allarray)

;  if (ptr_valid(s3drs.pxptr)) then $
;    (*s3drs.pxptr).sc.nadjust=snratio.value

  ; Now replot
  ignore=s3drs_tv_replot('SNPlot1')
  s3drs_hist

END
