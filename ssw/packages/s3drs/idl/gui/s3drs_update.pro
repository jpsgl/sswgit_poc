;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs_update
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: legacy code storing a widget structure, for tabs only
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : event
;               
; Outputs   : changes underlying state
;
; Keywords  : 
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; Event function to store the value of a widget in the correct 
; field of the 'retStruct' structure. Note that rather than 
; referring to the structure fields by name, we refer to them 
; by index. This allows us to save the index value of the 
; appropriate structure field in the user value of the widget 
; that generates the event, which in turn allows us to use the 
; same function to save the values of all of the widgets whose 
; values we want to save. 
; 
PRO s3drs_update, ev 
  ; Get the 'stash' structure. 
  WIDGET_CONTROL, ev.TOP, GET_UVALUE=stash 

  ; Get the value and user value from the widget that 
  ; generated the event. 
  WIDGET_CONTROL, ev.ID, GET_VALUE=val, GET_UVALUE=uval 
  ;print,'got value ',val
  ; Set the value of the correct field in the 'retStruct' 
  ; structure, using the field's index number (stored in 
  ; the widget's user value). 
  stash.retStruct.(uval) = val 
  ; Reset the top-level widget's user value to the updated 
  ; 'stash' structure. 
  WIDGET_CONTROL, ev.TOP, SET_UVALUE=stash 
END 
