PRO s3drs_psplots,ev
;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs_plots
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: Main 'useful' routine to actually run a reconstruction
;               
; Use       : usually called from 's3drs.pro' software, can be invoked bare
;    
; Inputs    : ev = (optional) event
;               
; Outputs   : creates postscript output
;
; Keywords  : 
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

  common s3drs,s3drs

  if (n_elements(ev) ne 0) then usescreensettings=1 else usescreensettings=0

  if (usescreensettings) then $
    s3drs_busy,1,waiting,'Generating postscript, please wait'

  ; and make standard plots, if possible
  sgo=0
  if (s3drs.choice eq 'synthetic' and $
      ptr_valid(s3drs.timage) and ptr_valid(s3drs.objptr)) then begin
      sgo=1
      syn=*s3drs.timage
  end else begin
      if (ptr_valid(s3drs.objptr)) then sgo=1
  end

  if (sgo) then begin
      Nd=(*s3drs.pxptr).sr.Nd

      if (usescreensettings) then begin
          ; they want to use their on-screen values
          uname='VizData'
          datamax=(widget_shorthand(uname+'max')).value
          datamin=(widget_shorthand(uname+'min')).value
          uname='Trio'
          imgmax=(widget_shorthand(uname+'max')).value
          imgmin=(widget_shorthand(uname+'min')).value
          ;help,datamax,datamin,imgmax,imgmin
      end
      ; greater than 3 means split across multiple pages
      if (Nd gt 3) then multips=1 else multips=0
      for ip=0,(Nd-1)/3,1 do begin
          if (multips ne 0 and (ip+1)*3 ge Nd) then multips=2; close it off
          ;print,multips
          p_standardplots,*s3drs.pxptr,*s3drs.objptr,/mega,syn=syn,$
            /slice,noises=*s3drs.snratio,datamax=datamax,datamin=datamin,$
            imgmax=imgmax,imgmin=imgmin,multips=multips,Nstart=ip*3,$
            Nend=min([(ip+1)*3,Nd])
      end

  end

  if (usescreensettings) then s3drs_busy,0,waiting

END
