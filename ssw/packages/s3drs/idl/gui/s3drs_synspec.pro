; fill in appropriate number of synthetic specification gadgets
; (called by s3drs_handler)
PRO s3drs_synspec,event
;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs_synspec
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: sets up variable number of synthetic parameter widgets
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : event
;               
; Outputs   : changes underlying GUI
;
; Keywords  : 
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

  common s3drs,s3drs

  wTsynmc = WIDGET_INFO(s3drs.wTLB,FIND_BY_UNAME='synspec')
  if (wTsynmc eq 0) then return

  ; clear it out first
  wchildren=WIDGET_INFO(wTsynmc,/ALL_CHILDREN)

  for ic=0,n_elements(wchildren)-1 do begin
      if (wchildren[ic] ne 0) then WIDGET_CONTROL,wchildren[ic],/DESTROY
  end

  numwin=WIDGET_INFO(s3drs.wTLB,FIND_BY_UNAME='nviewpoints')
  WIDGET_CONTROL,numwin,GET_VALUE=num

;  ent=['Name:',$
;       'Rho (angle along ecliptic, deg):',$
;       'Phi (angle above ecliptic, deg):',$
;       'R (distance from sun in AU):']

  ent=['Name:','Rho:','Phi:','R:']

  for id=0,num-1 do begin
      wTsynmcsub = WIDGET_BASE(wTsynmc,COLUMN=1,FRAME=1)
      cg = WIDGET_DROPLIST(wTsynmcsub,VALUE=s3drs.instr,TITLE='Instr.',$
                          UNAME='synspecinstr'+int2str(id))
      cg = CW_FIELD(wTsynmcsub,/STRING,TITLE=ent[0],XSIZE=12,$
                    VALUE='view'+int2str(id+1),$
                    UNAME='synspecname'+int2str(id))
      cg = CW_FIELD(wTsynmcsub,/FLOAT,TITLE=ent[1],XSIZE=4,VALUE=90.0*id,$
                    UNAME='synspecrho'+int2str(id))
      cg = CW_FIELD(wTsynmcsub,/FLOAT,TITLE=ent[2],XSIZE=4,VALUE=0.0,$
                    UNAME='synspecphi'+int2str(id))
      cg = CW_FIELD(wTsynmcsub,/FLOAT,TITLE=ent[3],XSIZE=4,VALUE=1.0,$
                    UNAME='synspecll'+int2str(id))
  end

  wextra = WIDGET_BASE(wTsynmc,COLUMN=1,FRAME=1)
  wbut = WIDGET_BUTTON(wextra,VALUE='(add another view)',$
                       EVENT_PRO='s3drs_nviewbutton')

  if (s3drs.tabs.synID ne 0) then ignore=s3drs_synoff()

  return
END

