;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs_datasyn (toggle), s3drs_predate (toggle), s3drs_fetch
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: fetches basic data values from widgets
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : none
;               
; Outputs   : sets s3drs structure variables
;
; Keywords  : 
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

FUNCTION s3drs_datasyn,event
  ; toggles the first 2 tabs based on choice

  common s3drs,s3drs

  uname=WIDGET_INFO(event.id,/UNAME)

  wTintro=WIDGET_INFO(event.top,FIND_BY_UNAME='tab_data')
  wTsyn=WIDGET_INFO(event.top,FIND_BY_UNAME='tab_syn')

  if (uname eq 'datasyn' or uname eq 'datasynB') then begin

      WIDGET_CONTROL,wTintro,SENSITIVE=1
      WIDGET_CONTROL,wTsyn,SENSITIVE=0
      s3drs.choice='data'
      ; switch to data choice
      if (s3drs.tabhop) then $
        if (uname eq 'datasyn') then s3drs_tabhop else s3drs_tabhop,skip=-1

  end else if (uname eq 'syndata' or uname eq 'syndataB') then begin

      WIDGET_CONTROL,wTintro,SENSITIVE=0
      WIDGET_CONTROL,wTsyn,SENSITIVE=1
      s3drs.choice='synthetic'
      ; switch past data choice to synthetic, or back from syn to data
      if (s3drs.tabhop) then $
        if (uname eq 'syndata') then s3drs_tabhop,skip=2 else s3drs_tabhop

  end

  return,0

END

PRO s3drs_predate,event
  ; toggles the 'date' gadget for pre-events

  datasub=widget_shorthand('datasub',/droplist)
  pid=WIDGET_INFO(event.top,FIND_BY_UNAME='priordate')
  if (datasub.index eq 1) then $
    WIDGET_CONTROL,pid,SENSITIVE=1 else $
    WIDGET_CONTROL,pid,SENSITIVE=0

  ignore=s3drs_dataoff(event); also toggle global state

END


PRO s3drs_fetch_datadate

  common s3drs,s3drs

  res=(strsplit((widget_shorthand('datares',/droplist)).name,'x',/extract))[0]
  s3drs.Nd=long(res)

  datearr=int2str(widget_shorthand('datadate',/date),2)
  fitsdate = strjoin(datearr[0:2]) + '_' + strjoin(datearr[3:4])
;  fitsdate='20071231_0252'

  s3drs.cmedate=fitsdate

  datearr=int2str(widget_shorthand('priordate',/date),2)
;  fitsdate='20071231_0220'
  fitsdate = strjoin(datearr[0:2]) + '_' + strjoin(datearr[3:4])
  subchoice=widget_shorthand('datasub',/droplist)
  if (subchoice.index eq 0) then fitsdate='background' else $
    if (subchoice.index eq 2) then fitsdate='raw'

  s3drs.priordate=fitsdate

END

PRO s3drs_fetch

  ; just to force initialization

END
