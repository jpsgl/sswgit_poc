FUNCTION s3drs_synres,event
;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs_synres
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: updates resolution for synthetic data
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : event
;               
; Outputs   : changes underlying state
;
; Keywords  : 
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

  common s3drs,s3drs

  res=(strsplit((widget_shorthand('synres',/droplist)).name,'x',/extract))[0]
  n2=res/2

  ; in syn screen, update center choiec
  wid=WIDGET_INFO(s3drs.tabs.synID,FIND_BY_UNAME='syninpos1')
  WIDGET_CONTROL,wid,SET_VALUE=n2
  wid=WIDGET_INFO(s3drs.tabs.synID,FIND_BY_UNAME='syninpos2')
  WIDGET_CONTROL,wid,SET_VALUE=n2
  wid=WIDGET_INFO(s3drs.tabs.synID,FIND_BY_UNAME='syninpos3')
  WIDGET_CONTROL,wid,SET_VALUE=n2

  ; also toggle screen
  ignore=s3drs_synoff(event)

END
