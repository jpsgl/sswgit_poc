;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs_text_links, s3drs_text_intro, s3drs_text_ack,
; s3drs_text_license, s3drs_text
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: routines to populate variable GUI messages, put here
; for clarity since they are mostly paragraphs of readable text.
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : none
;               
; Outputs   : returns the appropriate text
;
; Keywords  : 
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

FUNCTION s3drs_text_banner
    push,introtxt,"Welcome to the STEREO 3D Reconstruction Studio (s3drs)"
    push,introtxt,""
    return,introtxt
END

FUNCTION s3drs_text_credits,introflag

  if (introflag) then push,introtxt,s3drs_text_banner() ; for the pop-up widget

  push,introtxt,"Our tomographic reconstruction engine includes the PIXON method (see Puetter, Gosnell, and Yahil 2005, Ann. Rev. Astr. Astrophys., 43, 139-194), under license from Pixon LLC. This software may only be used under the terms of the free Personal Pixon[R] Single-User License. The development work for the white light coronagraph reconstruction application was funded by NASA and by the NRC supporting Alex Antunes at NRL."
  return,introtxt

END

FUNCTION s3drs_text_fmwarn

  push,fmtxt,"The Forward Modeling reconstruction task cannot be stopped,"
  push,fmtxt,"except by exiting the IDL session before it completes or"
  push,fmtxt,"by reaching the selected number of iterations."
  push,fmtxt,"Type 's3drs_restore' followed by 'retall' if it exits early"
  push,fmtxt,"to return to this session."

  return,fmtxt

END


FUNCTION s3drs_text_ack,pixon

  pixon=KEYWORD_SET(pixon)

  ; note fork here depending on whether Pixon was involved.
  if (pixon) then $
    push,acktxt,'If results from this analysis tool are used in a publication we ask for the following, or similar, acknowledgment: "This paper uses results produced by the NRL STEREO 3D Reconstruction Studio software tool, obtainable from the Solar Software Library, that includes a free personal Pixon[R] license from Pixon LLC."' else $
    push,acktxt,'If results from this analysis tool are used in a publication we ask for the following, or similar, acknowledgment: "This paper uses results produced by the NRL STEREO 3D Reconstruction Studio software tool, obtainable from the Solar Software Library."'

  push,acktxt,""
  push,acktxt,"Please email a copy of the publication to Pixon LLC (info@pixon.com) and to the Solar Physics Branch at NRL (stereo_3d@nrl.navy.mil)."
  push,acktxt,""

  return,acktxt

END

FUNCTION s3drs_text_links

;  push,linktxt,"Documentation is at http://secchi.nrl.navy.mil/wiki"
;  push,linktxt,"The SECCHI Wiki is at http://secchi.nrl.navy.mil/wiki/"
;  push,linktxt,"The full SECCHI Website is at http://secchi.nrl.navy.mil"
  push,linktxt,"Documentation is available at the SECCHI Wiki, http://secchi.nrl.navy.mil/wiki"
  push,linktxt,"Limited support is available from stereo_3d@nrl.navy.mil."
  
  return,linktxt
END

FUNCTION s3drs_text_new

  push,introtxt,"Welcome to the STEREO 3D Reconstruction Studio.  Both synthetic observations, and observations from the SECCHI Cor 1 and Cor 2 instruments onboard the twin STEREO spacecraft and from the LASCO C2 and C3 instruments onboard the SOHO spacecraft, can be employed.  Our tomographic reconstruction engine uses the PIXON method developed by Puetter and Yahil (1999, in Astronomical Data Analysis Software and Systems VIII, ASP Conference Series 172, 307-316), under license from Pixon LLC.  This software should not be used for purposes other than with this analysis tool.  The development work for the white light coronagraph reconstruction application was funded by NASA and by the NRC."

  push,introtxt,"As a courtesy we ask that when you first install our software from the Solar Soft Library that you notify both Pixon LLC at amos.yahil@gmail.com, and also the Solar Physics Branch at NRL at stereo_3d@nrl.navy.mil."

  push,introtxt,"Finally, if results from this analysis tool are used in a journal paper we ask for the following, or similar, acknowledgment:  'This paper uses results produced by the NRL STEREO 3D Reconstruction Studio software tool, obtainable from the Solar Software Library.'"

  return,introtxt

END

FUNCTION s3drs_text_intro

  push,introtxt,"Welcome to the STEREO 3D Reconstruction Studio"
  push,introtxt,""
  push,introtxt,"The S3DRS package sets up and then solves 3D reconstruction problems.  It ingests STEREO data and FITS files to extract the a) geometry and b) 2D data, aligns them in 3D space, then applies a choice of algorithms to reconstruct using inversion or forward modeling.  Algorithms include the proprietary Pixon method, licensed for use with S3DRS.  The output is the 3D density cube solution (in FITS format) that can best reproduce the observed images.  Everything is controlled via a GUI, invoked by typing 's3drs'."
  push,introtxt,""

  introtxt=[introtxt,s3drs_text_credits(0)]
  push,introtxt,""

  introtxt=[introtxt,s3drs_text_links()]

  push,introtxt,"If this program experiences errors, type 's3drs_restore' followed by 'retall' to return to the GUI."

  return, introtxt

  ; earlier version

  push,introtxt,"   3D Reconstruction finds the density distribution that best reproduces the observed data. Solutions are typically non-unique, but the more distinct views from different angles you use, the greater the confidence in your solution. Inversion is sensitive to the signal-to-noise ratio, and typically requires an S/N > 10."
  push,introtxt,""
  push,introtxt,"   Data are satellite observations, synthetic data, or a mix. An Image is the solution 3D density cube."
  push,introtxt,""
  push,introtxt,"   Methods include Pixon Tomography, Forward Modeling, and backprojection minimization. Pixon is a proprietary non-parametric tomographic method to find the  least complicated underlying distribution. Forward modeling rotates and resizes a chosen shape to reproduce the data. (Note that due to the IDL 'powell' algorithm, FM runtimes are lengthy.)  Backprojection minimization is a one-pass method to define the maximum spatial extent of the distribution, typically used to get a 1st order solution."
  push,introtxt,""
  push,introtxt,"   For each step, we show the data as currently pre-processed according to the parameters you chose. On the final visualization page, we also include orthogonal X/Y/Z mass projections of the solution. We use the Heliospheric Aries Ecliptic (HAE) reference coordinates, which is a fixed reference plane similar to the stereo satellite plane, where z is orthogonal to the ecliptic plane and X=0 is the first point of Aries. "
  push,introtxt,""
  push,introtxt,"   At the bottom of each screen is a button near an arrow, which will take you to the next step. You can go back to any step at any time."
  push,introtxt,"   1) select either real or synthetic data"
  push,introtxt,"   2) define the data (real: by date and instrument, synthetic: choose positions and instruments)"
  push,introtxt,"   3) (optional) add additional masking to focus on a specific region"
  push,introtxt,"   4) (optional, recommended) decrease the noise to artificially improve the S/N ratio for Pixon gradient searches"
  push,introtxt,"   5) Choose a reconstruction method, run it, then look at the visualizations"
  push,introtxt,"The most recent solution is autosaved as 's3drs_latest.sav' (inversion) or 's3drs_latestFM.sav' (FM). These saves hold the structure containing all run-time choices plus input data and solution."
  push,introtxt,"We also auto-save the solution image in FITS format as 's3drs_latest.fts.'
  push,introtxt,""
  introtxt=[introtxt,s3drs_text_links()]
  return,introtxt
END

FUNCTION s3drs_text_license
  push,lictxt,"                        ******** WARNING ********"

  push,lictxt,"You must accept the Pixon[R] license if you wish to use the Pixon[R] method."
  push,lictxt,"You can use S3DRS without Pixon[R] for forward modeling and backprojection."
  push,lictxt,"You can also modify S3DRS to use your own 3D minimization routine."
  push,lictxt,"Further details are in our documentation at http://secchi.nrl.navy.mil/wiki/"
  push,lictxt,""
  push,lictxt,"Please exit and restart 's3drs', and accept the Pixon license terms at startup."

  return,lictxt

END

FUNCTION s3drs_text_pixon

  push,lictxt,"As a courtesy we ask that when you first install our software from the Solar Software Library that you notify both Pixon LLC at amos.yahil@gmail.com, and also the Solar Physics Branch at NRL at stereo_3d@nrl.navy.mil"
  push,lictxt,""

  push,lictxt,""

  push,lictxt,"FREE PERSONAL PIXON[R] SINGLE-USER LICENSE"
  push,lictxt,"FOR RESEARCH AND ACADEMIC PURPOSES"

  push,lictxt,""
  push,lictxt,"The following license terms and the Disclaimer of Warranty, below, apply to the free, personal use of the Pixon[R] method and the Pixon[R] software by a single user."

  push,lictxt,""
  push,lictxt,"Limited License"

  push,lictxt,""
  push,lictxt,"By using the Pixon[R] software online or downloading it, you acquire a Personal Pixon[R] Single-User License ('the License').  This License lets you personally use the Pixon[R] software ('the Licensed Software') on any number of computers, but only on one computer at a time.  You are not permitted to allow anyone else to access or use the Licensed Software.  You may, under this License, transfer precompiled, executable applications incorporating the Licensed Software to other, unlicensed, persons, providing that (i) the application is noncommercial (i.e., does not involve the selling or licensing of the application for a fee), and (ii) the application was first developed, compiled, and successfully run by you, and (iii) the Licensed Software is bound into the application in such a manner that it cannot be accessed as individual routines and cannot practicably be unbound and used in other programs.  That is, under this License, your application user must not be able to use the Licensed Software as part of a program library or 'mix and match' workbench."
 
  push,lictxt,""
  push,lictxt,"Acknowledgement"

  push,lictxt,""
  push,lictxt,"You are requested to acknowledge use of the Licensed Software in any publication based on it, e.g., 'this research was supported in part by a free personal Pixon[R] license from Pixon[R]LLC', and e-mail a copy of the publication to Pixon[R]LLC."

  push,lictxt,""
  push,lictxt,"Commercial Use"

  push,lictxt,""
  push,lictxt,"The License specifically excludes any commercial use.  Businesses and other organizations may purchase licenses for commercial use of the Pixon[R] method from Pixon[R]LLC under terms to be negotiated."

  push,lictxt,""
  push,lictxt,"Ownership"

  push,lictxt,""
  push,lictxt,"Except as expressly licensed in this License, Pixon[R]LLC shall retain full, only and exclusive title to the Licensed Software, and any upgrades and modifications created in respect thereof by any party whatsoever."

  push,lictxt,""
  push,lictxt,"Support"

  push,lictxt,""
  push,lictxt,"Pixon[R]LLC shall have no obligation to offer you support services, and nothing contained herein shall be interpreted as to require Pixon[R]LLC to provide maintenance, installation services, version updates, debugging, consultation or end-user support of any kind."
  push,lictxt,"Disclaimer of Warranty"

  push,lictxt,""
  push,lictxt,"Pixon[R]LLC makes no warranties, express or implied, that the programs contained in the Licensed Software are free of error, are consistent with any particular standard of merchantability, or will meet your requirements for any particular application.  They should not be relied on for solving a problem whose incorrect solution could result in injury to a person or loss of property.  If you do use the programs in such a manner, it is at your own risk.  Pixon[R]LLC disclaims all liability for direct or consequential damages resulting from your use of the programs."

  push,lictxt,""
  push,lictxt,"Term"
  
  push,lictxt,""
  push,lictxt,"This License and the license rights granted herein shall become effective as of the date you first use the Licensed Software online or download it and shall be perpetual unless terminated in accordance with this Section. "
  
  push,lictxt,""
  push,lictxt,"Each of the parties shall have the right, at any time, to terminate this License without cause by written notice to the other party specifying the date of termination. "
  
  push,lictxt,""
  push,lictxt,"Upon termination, you shall destroy all full and partial copies of the Licensed Software."
  
  push,lictxt,""
  push,lictxt,"Governing Law"
  
  push,lictxt,""
  push,lictxt,"This License shall be construed in accordance with the laws of the State of New York. "
  
  push,lictxt,""
  push,lictxt,"General"
  
  push,lictxt,""
  push,lictxt,"The parties agree that this License is the complete and exclusive agreement between the parties and supersedes all proposals and prior agreements whether written or oral, and all other communications between the parties relating to the subject matter of this Software License. No modification to this Software License shall be valid unless it has been done in writing and signed by both parties. Failure by either party at any time to enforce any of the provisions of this Software License shall not constitute a waiver by such party of such provision nor in any way affect the validity of this Software License."

  return,lictxt

  push,lictxt,""
  push,lictxt,"Do you accept these terms?"

  return,lictxt

END

PRO s3drs_text
  ; just here to force compiles
END
