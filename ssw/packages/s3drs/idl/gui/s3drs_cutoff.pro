PRO s3drs_cutoff,dsptr,masks
;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs_cutoff
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: gathers state info from cutoff widgets
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : pointer to pxwrapper structure, optional masks
;               
; Outputs   : masks (altered)
;
; Keywords  : 
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            


  cutlow=(widget_shorthand('cutlowtoggle',/DROPLIST)).index
  cuthigh=(widget_shorthand('cuthightoggle',/DROPLIST)).index
  masktype=(widget_shorthand('masktype',/DROPLIST)).index

  if (cutlow or cuthigh or masktype) then begin
      ; something changes
      datamod=*dsptr; copy it here
      
      if (cutlow) then begin
          cutoff = (widget_shorthand('cutlow')).value
          ;print,'cutting low ',cutoff
          ie=where(datamod lt cutoff)
          if (ie[0] ne -1) then datamod[ie]=0
      end
      if (cuthigh) then begin
          ;print,'cutting high'
          cutoff = (widget_shorthand('cuthigh')).value
          ;print,'cutting high ',cutoff
          ie=where(datamod gt cutoff)
          if (ie[0] ne -1) then datamod[ie]=0
      end

      if (masktype) then begin
      ; zero data instead of using proper masks, allowed but not recommended
          ;print,'Note, zeroing data instead of masking'
          datamod = datamod * masks ; zero things out
          masks=masks*0 + 1     ; masks are now flat
      end

      dsptr=ptr_new(datamod)
  end


END
