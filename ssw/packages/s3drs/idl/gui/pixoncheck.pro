;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : pixoncheck
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: finds Pixon source or .sav, if it exists, and loads it
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 
;
; Keywords  : force=force (to force a reload even if it already exists)
;               
; Common    : common pixoncheck,pixoncheck  (to prevent reloading each time)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; looks for the Pixon source anywhere, or alternately
;   for 'pixonlocal.sav' in same directory as this file
; Returns 1 if found, 0 if not found.
;
; Sets a common used only by this routine, to ensure it does not
; multiply reload itself.
;

FUNCTION pixoncheck,force=force

  common pixoncheck,pixoncheck

  force=KEYWORD_SET(force)

  returnstat=0

  ; first, see if we already loaded this
 
  if (force eq 0 and n_elements(pixoncheck) ne 0) then begin
      ; already loaded
      returnstat=1
  end

  if (returnstat eq 0) then begin

    ; next, check for IDL Pixon

      ; 1) look for Pixon sourcecode

      PIXONCHECK='pxnlicense__define.pro'
      which,PIXONCHECK,outfile=outfile,/quiet
      if (outfile ne '') then begin
          returnstat=1
      end

  end

  if (returnstat eq 0) then begin

      ; 2) check for compiled version

      testfile='pixon_is_here'; this stub is in the same dir as the library

      which,testfile,outfile=outfile,/quiet
      if (outfile ne '') then begin

          ; found this routine, check for library
          mylib=file_dirname(outfile) + '/'
          myfile=mylib+'pixonlocal.sav'

          if (file_exist(myfile)) then begin
              restore,myfile,/verbose
              returnstat=1
          end

      end

  end

  if (returnstat eq 0) then begin

     ; 3) final try, a hard-coded path relative to this pixoncheck.pro

      testfile='pixoncheck'     ; this very routine
      which,testfile,outfile=outfile,/quiet
      if (outfile ne '') then begin
          mylib=file_dirname(outfile) + '/../../lib/'
          myfile=mylib+'pixonlocal.sav'
          if (file_exist(myfile)) then begin
              restore,myfile,/verbose
              returnstat=1
          end
      end
      
  end

  if (returnstat eq 1) then pixoncheck=1

  return,returnstat

END
