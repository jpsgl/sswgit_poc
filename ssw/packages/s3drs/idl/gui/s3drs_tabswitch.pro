; Procedure to cycle through the tabs when the user clicks 
; the 'Next' or 'Previous' buttons. 
PRO s3drs_tabswitch, thisTab, numTabs, stash, NEXT=NEXT, PREV=PREV 
;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs_tabswitch
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: handles main GUI state as to which tab to focus on
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : thisTab, numTabs, stash (storage var)
;               
; Outputs   : 
;
; Keywords  : NEXT=NEXT, PREV=PREV (tells where to go)
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

  common s3drs,s3drs

  if (n_elements(stash) eq 0) then $
    WIDGET_CONTROL, s3drs.wTLB, GET_UVALUE=stash 
  if (n_elements(thisTab) eq 0) then $
    thisTab = WIDGET_INFO(stash.TopTab, /TAB_CURRENT) 
  if (n_elements(numTabs) eq 0) then $
    numTabs = WIDGET_INFO(stash.TopTab, /TAB_NUMBER) 

 
  ; If user clicked the 'Next' button, we can just add one to 
  ; the current tab number and use the MOD operator to cycle 
  ; back to the first tab. 
  IF KEYWORD_SET(NEXT) THEN nextTab = (thisTab + 1) MOD numTabs 
 
  ; If the user clicked the 'Previous' button, we must explicitly 
  ; handle the case when the user is on the first tab. 
  IF KEYWORD_SET(PREV) THEN BEGIN 
    IF (thisTab EQ 0) THEN BEGIN 
      nextTab = numTabs - 1 
    ENDIF ELSE BEGIN 
      nextTab = (thisTab - 1) 
    ENDELSE 
  ENDIF 
 
; Display the selected tab. 
  WIDGET_CONTROL, stash.TopTab, SET_TAB_CURRENT=nextTab 
 
END 
 
