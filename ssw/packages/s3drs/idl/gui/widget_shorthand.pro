;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : widget_shorthand
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: syntax sugar wrapper for widgets to return standardized
; structures across different widget types.
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : uname
;               
; Outputs   : returns the value and field
;
; Keywords  : droplist=droplist, modellist=modellist, date=date,
; modellist=modelist (widget types),
; perframe=perframe (for multiple widgets), nonzero=nozero (flag for
; widgets that might otherwise allow returning zero)
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; syntax sugar to get values from any uname that accepts 'get_value'
; such as CW_BGROUP, 
; for WIDGET_DROPLIST, use the /droplist flag
; for data widgets, use the /date flag
; for widgets that are per frame, add /perframe to get back as an array

FUNCTION widget_shorthand,uname,droplist=droplist,date=date,perframe=perframe,$
                          nonzero=nonzero,modellist=modellist

  common s3drs,s3drs

  droplist=KEYWORD_SET(droplist)
  modellist=KEYWORD_SET(modellist)
  date=KEYWORD_SET(date)
  perframe=KEYWORD_SET(perframe)
  nonzero=KEYWORD_SET(nonzero)

  if (perframe) then begin

      ; recursively fetch these
      for i=0,s3drs.Nframes-1 do begin
          push,retval,$
            widget_shorthand(uname+int2str(i),droplist=droplist,date=date)
      end

  end else begin

      wid=WIDGET_INFO(s3drs.wTLB,FIND_BY_UNAME=uname)

      if (wid eq 0) then begin
          ; could not find, ignore

      end else if (droplist) then begin

          retN=WIDGET_INFO(wid,/DROPLIST_SELECT)
          WIDGET_CONTROL,wid,GET_VALUE=retV
          ; return structure with index and text explanation
          retval={index:retN,name:retV[retN]}
          
      end else if (modellist) then begin

          retN=WIDGET_INFO(wid,/LIST_SELECT)
          if (retN lt 0) then retN=0; force first option if none selected
          WIDGET_CONTROL,wid,GET_UVALUE=retU
          ; in modellists, first element is actually name
          retU=retU[1:*]
          ; return structure with index and text explanation
          if (retN ge 0 and retN lt n_elements(retU)) then $
            retV=retU[retN] else retV='none'
;;          retval={index:retN,name:retV[retN]}
          retval={index:retN,name:retV}

      end else if (date) then begin

          retval=widget_date_fetch(s3drs.wTLB,uname)

      end else begin

          WIDGET_CONTROL,wid,GET_VALUE=retV
          WIDGET_CONTROL,wid,GET_UVALUE=retU
          if (n_elements(retU) eq 0) then retU=retV; unames were undefined
          if (n_elements(retV) gt 1 and nonzero) then begin
            ; multi-items like CW_BGROUP can return only nonzeros if asked
              for i=0,n_elements(retV)-1 do $
                if (retV[i] ne 0) then push,retval,{value:retV[i],name:retU[i]}
              if (n_elements(retval) eq 0) then retval=-1
          end else begin
              retval={value:retV,name:retU}
          end

      end

  end

  if (n_elements(retval) eq 0) then retval=-1; general failsafe
  return,retval

END
