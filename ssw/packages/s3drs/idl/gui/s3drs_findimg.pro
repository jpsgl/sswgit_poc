;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs_findimg
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: general utility that, given a filename, finds any 3D
; item in it
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : filename
;               
; Outputs   : returns an image
;
; Keywords  : 
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI, 3D, densitycube, i/o
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
FUNCTION findimgin,fname

  ; finds a 3-D item in the given file and returns it

  retdata=-1; assume failure

  if (is_fits(fname)) then begin

      retdata=readfits(fname)

  end else if (is_idlsave(fname)) then begin

      sobj=obj_new('IDL_Savefile',fname)

      if (sobj) then begin

          names=sobj->names()
          idata=-1
          for i=0,n_elements(names)-1 do $
            if ((sobj->size(names[i]))[0] eq 3) then idata=i

          if (idata ne -1) then begin
              sobj->restore,names[idata]
              returncode=execute('retdata='+names[idata])
              obj_destroy,sobj
          ; retdata now exists
          end

      end

  end else begin

      INFOWIDGET,'Is not an IDL Save or FITS file, ending.'

  end

  return,retdata

END

FUNCTION s3drs_findimg,uname

  filename = DIALOG_PICKFILE(/MUST_EXIST,/READ,$
                             FILTER=['*.SAV','*.Sav','*.sav',$
                                     '*.fts','*.FTS','*.FITS','*.fits',$
                                    '*.Fts','*.Fits'])

  if (filename ne "") then begin

      timage=findimgin(filename)

      if (n_elements(timage) eq 1) then begin
                                ; failed
          INFOWIDGET,'Could not find an imagecube in that file, sorry.'
          if (n_elements(uname) ne 0) then begin
              jid=WIDGET_INFO(s3drs.wTLB,find_by_uname=uname)
              WIDGET_CONTROL,jid,SET_DROPLIST_SELECT=0
          end
          timage=-1
      end

  end else begin
      INFOWIDGET,'Okay, cancelling file load.'
      if (n_elements(uname) ne 0) then begin
          jid=WIDGET_INFO(s3drs.wTLB,find_by_uname=uname)
          WIDGET_CONTROL,jid,SET_DROPLIST_SELECT=0
      end
      timage=-1
  end

  return,timage

END
