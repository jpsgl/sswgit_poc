;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs_tv_report, s3drs_tv
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: wrappers for plotting data intelligently into
; predefined GUI windows
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : event, uname, title
;               
; Outputs   : changes underlying state
;
; Keywords  : rows=rows
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; wrapper for draw + plotting limits

FUNCTION s3drs_tv_replot,event

  common s3drs,s3drs

  if (DATATYPE(event) eq 'STC') then begin
      ; we got an event, get a uname
      WIDGET_CONTROL,event.id,GET_UVALUE=uname
  end else begin
      ; it is a uname (manually called)
      uname=event
      ; check to make sure it exists
      id=WIDGET_INFO(s3drs.wTLB,FIND_BY_UNAME=uname)
      if (id eq 0) then return,0
  end

  ;print,'processing uname ',uname,' ,',s3drs.choice

  if (ptr_valid(s3drs.mset2) eq 0) then mset2=1.0 else mset2=(*s3drs.mset2)
  masks = (*s3drs.mset) * mset2

  if (s3drs.choice eq 'synthetic') then dsptr=s3drs.syn else dsptr=s3drs.diff
  if (ptr_valid(dsptr) eq 0) then return,-1

  s3drs_cutoff,dsptr,masks
  ;print,'uname is ',uname
  CASE strupcase(uname) of
      'DATA1': data=(*s3drs.diff) * (*s3drs.mset); only original masks
      'SYN1': data=(*s3drs.syn) * (*s3drs.mset); only original masks
      'INPUT1': data=(*dsptr) * masks
      'MASK1': data = masks
      'SNPLOT1': begin
          noise1 = *s3drs.ndiff
          for iv=0,s3drs.nFrames-1 do noise1[*,*,iv] /= (*s3drs.snratio)[iv]
          data = (ratio(*dsptr,noise1)*masks)>1.0<100.0
      end
      'VIZDATA': begin
          data=(*dsptr) * masks
          render=(*s3drs.render) * masks
          data=[[[render]],[[data]]]
          linebreak=s3drs.Nframes
      end
;      'VizRender': data=(*s3drs.render) * masks
      'FMPLOT1': begin
          data=(*dsptr) * masks
          render=(*s3drs.render) * masks
          data=[[[render]],[[data]]]
          linebreak=s3drs.Nframes
      end
      'TRIO': begin
          data=*s3drs.trio
          lset=['X','Y','Z']
          if (s3drs.choice eq 'synthetic') then begin
              threeview,/log,*s3drs.timage,/pixon,/noplot,d2
              ;d2=*s3drs.trio
              data=[[[data]],[[d2]]]
              linebreak=3
          end
      end
      'CHISQ': data= RATIO((*dsptr)-(*s3drs.render),*s3drs.ndiff) * masks
      else: data=masks
  ENDCASE

  s3drs_floodfill,uname,data,lset=lset,linebreak=linebreak

  ; return success
  return,1

END

FUNCTION s3drs_tv,base,uname,title,rows=rows,double=double

  common s3drs,s3drs

  double=KEYWORD_SET(double)

  if (n_elements(title) eq 0) then title=''
  ; guess at initial size, dynamically adjust scrollbars later

  subbase = WIDGET_BASE(base,ROW=1)

  wtitle = WIDGET_LABEL(subbase,VALUE=title)

  wloglin = CW_BGROUP(subbase,['linear','log'],$
                      uname=uname+'loglin',UVALUE=uname,/ROW,/EXCLUSIVE,$
                      EVENT_FUNC='s3drs_tv_replot')
  wentry = CW_FIELD(subbase,/FLOAT,TITLE='Min:',XSIZE=11,VALUE='0',$
                    uname=uname+'min',UVALUE=uname,$
                    EVENT_FUNC='s3drs_tv_replot')
  wentry = CW_FIELD(subbase,/FLOAT,TITLE='Max:',XSIZE=11,VALUE='0',$
                    uname=uname+'max',UVALUE=uname,$
                    EVENT_FUNC='s3drs_tv_replot')
  wbutton = WIDGET_BUTTON(subbase,VALUE='Replot',UVALUE=uname,$
                          EVENT_FUNC='s3drs_tv_replot')

  if (n_elements(rows) eq 0) then rows=1
  ysize=s3drs.drawy*rows
  if (double) then ysize=ysize*2

  wPlot3 = WIDGET_DRAW(base,RETAIN=2,/SCROLL,uname=uname,$
                       XSIZE=s3drs.drawx-50,ysize=ysize,$
                       X_SCROLL_SIZE=s3drs.drawx,$
                       Y_SCROLL_SIZE=ysize-50)

  return,wPlot3

END
