;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs_coversheet, s3drs_satlosview, s3drs_nviewbutton,
; s3drs_dataoff, s3drs_synoff, s3drs_miscevents, s3drs_togglehop
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: various GUI toggles and tools
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (changes GUI state)
;
; Keywords  : 
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
PRO s3drs_coversheet

  common s3drs,s3drs

  if (PTR_VALID(s3drs.pxptr) eq 0) then return; only print if populated

  infobox = WIDGET_INFO(s3drs.tabs.vizID,FIND_BY_UNAME='coversheet')
  if (infobox ne 0) then begin
      p_report,*(s3drs.pxptr),/nosave,returntext=caption,noises=*s3drs.snratio
      WIDGET_CONTROL,infobox,SET_VALUE=caption
  end

END

; update all position plots

PRO s3drs_satlosview

  common s3drs,s3drs

  ; check each tab for this item, if found fill it
  if (PTR_EXIST(s3drs.pxptr) eq 0) then return; only go if data is valid

  for it=0,n_elements(tag_names(s3drs.tabs))-1 do begin

      draw_id = WIDGET_INFO(s3drs.tabs.(it),FIND_BY_UNAME='satlos')

      ;print,'draw id is ',draw_id,' for tab ',s3drs.tabs.(it)
      if (draw_id ne 0) then begin
          rho=(*s3drs.pxptr).sr.rho
          LL=(*s3drs.pxptr).sr.LL
          phi=(*s3drs.pxptr).sr.phi
          lset=int2str(make_array(/long,/index,n_elements(rho))+1)
          ;print,rho,phi,LL,lset
          sat_los_view,rho,LL,phiset=phi,viewport=draw_id,labelset=lset
      end

  end

END

; extra button to add an event
PRO s3drs_nviewbutton,event
  common s3drs,s3drs

  numwin=WIDGET_INFO(s3drs.wTLB,FIND_BY_UNAME='nviewpoints')

  nviewpoints=(widget_shorthand('nviewpoints')).value
  ++nviewpoints
  WIDGET_CONTROL,numwin,SET_VALUE=nviewpoints

  s3drs_synspec

END

FUNCTION s3drs_dataoff,event
  ; if you change a tab_data item, this turns off subsequent tabs
  ; until such time as you regenerate the appropriate data.

  common s3drs,s3drs

  warnid=WIDGET_INFO(s3drs.tabs.dataID,FIND_BY_UNAME='datawarn')
  WIDGET_CONTROL,warnid,SET_VALUE='Generate these data'

  WIDGET_CONTROL,s3drs.tabs.maskID,SENSITIVE=0
  WIDGET_CONTROL,s3drs.tabs.noiseID,SENSITIVE=0
  WIDGET_CONTROL,s3drs.tabs.reconID,SENSITIVE=0
  WIDGET_CONTROL,s3drs.tabs.vizID,SENSITIVE=0

END

FUNCTION s3drs_synoff,event
  ; if you change a tab_syn item, this turns off subsequent tabs
  ; until such time as you regenerate the appropriate data.

  common s3drs,s3drs

  
  checkme=WIDGET_INFO(s3drs.tabs.synID,/ACTIVE)
  if (checkme eq 0) then return,-1

  warnid=WIDGET_INFO(s3drs.tabs.synID,FIND_BY_UNAME='synwarn')
  if (warnid gt 0) then $
    WIDGET_CONTROL,warnid,SET_VALUE='Generate these data'

  WIDGET_CONTROL,s3drs.tabs.maskID,SENSITIVE=0
  WIDGET_CONTROL,s3drs.tabs.noiseID,SENSITIVE=0
  WIDGET_CONTROL,s3drs.tabs.reconID,SENSITIVE=0
  WIDGET_CONTROL,s3drs.tabs.vizID,SENSITIVE=0

END

FUNCTION s3drs_togglehop,ev
   common s3drs,s3drs

   s3drs.tabhop =~ s3drs.tabhop
   if (s3drs.tabhop eq 1) then $
     WIDGET_CONTROL,ev.id,SET_VALUE='turn off tab hopping' else $
     WIDGET_CONTROL,ev.id,SET_VALUE='turn on tab hopping'

END

PRO s3drs_miscevents
  ; just a stub for initializing
END
