; plots CM of solution

PRO s3drs_cm,ev

  common s3drs,s3drs

  if (PTR_VALID(s3drs.pxptr) and PTR_VALID(s3drs.solution)) then begin

      pxwrapper=*(s3drs.pxptr)
      img=*(s3drs.solution)

      m=p_getmask('COR2',pxwrapper.sr.ncube,rpixels=rpixels)
      mask3D = cube_sphere(pxwrapper.sr.ncube,rpixels[0],r_outer=rpixels[1])
      img=img*mask3D

      c3d= (centroid3d(img)-pxwrapper.sr.lla0)*pxwrapper.sr.dvox
      r_out_3d=sqrt(total(c3d^2))

      push,cmtxt,'center of mass in HAE x/y/z, units of Rsun:'
      push,cmtxt,strjoin(string(c3d),',')
      push,cmtxt,'radial distance to center-of-mass: '+string(r_out_3d)+' Rsun'

      INFOWIDGET,cmtxt

  end

END
