;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : quartermasks, s3drs_m2, s3drs_fillmask, s3drs_masks
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: tools to manipulate masks within GUI and data
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : (various)
;               
; Outputs   : (alters underlying s3drs structure)
;
; Keywords  : 
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
FUNCTION quartermasks,N,concave,direction

  ; right=1,left=2,top=3,bottom=4

  tempmask=bytarr(N,N) + 1
  if (concave) then begin
      ;print,N,direction
      tempmask *= p_getmask('corners',N,offshift=direction,rocc=0)
  end else begin
      case direction of
          1 : tempmask[N/2:*,*]= 0
          2 : tempmask[0:N/2,*]=0
          3 : tempmask[*,N/2:*]=0
          4 : tempmask[*,0:N/2]=0
      end
  end

  return,tempmask

END

; if any of a dataid's mset2 are changed, fetch all the relevant details
;
FUNCTION s3drs_m2,event

  common s3drs,s3drs

  WIDGET_CONTROL,event.id,GET_UVALUE=dataid

  concavewid=WIDGET_INFO(event.top,$
                         FIND_BY_UNAME='concave'+int2str(dataid))

  regionswid=WIDGET_INFO(event.top,$
                         FIND_BY_UNAME='regions'+int2str(dataid))

  ; right=1,left=2,top=3,bottom=4
  WIDGET_CONTROL,regionswid,GET_VALUE=values
  WIDGET_CONTROL,concavewid,GET_VALUE=concavity

  mask=bytarr(s3drs.Nd,s3drs.Nd) + 1
  concave=concavity[0]
  print,s3drs.Nd
  for id=0,n_elements(values)-1 do begin
      if (values[id] eq 1) then $
        mask *= quartermasks(s3drs.Nd,concave,id+1)
  end

  ;tv_multi,mask,min=0.0,max=1.0; just checking
  ;if (event.select eq 1) then str='adding ' else str='subtracting '
  ;str += 'choice='+int2str(event.value)
  ;str += ' for dataframe '+int2str(dataid)
  ;print,str

  ; now update and replaec
  if (PTR_VALID(s3drs.mset2)) then begin
      mset2=*s3drs.mset2
      PTR_FREE,s3drs.mset2
  end else begin
      mset2=bytarr(s3drs.Nd,s3drs.Nd,s3drs.Nframes)+1
  end
  mset2[*,*,dataid]=mask
  s3drs.mset2=PTR_NEW(mset2)

  ignore=s3drs_tv_replot('Mask1')
  ignore=s3drs_tv_replot('Input1')
  ignore=s3drs_tv_replot('SNPlot1')

  return,0

END


PRO s3drs_fillmask,event
  common s3drs,s3drs

  wTmaskmc=WIDGET_INFO(s3drs.wTLB,FIND_BY_UNAME='multimask')

  ; first clear it out
  wchildren=WIDGET_INFO(wTmaskmc,/ALL_CHILDREN)
  for ic=0,n_elements(wchildren)-1 do begin
      if (wchildren[ic] ne 0) then WIDGET_CONTROL,wchildren[ic],/DESTROY
  end

  bLabel = WIDGET_LABEL(wTmaskmc, VALUE='Add. Masking')

  ; right=1,left=2,top=3,bottom=4
  mtype1=["mask right half","mask left half",$
         "mask upper half","mask lower half"]
  mtype2=["Use concave masks","Add custom regions"]

  uval=make_array(/long,/index,s3drs.Nframes)
  for id=0,s3drs.Nframes-1 do begin
      wTmaskmcsub=WIDGET_BASE(wTmaskmc,COLUMN=1)
      wb1 = WIDGET_BASE(wTmaskmcsub,FRAME=1)
      bgroup = CW_BGROUP(wb1,mtype1,COLUMN=2,EVENT_FUNC='s3drs_m2',$
                         LABEL_TOP=(*s3drs.names)[id],/NONEXCLUSIVE,$
                         UVALUE=id,UNAME='regions'+int2str(id))
      wb2 = WIDGET_BASE(wTmaskmcsub,FRAME=1)
      bgroup = CW_BGROUP(wb2,mtype2,ROW=2,/NONEXCLUSIVE,EVENT_FUNC='s3drs_m2',$
                        UVALUE=id,UNAME='concave'+int2str(id))
  end

END

PRO s3drs_masks
  ; stub just to init this routine
END
