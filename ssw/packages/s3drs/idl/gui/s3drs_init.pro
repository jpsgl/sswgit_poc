;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs_init, also s3drs_arnaud as a converter
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: initializes the common 's3drs' structure for the run
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : none
;               
; Outputs   : none (creates common structure 's3drs')
;
; Keywords  : 
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
FUNCTION s3drs_arnaud,model
  ; Awkward conversion of modelnames here

  IF (stregex(strupcase(model),'ARNAUD',/BOOLEAN)) then begin

      arnaudloc=stregex(strupcase(model),'ARNAUD')
      arnaudend=stregex(strupcase(model),')')
      modelid=long(strmid(model,arnaudloc+7,arnaudend-arnaudloc-7))

  END ELSE BEGIN

      modelid=-1

  END

  return,modelid

END

PRO s3drs_init
  ; initialize global structure
  common s3drs,s3drs

  title='S3DRS'
  wrap=1; set to 1 to allow tab navigation 'round the world'
  res=[64,128,256]
  pol=['tB','pB','rad+tang pairs','trios']
  instr=p_instrspec('name',/s3drs)
  models=['half-shell',$
          'Chen fluxrope',$
          'Saito',$
          'Spherical shell',$
          'grad. cylinder (Arnaud 33)',$
          'single lit pixel',$
          'lit center region',$
          'image cube from file']
  ; now add in all the Arnaud models
  for i=1,68 do begin
      imod=i
      ; not all Arnaud models are parameterized or working
      if (i ne 32 and i ne 67 and i ne 25 and i ne 26 and i ne 30 and i ne 35 and i ne 36 and i ne 37 and i ne 40) then begin
          getmoddefparam,imod,ss
          push,models,((ss.s.def)[0])+' (Arnaud '+int2str(imod)+')' 
      end
  end

  winx=640                      ; master size for entire app
  winy=800                      ; master size for entire app

  drawx=580; typical plotting viewport (on-screen size, infinitely scrollable)
  drawy=220; typical plotting viewport (on-screen size, infinitely scrollable)

  Nd=64; initial simulation resolution
  Nframes=0; initially, no frames loaded
  choice='data'; a placeholder

  cmedate=''; string for CME date_time
  priordate=''; string for background method or prior frame date_time
  names=ptr_new(); pointer to eventually store user-chosen names

  snratio=ptr_new(); pointer for variable number of elements

  pxptr=ptr_new(); pointer to for eventual geometry pxwrapper structure
  diff=ptr_new(); pointer to eventual CME data
  ndiff=ptr_new(); pointer to eventual noise data
  syn=ptr_new(); pointer to potential synthetic data
  mset=ptr_new(); pointer to eventual mask data
  mset2=ptr_new(); pointer to eventual overlay mask data
  render=ptr_new(); pointer to eventual mask data
  trio=ptr_new(); pointer to eventual mask data
  objptr=ptr_new(); pointer to eventual pixon solution object
  timage=ptr_new(); pointer to optional synthetic input model
  solution=ptr_new(); pointer to eventual solution image

  tabs={introID:0,dataID:0,synID:0,maskID:0,$
        noiseID:0,reconID:0,fmID:0,vizID:0}
  tabhop=1; initial default is to automatically hop across tabs after actions

  which,'s3drs_init',outfile=outfile,/quiet
  if (outfile ne '') then begin
      ; found logo file's likely directory
      mylogo=file_dirname(outfile) + '/' + 's3drs_logo2.bmp'
      if (file_exist(mylogo)) then $
        logo=TRANSPOSE(READ_BMP(mylogo,/RGB),[1,2,0]) else logo=-1
  end else begin
      logo=-1
  end

  arrow=s3drs_arrow()
  link=1
  s3drs_link,linklogo,unlinklogo
  linklogo=linklogo; "link"
  unlinklogo=unlinklogo; "unlink"

  waiting=0; tracks busywait widget ids, when used

  pixon=0; tracks whether Pixon is available: 0=not installed, -1=not licensed, 1=ready

  s3drs={wTLB:-1,wrap:wrap,res:res,pol:pol,instr:instr,winx:winx,winy:winy,$
         drawx:drawx,drawy:drawy,Nd:Nd,Nframes:Nframes,snratio:snratio,$
         cmedate:cmedate,priordate:priordate,names:names,tabs:tabs,$
         models:models,diff:diff,ndiff:ndiff,mset:mset,mset2:mset2,$
         pxptr:pxptr,objptr:objptr,trio:trio,render:render,syn:syn,$
         choice:choice,logo:logo,solution:solution,pixon:pixon,title:title,$
         arrow:arrow,link:link,linklogo:linklogo,unlinklogo:unlinklogo,$
         timage:timage,waiting:waiting,tabhop:tabhop}

END
