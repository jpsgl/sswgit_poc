; later add in toggles for /saveslides, /log, /morphonorm, ftol

PRO s3drs_fmrun,event
;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs_fmrun
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: Runs the 'forward modeling' reconstruction mode
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : event (not used)
;               
; Outputs   : alters s3drs structure variables
;
; Keywords  : 
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

  common s3drs,s3drs

  model=(widget_shorthand('fminmodel',/MODELLIST)).name
  ; Need some model parsing here
  modelid=s3drs_arnaud(model)

  if (ptr_valid(s3drs.mset2) eq 0) then mset2=1.0 else mset2=(*s3drs.mset2)
  masks = (*s3drs.mset) * mset2

  if (s3drs.choice eq 'synthetic') then dsptr=s3drs.syn else dsptr=s3drs.diff

  s3drs_cutoff,dsptr,masks

  ; FETCH VALUES

  getmoddefparam,modelid,ss
  params=parsemoddefparam(ss)

  toggles=long(params*0 + 1)

  for ip=0,n_elements(params)-1 do begin
      pp=widget_shorthand('fmin'+'p'+int2str(ip))
      if (datatype(pp) eq 'STC') then begin
          ; note above sanity checking in case arnaud changes his s/w
          params[ip]=pp.value
          tt=widget_shorthand('fmin'+'f'+int2str(ip),/DROPLIST)
          toggles[ip]= ~(long(tt.index))
      end
  end

  euler=[(widget_shorthand('fmineuler1')).value,$
         (widget_shorthand('fmineuler2')).value,$
         (widget_shorthand('fmineuler3')).value]

  eulertoggle = ~ long((widget_shorthand('fminefix',/DROPLIST)).index)

  toggles=[eulertoggle,eulertoggle,eulertoggle,toggles]
  ; CALL FM RUNTIME

  s3drs_busy,1,waiting,'Running forward modeling, please wait'

  mxit=(widget_shorthand('mxit')).value

  ; note we can pass pointers to cloud_init just fine
  cloud_init,modelid,px_in=s3drs.pxptr,dat_in=dsptr,$
    sigma_in=s3drs.ndiff,mset_in=masks,mxit=mxit

  win1 = WIDGET_INFO(s3drs.tabs.fmID, FIND_BY_UNAME='FMplot1')
  if (win1 ne 0) then $
    WIDGET_CONTROL,win1,GET_VALUE=index1

  cloud_run,initial_rot=euler,parent=index1,$
    params=params,toggles=toggles,$
    answer=answer;,/saveslides

  if (n_elements(answer) eq 0) then begin

      INFOWIDGET,'Forward model failed, please adjust and try again.'

  end else begin
      ; it worked!

  ;answer = {bestrot,modparam,iterations,image,fmin}

  ; STORE ANSWERS

      threeview,/log,answer.image,/pixon,/noplot,trio

      if (PTR_VALID(s3drs.solution)) then ptr_free,s3drs.solution
      s3drs.solution=ptr_new(answer.image)

      if (PTR_VALID(s3drs.render)) then ptr_free,s3drs.render
      s3drs.render=ptr_new(pr_render(*s3drs.pxptr,answer.image))

      if (PTR_VALID(s3drs.trio)) then ptr_free,s3drs.trio
      s3drs.trio=ptr_new(trio)


  ; PLOT THE SOLUTIONS

      ignore=s3drs_tv_replot('VizData')
;      ignore=s3drs_tv_replot('VizRender')
      ignore=s3drs_tv_replot('Trio')
      ignore=s3drs_tv_replot('Chisq')
      ignore=s3drs_tv_replot('FMplot1')

  ; update details
      s3drs_coversheet

  ; also move to viz tab
      if (s3drs.tabhop) then $
        s3drs_tabhop            ; move to viz

  ; and save, just for safety
      save,s3drs,file='s3drs_latestFM.sav'
      densitycube,'init'
      cubestructure,cubeinfo,'pack',N=s3drs.Nd,desc='3D reconstruction',$
        author='S3DRS',program='S3DRS',obsdate=s3drs.cmedate
      densitycube,'save','s3drs_latest.fts',solution_image,cubeinfo

  end

  ; AND RETURN CONTROL TO THE USER

  s3drs_busy,0,waiting
  WIDGET_CONTROL,s3drs.tabs.vizID,SENSITIVE=1

END
