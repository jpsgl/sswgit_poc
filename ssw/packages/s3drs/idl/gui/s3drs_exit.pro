FUNCTION s3drs_exit,ev
;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs_exit
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: cleans up and exits s3drs when asked
;               
; Use       : only called from GUI 
;    
; Inputs    : event
;               
; Outputs   : 
;
; Keywords  : 
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: exits program
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

  common s3drs,s3drs

  check=YES_NO_MENU('Really exit S3DRS?')

  if (check eq 'YES') then begin

    ; record that all tabs are gone
      tabs={introID:0,dataID:0,synID:0,maskID:0,$
            noiseID:0,reconID:0,fmID:0,vizID:0}
      s3drs.tabs=tabs
      WIDGET_CONTROL,s3drs.wTLB,/DESTROY

      return,1

  end else begin
      return,0
  end

END
