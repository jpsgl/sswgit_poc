; populates an S3DRS gui window with the given data

PRO s3drs_floodfill,uname,data,lset=lset,linebreak=linebreak

;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs_floodfill
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: wrapper for tv_multi windows in GUI
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : uname of window to populate, data to fill, optional labels
;               
; Outputs   : (just draws into widget)
;
; Keywords  : 
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

  common s3drs,s3drs

  if (n_elements(uname) eq 0) then uname='Data1'; main data window

  win1 = WIDGET_INFO(s3drs.wTLB, FIND_BY_UNAME=uname)
  WIDGET_CONTROL,win1,GET_VALUE=index1

  ; also see if it is toggled as linear or log, has a datamin or datamax
  temploglin=widget_shorthand(uname+'loglin')
  tempmaxplot=widget_shorthand(uname+'max')
  maxid=WIDGET_INFO(s3drs.wTLB,FIND_BY_UNAME=uname+'max')
  tempminplot=widget_shorthand(uname+'min')
  minid=WIDGET_INFO(s3drs.wTLB,FIND_BY_UNAME=uname+'min')

  loglin=0

  if (DATATYPE(temploglin) eq 'STC') then loglin=temploglin.value
  if (DATATYPE(tempmaxplot) eq 'STC') then $
    if (tempmaxplot.value ne 0) then maxplot=tempmaxplot.value
  if (DATATYPE(tempminplot) eq 'STC') then $
    if (tempminplot.value ne 0) then minplot=tempminplot.value

  if (n_elements(maxplot) ne 0 and n_elements(minplot) ne 0) then $
    if (minplot ge maxplot) then minplot=maxplot/100.0; sanity check
  ; for backtracking later
  if (n_elements(maxplot) eq 0) then setmax=1 else setmax=0
  if (n_elements(minplot) eq 0) then setmin=1 else setmin=0


;  wset,win; not needed, just plot directly with tv_multi
  if (n_elements(data) eq 0) then begin
      ; placeholder/dummy data
      s3drs.Nd=128
      s3drs.Nframes=4
      data=make_array(s3drs.Nd,s3drs.Nd,s3drs.Nframes,/float,/index)
  end

  NFrames=n_elements(data[0,0,*])

  ; resize scroll frame up (but never down) if needed
  newxsize=Nframes*(s3drs.Nd+50)
  newysize=s3drs.Nd+100
  if (newxsize gt s3drs.drawx) then WIDGET_CONTROL,win1,DRAW_XSIZE=newxsize
  ; removed these last line because they were making double-wide windows short
;  if (newysize gt s3drs.drawy) then WIDGET_CONTROL,win1,DRAW_YSIZE=newysize

  if (n_elements(lset) eq 0) then lset=*s3drs.names; the usual items

  tv_multi,fixed=128,data,winid=index1,/slide,labels=lset,linebreak=linebreak,$
    log=loglin,maxval=maxplot,minval=minplot,ymargin=-20,/auto,/squish;,yrow=32

  ;help,setmax,maxid,setmin,minid,maxplot[0]
  ;print,setmax,maxid,(setmax and maxid)
  ;if (setmax and maxid gt 0) then print,'resetting' else print,'skipping'

  ; now update the widget if we had autoscaling
  if (setmax and maxid gt 0) then $
    WIDGET_CONTROL,maxid,SET_VALUE=fsigfig(maxplot[0])
  if (setmin and minid gt 0) then $
    WIDGET_CONTROL,minid,SET_VALUE=fsigfig(minplot[0])

;yrow, ymargin, border, ybuffer

END
