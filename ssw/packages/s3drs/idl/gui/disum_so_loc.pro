;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : disum_so_loc
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: finds where the compiled objects are, used to set path
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : none
;               
; Outputs   : changes 'disumlib' env var
;
; Keywords  : 
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; Simple routine that dynamically checks whether the disum*.so
; libraries are in the runtime path by checking if they are
; in the same directory as 'disum_is_here' or in the default
; location relative to this 'disum_so_loc' procedure.
; If so, sets the "disumlib" environmental variable to them.
;

PRO disum_so_loc,fname=fname

  if (n_elements(fname) eq 0) then fname='disum1.so'

  returnstat=0
 
  ; 2) first try their home directory, which often matches their OS
  if (returnstat eq 0) then begin
      ; last chance, check their home directory
      mylib=getenv('HOME') + '/disum_c/'
      myfile=mylib+fname
      ;print,'Checking home for ',myfile
      if (file_exist(myfile)) then begin
          setenv,'disumlib='+mylib
          returnstat=1
      end
  end

  ; 2) then look to see if lib is in IDL path
  if (returnstat eq 0) then begin
      testfile='disum_is_here'
      which,testfile,outfile=outfile,/quiet
      if (outfile ne '') then begin
          ; found this routine, check for library
          mylib=file_dirname(outfile) + '/'
          myfile=mylib+fname
          if (file_exist(myfile)) then begin
              setenv,'disumlib='+mylib
              returnstat=1
          end
      end
  end

  ; 3) if that doesn't work, try one last path
  if (returnstat eq 0) then begin
      testfile='disum_so_loc'
      which,testfile,outfile=outfile,/quiet
      if (outfile ne '') then begin
          ; found this routine, check for library
          mylib=file_dirname(outfile) + '/../../lib/'
          myfile=mylib+fname
          if (file_exist(myfile)) then begin
              setenv,'disumlib='+mylib
              returnstat=1
          end
      end

  end

END
