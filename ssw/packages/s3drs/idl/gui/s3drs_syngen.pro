;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs_syngen
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: generates input synthetic data
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : event
;               
; Outputs   : changes underlying state
;
; Keywords  : 
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
FUNCTION arnaudme,mid,pxwrapper
  ; little wrapper for Arnaud's model creator
  NL=long(pxwrapper.sr.ndata[0]);   * pxwrapper.sr.l0tau[0]
  rsun=NL * pxwrapper.sr.L0tau[0]
  getmoddefparam,mid,ss
  pset=parsemoddefparam(ss)

  ; fetch user-given widgets
  for ip=0,N_elements(pset)-1 do begin
      pp=widget_shorthand('synin'+'p'+int2str(ip))
      pset[ip]=pp.value
  end

  buildcloud,mid,cubesidenbpix=NL,cubesidersun=rsun,output=1,$
    modparam=pset,/quiet

  return,readcloudcube(mid)
END

PRO s3drs_syngen,event

  common s3drs,s3drs

  exptime=(widget_shorthand('exptime')).value
  if (exptime lt 1) then exptime=1
  nviewpoints=(widget_shorthand('nviewpoints')).value

  if (nviewpoints lt 1) then nviewpoints=1

  euler=[(widget_shorthand('synineuler1')).value,$
         (widget_shorthand('synineuler2')).value,$
         (widget_shorthand('synineuler3')).value]

  model=(widget_shorthand('syninmodel',/MODELLIST)).name

  res=(strsplit((widget_shorthand('synres',/droplist)).name,'x',/extract))[0]

  ; PER-INSTRUMENT VALUES

  ; POLARIZATION
  synpol=widget_shorthand('synpol',/DROPLIST)
  ; index 0=tB, 1=pB, 2=rad+tan, 3=trios
  if (synpol.index eq 3) then trios=1 else trios=0
  if (synpol.index eq 2) then rtpair=1 else rtpair=0
  if (synpol.index eq 1) then pb=1 else pb=0
  radonly=0; choice not available via GUI
  tanonly=0; choice not available via GUI
  indypair=0; choice not available via GUI

  if (rtpair) then iloop=2 else if (trios) then iloop=3 else iloop=1

  for iv=0,nviewpoints-1 do begin

      ; allow creation of polarization pairs and trios here
      for ipc=0,iloop-1 do begin
          push,instr,$
            (widget_shorthand('synspecinstr'+int2str(iv),/DROPLIST)).name
          push,name ,(widget_shorthand('synspecname'+int2str(iv))).value
          push,rho  ,(widget_shorthand('synspecrho'+int2str(iv))).value
          push,phi  ,(widget_shorthand('synspecphi'+int2str(iv))).value
          push,ll   ,(widget_shorthand('synspecll'+int2str(iv))).value
          if (rtpair) then begin
              if (ipc eq 0) then push,polar,'r' else push,polar,'t'
          end else if (trios) then begin
              if (ipc eq 0) then push,polar,'0' else $
                if (ipc eq 1) then push,polar,'120' else push,polar,'240'
          end else begin
              push,polar,'b'
          end
      end

  end
  
  nviewpoints = nviewpoints * iloop

  ; MASK
  specificocculter='ignore'
  specs=widget_shorthand('synmask',/DROPLIST)
  specname=(strsplit(specs.name,' ',/extract))[0]
  specificocculter=strmid(strupcase(specname),0,4)
  if (specificocculter eq 'OTHE') then begin
      INFOWIDGET,'Other instruments not yet implemented'
      specificocculter='ignore'
  end else if (specificocculter eq 'SAME') then begin
      specificocculter='ignore'
  end else if (specificocculter eq 'NONE') then begin
      specificocculter='none'
  end

  ; BACKGROUND MODEL
  ; 0 = none, 1 = Saito, 2 = Gausian, 3 = Poisson sim-photon
  ; aka NONE, SAIT, GAUS, POIS
  bkg=strmid(strupcase((widget_shorthand('synback',/DROPLIST)).name),0,4)

  ; block-and-busy while creating data
  s3drs_busy,1,waiting

  ; register this
  s3drs.choice='synthetic'

  ; placeholders for now
  cor2=1
  cor1=0

  prep_3drv,pxwrapper,N=res,gamma=rho,theta=phi,LL=ll,cor2=cor2,cor1=cor1,$
    polar=polar

  s3drs.Nframes=nviewpoints
  s3drs.Nd=res

  pxwrapper.sr.ch=model
  pxwrapper.sc.gname=model

  modelstr=strmid(strupcase(model),0,4)
  modelid=s3drs_arnaud(model)
  ;print,'modelstr is ',modelstr,' versus id ',modelid,' of type ',datatype(modelid)
  if (modelid ne -1) then begin
      timage=arnaudme(modelid,pxwrapper)
  end else begin
      ; extra cases
      case modelstr of
          'HALF' : timage=pixonmodels(res,'hshell2',ignore,stuff)
          'CHEN' : timage=pixonmodels(res,'fluxrope',ignore,stuff)
          'SAIT' : timage=pixonmodels(res,'saito',ignore,stuff)
          'SPHE' : timage=pixonmodels(res,'shell',ignore,stuff)
          'GRAD' : timage=arnaudme(33,pxwrapper)
          'CROI' : timage=arnaudme(33,pxwrapper)
          'SING' : timage=pixonmodels(res,'testcube2x2',ignore,stuff)
          'LIT ' : timage=pixonmodels(res,'ncube',ignore,stuff)
          'IMAG' : begin
              timage=s3drs_findimg('syninmodel')
                                ; if not an image, exit
              if (n_elements(timage) eq 1) then return
          end
          else : timage=pixonmodels(res,'fluxrope',ignore,stuff)
      endcase
  end

  if (bkg eq 'SAIT') then begin
      timage=timage + pixonmodels(res,'saito')
  end

  ; apply euler angles
  if (total(abs(euler)) gt 0) then $
    cuberot3d,timage,euler

  position=[(widget_shorthand('syninpos1')).value,$
         (widget_shorthand('syninpos2')).value,$
         (widget_shorthand('syninpos3')).value]
  diff= position - [s3drs.nD/2,s3drs.nD/2,s3drs.nD/2]
  if (total(abs(diff)) ne 0) then begin
      ; need to move in cube
      cuberot3d,timage,[0,0,0],trans=diff
  end

  diff=pr_render(pxwrapper,timage)

  ;print,'background is ',bkg

  if (bkg eq 'ARTI') then begin
      seed=systime(/seconds); random seed
      ;fsig=1.0/(40^2); Paul's historical value for s/n range
      fsig=0.0625; a higher value, useful for testing, based on S/N=4
      diff = (diff + $
              randomn(seed,s3drs.Nd,s3drs.Nd,s3drs.Nframes)*fsig*max(diff)) > 0
  end else if (bkg eq 'GAUS') then begin
      ; add in Gaussian scatter based on intrinsic photon noise
      seed=systime(/seconds)    ; random seed
      ntemp = p_simplenoise(diff,pxwrapper)/sqrt(exptime)
      diff = (diff + $
              randomn(seed,s3drs.Nd,s3drs.Nd,s3drs.Nframes)*ntemp) > 0
      ; add in -0.5 to +0.5 of photon noise as random component
;      diff = diff + $
;        (randomn(seed,s3drs.Nd,s3drs.Nd,s3drs.Nframes)-0.5) * $
;        p_simplenoise(diff,pxwrapper)/sqrt(exptime)
  end

  ndiff = p_simplenoise(diff,pxwrapper)/sqrt(exptime)

  if (specificocculter eq 'ignore') then begin
      masks=p_getmask(sr=pxwrapper.sr)
  end else if (specificocculter eq 'none') then begin
      masks=bytarr(res,res,nviewpoints) + 1
  end else begin
      ; called for a particular model
      masks=p_getmask(specificocculter,sr=pxwrapper.sr)
  end

  ; sets {diff,ndiff,mset,pxwrapper}

  s3drs.cmedate=systime()

  if (ptr_valid(s3drs.names)) then ptr_free,s3drs.names
  s3drs.names = ptr_new(name)

  if (ptr_valid(s3drs.timage)) then ptr_free,s3drs.timage
  s3drs.timage=ptr_new(timage)

  if (ptr_valid(s3drs.pxptr)) then ptr_free,s3drs.pxptr
  s3drs.pxptr=ptr_new(pxwrapper)
  if (ptr_valid(s3drs.syn)) then ptr_free,s3drs.syn
  s3drs.syn=ptr_new(diff)
  if (ptr_valid(s3drs.ndiff)) then ptr_free,s3drs.ndiff
  s3drs.ndiff=ptr_new(ndiff)
  if (ptr_valid(s3drs.mset)) then ptr_free,s3drs.mset
  s3drs.mset=ptr_new(masks)

  ; need to free custom masks, as they may no longer be relevant
  if (ptr_valid(s3drs.mset2)) then ptr_free,s3drs.mset2

  ignore=s3drs_tv_replot('Syn1')
  ignore=s3drs_tv_replot('Mask1')
  ignore=s3drs_tv_replot('Input1')

  ; and put up the template and populate the buttons for additional masks
  s3drs_fillmask
  s3drs_noisesliders
  ; plot the S/N in the noise tab (make sure this is after s3drs_noisesliders)
  s3drs_noise

  ; update button to show it was used
  warnid=WIDGET_INFO(s3drs.tabs.synID,FIND_BY_UNAME='synwarn')
  WIDGET_CONTROL,warnid,SET_VALUE='(re-generate data or proceed to next tab)'

  ; update position drawing and details
  s3drs_satlosview
  s3drs_coversheet

  ; add noise histograms
  s3drs_hist

  ; save a copy, just for utility
  save,s3drs,file='s3drs_synin.sav'

  ; turn off block-and-busy and turn on subsequent tabs
  s3drs_busy,0,waiting
  WIDGET_CONTROL,s3drs.tabs.maskID,SENSITIVE=1
  WIDGET_CONTROL,s3drs.tabs.noiseID,SENSITIVE=1
  WIDGET_CONTROL,s3drs.tabs.reconID,SENSITIVE=1

  if (s3drs.tabhop) then $
    s3drs_tabhop                ; move on to masking

END
