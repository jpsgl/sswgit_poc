;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs
;               
; Purpose   : controller for the s3drs GUI
;               
; Explanation: Master routine for the 's3drs' software and GUI, calls
; everything else
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : none
;               
; Outputs   : none (obviously sets up underlying states)
;
; Keywords  : 
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; wrapper and master routine for the S3DRS


; this is just a legacy stub used earlier in testing, useful
; if developers later want to play around with modifying thngs.
PRO fake_data
  common s3drs,s3drs
  if (ptr_valid(s3drs.names)) then ptr_free,s3drs.names
  s3drs.names=ptr_new(['Cor2A tB','Cor2B tB','C3 tB','ZMask'])
  s3drs.nframes=n_elements(*(s3drs.names))
END

PRO s3drs

  common s3drs,s3drs

  version=3.10; just for keeping track

  quiet_add_tag; precompiles our hacked 'add_tag' that forces quietness
  
  ; create global structure
  disum_so_loc
  if (n_elements(s3drs) eq 0) then s3drs_init else $
    s3drs.tabs={introID:0,dataID:0,synID:0,maskID:0,$
                noiseID:0,reconID:0,fmID:0,vizID:0}

  s3drs.pixon=pixoncheck(); see if Pixon exists

  s3drs_text; initialize

  s3drs_miscevents; initialize
  s3drs_handler; initialize

  pixonism=s3drs_popup(); Pixon license bit
 ; if -1, pixon exists, but needs to be disabled as they rejected the license  
  if (pixonism eq -1) then s3drs.pixon=-1

  s3drs_gui; main

  s3drs_fetch; initialize

  s3drs_masks; init

END
