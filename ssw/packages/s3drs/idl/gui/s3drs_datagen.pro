PRO s3drs_datagen,event
;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs_datagen
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: preps all data, major
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : event
;               
; Outputs   : stores data into structure
;
; Keywords  : 
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            


  common s3drs,s3drs

  s3drs_fetch_datadate; also sets srdrs.Nd

  datapol=widget_shorthand('datapol',/DROPLIST)
  ; index 0=tB, 1=pB, 2=rad+tan, 3=trios
  if (datapol.index eq 3) then polar=1 else polar=0
  if (datapol.index eq 2) then rtpair=1 else rtpair=0
  if (datapol.index eq 1) then pb=1 else pb=0
  radonly=0; choice not available via GUI
  tanonly=0; choice not available via GUI
  indypair=0; choice not available via GUI

  scrubby=widget_shorthand('datascrub')
  if (scrubby.value eq 1) then scrubbright=1 else scrubbright=0

  instr=widget_shorthand('datainstr')
  cor2=0
  cor1=0
  c2=0
  c3=0
  for ii=0,n_elements(instr.name)-1 do begin
      if (instr.value[ii] eq 1) then $
        case (strmid(strupcase(instr.name[ii]),0,4)) of
          'COR2': cor2=1
          'COR1': cor1=1
          'C2':   c2=1
          'C3':   c3=1
          'OTHE': INFOWIDGET,'Other instruments not yet implemented'
          else: push,otherinstr,instr.name[ii]
      end
  end
  if (cor2+cor1+c2+c3+n_elements(otherinstr) eq 0) then begin
      INFOWIDGET,'Please choose at least 1 instrument to get data from!'
      return
  end

  specificocculter='ignore'
  specs=widget_shorthand('datamask',/DROPLIST)
  specname=(strsplit(specs.name,' ',/extract))[0]
  specificocculter=strmid(strupcase(specname),0,4)
  if (specificocculter eq 'OTHE') then begin
      INFOWIDGET,'Other instruments not yet implemented'
      specificocculter='ignore'
  end else if (specificocculter eq 'SAME') then begin
      specificocculter='ignore'
  end

  print,'Now running data prep'

  ; register this
  s3drs.choice='data'

  ; block-and-busy while creating data
  s3drs_busy,1,waiting

  ; returns DS={diff,ndiff,mset,px}
  quickcme,s3drs.Nd,s3drs.cmedate,s3drs.priordate,$
    /setuponly,/resume,retstr=DS,/gui,$
    c2=c2,c3=c3,cor1=cor1,cor2=cor2,otherinstr=otherinstr,$
    scrubbright=scrubbright,$
    specificocculter=specificocculter,pb=pb,$
    polar=polar,rtpair=rtpair,indypair=indypair,$
    radonly=radonly,tanonly=tanonly

  if (n_elements(DS) eq 0) then begin
      ;print,'Failed to create data, try different dates?'
      INFOWIDGET,'Failed to create data, try different dates?'
      s3drs_busy,0,waiting
      return
  end

  s3drs.Nframes=DS.px.sr.Nd
  s3drs.Nd=DS.px.sr.Ndata[0]

  if (ptr_valid(s3drs.names)) then ptr_free,s3drs.names
  s3drs.names = ptr_new(DS.px.sr.tele_name+DS.px.sr.detector+DS.px.sr.sat_name)

  if (ptr_valid(s3drs.pxptr)) then ptr_free,s3drs.pxptr
  s3drs.pxptr=ptr_new(DS.px)
  if (ptr_valid(s3drs.diff)) then ptr_free,s3drs.diff
  s3drs.diff=DS.diff
  if (ptr_valid(s3drs.ndiff)) then ptr_free,s3drs.ndiff
  s3drs.ndiff=DS.ndiff
  if (ptr_valid(s3drs.mset)) then ptr_free,s3drs.mset
  s3drs.mset=DS.mset

  ; need to free custom masks, as they may no longer be relevant
  if (ptr_valid(s3drs.mset2)) then ptr_free,s3drs.mset2

  ;help,DS.diff,*(DS.diff)
  ;s3drs_floodfill,'Data1',(*(DS.diff))*(*(DS.mset))

  ignore=s3drs_tv_replot('Data1')
  ignore=s3drs_tv_replot('Mask1')
  ignore=s3drs_tv_replot('Input1')

  ; and put up the template and populate the buttons for additional masks
  s3drs_fillmask
  s3drs_noisesliders
  ; plot the S/N in the noise tab (make sure this is after s3drs_noisesliders)
  s3drs_noise

  ; update button to show it was used
  warnid=WIDGET_INFO(s3drs.tabs.dataID,FIND_BY_UNAME='datawarn')
  WIDGET_CONTROL,warnid,SET_VALUE='(re-generate data or proceed to next tab)'

  ; update position drawing and details
  s3drs_satlosview
  s3drs_coversheet

  ; add noise histograms
  s3drs_hist

  ; turn off block-and-busy and turn on subsequent tabs
  s3drs_busy,0,waiting
  WIDGET_CONTROL,s3drs.tabs.maskID,SENSITIVE=1
  WIDGET_CONTROL,s3drs.tabs.noiseID,SENSITIVE=1
  WIDGET_CONTROL,s3drs.tabs.reconID,SENSITIVE=1

  if (s3drs.tabhop) then $
    s3drs_tabhop,skip=2         ; switch past synthentic to masking

END
