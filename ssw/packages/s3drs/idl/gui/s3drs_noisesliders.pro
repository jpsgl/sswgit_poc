;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs_togglelink, s3drs_noisesliders
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: generates a variable number of noisesliders
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : none
;               
; Outputs   : alters GUI state
;
; Keywords  : 
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
PRO s3drs_togglelink,event

  common s3drs,s3drs

  s3drs.link = ~s3drs.link
  
  if (s3drs.link) then logo=s3drs.linklogo else logo=s3drs.unlinklogo

  ; remember there is always 1 fewer link buttons then data items
  for iv=0,s3drs.nFrames-1-1 do begin
      linkid=WIDGET_INFO(s3drs.tabs.noiseID,FIND_BY_UNAME='snlink'+int2str(iv))
      WIDGET_CONTROL,linkid,SET_VALUE=logo
  end

END

PRO s3drs_noisesliders

  common s3drs,s3drs

  noisehome = WIDGET_INFO(s3drs.tabs.noiseID,FIND_BY_UNAME='snratio')

  if (noisehome ne 0) then begin

     ; clear out old ones
      wchildren=WIDGET_INFO(noisehome,/ALL_CHILDREN)
      for ic=0,n_elements(wchildren)-1 do begin
          if (wchildren[ic] ne 0) then begin
              WIDGET_CONTROL,wchildren[ic],GET_UVALUE=uvalue
              if (n_elements(uvalue) ne 0) then $
                if (uvalue eq 3) then $
                WIDGET_CONTROL,wchildren[ic],/DESTROY
          end
      end
     

      for iv=0,s3drs.nFrames-1 do begin
          wSlider = WIDGET_SLIDER(noisehome,$
                                  VALUE=1,minimum=1,UVALUE=3,$
                                  UNAME='snratio'+int2str(iv),$
                                  TITLE=(*s3drs.names)[iv],$
                                  EVENT_PRO='s3drs_noise')
          if (s3drs.link) then logo=s3drs.linklogo else logo=s3drs.unlinklogo
          ; skip link icon for last item
          if (iv ne s3drs.nFrames-1) then $
            wLink = WIDGET_BUTTON(noisehome,VALUE=logo,UVALUE=3,$
                                  UNAME='snlink'+int2str(iv),$
                                  EVENT_PRO='s3drs_togglelink',FRAME=0)

        end

      end

  end

END
