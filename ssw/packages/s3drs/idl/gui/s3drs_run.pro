PRO s3drs_run,event
;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : s3drs_run
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: Main 'useful' routine to actually run a reconstruction
;               
; Use       : only called from 's3drs.pro' 
;    
; Inputs    : event
;               
; Outputs   : changes underlying state and s3drs variable
;
; Keywords  : 
;               
; Common    : common s3drs, s3drs (a structure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : GUI
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

  common s3drs,s3drs

  datapol=widget_shorthand('datapol',/DROPLIST)
  ; index 0=tB, 1=pB, 2=rad+tan, 3=trios
  if (datapol.index eq 3) then iend=5 else $
    if (datapol.index eq 2) then iend=3 else iend=1

  if (s3drs.choice eq 'synthetic') then dsptr=s3drs.syn else dsptr=s3drs.diff

  if (PTR_VALID(s3drs.pxptr) eq 0 OR PTR_VALID(dsptr) eq 0 OR $
      PTR_VALID(s3drs.ndiff) eq 0 OR PTR_VALID(s3drs.mset) eq 0) then begin
      INFOWIDGET,'Input items not fully specified, please go back to Data tab'
      return
  end

  ; now get GUI variables
  jumpstr=widget_shorthand('jumpstart',/DROPLIST)
  ;print,'index is ',jumpstr.index
  if (jumpstr.index eq 0) then jumpstart=1 else jumpstart=0
  if (jumpstr.index eq 2) then begin
      ; need to load external start image
      presolution=s3drs_findimg('jumpstart')
      ; if not an image, exit
      if (n_elements(presolution) eq 1) then return
  end
  if (jumpstr.index eq 3) then begin
      ; use previous solution, if one exists
      if (PTR_VALID(s3drs.solution)) then begin
          presolution=*(s3drs.solution)
      end else begin
          INFOWIDGET,'No previous solution exists, choose another option'
          jid=WIDGET_INFO(s3drs.wTLB,find_by_uname='jumpstart')
          WIDGET_CONTROL,jid,SET_DROPLIST_SELECT=0
          return
          ;jumpstart=1; default to best case
      end
  end

  mxit=(widget_shorthand('mxit')).value

  reconchoice=widget_shorthand('reconmethod',/DROPLIST)

  if (reconchoice.index lt 2 and s3drs.pixon le 0) then begin

      INFOWIDGET,s3drs_text_license()
      return

  end else if (reconchoice.index eq 2) then begin

      ; They chose forward modeling, so set that up then exit to that screen
      WIDGET_CONTROL,s3drs.tabs.fmID,SENSITIVE=1
      if (s3drs.tabhop) then $
        s3drs_tabhop
      return

  end else if (reconchoice.index eq 3) then begin

      ; backprojection minimization (does not require Pixon)
      ; no extra processing here, but there is a fork later in this routine

  end else if (reconchoice.index eq 4) then begin

      INFOWIDGET,$
        'Data prepped.  Reconstruct whenever you wish. Doing nothing as asked.'
      return

  end else if (reconchoice.index eq 5) then begin

      INFOWIDGET,$
        "Other methods not yet implemented.  Please consult developer's guide."
      return

  end

  ; we have optional overlay masks
  if (PTR_VALID(s3drs.mset2)) then mset2=*s3drs.mset2 else mset2=1.0
  masks = (*s3drs.mset) * mset2

  s3drs_cutoff,dsptr,masks

  s3drs_busy,1,waiting,'Running reconstruction, this could take a while, please wait'

  ; note we added our non-Pixon method here, useful also as a template
  ; if others want to put in their own routines

  ; always free any Pixon objects, just to avoid clash with different methods
  if (PTR_VALID(s3drs.objptr)) then ptr_free,s3drs.objptr

  if (reconchoice.index eq 3) then begin

      data= (*dsptr) * masks
      solution_image=simple3drv(data,pxwrapper=*(s3drs.pxptr),$
                                /backproject,/norm,/usemax,/carve)

      renders=pr_render(*(s3drs.pxptr),solution_image)

      ; store in pixon
      if (PTR_VALID(s3drs.objptr)) then $
        (*s3drs.objptr)->set,image=solution_image

  end else begin

      ; default at this stage is Pixon
      noise1=*s3drs.ndiff
      for iv=0,s3drs.nFrames-1 do noise1[*,*,iv] /= (*s3drs.snratio)[iv]
      ; quick sanity check
      res1=(*(s3drs.pxptr)).sr.Ncube
      res2=(*(s3drs.pxptr)).sr.Ndata[0]
      res3=n_elements((*(dsptr))[*,0,0])
      if (res1 ne res3 or res2 ne res3) then begin
          print,'Error in data resolution, correcting'
          (*(s3drs.pxptr)).Ncube = res3
          (*(s3drs.pxptr)).Ndata = res3
      end
      oPixon = quickcme_run(*(s3drs.pxptr),*(dsptr),noise1,masks,$
                            jumpstart,iend,mxit=mxit,presolution=presolution)
     ; STORE THE OBJECT

      s3drs.objptr = ptr_new(oPixon)

     ; FETCH AND STORE ALL THE APPROPRIATE VARIABLES

      masks=stereo_fetch(*s3drs.pxptr,*s3drs.objptr,/mask)

      solution_image=stereo_fetch(*s3drs.pxptr,*s3drs.objptr,/im)

      renders=stereo_fetch(*s3drs.pxptr,*s3drs.objptr,/render)

  end

  if (PTR_VALID(s3drs.solution)) then ptr_free,s3drs.solution
  s3drs.solution=ptr_new(solution_image)

  if (PTR_VALID(s3drs.render)) then ptr_free,s3drs.render
  s3drs.render=ptr_new(renders * masks)
  
  threeview,/log,solution_image,/pixon,/noplot,trio

  if (PTR_VALID(s3drs.trio)) then ptr_free,s3drs.trio
  s3drs.trio=ptr_new(trio)

  ; PLOT THE SOLUTIONS

  ignore=s3drs_tv_replot('VizData')
;  ignore=s3drs_tv_replot('VizRender')
  ignore=s3drs_tv_replot('Trio')
  ignore=s3drs_tv_replot('Chisq')

  ; update details
  s3drs_coversheet

  ; and save, just for safety
  save,s3drs,file=s3drs.title+'_latest.sav'
  densitycube,'init'
  cubestructure,cubeinfo,'pack',N=s3drs.Nd,desc='3D reconstruction',$
    author='S3DRS',program='S3DRS',obsdate=s3drs.cmedate
  densitycube,'save',s3drs.title+'_latest.fts',solution_image,cubeinfo

  s3drs_savename

  ; AND RETURN CONTROL TO THE USER

  s3drs_busy,0,waiting
  WIDGET_CONTROL,s3drs.tabs.vizID,SENSITIVE=1

  ; also move to viz tab
  if (s3drs.tabhop) then $
    s3drs_tabhop,skip=2         ; move past FM to viz

  s3drs_psplots; use default best plot values


END
