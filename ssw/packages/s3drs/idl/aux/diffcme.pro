;+
;
; Project   : s3drs and Pixon reconstruction software
;                   
; Name      : actualproblem, leveltest, allcme, preptb, run_diffCME,
; gen_diffCME, diffcme
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: (part of the testing and legacy code, documented in code)
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 

; Keywords  :
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : (see CVS logs)
;
; Written     : Sandy Antunes, NRC, 2007-2009
;               
;-            
; leveltest compares the signal and noise of the source and
; difference images
PRO actualproblem

  datetime='20070708_222230'
  bcktime='20070708_182230'

  gen_diffCME,datetime,N=128,/off
  gen_diffCME,bcktime,N=128,/off
  gen_diffCME,datetime,bcktime,N=128

  run_diffCME,datetime+'_'
  run_diffCME,bcktime+'_'
  run_diffCME,datetime+'_'+bcktime

END

PRO leveltest

  datetime='20070708_222230'
  bcktime='20070708_182230'
  gen_diffCME,datetime,N=128,/off
  gen_diffCME,bcktime,N=128,/off
  gen_diffCME,datetime,bcktime,N=128

  ; now just load and look at these
  restore,/v,'raw_20070708_222230_.sav'
  print,pxwrapper.sr.msbperdn
  d1=datum
  s1=totalsigma

  restore,/v,'raw_20070708_182230_.sav'
  print,pxwrapper.sr.msbperdn
  d2=datum
  s2=totalsigma

  restore,/v,'raw_20070708_222230_20070708_182230.sav'
  print,pxwrapper.sr.msbperdn
  d1a=EventFrames
  d2a=BckgFrames
  d3=datum
  s3=totalsigma

  ; first, consistence check: all these should have no difference
  print,minmax(d3-(d1-d2))
  print,minmax(d1a-d1)
  print,minmax(d2a-d2)

  ; But they're not!

;IDL64[duffer]>   print,pxwrapper.sr.msbperdn
;  2.70000e-12  2.80000e-12
;
;IDL64[duffer]>   print,minmax(d3-(d1-d2))
; -5.29576e-14  4.31877e-14
;IDL64[duffer]>   print,minmax(d1a-d1)
; -8.79445e-07  1.36001e-10
;IDL64[duffer]>   print,minmax(d2a-d2)
; -8.79428e-07  1.37851e-10

   print,d1[41:43,65:67]
   print,d2[41:43,65:67]
   print,d3[41:43,65:67]
   print,d1a[41:43,65:67]
   print,d2a[41:43,65:67]
   print,s1[41:43,65:67]
   print,s2[41:43,65:67]
   print,s3[41:43,65:67]

;IDL64[duffer]>    print,d1[41:43,65:67]
;  2.48480e-09  2.80067e-09  3.16503e-09
;  2.47441e-09  2.76509e-09  3.08092e-09
;  2.41286e-09  2.68686e-09  2.98715e-09
;IDL64[duffer]>    print,d2[41:43,65:67]
;  2.43983e-09  2.72581e-09  3.03568e-09
;  2.39946e-09  2.66895e-09  2.98200e-09
;  2.35216e-09  2.62647e-09  2.93214e-09
;IDL64[duffer]>    print,d3[41:43,65:67]
;  4.49638e-11  7.48654e-11  1.29345e-10
;  7.49535e-11  9.61457e-11  9.89187e-11
;  6.07019e-11  6.03890e-11  5.50046e-11
;IDL64[duffer]>    print,d1a[41:43,65:67]
;     -0.00000      0.00000      0.00000
;     -0.00000      0.00000      0.00000
;     -0.00000      0.00000      0.00000
;IDL64[duffer]>    print,d2a[41:43,65:67]
;     -0.00000     -0.00000     -0.00000
;     -0.00000     -0.00000     -0.00000
;     -0.00000     -0.00000     -0.00000
;IDL64[duffer]>    print,s1[41:43,65:67]
;  5.57052e-11  5.88101e-11  6.21935e-11
;  5.56026e-11  5.84665e-11  6.14317e-11
;  5.49772e-11  5.77101e-11  6.05673e-11
;IDL64[duffer]>    print,s2[41:43,65:67]
;  5.52512e-11  5.80907e-11  6.10141e-11
;  5.48394e-11  5.75355e-11  6.05178e-11
;  5.43523e-11  5.71189e-11  6.00538e-11
;IDL64[duffer]>    print,s3[41:43,65:67]
;  5.57052e-11  5.88101e-11  6.21935e-11
;  5.56026e-11  5.84665e-11  6.14317e-11
;  5.49772e-11  5.77101e-11  6.05673e-11

END

PRO allcme
  dates=['20070708_215230','20070708_222230','20070708_225230',$
        '20070708_232230','20070708_235230']

  ; prep tB, if needed
  for i=0,n_elements(dates)-1 do $
    preptb,dates[i]

  ; prepare difference images and noise as pixon inputs
  for i=0,n_elements(dates)-1 do $
    if (file_exist('raw_'+dates[i]+'.sav') eq 0) then gen_diffCME,dates[i]

  ; run pixon
  for i=0,n_elements(dates)-1 do run_diffCME,dates[i]

END

; use to prep a tB, e.g. preptb,'20070708_215230'
; if an appropriately named tB file exists, does nothing (safe!)
PRO preptb,timedate

  checka = timedate + '_1B4c2A.fts'
  checkb = timedate + '_1B4c2B.fts'
  dncheck='DN_img+bckg_'+timedate+'.sav'

  td=strsplit(timedate,'_',/extract)
  date=td[0]
  time=td[1]

  ; write the FITS total brightness for our regular p_ingest_fits
  if (file_exist(checka) eq 0) then begin
      fa=date+'_'+time+'_s4c2A.fts'
;      fa=date+'_'+time+'_n4c2A.fts'
      da=getenv('secchi')+'/lz/L0/a/seq/cor2/'+date+'/'
      flista=match_polarity(fa)
      for i=0,2 do flista[i]=da+flista[i]
      secchi_prep,flista,hdrA,dataA,/polariz_on,/smask,/write_f ; tB in MSB
  end

  ; write the FITS total brightness for our regular p_ingest_fits
  if (file_exist(checka) eq 0) then begin
      fb=date+'_'+time+'_s4c2A.fts'
;      fb=date+'_'+time+'_n4c2A.fts'
      db=getenv('secchi')+'/lz/L0/b/seq/cor2/'
      flistb=match_polarity(fb)
      for i=0,2 do flistb[i]=db+flistb[i]
      secchi_prep,flistb,hdrB,dataB,/polariz_on,/smask,/write_f ; tB in MSB
  end

  ; also get image and background for future work
  if (file_exist(dncheck) eq 0) then begin
      
      secchi_prep,flista,hdrA,dataA,/polariz_on,/smask ; tB in DN
      secchi_prep,flistb,hdrB,dataB,/polariz_on,/smask ; tB in DN

      bckA = scc_getbkgimg(hdrA,/match,/totalb,/daily)
      bckB = scc_getbkgimg(hdrB,/match,/totalb,/daily)

      save,hdrA,dataA,bckA,hdrB,dataB,bckB,file='DN_img+bckg_'+timedate+'.sav'
  end

END

PRO run_diffCME,tag,type=type,reducesigma=reducesigma
  restore,/v,'raw_'+tag+'.sav'

  if (n_elements(reducesigma) eq 0) then reducesigma=1.0
  ; type is 'datum', 'floored', 'totfloored', or '4oom'
  ; choices are datum, datumfloored, or data4oom
  ;             totalsigma, diffsigma, diffsigmafloored, sigma4oom,
  ;
  ; 'reducedsigma=' reduces the sigma by the given factor

  if (n_elements(type) eq 0) then type='datum'

  case type of
      'datum': begin
          dat=datum
          sig=totalsigma
      end
      'floored': begin
          dat=datumfloored
          sig=diffsigmafloored
      end
      'totfloored': begin
          dat=datumfloored
          sig=totalsigma
      end
      '4oom': begin
          dat=data4oom
          sig=sigma4oom
      end
      else: begin
          dat=datum
          sig=totalsigma
      end
  end

  p_insert_data,pxwrapper,dat,oPixon,sigma=sig*reducesigma,$
    masks=masks
  oPixon->set,mxit=4000
  oPixon->full,/restart
  p_report,pxwrapper,oPixon,/save
  p_standardplots,pxwrapper,oPixon,/no
END

PRO gen_diffCME,datetime,bcktime,basedir=basedir,N=N,off=off

; Takes existing tB pairs (pre-and-CME, A&B) to make
; difference files with a flat cutoff of signal<0
; and runs a recon.  If tB do not exist, creates.

  off=KEYWORD_SET(off); turns off differencing and just does main

  if (n_elements(datetime) eq 0) then datetime='20070708_222230'
  if (n_elements(bcktime) eq 0) then bcktime='20070708_182230'
  if (off) then bcktime=''

  if (n_elements(N) eq 0) then N=64

  if (n_elements(basedir) eq 0) then $
    basedir='/home/antunes/RUNS/CME0708/tb_fits/'

  ; makes tB if they do not yet exist
;  preptb,datetime
;  if (off eq 0) then preptb,bcktime

; now actually make the difference files and pixonize things

  QA=basedir + bcktime + '_1B4c2A.fts'
  QB=basedir + bcktime + '_1B4c2B.fts'

  A2=basedir + datetime + '_1B4c2A.fts'
  B2=basedir + datetime + '_1B4c2B.fts'

; generate different image, removing bad pixels (all negative values)

  if (off eq 0) then qad=trim_image_neg(sccreadfits(QA))
  if (off eq 0) then qbd=trim_image_neg(sccreadfits(QB))
  a2d=trim_image_neg(sccreadfits(A2,hdra))
  b2d=trim_image_neg(sccreadfits(B2,hdrb))
  
  if (off) then d1=a2d else d1=a2d-qad
  if (off) then d2=b2d else d2=b2d-qbd
  
  d1r=rebin(d1,N,N)
  d2r=rebin(d2,N,N)
  datum=[[[d1r]],[[d2r]]]
  
  prep_3drv,pxwrapper,views=2,N=N
  
  pxwrapper.sc.gname=datetime+'_'+bcktime

  p_ingest_fits,A2,dA,maskA,srjrA,/verbose,N=N,pxwrapper=pxwrapper,iele=0
  p_ingest_fits,B2,dB,maskB,srjrB,/verbose,N=N,pxwrapper=pxwrapper,iele=1
  masks=[[[maskA]],[[maskB]]]
  
; generate noise for originals
  
  EventFrames=[[[a2d]],[[b2d]]]
  if (off eq 0) then BckgFrames=[[[qad]],[[qbd]]]
  
  if (off) then totalnoise=p_quicknoise(EventFrames,pxwrapper=pxwrapper,$
                                        /readout) else $
    totalnoise=p_quicknoise(EventFrames,BckgFrames,pxwrapper=pxwrapper,$
                            /readout)
  totalsigma=rebin(totalnoise,N,N,2)
  diffsigma=p_quicknoise(datum,pxwrapper=pxwrapper)
  
  pxwrapper.sr.normd=5.7e+13
  
  p_generate_pixon,'classic',pxwrapper,oPixon
  
  oPixon->set,mxit=4000
  
  datumfloored=trim_image_neg(datum,/nonzero)
  diffsigmafloored=p_quicknoise(datumfloored,pxwrapper=pxwrapper)
  
  data4oom=datum
  oom4=max(datum)/(10.0^4)
  ie=where (datum le oom4)
  data4oom[ie]=0.0
  sigma4oom=p_quicknoise(data4oom,pxwrapper=pxwrapper)
  
  masks2=masks*0.0
  masks2[0:32,16:48,*]=1.0
  
; save our possible variants
  
  save,pxwrapper,oPixon,datum,totalsigma,diffsigma,masks,datumfloored,$
    diffsigmafloored,masks2,data4oom,sigma4oom,hdra,hdrb,$
    EventFrames,BckgFrames,file='raw_'+pxwrapper.sc.gname+'.sav'
  print,'saved ','raw_'+pxwrapper.sc.gname+'.sav'
  
END

PRO diffcme,datetime,bcktime,N=N

  if (n_elements(N) eq 0) then N=64

  if (n_elements(bcktime) eq 0) then begin
      gen_diffCME,datetime,bcktime,N=N,/off
  end else begin
      gen_diffCME,datetime,bcktime,N=N
  end

  run_diffCME,datetime

END
