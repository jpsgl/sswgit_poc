;+
;
; Project   : s3drs and Pixon reconstruction software
;                   
; Name      : p_noisewrapper
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: (part of the testing and legacy code, documented in code)
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 

; Keywords  :
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : (see CVS logs)
;
; Written     : Sandy Antunes, NRC, 2007-2009
;               
;-            
; The preferred way is to pass it "fitsfile=FNAME", a FITS filename
; of the data.  It will read it in, do the noise work, then save the
; result as a FITS "pixon noise" file with minimal headers.
;
; Alternately, you can pass it incoming data with 'data05' and/or
; 'data10' and it will run a noise routine to create the noise data.
;
; The routine will save the noise as a FITS file if given '/savefits'
; The routine will return the data via the 'noise=noise' pointer.
; If you give neither '/savefits' or 'noise=noise', why run it at all?
;

; see pixon/doc documentation for noise details
;
; 0 = none (also default)
; 1 = req L05, requires secchi_prep (so does not work for LASCO)
; 2 = req L05 and L10 (or fakes L10)
; 3 = req L05
; 4 = req L10
; 5 = req L10
;
; Currently, methods are geared for SECCHI.  Methods 2-5 also work
; for LASCO if the /lasco flag is set (so it skips SECCHI's
; 'get_calfac' in favor of 'c2_calfactor' or 'c3_calfactor'.)
;
; Note we do not check whether data matches the method, so make sure
; routines that invoke this (like p_process_filelist) check that
; the model matches the data.  The names are descriptive but we
; cannot check that, for example, data05 is really Level 0.5 DN data,
; and data10 is really MSB data.
;
; The variable 'data10' is only used for method 2, where both
; 'data' (presumed Level 0.5) and 'data10' (Level 1.0) are needed.
;
; If given optional 'N', rebins down to that N while preserving
; total signal.
;
; Optional /pointer tells it to return a pointer, not an array
;
; Note we assumed and save Noise with same rectification as the
; desired data.  If we read in level 05 data, we use scc_img_trim
; so it is Rectified.  Level 1.0 data is usually already Rectified.
; Ultimately, we save the FITS header for the noise file with the
; same header as the original data (including current Rectified
; state).  So long as the noise and data match in terms of
; rectification, the resulting noise routines and later ingest
; of the noise FITS file should be 'safe'.
;
; /forcemsb is there for earlier 'broken' pB files that are in
; MSB but list the units as DN/s.  Automatically assumes it is
; a SECCHI MSB file and ignores header parsing to determine state.
; Only set if p_noisewrapper fails due to this header bug.
;
; Optional 'outfile=outfile' will return file name if /savefits
; was used.
;

PRO p_noisewrapper,model,fitsfile,Nforce=Nforce, $
                   hdr=hdr,data05=data05,data10=data10,$
                   pointer=pointer,noise=noise,savefits=savefits,$
                   forcemsb=forcemsb,outfile=outfile

  pointer=KEYWORD_SET(pointer)
  savefits=KEYWORD_SET(savefits)
  forcemsb=KEYWORD_SET(forcemsb)

  if (n_elements(fitsfile) ne 0) then begin
      ; ingest a FITS data file, get necessary pieces, then process
      hdr=fitshead2struct(headfits(fitsfile))   ; general IDL routine
      if (forcemsb) then begin
          din=sccreadfits(fitsfile,hdr)
      end else if (hdr.instrume eq 'SECCHI') then begin
          din=sccreadfits(fitsfile,hdr)
          if (hdr.bunit ne 'MSB') then din=scc_img_trim(din,hdr);likely 05 data
      end else begin
          din=readfits(fitsfile)
      end
      if (hdr.bunit eq 'MSB' or forcemsb) then data10=din else data05=din
  end

  if (n_elements(data10) ne 0) then Ndata = n_elements(data10(0,*))
  if (n_elements(data05) ne 0) then Ndata = n_elements(data05(0,*))

  if (n_elements(Ndata) eq 0) then begin
      print,'No data provided, nothing returned.  Please check your usage.'
      return
  end

  if (n_elements(Nforce) eq 0) then Nforce=Ndata

  ; several but not all routines require a minimal header
  if (n_elements(hdr) eq 0) then $
    hdr={SECCHI_HDR_STRUCTNOISE,$
             BIASDEV: 0, $
             EXPTIME: sr.exptime, $
             DETECTOR: sr.detector,$
             OBSRVTRY: sc.sat_name, $
             IPSUM: 0 $
            }

  case model of

      0: begin
          noisedata=make_array(Ndata,Ndata,/float,value=0.0)
      end

      1: begin

; Method 1: use secchi_prep directly, likely more accurate and consistent
;  noise = sigmaMSB = secchi_prep{sigmaDN + offsetbias}
          noisedata = secchi_noiseprep(data05, hdr, /subtractbias, /cleanup)

      end

      2: begin

; Method 2: make noise using level 0.5 data and fractional error

          fractional_error = fractional_noise(data05)

          if (n_elements(data10) eq 0) then begin
              print,'Error, no L10 data given to frac err, faking it.'
              data10=fake_secchi_prep(data05,hdr)
          end

          noisedata = data10 * fractional_error

      end

      3: begin

; Method 3, like Method 1 but not invoking secchi_prep
; noise_method3 = ( (noisedata - hdrn.biasdev) * dn2msb / exptime) * data_flat
          sigmaDN = fractional_noise(data05, /sigma)
          noisedata = fake_secchi_prep(sigmaDN, hdr);  ,/use_calimg)
      end

      4: begin
; Method 4, if we only have Level 1 data, using Method 2 (fractional error)

          data_L05=secchi_unprep(data10,hdr)
          fractional_error = fractional_noise(data_L05)
          noisedata = data10 * fractional_error

      end

      5: begin

; Method 5, if we only have Level 1 data, using Method 3 (fake prep of DN)

          data_L05=secchi_unprep(data10,hdr)
          sigmaDN = fractional_noise(data_L05, /sigma)
          noisedata = fake_secchi_prep(sigmaDN, hdr) ; ,/use_calimg)

      end

      else: ; do not alter

  endcase

  ; do optional rebin, if requested
  if (Ndata ne Nforce) then begin
      scaling=float(Ndata)/float(Nforce)
      noisedata=rebin(TEMPORARY(noisedata),Nforce,Nforce) * scaling
  end

  if (savefits) then begin
      if (n_elements(fitsfile) eq 0) then fitsfile=hdr.filename
      outfile=fitsfile
      strput,outfile,'N',17; mark as noise
      newhdr=hdr
      newhdr.filename=outfile
      if (hdr.instrume eq 'SECCHI') then begin
          sccwritefits,outfile,noisedata,newhdr,$
            COMMENTS='Noise file, not original data'
      end else begin
          writefits,outfile,noisedata,newhdr
      end
  end

  ; finally, if asked for a recast to pointer type, do so
  if (pointer) then noise=ptr_new(noisedata) else noise=noisedata

END
