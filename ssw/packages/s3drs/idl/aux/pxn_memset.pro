;+
;
; Project   : s3drs and Pixon reconstruction software
;                   
; Name      : pxn_memset
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: (part of the testing and legacy code, documented in code)
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 

; Keywords  :
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : (see CVS logs)
;
; Written     : Sandy Antunes, NRC, 2007-2009
;               
;-            
; ancillary routine used to allow use of the common
; memory-tracking experimental variables without
; requiring a .reset of each run

PRO pxn_memset,experimental,debug=debug

  common mappy, mappy_echo,mappy_mapexists,mappy_fitexists,mappy_experimental,$
    mappy_cres, mappy_dsmth, mappy_sfim, mappy_varfac, $
    mappy_g, mappy_h, mappy_tphi, mappy_xg, $
    mappy_on, mappy_talk

  experimental=KEYWORD_SET(experimental); 1=use this experimental mode
  debug=KEYWORD_SET(debug)
  ; print memory tracking statements if asked, or if using experimental mode
  if (experimental or debug) then mappy_echo=1 else mappy_echo=0

  mappy_experimental=experimental

  if (n_elements(mappy_on) eq 0) then begin
      mappy_on=1 ; register this routine as existing
  end else begin
      ; to prevent memory leaks, free our global items each run
      ptr_free,mappy_cres,mappy_dsmth,mappy_sfim,mappy_varfac
      ptr_free,mappy_g,mappy_h,mappy_tphi,mappy_xg
  end

  mappy_fitexists=0; initial value, do not change
  mappy_mapexists=0; initial value, do not change

END
