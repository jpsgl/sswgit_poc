PRO p_save_pixon,pxwrapper,sigma,timage,data,mdata

;+
; $Id: p_save_pixon.pro,v 1.1 2009/04/22 18:31:38 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : p_save_pixon.pro
;               
; Purpose   : Saves Pixon data
;               
; Explanation: Mostly syntax sugar to save a Pixon datum
;               
; Use       : IDL> p_save_pixon,sr,sp,sn,sc,so,userstruct,sigma,timage,data,mdata,journ
;    
; Inputs    : lots of Pixon data items
;               
; Outputs   : none
;
; Keywords  : none
;
; Calls from LASCO : none 
;
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: creates a save file
;               
; Category    : Simulation, IO
;               
; Prev. Hist. : See Paul Reiser's code and notes.
;
; Written     : Sandy Antunes, NRL, Nov-Dec 2005
;               
; $Log: p_save_pixon.pro,v $
; Revision 1.1  2009/04/22 18:31:38  antunes
; first half of relocation.
;
; Revision 1.1  2009/03/31 14:56:52  antunes
; Re-org prepatory to checking into SSW.
;
; Revision 1.2  2006/11/30 20:03:57  antunes
; Updated pixon and other printing and output routines.
;
; Revision 1.1  2006/04/11 15:55:05  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.5  2006/03/08 17:52:23  antunes
; Abstracted pixon structures into an aggregate structure in order
; to be encapsulate Pixonisms from general rendering and reconstruction
; routines.
;
; Revision 1.4  2006/02/28 19:48:16  antunes
; Lots of code, some buggy-- interim checkin.
;
; Revision 1.3  2005/12/27 17:48:24  antunes
; Commented half of the routines using SSW style.  Also added a new
; axes test case 'arrowcube'.
; Checked in the Tomography/old/mikic_img.pro routine here because it
; is a dependency for the general image-prep routine 'get_timage.pro'
;
;-            

  sr=pxwrapper.sr
  sp=pxwrapper.sp
  sn=pxwrapper.sn
  sc=pxwrapper.sc
  so=pxwrapper.so
  userstruct=pxwrapper.userstruct
  journ=pxwrapper.journ

  outfname=sc.gname+'_'+sc.tag+'.dat'
  save,filename=outfname,sr,sp,sn,sc,so,userstruct,sigma,$
    timage,data,mdata,journ
  print,'PIXON datum saved to '+outfname

  outfitsname=sc.gname+'_'+sc.tag+'.fits'
  densitycube,'init'
  cubestructure,cubeinfo,'pack', sr.Ncube, sc.note, 'Pixon', sc.version, $
    sc.date, ['y','-x','z'], 'heliospheric', sr.lla0, sr.d, sr.n0
  densitycube,'save',outfitsname,cubeinfo,timage; saves FITS file too
  print,'PIXON result saved as FITS in '+outfitsname

END
