;+
;
; Project   : s3drs and Pixon reconstruction software
;                   
; Name      : sr2fitshdr
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: (part of the testing and legacy code, documented in code)
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 

; Keywords  :
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : (see CVS logs)
;
; Written     : Sandy Antunes, NRC, 2007-2009
;               
;-            
; given a Pixon 'sr' structure, return a FITS header (in array
; format) suitable for immediate output with writefits.
;
; Optional flag /struct returns a header structure rather than array.
;
; sample usage:
;   @p_run_test
;  writefits,'p_fluxrope.fits',datum,sr2fitshdr(pxwrapper.sr,0)
;

FUNCTION sr2fitshdr,sr,i,struct=struct

  struct=KEYWORD_SET(struct)

  if (n_elements(i) eq 0) then begin
      print,'sr2fitshdr, Missing array index telling which data to use'
      return,''
  end

  rad2arcsec = 206265; 1 radian is this many arcsec
  rsun2meters = 6.96e8
  dsun = sr.L0 * sr.LL[i]          ; distance to sun in solar radius
  rsun_arcsec = rad2arcsec * atan(1.0/dsun)
  dsun_m = dsun * rsun2meters
  platescale = sr.L0tau[i] / rsun_arcsec ; pixel spacing in arcsec
  if (tag_exist(sr,'detector')) then detector = sr.detector[i] $
    else detector='unknown'
  N = sr.Ndata[i]
  polar = sr.polar[i]
  exptime = sr.exptime[i]
  k0 = p_getk0(sr,i)
  sunk0 = p_getk0(sr,i,/sun)
  if (tag_exist(sr,'rho')) then rho=sr.rho[i] else rho=sr.gamma[i]
  if (tag_exist(sr,'phi')) then phi=sr.phi[i] else phi=sr.theta[i]

  obsdate = systime(/julian)

  ; convert internal HEEQ coords to HGNL/HGLT
  x = dsun_m * cos(phi) * cos(rho)
  y = dsun_m * cos(phi) * sin(rho)
  z = dsun_m * sin(phi)
  carr=[x,y,z]
  convert_stereo_coord,obsdate,carr,'HEEQ','CARR'
  hee=[x,y,z]
  convert_stereo_coord,obsdate,hee,'HEEQ','HEE'

  hdr = { $
          simple: 'T', $
          bitpix: -64, $
          desc: '', $
          author: 'Pixon', $
          date_obs: obsdate, $
          detector: detector, $
          naxis: 3, $
          naxis1: N, $
          naxis2: N, $
          naxis3: 2, $
          cdelt1: double(platescale), $
          cdelt2: double(platescale), $
          cunit1: 'arcsec', $
          cunit2: 'arcsec', $
          crpix1: double(k0[0]), $
          crpix2: double(k0[1]), $
          crval1: double(sunk0[0]-k0[0]), $
          crval2: double(sunk0[1]-k0[1]), $
          crval1: double(0.0), $
          crval2: double(0.0), $
          bunit: 'cm^(-3)', $
          polar: polar, $
          exptime: double(exptime), $
          dsun_obs: double(dsun_m), $
          crlt_obs: double(rho), $
          crln_obs: double(phi), $
          heex_obs: double(hee[0]), $
          heey_obs: double(hee[1]), $
          heez_obs: double(hee[2]), $
          heqx_obs: double(x), $
          heqy_obs: double(y), $
          heqz_obs: double(z) $
        }

  if (struct) then begin
      return,hdr
  end else begin
      return,struct2fitshdr(hdr)
  end

END


