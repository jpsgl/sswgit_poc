
;+
; $Id: stereo_tsetup.pro,v 1.1 2009/04/22 18:31:46 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : stereo_tsetup
;               
; Purpose   : 
;               
; Explanation: 
;               
; Use       : IDL> 
;    
; Inputs    : 
;             
; Outputs   : 
;
; Keywords  : 
;               
; Calls from LASCO : none 
;
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : Pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRL, Nov-Dec 2005
;               
; $Log: stereo_tsetup.pro,v $
; Revision 1.1  2009/04/22 18:31:46  antunes
; first half of relocation.
;
; Revision 1.2  2009/04/21 19:08:50  antunes
; Updated up through 'r' with SSW headers.  Also minor John bug fixes.
;
; Revision 1.1  2009/03/31 14:57:00  antunes
; Re-org prepatory to checking into SSW.
;
; Revision 1.7  2008/01/04 20:16:50  antunes
; Final units (hopefully), also more tests and tools.
;
; Revision 1.6  2007/01/09 20:06:03  antunes
; Test now works with pixon tetra
;
; Revision 1.5  2006/11/30 20:03:58  antunes
; Updated pixon and other printing and output routines.
;
; Revision 1.4  2006/11/16 18:27:33  antunes
; Did a rollback then an update to allow pixon wrapper to handle
; heterogeneous images.
;
; Revision 1.3  2006/11/09 18:32:54  antunes
; Minor fixes and improvements.
;
; Revision 1.2  2006/10/20 17:58:43  antunes
; Lots of incremental mods to Pixon-related routines.
;
; Revision 1.1  2006/04/11 15:55:06  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.5  2006/03/08 17:52:23  antunes
; Abstracted pixon structures into an aggregate structure in order
; to be encapsulate Pixonisms from general rendering and reconstruction
; routines.
;
; Revision 1.4  2006/02/21 18:10:19  antunes
; Improved shared code for C and T pixon.
;
; Revision 1.3  2006/02/16 20:18:55  antunes
; Refactored Reiser TPixon routines into procedures that work with
; my 'generic' Pixon preparation routines.
;
; Revision 1.2  2006/01/09 17:14:42  antunes
; More commented added.
; New 'super3drv' driver routine for getting user specs for reconstruction
; process now checked in.  Just type 'super3drv' to test.
;
;-            
pro stereo_tsetup,pxwrapper

; input   sr           rendering parameters
;         sp           pixon parameters
;         sc           misc.
;
; output  
;         sr.Istd      standard intensity according to instrument model
;         sc.machine
;         sc.date
;         sc.tag       unique tag for filenames
;------------------------------------------------------------------

common debugs,phys,tdsf,tdsft,dbg

sr=pxwrapper.sr
sp=pxwrapper.sp
sc=pxwrapper.sc

dbg = -1.0
dbug=0

tdsf = 0
tdsft = 0

print,'*** stereo_tsetup after memory free'
help,/mem

; need sr and sp for sure

N          =  sr.Ncube
tdsf       =  sc.tdsf
tdsft      =  sc.tdsft

sc.machine =  getenv('HOST')
sc.date    =  systime()

; ----- check for errors

err=0
Nd=n_elements(sr.gamma)  
if(n_elements(sr.LL)    ne Nd)then err=1
if(n_elements(sr.z0)    ne Nd)then err=2
if(n_elements(sr.eps)   ne Nd)then err=3
;if(n_elements(set)   ne Nd)then err=4
if(n_elements(sr.polar) ne Nd)then err=5
;if(n_elements(sc.sat_name) ne Nd)then err=6
if(n_elements(sr.k0)  ne Nd)then err=7
if(n_elements(sr.fan)   ne Nd)then err=8
if(n_elements(sr.theta) ne Nd)then err=9
q=where(finite(sr.z0) eq 0,count)
if(count gt 0)then err=11

if(err ne 0)then begin
  print,'ERROR ',err,' found in stereo_tsetup_07'
  stop
end

; -----

caldat,systime(/julian),mon,day,yr,hr,min,sec
fm = "(i2.2)"
sc.tag  =  string(yr-2000,fm) + string(mon,fm) + string(day,fm)       +$
         string(hr,fm)      + string(min,fm) + string(round(sec),fm)

if(n_elements(phys) eq 0)then phys = 1

if(dbug)then begin
print,'CHECK 1'
ans=''
read,'in stereo_tsetup (CR to continue) >',ans
if(ans eq 'x')then stop
end

; -----

idims  =  [N,N,N]
ddims  =  [N,N]

;Nd  =  N_elements(gamma)  ; number of data frames

; ----- Calculate Limb Darkening Fuctions

; ??? under new (07) code, cant call flimb yet, but
; this is where the polynomial coefficients (an) get
; entered into sr

;t_flimb,sr.ch,X,RR,sr.d,Xo,fa,fb,fc,ImI0,an
;sr.an=an
;ImI0=1.0; more accurately set in t_flimb.pro

; ----- calculate standard intensity

for i=0,Nd-1 do begin
    sr.Istd[i]  =  get_istd_Nd(sr.imodel[i],sr.L0,sr.n0,sr.L0tau[i],ImI0 = ImI0)
end

pxwrapper.sr=sr
pxwrapper.sc=sc

return
end

