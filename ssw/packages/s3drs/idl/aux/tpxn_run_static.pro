;+
;
; Project   : s3drs and Pixon reconstruction software
;                   
; Name      : tpxn_run_static
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: (part of the testing and legacy code, not documented herein)
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 

; Keywords  :
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : (see CVS logs)
;
; Written     : Sandy Antunes, NRC, 2007-2009
;               
;-            
; same as tpxn_run except no vertex add/remove 'anneal'

pro tpxn_run_static,oTPixon

; Initialization

   chi2 = oTPixon -> Get(/CHI2, /TOTAL)
   dof  = oTPixon -> Get(/DOF, /TOTAL)
   q    = (chi2 - dof)/SQRT(2*dof)

   qtol = oTPixon -> Get(/QTOL)

; run first pseudo step

   tstart = SYSTIME(/seconds)
   oTPixon -> Pseudo
   tpseudo = SYSTIME(/seconds) - tstart

   chi2 = oTPixon -> Get(/CHI2, /TOTAL)
   dof  = oTPixon -> Get(/DOF, /TOTAL)
   q    = (chi2 - dof)/SQRT(2*dof)
   print,'runtime ',tpseudo,' chi2 ',chi2,' dof ',dof,' q ',q

END
