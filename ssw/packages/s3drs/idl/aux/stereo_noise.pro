;+
; $Id: stereo_noise.pro,v 1.1 2009/04/22 18:31:44 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : stereo_noise
;               
; Purpose   : 
;               
; Explanation: 
;               
; Use       : IDL> 
;    
; Inputs    : 
;               
; Outputs   : 
;
; Keywords  : 
;
; Calls from LASCO : none 
;
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : Pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRL, Nov-Dec 2005
;               
; $Log: stereo_noise.pro,v $
; Revision 1.1  2009/04/22 18:31:44  antunes
; first half of relocation.
;
; Revision 1.2  2009/04/21 19:08:50  antunes
; Updated up through 'r' with SSW headers.  Also minor John bug fixes.
;
; Revision 1.1  2009/03/31 14:56:59  antunes
; Re-org prepatory to checking into SSW.
;
; Revision 1.5  2007/10/26 19:02:36  antunes
; Updated difference image needs. (Also made first cut at a back projection
; wrapper).
;
; Revision 1.4  2006/11/09 18:32:53  antunes
; Minor fixes and improvements.
;
; Revision 1.3  2006/10/20 17:58:43  antunes
; Lots of incremental mods to Pixon-related routines.
;
; Revision 1.2  2006/05/25 18:18:30  antunes
; Better handling.
;
; Revision 1.1  2006/04/11 15:55:06  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.2  2006/01/09 17:14:42  antunes
; More commented added.
; New 'super3drv' driver routine for getting user specs for reconstruction
; process now checked in.  Just type 'super3drv' to test.
;
;-            

pro stereo_noise,data,sn,sigma,addnoise=addnoise
;
; input    data    data array
;          sn      noise specification parameters
;		   (ftol for sigma specification, fsig for noise added
;                   to data)
; output   data    data with noise added if desired
;          sigma   noise sigma
;
;-----------------------------------------------------

addnoise=KEYWORD_SET(addnoise)

print,"debug: calling local Sandy copy of stereo_noise_06.pro"                  
;hit_any_key

noisemod = sn.noisemod      ; [] Noise model
seed     = sn.seed          ; [] random number seed for noise. -1 means no noise
fsig     = sn.fsig          ; [] if(seed ne -1) then noise added is fsig*max(data)
ftol     = sn.ftol          ; [?] Noise model parameter(s)

; un-transform it to raw data

Nx = n_elements(data[*,0,0])
Ny = n_elements(data[0,*,0])
Nd = n_elements(data[0,0,*])

ans=''
print,'NOTICE: This is a message from stereo_noise_06.pro. It calls'
print,'        transform_data_06.pro which has been changed and now'
print,'        requires theta, which is not available to stereo_noise_06.pro'
print,'        The call to transform_data has been commented out, so that'
print,'        calls to stereo_noise will give correct results for theta=90
print,'        but not otherwise. Hit <cr> to continue'
;read,'> ',ans

;for m=0,Nd-1 do begin
;  rdata[*,*,m] = transform_data(data[*,*,m],m,k0,tau,-1)
;end
if (addnoise) then rdata=double(data)

; add noise and calculate sigma

case noisemod of

'simple': begin		; constant gaussian noise
  if (addnoise) then rdata = (rdata + randomn(seed,Nx,Ny,Nd)*fsig*max(rdata)) > 0
  sigma = ftol*max(data)
  end

'poisson3': begin		; approximate poisson
  fd    = ftol*max(data)	; number of photons (N) is data/fd
  sigma = sqrt(fd*(data+fd))	; zero data gives zero sigma, so say variance is N+1 
  fd    = fsig*max(data)
  tol   = sqrt(fd*(data+fd))
  if (addnoise) then rdata = rdata + randomn(seed,Nx,Ny,Nd)*tol
  if (addnoise) then rdata = abs(rdata)
  end

'poisson4': begin            ; true poisson
  fd    = ftol*max(data)     ; number of photons (N) is data/fd
  sigma = sqrt(fd*(data+fd)) ; zero data gives zero sigma which is bad, so say
                             ; variance is N+1
  fd    = fsig*max(data) 
  if (addnoise) then rdata = rdata + fd*randomn(seed,Nx,Ny,Nd,poisson=data/fd)
  end

'poisson5': begin
; Assume the input data represents N=data/g photons and
; we will be adding Na photons as background noise. The noise
; added will be poisson distributed with a mean and variance of
; N+Na. ftol contains the following parameters:
; ftol[0] is 1/smax^2 where smax is the signal to noise ratio
;         at data maximum. In other words, ftol[0]*(max data)
;         is g, where N=data/g is # of photons.
; ftol[1] is the data value corresponding ta Na*g, the background noise.
; ftol[2] is the data maximum. If zero, use the actual data maximum.
; fsig    is the same as ftol[0] if actual noise consistent with ftol
;         is to be added to the data. It can be set to zero to
;         eliminate any noise being actually added to the data.

  if(ftol[2] eq 0)then datamax=max(data) else datamax=ftol[2]

  hdata = double(data)+ftol[1]
  g     = ftol[0]*datamax        ; number of photons is data/g
  sigma = sqrt(hdata*g)
  
  if (addnoise) then begin
      if(fsig gt 0)then begin
          gg    = fsig*datamax  ; number of photons is data/gg
          for i=0,Nx-1 do begin
;      print,i,Nx-1
              for j=0,Ny-1 do for k=0,Nd-1 do begin
                  rdata[i,j,k] = gg*randomn(seed,poisson=hdata[i,j,k]/gg)
              end
          end
      end
  endif

end

'poisson6': begin
   Smax = sqrt(1/sn.ftol[0])
   Smin = sqrt(1/sn.ftol[1])
   Dfmin=0
   Dfmax=max(data)
   a = (Dfmax-Dfmin)/(Smax^2-Smin^2)
   Nb = (Dfmax*Smin^2-Dfmin*Smax^2)/(Dfmax-Dfmin)
   sigma= sqrt(a*(data+a*Nb))
   if (addnoise) then begin
       rdata=data
       for i=0,Nx-1 do for j=0,Ny-1 do for k=0,Nd-1 do $
         rdata[i,j,k]=a*randomn(seed,poisson=data[i,j,k]/a+Nb) 
   endif
end

'sqrtsignal': begin
; here is a simple noise model where the signal, in level 1 B/B0
; units, and figure that the noise given as 'square root of
; all photons, divided by exposure time' is our goal.  We convert
; B/B0 into DN (or photons, if we can), figure out the percentage
; noise versus signal at the photon level, then use that percentage
; factor to get our percentage noise for B/B0.
;
; Basically, we use a formula that gives us the noise in terms of
;   sqrt[(photons/sec * exptime)] / exptime
; in our B/B0 scale.
; B2DN are the B/B0-to-DN conversion factors for each instrument, such that
;    L1_data_in_B/B0  * ftol[0] = DN/sec
; and DN2P are DN-to-photons, if available, such that
;    photons = ftol[1] * DN
; (if unknown, try a cor2-like 1 DN = 15 e-, i.e. ftol[1] = 15)
;
; Derivation is (where L1 is the L1 B/B0 data)
; L1 * c = DN/sec  and photons = g * DN, so
; noise_photons = noise_DN / sqrt(g) and
; noise_DN = sqrt(total_DN) / exp_time, where total_DN = L1 * c * exptime,
; and percentage noise is just noiseDN/total_DN, and thus
; noise in photons = noise in DN / sqrt(g),
; so noise in photons is just sqrt(L1 c exptime) / (exptime sqrt(g)) and
; percentage noise in photons is (noise_DN/sqrt(g)) / (L1 * c * exptime * g)
; which reduces to %_noise_photons = sqrt(e^3 g^2 c L1)^(-1), thus
; and since we are using %_noise_L1 = %_noise_photons then we get
; noise_L1 = L1 * %_noise_photons = L1 * exptime^-3/2 * g^-1 * (c L1)^-1/2
; which reduces to: noise_L1 = sqrt(L1) / [exptime^3/2 * g * sqrt(c)]

  ; note each data frame may have a different instrument
  sigma= sqrt(data) / ((sr.exptime)^(3/2.) * sr.msbperdn * sqrt(sr.dn2photon))

end

else: stop,'bad noisemod = '+noisemod
end  

; re-transform it to massaged data

;for m=0,Nd-1 do begin
;   data[*,*,m] = transform_data(rdata[*,*,m],m,k0,tau,1)
;end

if (addnoise) then data=rdata

return
end

