;+
;
; Project   : s3drs and Pixon reconstruction software
;                   
; Name      : fetch_instrmask
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: (part of the testing and legacy code, not documented herein)
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 

; Keywords  :
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : (see CVS logs)
;
; Written     : Sandy Antunes, NRC, 2007-2009
;               
;-            
FUNCTION fetch_instrmask,N,instr_name,optical_center,plate_scale

   case instr_name of

       "C2": mask=congrid(quickC2mask,N,N)

   return,mask

END
