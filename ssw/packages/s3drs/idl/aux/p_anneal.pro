;+
;
; Project   : s3drs and Pixon reconstruction software
;                   
; Name      : p_anneal, p_test_anneal
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: (part of the testing and legacy code, not documented herein)
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 

; Keywords  :
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : (see CVS logs)
;
; Written     : Sandy Antunes, NRC, 2007-2009
;               
;-            
PRO test_p_anneal

;;  .reset
;;  @cfluxrope_classic

;;  .reset
;;  @cfluxrope_recontest

; if I just run without using the Paul object in the Sandy run,
; I get Terrible-- 796 pixons vs 8430 pixons

; Oddly enough, restoring the Paul object at this point does not help(!)
;save,image,file='stopgap.sav'; Terrible-- 796 pixons vs 8430 pixons
;restore,'stopgap.sav',/verb; Terrible-- 796 pixons vs 8430 pixons

  jsched=[-1,0,0,0,0]
  msched=[50,100,100,100,100]
  rsched=[1.0E-05,1.0E-05,1.0E-05,1.0E-05,1.0E-05]
  npxn=13
  N=32
  Nd=2

;save,oPixon,file='stopgap.sav'; kinda worked, 7992 vs 8430 pixons at ->Map
restore,'stopgap.sav',/verb; kinda worked, 7992 vs 8430 pixons at ->Map
; BUT... by adding the following 'image=' line, we get a 100% match as
; long as we also restored the oPixon object in the previous line!!!
image=image*0.0+0.03125; only do for my cfluxrope, not the Paul case

idims = [N,N,N]
ddims = [N,N]
oData = oPixon->get(/oData)
oPixon->Setimage,image=image   ; insert starter image
oPixon->Set,sigma=sigma        ; insert sigma
for m=0,Nd-1 do oData[m]->Set, data=PTR_NEW(data[*,*,m])

;save,oPixon,file='stopgap.sav'; worked, identical, 100% perfect
;restore,'stopgap.sav',/verb; worked, identical, 100% perfect


scales = FINDGEN(npxn)
pxnidx = FLTARR(N,N,N)+npxn-1
  oPixon->Set, pxnidx=pxnidx
  oPixon->Set, mxit=msched[0]
  oPixon->Set, reltol=rsched[0]

;save,oPixon,file='stopgap.sav'; worked, identical, 100% perfect
;restore,'stopgap.sav',/verb; worked, identical, 100% perfect

  oPixon->pseudo
  annealsteps='LS'
  q=qval(oPixon)
  print,q
mxits   = msched[0]
reltols = rsched[0]
nj=n_elements(jsched)

;save,oPixon,file='stopgap.sav'; worked, identical, 100% perfect
;restore,'stopgap.sav'; worked, identical, 100% perfect

j=1
;j=2
;j=3
;j=4
  oPixon->Map

; TYPICALLY, if ->Map matches here, everything is identical afterwards.

  oPixon->PixonSmooth, /pxnidx
  pxnmap=oPixon->Get(/pxnidx, /nop)
  pxnidx=pxnmap > scales[jsched[j]]
  oPixon->Set, pxnidx = pxnidx
  oPixon->Set, mxit   = msched[j]
  oPixon->Set, reltol = rsched[j]
  oPixon->pseudo
  q=qval(oPixon)
  print,q,j

;j=1
j=2
;j=3
;j=4
  oPixon->Map
  oPixon->PixonSmooth, /pxnidx
  pxnmap=oPixon->Get(/pxnidx, /nop)
  pxnidx=pxnmap > scales[jsched[j]]
  oPixon->Set, pxnidx = pxnidx
  oPixon->Set, mxit   = msched[j]
  oPixon->Set, reltol = rsched[j]
  oPixon->pseudo
  q=qval(oPixon)
  print,q,j

;j=1
;j=2
j=3
;j=4
  oPixon->Map
  oPixon->PixonSmooth, /pxnidx
  pxnmap=oPixon->Get(/pxnidx, /nop)
  pxnidx=pxnmap > scales[jsched[j]]
  oPixon->Set, pxnidx = pxnidx
  oPixon->Set, mxit   = msched[j]
  oPixon->Set, reltol = rsched[j]
  oPixon->pseudo
  q=qval(oPixon)
  print,q,j

;j=1
;j=2
;j=3
j=4
  oPixon->Map
  oPixon->PixonSmooth, /pxnidx
  pxnmap=oPixon->Get(/pxnidx, /nop)
  pxnidx=pxnmap > scales[jsched[j]]
  oPixon->Set, pxnidx = pxnidx
  oPixon->Set, mxit   = msched[j]
  oPixon->Set, reltol = rsched[j]
  oPixon->pseudo
  q=qval(oPixon)
  print,q,j

threeview,oPixon->get(/im,/nop),/log,fixed=128


END

PRO p_anneal_test_plot

;  restore,/verb,'sandy_sol.sav'
  sandy_sol=oPixon->get(/im,/nop)
  restore,/verb,'paul_sol.sav'
  minus_sol=sandy_sol-paul_sol

  sandy_set=fltarr(32,32,3)
  paul_set=sandy_set
  minus_set=sandy_set
  orig_set=sandy_set

  sandy_set[*,*,0]=ijkproj(sandy_sol,'ij')
  sandy_set[*,*,1]=ijkproj(sandy_sol,'ik')
  sandy_set[*,*,2]=ijkproj(sandy_sol,'jk')
  paul_set[*,*,0]=ijkproj(paul_sol,'ij')
  paul_set[*,*,1]=ijkproj(paul_sol,'ik')
  paul_set[*,*,2]=ijkproj(paul_sol,'jk')
  minus_set[*,*,0]=ijkproj(minus_sol,'ij')
  minus_set[*,*,1]=ijkproj(minus_sol,'ik')
  minus_set[*,*,2]=ijkproj(minus_sol,'jk')
  orig_set[*,*,0]=ijkproj(timage,'ij')
  orig_set[*,*,1]=ijkproj(timage,'ik')
  orig_set[*,*,2]=ijkproj(timage,'jk')


  dset=[[[minus_set]],[[sandy_set]],[[paul_set]],[[orig_set]]]
  labels=['Sandy-Paul','Sandy-Paul','Sandy-Paul','Sandy','Sandy','Sandy', $
          'Paul','Paul','Paul','Orig','Orig','Orig']
  tv_multi,dset,labels=labels,/log,line=3,fixed=128,ct=3,$
    title='Comparison of cfluxrope sim: orig Paul run vs Sandy refactored code'

END


PRO p_anneal,oPixon,jsched,msched,rsched,npxn,N,pxwrapper

;common debugs,phys,tdsf,tdsft,dbg

  print,'Running Pixon annealing'

  scales = FINDGEN(npxn)
  pxnidx = FLTARR(N,N,N)+npxn-1

; ========== BEGIN THE LOOP

; detailed control of anneal schedule
; for each anneal step, the number of conjugate gradient
; minimization steps (mxit) and the pixon scale (j)
; are set separately. A record of pixon scales is
; kept in 'annealsteps' and a record of the mxit
; is kept in 'mxits'

; ----- do NNLS step

  if(msched[0] ne 0)then begin
      oPixon->Set, pxnidx=pxnidx
      oPixon->Set, mxit=msched[0]
      oPixon->Set, reltol=rsched[0]
      oPixon->pseudo
      annealsteps='LS'
      q=qval(oPixon)
  end

;stop,'Stopped in stereo_pixon after pseudo'

  mxits   = msched[0]
  reltols = rsched[0]

  if(q le 1)then begin
      pxnmap=oPixon->Get(/pxnidx, /nop)
      goto, donexx
  end else begin
      annealsteps=''
  end

; ========== do the anneal loop

  nj=n_elements(jsched)

  FOR j = 1,nj-1 DO BEGIN

      print,''
      print,'ANNEAL STEP '+string(j,"(i3)")+' of '+string(nj-1,"(i3)")+$
        ' scale='+string(jsched[j],"(i2)")+' mxit='+string(msched[j],"(i4)")

      oPixon->Map
      oPixon->PixonSmooth, /pxnidx
      pxnmap=oPixon->Get(/pxnidx, /nop)
      pxnidx=pxnmap > scales[jsched[j]]

      PRINT, "pxnidx          min-map     min-pxnidx"
      PRINT, jsched[j], MIN(pxnmap), MIN(pxnidx)
      oPixon->Set, pxnidx = pxnidx
      oPixon->Set, mxit   = msched[j]
      oPixon->Set, reltol = rsched[j]
      oPixon->pseudo
      annealsteps=[annealsteps,string(jsched[j],"(i2.2)")]
      mxits   = [mxits,msched[j]]
      reltols = [reltols,rsched[j]]
      q=qval(oPixon)

; save intermediate results

;  mdata=fltarr(N,N,Nd); data*0
;  for m=0,nd-1 do begin
;    image = oPixon->Get(/im,/nop)         ; get the reconstructed image
;    mdata[*,*,m] = oData[m]->dsf(image)
;  end

      pxwrapper.sc.totaltime=systime(-1)-pxwrapper.sc.t00

      pxncnt=oPixon->Get(/pxncnt)

      machine=pxwrapper.sc.machine
      npxn=pxwrapper.sp.npxn
      tempname=pxwrapper.sc.gname+'_'+pxwrapper.sc.tag+'_'+string(j,"(i2.2)")+'.dat'

;  save,filename=tempname,data,image,mdata,sigma,$
;  pxncnt,q,tdsf,tdsft,machine,version,sc,sp,userstruct,$
;  annealsteps,mxits,reltols,npxn,pxnmap

; end if q lt 1

      if(q lt 1.0)then goto, donexx

  ENDFOR

; ============

donexx:

END
