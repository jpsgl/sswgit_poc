;+
;
; Project   : s3drs and Pixon reconstruction software
;                   
; Name      : p_cropscale
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: (part of the testing and legacy code, documented in code)
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 

; Keywords  :
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : (see CVS logs)
;
; Written     : Sandy Antunes, NRC, 2007-2009
;               
;-            
; Given a FITS file e.g. noise or data, reads it in and bins and crops.
;
; Note there is 1 center shift and 2 size reductions:
; 'center' = new center of cropped region, default = no change
; 'cropfactor' = factor to crop down to, preserving pixel size, default = 2
; 'N'= desired rebinned size of final array, default = no change
;
; So note that, if you just pass it a file, the default behavior given
; no other options is to crop it down to 1/2 the original size,
; keeping the same image center.
;

FUNCTION p_cropscale,noisefile,center=center,cropfactor=cropfactor,N=N,$
                     pointer=pointer

  if (n_elements(cropfactor) eq 0) then cropfactor=[2,2]; default cropping
  if (n_elements(cropfactor) eq 1) then cf=[cropfactor,cropfactor] else $
    cf=cropfactor

  pointer=KEYWORD_SET(pointer)

  hdr=fitshead2struct(headfits(noisefile))

  if (n_elements(center) eq 0) then center=[hdr.NAXIS1/2,hdr.NAXIS2/2]
  if (n_elements(N) eq 0) then N=[hdr.NAXIS1,hdr.NAXIS2]
  if (n_elements(N) eq 1) then Nf=[N,N] else Nf=N

  if (hdr.tele_name eq 'SECCHI') then begin
      ndata=sccreadfits(noisefile)
      if (hdr.bunit ne 'MSB') then ndata=scc_img_trim(ndata,hdr)
  end else begin
      ndata=readfits(noisefile)
  end

  ; crop
  xspan=(n_elements(ndata[*,0])/cf[0])/2
  yspan=(n_elements(ndata[0,*])/cf[1])/2
  ndata=ndata[center[0]-xspan:center[0]+xspan-1,$
              center[1]-yspan:center[1]+yspan-1]

  ; scale

  scale=fltarr(2)
  scale[0]=float(hdr.NAXIS1)/float(Nf[0])
  scale[1]=float(hdr.NAXIS2)/float(Nf[1])

  D=[2*xspan,2*yspan]
  if (D[0] ne Nf[0]) then ndata=rebin(TEMPORARY(ndata),Nf[0],D[1]) * D[0]/Nf[0]
  if (D[1] ne Nf[1]) then ndata=rebin(TEMPORARY(ndata),D[0],Nf[1]) * D[1]/Nf[1]

  if (pointer) then return,ptr_new(ndata) else return,ndata

END
; Given a FITS file e.g. noise or data, reads it in and bins and crops.
;
; Note there is 1 center shift and 2 size reductions:
; 'center' = new center of cropped region, default = no change
; 'cropfactor' = factor to crop down to, preserving pixel size, default = 2
; 'N'= desired rebinned size of final array, default = no change
;
; So note that, if you just pass it a file, the default behavior given
; no other options is to crop it down to 1/2 the original size,
; keeping the same image center.
;

FUNCTION p_cropscale,noisefile,center=center,cropfactor=cropfactor,N=N,$
                     pointer=pointer

  if (n_elements(cropfactor) eq 0) then cropfactor=[2,2]; default cropping
  if (n_elements(cropfactor) eq 1) then cf=[cropfactor,cropfactor] else $
    cf=cropfactor

  pointer=KEYWORD_SET(pointer)

  hdr=fitshead2struct(headfits(noisefile))

  if (n_elements(center) eq 0) then center=[hdr.NAXIS1/2,hdr.NAXIS2/2]
  if (n_elements(N) eq 0) then N=[hdr.NAXIS1,hdr.NAXIS2]
  if (n_elements(N) eq 1) then Nf=[N,N] else Nf=N

  if (hdr.tele_name eq 'SECCHI') then begin
      ndata=sccreadfits(noisefile)
      if (hdr.bunit ne 'MSB') then ndata=scc_img_trim(ndata,hdr)
  end else begin
      ndata=readfits(noisefile)
  end

  ; crop
  xspan=(n_elements(ndata[*,0])/cf[0])/2
  yspan=(n_elements(ndata[0,*])/cf[1])/2
  ndata=ndata[center[0]-xspan:center[0]+xspan-1,$
              center[1]-yspan:center[1]+yspan-1]

  ; scale

  scale=fltarr(2)
  scale[0]=float(hdr.NAXIS1)/float(Nf[0])
  scale[1]=float(hdr.NAXIS2)/float(Nf[1])

  D=[2*xspan,2*yspan]
  if (D[0] ne Nf[0]) then ndata=rebin(TEMPORARY(ndata),Nf[0],D[1]) * D[0]/Nf[0]
  if (D[1] ne Nf[1]) then ndata=rebin(TEMPORARY(ndata),D[0],Nf[1]) * D[1]/Nf[1]

  if (pointer) then return,ptr_new(ndata) else return,ndata

END
