;+
;
; Project   : s3drs and Pixon reconstruction software
;                   
; Name      : get_lasco_solar_north
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: (part of the testing and legacy code, not documented herein)
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 

; Keywords  :
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : (see CVS logs)
;
; Written     : Sandy Antunes, NRC, 2007-2009
;               
;-            
; cludgy wrapper to get angle to rotate LASCO into STEREO's ecliptic frame

FUNCTION get_lasco_solar_north,hdr

          DEFSYSV, "!DRADEG", 180.0d/!DPI
          DEFSYSV, "!DDTOR", !DPI/180.0d
          if (tag_exist(hdr,'simdate')) then begin
              obsdate=hdr.simdate
          end else begin
              get_fits_time,hdr,obsdate
          end
          o1 = get_orbit(obsdate)
          tempxyz = [o1.gse_x,o1.gse_y,o1.gse_z]
          CONVERT_STEREO_COORD, obsdate, tempxyz, 'GSE', 'HAE' 
          tempxyz = tempxyz * 1000.0 ; convert LASCO GSE km to our meters
          ;angle=festival_ecl_solar_north(tempxyz[0],tempxyz[1],tempxyz[2])
    x=tempxyz[0]
    y=tempxyz[1]
    z=tempxyz[2]
 ; Routine taken from festival_ecl_solar_north and embedded in here
 ; so we can use Pixon even if Festival is not installed.
      r = SQRT(x^2.0d + y^2.0d + z^2.0d)
      ecl_lat = ASIN(z/r)
      ecl_lon = ATAN(y, x)
      beta = !dtor*82.746294d
      lambda = !dtor*345.59337d
      sx = COS(beta)*SIN(lambda - ecl_lon)
      sy = COS(ecl_lat)*sin(beta) - SIN(ecl_lat)*COS(lambda - ecl_lon)
      angle = ATAN(sx, sy) * !radeg

          return,angle

END

