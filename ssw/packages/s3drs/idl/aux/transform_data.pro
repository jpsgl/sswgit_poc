
;+
; $Id: transform_data.pro,v 1.1 2009/04/22 18:31:50 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : transform_data
;               
; Purpose   : 
;               
; Explanation: 
;               
; Use       : IDL> 
;    
; Inputs    : 
;               
; Outputs   : 
;
; Keywords  : 
;               
; Calls from LASCO : none 
;
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : Pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRL, Nov-Dec 2005
;               
; $Log: transform_data.pro,v $
; Revision 1.1  2009/04/22 18:31:50  antunes
; first half of relocation.
;
; Revision 1.2  2009/04/21 19:08:52  antunes
; Updated up through 'r' with SSW headers.  Also minor John bug fixes.
;
; Revision 1.1  2009/03/31 14:57:05  antunes
; Re-org prepatory to checking into SSW.
;
; Revision 1.5  2006/11/16 19:33:06  antunes
; Successfully replaced k0 with p_getk0 to allow for pointers
; and thus dynamic image loading.
;
; Revision 1.4  2006/11/16 18:28:07  antunes
; Added these back here, they were temporarily deleted.
;
; Revision 1.2  2006/11/09 18:32:54  antunes
; Minor fixes and improvements.
;
; Revision 1.1  2006/04/11 15:55:06  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.2  2006/01/09 17:14:42  antunes
; More commented added.
; New 'super3drv' driver routine for getting user specs for reconstruction
; process now checked in.  Just type 'super3drv' to test.
;
;-            

function transform_data,data,s,k0,tau,dir
; version 05

; input data - an nxn array
;       s    - data frame index
;       k0   - Nd ptr to 2x array: the indices of the optical center of the detectors
;       tau  - Tangent of plate scale
;       dir  - direction of transformation:
;              +1 raw to transf
;              -1 transf to raw
; return     - tdata - transformed data
;
; for the forward (+1) transform, the data is divided
; by cosine of the angle of the pixel from the camera z axis 
; (cosine theta). For the reverse (-1) transform, the process
; is reversed.

; --------------------------------------

n  = n_elements(data(*,0))
nn = (double(n)-1)/2

odata = data*0 	; output array

xct=data*0

for i=0,n-1 do for j=0,n-1 do begin

;  di = i-k0[0,s]
;  dj = j-k0[1,s]
  di = i-p_getk0(k0,s,0)
  dj = j-p_getk0(k0,s,1)
  ct = 1/sqrt(1+tau^2*(di*di+dj*dj))	; cosine theta
  xct[i,j]=ct

  case dir of

   1: begin				; raw to transformed
      odata[i,j] = data[i,j]/ct
      end

  -1: begin				; transformed to raw
      odata[i,j] = data[i,j]*ct 
      end

  else: stop,'Error in transform data'

  end

end

return,odata
end 
