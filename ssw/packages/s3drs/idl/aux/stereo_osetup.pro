;+
; $Id: stereo_osetup.pro,v 1.1 2009/04/22 18:31:45 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : stereo_osetup
;               
; Purpose   : 
;               
; Explanation: 
;               
; Use       : IDL> 
;    
; Inputs    : optional infile: filename to process
;               
; Outputs   : file T 
;
; Keywords  : 
;               
; Calls from LASCO : none 
;
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : Pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRL, Nov-Dec 2005
;               
; $Log: stereo_osetup.pro,v $
; Revision 1.1  2009/04/22 18:31:45  antunes
; first half of relocation.
;
; Revision 1.2  2009/04/21 19:08:50  antunes
; Updated up through 'r' with SSW headers.  Also minor John bug fixes.
;
; Revision 1.1  2009/03/31 14:57:00  antunes
; Re-org prepatory to checking into SSW.
;
; Revision 1.13  2008/01/25 16:10:38  antunes
; Major refactoring of code to allow differing FOVs.  Note Pixon
; structures here are now incompatible with earlier versions!
;
; Revision 1.12  2007/02/21 16:43:20  antunes
; Memory mods to allow for higher N (N=256 now works!)
;
; Revision 1.11  2007/01/09 20:06:03  antunes
; Test now works with pixon tetra
;
; Revision 1.10  2006/11/30 20:03:57  antunes
; Updated pixon and other printing and output routines.
;
; Revision 1.9  2006/11/17 19:02:33  antunes
; Updates to Pixon structures.
;
; Revision 1.8  2006/11/16 18:27:33  antunes
; Did a rollback then an update to allow pixon wrapper to handle
; heterogeneous images.
;
; Revision 1.5  2006/05/25 18:18:30  antunes
; Better handling.
;
; Revision 1.4  2006/05/02 18:47:58  antunes
; Added test wrappers for Pixon tests.
;
; Revision 1.3  2006/04/27 18:53:48  antunes
; Allow for multiple 'dragger' and reentrant GUI, removed last common,
; also testing masks in pixon.
;
; Revision 1.2  2006/04/24 19:04:01  antunes
; Fixed shared c/t pixon path environment problem.  Redid GUI to
; use widget struct rather than common so it's reentrant.  Also
; added in better comments, user input.  Now GUI is ready for
; prime time for render testing.
;
; Revision 1.1  2006/04/11 15:55:06  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.6  2006/03/16 20:25:28  antunes
; Improved pixon calling, wrote proto-frontend GUI for
; different rendering tests.
;
; Revision 1.5  2006/03/08 17:52:23  antunes
; Abstracted pixon structures into an aggregate structure in order
; to be encapsulate Pixonisms from general rendering and reconstruction
; routines.
;
; Revision 1.4  2006/02/21 18:10:19  antunes
; Improved shared code for C and T pixon.
;
; Revision 1.3  2006/01/09 17:14:42  antunes
; More commented added.
; New 'super3drv' driver routine for getting user specs for reconstruction
; process now checked in.  Just type 'super3drv' to test.
;
;-            
pro stereo_osetup,pxwrapper,timage,oPixon

; input   sr           rendering parameters
;         sp           pixon parameters
;         sc           misc.
;
; output  oPixon       pixon object (with timage)
;         userstruct   input structure for dsf, dsft
;         sr.istd      standard intensity according to instrument model
;         sc.machine
;         sc.date
;         sc.tag       unique tag for filenames
;------------------------------------------------------------------

common debugs,phys,tdsf,tdsft,dbg

print,"Obsolete-- use p_osetup now"

sr=pxwrapper.sr
sp=pxwrapper.sp
sc=pxwrapper.sc

dbg = -1.0
dbug=0

tdsf = 0
tdsft = 0

IF (OBJ_VALID(oPixon_map)) THEN OBJ_DESTROY, oPixon_map, /FREE
IF (OBJ_VALID(oPixon_fit)) THEN OBJ_DESTROY, oPixon_fit, /FREE
if (obj_valid(oPixon)) then obj_destroy,oPixon,/free
;heap_gc,/verb
heap_gc

print,'*** stereo_osetup after memory free'
help,/mem

; need sr and sp for sure

Ncube          =  sr.Ncube
Ndata          =  sr.Ndata[0]; uses first data as N for all rest, bad
fwhm_2d    =  0
fwhm_3d    =  0
tdsf       =  sc.tdsf
tdsft      =  sc.tdsft

machine    =  getenv('HOST')
sc.machine =  machine
sc.date    =  systime()

; ----- check for errors

err=0
Nd=n_elements(sr.gamma)  
if(n_elements(sr.LL)    ne Nd)then err=1
if(n_elements(sr.z0)    ne Nd)then err=2
if(n_elements(sr.eps)   ne Nd)then err=3
;if(n_elements(set)   ne Nd)then err=4
if(n_elements(sr.polar) ne Nd)then err=5
;if(n_elements(sc.sat_name) ne Nd)then err=6
;if(n_elements(sr.k0[0,*])  ne Nd)then err=7
if(n_elements(sr.fan)   ne Nd)then err=8
if(n_elements(sr.roco)  ne Nd)then err=9
q=where((sr.roco ne 0) and (sr.roco ne 1),c)
if(c gt 0)then err=10
q=where(finite(sr.z0) eq 0,count)
if(count gt 0)then err=11

if(err ne 0)then begin
  print,'ERROR ',err,' found in stereo_osetup_06'
  stop
end

; -----

caldat,systime(/julian),mon,day,yr,hr,min,sec
fm = "(i2.2)"
sc.tag  =  string(yr-2000,fm) + string(mon,fm) + string(day,fm)       +$
         string(hr,fm)      + string(min,fm) + string(round(sec),fm)

if(n_elements(phys) eq 0)then phys = 1

if(dbug)then begin
;print,'CHECK 1'
ans=''
read,'in stereo_osetup (CR to continue) >',ans
if(ans eq 'x')then stop
end

; -----

idims  =  [Ncube,Ncube,Ncube]
ddims  =  [Ndata[0],Ndata[0]]

;Nd  =  N_elements(gamma)  ; number of data frames

; ----- Calculate Limb Darkening Fuctions

;o_flimb,sr.chl,N,1/sr.L0tau,sr.d,sr.lla0,fa,fb,fc,rmin2,an
;sr.an=an

; ----- calculate standard intensity

;sr.istd  =  get_istd(1,sr,ImI0 = ImI0)
for m=0,Nd-1 do begin
    dvoxfrac=sr.dvox/sr.L0tau[m]
    o_flimb,sr.chl,Ncube,1/sr.L0tau[m],dvoxfrac,sr.lla0,fa,fb,fc,rmin2,an
    sr.an=an
    sr.Istd[m]  =  get_istd_Nd(sr.imodel[m],sr.L0,sr.n0,sr.L0tau[m],ImI0 = ImI0)
end

; ----- Create Pixon Object 

jjj    =  check_math(0,1)	
prf2d  =  gprf(ddims, fwhm = fwhm_2d)
jjj    =  check_math()

data      =  dblarr(Ndata,Ndata,Nd)+1     ; dummy data
sigma     =  0.05                ; dummy sigma
true_nfd  =  dblarr(Ndata,Ndata,Nd)

data_ptr  =  PTRARR(Nd)
FOR m  =  0,Nd-1 DO data_ptr[m]  =  PTR_NEW(data[*,*,m])

user  =  PTRARR(nd)
fprf  =   PrepFPRF( prf2d, ddim=[Ndata,Ndata], idim=[Ndata,Ndata], /FREE )
fprf = fprf[0]

tau2=make_array(Nd,/double)
tau2 = sr.tau*sr.tau

if(dbug)then begin
;print,'CHECK 2'
ans=''
read,'in stereo_osetup (CR to continue) >',ans
if(ans eq 'x')then stop
end

;mystruct  =  {uservariable,   $
mystruct  =  { $
fprf:    fprf,              $
roco:    0L,                $
polar:   'x',               $
c2z:     dblarr(Ndata,Ndata),       $
s2z:     dblarr(Ndata,Ndata),       $
lla0:    dblarr(3),         $
rho2:    dblarr(Ndata,Ndata),       $
rhos2:   dblarr(Ndata,Ndata),       $
psLs:    dblarr(Ndata,Ndata),       $
psLs2:   dblarr(Ndata,Ndata),       $
i:       dblarr(Ndata,Ndata),       $
jc:      dblarr(Ndata,Ndata),       $
K2:      dblarr(Ndata,Ndata),       $
d2:      0.0d0,             $
fa:      fa,                $
fb:      fb,                $
fc:      fc,                $
rmin2:   rmin2,             $
tau:     0.0d0,             $
tau2:    0.0d0,             $
interp:  sr.interp,         $
machine: machine,           $
g:       0.0d0              $
}

;userstruct  =  REPLICATE({uservariable},Nd)
userstruct = REPLICATE(mystruct,Nd)

if(dbug)then begin
;print,'CHECK 3'
ans=''
read,'in stereo_osetup (CR to continue) >',ans
if(ans eq 'x')then stop
end

for k = 0,Nd-1 do begin

  ffunc,sr,k,ps,mu,nu,is,jc,K2,u,x,y,rhos2,c2z,s2z

  dvoxfrac=sr.dvox/sr.L0tau[m]

  rho2   =  x*x+y*y
  psLs   =  ps*sr.LL[k]
  psLs2  =  psLs^2

  userstruct[k].fprf       =  fprf           ; ???
  userstruct[k].roco       =  sr.roco[k]        ; first execute image = rotate(image,roco)
  userstruct[k].polar      =  sr.polar[k]       ; polarization (t,r,p, or b)
  userstruct[k].c2z        =  c2z            ; 
  userstruct[k].s2z        =  s2z            ; 
  userstruct[k].lla0       =  sr.lla0           ; sun center
  userstruct[k].rho2       =  rho2           ; rho^2 array
  userstruct[k].rhos2      =  rhos2          ; rho_s^2 array
  userstruct[k].psLs       =  psLs           ; ps*Ls array
  userstruct[k].psLs2      =  psLs2          ; (ps*Ls)^2 array
  userstruct[k].i          =  is             ; i_s array
  userstruct[k].jc         =  jc             ; j_c array
  userstruct[k].K2         =  K2             ; K_2 array
  userstruct[k].d2         =  dvoxfrac*dvoxfrac ; normed d^2
  userstruct[k].fa         =  fa*dvoxfrac^3         ; limb darkening function
  userstruct[k].fb         =  fb*dvoxfrac^3         ; limb darkening function
  userstruct[k].fc         =  fc*dvoxfrac^3         ; limb darkening function
  userstruct[k].rmin2      =  rmin2          ; limb darkening function index offset
  userstruct[k].tau        =  sr.tau[k]            ; tangent of plate scale
  userstruct[k].tau2       =  tau2[k]          ; tangent of plate scale squared
  userstruct[k].interp     =  sr.interp         ; interpolation code
  userstruct[k].machine    =  machine        ; getenv(HOST) = machine name
  userstruct[k].g          =  sr.gamma[k]       ; view angle in degrees 
end

for i = 0,nd-1 do user[i]  =  PTR_NEW(userstruct[i])


;help,user
;help,user[0]
;help,*user[0]
;help,*(user[0])
;help,*user[0],/str
;help,*(user[0]),/str


; ----- create Pixon object

prf3d   =  gprf(idims, fwhm  =  fwhm_3d)
nodisk  =  1 

;print,'before oPixon OBJ_NEW'
;help,/mem

if(dbug)then begin
print,'CHECK 4'
ans=''
read,'in stereo_osetup (CR to continue) >',ans
if(ans eq 'x')then stop
end

;weight =  make_array(Ndata,Ndata,Nd,value=1);
;weight[0:Ndata/2,0:Ndata/2,*]=0; ignore one quarter of each datum

; TEMPORARY bits are from largearray

;wt     = weight,
oPixon = OBJ_NEW('Pixon', $
data   = TEMPORARY(data_ptr),        $
prf    = TEMPORARY(prf3d),           $
image  = TEMPORARY(timage),          $
sigma  = sigma,           $
npo    = sp.npo,          $
npxn   = sp.npxn,         $
anneal = sp.anneal_3d,    $
reltol = sp.reltol,       $
abstol = sp.abstol,       $
user   = user,            $
nodisk = nodisk,          $
mxit   = sp.mxit,         $
dllf   =  0               $
)

if(dbug)then begin
print,'CHECK 5'
ans=''
read,'in stereo_osetup (CR to continue) >',ans
if(ans eq 'x')then stop
end

;print,'after oPixon OBJ_NEW'
;help,/mem

pxwrapper.sr=sr
pxwrapper.sc=sc
;us2={userstruct:userstruct}
;pxwrapper = create_struct(pxwrapper,us2) ; appends to existing pxwrapper
;  now we use a better way
;pxwrapper=addrep_struct(pxwrapper,'userstruct',userstruct)

return
end

