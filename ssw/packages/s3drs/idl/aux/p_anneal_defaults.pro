FUNCTION p_anneal_defaults
;+
; $Id: p_anneal_defaults.pro,v 1.1 2009/04/22 18:31:29 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : p_anneal-defaults.pro
;               
; Purpose   : Sets annealing constants for Pixon
;               
; Explanation: 
;               
; Use       : IDL> anneal_specs = p_anneal_defaults
;    
; Inputs    : none
;               
; Outputs   : returns a Pixon anneal parameters structure
;
; Keywords  : none
;               
; Calls from LASCO : none 
;
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : Simulation
;               
; Prev. Hist. : See Paul Reiser's code and notes.
;
; Written     : Sandy Antunes, NRL, Nov-Dec 2005
;               
; $Log: p_anneal_defaults.pro,v $
; Revision 1.1  2009/04/22 18:31:29  antunes
; first half of relocation.
;
; Revision 1.1  2009/03/31 14:56:44  antunes
; Re-org prepatory to checking into SSW.
;
; Revision 1.1  2006/05/25 18:18:30  antunes
; Better handling.
;
; Revision 1.2  2005/12/27 17:48:24  antunes
; Commented half of the routines using SSW style.  Also added a new
; axes test case 'arrowcube'.
; Checked in the Tomography/old/mikic_img.pro routine here because it
; is a dependency for the general image-prep routine 'get_timage.pro'
;
;-            

; since annealing is often 0, these defaults are set here to help
; keep the usual invocation less crowded

  anneal_3d  =  0
  jsched = [-1,0,0,0,0]
  msched = [50,100,100,100,100]
  rsched = [1,1,1,1,1]*1.0e-5

  anneal_specs = {$
      anneal_3d: anneal_3d, $ ; [] Pixon: anneal factor
      jsched: jsched, $ ; [?] schedule of pixon sizes for anneal loop
      msched: msched, $ ; [?] schedule of mxit values for anneal loop
      rsched: rsched $ ; [?] schedule of reltol values for anneal loop
  }

  return,anneal_specs
END

