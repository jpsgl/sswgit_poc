;+
;
; Project   : s3drs and Pixon reconstruction software
;                   
; Name      : get_istd
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: (part of the testing and legacy code, not documented herein)
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 

; Keywords  :
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : (see CVS logs)
;
; Written     : Sandy Antunes, NRC, 2007-2009
;               
;-            
; obsolete in favor of minor modified 'get_istd_Nd' function,
; which is designed to work with hetereogenous data sets.
; So this function warns if called.

function get_istd,model,sr,ImI0=ImI0
; version 05

; Calculate Istd

; input   model - instrument model   
;         sr input structure
;         imI0      ratio of Im/I0 as calculated by the flimb (for model 0 only)

print,'warning, use get_istd_Nd instead'

; Cox, Arthur N., "Allen's Astrophysical
; Quantities/editor, Arthur Cox", Springer-Verlag, 14th ed., NY, 2000.
;--------------------------------------------------------------------------------

; some shorter names for input data

L0    = sr.L0	; average satellite distance
L0tau = sr.L0tau ; L0*plate scale
n0    = sr.n0	; standard electron density (electrons/cc)

; ----- Physical Constants

e2 = 23.070796e-20   ; elementary charge squared (esu)         [Allen]
m  = 9.109389754e-28 ; electron mass             (g)           [Allen]
c  = 2.99792458e10   ; speed of light            (cm/sec)      [Allen]
ss = (e2/(m*c*c))^2  ; Thompson Cross section    (cm^2/sr)
AU = 1.495979e13     ; 1 AU                      (cm)          [Allen]
h  = 6.6260755e-27   ; Planck's constant         (erg-sec)          [Allen]
Im = 2.009e10        ; mean solar intensity      (erg/sec/cm^2/sr)  [Allen]

; ----- duplicate Allen's data

; wavelength (A)

wA = [20,22,24,26,28,30,32,34,36,37,38,39,40,41,42,43,44,45,46,48,50,55,60,65,70,75,80,90,100,$
110,120,140,160,180,200,250,300,400,500]*.01
wA=wA*1.0e4

; central intensity (erg/sec/cm^2/sr/A)

IwA = [1.4,13,13,37,60,134,167,189,196,228,216,208,297,338,345,312,361,387,395,384,361,$
343,317,281,246,218,194,157,125,101,84,56,40,27,18,8.1,4.1,1.35,.57]*0.01
IwA=IwA*1.0e6

; convert IwA to photons  (photons/sec/cm^2/sr/A)

IwA = IwA*wA*1.0e-8/(h*c)

; ----- calculate proportionality constant K

K = !pi*ss*L0tau^3*n0*AU/(2*L0^2)

K = K*L0^2/L0tau^2

; ----- resample w and Iw to a finer grid

 dw = 1.0d0
; w = scale_tomo(min(wA),max(wA),dw)
 w = scale(min(wA),max(wA),dw)
 Iw = interpol(IwA,wA,w)

; ----- Implement Model

case model of

0: begin                   ; Paris, Palo Alto
   wl = 555.0e-7           ; typical solar wavelength  (cm)
   Istd = K*0.1*(Im/ImI0)  ; (erg/sec/cm^2/sr/A) 
   Istd = Istd*wl/(h*c)    ; convert to photons (photon/sec/cm^2/sr/A) 
   end

1: begin		; 
   
   FWHM=1000.	; width of instrument function
   w0 = 7000.	; central wavelength
   Q0 = 0.2	; peak efficiency

   v = FWHM/(2*sqrt(2*alog(2)))	; variance

   Q=Q0*exp(-(w-w0)^2/(2*v*v))	; efficiency function
   Istd = K*total(Q*Iw)*dw

   end

else: stop,'bad model = '+string(model)
end

return, Istd
end
