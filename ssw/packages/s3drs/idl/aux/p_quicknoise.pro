;+
;
; Project   : s3drs and Pixon reconstruction software
;                   
; Name      : p_quicknoise
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: (part of the testing and legacy code, documented in code)
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 

; Keywords  :
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : (see CVS logs)
;
; Written     : Sandy Antunes, NRC, 2007-2009
;               
;-            
; test case:
; quickcme,64,'20071231_022220','20071231_012220',/cor2,/setup,/checksn,/debug

; returns a quick sqrt(photon) noise for a given pxwrapper set
; Incoming dataset(s) can be a single array or a set of arrays
; (and can be pointers or actual arrays)
;
; requires either 'pxwrapper' entity or MSBperDN and DNperPhoton,
; otherwise assumes incident data is already in photons.
;
; If given one dataset (single, array of data, ptr, or ptr array),
;    returns its noise.
; If given two datasets (each a single, array of data, ptr or ptr array),
; returns their total noise presuming they were subtracted
;   (e.g. a difference image or bckg subtraction).
; Assumed units are MSB (and uses MSBperDN or pxwrapper.sr.MSBperDN to enforce)
; If data units are DN and MSBperDN is 1, then also works.
;
; If you give pxwrapper to get the constants but the incoming data
; is only a singlet and not the same as pxwrapper.sr.Nd, then you
; really should give the 'iele' element, which otherwise defaults
; to using the 0th element of pxwrapper.sr.  'iele' should only
; be used when processing single data frames, not sets of data.
;
; Optional flag 'photons' returns noise in photons, not in the
; same units as the original input data.
;
; Also adds the given biasdev, if any (as input or from pxwrapper).
;  Alternately, it will default to a Cor2 readout bias of 0.019DN
;
; As an alternative mode, it accepts input as DN (as per the
; lab's pre-made total brightness images in DN) and does the
; appropriate calculation into MSB->photons, then returns a
; noise in the same input DN units.
;

FUNCTION p_quicknoise,dataM,dataN,photons=photons,keepdn=keepdn,$
                      pxwrapper=pxwrapper,MSBperDN=MSBperDN,$
                      DNperPhoton=DNperPhoton,iele=iele,$
                      biasdev=biasdev,exptime=exptime,random=random,$
                      ingestDN=ingestDN,baseres=baseres,debug=debug


    photons=KEYWORD_SET(photons)
    random=KEYWORD_SET(random)
    ingestdn=KEYWORD_SET(ingestdn)
    keepdn=KEYWORD_SET(keepdn)
    debug=KEYWORD_SET(debug)
    if (n_elements(DNperPhoton) eq 0) then DNperPhoton=15
    if (n_elements(biasdev) eq 0) then begin
        biasdev=0.019      ; 0.019DN in 6 sec, in case /readout is used
    end

    polar='b'; default is total brightness

    if (n_elements(pxwrapper) ne 0) then begin
        MyMSBperDN=pxwrapper.sr.MSBperDN
        DNperPhoton=pxwrapper.sr.DNperPhoton
        exptime=pxwrapper.sr.exptime
        biasdev=pxwrapper.sr.biasdev
        baseres=pxwrapper.sr.ndata*pxwrapper.sr.scaling
        polar=pxwrapper.sr.polar
    end

    if (n_elements(baseres) eq 0) then baseres=2048; guess at native rez

    if (n_elements(iele) ne 0) then begin
        ; make sure incoming contants (esp. from pxwrapper) match data
        MyMSBperDN[0]=MSBperDN[iele]
        DNperPhoton[0]=DNperPhoton[iele]
        polar[0]=polar[iele]
    end

    ndim=size(dataM,/n_dimensions); 1=ptrs, 2=1 data, 3=array of data

    if (ndim eq 1) then ict=n_elements(dataM) else $
      if (ndim eq 2) then ict=1 else ict=n_elements(dataM[0,0,*])

    ; handles incoming DN as an alternative case
    if (n_elements(MSBperDN) ne 0) then MyMSBperDN=MSBperDN
    if (ingestdn eq 1 or n_elements(MSBperDN) eq 0) then $
      MyMSBperDN=fltarr(ict)+1.0

    if (ndim eq 1) then sizes=[size(*(dataM[i]),/dimensions),ict] else $
      sizes=size(dataM,/dimensions)
    pnoise=make_array(sizes,/float,value=0.0)

    ; do this after we know ict to pack exptime into appropriate array size
    if (n_elements(exptime) eq 0) then begin
      ; guess at usual Cor2A exp
        myexptime=make_array(/float,ict,value=6.0) 
    end else if (n_elements(exptime) eq 1 and ict gt 1) then begin
        myexptime=make_array(/float,ict,value=exptime) 
    end else begin
        myexptime=exptime
    end

; Compute noise and put into DN units

    for i=0,ict-1 do begin
        if (ndim eq 1) then datain=*(dataM[i]) else datain=dataM[*,*,i]

        ; use of 'abs' is questionable, for difference images,
        ; but kept here somewhat arbitrarily

        dn_persec_perpixel= abs(datain)/MyMSBperDN[i]
;;;        photons_persec_perpixel=dn_persec_perpixel*DNperPhoton[i]
        photons_persec_perpixel=dn_persec_perpixel / float(DNperPhoton[i])
        noisephoton=sqrt(photons_persec_perpixel * myexptime[i])

        if (n_elements(biasdev) ne 1) then bias=biasdev[i] else $
          bias=biasdev

        if (debug) then begin
            print,'datain min, datain max',minmax(datain)
            print,'msbperdn, dnperphoton',mymsbperdn[i], dnperphoton[i]
            print,'dn_persec_perpixel min, max',minmax(dn_persec_perpixel)
            print,'photons_persec_perpixel min, max',$
              minmax(photons_persec_perpixel)
            print,'noisephoton min, max',minmax(noisephoton)
            print,'datain min, datain max',minmax(datain)
            print,'noisephoton min, max, plus dnperphoton',$
              minmax(noisephoton),DNperPhoton[i]
            print,'bias',bias
        end

;;;        noiseDN=noisephoton/DNperPhoton[i]
        noiseDN=noisephoton * DNperPhoton[i]

;        print,msbperdn[i],dnperphoton[i],max(datain),max(photons_persec_perpixel),max(noisedn)

        ; now also add in resolution-dependent and exp time-dependent factors

; CORRECTION-- since we are not summing up areas but simply sampling
;              we should not apply correction for binning, just
;              exposure time.

        ; Advantage of Binning = lower noise
;        ndata=n_elements(datain[0,*])
;        help,ndata,i
;        if (n_elements(baseres) gt 1) then resdiff=baseres[i]/ndata else $
;          sqrtresdiff=baseres/ndata; handle both scalar and array inputs
       ; e.g. for a 2048x2048 frame sampled to 64x64, resdiff=32^2 so
       ;  sqrtresdiff=sqrt(32^2) = 32

        ; resdiff is a hack!!!
;        resdiff=6 * 2048.0/64.0; best value for s/n plots, why???

;        noiseDN = noiseDN / float(resdiff); 

        ; Advantage of longer exposure = lower noise
;;;        noiseDN = noiseDN / sqrt(myexptime[i])

        ; polarization trios are essentially 3x signal, thus 1/sqrt(3.0) noise
        if (polar[i] eq 'b') then noiseDN = 0.577350 * noiseDN

        ; add in biasdev in quadrature
        ; we apply an average readout noise
;bias=0; hack hack, noiseDN for sims is sooo low that bias overwhelms!!!
        if (debug) then begin
            print,'minmax of noiseDN, noiseDN^2, bias, bias^2'
            print,minmax(noiseDN),minmax(noiseDN^2),bias,bias^2
            print,'done'
        end

        noiseDN = sqrt (noiseDN^2 + bias^2)

        ; area of pixel Cor2 at N=64 is 0.0136 cm and MSB is per cm^2 so
;;;        ndata=n_elements(datain[0,*])
        noffset=1.0;0.15
;;;        print,ndata,noffset
        noiseDN = noiseDN * 0.0136 * noffset ;0.15; (64.0/(double(ndata)))^3; * 0.01
;
; N=64 noise 0.0136 converges quickly and accurately
; N=128 above times 0.15 slow to converge but accurate, noise at 1.0
; is still faint but might eventually be promising, noise at 8x yields crap.
; interesting, for N=128 a factor of noffset yields initial Q of:
;      0.01  5.7E+10
;      0.125 3.7E+08
;      0.15  2.6E+08,
;      0.25  9.3E+08
;      0.3   6.4E+07
;      0.5   2.3E+07
;      1.0   5.8E+06
; (for gamma=[-45,0,45])

        pnoise[*,*,i] = noiseDN

    end


    ; recursively get background noise, if needed
    if (n_elements(dataN) ne 0) then begin

        ; since our running noise tally is DN, we force DN here with /keepdn

        pnoise_B_DN = p_quicknoise(dataN,photons=photons,/keepdn,$
                                   pxwrapper=pxwrapper,MSBperDN=MSBperDN,$
                                   DNperPhoton=DNperPhoton,random=random,$
                                   biasdev=biasdev,exptime=exptime,$
                                   iele=iele,ingestDN=ingestDN,baseres=baseres)

        ; now add in quadrature like any noise calculation

        for i=0,ict-1 do $
          pnoise[*,*,i]=sqrt( pnoise[*,*,i]^2 + pnoise_B_DN[*,*,i]^2 )

    end



    if (random) then begin
        ; add poisson-like randomization
        N=n_elements(pnoise[*,0,0])
        pnoise= pnoise * randomn(seed,N,N,ict)
    end

    if (keepdn) then begin
        ; if they wanted DN, not orig units
        ; do nothing

    end else if (photons) then begin
        ; if they wanted photon, not orig units
        for i=0,ict-1 do pnoise[*,*,i]=pnoise[*,*,i] / DNperPhoton[i]
    end else begin
       ; otherwise, convert back to original units
        for i=0,ict-1 do $
          pnoise[*,*,i] = pnoise[*,*,i] * MyMSBperDN[i]
    end


    if (debug) then begin
        print,'final result: minmax of datain and noise'
        help,dataM,pnoise
        print,'data: ',minmax(dataM)
        print,'noise: ',minmax(pnoise)
    end

    return,pnoise



END


; here is a quick back-of-the-envelope noise comparison
; that accepts Data in 'DN' and generates an appropriate Noise
;

FUNCTION p_quicknoise_alt,dataA,backA

  N=n_elements(dataA[*,0])

  Nfactor=2048/float(N)         ; e.g. 1/16 for N=128 so Nfactor=16

  ; ** NOISE
  ;  noise(DN_sec)=noise(DN_tot)/6sec=noise(p*15)/6sec
  ;  noise(p) = sqrt(p) = sqrt(DN_tot/15) = sqrt(DN_sec*6sec/15)
  ; so noise(DN_sec)=sqrt(DN_sec*6/15)/6sec = sqrt(6/15)/6.0 * sqrt(DN_sec)
  ; so noise(DN_sec)=0.105409 * sqrt(DN_sec)
  ; Also noise total is sqrt of sum of 2 frames, roughly 1.4 each frame:
  ;   noise(DN_sec_diff) ~= 0.15 sqrt(DN_sec_raw)
  ;
  ; Since DN_diff = Q * DN_raw and noise(DN_sec_cme)=0.15 sqrt(DN_sec_raw)
  ; then  S/N = DN_sec_diff/noise(DN_sec_raw)=Q*DN_sec_raw/0.15 sqrt(DN_sec_raw)
  ;    so S/N = (Q/0.15) sqrt(DN_sec_raw),
  ; so S/N > 1 if sqrt(DN_sec_raw > (0.15/Q)
  
  const=1.0 / (sqrt(90.0) * Nfactor)
  
  noisedata=const * sqrt(dataA)

  noiseback=const * sqrt(backA)

  noisetotal=sqrt(noisedata^2 + noiseback^2)

  return,noisetotal

END
