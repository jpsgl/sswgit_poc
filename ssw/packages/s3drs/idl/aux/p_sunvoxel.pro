;+
;
; Project   : s3drs and Pixon reconstruction software
;                   
; Name      : p_sunvoxel
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: (part of the testing and legacy code, not documented herein)
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 

; Keywords  :
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : (see CVS logs)
;
; Written     : Sandy Antunes, NRC, 2007-2009
;               
;-            
; Given an initial HAE cube sun-centered at 0,0,0 in both HAE
; and cube voxel space, and a new HAE sun position of x,y,z,
; calculates and returns the new cube voxel position of the sun.
;
; Since Pixon uses sr.lla0 to store the sun position in the cube,
; we return the new lla0 needed.
;
; Typical usage:  sr.lla0 = p_sunvoxel(xyz,sr.d,sr.Ncube)
;

FUNCTION p_sunvoxel,xyz,D,Ncube

  au2km=149598000.0
  ; convert from HAE's km to units of AU
  xyz=xyz/au2km

  ; Pixon uses a normalized scale where 1 = 1 Rsun, with the
  ; conversion factor 'L0' of 1AU in solar radii available,
  ; e.g. the plate scale is both L0tau (in AU) and tau (in Rsun).
  ;
  ; The length of the voxel edge is 'd' in AU, often defaults to 1.0
  ; So the size of any individual voxel, in AU, is just d/float(Ncube)
  ;
  ; Therefore, converting HAE (in AU) to voxel position from center is:
  ;   delta_voxel = sr.d/float(sr.Ncube)
  ;   delta_cube = HAEinAU / delta_voxel
  ;   newcenter_cube = [Ncube/2,Ncube/2,Ncube/2] + delta_cube
  ; (note this new center does not have to physically be located
  ;  within the cube)

  lla0 = [1,1,1]*0.5*(Ncube-1); default of 'sun centered in cube and HAE'

  delta_voxel = D/float(Ncube)
  delta_cube = xyz / delta_voxel
  lla0 = lla0 + delta_cube

  return,lla0

END
