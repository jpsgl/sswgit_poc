;+
; $Id: opxn_run.pro,v 1.1 2009/04/22 18:31:28 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : opxn_run
;               
; Purpose   : 
;               
; Explanation: 
;               
; Use       : IDL> 
;    
; Inputs    : optional data: insert/update data in object
;               
; Outputs   : file T 
;
; Keywords  : 
;
; Calls from LASCO : none 
;
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : Pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRL, Nov-Dec 2005
;               
; $Log: opxn_run.pro,v $
; Revision 1.1  2009/04/22 18:31:28  antunes
; first half of relocation.
;
; Revision 1.2  2009/04/21 19:08:43  antunes
; Updated up through 'r' with SSW headers.  Also minor John bug fixes.
;
; Revision 1.1  2009/03/31 14:56:42  antunes
; Re-org prepatory to checking into SSW.
;
; Revision 1.6  2007/02/21 16:43:19  antunes
; Memory mods to allow for higher N (N=256 now works!)
;
; Revision 1.5  2006/11/30 20:03:57  antunes
; Updated pixon and other printing and output routines.
;
; Revision 1.4  2006/05/17 18:48:12  antunes
; Pixon geometry engine.
;
; Revision 1.3  2006/05/02 18:47:58  antunes
; Added test wrappers for Pixon tests.
;
; Revision 1.2  2006/04/14 18:44:42  antunes
; Streamlined pixon, plus bugfixes.
;
; Revision 1.1  2006/04/11 15:55:05  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.2  2006/03/16 20:25:27  antunes
; Improved pixon calling, wrote proto-frontend GUI for
; different rendering tests.
;
; Revision 1.1  2006/03/08 17:52:23  antunes
; Abstracted pixon structures into an aggregate structure in order
; to be encapsulate Pixonisms from general rendering and reconstruction
; routines.
;
; Revision 1.4  2006/02/21 19:36:03  antunes
; Bug fixes and streamlining.
;
; Revision 1.3  2006/02/21 18:10:19  antunes
; Improved shared code for C and T pixon.
;
; Revision 1.2  2006/01/09 17:14:42  antunes
; More commented added.
; New 'super3drv' driver routine for getting user specs for reconstruction
; process now checked in.  Just type 'super3drv' to test.
;
;-            
pro opxn_run,pxwrapper,oPixon,data

; input     sp            -  pixon control parameters
;           sc            -  misc
;           oPixon        -  Pixon object
;           userstruct    -  input structure for dsf and dsft
;           image         - [N,N,N]  starting image
;           sigma         - [N,N,Nd] noise specification
;           
; output    so            - pixon output parameters 
;           image         - [N,N,N]  calculated image
;           data          - [N,N,Nd] calculated (transformed) data
; 
;------------------------------------------------------------------

common debugs,phys,tdsf,tdsft,dbg

sp=pxwrapper.sp
sc=pxwrapper.sc
;userstruct=pxwrapper.userstruct
;journ=pxwrapper.journ

version='stereo_pixon: 06'

shmcut=128

jname = sc.gname+'_'+sc.tag+'.jou'
journal,jname

dbg=-1.0

tdsf=0
tdsft=0

; ----- fetch some basic parameters

jsched    = sp.jsched
msched    = sp.msched
rsched    = sp.rsched

; -----
if (n_elements(data) ne 0) then begin
    N  = n_elements(data[*,0,0])
    Nd = n_elements(data[0,0,*])
end else begin
    N = pxwrapper.sr.Ncube
    Nd = (size(pxwrapper.sr.gamma))[1]
end

;if(n_elements(image) le 1)then begin 
;    if(N le shmcut)then begin
;      image=fltarr(N,N,N)
;      stop,'GACK'
;    end else begin
;      shmmap,/float,get_name=segname,dim=[N,N,N]
;      image=shmvar(segname)
;      stop,'GACKK'
;    end
;  end

print,''
print,'====================== N=',N
;help,image
;print,'======================'
;print,''

idims = [N,N,N]
ddims = [N,N]
if(sc.term ne 'batch')then loadct,0

; ----- Insert values into pixon object

;oData = oPixon->get(/oData)

;oPixon->Setimage,image=image   ; insert starter image

;oPixon->Set,sigma=sigma        ; insert sigma

;for m=0,Nd-1 do oData[m]->Set,data=data_ptr      ; insert data
if (n_elements(data) ne 0) then begin
    p_insert_data,pxwrapper,data,oPixon
;    for m=0,Nd-1 do oData[m]->Set, data=PTR_NEW(data[*,*,m])
end

;;debug
;xmdata=dblarr(N,N,Nd)
;for m=0,Nd-1 do begin                   &$
;  ximage = oPixon->Get(/im,/nop)        &$
;;  ximage = conv(wrap(prf3d),ximage)     &$
;  xmdata[*,*,m] = oData[m]->dsf(ximage) &$
;end
;if(sc.term ne 'batch')then stop,'ximage, xmdata before pseudo'

time0=systime(-1)

scales = FINDGEN(sp.npxn)
pxnidx = FLTARR(N,N,N)+sp.npxn-1

; ========== BEGIN THE LOOP

; detailed control of anneal schedule
; for each anneal step, the number of conjugate gradient
; minimization steps (mxit) and the pixon scale (j)
; are set separately. A record of pixon scales is
; kept in 'annealsteps' and a record of the mxit
; is kept in 'mxits'

; ----- do NNLS step

;msched[0]=1
;stop,'Stopped in stereo_pixon before pseudo'

if(msched[0] ne 0)then begin
  oPixon->Set, pxnidx=pxnidx
  oPixon->Set, mxit=msched[0]
  oPixon->Set, reltol=rsched[0]
  oPixon->pseudo
  annealsteps='LS'
  q=qval(oPixon)
end

;stop,'Stopped in stereo_pixon after pseudo'

mxits   = msched[0]
reltols = rsched[0]

if(q le 1)then begin
    pxnmap=oPixon->Get(/pxnidx, /nop)
    goto, donexx
end else begin
  annealsteps=''
end

; ========== do the anneal loop

nj=n_elements(jsched)

FOR j = 1,nj-1 DO BEGIN

  print,''
  print,'ANNEAL STEP '+string(j,"(i3)")+' of '+string(nj-1,"(i3)")+$
    ' scale='+string(jsched[j],"(i2)")+' mxit='+string(msched[j],"(i4)")

  oPixon->Map
  oPixon->PixonSmooth, /pxnidx
  pxnmap=oPixon->Get(/pxnidx, /nop)
  pxnidx=pxnmap > scales[jsched[j]]

  PRINT, "pxnidx          min-map     min-pxnidx"
  PRINT, jsched[j], MIN(pxnmap), MIN(pxnidx)
  oPixon->Set, pxnidx = pxnidx
  oPixon->Set, mxit   = msched[j]
  oPixon->Set, reltol = rsched[j]
  oPixon->pseudo
  annealsteps=[annealsteps,string(jsched[j],"(i2.2)")]
  mxits   = [mxits,msched[j]]
  reltols = [reltols,rsched[j]]
  q=qval(oPixon)

; save intermediate results

;  mdata=fltarr(N,N,Nd); data*0
;  for m=0,nd-1 do begin
;    image = oPixon->Get(/im,/nop)         ; get the reconstructed image
;    mdata[*,*,m] = oData[m]->dsf(image)
;  end

  sc.totaltime=systime(-1)-sc.t00

  pxncnt=oPixon->Get(/pxncnt)

  machine=sc.machine
  npxn=sp.npxn
  tempname=sc.gname+'_'+sc.tag+'_'+string(j,"(i2.2)")+'.dat'

;  save,filename=tempname,data,image,mdata,sigma,$
;  pxncnt,q,tdsf,tdsft,machine,version,sc,sp,userstruct,$
;  annealsteps,mxits,reltols,npxn,pxnmap

; end if q lt 1

  if(q lt 1.0)then goto, donexx

ENDFOR

; ============

donexx:

; debug - 
res=dblarr(n,n,nd)
rdata=res
xdata=res
mdata=res
image=fltarr(n,n,n)
pxncnt=0
q = 0.00



pres = oPixon->Get(/res)		; get residuals
nd=n_elements(pres)
res=dblarr(n,n,nd)
for i=0,nd-1 do res[*,*,i]=*pres[i]



pdata  = oPixon->Get(/data)             ; get transformed data 
rdata=dblarr(n,n,nd)
for i=0,nd-1 do rdata[*,*,i]=*pdata[i]


; NOTE - transform_data arguements are wrong !!!!!
;xdata=dblarr(n,n,nd)			; get untransformed data
;for m=0,nd-1 do xdata[*,*,m] = transform_data(data[*,*,m],m,k0,tau,-1)

oData = opixon->get(/oData)		; get the noise free transformed data 
mdata=dblarr(n,n,nd)

; NOTE - transform_data arguements are wrong !!!!!
;xmdata=dblarr(n,n,nd)                    ; get noise free untransformed data
;for m=0,nd-1 do xmdata[*,*,m] = transform_data(mdata[*,*,m],m,k0,tau,-1)

for m=0,nd-1 do begin
  image = oPixon->Get(/im,/nop)		; get the reconstructed image
  mdata[*,*,m] = oData[m]->dsf(image)
end


DONE:
print,'DONE'

pxncnt=oPixon->Get(/pxncnt) ; get number of pixons
q = oPixon->Get(/qval)      ; get q value

; close journal file and create journal variable

journal

names=findfile(jname,count=c)
if(c gt 0)then begin
  close,1
  openr,1,names[c-1]
  linex=''
  journ=''
  on_ioerror,donejou
L:
  readf,1,linex
  journ=[journ,linex]
  goto, L
donejou:
  print,'journal file ',names[c-1],' read'
  close,1
  spawn,'rm '+names[c-1]
end else begin
  print,'journal file '+jname+' not found'
  journ='journal file '+jname+' not found'
end

if(n_elements(journ) gt 1)then journ=transpose(journ)

so = {                     $ ; PIXON output structure
pxncnt:       pxncnt,      $ ; 
q:            q,           $ ; Q
mdata:        mdata,       $ ; data rendered from the solution image
res:          res,         $ ; normalized residuals
annealsteps:  annealsteps, $ ; record of anneal steps performed
mxits:        mxits,       $ ; record of CGM  steps peformed
reltols:      reltols,     $ ; record of reltol values used
pxnmap:       pxnmap       $ ; pixon map
}

qdata=mdata
sc.tdsf    = tdsf
sc.tdsft   = tdsft
sc.version = version

pxwrapper.sc=sc

;so2={so:so}
;pxwrapper = create_struct(pxwrapper,so2) ; appends to existing pxwrapper
pxwrapper=addrep_struct(pxwrapper,'so',so)

;journ2={journ:journ}
;pxwrapper = create_struct(pxwrapper,journ2) ; appends to existing pxwrapper
pxwrapper=addrep_struct(pxwrapper,'journ',journ)

return
end
