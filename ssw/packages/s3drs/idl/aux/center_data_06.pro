;+
;
; Project   : s3drs and Pixon reconstruction software
;                   
; Name      : center_data_06
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: (part of the testing and legacy code, not documented herein)
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 

; Keywords  :
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : (see CVS logs)
;
; Written     : Sandy Antunes, NRC, 2007-2009
;               
;-            
; wrapper for p_center_data to preserve name compability with legacy code

pro center_data_06,sr,Ns,nn

  p_center_data,sr,Ns,nn,/check

end
