;+
;
; Project   : s3drs and Pixon reconstruction software
;                   
; Name      : tpxn_run
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: (part of the testing and legacy code, documented in code)
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 

; Keywords  :
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : (see CVS logs)
;
; Written     : Sandy Antunes, NRC, 2007-2009
;               
;-            
pro tpxn_run,pxwrapper,oTPixon,datum

; $Id: tpxn_run.pro,v 1.1 2009/04/22 18:31:49 antunes Exp $
;+
; <p> make_tpxn for investigating qadd and qrem 
;
; <p>
; @copyright
;    Copyright � Pixon LLC, 1999-2004.  All rights reserved.
;    Unauthorized reproduction prohibited.
; @author
;    <a href="mailto:Amos.Yahil\@Pixon.com">Amos Yahil, Pixon LLC.</a>
;    modified Sep 21/04 P Reiser
;
; input: diag        print diagnostic messages
;        dim
;        disp        1=>display intermediate steps 0=>don't
;        lstop        stops every lstop iterations (0=>no intermediate stops)
;        mxit
;        niter        number of iterations
;        print
;        qadd        when (local q)>qadd, vertices are added        
;        qrem        when (local q)<qrem, vertices are removed
;        tiff 
;        wait
; 
; output:  varnaud2*.dat files and oTPixon*.dat files    
; 
;-

; Compilation options

   COMPILE_OPT IDL2, HIDDEN, STRICTARRSUBS

sc=pxwrapper.sc
sr=pxwrapper.sr
sp=pxwrapper.sp
sn=pxwrapper.sn
sf=pxwrapper.sf


; Initialization

   dim=sr.n
   note=sc.note

   fname0=sf.fname0
   fname1=sf.fname1
   fname2=sf.fname2
   mysave=sf.mysave
   tmake=sf.tmake

   IF N_ELEMENTS(diag) EQ 0 THEN                diag = 1
   IF N_ELEMENTS(dim) EQ 0 THEN                 dim = 32
   IF N_ELEMENTS(disp) EQ 0 THEN                disp = 0
   IF N_ELEMENTS(lstop) EQ 0 THEN               lstop = 5
   IF N_ELEMENTS(mxit) EQ 0 THEN                mxit = 50
   IF N_ELEMENTS(niter) EQ 0 THEN               niter = 20
   IF N_ELEMENTS(print) EQ 0 THEN               print = 0
   IF N_ELEMENTS(qadd) EQ 0 THEN                qadd = 3
   IF N_ELEMENTS(qrem) EQ 0 THEN                qrem = 1
   IF N_ELEMENTS(tiff) EQ 0 THEN                tiff = 0
   IF N_ELEMENTS(wait) EQ 0 THEN                wait = 1.0/30

   IF N_ELEMENTS(recsave) EQ 0 THEN             recsave = 1

   taddrem = 0.0
   tpseudo = 0.0
   ttotal  = 0.0
   t0=SYSTIME(/seconds)

; Derived parameters

   IF KEYWORD_SET(cpu) THEN BEGIN
      disp = 0
      print = 0
      seed0 = 1235711
   ENDIF

; Restore the OTPIXON save set
; now it is passed as a procedure variable, not restored from a file
;;   usednoise=0
;;   if (usednoise) then begin
;;       oname='otpixon_' + STRTRIM(dim, 2) + '.idl'
;;       print,'restoring ',oname
;;       RESTORE, oname
;;   end else begin
;;       print,'restoring ',mysave
;;       RESTORE, mysave
;;   end

   oTPixon -> set, mxit=mxit, print=print

; if necessary, add in observed data to object
   if (n_elements(datum) ne 0) then begin
       p_insert_data,pxwrapper,datum,oPixon
;       nd = (size(pxwrapper.sr.gamma))[1]
;       data  = PTRARR(1,nd)     ; TETRA
;       for m=0,nd-1 do data[0,m]=ptr_new(datum[*,*,m]) ; TETRA
;       oTPixon->Replace,data=data ; TETRA
   end

; ----- Choose initial vertices

; Initial vertices - Amos' original starter -  low density sphere

;   vrtx  = ROUND(0.5*dim*Sph_Tess())
;   vrtx  = [[vrtx],[LONARR(3)]]
;   note = note+' LDS'

; Initial vertices - 1/4 max density sphere

;   sseed = 0
;   vrtx = round((randomu(sseed,3,0.25*long(dim)^3)-0.5)*dim)
;   rr   = total(vrtx^2,1)
;   vrtx = vrtx[*,where(rr le 0.25*dim^2)]
;   note = note+' 1/4DS'

; Initial vertices - vertex at all N^3 points

;   vrtx = Coord(idim) - dim/2
;   note = note+' HDC'

; Initial vertices - 1/4 density cube 

   vrtx = round((randomu(sseed,3,0.25*long(dim)^3)-0.5)*dim)
   note = note+' 1/4DC'

; -----

   vrtx  = vrtx[*,Uniq2(vrtx, RSort(vrtx))]	; eliminate duplicates
   nvrtx = N_ELEMENTS(vrtx)/3
   oTPixon -> Replace, image=FLTARR(nvrtx), vrtx=vrtx
  
   chi2 = oTPixon -> Get(/CHI2, /TOTAL)
   dof  = oTPixon -> Get(/DOF, /TOTAL)
   q    = (chi2 - dof)/SQRT(2*dof)

   print,'iteration 000 - before pseudo step q=',q

; create record structure and save before-pseudo oTPixon object

   if(recsave)then begin

;     title=';----------------- '+runfname+'---------------'
;     progs=[title,'']
;     get_lun,lun
;     openr,lun,runfname
;     line=''
;     on_ioerror,IOE0 &$
;     while(1) do begin & readf,lun,line & progs=[progs,line] & end &$
;     IOE0: free_lun,lun

     title=';----------------- varnaud2_make2_tpxn.pro ---------------'
     progs=[title,'']
;     get_lun,lun
;     openr,lun,'varnaud2_make2_tpxn.pro'
;     line=''
;     on_ioerror,IOE1 &$
;     while(1) do begin & readf,lun,line & progs=[progs,line] & end &$
;     IOE1: free_lun,lun

     title=';----------------- varnaud2_run2_tpxn.pro ---------------'
;     progs=[progs,title,'']
;     get_lun,lun
;     openr,lun,'varnaud2_run2_tpxn.pro'
;     line=''
;     on_ioerror,IOE2 &$
;     while(1) do begin & readf,lun,line & progs=[progs,line] & end &$
;     IOE2: free_lun,lun

     tag=get_tag()
     if(n_elements(note) eq 0)then note=''

     nnn=2*(niter+1)	; number of iteration records = two per iteration,
                        ; one for before pseudo step and one for after

     qtol = oTPixon -> Get(/QTOL)

     record={record6,$
     tag:      tag             ,$   ; tag number for this run
     note:     note            ,$   ; any text you want
     progs:    progs           ,$   ; make and run program listings
     diag:     diag            ,$   ; 
     dim:      long(dim)       ,$   ; data frame is N^2, image cube is N^3
     mxit:     long(mxit)      ,$   ; number of iterations in pseudo step
     niter:    long(niter)     ,$   ; max number of iterations
     qadd:     float(qadd)     ,$   ; add vertices for qlocal>qadd
     qrem:     float(qrem)     ,$   ; remove vertices for qlocal<qrem
     qtol:     float(qtol)     ,$   ; tolerance on Q (for pseudo ???)
     ttotal:   -1.0            ,$   ; total run time
     oTPixon0: fname0          ,$   ; preliminary oTPixon object before pseudo (from make)
     oTPixon1: fname1          ,$   ; preliminary oTPixon object after pseudo (from make)
     iter:     intarr(nnn)     ,$   ; iteration number
     rp:       bytarr(nnn)     ,$   ; 0:before 1:after pseudo step
     nvrtx:    lonarr(nnn)     ,$   ; number of vertices
     nph:      fltarr(nnn)     ,$   ; number of tetrahedra
     ttaddrem: fltarr(nnn)     ,$   ; add/remove time for this iteration
     ttpseudo: fltarr(nnn)     ,$   ; pseudo time for this iteration
     chi2:     fltarr(nnn)     ,$   ; chi squared
     q:        fltarr(nnn)     ,$   ; global Q
     oTPixon:  strarr(nnn)     ,$   ; oTPixon object file name
     machine:  getenv('HOST')  ,$   ; machine name
     tmake:    tmake            $   ; oTPixon object make time
     }

     record.iter[0]    = 0
     record.rp[0]      = 0
     record.nvrtx[0]   = oTPixon -> Get(/NVRTX)
     record.nph[0]     = oTPixon -> Get(/NPH)
     record.ttaddrem[0] = taddrem
     record.ttpseudo[0] = 0
     record.chi2[0]    = chi2
     record.q[0]       = q
     fname='oTPixonv_'+tag+'000.dat'
     fnameh='$PIXHERE/'+fname
     fnames='$PIXSAVE/'+fname+'.gz'
     save,filename=fnameh,oTPixon
     print,'data saved to ',fnameh
;     spawn,'gzip -c '+fnameh+' > '+fnames
     record.oTPixon[0] = fnames 
   end

; run first pseudo step

   tstart = SYSTIME(/seconds)
   oTPixon -> Pseudo
   tpseudo = SYSTIME(/seconds) - tstart + tpseudo

   chi2 = oTPixon -> Get(/CHI2, /TOTAL)
   dof  = oTPixon -> Get(/DOF, /TOTAL)
   q    = (chi2 - dof)/SQRT(2*dof)

   print,'iteration 000 - after  pseudo step q=',q

; save after-pseudo oTPixon object

    if(recsave)then begin
      record.iter[1]         = 0
      record.rp[1]           = 1
      record.nvrtx[1]        = oTPixon -> Get(/NVRTX)
      record.nph[1]          = oTPixon -> Get(/NPH)
      record.ttaddrem[1]      = 0
      record.ttpseudo[1]      = tpseudo
      record.chi2[1]         = chi2
      record.q[1]            = q
      fname='oTPixonv_'+tag+'001.dat'
      fnameh='$PIXHERE/'+fname
      fnames='$PIXSAVE/'+fname+'.gz'
      save,filename=fnameh,oTPixon
      print,'data saved to ',fnameh
;      spawn,'gzip -c '+fnameh+' > '+fnames
      fname='$PIXHERE/oTPixonv_'+tag+'000.dat'
;      spawn,'rm '+fname

      record.oTPixon[1] = fnames
    end
 
; Loop on mesh refinement

   old_nvrtx = nvrtx + 1
   qtol = oTPixon -> Get(/QTOL)

; ------------- begin loop  -----------------------

   FOR iter=1,niter DO BEGIN

      IF KEYWORD_SET(disp) THEN BEGIN
         DW
         oTPixon -> Disp, /RESIDUAL, min=0, max=1, name='CDF of Residuals', $
                          wait=wait, APPEND=tiff, /BAR, /FREE, /PDF, TIFF=tiff
         vrtx = oTPixon -> Get(/VRTX, /NOPOINTER)
         ph = oTPixon -> get(/PH, /NOPOINTER)
         image = oTPixon -> Get(/IMAGE, /NOPOINTER)
         start = MIN(vrtx, max=delta) - 2.0
         delta = (delta - start + 2.0)/(dim - 1)
         timage = QGRID3(vrtx, image, ph, delta=delta, dimension=dim, $
                         start=start)
         XVOLUME, BYTSCL(timage), REPLACE=replace
         replace = 1
         FOR n=-dim/2,dim/2 DO BEGIN
            tdx = WHERE(vrtx[2,*] EQ n, tcnt)
            IF tcnt NE 0 THEN BEGIN
               PXNplot, vrtx[0,tdx], vrtx[1,tdx], psym=1, title=STRTRIM(n, 2), $
                        xrange=[-dim/2,dim/2], xstyle=3, $
                        yrange=[-dim/2,dim/2], ystyle=3, /DW
               OPLOT, REPLICATE(SQRT((dim/2)^2 - n^2), 201), $
                      !PI/100*FINDGEN(201), /POLAR
               WAIT, wait
            ENDIF
         ENDFOR
      ENDIF

      IF lstop NE 0 AND iter MOD lstop EQ 0 THEN BREAK

      nqq=n_elements(qadd)
      if(nqq eq 1)then begin
        hqadd=qadd
      end else begin
        iqq=iter
        if(iqq gt nqq-1)then iqq=nqq-1
        hqadd=qadd[iqq]
      end

      nqq=n_elements(qrem)
      if(nqq eq 1)then begin
        hqrem=qrem
      end else begin
        iqq=iter
        if(iqq gt nqq-1)then iqq=nqq-1
        hqrem=qrem[iqq]
      end

      tstart = SYSTIME(/seconds)
      oTPixon -> AddRemove, hqadd, hqrem, DIAGNOSTICS=diag;,_REF_EXTRA=1	;!!!
      taddrem = SYSTIME(/seconds) - tstart

      chi2 = oTPixon -> Get(/CHI2, /TOTAL)
      q    = (chi2 - dof)/SQRT(2*dof)

      print,'iteration ',string(iter,"(i3.3)"),' - before pseudo step q=',q

; save before-pseudo oTPixon object

      if(recsave)then begin
        nnn=2*iter
        record.iter[nnn]      = iter
        record.rp[nnn]        = 0
        record.nvrtx[nnn]     = oTPixon -> Get(/NVRTX)
        record.nph[nnn]       = oTPixon -> Get(/NPH)
        record.ttaddrem[nnn]   = taddrem
        record.ttpseudo[nnn]   = 0
        record.chi2[nnn]      = chi2
        record.q[nnn]         = q
        fname='oTPixonv_'+tag+string(nnn,"(i3.3)")+'.dat'
        fnameh='$PIXHERE/'+fname
        fnames='$PIXSAVE/'+fname+'.gz'
        save,filename=fnameh,oTPixon
        print,'data saved to ',fnameh
;        spawn,'gzip -c '+fnameh+' > '+fnames
        record.oTPixon[nnn] = fnames
        fname='$PIXHERE/oTPixonv_'+tag+string(nnn-1,"(i3.3)")+'.dat'
;        spawn,'rm '+fname
      end

; run pseudo step

      tstart = SYSTIME(/seconds)
      oTPixon -> Pseudo
      tpseudo = SYSTIME(/seconds) - tstart

      chi2 = oTPixon -> Get(/CHI2, /TOTAL)
      q    = (chi2 - dof)/SQRT(2*dof)

      print,'iteration ',string(iter,"(i3.3)"),' - after  pseudo step q=',q

; save after-pseudo oTPixon object

      if(recsave)then begin
        nnn=2*iter+1
        record.iter[nnn]      = iter
        record.rp[nnn]        = 1
        record.nvrtx[nnn]     = oTPixon -> Get(/NVRTX)
        record.nph[nnn]       = oTPixon -> Get(/NPH)
        record.ttaddrem[nnn]   = 0
        record.ttpseudo[nnn]   = tpseudo
        record.chi2[nnn]      = chi2
        record.q[nnn]         = q
        fname='oTPixonv_'+tag+string(nnn,"(i3.3)")+'.dat'
        fnameh='$PIXHERE/'+fname
        fnames='$PIXSAVE/'+fname+'.gz'
        save,filename=fnameh,oTPixon
        print,'data saved to ',fnameh
;        spawn,'gzip -c '+fnameh+' > '+fnames
        record.oTPixon[nnn] = fnames
        fname='$PIXHERE/oTPixonv_'+tag+string(nnn-1,"(i3.3)")+'.dat'
;        spawn,'rm '+fname
      end

   ENDFOR

; Done

if(recsave)then begin
  record.ttotal = SYSTIME(/seconds)-t0
  sc.totaltime=record.ttotal
  fname='$PIXHERE/varnaud2_'+tag+'.dat'
;  save,filename=fname,record,sr,sp,sn,sc,datf
  save,filename=fname,record,sr,sp,sn,sc,oTPixon
  print,'data saved to ',fname
end else begin
  print,'data not saved'
end


so = {dummy: 0}
;so2={so:so}
;pxwrapper = create_struct(pxwrapper,so2) ; appends to existing pxwrapper
pxwrapper=addrep_struct(pxwrapper,'so',so)

journ = {dummy: 0}
;journ2={journ:journ}
;pxwrapper = create_struct(pxwrapper,journ2) ; appends to existing pxwrapper
pxwrapper=addrep_struct(pxwrapper,'journ',journ)

userstruct = {dummy: 0}
;us2={userstruct:userstruct}
;pxwrapper = create_struct(pxwrapper,us2) ; appends to existing pxwrapper
pxwrapper=addrep_struct(pxwrapper,'userstruct',userstruct)



END
