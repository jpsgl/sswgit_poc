;+
;
; Project   : s3drs and Pixon reconstruction software
;                   
; Name      : p_ingest_fits
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: (part of the testing and legacy code, documented in code)
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 

; Keywords  :
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : (see CVS logs)
;
; Written     : Sandy Antunes, NRC, 2007-2009
;               
;-            
; NEEDS SOHO polarization translation to SECCHI type notations.

; optional argument 'N' lets you set a size to rebin the
; data to (and change the wrapper to, et cetera), also adapts
; the pxwrapper Pixon structures to this rebinned N.
;
; If given the optional 'pxwrapper' structure plus an 'iele' index,
; it will put 'srjr' into that wrapper at position 'iele'.
;
; If the user gives it 'indata' and/or 'inhdr', we skip reading
; in the filename and use the pass-in entities.
;
; If /pointer flag is given, sets 'returndata' and 'mask' as pointers
; to the data, otherwise it returns the data arrays themselves.
; Note that use of pointers means you can pass a pointer to the
; returndata and mask variables-- but _both_ must be pointers or
; _both_ must data, you cannot mix and match.
; If you also include iele=N 'index', it assumes the returndata and mask
; are arrays and puts the appropriate data into that Nth ptr area.
;
; Unless you set /extracor2B, will not use large 'horn' mask on cor2B
;


pro p_ingest_fits,fname,returndata,mask,srjr,N=N,verbose=verbose, $
                  pxwrapper=pxwrapper,iele=iele,indata=indata,inhdr=inhdr,$
                  pointer=pointer,maskratio=maskratio,$
                  extracor2B=extracor2B

  au2km=149598000.0
  au2m=au2km*1000.0

  verbose=KEYWORD_SET(verbose)
  pointer=KEYWORD_SET(pointer)
  extracor2B=KEYWORD_SET(extracor2B)
  if (n_elements(n0) eq 0) then n0=1.0
  if (n_elements(iele) eq 0) then iele=0
  if (n_elements(maskration) eq 0) then maskratio=1.0

  if (file_exist(fname) eq 0 and $
      (n_elements(indata) eq 0 or n_elements(inhdr) eq 0) ) then begin
      print,'file ',fname,' does not exist, trying variant'
      fname=strep(fname,'s4','n4')
	if (file_exist(fname) eq 0) then begin
	 	print,'file variant also nonexistent, exiting'
  
	   stop
	end
end

  au2meters = 1.5E11;

  if (n_elements(inhdr) ne 0) then begin

      ; use the header given rather than reading from the file
      hdr=inhdr

  end else begin

      hf=headfits(fname)
;;  hdr=fitshdr2struct(hf); see also lasco_fitshdr2struct
; next line uses /net/cronus/opt/local/ssw/gen/idl/fits/fitshead2struct.pro
;      help,hf
      hdr=fitshead2struct(hf)   ; general IDL routine
;      help,hdr
;  hdr=hdr2struct(hf);       truncates so not used
  end

  ; best guess here at FITS file processing level
  if (tag_exist(hdr,'bunit')) then begin
      bunit=hdr.bunit
      level='0.5'               ; first guess
      if (bunit eq 'MSB') then level='1.0'
  end else begin
      bunit='MSB'
      level='sim 1.0'
  end

  sat_name='unknown'
  tele_name='unknown'
  if (tag_exist(hdr,'obsrvtry')) then begin
      sat_name=hdr.obsrvtry   ; e.g. STEREO_A, STEREO_B
      if (sat_name eq '') then sat_name=hdr.instrume; hack for roto tomo
  end else if (tag_exist(hdr,'telescop')) then begin
      sat_name=hdr.telescop; e.g. SOHO
  end else if (tag_exist(hdr,'program')) then begin
      sat_name=hdr.program
  end

  if (tag_exist(hdr,'instrume')) then begin
      tele_name=hdr.instrume; e.g. LASCO, SECCHI
  end else if (tag_exist(hdr,'program')) then begin
      tele_name=hdr.program
  end

  detector=hdr.detector; e.g. COR1, COR2, EUVI, HI1, HI1, C2, C3, EIT


  if (verbose) then print,sat_name+'/'+tele_name+'/'+detector

  ; get and/or force our data as a pointer for uniformity
  if (n_elements(indata) eq 0) then begin
      if (sat_name eq 'STEREO_A' or sat_name eq 'STEREO_B') then begin
          ; note this has the side behavior of redefining 'hdr'
          ; as a SECCHI_HDR, useful for trimming
          ptrdata=ptr_new(sccreadfits(fname,hdr))
      end else begin
          ptrdata=ptr_new(readfits(fname))
      end
  end else begin
      ptrdata=force_ptrs(indata,/single)
  end

 if (tele_name eq 'SECCHI' and level eq '0.5') then $
   *ptrdata=scc_img_trim(*ptrdata,hdr)

;help,returndata

;  if (n0 ne 1.0) then *ptrdata = *ptrdata / n0

; GET COORDINATES

  ; we now use a non-rotating coordinate system like HAE or HCI.

  ; for coordinates, get HEE x,y,z, convert to HAE as an
  ; absolute frame, then calculate spherical rho, phi, d
  ; and cylindrical z0.
  ;
  ; The reason we keep HAE as our x/y/z is that it allows us to
  ; compare across models at different times, because it is an
  ; absolute reference frame

  ; if not HEE, check for HEEQ
  ; if not HEE x,y,z, convert to HE x,y,z first
  ; HEE = Heliocentric Earth Ecliptic
  ; conversion is just, e.g.
  ; CONVERT_STEREO_COORD, '2007-05-06T11:30', COORD, 'HCI', 'HEE' 

  if (tag_exist(hdr,'simdate')) then begin
      obsdate=hdr.simdate
  end else begin
      get_fits_time,hdr,obsdate
  end

  units='m'; default is units in meters
  if (tele_name eq 'LASCO') then begin
      o1 = get_orbit(obsdate)
;       help,o1,/str
      ; lasco has GCI, GSE, GSM, HEC, CAR
;;      gamma= = o1.hel_lon_soho * !radeg ; angle of satellite from x-axis
;;      theta = o1.hel_lat_soho * !radeg ; angle of satellite from y-axis
      tempxyz = [o1.gse_x,o1.gse_y,o1.gse_z]
      CONVERT_STEREO_COORD, obsdate, tempxyz, 'GSE', 'HAE' 
      tempxyz = tempxyz * 1000.0; convert LASCO GSE km to our meters
      if (verbose) then print,'using GSE coordinates'
 end else if (tele_name eq 'SECCHI') then begin
      tempxyz = [hdr.heex_obs,hdr.heey_obs,hdr.heez_obs]
;      print,'got hee',tempxyz
      CONVERT_STEREO_COORD, obsdate, tempxyz, 'HEE', 'HAE' 
;      print,'made hea',tempxyz
;      x=hdr.heex_obs
;      y=hdr.heey_obs
;      z=hdr.heez_obs
      if (verbose) then print,'using HEE coordinates'
  end else begin
      ; unknown or sim data
      if (tag_exist(hdr,'haex_obs')) then begin
          tempxyz = [hdr.haex_obs,hdr.haey_obs,hdr.haez_obs]
          if (tag_exist(hdr,'hae_unit')) then units=hdr.hae_unit
          if (units eq 'au' or units eq 'AU') then begin
              ; convert to meters
              tempxyz=tempxyz*au2meters
              units='m'
          end
          if (verbose) then print,'using given HAE coordinates in ',units
      end else if (tag_exist(hdr,'heex_obs')) then begin
          tempxyz = [hdr.heex_obs,hdr.heey_obs,hdr.heez_obs]
          CONVERT_STEREO_COORD, obsdate, tempxyz, 'HEE', 'HAE' 
          if (verbose) then print,'using HEE coordinates'
      end else if (tag_exist(hdr,'crln_obs')) then begin
          x = hdr.crln_obs ; angle of satellite from x-axis in degrees
          y = hdr.crlt_obs ; angle of satellite from y-axis in degrees
          z=hdr.dsun_obs * au2meters ; satellite distance in AU
          tempxyz = [x, y, z]
          CONVERT_STEREO_COORD, obsdate, tempxyz, 'Carrington', 'HAE'
          if (verbose) then print,'using Carrington coordinates'
      end else begin
          print,'unknown coordinates, exiting.'
          stop
      end
  end

;  x=tempxyz[0]
;  y=tempxyz[1]
;  z=tempxyz[2]
  hae_coords = ptr_new(tempxyz); save, handy for later plotting

  xyz2pixon,tempxyz,verbose=verbose,rho,phi,LL,z0,ecliptic

 ; Also set whether image is aligned to solar north or ecliptic north
 ; Used in p_process_filelist to determine whether images need
 ; to be rotated to solar north or not.
 ; default for sims, SECCHI 0.5, SECCHI 1.0, most others
  align='ecliptic'
  if (tag_exist(hdr,'crota')) then crota=hdr.crota else $
    if (tag_exist(hdr,'crota1')) then crota=hdr.crota1 else crota=0.0
  
  rotateme=0
  if (tele_name eq 'LASCO') then begin
      DEFSYSV, "!DRADEG", 180.0d/!DPI
      DEFSYSV, "!DDTOR", !DPI/180.0d
      angle=festival_ecl_solar_north(tempxyz[0],tempxyz[1],tempxyz[2])
      align = 'solar' + ' , ecl angle = '+strtrim(string(angle),2) + $
        ' , crota = ' + strtrim(string(crota),2)

      rotateme=1
  end else if (tele_name eq 'SECCHI' and level eq '1.0' and $
               crota eq 0.0) then begin
      ; odd case of SECCHI Level1.0 where they set ROTATE_ON during secchi_prep
      align='solar'
      rotateme=1
  end


; NOW FIND CENTERS

  ; LASCO
  ; next line is a hack for 0.5 files lacking some details
  if (tag_exist(hdr,'time_obs') eq 0 and tag_exist(hdr,'mid_time')) then $
    hdr=addrep_struct(hdr,'time_obs',hdr.mid_time)

  if (tag_exist(hdr,'CRPIX1')) then begin

      ; extract properly from FITS header
      if (verbose) then print,'using exact optical center'
      ; note that FITS goes 1 to N using image centers, while
      ; classic Pixon goes 0 to (N-1) using lower corner, therefore
      ; physical image center assuming N is even of N/2,N/2
      ; translates in classic Pixon to (N/2-1),(N/2-1)
      k0=[hdr.CRPIX1-1.0, hdr.CRPIX2-1.0] ; optical center [2]
      if (sat_name eq 'SOHO' or sat_name eq 'LASCO') then begin

          sunc=get_sun_center(hdr)

          print,'sun center is ',sunc
          if (hdr.CROTA1 EQ 180) then begin
              center=512      ;  (1024-1)/2.
              delx = center - sunc.xcen
              dely = center - sunc.ycen
              sunc.xcen = center + delx
              sunc.ycen = center + dely

              del = [center,center] - k0
              k0 = center + del
          end
          print,'sun center is ',sunc

          sunk0=[sunc.xcen,sunc.ycen]
          if (verbose) then print,'  via get_sun_center(), ',sunk0str

      end else begin

          if (verbose) then print,'  via CRVAL'
          crval=[hdr.CRVAL1, hdr.CRVAL2] ; offset in arcsec
          crval=crval/hdr.cdelt1; convert to pixels
          sunk0=k0-crval

      end
;      print,'debug: used crpix1'

  end else if (sat_name eq 'SOHO' or sat_name eq 'LASCO') then begin

      ; some work-arounds if there is trouble parsing the SOHO data
      if (verbose) then print,'using SOHO ancillary routine t_param'
      k0=t_param(detector,'OCCENTER')
      sunk0str=t_param(detector,'CENTER')
      sunk0=sunk0str.center
;      print,'debug: used t_param'

  end else begin

      ; if no hdr info, assume it's a center-pointing sun-centered frame
      k0=[(N/2)-0.5,(N/2)-0.5]; guess at optical center [2]
      sunk0=[(N/2)-0.5,(N/2)-0.5]; guess at sun center [2]
      if (verbose) then $
        print,'guessing at optical center: sun-center pointing ',k0
;      print,'debug: guessed'

  end

; NOW GET, ROTATE (IF NEEDED), AND REBIN DATA

  if (hdr.obsrvtry eq 'STEREO_B') then ab='B' else ab='A'
  angle=0.0 - get_stereo_roll(hdr.date_obs,ab,SYSTEM='HAE')
  rotateme=1
  print,'Angle is ',angle

  if (rotateme) then begin
      tangle=angle;+crota
      ; rotate image
      print,'p_ingest_fits rotating data by ',tangle
      *ptrdata = rot(*ptrdata,tangle,1.0,sunk0[0],sunk0[1],/pivot)
      ; and reposition optical center (since rotation was around sun center)
      deltak0=k0-sunk0
      newk0=[0,0]
      newk0[0] = cos(tangle*!dtor)*deltak0[0] - sin(tangle*!dtor)*deltak0[1]
      newk0[1] = sin(tangle*!dtor)*deltak0[0] + cos(tangle*!dtor)*deltak0[1]
      k0=sunk0+newk0
 end

  Ndata=n_elements((*ptrdata)(0,*))
  if (n_elements(N) eq 0) then N=Ndata
  scaling=float(Ndata)/float(N)
  if (N ne Ndata and verbose) then $
    print,'Using scaling factor ',scaling,': ',Ndata,' to ',N

  ; use rebin to conserve count and multiply to conserve total number
  ;if (N ne Ndata) then *ptrdata=rebin(*ptrdata,N,N) * scaling
  ; No, wait, rebin now used to get average since units are B/B0, not photons

  if (N ne Ndata) then begin
      rebinned=rebin(*ptrdata,N,N)
      ptr_free,ptrdata
      ptrdata=ptr_new(rebinned)
;      help,k0,sunk0,scaling
      ; worked out here in long form just so it is clear how we figure this
      basectr=[(Ndata-1)/2.0,(Ndata-1)/2.0]
      newctr=[(N-1)/2.0,(N-1)/2.0]

      deltactr=k0-basectr
      newdeltactr=(deltactr/scaling)
      newk0=newctr+newdeltactr

      deltactr=sunk0-basectr
      newdeltactr=(deltactr/scaling)
      newsunk0=newctr+newdeltactr

      k0=newk0
      sunk0=newsunk0
  end


;  help,returndata,*ptrdata

  if (pointer) then begin
      if (n_elements(returndata) gt 1) then begin
          ; is an array of pointers
          if (verbose) then print,'setting index ',iele
          returndata[iele]=ptrdata
;          help,returndata[iele],ptrdata
      end else begin
          ; just a solitary pointer
          returndata=ptrdata
      end
  end else begin
      if (size(returndata,/n_dimensions) gt 2) then begin
          ; is an array of data
          returndata[*,*,iele]=*ptrdata
      end else begin
          ; is a single data
          returndata=*ptrdata
      end
  end

;;; NOW set all the structure tags from the header


;?  lla0=; sun-center in image, was [], default was [1,1,1]*0.5*(N-1)
;?  d = length of voxel edge in AU? def 1.0
  if (n_elements(pxwrapper) ne 0) then begin
      ; get lla0 from pxwrapper if possible
      lla0=pxwrapper.sr.lla0
      L0 = pxwrapper.sr.L0
      chl=pxwrapper.sr.chl
  end else begin
      ; default is 'center of cube'
      lla0=[1,1,1]*0.5*(Ncube-1)
      L0 = 214.943 ; aka 1 au in solar radii = constant, do not change
      chl=2
  end

;given cdelt1 = arcsec of 1 pixel and rsun = radius of sun in arcsec,
;  then 1 pixel subtends  cdelt1/rsun in solar radii... so the total
;  FOV is just  cdelt1/rsun * N_pixels (in solar radii) and our
;  L0tau is just this value/N or, cdelt1/rsun
; If rebinning, cdelt1' = cdelt1 * scaling, e.g. rebinning to 1/4th
;   the pixels has 4x the arcsec per pixel
; Note this means L0tau is 'rsun per pixel' so rsun in pixels is just 1/L0tau
;
;orig L0tau = 60/N (12/N C2, 60/N C3);distance subtended by pixel at L0 in rsun

  if (verbose) then $
    print,'Sat: ',sat_name,', Tele: ',tele_name,', Detector: ',detector


  ; first choose a default platescale, then choose a nicer one from header
  case strupcase(detector) of
      "COR1": L0tau = 4.0/double(N)
      "COR2": L0tau = 15.0/double(N)
      "EUVI": L0tau = 1.0/double(N)
      "C2": L0tau = 12.0/double(N)
      "C3": L0tau = 60.0/double(N)
      "EIT": L0tau = 1.0/double(N)
      ELSE: L0tau=1.0/double(N)
  endcase

  if (tag_exist(hdr,'rsun')) then begin
      ; e.g. SECCHI
      if (hdr.rsun eq 0) then begin
          ; not properly made, so use hard-coded version
           ; C1=4rsun FOV, C2=14rsunFOV
          if (verbose) then print,'Using hard-coded FOV estimates'
      end else begin
          if (verbose) then print,'Deriving accurate FOV'
          ; more accurate and robust calculation
          ; L0tau = pixel in arcsec over rsun in arcsec
          L0tau = scaling*hdr.cdelt1/hdr.rsun
      end

  end else if (sat_name eq 'SOHO' or sat_name eq 'LASCO') then begin
      rsunp=get_solar_radius(hdr,/pixel); returns solar radius in pixels
      L0tau = scaling/rsunp
  end else if (tag_exist(hdr,'dsun_obs')) then begin
      ; SIM or direct calc
      ; tan(rsun) = rsun/distance, rsun=6.96x10^5 km, 1AU=150x10^6 km,
      ; 1 degree = 3600 arcsec, hdr.dsun_obs is already in meters, so
      rsun_arcsec = atan(6.96E8/hdr.dsun_obs)
      ; L0tau = pixel in arcsec over rsun in arcsec
      L0tau = scaling*hdr.cdelt1/rsun_arcsec
  end
  if (verbose) then print,'L0tau (aka platescale) = ',L0tau
  tau=0.5*L0tau/L0; e.g. 30/N/214.943 (or 0 for pinhole)

;  if (verbose) then begin
;      ; a carrington sanity check
;      carr=[x,y,z]
;      CONVERT_STEREO_COORD, obsdate, carr, 'HEE','Carrington'
;      print,'Carrington: ',carr
;  end

  ; this will probably break for LASCO e.g. polar='Clear'
  ptype=datatype(hdr.polar)
  ; PIXON needs polarizations of:
  ;   'b' brightness
  ;   'p' plane
  ;   'r' radial
  ;   't' tangential
  ; If not 'b', it also needs the angle as 'fan'
  ;

  if (tele_name eq 'SECCHI') then begin
      ; SECCHI
      polar=hdr.polar
      fan=0; starting default
      if (polar eq 1001 or polar eq 1002) then begin
          polar='b'; total brightness or polarization brightness
      end else if (polar eq 1003 or polar eq 1004) then begin
          ; percent polarized or polarization angle-- cannot use
          print,'Problem, polarization is ',polar,',exiting.'
          return
      end else begin
          ; otherwise, it is likely an individual polarized angle
          fan=polar

          ;rectify polarization angles to HAE 
          if (sat_name eq 'STEREO_B') then fan=fan+90 else $
            if (sat_name eq 'STEREO_A') then fan=fan-90
          if (fan lt 0) then fan=fan+360

          polar='p'
      end

  end else if (datatype(hdr.polar) eq 'STR') then begin
      ; LASCO and some sims
      polar=hdr.polar
      if (polar eq 'Clear' or polar eq 'b' or polar eq 'B') then $
        polar='b' else polar='p'

      ; need further work on LASCO polarizations here

      if (tag_exist(hdr,'fan')) then begin
          fan=hdr.fan     ; e.g. 0, filter angle for plane-polarized frames
      end else begin
          fan=0      ; guess that angle is 0 if not otherwise knowable
          print,'Guessing polarization angle is 0, type is ',polar
      end
  end else begin
      ; generic or SIM
      fan=hdr.polar   ; e.g. 0, filter angle for plane-polarized frames
      if (fan ne 0) then polar='p' else polar='b'
      print,'Guessing polarization angle is ',fan,', type is ',polar
  end
;  polar= e.g. 'b', 'p', 't' or 'r'

; make masks for coronographs
;  mask=congrid(quickC2mask(fname),N,N)

; we can use an overzealous mask ratio here for clarity as a parameter
  ; we also use the 'cor2horn'
; BUT for now we disable this.
  if (detector eq 'COR2' and sat_name eq 'STEREO_B' and extracor2B) then $
    cor2B=1 else cor2B=0; allows for extra masking if asked
;  print,detector,sat_name,cor2B
  mdata=p_getmask(detector,N,k0,L0tau,ratio=maskratio,cor2horn=cor2B)

;  if (pointer) then mask=ptr_new(mdata) else mask=mdata
  if (pointer) then begin
      if (n_elements(mask) gt 1) then begin
          ; is an array of pointers
          mask[iele]=ptr_new(mdata)
      end else begin
          ; just a solitary pointer
          mask=ptr_new(mdata)
      end
  end else begin
      if (size(mask,/n_dimensions) gt 2) then begin
          ; is an array of data
          mask[*,*,iele]=mdata
      end else begin
          ; is a single data
          mask=mdata
      end
  end

  if (verbose) then print,'Centers: boresight = ',k0,', sun = ',sunk0

;  (eps = pointing angle from sun center)

  imodel = 10; instrument-specific model, 10=B/B0

  ; sets 'sr.an, sr.limbdark' as well as values needed for get_Istd_nd
  dvoxfrac=pxwrapper.sr.dvox/L0tau

;  chl=-1; set chl to -1 for test/debug with large images
  o_flimb,chl,N,1.0/L0tau,dvoxfrac,lla0,fa,fb,fc,rmin2,an,limbdark
  Istd = get_istd_Nd(imodel,L0,n0,L0tau,ImI0 = limbdark)


  ; date_obs now uses 'obsdate' from header, rather than the switch below
;  if (tag_exist(hdr,'date-obs')) then begin
;      date_obs=hdr.date-obs; secchi or lasco
;  end else if (tag_exist(hdr,'date_obs')) then begin
;      date_obs=hdr.date_obs; secchi or lasco
;  end else if (tag_exist(hdr,'date')) then begin
;      date_obs=hdr.date; sim or other case
;  end else begin
;      print,'No date found, using current day (probably wrong!)'
;      date_obs=systime(/julian)
;  end
;  if (verbose) then print,'date_obs: ',date_obs

  ; need exptime in seconds, note level 1 tB uses '-1' but keeps
  ;   the original exptime in expcmd
  if (tag_exist(hdr,'exptime')) then exptime=hdr.exptime else exptime=6.0
  if (exptime eq -1) then if (tag_exist(hdr,'expcmd')) then $
    exptime=hdr.expcmd else exptime=6.0

  MSBperDN=get_msb2dn(hdr)
  if (sat_name eq 'SOHO' or sat_name eq 'LASCO') then DNperPhoton = 13.0 else $
    DNperPhoton = 15.29         ; SECCHI value, also placeholder or best guess

  if (tag_exist(hdr,'biasdev')) then biasdev=hdr.biasdev else biasdev=0.0
;  if (tag_exist(hdr,'ipsum')) then begin
;      ; quickly make a fake header to get the info
;      if (tag_exist(hdr,'wavelnth')) then wave=hdr.wavelnth else wave=0
;      fakehdr={SECCHI_HDR_STRUCTTEMP,$
;               IPSUM:hdr.ipsum,$
;               CCDSUM:hdr.ccdsum,$
;               DETECTOR:detector,$
;               OBSRVTRY:sat_name,$
;               WAVELNTH:wave $
;              }
;
;      MSBperDN=get_calfac(fakehdr)
;      DNperPhoton = 15            ; placeholder or best guess
;
;  end else begin
;      MSBperDN = 1                ; not used for non-SECCHI,
;                                ; e.g. Lasco 0.5 FITS is in DN
;      DNperPhoton = 15; placeholder or best guess
;  end

  ; set some placeholder/defaults for now
  eps=double(0)
  set = sin(eps)/tau
  roco = long(0); only for opixon
  as = long(1);

;  z0=0.0; important, as z0 not properly implemented in cpixon

  srjr = { $
           Ndata: N, $
           scaling: scaling, $ ; tracks how original data was resampled
           sat_name: sat_name, $
           tele_name: tele_name, $
           detector: detector, $
           L0tau:  L0tau, $
           tau: tau, $
           rho: rho, $ ; same as gamma, but better descriptor
           gamma: rho, $ ; old pixon definitions, kept for legacy apps
           phi: phi, $ ; same as theta, but better descriptor
           theta: phi, $ ; old pixon definitions, kept for legacy apps
           LL: LL, $
           z0: z0, $
           polar: polar, $
           fan: fan, $
           k0: ptr_new(k0), $
           sunk0: ptr_new(sunk0), $
           Istd: Istd, $
           limbdark: limbdark, $
           imodel: imodel, $
           date_obs: obsdate, $
           MSBperDN: MSBperDN, $
           DNperPhoton: DNperPhoton, $
           biasdev: biasdev, $
           hae_coords: hae_coords, $
           exptime: exptime, $
           ecliptic: ecliptic, $
           align: align, $
           roco: roco, $
           eps: eps, $
           set: set, $
           as: as, $
           level: level, $
           bunit: bunit $
         }
  ; returns returndata, mask, srjr

  if (verbose) then print,'srjr polar and fan: ',srjr.polar,srjr.fan

  ; optionally updates 'pxwrapper' if provided
  if (n_elements(pxwrapper) ne 0 and n_elements(iele) ne 0) then begin
        pxwrapper_update,pxwrapper,'sr',srjr,iele
        pxwrapper.sr.an=an; being a dbl(3), requires manual update
  end

end
