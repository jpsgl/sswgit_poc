;+
;
; Project   : s3drs and Pixon reconstruction software
;                   
; Name      : tpxn_make
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: (part of the testing and legacy code, documented in code)
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 

; Keywords  :
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : (see CVS logs)
;
; Written     : Sandy Antunes, NRC, 2007-2009
;               
;-            
pro tpxn_make,pxwrapper,timage,oTPixon

;			varnaud2_make2_tpxn.pro

; $Id: tpxn_make.pro,v 1.1 2009/04/22 18:31:47 antunes Exp $
;+
; <p> The first sentence summarizes the documentation.
;
; <p> It is followed by a more detailed description.  Paragraphs should be
;    hung-indented as in this example.
;
; <p> Modify the author tag as appropriate.  Other tags follow the author tag.
;    See IDLDOC_FILES.HTML for a complete list of tags.
;
; @copyright
;    Copyright � Pixon LLC, 1999-2004.  All rights reserved.
;    Unauthorized reproduction prohibited.
; @author
;    <a href="mailto:Amos.Yahil\@Pixon.com">Amos Yahil, Pixon LLC.</a>
;-

; Compilation options

   COMPILE_OPT IDL2, HIDDEN, STRICTARRSUBS

sr=pxwrapper.sr
sc=pxwrapper.sc
sp=pxwrapper.sp
;sf=pxwrapper.sf

   tag=get_tag()

; Initialization

  dim=sr.n
  mxit=sp.mxit
  if(n_elements(recsave) eq 0)then recsave=1

  display=0
  dtor = !DPI/180
  if(n_elements(note) eq 0)then note=''
  if(n_elements(mxit) eq 0)then mxit=50 else note=note+' mxit='+string(mxit,"(i3)")
  ndata=1		; maximum of 1 data set per satellite
  npersp=214.943/60.
  nview=n_elements(sr.gamma)		; number of satellites
  q=where(sr.polar ne 'b',count)
  if(count ne 0)then stop,'ERROR - nview assignment needs polar=b'
  seed0=0
  phi=sr.gamma
  theta=sr.theta
  wait = 0

   IF N_ELEMENTS(reltol) EQ 0 THEN		reltol = 1e-4
   IF N_ELEMENTS(snr) EQ 0 THEN			snr = 30.0 
   IF N_ELEMENTS(tiff) EQ 0 THEN		tiff = 0

; Derived parameters

   IF KEYWORD_SET(cpu) THEN BEGIN
      display = 0
      seed0 = 1235711
   ENDIF
   ddim = REPLICATE(dim, 2)
   idim = REPLICATE(dim, 3)
   seed = seed0

; Truth image, full resolution

   true = timage
   vrtx = Coord(idim) - dim/2	; vertex at all N^3 points
   nvrtx = N_ELEMENTS(vrtx)/3

; Pointer arrays

   cpol  = PTRARR(nview)
   data  = PTRARR(ndata,nview)
   sigma = PTRARR(ndata,nview)
   ihat  = PTRARR(nview)
   khat  = PTRARR(nview)
   persp = FLTARR(nview)
   pos   = PTRARR(nview)
   user  = PTRARR(nview)

; specify user parameters

;  rr = 1/sr.L0tau	; solar radius in image units
;  prr = PTR_NEW(rr)

; Loop on views

   FOR iv=0,nview-1 DO BEGIN

       rr = 1/sr.L0tau[iv]          ; solar radius in image units
       prr = PTR_NEW(rr)
; Tomographic parameters

      sptomo=SIN(dtor*phi[iv])
      cptomo=COS(dtor*phi[iv])
      sttomo=SIN(dtor*theta[iv])
      cttomo=COS(dtor*theta[iv])

      ihat[iv] = PTR_NEW([-sptomo  ,cptomo   ,0 ])
      khat[iv] = PTR_NEW([sttomo*cptomo,sttomo*sptomo,cttomo])

; SANDY DISTANCE MOD, should include actual distance term
      persp[iv] = npersp*dim
;;      pos[iv] = PTR_NEW(-persp[iv]*(*khat[iv]))
      pos[iv] = PTR_NEW(-persp[iv] * sr.LL[iv] * (*khat[iv]))


; Polarization coefficients & data frames
; (introduced initially as noise only)

;      cpol[iv] = PTR_NEW([[1.0,0.0,0.0],[1.0,0.0,0.0]])
      data[0,iv]  = PTR_NEW(RANDOMN(seed, ddim), /NO_COPY)	; noise
      sigma[0,iv] = PTR_NEW(fltarr(ddim)+1,/NO_COPY)

      user[iv]=prr  ; e.g. = 1/L0tau
      PTR_FREE,prr
   ENDFOR

; TPixon object

   note=note+' @P'
   sc.note=note

   tmake = SYSTIME(1)
   oTPixon = OBJ_NEW('TPixon', cpol=cpol, data=data, ihat=ihat, image=true, $
                     khat=khat, mxit=mxit, persp=persp, pos=pos, $
                     reltol=reltol, sigma=sigma, vrtx=vrtx, user=user,/poisson)
   tmake=systime(1)-tmake

   fname0=''
   fname1=''
   fname2=''

   if(recsave)then begin
     fname0='oTPixonv0_'+tag+'_N' + strtrim(string(dim),2) + '.dat'
     fname0h='$PIXHERE/'+fname0
     fname0s='$PIXSAVE/'+fname0
     save,filename=fname0h,oTPixon,sr,sc
     print,'data 0 saved to ',fname0h
     mysave = fname0h
;     spawn,'gzip -c '+fname0h+' > '+fname0s+'.gz'
     fname1=fname0; in case we do not use noise later
   endif
 
; get background-free data

   dm = REFORM(oTPixon -> Get(/DM), ndata, nview, /OVERWRITE)	; data from true image

   q=where(ptr_valid(dm) eq 1)
   nn=n_elements(q)
   datf=fltarr(dim,dim,nn)
   for i=0,nn-1 do datf[*,*,i]=*dm[q[i]]

if (n_elements(usenoise) eq 0) then usenoise=0

if (usenoise) then begin

; implement noise

sigma = PTRARR(ndata,nview)	; ??? question for Amos

   Smax = sqrt(1/sn.ftol[0])
   Smin = sqrt(1/sn.ftol[1])
   Dfmin=0
   Dfmax=0
   for iv=0,nview-1 do for n=0,ndata-1 do Dfmax=max([Dfmax,(*dm[n,iv])[*]]) 
   a = (Dfmax-Dfmin)/(Smax^2-Smin^2)
   Nb = (Dfmax*Smin^2-Dfmin*Smax^2)/(Dfmax-Dfmin) 
   for iv=0,nview-1 do for n=0,ndata-1 do sigma[n,iv]= PTR_NEW(sqrt(a*((*dm[n,iv])+a*Nb)),/no_copy)

   for iv=0,nview-1 do for n=0,ndata-1 do for i=0,dim-1 do for j=0,dim-1 do $
     (*data[n,iv])[i,j]=a*randomn(seed,poisson=(*dm[n,iv])[i,j]/a+Nb)   

oTPixon->Replace,sigma=sigma	; ??? question for Amos

;stop,'stopped after datf and data calculated'

; Check for consistency

   oTPixon -> Check

; Residuals and chi^2

   chi2 = oTPixon -> Chi2(/TOTAL)
   dof = oTPixon -> Get(/DOF)
   PRINT, 'CHI2, Q', chi2, (chi2 - dof)/SQRT(2*dof)

   oTPixon -> Replace, image=PTR_NEW(FLTARR(nvrtx), /NO_COPY)
   oTPixon -> DM

   chi2 = oTPixon -> Chi2(/TOTAL)
   dof = oTPixon -> Get(/DOF)
   PRINT, 'CHI2, Q', chi2, (chi2 - dof)/SQRT(2*dof)

; 3-D visualization of the truth model

   IF KEYWORD_SET(display) THEN BEGIN
      start = MIN(vrtx, max=delta) - 2.0
      delta = (delta - start + 2.0)/(dim - 1)
      ph = oTPixon -> Get(/PH, /NOPOINTER)
      ttrue = QGRID3(vrtx, true, ph, delta=delta, dimension=dim, start=start)
      PXNdisp, ttrue, $
               title='Input_Electron_Density_X-Y', wait=wait, $
               /BAR, /DW, /FREE, TIFF=tiff
      PXNdisp, ttrue, 1, 3, ll=[0,dim/2-3,0], $
               title='Input_Electron_Density_X-Z', wait=wait, $
               /BAR, TIFF=tiff
      XVOLUME, BYTSCL(ttrue)
   ENDIF

   if(recsave)then begin
     fname1='oTPixonv1_'+tag+'.dat'
     fname1h='$PIXHERE/'+fname1
     fname1s='$PIXSAVE/'+fname1
     save,filename=fname1h,oTPixon
     mysave = fname1h
     print,'data 1 saved to ',fname1h
;     spawn,'gzip -c '+fname1h+' > '+fname1s+'.gz'
   endif

; Run Pseudo with the full grid

   oTPixon -> Pseudo

; Save the OTPIXON object before deletions

   if(recsave)then begin
     fname2='oTPixonv2_'+tag+'.dat'
     fname2h='$PIXHERE/'+fname2
     fname2s='$PIXSAVE/'+fname2
     save,filename=fname2h,oTPixon
     mysave = fname2h
     print,'data 2 saved to ',fname2h
;     spawn,'gzip -c '+fname2h+' > '+fname2s+'.gz'
   endif

; Delete the truth image and vertex structure

   oTpixon -> Free, /BOUNDS, /CONNECT, /FDX, /FPM, /IMAGE, /JPOL, /PH, /SDX, $
                    /SPM, /VRTX

; Save the OTPIXON object after delections

   oname='otpixon_' + STRTRIM(dim, 2) + '.idl'
   print,'isaving object as ',oname
   SAVE, oTPixon, file=oname
   mysave = oname
   print,'saved otpixon object to ',oname

   save,filename='junk.dat',sr,sp,sn,sc,datf,a

;   spawn,'rm '+fname0h
;   spawn,'rm '+fname1h
;   spawn,'rm '+fname2h

; Done

endif

  sf = {$
         fname0: fname0, $
         fname1: fname1, $
         fname2: fname2, $
         mysave: mysave, $
         tmake: tmake $
        }

;  sf2={sf:sf}

;pxwrapper = create_struct(pxwrapper,sf2) ; appends 'sf' to existing pxwrapper
pxwrapper=addrep_struct(pxwrapper,'sf',sf)

END
