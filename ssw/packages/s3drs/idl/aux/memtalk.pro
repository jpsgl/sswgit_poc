;+
;
; Project   : s3drs and Pixon reconstruction software
;                   
; Name      : memtalk
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: (part of the testing and legacy code, not documented herein)
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 

; Keywords  :
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : (see CVS logs)
;
; Written     : Sandy Antunes, NRC, 2007-2009
;               
;-            
; simple routine to print out the memory use in MB plus a reference
; prints both current and (highwater) usage plus allocs/frees, in MB
; e.g. memtalk 'break 1' prints out
;     ** break 1: 10MB (10MB)
;

PRO memtalk,tag

  if (n_elements(tag) eq 0) then tag='Memory'

  cm=strtrim(string(floor(memory(/current)/1e6)),2)
  hm=strtrim(string(floor(memory(/highwater)/1e6)),2)

  na=strtrim(string(floor(memory(/num_alloc)/1e6)),2)
  nf=strtrim(string(floor(memory(/num_free)/1e6)),2)

  st=strmid(systime(),11,8); clock time HH:MM:SS only

  print,'** '+tag+': '+cm+' MB ('+hm+' MB), '+na+'/'+nf+' alloc/free   '+st

END
