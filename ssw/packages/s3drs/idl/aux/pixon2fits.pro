;+
;
; Project   : s3drs and Pixon reconstruction software
;                   
; Name      : pixon2fits
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: (part of the testing and legacy code, documented in code)
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 

; Keywords  :
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : (see CVS logs)
;
; Written     : Sandy Antunes, NRC, 2007-2009
;               
;-            
; saves a Pixon original data (/data or default), render (/render),
; or noise file (/sigma) as a FITS file.  Data and render are
; ingest-able to p_ingest_fits.

; To create a model file as if it was from a detector, you should:
; 1) create a blank SECCHI Cor2A Level 1.0 FITS header as below
; 2) Insert the data, scaled to a typical dmax of 1e-07, at its usual N
; 3) Run p_ingest_fits and p_noisewrapper as in really.pro

; For a Cor2A tB sim data, just needs N,xyz, and data:


pro testpixonfits
  prep_3drv,pxwrapper,N=128,gamma=[0,45,90],theta=[0,0,45]
  N=pxwrapper.sr.Ncube
  data=make_array(N,N,/double,/index)
  m=2
  xyz=*(pxwrapper.sr.hae_coords[m])
  hdr={naxis:2,naxis1:N,naxis2:N,detector:'COR2',obsrvtry:'A',instrume:'SIM',$
    hae_unit:'AU',haex_obs:xyz[0],haey_obs:xyz[1],haez_obs:xyz[2],$
    simdate:systime(),polar:'b',fan:0.0}
  writefits,'cor2sim.fts',data,struct2fitshead(hdr)
  p_ingest_fits,'cor2sim.fts',rdata,mask,srjr,/verbose
  print,srjr.rho,srjr.phi,srjr.z0,srjr.LL
  print,*srjr.hae_coords
end
;(to add polarizations, set polar: and fan:)



;  HEADER FOR SIM DATA REQUIRES:
;  
;  ** REQUIRED
;  
;  hdr.naxis=2
;  hdr.naxis1=N
;  hdr.naxis2=N
;  hdr.detector='COR2' (required: used for mask, calibration values, etc)
;  
;  hdr.obsrvtry=e.g. 'STEREO_A_SIM','STEREO_B_SIM','SOHO_SIM', or variants
;  hdr.instrme=e.g. 'SECCHI_SIM','LASCO_SIM, or even just 'SIM''
;  
;  heax_obs,heay_obs,heaz_obs (presumed in meters but see below)
;  hea_unit='m' (default) or 'au' (optional)
;  
;  simdate (IDL systime() or FITS date/time field)
;  hdr.polar='b' or 'p' or 't' or 'r'
;  hdr.fan= polar angle (if any, i.e. if not 'b'... default is 0)
;  
;  hdr.msb2dn (e.g. as per c2_calfactor or get_calfac,
;       e.g. Cor2A=2.7E-12 Bsun/DN, otherwise default is '1.0')
;  
;  ** OPTIONAL
;  
;  hdr.exptime (in seconds, otherwise default is 1.0)
;  hdr.crpix1, hdr.crpix2 (optical center, if not given defaults to N/2,N/2)
;  hdr.crval1,hdr.crval2 (offset to sun, defaults to 0,0)
;  hdr.bunit (either 'MSB' if level 1.0 or other for 0.5, default is 'MSB')
;  hdr.rsun and hdr.cdelt1 -> alternative L0tau
;     (default sim L0tau derives a hard-coded FOV based on detector
;     (very useful since dataN may not equal usual real data N!))
;  



PRO pixon2fits,pxwrapper,oPixon,data=data,sigma=sigma,render=render,$
               filename=filename

  pixon_env,pxwrapper.object
  data=KEYWORD_SET(data)
  sigma=KEYWORD_SET(sigma)
  render=KEYWORD_SET(render)
  if (sigma eq 0 and render eq 0) then data=1; sets default option
  stem='_data.fts'
  if (sigma) then stem='_sigma.fts'
  if (render) then stem='_solution.fts'
  if (n_elements(filename) eq 0) then filename=pxwrapper.sc.gname + stem

  N=pxwrapper.sr.Ncube
  Nd=pxwrapper.sr.Nd

  if (render) then begin
      savedata=stereo_project(pxwrapper,oPixon)
  end else if (sigma) then begin
      savedata=oPixon->Get(/sigma,/nop)
  end else begin
      savedata=oPixon->Get(/data,/nop)
  end
  
; Not sure if I want to handle normalization yet, and how, so skipping for now
  normd=pxwrapper.sr.normd

  for m=0,pxwrapper.sr.Nd-1 do begin

      det=pxwrapper.sr.detector[m]
      tele=pxwrapper.sr.tele_name[m]
      sat=pxwrapper.sr.sat_name[m]

      polar=pxwrapper.sr.polar[m]
      fan=pxwrapper.sr.fan[m]
      simdate=pxwrapper.sr.date_obs[m]
      xyz=*(pxwrapper.sr.hae_coords[m])

      hdr={naxis:2,naxis1:N,naxis2:N,$
           detector:det,obsrvtry:sat,instrume:tele,$
           hae_unit:'AU',haex_obs:xyz[0],haey_obs:xyz[1],haez_obs:xyz[2],$
           simdate:systime(),polar:polar,fan:fan}

      fname=trim(string(m),2)+'_'+filename

      writefits,fname,savedata,struct2fitshead(hdr)
  end

END
