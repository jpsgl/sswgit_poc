;+
;
; Project   : s3drs and Pixon reconstruction software
;                   
; Name      : p_autorange
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: (part of the testing and legacy code, not documented herein)
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 

; Keywords  :
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : (see CVS logs)
;
; Written     : Sandy Antunes, NRC, 2007-2009
;               
;-            
; determines necessary normalization for data to ensure
; that later Pixon data will fall in the range of 0 to 'autorange'
;
; Note this throws away any existing sr.normd if an autorange is
; requested.  So you can either manually set sr.normd, or ask it
; to autorange, but not both.
;

PRO p_autorange,data,pxwrapper

  autorange=pxwrapper.sp.autorange

  ; note we do nothing if no autorange is requested
  if (autorange le 0) then return

  print,'Check is: if datamax * wrapper[0] eq wrapper[1], it worked'
  print,'***'

;  normd=pxwrapper.sr.normd; default

  dp=force_ptrs(data);

  dmax=max(*(dp[0]));
  for i=1,n_elements(dp)-1 do begin
      dtest=max(*(dp[i]))
      if (dtest gt dmax) then dmax=dtest
  end

;  print,'autorange dmax = ',dmax

;  normd = normd * (dmax * autorange)
  normd = autorange / dmax

  pxwrapper.sr.normd=normd

  ; free our copy if and only if our force_ptr did a copy, not reference
  if (datatype(data) ne 'PTR') then $
    for i=0,n_elements(dp)-1 do ptr_free,dp[i]

end

PRO test_p_autorange,autorange=autorange

  if (n_elements(autorange) eq 0) then autorange=-1

  sr={normd:1.0}
  sp={autorange:autorange}
  pxwrapper={sr:sr,sp:sp}

  print,'Wrapper: ',pxwrapper.sr.normd,pxwrapper.sp.autorange

  data=make_array(4,4,3,/double,/index)
  help,data
  print,'max data = ',max(data)
  print,'autoranging'
  p_autorange,data,pxwrapper

  print,'Wrapper: ',pxwrapper.sr.normd,pxwrapper.sp.autorange

  dp=ptrarr(3)
  for i=0,2 do dp[i]=ptr_new(data[*,*,i])
  print,'New pointers: '
  help,dp
  help,dp[1]
  help,*(dp[1])
  
  print,'autoranging'
  p_autorange,dp,pxwrapper

  print,'Recheck pointers: '
  help,dp
  help,dp[1]
  help,*(dp[1])

  print,'Wrapper: ',pxwrapper.sr.normd,pxwrapper.sp.autorange

END
