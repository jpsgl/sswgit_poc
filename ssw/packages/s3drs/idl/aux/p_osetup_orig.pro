; Modified 'stereo_osetup' to handle data of different N.
;+
; $Id: p_osetup_orig.pro,v 1.1 2009/04/22 18:31:33 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : p_osetup_orig
;               
; Purpose   : 
;               
; Explanation: 
;               
; Use       : IDL> 
;    
; Inputs    : 
;               
; Outputs   : 
;
; Keywords  : 
;               
; Calls from LASCO : none 
;
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : Pixon
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRL, Nov-Dec 2005
;               
; $Log: p_osetup_orig.pro,v $
; Revision 1.1  2009/04/22 18:31:33  antunes
; first half of relocation.
;
; Revision 1.2  2009/04/21 19:08:45  antunes
; Updated up through 'r' with SSW headers.  Also minor John bug fixes.
;
; Revision 1.1  2009/03/31 14:56:48  antunes
; Re-org prepatory to checking into SSW.
;
; Revision 1.19  2008/01/25 16:10:38  antunes
; Major refactoring of code to allow differing FOVs.  Note Pixon
; structures here are now incompatible with earlier versions!
;
; Revision 1.18  2008/01/24 20:14:41  antunes
; More diagnostics and tests.
;
; Revision 1.17  2008/01/09 20:42:14  antunes
; Added toggle and code for slower but more accurate rotations.
;
; Revision 1.16  2008/01/09 19:24:28  antunes
; Finally fixed the data scaling unit (bug=legacy code used AU not Rsun),
; also added more tests.
;
; Revision 1.15  2007/11/21 23:12:24  antunes
; Updates to test scripts and diagnostics.
;
; Revision 1.14  2007/10/03 18:44:20  antunes
; Implemented spherical coordinates in cpixon, major update!
;
; Revision 1.13  2007/09/14 13:40:06  antunes
; Improved scaling.
;
; Revision 1.12  2007/08/28 15:35:05  antunes
; Better automated running on real data.
;
; Revision 1.11  2007/06/26 18:56:18  antunes
; Got Pixon to work with real SECCHI FITS data!
;
; Revision 1.10  2007/06/21 18:29:35  antunes
; Finished test cases and got issues reconciled.
;
; Revision 1.9  2007/06/07 18:22:52  antunes
; More test cases.
;
; Revision 1.8  2007/03/27 18:48:13  antunes
; Fixed sigma scaling level problem.
;
; Revision 1.7  2007/03/26 17:44:23  antunes
; Fixed Pixon normalization woes, added documentation of entire Pixon flow.
;
; Revision 1.6  2007/02/23 14:04:57  antunes
; Succeeded in N=256 2-image reconstruction!
;
; Revision 1.5  2007/02/21 17:14:41  antunes
; Minor noshm mod to make it easier to not use shared memory via sc.noshm
;
; Revision 1.4  2007/02/21 16:43:19  antunes
; Memory mods to allow for higher N (N=256 now works!)
;
; Revision 1.3  2007/01/09 20:06:03  antunes
; Test now works with pixon tetra
;
; Revision 1.2  2006/12/21 18:15:37  antunes
; Better pretty pictures, more test cases for sims.
;
; Revision 1.1  2006/12/08 18:12:30  antunes
; Better plotting, time handling, etc.
;
; Revision 1.10  2006/11/30 20:03:57  antunes
; Updated pixon and other printing and output routines.
;
; Revision 1.9  2006/11/17 19:02:33  antunes
; Updates to Pixon structures.
;
; Revision 1.8  2006/11/16 18:27:33  antunes
; Did a rollback then an update to allow pixon wrapper to handle
; heterogeneous images.
;
; Revision 1.5  2006/05/25 18:18:30  antunes
; Better handling.
;
; Revision 1.4  2006/05/02 18:47:58  antunes
; Added test wrappers for Pixon tests.
;
; Revision 1.3  2006/04/27 18:53:48  antunes
; Allow for multiple 'dragger' and reentrant GUI, removed last common,
; also testing masks in pixon.
;
; Revision 1.2  2006/04/24 19:04:01  antunes
; Fixed shared c/t pixon path environment problem.  Redid GUI to
; use widget struct rather than common so it's reentrant.  Also
; added in better comments, user input.  Now GUI is ready for
; prime time for render testing.
;
; Revision 1.1  2006/04/11 15:55:06  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.6  2006/03/16 20:25:28  antunes
; Improved pixon calling, wrote proto-frontend GUI for
; different rendering tests.
;
; Revision 1.5  2006/03/08 17:52:23  antunes
; Abstracted pixon structures into an aggregate structure in order
; to be encapsulate Pixonisms from general rendering and reconstruction
; routines.
;
; Revision 1.4  2006/02/21 18:10:19  antunes
; Improved shared code for C and T pixon.
;
; Revision 1.3  2006/01/09 17:14:42  antunes
; More commented added.
; New 'super3drv' driver routine for getting user specs for reconstruction
; process now checked in.  Just type 'super3drv' to test.
;
;-            


pro p_osetup_orig,pxwrapper,timage,oPixon

; input   sr           rendering parameters
;         sp           pixon parameters
;         sc           misc.
;
; output  oPixon       pixon object (with timage)
;         userstruct   input structure for dsf, dsft
;         sr.istd      standard intensity according to instrument model
;         sc.machine
;         sc.date
;         sc.tag       unique tag for filenames
;------------------------------------------------------------------

common debugs,phys,tdsf,tdsft,dbg

dbg = -1.0
dbug=0

tdsf = 0
tdsft = 0

IF (OBJ_VALID(oPixon_map)) THEN OBJ_DESTROY, oPixon_map, /FREE
IF (OBJ_VALID(oPixon_fit)) THEN OBJ_DESTROY, oPixon_fit, /FREE
if (obj_valid(oPixon)) then obj_destroy,oPixon,/free
;heap_gc,/verb
; note that doing heap_gc including pointers can wipe out the
; pointers in pxwrapper.sr.k0 and pxwrapper.sr.sunk0, so it is
; not recommended.  Instead, the oPixon destroy method should
; ensure any relevant oPixon pointers are deleted directly, so
; that we do not have to call heap_gc
heap_gc,/obj

print,'*** stereo_osetup after memory free'
help,/mem

; need sr and sp for sure

Ncube          =  pxwrapper.sr.Ncube
Ndata          =  pxwrapper.sr.Ndata; array of Ns
fwhm_2d    =  0
fwhm_3d    =  0
tdsf       =  pxwrapper.sc.tdsf
tdsft      =  pxwrapper.sc.tdsft

machine    =  getenv('HOST')
pxwrapper.sc.machine =  machine
pxwrapper.sc.date    =  systime()

; ----- check for errors

err=0
Nd=n_elements(pxwrapper.sr.gamma)  
if(n_elements(pxwrapper.sr.LL)    ne Nd)then err=1
if(n_elements(pxwrapper.sr.z0)    ne Nd)then err=2
if(n_elements(pxwrapper.sr.eps)   ne Nd)then err=3
;if(n_elements(set)   ne Nd)then err=4
if(n_elements(pxwrapper.sr.polar) ne Nd)then err=5
;if(n_elements(sc.sat_name) ne Nd)then err=6
;if(n_elements(sr.k0[0,*])  ne Nd)then err=7
if(n_elements(pxwrapper.sr.fan)   ne Nd)then err=8
if(n_elements(pxwrapper.sr.roco)  ne Nd)then err=9
;q=where((pxwrapper.sr.roco ne 0) and (pxwrapper.sr.roco ne 1),c)
;if(c gt 0)then err=10
q=where(finite(pxwrapper.sr.z0) eq 0,count)
if(count gt 0)then err=11

if(err ne 0)then begin
  print,'ERROR ',err,' found in stereo_osetup_06'
  stop
end

; -----

caldat,systime(/julian),mon,day,yr,hr,min,sec
fm = "(i2.2)"
pxwrapper.sc.tag = string(yr-2000,fm) + string(mon,fm) + string(day,fm) + $
         string(hr,fm)      + string(min,fm) + string(round(sec),fm)

if(n_elements(phys) eq 0)then phys = 1

if(dbug)then begin
;print,'CHECK 1'
ans=''
read,'in stereo_osetup (CR to continue) >',ans
if(ans eq 'x')then stop
end

; -----

idims  =  [Ncube,Ncube,Ncube]
;ddims  =  [Ndata[0],Ndata[0]]

;Nd  =  N_elements(gamma)  ; number of data frames

; ----- Calculate Limb Darkening Fuctions

;o_flimb,sr.chl,N,1/sr.L0tau,sr.d,sr.lla0,fa,fb,fc,rmin2,an
;sr.an=an

; ----- calculate standard intensity

;sr.istd  =  get_istd(1,sr,ImI0 = ImI0)
for m=0,Nd-1 do begin

    dvoxfrac=pxwrapper.sr.dvox/pxwrapper.sr.L0tau[m]
    o_flimb,pxwrapper.sr.chl,Ncube,1/pxwrapper.sr.L0tau[m], $
      dvoxfrac,pxwrapper.sr.lla0,fa,fb,fc,rmin2,an,ImI0
    pxwrapper.sr.an=an
    pxwrapper.sr.limbdark[m]=ImI0
    pxwrapper.sr.Istd[m]  =  $
      get_istd_Nd(pxwrapper.sr.imodel[m],pxwrapper.sr.L0,pxwrapper.sr.n0, $
                  pxwrapper.sr.L0tau[m],ImI0 = ImI0)
    ;print,'istd is ',pxwrapper.sr.Istd[m]
end

; ----- Create Pixon Object 

jjj    =  check_math(0,1)	
;prf2d  =  gprf(ddims, fwhm = fwhm_2d)
prf2d=PTRARR(Nd)
for m=0,Nd-1 do prf2d[m]  =  ptr_new(gprf([Ndata[m],Ndata[m]], fwhm = fwhm_2d))
jjj    =  check_math()

;data      =  dblarr(Ndata,Ndata,Nd)+1     ; dummy data
sigma     =  0.05                ; dummy sigma
;sigma     =  1 ; default sigma, lets pixon decide whatever it wants
;true_nfd  =  dblarr(Ndata,Ndata,Nd)
sigma=0

data_ptr  =  PTRARR(Nd)
for m=0,Nd-1 do data_ptr[m]=ptr_new(dblarr(Ndata[m],Ndata[m]))
;FOR m  =  0,Nd-1 DO begin
    ; Note this WTF-- you have to assign a value (e.g. 1) to
    ; this data or the allocation-pointer-object method fails.
    ; Best guess is either IDL or Pixon treats 'ptr_new(dblarr(N,N))'
    ; differently than 'ptr_new(make_array(N,N,/dble,value=X)'
    ; To make this odder, the actual value stored here does matter,
    ; so this may be more than just a memory placeholder until actual
    ; data is inserted.  Rather than fully understand this by
    ; examining the Pixon object methods, we choose to just document
    ; it and move on.
;    if (pxwrapper.sc.doubleprec) then begin
;        data = dblarr(Ndata[m],Ndata[m])+0
;    end else begin
;        data = fltarr(Ndata[m],Ndata[m])+0 ; TEST FLOAT
;    end
;    data_ptr[m]  =  PTR_NEW(data)
;end

;fprf=PTRARR(Nd)
;for m=0,Nd-1 do begin
;  prepFPRF returns a ptrarr(ndata,nchan)
; fprfset=PrepFPRF(*(prf2d[m]),ddim=[Ndata[m],Ndata[m]],idim=[Ndata[m],Ndata[m]],/FREE )
; fprf[m] = fprfset[0]
;end

tau2=make_array(Nd,/double)
tau2 = pxwrapper.sr.tau*pxwrapper.sr.tau

if(dbug)then begin
;print,'CHECK 2'
ans=''
read,'in stereo_osetup (CR to continue) >',ans
if(ans eq 'x')then stop
end

;mystruct  =  {uservariable,   $
;mystruct  =  { $
;fprf:    fprf,              $
;roco:    0L,                $
;polar:   'x',               $
;c2z:     dblarr(Ndata,Ndata),       $
;s2z:     dblarr(Ndata,Ndata),       $
;lla0:    dblarr(3),         $
;rho2:    dblarr(Ndata,Ndata),       $
;rhos2:   dblarr(Ndata,Ndata),       $
;psLs:    dblarr(Ndata,Ndata),       $
;psLs2:   dblarr(Ndata,Ndata),       $
;i:       dblarr(Ndata,Ndata),       $
;jc:      dblarr(Ndata,Ndata),       $
;K2:      dblarr(Ndata,Ndata),       $
;d2:      0.0d0,             $
;fa:      fa,                $
;fb:      fb,                $
;fc:      fc,                $
;rmin2:   rmin2,             $
;tau:     0.0d0,             $
;tau2:    0.0d0,             $
;interp:  sr.interp,         $
;machine: machine,           $
;g:       0.0d0              $
;}

;userstruct  =  REPLICATE({uservariable},Nd)
;userstruct = REPLICATE(mystruct,Nd)

if(dbug)then begin
;print,'CHECK 3'
ans=''
read,'in stereo_osetup (CR to continue) >',ans
if(ans eq 'x')then stop
end

user  =  PTRARR(nd)

for k = 0,Nd-1 do begin

    ; note ffunc modifies 'pxwrapper.sr' (trW,trRRs,trP,trVs)
    ; as well as returning values
  ffunc,pxwrapper.sr,k,ps,mu,nu,is,jc,K2,u,x,y,rhos2,c2z,s2z

  rho2   =  x*x+y*y
  psLs   =  ps*pxwrapper.sr.LL[k]
  psLs2  =  psLs^2

  ; prepFPRF returns a ptrarr(ndata,nchan)
  fprfset=PrepFPRF(*(prf2d[k]), $
                   ddim=[Ndata[k],Ndata[k]], $
                   idim=[Ndata[k],Ndata[k]],/FREE )
  fprf = fprfset[0]

;  print,'rho2 is ',rho2 ; lousy way to proof matrixes
;  print,'psLs is ',psLs ; lousy way to proof matrixes
;  print,'psLs2 is ',psLs2 ; lousy way to proof matrixes
;  print,'c2z is ',c2z ; lousy way to proof matrixes
;  print,'s2z is ',s2z ; lousy way to proof matrixes
;  print,'rhos2 is ',rhos2 ; lousy way to proof matrixes
;  print,'fprf is ',*fprf ; lousy way to proof matrixes
;  print,'is is ',is ; lousy way to proof matrixes
;  print,'jc is ',jc ; lousy way to proof matrixes
;  print,'k2 is ',k2 ; lousy way to proof matrixes
;  print,'rmin2 is ',rmin2
;  print,'fa is ',fa ; lousy way to proof matrixes
;  print,'fb is ',fb ; lousy way to proof matrixes
;  print,'fc is ',fc ; lousy way to proof matrixes

  dvoxfrac=pxwrapper.sr.dvox/pxwrapper.sr.L0tau[k]

  mystruct  =  { $
                 fprf:    fprf,                               $
                 roco:    pxwrapper.sr.roco[k],               $
                 fast:    pxwrapper.sc.fast,                  $
                 theta:   pxwrapper.sr.theta[k],              $
                 polar:   pxwrapper.sr.polar[k],              $
                 c2z:     c2z,                                $
                 s2z:     s2z,                                $
                 lla0:    pxwrapper.sr.lla0,                  $
                 rho2:    rho2,                               $
                 rhos2:   rhos2,                              $
                 psLs:    psLs,                               $
                 psLs2:   psLs2,                              $
                 i:       is,                                 $
                 jc:      jc,                                 $
                 K2:      K2,                                 $
                 d2:      dvoxfrac^2,                         $
                 fa:      fa*dvoxfrac^3,                      $
                 fb:      fb*dvoxfrac^3,                      $
                 fc:      fc*dvoxfrac^3,                      $
                 rmin2:   rmin2,                              $
                 tau:     pxwrapper.sr.tau[k],                $
                 tau2:    tau2[k],                            $
                 interp:  pxwrapper.sr.interp,                $
                 machine: machine,                            $
                 g:       pxwrapper.sr.rho[k]                 $
               }

  user[k] = PTR_NEW(mystruct)

end

;help,user
;help,user[0]
;help,*user[0]
;help,*(user[0])
;help,*user[0],/str
;help,*(user[0]),/str

;for i = 0,nd-1 do user[i]  =  PTR_NEW(userstruct[i])

; ----- create Pixon object

prf3d   =  gprf(idims, fwhm  =  fwhm_3d)
nodisk  =  1 

;print,'before oPixon OBJ_NEW'
;help,/mem

if(dbug)then begin
print,'CHECK 4'
ans=''
read,'in stereo_osetup (CR to continue) >',ans
if(ans eq 'x')then stop
end

;weight =  make_array(Ndata,Ndata,Nd,value=1);
;weight[0:Ndata/2,0:Ndata/2,*]=0; ignore one quarter of each datum

; TEMPORARY bits are from largearray

;;defaultsigma=pxwrapper.sn.ftol[0]; best-guess, can be overwritten later
;;sigma=ptrarr(Ndata)
;;for m=0,nd-1 do $
;;  sigma[m]=ptr_new(make_array(Ndata[m],Ndata[m],/double,value=defaultsigma))

; Assume no weights for now.  Any weight gets overwritten anyway when
; you insert your data/sigma... at that time, you can either insert a
; weight after inserting the data, or let Pixon set its own weights.
;;wt=ptrarr(nd)
;;for m=0,nd-1 do $
;;  wt[m]=ptr_new(make_array(Ndata[m],Ndata[m],/float,value=1.0))

;wt     = weight,
method='paul'
;method='amos'
if (method eq 'amos') then begin
    print,'**************Creating with default non-Thomson rendering'
 ; using Amos's default renderer, with no 'user' or 'prf3d' structure
   oPixon = OBJ_NEW('Pixon', $
 data   = data_ptr,        $
 sigma  = sigma,           $
 npo    = pxwrapper.sp.npo,          $
 npxn   = pxwrapper.sp.npxn,         $
 anneal = pxwrapper.sp.anneal_3d,    $
 reltol = pxwrapper.sp.reltol,       $
 abstol = pxwrapper.sp.abstol,       $
 nodisk = nodisk,          $
 mxit   = pxwrapper.sp.mxit,         $
 noshm  = pxwrapper.sp.noshm,        $
 dllf   =  0               $
)
 oPixon->Replace,image=timage

end else begin
 ; using Paul's renderer, with 'user' structure
;    print,'*************Creating with Paul Thomson rendering'

;for m=0,nd-1 do *sigma[m]=*sigma[m]+0.05
;wt=1.0
;sigma=0.05
N=pxwrapper.sr.Ncube
if (n_elements(timage) eq 0) then timage=fltarr(N,N,N)
npo=pxwrapper.sp.npo
npxn=pxwrapper.sp.npxn
anneal_3d=pxwrapper.sp.anneal_3d
reltol=pxwrapper.sp.reltol
abstol=pxwrapper.sp.abstol
snrtol=1E-03
mxit=pxwrapper.sp.mxit
noshm=pxwrapper.sp.noshm

if (datatype(timage) eq 'PTR') then begin
    print,'Using pointer of image'
end else begin
    print,'Using array of image'
end


; place in a shmmap  item for 'phi' to ensure it is
; a memory mapped entity
shmmap,/DOUBLE,DIMENSION=[Ncube,Ncube,Ncube],GET_NAME=segname
tphi=ptr_new(shmvar(segname),/NO_COPY)

oPixon = OBJ_NEW('Pixon', $
                 data   = data_ptr,        $
                 sigma  = sigma,           $
                 phi    = tphi,             $
                 user   = user,            $
                 mxit   = mxit,            $
                 prf    = prf3d,           $
                 image  = timage,          $
                 nodisk = nodisk,          $
                 dllf   =  0,               $
                 npo    = npo,             $
                 npxn   = npxn,            $
                 anneal = anneal_3d,       $
                 snrtol = snrtol, $
                 reltol = reltol,          $
                 abstol = abstol,          $
                 noshm  = noshm           $
                )



end

if(dbug)then begin
print,'CHECK 5'
ans=''
read,'in stereo_osetup (CR to continue) >',ans
if(ans eq 'x')then stop
end

;print,'after oPixon OBJ_NEW'
;help,/mem

;pxwrapper.sr=sr
;pxwrapper.sc=sc
;us2={userstruct:userstruct}
;pxwrapper = create_struct(pxwrapper,us2) ; appends to existing pxwrapper
;  now we use a better way
;pxwrapper=addrep_struct(pxwrapper,'userstruct',userstruct)
; now store as array of pointers, not array of structures
; WE NO LONGER USE THIS.  ALSO, USING THIS CALL UNDEFINES
; THE POINTERS IN 'user' AND RENDERS THE PIXON OBJECT DOA!
;pxwrapper=addrep_struct(pxwrapper,'userstruct',user)

return
end

