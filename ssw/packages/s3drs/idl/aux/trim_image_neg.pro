;+
;
; Project   : s3drs and Pixon reconstruction software
;                   
; Name      : trim_image_neg
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: (part of the testing and legacy code, documented in code)
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 

; Keywords  :
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : (see CVS logs)
;
; Written     : Sandy Antunes, NRC, 2007-2009
;               
;-            
; simple routine floors a given image to zero and report
; how many pixels were adjusted.  Used best to remove
; the few negative 'stray' pixels. Can also set all
; below 0 values to the minimum of the non-zero values
; (useful when setting noises).  Returns an altered set
; of the input data, does not change the input data.
;

FUNCTION trim_image_neg,data,nonzero=nonzero

  nonzero=KEYWORD_SET(nonzero)

  data2=data
  ie=where(data < 0)
  if (ie[0] ne -1) then data2[ie]=0.0
  print,'Adjusted ',n_elements(ie),' negative pixels'

  if (nonzero) then begin
      ie=where(data le 0.0)
      ilow=where(data > 0.0)
      data2[ie]=min(data[ilow])
  end

  return,data2

END
