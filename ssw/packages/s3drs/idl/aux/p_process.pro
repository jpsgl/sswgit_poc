;+
;
; Project   : s3drs and Pixon reconstruction software
;                   
; Name      : p_process
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: (part of the testing and legacy code, not documented herein)
;               
; Use       : Just pseudocode here!
;    
; Inputs    : 
;               
; Outputs   : 

; Keywords  :
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : (see CVS logs)
;
; Written     : Sandy Antunes, NRC, 2007-2009
;               
;-            
I) Before we begin, we need:

1) Desired Ncube (== Ndata, for now)                           Ncube

II) Pre-processing needed:

1) Select data files, also gives us number of files Nd         Nd
                                                               datafiles[Nd]

2) Generate their noise and save as FITS
      model=3; 1-3 for level 0.5 data, 4-5 for level 1 data
      for i=0,Nd-1 do p_noisewrapper,model,datafiles[i],/savefits
      noisefiles=datafiles
      for i=0,Nd-1 do strput,noisefiles[i],'N',17


3) Crop and save the data and resulting POV centers            HAEcenter[3]
                                                               crops[2,Nd]
      scale=2; our default is cutting it to half size
      cropfiles=datafiles
      cachefile='thisrunname'
      scc_measure,datafiles[0],datafiles[1],outfile=cachefile,$
        /forcesave,/crop,scaling=[scale,scale]

      ; note we assume in this example there are 2 files, A & B (or B & A)
      data=read_ascii(cachefile)
      if (size(data.field1,/n_dimensions) eq 1) then myrow=data.field1 else $
        myrow=(data.field1)[*,(size(data.field1))[2]-1]
      HAEcenter=myrow[0:2]
      crops=fltarr(4,Nd)
      crops[0:1,0]=myrow[3:4]
      crops[0:1,1]=myrow[5:6]
      crops[2:3,0]=[scale,scale];
      crops[2:3,1]=[scale,scale];

      for i=0,Nd-1 do scc_crop,datafiles[i],crops[0:1,i]
      for i=0,Nd-1 do strput,cropfiles[i],'R',17

III) Pixon

1) Ingest metadata from above, asking for best range Pixon data to be 0-1000
     prep_3drv,pxwrapper,views=Nd,N=Ncube,autorange=1000
     pxwrapper.sr.lla0=p_sunvoxel(HAEcenter,pxwrapper.sr.dvox/pxwrapper.sr.L0tau[m],pxwrapper.sr.Ncube)

2) Ingest FITS (masks are automatically created and binning is handled)
   (and determine best normalization)

    datum=ptrarr(nfiles)
    masks=ptrarr(nfiles)
    noise=ptrarr(nfiles)
    for iele=0,Nd-1 p_ingest_fits,cropfiles[iele],datum,srjr,masks,$
        pxwrapper=pxwrapper,iele=iele,N=Ncube,/pointer,sigma=sigma
    for iele=0,Nd-1 do noise[iele]=p_cropscale(noisefiles[i],$
        center=crops[0:1,iele],cropfactor=crops[2:3,iele],N=Ncube,/pointer)

3) Generate Pixon object using actual data values for geometry
     p_generate_pixon,'classic',pxwrapper,oPixon

4) Insert data
    p_insert_data,pxwrapper,datum,oPixon,sigma=noise,mask=masks,/autoscale

5) Run Pixon!
