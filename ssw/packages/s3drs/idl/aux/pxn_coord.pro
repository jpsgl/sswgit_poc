;+
;
; Project   : s3drs and Pixon reconstruction software
;                   
; Name      : pxn_coord
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: (part of the testing and legacy code, documented in code)
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 

; Keywords  :
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : (see CVS logs)
;
; Written     : Sandy Antunes, NRC, 2007-2009
;               
;-            
;copy of /net/mercury/data1/pixxon/oopixon_11.3/util/coord.pro
; renamed to pxn_coord to avoid name clashes in environment
; $Id: pxn_coord.pro,v 1.1 2009/04/22 18:31:42 antunes Exp $
;+
; <p> This function returns an array containing the coordinates of all the grid
;    points in an array.
;
; @copyright
;    Copyright � Pixon LLC, 1999-2005.  All rights reserved.
;    Unauthorized reproduction prohibited.
; @author
;    <a href="mailto:Amos.Yahil\@Pixon.com">Amos Yahil, Pixon LLC.</a>
; @param   DIM {in}
;    Dimensions of the array.
; @keyword TYPE {in}{out}
;    Type of the output [DEFAULT=3 (long)].
;-
FUNCTION pxn_coord, dim, TYPE=type
                                ; Compilation options
   COMPILE_OPT IDL2, HIDDEN, STRICTARRSUBS
                                ; Initialization
   IF N_ELEMENTS(type) EQ 0 THEN		type = 3
                                ; Derived parameters
   ndim = N_ELEMENTS(dim)
                                ; Trivial 1-D case
   IF ndim EQ 1 THEN BEGIN
      out = MAKE_ARRAY(dim=dim, type=type, /INDEX)
                                ; General N-D case
   ENDIF ELSE BEGIN
      cnpxl = NpixelC(dim)
      npxl = cnpxl[ndim-1]
      out = MAKE_ARRAY(dim=[ndim,npxl], type=type, /NOZERO)
                                ; Loop on dimensions
      out[0,*] = Spread(LINDGEN(dim[0]), 2, npxl/dim[0])
      FOR n=1,ndim-2 DO BEGIN
         out[n,*] = Spread(Spread(LINDGEN(dim[n]), 1, cnpxl[n-1]), 3, $
                           npxl/cnpxl[n])
      ENDFOR
      out[ndim-1,*] = Spread(LINDGEN(dim[ndim-1]), 1, cnpxl[ndim-2])
   ENDELSE
                                ; Done
   RETURN, out
END
