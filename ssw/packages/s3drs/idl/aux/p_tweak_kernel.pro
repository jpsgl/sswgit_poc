;+
;
; Project   : s3drs and Pixon reconstruction software
;                   
; Name      : p_tweak_kernel
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: (part of the testing and legacy code, documented in code)
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 

; Keywords  :
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : (see CVS logs)
;
; Written     : Sandy Antunes, NRC, 2007-2009
;               
;-            
; applies appropriate numerics to modify number of kernels

function p_tweak_kernel,sp,noct,npo,debug=debug

  debug=KEYWORD_SET(debug)

  sp.noct=long(noct)
  sp.npo=long(npo)
  sp.npxn=long(noct*npo+1)
  if (debug) then print,'noct * npo + 1 = npxn ',sp.noct,sp.npo,sp.npxn

  return,sp

end
