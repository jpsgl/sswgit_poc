;+
;
; Project   : s3drs and Pixon reconstruction software
;                   
; Name      : ffunc_03b
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: (part of the testing and legacy code, not documented herein)
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 

; Keywords  :
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : (see CVS logs)
;
; Written     : Sandy Antunes, NRC, 2007-2009
;               
;-            
; just a wrapper in case legacy code calls this version

pro ffunc_03b,sr,s,ps,mu,nu,i,jc,K2,u,x,y,rhos2

  ffunc,sr,s,ps,mu,nu,i,jc,K2,u,x,y,rhos2,c2z,s2z

end
