;+
;
; Project   : s3drs and Pixon reconstruction software
;                   
; Name      : tpxn_render and related OO code
;               
; Purpose   : part of the s3drs GUI
;               
; Explanation: (part of the testing and legacy code, documented in code)
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 

; Keywords  :
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : (see CVS logs)
;
; Written     : Sandy Antunes, NRC, 2007-2009
;               
;-            
; $Id: tpxn_render.pro,v 1.1 2009/04/22 18:31:48 antunes Exp $
;+
; <p> The first sentence summarizes the documentation.
;
; <p> It is followed by a more detailed description.  Paragraphs should be
;    hung-indented as in this example.
;
; <p> Modify the author tag as appropriate.  Other tags follow the author tag.
;    See IDLDOC_FILES.HTML for a complete list of tags.
;
; @copyright
;    Copyright � Pixon LLC, 1999-2004.  All rights reserved.
;    Unauthorized reproduction prohibited.
; @author
;    <a href="mailto:Amos.Yahil\@Pixon.com">Amos Yahil, Pixon LLC.</a>
;-

PRO TPXNproj::Jpol

; Compilation options

;   COMPILE_OPT IDL2, HIDDEN, STRICTARRSUBS
;print,'***************************** using Paul renderer'
; Initialization

   self -> Free, /JPOL
   self.jpol = PTR_NEW(PTRARR(self.npol), /NO_COPY)
   self.oImage -> Get, x, /VRTX
   self.oImage -> TPXNdim::Get, nx, /NVRTX

   ; if polarization coeffs exist, use proper flimb rendering,
   ; otherwise use default tpixon line of sight rendering

   if (ptr_valid(self.user)) then begin

;       print,'***************************** with flimb'
; Calculate normalized flux

       ch = 2                ; polynomial limb darkening, extended sun
       rr = *self.user ; solar radius for arnaud's problem in image units
       d  = 1.0 ; multiplication factor (I think this is obsolete ???)
       x0 = [0,0,0]             ; position of sun in image units
       t_flimb,ch,(*x),rr,d,x0,fa,fb,fc,ImI0 ; get polarization coefficients fa,fb,fc and
                                ; ratio of mean to central solar intensity
       
       r  = float(*x)           ; sun-vertex vector
       r2 = total(r*r,1)        ; sun-vertex distance ^2

       q=where(r2 le rr*rr,count) ; fix r2 for r le rr
       if(count gt 0)then r2[q]=rr*rr
       
       rs  = Spread(FLOAT(*self.pos),2,nx)-r ; satellite-vertex vector
       rs2 = total(rs*rs,1)     ; satellite-vertex distance ^2
       cs2 = total(r*rs,1)/(r2*rs2) ; cosine ^2 of sun-vertex-satellite angle
       
       IF self.npol GT 1 THEN BEGIN
           (*self.jpol)[0] = PTR_NEW(fa) ; radial flux
           (*self.jpol)[1] = PTR_NEW(fb*cs2+fc) ; tangential flux
       ENDIF ELSE BEGIN
;     (*self.jpol)[0] = PTR_NEW(fa+fb*cs2+fc)	; total brightness
           (*self.jpol)[0] = PTR_NEW(fa*0+1) ; simple density sum 
       ENDELSE
       
   end else begin

       ; copy of original tpnxproj::jpol routine
;       print,'***************************** without flimb'

       CASE self.npol OF
           1: (*self.jpol)[0] = PTR_NEW(1.0)
           2: BEGIN
               pmx = Spread(FLOAT(*self.pos), 2, nx) - (*x)
               (*self.jpol)[0] = PTR_NEW(TOTAL((*x)*pmx, 1)^2 $
                                         /(TOTAL((*x)^2, 1)*TOTAL(pmx^2, 1) $
                                           + Epsilon(1)), /NO_COPY)
               (*self.jpol)[1] = PTR_NEW(1.0 - *(*self.jpol)[0])
           END
       ENDCASE

   end

;stop,'stop in tpxnproj__jpol_01.pro'

; Done

;f0=*((*self.jpol)[0])   ; normed flux: debug
;f1=*((*self.jpol)[1])   ; normed flux: debug
;stop,'jpol_01 stop'     ; debug

END

PRO tpxn_render
; force compilation of above new tpsnproj__jpol
END
