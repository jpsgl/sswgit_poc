;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : widgetexit
;               
; Purpose   : widgets, useful in general
;               
; Explanation: simple call to a widget button to nix the given widget window

;               
; Use       : as widgets
;    
; Inputs    : event
;               
; Outputs   : none
;
; Keywords  : none
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: changes GUI state
;               
; Category    : GUI, widget
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

PRO widgetexit,myevent
;   print,'killing ',myevent,myevent.top
   WIDGET_CONTROL,myevent.top,/destroy
END
