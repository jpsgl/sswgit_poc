;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : widget_date, widget_date_fetch
;               
; Purpose   : widgets, useful in general
;               
; Explanation: handy widget for getting user-input date in fixed format
; pass either top+uname or actual widget id to this fetch
; returns a 5-element array: YYYY,MM,DD,YYYY,HH,MM
; usage: make it with:
;    bwid = widget_date(toplevel,'tryme'); give it toplevel and a UNAME
; get 5-element result with:
;    print,widget_date_fetch(toplevel,'tryme'); find it by toplevel and UNAME
;               
; Use       : as widgets
;    
; Inputs    : (various)
;               
; Outputs   : none
;
; Keywords  : none
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: changes GUI state
;               
; Category    : GUI, widget
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; usage: make it with:
;    bwid = widget_date(toplevel,'tryme'); give it toplevel and a UNAME
; get 5-element result with:
;    print,widget_date_fetch(toplevel,'tryme'); find it by toplevel and UNAME


; pass either top+uname or actual widget id to this fetch
; returns a 5-element array: YYYY,MM,DD,YYYY,HH,MM

FUNCTION widget_date_fetch,wID,uname

  if (n_elements(uname) ne 0) then begin
      ; find by uname
;      print,'hunting up widget id from parent ',wid,' and name ',uname
      dID = WIDGET_INFO(wID,FIND_BY_UNAME=uname)
      if (dID eq 0) then return,0; no result
  end else begin
      dID=wID
;      print,'hunting up uname from widget ',dID
      uname=WIDGET_INFO(wID,/UNAME)
  end

  tid = WIDGET_INFO(dID,FIND_BY_UNAME=uname+'yyyy')
  WIDGET_CONTROL,tid,GET_VALUE=yyyy

  tid = WIDGET_INFO(dID,FIND_BY_UNAME=uname+'mon')
  mon=WIDGET_INFO(tid,/DROPLIST_SELECT)
  mon=mon+1; convert from base 0

  ; get items from pieces of the compound widget
  tid = WIDGET_INFO(dID,FIND_BY_UNAME=uname+'dd')
  dd=WIDGET_INFO(tid,/DROPLIST_SELECT)
  dd=dd+1; convert from base 0

  tid = WIDGET_INFO(dID,FIND_BY_UNAME=uname+'hh')
  hh=WIDGET_INFO(tid,/DROPLIST_SELECT)

  tid = WIDGET_INFO(dID,FIND_BY_UNAME=uname+'mm')
  mm=WIDGET_INFO(tid,/DROPLIST_SELECT)

  return,[yyyy,mon,dd,hh,mm]

END

; creates a data combo widget

FUNCTION widget_date,wFrame,uname,title=title,$
                     offset=offset,startdate=startdate,$
                     EVENT_FUNC=event_func,evens=evens

  if (n_elements(uname) eq 0) then uname='date'
  if (n_elements(title) eq 0) then title='Date:'
  if (n_elements(offset) eq 0) then offset=0; start date offset from'now'
  if (n_elements(startdate) eq 0) then $
    caldat,systime(/julian)-offset,mmm,dd,yyyy,hh,mm else $
    caldat,startdate-offset,mmm,dd,yyyy,hh,mm
  evens=KEYWORD_SET(evens)

  hh=0; always start at 0:00
  mm=0

  if (n_elements(event_func) ne 0) then $
    wF = WIDGET_BASE(wFrame,ROW=1,UNAME=uname,EVENT_FUNC=event_func) else $
    wF = WIDGET_BASE(wFrame,ROW=1,UNAME=uname)

  label = WIDGET_LABEL(wF,VALUE=title)

  wyear = CW_FIELD(wF,/INTEGER,VALUE=yyyy,TITLE='',XSIZE=4,UNAME=uname+'yyyy')

  label = WIDGET_LABEL(wF,VALUE='/')

  mon=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
  wmmm = WIDGET_DROPLIST(wF,TITLE='',VALUE=mon,UNAME=uname+'mon')
  WIDGET_CONTROL,wmmm,SET_DROPLIST_SELECT=mmm-1

  label = WIDGET_LABEL(wF,VALUE='/')

  wday = WIDGET_DROPLIST(wF,TITLE='',UNAME=uname+'dd',$
                         VALUE=int2str(make_array(/long,/index,31)+1,2))
  WIDGET_CONTROL,wday,SET_DROPLIST_SELECT=dd

  label = WIDGET_LABEL(wF,VALUE=' ')

  hour24=int2str(make_array(/long,/index,24),2)
  whour = WIDGET_DROPLIST(wF,TITLE='',UNAME=uname+'hh',$
                         VALUE=int2str(make_array(/long,/index,24),2))
  WIDGET_CONTROL,whour,SET_DROPLIST_SELECT=hh

  label = WIDGET_LABEL(wF,VALUE=':')

  if (evens) then mins=make_array(/long,/index,30)*2 else $
    mins=make_array(/long,/index,60)
  wmin = WIDGET_DROPLIST(wF,TITLE='',UNAME=uname+'mm',$
                        VALUE=int2str(mins,2))
  WIDGET_CONTROL,wmin,SET_DROPLIST_SELECT=mm

  return,wF

END
