;+
; NAME:
;	yes_no_menu
;
; PURPOSE:
;	Provide a simple way to ask the interactive use a Yes/No question.
;	A yes/no menu is created and widget/menu waits for mouse selection.
; CALLING:
;	answer = yes_no_menu( question )
;
; INPUTS:
;	question = string, the question to be answer with yes/no,
;		note that question mark "?" is automatically tacked on the end.
; KEYWORDS:
;	/BINARY	: causes result of function to be binary integer, 0=no, 1=yes.
;		(default is string result).
;
;	/NO_DEFAULT : causes mouse to be positioned over the NO selection.
;		(default is mouse  positioned over YES).
;
; OUTPUTS:	Function returns string YES or NO, or if /BINARY
;		it returns an integer 1 or 0, indicating user selection.
;
; HISTORY:
;	Written: Frank Varosi NASA/GSFC 1990.
;	F.V. 1993, added X widget menu.
;       Antunes, 2009, added /big and optionalquestion= inputs
;-

function yes_no_menu, question, NO_DEFAULT=no_init, BINARY=binary, $
                      TITLE=title, BIG=big, OPTIONALQUESTION=optionalquestion

big=KEYWORD_SET(big)

sz = size( question )
if sz(sz(0)+1) NE 7 then question = ""

if (n_elements(title) eq 0) then title='decisions...'

if ( !D.name EQ "SUN" ) then begin

	menu = [ question + "?", "NO", "YES" ]
	if keyword_set( no_init ) then init=1 else init=2

	sel = wmenu( menu, INIT=init, TIT=0 ) > 1

	if keyword_set( binary ) then  return, sel-1  else  return, menu(sel)

  endif else begin
      
        ysize=n_elements(question)
        if (big) then ysize+=4

        if (big) then $
          base = WIDGET_BASE( TIT=title, XOFF=400, YOFF=400, XSIZE=700, /COLUMN ) else $
          base = WIDGET_BASE( TIT=title, XOFF=400, YOFF=400, /COLUMN )
        if (n_elements(question) le 1) then $
          Label = WIDGET_LABEL( base, VAL=question+"?" ) else $
          Label = WIDGET_TEXT( base, VAL=question,ysize=ysize+2,/WRAP)

	bbase = WIDGET_BASE( base, /ROW, SPACE=20, XPAD=20, YPAD=20, /FRAME )

	bwid = Lonarr(2)
	bval = [ "NO", "YES" ]

        if (n_elements(optionalquestion) ne 0) then $
          ignore=WIDGET_LABEL(bbase, VAL=optionalquestion)

	for i=0,1 do bwid(i) = WIDGET_BUTTON( bbase, VAL=bval(i), UVAL=i )
	WIDGET_CONTROL, base,/REALIZE

	if keyword_set( no_init ) then WIDGET_CONTROL, bwid(0),/INPUT_FOCUS $
				  else WIDGET_CONTROL, bwid(1),/INPUT_FOCUS
	event = WIDGET_EVENT( base )

	if keyword_set( binary ) then begin
		WIDGET_CONTROL, event.id, GET_UVAL=answer
	  endif else begin
		WIDGET_CONTROL, event.id, GET_VAL=answer
	   endelse

	WIDGET_CONTROL, base, /DESTROY
	return, answer

   endelse
end
