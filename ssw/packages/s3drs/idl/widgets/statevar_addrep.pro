;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : statevar_addrep, statevar_delete, statevar_init
;               
; Purpose   : widgets, useful in general
;               
; Explanation: ; routines for generically handling state variable structures
; as if they were global (but safely reentrant)
; Note they can store scalars, pointers, and structures but not arrays
;               
; Use       : as widgets
;    
; Inputs    : (various)
;               
; Outputs   : none
;
; Keywords  : none
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: changes GUI state, stored statevar structure
;               
; Category    : GUI, widget
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; routines for generically handling state variable structures
; as if they were global (but safely reentrant)
; Note they can store scalars, pointers, and structures but not arrays
;
; Use 'statevar_init,basename' to initialize the structure pointer
;           into the given 'basename' widget
; Use 'statevar_addrep' to insert/replace items you wish to store
; Use 'statevar_fetch' to retrieve stored items
;
; This is not the most efficient way to store large data items,
; as adding or replacing requires the entire structure (of all stored
; items) be freed then restored, but this should be fast enough for
; most cases.
;

PRO statevar_addrep, mytop, var_name, var_value
  ; adds (or replaces, if item exists) the given variable

  WIDGET_CONTROL, mytop, get_uvalue=state_ptr
  state=*state_ptr

;  isarray = (size(var_name,/N_DIM) gt 0)

  iexists = struct_item_exists(state,var_name)
  if (iexists eq -1) then begin
      ; add
      state = create_struct(state, var_name, var_value)
  end else begin
      ; replace
      state.(iexists) = var_value
  end

  statevar_init,mytop,structure=state

END

PRO statevar_delete, mytop, var_name
  ; deletes the given variable, if it exists

  WIDGET_CONTROL, mytop, get_uvalue=state_ptr
  state=*state_ptr

  iexists = struct_item_exists(state,var_name)
  if (iexists eq -1) then begin
      ; delete
      state = rem_tag(state, var_name)
      statevar_init,mytop,structure=state
  end

END


PRO statevar_init,mytop,structure=structure,test=test
   ; initialize the pointer so it is not null and attaches it to the top
   ; calling this routine also ensures statevar handlers are compiled

  WIDGET_CONTROL, mytop, get_uvalue=state_ptr
  if (ptr_valid(state_ptr)) then ptr_free,state_ptr ; be tidy

   if (n_elements(structure) eq 0) then begin
       state={placeholder:0}
       state_ptr=ptr_new(state)
   end else begin
       state_ptr = ptr_new(structure)
   end

   WIDGET_CONTROL, mytop, set_uvalue=state_ptr

END
