;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : centerme
;               
; Purpose   : widgets, useful in general
;               
; Explanation: little utility to center a new widget
; taken from dialog_list.pro, after widget_control,mybase, /REALIZE
; and before xmanager is called.
;               
; Use       : centerme, mybase
;    
; Inputs    : mybase = top level widget to be realized
;               
; Outputs   : none
;
; Keywords  : none
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: recenters widgets
;               
; Category    : GUI, widget
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; little utility to center a new widget
; taken from dialog_list.pro, after widget_control,mybase, /REALIZE
; and before xmanager is called.

PRO centerme, mybase

  thisScreen = GET_SCREEN_SIZE()
  WIDGET_CONTROL, myBase, TLB_GET_SIZE = dialogSize

  dialogPt = [(thisScreen[0] / 2.0) - (dialogSize[0] / 2.0), $ 
              (thisScreen[1] / 2.0) - (dialogSize[1] / 2.0)] 

  WIDGET_CONTROL, myBase, $
    TLB_SET_XOFFSET = dialogPt[0], $
    TLB_SET_YOFFSET = dialogPt[1]
  WIDGET_CONTROL, myBase, MAP = 1

END
