;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : infowidget
;               
; Purpose   : widgets, useful in general
;               
; Explanation: simple widget that displays a screen of info
;               
; Use       : infowidget,textstring,parent=parent,title=title,maxlines=maxlines,keepalive=keepalive,id=id,NO_BLOCK=NO_BLOCK
;    
; Inputs    : textstring = (required) what to say
;               
; Outputs   : id = (optional) its id
;
; Keywords  : none
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: changes GUI state
;               
; Category    : GUI, widget
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; simple widget that displays a screen of info
; optional 'id=' returns its id

PRO closeinfowidget,myevent
   pid=WIDGET_INFO(myevent.id,/PARENT)
   WIDGET_CONTROL,pid,/destroy
END

PRO infowidget,textstring,parent=parent,title=title,maxlines=maxlines,$
               keepalive=keepalive,id=id,NO_BLOCK=NO_BLOCK

  if (n_elements(title) eq 0) then title='Information'
  if (n_elements(parent) eq 0) then parent=-1
  if (n_elements(maxlines) eq 0) then maxlines=-1
  if (n_elements(textstring) eq 0) then textstring='(no information)'
  keepalive = KEYWORD_SET(keepalive)
  NO_BLOCK = KEYWORD_SET(NO_BLOCK)

  if (parent ne -1) then begin
      mybase = WIDGET_BASE(parent,TITLE=title,/COLUMN)
      ; next line supressed flicking
      WIDGET_CONTROL, mybase, /REALIZE, UPDATE=0
  end else begin
      mybase = WIDGET_BASE(TITLE=title,/COLUMN)
  end

  nn='\\n'
  if (n_elements(textstring) eq 1) then begin
      ; if data is one line, split on line breaks
      mytext=strsplit(textstring,nn,/REGEX,/EXTRACT)
  end else begin
      mytext = textstring; it is an array of strings so use as is
  end

  n_items=n_elements(mytext)

  wordframe = WIDGET_BASE(mybase,/ROW)
  localframe = WIDGET_BASE(wordframe,/COLUMN)
  fcount=0
  for i=0,n_items-1 do begin
      fcount=fcount+1
      ignore=WIDGET_LABEL(localframe,VALUE=mytext[i],/ALIGN_LEFT)
      if (maxlines ne -1 and fcount gt maxlines) then begin
          ; make a new frame
          localframe = WIDGET_BASE(wordframe,/COLUMN)
          fcount=0
      end
  end

  if (parent eq -1) then begin
      
      if (keepalive eq 0) then $
        mybutton = WIDGET_BUTTON(mybase,EVENT_PRO='widgetexit',VALUE='OK')

      WIDGET_CONTROL, mybase, /REALIZE

        ; Place the dialog: window manager dependent
      centerme, mybase

      xmanager,'info',mybase,EVENT_HANDLER='ignore',NO_BLOCK=NO_BLOCK

  end else begin

      if (keepalive eq 0) then $
        mybutton = WIDGET_BUTTON(mybase,EVENT_PRO='closeinfowidget',VALUE='OK')

      ; for embedded widgets, this is the other half of anti-flickering
      WIDGET_CONTROL, mybase, /MAP, /UPDATE

      ; note use here of non-xmanager blocking for embedded widgets
;      REPEAT BEGIN
;          event = WIDGET_EVENT (mybase)
;      ENDREP UNTIL (event.id eq mybutton)

  end

  id=mybase; optional return value

END
