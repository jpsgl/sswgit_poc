;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : rfft
;               
; Purpose   : memory tools and Pixon 'syntax sugar' helper routines
;               
; Explanation: computes a real FFT
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 
;
; Keywords  : 
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
;+
; <p> This function computes a real FFT.  It differs from the standard IDL FFT
;    in that it truncates the first dimension beyond the Nyquist frequency to
;    remove redundant frequencies.  This saves both storage and time.
;
; <p> The code utilizes the optional variable DIMENSION, available in the IDL
;    FFT function, starting in version 5.6.
;
; @copyright
;    Copyright � Pixon LLC, 1999-2006.  All rights reserved.
;    Unauthorized reproduction prohibited.
; @author
;    <a href="mailto:Amos.Yahil\@Pixon.com">Amos Yahil, Pixon LLC.</a>
; @param   a {in}
;    2-D array to be FFT'ed.
; @keyword NAN {in}{type=boolean}
;    Convert nonfinite numbers to 0 (missing data). 
; @keyword _REF_EXTRA {in}{out}
;    Extra variables (by reference).
;-
FUNCTION RFFT, a, NAN=NAN, _REF_EXTRA=e
                                ; Compilation options
   COMPILE_OPT IDL2, HIDDEN, STRICTARRSUBS
                                ; Dimensions
   ndim = Ndim(a)
   new = Dim(a)
   new[0] = new[0]/2 + 1
                                ; FFT the first dimension and truncate beyond
                                ; the Nyquist frequency.  If NAN is set, convert
                                ; nonfinite numbers to 0 (missing data).
   IF KEYWORD_SET(nan) THEN BEGIN
      temp = a
      tdx = WHERE(~ FINITE(temp), tcnt)
      IF tcnt NE 0 THEN BEGIN
         temp[tdx] = 0.0
      ENDIF
      out = FFT(TEMPORARY(temp), 1, DIMENSION=1, OVERWRITE=0, _EXTRA=e)
   ENDIF ELSE BEGIN
      out = FFT(a, 1, DIMENSION=1, OVERWRITE=0, _EXTRA=e)
   ENDELSE
   out = SubArray(TEMPORARY(out), 0, new-1)
                                ; FFT the higher dimensions
   FOR n=2,ndim DO BEGIN
      out = FFT(TEMPORARY(out), 1, DIMENSION=n, /OVERWRITE, _EXTRA=e)
   ENDFOR
                                ; Done
DONE:
   RETURN, out
END
