;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : irfft_h
;               
; Purpose   : memory tools and Pixon 'syntax sugar' helper routines
;               
; Explanation: inverse FFT
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 
;
; Keywords  : 
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; $Id: irfft_h.pro,v 1.1 2009/04/22 18:32:32 antunes Exp $
;+
; <p> This function computes an inverse real FFT.  It differs from the standard
;    IDL FFT in that it uses FT arrays that were truncated on the first
;    dimension beyond the Nyquist frequency to remove redundant frequencies.
;    This saves both storage and time.
;
; <p> Set the keyword ODD if the first dimension of the array in configuration
;    space is odd.  Otherwise it is assumed to be even.
;
; <p> The code utilizes the optional variable DIMENSION, available in the IDL
;    FFT function, starting in version 5.6.
;
; @copyright
;    Copyright � Pixon LLC, 1999-2004.  All rights reserved.
;    Unauthorized reproduction prohibited.
; @author
;    <a href="mailto:Amos.Yahil\@Pixon.com">Amos Yahil, Pixon LLC.</a>
; @param   fa {in}
;    2-D Fourier array to be inverse FFT'ed.
; @keyword ODD {in}{type=boolean}
;    Odd first dimension.
; @keyword _REF_EXTRA {in}{out}
;    Extra variables (by reference).
;-
FUNCTION IRFFT, fa, ODD=odd, _REF_EXTRA=e

                                ; Compilation options
   COMPILE_OPT IDL2, HIDDEN, STRICTARRSUBS
                                ; Dimensions
   dim = Dim(fa)
   ndim = Ndim(fa)

   print,'using slower shared memory irfft now'

                                ; Inverse FFT the higher dimensions, if any
   shmmap,size=SIZE(fa),get_name=segname
   out=shmvar(segname)
;   out = fa
   FOR n=2,ndim DO BEGIN
      shmmap,size=SIZE(fa),get_name=segname
      nout=shmvar(segname)
      nout = FFT(out, -1, dimension=n, /OVERWRITE, _EXTRA=e)
      out=nout
   ENDFOR
                                ; Add the truncated frequencies to the first
                                ; dimension.  Note the different procedures for
                                ; even and odd arrays in configuration space,
                                ; determined in the next line of code.
   n = dim[0] - 2 + KEYWORD_SET(odd)
   IF n GT 0 THEN BEGIN
      temp = CONJ(out[n-LINDGEN(n),*,*,*,*,*,*,*])
      shmmap,size=SIZE(temp),get_name=segname
      temp2=shmvar(segname)
      nout=out
      out = [nout, temp2]
   ENDIF
                                ; Inverse FFT the first dimension and take the
                                ; real part
   nout=out
   out = FFT(nout, -1, dimension=1, /OVERWRITE, _EXTRA=e)
   nout=out
   out = REAL_PART(nout)
                                ; Done
DONE:
   RETURN, out
END
