;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : iscomplex
;               
; Purpose   : memory tools and Pixon 'syntax sugar' helper routines
;               
; Explanation: tests if something is complex or not
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 
;
; Keywords  : 
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
;+
;
; @copyright
;    Copyright � Pixon LLC, 1999-2006.  All rights reserved.
;    Unauthorized reproduction prohibited.
; @author
;    <a href="mailto:Amos.Yahil\@Pixon.com">Amos Yahil, Pixon LLC.</a>
; @param   a 
;    Variable whose complexity is sought.
;-
FUNCTION IsComplex, a
                                ; Compilation options
   COMPILE_OPT IDL2, HIDDEN, STRICTARRSUBS
                                ; Test for complexity
   IF Type(a) EQ 6 OR Type(a) EQ 9 THEN BEGIN
      out = 1B
   ENDIF ELSE BEGIN
      out = 0B
   ENDELSE
                                ; Done
   RETURN, out
END
