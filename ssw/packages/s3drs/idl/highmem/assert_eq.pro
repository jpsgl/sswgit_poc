;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : assert_eq
;               
; Purpose   : memory tools and Pixon 'syntax sugar' helper routines
;               
; Explanation: 
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 
;
; Keywords  : 
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
;+
; <p> This function verifies that all the components of NVEC are equal and
;    returns that value.  (Based on Numerical Recipes)
;
; @copyright
;    Copyright � Pixon LLC, 1999-2006.  All rights reserved.
;    Unauthorized reproduction prohibited.
; @author
;    <a href="mailto:Amos.Yahil\@Pixon.com">Amos Yahil, Pixon LLC.</a>
; @param   nvec {in}
;    Vector whose components are tested.
; @param   msg {in}
;    Error message.
;-
FUNCTION Assert_EQ, nvec, msg

                                ; Compilation options
   COMPILE_OPT IDL2, HIDDEN, STRICTARRSUBS
                                ; Error handling
   ON_ERROR, 2
                                ; Check equality of vector components
   out = nvec[0]
   IF ~ ARRAY_EQUAL(out, nvec) THEN BEGIN
      void = DIALOG_MESSAGE(msg, /ERROR)
   END
                                ; Done
   RETURN, out
END
