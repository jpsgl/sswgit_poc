;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : ndqf_h
;               
; Purpose   : memory tools and Pixon 'syntax sugar' helper routines
;               
; Explanation: 
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 
;
; Keywords  : 
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; $Id: ndqf_h.pro,v 1.1 2009/04/22 18:32:34 antunes Exp $
;+
; <p> This function computes the quadratic form R_{IJ}.A.R_{IJ} for all points I
;    on a grid, where R_{IJ} is the distance to a center point J.
;
; <p> The optional variable CENTER defaults to the center of the array.
;
; <p> The optional variable A allows for arbitrary quadratic forms.  It can be a
;    scalar, simply multiplying the distance squared, a vector with each
;    component scaling differently (e.g. for uneven pixel sizes), or a
;    fully-fledged tensor.
;
; <p> The default A is a unit scalar, i.e., the function measures distance
;    squared from CENTER.
;
; @copyright
;    Copyright � Pixon LLC, 1999-2004.  All rights reserved.
;    Unauthorized reproduction prohibited.
; @author
;    <a href="mailto:Amos.Yahil\@Pixon.com">Amos Yahil, Pixon LLC.</a>
; @param   dim {in}
;    Dimensions of the array.
; @keyword A {in}
;    Matrix of the quadratic form.
; @keyword CENTER {in}{out}
;    Center position.
;-
FUNCTION NDQF, dim, A=a, CENTER=center

                                ; Compilation options
   COMPILE_OPT IDL2, HIDDEN, STRICTARRSUBS
                                ; Initialization & defaults
   IF N_ELEMENTS(center) EQ 0 THEN center = [dim]/2
   ndim = Assert_EQ([N_ELEMENTS(dim),N_ELEMENTS(center)], $
                    'NDQF: DIM and CENTER have inconsistent dimensionality')
   prod = NPixel(dim)
   sz = SIZE(a)
                                ; Vector coordinates relative to CENTER
   c = Coord(dim) - Spread(center, 2, prod)
                                ; 1-D case
   s1=SIZE(c)
;   print,'size check,',s1
   shmmap,size=s1,get_name=segname
   out=shmvar(segname)
   shmmap,size=s1,get_name=segname
   tout=shmvar(segname)
   print,'using ndqf slower shared memory format',s1,dim

   IF ndim EQ 1 THEN BEGIN


      out = a[0]*c^2
                                ; Scalar/vector quadratic form with no
                                ; off-diagonal terms
   ENDIF ELSE IF sz[0] LE 1 THEN BEGIN

      out = c^2
      tout=out
      CASE N_ELEMENTS(a) OF
         0: out = TOTAL(tout, 1)
         1: out = a[0]*TOTAL(tout, 1)
         ELSE: BEGIN
            ndim = Assert_EQ([ndim,Dim(a)], 'NDQF: DIM and A have' + $
                             ' inconsistent dimensionality')
            tout=out
            out = TOTAL(Spread(a, 2, prod)*tout, 1)
         END
      ENDCASE
                                ; Tensor quadratic form with off-diagonal terms
   ENDIF ELSE IF sz[0] EQ 2 THEN BEGIN
      ndim = Assert_EQ([ndim,Dim(a)], 'NDQF: DIM and A have' + $
                       ' inconsistent dimensionality')


      out = c*(a # c)
      tout=out
      out = TOTAL(tout, 1)
   ENDIF ELSE BEGIN
      MESSAGE, /INFO, 'A must be a scalar, vector, or 2-D tensor'
      MESSAGE, ''
   ENDELSE
                                ; Reformat
   tout=out
   out = REFORM([tout], dim, /OVERWRITE)
                                ; Done
   RETURN, out
END
