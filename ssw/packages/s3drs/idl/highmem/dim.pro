;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : dim
;               
; Purpose   : memory tools and Pixon 'syntax sugar' helper routines
;               
; Explanation: wrapper for size(/n_dimensions)
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 
;
; Keywords  : 
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
;+
; <p> This function returns the dimensions of the input array A.
;
; @copyright
;    Copyright � Pixon LLC, 1999-2006.  All rights reserved.
;    Unauthorized reproduction prohibited.
; @author
;    <a href="mailto:Amos.Yahil\@Pixon.com">Amos Yahil, Pixon LLC.</a>
; @param   a {in}
;    Array whose dimensions are sought.
;-
FUNCTION Dim, a

                                ; Compilation options
   COMPILE_OPT IDL2, HIDDEN, STRICTARRSUBS
                                ; Dimensions
   out = SIZE(a, /DIMENSIONS)
                                ; Done
   RETURN, out
END
