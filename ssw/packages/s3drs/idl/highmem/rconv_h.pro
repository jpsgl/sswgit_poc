;+
;
; Project   : s3drs reconstruction software
;                   
; Name      : rconv_h
;               
; Purpose   : memory tools and Pixon 'syntax sugar' helper routines
;               
; Explanation: convolutes two arrays with FFT
;               
; Use       : 
;    
; Inputs    : 
;               
; Outputs   : 
;
; Keywords  : 
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; $Id: rconv_h.pro,v 1.1 2009/04/22 18:32:35 antunes Exp $
;+
; <p> This function computes the convolution of two arrays using RFFT.
;
; <p> The convolving array can be integer or real, in which case its RFFT is
;    computed, or complex in which case it is assumed to be the Fourier
;    transform.
;
; <p> Both single and double precision are allowed.
;
; <p> If the keyword CONJ is set, A (or its RFFT transform) is complex
;    conjugated, and RCONV is called recursively.
;
; <p> Additional keywords can be passed to the RFFT routine with the _EXTRA
;    construct.
;
; <p> Note that we are multiplying the convolution products with b first.  This
;    is done to ensure that the result has the same dimensionality as b, while
;    a can have any dimensionality, as long as its total size equals that of b.
;
; @copyright
;    Copyright � Pixon LLC, 1999-2004.  All rights reserved.
;    Unauthorized reproduction prohibited.
; @author
;    <a href="mailto:Amos.Yahil\@Pixon.com">Amos Yahil, Pixon LLC.</a>
; @param   a {in}
;    First array of the convolution.
; @param   b {in}
;    Second array of the convolution.
; @keyword CONJ {in}{type=boolean}
;    Conjugate A before convolving.
; @keyword _REF_EXTRA {in}{out}
;    Extra variables (by reference).
;-
FUNCTION RConv, a, b, CONJ=conj, _REF_EXTRA=e

                                ; Compilation options
   COMPILE_OPT IDL2, HIDDEN, STRICTARRSUBS
                                ; Complex A

   IF IsComplex(a) THEN BEGIN

       shmmap,size=SIZE(a),get_name=segname
       out=shmvar(segname)


                                ; Complex conjugate & call RCONV recursively
      IF KEYWORD_SET(conj) THEN BEGIN
         out = RCONV(CONJ(a), b, _EXTRA=e)
;          print,'debug, fork A of rconv,',SIZE(out),SIZE(a),SIZE(b)
         GOTO, DONE
                                ; Product of RFFT transforms
      ENDIF ELSE BEGIN
         IF IsComplex(b) THEN BEGIN
            out = b*a
;            print,'debug, fork B of rconv,',SIZE(out),SIZE(a),SIZE(b)
         ENDIF ELSE BEGIN
            out = RFFT(b, _EXTRA=e)*a
;            print,'debug, fork C of rconv,',SIZE(out),SIZE(a),SIZE(b)
         ENDELSE
      ENDELSE
                                ; Real A
   ENDIF ELSE BEGIN
      fa = RFFT(a, _EXTRA=e)

;      print,'fork D of using slower shared memory version of rconv.pro',SIZE(fa)
      shmmap,size=SIZE(fa),get_name=segname
      tfa=shmvar(segname)
      shmmap,size=SIZE(fa),get_name=segname
      out=shmvar(segname)
      tfa=fa
                                ; Complex conjugate & call RCONV recursively
      IF KEYWORD_SET(conj) THEN BEGIN
         out = CONJ(tfa)
         tfa=out
         out = RCONV(tfa, b, _EXTRA=e)
         GOTO, DONE
                                ; Product of RFFT transforms
      ENDIF ELSE BEGIN

         tfa=fa
         IF IsComplex(b) THEN BEGIN
            out = b*tfa
         ENDIF ELSE BEGIN
            out = RFFT(b, _EXTRA=e)*tfa
         ENDELSE
      ENDELSE
   ENDELSE
                                ; IRFFT the product RFFT transforms

   print,'end of using slower shared memory version of rconv.pro'
   shmmap,size=SIZE(out),get_name=segname
   nout=shmvar(segname)
   out = IRFFT(nout, _EXTRA=e)
                                ; Done
DONE:
   RETURN, out
END
