;+
;
; Project   : Densitycube, reconstruction, 3D, and s3drs software
;                   
; Name      : readcloudcube
;               
; Purpose   : used to manipulate 3D densitycubes and do 3D reconstructions
;               
; Explanation: reads in an Arnaud-generated 3D cube file
;               
; Use       : standalone or via s3drs GUI
;    
; Inputs    : modelid
;               
; Outputs   : returns the 3D cube
;
; Keywords  : 
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 3D
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
;               
; $Log: readcloudcube.pro,v $
; Revision 1.1  2009/04/22 18:32:56  antunes
; first half of relocation.
;
; Revision 1.1  2009/03/31 15:46:03  antunes
; More reorg
;
; Revision 1.2  2008/04/04 17:27:07  antunes
; Added a forward modeling routine.
;
; Revision 1.1  2006/04/11 15:54:48  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.4  2006/03/16 14:44:13  antunes
; Axes improvements and better handling of model params.
;
; Revision 1.3  2006/02/28 19:48:16  antunes
; Lots of code, some buggy-- interim checkin.
;
; Revision 1.2  2006/01/09 17:14:42  antunes
; More commented added.
; New 'super3drv' driver routine for getting user specs for reconstruction
; process now checked in.  Just type 'super3drv' to test.
;
;-            
; readcloud, modified from cloud.pro to load the cube format
; cloud into an array rather than reformating and calling dragger
;+
; $Id: readcloudcube.pro,v 1.1 2009/04/22 18:32:56 antunes Exp $
;-


FUNCTION readcloudcube,modelid

;  ofile = mypath+ "cube" + strtrim(string(modelid),2) + ".txt"
  ofile1 = "cube" + strtrim(string(modelid),2) + ".txt"
  ofile2 = "output" + strtrim(string(modelid),2) + ".txt"

  if (file_exist(ofile1)) then ofile=ofile1 else $
    if (file_exist(ofile2)) then ofile=ofile2 else $
    stop,'No file exist for model, aborting'

  openr,lun,ofile,ERROR=err, /GET_LUN

; ---- skip first line
  readf,lun,modelidfile,cubesidenbpix,cubesidersun

  if (err ne 0) then begin
      print,'Ne density file does not exist, exiting'
      stop
  end

  N=cubesidenbpix

  i=double(0)
  timage=fltarr(N,N,N)

  i= double(0)
  while NOT EOF(lun) DO BEGIN
;      readf,lun,yt,xt,zt,bt
      readf,lun,xt,yt,zt,bt
;      if (bt gt 0) then print,xt,yt,zt,bt
      timage[long(xt),long(yt),long(zt)]=bt
  end

  close,lun, /ALL

  return,timage
END
