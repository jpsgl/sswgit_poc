;+
;
; Project   : Densitycube, reconstruction, 3D, and s3drs software
;                   
; Name      : densitycube
;               
; Purpose   : used to manipulate 3D densitycubes and do 3D reconstructions
;               
; Explanation: Saves or loads a FITS density cube, defines that FITS standard
;               
; Use       : standalone or via s3drs GUI
;    
; Inputs    : action, fname, cubedata
;             (optional) cubeinfo
;             (optional) desc, author, program, date, suncenter
;               
; Outputs   : either populates 'cubedata' (if given 'load') or saves
; 'cubedata' into a FITS file (if given 'save')
;
; Keywords  : 
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: updates cubeinfo
;               
; Category    : 3d, density, cube
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; densitycube,[args]
; Saves or reads a FITS Density Cube file and returns the
; information/header structure as well as the actual cube data.
; Inputs:
;    action = 'load' or 'save' (or 'init' to compile without acting)
;    cubeinfo = structure with cube specs in required format
;    cubearr = data array to read in or save from

;  cubestructure,[args]
; Fetches the Density Cube information structure into variables, or
;   packs the variables into the Density Cube information structure
; Inputs:
;    action = 'fetch' or 'pack'
;    cubeinfo = structure with cube specs in required format
;    (various variables) = items to either put into or pull from structure

; test case:
PRO densitycubetest
  cubestructure,testinfo,'pack' ; creates default structure
  testdata=make_array(3,3,3,/float,/index) ; fake data
  densitycube,'save','test.fts',testinfo,testdata ; saves FITS file
  densitycube,'load','test.fts',testinfo2,testdata2 ; reads FITS file
  print,'Original info:'
  help,testinfo,/str ; shows original info
  print,'Restored info:'
  help,testinfo2,/str ; shows restored info
  print,'Original data:'
  print,testdata ; shows original data
  print,'Restored data:'
  print,testdata2 ; shows restored data
  print,'Data difference (should be 0s): '
  print,testdata-testdata2
END


; converts a systime() date to FITS format YYYY-MM-DDTHH:MM:SS
FUNCTION systime2fitsdate,intime
  if (n_elements(intime) eq 0) then intime=systime()
  mmon=  int2str(1+get_month(strmid(intime,4,3)),2)
  dday=  int2str(long(strmid(intime,8,2)),2)
  hhr=   strmid(intime,22)
  yyyy=    strmid(intime,20)
  datestamp=yyyy+'-'+mmon+'-'+dday+'T'+hhr+':00:00'
  return,datestamp
END


;densitycube,'save','cube.fts',im,desc='envelope FM applied to IM',author='Sandy Antunes'

PRO cubestructure,cubeinfo,action,N=N,desc=desc,author=author,$
                  program=program,suncenter=suncenter,obsdate=obsdate,$
                  centers=centers,fov_rsun=fov_rsun,wcstype=wcstype,$
                  cunit=cunit,bunit=bunit

  if (action eq 'pack') then begin

      date=systime2fitsdate()

     ; put in defaults for items not specified
     if (n_elements(N) eq 0) then N=0
     if (n_elements(desc) eq 0) then desc='default'
     if (n_elements(author) eq 0) then author='unknown'
     if (n_elements(program) eq 0) then program='unknown'
     if (n_elements(obsdate) eq 0) then obsdate=date
     if (n_elements(axes) eq 0) then axes=['x','y','z']
     if (n_elements(orient) eq 0) then orient='heliospheric'
     if (n_elements(suncenter) eq 0) then $
       suncenter=[(N-1)/2.0,(N-1)/2.0,(N-1)/2.0]
     if (n_elements(voxelcm) eq 0) then voxelcm=1e10
     if (n_elements(eunits) eq 0) then eunits=1e8
     if (n_elements(centers) eq 0) then centers=[(N-1)/2.0,(N-1)/2.0,(N-1)/2.0]
     if (n_elements(fov_rsun) eq 0) then fov_rsun=30.0
     if (n_elements(cunit) eq 0) then cunit='RSun'
     if (n_elements(bunit) eq 0) then bunit='cm^(-3)';

     ; try and populate likely WCS keys
     if (n_elements(wcstype) ne 0) then begin
         if (strupcase(strmid(wcstype[0],0,4)) eq 'CARR') then begin
             wcstype=[wcstype+'LON',wcstyle+'LAT']
             wcsname='Carrington'
         end else begin
             wcstype=[wcstype+'X',wcstype+'Y',wcstype+'Z']
             wcsname=wcstype; guess
         end
     end else begin
         wcstype=['HAEX','HAEY','HAEZ']
         wcsname='Heliocentric Aries Ecliptic'
     end

     deltas=fov_rsun/N

     cubeinfo = {$
                  bitpix: -32, $
                  naxis: 3, $
                  naxis1: N, $
                  naxis2: N, $
                  naxis3: N, $
                  cdelt1: deltas, $
                  cdelt2: deltas, $
                  cdelt3: deltas, $
                  cunit1: cunit, $
                  cunit2: cunit, $
                  cunit3: cunit, $
                  bunit:  bunit, $
                  crpix1: centers[0], $
                  crpix2: centers[0], $
                  crpix3: centers[0], $
                  crval1: 0, $
                  crval2: 0, $
                  crval3: 0, $
                  ctype1: 'HAEX', $
                  ctype2: 'HAEY', $
                  ctype3: 'HAEZ', $
                  pc1_1: 1.0, $
                  pc1_2: 0.0, $
                  pc1_3: 0.0, $
                  pc2_1: 0.0, $
                  pc2_2: 1.0, $
                  pc2_3: 0.0, $
                  pc3_1: 0.0, $
                  pc3_2: 0.0, $
                  pc3_3: 1.0, $
                  wcsname: wcsname, $
                  N: N, $
                  desc: desc, $
                  author: author, $
                  program: program, $
                  filedate: date, $
                  date_obs: obsdate $
                }

  end else begin

      print,"Please choose action of 'load' or 'save'"

  end

END

; e.g.
;densitycube,'save','cube.fts',im,desc='envelope FM applied to IM',author='Sandy Antunes',program='Pixon',event='Dec 31, 2008 02:22'

PRO densitycube,action,fname,cubedata,cubeinfo,$
                desc=desc,author=author,program=program,date=date,$
                suncenter=suncenter,_EXTRA=extras

  if (action eq 'load') then begin

      cubedata = readfits(fname,cubeheader)
      cubeinfo = fitshead2struct(cubeheader)

  end else if (action eq 'save') then begin

      if (n_elements(cubeinfo) eq 0) then begin
          N=n_elements(cubedata[0,0,*])
          cubestructure,cubeinfo,'pack',N=N,desc=desc,author=author,$
            program=program,date=date,suncenter=suncenter
      end

      if (n_elements(extras) ne 0) then begin
          ; append any extra 'keywords' to the fits header
          cubeinfo=create_struct(cubeinfo,extras)
      end

      cubeheader = struct2fitshead(cubeinfo,/wcs)

      writefits,fname,cubedata,cubeheader

  end else if (action eq 'init') then begin

      ; does nothing, but ensures routine is compiled

  end else begin

      print,"Please choose action of 'load' or 'save'"

  end

END

;1) N: size of cube data array, N by N by N.  If '0' then the user
;    must read the actual data to determine (usually '0' means the
;    user did not specify this and simply used a default header.)
;
;2) Desc: string, a run name/identifier that users can specify/change at
;    will.
;
;3) Author: string, name of person who made this/did this run.
;
;4) Program: string, 'Pixon/'WLRaytrace'/'IDL'/Other, handy to know
;    what program created this (hence underlying assumptions)
;
;5) Date: string, date this was run was created.
;
;6) FITS spec for size and scale
;
;7) WCS units
;
;Plus, the Image: The data cube, int(N,N,N) or float(N,N,N) or double(N,N,N)
;
;cubeinfo.bitpix=-32
;cubeinfo.naxis=3
;cubeinfo.naxis1=N
;cubeinfo.naxis2=N
;cubeinfo.naxis3=N
;cubeinfo.cdelt1=30.0/N
;cubeinfo.cdelt2=30.0/N
;cubeinfo.cdelt3=30.0/N
;cubeinfo.cunit1='Rsun'
;cubeinfo.cunit2='Rsun'
;cubeinfo.cunit3='Rsun'
;cubeinfo.bunit='cm^(-3)'
;cubeinfo.crpix1=(N-1)/2.0
;cubeinfo.crpix2=(N-1)/2.0
;cubeinfo.crpix3=(N-1)/2.0
;cubeinfo.crval1=0
;cubeinfo.crval2=0
;cubeinfo.crval3=0
;cubeinfo.ctype1='HAEX'
;cubeinfo.ctype2='HAEY'
;cubeinfo.ctype3='HAEZ'
;cubeinfo.pc1_1=1.0
;cubeinfo.pc1_2=0.0
;cubeinfo.pc1_3=0.0
;cubeinfo.pc2_1=0.0
;cubeinfo.pc2_2=1.0
;cubeinfo.pc2_3=0.0
;cubeinfo.pc3_1=0.0
;cubeinfo.pc3_2=0.0
;cubeinfo.pc3_3=1.0
;cubeinfo.wcsname='Heliocentric Aries Ecliptic'
;cubeinfo.N=N
;cubeinfo.desc='envelope FM applied to IM'
;cubeinfo.author='Sandy Antunes'
;cubeinfo.program='Pixon','WLRaytrace','IDL','Other'
;cubeinfo.date=now()
;;;cubeinfo.axis_i='y'
;;;cubeinfo.axis_j='-x'
;;;cubeinfo.axis_k='z'
;;;;cubeinfo.orient='heliospheric','ecliptic','other'
