function gaus,x,m,s
;+
; $Id: gaus.pro,v 1.1 2009/04/22 18:32:52 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : gaus.pro
;               
; Purpose   : return a gaussian value for the given x
;               
; Explanation: return a gaussian value for the given x
;               
; Use       : IDL> y=gaus(x,m,s)
;    
; Inputs    : x : x to sample gaussian at
;             m : mean
;             s : sigma
;               
; Outputs   : value of gaussian at that x
;
; Keywords  : none
;
; Calls from LASCO : none 
;
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : Mathematics
;               
; Prev. Hist. : originally in get_large_timage.pro and get_timage.pro
;
; Written     : Paul Reiser, imported by Sandy Antunes, NRL, Nov-Dec 2005
;               
; $Log: gaus.pro,v $
; Revision 1.1  2009/04/22 18:32:52  antunes
; first half of relocation.
;
; Revision 1.1  2009/03/31 15:46:00  antunes
; More reorg
;
; Revision 1.3  2007/02/13 17:22:25  antunes
; Restored after SolarSoft rollback.
;
; Revision 1.1  2006/04/11 15:54:48  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.2  2005/12/27 17:48:23  antunes
; Commented half of the routines using SSW style.  Also added a new
; axes test case 'arrowcube'.
; Checked in the Tomography/old/mikic_img.pro routine here because it
; is a dependency for the general image-prep routine 'get_timage.pro'
;
;-            

g=exp(-(x-m)^2/2/s/s)/s/sqrt(2*!pi)
return,g
end
