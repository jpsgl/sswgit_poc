;+
;
; Project   : Densitycube, reconstruction, 3D, and s3drs software
;                   
; Name      : centroid3d
;               
; Purpose   : used to manipulate 3D densitycubes and do 3D reconstructions
;               
; Explanation: calculates the center of mass of an arbitrary
; multidimensional array
;               
; Use       : standalone or via s3drs GUI
;    
; Inputs    : arr
;               
; Outputs   : returns the centroid
;
; Keywords  : 
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 3d, densitycube
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; Calculates the center of mass of a an arbitrary multidimensional array.
; The middle method from http://www.dfanning.com/tips/centroid.html

function centroid3d, arr,DOUBLE=dbl
  s=size(arr,/DIMENSIONS)
  n=n_elements(s) 
  tot=total(arr,DOUBLE=dbl)
  if keyword_set(dbl) then ret=dblarr(n,/NOZERO) else ret=fltarr(n,/NOZERO)
  for i=0,n-1 do begin 
      tmp=arr
      for j=0,i-1   do tmp=total(tmp,1,DOUBLE=dbl)
      for j=i+1,n-1 do tmp=total(tmp,2,DOUBLE=dbl)
      ret[i]=total(findgen(s[i])*tmp,DOUBLE=dbl)/tot
  endfor 
  return,ret
end
