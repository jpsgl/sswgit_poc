;+
;
; Project   : Densitycube, reconstruction, 3D, and s3drs software
;                   
; Name      : timage2wireframe
;               
; Purpose   : used to manipulate 3D densitycubes and do 3D reconstructions
;               
; Explanation: converts a density cube to a simple array of xx, yy, zz
; values for populated points (non-zero)
; likely this is highly inefficient but, as it is only called
; typically once, we can live with that.
; Assumes a centered cube and returns integer positions from that center.
;               
; Use       : used by render_rot_gui
;    
; Inputs    : timage, optional threshold
;               
; Outputs   : returns wireframe
;
; Keywords  : 
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 3d, visualization, density, cube
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; converts a density cube to a simple array of xx, yy, zz
; values for populated points (non-zero)
; likely this is highly inefficient but, as it is only called
; typically once, we can live with that.
; Assumes a centered cube and returns integer positions from that center.

FUNCTION timage2wireframe,timage,threshold=threshold

  if (n_elements(threshold) eq 0) then threshold=0

  N=(size(timage))[1]
;  print,'N = ',N
;  help,timage
  ; allocate dummy too-big arrays for temporary storage
  x=lonarr(N*N*N)
  y=lonarr(N*N*N)
  z=lonarr(N*N*N)

  skip=1; test var used for sampling, but keep as 1 for actual use
  icount=long(0)
  for i=0,N-1,skip do begin
      for j=0,N-1,skip do begin
          for k=0,N-1,skip do begin
              if (timage[i,j,k] gt threshold) then begin
                  x[icount]=i-N/2
                  y[icount]=j-N/2
                  z[icount]=k-N/2
                  icount=icount+1
              end
          end
      end
  end

  ic3=icount^(0.333)
;  print,'count is ',icount,' implies n= ',ic3

  xxyyzz=lonarr(icount,3)
  xxyyzz[*,0]=x[0:icount-1]
  xxyyzz[*,1]=y[0:icount-1]
  xxyyzz[*,2]=z[0:icount-1]
;  help,xxyyzz

  return,xxyyzz

END
