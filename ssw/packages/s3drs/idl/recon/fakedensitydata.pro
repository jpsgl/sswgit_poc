;+
;
; Project   : Densitycube, reconstruction, 3D, and s3drs software
;                   
; Name      : fakedensitydata
;               
; Purpose   : used to manipulate 3D densitycubes and do 3D reconstructions
;               
; Explanation: test routine for making fake density cubes
;               
; Use       : standalone or via s3drs GUI
;    
; Inputs    : (optional) N, views
;               
; Outputs   : data, phi
;
; Keywords  : 
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 3D, density, cube
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; litle test routine that generates a set of sample datum and
; angles, for use with routines like simple3drv etc.

PRO fakedensitydata,datum,phi,N=N,views=views

  if (n_elements(N) eq 0) then N=32

  Nout=N
  if (N gt 256) then begin
      ; due to memory issues, we up-sample our data past 256
      N=256
  end

  views=2

  print,'generating demo data, cube, ',views,' views, N=',N

  ; generates fake datum
  timage=fake_ne_cube(N,lit=N/4)
  modparam=timage2modparam(timage,/quiet)
  raytracewl,sbt,imsize=[N,N],losnbp=long(N),losrange=[-20,20],$
    modelid=25,modparam=modparam,neang=[0,0,0]*!dtor,/cor2,/quiet

  datum=fltarr(N,N,views)
  ; can safely replicate views since object is symmetric
  for m=0,views-1 do datum(*,*,m)=sbt.im

  if (Nout ne N) then datum=congrid(datum,Nout,Nout,views)

  phi=fltarr(3,views)
  for m=0,views-1 do phi[*,m] = [0,90*m,0]
      
END
