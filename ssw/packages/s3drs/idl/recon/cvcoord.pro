pro cvcoord,x,y,z,r,phi,theta
;+
; $Id: cvcoord.pro,v 1.1 2009/04/22 18:32:49 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : cvcoord.pro
;               
; Purpose   : cartesian to spherical coord conversion
;               
; Explanation: simple geometry conversion
;               
; Use       : IDL> cvcoord,x,y,z,r,phi,theta
;    
; Inputs    : x,y,z
;               
; Outputs   : r, phi, theta
;
; Keywords  : none
;
;               
; Calls from LASCO : none 
;
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : Geometry
;               
; Prev. Hist. : None.
;
; Written     : Unknown, imported by Sandy Antunes, NRL, Nov-Dec 2005
;               
; $Log: cvcoord.pro,v $
; Revision 1.1  2009/04/22 18:32:49  antunes
; first half of relocation.
;
; Revision 1.1  2009/04/16 15:58:47  antunes
; Refactoring of util & pixon part 1, preparing for SSW packaging.
;
; Revision 1.4  2009/03/31 15:44:22  antunes
; More reorg.
;
; Revision 1.1  2006/04/11 15:54:48  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.2  2005/12/27 17:48:23  antunes
; Commented half of the routines using SSW style.  Also added a new
; axes test case 'arrowcube'.
; Checked in the Tomography/old/mikic_img.pro routine here because it
; is a dependency for the general image-prep routine 'get_timage.pro'
;
;-            

; cartesian to spherical coord conversion

 r=sqrt(x*x+y*y+z*z)
 theta=asin(z/r)
 ax=abs(x)
 ratio=ax/(r * cos(theta))
 phi=0

 if(abs(fix(ratio)) lt 1)then phi=acos(ratio)
 
 if(x lt 0)then phi=!pi-phi
 if(y lt 0)then phi=2*!pi-phi

 return
 end

