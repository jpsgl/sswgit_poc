;+
;
; Project   : Densitycube, reconstruction, 3D, and s3drs software
;                   
; Name      : timage2modparam
;               
; Purpose   : used to manipulate 3D densitycubes and do 3D reconstructions
;               
; Explanation: reformats a 3D image (ala s3drs or pixon) into Arnaud's
; 'modparam' structure for rendering by his renderer
;               
; Use       : standalone or via s3drs GUI
;    
; Inputs    : timage, rsun, reorder
;               
; Outputs   : returns the modparam structure
;
; Keywords  : noconvert, quiet
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 3d, density
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
;               
; $Log: timage2modparam.pro,v $
; Revision 1.1  2009/04/22 18:32:59  antunes
; first half of relocation.
;
; Revision 1.2  2009/04/01 16:01:52  antunes
; GUI v 1.7, prettification, better error handling, more models.
;
; Revision 1.1  2009/03/31 15:15:52  antunes
; More reorg.
;
; Revision 1.3  2008/01/09 19:24:25  antunes
; Finally fixed the data scaling unit (bug=legacy code used AU not Rsun),
; also added more tests.
;
; Revision 1.2  2008/01/04 20:16:45  antunes
; Final units (hopefully), also more tests and tools.
;
; Revision 1.1  2006/04/11 15:54:48  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.4  2006/03/16 20:25:28  antunes
; Improved pixon calling, wrote proto-frontend GUI for
; different rendering tests.
;
; Revision 1.3  2006/03/16 14:44:13  antunes
; Axes improvements and better handling of model params.
;
; Revision 1.2  2006/01/09 17:14:42  antunes
; More commented added.
; New 'super3drv' driver routine for getting user specs for reconstruction
; process now checked in.  Just type 'super3drv' to test.
;
;-            
; converts from Pixon 'timage' format to RaytraceWL 'modparam' array
; optional param 'rsun' lets you set the voxel size in units of solar radius
;
; Note that raytracelwl expects modelid=25 or 26 for user-specifed cubes
;

FUNCTION timage2modparam,timage,rsun=rsun,noconvert=noconvert,$
                         reorder=reorder,quiet=quiet

  if (n_elements(rsun) eq 0) then rsun=0.8 ; a reasonable default value
  if (n_elements(reorder) eq 0) then reorder=[3,2,1]; default transform

  Ns=size(timage,/DIMENSIONS)
  N=long(Ns[0])

  noconvert=KEYWORD_SET(noconvert)

  if (noconvert) then begin
      modparam=[N,N,N,N/2,N/2,N/2,rsun,reform(timage,N*N*N)]
  end else begin
      ; converts from pixon axes scheme to rtwl axes scheme non-destructively
      if (keyword_set(quiet) eq 0) then print,'converting to pixon schema'
      modparam=[N,N,N,(N-1)/2.0,(N-1)/2.0,(N-1)/2.0,rsun,$
                reform(rearrange(timage,reorder),N*N*N)]
  end

  return,modparam

END
