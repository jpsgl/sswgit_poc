FUNCTION fake_ne_cube,Ncube, generate=generate,angles=angles, lit=lit, $
                      center=center
;+
; $Id: fake_ne_cube.pro,v 1.1 2009/04/22 18:32:50 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : fake_ne_cube.pro
;               
; Purpose   :  builds a fake density cube for rendering, of size N^3,
;              sun-centered, with the defined pixel region bright and
;              (if defined region is of odd size) the center-center of
;              region brightest.
;               
; Explanation: 
;               
; Use       : IDL> cube=fake_ne_cube(Ncube,generate='cube/data/modparam',$
;                  [angles=[90,90,0]],[center=[N/2,N/2,N/2]],[lit=1])'
;    
; Inputs    : Ncube : size of array object to create and return
;               
; Outputs   : N^3 float cube (3-dimensional array) of n_e values
;
; Keywords  : generate : can be 'cube' (N^3 cube array, default)
;             'modparam' (N^3 cube and angles in raytracewl structure) or
;             'data' (raytracewl data of desired cube at desired angles)
;
;             lit : integer describing how many central pixels to lit
;                   (as a cube, i.e. 2 = a 2x2x2 lit cube)
;
;             center : an integer 3-vector giving the position to
;                      center the lit region within the created cube
;
;             angles : a 3-vector of desired observer position angles, only
;                      useful if using raytracewl as 'modparam' or 'data'
;
; Calls from LASCO : none 
;
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: none
;               
; Category    : Simulation, Geometry
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRL, Nov-Dec 2005
;               
; $Log: fake_ne_cube.pro,v $
; Revision 1.1  2009/04/22 18:32:50  antunes
; first half of relocation.
;
; Revision 1.1  2009/03/31 15:15:48  antunes
; More reorg.
;
; Revision 1.1  2006/04/11 15:54:48  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.6  2006/03/24 17:53:20  antunes
; Reconciled c and t environments, created test cases and profiling
; checks, working towards generalized tetra object.
;
; Revision 1.5  2006/03/16 20:25:27  antunes
; Improved pixon calling, wrote proto-frontend GUI for
; different rendering tests.
;
; Revision 1.4  2005/12/27 17:48:23  antunes
; Commented half of the routines using SSW style.  Also added a new
; axes test case 'arrowcube'.
; Checked in the Tomography/old/mikic_img.pro routine here because it
; is a dependency for the general image-prep routine 'get_timage.pro'
;
;-            
; builds a fake density cube for rendering, of size N^3, sun-centered,
; with the defined pixel region bright and (if defined region is of
; odd size) the center-center of region brightest.
;
; suitable for use with raytracewl
;
; give it the desired 'N' and also indicate what to make:
; generate = 'cube' (3D), 'data' (2D), or 'modparam' (cube+params as modparam)
;    default is to return a cube
; litarea = optional, the number of centered inner pixels to light, def=1
; center = optional cube location to put lit area, def = center
;   you can also give it a 3-array of angles (in degrees) for camera position
; angles= optional, def = [90,90,0]
;
; see also 'plot_2dbordered', which will invoke this with a sample
; cube if you do not give it data


  if (n_elements(Ncube) eq 0) then begin
      print,"usage is 'item=fake_ne_cube(Ncube,generate='cube/data/modparam',[angles=[90,90,0]],[center=[N/2,N/2,N/2]],[lit=1])'"
      return,fltarr(1,1,1)
  end

  N=long(Ncube); necessary to ensure N^3 fits integer size

  if (n_elements(angles) eq 0) then angles=[90,90,0]
  if (n_elements(center) eq 0) then center=[fix(N/2),fix(N/2),fix(N/2)]
  if (n_elements(generate) eq 0) then generate='cube'
  if (n_elements(lit) eq 0) then lit=1

;  N2=fix(N/2)
  cube=fltarr(N,N,N)

  cx=center[0]
  plx=max([0,cx-fix(lit/2)])
  phx=min([N-1,cx+fix((lit-1)/2)])
  cy=center[1]
  ply=max([0,cy-fix(lit/2)])
  phy=min([N-1,cy+fix((lit-1)/2)])
  cz=center[2]
  plz=max([0,cz-fix(lit/2)])
  phz=min([N-1,cz+fix((lit-1)/2)])
;  print,'centers from, to',cx,cy,cz,plx,phx,ply,phy,plz,phz
  cube[plx:phx,ply:phy,plz:phz]=1e9
  ; light center pixel brighter for odd-sized cubes
  if (evenodd(lit) eq 1) then cube[cx,cy,cz]=1e10

  if (generate eq 'modparam' or generate eq 'data') then begin
      ; xsize, ysize, zsize, xcoord_sun, ycoord_sun, zcoord_sun,
      ;  voxel size in rsun, datacube in lexicographical order x,y,z
      Ncubed=N*N*N
      cube=reform(cube,Ncubed,/OVERWRITE)
      params=[N,N,N,N/2,N/2,N/2,0.8,cube]

      if (generate eq 'modparam') then begin
          return,params
      end else if (generate eq 'data') then begin
; model 26 uses trilinear interpolation between voxels, model 25 uses no interp
          raytracewl,sbt,imsize=[N,N],losnbp=N,losrange=[-20,20],$
            modelid=25,modparam=params,neang=angles*!dtor,/cor2,quiet=1
          return,sbt.im
      end
  end else begin
      return,cube
  end

END
