;+
;
; Project   : Densitycube, reconstruction, 3D, and s3drs software
;                   
; Name      : hybrid
;               
; Purpose   : used to manipulate 3D densitycubes and do 3D reconstructions
;               
; Explanation: research suite of tools to do hybrid reconstructions
;             (FM + inversion)
;               
; Use       : standalone, contains 'buildfm, runfm, extractfms,
; plotfms, extendedfms, datafms, densityfms'
;    
; Inputs    : type 'hybrid' for details
;               
; Outputs   : type 'hybrid' for details
;
; Keywords  : 
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: resulted in SolarPhys paper, 2009
;               
; Category    : 3d, reconstruction
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; best rotation is 99.2362, 102.908, -23.969

PRO buildfm,check=check
; BUILD IT, IF YOU NEED IT
  check=KEYWORD_SET(check)
  fin='fluxrope_rotated.sav'
  rotparam=[99.2362, 102.908, +23.969]
  buildcloud,33,cubesidersun=15.0,output=1,cubesidenbpix=128,/quiet
  sol_image=readcloudcube(33)
  cuberot3d,sol_image,rotparam
  save,file=fin,sol_image
  if (check) then threeview,sol_image,/pixon
END

PRO initfm,intimes
; 0=preframe, 1=first background for running, 2:* = dataframes
  intimes=['0022','0122','0152','0222','0252','0322','0352','0422']
END

PRO runfm,intimes,running=running,envviews=envviews,N=N,infile=infile,$
          mxit=mxit,checksn=checksn,maskseries=maskseries

  running=KEYWORD_SET(running)
  checksn=KEYWORD_SET(checksn)
  maskseries=KEYWORD_SET(maskseries)
  if (n_elements(N) eq 0) then N=128
  if (n_elements(intimes) eq 0) then initfm,intimes
  if (n_elements(infile) eq 0) then infile='fluxrope_rotated.sav'
  if (n_elements(mxit) eq 0) then mxit=20; short test runs

  if (maskseries) then begin
      ; just skip ahead
  end else if (strmid(infile,3,/reverse) eq '.xdr') then begin
      ; it's an Arnaud XDR
      dataZ=readstruct(infile)
      solution=congrid(dataZ.im,N,N)
  end else if (strmid(infile,3,/reverse) eq '.sav') then begin
      restore,/v,infile; reads cubes or data
      if (n_elements(sol_image) ne 0) then $
        solution=congrid(sol_image,N,N,N) else $
        solution=congrid(solution,N,N)
  end else begin
      print,'Not sure what to do with envelope file, ending.  ',infile
      stop
  end

  for i=2,n_elements(intimes)-1 do begin
      if (running) then pi=i-1 else pi=0
      if (maskseries) then begin
          ; rotate through different masks
          infile='mask'+intimes[i]+'.sav'
          restore,/v,infile; contains 'solution' mask
      end
      quickcme,128,'20071231_'+intimes[i]+'00','20071231_'+intimes[pi]+'00',$
        /cor2,/standard,/jump,nadjust=10.0,/scrub,checksn=checksn,$
        envelope=solution,envviews=envviews,/occultright,/concave,mxit=mxit,$
        datacut=1E-11
  end

  print,'Done running simulations'

END

; the '/raw' flag lets my post-processor be used by non-envelope runs too

PRO extractfms,intimes,running=running,envviews=envviews,N=N,raw=raw

  raw=KEYWORD_SET(raw)
  if (n_elements(intimes) eq 0) then initfm,intimes
  if (n_elements(N) eq 0) then N=128

  running=KEYWORD_SET(running)
  outname='plotme' + int2str(running) + '.sav'

  ; botched here, debug
  if (raw) then es="" else if (n_elements(envviews) eq 0) then $
    es='_envelope' else es='+'+strjoin(int2str(envviews),'_')

  for i=2,n_elements(intimes)-1 do begin

      if (running) then pi=i-1 else pi=0

      ; try both the regular and datacut variants
      fin='20071231_'+intimes[i]+'00_'+int2str(N)+'_20071231_'+$
        intimes[pi]+'00'+'_cor2'+es+$
        '_jumpstart_wt=10_overmask=2c_solved.sav'
      if (file_exist(fin) eq 0) then $
        fin='20071231_'+intimes[i]+'00_'+int2str(N)+'_20071231_'+$
        intimes[pi]+'00'+'_cor2'+es+$
        '_jumpstart_datacut_wt=10_overmask=2c_solved.sav'

      if (file_exist(fin) eq 0) then begin
          print,'File not found: ',fin
      end else begin
          restore,/v,fin
          
          dvox=pxwrapper.sr.dvox
          N=pxwrapper.sr.Ncube

          ;push,imstack,solution_image

          ; carve out center
          m=p_getmask('COR2',pxwrapper.sr.Ncube,rpixels=rpixels)
          mask3D = cube_sphere(pxwrapper.sr.Ncube,rpixels[0],r_outer=rpixels[1])
          solution_image=solution_image*mask3D

          threeview,solution_image,/pixon,/noplot,triofull

          push,totalmass,total(solution_image)

          push,lset,intimes[i]+' Y'
          push,lset,intimes[i]+' Z'

          push,yzviewsfull,triofull[*,*,1]
          push,yzviewsfull,triofull[*,*,2]

          mytime=long(strmid(intimes[i],0,2))*60 + long(strmid(intimes[i],2))
          push,rtimes,mytime

          cm=(centroid3d(solution_image) - ((N-1)/2.0))
          push,fullcm,cm*dvox
          rsun=sqrt(total(cm^2)) * dvox
          push,fullradials,rsun

          ; now separate into two pieces
          splitfmcube,solution_image,tophalf,basehalf

          tstamp=intimes[i]
          if (running eq 0) then $
            save,tstamp,tophalf,basehalf,solution_image,$
            file=int2str(i)+'_halves.sav'

          threeview,tophalf,/pixon,/noplot,triotop
          push,topmass,total(tophalf)

          push,yzviewstop,triotop[*,*,1]
          push,yzviewstop,triotop[*,*,2]

          cm=(centroid3d(tophalf) - ((N-1)/2.0))
          push,topcm,cm*dvox
          rsun=sqrt(total(cm^2)) * dvox
          push,topradials,rsun

          threeview,basehalf,/pixon,/noplot,triobase
          push,basemass,total(basehalf)

          push,yzviewsbase,triobase[*,*,1]
          push,yzviewsbase,triobase[*,*,2]

          cm=(centroid3d(basehalf) - ((N-1)/2.0))
          push,basecm,cm*dvox
          rsun=sqrt(total(cm^2)) * dvox
          push,baseradials,rsun

          push,yzall,triofull[*,*,1]
          push,yzall,triotop[*,*,1]
          push,yzall,triobase[*,*,1]
          push,lsetall,intimes[i]+' Y total'
          push,lsetall,intimes[i]+' Y upper'
          push,lsetall,intimes[i]+' Y lower'

          push,yzall,triofull[*,*,2]
          push,yzall,triotop[*,*,2]
          push,yzall,triobase[*,*,2]
          push,lsetall,intimes[i]+' Z total'
          push,lsetall,intimes[i]+' Z upper'
          push,lsetall,intimes[i]+' Z lower'

      end

  end

  save,file=outname,yzviewsfull,yzviewstop,yzviewsbase,yzall,lsetall,dvox,N, $
    lset,fullradials,rtimes,topradials,baseradials,$
    totalmass,topmass,basemass,fullcm,topcm,basecm
  print,'Saved CMs, y-z views in ',outname

END

PRO plotfms,running=running,ps=ps,nomulti=nomulti

  running=KEYWORD_SET(running)
  ps=KEYWORD_SET(ps)
  nomulti=KEYWORD_SET(nomulti)

  fin='plotme' + int2str(running) + '.sav'

  restore,/v,fin

  fout1='yz'+int2str(running)+'.ps'
  fout2='2d'+int2str(running)+'.ps'

  tv_multi,/log,yzall,labels=lsetall,line=6,oom=2,/reverse,/notitle,/compact,$
    header='Time series of Dec 31 CME from Z and Y views',file=fout1,ps=ps

  current_device=!d.name

  if (ps) then begin
      set_plot,'ps'
      if (nomulti) then foutps='1'+fout2 else foutps=fout2
      device,filename=foutps,/color,bits_per_pixel=8,$
        font_size=9,/landscape;,/encapsulate,ysize=8.25,xsize=11.25,/inches
      print,'Using ps'
  end else begin
      window,/free,retain=2
      print,'Using xwin'
  end

  if (nomulti eq 0) then !P.MULTI=[0,3,2]

  ;!P.MULTI=[0,0,3]
  ;plot,fullcm[0,*],fullcm[1,*],psym=-6,tit='X-Y trajectory',charsize=1.5,/yno
  ;oplot,fullcm[1,*],fullcm[2,*],psym=-6,tit='Y-Z trajectory',charsize=1.5,/yno
  ;oplot,fullcm[0,*],fullcm[2,*],psym=-6,tit='X-Z trajectory',charsize=1.5,/yno
  ;!P.MULTI=0
  tmx=max(abs(fullcm[0:1,*]))
  tmy=max(abs(fullcm[1:2,*]))
  plot,abs(fullcm[0,*]),abs(fullcm[1,*]),psym=-6,$
    xrange=[0,tmx],yrange=[0,tmy],$
    title='Trajectories',charsize=1.5 ; ,/yno
  oplot,abs(fullcm[1,*]),abs(fullcm[2,*]),psym=-4
  oplot,abs(fullcm[0,*]),abs(fullcm[2,*]),psym=-5
  legend,['X-Y','Y-Z','X-Z'],psym=[-6,-4,-5],/right

  if (nomulti) then begin
      if (ps) then begin
          device,/close
          device,filename='2'+fout2,/color,bits_per_pixel=8,$
            font_size=9,/landscape;,/encapsulate,ysize=8.25,xsize=11.25,/inches
      end else begin
          window,/free,retain=2
      end
  end

  nel=n_elements(fullcm[0,*])-2; get next-to-last frame for clarity
  finalrho=atan(fullcm[1,nel],fullcm[0,nel])*!radeg  ; x-y projection
  r=sqrt(fullcm[0,nel]^2+fullcm[1,nel]^2)
  finalphi=atan(fullcm[2,nel],r)*!radeg  ; z angle
  print,'hae lon: ',finalrho,', hae lat: ',finalphi

  tit='D-T Plot'
  if (running) then tit+= 'for running diff' else tit+= 'for diff image'

  plot,rtimes,fullradials,xtitle='Time of day (minutes)',ytitle='Rsun',/ynoz,$
    yrange=[1,11],title=tit,psym=-4,charsize=1.5
  oplot,rtimes,topradials,psym=-1
  oplot,rtimes,baseradials,psym=-5
  legend,['Top','Base','all'],psym=[-1,-5,-4],/right
  if (running) then $
    save,file='d-t_run.sav',rtimes,fullradials,topradials,baseradials else $
    save,file='d-t_diff.sav',rtimes,fullradials,topradials,baseradials


  if (nomulti) then begin
      if (ps) then begin
          device,/close
          device,filename='3'+fout2,/color,bits_per_pixel=8,$
            font_size=9,/landscape;,/encapsulate,ysize=8.25,xsize=11.25,/inches
      end else begin
          window,/free,retain=2
      end
  end

  plot,rtimes,ratio(topradials,baseradials),psym=-6,/ynoz,$
    title='Ratio, Upper to Lower D-T',charsize=1.5

  if (nomulti) then begin
      if (ps) then begin
          device,/close
          device,filename='4'+fout2,/color,bits_per_pixel=8,$
            font_size=9,/landscape;,/encapsulate,ysize=8.25,xsize=11.25,/inches
      end else begin
          window,/free,retain=2
      end
  end

  plot,rtimes,topradials-baseradials,psym=-6,/ynoz,$
    title='Diff, Upper to Lower D-T',charsize=1.5

  if (nomulti) then begin
      if (ps) then begin
          device,/close
          device,filename='5'+fout2,/color,bits_per_pixel=8,$
            font_size=9,/landscape;,/encapsulate,ysize=8.25,xsize=11.25,/inches
      end else begin
          window,/free,retain=2
      end
  end

  difffull=fullradials
  difftop=topradials
  diffbase=baseradials
  for i=1,n_elements(topradials)-1 do $
    difftop[i]=ratio(topradials[i]-topradials[i-1],rtimes[i]-rtimes[i-1])
  for i=1,n_elements(baseradials)-1 do $
    diffbase[i]=ratio(baseradials[i]-baseradials[i-1],rtimes[i]-rtimes[i-1])
  for i=1,n_elements(fullradials)-1 do $
    difffull[i]=ratio(fullradials[i]-fullradials[i-1],rtimes[i]-rtimes[i-1])

  ; units are currently Rsun/minute, convert to km/sec
  ; Rsun=6.65997E+05 km, minute=60 sec, so conv=6.65997E+05/60=11100.0
  difffull=difffull*11100.0
  difftop=difftop*11100.0
  diffbase=diffbase*11100.0

  plot,rtimes[1:*],difffull[1:*],psym=-4,/ynoz,yrange=[-200,600], $
    title='Vr vs time',charsize=1.5,xtitle='minutes',ytitle='Vr (km/sec)'
  oplot,rtimes[1:*],difftop[1:*],psym=-1
  oplot,rtimes[1:*],diffbase[1:*],psym=-5
  legend,['Top','Base','all'],psym=[-1,-5,-4],/right

  ; for an N=128 Cor2 sim, solution is a normalized electron density e-/vox
  ; dvox=0.234375 solar radii, m_e is 9.10938975e-28 g, rsun = 6.65997e10 cm
  ;
  ; so given e-/cm^3, convert to e-/voxel with e-/cm^3 * (6.65997e10 cm/rsun)^3
  ;     * (0.234375^3 rsun/voxel) =  e-/voxel, or
  ;   e-/voxel = 3.80319E30 times a given voxel's e-/cm^3
  ; So given total(image) summed over all voxels, we just apply the above
  ;   total(image) * 3.80319E30 = total_image_in_e-
  ; And to convert this to a mass in grams, total_image_in_e- * 9.1E-28g
  ; Yielding us a total conversion factor of 3.460 E3.
  ; Using Russ's fully ionized hydrogen + 10% helium, it's 1.97E-24 g per
  ;    per observed electron, giving us a total conversion factor of 7.49E6
  ;
  ; In summary:
  ;   For a single voxel,  image[x,y,z] * 7.49E6 = mass in g in that voxel
  ;   For the entire image,  total(image) * 7.49E6 = total mass of e- in g
  ;
  ; Typical solution total(image) = 5E8 so total mass = 1E15 grams

  ;print,total(solution_image)*((pxwrapper.sr.dvox*6.66)^3)*9.1*100
  econv=7.49E6

  if (nomulti) then begin
      if (ps) then begin
          device,/close
          device,filename='6'+fout2,/color,bits_per_pixel=8,$
            font_size=9,/landscape;,/encapsulate,ysize=8.25,xsize=11.25,/inches
      end else begin
          window,/free,retain=2
      end
  end

  plot,rtimes,totalmass*econv,psym=-4,/ynoz,yrange=[1E15,2E16],charsize=1.5, $
    title='Mass Cons.- total, top, bottom'
  oplot,rtimes,topmass*econv,psym=-1
  oplot,rtimes,basemass*econv,psym=-5
  legend,['Top','Base','all'],psym=[-1,-5,-4],/right

  if (nomulti eq 0) then !P.MULTI=0

  if (ps) then begin
      device,/close
      set_plot,current_device
  end

  print,'max total mass is ',max(totalmass*econv)
  print,'D-T plot times are: ',rtimes
  print,'D-T plot radii are: ',fullradials
  print,'D-T front radii is: ',topradials


END

PRO extendedfms,ps=ps,nomulti=nomulti

  ps=KEYWORD_SET(ps)
  nomulti=KEYWORD_SET(nomulti)

  current_device=!d.name

  if (ps) then begin
      set_plot,'ps'
      device,filename='velocities.ps',/color,bits_per_pixel=8,$
        font_size=9,/landscape;,/encapsulate,ysize=8.25,xsize=11.25,/inches
  end else begin
      window,/free,retain=2
  end

  if (nomulti eq 0) then !P.MULTI=[0,1,3]

; 6) Vexp-T plot (FM Mask 0452)
;  this is (cm_diff - cm_running)at N - (cm_diff - cm_running) at N-1

  restore,'plotme0.sav'
  ; units are currently Rsun/minute, convert to km/sec
  ; Rsun=6.65997E+05 km, minute=60 sec, so conv=6.65997E+05/60=11100.0
  topradials *= 11100.0
  baseradials *= 11100.0
  fullradials *= 11100.0

  for i=1,n_elements(fullradials)-1 do $
        push,vrtop,ratio(topradials[i]-topradials[i-1],rtimes[i]-rtimes[i-1])
  for i=1,n_elements(fullradials)-1 do $
        push,vrbase,ratio(baseradials[i]-baseradials[i-1],rtimes[i]-rtimes[i-1])
  for i=1,n_elements(fullradials)-1 do $
        push,vrfull,ratio(fullradials[i]-fullradials[i-1],rtimes[i]-rtimes[i-1])

  restore,'plotme1.sav'
  ; units are currently Rsun/minute, convert to km/sec
  ; Rsun=6.65997E+05 km, minute=60 sec, so conv=6.65997E+05/60=11100.0
  topradials *= 11100.0
  baseradials *= 11100.0
  fullradials *= 11100.0

  for i=1,n_elements(fullradials)-1 do $
        push,rvrtop,ratio(topradials[i]-topradials[i-1],rtimes[i]-rtimes[i-1])
  for i=1,n_elements(fullradials)-1 do $
        push,rvrbase,ratio(baseradials[i]-baseradials[i-1],rtimes[i]-rtimes[i-1])
  for i=1,n_elements(fullradials)-1 do $
        push,rvrfull,ratio(fullradials[i]-fullradials[i-1],rtimes[i]-rtimes[i-1])


  vexpfull=rvrfull-vrfull
  vexptop=rvrtop-vrtop
  vexpbase=rvrbase-vrbase

  plot,rtimes[1:*],vexptop,psym=-1,$
    title='Expansion Vel vs T',xtitle='T(min)',ytitle='Vexp (km/sec)'
  oplot,rtimes[1:*],vexpbase,psym=-5
  oplot,rtimes[1:*],vexpfull,psym=-4
  legend,['Top','Base','all'],psym=[-1,-5,-4]

  if (nomulti) then begin
      if (ps) then begin
          device,/close
          device,filename='constant.ps',/color,bits_per_pixel=8,$
            font_size=9,/landscape;,/encapsulate,ysize=8.25,xsize=11.25,/inches
      end else begin
          window,/free,retain=2
      end
  end

; 7) Vexp/V vs T (FM Mask 0452): if constant, I win!
;  take item from #6 and divide by Vr
;

  if (nomulti) then !P.MULTI=[0,0,2]; counter-intuitive, but correct

  vcontop=vexptop/vrtop
  vconbase=vexpbase/vrbase
  vconfull=vexpfull/vrfull

  plot,rtimes[2:*],vcontop[1:*],title='Ratio of Exp to Vel',xtitle='T',ytitle='Ratio',psym=-1
  oplot,rtimes[2:*],vconbase[1:*],psym=-5
  oplot,rtimes[2:*],vconfull[1:*],psym=-4
  legend,['Top','Base','all'],psym=[1,5,4]

; or maybe
  plot,vrtop,vcontop,title='Ratio of Exp to Vel',xtitle='Vr (km/sec)',ytitle='Ratio',psym=1,$
    yrange=[-1,1],xrange=[100,600]
  oplot,vrbase,vconbase,psym=5
  oplot,vrfull,vconfull,psym=4
  legend,['Top','Base','all'],psym=[1,5,4]


  !P.MULTI=0

  if (ps) then begin
      device,/close
      set_plot,current_device
  end

END


PRO splitfmcube,cube,tophalf,basehalf,debug=debug

  angleguess=-25.0
  debug=KEYWORD_SET(debug)

  cube2=cube
  cuberot3d,cube2,[0,angleguess,0]
  if (debug) then threeview,/pixon,/log,oom=2,cube2

  tophalf=cube2
  tophalf[*,*,0:64]=0
  cuberot3d,tophalf,[0,0-angleguess,0]
  if (debug) then threeview,/pixon,/log,oom=2,tophalf,title='upper half'

  basehalf=cube2
  basehalf[*,*,65:*]=0
  cuberot3d,basehalf,[0,0-angleguess,0]
  if (debug) then threeview,/pixon,/log,oom=2,basehalf,title='lower half'

END

PRO datafms,ps=ps,N=N

  ps=KEYWORD_SET(ps)
  if (n_elements(N) eq 0) then N=128
  if (n_elements(intimes) eq 0) then initfm,intimes

; 1) time series of input data to show the Dec 31, 2007 event

  pi=0

  ; botched here, debug
  es='_envelope'

  ; note we stop 1 element early to remove the last 'gone' element
  for i=1,n_elements(intimes)-2 do begin
      ; try both the regular and datacut variants
      fstem='20071231_'+intimes[i]+'00_'+int2str(N)+'_20071231_'+$
        intimes[pi]+'00'+'_cor2'+es+'_jumpstart_wt=10_overmask=2c'
      if (file_exist(fstem+'_inputdata.sav') eq 0) then $
        fstem='20071231_'+intimes[i]+'00_'+int2str(N)+'_20071231_'+$
        intimes[pi]+'00'+'_cor2'+es+'_jumpstart_datacut_wt=10_overmask=2c'

      if (file_exist(fstem+'_inputdata.sav')) then begin

          restore,fstem+'_inputdata.sav'

          push,dsetA,diff[*,*,0]*mset[*,*,0]
          push,dsetB,diff[*,*,1]*mset[*,*,1]
          push,lsetA,intimes[i]+' A'
          push,lsetB,intimes[i]+' B'

      ; also set up for plot#2
          if (intimes[i] eq '0222') then fnametwo=fstem+'_solved.sav'

      end

  end

  dset=[[[dsetA]],[[dsetB]]]

  ; burn away corners
  altmask =~ p_getmask('corners',128)
  altmask *= max(dset)
  for i=0,n_elements(dset[0,0,*])-1 do $
    dset[*,*,i]=dset[*,*,i]+altmask

  lset=[lsetA,lsetB]
  dset2=fltarr(64,128,10)
  for i=0,n_elements(dset2[0,0,*])-1 do dset2[*,*,i]=dset[0:63,*,i]

  tv_multi,dset2,labels=lset,/log,/notitle,/compact,oom=3,$
    line=5,file='cme.ps',ps=ps;,header='Dec 31, 2007 CME Event'


; 2) A sample recon pair: A in vs out for constrained line

  if (file_exist(fnametwo)) then begin
      restore,fnametwo

      masks=stereo_fetch(pxwrapper,opixon,/mask)
      diff=stereo_fetch(pxwrapper,opixon)*masks
      diff=diff[*,*,0]
      rend=stereo_fetch(pxwrapper,opixon,/render)*masks
      rend=diff[*,*,0]
      lset=['Input Image','Reconstruction']
      tv_multi,[[[diff]],[[rend]]],/log,oom=2,line=2,labels=lset,res=2,$
        /notitle,/compact,$
        header='Diff. input and recon output for 02:22',ps=ps,file='io.ps'

  end

END

PRO densityfms,intimes=intimes,ps=ps

  ps=KEYWORD_SET(ps)

  ;   For a single voxel,  image[x,y,z] * 7.49E6 = mass in g in that voxel
  ;   dvox=0.234375 solar radii, m_e is 9.10938975e-28 g, rsun = 6.65997e10 cm
  ;   so grams/cm^3 is   mass*7.49E6 / ((0.23475 * 6.65997e10)^3)
  rhoconv=1.96E-24; converts mass units to density in g/cm^3

  print,'mean density (top) ','mean density (base)'
  if (n_elements(intimes) eq 0) then initfm,intimes
  for i=0,n_elements(intimes)-1 do begin
      ftest=int2str(i)+'_halves.sav'
      if (file_exist(ftest)) then begin
          restore,/v,ftest
          tophalf=tophalf*rhoconv
          basehalf=basehalf*rhoconv
          full=solution_image*rhoconv

          topdev=stddev(tophalf)
          topmean=mean(tophalf)

          basedev=stddev(tophalf)
          basemean=mean(basehalf)

          fulldev=stddev(full)
          fullmean=mean(full)

          cutoff=min([topmean,basemean,fullmean])

          ; so we are getting the average density among those voxels
          ; that exist

          iet=where(tophalf gt cutoff)
          trho=mean(tophalf[iet])
          numtop=float(n_elements(iet))

          ieb=where(basehalf gt cutoff)
          brho=mean(basehalf[ieb])
          numbase=float(n_elements(ieb))

          ief=where(full gt cutoff)
          frho=mean(full[ief])
          numfull=float(n_elements(ief))

          print,100.0*numtop/n_elements(tophalf),$
            100.0*numbase/n_elements(basehalf),$
            100.0*numfull/n_elements(full)

          push,toprho,trho
          push,baserho,brho
          push,allrho,frho
          mytime=long(strmid(tstamp,0,2))*60 + long(strmid(tstamp,2))
          push,rtimes,mytime
          push,tstamps,strmid(tstamp,0,2)+':'+strmid(tstamp,2)

      end
  end

  save,rtimes,tstamps,toprho,baserho,allrho,file='rho.sav'

  rtimes=[rtimes[0:1],rtimes[3:*]]; remove 0252 element
  tstamps=[tstamps[0:1],tstamps[3:*]]; remove 0252 element
  baserho=[baserho[0:1],baserho[3:*]]; remove 0252 element
  toprho=[toprho[0:1],toprho[3:*]]; remove 0252 element
  allrho=[allrho[0:1],allrho[3:*]]; remove 0252 element


  current_device=!d.name

  if (ps) then begin
      set_plot,'ps'
      device,filename='density.ps',/color,bits_per_pixel=8,$
        font_size=9,/landscape;,/encapsulate,ysize=8.25,xsize=11.25,/inches
  end

  ; hack for paper
  ;tstamps=['01:52','02:22','02:52','03:22','03:52','04:22']
  ;save,rtimes,baserho,toprho,allrho,tstamps,file='densityplottables.sav'

  plot,rtimes,baserho,psym=-5,xstyle=11,ymargin=[4,10],$
    ytitle='Density (g/cm^3)',xticks=1,xtickformat='(A1)'
  xyouts,0.5,0.9,'Mean Density for CME by region',align=0.5,charsize=1.2,/norm
  oplot,rtimes,toprho,psym=-1
  oplot,rtimes,allrho,psym=-4
  axis,xtickname=tstamps,xaxis=0,$
    xticks=n_elements(rtimes)-1,xtickv=rtimes,$
    xtitle='Data timestamp'
  legend,['Upper','Lower','entire'],psym=[-1,-5,-4],/right

  if (ps) then begin
      device,/close
      set_plot,current_device
  end

END




PRO hybrid

  print,'This is a wrapper for the FM/IM Envelope hybrid reconstruction method'
  print,'It defaults to reconstructing Dec 31, 2007'
  print,'* buildfm to create the fluxrope envelope'
  print,'* runfm[,/running,envviews=envviews,infile=infile,N=N,/maskseries] to envelope reconstruct'
  print,'    (you can also pass along /checksn and mxit=X to quickcme in runfm)'
  print,'* extractfms[,/running,envviews=envviews,N=N] to prep the plottables'
  print,'    (for non-hybrid runs, use /raw with extractfms)'
  print,'* plotfms[,/running,/ps,/nomulti] to actually view the D-T curves'
  print,'* extendedfms[,/ps,/nomulti] for diff+running comparative plots'
  print,'* datafms[,/ps] for creating simple paper-ready visualizations'
  print,'* densityfms[,/ps]  creates a plot of mean density, top vs bottom'

END
