;+
; $Id: ijkproj.pro,v 1.1 2009/04/22 18:32:53 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : ijkproj
;               
; Purpose   : project a 3D cube into 2D
;               
; Explanation: returns 2D on-axis summation along a line of sight for a
; 3D cube 
;               
; Use       : IDL> ijkproj,cube,axis
;    
; Inputs    : 3D cube data, choice of axis
;               
; Outputs   : returns the 2D projection
;
; Keywords  : 
;               
; Calls from LASCO : none 
;
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 3d, density, cube, visualization
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRL, 2005-2009
;               
; $Log: ijkproj.pro,v $
; Revision 1.1  2009/04/22 18:32:53  antunes
; first half of relocation.
;
; Revision 1.1  2009/03/31 15:46:01  antunes
; More reorg
;
; Revision 1.1  2006/04/11 15:54:48  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.3  2006/03/16 20:25:27  antunes
; Improved pixon calling, wrote proto-frontend GUI for
; different rendering tests.
;
; Revision 1.2  2006/03/03 18:07:29  antunes
; More work on the axes problems.
;
; Revision 1.1  2006/01/09 17:14:42  antunes
; More commented added.
; New 'super3drv' driver routine for getting user specs for reconstruction
; process now checked in.  Just type 'super3drv' to test.
;
;-            


; simply sums 'projection' along each IDL i,j,k axes plane for
; plotting.  Tell it which 2 axes to sum as x,y, e.g.
;    img_ij=ijkproj(timage,'ij')
; 6 cases exist to define a cube: ij, ik, jk, kj, ji, ki

FUNCTION ijkproj,cube,axes

   if (n_elements(cube) eq 0) then return,fltarr(0,0)
   if (n_elements(axes) eq 0) then axes='ij'

   if (axes ne 'ij' and axes ne 'ik' and axes ne 'jk' and $
      axes ne 'kj' and axes ne 'ji' and axes ne 'ki') then begin
       print,"usage is: image=ijkproj(cube,axes='ij/ik/jk/kj/ji/ki'";
       return,fltarr(0,0)
   end

   N=(size(cube,/DIMENSIONS))[0]
   img=fltarr(N,N)

   for a1 = 0, N-1 do begin
       for a2 = 0, N-1 do begin

           case axes of
               'ij': img[a1,a2]=total(cube[a1,a2,*])
               'ik': img[a1,a2]=total(cube[a1,*,a2])
               'jk': img[a1,a2]=total(cube[*,a1,a2])
               'ji': img[a2,a1]=total(cube[a1,a2,*])
               'ki': img[a2,a1]=total(cube[a1,*,a2])
               'kj': img[a2,a1]=total(cube[*,a1,a2])
           endcase

       end
   end

   return, img

END
