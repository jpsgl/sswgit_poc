;+
;
; Project   : Densitycube, reconstruction, 3D, and s3drs software
;                   
; Name      : cube_los
;               
; Purpose   : used to manipulate 3D densitycubes and do 3D reconstructions
;               
; Explanation: boolean returns a cube where lines of sight blocked by
; the sun are set to zero
;               
; Use       : standalone or via s3drs GUI
;    
; Inputs    : N, rsun
;               
; Outputs   : los, loscount
;
; Keywords  : center, large, null
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : 3d, density, cubes
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; given a size in pixels/voxels N, a sun center in voxel space, and
; rsun in pixels, returns a boolean cube (byte array) where the
; everything blocked by the sun is masked out as a 0.
;
; If not given 'center', assumes it's a sun-centered cube
;
; Note you can get how many voxels are along the LOS simply with
;   loscount=total(los,3)
; You can also just return it by giving the loscount parameter.
;
; Optional /null flag simply returns an all-1 bytecube as a useful placeholder
;
; We support the 'large' array type (array of z pointers, each *,*)
; (in which case the lossum is for m=0,N-1 do loscount=loscount+*los(m))
;

PRO cube_los,N,rsun,los,loscount,center=center,large=large,null=null

  if (n_elements(N) eq 0 or n_elements(rsun) eq 0) then begin
      print,'usage is: cube_los,N,rsun,los,[loscount],[center=center])'
      return
  end
  if (n_elements(center) eq 0) then center=[(N-1)/2,(N-1)/2,(N-1)/2]

  large=KEYWORD_SET(large)
  null=KEYWORD_SET(null)

  ; LOS stops at sun surface
  ; go through z-slices and, for each 2D array, set circle
  ; make array 'los' of 1 or 0 indicating if it is valid

  if (large) then begin
      ; support for very large arrays stored in pointer slice format
      los=ptrarr(N,/allocate_heap)
      for m=0,N-1 do *los[m]=make_array(N,N,/byte,value=1)
  end else begin
      los=make_array(N,N,N,/byte,value=1)
  end
  loscount=make_array(N,N,/float,value=N)

  if (null) then return; early return

  rslice=make_array(N,N,/byte,value=1)

  zcenter=center[2]

  for rstep=0,rsun do begin

      zout = zcenter-rstep
      if (zout lt 0) then break
      rout = sqrt(rsun^2 - rstep^2)
      ;print,'solving for ',zout,rout

      rslice=rslice*0+1; reset to 1 each loop
      maskindices=cir_mask(rslice,center[0],center[1],rout)
      rslice(maskindices)=0; set inside sun to 0

      if (large) then *los[zout]=rslice else los[*,*,zout]=rslice

  end

  ; since that was the frontsize, note the backside is blocked off
  for m=zcenter+1,N-1 do $
    if (large) then *los[m]=*los[zcenter] else los[*,*,m]=los[*,*,zcenter]

  ; calculate loscount in case they want it
  loscount=loscount*0.0; set to empty
  if (large) then $
    for m=0,N-1 do loscount=loscount+ *los(m) else loscount=total(los,3)

END
