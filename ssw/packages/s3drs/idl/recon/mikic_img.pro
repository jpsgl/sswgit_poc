;+
;
; Project   : Densitycube, reconstruction, 3D, and s3drs software
;                   
; Name      : mikic_img
;               
; Purpose   : used to manipulate 3D densitycubes and do 3D reconstructions
;               
; Explanation: read and convert one of zoran mikic's rho-theta-phi images
; from polar coordinates to cartesian coordinates
; filenames are of the form rhoi###.dat
; legacy code from Paul Reiser
;               
; Use       : standalone or via s3drs GUI
;    
; Inputs    : fname, method, fnum, rr, tt, hh
;               
; Outputs   : 
;
; Keywords  : 
;               
; Common    : none
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : image cube
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            

; read and convert one of zoran mikic's rho-theta-phi images
; from polar coordinates to cartesian coordinates
; filenames are of the form rhoi###.dat

function mikic_img,fname,method=method,fnum=fnum,rr=rr,tt=tt,hh=hh

; enter filename or fnum to generate filename from CD
; rr,tt, and hh are the r, theta and phi for the cartesian cube

if(n_elements(fnum) ne 0)then begin
  if(fnum lt 0)then fname='/cdrom/cme_saic/rhoi'+string(-fnum,"(i3.3)")+'.hdf'
  if(fnum gt 0)then fname='/net/corona/cplex3/reiser/Tomography/rhoi'+string(fnum,"(i3.3)")+'.hdf'
end

if(n_elements(method) eq 0)then method=0
if(method ne 0)then goto, LL

; get r in solar radii, theta in radians, phi in radians

imr=read_mas_3d(filename=fname,r=r,theta=t,phi=h,name=name)

N=128

rr=make_array(N,N,N,/float,/noz)
tt=make_array(N,N,N,/float,/noz)
hh=make_array(N,N,N,/float,/noz)

mi = make_array(N,N,N,/float,/noz)
mj = make_array(N,N,N,/float,/noz)
mk = make_array(N,N,N,/float,/noz)

for i=0,N-1 do begin
  mi[i,*,*]=i
  mj[*,i,*]=i
  mk[*,*,i]=i
end
 
x = 60*(mi-0.5*(N-1))/N
xx = x*x
y = 60*(mj-0.5*(N-1))/N
yy = y*y
pp = xx+yy
p  = sqrt(xx+yy)
z = 60*(mk-0.5*(N-1))/N

rr=sqrt(pp+z*z)
tt=acos(z/rr)
hh=acos(x/p)

ii=findgen(100) 
ir=interpol(ii,r,rr)
it=interpol(ii,t,tt)
ii=findgen(129)
ih=interpol(ii,h,hh)

img=interpolate(imr,ir,it,ih)

q=where((rr le 1) or (rr gt 30),c)
if(c gt 0)then img[q]=0

return,img

;==========

LL: ;old way

imr=read_mas_3d(filename=fname,r=r,theta=t,phi=h,name=name)

N=128
img=fltarr(N,N,N)
rad=fltarr(N,N,N)


for i=0,N-1 do begin
  x = 60*(i-0.5*(N-1))/N
  xx = x*x
print,i,'/',N
for j=0,N-1 do begin
  y = 60*(j-0.5*(N-1))/N
  yy = y*y
  p  = sqrt(xx+yy)
for k=0,N-1 do begin
  z = 60*(k-0.5*(N-1))/N

  rr=sqrt(xx+yy+z*z)
  dif=abs(r-rr)
  ir=(where(dif eq min(dif)))[0]

  tt= acos(z/rr) 
  dif=abs(t-tt)
  it=(where(dif eq min(dif)))[0]

  hh= acos(x/p)
  dif=abs(h-hh)
  ih=(where(dif eq min(dif)))[0]

  img[i,j,k]=imr[ir,it,ih]

end
end
end

q=where((rr le 1) or (rr gt 30),c)
if(c gt 0)then img[q]=0

return,img

;==========

end
  

