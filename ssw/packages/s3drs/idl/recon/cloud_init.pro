;+
;
; Project   : Densitycube, reconstruction, 3D, and s3drs software
;                   
; Name      : cloud_init, cloud_compare, cloud_rot, cloud_fit,
; cloud_xi, cloud_run
;               
; Purpose   : forward modeling routine
;               
; Explanation: stand-alone forward modeling routine
;               
; Use       : standalone or via s3drs GUI
;    
; Inputs    : many
;               
; Outputs   : 
;
; Keywords  : many
;               
; Common    : cloudy
;               
; Restrictions: none
;               
; Side effects: saves the results in an output file
;               
; Category    : 3d, density, reconstruction, modeling
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; default is fit rotation and parameters in one go.
; can as 2 passes-- first, best rotation, then, best fluxrope.
; also can do additional passes later since I save these values.
;

PRO cloud_end
  ; this routine is an awkward hack to get around that the IDL Powell
  ; implementation does not do a maximum iterations.

  common cloudy,im1,im2,px,dat,sigma,mset,NL,rsun,pset,ivalid,flags,winid,maxit

  bestrot=pset[0:2]     ; just giving it a better name for the save file
  modparam=pset[3:*]  ; just giving it a better name for the save file
  rot_iterations=flags.iloop ; giving it a better name for the save file

  save,file=int2str(flags.mid,2)+'.sav',$
    bestrot,modparam,rot_iterations,im1,dat,sigma,mset,px

  print,"Ended at max iterations: ",bestrot,modparam,rot_iterations


  ; added hook for seperate reconstruction GUI
  common s3drs,s3drs
  if (n_elements(s3drs) ne 0) then begin
      s3drs_restore
      infowidget,"Note, fit not converged, no result saved.  Type 'retall' to resume control"
  end

  print,"Type 'retall' to resume control'"

  stop
END


FUNCTION cloud_compare,showme=showme,gif=gif

  common cloudy,im1,im2,px,dat,sigma,mset,NL,rsun,pset,ivalid,flags,winid,maxit
  gif=KEYWORD_SET(gif)

  renders=(pr_render(px,im1)*mset)

  if (maxit gt 0 and flags.iloop gt maxit) then cloud_end
  ;print,maxit,flags.iloop

  ;Calculate chi-squared
  ie=where(mset gt 0)

  ; memory-wasting awkwardness to manage potential use of 'log' flag
  if (flags.log) then begin
      rtemp=alog10(renders)
      dtemp=alog10(dat)*mset
  end else begin
      dtemp=dat*mset
      rtemp=renders
  end

  if (flags.mn eq 'n') then begin
      ; experimental morphology-only normalized ratio variant method
      rat=ratio( (dtemp[ie]/max(dtemp[ie])) - (rtemp[ie]/max(rtemp[ie])),$
                 sigma[ie]/max(sigma[ie]) )
      ;print,'mn = n'
  end else if (flags.mn eq 'r') then begin
      ; physically meaningful calculation
      rat=dtemp[ie]-rtemp[ie]
      ;print,'mn = r'
  end else if (flags.mn eq 's') then begin
      ; physically meaningful calculation
      rat=ratio(dtemp[ie]-rtemp[ie],sigma[ie])
      ;print,'mn = p'
  end else begin
      if (flags.iloop lt 2 ) then print,'No chisq chosen, using morpho="r"'
      rat=dtemp[ie]-rtemp[ie]
      ;print,'mn = r'
  end

  fnie=float(n_elements(ie))

  chi_squared = sqrt(total(rat^2))/(fnie^2-1.0)

  chisqdat = ratio( (dtemp-rtemp)^2,sigma^2)

  if (n_elements(flags.iloop) eq 0) then flags.iloop=0
  ++flags.iloop

  Nd=n_elements(dtemp[0,0,*])
  labels=[make_array(Nd,/string,value='             chisq'),$
          make_array(Nd,/string,value='             Render'),$
          make_array(Nd,/string,value='             Data')]

  tv_multi,winid=winid,/slide,line=Nd,/cap,/log,fixed=96,oom=2,$
    header='model '+int2str(flags.mid,2)+' , iteration '+int2str(flags.iloop),$
    [[[chisqdat]],[[rtemp]],[[dtemp]]],labels=labels,$
    file='m'+int2str(flags.mid,2)+'_'+int2str(flags.iloop)+'.gif',$
    gif=flags.saveslides,/unique

  if (gif) then write_gif,'compare_'+int2str(flags.mid,2)+flags.mn+'.gif',tvrd()

  if (n_elements(flags.cs) eq 0) then flags.cs=chi_squared; initialize this

  print,'Fit iteration: ',flags.iloop,', Chi2: ',chi_squared,$
    ', delta%: ',100*((chi_squared-flags.cs)/chi_squared)
  if (n_elements(showme) ne 0) then print,'  parameters = ',showme
  if (flags.saveslides) then begin
      if (flags.lun eq -1) then begin
          get_lun,lun
          flags.lun=lun
          print,'lun is ',flags.lun
      end
      openw,flags.lun,'m'+int2str(flags.mid)+'.log',/append
      printf,flags.lun,flags.iloop,chi_squared,showme
      close,flags.lun
  end

  flags.cs=chi_squared; save for next comparison

  if (maxit gt 0 and flags.iloop ge maxit) then begin
      print,'Maximum number of iterations reached'
      return,0.0                  ; try to force exit
  end else begin
      return,chi_squared
  end

END

FUNCTION cloud_rot,rotparam

  common cloudy,im1,im2,px,dat,sigma,mset,NL,rsun,pset,ivalid,flags,winid,maxit

  ; only need to build image once for rotation tests
  if (n_elements(im2) lt 4) then begin

      ; while we do not need modparam here ('buildcloud' will use defaults),
      ; it can be useful to generate here in case we manually wish to tweak it
      if (n_elements(pset) eq 0) then begin
          if (n_elements(flags.mid) eq 0) then flags.mid=54
          getmoddefparam,flags.mid,ss
          pset=parsemoddefparam(ss)
      end

      ;modparam[3]=modparam[3]/2.0; hack
      buildcloud,flags.mid,cubesidenbpix=NL,cubesidersun=rsun,output=1,$
        modparam=pset[3:*],/quiet
      im2=readcloudcube(flags.mid)


  end
  im1=im2; place cube sans all rotations
  cuberot3d,im1,rotparam

  chi_squared=cloud_compare(showme=rotparam)

  return,chi_squared

END

FUNCTION cloud_fit,tempparam

  common cloudy,im1,im2,px,dat,sigma,mset,NL,rsun,pset,ivalid,flags,winid,maxit

  ; insert pset2 into full pset

  for i=0,n_elements(ivalid)-1 do pset[ivalid[i]]=tempparam[i]

  buildcloud,flags.mid,cubesidenbpix=NL,cubesidersun=rsun,output=1,$
    modparam=pset[3:*],/quiet
  im1=readcloudcube(flags.mid)

;  cuberot3d,im1,modparam[0:2]
  cuberot3d,im1,pset[0:2]

  chi_squared=cloud_compare(showme=tempparam)

  return,chi_squared

end


FUNCTION cloud_xi,inputs

  nele=n_elements(inputs)
  mat=intarr(nele)
  for i=0,nele-1 do begin
      mat2=mat
      mat2[i]=1
      mat2=transpose(mat2)
      if (i eq 0) then xi=[mat2] else xi=[xi,mat2]
  end

  return,xi

END


; includes optional return structure
;     answer={bestrot,modparam,iterations,image,fmin}

PRO cloud_run,ftol=ftol,model=model,toggles=toggles,$
              morphonorm=morphonorm,log=log,saveslides=saveslides, $
              initial_rot=initial_rot,params=params,setuponly=setuponly,$
              answer=answer,parent=parent

  common cloudy,im1,im2,px,dat,sigma,mset,NL,rsun,pset,ivalid,flags,winid,maxit

  rotateonly=KEYWORD_SET(rotateonly)
  fitonly=KEYWORD_SET(fitonly)
  saveslides=KEYWORD_SET(saveslides)
  log=KEYWORD_SET(log)
  setuponly=KEYWORD_SET(setuponly)

  if (n_elements(morphonorm) eq 0) then morphonorm='r'
  flags.mn=morphonorm

  if (n_elements(parent) ne 0) then winid=parent

  if (n_elements(ftol) eq 0) then ftol=1.0E-4

  if (n_elements(model) ne 0) then flags.mid=model
  flags.saveslides=saveslides
  flags.log=log

  im2=0; null for initial setup
  fit_iterations=0

  if (n_elements(params) ne 0) then begin
      pset[3:*]=params
  end

  if (n_elements(initial_rot) ne 0) then begin
      pset[0:2]=initial_rot
  end

  if (n_elements(toggles) eq 0) then toggles=pset*0 + 1

  if (total(toggles[3:*]) eq 0) then fname="cloud_rot" else fname="cloud_fit"
  print,'Fitting using routine: ',fname

  ; extract only those parameters we want to explore
  ivalid=where(toggles ne 0)
  ; powell requires at least 2 adjustable parameters
  if (total(toggles) gt 1) then pset2=pset[ivalid] else begin
      print,'not enough valid variables'
      return
  end

  ; now make chisquare for those
  xi_uni=cloud_xi(pset2)

  if (setuponly) then begin

      bestrot=pset[0:2]
      modparam=pset[3:*]

      buildcloud,flags.mid,cubesidenbpix=NL,cubesidersun=rsun,$
        output=1,modparam=modparam,/quiet
      image=readcloudcube(flags.mid)
      cuberot3d,image,bestrot

      save,bestrot,modparam,image,dat,sigma,mset,px,$
        file=int2str(flags.mid,2)+'_in.sav'

      rot_iterations=0
      im1=0
      fmin=0

  end else begin
      ; usual runtime

      ; note there is no point running if itmax=1, that just means
      ; the problem should be instantiated but not iterated.
      if (maxit eq 1) then begin
          ; force just 1 instantiation
          fmin=cloud_fit(pset2)
      end else begin
          powell,pset2,xi_uni,ftol,fmin,fname;,itmax=itmax;itmax does not work!
      end

  ; now restore full parameter set
      for i=0,n_elements(ivalid)-1 do pset[ivalid[i]]=pset2[i]

      bestrot=pset[0:2] ; just giving it a better name for the save file
      modparam=pset[3:*] ; just giving it a better name for the save file
      rot_iterations=flags.iloop ; giving it a better name for the save file

      save,file=int2str(flags.mid,2)+'.sav',$
        bestrot,modparam,fmin,rot_iterations,toggles,im1,dat,sigma,mset,px

      print,"Done: ",fmin,fmin,bestrot,modparam,rot_iterations
  end

  answer={bestrot:bestrot,modparam:modparam,iterations:rot_iterations,$
          image:im1,fmin:fmin}

  if (flags.lun ne -1) then free_lun,flags.lun

END

PRO cloud_init,model,filename=filename,mxit=mxit,$
               px_in=px_in,dat_in=dat_in,sigma_in=sigma_in,mset_in=mset_in

  common cloudy,im1,im2,px,dat,sigma,mset,NL,rsun,pset,ivalid,flags,winid,maxit

  flags={mid:54,mn:'r',cs:0,iloop:0,saveslides:0,lun:-1,log:0}

  if (n_elements(model) eq 0) then model=54; 33

  flags.mid=model
  getmoddefparam,flags.mid,ss
  pset=parsemoddefparam(ss)
  print,'model ',flags.mid,' number of parameters: ',n_elements(pset)
  pset=[0,0,0,pset]; rotation plus model

  if (n_elements(mxit) ne 0) then maxit=mxit else maxit=-1

  if (n_elements(px_in) ne 0 AND n_elements(dat_in) ne 0 AND $
      n_elements(sigma_in) ne 0 AND n_elements(mset_in) ne 0) then begin

      ; ALL ARGS WERE PROVIDED
      ; because we use a common (ugh) we have to copy the data items
      if (DATATYPE(dat_in) eq 'PTR') then dat=*dat_in else dat=dat_in
      if (DATATYPE(sigma_in) eq 'PTR') then sigma=*sigma_in else sigma=sigma_in
      if (DATATYPE(mset_in) eq 'PTR') then mset=*mset_in else mset=mset_in
      if (DATATYPE(px_in) eq 'PTR') then px=*px_in else px=px_in

      NL=long(px.sr.ndata[0])
      rsun=NL * px.sr.l0tau[0]

  end else if (n_elements(filename) eq 0) then begin

      ; from a FILE

      restore,/v,filename
      dat=diff[*,*,0:1]
      sigma=ndiff[*,*,0:1]
      px=pxwrapper
      px.sr.Nd=2
      NL=long(px.sr.ndata[0])
      rsun=NL * px.sr.l0tau[0]

      mask=p_getmask('C3',sr=px.sr)
      mset=[[[mask]],[[mask]]]

  end else begin

      ; DEMO

      ; create a test synthetic case as a demo
      print,'No datafile give, generating sample test case'

      N=64
      prep_3drv,N=N,gamma=[0,90],model='fluxrope',px,/cor2
      NL=long(px.sr.ndata[0])
      rsun=NL * px.sr.l0tau[0]

      buildcloud,model,cubesidenbpix=NL,cubesidersun=rsun,output=1,$
        modparam=pset[3:*],/quiet
      timage=readcloudcube(model)

      cuberot3d,timage,[45,15,15]; give it an initial offset to try to fit
      dat=pr_render(px,timage)*p_getmask(sr=px.sr)
      conv=6*px.sr.dnperphoton[0]/px.sr.msbperdn[0]
      sigma=(sqrt(conv*dat))/conv

      mask=p_getmask('C3',sr=px.sr)
      mset=[[[mask]],[[mask]]]

  end

  print,"Now type 'cloud_run[,initial_rot=[90,90,0][,params=params][,mxit=40]'"

END

;samples for Dec 31 FMing
;  bestrot=[0,112.3,8.0]; Arnaud's lat=-22.3, rotation around axis=-82
;  modparam=[ 2.5 , 33.8*!dtor , 3.79 , 0.45 , 1e6 , 0 , 0 , 0 , 0.1 , 0.1 ]
;  filename='/home/antunes/MERCURY/best_dec31_cor2+c3/20071231_022200_128_20071231_002200_cor2_c3ex_maxocc_jumpstart_datacut_lascowt=2_wt=10_overmask=2c_inputdata.sav'  
;
;cloud_init,file=filename,54; fluxrope
;cloud_run,initial_rot=[0,112.3,8.0],params=[ 2.5 , 33.8*!dtor , 3.79
;, 0.45 , 1e6 , 0 , 0 , 0 , 0.1 , 0.1
;],toggles=[0,0,0,1,1,1,1,1,0,0,0,0,0]
; ;OR
;cloud_init,file=filename,51; halfshell
;cloud_run,initial_rot=[0,112.3-90,8.0],toggles=[0,0,0,1,1,1]
