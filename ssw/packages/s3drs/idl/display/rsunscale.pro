;+
;
; Project   : general display tools
;                   
; Name      : rsunscale
;               
; Purpose   : standalone, used as part of the s3drs GUI
;               
; Explanation: neatly plots units of Rsun for an arbitrary image
;               
; Use       : see documentation in code
;    
; Inputs    : xsize, ysize, x1, y1, rsun, csun,border
;               
; Outputs   : 
;
; Keywords  : corciel, nx, ny, color, xtitle, ytitle, fontsize, normal
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : display
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; Neatly plots units of Rsun for an arbitrary image,
; given a plot of size 'xsize,ysize' that has a 'border'
; and is cornered on ,'x1,y1' with a given plate scale of
; 'rsun' in pixels-per-rsun and a sun center in the image
; frame of 'csun'.
;
; default is /device units unless /normal is set

PRO rsunscale,xsize,ysize,x1,y1,rsun,csun,border,circle=circle,nx=nx,ny=ny,$
              color=color,xtitle=xtitle,ytitle=ytitle,fontsize=fontsize,$
              normal=normal

  circle=KEYWORD_SET(circle)
  normal=KEYWORD_SET(normal)

  ; see 'FONT system variable field' in IDL for the ! formatting options
  if (n_elements(xtitle) eq 0) then xtitle='x (R!D!9n!N!3)'
  if (n_elements(ytitle) eq 0) then ytitle='y (R!D!9n!N!3)'
  if (n_elements(fontsize) eq 0) then fontsize=1.0

  if (n_elements(color) eq 0) then color=64; an arbitrary default

  ; rsun is in (rescaled) pixels per rsun
  ; since images are square, rsun is for x and y
  ; set xyrange=plot min/max in units of rsun
  ; then set xytickv to integer rsun values in that range
  ; and xytick is how many of those values fit

  yrange=[0-(csun[1]/(1.0*rsun)),(ysize-csun[1])/(1.0*rsun)]
  xrange=[0-(csun[0]/(1.0*rsun)),(xsize-csun[0])/(1.0*rsun)]
  tny=fix(yrange[1])+1-fix(yrange[0]) ; e.g.if 0 to 2,ny=2+1-0=3
  tnx=fix(xrange[1])+1-fix(xrange[0]) ; e.g.if -2 to 2,nx=2+1-2=5
  yspan=indgen(tny) + fix(yrange[0])
  xspan=indgen(tnx) + fix(xrange[0])

 ; (ignore, old method) do in steps of 2 if too many elements
;  if (ny gt 8) then yspan=yspan(where(2*(yspan/2) eq yspan))
;  ny=n_elements(yspan)
;  if (nx gt 8) then xspan=xspan(where(2*(xspan/2) eq xspan))
;  nx=n_elements(xspan)

; reduce to just seven (or user-specifed nx/ny) rsun axis ticks for clarity
  if (n_elements(ny) eq 0) then ny=7
  yspan=congrid(yspan,ny)
  if (n_elements(nx) eq 0) then nx=7
  xspan=congrid(xspan,nx)

  ;e.g. yrange=[-5.3,10.2], yspan=[-4,0,4,8] if step2
  yhop=ysize/(yrange[1]-yrange[0])
  xhop=xsize/(xrange[1]-xrange[0])
  deltay = yhop*(yspan - yrange[0])
  deltax = xhop*(xspan - xrange[0])
  for iys=0,ny-1 do xyouts,x1-border,y1+deltay[iys],yspan[iys],alignment=0.5,/device,color=color,charsize=fontsize,normal=normal
  for ixs=0,nx-1 do xyouts,x1+deltax[ixs]-border/2,y1-border/2,xspan[ixs],alignment=0.5,/device,color=color,charsize=fontsize,normal=normal
;                  axis,x1-border,yaxis=1,/device,/noerase, $
;                    yminor=1,ystyle=9,yticklayout=1,/ynozero, $
;                    yrange=yrange,ytickv=yspan,yticks=ny
;                  axis,x1,y1-border,xaxis=1,/device,/noerase, $
;                    xminor=1,xstyle=9,xticklayout=1,/ynozero, $
;                    xrange=xrange,xtickv=xspan,xticks=nx


  xl=strlen(xtitle)/2
  yl=strlen(ytitle)/2
  xyouts,x1-3*border/4,y1-yl+ysize/2,ytitle,/device,orientation=90,color=color,charsize=fontsize,normal=normal
  xyouts,x1-xl+xsize/2,y1-border,xtitle,/device,color=color,charsize=fontsize,normal=normal

  if (circle) then $
    draw_circle,x1+csun[0],y1+csun[1],rsun,/device

END
