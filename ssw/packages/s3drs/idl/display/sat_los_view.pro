;+
; $Id: sat_los_view.pro,v 1.1 2009/04/22 18:31:56 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : sat_los_view
;               
; Purpose   : Puts up a tiny window showing polar view of satellite
; positions in a generalized sense
;               
; Explanation: used to visualize where satellites and FOVs are.
;               
; Use       : IDL> 
;    
; Inputs    : 
;               
; Outputs   : 
;
; Keywords  : 
;               
; Calls from LASCO : none 
;
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : display
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRL, Nov-Dec 2005
;               
; $Log: sat_los_view.pro,v $
; Revision 1.1  2009/04/22 18:31:56  antunes
; first half of relocation.
;
; Revision 1.2  2009/04/21 19:08:55  antunes
; Updated up through 'r' with SSW headers.  Also minor John bug fixes.
;
; Revision 1.1  2009/04/16 15:58:34  antunes
; Refactoring of util & pixon part 1, preparing for SSW packaging.
;
; Revision 1.15  2009/04/01 16:01:37  antunes
; GUI v 1.7, prettification, better error handling, more models.
;
; Revision 1.14  2008/11/13 16:25:09  antunes
; Checkin to CVS after hiatus.
;
; Revision 1.13  2008/07/01 19:21:33  antunes
; Checking in Pixon, finally.
;
; Revision 1.12  2008/06/16 18:22:39  antunes
; Minor mods.
;
; Revision 1.11  2008/06/06 18:40:22  antunes
; More tests and better plotting.
;
; Revision 1.10  2008/03/07 19:53:55  antunes
; One bug fix and several test cases, plus tools.
;
; Revision 1.9  2008/01/18 20:03:43  antunes
; More prettification, also one major structure pxwrapper.sr name change.
;
; Revision 1.8  2008/01/15 16:50:06  antunes
; Better units, more tests.
;
; Revision 1.7  2007/12/07 19:58:42  antunes
; Better tests, plotting, and numerics.
;
; Revision 1.6  2007/10/11 15:07:04  antunes
; Final pretty postscript.
;
; Revision 1.5  2007/08/29 18:55:07  antunes
; Greatly improved plotting and visualizations.
;
; Revision 1.4  2006/12/21 18:15:35  antunes
; Better pretty pictures, more test cases for sims.
;
; Revision 1.3  2006/10/20 17:58:40  antunes
; Lots of incremental mods to Pixon-related routines.
;
; Revision 1.2  2006/05/31 18:11:13  antunes
; Better plotting ranges.
;
; Revision 1.1  2006/04/11 15:55:07  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.8  2006/01/25 16:36:02  antunes
; Latest GUI mods, also renamed for clarity, also renamed some conflicting
; Reiser modules.
;
; Revision 1.7  2006/01/18 17:58:17  antunes
; Improved GUI.
;
; Revision 1.6  2006/01/13 16:15:19  antunes
; Improvements and changes to GUI tools.
;
; Revision 1.5  2006/01/12 18:57:00  antunes
; GUI additions, new layout, more functions.
;
; Revision 1.4  2006/01/10 19:55:00  antunes
; GUI for 3D RV
;
; Revision 1.3  2006/01/09 17:14:42  antunes
; More commented added.
; New 'super3drv' driver routine for getting user specs for reconstruction
; process now checked in.  Just type 'super3drv' to test.
;
;-            
;
; plots viewpoint, pointing cone, earth, FPA, and sun in sun-centered view
; including a circle for a 1AU orbit.  Can plot Cor1, Cor2, or EUVI fov.
;
; Location can be given in (default) polar coordinates (R, Rho)
; or X-Y (X, Y,/cart) coordinates.  Position of the earth in its orbit
; is given by 'utc', by 'earthrho', or defaults to 0 (indicating satellite
; position is relative to earth).  Can also include a plot of a
; nominal LASCO C2 (6 solar radii).
;
; utc=utc can be used to determine the Earth and STEREO A/B positions.
; If you give utc=, it automatically includes /plotearth and /fpa
;
; Can be invoked with overplot=1 to overplot additional pointing
; cones, e.g. los_view,r1,rho1,instr=cor2
;     followed by los_view,r2,rho2,instr=c2,overplot=1
;
; 'blank' means do not yet draw in a satellite fov, just the
; orbit/earth part of the drawing
;
; /fpa means include the label for First Point in Aries at deg=0
;
; /plotearth will plot the earth location (using earthrho= if
; provided, utc= if not, defaulting to 0 otherwise)
;
; embed= lets you embed the given wsize (default: 60) item into
; an existing display.  1=lower left corner, 2=lower right corner,
; 3=upper left corner, 4=upper right corner.  Also requires the
; 'parent' option or /ps option.
;
; /ps saves the item as postscript in sat_los_view.ps (default) or
; 'filename' if filename='filename' is given.  It uses landscape mode
; and hard-coded placement values.  /nocloseps will keep this file
; open (so additional pages can be written to it).
;
; /inverse will flip the colormap (bg=fg, fg=bg) to plot, then restore
;
; If given 'caption', will include a lot of text with it.
; A good usage is /ps,caption='whatever!Cetc'.  Right now,
; captioning only works accurately in the /ps mode.
;
; If given optional 'phiset', uses that angle above the plane to
; properly adjust the projected distance.
;
; /newwindow forces it to create a new window, rather than using the
; most recently created window

PRO sat_los_view,Rhoset,Rset,overplot=overplot,cart=cart,instr=instr,$
                 earthrho=earthrho,lasco=lasco,parent=parent,$
                 viewport=viewport,blank=blank,wsize=wsize,labelset=labelset,$
                 fpa=fpa,plotearth=plotearth,utc=utc,embed=embed,$
                 inverse=inverse,caption=caption,ps=ps,filename=filename,$
                 phiset=phiset,newwindow=newwindow,nocloseps=nocloseps,$
                 writegif=writegif

  overplot=KEYWORD_SET(overplot)
  cart=KEYWORD_SET(cart)
  lasco=KEYWORD_SET(lasco)
  blank=KEYWORD_SET(blank)
  fpa=KEYWORD_SET(fpa)
  plotearth=KEYWORD_SET(plotearth)
  inverse=KEYWORD_SET(inverse)
  ps=KEYWORD_SET(ps)
  writegif=KEYWORD_SET(writegif)
  nocloseps=KEYWORD_SET(nocloseps)
  newwindow=KEYWORD_SET(newwindow)
  ;whether to add an index to each position
  if (n_elements(labelset) eq 0) then dolabel=0 else dolabel=1
  if (n_elements(instr) eq 0) then instr='cor2'

  if ((ps or writegif) and n_elements(embed) eq 0) then $
    embed=2                     ; forces good size/alignment
  if (n_elements(embed) eq 0) then embed=0; default for non-ps
  if (embed ne 0 and n_elements(wsize) eq 0) then wsize=200

  ; if given a parent window to plot into, do so
  if (n_elements(viewport) ne 0) then WIDGET_CONTROL,viewport, GET_VALUE=parent

  if (n_elements(filename) eq 0) then begin
      filename='sat_los_view'
      if (writegif) then filename+='.gif' else filename+='.ps'
  end

  if (inverse) then flip_colors,/quiet; reverse

  if (ps) then begin

      current_device=!d.name
      set_plot,'PS'
      device,filename=filename,/color,bits_per_pixel=8,/landscape,$
        font_size=9,xsize=9.0,ysize=7.5,/inches,xoffset=0.5,yoffset=10.5
      wsize=6000

  end else if (n_elements(parent) ne 0) then begin

      cwin = !D.WINDOW
      clean_wset,parent,status=status
      if (status eq 0) then begin
         ; could not find asked-for window, making a new one
          window,/free,xsize=wsize,ysize=wsize
          parent=!D.WINDOW
      end

  end else if (n_elements(wsize) ne 0 or newwindow) then begin
      ; wants a new window of a specified size
      if (n_elements(wsize) eq 0) then wsize=400; a default size
      sizes=[wsize,wsize]
      if (writegif) then sizes=[800,500]
      window,/free,xsize=sizes[0],ysize=sizes[1]
  end

  if (n_elements(earthrho) eq 0) then earthrho=0
  REarth=1.0

  if (n_elements(utc) ne 0) then begin
      ; extract our elements
      fpa=1; since utc yields HAE we can include the FPA
      plotearth=1; 

      if (n_elements(Rhoset) eq 0) then Rhoset=dblarr(2) else Rhoset=[Rhoset,[0.0,0.0]]
      if (n_elements(Rset) eq 0) then Rset=dblarr(2) else Rset=[Rset,[0.0,0.0]]
      numrho=n_elements(Rhoset)
      numr=n_elements(Rset)

      ; get HAE longitude and distance
      hae_e  = get_stereo_lonlat(utc, 'A', /au, /degrees, system='HAE')
      if hae_e[1] lt 0 then hae_e[1] = hae_e[1] + 360.d0
      Rhoset[numrho-2]=hae_e[1]
      Rset[numrho-2]=hae_e[0]
      hae_e  = get_stereo_lonlat(utc, 'B', /au, /degrees, system='HAE')
      if hae_e[1] lt 0 then hae_e[1] = hae_e[1] + 360.d0
      Rhoset[numrho-1]=hae_e[1]
      Rset[numrho-1]=hae_e[0]
      hae_e  = get_stereo_lonlat(utc, 'Earth', /au, /degrees, system='HAE')
      if hae_e[1] lt 0 then hae_e[1] = hae_e[1] + 360.d0
      earthrho=hae_e[1]
  end

  polrec,REarth,earthrho,xEarth,yEarth,/DEGREES
  polrec,1.2,-90.0,xFPA,yFPA,/DEGREES

  ; set up a sample/example plot if none specified
  if(n_elements(Rhoset) eq 0) then Rhoset=[45]
  if(n_elements(Rset) eq 0) then Rset=make_array(n_elements(Rhoset),/float,value=1.0)

  bordersize=1.2
  cubex=[-bordersize,-bordersize,bordersize,bordersize,-bordersize]
  cubey=[-bordersize,bordersize,bordersize,-bordersize,-bordersize]


  ; NOW WE FINALLY INVOKE PLOTTING

  if (n_elements(caption) ne 0) then begin
      if (writegif) then $
        xyouts,0.05,0.9,caption,/normal,charsize=1.5 else $
        xyouts,200,16000,caption,/device,charsize=1.5

  end

  if (overplot ne 1) then begin
      if (embed gt 0) then begin
          xpos = !D.X_SIZE-wsize
          ypos = !D.Y_SIZE-wsize
          case embed of
              '1': pos=[0,0,wsize,wsize] ; lower left
              '2': pos=[xpos,0,xpos+wsize,wsize] ; lower right
              '3': pos=[0,ypos,wsize,ypos+wsize] ; upper left
              '4': pos=[xpos,ypos,xpos+wsize,ypos+wsize] ; upper right
              else: pos=[0,0,wsize,wsize]; default to lower left
          endcase
;          print,xpos,ypos,pos
          plot,cubex,cubey,xstyle=4,ystyle=4,XMARGIN=[0,0],YMARGIN=[0,0], $
            pos=pos,/device,/noerase
      end else begin
          plot,cubex,cubey,xstyle=4,ystyle=4,XMARGIN=[0,0],YMARGIN=[0,0]
      end

      if (plotearth) then xyouts,xEarth,yEarth,'E',alignment=0.5
;      xyouts,0,0,'!Uo!N',alignment=0.5
      xyouts,0,bordersize*1.05,'SECCHI views',alignment=0.5
      if (fpa) then xyouts,xFPA,yFPA,'FPA',alignment=0.5
      if (fpa) then oplot,[0,xFPA],[0,yFPA],linestyle=1,thick=0.5
      oplot_circle2,1,1
      oplot_circle2,1.0/215.0,0;  1 AU is around 215 Rsun
  end

  iters=n_elements(Rset)-1
  if (blank) then iters=-1; 'blank' surpressed FOV plots
  for i=0,iters do begin
      R=Rset[i]

      Rho=Rhoset[i]-90.0

      if (n_elements(phiset) ne 0) then begin
          ; adjust for 2D projection of 3D position
          phi=phiset[i]
          R=R * cos(phi/!radeg)
      end

      if (cart eq 1) then begin
          xView=R
          yView=Rho
          recpol,R,Rho,RView,RhoView,/DEGREES
      end else begin
          RView=R
          RhoView=Rho
          polrec,RView,RhoView,xView,yView,/DEGREES
      end

      case instr of
          'cor1': roff=4*0.0047 ; 1.25-4 solar radii out, sun-centered (in AU)
          'cor2': roff=15*0.0047 ; 2-15 solar radii sun-centered (in AU)
          'euvi': roff=1.7*0.0047 ; 0-1.7 solar radii out, sun-centered (in AU)
          else:   roff=15*0.0047 ; default is cor2
      endcase

                                ; now create an FOV cone for viewing
      polrec,roff,RhoView+90,x1,y1,/DEGREES
      polrec,roff,RhoView-90,x2,y2,/DEGREES

      xCone=[x1,xView,x2]
      yCone=[y1,yView,y2]

      oplot,xCone,yCone

      if (dolabel) then begin
          polrec,(RView*1.1),RhoView,xViewL,yViewL,/DEGREES
          if (i lt n_elements(labelset)) then begin
              ; use given label
              ll=labelset[i]
          end else begin
              ; default label is just number
              ll=strtrim(string(i+1),2)
          end
          xyouts,xViewL,yViewL,ll,/data,alignment=0.5
      end

  end

  if (lasco eq 1) then begin
                                ; now create an FOV cone for viewing
      C2ConeR=6*0.0047          ; 6 solar radii, sun-centered, in AU
      polrec,C2ConeR,earthrho+90,C2x1,C2y1,/DEGREES
      polrec,C2ConeR,earthrho-90,C2x2,C2y2,/DEGREES
      C2xCone=[C2x1,xEarth,C2x2]
      C2yCone=[C2y1,yEarth,C2y2]
      oplot,C2xCone,C2yCone
  end


  if (ps eq 1 and nocloseps eq 0) then begin
      device,/CLOSE_FILE
      set_plot,current_device
  end else if (writegif) then begin
      write_gif,filename,tvrd()
  end

  parent=!D.WINDOW; save just in case it is needed
  if (n_elements(cwin) ne 0) then cwin = !D.WINDOW

  if (inverse) then flip_colors,/quiet; restore

END
