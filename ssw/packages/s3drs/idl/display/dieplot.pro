;+
;
; Project   : general display tools
;                   
; Name      : dieplot
;               
; Purpose   : standalone, used as part of the s3drs GUI
;               
; Explanation: wrapper for 'threeview' that forces 6-sided plot mode
;               
; Use       : see documentation in code
;    
; Inputs    : (same as threeview via _EXTRA)
;               
; Outputs   : (same as threeview via _EXTRA)
;
; Keywords  : (same as threeview via _EXTRA)
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : display
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; wrapper for 'threeview' that just does 6-sided 3D cube plots

PRO dieplot,image,datum,_EXTRA=ex

  threeview,image,datum,_EXTRA=ex,/dieplot

END
