PRO oplot_circle2, radius, linestyle
;+
; $Id: oplot_circle2.pro,v 1.1 2009/04/22 18:31:54 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : oplot_circle2.pro
;               
; Purpose   : Overplots a circle of given radius and linestyle onto a graph
;               
; Explanation: update of an older non-SSW routine, adding linestyle option
;               
; Use       : IDL> oplot_circle2,radius,linestyle
;    
; Inputs    : radius : radius of circle to plot
;             linestyle : type of line (0=solid, 1=dotted, etc)
;               
; Outputs   : none
;
; Keywords  : 
;               
; Calls from LASCO : none 
;
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: adds a circle to the existing plot
;               
; Category    : Plotting
;               
; Prev. Hist. : Based on 'oplot_circle.pro'
;
; Written     : modified by Sandy Antunes, NRL, Nov-Dec 2005
;               
; $Log: oplot_circle2.pro,v $
; Revision 1.1  2009/04/22 18:31:54  antunes
; first half of relocation.
;
; Revision 1.1  2009/04/16 15:58:33  antunes
; Refactoring of util & pixon part 1, preparing for SSW packaging.
;
; Revision 1.1  2006/04/11 15:55:06  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.2  2005/12/27 17:48:24  antunes
; Commented half of the routines using SSW style.  Also added a new
; axes test case 'arrowcube'.
; Checked in the Tomography/old/mikic_img.pro routine here because it
; is a dependency for the general image-prep routine 'get_timage.pro'
;
;-            
; adapted from $SSW/radio/nrh/idl/gen/oplot_circle.pro
; to allow overplotting a dotted circle, not just a lined one

  IF n_elements(radius) eq 0 THEN radius=1.0
  IF n_elements(linestyle) eq 0 THEN linestyle=0
  ipts=101
  rayon=fltarr(ipts)
  delta=2.*!PI/(ipts-1)

  angle = FINDGEN(ipts)*delta
  rayon(*) = radius

  oplot, rayon, angle,/polar,linestyle=linestyle
END
