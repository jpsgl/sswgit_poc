PRO flip_colors,quiet=quiet
;+
; $Id: flip_colors.pro,v 1.1 2009/04/22 18:31:53 antunes Exp $
;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : flip_colors.pro
;               
; Purpose   : switch background/foreground black/white or white/black
;               
; Explanation: usage automatically changes white/black to black/white
;               or vis versa
;               
; Use       : IDL> flip_colors
;    
; Inputs    : /quiet: optional, turns off notification of flipping
;               
; Outputs   : none
;
; Keywords  : none
;               
; Calls from LASCO : none 
;
; Common    : none 
;               
; Restrictions: none
;               
; Side effects: changes global P.BACKGROUND and P.COLOR
;               
; Category    : Plotting
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRL, Nov-Dec 2005
;               
; $Log: flip_colors.pro,v $
; Revision 1.1  2009/04/22 18:31:53  antunes
; first half of relocation.
;
; Revision 1.1  2009/04/16 15:58:32  antunes
; Refactoring of util & pixon part 1, preparing for SSW packaging.
;
; Revision 1.3  2009/04/01 16:01:35  antunes
; GUI v 1.7, prettification, better error handling, more models.
;
; Revision 1.2  2007/08/29 18:55:07  antunes
; Greatly improved plotting and visualizations.
;
; Revision 1.1  2006/04/11 15:55:06  antunes
; Massive re-org of cvs 'dev' preparatory to moving into solarsoft.
;
; Revision 1.2  2005/12/27 17:48:23  antunes
; Commented half of the routines using SSW style.  Also added a new
; axes test case 'arrowcube'.
; Checked in the Tomography/old/mikic_img.pro routine here because it
; is a dependency for the general image-prep routine 'get_timage.pro'
;
;-            
; idiotic routine that changes the background/foreground colors.
; if !P.BACKGROUND is white, it sets it black and P.COLOR white,
; if !P.BACKGROUND is black, it sets it white and P.COLOR black,
; if !P.BACKGROUND is neither, it does nothing.

  quiet=KEYWORD_SET(quiet)

  nc = !D.N_COLORS-1
  if (!P.BACKGROUND eq 0) then begin
      !P.BACKGROUND = nc
      !P.COLOR = 0
      if (quiet eq 0) then print,'Setting background to black'
  end else if (!P.BACKGROUND ge nc) then begin
      !P.BACKGROUND = 0
      !P.COLOR = nc
      if (quiet eq 0) then print,'Setting background to white'
  end else begin
      if (quiet eq 0) then print,'No change.'
  end

  if (quiet eq 0) then print,'background: ',!P.BACKGROUND,', foreground: ',!P.COLOR

END
