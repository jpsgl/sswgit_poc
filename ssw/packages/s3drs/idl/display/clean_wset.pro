;+
;
; Project   : general display tools
;                   
; Name      : clean_wset
;               
; Purpose   : standalone, used as part of the s3drs GUI
;               
; Explanation: wrapper for 'wset' that exits gracefully if window does
; not exist.
;               
; Use       : see documentation in code
;    
; Inputs    : wid
;               
; Outputs   : 
;
; Keywords  : status=status
;               
; Common    : 
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : display
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRC, March-April 2009
;               
;-            
; handler for 'wset' that, if the chosen window does
; not exist, gracefully ignores the 'wset' command instead
; of erroring out.  Also returns optional 'status' indicating
; success (1) or failure (-1)

pro clean_wset,wid,status=status

   status=0; start off presuming failure
   catch,ignore_error
   if (ignore_error eq 0) then begin
       wset,wid
       status=1; success
   end
   catch,/cancel

end
