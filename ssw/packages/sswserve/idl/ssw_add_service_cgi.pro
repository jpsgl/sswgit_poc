pro ssw_add_service_cgi, service_name, _extra=_extra, $
   cgidir=cgidir, replace=replace

if not data_chk(service_name,/string) then begin 
   box_message,'Need to supply service name'
   return
endif

cgienv=get_logenv('ssw_cgi_dir')
case 1 of
   data_chk(cgidir,/string):  ; user supplied via keyword
   cgienv ne '': cgidir=cgienv ; via enronmental
   else: cgidir='/www1/cgi-diapason ; 1st 
endcase

if not file_exist(cgidir) then begin 
   box_message,'Cannot find cgi directory> ' + cgidir +' , aborting'
   return
endif

if data_chk(_extra,/struct) then begin 
   servstring='ssw_'+strlowcase((tag_names(_extra))(0)) + '_service_'
endif else servstring='ssw_service_'

if not data_chk(template,/string) then $
   template=concat_dir(cgidir,servstring+'xxx.sh')

newcgi=concat_dir(cgidir, $
   servstring + str_replace(service_name, servstring,'')+'.sh')

if file_exist(newcgi) and 1-keyword_set(replace) then begin 
   box_message,'service cgi > ' + newcgi + ' exists, use /replace'
   return
endif

if not file_exist(template) then begin 
   box_message,'cgi template > ' + template(0) + ' does not exist, returning..'
   return
endif

cgidat=rd_tfile(template)
ssserv=(where(strpos(cgidat,'=xxx') ne -1))(0)
cgidat(ssserv)=str_replace(cgidat(ssserv),'=xxx','='+service_name(0))

file_append,newcgi(0),cgidat,/new
spawn,['chmod','755',newcgi(0)],/noshell

return
end
