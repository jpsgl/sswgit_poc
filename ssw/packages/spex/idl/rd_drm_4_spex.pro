;+
; PROJECT: SDAC
;
; NAME: rd_drm_4_spex
;
;
; PURPOSE: read drm files, match output bins to data edges
;
;
; CATEGORY: i/o, spectral analysis
;
;
; CALLING SEQUENCE:
;	rd_drm_4_spex, drm_to_read=drm_to_read, scatter_to_read=scatter_to_read, data_tipe=data_tipe,$
;		edges_out_drm=edges_out_drm, p_drm_reader=p_drm_reader, e_in=e_in, drm_out=drm, area=area
;
; INPUTS:
;       drm_to_read - direct detector response matrix filename
;	scatter_to_read - additional optional scattering matrix filename, '' if none
;	data_tipe  - specifies spacecraft/instrument (0), detector (1)
;			e.g. BATSE CONT or
;			     BATSE SHERS
;		     others allowed if reader is specified in p_drm_reader
;	edges_out  - output channels edges in keV (2xm)
;	date       - time of observation (optional)
; OPTIONAL INPUTS:
;	p_drm_reader - procedure name to read drm for other data_tipe
;
; OUTPUTS:
;       drm - response matrix counts/cm2/keV per photon/cm2
;	e_in - channel edges (2xn) of incident photon bins
;	area - normal geometric detector area 127 cm2 for BATSE SPECs and 2025 cm2 for LADs
;	error - 0 is returned if files are found, 1 returned if files aren't found
; Note:
;	Edges_out - lower and upper limits are forced to be within the low and upper limits
;		   of the output edge of the drm read from the file!
; MODIFICATION HISTORY:
;	ras, 27-dec-93
;	ras, 12-mar-94	added error keyword
;	ras, 25-may-94  added yohkoh hxs and grs1, grs2
;	ras, 7-july-1996, restricted range on SXS drms
;	Version 5, richard.schwartz@gsfc.nasa.gov, 8-aug-1997, changed path to sxs_response.dat
;	Version 6,
;	richard.schwartz@gsfc.nasa.gov, 29-sep-1997, moved all non-spex
;	calls to call_procedure.
;	Version 7
;	richard.schwartz@gsfc.nasa.gov, 24-oct-1997, cleaned bugs from version 6 for YOHKOH SXS.
;	Version 8 18-feb-2001 added _extra and passed to p_drm_reader

pro rd_drm_4_spex, drm_to_read=drm_to_read, scatter_to_read=scatter_to_read, data_tipe=data_tipe,$
	edges_out_drm=edges_out_drm, p_drm_reader=p_drm_reader, e_in=e_in, drm_out=drm, area=area, $
	date=date, _extra=_extra, error=error

error = 0	;assume no error

	if strupcase(data_tipe(0)) eq 'BATSE'  THEN BEGIN
		;Check for files!
	  if drm_to_read ne '' then begin
		fdrm = findfile(drm_to_read, count=findcount)
		if findcount ne 1 then begin
		  printx,string(/print,'Cannot find just one direct DRM file for ',data_tipe)
		  printx,string(/print,'Looking for:', drm_to_read,' Found: ',fdrm)
		  error=1
		  return
		endif
	   endif
	  if scatter_to_read ne '' then begin
		fscatter = findfile(scatter_to_read, count=findcount)
		if findcount ne 1 then begin
		  printx,string(/print,'Cannot find just one SCATTER DRM file for ',data_tipe)
		  printx,string(/print,'Looking for:', scatter_to_read,' Found: ',fscatter)
		  error=1
		  return
		endif
	   endif
	  IF strupcase(data_tipe(1)) eq 'CONT' or strupcase(data_tipe(1)) eq 'HERS' THEN $
	    drm = call_function("lad_drm", chan=edges_out_drm, e_in=e_in, filename=drm_to_read, $
		scatterfile=scatter_to_read )
	  if strupcase(data_tipe(1)) eq 'SHERS' THEN begin
		call_procedure,"read_bdas_drm", file=drm_to_read, drm=drm_in, $
                	head=hd, area=area
        	if scatter_to_read ne '' then begin
		  call_procedure,"read_bdas_drm", file=scatter_to_read, drm=sdrm,$
                	head=shd, area=area
        	  call_procedure,"add_scatter_drm", ddrm= drm_in, sdrm= sdrm, new_drm=drm_in,$
                	shead = shd, dhead=hd, nsmooth=1
                endif
		  in_e_out = hd.e_out(*,0:hd.nobins_out-1)
		  edges_out_drm = edges_out_drm < max(e_out) > min(e_out)

        	drm=  x_eout_drm(in_drm=drm_in,out_e_out=edges_out_drm, $
                        in_e_out=in_e_out, nsub=10)
                e_in = hd.e_in(*,0:hd.nobins_in-1)
	  endif
	endif else begin
	if strupcase(data_tipe(0)) eq 'YOHKOH'  THEN BEGIN
	   case 1 of
	    strpos(strupcase(data_tipe(1)),'SXS') eq 0: begin
		if strpos(data_tipe(1),'1') ne -1 then sxs1=1 else sxs1=0
		sxs2 = 1 - sxs1 ;logicals for desired data
		dfile = fcheck(drm_to_read,'')
                if strpos(strupcase(fcheck(drm_to_read,'')),'SXS') eq 0 $
			then dfile_test = findfile( drm_to_read,count=ncount)
		if fcheck(ncount,0) eq 0 then begin
		  path=[curdir(),'DIR_HXT_CAL','PERM_DATA','SSWDB_SPEX']
		  dfile = 'sxs_response.dat'
		  dfile_test = loc_file('sxs_response.dat',path=path, count=ncount)
		endif
		if ncount eq 0 then begin
			printx,string(/print,'Error, SXS response file, ',dfile, $
		' not online in '+arr2str(path,' '))
			error = 1
			return
		endif
		edges = 0.
		sxs1a = 0.
		sxs1b = 0.
		sxs2a = 0.
		sxs2b = 0.
		restore, dfile_test(0)
		if sxs1 then begin
			area = 11.95
			drm_in = (sxs1a + sxs1b)/area
		endif else begin
			area = 6.18
			drm_in = (sxs2a+ sxs2b)/area
		endelse
		edge_products, edges, edges_2=e_in
		winput = where(e_in(0,*) lt max(edges_out_drm)+5., ninput)
		e_in = e_in(*,winput)
		drm_in = drm_in(*,winput)
        	drm=  x_eout_drm(in_drm=drm_in,out_e_out=edges_out_drm, $
                        in_e_out=e_in, nsub=10)
		use_vlth,/set
		printx,string(/print,'SETTING DEFAULT EDGES FOR VLTH.PRO',vlth(edges=e_in))
	   end
	    strpos(strupcase(data_tipe(1)),'HXS') eq 0 or $
		strpos(strupcase(data_tipe(1)),'GRS') eq 0 : begin
		call_procedure, "wbs_response", strlowcase(data_tipe(1)), date=date, drm=drm, area=area, $
			edges_out=edges_out_drm, edges_in=e_in
	   end
	endcase
        endif else begin
        ;if another data type, construct the name of the reader if not passed
        break_file, drm_to_read, fdisk, fdir, fnam, fext
        if strlowcase( fext ) eq '.genx' then genx = 'genx' else genx = ''
		checkvar, p_drm_reader, strlowcase(data_tipe(0)+data_tipe(1)+'drm'+ genx)
		p_drm_reader_chk = p_drm_reader
		chkarg, p_drm_reader_chk, found=found, /search
		if found then call_procedure, p_drm_reader, file=drm_to_read, sfile=scatter_to_read, $
			edges_out=edges_out_drm, edges_in=e_in, area=area, drm=drm
	endelse
	endelse



end
