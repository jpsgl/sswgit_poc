;+
;
; NAME:
;	SPEX_COMMONS
;
; PURPOSE:
;	Data and Option storage common blocks for SPEX
;
; CATEGORY:
;	SPEX
;
; CALLING SEQUENCE:
;	Not a procedure, code to be included using "@spex_commons"
;
; CALLS:
;	none
;
; INPUTS:
;       none explicit, only through commons;
;
; OPTIONAL INPUTS:
;	none
;
; OUTPUTS:
;       none explicit, only through commons;
;
; OPTIONAL OUTPUTS:
;	none
;
; KEYWORDS:
;	none
; COMMON BLOCKS:
;	SPEX_PROC_COM - Input and Processed Data Arrays
;	SPEX_SETUP_COM- Control Options
;	SPEX_HOLD_COM - Used for merging data input streams
;
; SIDE EFFECTS:
;	none
;
; RESTRICTIONS:
;	none
;
; PROCEDURE:
;	Common blocks only, used in SPEX_PROC, SPEX_CURRENT, SPEX_SAVE
;				    SPEX_SAVE_DATA, SPEX_CURRENT, SPEX
;				    OPTION_CHANGER, SPEX_SOURCE, HELP_MERGE
;				    SPEX_DELETE, SPEX_MERGE, SPEX_HOLD
;
; MODIFICATION HISTORY:
;	Documented ras, April 1996
;	Version 2, ras, Added HANDLES, 3 April 1996, last_plot
;
;	Version 3, ras, Added HANDLES, 15 May 1996, added p_read_data and
;	moved p_drm_reader
;	Version 4, ras, 18-Jul-1997, added erange and flare
;	19-mar-02, ras, pyb, added spex_debug, spex_obj to
;		support catch and objects
;-


;
;COMMON BLOCKS FOR WSPEX.PRO AND SPEX_PROC.PRO
;
common spex_proc_com, $
  cosine, $
  det_id,  $
  ut, $
  flux, $
  eflux, $
  ltime, $
  rate, $
  area, $
  datamode, $
  title, $
  count_2_flux, $
  uflux, $
  erate, $
  handles, $
  last_plot, $
  back, $
  eback, $
  wback, $
  tb, $
  def_tb, $
  xselect, $
  iselect, $
  wint, $
  wuse, $
  edges, $
  delta_light, $
  edg_units, $
  e_in, $
  eff, $
  drm, $
  obsi, $
  convi, $
  eobsi, $
  backi, $
  ebacki, $
  live, $
  start_time_data, $
  end_time_data,$
  iegroup,  $
  range, $
  apar, $
  apar_arr, $
  apar_last, $
  apar_sigma, $
  chi,  $
  batse_burst, $
  erange, $
  flare, $
  spex_debug,$
  spex_obj

common spex_setup_com, $
  noreadinput, $
  style_sel, $
  setup_sys, $
  prompt, $
  nolist, $
  comments, $
  dflts_file, $
  files, $
  families, $
  parameters, $
  prm_spex, $
  prm_spex_sav, $
  prm_spex_info, $
  tavg, $
  uncert, $
  y_spectitle, $
  script_opt1, $
  script_opt2, $
  standard_types, $
  back_order,  $
  backscale, $
  bdata, $
  psfile, $
  use_band, $
  dir_queue, $
  need, $
  last_found, $
  last_read, $
  wtitles, $
  gaction, $
  sensitive, $
  p_drm_reader, $
  p_read_data, $
  gdaction, $
  goaction,  $
  plotbuffer, $
  bang_enter, $
  nowidget, $
  sher_only, $
  merge_list, $
  mscript

;this common block will allow merging of data_tipes
common spex_hold_com, $
  flux_h, $
  eflux_h, $
  edges_h, $
  count_2_flux_h, $
  delta_light_h, $
  e_in_h, $
  det_id_h, $
  ut_h, $
  drm_h, $
  ltime_h, $
  area_h, $
  source_h, $
  source, $
  insource, $
  insource_h, $
  edges_drm, $
  edges_drm_h, $
  more_types
;
;
