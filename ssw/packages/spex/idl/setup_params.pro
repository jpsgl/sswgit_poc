;+
;
; NAME:
;	setup_params
;
; PURPOSE:
;       intialize parameters and command strings for SPEX
;
; CATEGORY:
;       initializations
;
; CALLING SEQUENCE:
;   	setup_params, params, fam_info
;
; CALLED BY:
;       SPEX
;
; INPUTS:
;       none
;
; OUTPUTS:
;       params - parameter structures with family, type, initial value, range info
;	fam_info - structure which allocates command strings to FAMILY menus
;
; MODIFICATION HISTORY:
;	akt
;	ras, 19-may-94 added command string 'reset_event' and parameter nback
;	ras, 03-aug-94 added command string 'restore_event','save_event'
;	ras, 25-aug-94 added command string 'pspreview'
;	RAS, 4-mar-95, added test_drm option
;	RAS, 14-mar-95, added cont_pendleton option
;	RAS, 28-mar-95, added controls for recalibration
;	ras, 2-nov-95, changed defaults to test_drm,2 and cont_pendleton,1
;	Version 2, ras, 8-April-1996, changed str name to parm_structure2
;	Version 10, ras, 30-apr-1997, changed back_order max to 3
;	Version 11, richard.schwartz@gsfc.nasa.gov, 21-aug-1997, default
;	on cont edges in set_pendleton is 2, Preece mod of Lestrade!
;	Version 12, richard.schwartz@gsfc.nasa.gov, 1-oct-1997,
;	removed @flare_catalog
;	PB, 21-Jun-00, added call to chk_batse_env and error checking to allow start-
;		up to proceed without calls to read_flare, etc.
;-

pro setup_params, params, fam_info


if (size(params))(2) eq 8 then $
   if tag_names(params, /structure_name) eq 'PARAM_STRUCTURE2' then goto, getout


;call_procedure,"read_flare", 665, fld665
; make sure we can initialize the parameters with batse data
chk_batse_env, err_msg, file

if err_msg[0] ne '' then begin
	err_msg=['', $
			'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', $
			err_msg, $
			'Using dummy values of UTS and UTE.', $
			'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', $
			'']

	for i=0, n_elements(err_msg)-1 do printx, err_msg[i]

	get_utc, utc
	UTS= systime(1) - julday(1,1,1979,0.,0.,0.) + utc.time
	UTE= UTS + 600.d0
endif else begin
	call_procedure, file, 665, fld665
	UTS=atime(fld665.start_secs)
	UTE=atime(fld665.start_secs + fld665.duration)
endelse

create_family, fam_info, 'main', 'Spectral Analysis Executive  (SPEX)', $
	['/time_history', '/interval', '/drm', '/fitting', '/summary', $
	'batse_menu', 'merge_batse','find_files', 'online?', $
        'hard', 'pspreview','erase', 'tek', $
        'restore_dflts', 'save_dflts', 'steps_help', 'comments', $
	'preview', 'reset_event', 'execute', 'zoom', 'list', 'kleanplot', $
	'initialize', 'exit', 'stop', '?', 'script', 'idl']

create_param, params, $
   'START_TIME_DATA', 'main', 'string', '', 1, -999., -999., ''

create_param, params, $
   'END_TIME_DATA', 'main', 'string', '', 1, -999., -999., ''

create_param, params, $
   'QUIET', 'main', 'fix', '1', 1, 0, 1, ''

create_param, params, $
   'CHECK_DEFAULTS', 'main', 'fix', '1', 1, 0, 1, ''

create_param, params, $
   'DFLTS_FILE', 'main', 'string', 'spex_dflts.dat', 1, -999.,  -999., ''

create_param, params, $
   'DATA_TIPE', 'main', 'string', ['BATSE', 'CONT'], 2, -999.,  -999., ''

create_param, params, $
   '_1FILE', 'main', 'string', ' ', 1, -999.,  -999., ''

create_param, params, $
   '_2FILE', 'main', 'string', ' ', 1, -999.,  -999., ''

create_param, params, $
   'DIRECTORY', 'main', 'string', ' ', 1, -999.,  -999., ''

create_param, params, $
   'FLARE', 'main', 'long', '665', 1, 0, 20000, ''

create_param, params, $
   'DET_ID', 'main', 'fix', '1', 1, 0, 7, ''

create_param, params, $
   'COSINE', 'main', 'float', '0.', 1, -1., 1., ''

create_param, params, $
   'SOURCE_RADEC', 'main', 'float', string([0.,0.]), 2, -90., 360., ''


create_param, params, $
   'PSSTYLE', 'main', 'string', 'PORTRAIT', 1, -999., -999., $
   ['PORTRAIT', 'LANDSCAPE','FULLPORTRAIT','GIF']


create_family, fam_info, 'time', 'Time plotting parameters:', $
	['/', 'time_history', 'bspectrum', $
        'display_intervals', 'background_interval', 'graph', $
        'accum', 'spec_bands', 'nospec_bands', 'clear_ut', 't_hist_mode',$
        'create_ps', 'pspreview','hard', 'erase', 'tek', 'zoom', 'list', 'kleanplot']



create_param, params, 'UTS', 'time', 'string', UTS, 1, -999., -999., ''

create_param, params, 'UTE', 'time', 'string', UTE, 1, -999., -999., ''

create_param, params, $
   'T_HIST_MODE', 'time', 'string', 'FLUX', 1, -999., -999., ['FLUX', 'COUNTS']

create_param, params, $
   'TAVG', 'time', 'fix', '1', 1, 1., -999., ''

create_param, params, $
   'ENERGY_BANDS', 'time', 'float', string([25.,35.,35.,50.,50.,200.,200.,500.]), $
   8, 1., 20000., ''
create_param, params, $
   'BACK_ORDER', 'time', 'fix','1,1,1,1,1',5, 0, 3, ''	;order of polynomial for background fit

create_param, params, $
   'SCALE_BANDS', 'time', 'float', string([1.,2.,4.,8.]), 4, -999., -999., ''

create_param, params, $
   'EPLOTS', 'time', 'fix', '0', 1, 0., 1., ''

create_param, params, $
   'TH_YTYPE', 'time', 'fix', '0', 1, 0., 1., ''

create_param, params, $
   'TH_YRANGE', 'time', 'float', string([0.,0.]), 2, -999., -999., ''



create_family, fam_info, 'inte', 'Interval Selection parameters:', $
	['select_interval', 'list', '/']


create_param, params, $
   'IAVG', 'inte', 'fix', '1', 1, 1., -999., ''


create_family, fam_info, 'fit', 'Spectral Plot and Fit Parameters:', $
	['/', 'fitting', 'count_spectrum', 'photon_spectrum', $
        'ch_bands', 'enrange', 'force_apar', 'create_ps', 'pspreview','hard', 'erase', $
	'tek', 'zoom', 'list', 'kleanplot']


create_param, params, $
   'F_MODEL', 'fit', 'string', 'f_vth_bpow', 1, -999., -999., ''

create_param, params, $
   'APAR', 'fit', 'float', string([1.0e-5,1.0,3.92,2.66,400.,1.93]), $
   6, -999., -999., ''

create_param, params, $
   'FREE', 'fit', 'fix', string([0,0,1,1,0,0]), 6, 0., 1., ''

create_param, params, $
   'UNCERT', 'fit', 'float', '0.05', 1, 0., -999., ''

create_param, params, $
   'IFIRST', 'fit', 'fix', '0', 1, 0., -999., ''

create_param, params, $
   'ILAST', 'fit', 'fix', '0', 1, 0., -999., ''

range = reform([[1e-7,0.5,1e-10,1.,12.,1.],$
               [1e3,20.,1e10,12.,1500.,12.]],6,2)
create_param, params, $
   'RANGE_LO', 'fit', 'float', string(range(*,0)), 6, -999., -999., ''

create_param, params, $
   'RANGE_HI', 'fit', 'float', string(range(*,1)), 6, -999., -999., ''

create_param, params, $
   'ERANGE', 'fit', 'float', string([30.,100.]), 2, -999., -999., ''

create_param, params, $
   'SUMFIT', 'fit', 'fix', '0', 1, -999., -999., ''

create_param, params, $
   'SP_XTYPE', 'fit', 'fix', '1', 1, -999., -999., ''

create_param, params, $
   'SP_YTYPE', 'fit', 'fix', '1', 1, -999., -999., ''

create_param, params, $
   'SPXRANGE', 'fit', 'float', string([0.,0.]), 2, -999., -999., ''

create_param, params, $
   'SPYRANGE', 'fit', 'float', string([0.,0.]), 2, -999., -999., ''

create_param, params, $
   'MORE_INFO', 'fit', 'fix', '0', 1, 0., 1., ''


create_family, fam_info, 'drm', 'Detector Response Matrix Parameters:', $
	['/', 'multidrm','read_drm', 'build_drm', 'recalibrate', 'cal_save','cal_restore','list']

create_param, params, $
   'DDIRECTORY', 'drm', 'string', '', 1, -999., -999., ''

create_param, params, $
   'DFILE', 'drm', 'string', '', 1, -999., -999., ''

create_param, params, $
   'SFILE', 'drm', 'string', '', 1, -999., -999., ''

create_param, params, $
   'HIGH_DRM', 'drm', 'fix', '0', 1, 0., 1., ''

create_param, params, $
   'GAIN_OFFSET', 'drm', 'float', '0.', 1, -100., 100., ''

create_param, params, $
   'GAIN_MULT', 'drm', 'float', '1.', 1, 0.1, 2., ''

create_param, params, $
   'TEST_DRM', 'drm', 'fix', '2', 1, 0., 2., ''

create_param, params, $
   'CONT_PENDLETON', 'drm', 'fix', '2', 1, 0., 2., ''

create_family, fam_info, 'summary', 'Summary Procedure Parameters:', $
	['/', 'save_event', 'restore_event', 'list']

create_param, params, $
   'SUMDIRECTORY', 'summary', 'string', ' ', 1, -999., -999., ''

create_param, params, $
   'SUMFILE', 'summary', 'string', 'summary.dat', 1, -999., -999., ''
getout:
return & end
