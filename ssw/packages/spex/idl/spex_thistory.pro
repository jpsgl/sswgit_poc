;+
; NAME: 
;	SPEX_THISTORY
;	
; PURPOSE: 
;	Plot the SPEX count rate as a function of time in one
;	   or up to four scaled bands. 
;	
; CALLING SEQUENCE: 
;	spex_thistory, ut, rate, erate, command, data_tipe, edges, $
;	  count_2_flux, uflux, title, $
;	  t_hist_mode, energy_bands, scale_bands, th_yrange, th_ytype, $
;	  real_eband = real_eband, sampav=sampav, ltime=ltime, linestyle=linestyle
; INPUTS:
;	Ut - time array 2xnbin in secs. relative to getutbase()
;	Rate - count rate (nchan,nbin) usually counts/cm2/s/keV
;	Erate - sqrt uncertainties on rate
;	Command - spex command, 'graph' - multichannels
;	                        'time_history' - integrated standard channels
;	Data_tipe- instrument and data format in SPEX
;	Edges - 2xnchan - defined energy edges, standard is energy-loss 
;	Count_2_flux - conversion vector from counts/s to counts/s/kev/cm2
;	Uflux - units on rate, a string variable 
;	Area  - detector area
;	Title - string describing observation, instr. & detec & date
; 	T_hist_mode - string, 'COUNTS' or 'FLUX' for counts/s or counts/s/kev/cm2
;	Energy_bands - nominal bands for plotting
;	Scale_bands  - scaling factors for energy_bands display
;	Th_yrange   - yrange for plot
;	Th_ytype    - ytype for plot, 0 linear, 1 log
;	EDG_UNITS   - units of edges, normally keV
;	PHOTONS     - If set, then 'Photons' replaces 'Counts' in ytitle
;	EPLOTS      - if set, then plot rate uncertainties with error bars
;	SAMPAV      - sample average for plotting
;	LTIME       - sample livetimes
;	LINESTYLE   - vector of linestyles
;	OVERPLOT    - If set, overplot input time history
;	COLORS	    - referenced to linecolors, defaults [!p.color,3,4,5] for each band	
; OPTIONAL OUTPUTS:
;	real_eband - actual energy bands displayed
;	command(0) may return set to 't_hist_mode'
;	RATES_OUT: Plotted Time-history
;	UTR_OUT  : Times used for RATES_OUT in seconds from GETUTBASE()
; CALLS:
;	DATPLOT, EDGE_PRODUCTS, GETUT, EPLOT, GRAPHICS_PAGE, UTPLOT
;	ARR2STR, FCHECK, FCOLOR, F_DIV, GETUTBASE, LIMITS, READ_CHANMASK, STR2ARR, UTIME
; COMMON BLOCKS:
;	spex_thistory
; PROCEDURE: 
;	This procedure integrates over energies to get rates as a function of
;	time and then plots them using utplot
;
; MODIFICATION HISTORY: ras, 15-apr-94
;	ras, 20-oct-94, modified to use count_2_flux and multi-band livetime
;	ras, 3-aug-95, fixed bug in sample averaging from new livetime array dimensions
;	ras, 9-apr-1996, add overplot, RATES_OUT, UTR_OUT, COLORS
;-
;

;-------------------------------------------------------------
pro spex_thistory, ut, rate, erate, command, data_tipe, edges, $
count_2_flux, uflux, area, title, $
t_hist_mode, energy_bands, scale_bands, th_yrange, th_ytype, $
rates_out=rates, utr_out=utr, photons=photons, $
real_eband = real_eband, edg_units=edg_units, eplots=eplots, colors=colors, $
sampav=sampav, ltime=ltime, linestyle=linestyle, overplot=overplot


common spex_thistory, win

checkvar, colors, [!p.color, 3+indgen(10)]
edge_products, edges, mean=emdge, width = wedge
nchan = n_elements( rate(*,0))
nbin  = n_elements( rate(0,*))
nband= n_elements( energy_bands ) /2
;convert the input bands into actual bands using the standard algorithm for SPEX
nband = nband < nchan
spex_intervals, edges, real_eband, xinput=reform(energy_bands(0:2*nband-1),2,nband), $
	v_style=['d'], style='discrete',input='energy_bands', iselect=ibands
ibands(1,*)=ibands(1,*)+1
real_eband = [edges(0,ibands(0,*)),edges(1,ibands(1,*)-1)]
edge_products, ibands, width=wbands
graphics_page

if command(0) eq 'time_history' then  begin	;plot a channel integrated light curve
    ;Display range for different data types
    checkvar, cont_valid, 1+indgen(15)
    checkvar, sher_valid, 25+indgen(100)
    checkvar, her_valid, 7+indgen(100)
    wv = indgen(n_elements(edges(0,*)))
    if strupcase(data_tipe(1)) eq 'CONT' then wv =cont_valid
    if strupcase(data_tipe(1)) eq 'SHERS' then wv = sher_valid
    if strupcase(data_tipe(1)) eq 'HERS' then wv = her_valid
    bands = reform([limits(edges(*,wv)),fltarr(6)],2,4)
    nband= 4
    endif 
colors = colors(0:nband-1)

if total(abs([energy_bands(*),(fcheck(bands))(*)])) ne 0.0 then begin
    ;sum the background subtracted flux over the energy bands
    if command(0) ne 'time_history' then  bands= reform( energy_bands(0:2*nband-1),2,nband )
    rates = fltarr(nband, nbin)
    erates = fltarr(nband, nbin)
    wplot = intarr(nband)
    real_eband = fltarr(2,nband)
    for i=0,nband-1 do begin
        if total(bands(*,i)) ne 0 then begin
            if wbands(i) ge 1 then begin 
		wband = ibands(0,i)+indgen(wbands(i))
                case t_hist_mode of
                    'FLUX': begin
                        rates(i,*) = total( rate(wband,*)*rebin(wedge(wband),wbands(i),nbin),1) / $
                        total(rebin(wedge(wband),wbands(i),nbin),1) 
                        erates(i,*) = sqrt(total( (erate(wband,*)*rebin(wedge(wband),wbands(i),nbin))^2,1) / $
                        (total(rebin(wedge(wband),wbands(i),nbin),1) )^2)
                        real_eband(*,i) = [edges(0,wband(0)),edges(1,wband(wbands(i)-1))] 
                        end
                    'COUNTS': begin
                        rates(i,*) = total( rate(wband,*)*rebin(count_2_flux(wband),wbands(i),nbin),1)
                        erates(i,*) = sqrt(total( (erate(wband,*)* $
                        rebin(count_2_flux(wband),wbands(i),nbin))^2,1))
                        real_eband(*,i) = [edges(0,wband(0)),edges(1,wband(wbands(i)-1))] 
                        end
                    else: goto, t_hist_mode	; go set mode to allowed values
                    endcase
                wplot(i) = 1
                endif
            endif
        endfor
    wplot = where(wplot,ncount)
    endif else begin
    rates = rate 
    if strupcase(t_hist_mode) eq 'COUNTS' then $
    rates= rates * rebin( reform( count_2_flux, nchan, 1), nchan,nbin)
    wplot = where( read_chanmask(chanmask) eq 1, ncount)
    real_eband = edges(*,wplot)
    endelse

y = rates(wplot(0),*)
getut, uts=uts, ute=ute, /string

xlim = [ (utime(uts)-getutbase(0))>min(ut(0,*)), (utime(ute)-getutbase(0))<max(ut(0,*))]
imin = ((where( ut(0,*) ge xlim(0), nimin))(0) > 0 ) < (nbin- 2*(fcheck(sampav,1)>1))
imax = (where( ut(0,*) gt xlim(1), nimax)-1)(0) > (imin + 2*(fcheck(sampav,1)>1))< (nbin-1)
if nimax eq 0 then imax = nbin-1
wgraph = lindgen(imax-imin+1)+imin

!x.type = 0
edge_products, ut, mean=utm

if total( abs( scale_bands) ) eq 0 then scale_bands(0) = 1.

sc = scale_bands(0)
nwplot = n_elements(wplot)
band_limits = fltarr(2,nwplot)
for i=0, nwplot -1 do $
if i le (nband-1) then band_limits(*,i) = scale_bands(i)*limits( rates(wplot(i),wgraph)) else $
band_limits(*,i) = limits( rates(wplot(i),wgraph)) 


if keyword_set(photons) then yunit='Photons' else yunit='Counts'
if strupcase(t_hist_mode) eq 'COUNTS' then $
ytitle = yunit+' s!u-1!n' else ytitle= yunit +uflux

;	  Create informational strings for subtitles
string_scale = arr2str(delim=' ',str2arr(strcompress( string(scale_bands(0:nwplot-1),  $
form = '('+strtrim(nwplot,2)+'(f7.1,"x::"))'),/remove),delim='::') )
string_bands = arr2str(delim=' ',str2arr(strcompress( string(real_eband(*,0:nwplot-1),  $
form = '('+strtrim(nwplot,2)+'(f7.0,"-",f7.0,"::"))'),/remove),delim='::') )
th_subtitle = 'Actual Energy Bands: '+ string_bands + edg_units + $
'!cRate Scaling Factors:'+string_scale

if total(abs(th_yrange)) ne  0 then yrange = th_yrange else begin
    yrange = limits( band_limits) 
    if th_ytype eq 1 then $	;guard against bad yrange for log plots
    th_yrange(0) = th_yrange(0) > 1e-4* th_yrange(1)
    endelse

;CREATE interval averaged rates for plots
if sampav gt 1 then begin
    nselect = nbin / sampav > 1
    mgraph   = sampav * nselect
    
    ix =  [indgen(nselect)*sampav,indgen(nselect)*sampav+sampav-1]
    ix = transpose( reform(ix,nselect,2) ) < (nbin-1)
    ;bin up the times to sampav
    utr = dblarr(2,nselect)
    utr(0,*) = ut(0,ix(0,*))
    utr(1,*) = ut(1,ix(1,*))
    utm = (rebin(utr,1,nselect))(*)
    nrates = n_elements(rates(*,0))
    rates = temporary(f_div( rebin( rebin(reform(ltime(0,0:mgraph-1),1,mgraph), nrates, mgraph) $
    * rates(*,0:mgraph-1),nrates, nselect), rebin( ltime(0,0:mgraph-1),nrates, nselect) ))
    if eplots then $
    erates= temporary(sqrt(f_div( rebin( rebin(reform(ltime(0,0:mgraph-1),1,mgraph), nrates, mgraph)^2 * $
    erates(*,wgraph(0:mgraph-1))^2,nrates, nselect), $  
    rebin( ltime(0,0:mgraph-1),nrates, nselect)^2 )))
    endif else utr = ut

y = rates(wplot(0),*)
if n_elements(linestyle) ne ncount then linestyle=fltarr(ncount)
if not keyword_set(overplot) then begin
    
    utplot, utr(0,*), y*sc,/nodata, ytit=ytitle,title=title, $
    subtitle=th_subtitle, ytype = th_ytype, yrange =yrange, /xstyle
    win = {winsav, x:!x, y:!y, clip:!p.clip}
    endif else reset_xy, win


datplot, 0,0,xs=utr, y * sc,/stairs,/nolegs, color=fcolor(colors(0)), linestyle=linestyle(0)
if eplots then eplot, utm(*), y(*)* sc, ey=(erates(wplot(0),*))(*)*sc,color=fcolor(colors(0))

if ncount ge 2 then begin
    for i=1,ncount-1 do begin 
        sc = scale_bands(i)
        datplot,0,0,xs=utr, sc * rates(wplot(i),*), color=fcolor(colors(i)), $
        /stairs,/nolegs, linestyle=linestyle(i)
        if eplots then $
        eplot, utm(*),sc * (rates(wplot(i),*))(*), ey=sc *( erates(wplot(i),*) )(*), $
        color=fcolor(colors(i))
        endfor	
    endif

return
t_hist_mode:
;
; In case the T_HIST_MODE is not an allowed value, go back and set it.
;
command(0) = 't_hist_mode'

end
