
;+
; NAME: 
;	rd_wbs_pha	
; PURPOSE:
;	accumulate wbs pulse-height data
; CALLING SEQUENCE:
;	rd_wbs_pha, infil=infil, data=data, index=index, wuse=wuse,  $
;		ut=ut, pha=pha, live=live, data_tipe, $
;		title=title
;	
; INPUTS:
;	Get WBS PHA data from INFIL or DATA for SXS, HXS, or GRS whichever
;	is input.  If DATA and INDEX exist, they are used.  Usual
;	Yohkoh meanings (see Yodat).
;	infil
;	data
;	index
;	wuse  - frames to select from data, can be changed inside
;	/new  - select new data from index (or data) using plot_lcur
;	experiment ='sxs1','sxs2','hxs','grs1','grs2', 'grsh1', or 'grsh2'
;	
; OUTPUTS:
;	pha - pulse-height analyzer data, fltarr(128,nbins)
;	ut  - time bins, start, stop, dblarr(2,nbins) relative to 1-jan-79
;	live- live time per bin
;	titl- descriptive title for plotting, string
; PROCEDURE:
;
; RESTRICTIONS:
;
; MODIFICATION HISTORY:
;	ras- 24-apr-94
;	ras- 6-oct-94, fixed livetime calculation error for hxs and grs
;	ras, 26-mar-2001, prevent crash on grsh1 and grsh2, bypass
;	plot_lcur and gt_grsh1 (does not exist).
;-
;
pro rd_wbs_pha, infil=infil, data=data, index=index, wuse=w, new=new,  $
	pha=pha, live=live, ut=ut, experiment = exper, title=title, error=error

;NB this only works in high rate mode for now

error = 1 ;clear on successful data read
common rd_wbs_pha, index_old, data_old, infil_old, w_old
checkvar, infil_old, ''
checkvar, new, 0

expm = ['sxs1','sxs2','hxs','grs1','grs2', 'grsh1', 'grsh2']
tags = ['SXS_PH1','SXS_PH2','HXS_PH','GRS_PHL1','GRS_PHL2','GRS_PHH1','GRS_PHH2']

ncyc = [ 2, 2, 4, 1, 1, 1, 1] ; pha cycles per major frame
nchan = [ 128, 128, 32, 128, 128, 16, 16]
exp     =strlowcase(exper)
wexpm = where(  expm eq exp, nexpm)

if nexpm ne 1 then begin
	printx, 'Error in instrument specification in RD_WBS_PHA'
	printx, string(/print,exper, ' specified, please select one from ', expm)
	return
endif

exp = expm(wexpm(0))
ncyc= ncyc(wexpm(0))
nchan= nchan(wexpm(0))

if (n_elements(data) eq 0) or (n_elements(index) eq 0) then  $;read infil
	if fcheck(infil,'-1') ne infil_old then $
	rd_xda,infil,-1,index,data else begin 
		data = data_old
		index= index_old
		if not new then w=w_old
	endelse
data_old = data
index_old= index
infil_old= fcheck(infil,'')
w = lindgen(n_elements(index_old))

itag = (where( strupcase(tag_names(data.ph)) eq tags(wexpm(0))))(0)

if keyword_set(infil) then infil_old=infil

;Select data interval 
if not keyword_set(w) or new then begin
  if have_windows() or !d.name eq 'TEK' and strmid(exp,0,4) ne 'grsh' then begin
	y = call_function('gt_'+ exp, index, title=title)  
	psav = !p
	clear_utplot
	w = plot_lcur(index, y, title=title, plotr=plotr)
  	!p = psav
  endif else w =lindgen(n_elements(index))
endif 
w_old = w

ndata= n_elements(w)
printx,'Processing data array for '+exp+'.'
pha = reform(hxt_decomp( data(w).ph.(itag)), nchan,ncyc*ndata) *1.0 

;time covered by each record of index in seconds, x2 for WBS
dt = (rebin( reform(dprate2sec(index(w)) *2,1,ndata),ncyc,ndata))(*) 
dt = dt/ncyc	;for each sample in the frame

ut = mk_timarr( index(w), ncyc, /wbs) +anytim(index(w(0)), /sec) 
ut = transpose( reform([ut(*),ut(*) + dt],ncyc*ndata,2))

case 1 of 
      exp eq 'sxs1' or exp eq 'sxs2': 	 live = dt - 86.7e-6 * total(pha,1) >0;counts/interval
      exp eq 'hxs'                  :    $
         live = dt / (hxs_ph_dead( f_div( total(pha,1), dt ) ) > 1.0)
      exp eq 'grs1' or exp eq 'grs2' :   $
         live = dt / ( call_function( 'dtcf_ph_'+exp+'l',f_div( total(pha,1),dt )) >1.0 )
      exp eq 'grsh1' or exp eq 'grsh2':  live = dt 	;assume no deadtime for the moment	
      else:
endcase

title = 'YOHKOH/'+STRUPCASE(EXP)
title = title +' '+ anytim(ut(0),/date,/yohkoh)

error = 0
end
	
