;+
; PROJECT:
;	YOHKOH
; NAME: 
;
;
; PURPOSE:
;	This procedure reads the GRS response functions in SUGA format.
;
; CATEGORY:
;	YOHKOH, WBS, RESPONSE
;
; CALLING SEQUENCE:
;	
;
; CALLS:
;	none
;
; INPUTS:
;       none explicit, only through commons;
;
; OPTIONAL INPUTS:
;	none
;
; OUTPUTS:
;       none explicit, only through commons;
;
; OPTIONAL OUTPUTS:
;	none
;
; KEYWORDS:
;	none
; COMMON BLOCKS:
;	none
;
; SIDE EFFECTS:
;	none
;
; RESTRICTIONS:
;	none
;
; PROCEDURE:
;	none
;
; MODIFICATION HISTORY:
;	Version 1, richard.schwartz@gsfc.nasa.gov
;
;-
pro wbs_grs_response, det_id, drm, pha_edges, photon_edges, area

resp_file = loc_file(path='DIR_WBS_CAL', 'grs'+strtrim(det_id,2)+ '_9711.genx')

restgen, file=resp_file, photon_edges, drm, pha_edges

;DRM is in cts/keV per unit photon (not normalized by geometric area of 45.6 in stored file
area = 45.6
drm = drm / area

end

