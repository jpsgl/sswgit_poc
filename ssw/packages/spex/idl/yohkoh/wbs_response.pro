;+
;
; NAME: 
; 	wbs_response
;
; PURPOSE:
;       read Yohkoh WBS response functions for HXS and GRS1, GRS2 as well as nominal energy
;	calibration files
;
; CATEGORY: 
;	spectral analysis
;
; CALLING SEQUENCE:
;
;        Responses,infile,ncm,nsc,esc,rsc0,rsc1,e1,e2,ebar
;
; CALLED BY:
;       spex.pro, rd_drm_4_spex
;
;
; INPUTS:
;       experiment - string, either 'hxs', 'grs1', 'grs2'
;
; OPTIONAL INPUTS:
;	date=date  - any time format accepted by ANYTIM, defaults to current utbase
;
; OUTPUT:
;        drm  = response function in units of cnts/keV/cm2 per unit photon input
;	 area = nominal geometric area of detector
;        edges_out= channel edges in keV for output detector bins in drm
;	 edges_in = channel edges in keV for incident photon bins in drm
;
; COMMON BLOCKS:
;	  common wbs_response, exp_com, date_com, drm_com, area_com, e_out_com, e_in_com
;	  used for program memory, not for communications
; MODIFICATION HISTORY:
;	ras, 25-may-94
;-
pro wbs_response, experiment, date=date, drm=drm, area=area, edges_out=edges_out, edges_in=edges_in, $
	error=error

common wbs_response, iexp_com, date_com, drm_com, e_out_com, e_in_com
error = 1

hxs_date = utime('9-jun-92') ;date of hxs gain change
checkvar, date_com, hxs_date

rsp_fils = [ 'hxs01_resp.dta', 'hxs21_resp.dta', 'grs1_resp.dta', 'grs2_resp.dta' ]
expm = [ 'hxs', 'grs1', 'grs2']
area = 45.6 ;nominal effective area for a 7.6 cm diameter xsection

wexp = where(expm eq experiment, nexp)

if nexp ne 1 then begin
	printx,'Error in routine WBS_RESPONSE.'
	printx,'Incorrect specification of instrument, input string: '+experiment+' should'
	printx,'contain only 1 of these strings: '+arr2str( expm, ', ')
	return
endif

iexp = wexp(0)

;If the response is for HXS, get the correct energy calibration!
ut   = anytim( fcheck(date,getutbase()), /sec)
if iexp ne 0 or ((ut - hxs_date)*(date_com - hxs_date) gt 0) then both = 1 else both = 0

;If the response is for GRS1 or GRS2, then the date doesn't matter

;Read the response and energy files if they are not the last read
if iexp ne fcheck(iexp_com,-1) or not both then begin
	case 1 of 
	  expm(iexp) eq 'hxs' and ut ge hxs_date: ifil = 0
	  expm(iexp) eq 'hxs' and ut lt hxs_date: ifil = 1
	  expm(iexp) eq 'grs1' or expm(iexp) eq 'grs2': ifil = iexp+1
	endcase
	infil=concat_dir(chklog('$DIR_WBS_CAL'),rsp_fils(ifil))
	test = findfile( infil, count=count)
	if count ne 1 then begin
		printx,'ERROR! Cannot find '+infil+ ' Returning from WBS_RESPONSE.'
		return
	endif
	printx,'Reading response and cal file '+infil+' for '+expm(iexp)
	responses, infil, ncm, nsc, esc, drm, rsc1, e1, e2
	edges_out = transpose( reform( [e1,e2], ncm, 2 ))
	de = rebin( reform(e2 - e1,ncm,1), ncm, nsc)
	drm= drm / de / area
	desc= esc(1:*)/esc
	edges_in = sqrt( esc(1:*)*esc) / desc
	edges_in = [ edges_in, edges_in(nsc-2)*[desc(0),desc(0)^2]]
	edge_products, edges_in, edges_2=edges_in

	;save new values in common
	drm_com = drm
	iexpm_com= iexp
	e_out_com= edges_out
	e_in_com = edges_in
	date_com = ut
endif else begin ;same energy calibration for same detector so use common block
	drm = drm_com
	edges_out = e_out_com
	edges_in  = e_in_com
endelse
	
error=0

end
	
	

