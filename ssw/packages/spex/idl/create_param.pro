;+
; NAME: CREATE_PARAM
;
; CALLING SEQUENCE:
;	create_param, str, nam, fam, typ, val, numval, min, max, opt
;
; SAMPLE CALLS:
;	To create a new parameter:
;	create_param, str, nam, fam, typ, val, numval, min, max, opt
;
; Creates structures of parameters for menu driven program.
; Creates or adds onto the parameter structure.  The purpose
; of families is to have
; groups of menu items - list_op can be called to only display a particular
; family, or to display multiple families separated by a space and with the
; family title.
;
; The structure for each parameter looks like:
;      str =	{param_structure2, $
;		nam:'', $	; parameter name
;		fam:'', $	; family that parameter belongs to
;		typ:'', $	; variable type (fix,long,float,double,string)
;	   	val:strarr(20), $ ; current value of parameter
;		numval: 0L, $    ; number of values to use from val tag
;		min:0.d0, $	; minimum value allowed for parameter
;		max:0.d0, $	; maximum value allowed for parameter
;		opt:strarr(20)} ; choices for string parameters
; After multiple calls, there will be an array of such structures, one
; element for each parameter.
;
; Each family of parameters has a corresponding structure (that must be
; created by a call to create_family) that looks like:
;       str = {family_info, $
;		fam:'', $	; family name
;		title:'', $ 	; title for section of menu
;		commands:strarr(40)}  ; commands for family
;
; CALLING ARGUMENTS:
;	STR - Name of structure to create or add to
;	NAM - String. Parameter name that will appear in menu
;	FAM - String. Family parameter belongs to.  For each family, must
;		also call create_param with family and title keywords to
;		create parameter info structure entry.
;	TYP - String. Type of variable: fix,long,float,double,string,ut
;	VAL - String. Current value of parameter.  Array of up to 20.
;		ras, 6-nov-02, array of up to 200 to support more lines in
;			in f_bpow_nline
;	NUMVAL - Long.  Number of values in VAL to use.
;	MIN - Double. If VAL is numeric, minimum value allowed. -999. means
;		don't check min.
;	MAX - Double. If VAL is numeric, maximum value allowed. -999. means
;		don't check max.
;	OPT - String. Array of up to 20 allowed options if VAL is a string.
;
; Written 9-mar-94 akt
; Version 2, ras, 8-april-1996, aligned structure boundary, numval ==> longword
; Version 3, richard.schwartz, allow up to 60 values in a parameter array.
;	11-jan-2001
; Version 4, ras, 6-nov-2002, enable up to 200 parameters in spex after 5.1
;-




pro create_param, str, nam, fam, typ, val, numval, min, max, opt

new = 0

; check that str has been defined.  If not, define it.
if (size(str))(2) ne 8 then new = 1 else $
   if tag_names(str, /structure_name) ne 'PARAM_STRUCTURE3' then new = 1
maxval =  since_version('5.1')? 200 : 60
if new then $
   str =	{param_structure3, $
		nam:'', $	; parameter name
		fam:'', $	; family that parameter belongs to
		typ:'', $	; variable type (fix,long,float,double,string)
		val:strarr(maxval), $ ; current value of parameter
                numval: 0L, $    ; number of values to use from val tag
		min:0.d0, $	; minimum value allowed for parameter
		max:0.d0, $	; maximum value allowed for parameter
		opt:strarr(maxval)} ; choices for string parameters

nopt = n_elements(opt)
opt20 = [opt,strarr(maxval-nopt)]
nval = n_elements(val)
val20 = [val,strarr(maxval-nval)]
new_param = {param_structure3, nam, fam, typ, val20, numval*1L, $
             min*1.d0, max*1.d0, opt20}

if new then str = new_param else str = [str, new_param]

return&end
