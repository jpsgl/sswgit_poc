
;+
;
; NAME:   SPEX_SAVE_DATA
;
;
; PURPOSE: Write current spex input data to a save file.  Rates
;
;
; CATEGORY:  SPEX, Spectral Analysis
;
;
; CALLING SEQUENCE:  SPEX_SAVE_DATA [, Filename]
;
;
; CALLED BY:
;
;
; CALLS:
;	none
;
; INPUTS:
;       none explicit
;
; OPTIONAL INPUTS:
;	Filename - Name of file to write to.
;	ALL: If set, then save all of the defined data in SPEX_COMMONS
;	RESTORE: If set, then restore Filename, may use findfile wildcards
;	or enter a null string.
; OUTPUTS:
;       none explicit, only through commons;
;
; OPTIONAL OUTPUTS:
;	none
;
; COMMON BLOCKS:
;	none
;
; SIDE EFFECTS:
;	none
;
; RESTRICTIONS:
;	none
;
; PROCEDURE:
;	none
;
; MODIFICATION HISTORY:
;            Version 1, RAS July 1996
; Version 4, ras, 6-nov-2002, enable up to 200 parameters in spex after 5.1
;-
pro spex_save_data, filename, ALL=ALL, RESTORE=RESTORE

@spex_commons

if not exist(filename) then begin
	data_tipe = spex_current('data_tipe')
	utbase = getutbase()
	date =  str_sep(  atime(/dat, utbase),  '/' )
	date =   arr2str(del='',   date )
	filename = 'spex_'+arr2str( strlowcase(data_tipe), del='_')+'.'+date
endif

if not keyword_set(restore) then begin
  if not keyword_set(all) then begin
	files = spex_current('files')

	photons = f_div( obsi-backi, convi)
	ephotons= f_div( sqrt(eobsi^2+ebacki^2),  convi)
	chkarg,'spex_save_data', readme, /reset
	erange=spex_current('erange')
;Accumulation time interval is in XSELECT

	save,/verb,file=filename, $
	rate, back, ut, erate, eback, edges, e_in, delta_light, drm, count_2_flux, $
	uflux,  area, det_id, files, obsi, eobsi, backi, ebacki, convi, apar_arr, $
	chi, photons, ephotons, xselect, wuse, erange, readme
  endif else begin

	;Save all data in common blocks which are defined & UTBASE
	utbase = getutbase()
	save,/xdr,file=filename,/var
  endelse
endif else begin

  files = findfile(filename(0),count= nfile)
  if nfile eq 0 then if f_use_widget(/test) then files = pickfile() else begin
	message,/cont,"Can't find "+filename(0)
	return
  endelse
  if nfile gt 1 then begin

  if f_use_widget(/test) then $
  nselect = select_widg( files, /index, title='Select the datafile below') $
  else $
  nselect = emenu( files, $
  mtitle='Enter the number for the desired datafile from the following:', $
  imax = 100 < n_elements(files) )
  files = files(nselect)
  endif
  if since_version('5.1') then restore, files(0), /relaxed else restore, files(0)

endelse

end

