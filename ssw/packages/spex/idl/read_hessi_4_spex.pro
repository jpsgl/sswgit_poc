;+
;
; PROJECT:
; SDAC
;
; NAME:
; read_hessi_4_spex
;
; PURPOSE:
; Read HESSI data from a FITS/GENX file into Spex internal variables.
;
; CATEGORY:
; SPEX
;
; CALLING SEQUENCE:
; read_hessi_4_spex, _1file, START_TIME=start_time, END_TIME=end_time, $
;                    LTIME=ltime, EDGES=edges, DELTA_LIGHT=delta_light,
;                    UT=ut, UNITS=units, AREA=area, WCHAN=wchan
;
; INPUTS:
;
; INPUT KEYWORDS:
; FILES - Name of file to read.
;
; OUTPUTS:
;
; OUTPUT KEYWORDS:
; The following keywords are set to values needed by Spex.  Many are described
; in the Spex documentation.  Those not described are internal variables not
; meant to be used / accessed by the user.  Those keywords listed in the
; procedure definition line not listed here are included for Spex comptability,
; and are not used by this procedure.
; 	START_TIME
; 	END_TIME
; 	FLUX
; 	EFLUX
; 	UT
; 	UNITS
; 	AREA
; 	LTIME
; 	EDGES
; 	DELTA_LIGHT
; 	WCHAN
; 	TITLE
; 	COMMAND_OUT
;
; OPTIONAL OUTPUTS:
;
; COMMON BLOCKS:
; read_spex_com, data, ut_data, data_file_read, read_start, read_end
;
; SIDE EFFECTS:
;
; RESTRICTIONS:
;
; PROCEDURE:
;
; EXAMPLE:
; Not to be used directly.  Called from within SPEX to load HESSI data
;
; CALLS:
; break_file, edge_products, spex_data_genx, read_hessi_fits_4_spex
;
; Written 25-May-2001 Paul Bilodeau, RITSS/NASA-GSFC
;
; Modification History:
; 	31-May-2001 Paul Bilodeau - added GENX file reading
;                                   capability.
;       18-March-2002 Paul Bilodeau - merged FITS and GENX into the
;                                     same level.
;       20-March-2002 Paul Bilodeau - only modify command_out with
;                                     respfile if respfile is found.
;
;
;-
;------------------------------------------------------------------------------
PRO read_hessi_4_spex, DFORMAT=dformat, $
                       FILES=files, $
                       START_TIME=start_time, $
                       END_TIME=end_time, $
                       FLARE=flare, $
                       P_READ_DATA=p_read_data, $
                       FLUX=flux, $
                       EFLUX=eflux, $
                       UT=ut, $
                       UNITS=units, $
                       AREA=area, $
                       COSINES=cosines, $
                       LTIME=ltime, $
                       EDGES=edges, $
                       DELTA_LIGHT=delta_light, $
                       WCHAN=wchan, $
                       TITLE=title, $
                       ERROR=error, $
                       COMMAND_OUT=command_out, $
                       _REF_EXTRA=_ref_extra

common read_spex_com, data, ut_data, data_file_read, read_start, read_end
checkvar, data, 0
checkvar, data_file_read, ''
checkvar, read_start, ''
checkvar, read_end, ''

command_out = 'read_drm'
com_param=command_out

delta_light= 1.0
checkvar, units, ' s!u-1!n cm!u-2!n keV!u-1!n'

break_file, files[0], disk, dir, fname, fext

fits = Strupcase( fext ) EQ '.FITS'

IF fits THEN BEGIN
    fits2spectrum, files[0], extnumber, $
                   PRIMARY_HEADER=p_hdr, EXT_HEADER=sp_hdr, $
                   EXT_DATA=sp_data, ENEBAND_DATA=en_data, $
                   _EXTRA=_ref_extra, ERR_MSG=err_msg, ERR_CODE=err_code
    IF err_code THEN BEGIN
        printx, err_msg
        RETURN
    ENDIF
    ut = Dblarr( 2, N_Elements(sp_data) )
    IF tag_exist( sp_data, 'TSTART') THEN BEGIN
         ut[0,*] = sp_data.tstart
        ut[1,*] = sp_data.tstop
    ENDIF ELSE BEGIN
      ;This is the new, compliant formulation for time in the RATE files.
        timedel = tag_exist(sp_data, 'TIMEDEL')? $
          sp_data.timedel : float( fxpar('TIMEDEL'))
        ut = (sp_data.time- timedel / 2.0)## (fltarr(2)+1.0)
        ut[1,*] = ut[1,*] + timedel
    ENDELSE
    edges = Transpose( [ [ en_data.e_min ] , [ en_data.e_max ] ] )
    ltime = sp_data.livetime
ENDIF ELSE BEGIN
    restgen, FILE=files[0], spex_data, sp_data
    ut = sp_data.ut
    p_hdr = spex_data.primary_header
    sp_hdr = spex_data.energy_extheader
    edges = spex_data.edges
    ltime = sp_data.ltime
ENDELSE

sp_tags = Tag_Names( sp_data )
data_tags = [ 'FLUX', 'RATE', 'COUNTS' ]
eflux_idx = -1
FOR i=0, N_Elements( sp_tags )-1L DO BEGIN
    match = Where( sp_tags[i] EQ data_tags, n_match )
    IF n_match GT 0 THEN BEGIN
        flux_idx = i
        unit = data_tags[ match[ 0 ] ]
    ENDIF
    ematch = Where( sp_tags[i] EQ 'E' + data_tags, n_ematch )
    IF n_ematch GT 0 THEN eflux_idx = i
ENDFOR

flux = sp_data.( flux_idx )
eflux = eflux_idx[0] GT -1 ? sp_data.( eflux_idx ) : sp_data.error

area = fxpar(sp_hdr, 'GEOAREA', COUNT=area_cnt)
IF area_cnt GT 0L THEN area = (st2num( area, area_stat ))[0]
IF 1 - area_stat THEN area = 1.

IF fits THEN BEGIN
    IF strtrim( FXPAR( sp_hdr, 'TIMESYS'),2) EQ 'MJD' then begin
        mjd = replicate( anytim(0.0,/mjd), n_elements(ut) )
        ;; convert to MJdays
        mjd.mjd = long( ut[*] )
        ;; convert to millisec
        mjd.time = long( ( ut[*] MOD 1. ) * 8.64e7)
        ;; convert to ut seconds format from 1-jan-1979
        ut[0] = anytim( mjd, /sec)
    ENDIF
ENDIF
; Make any 3-D arrays into 2-D - multiple detector sets must be combined for
; analysis
flux_size = Size( flux )
nbin = flux_size[flux_size[0]]
nchan = flux_size[flux_size[0]-1L]
IF flux_size[0] GT 2L THEN flux = Total( flux, 2 )

IF ( Size( eflux ) )[0] GT 2L THEN eflux = Total( eflux, 2 )

;The next line is an approximation, it needs refinement
;That's why this shouldn't be done here.
IF ( Size( ltime ) )[0] GT 2L THEN ltime = Total( ltime, 2 )

; transpose the flux, eflux, and ltime matrices, if necessary
delta_light = get_edges( edges, /width ) # ( 1+fltarr(nbin) )
acctime = float( get_edges( ut, /width)) ## ( 1+fltarr(nchan) )

IF n_elements( ltime ) EQ 1 THEN $
  ltime = ltime[0]

IF n_elements( ltime ) NE n_elements(flux) THEN $
  ltime = ltime ## (1+fltarr(nchan))

CASE unit OF
    'COUNTS': BEGIN
        flux = f_div( f_div( flux / area, acctime*ltime ), delta_light)
        eflux = f_div( f_div( eflux / area, acctime*ltime ), delta_light)
    END
    'RATE': BEGIN
        flux = f_div(  flux / area, delta_light)
        eflux = f_div(  eflux / area, delta_light)
    END
    ELSE: ;; It's already flux.
ENDCASE

ltime = ltime * acctime

units = ' s!u-1!n cm!u-2!n keV!u-1!n'
wchan = Lindgen( nchan )

start_time = anytim( fxpar( p_hdr, 'DATE-OBS' ), /vms )
end_time = anytim( fxpar( p_hdr, 'DATE-END' ), /vms )

title = 'HESSI SPECTRUM'

; Look for a response file in the spectral header
respfile = fxpar( sp_hdr, 'RESPFILE', COUNT=respcount )
IF respcount GT 0 THEN BEGIN
    rfile = findfile( respfile, COUNT=count )
    IF count GT 0 THEN BEGIN
        command_out = [ 'dfile,'+ rfile, command_out ]
    ENDIF ELSE BEGIN
        printx, 'FILE ' + files[0] +':'
        printx, '    has RESPFILE.'
        printx, 'RESPFILE: ' + respfile + ' not found.'
    ENDELSE
ENDIF

END
