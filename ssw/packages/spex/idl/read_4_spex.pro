;+
; PROJECT: SPEX
;
; NAME:   read_4_spex
;
;
; PURPOSE:  general purpose data input proc. for SPEX
;
;
; CATEGORY: SPEX, analysis i/o
;
; CALLS:
;	SPEC_SENSITIVITY, READ_CONT, READ_DISCSP, CONT_EDGES, FCHECK, CHECKVAR,
;	BATSE_DEADTM, F_DIV, DPRATE2SEC, LIMITS, AVE_CTS2, ANYTIM, GT_DP_MODE
;	RESTORE_OVERFLOW CHKARG CLEAR_UTPLOT DET_COSINES EDGE_PRODUCTS FITS_SPECTRA
;	GET_HXRBS_FITS HXT_CAL_FIX PRINTX RD_ROADMAP RD_SXS_PHA RD_WBS_PHA RD_XDA
;	SHERS_LOAD  SPEX_INTERVALS SXS_ECAL UTPLOT WBS_RESPONSE EXIST ATIME
;
; CALLING SEQUENCE:
; read_4_spex, instrument=instrument, dformat=dformat, files=files, $
; start_time = start_time, end_time=end_time, title=title, command_out=command_out, $
; det_id=det_id, id_style=id_style, p_read_data=p_read_data, _extra=_extra, $
; flux=flux, eflux=eflux, ut=ut, units=units, area=area, ltime=ltime, edges=edges,$
;       id_use=id_use, auto=auto, noplot=noplot, mode=mode, wchan=wchan, delta_light=delta_light
; examples
;
; read_4_spex, instrument='BATSE', dformat='SHER', det_id=1, id_style='id', id_use=id_use
;         files=['hxrbs::sys$user:[richard.batse.seq_obs]sherb_ibdb.468',flare=665, $
;         'hxrbs::sys$user:[richard.batse.seq_obs]sher_cor_ibdb*468.fits'], $
;         flux=flux, eflux=eflux, ut=ut, units=units, area=area, ltime=ltime, edges=edges
;
;
; INPUTS:
; instrument  - string referencing instrument:
;         'BATSE', 'YOHKOH', 'SMM'
; dformat     - data format:
;                 for BATSE - 'DISCLA','DISCSP','CONT', 'SHERS' (SHER & SHERB), 'HERS' (HER & HERB)
;                  YOHKOH-  HXT, SXS, HXS, GRS
;                  SMM - HXRBS, GRSX, tbd GRS,
;                        HIREX - GERMANIUM
;				   HXRS - SHIELDED, NONSHIELDED, TOTAL
; files       - file or files with the data to be read
; flare       - BATSE archive flare number
; start_time  -
; end_time    -
;               optional, read file between start_time and end_time, defaults
;               are the start and end of the files, must be readable by ANYITM
;
; det_id      - detector identifier, for BATSE 0-7
; id_style    - what style of identifier, For BATSE, 'id' - assigned number, default
;                                                    'aspect' - sunward aspect
;                                                    'dsel'   - order in burst readout
; _extra      - keyword inheritance in version >3.1
; /auto       - non-interactive calibration
; /noplot     - no plotting of calibration activities
; p_read_data - name of read procedure for data type not explicitly allowed
; OUTPUTS:
; flux        - fltarr(nchan,nbins) - counts/cm2/sec/keV, overflow and livetime corrected
; eflux       - fltarr(nchan,nbins) - sqrt(counts)/cm2/sec/keV
; ut          - dblarr(2,nbins), seconds referenced to 79/1/1
; units       - e.g. 'cm!u-2!n s!u-1!n keV!u-1!n'
; area        - detector area in cm2
; ltime       - fltarr(1,nbins)  accumulation live time in interval, seconds
;		changed to fltarr(nchan,nbins) - 20-oct-94
; edges       - fltarr(2,nchan)  lo and hi edges in keV
; delta_light - fltarr(nchan), for BATSE SHER, channel width in pulse height
;               see cal_sher and calibrate, and Band 1992 on SLED problem for
;               discussion.
; id_use      - actual detector id, to reference drm
; title       - title string referencing instrument, data
; mode        - special output for YOHKOH, mode=0 for non-flare, 1 for flare
; wchan       - indices of allowed channels
; command_out - further instructions to SPEX, e.g. "read_drm", may also be used for input
; SIDE EFFECTS:
; none
;
; RESTRICTIONS:
; none
;
; PROCEDURE:
; This procedure provides a 1-stop shop for all data types readable through SPEX.
; The correct reader is identified for each data type, it is called for the input
; data file.  The correct PHA edge generator is also called.  Corrections are made
; to the data for livetime and overflow and all vectors by SPEX are returned.
;
; MODIFICATION HISTORY:
; ras, 22-Nov-93
; ras, 20-apr-94, integrate Yohkoh HXT
; ras, 5-May-94, integrate Yohkoh SXS
; ras, 25-May-94, integrate Yohkoh hxs and grs1,2
; ras, 21-jun-94, replace div=>f_div, reset=>restore_overflow
; ras, 18-aug-94, added delta_light
; akt, 7-sep-94, changed hxrbs section to call get_hxrbs_fits
; ras, 18-oct-94, added batse discsp
; ras, 20-oct-94, ltime changed to (nchan,nbins)
; ras, 8-feb-95, corrected typo endt_time => end_time
; ras, 29-sep-95, merged discsp with sher(b)
; ras, 15-dec-95, restore_overflow mod
; Version 12
;	ras 28-mar-1996, moved sher+discsp merge to shers_load.pro
; Version 13
;	ras 16-apr-1996, used cosine on input.
; Version 14
;	ras 16-may-1996, fixed generic reader
; Version 15
;	ras 26-jul-1996, fixed generic reader
; Version 16
;	ras 21-oct-1996, added call to spec_sensitivity for cos_sort
; Version 17
;	ras 23-jan-1997,  initialized area to prevent undecipherable error calling rd_near_pin
; Version 18
;	ras 17-feb-1997,  removed _extra from call to general reader, potential source of
;	problems in the future, tags in _extra need to be initialized or problems result!
; Version 19
;	ras, 26-mar-1997, added GRS Xray detectors and GSR spectrometer.
; Version 20
;	ras, 16-Jul-1997 generalized input using spex_data_genx.
; Version 21
;	richard.schwartz@gsfc.nasa.gov, 26-sep-1997, added BATSE DISCLA.
; Version 22
;	richard.schwartz@gsfc.nasa.gov, 29-sep-1997, moved all non-spex
;	calls to call_procedure.
; 31-May-2000 Paul Bilodeau added HXRS to inst_procs
;-
pro read_4_spex, instrument=instrument, dformat=dformat, files=files, $
start_time = start_time, end_time=end_time, flare=flare, $
det_id=det_id, id_style=id_style, p_read_data=p_read_data,  $
flux=flux, eflux=eflux, ut=ut, units=units, area=area, cosines=cosines, $
ltime=ltime, edges=edges, delta_light=delta_light, wchan=wchan, id_use=id_use, title=title, $
auto=auto, noplot=noplot, mode=mode, error=error, $
command_out=command_out, efficiency = eff, _extra = _extra


common read_spex_com, data, ut_data, data_file_read, read_start, read_end
checkvar,data,0
checkvar,data_file_read,''
checkvar,read_start,''
checkvar,read_end,''
checkvar, command_out, ''
if command_out(0) ne '' then com_param=command_out else com_param=''
delta_light= 1.0
checkvar,    units, ' s!u-1!n cm!u-2!n keV!u-1!n'

inst_procs = ['BATSE','YOHKOH','SMM','HIREX','HXRS']
winst = where( inst_procs eq strupcase(instrument(0)), ninst)

if ninst eq 1 then $
call_procedure, "read_"+inst_procs(winst(0))+"_4_spex", dformat=dformat, files=files, $
start_time = start_time, end_time=end_time, flare=flare, $
det_id=det_id, id_style=id_style, p_read_data=p_read_data, $
flux=flux, eflux=eflux, ut=ut, units=units, area=area, cosines=cosines, $
ltime=ltime, edges=edges, delta_light=delta_light, wchan=wchan, id_use=id_use, title=title, $
auto=auto, noplot=noplot, mode=mode, error=error, $
command_out=command_out, efficiency = eff, _extra = _extra  $
else  begin

    ;Other DATA
    checkvar, p_read_data, 'spex_data_genx'
    p_read_data_chk =  p_read_data
    if strlowcase(p_read_data_chk) eq 'spex_data_genx' then found=1 else $
    chkarg,p_read_data_chk, found=found,/search
    if found then begin
        title = ''
        call_procedure, p_read_data, files=files, $
        start_time = start_time, end_time=end_time, flare=flare, $
        det_id=det_id, id_style=id_style, wchan=wchan,$
        flux=flux, eflux=eflux, ut=ut, units=units, $
        ltime=ltime, edges=edges, id_use=det_id,  area=area,  $
        auto=auto, noplot=noplot,  command_out=command_out, title=title

        edge_products, edges, width=ww
        wchan = fcheck( wchan, indgen(n_elements(ww)))
        if title eq '' then title=instrument+' '+dformat+' '+atime(/yoh,ut(0),/date)
        if datatype(auto) eq 'STR' then command_out=auto
        endif
    endelse
if n_elements(delta_light) eq 1 then delta_light = fcheck( ww, (edges(1,*)-edges(0,*))(*))
;specify live time for each channel and data interval separately!,  ras 20-oct-94
if (size(ltime))(0) eq 1 then ltime = rebin( reform(ltime,1,n_elements(ltime)), $
n_elements(edges(0,*)), n_elements(ut(0,*)) )

end
