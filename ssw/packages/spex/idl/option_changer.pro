PRO DEP150_Event, Event

@spex_commons
common dep150_com, field, spex_name, value_in, value_out, its_common, its_direct

value_out = value_in
;return, get the value of the field widget
for i=0,n_elements(value_in)-1 do begin
	widget_control, field(i), get_value =value
	if value(0) eq '' then value="''"	
	;op_com interprets "''" as a null field and will only
	;change an entry to a blank field this way!
	value_out(i) = value
endfor

widget_control, event.top, /destroy

END



;+
; option_changer
;
; generate widget to change values of options & variables in SPEX
;
; input:
;	option_name - string, descriptive name of option to change in SPEX
;	              
;	prog_name   - string, internal name of option to change in SPEX
;		      must match one of PARAMETERS (ARRAY) in SPEX
;		      see SETUP_PARAMS.PRO
;       incommon    - if set then option is a 
;		      string named variable in one of SPEX_COMMONS(.PRO)
;	twidget     - if set then use twidget
;	snu         - if set then use snu widget
;	direct      - variable to use outside of SPEX commons and parameter list
;
; NAME: 
;
;
; PURPOSE:
;
;
; CATEGORY:
;
;
; CALLING SEQUENCE:
;
;
; CALLED BY:
;
;
; CALLS TO:
;	none
;
; INPUTS:
;       none explicit, only through commons;
;
; OPTIONAL INPUTS:
;	none
;
; OUTPUTS:
;       none explicit, only through commons;
;
; OPTIONAL OUTPUTS:
;	none
;
; COMMON BLOCKS:
;	none
;
; SIDE EFFECTS:
;	none
;
; RESTRICTIONS:
;	none
;
; PROCEDURE:
;	none
;
; MODIFICATION HISTORY:
;	ras, jul-94
;-


pro option_changer, option_name, prog_name, incommon=incommon, $
  twidget=twidget, snu_u=snu_u, digits_snu=digits, toggle=toggle, $
  direct=direct, message=message

common wspex_common, main2, info_base, new1, new2, new3, new4
common dep150_com, field, spex_name, value, value_out, its_common, its_direct
checkvar, message, ''

spex_name = fcheck(prog_name,'')
its_direct = n_elements(direct) ne 0 
its_common = keyword_set(incommon)
if its_direct then value = direct else value = spex_current(prog_name, incommon=incommon) 

case 1 of
 keyword_set(twidget):  begin
  time = anytim(value(0),/ex)
  year = time(6)
  if year lt 100 then $
	if year lt 50 then year = year +2000 else year = year + 1900 
  twidget, year= year, month=time(5), $
	day=time(4),min=time(1),sec=time(2),hour=time(0), /init
  twidget, rdate, outtime=value, /all, $
	message=[message,$
	'Select '+option_name+' Internal Name: '+strupcase(prog_name)], error=error
  if error then value=anytim(time,/atime) else value=anytim(value,/atime)
 if its_direct then direct=value else if not keyword_set(incommon) then $
	spex_proc, input_line=prog_name+','+string(value(0)),/gui else $
	spex_proc, input_line='idl,'+prog_name+'='+string(value(0)),/gui 
 end
 keyword_set(snu_u): begin
  initial = value
  snuvalue:
  value = snu(fcheck(digits,5),message=[message,'Select '+option_name+ $
	' Internal Name: '+strupcase(prog_name)], error=error, $
	group=main2, initial=initial)
; if error then goto, snuvalue
 if its_direct then direct=value else if not keyword_set(incommon) then $
	spex_proc, input_line=prog_name+','+string(value),/gui else $
	spex_proc, input_line='idl,'+prog_name+'='+string(value),/gui 
 end
 keyword_set(toggle): begin

 value_out = option_toggle( value, message=message, name=option_name, /modal)

 if its_direct then direct=value else $
  if not keyword_set(incommon) then begin $
	input_line= prog_name+','+ arr2str(value_out,delim=', ')
	spex_proc, input_line=input_line, /gui
  endif else spex_proc, input_line='idl,'+prog_name+'='+string(value_out),/gui
  end
 else: begin
  DEP150 = WIDGET_BASE( $
      COLUMN=1, $
      MAP=1, $
      TITLE='MODIFY OPTION', $
      UVALUE='DEP150')

  Btns741 = [  'DONE']
  BGROUP153 = CW_BGROUP( DEP150, Btns741, $
      COLUMN=2, $
      UVALUE='MOD_BUTTONS')

  ListVal747 = [ message,'Modify '+strupcase(option_name)+' '+$
    'Internal Name: '+strupcase(spex_name) , $
    'Change the option in the field(s) below.', $
    'Hit the DONE button or [RETURN] TO enter the value.']

  LIST166 = WIDGET_LIST( DEP150,VALUE=ListVal747, $
      FRAME=4, $
      UVALUE='OPTION_NAME', $
      YSIZE=n_elements(listval747) )

nv = n_elements(value)
field = lonarr(nv)

for i=0,nv-1 do begin
  if nv eq 1 then prefx='' else prefx=strtrim(i,2)+'. '
  FieldVal = [ value(i) ]
  subfield = CW_FIELD( DEP150,VALUE=FieldVal, $
      ROW=1, $
      STRING=1, $
      RETURN_EVENTS=1, $
      TITLE=prefx + strupcase(option_name) + '==>', $
      UVALUE=spex_name, $
      XSIZE=30)
  field(i) = subfield
endfor

  WIDGET_CONTROL, DEP150, /REALIZE
  XMANAGER, 'DEP150', DEP150, /modal

if its_direct then reads, value_out,direct else if not its_common then begin
	input_line= spex_name+','+ arr2str(value_out,delim=', ')
	spex_proc, input_line=input_line, /gui
endif else spex_proc, input_line='idl,'+spex_name+'='+string(value_out),/gui
  
end
endcase
end

