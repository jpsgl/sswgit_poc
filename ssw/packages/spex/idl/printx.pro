;+
; Name: printx
;
; Purpose: 
; This procedure includes both normal print to screen but also to a text widget if enabled
; E.g.
;	printx, a
;
; keyword base is used to set the default list widget
;	printx, base=default
; 	JOURNAL_OUT- if A1_in is undefined, journal_out is set to journal
;		in the common block, all the lines output from SPEX
;
; to override the default, but not to change it
;	printx, a, base=base
; Inputs:
; 	A1_in- Text string scalar.  N.B. SCALAR
;	
; Keywords:
;	FORMAT-
;	NOPRINT- If set, then A1_in is added to journal, not to output
;	BASE-  Base of text widget to use
;	JOURNAL_OUT- If A1_in is not input, set to total journal to this point.
;	SEARCH- If JOURNAL_OUT option is used, then SEARCH is the INPATTERN for WC_WHERE
;	CASE_IGNORE- passed to WC_WHERE
; Calls:
;	EXIST, WC_WHERE
; History:
; Version 2, ras 13-March-1996, added journal_out
; Version 3, ras, 28-Oct-1996, include WC_WHERE interface
;-
pro printx, a1_in, format=format, base=base, noprint=noprint, $
	journal_out=journal_out, search=search, case_ignore=case_ignore

common text_widg_com, info_base, journal, ijournal
checkvar, journal, strarr(100)
checkvar, ijournal, 0

if keyword_set(base) then begin
	info_base=base
	return
endif

if not exist(a1_in) then begin
	case keyword_set(search) of
	0:	journal_out=journal
	1:      begin
	wc = wc_where(journal, search, mcount, case_ignore=case_ignore)
	if mcount ge 1 then begin
		journal_out = journal(wc)
		for i=0,mcount-1 do printx, journal_out(i)
		endif
	return
	end
	endcase
endif


;if n_elements(info_base) ne 0 then print,'info_base:', info_base

if datatype(a1_in) ne 'STR' then $
	if keyword_set(format) then $
	  a1_intermed = string(/print,format=format,a1_in) else $
	    a1_intermed = string(/print,a1_in) $
	  else a1_intermed = a1_in

a1 = byte([string(bytarr(80)+'32'),a1_intermed]) 

wbl = where( a1 eq 0,nbl)
if nbl ge 1 then a1( wbl ) = 32b
a1 = string(a1(*,1:*))

if keyword_set(format) then a_out = string(/print,format=format,a1) else a_out=string(/print,a1)
 nout = n_elements(a_out)
 if nout+ijournal ge n_elements(journal) then journal=[journal,strarr(100+nout)]
 journal(ijournal) = a_out
 ijournal = ijournal + nout
if n_elements(journal) mod 1000 eq 0 then print,'ijournal: ',ijournal
if n_elements(journal) gt 1e4 then stop

if not keyword_set(noprint) then $
if fcheck(base,fcheck(info_base,-1)) gt 0 then begin

;  if keyword_set(format) then a_out = string(/print,format=format,a1) else a_out=string(/print,a1)
  widget_control,fcheck(base,info_base),/append, set_value=a_out

endif else if keyword_set(format) then print, a1, format=format else print, a1


end

