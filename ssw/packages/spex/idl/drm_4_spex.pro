;+
; PROJECT: SPEX
; NAME:   drm_4_spex
;
;
; PURPOSE:  general purpose detector response matrix (DRM) input proc. for SPEX
;
;
; CATEGORY: SPEX, spectral analysis
;
;
; CALLING SEQUENCE:
;	examples
;
;	drm_4_spex, data_tipe=data_tipe,  det_id=1,
;		high=high, build=build, read=read, dir = dir
;		dfile=['hxrbs::sys$user:[richard.batse.seq_obs]spec_direct_1_468',flare=665, $
;		sfile=['hxrbs::sys$user:[richard.batse.seq_obs]spec_scatter_1_468',  $
;		edges=edges, e_in=e_in, drm=drm, area=area
;
;
; INPUTS:
;	command_in  - input command string, currently allowed are
;		      build_drm, read_drm, recalibrate, cal_save, cal_restore
;	data_tipe   - string array referencing instrument  -
;		      allowed 'BATSE','SHERS';'BATSE','STTE'; 'BATSE','HERS'; 'BATSE','CONT';'YOHKOH/HXT'
;			     YOHKOH/SXS, YOHKOH/HXS, YOHKOH/GRS1, YOHKOH/GRS2, 'SMM/HXRBS',
;		tbd: 'SMM/GRS',
;
;	cosine      - cosine of angle between source and detector axis, defaults to 1.
;	dir	    - first directory to check for drm for read
;	d(s)file    - file or files with the drm to be read
;	flare	    - BATSE archive flare number
;	high_drm    - if set, use high energy drm files
;	test_drm    - 0 default batse drm, 1 1st test batse spec drm, 2 2nd test drm
;       com_param   - strings containing additional commands for drm_4_spex
;	det_id      - detector identifier, for BATSE 0-7
;	edges	    - fltarr(2,nchan)  lo and hi edges in keV for detector channels
;	date        - time of observation for response matrix, ANYTIM format
;       efficiency  - diagonal matrix elements for diagonal option
;	p_drm_reader- name of read procedure for drm type not explicitly allowed
;	need_drm    - if set then build the drm regardless of any other considerations
;	delta_light - pha bin widths in light output units
;	gain_offset - PHA offset for BATSE SPEC fiducial calibration
;	gain_mult   - gain factor for BATSE SPEC fiducial calibration
; OUTPUTS:
;	drm	    - detector response matrix per input photon/cm2 in cnts/cm2/keV
; 	area        - detector area in cm2
;	e_in	    - lo and hi edges (keV) for input side of DRM
;	error       - if set, then no drm was reported
; SIDE EFFECTS:
;	none
;
; RESTRICTIONS:
;	none
;
; PROCEDURE:
;	none
;
; MODIFICATION HISTORY:
;	ras, 10-Mar-94
;	ras, 20-apr-94, added yohkoh hxt
;	ras, 25-may-94 added yohkoh sxs, hxs, grs
;	ras, 26-aug-94 added smm hxrbs
;	ras, 27-sep-94 added hirex germanium
;	ras, 18-oct-94 added batse discsp
;	ras, 3-mar-95 added det_id to call to build_drm for batse
;	ras, 14-mar-95, added need_drm to force drm construction
;	ras, 29-mar-95, modifications to support recalibration
;	ras, 17-jan-96, drm for hxt cal mode
;	Version 2, ras, 8-apr-1996, align structure boundary
;	Version 3, ras, 21-oct-1996, added capability for merged DISCSP_CONT
;	Version 4, ras, 1-nov-1996, added capability for merged DISCSP, MULTI
;	Version 4, ras, 26-mar-1997, added SMM GRS X and Gamma detectors
;	Version 5,
;	richard.schwartz@gsfc.nasa.gov, 29-sep-1997, moved all non-spex
;	calls to call_procedure.
;	Version 5,
;	richard.schwartz@gsfc.nasa.gov, 8-feb-1998, check for changes in
;	channel edges to redo drm.
;	richard.schwartz@gsfc.nasa.gov, 18-feb-1998, protect against NAN universally using
;	idl's finite function, not the woeful ssw routine.
;	23-June-2000 Paul Bilodeau added HXRS capability
; 	25-May-2001 Paul BIlodeau added HESSI build capability - only reads drm
;-

pro drm_4_spex, command, $
  data_tipe=data_tipe, det_id=det_id, com_param=com_param, date=date, $
  error=error, cosine=cosine, efficiency = eff, test_drm=test_drm, need_drm=need_drm, $
  high_drm=high_drm, dir = ddirectory, p_drm_reader=p_drm_reader, $
  flare=flare, edges=edges, e_in=e_in, drm=drm,$
  gain_offset=gain_offset, gain_mult=gain_mult, $
  dfile=dfile, sfile=sfile,  delta_light=delta_light,  area=area

error = 1	;assumes no drm was returned

common drm_4_spex_com, drm_last, last_build, edges_last, e_in_last, area_last
checkvar, last_build, {last_build, data: ['',''], flare: 0L, det_id:-1L, $
	high:0L, ut_date:0.d0}
if need_drm eq 1 then last_build.det_id = -1

if strupcase(data_tipe(0)) eq 'BATSE' then begin
        flare_string = strmid(string(form='(i6)',100000L+flare),1,5)
        case strupcase(data_tipe(1)) of
                'CONT': det_type='lad'
                'HERS': det_type='lad'
                'SHERS':det_type='spec'
                'STTE':det_type='spec'
                'DISCSP':det_type='spec'
		'DISCSP_CONT': det_type=['spec','lad']
                else: det_type='lad'
        endcase
endif

drmcom:
alpha_page

case 1 of


    command(0) eq 'build_drm' or command(0) eq 'read_drm': begin
        if data_tipe(0)+data_tipe(1) eq 'BATSECONT' then begin
            if high_drm then edges(1,15)=3.e4  else edges(1,15) = 8.e3
            endif
        edges_out_drm = edges

        case command(0) of
            'multidrm': call_procedure, "discsp_bands_drm", edges_out=edges_out_drm, area=area,$
                             edges_in=e_in, drm=drm, error=error

            'build_drm': begin
                ;	      Check to see if data specifications have changed before building

                if last_build.data(0) ne data_tipe(0) or last_build.data(1) ne data_tipe(1) or $
                  last_build.flare ne flare or last_build.det_id ne det_id or $
                  last_build.high ne high_drm or $
                  last_build.ut_date ne (anytim(fcheck(date,[intarr(4),1,1,1979]),/sec))(0) or $
                  total(abs(fcheck(edges_last,0.0) - edges_out_drm)) ne 0.0 then begin

                    last_build.data = ''
                    last_build.flare= -1
                    last_build.det_id=-1
                    last_build.ut_date=(anytim([intarr(4),1,1,1951],/sec))(0)

                    CASE 1 OF
                        strupcase(data_tipe(0)) eq 'BATSE':begin
                            printx,string(/print,'Building DRM for ',data_tipe)
			    if strupcase(data_tipe(1)) eq 'DISCSP_CONT' then $
                             call_procedure,"merge_batse_drm", flare=flare, edges_out=edges_out_drm, area=area,$
                             edges_in=e_in, drm=drm, error=error  $
                            else begin
                            drm=call_function("build_drm",$
                              cos=cosine, high=high_drm, chan_edges=edges_out_drm, $
                              det_type=det_type, e_in=e_in, det_id=det_id, area=area, test_drm=test_drm)
                            error = 0
                            endelse
                        end
                        strupcase(data_tipe(0)+data_tipe(1)) eq 'SMMHXRBS': begin
                            printx, string(/print,'Building DRM for SMM HXRBS')
                            call_procedure, "hxrbs_response",$
                            edges=edges_out_drm, omatrix=drm, area=area, $
                              e_matrix= e_in, ut_date=date  ;resolution passed via common block!
                            error = 0
                            end
                        strupcase(data_tipe(0)+data_tipe(1)) eq 'SMMGRSX': begin
                            printx, string(/print,'Building DRM for SMM GRS XRAY DETECTORS')
                            call_procedure,"grs_xray_resp", edges_out=edges_out_drm, drm=drm, area=area, $
                              edges_in= e_in
                            error = 0
                            end
                        strupcase(data_tipe(0)+data_tipe(1)) eq 'SMMGRS': begin
                            printx, string(/print,'Restoring DRM for SMM GRS DETECTOR')
                            call_procedure,"grs_resp", edges_out=edges_out_drm, drm=drm, area=area, $
                              edges_in= e_in
                            error = 0
                            end
                        strupcase(data_tipe(0)+data_tipe(1)) eq 'HIREXGERMANIUM': begin
                            printx, string(/print,'Building DRM for HIREX GERMANIUM')
                            e_in = edges_out_drm
                            edge_products, e_in, edges_2=e_in
                            edge_products, edges_out_drm, width=ww
                            nww = n_elements(ww)
                            drm = fltarr( nww, nww)
                            ;give the detector response matrix a diagonal response
                            if strupcase(com_param(0)) eq 'DIAGONAL' then $
                              drm( indgen(nww)*(nww+1) ) = eff/ww(indgen(nww))  else begin
                                printx,'Count rate response matrix not implemented for HIREX GERMANIUM'
                                goto, drmcom
                                endelse
                            error = 0
                            end
                        ; Added 5/31/2000 - Paul Bilodeau
                        strupcase(data_tipe[0]) eq 'HXRS': begin
                        	printx, string(/print,'Building DRM for HXRS')
                        	if n_elements(dfile) ne 0 then begin
                        		drm_to_read = concat_dir(ddirectory,dfile)
                        		printx, 'Found response file: ' + drm_to_read
                        		p_drm_reader='hxrs_fits2drm'
								tmp=get_logenv('SSWDB_HXRS')
								if strpos(drm_to_read, tmp) eq -1 then drm_to_read=concat_dir(tmp, drm_to_read)
									rd_drm_4_spex, drm_to_read=drm_to_read, scatter_to_read=scatter_to_read, $
                  						data_tipe=data_tipe, date=date, p_drm_reader=p_drm_reader, $
                  						edges_out_drm=edges, e_in=e_in, drm_out=drm, area=area, $
                  			        	error=error
                        		endif else begin
                        			call_procedure,"hxrs_response", edges_out=edges_out_drm, drm=drm, area=area, $
                              		edges_in=e_in, dfile=dfile
                              	endelse

                            error = 0
                            end
                        ; Added 25-May-2001 - Paul Bilodeau
                        strupcase( data_tipe[0] ) eq 'HESSI': begin
                        		printx, string(/print,'Reading DRM for HESSI - build is not implemented.')
                        		error = 0
                        		if n_elements(dfile) NE 0 then begin
                        			drm_to_read = find_file( dfile, COUNT=count )
                        			if count gt 0L then begin
                        				p_drm_reader = 'hessi_fits2drm'
                        				rd_drm_4_spex, drm_to_read=drm_to_read, scatter_to_read=scatter_to_read, $
                  						data_tipe=data_tipe, date=date, p_drm_reader=p_drm_reader, $
                  						edges_out_drm=edges, e_in=e_in, drm_out=drm, area=area, $
                  			        	error=error
                  			        endif else error = 1
                        		endif else error = 1
                        	end
                        else: begin
                            printx,string(/print,'Build_drm not implemented for ',data_tipe)
                            goto, drmcom
                            end
                        endcase
                    ;	           Record the last drm built.
                    last_build.data  = data_tipe
                    last_build.flare = flare
                    last_build.det_id    = det_id
                    last_build.high  = high_drm
                    last_build.ut_date = anytim(fcheck(date,'79/1/1'),/sec)
                    drm_last = drm
                    edges_last = edges_out_drm
                    e_in_last  = e_in
                    area_last  = area
                    endif else begin
                    drm = drm_last
                    edges_out_drm = edges_last
                    e_in = e_in_last
                    area = area_last
                    endelse
                end

            'read_drm': begin

                data_tipe_drm = data_tipe
                if strupcase(data_tipe(0)) eq 'YOHKOH' then begin
                    if strupcase(data_tipe(1)) eq 'HXT'  then begin
						if n_elements(edges(0,*)) eq 4 then $
                        	call_procedure,"rd_hxt_drm", drm, edges, e_in, area, error=error else $
							call_procedure,"hxt_cal_drm", drm, edges, e_in, area, $
                        delta_light=delta_light, error=error
                        goto, AROUND
                        endif
                    if strpos(strupcase(data_tipe_drm(1)),'SXS') eq 0 then $
                    	data_tipe_drm(1) = data_tipe_drm(1)+strtrim(det_id,2)

                    if strpos(strupcase(data_tipe_drm(1)),'GRS') eq 0 then $
                      	data_tipe_drm(1) = data_tipe_drm(1)+strtrim(det_id,2)

                    ;	     if strpos(strupcase(data_tipe_drm(1)),'HXS') eq 0 then no action needed
                    ;

                    endif

                ;Mainly a BATSE drm reader now, 20-apr-94, yohkoh sxs added 24-apr-94, hxs, grs, 25-may-94

                drm_to_read = concat_dir(ddirectory,dfile)
                if sfile ne '' then $
                scatter_to_read = concat_dir(ddirectory,sfile) else $
                scatter_to_read = ''

				; added 22-June-2000, Paul Bilodeau
				; set file to read via 'dfile, file' at the prompt
				if strupcase(data_tipe[0]) eq 'HXRS' then begin
					p_drm_reader='hxrs_fits2drm'
					tmp=get_logenv('SSWDB_HXRS')
					if strpos(drm_to_read, tmp) eq -1 then drm_to_read=concat_dir(tmp, drm_to_read)
					endif

				if strupcase(data_tipe[0]) eq 'HESSI' then p_drm_reader = 'hessi_fits2drm'

                rd_drm_4_spex, drm_to_read=drm_to_read, scatter_to_read=scatter_to_read, $
                  data_tipe=data_tipe_drm, date=date, $
                p_drm_reader=p_drm_reader, edges_out_drm=edges, e_in=e_in, drm_out=drm, area=area, $
                  error=error
                AROUND:
                last_build.data = ''
                last_build.flare= -1
                last_build.det_id=-1
                last_build.ut_date=anytim('51/1/1',/sec)
                end
            endcase ; closed inner case loop for building or reading drms

        if total(abs(edges_out_drm - edges)) ne 0.0 then begin
            printx,'WARNING, EDGES changed by DRM creation'
            edges = edges_out_drm ;check that edges don't exceed range of drm file
            endif
        goto, leavedrm

        end  ;closes outer block for building and reading

    command(0) eq 'top': begin
        top: goto, leavedrm
        end
    command(0) eq 'recalibrate': begin	;applies to BATSE SPEC
        if fcheck(com_param(0),'') eq '' then begin
          last_build.det_id = -1
          call_procedure, "calibrate",/report, $
             error=error,ndet, width64, spec_lld, nlines, chanlines, enlines,evec, olight=lite
	  edge_products, evec, edges_2=edges
	  edge_products, lite, width= delta_light
          command = 'build_drm'
        endif else begin
	  ;for batse spec only
	  file=arr2str(['ss', strtrim([call_function("burst_flare",flare,/fla),det_id],2),'dirmat'],delim='_')+'.fits'
	  call_procedure,"read_compressed", file, nph,e_in,nedges, bedges,mat_type, bfitsdrm
          edge_products, bedges, edges_2=bedges, wid=wedge
	  case strupcase(com_param(0)) of
         'BFITSDRM':  begin
		bfitsdrm = transpose(bfitsdrm)/127.
		bfitsdrm = temporary( f_div( bfitsdrm, rebin( reform(wedge,nedges,1),nedges,nph) ) )
	 	edge_products, e_in, edges_2=e_in
		delta_light(4:*) = wedge
		drm = fltarr(nedges+4,nph)
		drm(4:*,*) = bfitsdrm
	  	edges(*,4:*) = bedges
	 	goto, leavedrm
	  end
          'BFITSCAL':  begin  ;just recalibrating to edges of fits file! Make SPEX DRM.
	  	edges(*,4:*) = bedges
		edge_products, edges, edges_1=evec
		call_procedure,"g_c", evec, lite & lite = evec
		edge_products, lite, width= delta_light
		command = 'build_drm'
	  end
	  else: begin
		 printx,['Warning, No action taken on RECALIBRATE', $
			'Use commands RECALIBRATE, ', $
			'             RECALIBRATE,BFITSDRM', $
                        ' or RECALIBRATE,BFITSCAL']
		 goto, leavedrm
	  end
	endcase
	endelse
        end
    command(0) eq 'cal_save': begin
        gain_offset = spex_current('gain_offset')
        gain_mult   = spex_current('gain_mult')
        if dfile eq '' then $
        dfile = arr2str(delim='-',['batse','spec','cal', $
          strtrim(det_id,2),strtrim(flare,2), $
          strtrim(fix(gain_offset*10.),2),$
          strtrim(fix(gain_mult*1000.),2),'.sav'])
        cal_file = concat_dir(chklog(ddirectory),dfile)
        call_procedure,"calibrate",/report, ndet, width64, spec_lld, nlines, chanlines, enlines, error=error
	if error eq 0 and ndet eq det_id then $
          save,file=cal_file,/verb,/xdr, edges, e_in, drm, gain_offset, gain_mult,$
            det_id, cosine, delta_light, high_drm, flare, $
            width64, spec_lld, nlines, chanlines, enlines  else $
          printx,'Inconsistent/non-existent calibration data.'
        goto, leavedrm
        end
    command(0) eq 'cal_restore': begin
        cal_file = concat_dir(chklog(ddirectory),dfile)
        restore, cal_file, /verb
        call_procedure,"calibrate", det_id, width64, spec_lld, nlines, chanlines, enlines, error=error
        command  = 'recalibrate'

        end
    ;When in doubt,
    else: goto, leavedrm
    endcase

goto, drmcom

leavedrm:
;protect against NAN
wnan = where(finite(drm) eq 0, num_nan)
if num_nan ge 1 then drm( wnan ) = 0.0
printx, string(/print,'minmax(drm)',minmax(drm))

end
