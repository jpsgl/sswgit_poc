;+  
; PROJECT: SDAC
;
;
; NAME:   read_yohkoh_4_spex
;
;
; PURPOSE:  This procedure controls the input of YOHKOH spectral data for SPEX.
;
;
; CATEGORY:
;	YOHKOH, SXS, I/O, SPEX
;
; CALLS:
;	SPEC_SENSITIVITY, READ_CONT, READ_DISCSP, CONT_EDGES, FCHECK, CHECKVAR,
;	F_DIV, DPRATE2SEC, LIMITS, AVE_CTS2, ANYTIM, GT_DP_MODE
;	RESTORE_OVERFLOW CHKARG CLEAR_UTPLOT DET_COSINES EDGE_PRODUCTS FITS_SPECTRA
;	GET_HXRBS_FITS HXT_CAL_FIX PRINTX RD_ROADMAP RD_SXS_PHA RD_WBS_PHA RD_XDA 
;	SHERS_LOAD  SPEX_INTERVALS SXS_ECAL UTPLOT WBS_RESPONSE EXIST ATIME
; 
; CALLING SEQUENCE: 
; read_yohkoh_4_spex, dformat=dformat, files=files, $
; start_time = start_time, end_time=end_time, title=title, command_out=command_out, $
; det_id=det_id, id_style=id_style, p_read_data=p_read_data, _extra=_extra, $
; flux=flux, eflux=eflux, ut=ut, units=units, area=area, ltime=ltime, edges=edges,$
;       id_use=id_use, auto=auto, noplot=noplot, mode=mode, wchan=wchan, delta_light=delta_light
; examples
; 
; read_yohkoh_4_spex, instrument='YOHKOH', dformat='HXT', det_id=1, id_style='id', id_use=id_use
;         files=HDA_FILE, $
;         flux=flux, eflux=eflux, ut=ut, units=units, area=area, ltime=ltime, edges=edges
;
;
; INPUTS:
; dformat     - data format:
;                  YOHKOH-  HXT, SXS, HXS, GRS
; files       - file or files with the data to be read
; flare       - BATSE archive flare number
; start_time  -  
; end_time    -
;               optional, read file between start_time and end_time, defaults
;               are the start and end of the files, must be readable by ANYITM
;
; det_id      - detector identifier, for GRS 0-1 
; id_style    - NO MEANING
; _extra      - keyword inheritance in version >3.1
; /auto       - non-interactive calibration
; /noplot     - no plotting of calibration activities
; p_read_data - name of read procedure for data type not explicitly allowed
; OUTPUTS:
; flux        - fltarr(nchan,nbins) - counts/cm2/sec/keV, overflow and livetime corrected
; eflux       - fltarr(nchan,nbins) - sqrt(counts)/cm2/sec/keV
; ut          - dblarr(2,nbins), seconds referenced to 79/1/1
; units       - e.g. 'cm!u-2!n s!u-1!n keV!u-1!n'
; area        - detector area in cm2
; ltime       - fltarr(1,nbins)  accumulation live time in interval, seconds
;		changed to fltarr(nchan,nbins) - 20-oct-94
; edges       - fltarr(2,nchan)  lo and hi edges in keV
; delta_light - fltarr(nchan), for BATSE SHER, channel width in pulse height
;               see cal_sher and calibrate, and Band 1992 on SLED problem for 
;               discussion.
; id_use      - actual detector id, to reference drm
; title       - title string referencing instrument, data
; mode        - special output for YOHKOH, mode=0 for non-flare, 1 for flare
; wchan       - indices of allowed channels
; command_out - further instructions to SPEX, e.g. "read_drm", may also be used for input
; SIDE EFFECTS:
; none
;
; RESTRICTIONS:
; none
;
; PROCEDURE:
; This procedure provides a 1-stop shop for all YOHKOH data types readable through SPEX.
; The correct reader is identified for each data type, it is called for the input
; data file.  The correct PHA edge generator is also called.  Corrections are made
; to the data for livetime and overflow and all vectors by SPEX are returned.
;
; MODIFICATION HISTORY: 
; Version 1
;	richard.schwartz@gsfc.nasa.gov, 26-sep-1997, extracted
;	from read_4_spex
; Version 2
;	richard.schwartz@gsfc.nasa.gov, 27-oct-1997, extend
;	start_time and end_time to sxs section.
;-
pro read_yohkoh_4_spex, dformat=dformat, files=files, $
start_time = start_time, end_time=end_time, flare=flare, $
det_id=det_id, id_style=id_style, p_read_data=p_read_data, $                      
flux=flux, eflux=eflux, ut=ut, units=units, area=area, cosines=cosines, $
ltime=ltime, edges=edges, delta_light=delta_light, wchan=wchan, id_use=id_use, title=title, $
auto=auto, noplot=noplot, mode=mode, error=error, $
command_out=command_out, efficiency = eff, _extra = _extra                        


common read_spex_com, data, ut_data, data_file_read, read_start, read_end
checkvar,data,0
checkvar,data_file_read,''
checkvar,read_start,''
checkvar,read_end,''
checkvar, command_out, ''
if command_out(0) ne '' then com_param=command_out else com_param=''
delta_light= 1.0
checkvar,    units, ' s!u-1!n cm!u-2!n keV!u-1!n'
format = strupcase(dformat(0))

command_out = 'read_drm'              ;Sun is in a fixed position, so always read drm
data_file = files(0)
if format eq 'HXT' then begin
    ;if strpos(data_file,
    rd_roadmap, data_file, roadmap
    ;Here we select on a low resolution light curve of the lowest hxt channel
    clear_utplot
    if !d.name eq 'X' or !d.name eq 'TEK' then begin
        result = execute('y = gt_sum_l(roadmap, title=title)')        ;akt 94/4/28
        psav=!p
        ut = anytim(roadmap,/sec) - anytim(roadmap(0),/date,/sec)
        replot: utplot, ut, y, anytim(/date,roadmap(0),/yohkoh),title=title, psym=1
        dt = dprate2sec(roadmap)/4. + ut*0.0 ;time duration of each full frame
        spex_intervals, ut, zoom, xwidth=dt, $
        style='b', v_styles=['b'], /query,color=fcolor(7), $
        xoffset= (indgen(3)*200+200),yoffset = (indgen(3)*200+200), $
        message='SELECT XRANGE FOR ZOOM',  error=error
        ssss = where( ut ge (limits(zoom))(0) and ut lt (limits(zoom))(1), nsss)
        if nsss le 1 then goto, replot
        ;ssss=plot_lcur(roadmap, y, title=title)
        !p = psav
        endif else ssss = indgen(n_elements(roadmap))
    printx,'HXT data selected.'
    help, ssss
    ;Look for HXT cal data
    dtypes=roadmap(ssss).datarectypes
    wdtype2 =where(dtypes eq 2, ndtype2)
    if ndtype2 ge 1 then begin
        ssss=ssss(wdtype2)
        dtypes = dtypes(wdtype2)
        use_hxt_cal_fix=1
        endif
    
    ;ssss is the index array into index and data
    if not fcheck( use_hxt_cal_fix,0) then begin
        rd_xda, data_file, ssss, index, data
        
        flux = ave_cts2(index, data, time=ut,dt=dt, datarectypes=dtypes)
        endif else begin
        hxt_cal_fix, data_file, ut, dt, flux, sel_det = sel_det, edges=edges_hxt, iout=index
        ut = anytim( ut, /sec) - anytim(index(0),/sec)	
        endelse
    smode = gt_dp_mode(index,/string)
    mode  = dt*0
    wflare= where(strtrim(smode,2) eq 'Flare',nmode)
    if nmode ge 1 then mode(*,wflare)=1
    nbin = n_elements(ut)
    if ndtype2 eq 0 then nchan = 4 else nchan = 64
    flux = reform(flux,nchan,nbin) ;cnts/s/sc
    dt   = dt(*)
    ut   = ut(*) + anytim( index(0), /sec)
    ut = transpose( reform([ut-dt/2,ut+dt/2],nbin, 2))
    area = 55. ;cm2 for HXT
    if exist(edges_hxt) then edges=  edges_hxt else $
    edges= [13.9,  22.7,  32.7, 52.7, 92.8]
    edge_products, edges, edges_2=edges, width=ww
    ltime= dt ;no livetime correction thought to be needed
    num_det = n_elements( sel_det) *1.0
    if num_det eq 0 then num_det=64.
    flux = flux * rebin( reform(dt,1,nbin),nchan,nbin) * 64. ;counts / channel / time bin
    title= 'YOHKOH HXT '+' '+atime(/yoh, ut(0), /date)
    wchan=indgen(nchan)
    ;stop
    endif   ;close HXT selection
if strpos(format,'SXS') eq 0 then begin
    if det_id eq 1 then sxs1=1 else sxs1=0
    sxs2 = 1 - sxs1 ;logicals for desired data
    rd_sxs_pha, start_time, end_time, pha=flux, infil=data_file, sxs1=sxs1, sxs2=sxs2, live=ltime, $
    ut=ut, title=title,/new
    if sxs1 then area = 11.95 else area = 6.12        ;nominal geometric areas, must be reconciled with drm
    ;hard code in the amp_mode as a function of data
    if (anytim(ut(0),/sec))(0) gt (anytim(/sec,'18-jun-92'))(0) then $
    amp_mode=1 else amp_mode=0
    
    sxs_ecal, echan1,echan2, amp_mode=amp_mode , hv_mode=0 
    if sxs1 then edges = echan1 else edges = echan2
    ;Empirically from calibration another factor
    edges = edges/1.11
    if not sxs1 then edges = edges * 1.3      ;we have no idea of the calibration for now, ras, 20-may-94
    edge_products, edges, width=ww
    nchan=128
    wchan=indgen(125)+3
    nbin = n_elements(ltime)
    endif
if strpos(format,'HXS') eq 0 then begin
    error=0
    rd_wbs_pha, exp='hxs', pha=flux, infil=data_file, error=error, $
    live=ltime, ut=ut, title=title,/new
    if error then return
    wbs_response, 'hxs', date=ut(0), area=area, edges_out=edges, error=error
    if error then return
    edge_products, edges, width=ww
    nchan=32
    wchan=indgen(30)+1
    nbin = n_elements(ltime)
    endif
if strpos(format,'GRS') eq 0 then begin
    error=0
    exp = 'grs'+strtrim(det_id,2)
    rd_wbs_pha, exp=exp, pha=flux, infil=data_file, error=error, $
    live=ltime, ut=ut, title=title,/new
    if error then return
    wbs_response, exp, date=ut(0), area=area, edges_out=edges, error=error
    if error then return
    edge_products, edges, width=ww
    nchan=128
    wchan=indgen(100)+3
    nbin = n_elements(ltime)
    endif


;convert to fluxes, counts/s/cm2/keV
count_2_flux =  $
rebin(reform(ltime,1,nbin),nchan,nbin) * area * rebin(ww(*),nchan,nbin)
eflux = f_div(sqrt(flux), count_2_flux) ;sqrt counts/ factors
flux  = f_div(flux, count_2_flux)       ; counts / factors




end
