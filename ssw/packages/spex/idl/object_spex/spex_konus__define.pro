;+
; Name: spex_konus__define
;
; Purpose: Provide methods to deal with the konus data when downloading,etc. for OSPEX.
; 
; Method: In search, calls spex_find_data, which uses the konus class to find and copy the data files.
; In select, only search for *.pha files. Don't need to set the SRM file (but need to have the rmf and
; arf files in dir since konus reader reads them and inserts the DRM arrays directly into ospex).
;
; Written: Kim Tolbert, July 2016
;
;-

function spex_konus::init
  return,1
end

;-----

function spex_konus::search, time_range, count=count, _ref_extra=extra

  spex_find_data, 'konus', time_range, url=url, count=count, _extra=extra
  
  if count eq 0 then return, ''
  
  return, url
end

;-----
; 
pro spex_konus::select_files, files, time_range, specfile, drmfile

  specfile = ''
  drmfile = ''
  q = where (strpos(file_basename(files), '.pha') ne -1, count)
  if count gt 0 then begin
    specfile = files[q[0]]    
  endif
  
end

;-----

pro spex_konus__define
  void = {spex_konus, inherits spex_instr}
end