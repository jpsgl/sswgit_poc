;+
; Function to return the structure of default values for the specified function
; Keywords:
; nparam - if set, then return the number of parameters instead of the structure
; Modifications:
;  30-Sep-2005, Kim, Make sure function routine is defined as well.  Previously only
;    checked that defaults routine was defined.
;
;-

function fit_comp_defaults, fit_comp, nparam=nparam

; first make sure function routine and defaults routine for function exist
proc = strlowcase( 'f_'+fit_comp)
if not have_proc(proc) then begin
	message,'Undefined function routine : ' + proc, /cont
	return,-1
endif

def_proc = strlowcase( 'f_'+fit_comp+'_defaults' )
if not have_proc(def_proc) then begin
	message,'Undefined defaults routine for function: ' + def_proc, /cont
	return,-1
endif

defaults = call_function (def_proc)

if keyword_set(nparam) then return, n_elements(defaults.(0)) else return, defaults

end