
; This is the function that is called from inside of mcurvefit while it's iterating.
; It calculates the fit function for the new values of the parameters.  The function
; will return photons/cm^2/sec/keV, so then we convert that to counts/sec by
; using the drm.
;
; Input:
;   x - x values
;   params - new values of params for function
;   private - the spex object, so we have access to the drm
; Output:
;   yfit - function values in counts/sec as a function of x
;
; Modifications:
;	27-Jun-2006, Kim.  Added func_obj to call to apply_drm to
;	  accomodate new drm_mod function for RHESSI (to fine-tune detector params in DRM)
;	8-Aug-2006, Kim.  Get spex_fit_obj out of spex_obj to get access to index_range and
;	  drm, since they're no longer in the object chain for fitalg
;
;

;------------------------------------------------------------------------------

pro spex_fitalg_mcurvefit_eval, x, params, yfit, private=spex_obj

yfit = x * 0.

;print, '  Parameters = ', arr2str(trim(params[*]), ', ')

yfit = spex_obj -> getdata(class_name='fit_function', fit_comp_params=params)

; index_erange are the indices of the counts energy edges in drm to use.
spex_fit_obj = spex_obj->get(/spex_fit_obj)
index_erange = spex_fit_obj -> getdata(class='spex_fitrange')

; previously called convert_fitfunc_units, but that's more general, so does more stuff, faster
; to just call apply_drm directly - it returns counts/sec
;yfit = spex_obj -> convert_fitfunc_units (yfit, index_erange=index_erange, spex_units='rate')
yfit = (spex_fit_obj->get(/obj,class='spex_drm')) -> $
	apply_drm(yfit, index_erange=index_erange, func_obj=spex_obj->get(/obj,class='fit_function') )

end
