;+
; Name: SPEX_DRM__DEFINE
;
; Purpose: This object manages the Detector Response Matrix for OSPEX.
;
; Category: OSPEX
;
; Written: 2003, Kim Tolbert
; Modifications:
;   15-Jul-2004, Kim.  Handle DRM files with multiple filter states
;   16-Jul-2004, Kim.  Move preview method here from spex__define.
;   23-Aug-2004, Kim.  Added albedo correction stuff
;   10-Oct-2004, Kim.  In get_diagonal, check for drm[0]=-1 before using
;   19-Nov-2004, Kim.  In read_drm, if xsm data, call xsm drm reader.  Also changed name of hessi
;     drm reader to spex_hessi_fits2drm.
;   6-Jun-2005, Kim.  Changed solar radius in ::set angle calculation from 916 to 960 arcsec.
;	19-Sep-2005, Kim.  For imagecubes, process wasn't getting called again when the imagecube file
;	  and therefore respinfo was changed (because respinfo is an info parameter of spex_data.
;	  spex_data doesn't know anything about spex_drm, so it can't set a control parameter here.)
;	  So save value of respinfo in self, and if it changes, then set need_update.
;	9-Feb-2006, Kim.  Added ability to calculate drm if data is rhessi and spex_image_full_srm
;	  is set (for image cube, this flags made input spectrum counts instead of photons, and then
;	  calculates full drm to convert to photons.)
;	  Added need_update method, so can set need_update if respinfo or image_full_srm change,
;	  even though they aren't control parameters of drm.
;	  Rewrote process method to use a file, array, or build method and added array_drm and
;	  build_drm methods.
;	  Modified preview method to handle the build method.
;	  Only allow albedo correction if drm was read from file.
;	7-Apr-2006, Kim.  changed get_diagonal args.  Previously passed in func name and array of
;	  params, now pass in fit function object with everything already set in it.  This is better
;	  now that we have keywords to set in function as well as params.
;	27-Jun-2006, Kim.  Added func_obj to args to apply_drm and call to apply_drm in get_diagonal to
;	  accomodate new drm_mod function for RHESSI (to fine-tune detector params in DRM).  When one
;     of the functions used is 'drm_mod', then calls the hsi_drm_mod obj to compute drm instead of
;     using the one stored.
;	10-july-2006, used this_filter or info param to pass correct
;		atten_state into hsi_drm_mod object
;	14-Jul-2006, Kim.  Use data_name from units structure to assign data name for drm in
;		array_drm
;	18-Jul-2006, Kim.  Set atten_state into hsi_drm_mod each time, not just when obj is created.
;	06-Oct-2006, Kim.  Added pileup_mod changes in apply_drm. pileup_mod is another pseudo
;		function (like drm_mod) that lets us add in pileup effects to the expected spectrum.
;	11-Oct-2006, Kim.  Added 3rd parameter (smoothing) in call to pileup_mod
;	15-Oct-2006, Kim.  Added 4th parameter (mult. factor) for pileup_mod
;	17-Oct-2006, Kim.  Pileup was returning ctr rate as double, and this was
;		causing mcurvefit to crash when only fitting one variable (IDL bug!)- so
;		make pileup_mod output a float.
;	30-Oct-2006, Kim.  Changed calls to hsi_drm_mod because of changes in hsi_drm_mod
;	14-Nov-2006, Kim.  When drm input file changes, set need_update in spex_drm_mod_obj
;	16-Jan-2008, Kim, Richard - changed pileup_mod to use modified pileup_countrate
;	8-Feb-2008,  Kim. In apply_drm method: Added disable_pileup_mod keyword - if set, don't
;		apply pileup correction even if pileup_mod is a component (so we can plot separate
;		function components without pileup corr.)
;	1-may-2008, ras, added blanket_coeff to modify the xsec coefficient in hsi_blanket_model
;		this parameter replaces center_thick ratio in function param list
;	13-May-2008, Kim.  added drm_obj keyword to call to spex_xsm_fits2drm
; 25-Jun-2008, Kim. In read_drm, if no file selected, abort
; 28-Jul-2008, Kim.  In read_drm, if area doesn't agree with data area, print warning message,
;   and force data area = drm area.
; 13-Aug-2008, Kim, Richard - added gain_mod function - changes ebins of model during fitting
; 19-Aug-2008, Kim.  Modify gain_mod - pass flux to interp2integ
; 27-Aug-2008, Kim.  Modify gain_mod.  Don't allow new edges to extend beyond old edges, otherwise
;   adds new bogus values depending on gradient at end of old edges
; 15-Dec-2008, Kim.  If multiple detectors, allow drm_mod, but don't use it.  Print a
;   message to that effect (but not for than once every 20 sec)
; 14-Jan-2009, Kim. In apply_drm, if no values in countrate array are positive, don't call
;   pileup_countrate (since it crashes) - means we have a nonsense array anyway.
; 24-Feb-2009, Kim. Save specfile value in process.  In need_update check if it's changed. (previously
;   just checked respinfo and full_srm values, but wrong e.g. if input files same except for
;   different detectors - didn't reprocess but should (shown by area wrong))
; 12-Aug-2009, Kim.  Added cleanup method
; 28-Oct-2009, Kim.  In read_drm, added branch for GBM
; 28-Nov-2009, Kim.  In array_drm, added case statement for yohkoh case
; 16-Dec-2009, Kim.  In apply_drm and getdata, check spex_ignore_filters flag
; 19-Dec-2009, Kim.  Need to use self -> get(/spex_drm_dataobj) to get ignore_filters flag - not
;   known in drm object.
; 31-Dec-2009, Kim.  Save spex_data_sel in self.data_sel, and in need_update, check if changed.
; 21-Apr-2010, Kim.  Previously did albedo correction to drm in process method when we first read it in,
;   and stored drm with albedo correction as the DRM data product.  However, if use drm_mod, it
;   recalculates the DRM and albedo correction is lost.  Instead do it in apply_drm and now apply
;   the albedo correction to the photon spectrum. Use the new spex_albedo obj so that we don't
;   do more calculations than necessary (would be slow).
; 10-May-2010, Kim. Now albedo is a function component that we can fit on.
; 07-Jul-2010, Kim. Added ability to use drm that is a function of time (e.g. fermi gbm).  Added
;   this_time keyword to getdata, apply_drm, get_diagonal.  In process, if times are returned in structure
;   from instrument-specific read drm, store them in new info param spex_drm_times.  In getdata, if spex_drm_times
;   has valid times, use time to return correct drm (combine drms within requested time interval by weighted average).
;   Added last_drm and last_use_time as properties - if nothing's changed, then don't need to re-calculate weighted average.
; 25-Jan-2011, Kim.  In read_drm, added branch for Fermi LAT
; 06-Jun-2011, Kim. In preview, force detused to be a string
; 16-Oct-2011, Kim. In preview, if sepdets set, don't use n_detectors (doesn't exist in struct), use n_elements( det array)
; 26-Nov-2011, Kim. Added disable_albedo to apply_drm (like disable_pileup_mod). Change yfit arg name in apply_drm to protect
;   changed version from being passed out.  Added energy keyword to ::albedo (default is ph edges, but may want ct edges)
; 26-Jan-2012, Kim. If GBM data file is a CTIME file, group energy edges in CSPEC DRM so we can use for CTIME.
; 05-Oct-2012, Kim. Small change in apply_drm (in drm_mod part) to get spex_drm_dataobj just once.
; 31-Oct-2012, Kim, Richard. In gain_mod method, don't limit new_ct to ct_edges range. Causes crash.
; 19-Feb-2013, Kim. In array_drm, get hxrbs ph_edges from common
; 21-Aug-2013, Kim. Added drm_mod2 component - in apply_drm, use center_thick_ratio parameter if comp is drm_mod2
; 05-Nov-2013, Kim. When using pileup_mod with multiple detectors (which is not recommended!), we need to divide the
;   countrate by the number of detectors, and then multiply the result by ndet.  pileup_countrate routine assumes the
;   countrate is from a single detector.
; 17-feb-2014, richard.schwartz@nasa.gov, added structure possibility to array_drm test in process, getdata and array_drm
;		modified apply_drm to support drm_mod for MESSENGER SAX. Added protection for drm_mod so
;		it only can be used with HESSI or MESSENGER
; 02-Mar-2014, Kim. In apply_drm, handle the case where drm is 1-D when checking number of elements
; 03-mar-2014, richard.schwartz, activate gain parameter for messenger response drm_mod, drm_param[2], multiplier so
;		default value should be one
; 11-Apr-2014, Kim. In apply_drm, added this_strat keyword, and code to compute and add in the 'nodrm' components after the
;   DRM has been applied to the combined function (for combined function the 'nodrm' components were an array of zeroes)
; 16-Apr-2014, Kim. In set, if source_xy is [-9999,-9999], don't compute angle from it - just set to default of 45 deg
; 24-Apr-2014, Kim. Tweak to the 'nodrm' component calculation in apply_drm. Recalculate without /do_calc to put
;   'normal' values of zeroes back into setdata.
; 13-May-2013, Kim. nodrm component always needs to be computed on count energy edges since we're adding it to
;   the model flux after it's in the count edges. Previously was using whatever was in the func_obj's fit_xvals,
;   which were photon edges while fitting, but count edges while plotting.  Also now make a temporary fit_comp
;   object instead of using internal obj, so don't have to worry about setting energy edges back and forth, and also
;   setting the data for setdata back to all zeroes after calling with /do_calc.
; 14-May-2013, Kim. Use check_math to avoid printing underflow errors in drm matrix multiplication
; 25-Nov-2014, Kim. In read_drm, if can't figure out what reader to call, try getting spex_file_reader, appending
;   '_drm' and calling that.  (used when input data type is unknown, and data strategy is 'spex_any_specfile')
; 29-Sep-2015, Kim.  Some code in read_drm for LAT about rebinning - commented out for now, but don't want to lose
; 13-Nov-2017, Kim. Clarified units comment in process method. Units coming in are cts / photon / keV.  In process
;   setdata, units are cts/s per ph/cm2/s/keV (same as (cts / photon) *cm^2 * keV)
;-


;------------------------------------------------------------------------------
function spex_drm::init, source = source, _extra=_extra

;if not keyword_set( source ) then source = obj_new( 'spex_fitint', _extra=_extra )

retval = self->framework::init( source=source, $
                   control={spex_drm_control}, $
                   info={spex_drm_info}, $
                   _extra=_extra )

self -> set, spex_albedo_correct=0, $
    spex_anisotropy=1., $
    spex_source_xy=[-9999.,-9999.]

; set spex_source_angle after spex_source_xy because otherwise spex_source_xy
; takes precedence
self -> set, spex_source_angle=45.

self.respinfo = ptr_new(-1)
self.image_full_srm = -1
self.specfile = ptr_new('')
self.data_sel = ''
self.last_drm = ptr_new(-1)

return, retval

END

;------------------------------------------------------------------------------

pro spex_drm::cleanup
free_var, self.respinfo
free_var, self.specfile
free_var, self.last_drm
destroy, self.albedo_obj
self->framework::cleanup
end

;------------------------------------------------------------------------------

; if source_xy is provided, calculate angle from xy
; if source_angle is given, but not source_xy, set xy to -[9999,-9999].
; So angle calculated from xy coords (if provided) takes precedence over
;   angle supplied directly.

pro spex_drm::set, spex_source_xy=spex_source_xy, $
       spex_source_angle=spex_source_angle, $
       spex_drm_current_time=spex_drm_current_time, $
       done=done, not_found=not_found, $
       _extra=_extra

;if exist(spex_source_xy) then print,spex_source_xy

if keyword_set(spex_source_xy) then begin
    ; if pos is [-9999., -9999.], then it's unknown, so set angle to default value of 45.
    if same_data(spex_source_xy, [-9999., -9999.]) then spex_source_angle = 45. else $
      spex_source_angle = asin( (sqrt(total(spex_source_xy^2.))/960.) < 1) * !radeg
endif else begin
    if keyword_set(spex_source_angle) then spex_source_xy = [-9999., -9999.]
endelse

if exist(spex_drm_current_time) then time = anytim(spex_drm_current_time)

;if exist(spex_source_angle) then print,spex_source_angle
self->framework::set, spex_source_angle=spex_source_angle, $
    spex_source_xy=spex_source_xy, $
    spex_drm_current_time=time, $
    _extra=_extra, done=done, not_found=not_found
end


;------------------------------------------------------------------------------
; drm object holds drm for all filter states or times in its setdata data.  Filters that
; the array elements correspond to are in spex_drm_filter.  i.e. if 1-D drm, drm may be
; dimensioned (19,3) and spex_drm_filter might contain [0,1,3].  If drm is a function of
; time (e.g. fermi gbm) then the 3rd dimension is time, and the times are stored in
; spex_drm_times.  If spex_drm_times contains valid times, then we use this_time (or
;  spex_drm_current_time) to find correct drm.
;
;  In some cases getdata is called just to make sure we have drm edges, etc, but don't care
;  about actual drm matrix returned.  If we do care, we MUST do one of the following:
;  1.  This MUST be called with this_filter or this_time for the interval of interest, or
;  2.  WE MUST have set spex_drm_current_filter or spex_drm_current_time to the filter or
;      time for the interval we want the matrix for prior to calling getdata
;
; Keywords:
; force - if set, do all steps (call process etc) even if need_update isn't set
; all_filter - really means all filter or all times - returrns full array of all drms stored
; this_filter - for drms that are a function of filter state, return drm for 'this_filter'
; this_time - for drms that are a function of time, return drm for 'this_time' (should be a start/end interval)
; If this_filter or this_time isn't passed in, uses value in spex_drm_current_filter or
; getdata returns the DRM for the current selection of filter in spex_drm_current_filter or spex_drm_current_time
;
; this_time was added 7-jul-2010 for drm types like fermi gbm that are
; a function of time.  Used time if spex_drm_times is defined as non-zero.

function spex_drm::getdata, this_filter=this_filter, this_time=this_time, all_filter=all_filter, force=force, _extra=_extra

@spex_insert_catch

need_update = self -> need_update()
if keyword_set(force) then self -> set, /need_update
;
data = self -> framework::getdata(_extra=_extra)
if data[0] eq -1 then return, -1

if keyword_set(all_filter) then return, data

drm_times = self->get(/spex_drm_times)
if drm_times[0] gt 0. then begin
  use_time = exist(this_time) ? this_time : self -> get(/spex_drm_current_time)

  ; if no time requested, just return first matrix
  if same_data(use_time, [0.d0,0.d0]) then return, data[*,*,0]

  ; if we don't need to recalculate, just return last drm stored for this use_time (to save time)
  if ~keyword_set(force) and ~need_update and same_data(atime(use_time),atime(self.last_use_time)) then begin
    ;message,'returning previously saved drm for ' +format_intervals(use_time,/ut),/cont
    ;pmm,*self.last_drm
    return, *self.last_drm
  endif else begin
    ; Combine all the rsp matrices that overlap with requested time by adding the fraction of the
    ; first and last that are contained within use_time plus the full matrices in between, and dividing
    ; by the sum of those fractions + number of full intervals.
    ; frac will be an array of fractions or 1. to use for each matrix
    ind = where (drm_times[1,*] gt use_time[0] and drm_times[0,*] lt use_time[1], count)
    ; count is the number of drms we'll be combining
    case 1 of
      count eq 0: begin
        use_first = (use_time[0] lt drm_times[0,0])
;        wh = [' Using last matrix.',' Using first matrix.']
;        message, 'DRM not available for requested time ' + format_intervals(use_time,/ut) + wh[use_first], /cont
        ndrm = n_elements(data[0,0,*])
        data = use_first ? data[*,*,0] : data[*,*,ndrm-1]
        end
      count eq 1: data = data[*,*,ind]
      count gt 1: begin
        ends = [ind[0], ind[count-1]]   ; index of first and last drm we'll use
        frac_ends = float(abs(drm_times[0,ends] - use_time) / (drm_times[1,ends] - drm_times[0,ends]))
        if count eq 2 then begin
          frac = [(1.-frac_ends[0]), frac_ends[1]]
        endif else begin
          nfull = count - 2   ; number of
          frac = [(1.-frac_ends[0]), fltarr(nfull)+1., frac_ends[1]]
        endelse
        ;frac holds the weighting for each matrix.  mfrac is frac expanded to be the size of the full drm data (m,n,count)
        dim = [count, size(data[*,*],/dim)]
        mfrac = rebin(frac, dim)
        mfrac = transpose(mfrac, [1,2,0])
        data = total((mfrac * data[*,*,ends[0]:ends[1]]),3) / total(frac)
        end
    endcase
    self.last_use_time = use_time
    *self.last_drm = data
    ;message,'RETURNING NEW DRM FOR ' +format_intervals(use_time,/ut),/cont
    ;pmm,data
    return, data
  endelse
endif


; if don't care about the filter state, just return the matrix or array for the first filter state.
dataobj = self -> get(/spex_drm_dataobj)
if dataobj->get(/spex_ignore_filters) then begin
  drm_filter = self->get(/spex_drm_filter)
  if n_elements(drm_filter) eq 1 then return, data else $
    return, size(data,/n_dim) eq 3 ? data[*,*,0] : data[*,0]
endif

use_filter = exist(this_filter) ? this_filter : self -> get(/spex_drm_current_filter)

wrong = 0
if use_filter ne -1 then begin
    drm_filter = self->get(/spex_drm_filter)
    ; if there's only one filter in the drm, then just return that one
    if n_elements(drm_filter) gt 1 then begin
       q = where(use_filter eq drm_filter, count) > 0
       if count eq 0 then wrong = 1
       data = size(data,/n_dim) eq 3 ? data[*,*,q[0]] : data[*,q[0]]
    endif else if drm_filter ne -1 and drm_filter ne use_filter then wrong = 1
    if spex_get_debug() eq 5 and exist(this_filter) then $
       print,'In spex_drm::getdata, Returning DRM for filter state ' + trim(use_filter)

endif else if spex_get_debug() eq 5 then print,'In spex_drm::getdata, No filter selection for DRM.'

if wrong then message, 'DRM for requested filter (' + trim(use_filter) + ') not available.' + $
       '  Using ' + trim(drm_filter[0]), /cont

return, data
end

;------------------------------------------------------------------------------

function spex_drm::need_update

dataobj = self -> get(/spex_drm_dataobj)
spex_respinfo = dataobj->get(/spex_respinfo)
spex_image_full_srm = dataobj->get(/spex_image_full_srm)
spex_specfile = dataobj->get(/spex_specfile)
spex_data_sel = dataobj->get(/spex_data_sel)
if not same_data(spex_respinfo, *self.respinfo) or $
		not same_data(spex_image_full_srm, self.image_full_srm) or $
		not same_data(spex_specfile, *self.specfile) or $
		not same_data(spex_data_sel, self.data_sel) then begin
	drm_mod_obj = self->get(/spex_drm_mod_obj)
	if obj_valid(drm_mod_obj) then drm_mod_obj->set, /need_update
	return,1
endif else return, self->framework::need_update()

end

;------------------------------------------------------------------------------

pro spex_drm::process, _extra=_extra

print,' in spex_drm::process'

IF Keyword_Set( _EXTRA ) THEN self->Set, _EXTRA = _extra

dataobj = self -> get(/spex_drm_dataobj)
spex_respinfo = dataobj->get(/spex_respinfo)
*self.respinfo = spex_respinfo
self.image_full_srm = dataobj->get(/spex_image_full_srm)
*self.specfile = dataobj->get(/spex_specfile)
self.data_sel = dataobj->get(/spex_data_sel)

case 1 of
	dataobj->get(/spex_image_full_srm): method = 'build'
	self->get(/spex_drmfile) ne '': method='file'
	else: begin
		if is_number(spex_respinfo[0]) or is_struct( spex_respinfo[0]) then method='array' else begin $
			; if drm file name wasn't set, try getting drm file from spex_respinfo info
			; param (drm file  referenced in spectrum FITS file)
			if is_string(spex_respinfo) then self -> set, spex_drmfile=spex_respinfo
			method='file'
		endelse
	end
endcase

err_code = 0
err_msg = ''
case method of
	'file': drm_str = self->read_drm (self->get(/spex_drmfile), err_code=err_code, err_msg=err_msg)
	'array': drm_str = self->array_drm (spex_respinfo, dataobj, err_code=err_code, err_msg=err_msg)
	'build': drm_str = self->build_drm (dataobj, err_code=err_code, err_msg=err_msg)
endcase

self -> set, spex_drm_method = method
if (err_code ne 0) then begin
    if not spex_get_nointeractive() then a=dialog_message(err_msg) else print,err_msg
endif

if size(/tname, drm_str) ne 'STRUCT' then goto, error_exit

data_area = dataobj->get(/spex_area)
if data_area ne drm_str.area then begin
    err_msg = ['', $
        'WARNING.  ERROR IN SPECTRUM AND/OR SRM FILE !!!!!!!!!!!!!!!!!!!!!', $
        'Areas do not match.', $
        '   Area in spectrum file = ' + trim(data_area), $
        '   Area in SRM file      = ' + trim(drm_str.area), $
        '', $
        'If the mismatch is due to using incompatible spectrum/SRM files, do not', $
        'proceed without correcting the file selection.', $
        '', $
        'However, the mismatch may be due to a temporary bug in spectrum FITS writing software - the area', $
        '  in the spectrum file was incorrect unless an SRM file was written simultaneously.', $
        '', $
        'In that case, you may proceed.  The data area is being set to the SRM area.', $
        '']
    prstr,/nomore,err_msg
    if not spex_get_nointeractive() then a=dialog_message(err_msg)
    dataobj ->set, spex_area = drm_str.area
endif

self -> set, spex_drm_data_name=drm_str.data_name, $
    spex_drm_area=drm_str.area, $
    spex_drm_ct_edges=drm_str.edges_out, $
    spex_drm_ph_edges=drm_str.ph_edges, $
    spex_drm_sepdets=drm_str.sepdets, $
    spex_drm_filter=drm_str.filter, $
    spex_drm_current_filter= drm_str.filter[0], $
    spex_drm_detused=drm_str.detused

if tag_exist(drm_str,'times') then self -> set, spex_drm_times=drm_str.times else self -> set, spex_drm_times=0.

nfilters = n_elements(drm_str.filter)
dim_drm = size(/dim, drm_str.drm) > 1
; drm may be (nct,nph,nfilt) or
; one_d will be 1 if drm is 1-d whether there's one or several filters
one_d = (n_elements(dim_drm) - (nfilters gt 1)) eq 1

; DON'T DO THIS HERE, DO IN APPLY_DRM  21-Apr-2010, Kim
; correct for photospheric albedo if requested. If multiple filters, correct drm
; for each filter separately
;if self->get(/spex_albedo_correct) then begin
;	; albedo correction isn't appropriate unless we've read srm from file
;	if method eq 'file' then begin
;	    for i = 0, nfilters-1 do begin
;	       theta=self->get(/spex_source_angle)
;	       anis=self->get(/spex_anisotropy)
;	       if one_d then begin
;	         newdrm = drm_correct_albedo(theta=theta, anisotropy=anis, drm=drm_str.drm[*,i], $
;	          ph_edges=drm_str.ph_edges)
;	         if newdrm[0] ne -1 then drm_str.drm[*,i] = newdrm
;
;	       endif else begin
;	         newdrm = drm_correct_albedo(theta=theta, anisotropy=anis, drm=drm_str.drm[*,*,i], $
;	          ph_edges=drm_str.ph_edges)
;	         if newdrm[0] ne -1 then drm_str.drm[*,*,i] = newdrm
;
;	       endelse
;	    endfor
;	 endif else message, 'Warning: Albedo correction not applied for this source of DRM.', /cont
;endif

;
; DRM coming is in units of cts / photon / keV 
; Put DRM in units of cts/s per ph/cm2/s/keV (same as (cts / photon) *cm^2 * keV - these units
; make inversion simpler and apply_drm doesn't have to get ph_edges each time

out = drm_str.drm * drm_str.area[0] * $
    rebin( get_edges( drm_str.edges_out, /width), dim_drm )  ; these are the count bin widths
;Be sure to take care of the 1-d case, don't use ph_edges to scale in this case.
one_d = (n_elements(dim_drm) - (n_elements(drm_str.filter) gt 1)) eq 1
out = one_d ? out : $
       out * rebin( transpose( get_edges(drm_str.ph_edges, /width)), dim_drm)  ; these are the photon bin widths
self -> setdata, out
return

error_exit:
self -> setdata, -1

end

;------------------------------------------------------------------------------

function spex_drm::read_drm, file, $
                    ERR_CODE=err_code, $
                    ERR_MSG=err_msg

if not exist(file) then file = self->get(/spex_drmfile)

if file eq '' then file = self->select_file()
if file eq '' then begin
  err_code = 1
  err_msg = 'No drm file selected.'
  return, -1
endif

instr = get_fits_instr(file=file)

case 1 of
    stregex (instr, 'hessi', /boolean, /fold_case): $
       spex_hessi_fits2drm, FILE=file, $
                          drm_str, $
                          ERR_MSG=err_msg, $
                          ERR_CODE=err_code

    stregex (instr, 'xsm', /boolean, /fold_case):  begin
        spex_xsm_fits2drm, FILE=file, drm_obj=self,  $  ;afile is written into file
                          drm_str, $
                          ERR_MSG=err_msg, $
                          ERR_CODE=err_code
       end

    stregex (instr, 'GBM', /boolean, /fold_case): begin
        spex_gbm_fits2drm, FILE=file, $
                          drm_str, $
                          ERR_MSG=err_msg, $
                          ERR_CODE=err_code

        ; If there are only 8 energy bins, then we have a CTIME data file, not a CSPEC data file
        ; DRM files were written for CSPEC files (128 output energy bins).
        ; For CTIME, group the DRM output (counts) energy bins to match the CTIME bins and
        ; replace the bins and the drm in the structure.
        dataobj = self -> get(/spex_drm_dataobj)
        new_edges = dataobj->getaxis(/ct_energy,/edges_2)
        if n_elements(new_edges) eq 16 then begin
          drm = drm_str.drm
          data_grouper_edg, drm, drm_str.edges_out, new_edges, /perwidth, error=error, emsg=emsg
          drm_str = rep_tag_value(drm_str, new_edges, 'edges_out')
          drm_str = rep_tag_value(drm_str, drm, 'drm')
        endif
       end

    stregex (instr, 'LAT', /boolean, /fold_case): begin
        spex_lat_fits2drm, FILE=file, $
                          drm_str, $
                          ERR_MSG=err_msg, $
                          ERR_CODE=err_code

;        ; If data has 10 energy bins and rsp has 100, then we have an old spectrum file, new rsp
;        ; Group the DRM output (counts) energy bins to match the spectrum file bins and
;        ; replace the bins and the drm in the structure.
;        dataobj = self -> get(/spex_drm_dataobj)
;        new_edges = dataobj->getaxis(/ct_energy,/edges_2)
;        if n_elements(new_edges) eq 20 and n_elements(drm_str.edges_out) eq 200 then begin
;          drm = drm_str.drm
;          data_grouper_edg, drm, drm_str.edges_out, new_edges, /perwidth, error=error, emsg=emsg
;          drm_str = rep_tag_value(drm_str, new_edges, 'edges_out')
;          drm_str = rep_tag_value(drm_str, drm, 'drm')
;        endif
        
        
       end
     
     else: begin
       dataobj = self->get(/spex_drm_dataobj)
       reader = dataobj->get(/spex_file_reader) + '_drm'
       which, reader, out=out
       if out eq '' then begin
         message, /info, 'Input file was not recognized and no routine to read files has been provided.  '
         message, /info, '   You must set spex_file_reader parameter to base name of routine to read data and drm files.'
         err_code = 1
         err_msg = 'No drm file reader routine specified..'
         return, -1
       endif
       
       call_procedure, reader, $
         FILE=file, $
         drm_str, $
         ERR_MSG=err_msg, $
         ERR_CODE=err_code
         
       end
            
endcase

return, drm_str
end

;------------------------------------------------------------------------------

function spex_drm::array_drm, spex_respinfo, dataobj, $
                    ERR_CODE=err_code, $
                    ERR_MSG=err_msg

common hxrbs_response, e_matrix_sav, inmatrix, res_coef_com

if ~is_struct( spex_respinfo[0] ) && spex_respinfo[0] eq -1 then begin
	err_code = 1
	err_msg = 'No drm values in respinfo array.'
	drm_str = -1
endif else begin
  units_str = dataobj->get(/spex_data_units)
  case strlowcase(units_str.data_name) of
    'yohkoh': begin
      yohkoh_wbs_drm, ct_edges, ph_edges, drm_area, spex_respinfo
      end
    'hxrbs': begin
      ct_edges = dataobj -> get(/spex_ct_edges)
      ph_edges = get_edges(e_matrix_sav, /edges_2)
      drm_area = dataobj->get(/spex_area)
      end
    else: begin
      ct_edges = dataobj -> get(/spex_ct_edges)
      nedges = n_elements(ct_edges[0,*])
      if is_struct( spex_respinfo[0] ) then begin
	      ph_edges = spex_respinfo.edges_in
	      ;ct_edges = spex_respinfo.edges_out - they're available but won't use right now
	      spex_respinfo = spex_respinfo.drm
	  endif
      if n_elements(spex_respinfo) eq 1 then spex_respinfo = rebin([spex_respinfo],nedges)
      default, ph_edges, ct_edges

      drm_area = dataobj->get(/spex_area)
      end
  endcase

	drm_str = { $
		EDGES_OUT: ct_edges, $
		PH_EDGES: ph_edges, $
		AREA: drm_area, $
		DRM: spex_respinfo, $
		SEPDETS: 0, $
		data_name: units_str.data_name, $
		filter: -1, $
		detused: dataobj->get(/spex_detectors) }
endelse

return, drm_str

end

;------------------------------------------------------------------------------

function spex_drm::build_drm, dataobj, $
                    ERR_CODE=err_code, $
                    ERR_MSG=err_msg

case 1 of

	stregex (dataobj->get(/spex_title), 'hessi', /boolean, /fold_case): begin
		atten = dataobj -> get(/spex_interval_filter)
		atten_u = get_uniq(atten)
		natten = n_elements(atten_u)

		index = hessi_id2index( str2arr(dataobj->get(/spex_detectors), ' ') )
		use_vird = bytarr(18)
		use_vird[index] = 1b

		simplify = bytarr(10)

		ct_edges = dataobj->getaxis(/ct_energy, /edges_2)
		nct = n_elements(ct_edges[0,*])

		; convert 2xn energy bands to 1d array.  Have to be contiguous for hessi_build_srm, so
		; make contiguous bands, and then just use the values for the bands we really have
		en = reform(ct_edges, n_elements(ct_edges))
		en = get_uniq(en)

;		ct_edges = dataobj->getaxis(/ct_energy, /edges_1)
;		nct = n_elements(ct_edges)-1
		ph_edges = hsi_srm_extend_ph_edges( en )
		nph      = n_elements(ph_edges)-1

		time_axis = dataobj -> getaxis(/ut, /edges_2)
		ntime = n_elements(time_axis[0,*])
		date = anytim(time_axis[0,0])

		srm = fltarr(nct, nph, natten)

		; q will be the array of indices in en (and srmi) of the energy bands we want.
		q = reform( value_locate(en, ct_edges[0,*]) )

		for i=0,natten-1 do begin
			hessi_build_srm, en, use_vird, srmi, $
				ph_edges=ph_edges, $
				geom, atten=atten_u[i], /pha_on_row, time_wanted=date, simplify=simplify
			srm[0,0,i] = srmi[q,*]
		endfor

		drm_str = { $
			EDGES_OUT: get_edges(ct_edges, /edges_2), $
			PH_EDGES: get_edges(ph_edges, /edges_2), $
			AREA: dataobj->get(/spex_area), $
			DRM: srm, $
			SEPDETS: 0, $
			data_name: 'HESSI', $
			filter: atten_u, $
			detused: dataobj->get(/spex_detectors) }

		end

	else: begin
		drm_str = -1
		err_code = 0
		err_msg = 'Error.  Can not build srm for this data type: ' + dataobj->get(/spex_title)
		end

endcase

return, drm_str

end

;------------------------------------------------------------------------------

function spex_drm::select_file

newfile = dialog_pickfile (filter=['*.fits','*.rmf', '*.dat'], title='Select input DRM FITS file.')

if newfile ne '' then self -> set, spex_drmfile=newfile

return, newfile

end

;------------------------------------------------------------------------------

pro spex_drm::preview, out=out, nomore=nomore

dataobj = self -> get(/spex_drm_dataobj)
if dataobj->get(/spex_image_full_srm) eq 1 then begin
	if self->need_update() then out = 'DRM will be calculated.' else begin
		nct = n_elements( (self->get(/spex_drm_ct_edges))[0,*] )
		nph = n_elements( (self->get(/spex_drm_ph_edges))[0,*] )
		filters = self->get(/spex_drm_filter)
		filter_info = filters[0] eq -1 ? 'Unknown' : arr2str(trim(fix(filters)))
		out = [ $
			'DRM was calculated.', $
			'# Count, Photon Energy Bins: ' + trim(nct) + ' ' + trim(nph), $
			'Filter State(s): ' + filter_info ]
	endelse
endif else begin

	drmfile = self -> get(/spex_drmfile)
	if drmfile eq '' then begin
	    out = 'No DRM File Defined.'
	endif else begin

	    drm_str = self -> read_drm(drmfile, err_code=err_code)
	    if not err_code then begin

	       n_peband = n_elements (drm_str.ph_edges[0,*])
	       epmm = format_intervals (minmax (drm_str.ph_edges))

	       n_ceband = n_elements (drm_str.edges_out[0,*])
	       ecmm = format_intervals (minmax (drm_str.edges_out))

	       det_info = drm_str.sepdets ? '# individual Detectors: ' + trim(n_elements(arr2str(drm_str.detused,','))) : $
	         'Separate Detectors: False'
	       filter_info = drm_str.filter[0] eq -1 ? 'Unknown' : arr2str(trim(fix(drm_str.filter)))
	       out = [ $
	         'Detector Response Matrix (DRM) File Summary', $
	         'File name: ' + drmfile, $
	         'Data type: ' + drm_str.data_name, $
	         '# Count  Energy Bins: ' + trim(n_ceband) + '  Energy range: ' + ecmm, $
	         '# Photon Energy Bins: ' + trim(n_peband) + '  Energy range: ' + epmm, $
	         'Geometric Area: ' + trim(drm_str.area[0]), $
	         det_info, $
	         'Detectors Used: ' + trim(drm_str.detused), $
	         'Filter State(s):  ' + filter_info]
	    endif else out = ['', 'Error reading file ' + drmfile]
	endelse
endelse

if not keyword_set(nomore) and n_elements(out) gt 1 then prstr, strjustify(out,/box)

end
;------------------------------------------------------------------------------
; get_diagonal returns the efficiency factors for an assumed function and parameters.
; data in count space are divided by efficiency factors to convert to photon space.
; units are counts / photon

;function spex_drm::get_diagonal, func, params, this_filter=this_filter, this_time=this_time
function spex_drm::get_diagonal, fitobj, this_filter=this_filter, this_time=this_time

drm=self->getdata(this_filter=this_filter, this_time=this_time)
if drm[0] eq -1 then return, -1

ct_edges = Self->get(/spex_drm_ct_edges, /this_class, /info)
w_ct = get_edges( ct_edges, /width)
area = Self->Get(/spex_drm_area, /this_class, /info)

if size(/n_dim,drm) gt 1 then begin ;multi-d case first, then 1d
    ph_edges = Self->get(/spex_drm_ph_edges, /this_class, /info)
;    obj = obj_new('fit_function', fit_function=func, fit_comp_params=params)
    fc = fitobj -> getdata(fit_xval=ct_edges)
    fe = fitobj -> getdata(fit_xval=ph_edges)
    eff = f_div( Self->apply_drm(fe,this_filter=this_filter, this_time=this_time, func_obj=fitobj), fc*w_ct*area )
;    obj_destroy, obj

endif else begin
    if n_elements(drm) eq 1 then drm = rebin(drm, size(w_ct, /dim))
    eff = f_div( drm, w_ct*area)
endelse

return, eff
end

;------------------------------------------------------------------------------
; apply_drm converts data in photon flux to count rate
;
;yfit_in is flux, ph/cm2/s/keV
;drm should be cts/cm2/s/kev per ph/cm2/s/keV
;return should be in counts/s
;
; if index_erange is set, use them for count index in drm
; this_filter - filter value to get drm for
; this_time - fit time interval to get drm for (for drm types that change with time, e.g. fermi gbm)
; this_strat - strategy we're working on.  '' if working on combined component function
; func_obj - fit function object. If function includes the drm_mod function, then
;   we'll get the drm from the hsi_drm_mod object instead of the one stored.

function spex_drm::apply_drm, yfit_in, $
	index_erange=index_erange, $
	this_filter=this_filter, $
	this_time=this_time, $
	this_strat=this_strat, $
	func_obj=func_obj, disable_pileup_mod=disable_pileup_mod, disable_albedo=disable_albedo

checkvar, this_strat, ''

@spex_insert_catch
;;;;;; added 17-feb-2014, richard.schwartz@nasa.gov ;;;;;;;;;
dataobj = Self->Get(/spex_drm_dataobj)
data_units = dataobj->Get(/spex_data_units)
data_name = data_units.data_name
compute_counts_from_drm = 1 ;this is the normal case
;;;;;;                                              ;;;;;;;;;
yfit = yfit_in  ; protect input arg from changing

drm = self -> getdata(this_filter=this_filter, this_time=this_time)
if drm[0] eq -1 then message,'No drm available.  Can not convert photons to counts.'

select = keyword_set(index_erange) ? index_erange : -1
select = select[0] eq -1? lindgen( (size(/dim, drm))[0]) : select

fit_function = func_obj->get(/fit_function)

; DO ALBEDO CORRECTION HERE. 21-Apr-2010, Kim.
; The next 3 lines worked instead of the following4 before adding albedo as a function component was allowed
albedo_correct = self->get(/spex_albedo_correct)
albedo_fit = strpos(fit_function, 'albedo') ne -1
if (albedo_correct or albedo_fit) and ~keyword_set(disable_albedo) then begin
  theta = self -> get(/spex_source_angle)  
  if albedo_fit then begin
    alb_params = func_obj -> get(/fit_comp_params,comp_name='albedo')
    anis=alb_params[0]
  endif else anis = self -> get(/spex_anisotropy)
 ;  print,'theta,anis: ', theta, anis
  yfit = self -> albedo(yfit, theta=theta, anisotropy=anis)
;  print,'corrected albedo, theta, anisotropy = ', theta, anis
endif


if strpos(fit_function, 'drm_mod') ne -1 then begin
  is_hessi = stregex( data_name, 'HESSI',/fold,/bool)
  is_messenger = stregex( data_name, 'MESSENGER',/fold,/bool)
  valid_mod = is_hessi or is_messenger
  ndet = n_elements(str2arr(self->get(/spex_drm_detused), ' '))
  if ndet gt 1 || ~valid_mod then begin

    ; This is just to avoid spitting out too many messages. Only one every 20 sec.
    defsysv,'!spex_drm_mod_message', exist=exists
    if not exists then DEFSYSV,'!spex_drm_mod_message',systime(1)
    if systime(1) - !spex_drm_mod_message gt 20 then begin
      message, /cont, 'DRM_MOD is valid for single detectors only.  Continuing, but NOT USING DRM_MOD.'
      DEFSYSV,'!spex_drm_mod_message',systime(1)
    if ~valid_mod then message,/cont, 'DRM_MOD is valid only for RHESSI single detector or MESSENGER SAX'
    endif

  endif else begin
      drm_mod_obj = self->get(/spex_drm_mod_obj)
      if not is_class(drm_mod_obj, 'hsi_drm_mod') and is_hessi then begin
	  	drm_mod_obj = obj_new('hsi_drm_mod')
	  	drm_mod_obj -> set, spex_drm = self
	  	self -> set, spex_drm_mod_obj = drm_mod_obj
	  endif
	  comp_name = (strpos(fit_function, 'drm_mod2') eq -1) ? 'drm_mod' : 'drm_mod2'
	  drm_params = func_obj -> get(/fit_comp_params,comp_name=comp_name)

	case 1 of
		is_messenger : begin
		;compute new DRM and use it.  Nothing fancy, nothing saved, just compute every time
		ph_edges = self->Get(/spex_drm_ph_edges)
		ct_edges = self->Get(/spex_drm_ct_edges)
		drm0 = drm

		drm = messenger_use_resp_calc( edges_in = ph_edges, edges_out = ct_edges, $
		 fwhm = drm_params[0] * 0.59, offset = drm_params[1], gain = drm_params[2] )
	 	area = Self->Get(/spex_drm_area)
	 	dim_drm = size(/dimensions, drm)
;			pmm, drm0, drm
;			help, drm0, drm
		drm = drm * area[0] * rebin( get_edges( ct_edges, /width), dim_drm ) $
			* rebin( transpose( get_edges(ph_edges, /width)), dim_drm)
	    end

	else: begin
      compute_counts_from_drm = 0

	  center_thick_ratio = n_elements(drm_params) gt 3 ? drm_params[3] : 1. ; a[3] for drm_mod2 is center_thick_ratio
	  dataobj = self->get(/spex_drm_dataobj)  ; get spex_data object
	  time_wanted = avg( ( dataobj->get(/spex_ut_edges) )[*,0] )

	  if dataobj->get(/spex_ignore_filters) then atten_state = 1 else $
	    atten_state = exist(this_filter) ? this_filter : self->Get(/spex_drm_current_filter)
	  drm_mod_obj -> set, atten_state = atten_state

	  drm_mod_obj -> set, $
	  	fwhm_frac = drm_params[0], $
	  	blanket_coeff = drm_params[2], $ ;1-may-2008, ras
	  	center_thick_ratio = center_thick_ratio, $  ;21-aug-2013, kim
	  	time_wanted = time_wanted

	  countrate = (drm_mod_obj -> getdata(photon_spectrum=yfit, gain_offset=drm_params[1]))
	  end
	  endcase
  endelse
endif
if (size(drm,/n_dim) eq 1 ? n_elements(drm) : n_elements( drm[0,*] )) ne n_elements(yfit) then stop
if compute_counts_from_drm then countrate =  size(/n_dim,drm) eq 1 ? drm*yfit: drm # yfit
chk=check_math(mask=32) ; clear underflow errors, don't need to see them

if strpos(fit_function, 'gain_mod') ne -1 then begin
  gain_params = func_obj -> get(/fit_comp_params, comp_name='gain_mod')
  countrate = self -> gain_mod (countrate, $
    drm=drm, $
    gain=gain_params[0], $
    offset=gain_params[1])
endif

if not keyword_set(disable_pileup_mod) and strpos(fit_function, 'pileup_mod') ne -1 then begin

	ct_edges = self -> get(/spex_drm_ct_edges)
	pileup_params = func_obj -> get(/fit_comp_params,comp_name='pileup_mod')
	pileup_struct = {tau0: pileup_params[0], $
					sigma: pileup_params[2], $
					pilerfrac: pileup_params[1], $
					ecut: pileup_params[3], $
					ratio: pileup_params[4], $
					offset: 0., $
					modfrac: pileup_params[5] }
  q = where (countrate gt 0.,nz)
  ndet = n_elements(str2arr(self->get(/spex_drm_detused),' '))
  if nz gt 0 then $
	   retval = ndet * float((pileup_countrate (countrate/ndet, ct_edges, pileup_struct))[select]) $
	else retval = countrate[select]

;	return, pileup_params[3] * float((pileup_countrate (countrate, ct_edges, $
;		pileup_params[0], pileup_params[1], $
;		sigma=pileup_params[2], tau=1.e-6))[select])

endif else retval = countrate[select]

;Only add in the '/do_calc' nodrm function values if this_strat is '', which means we're doing the combined function
; For each xxx_nodrm component, calculate that component's values by calling function with /do_calc, convert units to rate
; and add to already computed count rate that was calculated from the rest of the componenents and folded through the DRM.
if this_strat eq '' or strpos(this_strat,'nodrm') ne -1 then begin
  if strpos(this_strat,'nodrm') ne -1 then begin
    strat_arr = this_strat
    count = 1
    q = 0
  endif else begin
    strat_arr = func_obj->get(/compman_strategy)
    q = where(strpos(strat_arr,'nodrm') ne -1, count)
  endelse
  
  if count gt 0 then begin
;    en_orig = func_obj->get(/fit_xvals)
    en = dataobj->get(/spex_ct_edges) 
    en_wid = get_edges(en, /width)
    area = dataobj->get(/spex_area)
    comp_arr = ssw_strsplit(strat_arr,'#',/head)
    for i = 0,count-1 do begin
      
;      icomp = q[i]
;      nodrm_params = func_obj -> get(/fit_comp_params,strategy_name=strat_arr[icomp])    
;;      print, 'nodrm_params = ', nodrm_params
;      fval = func_obj->getdata(strategy_name=strat_arr[icomp], fit_xvals=en, /do_calc)      
;      retval = retval  +  fval[select] * en_wid[select] * area
;      ; this next call is needed to put the normal values of all zeroes back into the
;      ; setdata of the xxx_nodrm fit_comp object. Otherwise on the next call from higher
;      ; up, the do_calc values will still be there, since it won't know it needs to reprocess.
;      dummy = func_obj->getdata(strategy_name=strat_arr[icomp], fit_xvals=en_orig)
;      
;     Previously was using the interval func_obj to calculate the nodrm func with /do_calc, but
;     then had to make dummy call to reset the setdata to all zeros, the normal state (since do_calc
;     is a keyword not a control parameter.  Safer to make a temporary func obj just for this calc.
      strat = strat_arr[q[i]]
      comp = comp_arr[q[i]]
      nodrm_params = func_obj -> get(/fit_comp_params,strategy_name=strat)
      temp_comp_obj = obj_new('fit_comp')
      temp_comp_obj -> set, fit_comp_function=comp, fit_xvals=en, fit_comp_params=nodrm_params
      fval = temp_comp_obj ->getdata(/do_calc)
      retval = retval  +  fval[select] * en_wid[select] * area
      chk=check_math(mask=32) ; clear underflow errors, don't need to see them
      destroy, temp_comp_obj
    endfor
  endif
endif  



return, retval

end
;------------------------------------------------------------------------------
;------------------------------------------------------------------------------
; Move count spectrum based on gain parameters
;positive offset shifts lines to lower apparent energy
;i.e. lower channels (because they are really at higher energy)
;In principle, we could adjust the display energy as well to new_ct
;
;yfit is flux, ph/cm2/s/keV
;drm should be cts/cm2/s/kev per ph/cm2/s/keV
;return should be in counts/s
;
;
;
;function spex_drm::gain_mod, yfit, $
;	index_erange=index_erange, $
;	new_ct_edges=new_ct ;these are the new channel edges if you want them
;
;@spex_insert_catch
;
;drm = self -> getdata(this_filter=this_filter)
;if drm[0] eq -1 then message,'No drm available.  Can not convert photons to counts.'
;
;select = keyword_set(index_erange) ? index_erange : -1
;select = select[0] eq -1? lindgen( (size(/dim, drm))[0]) : select
;
;ct_edges = Self->Get(/spex_drm_ct_edges)
;wct = get_edges(/width, ct_edges)
;countrate =  size(/n_dim,drm) eq 1 ? drm*yfit: drm # yfit
;
;;INTERP2INTEG, Xlims, Xdata, Ydata, log=log
;new_ct = (1.+self->get(/spex_drm_gain))*ct_edges + Self->get(/spex_drm_offset)
;wnew    = get_edges(/width, new_ct)
;cspectrum = f_div(countrate, wct)
;countrate = interp2integ(new_ct, ct_edges, cspectrum) * wnew
;
; return, countrate[select]
;
;end

function spex_drm::gain_mod, countrate, drm=drm, gain=gain, offset=offset, new_ct_edges=new_ct

ct_edges = Self->Get(/spex_drm_ct_edges)
wct = get_edges(/width, ct_edges)

;INTERP2INTEG, Xlims, Xdata, Ydata, log=log
new_ct = (1.+gain)*ct_edges + offset
; Richard and Kim, 31-Oct-2012 Remove next two lines, not necessary?  and can cause crash because
; new_ct then has wrong number of elements.
;q = where_within(new_ct, minmax(ct_edges),count)
;new_ct = new_ct[*,q]
wnew    = get_edges(/width, new_ct)

cspectrum = f_div(countrate, wct)
countrate = interp2integ(new_ct, ct_edges, cspectrum)
;countrate = interp2integ(new_ct, ct_edges, countrate) / wnew
return, countrate
end

;------------------------------------------------------------------------------
function spex_drm::albedo, yfit, anisotropy=anisotropy, theta=theta, energy=energy

  if ~is_class(self.albedo_obj, 'spex_albedo') then self.albedo_obj = obj_new('spex_albedo')
  ph_edges = keyword_set(energy) ? energy : self -> get(/spex_drm_ph_edges)
  return, self.albedo_obj -> apply(yfit, theta=theta, anisotropy=anisotropy, ph_edges=ph_edges)


;  edge_products, ph_edges, mean=ph_mean, edges_2=ph_edges_2, width=ph_width
;  nen = n_elements(ph_mean)
;
;  ;Build the albedo matrix
;  idiag = lindgen(nen) * (nen+1)
;  drm_unity = fltarr(nen, nen)
;  drm_unity[idiag] = 1. / ph_width
;  albedo_drm = drm_correct_albedo(theta=theta, anisotropy=anisotropy, ph_edges=ph_edges_2, drm=drm_unity)
;
;;  plot_oo, ph_mean, yfit , xran=[5,500]
;;  oplot, ph_mean, albedo_drm # (yfit * ph_width), col=2
;;  oplot, ph_mean, ph_width, col=5
;;
;  ;apply the albedo correction to the photon flux
;  return, albedo_drm[0] eq -1 ? yfit : albedo_drm # (yfit * ph_width)

end

;------------------------------------------------------------------------------

pro spex_drm__define

self = { spex_drm, $
	respinfo: ptr_new(), $
	image_full_srm: 0, $
	specfile: ptr_new(), $
	data_sel: '', $
	albedo_obj: obj_new(), $
	last_drm: ptr_new(), $
	last_use_time: [0.d0,0.d0], $
  INHERITS framework }

END
