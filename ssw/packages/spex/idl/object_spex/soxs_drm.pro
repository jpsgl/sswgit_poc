;+
; PROJECT:
;	SOXS
; NAME:
;	SOXS_DRM
;
; PURPOSE:
;	This function constructs and returns the DRM for SOXS.
;
; CATEGORY:
;	Spectral Analysis
;
; CALLING SEQUENCE:
;	soxs_drm, efficiency_file=efficiency_file, $
;		transmission_file=transmission_file, $
;	    fwhm=fwhm, edges_file=edges_file, path=path, $
;	    ein2 = ein2, $
;	    area = area, $ ;nominal geometric area
;	    error = error
;
;
; INPUTS:
;
;       Efficiency_file - defaults to 'effi_si.txt'
;		Transmission_file - defaults to 'efsi80150.txt'
;		Edges_file - defaults to 'corr_si_180_140804_054312.txt'
;		fwhm - constant, uses 0.70 keV
;		offset - empirical channel offset to fix apparent gain
;
;
; OUTPUTS:
;		Returns DRM as 256 x 256 array
;			for a photon spectrum in photons/cm2/sec input
;			(i.e. integrated over photon energy bins), the output
;			is in units of cnts/cm2/s/keV
;			count_flux = drm # (photon_spectrum * DE)
;			where photon_spectrum is in units of photons/cm2/sec/keV
;			and DE is the input energy bin width in keV
;       EIN2 - edges in 2 x 256 array
;		AREA - nominal area, 0.06 cm^2
;
;
;
; RESTRICTIONS:
;	none
;
; PROCEDURE:
;	Reads efficiency, transmission, and edge files provided by
;	SOXS team.  Energy calibration is fit and uses, efficiency and transmission
;	are read and smoothed to energy edge midpoints.
;
; MODIFICATION HISTORY:
;	6-jul-2006, richard.schwartz@gsfc.nasa.gov
;	10-Aug-2006, Kim.  Changed default for offset from 1.25 to 2.07
;	31-Aug-2006, Kim.  Use fwhm keyword
;
;-


function soxs_drm, efficiency_file=efficiency_file, $
	transmission_file=transmission_file, $
    fwhm=fwhm, edges_file=edges_file, path=path, $
    ein2 = ein2, $
    area = area, $ ;nominal geometric area
    offset = offset, $
    error = error
error = 1
drm = 0

default, efficiency_file, 'effi_si.txt'
default, transmission_file, 'efsi80150.txt'
default, edges_file, 'corr_si_180_140804_054312.txt
default, fwhm, .7

area = .06 ; cm^2
tfile = obj_new('tfile')
tfile->Set, path = 'SSWDB_SOXS'
efficiency = tfile->getdata(/auto, /convert, filename=efficiency_file)
transmission = tfile->getdata(/auto,/convert, filename=transmission_file)
edges = tfile->getdata(/auto,/convert, filename=edges_file)

test = is_number(efficiency[0]) * is_number(transmission[0]) * is_number(edges[0])

if not test then begin
	message,/cont, 'Invalid efficiency, transmission, or edge file'
	return, drm
	endif

coef = poly_fit( edges[0,*], edges[1,*], 1)
default, offset, 2.07 ;empirical correction
edges= poly( findgen(257)-offset, coef)
ein2 = get_edges(edges,/edges_2)
dein = coef[1]
ems  = avg(ein2,0)

transmission = interpol( transmission[1,*],transmission[0,*], ems, /quad)
efficiency = interpol( efficiency[1,*],efficiency[0,*], ems, /quad)

oeres = energy_res(ein=ein2, eout=ein2, e_vs=ems, fwhm=ems*0+fwhm, sig_lim=8)
eres = oeres->getdata()
obj_destroy, oeres
sxeff = smooth(transmission * efficiency,9)
sxeffmtrx= rebin(transpose(sxeff),256,256)
drm = eres * sxeffmtrx / rebin( dein+fltarr(256), 256, 256)/area
error = 0
return, drm
end


