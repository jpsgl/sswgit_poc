;+
;Name:
;   f_drm_mod
;PURPOSE:
;   This pseudo function always returns an array of 0s.  It allows us to find the
;   best values of several RHESSI DRM parameters by varying them (and
;   computing a new DRM) while fitting.
;
;
;CATEGORY:
;   SPECTRA, XRAY
;
;INPUTS:
;   E  energy vector in keV, 2XN edges or N mean energies
;   apar(0)  FWHM fraction
;   apar(1)  gain offset
;   apar(2)  center thickness ratio
;
; Written: Kim Tolbert, 27-Jun-2006
;-

function f_drm_mod, e, apar, _extra=_extra

if (size(e))(0) eq 2 then edge_products, e, mean=em else em=e
return, fltarr(n_elements(em))
end