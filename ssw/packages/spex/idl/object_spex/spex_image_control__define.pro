;+
; Name: spex_image_control__define
;
; Purpose:  Define control structure for spex_image
;
; Kim Tolbert Aug 2005
; Modifications:
;	8-Feb-2006, Kim.  Added spex_image_full_srm
;	  Also, make flags integer not byte.  If strategy isn't image, then
;	  framework returns an integer -1 for these params, so when checking values
;	  better if they're the same type.
;
;-

pro spex_image_control__define

struct = {spex_image_control, $
          spex_roi_use: ptr_new(),  $
          spex_roi_integrate: 0, $
          spex_roi_infile: '', $
          spex_image_full_srm: 0, $
          inherits spex_data_strategy_control}

end
