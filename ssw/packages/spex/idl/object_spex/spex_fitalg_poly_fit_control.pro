;------------------------------------------------------------------------------
function spex_fitalg_poly_fit_control

struct = { spex_fitalg_poly_fit_control }

; do at least a linear fit
struct.poly_fit_n_degree = 2

struct.spex_error_use_expected = 1
struct.spex_uncert = .05

return, struct

end
