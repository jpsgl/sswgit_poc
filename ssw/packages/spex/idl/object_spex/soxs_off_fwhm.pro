;+
; Name: soxs_off_fwhm
;
; Purpose: Modify the energy gain offset and FWHM used to calculate the
;   SOXS DRM, replace the modified DRM in the spex drm object, insert
;   the new energy edges in the spex object, and tell the spex_drm that it
;   needs to reprocess next time it's needed.
;   This allows the user to find the best value of the offset and fwhm
;   by fitting the data using the current drm, adjusting the offset
;   and/or the fwhm, and fitting again, and determining which drm produces the
;   the best spectrum.
;
; Example:
;   Start ospex by
;     obj = ospex()
;   and read in an input data file.  You can plot the data, and do fits at
;   this point if you want.  The default values for offset and fwhm are:
;     offset = 2.07   (in channels)
;     fwhm = .7 (in keV)
;   If you don't call soxs_off_fwhm, those are the values that are used.
;   To change the offset to 3. and the fwhm to .8 type the following:
;     soxs_off_fwhm, obj, offset=3., fwhm=.8
;   Now you can fit again using the new DRM.
;
;   Note: if you prefer to start ospex using an ospex script, be sure to return
;   the object reference in the variable obj, e.g.  ospex_script_18_Jul_2006, obj=obj
;
; Calling arguments:
;   obj - the ospex object reference
; Input keywords:
;   offset - the new energy gain offset value in channels
;   fwhm - the new fwhm in keV
;
;-


pro soxs_off_fwhm, obj, offset=offset, fwhm=fwhm

if n_elements(offset) eq 0 then offset = 2.07
if n_elements(fwhm) eq 0 then fwhm = .7

drm=soxs_drm(ein2=ct_edges, offset=offset, fwhm=fwhm)
obj->set, spex_respinfo=drm[2:253, 2:253], spex_ct_edges=ct_edges[*,2:253]
d = obj->getdata(class='spex_drm', /force)

print, 'Setting offset to ' + trim(offset) + ', fwhm to ' + trim(fwhm) + ' for SOXS DRM'

end
