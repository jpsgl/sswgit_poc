;+
;Name: MESSENGER_READ_HELIOGRAPHIC_POSITION
;
;Purpose: This function reads the Messenger Heliographic Position file.
;
;Usage: Mess_hg_str = messenger_read_heliographic_position( infile )
;
;Input:
;	Infile - Reads the ouput of MESSENGER_CONVERT_TRAJECTORY_FILE
;				full path descriptor, assumes a default of 'sun_messenger_heliographic.txt' in
;				the current directory.
;Optional Keywords
;	USE_CURRENT - if set then look for infile int the working(current) directory
;	ERROR - output, 0 if successfule, 1 otherwise
;Output:
;		Returns a replicated structure, { date: 0.0d0, d_km: 0.0, xyz: fltarr(3)}, with one element
;		for each line (time entry) in Infile
;			date: time in default output of anytim(), sec since 1-jan-1979
;			d_km: distance in km from Sun to Messenger spacecraft
;			xyz: 3 vector, norm of 1, Cartesian coordinates of Heliographic position, ( HEEQ, HEQ, Stonyhurst system)
;
;History: 13-dec-2013, richard.schwartz@nasa.gov
;-
function messenger_read_heliographic_position, infile, use_current=use_current, error = error
error = 1

default, infile, 'sun_messenger_heliographic.txt'
infile_found = messenger_file_search( infile, $
	use_current = use_current, $
	error_message = 'Sun to Messenger Heliographic Vector (position) file not found!', error_state=file_error, count=count)
if file_error then return, ''

;Read the messenger heliographic position file into a data structure
mess_data_hg = rd_tfile( infile_found[0], /autocol, hskip=2 )
nv = (size(/dim, mess_data_hg))[1]
mess_hg_str = replicate({ date: 0.0d0, d_km: 0.0, xyz: fltarr(3)}, nv)
mess_hg_str.date = reform( anytim( mess_data_hg[0,*] ))
mess_hg_str.d_km = reform( float( mess_data_hg[1, *] ))
mess_hg_str.xyz  = float( mess_data_hg[2:4, *] )
error = 0
return, mess_hg_str
end