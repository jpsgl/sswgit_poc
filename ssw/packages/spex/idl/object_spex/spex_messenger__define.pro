;+
; Name: spex_messenger__define
;
; Purpose: Provide methods to deal with the messenger data when downloading,etc. for OSPEX.
;   
; Written: Kim Tolbert, June 2010
;-

function spex_messenger::init
return,1
end

;-----

function spex_messenger::search, time_range, count=count, _ref_extra=extra

spex_find_data, 'messenger', time_range, url=url, count=count, _extra=extra

if count eq 0 then return, ''

return, url
end

;-----
; We need .dat and .lbl files, but only want to select the .dat file for input as spex_specfile to OSPEX.
pro spex_messenger::select_files, files, time_range, specfile, drmfile

specfile = ''
drmfile = ''
q = where (strpos(file_basename(files), '.dat') ne -1, count)
specfile = files[q[0]]

end

;-----

pro spex_messenger__define
void = {spex_messenger, inherits spex_instr}
end