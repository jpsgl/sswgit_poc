;+
;
; NAME:
;	spex_hessi_fits2drm
;
; PURPOSE:
;   Read a HESSI response matrix from a FITS file and return information of
; 	interest to OSPEX.
;
;
; CATEGORY:
;       SPECTRAL FITTING, SPEX - HESSI
;
; CALLING SEQUENCE:
;
; CALLS:
; 	fits2rm, fxpar
;
; INPUTS:
;
; OUTPUTS:
;
; INPUT KEYWORDS:
; 	FILE - name of FITS file to read.
; 	SFILE - not used.  Included for Spex comptability.
;
; OUTPUT KEYWORDS:
; 	drm_str - structure with drm info
;     If there are multiple attenuator states, the DRM and ATTEN_STATE items
;     in the structure are dimensioned to the # of atten states.
;
; PROCEDURE:
;
; Written 25-May-2001 Paul Bilodeau, RITSS/NASA-GSFC
;
; Modification History:
;   06-Mar-2003, Paul Bilodeau - change sumflag to sepdets for
;     correct header keyword that determines if the matrix is the sum
;     or individual detector responses.
;   1-Jul-2004, Kim.  Handle files with extensions for drms for multiple
;     attenuator states.
;	19-Nov-2004, Kim.  Changed name from hessi_fits2drm_ospex.
;	20-Nov-2006, Kim. Abort if sum_flag=0
;	23-Mar-2011, Kim. If sum_flag is 0, only abort if more than one det used.
;	25-Nov-2014, Kim. Renamed from spex_hessi_fits2drm to use for stx drm file. Set spex_file_reader to 'stx_read' to use.
;-
;------------------------------------------------------------------------------
PRO stx_read_drm, FILE=file, $
                          SFILE=sfile, $
                          drm_str, $
                          ERR_CODE=err_code, $
                          ERR_MSG=err_MSG

drm_str = -1

fits2rm, file[0], $
         RM=drm, $
         EBINS=ph_edges, $
         DETBINS=edges_out, $
         EXT_HEADER=hdr, $
         ERR_CODE=err_code, $
         ERR_MSG=err_msg

IF err_code THEN RETURN

detused = fxpar( hdr, 'DETUSED', COUNT=count )
if count eq 0 then detused = '' else begin
  detused = str_replace (detused, 'SEGMENTS: ', '')
  detused = str_replace (detused, '|', ' ')
endelse

sum_flag = fxpar (hdr, 'SUMFLAG', count=count)
if count gt 0 and sum_flag eq 0 then begin
  if n_elements(str2arr(detused, ' ')) gt 1 then begin   ; check for > 1 det
	  err_code = 1
	  err_msg = 'OSPEX can not handle SRM files with >1 detectors that are not summed (sum_flag=0). Aborting'
	  return
	endif
endif

data_name = fxpar( hdr, 'TELESCOP', count=count)
if count eq 0 then data_name = fxpar( hdr, 'ORIGIN')

area = fxpar( hdr, 'GEOAREA', COUNT=count )
area = count GT 0L ? st2num( area ) : 1.

sepdets = fxpar( hdr, 'SEPDETS', COUNT=count )
IF count LE 0 THEN sepdets = 0

IF sepdets THEN $
  IF Size( drm, /N_DIMENSIONS ) EQ 2 THEN $
    drm = Transpose( drm )

fits_info, file[0], /silent, n_ext=n_ext

; get atten state from FILTER keyword if available, otherwise from extension
; with object parameters if available.  Otherwise set to -1.
atten_state = fxpar( hdr, 'FILTER', count=count)
if count le 0 or strcompress(atten_state) eq ' ' then begin
	if n_ext gt 2 then begin
		struct = mrdfits(file[0], 3, head, /silent)
		atten_state = tag_exist(struct, 'atten_state') ? struct.atten_state : -1
	endif else atten_state = -1
endif

if atten_state eq -1 then message,'Warning:  Attenuator state in SRM file could not be determined.', /cont

if n_ext gt 3 then begin
	; drm could be 1-D or 2-D.  Using a replicated structure is easy way
	; to handle either.
	rm_struct = replicate({rm:drm}, n_ext-2)
	ndrm = 1
	for ext=4,n_ext do begin
		rm_ext = mrdfits(file[0], ext, head, /silent)
		q = where (stregex (head, 'type.*matrix', /boolean, /fold_case), count)
		if count gt 0 then begin
			ndrm = ndrm + 1
			rm_struct[ndrm-1].rm = rm_ext.matrix
			atten = fxpar( head, 'FILTER')
			atten_state = [atten_state, atten]
		endif
	endfor
	drm = rm_struct[0:ndrm-1].rm
endif

drm_str = { $
	EDGES_OUT: edges_out, $
	PH_EDGES: ph_edges, $
	AREA: area, $
	DRM: drm, $
	SEPDETS: sepdets, $
	data_name: data_name, $
	filter: atten_state, $
	detused: detused }

END
