;+
;Name: Messenger_convert_trajectory_file
;
;Purpose: This procedure converts the Messenger trajectory file in HAE coordinates
; to HEQ (Heliographic Cartesian Coordinates) and writes it back to the specified location
;
;Usage:
;		messenger_convert_trajectory_file [, Input_file] [, Output_file ]

;Inputs:
;	Input_file - full path to file created here
;		defaults to: 'sun_messenger_ecliptic.txt'
;		http://messweb.jhuapl.edu/MD/servlet/Trajdb
;		login
;		current best trajectory
;		date and time, full mission or subset, every 4 hours
;		position Sun to spacecraft,  Earth Mean Ecliptic & Equinox of J2000, position vector, km
;		Submit
;		output options ascii-text file
;		save as, eg: sc_messenger_ecliptic.txt and put in current directory or pass the
;		full reference in the first argument, sun_spacecraft_ecliptic_file
;   Output_file - full path to desired file. New text file is written here. Does not have a header
;		Here is an example of one line:
;		03-AUG-2004          1.51781e+008        0.994565725        0.004969063        0.103991449
;		Date                 Distance in km		 x                  y                  z as unit vector
;Optional Keywords
;	USE_CURRENT - if set then look for infile int the working(current) directory
;	ERROR - output, 0 if successfule, 1 otherwise
;
;Restrictions:
;	Must have ssw/stereo/gen in your path
;History:
;	Version 1, 12-dec-2013, richard.schwartz@nasa.gov
;-
pro messenger_convert_trajectory_file, $
	sun_spacecaft_ecliptic_file, $
	sun_spacecraft_heliographic_file,  $
	use_current=use_current, error = error
error = 1


default, infile, 'sun_messenger_ecliptic.txt'
infile_found = messenger_file_search( 'sun_messenger_ecliptic.txt', $
	use_current = use_current, $
	error_message = 'Sun to Messenger Ecliptic Vector (position) Trajectory file not found!', $
	error_state = file_error, count=count)
if file_error then goto, Get_Out



default, sun_spacecraft_heliographic_file, 'sun_messenger_heliographic.txt'
outfile = sun_spacecraft_heliographic_file

;infile has a header line so skip

text = rd_tfile( infile_found, /auto, /hskip)

v_file = float( text[5:7, *] )

date_text = reform( text[2,*]+'-'+text[1,*]+'-'+text[0,*] )
date = date_text

;convert vectors to spacecraft into unit vectors and distance
d = sqrt( total( v_file^2, 1) )
v_unit_ec = v_file / (( fltarr(3,1) + 1.0) # d)
;convert from ecliptic to heliographic Cartesian

v_unit_hg = v_unit_ec ;define a new 3 vector because convert_stereo_coord changes the input
;Must have stereo as an ssw instr to do this
;'HEQ' coordinates are the usual heliographic coordinates from Earth
convert_stereo_coord, anytim(date, /utc_int), v_unit_hg, 'hae', 'heq'
;submit, save link as... use this file in the next section, must be converted to a heliographic unit and saved
out = date_text +'         '+ string( transpose(d) ) + string(v_unit_hg, format='(3f19.9)')
out_hdr = ['Time and vector to Messenger from Sun in Heliographic Cartesian Coordinates (HEQ, HEEQ, Stonyhurst system)', $
          'Date				 Distance (km)       x(hat)             y(hat)             z(hat)']
out = [out_hdr, out]
prstr, out, file= outfile
error = 0
Get_Out:
end