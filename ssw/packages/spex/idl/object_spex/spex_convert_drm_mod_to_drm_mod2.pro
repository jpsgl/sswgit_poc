;+
; Name: spex_convert_drm_mod_to_drm_mod2
; 
; Purpose: Convert an object set up to use the drm_mod component to one using the new drm_mod2 component.  The first three
;   parameters in drm_mod are the same as drm_mod2, but drm_mod2 has 4 more - the center thickness ratio and three dummies.
; 
; Method: First modifies the spex_summ structure, then the current fit_comp... parameters.  In both cases, the drm_mod 
;   component is removed. The drm_mod2 component is added to the end of the components with default settngs.
;   The drm_mod values for starting param, param, minima, maxima, free_mask, and sigma that were in the old structure are set 
;   into the first three elements of the arrays for drm_mod2.
;   
; Arguments:
; o - ospex object that you want to change
; 
; Written: 26-Aug-2013, Kim Tolbert
;-


pro spex_convert_drm_mod_to_drm_mod2, o

; First change spex_summ structure values 
s = o->get(/spex_summ)
sfunc = s.spex_summ_fit_function

if strpos(sfunc, 'drm_mod') ne-1 and strpos(sfunc, 'drm_mod2') eq -1 then begin

  s_old = str_subset(s,['*params','*minima', '*maxima', '*free_mask', '*sigmas', '*func_spectrum', '*func_model'], /regex)  ; save old values
  iold = fit_function_query(sfunc, /param_elem, comp='drm_mod')
  
  spex_change_summ_comp, summ=s, comp_remove='drm_mod', comp_add='drm_mod2'
  
  inew = fit_function_query(s.spex_summ_fit_function, /param_elem, comp='drm_mod2')
  inew = inew[0:2] ; we'll only be replacing items 0,1,2 in drm_mod2 with drm_mod values
  s.spex_summ_starting_params[inew,*] = s_old.spex_summ_starting_params[iold,*]
  s.spex_summ_params[inew,*] = s_old.spex_summ_params[iold,*]
  s.spex_summ_minima[inew,*] = s_old.spex_summ_minima[iold,*]
  s.spex_summ_maxima[inew,*] = s_old.spex_summ_maxima[iold,*]
  s.spex_summ_free_mask[inew,*] = s_old.spex_summ_free_mask[iold,*]
  s.spex_summ_sigmas[inew,*] = s_old.spex_summ_sigmas[iold,*]
  
  o->set, _extra=s
  message, /cont, 'Removed drm_mod, added drm_mod2, and set modified spex_summ structure in object.'
  
endif
  
  ; Now change current fit function and fit_comp params
func = o->get(/fit_function)
; iold are the indices of the drm_mod values in thhe old fit_comp arrays for 
iold = fit_function_query(func, /param_elem, comp='drm_mod')
if iold[0] ne -1 then begin
  ; save old drm_mod values
  old_fc = o->get(/fit_comp_params,/fit_comp_minima, /fit_comp_maxima, /fit_comp_free_mask)
  
  ; remove drm_mod and drm_mod2 (will be at end of list of components)
  o->set,fit_function='-drm_mod'
  o->set,fit_function='+drm_mod2'
  
  ; inew will be indices of the drm_mod2 values in the new fit_comp arrays
  func_new = o->get(/fit_function)
  inew = fit_function_query(func_new, /param_elem, comp='drm_mod2')
  inew = inew[0:2] ; we'll only be replacing items 0,1,2 in drm_mod2 with drm_mod values
  
  ; get new arrays of fit_comp params and insert old values from drm_mod into parallel locations for drm_mod2
  new_fc = o->get(/fit_comp_params,/fit_comp_minima, /fit_comp_maxima, /fit_comp_free_mask)
  new_fc.fit_comp_params[inew] = old_fc.fit_comp_params[iold]
  new_fc.fit_comp_minima[inew] = old_fc.fit_comp_minima[iold]
  new_fc.fit_comp_maxima[inew] = old_fc.fit_comp_maxima[iold]
  new_fc.fit_comp_free_mask[inew] = old_fc.fit_comp_free_mask[iold]
  
  ; and set new fit_comp arrays back into object
  o->set, _extra=new_fc
  message, /cont, 'Removed drm_mod, added drm_mod2, and set modified function and fit_comp arrays in object.'
endif

end


