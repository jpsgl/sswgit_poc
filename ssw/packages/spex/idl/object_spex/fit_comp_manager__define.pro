; fit_comp_manager object - manages the multiple components of a fit function.
; It does this through the spex_gen_strategy_holder object which stores each
; component as a strategy.  Each strategy is an instance of the fit_comp object,
; but with different function and parameter settings.
; Methods: init
;			add_comp
;			delete_comp
;			get_comp
;			set
;			get
;			getdata
;			sep_comp_struct
;			concat_params
;
; Modifications:
;  30-Sep-2005, Kim.  in add method, make sure that defaults are available before
;    doing anything with new component.  Previously test was after calling
;    createstrategy.
;  7-Apr-2006, Kim.  Changes for new spectrum, model keywords.  Added comp_index,
;    and spectrum, model keywords to sep_comp_struct and concat_params methods.  In
;    set method, move test for strategy_name to before other stuff.  And added
;    list_function method.
; 16-Oct-2007, Kim.  In set method, when checking if fit_comp params are being set, check
;    for fc_... as well as fit_comp... (to include the now general fc_spectrum_options
;    and fc_model_options.  Also in concat_params method, added fc_spectrum_options and
;    fc_model_options to keywords, and added boost_tag commands for them.
; 26-Sep-2009, Kim. In concat_params, use exist instead of keyword_set (to handle input of 0.)
;
;--------------------------------------------------------------------------

pro fit_comp_manager_test, a
a=obj_new('fit_comp_manager')
a -> add_comp, 'vth'
a->list
a -> add_comp, 'bpow'
a->list
print,a->get(/fit_comp)

a->set,fit_xvals=findgen(5)+2.
print,a->get(/fit_xvals)

b=a->get_comp('vth')
help,b
print,b->get(/fit_xvals)

c=a->get_comp('bpow')
help,c
print,c->get(/fit_xvals)

a->set,fit_comp_param=[2,3],strategy_name='vth_0'

print,a->get(/fit_comp_param)

d1 = a->getdata(comp_name='vth')
d2 = a->getdata(comp_name='bpow')
d = a->getdata()
print, d1, d2, d1+d2, d

end

;--------------------------------------------------------------------------
function fit_comp_manager::init, _extra=_extra

source = obj_new('fit_comp_gen', fit_xvals=-1)

ret = self -> spex_gen_strategy_holder::init('', strategy_object='fit_comp', $
	source=source, info={fit_comp_manager_info} )

self -> set, compman_name='', compman_strategy=''
self.get_all_strategies = 1
return, ret
end

;--------------------------------------------------------------------------

pro fit_comp_manager::add_comp, comp_name

defaults = fit_function_query(comp_name, /defaults)

if comp_name eq 'vth_noline' then begin
	msg = 'Invalid function: vth_noline.  Use vth with continuum option instead.'
	if spex_get_nointeractive() then message,/cont,msg else a=dialog_message(msg)
	defaults = -1
endif

; if comp_name wasn't a valid name, then defaults will be -1, so don't set
if is_struct(defaults) then begin

	strategy = self -> createstrategy (strategy_type=comp_name, strategy_name=strategy_name)
	compman_name = [self->get(/compman_name), comp_name]
	compman_strategy = [self -> get(/compman_strategy), strategy_name]
	q = rem_elem(compman_name, '')	; initialized to ''. Get rid of that now if it's still there.
	self -> set, compman_name = compman_name[q], compman_strategy=compman_strategy[q]

	strategy -> set, fit_comp_function=comp_name, _extra = defaults
	nparams = fit_function_query(comp_name,/nparams)
	strategy -> set, fit_comp_sigmas=fltarr(nparams) ; init sigmas to 0.

endif

end

;--------------------------------------------------------------------------

pro fit_comp_manager::delete_comp, comp_name, strategy_name=strategy_name

strategy_name = self -> name2strat(comp_name=comp_name, strategy_name=strategy_name)

if strategy_name ne '' then begin
	self -> deletestrategy, strategy_name
	compman_name = self->get(/compman_name)
	compman_strategy = self -> get(/compman_strategy)
	q = rem_elem(compman_strategy, strategy_name, count)
	if count gt 0 then begin
		self -> set, compman_name=compman_name[q], compman_strategy=compman_strategy[q]
		return
	endif
endif else begin
	self -> deletestrategy	;delete all strategies
endelse

self -> set, compman_name = '', compman_strategy=''

end

;--------------------------------------------------------------------------

function fit_comp_manager::get_comp, comp_name, strategy_name=strategy_name

strategy_name = self -> name2strat(comp_name=comp_name, strategy_name=strategy_name)

return, self->getstrategy(strategy_name=strategy_name)
end

;--------------------------------------------------------------------------

pro fit_comp_manager::set, $
	comp_name=comp_name, $
	strategy_name=strategy_name, $
	done=done, $
	not_found=not_found, $
	_extra=_extra

done = 0
not_found = ''

if not keyword_set(_extra) then return

; if comp_name or strategy_name is set, then set for just this component
strategy_name = self -> name2strat(comp_name=comp_name, strategy_name=strategy_name)

if strategy_name ne '' then begin
	strategy = self -> get_comp(strategy_name=strategy_name)
	strategy -> set, _extra=_extra, done=done, not_found=not_found
	return
endif

; find if _extra has tag names that don't include fit_comp_.  If so, then strip out the fit_comp_...
; tags and call framework::set with the remaining.  Then modify _extra to leave ONLY the fit_comp_... tags.

etags = tag_names(_extra)
not_fit_comp = where (strpos(etags,'FIT_COMP_') eq -1 and $
						strpos(etags,'FC_') eq -1, count)

if count gt 0 then begin
	new_struct = struct_subset(_extra, etags[not_fit_comp])
	self->framework::set, _extra=new_struct, $
		not_found=not_found, $
		done=done
	if count eq n_elements(etags) then return else _extra = struct_subset (_extra, etags[not_fit_comp], /exclude)
endif


;otherwise, set for all components
i1=0
compman_name = self -> get(/compman_name)
compman_strategy = self -> get(/compman_strategy)
for index = 0, n_elements(compman_strategy)-1 do begin
	nparams = fit_function_query( compman_name[index], /nparams)
	strategy = self -> getstrategy( strategy_name=compman_strategy[index] )
	if obj_valid(strategy) then begin
		i2 = i1 + nparams-1
		new_struct = self -> sep_comp_struct(index,i1,i2, _extra=_extra, status=status)
		if status then strategy -> set, done=done, not_found=not_found, _extra=new_struct
		i1 = i2+1
	endif else return
endfor

end

;--------------------------------------------------------------------------

; Note: can't combine getting fit_comp... parameters with getting regular info parameters for this object
; Use nosingle keyword instead of letting it pass through in _extra, because when we're getting fit_comp...
; parameters, we pass only the tags with fit_comp... in the names, so need to pass nosingle explicitly.
; get(/fit_comp, /nosingle) makes it retrieve all control and info parameters that start with fit_comp - I think
; I need the nosingle because fit_comp_sigma is the only info param, and it gets returned as a single variable
; not as a structure, and then it's not concated with the fit_comp... control parameters already retrieved, so it's
; just missing.

function fit_comp_manager::get, $
	comp_name=comp_name, $
	strategy_name=strategy_name, $
	nosingle=nosingle, $
    not_found=not_found, $
    found=found, $
    fw_get_id=fw_get_id, fc_get_id=fc_get_id, $
    _extra=_extra

if keyword_set(_extra) then begin
	fit_comp_tags = where (strpos(tag_names(_extra),'FIT_COMP') ne -1 or $
							strpos(tag_names(_extra),'FC_') ne -1, count)
	if count eq 0 then return, self->framework::get(_extra=_extra, $
					nosingle=nosingle, $
					not_found=not_found, $
					fw_get_id=fw_get_id, fc_get_id=fc_get_id, $
					found=found)
endif

; if comp_name or strategy_name is set, then get for just this component
strategy_name = self -> name2strat (comp_name=comp_name, strategy_name=strategy_name)

if strategy_name ne '' then begin
	strategy = self->getstrategy(strategy_name=strategy_name)
	if obj_valid(strategy) then begin

		return, strategy -> get( $
		    nosingle=nosingle, $
		    not_found=not_found, $
		    found=found, $
		    fw_get_id=fw_get_id, fc_get_id=fc_get_id, $
		    _extra=_extra)

	endif else return,-1

endif else begin  ;otherwise, get requested fit params for all function components

	; here we will only retrieve fit_comp_... parameters, so remove other tags
	; from _extra.
	; This is because we need to concatenate the values from the different strategies,
	; and that only makes sense for the fit_comp... parameters.

	if keyword_set(_extra) then _extra = str_subset(_extra, (tag_names(_extra))[fit_comp_tags])
	compman_strategy = self -> get(/compman_strategy)
	for index=0, n_elements(compman_strategy)-1 do begin
		strategy = self -> get_comp( strategy_name=compman_strategy[index] )
		if obj_valid(strategy) then begin
			vals = strategy -> get( $
				nosingle=nosingle, $
				not_found=not_found, $
				found=found, $
				fw_get_id=fw_get_id, fc_get_id=fc_get_id, $
				_extra=_extra)
			if index eq 0 then ret_vals = vals else begin
				; after the first object, combine return values.
				; For structures, call concat_params to boost arrays in each element
				; For non-structure, just concat array
				ret_vals = size(vals, /tname) eq 'STRUCT' ? $
					self->concat_params(ret_vals, _extra=vals) : [ret_vals,vals]
			endelse
		endif

	endfor
	return, exist(ret_vals) ? ret_vals : -1

endelse
end

;--------------------------------------------------------------------------

; passing in strategy_name='' will pass back the sum of all component strategies

function fit_comp_manager::getdata, _extra=_extra

;print,'in fit_comp_manager::getdata'

if keyword_set(_extra) then self -> set, _extra=_extra

return, self -> framework::getdata(_extra=_extra)
end

;--------------------------------------------------------------------------

pro fit_comp_manager::process, $
	comp_name=comp_name, $
	strategy_name=strategy_name, $
	_extra=_extra

strategy_name = self -> name2strat(comp_name=comp_name, strategy_name=strategy_name)

; if strategy_name is blank means we want sum of all components
if strategy_name ne '' then begin

	; get data for this individual fit component
	strategy = self -> get_comp (strategy_name=strategy_name)
	result = strategy -> getdata()

endif else begin

	; get data for sum of all fit components
	compman_strategy = self->get(/compman_strategy)
	result = self->get(/fit_xvals) * 0.  ; init result to array of size of zeroes, dimensioned same as x values
	for i = 0, n_elements(compman_strategy)-1 do begin
		strategy = self -> get_comp( strategy_name=compman_strategy[i] )
		result = result + strategy -> getdata()
	endfor

endelse

self -> setdata, result

end

;--------------------------------------------------------------------------

; function to extract the values for a particular fit component out of a structure that has
; information for all fit components
; comp_index is the function component index - use that for structure tags that have only one item
; per component (i.e. the keywords
; i1,i2 are the indicies in each structure tag that has nparams items per component that we want to extract
; Example: Say _extra contained the tag fit_comp_params=[2,3,4,5,6,7] and fit_comp_minima=[1,2,3,4,5,6]
;   and that we've previously selected a fit function with 2 components - first has 2 params, second has 4.
;   If we want to extract the subarrays that apply to the second fit component, we would call this function
;   with _extra=_extra, i1=2, i2=5.  The returned structure will have tags
;   fit_comp_params=[4,5,6,7] and fit_comp_minima=[3,4,5,6] which are the values for the second fit component.

function fit_comp_manager::sep_comp_struct, comp_index, i1, i2, $
	fit_comp_params=fit_comp_params,$
	fit_comp_maxima=fit_comp_maxima, $
	fit_comp_minima=fit_comp_minima, $
	fit_comp_free_mask=fit_comp_free_mask, $
	fit_comp_sigmas=fit_comp_sigmas, $
	fit_comp_spectrum=fit_comp_spectrum, $
	fit_comp_model=fit_comp_model, $
	status=status

if keyword_set(fit_comp_params) then struct = add_tag(struct, fit_comp_params[i1:i2], 'fit_comp_params')
if keyword_set(fit_comp_maxima) then struct = add_tag(struct, fit_comp_maxima[i1:i2], 'fit_comp_maxima')
if keyword_set(fit_comp_minima) then struct = add_tag(struct, fit_comp_minima[i1:i2], 'fit_comp_minima')
if keyword_set(fit_comp_free_mask) then struct = add_tag(struct, fit_comp_free_mask[i1:i2], 'fit_comp_free_mask')
if keyword_set(fit_comp_sigmas) then struct = add_tag(struct, fit_comp_sigmas[i1:i2], 'fit_comp_sigmas')

; if any of the keyword tags are present add them to struct.  Here we don't index by
; i1:i2 because there's only one per function component, so use comp_index

if exist(fit_comp_spectrum) then struct = add_tag(struct, fit_comp_spectrum[comp_index], 'fit_comp_spectrum')
if exist(fit_comp_model) then struct = add_tag(struct, fit_comp_model[comp_index], 'fit_comp_model')

status =  exist(struct) ? 1 : 0
return, exist(struct) ? struct : -1
end

;--------------------------------------------------------------------------

; function to combine retrieved parameters from the multiple objects in the linked list
; for any of named keywords, boost that tag in struct by the new values in the keyword
; for anything left over in _extra, set that tag to the max of all the values in that tag

function fit_comp_manager::concat_params, $
	struct, $
	fit_comp_function=fit_comp_function, $
	fit_comp_params=fit_comp_params,$
	fit_comp_maxima=fit_comp_maxima, $
	fit_comp_minima=fit_comp_minima, $
	fit_comp_free_mask=fit_comp_free_mask, $
	fit_comp_sigmas=fit_comp_sigmas, $
	fit_comp_spectrum=fit_comp_spectrum, $
	fit_comp_model=fit_comp_model, $
	fc_spectrum_options=fc_spectrum_options, $
	fc_model_options=fc_model_options
;	_extra=_extra ;, $

if exist(fit_comp_function) then struct = boost_tag(struct, fit_comp_function, 'fit_comp_function')
if exist(fit_comp_params) then struct = boost_tag(struct, fit_comp_params, 'fit_comp_params')
if exist(fit_comp_maxima) then struct = boost_tag(struct, fit_comp_maxima, 'fit_comp_maxima')
if exist(fit_comp_minima) then struct = boost_tag(struct, fit_comp_minima, 'fit_comp_minima')
if exist(fit_comp_free_mask) then struct = boost_tag(struct, fit_comp_free_mask, 'fit_comp_free_mask')
if exist(fit_comp_sigmas) then struct = boost_tag(struct, fit_comp_sigmas, 'fit_comp_sigmas')

if exist(fit_comp_spectrum) then struct = boost_tag(struct, fit_comp_spectrum, 'fit_comp_spectrum')
if exist(fit_comp_model) then struct = boost_tag(struct, fit_comp_model, 'fit_comp_model')

; don't need to do this (and generates an error when the value is -1) because we'll always specify
; a comp_name or strategy_name when getting these two params, so never need to concat them for all comps
;if exist(fc_spectrum_options) then struct = boost_tag(struct, fc_spectrum_options, 'fc_spectrum_options')
;if exist(fc_model_options) then struct = boost_tag(struct, fc_model_options, 'fc_model_options')

return, struct
end

;--------------------------------------------------------------------------

;; function to return strategy name associated with a component name.  If there's more than
;; one component with comp_name, returns first strategy name found.
;
;function fit_comp_manager::name2strat, comp_name
;
;compman_name = self->get(/compman_name)
;q = where (compman_name eq comp_name, count )
;return, count eq 0 ? '' : (self->get(/compman_strategy))[q[0]]
;
;end

;--------------------------------------------------------------------------

pro fit_comp_manager::list_function, simple=simple, nolist=nolist, out=outlist

strat = self -> get(/compman_strategy)
if strat[0] eq '' then out = 'No fit components available.' else begin
	out = ['Current Fit Function and Parameters:', '']
	for i = 0,n_elements(strat) -1 do begin
		strategy = self->getstrategy(strategy_name=strat[i])
		comp_name = strategy->get(/fit_comp_function)
		defaults = fit_comp_defaults(comp_name)
		out = append_arr(out, ['', trim(i) +  '  Component Name: ' + comp_name ])
		if not keyword_set(simple) then begin
			help,strategy, out=strathelp
			out = [out, $
				'  Strategy name: '+ strat[i]+ '  Strategy index: '+ trim((*self.strategy_available_index)[i]), $
				'  ' + strathelp + '  Object valid: ' + trim(fix(obj_valid(strategy))) + $
				'  Need udpate: ' + trim(fix(strategy->get(/need_update))) ]
		endif
		struct = strategy->get(/fit_comp)
		out = [out, $
			'   Parameters:      ' + arr2str(trim(struct.fit_comp_params), '  '), $
			'   Minimum allowed: ' + arr2str(trim(struct.fit_comp_minima), '  '), $
			'   Maximum allowed: ' + arr2str(trim(struct.fit_comp_maxima), '  '), $
			'   Free Mask:       ' + arr2str(trim(fix(struct.fit_comp_free_mask)), '  ') ]
		if fit_comp_kw(struct, list_string=list_string, obj=self) then out=[out, list_string]
	endfor
	if not keyword_set(simple) then $
		out = [out, '', 'Current strategy index = ' + trim(self.strategy_current)]
endelse

outlist = out
if not keyword_set(nolist) then prstr, outlist;, /nomore

end

;--------------------------------------------------------------------------

pro fit_comp_manager__define

struct = {fit_comp_manager, $
	inherits spex_gen_strategy_holder }

end