;+
; NAME:
;	F_THICK_DEFAULTS
;
; PURPOSE: Function to return default values for
;   parameters, minimum and maximum range, and free parameter mask when
;   fitting to f_thick function.
;
; CALLING SEQUENCE: defaults = f_thick_defaults()
;
; INPUTS:
;	None
; OUTPUTS:
;	Structure containing default values
;
; MODIFICATION HISTORY:
; Kim Tolbert, February 2004
;
;-
;------------------------------------------------------------------------------

function f_thick_defaults

defaults = { $
  fit_comp_params:      	 [1.,  4., 600., 6., 10., 3.2e4], $
  fit_comp_minima:           [1e-10,  1.1, 1., 1.1, 1., 100.], $
  fit_comp_maxima:           [1e10,  20,  1e5, 20,  1e3, 1e7], $
  fit_comp_free_mask:        [1,1,0,0,0,0] $
  }

return, defaults

end