pro ospex_params_manual, d

k = -1
d = replicate({ospex_params_str}, 2000)

; Control parameters for fit_comp
k = k + 1
d[k].name = 'fit_comp_function'
d[k].class = 'fit_comp'
d[k].type = 'string'
d[k].units = ''
d[k].default = 'vth'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Array of fit function component names'
d[k].more_doc = '>fit_function'

k = k + 1
d[k].name = 'fit_comp_params'
d[k].class = 'fit_comp'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = '[1., 2.]'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Fit function parameters'
d[k].more_doc = '>fit_function'

k = k + 1
d[k].name = 'fit_comp_minima'
d[k].class = 'fit_comp'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = '[1.e-20, .5]'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Fit function parameter minimum values'
d[k].more_doc = '<h3 align="center">Fit Parameters Limits and Free Parameter Mask</h3>' + $ 
  'When iterating the parameter values to find the best fit to the model, ' + $
	'the following parameters are used.  Each is dimensioned to the sum of the number ' + $
	'of parameters for each function component in fit_function.' + $
	'<P>fit_comp_minima , fit_comp_maxima - limits the range of values for each parameter ' + $
	'<P>fit_comp_free_mask - 0 or 1 indicating whether the parameter is fixed, or free ' + $
	'to vary during fitting' + $
	'<P><P>For example for a multi_therm_pow+line function, you would set values like this: ' + $
	'<P>fit_comp_minima=[1.e-10, .5, 1., .01, .01,  1.e-20, 1., .01] ' + $
	'<P>fit_comp_free_mask=[1, 0, 1, 1, 0,  1, 0, 0]' + $
	'<P>where the first 5 values are for multi_therm_pow and the next 3 for line'

k = k + 1
d[k].name = 'fit_comp_maxima'
d[k].class = 'fit_comp'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = '[1.e20, 50.]'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Fit function parameter maximum values'
d[k].more_doc = '>fit_comp_minima'

k = k + 1
d[k].name = 'fit_comp_free_mask'
d[k].class = 'fit_comp'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = '[1,1]'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Fit function parameter free/fixed mask'
d[k].more_doc = '>fit_comp_minima'

k = k + 1
d[k].name = 'fit_comp_spectrum'
d[k].class = 'fit_comp'
d[k].type = 'string'
d[k].units = ''
d[k].default = 'full (for thermal)'
d[k].range = 'full / continuum / lines'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Fit function spectrum type (for thermal)'
d[k].more_doc = '>fit_function'

k = k + 1
d[k].name = 'fit_comp_model'
d[k].class = 'fit_comp'
d[k].type = 'string'
d[k].units = ''
d[k].default = 'chianti (for thermal)'
d[k].range = 'chianti / mewe'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Fit function model'
d[k].more_doc = '>fit_function'

; Control parameters for fit_comp_gen
k = k + 1
d[k].name = 'fit_xvals'
d[k].class = 'fit_comp_gen'
d[k].type = 'pointer'
d[k].units = 'keV'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Energies used in fit (2,n)'
d[k].more_doc = '>fit_function'

; Control parameters for fit_function
k = k + 1
d[k].name = 'fit_function'
d[k].class = 'fit_function'
d[k].type = 'string'
d[k].units = ''
d[k].default = 'vth'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Fit function used'
d[k].more_doc = '<h3 align="center">Fit Function Parameters</h3>' + $ 
  'The fit function computes the model output based on these parameters:' + $
	"<P>fit_function - function component names separated by '+', e.g. 'vth+bpow'.  " + $
	'The output is the sum of each function component computed at each energy in fit_xvals.' + $
	'<br><a href="http://hesperia.gsfc.nasa.gov/ssw/packages/spex/idl/object_spex/fit_model_components.txt">List of Function Components</a>' + $
	'<P>fit_xvals - energy edges in keV as 2,n array ' + $
	'<P>fit_comp_params - dimensioned to the sum of the number of parameters for ' + $
	'each component' + $
	"<P>fit_comp_spectrum  - options are 'full', 'continuum', or 'lines', " + $
	"default is 'full', dimensioned to the number of function components" + $
	"<P>fit_comp_model - options are 'chianti' or 'mewe', " + $
	"default is 'chianti', dimensioned to the number of function components " + $
	'<P>fit_comp_spectrum and fit_comp_model apply only to thermal functions, and are ' + $
	'blank for non-thermal components' + $
	'<P><P>For example for a multi_therm_pow+line function, you would have something like: ' + $
	'<P>fit_comp_params=[.005, .5, 4., 1., 1.,  1000., 6.7, .1]  where the first 5 values are ' + $
	'for multi_therm_pow and the next 3 for line' + $
	"<P>fit_comp_spectrum=['continuum', ''] where the first string applies to multi_therm_pow, " + $
	'and the second (null) string applies to the line function' + $
	'<P><P>Note: the fit_comp_function parameter should not be set by the user.  It is ' + $
	'set internally to an array of the function component names corresponding to the names ' + $
	'in the fit_function parameter string.'

; Control parameters for spex_bkint
k = k + 1
d[k].name = 'spex_bk_eband'
d[k].class = 'spex_bkint'
d[k].type = 'pointer'
d[k].units = 'keV'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Energy bands for background if spex_bk_sep is set'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_bk_time_interval'
d[k].class = 'spex_bkint'
d[k].type = 'pointer'
d[k].units = 'ANYTIM'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Background time intervals'
d[k].more_doc = 'If spex_bk_sep is set, then spex_bk_time_interval is an array of ' + $
	'pointers, one for each spex_bk_eband energy band.  Each pointer points to an ' + $
	'array of background time intervals dimensioned [2,n] for that energy band.' + $
	'  <P>Note that once spex_bk_sep has been set, even after unsetting spex_bk_sep, ' + $
	' spex_bk_time_interval will remain ' + $
	' a pointer array in order to remember your settings, even though only the first' + $
	' pointer will be used. ' + $
	'<P>When spex_bk_sep is set, use this_band and this_time to get and set background ' + $
	'time intervals for each band: ' + $
	'<P>time1 = o -> get(this_band=1, /this_time)' + $
	"<P>o -> set, this_band=1, this_time=['20-Feb-2002 10:56:23.040', '20-Feb-2002 10:57:02.009']"

k = k + 1
d[k].name = 'spex_bk_sep'
d[k].class = 'spex_bkint'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, separate background for different energy bands'
d[k].more_doc = ''

; Control parameters for spex_bk
k = k + 1
d[k].name = 'spex_bk_order'
d[k].class = 'spex_bk'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = '1'
d[k].range = '0 - 3'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Order of polynomial or method for computing background fit'
d[k].more_doc = '<h3 align="center">Background Methods</h3>' + $
  'If spex_bk_sep is 0, spex_bk_order is a scalar. ' + $
	'<P>If spex_bk_sep is set, then spex_bk_order is an array of ' + $
	'integers, one for each spex_bk_eband energy band.  ' + $
	'<P>The allowed values for spex_bk_order are: ' + $
	'<br>0 - 0Poly, 0th order polynomial' + $
	'<br>1 - 1Poly, 1st order polynomial' + $
	'<br>2 - 2Poly, 2nd order polynomial' + $
	'<br>3 - 3Poly, 3rd order polynomial' + $
	'<br>4 - Exp, exponential' + $
	'<br>5 - High E Profile, use the ratio to the profile in the highest energy band' + $
	'<br>6 - This E Profile, use the ratio to the profile in this energy band' + $
	'  <P>Note that once spex_bk_sep has been set, even after unsetting spex_bk_sep, ' + $
	' spex_bk_order will remain ' + $
	' a vector in order to remember your settings, even though only the first' + $
	' element will be used. ' + $
	'<P>When spex_bk_sep is set, use this_band and this_order to get and set the order ' + $
	'for each band: ' + $
	'<P>order1 = o -> get(this_band=1, /this_order)' + $
	"<P>o -> set, this_band=1, this_order=2" + $
	'<P>spex_bk_ratio - This parameter is deprecaded. spex_bk_order = 5 now means use the ratio ' + $
	 'to the high-energy band profile, but now that option can be applied to any energy band.  Previously, ' + $
	 'it was either set or not set for all bands.' + $
	 '<P>When one of the two Profile methods (spex_bk_order = 5 or 6) is selected ' + $
	 'for an energy band, the time profile of ' + $
	 'the background for the highest energy band or the current energy band is ' + $
	 'used to define the shape of the background for all raw energy bins within that energy band. For each energy ' + $
	 'bin, the background time profile is computed by multiplying the ' + $
	 'smoothed profile by the ratio of the rate in that energy to ' + $
	 'the smoothed profile in the time intervals selected for that energy. ' + $
	 '<br>More specifically: ' + $
	 '<br>1.  The background profile is computed by interpolating across gaps in the time ' +$ 
	 'intervals selected. ' + $
	 '<br>2.  This profile is smoothed using the smoothing width specified by the ' + $
	 'spex_bk_sm_width parameter (the profile is convolved with a Savitzky-Golay smoothing filter ' + $
	 'of the width selected). ' + $
	 '<br>3.  For each data energy bin (the raw bins, not the broader energy ' + $
	 'bands you have selected), the data in the time intervals selected for ' + $
	 'the energy band containing this bin are averaged and divided by the ' + $
	 'average of the smoothed profile in those times. ' + $
	 '<br>4.  The background time profile for this energy bin is computed by ' + $
	 'multiplying the smoothed profile by that ratio. ' + $
	 '<br>5.  The errors on the background are the square root of the counts in the time ' + $
	 'intervals used to calculate the ratio.'

k = k + 1
d[k].name = 'spex_bk_ratio'
d[k].class = 'spex_bk'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Use ratio of high-energy bk profile for bk for lower energies'
d[k].more_doc = '>spex_bk_order'

k = k + 1
d[k].name = 'spex_bk_sm_width'
d[k].class = 'spex_bk'
d[k].type = 'int'
d[k].units = ''
d[k].default = '128'
d[k].range = '1 - ?'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Smoothing width (# of points), used when background method uses profile'
d[k].more_doc = '>spex_bk_order'

k = k + 1
d[k].name = 'spex_bk_poisson_error'
d[k].class = 'spex_bk'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, bk data errors are sqrt(counts). Otherwise errors ' + $
    'are averaged.'
d[k].more_doc = 'Set to 0 (default) when background is computed by fit to data in ' + $
  'pre- and post-flare intervals.  In this case, the background estimate is only as good ' + $
  'as the fit to those intervals.  The percentage uncertainty should not ' + $
  'change when binning over multiple time bins.  So average errors rather than combining ' + $
  'errors in quadrature (which assumes bk is based on statistics, and error is sqrt(counts)). ' + $
  '<P>Set to 1 when bk is uncoupled (e.g. if you inserted background from a ' + $
  'previous orbit) and errors should be summed in quadrature.'  + $
  '<P>(Introduced 9-Dec-2009)'

; Control parameters for spex_drm
k = k + 1
d[k].name = 'spex_drmfile'
d[k].class = 'spex_drm'
d[k].type = 'string'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Input DRM file name'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_albedo_correct'
d[k].class = 'spex_drm'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, correct drm for albedo of photosphere'
d[k].more_doc = 'The albedo correction depends on an angle and an anisotropy estimate.' + $
	'<P>See the parameters spex_source_angle, spex_source_xy, and spex_anisotropy ' + $
	'for more details. <P>Note: Currently (as of Sept 2004), the albedo correction can ' + $
	'be applied only if the DRM energy edges are on integer boundaries.'

k = k + 1
d[k].name = 'spex_anisotropy'
d[k].class = 'spex_drm'
d[k].type = 'float'
d[k].units = ''
d[k].default = '1.'
d[k].range = '0. - 1.'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Anisotropy ratio, for albedo correction'
d[k].more_doc = 'Used in the albedo correction.  <P>Anisotropy ratio is the ' + $
	'ratio of the flux in the viewing direction to the flux downwards.  ' + $
	'A value of 1 means the source is isotropic.'

k = k + 1
d[k].name = 'spex_source_angle'
d[k].class = 'spex_drm'
d[k].type = 'float'
d[k].units = ''
d[k].default = '45.'
d[k].range = '0. - 90.'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Angle to source in degrees, for albedo correction'
d[k].more_doc = 'Used in the albedo correction.  <P>The albedo correction ' + $
	'depends on the angle between the normal to the Sun surface at the flare ' + $
	'location and the viewing direction.  The angle can be specified directly ' + $
	'by supplying an angle (in degrees) or by specifying the position of the ' + $
	'flare in arcsec.' + $
	'<P>If you set the source angle and xy position in the same call, the xy ' + $
	'position takes precedence.  If you set an angle directly, the parameter ' + $
	'spex_source_xy will be set to [-9999.,-9999.] since it can not be computed ' + $
	'from the angle.'

k = k + 1
d[k].name = 'spex_source_xy'
d[k].class = 'spex_drm'
d[k].type = 'float(2)'
d[k].units = ''
d[k].default = '[-9999.,-9999.]'
d[k].range = '0. - ~950.'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'XY Position of source in arcsec, for albedo correction'
d[k].more_doc = 'Used in the albedo correction.  <P>The albedo correction ' + $
	'depends on the angle between the normal to the Sun surface at the flare ' + $
	'location and the viewing direction.  The angle can be specified directly ' + $
	'by supplying an angle (in degrees) or by specifying the position of the ' + $
	'flare in arcsec.' + $
	'<P>If you set the source angle and xy position in the same call, the xy ' + $
	'position takes precedence.  If you set an angle directly, the parameter ' + $
	'spex_source_xy will be set to [-9999.,-9999.] since it can not be computed ' + $
	'from the angle.'

k = k + 1
d[k].name = 'spex_drm_dataobj'
d[k].class = 'spex_drm'
d[k].type = 'objref'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Data object to get info from if needed'
d[k].more_doc = ''

; Control parameters for spex_fitalg_gen
k = k + 1
d[k].name = 'spex_error_use_expected'
d[k].class = 'spex_fitalg_gen'
d[k].type = 'int'
d[k].units = ''
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, use expected counts to calc error'
d[k].more_doc = 'Normally errors used in the fit are computed by combining the ' + $
	'expected count rates with the background count rates and the systematic error.  ' + $
	'If you want to use the errors you set into the object (they will still be ' + $
	'combined with the systematic error), set this parameter to 0.'

k = k + 1
d[k].name = 'spex_uncert'
d[k].class = 'spex_fitalg_gen'
d[k].type = 'float'
d[k].units = ''
d[k].default = '.05'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Systematic Uncertainty'
d[k].more_doc = 'The systematic uncertainty is added in ' + $
	'quadrature to the poisson statistical uncertainty for each data point to ' + $
	'compute the error bar on each point.  The value used for the systematic ' + $
	'uncertainty is arbitrary - it is really a fudge factor that allows you to prevent ' + $
	'the ridiculously small statistical uncertainties that might result from ' + $
	'very high count rates from skewing the fit results.'

; Control parameters for spex_fitalg_mcurvefit
k = k + 1
d[k].name = 'mcurvefit_itmax'
d[k].class = 'spex_fitalg_mcurvefit'
d[k].type = 'long'
d[k].units = ''
d[k].default = '10'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Maximum number of iterations in mcurvefit fit'
d[k].more_doc = ''

k = k + 1
d[k].name = 'mcurvefit_tol'
d[k].class = 'spex_fitalg_mcurvefit'
d[k].type = 'double'
d[k].units = ''
d[k].default = '0.001'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Convergence Tolerance. Stop fit if relative decrease in ' + $
    'chi-squared < tol'
d[k].more_doc = ''

k = k + 1
d[k].name = 'mcurvefit_quiet'
d[k].class = 'spex_fitalg_mcurvefit'
d[k].type = 'byte'
d[k].units = ''
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = '0/1 quiet/verbose during mcurvefit'
d[k].more_doc = ''

; Control parameters for spex_fitalg_poly_fit
k = k + 1
d[k].name = 'poly_fit_n_degree'
d[k].class = 'spex_fitalg_poly_fit'
d[k].type = 'long'
d[k].units = ''
d[k].default = '2'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Number of degrees of freedom for polynomial fit.  Not used yet.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'poly_fit_double'
d[k].class = 'spex_fitalg_poly_fit'
d[k].type = 'byte'
d[k].units = ''
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Not used yet.'
d[k].more_doc = ''

; Control parameters for spex_fitint
k = k + 1
d[k].name = 'spex_fit_time_interval'
d[k].class = 'spex_fitint'
d[k].type = 'pointer'
d[k].units = 'ANYTIM'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Fit time intervals, (2,n)'
d[k].more_doc = ''

; Control parameters for spex_fitrange
k = k + 1
d[k].name = 'spex_erange'
d[k].class = 'spex_fitrange'
d[k].type = 'pointer'
d[k].units = 'keV'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Energy range(s) to fit over (2,n)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_intervals_tofit'
d[k].class = 'spex_fitrange'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = '-1'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Array of interval numbers to fit (start from 0)'
d[k].more_doc = ''

; Control parameters for spex_fit
k = k + 1
d[k].name = 'spex_fit_manual'
d[k].class = 'spex_fit'
d[k].type = 'int'
d[k].units = ''
d[k].default = '1'
d[k].range = '0 - 2'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = '0=automatic, 1=manual on first interval, 2=manual on all ' + $
    'intervals'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_fit_reverse'
d[k].class = 'spex_fit'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Interval Loop direction, 0 = forward, 1 = reverse'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_fit_start_method'
d[k].class = 'spex_fit'
d[k].type = 'string'
d[k].units = ''
d[k].default = 'previous_int'
d[k].range = 'default / previous_int / previous_start / previous_iter'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Method for getting starting params for fit after first interval'
d[k].more_doc = '<h3 align="center">Fit Parameter Initialization</h3>' + $ 
        '<P>There are two fit parameter initialization methods:' + $
        '<P>SPEX_FIT_FIRSTINT_METHOD - sets parameters for first interval in the series of intervals selected for fitting.' + $
        '<br>The following parameters are set (unless spex_fit_init_params_only is set to 1, see below): ' + $
        '<br>fit_comp_params, fit_comp_minima, fit_comp_maxima, fit_comp_free_mask, ' + $
        'fit_comp_spectrum, fit_comp_model' + $
        '<br> Choices are:' + $
        '<br> default - Use program defaults for each function component' + $
        '<br> current - Leave all parameters at their current setting' + $
        '<br> fn - Use parameters from interval n. Use final fitted values from interval n for fit_comp_params' + $
        '<br> sn - Use parameters from interval n. Use starting values from interval n for fit_comp_params' + $        
        '<P><br>SPEX_FIT_START_METHOD - sets parameters for subsequent (after first) intervals in the series of ' + $
        'intervals selected for fitting.' + $
        '<br> The following parameters are set (unless spex_fit_init_params_only is set to 1, see below): ' + $
        '<br>same list as above, as well as ' + $
        'erange, uncertainty, # iterations (except for default method)' + $
        '<br> Choices are:' + $        
        '<br> default - Use program defaults for each function component' + $
        '<br> previous_int - Use values most recently fit interval. Use final fitted value for fit_comp_params' + $
        '<br> previous_start - Use values most recently fit interval. Use starting value for fit_comp_params' + $
        '<br> previous_iter - Use values from earlier fit on each interval.  Use ' + $
        'final fitted values from the previous iteration for fit_comp_params.  If no earlier fit done, leave all parameters ' + $
        'at current settings.' + $        
        '<P>Note: If spex_fit_start_method is set to previous_iter, it applies to the first interval as well, ' + $
        'and spex_fit_firstint_method is ignored.' + $
        '<P>SPEX_FIT_INIT_PARAMS_ONLY - If set, transer only the fit parameter values to the current ' + $
        'interval being fit (which will be set based on your selections for first and subsequent interval ' + $
        'initialization method. ' + $
        '<br>If 0 (default), then transer min, max, free, erange, uncertainty, #iter and fit ' +$
        'parameter values.'
        
k = k + 1
d[k].name = 'spex_fit_firstint_method'
d[k].class = 'spex_fit'
d[k].type = 'string'
d[k].units = ''
d[k].default = 'current'
d[k].range = 'default / current / fn / sn'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Method for setting starting params for fit for first ' + $
    'interval (fn,sn = final,start from interval n)'
d[k].more_doc = '>spex_fit_start_method'

k = k + 1
d[k].name = 'spex_fit_init_params_only'
d[k].class = 'spex_fit'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, set params only according to firstint_method ' + $
    'or start_method'
d[k].more_doc = '>spex_fit_start_method'

k = k + 1
d[k].name = 'spex_fit_auto_erange'
d[k].class = 'spex_fit'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, automatically set upper limit of energy range to fit'
d[k].more_doc = '<h3 align="center">Auto-setting Energy Range to Fit</h3>' + $
  '<P>Note: These settings modify spex_erange.  spex_erange can be an array of start/end energies. ' + $
  'The energy intervals will be preserved if they are between the new low/high limits.' + $
  '<P>spex_fit_auto_erange - If set, automatically select upper limit for energy range ' + $
  'to fit based on background-subtracted counts/bin.  For energies > 10. keV, find ' + $
  'the energy bin where the counts/bin becomes less than spex_fit_auto_emax_thresh, ' + $
  'and set spex_erange upper limit to the lower edge of the bin.<br>' + $
  '(Prior to 30-Oct-2013, we used the count rate > .01 test, and rounded the energy up to the next multiple of 10.)' + $
  '<P>spex_fit_auto_emax_thresh - Threshold for automatically setting upper limit of ' + $
  'energy range to fit.  Default is 10.0 counts/bin.' + $
  '<P>spex_fit_auto_emin - If set, automatically select lower limit for energy range to fit. ' + $
  'This applies only to RHESSI data: <br>' + $
  '&nbsp;&nbsp; attenuator state A0: low limit is 4.0 keV.<br>' + $
  '&nbsp;&nbsp; attenuator state A1 or A3: low limit is 6.0 keV.' + $
  '<P>Note: Normally for every parameter initialization ' + $
  'method other than "current" or "default", the fit energy range is initialized from the interval ' + $
  'you select as the basis for your parameter values.  However, if spex_fit_auto_erange or spex_fit_auto_emin is set, ' + $
  'they override the energy range transferred from a previous fit of an interval.  '  

k = k + 1
d[k].name = 'spex_fit_auto_emax_thresh'
d[k].class = 'spex_fit'
d[k].type = 'float'
d[k].units = 'counts/bin'
d[k].default = '10.0'
d[k].range = '10. - ?'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Threshold for #counts/bin for automatically setting upper ' + $
    'limit of energy range to fit.'
d[k].more_doc = '>spex_fit_auto_erange'

k = k + 1
d[k].name = 'spex_fit_auto_emin'
d[k].class = 'spex_fit'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, automatically set lower limit of energy range to fit ' + $
    '(only applies to RHESSI)'
d[k].more_doc = '>spex_fit_auto_erange'

; Control parameters for spex_data_strategy
k = k + 1
d[k].name = 'spex_specfile'
d[k].class = 'spex_data_strategy'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Input spectrum file name'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_data_sel'
d[k].class = 'spex_data_strategy'
d[k].type = 'string'
d[k].units = ''
d[k].default = ''
d[k].range = 'Depends on data source'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Instrument or detector selection if file has more than one type of data'
d[k].more_doc = 'Some data files contain data for more than one instrument. ' + $
  'As of July 2011, these are YOHKOH WBS and SOXS.' + $
  "<P>For SOXS, the choices are 'CZT' and 'SI' "+ $
  "<P>For YOHKOH WBS, the choices are 'GRS1', 'GRS2', and 'HXS' " + $
  '<P>The default is the first data type in the list.'  

k = k + 1
d[k].name = 'spex_ut_offset'
d[k].class = 'spex_data_strategy'
d[k].type = 'float'
d[k].units = 'sec'
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Time offset for data in seconds'
d[k].more_doc = 'Time array (spex_ut_edges) for data will be shifted later for ' + $
	'positive values of spex_ut_offset, earlier for negative values.  If you have ' + $
	'already defined other time intervals (background, fit) they will not be changed, ' + $
	'but the data in the intervals is now different and will be reaccumulated.'

k = k + 1
d[k].name = 'spex_ignore_filters'
d[k].class = 'spex_data_strategy'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set, ignore filters'
d[k].more_doc = 'Set to 1 when in a regime (e.g. high-energy rear detector RHESSI data analysis), ' + $
  'where you want OSPEX to ignore the filter (or attenuator) states.' + $
  "<P>Normally (spex_ignore_filters=0), OSPEX won't allow you to analyze a time " + $
  'interval that spans a change in filter state, and checks that the DRM used ' + $
  'matches the filter state of the analysis interval.'

k = k + 1
d[k].name = 'spex_accum_time'
d[k].class = 'spex_data_strategy'
d[k].type = 'double(2)'
d[k].units = ''
d[k].default = '[0.,0.] (full file)'
d[k].range = 'Time range of input file'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Time interval to accumulate for spex analysis. [0.,0.] means ' + $
    'full file.'
d[k].more_doc = 'Some input files contain an entire day of data (e.g. fermi gbm). ' + $
  'You may want to limit what OSPEX accumulates to part of the file.' + $
  '<P>For example:' + $
  "<br>  o->set,spex_accum_time=['12-jun-2010 0:52','12-jun-2010 01:10']" + $
  '<P> A value of [0.,0.] means read the entire file.'

; Control parameters for spex_image
k = k + 1
d[k].name = 'spex_roi_use'
d[k].class = 'spex_image'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 3'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Specifies which region(s) to use to compute spectrum'
d[k].more_doc ='<h3 align="center">Imaging Spectroscopy Regions</h3> ' + $
  'When input to OSPEX is an imagecube, use the ' + $
	'Region Selection tool for defining up to 4 regions (ROIs) in each image to ' + $
	'select which parts of each image to use to compute spectra through the image  ' + $
	'cube.' + $
	'<P>The following parameters control region selection and use: ' + $
	'<P>spex_roi_use  - Selects which region(s) ' + $
	'to use to compute spectra.  Any image that does not have that region ' + $
	'defined will not be used in the fitting process. ' + $
	'<br>Example:  o->set, spex_roi_use = 1 or o->set, spex_roi_use=[0,1]' + $
	'<P>spex_roi_integrate - If set, all regions defined are used to compute spectrum. ' + $
	'<P>spex_roi_infile - If set to an existing IDL save file compatible with ' + $
	'the current imagecube file (same number of times and energies), then regions ' + $
	'are initialized to regions in file. ' + $
	'(File was written by OSPEX by specifing spex_roi_outfile, or calling ' + $
	'save_roi method).  To unset, set to blank string.' + $
	'<P>spex_roi_outfile - Name of output save file to write region selection in. ' + $
	'To unset, set to blank string.' + $
	'<P>spex_roi_size - The size of each image in the Region Selection tool panel ' + $
	'display in device pixels.  Default is 50.' + $
	'<P>spex_roi_expand - The factor to expand selected images by in the Region ' + $
	'selection tool.  Default is 3.' + $
	'<P>spex_roi_color - Color table to use for panels in Region Selection tool. ' + $
	'Default is 5.'


k = k + 1
d[k].name = 'spex_roi_integrate'
d[k].class = 'spex_image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, integrate all regions to compute spectrum.'
d[k].more_doc = '>spex_roi_use'

k = k + 1
d[k].name = 'spex_roi_infile'
d[k].class = 'spex_image'
d[k].type = 'string'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Name of IDL save file to read ROI selection from.'
d[k].more_doc = '>spex_roi_use'

k = k + 1
d[k].name = 'spex_image_full_srm'
d[k].class = 'spex_image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, use full SRM for image cube'
d[k].more_doc = 'This option only applies to RHESSI image cubes.' + $
	'<P>RHESSI images are output in units of photons/cm^2/sec/asec^2, Since the ' + $
	'diagonal SRM was used to convert from count flux to photon flux, there ' + $
	'are no corrections for effects such as k-escape and compton scattering (the ' + $
	'off-diagonal elements of the detector response matrix).  Spectra from image cubes ' + $
	'should not be analyzed at low energies in the default mode ' + $
	'(i.e. with spex_image_full_srm set to 0.' + $
	'<P>If the spex_image_full_srm option is set, then OSPEX calculates the diagonal ' + $
	' SRM and applies to the spectrum to convert back to counts/cm^2/sec/asec^2 ' + $
	'(basically reversing the conversion from counts to photons applied in the ' + $
	'RHESSI image object).  A full SRM incorporating all off-diagonal elements is ' + $
	'calculated for each attenuator state that occurred during the time of the image ' + $
	'cube, and stored in the spex_drm object for use in converting the spectrum ' + $
	'to photon rate or flux.  You will notice that if this option is set ' + $
	'(spex_image_full_srm=1), you have the option to plot in count or photon space, ' + $
	'whereas you can only plot in photon space if the option is not set.'

; Info parameters for fit_comp
k = k + 1
d[k].name = 'fit_comp_sigmas'
d[k].class = 'fit_comp'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Sigma in fit parameters, (nparam)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'fc_spectrum_options'
d[k].class = 'fit_comp'
d[k].type = 'string(3)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Options for fit_comp_spectrum'
d[k].more_doc = ''

k = k + 1
d[k].name = 'fc_model_options'
d[k].class = 'fit_comp'
d[k].type = 'string(2)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Options for fit_comp_model'
d[k].more_doc = ''

; Info parameters for fit_comp_manager
k = k + 1
d[k].name = 'compman_name'
d[k].class = 'fit_comp_manager'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Array of function component names'
d[k].more_doc = ''

k = k + 1
d[k].name = 'compman_strategy'
d[k].class = 'fit_comp_manager'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Array of function component strategy names'
d[k].more_doc = ''

; Info parameters for fit_function
k = k + 1
d[k].name = 'fit_func_units'
d[k].class = 'fit_function'
d[k].type = 'struct(1)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Structure with units of fit function output'
d[k].more_doc = ''

; Info parameters for spex_bkint

k = k + 1
d[k].name = 'spex_have_bk'
d[k].class = 'spex_bkint'
d[k].type = 'int'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'If set, some background intervals have been defined.'
d[k].more_doc = ''

; Info parameters for spex_bksub
k = k + 1
d[k].name = 'spex_bksub_origunits'
d[k].class = 'spex_bksub'
d[k].type = 'struct(1)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Structure with original units for background-subtracted data'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_bksub_units'
d[k].class = 'spex_bksub'
d[k].type = 'struct(1)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Structure with units of last accumulation for ' + $
    'background-subtracted data'
d[k].more_doc = ''

; Info parameters for spex_bk
k = k + 1
d[k].name = 'spex_bk_origunits'
d[k].class = 'spex_bk'
d[k].type = 'struct(1)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Structure with original units for background data'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_bk_units'
d[k].class = 'spex_bk'
d[k].type = 'struct(1)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Structure with units of last accumulation for background ' + $
    'data'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_bk_used_ratio'
d[k].class = 'spex_bk'
d[k].type = 'int'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = " 0/1 means we didn't / did use the ratio method for bk calc"
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_bk_ratprofile_plot'
d[k].class = 'spex_bk'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = " If set, show plot of background profile " + $
    "used for 'High E Profile' or 'This E Profile' ratio method"
d[k].more_doc = ''

; Info parameters for spex_drm
k = k + 1
d[k].name = 'spex_drm_data_name'
d[k].class = 'spex_drm'
d[k].type = 'string'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Data type DRM file is for, e.g. HESSI'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_drm_area'
d[k].class = 'spex_drm'
d[k].type = 'float'
d[k].units = 'cm^2'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Detector area from DRM file'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_drm_ct_edges'
d[k].class = 'spex_drm'
d[k].type = 'pointer'
d[k].units = 'keV'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Count space energy edges in DRM file (2,n)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_drm_ph_edges'
d[k].class = 'spex_drm'
d[k].type = 'pointer'
d[k].units = 'keV'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Photon space energy edges in DRM file (2,n)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_drm_sepdets'
d[k].class = 'spex_drm'
d[k].type = 'int'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'If set, detectors are separate in DRM file'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_drm_filter'
d[k].class = 'spex_drm'
d[k].type = 'int'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Filter (attenuator) state(s) in DRM file'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_drm_current_filter'
d[k].class = 'spex_drm'
d[k].type = 'int'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Current filter in use'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_drm_times'
d[k].class = 'spex_drm'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Time interval for each DRM matrix'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_drm_current_time'
d[k].class = 'spex_drm'
d[k].type = 'double(2)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Current DRM time interval in use'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_drm_detused'
d[k].class = 'spex_drm'
d[k].type = 'string'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'String of detectors included in DRMfile'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_drm_method'
d[k].class = 'spex_drm'
d[k].type = 'string'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Method used to get drm values: "file", "array", or "build"'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_drm_mod_obj'
d[k].class = 'spex_drm'
d[k].type = 'objref'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Used internally in drm_mod function'
d[k].more_doc = ''

; Info parameters for spex_fitalg_mcurvefit
k = k + 1
d[k].name = 'mcurvefit_fail_type'
d[k].class = 'spex_fitalg_mcurvefit'
d[k].type = 'int'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Mcurvfit success/failure code'
d[k].more_doc = ''

k = k + 1
d[k].name = 'mcurvefit_fail_msg'
d[k].class = 'spex_fitalg_mcurvefit'
d[k].type = 'string'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Mcurvefit success/failure message'
d[k].more_doc = ''

k = k + 1
d[k].name = 'mcurvefit_iter'
d[k].class = 'spex_fitalg_mcurvefit'
d[k].type = 'long'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Number of iterations done in mcurvefit'
d[k].more_doc = ''

k = k + 1
d[k].name = 'mcurvefit_chi2'
d[k].class = 'spex_fitalg_mcurvefit'
d[k].type = 'double'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Chi-squared value after mcurvefit'
d[k].more_doc = ''

k = k + 1
d[k].name = 'mcurvefit_corr'
d[k].class = 'spex_fitalg_mcurvefit'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Correlation matrix'
d[k].more_doc = 'The correlation matrix is the covariance matrix normalized by the diagonals ' + $
  'returned by mcurvefit for the parameters that were fit.  A 1. indicates high correlation between the ' + $
  'pair of parameters, 0. indicates no correlation.'

k = k + 1
d[k].name = 'mcurvefit_covar'
d[k].class = 'spex_fitalg_mcurvefit'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Covariance matrix'
d[k].more_doc = ''

k = k + 1
d[k].name = 'mcurvefit_free_mask'
d[k].class = 'spex_fitalg_mcurvefit'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Parameter free/fixed mask used'
d[k].more_doc = ''

; Info parameters for spex_fitalg_poly_fit
k = k + 1
d[k].name = 'poly_fit_chisq'
d[k].class = 'spex_fitalg_poly_fit'
d[k].type = 'double'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'poly_fit_status'
d[k].class = 'spex_fitalg_poly_fit'
d[k].type = 'int'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'poly_fit_covar'
d[k].class = 'spex_fitalg_poly_fit'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'poly_fit_yband'
d[k].class = 'spex_fitalg_poly_fit'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'poly_fit_yerror'
d[k].class = 'spex_fitalg_poly_fit'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

; Info parameters for spex_fitint
k = k + 1
d[k].name = 'spex_fit_time_used'
d[k].class = 'spex_fitint'
d[k].type = 'pointer'
d[k].units = 'ANYTIM'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Fit time intervals used (spex_fit_time_interval adjusted to data boundaries), (2,n)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_interval_index'
d[k].class = 'spex_fitint'
d[k].type = 'int'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Index of current fit interval'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_fitint_filter'
d[k].class = 'spex_fitint'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Data filter for each fit interval'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_fitint_origunits'
d[k].class = 'spex_fitint'
d[k].type = 'struct(1)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Structure with original units for fit interval data'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_fitint_units'
d[k].class = 'spex_fitint'
d[k].type = 'struct(1)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Structure with units of last accumulation for fit interval ' + $
    'data'
d[k].more_doc = ''

; Info parameters for spex_fit
k = k + 1
d[k].name = 'spex_autoplot_enable'
d[k].class = 'spex_fit'
d[k].type = 'int'
d[k].units = ''
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Admin'
d[k].level = 'N/A'
d[k].purpose = 'If set, automatically plot after fitting'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_autoplot_bksub'
d[k].class = 'spex_fit'
d[k].type = 'int'
d[k].units = ''
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Admin'
d[k].level = 'N/A'
d[k].purpose = 'If set, plot data-bk, not data with bk in autoplot'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_autoplot_overlay_back'
d[k].class = 'spex_fit'
d[k].type = 'int'
d[k].units = ''
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Admin'
d[k].level = 'N/A'
d[k].purpose = 'If set, overlay bk in autoplot'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_autoplot_overlay_bksub'
d[k].class = 'spex_fit'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Admin'
d[k].level = 'N/A'
d[k].purpose = 'If set, overlay data-bk in autoplot'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_autoplot_show_err'
d[k].class = 'spex_fit'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Admin'
d[k].level = 'N/A'
d[k].purpose = 'If set, show error bars in autoplot'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_autoplot_photons'
d[k].class = 'spex_fit'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Admin'
d[k].level = 'N/A'
d[k].purpose = 'If set, plot in photon space in autoplot'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_autoplot_units'
d[k].class = 'spex_fit'
d[k].type = 'string'
d[k].units = ''
d[k].default = 'flux'
d[k].range = ''
d[k].control_or_info = 'Admin'
d[k].level = 'N/A'
d[k].purpose = 'Units for autoplot ("counts", "rate", "flux")'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_fitcomp_autoplot_enable'
d[k].class = 'spex_fit'
d[k].type = 'int'
d[k].units = ''
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Admin'
d[k].level = 'N/A'
d[k].purpose = 'If set, autoplot in FITCOMP widget after any change'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_fitcomp_plot_units'
d[k].class = 'spex_fit'
d[k].type = 'string'
d[k].units = ''
d[k].default = 'flux'
d[k].range = 'counts / rate / flux'
d[k].control_or_info = 'Admin'
d[k].level = 'N/A'
d[k].purpose = 'Units for plot in FITCOMP widget'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_fitcomp_plot_photons'
d[k].class = 'spex_fit'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Admin'
d[k].level = 'N/A'
d[k].purpose = 'If set, plot in photon space in FITCOMP widget'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_fitcomp_plot_bk'
d[k].class = 'spex_fit'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Admin'
d[k].level = 'N/A'
d[k].purpose = 'If set, overlay bk on plot in FITCOMP widget'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_fitcomp_plot_err'
d[k].class = 'spex_fit'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Admin'
d[k].level = 'N/A'
d[k].purpose = 'If set, show errors on plot in FITCOMP widget'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_fitcomp_plot_resid'
d[k].class = 'spex_fit'
d[k].type = 'int'
d[k].units = ''
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Admin'
d[k].level = 'N/A'
d[k].purpose = 'If set, plot residuals in autoplot in FITCOMP widget'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_fitcomp_plotobj_resid'
d[k].class = 'spex_fit'
d[k].type = 'objref'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Plotman object to use for residuals'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_fit_progbar'
d[k].class = 'spex_fit'
d[k].type = 'int'
d[k].units = ''
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Admin'
d[k].level = 'N/A'
d[k].purpose = 'If set, show progress bar during fit loop through intervals'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_allow_diff_energy'
d[k].class = 'spex_fit'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Admin'
d[k].level = 'N/A'
d[k].purpose = 'If set, allow saved (spex_summ) energy values to differ from data energies'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_summ_fit_function'
d[k].class = 'spex_fit'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Fit function used'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_summ_area'
d[k].class = 'spex_fit'
d[k].type = 'float'
d[k].units = 'cm^2'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Detector area for data in fit results'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_summ_energy'
d[k].class = 'spex_fit'
d[k].type = 'pointer'
d[k].units = 'keV'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Full array of energy edges, (2,nenergy)'
d[k].more_doc = 'spex_summ_energy contains the full array of (2,nenergy) edges that were in ' + $
	'the original data. <P>Use spex_summ_emask to determine which energy bins were used ' + $
	'for the fit in each time interval.'

k = k + 1
d[k].name = 'spex_summ_time_interval'
d[k].class = 'spex_fit'
d[k].type = 'pointer'
d[k].units = 'UT'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Array of time intervals fit, (2,ntime)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_summ_filter'
d[k].class = 'spex_fit'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Array of filter states for each time interval (ntime)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_summ_fit_done'
d[k].class = 'spex_fit'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Interval fit completed mask. For each interval 0=not fit, ' + $
    '1=fit. (ntime)'
d[k].more_doc = 'spex_summ_fit_done is a mask showing which fit time intervals have been fit. '+$
	'The array is dimensioned by the number of fit time intervals.  For each interval, ' + $
	'the mask has a value of 0 or 1 ' + $
	'indicating whether a fit was done for the  time interval (1 means yes).' + $
	'<P>Look at this parameter, as well as the spex_summ_fit_emask, to determine ' + $
	'which energy/time bins are valid for fit results: ' + $
	'<P>  qenergy = where (s.spex_summ_emask) - indices of energy bins used' + $
	'<P>  qtime = where (s.spex_summ_fit_done) - indices of time intervals fit'

k = k + 1
d[k].name = 'spex_summ_emask'
d[k].class = 'spex_fit'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Mask of energies used for each interval (nenergy,ntime)'
d[k].more_doc = 'spex_summ_emask is a mask showing which energies were used ' + $
	'during fitting for each time interval.  The array is dimensioned (nenergy,ntime) ' + $
	'where nenergy is the total number of energy bins in the original data, and ntime ' + $
	'is the number of fit time intervals.  For each bin, the mask has a value of 0 or 1 ' + $
	'indicating whether the energy bin was used (1 means used) for that time interval.' + $
	'<P>Look at this parameter, as well as the spex_summ_fit_done mask, to determine ' + $
	'which energy/time bins are valid: ' + $
	'<P>  qenergy = where (s.spex_summ_emask) - indices of energy bins used' + $
	'<P>  qtime = where (s.spex_summ_fit_done) - indices of time intervals fit'

k = k + 1
d[k].name = 'spex_summ_ct_rate'
d[k].class = 'spex_fit'
d[k].type = 'pointer'
d[k].units = 'counts/sec'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Count rate data that was fit for each interval, ' + $
    '(nenergy,ntime)'
d[k].more_doc = 'The count rate data that is used as input to the fit engine ' + $
	'during the fitting process.' + $
	'<P>If background was defined, this is the background-subtracted count rate.'

k = k + 1
d[k].name = 'spex_summ_ct_error'
d[k].class = 'spex_fit'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Error in count rate data for each interval, (nenergy,ntime)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_summ_bk_rate'
d[k].class = 'spex_fit'
d[k].type = 'pointer'
d[k].units = 'counts/sec'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Background rate corresponding to ct_rate, (nenergy,ntime)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_summ_bk_error'
d[k].class = 'spex_fit'
d[k].type = 'pointer'
d[k].units = 'counts/sec'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Error in background rate data for each interval, ' + $
    '(nenergy,ntime)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_summ_ph_model'
d[k].class = 'spex_fit'
d[k].type = 'pointer'
d[k].units = 'phot/cm^2/sec/keV'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Photon flux model for each interval, (nenergy,ntime)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_summ_conv'
d[k].class = 'spex_fit'
d[k].type = 'pointer'
d[k].units = 'counts/photon'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Conversion factors for each interval (nenergy,ntime)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_summ_resid'
d[k].class = 'spex_fit'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Residuals for each interval, (nenergy,ntime)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_summ_starting_params'
d[k].class = 'spex_fit'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Starting fit parameters for each interval, (nparams,ntime)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_summ_params'
d[k].class = 'spex_fit'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Final fit parameters for each interval, (nparams,ntime)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_summ_sigmas'
d[k].class = 'spex_fit'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Sigma in fit parameters for each interval, (nparams,ntime)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_summ_minima'
d[k].class = 'spex_fit'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Parameter minima for each interval, (nparams,ntime)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_summ_maxima'
d[k].class = 'spex_fit'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Parameter maxima for each interval, (nparams,ntime)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_summ_free_mask'
d[k].class = 'spex_fit'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Parameter free mask for each interval, (nparams,ntime)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_summ_func_spectrum'
d[k].class = 'spex_fit'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Fit function spectrum type (full, cont, or line) used for each component ' + $
	'in each interval, (ncomp,ntime)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_summ_func_model'
d[k].class = 'spex_fit'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Fit function model  (chianti or mewe) used for each component ' + $
	'in each interval, (ncomp,ntime)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_summ_chianti_version'
d[k].class = 'spex_fit'
d[k].type = 'string'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Chianti package version used to make tables'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_summ_abun_table'
d[k].class = 'spex_fit'
d[k].type = 'string'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Abundance table name'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_summ_maxiter'
d[k].class = 'spex_fit'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Maximum number of iterations allowed for each interval, ' + $
    '(ntime)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_summ_uncert'
d[k].class = 'spex_fit'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Systematic uncertainty for each interval, (ntime)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_summ_chisq'
d[k].class = 'spex_fit'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Chi-square for each interval, (ntime)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_summ_stop_msg'
d[k].class = 'spex_fit'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Reason for stopping fit for each interval, (ntime)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_summ_niter'
d[k].class = 'spex_fit'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Number of fit iterations done for each interval, (ntime)'
d[k].more_doc = ''

; Info parameters for spex_data
k = k + 1
d[k].name = 'spex_plotman_obj'
d[k].class = 'spex_data'
d[k].type = 'objref'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Plotman object reference'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_data_dir'
d[k].class = 'spex_data'
d[k].type = 'string'
d[k].units = ''
d[k].default = 'Current directory'
d[k].range = ''
d[k].control_or_info = 'Admin'
d[k].level = 'Standard'
d[k].purpose = 'Directory containing input files'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_panel_replace'
d[k].class = 'spex_data'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Admin'
d[k].level = 'Standard'
d[k].purpose = 'Not used yet'
d[k].more_doc = ''

; Info parameters for spex_data_strategy
k = k + 1
d[k].name = 'spex_respinfo'
d[k].class = 'spex_data_strategy'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Name of response file written with spectrum file, or array of ' + $
    'response values'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_file_time'
d[k].class = 'spex_data_strategy'
d[k].type = 'double(2)'
d[k].units = 'ANYTIM'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Start,end time of data in spectrum file'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_ut_edges'
d[k].class = 'spex_data_strategy'
d[k].type = 'pointer'
d[k].units = 'ANYTIM'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Time bins of data in spectrum file, (2,n)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_ct_edges'
d[k].class = 'spex_data_strategy'
d[k].type = 'pointer'
d[k].units = 'keV'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Energy bins of data in spectrum file, (2,n)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_area'
d[k].class = 'spex_data_strategy'
d[k].type = 'float'
d[k].units = 'cm^2'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Area of detector/ instrument from spectrum file'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_title'
d[k].class = 'spex_data_strategy'
d[k].type = 'string'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Title of data, e.g. "HESSI SPECTRUM"'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_file_units'
d[k].class = 'spex_data_strategy'
d[k].type = 'struct(1)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Structure of units info for data in spectrum file'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_detectors'
d[k].class = 'spex_data_strategy'
d[k].type = 'string'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'String of detectors included in spectrum file'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_interval_filter'
d[k].class = 'spex_data_strategy'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Filter state for each original data time interval'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_eband'
d[k].class = 'spex_data_strategy'
d[k].type = 'pointer'
d[k].units = 'keV'
d[k].default = 'If RHESSI, nine RHESSI ql bands.  Otherwise, divide energy range into 4 bands.
d[k].range = ''
d[k].control_or_info = 'Admin'
d[k].level = 'N/A'
d[k].purpose = 'Energy bands for viewing data'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_def_eband'
d[k].class = 'spex_data_strategy'
d[k].type = 'pointer'
d[k].units = 'keV'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Admin'
d[k].level = 'N/A'
d[k].purpose = 'Default energy bands for current type of data'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_tband'
d[k].class = 'spex_data_strategy'
d[k].type = 'pointer'
d[k].units = 'ANYTIM'
d[k].default = 'Divide full time range into 4 bands.'
d[k].range = ''
d[k].control_or_info = 'Admin'
d[k].level = 'N/A'
d[k].purpose = 'Time bands for viewing data'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_data_origunits'
d[k].class = 'spex_data_strategy'
d[k].type = 'struct(1)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Structure with original units for spectrum data'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_data_units'
d[k].class = 'spex_data_strategy'
d[k].type = 'struct(1)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Structure with units of last accumulation for spectrum data'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_deconvolved'
d[k].class = 'spex_data_strategy'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'If 1, original data is already photons.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_pseudo_livetime'
d[k].class = 'spex_data_strategy'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = "If 1, livetime isn't a real livetime."
d[k].more_doc = ''

k = k + 1
d[k].name = 'spex_data_pos'
d[k].class = 'spex_data_strategy'
d[k].type = 'float(2)'
d[k].units = 'arcsec'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Xyoffset of source in data file'
d[k].more_doc = ''

; Info parameters for spex_image
k = k + 1
d[k].name = 'spex_roi_outfile'
d[k].class = 'spex_image'
d[k].type = 'string'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Admin'
d[k].level = 'N/A'
d[k].purpose = 'Name of IDL save file to write ROI selection into'
d[k].more_doc = '>spex_roi_use'

k = k + 1
d[k].name = 'spex_roi_size'
d[k].class = 'spex_image'
d[k].type = 'int'
d[k].units = 'device pixels'
d[k].default = '50'
d[k].range = ''
d[k].control_or_info = 'Admin'
d[k].level = 'N/A'
d[k].purpose = 'Size of each image in Region Selection panel display.'
d[k].more_doc = '>spex_roi_use'

k = k + 1
d[k].name = 'spex_roi_color'
d[k].class = 'spex_image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '5'
d[k].range = '0 - 40'
d[k].control_or_info = 'Admin'
d[k].level = 'N/A'
d[k].purpose = 'Color table to use for Region Selection panel display.'
d[k].more_doc = '>spex_roi_use'

k = k + 1
d[k].name = 'spex_roi_expand'
d[k].class = 'spex_image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '3'
d[k].range = '1 - ?'
d[k].control_or_info = 'Admin'
d[k].level = 'N/A'
d[k].purpose = 'Expansion factor for expanded images in Region Selection tool.'
d[k].more_doc = '>spex_roi_use'

d = d[0:k]
end
