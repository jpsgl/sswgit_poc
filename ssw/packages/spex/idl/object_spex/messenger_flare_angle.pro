;+
;Name: Messenger_Flare_Angle
;
;Purpose: Computes the angle in degrees between the Sun-Messenger vector and a unit vector from the spherical center of the Sun
;	to a location on the Solar sphere.  The occultation angle is just about 90 degrees, so larger angles probably mean the flare was
;	not visible to Messenger. Flare height is an obvious variable in determining the actual occultation angle.
;Usage:
;	angle = messenger_flare_angle( flare_coord, flare_date[, mess_hg_str] [,mess_hg_file = mess_hg_file ] )
;
;		IDL> print, messenger_flare_angle(   [-200, 200.], '15-feb-2011', mess_hg_str)
;		      142.098
;		IDL> help, mess_hg_str,/st
;		** Structure <5e62e90>, 3 tags, length=24, data length=24, refs=1:
;		   DATE            DOUBLE      8.0749440e+008
;		   D_KM            FLOAT      1.51781e+008
;		   XYZ             FLOAT     Array[3]
;

;
;Inputs:
;	flare_coord - flare location on solar sphere in earth centered heliocentric coords. Values should be in apparent arcseconds
;		from Sun center as viewed from Earth center, Heliographic longitude and latitude (aka Systems HEQ, HEEQ, or Stonyhurst) in degrees may be
;		used if the HELIOGRAPHIC keyword is set
;		multiple values can be passed as a 2 x N array of float values
;		range isn't checked for validity. caller must pass valid coordinates in the chosen system
;	Flare_date - date-time of flare in anytim format, should be one date for each flare_coord pair passed
;	Mess_HG_str - Optional, messenger location struction in heliographic coordinates, see messenger_read_heliographic_position.pro
;		Structure returned if the argument is there, file path thru the optional MESS_HG_FILE
;		If this parameter is not present, the MESS_HG_FILE will be read on each call
;Optional Keywords:
;	MESS_HG_FILE - See Mess_HG_str and messenger_read_heliographic_position.pro
;	MESS_HELIO_LONLAT - Returns the Stoneyhurst longitude and latitude in degrees of the Sun-Messenger vector
;	USE_CURRENT - if set then look for MESS_HG_FILE int the working(current) directory
;	HELIOGRAPHIC - if set, then flare_coord is interpreted as heliographic longitude and latitude, default is 0
;
;History:
;	16-Dec-2013, richard.schwartz@nasa.gov
;	12-Feb-2015, richard.schwartz@nasa.gov
;-
function messenger_flare_angle,  flare_coord, flare_date, mess_hg_str, $
	mess_hg_file = mess_hg_file, $
	mess_helio_lonlat = mess_helio_lonlat, $
	use_current = use_current, $
	heliographic = heliographic, $
	error = error
error = 1
default, heliographic, 0
mess_hg_str = is_struct( mess_hg_str ) && have_tag( mess_hg_str, 'date') && have_tag( mess_hg_str, 'xyz') ? mess_hg_str : $
	messenger_read_heliographic_position( mess_hg_file, use_current=use_current )

date = anytim( flare_date )
if n_elements( flare_coord ) /2 ne n_elements( date ) then begin
	message, /info, 'There must be a date for each set of flare coords'
	return, 0
	endif

mmdate = minmax( date )
if mmdate[0] lt mess_hg_str[0].date or mmdate[1] gt last_item( mess_hg_str.date ) then begin
	message, /info, 'Date passed is outside of range of date obtained from Messenger trajectory file'
	return, 0
	endif

z = value_locate( mess_hg_str.date, date)
flare_coord_use = keyword_set( heliographic ) ? flare_coord : xy2lonlat( flare_coord, date, /quiet )
flare_xyz = sph2xyz( flare_coord_use )
angle = !radeg * acos( total( flare_xyz * mess_hg_str[z].xyz,1) )
mess_helio_lonlat = transpose( xyradec( transpose( mess_hg_str[z].xyz ) ) )
error = 0
return, angle
end
