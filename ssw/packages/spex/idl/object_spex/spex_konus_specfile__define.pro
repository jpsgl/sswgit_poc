;+
;
; NAME:
;   spex_konus_specfile__define
;
; PURPOSE:
;   Provides read_data method for the konus instrument.
;   NOTE: This procedure is based on spex_hessi_specfile__define.
;
;
; CATEGORY:
;       SPECTRAL FITTING, SPEX - konus
;
; CALLING SEQUENCE:
;
; CALLS:
;   read_konus_4_ospex
;
; INPUTS:
;
; OUTPUTS:
;
; INPUT KEYWORDS:
;
; OUTPUT KEYWORDS:
;
; PROCEDURE:
;
; Written 18-Jul-2016, Kim Tolbert, based on spex_messenger_specfile__define
;
; Modification History:
;
;------------------------------------------------------------------------------

pro spex_konus_specfile_test, o

  o =  obj_new( 'spex_konus_specfile' )
  ; Note - must have corresponding rmf and arf in directory to be able to read this file.  
  o->set,  spex_spec =  'C:\Analysis\working\konus\KW20121113_T20834_1.pha'
  data =  o->getdata()

end

;------------------------------------------------------------------------------

pro spex_konus_specfile::read_data, file_index=file_index, $
                       spectrum,  errors,  livetime,  $
                       spex_respinfo, spex_file_time,  spex_ut_edges,  spex_ct_edges,  $
                       spex_area, spex_title, spex_detectors,  $
                       spex_interval_filter, spex_units, spex_data_name, $
                       spex_deconvolved, spex_pseudo_livetime, spex_data_pos, $
                       err_code=err_code, _extra=_extra

  file =  self -> get( /spex_specfile )
  checkvar, file_index, 0
  file = file[file_index]
  
  konus_data, FILES=file, $
    DATA_STR=ds, $
    ERR_CODE=err_code, $
    ERR_MSG=err_msg
                                                                                                
  if err_msg[0] ne '' then begin
      ;if not spex_get_nointeractive() then xmessage,err_msg else print,err_msg
      message, err_msg, /cont
      if err_code then return
  endif
    
  spectrum = ds.rcounts
  errors = ds.ercounts
  livetime = ds.ltime
  spex_file_time = utime([ds.start_time, ds.end_time])
  spex_ut_edges = ds.ut_edges
  
  spex_respinfo = ds.respfile
  spex_ct_edges = ds.ct_edges
  spex_area = ds.area[0]
  spex_title = ds.title
  spex_detectors = ds.detused
  spex_units = ds.units
  spex_interval_filter = ds.atten_states
  spex_data_name = ds.data_name
  spex_deconvolved = ds.deconvolved
  spex_pseudo_livetime = ds.pseudo_livetime
  spex_data_pos = ds.xyoffset

end

;------------------------------------------------------------------------------
pro spex_konus_specfile__define

  self = {spex_konus_specfile, $
          INHERITS spex_data_strategy }

END
