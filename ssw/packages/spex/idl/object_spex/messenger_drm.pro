;+
; PROJECT:
;	MESSENGER
; NAME:
;	MESSENGER_DRM
;
; PURPOSE:
;	This function constructs and returns the DRM for MESSENGER.
;
; CATEGORY:
;	Spectral Analysis
;
; CALLING SEQUENCE:
;	messenger_drm, efficiency_file=efficiency_file, $
;
;	    fwhm=fwhm, edges_file=edges_file, path=path, $
;	    ein2 = ein2, $
;	    area = area, $ ;nominal geometric area
;	    error = error
;
;
; INPUTS:
;
;       Efficiency_file - defaults to 'SAX_modelled_response2.csv'

;		THESE COMMENTS REFER TO THE EFFICIENCY FILE
;		The MESSENGER solar monitor is a Si-PIN made by Metorex,
;		which is the same company that supplied the PIN for SMART-1.
;		The detector is 500 microns thick with a 76 micron Be window and a 200 nanometer SiO2 dead layer.
;		The detector area is 0.0314 mm2.
;		I have attached my model of the response function.
;		The column labeled "Si-PIN" is the response function based on the Si and Be attenuation coefficients.
;		The column labeled "mcnpx" is the response function calculated by the MCNPX monte carlo code.
;		The results are pretty similar.


;		The measured efficiency at 5.9 keV (Fe-55) is 0.982 �0.0007 which is a little higher than the modeled value of ~0.96.
;		On January 22, 2006 the MESSENGER spacecraft was about 109*106 km from the Sun.;
;
;		Edges_file - defaults to gain   =.0375294
;		offset = 0.863, for 231 channels

;		fwhm - constant, uses 0.590 keV
;		offset - empirical channel offset to fix apparent gain
;		SAX_MODELLED  - uses photoefficiency file provide by
;			Richard Starr, otherwise uses analytic model based on detector thickness,
;			deadlayer thickness, and Be window thickness.  Fluorescent emission
;			is not considered because that effect is at most 5%
;		PLOT_TEST - plot photoefficiency for default and SAX_MODELLED cases

;
; OUTPUTS:
;		Returns DRM as 231 x 231 array
;			for a photon spectrum in photons/cm2/sec input
;			(i.e. integrated over photon energy bins), the output
;			is in units of cnts/cm2/s/keV
;			count_flux = drm # (photon_spectrum * DE)
;			where photon_spectrum is in units of photons/cm2/sec/keV
;			and DE is the input energy bin width in keV
;       EIN2 - edges in 2 x 231 array
;		AREA - nominal area, 0.000314 cm^2
;		MXEFF  - detector photoefficiency on mid-points of ein2
;
;
;
; RESTRICTIONS:
;	none
;
; PROCEDURE:
;	Reads efficiency, transmission, and edge files provided by
;	Messenger team.  Energy calibration is fit and uses, efficiency and transmission
;	are read and smoothed to energy edge midpoints.
;
; MODIFICATION HISTORY:
;	12-jun-2007, richard.schwartz@gsfc.nasa.gov
; 29-Apr-2008, Kim.  Don't divide by area
; 29-Apr-2008. Richard. Use my nbs tables instead of Starr-provided model
; 7-May-2008, richard.schwartz@nasa.gov, allow both response models,
;  	analytic model is the default to avoid interpolation problems
; 25-Sep-2013, Kim. Use SSWDB_MESSENGER env. var. to find SAX modelled response csv file
;
;-


function messenger_drm, efficiency_file=efficiency_file, $
    fwhm=fwhm, $
    ct_edges=ct_edges, $ ;not used
    path=path, $ ;notUsed
    ein2 = ein2, $
    area = area, $ ;nominal geometric area
    offset = offset, $
    sax_modelled=sax_modelled, $
    mxeff = mxeff, $
    plot_test = plot_test, $
    error = error

default, plot_test, 0
if plot_test then begin
;Code below can be used to plot the difference between the two model photoefficiencies
	drm0=messenger_drm(mxeff=mxeff0)
	drm1=messenger_drm(/sax, mxeff=mxeff1, ein=ein)

	ems=get_edges(ein,/mean)


	w=where(ems gt 1.01)
	pmulti_sav = !p.multi
	!p.multi=[0,3,1]

	plot,/ylog, ems[w], mxeff0[w], xrang=[0,10], psy=1,ytitl='SCHWARTZ Analytic Model',chars=2
	plot,/ylog, ems[w], mxeff1[w], xrang=[0,10], psy=1,ytitl='SAX MC Model',chars=2
	plot,ems[w],mxeff0[w]/ mxeff1[w],yran=[.001,10],/ylog,/xlog, xrang=[1,10],/xstyle, psy=1,ytitl='SCHWARTZ/SAX',chars=2
	oplot,ems[w],(mxeff0[w]/ mxeff1[w])*mxeff1[w], psy=4
	!p.multi = pmulti_sav
	endif

error = 1
drm = 0
default, sax_modelled, 0

default, fwhm, .590

default, area, .000314 ; cm^2 - area of aperture, requires "perfect" pointing

coef = [[0.863173],[0.0375294]] ;nominal

default, offset, 0.00 ;empirical correction

edges= poly( findgen(232)-offset-0.5, coef)
ein2 = get_edges(edges,/edges_2)
dein = coef[1]
ems  = avg(ein2,0)

oeres = energy_res(ein=ein2, eout=ein2, e_vs=ems, fwhm=ems*0+fwhm, sig_lim=8)
eres = oeres->getdata()
obj_destroy, oeres


if sax_modelled then begin
  resp_dir = getenv('SSWDB_MESSENGER')
  if resp_dir eq '' then resp_dir = concat_dir ('$SSW_OSPEX', 'messengerresp')
	resp_file = file_search (concat_dir(resp_dir,'SAX_modelled_response2.csv'))
	default, efficiency_file, resp_file
	sresp = transpose( rd_tfile( efficiency_file,8,1,header=h,delim=',',/convert))

	eresp = sresp[*,0]
	cresp = sresp[*,5]
	mresp = sresp[*,7]
	mxeff = interpol(mresp, eresp, ems)
endif else begin
;using my nbs tables instead of Starr provided mode
	ems1  = ems > 1.01
	attn  = exp(-( xsec(ems1, 14, 'pe')*200*1e-7 + 76e-4*xsec(ems1, 4,'pe')))
	prob  = 1 - exp(-(xsec(ems1,14,'pe')*.05))

	mxeff = attn * prob
	endelse

sxeffmtrx= rebin(transpose(mxeff),231,231)

drm = eres * sxeffmtrx / rebin( dein+fltarr(231), 231, 231)
error = 0
return, drm
end
