;+
; NAME:
;	F_TEMPLATE_DEFAULTS
;
; PURPOSE: Function to return default values for
;   parameters, minimum and maximum range, free parameter mask,
;   table to use, and table options when
;   fitting to f_template function.
;
; CALLING SEQUENCE: defaults = f_template_defaults()
;
; INPUTS:
;	None
; OUTPUTS:
;	Structure containing default values
;
; MODIFICATION HISTORY:
; Kim Tolbert, 12-Oct-2007
; 15-Aug-2012, Kim.  Added 8 new shortcut names and changed default to brd_nuc
;
;-
;------------------------------------------------------------------------------

FUNCTION F_TEMPLATE_DEFAULTS

defaults = { $
  fit_comp_params:           [1.e0], $
  fit_comp_minima:           [1e-20], $
  fit_comp_maxima:           [1e20], $
  fit_comp_free_mask:        [1b], $

  fit_comp_spectrum:         'brd_nuc', $

  fc_spectrum_options:	['brd_nuc', 'nar_nuc', 'brd_nar_nuc', $
    'pion_s30', 'pion_s35', 'pion_s40', 'pion_s45', 'pion_s50', $
    'nuc1', 'nuc2', 'vern', 'alpha', 'fline', 'bline', 'nline', 'user'] $

}

RETURN, defaults

END
