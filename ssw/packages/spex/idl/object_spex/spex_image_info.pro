;+
; Name: spex_image_info
;
; Purpose:  Initialize variables in info structure for spex_image
; (some of these info params are really used as admin params)
;
; Kim Tolbert Aug 2005
; Modifications:
;
;-

function spex_image_info

var = {spex_image_info}
data_str = spex_data_strategy_info()
struct_assign, data_str, var
var.spex_roi_outfile = ''
var.spex_roi_size = 50
var.spex_roi_color = 5
var.spex_roi_expand = 3

return, var
end