;------------------------------------------------------------------------------
function spex_fitalg::init, $
	source=source, $
	_extra=_extra

strategy = [ 'SPEX_FITALG_MCURVEFIT', 'SPEX_FITALG_POLY_FIT' ]

if not obj_valid( source ) then source = obj_new( 'spex_fitrange', _extra=_extra )

ret = self->spex_gen_strategy_holder::init( strategy, $
						source=source, $
						_extra=_extra )

;self.get_all_strategies = 1
self.do_all_classes = 1

dummy = self->getstrategy()  ; force it to create the default strategy object
if keyword_set(_extra) then self -> set, _extra=_extra

return, ret

end

;------------------------------------------------------------------------------
pro spex_fitalg__define

struct = { spex_fitalg, $
	   inherits spex_gen_strategy_holder $
	   }

end
