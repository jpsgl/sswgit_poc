;+
; Name: fit_model_components
;
; Purpose: Return function component name or description for all available components, or for a
;	specified component.  Reads ascii list of function component names in fit_model_components.txt.
;
; Category: FITTING, SPEX
;
; Input:
;	comp - Unique string identifying component to return name or description for ( e.g. 'bpow').
;		If not passed, then all are returned.
; one_line - If set, return one line per component with name and description
;	full - if set, return name, description
; struct - if set, return structure containing name, short and long description, number of parameters,
;   and parameter descriptions.  Only valid when a single component is specified.
;
; Output (optional):
;	desc - variable to return description in (e.g. Broken Power Law)
;
; Written: Kim Tolbert, April 2, 2003
; Modifications:
; 30-Sep-2005, Kim.  Use OSPEX_MODELS_DIR env. var. to find fit_model_components.txt file
;   (instead of SSW_OSPEX) so users can add models and point to their own
;   fit_model_components.txt file.
; 16-Jan-2009, Kim.  Added struct keyword
; 1-Aug-2012, Kim. Use 0 in strtrim(this... to preserve spacing at start of lines for readability,but
;   use trim when searching for a[ lines to remove leading blanks. (previously lost all spaces, hard to read)
;
;------------------------------------------------------------------------------------------

function fit_model_components, comp, one_line=one_line, full=full, struct=struct

desc = ''
filename = 'fit_model_components.txt'
dirfile = concat_dir('OSPEX_MODELS_DIR', filename)
file  = findfile(dirfile, count=count)
if count eq 0 then begin
	message,'Can not find file ' + dirfile + '.  Aborting.  Is OSPEX_MODELS_DIR defined correctly?',/cont
	retall
endif

list = rd_ascii(file, error=error)
if error then return, ''

list = strnocomment(list, /remove)

full_ind = where (stregex(list, '^ ', /bool), complement=short_ind)

;short = str2cols(list[short_ind], '-', /unaligned)
;name = trim(short[0,*])
name = trim(ssw_strsplit(list[short_ind], '-', tail=short_desc))


ind = exist(comp) ? (where(name eq strlowcase(comp)))[0] : indgen(n_elements(short_ind))

if ind[0] eq -1 then return, ''
nout = n_elements(ind)
if keyword_set(struct) and nout gt 1 then begin
  message,'Can not request structure for more than one component. Aborting.',/cont
  return, ''
endif

if keyword_set(full) or keyword_set(struct) then begin
	short_ind = [short_ind, n_elements(list)]
	i1 = short_ind[ind] + 1
	i2 = short_ind[ind+1] - 1
	for i=0,nout-1 do begin
	   this = strtrim(list[i1[i]:i2[i]], 0)  ; remove trailing blanks
	   p_index = where (strpos(trim(this), 'a[') eq 0)
	   long_desc = this[0:p_index[0]-1]
	   aa= ssw_strsplit(this[p_index],'-',tail=param_desc)
     out =append_arr(out, [this, ''])
     out_str = {name: name[ind[i]], $
           short_desc: short_desc[ind[i]], $
           long_desc: long_desc, $
           nparams: n_elements(param_desc), $
           param_desc: param_desc }

  endfor
	return, keyword_set(full) ? out : out_str

endif

if keyword_set(one_line) then return, list[short_ind[ind]]
return, name[ind]

end
