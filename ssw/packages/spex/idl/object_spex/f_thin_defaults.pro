;+
; NAME:
;	F_THIN_DEFAULTS
;
; PURPOSE: Function to return default values for
;   parameters, minimum and maximum range, and free parameter mask when
;   fitting to f_thin function.
;
; CALLING SEQUENCE: defaults = f_thin_defaults()
;
; INPUTS:
;	None
; OUTPUTS:
;	Structure containing default values
;
; MODIFICATION HISTORY:
; Kim Tolbert, February 2004
; 13-Feb-2009, Kim.  Changed lower limit for both power-law indices to .1 from 1.1
;
;-
;------------------------------------------------------------------------------

function f_thin_defaults

defaults = { $
  fit_comp_params:      	 [100.,  2., 100., 2., 10., 3.2e4], $
  fit_comp_minima:           [1e-10,  .1, 1., .1, 1., 100.], $
  fit_comp_maxima:           [1e15,  20,  1e5, 20,  1e3, 1e7], $
  fit_comp_free_mask:        [1,1,0,1,0,0] $
  }

return, defaults

end
