;+
; Name: spex_image_info__define
;
; Purpose:  Define info structure for spex_image
;
; Kim Tolbert Aug 2005
; Modifications:
;
;-

pro spex_image_info__define

struct = {spex_image_info, $
          spex_roi_outfile: '', $
          spex_roi_size: 0, $
          spex_roi_color: 0, $
          spex_roi_expand: 0, $
          inherits spex_data_strategy_info }

end