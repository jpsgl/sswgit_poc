; Modifications:
; 19-Aug-2012, Kim.  netscape_control doesn't work for unix or mac.  Try this. If it works
;  make new routine called url_open or something.

pro ospex_help, file

found = loc_file (file, path='$OSPEX_DOC')

if found eq '' then begin
	a = dialog_message(['Help file ' + file + ' not found.  Current value of OSPEX_DOC is ' + get_logenv('OSPEX_DOC'), $
			'Please notify kim.tolbert@gsfc.nasa.gov'], $
			title='Help file not found')
	return
endif

;netscape_control, found[0]
case os_family() of
  'Windows': command = 'start '
  'unix': command = 'xdg-open '
  'Mac': command = 'open '
  else:  message,/cont,'Unrecognized OS: ' + os_family() + ' do not know how to open URL.'
endcase

spawn, 'start ' + found[0]
return

end