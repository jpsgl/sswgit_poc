; Modifications:
;	30-Jul-2006, Kim.  Added spex_ut_offset
; 28-Nov-2009, Kim.  Added spex_data_sel to select data type from within a file
; 16-Dec-2009, Kim.  Added spex_ignore_filters
; 19-Feb-2010, Kim.  Added spex_accum_time
;-

pro spex_data_strategy_control__define

struct = {spex_data_strategy_control, $
          spex_specfile: ptr_new(), $	; Input spectrum file name
          spex_data_sel: '', $        ; instrument or detector selection if file has more than one
          spex_ut_offset: 0., $ 			; Time offset for data in seconds
          spex_ignore_filters: 0, $   ; If set, ignore filters
          spex_accum_time: [0.d,0.d] }  ; time interval to accumulate for spex analysis. [0.,0.] means full file.  
end
