; function called by read_messenger_pds to return integer data from byte array read from dat file.
; First do ieee_to_host conversion, then extract as 2-byte or 4-byte integers based on type.
; bytearr - byte array read from file
; sb,eb - start, end byte to use
; type - IDL data type code
; nb - number of bytes in each value
function messenger_get_intdata, bytearr, sb,eb, type, nb
nrec = n_elements(bytearr[0,*])
a = bytearr[sb:eb,*]
ieee_to_host, a, idltype=type
return, reform (fix(a, 0, (eb-sb+1)/nb, nrec, type=type) )
end

; function called by read_messenger_pds to return value for a label name from .lbl file
; lbl_file - lbl file name
; lbl - string array read from lbl file
; name - name of value to get (first columnn of lbl)
function messenger_get_lbltag, lbl_file, lbl, name
q = where (lbl[0,*] eq name, c)
if c gt 0 then return,lbl[2,q[0]] else begin
  message, 'No ' + name + ' in file ' + lbl_file
  return,''
endelse
end

;+
;
; PROJECT:
; RHESSI
;
; NAME:
; read_messenger_pds
;
; PURPOSE:
; Read messenger solar data and times from a pds format .dat (and associated .lbl file), compute drm, and
; return data and associated values.  By default, data is filtered for:
;   night time - use ratio of low to high energy channels
;   too hot - temperature allowed is 19. degrees before '5-march-2012', 2. degrees after (celsius)
;   bias voltage too low - should be > 110 volts
;   gaps in data of more than 1 hour - remove points immediately after gap since unreliable
;   data is within a period of known bad times.  See bad_times table in code.
; For points that fail any of the filters, set the rcounts, ercounts, ltime returned in output
; structure to 0.  Note that the other data fields returned are not modified by filters.
;   
; This procedure is based on read_hessi_4_ospex.
;
; CATEGORY:
; SPEX
;
; CALLING SEQUENCE:
; read_messenger_pds, FILES=files, data_str=data_str[, ERR_CODE=err_code,
;                   ERR_MSG=err_msg, _REF_EXTRA=_ref_extra $
;                   verbose=verbose, $
;                   no_filter=no_filter, $
;                   ratio_threshold=ratio_threshold, $
;                   expand_night=expand_night, $
;                   expand_temp_volt=expand_temp_volt, $
;                   expand_gap=expand_gap, $
;                   show_plot=show_plot
;
; INPUT KEYWORDS:
;  FILES - Name of messenger .dat file to read.  Corresponding .lbl file must be in the same directory.
;  verbose - if set, print information about filtering. Default is 0.
;  no_filter - if set, don't filter data. Default is 0.
;  ratio_threshold - if ratio of lower to higher energies is great than this, assume night. Default is 100.
;  expand_night - expand night intervals by this number of points. Default is 5.
;  expand_temp_volt - expand bad temperature or voltage intervals by this number of points. Default is 2.
;  expand_gap - expand gap intervals by this number of points. Default is [0,2].
;  show_plot - if set, shows diagnostic plot showing filtering that was done.
;
; OUTPUT KEYWORDS: 
;  data_str - structure containing the information read from the file (see
;   structure definition at end of code for fields in structure)
;  err_code - 0 / 1 means no error / error
;  err_msg - string containing error message. Blank if none.
;  
;
;
;
; EXAMPLE:
; Can be called standalone:
;   read_messenger_pds,files='xrs2007152.dat', data_str=data_str
;   read_messenger_pds,files='xrs2007152.dat', data_str=data_str, /verbose, /show_plot
;   read_messenger_pds,files='xrs2007152.dat', data_str=data_str, /no_filter, /show_plot
; or, called from within OSPEX to load MESSENGER data
;
; Written: 12-jun-2007.  richard.schwartz@gsfc.nasa.gov
;
; Modification History:
; 29-Apr-2008, Kim.  Rewrote to read xrs...dat files instead of excel files.
; 21-Oct-2010, Kim. clock_start now has "" in string, so remove them before using.
; 25-Sep-2013, Kim. Remove check/set of SSWDB_MESSENGER env. var.  Use env. var. in routines that need it.
; 18-Dec-2013, Kim. Remove 1/ or 2/ from SPACECRAFT_CLOCK_START_COUNT value. (These were added in 2012? to
;   indicate 1/ = before clock rollover, 2/ = after clock rollover (28-bit clock)
; 07-Feb-2014, Kim.
;   Find data that's in solar eclipse, detectors are too hot, high voltage is 0, or just after gap.
;   Zero out the data and livetime for those elements.  This is especially important once MESSENGER went into orbit
;   around Mercury on  4-mar-2011.
;     Use ratio of lo energy to hi energy rates to find eclipses.
;     Use solar_detector_temp, pin_tec_mode, and pin_tec_enable to get detector temperature.  Before CME of 5-mar-2012
;       allowed to go up to 19C, after that date, max is 2C.
;     There are periods of several-hour gaps (especially in nov 2011), and the first few points after the gap are
;       screwy (usually too low).
;     Added ratio_threshold and expand_night keywords.
;   Also added show_plot keyword - when enabled, shows original data, temperature, voltage, and then final data after
;     cleaning everything up.
;   Also, don't need to use SPACECRAFT_CLOCK_START_COUNT (and therefore don't need to worry about rollovers), just use
;     time array - first element of time array + start time from header.  The only rollover that's happened was on 8-jan-2013,
;     near end of day, and data stops before rollover.
; 12-Feb-2014, Kim. Add setting err_code when data in file is all zeroes.
; 17-feb-2014, richard.schwartz@nasa.gov, added support for drm offset and fwhm param input and reporting
; 11-mar-2014, Kim. Add table of bad_times and zero out all data in those intervals
; 23-Apr-2014, Kim. Add gpc1_veto_rate, gpc2_veto_rate, gpc3_veto_rate to returned structure (these should indicate
;   proton events when > ~100 c/s, according to Richard Starr)
; 23-Jul-2014, Kim.  Added keywords no_filter, expand_temp_volt, expand_gap, and added
;   labels and more info to plots. Also if verbose is set, gives info about how much was
;   filtered out and why.
; 
;
;-
;------------------------------------------------------------------------------
pro read_messenger_pds, FILES=files, data_str=data_str, ERR_CODE=err_code,$
                   ERR_MSG=err_msg, _REF_EXTRA=_ref_extra, $
                   verbose=verbose, $
                   no_filter=no_filter, $
                   ratio_threshold=ratio_threshold, $
                   expand_night=expand_night, $
                   expand_temp_volt=expand_temp_volt, $
                   expand_gap=expand_gap, $
                   show_plot=show_plot


checkvar, verbose, 0
checkvar, no_filter, 0
checkvar, ratio_threshold, 100. ; if ratio of lower to higher energies is great than this, assume night
checkvar, expand_night, 5       ; expand night intervals by this number of points
checkvar, expand_temp_volt, 2   ; expand bad temperature or voltage intervals by this number of points
checkvar, expand_gap, [0,2]     ; expand gap intervals by this number of points

checkvar, show_plot, 0     ; show plot of before and after filtering for night and too hot

good_bias_voltage = 110

orbit_date = anytim('4-mar-2011')  ; first day of Mercury Orbit
temp_change_date = anytim('5-march-2012')
temp_cutoff = [19., 2.]         ; valid maximum temperature for before/after temp_change_date

; These are times that weren't caught by any of our eclipse, etc filters, but are clearly bad data
bad_times = [[' 3-Sep-2012 07:51:41.327', ' 3-Sep-2012 08:01:01.327'], $
             ['20-Sep-2012 07:54:57.355', '20-Sep-2012 08:14:57.355'], $
             ['20-Sep-2012 15:56:39.355', '20-Sep-2012 16:14:39.355'], $
             ['21-Sep-2012 07:58:37.072', '21-Sep-2012 08:11:17.072'], $
             ['21-Sep-2012 15:47:57.072', '21-Sep-2012 16:17:57.072']]
bad_times = anytim(bad_times)
             
fcount = 0
If is_string( files ) then file = file_search(/fully_qualify_path, files, count=fcount)

if fcount eq 0 then begin
    err_code=1
    err_msg = 'No MESSENGER spectrum file selected.'
    return
endif

Catch, err_code
IF err_code ne 0 then begin
	catch, /cancel
	print,!error_state.msg
	err_msg = 'Error reading MESSENGER file ' + file[0]
	message,/continue, err_msg
	return
endif

; get header information from associated .lbl file
filename = file[0]
break_file, filename, disk,dir,f,ext
lbl_file = disk+dir+f+'.lbl'
lbl = rd_tfile(lbl_file,/auto)
nrec = messenger_get_lbltag (lbl_file, lbl, 'FILE_RECORDS')
start_time = anytim(messenger_get_lbltag (lbl_file, lbl, 'START_TIME'))
; don't need spacecraft_clock_start_count.  first time in file is always this value
;clock_start = messenger_get_lbltag (lbl_file, lbl, 'SPACECRAFT_CLOCK_START_COUNT')
;; remove quotes and 1/ or 2/ if present from clock start
;if is_string(clock_start) then begin
;  remchar,clock_start,'"'
;;  add_rollover = strmid(clock_start, 0, 2) eq '2/'  ; if 2/xxx then clock rolled over, so add 2^28
;  if strmid(clock_start, 0, 2) eq '1/' or strmid(clock_start, 0, 2) eq '2/' then clock_start = strmid(clock_start,2,99)
;;  if add_rollover then clock_start = clock_start + 2.^28
;endif


; get corrections for times and area to Earth view
messenger_dist_correct, intime=start_time, time_offset=time_offset, area_factor=area_factor
start_time = start_time + time_offset

; read file into byte array
openr,lu,/get,filename[0]
bdata=bytarr(2258,nrec) & readu, lu, bdata & free_lun,lu

;SOLAR_MON_SPECTRUM_23_253 - xrs data
data = messenger_get_intdata (bdata, 332, 332+461, 12, 2) * 1.

; SOLAR_MONITOR_SPECT_SHIFT - if data in a time interval exceeds 2^16, the data is divided
; by this number to discard the least significant bits and shift the most significant 16 bits of
; data to the right.  Multiply data by this number to restore. This may need to be corrected.
bshift = messenger_get_intdata (bdata, 330, 331, 12, 2)
q = where (bshift gt 1,c)
nen = n_elements(data[*,0])
if c gt 0 then data = data * transpose(reproduce(bshift,nen))

if max(data) eq 0. then begin
  err_msg = 'No non-zero data in MESSENGER file ' + file[0]
  message,/continue, err_msg
  err_code = 1
  return
endif

;MET, start time
t = messenger_get_intdata (bdata, 0, 3, 13, 4)
times = t - t[0] + start_time

; actual_integration time
int_time = messenger_get_intdata (bdata, 94, 97, 13, 4)

ut_edges = transpose( [[times], [times+int_time]] )

; SOLAR_STABILITY
stab = messenger_get_intdata (bdata, 310, 310+19, 12, 2)
nt = n_elements(times)
t1 = rebin(times, nt, 10) + (int_time/10. # findgen(10))
stab_times = get_edges( [ (transpose(t1))[*], max(ut_edges) ], /edges_2)
solar_stab = {rate: stab[*], times: stab_times}

; SOLAR_MONITOR_RATE
mon_rate = messenger_get_intdata (bdata, 154, 157, 13, 4)

; SOLAR_MONITOR_PILEUP_RATE
mon_pileup_rate = messenger_get_intdata (bdata, 158, 161, 13, 4)

; SOLAR_MONITOR_VALID_RATE
mon_valid_rate = messenger_get_intdata (bdata, 162, 165, 13, 4)

; SOLAR_MONITOR_ANALYZED_RATE
mon_an_rate = messenger_get_intdata (bdata, 166, 169, 13, 4)

; GPC1_MG_VETO_RATE
gpc1_veto_rate = messenger_get_intdata (bdata, 183, 186, 13, 4)

; GPC2_AL_VETO_RATE
gpc2_veto_rate = messenger_get_intdata (bdata, 228, 231, 13, 4)

; GPC3_UN_VETO_RATE
gpc3_veto_rate = messenger_get_intdata (bdata, 273, 276, 13, 4)

;GPC1_MG_VALID_RATE
gpc1_valid_rate = messenger_get_intdata (bdata, 195, 198, 13, 4)

;GPC1_MG_ANALYZED_EVENT_RATE
gpc1_an_rate = messenger_get_intdata (bdata, 199, 202, 13, 4)

bias_voltage = .507 * messenger_get_intdata(bdata, 88, 89, 12, 2)
solar_detector_temp = messenger_get_intdata(bdata, 75, 75, 1, 1)
solar_detector_temp = float(solar_detector_temp)
pin_tec_mode = messenger_get_intdata(bdata, 116, 116, 1, 1)
pin_tec_enable = messenger_get_intdata(bdata, 115, 115, 1, 1)

; where pin_tec_mode is 0 use lo algorithm, where pin_tec_mode is 1 use hi algorithm
; if pin_tec_enable is 0, always use lo algorithm
pin_tec_mode = pin_tec_mode * pin_tec_enable
qlo = where(pin_tec_mode eq 0, klo, complement=qhi, ncomplement=khi)
temp = fltarr(n_elements(pin_tec_mode))

if khi gt 0 then temp[qhi] = 4.57782E-09*SOLAR_DETECTOR_TEMP[qhi]^5 - $
  3.16578E-06*SOLAR_DETECTOR_TEMP[qhi]^4 + $
  8.58411E-04*SOLAR_DETECTOR_TEMP[qhi]^3 - $
  1.11961E-01*SOLAR_DETECTOR_TEMP[qhi]^2 + $
  7.16858E+00*SOLAR_DETECTOR_TEMP[qhi] - 1.23365E+2

if klo gt 0 then temp[qlo] = 2.06686*[alog(SOLAR_DETECTOR_TEMP[qlo]+1)]^2 - $
  38.94592*alog(SOLAR_DETECTOR_TEMP[qlo]+1) +107.39573

solar_detector_temp_raw = solar_detector_temp
solar_detector_temp = temp

data_str = -1
err_code = 0
err_msg = ''
atten_states = -1

; get drm, energy edges and area
;drm = messenger_drm(ein2=ct_edges, area=area)
;richard schwartz, 17-feb-2014, use new drm computation with possible offset in keV
defsysv, '!messenger_drm_params', value, exist = have_drm_params
if have_drm_params then begin
	value = !messenger_drm_params
	if have_tag( value, 'offset') then offset = value.offset
	if have_tag( value, 'fwhm' ) then fwhm = value.fwhm
endif

drm = messenger_use_resp_calc( edges_in = ph_edges, edges_out = ct_edges, area = area, fwhm=fwhm, offset=offset, zout = zout )
area = area * area_factor ; correct to Earth view

;; don't keep any energies below 1. keV - chianti can't handle
;qct = where (ct_edges[0,*] gt 1.)
;qph = where (ph_edges[0,*] gt 1.)
;e1 = qct[0]
;e2 = qph[0]
;drm = drm[e1:*,e2:*]
;ct_edges = ct_edges[*,e1:*]
;ph_edges = ph_edges[*, e2:*]
data = data[zout,*]

nt = n_elements(data[0,*])
nen = n_elements(data[*,0])

ut_width = get_edges(ut_edges, /width)

; livetime estimate is width of time bin minus 16 microsec for each count in all energies
ltimes = transpose( rebin(ut_width - .000016 * total(data,1), nt, nen))

; Use ratio of rate in a low energy band to a high energy band to determine when we're in eclipse
rate_lo = f_div(total(data[2:15,*],1),  ltimes[0,*])   ; low e band (~1.1 - 1.6 keV)
rate_hi = f_div(total(data[16:200,*],1), ltimes[0,*])   ; high e band (~1.6 - 8.6 keV)

knight = 0
check_ratio = 0
kbad = 0
khot = 0
kvolt = 0
temp_max = ut_edges[0] lt anytim(temp_change_date) ? temp_cutoff[0] : temp_cutoff[1]
kgap = 0

if ~no_filter then begin
  
  ; Only do ratio test after MESSENGER started orbiting Mercury
  if ut_edges[0] gt orbit_date then begin
    check_ratio = 1
    ; check ratio of low to high bands to find night intervals. qnight will be the time array elements during night
    ratio = f_div(rate_lo, rate_hi)
    qnight = where(ratio gt ratio_threshold or ratio eq 0., knight)
    ; for night intervals, expand by expand_night number of points on either side
    if knight gt 0 then begin
      qnight = expand_elems(qnight, nexpand=expand_night, min=0, max=n_elements(ratio)-1)
      if verbose then print, 'Number of points in night = ' + trim(knight) + '. Removing ' + trim(n_elements(qnight)) + ' points'
    endif
  endif

  ; Check if any intervals fall in the bad_times intervals

  if min(ut_edges) lt max(bad_times) && max(ut_edges) gt min(bad_times) then begin 
    for i=0,n_elements(bad_times[0,*])-1 do begin
      qbadi = where(ut_edges[0,*] lt (bad_times[1,i]-.001) and ut_edges[1,*] gt (bad_times[0,i]+.001), kbadi)
      if kbadi gt 0 then qbad = append_arr(qbad, qbadi)
      kbad = kbad + kbadi
    endfor
    if verbose and kbad gt 0 then print, 'Removing ' + trim(kbad) + ' points because in bad time intervals.'
  endif

  ; Check solar_detector_temp for within allowable range given by temp_cutoff.  qhot will be the time array elements when
  ; too hot
  qhot = where(solar_detector_temp gt temp_max, khot)
  ; for bad intervals, expand by 2 data points on either side
  if khot gt 0 then begin
    qhot = expand_elems(qhot, nexpand=expand_temp_volt, min=0, max=n_elements(solar_detector_temp)-1)  
    if verbose then begin            
      print, 'Number of points too hot = ' + trim(khot) + '. Removing ' + trim(n_elements(qhot)) + ' points.'
    endif
  endif

  qvolt = where(bias_voltage lt good_bias_voltage, kvolt)
  ; for bad intervals, expand by 2 data points on either side
  if kvolt gt 0 then begin
    qvolt = expand_elems(qvolt, nexpand=expand_temp_volt, min=0, max=n_elements(bias_voltage)-1)
    if verbose then begin
      print, 'Number of points with voltage bad = ' + trim(kvolt) + '. Removing ' + trim(n_elements(qvolt)) + ' points.'
    endif
  endif

  ;After long gaps > hour, the first few points after gap are usually low, so eliminate them
  qgap = where (ut_edges[0,1:*] - ut_edges[0,*] gt 3600., kgap)
  if kgap gt 0 then begin
    qgap = expand_elems(qgap+1, nexpand=expand_gap, min=0, max=n_elements(ut_edges[0,*]))
    if verbose then begin
      print, 'Number of points with gap = ' + trim(kgap) + '. Removing ' + trim(n_elements(qgap)) + ' points.'
    endif
  endif

endif ;end of filtering tests

; combine elements that were too hot or in night, and zero out the data and livetimea arrays for those elements
kzero = 0
if knight + khot + kgap + kbad gt 0 then begin
  if knight gt 0 then qzero = qnight
  if khot gt 0 then qzero = append_arr(qzero,qhot)
  if kvolt gt 0 then qzero = append_arr(qzero,qvolt)
  if kgap gt 0 then qzero = append_arr(qzero,qgap)
  if kbad gt 0 then qzero = append_arr(qzero,qbad)
  data[*,qzero] = 0.
  ltimes[*,qzero] = 0.
  kzero = n_elements(qzero)  ; might have some duplication, but kzero will be non-zero if there are any bad points
  if verbose then print,trim(n_elements(get_uniq(qzero))) + ' points zeroed out, out of ' + trim(n_elements(data[0,*])) + ' total points.'
endif

if show_plot then begin

  !p.multi=[0,1,3]
  linecolors
  erase
  utmid = get_edges(ut_edges,/mean)
  stars = '   Stars indicate points to be filtered out.
  
  yr_rate = minmax([rate_lo[where(rate_lo gt 0.)],rate_hi[where(rate_hi gt 0)]])
  utplot, /noerase, anytim(/vms,utmid),  rate_lo, col=7, /ylog, charsize=2., yran=yr_rate, ystyle=8, xmargin=[10,8], $
    /xstyle, title=filename + '  Top panel: Original Data' + stars, ytitle='Counts s!u-1!n'
  if knight gt 0 then outplot, anytim(/vms,utmid[qnight]), rate_lo[qnight], col=7, psym=2
  outplot, anytim(/vms,utmid), rate_hi, col=9
  if kgap gt 0 then outplot, anytim(/vms,utmid[qgap]), rate_hi[qgap], psym=2, col=12
  leg_text = ['Channels 2-15, 1.1-1.6 keV', 'Channels 16-200, 1.6-8.6 keV']
  leg_col = [7,9]
  leg_ls = [0,0]

  if check_ratio then begin
    yr = minmax(ratio[where(ratio ne 0.)])
    utplot, /noerase, anytim(/vms,utmid), ratio, /ylog, yrange=yr, charsize=2., ystyle=4, xmargin=[10,8], /xstyle
    axis, yaxis=1, yrange=yr, xmargin=[10,8], charsize=2., ytitle='Ratio', /xstyle
    oplot, !x.crange, ratio_threshold + [0.,0.], lines=1
    if knight gt 0 then outplot, anytim(/vms,utmid[qnight]), ratio[qnight], psym=2
    leg_text = [leg_text, 'Ratio of low band to high band', 'Ratio > '+trim(ratio_threshold,'(i3)') + ' means night.']
    leg_col = [leg_col, 255, 255]
    leg_ls = [leg_ls, 0, -99]
  endif
  ssw_legend, leg_text, color=leg_col, linestyle=leg_ls, box=0, charsize=1.
  !p.multi[0]=2

  title='Temperature (Red) Max Allowed = ' + trim(temp_max,'(f3.0)') + $
    ', Bias Voltage (White) Good value = ' + trim(good_bias_voltage,'(i3)') + stars
  utplot,/noerase,anytim(/vms,utmid),solar_detector_temp,col=2, charsize=2., ystyle=8, xmargin=[10,8], $
    title=title, ytitle='Degrees C', /xstyle
  if khot gt 0 then outplot, anytim(/vms,utmid[qhot]),solar_detector_temp[qhot],psym=2,col=2
  oplot, !x.crange, temp_max+[0.,0.], col=2, lines=1
  yr = minmax(bias_voltage)
  utplot, anytim(/vms,utmid), bias_voltage, ystyle=4, yrange=yr, charsize=2., xmargin=[10,8], /xstyle
  axis, yaxis=1, yrange=yr, xmargin=[10,8], charsize=2., ytitle='Volts', /xstyle
  if kvolt gt 0 then outplot, anytim(/vms,utmid[qvolt]),bias_voltage[qvolt],psym=2
  oplot, !x.crange, good_bias_voltage+[0.,0.], col=255, lines=1
;  ssw_legend, ['Temperature', 'Bias Voltage'], color=[2,255], lines=0

  rate_lo_new = f_div(total(data[2:15,*],1), ltimes[0,*])
  rate_hi_new = f_div(total(data[16:200,*],1), ltimes[0,*])
  have_data = max([rate_lo_new,rate_hi_new]) gt 0.
  if have_data then begin
    em = get_edges(ct_edges,/mean)
    z = value_locate(em, [4.,6.,8.,9.5])
    rate_4_6 = f_div(total(data[z[0]:z[1],*],1), ltimes[0,*])
    rate_6_8 = f_div(total(data[z[1]:z[2],*],1), ltimes[0,*])
    rate_8_95 = f_div(total(data[z[2]:z[3],*],1), ltimes[0,*])
    yr_rate = minmax([rate_lo_new[where(rate_lo_new gt 0.)],$
                      rate_hi_new[where(rate_hi_new gt 0)], $
                      rate_4_6[where(rate_4_6 gt 0.)], $
                      rate_6_8[where(rate_6_8 gt 0.)], $
                      rate_8_95[where(rate_8_95 gt 0.)] ])
  endif
  title = 'Filtered Data'
  if no_filter then title = title + '  (No filtering selected, so same as original data)'
  utplot, /noerase, anytim(/vms,utmid),  rate_lo_new, col=7, /ylog, charsize=2., yran=yr_rate, xmargin=[10,8], $
    title=title, ytitle='Counts s!u-1!n', /xstyle
  outplot, anytim(/vms,utmid), rate_hi_new, col=9
  if have_data then begin
    outplot, anytim(/vms,utmid), rate_4_6, col=12
    outplot, anytim(/vms,utmid), rate_6_8, col=5
    outplot, anytim(/vms,utmid), rate_8_95, col=2
  endif
  leg_text = ['Channels 2-15, 1.1-1.6 keV', 'Channels 16-200, 1.6-8.6 keV', $
              '4 - 6 keV', '6 - 8 keV', '8 - 9.5 keV']
  leg_col = [7,9,12,5,2]
  ssw_legend, leg_text, color=leg_col, linestyle=0, box=0, charsize=1.
  
  timestamp, /bottom
  !p.multi=0
  
endif

title = 'MESSENGER SPECTRUM'
units = 'counts'

data_str = { $
    START_TIME: anytim(ut_edges[0],/vms), $  ;start time of data in sec rel to 79/1/1
    END_TIME: anytim(ut_edges[1,nt-1], /vms), $  ;end time of data in sec rel to 79/1/1
    RCOUNTS: data, $  ; SOLAR_MON_SPECTRUM_23_253 - xrs data (nenergy, ntime)
    ERCOUNTS: sqrt(data), $  ; error in xrs data (nenergy, ntime)
    UT_EDGES: ut_edges, $  ;edges of time bins of xrs data in sec rel to 79/1/1 (2,ntime)
    UNITS: units, $  ; 'counts'
    AREA: area, $  ; detector area corrected to Earth view
    LTIME: ltimes, $  ;live times in seconds (nenergy, ntime)
    CT_EDGES: ct_edges, $  ;edges of energy bins in keV (2,nenergy)
    TITLE: title, $  ; label for data 'MESSENGER SPECTRUM'
    RESPFILE: {drm: drm, edges_in: ph_edges, edges_out: ct_edges }, $  ; DRM matrix, ras added edges, 17-feb-2014
    detused: 'XRS', $  ; label for detector name for main data in RCOUNTS
    atten_states: atten_states, $   ; -1, not used
    solar_stab: solar_stab, $  ; SOLAR STABILITY, structure with times and rates
    mon_rate: mon_rate, $  ;SOLAR_MONITOR_RATE (ntime)
    mon_pileup_rate:mon_pileup_rate, $  ; SOLAR_MONITOR_PILEUP_RATE (ntime)
    mon_valid_rate: mon_valid_rate, $  ;SOLAR_MONITOR_VALID_RATE (ntime)
    mon_an_rate: mon_an_rate, $  ; SOLAR_MONITOR_ANALYZED_RATE (ntime)
    solar_detector_temp_raw: solar_detector_temp_raw, $  ; raw detector temperature (ntime)
    solar_detector_temp: solar_detector_temp, $  ; detector temperature in degrees C (ntime)
    bias_voltage: bias_voltage, $
    pin_tec_mode: pin_tec_mode, $
    pin_tec_enable: pin_tec_enable, $
    gpc1_veto_rate: f_div(gpc1_veto_rate, int_time), $ ; convert to counts/sec
    gpc2_veto_rate: f_div(gpc2_veto_rate, int_time), $ ; convert to counts/sec
    gpc3_veto_rate: f_div(gpc3_veto_rate, int_time), $  ; convert to counts/sec
    gpc1_valid_rate: f_div(gpc1_valid_rate, int_time), $ ; convert to counts/sec, may need to correct for overflow?
    gpc1_an_rate: f_div(gpc1_an_rate, int_time) } ; convert to counts/sec, may need to correct for overflow?

END
