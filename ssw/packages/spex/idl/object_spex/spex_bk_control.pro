;+
; Name: spex_bk_control
;
; Purpose: Initialize control params for spex_bk class
;
; Modifications:
; 9-sep-2006,Kim.  Added spex_bk_ratio and spex_bk_sm_width
;-

function spex_bk_control

var = {spex_bk_control}

var.spex_bk_order = ptr_new(0)
var.spex_bk_ratio = 0
var.spex_bk_sm_width = 128

return, var
end