;+
;
; NAME:
;   spex_any_specfile__define
;
; PURPOSE:
;   Provides read_data method for any instrument.  CURRENTLY ONLY WORKS FOR MESSENGER SAXS CSV FILES!
;
;
; CATEGORY:
;       SPECTRAL FITTING, SPEX 
;
; CALLING SEQUENCE:
;
; CALLS:
;
; INPUTS:
;
; OUTPUTS:
;
; INPUT KEYWORDS:
;
; OUTPUT KEYWORDS:
;
; PROCEDURE:
;
; Written 09-Oct-2014, Kim Tolbert
;
; Modification History:
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------

pro spex_any_specfile::read_data, file_index=file_index, $
  spectrum,  errors,  livetime,  $
  spex_respinfo, spex_file_time,  spex_ut_edges,  spex_ct_edges,  $
  spex_area, spex_title, spex_detectors,  $
  spex_interval_filter, spex_units, spex_data_name, $
  spex_deconvolved, spex_pseudo_livetime, spex_data_pos, $
  err_code=err_code, _extra=_extra
  
  file =  self -> get( /spex_specfile )
  checkvar, file_index, 0
  file = file[file_index]
  
  read_messenger_pds_csv, FILES = file, $
    _EXTRA = _ref_extra, $
    data_str = ds, $
    ERR_CODE = err_code, $
    ERR_MSG = err_msg
    
  if err_msg[0] ne '' then begin
    ;if not spex_get_nointeractive() then xmessage,err_msg else print,err_msg
    message, err_msg, /cont
    if err_code then return
  endif
  
  spectrum = ds.rcounts
  errors = ds.ercounts
  livetime = ds.ltime
  spex_file_time = utime([ds.start_time, ds.end_time])
  spex_ut_edges = ds.ut_edges
  
  spex_respinfo = ds.respfile
  spex_ct_edges = ds.ct_edges
  spex_area = ds.area[0]
  spex_title = ds.title
  spex_detectors = ds.detused
  spex_units = ds.units
  spex_interval_filter = ds.atten_states
  spex_data_name = ds.data_name
  spex_deconvolved = 0
  spex_pseudo_livetime = 0
  spex_data_pos = [0.,0.]
  
end

;------------------------------------------------------------------------------
pro spex_any_specfile__define

  self = {spex_any_specfile, $
    INHERITS spex_data_strategy }
    
END
