;+
; Name: spex_fitalg_mcurvefit_info__define
;
; Purpose: Defines info structure for spex_fitalg_mcurvefit object
;
; Category: OSPEX
;
; Modifications:
;  23-Jun-2004. Kim.  Added mcurvefit_free_mask
;  3-Dec-2009, Kim.  Added mcurvefit_corr
;------------------------------------------------------------------------------
PRO spex_fitalg_mcurvefit_info__define

struct = { spex_fitalg_mcurvefit_info, $
           mcurvefit_fail_type:	0, $	; Mcurvfit success/failure code
           mcurvefit_fail_msg:	'', $	; Mcurvefit success/failure message
           mcurvefit_iter:	0L, $		; Number of iterations done in mcurvefit
           mcurvefit_chi2:	0.d0, $		; Chi-squared value after mcurvefit
           mcurvefit_corr: ptr_new(), $    ; Correlation matrix
           mcurvefit_free_mask: ptr_new() $ ; Parameter free/fixed mask used

	   }

END
