;+
;Name:
;   f_pileup_mod
;PURPOSE:
;   This pseudo function always returns an array of 0s.  Parameters are varied
;   during fit and used to add pileup effects to the model on the fly.

;
;
;CATEGORY:
;   SPECTRA, XRAY
;
;INPUTS:
;   E  energy vector in keV, 2XN edges or N mean energies
;   apar(0)  FWHM fraction
;   apar(1)  gain offset
;   apar(2)  center thickness ratio
;
; Written: Kim Tolbert, 27-Jun-2006
;-

function f_pileup_mod, e, apar, _extra=_extra

if (size(e))(0) eq 2 then edge_products, e, mean=em else em=e
return, fltarr(n_elements(em))
end