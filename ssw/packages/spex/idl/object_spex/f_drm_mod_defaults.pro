;+
; NAME:
;	f_drm_mod_defaults
;
; PURPOSE: Function to return default values for
;   parameters, minimum and maximum range, and free parameter mask when
;   fitting with drm_mod function.  drm_mod is a pseudo function - the function
;   value is always 0., but the params are used to create a new RHESSI DRM.
;
; CALLING SEQUENCE: defaults = f_drm_mod_defaults()
;
; INPUTS:
;	None
; OUTPUTS:
;	Structure containing default values
;
; MODIFICATION HISTORY:
; Kim Tolbert, 27-Jun-2006
;	ras, 1-may-2008 added blanket_coeff to replace center_thick ratio
;
;-
;------------------------------------------------------------------------------

FUNCTION f_drm_mod_defaults

defaults = { $
  fit_comp_params:           [1., 0., 1.], $  ; fwhm_frac, gain_offset, blanket_coeff
  fit_comp_minima:           [.5, -1, .05], $
  fit_comp_maxima:           [1.5, 1., 1.5], $
  fit_comp_free_mask:        [1b,    1b,   1b] $

}

RETURN, defaults

END