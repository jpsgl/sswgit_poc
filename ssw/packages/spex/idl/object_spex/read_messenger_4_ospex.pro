;+
;
; PROJECT:
; RHESSI
;
; NAME:
; read_messenger_4_ospex
;
; PURPOSE:
; Read messenger data and times from a file, compute drm, and return data and
;	associated values needed by OSPEX.
; This procedure is based on read_hessi_4_ospex.
;
; CATEGORY:
; SPEX
;
; CALLING SEQUENCE:
; read_messenger_4_ospex, FILES=files, data_str=data_str, ERR_CODE=err_code,
;                   ERR_MSG=err_msg, _REF_EXTRA=_ref_extra
;
; INPUTS:
;
; INPUT KEYWORDS:
; FILES - Name of file to read.
;
; OUTPUT KEYWORDS: structure data_str containing the values needed by OSPEX.
;
; OPTIONAL OUTPUTS:
;
; SIDE EFFECTS:
;
; RESTRICTIONS:
;
; PROCEDURE:
;
; EXAMPLE:
; Called from within SPEX to load MESSENGER data
;
; Written: 12-jun-2007.  richard.schwartz@gsfc.nasa.gov
;
; Modification History:
; 29-Apr-2008, Kim.  Rewrote to read xrs...dat files instead of excel files.
;
;-
;------------------------------------------------------------------------------
pro read_messenger_4_ospex, FILES=files, data_str=data_str, ERR_CODE=err_code,$
                   ERR_MSG=err_msg, _REF_EXTRA=_ref_extra, verbose=verbose


fcount = 0
If is_string( files ) then file = file_search(/fully_qualify_path, files, count=fcount)

if fcount eq 0 then begin
    err_code=1
    err_msg = 'No spectrum file selected.'
    return
endif

Catch, err_code
IF err_code ne 0 then begin
	catch, /cancel
	print,!error_state.msg
	err_msg = 'Error reading MESSENGER file ' + file[0]
	message,/continue, err_msg
	return
endif

filename = file[0]
break_file, filename, disk,dir,f,ext
lbl_file = disk+dir+f+'.lbl'
lbl = rd_tfile(lbl_file,/auto)
q = where (lbl[0,*] eq 'FILE_RECORDS', c)
if c gt 0 then nrec = lbl[2,q[0]] else err_msg = 'No FILE_RECORDS in file ' + lbl_file
q = where (lbl[0,*] eq 'START_TIME', c)
if c gt 0 then start_time = anytim(lbl[2,q[0]]) else err_msg = 'No START_TIME in file ' + lbl_file
q = where (lbl[0,*] eq 'STOP_TIME',c)
if c gt 0 then stop_time = anytim(lbl[2,q[0]]) else err_msg = 'No STOP_TIME in file ' + lbl_file
q = where (lbl[0,*] eq 'SPACECRAFT_CLOCK_START_COUNT',c)
if c gt 0 then clock_start = lbl[2,q[0]] else err_msg = 'No SPACECRAFT_CLOCK_START_COUNT in file ' + lbl_file

openr,lu,/get,filename[0]
bata=bytarr(2258,nrec) & readu, lu, bata & free_lun,lu

x=bata[332:332+461,*]  ;SOLAR_MON_SPECTRUM_23_253 - xrs data
;x=bata[328:328+461,*]
ieee_to_host,x,idltype=12
data = fix(x,0,231,nrec,type=12) * 1.

;shift = bata[330:331,*]
;ieee_to_host,shift,idltype=12
;shift = fix(shift,0,nrec,type=12)

a=bata[0:3,*]  ;MET, start time 
ieee_to_host, a,idltype=13
times = fix(a,0,nrec,type=13) - clock_start + start_time

a=bata[94:97,*]  ; actual_integration time
ieee_to_host, a,idltype=13
int_time = fix(a,0,nrec,type=13)

ut_edges = transpose( [[times], [times+int_time]] )

data_str = -1
err_code = 0
err_msg = ''
atten_states = -1

; compute and return drm, energy edges, and area
; first check that env var. for directory for messenger response information is set
messengerresp_dir = getenv('SSWDB_MESSENGER')
if messengerresp_dir eq '' then setenv, 'SSWDB_MESSENGER=' + concat_dir ('$SSW_OSPEX', 'messengerresp')

; get drm, energy edges and area
drm = messenger_drm(ein2=ct_edges, area=area)

; don't keep any energies below 1. keV - chianti can't handle
q = where (ct_edges[0,*] gt 1.)
e1 = q[0]
drm = drm[e1:*,e1:*]
ct_edges = ct_edges[*,e1:*]
data = data[e1:*,*]

nt = n_elements(data[0,*])
nen = n_elements(data[*,0])

ut_width = get_edges(ut_edges, /width)

; livetime is width of time bin minus 16 microsec for each count in all energies
ltimes = transpose( rebin(ut_width - .000016 * total(data,1), nt, nen))

title = 'MESSENGER SPECTRUM'
units = 'counts'

data_str = { $
    START_TIME: anytim(ut_edges[0],/vms), $
    END_TIME: anytim(ut_edges[1,nt-1], /vms), $
    RCOUNTS: data, $
    ERCOUNTS: sqrt(data), $
    UT_EDGES: ut_edges, $
    UNITS: units, $
    AREA: area, $
    LTIME: ltimes, $
    CT_EDGES: ct_edges, $
    TITLE: title, $
    RESPFILE: drm, $
    detused: 'XRS', $
    atten_states: atten_states }
if keyword_set(verbose) then help, data_str,/st

END
