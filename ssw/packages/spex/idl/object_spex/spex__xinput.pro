;+
; Name: SPEX::XINPUT
;
; Purpose: OSPEX method to provide widget to handle input file selection and plotting
;   of raw data.
;
; Category: OSPEX
;
; Calling sequence:  o -> xinput  (o is an ospex object)
;
; Modifications:
; 20-Jul-2004, Kim.  Added buttons to preview all spectrum or srm files in current dir.
; 16-Sep-2004, Kim.  Added showing albedo settings, and change button for them
; ?-Dec-2004,  Kim.  Added roi stuff for SPEX_HESSI_IMAGE strategy
; 17-Aug-2005, Kim.  If spex_pseudo_livetime set, don't allow counts to be plotted.
;   Desensitize ROI stuff if input is not an image cube.  Use new units_widget gen
;   method to handle units.  Use plot_time and plot_spectrum instead of plot method
;   of data object.  Made 'Select Regions' button a pulldown with Config and Select.
;	Added Region Number box.
; 9-Feb-2006, Kim.  Added spex_image_full_srm option (added button on ROI line)
; 15-Mar-2006, Kim. added gui_label to put on title bar of spex widget interfaces
; 2-Jul-2006, Kim.  Added buttons for spex_tband
; 28-Jul-2006, Kim.  Added timeshift widget, and de-sensitize drm if input is soxs
; 16-Mar-2007, Kim.  Changed Anisotropy label to Isotropy (since meaning is backward!)
; 13-Jun-2007, Kim.  set use_drmfile based on data type.  Added MESSENGER.
; 24-Sep-2007, Kim.  Set current value of spex_roi_use in update_widget method
; 4-Nov-2007, Kim. Call widget spex_xinput in call to xmanager (instead of xinput) - was
;  getting confused with xinput text widget, and then lost widget handling of spex_xinput.
; 16-Jun-2008, Kim.  spex_specfile is now allowed to be an array. use arr2str for widget value.
; 10-Jul-2008, Kim. Change hsi_ui_getfont to get_font (which is in ssw gen)
; 10-Jul-2009, Kim. Allow user to select combinations of ROIs for image input
; 14-Jul-2009, Kim. Guard against no ROIs selected. Desensitize entire wroi_base when not image input.
; 04-Aug-2009, Kim. Added 'Plot GOES' button.
; 25-Feb-2010, Kim. Added remote search, but comment out for now.
; 07-Jul-2010, Kim. Enabled remote search for input files (Browse button now has 2 options)
;  For non image files, show time selection widgets under file widget.  Moved roi widget stuff to new xinput_roi_widget
;  method and created new xinput_time_widget method - call either of these depending on type of input file.
;  Use spex_datatypes drmfile flag to determine whether a data type needs a drm file (hardcoded before)
; 22-Jan-2011, Kim.  Reduced size of widget by reducing space, pad, and # lines to show. Also put all
;  widgets inside wbox which is inside wbase in case later want to add /scroll to wbase.
; 23-Feb-2011, Kim. Added Detector Angle button when input is Fermi GBM.
; 10-Aug-2011, Kim. Added 'Set to default' button for energy bands for time plots. Uses new spex_def_eband param.
; 19-Aug-2012, Kim. Expand flare time by +-20 min when selecting time from GBM or RHESSI flare catalog
; 02-Mar-2015, Kim. Plot Goes button now has menu - options to plot flux or open goes workbench gui
; 11-Mar-2016, Kim. Increased number of ROI buttons to 6 (from 4) (note 64 are allowed, but user has to control them from CLI)
; 03-Oct-2017, Kim. Added option in Region pulldown menu to get spectrum from RHESSI image alg params
;-
;-------------------------------------------------------------------------------------
pro spex::xinput_roi_widget, state

  data_units = self->Get(/spex_data_units)
  data_name = data_units.data_name
  is_hessi = stregex( data_name, 'HESSI',/fold,/bool)
  image_alg = is_hessi ? (hsi_alg_units(self->get(/image_alg))).name : ''
  state.wroi_base = widget_base (state.wroi_or_time_base, /row, space=1, /align_center)
  state.wroi = widget_button (state.wroi_base, value='Select Regions ->', /menu)
  tmp = widget_button (state.wroi, value='Configure Region Selection Parameters', uvalue='roi_config')
  tmp = widget_button (state.wroi, value='Region Selection Widget', uvalue='roi')
  if image_alg eq 'VIS FWDFIT' then $
    tmp = widget_button(state.wroi, value='Use image algorithm parameters', uvalue='use_alg_parms')
  state.w_full_srm = cw_bgroup (state.wroi_base, 'Calc Full SRM', /row, $
    /nonexclusive, uvalue='full_srm')
  widget_control, state.w_full_srm, sensitive=(image_alg ne 'VIS FWDFIT')
  ;wregion = widget_droplist(wroi_base, value=['0','1','2','3','All','Comb'], uvalue='region', title='Region #: ')
  tmp = widget_label (state.wroi_base, value='Region #')
  state.wregion =cw_bgroup(state.wroi_base, trim(indgen(6)), /row, space=1, /nonexclusive, uvalue='region')
  state.wreg_integrate =  cw_bgroup(state.wroi_base, 'All', /row, /nonexclusive, uvalue='reg_integrate')

  widget_control, state.wbase, set_uvalue=state

end

;-----

pro spex::xinput_time_widget, state

  state.wtime_base = widget_base (state.wroi_or_time_base, /row, space=1, /align_center)
  state.wentire_file = cw_bgroup (state.wtime_base, 'Entire file', uvalue='entire_file', /row, /nonexclusive)
  state.wsetfrom_base = widget_button (state.wtime_base, value='Set from ->', /menu, /align_center)
  tmp = widget_button (state.wsetfrom_base, value='Current plot limits', uvalue='setfromplot')
  tmp = widget_button (state.wsetfrom_base, value='RHESSI flare catalog', uvalue='fromrhessicat')
  tmp = widget_button (state.wsetfrom_base, value='FERMI GBM flare catalog', uvalue='fromgbmcat')

  state.wut_range = cw_ut_range ( state.wtime_base, $
    value=[0.,0.], $
    label='', $
    uvalue='time_range', $
    /oneline, /noreset, /nomsec, frame=0 )

  accum_time = self -> get(/spex_accum_time)
  file_time = self -> get(/spex_file_time)
  if valid_time_range(accum_time) then begin
    entire_file = 0
    if valid_time_range(file_time) then if ~has_overlap(accum_time, file_time) then entire_file = 1
  endif else entire_file = 1
  widget_control, state.wentire_file, set_value=entire_file
  if entire_file then widget_control, state.wut_range, set_value=file_time

  widget_control, state.wbase, set_uvalue=state

end

;-----

pro spex::xinput_update_widget, wbase

  widget_control, wbase, get_uvalue=state
  specfile = self -> get(/spex_specfile)
  data_strat = (self->get(/obj,class='spex_data')) -> getstrategy()
  if is_class(data_strat, 'SPEX_USER_DATA') then specfile='None - User data selected.'

  image_input = is_class(data_strat, 'SPEX_IMAGE')
  struct = spex_datatypes(data_strat)
  use_drmfile = is_struct(struct) ? struct.drmfile : 1
  ;use_drmfile =  (image_input or $
  ;	is_class(data_strat, 'SPEX_SOXS_SPECFILE') or $
  ;	is_class(data_strat, 'SPEX_MESSENGER_SPECFILE')) eq 0

  ; If image input, then make ROI buttons active.  Otherwise make time selection buttons active
  if image_input then begin
    if ~xalive(state.wroi_base) then begin
      if xalive(state.wtime_base) then widget_control, state.wtime_base, /destroy
      self->xinput_roi_widget, state
    endif
    roi_use = self->get(/spex_roi_use)
    roi_integrate = self->get(/spex_roi_integrate)
    ;  if roi_integrate eq 1 then roi_index=4 else roi_index=fix(roi_use)>0<3
    ;endif else roi_index = 0
    roi_index = [0,0,0,0,0,0]
    q = where(roi_use lt 6, nq)
    if ~roi_integrate and nq gt 0 then roi_index[roi_use[q]] = 1  ; could have more ROIs, but only showing 0-5 here.
    widget_control, state.wroi
    widget_control, state.w_full_srm, set_value = self->get(/spex_image_full_srm)
    widget_control, state.wregion, set_value=roi_index
    widget_control, state.wreg_integrate, set_value=roi_integrate
    widget_control, state.wroi_base, sensitive=image_input
  endif else begin
    if ~xalive(state.wtime_base) then begin
      if xalive(state.wroi_base) then widget_control, state.wroi_base, /destroy
      self->xinput_time_widget, state
    endif
    widget_control, state.wentire_file, get_value=entire_file
    widget_control, state.wut_range, sensitive=~entire_file
    widget_control, state.wsetfrom_base, sensitive=~entire_file
    accum_time = self -> get(/spex_accum_time)
    time = valid_time_range(accum_time) ? accum_time : self -> get(/spex_file_time)
    widget_control, state.wut_range, set_value=time

    ; If input is gbm file, then add buttons to plot and print detector angle to sun info.
    ; When made widget, added a base called w_input_extra that holds nothing until we get here.
    ; Make first child under w_input_extra a base - for some reason removing and adding
    ; the button doesn't work if the first child is the button (that worked until I made it a menu
    ; button, don't know why the difference)
    ; If input isn't gbm, and this button was alive, destroy it.
    child = widget_info(state.w_input_extra, /child)
    if is_class(data_strat, 'SPEX_FERMI_GBM_SPECFILE') then begin
      if ~xalive(child) then begin
        tmp_base = widget_base(state.w_input_extra, /row)
        tmp = widget_button(tmp_base, value='Detector Angles->', /menu)
        tmp2 = widget_button(tmp, value='Plot Cosines of Angle to Sun', uvalue='gbm_angle_plot')
        tmp2 = widget_button(tmp, value='Print Cosines at Flare Peak', uvalue='gbm_angle_print')
      endif
    endif else if xalive(child) then widget_control, child, /destroy

  endelse  ; end of if for image/roi vs time input


  widget_control, state.wutoffset, set_value = self->get(/spex_ut_offset), sensitive=(image_input eq 0)
  widget_control, state.w_ignore_filt, set_value=self->get(/spex_ignore_filters)
  widget_control, state.wfile2, sensitive=use_drmfile

  widget_control, state.wspecfile, set_value=arr2str(specfile)
  widget_control, state.wsrmfile, set_value=self -> get(/spex_drmfile)

  albedo = self -> get(/spex_albedo)
  case albedo of
    0: enab = 'Disabled'
    1: enab = 'Enabled'
    else: enab = 'Unknown'
  endcase
  text = 'Albedo Correction: ' + enab
  if albedo then begin
    angle = trim(self -> get(/spex_source_angle), '(f7.2)')
    aniso = trim(self -> get(/spex_anisotropy), '(f4.2)')
    text = text + ';  Source Angle (deg): ' + angle + ';  Isotropy: ' + aniso
  endif
  widget_control, state.walbedo, set_value=text

  widget_control, state.weband, set_value=format_intervals(self -> get(/spex_eband), format='(f9.1)')

  widget_control, state.wtband, set_value=format_intervals(self -> get(/spex_tband), /ut)

  ;widget_control, wbase, set_uvalue=state

  self -> units_widget, state.wunits, /update
  ;allow_counts = not self->get(/spex_pseudo_livetime)
  ;units_new = allow_counts ? ['Counts', 'Rate', 'Flux'] : ['Rate', 'Flux']
  ;widget_control, state.wunits, get_value=units
  ;index = widget_info(state.wunits, /droplist_select)
  ;if n_elements(units) lt n_elements(units_new) then index = index + 1
  ;if n_elements(units) gt n_elements(units_new) then index = (index - 1) > 0
  ;widget_control, state.wunits, set_value=trim(units_new,'(a9)'), set_droplist_select=index

end

;-----
; Event handlers can't be object methods, so this is just a wrapper to get to the method

pro spex_xinput_event,event
  widget_control,event.top,get_uvalue=state
  if obj_valid(state.object) then state.object->xinput_event,event
  return & end

  ;-----
  ; xinput event handler

pro spex::xinput_event, event

  widget_control, event.top, get_uvalue=state

  widget_control, event.id, get_uvalue=uvalue

  if not exist(uvalue) then return

  self -> units_widget, state.wunits, units=units, /getval
  ;widget_control, state.wunits, get_value=unit_vals
  ;units = trim(unit_vals[widget_info(state.wunits, /droplist_select)])

  widget_control, state.w_int_sel, get_value=full_options

  case uvalue of

    'specfile': begin
      widget_control, state.wspecfile, get_value=value
      self -> set_file, value, dialog=0, /nointeractive
    end

    'roi_config':  self -> roi_config, group=event.top

    'roi': begin
      self -> roi, group=event.top
      self->set, spex_image_use_alg_parms=0
    end

    'use_alg_parms': self->set, /spex_image_use_alg_parms, /spex_image_full_srm

    'full_srm':  self->set, spex_image_full_srm = event.select

    'region': begin
      ;		if event.index lt 4 then begin
      ;			self -> set, spex_roi_use=event.index
      ;			self -> set, spex_roi_integrate=0
      ;		endif else begin
      ;			self -> set, spex_roi_integrate=1
      ;			self -> set, spex_roi_use=99
      ;		endelse
      ; roi_sel will be array of 0s/1s, roi_use is indices where it equals 1.
      widget_control, event.id, get_value=roi_sel
      roi_use = where(roi_sel)
      if total(roi_sel) eq 0 then roi_use = 0
      self -> set, spex_roi_use = roi_use
      self -> set, spex_roi_integrate = 0
    end

    'reg_integrate': begin
      self -> set, spex_roi_use = 99
      self -> set, spex_roi_integrate=1
    end

    'entire_file': begin
      if event.select then self -> set, spex_accum_time = [0.d, 0.d]
    end

    'setfromplot': begin
      p_obj = self->get_plotman_obj()
      if p_obj -> valid_window(/message, /utplot) then begin
        p_obj -> select   ; select so !x.crange will be correct
        accum_time=p_obj->get(/utbase) + !x.crange
        p_obj -> unselect
        self -> set_time, accum_time
      endif
    end

    'fromrhessicat': begin
      accum_time = self -> get(/spex_accum_time)
      if have_proc('hsi_ui_flarecat') then begin
        flare = hsi_ui_flarecat (group=event.top, /select_one, $
          obs_time_interval=accum_time, error=error)
        if flare[0] ne -1 then begin
          a = hsi_getflare(flare, desc=desc, err_msg=err_msg)
          if err_msg eq '' then self -> set_time, [a.start_time-20.*60., a.end_time+20.*60.]
        endif
      endif else message,'You do not have the RHESSI SSW branch loaded.', /cont
    end

    'fromgbmcat': begin
      catobj = obj_new('gbm_cat')
      index = catobj->select(/index)
      cat = catobj->getdata()
      self -> set_time, [cat[index].utstart-20.*60., cat[index].utend+20.*60.]
    end

    'time_range': self -> set_time, event.value

    'utoffset': self -> set, spex_ut_offset = event.value

    'ignore_filt': self->set, spex_ignore_filters = event.select

    'gbm_angle_plot': begin
      accum_time = self->get(/spex_accum_time)
      time_range = valid_time_range(accum_time) ? accum_time : self->get(/spex_file_time)
      gbm_plot_det_cos, time_range, plotman_obj=self->get_plotman_obj()
    end

    'gbm_angle_print': begin
      accum_time = self->get(/spex_accum_time)
      time_range = valid_time_range(accum_time) ? accum_time : self->get(/spex_file_time)
      prstr, gbm_get_det_cos(time_range, /string)
    end

    'srmfile': begin
      widget_control, state.wsrmfile, get_value=value
      if value eq '' then self -> set, spex_drmfile='' else $
        self -> set_file, value, /srm, dialog=0, /nointeractive
    end

    'change_alb': begin
      widget_control, event.top, sensitive=0
      self -> xalbedo, group=event.top
      widget_control, event.top, sensitive=1
    end

    'specbrowse': self -> set_file, self->get(/spex_specfile), /dialog

    'find_data': begin
      self -> xtime, group=event.top, widget_notify=state.wrefresh
      ;	  self -> xtime, group=event.top, widget_notify=state.wentire_file ;state.{id:state.wentire_file, top:0L, handler:0L, value:0}
      widget_control, state.wentire_file, set_value=0
    end

    'specpreview': self -> preview, /spec

    'specpreview_all': begin
      xmessage,['', '      Working...       ', ''], wbase=wxmessage
      list_sp_files
      widget_control, wxmessage, /destroy
    end
    'specheader': self->fitsheader, /spec

    'srmbrowse': self -> set_file, self->get(/spex_drmfile), /dialog, /srm
    'srmpreview': self -> preview, /drm
    'srmpreview_all': begin
      xmessage,['', '      Working...       ', ''], wbase=wxmessage
      list_sp_files, /rm
      widget_control, wxmessage, /destroy
    end
    'srmheader': self->fitsheader, /drm

    'plottime': begin
      widget_control, state.wfilter, get_value=show_filter
      self -> plot_time, show_filter=show_filter, spex_units=units
    end

    ;	'plotspec': self -> plotman, class='spex_data', /pl_spec, /show_filter, spex_units=units, /log_scale
    'plotspec': begin
      widget_control, state.wfilter, get_value=show_filter
      self -> plot_time, /data, /pl_spec, show_filter=show_filter, spex_units=units, /log_scale
    end

    'plot_goes': begin
      d = self->getdata()
      if is_struct(d) then $
        goes_plot, time_range=minmax(self->getaxis(/ut,/edges_2)), plotman_obj=self -> get_plotman_obj()
    end

    'goes_workbench': begin
      d = self->getdata()
      tr = minmax(self->getaxis(/ut,/edges_2))
      gobj = obj_new('goes')
      gobj->set, tstart=anytim(tr[0],/vms), tend=anytim(tr[1],/vms), plotman_obj = self->get_plotman_obj()
      gobj->gui
    end

    ;'plotenergy': self -> plotman, class='spex_data', /pl_energy, spex_units=units
    ; plot spectrum in spex_tbands times if set, otherwise allint
    'plotenergy': self -> plot_spectrum, /tband, /dim1_sum, $
      spex_units=units, /allint, use_fitted=0

    'ebands': self -> intervals, /eband, full_options=full_options, spex_units=units

    'draw_ebands': begin
      if not self->valid_plot(/xyplot) then $
        self -> plotman, class='spex_data', spex_units=units, /pl_energy
      intervals = self -> get(/spex_eband)
      plotman_obj = self -> get_plotman_obj()
      if intervals[0] ne -1 then $
        plotman_draw_int,'all',{plotman_obj:plotman_obj}, intervals=intervals, type='Pre-bin'
    end

    'def_ebands':  self -> set,spex_eband=(self->get(/spex_def_eband))

    'tbands': self -> intervals, /tband, full_options=full_options, spex_units=units

    'draw_tbands': begin
      if not self->valid_plot(/utplot) then $
        self -> plotman, class='spex_data', spex_units=units, /pl_time
      intervals = self -> get(/spex_tband)
      plotman_obj = self -> get_plotman_obj()
      if intervals[0] ne -1 then $
        plotman_draw_int,'all',{plotman_obj:plotman_obj}, intervals=intervals, type='Pre-bin'
    end

    'refresh': self -> xinput_update_widget, state.wbase

    'exit': widget_control, state.wbase, /destroy

    else:
  endcase

  if xalive (state.wbase) then state.object -> xinput_update_widget, state.wbase
end

;-----

pro spex::xinput, group=group, gui_label=gui_label

  if xregistered ('spex_xinput') then begin
    xshow,'spex_xinput', /name
    return
  endif

  checkvar, gui_label, ''

  get_font, font, big_font=big_font

  wbase = widget_base (group=group, title='SPEX Input Options '+gui_label, $
    space=2, xpad=2, ypad=2)

  wbox = widget_base( wbase, /column, /frame, space=1, xpad=1, ypad=1)

  tmp = widget_label( wbox, value='Select Input', $
    font=big_font, /align_center)

  wfile1 = widget_base( wbox, /column, /frame, xpad=1, ypad=1, space=1, /align_center)
  wfile1_r1 = widget_base( wfile1, /row, space=10)
  wfile1_name = widget_base( wfile1_r1, /row)
  tmp = widget_label( wfile1_name, value='Spectrum or Image File: ')
  wspecfile = widget_text( wfile1_name, /edit, xsize=60, value='', uvalue='specfile')
  ;tmp = widget_button( wfile1_r1, value='Browse...', uvalue='specbrowse')
  w_browse = widget_button( wfile1_r1, value='Browse ->', /menu)
  tmp = widget_button( w_browse, value='On this computer...', uvalue='specbrowse')
  tmp = widget_button( w_browse, value='On remote sites...', uvalue='find_data')

  ;wfile1_r2 = widget_base( wfile1, /row, space=30);, /align_center)
  wroi_or_time_base = widget_base( wfile1, /row, space=1)

  wsumm_base = widget_base( wfile1, /row, space=20, /align_center)
  w_ignore_filt = cw_bgroup( wsumm_base, 'Ignore filters', /row, $
    /nonexclusive, uvalue='ignore_filt')
  wutoffset = cw_edroplist ( wsumm_base, $
    label='Time Offset (s): ', $
    value=0., $
    format='(f7.2)', $
    xsize=10, $
    drop_values =  -100 + findgen(20) * 10., $
    uvalue='utoffset' )
  wfile1_sum = widget_button( wsumm_base, value='Summarize -> ', /menu)
  tmp = widget_button( wfile1_sum, value='this file', uvalue='specpreview')
  tmp = widget_button( wfile1_sum, value='all spectrum files in current directory', uvalue='specpreview_all')
  tmp = widget_button( wsumm_base, value='Show Header', uvalue='specheader')
  ; the w_input_extra base will be used to put the button for fermi gbm detector angles,
  ; if that is the input type.  Could be used for other data-specific needs too.
  w_input_extra = widget_base(wsumm_base, /row, space=1)

  wfile2 = widget_base( wbox, /column, /frame)
  wfile2_r1 = widget_base( wfile2, /row, space=10)
  wfile2_name = widget_base( wfile2_r1, /row)
  tmp = widget_label( wfile2_name, value='SRM File: ')
  wsrmfile = widget_text( wfile2_name, /edit, xsize=60, value='', uvalue='srmfile')
  tmp = widget_button( wfile2_r1, value='Browse...', uvalue='srmbrowse')

  wfile2_r2 = widget_base( wfile2, /row, space=30, /align_center)
  walb_row = widget_base( wfile2_r2, /row, space=5)
  walbedo = widget_label( walb_row, value='', /dynamic_resize)
  tmp = widget_button( walb_row, value='Change...', uvalue='change_alb')
  wfile2_sum = widget_button( wfile2_r2, value='Summarize -> ', /menu)
  tmp = widget_button( wfile2_sum, value='this file', uvalue='srmpreview')
  tmp = widget_button( wfile2_sum, value='all srm files in current directory', uvalue='srmpreview_all')
  tmp = widget_button( wfile2_r2, value='Show Header', uvalue='srmheader')

  w_plot_base = widget_base( wbox, /column, space=1, /frame)

  w_row1 = widget_base(w_plot_base, /row)
  w_int_sel = cw_bgroup(w_row1, ['Graphical', 'Full Options'], /row, $
    label_left='Interval Selection Interface: ', /exclusive, /return_index, $
    uvalue='None', /no_release, set_value=0)

  wfilter = cw_bgroup( w_row1, 'Show Filter', uvalue='show_filter', /nonexclusive)

  w_plot_base1 = widget_base( w_plot_base, /row, space=8)
  weband = widget_droplist( w_plot_base1, title='Energy Bands for Time Plots:', $
    value='xxxxxxxxxxxxxxxxxxxxx', uvalue='none', /align_center, /dynamic_resize)
  tmp = widget_button( w_plot_base1, value='Change', uvalue='ebands', /align_center)
  tmp = widget_button( w_plot_base1, value='Show Ebands', uvalue='draw_ebands', /align_center)
  tmp = widget_button( w_plot_base1, value='Set to default', uvalue='def_ebands', /align_center)

  w_plot_base2 = widget_base( w_plot_base, /row, space=8)
  wtband = widget_droplist( w_plot_base2, title='Time Bands for Energy Plots:', $
    value='xxxxxxxxxxxxxxxxxxxxx', uvalue='none', /align_center, /dynamic_resize)
  tmp = widget_button( w_plot_base2, value='Change', uvalue='tbands', /align_center)
  tmp = widget_button( w_plot_base2, value='Show Tbands', uvalue='draw_tbands', /align_center)

  w_plot_base3 = widget_base( w_plot_base, /row, space=20, /align_center)

  self -> units_widget, wunits, parent=w_plot_base3, units='flux'
  ;allow_counts = not self->get(/spex_pseudo_livetime)
  ;units = allow_counts ? ['Counts', 'Rate', 'Flux'] : ['Rate', 'Flux']
  ;wunits = widget_droplist( w_plot_base3, title='Plot Units: ', $
  ;	value=trim(units,'(a9)'), uvalue='none')
  ;widget_control, wunits, set_droplist_select=n_elements(units)-1

  tmp = widget_button( w_plot_base3, value='Plot Spectrum', uvalue='plotenergy')
  tmp = widget_button( w_plot_base3, value='Plot Time Profile', uvalue='plottime')
  tmp = widget_button( w_plot_base3, value='Plot Spectrogram', uvalue='plotspec')

  wgoes = widget_button( w_plot_base3, value='Plot GOES ->', /menu)
  tmp = widget_button( wgoes, value='Plot flux', uvalue='plot_goes')
  tmp = widget_button( wgoes, value='GOES workbench', uvalue='goes_workbench')

  wbase_but2 = widget_base( wbox, /row, space=20, /align_center)
  wrefresh = widget_button( wbase_but2, value='Refresh', uvalue='refresh')
  tmp = widget_button( wbase_but2, value='Close', uvalue='exit')

  state = {wbase: wbase, $
    wfile2: wfile2, $
    wspecfile: wspecfile, $
    wroi_or_time_base: wroi_or_time_base, $
    wroi_base: 0L, $
    wroi: 0L, $
    w_full_srm: 0L, $
    wregion: 0L, $
    wreg_integrate: 0L, $
    wtime_base: 0L, $
    wentire_file: 0L, $
    wsetfrom_base: 0L, $
    wut_range: 0L, $
    w_ignore_filt: w_ignore_filt, $
    wutoffset: wutoffset, $
    w_input_extra:w_input_extra, $
    wsrmfile: wsrmfile, $
    walbedo: walbedo, $
    wfilter: wfilter, $
    weband: weband, $
    wtband: wtband, $
    wunits: wunits, $
    w_int_sel: w_int_sel, $
    wrefresh:wrefresh, $
    object: self, $
    gui_label: gui_label }

  widget_control, wbase, set_uvalue=state

  self -> xinput_update_widget, wbase

  widget_offset, group, newbase=wbase, xoffset, yoffset

  widget_control, wbase, xoffset=xoffset, yoffset=yoffset

  widget_control, wbase, /realize
  xmanager, 'spex_xinput', wbase, /no_block

end