; Modifications:
; 18-Apr-2008, Added spex_fit_firstint_method and added previous_int_start to comment for spex_fit_start_method
; 08-Dec-2008, Added spex_fit_auto_erange
; 29-May-2009, Added spex_fit_init_params_only
pro spex_fit_control__define

struct = {spex_fit_control, $
	spex_fit_manual: 0, $	; 0=automatic, 1=manual on first interval, 2=manual on all intervals
	spex_fit_reverse: 0, $	; Interval Loop direction, 0 = forward, 1 = reverse
	spex_fit_start_method: '', $ ; Method for setting starting params for fit, 'default', 'previous_int', 'previous_start', 'previous_iter'
	spex_fit_firstint_method: '', $ ; Method for setting starting params for fit for first interval, 'default', 'current', 'fn', 'sn' (final,start from interval n)
	spex_fit_init_params_only: 0, $ ;If set, only set params according to firstint_method orstart_method 
	spex_fit_auto_erange: 0 $ ; if set, automatically set upper limit of energy range to fit
	}
end