;+
; Name: spex_smm_hxrbs__define
;
; Purpose: Provide methods to deal with the smm hxrbs data when downloading,etc. for OSPEX and SHOW_SYNOP.
;   
; Written: Kim Tolbert, 18-Mar-2013
;-

function spex_smm_hxrbs::init
return,1
end

;-----

function spex_smm_hxrbs::search, time_range, count=count, _ref_extra=extra

spex_find_data, 'smm_hxrbs', time_range, url=url, count=count, _extra=extra

if count eq 0 then return, ''

return, url
end

;-----
; We need .dat and .lbl files, but only want to select the .dat file for input as spex_specfile to OSPEX.
pro spex_smm_hxrbs::select_files, files, time_range, specfile, drmfile

specfile = files[0]
drmfile = ''

end

;-----

pro spex_smm_hxrbs__define
void = {spex_smm_hxrbs, inherits spex_instr}
end