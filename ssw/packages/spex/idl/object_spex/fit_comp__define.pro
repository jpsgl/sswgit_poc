;+
; Name: fit_comp__define
; Purpose: Object definition for each fit component
;
; Written:  Kim Tolbert
; Modifications:
; 7-Apr-2006, Kim.  In process, call fit_comp_kw to get keyword options (so far
;   that's the spectrum and model keywords) into a structure to
;   call the function with.
;   In init, initialize source if not passed in.
;
;------------------------------------------------------------------------------

function fit_comp::init, source=source, _extra=_extra

if not obj_valid(source) then source = obj_new('fit_comp_gen')

ret = self -> framework::init (control={fit_comp_control}, info=fit_comp_info(), source=source )

if keyword_set(_extra) then self -> set, _extra=_extra, status=status

if exist(status) then ret=status

return, ret

end

;------------------------------------------------------------------------------

pro fit_comp::cleanup

self -> framework::cleanup, /this_class_only
end

;------------------------------------------------------------------------------

pro fit_comp::set, $
	fit_comp_function=fit_comp_function, $
	done=done, not_found=not_found, $
	status=status, $
	_extra=_extra

status = 0

if keyword_set(fit_comp_function) then begin
	fit_comps = fit_model_components()
	comp = str_match (fit_comp_function, fit_comps, count=count)
	if count gt 0 then begin
		fit_comp_function = comp
		self -> framework::set, fit_comp_function=comp, done=done, not_found=not_found
		defaults = fit_comp_defaults(comp)
		self -> framework::set, _extra=defaults, done=done, not_found=not_found
	endif else begin
		message, 'Invalid fit component: ' + comp, /cont
		return
	endelse
endif

if keyword_set(_extra) then self -> framework::set, _extra=_extra, done=done, not_found=not_found
status = 1

end



;------------------------------------------------------------------------------
; need this method to insert the catch
function fit_comp::getdata, _extra=_extra

@spex_insert_catch

return, self -> framework::getdata(_extra=_extra)
end

;------------------------------------------------------------------------------

pro fit_comp::process, _extra=_extra

;print,'in fit_comp::process for function ' + self -> get(/fit_comp_function)

fit_xvals = self -> getdata(class='fit_comp_gen')
if fit_xvals[0] eq -1 then begin
	message,'No Fit X values have been set yet.  Aborting.'
endif

; get structure to pass into function for keyword options

fc = self->get(/fit_comp)
kw = fit_comp_kw (fc, process_struct=process_struct, obj=self)
;if kw then print,tag_names(process_struct), process_struct

;print,'in fit_comp::process, func = ', fc.fit_comp_function, ' ', trim(fc.fit_comp_params)
;help,/tr
;if keyword_set(process_struct) then help,process_struct,/st

self -> setdata, call_function( 'f_' + fc.fit_comp_function, $
	fit_xvals, $
	fc.fit_comp_params, $
	_extra=process_struct )

end

;------------------------------------------------------------------------------

pro fit_comp__define

dummy = {fit_comp, $
	inherits framework $
	}

end