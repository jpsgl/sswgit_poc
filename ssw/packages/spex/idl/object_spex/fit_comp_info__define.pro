;+
; Name: fit_comp_info__define
; Purpose: Define fit_comp_info parameter structure
;
; Written: Kim Tolbert
; Modifications:
; 7-Apr-2006 Kim.  Added spectrum,model params
;
;---------------------------------------------------------------

pro fit_comp_info__define

struct = { fit_comp_info, $
  fit_comp_sigmas:     ptr_new(), $	; Sigma in fit parameters, (nparam)
  fc_spectrum_options: strarr(3), $ ; Options for fit_comp_spectrum
  fc_model_options:    strarr(2) $ ; Options for fit_comp_model
  }

end


