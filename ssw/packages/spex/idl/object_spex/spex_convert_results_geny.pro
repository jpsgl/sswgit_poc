;+
; Name: spex_convert_results

; Purpose: Convert structure from ospex results geny file from Version 1. to 1.1.
;  Same function as spex_convert_results, except conveniently, the tag names are different, so
;  couldn't call that.  Plan to phase out geny files, so conversion from 1.0 to 1.1 hopefully
;  will be the only conversion necessary for geny files.
;
; Input:
;  struct - structure of OSPEX results from gen file
;
; Output:
;  Each of the input variable is changed as needed and returned.
;
; Method:
; Changes need to convert from Version 1. to 1.1:
;   1. vth functions (vth, multi_therm_pow, multi_therm_exp) now have an extra parameter
;     (abundance).  Need to add an extra element in the correct place in all arrays that
;     are dimensioned by nparams and set to default values.
;   2. func_spectrum, func_model need to be added to structure, and set to -1 for non-thermal
;      functions, and 'full', 'mewe' for any thermal function
;   3. chianti_version, abun_table need to be added to structure.
;      since we only had mewe before this, abun_table should be set mewe_solar
;
; Written: Kim Tolbert 1-May-2006
; 15-May-2006, Kim.  Replace any vth_noline components with vth and set their
;   corresponding func_spectrum value to 'continuum'
;-


pro spex_convert_results_geny, struct


version = '1.0'
ntimes = n_elements(struct.spex_summ_time_interval) / 2
nchan = n_elements(struct.spex_summ_energy) / 2
fit_func = trim(struct.spex_summ_fit_function)

fit_comp_arr = str2arr(fit_func, '+')

; replace any vth_noline with vth, and down below set keyword to continuum
ind_noline = where (fit_comp_arr eq 'vth_noline', count_noline)
if count_noline gt 0 then begin
	fit_comp_arr[ind_noline] = 'vth'
	fit_func = arr2str(fit_comp_arr, '+')
	struct.spex_summ_fit_function = fit_func
endif

; the indices returned will be the for the new function definitions
ind = fit_function_query(fit_func, /param_index)
nparams = fit_function_query(fit_func, /nparams)
ncomp = fit_function_query (fit_func, /ncomp)

params = struct.spex_summ_params
startpar = struct.spex_summ_starting_params
minima = struct.spex_summ_minima
maxima = struct.spex_summ_maxima
freemask = struct.spex_summ_free_mask
sigmas = struct.spex_summ_sigmas

func_spectrum = strarr(ncomp)
func_model = strarr(ncomp)
chianti_version = ''
abun_table = 'mewe_solar'

need_update = 0  ; if no thermal funcs, don't need to update structure
for i=0,ncomp-1 do begin

	if fit_comp_arr[i] eq 'vth' or $
	 fit_comp_arr[i] eq 'multi_therm_pow' or $
	 fit_comp_arr[i] eq 'multi_therm_exp' then begin

		need_update = 1

		func_model[i] = 'mewe'
		func_spectrum[i] = 'full'

		index = ind[i,1]
		params = array_insert(params, 1., index)
		startpar = array_insert(startpar, 1., index)
		minima = array_insert(minima, .01, index)
		maxima = array_insert(maxima, 10., index)
		freemask = array_insert(freemask, 0, index)
		sigmas = array_insert(sigmas, 0., index)

	endif

endfor

if count_noline gt 0 then func_spectrum[ind_noline] = 'continuum'

if need_update then begin
	struct = rep_tag_value (struct, params, 'spex_summ_params')
	struct = rep_tag_value (struct, startpar, 'spex_summ_starting_params')
	struct = rep_tag_value (struct, minima, 'spex_summ_minima')
	struct = rep_tag_value (struct, maxima, 'spex_summ_maxima')
	struct = rep_tag_value (struct, freemask, 'spex_summ_free_mask')
	struct = rep_tag_value (struct, sigmas, 'spex_summ_sigmas')
endif

; need to add these regardless of whether any component was thermal
struct = add_tag (struct, func_model, 'spex_summ_func_model')
struct = add_tag (struct, func_spectrum, 'spex_summ_func_spectrum')
struct = add_tag (struct, abun_table, 'spex_summ_abun_table')
struct = add_tag (struct, chianti_version, 'spex_summ_chianti_version')

end