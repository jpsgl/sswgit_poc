;+
;
; PROJECT:
; RHESSI
;
; NAME:
; read_soxs_4_ospex
;
; PURPOSE:
; Read soxs data and times from a file, compute drm, and return data and
;	associated values needed by OSPEX.
; This procedure is based on read_hessi_4_ospex.
;
; CATEGORY:
; SPEX
;
; CALLING SEQUENCE:
; read_soxs_4_ospex, FILES=files, data_str=data_str, ERR_CODE=err_code,
;                   ERR_MSG=err_msg, _REF_EXTRA=_ref_extra
;
; INPUTS:
;
; INPUT KEYWORDS:
; FILES - Name of file to read.
;
; OUTPUT KEYWORDS: structure data_str containing the values needed by OSPEX.
;
; OPTIONAL OUTPUTS:
;
; SIDE EFFECTS:
;
; RESTRICTIONS:
;
; PROCEDURE:
;
; EXAMPLE:
; Called from within SPEX to load soxs data
;
; Written: Kim Tolbert, June 2006
;
; Modification History:
; 6-Nov-2006, Kim.  Use detector name returned by soxs_readfile
; 13-feb-2008, richard.schwartz@nasa.gov
;	removed 3 lowest data bins on direction from R Jain. Maintained
;		channel gain for 56 keV over 256 channels(implemented in soxs_czt_drm)
;-
;------------------------------------------------------------------------------
PRO read_soxs_4_ospex, FILES=files, $
                        data_str=data_str, $
                        ERR_CODE=err_code, $
                        ERR_MSG=err_msg, $
                        CZT=czt, $ ;ras, 4-dec-2006
                        _REF_EXTRA=_ref_extra

data_str = -1

err_code = 0
err_msg = ''

respfile = ''

delta_light= 1.0

if files[0] eq '' then begin
    err_code=1
    err_msg = 'No spectrum file selected.'
    return
endif

atten_states = -1

; Read spectrum from the file.
soxs_readfile, files[0], data, ut_edges, czt=czt, detector=detector, err_code=err_code

IF err_code then begin
	err_msg = 'Error reading SOXS file ' + files[0]
	return
endif

; compute and return drm, energy edges, and area
; first check that env var. for directory for soxs response information is set
soxsresp_dir = getenv('SSWDB_SOXS')
if soxsresp_dir eq '' then setenv, 'SSWDB_SOXS=' + concat_dir ('$SSW_OSPEX', 'soxsresp')


; get rid of first two and last two channels in drm and energy edges,
;  just as we did in the data
if strupcase(detector) eq 'SI' then begin
	drm = soxs_drm(ein2=ct_edges, area=area)
	drm = drm[2:253, 2:253]
	ct_edges = ct_edges[*,2:253]
endif else begin
	;Separate czt drm based on Si methods

	drm = soxs_czt_drm(ein2=ct_edges, area=area, nchan=238)


	data = data[14:*,*] ;new data chan start on input for R Jain,, feb 13, 2008
endelse

nt = n_elements(data[0,*])
nen = n_elements(data[*,0])

;  despike data, using time bin width as scaling factor
ut_width = get_edges(ut_edges, /width)
data = array_despike (data, scale=transpose(rebin(ut_width, nt, nen)))

; livetime is width of time bin minus 16 microsec for each count in all energies
ltimes = transpose( rebin(ut_width - .000016 * total(data,1), nt, nen))

detused = detector

title = 'SOXS SPECTRUM'
units = ' s!u-1!n cm!u-2!n keV!u-1!n'

data_str = { $
    START_TIME: anytim(min(ut_edges),/vms), $
    END_TIME: anytim(max(ut_edges), /vms), $
    RCOUNTS: data, $
    ERCOUNTS: sqrt(data), $
    UT_EDGES: ut_edges, $
    UNITS: units, $
    AREA: area, $
    LTIME: ltimes, $
    CT_EDGES: ct_edges, $
    TITLE: title, $
    RESPFILE: drm, $

    detused: detused, $
    atten_states: atten_states }

END
