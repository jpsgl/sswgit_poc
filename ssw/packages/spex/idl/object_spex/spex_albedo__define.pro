;+
; Name: SPEX_ALBEDO__DEFINE
;
; Purpose: This object manages the Albedo correction to the Detector Response Matrix for OSPEX.
; 
; Method:  This object is stored as a property in the spex_drm object and is used to calculate a DRM that has
; just the albedo correction and apply that to the flux spectrum.
; Albedo depends on the angle, anisotropy and energy edges.  To save time, we store these
; values for the last drm we calculated and don't redo the calculations unless something changes.
; Note: this is not a framework object.
;
; Category: OSPEX
;
; Written: 21 April 2010, Kim Tolbert
;-

pro spex_albedo_test, theta=theta, anis=anis, phe=phe

o = obj_new('spex_albedo')
checkvar, phe, get_edges(findgen(1801)/3.+3., /edges_2)
checkvar, theta, 50.
checkvar, anis, 1. 

flux = f_vth_bpow(phe, [2.,2., 1, 3, 60, 4])

albedo_drm = o->getdata(theta=theta, anis=anis, ph_edges=phe)
;help, albedo_drm
new = o->apply(flux)

destroy,o
phm = get_edges(phe, /mean)
edge_products, phe, mean=phm, width=phw

linecolors
plot_oo, phm, flux , xran=[5,500]
oplot, phm, new, col=2
oplot, phm, phw, col=7
legend,['original flux', 'albedo-corrected flux', 'energy bin width'], color=[255,2,7], lines=0

end

;-----

function spex_albedo::init, _extra=_extra

self.ph_edges = ptr_new(/alloc)
self.saved_ph_edges = ptr_new(/alloc)
self.saved_ph_width = ptr_new(/alloc)
self.albedo_drm = ptr_new(/alloc)

if keyword_set(_extra) then self->set, _extra=_extra
return, 1
end

;-----

pro spex_albedo::cleanup
ptr_free, self.ph_edges
ptr_free, self.saved_ph_edges
ptr_free, self.saved_ph_width
ptr_free, self.albedo_drm
end

;-----

pro spex_albedo::set, theta=theta, anisotropy=anisotropy, ph_edges=ph_edges
if exist(theta) then self.theta = theta
if exist(anisotropy) then self.anis = anisotropy
if exist(ph_edges) then *self.ph_edges = ph_edges
end

;-----

function spex_albedo::getdata, _extra=_extra

if keyword_set(_extra) then self->set,_extra=_extra

; If theta, anis, and edges haven't changed, don't need to redo calculation, just 
; return saved drm
if ~(self.theta eq self.saved_theta) or $
 ~(self.anis eq self.saved_anis) or $
 ~same_data(*self.ph_edges, *self.saved_ph_edges) then begin
 
  ; Something changed.  Recalculate albedo drm.  Do this by creating a matrix of the right size
  ; with 1/energywidth along the diagonals.  Use that as input to the drm_correct_albedo and it
  ; will return a drm matrix with just the albedo correction.
;  print,'Calculating albedo drm.' , self.theta,self.anis
  edge_products, *self.ph_edges, mean=ph_mean, width=ph_width
  nen = n_elements(ph_mean)
  ;Build the albedo matrix
  idiag = lindgen(nen) * (nen+1)
  drm_unity = fltarr(nen, nen)
  drm_unity[idiag] = 1. / ph_width
  *self.albedo_drm = drm_correct_albedo(theta=self.theta, anisotropy=self.anis, ph_edges=*self.ph_edges, drm=drm_unity)
  self.saved_theta = self.theta
  self.saved_anis = self.anis
  *self.saved_ph_edges = *self.ph_edges
  *self.saved_ph_width = ph_width  

endif

return, *self.albedo_drm
end

;-----
; Get the albedo drm and apply the correction to the photon flux.  If the albedo drm couldn't
; be calculated it will be -1 - in that case just return the photon flux vector unchanged. (A 
; message should have been printed by drm_correct_albedo in that case.)

function spex_albedo::apply, yfit, _extra=_extra

; first set any parameters into the object
albedo_drm = self -> getdata(_extra=_extra)
;help,self
;pmm,albedo_drm   
; apply the albedo correction to the photon flux
return, albedo_drm[0] eq -1 ? yfit : albedo_drm # (yfit * (*self.saved_ph_width) )
end

;-----

pro spex_albedo__define
self = {spex_albedo, $
        theta: 0., $
        saved_theta: 0., $
        anis: 0., $
        saved_anis: 0., $
        ph_edges: ptr_new(), $
        saved_ph_edges: ptr_new(), $
        saved_ph_width: ptr_new(), $
        albedo_drm: ptr_new()}
end               