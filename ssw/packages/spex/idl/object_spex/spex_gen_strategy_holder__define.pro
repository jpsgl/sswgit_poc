;+
; Name: spex_gen_strategy_holder
;
; Purpose:  This is similar to the RHESSI strategy holder tools, except that here
;  we are adding unknown numbers (max=50) of instances of one object (passed to init in
;  strategy_object keyword), whereas in the other strategy holder, there is a pre-defined
;  set of unique strategies, and only one (or none) of each is instantiated.
;  This routine handles both cases - if self.strategy_object is just a blank string, then
;  just falls through to the regular strategy holder.
;  Here since we'll have multiple strategies of the same class, we need to create a unique
;  identifier for each - done in getnewname method.
;  Also different because here, we want to control the creation of the strategy objects
;  by only creating them when createstrategy is called.
;
; Modifications:
;-

FUNCTION spex_gen_strategy_holder::INIT, strategy_available, strategy_alternate_source, $
                 strategy_object=strategy_object, _EXTRA=_extra

ret = self ->strategy_holder_tools::init (strategy_available, strategy_alternate_source, _EXTRA=_extra)

if is_string(strategy_object) then self.strategy_object = strategy_object
self.strategy_available_index = ptr_new(-1)

return, ret
end

;----------------------------------------------------------

; if strategy_name isn't suppled, then must supply strategy_type and strategy_name will be
; generated and returned.  If don't supply idx, then it will find next free index to put
; this strategy in.

FUNCTION spex_gen_strategy_holder::CreateStrategy, idx, strategy_name=strategy_name, strategy_type=strategy_type

if self.strategy_object eq '' then $
	return, self -> strategy_holder_tools::createstrategy (idx)

; the alternate source stuff doesn't work in this version of strategy holder, so removed it.

source = self->Framework::Get( /SOURCE )

if not exist(idx) then idx = self -> getfreeindex()
if keyword_set(strategy_type) then strategy_name = self -> getnewname(strategy_type)

strategy = Obj_New( self.strategy_object , SOURCE=source )
IF self.debug GE 5 THEN BEGIN
    print, '*************************   creating ', self.strategy_object
ENDIF

self->Framework::Set, SOURCE = strategy, SRC_INDEX=idx+1, /DELETE_OLD_SOURCE

if (*self.strategy_available)[0] eq '' then begin
	*self.strategy_available = Strupcase(strategy_name)
	*self.strategy_available_index = idx
endif else begin
	*self.strategy_available = [*self.strategy_available, Strupcase(strategy_name)]
	*self.strategy_available_index = [*self.strategy_available_index, idx]
endelse

RETURN, strategy

END

;---------------------------------------------------------

; Function to retrieve an existing strategy by
;  if idx is passed - returns strategy in that index
;  if strategy_name is passed - finds strategy_available matching that name
;  if neither is passed, returns current strategy
;
; If strategy doesn't exist, we don't create a new one here.  This is different from
; strategy_holder_tools - there we have a defined set of strategies to create/get.  Here
; we are adding unknown numbers of instances of one object, so want to control
; creation of these object by only creating them when createstrategy is called.

FUNCTION spex_gen_strategy_holder::GetStrategy, idx, $
                 NO_COMPLAIN=no_complain, $
                 STRATEGY_NAME=strategy_name, $
                 STRATEGY_INDEX=strategy_index

if self.strategy_object eq '' then $
	return, self -> strategy_holder_tools::GetStrategy (idx, $
                 NO_COMPLAIN=no_complain, $
                 STRATEGY_NAME=strategy_name, $
                 STRATEGY_INDEX=strategy_index)

IF exist( STRATEGY_NAME ) THEN BEGIN
    ind = Where( *self.strategy_available EQ Strupcase( strategy_name ), count )
    IF count EQ 0 THEN strategy = -1 else begin
    	idx = (*self.strategy_available_index)[ind[0]]
    	strategy=self->Strategy_Holder_Tools::Get( /SOURCE, SRC_INDEX = idx+1 )
    endelse
ENDIF ELSE BEGIN
    CheckVar, idx, self.strategy_current
    strategy=self->Strategy_Holder_Tools::Get( /SOURCE, SRC_INDEX = idx+1 )
ENDELSE

if not obj_valid( strategy ) then begin
	if not keyword_set( no_complain ) then begin
		id = exist(strategy_name) ? strategy_name : 'index=' + trim(idx)
    	Message, 'This strategy is unavailable: ' + id, /continue
    endif
    return, -1
endif

IF Keyword_Set( STRATEGY_INDEX ) THEN BEGIN
    RETURN, idx
ENDIF ELSE BEGIN
    RETURN, strategy
ENDELSE

END

;----------------------------------------------------------

function spex_gen_strategy_holder::getfreeindex

; we'll never have more than 50 strategies (?).  Find lowest number between
; 0 and 50 that's not being used so far.

if (*self.strategy_available_index)[0] eq -1 then return, 0 else $
	return, (rem_elem (indgen(50), *self.strategy_available_index)) [0]

end

;----------------------------------------------------------

; strategy names are formed by appending a unique identifier to the strategy_type
; For example, VTH_0, VTH_1 would be the strategy names for two instance if type VTH
function spex_gen_strategy_holder::getnewname, strategy_type

if (*self.strategy_available)[0] eq '' then return, strategy_type + '#0'

avail = ssw_strsplit ( (*self.strategy_available), '#', /head)
num = fix(ssw_strsplit ( (*self.strategy_available), '#', /tail))

q = where (avail eq strupcase(strategy_type), count)
if count eq 0 then return, strategy_type + '#0'

return, strategy_type + '#' + trim( (rem_elem(indgen(50), num[q]))[0] )

end

;----------------------------------------------------------

pro spex_gen_strategy_holder::deletestrategy, strategy_name

if keyword_set(strategy_name) then begin
	ind = Where( *self.strategy_available EQ Strupcase( strategy_name ), count )
	if count eq 0 then begin
		message, 'This strategy is not there to delete: ' + strategy_name, /continue
		return
	endif

	strategy = self -> spex_gen_strategy_holder::getstrategy(strategy_name=strategy_name)
	obj_destroy, strategy

	remaining = rem_elem(*self.strategy_available, Strupcase(strategy_name))
	*self.strategy_available = remaining[0] eq -1 ? '' : (*self.strategy_available)[remaining]
	*self.strategy_available_index = remaining[0] eq -1 ? -1 : (*self.strategy_available_index)[remaining]
	self.strategy_current = 0

endif else begin
	if (*self.strategy_available)[0] eq '' then return
	; if no strategy_name supplied, delete all strategies
	strats = *self.strategy_available
	for i=0,n_elements(strats)-1 do $
		self->deletestrategy, strats[i]
endelse

end

;----------------------------------------------------------

; Need to have this here, even though it's mostly the same as the one in strategy_holder_tools
; because that one explictly calls strategy_holder_tools::getstrategy, and we need to call this
; getstrategy.

PRO spex_gen_strategy_holder::SetStrategy, strategy

; strategy can be set either by name or diretly by index
IF Size( strategy, /TYPE ) EQ 7 THEN BEGIN
    idx = self->spex_gen_strategy_holder::GetStrategy( STRATEGY_NAME=strategy, /STRATEGY_INDEX )
ENDIF ELSE BEGIN
    idx =  strategy
ENDELSE

self.strategy_current = idx
dummy =  self->spex_gen_strategy_holder::GetStrategy( idx )

END

;----------------------------------------------------------

function spex_gen_strategy_holder::need_update, _extra = _extra

; we need to redefine need_update, because it needs to be passed to the
; strategy class.  This is different from spex_strategy_holder_tools,
; because here we return a 1 if self or ANY of the strategies has
; need_update set to 1.  In spex_strategy_holder_tools, it only looks in
; the current strategy.

if not self.get_all_strategies then $
	return, self ->strategy_holder_tools::need_update(_extra=_extra)

strat = *self.strategy_available
if strat[0] eq '' then return, 0
need_update = self->get(/need_update)
for i=0,n_elements(strat)-1 do begin
	strategy = self->getstrategy(strategy_name=strat[i])
	need_update = need_update or strategy->need_update()
endfor
return, need_update

end

;----------------------------------------------------------

; this method is really just to help while debugging - lists information about the stored strategies.
PRO spex_gen_strategy_holder::list

strat = *self.strategy_available
if strat[0] eq '' then print,'No strategies available.' else begin
	for i = 0,n_elements(strat) -1 do begin
		strategy = self->getstrategy(strategy_name=strat[i])
		print, ''
		print, trim(i), $
			'  Strategy name: ', strat[i], '  Strategy index: ', trim((*self.strategy_available_index)[i])
		print, ' Object variable: ', strategy, '  Object valid: ',obj_valid(strategy), $
			'  Need udpate: ', strategy->get(/need_update)
	endfor
	print,''
	print,'Current strategy index = ', trim(self.strategy_current)
endelse

end

;----------------------------------------------------------

pro spex_gen_strategy_holder__define

  dummy =  {spex_gen_strategy_holder, $
  			strategy_object: '', $
  			strategy_available_index: ptr_new(), $
            inherits strategy_holder_tools,  $
            inherits spex_gen}

end
