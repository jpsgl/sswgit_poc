function messenger_fwhm, e, par_fwhm

default, par_fwhm,  0.59 ;keV
return, e*0.0 + par_fwhm[0]
end

;+
;
; NAME:
;	messenger_use_resp_calc
;
; PURPOSE:
;	This procedure computes the X-ray response of the messenger P-I-N SI detector to a solar flux.
;	Because it is designed to work with the generic SPEX
; CATEGORY:
;	messenger, SPEX
;
; CALLING SEQUENCE:
;	drm = messenger_use_resp_calc(  edges_out=edges_out, edges_in=edges_in, area=area,  $
;	 ehi=ehi, elo=elo,  fwhm=fwhm, dead_layer_microns=dead_layer_microns, $
;	 coef=coef, offset=offset, gain=gain, nflux=nflux)
; CALLS:
;	RESP_CALC, XSEC, messenger_PIN_CAL, F_DIV, DATATYPE, PULSE_SPREAD
;
; KEYWORDS:
;	EDGES_OUT- Energy channel edges in keV. 2xn
;	EDGES_IN - Photon input energies for DRM in keV. 2xn
;		supercedes elo, ehi, and nflux computation if present
;	ELO - lower bound of photon energies used to compute edges_in, keV, default 1.03 keV
;	EHI - upper bound of photon energies used to compute edges_in, keV, default 13 keV
;	ZOUT - channel bin array for output
;	Nflux - number of bins from elo to ehi, linear steps, defaults to step size to
;		match gain coefficient
;	COEF - default [[0.863173],[0.0375294]] nominal channel coefficients
;		where coef[0] is the low energy bound and coef[1] is the keV per channel
;		used in a first-order polynomial
;	dead_layer_microns - of Si on the detector front surface
;	FWHM - resolution broadening, fixed with energy, in keV, default 0.59 keV
;	Area - detector area in cm^2, default .000314 cm^2
;	Offset - energy shift in keV
;
;
;
; OPTIONAL INPUTS:
;	none
;
; OUTPUTS:
;	DRM - detector response, counts/sec/cm2/keV per photon/keV/cm2.
;
; OPTIONAL OUTPUTS:
;	none
;
; COMMON BLOCKS:
;	none
;
; SIDE EFFECTS:
;	none
;
; RESTRICTIONS:
;	none
;
; PROCEDURE:
;	none
;
; MODIFICATION HISTORY:
;	17-feb-2014, richard.schwartz@nasa.gov, wraps resp_calc, based on params in messenger_drm
;	21-feb-2014, kim.tolbert@nasa.gov. Fixed error in zout calculation (use 231 not 232)
;-

function messenger_use_resp_calc, edges_out=edges_out, edges_in=edges_in, area=area,  $
 ehi=ehi, elo=elo,  fwhm=fwhm, dead_layer_microns=dead_layer_microns, $
 coef=coef, offset=offset, gain=gain, nflux=nflux, zout = zout


default, ehi, 11.0 ;keV
default, elo, 1.03259972 ;keV, also used to trim the edges
detector='SI

func='messenger_fwhm
default, fwhm, .590
func_par=[fwhm,fltarr(3)]


default, area, .000314 ; cm^2 - area of aperture, requires "perfect" pointing
default, dead_layer_microns, 0.2 ;200 nanometers
default, coef,  [[0.863173],[0.0375294]] ;nominal

default, offset, 0.00 ;empirical correction in keV
default, gain, coef[1]
coef[1] = gain ;allow possibility of modifying the gain
nout    = exist( edges_out ) ? n_elements( get_edges( edges_out, /width) ) : 0
edges_out= offset + get_edges( poly( findgen(232)-0.5, coef), /edges_2)
default, nflux, [ehi-elo] / coef[1] + 1
default, edges_in, edges_out[*, where(edges_out[0,*] gt elo)] ;get_edges( interpol( [elo, ehi], nflux[0] ), /edges_2)


d = 0.05 ;cm


z=[4,14]
gmcm=[.00762*1.847, dead_layer_microns*1e-4*2.33]
resp_area = 1.0
defsysv, '!xsec_emin', value, exist= exist_emin
value = exist_emin ? !xsec_emin : 1.0 ;to keep xsec from breaking at energy lt 1 keV
value = value < edges_out[0]
defsysv, '!xsec_emin', value
; kim changed next line to 231-nout (was 232-nout)
zout = nout gt 0 ? lindgen(nout) + 231-nout : where( edges_out[0,*] ge elo and edges_out[1,*] le ehi)
;zin  = where( edges_in[0,*] ge elo and edges_in[1,*] le ehi<max(edges_out))
edges_out = edges_out[*, zout ]
;edges_in  = edges_in [*, zin  ]
resp_calc, detector, resp_area, func, func_par, d, z, gmcm, nflux, elo, ehi,  $;inputs
	energy_loss_mtrx, pulse_height_mtrx, ein, drm,		   $;outputs
	verbose=verbose, $
	edges_in=edges_in, $
	edges_out = edges_out

return, drm
end
