;+
; Name: spex_drm_info__define
;
; Purpose: Defines info structure for spex_drm object
;
; Category: OSPEX
;
; Modifications:
;	15-Jul-2004, Kim.  Added spex_drm_filter
;	9-Feb-2006, Kim.  Added spex_drm_method
;   27-Jun-2006, Kim.  Added spex_drm_mod_obj
;-

pro spex_drm_info__define

struct = {spex_drm_info, $
	spex_drm_data_name: '', $	; Data type DRM file is for, e.g. HESSI
	spex_drm_area: 0.0, $	; Detector area from DRM file
	spex_drm_ct_edges: ptr_new(), $	; Count space energy edges in DRM file (2,n)
	spex_drm_ph_edges: ptr_new(), $	; Photon space energy edges in DRM file (2,n)
	spex_drm_sepdets: 0, $	; If set, detectors are separate in DRM file
	spex_drm_filter: ptr_new(), $	; Filter state(s) in DRM file
	spex_drm_current_filter: 0, $ 	; Current filter in use
	spex_drm_detused: '', $	; String of detectors included in DRMfile
	spex_drm_method: '', $	; Method used to get drm values: 'file', 'array', or 'build'
	spex_drm_mod_obj: obj_new() $	; drm_mod object for determining RHESSI DRM params
	 }
end