; 08-Jan-2010, Kim.  Added spex_bk_used_ratio
pro spex_bk_info__define

struct = {spex_bk_info, $
	spex_bk_origunits: {spex_units}, $	; Structure with original units for background data
	spex_bk_units: {spex_units}, $	; Structure with units of last accumulation for background data
	spex_bk_used_ratio: 0 $         ; 0/1 means we didn't / did use the ratio method for bk calc
	}
end