;+
; Name: spex_xalbedo
;
; Purpose: Widget to set albedo options in ospex
;
; CATEGORY:  SPEX
;
; Calling sequence:  o->xalbedo
;
; Input arguments:
;   group - widget id of calling widget
;
; Output: the changed values are set in the ospex object
;
;
; Written:  Kim, 8-Sep-2004
; Modifications:
; 16-Mar-2007, Kim.  Made wording of isotropy option clearer
; 10-Jul-2008, Kim. Change hsi_ui_getfont to get_font (which is in ssw gen)
;-

;============================================================================

pro spex::xalbedo_update_widget, wbase

widget_control, wbase, get_uvalue=state

correct = state.vals.correct
widget_control, state.w_correct, set_button=correct

widget_control, state.w_angval, set_value=state.vals.angle
widget_control, state.w_xyval, set_value=state.vals.xy
widget_control, state.w_angval, sensitive=state.angsel
widget_control, state.w_xyval, sensitive=(state.angsel eq 0)

widget_control, state.w_aniso, set_value=state.vals.aniso

widget_control, state.w_angbase, sensitive=correct
widget_control, state.w_aniso, sensitive=correct


end

;-----
; Event handlers can't be object methods, so this is just a wrapper to get to the method

pro xalbedo_event, event
widget_control, event.top, get_uvalue=state
if obj_valid(state.object) then state.object -> xalbedo_event, event
return & end

;-----

pro spex::xalbedo_event, event

widget_control, event.top, get_uvalue=state

widget_control, event.id, get_uvalue=uvalue

if not exist(uvalue) then return

exit = 0

case uvalue of
	'angsel':  state.angsel = 1
	'xysel':   state.angsel = 0
	'angval':  state.vals.angle = event.value
	'xyval':   state.vals.xy = event.value
	'aniso':   state.vals.aniso = event.value
	'correct': state.vals.correct = event.select
	'cancel':  exit = 1
	'accept':  begin
		exit = 1
		self -> set, spex_albedo_correct = state.vals.correct, $
			spex_anisotropy = state.vals.aniso
		if state.angsel then begin
			; don't set angle unless it's changed because it wipes out xy vals
			; stored in object.
			if self->get(/spex_source_angle) ne state.vals.angle then $
				self -> set,spex_source_angle=state.vals.angle
		endif else self -> set, spex_source_xy=state.vals.xy
		end
	else:
endcase

if exit then widget_control, event.top, /destroy else $
	widget_control, event.top, set_uvalue=state

if xalive (state.wbase) then state.object -> xalbedo_update_widget, state.wbase

end

;-----

; Main routine

pro spex::xalbedo,  group_leader=group

drm_obj = self -> get(/obj, class='spex_drm')
vals = {correct: drm_obj -> get(/spex_albedo_correct, /this_class_only), $
	angle: drm_obj -> get(/spex_source_angle, /this_class_only), $
	xy: drm_obj -> get(/spex_source_xy, /this_class_only), $
	aniso: drm_obj -> get(/spex_anisotropy, /this_class_only) }

wbase = widget_base ( group_leader=group, $
					/column, $
					xpad=10, $
					ypad=20, $
					space=20, $
					title='Albedo Correction Parameters', $
					/frame )

get_font, font, big_font=big_font

tmp = widget_label (wbase, value='Albedo Correction', $
	font=big_font, /align_center)
	
	
wbase_label = widget_base(wbase, xpad=2, ypad=2, /column, /frame)	
tmp = widget_label(wbase_label, value='NOTE: Change on May 11, 2010', $
  font=big_font, /align_center)
tmp = widget_label(wbase_label, value='You can now add an albedo function component and fit on', $
  font=font, /align_center)      
tmp = widget_label(wbase_label, value=' the anisotropy value.  If you enable albedo here AND add the function', $
  font=font, /align_center)  
tmp = widget_label(wbase_label, value=' component, the function component takes precedence.', $
  font=font, /align_center) 
  
wbase2 = widget_base (wbase, /frame, /column, space=10)
w_correctbase = widget_base (wbase2, /nonexclusive)
w_correct = widget_button (w_correctbase, value='Enable Albedo Correction', uvalue='correct')

w_angbase = widget_base (wbase2, /column, /frame)

w_angrow1 = widget_base (w_angbase, /row)
tmp = widget_label (w_angrow1, value='Specify Flare Angle by ')
w_xyangbut = widget_base (w_angrow1, /exclusive, /row)
w_ang = widget_button (w_xyangbut, value='Angle', uvalue='angsel', /no_release)
w_xy = widget_button (w_xyangbut, value='X,Y Position', uvalue='xysel', /no_release)
widget_control, w_xy, set_button=1

w_angval  = cw_edroplist ( w_angbase, $
					label='Angle (degrees): ', $
					value=0., $
					format='(f7.2)', $
					xsize=10, $
					drop_values =  findgen(19) * 5., $
					uvalue='angval' )

w_xyval = cw_range ( w_angbase, $
					uvalue='xyval', $
					label1='XY Offset (arcsec)   X: ', $
					label2=' Y: ', $
					value=[0.,0.], $
					format='(f12.2)', $
					dropvals1=findgen(39)*50. - 950., $
					dropvals2=findgen(39)*50. - 950., $
					xsize=6, $
					xpad=0, $
					ypad=0, $
					space=0 )

w_aniso = cw_edroplist (wbase2, $
					label='Anisotropy (Ratio of up to down flux, 1=isotropic):  ', $
					value=1., $
					format='(f5.2)', $
					xsize=6, $
					drop_values=[findgen(10)*.1, findgen(6)+1.], $
					uvalue='aniso')

w_button_base = widget_base ( wbase2, 	/row, 	/align_center, 	space=15 )

tmp = widget_button (w_button_base, $
					value='Cancel', $
					uvalue='cancel')

tmp = widget_button (w_button_base, $
					value='Accept', $
					uvalue='accept' )

state = { wbase: wbase, $
			w_correct: w_correct, $
			w_angbase: w_angbase, $
			w_ang: w_ang, $
			w_xy: w_xy, $
			w_angval: w_angval, $
			w_xyval: w_xyval, $
			w_aniso: w_aniso, $
			vals: vals, $
			angsel: 0, $
			orig_vals: vals, $
			object: self }

widget_control, wbase, set_uvalue=state

self -> xalbedo_update_widget, wbase

if xalive(group) then begin
	widget_offset, group, xoffset, yoffset, newbase=wbase
	widget_control, wbase, xoffset=xoffset, yoffset=yoffset
endif

widget_control, wbase, /realize

xmanager, 'xalbedo', wbase, no_block=0

end