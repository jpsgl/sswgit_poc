;+
; Name: spex__xbk
; Purpose: Widget interface to background selection in OSPEX
;
; Written, Kim Tolbert 2002
; Modifications:
;   11-Apr-2005, Kim.  Use plot_time,/data instead of plot, class=spex_data, and
;     added /show_filter on time plots
;	15-Mar-2006, Kim. added gui_label to put on title bar of spex widget interfaces
;	18-Sep-2006, Kim. Added bk_ratio option with smoothing width
;	10-Jul-2008, Kim. Change hsi_ui_getfont to get_font (which is in ssw gen)
; 03-Dec-2008, Kim. Don't draw new plot for defining intervals if current plot is a utplot
; 15-Dec-2008, Kim.  Added exponential fit for background (order=4 means exp).  Also,
;     if current plot is a utplot, don't redraw when showing bk intervals on plot
; 30-Jul-2013, Kim.  Added bk_ratio plot high eband profile button.  Added Error buttons to show errors 
;     on each eband time profile or spectrum, or overall time plot option at bottom of widget. 
; 05-Sep-2013, Kim. Added ratio option to order droplist when use_bk_ratio is selected. Now user
;   can use ratio method for some ebands, but not others.
;
;-

pro spex::xbk_update_widget, wbase

widget_control, wbase, get_uvalue=state

bk_sep = self -> get(/spex_bk_sep)
widget_control, state.w_sep, set_button=bk_sep

bk_ratio = self -> get(/spex_bk_ratio)
widget_control, state.w_ratio, set_button=bk_ratio

widget_control, state.w_sm_width, set_value=self -> get(/spex_bk_sm_width)
widget_control, state.w_ratplot, set_button=self->get(/spex_bk_ratprofile_plot)

widget_control, state.w_ratio, sensitive=bk_sep
widget_control, state.w_sm_width, sensitive=bk_ratio and bk_sep
widget_control, state.w_ratplot, sensitive=bk_ratio and bk_sep
widget_control, state.w_bands_base1, sensitive=(bk_sep eq 1)
widget_control, state.w_bands_base2, sensitive=(bk_sep eq 1)

bands = self -> get(/spex_bk_eband)
nbands = bands[0] eq -1 ? 0 : n_elements(bands[0,*])
s_bands = bands[0] eq -1 ? strpad('None',20) : format_intervals(bands, format='(f9.1)')
widget_control, state.w_nbands, set_value='# Bands: ' + strtrim(nbands, 2)
widget_control, state.w_bands, set_value='  ' + s_bands + '  '

order_options = bk_ratio ? state.bk_order_options_ratio : state.bk_order_options_noratio

nsep = bk_sep ? nbands : 1
; if nsep hasn't changed, don't need to recreate the whole table
if nsep ne state.nsep_widget then begin
	widget_control, state.wbase1, update=0
	widget_control, state.wbase2, /destroy
	state.wbase2 = widget_base (state.wbase1, /column)
	widget_control, /hourglass
	for ii=0,nsep-1 do begin
		state.table[ii].w_band_base = widget_base (state.wbase2, /column, /frame)
		w_time_base0 = widget_base (state.table[ii].w_band_base, /row, space=10)
		val = bk_sep ? format_intervals(bands[*,ii], format='(f9.1)') + ' keV' : 'For all Energies: '
		state.table[ii].w_elabel = widget_label (w_time_base0, value=val)
		state.table[ii].w_times = widget_droplist (w_time_base0, title='BK Times: ', $
							value='xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', uvalue='none')
		state.table[ii].w_ntimes = widget_label (w_time_base0, value='xxxxxxxxxxxxx', /dynamic_resize)
		state.table[ii].w_order = widget_droplist ( w_time_base0, value=order_options, $
							title='Function: ', uvalue='order')

		w_time_base1 = widget_base (state.table[ii].w_band_base, /row, space=10)
		tmp = widget_label (w_time_base1, value='BK Time Intervals: ')
		state.table[ii].w_deleteall = widget_button (w_time_base1, value='Delete', uvalue='delete_times')
		state.table[ii].w_change = widget_button (w_time_base1, value='Change', uvalue='change_times')
		state.table[ii].w_show = widget_button (w_time_base1, value='Show', uvalue='draw_times')
		state.table[ii].w_plotspec = widget_button (w_time_base1, value='Plot Spectrum', uvalue='plot_spec')
		state.table[ii].w_plottime=widget_button (w_time_base1, value='Plot vs Time', uvalue='plot_time')
		w_time_base_1a = widget_base (w_time_base1, /row, /nonexclusive, space=2)
		state.table[ii].w_show_error = widget_button (w_time_base_1a, value='Error', uvalue='show_err') 
	endfor
	;widget_control, state.wbase1, update=1
	state.nsep_widget = nsep
	widget_control, wbase, set_uvalue=state

	; The separate band part of this widget can put up a scroll bar if there are lots
	; of bands and it gets too big.
	; X and Win behave differently.
	; On X, when add new band inside wbase1, size of wbase1 doesn't change, so use size of wbase2, and
	;  puts scroll bar in x too, unless allow x size to expand to what it needs.
	; On Win, when add new bands inside of wbase1, wbase1 grows accordingly, so have to check
	;   its size and reduce it if necessary.
	; In either case, we won't let size of band part be larger than .5 of y screensize (to leave
	; room for buttons on top and bottom)
	device, get_screen_size=scrsize
	max_ysize = .4*scrsize[1]

	if !d.name eq 'X' then begin
		base2_geom = widget_info(state.wbase2,/geometry)
		bandbase_geom = widget_info(state.table[0].w_band_base, /geometry)
		widget_control, state.wbase1, ysize = (base2_geom.ysize < max_ysize), xsize=bandbase_geom.xsize
	endif else begin
		base1_geom = widget_info(state.wbase1, /geometry)
		widget_control, state.wbase1, ysize=(base1_geom.ysize < max_ysize)
	endelse

endif

widget_control, state.wbase1, update=0
for ii=0,nsep-1 do begin
	val = bk_sep ? format_intervals(bands[*,ii], format='(f9.1)') + ' keV' : 'For all Energies: '
	widget_control, state.table[ii].w_elabel, set_value=val
	times = self -> get(this_band=ii, /this_time)
	ntimes = times[0] eq -1 ? 0 : n_elements(times[0,*])
	s_times = times[0] eq -1 ? 'None' : format_intervals(times, /ut)
	widget_control, state.table[ii].w_ntimes, set_value='# Times: ' + strtrim(ntimes, 2)
	widget_control, state.table[ii].w_times, set_value='  ' + s_times + '  '
	widget_control, state.table[ii].w_order, set_value=order_options
	widget_control, state.table[ii].w_order, set_droplist_select=self->get(this_band=ii, /this_order);, $
;		sensitive=(bk_ratio and bk_sep) eq 0
endfor
widget_control, state.wbase1, update=1

end

;-----
; Event handlers can't be object methods, so this is just a wrapper to get to the method

pro xbk_event,event
widget_control,event.top,get_uvalue=state
if obj_valid(state.object) then state.object->xbk_event,event
return & end

;-----
; xbk event handler

pro spex::xbk_event, event

widget_control, event.top, get_uvalue=state

widget_control, event.id, get_uvalue=uvalue

if not exist(uvalue) then return

units = state.units[widget_info(state.w_units, /droplist_select)]

widget_control, state.w_int_sel, get_value=full_options

case uvalue of

	'sep': self -> set, spex_bk_sep = event.select

	'ratio': self -> set, spex_bk_ratio = event.select

	'sm_width': self -> set, spex_bk_sm_width = event.value
	
	'ratplot': self -> set, spex_bk_ratprofile_plot=event.select

	'change_ebands': self -> intervals, /bk_eband, full_options=full_options

	'spex_eband': self -> set, spex_bk_eband = self -> get(/spex_eband)

	'draw_ebands': begin
		self -> plotman, /pl_energy, class='spex_data', spex_units=units
		intervals = self -> get(/spex_bk_eband)
		plotman_obj = self -> get_plotman_obj()
		if intervals[0] ne -1 then $
			plotman_draw_int,'all',{plotman_obj:plotman_obj}, intervals=intervals, type='Background'
		end

	'loop': self -> intervals, /bk_time, full_options=full_options, /loop

	'deleteall': self -> set, spex_bk_time_interval=-1

	'change_times': begin
		ib = (where (state.table.w_change eq event.id, count))[0]
		;plot_choice = state.intplot_choices[widget_info(state.w_bkint_plot, /droplist_select)]
		;if strpos(plot_choice, 'Standard') ne -1 then self -> plotman, class_name='spex_data'
		bands = self -> get(/spex_bk_eband)
		if self->get(/spex_bk_sep) then interval=bands[*,ib]
		;self -> plotman, class_name='spex_data', interval=interval, spex_units=units
		; if current plot is a utplot, use it, otherwise draw full data plot
		if not (self->get_plotman_obj())->valid_window(/utplot) then $
		   self -> plot_time, /data, interval=interval, spex_units=units, /show_filter
		self -> intervals, /bk_time, full_options=full_options, this_band=ib
		end

	'delete_times': begin
		ib = (where (state.table.w_deleteall eq event.id, count))[0]
		self -> set, this_band=ib, this_time=-1
		end

	'draw_times': begin
;		if self->valid_plot(/utplot) then begin
;			plot_choice = state.intplot_choices[widget_info(state.w_bkint_plot, /droplist_select)]
;			if strpos(plot_choice, 'Standard') ne -1 then self -> plotman, class_name='spex_data'
;		endif else self -> plotman, class='spex_data'
    ib = (where (state.table.w_show eq event.id, count))[0]
    plotman_obj = self -> get_plotman_obj()
    if not plotman_obj->valid_window(/utplot) then begin
	  	;self -> plotman, class='spex_data', spex_units=units  ; always replot, in case we deleted intervals
	  	if self->get(/spex_bk_sep) then plotbands = self -> get(/spex_bk_eband)
	  	self -> plot_time, /data, spex_units=units, /show_filter, interval=plotbands
	  endif
	  intervals = self -> get(this_band=ib, /this_time)
		if intervals[0] ne -1 then $
			plotman_draw_int,'all',{plotman_obj:plotman_obj}, intervals=intervals, type='Background'
		end

	'plot_spec': begin
		ib = (where (state.table.w_plotspec eq event.id, count))[0]
		show_err = widget_info(state.table[ib].w_show_error, /button_set)
		self -> plotman, class='spex_bkint', this_band=ib, spex_units=units, show_err=show_err
		end

	'plot_time': begin
		ib = (where (state.table.w_plottime eq event.id, count))[0]
		
		; tried to change this to use oplot_xmarkers, but since /show_filter makes it use addplot for that,
		; this doesn't work.  If don't use /show_filter, then still doesn't work because addplot.. are in 
		; extra this way, not plot_params (in plot_time)
;		intervals = self -> get(this_band=ib, /this_time)
;		
;		self -> plot_time, /data, /bksub, /bkdata, /bkint, this_band=ib, spex_units=units, /show_filter, $
;		   addplot_name='oplot_xmarkers', addplot_args={intervals:intervals, ut: 1}

    show_err = widget_info(state.table[ib].w_show_error, /button_set)
    self -> plot_time, /data, /bksub, /bkdata, /bkint, this_band=ib, spex_units=units, /show_filter, show_err=show_err

		intervals = self -> get(this_band=ib, /this_time)
		plotman_obj = self -> get_plotman_obj()
		if intervals[0] ne -1 then begin
			plotman_obj -> select
			utbase = plotman_obj -> get(/utbase)
			intervals = intervals - utbase
			for ir = 0,n_elements(intervals)/2. -1 do begin
				oplot, [intervals[0,ir],intervals[0,ir]], crange('y'), psym=0, linestyle=1
				oplot, [intervals[1,ir],intervals[1,ir]], crange('y'), psym=0, linestyle=2
				empty	; empty plot buffer.  On linux, last line wasn't getting drawn.
			endfor
			plotman_obj -> unselect
		endif

		end

	'order': begin
		ib = (where (state.table.w_order eq event.id, count))[0]
		self -> set, this_band=ib, this_order=event.index
		end

	'timeplot': begin
		data = widget_info(state.w_showdata, /button_set)
		bkdata = widget_info(state.w_showbk, /button_set)
		bksub = widget_info(state.w_showbksub, /button_set)
		show_err = widget_info(state.w_show_err, /button_set)
		self -> plot_time, data=data, bkdata=bkdata, bksub=bksub, spex_units=units, show_err=show_err, /show_filter
		end

	'refresh':  ; it will refresh after case statement

	'exit': widget_control, state.wbase, /destroy

	else:
endcase

if xalive (state.wbase) then self -> xbk_update_widget, state.wbase
end

;-----

pro spex::xbk, group=group, gui_label=gui_label

if xregistered ('xbk') then begin
	xshow,'xbk', /name
	return
endif

checkvar, gui_label, ''

get_font, font, big_font=big_font

bk_order_options_noratio = ['0Poly','1Poly','2Poly','3Poly','Exp']
bk_order_options_ratio = ['0Poly','1Poly','2Poly','3Poly','Exp', 'Ratio']

wbase = widget_base (group=group, title='SPEX Background Options '+gui_label, /column, space=2, xpad=4, ypad=4)

tmp = widget_label (wbase, value='Select Background', $
	font=big_font, /align_center)

w_intbase = widget_base (wbase, /row, /frame, space=10)
w_int_sel = cw_bgroup (w_intbase, ['Graphical', 'Full Options'], /row, $
	/exclusive, label_left='Interval Selection: ', $
	/return_index, uvalue='None', /no_release, set_value=0)

units = ['Counts', 'Rate', 'Flux']
w_units = widget_droplist (w_int_sel, title='Plot Units: ', $
	value=units, uvalue='none')
widget_control, w_units, set_droplist_select=2

;intplot_choices = ['Current plot', 'Standard Bk eband or interval plot']
;w_bkint_plot = widget_droplist(w_intbase, title='Use:', value=intplot_choices, uvalue='none', /align_center)


w_bands_base = widget_base (wbase, /column, /frame)

w_bands_base0 = widget_base (w_bands_base, /row, space=0)
w_sep_base = widget_base (w_bands_base0, /nonexclusive)
w_sep = widget_button (w_sep_base, $
					value='Separate BK for each energy band', $
					uvalue='sep')
w_ratio_base = widget_base (w_bands_base0, /nonexclusive)
w_ratio = widget_button (w_ratio_base, $
					value='Use ratio to hi band', $
					uvalue='ratio')
w_sm_width = cw_edroplist (w_bands_base0, value=128, format='(i6)', $
	xsize=5, label='Half Smoothing width (#pts): ', drop_values=[0,1,2^(indgen(7)+2)], $
	uvalue='sm_width')
w_ratplot_base = widget_base (w_bands_base0, /nonexclusive)
w_ratplot = widget_button (w_ratplot_base, $
          value='Show hi band profile', $
          uvalue='ratplot')	

w_bands_base1 = widget_base (w_bands_base, /row, space=10)
w_bands = widget_droplist (w_bands_base1, title='Current energy bands:', $
					value='xxxxxxxxxxxxxxxxxxxxx', uvalue='none')
w_nbands = widget_label (w_bands_base1, value='', /dynamic_resize)
tmp = widget_button (w_bands_base1, value='Change', uvalue='change_ebands')
tmp = widget_button (w_bands_base1, value='Set to spex_eband', uvalue='spex_eband')
tmp = widget_button (w_bands_base1, value='Show', uvalue='draw_ebands')
w_bands_base2 = widget_base (w_bands_base, /row, space=10)
tmp = widget_button (w_bands_base2, value='Loop to Set Times', uvalue='loop')
tmp = widget_button (w_bands_base2, value='Delete all Times', uvalue='deleteall')

wbase1 = widget_base (wbase, /column, /frame, /scroll)
wbase2 = widget_base (wbase1, /column)

w_plot_base = widget_base (wbase, /column, /frame, space=0)

w_plot_base1 = widget_base (w_plot_base, /row, space=10)
tmp = widget_label (w_plot_base1, value='Time Profile Plots in spex_ebands: ')
w_plot_base11 = widget_base (w_plot_base1, /row, /nonexclusive, space=2)
w_showdata = widget_button (w_plot_base11, value='Data', uvalue='showdata')
w_showbk = widget_button (w_plot_base11, value='Background', uvalue='showbk')
w_showbksub = widget_button (w_plot_base11, value='Data-Background', uvalue='showbksub')
w_show_err = widget_button (w_plot_base11, value='Error', uvalue='show_err')
tmp = widget_button (w_plot_base1, value='Plot', uvalue='timeplot')


wbase_but2 = widget_base (wbase, /row, space=20, /align_center)
tmp = widget_button (wbase_but2, value='Refresh', uvalue='refresh')
tmp = widget_button (wbase_but2, value='Close', uvalue='exit')

table = replicate ( $
	{w_band_base: 0L, $
	w_elabel: 0L, $
	w_times: 0L, $
	w_ntimes: 0L, $
	w_order: 0L, $
	w_deleteall: 0L, $
	w_change: 0L, $
	w_show: 0L, $
	w_plotspec: 0L, $
	w_plottime: 0L, $
	w_show_error: 0L}, 20 )

state = {wbase: wbase, $
  bk_order_options_noratio: bk_order_options_noratio, $
  bk_order_options_ratio: bk_order_options_ratio, $
	w_int_sel: w_int_sel, $
	w_sep: w_sep, $
	w_ratio: w_ratio, $
	w_sm_width: w_sm_width, $
	w_ratplot: w_ratplot, $
	w_bands_base1: w_bands_base1, $
	w_bands_base2: w_bands_base2, $
	w_nbands: w_nbands, $
	w_bands: w_bands, $
	wbase1: wbase1, $
	wbase2: wbase2, $
	table: table, $
	nsep_widget: 0, $
	;intplot_choices: intplot_choices, $
	;w_bkint_plot: w_bkint_plot, $
	w_units: w_units, $
	units: units, $
	w_showdata: w_showdata, $
	w_showbk: w_showbk, $
	w_showbksub: w_showbksub, $
	w_show_err: w_show_err, $
	object: self, $
	gui_label: gui_label }

widget_control, wbase, set_uvalue=state

widget_offset, group, newbase=wbase, xoffset, yoffset

widget_control, wbase, xoffset=xoffset, yoffset=yoffset

widget_control, wbase, /realize

self -> xbk_update_widget, wbase

xmanager, 'xbk', wbase, /no_block

end
