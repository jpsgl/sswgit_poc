;+
; Name: spex_find_data
; 
; Purpose: Search for a type of data by time range over the network.
; 
; Method: Assumes there a an object class already defined for the datatype passed in (e.g. 'fermi_gbm', 'soxs') 
;   Uses that object to search for URLs within the specified time.  Searches among URLs found for any containing
;   pattern if argument was passed in.  If copy was set, copies the URLs to local computer
; 
; Arguments:
; datatype - string of data type, e.g. 'fermi_gbm', 'fermi_gbm_rsp, 'soxs', 'messenger'.  Must have object class of
;   same name that has site::search capability
; time_range - time range to find data files for.  If seconds, must be rel to 79/1/1. Otherwise any anytim format
;
; Input Keywords:
;   pattern - if set to a string, narrow found file down to those containing this pattern
;   det - string array of detectors to get files for.  If not set, get any det.  Choices are 
;         ['b0', 'b1', 'n0', 'n1', 'n2', 'n3', 'n4', 'n5', 'n6', 'n7', 'n8', 'n9', 'na', 'nb']
;   copy - if set, copy the urls found
;   dir - directory to write to if copying files. Default is current directory.
; 
; Output Keywords:
;   url - returns urls of files found
;   count - number of urls found, or if copy requested, number of files copied
;   file - if copy was set, this contains the file names on the local system
;   err - string containing any error messages, blank string if no error
;   _extra - any additional keywords to pass to search
; 
; Written, Kim Tolbert, June 2010
; 21-Oct-2010, Kim.  Dominic changed sock_copy keyword from copy_file to local_file, made same change here
; 19-Oct-2011, Kim.  Added det keyword.  If set, for detector-specific files, do further selection to return only
;   the detectors requested.
; 04-Nov-2011, Kim.  Added err keyword and initialize file_out to ''
; 
;-

pro spex_find_data, datatype, time_range, pattern=pattern, det=det, url=url, copy=copy, file=file_out, $
  dir=dir, count=count, err=err, _extra=extra

copy = keyword_set(copy)
file_out = ''

obj = obj_new(datatype)
ta = anytim(time_range,/vms)
url = obj -> search(ta[0], ta[1],count=count,err=err,_extra=extra)
destroy,obj

if count eq 0 then return

; First find urls with file names that match the pattern specified.
if keyword_set(pattern) then begin
;  q = where (strpos(file_break(url), pattern) ne -1, k)
  q = where( stregex(file_basename(url), pattern, /bool), k )
  if k gt 0 then url = url[q] ; else it will return whatever it found
endif

; Now, if certain detectors were selected, for filenames that are detector-specific, search for those that are
; for requested detectors.
kdet = 0
if keyword_set(det) then begin
  ; first check whether there are any file types with detector names (e.g. poshist doesn't have det names)
  q = where( stregex(file_basename(url), 'cspec|ctime|spechist', /bool), k, complement=qnodet, ncomplement=knodet)
  if k gt 0 then begin
    
    url_det = url[q]
    if knodet gt 0 then url_nodet = url[qnodet]
     
    ok_det = ['b0', 'b1', 'n0', 'n1', 'n2', 'n3', 'n4', 'n5', 'n6', 'n7', 'n8', 'n9', 'na', 'nb']
    qd = where (is_member(det, ok_det), nd)
    if nd gt 0 then begin
      det = det[qd]
      patt_det = arr2str('_' + det + '_', '|')    ; patt_det will look something like '_n5_|_n0_'
      q = where (stregex(file_basename(url_det), patt_det, /bool), kdet)
      if kdet gt 0 then url= url_det[q]
      if knodet gt 0 then url = [url, url_nodet]
    endif else message,/cont,'None of detectors requested are valid detector names: ' + arr2str(det)
  endif
endif     
    
count = n_elements(url)

if copy then begin      
  checkvar, dir, curdir()
  clobber = 0
  sock_copy, url, out_dir=dir, _extra=_extra, err=err, local_file=copyfiles, $
      cancelled=cancelled,clobber=clobber,status=status;,/verbose,/progress
  if ~is_string(err) then begin
    file_out = copyfiles
    count = n_elements(file_out)
  endif
endif

end