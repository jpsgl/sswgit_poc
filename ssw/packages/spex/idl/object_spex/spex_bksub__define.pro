;+
; Modifications:
; 16-Sep-2004, Kim.  Added show_err option in plot_setup method.
; 13-Feb-2006, Kim.  In plot_setup, call get_plot_title to get title
; 13-Sep-2006, Kim.  In plot_setup, call spex_apply_drm for errors too if photons
;
;---------------------------------------------------------------------------
;-

;---------------------------------------------------------------------------

function  spex_bksub::init, source = source, _extra = _extra

if not keyword_set( source ) then source = obj_new( 'spex_bk', _extra=_extra )

return, self->framework::init( source = source, $
                                            info = {spex_bksub_info}, $
                                            _extra = _extra )

end

;--------------------------------------------------------------------------

; data product created is data,edata,ltime structure for raw data counts minus expected
; background counts for every raw time and energy bin.

; explictly name spex_units keyword to keep it out of _extra
pro spex_bksub::process, spex_units=spex_units, _extra = _extra

;print,'in spex_bksub::process'

data_obj = self -> get(/obj, class='spex_data')
data = data_obj -> getdata(/original_units, _extra=_extra)
if not is_struct(data) then message, 'No data.'

bk_obj = self -> get(/obj, class='spex_bk')
bk = bk_obj -> getdata(/original_units, _extra=_extra)

units_str = data_obj -> getunits(/orig)

self -> setunits, units_str, /orig

if bk.data[0] ne -1 then $
	self->setdata, {data: data.data - bk.data, $
		edata: sqrt (data.edata^2 + bk.edata^2), $
		ltime: data.ltime} $  ; check again !!!!!
else $
	self->setdata, {data: data.data, edata: data.edata, ltime: data.ltime}

end

;--------------------------------------------------------------------------

function spex_bksub::getunits, orig=orig
if keyword_set(orig) then return, self->get(/spex_bksub_origunits)

return, self->get(/spex_bksub_units)
end

;--------------------------------------------------------------------------

pro spex_bksub::setunits, units, orig=orig

if keyword_set(orig) then self -> set, spex_bksub_origunits=units else $
	self -> set, spex_bksub_units=units

end

;--------------------------------------------------------------------------
; Plot method plots time profile of background-subtracted data.
; If intervals=-1 then uses raw energy bands.  If no intervals passed in, then
; uses spex_eband to bin in energy.
; If overlay_data is set, overlays data from spex_data
; If overlay_bk is set, overlays data from spex_bk
;
function spex_bksub::plot_setup, $
	intervals=intervals, $
	pl_time=pl_time, $
	pl_energy=pl_energy, $
	overlay_data=overlay_data, $
	overlay_bk=overlay_bk, $
	photons=photons, $
	drm_eff=drm_eff, $
	dim1_sum=dim1_sum, $
	show_err = show_err, $
	plot_params=plot_params, $
	_extra=_extra

error_catch = 0
if spex_get_debug() eq 0 then catch, error_catch
if error_catch ne 0 then begin
	catch, /cancel
	message, !error_state.msg, /cont
	;message,'Error making plot.', /cont
	return, 0
endif

photons = keyword_set(photons)
if photons and not keyword_set(drm_eff) then message,'No efficiency factors provided.  Cannot display photons.'

pl_time = keyword_set(pl_time)
pl_energy = keyword_set(pl_energy)
if not (pl_time or pl_energy) then pl_time = 1

overlay_data = keyword_set(overlay_data)
overlay_bk = keyword_set(overlay_bk)
overlay = overlay_data or overlay_bk

data = self -> getdata(_extra=_extra)
if not is_struct(data) then message, 'No data accumulated.'
if data.data[0] eq -1 then message, 'No data accumulated.'

label = 'Detectors: ' + self -> get(/spex_detectors)

title = self -> get_plot_title(photons=photons)

units_str = self -> getunits()

if pl_energy then begin	;plotting vs energy

	energies = self -> getaxis(/ct_energy, /edges_2)
	checkvar, intervals, -1
	z = self -> bin_data (data=data, intervals=intervals, er=z_err, /do_time, newedges=newedges)
	if z[0] eq -1 then return, 0

	xdata = energies
	ydata = z
	plot_type = 'xyplot'
	title = title + ' vs Energy'
	xtitle = 'Energy (keV)'
	dim1_id = format_intervals(newedges, /ut) + ' (Data-Bk)'
	dim1_vals = anytim(newedges, /vms)
	;dim1_sum = (overlay eq 0)
	checkvar, dim1_sum, 1
	if overlay then dim1_sum=0
	dim1_unit = ''
	xlog = 1
	ylog = 1

	if overlay_data then begin
		data = self -> getdata(class='spex_data', _extra=_extra)
		bdata = self -> bin_data (data=data, intervals=intervals, er=er, /do_time, newedges=newedges)
		if bdata[0] ne -1 then begin
			ydata = [ [ydata], [bdata] ]
			dim1_id = [dim1_id, format_intervals(newedges, /ut) + ' (Data)']
			dim1_vals = [ [ dim1_vals], [anytim(newedges,/vms)] ]
		endif
	endif

	if overlay_bk then begin
		data = self -> getdata(class='spex_bk', _extra=_extra)
		if data.data[0] ne -1 then begin
			bdata = self -> bin_data (data=data, intervals=intervals, er=z_err, /do_time, newedges=newedges)
			if bdata[0] ne -1 then begin
				ydata = [ [ydata], [bdata] ]
				dim1_id = [dim1_id, format_intervals(newedges, /ut) + ' (Bk)']
				dim1_vals = [ [ dim1_vals], [anytim(newedges,/vms)] ]
			endif
		endif
	endif

endif else begin	;plotting vs time

	checkvar, intervals, self->get(/spex_eband)  ; if no energy bins passed in, use eband

	bdata = self -> bin_data (data=data, intervals=intervals, er=z_err, newedges=newedges)
	if bdata[0] eq -1 then return, 0
	ydata = transpose(bdata)
	z_err = transpose(z_err)
	xdata = self -> getaxis(/ut, /edges_2)
	utbase = min(xdata)
	xdata = xdata - utbase
	if n_elements(xdata) le 2 then begin
		message, /cont, 'Error - only one time interval.  Aborting plot.'
		return, 0
	endif
	plot_type = 'utplot'
	title = title + ' Minus Background'
	xtitle = ''
	dim1_id = format_intervals(newedges, format='(f9.1)') + ' keV'
	dim1_id = dim1_id + ' (Data-Bk)'
	dim1_vals = newedges
	checkvar, dim1_sum, 0
	dim1_unit = units_str.ct_edges
	xlog = 0
	ylog = 1

	if overlay_data then begin
		data = self -> getdata(class='spex_data', _extra=_extra)
		bdata = self -> bin_data (data=data, intervals=intervals, er=er, newedges=newedges)
		if bdata[0] ne -1 then begin
			ydata = [ [ydata], [transpose(bdata)] ]
			dim1_id = [dim1_id, format_intervals(newedges, format='(f9.1)') + ' keV (Data)']
			dim1_vals = [ [ dim1_vals], [newedges] ]
		endif
	endif

	if overlay_bk then begin
		data = self -> getdata(class='spex_bk', _extra=_extra)
		if data.data[0] ne -1 then begin
			bdata = self -> bin_data (data=data, intervals=intervals, er=er, newedges=newedges)
			if bdata[0] ne -1 then begin
				ydata = [ [ydata], [transpose(bdata)] ]
				dim1_id = [dim1_id, format_intervals(newedges, format='(f9.1)') + ' keV (Bk)']
				dim1_vals = [ [ dim1_vals], [newedges] ]
			endif
		endif
	endif

endelse

if photons then begin
	spex_apply_eff, ydata, drm_eff, units_str=units_str
	spex_apply_eff, z_err, drm_eff
endif

checkvar, utbase, 0
plot_params = { $
	plot_type: plot_type, $
	xdata: xdata, $
	utbase: anytim(utbase, /vms), $
	ydata: ydata, $
	id: title, $
	label: label, $
	data_unit: units_str.data, $
	dim1_id: dim1_id, $
	dim1_vals: dim1_vals, $
	dim1_sum: dim1_sum, $
	dim1_unit: dim1_unit, $
	dim1_enab_sum: (overlay eq 0), $
	weighted_sum: 1, $
	xlog: xlog, $
	ylog: ylog, $
	xtitle: xtitle $
	}

if keyword_set(show_err) then plot_params = add_tag (plot_params, z_err, 'edata')

return, 1
end

;-------------------------------------------------------------------------------

;pro spex_bksub::plot, _extra=_extra
;
;status = self -> plot_setup (plot_params=plot_params, _extra=_extra)
;
;if status then self -> do_plot, plot_params=plot_params, _extra=_extra
;
;end

;---------------------------------------------------------------------------

pro spex_bksub__define

dummy = {spex_bksub, $
         inherits spex_gen }

end
