;---------------------------------------------------------------------------
; Document name: spex_summ_define_str.pro
; Created by:   Sandhia Bansal
;
; Time-stamp:
;---------------------------------------------------------------------------
;
;+
; PROJECT:
;       HESSI
;
; NAME:
;       spex_summ_define_str
;
; PURPOSE:
;       Defines the structure that contains spex_summ parameters
;
; Calling arguments:
;       nintervals - number of time intervals
;       nenergies - number of energy bins
;       nparams - total number of parameters for combined function
;       ncomp - number of function components in combined function
;
; CATEGORY:
;       gen/fits
;
; CONSTRUCTION:
;       o = spex_summ_define_str(nintervals, nenergies, nparams, ncomp)
;
; SEE ALSO
; HISTORY:
;       Sep-2004: created - Sandhia Bansal
; Modifications:
; 17-Sep-2004, Kim.  Added bk_rate and bk_error
; 29-Sep-2004, Kim.  Init spex_summ_filter to -1
; 20-Mar-2006, Kim.  Added ncomp arg and spex_summ_func_spectrum, spex_summ_func_model,
;   and spex_summ_chianti_version and spex_summ_abun_table
;
;--------------------------------------------------------------------

function spex_summ_define_str, nintervals, nenergies, nparams, ncomp


 summ = {spex_summ_fit_function: '', $
  spex_summ_area: 0.0, $
  spex_summ_energy: fltarr(2,nenergies), $
  spex_summ_time_interval: dblarr(2, nintervals), $
  spex_summ_filter: intarr(nintervals)-1, $
  spex_summ_fit_done: bytarr(nintervals), $
  spex_summ_emask: bytarr(nenergies, nintervals), $
  spex_summ_ct_rate: fltarr(nenergies, nintervals), $
  spex_summ_ct_error: fltarr(nenergies, nintervals), $
  spex_summ_bk_rate: fltarr(nenergies, nintervals), $
  spex_summ_bk_error: fltarr(nenergies, nintervals), $
  spex_summ_ph_model: fltarr(nenergies, nintervals), $
  spex_summ_conv: fltarr(nenergies, nintervals), $
  spex_summ_resid: fltarr(nenergies, nintervals), $
  spex_summ_starting_params: fltarr(nparams, nintervals), $
  spex_summ_params: fltarr(nparams, nintervals), $
  spex_summ_sigmas: fltarr(nparams, nintervals), $
  spex_summ_minima: fltarr(nparams, nintervals), $
  spex_summ_maxima: fltarr(nparams, nintervals), $
  spex_summ_free_mask: bytarr(nparams, nintervals), $
  spex_summ_func_spectrum: strarr(ncomp, nintervals), $
  spex_summ_func_model: strarr(ncomp, nintervals), $
  spex_summ_chianti_version: '', $
  spex_summ_abun_table: '', $
  spex_summ_maxiter: intarr(nintervals), $
  spex_summ_uncert: fltarr(nintervals), $
  spex_summ_chisq: fltarr(nintervals), $
  spex_summ_stop_msg: strarr(nintervals), $
  spex_summ_niter: intarr(nintervals) }


return, summ

end

