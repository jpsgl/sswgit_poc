;+
; Name: spex_fit__xfit_comp
;
; Purpose: Method of spex_fit object to allow users to select fit components and parameters
;	through a widget interface
;
; Category: FITTING, SPEX
;
; Input:
;	group - widget id of parent widget, if any
;
; Written: Kim Tolbert, March, 2003
; Modifications:
;	13-Aug-2004, Kim.  Now 'accept' button has two options - accept and continue as before,
;	  or just accept these parameters, but don't continue looping, and don't save fit
;	  results.  Also, on cancel was resetting the fit comp stuff, but not the erange, uncert,
;	  or itmax to original - fixed.
;	31-May-2005, Kim.  Previously called xmanager with cleanup= a routine that would get the
;	  uvalue and then re-sensitize the main Fit widget (state.xfit_widget_id).  On Linux, (this
;	  never happened on Windows), this was causing IDL to hang intermittently with:
;	     % X windows protocol error: BadFont (invalid Font parameter).
;	     Xlib: unexpected async reply (sequence 0xdb70)!
;	  or if it didn't crash, often got this error:
;	     % X windows protocol error: BadFont (invalid Font parameter).
;	     Xlib: sequence lost (0xf9fd9 > 0xea003) in reply type 0x0!
;	  Got rid of the cleanup routine (I think the problem was in trying to get the uvalue out of
;	  the widget that it was killing) and just resensitize the fit widget after we pass through
;	  xmanager to the end of the routine.
;	28-Jun-2005, Kim.  After fitting, call plot_spectrum with show_chi
;	22-Aug-2005, Kim.  Use new units_widget gen method to handle units.
;	25-Aug-2005, Kim.  Added class= to calls to get wherever possible to speed things up
;	16-Feb-2006, Kim.  Add 0. to dropdown list for systematic uncertainty
;	15-Mar-2006, Kim. added gui_label to put on title bar of spex widget interfaces
;	7-Apr-2006, Kim.  Changes for addition of spectrum,model keywords
;	26-Apr-2006, Kim. Save table values as strings in tables[ii].vals pointer to check what's changed
;	29-May-2006, Kim.  Use trim when checking func_spectrum and func_model in case they = '  '
;	26-Jul-2006, Kim.  Added /force in call to getdata(class='spex_fitalg') to ensure that a fit
;		is done, even if didn't change any parameters
;	9-Aug-2006, Kim.   Added a catch in event handler.  Added a kill for wxmessage at the exit
;		in case it's still alive (because of routing through the catch).  Also in 'fit' event,
;		check if data is a structure before using it.
;	31-Oct-2006, Kim.  Rearranged widgets to make widget narrower
;	8-Mar-2007, Kim.   Reset to previous interval when in backward fitting direction should
;		use interval+1.  (previously wasnt' checking direction)
;	13-Mar-2007, Kim.  Added a Reset button for all components with same options as the button
;		already there for individual compononents.  Also don't plot unless reset item is
;		'all' or 'val' and status is 1 (ie. unless value changed) if autoplot is selected.
;		Also, init error_catch, and print error message if jumps to catch handler.
;	16-Oct-2007, Kim.  When getting fc_spectrum_options or fc_model_options, have to supply comp_name
;		or strategy_name, since now they are available for functions other than vth.
;	4-Feb-2008, Kim.  Use strategy_name in call to fit_function_query to differentiate between
;		different instances of a comp in a function in reset options. (like line+line)
;	12-Feb-2008, Kim.  Added spex_fitcomp_plot_bk param and button for plotting bk overlay
;	27-Feb-2008, Kim.  Added Fit Summary button - calls quick_summary method for qk look at fit
;		(to show sigmas and chisq)
;	21-Mar-2008, Kim.  Added /check_match in call to valid_summ_params for previous iter or int reset
;	27-Mar-2008, Kim.  In calls to plot_resid, pass in chisq, and don't use show_chi (obsolete). Also
;		pass chisq explictly into plot_spectrum.
;	10-Jul-2008, Kim.  When exiting, call check_func with /justask so if user has changed function
;		but doesn't want to reinitializ spex_summ, we can stay in the xfit_comp widget.
;		Also, change hsi_ui_getfont to get_font (which is in ssw gen)
; 11-Sep-2008, Kim.  Added Reset options to set all free masks to 1 or 0 for all comp or one comp
; 14-Jul-2009, Kim.  Added Auto-set max erange button. Modified wording on 'Accept' button options.
; 17-Aug-2009, Kim.  Added cleanup routine back in (hope it doesn't cause linux problems mentioned above)
;   Don't free start_, final_ and status_ptr until just before return so we can extract values.
; 26-Sep-2009, Kim.  Added widgets for entering template file name other than shortcuts available
;   through droplist.  Also summ is now passed in - store in uvalue struct to use in check_func and
;   valid_summ_params since we're now calling check_func before entering xfit_comp, so summ might be
;   different from stored spex_summ.  Store spectrum_opts and model_opts in table struct for each component now.
; 10-Oct-2009, Kim. Fixed bug in xfit_comp_reset - was using summ instead of state.summ (thanks Amir!)
; 09-Nov-2009, Kim. Added a column to the left of each table of params giving the param number, and making
;   a hover (tooltip) over the number show the parameter description.  Also, changed free mask column to
;   be a green or red button instead of a 0 or 1 (which is now contained in the uvalue of the button)
; 11-May-2010, Kim. Small change in xfit_comp_reset to use fit_function_query for defaults for single 
;   and multiple component functions.
; 24-Sep-2012, Kim. Added 0 to values in # Iter pulldown.
; 11-Oct-2012, Kim.  Added spex_fitcomp_plot_err param and button for plotting errors. Also added option
;   to plot raw residuals (made Plot Residuals a pulldown with Normalized and Raw options) and save fit_raw_resid
;   in state.  When plotting from Plot Residuals button, plot in Main Window intead of Resid Window.
; 30-Oct-2013, Kim. Changed auto-set erange. Previously only for emax, and set the flag as well as the erange for this
;   interval, which meant that the flag was set for all subsequent intervals.  Now have choice of max, min, both, and
;   the click 'Auto-set' button to apply it for current interval only.
; 02-Apr-2014, Kim. Added Plot DEM button for f_multi_therm_xxx component only
; 26-Jun-2014, Kim. Added Plot EDF button for f_thick2_vnorm component
; 11-Jul-2014, Kim. Modified to add plot EDF button for any component that can do it
; 04-Mar-2016, Kim. Changed auto-set erange button wording to make it clearer that the button does not set
;   the auto-set flags.  In this case, sets the values based on current interval, and they may persist, depending
;   on parameter initialization options set by user. 
;
;
;------------------------------------------------------------------------------------------

; when xfit_comp widget dies, make sure xfit widget (if it's around) is sensitive
pro xfit_comp_cleanup, widget_id
widget_control, widget_id, get_uvalue=state, /no_copy
if n_elements(state) eq 0 then return
heap_free, state.table
ptr_free, state.fit_xdata
ptr_free, state.fit_resid
ptr_free, state.fit_raw_resid
;free_var, state.start_ptr  ; can't free these until we get values out of them
;free_var, state.final_ptr
;free_var, state.status_ptr
;if xalive(state.xfit_widget_id) then widget_control, state.xfit_widget_id, /sensitive
end

;------------------------------------------------------------------------------------------

function spex_fit::xfit_comp_select_template_file

      sel = ssw_pickfile( exists=exists, title='Select template file', filter=['*.txt','*.sav'])
      sel = sel[0]
      case 1 of
        sel eq '': msg = 'ERROR: No template file selected.'
        exists eq 0: msg = 'ERROR: Selected file not found - ' + sel
        else: msg = ''
      endcase
      if msg ne '' then begin
        message, /cont, msg
        sel = ''
    endif
    return, sel  
end

;------------------------------------------------------------------------------------------

; 13-Mar-2007 made this a method so we could use it for the 'Reset all components' button as
; well as reset for each component.
; Create reset button with lots of options for resetting parameters.
; Input:
; wparent - the parent widget of the reset button to be created
; value - the text to show on the button
; Output:
; wreset - widget id of the reset button created.

pro spex_fit::xfit_comp_reset_button, wparent, value, wreset

reset_items = ['All', 'Value', 'Minima', 'Maxima', 'Free']
reset_item_codes = ['all','val','min','max','free']
reset_modes = ['Program Defaults', 'Previous Interval', 'Previous Iteration', 'Original value']
reset_mode_codes = ['def', 'prevint','previter','orig']
free_reset_modes = [reset_modes,'All 0', 'All 1']
free_reset_mode_codes = [reset_mode_codes, 'allzero', 'allone']

wreset = widget_button (wparent, /align_center, value=value+' ->', /menu)
for i = 0,n_elements(reset_items)-1 do begin
	tmp_base = widget_button (wreset, value=reset_items[i], /menu)
	rmodes = reset_items[i] eq 'Free' ? free_reset_modes : reset_modes
  rmode_codes = reset_items[i] eq 'Free' ? free_reset_mode_codes : reset_mode_codes
	for j = 0,n_elements(rmodes)-1 do begin
		tmp = widget_button (tmp_base, value=rmodes[j], $
			uvalue='reset' + '_' + reset_item_codes[i] + '_' + rmode_codes[j])
	endfor
endfor

end

;------------------------------------------------------------------------------------------

pro spex_fit::xfit_comp_update_widget, wbase

widget_control, wbase, get_uvalue=state

afit = self -> get(/fit_function, class='fit_function')
if afit eq '' then afit = 'None'
widget_control, state.wcurrent, set_value='Current fit function: ' + afit

interval_index = self -> get(/spex_interval_index)
interval_times = format_intervals((self -> get(/spex_fit_time_interval))[*,interval_index], /ut)
widget_control, state.winterval, set_value='Interval ' + trim(interval_index) + ':  ' + interval_times


cell_size = 11

widget_control, state.wbase1, update=0

;check_size = 0
; Create widget tables of parameters for each function component
 
if afit ne 'None' then begin

	compman_name = self -> get(/compman_name, class='fit_comp_manager')
	compman_strategy = self -> get(/compman_strategy, class='fit_comp_manager')

	for ii = 0,n_elements(compman_name)-1 do begin	; loop through function components

		str = self -> get(/fit_comp, strategy_name=compman_strategy[ii], class='fit_comp_manager')
		npar = n_elements(str.fit_comp_params)
		func = str.fit_comp_function
		; if is_multi_therm, add Plot DEM button
		is_multi_therm = strpos(compman_name[ii], 'multi_therm') ne -1
		; if yes_electron_plot, add Plot Electron Dist. button
		yes_electron_plot = check_func_electron(compman_name[ii])

		; if widget base for this component is alive, and the function hasn't changed, then don't need to
		; create the widgets, just jump down and fill in the current values
		if (not xalive(state.table[ii].wcompbase)) or (state.table[ii].npar ne npar) or (state.table[ii].func ne func) then begin

		;	check_size = 1	;we're changing size of widget, check that it's not too big when done
			if xalive(state.table[ii].wcompbase) then widget_control, state.table[ii].wcompbase, /destroy
			state.table[ii].func = func
			state.table[ii].npar = npar
			state.table[ii].strategy_name = compman_strategy[ii]

			wcompbase = widget_base(state.wbase1, /column, /frame, xpad=10, ypad=5)
			state.table[ii].wcompbase = wcompbase

      comp_str = fit_model_components(compman_name[ii],/struct)
      
			wbase_r1 = widget_base(wcompbase, /row, space=0, xpad=0)
			tmp = widget_button(wbase_r1, value=compman_name[ii], uvalue='desc', /align_center, tooltip=comp_str.short_desc)
			
			; add column of param number with tooltip showing description on hover
			; if change something else about the table, these might not line up, so be careful
      wtable0 = widget_base (wbase_r1, /grid_layout, column=1)
      tmp = widget_text (wtable0, value=' #', xsize=2)
      for i=0,npar-1 do tmp = widget_button(wtable0, value=trim(i),xsize=2, $
        tooltip=trim(i) + ':  ' + comp_str.param_desc[i])
 			
 			  ; the following also works on windows, but doesn't work on unix - doesn't line up
 			  ; with the rows in the table.
;		  	wnum = widget_base(wbase_r1, /column)
;		  	tmp = widget_text(wnum,value=' #',xsize=2)
;		  	for i=0,npar-1 do tmp = widget_button(wnum, value=trim(i),xsize=2, $
;			    tooltip=trim(i) + ':  ' + comp_str.param_desc[i])

			wtable = widget_base (wbase_r1, /grid_layout, column=4, /align_left)
			state.table[ii].wtable = wtable

			wids = lonarr(npar,4)
      ;NOTE:  if change certain things in the table format, first and last column might not line up, so be careful
			w1 = widget_text (wtable, value='   Value', xsize=cell_size)
			for i=0,npar-1 do wids[i,0] = widget_text (wtable, /edit, value='', xsize=cell_size, uvalue='chg_value')
			w1 = widget_text (wtable, value='   Minimum', xsize=cell_size)
			for i=0,npar-1 do wids[i,1] = widget_text (wtable, /edit, value='', xsize=cell_size)
			w1 = widget_text (wtable, value='   Maximum', xsize=cell_size)
			for i=0,npar-1 do wids[i,2] = widget_text (wtable, /edit, value='', xsize=cell_size)
			w1 = widget_text (wtable, value='   Free (green)', xsize=cell_size)
			; change 0/1 text for free mask to green or red button
;			for i=0,npar-1 do wids[i,3] = widget_text (wtable, /edit, value='', xsize=cell_size)
			for i=0,npar-1 do wids[i,3] = widget_button (wtable, value=red_bmp, uvalue=0, /align_center, /frame)

			ptr_free, state.table[ii].wids
			state.table[ii].wids = ptr_new(wids)

			wcompbut = widget_base (wbase_r1, /column, space=10)

			; create button with menu of options for resetting this component
			self -> xfit_comp_reset_button, wcompbut, 'Reset', wreset
			state.table[ii].wreset = wreset

			state.table[ii].wdelete = widget_button (wcompbut, /align_center, value='Delete comp', uvalue='delcomp')
			state.table[ii].wplot = widget_button (wcompbut, /align_center,  value='Plot comp', uvalue='plotcomp')
			if is_multi_therm then state.table[ii].wplot_dem = widget_button (wcompbut, /align_center,  value='Plot DEM', uvalue='plot_dem')
			if yes_electron_plot then state.table[ii].wplot_edf = widget_button (wcompbut, /align_center,  value='Plot Electron Dist', uvalue='plot_edf')

			if fit_comp_kw(str, nkw=nkw) gt 0 then begin
				is_template = compman_Name[ii] eq 'template'
				wbase_r2 = widget_base(wcompbase, /row, space=10)
				temp = widget_label(wbase_r2, value='Keywords:')
				if trim(str.fit_comp_spectrum) ne '' then begin
				  opts = self->get(/fc_spectrum_options, strategy_name=state.table[ii].strategy_name)
				  if is_template then opts = [opts, 'Other']
				  ptr_free, state.table[ii].spectrum_opts
				  state.table[ii].spectrum_opts = ptr_new(opts)
					state.table[ii].wspectrum = widget_droplist (wbase_r2, value=opts, uvalue='chg_spectrum')
				  if is_template then begin
				    state.table[ii].wchange_spec_other = widget_text(wbase_r2, /edit, xsize=40, value='', uvalue='change_spec_other') 
;				    widget_label(wbase_r2, value=string(' ',format='(a40)') )
            state.table[ii].wbrowse_spec_other = widget_button(wbase_r2, value='Browse', uvalue='browse_spec_other')
          endif
				endif
				if trim(str.fit_comp_model) ne '' then begin
					ptr_free, state.table[ii].model_opts
					state.table[ii].model_opts = ptr_new(self->get(/fc_model_options, strategy_name=state.table[ii].strategy_name))
					state.table[ii].wmodel = widget_droplist (wbase_r2, $
						value=*state.table[ii].model_opts, $
						uvalue='chg_model')
					endif
			endif

		endif

		wids = *state.table[ii].wids
		for i=0,npar-1 do widget_control, wids[i,0], set_value=strpad(trim(str.fit_comp_params[i]),cell_size)
		for i=0,npar-1 do widget_control, wids[i,1], set_value=strpad(trim(str.fit_comp_minima[i]),cell_size)
		for i=0,npar-1 do widget_control, wids[i,2], set_value=strpad(trim(str.fit_comp_maxima[i]),cell_size)
;		for i=0,npar-1 do widget_control, wids[i,3], set_value=strpad(trim(fix(str.fit_comp_free_mask[i])),cell_size)
   ; For free mask, set uvalue to free value, and set button value to green or red bitmap
    for i=0,npar-1 do widget_control, wids[i,3], set_value=fix(str.fit_comp_free_mask[i]) ? state.green_bmp : state.red_bmp, $
      set_uvalue=fix(str.fit_comp_free_mask[i])

		; Store snapshot of values in table as strings in vals pointer
		; Need this to compare with table later to see if anything's changed.  Can't use internal
		; object values because they're binary and these are ascii, so they're always different
		vals = strarr(npar,4)
		for i=0,npar-1 do for j=0,3 do begin
		  ; For free mask, have to get_uvalue (0 or 1), but for other widgets in table, get_value
			if j lt 3 then widget_control, wids[i,j], get_value=val else widget_control, wids[i,j], get_uvalue=val
			vals[i,j] = val
		endfor
		ptr_free, state.table[ii].vals
		state.table[ii].vals = ptr_new(vals)

		if fit_comp_kw(str) then begin
			if trim(str.fit_comp_spectrum) ne '' then begin
				opts = *state.table[ii].spectrum_opts
				; If current setting isn't in opts list, then select 'Other' which is last in list
				q = where (stregex(opts, str.fit_comp_spectrum, /fold, /bool), count)
				sel = count gt 0 ? q[0] : n_elements(opts)-1
				widget_control, state.table[ii].wspectrum, set_droplist_select=sel
				; set label to blank unless 'Other' is selected 
				if xalive(state.table[ii].wchange_spec_other) then $
				  widget_control, state.table[ii].wchange_spec_other,set_value=(count gt 0) ? '' : str.fit_comp_spectrum 
			endif
			
			if trim(str.fit_comp_model) ne '' then begin
				opts = *state.table[ii].model_opts
				q = where (stregex(opts, str.fit_comp_model, /fold, /bool), count)				
				if count gt 0 then widget_control, state.table[ii].wmodel, set_droplist_select=q[0]
			endif
		endif

	endfor	; end of loop through function components

endif

erange = self -> get(/spex_erange)
nbands = erange[0] eq -1 ? 0 : n_elements(erange[0,*])
s_erange = erange[0] eq -1 ? strpad('All',20) : format_intervals(erange, format='(f9.1)')
widget_control, state.w_nbands, set_value=strtrim(nbands, 2) + ' '
widget_control, state.w_erange, set_value='  ' + s_erange + '  '

widget_control, state.w_maxiter, set_value=self->get(/mcurvefit_itmax)  ;!!!! make general

widget_control, state.w_uncert, set_value=self->get(/spex_uncert)

;if check_size then begin
	; The function component part of this widget can put up a scroll bar if there are lots
	; of components and it gets too big.
	; X and Win behave differently.
	; On X, when add new wcompbase inside wbase1, size of wbase1 doesn't change.
	;   So have to add up the sizes of the wcompbases to see what total size of
	;   comp part of window should be.
	; On Win, when add new wcompbase insde of wbase, wbase grows accordingly, so have to check
	;   its size and reduce it if necessary.
	; In either case, we won't let size of component part be larger than .5 of y screensize (to leave
	; room for buttons on top and bottom)

	device, get_screen_size=scrsize
	max_ysize = .5*scrsize[1]
	base1 = widget_info(state.wbase1, /geometry)

	if !d.name eq 'X' then begin
		ysize = 0
		for i=0,19 do begin
			if xalive(state.table[i].wcompbase) then begin
				geom = widget_info(state.table[i].wcompbase, /geom)
				ysize = ysize+geom.ysize
			endif
		endfor
		widget_control, state.wbase1, ysize = (ysize < max_ysize)

	endif else begin
		ysize = base1.ysize
;		if ysize gt max_ysize then widget_control, state.wbase1, ysize=max_ysize
		widget_control, state.wbase1, ysize = (ysize < max_ysize)
	endelse

;endif
widget_control, state.wbase1, update=1

widget_control, wbase, set_uvalue=state

end

;-----

; This method is called when user clicks any of the reset buttons
; Input:
; state - strucure with widget state
; reset_item - what to reset. Options are all, val, min, max, or free
; reset_mode - what to reset them to. Options are defaults, previous int,
;   previous iter, original val when entered widget
; ic - index of component to change, or, if -99, means do all components
; Output:
; status - 0/1 means failure/success resetting params

pro spex_fit::xfit_comp_reset, state, reset_item, reset_mode, ic, status

status = 1

all_comp = (ic eq -99)

fit_func = self -> get(/fit_function)

if all_comp then begin
	strategy_name = ''
endif else begin
	compman_name = self -> get(/compman_name, class='fit_comp_manager')
	comp = compman_name[ic]
	strategy_name = state.table[ic].strategy_name
endelse

err_msg = ''

case 1 of
	reset_mode eq 'def': begin
;		vals = all_comp ? fit_function_query(fit_func, /defaults) : fit_comp_defaults(comp)
    func = all_comp ? fit_func : comp
    vals = fit_function_query(func, /defaults)
		end

	reset_mode eq 'prevint' or reset_mode eq 'previter': begin
		interval = self -> get(/spex_interval_index)
		if reset_mode eq 'prevint' then $
			interval = (self->get(/spex_fit_reverse)) ? interval + 1 : interval - 1
		word = (reset_mode eq 'prevint') ? 'interval' : 'iteration'
		if not self->valid_summ_params(interval=interval, summ=state.summ, /check_match) then $
		  err_msg = 'No previous fit ' + word + ' to use'
		if err_msg eq '' then begin
;			vals = self -> get(/spex_summ, class='spex_fit')
			if fit_func eq state.summ.spex_summ_fit_function then begin
				if all_comp then begin
					i1 = 0
					i2 = n_elements(state.summ.spex_summ_params[*,0]) - 1
				endif else begin
					ind = fit_function_query(state.summ.spex_summ_fit_function, /param_index, comp=strategy_name)
					i1 = min(ind)
					i2 = max(ind)
				endelse
				vals = {fit_comp_params: state.summ.spex_summ_params[i1:i2,interval], $
						fit_comp_minima: state.summ.spex_summ_minima[i1:i2,interval], $
						fit_comp_maxima: state.summ.spex_summ_maxima[i1:i2,interval], $
						fit_comp_free_mask: state.summ.spex_summ_free_mask[i1:i2,interval]}
			endif else begin
				err_msg = 'Can not set to previous ' + word + ' value because function has changed.'
			endelse
		endif
		end

	reset_mode eq 'orig': begin
		; reset to original values (values when entered widget)
		; can only use this if original function was same as current function
		vals = state.orig_str
		old_func = arr2str(vals.fit_comp_function,'+')
		if fit_func eq old_func then begin
			if all_comp then begin
				i1 = 0
				i2 = n_elements(vals.fit_comp_params) - 1
			endif else begin
				ind = fit_function_query(arr2str(vals.fit_comp_function,'+'), /param_index, comp=strategy_name)
				i1 = min(ind)
				i2 = max(ind)
			endelse
			vals = {fit_comp_params: vals.fit_comp_params[i1:i2], $
				fit_comp_minima: vals.fit_comp_minima[i1:i2], $
				fit_comp_maxima: vals.fit_comp_maxima[i1:i2], $
				fit_comp_free_mask: vals.fit_comp_free_mask[i1:i2]}
		endif else begin
			err_msg = 'Can not set to original value because function has changed.'
		endelse
		end

  reset_mode eq 'allzero' or reset_mode eq 'allone': begin
    if all_comp then begin
      nfree = n_elements(self->get(/fit_comp_params))
    endif else begin
      ind = fit_function_query(fit_func, /param_index, comp=strategy_name)
      nfree = ind[1]-ind[0]+1
    endelse
    free = intarr(nfree) + (reset_mode eq 'allzero' ? 0 : 1)
    vals = {fit_comp_free_mask: free}
    end

endcase

if err_msg ne '' then begin
	message, err_msg, /cont
	status = 0
	return
endif

; strategy_name will be blank string if we're doing all components.
case reset_item of

	'all':  self -> set, _extra=vals, strategy_name=strategy_name

	'val': self -> set, fit_comp_params = vals.fit_comp_params, strategy_name=strategy_name

	'min': 	self -> set, fit_comp_minima = vals.fit_comp_minima, strategy_name=strategy_name

	'max': self -> set, fit_comp_maxima = vals.fit_comp_maxima, strategy_name=strategy_name

	'free': self -> set, fit_comp_free_mask = vals.fit_comp_free_mask, strategy_name=strategy_name

endcase

end

;-----
; Event handlers can't be object methods, so this is just a wrapper to get to the method

pro xfit_comp_event,event
widget_control,event.top,get_uvalue=state
if obj_valid(state.object) then state.object->xfit_comp_event,event
return & end

;-----
; xfits_setup event handler

pro spex_fit::xfit_comp_event, event

error_catch = 0
if spex_get_debug() eq 0 then catch, error_catch
if error_catch ne 0 then begin
	catch, /cancel
	message, /info, 'ERROR: ' + !error_state.msg
	goto, exit
end

widget_control, event.top, get_uvalue=state

; if id is top, then event is a resize window event
if event.id eq event.top then begin
	if tag_names(event,/struc) eq 'WIDGET_KILL_REQUEST' then uvalue = 'cancel' else begin
		wsize = widget_info(state.wbase, /geometry)
		widget_control, state.wbase, scr_ysize=wsize.scr_ysize
		return
	endelse
endif else widget_control, event.id, get_uvalue=uvalue

checkvar, uvalue, ''

; Currently the only numeric uvalue is the free mask. If add more, have to differentiate here.
if is_number(uvalue) then begin
    widget_control, event.id, get_uvalue=val
    val = val ? 0 : 1
    widget_control, event.id, set_value=val ? state.green_bmp : state.red_bmp, set_uvalue=val
    uvalue=''
endif

if uvalue ne 'cancel' then begin

	; see if the current table still matches the last snapshot taken of the table
	; If not, store new values in object, and set status to 1
	for ic = 0,19 do begin
		if xalive(state.table[ic].wcompbase) then begin
			wids = *state.table[ic].wids
			vals = *state.table[ic].vals
			newvals = make_array(/string, size(wids,/dim))
			for ip=0, state.table[ic].npar-1 do begin
			 	for j=0,3 do begin
;			 		widget_control, wids[ip,j], get_value=value
          ; For free mask, have to get_uvalue (0 or 1), but for other widgets in table, get_value
          if j lt 3 then widget_control, wids[ip,j], get_value=value else widget_control, wids[ip,j], get_uvalue=value
			 		newvals[ip,j] = value
			 	endfor
			endfor
			q = where (newvals ne vals, count)
			if count gt 0 then begin
				; if anything changed, set them all instead of bothering to figure out which changed
				self -> set, fit_comp_params=float(newvals[*,0]), $
					fit_comp_minima=float(newvals[*,1]), $
					fit_comp_maxima=float(newvals[*,2]), $
					fit_comp_free_mask=byte(fix(newvals[*,3])), $
					strategy_name=state.table[ic].strategy_name
				*state.status_ptr = 1
			endif
		endif
	endfor

	; if just refreshing, go to widget update to get values from object
	if uvalue eq 'refresh' then goto, exit

	; if the uvalue is reset_xxx_yyy, then set reset_item=xxx, reset_mode=yyy, and uvalue=reset
	if strpos(uvalue,'reset') ne -1 then begin
		menu_id = widget_info(event.id, /parent)
		but_id = widget_info(menu_id, /parent)
		if but_id eq state.wallreset then ic = -99 else $
			ic = (where (state.table.wreset eq but_id, count))[0]
		val = str2arr(uvalue, '_')
		reset_item = val(1)
		reset_mode = val(2)
		uvalue = 'reset'
		*state.status_ptr = 1
	endif

endif

self -> units_widget, state.w_units, units=units, /getval

comp_plot = -1

compman_name = self -> get(/compman_name, class='fit_comp_manager')

case uvalue of

	'addcomp': begin
		ind = widget_info (state.wcomplist, /droplist_select)
		self -> set, fit_function='+' + state.fit_comps[ind]
		*state.status_ptr = 1
		end

	'listcomp': text_output, fit_model_components(/full), title='Function descriptions'

	'desc': begin
		widget_control, event.id, get_value=comp
		text_output, fit_model_components(/full, comp), title=comp + ' description'
		end

	'chg_value': if self->get(/spex_fitcomp_autoplot_enable) then $
		comp_plot = where (widget_info(event.id,/parent) eq state.table.wtable)

	'chg_spectrum': begin
		ic = where (state.table.wspectrum eq event.id)
		options = *state.table[ic].spectrum_opts
		sel = options[event.index]
    if sel eq 'Other' then sel = self -> xfit_comp_select_template_file()
    if sel ne '' then begin
		  self -> set, fit_comp_spectrum=sel, strategy_name=state.table[ic].strategy_name
		  if self->get(/spex_fitcomp_autoplot_enable) then comp_plot = ic
		  *state.status_ptr = 1
		endif
		end

  'change_spec_other': begin
    widget_control, event.id, get_value=sel
    if sel[0] ne '' && file_exist(sel) then begin
      ic = where (state.table.wchange_spec_other eq event.id)
      self -> set, fit_comp_spectrum=sel, strategy_name=state.table[ic].strategy_name
      if self->get(/spex_fitcomp_autoplot_enable) then comp_plot = ic
      *state.status_ptr = 1
    endif
    end      
  
  'browse_spec_other': begin
    sel = self -> xfit_comp_select_template_file()
    if sel ne '' then begin
      ic = where (state.table.wbrowse_spec_other eq event.id)
      self -> set, fit_comp_spectrum=sel, strategy_name=state.table[ic].strategy_name
      if self->get(/spex_fitcomp_autoplot_enable) then comp_plot = ic
      *state.status_ptr = 1
    endif
    end
    
	'chg_model': begin
		ic = where (state.table.wmodel eq event.id)
		options = *state.table[ic].model_opts
		self -> set, fit_comp_model=options[event.index], strategy_name=state.table[ic].strategy_name
		if self->get(/spex_fitcomp_autoplot_enable) then comp_plot = ic
		*state.status_ptr = 1
		end

	'reset': begin
		self -> xfit_comp_reset, state, reset_item, reset_mode, ic, status
		if self->get(/spex_fitcomp_autoplot_enable) and $
			(reset_item eq 'all' or reset_item eq 'val') and status eq 1 then comp_plot = ic
		end

	'delcomp': begin
		ic = (where (state.table.wdelete eq event.id, count))[0]
		self -> set, fit_function='-' + compman_name[ic], strategy_name=state.table[ic].strategy_name
		widget_control, state.wbase1, update=0
		for i = ic,19 do if xalive(state.table[i].wcompbase) then widget_control, state.table[i].wcompbase, /destroy
		widget_control, state.wbase1, update=1
		*state.status_ptr = 1
		end

	'plotcomp': begin
		comp_plot = (where (state.table.wplot eq event.id, count))[0]
		;self -> plotman, class='spex_fit', this_component=fit_comp_arr[ic], spex_units='flux'
		;self -> plotman, class='spex_fit', strategy_name=state.table[ic].strategy_name, spex_units='flux', /photon
		end
		
  'plot_dem': self -> plot_dem, /current 
  
  'plot_edf': begin
    ic = (where(state.table.wplot_edf eq event.id, count))[0]
    self -> plot_edf, /current, comp=compman_name[ic]
    end

	'change_erange': begin
	; as long as we only allow one erange, we don't need the stuff below.  We'll just use a range
	; widget directly.  But when we go to multiple eranges, stuff below works. graphical
	; selection does not work from this widget.
		;self -> set, spex_erange = event.value

		; can't use plotman__intervals here because we're in a blocking widget, and can't call
		; another blocking widget.  plotman_edit_int calls xtextedit which is modal, which is OK.
		erange = self -> get(/spex_erange)
		int_info = {nint: n_elements(erange)/2, $
			se: ptr_new(erange), $
			utbase: 0.d0, $
			plot_type: 'xyplot', $
			max_intervals: 1 }
		plotman_edit_int, int_info, err_msg=err_msg
		if err_msg ne '' then message, err_msg, /cont else begin
			se = *int_info.se
			xaxis = self -> get(/spex_ct_edges, /edges_2)
			for i =0, int_info.nint*2-1 do begin
				q = min (abs(se[i] - xaxis), index)
				se[i] = xaxis[index]
			endfor
			self -> set, spex_erange = se
			*state.status_ptr = 1
		endelse
		end
  
  'auto_set_erange': begin
    uname = widget_info(event.id, /uname)
    set_emin = (uname eq 'autoemin') or (uname eq 'autoeboth')
    set_emax = (uname eq 'autoemax') or (uname eq 'autoeboth')
    print,uname,set_emin,set_emax
;    choice = widget_info(state.w_autoset_choices, /droplist_select)
    self -> auto_erange, this_interval=self->get(/spex_interval_index), this_set_min=set_emin, this_set_max=set_emax
    end
    
	'draw_erange': begin
		;if not self->valid_plot(/xyplot) then $
			self -> plot_spectrum, /overlay_back, spex_units='flux'
		intervals = self -> get(/spex_erange)
		plotman_obj = self -> get_plotman_obj()
		if intervals[0] ne -1 then $
			plotman_draw_int,'all',{plotman_obj:plotman_obj}, intervals=intervals, type='Analysis'
		end

	'maxiter': begin
		self -> set, mcurvefit_itmax = event.value	;!!! make general for other than mcurvefit
		*state.status_ptr = 1
		end

	'uncert': begin
		self -> set, spex_uncert = event.value
		*state.status_ptr = 1
		end

	'autoplot': self -> set, spex_fitcomp_autoplot_enable = event.select

	'plot_units': self -> set, spex_fitcomp_plot_units = units

	'phot_plot': self -> set, spex_fitcomp_plot_photons = event.select

	'bk_plot': self -> set, spex_fitcomp_plot_bk = event.select
	
	'err_plot': self -> set, spex_fitcomp_plot_err = event.select

	'resid_plot': self -> set, spex_fitcomp_plot_resid = event.select

	'fit': begin
		*state.start_ptr = self -> get(/fit_comp, class='fit_comp_manager')
		if not spex_get_nointeractive() then $
			xmessage,['', '      Fitting ...       ', ''], wbase=wxmessage
		data = self->getdata(class='spex_fitalg', /force)
		*state.status_ptr = 2
		if xalive(wxmessage) then widget_control, wxmessage, /destroy
		if is_struct(data) then begin
			*state.final_ptr = self -> get(/fit_comp, class='fit_comp_manager')
			*state.fit_xdata = data.fit_xdata
			*state.fit_resid = data.fit_resid
			*state.fit_raw_resid = data.fit_raw_resid
			chisq = self->getalg(/chisq)

			if self -> get(/spex_fitcomp_plot_resid) then $
				self -> plot_resid, xdata=*state.fit_xdata, ydata=*state.fit_resid, $
					chisq=chisq, interval=self -> get(/spex_interval_index), $
					xrange=minmax(self -> getaxis(/ct_energy,/edges_2))

			if self->get(/spex_fitcomp_autoplot_enable) then $
				self -> plot_spectrum, /bksub, $
					overlay_back=self->get(/spex_fitcomp_plot_bk), $
					show_err=self->get(/spex_fitcomp_plot_err), $
					spex_units=self->get(/spex_fitcomp_plot_units), $
					photon=self->get(/spex_fitcomp_plot_photons), $
					/show_fit, use_fitted=0, chisq=chisq		; after fit, force showing chi-square
		endif
		end

	'plotall': self -> plot_spectrum, /bksub, $
		overlay_back=self->get(/spex_fitcomp_plot_bk), $
		show_err=self->get(/spex_fitcomp_plot_err), $
		spex_units=self->get(/spex_fitcomp_plot_units), $
		photon=self->get(/spex_fitcomp_plot_photons), $
		/show_fit, use_fitted=0

	'plotresid': begin
		if not ptr_exist(state.fit_xdata) then begin
			xmessage,'You have not done a fit - residuals are not available to plot.'
			return
		endif
		self -> plot_resid, xdata=*state.fit_xdata, ydata=*state.fit_resid, $
			chisq=self->getalg(/chisq), interval=self -> get(/spex_interval_index), $
			xrange=minmax(self -> getaxis(/ct_energy,/edges_2)), /main_window
		end
		
  'plotrawresid': begin
    if not ptr_exist(state.fit_xdata) then begin
      xmessage,'You have not done a fit - residuals are not available to plot.'
      return
    endif
    self -> plot_resid, xdata=*state.fit_xdata, ydata=*state.fit_raw_resid, /raw, $
      chisq=self->getalg(/chisq), interval=self -> get(/spex_interval_index), $
      xrange=minmax(self -> getaxis(/ct_energy,/edges_2)), /main_window
    end

	'quicksummary': begin
		if not ptr_exist(state.fit_xdata) then begin
			xmessage,'You have not done a fit yet.'
			return
		endif
		self -> quick_summary
		end

	'cancel': begin
		*state.status_ptr = 0
		; do fit function separately so the set can handle the multiple components
		self -> set, fit_function=arr2str(state.orig_str.fit_comp_function,'+')
		struct = rem_tag(state.orig_str, 'fit_comp_function')
		self -> set, _extra=struct
		widget_control, state.wbase, /destroy
		end

	'exit_stop': begin
		*state.status_ptr = 0
		widget_control, state.wbase, /destroy
		end

	'exit': begin
;		summ = self -> get(/spex_summ,class='spex_fit')
    ; since /justask is set, check_func doesn't need to change summ, so OK that we're passing an expression 
		self -> check_func, state.summ, dummy, /justask, yesno=yesno
		if yesno eq 'Yes'then widget_control, state.wbase, /destroy
		end

	else:
endcase

if comp_plot ne -1 then begin
	strategy_name = (comp_plot eq -99) ? '' : state.table[comp_plot].strategy_name
	self -> plot_spectrum, /bksub, $
		overlay_back=self->get(/spex_fitcomp_plot_bk), $
		show_err=self->get(/spex_fitcomp_plot_err), $
		spex_units=self->get(/spex_fitcomp_plot_units), $
		photon=self->get(/spex_fitcomp_plot_photons), $
		/show_fit, use_fitted=0, $
		strategy_name=strategy_name
endif

exit:
if xalive(wxmessage) then widget_control, wxmessage, /destroy
if xalive (state.wbase) then self -> xfit_comp_update_widget, state.wbase
end

;-----
; call with /internal if called from within spex_fit::process, so that widget will be blocked.
; if user calls directly (i.e. o->xfit_comp) widget isn't blocked.
; if user does a fit while in xfit_comp, the starting parameters (structure) and final
; parameters (an array) will be returned in start and final.
; On return, status = 0/1/2 means: 0 - cancelled, 1 - continue and do fit, 2 - continue,
; but use fit user did manually in xfit_comp.
; status comes in as 1, and gets set to 0 if click cancel, or 2 if do a fit.

pro spex_fit::xfit_comp, internal=internal, group=group, $
	start=start, final=final, summ=summ_struct, status=status, gui_label=gui_label, _extra=_extra

checkvar, status, 1

if xregistered ('xfit_comp') then begin
	xshow,'xfit_comp', /name
	return
endif

checkvar, gui_label, ''

;print,'starting xfit_comp ', systime(0)
get_font, font, big_font=big_font

; this takes longer since for multiple params, has to go through entire chain of
; objects (and can't specify class for fit_comp), so get params separately
;orig_str = self -> get(/fit_comp, /spex_erange, /spex_uncert, /mcurvefit_itmax)

str = {mcurvefit_itmax: self->get(/mcurvefit_itmax), $
			spex_uncert: self->get(/spex_uncert), $
			spex_erange: self->get(/spex_erange), $
			spex_fit_auto_erange: self->get(/spex_fit_auto_erange), $
			spex_fit_auto_emin: self->get(/spex_fit_auto_emin) }
fit_comp = self->get(/fit_comp,class='fit_comp_manager')
orig_str = join_struct(str, fit_comp)

wbase = widget_base (group=group, title='Fit Function Setup '+gui_label, /column, xpad=10, ypad=10, $
	/tlb_kill, /tlb_size_events)

tmp = widget_label (wbase, value='Choose Fit Function Components and Set Parameters', $
	font=big_font, /align_center)

winterval = widget_label (wbase, value='', /align_left, /dynamic_resize)
wcurrent = widget_label (wbase, value='', /align_left, /dynamic_resize)

wbase_add = widget_base (wbase, /row, space=10)

wcomplist = widget_droplist (wbase_add, title='Choose: ', $
	value=fit_model_components(/one_line), uvalue='whichcomp')

tmp = widget_button (wbase_add, value='Add', uvalue='addcomp')

tmp = widget_button (wbase_add, value='List', uvalue='listcomp')

wbase1 = widget_base (wbase, /column, /scroll) ; for populating in xfit_comp_update_widget

w_optbase = widget_base (wbase, /row, space=2)

w_erange_base = widget_base (w_optbase, /row, space=10,  /frame)

w_bands_base = widget_base (w_erange_base, space=0, /row)
w_nbands = widget_label (w_bands_base, value='', /dynamic_resize)
w_erange = widget_droplist (w_bands_base, title='Energy range(s) to fit:', $
					value='xxxxxxxxxxxxxxxxxxxxx', uvalue='none')
tmp = widget_button (w_erange_base, value='Change', uvalue='change_erange')

;w_autobase = widget_base(w_erange_base, /row, space=0, /frame)
;w_tmp = widget_label(w_autobase, value='Auto-set: ')
;w_autoset_base = widget_base (w_autobase, /row, /nonexclusive, space=0)
;w_auto_erange = widget_button (w_autoset_base, value='Max', uvalue='auto_erange', $
;  tooltip='Automatically set upper limit of energy range based on bk-subtracted counts/bin. ' + $
;   'Change threshold via spex_fit_auto_emax_thresh parameter.')
;w_auto_emin = widget_button (w_autoset_base, value='Min', uvalue='auto_emin', $
;  tooltip='Automatically set lower limit of energy range based on background-subtracted counts/bin. (RHESSI only)')

;w_autobase = widget_base(w_erange_base, /row);, space=5, frame=3)
;w_tmp = widget_button(w_autobase,  value='Auto-set:', uvalue='auto_set_erange', $
;  tooltip='FOR THIS INTERVAL ONLY: Automatically set upper limit of energy range based on bk-subtracted cts/bin and/or lower limit based on atten state (RHESSI only for lower)')
;w_autoset_choices = widget_droplist(w_autobase, value=['Max E','Min E','Both'])
;widget_control, w_autoset_choices, set_droplist_select=choice

w_automenu = widget_button(w_erange_base, value='Auto-set Erange ->', /menu)
tmp = widget_button(w_automenu, $
  value='Set Emin value based on atten state in this interval (RHESSI only)', uvalue='auto_set_erange', uname='autoemin')
tmp = widget_button(w_automenu, $
  value='Set Emax value based on count rate in this interval (use spex_fit_auto_emax_thresh param to set threshold)', uvalue='auto_set_erange', uname='autoemax')
tmp = widget_button(w_automenu, $
  value='Set Emin and Emax based on this interval', uvalue='auto_set_erange', uname='autoeboth')
tmp = widget_button(w_automenu, $
  value='  Note 1: These Emin, Emax values may persist in subsequent intervals depending on param init options.')
tmp = widget_button(w_automenu, $
  value='  Note 2: This does not set the spex_fit_auto_erange or spex_fit_auto_emin flag.')
;tmp = widget_button(w_automenu, value='Auto-set Emin', uvalue='auto_set_erange', uname='autoemin')
;tmp = widget_button(w_automenu, value='Auto-set Emax', uvalue='auto_set_erange', uname='autoemax')
;tmp = widget_button(w_automenu, value='Auto-set Emin and Emax', uvalue='auto_set_erange', uname='autoeboth')


tmp = widget_button (w_erange_base, value='Show', uvalue='draw_erange')

w_iterbase = widget_base (w_optbase, /row, /frame, space=0)

w_maxiter = cw_edroplist (w_iterbase, value=1, format='(i4)', $
	xsize=4, label='# Iter: ', drop_values=[0,1,5,10,20,50,100,200,300], uvalue='maxiter')

w_uncert = cw_edroplist (w_iterbase, value=.05, format='(f4.2)', $
	xsize=5, label='Uncert: ', drop_values=[0.0,.01,.02,.03,.04,.05,.06,.07,.08,.09,.10], $
	uvalue='uncert')

;w_optbase = widget_base (wbase, /row, space=10)
w_optbase2 = widget_base (wbase, /row, /frame, space=8);, /align_center)

w_plot_base0 = widget_base (w_optbase2, /row, /nonexclusive, space=0)
w_autoplot = widget_button (w_plot_base0, value='Auto Plot', uvalue='autoplot')

self -> units_widget, w_units, parent=w_optbase2, $
	units=self->get(/spex_fitcomp_plot_units), uvalue='plot_units'

w_plot_col1 = widget_base (w_optbase2, /row, /nonexclusive, space=0)
w_phot = widget_button (w_plot_col1, value='Photons', uvalue='phot_plot', $
	sensitive=(self->get(/spex_deconvolved) ne 1))
w_bk = widget_button (w_plot_col1, value='Background', uvalue='bk_plot')
w_err = widget_button (w_plot_col1, value='Error', uvalue='err_plot')
w_resid = widget_button (w_plot_col1, value='Residuals', uvalue='resid_plot')

; fill in current settings of spex_fitcomp values
struct = self -> get(/spex_fitcomp)
widget_control, w_autoplot, set_button = struct.spex_fitcomp_autoplot_enable
;index = str_fuzzy (['co', 'ra', 'fl'], struct.spex_fitcomp_plot_units)
;if index eq -1 then index=1
;widget_control, w_units, set_droplist_select=index
widget_control, w_phot, set_button = struct.spex_fitcomp_plot_photons
widget_control, w_bk, set_button = struct.spex_fitcomp_plot_bk
widget_control, w_err, set_button = struct.spex_fitcomp_plot_err
widget_control, w_resid, set_button = struct.spex_fitcomp_plot_resid


wbase_but = widget_base (wbase, /row, space=15);, /align_center)
tmp = widget_button (wbase_but, value='Refresh', uvalue='refresh')
tmp = widget_button (wbase_but, value='Fit', uvalue='fit')
; create button with menu of options for resetting all components
self->xfit_comp_reset_button, wbase_but,'Reset All Comp.', wallreset
tmp = widget_button (wbase_but, value='Plot All', uvalue='plotall')
w_resid_choice = widget_button (wbase_but, value='Plot Resid ->', /menu)
tmp = widget_button (w_resid_choice, value='Normalized', uvalue='plotresid')
tmp = widget_button (w_resid_choice, value='Raw', uvalue='plotrawresid')
tmp = widget_button (wbase_but, value='Fit Summary', uvalue='quicksummary')
;tmp = widget_button (wplot_but, value='Separately', uvalue='plotall_sep')
;tmp = widget_button (wplot_but, value='Combined', uvalue='plotall_comb')
;w_plotall = widget_button (wplot_but, value='Separately and Combined', uvalue='plotall')
w_accept = widget_button (wbase_but, value='Accept ->', /menu)
tmp = widget_button (w_accept, value='and store fit. Continue looping through intervals.', uvalue='exit')
tmp = widget_button (w_accept, value=" changes to values but don't store fit. Stop looping.", uvalue='exit_stop')
tmp = widget_button (wbase_but, value='Cancel', uvalue='cancel')

table = replicate( $
	{func: '', $
	strategy_name: '', $
	npar: 0, $
	wcompbase: 0L, $
	wtable: 0L, $
	wids: ptr_new(/alloc), $	; widget id of each cell in table, (nparm,4)
	vals: ptr_new(/alloc), $	; snapshot val of each cell in table, (nparm,4)
	wspectrum: 0L, $
	spectrum_opts: ptr_new(/alloc), $
	wchange_spec_other: 0L, $
	wbrowse_spec_other: 0L, $
	wmodel: 0L, $
	model_opts: ptr_new(/alloc), $
	wreset: 0L, $
	wdelete: 0L, $
	wplot: 0L, $
	wplot_dem: 0L, $
	wplot_edf: 0L }, $
	40)

xfit_widget_id = keyword_set(internal) ? get_handler_id('xfit') : 0L

red_bmp = bytarr(7,7,3)  &  red_bmp[*,*,0] = 240  ; red 7x7 bit map
green_bmp = bytarr(7,7,3) & green_bmp[*,*,1] = 240 ; green 7x7 bitmap
  
state = {wbase: wbase, $
	winterval: winterval, $
	wcurrent: wcurrent, $
	wbase1: wbase1, $
	wcomplist: wcomplist, $
	w_erange: w_erange, $
	w_nbands: w_nbands, $
;	w_auto_erange: w_auto_erange, $
;	w_auto_emin: w_auto_emin, $
;	w_autoset_choices: w_autoset_choices, $
	w_maxiter: w_maxiter, $
	w_uncert: w_uncert, $
	fit_comps: fit_model_components(), $
	table: table, $
	object: self, $
	w_units: w_units, $
	wallreset: wallreset, $
;	plotman_resid: obj_new(), $
	fit_xdata: ptr_new(/alloc), $
	fit_resid: ptr_new(/alloc), $
	fit_raw_resid: ptr_new(/alloc), $
	orig_str: orig_str, $
	summ: exist(summ_struct) ? summ_struct : self->get(/spex_summ), $
	start_ptr: ptr_new(/alloc), $  ;if did fit, will contain structure of starting values
	final_ptr: ptr_new(/alloc), $  ;if did fit, will contain array of resulting fit params
	status_ptr: ptr_new(status), $  ; 0/1/2 = cancelled / do fit after return / did fit
	gui_label: gui_label, $
	green_bmp: green_bmp, $
	red_bmp: red_bmp }

widget_control, wbase, set_uvalue=state

self -> xfit_comp_update_widget, wbase

widget_offset, group, newbase=wbase, xoffset, yoffset

widget_control, wbase, xoffset=xoffset, yoffset=yoffset

widget_control, wbase, /realize

; Desensitize xfit widget. Really want to block here, but can't seem to make it work -
; this will make it so user can't interact with xfit.  Resensitize it after killing this widget.
if xalive(xfit_widget_id) then widget_control, xfit_widget_id, sensitive=0

xmanager, 'xfit_comp', wbase, no_block=(keyword_set(internal) eq 0), cleanup='xfit_comp_cleanup'

if xalive(xfit_widget_id) then widget_control, xfit_widget_id, /sensitive

status = *state.status_ptr

; status eq 0/1/2 means  user cancelled / do fit after return / did fit
;print,'status on leaving xfit_comp = ', status

if status eq 2 then begin
	start = *state.start_ptr
	final = *state.final_ptr
endif

ptr_free, state.start_ptr
ptr_free, state.final_ptr
ptr_free, state.status_ptr
end
