; Modifications:
; 28-Nov-2009, Kim.  Added spex_data_sel to select data type from within a file
;-

function spex_data_strategy_control

struct = {spex_data_strategy_control}

struct.spex_specfile = ptr_new('')
struct.spex_data_sel = ''


return, struct

end
