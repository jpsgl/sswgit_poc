;+
; Name: read_hxrbs_4_ospex
;
; Purpose: Read hxrbs flare FITS files into ospex.  Called by spex_smm_hxrbs_specfile::read_data
; 
; Written: Kim Tolbert19-Feb-2013
; Modifications:
; 
;-

pro read_hxrbs_4_ospex, FILES=files, data_str=data_str, ERR_CODE=err_code,$
                   ERR_MSG=err_msg, _REF_EXTRA=_ref_extra, verbose=verbose
                   
data_str = -1
err_code = 0
err_msg = ''
                  
rd_hxrbs_fits, files, ut_mid, counts, livetime, edges, deltat

nt = n_elements(ut_mid)
tbin = .512 
;utedges = get_edges([ut_mid-tbin/2., max(ut_mid) + tbin/2.], /edges_2)
ut_edges = transpose([[ut_mid - tbin/2.],[ut_mid+ tbin/2.]])

nchan = n_elements(edges[0,*])
ltime = livetime ## (fltarr(15)+1)

hxrbs_response, edges=edges, omatrix=drm, area=area, e_matrix= ph_edges, ut_date=ut_mid[0] 
                              
data_str = { $
  start_time: anytim(ut_edges[0,0], /vms), $
  end_time: anytim(ut_edges[1,nt-1], /vms), $
  rcounts: counts, $
  ercounts: sqrt(counts), $
  ut_edges: ut_edges, $
  units: 'counts', $
  area: area, $
  ltime: ltime, $
  ct_edges: edges, $
  title: 'SMM HXRBS', $
  respfile: drm, $
  detused: 'HXRBS', $
  atten_states: -1 }
  
end