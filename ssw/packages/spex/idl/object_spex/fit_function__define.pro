;===============================================================================
pro fit_function_test,o
o=obj_new('fit_function')
o->set,fit_function='+bpow'
print,o->get(/fit_comp_param)
help,o->get(/fit_comp,comp_name='bpow'),/st
o->set,fit_xvals=findgen(10)+2.
d1 = o->getdata(comp_name='vth')
print,d1
;d2 = o->getdata(strategy_name='vth_0')
;b1 = o->getdata(comp_name='bpow')
;print,b1,d1, b1+d1, o->getdata()

end

;------------------------------------------------------------------------------
function fit_function::init, _extra=_extra

ret = self -> framework::init (source = obj_new('fit_comp_manager'), $
								control={fit_function_control}, $
								info={fit_function_info} )

if keyword_set(_extra ) then self -> set, _extra=_extra

if self -> framework::get(/fit_function) eq '' then self -> set, fit_function='vth'

return, 1

end

;------------------------------------------------------------------------------

; fit_function can look like 'vth' or 'vth+bpow' or '-vth'  or '+bpow' or ...
;   If doesn't start with + or -, then we're deleting old component objects and starting fresh.
;   Any function preceded by + gets added to current functions,
;   Any function preceded by - gets deleted from current functions
;   ',' in list means same as '+'
; If strategy_name is supplied when deleting a component, this strategy name for that
;   component will be deleted (in case there are multiple components of same type)
;   Otherwise, strategy_name is used for setting values for that particular component
;   strategy, and so strategy_name is passed on to framework::define

pro fit_function::set, $
	fit_function=fit_function, $
	strategy_name=strategy_name, $
	done=done, not_found=not_found, $
	_extra=_extra

if keyword_set(fit_function) then begin
	fit_comp_man = self -> get(/obj, class='fit_comp_manager')

	fit_function = strcompress(/remove_all, fit_function)
	s = str_replace(fit_function, '+', ' +')
	s = str_replace(s, ',', ' +')
	s = str_replace(s, '-', ' -')
	sep_func =  str_sep(s,' ')
	if sep_func[0] eq '' then sep_func = sep_func[1:*]

	add = strmid(sep_func,0,1) eq '+' & delete = strmid(sep_func,0,1) eq '-'
	sep_func = str_replace(sep_func,'+','') & sep_func = str_replace(sep_func,'-','')

	nfunc = n_elements(sep_func)

	if not(add[0] or delete[0]) then begin ; we're creating a fresh list of components
		fit_comp_man -> delete_comp
		add[0] = 1
	endif

	;  now add and/or subtract components
	for i=0,nfunc-1 do begin

		if add[i] then fit_comp_man -> add_comp, sep_func[i]

		if delete[i] then  fit_comp_man -> delete_comp, sep_func[i], strategy_name=strategy_name

	endfor

	fit_comp_function = self -> get(/fit_comp_function)
	new_fit_function = size(fit_comp_function, /tname) eq 'STRING' ? arr2str(fit_comp_function, '+') : ''
	self -> framework::set, fit_function=new_fit_function, done=done, not_found=not_found
endif

if keyword_set(_extra) then self->framework::set, _extra=_extra, strategy_name=strategy_name, $
	done=done, not_found=not_found

end

;------------------------------------------------------------------------------

function fit_function::get_fit_param_defaults, all=all, _extra=_extra

struct = fit_function_query (self -> get(/fit_function), /defaults)
if keyword_set(all) then return, struct else return, struct.fit_comp_params
end

;------------------------------------------------------------------------------

pro fit_function::set_fit_param_defaults, all=all, _extra=_extra

def = self -> get_fit_param_defaults(all=all)
if keyword_set(all) then $
	self -> set, _extra=def $
else $
	self -> set, fit_comp_params = def

end

;------------------------------------------------------------------------------

function fit_function::getaxis, _extra=_extra
if keyword_set(_extra) then $
	return, get_edges(self -> get(/fit_xvals), _extra=_extra) else $
	return, get_edges(self -> get(/fit_xvals), /mean)
end

;------------------------------------------------------------------------------

; this isn't needed anymore??? 9-Mar-2004,Kim
;function fit_function::get_fit_comp_arr, _extra=_extra
;
;return, str2arr(self->framework::get(/fit_function), '+')
;end

;------------------------------------------------------------------------------
; REPLACE by spex_get_comp_index function (not method)
; function to find whether one of the tag names in _extra is a function comp name, and if so,
; return the index into the function component array that it is, and the function name
;function fit_function::get_comp_index, _extra=_extra, comp_name=comp_name
;
;fit_comp_arr = self->get_fit_comp_arr()
;ind = where_arr( strlowcase(fit_comp_arr), strlowcase(tag_names(_extra)) )
;comp_name = ind[0] ne -1 ? fit_comp_arr[ind[0]] : ''
;return, ind[0]
;end

;------------------------------------------------------------------------------

; added this_component so that routines can call this with a particular function without
; having to construct a keyword string like /vth which would require the execute function.
;function fit_function::get, $
;	this_component=this_component, $
;	found=found, not_found=not_found, $
;	_extra=_extra
;
;index_olist = -1
;
;if keyword_set(_extra) then $
;	index_olist = spex_get_comp_index(self->framework::get(/fit_function), $
;		this_component=this_component, $
;		_extra=_extra, $
;		comp_name=comp_name)
;
;;	if index ne -1 then begin
;;		index_olist = index
;;		_extra = rem_tag (_extra, comp_name)
;;		if size(_extra, /tname) eq 'STRUCT' then new_extra = _extra
;;	endif else new_extra=_extra
;;endif
;
;; set index of object in linkedlist that we want to work with.  -1 means all
;self->framework::set, index_olist=index_olist[0]
;
;return, self->framework::get(_extra=_extra, found=found, not_found=not_found)
;
;end

;;------------------------------------------------------------------------------
;
;function fit_function::get_fit_components, all=all, full=full, print=print
;
;case 1 of
;
;	keyword_set(full): begin
;		val = fit_model_components(/full)
;		end
;
;	keyword_set(all): val = fit_model_components()
;
;	else: val = self->get(/fit_function)
;
;endcase
;
;if keyword_set(print) then prstr, val, /nomore
;return, val
;
;end

;------------------------------------------------------------------------------

function fit_function::getdata, _extra=_extra

@spex_insert_catch

; need force because we may have a strategy or component name in _extra,
; and we only want getdata for that component, but need_update won't be set.
; don't want to set need_update=1 because then all objects above this
; think they need to update every time.
data = self -> framework::getdata(_extra=_extra, /force)

return, data
end

;@spex_insert_catch
;
;if keyword_set(_extra) then self->framework::set, _extra=_extra
;
;result = self -> framework::getdata (class='fit_comp_manager', $
;	_extra=_extra)
;
;self -> setdata,result
;
;units = {spex_units}
;units.data_name = 'Function'
;units.data_type = 'flux'
;units.data = 'photons cm!u-2!n s!u-1!n keV!u-1!n'
;self -> set, fit_func_units=units
;
;;self -> set, need_update=0
;
;return, result
;
;end

;------------------------------------------------------------------------------

pro fit_function::process, _extra=_extra
source = self -> get(/source)
result = source -> getdata(_extra=_extra)
;
;fit_comp_x = self->get(/fit_xvals)
;
;result = self -> framework::getdata (class='fit_comp_manager', fit_comp_x=fit_comp_x, _extra=_extra)
;
self -> setdata,result

units = {spex_units}
units.data_name = 'Function'
units.data_type = 'flux'
units.data = 'photons cm!u-2!n s!u-1!n keV!u-1!n'
self -> set, fit_func_units=units
end

;------------------------------------------------------------------------------

function fit_function::plot_setup, plot_params=plot_params,  _extra=_extra

error_catch = 0
if spex_get_debug() eq 0 then catch, error_catch
if error_catch ne 0 then begin
	catch, /cancel
	message, !error_state.msg, /cont
	;message,'Error making plot.', /cont
	return, 0
endif

strat_arr = ['', self -> get(/compman_strategy)]
n_comp = n_elements(strat_arr)	; number of components to plot

xvals = self -> get(/fit_xvals)
nx = n_elements(xvals)/2
if nx le 1 then message,'Error.  No fit_xvals stored in fit_function object yet.'

fit_function = self->get(/fit_function)
yvals = fltarr(nx, n_comp)
id = strarr(n_comp)
for i = 0,n_comp-1 do begin
	yvals[*,i] = self -> getdata(strategy_name=strat_arr[i])
	comp_params = self -> getfit(/fit_comp_params, strategy_name=strat_arr[i])
	str_params = ' ' + arr2str (trim(comp_params, '(g9.3)'), ',')
	;strat_arr[i] = '' is the sum of all components.  If there's only one, show the parameters
	; next to it, otherwise, show the parameters next to the separate component they apply to.
	if strat_arr[i] eq '' then begin
		id[i] = n_comp eq 1 ? fit_function + str_params : fit_function
	endif else id[i] = self->strat2name(strategy_name=strat_arr[i]) + str_params
endfor
fit_func_units = self -> get(/fit_func_units)
plot_params = { $
	plot_type: 'xyplot', $
	xdata: xvals, $
	ydata: yvals, $
	id: 'FIT_FUNCTION', $
	data_unit: fit_func_units.data, $
	dim1_id: id, $
	dim1_enab_sum: 0, $
	xlog: 1, $
	ylog: 1, $
	xtitle: 'Energy (keV)', $
	legend_loc: 3 $
	}

return, 1
;if keyword_set(plotman) then begin
;	plotman_obj = self -> get_plotman_obj(valid=valid)
;	if valid then begin
;		status = plotman_obj -> setdefaults (input=fitplot_obj, plot_type='xyplot')
;		plotman_obj -> set, xtitle='Energy (keV)', psym=0, _extra=_extra
;		plotman_obj -> new_panel, fitplot_obj->get(/id), /replace
;		return
;	endif
;endif
;
;fitplot_obj -> plot, psym=0, _extra=_extra
;self -> do_plot, fitplot_obj, 'xyplot', fitplot_obj->get(/id), xtitle='Energy (keV)', psym=0, _extra=_extra

end

;------------------------------------------------------------------------------

pro fit_function__define

struct = { fit_function, $
	inherits spex_gen }

end
