;+
; Name: spex_splash
;
; Purpose:  Put up a splash screen on initial window in spex GUI
;
; Call:  spex_splash
;
; Kim Tolbert, 14-Apr-2003
;-


pro spex_splash

device,get_visual_name=visual_name
truecolor = visual_name eq 'TrueColor'

font_save = !p.font
!p.font = 0

if truecolor then begin
	tvlct, r, g, b, /get
	linecolors
endif

hsi_ui_getfont, font, big_font, huge_font=huge_font

device, set_font=huge_font
color = truecolor ? 2 : 255
xyouts, .5, .9, 'OSPEX', charsize=2., color=color, /normal, align=.5

xyouts, .5, .75, 'Spectral Data Analysis Package', charsize=2., color=color, /normal, align=.5

device, set_font=big_font
color = truecolor ? 5 : 255
text = ['Use the buttons under File to:', $
	'!C!C   1. Select Input Data Files', $
	'!C!C!C   2. Define Background and Analysis Intervals,', $
	'!C!C!C!C        and Select Fit Function Components', $
	'!C!C!C!C!C   3. Fit data', $
	'!C!C!C!C!C!C   4. View Fit Results', $
	'!C!C!C!C!C!C!C   5. Save Session and Results']
text2 = ['Use Plot_Control buttons to change display of current plot.', $
	'!CUse Window_Control buttons to redisplay previous plots.']
xyouts, .02, .65, text, charsize=2., color=color, /normal

device, set_font=font
xyouts, .02, .2, text2, charsize=2., color=color, /normal

!p.font = font_save
if truecolor then tvlct, r, g, b

;set font back to normal size for device
device, set_font=font

end
