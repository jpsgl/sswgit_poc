;+
;
; NAME:
;   spex_yohkoh_wbs_specfile__define
;
; PURPOSE:
;   Provides read_data method for the yohkoh wbs instruments - GRS1, GRS2, HXS.
;   User should have already set spex_data_sel to one of those three options.
;   (In GUI, xinput widget pops up widget for user's choice and sets spex_data_sel.) 
;   If not, aborts.
;   NOTE: This procedure is based on spex_hessi_specfile__define.
;
;
; CATEGORY:
;       SPECTRAL FITTING, SPEX - yohkoh_wbs
;
; CALLING SEQUENCE:
;
; CALLS:
;   read_yohkoh_4_spex (which is an old spex routine, and is in old spex directory)
;
; INPUTS:
;
; OUTPUTS:
;
; INPUT KEYWORDS:
;
; OUTPUT KEYWORDS:
;
; PROCEDURE:
;
; Written 28-Nov-2009  - Kim Tolbert
;
; Modification History:
; 
;------------------------------------------------------------------------------

pro spex_yohkoh_wbs_specfile_test, o, _extra=_extra

  o =  obj_new( 'spex_yohkoh_wbs_specfile' )
  o->set,  spex_spec =  'c:\Analysis\working\wbs\wda971106.1111'
  data =  o->getdata()

end

;------------------------------------------------------------------------------

pro spex_yohkoh_wbs_specfile::read_data, $
                       spectrum,  errors,  livetime,  $
                       spex_respinfo, spex_file_time,  spex_ut_edges,  spex_ct_edges,  $
                       spex_area, spex_title, spex_detectors,  $
                       spex_interval_filter, spex_units, spex_data_name, $
                       spex_deconvolved, spex_pseudo_livetime, spex_data_pos, $
                       err_code=err_code

common wbs_response, iexp_com, date_com, drm_com, e_out_com, e_in_com

file =  self -> get( /spex_specfile )
data_sel = self -> get(/spex_data_sel)

err_code = 1
err_msg = ''

data_sel = strupcase(data_sel)
types = ['GRS1','GRS2','HXS']
if not is_member(data_sel, types) then $
  err_msg = 'Invalid data selection = ' + data_sel + ', aborting...  Set spex_data_sel to one of ' + arr2str(types)
dformat = strmid(data_sel,0,3)
det_id = fix(strmid(data_sel+'0', 3, 1)) 

if err_msg ne '' then begin
  message, err_msg, /cont
  return
endif

adet = dformat + ((dformat eq 'GRS') ? trim(det_id) : '')

read_yohkoh_4_spex, dformat=dformat, files=file, $
  det_id=det_id, new=0, $
  flux=flux, eflux=eflux, ut=ut, units=units, area=area, $
  ltime=ltime, edges=edges, wchan=wchan, id_use=id_use, title=title, $
  /auto, /noplot, mode=mode, error=err_code, _extra = _extra 

  if err_code ne 0 then begin
      ;if not spex_get_nointeractive() then xmessage,err_msg else print,err_msg
      err_msg = 'Error reading YOHKOH file ' + file[0]
      return
  endif

ntime = n_elements( ut )/2
nchan = n_elements( edges )/2

delta_light = get_edges( edges, /width ) # ( 1+fltarr(ntime) )
ltime = ltime ## (1+fltarr(nchan))
; ltime is in seconds, it's not a fractional livetime
spectrum =  flux * ltime * area * delta_light
errors =  eflux * ltime * area * delta_light
livetime =  ltime

spex_respinfo = 1  ; will be filled in in spex_drm::array_drm
spex_file_time = minmax(ut)
spex_ut_edges = ut
spex_ct_edges = edges
spex_area = area
spex_title = 'Yohkoh Spectrum'
spex_detectors = adet
spex_units = 'counts'
spex_interval_filter = -1
spex_data_name = 'Yohkoh'
spex_deconvolved = 0
spex_pseudo_livetime = 0
spex_data_pos = [0.,0.]

end

;------------------------------------------------------------------------------
pro spex_yohkoh_wbs_specfile__define

  self = {spex_yohkoh_wbs_specfile, $
          INHERITS spex_data_strategy }

END
