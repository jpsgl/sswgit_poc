;+
; Function to return information about a function (which can consist of multiple
; components, e.g. vth+bpow).
; Keywords:
; nparams - if set, return the total number of parameters
; ncomp - if set, return the total number of function components
; defaults - if set, return a structure with default values
; param_index - if set, return arrays of start/end values of parameters for each component
;    in form (nfunc,2).  If comp set, return indices into parameters for just that component
;    for example - a =fit_function_query('vth+bpow', /param_index) returns
;      a[0,*] = [0,1]  - parameters for 'vth'
;      a[1,*] = [2,5]  - parameters for 'bpow'
;    a = fit_function_query('vth+bpow', /param_index, comp='bpow') returns
;      a = [2,5] meaning parameters for 'bpow' are in elements 2 through 5 of param array
; comp - used with param_index or param_elem. Can be one of the following:
;    name of the component - e.g. 'line' (assume only one or first instance of line in func)
;    number of the component (from 0), e.g. if func='vth+bpow+line+line', comp=3 means 2nd line
;    name # number - e.g. if function has two line components, line#0 means the
;    first line component, line#1 means the second
; param_elem - if set, return the array of element numbers (not start/end indices like param_index returns)
;    Useful when comp is passed (otherwise it's just an indgen of the number of parameters)
; param_desc - if set, return an array of parameter descriptions for all parameters for all components
; param_num - if set, returns each comp name followed by P0, P1, etc for each parameter, for all comps
;   e.g. a=fit_function_query('vth+bpow',/param_num)
;        help,a
;          A               STRING    = Array[7]
;       print,arr2str(a,', ') 
;       vth P0, vth P1, vth P2, bpow P0, bpow P1, bpow P2, bpow P3
;
; ...
; Modifications:
;  20-Mar-2006, Kim.  Added ncomp keyword
;  4-Feb-2008, Kim.  Comp can now include information about which comp we want if there
;    are multiple instances of comp.  comp='line#2' means the 3rd instance of line
;  13-Sep-2012, Kim. Added param_desc and param_num keywords
;  26-Aug-2013, Kim. Added param_elem keyword
;-

function fit_function_query, fit_function, nparams=nparams, ncomp=ncomp, $
	defaults=defaults, param_index=param_index, comp=comp, param_elem=param_elem, $
	param_desc=param_desc, param_num=param_num

fit_comp_arr = str2arr(fit_function, '+')

num_params = 0
for i=0,n_elements(fit_comp_arr)-1 do num_params = num_params + fit_comp_defaults(fit_comp_arr[i], /nparam)
    
if keyword_set(nparams) then return, num_params

if keyword_set(ncomp) then return, n_elements(fit_comp_arr)

if keyword_set(defaults) then begin
	if n_elements(fit_comp_arr) eq 1 then return, fit_comp_defaults(fit_function) else $
		; if more than one component, let fit_function object concat values from separate components
		return, fit_function_defaults(fit_function)
endif

if keyword_set(param_index) or keyword_set(param_elem) then begin
	if exist(comp) then begin
		comp2 = comp ; don't want to change input variable
		if is_string(comp2) then begin
			if strpos(comp2,'#') gt -1 then $
				comp2 = ssw_strsplit(comp2, '#', tail=which_comp) else which_comp = 0
			q = where(fit_comp_arr eq trim(comp2), count)
			if count eq 0 then return, -1
			q = q[which_comp]
		endif else begin
			q = comp2
			if q gt n_elements(fit_comp_arr)-1 then return, -1
		endelse
	endif
	ind = 0
	for i = 0,n_elements(fit_comp_arr)-1 do begin
		nparam = fit_comp_defaults(fit_comp_arr[i], /nparam)
		i1 = append_arr(i1, ind)
		i2 = append_arr(i2, ind+nparam-1)
		ind = ind + nparam
	endfor
	if exist(comp) then begin
	  is = i1[q[0]] & ie = i2[q[0]]
	  return, keyword_set(param_elem) ? is + indgen(ie-is+1): [is,ie]
	endif else return, keyword_set(param_elem) ? indgen(num_params) : [[i1],[i2]]
endif

if keyword_set(param_desc) then begin
  for i=0,n_elements(fit_comp_arr)-1 do begin
    fit_struct = fit_model_components(fit_comp_arr[i], /struct)
    desc = append_arr(desc, fit_comp_arr[i] + ': ' + fit_struct.param_desc)
  endfor
  return, desc
endif

if keyword_set(param_num) then begin
  for i=0,n_elements(fit_comp_arr)-1 do begin
    nparam = fit_comp_defaults(fit_comp_arr[i], /nparam)
    pnum = append_arr(pnum, fit_comp_arr[i] + ' P' + trim(indgen(nparam)))
  endfor
  return, pnum
endif
 
return,0

end