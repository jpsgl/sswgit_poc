;+
; NAME:
;	F_Thick2_rc_DEFAULTS
;
; PURPOSE: Function to return default values for
;   parameters, minimum and maximum range, and free parameter mask when
;   fitting to F_Thick2_vnorm function.
;   
;   *** To use a single power-law electron distribution, it is better to change the break energy to a value that is equal to
;           or larger than the high energy cutoff. Or one can set the low delta and the high delta to same value.
;   *** eref (7th param) should be between low energy cutoff and break energy, but should not be a free param when fitting.
;
; CALLING SEQUENCE: defaults = f_thick2_rc_defaults()
;
; INPUTS:
;	None
; OUTPUTS:
;	Structure containing default values
;
; MODIFICATION HISTORY:
; Kim Tolbert, 8-Jan-2013, copied from f_thick2_vnorm_defaults and modified, added 7th param
;
;-
;------------------------------------------------------------------------------

function f_Thick2_rc_defaults

defaults = { $
  fit_comp_params:      	 [1.,  4., 150., 6., 20., 3.2e4, 50.], $
  fit_comp_minima:           [1e-10,  1.1, 1., 1.1, 1., 100., 1.], $
  fit_comp_maxima:           [1e10,  20,  1e5, 20,  1e3, 1e7, 1.e5], $
  fit_comp_free_mask:        [1,1,1,1,1,0,0] $
  }

return, defaults

end