;+
;
; NAME:
;   spex_xsm_specfile__define
;
; PURPOSE:
;   Provides read_data method for the XSM instrument.
;   NOTE: This procedure is based on spex_hessi_specfile__define.
;
;
; CATEGORY:
;       SPECTRAL FITTING, SPEX - XSM
;
; CALLING SEQUENCE:
;
; CALLS:
;   read_xsm_4_ospex
;
; INPUTS:
;
; OUTPUTS:
;
; INPUT KEYWORDS:
;
; OUTPUT KEYWORDS:
;
; PROCEDURE:
;
; Written 23-Nov-2004  - Sandhia Bansal
;
; Modification History:
; Sandhia Bansal - 12/08/04 - Specified a xsm file in the test routine.
; 12-May-2008, Kim.  Added new arguments to read_data method for compatibility w/ spex_data_strategy
; 25-Feb-2010, Kim. Added file_index arg to read_data.
;------------------------------------------------------------------------------

pro spex_xsm_specfile_test

  o =  obj_new( 'spex_xsm_specfile' )
  o->set,  spex_spec =  'DT_24_05_2004.fits'
  data =  o->getdata()

end

;------------------------------------------------------------------------------

pro spex_xsm_specfile::read_data, file_index=file_index, $
                       spectrum,  errors,  livetime,  $
                       spex_respinfo, spex_file_time,  spex_ut_edges,  spex_ct_edges,  $
                       spex_area, spex_title, spex_detectors,  $
                       spex_interval_filter, spex_units, spex_data_name, $
                       spex_deconvolved, spex_pseudo_livetime, spex_data_pos, $
                       err_code=err_code, _extra=_extra

  file =  self -> get( /spex_specfile )
  checkvar, file_index, 0
  file = file[file_index]

  read_xsm_4_ospex, FILES = file, $
                      _EXTRA = _ref_extra, $
                      data_str = data_str, $
                      ERR_CODE = err_code, $
                      ERR_MSG = err_msg

  if err_msg[0] ne '' then begin
      ;if not spex_get_nointeractive() then xmessage,err_msg else print,err_msg
      message, err_msg, /cont
      if err_code then return
  endif

  spectrum =  data_str.rcounts
  errors =  data_str.ercounts
  livetime =  data_str.ltime

  spex_respinfo = data_str.respfile
  ;spex_respinfo = replicate(1, (size( spectrum,  /dim))[0] )
  spex_file_time = utime([data_str.start_time, data_str.end_time])
  spex_ut_edges = data_str.ut_edges
  spex_ct_edges = data_str.ct_edges
  spex_area = data_str.area[0]
  spex_title = data_str.title
  spex_detectors = data_str.detused
  spex_units = data_str.units
  spex_interval_filter = data_str.atten_states
  spex_data_name = 'XSM'
  spex_deconvolved = 0
  spex_pseudo_livetime = 0
  spex_data_pos = [0.,0.]

end

;------------------------------------------------------------------------------
pro spex_xsm_specfile__define

  self = {spex_xsm_specfile, $
          INHERITS spex_data_strategy }

END
