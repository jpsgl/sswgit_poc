;+
; PROJECT:
;   SPEX
; NAME:
;   XSM_PREP
;
; PURPOSE:
;   This procedures takes XSM data files and prepares the three files
;   needed for analysis, data - prefix.fits, drm - prefix.rmf and prefix.arf
;   Prefix is a string including the date and time of the observation start.
;
; CATEGORY:
;   SPEX
;
; CALLING SEQUENCE:
;
;
; CALLS:
;   none
;
; INPUTS:
;       datafilename - xsm data file (a fits file even if it has a .dat extension)
;
; OPTIONAL INPUTS:
;   none
;
; OUTPUTS:
;       none explicit, only through commons;
;
; OPTIONAL OUTPUTS:
;   none
;
; KEYWORDS:
;    fitsdatafile - name of fits data file to write, optional
;    rmf_file - name of rmf (matrix redistribution) file to write, optional, filename only
;    ancrfile - name of ancr (ancillary response) file to write, optional, filename only
;    outdir - destination directory
;    err_msg - text message on error return
;    err_code - error status, 0 no problems, 1 means problem see err_msg
; COMMON BLOCKS:
;   none
;
; SIDE EFFECTS:
;   none
;
; RESTRICTIONS:
;   Need write privileges in the current directory.
;
; PROCEDURE:
;   This procedure takes a fits file of an observation interval made with the XSM
;   detector on the SMART-1 spacecraft.  The data set contains energy calibration data,
;   the count data, and the effective area vs energy for each observation interval.
;   From this file three files will be written, xsm*.fits containing the counts data,
;   xsm*.rmf and xsm*.arf containing the equivalent of the detector response matrix.
;   These are the input files to be used with Object SPEX (ospex).  A second procedure,
;   xsm_filter_channels.pro can be used to select a subset of data in energy and time.
;
;
; MODIFICATION HISTORY:
;   22-Jun-2005, Version 1, richard.schwartz@gsfc.nasa.gov
;
;-


pro xsm_prep, datafilename, $
    outdir = outdir, $

    fitsdatafile=fitsdatafile, $
    rmf_file=rmf_file, $
    ancrfile=ancrfile, $
    dest_dir = dest_dir, $
    err_msg=err_msg, $
    err_code=err_code

err_code = 0
err_msg = ''
inputfile = loc_file( datafilename, count=count)
if count ne 1 then begin
    err_msg = datafilename + ' not found.  Exiting xsm_prep'
    err_code = 1
    message, /info, err_msg
    return
    endif

break_file, inputfile, disk, dir, prefix
path = disk + dir
mu_si_fits = 'mu_si_4xsm.fits'
mu_si_fits = loc_file(path=['$SSWDB_XRAY',path,curdir()],mu_si_fits, count=count)
if count lt 1 then begin
    err_msg = datafilename + ' not found.  Exiting xsm_prep'
    err_code = 1
    message, /info, err_msg
    return
    endif

prefix = 'xsm_'+prefix

;******INSERT THE INPUT DATA FILE NAME ON THE ROW BELOW************************

default, outdir, ''
fitsdatafile    = prefix + '.fits'
rmf_file        = prefix +'.rmf'
ancrfile        =  prefix +'.arf'
;default, channelrange, [ 31, 250] ;remove data energy channels outside this range ~ 1-10.5 keV
;******************************************************************************

XSM_SPEC_TABLE=mrdfits(INPUTFILE, 1)

;Write the rmf name into the dat file
;Let's call it a FITS file now
;
fitsdatafile = concat_dir( outdir, fitsdatafile )
file_copy, inputfile, fitsdatafile, /overwrite
;
fxhmodify, fitsdatafile, 'respfile', rmf_file,extension='XSM_DATA_TABLE'
fxhmodify, fitsdatafile, 'ancrfile', ancrfile,extension='XSM_DATA_TABLE'
fxhmodify, fitsdatafile, 'telescop', 'SMART-1',extension='XSM_DATA_TABLE'
;nominal geometric area is a circle of radius .075 cm
geoarea = .01767 ;cm^2
geoareastring = string(format='(f7.5)',geoarea)
fxhmodify, fitsdatafile, 'geoarea', geoareastring,extension='XSM_DATA_TABLE'
cal_data=xsm_spec_table.spectrum
flag=xsm_spec_table.flag


cal_data(0,*)=0
cal_data(511,*)=0

nt=n_elements(flag)
cal_t=dblarr(512,nt)
for i=0, nt-1 do begin
if flag(i) eq 1 then cal_t(*,i)=cal_data(*,i)
cal_t(0:85,i)=0.
endfor

ind=where(cal_t(140,*) gt 0)
nf=n_elements(ind)
calf=dblarr(512,nf)
for q=0, nf-1 do begin
calf(*,q)=cal_t(*,ind(q))
endfor

n_b=floor(0.5*nf)
n_e=ceil(0.5*nf)

c_beg=dblarr(512,n_b)
c_end=dblarr(512,n_e)

for hb=0, n_b-1 do begin
c_beg(*,hb)=calf(*,hb)
endfor

for he=n_b, nf-1 do begin
c_end(*,he-n_b)=calf(*,he)
endfor

tab=dblarr(6,n_b)

for z=0, n_b-1 do begin
    mn_chb=where(c_beg(*,z) eq max(c_beg(*,z)))
    mn_chb0=mn_chb(0)
    mn1=mn_chb0-19
    mn2=mn_chb0+19

    xb=findgen(512)
    xb1=xb(mn1:mn2)
    yb1=c_beg[mn1:mn2]
    ybf1 = gaussfit(xb1, yb1, ab)
    mn_centb=ab(1)
    mn_sigmab=ab(2)
    tab(1,z)=mn_centb

    dab=dblarr(512)
    dab=c_beg(*,z)
    dab(mn_chb0-22:*)=0
    ti_maxb=max(dab)

    ti_chb=where(dab eq max(dab))
    ti_chb0=ti_chb(0)
    tib1=ti_chb0-15
    tib2=ti_chb0+15

    xb=findgen(512)
    xb2=xb(tib1:tib2)
    yb2=dab[tib1:tib2]
    ybf2 = gaussfit(xb2, yb2, bb)
    ti_centb=bb(1)
    ti_sigmab=bb(2)
    tab(0,z)=ti_centb

    kb1=ti_centb
    kb2=mn_centb
    e1=4.5088
    e2=5.8951

    gain_ab=(e2-e1)/(kb2-kb1)
    tab(4,z)=gain_ab
    offset_ab=e2-(e2-e1)*kb2/(kb2-kb1)
    tab(5,z)=offset_ab

    e_res_mnb=2.35*mn_sigmab*gain_ab
    tab(3,z)=e_res_mnb
    e_res_tib=2.35*ti_sigmab*gain_ab
    tab(2,z)=e_res_tib

    endfor

kab=dblarr(6)
kab(0)=total(tab(0,0:n_b-1))/n_b
kab(1)=total(tab(1,0:n_b-1))/n_b
kab(2)=total(tab(2,0:n_b-1))/n_b
kab(3)=total(tab(3,0:n_b-1))/n_b
kab(4)=total(tab(4,0:n_b-1))/n_b
kab(5)=total(tab(5,0:n_b-1))/n_b

cb1=where(tab(3,*)-tab(2,*) lt 0)

for hb=0, n_elements(cb1)-1 do begin
    c_beg(*,cb1(hb))=0
    endfor

c_beg_tot=dblarr(512)

for hc=0, 511 do begin
    c_beg_tot(hc)=total(c_beg(hc,*))
    endfor

;plot, c_beg_tot

tabt=dblarr(6)

mn_chbt=where(c_beg_tot eq max(c_beg_tot))
mn_chbt0=mn_chbt(0)
mnbt1=mn_chbt0-19
mnbt2=mn_chbt0+19

xbt=findgen(512)
xbt1=xbt(mnbt1:mnbt2)
ybt1=c_beg_tot[mnbt1:mnbt2]
ybtf1 = gaussfit(xbt1, ybt1, abt)

mn_centbt=abt(1)
mn_sigmabt=abt(2)
tabt(1)=mn_centbt

dabt=dblarr(512)
dabt=c_beg_tot
dabt(mn_chbt0-22:*)=0
ti_maxbt=max(dabt)

ti_chbt=where(dabt eq max(dabt))
ti_chbt0=ti_chbt(0)
tibt1=ti_chbt0-15
tibt2=ti_chbt0+15

xbt=findgen(512)
xbt2=xbt(tibt1:tibt2)
ybt2=dabt[tibt1:tibt2]
ybtf2 = gaussfit(xbt2, ybt2, bbt)

ti_centbt=bbt(1)
ti_sigmabt=bbt(2)
tabt(0)=ti_centbt
kbt1=ti_centbt
kbt2=mn_centbt
e1=4.5088
e2=5.8951

gain_abt=(e2-e1)/(kbt2-kbt1)
tabt(4)=gain_abt
offset_abt=e2-(e2-e1)*kbt2/(kbt2-kbt1)
tabt(5)=offset_abt
e_res_mnbt=2.35*mn_sigmabt*gain_abt
tabt(3)=e_res_mnbt
e_res_tibt=2.35*ti_sigmabt*gain_abt
tabt(2)=e_res_tibt

print,'MEAN VALUES OF THE 1ST SET: Ti 4.5 keV Mn 5.9 keV, ENERGY=GAIN*CHANNEL#+OFFSET'
print, '       Ti Centroid    ', ' Mn Centroid    ', 'Ti Res      ', '    Mn Res      ', '   Gain      ', '       Offset'
print, TABT(*)
print,''



tae=dblarr(6,n_e)

for z=0, n_e-1 do begin
    mn_che=where(c_end(*,z) eq max(c_end(*,z)))
    mn_che0=mn_che(0)
    mne1=mn_che0-19
    mne2=mn_che0+19

    xe=findgen(512)
    xe1=xe(mne1:mne2)
    ye1=c_end[mne1:mne2]
    yef1 = gaussfit(xe1, ye1, ae)

    mn_cente=ae(1)
    mn_sigmae=ae(2)
    tae(1,z)=mn_cente

    dae=dblarr(512)
    dae=c_end(*,z)
    dae(mn_che0-22:*)=0
    ti_maxe=max(dae)

    ti_che=where(dae eq max(dae))
    ti_che0=ti_che(0)
    tie1=ti_che0-15
    tie2=ti_che0+15

    xe=findgen(512)
    xe2=xe(tie1:tie2)
    ye2=dae[tie1:tie2]
    yef2 = gaussfit(xe2, ye2, be)

    ti_cente=be(1)
    ti_sigmae=be(2)
    tae(0,z)=ti_cente
    ke1=ti_cente
    ke2=mn_cente
    e1=4.5088
    e2=5.8951

    gain_ae=(e2-e1)/(ke2-ke1)
    tae(4,z)=gain_ae
    offset_ae=e2-(e2-e1)*ke2/(ke2-ke1)
    tae(5,z)=offset_ae

    e_res_mne=2.35*mn_sigmae*gain_ae
    tae(3,z)=e_res_mne
    e_res_tie=2.35*ti_sigmae*gain_ae
    tae(2,z)=e_res_tie

    endfor

kae=dblarr(6)
kae(0)=total(tae(0,0:n_b-1))/n_e
kae(1)=total(tae(1,0:n_b-1))/n_e
kae(2)=total(tae(2,0:n_b-1))/n_e
kae(3)=total(tae(3,0:n_b-1))/n_e
kae(4)=total(tae(4,0:n_b-1))/n_e
kae(5)=total(tae(5,0:n_b-1))/n_e

ce1=where(tae(3,*)-tae(2,*) lt 0)

for he=0, n_elements(ce1)-1 do begin
    c_end(*,ce1(he))=0
    endfor

c_end_tot=dblarr(512)

for hd=0, 511 do begin
    c_end_tot(hd)=total(c_end(hd,*))
    endfor

;oplot, c_end_tot

taet=dblarr(6)

mn_chet=where(c_end_tot eq max(c_end_tot))
mn_chet0=mn_chet(0)
mnet1=mn_chet0-19
mnet2=mn_chet0+19

xet=findgen(512)
xet1=xet(mnet1:mnet2)
yet1=c_end_tot[mnet1:mnet2]
yetf1 = gaussfit(xet1, yet1, aet)

mn_centet=aet(1)
mn_sigmaet=aet(2)
taet(1)=mn_centet

daet=dblarr(512)
daet=c_end_tot
daet(mn_chet0-22:*)=0
ti_maxet=max(daet)

ti_chet=where(daet eq max(daet))
ti_chet0=ti_chet(0)
tiet1=ti_chet0-15
tiet2=ti_chet0+15

xet=findgen(512)
xet2=xet(tiet1:tiet2)
yet2=daet[tiet1:tiet2]
yetf2 = gaussfit(xet2, yet2, bet)

ti_centet=bet(1)
ti_sigmaet=bet(2)
taet(0)=ti_centet
ket1=ti_centet
ket2=mn_centet
e1=4.5088
e2=5.8951

gain_aet=(e2-e1)/(ket2-ket1)
taet(4)=gain_aet
offset_aet=e2-(e2-e1)*ket2/(ket2-ket1)
taet(5)=offset_aet
e_res_mnet=2.35*mn_sigmaet*gain_aet
taet(3)=e_res_mnet
e_res_tiet=2.35*ti_sigmaet*gain_aet
taet(2)=e_res_tiet

print,'mean values of the last set: Ti 4.5 keV Mn 5.9 keV, energy=gain*channel#+offset'
print, '       Ti centroid    ', ' Mn centroid    ', 'Ti res      ', '    Mn res      ', '   gain      ', '       offset'
print, taet(*)
print,''

tic=0.5*(kbt1+ket1)
mnc=0.5*(kbt2+ket2)
tir=0.5*(e_res_tibt+e_res_tiet)
mnr=0.5*(e_res_mnbt+e_res_mnet)
gain=0.5*(gain_abt+gain_aet)
offset=0.5*(offset_abt+offset_aet)


print,'*****adopted mean values for rmf*****'
print,'Ti-reso (ev):'
print,tir
print,'Mn-reso (ev):'
print,mnr
print,'gain (ev/ch):'
print,gain
print,'offset (ev):'
print,offset
print,''
;emin=fltarr(512)
;emax=fltarr(512)
;el=fltarr(512)
;eh=fltarr(512)

i1=1+findgen(512)
emin = gain*(i1-1)+offset > 0.0 < 20.
emax = gain*i1+offset > 0.0 < 20.


bad_ch=where(emin lt 0. or emax gt 20., nbad_ch)
if nbad_ch ge 1 then begin
    print,'Ignore channels below'
    print, bad_ch
    endif

;for m=0, 511 do begin

bad_ch = where( emax ge 20, nbad_ch)
if nbad_ch ge 1 then begin
    emin[bad_ch] = 20.
    emax[bad_ch] = 20.
    endif

bad_ch = where( emin le 0, nbad_ch)
if nbad_ch ge 1 then begin
    emin[bad_ch] = 0.
    emax[bad_ch] = 0.
    endif

el = emin
eh= emax

;if (emin(m) ge 0. and emin(m) le 20.) then el(m)=emin(m)
;if (emax(m) ge 0. and emax(m) le 20.) then eh(m)=emax(m)
;
;if emin(m) lt 0. then el(m)=0.
;if emax(m) lt 0. then eh(m)=0.
;endfor

x3=[0.0, 4.5088, 5.8951]
y3=[0.200^2, (tir)^2, (mnr)^2]
result=linfit(x3, y3, measure_errors=measure_errors)
f=dblarr(2)
f=result
v2=f(0)  ;reso offset
v1=f(1)  ;reso gain

ae=0.5*(el+eh)
ax_1=ae(where(ae ge 0. and ae le 20.))
nx_1=n_elements(ax_1)
ab=(findgen(nx_1)/(nx_1-1))*(max(ax_1)-min(ax_1))
ac=(findgen(512)/511.)*(max(ax_1)-min(ax_1))
a=interpol(ax_1, ab, ac )

e_min=emin
e_max=emax
e_low=findgen(512)*39.0625/1000.
e_high=findgen(512)*39.0625/1000.+39.0625/1000.

fwhm=sqrt(v1*a+v2)
;plot, a, xr=[0,max(e_max)], fwhm, ytitle='fwhm', xtitle='photon energy / kev', title='xsm energy resolution'
;wait, 0.1


mu_si=readfits(mu_si_fits)
f=fltarr(512)
gm=fltarr(512)
gp=fltarr(512)

eb=findgen(512)/511.*20.103
ee=findgen(512)/511.*20.0
mu=interpol(mu_si, eb, ee)

v_2=v2*0.18033688
v_1=v1*0.18033688

hyp=fltarr(512,512)

for i2=0, 511 do begin
    e=max(a)*i2/511
    s=sqrt(v_1*e+v_2)
    f=mu(i2)

    gm=1.0*exp(-(a-e)^2/(2.*s*s))
    gm=gm>0.000000000001
    if e lt 1.747 then k=0
    if e gt 1.747 then k=0.023*(1-(851./f)*alog(1.+(f/851.)))*total(gm)
    gp=k*exp(-(a-e+1.747)^2/(2.*s*s))
    gp=gp>0.000000000001
    d_tot=fltarr(512)
    d_tot=(gm+gp)/(total(gm)+total(gp))
    hyp(*,i2)=d_tot
    endfor



hypermet=hyp

n_grp=indgen(512)
n_grp(*)=1
f_chan=indgen(512)
f_chan(*)=1
n_chan=indgen(512)
n_chan(*)=512

pdr_0=strarr(5)
pdr_0(0)='SIMPLE  =                    T / file does conform to FITS standard'
pdr_0(1)='BITPIX  =                    8 / number of bits per data pixel'
pdr_0(2)='NAXIS   =                    0 / number of data axes'
pdr_0(3)='EXTEND  =                    T / FITS dataset may contain extensions'
pdr_0(4)='END'

pdr_1=strarr(35)
pdr_1(0)='XTENSION= ''BINTABLE''           / binary table extension'
pdr_1(1)='BITPIX  =                    8 / 8-bit bytes'
pdr_1(2)='NAXIS   =                    2 / 2-dimensional binary table'
pdr_1(3)='NAXIS1  =                 2062 / width of table in bytes'
pdr_1(4)='NAXIS2  =                  512 / number of rows in table'
pdr_1(5)='PCOUNT  =                    0 / size of special data area'
pdr_1(6)='GCOUNT  =                    1 / one data group (required keyword)'
pdr_1(7)='TFIELDS =                    6 / number of fields in each row'
pdr_1(8)='TTYPE1  = ''ENERG_LO''           / label for field'
pdr_1(9)='TFORM1  = ''1E      ''           / format of field: 4 b'
pdr_1(10)='TUNIT1  = ''keV    ''            / unit of field'
pdr_1(11)='TTYPE2  = ''ENERG_HI''           / label for field'
pdr_1(12)='TFORM2  = ''1E     ''            / format of field: 4 b'
pdr_1(13)='TUNIT2  = ''keV    ''            / unit of field'
pdr_1(14)='TTYPE3  = ''N_GRP  ''            / label for field'
pdr_1(15)='TFORM3  = ''1I     ''            / format of field: 2 b'
pdr_1(16)='TTYPE4  = ''F_CHAN ''            / label for field'
pdr_1(17)='TFORM4  = ''1I     ''            / format of field: 2 b'
pdr_1(18)='TTYPE5  = ''N_CHAN ''            / label for field'
pdr_1(19)='TFORM5  = ''1I     ''            / format of field: 2 b'
pdr_1(20)='TTYPE6  = ''MATRIX ''            / label for field'
pdr_1(21)='TFORM6  = ''512E   ''            / format of field: 2048 b'
pdr_1(22)='EXTNAME = ''MATRIX ''            / Extension name'
pdr_1(23)='INSTRUME= ''XSM    ''            / instrumennt name
pdr_1(24)='HDUCLASS= ''OGIP   ''            / format conforms to OGIP/GSFC conventions'
pdr_1(25)='HDUCLAS1= ''RESPONSE''           / Dataset contains a response mateix'
pdr_1(26)='HDUCLAS2= ''RSP_MATRIX''         / Extension contains response matrix'
pdr_1(27)='HDUVERS = ''1.3.0  ''            / version number of the format'
pdr_1(28)='CHANTYPE= ''PHA    ''            / Type of detector channels'
pdr_1(29)='DETCHANS=                  512 / Total number of PI channels in the full matrix'
pdr_1(30)='TLMIN   =                    1 / Channel number of first channel'
pdr_1(31)='LO_THRES=                   0. / Lowest value in MATRIX
pdr_1(32)='TELESCOP= ''SMART-1''            / telescope (mission) name'
pdr_1(33)='FILTER  = ''none   ''            / filter not specified'

pdr_1(34)='END'

pdr_2=strarr(28)
pdr_2(0)='XTENSION= ''BINTABLE''           / binary table extension'
pdr_2(1)='BITPIX  =                    8 / 8-bit bytes'
pdr_2(2)='NAXIS   =                    2 / 2-dimensional binary table'
pdr_2(3)='NAXIS1  =                   10 / width of table in bytes'
pdr_2(4)='NAXIS2  =                  512 / number of rows in table'
pdr_2(5)='PCOUNT  =                    0 / size of special data area'
pdr_2(6)='GCOUNT  =                    1 / one data group (required keywor'
pdr_2(7)='TFIELDS =                    3 / number of fields in each row'
pdr_2(8)='TTYPE1  = ''CHANNEL ''           / label for field'
pdr_2(9)='TFORM1  = ''1I      ''           / format of field'
pdr_2(10)='TTYPE2  = ''E_MIN  ''            / label for field'
pdr_2(11)='TFORM2  = ''1E     ''            / format of field'
pdr_2(12)='TTYPE3  = ''E_MAX  ''            / label for field'
pdr_2(13)='TFORM3  = ''1E     ''            / format of field'
pdr_2(14)='EXTNAME = ''EBOUNDS''            / Extension name'
pdr_2(15)='TELESCOP= ''SMART-1''            / Telescope or mission name'
pdr_2(16)='ORIGIN  = ''XSM-team''           / Origin of FITS file'
pdr_2(17)='INSTRUME= ''XSM    ''            / Instrument name'
pdr_2(18)='HDUCLASS= ''OGIP   ''            / Format conforms mostly to OGIP standards'
pdr_2(19)='HDUVERS = ''1.2.0  ''            / Version of format'
pdr_2(20)='HDUCLAS1= ''RESPONSE''           / Dataset contains response information'
pdr_2(21)='HDUCLAS2= ''EBOUNDS''            / Extension contains energy'
pdr_2(22)='CHANTYPE= ''PHA    ''            / Type of detector channels'
pdr_2(23)='DETCHANS=                  512 / Total number of PI channels in the full matrix'
pdr_2(24)='TUNIT2  = ''keV    ''            / Unit of this field'
pdr_2(25)='TUNIT3  = ''keV    ''            / Unit of this field'
pdr_2(26)='FILTER  = ''none   ''            / filter not specified'
pdr_2(27)='END'

rmf_file_path = concat_dir( outdir, rmf_file)
fxwrite, rmf_file_path, pdr_0
fxbcreate, unit_1, rmf_file_path, pdr_1, errmsg=errmsg
fxbcreate, unit_2, rmf_file_path, pdr_2, errmsg=errmsg

for i3=1, 512 do begin
    fxbwrite, unit_1, float(e_low(i3-1)), 1, i3
    fxbwrite, unit_1, float(e_high(i3-1)), 2, i3
    fxbwrite, unit_1, n_grp(i3-1), 3, i3
    fxbwrite, unit_1, f_chan(i3-1), 4,i3
    fxbwrite, unit_1, n_chan(i3-1), 5,i3
    fxbwrite, unit_1, float(hypermet(*,i3-1)), 6, i3
    endfor

cha=indgen(512)

for j3=1, 512 do begin
    fxbwrite, unit_2, cha(j3-1), 1, j3
    fxbwrite, unit_2, float(e_min(j3-1)), 2, j3
    fxbwrite, unit_2, float(e_max(j3-1)), 3, j3
    endfor

fxbfinish, unit_1
fxbfinish, unit_2



spec=xsm_spec_table.a_eff
valid = where( avg(spec[100:300,*],0) gt .01, nvalid)
;n1=n_elements(spec(0,*))
a_eff=avg(float(spec[*,valid]),1) ;This effectively contains geoarea factor but we leave it in
;for compatability with xspec



pda_0=strarr(5)
pda_0(0)='SIMPLE  =                    T / file does conform to FITS standard'
pda_0(1)='BITPIX  =                    8 / number of bits per data pixel'
pda_0(2)='NAXIS   =                    0 / number of data axes'
pda_0(3)='EXTEND  =                    T / FITS dataset may contain extensions'
pda_0(4)='END'

;OUTBEG = 'XSM_'
;OUTEND = '.ARF'
;RMF_BEG = 'XSM_'
;RMF_END = '.RMF'
;DATA_END = '.DAT'

;FOR K=0,0 DO BEGIN
k=0
;
;Write Ancrfile

ancrfile_path = concat_dir( outdir, ancrfile)
fxwrite, ancrfile_path, pda_0


;arffile=rmf_beg+strtrim(string(k),2)+outend
;datafile=outbeg+strtrim(string(k),2)+data_end

pda_1=strarr(33)
pda_1(0)='XTENSION= ''BINTABLE''           / binary table extension'
pda_1(1)='BITPIX  =                    8 / 8-bit bytes'
pda_1(2)='NAXIS   =                    2 / 2-dimensional binary table'
pda_1(3)='NAXIS1  =                   12 / width of table in bytes'
pda_1(4)='NAXIS2  =                  512 / number of rows in table'
pda_1(5)='PCOUNT  =                    0 / size of special data area'
pda_1(6)='GCOUNT  =                    1 / one data group (required keyword)'
pda_1(7)='TFIELDS =                    3 / number of fields in each row'
pda_1(8)='TTYPE1  = ''E_LOW   ''           / label for field'
pda_1(9)='TFORM1  = ''E       ''           / format of field: 4 b'
pda_1(10)='TUNIT1  = ''ENERGY ''            / unit of field'
pda_1(11)='TTYPE2  = ''E_HIGH ''            / label for field'
pda_1(12)='TFORM2  = ''E      ''            / format of field: 4 b'
pda_1(13)='TUNIT2  = ''ENERGY ''            / unit of field'
pda_1(14)='TTYPE3  = ''SPECRESP''           / label for field'
pda_1(15)='TFORM3  = ''E      ''            / format of field: 4 b'
pda_1(16)='TUNIT3  = ''cm^2   ''            / unit of field'
pda_1(17)='TELESCOP= ''SMART-1''            / telescope (mission) name'
pda_1(18)='INSTRUME= ''XSM    ''            / instrument name'
pda_1(19)='FILTER =  ''NONE   ''            / filter information'
pda_1(20)='CCLS0001= ''CPF    ''            / OGIP class of calibration file '
pda_1(21)='CDTP0001= ''DATA   ''            / OGIP code for contents'
pda_1(22)='HDUCLASS= ''OGIP   ''            / format conforms to OGIP/GSFC conventions'
pda_1(23)='HDUCOC  = ''CAL/GEN/92-019''     / document describing the format'
pda_1(24)='HDUCLAS1= ''RESPONSE''           / response of instrument'
pda_1(25)='HDUCLAS2= ''SPECRESP''           / dataset contains spectral response'
pda_1(26)='HDUVERS1= ''1.0.0  ''            / version of famoly format'
pda_1(27)='HDUVERS2= ''1.1.0  ''            / version number of the format'

;***************GIVE THE FILE NAME FOR THE RMF**************************************************

pda_1(28)='RESPFILE= ''' + rmf_file + '''       / Redistribution matrix file (RMF)'

;***********************************************************************************************

pda_1(29)='ANCRFILE= ''' + ancrfile + '''          / Anc. Resp. file (ARF)'
pda_1(30)='EXTNAME= ''SPECRESP''            / extension name'
pda_1(31)='CREATOR = ''L. Alha''            / person that created this file'
pda_1(32)='END'

FXBCREATE, UNIT_3, ancrfile_path, pda_1, errmsg=errmsg

FOR I=1, 512 DO BEGIN
FXBWRITE, UNIT_3, FLOAT(E_LOW(I-1)), 1, I
FXBWRITE, UNIT_3, FLOAT(E_HIGH(I-1)), 2, I
FXBWRITE, UNIT_3, FLOAT(A_EFF(I-1,K)), 3, I
ENDFOR
;print, K
FXBFINISH, UNIT_3
fxhmodify, ancrfile_path, 'geoarea', $
     'Folded into ARF -arf is efficiency*geoarea', geoareastring,extension=1
fxhmodify, rmf_file_path, 'ancrfile', ancrfile,extension='MATRIX'
;xsm_filter_channels, channelrange, fitsdatafile
END