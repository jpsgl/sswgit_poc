;+
; Modifications:
; 16-Sep-2004, Kim.  Added spex_autoplot_show_err
; 17-Sep-2004, Kim.  Added bk_rate, bk_error
; 7-Apr-2006,  Kim.  Added func_spectrum func_model, chianti_version, spex_summ_abun_table
; 12-Feb-2008, Kim.  Added spex_fitcomp_plot_bk param for plotting bk overlay in fitcomp widget
; 17-Mar-2008, Kim.  Added spex_allow_diff_energy
; 11-Oct-2012, Kim. Added spex_fitcomp_plot_err
;-

pro spex_fit_info__define

struct = {spex_fit_info, $

	;spex_fit_origunits: {spex_units}, $	; units of original fit data
	;spex_fit_units: {spex_units}, $	; units of fit data
	spex_autoplot_enable: 0, $	    ; If set, automatically plot after fitting
	spex_autoplot_bksub: 0, $	    ; If set, plot data-bk, not data with bk in autoplot
	spex_autoplot_overlay_back: 0, $	; If set, overlay bk in autoplot
	spex_autoplot_overlay_bksub: 0, $	; If set, overlay data-bk in autoplot
	spex_autoplot_show_err: 0, $		; If set, show error bars
	spex_autoplot_photons: 0, $	    ; If set, plot in photon space in autoplot
	spex_autoplot_units: '', $	    ; Units for autoplot ('counts', 'rate', 'flux')

	spex_fitcomp_autoplot_enable: 0, $	; If set, autoplot in FITCOMP widget after any change
	spex_fitcomp_plot_units: '', $	; Units for plot in FITCOMP widget
	spex_fitcomp_plot_photons: 0, $	; If set, plot in photon space in FITCOMP widget
	spex_fitcomp_plot_bk: 0, $	    ; If set, overlay bk on plot in FITCOMP widget
	spex_fitcomp_plot_err: 0, $     ; if set, show errors on plot in FITCOMP widget
	spex_fitcomp_plot_resid: 0, $	; If set, plot residuals in autoplot in FITCOMP widget
	spex_fitcomp_plotobj_resid: obj_new(), $ ; Plotman object to use for residuals

	spex_fit_progbar: 0, $	; If set, show progress bar during fit loop through intervals

	spex_allow_diff_energy: 0, $	If set, allow spex_summ energies vals to differ from ct_edges

	spex_summ_fit_function: ptr_new(), $	; Fit function used
	spex_summ_area: 0., $	; Detector area for data in fit results
	spex_summ_energy: ptr_new(), $	; Full array of energy edges, (2,nenergy)
	spex_summ_time_interval: ptr_new(), $	; Array of time intervals fit, (2,ntime)
	spex_summ_filter: ptr_new(), $	; Array of filter states for each time interval (ntime)
	spex_summ_fit_done: ptr_new(), $	; Interval fit completed mask.  For each interval 0=not fit, 1=fit. (ntime)
	spex_summ_emask: ptr_new(), $	; Mask of energies used for each interval (nenergy,ntime)
	spex_summ_ct_rate: ptr_new(), $	; Count rate data that was fit for each interval, (nenergy,ntime)
	spex_summ_ct_error: ptr_new(), $	; Error in count rate data for each interval, (nenergy,ntime)
	spex_summ_bk_rate: ptr_new(), $	; Background rate corresponding to ct_rate, (nenergy,ntime)
	spex_summ_bk_error: ptr_new(), $	; Error in background rate data for each interval, (nenergy,ntime)
	spex_summ_ph_model: ptr_new(), $	; Photon flux model for each interval, (nenergy,ntime)
	spex_summ_conv: ptr_new(), $	; Conversion factors for each interval (nenergy,ntime)
	spex_summ_resid: ptr_new(), $	; Residuals for each interval, (nenergy,ntime)
	spex_summ_starting_params: ptr_new(), $	; Starting fit parameters for each interval, (nparams,ntime)
	spex_summ_params: ptr_new(), $	; Final fit parameters for each interval, (nparams,ntime)
	spex_summ_sigmas: ptr_new(), $	; Sigma in fit parameters for each interval, (nparams,ntime)
	spex_summ_minima: ptr_new(), $	; Parameter minima for each interval, (nparams,ntime)
	spex_summ_maxima: ptr_new(), $	; Parameter maxima for each interval, (nparams,ntime)
	spex_summ_free_mask: ptr_new(), $	; Parameter free mask for each interval, (nparams,ntime)
	spex_summ_func_spectrum: ptr_new(), $	; Fit function spectrum option (full, cont, or line)
	spex_summ_func_model: ptr_new(), $		; Fit function model option (chianti or mewe)
	spex_summ_chianti_version: '', $       ; Chianti package version used to make tables
	spex_summ_abun_table: '', $				; Abundance table name
	spex_summ_maxiter: ptr_new(), $	; Maximum number of iterations allowed for each interval, (ntime)
	spex_summ_uncert: ptr_new(), $	; Systematic uncertainty for each interval, (ntime)
	spex_summ_chisq: ptr_new(), $	; Chi-square for each interval, (ntime)
	spex_summ_stop_msg: ptr_new(), $	; Reason for stopping fit for each interval, (ntime)
	spex_summ_niter: ptr_new() $	; Number of fit iterations done for each interval, (ntime)

	}
end