;+
; Name: spex_map__define
;
; Purpose: A plot object that inherits the fits and map plot object.  This wrapper
;  object allows us to add features like grid lines and a legend that aren't available
;  otherwise.  Used in ospex to plot summary parameters as a map.  Assumes the values
;  1,2,3 are in the map, and that the scaling should be from 0-6 (so that 1,2,3 will
;  be scaled somewhere between the min and max).  
;  For example, in the plot of free/fixed parameters, the x axis is the interval #, the y axis
;  is the parameter #, and the map has value 1 (interval not fit yet), 2 (parameter is fixed) and
;  3 (parameter is free).
;
; Written: Kim Tolbert 10-Dec-2008
; Modifications:
; 12-Dec-2008, Kim.  Added label stuff
; 8-Oct-2011, Kim. Call al_legend instead of legend (IDL 8. name conflict)
; 10-Nov-2011, Kim. Set psym to 0 before al_legend, otherwise get psym errors. Also, don't plot xgrid at
;   every x value if number of intervals plotted is large - set a stride depending on !x.crange (previously
;   with many intervals, the xgrid lines blocked the colors, so entire plot was blue). In plot, remove
;   limb_plot from _extra.
; 19-Aug-2012, Kim. Too many problems with al_legend - use ssw_legend
;
;-


function spex_map::init, _extra=_extra
self.legend = ptr_new('')
self.label_arr = ptr_new('')
self.xlabpos = ptr_new(-1)
self.ylabpos = ptr_new(-1)
return,  self->fits::init(_extra=_extra)
end

;-----------------------------------------------------------------------------------------------------

pro spex_map::set, k, xgrid=xgrid, ygrid=ygrid, legend=legend, $
   label_arr=label_arr, xlabpos=xlabpos, ylabpos=ylabpos, _extra=_extra
if ~is_number(k) then k=0
if exist(xgrid) then self.xgrid = xgrid
if exist(ygrid) then self.ygrid = ygrid
if exist(legend) then *self.legend = legend
if exist(label_arr) then *self.label_arr = label_arr
if exist(xlabpos) then *self.xlabpos = xlabpos
if exist(ylabpos) then *self.ylabpos = ylabpos
self -> map::set,k,_extra=_extra
end

;-----------------------------------------------------------------------------------------------------

pro spex_map::plot, k, $
   square_scale=square_scale, $
   legend_loc=legend_loc, legend_color=legend_color, charsize=charsize, drange=drange, $
   _extra=_extra
   
checkvar, square_scale, 0
_extra = rem_tag(_extra, 'limb_plot')
self -> map::plot,k,square_scale=square_scale,_extra=_extra,drange=[0,6]

if self.xgrid or self.ygrid then begin
  nx = self->get(/nx)
  ny = self->get(/ny)
  dx = self->get(/dx)
  dy = self->get(/dy)
  xp = self->xp(/oned) - dx/2.
  yp = self->yp(/oned) - dy/2.
  xp = [xp, xp[nx-1]+dx]
  yp = [yp, yp[ny-1]+dy]
  xr = minmax(xp)
  yr = minmax(yp)

  xmm = !x.crange[1] - !x.crange[0]
  ind = value_locate([0.,500., 1000., 2000., 3000.], xmm) > 0
  stride = ([1, 2, 5, 10, 20])[ind]
  
  if self.xgrid then for i=0,nx,stride do oplot,[xp[i],xp[i]],yr,color=20

  if self.ygrid then for i=0,ny do oplot,xr,[yp[i],yp[i]],color=20
endif

if is_string(*self.legend) then begin
  checkvar, legend_loc, 1
  if legend_loc ne 0 then begin
     top = legend_loc lt 3
     bottom = legend_loc ge 3
     right = (legend_loc mod 2) eq 0
     left = legend_loc mod 2
     checkvar, legend_color, 0
     save_psym=!p.psym
     !p.psym = 0
     ssw_legend, *self.legend, box=0, $
       top_legend=top, bottom_legend=bottom, right_legend=right, left_legend=left,$
       textcolor=legend_color, charsize=charsize, color=cscale([1,2,3,6,0]), thick=4., linestyle=0,/clear
     !p.psym = save_psym
  endif
endif

if is_string(*self.label_arr) then begin
  nlabel  = n_elements(*self.label_arr)
  for i=0,nlabel-1 do begin
     xr = !x.crange & yr = !y.crange
     x = (*self.xlabpos)[0] eq -1 ? xr[0]+.01*(xr[1]-xr[0]) : (*self.xlabpos)[i]
     y = (*self.ylabpos)[0] eq -1 ? yr[0]+.05*(yr[1]-yr[0]) : (*self.ylabpos)[i] 
     xyouts, x, y, (*self.label_arr)[i]
  endfor
endif

end

;-----------------------------------------------------------------------------------------------------

pro spex_map__define,dummy

dummy = {spex_map, xgrid:0, ygrid:0, legend:ptr_new(), $
   label_arr: ptr_new(), xlabpos: ptr_new(), ylabpos: ptr_new(), $
   inherits fits}

end