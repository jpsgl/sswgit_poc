;+
; Name: SPEX::XFIT
;
; Purpose: OSPEX method to provide widget to handle fit intervals and fit options.
;
; Category: OSPEX
;
; Calling sequence:  o -> xfit  (o is an ospex object)
;
; Modifications:
; 22-Jun-2004, Kim.  Replaced spex_interval_range with spex_intervals_tofit
; 20-Jul-2004, Kim.  Added 'Remove Bad Intervals' and 'Show Filter States' buttons, and
;	changed show erange and show fit intervals so they show on current plot if the
;   right type, rather than drawing new plot every time.
; 12-Aug-2004, Kim.  Made the Remove Bad Intervals and Show Fit Intervals menu buttons with
;   two options each.  For remove, choose whether to remove from defined or selected list.
;   For show, choose whether to show all or just selected intervals.  Also, when drawing intervals
;   if a utplot is already up, refresh it just to clear what was already on it.
; 16-Sep-2004, Kim.  Added 'Show Error' button for plots.
; 5-Oct-2004, Kim.  Made an Adjust Intervals button and moved remove bad options to menu items
;	under that button, and added another item - force to data boundaries.
; 11-Apr-2005, Kim.  Got rid of Show Filter States button (now all time plots show filters)
; 18-Aug-2005, Kim.  'All raw time intervals' now only uses raw time intervals with non-zero
;	data in at least some energy bin.  Also, use spex_deconvolved flag to decide whether
;	Photon button should be sensitive.  Use new units_widget gen method to handle units.
;	Also, added class_name where possible to speed up.
; 10-Feb-2006, Kim.  Add 0. to dropdown list for systematic uncertainty
; 15-Mar-2006, Kim. added gui_label to put on title bar of spex widget interfaces
; 7-Apr-2006, Kim.  Added Show Function button and changed some wording on widget for clarity
; 2-Jul-2006, Kim.  Added Set to 'spex_tband' button. And corrected spex__erange plot to plot intervals
;  selected (before plotting minmax(sel_intervals))
; 31-Oct-2006, Kim. If all fitint_filters aren't the same, show the filter for each interval in
;  the list of times in parentheses.  Also change plot to show intervals to use plot_time method,
;  and use /show_filter option.
; 01-Nov-2006, Kim.  Added options to select intervals based on filter and plot limits.
; 14-Nov-2006, Kim. In xfit_update_times, do getdata on fitint class to make sure we have
;  the current filters.  (Unfortunately needed, but hopefully not too time-consuming). Also
;  save filters in self, and compare to new - if same and times are same don't redo time int list.
; 27-Mar-2008, Kim. Added plot_resid button
; 03-Apr-2008, Kim. Added current + prev int method for start parameters.
; 04-Apr-2008, Kim. Added options for plotting residuals (main window, resid window, sep panels)
; 23-Apr-2008, Kim. Added separate options for specifying first interval and subsequent interval
;  parameter initialization.
; 10-Jul-2008, Kim. Change hsi_ui_getfont to get_font (which is in ssw gen)
; 08-Dec-2008, Kim. Removed button for selecting standard or current plot for defining energy ranges (now
;  just uses current unless it's not an xyplot, then draws standard), and added button for spex_fit_auto_erange
; 08-Feb-2009, Kim.  Added option under 'Show Fit Intervals' to show just the fitted intervals
; 13-Jul-2009, Kim.  Added button for spex_fit_init_params_only.  Also tooltip for that, and auto-set max.
; 14-Aug-2009, Kim.  Added cleanup routine
; 22-Jan-2011, Kim.  Added /scroll to wbase widget, made new wbox base inside wbase and put all 
;   widgets inside that, then call widget_limit_ysize after all widgets are set up to control y size.
; 14-Nov-2011, Kim. Added option to remove highlighted intervals, and to select fitted intervals. When there
;   are multiple filters, the Select->Intervals with filter N option didn't include -99.  Now it does.
; 24-Sep-2012, Kim. Added 0 to values in # Iter pulldown.
; 03-Oct-2012, Kim. Added 'edit stored results' button
; 11-Oct-2012, Kim. Added option under Plot Resid pulldown to plot raw residuals
; 14-Nov-2012, Kim. Added option to plot spectrum and resid automatically in stacked plot with 70%, 30%
; 30-Oct-2013, Kim. Added auto-set for min of energy range (in addition to max)
; 26-Jun-2014, Kim. Added option to set fit time intervals to stored intervals (spex_summ_time_interval) and
;  made the 'Set intervals to ' a dropdown menu
;
;-
;-------------------------------------------------------------------------------------

pro spex_xfit_cleanup, id
widget_control, id, get_uvalue=state, /no_copy
if n_elements(state) eq 0 then return
ptr_free, state.times
ptr_free, state.filters
end

;-----

pro spex::xfit_update_widget, wbase

widget_control, wbase, get_uvalue=state

self -> xfit_update_times, state

erange = self -> get(/spex_erange)
nbands = erange[0] eq -1 ? 0 : n_elements(erange[0,*])
widget_control, state.w_nbands, set_value=strtrim(nbands, 2) + ' '
s_erange = erange[0] eq -1 ? strpad('All',20) : format_intervals(erange, format='(f9.1)')
widget_control, state.w_erange, set_value='  ' + s_erange + '  '
widget_control, state.w_auto_erange, set_button=self->get(/spex_fit_auto_erange)
widget_control, state.w_auto_emin, set_button=self->get(/spex_fit_auto_emin)

func = self -> get(/fit_function, class='fit_function')
params = self -> getfit(/fit_comp_params, class='fit_comp_manager')
value = 'Current function: ' + func + ':  ' + arr2str(trim(params, '(g12.4)'), ', ')
value = strlen(value) gt 95 ? strmid(value,0,95) + '...' : value
widget_control, state.w_current, set_value=value

widget_control, state.w_param_only, set_button=self->get(/spex_fit_init_params_only)

firstint =  (self->get(/obj,class='spex_fit')) -> parse_firstint_method()
q = where (firstint.method eq state.firstint_methods)
widget_control, state.w_first, set_droplist_select=q[0]
if firstint.interval ne -1 then $
    widget_control, state.w_interval_init, set_value=trim(firstint.interval)

start_method = self->get(/spex_fit_start_method)
q = where (start_method eq state.start_methods)
widget_control, state.w_subs, set_droplist_select=q[0]
widget_control, state.w_first, sensitive = start_method ne 'previous_iter'
widget_control, state.w_interval_init, sensitive = (start_method ne 'previous_iter') and (firstint.interval ne -1)

widget_control, state.w_maxiter, set_value=self->getalg(/maxiter)

widget_control, state.w_uncert, set_value=self->get(/spex_uncert)

widget_control, state.w_mode, set_droplist_select=self->get(/spex_fit_manual)

widget_control, state.w_reverse, set_droplist_select=self->get(/spex_fit_reverse)

widget_control, state.w_autoplot, set_button=self->get(/spex_autoplot_enable)
widget_control, state.w_autoresid, set_button=self->get(/spex_fitcomp_plot_resid)
widget_control, state.w_progbar, set_button=self->get(/spex_fit_progbar)

widget_control, state.w_phot, sensitive=(self->get(/spex_deconvolved) ne 1)
widget_control, state.w_show_comb, sensitive=widget_info(state.w_show_fit, /button_set)

end
;-----

pro spex::xfit_update_times, state

times = self -> get(/spex_fit_time_interval)
data = self->getdata(class='spex_fitint') ; make sure filter info is up to date
filt = self -> get(/spex_fitint_filter)

; if times and filters haven't changed, don't need to remake list
if same_data(times, *state.times) and same_data(filt, *state.filters) then return

mm_filt = minmax(filt)

if times[0] eq -1 then begin
	ntimes = 0
	s_times = 'None'
	sel = 0
endif else begin
	ntimes = n_elements(times[0,*])
	s_times = trim(indgen(ntimes)) + '.  ' + format_intervals(times, /ut)
	if mm_filt[0] ne mm_filt[1] then s_times = s_times + '  (' + trim(filt) + ')'
	sel = indgen(ntimes)
endelse

widget_control, state.w_times, set_value=s_times, set_list_select=sel
widget_control, state.w_ntimes, $
	set_value='Total # Fit Intervals:' + strtrim(ntimes, 2) + '.    Highlight intervals to plot or fit:'

*state.times = times
*state.filters = filt
widget_control, state.wbase, set_uvalue=state

end

;-----
; Event handlers can't be object methods, so this is just a wrapper to get to the method

pro xfit_event,event
widget_control,event.top,get_uvalue=state
if obj_valid(state.object) then state.object->xfit_event,event
return & end

;-----
; xfit event handler

pro spex::xfit_event, event

widget_control, event.top, get_uvalue=state

widget_control, event.id, get_uvalue=uvalue

if not exist(uvalue) then return

sel_intervals = widget_info (state.w_times, /list_select)
plotman_obj = self -> get_plotman_obj()

self -> units_widget, state.w_units, units=units, /getval

; Select options are complicated.  They include:
; sel_all  - select all
; sel_all_plot  - select all within plot limits
; sel_fitted  - select all fitted intervals
; sel_all_filtn  - select all with filter=n
; sel_highl_filtn - select all highlighted with filter=n
; sel_plot_fitn - select all within plot limits with filter=n
; 
; Break up into three variables:
; uvalue = 'sel'
; sel_filt = -1 (if don't care) or filter number
; sel_mode = 'all', 'highl', 'plot', or 'fitted'

if strmid(uvalue, 0, 3) eq 'sel' then begin
	if strpos(uvalue,'filt') eq -1 then $
		sel_filt=-1 $
	else $
		sel_filt = fix ( (ssw_strsplit(uvalue, 'filt', /tail))[0] )
	sel_mode = 'all'
	if strpos(uvalue, 'fitted') ne -1 then sel_mode='fitted'
	if strpos(uvalue, 'highl') ne -1 then sel_mode='highl'
	if strpos(uvalue, 'plot') ne -1 then sel_mode='plot'
	uvalue='sel'
endif

bksub = widget_info(state.w_plotbksub, /button_set)
overlay_back = widget_info (state.w_overlay_back, /button_set)
show_err = widget_info (state.w_show_err, /button_set)
photons = widget_info (state.w_phot, /button_set)
show_fit = widget_info (state.w_show_fit, /button_set)
show_comb = widget_info (state.w_show_comb, /button_set)
    
; NOTE: in cases where we're changing fit_time_intervals, the fitint_filters will be updated to 
; correspond to new times in spex_fitint::process when  getdata is called in xfit_update_times method.

case uvalue of

	'sel': begin
	    new_sel = indgen(widget_info(state.w_times, /list_number))
		if sel_filt ne -1 then begin
			filt = self->get(/spex_fitint_filter)
			new_sel = where (filt eq sel_filt)
		endif else if sel_mode eq 'fitted' then begin
            fit_done = where (self -> get(/spex_summ_fit_done), ndone)
            if ndone gt 0 then new_sel = fit_done else begin
              new_sel = -1
              message,'No intervals fit yet.', /cont
            endelse
        endif 
        if new_sel[0] ne -1 then begin
    		if sel_mode eq 'highl' then begin
    			q = where_arr(sel_intervals, new_sel, count)
    			new_sel = count gt 0 ? sel_intervals[q]: -1
    		endif
    		if sel_mode eq 'plot' then begin
    			if self->valid_plot(/utplot) then begin
    				plotman_obj -> select   ; select so !x.crange will be correct
    				plot_times = plotman_obj->get(/utbase) + !x.crange
    				plotman_obj -> unselect
    				fit_times = self->get(/spex_fit_time_interval)
    				q = where_within(fit_times[*,new_sel], plot_times, count)
    				new_sel = count eq 0 ? -1 : new_sel[q]
    			endif else begin
    				message,'No time plot displayed.  NO CHANGE.', /cont
    				new_sel=-1
    			endelse
    		endif
        endif
		
		if new_sel[0] eq -1 then begin
			message, 'No intervals meeting selection requirements. NO CHANGE.', /cont
		endif else begin
			widget_control, state.w_times, set_list_select=new_sel
		endelse
		end

	'all_raw_times': begin
		data = self -> getdata(class='spex_data')
		if is_struct(data) then begin
			q = where (total(data.data, 1) ne 0., count)
			times = self->getaxis(/ut, /edges_2)
			times = count gt 0 ? times[*, q] : -1
		endif else times = -1
		self -> set, spex_fit_time_interval = times
		end

  'summ_times': begin
    summ_times = self->get(/spex_summ_time_interval)
    if ~ptr_chk(summ_times) && (summ_times[0] ne -1) then self->set, spex_fit_time_interval=summ_times else $
      message,/cont,'There are no stored interval times (spex_summ_time_interval).'      
    end

	'tband': self -> set, spex_fit_time_interval = self->get(/spex_tband)

	'change_times': begin
		widget_control, state.w_int_sel, get_value=full_options
		self -> intervals, /fit, full_options=full_options
		end

	'forcetodata': self -> adjust_intervals

	'rm_times': (self->get(/obj,class='spex_fitint')) -> find_bad_intervals, /remove

	'unsel_times': begin
		(self->get(/obj,class='spex_fitint')) -> find_bad_intervals, bad=bad
		left = rem_elem(sel_intervals, bad)
		if left[0] ne -1 then begin
			sel_intervals = sel_intervals[left]
			widget_control, state.w_times, set_list_select=sel_intervals
		endif else $
			xmessage,'Not changed because there would be no intervals remaining after remove.'
		end
		
	'rm_selected': begin
	  intervals = self -> get(/spex_fit_time_interval)
	  int_ind = indgen(n_elements(intervals[0,*]))
	  left = rem_elem(int_ind, sel_intervals)
	  new = left[0] eq -1 ? -1 : intervals[*,left]
	  self->set, spex_fit_time_interval=new 
	  end

	'draw_times_all': begin
		if self->valid_plot(/utplot) then begin
			plotman_obj -> select
			plotman_obj -> plot
		endif else self->plot_time,/bksub,/show_filter ;self -> plotman, class='spex_bksub'
		intervals = self -> get(/spex_fit_time_interval)
		if intervals[0] ne -1 then $
			plotman_draw_int,'all',{plotman_obj:plotman_obj}, intervals=intervals, type='Analysis'
		end

	'draw_times_sel': begin
		if self->valid_plot(/utplot) then begin
			plotman_obj -> select
			plotman_obj -> plot
		endif else self->plot_time,/bksub,/show_filter; /self -> plotman, class='spex_bksub'
		intervals = self -> get(/spex_fit_time_interval)
		if intervals[0] ne -1 and sel_intervals[0] ne -1 then $
			plotman_draw_int,'all',{plotman_obj:plotman_obj}, intervals=intervals[*,sel_intervals], type='Analysis'
		end

	'draw_times_fitted': begin
		if self->valid_plot(/utplot) then begin
			plotman_obj -> select
			plotman_obj -> plot
		endif else self->plot_time,/bksub,/show_filter; /self -> plotman, class='spex_bksub'
		ndone = 0
		intervals = self -> get(/spex_summ_time_interval)
		if size(intervals,/tname) ne 'POINTER' then begin
			if intervals[0] ne -1 then begin
				fit_done = where (self -> get(/spex_summ_fit_done), ndone)
				if ndone gt 0 then $
					plotman_draw_int,'all',{plotman_obj:plotman_obj}, intervals=intervals[*,fit_done], type='Analysis'
			endif
		endif
		if ndone eq 0 then message,'No intervals fit yet.', /cont
		end	
	
;	'draw_filter': begin
;		if not self->valid_plot(/utplot) then self -> plotman, class='spex_bksub'
;		plotman_obj -> select & self -> filter_bars, /plot
;		plotman_obj -> unselect
;		end

	'change_erange': begin
;		erange_choice = state.erange_choices[widget_info(state.w_erange_plot, /droplist_select)]
;		if strpos(erange_choice, 'Standard') ne -1 then $
    if not self->valid_plot(/xyplot) then $
			self -> plot_spectrum, /overlay_back, spex_units='flux', this_interval=sel_intervals
		widget_control, state.w_int_sel, get_value=full_options
		self -> intervals, /erange, full_options=full_options
		end
  
  'auto_erange': self->set,spex_fit_auto_erange = event.select
  
  'auto_emin': self->set,spex_fit_auto_emin = event.select
  
	'draw_erange': begin
		if not self->valid_plot(/xyplot) then $
			self -> plot_spectrum, /overlay_back, spex_units='flux', this_interval=sel_intervals
		intervals = self -> get(/spex_erange)
		if intervals[0] ne -1 then $
			plotman_draw_int,'all',{plotman_obj:plotman_obj}, intervals=intervals, type='Analysis'
		end

	'showfunc': self->list_function, /simple

	'first_method': begin
  	  firstint_method = state.firstint_methods[event.index]
  	  if firstint_method eq 'finterval' or firstint_method eq 'sinterval' then begin
  	      widget_control, state.w_interval_init, get_value=val
  	      firstint_method = strmid(firstint_method,0,1) + trim(val)
  	  endif
  	  self -> set, spex_fit_firstint_method = firstint_method
  	  end

	'interval_init': begin
	    if tag_exist (event, 'ENTER') then if event.enter eq 1 then return
      widget_control, event.id, get_value=value
	    firstint_method = self -> get(/spex_fit_firstint_method)
	    f_or_s = strmid(firstint_method,0,1)
	    self -> set, spex_fit_firstint_method = f_or_s + trim(value)
	    end

  'param_only': self -> set, spex_fit_init_params_only = event.select

	'subs_method': self -> set, spex_fit_start_method = state.start_methods[event.index]

	'maxiter': self -> set, mcurvefit_itmax = event.value	;!!! make general for other than mcurvefit

	'uncert': self -> set, spex_uncert = event.value

	'mode': self -> set, spex_fit_manual = event.index

	'reverse': self -> set, spex_fit_reverse = event.index

;	'set_fit': begin
;		self -> set, spex_interval_index = sel_intervals[0]
;		self -> xfit_comp
;		end

	'autoplot': self -> set, spex_autoplot_enable=event.select

	'autoresid': self -> set, spex_fitcomp_plot_resid=event.select

	'progbar': self -> set, spex_fit_progbar=event.select

	'do_fit': begin
		; for speed, get obj and set directly, using /this_class_only
		fobj = self->get(/obj, class='spex_fit')
		fobj->set, spex_autoplot_units = units, $
			spex_autoplot_bksub = bksub, $
			spex_autoplot_overlay_back = overlay_back, $
			spex_autoplot_photons = photons, $
			spex_autoplot_error = show_err, $
			/this_class_only
		self -> dofit, spex_intervals_tofit=sel_intervals, gui_label=state.gui_label
		end

	'fitsummary': self -> fitsummary

	'plot_spec': begin
;		bksub = widget_info(state.w_plotbksub, /button_set)
;		overlay_back = widget_info (state.w_overlay_back, /button_set)
;		show_err = widget_info (state.w_show_err, /button_set)
;		photons = widget_info (state.w_phot, /button_set)
;		show_fit = widget_info (state.w_show_fit, /button_set)
;		show_comb = widget_info (state.w_show_comb, /button_set)
		self -> plot_spectrum, bksub=bksub, overlay_back=overlay_back, overlay_bksub=overlay_bksub, $
			photons=photons, spex_units=units, $
			show_fit=show_fit, show_err=show_err, $
			this_interval=sel_intervals, comb_func=show_comb

;		self -> plotman, class='spex_fit', $
;			spex_units=units, $
;			no_background=(show_bk eq 0), $
;			no_fit=no_fit, $
;			photons=phot_spec, $
;			this_interval=sel_intervals
		end

	'plot_resid_main': (self -> get(/obj,class='spex_fit')) -> plot_resid, interval=sel_intervals, /main_window
	
	'plot_raw_resid_main': (self -> get(/obj,class='spex_fit')) -> plot_resid, interval=sel_intervals, /main_window, /raw

	'plot_resid': (self -> get(/obj,class='spex_fit')) -> plot_resid, interval=sel_intervals

	'plot_resid_sep': (self -> get(/obj,class='spex_fit')) -> plot_resid, interval=sel_intervals, /separate_panels
	
	'plot_spec_normresid': self->plot_spectrum_resid, bksub=bksub, overlay_back=overlay_back, overlay_bksub=overlay_bksub, $
      photons=photons, spex_units=units, $
      show_fit=show_fit, show_err=show_err, $
      this_interval=sel_intervals, comb_func=show_comb

  'plot_spec_rawresid': self->plot_spectrum_resid, bksub=bksub, overlay_back=overlay_back, overlay_bksub=overlay_bksub, $
      photons=photons, spex_units=units, $
      show_fit=show_fit, show_err=show_err, $
      this_interval=sel_intervals, comb_func=show_comb, /raw
      
	'refresh':

	'clear':  begin
		msg = ['WARNING.  Proceeding will reinitialize all saved fit results ', $
			'in spex_summ structure to null values.']
		answer = xanswer ([msg, '', 'Do you want to continue?'],  /str, default=1, justify_it='|')
		if answer eq 'y' then self->clearsumm
		end
		
  'editsumm': self->xedit_summ, group=event.top, gui_label=state.gui_label
  
	'exit': widget_control, state.wbase, /destroy

	else:
endcase

if xalive (state.wbase) then state.object -> xfit_update_widget, state.wbase
end

;-----

pro spex::xfit, group=group, gui_label=gui_label

if xregistered ('xfit') then begin
	xshow,'xfit', /name
	return
endif

checkvar, gui_label, ''

get_font, font, big_font=big_font

wbase = widget_base (group=group, title='SPEX Fit Options '+gui_label, /scroll)

wbox = widget_base(wbase, /frame, /column, xpad=1, ypad=1)

tmp = widget_label (wbox, value='Fit Options', $
	font=big_font, /align_center)

w_int_sel = cw_bgroup (wbox, ['Graphical', 'Full Options'], /row, $
	/exclusive, label_left='Interval Selection: ', $
	/return_index, uvalue='None', /no_release, set_value=0)

w_time_base = widget_base (wbox, /row, space=15, /frame)

w_time_base0 = widget_base (w_time_base, /column)

w_ntimes = widget_label (w_time_base0, value='', /align_left, /dynamic_resize)
w_times = widget_list (w_time_base0, $
 					/multiple, $
					ysize=10, $
					xsize=53, $
					value='', $
					uvalue='times')

w_time_base1 = widget_base (w_time_base, /column, space=8)
w_set = widget_button (w_time_base1, value='Set intervals to ->', /menu)
tmp = widget_button (w_set, value='to raw file intervals', uvalue='all_raw_times')
tmp = widget_button (w_set, value='to stored intervals (in spex_summ)', uvalue='summ_times')
tmp = widget_button (w_set, value='to spex_tband', uvalue='tband')
tmp = widget_button (w_time_base1, value='Change Fit Intervals', uvalue='change_times')
w_remove = widget_button (w_time_base1, value='Adjust Intervals ->', /menu)
tmp = widget_button (w_remove, value='Force to data boundaries', uvalue='forcetodata')
tmp = widget_button (w_remove, value='Remove bad intervals completely', uvalue='rm_times')
tmp = widget_button (w_remove, value='Unhighlight bad intervals', uvalue='unsel_times')
tmp = widget_button (w_remove, value='Remove highlighted intervals', uvalue='rm_selected')
w_time_base2 = widget_base (w_time_base, /column, space=10)

filt = self -> get(/spex_fitint_filter)
mm_filt = minmax(filt)

w_sel = widget_button (w_time_base2, value='Select ->', /menu)
tmp = widget_button (w_sel, value='All', uvalue='sel_all')
tmp = widget_button (w_sel, value='All within plot limits', uvalue='sel_all_plot')
tmp = widget_button (w_sel, value='Fitted intervals', uvalue='sel_fitted')

if mm_filt[0] ne mm_filt[1] then begin
	filt_uniq = get_uniq(filt)
	mode_text = ['from all intervals', 'from highlighted intervals', 'from intervals within plot limits']
	mode_code = ['all', 'highl', 'plot']
	for i=0,n_elements(filt_uniq)-1 do begin
		;if filt_uniq[i] ne -99 then begin
			ff = trim(filt_uniq[i])
			w_filt = widget_button (w_sel, value='Intervals with filter='+ff, /menu)
			for j=0,2 do $
				tmp = widget_button (w_filt, value=mode_text[j], uvalue='sel_'+mode_code[j]+'_filt'+ff)
		;endif
	endfor
endif
w_show = widget_button (w_time_base2, value='Show Fit Intervals ->', /menu)
tmp = widget_button (w_show, value='All defined intervals', uvalue='draw_times_all')
tmp = widget_button (w_show, value='Selected intervals', uvalue='draw_times_sel')
tmp = widget_button (w_show, value='Fitted intervals', uvalue='draw_times_fitted')
;tmp = widget_button (w_time_base2, value='Show Filter States', uvalue='draw_filter')

w_erange_base = widget_base (wbox, /row, space=10, ypad=4, /frame)
w_bands_base = widget_base (w_erange_base, space=0, /row)
w_nbands = widget_label (w_bands_base, value='', /dynamic_resize)
w_erange = widget_droplist (w_bands_base, title='Energy range(s) to fit:', $
					value='xxxxxxxxxxxxxxxxxxxxx', uvalue='none')
;w_nbands = widget_label (w_bands_base1, value='', /dynamic_resize)
tmp = widget_button (w_erange_base, value='Change', uvalue='change_erange')
;erange_choices = ['Current spectrum plot', 'Standard erange plot']
;w_erange_plot = widget_droplist(w_erange_base, title='Use:', value=erange_choices, uvalue='none')

w_autobase = widget_base(w_erange_base, /row, space=0, /frame)
w_tmp = widget_label(w_autobase, value='Auto-set: ')
w_autoset_base = widget_base (w_autobase, /row, /nonexclusive, space=0)
w_auto_erange = widget_button (w_autoset_base, value='Max', uvalue='auto_erange', $
  tooltip='Automatically set upper limit of energy range based on bk-subtracted counts/bin. ' + $
   'Change threshold via spex_fit_auto_emax_thresh parameter.')
w_auto_emin = widget_button (w_autoset_base, value='Min', uvalue='auto_emin', $
  tooltip='Automatically set lower limit of energy range (RHESSI only).')
    
tmp = widget_button (w_erange_base, value='Show', uvalue='draw_erange')

w_fit_base = widget_base (wbox, /column, /frame)

w_current_base = widget_base (w_fit_base, /frame, /row)
tmp = widget_button (w_current_base, value='Show Func', uvalue='showfunc', /align_center)
w_current_base2 = widget_base (w_current_base, /column)
; blank line before and after, just so not so crowded looking
tmp = widget_label (w_current_base2, value='')
w_current = widget_label (w_current_base2, value='', /dynamic_resize, /align_left)
tmp = widget_label (w_current_base2, $
	value='When Loop Mode is Manual, the "Do Fit" button lets you set the function and parameters', $
	/align_left)
tmp = widget_label (w_current_base2, value='')

w_fit_base0 = widget_base (w_fit_base, /row, space=10);, /align_center)

;w_mode = widget_droplist (w_fit_base0, title='Loop Mode: ', $
;	value=['Automatic','Manual First + Auto','Manual All Intervals'], uvalue='mode')
;
;w_reverse = widget_droplist (w_fit_base0, title='Loop Direction: ', $
;	value=['Forward','Backwards'], uvalue='reverse')

;w_loopbase = widget_base (w_fit_base0, /column, /align_left)
w_mode = widget_droplist (w_fit_base0, title='Loop Mode: ', $
  value=['Automatic','Manual First + Auto','Manual All Intervals'], uvalue='mode')
w_reverse = widget_droplist (w_fit_base0, title='Loop Direction: ', $
  value=['Forward','Backwards'], uvalue='reverse')

firstint_methods = ['default', 'current', 'finterval', 'sinterval']
first_text = ['Program Defaults', $
  'Current values (shown above)', $
  'Final Fit Parameters from Interval: ', $
  'Starting Fit Parameters from Interval: ']
first_uvalue = ['first_def', 'first_curr', 'first_final', 'first_start']

start_methods = ['default', 'previous_int', 'previous_start', 'previous_iter']
subs_text = ['Program Defaults', $
  'Fitted parameters from most recently fit interval', $
  'Starting parameters from most recently fit interval', $
  'Fitted parameters from previous iteration on each interval']
subs_uvalue = ['subs_def', 'subs_prevfit', 'subs_prevstart', 'subs_previter']

w_method_base = widget_base (w_fit_base, /row, space=10, /frame)
w_method_base0 = widget_base (w_method_base, /column)
tmp = widget_label (w_method_base0, value='Parameter Initialization Method')
w_param_only_base = widget_base (w_method_base0, /column, /nonexclusive, space=0)
w_param_only = widget_button (w_param_only_base, value='Init Fit Parameters Only', uvalue='param_only',$
  tooltip=['If not selected, min,max,free, and possibly erange,uncert,#iter are set as well as ' + $
    'fit parameters depending on init method.'])

w_method_basec = widget_base (w_method_base, /column)
w_method_base1 = widget_base (w_method_basec, /row, space=5)

w_first = widget_droplist (w_method_base1, title='First Interval: ', value=first_text, uvalue='first_method')
w_interval_init = widget_text ( w_method_base1, $
          value='0', $
          uvalue='interval_init', $
          /editable, $
          xsize=5, $
          /kbrd_focus_events )

w_method_base2 = widget_base (w_method_basec, /row, space=5)
w_subs = widget_droplist (w_method_base2, title='Subsequent Intervals: ', value=subs_text, uvalue='subs_method')

w_fit_base2 = widget_base (w_fit_base, /row, space=10, /align_center)

w_maxiter = cw_edroplist (w_fit_base2, value=1, format='(i5)', $
	xsize=5, label='# Iter: ', drop_values=[0,1,5,10,20,50,100,200,300], uvalue='maxiter')

w_uncert = cw_edroplist (w_fit_base2, value=.05, format='(f4.2)', $
	xsize=5, label='Systematic Uncert: ', drop_values=[0.0, .01,.02,.03,.04,.05,.06,.07,.08,.09,.10], $
	uvalue='uncert')

w_fit_base3 = widget_base (w_fit_base2, /row, space=10, /frame)
tmp = widget_label(w_fit_base3, value='Show:')
w_plot_fit_base0 = widget_base (w_fit_base3, /row, /nonexclusive, space=2)
w_autoplot = widget_button (w_plot_fit_base0, value='Fit', uvalue='autoplot')
w_autoresid = widget_button (w_plot_fit_base0, value='Residuals', uvalue='autoresid')
w_progbar = widget_button (w_plot_fit_base0, value='Progress Bar', uvalue='progbar')

w_plot_base = widget_base (wbox, /row, space=10, /frame)

w_plot_base1 = widget_base (w_plot_base, /column)

self -> units_widget, w_units, parent=w_plot_base1, units=self->get(/spex_autoplot_units)
;units = ['Counts', 'Rate', 'Flux']
;w_units = widget_droplist (w_plot_base1, title='Plot Units: ', $
;	value=units, uvalue='none')

w_plot_col1 = widget_base (w_plot_base1, /column, /nonexclusive, space=0)
w_phot = widget_button (w_plot_col1, value='Photons', uvalue='phot_spec')
w_show_fit = widget_button (w_plot_col1, value='Show Fit', uvalue='show_fit')
w_show_comb = widget_button (w_plot_col1, value='Combined Function Only', uvalue='show_comb')

w_plot_base2 = widget_base (w_plot_base, /column)

w_plot_col2 = widget_base (w_plot_base2, /column, /exclusive, space=0)
w_plotdata = widget_button (w_plot_col2, value='Show Data')
w_plotbksub = widget_button (w_plot_col2, value='Show Data-Background')
w_plot_col2b = widget_base (w_plot_base2, /column, /nonexclusive, space=0)
w_overlay_back = widget_button (w_plot_col2b, value='Show Background')
w_plot_col2c = widget_base (w_plot_base2, /column, /nonexclusive, space=0)
w_show_err = widget_button (w_plot_col2c, value='Show Error')


; set the above options to default to the values in the spex_autoplot... parameters
autop = self -> get(/spex_autoplot)
;index = str_fuzzy (['co', 'ra', 'fl'], autop.spex_autoplot_units)
;if index eq -1 then index=1
;widget_control, w_units, set_droplist_select=index
widget_control, w_phot, set_button=autop.spex_autoplot_photons
widget_control, w_plotdata, set_button=(autop.spex_autoplot_bksub eq 0)
widget_control, w_plotbksub, set_button=autop.spex_autoplot_bksub
widget_control, w_overlay_back, set_button=autop.spex_autoplot_overlay_back
widget_control, w_show_err, set_button=autop.spex_autoplot_show_err

w_plot_base3 = widget_base (w_plot_base, /column, /align_center, space=10)
tmp = widget_button (w_plot_base3, value='Plot Spectrum in Fit Intervals', $
	uvalue='plot_spec', /align_center)

w_resid_plot = widget_button (w_plot_base3, value='Plot Resid in Fit Intervals ->', /menu, /align_center)
tmp = widget_button (w_resid_plot, value = 'Normalized Resid in Main Window', uvalue='plot_resid_main')
tmp = widget_button (w_resid_plot, value = 'Raw Resid in Main Window', uvalue='plot_raw_resid_main')
tmp = widget_button (w_resid_plot, value='Normalized Resid in Residual Window, one panel', uvalue='plot_resid')
tmp = widget_button (w_resid_plot, value='Normalized Resid in Residual Window, separate panels', uvalue='plot_resid_sep')

w_spec_resid_plot = widget_button (w_plot_base3, value='Plot Spectrum and Resid in Fit Intervals ->', /menu, /align_center)
tmp = widget_button (w_spec_resid_plot, value = 'Normalized Residuals', uvalue='plot_spec_normresid')
tmp = widget_button (w_spec_resid_plot, value = 'Raw Residuals', uvalue='plot_spec_rawresid')

wbase_but2 = widget_base (wbox, /row, space=20, /align_center)
tmp = widget_button (wbase_but2, value='Refresh', uvalue='refresh')
tmp = widget_button (wbase_but2, value='Clear Stored Results', uvalue='clear')
tmp = widget_button (wbase_but2, value='Edit Stored Results', uvalue='editsumm')
tmp = widget_button (wbase_but2, value='Do Fit', uvalue='do_fit')
tmp = widget_button (wbase_but2, value='Show Fit Summary', uvalue='fitsummary')
tmp = widget_button (wbase_but2, value='Close', uvalue='exit')

state = {wbase: wbase, $
	w_int_sel: w_int_sel, $
	w_times: w_times, $
	w_ntimes: w_ntimes, $
	w_units: w_units, $
;	units: units, $
	w_phot: w_phot, $
	w_plotdata: w_plotdata, $
	w_plotbksub: w_plotbksub, $
	w_overlay_back: w_overlay_back, $
	w_show_err: w_show_err, $
	w_show_fit: w_show_fit, $
	w_show_comb: w_show_comb, $
	w_nbands: w_nbands, $
	w_erange: w_erange, $
;	w_erange_plot: w_erange_plot, $
;	erange_choices: erange_choices, $
  w_auto_erange: w_auto_erange, $
  w_auto_emin: w_auto_emin, $
	w_current: w_current, $
	w_param_only: w_param_only, $
	w_first: w_first, $
	w_interval_init: w_interval_init, $
	w_subs: w_subs, $
	w_maxiter: w_maxiter, $
	w_uncert: w_uncert, $
	times: ptr_new(-99), $		; save this since can't do get_value on list widget
	filters: ptr_new(-99), $
	firstint_methods: firstint_methods, $
	start_methods: start_methods, $
	w_mode: w_mode, $
	w_reverse: w_reverse, $
	w_autoplot: w_autoplot, $
	w_autoresid: w_autoresid, $
	w_progbar: w_progbar, $
	object: self, $
	gui_label: gui_label }

widget_control, wbase, set_uvalue=state

widget_limit_ysize, wbase, wbox

widget_offset, group, newbase=wbase, xoffset, yoffset

widget_control, wbase, xoffset=xoffset, yoffset=yoffset

widget_control, wbase, /realize

self -> xfit_update_widget, wbase

xmanager, 'xfit', wbase, /no_block, cleanup='spex_xfit_cleanup'

end
