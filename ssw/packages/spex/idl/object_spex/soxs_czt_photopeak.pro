;+
;Procedure for determining photopeak response of SOXS czt detector
;Based on discussions at PRL with R Jain and KJ Shah
;;
; the response is computed from the exposed geometric area thru the
; collimator circle, the absorption from the Be, Al, and Kapton and then
; the prob of single pe detection in czt. A correction to the photopeak
; efficiency was obtained by a second order fit of the detector efficiency
; data which included the Be window of 11 mil.  We therefore group the be
; window with the detector and treat the Al and Kapton separately as absorbers


;-

function soxs_czt_photopeak, e, area=area, nowindow=nowindow, error=error
error = 1

;zn is 30 doping
;cd is 48, atomic - 112.41
;Te is 52, atomic - 127.60
;cdte density is 5.8 gm/cm3
;actual density of czt

;density of cd is 2.7 gm/cm3
;density of te is 3.1 gm/cm3
be = 290 * 1e-4 ;11 mil beryllium for czt - from fit to efficiency

cz = 0.2 ;cm
default, area, 0.18
;acz= 0.18 ; from 0.48cm circle

xcd=xsec(e,48,'pe',/cm2)*2.7
xte=xsec(e,52,'pe',/cm2)*3.1
dtct = (1-exp(-(xte+xcd)*cz))*exp(-xsec(e,4,'pe')*be)
pcoeff =  [9.894531e-001 , 4.578042e-004  ,1.834513e-005, -4.017244e-007]


correction = poly( e, transpose(pcoeff))
w = where( e ge 60, nw)

out = dtct
if nw ge 1 then out[w] = out[w]/correction[w]
;Add kapton and al
xalcm = xsec(e,13,'pe')


al = 95 * 1e-4
kp = 150 * 1e-4
tfile = obj_new('tfile')
tfile->Set, path = 'SSWDB_SOXS'
kapton = tfile->getdata(/auto,/convert, filename='kaptonxs.txt')
obj_destroy, tfile
test =  is_number(kapton[0])

if not test then begin
	message,/cont, 'Invalid kapton file'
	return, 0
	endif

xkapton= interpol(kapton[1,*],kapton[0,*],e) * 1.42>0 ;1.42gm/cm3 kapton

if not keyword_set(nowindow) then out = out * exp(- (xkapton*kp +xalcm*al))

error=0
return, out * area
end
