;+
; Name: SPEX_FIT__DEFINE
;
; Purpose: This object manages the Fitting for OSPEX
;
; Category: OSPEX
;
; Written: 2003, Kim Tolbert
; Modifications:
;   21-Jun-2004, Kim. Switched to using progressbar object (from progbar and showprogress object)
;     because it wasn't trapping the 'cancel' clicks.
;   22-Jun-2004, Kim.  Removed spex_interval_range parameter from control parameters, and
;     added spex_intervals_tofit.  Now can specify non-contiguous intervals to fit.
;   20-Jul-2004, Kim.  Added spex_summ_filter to fit results structure
;   09-Sep-2004, Sandhia.  Moved definition of spex_summ structure to a separate routine
;                                  spex_summ_define_str.  This is to avoid duplication of definition.
;   17-Sep-2004, Kim.  Added bk_rate and bk_error to spex_summ structure
;	5-Oct-2004, Kim.  In process, call getdata for fitint class at start, so bad intervals will be
;	  removed immediately, then adjust intervals_tofit accordingly.  Also use spex_fit_time_used
;	  instead of spex_fit_time_interval for summary data since it will show exact times used (since
;	  spex_fit_time_interval might not be on data boundaries)
;	29-Jun-2005, Kim. Position progbar at lower left of screen
;	25-Aug-2005, Kim.  Added class= to calls to get wherever possible to speed things up
;	  Since get(/fit_comp,class='fit_comp_manager') no longer include fit_comp_sigmas in structure,
;	  get sigmas directly with a get.
;	15-Mar-2006, Kim.  Added epsilon=1.e-5 to call to find_contig_ranges
;	15-Mar-2006, Kim.  In process, add _extra to call to xfit_comp to pass through gui_label
;	7-Apr-2006, Kim.  Changes for addition of spectrum,model keywords:  Added setting
;	  corresponding new spex_summ params. Added ncomp arg in call to spex_summ_define_str
;	8-Aug-2006, Kim.  Change object chain structure.  Previously spex_fitalg was the source here.
;	  Now fitrange is first source and fitalg is second source.  Set the spex_fit obj reference (self)
;	  into the fitalg object so it can get to everything.  This fixes the problem of every set into
;	  a lower level object being done twice.
;	30-Oct-2006,Kim.  Do heap_gc after every fit.  Use this_class_only when
;	  saving spex_summ params in obj.  Both help with running out of memory issue.
;	  Also, before starting fit loop, find bad intervals and skip over them during loop.
;	8-Nov-2006, Kim.  Allow users to change fit function while fitting.  Added check_func method.
;	  Adds and removes elements from fit information arrays in spex_summ structure for new or
;	  deleted components.
;	  Also, don't replot interval if status is 2 (already did fit in fit_comp widget)
;	24-Apr-2007, Kim. spex_summ_area was only being set when init spex_summ struct. But
;	  if change files to one with fewer dets, but use same times and energies, init isn't done,
;	  so area is wrong. Now set it after the fit.  Thanks to Jana for tracking this down.
;	  Also, check if area has changed and reinit spex_summ structure if it has.  Otherwise, if
;	  don't refit all intervals, some intervals will have wrong area.
;	23-May-2007, Kim. Fix check for same area (can't use = for reals)
;	20-Sep-2007, Kim.  In check_func, when adding function component but want it to have no effect,
;	  set a[0] to 0 for all functions except drm_mod (previously excluded drm_mod and pileup_mod).
;	17-Jan-2008, Kim. Progress bar red didn't fill correctly when in reverse fitting mode
;	12-Feb-2008, Kim.  Added initializing spex_fitcomp_plot_bk param to 0
;	27-Feb-2008, Kim.  Added quick_summary method to show current values of params, sigmas
;	  and chisq - called from spex_fit__xfit_comp 'Fit Summary' button.
;	05-Mar-2008, Kim. In process, use valid_summ_params to check if prev_iter is usable, (previously
;	  used spex_summ_fit_done, but crashed if new intervals.
;	25-Mar-2008, Kim.
;	  1.Added check_times method.  If user changes fit times, they will be asked if
;	  spex_summ should be reset to accommodate different intervals (in non-interactive mode, just does
;	  it) - for any intervals that match, moves old intervals' info to corresponding place in new
;	  arrays.  Called at beginning of process so after adjustment everything will use new dimensions.
;	  2. Added init_summ method.
;	  3. Removed time check after return from xfit_comp, since we already checked times in check_times.
;	27-Mar-2008, Kim.  plot_resid changed to use values from spex_summ if x value is not passed in
;	  via keyword. So now plots either the current vals (passed in as args) or the fit results vals.
;	  Also put resid data xyplot obj and pass that into plotman - allows us to plot multiple intervals.
;	  In check_times, if summ structure is null pointers, just return.  Call plot_resid
;	  with chisq arg.
;	3-Apr-2008, Kim. Added current + prev int method for start parameters.
;	4-Apr-2008, Kim. Modified plot_resid method to add options to use main window or residual
;	  plotman window, or to plot each interval in a separate panel or all in one panel (only
;	  available when plotting int resid plotman)
;	8-Apr-2008, Kim.  Fixed x,y titles on residual plots
; 23-Apr-2008, Kim. Modified parameter initialization methods.  Added separate options
;   for specifying first interval ('default','current','fnnn','snnn') and subsequent interval
;   ('default','previous_int', 'previous_start', 'previous_iter') parameters.  On subs int, for
;   previous_int (starting or fitted) also transfer other fit_comp params, and emask, uncert and maxiter
;   from previous int - previously just transfered fit_comp_params.
; 25-Apr-2008, Kim.  reinitialize params after calling init_summ in process, since func may have changed.
; 23-Jun-2008, Kim. In check_func, if none of function components are the same, reinit structure, and
;   reinit params array to correct new dimensions.
; 10-Jul-2008, Kim.  Make check_func a procedure instead of a function.  Wasn't using function value.
;	Added some keywors to check_func, so could call it from spex_fit__xfit_comp as well.
;	Make sure progbar widget is killed no matter how we got out of loop.
; 27-Aug-2008, Kim. In plot_resid, allow summing plot (dim1_enab_sum=1)
; 08-Dec-2008, Kim.  Added auto_erange method.  Call it in process if spex_fit_auto_erange is set to
;   automatically set the upper limit of energy range to fit.
; 09-Jan-2009, Kim. In plot, use addplot with oplot_xmarkers to plot erange, so it persists
; 29-May-2009, Kim.  Added spex_fit_init_params_only.  If set, only set params themselves in various
;   methods for getting starting values for fits from firstint_method and start_method. If not set,
;   transfer all fit_comp... params, energy ranges, uncertainty, maxiter.  
; 14-Jul-2009, Kim. Added energy range of fit to quick_summary
; 17-Aug-2009, Kim. In plot_resid, destroy xy_obj at end. memory leak.
; 26-Sep-2009, Kim. In process, fixed bug where if change function from command line, and then enter xfit_comp,
;   really screwed up.  Now call check_func before start loop through intervals.  Pass this summ structure to
;   xfit_comp, so it can check for changes against it (not stored spex_summ).  Also, in check_func, wasn't storing
;   new fit_function in new summ structure. 
; 3-Sep-2010, Kim.  Added a set_fitting_params method, and moved setting initializing params to
;   this method (so I could call this method from chi2_map and monte_carlo routines).
; 28-Sep-2010, Kim, Added quiet keyword to process. 
; 03-Oct-2012, Kim. For spex_fit_firstint_method equal to fn or sn, set set_all to 1 so we'll set erange, ucert
;   and iter max for those cases.
; 11-Oct-2012, Kim.  Added initializing spex_fitcomp_plot_err param to 0.  And added raw keyword to plot_resid.
; 14-Nov-2012, Kim.  Added status keyword to plot_resid
; 28-Aug-2013, Kim. In check_func method, removed a big chunk into a separate routine spex_change_summ_comp, and call it
; 30-Oct-2013, Kim. In auto_erange, change auto max method to use counts/bin > spex_fit_auto_emax_thresh (new parameter) rather 
;   than count rate > .01.  Also changed to use input args or get parameter settings, do min as well
;   as max (min only for RHESSI).  In process, if auto-set is selected for previous_iter mode, pop up warning.
; 02-Apr-2014, Kim.  Added plot_dem method to plot Differential Emission Measure
; 16-Apr-2014, Kim.  In process and check_times methods, added source_xy and source_angle to spex_summ structure
; 26-Jun-2014, Kim.  Added plot_edf method to plot electron distribution for thick target models. Also call spex_get_dem with 
;   /use_temp_minmax.
; 11-Jul-2014, Kim. Generalize plot_edf method to work for any of the thick or thin functions allowed.
;
;-
;----------------------------------------------------------------------------------------------------

function spex_fit::init, $
    source=source, $
    _extra=_extra

if not obj_valid( source ) then source = obj_new( 'spex_fitrange', _extra=_extra )

ret = self->framework::init( source=source, $
                 control = spex_fit_control(), $
                 info = {spex_fit_info}, $
                 _extra=_extra )

fitalg_obj = obj_new('spex_fitalg')
fitalg_obj -> set, spex_fit_obj = self

self -> set, source=fitalg_obj, src_index=1

if keyword_set(_extra) then self -> set, _extra=_extra

self -> set, spex_autoplot_enable=1, $
    spex_autoplot_bksub = 1, $
    spex_autoplot_overlay_back = 1, $
    spex_autoplot_overlay_bksub = 0, $
    spex_autoplot_photons = 0, $
    spex_autoplot_units = 'flux', $

    spex_fitcomp_autoplot_enable = 1, $
    spex_fitcomp_plot_units = 'flux', $
    spex_fitcomp_plot_bk = 0, $
    spex_fitcomp_plot_err = 0, $
    spex_fitcomp_plot_photons = 0, $
    spex_fitcomp_plot_resid = 1, $

    spex_fit_progbar = 1
return, ret

end

;--------------------------------------------------------------------------

;pro spex_fit::set, $
;   spex_erange=erange, $
;   done=done, not_found=not_found, $
;   _extra=_extra
;
;if exist(erange) then begin
;
;   energy_mids = self -> getaxis(/ct_energy)
;
;   eindex = indgen(n_elements(energy_mids))
;
;   if not (erange[0] eq -1 or same_data(erange, [0.,0.]) ) then begin
;     eind = self -> edges2index(interval=erange)
;     eindex = eindex[eind[0]:eind[1]] ; !!!! this just handles first erange, change for multiple
;   endif
;   self -> framework::set, spex_erange=erange, spex_index_erange = eindex, done=done, not_found=not_found
;
;endif
;
;if keyword_set(_extra) then self -> framework::set, _extra=_extra, done=done, not_found=not_found
;
;end

;--------------------------------------------------------------------------

function spex_fit::get, _extra=_extra, found=found;, not_found=not_found, fw_get_id = fw_get_id

ret = self -> framework::get(_extra=_extra, found=found, not_found=not_found, fw_get_id = fw_get_id)
if size(ret,/tname) ne 'STRUCT' then return, ret

tags = tag_names(ret)
summ_params = strmatch(tags, 'spex_summ*', /fold)
q = where (summ_params eq 1,count)
if count gt 0 then begin
    fit_function = self->get(/fit_function,class='fit_function')
    nparams = fit_function_query(fit_function, /nparams)
    strategy = self -> name2strat(_extra=_extra)
    index = self -> strat2paramindex(strategy)
    if index[0] ne -1 then begin
       for i=0,count-1 do begin
         val = ret.(q[i])
         dim = size(val, /dim)
         if n_elements(dim) eq 2 and dim[0] eq nparams then begin
          val = val[index[0]:index[1],*]
          ret = rep_tag_value (ret, val, tags[i])
         endif
       endfor
    endif
endif

if size(ret,/tname) eq 'STRUCT' then if n_tags(ret) eq 1 then return, ret.(0)
return, ret
end

;--------------------------------------------------------------------------

function spex_fit::getdata, _extra=_extra

@spex_insert_catch

data = self -> framework::getdata(_extra=_extra)

return, data

end

;------------------------------------------------------------------------------

pro spex_fit::process, all_intervals=all_intervals, quiet=quiet, _extra = _extra

checkvar, quiet, 0

; by getting fitint data, we'll make sure that defined intervals are good. It will
; delete any intervals in which there's no data.
d = self -> getdata(class='spex_fitint')

fit_times = self -> get(/spex_fit_time_interval)
fit_time_used = self -> get(/spex_fit_time_used)
nintervals = n_elements(fit_times[0,*])
nparams = fit_function_query (self->get(/fit_function,class='fit_function'), /nparams)

summ = self -> get(/spex_summ)

; if there were no previous params in spex_summ, then set params to an array of zeroes.
; Otherwise, check if time intervals have changed (will adjust all arrays in structure
; if they have), and get params from structure.
if size(summ.spex_summ_params, /tname) eq 'POINTER' then begin
	params = fltarr(nparams, nintervals)
endif else begin
	summ = self->check_times(summ, status=status)
	if status eq 0 then begin
		message,"Can't use existing spex_summ structure. Aborting.", /cont
		goto, getout
	endif
	params = summ.spex_summ_params
endelse

if keyword_set(all_intervals) then $
    intervals_tofit = indgen(nintervals) $
else $
	; use get_uniq in case we delete some intervals from list after user selected intervals
    intervals_tofit = get_uniq( ((self -> get(/spex_intervals_tofit)) > 0) < (nintervals-1))

(self->get(/obj,class='spex_fitint')) -> find_bad_intervals, bad=bad
if bad[0] ne -1 then begin
	left = rem_elem(intervals_tofit, bad)
	if left[0] eq -1 then begin
		message, 'No intervals left to fit after removing bad intervals. Aborting.', /cont
		return
	endif
	if n_elements(intervals_tofit) ne n_elements(left) then begin
		q = where_arr(intervals_tofit, bad)
		message, 'Bad intervals ' + arr2str(trim(intervals_tofit[q])) + ' will be skipped during fitting loop.', /cont
		intervals_tofit = intervals_tofit[left]
	endif
endif

tot_int_tofit = n_elements(intervals_tofit)

if self->get(/spex_fit_reverse) then begin
    start_ind = tot_int_tofit-1
    end_ind = 0
    increment = -1
endif else begin
    start_ind = 0
    end_ind = tot_int_tofit-1
    increment = 1
endelse

manual = self -> get(/spex_fit_manual)
start_method = self -> get(/spex_fit_start_method)
;firstint_method = self -> get(/spex_fit_firstint_method)
firstint_method = self -> parse_firstint_method()

; If user changed func from command line by say just adding a component, then some of param initialization 
; methods won't work unless the spex summ struct is filled in with default values for the new components
; Call with /justdo since if user changed func from command line, they intended to.
self -> check_func, summ, params, /justdo
;
;func_changed = summ.spex_summ_fit_function ne self->get(/fit_function)
;if func_changed then begin
;  if start_method eq 'previous_iter' then begin
;    message,/cont,"Function has changed. Can't use start method = " + start_method + $ 
;      "  Resetting to 'current' for first int, then 'previous_int'."   
;    firstint_method.method = 'current'  
;    start_method = 'previous_int'
;  endif else begin
;    if not is_member(firstint_method.method, ['current', 'default']) then begin    
;      message, /cont, "Function has changed. Can't use first interval method = " + self -> get(/spex_fit_firstint_method) + $
;        "  Resetting to 'current'."
;      firstint_method.method = 'current'
;    endif
;  endelse
;
;endif
    
init_msg = ['', '', 'number of energies has changed', 'area has changed.']

do_spectrum_plot = self->get(/spex_autoplot_enable) and not spex_get_nointeractive()
do_resid_plot = self->get(/spex_fitcomp_plot_resid) and not spex_get_nointeractive()
do_progbar = self -> get(/spex_fit_progbar) and not spex_get_nointeractive() and tot_int_tofit gt 1

; get backround rate to put into spex_summ structure
bk = self -> getdata(class='spex_bk', spex_units='rate')
have_bk = self->get(/spex_have_bk) ? bk.data[0] ne -1 : 0
if have_bk then bkbinned = $
	(self->get(/obj,class='spex_bk')) -> bin_data (data=bk, intervals=fit_time_used, er=bkbinned_err, /do_time)

spex_allow_diff_energy = self -> get(/spex_allow_diff_energy)

offset = self->get(/spex_fit_reverse) ? 1 : -1

; Loop over selected intervals (interval numbers are in intervals_tofit array), either forward or backward
for int_index = start_ind, end_ind, increment do begin

    int = intervals_tofit[int_index]
    if ~quiet then begin
      print, ' '
      print, 'Fitting Interval ' + trim(int) + ' ' + format_intervals(fit_times[*,int], /ut)
    endif

    self -> set, spex_interval_index = int

    first_int = int_index eq start_ind
    set_all_comp = 0   ; if 1, set all fit_comp_... params
    set_all = 0        ; if 1, set all fit_comp_... params, emask, uncert, and maxiter
    err_msg = ''

    ; If first int in loop, set fit_comp_... according to firstint_method chosen
    ; Except if previous_iter is chosen, use that for all intervals.
    if first_int and start_method ne 'previous_iter' then begin

        case firstint_method.method of
            ; program defaults
            'default': (self -> get(/obj,class='fit_function')) -> set_fit_param_defaults, /all
            ; leave values as they are
            'current':
            '':
            ; Otherwise, check if method is fn or sn (where n is interval to use, f/s is for fitted/starting)
            ; If so, parse_fitint_method will return structure with finterval/sinterval in method tag.
            else: begin
              if strmid(firstint_method.method,1,99) eq 'interval' then begin
                int_use = firstint_method.interval
                if int_use ge 0 and int_use le nintervals-1 then begin
                    if self -> valid_summ_params(interval=int_use)then begin
                        set_all_comp = 1
                        set_all = 1
                        case firstint_method.fit_or_start of
                            'f': pars = summ.spex_summ_params[*,int_use]
                            's': pars = summ.spex_summ_starting_params[*,int_use]
                        endcase
                    endif else err_msg = 'Interval selected for first interval values has not been fit.  Using current values.'
                endif else err_msg = 'Interval selected for first interval values is invalid.  Using current values.'
              endif else err_msg = 'Invalid spex_fit_firstint_method value: ' + self->get(/spex_fit_firstint_method) + '  Using current values.'
              end
            endcase

    endif else begin

        ; Set fit_comp_..., emask, uncert, maxiter for subsequent intervals according to start_method
        case start_method of

           ; Use program default params for this fit function. In this case, don't set emask, uncert, maxiter
           'default': (self -> get(/obj,class='fit_function')) -> set_fit_param_defaults, /all
           ; use starting params from previous interval fit
           'previous_start': begin
               int_use = intervals_tofit[int_index + offset]
               pars = summ.spex_summ_starting_params[*,int_use]
               set_all = 1
               end
           ; use fit parameters from previous interval fit
           'previous_int': begin
               int_use = intervals_tofit[int_index + offset]
               pars = summ.spex_summ_params[*,int_use]
               set_all = 1
               end
           ; Use fit params from previous iteration of this interval (if interval has been fit, otherwise
           ; leave alone)
           'previous_iter': begin
               if self -> valid_summ_params(interval=int, /check_match) then begin
                  int_use = int
                  pars = params[*,int]
                  set_all = 1
               endif else err_msg = 'No previous iteration to use.  Using current values.'
             end

           else: err_msg = 'Invalid spex_fit_start_method value: ' + spex_fit_start_method + '  Using current values.'
       endcase

    endelse

    if err_msg ne '' then message, err_msg, /continue    
    
    self -> set_fitting_params, set_all, set_all_comp, summ, pars, int_use
    
    if first_int and start_method eq 'previous_iter' then begin
      if self->get(/spex_fit_auto_erange) or self->get(/spex_fit_auto_emin) then begin
        reset = 1
        if ~spex_get_nointeractive() then begin
      
          msg = ['You have selected the Previous Iteration method for setting parameters, but you have', $
                'enabled the Auto-Set option for the upper and/or lower limit of the Energy range to fit. ', $
                '', $
                'The Auto-Set option will override the energy range used in the previous fit of this', $
                'interval.  Normally you would not enable the Auto-Set option for previous_iter mode.']
          reset = xanswer(msg, /suppress, instruct='Do you want me to turn off the Auto-Set option before continuing?', $
            message_supp='Do not ask this question again (default answer is yes)', /check_instruct)
        endif
        if reset then self->set, spex_fit_auto_erange=0, spex_fit_auto_emin=0
      endif
    endif                      

    self->auto_erange  ; adjust low or high limit or spex_erange if requested
    results = -1 ; !!!!!!!!!!!!!!  CHANGE

    status = 1
    start = self -> get (/fit_comp, class='fit_comp_manager')

    ; manual=0 means automatic, manual=1 means manual on first interval,2 means manual on all
    ; status on return = 0/1/2 means: 0 - cancelled, 1 - continue and do fit, 2 - continue, just use fit already done
    if (manual + first_int) gt 1 then begin
    	self -> xfit_comp, /internal, start=start, final=final, summ=summ, $
    		status=status, _extra=_extra
    	do_func_reinit = 1
    endif else do_func_reinit = 0

    ;status = 0 means user cancelled out of xfit_comp, so we want to cancel this loop over intervals
    if status eq 0 then break


    ;First check if we need to reinit arrays, and ask user if OK to proceed.

    ; If on first interval, set up new arrays for spex_summ if:
    ;   1. spex_summ hasn't been defined or
    ;   2. nintervals has changed or
    ;   3. nenergies has changed
    ; Otherwise we'll just replace the spex_summ values with the
    ; new values for this interval.
    ; Have to do this here instead of before loop because user may have changed fit function
    ; in manual xfit_comp, so number of parameters may have changed.

    if first_int then begin

       ;just in case data hasn't been read yet, need to get energies into info
       dummy = self -> framework::getdata(class='spex_data')
       nenergies = n_elements(self -> getaxis(/ct_energy))

       do_init = 0
       if size(summ.spex_summ_params, /tname) eq 'POINTER' then do_init = 1 else begin
          if nenergies ne n_elements(summ.spex_summ_energy[0,*]) then do_init=2 else begin
                    if (abs(summ.spex_summ_area - self->get(/spex_area)) gt 1.e-2) then do_init=3
          endelse
       endelse

       if do_init gt 0 then begin
          if do_init gt 1 then begin
            msg = ['Have to reinitialize all saved fit results in spex_summ structure because ', $
              init_msg[do_init] ]
            if spex_get_nointeractive() then print,msg else begin
               msg = [msg, '', 'Do you want to continue?']
               answer = dialog_message (msg,  /question)
               if answer eq 'No' then begin
                  status = 0 ; so it won't print 'Completed and Stored...' message
                  break
               endif
            endelse
          endif
          ; initialize summ structure
          summ = self -> init_summ (nintervals=nintervals, nenergies=nenergies)
          params = summ.spex_summ_params
       endif

       ;start the progress bar since we're going to be looping now.
       if do_progbar then begin
         msg = (status eq 1 ? 'Working on ' : 'Saving ') + 'Interval ' + trim(int)
         progressbar = Obj_New('progressbar', Text=msg, xloc=-1, yloc=1)
         progressbar -> Start
       endif
    endif

    ; check if need to adjust summ structure for changed function
    self -> check_func, summ, params, status=chk_status, justdo=do_func_reinit
    if chk_status eq 0 then break

    ; status = 1 means we need to do the fit here.
    if status eq 1 then begin
       start = self -> get (/fit_comp, class='fit_comp_manager')    ; get again, in case changed in xfit_comp
       if not spex_get_nointeractive() and not do_progbar then $
         xmessage,['', '      Fitting ...       ', ''], wbase=wxmessage
       results = self -> framework::getdata(class='spex_fitalg', quiet=quiet, _extra=_extra)
       if xalive(wxmessage) then widget_control, wxmessage, /destroy
       final = self -> get (/fit_comp, class='fit_comp_manager')
    endif

    ; status = 2 means we already did fit in xfit_comp.  Just use the results
    if status eq 2 then begin
       fit_comp = start
       ; call fitalg getdata not to process again, just to get structure
       ; need to call getdata from the actual object for noprocess to take effect.
       results = (self->get(/obj,class='spex_fitalg')) -> getdata(/noprocess)
    endif

    ; save fit results in spex_summ structure

    eindex = self->framework::getdata(class='spex_fitrange')
    params[*,int] = final.fit_comp_params
    drm_eff = self -> get_drm_eff(use_fitted=0)

    ; could get photon flux model from count rate model returned by fit by doing the
    ; following, but then only have it at eindex energies instead of all energies, so
    ; calculate function again instead
	;   ph_model = results.fit_model / summ.spex_summ_area / $
	;     get_edge_products(summ.spex_summ_energy[*,eindex], /width) / drm_eff[eindex]
	;    obj = obj_new('fit_function')
	;    ph_model = obj -> getdata(fit_xvals=self->getaxis(/ct_energy,/edges_2), $
	;       fit_function=self->get(/fit_function,class='fit_function'), $
	;       fit_comp_params=final.fit_comp_params)
	; was creating new object.  Better to clone existing fit object so get all current params and kws.
    obj = obj_clone(self->get(/obj,class='fit_function'))
    obj -> set, fit_xvals=self->getaxis(/ct_energy,/edges_2)
    ph_model = obj -> getdata(fit_comp_params=final.fit_comp_params)
    obj_destroy, obj

    fit_comp_model = self->get(/fit_comp_model)
    summ.spex_summ_area = self->get(/spex_area)
    summ.spex_summ_fit_function = self->get(/fit_function, class='fit_function')
    summ.spex_summ_energy = self -> getaxis(/ct_energy,/edges_2)
    summ.spex_summ_time_interval = fit_time_used
    summ.spex_summ_filter = self -> get(/spex_fitint_filter)
    summ.spex_summ_fit_done[int] = 1
    summ.spex_summ_emask[*,int] = 0
    summ.spex_summ_emask[eindex,int] = 1
    summ.spex_summ_ct_rate[*,int] = results.fit_ydata
    summ.spex_summ_ct_error[*,int] = results.fit_yerror
	  summ.spex_summ_bk_rate[*,int] = have_bk ? bkbinned[*,int] : 0.
    summ.spex_summ_bk_error[*,int] = have_bk ? bkbinned_err[*,int] : 0.
    summ.spex_summ_ph_model[*,int] = ph_model
    summ.spex_summ_conv[*,int] = drm_eff
    summ.spex_summ_resid[*,int] = 0.
    summ.spex_summ_resid[eindex,int] = results.fit_resid
    summ.spex_summ_starting_params[*,int] = start.fit_comp_params
    summ.spex_summ_params[*,int] = final.fit_comp_params
    summ.spex_summ_sigmas[*,int] = self->get(/fit_comp_sigmas,class='fit_comp_manager')
    summ.spex_summ_minima[*,int] = start.fit_comp_minima
    summ.spex_summ_maxima[*,int] = start.fit_comp_maxima
    summ.spex_summ_free_mask[*,int] = self -> getalg(/free_mask)
    summ.spex_summ_func_spectrum[*,int] = self->get(/fit_comp_spectrum)
    summ.spex_summ_func_model[*,int] = fit_comp_model
    summ.spex_summ_chianti_version = chianti_kev_version()
    summ.spex_summ_abun_table = get_abun_table(model=fit_comp_model, /type)
    summ.spex_summ_source_xy = self -> get(/spex_source_xy)
    summ.spex_summ_source_angle = self -> get(/spex_source_angle)
    summ.spex_summ_maxiter[int] = self -> getalg(/maxiter)
    summ.spex_summ_uncert[int] = self -> get(/spex_uncert)
    summ.spex_summ_chisq[int] = self -> getalg(/chisq)
    summ.spex_summ_stop_msg[int] = self -> getalg(/stop_msg)
    summ.spex_summ_niter[int] = self -> getalg(/niter)

    if do_progbar then begin
        IF progressbar->CheckCancel() THEN BEGIN
         yesno = Dialog_Message('Are you sure you want to cancel this operation?', /question)
         if strlowcase(yesno) eq 'yes' then begin
          progressbar -> destroy
          return
         endif else progressbar -> setproperty,cancel=0
        ENDIF
    endif

    ; set these results here instead of waiting just in case we crash in fit interval loop, also lets
    ; autoplot get the parameters out of the summ variables for the interval just fit
    self -> set, _extra=summ, /this_class_only

; no_unique_label on plot calls means use generic labels won't save each plot as a separate
; panel.  Could generate too many panels.
    if do_spectrum_plot and status ne 2 then begin	;don't plot if did fit and plot in xfit_comp
       autop = self -> get(/spex_autoplot)
       self -> plot_spectrum, bksub=autop.spex_autoplot_bksub, $
         overlay_back=autop.spex_autoplot_overlay_back, $
         overlay_bksub=autop.spex_autoplot_overlay_bksub, $
         spex_units=autop.spex_autoplot_units, $
         photons=autop.spex_autoplot_photons, $
         /show_fit, /no_unique_label
    endif

    if do_resid_plot and status ne 2 then $	;don't plot if did fit and plot in xfit_comp
       self -> plot_resid, xdata=summ.spex_summ_energy[*,eindex], ydata=results.fit_resid, $
         chisq=self -> getalg(/chisq), interval=int, $
         xrange=minmax(summ.spex_summ_energy), /no_unique_label

    if do_progbar then begin

       message_text = ['------   Finished Interval ' + trim(int) + ',   ' + $
         format_intervals(summ.spex_summ_time_interval[*,int], /ut) + $
         ',   Function ' + summ.spex_summ_fit_function + '   ------', $
         ' Chisq= ' + trim(summ.spex_summ_chisq[int]) + '  #Iter=' + trim(summ.spex_summ_niter[int]), $
         '  Params= ' + arr2str(trim(summ.spex_summ_params[*,int]), ', ')]
       progressbar -> update, (float( abs(int_index-start_ind) +1)/tot_int_tofit)*100, text=message_text

    endif
    heap_gc ; clean up

endfor

if obj_valid(progressbar) then progressbar -> Destroy

if status gt 0 then begin
    self -> setdata, results
    if do_progbar then a = dialog_message ('Completed and stored fits for selected intervals between ' + $
         trim(min(intervals_tofit)) + ' to ' + trim(max(intervals_tofit)), /info)
endif

getout:
end

;------------------------------------------------------------------------------
; Set fitting values from summ structure into object
; set_all - if set, set all ..comp.. values plus erange, uncert, itmax
; set_all_comp - if set, set all ...comp... values
; summ - spex_summ structure to use to get values from
; pars - values to set fit_comp_params to
; int_use - interval # to get values from summ structure for

pro spex_fit::set_fitting_params,  set_all, set_all_comp, summ, pars, int_use

init_params_only = self -> get(/spex_fit_init_params_only)

if set_all or set_all_comp then begin
   print,'Using interval # ', int_use
   self -> set, fit_comp_params = pars
   if ~init_params_only then begin
      self -> set,  $
                 fit_comp_minima = summ.spex_summ_minima[*,int_use], $
                 fit_comp_maxima = summ.spex_summ_maxima[*,int_use], $
                 fit_comp_free_mask = summ.spex_summ_free_mask[*,int_use], $
                 fit_comp_spectrum = summ.spex_summ_func_spectrum[*,int_use], $
                 fit_comp_model = summ.spex_summ_func_model[*,int_use]
   endif
endif
if set_all and ~init_params_only then begin
   eindex = where (summ.spex_summ_emask[*,int_use])
   eranges = find_contig_ranges(summ.spex_summ_energy[*,eindex], epsilon=1.e-5)
   self -> set, spex_erange = eranges, $
      spex_uncert = summ.spex_summ_uncert[int_use], $
      mcurvefit_itmax = summ.spex_summ_maxiter[int_use]  ; !!!make general
endif
    
end    
;------------------------------------------------------------------------------
; function to init spex_summ structure
function spex_fit::init_summ, nintervals=nintervals, nenergies=nenergies

checkvar, nintervals, n_elements( (self->get(/spex_fit_time_used))[0,*] )
checkvar, nenergies, n_elements(self -> getaxis(/ct_energy))

fit_func = self->get(/fit_function,class='fit_function')
nparams = fit_function_query (fit_func, /nparams)
params = fltarr(nparams, nintervals)
ncomp = fit_function_query (fit_func, /ncomp)
summ = spex_summ_define_str(nintervals, nenergies, nparams, ncomp)
summ.spex_summ_area = self->get(/spex_area)
summ.spex_summ_fit_function = fit_func

return, summ

end

;------------------------------------------------------------------------------
;
; Function to check whether the fit time intervals currently selected are different from the ones used
;  in the summ structure.  If so, then ask user if they want to change the summ structure
;  to accommodate the new intervals.  (If not in interactive mode, just do it.)
; Arguments:
;  summ - input existing summ structure, and on return, new summ structure
;  status - 0 means time intervals are different, and user chose not to update spex_summ struct
;           1 means time intervals haven't changed, or user chose to update spex_summ struct, or there's
;             nothing in spex_summ structure yet

function spex_fit::check_times, summ, status=status

status = 1
if not exist(summ) then summ = self -> get(/spex_summ)

if size(summ, /tname) ne 'STRUCT' then return, summ
if size(summ.spex_summ_fit_done, /tname) eq 'POINTER' then return, summ

time_new = self -> get(/spex_fit_time_used)
time_old = summ.spex_summ_time_interval

changed = 0
if not same_data(size(time_old), size(time_new)) then changed=1 else $
      if max(abs(time_old - time_new)) gt .0015 then changed=1

if changed then begin
	status = 0
	nintervals = n_elements(time_new[0,*])
	nenergies = n_elements(summ.spex_summ_energy[0,*])
	nparams = n_elements(summ.spex_summ_params[*,0])
	ncomp = n_elements(summ.spex_summ_func_model[*,0])

	match_intervals, time_new, time_old, inew, iold, count=count, epsilon=.0015
	print,'inew = ',inew
	print,'isumm =',iold
	if count gt 0 then ptim,time_new[*,inew]
	if count gt 0 then ptim,time_old[*,iold]
	print,''


	if spex_get_nointeractive() then begin ; if not interactive, just do it
		yesno = 'Yes'
	endif else begin
		msg = 'Fit Time intervals are different from those saved in spex_summ structure.'

		if count eq 0 then begin
			msg = [msg, $
				'There are no matching time intervals.', $
				'', $
				'Reinitialize spex_summ structure? ']
		endif else begin
			msg = [msg, $
				' Number of current intervals: ' + trim(nintervals), $
				' Number of saved intervals: ' + trim(n_elements(time_old[0,*])), $
				' Number of intervals with the same times: ' + trim(count), $
				'', $
				'Will create a new spex_summ structure with ' + trim(nintervals) + ' time intervals', $
				'and merge existing intervals that match into the new structure.', $
				'', $
				'Do you want to continue?']
		endelse

		yesno = dialog_message(msg, /question)
	endelse

	if yesno eq 'Yes' then begin
		message,'Changing summ structure because fit time intervals were changed.', /cont
		; in either case, set all times to new times so that next time through here we won't think
		; they're changed, just because they're set to 0.
		if count eq 0 then begin
			new = self -> init_summ (nintervals=nintervals, nenergies=nenergies)
			new.spex_summ_time_interval = time_new
		endif else begin
			new = spex_summ_define_str(nintervals, nenergies, nparams, ncomp)
			new.spex_summ_fit_function = summ.spex_summ_fit_function
			new.spex_summ_area = summ.spex_summ_area
			new.spex_summ_energy = summ.spex_summ_energy
			new.spex_summ_time_interval = time_new	; set all times
			new.spex_summ_filter = self -> get(/spex_fitint_filter)
			new.spex_summ_fit_done[inew] = summ.spex_summ_fit_done[iold]
			new.spex_summ_emask[*,inew] = summ.spex_summ_emask[*,iold]
			new.spex_summ_ct_rate[*,inew] = summ.spex_summ_ct_rate[*,iold]
			new.spex_summ_ct_error[*,inew] = summ.spex_summ_ct_error[*,iold]
			new.spex_summ_bk_rate[*,inew] = summ.spex_summ_bk_rate[*,iold]
			new.spex_summ_bk_error[*,inew] = summ.spex_summ_bk_error[*,iold]
			new.spex_summ_ph_model[*,inew] = summ.spex_summ_ph_model[*,iold]
			new.spex_summ_conv[*,inew] = summ.spex_summ_conv[*,iold]
			new.spex_summ_resid[*,inew] = summ.spex_summ_resid[*,iold]
			new.spex_summ_starting_params[*,inew] = summ.spex_summ_starting_params[*,iold]
			new.spex_summ_params[*,inew] = summ.spex_summ_params[*,iold]
			new.spex_summ_sigmas[*,inew] = summ.spex_summ_sigmas[*,iold]
			new.spex_summ_minima[*,inew] = summ.spex_summ_minima[*,iold]
			new.spex_summ_maxima[*,inew] = summ.spex_summ_maxima[*,iold]
			new.spex_summ_free_mask[*,inew] = summ.spex_summ_free_mask[*,iold]
			new.spex_summ_func_spectrum[*,inew] = summ.spex_summ_func_spectrum[*,iold]
			new.spex_summ_func_model[*,inew] = summ.spex_summ_func_model[*,iold]
			new.spex_summ_chianti_version = summ.spex_summ_chianti_version
			new.spex_summ_abun_table = summ.spex_summ_abun_table
			new.spex_summ_source_xy = summ.spex_summ_source_xy
			new.spex_summ_source_angle = summ.spex_summ_source_angle
			new.spex_summ_maxiter[inew] = summ.spex_summ_maxiter[iold]
			new.spex_summ_uncert[inew] = summ.spex_summ_uncert[iold]
			new.spex_summ_chisq[inew] = summ.spex_summ_chisq[iold]
			new.spex_summ_stop_msg[inew] = summ.spex_summ_stop_msg[iold]
			new.spex_summ_niter[inew] = summ.spex_summ_niter[iold]
		endelse
		; set this new structure into the spex_summ variables
		self -> set, _extra=new, /this_class_only
		status = 1
		return, new
	endif

endif

return, summ

end


;------------------------------------------------------------------------------
;
; Function to check whether the fit function currently selected is different from the one used
;  in the summ structure.  If so, then ask user if they want to change the summ structure
;  to accommodate the new function.  (If not in interactive mode, just do it.)
;
; Arguments:
;   summ - current spex_summ structure.  Will be modified and returned.
;   params - current fit comp params for all intervals.  Modified ones will be returned.
;   status - 0 if user doesn't want to continue.  Otherwise leave alone.
;	justask - if set, just ask the question about reinitializing and return answer
;		in yesno keyword
;	yesno - 'Yes' or 'No' returned when justask is set
;	justdo - if set, don't ask question, just do the reinitialization
;
; Each component of new function is compared to corresponding function in summ.
; If don't match, then figure out which array elements correspond to that function
; and remove them from arrays.  If there are new components in new function, add
; default values for that component at the end of each array. (except set the normalization
; parameter to 0, so the function will have no contribution when plotted.)
; starting_params, params, sigmas, minima, maxima, free_mask are all dimensioned (nparam,ntime)
; func_spectrum and func_model are dimensioned (ncomp,ntime)
;

pro spex_fit::check_func, summ, params, justask=justask, yesno=yesno, justdo=justdo, status=status

status = 1

justask = keyword_set(justask)
justdo = keyword_set(justdo)
yesno='Yes'

; o... variables are for the old values (the ones in summ structure)

func = self -> get(/fit_function,class='fit_function')
ofunc = summ.spex_summ_fit_function
if size(ofunc,/tname) eq 'POINTER' then return

if func ne ofunc then begin
	ncomp = fit_function_query(func, /ncomp)
	oncomp = fit_function_query(ofunc, /ncomp)

	fit_comps = str2arr(func, '+')
	old_fit_comps = str2arr(ofunc, '+')

	ind = fit_function_query(func, /param_index)
	oind = fit_function_query(ofunc, /param_index)

	;loop through new and old functions, keeping track of which components and indices
	; need to be removed.
	in = 0 ; new
	io = 0 ; old
	while io lt oncomp do begin
		remove_this = 0
		if in ge ncomp then remove_this=1 else $
			if fit_comps[in] ne old_fit_comps[io] then remove_this=1
		if remove_this then begin
			i1 = oind[io,0] & n = oind[io,1]-i1+1
			param_ind_remove = append_arr(param_ind_remove, i1 + indgen(n))
			comp_ind_remove = append_arr(comp_ind_remove, io)
			comp_remove = append_arr(comp_remove, old_fit_comps[io])
		endif else in = in + 1
		io = io + 1
	endwhile

	; if we're removing all components, we don't need to merge anything.  Just call init_summ
	if n_elements(comp_remove) eq oncomp then begin
		msg = 'Reinitializing summ structure (stored fit results) because none of function components are the same.'
		if spex_get_nointeractive() or justdo then begin
			message, msg, /cont
			yesno = 'Yes'
		endif else begin
			msg = [msg, '', 'Do you want to continue?', '', 'If you say no, results will not be stored.']
            yesno = dialog_message (msg,  /question)
        endelse

        if justask then return
        if yesno eq 'Yes' then begin
        	nintervals = n_elements(summ.spex_summ_time_interval[0,*])
        	nenergies = n_elements(summ.spex_summ_energy[0,*])
        	summ = self -> init_summ(nintervals=nintervals, nenergies=nenergies)
        	nparams = fit_function_query(func, /nparams)
        	params = fltarr(nparams, nintervals)
        	return
        endif else begin
        	status = 0
        	self -> set, fit_function = ofunc  ; restore old function setting
        	return
        endelse
	endif

	; if we didn't get to the end of the new components, then need to add them
	if in le ncomp-1 then  comp_add = fit_comps[in:*]

	if spex_get_nointeractive() or justdo then begin
		message,'Changing summ structure (stored fit results) because fit function was changed.', /cont
		yesno = 'Yes'
	endif else begin
		msg = ['Function has changed.', $
				'  Old function: ' + ofunc, $
				'  New function = ' + func, $
				'Will have to adjust spex_summ structure (stored fit results)  to accommodate change in number of parameters.', $
				'', $
				'The dimensions of the following spex_summ arrays will be changed:', $
				'  spex_summ_starting_params', $
				'  spex_summ_params', $
				'  spex_summ_sigmas', $
				'  spex_summ_minima', $
				'  spex_summ_maxima', $
				'  spex_summ_free_mask', $
				'  spex_summ_func_spectrum', $
				'  spex_summ_func_model', $
				'']
		if exist(comp_remove) then begin
			msg = [msg, $
				 'Will remove fit components: ' + arr2str(comp_remove), $
				 'Will remove indices from above arrays: ' + arr2str(trim(param_ind_remove)), $
				'']
		endif
		if exist(comp_add) then begin
			msg = [msg, $
				'Will add fit components: ' + arr2str(comp_add), $
				'Will expand above arrays to accommodate new components', $
				'For intervals other than the current interval, the value of the inserted', $
				'parameters will be such that they have no effect on the computed function.', $
				'']
		endif
		msg = [msg, ' Do you want to continue?']

		yesno = dialog_message(msg, /question)
	endelse

	if justask then return
	if yesno eq 'Yes' then begin
	  spex_change_summ_comp, summ=summ, $
      comp_remove=comp_remove, param_ind_remove=param_ind_remove, comp_ind_remove=comp_ind_remove, $
      comp_add=comp_add
      
    params = summ.spex_summ_params

	endif else begin

		status=0
		self -> set, fit_function = ofunc  ; restore old function setting

	endelse

endif

return
end

;------------------------------------------------------------------------------
; Auto_erange - If requested, automatically set upper and/or lower limit of energy range to fit.
; For upper limit, look at bk-subtracted counts/bin.  For energies > 10., find the energy (emax) where 
; the counts/bin becomes less than the threshold set in spex_fit_auto_emax_thresh, and set erange 
; upper limit to that.
; For lower limit (only applies to RHESSI), look at attenuator (filter) states.  If A0, min is 4 keV, 
; if A1 or A3, min is 6 keV.
; spex_erange can be an array of start/end energies. Preserve them if they 
; are below the emax found and above the emin found.
; Prior to 30-Oct-2013, auto_erange only set the max energy limit, and used the count rate > .01 test. Also
;   previously we rounded emax found to the next multiple of 10. Now we use the upper edge of the bin found.
; 
pro spex_fit::auto_erange, this_interval=this_int, this_set_max=this_set_max, this_set_min=this_set_min

;int = self -> get(/spex_interval_index)
;d = self ->getdata(class='spex_fitint', spex_units='flux')
;e = self ->getaxis(/ct_energy,/mean)
;q = where (d.data[*,int] lt .01 and e gt 10., count)
;if count gt 0 then begin
;   ; set max energy to next multiple of 10 higher than e[q[0]]
;   emax = ceil(e[q[0]]/10.) * 10.
;   eranges = (self -> get(/spex_erange)) < emax
;   if eranges[0] eq -1 then eranges = [min(e),emax]
;   q = where (eranges[0,*] lt eranges[1,*], count)
;   if count gt 0 then begin
;      eranges = eranges[*,q]
;      nrange = n_elements(eranges)/2
;      eranges[1,nrange-1] = emax
;      self -> set, spex_erange=eranges[*,q]
;   endif else begin
;      message,/cont, 'Not auto-setting erange because no bins meet criteria.'
;   endelse
;endif

; Replace the above (where we checked for flux lt .01, and didn't set emin) with the below, 24-Oct-2013

if exist(this_int) then begin
  int = this_int
  checkvar, set_max, this_set_max, 0
  checkvar, set_min, this_set_min, 0
endif else begin
  set_max = self->get(/spex_fit_auto_erange)
  set_min = self->get(/spex_fit_auto_emin)
  int = self -> get(/spex_interval_index)
endelse  

if set_min eq 0 and set_max eq 0 then return

eranges_orig = self -> get(/spex_erange)
eranges = eranges_orig

u_str = self->get(/spex_data_units)
if u_str.data_name ne 'HESSI' then set_min = 0

e2 = self ->getaxis(/ct_energy,/edges_2)
emid = get_edges(e2,/mean)

if set_max then begin
  ct_thresh = self->get(/spex_fit_auto_emax_thresh)
  d = self ->getdata(class='spex_fitint', spex_units='counts')  
  ; find energy bins where counts/bin is < threshold (for bins above 10 keV)
  q = where(d.data[*,int] lt ct_thresh and emid gt 10., count)
  ; Bottom edge of first bin found will be the upper limit to use, or if none found, then use max of ebins
  emax = count gt 0 ? e2[0,q[0]] : max(e2)
endif else emax = max(e2)

if set_min then begin
  atten = self->get(/spex_fitint_filter) ; for RHESSI this is attenuator state
  emin = atten[int] eq 0 ? 4. : 6.
endif else emin = min(e2)

; if erange not defined so far, just set to emin and emax
if eranges[0] eq -1 then eranges = [emin,emax] else begin
  if set_max then begin
    ; get rid of bins that start above emax and set top of highest bin to emax
    q = where (eranges[0,*] lt emax, nkeep)
    if nkeep gt 0 then begin
      eranges = eranges[*,q]      
      eranges[1,nkeep-1] = emax        
    endif else begin
      ; if none left to keep, but we're also setting emin, just set the range to emin,emax, otherwise do nothing (with warning)
      if set_min then eranges = [emin,emax] else $
        message,/cont, 'Not auto-setting erange upper limit because your spex_erange low value is > auto-max value.'
    endelse
  endif
  if set_min then begin
    q = where(eranges[1,*] gt emin, k)
    if k gt 0 then begin
      eranges = eranges[*,q]
      eranges[0,0] = emin
    endif else message,/cont, 'Not auto-setting erange lower limit because your spex_erange high value is < auto-min value.'
  endif
endelse

if ~same_data(eranges, eranges_orig) then self -> set, spex_erange=eranges
end

;------------------------------------------------------------------------------

; _extra can contain spex_units, this_interval, all_func, comb_func,
;     sep_func, function components to plot (e.g. /vth, /bpow)
;
; If this_interval isn't set, uses current value of spex_interval_index (last interval fit)
; otherwise set this_interval to a scalar.
;


pro spex_fit::plot, photons=photons, no_fit=no_fit, _extra=_extra

; call getdata with _extra just to see what units will be used.
;data = self -> getdata(class='spex_fitint', _extra=_extra)
;units = self -> get(/spex_fitint_units)
;fitplot_obj = self -> make_fitplot_obj (/overlay, func_units=units.data_type, photons=photons,  _extra=_extra)

show_fit = keyword_set(no_fit) eq 0
if show_fit then fitplot_obj = self -> make_fitplot_obj (/overlay, photons=photons, _extra=_extra)

if keyword_set(photons) then drm_eff = self -> get_drm_eff()

if show_fit then begin
    erange = self -> get(/spex_erange)
    if erange[0] ne -1 then begin
       addplot_name = 'oplot_xmarkers'
       addplot_arg =  {intervals: erange}
    endif    
endif

self -> plotman, class='spex_fitint', $
    overlay_obj=fitplot_obj,  photons=photons, drm_eff=drm_eff, $
    addplot_name=addplot_name, addplot_arg=addplot_arg, _extra=_extra

;if show_fit then begin
;    p = self -> get_plotman_obj()
;    p->select
;    erange = self -> get(/spex_erange)
;       for ir = 0,n_elements(erange)/2. -1 do begin
;         oplot, [erange[0,ir],erange[0,ir]], crange('y'), psym=0, linestyle=1
;         oplot, [erange[1,ir],erange[1,ir]], crange('y'), psym=0, linestyle=2
;       endfor
;    endif
;endif

end

;------------------------------------------------------------------------------
; if pass in data to plot, must pass in all three - x,y,chisq.  Test is x is passed in
; and if not, get data from spex_summ struct.
; main_window - if set, plot in main plotman_obj.  Otherwise use residual's plotman_obj -
;   if it doesn't exist yet, create it.
; separate_panels - if set and have # intervals > 1, plot each interval in separate plotman panel
; raw - if set, y units are changed.  If ydata passed in, assume it's raw, but if not compute raw
;   resid from the normalized resid that are saved

pro spex_fit::plot_resid, xdata=x, ydata=y, raw=raw, chisq=chisq, interval=interval, $
	xlog=xlog, ylog=ylog, $
	no_unique_label=no_unique_label, $
	main_window=main_window, separate_panels=separate_panels, status=status, _extra=_extra

checkvar, interval, -1
checkvar, xlog, 1
checkvar, ylog, 0
raw = keyword_set(raw)

status = 1 ; init to successful

if keyword_set(main_window) then begin

	plotman_obj = self -> get_plotman_obj(valid=valid_p)

endif else begin

	plotman_obj = self->get(/spex_fitcomp_plotobj_resid)

	if is_class(plotman_obj, 'plotman') then begin
		valid_p = 1
	endif else begin
	    device, get_screen_size=scr
	    wxsize = fix (scr[0] * .4) & wysize = wxsize * .6
	    ; if there is a main spex gui plotman activated, we'll send its widget id as group so that this new
	    ; plotman will be placed next to it if possible instead of on top
	    main_plotman_obj = self -> get_plotman_obj(valid=valid, /nocreate)
	    if valid then begin
	       parent=main_plotman_obj -> get(/plot_base)
	       main_geom = widget_info(parent,/geom)
	       wxsize = main_geom.xsize
	       wysize = wxsize * .6
	    endif
	    plotman_obj = plotman(/multi, wxsize=wxsize,wysize=wysize, group=parent)
	    if is_class(plotman_obj, 'plotman') then begin
	    	valid_p = 1
	    	self -> set, spex_fitcomp_plotobj_resid = plotman_obj
	    endif
	endelse

endelse

; if passed in x,y,chisq use them.  Otherwise get from spex_summ.
if not keyword_set(x) then begin

	summ = self -> check_times()
	valid = self -> valid_summ_params(interval=interval)
	if total(valid) eq 0 then begin
		msg = 'No fit results saved for this (these) intervals. No residuals to plot.'
		if spex_get_nointeractive() then print,msg else xmessage, msg
		status = 0
		return
	endif
	x = summ.spex_summ_energy
	y = summ.spex_summ_resid[*,interval]
	if raw then y = y * summ.spex_summ_ct_error[*,interval]
	chisq = summ.spex_summ_chisq[interval]

endif

chi_label='  Chi-square = ' + trim(chisq,'(f7.2)')
titles = 'Residuals for Interval ' + trim(interval)
titles = raw ? 'Raw ' + titles : 'Normalized ' + titles

nintervals = n_elements(chisq)
do_sep =  (valid_p and keyword_set(separate_panels) and nintervals gt 1) eq 1

if do_sep then begin
	ints = 0 & inte = nintervals-1
endif else begin
	ints = 0 & inte = 0
endelse

for int = ints,inte do begin

	if do_sep then begin
		yuse = y[*,int]
		chiuse = chi_label[int]
		titleuse = titles[int]
		intervaluse = interval[int]
	endif else begin
		yuse = y
		chiuse = chi_label
		titleuse = nintervals gt 1 ? 'Residuals' : titles[0]
		intervaluse = interval
	endelse

	xy_obj = obj_new('xyplot', x,yuse)
	xy_obj -> set, $
		xlog = xlog, $
		ylog = ylog, $
		id=titleuse, $
		data_unit = raw ? 'Raw Residuals (counts/sec)' : 'Normalized Residuals (sigma)', $
		xtitle = 'Energy (keV)', $
		dim1_id = 'Int ' + trim(intervaluse) + chiuse, $
		dim1_enab_sum=1

	

  ; Now override with anything in _extra, added 28-Oct-2014
  xy_obj->set,_extra=_extra
  
	if valid_p then begin

		;status = pobj -> setdefaults (input=[[average(x,1)], [y]], $
		;    plot_type='xyplot', /xlog, xtitle='keV', ytitle='Residual', $
		;       title='Residuals for Interval ' + trim(interval), _extra=_extra)

		status = plotman_obj -> setdefaults (input=xy_obj, plot_type='xyplot', _extra=_extra)

		panel_label = keyword_set(no_unique_label) ? 'Residuals' : titleuse
		plotman_obj -> new_panel, panel_label, /replace


	endif else xy_obj -> plot, _extra=_extra
	
	destroy, xy_obj

endfor

end

;------------------------------------------------------------------------------
; Plot Differential Emission Measure for functions f_multi_therm_xxx

pro spex_fit::plot_dem, current=current, interval=interval, units=units

  if keyword_set(current) then begin
    func = self->get(/fit_function)
    par = self->get(/fit_comp_params)
  endif else begin
    s = self->get(/spex_summ)
    func = s.spex_summ_fit_function
    checkvar, interval, 0
    par = s.spex_summ_params[*,interval]
  endelse
  
  pobj = self->get_plotman_obj(valid=valid_p)
  
  comp = str2arr(func, '+')
  q = where(strpos(comp, 'multi_therm') ne -1, count)
  if count eq 0 then begin
    message,'No multi_therm components with DEM to plot.', /cont
    return
  endif
  
  if count gt 1 then message,'You have >1 multi_therm component. Using first one. Contact Kim Tolbert if you want this changed.', /cont
  
  fmulti = comp[q[0]]
  form = ssw_strsplit(fmulti, 'multi_therm_', /tail)  ; get the form of the DEM
  form = str_replace(form, 'abun_', '')  ; remove abun_ if it's there
  
  pind = fit_function_query(func, comp=fmulti, /param_elem)
  par_use = par[pind]
  
  checkvar, units, 'KEV'
  dem = spex_get_dem(par=par_use, form=form, units=units, temp=temp, leg_text=leg_text, /use_temp_minmax)
  
  xy_obj = obj_new('xyplot', temp, dem)
  id = strupcase(form) + ' DEM'
  tunits = units eq 'K' ? 'K' : 'keV'
  xtitle = 'Temperature (' + tunits + ')'
  ytitle = 'DEM (cm!u-3!n ' + tunits + '!u-1!n)'
  xy_obj->set, id=id, data_unit=ytitle, /xlog, /ylog, label=leg_text
  z=str2arr(leg_text,' ')
  q = where (is_number(z), count)
  desc = count eq 0 ? id : id + ' ' + arr2str(z[q],',')
  xy_obj->plotman, plotman_obj=pobj, xtitle=xtitle, /replace, nodup=0, desc=desc
  
  ;plot, temp, dem, /xlog, /ylog
  
end

;------------------------------------------------------------------------------
; Plot electron spectrum for thick and thin target functions
; Arguments:
;  current - if set, use current values of fit functions and fit_comp_params, otherwise get from spex_summ
; interval - interval number to use if getting values from spex_summ.  Default=0
; comp - function component name to plot edf for. If not set finds first thick or thin func in fit_function
; 
; Plots in spex plotman window
; 

pro spex_fit::plot_edf, current=current, interval=interval, comp=comp, _extra=_extra

  ; get function and parameters for either the current values or from the specified interval in spex_summ values.
  if keyword_set(current) then begin
    func = self->get(/fit_function)
    par = self->get(/fit_comp_params)
    energy = self->getaxis(/ct_energy, /mean)
  endif else begin
    s = self->get(/spex_summ)
    func = s.spex_summ_fit_function
    checkvar, interval, 0
    par = s.spex_summ_params[*,interval]
    times = s.spex_summ_time_interval[*,interval]
    energy = s.spex_summ_energy
  endelse
  
  comp_arr = str2arr(func, '+')
  
  if keyword_set(comp) then begin
    q = where(comp eq comp_arr, count)
  endif else begin
    q = where(strpos(comp_arr, 'thick') ne -1  or  strpos(comp_arr, 'thin') ne -1, count)
  endelse
  if count eq 0 then begin
    text = keyword_set(comp) ? 'No ' + comp + ' component to plot electron distribution for.' : $
      'Your function has no component to plot the electron distribution for.'
      message, /cont, text + 'Aborting.'
      return
  endif
  
  if count gt 1 then $
      message, /cont, 'You have >1 component to plot edf for. Using first one. Use comp="xxx" keyword to select component.'
  fcomp = comp_arr[q[0]]
  if ~check_func_electron(fcomp) then begin
    message, /cont, 'Can not plot electron distribution for ' + fcomp + '. Aborting'
    return
  endif
           
  pobj = self->get_plotman_obj(valid=valid_p)    
  
  pind = fit_function_query(func, comp=fcomp, /param_elem)
  par_use = par[pind]
  
  plot_electron_distribution, energy=energy, par=par_use, func=fcomp, $
    plotman_obj = self -> get_plotman_obj(), $
    interval=interval, times=times, _extra=_extra
  
end



;------------------------------------------------------------------------------


pro spex_fit::quick_summary
strats = self -> get(/compman_strategy)
names = self -> get(/compman_name)

out = ['', $
		'Chi-square = ' + trim(self -> get(/mcurvefit_chi2), '(g12.3)') + '  # Iter = ' + $
			trim(self -> get(/mcurvefit_iter)) + '   ' + self -> get(/mcurvefit_fail_msg), $
	  'Energy range fit = ' + format_intervals(self->get(/spex_erange),format='(f9.2)'), $
		'']

for i=0,n_elements(strats)-1 do begin
	out = [out, strup(names[i])  + '     Parameters     (Sigmas)']
	fc = self -> get(/fit_comp, strat=strats[i])
	for j=0,n_elements(fc.fit_comp_params)-1 do begin
		param = trim(fc.fit_comp_params[j], '(g12.4)')
		sigma = trim(fc.fit_comp_sigmas[j], '(g12.4)')
		out = [out, string(param,sigma, format="(T10,A,T28,'(',A,')' )") ]
	endfor
	out = [out, '']
endfor

prstr,out,/nomore
a=dialog_message(out,/info)
end

;------------------------------------------------------------------------------

function spex_fit::parse_firstint_method

firstint_method =  self -> get(/spex_fit_firstint_method)
fit_or_start = strmid(firstint_method,0,1)
int_use = strmid(firstint_method,1,99)

if (fit_or_start eq 'f' or fit_or_start eq 's') then begin
  if is_number(int_use) then begin
    return, {method: fit_or_start+'interval', fit_or_start:fit_or_start, interval: int_use}
  endif else return, {method: 'invalid', fit_or_start: '', interval: -1}
endif else begin
  return, {method: firstint_method, fit_or_start: '', interval: -1}
endelse

end

;------------------------------------------------------------------------------

pro spex_fit__define

struct = { spex_fit, $
       inherits spex_gen $
       }

end
