;+
; Name: SPEX::GUI
;
; Purpose: OSPEX method to provide main widget to handle everything.
;
; Category: OSPEX
;
; Calling sequence:  o -> gui  (o is an ospex object)
;
; Modifications:
; 20-Jul-2004, Kim.  Added buttons to send setup and fit summary to printer or file as
;     well as the screen.
; 09-Aug-2004, Sandhia, Added an option to set parameters manually.
; 24-Aug-2004, Sandhia, Added buttons for FITS or IDL save (genx) output file for results
; 5-Oct-2004, Kim.  Added buttons to set params from script procedure.
; 18-Nov-2004, Sandhia.  Added buttons to write rate files with following options:
;                       - binned background-subtracted data
;                       - binned data/background data
;                       - background-subtracted data in original time intervals
;                       - data/background in original time intevals
; 9-Feb-2006, Kim.  clearall changed to init_params
; 15-Mar-2006, Kim. added gui_label to put on title bar of spex widget interfaces
; 9-May-2006, Kim.  Disabled option to write fit results in save (genx) file
; 10-Jul-2008, Kim. Change hsi_ui_getfont to get_font (which is in ssw gen)
; 11-May-2010, Kim. removed code to set default size of widget - let plotman do that.
; 20-Aug-2012, Kim. changed Help wording and added help button for html parameter tables
;-
;-------------------------------------------------------------------------------------

;----- object event handler. Event handler can't be a method, so this is just a wrapper
;   that gets the object out of the uvalue and calls the event method

pro spex_event, event
widget_control, event.top, get_uvalue=state
if obj_valid(state.object) then state.object->event,event
return & end


;----- main event handler

pro spex::event,event

if tag_names(event,/struc) eq 'WIDGET_KILL_REQUEST' then begin
    answer = xanswer ('     Do you really want to exit the SPEX GUI?     ', /str, default=1)
    if answer eq 'y' then goto, exit else return
endif

widget_control, event.top, get_uvalue=state

; if id is top, then event is a resize window event, let plotman handle it
if event.id eq event.top then begin
    plotman_widget_event,event
    return
endif

widget_control, event.id, get_uvalue=uvalue

case uvalue of

    'xinput': self -> xinput, group=event.top, gui_label=state.gui_label

    'xbk': self -> xbk, group=event.top, gui_label=state.gui_label

    'xfit': self -> xfit, group=event.top, gui_label=state.gui_label

    'fitresults': self -> xfitview, group=event.top, gui_label=state.gui_label

    'setparams':  self -> setParams
    'setscript_init' : self -> runscript, /init
    'setscript_noinit': self -> runscript

    'scriptfits': self -> writescript, /gui, /restorefit
;    'scriptsav': self -> writescript, /gui, /restorefit, /sav
    'script': self -> writescript, /gui

    'makefits': self -> savefit
;    'makesav': self -> savefit, /sav

    'wrtbinnedbksub': self -> fitswrite, /bksub
    'wrtbksub': self -> fitswrite, /origint, /bksub
    'wrtbinneddata': self -> fitswrite
    'wrtorigdata': self -> fitswrite, /origint

    'restorefit': self -> restorefit

    'show_setup': self-> setupsummary
    'print_setup': self-> setupsummary, /print
    'file_setup': self-> setupsummary, /file_text

    'show_fitsummary': self->fitsummary
    'print_fitsummary': self->fitsummary, /print
    'file_fitsummary': self->fitsummary, /file_text

    'clearsumm': begin
       msg = ['WARNING.  Proceeding will reinitialize all saved fit results ', $
         'in spex_summ structure to null values.']
       answer = xanswer ([msg, '', 'Do you want to continue?'],  /str, default=1, justify_it='|')
       if answer eq 'y' then self->clearsumm
       end

    'init_params': begin
       msg = ['WARNING.  Proceeding will reinitialize all control parameters ', $
         'do default values and all saved fit results ', $
         'in spex_summ structure to null values.']
       answer = xanswer ([msg, '', 'Do you want to continue?'],  /str, default=1, justify_it='|')
       if answer eq 'y' then self->init_params
       end

;   'int_eband': self -> intervals, /eband
;   'int_bkint': self -> intervals, /bk_time
;   'int_fitint': self -> intervals, /fit
;   'int_erange': self -> intervals, /erange
;   'plot_raw_spectrum': self -> plotman, class='spex_data', /pl_energy, spex_units=units
;   'plot_raw_time': self -> plotman, class='spex_data', spex_units=units
;   'plot_raw_spectro': self -> plotman, class='spex_data', /pl_spec, spex_units=units, /log_scale
;   'plot_bk_spectrum': self -> plotman, class='spex_bkint', spex_units=units
;   'plot_bk_time': self -> plotman, class='spex_bk', spex_units=units
;   'plot_bk_sub_time': self -> plotman, class='spex_bksub', spex_units=units
;   'plot_fit_spec': self -> plotman, class='spex_fitint', intervals=[0,99]

    'help_whatsnew': ospex_help, 'ospex_whatsnew.htm'

    'help_guide': ospex_help, 'ospex_explanation.htm'
    
    'help_tables': ospex_help, 'ospex_params_all.htm'

    'help_contact': begin
       widget_control, /hour
        msg = ['The OSPEX package was developed by', $
             'Kim Tolbert of CUA at NASA/GSFC.', $
             ' ', $
             'kim.tolbert@nasa.gov     301-286-3965']
             ok = dialog_message (msg, title='OSPEX Contact Information', /info)
             end

    'help_help': begin
       msg = ['The documentation for OSPEX is in HTML format.  When you click the', $
         'Help buttons, your preferred browser will be activated.', $        
         '', $
         " The browser may start in iconized mode.  If it doesn't", $
         'appear, you may need to find it on the taskbar.']
         ok = dialog_message (msg, title='OSPEX Help Information', /info)
         end

    'exit': goto, exit

    else:
endcase

return

exit:
widget_control, event.top, /destroy
end

;----- spex gui cleanup

pro spex_gui_cleanup, id

widget_control, id, get_uvalue=state
free_var, state, exclude='object'
end

;----- spex gui method - creates main SPEX GUI which containts plotman

pro spex::gui, wxsize=wxsize, wysize=wysize, gui_label=gui_label, $
    colortable=colortable, plotman_obj=plotman_obj, error=error

error = 0

if spex_get_nointeractive(/message) then return

if xregistered ('spex') then begin
    xshow,'spex', /name
    return
endif

if not have_widgets() then begin
 message,'Widgets are not available.  Could not create SPEX GUI.', /cont
 error = 1
 return
endif

checkvar, gui_label, ''

get_font, font, big_font=big_font

widget_control, default_font = font

tlb = widget_base ( /column, $
              title='SPEX Main Window ' + gui_label, $
              uname='SPEX Main Window', $
              xoffset=100, yoffset=100, $
              mbar=mbar, $
              space=1, $
              /tlb_size_events, $
              /tlb_kill, $
              map=0 )

w_file = widget_button (mbar, value='File', /menu, uvalue='file')

temp = widget_button (w_file, $
              value='Select Input ...', $
              uvalue='xinput' , $
              event_pro='spex_event')

temp = widget_button (w_file, $
              value='Select Background ...', $
              uvalue='xbk', $
              event_pro='spex_event')

temp = widget_button (w_file, $
              value='Select Fit Options and Do Fit ...', $
              uvalue='xfit', $
              event_pro='spex_event')

temp = widget_button (w_file, $
              value='Plot Fit Results ...', $
              uvalue='fitresults', $
              event_pro='spex_event')

temp = widget_button (w_file, $
              value='Set parameters manually ...', $
              uvalue='setparams', $
              event_pro='spex_event')

w_set = widget_button (w_file, $
              value='Set parameters from script', /menu, event_pro='spex_event')
temp = widget_button (w_set, value='Initialize All Parameters First', uvalue='setscript_init')
temp = widget_button (w_set, value="Don't Initialize First", uvalue='setscript_noinit')


w_setup = widget_button (w_file,$
              value='Setup Summary', /menu, event_pro='spex_event', /separator)
temp = widget_button (w_setup, value='Display', uvalue='show_setup')
temp = widget_button (w_setup, value='Print', uvalue='print_setup')
temp = widget_button (w_setup, value='Send to File', uvalue='file_setup')

w_fitsummary = widget_button (w_file, $
              value='Fit Results Summary', /menu, event_pro='spex_event')
temp = widget_button (w_fitsummary, value='Display', uvalue='show_fitsummary')
temp = widget_button (w_fitsummary, value='Print', uvalue='print_fitsummary')
temp = widget_button (w_fitsummary, value='Send to File', uvalue='file_fitsummary')

temp = widget_button (w_file, $
              value='Write script', $
              /menu, $
              event_pro='spex_event')
tmp = widget_button (temp, $
                      value='and Fit Results', $
                      uvalue='scriptfits')
;tmp = widget_button (temp, $
;                      value='and Fit Results in IDL Save File', $
;                      uvalue='scriptsav')
tmp = widget_button (temp, $
                       value='Just Script, No Fit Results', $
                       uvalue='script')

temp = widget_button (w_file, $
              value='Save Fit Results (No Script)', $
;              /menu, $
              event_pro='spex_event', $
              uvalue='makefits')

;tmp = widget_button (temp, $
;                       value='in FITS File', $
;                       uvalue='makefits')
;tmp = widget_button (temp, $
;                       value='in IDL Save File', $
;                       uvalue='makesav')

temp = widget_button (w_file, $
              value='Import Fit Results', $
              uvalue='restorefit', $
              event_pro='spex_event')

temp = widget_button (w_file, $
              value='Write FITS Spectrum File', $
              /menu, $
              event_pro='spex_event')
tmp = widget_button (temp, $
                       value='Background-Subtracted', $
                       /menu, $
                       event_pro='spex_event')
tmp1 = widget_button (tmp, $
                       value='Fit Time Intervals', $
                       uvalue='wrtbinnedbksub')
tmp1 = widget_button (tmp, $
                       value='Original Time Intervals', $
                       uvalue='wrtbksub')
tmp = widget_button (temp, $
                       value='Data and Background', $
                       /menu, $
                       event_pro='spex_event')
tmp1 = widget_button (tmp, $
                       value='Fit Time Intervals', $
                       uvalue='wrtbinneddata')
tmp1 = widget_button (tmp, $
                       value='Original Time Intervals', $
                       uvalue='wrtorigdata')

temp = widget_button (w_file, $
              value='Clear Stored Fit Results', $
              uvalue='clearsumm', $
              event_pro='spex_event', $
              /separator)

temp = widget_button (w_file, $
              value='Reset Entire OSPEX Session to Defaults', $
              uvalue='init_params', $
              event_pro='spex_event')

temp = widget_button (mbar, value=' ')

state = {mbar: mbar, $
       w_file: w_file }

widget_control, tlb, set_uvalue=state

;device, get_screen_size=scr
;if not exist(wxsize) then wxsize = fix (scr[0] * .4)
;if not exist(wysize) then wysize = wxsize * 1.1

plotman_obj = obj_new ('plotman', mainbase=tlb, /multi_panel, $
    wxsize=wxsize, wysize=wysize, $
    colortable=colortable, $
    widgets=widgets, error=error)
if error then return

export_but = widget_info(tlb, find_by_uname='export_data')
widget_control, export_but, sensitive=0

temp = widget_button (w_file, value='Reset Widgets (Recover from Problems)', uvalue='reset', event_pro='plotman_widget_event')

temp = widget_button (w_file, value='Exit', uvalue='exit')

w_help = widget_button (mbar, value='Help',  /menu)
tmp = widget_button (w_help, value="What's New", uvalue='help_whatsnew' )
tmp = widget_button (w_help, value='OSPEX Guide', uvalue='help_guide' )
tmp = widget_button (w_help, value='OSPEX Parameter Tables', uvalue='help_tables' )
tmp = widget_button (w_help, value='Contact', uvalue='help_contact')
tmp = widget_button (w_help, value='Help on Help', uvalue='help_help')

geom = widget_info (widgets.w_maindrawbase, /geom)
w_splashdraw = widget_draw ( widgets.w_maindrawbase, xsize=geom.scr_xsize, ysize=geom.scr_ysize)

state = { $
    w_file: w_file, $
    w_splashdraw: w_splashdraw, $
    widgets: widgets, $
    plotman_obj: plotman_obj, $
    object: self, $
    gui_label: gui_label }

widget_control, tlb, set_uvalue=state

widget_control, /realize, tlb

spex_splash

widget_control, tlb, /map, /update, /show

xmanager, 'spex', tlb, event='spex_event', /no_block, cleanup='spex_gui_cleanup'

self -> set, spex_plotman_obj = plotman_obj

end