;+
; Name: spex_conv_struct
; 
; Purpose: Convert the spex summ structure which contains some arrays dimensioned by the number of time intervals to a structure
;  that contains all of the tags not dimensioned by time at the top level, and a sub-structure dimensioned by the number of 
;  time intervals containing all of the items dimensioned by time.  I.e. we packetize the times, so that we have an array of structures
;  where each element contains all of the information for that time interval.  (I should have arranged it this way in the first place!)
;  This also operates in the reverse direction - if a packetized structure is passed in, it is converted to the original style structure.
;  
;  Calling sequence:  new_struct = spex_conv_struct(struct)
;  
;  Calling argument:  
;    struct - a spex_summ structure, either in original or packetized format
;    
;  Output: 
;    new_struct - Either the packetized spex summ structure, or the origianl format structure, depending on format of input struct
;    
;  Example:
;    If struct looks like this (partial listing):
;      SPEX_SUMM_FIT_FUNCTION   STRING    'vth+drm_mod2+albedo+pileup_mod'
;      SPEX_SUMM_AREA           FLOAT           39.5900
;      SPEX_SUMM_ENERGY         FLOAT     Array[2, 305]
;      SPEX_SUMM_TIME_INTERVAL  DOUBLE    Array[2, 43]
;      SPEX_SUMM_FILTER         INT       Array[43]
;      ...
;    new_struct = spex_conv_struct(struct)
;    will return this:
;      SPEX_SUMM_FIT_FUNCTION   STRING    'vth+drm_mod2+albedo+pileup_mod'
;      SPEX_SUMM_AREA           FLOAT           39.5900
;      SPEX_SUMM_ENERGY         FLOAT     Array[2, 305]
;      SPEX_SUMM_INTERVALS      STRUCT    -> <Anonymous> Array[43]
;      and new_struct.spex_summ_intervals contains
;        SPEX_SUMM_TIME_INTERVAL  DOUBLE    Array[2]
;        SPEX_SUMM_FILTER         INT              0
;        ...
;        
; Written: 2-Jan-2014, Kim Tolbert
; Modifications:
; 
;;-

function spex_conv_struct, struct

s = struct

; If input structure contains the spex_summ_intervals tag, then it is in packetized form.
if tag_exist(s, 'spex_summ_intervals') then begin
  
  ; Unpacketize the structure
  tnames = tag_names(s.spex_summ_intervals)
  for i=0,n_tags(s.spex_summ_intervals)-1 do begin
    s = add_tag(s, s.spex_summ_intervals.(i), tnames[i])
  endfor
  s = rem_tag(s, 'spex_summ_intervals')
  
endif else begin
  
  ; Packetize the time arrays of the structure
  ntime = n_elements(s.spex_summ_filter)
  tnames = tag_names(s)
  spex_summ_intervals = replicate({dummy: 1}, ntime)
  
  ; For each tag, if it has a time dimension, then move those items to new struct calle spex_summ_intervals repicated ntime times,
  ; and remove from origianl structure
  for i=0,n_tags(s)-1 do begin
    dim = size(s.(i), /dim)
    if last_item(dim) eq ntime then begin     
      spex_summ_intervals = add_tag(spex_summ_intervals, s.(i), tnames[i])
      tags_to_remove = append_arr(tags_to_remove, tnames[i])
    endif    
  endfor
  spex_summ_intervals = rem_tag(spex_summ_intervals, 'dummy')
  s = rem_tag(s, tags_to_remove)
  s = add_tag(s, spex_summ_intervals, 'spex_summ_intervals')
endelse

return, s
end


