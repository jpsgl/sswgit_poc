;+
; NAME:
;	F_MULTI_THERM_ABUN_POW_DEFAULTS
;
; PURPOSE: Function to return default values for
;   parameters, minimum and maximum range, and free parameter mask when
;   fitting to f_multi_therm_abun_pow function.
;
; CALLING SEQUENCE: defaults = f_multi_therm_abun_pow_defaults()
;
; INPUTS:
;	None
; OUTPUTS:
;	Structure containing default values
;
; MODIFICATION HISTORY:
; Kim Tolbert, February 2004
; 7-Apr-2006, Kim.  Added fit_comp_spectrum,fit_comp_model
; 19-Apr-2006, Kim.  added defaults for 5th param (rel abun)
; 13-Jul-2006, Kim.  Default for a[2] maximum changed from 1e4 to 1e1
; 6-Sep-2006, Kim.   Default for a[2] maximum changed from 1e4 to 8.
; 12-May-2008, Kim.  Added defaults for 5,6,7th parameters (separate abundances)
;
;-
;------------------------------------------------------------------------------

FUNCTION F_MULTI_THERM_ABUN_POW_DEFAULTS

defaults = { $
  fit_comp_params:      [ 0.005, 0.5, 4e0, 1e0,    1.,   1.,   1.,  1.], $
  fit_comp_minima:      [ 1e-10, 0.5, 1e0,  1e-2, .01,  .01,  .01, .01], $
  fit_comp_maxima:      [ 1e2,  4.0,  8.,  1e2,    10.,  10.,  10., 10.], $
  fit_comp_free_mask:   [1b,    0b,   1b,   1b,    1b,   1b,   1b,  1b], $

  fit_comp_spectrum:         'full', $
  fit_comp_model:            'chianti', $

  fc_spectrum_options:		['full','continuum','line'], $
  fc_model_options:			['chianti', 'mewe'] $
}

RETURN, defaults

END
