;
; Modifications:
; 4-Oct-2005, Kim.  Changed to allow > in more_doc to point to a different param's little html file.
;

;-----

pro create_more_doc, name, text

list = ['<HTML>', $
	'<HEAD>', $
	'<link rel="stylesheet" type="text/css" href="ospex_help.css">', $
	'</HEAD>', $
	'<BODY>', $
	'<h3 align="center">' + name + '</h3>', $
	text, $
	'</BODY>', $
	'</HTML>']


outfile = name + '_doc.htm'
oldfile = hsi_loc_file (outfile, path='$OSPEX_DOC', count=count, /no_dialog)
;oldfile = findfile(outfile, count=count)
if count gt 0 then begin
	oldlist = rd_ascii(oldfile)
	if same_data( oldlist, list) then return
endif

;stop
print, 'Writing new ' + outfile
prstr, list, file=outfile
end

;-----

pro ospex_do_htm, str, outfile=outfile, title=title
template_file = 'ospex_params_template.htm'
filename = find_files (template_file, get_logenv('SSW_OSPEX'))
if filename[0] eq '' then message,'No html template file found.  Aborting.'
list = rd_ascii (filename)
list = str_replace(list, 'INSERT TITLE', title)
list = str_replace(list, 'INSERT DATE', anytim(!stime, /vms, /date))

for i = 0, n_elements(str)-1 do begin

;	if str[i].more_doc eq '' then name = str[i].name else begin
;		name = '<a href=JavaScript:OpenNew("' + str[i].name + '_doc.htm")>' + str[i].name + '</a>'
;		;prstr,str[i].more_doc,file=str[i].name + '_doc.htm'
;		create_more_doc, str[i].name, str[i].more_doc
;	endelse

	if str[i].more_doc eq '' then name = str[i].name else begin
		if strmid(str[i].more_doc,0,1) eq '>' then begin
			pname = strmid(str[i].more_doc,1,99)
		endif else begin
			pname = str[i].name
			create_more_doc, str[i].name, str[i].more_doc
		endelse
		name = '<a href=JavaScript:OpenNew("' + pname + '_doc.htm")>' + str[i].name + '</a>'
		;prstr,str[i].more_doc,file=str[i].name + '_doc.htm'
	endelse

	for j = 0,n_tags(str)-1 do if str[i].(j) eq '' then str[i].(j) = '--'

  	row = ['<tr>', $
    '  <td>' + name, $
    '  <td>' + str[i].control_or_info, $
    '  <td>' + str[i].purpose, $
    '  <td>' + str[i].default, $
    '  <td>' + str[i].units, $
    '  <td>' + str[i].range, $
    '  <td>' + str[i].type, $
    '  <td>' + str[i].level, $
    '  <td>' + str[i].class, $
  '</tr>']

	table = append_arr(table, row)
endfor

list = strarrinsert (list, table, 'INSERT TABLE', /deldelim)

;oldfile = findfile(outfile, count=count)
oldfile = hsi_loc_file (outfile, path='$HSI_DOC', count=count, /no_dialog)
if count eq 1 then begin
	oldlist = rd_ascii(oldfile)
	qold = where (strpos(oldlist, 'Last updated') ne -1)
	qnew = where (strpos(list, 'Last updated') ne -1)
	if same_data(list[0:qnew[0]], oldlist[0:qold[0]]) then return
endif

print,'Writing new ' + outfile
prstr, list, file=outfile
end

;-----

pro ospex_params_write_htm

ospex_params, d

q = sort(d.name)
str = d[q]
ospex_do_htm, str, outfile='ospex_params_all.htm', $
	title='All OSPEX Parameters'

w = where (d.level eq 'Standard', count)
if count gt 0 then begin
	str = d[w]
	str = str(sort(str.name))
	ospex_do_htm, str, outfile='ospex_params_standard.htm', $
		title = 'OSPEX Standard Control Parameters'
endif

w = where (d.control_or_info eq 'Info', count)
if count gt 0 then begin
	str = d[w]
	str = str(sort(str.name))
	ospex_do_htm, str, outfile='ospex_params_info.htm', $
		title = 'OSPEX Info Parameters'
endif

w = where (d.control_or_info eq 'Admin', count)
if count gt 0 then begin
	str = d[w]
	str = str(sort(str.name))
	ospex_do_htm, str, outfile='ospex_params_admin.htm', $
		title = 'OSPEX Admin Parameters'
endif

end
