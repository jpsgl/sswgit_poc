;+
; NAME:
;	f_pileup_mod_defaults
;
; PURPOSE: Function to return default values for
;   parameters, minimum and maximum range, and free parameter mask when
;   fitting with pileup_mod function.  pileup_mod is a pseudo function - the function
;   value is always 0., but the params are used to add pileup effects to the model.
;
; CALLING SEQUENCE: defaults = f_pileup_mod_defaults()
;
; INPUTS:
;	None
; OUTPUTS:
;	Structure containing default values
;
; MODIFICATION HISTORY:
; Kim Tolbert, 6-Oct-2006
; 11-Oct-2006, Kim.  Added 3rd parameter (smoothing)
; 15-Oct-2006, Kim.  Added 4th parameter (multiplicative factor)
;
;-
;------------------------------------------------------------------------------

FUNCTION f_pileup_mod_defaults

defaults = { $
  fit_comp_params:           [1., .8, 0., 1.], $  ; prob_gain, pilerfrac, smoothing parameter, mult factor
  fit_comp_minima:           [.1, .5, 0., 0.], $
  fit_comp_maxima:           [2., .95, 1., 1.e2], $
  fit_comp_free_mask:        [1b,  0b, 0b, 0b] $

}

RETURN, defaults

END