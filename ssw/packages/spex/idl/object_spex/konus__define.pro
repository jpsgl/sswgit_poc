;+
; Project     : RHESSI
;
; Name        : KONUS__DEFINE
;
; Purpose     : Define a KONUS data object for finding and copying konus-wind data files,  and reading
;  konus flare catalog.
;
;
; Category    : Synoptic Objects
;
; Syntax      : IDL> k=obj_new('konus')
;
; History     : Written 27-Jul-2016, Kim Tolbert, modified from fermi_gbm__define
;
; Modifications:
; 04-Aug-2016, Kim. Modified to read the KONUS spectrum time file instead of the flare list.
; 05-Aug-2016, Kim. Subtract 10 seconds from start time in search since filenames have times (times at Konus)
;   that are 4-6 seconds earlier than the times of the data (times at Earth)
; 08-Aug-2016, Kim. Changed read method since Konus spectrum times file now has hh:mm:ss.msec instead of sec of day
; 06-Sep-2016, Kim. Use /unaligned on call to str2cols, and need konus team to put some (any) character in for missing Class.
; 15-Nov-2016, Kim. In search method, added searching for background file names in pha file headers and adding them to
;   list of files to copy (assume they're in same dir as pha files, problem is date in name might be different). 
;
;-
;----------------------------------------------------------------
;
function konus::init,_ref_extra=extra

  if ~self->synop_spex::init() then return,0
  
  rhost = 'www.ioffe.ru'
  
  self->setprop,rhost=rhost,ext='pha|rmf|arf',org='day', $
    topdir='/LEA/kwsun/data',/full, delim='/'
  return,1
  
end
;-----------------------------------------------------------------

;-- search method

function konus::search,tstart,tend,count=count,type=type,_ref_extra=extra

  type=''
  
  ; konus files are named by the date and trigger time, which is the time at konus.
  ; The times in the file are the times at Earth, usually 4-6 seconds later. So the
  ; files we're looking for have names that are 4-6 seconds too early, so we'll
  ; modify tstart by subtracting 10 seconds.
  tstart = anytim(anytim(tstart)-10.d, /vms)
  files=self->site::search(tstart,tend,_extra=extra,count=count)
  if count gt 0 then begin
    ; Background files are in the same directory but may not have the same time in their filename,
    ; so have to read 'SPECTRUM' extension (ext=2) of the PHA files to see what background files
    ; are specified, and add them to list of files to copy.
    qpha = where(ssw_strsplit(files, '.', /tail) eq 'pha', npha)
    if npha gt 0 then begin
      for ii = 0,npha-1 do begin
        phafile = files[qpha[ii]]
        bkfile = sock_fits_head(phafile, 'BACKFILE', extension='SPECTRUM')
        path = str_replace(file_dirname(phafile, /mark), '\', '/') ; on Windows, dirname returns wrong slash
        if bkfile ne '' and trim(bkfile) ne 'none' then files = [files, path+bkfile]
      endfor
      count = n_elements(files)
    endif
    type=replicate('hxr/lightcurves',count)
  endif else begin
    files=''
    if count eq 0 then message,'No files found.',/cont
  endelse
  
  return,files
end

;----------------------------------------------------------------

function konus::parse_time,file,_ref_extra=extra

  fil = file_break(file)
  date = file2time(fil, out_style='sec')
  pos = stregex(fil, 'T.*_', length=len)
  sec = long( strmid(fil, pos+1, len-2) )
  sec = reform(sec[0,*])
  return, anytim(date + sec, /tai)
  
end

;----------------------------------------------------------------

; Read KONUS solar flare list. If stored in common, don't read again, unless force is set
function konus::read_flare_cat, times=times, ids=ids, $
  n_header=n_header, n_trailer=n_trailer, force=force
  
  checkvar, force, 0
  
  common konus_flare_cat,   kflare_list, kflare_times, kflare_ids
  
  if ~exist(kflare_list) or force then begin
  
    ; change to read spectrum file instead of flare list, only has times
    ; that there are spectrum files for, and includes end time (previously guessed 20 min dur)
;    tvalid = anytim(['1-jan-2012', '1-jan-2013'])
    
    ;    sock_copy, 'http://www.ioffe.ru/LEA/Solar/KonusWIND_SolarFlares.txt', local_file=local_file
    sock_copy, 'http://www.ioffe.ru/LEA/kwsun/KW_spectrum_times.txt', local_file=local_file
    
    if file_exist(local_file) then begin
      kflare_list = rd_ascii(local_file)
      
      ; convert tabs in kflare_list to 4 spaces, since tabs are not recognized for widget display of
      ; flare list. Do this by converting
      ; to bytes, finding tabs (which = 9b), replacing all 9b to 36b (which is a '$' and isn't used
      ; anywhere else in the text file), converting back to string, and replacing all $ with 4 spaces.
      zbyte = byte(kflare_list)
      qtab = where(zbyte eq 9b, ctab)
      if ctab gt 0 then begin
        zbyte[qtab] = 36b
        new = string(zbyte)
        kflare_list = str_replace(new, '$', '    ')
        endif
        
      cols = str2cols(kflare_list[2:*], ' ', /unaligned)
      stimes = reform(anytim(cols[1,*] + ' ' + cols[6,*]))
      etimes = reform(anytim(cols[1,*] + ' ' + cols[7,*]))
      ; check if start or end time is less than trigger time - means it's on the next day, so add a day
      trigtimes = reform(anytim(cols[1,*] + ' ' + cols[2,*]))
      qnext = where(stimes lt trigtimes, knext)
      if knext gt 0 then stimes[qnext] += 86400.d
      qnext = where(etimes lt trigtimes, knext)
      if knext gt 0 then etimes[qnext] += 86400.d
      
      kflare_times = transpose([[stimes],[etimes]])
      kflare_ids = trim(reform(cols[0,*]))
;      q = where(kflare_times ge tvalid[0] and kflare_times lt tvalid[1], count)
;      if count gt 0 then begin
;        kflare_list = [kflare_list[0:1], kflare_list[q+2]]
;        kflare_times = kflare_times[q]
;        kflare_ids = kflare_ids[q]
;      endif else kflare_list = ''
      
    endif else kflare_list = ''
  endif
  
  times= kflare_times
  ids = kflare_ids
  n_header = 2
  n_trailer = 0
  return, kflare_list
  
end

;----------------------------------------------------------------

function konus::select, id=id, index=index, string=string

  list = self->read_flare_cat(times=times, ids=ids, n_header=n_header, n_trailer=n_trailer)
  
  if list[0] eq '' then return, -1
  
  if os_family() eq 'Windows' then font = 'fixedsys' else font = 'fixed'
  nlist = n_elements(list) - n_header - n_trailer
  
  ind = xsel_list (list, $
    lfont=font, title='Konus Flare Selection', $
    subtitle='Select a flare from the list below', $
    /no_remove, /no_sort, /index, ysize=n_elements(list) < 20, $
    initial=list[n_header], status=status)
    
  if status and ind ge n_header and (ind-n_header) le (nlist-1) then begin
    if keyword_set(id) then return, ids[ind - n_header]
    if keyword_set(index) then return, ind - n_header
    if keyword_set(string) then return, list[ind]
    return, list[ind]
  endif
  
end

;----------------------------------------------------------------


function konus::getflare, flare
  list = self->read_flare_cat(times=times, ids=ids, n_header=n_header, n_trailer=n_trailer)
  q = where(ids eq trim(flare), nq)
  if nq eq 0 then return, -1
  return, {id: ids[q], utstart: times[0,q], utend: times[1,q], desc: list[q+n_header]}
end

;----------------------------------------------------------------

function konus::whichflare, time, err_msg=err_msg
  err_msg = ''
  list = self->read_flare_cat(times=times, ids=ids, n_header=n_header, n_trailer=n_trailer)
  q = where(time[0] lt times[1,*] and time[1] gt times[0,*], nq)
  if nq eq 0 then return, -1
  
  q = q[0] ; take first one
  return, {id: ids[q], utstart: times[0,q], utend: times[1,q], desc: list[q+n_header]}
end

;----------------------------------------------------------------

pro konus__define
  void={konus, inherits synop_spex}
  return & end
  
