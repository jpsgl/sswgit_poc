;+
; PROJECT:
;   SPEX
; NAME:
;   XSM_VALID
;
; PURPOSE:
;   This procedures edits XSM data fits files and rmf files
;   and leaves only the designated energy and time ranges
;
; CATEGORY:
;   SPEX, XSM
;
; CALLING SEQUENCE:
;   xsm_valid, energy_range=energy, time_range=time_range
;
; CALLS:
;   none
;
; INPUTS:
;       none explicit, only through commons;
;
; OPTIONAL INPUTS:
;   none
;
; OUTPUTS:
;       none explicit, only through commons;
;
; OPTIONAL OUTPUTS:
;   none
;
; KEYWORDS:
;   ENERGY_RANGE - given as 2 element vector with energies in keV, >0 < 20, in keV
;   TIME_RANGE - 2 times recognizable by anytim()
;   RMF_DIR - optional, directory of rmf file if not in same directory as data fits file.
; COMMON BLOCKS:
;   none
;
; SIDE EFFECTS:
;  data fits file and rmf files are modified to reflect the new ranges. If you
;   want to preserve the current versions of the fits and rmf files, you must make copies
;   of them.  The .arf ancillary response file is unchanged.
;
; RESTRICTIONS:
;   none
;
; PROCEDURE:
;   none
;
; MODIFICATION HISTORY:
;   Version 1, richard.schwartz@gsfc.nasa.gov
;   23-jun-2005
;-



pro xsm_valid, inputfile, $
    time_range=time_range, $
    energy_range = energy_range, $
    rmf_dir = rmf_dir


fitsdatafile = loc_file( inputfile )
break_file, fitsdatafile, disk, dir
default, rmf_dir, disk + dir

data     = mrdfits( inputfile, 1, hdr)
respfile = fxpar( hdr, 'respfile')
respfile = loc_file( path=rmf_dir, respfile)


ebounds = mrdfits( respfile, 2, rmfhdr2)
default, energy_range, [0.1, 20.0]
mmchan = [0, n_elements(ebounds)-1]
channelrange = value_locate( ebounds.e_max, energy_range) $
    > mmchan[0] <mmchan[1]

ut = anytim( data.t_utc)
default, time_range, minmax(ut)


mmbin = [0, n_elements(ut) -1]
binrange = value_locate(  ut, anytim( time_range) ) $
    > mmbin[0] < mmbin[1]

roi = channelrange[0]+lindgen(channelrange[1]-channelrange[0]+1)
troi = binrange[0] + lindgen( binrange[1]-binrange[0]+1)
ntbin = n_elements(troi)
nchan = n_elements(roi)


data = data[troi]
spectrum = data. spectrum[roi]
data = rep_tag_value( data, spectrum,'SPECTRUM')
file_delete, inputfile
mwrfits, data, inputfile, hdr




rmf1 = mrdfits( respfile, 1, rmfhdr1)
oldchan = n_elements( rmf1[0].matrix)
matrix = rmf1.matrix[roi]
rmf1 = rep_tag_value( rmf1, matrix,'MATRIX')
rmf1 = rep_tag_value( rmf1, nchan, 'N_CHAN')
;Ebounds


ebounds = ebounds[roi]

fxaddpar,rmfhdr1, 'DETCHANS', nchan

fxaddpar,rmfhdr2, 'DETCHANS', nchan
file_delete, respfile
mwrfits, rmf1, respfile, rmfhdr1
mwrfits, ebounds, respfile, rmfhdr2

end