;+
; Modifications:
; 10-Oct-2004, Kim.  In process, abort if data isn't a structure
; 11-Apr-2005, Kim.  In intervals_pre_hook, use plot_time,/data instead of plot, class=spex_data, and
;   added /show_filter on time plot
; 09-Aug-2005, Kim.  In get, found needs to be param name, not 1
; 29-Sep-2005, Kim.  In set, /need_update was causing it to re-read the raw data file.  Use
;   /this_class_only to limit to this class.  (not sure why we need to set need_update, but
;   I think it has something to do with framework not finding this_xxx param)
; 13-Feb-2006, Kim.  In plot_setup, call get_plot_title to get title
; 15-Dec-2008, Kim. In intervals_pre_hook, make sure energy intervals are floats
; 26-Jul-2013, Kim. In plot_setup, add show_err option
;---------------------------------------------------------------------------
;-

;---------------------------------------------------------------------------

function  spex_bkint::init, source = source, _extra = _extra

if not keyword_set( source ) then source = obj_new( 'spex_data', _extra=_extra )

control=spex_bkint_control()

return, self->framework::init( source = source, $
                                            control = control, $
                                            info = {spex_bkint_info}, $
                                            _extra = _extra )

end

;--------------------------------------------------------------------------

pro spex_bkint::set, $
	spex_bk_sep=spex_bk_sep, $
	spex_bk_eband=spex_bk_eband, $
	spex_bk_time_interval=spex_bk_time_interval, $
	this_band=this_band, this_time=this_time, $
	done=done, not_found=not_found, $
	_extra=_extra

if keyword_set(spex_bk_eband) then new_bk_eband = spex_bk_eband

if keyword_set(spex_bk_time_interval) then begin
	new_time = size(spex_bk_time_interval,/tname) eq 'POINTER' ? $
		spex_bk_time_interval : anytim(spex_bk_time_interval)
	self -> set, /need_update, /this_class_only
endif

if exist(spex_bk_sep) then begin
	if spex_bk_sep then begin
		checkvar, bk_eband, new_bk_eband, self->get(/spex_bk_eband)
		tmp = self->getdata(class='spex_data') ; won't have energy bands if haven't read data
		eband = self->get(/spex_eband)
		if bk_eband[0] eq -1 and eband[0] ne -1 then new_bk_eband = eband
	endif else begin
		; maybe not necessary to change time back to non-pointer?
		;checkvar, time, new_time, self -> get(/spex_bk_time_interval)
		;if ptr_valid(time) then new_time = *time[0]
	endelse
endif

if exist(this_band) and exist(this_time) then begin
	bk_sep = self->get(/spex_bk_sep)
	if bk_sep then begin
		checkvar, bk_eband, new_bk_eband, self->get(/spex_bk_eband)
		nband = bk_eband[0] eq -1 ? 0 : n_elements(bk_eband)/2
	endif else nband=1
	if this_band gt nband-1 then begin
		message,/cont, 'Invalid value for this_band = '+trim(this_band)+$
			(bk_sep ? '.  Only ' + trim(nband) + ' bk ebands defined.  Aborting.' : $
				'.  Separate bk bands not enabled.')
		done=1
		return
	endif
	checkvar, new_time, self -> get(/spex_bk_time_interval)
	if size(new_time,/tname) eq 'POINTER' then begin
		ntime = n_elements(new_time)
		while n_elements(new_time) lt nband do new_time = append_arr(new_time, ptr_new(*new_time[ntime-1]))
	endif else begin
		sav_time = new_time
		new_time = nband eq 1 ? ptr_new() : ptrarr(nband)
		for i = 0,nband-1 do new_time[i] = ptr_new(sav_time)
	endelse
	*new_time[this_band] = anytim(this_time)
	if n_elements(new_time) eq 1 then new_time=new_time[0]
	self -> set, /need_update, /this_class_only
endif

; if user passed in a pointer array, then it will be a new set of pointers, so free up the old ones.
if exist(new_time) then begin
	tmp = self -> get(/spex_bk_time_interval)
	if size(/tname,tmp) eq 'POINTER' and size(/tname,new_time) ne 'POINTER' then ptr_free, tmp
endif

self->framework::set,spex_bk_time_interval=new_time, $
	spex_bk_sep=spex_bk_sep, $
	spex_bk_eband=new_bk_eband, $
	_extra=_extra, done=done, not_found=not_found

end

;--------------------------------------------------------------------------

function spex_bkint::get, this_band=this_band, this_time=this_time, $
	_extra=_extra, found=found, not_found=not_found

if exist(this_band) and keyword_set(this_time) then begin

	bk_time = self -> get(/spex_bk_time_interval)
	bk_sep = self -> get(/spex_bk_sep)

	found=['this_band', 'this_time']
	ret = bk_time

	bk_eband = self -> get(/spex_bk_eband)
	neband = bk_sep ? n_elements(bk_eband)/2 : 1
	if this_band gt neband-1 then ret = -1 else begin

		t_ptr = size(/tname, bk_time) eq 'POINTER'
		if t_ptr then begin
			ntime = n_elements(bk_time)
			while n_elements(bk_time) lt neband do bk_time = append_arr(bk_time, bk_time[ntime-1])
			ret = *bk_time[this_band]
		endif else begin
			if size(/n_dim, bk_time) ne 3 then ret = bk_time else ret = bk_time[*,*,this_band]
		endelse

	endelse

	return, ret
endif

ret = self -> framework::get(_extra=_extra, found=found, not_found=not_found)
return, ret
end

;--------------------------------------------------------------------------

function spex_bkint::getdata, _extra=_extra

@spex_insert_catch

return, self -> framework::getdata(_extra=_extra)

end

;--------------------------------------------------------------------------

; data product created is spex_bk_index - indices of raw data array that
; are in the bk time intervals.  Also sets spex_have_bk = 0/1 to indicate
; whether we actually have defines some background intervals.
;
; explictly name spex_units keyword, so won't get passed to getdata in _extra, so
; we'll get raw units
pro spex_bkint::process, spex_units=spex_units, _extra = _extra

;print,'in spex_bkint::process'

; If bk_sep then for each bk_eband, save bk_order (order of background fit) and selected (pointer
; array of indices into data to use for background.
; If not bk_sep, then use 0th bk_time and bk_order
bk_sep = self -> get(/spex_bk_sep)
bk_eband = self -> get(/spex_bk_eband)
neband = bk_sep ? n_elements(bk_eband)/2 : 1

bk_index = ptrarr(neband)
have_bk = 0  ; will be set to 1 if at least one band has valid background time interval(s)

data_obj = self -> get(/obj, class_name='spex_data')
data = data_obj -> getdata(/original_units, _extra=_extra)

if is_struct(data) then begin

	;filetime = self -> get(/spex_file_time)
	;datatime = self -> get(/spex_bk_time_interval)
	;if (datatime[0] ne -1) and $
	;	(min(datatime)-filetime[0] lt -1.e-3 or $
	;	max(datatime)-filetime[1] gt 1.e3) then begin
	;	message, 'Background time interval(s) outside range of file.  Resetting to no background.', /cont
	;	self -> set, spex_bk_time_interval = -1
	;endif

	units_str = data_obj -> getunits(/orig)

	times = data_obj -> get(/spex_ut_edges)
	ntimes = n_elements(times[0,*])

	;bk_index = self -> get(/spex_bk_index)
	;free_var, bk_index


	for ib = 0,neband-1 do begin
		bk_time = self -> get(this_band=ib, /this_time)
		if bk_time[0] eq -1 then sel = -1 else begin
			se_index = self -> edges2index(intervals=bk_time, /do_time_bin)
			if se_index[0] eq -1 then sel = -1 else begin
				ind = intarr(ntimes)
				for i=0,n_elements(se_index[0,*])-1 do ind[se_index[0,i]:se_index[1,i]] = 1
				sel = where (ind eq 1)
			endelse
		endelse
		if sel[0] ne -1 then have_bk = 1
		bk_index[ib] = ptr_new(sel)
	endfor

endif else message, 'No data.'

self -> set, spex_have_bk=have_bk
self -> setdata, bk_index

end

;--------------------------------------------------------------------------

function spex_bkint::getunits, orig=orig
return, (self->get(/obj,class='spex_data'))->getunits(orig=orig)
end

;--------------------------------------------------------------------------

pro spex_bkint::setunits, units, orig=orig

if keyword_set(orig) then self -> set, spex_bkint_origunits=units else $
	self -> set, spex_bkint_units=units

end

;--------------------------------------------------------------------------
; Plot Method plots spectrum in background time intervals that user has defined.
; Default is to plot each interval separately.
;
function spex_bkint::plot_setup, $
	plot_params=plot_params, $
	this_band=this_band, $
	show_err=show_err, $
	_extra=_extra

error_catch = 0
if spex_get_debug() eq 0 then catch, error_catch
if error_catch ne 0 then begin
	catch, /cancel
	message, !error_state.msg, /cont
	;message,'Error making plot.', /cont
	return, 0
endif

checkvar, this_band, 0

bk_index = self -> getdata(_extra=_extra)
have_bk = self -> get(/spex_have_bk)
if not have_bk then message, 'No background time intervals set.'

bk_time = self -> get(this_band=this_band, /this_time)
if bk_time[0] eq -1 then message, 'No background time interval set for this energy band.'

data = self -> getdata(class='spex_data', _extra=_extra)
z = self -> bin_data (data=data, intervals=bk_time, eresult=z_err, /do_time, newedges=newedges)
if z[0] eq -1 then return,0

label = 'Detectors: ' + self -> get(/spex_detectors)

title = self -> get_plot_title(photons=photons)

units_str = self -> getunits()

xdata = self -> get(/spex_ct_edges)
ydata = z
plot_type='xyplot'
bk_sep = self->get(/spex_bk_sep)
if bk_sep then begin
	band = (self -> get (/spex_bk_eband))[*,this_band]
	title2 = ' for ' + format_intervals(band, format='(i9)') + ' keV'
endif else title2 = ''
title = title + ' vs Energy in Bk Time Bins' + title2
xtitle = 'Energy (keV)'
dim1_id = format_intervals(bk_time, /ut)
dim1_vals = anytim(bk_time, /vms)
dim1_sum = 0
dim1_unit = ''

plot_params = { $
	plot_type: plot_type, $
	xdata: xdata, $
	ydata: ydata, $
	id: title, $
	label: label, $
	data_unit: units_str.data, $
	dim1_id: dim1_id, $
	dim1_vals: dim1_vals, $
	dim1_sum: dim1_sum, $
	dim1_unit: dim1_unit, $
	dim1_enab_sum: 1, $
	weighted_sum: 1, $
	xlog: 1, $
	ylog: 1, $
	xtitle: xtitle $
	}
	
if keyword_set(show_err) then plot_params = add_tag (plot_params, z_err, 'edata')

return, 1
end

;------------------------------------------------------------------------------

;pro spex_bkint::plot, _extra=_extra
;
;status = self -> plot_setup (plot_params=plot_params, _extra=_extra)
;
;if status then self -> do_plot, plot_params=plot_params, _extra=_extra
;
;end

;------------------------------------------------------------------------------
; this_band is the index (starting with 0) of the spex_bk_eband that we're setting a time interval
; for.
pro spex_bkint::intervals_pre_hook, $
	full_options=full_options, $
	intervals=intervals, $
	valid_range=valid_range, $
	title=title, $
	type=type, $
	energy=energy, $
	this_band=this_band, $
	abort=abort, $
	_extra=_extra

; make sure data has been retrieved for current settings.  May be a better way?  !!!!
data = self -> getdata(class_name='spex_data')
if not is_struct(data) then abort = 1

if abort then return

;Assume we're doing bk time bins unless energy keyword was set.

if keyword_set(energy) then begin

	intervals = self -> get(/spex_bk_eband)
	if intervals[0] ne -1 then intervals = float(intervals) ; if integer, gets interpreted as time later
	title = 'Select Energy Bands for Background'
	type='Background'
	if not keyword_set(full_options) then $
		if not self->valid_plot(/xyplot) then self -> plotman, class_name='spex_data', /pl_energy
	valid_range = minmax (self->get(/spex_ct_edges))

endif else begin

	timeaxis = self -> getaxis(/ut)
	if n_elements(timeaxis) le 1 then begin
		message,/cont,'Error - Only one time interval.  Can not define background time intervals. Aborting.'
		abort = 1
		return
	endif
	title='Select Time Intervals for Background'
	checkvar, this_band, 0
	bk_sep = self -> get(/spex_bk_sep)
	if bk_sep then begin
		bk_ebands = self -> get(/spex_bk_eband)
		interval = bk_ebands[*,this_band]
		title = [title, ' for Energy Band ' + format_intervals(interval) + ' keV']
	endif
	intervals = self -> get(this_band=this_band, /this_time)
	type='Background'
	if not keyword_set(full_options) then $
		if not self->valid_plot(/utplot) then $ ;self -> plotman, class_name='spex_data'
			self -> plot_time, /data, interval=interval, /show_filter
	valid_range = minmax (self->get(/spex_ut_edges))

endelse

end

;------------------------------------------------------------------------------
; this_band is the index (starting with 0) of the spex_bk_eband that we're setting a time interval
; for.
pro spex_bkint::intervals_post_hook, energy=energy, this_band=this_band, bins

if keyword_set(energy) then self -> set, spex_bk_eband=bins else begin
	if exist(this_band) then self -> set, this_band=this_band, this_time=bins else $
		self -> set, spex_bk_time_interval=bins
endelse

end

;---------------------------------------------------------------------------

pro spex_bkint__define

dummy = {spex_bkint, $
         inherits spex_gen}

end
