;+
; 5-Sep-2006, Kim.  Added spex_bk_ratio and spex_bk_sm_width
;-
;

PRO spex_bk_control__define

dummy = {spex_bk_control, $
	spex_bk_order: ptr_new(), $	; Order of polynomial for computing background fit
	spex_bk_ratio: 0, $	; use ratio of high-energy profile for lower energies
	spex_bk_sm_width: 0 }	; smoothing width, used when bk_ratio is set

END
