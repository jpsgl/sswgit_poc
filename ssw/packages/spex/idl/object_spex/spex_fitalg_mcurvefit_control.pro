;------------------------------------------------------------------------------
function spex_fitalg_mcurvefit_control

struct = { spex_fitalg_mcurvefit_control }

struct.mcurvefit_itmax = 10
struct.mcurvefit_tol = 1e-3
struct.mcurvefit_quiet = 1

struct.spex_error_use_expected = 1
struct.spex_uncert = .05

return, struct

end
