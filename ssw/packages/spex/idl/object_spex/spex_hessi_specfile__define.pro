;------------------------------------------------------------------------------
; Time-stamp: <Tue Jul 26 2005 15:52:36 csillag tounesol.gsfc.nasa.gov>
;------------------------------------------------------------------------------

; Modifications:
;	15-Jul-2004, Kim.  Return spex_interval_filter in calling arguments.
;	30-Sep-2005, Kim.  Added spex_deconvolved, spex_pseudo_livetime to args.
;	26-May-2006, Kim.  Added spex_data_pos to args.
;	23-Jun-2006, Kim.  Add get_source_pos method
;	25-Feb-2010, Kim. Added file_index arg to read_data.
;

pro spex_hessi_specfile_test

  o =  obj_new( 'spex_hessi_specfile' )
  o->set,  spex_spec =  'hsi_spectrum_20020220_105002.fits'
  data =  o->getdata()

end

;------------------------------------------------------------------------------

pro spex_hessi_specfile::read_data, file_index=file_index, $
                       spectrum,  errors,  livetime,  $
                       spex_respinfo, spex_file_time,  spex_ut_edges,  spex_ct_edges,  $
                       spex_area, spex_title, spex_detectors,  $
                       spex_interval_filter, spex_units, spex_data_name, $
                       spex_deconvolved, spex_pseudo_livetime, spex_data_pos, $
                       err_code=err_code, _extra=_extra

  file =  self -> get( /spex_specfile )
  checkvar, file_index, 0
  file = file[file_index]
  
; why not use hsi_spectrum_fitsread here?  Ask richard. !!!!!
  read_hessi_4_ospex, FILES = file, $
                      _EXTRA = _ref_extra, $
                      data_str = data_str, $
                      ERR_CODE = err_code, $
                      ERR_MSG = err_msg

  if err_msg[0] ne '' then begin
      ;if not spex_get_nointeractive() then xmessage,err_msg else print,err_msg
      message, err_msg, /cont
      return
  endif

  spectrum =  data_str.rcounts
  errors =  data_str.ercounts
  livetime =  data_str.ltime

  spex_data_name = 'HESSI'
  spex_respinfo = data_str.respfile
  spex_file_time = utime([data_str.start_time, data_str.end_time])
  spex_ut_edges = data_str.ut_edges
  spex_ct_edges = data_str.ct_edges
  spex_area = data_str.area[0]
  spex_title = data_str.title
  spex_detectors = data_str.detused
  spex_units = data_str.units
  spex_interval_filter = data_str.atten_states
  spex_deconvolved = 0
  spex_pseudo_livetime = 0
  spex_data_pos = data_str.xyoffset

end

;------------------------------------------------------------------------------

function spex_hessi_specfile::get_source_pos

read_hessi_4_ospex_params, self -> get( /spex_specfile ), param, status
if status then begin
	if tag_exist(param,'USED_XYOFFSET') then return, param.used_xyoffset else $
		return, [-9999.,-9999.]
endif

end

;------------------------------------------------------------------------------

pro spex_hessi_specfile__define

  self = {spex_hessi_specfile, $
          INHERITS spex_data_strategy }

END
