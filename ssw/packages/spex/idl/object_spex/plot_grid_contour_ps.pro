; Find which parameters were used in grid (look for constant in either direction)
; and plot image of chisq over grid of these two parameters.
; Plot contours of full chisq +1,2,3,4,5
; If base is set, then image is that parameter index - shows how that parameter 
; varied over grid of two parameters, and plot the same chisq contours as above
; over image.
; dir is directory, like 'test1a'
; base is the index of the parameter for the base image
; _extra is sent to plot

; To send all to a single ps file, call plot_grid_contour_ps (listed below)


pro plot_grid_contour, savefile=savefile, base=base, _extra=_extra

;file = file_search(dir, '*n80*.sav')
checkvar, savefile, 'chigrid1/chi2_map_grid_i0_p5_n100_p6_n100.sav'
restore,savefile

;name=['Emission Measure','Temperature','','Electron Flux','Delta1','Break Energy (keV)', 'Delta2','Low Energy Cutoff (keV)']

np = n_elements(savep[*,0,0])
nx = n_elements(savefullchi[*,0])
ny = n_elements(savefullchi[0,*])

name = 'p' + trim(indgen(np))

; find indices that we did grid for
; if min,max of entire nx by ny are same then we didn't fit that param, so skip
; if min,max of all ny for x=0 are same then that's param1
; if min,max of all nx for y=0 are same then that's param2
for i=0,np-1 do begin
  if min(savep[i,*,*]) eq max(savep[i,*,*]) then continue
  if min(savep[i,0,*]) eq max(savep[i,0,*]) then i1 = i
  if min(savep[i,*,0]) eq max(savep[i,*,0]) then i2 = i
endfor

p1 = savep[i1,*,0]
p2 = savep[i2,0,*]
p1inc = average(p1[1:*]-p1)
p2inc = average(p2[1:*]-p2)

minc = min(savefullchi,imin)
iy = imin/nx
ix = imin - iy*nx

; Do this even if we're doing another one below (if base exists) because
; we'll use chisq contours from this object
m = obj_new('map')
m -> setmap, data=savefullchi, $
                 xc=p1[nx/2]-p1inc/2, $
                 yc=p2[ny/2]-p2inc/2, $
                 dx=p1inc, $
                 dy=p2inc, $
                 time=anytim(fit_time[0],/tai )

p_levs = [2.30, 4.61, 6.17, 9.21]
ci_levs = ['68.3%', '90%', '95.4%', '99%']
level = minc + p_levs
color = [5,2,7,10]

if !d.name eq 'PS' then begin
  !p.thick=5
  !p.charthick=3
  !p.font=0
endif

if not exist(base) then begin
  loadct,5
  m->plot,square=0,xtitle=name[i1], ytitle=name[i2], title='Chisquare', $
     /rescale,charsize=1.4, _extra=_extra, /cbar , drange=min(savefullchi) + [0.,25.]
  linecolors
  m->plot,/over,level=level,c_color=color
  !p.color = 255  
  legend, 'Min+'+trim(p_levs)+' ('+ci_levs+')', color=color, linest=0, $
    /top,charsize=1.4, textcolor=0, box=0
  plots, p1[ix], p2[iy], psym=7
  timestamp,/bottom
endif
;    ; For each of the three minima, find the index at the local minimum and get the p1,p2,fullchi values
;    q = min (savefullchi[*,0:40],i)
;    ix = i mod 100 & iy = i/100
;    q = min (savefullchi[*,45:70],i)
;    ix = [ix,i mod 100] & iy = [iy,45 + i/100]
;    q = min (savefullchi[*,70:*],i)
;    ix = [ix,i mod 100] & iy = [iy,70 + i/100]
;    for i=0,2 do print,savep[4,ix[i],iy[i]], savep[7,ix[i],iy[i]], savefullchi[ix[i],iy[i]]
;    ;      1.53000      19.4000      94.0799
;    ;      1.39000      29.3000      95.5953
;    ;      1.68500      39.6500      95.8126
;    
;    for i=0,2 do print,savep[0:7,ix[i],iy[i]]
;    ;      2.74624      3.18114      1.00000      12.3151      1.53000      128.622      2.56288      19.4000
;    ;      2.71541      3.19350      1.00000      8.82047      1.39000      116.329      2.53548      29.3000
;    ;      2.62881      3.21807      1.00000      7.34655      1.68500      147.476      2.59702      39.6500
;    
;    ;for i=0,2 do plots,savep[4,ix[i],iy[i]], savep[7,ix[i],iy[i]],psym=2
;    ;f='(f5.2)'
;    ;for i=0,2 do xyouts, savep[4,ix[i],iy[i]], savep[7,ix[i],iy[i]], $
;    ;  '  '+trim(savep[4,ix[i],iy[i]],f)+', '+ trim(savep[7,ix[i],iy[i]],f)+', '+trim(savefullchi[ix[i],iy[i]],f),charsize=1.5

if exist(base) then begin
  ; base image here is parameter with index = base, but contours
  ; on top of it are from map obj where chisq is the base image.
  if base ne i1 and base ne i2 then begin
    mp = obj_new('map')
    mp -> setmap, data=reform(savep[base,*,*]), $
                     xc=p1[nx/2-1], $
                     yc=p2[ny/2-1], $
                     dx=p1inc, $
                     dy=p2inc, $
                     time=anytim('23-Jul-2002 00:30',/tai)
    loadct,5
    mp->plot,square=0,xtitle=name[i1], ytitle=name[i2], /rescale,  title=' ', $
      charsize=1.4, /cbar, _extra=_extra, drange=[10.,2500.]
    xyouts, .5, .997, 'Chisquare Contours on Base Image = '+name[base], /normal, align=.5, charsize=1.6, charthick=3.
    linecolors
    ; use map object made above for chisq contours
    m->plot,/over,level=level,c_color=color
    !p.color = 255
    legend, 'Chi-square = ' + trim(level,'(f4.1)') + ' (+'+trim(p_levs)+')', color=color, linest=0, /bottom,charsize=1.4
  endif
endif

cleanplot
end

;--------------------------------------

pro plot_grid_contour_ps, psfile=psfile, _extra=_extra

checkvar, psfile, 'chi1grid1/chigrid1_grid_contour_plots_bigger2.ps'
ps, psfile, /color, /land
plot_grid_contour, _extra=_extra

;for i=0,7 do begin
;  if i eq 2 then continue
;  plot_grid_contour, dir=dir, base=i, _extra=_extra
;endfor

psclose
end