;+
; Name: spex_image_control
;
; Purpose:  Initialize variables in control structure for spex_image
;
; Kim Tolbert Aug 2005
; Modifications:
;
;-

function spex_image_control

var = {spex_image_control}

var.spex_roi_use = ptr_new(0)
var.spex_roi_integrate = 0
var.spex_roi_infile = ''

return, var
end