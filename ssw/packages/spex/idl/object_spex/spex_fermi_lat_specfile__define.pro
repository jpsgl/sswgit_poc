;+
;
; NAME:
;   spex_fermi_lat_specfile__define
;
; PURPOSE:
;   Provides read_data method for the Fermi LAT instrument.
;   NOTE: This procedure is based on spex_fermi_gbm__define.
;   Handles both PHA1 (single time) and PHAII (multiple times) files -
;     PHA1 structured with rows for energy, PHAII has rows for times
;
;
; CATEGORY:
;       SPECTRAL FITTING, SPEX - messenger
;
; CALLING SEQUENCE:
;
; CALLS:
;
; INPUTS:
;
; OUTPUTS:
;
; INPUT KEYWORDS:
;
; OUTPUT KEYWORDS:
;
; PROCEDURE:
;
; Written 24-Jan-2011, Kim Tolbert, based on spex_fermi_gbm_specfile__define
;
; Modification History:
; 15-Mar-2011, Kim.  Handle both PHA1 (single time) and PHAII (multiple times) files
; 21-Aug-2011, Kim. Added spex_def_eband to read arg list, even though not setting here
; 16-Dec-2011, Kim. In read_data, added check of hduclas4 in header for type of file, and for type PHAII, now handle 
;   two different formats - one has time,endtime,  other has tstart,telapse. Also for PHAII with 'endtime' now call
;   fermi_met2tim (previously times added to ref_utc and add leap sec in manually, same thing as fermi_met2tim does)
; 21-Mar-2012, Kim.  Extensions can be in different order, so now read extension by ext name (using rd_fits_ext). 
;   Also, handle a different format PHA1 file that has rate instead of counts in spec structure and make sure exposure
;   returned by fxpar is float (data stored as double screws things up later)
;
;------------------------------------------------------------------------------

pro spex_fermi_lat_specfile_test, o

  o =  obj_new( 'spex_fermi_lat_specfile' )
  o->set,  spex_specfile =  'D:\Analysis\working\fermig\LAT\pha2_LLE_roi21_theta80.fit'
  data =  o->getdata()

end

;------------------------------------------------------------------------------

pro spex_fermi_lat_specfile::read_data, file_index=file_index, $
                       spectrum,  errors,  livetime,  $
                       spex_respinfo, spex_file_time,  spex_ut_edges,  spex_ct_edges,  $
                       spex_area, spex_title, spex_detectors,  $
                       spex_interval_filter, spex_units, spex_data_name, $
                       spex_deconvolved, spex_pseudo_livetime, spex_data_pos, spex_def_eband=spex_def_eband, $
                       err_code=err_code

  file =  self -> get( /spex_specfile )
  checkvar, file_index, 0
  file = file[file_index]
  ;message, /cont, 'Reading ' + file + ' ...'
  a = mrdfits (file, 0, header, /silent)
  
  ; get filetype PHAI (single time) or PHAII (multiple times) from header.  If not there, assume PHAI.
  filetype = fxpar(header, 'FILETYPE', count=c)
  if c eq 0 then filetype = 'PHAI'  
      
;  spec =    mrdfits(file, 1, shdr, /silent, /dscale)
;  ebounds = mrdfits(file, 2, ehdr, /silent)
;  gti =     mrdfits(file, 3, ghdr, /silent)
   
  rd_fits_ext, file=file, extname='SPECTRUM', header=shdr, data=spec, /silent, /dscale
  rd_fits_ext, file=file, extname='EBOUNDS', header=ehdr, data=ebounds, /silent, /dscale
  rd_fits_ext, file=file, extname='GTI', header=ghdr, data=gti, /silent, /dscale
  
  hduclas4 = fxpar(shdr, 'HDUCLAS4', count=c)
  if c gt 0 then begin
    if trim(hduclas4) eq 'PHA:I' then filetype='PHAI'
    if trim(hduclas4) eq 'PHA:II' then filetype='PHAII'
  endif
  
;  spectrum = float(spec.counts)
;  errors = sqrt(spectrum)
  
  ; start/end times and livetime are stored differently in different types of files
  if filetype eq 'PHAI' then begin  ; PHA I file, single time interval, start/stop, exp are in header
    nen = n_elements(tag_exist(spec,'COUNTS') ? spec.counts : spec.rate)
    livetime = (fltarr(nen)+1.)*float(fxpar(shdr,'EXPOSURE'))  ; fxpar returns double
    stime = fermi_met2tim(fxpar(shdr, 'TSTART'))
    etime = fermi_met2tim(fxpar(shdr, 'TSTOP'))
    spex_ut_edges = [stime, etime]
  endif else begin   ; PHA II file, multiple time intervals
    nen = n_elements(spec[0].counts)
    if tag_exist(spec, 'TELAPSE') then begin  ; times are in MET
      livetime = (fltarr(nen)+1.0) # (spec.exposure)
      stime = fermi_met2tim(spec.tstart)
      etime = stime + spec.telapse
    endif else begin  ; times are rel to MET
      livetime = (fltarr(nen)+1.0) # (spec.exposure*(1.0-spec.quality)) ;bad points have quality flag set to 1
      stime = fermi_met2tim(spec.time)
      etime = fermi_met2tim(spec.endtime)
    endelse
    spex_ut_edges = transpose([[stime], [etime]])
  endelse

  spectrum = tag_exist(spec,'COUNTS')  ? float(spec.counts) : spec.rate * livetime
  errors = sqrt(spectrum)
    
  spex_file_time = minmax(spex_ut_edges)
  spex_ct_edges = transpose([[ebounds.e_min], [ebounds.e_max]])
  spex_area = 8000. ; from Gerry Share and Richard Schwartz 24-jan-2011
  spex_title = 'FERMI LAT'
  spex_detectors = fxpar(header, 'DETNAM', count=c)
  if c eq 0 then spex_detectors = 'LAT' 
  spex_interval_filter = -1
  spex_units = 'counts'
  spex_data_name = 'FERMI LAT'
  spex_deconvolved = 0
  spex_pseudo_livetime = 0
  spex_data_pos = [0.,0.]  

  spex_respinfo=''
  ; if user's accum time is within the input file time (should normally be!), then find any local rsp files
  ; and see if they have the correct detector and time for the input file.  If not, then look across network
  ; at rsp archive and download file.
;  accum_time = self->get(/spex_accum_time)
;  if has_overlap(spex_file_time, accum_time) then begin
;    rspfiles = file_search('*.{rsp,rsp2}',/full)  ; look for either rsp or rsp2 files in local dir
;    flat_obj = obj_new('spex_fermi_lat')
;    det = flat_obj->file2det(file)
;    ; time2rsp returns string like '_n1_bn100612_0054', which is det, date, time that must be in rsp file name
;    rspstring = flat_obj->time2rsp(accum_time, det, status=status) 
;    if status eq 1 then begin  ; found flare in accum_time interval
;      q = where (stregex(file_basename(rspfiles), rspstring, /bool), count)
;      if count gt 0 then begin
;        spex_respinfo = rspfiles[q[0]]
;      endif else begin
;        ; find url over network and copy to local dir
;        url = flat_obj->search(accum_time,/rsp, count=count, /copy, file=copyfiles)
;        q = where (stregex(file_basename(copyfiles), rspstring, /bool), count)
;        if count gt 0 then spex_respinfo = copyfiles[q[0]]
;      endelse
;    endif
;  endif       
   
;           FIX AREA, DATA_POS, UT_EDGES !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
end

;------------------------------------------------------------------------------
pro spex_fermi_lat_specfile__define

  self = {spex_fermi_lat_specfile, $
          INHERITS spex_data_strategy }

END
