;+
; Name: spex_instr__define
;
; Purpose: Abstract class inherited by the spex_... instrument classes (like spex_fermi_gbm) to provide
;   generic download, ordering, select_files, etc methods.
;   
; Written: Kim Tolbert, June 2010
; 21-Oct-2010, Kim.  Dominic changed sock_copy keyword from copy_file to local_file, made same change here
; 25-Jan-2012, Kim.  In order method, added best and _extra keywords, and return files instead of indices
; 21-Aug-2012, Kim. Something changed -copyfiles isn't returned correctly if files already exist, added
;   /use_network and /network to sock_copy call.  Now works, but not sure why it changed.
; -

pro spex_instr::download, files, copyfiles=copyfiles, dir=dir, err=err

checkvar, dir, curdir()
    
sock_copy, files, out_dir=dir, err=err, local_file=copyfiles, clobber=0, /use_network, /network

end

;-----
; If want to sort the files by something contained in the file name, do that here.
function spex_instr::order, files, time_range, best=best, _extra=_extra
;order = indgen(n_elements(files))
;return, order
best = 0
return, files
end

;-----
; If we need to select which file from among files to set as specfile and drmfile, do it here.
pro spex_instr::select_files, files, time_range, specfile, drmfile

specfile = files[0]
drmfile = ''

end

;-----

pro spex_instr__define
void = {spex_instr, dummy:0}
end