;+
; Name: ospex_params_write_pro
;
; Project:  HESSI
;
; Purpose: Write procedure ospex_params which defines structure with information
;	for all HESSI control and info parameters
;
; Method:  This routine generates the code in ospex_params.pro.
;	It  examines the routines in the HESSI branch of the
;	ssw tree, and attempts to find all of the control and info parameters used.
;	Then it looks at the structure defined in ospex_params_manual for
;	information that can't be generated automatically (like descriptions).  From those
;	two sources, all of the known information is accumulated, and the routine ospex_params
;	is written to create a structure with that information.
;
;	After calling ospex_params_write_pro to write the ospex_params.pro routine, you should
;	call ospex_params_write_htm.  ospex_params_write_htm will ospex_params to construct the
;	structure with all of the parameter information and generate html documentation for them.
;
;	These programs should be run periodically to keep the documentation up-to-date.
;
; Written:  Kim Tolbert 7-Mar-2004, based on hsi_params_write_pro
; Modifications:

;
;-


; break long text into strings with length <=60
function ospex_break_string, text, name

out_text = str2lines(text, length=60)
phead = "d[k]." + name + " = '"
ptail = "'"
nlines = n_elements(out_text)
if nlines gt 1 then begin
	phead = [phead, replicate("    '", nlines-1)]
	ptail = [replicate(" ' + $",nlines-1), ptail]
endif
return, phead + out_text + ptail
end

pro ospex_get_ci_params, fullfiles, c_or_i, list

skip_files = ['']	; skip files containing these strings

break_file, fullfiles, disk, dir, filename, ext

; same file may be in regular directories and atest.  Use atest version.
q = find_dup(filename)
if q[0] ne -1 then begin
	for i=0,n_elements(q)-1 do begin
		w = where(filename eq filename[q[i]], count)
		if count gt 2 then message,'Aborting  - more than two files called ' + filename[q[i]]
		z = where (strpos(fullfiles(w), 'atest') ne -1, count)
		if count eq 0 then message,'Aborting - two files with same name, but neither in atest ' + arr2str(fullfiles[w])
		if count eq 2 then message,'Aborting - two files with same name, both in atest ' + arr2str(fullfiles[w])
		rem = z[0] eq 0 ? 1 : 0
		rem_ind = append_arr(rem_ind, w[rem])
	endfor
	remove, rem_ind, fullfiles
	break_file, fullfiles, disk, dir, filename, ext
endif

; Use only files whose name starts with 'spex' or 'fit'
ind = where (stregex (filename, '^spex|^fit', /boolean, /fold_case) eq 1, count)
if count ne 0 then fullfiles = fullfiles(ind)

; eliminate files to skip
for i = 0,n_elements(skip_files)-1 do begin
	ind = where (strpos(fullfiles, skip_files[i]) eq -1, count)
	if count ne 0 then fullfiles = fullfiles(ind)
endfor

break_file, fullfiles, disk, dir, filename, ext

; ok, now have files we want to use.  Get structure and class names

; structure name is the part of the file name before __define
str_names = ssw_strsplit (filename, '__define')

; class name is the part of the name before control__define (or inpars, or parameters or outpars)
; some have _control, some just control, so after removing control__define, remove trailing _ if any.
if c_or_i eq 'Control' then begin
	classes = ssw_strsplit(filename, 'control__define')
endif else begin
	classes = ssw_strsplit(filename, 'info__define')
endelse
q = where (strlastchar(classes) eq '_', count)
if count gt 0 then begin
	tail = ssw_strsplit (classes[q], '_', /tail, head=head)
	classes[q] = head
endif

dirs = expand_path('+'+get_logenv('SSW_OSPEX') )

; get structure from manually edited ospex_params, so we can keep manually edited values
ospex_params_manual, man

; Loop through the structures we've found
for i = 0, n_elements(str_names)-1 do begin
	class = classes[i]
	list = [list + $
		'', $
		'; ' + c_or_i + ' parameters for ' + class]

	; if there is an initialization function for this class, run that, otherwise just
	; make a structure from the class name
	init_file = find_files (str_names[i] + '.pro', dirs)
	run_init = init_file[0] eq '' ? 0 : 1
	cmd = run_init ? 'str = ' + str_names[i] + '()' : 'str = {' + str_names[i] + '}'
	ok = execute(cmd)

	ntags = n_tags (str)
	if ntags eq 0 then begin
		print,'No tags at all in structure ' + str_names[i] + '.  Skipping.'
		goto, skip_str
	endif
	tnames = strlowcase(tag_names (str))

	; in order to find purpose in comments, read the full file and remove comment and null lines
	; also read initialization file if any.
	lines = rd_ascii(fullfiles[i])
	lines = strnocomment(lines, /remove_nulls, /leave_inline_comments)
	if run_init then begin
		init_lines = rd_ascii (init_file[0])
		init_lines = strnocomment (init_lines, /remove_nulls, /leave_inline_comments)
	endif

	; remove tags in structure that aren't explictly listed in file (i.e. get rid of
	; those that are in structure due to an inheritance)

	lines_str = strlowcase(arr2str(lines,delim=''))
	for j = 0,ntags-1 do $
		if strpos(lines_str, tnames[j]) eq -1 then str = rem_tag(str, tnames[j])

	ntags = n_tags (str)
	if ntags eq 0 then begin
		print,'No tags left in structure ' + str_names[i] + '.  Skipping.'
		goto, skip_str
	endif
	tnames = tag_names (str)

	if ntags gt 0 then begin
		for j = 0,ntags-1 do begin
			name = strlowcase(tnames[j])
			sz = size(str.(j),/st)
			type = sz.type_name
			if sz.n_dimensions gt 0 then begin
				dim = strtrim(sz.dimensions,2)
				type = type + '('
				for m = 0,sz.n_dimensions-1 do type = type + dim[m] + ','
				type = strmid(type, 0, strlen(type)-1) + ')'
			endif
			type = strlowcase(type)

			; only try to get default if run_init is set (we ran an initialization file)
			; can only list default if data type is not ptr, obj, or str, and n_elements is 1
			default = ''
			if run_init then begin
				can_do = (sz.type gt 0 and sz.type lt 8) or (sz.type gt 11)
				if can_do and (sz.n_dimensions eq 0) then begin
					default = str.(j)
					; if type is byte, make it fixed before converting to string or it's wrong
					if sz.type eq 1 then default = fix(default)
					default = strlowcase(strtrim(default, 2))
				endif
			endif

			; try to get purpose from comment in line.  first make sure parameter was listed in
			; file (could have been inherited).  If ran an initialization routine, then let that
			; comment override comment in __define file.
			purpose = ''
			ind = stregex(lines, name+':', /fold_case, /boolean)
			q = (where(ind ne 0, count))[0]
			if count gt 0 then if strpos(lines[q],';') ne -1 then $
				purpose = strtrim(ssw_strsplit (lines[q], ';', /tail),2)
			if purpose[0] eq '' and run_init then begin
				ind = stregex(init_lines, name, /fold_case, /boolean)
				q = (where(ind ne 0, count))[0]
				if count gt 0 then if strpos(init_lines[q],';') ne -1 then $
					purpose = strtrim(ssw_strsplit (init_lines[q], ';', /tail),2)
			endif
			if purpose[0] ne '' then purpose = repchar (purpose[0], "'", '"')

			units = ''
			range = ''
			level = c_or_i eq 'Control' ? 'Expert' : 'N/A' ;default for control is expert
			more_doc = ''

			; use values from previous ospex_params file if they're there.
			q = where (man.name eq name and man.class eq class, count)
			if count gt 0 then begin

				;first print message if default found from .pro's is different from what's in
				;ospex_params_manual
				;print,class, name, default
				if is_number(default) then if float(default) ne float(man[q[0]].default) then begin
					print, 'Default for ' + name + '(class=' + class + ') has changed from ' + $
						man[q[0]].default + ' to ' + default + '  @@@@@@@@@'
				endif

				c_or_i = man[q[0]].control_or_info
				units = man[q[0]].units
				default = man[q[0]].default
				range = man[q[0]].range
				level = man[q[0]].level
				purpose = man[q[0]].purpose
				more_doc = man[q[0]].more_doc
			endif else begin
				w = where (man.name eq name,c)
				if c eq 0 then begin
					print,''
					print,'Found new parameter:  ' + name + '   !!!!!!!!!!!!!!!!!!!!!!!!!!'
					print,' '
				endif else begin
					print,''
					print,'Parameter in a different class  !!!!!!!!!!!!'
					print,'  Parameter: ' + name + '   New class: ' + class + ''
					print,'  Previous class(es): ' + arr2str(man[w].class, ', ')
					print,''
				endelse
			endelse

			; break long purpose into strings with length <=60
			purpose = repstr (purpose, "'", "''")
			more_doc = repstr (more_doc, "'", "''")
			purpose = ospex_break_string(purpose, 'purpose')
			more_doc = ospex_break_string(more_doc, 'more_doc')

			list = [list + $
			'', $
			'k = k + 1', $
			"d[k].name = '" + name + "'", $
			"d[k].class = '" + class + "'", $
			"d[k].type = '" + type + "'", $
			"d[k].units = '" + units + "'", $
			"d[k].default = '" + default + "'", $
			"d[k].range = '" + range + "'", $
			"d[k].control_or_info = '" + c_or_i + "'", $
			"d[k].level = '" + level + "'", $
			purpose, $
			more_doc, $
			'' ]

		endfor
	endif

skip_str:

endfor

end

;-----

pro ospex_params_write_pro

dirs = expand_path('+'+get_logenv('SSW_OSPEX') )

c_files = find_files('*control__define.pro', dirs)

i_files = find_files('*info__define.pro', dirs)

list = [';+', $
	'; Name: ospex_params', $
	'; Purpose: Define structure with information for all OSPEX control and info parameters', $
	'; Calling Sequence: ospex_params, param_struct', $
	'; ', $
	'; NOTE:  THIS ROUTINE IS GENERATED AUTOMATICALLY BY OSPEX_PARAMS_WRITE_PRO.  Do not edit.', $
	';-', $
	'', $
	'pro ospex_params, d', $
	'', $
	'k = -1', $
	'd = replicate({ospex_params_str}, 2000)', $
	'']

ospex_get_ci_params, c_files, 'Control', list
ospex_get_ci_params, i_files, 'Info', list

list = [list, $
	'', $
	'd = d[0:k]', $
	'end']

prstr, list, file='ospex_params.pro'
prstr, list, file='ospex_params_copy.pro'

;check whether there are some parameters in ospex_params_manual that are no longer needed
resolve_routine, 'ospex_params'
ospex_params,d & ospex_params_manual,dm
n = n_elements(d) & nm = n_elements(dm)
print,'There are ', n, ' in new ospex_params, ', nm, ' in ospex_params_manual.'
;if nm gt n then begin
	for i = 0,nm-1 do begin
		q = where (dm[i].name eq d.name and dm[i].class eq d.class, count)
		if count eq 0 then print, dm[i].name + ' in class ' + dm[i].class + ' should be removed from ospex_params_manual.'
		if count gt 1 then print, strtrim(count,2) + ' Duplicates.  ' + dm[i].name + ' in class ' + dm[i].class
	endfor
;endif

end

