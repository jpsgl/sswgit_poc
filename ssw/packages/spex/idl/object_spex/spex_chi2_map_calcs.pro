function spex_chi2_map_calcs, p, chi, minind, plusn, valid=valid

errs = ''
erre = ''

chiplusn = chi[minind] + plusn

w = where (chi[0:minind] gt chiplusn, nst)
if nst eq 0 then begin
  xs = p[0]
  errs = 'lower'
endif else begin
  ii = w[nst-1] + [0,1]
  xs = interpol(p[ii], chi[ii], chiplusn)
endelse

w = where (chi[minind:*] gt chiplusn, nen)
if nen eq 0 then begin
  xe = last_item(p)
  erre = 'upper'
endif else begin
  ii = (w[0] + minind) + [-1,0]
  xe = interpol(p[ii], chi[ii], chiplusn)
endelse

valid = [nst gt 0, nen gt 0]

if errs ne '' or erre ne '' then begin
  msg = 'WARNING: Increase your parameter range!  Minimum chi-square plus '+trim(plusn)+ ' '
  if errs ne '' then message, /cont, msg + errs + ' limit never reached.'
  if erre ne '' then message, /cont, msg + erre + ' limit never reached.'
endif

return, [xs,xe]
end