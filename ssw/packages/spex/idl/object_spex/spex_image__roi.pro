;+
; Name: spex_image__roi
;
; Purpose:  ROI method presents user with a panel display showing all the images in an image cube.
; The user can click on single or multiple (click and drag) images.  A list of options pops up
; to choose what to define regions for, or what to display.
;
; Written, Kim, August 2005
; Modifications:
;   7-Oct-2005, Kim.  Added panel_size, colortable keywords to roi method.
;   27-Jan-2006, Kim.  When first called, call self->getdata just in case it hasn't
;     been done yet, since this expects some data info parameters to be set (ut,energy)
;   29-Oct-2007, Kim.  Added options to send multiple images to plotman (previously one at at time)
;   11-Mar-2016, Kim. Corrected error clicking 'draw images only' incorrectly sent all images to plotman,
;     and added options for up to 6 ROIs (previously 4), and added get_num_roi method
;   06-Sep-2016, Kim. Moved get_num_roi method to spex_image__define (sometimes need it before
;     this file has been compiled, methods here should only be for defining ROIs graphically) 
;     
;----------

function spex_panel_pos, x, y, state
	xsize = state.xsize
	ysize = state.ysize
	ntim= state.ntim
	nen = state.nen
	x = x / float(xsize)
	y = y / float(ysize)
	t_index = (Fix( x*(ntim+1) ) -1) >0 < (ntim-1)
	e_index = nen-1 - ((Fix( y*(nen+1) ) -1) >0) >0 < (nen-1)
	return, [t_index, e_index]
end

;----------

pro spex_image::panel_draw, state, subset=subset, panel_size=psize, xsize=xsize, ysize=ysize, $
	show_selection=show_selection, roinum=roinum

show_sel = keyword_set(show_selection)

if keyword_set(subset)  then begin
	j1 = state.e_idx[0] & j2 = state.e_idx[1]
	i1 = state.t_idx[0] & i2 = state.t_idx[1]
	ntim = i2 - i1 + 1
	nen = j2 - j1 + 1
endif else begin
	wset,state.drawid
	erase
	j1 = 0 & j2 = state.nen - 1
	i1 = 0 & i2 = state.ntim - 1
	psize = state.panel_size
	xsize = state.xsize_win
	ysize = state.ysize
	ntim = state.ntim
	nen = state.nen
endelse

yes_boxes = ptr_valid(self.boxes)
if yes_boxes then begin
	boxes = *self.boxes
	valid = ptr_valid(boxes)
	yes_boxes = total(valid) ne 0
endif else valid = intarr(nen,ntim)

tvlct, r,g,b, /get
loadct, state.colortable, /silent

pos_old = !p.position

; xfrac, yfrac are the normalized size of each image (+1 because label takes one image space)
xfrac = 1./(ntim+1.)
yfrac = 1./(nen+1.)

; Loop through images, putting energy labels to the left.  For each image, make a border by setting
; edge pixels to min of image, or if show_sel and there's a valid box for image,
; then set to max of image.

for ie = j1,j2 do begin

    xyouts, 0.5*psize[0]/xsize, 1-(ie-j1+0.5)*psize[1]/ysize, $
       state.elabels[0,ie] + ' - ' + '!C' + state.elabels[1,ie] + ' keV', $
       /normal, charsize = 0.9, align = 0.5

    for it = i1,i2 do begin

		; make sure the valid pointer points to a real box (nop ne 0)
		if valid[ie,it] then valid[ie,it] = ( (*boxes[ie,it]).nop[0] ne 0 )

		im = congrid( state.im(*, *, ie, it), psize[0], psize[1])
		border = (show_sel and valid[ie,it]) ? max(im) : min(im)
		im[*, [0,psize[1]-1] ] = border
		im[ [0,psize[0]-1], *] = border
		tvscl, im, (ie-j1)*(ntim+1)+(it-i1)+1

		if show_sel then begin
			xloc = ( (it-i1) +1.)/(ntim+1.)
			yloc = (float( nen )- (ie-j1) )/(nen+1.)
			!p.position = [xloc, yloc, xloc+xfrac, yloc+yfrac] ;normalized plot box location

			if valid[ie,it] then begin
				box = *boxes[ie,it]
				; first just set internal plot parameters so next command will work right
				plot, state.xrange_img, state.yrange_img, /nodata, /noerase, xstyle=5,ystyle=5
				hsi_oplot_clean_boxes, 0, 2,2,255, cw_pixels=box.list, cw_nop=box.nop, boxnum=roinum
			endif else begin
				;draw an X over image to show that no boxes are defined
				plot, state.xrange_img, state.yrange_img, /noerase, xstyle=5,ystyle=5
				plot, reverse(state.xrange_img), state.yrange_img, /noerase, xstyle=5,ystyle=5
			endelse
		endif

    endfor
endfor

; now put time labels across bottom
for it = i1,i2 do begin
  xyouts, (1+it-i1+0.5)*psize[0]/xsize, 0.7*psize[1]/ysize, $
     state.tlabels[0,it] + '!C' + state.tlabels[1,it], $
     /normal, charsize = 0.9, align = 0.5
endfor

tvlct, r,g,b
!p.position = pos_old

end

;----------

pro spex_image::highlight, state, t_use=t_use, e_use=e_use, redraw=redraw, color=color

if keyword_set(redraw) then self -> panel_draw, state, /show_selection

wset, state.drawid
ntim = state.ntim
nen = state.nen
checkvar, t_use, state.t_idx
checkvar, e_use, state.e_idx
checkvar, color, 0

; xfrac, yfrac are the normalized size of each image (+1 because label takes one image space)
xfrac = 1./(ntim+1.)
yfrac = 1./(nen+1.)

; set position to the outermost edges of all images selected.
xloc_left = 1.
xloc_right = 0.
yloc_bot = 1.
yloc_top = 0.
for ie = e_use[0], last_item(e_use) do begin
	for it = t_use[0], last_item(t_use) do begin
		xloc = ( it +1.)/(ntim+1.)
		yloc = (float( nen )- ie )/(nen+1.)
		xloc_left = xloc_left < xloc
		xloc_right = xloc_right > (xloc+xfrac)
		yloc_bot = yloc_bot < yloc
		yloc_top = yloc_top > (yloc+yfrac)
	endfor
endfor

pos_old = !p.position

!p.position = [xloc_left, yloc_bot, xloc_right, yloc_top] ;normalized plot box location
; sometimes the top and right lines aren't drawn, so make the range a little smaller
; than the actual range
xr = state.xrange_img
delta = (xr[1]-xr[0]) * .005
xr = [xr[0]+delta, xr[1]-delta]
x = [xr[0], xr[1], xr[1], xr[0], xr[0]]
yr = state.yrange_img
delta = (yr[1]-yr[0]) * .005
yr = [yr[0]+delta, yr[1]-delta]
y = [yr[0], yr[0], yr[1], yr[1], yr[0]]
plot,x,y, /noerase, xstyle=5, ystyle=5, thick=5, linestyle=2, color=color, $
	xrange=state.xrange_img, yrange=state.yrange_img


!p.position = pos_old

end

;----------

;pro spex_image::show_selection, state, subset=subset, refresh=refresh, roinum=roinum
;
;if keyword_set(refresh) then spex_panel_draw, state
;
;if not ptr_valid(self.boxes) then return
;
;boxes = *self.boxes
;
;valid = ptr_valid(boxes)
;
;if total(valid) eq 0 then return
;
;if keyword_set(subset) then begin
;	j1 = state.e_idx[0] & j2 = state.e_idx[1]
;	i1 = state.t_idx[0] & i2 = state.t_idx[1]
;	ntim = i2 - i1 + 1
;	nen = j2 - j1 + 1
;endif else begin
;	wset, state.drawid
;	j1 = 0 & j2 = state.nen - 1
;	i1 = 0 & i2 = state.ntim - 1
;	ntim = state.ntim
;	nen = state.nen
;endelse
;pos_old = !p.position
;
;for it = i1,i2 do begin
;	for ie = j1,j2 do begin
;        x_term = ( (it-i1) +1.)/(ntim+1.)
;        y_term = (float( nen )- (ie-j1) )/(nen+1.)
;        xfact = 1./(ntim+1.)
;        yfact = 1./(nen+1.)
;		!p.position = [x_term, y_term, x_term+xfact, y_term+yfact]
;;		print, 'x_term, xfact, y_term, yfact = ', x_term, xfact, y_term, yfact
;;		print,'!p.position = ', !p.position
;
;		xr = state.xrange_img
;		x = [xr[0], xr[1], xr[1], xr[0], xr[0]]
;		yr = state.yrange_img
;		y = [yr[0], yr[0], yr[1], yr[1], yr[0]]
;		color = valid[ie,it] ? 255 : 0
;		plot, x,y, /noerase, xstyle=5,ystyle=5, color=color
;;        plot, [0,1,1,0,0], [0,0,1,1,0], /normal, thick=2, /nodata, /noerase, xstyle=5,ystyle=5
;;        z0=.002
;;		z1=.998
;;		oplot,[z0,z1,z1,z0,z0], [z0,z0,z1,z1,z0],thick=2
;		if valid[ie,it] then begin
;
;			box = *boxes[ie,it]
;
;
;
;			;plot, state.xrange_img, state.yrange_img, /nodata, /noerase, xstyle=5,ystyle=5
;			hsi_oplot_clean_boxes, 0, 2,2,255, cw_pixels=box.list, cw_nop=box.nop, boxnum=roinum
;
;	     endif
;	 endfor
;endfor
;
;!p.position = pos_old
;end

;----------

function spex_image::choices, state, ev

case ev.press of

	; Left mouse button - list options for a single image or multiple images depending
	; on whether user selected single or multiple
	1: begin
		single = same_data( [state.t_idx[0], state.e_idx[0]], [state.t_idx[1], state.e_idx[1]] )

		choices = single ? state.single_choices : state.multi_choices

		short = single ? state.single_short : state.multi_short
		end

	; Right mouse button - list the general options that don't apply to particular images
	4: begin
		choices = state.choices
		short = state.choices_short
		end

	else: return, 'cancel'

endcase

; since panel displays scrolls, there doesn't seem to be a way to place these options
; next to the panel clicked, since can't find out where that is on screen (only know where
; it is in full scrollable window.  So put it halfway down.  Can work on this later.
geomid = widget_info( ev.id, /geometry)
geomtop = widget_info( ev.top, /geometry)
position=[geomtop.xoffset + geomid.xoffset + geomid.xsize/2., $
	geomtop.yoffset + geomid.yoffset + geomid.ysize/2.]

choice = popup_menu (choices, /index, position=position, group=ev.top)

return, choice eq -1 ? 'cancel' : short[choice]

end

;----------

pro spex_image::plot_spectra, state, motion=motion

window_save = !d.window
pmulti_save = !p.multi

t_index = state.t_idx
n_spectra = t_index[1] - t_index[0] + 1
!p.multi = [0, 1, n_spectra]
charsize = n_spectra gt 2 ? 1.5 : 1.

device, get_screen=scr_size
if keyword_set(motion) then begin
	if is_wopen(state.spec_window,/show) then wset, state.spec_window else begin
		;ypos=0 means lower left on Motif, upper left on windows
		ypos = os_family() eq 'Windows' ? scr_size[1]-250 : 0
		window, /free, xsize=.4*scr_size[0], ysize=200, xpos=.6*scr_size[0], ypos = ypos
		state.spec_window = !d.window
	endelse
endif else begin
	ysize = n_spectra*200
	wbase = widget_base(title='SPEX image spectra', group=state.wbase)
	wdraw = WIDGET_DRAW(wbase, x_scroll_size=.4*scr_size[0], Y_SCROLL_SIZE=(.8*scr_size[1] <  ysize), $
                     xsize=.4*scr_size[0],  ysize=ysize,  /scroll)
	widget_offset, state.wbase, xoffset, yoffset, newbase=wbase
	widget_control, wbase, /realize, xoffset=xoffset, yoffset=yoffset
endelse

; can't call getdata here because it will close the roi selector.
;data = self -> getdata()
;en = self -> getaxis(/ct_energy, /mean)
tim = self -> getaxis(/ut, /mean)

spex_roi_use = (self->get(/spex_roi_integrate)) ? '  All regions' : $
	'  Region ' + arr2str(trim(self->get(/spex_roi_use)))
for it = t_index[0], t_index[1] do begin
	int=it	; for some reason next line changes int to long - crashes since it is FOR index
	state.spex_obj ->plot_spectrum, /no_plotman, this_interval=int, /origint, $
		spex_units='flux', $
		title='Spectrum, ' + anytim(tim[int],/vms) + spex_roi_use, $
		legend_loc=0, xstyle=1, charsize=charsize, /show_err
;	plot, en, data.data[*,it], /ylog, /xlog, xstyle=1, title = 'Spectrum, ' + anytim(ti[it],/vms)
endfor

!p.multi = pmulti_save
wset,window_save

end

;----------

pro spex_image::plot_timeprofile, state, motion=motion


if state.ntim eq 1 then begin
	if not keyword_set(motion) then message, /cont, 'Only one time interval - not drawing time profiles.'
	return
endif

window_save = !d.window
pmulti_save = !p.multi

e_index = state.e_idx
n_times = e_index[1] - e_index[0] + 1
!p.multi = [0, 1, n_times]
device, get_screen=scr_size

if keyword_set(motion) then begin
	if is_wopen(state.time_window,/show) then wset, state.time_window else begin
		;ypos=0 means lower left on Motif, upper left on windows
		ypos = os_family() eq 'Windows' ? scr_size[1]-250 : 0
		window, /free, xsize=.4*scr_size[0], ysize=200, xpos=.1*scr_size[0], ypos = ypos
		state.time_window = !d.window
	endelse
endif else begin
	ysize = n_times*200
	wbase = widget_base(title='SPEX image time profiles', group=state.wbase)
	wdraw = WIDGET_DRAW(wbase, x_scroll_size=.4*scr_size[0], Y_SCROLL_SIZE=(.8*scr_size[1] <  ysize), $
	                     xsize=.4*scr_size[0],  ysize=ysize,  /scroll)
	widget_offset, state.wbase, xoffset, yoffset, newbase=wbase
	widget_control, wbase, /realize, xoffset=xoffset, yoffset=yoffset
endelse

; can't call getdata here because it will close the roi selector.
;data = self -> getdata()
en = self -> getaxis(/ct_energy, /mean)
;ti = self -> getaxis(/ut, /mean)
charsize = n_times gt 2 ? 1.5 : 1.

spex_roi_use = (self->get(/spex_roi_integrate)) ? '  All regions' : $
	'  Region ' + arr2str(trim(self->get(/spex_roi_use)))
for ie = e_index[0], e_index[1] do begin
	int=ie	; for some reason next line changes int to long - crashes since it is FOR index
	; use origint - - otherwise it will bin by spex_eband
	state.spex_obj ->plot_time, /no_plotman, this_interval=int, /origint, $
		spex_units='flux', $
		title='Time Profile, ' + trim(en[int]) + ' keV' + spex_roi_use, $
		legend_loc=0, xstyle=1, charsize=charsize
;	utplot, anytim(ti,/ext), data.data[ie,*], /ylog, xstyle=1, title = 'Time Profile, ' + trim(en[ie]) + ' keV'
endfor

!p.multi = pmulti_save
wset,window_save

end

;----------

;pro spex_hessi_image::plot_image, state
;
;pobj = state.spex_obj->get(/spex_plotman_obj)
;
;source = state.obj->get(/source)
;source -> plotman,t_idx=state.t_idx[0],e_idx=state.e_idx[0], $
;	plotman_obj=pobj, colortable=state.colortable
;
;end

;----------

pro spex_image::expand, state, panel_size=panel_size

self -> highlight, state,  /redraw

window_save = !d.window

nen = state.e_idx[1] - state.e_idx[0] + 1
ntim = state.t_idx[1] - state.t_idx[0] + 1
panel_size = keyword_set(panel_size) ? panel_size : state.panel_size*self->get(/spex_roi_expand)
xsize =  (ntim+1)*panel_size[0]
;xsize_win = xsize * 1.004
xsize_win = xsize
ysize =  (nen+1)*panel_size[1]

wbase = WIDGET_BASE(title='SPEX Image Panels', group=state.wbase)

device, get_screen_size=scr_size
wdraw = WIDGET_DRAW(wbase, X_SCROLL_SIZE=(.9*scr_size[0] < xsize_win), Y_SCROLL_SIZE=(.8*scr_size[1] <  ysize), $
                     xsize =  xsize_win,  ysize = ysize,  /scroll)
widget_offset, state.wbase, xoffset, yoffset, newbase=wbase
widget_control, wbase, /realize, xoffset=xoffset, yoffset=yoffset

self -> panel_draw, state, /subset, panel_size=panel_size, xsize=xsize_win, ysize=ysize, /show_selection

wset,window_save

end

;----------

pro spex_roi_event, ev
widget_control, ev.top, get_uvalue=state
if obj_valid(state.obj) then state.obj -> roi_event, ev else widget_control, state.wbase, /destroy
end

;----------

pro spex_image::roi_event, ev
;PRO spex_panel_display_event, ev

;  COMPILE_OPT hidden

  if tag_names( ev, /structure) eq 'WIDGET_KILL_REQUEST' then begin
    widget_control, ev.top, get_uvalue=state
    widget_control, /destroy, ev.top
  endif

  if tag_names( ev, /structure_name ) eq 'WIDGET_DRAW' then begin
      widget_control, ev.top, get_uvalue=state

      ; type of 2 means motion event, so draw spectrum and tprofile for this column and row
      if ev.type eq 2 then begin
        te = spex_panel_pos (ev.x, ev.y, state)
        state.t_idx = [te[0], te[0]]
        state.e_idx = [te[1], te[1]]
        self -> plot_spectra, state, /motion
        self -> plot_timeprofile, state, /motion

      endif else begin

        ; Otherwise it's a click.
        ; Left-click (press=1) gives menu of options that depend on which image(s) were selected
        ; Right-click (press=4) gives menu of options that are general to all images

		if ev.press eq 1 or ev.press eq 4 then begin
			if ev.press eq 1 then begin
	          wset,state.drawid
	          xydev = stretch_box (ev.id, /dev, color=255, event=ev, /no_sort)
	          te0 = spex_panel_pos (xydev[0,0], xydev[1,0], state)
	          te1 = spex_panel_pos (xydev[0,1], xydev[1,1], state)
	          state.t_idx = [ te0[0], te1[0] ]
	          state.e_idx = [ te0[1], te1[1] ]
	;          print, 't index start, end ', state.t_idx
	;          print, 'e index start, end ', state.e_idx
	          widget_control, ev.top, set_uvalue=state
	          start_te = te0
	          self -> highlight, state, color=255
	        endif

	        choice = self -> choices (state, ev)
	        orig_choice = choice

			; for any choices with all_en, set to all energies.
			; for any choices with all, but not all_en, set to all energies, all times
			if strpos(choice, 'all_en') ne -1 then begin
				state.e_idx = [0, state.nen-1]
			endif else begin
				if strpos(choice, 'all') ne -1 then begin
					state.e_idx = [0, state.nen-1]
					state.t_idx = [0, state.ntim-1]
				endif
			endelse

			; now that we've set the energies and times to do, if choice was any of the
			; roi options, set to 'roi'  If choice was any of the clear or image options,
			; set to 'clear' or 'image' (i.e. strip off the _all or _all_en)

			if strpos(choice, 'roi') ne -1 then begin
				self -> highlight, state, /redraw, color=255
				choice = 'roi'
			endif

			if strpos(choice, 'clear') ne -1 then choice = 'clear'

			if choice ne 'draw_images' && strpos(choice, 'image') ne -1 then choice = 'image'

			case choice of

                'roi': begin
                  if strpos(orig_choice, 'click') ne -1 then begin
                	; if original choice had a 'click' in it, need to let user click
                  ; in an image to choose which one to define regions in. Temporarily
                  ; set event procedure to nothing so we can detect click here
                  ; instead of calling 'spex_roi_event'.
                	print, 'Click image to use for defining ROIs'
                  widget_control, ev.id, event_pro=''
                  click_ev = widget_event(ev.id)
                  widget_control, ev.id, event_pro='spex_roi_event'
                  te = spex_panel_pos (click_ev.x, click_ev.y, state)
                  t_use = te[0]  & e_use = te[1]
                endif else begin
                  t_use = start_te[0]
                  e_use = start_te[1]
                endelse
                  self -> highlight, state, t_use=t_use, e_use=e_use
                  self -> select_roi, state.t_idx, state.e_idx, $
	                  colortable=state.colortable, t_use=t_use, e_use=e_use
	             	end

	             'clear': self -> clear_roi, state.t_idx, state.e_idx

	             'spectra': begin
	             	self -> highlight, state, /redraw
	             	self -> plot_spectra, state
	             	end

	             'tprofile': begin
	             	self -> highlight, state, /redraw
	             	self -> plot_timeprofile, state
	             	end

	             'image': begin
	             	self -> highlight, state, /redraw
	             	self -> plot_image, spex_obj=state.spex_obj, $
	             		t_idx=state.t_idx,e_idx=state.e_idx, $
	             		colortable=state.colortable
	             	end

	             'expand': self -> expand, state

	             'draw_images': self -> panel_draw, state

	             'showall': self -> panel_draw, state, /show_selection

	             'show0': self -> panel_draw, state, /show_selection, roinum=0

	             'show1': self -> panel_draw, state, /show_selection, roinum=1

	             'show2': self -> panel_draw, state, /show_selection, roinum=2

	             'show3': self -> panel_draw, state, /show_selection, roinum=3
	             
	             'show4': self -> panel_draw, state, /show_selection, roinum=4
	             
	             'show5': self -> panel_draw, state, /show_selection, roinum=5

	             'enable_auto': widget_control, state.wdraw, draw_motion_events=1

	             'disable_auto': widget_control, state.wdraw, draw_motion_events=0

	             'close': widget_control, /destroy, ev.top

	             else:
	          endcase

				; if changed roi or cleared, then redraw
				if stregex(choice, 'roi|clear', /bool) then begin
					self -> panel_draw, state, /show_selection
				endif

	          ;end

	      endif
       endelse

  endif

if xalive(state.wbase) then widget_control, state.wbase, set_uvalue=state
END

;----------

; Widget creation routine.
PRO spex_image::roi, group=group, $
	spex_obj=spex_obj, panel_size=panel_size, colortable=colortable;, scale =  scale

; if there's already a region selector active, kill it so there's no confusion.
if xregistered ('spex_roi') then xkill, 'spex_roi'

; call getdata in case it hasn't already been called so info parameters for
; the input file have been set
a = self -> getdata()

checkvar, panel_size, self->get(/spex_roi_size)
checkvar, colortable, self->get(/spex_roi_color)

choices = [	'Note: Left click, or click-and-drag for options for selected images.', $
	'', $
	'Show all regions selected', $
	'Show Region 0', $
	'Show Region 1', $
	'Show Region 2', $
	'Show Region 3', $
	'Show Region 4', $
	'Show Region 5', $
	'Draw images only', $
	'', $
	'Enable auto plots', $
	'Disable auto plots', $
	'', $
	'Cancel', $
	'Close Region Selector']

choices_short = ['', $
	'', $
	'showall', $
	'show0', $
	'show1', $
	'show2', $
	'show3', $
	'show4', $
	'show5', $
	'draw_images', $
	'', $
	'enable_auto', $
	'disable_auto', $
	'', $
	'cancel', $
	'close']

single_choices = [ 'Note: Right-click for general options.', $
	'', $
	'Define region(s) in this image for all energies', $
	'Define region(s) in this image for all times and energies', $
	'Define region(s) in this image for this image only', $
	'', $
	'Show Spectrum for this time', $
	'Show Time Profile for this energy', $
	'Show Images for this time, all energies in Plotman', $
	'Show all Images in Plotman', $
	'Show this Image in Plotman', $
	'Expand this image', $
	'', $
	'Clear regions for this image', $
	'Clear regions for this time for all energies', $
	'Clear all regions from all images', $
	'', $
	'Cancel', $
	'Close Region Selector']

single_short = ['', $
	'', $
	'roi_all_en', $
	'roi_all', $
	'roi', $
	'', $
	'spectra', $
	'tprofile', $
	'image_all_en', $
	'image_all', $
	'image', $
	'expand', $
	'', $
	'clear', $
	'clear_all_en', $
	'clear_all', $
	'', $
	'cancel', $
	'close']

multi_choices = [  'Note: Right-click for general options.', $
	'', $
	'Define region(s) for selected times, all energies. Next click specifies image to use.', $
	'Define region(s) for selected images. Next click specifies image to use.', $
	'', $
	'Show Spectra for these times', $
	'Show Time Profiles for these energies', $
	'Show these Images in Plotman', $
	'Expand these images', $
	'', $
	'Clear regions for these images', $
	'Clear regions for these times for all energies', $
	'Clear all regions from all images', $
	'', $
	'Cancel', $
	'Close Region Selector']

multi_short = [ '', $
	'', $
	'roi_all_en_click', $
	'roi_click', $
	'', $
	'spectra', $
	'tprofile', $
	'image', $
	'expand', $
	'', $
	'clear', $
	'clear_all_en', $
	'clear_all', $
	'', $
	'cancel', $
	'close']

img_obj = self -> get(/source)
old_use_single =  img_obj -> get( /use_single )
img_obj -> set,  use_single = 0
im = img_obj -> getdata()
im_size = size(im,/dim)

panel_size = n_elements(panel_size) eq 1 ? [panel_size, fix(panel_size*im_size[1]/im_size[0])] : panel_size

e_edges = img_obj -> getaxis(/energy, /edges_2)
t_edges = img_obj->getaxis(/ut, /edges_2 )
img_obj -> set,  use_single =  old_use_single
xrange_img = minmax(img_obj -> getaxis(/xaxis))
yrange_img = minmax(img_obj -> getaxis(/yaxis))

elabels = trim(e_edges,'(f5.1)')
tlabels = strmid(anytim(t_edges, /yy), 10, 10)

ntim = n_elements(t_edges[0,*])
nen = n_elements(e_edges[0,*])

; this scaling doesn't seem to work now.  Fix and uncomment if needed. Kim
;if keyword_set( scale ) then begin
;	; are these two reforms necessary? kim
;    max_in_time_int = reform( max( max( im, dim=1 ), dim=1 ))
;    if ntim gt 1 then begin
;        max_in_time_int =  max( max_in_time_int,  dim = 2)
;    endif
;    min_in_time_int = reform( min( min( im, dim=1 ), dim=1 ))
;    if ntim gt 1 then begin
;      min_in_time_int =  min( min_in_time_int,  dim=2)
;    endif
;    if keyword_set( pos ) then min_in_time_int =  min_in_time_int*0
;    for j=0, nen-1 do begin
;      im[*, *, j, *] =  bytscl( im[*, *, j, *], $
;                                max=max_in_time_int[j], $
;                                min=min_in_time_int[j] )
;    endfor
;endif

xsize =  (ntim+1)*panel_size[0]
  ; 1.004 factor is because window needs to be a teeny bigger. For large number of images in x
  ; dimension, last image was getting cut off on the right.  But need to keep xsize to figure
  ; out which image user clicked.
;xsize_win = xsize * 1.004
xsize_win = xsize

ysize =  (nen+1)*panel_size[1]

if not self->valid_roi(nen,ntim, exists=exists) then begin
	if exists then message, /cont, "Dimensions of current ROIs don't match data dimensions."
	self -> restore_roi, nen=nen, ntim=ntim, done=done
	if not done then begin
		if exists then message, /cont, $
			"Resetting ROIs to none."
		self.boxes = ptr_new(ptrarr(nen, ntim))
	endif
endif

wbase = WIDGET_BASE(/tlb_kill, group=group, title='SPEX Region Selector')

; Create the draw widget. The size of the drawable area is
  ; set equal to the dimensions of the image array using the

device, get_screen_size=scr_size
wdraw = WIDGET_DRAW(wbase, X_SCROLL_SIZE=(.9*scr_size[0] < xsize_win), Y_SCROLL_SIZE=(.8*scr_size[1] <  ysize), $
                     xsize =  xsize_win,  ysize = ysize,  /scroll, /button_events, $
                     event_pro='spex_roi_event', motion_events=0)

WIDGET_CONTROL, wbase, /REALIZE, xoffset=0, yoffset=0

; Retrieve the window ID from the draw widget.
WIDGET_CONTROL, wdraw, GET_VALUE=drawID

state = $

	{obj:self, $
	spex_obj:spex_obj, $
	colortable: colortable, $
	xsize:xsize, $
	xsize_win:xsize_win, $
	ysize:ysize, $
    ntim: ntim, $
    nen:nen, $
    t_idx:[0,0], $
    e_idx:[0,0], $
    wbase:wbase, $
    wdraw: wdraw, $
	drawid:drawid, $
	time_window:-1, $
	spec_window:-1, $
	im: im, $
	panel_size: panel_size, $
	elabels:elabels, $
	xrange_img: xrange_img, $
	yrange_img: yrange_img, $
	tlabels: tlabels, $
	choices: choices, $
	choices_short: choices_short, $
	single_choices: single_choices, $
	single_short: single_short, $
	multi_choices: multi_choices, $
	multi_short: multi_short $
	}

WIDGET_CONTROL, wbase, set_uvalue=state

self -> panel_draw, state, /show_selection

XMANAGER, 'spex_roi', wbase, /no_block

END
