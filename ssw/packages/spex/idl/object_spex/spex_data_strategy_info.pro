; Modifications:
; 16-Mar-2006, Kim.  Init spex_interval_filter

function spex_data_strategy_info

var = {spex_data_strategy_info}

var.spex_interval_filter = ptr_new(-1)
var.spex_eband = ptr_new(-1)

return, var

end