; fit_range object returns the element numbers in the data array to use for fitting.
; Modifications:
; 23-Aug-2005, Kim.  Keep track of previous_interval_index, and when current index changes
;  then force a call to process.  Exclude elements whose livetime is 0. in addition to
;  excluding elements that don't fall within the spex_erange values set by user.
; 15-Mar-2006, Kim.  In intervals_pre_hook, plot spectrum with /allint, /dim1_sum just in case user
;  has lots of interval - default shouldn't be to plot them separately as before
;--------------------------------------------------------------------------

function spex_fitrange::init, source=source, _extra=_extra

if not keyword_set( source ) then source = obj_new( 'spex_fitint', _extra=_extra )
;if not obj_valid( source ) then source = obj_new( 'spex_drm', _extra=_extra )

ret = self->framework::init( source=source, $
						control = spex_fitrange_control(), $
						_extra=_extra )

drm_obj = obj_new('spex_drm')
drm_obj -> set, spex_drm_dataobj = self -> get(/obj, class='spex_data')
self -> set, source=drm_obj, src_index=1

self.previous_interval_index = -1

if keyword_set(_extra) then self -> set, _extra=_extra

return, ret

end

;--------------------------------------------------------------------------

;pro spex_fitrange::set, $
;	spex_erange=erange, $
;	done=done, not_found=not_found, $
;	_extra=_extra
;print,'in spex_fitrange::set'
;help,erange
;help,_extra

;if exist(erange) then begin
;
;	energy_mids = self -> getaxis(/ct_energy)
;	if is_number(energy_mids[0]) then begin
;		eindex = indgen(n_elements(energy_mids))
;
;		if not (erange[0] eq -1 or same_data(erange, [0.,0.]) ) then begin
;			eind = self -> edges2index(interval=erange)
;			eindex = eindex[eind[0]:eind[1]]	; !!!! this just handles first erange, change for multiple
;		endif
;		self -> framework::set, spex_erange=erange, spex_index_erange = eindex, done=done, not_found=not_found
;	endif else message,'Error.  No input data selected.  Not setting spex_erange.', /cont
;
;endif

;if keyword_set(_extra) then self -> framework::set, _extra=_extra, done=done, not_found=not_found
;
;end

;--------------------------------------------------------------------------

function spex_fitrange::getdata, class_name=class_name, _extra=_extra

@spex_insert_catch

if keyword_set(class_name) then $
	return, self -> framework::getdata(class_name=class_name, _extra=_extra)

; This getdata should never be called without class_name unless we're actually working
; on an interval, which means spex_interval_index will be set to that interval
; and we want to get indices to use for that interval.
if self->get(/spex_interval_index) ne self.previous_interval_index then $
		self -> framework::set, /need_update, /this_class_only

;print, 'eindex = ', self -> framework::getdata(_extra=_extra)
return, self -> framework::getdata(_extra=_extra)

end

;--------------------------------------------------------------------------

; process sets the element numbers in the fitint data array to use for fitting,
; based on spex_erange, and ltime valid.

pro spex_fitrange::process, _extra=_extra
;print,'in spex_fitrange::process'

erange = self->get(/spex_erange)

energy_mids = self -> getaxis(/ct_energy)

if is_number(energy_mids[0]) then begin

	; First find elements that fall within erange intervals
	eindex = indgen(n_elements(energy_mids))
	if not (erange[0] eq -1 or same_data(erange, [0.,0.]) ) then begin
		nrange = n_elements(erange) / 2
		for ir = 0,nrange-1 do begin
			eind = self -> edges2index(interval=erange[*,ir])
			eindall = append_arr(eindall, eindex[eind[0]:eind[1]])
		endfor
		eindex = eindall
	endif

	; Also remove any elements whose livetime is 0 (that's a flag for no data)
	int = self->get(/spex_interval_index)
	fitdata = self->framework::getdata(class='spex_fitint')
	izero = where (fitdata.ltime[*,int] eq 0., count)
	if count gt 0 then begin
		elem = rem_elem (eindex, izero, countleft)
		if countleft gt 0 then eindex = eindex[elem] else message,'No points left to fit'
	endif
	self.previous_interval_index = int

	self -> setdata, eindex

endif else self -> setdata,-1

end

;--------------------------------------------------------------------------

pro spex_fitrange::intervals_pre_hook, $
	full_options=full_options, $
	intervals=intervals, $
	valid_range=valid_range, $
	title=title, $
	energy=energy, $
	type=type, $
	abort=abort, $
	_extra=_extra

; make sure data has been retrieved for current settings.  May be a better way?  !!!!
data = self -> getdata(class_name='spex_fitint')
if not is_struct(data) then abort = 1

if abort then return

energy = 1

intervals = self -> get(/spex_erange)

valid_range = minmax (self->get(/spex_ct_edges))

title='Select Energy Range(s) for Analysis'
type='Analysis'

if not keyword_set(full_options) then $
	if not self->valid_plot(/xyplot) then $
		;self -> plotman, spex_units='flux', class_name='spex_fitint'
		self -> plot_spectrum, /overlay_back, spex_units='flux', /allint, /dim1_sum;, this_interval=indgen(999)

end

;------------------------------------------------------------------------------

pro spex_fitrange::intervals_post_hook, erange, _extra=_extra

;if erange[0] eq -1 then self->set,spex_erange=-1 else begin
;	; only one erange allowed
;	if n_elements(erange) gt 2 then $
;		message,'Only one spex_erange allowed.  Using first interval you defined.', /cont
;	self -> set, spex_erange=erange[0:1]
;
;endelse

self -> set, spex_erange = erange

end

;------------------------------------------------------------------------------

pro spex_fitrange__define

struct = { spex_fitrange, $
		previous_interval_index: -1, $
		inherits spex_gen $
		}

end
