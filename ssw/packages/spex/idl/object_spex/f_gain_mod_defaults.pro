;+
; NAME:
;	f_gain_mod_defaults
;
; PURPOSE: Function to return default values for
;   parameters, minimum and maximum range, and free parameter mask when
;   fitting with gain_mod function.  gain_mod is a pseudo function - the function
;   value is always 0., but the params are used to adjust the gain and offset in the DRM.
;
; CALLING SEQUENCE: defaults = f_gain_mod_defaults()
;
; INPUTS:
;	None
; OUTPUTS:
;	Structure containing default values
;
; MODIFICATION HISTORY:
; Kim Tolbert, 12-Aug-2008
;
;-
;------------------------------------------------------------------------------

FUNCTION f_gain_mod_defaults

defaults = { $
  fit_comp_params:           [0.,  0.], $  ; gain delta, offset
  fit_comp_minima:           [-5., -5.], $
  fit_comp_maxima:           [5.,  5.], $
  fit_comp_free_mask:        [1b,  1b] $

}

RETURN, defaults

END