;------------------------------------------------------------------------------
pro fit_comp_control__define

struct = { fit_comp_control, $
  fit_comp_function:                     '', $	; Array of fit function component names
  fit_comp_params:                   ptr_new(), $	; Fit function parameters
  fit_comp_minima:                       ptr_new(), $	; Fit function parameter minimum values
  fit_comp_maxima:                       ptr_new(), $	; Fit function parameter maximum values
  fit_comp_free_mask:                    ptr_new() $	; Fit function parameter free/fixed mask
  }

end


