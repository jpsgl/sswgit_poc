;+
; Name: plot_goes_ospex
;
; Purpose: Plot goes .5-4 Angstrom channel overlaid on a count flux spectrum from any
;   data file that OSPEX can read.  Time offset for ospex file is a keyword input, so
;   this allows you to try different time offsets, and get a rough idea of the offset
;   that matches the goes data best.
;
; Calling sequence:  plot_goes_ospex

; Keywords:
; file - input file to read (if not passed in, a dialog will pop for you to choose)
; time_offset - seconds to offset ospex file by
; goes_factor - factor to multiply goes data by so plot will fall in range of
;   ospex plot. Default is 2.e9
; obj - ospex object (if don't pass this in, creates a new one and passes it out - use this
;   same obj as input for your next call if you're going to read the same file)
; _extra - any plot keywords to pass through to the plot command, e.g. timerange for
;   plotting a subset of times
;
; Examples:
;   plot_goes_ospex, file='D:\soxs\13-NOV-2003\raw_si_131103.out', obj=obj, time_offset=-95
;   plot_goes_ospex, obj=obj, timerange=['31-oct-04 05:20','31-oct-04 06:00']
;
; Notes:
;   1.  Since reading the soxs data takes a while, if you want to try different time offsets for
;   one soxs file, on each call (with different values of time_offset keyword), be sure to
;   pass in the same obj and file keywords so it doesn't have to read the file each time.
;   2.  If you don't see the GOES data on the plot, you may need to try different values of the
;   goes_factor keyword.  Or look in the IDL log - you may be having trouble reading the GOES
;   data.
;
; Written: Kim Tolbert October 4, 2006
;
;-


pro plot_goes_ospex, file=file, time_offset=time_offset, goes_factor=goes_factor, obj=o, _extra=_extra

if not is_class(o,'spex',/quiet) then o=ospex(/no_gui)

if not exist(file) then begin
	file = o->get(/spex_specfile)
	file = dialog_pickfile(file=file)
endif

checkvar, time_offset, 0
checkvar, goes_factor,2.e9


o->set,spex_specfile = file
o->set, spex_eband = [6,8]
o->set, spex_ut_offset = time_offset
o->plot_time, spex_units='flux', /no_plotman, _extra=_extra

file_time = minmax(o->getaxis(/ut, /edges_2))

goes_obj = ogoes()
goes_obj -> set, tstart = atime(file_time[0]), tend=atime(file_time[1])
g = goes_obj -> getdata(/struct)

linecolors
outplot, g.tarray + (anytim(g.utbase)-file_time[0]), g.yclean[*,1]*goes_factor, color=2

legend, ['RED - GOES * '+trim(goes_factor), 'Time_offset = ' + trim(time_offset)], box=0,/right,/top

end


