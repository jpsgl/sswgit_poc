pro konus_data, FILES=files, $
  DATA_STR=data_str, $
  ERR_CODE=err_code, $
  ERR_MSG=err_msg
  ;+
  ; Name:
  ;   KONUS_DATA
  ; Purpose:
  ;   This routine read a multi-spectrum pha-file (PHA-II, with time history)
  ;   obtained with the Konus-Wind gamma-ray spectrometer and fills the DATA_STR structure.
  ;   This routine is intended to be used by OSPEX to read Konus-Wind data
  ;   after the ospex control parameter spex_file_reader is set to the base name of the function:
  ;   IDL> o=ospex()
  ;   IDL> o->set, spex_file_reader='konus'
  ;   ('_data' will be appended to the base name)
  ;
  ;   The required routine arguments and output structure
  ;   are described in spex_any_specfile__define.pro (can be found in \ssw\packages\spex\idl\object_spex\)
  ;
  ; Method:
  ;   Read info about the response matrix from the header of the 'SPECTRUM' extension of the input pha-file.
  ;   Read the spectrum and response data; construct the response matrix.
  ;   Fill DATA_STR
  ;
  ; Calling sequence:
  ;   KONUS_DATA
  ;  Keywords:
  ;   FILES -  (INPUT) Scalar or vector string of file names to read
  ;   DATA_STR - (OUTPUT) Structure containing info read from file
  ;   ERR_CODE - (OUTPUT) 0 means success reading file(s). 1 means failure
  ;   ERR_MSG -  (OUTPUT) string containing error message if any.  '' means no error.
  ; Author and history:
  ;   26 Mar 2015: Original version by Alexey Kuznetsov.
  ;   17 Aug 2015: Modified by Valentin Pal'shin.
  ;   22 Aug 2015: Modified by Alexey Kuznetsov.
  ;   16 Sep 2015: Modified by Alexey Kuznetsov: correction for the propagation time added.
  ;   29 Aug 2016: Modified by Alexey Kuznetsov: background signal is read from a separate file (if present)
  ;                                              and appended at the end of the event

  data_str=-1
  err_code=0
  err_msg=''

  phafile=files[0]
  if ~file_test(phafile) then begin
    err_code=1
    err_msg='Cannot find the input PHA file '+phafile
    return
  endif

  stat=0

  sp=mrdfits(phafile, 'SPECTRUM', sp_header, status=stat)
  if stat then begin
    err_code=1
    err_msg='Cannot read the counts info from the file '+phafile
    return
  endif

  rmffile=fits_keyword_value(sp_header, 'RESPFILE')
  if rmffile eq '' then begin
    err_code=1
    err_msg='The input PHA file does not contain the response matrix link.'
    return
  endif

  rmffile=file_dirname(phafile)+'/'+rmffile
  if ~file_test(rmffile) then begin
    err_code=1
    err_msg='Cannot find the response RMF file '+rmffile
    return
  endif

  fits2rm, rmffile, $
    RM=drm, $
    EBINS=ph_edges, $
    DETBINS=edges_out, $
    EXT_HEADER=hdr, $
    ERR_CODE=ec, $
    ERR_MSG=em
  if ec then begin
    err_code=1
    err_msg='Error reading the response RMF file '+rmffile
    return
  endif

  arffile=fits_keyword_value(sp_header, 'ANCRFILE')
  if arffile eq '' then begin
    err_code=1
    err_msg='The input PHA file does not contain the ancillary response matrix link.'
    return
  endif

  arffile=file_dirname(phafile)+'/'+arffile
  if ~file_test(arffile) then begin
    err_code=1
    err_msg='Cannot find the ancillary response ARF file '+arffile
    return
  endif

  arm=mrdfits(arffile, 'SPECRESP', arm_header, status=stat)
  if stat then begin
    err_code=1
    err_msg='Cannot read the ancillary response info from the file '+arffile
    return
  endif

  t0time= fits_keyword_value(sp_header, 'T0TIME')
  t0earth=fits_keyword_value(sp_header, 'T0EARTH')
  if (t0time ne '') && (t0earth ne '') then begin
    tprop=double(t0earth)-double(t0time)
    print, 'Propagation time: ', tprop, ' s'
  endif else begin
    tprop=0d0
    print, 'Propagation time is unknown. The satellite time will be used.'
  endelse

  start_time=anytim(anytim(fits_keyword_value(sp_header, 'DATE_OBS'))+$
    anytim(fits_keyword_value(sp_header, 'TIME_OBS'))+tprop, /vms)
  end_time=  anytim(anytim(fits_keyword_value(sp_header, 'DATE_END'))+$
    anytim(fits_keyword_value(sp_header, 'TIME_END'))+tprop, /vms)

  rcounts=float(sp.counts)
  ercounts=sqrt(rcounts)

  s=size(rcounts)
  N_E=s[1]
  N_t=s[2]

  ut_edges=anytim(start_time)+[transpose(sp.tstart), $
    transpose(sp.tstop)]

  units='counts'

  area=97.5

  exposure=sp.exposure
  ltime=fltarr(N_E, N_t)
  for i=0, N_E-1 do ltime[i, *]=exposure

  data_name='Konus-Wind'
  title='Konus-Wind spectrum'
  detused=fits_keyword_value(sp_header, 'DETNAM')

  dim_drm=size(/dim, drm)
  drm=drm/area*rebin(transpose(arm.specresp), dim_drm)
  drm=drm/rebin(get_edges(edges_out, /width), dim_drm)

  backfile=fits_keyword_value(sp_header, 'BACKFILE')
  if backfile ne '' then begin
    backfile=file_dirname(phafile)+'/'+backfile
    if file_test(backfile) then begin
      print, 'Background data file found.'
      bk=mrdfits(backfile, 'SPECTRUM', bk_header, status=stat)
      if ~stat then begin
        bstart=float(fits_keyword_value(bk_header, 'TSTART'))
        bstop= float(fits_keyword_value(bk_header, 'TSTOP'))
        bltime=float(fits_keyword_value(bk_header, 'EXPOSURE'))
        bcounts=  float(fix(bk.rate*bltime+0.5))
        bercounts=bk.stat_err*bltime

        bk_t1=anytim(end_time)
        bk_t2=bk_t1+bstop-bstart

        end_time=anytim(bk_t2, /vms)
        rcounts=transpose([transpose(rcounts), transpose(bcounts)])
        ercounts=transpose([transpose(ercounts), transpose(bercounts)])
        ut_edges=transpose([transpose(ut_edges), transpose([bk_t1, bk_t2])])
        ltime=transpose([transpose(ltime), transpose(make_array(N_E, /float, value=bltime))])

        print, 'Signal after ', anytim(bk_t1, /vms), ' is the background taken from another event.'
      endif else print, 'Cannot read the counts info from the file '+backfile
    endif
  endif

  respinfo={$
    DRM: drm, $
    EDGES_IN: ph_edges, $ ;*
    EDGES_OUT: edges_out} ;*

  data_str={$
    START_TIME: start_time, $     ;start time of file in anytim format *
    END_TIME: end_time, $         ;end time of file in anytim format *
    RCOUNTS: rcounts, $           ;[nenergy,ntime] count data *
    ERCOUNTS: ercounts, $         ;[nenergy,ntime] error in count data *
    UT_EDGES: ut_edges, $         ;[2,ntime] edges of time bins in anytim format *
    UNITS: units, $               ;units of rcounts, usually 'counts' *
    AREA: area, $                 ;effective detector area in cm^2 *
    LTIME: ltime, $               ;[nenergy,ntime] livetimes *
    CT_EDGES: edges_out, $        ;[2,nenergy] edges of energy bins in keV *
    DATA_NAME: data_name, $       ;string name of data, e.g. 'RHESSI' *
    TITLE: title, $               ;string title of data, e.g. 'RHESSI SPECTRUM' *
    RESPFILE: respinfo, $         ;DRM etc. *
    detused: detused, $           ;string of detector names *
    atten_states: -1, $           ;[ntime] array of filter states, or scalar -1 if doesn't apply *
    deconvolved: 0, $             ;If 1, original data is already photons *
    pseudo_livetime: 0, $         ;if 1, livetime isn't a real livetime *
    xyoffset: [0, 0]}             ;[2] x,y location of source on Sun *
end