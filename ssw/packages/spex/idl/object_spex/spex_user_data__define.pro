;+
; Name: SPEX_USER_DATA__DEFINE
;
; Category:  OSPEX
;
; Purpose: Provide a mechanism for the user to set data arrays directly into OSPEX for fitting (normally
;   data is read from one of the allowed instrument FITS or other files).
;   To use this:
;    o -> set, spex_data_source = 'SPEX_USER_DATA'
;    o -> set, spectrum = spectrum_array,  $
;        spex_ct_edges = energy_edges, $
;        spex_ut_edges = ut_edges, $
;        errors = error_array, $
;        livetime = livetime_array
;    
;    where
;    spectrum_array - the spectrum or spectra you want to fit in counts, dimensioned n_energy, or n_energy x n_time.
;    energy_edges - energy edges of spectrum_array, dimensioned 2 x n_energy.
;    ut_edges - time edges of spectrum_array
;       Only necessary if spectrum_array has a time dimension.  Dimensioned 2 x n_time.
;    error_array - array must match spectrum_array dimensions. If not set, defaults to all zeros.
;    livetime_array - array must match spectrum_array dimensions.  If not set, defaults to all ones.
;    
;    You must supply at least the spectrum array and the energy edges.  The other inputs are optional.   
;    By default, the DRM is set to a 1-D array dimensioned n_energy of 1s, the area is set to 1., and 
;    the detector is set to 'unknown'.
;
; Written: Kim Tolbert
; Modifications:
;   1-Jul-2008, Kim.  Added doc header
;   29-Jul-2008, Kim. Changed kev to keV
;- 

;------------------------------------------------------------------------------

;pro spex_user_data_test
;
;  o =  obj_new( 'spex_user_data' )
;  o->set,  spex_spec =  '../ospex/hsi_spectrum*'
;  data =  o->getdata()
;
;end

;------------------------------------------------------------------------------

function spex_user_data::init,  source = source, _extra = _extra

return, self->spex_data_strategy::INIT()

end

;------------------------------------------------------------------------------
; Can't put this initialization stuff in the init method because we want to reinitialize
; every time we set strategy to spex_user_data.  The init method only gets run on the first
; creation of the spex_user_data strategy.

pro spex_user_data::initialize

dummy_time = anytim(['1-Jan-79 00:00','1-Jan-79 01:00'])
self -> framework::setdata, {data: -1, edata: 0., ltime: 1.}
self -> framework::set, spex_respinfo=1, spex_file_time=dummy_time, spex_ut_edges=dummy_time, spex_ct_edges=-1, $
	spex_area=1, spex_title='', spex_detectors='unknown', spex_specfile='', spex_data_pos = [-9999.,-9999.]

units_str = {spex_units}
units_str.data_name = 'Unknown'
units_str.data_type = 'counts'
units_str.data = 'counts'
units_str.drm = 'counts photons!u-1!n'
units_str.area = 'cm!u-2!n'
units_str.energy = 'keV!u-1!n'
units_str.time = 's!u-1!n'
units_str.ct_edges = 'keV'
units_str.ut_edges = 'UTC'
units_str.ph_edges = 'keV'

self -> setunits, units_str, /orig
self -> setunits, units_str

end

;------------------------------------------------------------------------------

pro spex_user_data::set, spectrum=spectrum,  errors=errors,  livetime=livetime,  $
	spex_ut_edges=spex_ut_edges, _extra=_extra, done = done, not_found = not_found

if keyword_set(spectrum) or keyword_set(errors) or keyword_set(livetime) then begin
	data_str = *self.data
	checkvar, sp, spectrum, data_str.data
	checkvar, er, errors, data_str.edata
	checkvar, lv, livetime, data_str.ltime

	sp_size = size(sp,/dim)
	er_size = size(er,/dim)
	lv_size = size(lv,/dim)

	if not same_data(sp_size, er_size) then begin
		message, /cont, "Spectrum and Errors arrays don't match.  Filling error array with error[0]"
		er = rebin([er[0]], sp_size)
	endif

	if not same_data(sp_size, lv_size) then begin
		message, /cont, "Spectrum and Livetime arrays don't match.  Filling livetime array with ltime[0]"
		lv = rebin([lv[0]], sp_size)
	endif

	self -> setdata, {data:sp, edata:er, ltime:lv}

endif

if keyword_set(spex_ut_edges) then $
	self -> framework::set, spex_ut_edges=spex_ut_edges, spex_file_time = minmax(spex_ut_edges)

if keyword_set(_extra) then self -> framework::set, _extra=_extra, done = done, not_found = not_found

end

;------------------------------------------------------------------------------

pro spex_user_data::process, _extra=_extra
if keyword_set(_extra) then self->set, _extra=_extra
end

;------------------------------------------------------------------------------

pro spex_user_data::read_data, $
                       spectrum,  errors,  livetime,  $
                       spex_respinfo, spex_file_time,  spex_ut_edges,  spex_ct_edges,  $
                       spex_area, spex_title, spex_detectors,  $
                       spex_interval_filter, spex_units, spex_data_name, $
                       spex_deconvolved, spex_pseudo_livetime, spex_data_pos, $
                       err_code=err_code

data_str = *self.data

spectrum =  data_str.data
errors =  data_str.edata
livetime =  data_str.ltime

spex_data_name = 'User Data'
input_struct = self -> get(/info)
@unpack_struct
spex_units = spex_data_units.data

err_code = 0

end

;------------------------------------------------------------------------------
pro spex_user_data__define

  self = {spex_user_data, $
          INHERITS spex_data_strategy }

END
