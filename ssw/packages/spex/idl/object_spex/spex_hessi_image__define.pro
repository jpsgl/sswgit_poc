;+
; Modifications:
; August-2005, Andre, Kim.  Lots of changes.  Inherits spex_image now.
; 10-Nov-2005, Kim.  If total in roi is negative then set spectrum and error for
;	that image to 0.  Also use max(abs(...)) for max noise and max signal.
; 9-Feb-2006, Kim.  Added spex_image_full_srm option.  Gets diagonal drm
;   and converts spectrum to counts instead of photons.  Added get_diag method.
;   Set spex_interval_filter from image_atten_state instead of just 0s since
;     we'll need filter states for srm if full_srm is set.
;   And moved ptr_valid(self.boxes) check out of loops over time and energy.
; 2-Mar-2006, Kim.  In compute_spectra corrected area used to convert to phot/sec
; 5-Apr-2006, Kim.  Corrected error in compute_spectra - was using midpoints for
;   image axes instead of start (left/bottom) for each pixel.  Also adjusted boxes
;   for limitation of polyfillv - it doesn't include a pixel unless center is to
;   right of boundary
; 26-May-2006, Kim.  Added spex_data_pos to args.
; 8-Aug-2006, Kim.  In read_data: Older imagecube files return a scalar
;   image_atten_state. We want one per time interval so rebin if necessary.
; 3-Nov-2006, Kim.  Fixed bug in compute_spectra setting ibox_start (Thanks to Pascal!)
; 13-May-2007, Kim.  After changes to hsi_image__plotman, changed select_roi method to
;	pass in e and t index, because new default is to put all images in plotman panels and
;	show the last one.
; 24-Aug-2007, Kim. Fixed plot_image - e_idx and t_idx are args to plotman, not keywords
; 03-Oct-2007, Kim. Set /use_single_return_mode at end of read_data method
; 29-Oct-2007, Kim. In plot_image method, previously allowed just one image to be plotted
;   (used 0th index).  Now create array of indexes to pass to plotman method.
; 10-Dec-2009, Kim. In compute_spectra method: Previously was accumulating spectrum from each
;   box separately, and then if user wanted boxes combined, I was adding them.  This was wrong for
;   overlapping boxes.  Also previously average the error when combining boxes. Now use max
;   of signal in all boxes selected and max outside of all boxes to determine error.
; 28-Jan-2010, Kim.  Fixed error in compute_spectra. Wasn't initializing tot_roi_use and tot_roi
;   so append_arr was using the previous values and adding to them.
; 25-Feb-2010, Kim. Added _extra to read_data.
; 19-Jan-2013, Kim. If image units are per keV (e.g. electron images), then multiple by energy bin
;  width to take the keV out before storing (when computing flux will divide by bin width)
; 11-Mar-2016, Kim. Changed max number of ROIs (n_boxes_max) to 64 from 4. GUI will only handle 6,
;   but user can set spex_roi_use from the command line.
; 02-Oct-2017, Kim. Added spectra_from_alg_parms method to set spectrum from rhessi image algorithm fit parameters.
;   Currently only works for VIS_FWDFIT image algorithm.
;
;---------------------------------------------------------------------------

pro spex_hessi_image_test

  o = ospex( /no )
  f = findfile( '../fits/cc*fits' )
  o -> set,  spex_spec =  f[2]
  print, o->get( /spex_spec )
  print, o->get( /im_in )
  o->set, spex_roi_infile = 'gaga_in.sav'
  o->set, spex_roi_outfile = 'gaga_out.sav'
  data = o->getdata()
  o->roi

  ; try with the new format
  o = obj_new( 'spex_hessi_image' )
  f = findfile( '../fits/cc*fits' )
  o -> set,  im_in =  f[2]
  print, o->get( /spex_spec )
  print, o->get( /im_in )
  data = o->getdata()

  ; try with the old format
  o = obj_new( 'spex_hessi_image' )
  o -> set,  im_in =  'test_cube_small.fits'
  print, o->get( /spex_spec )
  print, o->get( /im_in )

  spectra =  o->getdata()

end

;---------------------------------------------------------------------------

function spex_hessi_image::init,  source = source, _extra = _extra

  if not keyword_set( source ) then source = hsi_image()

  ret = self->spex_image::INIT( source = source,_extra = _extra )

  return, ret

end

;--------------------------------------------------------------------

pro spex_hessi_image::read_data, $
  image,  $
  spex_respinfo, spex_file_time,  $
  spex_ut_edges,  spex_ct_edges,  $
  spex_area, spex_title, spex_detectors,  $
  spex_interval_filter, spex_units, spex_data_name, $
  spex_deconvolved, spex_pseudo_livetime, spex_data_pos, $
  err_code=err_code, _extra=_extra

  err_code = 0

  source = self -> get( /source )

  image = source->getdata( use_single_return_mode = 0 )

  if size(image,  /n_dim) eq 0 then begin
    err_code = 1
    return
  endif

  spex_data_name = 'RHESSI Image Cube'
  ; since we're now converting spectra to photons, drm values should just be
  ; an array of ones for each energy bin
  ;spex_respinfo =  source -> get( /cbe_det_eff )

  spex_ut_edges = source->getaxis( /ut, /edges_2 )
  spex_file_time =  minmax( spex_ut_edges )
  spex_ct_edges =  float(source->getaxis( /energy, /edges_2 ))
  spex_respinfo = replicate( 1, n_elements( spex_ct_edges[0,*] ) )

  spex_area = hessi_constant( /detector_area ) ;  !!! wrong multiply by # det used
  spex_title =  'RHESSI Image Cube'

  str = source -> get(/det_index_mask, /a2d_index_mask, /front_segment, /rear_segment)

  spex_detectors = hsi_coll_segment_list(str.det_index_mask, $
    str.a2d_index_mask, $
    str.front_segment, $
    str.rear_segment)
  ndet = n_elements(str2arr(spex_detectors, ' '))

  spex_area = hessi_constant( /detector_area ) * ndet

  spex_units =  (source->get( /image_units ))[0]

  spex_interval_filter = reform((source->get( /image_atten_state ))[0,*])
  ; older imagecube files had just one image_atten_state, we want one per time interval
  ntimes = n_elements(spex_ut_edges[0,*])
  if n_elements(spex_interval_filter) lt ntimes then $
    spex_interval_filter = rebin([spex_interval_filter[0]],ntimes)

  spex_deconvolved = 1
  spex_pseudo_livetime = 1
  spex_data_pos = (source -> get (/used_xyoffset))[0:1]

  source -> set, /use_single_return_mode

end

;--------------------------------------------------------------------

pro spex_hessi_image::compute_spectra,  image, time_axis, energy_axis, $
  out_spectra,  livetime, errors;, full=full

  message, 'Computing spectra...', /cont

  source = self->get( /source )  ; source will be image object

  if self->get(/spex_image_use_alg_parms) eq 1 then begin
    ; Set spectrum into ospex from RHESSI image reconstruction parameters
    self->spectra_from_alg_parms, source, time_axis, energy_axis, out_spectra, livetime, errors, status=status
    if status then return
  end

  ntim = n_elements(time_axis[0,*])
  nen = n_elements(energy_axis[0,*])

  self->set, spex_image_spectrum_source='' ; not getting from image algorithm parameters, so blank

  dim = size( image, /dim )
  ny = dim[1]
  nx = dim[0]

  image = reform( image, nx*ny, nen, ntim )

  xaxis = source->getaxis( /xaxis, /edges_1 )
  yaxis = source->getaxis( /yaxis, /edges_1 )

  ; Allow up to 64 ROIs, but only the first 6 will be managed from the GUI.  From the command line, user can
  ; set spex_roi_use to the ones they want.
  n_boxes_max = 64

  if not self ->valid_roi(nen,ntim, exists=exists) then begin
    if exists then message, /cont, "Dimensions of current boxes don't match data dimensions."
    self -> restore_roi, nen=nen, ntim=ntim, done=done
    if not done then begin
      if exists then message, /cont, $
        "Resetting ROIs to none."
      free_var, self.boxes
    endif
  endif

  ;out_spectra, errors arrays will hold the spectra, errors for all selected boxes
  ;out_specta_box, errors_box arrays will hold spectra,errors for individual boxes
  ; Previously combined out_spectra_box at end for all selected boxes, but that included
  ; overlapping parts of ROIS twice. Keep accumulating the separate box spectra even though
  ; we don't need them now, just in case we want to use them in future.
  out_spectra = fltarr(nen, ntim)
  errors = fltarr(nen, ntim)
  out_spectra_box =  fltarr( nen,  ntim, n_boxes_max )
  errors_box = fltarr( nen, ntim, n_boxes_max ) ;+0.05

  diag = self->get(/spex_image_full_srm) ? self->get_diag(time_axis, energy_axis) : $
    intarr(nen,ntim)+1

  spex_roi_use =  self->get( /spex_roi_use )
  if self->get( /spex_roi_integrate ) then spex_roi_use = indgen(n_boxes_max)

  num_roi = self->get_num_roi(ntim=ntim,nen=nen)
  if min(spex_roi_use) gt (num_roi - 1) then begin
    message, /cont, 'Selected ROI(s) -  ' + trim(arr2str(spex_roi_use)) + ' not defined.  Max ROI defined is ' + $
      trim(num_roi-1) +'. Setting spectrum to 0.'
    goto, done
  endif

  any = 0
  if ptr_valid(self.boxes) then begin
    for it = 0,ntim-1 do begin
      for ie = 0,nen-1 do begin
        box = (*self.boxes)[ie,it]
        if ptr_exist(box) then begin
          any = 1
          boxes = *box
          nboxes = n_elements(boxes.nop) < n_boxes_max
          if nboxes eq 1 and boxes.nop[0] eq 0 then break
          max_of_signal_box = fltarr( nboxes )

          image[*,ie,it] = image[*,ie,it] * diag[ie,it]

          ; do all the defined boxes even if all weren't selected, so we can get max value outside all boxes
          tot_roi = -1
          tot_roi_use = -1
          for ibox = 0, nboxes-1 do begin
            ibox_start = ibox eq 0 ? 0 : ibox_end+1
            ibox_end = ( ibox_start + boxes.nop[ibox]-1 ) > 0
            these_xy_index = boxes.list[*, ibox_start:ibox_end ]
            pixels_x = value_locate( xaxis, these_xy_index[0, *] )>0
            pixels_y = value_locate( yaxis, these_xy_index[1, *] )>0
            ; We want to maximize size of box.  pollyfillv won't activate a box
            ; unless center lies to right of boundary, so
            ; for all boundaries to right of center, add a box
            ave = [avg(pixels_x), avg(pixels_y)]
            q = where (pixels_x - ave[0] gt 0)
            pixels_x[q] = pixels_x[q]+1
            q = where (pixels_y - ave[1] gt 0)
            pixels_y[q] = pixels_y[q]+1
            roi = polyfillv( pixels_x, pixels_y, nx, ny )

            ; tot_roi is cumulative unique roi for all defined boxes (for error calc)
            tot_roi = tot_roi[0] eq -1 ? roi : get_uniq( append_arr( tot_roi,  roi ) )

            ;tot_roi_use is cumulative unique roi for selected boxes
            if is_member(ibox, spex_roi_use) then $
              tot_roi_use = tot_roi_use[0] eq -1 ? roi :get_uniq(append_arr(tot_roi_use, roi))

            ;10-nov-05 added setting out_spectra_box and error_box to 0 if total_in_roi is negative
            ;  and added abs in max signal calculation
            total_in_roi = total( image[roi, ie, it ], 1 )
            if total_in_roi lt 0. then begin
              out_spectra_box[ie, it, ibox] = 0.
              max_of_signal_box[ibox] = 0.
            endif else begin
              out_spectra_box[ie, it, ibox] = total_in_roi
              max_of_signal_box[ibox] =  max(  abs( image[ roi, ie, it ] ), dim = 1 )
            endelse
          endfor  ; end of loop over boxes

          ; tot_roi_use is indices from all selected boxes. total_in_roi is sum of all pixels in selected boxes
          ; if no boxes selected, set to -1.
          total_in_roi = tot_roi_use[0] eq -1 ? -1. : total( image[tot_roi_use, ie, it ], 1 )
          if total_in_roi lt 0. then begin
            out_spectra[ie, it] = 0.
            max_of_signal = 0.
          endif else begin
            out_spectra[ie, it] = total_in_roi
            max_of_signal =  max(  abs( image[ tot_roi_use, ie, it ] ), dim = 1 )
          endelse

          ; this where_arr takes 1000 times longer than using the where below!
          ;                tot_roi_inverse =  where_arr( lindgen( nx*ny ),  tot_roi,  /notequal )

          ind_box = intarr(nx,ny)
          ind_box[tot_roi] = 1
          tot_roi_inverse = where(ind_box eq 0)

          max_of_noise = max( abs( image[ tot_roi_inverse, ie, it ] ) )  ;10-nov-05 added abs
          errors_box[ie,it,0:nboxes-1] = f_div( max_of_noise, max_of_signal_box )/3.
          errors[ie,it] = f_div(max_of_noise, max_of_signal)/3.

        endif else begin
          out_spectra[ie, it] = 0.
        endelse
      endfor ; end of loop over energies
    endfor ; end of loop over times
  endif

  if not any then message, /cont, 'No (or no valid) ROIs defined. All points in spectra set to 0.'

  done:

  ; 10-Dec-2009. This was wrong if boxes overlapped. Also shouldn't average the errors.
  ;if n_elements(spex_roi_use) gt 1 then begin
  ;	; get total of spectra and average of errors over roi dimension
  ;	out_spectra =  total( out_spectra_box[*, *, [spex_roi_use]],  3 )
  ;	; can't just use avg function because we don't want to average in the zeroes for
  ;	; unset regions.  So total the errors, then figure out how many defined regions
  ;	; there are in each image, and divide by that.
  ;	qnonzero = where (errors_box ne 0)
  ;	errset = byte(errors_box*0)
  ;	errset[qnonzero] = 1
  ;	errors = f_div ( total( errors_box[*,*, [spex_roi_use]], 3 ), $
  ;					total(errset[*,*,[spex_roi_use]], 3) )
  ;	;errors =  avg( errors[*, *, [spex_roi_use]], 2 )
  ;endif else begin
  ;	out_spectra =  out_spectra_box[*, *, spex_roi_use]
  ;	errors =  errors_box[*, *, spex_roi_use]
  ;endelse
  ;

  ; if image units are already per keV as they are for electron images, then multiple by energy bin width here to take keV out.
  image_units = source->get(/image_units)
  if strpos(image_units, 'keV!u-1') ne -1 then begin
    en_width = get_edges(energy_axis, /width)
    if nen gt 1 and ntim gt 1 then en_width = en_width # (fltarr(ntim)+1.)
    out_spectra = out_spectra * en_width
  endif

  errors = errors*out_spectra

  ; image data are in phot/cm^2/sec/asec^2.
  ; Richard suggests converting the spectra and error to photons (multiply by the interval duration,
  ; the detector area and the pixel_size area), and instead of setting the livetime to the
  ; interval duration, set it to whatever makes the error calculation used for fitting come
  ; out equal to the error calculated here.
  ; Error for fitting is sqrt (yfit_rate*ltime + bk.data) / ltime.  For images, bkrate is 0.  So to
  ; make the error equal to the image error calculated here (when yfit_rate equals image photon rate)
  ; 'livetime' needs to be (image photon rate) / (image error^2)

  ; first convert it to phot/sec because need out_spectra as rate for 'livetime' calculation
  source = self -> get( /source )
  pixel_size = source -> get(/pixel_size)
  ; areas will be asec^2 * cm^2.  changed from hessi_constant(/detector_area) to
  ; get(/spex_area) on 2-Mar-2006.  Wasn't taking into account area for multiple detectors.
  areas = pixel_size[0]*pixel_size[1] * self->get(/spex_area)
  out_spectra = out_spectra * areas
  errors = errors * areas
  livetime = f_div(out_spectra, errors^2)

  ; now convert out_spectra and errors to photons, using duration of time interval
  ;time_width =  transpose( reproduce( time_width,  (size( out_spectra,  /dim))[0] ) )
  ;out_spectra = out_spectra * time_width
  ;errors = errors * time_width

  ; that's wrong - use the weirdo livetime above so that in spex when get rate, and we divide
  ; by livetime, we'll get back the photon rate here
  out_spectra = out_spectra * livetime
  errors = errors * livetime

end

;--------------------------------------------------------------------------
; Gets values for spectrum and errors directly from image algorithm parameters (e.g. RHESSI's VIS FWDFIT or FWDFIT)
; (instead of getting spectra from regions in image)
; Called from compute_spectra method when spex_image_use_alg_parms is set

pro spex_hessi_image::spectra_from_alg_parms, imobj, time_axis, energy_axis, $
  out_spectra, livetime, errors, status=status

  status = 0

  if (hsi_alg_units(imobj->get(/image_alg))).name eq 'VIS FWDFIT' then begin

    area = self->get(/spex_area)
    diag = self->get_diag(time_axis, energy_axis)

    ntim = n_elements(time_axis[0,*])
    nen = n_elements(energy_axis[0,*])

    iuse = self->get(/spex_roi_use)
    integrate_roi = self->get( /spex_roi_integrate )
    out_spectra = fltarr(nen, ntim)
    errors = fltarr(nen, ntim)

    use_all_info_save = imobj->get(/use_all_info)
    use_single_return_save = imobj->get(/use_single_return)
    imobj->set, /use_all_info, use_single_return_mode=0
    vf_srcout = imobj->get(/vf_srcout)
    vf_sigma  = imobj->get(/vf_sigma)

    nreg = n_elements(vf_srcout) / (ntim*nen)
    if integrate_roi then iuse = indgen(nreg)
    iuse = get_uniq(iuse < (nreg-1))  ; iuse can't include regions that aren't there in vf_srcout
    nuse = n_elements(iuse)

    if nuse eq 1 then begin
      flux = nreg eq 1 ? vf_srcout.srcflux * diag : reform(vf_srcout[iuse,*,*].srcflux) * diag
      err_flux = nreg eq 1 ? vf_sigma.srcflux * diag : reform(vf_sigma[iuse,*,*].srcflux) * diag
    endif else begin
      ct_flux = fltarr(nuse, nen, ntim)
      ct_error = fltarr(nuse, nen, ntim)
      for ii=0,nuse-1 do begin
        ct_flux[ii,*,*]  = reform(vf_srcout[iuse[ii],*,*].srcflux) * diag
        ct_error[ii,*,*] = reform(vf_sigma[iuse[ii],*,*].srcflux) * diag
        ;    ct_error_rel = f_div(vf_sigma[iuse,*,*].srcflux, vf_srcout[iuse,*,*].srcflux) ; diag cancels out
      endfor
      flux = total(ct_flux, 1)
      err_flux = sqrt(total(ct_error ^ 2, 1))
      ;    err_flux = flux * sqrt(total(ct_error_rel ^ 2, 1))
    endelse

    rate = reform(flux) * area
    err_rate = reform(err_flux) * area
    livetime = f_div(rate, err_rate^2)

    out_spectra = rate * livetime
    errors =  err_rate * livetime

    imobj->set, use_all_info=use_all_info_save, use_single_return_mode=use_single_return_save

    self->set, spex_image_spectrum_source='VIS FWDFIT'

    status = 1

    self->set, spex_image_full_srm=1

  endif

  if status eq 0 then message, /info, 'Could NOT get spectrum from image algorithm parameters.'

end


;--------------------------------------------------------------------------

; returns diagonal srm for each energy, time of image
; Arguments are:
;  time_axis - (2,ntime) array of time intervals of images
;  energy_axis - (2,nen) array of energy edges of images
function spex_hessi_image::get_diag, time_axis, energy_axis

  if n_params() eq 0 then begin
    time_axis = self -> get(/spex_ut_edges)
    energy_axis = self -> get(/spex_ct_edges)
  endif

  atten = self -> get(/spex_interval_filter)
  atten_u = get_uniq(atten)
  natten = n_elements(atten_u)

  index = hessi_id2index( str2arr(self->get(/spex_detectors), ' ') )
  use_vird = bytarr(18)
  use_vird[index] = 1b

  simplify = 2+bytarr(10)

  ;en = get_edges(energy_axis, /edges_1)
  ;nen = n_elements(en)-1

  nen = n_elements(energy_axis[0,*])

  ; convert 2xn energy bands to 1d array.  Have to be contiguous for hessi_build_srm, so
  ; make contiguous bands, and then just use the values for the bands we really have
  en = reform(energy_axis, n_elements(energy_axis))
  en = get_uniq(en)

  ntime = n_elements(time_axis[0,*])
  date = anytim(time_axis[0,0])

  srm_diag = fltarr(nen, natten)

  ; q will be the array of indices in en (and srmi) of the energy bands we want.
  q = reform( value_locate(en, energy_axis[0,*]) )

  for i=0,natten-1 do begin
    hessi_build_srm, en, use_vird, srmi, $
      geom, atten=atten_u[i], /diag, time_wanted=date, simplify=simplify
    srm_diag[*,i] = srmi[q]
  endfor

  srm_diag_int = fltarr(nen, ntime)
  for i=0,ntime-1 do begin
    this_atten = where(atten[i] eq atten_u)
    srm_diag_int[*,i] = srm_diag[*,this_atten]
  endfor

  return, srm_diag_int

end

;--------------------------------------------------------------------------

function spex_hessi_image::select_roi_hook, t_idx, e_idx, $
  nop = nop, list = list, cancel=cancel, colortable=colortable

  ;these ones are hessi dependent, that's why they are here.

  ;---- this need to be defined in any classes, such that we get the correct image in plotman
  source = self -> get(/source)
  source -> set, /use_single_return_mode
  source -> set, t_idx = t_idx
  source -> set, e_idx = e_idx
  ; possibly we could assume that plotman is the plotter to select boxes.
  ; in this case we could move this to spex_image
  source -> plotman, e_idx, t_idx, plotman_obj=pobj;, colortable=colortable

  boxes = pobj -> mark_box (nop=nop, list=list, cancel=cancel)

  return, boxes

end

;----------
; this method is used by the ROI selector, but is specific to the type of
; image cube data.
; state is the widget uvalue structure from the ROI selector

pro spex_hessi_image::plot_image, spex_obj=spex_obj, $
  t_idx=t_idx, e_idx=e_idx, colortable=colortable

  pobj = spex_obj->get(/spex_plotman_obj)

  source = self->get(/source)
  nei = e_idx[1] - e_idx[0]
  ei = nei eq 0 ? e_idx[0] : e_idx[0] + indgen(nei+1)
  nti = t_idx[1] - t_idx[0]
  ti = nti eq 0 ? t_idx[0] : t_idx[0] + indgen(nti+1)
  source -> plotman, ei, ti, $
    plotman_obj=pobj;, colortable=colortable

end

;--------------------------------------------------------------------------

pro spex_hessi_image__define

  dummy = {spex_hessi_image, $
    inherits spex_image }

end
