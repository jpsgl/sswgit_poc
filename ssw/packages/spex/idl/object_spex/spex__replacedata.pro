;+
; Project     : HESSI
;
; Name        : replacedata method for ospex
;
; Purpose     : After reading data from a FITS file, replace original or background data counts,
;	error, or livetime in objects with user data.
;
; Method      : The data object (original data) and the background object both return a structure
;	on a getdata call that contains the following tags:
;	  struct.data - counts data
;	  struct.edata - error in data
;	  struct.ltime - livetime in seconds
;	This method replaces any of those three structure tags in either the original or bk data, and
;   then writes the structure back into the object (via a call to setdata).
;
; Category    : spex
;
; Syntax      : IDL> o -> replacedata, new, [/orig, /bkdata, /counts, /error, /ltime]
;
; Inputs      : new - user data to put in objects.  new must be in units of counts. (In ospex,
;	fitting is done in units of rate, i.e. struct.data will be divided by struct.ltime, so even
;	if you don't put correct livetimes in, you really just need to make everything consistent.)
;	If new is a scalar, then every element in the array
;	(entire array, or array for selected interval) is set to that scalar value.
;   Otherwise, if interval keyword is set, then new array must have same dimension
;	as a single interval in the structure tag you're replacing.  If interval keyword
;	is not set, then new array must have the same dimensions as the tag you're replacing.
;
; Example:  Suppose original data's getdata structure looks like:
;	data[1600,5]
;	edata[1600,5]
;	ltime[1600,5]
;	To replace interval 0 data with your own array new[1600]:
;		o -> replacedata, new, /orig, /counts, interval=0
;	To replace all livetimes with 1.:
;		o -> replacedata, 1., /orig, /ltime
;	To replace all counts data, new must be dimensioned [1600,5]:
;		o -> replacedata, new, /orig, /counts
;
; Keywords:
;	interval - Time Interval number to replace.
;	orig - If set, replace an item in original data structure (data from spex_data object)
;	bkdata - If set, replace an item in background structure (data from spex_bk object)
;	counts - If set, replace .data tag in structure
;	error -  If set, replace .edata tag in structure
;	ltime -  If set, replace .ltime tag in structure
;
; History     : Written by Kim Tolbert, June 2004
;
; Contact     : kim.tolbert@gsfc.nasa.gov
;
;-
pro spex::replacedata, new, interval=interval, $
	orig=orig, bkdata=bkdata,  $
	counts=counts, error=error, ltime=ltime

checkvar, orig, 0
checkvar, bkdata, 0

checkvar, counts, 0
checkvar, error, 0
checkvar, ltime, 0

case 1 of
	orig:  obj = (self->get(/obj,class='spex_data')) -> getstrategy()
	bkdata: obj = self->get(/obj,class='spex_bk')
	else: begin
		message, 'You did not set keyword orig or bkdata.  Aborting replace.', /cont
		return
	end
endcase

current = obj -> getdata()
if n_elements(new) gt 1 then begin
	if exist(interval) then begin
		if not same_data(size(current.data[*,0], /dim), size(new, /dim)) then begin
			message, 'New data has different dimension from a single interval in current data.  Aborting replace.', /cont
			return
		endif
	endif else begin
		if not same_data(size (current.data,/dim), size(new,/dim)) then begin
			message, 'New data has different dimensions from current data.  Aborting replace.', /cont
			return
		endif
	endelse
endif

case 1 of
	counts: if exist(interval) then current.data[*,interval] = new else current.data = new
	error: if exist(interval) then current.edata[*,interval] = new else current.edata = new
	ltime: if exist(interval) then current.ltime[*,interval] = new else current.ltime = new
	else: begin
		message, 'You did not set keyword counts, error, or ltime.  Aborting replace.', /cont
		return
	end
endcase

obj -> setdata, current

end


