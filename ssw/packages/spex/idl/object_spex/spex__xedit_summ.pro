;+
; Name: ospex::xedit_summ
; 
; Purpose:  Widget to allow user to select a spex_summ param to edit, and pop up a table widget for user
;  to edit values.  Spex_summ... values in object are modified only when user hits Accept.  For any time 
;  interval that was modified, the corresponding chisquare value in spex_summ is set to 0.
;  
; Calling Sequence:
;   o->xedit_summ[, summ=summ]
;   
; Keyword arguments: 
;   summ - spex_summ structure from ospex.  If not passed in, it's retrieved from the object.
;      
; Written: Kim Tolbert, 17-Sep-2012
; Modifications:
;-

pro xedit_summ_event, event
widget_control, event.top, get_uvalue=state
if obj_valid(state.object) then state.object->xedit_summ_event,event
return & end

;-----

pro spex::xedit_summ_event, event

widget_control, event.top, get_uvalue=state

widget_control, event.id, get_uvalue=uvalue

if not exist(uvalue) then return

exit = 0
case uvalue of
  'edit':  begin
    sel_item = widget_info(state.wlist, /list_select)
    widget_control, state.wsize, get_value=size
    new = spex_edit_summ(state.summ, state.item_tags[sel_item], xsize=size[0], ysize=size[1], status=status)
    if status then begin
      state.summ = new
      state.changed = 1
      widget_control,event.top, set_uvalue=state
    endif
    end
    
  'cancel': begin
    exit = 1
    state.changed = 0
    summ = state.orig_summ
    end
    
  'accept': begin
    exit = 1
    summ = state.summ
    end
    
  else:
endcase

if exit then begin
;  message,/info,'state.changed = ' + trim( state.changed)
  if state.changed then begin
    ;find all of the intervals with changed values so we can set the chisq to that interval to 0.
    dif=compare_struct(summ,state.orig_summ)
    if dif[0].ndiff gt 0 then begin
      for i=0,n_elements(dif)-1 do begin
        dims = size(summ.(dif[i].tag_num_a), /dim)
        ncolumns = n_elements(dims) eq 1 ? 1 : dims[0]
        q = where (summ.(dif[i].tag_num_a) ne state.orig_summ.(dif[i].tag_num_b))        
        int_changed = append_arr(int_changed, q / ncolumns)
      endfor
      int_changed = get_uniq(int_changed)
      print, 'int_changed = ', int_changed
      summ.spex_summ_chisq[int_changed] = 0.
      self -> set, _extra = summ
      message,/info,'Setting spex_summ... parameters into object.'
    endif else message,/info,'No values were changed.'
  endif
  widget_control, event.top, /destroy
endif

end

;-----

pro spex::xedit_summ, summ=summ, group=group, gui_label=gui_label

if xregistered ('xedit_summ') then begin
  xshow,'xedit_summ', /name
  return
endif

if ~keyword_set(summ) then summ = self->get(/spex_summ)

if ptr_chk(summ.spex_summ_fit_function) then begin
  message,/info,'No spex_summ parameters have been stored yet.  Aborting.'
  return
endif

checkvar, gui_label, ''

get_font, font, big_font=big_font

items = ['Fit Parameters', $
  'Minimum Allowed for Fit Parameters', $
  'Maximum Allowed for Fit Parameters', $
  'Free/fixed Mask', $
  'Energy Range', $
  'Energy Bin Mask', $
  'Function Spectrum', $
  'Function Model', $
  'Maximum # Iterations', $
  'Systematic Uncertainty']

nitems = n_elements(items)
nlen = max(strlen(items))
 
item_tags = ['spex_summ_params', $
  'spex_summ_minima', $
  'spex_summ_maxima', $
  'spex_summ_free_mask', $
  'energy_range', $
  'spex_summ_emask', $
  'spex_summ_func_spectrum', $
  'spex_summ_func_model', $
  'spex_summ_maxiter', $
  'spex_summ_uncert']
  
wbase = widget_base(group=group, title='SPEX Edit Summ Values '+gui_label, $
  /column, xpad=15, ypad=15, space=10)


tmp = widget_label (wbase, value='Edit SPEX_SUMM Values', $
  font=big_font, /align_center)

wtextbase = widget_base(wbase, /column, space=0)
tmp = widget_label(wtextbase, value='')
tmp = widget_label(wtextbase, value='This widget allows you to edit')
tmp = widget_label(wtextbase, value='  the stored fit results in the spex_summ...')
tmp = widget_label(wtextbase, value='  parameters.  The new values are not')
tmp = widget_label(wtextbase, value='  set in the OSPEX object until you click')
tmp = widget_label(wtextbase, value='  the Accept button.')
tmp = widget_label(wtextbase, value='')
tmp = widget_label(wtextbase, value='The chi-square value for intervals')
tmp = widget_label(wtextbase, value='  that were changed will be set to 0.')
 
  
  
wbase1 = widget_base(wbase, /column, /frame)

tmp = widget_label(wbase1, value='Select item to edit:', /align_left)
  
wlist = widget_list (wbase1, $
          frame=5, $
          ysize=nitems, $
          xsize=nlen, $
          value=items, $
          uvalue='items')
widget_control, wlist, set_list_select=0

wsize = cw_range(wbase,   value=[20,20], label1='# Columns', label2='# Rows', uvalue='size', /frame)

wbuttonbase = widget_base(wbase, /row, space=15)
tmp = widget_button(wbuttonbase, value='Edit Selected Item', uvalue='edit')
tmp = widget_button(wbuttonbase, value='Cancel', uvalue='cancel')
tmp = widget_button(wbuttonbase, value='Accept', uvalue='accept')

state = { $
  wbase: wbase, $
  wlist: wlist, $
  wsize: wsize, $
  object: self, $
  items: items, $
  item_tags: item_tags, $
  changed: 0, $
  summ: summ, $
  orig_summ: summ}
  
widget_control, wbase, set_uvalue=state

widget_offset, group, newbase=wbase, xoffset, yoffset

widget_control, wbase, xoffset=xoffset, yoffset=yoffset

widget_control, wbase, /realize
  
xmanager, 'xedit_summ', wbase, /no_block
end
        