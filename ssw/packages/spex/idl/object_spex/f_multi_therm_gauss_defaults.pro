function f_multi_therm_gauss_defaults
;+
; NAME:
;	F_MULTI_THERM_GAUSS_DEFAULTS
;
; PURPOSE: Function to return default values for
;   parameters, minimum and maximum range, and free parameter mask when
;   fitting to f_multi_therm_gauss.
;
; CALLING SEQUENCE: defaults = f_multi_therm_gauss_defaults()
;
; INPUTS:
;	None
; OUTPUTS:
;	Structure containing default values
;
; MODIFICATION HISTORY:
; Andrew Inglis, 12-Jul-2012, first created.
; Andrew Inglis, 13-Aug-2012, edited limits so that the call to f_vth is always ok
;
;-
;------------------------------------------------------------------------------

  defaults = { $
  fit_comp_params:           [1.0,   6.5, 0.3], $
  fit_comp_minima:           [1e-8, 6.0, 0.01], $
  fit_comp_maxima:           [1e8,  8.0,  1.0], $
  fit_comp_free_mask:        [1b,    1b,   1b] $
             }

  return, defaults
end
