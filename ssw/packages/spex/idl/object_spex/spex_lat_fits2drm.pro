;+
;
; NAME:
; spex_lat_fits2drm
;
; PURPOSE:
;   Read a Fermi LAT response matrix from a FITS file and return information of
;   interest to OSPEX.
;
;
; CATEGORY:
;       SPECTRAL FITTING, SPEX - HESSI
;
; CALLING SEQUENCE:
;
; CALLS:
;   fits2rm, fxpar
;
; INPUTS:
;
; OUTPUTS:
;
; INPUT KEYWORDS:
;   FILE - name of FITS file to read.
;   SFILE - not used.  Included for Spex comptability.
;
; OUTPUT ARG:
;   drm_str - structure with drm info
;     If there are multiple attenuator states, the DRM and ATTEN_STATE items
;     in the structure are dimensioned to the # of atten states.
;
; PROCEDURE:
;
; Written Kim 26-Feb-2009.  Based on spex_gbm_fits2drm
;
; Modification History:
;
;-
;------------------------------------------------------------------------------
pro spex_lat_fits2drm, FILE=file, $
                       SFILE=sfile, $
                       drm_str, $
                       ERR_CODE=err_code, $
                       ERR_MSG=err_MSG
                       
drm_str = -1

fits2rm, file[0], $
         RM=drm, $
         EBINS=ph_edges, $
         DETBINS=edges_out, $
         EXT_HEADER=hdr, $
         ERR_CODE=err_code, $
         ERR_MSG=err_msg

IF err_code THEN RETURN

data_name = get_fits_instr(hdr)

area = 8000.

;matrix_ext = get_fits_extno(file, 'specresp matrix')
;nmatrix = n_elements(matrix_ext)
;drm = reproduce(drm,nmatrix)
;times = dblarr(2,nmatrix)
;times[*,0] = anytim([fxpar(hdr,'date-obs'), fxpar(hdr,'date-end')])
detectors = fxpar(hdr, 'DETNAM')

;if nmatrix gt 1 then begin
;  for i=1,nmatrix-1 do begin
;    rm_struct = mrdfits(file[0], matrix_ext[i], head, /silent)
;    drm[*,*,i] = rm_struct.matrix
;    times[0,i] = anytim(fxpar(head, 'date-obs'))
;    times[1,i] = anytim(fxpar(head, 'date-end'))
;  endfor
;endif

dim_drm = size(/dim, drm)

; drm for each time interval is for start time of interval.  Shift times so that mid-point of time
; intervals is time of drm (makes later software for combining drms for fit time intervals easier)
;w = get_edges(times, /width)
;tstart = reform(times[0,*])
;times = get_edges([tstart-w/2., tstart[nmatrix-1]+w[nmatrix-1]/2.], /edges_2)

; Divide drm by area and output energy widths, since spex_drm::process will multiply by them
drm_str = { $
  EDGES_OUT: edges_out, $
  PH_EDGES: ph_edges, $
  AREA: area, $
  DRM: drm / area / rebin( get_edges( edges_out, /width), dim_drm ), $
  SEPDETS: 1, $
  data_name: data_name, $
  filter: -1, $
  detused: detectors };, $
;  times: times }

END
                       