;+
; Name: SPEX_FITALG_GEN__DEFINE
;
; Purpose: This object manages the fit algorithms for OSPEX.
;
; Category: OSPEX
;
; Calling sequence:
;
; Modifications:
;   15-Jul-2004, Kim.  Set spex_drm_current_filter before starting fits. And in process
;      method, change ->getdata to ->framework::getdata
;	6-Oct-2004, Kim.  Change process method to use spex_fit_time_used instead of
;	   spex_fit_time_interval
;	18-Jul-2006, Kim.  Fixed error in process method when calculating errors to use for weighting
;		in fit.  In process, when spex_error_use_expected is set, calc error based
;		on initial params, then loop to iterate, using params after each fit to calculate new
;		errors.
;	31-Jul-2006, Kim. Moved message about re-entering fit procedure with new errors
;	8-Aug-2006, Kim.  Added spex_fit_obj property, and set and get methods so we can
;	  set and get it.  Now fitrange...down to data are not in object chain, so wherever
;	  we were using self before to get at something in that area, now use spex_fit_obj.
;	  Also, use f_div when dividing by ltime in process.
;	  Also, pass loop_index to process_hook in process.
; 23-Apr-2008,  Kim. Took spex_fit_obj keyword out of get method (caused crash when
;   did o->get(/spex_fit) ) and made separate get_spex_fit_obj method.
; 09-Dec-2009, Kim.  In process method, fix error that gets passed into mucurvefit.  Previously 
;   get and binned bk data, and used sqrt(counts) as error in bk.  Now use bk data and error 
;   in structure returned by spex_fitint getdata, which is already binned. This bk error was
;   averaged (depending on value of spex_bk_poisson_error) instead of being based on counts in
;   binned interval.  This makes the errors bigger, and chisq from fit smaller.
; 14-Dec-2009, Kim. Corrected bug introduced by 9-dec fix.  Set ebkrate to 0. when no bk defined.
; 07-Jul-2010, Kim. Set spex_drm_current_time in process
; 24-Sep-2010, Kim.  Changed calc of weights (1/error^2) used in fitting.  Previously weren't adding bk counts
;   to total counts used in error.  Now use total counts (if use_expected is set, that's total obs counts,
;   if not, it's the model counts plus bk) and add error on that to error in bk in quadrature.
;   Also added quiet and use_errors keywords to process (needed for when we're running chi2 mapping)
; 12-Jan-2011, Kim. In chi2 mapping, when pass in use_errors to process method, use_errors already 
;   includes the uncert component.  I was adding that in again.  Moved setting errors to after 
;   adding uncert contribution.
; 18-Mar-2011, Kim. In process, when use_expected is 0, use eobsdata, not sqrt(obsdata) combined
;   with bk error.  eobsdata is often same as sqrt(obsdata), but not always.
;
;-

;------------------------------------------------------------------------------

pro spex_fitalg_gen::set, spex_fit_obj=spex_fit_obj, done=done, not_found=not_found, $
	_extra=_extra

if keyword_set(spex_fit_obj) then self.spex_fit_obj = spex_fit_obj

self->framework::set, _extra=_extra, done=done, not_found=not_found
end

;------------------------------------------------------------------------------

function spex_fitalg_gen::get, _extra=_extra, found=found, not_found=not_found
return, self->framework::get(_extra=_extra, found=found, not_found=not_found)
end

;------------------------------------------------------------------------------

function spex_fitalg_gen::get_spex_fit_obj
return,self.spex_fit_obj
end

;------------------------------------------------------------------------------

function spex_fitalg_gen::getdata, done=done, _extra=_extra

@spex_insert_catch

if keyword_set( _extra ) then self->set, _extra=_extra

return, self->framework::getdata( _extra = _extra, done=done )

end

;------------------------------------------------------------------------------
; use_errors - pass in errors (for weights) to use when want to keep weights fixed (e.g. for chi2 mapping)
; quiet - if set, don't print informative messages.  Default is 0.

pro spex_fitalg_gen::process, use_errors=use_errors, quiet=quiet, _extra=_extra

checkvar, quiet, 0

interval_index = self.spex_fit_obj -> get(/spex_interval_index)

; We'll pass rate data to curve fitting routine.  That way drm calc doesn't need to know about livetimes.
data = self.spex_fit_obj -> framework::getdata(class='spex_fitint', spex_units='rate')

; Next step no longer necessary, 8-Dec-09. bk in fit interval now in data structure we just got.
; get background we subtracted from data so that we can use the sqrt(counts) for errors
; bk is data for every energy, every time bin.  Bin it into the current fit time interval.
; if no background defined, bk.data will be -1, so set bkbinned to an array of 0's
;bk = self.spex_fit_obj -> getdata(class='spex_bk', spex_units='counts')
;fit_times = self.spex_fit_obj->get(/spex_fit_time_used)
;bkbinned = bk.data[0] eq -1 ? fltarr(n_elements(data.data[*,0])) : $
;	(self.spex_fit_obj->get(/obj,class='spex_bk')) -> bin_data (data=bk, intervals=fit_times[*,interval_index], /do_time)

; eindex will be energy indices into ct_edges to use in fit.  If not defined, use all.
eindex = self.spex_fit_obj -> getdata(class='spex_fitrange')
if eindex[0] eq -1 then eindex = indgen(n_elements(data.data[*,0]))

; Make sure drm stuff has been set by doing getdata, then set filter for current interval and drm 
; photon edges into fit function xvals so that in fit eval routine, function will be at ph edges
drm = self.spex_fit_obj -> getdata(class='spex_drm')  ;!!!! just to initialize the drm object
self.spex_fit_obj -> set, spex_drm_current_filter=self.spex_fit_obj->get_fitint_filter()
self.spex_fit_obj -> set, spex_drm_current_time=self.spex_fit_obj->get_fitint_time()

;print,'setting spex_drm_current_filter to ' + trim(self -> get(/spex_drm_current_filter))
self -> set, fit_xvals = self.spex_fit_obj -> get(/spex_drm_ph_edges)

ct_energy = self.spex_fit_obj -> get(/spex_ct_edges)

use_expected = self -> get(/spex_error_use_expected)

; Compute errors on data.
; If spex_error_use_expected is set, then use error in expected counts (from function), otherwise
; use error in data (data.edata).  In either case, combine in quadrature with error in bk.
; Then combine that error with the systematic error (value of spex_uncert*data values).
; If using expected counts, then when params are not a good fit, errors are wrong.  So iterate 3 times
; (or until old_chi-new_chi/old_chi < 1.e-3) by calculating errors again using result of previous fit, and 
; doing fit again. 

ltime = data.ltime[*,interval_index]
;if not use_expected then errors = f_div (sqrt ( (data.edata*ltime)^2 + bkbinned), ltime) ;use f_div, 8-aug-06
; data structure now has background in fit interval 8-Dec-009
bkrate = data.bkdata[0] eq -1 ? 0. : data.bkdata[*,interval_index]
ebkrate = data.ebkdata[0] eq -1 ? 0. : data.ebkdata[*,interval_index]

;if not use_expected then errors = sqrt(data.edata[*,interval_index]^2 + ebkrate^2)
;; 24-Sep-2010 -That was wrong - if calculating error for weights from observed instead of model, was
;; using bk-subtracted counts, should use total counts 
;if ~use_expected then begin
;  obsrate = data.obsdata[*,interval_index ]
;  ; error in total counts squared is (sqrt(obsrate*ltime) / ltime)^2 = obsrate/ltime
;  ; add error in bk to that, in quadrature
;  errors = sqrt(f_div(obsrate, ltime) + ebkrate^2)
;endif
; 17-Mar-2011 - That was wrong too.  Should use eobsdata - it isn't always the
; same as sqrt(total counts) (e.g. hessi data from FITS file, 'SPEX_USER_DATA' data, or replacedata)
if ~use_expected then errors = sqrt(data.eobsdata[*,interval_index]^2 + ebkrate^2)

nloop = use_expected ? 3 : 0
if keyword_set(use_errors) then nloop = 0
check = 1.e-3
;print, 'nloop = ', nloop

for iloop = 0,nloop do begin
	;print,'iloop = ', iloop
	if use_expected then begin
		yfit = self -> getdata(class_name='fit_function', fit_comp_params=params)
		yfit = self.spex_fit_obj -> convert_fitfunc_units (yfit, spex_units='rate')  ; will be in counts / sec
		; Ssqrt ( error squared + error in bk counts squared) is just sqrt(expected counts + bk counts).
		;errors =  f_div(sqrt (yfit*ltime + bkbinned), ltime) ;use f_div, 8-aug-06
		; Error in model rate is sqrt(yfit*ltime) / ltime.  Square of that is yfit/ltime. Replaced above line 9-Dec-09
;		errors = sqrt( f_div(yfit,ltime) + ebkrate^2)

        ;changed the above 23-Sep-2010.  Need to include bk counts when take sqrt of counts, and then add whatever error
        ; is in background (due to calculations) in quadrature.  
        ; Total counts is (yfit+bkrate)*ltime, error in rate space is square root of that divided by ltime, so
        ; error in rate including background counts = sqrt(f_div(yfit + bkrate, ltime))
        ; square that, add square of bk rate error, then take square root
        errors = sqrt (f_div(yfit + bkrate, ltime) + ebkrate^2)

	endif

	; We can combine errors with systematic error either by taking the greater of the calculated error
	; and the spex_uncert * the data, or by summing in quadrature. We'll do the latter, since
	; it matches old spex better.

	uncert = (self->get(/spex_uncert) * data.data[*,interval_index])
	errors = sqrt(errors^2 + uncert^2)

  ; if passed in errors, use them regardless of how we calculated errors above (uncert has
  ; already been added into use_errors) 
  if keyword_set(use_errors) then errors = use_errors
  
	; doesn't matter what energy we pass in here,  drm needs to work with full energy array, which
	; has been set into fit_xvals and will be used in the eval routine.  Results will be at
	; ct_energy energies

	if iloop gt 0 and ~quiet then print,'Re-enter fit procedure with errors calculated from best-fit parameters... '
	self->process_hook, ct_energy, data.data[eindex,interval_index], f_div(1.,errors[eindex]^2), $
		result=result, params=params, sigmas=sigmas, loop_index=iloop, quiet=quiet

	self -> set, $
	  fit_comp_params=params, $
	  fit_comp_sigmas=sigmas

	new_chisq = self -> getalg(/chisq)
	if iloop gt 0 then begin
		;print, 'old, new, change = ', old_chisq, new_chisq, abs(old_chisq - new_chisq) / old_chisq
		if abs(old_chisq - new_chisq) / old_chisq lt check then break
	endif
	old_chisq = new_chisq

endfor


; calculate residuals = (data - fit) / error in data
; !!! which error should this be?  edata or errors in expected counts (errors from above)

resid = f_div( data.data[eindex,interval_index] - result, errors[eindex] )

self -> setdata, {fit_model: result, $  ; model in count rate units (at eindex energies)
	fit_xdata: ct_energy[*,eindex], $	; eindex energies used in fitting
	fit_ydata: data.data[*,interval_index], $	; count rate data (all energies) used in fit
	fit_yerror: errors, $	; error in count rate data (all energies) used in fit
	fit_resid: resid}	; residuals (at eindex energies)

units_str = self.spex_fit_obj -> get(/spex_fitint_units)

;self -> set, spex_fit_units=units_str

end

;------------------------------------------------------------------------------

pro spex_fitalg_gen__define

struct = { spex_fitalg_gen, $
		   spex_fit_obj: obj_new(), $
           inherits spex_gen $
           }
end
