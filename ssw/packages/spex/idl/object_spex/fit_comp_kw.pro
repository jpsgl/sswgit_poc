;+
; OSPEX function  fit_comp_kw handles the fit component keywords
;
; Function returns 0 or 1 indicating if there are any fit keywords to use.  In addition
; there are a number of keyword options for returning different types of information:
;
; Arguments:
;   struct - either the structure of spex_summ... params or fit_comp... params
; Keywords:
;
; obj - Some of the options require the ospex object reference (this function can't be
;   an ospex obj method (like in spex_gen) because, e.g the fit_comp class doesn't
;   inherit spex_gen)
;
; nkw - returns the number of keywords being used (except for summary_string output)

; summary_string - If requested, struct must be a spex_summ structure. Return a string array
;   with keywords for each function component for time interval time_int, e.g.
;     bpow: none
;     vth: /cont /chianti5.2
; time_int - used with summary_string, time_interval index in struct to use
;
; process_struct - return structure with keyword values appropriate for passing via
;   _extra into call to compute function component
;
; plot_label - return string array of 5-character keyword values for plot label
; long_label - used with plot_label.  If set, return full info in plot_label

; fix_default - if set, then check if kw tags are in default structure, and if not,
;   add the tags with -1 values. struct arg itself is changed and returned.
;
; list_string - return string array for inclusion in list_function output
;
; Written:  Kim Tolbert March 2006
; Modifications:
; 29-May-2006, Kim.  Use trim when checking func_spectrum and func_model in case they = '  '
; 16-Oct-2007, Kim.  When getting fc_spectrum_options or fc_model_options, have to supply comp_name
;   or strategy_name, since now they are available for functions other than vth.
; 18-Sep-2009, Kim.  If returning process_struct, then if fit_comp_spectrum isn't
;   one of the pre-defined shortcut names, add tag 'file' with value fit_comp_spectrum.
; 11-Mar-2010, Kim. For plot_label for template component, strip path from filename and allow more characters
; 10-May-2010, Kim. Add pseudo_function tag if not there already
; 24-Sep-2012, Kim. When checking if struct.fit_comp_spectrum is one of he values in defaults structure,
;   use trim because fit_comp_spectrum are padded to be all same length string when put into structure array.
;   Also, added vth_abun, multi_therm_abun_exp, multi_therm_abun_pow, and multi_therm_gauss to th_funcs (thermal
;   functions with an abundance parameter)
;
;---------------------------------------------------------------------------------

function fit_comp_kw, struct, obj=obj, $
	nkw=nkw, $
	summary_string=summary_string, time_int=time_int, $
	process_struct=process_struct, $
	plot_label=plot_label, long_label=long_label, $
	fix_default=fix_default, $
	list_string=list_string

; these are the functions that have an abundance param, followed by the param number
th_funcs = ['vth', 'vth_abun', $
            'multi_therm_exp', 'multi_therm_abun_exp', $
            'multi_therm_pow', 'multi_therm_abun_pow', $
            'multi_therm_gauss']
abun_param = [2, 2,  4, 4,  4, 4,  5]

; summary_string is the only one that requires struct to be a spex_summ structure
if arg_present(summary_string) then begin
	if  not is_member('spex_summ', tag_names(struct), /wc, /ignore_case) then return, 0
	checkvar, time_int, 0
	comps = str2arr(struct.spex_summ_fit_function, '+')
	ncomp = n_elements(comps)
	for i=0,ncomp-1 do begin
		sp = trim(struct.spex_summ_func_spectrum[i,time_int])
		mo = trim(struct.spex_summ_func_model[i,time_int])
		th_ind = where (comps[i] eq th_funcs, is_th)
		if is_th then begin
			p_ind = fit_function_query(struct.spex_summ_fit_function, /param_index, $
				comp=i)
			rel_abun = (struct.spex_summ_params[p_ind[0]:p_ind[1], time_int])[abun_param[th_ind]]
			table = struct.spex_summ_abun_table
			abun = trim(get_fe_abun_rel2h(rel_abun,table),'(e10.2)')
			abun_str = '   Abundance:  Table=' + table + '  FE/H=' + abun
		endif
		strng = (trim(sp) eq '' ? '' : ' /'+sp) + $
				(trim(mo) eq '' ? '' : ' /'+mo + $
					(stregex(mo, 'ch', /fold, /bool) ? struct.spex_summ_chianti_version : '')) + $
				(is_th ? abun_str  : '')
		if strng ne '' then comp_strng = append_arr(comp_strng, comps[i] + ': ' + strng)
	endfor
	if not exist(comp_strng) then return, 0
	summary_string = comp_strng
	return, 1
endif

if keyword_set(fix_default) then begin
	if ~have_tag(struct, 'fit_comp_spectrum') then struct = add_tag(struct, '', 'fit_comp_spectrum')
	if ~have_tag(struct, 'fit_comp_model') then struct = add_tag(struct, '', 'fit_comp_model')
	if ~have_tag(struct, 'pseudo_function') then struct = add_tag(struct, 0, 'pseudo_function')
	return, 1
endif

; all other requests require struct to be a complete fit_comp structure.  fit_comp structure
; has information for only one component, and one time interval

is = trim(struct.fit_comp_spectrum) ne ''
im = trim(struct.fit_comp_model) ne ''

th_ind = where (struct.fit_comp_function eq th_funcs, is_th)
if is_th then begin
	rel_abun = struct.fit_comp_params[abun_param[th_ind]]
	table = get_abun_table(/type, model=struct.fit_comp_model)
	abun = trim(get_fe_abun_rel2h(rel_abun,table),'(e10.2)')
	abun_str = '   Abundance:  Table=' + table + '  FE/H=' + abun
endif

nkw = is+im

if arg_present(process_struct) then begin
  if is then begin
    defaults = fit_function_query(struct.fit_comp_function, /defaults)
    ;if fit_comp_spectrum is one of the pre-defined shortcuts, then 
    ; create a tag with that keyword set to 1.  Otherwise create a tag called
    ; file set to the filename contained in fit_comp_spectrum
    if is_member(trim(struct.fit_comp_spectrum), defaults.fc_spectrum_options) then $
	    process_struct = add_tag (process_struct, 1, struct.fit_comp_spectrum) $
	    else $
	    process_struct = add_tag (process_struct, struct.fit_comp_spectrum, 'file')
	endif
	if im then process_struct = add_tag (process_struct, 1, struct.fit_comp_model)
endif

if arg_present(plot_label) then begin
	;if somehow snuck in here with more than one fit component, return blank label
	if n_elements(struct.fit_comp_function) gt 1 then plot_label = '' else begin
		label = ''
		nchar = keyword_set(long_label) ? 99 : 5
		if is then begin
		  ;if fit_comp_function is 'template', then have only one param, so can use more chars.
		  ; but strip off path and extension from template file name first (works if it's a shortcut too).   
		  if struct.fit_comp_function eq 'template' then begin
		    spec = file_basename(struct.fit_comp_spectrum, '.sav')
		    if strlen(spec) gt nchar+20 then spec = strmid(spec,0,nchar+20) + '...'
		  endif else spec = strmid(struct.fit_comp_spectrum,0,nchar) 
		  label = append_arr(label, spec)
		endif
		if im then begin
			model = struct.fit_comp_model
			version = (keyword_set(long_label) and stregex(model, 'ch', /fold, /bool)) ? $
				chianti_kev_version() : ''
			label = append_arr(label, strmid(model,0,nchar)+version )
		endif
		if is_th then label=append_arr(label, abun)
		plot_label = arr2str(label, ' ')
	endelse
endif

if arg_present(list_string) then begin
	if is then $
		list = append_arr(list, '   Spectrum: ' +  struct.fit_comp_spectrum +  $
			'    (Options are '+arr2str(obj->get(/fc_spectrum_options, comp_name=struct.fit_comp_function)) + ')')
	if im then $
		list = append_arr(list, '   Model: ' + struct.fit_comp_model + $
			'    (Options are '+arr2str(obj->get(/fc_model_options, comp_name=struct.fit_comp_function)) + $
			'. Chianti Version = ' + chianti_kev_version() + ')')
	if is_th then list = append_arr(list, abun_str)
	list_string = fcheck(list,'')
endif

return, nkw gt 0

end

