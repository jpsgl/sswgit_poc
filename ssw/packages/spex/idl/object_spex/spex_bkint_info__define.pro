pro spex_bkint_info__define

struct = {spex_bkint_info, $
	spex_bkint_origunits: {spex_units}, $ ; Structure with original units for background interval data
	spex_bkint_units: {spex_units}, $ ; Structure with units of last accumulation for background interval data
	spex_bk_index: ptr_new(), $	; Start/end indices of background in data array, (2,n)
	spex_bk_time_used: ptr_new() $	; Background time intervals used, (2,n)
	}
end