;+
; NAME:
;	F_thick_warm_DEFAULTS
;
; PURPOSE: Function to return default values for
;   parameters, minimum and maximum range, and free parameter mask when
;   fitting to f_thick_warm function.
;   
;   ***** To use a single power-law electron distribution, it is better to change the break energy to a value that is equal to
;           or larger than the high energy cutoff. Or one can set the low delta and the high delta to same value.
;
; CALLING SEQUENCE: defaults = f_thick_warm_defaults()
;
; INPUTS:
;	None
; OUTPUTS:
;	Structure containing default values
;
; MODIFICATION HISTORY:
; Kim Tolbert, 24-Jul-2015, based on f_thick_defaults, with 3 new parameters
;
;-
;------------------------------------------------------------------------------

function f_thick_warm_defaults

defaults = { $
  fit_comp_params:      	    [1.,  4., 150., 6., 20., 3.2e4,   5.,   1.8,  10.], $
  fit_comp_minima:           [1e-10,  1.1, 1., 1.1, 1., 100.,   1.e-2, 0.1,  1.], $
  fit_comp_maxima:           [1e10,  20,  1e5, 20,  1e3, 1e7,   1.e3,  10., 100.], $
  fit_comp_free_mask:        [1,1,1,1,1,0, 0,0,0] $
  }

return, defaults

end