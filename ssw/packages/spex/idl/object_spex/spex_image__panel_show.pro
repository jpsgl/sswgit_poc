;+
; PANEL_SHOW method for OSPEX imaging spectroscopy
;
; Purpose: Show all the images and the regions defined in a user-configurable
; display, so that they can easily see all panels at once, and take a snapshot of the screen
; or send the plot to a gif file.
; Very similar to the panel_draw method that's part of the roi widget tool, but there,
; energy is always in columns, and time is always across rows, and there's a scroll bar to see
; them all at once.  Needed to leave that alone, since that tool is interactive, and need to know
; where user clicked.
;
; Arguments:
;   nx - number of images across (default is square root of # images)
;   panel_size - size of each image in pixels (default is 100)
;   e_idx - energy indices of image cube to show. 2-d array [first,last] Default is all.
;   t_idx - time indices of image cube to show. 2-d array [first,last] Default is all.
;   roinum - region number to show. Scalar.  Default is all.
;   gif_file - File name to write gif image of panels. Default - screen output.
;
; Written: Kim Tolbert 29-Oct-2007
;
;-


pro spex_image::panel_show, nx=nx,  panel_size=psize, e_idx=e_idx, t_idx=t_idx, $
	roinum=roinum, gif_file=gif_file, _extra=_extra

checkvar, psize, 100

img_obj = self -> get(/source)
old_use_single =  img_obj -> get( /use_single )
img_obj -> set,  use_single = 0
images = img_obj -> getdata()
im_size = size(images,/dim)

psize = n_elements(psize) eq 1 ? [psize, fix(psize*im_size[1]/im_size[0])] : psize

e_edges = img_obj -> getaxis(/energy, /edges_2)
t_edges = img_obj->getaxis(/ut, /edges_2 )
img_obj -> set,  use_single =  old_use_single
xrange_img = minmax(img_obj -> getaxis(/xaxis))
yrange_img = minmax(img_obj -> getaxis(/yaxis))

elabels = trim(e_edges,'(f5.1)')
tlabels = strmid(anytim(t_edges, /yy), 10, 10)

ntim = n_elements(t_edges[0,*])
nen = n_elements(e_edges[0,*])

checkvar, t_idx, [0,ntim-1]
checkvar, e_idx, [0,nen-1]
j1 = e_idx[0] & j2 = e_idx[1]
i1 = t_idx[0] & i2 = t_idx[1]
ntim = i2 - i1 + 1
nen = j2 - j1 + 1
nshow = ntim * nen
checkvar, nx, ceil (sqrt (nshow) )
ny = ceil (float(nshow) / nx)

yes_boxes = ptr_valid(self.boxes)
if yes_boxes then begin
	boxes = *self.boxes
	valid = ptr_valid(boxes)
	yes_boxes = total(valid) ne 0
endif else valid = intarr(nen,ntim)

tvlct, r,g,b, /get
loadct, self->get(/spex_roi_color), /silent

xsize = nx*psize[0]
ysize = ny*psize[1]
if keyword_set(gif_file) then begin
	set_plot,'Z'
	device, set_resolution=[xsize,ysize]
	charsize = .7
endif else begin
	window,/free, xsize=xsize, ysize=ysize
	charsize = .9
endelse

pos_old = !p.position

; xfrac, yfrac are the normalized size of each image
xfrac = 1./ nx
yfrac = 1./ ny

; Loop through images.  For each image, make a border by setting
; edge pixels to min of image, or if there's a valid box for image,
; then set to max of image.

nimg = 0
xloc = 0
yloc = 1. - yfrac

for ie = j1,j2 do begin

    for it = i1,i2 do begin

		; make sure the valid pointer points to a real box (nop ne 0)
		if valid[ie,it] then valid[ie,it] = ( (*boxes[ie,it]).nop[0] ne 0 )

		im = congrid( images(*, *, ie, it), psize[0], psize[1])
		border = valid[ie,it] ? max(im) : min(im)
		im[*, [0,psize[1]-1] ] = border
		im[ [0,psize[0]-1], *] = border
		tvscl, im, (ie-j1)*(ntim)+(it-i1)

		!p.position = [xloc, yloc, xloc+xfrac, yloc+yfrac] ;normalized plot box location

	    xyouts, xloc, yloc+yfrac,  $
	    	'!C' + tlabels[0,it] + '-' + tlabels[1,it] + '!C' +  $
	    	elabels[0,ie] + '-' + elabels[1,ie] + ' keV', $
	       /normal, charsize=charsize

		if valid[ie,it] then begin
			box = *boxes[ie,it]
			; first just set internal plot parameters so next command will work right
			plot, xrange_img, yrange_img, /nodata, /noerase, xstyle=5,ystyle=5
			hsi_oplot_clean_boxes, 0, 2,2,255, cw_pixels=box.list, cw_nop=box.nop, boxnum=roinum
		endif else begin
			;draw an X over image to show that no boxes are defined
			plot, xrange_img, yrange_img, /noerase, xstyle=5,ystyle=5
			plot, reverse(xrange_img), yrange_img, /noerase, xstyle=5,ystyle=5
		endelse

		xloc = xloc + xfrac
		if 1.-xloc lt 1.e-4  then begin
			xloc = 0
			yloc = yloc - yfrac
		endif

    endfor
endfor

if keyword_set(gif_file) then begin
	tvlct,rr,gg,bb,/get & image = tvrd()
	break_file, gif_file, disk,dir,name,ext
	if strlowcase(ext) ne '.gif' then gif_file = gif_file + '.gif'
	write_gif,gif_file,image,rr,gg,bb
	message,'Wrote gif plot file ' + gif_file, /cont
	set_x
endif


tvlct, r,g,b
!p.position = pos_old

end
