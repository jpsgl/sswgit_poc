;+
; ospex function method to get the structure containing the info for showing the filter states
; at the top of the plot.  In caller need to know how many bars will be displayed
; so can adjust the legend down by that amount.  Then call spex_draw_bars with this structure
; (or add the bars to plotman via addplot_name, addplot_arg properties) to plot the bars
; on the current plot.
;
; Written: 20-Jul-2004, Kim
; Modifications:
;	8-Apr-2005, Kim.  Previously this method returned str, and/or plotted the bars. Now just
;	  return str, and use spex_draw_bars routine as an addplot_name in xyplot to replot the
;	  filter bars every time plotman redraws the plot (previously only plotted once, and then
;	  were lost, since plotman didn't know about them.)
;	6-Feb-2006, Kim.  Finding start/end times didn't work right when intervals weren't
;	  contiguous.  Fixed.
;	16-Jan-2008, Kim. Was including the -99 interval in the previous good interval.  Now leave
;	  a gap between end and start of next good filter state, so -99 values will have a gap
;	  in plot bar.
;-

function spex_gen::get_filter_bars


ut = self -> getaxis(/ut, /edges_2)
filter = self -> get(/spex_interval_filter)
if filter[0] eq -1 then return, -1

find_changes, filter, index, state, count=count

if count eq 1 then begin
	stime = min(ut)
	etime = max(ut)
	label = 'F' + trim(state)
endif else begin
	q = where (state ne -99, count)
	q = count gt 0 ? q : indgen(n_elements(state))
;	index = [index, n_elements(ut[0,*])-1]
;	stime = ut[0, index[q]]
;	etime = ut[0, index[q+1]]

	; make arrays of start/end of each state, and then just use the ones in q (not -99)
	st = reform (ut[0,index])
	en = [ reform (ut[1,index[1:*]-1]), max(ut[1,*]) ]
	stime = st[q]
	etime = en[q]
;	index = index[q]
;	stime = reform(ut[0,index])
;	etime = [ reform(ut[1,index[1:*]-1]), max(ut[1,*]) ]

	label = 'F' + trim(state[q])
endelse

return, {stime: stime, etime: etime, label: label}

end




