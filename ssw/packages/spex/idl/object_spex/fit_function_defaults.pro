;+
; Name: fit_function_defaults
; Purpose: return a structure with the default values for a specified fit function (which
;  can contain multiple fit components (e.g. 'vth+bpow')
;
; Kim, 4-Oct-2003
;-

function fit_function_defaults, fit_function

dummy = obj_new('fit_function', fit_function=fit_function)
struct = dummy -> get(/fit_comp)
obj_destroy,dummy

return, struct
end