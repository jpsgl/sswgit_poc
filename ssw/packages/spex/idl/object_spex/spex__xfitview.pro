
; Modifications:
; 15-Mar-2006, Kim. added gui_label to put on title bar of spex widget interfaces
; 10-Jul-2008, Kim. Change hsi_ui_getfont to get_font (which is in ssw gen)
; 24-Nov-2008, Kim. Since added Interval # to plot_summ, energy is now xval=2 (changed in xfitview_update_widget
; 16-Jan-2009, Kim.  Totally rewrote to be easier to use.
; 18-Mar-2009, Kim.  Added show free/fixed bar options
; 22-Jan-2011, Kim.  Reduced size of widget by reducing space, pad, and # lines to show.
; 14-Nov-2012, Kim.  Changed Energy for Profile default from 25,50 to 10,50
; 26-Feb-2014, Kim.  Protect against selection of function name in time plot options (uvalue returns an undefined).
;   Also, print warning if select multiple items, but single plot button, that only first item will be plotted.
;-


;-----
; Event handlers can't be object methods, so this is just a wrapper to get to the method

pro xfitview_event,event
widget_control,event.top,get_uvalue=state
if obj_valid(state.object) then state.object->xfitview_event,event
return & end

;-----
; xfitview event handler

pro spex::xfitview_event, event

widget_control, event.top, get_uvalue=state

widget_control, event.id, get_uvalue=uvalue
;print,tag_names(event,/struc)
;help,event,/st
;help,uvalue

tree = widget_info(event.id,/tree_root)
if tree ne 0 then begin
	if tree ne state.w_time_tree then widget_control,state.w_time_tree, set_tree_select = 0
	if tree ne state.w_map_tree then widget_control,state.w_map_tree, set_tree_select = 0
	if tree ne state.w_energy_tree then widget_control,state.w_energy_tree, set_tree_select = 0
endif

if not exist(uvalue) then return

do_map = 0

case 1 of
    
  uvalue eq 'expand': for i=0,n_elements(state.w_func)-1 do widget_control, state.w_func[i], set_tree_expanded=1
    
  uvalue eq 'collapse': for i=0,n_elements(state.w_func)-1 do widget_control, state.w_func[i], set_tree_expanded=0
        
	strpos(uvalue,'plot') ne -1: begin
	
	  wt = widget_info(state.w_time_tree, /tree_select)
	  wm = widget_info(state.w_map_tree, /tree_select)
	  we = widget_info(state.w_energy_tree, /tree_select)
	  if total([wt,wm,we]) eq -3 then begin
	    msg = 'You have not selected anything to plot.'
	    a=dialog_message(msg, /info)
	    goto, done
	  endif

	  if we[0] ne -1 then begin
	     xaxis='energy'
	     nsel = n_elements(we)
	     if uvalue eq 'plot' and nsel gt 1 then begin
	       message,'You have selected more than one item, but the single plot button.  Plotting first item. ', /cont
	       we = we[0]
	       nsel = 1
	     endif
	     for i=0,nsel-1 do begin
	       widget_control, we[i], get_uvalue=ind
	       item = append_arr(item,state.ven[ind].item)
	       index = append_arr(index,-1)
	       strat = append_arr(strat,'')
	     endfor	     
	     sel = widget_info (state.w_timelist, /list_select)
	     if sel[0] ne -1 then sel_intervals = state.done[sel]
	     widget_control, state.w_e_error, get_value=show_error
	     widget_control, state.w_e_erange, get_value=show_erange
	     widget_control, state.w_e_overlay_model, get_value=overlay_model

	  endif else begin
	     xaxis = 'time'
	     widget_control, state.w_intervalaxis, get_value=interval_axis
	     if interval_axis[0] then xaxis='interval'
	     if wm[0] ne -1 then begin
	       do_map = 1
	     	 widget_control, wm[0], get_uvalue=ind
	     	 yaxis = state.vmap[ind].item
		   endif else begin
		   
		     widget_control, state.w_t_error, get_value=show_error
		     widget_control, state.w_t_freebar, get_value=show_free
  		   widget_control, state.w_energy, get_value=evals
  		   func_energy = str2arr(evals)
  		   nsel = n_elements(wt)
  		   if uvalue eq 'plot' and nsel gt 1 then begin
  		      message,'You have selected more than one item, but the single plot button.  Plotting first item. ', /cont  		      
  		      wt = wt[0] 
  		      nsel = 1 		      
  		   endif
         for i=0,nsel-1 do begin
           widget_control, wt[i], get_uvalue=ind
           if ~exist(ind) then begin
             widget_control, wt[i], get_value=item
             msg = 'Invalid item selected for plotting:  ' + item
             a=dialog_message(msg, /info)
             goto, done
           endif
           item = append_arr(item,state.vtime[ind].item)
           index = append_arr(index,state.vtime[ind].index)
           strat = append_arr(strat, state.vtime[ind].strat)
         endfor
         
		   endelse
	  endelse
	  
	  if not do_map then begin
	    if uvalue eq 'plot2v1' or uvalue eq 'plot1v2' then begin
	       if n_elements(item) eq 1 then begin
	          uvalue = 'plot'
	          message,'You have only selected one item, plotting vs ' + xaxis, /cont
	       endif
         if uvalue eq 'plot2v1' then begin
           xaxis = item[0]   & yaxis = item[1]
           xindex = index[0] & yindex = index[1]
           xstrat = strat[0]   & ystrat = strat[1]
         endif
         if uvalue eq 'plot1v2' then begin
           xaxis = item[1]   & yaxis = item[0]
           xindex = index[1] & yindex = index[0]
           xstrat = strat[1]   & ystrat = strat[0]
         endif
      endif
      	      
  	  if uvalue eq 'plot' then begin
  	    xstrat = ''
  	    yaxis = item[0]
  	    yindex = index[0]
  	    ystrat = strat[0]
  	  endif
  	  
    endif

    self -> plot_summ, xplot=xaxis, xindex=xindex, $
			yplot=yaxis, yindex=yindex, $
			xstrat=xstrat, ystrat=ystrat, $
			show_error=show_error, $
			show_free=show_free, $
			show_erange=show_erange, $
			overlay_model=overlay_model, $
			func_energy=func_energy, $
			this_interval=sel_intervals, err_msg=err_msg
	  if err_msg[0] ne '' then a=dialog_message(err_msg)
	  end

	uvalue eq 'exit': widget_control, state.wbase, /destroy

	else:
endcase

done:
if xalive (state.wbase) then begin
	widget_control, state.wbase, set_uvalue=state
;	self -> xfitview_update_widget, state.wbase
endif

end

;-----

pro spex::xfitview, group=group, gui_label=gui_label

if not self->valid_summ_params() then begin
	a=dialog_message('There are no fit results stored yet.  Aborting.')
	return
endif

if xregistered ('xfitview') then begin
	xshow,'xfitview', /name
	return
endif

checkvar, gui_label, ''

get_font, font, big_font=big_font

; read in the itt-supplied bitmap of a plot to use for function names
file=filepath('plot.bmp', subdir=['resource', 'bitmaps'])
image8 = read_bmp(file, red, green, blue)
; convert this to a 16x16x3 RGB image
plot_bmp = bytarr(16,16,3,/nozero)
plot_bmp[0,0,0] = red[image8]
plot_bmp[0,0,1] = green[image8]
plot_bmp[0,0,2] = blue[image8]

;; create a blue and darker blue bitmaps for the items to plot
blue_bmp=bytarr(16,16,3)+240
blue_bmp[2:13,2:13,0] = 75
blue_bmp[2:13,2:13,1] = 195
blue_bmp[2:13,2:13,2] = 195

dark_bmp = blue_bmp
dark_bmp[2:13,2:13,1]=125
dark_bmp[2:13,2:13,2]=125


self -> plot_summ_tables, vtime=vtime, ven=ven, vmap=vmap

summ = self -> get(/spex_summ)
done = where (summ.spex_summ_fit_done, ntimes)
if ntimes eq 0 then begin
  a = dialog_message('None of your intervals have been fit yet.  Aborting.')
  return
endif

free = summ.spex_summ_free_mask

funcs = str2arr(summ.spex_summ_fit_function, '+')

xindex = 0
yindex = 0
xaxis = 'time'
yaxis = 'params'


wbase = widget_base (group=group, title='View SPEX Fit Results '+gui_label, /column, space=10, xpad=4, ypad=4)

tmp = widget_label (wbase, value='View SPEX Fit Results', $
	font=big_font, /align_center)

w_time_energy = widget_base (wbase, /row, space=10)
w_time_base = widget_base (w_time_energy, /column, /frame)

tmp = widget_label (w_time_base, value='in Fit Time Intervals ')

nttree = n_elements(vtime)
ysize = nttree/5 > 1 < 3
w_time_tree = widget_tree (w_time_base, /multiple, units=1, ysize=ysize)

prev_strat = 'none'
nstrat = n_elements(get_uniq(vtime[where(vtime.strat ne '')].strat))
w_func = lonarr(nstrat)
ifunc = 0

for i = 0,nttree-1 do begin
  if vtime[i].strat eq '' then begin
    tmp = widget_tree (w_time_tree, value = vtime[i].label, uvalue=trim(i), bitmap=blue_bmp,/align_left)
  endif else begin
    if vtime[i].strat ne prev_strat then begin
  	  w_f = widget_tree(w_time_tree, /align_left, /folder, value=vtime[i].comp, bitmap=plot_bmp)
  	  w_func[ifunc] = w_f
  	  ifunc = ifunc + 1
  	  prev_strat = vtime[i].strat
    endif
    bmp = blue_bmp
    if vtime[i].item eq 'params' then if total(free[vtime[i].index,*]) eq 0 then bmp = dark_bmp
    lab_ind = vtime[i].index eq -1 ? '' : ' (' + trim(vtime[i].index) + ')'
    tmp = widget_tree (w_f, value=vtime[i].label + lab_ind, uvalue=trim(i), bitmap=bmp, /align_left)
  endelse
endfor

nmtree = n_elements(vmap)
w_map_tree = widget_tree (w_time_base, units=1, ysize=nmtree/5 > 1)
for i=0,nmtree-1 do tmp = widget_tree(w_map_tree, value=vmap[i].label, uvalue=trim(i), bitmap=blue_bmp)


w_time_opts = widget_base (w_time_base, /row)
w_intervalaxis =cw_bgroup (w_time_opts, 'vs Interval #', uvalue='none', /nonexclusive, /row, space=0)
tmp = widget_label (w_time_opts, value='Overlay: ')
w_t_error = cw_bgroup (w_time_opts, 'Errors', uvalue='none', /nonexclusive, /row, space=0, xpad=0)
widget_control, w_t_error, set_value=1
w_t_freebar = cw_bgroup (w_time_opts, 'Free/Fixed Bar', uvalue='none', /nonexclusive, /row, space=0, xpad=0)
widget_control, w_t_freebar, set_value=1

w_time_opts2 = widget_base (w_time_base, /row, space=5, /align_center)
tmp = widget_button (w_time_opts2, value='Expand all', uvalue='expand')
tmp = widget_button (w_time_opts2, value='Collapse all', uvalue='collapse')
w_energy = cw_field (w_time_opts2, title='Energy for Flux at E or Integral (keV)', value='10.,50.', /string)


w_en_base = widget_base (w_time_energy, /column, /frame)
tmp = widget_label (w_en_base, value='in Energy Bins')

netree = n_elements(ven)
w_energy_tree = widget_tree (w_en_base, /multiple, units=1, ysize=ysize)
for i=0,netree-1 do $
	tmp = widget_tree (w_energy_tree, value=ven[i].label, uvalue=trim(i), bitmap=blue_bmp)

tmp = widget_label (w_en_base, value='Select Time Interval(s) for energy plots:')
w_tlist_base = widget_base (w_en_base, /column, space=10, /frame)
w_timelist = widget_list (w_tlist_base, $
 					/multiple, $
					ysize=ntimes < 6, $
					xsize=44, $
					value=trim(done) + '.  ' + format_intervals(summ.spex_summ_time_interval[*,done], /ut), $
					uvalue='none')
widget_control, w_timelist, set_list_select=0 ;indgen(ntimes)

w_en_opts = widget_base (w_en_base, /row, space=0, xpad=0)
tmp = widget_label (w_en_opts, value='Overlay: ')
w_e_error = cw_bgroup (w_en_opts, 'Errors', uvalue='none', /nonexclusive, /row, space=0, xpad=0)
widget_control, w_e_error, set_value=1
w_e_erange = cw_bgroup (w_en_opts, 'Fit Range', uvalue='none', /nonexclusive, /row, space=0, xpad=0)
widget_control, w_e_erange, set_value=1
w_e_overlay_model = cw_bgroup (w_en_opts, 'Model', uvalue='none', /nonexclusive, /row, space=0, xpad=0)
widget_control, w_e_overlay_model, set_value=1

wbase_but = widget_base (wbase, /row, space=20, /align_center)
tmp = widget_button (wbase_but, value='Plot', uvalue='plot')
tmp = widget_button (wbase_but, value='Plot 1st vs 2nd', uvalue='plot1v2')
tmp = widget_button (wbase_but, value='Plot 2nd vs 1st', uvalue='plot2v1')
;tmp = widget_button (wbase_but, value='Write Save File', uvalue='write', sensitive = 0)
tmp = widget_button (wbase_but, value='Close', uvalue='exit')

state = {wbase: wbase, $
	w_tlist_base: w_tlist_base, $
	w_timelist: w_timelist, $
	done: done, $
	vtime: vtime, $
	ven: ven, $
	vmap: vmap, $
	w_time_tree: w_time_tree, $
	w_func: w_func, $
	w_map_tree: w_map_tree, $
	w_energy_tree: w_energy_tree, $
	w_intervalaxis: w_intervalaxis, $
	w_t_error: w_t_error, $
	w_t_freebar: w_t_freebar, $
	w_energy: w_energy, $
	w_e_error: w_e_error, $
	w_e_erange: w_e_erange, $
	w_e_overlay_model: w_e_overlay_model, $
	object: self, $
	gui_label: gui_label }

widget_control, wbase, set_uvalue=state

widget_offset, group, newbase=wbase, xoffset, yoffset

widget_control, wbase, xoffset=xoffset, yoffset=yoffset

widget_control, wbase, /realize

;self -> xfitview_update_widget, wbase

xmanager, 'xfitview', wbase, /no_block

end