;+
; PROJECT: RHESSI
;
; NAME: SPEX::CHI2_MAP  
;
; PURPOSE: OSPEX method to determine the error in a best-fit parameter via chisquare mapping.
;   The selected fit parameter is fixed at a grid of values centered on the best-fit value, and
;   the remaining parameters are fit.  The chi-square at each fit is plotted vs the selected fit parameter
;   (usually looks like a smooth inverted gaussian), and the parameter values at the chisqr min plus 1 and
;   the chisqr min plus 4 give the 1- and 2-sigma error estimates.   
;   
; EXPLANATION: After the user sets up their ospex object, defines input file, background options, fit 
;   intervals and function, and finds the best-fit parameters, the user can call this method to
;   determine the error on one of those best-fit parameters for one of those intervals via a chisqr-mapping technique.  
;   We loop through a grid of values surrounding the best-fit value for the selected parameter, setting the parameter
;   to that value and fixing it.  The remaining parameters are fit.  This yields a chisqr value for each fixed value of
;   the selected parameter.  We save the results in an IDL save file.   We call spex_chi2_map_results to compute the
;   1-sigma and 2-sigma errors based on chisqr min plus 1 and 4.
;   
;   Once the save file is created, the user can call spex_chi2_map_results directly and request plots of the 
;   chisqr maps on screen, png, or ps.
;      
;   On exit, the ospex object is restored to its state on entering, including resetting the spex_summ
;   parameters (i.e. the values from the last fit iteration in this routine are not what is left in the 
;   spex_summ structure for this interval upon exit).  This means that if desired, you can run this routine
;   again with the same object rather than reinitializing the object with your original best-fit params.
; 
;   RESTRICTIONS:
;   
;   This works on only one interval at a time (specify the interval in the keyword).
;   
;   Note:  A monte carlo method is also available for determining error estimates (see the spex__monte_carlo routine.
;   Monte Carlo works well in cases where your best-fit parameters are well-defined.  It assumes
;   they are the correct answer, and varies the model around them.  It does not explore parameter space far
;   away from your best-fit parameters.  Chi-square mapping (done in this routine) or Bayesian analysis is 
;   more appropriate for cases where there are multiple possible solutions.
;   
; CALLING SEQUENCE: 
;   o -> chi2_map
;  
; INPUT KEYWORDS:
;  param_index - parameter index to find error for (default=0)
;  center_value - value of parameter presumed to be at center of chisqr map (default is best-fit calculated)
;  delta - plus and minus value from center_value to extend parameter values by (default is 3*sigma from best fit)
;  ntrials - number of values in grid (each parameter will be spaced by (2*delta)/ntrials) (default is 100
;  interval - fit interval number (default is 0)
;  id - string to include in output file name if it is being auto-generated to make filename unique
;     e.g., if id='_fixedweights', the auto-generated name might be 'chi2_map_i1_p2_n40_fixedweights.sav'
;  savefile - name of save file to write (default is chi2_map.sav)
;  plot - if set, plot results in spex_chi2_map_results (default is 1)
;  quiet - if set, don't print info about each iteration.  Default is 1.
;  _extra - any additional keywords to pass through to spex_chi2_map_results (e.g. /ps, /png)
;
; OUTPUT KEYWORDS:
;  chi2 - array of chisqr values
;  vals - array of parameter values
;  out_struct - structure containing results from spex_chi2_map_results including 1-sigma and 2-sigma values,as
;    well as an indication of whether they're valid (see spex_chi2_map_results for details of structure)
;
; Example:
;  o->chi2_map, param_index=4, ntrials=200,interval=5, out_struct=out
;  o->chi2_map, savefile='myfile.sav', chi2=chi2, vals=vals, plot=0
;  
; OUTPUT:
;  A save file will be created containing the following values from the chisqr mapping process:
;  fit_function, interval, fit_time, param_index, savep, savechi, savefullchi, savestart
;  A plot will also be sent to the screen showing chisqr vs the parameter value.
;  
; WRITTEN: Kim Tolbert, 22-Aug-2010
; 
; MODIFICATIONS:
; 1-Sep-2010, Kim.  Added _extra to pass through to spex_chi2_map_results.  Added checks that parameter and
;   interval requested are valid. Call new set_fitting_params method to set params from summ struct. If
;   ntrials is 1, just use center_value.
; 28-Sep-2010, Kim.  Realized that unless you fix the weighting, chi2 map is not valid. Minimum is not at best-fit
;   values for parameters. Now get the errors (weights for fitting are 1/error^2) from the best fit (spex_summ_ct_error, 
;   which is a rate) and pass that to the dofit call, so that every set of parameters uses those errors.  
;   Also, added id keyword for string to include in output file name, added quiet keyword,
;   get orig params in a structure, and use _extra to set them back (instead of individually), and
; 22-Jun-2011, Kim.  Added start_center keyword.  Normally start at lowest value of param range and
;   move forward using best fit params as starting params for each fit.  If start_center is set, start at 
;   center value (best fit) and move backwards using params from previous fit as starting values.  
;   Then start at center with best fit values, and move forward using previous fit.  This helps each fit
;   to find lowest chi2.
;   Also, added check for negative values (val-3*sigma might be neg).  For each trial that para is neg,
;   skip that trial, and then remove those elements from saved arrays.
;
;-

pro spex::chi2_map, $
  param_index=param_index, $
  center_value=center_value, $
  delta=delta, $
  start_center=start_center, $
  ntrials=ntrials, $
  interval=interval, $
  id=id, $  
  savefile=savefile, $
  plot=plot, $
  out_struct=out_struct, $
  chi2=chi2,vals=vals, $
  quiet=quiet, $
  _extra=_extra

checkvar, param_index, 0
checkvar, start_center, 0
checkvar, ntrials, 100
checkvar, interval, 0
checkvar, id, ''
checkvar, savefile, 'chi2_map_i' + trim(interval) + '_p' + trim(param_index) + '_n' + trim(ntrials) + id + '.sav'
checkvar, plot, 1
checkvar, quiet, 1

; initialize output variables in case we abort
out_struct = -1
vals = -1
chi2 = -1

summ_orig = self -> get(/spex_summ)

; Make sure requested interval # is defined and has been fit.
if interval ge n_elements(summ_orig.spex_summ_fit_done) || $
  summ_orig.spex_summ_fit_done[interval] ne 1 then begin
  message, /cont, 'Requested interval ' + trim(interval) + ' does not exist, or has not been fit.  Aborting'
  return
endif

; Make sure requested parameter index is valid
if param_index ge n_elements(summ_orig.spex_summ_params[*,0]) then begin
  message, /cont, 'Requested parameter index ' + trim(param_index) + ' does not exist.  Aborting.'
  return
endif

; Get previously stored best-fit parameters and sigmas for requested interval
params = summ_orig.spex_summ_params[*,interval]
sigmas = summ_orig.spex_summ_sigmas[*,interval]

; Get errors to use for weighting for all parameter values
errors = summ_orig.spex_summ_ct_error[*,interval]

; if center_value and delta weren't passed in, set them from previous best fit
checkvar, center_value, params[param_index]
checkvar, delta, 3. * sigmas[param_index] 

; Save some settings so we can restore them when we're done.
nointeractive_orig = get_logenv('OSPEX_NOINTERACTIVE')
orig_vals = self -> get(/spex_fit_firstint_method, $
  /spex_fit_start_method, $
  /spex_fit_init_params_only, $
  /spex_fit_manual, $
  /spex_interval_index, $
  /fit_comp_params, $
  /fit_comp_free_mask)

; don't plot or popup widgets while running the chi2 mapping
set_logenv, 'OSPEX_NOINTERACTIVE', '1'

; Use 'current' instead of 'prev_iter' so that we can store params back into fit_comp_params for
; each iteration, and it will use them to start (otherwise would use results from most recent fit)
; Have to also set spex_fit_start_method because if it's already set to 'previous_iter', that
; overrides spex_fit_firstint_method='current'.
; Set spex fit methods to use current params, and auto-fit, and to work on selected interval
self -> set, spex_fit_firstint_method='current', $
  spex_fit_start_method='previous_int', $
  spex_fit_init_params_only=0, $
  spex_fit_manual=0, $
  spex_interval_index = interval, $
  spex_intervals_tofit = interval

; get free mask used for this interval, and set parameter we're working on to fixed.
free_mask = summ_orig.spex_summ_free_mask[*,interval]
free_mask[param_index] = 0

; set min, max, spectrum, model, erange, uncert, itmax previously used for this interval
set_all=1 & set_all_comp=1
(self->get(/obj,class='spex_fit')) -> set_fitting_params,  set_all, set_all_comp, summ_orig, params, interval
self -> set, fit_comp_free_mask = free_mask

; create arrays for storing results of fit iterations
savep = reproduce(params,ntrials) + 0.
savechi = fltarr(ntrials)
savefullchi = savechi
savestart = params

;loop through ntrials settings of selected parameter, fitting other parameters.
; Use errors from best fit so weighting doesn't change for chi2 map.
if quiet then begin
  print, ' '
  print, 'Quiet is set, so will print update only every 5 iterations.'
  print, 'For more output, call with quiet=0'
endif

; set up array of values for param to step through
newpar = ntrials eq 1 ? center_value : center_value - delta + indgen(ntrials)*2.*delta/ntrials

; If start_center start at center_value and go backward, then forward from there using previous
; interval's best fit as starting params for next trial.
; Otherwise, start at min value and step forward, using best fit as starting params for all trials
; i1,i2 are start end indices for each group.
if start_center and ntrials gt 1 then begin
  njj = 1
  i1 = [ntrials/2, ntrials/2+1]
  i2 = [0, ntrials-1]
  incr = [-1, 1]  ; backward, then forward
endif else begin
  njj=0
  i1 = 0
  i2 = ntrials-1
  incr = 1
endelse

best_params = params
;print, 'newpar = ',newpar
;print, 'best_params = ', best_params

; loop through trials
for jj=0,njj do begin
    self -> set, fit_comp_param=best_params
    for i = i1[jj],i2[jj],incr[jj] do begin
      if newpar[i] lt 0. then goto, next_trial
      params = self -> get(/fit_comp_params)
      params[param_index] = newpar[i]
      self -> set, fit_comp_param=params
      if ~quiet then begin
        print,'Start parameters = ', params
        print,'Free mask        = ', free_mask
      endif
    
      self -> dofit, use_errors=errors, quiet=quiet
      
      savep[*,i] = self -> get(/fit_comp_params)
      chi2 = self -> get(/mcurvefit_chi2)
      fullchi2 = self -> calc_summ(item='chisq_full') 
      savechi[i] = chi2
      savefullchi[i] = fullchi2[interval]
      
      if ~quiet or ((i+1) mod 5 eq 0) then begin
        print,''
        print,' Completed chi2 mapping trial ', trim(i+1), ' / ',trim(ntrials), '   Chi2,   Fullchi2  ', chi2, fullchi2[interval]
      endif 
      
    next_trial:
    endfor
endfor

; Write save file with results from all iterations
fit_function = self -> get(/fit_function)
fit_time = self -> get(/spex_fit_time_interval)
fit_time = anytim(fit_time[*,interval],/vms)
; if didn't do some of the trials (because param was neg.), savechi will still be zero,
; get rid of those elements.
q = where (savechi eq 0., nzero, complement = good, ncomplement=ngood)
if nzero gt 0 then begin
  savep = savep[*,good]
  savechi = savechi[good]
  savefullchi = savefullchi[good]
  message,/cont, 'Removed ' + trim(nzero) + ' trials because param was < 0., ' + trim(ngood) + ' trials were kept.'
endif
save, fit_function, interval, fit_time, param_index, savep, savechi, savefullchi, savestart, file=savefile
print,''
print,'Wrote IDL save file ' + savefile
print,''

; also return just the chi2 and values for the param_index parameter
chi2 = savechi
vals = reform(savep[param_index,*])

; Restore values in object that we changed

self -> set, _extra=orig_vals

fit_obj = self -> get(/obj, class='spex_fit')
fit_obj -> set, _extra=summ_orig, /this_class_only
set_logenv, 'OSPEX_NOINTERACTIVE', nointeractive_orig

; Print error ranges of each fitted parameter
spex_chi2_map_results, savefile=savefile, plot=plot, out_struct=out_struct, _extra=_extra


end
