; Modifications:
; 23-Apr-2008, Kim.  Added spex_fit_firstint_method init
; 08-Dec-2008, Kim.  Added spex_fit_auto_erange
; 29-May-2009, Added spex_fit_init_params_only

function spex_fit_control

var = {spex_fit_control}

var.spex_fit_manual = 1
var.spex_fit_reverse = 0
var.spex_fit_start_method = 'previous_int'
var.spex_fit_firstint_method = 'current'
var.spex_fit_init_params_only = 0
var.spex_fit_auto_erange = 0

return, var

end