;+
; Name: spex_convert_results

; Purpose: Convert structure from ospex results file from Version 1. to current version.
;
; Input:
;  primhdr - primary header from OSPEX results FITS file
;  rates_table - structure from rates table extension of FITS file
;  rates_hdr - header of rates table extension of FITS file
;  ebounds_table - structure from ebounds table extension of FITS file
;  ebounds_hdr - header of ebounds table extension of FITS file
;
; Output:
;  Each of the input variable is changed as needed and returned.
;
; Method:
; Changes need to convert from Version 1. to 1.1:
;   All changes are in rates_table structure:
;   1. vth functions (vth, multi_therm_pow, multi_therm_exp) now have an extra parameter
;     (abundance).  Need to add an extra element in the correct place in all arrays that
;     are dimensioned by nparams and set to default values.
;   2. func_spectrum, func_model need to be added to structure, and set to -1 for non-thermal
;      functions, and 'full', 'mewe' for any thermal function
;   3. chianti_version, abun_table need to be added to structure.
;      since we only had mewe before this, abun_table should be set mewe_solar
;
; Written: Kim Tolbert 1-May-2006
; 15-May-2006, Kim.  Replace any vth_noline components with vth and set their
;   corresponding func_spectrum value to 'continuum'
;-


pro spex_convert_results, primhdr, $
       rates_table, rates_hdr, $
       ebounds_table, ebounds_hdr

version = fxpar(primhdr, 'VERSION')

if float(version) lt 1.1 then begin

	ntimes  = n_elements(rates_table)
	nchan   = n_elements(ebounds_table)
	fit_func = trim(fxpar(rates_hdr, 'FITFUNC'))

	fit_comp_arr = str2arr(fit_func, '+')

	; replace any vth_noline with vth, and down below set keyword to continuum
	ind_noline = where (fit_comp_arr eq 'vth_noline', count_noline)
	if count_noline gt 0 then begin
		fit_comp_arr[ind_noline] = 'vth'
		fit_func = arr2str(fit_comp_arr, '+')
		q = where (strpos(rates_hdr,'FITFUNC') ne -1)
		rates_hdr[q[0]] = "FITFUNC = '" + fit_func + "'"
	endif


   ; the indices returned will be the for the new function definitions
   ; ind will contain the start/end index for params for each fit compononent
	ind = fit_function_query(fit_func, /param_index)
	nparams = fit_function_query(fit_func, /nparams)
	ncomp = fit_function_query (fit_func, /ncomp)

	params = rates_table.params
	startpar = rates_table.startpar
	minima = rates_table.minima
	maxima = rates_table.maxima
	freemask = rates_table.freemask
	sigmas = rates_table.sigmas

	func_spectrum = strarr(ncomp, ntimes)
	func_model = strarr(ncomp, ntimes)
	chianti_version = ''
	abun_table = 'mewe_solar'

	need_update = 0  ; if no thermal funcs, don't need to update structure

	; ind[i,1] is the last param element numberfor component i.  For the 3 thermal
	; functions, that's the one that's missing, so we need to insert the default value for
	; that param in each array.

	;model and spectrum should be set to '' except for thermal functions, set to 'mewe' and 'full'
	for i=0,ncomp-1 do begin

		if fit_comp_arr[i] eq 'vth' or $
		 fit_comp_arr[i] eq 'multi_therm_pow' or $
		 fit_comp_arr[i] eq 'multi_therm_exp' then begin

			need_update = 1

			func_model[i,*] = 'mewe'
			func_spectrum[i,*] = 'full'

			index = ind[i,1]
			params = array_insert(params, 1., index)
			startpar = array_insert(startpar, 1., index)
			minima = array_insert(minima, .01, index)
			maxima = array_insert(maxima, 10., index)
			freemask = array_insert(freemask, 0, index)
			sigmas = array_insert(sigmas, 0., index)

		endif

	endfor

	if count_noline gt 0 then func_spectrum[ind_noline,*] = 'continuum'


	if need_update then begin
		rates_table = rep_tag_value (rates_table, params, 'params')
		rates_table = rep_tag_value (rates_table, startpar, 'startpar')
		rates_table = rep_tag_value (rates_table, minima, 'minima')
		rates_table = rep_tag_value (rates_table, maxima, 'maxima')
		rates_table = rep_tag_value (rates_table, freemask, 'freemask')
		rates_table = rep_tag_value (rates_table, sigmas, 'sigmas')
	endif

	; need to add these regardless of whether any component was thermal
	rates_table = add_tag (rates_table, func_model, 'func_model')
	rates_table = add_tag (rates_table, func_spectrum, 'func_spectrum')
	rates_table = add_tag (rates_table, abun_table, 'abun_table')
	rates_table = add_tag (rates_table, chianti_version, 'chianti_version')


endif

end




