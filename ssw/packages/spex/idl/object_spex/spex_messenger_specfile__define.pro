;+
;
; NAME:
;   spex_messenger_specfile__define
;
; PURPOSE:
;   Provides read_data method for the messenger instrument.
;   NOTE: This procedure is based on spex_hessi_specfile__define.
;
;
; CATEGORY:
;       SPECTRAL FITTING, SPEX - messenger
;
; CALLING SEQUENCE:
;
; CALLS:
;   read_messenger_4_ospex
;
; INPUTS:
;
; OUTPUTS:
;
; INPUT KEYWORDS:
;
; OUTPUT KEYWORDS:
;
; PROCEDURE:
;
; Written 13-Jun-2007, Kim Tolbert
;
; Modification History:
; 2-May-2008, Kim.  Call read_messenger_pds instead of read_messenger_4_ospex
; 16-Jun-2008,  Kim. spex_specfile is now allowed to be an array.  Loop through input files,
;   concatenating data into arrays.
; 25-Feb-2010, Kim. Added file_index arg to read_data.
;------------------------------------------------------------------------------

pro spex_messenger_specfile_test, o

  o =  obj_new( 'spex_messenger_specfile' )
  o->set,  spex_spec =  'D:\Analysis\working\messenger\xrs2007152.dat'
  data =  o->getdata()

end

;------------------------------------------------------------------------------

pro spex_messenger_specfile::read_data, file_index=file_index, $
                       spectrum,  errors,  livetime,  $
                       spex_respinfo, spex_file_time,  spex_ut_edges,  spex_ct_edges,  $
                       spex_area, spex_title, spex_detectors,  $
                       spex_interval_filter, spex_units, spex_data_name, $
                       spex_deconvolved, spex_pseudo_livetime, spex_data_pos, $
                       err_code=err_code, _extra=_extra

  file =  self -> get( /spex_specfile )
  checkvar, file_index, 0
  file = file[file_index]

  nfiles = n_elements(file)
  file = file(sort(file))
  for i=0,nfiles-1 do begin
    read_messenger_pds, FILES = file[i], $
                        _EXTRA = _ref_extra, $
                        data_str = ds, $
                        ERR_CODE = err_code, $
                        ERR_MSG = err_msg                                                                                                    
    if err_msg[0] ne '' then begin
        ;if not spex_get_nointeractive() then xmessage,err_msg else print,err_msg
        message, err_msg, /cont
        if err_code then return
    endif
    
    spectrum = i eq 0 ? ds.rcounts : [[spectrum], [ds.rcounts]]
    errors = i eq 0 ? ds.ercounts :  [[errors], [ds.ercounts]]
    livetime = i eq 0 ? ds.ltime :  [[livetime], [ds.ltime]]
    spex_file_time = i eq 0 ?  utime([ds.start_time, ds.end_time]) : [spex_file_time[0],utime(ds.end_time)]
    spex_ut_edges = i eq 0 ? ds.ut_edges : [[spex_ut_edges], [ds.ut_edges]]
  endfor

  spex_respinfo = ds.respfile
  spex_ct_edges = ds.ct_edges
  spex_area = ds.area[0]
  spex_title = ds.title
  spex_detectors = ds.detused
  spex_units = ds.units
  spex_interval_filter = ds.atten_states
  spex_data_name = 'MESSENGER'
  spex_deconvolved = 0
  spex_pseudo_livetime = 0
  spex_data_pos = [0.,0.]

end

;------------------------------------------------------------------------------
pro spex_messenger_specfile__define

  self = {spex_messenger_specfile, $
          INHERITS spex_data_strategy }

END
