;+
; Name: SPEX__FITSWRITE
;
; Purpose: Provides methods to create a FITS file for OSPEX.
;
; Calling Sequence
; fitswrite, outfile=outfile, fits=fit_results, bksub=bksub, origint=origint
;
; INPUTS:
;
; INPUT KEYWORDS:
; /FIT_RESULTS -  Specify this keyword to create FITS file containing OSPEX fit results
; /BKSUB -  Specify this keyword to create a Background-subtracted RATES
; /ORIGINT - Specify this keyword to create an unbinned RATES
;
;********************************************************************************************
; *** NOTE ***
; FIT_RESULTS keyword cannot be used in combination with BKSUB and/or ORIGINT keywords.
;********************************************************************************************
; This procedure can be called in one of the following ways:
;   fitswrite, outfile=outfile   ; Creates a RATE file containing binned count data (and
;                                      background, if available in the input file)
;   fitswrite, outfile=outfile, /bksub   ; Creates a RATE file containing binned background-
;                                              subtracted data
;   fitswrite, outfile=outfile, /bksub, /origint   ; Creates a RATE file containing unbinned
;                                                        background subtracted data
;   fitswrite, outfile=outfile, /origint   ; Creates a RATE file containing unbinned
;                                                count (and background data, if applicable)
;   fitswrite, outfile=outfile, /fit_results   ; Creates a file containing FIT results
;
;
; OUTPUTS:
; OUTFILE - Name of the output file
;
;
; Category: OSPEX
;
; Written: Oct. 22, 2004  Sandhia Bansal
; Modifications:
; Sandhia Bansal - 12/08/04 - renamed three procedures: createRateExt to fit_results_write,
;             getRates to fit_results and createControlExt to fit_results_control_ext.
; Sandhia Bansal - 12/20/04 - Modified defineRateKeys to set telescope to "unknonwn" and
;                                chantype to a blank string when the instrument is not "HESSI"
;                                or "XSM".
; Kim.  15-Aug-2005.  Changed catch in fitswrite method to check debug level, and
;   print more info, changed getRateFilename to use break_file, changed getRateFilename
;   filetype argument to not include '.', and Andre made changes to use his new fitswrite obj.
; Kim, 7-Apr-2006. added new spex_summ params (spectrum, model, chianti_version,
;   abun_table) in fit_results method.  New version of fit results FITS file is 1.1 (was 1.00)
;!!!!!!!!!!!!!!!!!!!!!!!
; NOTE from Kim, 7-apr-2006:  fit_results method here and spex_read_fit_results should
; be changed to use the original names in the spex_summ structure instead of
; changing the names to shorter names to write the binary extension, and then
; repopulating the spex_summ structure with each name.  That requires manually moving
; each piece of info in spex_summ, instead of just using the whole structure.
;  Should just use the spex_summ structure and add
; whatever's necessary to it before calling mwrfits.  I think.  I think Sandhia did it this
; way so that each interval has a structure (replicated with same tags)?   But is it worth it?
;!!!!!!!!!!!!!!!!!
;
; 29-May-2006, Kim.  Made sure func_spectrum and func_model aren't '' when the structure
;   containing them is being set up to write to a FITS file.  There's a bug in the FITS writer.
; 16-Jun-2008, Kim. Use spex_specfile[0] since now allow multiple input files (for MESSENGER)
; 24-Jun-2008, Kim. Default results fitsfile name now lowercase.
; 19-Jun-2009, Kim. In fit_results, correct calculation of time offsets
;   This error resulted in fit results files with a 1 second error in spex_summ_time_interval
;   for any times after a day crossing boundary.  To fix a results FITS file with this problem:
;     t=o->get(/spex_summ_time_interval)
;     q = where(t gt anytim(t[0],/date)+86400)
;     t[q] = t[q] -1.
;     o->set,spex_summ_time_interval=t
; 17-Aug-2009, Kim. In writeRateFile and savefit_fits methods destroy fitswrite object. memory leak
; 18-Sep-2009, Kim. In getRateFilename, make default output extension = '.fits'.  And in
;   writeRateFile, only set readSpecFile to 1 if input file was a fits file.
; 2-Dec-2013, Kim. Use tag name ebkdata, not bkerr when constructing and using background
;      structure (in writeBinnedData, setupBkRateStruct, and writeData) since that matches tag in fitint data structure.
;   In createbkrateext, get (/spex_detectors) instead of reading header of FITS file.
;   In createEboundsExt, check EBOUNDS ext too (previously just ENEBAND), and if neither is available, make new header, don't just quit
;   In createSpecParExt, call rd_fits_ext with /silent
;   SHOULD CHANGE WRITEBINNEDDATA TO USE FITINT STRUCTURE THAT ALREADY HAS DATA AND BK BINNED TO FIT INTERVAL
;   
;-
;---------------------------------------------------------------------------

; Adds common primary keywords to the current extension.
pro spex::add_primary_keywords, fptr, rate_struct, sdate, version


; Add keywords to the primary header
fptr->Addpar, 'ORIGIN', rate_struct.origin, 'Origin of FITS file'
fptr->Addpar, 'TELESCOP', rate_struct.telescope, 'Telescope or mission name'
fptr->Addpar, 'INSTRUME', rate_struct.instrument, 'Instrument name'
fptr->Addpar, 'OBJECT', rate_struct.object, 'Observed object'
fptr->Addpar, 'OBSERVER', rate_struct.observer, 'Name of the user who genrated the file'
fptr->Addpar, 'RA', 0.0, 'Source right ascension in degrees'
fptr->Addpar, 'DEC', 0.0, 'Source declination in degrees'
fptr->Addpar, 'RA_NOM', 0.0, 'r.a. nominal pointing in degrees'
fptr->Addpar, 'DEC_NOM', 0.0, 'dec. nominal pointing in degrees'
fptr->Addpar, 'EQUINOX', rate_struct.equinox, 'Equinox of celestial coordinate system'
fptr->Addpar, 'RADECSYS', rate_struct.radecsys, 'Coordinate frame used for equinox'
fptr->Addpar, 'DATE-OBS', sdate[0], 'Data start time UT'
fptr->Addpar, 'DATE-END', sdate[1], 'Data end time UT'
fptr->Addpar, 'TIMVERSN', 'OGIP/93-003', 'OGIP memo number where the convention used'
fptr->Addpar, 'VERSION', version, 'File format version number'
fptr->Addpar, 'AUTHOR', rate_struct.author, 'Software that created this file'

; Get current time and place the 6 elements returned by bin_date in a string array
currtime = strmid(anytim(!stime, /ccsds), 0, 19)
fptr->Addpar, 'DATE', currtime, 'File creation date (YYYY-MM-DDThh:mm:ss UTC)'

end

;--------------------------------------------------------------------------
; Writes common RATE keywords to the header.
pro spex::add_rate_keywords, fptr, rate_struct, sdate


; Add common keywords to the RATE header
fptr->Addpar, 'DETUSED', ' '
fptr->Addpar, 'SUMFLAG', 1, 'Spectra summed over detectors'
fptr->Addpar, 'SUMCOINC', 0
fptr->Addpar, 'COINCIDE', 0, 'Anti-coincident spectra'

IF (strcmp(rate_struct.instrument, 'XSM')) THEN BEGIN
   fptr->Addpar, 'COMMENT', $
      'absTime[i]=mjd2any(MJDREF+TIMEZERO)+HOUR[i]*3600+MINUTE[i]*60+SECOND[i]'
   fptr->Addpar, 'COMMENT', 'Correct absTime[i] for Day passing through midnight'
ENDIF ELSE $
   fptr->Addpar, 'COMMENT', 'absTime[i] = mjd2any(MJDREF + TIMEZERO) + TIME[i]'

respfile = self->get(/spex_drmfile)
break_file, respfile, dum,dum, name, ext
fptr->Addpar, 'RESPFILE', name+ext, 'Name of the response file'


fptr->Addpar, 'DATE-OBS', sdate[0], 'Nominal U.T. date when integration of this PHA data started'
fptr->Addpar, 'DATE-END', sdate[1], 'Nominal U.T. date when integration of this PHA data stopped'

fptr->Addpar, 'FITFUNC', rate_struct.fitFunction, 'Fit function used'

phafile = self->get(/spex_specfile)
IF (phafile[0] ne "") THEN BEGIN
   break_file, phafile[0], dum,dum, name, ext
   phafile = name+ext
ENDIF
fptr->Addpar, 'PHAFILE', phafile, 'Name of the spectrum file'

; Get current time and place the 6 elements returned by bin_date in a string array
currtime = strmid(anytim(!stime, /ccsds), 0, 19)
fptr->Addpar, 'DATE', currtime, 'File creation date (YYYY-MM-DDThh:mm:ss UTC)'

end

;--------------------------------------------------------------------------
; Write Energy Bounds related keywords to the extension.

pro spex::add_ebounds_keywords, fptr, rate_struct, sdate, version

; Add keywords to the primary header
fptr->Addpar, 'ORIGIN', rate_struct.origin, 'Origin of FITS file'
fptr->Addpar, 'TELESCOP', rate_struct.telescope, 'Telescope or mission name'
fptr->Addpar, 'INSTRUME', rate_struct.instrument, 'Instrument name'
fptr->Addpar, 'DATE-OBS', sdate[0], 'Data start time UT'
fptr->Addpar, 'DATE-END', sdate[1], 'Data end time UT'
fptr->Addpar, 'TIMVERSN', 'OGIP/93-003', 'OGIP memo number where the convention used'
fptr->Addpar, 'EXTNAME', 'ENEBAND', 'Extension name'
fptr->Addpar, 'VERSION', version, 'File format version number'

; Get current time and place the 6 elements returned by bin_date in a string array
currtime = strmid(anytim(!stime, /ccsds), 0, 19)
fptr->Addpar, 'DATE', currtime, 'File creation date (YYYY-MM-DDThh:mm:ss UTC)'

end
;--------------------------------------------------------------------------
; Create a structure to store energy range specific information and
;    populate its fields with data.
function spex::getEbounds, nchan, energy


struct = add_tag(struct, 0L, 'CHANNEL')
struct = add_tag(struct, 0.0, 'E_MIN')
struct = add_tag(struct, 0.0, 'E_MAX')

input = nchan GT 1L ? Replicate( struct, nchan ) : struct

input.( 0L ) = input.( 0L ) + indgen(nchan)
input.( 1L )  = input.( 1L )  + energy[0,*]
input.( 2L )  = input.( 2L ) + energy[1,*]

return, input

end

;--------------------------------------------------------------------------
; Prompt user for the output file name.

pro spex::getRateFilename, outfile, filetype

;print, "in getRateFilename"


IF not is_string(outfile) THEN BEGIN
   file = self->get(/spex_specfile)
   break_file, file, disk, dir, filen, ext
   outfile = filen + filetype + '.fits'
;   words = strsplit(file, '.', /extract)
;   newwords = [words[0], words[1]]
;   outfile = strjoin(newwords, filetype)
   outfile = dialog_pickfile (path=curdir(), filter='*.fits', $
       file=outfile, $
       title = 'Select output file',  $
       group=group, $
       get_path=path)
ENDIF


end


;--------------------------------------------------------------------------

; Define the values for the time-related keywords
; 	Timezero, Tstart, Tstop, and Telapse
pro spex::defineTimeKeywords, rate_struct, timeInterval



;print, "in defineTimeKeywords"


ntimes = n_elements(timeInterval)/2   ; spex_summ_time_interval is 2-d array

timezero = anytim( [timeInterval[0,0], timeInterval[1,ntimes-1]],/mjd)
timezerf = 0.0

; Integer part of reference time minus mjdref
rate_struct.timezero = (timezero[0].mjd-rate_struct.mjdref)+timezerf

rate_struct.tstarti = timezero[0].mjd-rate_struct.mjdref
rate_struct.tstartf = double(timezero[0].time) /8.64d7   ; fraction of the day in seconds
rate_struct.tstopi = timezero[1].mjd-rate_struct.mjdref
rate_struct.tstopf = double(timezero[1].time) / 8.64d7   ; fraction of the day in seconds
rate_struct.telapse = $
    double(((rate_struct.tstopi-rate_struct.tstarti) + $
       (rate_struct.tstopf-rate_struct.tstartf))*86400.0)

; If there is no GTI information available and there are no dead time corrections,
;   THEN EXPOSURE, ONTIME and TELAPSE should contain the same value.
rate_struct.exposure = rate_struct.telapse

end

;--------------------------------------------------------------------------

; Define keywords for Rate file
function spex::defineRateKeys, timeInterval, version

;print, "in defineRateKeys"

rate_struct = {rate_header}

; Determine name of the
instrument = (self->get(/spex_data_units)).DATA_NAME

telescop = ' '
;IF (strcmp((instrument, 'RHESSI') OR (str) THEN BEGIN
;IF ((strcmp(instrument, 'RHESSI')) OR (strcmp(instrument, 'HESSI'))) THEN BEGIN
;instrument = "unknown"
IF stregex( instrument, 'hessi',/FOLD_CASE,/BOOLEAN) THEN BEGIN
   telescop = instrument
   rate_struct.chantype = 'PI'
ENDIF ELSE IF (strcmp(instrument, 'XSM')) THEN BEGIN
   telescop = 'SMART-1'
   respfile = self->get(/spex_drmfile)
   extno = get_fits_extno(respfile, 'PDR_1')
   n = 0
   IF (extno NE -1) THEN BEGIN
      hdr = headfits(respfile, exten=extno)
      rate_struct.chantype =  fxpar(hdr, 'CHANTYPE', count=n)
   ENDIF
   IF ((extno EQ -1) OR (n EQ 0)) THEN $
      rate_struct.chantype = 'PHA'
ENDIF ELSE BEGIN
   telescop = "unknown"
   rate_struct.chantype = " "
ENDELSE


rate_struct.telescope = telescop
rate_struct.instrument = instrument
rate_struct.origin = telescop
rate_struct.object = 'Sun'
rate_struct.equinox = 2000.0
rate_struct.radecsys = 'FK5'
rate_struct.author = 'OSPEX'
rate_struct.timeunit = "d"
rate_struct.timeref = "LOCAL"
rate_struct.tassign = "SATELLITE"
;rate_struct.chantype = 'PI'
rate_struct.areascal = 1.0
rate_struct.backscal = 1.0
rate_struct.corrscal = 1.0
rate_struct.exposure = 1.0
rate_struct.grouping = 0
rate_struct.quality = 0
rate_struct.clockcor = 'T'
mjdref = anytim('00:00 1-Jan-79', /MJD)
rate_struct.mjdref = mjdref.mjd
rate_struct.timesys = strmid(anytim('00:00 1-Jan-79', /ccsds), 0, 19)
rate_struct.hduclas2 = 'TOTAL'   ; Extension contains a spectrum.
rate_struct.poisserr = 'F'
rate_struct.version = version
rate_struct.backapp = 'F'
rate_struct.deadapp = 'T'
rate_struct.vignapp = 'F'
rate_struct.observer = 'Unknown'
rate_struct.timversn = 'OGIP/93-003'


; define time keywords
self->defineTimeKeywords, rate_struct, timeInterval


return, rate_struct


end
;--------------------------------------------------------------------------

; Define a structure that will hold data for the RATE extension and
;    fill it up with data
function spex::setupBkRateStruct, dstr, timeInterval

;print, "in setupBkRateStruct"


; Determine number of energy bins
nchan = n_elements(self->get(/spex_ct_edges))/2

; Create structure to be written to the fits file
struct = {spec_num: 0, $
    channel: lonarr(nchan), $
    time: 0.0D, $
    timedel: 0.0, $
    livetime: 0.0, $
    ;exposure: 0.0D, $
    rate: fltarr(nchan), $
    stat_err: fltarr(nchan) }

; If background is defined, add BK_RATE and BK_ERROR columns
tags = tag_names(dstr)
pos = where (stregex (tags, '^BKDATA$', /boolean, /fold_case), count)
IF (pos GE 0) THEN BEGIN
   IF (dstr[0].bkdata[0] NE -1) THEN $
      struct = CREATE_STRUCT(struct, 'bk_rate', fltarr(nchan), 'bk_error', fltarr(nchan))
ENDIF


; Fill up the structure with data
ntimes = n_elements(timeInterval)/2   ; spex_summ_time_interval is 2-d array

str = ntimes GT 1L ? Replicate( struct, ntimes ) : struct

str.spec_num  = str.spec_num  + indgen(ntimes) + 1
str.channel  = str.channel + rebin(lindgen(nchan), nchan, ntimes)

; Start and stop times of packets in MJD.
ut = anytim( timeInterval, /MJD )


; calculate the offsets since start day
ut = (ut.mjd-ut[0,0].mjd) + ((double(ut.time)/1000.) + (ut.mjd-ut[0,0].mjd)*86400)

str.timedel  = reform(ut[1,*]-ut[0,*])
str.time  = reform(ut[0,*]) + str.timedel/2.0

str.rate      = dstr.data
str.stat_err  = dstr.edata
str.livetime  = reform(dstr.ltime[0,*]) / str.timedel
;str.exposure  = reform(dstr.ltime[0,*])

; If background is defined, fill up the background related fields.
IF (pos GE 0) THEN BEGIN
   IF (dstr[0].bkdata[0] NE -1) THEN BEGIN
      str.bk_rate  = dstr.bkdata
      str.bk_error = dstr.ebkdata
   ENDIF
ENDIF

return, str

end

;--------------------------------------------------------------------------

; Create the output file in RATE format.  Write RATE and EBOUNDS extensions
;   to it.  If applicable, write BKINFO and HESSI Object Parameter
;   extensions as well.
pro spex::writeRateFile, dstr, timeInterval, outfile

;print, "in writeRateFile"

version = '1.00'   ; version number of file format


; define additional keys specific to this file
sdate = anytim(/ccsds, minmax(anytim( timeInterval, /sec )))


; define keywords for RATE fits files
rate_struct = self->defineRateKeys(timeInterval, version)
rate_struct.detchans = n_elements(self->get(/spex_ct_edges))/2


; Create a fitswrite object
fptr = self->createFits(rate_struct, sdate, version, outfile, header)


; Get the name of the strategy.  If it contains the word SPECFILE, then the
;    spectrum was read from a file.  In this case, read the header from the
;    spectrum file, and copy it to the new file.  Otherwise, don't create
;    this extension and display a message informing the user.
o = self->get(/obj, class='spex_data')
strategy = obj_class(o->getstrategy())

readSpecFile = 0
;IF ((strpos(strupcase(strategy), 'XSM') LT 0) AND $
;    (strpos(strupcase(strategy), 'SPECFILE') GE 0)) THEN $
;   readSpecFile = 1
IF ((strpos(strupcase(strategy), 'XSM') LT 0) AND $
   is_fits(self->get(/spex_specfile)) ) then readSpecFile = 1
   
keysToFilter = ['TTYPE', 'TUNIT', 'TFORM', 'TDIM', 'XTENSION', 'BITPIX', $
                'NAXIS', 'PCOUNT', 'GCOUNT', 'TFIELDS', 'COMMENT  ***', $
                'COMMENT          ', 'End of mandatory fields']

; Create rate header, add keywords and write to file
self->createBkRateExt, fptr, header, rate_struct, 1, keysToFilter, dstr, $
                sdate, timeInterval, version, readSpecFile

; Create ebounds extension, add keywords and write to file
self->createEboundsExt, fptr, rate_struct, 2, sdate, version, readSpecFile, $
                keysToFilter=keysToFilter

; Create BACKINFO extension, add keywords and write to file
self->createBckInfoExt, fptr, rate_struct, 3, sdate, version

; Copy HESSI Spectral Object Parameters extension from spectral file, if
;    available
self->createSpecParExt, fptr, 4, readSpecFile

destroy, fptr

end

;--------------------------------------------------------------------------

; Create a FITSWRITE object containing the primary header
function spex::createFits, rate_struct, sdate, version, outfile, header

;print, "in createFits"

; Create a fitswrite object
fptr = fitswrite()

; Set filename for fptr object
fptr->Set, filename=outfile

; Create primary header, add keywords and write to file
header = fptr->getheader( /primary )

self->add_primary_keywords, fptr, rate_struct, sdate, version
fptr->write_primary;, outfile

return, fptr

end

;--------------------------------------------------------------------------

; Create RATE extension and write it to disk.
pro spex::createBkRateExt, fptr, header, rate_struct, ext, keysToFilter, dstr, $
             sdate, timeInterval, version, readSpecFile


;print, "in createBkRateExt"

;; If spectrum was read from a file, read it and retrieve value of DETUSED from the header.
;detused = ''
;IF (readSpecFile) THEN BEGIN
;   ; Get the name of the original spectrum file
;   f = self->get(/spex_specfile)
;
;   extno = get_fits_extno(f, 'RATE')
;   IF (extno NE -1) THEN BEGIN
;      rate_hdr = headfits(f, exten=extno)
;      rate_hdr = header_filter(rate_hdr, keysToFilter)
;      detused = fxpar(rate_hdr, 'DETUSED')
;   ENDIF ELSE BEGIN
;      message, 'RATE extension not found in specified file, returning...'
;   ENDELSE
;ENDIF
detused = self->get(/spex_detectors)

; Create RATE header related information.
rate_struct.area = self->get(/spex_area)
ntimes = n_elements(timeInterval)/2
rate_hdr = mk_rate_hdr(header, rate_struct, N_ROWS=ntimes, fptr=fptr, $
                       ERR_MSG=err_msg, ERR_CODE=err_code )

; Write RATE header to the disk file.
fptr->Set, extension=ext
fptr->setheader, rate_hdr

; If user created the original spectrum on the fly, write more keywords to the
;    rate header.
rate_struct.fitFunction = self->get(/fit_function)
self->add_rate_keywords, fptr, rate_struct, sdate


; Add additional keywords to the rate header.
IF (strlen(strtrim(detused,2)) GT 0) THEN $
   fptr->Addpar, 'DETUSED', detused
fptr->Addpar, 'TUNIT3', 's'
fptr->Addpar, 'TUNIT4', 's'
fptr->Addpar, 'TUNIT6', 'counts/s'
fptr->Addpar, 'TUNIT7', 'counts/s'
fptr->Addpar, 'TUNIT8', 'counts/s'
fptr->Addpar, 'TUNIT9', 'counts/s'

; Get the structure containing data to be written.
rateInfo = self->setupBkRateStruct(dstr, timeInterval)

; Write data and keys to RATE extension.
fptr->write, rateInfo


end

;--------------------------------------------------------------------------

; Create EBOUNDS extension and write it to disk
pro spex::createEboundsExt, fptr, rate_struct, ext, sdate, version, $
         readSpecFile, keysToFilter=keysToFilter

;print, "in createEboundsExt"

fptr->Set, extension=ext

; If spectrum was read from a file, read it and copy its ENEBAND header to the new file.
make_new_header = 1
IF (readSpecFile) THEN BEGIN
   ; Get the name of the original spectrum file
   f = self->get(/spex_specfile)

   extno = get_fits_extno(f, 'ENEBAND')
   if extno eq -1 then extno = get_fits_extno(f, 'EBOUNDS')
   IF (extno NE -1) THEN BEGIN
      ebounds_hdr = headfits(f, exten=extno)
      ebounds_hdr = header_filter(ebounds_hdr, keysToFilter)
;   ENDIF ELSE BEGIN
;      message, 'ENEBAND extension not found in specified file, returning...'
;   ENDELSE
     fptr->setheader, ebounds_hdr
     make_new_header = 0
   ENDIF
ENDIF
   
if make_new_header then begin
   ; Otherwise, write ENEBAND-related keywords to the header.
   fptr->setheader, hdr, /init
   self->add_ebounds_keywords, fptr, rate_struct, sdate, version
ENDIF

; Specify the units of energy range.
fptr->Addpar, 'TUNIT2', 'keV'
fptr->Addpar, 'TUNIT3', 'keV'

; Get current time and place the 6 elements returned by bin_date in a string array
currtime = strmid(anytim(!stime, /ccsds), 0, 19)
fptr->Addpar, 'DATE', currtime, 'File creation date (YYYY-MM-DDThh:mm:ss UTC)'

; Retrieve energy bounds.
ct_edges = self->get(/spex_ct_edges)

; Retrieve energy ranges to be written to the new file.
eboundsInfo = self->getEbounds(n_elements(ct_edges)/2, ct_edges)

; Write data and keys to EBOUNDS extension
fptr->write, eboundsInfo


end


;--------------------------------------------------------------------------

; Create HESSI Spectral Object Parameters extension and write it to disk
pro spex::createSpecParExt, fptr, ext, readSpecFile

;print, "in createSpecParExt"

; If spectrum was read from a file, read it and copy its HESSI Spectral Object
;    Parameter header to the new file.
IF (readSpecFile) THEN BEGIN
   ; Get the name of the original spectrum file
   f = self->get(/spex_specfile)

   ; Read Spectral Object Parameters extension from the spectrum file and copy
   ;    header and data to the new file.
   rd_fits_ext, file=f, extname='HESSI Spectral Object Parameters', $
                setnumber=1, header=specpar_hdr, data=data, err_code=err_code, /silent
   IF (err_code EQ 0) THEN BEGIN
      fptr->Set, extension=ext
      fptr->setheader, specpar_hdr

      ; Get current time and place the 6 elements returned by bin_date in a string array
      currtime = strmid(anytim(!stime, /ccsds), 0, 19)
      fptr->Addpar, 'DATE', currtime, 'File creation date (YYYY-MM-DDThh:mm:ss UTC)'

      ; Write data and keys to EBOUNDS extension
      fptr->write, data
   ENDIF
ENDIF


end
;--------------------------------------------------------------------------

; Create BACKINFO extension and write it to disk
pro spex::createBckInfoExt, fptr, rate_struct, ext, sdate, version

;print, "in createBckInfoExt"

; Get the structure containing data to be written
bckInfo = self->getBckInfo()

; Write data and keys to BCKINFO extension
IF (n_tags(bckInfo) GT 0) THEN BEGIN
   fptr->Set, extension=ext
   fptr->setheader, hdr, /init
   ct_edges = self->get(/spex_ct_edges)
   self->add_bckinfo_keywords, fptr, rate_struct, sdate, version

   fptr->write, bckInfo
ENDIF


end

;--------------------------------------------------------------------------

; Add BACKINFO-related keywords to the fits structure
pro spex::add_bckinfo_keywords, fptr, rate_struct, sdate, version

;print, "in add_bckinfo_keywords"

; Add keywords to the primary header
fptr->Addpar, 'ORIGIN', rate_struct.origin, 'Origin of FITS file'
fptr->Addpar, 'TELESCOP', rate_struct.telescope, 'Telescope or mission name'
fptr->Addpar, 'INSTRUME', rate_struct.instrument, 'Instrument name'
fptr->Addpar, 'DATE-OBS', sdate[0], 'Data start time UT'
fptr->Addpar, 'DATE-END', sdate[1], 'Data end time UT'
fptr->Addpar, 'TIMVERSN', 'OGIP/93-003', 'OGIP memo number where the convention used'
fptr->Addpar, 'EXTNAME', 'BACKINFO', 'Extension name'
opt = self->get(/spex_bk_sep)
IF (opt EQ 0) THEN $
   msg = 'Single background for all energy bands' ELSE $
   msg = 'Separate background for each energy band'
fptr->Addpar, 'MJDREF',   float(rate_struct.mjdref), 'TIMESYS in MJD (d)'
fptr->Addpar, 'TIMESYS',  rate_struct.timesys, 'Reference time in YYYY MM DD hh:mm:ss'
fptr->Addpar, 'TIMEZERO', rate_struct.timezero, 'Start day of the first bin rel to TIMESYS'
fptr->Addpar, 'TSTARTI',  rate_struct.tstarti, 'Integer portion of start time rel to TIMESYS'
fptr->Addpar, 'TSTARTF',  rate_struct.tstartf, 'Fractional portion of start time'
fptr->Addpar, 'TSTOPI',   rate_struct.tstopi, 'Integer portion of stop time rel to TIMESYS'
fptr->Addpar, 'TSTOPF',   rate_struct.tstopf, 'Fractional portion of stop time'
fptr->Addpar, 'TASSIGN',  rate_struct.tassign, 'Place of time assignment'
fptr->Addpar, 'SEPBCK', self->get(/spex_bk_sep), msg
fptr->Addpar, 'VERSION', version, 'File format version number'
eunit = (self->get(/spex_data_units)).ct_edges
fptr->Addpar, 'TUNIT2', eunit
fptr->Addpar, 'TUNIT3', eunit
fptr->Addpar, 'TUNIT4', 's'
fptr->Addpar, 'TUNIT5', 's'

IF (strcmp(rate_struct.instrument, 'XSM')) THEN BEGIN
   fptr->Addpar, 'COMMENT', $
      'absTime[i]=mjd2any(MJDREF+TIMEZERO)+HOUR[i]*3600+MINUTE[i]*60+SECOND[i]'
   fptr->Addpar, 'COMMENT', 'Correct absTime[i] for Day passing through midnight'
ENDIF ELSE $
   fptr->Addpar, 'COMMENT', 'absTime[i] = mjd2any(MJDREF + TIMEZERO) + TIME[i]'

; Get current time and place the 6 elements returned by bin_date in a string array
currtime = strmid(anytim(!stime, /ccsds), 0, 19)
fptr->Addpar, 'DATE', currtime, 'File creation date (YYYY-MM-DDThh:mm:ss UTC)'

end

;--------------------------------------------------------------------------

; Return structure containing Backinfo data.
function spex::getBckInfo

;print, "in getBckInfo"


; Determine whether single background or multiple background option was
;    applied.
opt = self->get(/spex_bk_sep)


IF (opt EQ 0) THEN BEGIN
   return, self->singleBackground()

ENDIF ELSE BEGIN
   return, self->multipleBackground()
ENDELSE


return, result


end

;--------------------------------------------------------------------------

; Return structure filled with background information when a single
;    background option was selected for all energies.
function spex::singleBackground


;print, "in singleBackground"

result = 0
bckT = self->get(this_band=0, /this_time)
IF (n_elements(bckT) GT 1) THEN BEGIN
   temp  = self->get(/spex_ct_edges)
   bckE = fltarr(2,1)
   bckE[0] = temp[0,0]
   bckE[1] = temp[1,(n_elements(temp)/2)-1]
   order = self->get(/spex_bk_order)

   nenergies = n_elements(bckE) / 2
   ntimes = n_elements(bckT) / 2
   IF (nenergies EQ 0) THEN BEGIN
      print, 'number of energies in background spectra is zero, cannot continue...'
   ENDIF

   bckO = order

   ; Get a structure filled with background information
   result = self->fillBackInfoStr(ntimes, nenergies, bckE, bckO)

   ; Set time field
   ut = anytim( bckT, /MJD )

   ; calculate the offsets since start day
   ut = (ut.mjd-ut[0,0].mjd) + ((double(ut.time)/1000.) + (ut.mjd-ut[0,0].mjd)*86400)

   result.tstart = ut[0,*]
   result.tstop = ut[1,*]
ENDIF

return, result


end

;--------------------------------------------------------------------------

; Return structure filled with background information when user selected
;    to calculate a background per energy bin.
function spex::multipleBackground


;print, "in multipleBackground"

bckE  = self->get(/spex_bk_eband)   ; from background screen

nenergies = n_elements(bckE) / 2

nelem = intarr(nenergies)
for i=0, nenergies-1 do begin
  tint     = self->get(this_band=i, /this_time)
  nelem[i] = n_elements(tint)
endfor

ntimes = max(nelem)

bckT = dblarr(ntimes, nenergies)
bckO = intarr(nenergies)

for i=0, nenergies-1 do begin
  tint    = self->get(this_band=i, /this_time)
  bckO[i] = self->get(this_band=i, /this_order)

  for j=0, nelem[i]-1 do begin
     bckT[j,i] = tint[j]
  endfor
endfor

; Get a structure filled with background information
ntimes = ntimes / 2
result = self->fillBackInfoStr(ntimes, nenergies, bckE, bckO)

; Set time field
ut = anytim( bckT, /MJD )

; calculate the offsets since start day
ut = (ut.mjd-ut[0,0].mjd) + ((double(ut.time)/1000.) + (ut.mjd-ut[0,0].mjd)*86400)
t = where(ut < 0)
if (t[0] NE -1) then $
   ut[t] = 0
for i=0, ntimes-1 do begin
   result.tstart[i,*] = ut[i*2,*]
   result.tstop[i,*]  = ut[i*2+1,*]
endfor

return, result


end

;--------------------------------------------------------------------------

; Return a structure filled with background information (energy and
;    polynomial order.  Time will be filled by the caller based on
;    whether it was a single background or multiple for given
;    energy bands.
function spex::fillBackInfoStr, ntimes, nenergies, bckE, bckO

;print, "in fillBackInfoStr"

struct = {spec_num: 0, $
          emin: fltarr(1), $
          emax: fltarr(1), $
          tstart: dblarr(ntimes), $
          tstop: dblarr(ntimes), $
          order:  0 }
s = Replicate(struct, nenergies)
s.spec_num = indgen(nenergies)
s.emin = bckE[0,*]
s.emax = bckE[1,*]
s.order = bckO

return, s


end

;--------------------------------------------------------------------------

; Write a binned background subtracted spectrum
pro spex::writeBinnedBsub, outfile

print, "binned bsub"
fitIntervals = self->get(/spex_fit_time_int)
IF (fitIntervals[0] NE -1) THEN BEGIN
	dstr = self->getData(class='spex_fitint', spex_unit='rate')
	IF is_struct(dstr) THEN BEGIN
       IF (self->get(/spex_have_bk)) THEN BEGIN
	      self->getRateFilename, outfile, '_bksub'

  	      IF outfile ne '' THEN BEGIN
	         self->writeRateFile, dstr, self->get(/spex_fit_time_used),  $
	                    outfile
	      ENDIF ELSE $
	         message,'No output file name selected.  Not saving results.'
 	   ENDIF ELSE $
 	      message, 'Please select a background before choosing this option.
 	ENDIF ELSE $
 	   message, 'Error while retrieving data in the units of rate, returning...'
ENDIF ELSE $
   message, 'Please select Fit time intervals before choosing this option.


end

;--------------------------------------------------------------------------
; Write an unbinned background subtracted spectrum
pro spex::writeBsub, outfile

print, "unbinned bsub"

dstr = self->getData(class='spex_bksub', spex_unit='rate')
IF is_struct(dstr) THEN BEGIN
   IF (self->get(/spex_have_bk)) THEN BEGIN
      self->getRateFilename, outfile, '_bksub'

      IF outfile ne '' THEN BEGIN
         self->writeRateFile, dstr, self->get(/spex_ut_edges), outfile
      ENDIF ELSE message,'No output file name selected.  Not saving results.'

   ENDIF ELSE $
 	  message, 'Please select a background before choosing this option.
ENDIF ELSE $
   message, 'Error while retrieving data in the units of rate, returning...'

end

;--------------------------------------------------------------------------

; Write a  spectrum containing information ahout binned data and
;    background
pro spex::writeBinnedData, outfile

print, "binned data/bck"
dstr = self->getData(class='spex_data', spex_unit='rate')
;IF is_struct(dstr) and is_struct(bkStr) THEN BEGIN
IF (is_struct(dstr) AND dstr.data[0] NE -1) THEN BEGIN
   intervals = self->get(/spex_fit_time_interval)
   IF (n_elements(intervals) GT 1) THEN BEGIN
      self->getRateFilename, outfile, '_data'

      IF outfile ne '' THEN BEGIN
      ;intervals = self->get(/spex_fit_time_interval)
      ;IF (n_elements(intervals) GT 1) THEN BEGIN

         ; Declare the new structure
         nchan = n_elements(self->get(/spex_ct_edges))/2
         struct = { data: fltarr(nchan), $
                    edata: fltarr(nchan), $
                    ltime: dblarr(nchan) }

         ; If background is defined, add 'bkdata' and 'ebkdata' fields to structure
         IF (self->get(/spex_have_bk)) THEN $
            struct = CREATE_STRUCT(struct, 'bkdata', fltarr(nchan), $
                                           'ebkdata',  fltarr(nchan)) $
         ELSE $
            message, 'Warning: No background specified', /info

         ; Bin data in time
         obj = self->get(/obj, class='spex_data')
         z = obj->bin_data(data=dstr, eresult=eresult, ltime=ltime, intervals=intervals, $
             newedges=newedges,  /do_time_bin)
	     IF z[0] eq -1 THEN BEGIN
	         message, 'Binning failed, aborting.'
	     ENDIF

	     dstr = Replicate( struct, n_elements(newedges)/2 )
	     dstr.data = z
	     dstr.edata = eresult
	     dstr.ltime = ltime

         IF (self->get(/spex_have_bk)) THEN BEGIN
   	         ; Bin background data in time
             bkStr = self->getData(class='spex_bk', spex_unit='rate')
             obj = self->get(/obj, class='spex_bk')
	         z = obj->bin_data(data=bkStr, eresult=eresult, ltime=ltime, intervals=intervals, $
	             newedges=newedges,  /do_time_bin)
	         IF z[0] eq -1 THEN $
	            message, 'Binning failed, aborting.'

   	         dstr.bkdata = z
	         dstr.ebkdata = eresult
         ENDIF

	     self->writeRateFile, dstr, newedges, outfile

      ENDIF ELSE $
         message,'No output file name selected.  Not saving results.'
   ENDIF ELSE $
      message, 'Please select Fit time intervals before choosing this option.'
ENDIF ELSE $
   message, 'Error while retrieving data, returning...'

end

;--------------------------------------------------------------------------

; Write spectrum containing information about the original data and
;    background.
pro spex::writeData, outfile


print, "unbinned data/bck"
dstr = self->getData(class='spex_data', spex_unit='rate')
bkStr = self->getData(class='spex_bk', spex_unit='rate')
IF is_struct(dstr) and is_struct(bkStr) THEN BEGIN
   self->getRateFilename, outfile, '_data'

   IF outfile ne '' THEN BEGIN
      times = self->get(/spex_ut_edges)
      nchan = n_elements(self->get(/spex_ct_edges))/2

      if (self->get(/spex_have_bk)) then begin
         ; Append bkdata and ebkdata to dstr
         dstr = CREATE_STRUCT(dstr, 'bkdata', fltarr(nchan,n_elements(times)/2), $
                                    'ebkdata',  fltarr(nchan,n_elements(times)/2))
         dstr.bkdata = bkStr.data
         dstr.ebkdata  = bkStr.edata
      endif else $
         message, 'Warning: No background specified', /info

      self->writeRateFile, dstr, times, outfile
   ENDIF ELSE $
      message,'No output file name selected.  Not saving results.'

ENDIF ELSE $
   message, 'Error while retrieving data in the units of rate, returning...'


end

;--------------------------------------------------------------------------
; This procedure creates a header with appropriate keywords file containing FIT results.
;    It calls fit_results method to read spectral data into a structure and writes this
;    information to the RATE extension in a FITS file.
; This procedure is called only with /fit_results option is on.
pro spex::fit_results_write, fptr, header, rate_struct, ext, sdate, $
                         timeInterval, version

; Fill up a structure containing header information for the RATE extension
ntimes = n_elements(timeInterval)/2
rate_hdr = mk_rate_hdr(header, rate_struct, N_ROWS=ntimes, fptr=fptr, $
                       ERR_MSG=err_msg, ERR_CODE=err_code )

; Create RATE extension in the file pointed to by "fptr" and write rate_hdr to it.
; Add few more keywords that
fptr->Set, extension=ext
fptr->setheader, rate_hdr
self->add_rate_keywords, fptr, rate_struct, sdate

; Add additional RATE keywords to this header that are specific to the file generated
;    by the /fit_results option.
fptr->Addpar, 'TUNIT3', 's'
fptr->Addpar, 'TUNIT5', 'counts/s'
fptr->Addpar, 'TUNIT6', 'counts/s'
fptr->Addpar, 'TUNIT7', 'counts/s'
fptr->Addpar, 'TUNIT8', 'counts/s'
fptr->Addpar, 'TUNIT10', 'counts/photon'

; Get the structure containing data to be written
rateInfo = self->fit_results(ntimes, n_elements(self->get(/spex_summ_energy))/2)

; Write data and keys to RATE extension
fptr->write, rateInfo


end

;--------------------------------------------------------------------------

; Create OSPEX Control Parameter extension and write it to disk
pro spex::fit_results_control_ext, fptr, rate_struct, ext, sdate, version


; Get header related information
fptr->Set, extension=ext
fptr->setheader, hdr, /init
self->add_primary_keywords, fptr, rate_struct, sdate, version
fptr->Addpar, 'EXTNAME', 'OSPEX Control Parameters', 'Extension name'
control = self ->get(/control)
control = str_sub2top(control, err_msg=err_msg, err_code=err_code)
IF err_code THEN message, err_msg, /cont ELSE fptr->write,control


end

;--------------------------------------------------------------------------
; Create a structure to store spectral data. Populate its fields from
;   spex parameters.

function spex::fit_results, ntimes, nchan

; Create structure to be written to the fits file
nparams = n_elements(self->get(/spex_summ_free_mask))/ntimes
ncomp = fit_function_query (self->get(/spex_summ_fit_function), /ncomp)

struct = {spec_num: 0, $
    channel: lonarr(nchan), $
    time: 0.0D, $
    timedel: 0.0, $
    rate: fltarr(nchan), $
    stat_err: fltarr(nchan), $
    bk_rate: fltarr(nchan), $
    bk_error: fltarr(nchan), $
    chisq: 0.0, $
    convfac: fltarr(nchan), $
    emask: bytarr(nchan), $
    phmodel: fltarr(nchan), $
    residual: fltarr(nchan), $
    filter: 0.0, $
    fit_done: 0.0, $
    freemask: fltarr(nparams), $
    func_spectrum: strarr(ncomp), $
    func_model: strarr(ncomp), $
    chianti_version: '', $
    abun_table: '', $
    params: fltarr(nparams), $
    startpar: fltarr(nparams), $
    minima: fltarr(nparams), $
    maxima: fltarr(nparams), $
    sigmas: fltarr(nparams), $
    uncert: 0.0, $
    maxiter: 0, $
    niter: 0, $
    stopmsg: ' ' }

; Populate fields of the structure.
input = ntimes GT 1L ? Replicate( struct, ntimes ) : struct

input.spec_num  = input.spec_num  + indgen(ntimes) + 1
input.channel  = input.channel + rebin(lindgen(nchan), nchan, ntimes)

time_interval = self->get(/spex_summ_time_interval)

; Start and stop times of packets in MJD.
ut = anytim( time_interval, /MJD )


; calculate the offsets since start day. 19-jun-2009, correct this line - was
; adding an extra 1 second when interval crossed a day boundary because of 
; unnecessary first term.
;ut = (ut.mjd-ut[0,0].mjd) + ((double(ut.time)/1000.) + (ut.mjd-ut[0,0].mjd)*86400)
ut = double(ut.time)/1000. + (ut.mjd-ut[0,0].mjd)*86400

;input.time  = reform(ut[0,*])
input.timedel  = reform(ut[1,*]-ut[0,*])
input.time  = reform(ut[0,*]) + input.timedel/2.0

input.rate  = self->get(/spex_summ_ct_rate)
input.stat_err  = self->get(/spex_summ_ct_error)
input.bk_rate  = self->get(/spex_summ_bk_rate)
input.bk_error  = self->get(/spex_summ_bk_error)
input.chisq  = self->get(/spex_summ_chisq)

input.convfac  = self->get(/spex_summ_conv)
input.emask  = self->get(/spex_summ_emask)
input.phmodel  = self->get(/spex_summ_ph_model)
input.residual = self->get(/spex_summ_resid)
input.filter = self->get(/spex_summ_filter)
input.fit_done = self->get(/spex_summ_fit_done)
input.freemask = self->get(/spex_summ_free_mask)

; There's a bug in the FITS writer - can't handle an ascii value of length 0 or 1 in
; an array of structures.
; Writes without msg, but reading generates:  Error: TDIM/TFORM dimension mismatch
; Add blanks so just in case these values are '', they'll now be '  '

func_spectrum = self -> get(/spex_summ_func_spectrum)
if max(strlen(func_spectrum)) le 1 then func_spectrum = func_spectrum + '  '
func_model = self->get(/spex_summ_func_model)
if max(strlen(func_model)) le 1 then func_model = func_model + '  '

input.func_spectrum = func_spectrum
input.func_model = func_model
input.chianti_version = self->get(/spex_summ_chianti_version)
input.abun_table = self->get(/spex_summ_abun_table)
input.params = self->get(/spex_summ_params)
input.startpar = self->get(/spex_summ_starting_params)
input.minima = self->get(/spex_summ_minima)
input.maxima = self->get(/spex_summ_maxima)
input.sigmas = self->get(/spex_summ_sigmas)
input.uncert = self->get(/spex_summ_uncert)
input.maxiter = self->get(/spex_summ_maxiter)
input.niter = self->get(/spex_summ_niter)
input.stopmsg = self->get(/spex_summ_stop_msg)

return, input

end

;--------------------------------------------------------------------------
; Calls appropriate method to write RATE file.
; If "origint" is set, it writes the un-binned data, otherwise data is
;    binned.
pro spex::writebkfits, bksub=bksub, origint=origint, outfile=outfile


;print, "in writebkfits"

;message, 'testing error', /info


; Call appropriate method based on the keyword values
IF (keyword_set(bksub) AND NOT keyword_set(origint)) THEN BEGIN
   self->writeBinnedBsub, outfile

ENDIF ELSE IF (keyword_set(bksub) AND keyword_set(origint)) THEN BEGIN
   self->writeBsub, outfile

ENDIF ELSE IF (NOT keyword_set(bksub) AND NOT keyword_set(origint)) THEN BEGIN
   self->writeBinnedData, outfile

ENDIF ELSE BEGIN
   self->writeData, outfile

ENDELSE

message, 'Spectrum data saved in file ' + outfile, /info

end

;--------------------------------------------------------------------------

; Creates a RATE file with data related to the current fit.
pro spex::savefit_fits, outfile=outfile

version = '1.1'  ; version number of file format

summ = self->get(/spex_summ)
;instrument = (self->get(/spex_data_units)).DATA_NAME

IF (n_elements(summ.SPEX_SUMM_TIME_INTERVAL) gt 1) THEN BEGIN
    IF not is_string(outfile) THEN BEGIN
        atim = strlowcase( trim( str_replace(anytim(!stime, /vms, /date), '-', '_') ))
        outfile = 'ospex_results_'+atim+'.fits'
        outfile = dialog_pickfile (path=curdir(), filter='*.fits', $
           file=outfile, $
           title = 'Select output FITS file name',  $
           group=group, $
           get_path=path)
    ENDIF


    IF outfile ne '' THEN BEGIN
        dummy = self->get(/spex_summ_energy)
        nchan = n_elements(dummy)/2   ; spex_summ_energy is 2-d array

        ; Create a structure with keyword values - used for RATE extension
        timeInterval = self->get(/spex_summ_time_interval)
        ntimes = n_elements(timeInterval)/2   ; spex_summ_time_interval is 2-d array

        sdate = anytim(/ccsds, minmax(anytim( summ.SPEX_SUMM_TIME_INTERVAL, /sec )))

        rate_struct = self->defineRateKeys(timeInterval, version)
        rate_struct.detchans = nchan
        rate_struct.fitFunction = self->get(/spex_summ_fit_function)
        rate_struct.area = self->get(/spex_summ_area)


        ; Create a fitswrite object
        fptr = self->createFits(rate_struct, sdate, version, outfile, header)

        ; Create a header with fit results, add keywords and write to file
        self->fit_results_write, fptr, header, rate_struct, 1, sdate, timeInterval, version

       ; Create ebounds extension, add keywords and write to file
       self->createEboundsExt, fptr, rate_struct, 2, sdate, version, 0

       ; Create OSPEX control parameter extension
       self->fit_results_control_ext, fptr, rate_struct, 3, sdate, version

        message, 'Fit results saved in file ' + outfile, /info
        destroy, fptr
        
    ENDIF ELSE message,'No output file name selected.  Not saving results.'

ENDIF ELSE BEGIN
    message, 'No Fit Results to save.  Aborting.'
    outfile = ' '
ENDELSE

end


;-----------------------------------------------------------------------------------------------
; Main procedure that writes data to the output file.
; FIT_RESULTS keyword cannot be used in combination with BKSUB and/or BINNED keywords. So this
; procedure can be called in one of the following ways:
;   fitswrite, outfile=outfile   ; Creates a file containing binned background and count data
;   fitswrite, outfile=outfile, /bksub   ; Creates a file containing binned background-
;                                          subtracted data
;   fitswrite, outfile=outfile, /bksub, /origint   ; Creates a file containing unbinned
;                                                   background subtracted data
;   fitswrite, outfile=outfile, /origint   ; Creates a file containing unbinned count/background
;                                            data
;   fitswrite, outfile=outfile, /fit_results   ; Creates a file containing FIT results
;
;-----------------------------------------------------------------------------------------------
pro spex::fitswrite, outfile=outfile, fit_results=fit_results, bksub=bksub, origint=origint

err_msg = ''

checkvar, outfile, ''
err = 0
if spex_get_debug() eq 0 then catch, err
IF err NE 0 THEN BEGIN
    err_msg = 'SPEX::FITSWRITE: '+!err_string
    if !error_state.name eq 'IDL_M_USER_ERR' then print,!error_state.msg else begin
	    help, /last_message, out=out
		prstr,out, /nomore
		print,'Aborting...'
	endelse
    RETURN
ENDIF

IF (keyword_set( fit_results )) THEN BEGIN
   IF ((keyword_set( bksub )) OR (keyword_set( origint ))) THEN BEGIN
      message, 'Cannot combine FIT_RESULTS keyword with BKSUB and/or ORIGINT'
      return
   ENDIF
   self->savefit_fits, outfile=outfile
ENDIF ELSE BEGIN
   self->writebkfits, outfile=outfile, bksub=bksub, origint=origint
ENDELSE


end
