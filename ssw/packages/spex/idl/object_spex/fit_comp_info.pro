;+
; Name: fit_comp_info
; Purpose: Function to initialize fit_comp info params
;
; Written: Kim Tolbert 1-Apr-2006
; Modifications
;
;---------------------------------------------------------------

function fit_comp_info

var = {fit_comp_info}

var.fc_spectrum_options = ['full', 'continuum', 'lines']
var.fc_model_options = ['chianti', 'mewe']

return, var
end