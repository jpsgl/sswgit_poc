;+
; Name: gbm_get_det_cos
;
; Purpose: Return the detector cosines (of angle between each detector and Sun) for Fermi GBM detectors
;
; Method: Locates the poshist file(s) for requested time either already in your current directory, or 
;   copies it (them) from the fermi gbm archive.  Calls gbm_det_cosines to read poshist file(s).  If more
;   than one file, concatenates arrays.
;   
; Arguments:
; date - Either a single time, or a range in anytim format (if sec, since 1979/1/1)
;
; Input Keywords:
; string - if set returns a string array with detector name and cosine value.  If date was a single
;   time, then cosines are for that time.  If date was a time range, then finds the flares during
;   that time range, and returns a string array with the peak time and cosines at that peak time for
;   each flare in the interval.
; 
; Output Keywords:
; ut_cos - returns the array of times corresponding to the cosines returned (only applies if
;   date is a range of times)
; status - 0/1 means failure/success
; 
; Output:
; Returns an array of cosines for each GBM NaI detector for each time interval in time range requested (unless
; /string used, then see above).  Dimensioned (12,ntime)
; 
; Examples:
; 
;   cos = gbm_get_det_cos(['12-Jun-2010 00:00','12-Jun-2010 04:00'])
;   returns COS             DOUBLE    = Array[12, 14401]
;   
;   cos_string = gbm_get_det_cos(['12-Jun-2010 00:00','12-Jun-2010 04:00'],/string)
;   returns string array containing:
;   Time: 12-Jun-2010 00:56:02.862
;   NAI_05 cosine = 0.97
;   NAI_01 cosine = 0.65
;   NAI_03 cosine = 0.64
;   etc.
;   
; Written: Kim Tolbert 23-Feb-2011
; Modifications:
;-

function gbm_get_det_cos, date, ut_cos = ut_cos, status=status, string=string

status = 1

gbm_find_data, date=date, pattern='_poshist_',count=count, file=file
if count eq 0 then begin
  message,/cont,'Could not download poshist file from fermi gbm site. Aborting.'
  status = 0
  return, -1
endif

cos = gbm_det_cosines(file[0], utc=utc)
nfile = n_elements(file)
if nfile gt 1 then begin
  for i=1,nfile-1 do begin
    cos = [[cos], [gbm_det_cosines(file[i], utc=utc2)]]
    utc = [utc, utc2]
  endfor
  s = sort(utc)
  utc = utc[s]
  cos = cos[*,s]
endif

time = anytim(date)

if keyword_set(string) then begin
  if n_elements(time) gt 1 then begin
    cat = gbm_read_cat(status=status)
    if ~status then return, 'Error reading GBM flare catalog.'
    q = where (cat.utpeak[1] gt time[0] and cat.utpeak[1] lt time[1], count)
    if count eq 0 then return,'No GBM flares in time interval.'
    for i=0,count-1 do begin
      tind = append_arr(tind, cat[q[i]].utpeak[1])
      ind = append_arr(ind, value_locate(utc, tind[i]))
    endfor
  endif else begin
    tind = time
    ind = value_locate(utc, time)
  endelse
  for i=0,n_elements(ind)-1 do begin
    s=reverse(sort(cos[*,ind[i]]))
    outi = 'NAI_'+trim(s,'(i2.2)') + ' cosine = ' + trim(cos[s,ind[i]],'(f5.2)')  
    out = append_arr(out, ['Time: '+anytim(tind[i],/vms), outi, ''] )
  endfor
  return, out
endif

ind = value_locate(utc, time)
if n_elements(ind) eq 1 then begin
  ut_cos = utc[ind]
  return, cos[*,ind]
endif else begin
  ut_cos = utc[ind[0]:ind[1]]
  return, cos[*,ind[0]:ind[1]]
endelse

end