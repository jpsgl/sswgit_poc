;+
; Name: FERMI_TIM2MET
;
; Purpose: Convert time in seconds since 79/1/1 to Fermi Mission Elapsed time
; 
; Calling sequence:  met = fermi_tim2met(time)
; 
; Output - time in Fermi mission elapsed time (seconds since 1-jan-2001 with no leap sec)
; 
; Written, Kim Tolbert 24-May-2010
; Modifications:
; 
;-

function fermi_tim2met, time

ref_utc = anytim('1-jan-2001 00:00:00.000')

nleap = fermi_nleap_sec(time)

met = anytim(time) - ref_utc - nleap

return, met
end