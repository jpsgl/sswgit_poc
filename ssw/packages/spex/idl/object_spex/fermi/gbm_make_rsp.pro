;+
;; NAME: gbm_make_rsp
; 
; PURPOSE: Make RSP files for all GBM detectors for a single time interval (or flare). NOTE: You can only run this on 
;   hesperia because the perl and fortran software is set up there.
; 
; METHOD:  The software to generate GBM response matrices is a collection of perl code and a Fortran executable
;  on hesperia in /usr/local/gbmrsp/software. We have to be in that directory to run it, so we cd there
;  temporarily and then cd back to wherever we were.  If a gbm_ql object isn't passed in, we create one here
;  to retrieve the daily files and then delete it.  The daily files are placed in
;  $GMB_DATA_DIR/yyyymmdd if $GBM_DATA_DIR is defined, otherwise curdir()/yyyymmdd.  The rsp files produced
;  are also put in that directory.  The response files are named glg_cspec_nx_bnyymmddfff_vnn.rsp (or rsp2 when
;  there are multiple extensions for different slew positions), where x is the detector number and fff is
;  the fraction of day.  Since fff isn't easily recognizable as a time, we rename the files to 
;  glg_cspec_nx_bnyymmdd_hhmm_fff_vnn.rsp or rsp2.  If the outdir keyword or $GBM_RSP_DIR is defined, the final 
;  location for the renamed files is there in a dir named yyyy/mm/dd, otherwise the files are renamed in place.
;  
;  This is normally run as part of the daily pipeline in gbm_qlook_products.  RSP files are created for
;  every flare interval.  
;  
;  This can also be run standalone by any user on hesperia for any time interval by using a command like:
;  gbm_make_rsp,time_range='31-oct-2010 ' +['02:45','02:50']
;  or
;  gbm_make_rsp,time_range='31-oct-2010 ' +['02:45','02:50'], outdir='/home/kim/rspfiles'
;  or 
;  gbm_make_rsp, flare='100120_1229'
;  Note: the pha files will remain in curdir()/yyyymmdd, for the user to clean up
;  
; Input keywords:
; time_range - time interval to make RSP file for
; flare - flare id to make RSP file for.  Uses flare catalog to convert this to a time_range
; det -  string array of detectors to get files for.  If not set, get all det.  Choices are 
;         ['b0', 'b1', 'n0', 'n1', 'n2', 'n3', 'n4', 'n5', 'n6', 'n7', 'n8', 'n9', 'na', 'nb']
; gbm_ql - qbm_ql object (to get catalog).  If not passed in, creates one and destroys it
; outdir - dir to write RSP files in.  If not set, uses $GBM_RSP_DIR/yyyymmdd, or if GBM_RSP_DIR
;   is not defined, uses curdir()/yyyymmdd
; no_expand - if set, don't expand input photon edges of rsp file for b0 detector
;
; Output keywords:
; result - string result of call to
; 
; Kim Tolbert, May 2010
; Modifications:
; 2-Nov-2010, Kim.  Made changes to enable running standalone by any user:
;   gbm_ql default changed from gbm_cat obj to gbm_ql obj, and changed directories to write in
; 19-Oct-2011, Kim. Added det and no_expand keywords.  If no_expand isn't set, then call gbm_bgo_rsp_mod to modify
;   the b0 rsp file to expand the input energy bins. Also, call SA_GBM_RSP_Gen.pl with -W so namelist file won't be
;   deleted, so we can modify edges and remake b0 rsp file.
; 18-Nov-2011, Kim. Remove csh from spawn command (see note in code)
; 
;-
pro gbm_make_rsp, time_range=time_range, flare=flare, det=det, gbm_ql=gbm_ql, outdir=outdir, result=result, no_expand=no_expand

 checkvar, gbm_ql, obj_new('gbm_ql')
 
if keyword_set(time_range) then time = time_range else begin
  if keyword_set(flare) then begin   
    cat = gbm_ql->get(/cat)
    q = where (cat.id eq flare, k)
    if k eq 0 then begin
      message, /cont, 'Flare ' + trim(flare) + ' not found. Not making rsp files.'
      return
    endif
    time = [cat[q[0]].utstart, cat[q[0]].utend]
  endif
endelse

if n_elements(time) ne 2 then begin
  message, /cont, 'Error.  Specify a time range or a GBM flare number.'
  return
endif

gbm_ql -> getfiles, time, det=det
if ~arg_present(gbm_ql) then destroy, gbm_ql  ; if not passed in, destroy object here
   
radec = solephut(time[0])
aradec = arr2str( [' -R', ' -D'] + trim(radec, '(f6.2)'), ' ')

met = fermi_tim2met(time)
amet = trim(met,'(f20.6)')
atimes = n_elements(amet) eq 1 ? ' -S'+amet : arr2str( [' -S', ' -E'] + amet, ' ')

topdir = chklog('GBM_DATA_DIR')
if ~file_test(topdir, /dir, /write) then topdir = curdir()
datedir = time2file(time[0], /date_only)
dir = concat_dir(topdir, datedir)

; make sure there aren't any rsp files there already
oldfiles = file_search(dir,'*.rsp*', count=count)
if count gt 0 then file_delete, oldfiles, /quiet  

command = './SA_GBM_RSP_Gen.pl -M2 -T -N -W' + aradec + atimes + ' ' + dir

cd, '/usr/local/gbmrsp/software', current=cur_dir
pwd
print,'command = ', command
; spawn runs in whatever shell is specified by SHELL env. var.  When running interactively, this will be the user's login shell.  When
; running in batch from cron, this will Bourne shell.  Now alias heainit is defined for both, so remove csh from spawn (which caused it to
; hang when running interactively)  18-Nov-2011
spawn, 'heainit; ' + command, result
help,result

cd, cur_dir
wait, 10.  ; Seems the files sometimes aren't ready immediately for the this search, so pause
rsp_files = file_search(dir, '*.rsp*', count=count)
atim = anytim(time, /vms)
atims = atim[0] + ' to ' + atim[1]
case 1 of
  count eq 0:  msg = 'No rsp files created for times ' + atims
  count lt 14: msg = '<14 rsp files created for times ' + atims
  count eq 14: msg = 'Successfully created rsp files for ' + atims
  count gt 14: msg = 'Too many rsp files created for ' + atims
endcase

message, /cont, msg

if count gt 0 then begin

  ; For bgo rsp file, rewrite with more input photon energy bnis
  if ~keyword_set(no_expand) then gbm_bgo_rsp_mod, dir
  
  if keyword_set(outdir) then begin
    odir = outdir
    if ~file_test(odir, /dir, /write) then mk_dir, odir
    if ~file_test(odir, /dir, /write) then odir = dir
  endif else begin
    odir = chklog('GBM_RSP_DIR') ne '' ? concat_dir('GBM_RSP_DIR', datedir) : dir
    if ~file_test(odir, /dir, /write) then mk_dir, odir
  endelse
  
  ; change name to include hhmm between date and fraction of day (still include fraction for easy
  ; cross-reference with the fermi group's rsp files)
  if count eq 1 then rsp_files = rsp_files[0]   ; file_move below doesn't work if array of 1 element because dest is scalar
  base = file_basename(rsp_files)
  olds = stregex(base, 'bn[0-9]*_', /extract)
  ext = anytim(time[0],/ext)
  hhmm = '_' + trim(ext[0],'(i2.2)')+ trim(ext[1],'(i2.2)') + '_'
  news = strmid(olds,0,8) + hhmm + strmid(olds,8,4)
  new_name = str_replace(base, olds, news)
;  file_delete, concat_dir(odir,base), /quiet  ; can remove this after transition completely to new names
  file_move,  rsp_files, concat_dir(odir,new_name), /overwrite
  message,/cont, 'RSP files renamed and placed in ' + odir
  
  if dir ne odir then begin
    logf = file_search(dir, '*.logfile', count=count)
    if count gt 0 then file_move, logf, odir, /overwrite
  endif
  
endif

end
