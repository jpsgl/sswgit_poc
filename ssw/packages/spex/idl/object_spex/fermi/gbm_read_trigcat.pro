;+
;; Name: gbm_read_trigcat
;
; Purpose: Read Fermi GBM trigger catalog and return structure of trigger data
;
; Calling Sequence:  tcat = gbm_read_trigcat()
; 
; Keywords:
;  force - if set, force re-reading catalog even if it was read recently (stored in common)
;  status - 1 if successful, 0 if problem
;  
;  
; Modifications:
; 21-Oct-2010, Kim. Dominic changed sock_copy keyword from copy_file to local_file, made same change here
; 15-Nov-2011, Kim. Was calling gbm2tim to convert start/end/trig times to UTC, but realized they are already 
;   UTC (they're in MJD, which includes leap seconds), so can just use mjd2any
;-

function gbm_read_trigcat, force=force, status=status

common gbm_trigcat, trigcat, trigcat_read_time

status = 1

elapsed = 1.e20
if is_string(trigcat_read_time) then elapsed = anytim(!stime) - anytim(trigcat_read_time)

; force read if nothing in trigcat, or we haven't read the file for more than 1 day, or force is set
if (n_elements(trigcat) le 1) or (elapsed gt 86400.) or keyword_set(force) then begin

  url = 'http://heasarc.gsfc.nasa.gov/FTP/heasarc/dbase/dump/heasarc_fermigtrig.tdat.gz'

  sock_copy,url, out_dir=dir,_extra=extra,err=err,local_file=copyfile,$
      cancelled=cancelled,clobber=clobber,/progress,status=status,/verbose
  
  if ~status then return, -1
  
  if os_family(/lower) eq 'unix' then gzip, copyfile, gfile, /unzip else $ ; gzip didn't work on Windows, but can!
   espawn,'C:\ssw\packages\binaries\exe\Win32_x86\GZIP.EXE -df ' + copyfile   
   
  tf = 'heasarc_fermigtrig.tdat'
  cat = transpose(rd_tfile(tf,0,-1,delim='|',/autocol))
  trigcat = replicate ({trig_desig:'', source_desig: '', ra: 0.0, dec: 0.0, $
                        radec_uncert: 0.0, utstart: 0.d0, utend: 0.d0, uttrig: 0.d0, $
                        class: '', class_probability: 0.}, n_elements(cat[*,0]) )
 
  trigcat.trig_desig = cat[*,1]
  trigcat.source_desig = cat[*,2]
  trigcat.ra = float(cat[*,3])
  trigcat.dec = float(cat[*,4])
  trigcat.radec_uncert = float(cat[*,7])
  trigcat.utstart = mjd2any(double(cat[*,8]))
  trigcat.utend = mjd2any(double(cat[*,9]))
  trigcat.uttrig = mjd2any(double(cat[*,10]))
  trigcat.class = cat[*,11]
  trigcat.class_probability = float(cat[*,12])
                     
  trigcat_read_time = !stime

endif

return, trigcat
end