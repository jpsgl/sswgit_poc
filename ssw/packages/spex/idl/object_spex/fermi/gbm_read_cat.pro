;+
; Name:; gbm_read_cat
; Purpose: Quick function to read fermi gbm catalog
; Calling Sequence:  cat = gbm_read_cat()
; Written:  Kim Tolbert, June 2010
;
;-

function gbm_read_cat, _ref_extra=extra, status=status

gbm_cat_obj = obj_new('gbm_cat')

status = 0

if is_object(gbm_cat_obj) then return, gbm_cat_obj->getdata(_extra=extra, status=status) else return, -1

end