;+
; gbm_daily - cron job to make gbm qlook plots, find flares, and make rsp files. Default is to do previous 
; 5 days.
; 
;; Modifications:
; 01-Apr-2014, Kim.  Added call to gbm_check_rsp_b0 to fix b0 rsp files that don't have 199 bins in all ext.
;-

!quiet=1
cd,'/home/softw/fermi'
gbm_qlook_products, ndays=1,  /cleanup
gbm_check_rsp_b0, /rewrite
end
