;+
; Name: gbm_bgo_rsp_mod
;
; Purpose:  Modfify the B0 rsp file to have more input (photon) energy bins.  
; 
; Method:  This routine is called from gbm_make_rsp.  When this routine is called, we have already called the GBM response
;  matrix generator with -W which means it didn't delete the namelist file.  Now we modify the namelist file to change the
;  input energy bins to the ones we want (which are in the sav file /home/softw/fermi/gbm_bgo_200_photon_bins.sav) and 
;  create a new rsp (or rsp2) file with the new input bins.  (Note that there is a limit of 200 bins allowed in the 
;  namelist file for input bins.)  Procedure is:
;    cd to dir with b0 namelist and rsp files
;    Read the namelist file into an string array
;    Restore the save file containing the desired input photon bins
;    Modify the string array from the namelist file to remove the old input edges and insert the new ones.
;    Modify the line that contains nobins_in=mmm to have the new number of bins
;    Save the new namelist back into the same file
;    Set the env var. GBMRSP_NML to point to the new namelist file
;    Run gbmrsp.exe
;    cd back to original dir
;
; Calling arguments:
;  dir - string containing directory where the original ...b0...nml namelist file is
;  
; Written: Kim Tolbert  7-Oct-2011
; Modifications:
; 18-Nov-2011, Kim. Added /sh to spawn to force running in Bourne shell
;-
pro gbm_bgo_rsp_mod, dir

cd, dir, curr=curdir

nml_file = file_search('*b0*.nml', count=knml)
rsp_file = file_search('*b0*.rsp*', count=krsp)

if knml eq 0 then begin
  message, /cont, 'No b0 namelist file in dir ' + dir + '. Can not modify b0 rsp file. Returning.'
  return
endif

text = rd_ascii(nml_file)

restore,'/home/softw/fermi/gbm_bgo_200_photon_bins.sav'  ; restores ELO and EHI of bins we want to use

edges = [elo, last_item(ehi)]
nedges = n_elements(elo)

edg_text = string(edges, format='(6f11.3)')
nedg_text = n_elements(edg_text)

new_edges = ['ebin_edge_in = ' + edg_text[0], $
  edg_text[1:nedg_text-2], $
  edg_text[nedg_text-1] + ' ,']
  
; replace number of bins line with new number of bins
q = (where(strmatch (text, 'nobins_in*'), count))[0]
z = stregex(text[q], 'nobins_in=[0-9]+,', /extract)
text[q] = str_replace(text[q], z, 'nobins_in='+trim(nedges)+',')

; i_edge_out will be index of first line after input bin edges we're replacing 
i_edge_out = where(strmatch (text,'ebin_edge_out*'))

; put together lines of text to go into new namelist file
out = [text[0:q], $
  new_edges, $
  text[i_edge_out:*]]
  
; write new namelist file  
wrt_ascii, out, nml_file, /no_pad

; delete existing rsp file for B0, if any, because gbmrsp will fail if it's already there.
if rsp_file ne '' then file_delete, rsp_file, /quiet

; set env var to point to new namelist file and run gbmrsp to create new rsp or rsp2 file
; note: don't need to unset env var, since the set only holds for the spawned comamnds.  Also note
; that must use sh syntax for setting env var (export), not csh or tcsh syntax (setenv) 

; use /sh on spawn command to force using bourne shell so export command will work
cmd = 'export GBMRSP_NML=' + nml_file + '; gbmrsp.exe'
print, 'Spawning command: ', cmd
spawn, cmd, result,/sh
help,result

; Don't really know if successful, but result is usually a short array if it failed
if n_elements(result) gt 5 then message,/cont,'Rewrote ' + rsp_file + ' with expanded input energy bins.'

cd,curdir

end
