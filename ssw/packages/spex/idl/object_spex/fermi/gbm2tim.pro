function gbm2tim, gbmt

ref_utc = anytim('1-jan-2001 00:00:00.000')
new_utc =anytim(mjd2any(gbmt))

  ; Compute # leap seconds that happened since reference time and add to sec
ref_tai = anytim(ref_utc, /tai)
new_tai = anytim(mjd2any(gbmt), /tai)
leap_sec = (ref_tai - ref_utc) - (new_tai - new_utc) 

return, new_utc + leap_sec
end





;  ref_utc = mjd2any(fxpar(shdr, 'MJDREFI')) ; UTC reference time in sec since 79/1/1, ignore MJDREFF (TT)
;  base_sec = fxpar(shdr,'TZERO4')
;  sec = ref_utc + base_sec  ; this is elapsed sec since UTC reference time of 2001.
;  ; Compute # leap seconds that happened since reference time and add to sec
;  ref_tai = anytim(ref_utc,/tai)
;  sec_tai = anytim(sec, /tai)
;  leap_sec = (ref_tai - ref_utc) - (sec_tai - sec)
;  start_time = sec + leap_sec  ; base time to add time arrays to
  