;+
; Name: gbm_find_data
; 
; Purpose - Retrieve GBM files from Fermi GBM archive for specified time. For poshist files (only) look in local
;  directory first (by constructing file name with date and wildcards for version and searching).
; 
; Input keywords:
;   date - either a single time, or a time range, in anytim
;          format (if seconds then sec since 1979/1/1)
;   pattern - pattern in file names to retrieve, default = '_cspec_|_poshist_'   (| means or)
;   det - string array of detectors to get files for.  If not set, get any det.  Choices are 
;         ['b0', 'b1', 'n0', 'n1', 'n2', 'n3', 'n4', 'n5', 'n6', 'n7', 'n8', 'n9', 'na', 'nb']
;   copy - if set, copy files to local computer, default is 1
;   dir - dir to copy to, default is current dir
;
; Output keywords:
;   count - number of files found/copied
;   file - name of files found
;   err - string containing any error messages, blank string if no error
;   
; Examples:
;   Copy cspec and poshist files for 12-jun-2010 to current directory
;     gbm_find_data, date='12-jun-2010'
;   Copy cspec file for NaI detector 5 and return number of files copied in count and name of copied file in file
;     gbm_find_data, date='12-jun-2010', pattern='cspec', det='n5', count=count, file=file
;   Copy poshist file for date
;     gbm_find_data, date='12-jun-2010', pattern='poshist'
;
; Kim Tolbert
; Modifications:
;  23-Feb-2011, Kim.  Added file output keyword.  And previously date was single value and 
;  added 1 day to get range. Now can accept range, or if single value add 1 min - will 
;  still get daily file. Also added header comments.
;  19-Oct-2011, Kim.  Added det keyword.
;  27-Oct-2011, Kim.  If only searching for poshist files, search in current dir first.
;  04-Nov-2011, Kim.  Added err keyword, and if not copying return url in file keyword
;-

pro gbm_find_data, date=date, pattern=pattern, det=det, copy=copy, dir=dir, count=count, file=file, err=err

checkvar, pattern, '_cspec_|_poshist_'
checkvar, copy, 1
checkvar, dir, curdir()

time_range = n_elements(date) eq 1 ? anytim(date) + [0,1.d0] : anytim(date)

if strpos(pattern,'poshist') ne -1 and strpos(pattern,'|') eq -1 then begin
  filestofind = gbm_time2posfile(time_range)
  file = file_search( filestofind, count=count)
  if count eq n_elements(filestofind) then return
endif

spex_find_data, 'fermi_gbm', time_range, pattern=pattern, det=det, url=url, copy=copy, dir=dir, count=count, file=file, err=err

if ~copy then file = url

return
end