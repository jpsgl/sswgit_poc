
;+
;
; NAME:
;   GBM_CORRECT_OVERFLOW
;
; PURPOSE:
;  Corrects 2 byte counter overflow in the gbm spectral files
;
;
; CATEGORY:
;       OSPEX, FERMI, GBM
;
; CALLING SEQUENCE:
;		corrected_counts = GBM_correct_overflow( spec, ebounds)
; CALLS:
;
; INPUTS:
;	Spec - gbm count rate structure from pha file, only overflow intervals are changed
;	Spec is obtained from mrdfits( phafile, 2, /dscale)
;	dscale must be used or counts will be a 2-byte integer field that can't
;	hold the corrected value properly
;	** Structure <7c92700>, 5 tags, length=1048, data length=1046, refs=1:
;	   COUNTS          DOUBLE    Array[128]
;	   EXPOSURE        FLOAT           4.08290
;	   QUALITY         INT              0
;	   TIME            DOUBLE      3.1930713e+008
;	   ENDTIME         DOUBLE      3.1930713e+008
;	Ebounds - structure with energy calibration
;		IDL> ebounds = mrdfits(f,1,/fscale)
;		MRDFITS: Binary table.  3 columns by  128 rows.
;		IDL> help, ebounds,/st
;		** Structure <d3227c0>, 3 tags, length=12, data length=10, refs=1:
;		   CHANNEL         INT              0
;		   E_MIN           FLOAT           4.60264
;		   E_MAX           FLOAT           5.52407

;
; OUTPUTS:
;	Function returns the corrected counts with overflow removed

; PROCEDURE:
;	Single oveflows are identified in gbm spectra by identifying
;	times with high count rates and then looking through pairs of channels starting
;	from higher energies without overflows and looking at the lower energy neighbor
;	to see if the ratio is well beyond the normal range.  If the low to high ratio is
;	too low, then by adding 65536 to the low channel we see if the ratio is in the
;	acceptable range.  If it is, then we add the 65536 overflow to the low channel and
; 	repeat until the 2nd channel from the bottom. Channel 0 cannot overflow in 4 seconds.
;
; Modification History:
;
; 18-feb-2011 richard.schwartz@nasa.gov
; 21-aug-2012, richard.schwartz@nasa.gov, bypass for ctime, no correction for ctime
;
;------------------------------------------------------------------------------
function gbm_correct_overflow, spec, ebounds

is_ctime = n_elements(spec[0].counts) eq 8
nchan = is_ctime ? 2 : 30 ; channels to check 0:29
cnts = size(spec,/tname) eq 'STRUCT' ? spec.counts : spec
z   = where( total( cnts,1) gt 5e4, nz)
if nz eq 0 or is_ctime then return, cnts

cnt = cnts[0:nchan,z]
de = ebounds.e_max-ebounds.e_min
overflow = 65536L
;for a big corrected flare ratio of  (counts[i]/de[i]) / (counts[i+1]/de[i+1]) for more than 50 counts
;These are the range of ratios found over the range of a very large flare
mm =  reform( [0.03698,0.15376, $
				0.23037,0.83268, $
				0.39549,1.08409, $
				0.55422,1.51232, $
				0.63879,1.33963, $
				0.71062,1.42990, $
				0.68982,1.31681, $
				0.78657,1.42771, $
				0.78495,1.42371, $
				0.80852,1.59290, $
				0.64080,1.50979, $
				0.76925,1.61912, $
				0.80392,1.57055, $
				0.75755,1.64533, $
				0.96052,1.74877, $
				0.69335,1.63524, $
				0.77991,1.62717, $
				0.82850,1.82129, $
				0.80954,1.74061, $
				0.78658,1.74768, $
				0.67520,1.72531, $
				0.84923,1.88894, $
				0.73688,1.67904, $
				0.66684,1.56896, $
				0.85455,1.85153, $
				0.81428,2.04368, $
				0.86729,1.89679, $
				0.90748,1.96238, $
				0.92834,1.78881, $
				0.79343,1.88666 ], 2, nchan)

;Start from channel 29 and work down
nb = n_elements(cnt[0,*])
for j=0L,nb-1 do begin

	c = cnt[*,j]
	sl = where( c gt 5000., nsl)
	if nsl ge 0 then begin
		i0 = last_item(sl)
		for i=i0,2,-1 do begin
			otest = (c[i-1]+[0.,overflow])/(fltarr(2)+de[i-1]) / (c[i]/de[i]+fltarr(2))
			test = value_locate( [0.0, .8*mm[0,i-1],1.2*mm[1,i-1],1e9], otest)
			if test[0] eq 0 and test[1] eq 1 then c[i-1]= c[i-1]+overflow
			endfor
		cnt[0,j] = c
		endif
	endfor
cnts[0:nchan, z] = cnt
return, cnts
end
