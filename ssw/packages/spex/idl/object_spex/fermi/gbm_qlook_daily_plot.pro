;+
; NAME: qbm_qlook_daily_plot
;
; Purpose: Create daily composit rate qlook plots for Fermi GBM data.  The output is a stacked plot
;  with the two GOES traces in the top panel, the sum of the 2 BGO detectors in the middle panel, and
;  the 3 most sunward minus the 3 least sunward NaI detectors in the bottom panel.  (sunward/antisunward
;  dets are chosen on a point by point basis).  If png or ps is set, makes plot file,
;  otherwise, plots on screen.
;
; Note: The png plot files are stored in $GBM_QLOOK_DIR and are available over the web.  They can
;  be viewed directly, but are also accessed through the RHESSI Browser.  The size and exact positioning
;  are so they line up with the RHESSI qlook plots.
;
; Calling arguments:
; time - scalar time of day to make daily plot for
;
; Keyword arguments:
; png - if set, make png file. Default name is 'fermi_gbm_daily_rates_yyyymmdd.png, default dir is $GBM_QLOOK_DIR/daily_plots/yyyy/mm
; ps  - if set, make ps  file. Default name is 'fermi_gbm_daily_rates_yyyymmdd.ps, default dir is current dir
; plotfile - plot file name to use for output (if ps or png is set)
; dir - directory to use for plot file
; erange - accept current value of energy edges for rates
;
; Kim Tolbert, May 2010
; Modifications:
;  26-Nov-2011, Kim. Call al_legend instead of legend (IDL V8 conflict)
;  26-Jan-2012, Kim. Use ssw_legend instead of al_legend (had problems with Z-buffer)
;  13-jul-2012, richard.schwartz@nasa.gov, added erange keyword
;-

pro gbm_qlook_daily_plot, time, gbm_cat=ogbm, png=png, ps=ps, plot_file=plot_file, dir=dir, $
	erange = cerange


ymd = time2file(time,/date_only)  ; yyyymmdd actually
datedir = concat_dir(strmid(ymd,0,4), strmid(ymd,4,2))  ; yyyy/mm directory name

daily_files = concat_dir('$GBM_DATA_DIR', ymd)

default, cerange, [6,12,25,50,100,300.]
gbm_qlook_single, daily_files,  data, synrate, $
  erange=cerange, threshold=threshold, testscale=testscale, smoothbin=smoothbin, gtitle=gtitle, status=status
if ~status then return

rate = total(data.df[*,0:2] - data.dr[*,3:5],2)
find_changes, data.dn, dni, dns, count=dnc

berange = [300.,1000.,50000.]
gbm_rd_pha, daily_files, ut2=ut2, data=bdata, type='bgo', erange=berange
brate = total(bdata,2)  ; total b0 and b1 detectors

ncolors = [3,7,9,10,13]  ;nai colors
bcolors = [3,7]         ;bgo colors
gcolors = [3,0]         ;goes colors

savedev = !d.name
savechars = !p.charsize

tvlct,rr,gg,bb,/get
; move linecolors to after set plot to Z or PS because in batch mode can't call it if plot is X
;linecolors

case 1 of
  keyword_set(png): begin
    if ~is_string(plot_file) then begin
      checkvar, dir, concat_dir('$GBM_QLOOK_DIR', concat_dir('daily_plots', datedir))
      if ~file_test(dir, /dir) then file_mkdir, dir
      checkvar, plot_file, 'fermi_gbm_daily_rates_' + ymd + '.png'
      out_file = concat_dir(dir, plot_file)
    endif
    set_plot,'Z'
    device, set_resolution=[640,640]
    !p.charsize=.8
    linecolors
    end

  keyword_set(ps): begin
    savefont = !p.font
    checkvar, dir, curdir()
    checkvar, plot_file, 'fermi_gbm_daily_rates_' + ymd + '.ps'
    out_file = concat_dir(dir, plot_file)
    ps, out_file, /port,/color
    ncolors = [2,10,8,1,4]  ;nai colors
    bcolors = [2,10]         ;bgo colors
    gcolors = [2,10]         ;goes colors
    !p.font=0
    device,/bold
    !p.charsize = .8
    !p.thick=3
    linecolors
    end

  else: linecolors

endcase

dayrange = anytim([anytim(time), anytim(time)+86400.], /ints, /date)
yrange = [100., max(rate[where(rate gt 0.)])]
utplot, data.utm, yrange, /nodata, /ylog, psym=10, color=0, background=255, /xstyle, $
  ytitle='Counts s!u-1!n', position=[.125,.1,.962, .595], timerange=dayrange, /nolabel, xtitle=anytim(time,/date_only,/vms)
for i=0,4 do outplot,data.utm, rate[i,*], color=ncolors[i]
aen = trim(get_edges(cerange,/edges_2), '(i6)')
en_text = reform(aen[0,*] + ' - ' + aen[1,*] + ' keV')
text = 'Three most sunward minus three least sunward NaI detectors'
ssw_legend, [text, en_text], box=0, linest=[-99,intarr(5)], color=byte([0,ncolors]), textcolor=0, thick=!p.thick
if dnc gt 0 then for i=0,dnc-1 do outplot, [data[dni[i]].utm, data[dni[i]].utm], crange('Y'), linestyle=1, color=10
gbm_mark_flare, ogbm, dayrange[0], dayrange[1]

yrange = [1000.,max(brate)]
utplot, data.utm, yrange, /nodata, /ylog, psym=10, color=0, background=255, /xstyle, $
  ytitle='Counts s!u-1!n', position=[.125, .605, .962, .745], timerange=dayrange, $
  /noerase, xtickname=strarr(30)+' ', /nolabel
for i=0,1 do outplot, data.utm, brate[i,*], color=bcolors[i]
aen = trim(get_edges(berange,/edges_2), '(i6)')
en_text = reform(aen[0,*] + ' - ' + aen[1,*] + ' keV')
text = 'Sum of BGO detectors'
ssw_legend, [text, en_text], box=0, linest=[-99,0,0], color=[0,bcolors], textcolor=0
if dnc gt 0 then for i=0,dnc-1 do outplot, [data[dni[i]].utm, data[dni[i]].utm], crange('Y'), linestyle=1, color=10

yrange = [1.e-9, 2.e-3]
utplot, data.utm, yrange, /nodata, /ylog, psym=10, color=0, background=255, /xstyle, $
  ytitle='Watts m!u-2!n', position=[.125, .755, .962, .9], timerange=dayrange, $
  title='FERMI GBM DAILY QUICKLOOK  ' + anytim(time,/date,/vms), /noerase, xtickname=strarr(30)+' ', /nolabel, /ystyle
outplot, data.utm, data.lo, color=gcolors[0]
outplot, data.utm, data.hi, color=gcolors[1]
text = [gtitle, ['1 - 8 A', '.5 - 4 A']]
ssw_legend, text, box=0, linest=[-99,0,0], color=[0,gcolors], textcolor=0
class = ['A','B','C','M','X'] & yclass = 1.e-8 * 10.^findgen(5)
xc = !x.crange
for i=0,4 do xyouts, xc[1] + (xc[1]-xc[0])/100., yclass[i], class[i], color=0
for i=0,4 do oplot, xc, [yclass[i],yclass[i]], linestyle=2, color=0
if dnc gt 0 then for i=0,dnc-1 do outplot, [data[dni[i]].utm, data[dni[i]].utm], crange('Y'), linestyle=1, color=10

timestamp, /bottom, charsize=.7,color=0

if keyword_set(png) then begin
  image = tvrd()
  tvlct, r,g,b,/get
  write_png, out_file, image, r, g, b
  print, 'Wrote ' + out_file
endif

set_plot, savedev
if exist(savefont) then !p.font = savefont
if keyword_set(ps) then psclose
if ~is_batch() then begin
  tvlct, rr,gg,bb
  cleanplot,/silent
endif

end