; dir - daily directories for 1, 2, or 3 days
;
; flare_cat - returned flare catalog structure for requested days

pro gbm_mk_cat_wrapper, dir=dir, erange=erange, flare_cat=flare_cat, nflare=nflare, $
  _extra=_extra

nflare = 0

;Read and organize the cspec rates and compute the day/night and detector cosines
default, erange, [6,12.,25.,50.,100.,300] ; keV
; generate synthetic rate
gbm_qlook, dir, data, synrate, erange=erange, havedata=havedata, _extra=_extra
if ~havedata then return

; onsun contains the start and stop indices of the intervals where the detectors see the Sun and
; are turned on
onsun = gbm_find_contig(data, count=count)

; build an array of flare_cat structures, fill them in, then delete the empty ones
flare_cat = replicate({gbm_flr0}, 300) ;assume fewer than 100 flares per day
iflr=0
for i=0,count-1 do begin
	ix = lindgen(onsun[1,i]-onsun[0,i]+1) + onsun[0,i]
	;This does the business of filling in the catalog information based
	;on the rates and directions in DATA
	tmp_cat = gbm_mk_cat( synrate[ix,0], data[ix], nflare, _extra=_extra)
	if nflare gt 0 then flare_cat[ iflr: iflr+nflare-1] = tmp_cat
	iflr = iflr + nflare
endfor

nflare = iflr
if nflare ge 1 then begin
	flare_cat = flare_cat[0:nflare-1]
	flare_cat.erange = rebin(erange,size(/dim, flare_cat.erange))
	flare_cat.trigger = gbm_match_trigcat(flare_cat)
	flare_cat.rhessi = gbm_match_rhessi(flare_cat)
	
endif else flare_cat = -1

end
