;+
; Name: gbm_check_rsp_b0
;
; Purpose: Check the fermi gbm rsp files for the b0 detector.  We modify the b0 files to extend the photon
;   energy bins to the max number (199) rather than the default 140.  Some files are screwed up though,
;   and have some 140 bin extensions, and some 199 bin extensions.  When I remake them, they seem OK, so don't
;   know what the cause of this was.
;   For each b0 rsp file, we check the dimension of each extension. If they're not all 199, we rewrite
;   the file.  The out string array will have the word 'wrong' in the line following the file name if
;   it needed to be redone.
;   
;   Must be run on hesperia by softw account.
;   
;   This should be run by me every once in a while to clean up the rsp files. First run gbm_check_rsp to
;   clean up any duplicate files (same yymmdd_hhmm, but different fraction of day in name)
;   
; Calling sequence: gbm_check_rsp_b0, out=out, /rewrite
;
; Input Keywords: 
;   rewrite - if set, rewrite files that are wrong.  Ddefault is 0.
;   
; Output Keywords:
;   out - string array of all of the flares that were redone
; 
; Written: Kim Tolbert 4-Sep-2013
; 
;-


pro gbm_check_rsp_b0, out=out, rewrite=rewrite

checkvar, rewrite, 0

f=file_search('/data/fermi/gbm/rsp','*b0*.rsp*')


cat=gbm_read_cat()

out = ''
for i=0,n_elements(f)-1 do begin
  file = f[i]
  if file_test(file, /zero_length) then begin
    out = [out, file + ' is zero bytes.']
    text = 'Empty file.  wrong.'
    goto, do_rewrite
  endif
  next = get_fits_nextend(file)
  out = [out, file + ' # extensions = ' + trim(next)]
  dim = intarr(next-1)
  for j=2,next do begin
    a=mrdfits(file,j,/silent)
    sz = size(a.matrix,/dim)
    dim[j-2] = sz[1]
  endfor
  
  if min(dim) eq 199 and max(dim) eq 199 then continue ; perfect, don't do anything
  
  ; make text string with more info about what's wrong
  if min(dim) eq 140 and max(dim) eq 140 then begin
    text = 'All extensions are wrong'
  endif else begin
    if min(dim) eq 0 then text = 'Some extensions werent read. wrong.' else begin
      q = where(dim ne 199)
      str = arr2str(trim(q+2),',')
      text = 'Some extensions are wrong: ' + str
    endelse
  endelse
  
  do_rewrite:
  if rewrite then begin
    text = text + '  Rewriting.'    
    dir = file_dirname(file)
    id = stregex(file_basename(file),'[0-9]{6}_[0-9]{4}',/extract)
    if is_member(id, cat.id) then gbm_make_rsp, flare=id, det='b0', outdir=dir, /cleanup else begin
      a = mrdfits(file, 0 , hdr)
      stime = fxpar(hdr, 'DATE-OBS')
      etime = fxpar(hdr, 'DATE-END')
      gbm_make_rsp, time_range=[stime,etime], det='b0', outdir=dir, /cleanup
    endelse
  endif
  
  out = [out, text]
  
endfor

end