;+
; NAME: gbm_qlook_orbit_plot
; 
; PURPOSE:  Make composite rate plots of fermi GBM data on RHESSI orbit time boundaries.  The output is a stacked plot
;  with the sum of the 2 BGO detectors in one panel, and the 3 most sunward minus the 3 least sunward NaI detectors in the 
;  lower panel.  (sunward/antisunward dets are chosen on a point by point basis).  If png or ps is set, makes plot file, 
;  otherwise, plots on screen.
;  
; Calling arguments: 
; time - start/end time of interval to make orbit plots for, or
;   if scalar, single time to make plot for (orbit containing that time), OR
;   if scalar, and /day is set then do full day from that start time
;   
; Keyword arguments:
; day - if set, and time is scalar, then do plots for full day from that start time
; png - if set, make png file. Default name is 'fermi_gbm_orbit_rates_yyyymmdd_hhmmss.png, default dir is $GBM_QLOOK_DIR/orbit_plots/yyyy/mm
; ps  - if set, make ps  file. Default name is 'fermi_gbm_orbit_rates_yyyymmdd_hhmmss.ps, default dir is current dir
; plotfile - plot file name to use for output (if ps or png is set)
; dir - directory to use for plot file
;
; Kim Tolbert, May 2010
;  27-Aug-2010, Kim.  Modified position keyword in plot command to align with RHESSI orbit plot
;  26-Nov-2011, Kim. Call al_legend instead of legend (IDL V8 conflict)
;  26-Jan-2012, Kim. Use ssw_legend instead of al_legend (had problems with Z-buffer)
;  
;-

pro gbm_qlook_orbit_plot, time, day=day, gbm_cat=ogbm, png=png, ps=ps, plotfile=plotfile, dir=dir

tr = anytim(time)
if n_elements(tr) eq 1 then tr = keyword_set(day) ? [tr,tr+86400.d0] : [tr, tr+2.d0]

ym = strmid(time2file(tr[0]), 0, 6)  ; yyyymm
datedir = concat_dir(strmid(ym,0,4), strmid(ym,4,2))  ; yyyy/mm directory name

gbm_get_rhessi_orbit_times, tr, orb_start=orb_start, orb_end=orb_end, count=norb
if norb eq 0 then return

; we'll get data into synrate for previous and current day, in case first orbit starts on previous day
ymd = time2file([tr[0]-86400., tr[0]], /date_only)

daily_files = concat_dir('$GBM_DATA_DIR', ymd)

cerange = [6,12,25,50,100,300.]
gbm_qlook, daily_files,  data, synrate, $
  erange=cerange, threshold=threshold, testscale=testscale, smoothbin=smoothbin, gtitle=gtitle, havedata=havedata
if ~havedata then begin
  message,'No data for ' + arr2str(daily_files) + ' so no orbit plots.',/cont
  return
endif

rate = total(data.df[*,0:2] - data.dr[*,3:5],2)
find_changes, data.dn, dni, dns, count=dnc

berange = [300.,1000.,50000.]
gbm_qlook_bgo, daily_files, ut=ut2, data=brate, erange=berange, havedata=havedata

ncolors = [3,7,9,10,13]  ;nai colors
bcolors = [3,7]         ;bgo colors

savedev = !d.name
savechars = !p.charsize
tvlct,rr,gg,bb,/get
; move linecolors to after set plot to Z or PS because in batch mode can't call it if plot is X
;linecolors

case 1 of
  keyword_set(png): begin
    if ~is_string(plotfile) then begin
      checkvar, dir, concat_dir('$GBM_QLOOK_DIR', concat_dir('orbit_plots', datedir))
      if ~file_test(dir, /dir) then file_mkdir, dir
    endif
    set_plot,'Z'
    device, set_resolution=[640,480]
    !p.charsize=.8
    linecolors  
    end

  keyword_set(ps): begin
    savefont = !p.font
    checkvar, dir, curdir()
    ncolors = [2,10,8,1,4]  ;nai colors
    bcolors = [2,10]         ;bgo colors
    !p.font=0
    device,/bold
    !p.charsize = .8
    !p.thick=3
    linecolors
    end
    
  else: linecolors
  
endcase

for i = 0,norb-1 do begin

;  tstart = db[rind[rind[i]]].DATA_START_TIME__ANYTIM_ 
;  tend = db[rind[rind[i+1]-1]].DATA_END_TIME__ANYTIM_  + 180.  ; 3 minutes overlap
;  if tstart lt tr[0] or tstart gt tr[1] then goto, next

  tstart = orb_start[i]
  tend = tstart + 6000.  ; plots are 100 minutes long to match RHESSI ql plots
  filetime = time2file(tstart,/sec)
  if exist(dir) then plotfile = concat_dir(dir, 'fermi_gbm_orbit_rates_'+filetime+(keyword_set(png)?'.png':'.ps') )

  if keyword_set(ps) then begin
    ps, plotfile, /port,/color
    device, /bold
  endif 
    
  i1 = value_locate(anytim(data.utm), tstart) > 0
  i2 = value_locate(anytim(data.utm), tend) > 0
  if i2-i1 lt 10 then begin
    message,'Orbit plot missing. No data for orbit '+atime(tstart)+' to ' + atime(tend), /cont
    goto, next
  endif
  utmi = data[i1:i2].utm
  ratei = rate[*,i1:i2]
  
  q = where(ratei gt 0., count)
  if count eq 0 then goto, next
  yrange = [10., max(ratei[q])]
  utplot, utmi, yrange, /nodata, /ylog, psym=10, color=0, background=255, /xstyle, $
;    ytitle='Counts s!u-1!n', position=[.124,.1,.784, .745], timerange=[tstart,tend]
    ytitle='Counts s!u-1!n', position=[.124,.1,.750, .745], timerange=[tstart,tend]
  for j=0,4 do outplot,utmi, ratei[j,*], color=ncolors[j]
  aen = trim(get_edges(cerange,/edges_2), '(i6)')
  en_text = reform(aen[0,*] + ' - ' + aen[1,*] + ' keV')
  text = 'Three most sunward minus three least sunward NaI detectors'
  ssw_legend, text, box=0, textcolor=0
  ssw_legend, en_text, box=0, linest=intarr(5), color=ncolors, textcolor=0, position=[1.,.7], /right, /norm, charsize=.7
  if dnc gt 0 then for j=0,dnc-1 do outplot, [data[dni[j]].utm, data[dni[j]].utm], crange('Y'), linestyle=1, color=10
  gbm_mark_flare, ogbm, tstart, tend
  
  utm = get_edges(ut2, /mean)
  i1 = value_locate(anytim(utm), tstart) > 0
  i2 = value_locate(anytim(utm), tend) > 0
  utmi = anytim(utm[i1:i2],/ints)
  bratei = brate[*,i1:i2]
  yrange = [1000.,max(bratei)]
  utplot, utmi, yrange, /nodata, /ylog, psym=10, color=0, background=255, /xstyle, $
;    ytitle='Counts s!u-1!n', position=[.124, .755, .784, .95], timerange=[tstart,tend], $
    ytitle='Counts s!u-1!n', position=[.124, .755, .750, .95], timerange=[tstart,tend], $    
    /noerase, xtickname=strarr(30)+' ', /nolabel, title='FERMI GBM QUICKLOOK ON RHESSI ORBIT TIMES'
  for j=0,1 do outplot, utmi, bratei[j,*], color=bcolors[j]
  aen = trim(get_edges(berange,/edges_2), '(i6)')
  en_text = reform(aen[0,*] + ' - ' + aen[1,*] + ' keV')
  text = 'Sum of BGO detectors'
  ssw_legend,text, box=0, textcolor=0
  ssw_legend, en_text, box=0, linest=[0,0], color=bcolors, textcolor=0, position=[1.,.9], /right, /norm, charsize=.7
  if dnc gt 0 then for j=0,dnc-1 do outplot, [data[dni[j]].utm, data[dni[j]].utm], crange('Y'), linestyle=1, color=10
  
  timestamp, /bottom, charsize=.7,color=0

  if keyword_set(png) then begin
    image = tvrd()  
    tvlct, r,g,b, /get  
    write_png, plotfile, image, r, g, b
    print, 'Wrote ' + plotfile
  endif
          
  if keyword_set(ps) then psclose
  
next:
endfor

set_plot, savedev
if ~is_batch() then begin
  cleanplot, /silent
  tvlct, rr,gg,bb
endif

end