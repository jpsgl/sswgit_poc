;+
; Name: gbm_cat__define
;
; Purpose:  Object class to read in and perform various functions on the FERMI GBM flare catalog.
; 
; Method: The default flare list to read is in $SSWDB_GBM/fermi_gbm_flare_list.fits, but cat_dir and
; file are properties and can be changed.  The read_cat method stores the catalog into a common.  On subsequent
; calls to getdata or read_cat, unless force is set, or more than a day has passed, the catalog info is retrieved
; from the common.
; 
; Calling Sequence:   obj = obj_new('gbm_cat')
; 
; Summary of main methods:
;  getdata - function to read contents of catalog
;  getflare - funtion to read index of structure of flare, given the flare id (e.g. 100612_0054)
;  list - function to return ascii list of all or select flares in catalog
;  select - function to allow user to select a flare from widget of flare catalog, and return structure, index or string
;  sunward_dets - function to return an array or string of the most sunward detectors for flare at given index
;  whichflare - function to find flare in given time interval, based on peak time, biggest, closest
;  
; Written: Kim Tolbert, June 2010
; Modifications:
; 14-Feb-2011, Kim. In list, if no non-null lines in text to add, set n_trailer = 0
; 18-Jan-2012, Kim. In list method, use the first trigger in catalog for list (now there are up to 10). Also
;   changed name of text file with flare catalog explanations to append to ascii list (and created that file).
; 14-May-2014, Kim. In list method, increase column width of peak rate and total counts by one.
;-

function gbm_cat::init, _extra=_extra

self.cat_dir = chklog('SSWDB_GBM')
self.file = 'fermi_gbm_flare_list.fits'
self.cat = ptr_new(-1)

self -> set, _extra=_extra

self -> read_cat

return, 1
end

;-----

pro gbm_cat::cleanup
if ~is_struct(*self.cat) then ptr_free, self.cat
end

;-----

pro gbm_cat::set, $
  cat_dir=cat_dir, $
  file=file

if exist(cat_dir) then self.cat_dir = cat_dir
if exist(file) then self.file = file
end

;-----

function gbm_cat::get, $
  cat_dir=cat_dir, $
  file=file, $
  _extra=_extra

if keyword_set(cat_dir) then return, self.cat_dir
if keyword_set(file) then return, self.file

end

;-----
; Returns structure of all flares in gbm catalog

function gbm_cat::getdata, status=status, ptr=ptr, _ref_extra=_extra
status = 1
;if ~self->have_cat() then self -> read_cat, status=status
self -> read_cat, status=status, _extra=_extra
return, keyword_set(ptr) ? self.cat : *(self.cat)
end

;-----

pro gbm_cat::read_cat, force=force, dates=dates, status=status

common gbm_flarecat, flarecat, flarecat_dates, flarecat_read_time
;common gbm_flarecat, flarecat, flarecat_read_time
status = 1

do_read = keyword_set(force)

elapsed = 1.e20
if is_string(flarecat_read_time) then elapsed = anytim(!stime) - anytim(flarecat_read_time)
if elapsed gt 86400. then do_read = 1
if ~ptr_exist(flarecat) then do_read=1 else if ~is_struct(*flarecat) then do_read=1

; force read if nothing in flarecat, or we haven't read the file for more than 1 day, or force is set
if do_read then begin

  file = concat_dir(self.cat_dir, self.file)
  if is_fits(file) then begin
    o = fitsread()
    o -> set, filename = file
    head = o->getheader()
    flarecat_dates = anytim( [fxpar(head, 'DATE-OBS'), fxpar(head, 'DATE-END')])    
    *self.cat = o -> getdata(extnr=1, status=status)
    destroy,o
    if ~status then *self.cat = -1
  endif else begin
    message,'Flare catalog ' + file + ' does not exist yet.', /cont
    status=0
    *self.cat = -1
  endelse
  flarecat_read_time = !stime
  flarecat = self.cat
endif else self.cat = flarecat

if ~is_struct(*self.cat) then status = 0
dates = exist(flarecat_dates) ? flarecat_dates : [0.d0, 0.d0]

end

;-----

function gbm_cat::have_cat, verbose=verbose
self -> read_cat, status=status
if ~status && keyword_set(verbose) then message,'No GBM flare list available.', /info
return, status
end

;-----
;-----
; Returns formatted list of all gbm flares in catalog

function gbm_cat::list, flares=flares, $
  ebin=ebin, $
  sort_field=sort_field, $
  descending=descending, $
  noheader=noheader, $
  n_header=n_header, n_trailer=n_trailer
  
cat = self -> getdata(/force,dates=cat_dates, status=status)
if ~status then return, ''

checkvar, ebin, 1

if keyword_set(flares) then ind = where_arr(cat.id, flares) else $
  ind = lindgen(n_elements(cat))
  
if ind[0] eq -1 then begin
  err_msg = 'No flares found matching requested flare numbers.'
  return, ''
endif  

if keyword_set(sort_field) then begin
  ok = execute ('n_el = n_elements(cat[0].' + sort_field + ')')
  if n_el eq 1 then $
    sort_cmd = 'ind = ind(sort(cat[ind].' + sort_field + '))' $
  else $
    sort_cmd = 'ind = ind(sort(cat[ind].' + sort_field + '[0]))'
  ok = execute (sort_cmd)
  if keyword_set(descending) then ind = reverse(ind)
  flares = cat[ind].id
endif

nind = n_elements(ind)

fform = '(f10.1)'
iform = '(i10)'
fnum = cat[ind].id
stime = anytim (cat[ind].utstart, /vms, /trunc)
ptime = anytim (cat[ind].utpeak[ebin], /vms, /trunc, /time)
etime = anytim (cat[ind].utend, /vms, /trunc, /time)
duration = trim( round( cat[ind].utend - cat[ind].utstart) )
prate = trim (fstring (cat[ind].peak_rate[ebin], format=iform))
tcounts = trim (round (cat[ind].total_counts[ebin]))
sunward = self -> sunward_dets(ind)
trigger = cat[ind].trigger[0]
rhessi = strtrim(cat[ind].rhessi, 2)
q = where (cat[ind].rhessi eq 0, c)
if c gt 0 then rhessi[q] = ''

; for each column specify variable name, first line label, second line label, whether
; label should be centered, and width of column.
fields = ['fnum', 'stime', 'ptime', 'etime', 'duration', 'prate', 'tcounts',  'sunward', $
  'trigger', 'rhessi']
label1 = ['Flare   ', 'Start time    ', 'Peak   ', 'End   ', 'Dur', 'Peak', 'Total', 'Sunward', $
  'Trigger  ', 'RHESSI']

label2 = ['', '', '', '', 's', 'c/s', 'Counts', 'Detectors', '', 'Flare #']

center = [1,1,1,1,0,0,0,1,1,0]

width = [12, 21, 9, 9, 6, 8, 11, 13, 16, 10]

prefix = [1,1,1,1,1,1,1,1,1,1]

ncol = n_elements(label1)

columns = strarr(ncol, n_elements(ind))

if not keyword_set(noheader) then  begin
  list = ['Fermi GBM Flare List  (generated ' + $
      strmid (anytim(!stime, /trunc, /vms), 0, 17) + ')', $
    '', $
    'Total # flares: ' + trim(n_elements(cat)) + '  Time range: ' + $
      anytim(cat_dates[0],/vms) + ' - ' + anytim(cat_dates[1],/vms), $
      '']

  l1 = '' & l2 = ''
  for i = 0,ncol-1 do l1 = l1 + fix_strlen_arr(label1[i], width[i], prefix=(center[i] eq 0), center=center[i])
  for i = 0,ncol-1 do l2 = l2 + fix_strlen_arr(label2[i], width[i], prefix=(center[i] eq 0), center=center[i])

  list = [list, l1, l2, '']
  n_header = n_elements(list)
endif else n_header = 0


for i = 0, ncol-1 do begin

  ok = execute ('column = [fix_strlen_arr(' + fields[i] + ',width[i], prefix=prefix[i])]')
  if i eq 12 then column = '  ' + column
  columns[i,*] = column
endfor

list = reform(append_arr (list, strjoin(columns)))

; Add notes at the bottom of the flare list
text = rd_ascii('$SSW_OSPEX/fermi/fermi_gbm_flare_list_text.txt', error=error)
if ~error and is_string(text) then begin
  list = [list, text]
  n_trailer = n_elements(text)
endif else n_trailer = 0

return, list
end

;-----
;Returns an array or string of the most sunward detectors for flare at index 'index'
;  string look like 'n5 n3 n1 n0'.  If string, only returns 4 most sunward.
; Inputs:
; index - index of flare in flare catalog. If index eq -1 (flare not found) just returns
;   dets in numerical order, i.e. n0,n1,n2...
; array - if set, return array of dets in most to least sunward order.  Otherwise returns a string of first 4 dets.
; ebin - energy bin index to use for cosines

function gbm_cat::sunward_dets, index, array=array, ebin=ebin
checkvar, ebin,1
nind = n_elements(index)
det = ['n'+trim(indgen(10)),'na','nb']
sdet = strarr(12,nind)
for i=0,nind-1 do begin
  if index[i] eq -1 then sdet[*,i] = det else $
    sdet[*,i] = det[reverse(sort((*self.cat)[index[i]].cosines[*,ebin]))]
endfor
return, keyword_set(array) ? sdet : strjoin(sdet[0:3,*], ' ')
end

;-----

; Pops up widget with flare list, lets user select one, and returns cat structure for selected flare unless
; index  - if set, returns index of selected flare
; string - if set, returns string of catalog info for selected flare

function gbm_cat::select, id=id, index=index, string=string

index = -1
if ~self->have_cat(/verbose) then return, -1

list = self->list(n_header=n_header, n_trailer=n_trailer)

if os_family() eq 'Windows' then font = 'fixedsys' else font = 'fixed'
nlist = n_elements(list) - n_header - n_trailer
ind = xsel_list (list, $
                   lfont=font, title='GBM Flare Selection', $
                   subtitle='Select a flare from the list below', $
                   /no_remove, /no_sort, /index, ysize=n_elements(list) < 20, $
                   initial=list[n_header], status=status)
                   
if status and ind ge n_header and (ind-n_header) le (nlist-1) then begin
  if keyword_set(id) then return, (*self.cat)[ind - n_header].id
  if keyword_set(index) then return, ind - n_header
  if keyword_set(string) then return, list[ind]
  return, *self.cat[ind - n_header]
endif

end

;-----

; Returns a short description for flare including start,end,peak time, peak rate and 4 sunward dets
; Arguments:
;  flare - flare id

function gbm_cat::desc, flare

if ~self->have_cat(/verbose) then return, ''

checkvar, ebin, 1

q = where ( (*self.cat).id eq flare, count)

if count gt 0 then begin
  fl = (*self.cat)[q[0]]
  return, '   Flare ' + trim(fl.id) + ': ' + $
    anytim(fl.utstart,/vms) + $
    ' to ' + anytim(fl.utend,/vms, /time) + '   Peak: ' + $
    anytim(fl.utpeak[ebin], /vms, /time) + ', ' + $
    trim(fl.peak_rate[ebin]) + ' c/s  ' + self->sunward_dets(q[0])
endif else return, ''
end

;-----
; Inputs:
; id - flare id
; index - if set, return index (indices) of flares corresponding to id.  Otherwise, return structure

function gbm_cat::getflare, id, index=index, err_msg=err_msg, quiet=quiet
err_msg = ''
quiet = keyword_set(quiet)
if ~self->have_cat(/verbose) then return, -1

if n_elements(id) eq 1 then $
    subb = where ((*self.cat).id eq id, count) else $
    match, id, (*self.cat).id, suba, subb, count=count
if count eq 0 then begin
    err_msg = 'Flare ' + arr2str(strtrim(id,2)) + ' not found in flare list.'
    if not quiet then message, err_msg, /cont
    return, -1
endif

fl = (*self.cat)[subb]

q = get_uniq(fl.id, sorder)
if keyword_set(index) then return, n_elements(sorder) eq 1 ? subb[sorder[0]] : subb[sorder]

fl = fl[sorder]
if n_elements(fl) eq 1 then return, fl[0] else return, fl

end

;-----

; Input:
; intime_range - time range to find flare(s) in
; ebin - energy bin to use if finding biggest or using peak time to find closest
; only_one - only return one flare nearest midpoint of time (overidden by biggest or closest option)
; biggest - return biggest flare in interval (takes priority over only_one)
; closest - if no flares within inteval, return flare closest to start or end time
; structure - if set, return structure of flare(s), default is to return flare id(s)
; index - if set, return index of flare(s), default is to return flare id(s)
; Output:
; err_msg - error string, if any
; count - number of flares found
; 
function gbm_cat::whichflare, intime_range, ebin=ebin, err_msg=err_msg, quiet=quiet, $
    only_one=only_one, biggest=biggest, closest=closest, structure=structure, index=index, count=count

err_msg = ''
quiet = keyword_set(quiet)
checkvar, ebin, 1

if n_elements(intime_range) ne 2 then begin
    err_msg = 'Error - input time range is not 2-D'
    if not quiet then message, err_msg, /cont
    return, -1
endif

sec_range = anytim (intime_range, /sec, error=error)
if error then begin
    err_msg = 'Error in input time range.'
    if not quiet then message, err_msg, /cont
    return, -1
endif


if ~self->have_cat(/verbose) then return, -1

q = where ((*self.cat).utend ge (sec_range[0]-1.d-6) and  $
            (*self.cat).utstart le (sec_range[1]+1.d-6), count)

if count eq 0 then begin
    if keyword_set(closest) then begin
        ms = min ( abs((*self.cat).utend - sec_range[0]), qs)
        me = min ( abs((*self.cat).utstart - sec_range[1]), qe)
        q = ms lt me ? qs : qe
    endif else return, -1
endif

; if more than one flare in interval and only_one or biggest is set, find one closest to center of interval
; or the biggest in the interval
if count gt 1 then begin
  if keyword_set(biggest) then begin
    max_tc = max((*self.cat)[q].total_counts[ebin], max_index)
    q = q[max_index]
  endif else begin
    if keyword_set(only_one) then begin
          midpoint = ( sec_range[0] + sec_range[1] )/ 2.
        diff = abs ((*self.cat)[q].utpeak[ebin] - midpoint)
        mindiff = min (diff, min_index)
        q  = q[min_index]
      endif
    endelse
  
endif

count = n_elements(q)

if keyword_set(index) then return, n_elements(q) eq 1 ? q[0] : q

retval = keyword_set(structure) ? (*self.cat)[q] : (*self.cat)[q].id
if n_elements(retval) eq 1 then return, retval[0] else return, retval

end

;-----

pro gbm_cat__define
void = {gbm_cat, $
  cat_dir: '', $    ; flare catalog directory
  file: '', $       ; flare catalog file name
  cat: ptr_new() $  ; current catalog structure
  }
return & end
   