;+
; NAME: GBM_QL__DEFINE
; 
; PURPOSE: Define an object class to create or use FERMI GBM quicklook products including flare catalog,
;   daily plots, orbit plots, and RSP files for all flares
;   Normally this is used from the routine gbm_qlook_products to make the gbm qlook products, but
;   can also be used to read the flare catalog or get the daily files.
;
; SYNTAX:  obj = obj_new('gbm_ql')
; 
; Kim Tolbert, June 2010
; Modifications:
; 16-Nov-2010, Kim.  If GBM_QLOOK_DIR isn't defined then let gbm_cat::init define cat directory
; 19-Oct-2011, Kim.  Added _extra keyword to getfiles, so we can pass through to gbm_find_data (e.g. det keyword)
; 18-Jan-2012, Kim. In write_cat method, added complete keyword.  If set, the cat passed in via the first arg is
;   the complete catalog, just write it (don't try to merge with old).
;-

function gbm_ql::init, _extra=_extra

self.threshold = 1.e-6
self.testscale = 1.e6
self.erange = ptr_new([6,12.,25.,50.,100.,300]) ; keV
self.bnd_test = 1.e-7
self.smoothbin = 15
self.merge_time = 60.0

self.working_dir = curdir()
; if running from gbm_qlook_products, GBM_QLOOK_DIR will point to dir on hesperia
if chklog('GBM_QLOOK_DIR') ne '' then cat_dir = chklog('GBM_QLOOK_DIR')
self.cat_obj = obj_new('gbm_cat', cat_dir = cat_dir)
self.cat_obj -> read_cat

self -> set, _extra=_extra

return, 1
end

;-----

pro gbm_ql::cleanup
ptr_free, self.erange
destroy,self.cat_obj
end

;-----

pro gbm_ql::set, threshold=threshold, $
  testscale=testscale, $
  erange=erange, $
  bnd_test=bnd_test, $
  smoothbin=smoothbin, $
  merge_time=merge_time, $
  working_dir=working_dir


if is_number(threshold) then self.threshold = threshold
if is_number(testscale) then self.testscale = testscale
if n_elements(erange) gt 2 then *self.erange = erange
if is_number(bnd_test) then self.bnd_test = bnd_test
if is_number(smoothbin) then self.smoothbin = smoothbin
if is_number(merge_time) then self.merge_time = merge_time
if exist(working_dir) then self.working_dir = working_dir
end

;-----

function gbm_ql::get, threshold=threshold, $
  testscale=testscale, $
  erange=erange, $
  bnd_test=bnd_test, $
  smoothbin=smoothbin, $
  merge_time=merge_time, $
  working_dir=working_dir, $
  cat=cat, $
  _extra=_extra

if keyword_set(threshold) then return, self.threshold
if keyword_set(testscale) then return, self.testscale
if keyword_set(erange) then return, get_edges(*self.erange, _extra=_extra)
if keyword_set(bnd_test) then return, self.bnd_test
if keyword_set(smoothbin) then return, self.smoothbin
if keyword_set(merge_time) then return, self.merge_time
if keyword_set(working_dir) then return, self.working_dir
if keyword_set(cat) then return, self.cat_obj->getdata(_extra=_extra)

end

;-----


pro gbm_ql::do_cat, dates_in, cat=cat, status=status

dates = anytim(dates_in, /date_only)
if n_elements(dates) eq 1 then dates=[dates,dates]
curr = dates[0]
nflare_tot = 0
while curr le dates[1] do begin
  self -> make_day_cat, curr, cat=new, nflare=nflare
  if nflare gt 0 then begin
    cat = nflare_tot eq 0 ? new : [cat, new]
    nflare_tot = nflare_tot + nflare
  endif
  curr = curr + 86400.
endwhile
  
self -> write_cat, cat, replace_dates=dates, n_new=nflare_tot, status=status

if ~status then return

list = self.cat_obj -> list()
cat_dir = self.cat_obj->get(/cat_dir)
text_output, list, file_text = concat_dir(cat_dir, 'fermi_gbm_flare_list.txt')

end

;-----
;Input:
; times can be the dates to copy, or the time range to copy.  We find the unique set of dates of those times and get the daily
;  files for those dates.
; Output:
; dir - directories used

pro gbm_ql::getfiles, times, dir=dir, surrounding=surrounding, next=next, _extra=_extra

topdir = chklog('GBM_DATA_DIR')
if ~file_test(topdir, /dir, /write) then topdir = ''

dates = anytim(times, /date_only)

case 1 of 
  keyword_set(surrounding): begin
    for i=0,n_elements(times)-1 do $ 
      dates_all = append_arr(dates_all, timegrid(dates[i]-86400.d0, dates[i]+86400.d0, /days, /quiet, /utime))
    end
  keyword_set(next): begin
    for i=0,n_elements(times)-1 do $
      dates_all = append_arr(dates_all, timegrid(dates[i], dates[i]+86400.d0, /days, /quiet, /utime))
    end
  else: dates_all = dates
endcase
; use ascii times to find unique, since might be insignificant difference in double prec. times, but then
; convert back to seconds
dates = get_uniq(anytim(dates_all,/vms))
dates = anytim(dates)

dir = concat_dir(topdir, time2file(dates, /date_only))
;print,'Output directories: ', dir

for i=0,n_elements(dir)-1 do begin  
  file_mkdir, dir[i]     
  gbm_find_data, date=dates[i], dir=dir[i], _extra=_extra
;  print, trim(count) + ' files copied for day ' + atime(dates[i]) + ' into dir ' + dir[i]
endfor
end

;-----

pro gbm_ql::make_day_cat, date_in, cat=cat, nflare=nflare

date = anytim(date_in, /date_only)
;time_range = n_elements(time) eq 1 ? anytim(time,/date_only) + [0.,86400.] : anytim(time,/date_only)

self -> getfiles, date, /surrounding, dir=dir

gbm_mk_cat_wrapper, dir=dir, flare_cat=cat, nflare=nflare, $
  erange=*self.erange, $
  threshold=self.threshold, $
  testscale=self.testscale, $
  bnd_test=self.bnd_test,$
  smoothbin=self.smoothbin, $
  merge_time=self.merge_time

if nflare gt 0 then begin
  q = where (cat.utstart gt date and cat.utstart lt date+86400.d0, nflare)
  if nflare gt 0 then cat = cat[q]    
endif

message,trim(nflare) + ' flares found on ' + atime(date), /cont

end

;-----
;Write the GBM flare catalog FITS file
; cat - structure with new flares to add (or old flares to replace)
; replace_dates - dates for flares covered by cat (if already exist, but we've removed some flares, need
;   to complete replace this group, so removed flares will be removed from file)
; n_new - number of new flares
; status - 0, if failed to read old catalog
; complete - if set, then cat contains the entire new catalog, don't merge with existing catalog, just write cat
; 
; Currently this rewrites the whole structure each time.  And all in one file.

pro gbm_ql::write_cat, cat, replace_dates=replace_dates, n_new=n_new, status=status, complete=complete

complete = keyword_set(complete)

if complete then begin
  cat_dates = [anytim('12-aug-2008'), max(cat.utend)]
endif else begin

  old_cat = self.cat_obj->getdata(dates=cat_dates, /force, status=status)
  if status then begin
    ; q will be indices where old catalog had start time outside of the days in replace_dates, 
    ; i.e. the old entries to keep
    q = where (old_cat.utstart lt replace_dates[0] or old_cat.utstart gt replace_dates[1]+86400., c)
    if c gt 0 then old_cat = old_cat[q] else old_cat = -1
  endif

  if n_new gt 0 then begin
    message,trim(n_new) + ' flares found and added to flare catalog.',/cont
    if is_struct(old_cat) then begin
      cat = [old_cat, cat]
      q = sort (cat.utstart)
      cat = cat[q]
    endif
  endif else begin
    if ~is_struct(old_cat) then begin
      status = 0
      message, 'No new flares, no old flares.  Exiting.', /cont
      return
    endif
    cat = old_cat
  endelse

  cat_dates[0] = anytim('12-aug-2008')
  cat_dates[1] = cat_dates[1] > replace_dates[1]
endelse

self -> create_fits, cat, cat_dates, fptr=fptr

fptr -> empty_hdu  ; clear out primary header and data to make new ones for extension
fptr -> set, filename=file
fptr -> set,extension=1
fptr -> setdata,cat
fptr -> addpar,'extname','gbm_flare_cat'

fptr -> write, status=status
if status then message,'Wrote flare catalog with total of ' + trim(n_elements(cat)) + ' flares.', /cont
  
destroy, fptr
  
end

;----- 

pro gbm_ql::create_fits, cat, cat_dates, fptr=fptr

fptr = fitswrite()
cat_dir = self.cat_obj -> get(/cat_dir)
file = self.cat_obj->get(/file)
fptr -> set, filename=concat_dir(cat_dir, file)
header = fptr->getheader( /primary )

sdate = anytim(/ccsds, cat_dates)
version=1.00

fptr->Addpar, 'ORIGIN', 'Fermi', 'Origin of FITS file'
fptr->Addpar, 'TELESCOP', 'Fermi', 'Telescope or mission name'
fptr->Addpar, 'INSTRUME', 'GBM', 'Instrument name'
fptr->Addpar, 'OBJECT', 'Sun', 'Observed object'
fptr->Addpar, 'OBSERVER', 'Unknown', 'Name of the user who generated the file'
;fptr->Addpar, 'RA', 0.0, 'Source right ascension in degrees'
;fptr->Addpar, 'DEC', 0.0, 'Source declination in degrees'
;fptr->Addpar, 'RA_NOM', 0.0, 'r.a. nominal pointing in degrees'
;fptr->Addpar, 'DEC_NOM', 0.0, 'dec. nominal pointing in degrees'
;fptr->Addpar, 'EQUINOX', rate_struct.equinox, 'Equinox of celestial coordinate system'
;fptr->Addpar, 'RADECSYS', rate_struct.radecsys, 'Coordinate frame used for equinox'
fptr->Addpar, 'DATE-OBS', sdate[0], 'Data start time UT'
fptr->Addpar, 'DATE-END', sdate[1], 'Data end time UT'
fptr->Addpar, 'TIMVERSN', 'OGIP/93-003', 'OGIP memo number where the convention used'
fptr->Addpar, 'VERSION', version, 'File format version number'
fptr->Addpar, 'AUTHOR', 'gbm_ql', 'Software that created this file'

; Get current time and place the 6 elements returned by bin_date in a string array
currtime = strmid(anytim(!stime, /ccsds), 0, 19)
fptr->Addpar, 'DATE', currtime, 'File creation date (YYYY-MM-DDThh:mm:ss UTC)'

fptr->write_primary

end

;-----
; dates - start / end date to make plots for
; Look in gbm_qlook_daily_plot for input keywords
pro gbm_ql::daily_plot, dates, _extra=_extra
date_days = timegrid(dates[0], dates[1], /days, /string, /quiet)
for i=0,n_elements(date_days)-1 do begin
  self->getfiles, date_days[i], /surrounding
  gbm_qlook_daily_plot,date_days[i], gbm_cat=self.cat_obj, _extra=_extra
endfor
end

;-----
; dates - start / end date to make plots for
; Look in gbm_qlook_orbit_plot for input keywords
pro gbm_ql::orbit_plot, dates, _extra=_extra
dates = anytim(dates)

; include surrounding days for orbits that span days
date_days = timegrid(dates[0], dates[1], /days, /string, /quiet)
for i=0,n_elements(date_days)-1 do begin
  self->getfiles, date_days[i], /surrounding
  gbm_qlook_orbit_plot, date_days[i], /day, gbm_cat=self.cat_obj, _extra=_extra
endfor
end

;----- 
; Look at gbm_make_rsp for input keywords
pro gbm_ql::make_rsp,_extra=_extra, result=result
gbm_make_rsp, _extra=_extra, gbm_ql=self, result=result
end

;-----

pro gbm_ql__define
void = {gbm_ql, $
  threshold: 0., $     ; threshold in synthetic rate for definition of flare
  testscale: 0., $      ; scale factor for synthetic rates (if change, change threshold too)
  erange: ptr_new(), $ ; 1-D array of energy edges used to define flares
  bnd_test: 0., $   ; test value defining time boundaries of flare interval
  smoothbin: 0, $   ; width of smoothing window for constructing synthetic rate
  merge_time: 0., $ ; flare intervals separated by < merge_time are merged into a single flare
  working_dir: '', $; directory in which data directories will be created
  cat_obj: obj_new() $  ; gbm_cat object for reading gbm flare catalog
  }
return & end
   