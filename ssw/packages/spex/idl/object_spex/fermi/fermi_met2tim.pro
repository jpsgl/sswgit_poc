;+
; Name: FERMI_MET2TIM
;
; Purpose: Convert Fermi Mission Elapsed time (seconds since 1-jan-2001 with 
;   no leap sec)to seconds since 79/1/1
; 
; Calling sequence:  tim = fermi_met2tim(met)
; 
; Output - time in seconds since 79/1/1
; 
; Written, Kim Tolbert 14-Mar-2011
; Modifications:
; 
;-

function fermi_met2tim, met

ref_utc = anytim('1-jan-2001 00:00:00.000')

time = met + ref_utc

nleap = fermi_nleap_sec(time)

time = time + nleap

return, time
end