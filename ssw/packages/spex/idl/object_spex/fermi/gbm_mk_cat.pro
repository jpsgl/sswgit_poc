;+
; PROJECT:
; FERMI
; NAME:
; GBM_MK_CAT
;
; PURPOSE:
; This procedures identifies solar flares in the Fermi GBM data stream.
; The flare metadata is put into a data structure that's used to make a metadata catalog
;
; CATEGORY:
; QUICKLOOK AND CATALOG GENERATION FOR FERMI GBM
;
; CALLING SEQUENCE:
;
;
; CALLS:
; none
;
; INPUTS:
;       Synrate - synthetic value that is the product of the front-back GBM detectors
;   in the ranges from ~6-12 keV and ~12-25 keV and the short wavelength GOES XRS
;   channel. Also masked with a day (1)/ night(0) flag but all the data in these intervals
;   will be on the Sun by requirement on the calling program.  Synrate must be contiguous
;   (no data off for SAA or otherwise gt 1 minute) and when the GBM sees the Sun
;   Data- A data structure produced by gbm_qlook
;
;   UTM             STRUCT    -> ANYTIM2INTS_FULL Array[1] ;Time
;   DUT             DOUBLE           4.0960001 ;diff in sec between intervals
;   DF              FLOAT     Array[5, 6] ; five energy ranges of rates, 6 Sunward detectors
;           Nominal energy ranges: [6,12,25,50,100,300] keV, Counts/s
;   DR              FLOAT     Array[5, 6] ;five energy ranges of rates, 6 A-Sunward detectors
;   RATE            FLOAT     Array[5]    ; df-dr, totaled over detectors
;   HI              FLOAT      1.87337e-009 ;Higher energy GOES at UTM
;   LO              FLOAT      3.02004e-007 ;Lower energy GOES at UTM
;   DN              BYTE         1          ;Daynight flag, day is 1
;   DETCOSINES      FLOAT     Array[12]     ;detector cosines to Sun at UTM
;   Synrate - a synthetic rate built from data.rate[0:1], data.hi, and data.dn

;
; OPTIONAL KEYWORD INPUTS:
;   FLR_TEST -DEFAULT,  1E-6 ;TEST VALUE FOR FLARE FROM SCALED PRODUCT OF GBM NAI AND GOES HI
;   BND_TEST -DEFAULT, 1E-7 ;TEST VALUE DEFINING TIME BOUNDARIES OF FLARE INTERVAL
;   SMOOTHBIN - DEFAULT, 15  smooth(synrate, smoothbin) used to define flare boundaries
;   MERGE_TIME -DEFAULT, 60.0 (seconds)- flares with peaks separated by fewer seconds are merged
;
;   ;
; OUTPUTS:
;       none explicit, only through commons;
;
; OPTIONAL OUTPUTS:
; none
;
; KEYWORDS:
; none
; COMMON BLOCKS:
; none
;
; SIDE EFFECTS:
; none
;
; RESTRICTIONS:
; none
;
; PROCEDURE:
; A stretch of contiguous solar observing data synthesized from the the GBM and GOES XRS detectors is searched
; for excursions above a threshold level that roughly corresponds to a low C level flare.
;
; MODIFICATION HISTORY:
; 18-MAR-2010, richard.schwartz@nasa.gov
; 18-Jan-2012, Kim.  Use gbm_flr1 for flare structure definition instead of gbm_flr0.  Contains space for 10 triggers.
;
;-


function gbm_mk_cat, synrate, data, nflare, $
	threshold=threshold,$
	bnd_test=bnd_test,$
	smoothbin=smoothbin, $
	merge_time = merge_time

;synrate and data are over a contiguous range of day with the detectors on (non SAA)

default, threshold, 1e-6 ;test value for flare from scaled product of gbm nai and goes hi
default, bnd_test,1e-7 ;test value defining time boundaries of flare interval
default, smoothbin, 15
default, merge_time, 60.0
utm = anytim(data.utm)
df  = data.df
rate = data.rate

nx = (size( synrate,/dimensions))[0]
ix = lindgen( nx )
isflr = where(synrate gt threshold, nflr)
nflare = 0

if nflr ge 1 then begin ; there is at least one flare time interval

;How many flares?
	rng = find_contig(isflr, ss_sel, fss2d)
	fss2d = n_elements(rng) eq 1 ? isflr[fss2d] :transpose(isflr[fss2d])
	;Merge any intervals separated by less than merge_time

	nflare = n_elements(rng)
	if nflare gt 1 then begin
		for i=nflare-1,1,-1 do $
		if (utm[fss2d[0,i]]-utm[fss2d[1,i-1]]) lt merge_time then begin
			fss2d[1,i-1]=fss2d[1,i]
			fss2d[*,i]=0
			endif
		keep = where( (total(fss2d,1) gt 0 ) and (get_edges(fss2d,/wid) gt 4),nflare)
		if nflare ge 1 then fss2d = fss2d[*,keep]
		endif
	if nflare ge 1 then begin
	flr_cat = {gbm_flr1}

	flr_cats = replicate( flr_cat, nflare)
	;Find ranges around flares, assoc flare with each boundary interval
	;
	smoothtest = smooth(synrate, smoothbin<(nx-1))
	bndtest = where( smoothtest gt bnd_test, nbnd)
	bnd = find_contig(bndtest, ss_sel, bss2d)
	bss2d = n_elements(bnd) eq 1 ? bndtest[bss2d] :transpose(bndtest[bss2d])
	for i=0,nflare-1 do begin
		flr_cat = flr_cats[i]
		isbnd = where( fss2d[0,i] ge bss2d[0,*],count)
		ixsub = ix[bss2d[*,isbnd[count-1]]]
		ixsub = lindgen(ixsub[1]-ixsub[0]+1)+ixsub[0]
		flr_cat.utstart = utm[ixsub[0]]
    flr_cat.id = time2file(flr_cat.utstart, /year2digit)		
		flr_cat.utend = utm[last_item(ixsub)]
		flr_cat.no_start = ixsub[0] eq 0
		flr_cat.no_end = last_item(ixsub) eq (nx-1)

		for k=0,4 do begin
			flr_cat.peak_rate[k] = max( rate[k,ixsub], ip)
			flr_cat.utpeak[k] = utm[ixsub[ip]]
			flr_cat.total_counts[k] = total(rate[k,ixsub])
			flr_cat.cosines[*,k] = data[ixsub[ip]].detcosines
			endfor
		flr_cats[i]=flr_cat
		endfor
    ;Remove any duplicates - RAS, 26-mar-2010
    if nflare gt 1 then begin
      select = uniq(flr_cats.utstart)
      flr_cats = flr_cats[select]
      nflare = n_elements(flr_cats)
      endif
    ;Remove any duplicates - RAS, 26-mar-2010		
	return, flr_cats
	endif
	endif
	return, 0
end

