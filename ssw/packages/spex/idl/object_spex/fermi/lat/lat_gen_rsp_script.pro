;+
; Name: lat_gen_rsp_script
; 
; Purpose: Generate script to create LAT response (RSP) files. This is usually called by the daily cron job
;  that runs lat_make_files, but can also be called directly for specific time intervals and/or parameters.
; 
; Calling sequence:
;   script_name = lat_gen_rsp_script(...)
;   
;  Input Keywords:
;    NOTE: Either tevent or (rsp_ts and rsp_te) must be set.
;    tevent - event time in anytim format. If set, and rsp_ts and rsp_te are not set, then rsp_ts
;      defaults to tevent minus 1 hour, and rsp_te defaults to tevent plus 20 hours.
;      If not set, then tevent defaults to rsp_ts
;      Directory to write into is taken from yymmdd of tevent  
;    rsp_ts, rsp_te - start, end time of interval to make RSP files for.  If not set, they are generated
;      from tevent as mentioned above.  If use_gti=0 and use_exp=0, one RSP file is made for this entire interval.  If
;      use_gti=1, then find Good Time Intervals (GTIs) within those times to make RSP files.  If use_exp is 1 (default),
;      then find times of good exposure within those times to make RSP files.
;    use_gti - if set (default=0) use Good Time Intervals GTIs within requested times (rsp_ts, rsp_te). Otherwise
;      just make summed RSP file for entire rsp_ts, rsp_te interval.
;    use_exp - if set (default) use exposures from LC file to determine intervals within requested times (rsp_ts, rsp_te). Otherwise
;      just make summed RSP file for entire rsp_ts, rsp_te interval.
;    outdir - output directory to write scripts and files in.  If not set and archive isn't set, use yymmdd_hhmm/rsp in current dir, 
;      If archive is set, then write in /data/fermi/lat/yymmdd_hhmm/rsp. yymmdd_hhmm is time of event in tevent. 
;      If env var LAT_PARENT_DIR is defined, then replace /data/fermi/lat with value of LAT_PARENT_DIR   
;    archive - if set, use directory on /data/fermi/lat (which is available via anon. ftp from outside) or env var LAT_PARENT_DIR if defined
;    suffix - string to append to directory and file names
;    The following keywords are parameters for the Fermi LAT routines and are documented at 
;      http://fermi.gsfc.nasa.gov/ssc/data/analysis/scitools/overview.html
;      evclass
;      rad
;      zmax
;      filter
;      roicut
;      emin,emax
;      dtime
;      specin
;      respalg
;      thetacut
;      dcostheta
;    ebin_file - Name of energy binning FITS file to use
;    solar_class - if set, we're using 'extended' EV and SC files that we requested through data server, and evclass 65536, etc for solar-specific data
;    solar_files - if solar_class is set, must provide two file names - the event file and sc file requested from server - in that order.
;    
;  Output keywords:
;    status - 0/1 means 
;    
;  Example: To make an rsp for a specific interval do this:
;  lat_spec_rsp_scripts,/archive, emin=100., rad=10., /cleanup, /do_rsp, rsp_ts='25-feb-2014 01:17:30',rsp_te='25-feb-2014 01:30:30', use_gti=0, use_exp=0
;  lat_spec_rsp_scripts,/archive, rsp_ts='25-feb-2014 01:17:30', rsp_te='25-feb-2014 01:30:30', /do_rsp, /cleanup, use_gti=0, use_exp=0, emin=100., rad=10.
;    
;  Written, Kim Tolbert, November 2012
;  22-Jan-2014, Kim.Changed default filter from '"DATA_QUAL==1 && LAT_CONFIG==1 && ABS(ROCK_ANGLE)<52"' to 
;    "DATA_QUAL==1 && LAT_CONFIG==1" at suggestion of Melissa and Eric. Don't want that restriction after started
;      galactic center (~dec 2013?), but Gerry and Eric think we never needed it anyway, so it's OK like this for all data. 
;    Also changed instrument response function from irfs = evclass eq 2 ? 'P7SOURCE_V6' : 'P7TRANSIENT_V6' to 
;      irfs = evclass eq 2 ? 'P7REP_SOURCE_V15' : 'P7REP_TRANSIENT_V15'. Should have done this when the online data was 
;      switched to P7REP (Nov 6, 2013), so I will reprocess all of the files since Nov 6. (Older data used the irfs corresponding
;      to the P7 data when the files were made, so they're OK, and if I redo any old data, I automatically get the P7REP data, 
;      so using the new irfs value is correct.)
;  04-Mar-2014, Kim. Added use_exp option.  The GTIs no longer seem to be valid (starting at least some time in Jan 2014) - they
;      include a lot of time that has zero exposure.  With use_exp option, read LC file to get exposures, and find exposures
;      > 200, group together if there's a gap less than a minute, and skip intervals < 120 sec. use_exp=1 and use_gti=0 is now default.
;  30-Jan-2015, Kim. Added ebin_file as a keyword input
;  30-Jun-2015, Kim. Changed irfs(from 'P7REP_SOURCE_V15' to 'P8R2_SOURCE_V6'), evclass (from 2 to 128) and added
;      evtype (new, set to 3) for Pass 8 data
;  08-Sep-2015, Kim. Added check for LAT_PARENT_DIR.  If set (and /archive set) use that as parent directory for archive data.
;  27-Oct-2015, Kim. Added solar_class keyword.  If solar_class set, set evclass=65536, irfs='P8R2_TRANSIENT015S_V6', and
;    use extended file instead of photon file.  Also changed emin and rad defaults to 100.,10. (from 30., 20.) since
;    lat_make_files was calling this with those settings.
;  13-Nov-2015. Kim. For solar_class, use filter='"(DATA_QUAL>0||DATA_QUAL==-1)&&(LAT_CONFIG==1)"'
;  23-Nov-2015, Kim. For solar_class, need to also pass in solar_files which should contain two file names - the EV and SC files generated
;    for the day from the data server via the web form http://fermi.gsfc.nasa.gov/cgi-bin/ssc/LAT/LATDataQuery.cgi.  Only get weekly files if not
;    doing solar_class.  For solar_class, use ra, dec from middle of day for all exposures (for non-solar_class, use ra,dec at middle of exposure).
;    Since some of these command require a single spacecraft file name (can't use sc_file.txt which could contain multiple files), for solar_class, 
;    just use solar_files[1]. 
;  30-Nov-2015, Kim. For solar_class, if ra,dec aren't *exactly* what they were in the file created by request from data server, then there may be 
;    some rounding and may end up with two positions in file (look at DS keywords in extension 1), and then gtrspgen files with message
;    Not supported: multiple regions specified in spectrum in lat_pha1_....fits. So now we format ra,dec with f8.3 here and in data request
;-

function lat_gen_rsp_script, tevent=tevent, rsp_ts=rsp_ts, rsp_te=rsp_te, use_gti=use_gti, use_exp=use_exp, $
  outdir=outdir, archive=archive, suffix=suffix, $
  evclass=evclass, evtype=evtype, rad=rad, zmax=zmax, filter=filter, roicut=roicut, emin=emin, emax=emax, $
  dtime=dtime, specin=specin, respalg=respalg, thetacut=thetacut, dcostheta=dcostheta, $
  status=status, remove_old=remove_old, $
  ebin_file=ebin_file, solar_class=solar_class, solar_files=solar_files, _extra=_extra

status = 0
checkvar, archive, 0

if ~ (keyword_set(tevent) or (keyword_set(rsp_ts) and keyword_set(rsp_te))) then begin
  message,'You must specify an event time, or a start and end time.',/cont
  return, -1
endif

if keyword_set(tevent) then begin
  tevent = anytim(tevent) 
  checkvar, rsp_ts, tevent - 3600.d0
  checkvar, rsp_te, tevent + 24.*3600.d0
  rsp_ts = anytim(rsp_ts)
  rsp_te = anytim(rsp_te)
endif else begin
  rsp_ts = anytim(rsp_ts)
  rsp_te = anytim(rsp_te)
  checkvar, tevent, rsp_ts
  tevent = anytim(tevent)
endelse

checkvar, use_gti, 0
checkvar, use_exp, 1
checkvar, solar_class, 0

parent = chklog('LAT_PARENT_DIR')
if ~is_string(parent) then parent = '/data/fermi/lat'

checkvar, outdir, archive ? ssw_time2paths(tevent,tevent,parent=parent,/daily) : time2file(tevent,/date_only)
outdir = outdir + '/rsp'

if ~file_test(outdir, /dir) then file_mkdir,outdir else begin
  if keyword_set(remove_old) then begin
    frsp = file_search(outdir,'*')
    file_delete, frsp
  endif
endelse

checkvar, ebin_file, '/home/softw/lat/energy_bins.fits'

file_copy, ebin_file, outdir,/overwrite
ebin_file_basename = file_basename(ebin_file)

;checkvar, evclass, 2
checkvar, evtype, 3
checkvar, rad, 10.
checkvar, zmax, 100
checkvar, roicut, "yes"
checkvar, emin, 100.
checkvar, emax, 10000
checkvar, dtime, 60
checkvar, specin, -2.
checkvar, respalg, 'PS'
checkvar, thetacut, 75
checkvar, dcostheta, .05

checkvar, suffix, ''

;irfs = evclass eq 2 ? 'P7REP_SOURCE_V15' : 'P7REP_TRANSIENT_V15'

if solar_class then begin
  checkvar, evclass, 65536
  irfs = 'P8R2_TRANSIENT015S_V6'
  extended = 1
  checkvar, filter, '"(DATA_QUAL>0||DATA_QUAL==-1)&&(LAT_CONFIG==1)"'
  file_copy, solar_files, outdir, /overwrite
  prstr, solar_files[0], file=concat_dir(outdir,'ph_files.txt')
  prstr, solar_files[1], file=concat_dir(outdir,'sc_files.txt')
endif else begin
  checkvar, evclass, 128
  irfs = 'P8R2_SOURCE_V6'
  extended = 0
  checkvar, filter, '"DATA_QUAL==1 && LAT_CONFIG==1"'
  ; get spacecraft and photon weekly files for period needed and write filenames in ph_files.txt and sc_files.txt
  lat_get_weekly_files, rsp_ts, rsp_te, outdir=outdir, spacecraft=1, status=status_weekly, extended=extended
  if ~status_weekly then return, -1
endelse

sevclass = trim(evclass)
sevtype = trim(evtype)
srad = trim(rad)
szmax = trim(zmax)
semin = trim(emin)
semax = trim(emax)
sdtime = trim(dtime)
sspecin = trim(specin, '(f6.2)')
sthetacut = trim(thetacut)
sdcostheta = trim(dcostheta)


run_filename = concat_dir(outdir,'run_lat_rsp')


;filter = '"(DATA_QUAL==1) && (LAT_CONFIG==1) && (ABS(ROCK_ANGLE)<52 || (angsep('+sra+','+sdec+', RA_ZENITH, DEC_ZENITH)+'+scone+')<'+szmax+')"'
;roicut = "no"

case 1 of 
  use_gti: begin
    pha_binned_file = file_search(outdir+'/..','lat_spectrum*.fits', count=npha)
    if npha eq 0 then begin
      message,'No spectrum file found for ' + outdir, /cont
      return, -1
    endif
    gti=mrdfits(pha_binned_file[0], 3, h, /dscale)
    stimes = fermi_met2tim(gti.start)
    etimes = fermi_met2tim(gti.stop)
    end
  use_exp: begin
    exp_struct = lat_get_exposures([rsp_ts, rsp_te])
    if ~is_struct(exp_struct) then return, -1
;  lc_file = file_search(outdir+'/..','lat_LC*.fits', count=nlc)
;  if nlc eq 0 then begin
;    message,'No LC file found for ' + outdir, /cont
;    return, -1
;  endif
;  a = mrdfits(lc_file[0], 1, /dscale)
;  ts = fermi_met2tim(a.time)
;  te = ts + a.timedel
    expos = exp_struct.expos
    q = where (expos gt 200., count)
    if count eq 0 then begin
      message, 'No time periods with exposure > 200. Not making RSP files.', /cont
      return, -1
    endif
    ind = find_contig(q,dum,ss)  ; ss contains start/end indices into q of each contiguous group
    count = n_elements(ind)
    stimes = exp_struct[q[ss[*,0]]].times[0]
    etimes = exp_struct[q[ss[*,1]]].times[1]
    end
  else: begin
    count = 1
    stimes = rsp_ts
    etimes = rsp_te
    end
endcase

if use_gti or use_exp then begin
  ind = where (etimes gt rsp_ts and stimes lt rsp_te, count)
  if count eq 0 then begin
    message,'None of Good Time Intervals or good exposure times is within the Times we want to make RSPs for.',/cont
    print,'  MINMAX: ', anytim(minmax([stimes,etimes]), /vms)
    print,'  Desired RSP time interval: ', anytim(rsp_ts,/vms), ' to ', anytim(rsp_te, /vms)
    return, -1
  endif

  ; merge intervals that have gap of one minute or less
  q = where (stimes[1:*] - etimes le 60., nq)
  if nq gt 0 then begin
    etimes[q] = etimes[q+1]
    remove, q+1, stimes
    remove, q+1, etimes
    ; now see if any intervals left with gap < 1 minute
    q = where (stimes[1:*] - etimes le 60., nq)
    if nq gt 0 then begin
      etimes[q] = etimes[q+1]
      remove, q+1, stimes
      remove, q+1, etimes
    endif
  endif
  count = n_elements(stimes)
  if use_gti then begin
    stimes = stimes + 60.
    etimes = etimes - 60.
  endif
  
  ; if we're writing RSPs for archive, just want to do the ones that start on the day we're doing (we include a bit of the second day
  ; in the rsp_te time, so that we'll get any rsp intervals that span the day crossing, but we don't want to do the whole second day.
  if archive then begin
    end_of_day = anytim(rsp_ts,/date_only) + 86400.
    q = where(stimes lt end_of_day, count)
    stimes = stimes[q]
    etimes = etimes[q]
  endif
end

; for solar class, ra, dec must match what's in the requested data file from the data server 
if solar_class then begin
  radec = solephut(rsp_ts+43200.)
  sra = trim(radec[0], '(f8.3)')
  sdec = trim(radec[1], '(f8.3)')
endif

for j=0, count-1 do begin
  ts = stimes[j]
  te = etimes[j]

  if use_exp and te-ts lt 120. then goto, next_time
  ts_met = trim(fermi_tim2met(ts), '(f18.4)')
  te_met = trim(fermi_tim2met(te), '(f18.4)')
  tmid_met = trim(fermi_tim2met(average([ts,te])), '(f18.4)')
  str_times = time2file(ts,/sec)+'_'+strmid(time2file(te,/sec),9,6)
  
  ; if not solar class, then get ra  and dec at the middle of this time interval
  if ~solar_class then begin
    radec = solephut( (ts+te) / 2.)
    sra = trim(radec[0])
    sdec = trim(radec[1])
  endif
    
  evfile1 = 'lat_events1_' + str_times + suffix + '.fits'
  evfile2 = 'lat_events2_' + str_times + suffix + '.fits'
  phafile = 'lat_pha1_'+ str_times + suffix + '.fits'
  rspfile = 'lat_' + strlowcase(respalg) + '_'+ str_times + suffix + '.rsp'

  ;gtrspgen can't handle @sc_files.txt input, so have to actualy set file name into scfile argument
  if solar_class then begin
    scfile = solar_files[1]
  endif else begin
    ; Have to specify a single spacecraft file (since can't handle the @sc_files.txt file input, which could contain multiple
    ; files), so supply a single weekly file or the merged s/c file.
    ; If start and end weeks are same, use the weekly spacecraft file.  If not, can't concatenate weekly spacecraft files,
    ; so use merged spacecraft file as long as date is before date in file name.
    wks = lat_tim2wk(ts)
    wke = lat_tim2wk(te)
    if wks ne wke then begin
      scfile = file_search('/home/softw/lat/lat_spacecraft_merged_*.fits', count=count)
      if count gt 1 then scfile = get_recent_file(scfile)
      time_scfile = file2time(scfile)
      if te gt anytim(time_scfile) then begin
        message,'Time period crosses week boundary, and is later than merged file.  Update merged file.', /cont
        goto, next_time
      endif
    endif else scfile = file_basename(file_search(outdir,'lat_spacecraft_weekly_w'+trim(wks,'(i3.3)')+'*.fits'))
  endelse
  
  cmd_select = $
    ['gtselect ' + $
     'infile=@ph_files.txt ' + $
     'outfile='+evfile1+' ' + $
     'ra='+sra+ ' ' + $
     'dec='+sdec+ ' ' + $
     'rad='+srad+ ' ' + $
     'tmin='+ts_met+' ' + $
     'tmax='+te_met+' ' + $
     'emin='+semin+' ' + $
     'emax='+semax+' ' + $
     'zmax='+szmax+' ' + $
     'evclass='+sevclass+' ' + $
     'evtype='+sevtype]
     
  cmd_mktime = $
    ['gtmktime ' + $
     'scfile='+scfile+' ' + $
     'sctable=SC_DATA ' + $
     'filter='+filter+ ' ' + $
     'roicut='+roicut+ ' ' + $
     'evfile='+evfile1+' ' + $
     'evtable=EVENTS ' + $
     'outfile='+evfile2]
     
  cmd_bin = $
    ['gtbin ' + $
      'algorithm=PHA1 ' + $
      'evfile='+evfile2+' ' + $
      'outfile='+phafile+' ' + $
      'scfile='+scfile+' ' + $
      'ebinalg=FILE ' + $
      'ebinfile='+ebin_file_basename]

  cmd_rspgen = $
    ['gtrspgen ' + $
      'respalg='+respalg+' ' + $
      'specfile='+phafile+' ' + $
      'scfile='+scfile+' ' + $
      'outfile='+rspfile+' ' + $
      'irfs='+irfs+' ' + $
      'ebinalg=FILE ' + $
      'ebinfile='+ebin_file_basename+' ' + $
      'thetacut='+sthetacut+' ' + $
      'dcostheta='+sdcostheta]
      
  run_filename = concat_dir(outdir,'run_selbinrsp_'+str_times) + suffix
  
  ; call punlearn before every commmand so all previous values of parameters are cleared.
  wrt_ascii, ['punlearn gtselect', cmd_select, $
              'punlearn gtmktime', cmd_mktime, $
              'punlearn gtbin', cmd_bin, $
              'punlearn gtrspgen',cmd_rspgen], run_filename
  file_chmod, run_filename, /u_execute
  scripts = append_arr(scripts, run_filename)
next_time:
endfor

status = 1
return, scripts

end