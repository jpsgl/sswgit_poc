;+
; Name: lat_gen_spectrum_script
; 
; Purpose: Generate script to create LAT spectrum file and Lightcurve file with exposure info for a specified time interval.
;   Copies the necessary files from the FERMI archive and returns the name of the script.  This is usually run from lat_spec_rsp_scripts
;   which calls this and the routine to generate scripts to create RSP files, and then runs the scripts.
; 
; Calling sequence:
;   script_name = lat_gen_spectrum_script(...)
;   
;  Input Keywords:
;    NOTE: Either tevent or (tstart and tend) must be set.
;    tevent - event time in anytim format. If set, and tstart and tend are not set, then tstart
;      defaults to tevent minus 2 days, and tend defaults to tevent plus 2 days.
;      If not set, then tevent defaults to the midpoint between tstart and tend.
;      Directory to write into is taken from yymmdd of tevent  
;    tstart,tend - start, end time of interval to make spectrum file for.  If not set, they are generated
;      from tevent as mentioned above.    
;    outdir - output directory to write scripts and files in.  If not set and archive isn't set, use yymmdd_hhmm/rsp in current dir,
;      If archive is set, then write in /data/fermi/lat/yymmdd_hhmm/rsp. yymmdd_hhmm is time of event in tevent.
;      If env var LAT_PARENT_DIR is defined, then replace /data/fermi/lat with value of LAT_PARENT_DIR
;    archive - if set, use directory on /data/fermi/lat (which is available via anon. ftp from outside) or env var LAT_PARENT_DIR if defined
;    suffix - string to append to directory and file names
;    The following keywords are parameters for the Fermi LAT routines and are documented at 
;      http://fermi.gsfc.nasa.gov/ssc/data/analysis/scitools/overview.html
;      evclass
;      rad
;      zmax
;      filter
;      roicut
;      emin,emax
;      dtime
;      specin
;    ebin_file - Name of energy binning FITS file to use
;    solar_class - if set, we're using 'extended' EV and SC files that we requested through data server, and evclass 65536, etc for solar-specific data
;    solar_files - if solar_class is set, must provide two file names - the event file and sc file requested from server - in that order.
;    
;  Output keywords:
;    status - 0/1 means failed / succeeded to generate script
;    
;  Written, Kim Tolbert, November 2012
;  22-jan-2014, Kim. Changed default filter from '"DATA_QUAL==1 && LAT_CONFIG==1 && ABS(ROCK_ANGLE)<52"' to 
;    "DATA_QUAL==1 && LAT_CONFIG==1" at suggestion of Melissa and Eric. Don't want that restriction after started
;      galactic center (~dec 2013?), but Gerry and Eric think we never needed it anyway, so it's OK like this for all data.
;    Also changed instrument response function from irfs = evclass eq 2 ? 'P7SOURCE_V6' : 'P7TRANSIENT_V6' to 
;      irfs = evclass eq 2 ? 'P7REP_SOURCE_V15' : 'P7REP_TRANSIENT_V15'. Should have done this when the online data was 
;      switched to P7REP (Nov 6, 2013), so I will reprocess all of the files since Nov 6. (Older data used the irfs corresponding
;      to the P7 data when the files were made, so they're OK, and if I redo any old data, I automatically get the P7REP data, 
;      so using the new irfs value is correct.)
;  30-Jan-2015, Kim. Added ebin_file as a keyword input
;  30-Jun-2015, Kim. Changed irfs(from 'P7REP_SOURCE_V15' to 'P8R2_SOURCE_V6'), evclass (from 2 to 128) and added 
;      evtype (new, set to 3) for Pass 8 data
;  08-Sep-2015, Kim. Added check for LAT_PARENT_DIR.  If set (and /archive set) use that as parent directory for archive data.
;  27-Oct-2015, Kim. Added solar_class keyword.  If solar_class set, set evclass=65536, irfs='P8R2_TRANSIENT015S_V6', and
;    use extended file instead of photon file.  Also changed emin and rad defaults to 100.,10. (from 30., 20.) since 
;    lat_make_files was calling this with those settings.
;  13-Nov-2015. Kim. For solar_class, use filter='"(DATA_QUAL>0||DATA_QUAL==-1)&&(LAT_CONFIG==1)"'
;  23-Nov-2015, Kim. For solar_class, need to also pass in solar_files which should contain two file names - the EV and SC files generated
;    for the day from the data server via the web form http://fermi.gsfc.nasa.gov/cgi-bin/ssc/LAT/LATDataQuery.cgi.  Only get weekly files if not
;    doing solar_class
;  30-Nov-2015, Kim. For solar_class, if ra,dec aren't *exactly* what they were in the file created by request from data server, then there may be
;    some rounding and may end up with two positions in file (look at DS keywords in extension 1), and then gtrspgen files with message
;    Not supported: multiple regions specified in spectrum in lat_pha1_....fits. So now we format ra,dec with f8.3 here and in data request
;      
;    
;-

function lat_gen_spectrum_script, tevent=tevent, nbefore=nbefore, nafter=nafter, $
  tstart=ts, tend=te, outdir=outdir, archive=archive, $
  evclass=evclass, evtype=evtype, rad=rad, zmax=zmax, filter=filter, roicut=roicut, emin=emin, emax=emax, $
  dtime=dtime, specin=specin, suffix=suffix, status=status, $
  ebin_file=ebin_file,  solar_class=solar_class, solar_files=solar_files, _extra=_extra

status =0
archive = keyword_set(archive)

if ~ (keyword_set(tevent) or (keyword_set(ts) and keyword_set(te))) then begin
  message,'You must specify an event time, or a start and end time.',/cont
  return, -1
endif

if keyword_set(tevent) then begin
  tevent = anytim(tevent)
  checkvar, nbefore, 2
  checkvar, nafter, nbefore
  checkvar, ts, anytim(tevent - nbefore*86400.d0, /date)
  checkvar, te, anytim(tevent + nafter*86400.d0, /date) + 86399.d0
  ts = anytim(ts)
  te = anytim(te)
endif else begin
  ts = anytim(ts)
  te = anytim(te)
  checkvar, tevent, (ts+te)/2.
  tevent = anytim(tevent)
endelse

checkvar, solar_class, 0

parent = chklog('LAT_PARENT_DIR')
if ~is_string(parent) then parent = '/data/fermi/lat'

checkvar, outdir, archive ? ssw_time2paths(tevent,tevent,parent=parent,/daily) : time2file(tevent,/date_only)

checkvar, ebin_file, '/home/softw/lat/energy_bins.fits'

if ~file_test(outdir, /dir) then file_mkdir,outdir 

file_copy, ebin_file, outdir, /overwrite
ebin_file_basename = file_basename(ebin_file)

;checkvar, evclass, 2
checkvar, evtype, 3
checkvar, rad, 10.
checkvar, zmax, 100
checkvar, roicut, "yes"
checkvar, emin, 100.
checkvar, emax, 10000
checkvar, dtime, 60
checkvar, specin, -2.

checkvar, suffix, ''

;irfs = evclass eq 2 ? 'P7REP_SOURCE_V15' : 'P7REP_TRANSIENT_V15'
 
if solar_class then begin
  checkvar, evclass, 65536
  irfs = 'P8R2_TRANSIENT015S_V6'
  extended = 1
  checkvar, filter, '"(DATA_QUAL>0||DATA_QUAL==-1)&&(LAT_CONFIG==1)"'
  file_copy, solar_files, outdir, /overwrite
  prstr, solar_files[0], file=concat_dir(outdir,'ph_files.txt')
  prstr, solar_files[1], file=concat_dir(outdir,'sc_files.txt')
endif else begin
  checkvar, evclass, 128
  irfs = 'P8R2_SOURCE_V6'
  extended = 0
  checkvar, filter, '"DATA_QUAL==1 && LAT_CONFIG==1"'
  ; get spacecraft and photon weekly files for period needed and write filenames in ph_files.txt and sc_files.txt
  lat_get_weekly_files, ts, te, outdir=outdir, spacecraft=1, status=status_weekly, extended=extended
  if ~status_weekly then return, -1
endelse

sevclass = trim(evclass)
sevtype = trim(evtype)
srad = trim(rad)
szmax = trim(zmax)
semin = trim(emin)
semax = trim(emax)
sdtime = trim(dtime)
sspecin = trim(specin, '(f6.2)')

run_filename = concat_dir(outdir,'run_lat_spectrum_')

radec = solephut(tevent)
sra = trim(radec[0], '(f8.3)')
sdec = trim(radec[1], '(f8.3)')
ts_met = trim(fermi_tim2met(ts), '(f18.4)')
te_met = trim(fermi_tim2met(te), '(f18.4)')

times = archive ? time2file(ts,/date) : time2file(ts,/date)+'_'+time2file(te,/date)
evfile1 = 'lat_events1_' + times + suffix + '.fits'
evfile2 = 'lat_events2_' + times + suffix + '.fits'
specfile = 'lat_spectrum_' + times + suffix + '.fits'
lcfile = 'lat_LC_' + times + suffix + '.fits'


scfile='@sc_files.txt'
; Use merged spacecraft file for any dates before time in file name. 4-Nov-2012 Don't use this! It takes
; 1.25 HOURS to write a day's spectrum and LC files, instead of less than a minute using the weekly sc file.
;scfile = '/home/softw/lat/lat_spacecraft_merged_20121116.fits'
  
;filter = '"(DATA_QUAL==1) && (LAT_CONFIG==1) && (ABS(ROCK_ANGLE)<52 || (angsep('+sra+','+sdec+', RA_ZENITH, DEC_ZENITH)+'+srad+')<'+szmax+')"'
;roicut = "no"

cmd_select = $
    ['gtselect ' + $
     'infile=@ph_files.txt ' + $
     'outfile='+evfile1 +' ' + $
     'ra='+sra+ ' ' + $
     'dec='+sdec+ ' ' + $
     'rad='+srad+ ' ' + $
     'tmin='+ts_met+' ' + $
     'tmax='+te_met+' ' + $
     'emin='+semin+' ' + $
     'emax='+semax+' ' + $
     'zmax='+szmax+' ' + $
     'evclass='+sevclass+' ' + $
     'evtype='+sevtype]
     
cmd_mktime = $
    ['gtmktime ' + $
     'scfile='+scfile+' ' + $
     'sctable=SC_DATA ' + $
     'filter='+filter+' ' + $
     'roicut='+roicut+' ' + $
     'evfile='+evfile1+' ' + $
     'evtable=EVENTS ' + $
     'outfile='+evfile2]
     
cmd_bin = $
     ['gtbin ' + $
      'algorithm=PHA2 ' + $
      'evfile='+evfile2+' ' + $
      'outfile='+specfile+' ' + $
      'scfile='+scfile+' ' + $
      'tbinalg=LIN ' + $
      'tstart='+ts_met+' ' + $
      'tstop='+te_met+' ' + $
      'dtime='+sdtime+' ' + $
      'ebinalg=FILE ' + $
      'ebinfile='+ebin_file_basename]
      
cmd_lc = $
      ['gtbin ' + $
       'algorithm=LC ' + $
       'evfile='+evfile2+' ' + $
       'outfile='+lcfile+' ' + $
       'scfile='+scfile+' ' + $
       'tbinalg=LIN ' + $
       'tstart='+ts_met+' ' + $
       'tstop='+te_met+' ' + $
       'dtime='+sdtime]
       
cmd_exp = $
      ['gtexposure ' + $
       'infile='+lcfile+' ' + $
       'scfile='+scfile+' ' + $
       'irfs='+irfs+' ' + $
       'srcmdl=none ' + $
       'specin='+sspecin+' ' + $
       'emin='+semin+' ' + $
       'emax='+semax+' ' + $
       'enumbins=30']
    
      
run_filename = run_filename + times + suffix

; call punlearn before every commmand so all previous values of parameters are cleared.
wrt_ascii, ['punlearn gtselect', cmd_select, $
            'punlearn gtmktime', cmd_mktime, $
            'punlearn gtbin', cmd_bin, $
            'punlearn gtbin', cmd_lc, $
            'punlearn gtexposure', cmd_exp], run_filename
file_chmod, run_filename, /u_execute

print,'Wrote script ' + run_filename + ' to generate spectrum and LC (exposure) files for
print,' File time: ' + anytim(ts,/vms,/truncate) + ' to ' + anytim(te,/vms,/truncate)
print,' Event time ' + anytim(tevent,/vms)

status = 1
return, run_filename   

end