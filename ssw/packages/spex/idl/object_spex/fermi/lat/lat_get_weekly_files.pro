; Find week numbers corresponding to start/end times, and get all photon and spacecraft weekly files needed, and put the
; names of the files in ph_files.txt and sc_files.txt in outdir.
; 
; Kim Tolbert, 1-Nov-2012
; Modifications:
;  2-Jul-2014, Kim. Made pattern searches more specific.  In one case, with more general search just for
;  week number in file name, got two files for one week: 
;  lat_1sec_spacecraft_weekly_w316_p203_v001.fits  and lat_spacecraft_weekly_w316_p203_v001.fits and we were
;  getting both, so s/w that uses them crashed.  

pro lat_get_weekly_files, tstart, tend, outdir=outdir, photon=photon, spacecraft=spacecraft, status=status

checkvar,outdir, curdir()
checkvar, photon, 1
checkvar, spacecraft, 1
status = 0

wks = lat_tim2wk(tstart)
wke = lat_tim2wk(tend)

if wke lt wks or wks lt 0 then begin
  message,'Error in times. Start time must be < end time, and both must be > 29-may-2008.',/cont
  print, 'Start time = ',anytim(tstart,/vms), '  End time = ', anytim(tend,/vms)
  return
endif

wk = wks
site = 'heasarc.gsfc.nasa.gov'
while wk le wke do begin
  if photon then begin
    pattern = 'lat_photon_weekly_w'+trim(wk,'(i3.3)')+'_*.fits'
    ph=sock_find(site, pattern, path='FTP/fermi/data/lat/weekly/photon', count=count, /use_network)
    if count gt 0 then sock_copy,ph,out_dir=outdir
  endif
  if spacecraft then begin
    pattern = 'lat_spacecraft_weekly_w'+trim(wk,'(i3.3)')+'_*.fits'
    sp=sock_find(site, pattern, path='FTP/fermi/data/lat/weekly/spacecraft', count=count,/use_network)
    if count gt 0 then sock_copy,sp,out_dir=outdir
  endif
  wk = wk+1
endwhile

status = 1

if photon then begin
  ph = file_search(outdir + '/*photon*.fits', count=nph)
  if nph gt 0 then begin
    prstr, file_basename(ph), file=concat_dir(outdir,'ph_files.txt')
  endif else begin
    status = 0
    message, 'No weekly photon files for week ' + arr2str([wks,wke]), /cont
  endelse
endif

if spacecraft then begin
  sp = file_search(outdir +'/*spacecraft*.fits', count=nsc)
  if nsc gt 0 then begin
    prstr, file_basename(sp), file=concat_dir(outdir,'sc_files.txt')
  endif else begin
    status = 0
    message, 'No weekly spacecraft files for week ' + arr2str([wks,wke]), /cont
  endelse
endif

end