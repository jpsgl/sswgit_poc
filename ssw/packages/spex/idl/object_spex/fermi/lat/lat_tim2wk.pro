;+
; Name: lat_tim2wk
;
; Purpose: Convert a time to a Fermi LAT week number 
;
; Calling sequence:  wk = lat_tim2wk(time)
; 
; Calling argument:
;  time - time in anytim format
; 
; Example:
;  print, lat_tim2wk('7-Mar-2011')
;  144
;  
; Written: Kim Tolbert, Nov 2012
; 
;-

function lat_tim2wk, time

time = anytim(time)

tweek0 = anytim('29-may-2008') ; reference for week numbers, this is week 0

return, fix((time - tweek0) / 86400. / 7.)

end