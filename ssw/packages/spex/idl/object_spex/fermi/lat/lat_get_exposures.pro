;+
; Name: lat_get_exposures
; 
; Purpose: Retrieve LAT Exposure data, with option to return calculated Good Time Intervals (GTI)
; 
; Calling Sequence:
;   str = lat_get_exposures, date[, gtis=gtis]
;    
; Input Argument:
;   date - time interval to get exposure info for (in anytim format). 2-element range, or if scalar assumes 1 day.
;  
; Output Keywords:
;   gtis - good time intervals calculated by gtexposure and added in extension to LC file
; 
; Output:  Returns a structure array with the times, exposure info, and # counts from the LightCurve file.  Returns the
;   Good Time Intervals times in keyword gtis. If no LC files are find for requested times, returns -1.
;   Structure looks like this:
;   times: start/end of time bin in anytim format
;   expos: exposure for eaach time bin in cm^2 seconds
;   cts:   counts in time bin over all energies> 100 MeV
;
;Method: Reads LAT daily Lightcurve (LC) file in hesperia LAT solar archive.  If user is not on hesperia computer,
;  copies file to user's local directory.  If requested time spans a day boundary concatenates data from each
;  daily file.
;   
; Examples:
; str = lat_get_exposures('25-feb-2014', gtis=gtis)
;  
; Kim Tolbert, Jan-2014
; Modifications: 
; 2-Nov-2014, Kim. get_host() now returns gs671-hesperia.ndc.nasa.gov
; 3-Jul-2015, Kim. Added error handling. If  mrdfits doesn't return a structure for a or gti, skip that file.
; 11-Sep-2015, Kim. Also add check that a structure has exposure tag. (12-mar-2009 doesn't)
; 
;-

function lat_get_exposures, date, gtis=gtis

time_range = n_elements(date) eq 1 ? anytim(date, /date_only) + [0,86400.d0] : anytim(date)

spex_find_data,'fermi_lat', time_range, pattern='LC', url=url, count=count
if count eq 0 then begin
  message, /cont, 'No LAT LC files found for time requsted: ' + anytim(time_range[0],/vms) + ' to ' + anytim(time_range[1],/vms)
  return, -1
endif

if get_host() eq 'gs671-hesperia.ndc.nasa.gov' then begin
  for i = 0,n_elements(url)-1 do begin
    res = parse_url(url[i])
    lc_file = append_arr(lc_file, '/data/' + res.path)
  endfor
endif else begin
  spex_find_data,'fermi_lat', time_range, pattern='LC', url=url, /copy,file=lc_file
endelse

nfile = n_elements(lc_file)
if nfile gt 1 then lc_file = lc_file[sort(lc_file)]

for i=0,nfile-1 do begin
  
  ext_rate = get_fits_extno(lc_file[i], 'RATE')
  a=mrdfits(lc_file[i],ext_rate, /dscale)
  if ~is_struct(a) then continue
  if ~tag_exist(a, 'exposure') then continue
  this_ts = fermi_met2tim(a.time)
  if exist(te) then begin
    q = where(this_ts -last_item(te) ge -.0005, count)
    if count eq 0 then continue  ; skip this file if no times > last file
    a = a[q]
    this_ts = this_ts[q]
  endif
  ts = append_arr(ts, this_ts)
  te = append_arr(te, this_ts+a.timedel)
  expos = append_arr(expos, a.exposure)
  cts = append_arr(cts, a.counts)
  
  ext_gti = get_fits_extno(lc_file[i], 'GTI')
  gti = mrdfits(lc_file[i],ext_gti,/dscale)
  if ~is_struct(gti) then continue
  this_st = fermi_met2tim(gti.start)
  if exist(etimes) then begin
    q = where(this_st - last_item(etimes) ge -.0005, count)
    if count eq 0 then continue
    gti = gti[q]
    this_st = this_st[q]
  endif
  stimes = append_arr(stimes, this_st)
  etimes = append_arr(etimes, fermi_met2tim(gti.stop))  
  
endfor

if ~exist(stimes) then return, -1

t2d = transpose([[ts],[te]])  ; now [2,ntimes]
np = n_elements(expos)
data = replicate({times: [0.d,0.d], expos: 0., cts: 0.}, np)
data.times = t2d
data.expos = expos
data.cts = cts

gtis = transpose([[stimes],[etimes]])

return, data
end