;+ 
; Name: lat_make_files
; 
; Purpose:  Wrapper around routine to generate and run scripts to make LAT daily spectrum and lightcurve file, 
;  as well as RSP files for each solar exposure.  The files are on hesperia's LAT solar data archive at 
;  http://hesperia.gsfc.nasa.gov/fermi/lat/. This can only be run on hesperia because the scripts require the
;  FERMI Science Tools that are installed on hesperia.  It is usually run via a cron job, but can be run manually too.
; 
; Calling arguments:
;  time - time range to make files for.  If scalar, just do that one day.  If 2 elements, makes files for start
;         day through last day, i.e. time=['12-mar-2014','13-mar-2014'] will do BOTH days, not stop at beginning of 13th.
;         If not present, do 10 days starting 10 days ago.
;  
; Example:
;  lat_make_files
;  lat_make_files, time='12-mar-2014'   ; will do just 12-mar-2014
;  lat_make_files, time=['12-mar-2014', '13-mar-2014']   ; will do both days
;  lat_make_files, time=['20-dec-2012', '10-jan-2013']
;
; Written:  Kim Tolbert, Jan, 2013
; Modified:
;  25-Oct-2013 Change default dates to 10 days ago to end of yesterday (was 5 days starting 12 days ago, i.e.
;    stayed a week behind, but no need for that)
;
;-


pro lat_make_files, time=time_in, _extra=_extra

; set times either to input times (date only), or current date minus 11 days for 5 days
time = keyword_set(time_in) ? anytim(time_in, /date) : anytim(!stime, /date) + [-10.*86400., -1.]
t0 = time[0]
; if end time wasn't supplied, just do one day.
t1 = n_elements(time) eq 2 ? time[1] : t0 + 86399.

ts = t0 & te = ts+86399. 
while ts le t1 do begin &$
  print, 'Making LAT spectrum and RSP files for ' + anytim(/vms, [ts,te]) &$
  lat_spec_rsp_scripts,/archive, tstart=ts,tend=te, rsp_ts=ts, rsp_te=te+3600., emin=100.,  rad=10., /cleanup, _extra=_extra &$
  ts = ts + 86400. & te = ts + 86399. &$
endwhile

end
