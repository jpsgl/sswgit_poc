;+
; Name lat_nonsolar
; 
; Purpose: For significant events that we know are non-solar, mark the current LAT quicklook
; plot with a notation, and return the text to be added to the LAT significant event list.
; Currently there is just one such time - if we find more, I should make the times and text
; list a separate text file that this routine reads.
; 
; Input Keywords:
; times - times of the signficant LAT flux (midpoint of the time interval in sec since 79/1/1)
; mark_plot - if set, annotate the current quicklook plot with a non-solar text msg
; yhigh -  flux values at those times
; tr    -  time range of current plot in sec seince 79/1/1
; 
; Output Keywords:
; text - Text to show for the non-solar times
; 
; Written: 12-Feb-2014, Kim Tolbert
;-
pro lat_nonsolar, times=times_in, mark_plot=mark_plot, yhigh=yhigh, tr=tr, text=text

times = anytim(times_in)
count = n_elements(times)

nonsolar_time = ['7-nov-2011 18:00', '8-nov-2011']
nonsolar_text = 'Radio Quasar PKS 1510-089, Not Solar'

for i=0,count-1 do begin
  if in_range(times[i], anytim(nonsolar_time)) then begin
    text_out = append_arr(text_out, nonsolar_text)
    if mark_plot then begin
      if i gt 0 && (times[i] - times[i-1]) lt 6.*3600. then continue
      left = (times[i] - tr[0]) / (tr[1]-tr[0]) lt .5
      align = left ? 0. : 1.
      xval = times[i] - tr[0] - 3.*3600.
      yval = yhigh[i]
      xyouts, xval, yval, nonsolar_text, align=align, charsize=.7
    endif
  endif else text_out = append_arr(text_out, '')
endfor

text = text_out
end
