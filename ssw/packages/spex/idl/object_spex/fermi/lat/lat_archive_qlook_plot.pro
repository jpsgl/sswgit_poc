;+
; Name: lat_archive_qlook_plot 
; 
; Purpose: Make 4-day LAT light-bucket or maximum-likelihoodqlook plots that are displayed in RHESSI Browser.
; 
; Method: For time requested, figure out the 4-day interval from the start of LAT data that time is in, so
;  boundaries of 4-day plots are always the same.
;  
; Input Keywords:
;  time - date in anytim format, either scalar or a time range. Makes all of the 4-day plots containing the
;    date or range requested.
;  maxlike - if set, do maximum-likelihood plots. Default is to do light-bucket.
;  cleanup - if set, delete maximum-likelihood text files copied to hesperia when done
; 
; Written: Kim Tolbert Jan 2013
; 
; Modified:
;  25-Oct-2013 Change default dates to 10 days ago to end of yesterday (was 5 days starting 12 days ago)
;  29-Dec-2013 Add option to plot max-likelihood data from N. Omodei
;  21-Feb-2014 Changed errfrac for light-bucket plots to .8 (from .4). When started Galactic Center mode,
;    sometimes no data points because less solar exposure.  Redid all plots in archive with errfrac=.8
;-


pro lat_archive_qlook_plot, time=time_in, maxlike=maxlike, cleanup=cleanup

pl_start = anytim('12-aug-2008')  ; time of first 4-day plot

; set times either to input times (date only), or current date minus 11 days through end of yesterday
time = keyword_set(time_in) ? anytim(time_in, /date) : anytim(!stime, /date) + [-10.*86400., -1.]
t0 = time[0]
; if end time wasn't supplied, just do one 4-day plot.
t1 = n_elements(time) eq 2 ? time[1] : t0 + 86399.
print,'Making Lat archive qlook plots for ' + anytim(t0,/vms) + ' to ' + anytim(t1,/vms)
nd = 4.  ; # days in each plot

; Find 4-day interval from pl_start that t0 is in
tdiff = (t0 - pl_start) mod (nd*86400.d0)
t0 = t0 - tdiff

ts = t0
while ts le t1 do begin
  te = ts + nd*86400.
  if keyword_set(maxlike) then lat_maxlike_plot, [ts,te-1.], /browser, cleanup=cleanup else $
    lat_qlook_plot,[ts, te-1.], /browser, exp_cutoff=20000., errfrac=.8, /write
  ts = te
endwhile


end
