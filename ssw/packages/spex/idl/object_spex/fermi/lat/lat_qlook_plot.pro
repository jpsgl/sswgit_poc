;+
; Name: lat_qlook_plot
;
; Purpose: Generate Fermi LAT light-bucket quicklook plots, one point per solar exposure interval.
;
; Method: Use the daily LAT spectrum FITS file and daily LAT lightcurve FITS file (for exposure info) that
;  we create on hesperia (this routine will download the needed files if you're not running on hesperia).
;  Read the rate data from spectrum file using ospex and sum over energy bins.
;  Use the LC file to find the time intervals of solar exposure (expos > exp1min_cutoff.)
;  For each interval, sum the exposures and the total rate and divide to get total flux
;  Use only the points where total exposure is > exp_cutoff
;  Of those points, use those where fractional error is < errfrac_cutoff and total flux > 1.e-8
;  Plot the total flux vs time
;  Points that are >3 sigma above mean are plotted in red, and added to a 'Significant Event' text file.
;
; Calling Arguments:
;  time_range - time interval of plot (in anytim format). 2-element range, or if scalar assumes 1 day.
;
; Input Keywords:
;  exp_cutoff - total exposure for a solar exposure must be greater than this to use (default=20000.)
;  errfrac_cutoff - fractional error in point must be less than this to use (default=.8)
;  use_plotman - if set, send plot to plotman interactive window (default=0)
;  png - if set, draw plot in png file (default=0), filename is fermi_lat_yyyymmdd.png in curr dir
;  filename_append - text to add to png file name
;  browser - if set, then plots are for Browser.  Must be running as softw on hesperia to use this.  Sets png=1 and
;    directory to write png in to data/fermi/lat/qlook/ on hesperia
;  no_timestamp - if set, don't write time stamp at bottom of plot
;  no_legend - if set, don't write legends
;  no_level_plot - if set, don't plot mean levels
;  testdir - if browser was set, then directory to write to instead of normal 4day_plots/yyyy/mm or daily/yyyy/mm
;  add_label - any additional label to add to top right label (will add another line)
;  write_txt - if set, will add any >3sigma points to text file 'lat_events.txt'. Only for use with browser option.
;  debug - if set, make intermediate plots
;
; Output keywords:
;  pobj - if use_plotman was set, the plotman object reference is returned in pobj
;  data_str - structure containing values that were plotted.  See end of routine for description of fields.
;
; Example:
;   lat_qlook_plot, '25-feb-2014'
;   lat_qlook_plot, ['25-feb-2014','1-mar-2014']
;   lat_qlook_plot, ['25-feb-2014','1-mar-2014'], errfrac=1., exp_cutoff=0.  ; see data without filters for exp and err
;   lat_qlook_plot, ['25-feb-2014','1-mar-2014'], /png   ; make PNG file
;
; Written: Kim Tolbert ? 2012
;
; 19-dec-2012 - only use intervals where expos > exp1min_cutoff instead of expos > 0.
; 21-Feb-2014, Kim. Take care of case with only one point to plot.  Remove hat from end points of widths in time.
; 26-Feb-2014, Kim. Add a little to upper y range for fluxes and effective areas to make room for legends. Fixed
;  two errors with plotting the eff. areas - wasn't applying the new indices after filtering for exp, errfrac or time,
;  so the array plotted was the original array (including the previous day) with all values - since this array
;  would be bigger than the filtered array of times, it would just go ahead and use the first n points. Totally wrong.
;  Also, for the eff. area plot called utplot,/nodata,ystyle=4... and axis, ystyle=1... so in 1st case, using nice
;  limits, in 2nd case using exact, so axis values were shown in wrong place. Changed 1st to ystyle=5.
;  Also changed units for showing exp cutoff in legend to m^2 min (from cm^2 min)
;  Also removed test for errfrac eq .4 to write sig event text file.  If write_txt is set, do it.
; 19-Mar-2014, Kim.  Use lat_get_exposures instead of lat_read_exposures.  Changed default for errfrac_cutoff to .8 (from .4)
; 25-Jul-2014, Kim. Added data_str keyword to return values that were plotted and if multiple plots are selected by !p.multi
;   this will now handle correctly (doesn't move to next plot until overplotting is done)
; 07-Oct-2014, Kim.  Want to include points that have very high flux that were excluding previously because failed exposure
;   or error test.  Now check if any of original flux values is great 3sigma from mean calculated using the filtered values, and
;   if so, add those points back into the array of points to use.  To do this, made a lot of changes to store data as
;   array of structures, instead of individual data arrays, so indexing can be done more easily.
; 16-Sep-2015, Kim. Added exp1min_cutoff and restore_reject_sigma to try different values for Pass 8 data. Previously they
;   were hard-coded to 500 and 3. Also, if # point in LC and spectrum file different, continue - reduce # if ut_spec has
;   fewer points than times. May need to correct other case too (ut_spec more points than times).  This happened because of
;   the gap ~ 12-mar-2009 (lc file had more points than spectrum).
; 27-Sep-2015, Kim. Added check for LAT_PARENT_DIR env var..  If set, use that as parent directory for archive data.
; 28-Sep-2015, Kim. Changed defaults for exp_cutoff, exp1min_cutoff, and restore_reject_sigma
; 28-Oct-2015, Kim. Call str2cols with /unaligned.  Not sure why it worked without this!
; 5-Nov-2015, Kim. Added debug option.  When set, makes intermediate plots
; 02-Mar-2016, Kim. Modified plot legend and text file to change '3 sigma' to '4.2 sigma' (which is 3 times 'effective' sigma -
;   since our sigma combines the statistical and systematic uncertainties (1-sigma value and rms added in quadrature)
; 6-Apr-2017, Kim. Make plots look nice for PS (if set outside of this routine), and added no_timestamp, no_legend, no_level_plot and data_str keywords

;
;-


pro lat_qlook_plot, time_range, $
  exp_cutoff=exp_cutoff, errfrac_cutoff=errfrac_cutoff, $
  exp1min_cutoff=exp1min_cutoff, restore_reject_sigma=restore_reject_sigma, $
  use_plotman=use_plotman, pobj=pobj, png=png, filename_append=filename_append, browser=browser, $
  no_timestamp=no_timestamp, no_legend=no_legend, no_level_plot=no_level_plot, $
  cleanup=cleanup, testdir=testdir, add_label=add_label, write_txt=write_txt, data_str=data_str, $
  debug=debug

  checkvar, debug, 0
  checkvar, write_txt, 0
  ;checkvar, emin, 30.
  browser = keyword_set(browser)
  if ~browser then write_txt = 0
  if browser then png = 1
  png = keyword_set(png)
  if png then use_plotman = 0

  if !d.name eq 'PS' then begin
    png = 0
    browser = 0
    use_plotman = 0
    yes_ps = 1
  endif else yes_ps = 0

  checkvar, exp_cutoff, 26000.
  checkvar, errfrac_cutoff, .8
  checkvar, exp1min_cutoff, 650.
  checkvar, restore_reject_sigma, 4.

  tr = anytim(time_range)
  ; if single time supplied in time_range, then set end time to end of day
  if n_elements(tr) eq 1 then tr = [tr, tr+86399.]
  tr_ext = [tr[0] -86400., tr[1]+86400.]  ; extend by one day either side, so first and last exposures have all data

  ; get exposure data from lightcurve file
  exp_struct = LAT_GET_EXPOSURES(tr_ext)
  if ~is_struct(exp_struct) then return

  times = exp_struct.times
  twidth = times[1,0] - times[0,0]
  ; expos tags is exposure in cm^2 sec.  Our bins are 1-minute, so exp_struct.expos is 60 times effective area.
  expos = exp_struct.expos / twidth  ; now expos is in cm^2

  ; get rate data from spectrum file.  First get files in time range
  tmp_obj = obj_new('spex_fermi_lat')
  url = tmp_obj-> search( tr_ext, /pha, count=count)
  if count eq 0 then begin
    message,'No spectrum files for ' + anytim(tr[0],/vms) + ' to ' + anytim(tr[1],/vms), /cont
    return
  endif

  ; If we're on hesperia, don't need to copy files, just set copyfiles to the full path of the data files
  if get_host() eq 'gs671-hesperia.ndc.nasa.gov' then begin
    for i=0,count-1 do begin
      res = parse_url(url[i])
      copyfiles = append_arr(copyfiles, '/data/' + res.path)
    endfor
  endif else begin
    tmp_obj->download, url, copyfile=copyfiles
  endelse
  destroy, tmp_obj

  setenv,'OSPEX_NOINTERACTIVE=1'
  o = ospex(/no)
  o->set_file, copyfiles, dialog=0
  d = o->getdata(spex_units='rate')
  ut_spec = o->get(/spex_ut_edges)

  ; get radius and emin used to make LAT spectrum file from header in first extension, usually in
  ; DSVAL3 and DSVAL4, but number might change, so find correct DSTYPx first.
  a = mrdfits(copyfiles[0], 1, head)
  qpos = where(fxpar(head, 'DSTYP*') eq 'POS(RA,DEC)')
  rad = '?'
  if qpos ne -1 then begin
    a = str2arr(fxpar(head, 'DSVAL'+trim(qpos+1)), ',')
    if n_elements(a) gt 1 then rad = float(a[2])
  endif
  qpos = where(stregex(fxpar(head, 'DSTYP*'), 'ENERGY', /bool))
  emin = '?'
  if qpos ne -1 then begin
    a = str2arr(fxpar(head, 'DSVAL'+trim(qpos+1)), ':')
    if n_elements(a) gt 1 then emin = float(a[0])
  endif

  destroy,o
  setenv,'OSPEX_NOINTERACTIVE=0'

  if n_elements(times) ne n_elements(ut_spec) then begin
    message, 'Number of points from LC file different from spectrum file: ' + $
      trim(n_elements(times)/2.) + ' ' + trim(n_elements(ut_spec)/2.), /cont
    if n_elements(times) gt n_elements(ut_spec) then begin
      z=where(ut_spec[0,*] le times[0,*] and ut_spec[1,*] ge times[0,*])
      ut_spec = ut_spec[*,z]
      d = {data: d.data[*,z], edata: d.edata[*,z], ltime: d.ltime[*,z]}
    endif
  endif


  totrate = total(d.data,1)  ; sum over energies to get total rate in each time interval

  ; Find start / end of groups of data where exposure is > exp1min_cutoff.
  q = where (expos gt exp1min_cutoff)  ; cm^2
  ind = find_contig(q,dum,ss)  ; ss contains start/end indices into q of each group

  if debug then begin
    linecolors
    utplot, anytim(times[0,*],/ext),expos, title='exposure and cutoff, red are above cutoff'
    linecolors & outplot, anytim(times[0,q],/ext),expos[q], col=2, psym=2
    oplot,!x.crange, exp1min_cutoff+[0.,0.]
    stop
  endif

  ; acc will be array of structures holding time, exposure, rate, etc. for each point
  np = n_elements(ind)
  acc = replicate({ut: [0.d,0.d], texp: 0., trate: 0., ave_exp: 0., tflux: 0., error: 0.}, np)
  ;ut = dblarr(2,np)
  ;texp = fltarr(np)
  ;trate = fltarr(np)
  ;ave_exp = fltarr(np)

  ; For each group of non-zero exposures get start/end time, total exposure and total rate
  for i=0,np-1 do begin &$
    i1 = q[ss[i,0]] &$  ; for group i, i1, i2 are start / end indices
    i2 = q[ss[i,1]] &$
    acc[i].ut = [times[0,i1],times[1,i2]] &$
    acc[i].texp = total(expos[i1:i2]) &$  ; cm^2 sec
    acc[i].ave_exp = acc[i].texp / (i2-i1+1)  ; cm^2
  acc[i].trate = total(totrate[i1:i2]) &$    ; Should convert rate to counts before adding times, but close enough for now
  endfor
acc.tflux = f_div(acc.trate, acc.texp)
acc.error = f_div(acc.tflux, sqrt(acc.trate*twidth))

ts = time2file(tr[0],/date)
te = time2file(tr[1],/date)
ut_mean = get_edges(acc.ut, /mean)

; Keep points where flux is > 1.e-8, and within time requested
keep = where((acc.tflux gt 1.e-8) and (ut_mean ge tr[0] and ut_mean le tr[1]), count )
all_bad = 0
if count gt 0 then acc = acc[keep] else all_bad = 1
acc_orig = acc  ; keep a copy of these data points
;-------------------------

; Find points where exposure is > exp_cutoff and relative error is < threshold
q = where(acc.texp ge exp_cutoff and f_div(acc.error, acc.tflux) lt errfrac_cutoff, complement=reject, ncomplement=nreject, count)
if nreject gt 0 then acc_reject = acc[reject]
if count gt 0 then acc = acc[q] else all_bad = 1

if all_bad then begin

  acc.tflux = acc.tflux * 0.
  acc.error = acc.error * 0.
  acc.texp = acc.texp * 0.
  acc.ave_exp = acc.ave_exp * 0.
  print,'No data for ' + anytim(tr[0],/vms) + ' to ' + anytim(tr[1],/vms) + ' passed the threshold tests.  Setting to 0.'

endif else begin
  if debug then begin
    !p.multi=[0,1,3]
    utplot,anytim(acc_orig.ut[0,*],/ext), acc_orig.tflux, title='Flux', charsize=1.8
    outplot,anytim(acc.ut[0,*],/ext), acc.tflux, col=7, psym=2
    qbadexp = where(acc_orig.texp lt exp_cutoff, kexp)
    if kexp gt 0 then begin
      t1 = acc_orig[qbadexp].ut[0,*]
      y1 = acc_orig[qbadexp].tflux
      if kexp eq 1 then begin
        t1 = t1[0] + [0.,0.]
        y1 = y1[0] + [0.,0.]
      endif
      outplot, anytim(t1,/ext), y1, col=2, psym=4, symsize=4, thick=2
    endif
    qbaderr = where(f_div(acc_orig.error, acc_orig.tflux) ge errfrac_cutoff, kerr)
    if kerr gt 0 then begin
      t2 = acc_orig[qbaderr].ut[0,*]
      y2 = acc_orig[qbaderr].tflux
      if kerr eq 1 then begin
        t2 = t2[0] + [0.,0.]
        y2 = y2[0] + [0.,0.]
      endif
      outplot, anytim(t2,/ext), y2, col=4, psym=6, symsize=4, thick=2
    endif
    ssw_legend, ['original flux', 'accepted flux', 'rejected for low exposure', 'rejected for high error'], $
      linest=0, psym=[-3,2,4,6], col=[255,7,2,4], textcol=[255,7,2,4], box=0, charsize=1.4

    utplot, anytim(acc_orig.ut[0,*],/ext), acc_orig.texp, title='total exposure and cutoff', charsize=1.8
    oplot,!x.crange, exp_cutoff+[0.,0.], col=7

    utplot, anytim(acc_orig.ut[0,*],/ext), f_div(acc_orig.error, acc_orig.tflux), title='Fractional error and cutoff', charsize=1.8
    oplot,!x.crange, errfrac_cutoff+[0.,0.], col=7

    stop
    !p.multi=0

  endif
endelse

; Use resistant_mean routine to find outliers, non-outlier point indices to use are returned in wused.
; Then use those points to find weighted mean and weighted rms (spread of points).
; Weighted mean is total(flux/error) / total(1./error)
; Weighted rms is sqrt( total( (flux-mean)^2/error^2) / total(1/error^2) )
; Combine weighted rms with error in each point in quadrature to get 1-sigma value for each point from mean
; Flag values that are more than 3 sigmas from mean
if n_elements(acc) gt 1 then resistant_mean, acc.tflux, 3., mean, sigma, num_rej, wused=wused else wused=0
mean = total( f_div(acc[wused].tflux, acc[wused].error) ) / total( f_div(1., acc[wused].error) )
rms = sqrt( total( f_div( (acc[wused].tflux-mean)^2, acc[wused].error^2) ) / total( f_div( 1., acc[wused].error^2)) )
print, 'mean = ', mean, '   rms = ', rms

; Check if any of original points is > restore_reject_sigma*sigma using mean and rms calculated without these additional points. If so, add them
; back into array.
if nreject gt 0 then begin
  z = where (acc_reject.tflux-mean gt restore_reject_sigma*sqrt(acc_reject.error^2 + rms^2), count)
  if count gt 0 then begin
    print, trim(count) + ' rejected point(s) were added back to array because failed tests, but > 3 sigma:'
    for ii = 0,count-1 do print, format_intervals(acc_reject[z[ii]].ut, /ut, /end_date)  +  ' Flux = ' + trim(acc_reject[z[ii]].tflux)
    print,' '
    acc = [acc, acc_reject[z]]
    s = sort(acc.ut[0])
    acc = acc[s]
  endif
endif

sigma = sqrt(acc.error^2 + rms^2)  ; this is our 'effective' sigma, combines statistical and systematic errors
sigma3 = 3.* sigma   ; so this is really ~4.2 * statistical sigma (if error and rms were equal, we'd be multiplying by sqrt(2) = 1.4, *3 = 4.2)

bw = 0
bkcolor = 255

if yes_ps then begin
  thick0 = 0
  thicka = 2
  thicker = 4
  thickest = 10
  xthick = 2
  ythick = 2
  hatlength = 150
endif else begin
  thick0 = 0
  thicka = 2
  thicker = 3
  thickest = 4
  xthick = 1
  ythick = 1
  hatlength = 6
endelse

IF png THEN BEGIN

  daily = ts eq te  ; start/end days same means doing a daily plot
  file = 'fermi_lat_' + ts
  if ~daily then file = file + '_' + te  ; if not daily, include end time in file name
  if keyword_set(filename_append) then file = file +  '_' + filename_append
  file = file + '.png'
  default, outfile, file

  year = strmid(ts, 0, 4)
  month = strmid(ts, 4, 2)

  IF browser then begin
    checkvar, testdir,'light_bucket/'
    dd = daily ? 'daily_plots/' : testdir
    date = year + '/' + month
    parent = chklog('LAT_PARENT_DIR')
    if ~is_string(parent) then parent = '/data/fermi/lat'
    dir = parent+'/qlook/' + dd + date + '/'
    if ~file_test(dir, /dir) then file_mkdir, dir
  endif else dir = curdir()

  save_dev = !d.name
  set_plot, 'z'
  device, set_resolution = [640,300]

endif

ut_mean = get_edges(acc.ut, /mean)
ut_width = get_edges(acc.ut, /width)

if keyword_set(use_plotman) then begin
  utobj=obj_new('utplot', anytim(/vms,ut_mean), acc.tflux, acc.error)
  utobj->set,data_unit='Rate', /ylog, id=trim(exp_cutoff) + ' ' + trim(errfrac_cutoff)
  pobj=plotman(input=utobj, psym=4)
  destroy,utobj
endif else begin

  linecolors
  multiplot = total(!p.multi) gt 0.
  save_multi = !p.multi
  ; if ratio of min to max is gt 10, then plot on log scale.
  yminmax = minmax(acc.tflux)
  ylog = yminmax[1]/yminmax[0] gt 10

  ; can't autoscale y because it doesn't include error bars extent, so calculate yrange with error bars, and make
  ; sure the 3sigma level shows
  yr = minmax([acc.tflux+acc.error, acc.tflux-acc.error])
  yr[1] = yr[1] > max(mean+sigma3)
  yr[1] = ylog ? yr[1]*10. : yr[1] + (yr[1]-yr[0])/10.  ; expand upper limit so doesn't interfere with legends

  utplot, anytim(/vms,ut_mean), acc.tflux, ylog=ylog,  background=bkcolor, color=0, /nodata, psym=4, yrange=yr,  $
    ytitle='Flux > ' + trim(emin,'(i4)') + ' MeV  [!4c!3 cm!u-2!n s!u-1!n]' , $
    title='LAT Light-Bucket Flux', charsize=.9, $
    xrange=anytim(/vms,tr), /xstyle, xmargin=[12,8], xthick=xthick, ythick=ythick, ystyle=8

  ;  col_save = !p.color
  ;  thick_save = !p.thick
  ;  !p.color = 10
  ;  !p.thick=2

  ;  datplot,ut[0,*], ut[1,*], tflux, STAIRS=0, color=10, /outplot
  ;  outplot, anytim(/vms,ut_mean), tflux, color=10, psym=4
  ; If only one point to plot, repeat value to make array.
  ; Separate plotting the x and y errors. Don't want hat on x errors.
  if n_elements(acc.tflux) gt 1 then begin
    eutplot, anytim(/vms,ut_mean), acc.tflux, acc.error, /unc, psym=4,  /noconnect, errcolor=10, thick=thicker, hatlength=hatlength
    eutplot, anytim(/vms,ut_mean), acc.tflux, acc.error, /unc, psym=4,  /noconnect, errcolor=10, thick=thicker, xerr=ut_width/2., /nohat
    if ~keyword_set(no_level_plot) then outplot,anytim(/vms,ut_mean), mean+sigma3, col=9
  endif else begin
    eutplot, anytim(/vms,[ut_mean,ut_mean]), [acc.tflux,acc.tflux], acc[0].error, /unc, psym=4, /noconnect, errcolor=10, thick=thicker, hatlength=hatlength
    eutplot, anytim(/vms,[ut_mean,ut_mean]), [acc.tflux,acc.tflux], acc[0].error, /unc, psym=4, /noconnect, errcolor=10, xerr=ut_width/2., thick=thicker, /nohat
    if ~keyword_set(no_level_plot) then outplot,anytim(/vms,[ut_mean,ut_mean]), mean+sigma3+[0.,0.], col=9
  endelse

  if ~keyword_set(no_level_plot) then begin
    oplot, !x.crange, [mean,mean], color=0, thick=thick0
    oplot, !x.crange, [mean+rms,mean+rms], col=7, thick=thick0
    oplot, !x.crange,[ mean-rms,mean-rms], col=7, thick=thick0
  endif



  ; for points that are more than 3 sigma above mean, plot them in red and add the times to a text file (if errfrac_cutoff=.4)
  q = where (acc.tflux-mean gt sigma3, count)
  sig_points = q
  if count gt 0 then begin
    !p.color = 2
    if count eq 1 then q = [q,q]  ; for some reason eutplot doesn't allow single element arrays
    outplot,anytim(/vms,ut_mean[q]), acc[q].tflux, color=2, psym=4, thick=thicker
    eutplot, anytim(/vms,ut_mean[q]), acc[q].tflux, acc[q].error, /unc, psym=4, /noconnect,color=2, thick=thicker, hatlength=hatlength
    eutplot, anytim(/vms,ut_mean[q]), acc[q].tflux, acc[q].error, /unc, psym=4, /noconnect,color=2, xerr=ut_width[q]/2., thick=thicker, /nohat
    lat_nonsolar, times=ut_mean[q], /mark_plot, yhigh=acc[q].tflux, tr=tr, text=nonsolar_text

    ;    if errfrac_cutoff eq .4 then begin
    nsig = (acc[q].tflux - mean) / sigma[q]
    times = format_intervals(acc[q].ut, /ut, /end_date) + ' '+trim(acc[q].tflux,'(e9.2)') + ' '+trim(nsig,'(f8.2)')
    qns = where(nonsolar_text ne '', kns)
    if kns gt 0 then times[qns] = times[qns] + ' ; ' + nonsolar_text[qns]
    prstr, times, /nomore
    ; if writing text file, append new to what's already in list, sort it by time and sigma and write two lists in output file
    if write_txt then begin
      parent = chklog('LAT_PARENT_DIR')
      if ~is_string(parent) then parent = '/data/fermi/lat'
      textfile = parent + '/qlook/lat_events.txt'
      list = rd_ascii(textfile)
      ; there are 2 versions of list, sorted differently - get first set by taking all elements between the first two blank lines.
      q = where (strcompress(list, /remove_all) eq '')
      ; if file didn't exist, list will contain one blank line, so q will equal 0
      if n_elements(q) eq 1 then list = times else begin
        list = list[q[0]+1:q[1]-1]
        ; first remove flare that are in the time range we just processed - ifnew time are slightly different
        ; would keep both versions.  qkeep will be indices of those outside current time range.  Append new list to that.
        tt = anytim(strmid(list,0,24))
        qkeep = where(tt lt tr[0] or tt ge tr[1],keep)
        if keep gt 0 then list = list[qkeep]
        list = [list, times]
      endelse
      tt = anytim(strmid(list,0,24))
      z = get_uniq(tt, sorder)  ; get unique first time in each line, sorder returns sort order of unique elements
      list = list[sorder]
      z = str2cols(list, ' ', /unaligned)
      sig = float(z[6,*])
      q=reverse(sort(sig))
      list2 = list[q]
      out = ['Fermi LAT Significant Event List', $
        'Time periods with LAT flux > 4.2 sigma (3*effective sigma) above mean, sorted by time:', $
        'Columns 0-4: Start and end date and time', $
        'Column    5: Flux >100 MeV (gamma cm^-2 s^-1)', $
        'Column    6: Number of (effective) sigmas above mean', $
        '', $
        list, $
        '', $
        'Same list, sorted by sigma: ', $
        '', $
        list2 ]
      wrt_ascii, out, textfile ; output file is sorted by first time in each line
      print, 'Rewrote LAT signficant event file ' + textfile
    endif
    ;    endif
  endif

  ;  texp_ms = texp / 1.e4
  ;  utplot, anytim(/vms,ut_mean), texp_ms, color=0, psym=4, ystyle=4, /noerase, xrange=anytim(/vms,tr), /xstyle, xmargin=[12,8], charsize=.9,/nodata
  ;  axis, yaxis=1, ytitle='Exposure (m!u2!n s)', color=12, ystyle=1, yrange=minmax(texp_ms), charsize=.9
  ;  datplot,ut[0,*], ut[1,*], texp_ms, STAIRS=0, color=12, /outplot, thick=4

  acc.ave_exp = acc.ave_exp / 1.e4  ; convert from cm^2 to m^2
  ;  print, 'ut_width = ', ut_width
  ;  print, 'exp*tim =  ', ave_exp * ut_width
  ;  print, 'ave_exp =  ', ave_exp

  ; want to plot exposures on same plot as above, so don't advance plot number
  if multiplot then begin
    new_multi = !p.multi
    !p.multi = save_multi
  endif
  yr = minmax(acc.ave_exp)
  yr[1] = yr[1] + (yr[1]-yr[0])/5.  ; expand upper limit so doesn't interfere with legends
  utplot, anytim(/vms,ut_mean), acc.ave_exp, color=0, psym=4, ystyle=5, /noerase, xrange=anytim(/vms,tr), /xstyle, xmargin=[12,8], $
    charsize=.9, xthick=xthick, ythick=ythick, /nodata, yrange=yr
  axis, yaxis=1, ytitle='Effective Area (m!u2!n)', color=12, ystyle=1, yrange=yr, xthick=xthick, ythick=ythick, charsize=.9
  datplot,acc.ut[0], acc.ut[1], acc.ave_exp, STAIRS=0, color=12, /outplot, thick=thickest

  if ~keyword_set(no_legend) then begin
    ec = exp_cutoff / 1.e4 ; convert from cm^2 min to m^2 min
    text = ['Exposure threshold = '+trim(ec,'(f3.1)')+' m!u2!n min', $
      'Error fraction cutoff = ' + trim(errfrac_cutoff), $
      'ROI Radius = '+trim(rad)]
    if keyword_set(add_label) then text = [text, add_label]
    ssw_legend, text, box=0, charsize=.7, textcolor=0
    ssw_legend, ['> 4.2 sigma above mean', '4.2-sigma level', 'Weighted mean', 'Weighted RMS'], $
      color=[2,9,0,7], lines=0, box=0, charsize=.7, textcolor=0, /right
  endif
  if ~keyword_set(no_timestamp) then timestamp, charsize=.7, /bottom, color=0
;  !p.color = col_save
;  !p.thick = thick_save

  ; Now advance one, since last plot used /noerase, it won't advance
  if multiplot then !p.multi = new_multi


endelse

IF keyword_set(png) THEN BEGIN
  tvlct, r, g, b, /get
  filename = concat_dir(dir, outfile)
  write_png, filename, tvrd(), r,g,b
  print, 'Wrote plot file ' + filename
  set_plot, save_dev
ENDIF

if arg_present(data_str) then $
  data_str = { $
  ut: acc.ut, $  ; start/end times of bins [2,n]
  tflux: acc.tflux, $  ; flux in each time bin [n]
  error: acc.error, $  ; error for each tflux value [n]
  mean: mean, $    ; mean value of tflux
  rms: rms, $      ; weighted rms of tflux
  sigma3: sigma3, $  ; 3-sigma level of tflux
  ave_exp: acc.ave_exp, $  ; effective area [n] in m^2
  sig_points: sig_points}  ; indices of points with flux > sigma3, -1 if none

end

