;+
; Name: lat_maxlike_plot
; 
; Purpose: Generate quicklook plots for Fermi LAT from Nicola Omodei's Maximum-likelihood analysis. He
;   provides daily text files at http://www.slac.stanford.edu/~omodei/SunMonitor/_CSV_ORB/ of data points.
;   He makes plots from these data himself (http://www.slac.stanford.edu/~omodei/SunMonitor/_PNG_ORB/) but
;   we make them again so that they will be tailored to work in RHESSI Browser.  We group the data into
;   4-day plots on the same boundaries as for the light-bucket LAT plots (based on Gerry Share's method), 
;   produced by lat_qlook_plot.  Both lat_qlook_plot and lat_maxlike_plot are run from lat_archive_qlook_plot
;   with keywords to select times and options.  The plots are archived on hesperia.
; 
; Method: Copy and read Nicola's text files for days selected.  For points with Test Statistic > 20, plot 
; points in red. Otherwise plot blue upper limits (draw arrows pointing down)
; 
; Calling Arguments:
;  time_range - time interval of plot (in anytim format)
; 
; Input Keywords:
;  png - if set, draw plot in png file (default=0), filename is lat_maxlike_yyyymmdd_yyyymmdd.png in curr dir
;  browser - if set, then plots are for Browser.  Must be running as softw on hesperia to use this.  Sets png=1 and 
;    directory to write png in to data/fermi/lat/qlook/max_likelihood on hesperia
;  cleanup - if set, delete Nicola's text files when done.
;  no_timestamp - if set, don't write time stamp at bottom of plot
;  
; Output Keywords:
;  data_str - return data structure of data read from text files
; 
; Example:
;   lat_maxlike_plot, ['25-feb-2014','1-mar-2014']
;   lat_maxlike_plot, ['25-feb-2014','1-mar-2014'], /png, /cleanup
;
;Written: Kim Tolbert Jan-2014
; 2-Nov-2014, Kim. get_host() now returns gs671-hesperia.ndc.nasa.gov
; 6-Apr-2017, Kim. Make plots look nice for PS (if set outside of this routine), and added no_timstamp and data_str keywords
; 17-Jul-2017, Kim. Some files really contain data for the next day (starts on last point of day in file name), so read
;   days surrounding requested times, just to make sure we get everything, then truncate structure to selected times.
;
;-
pro lat_maxlike_plot, time_range, png=png, browser=browser, cleanup=cleanup, no_timestamp=no_timestamp, data_str=data_str

on_hesperia = get_host() eq 'gs671-hesperia.ndc.nasa.gov'
if ~on_hesperia then browser = 0

browser = keyword_set(browser)
png = keyword_set(png) or browser

if !d.name eq 'PS' then begin
  png = 0
  browser = 0
  yes_ps = 1
endif else yes_ps = 0

tr = anytim(time_range)
; if single time supplied in time_range, then set end time to end of day
if n_elements(tr) eq 1 then tr = [tr, tr+86399.]
  
ts = time2file(tr[0],/date)
te = time2file(tr[1],/date)
  
url = 'http://www.slac.stanford.edu/~omodei/SunMonitor/_CSV_ORB/'
; sample file name: SUN-ORB-REP_2012-11-01.csv
; 17-jul-2017, Kim - read the day before and day after as well as requested days
days = anytim(timegrid(tr[0]-86400.d0, tr[1]+86400.d0, /days, /string, /quiet), /ccsds)
days = strmid(days,0,10)
files_to_get = url + 'SUN-ORB-REP_' + days + '.csv'
if on_hesperia and browser then out_dir = '/home/softw/lat/maxlike_dailycsv' else out_dir = curdir()
sock_copy, files_to_get, local_file=files, out_dir=out_dir
if array_equal(files,'') then begin
  message,'No files found for dates ' + ts + ' to ' + te, /cont
  return
endif

nfiles = n_elements(files)

IF png THEN BEGIN
  dir = curdir()

  file = 'lat_maxlike_' + ts + '_' + te   
  default, outfile, file

  IF browser then begin
    year = strmid(ts, 0, 4)
    month = strmid(ts, 4, 2)
    dir = '/data/fermi/lat/qlook/max_likelihood/' + year +'/' + month + '/'
    if ~file_test(dir, /dir) then file_mkdir, dir
    outfile = file
  endif

  if keyword_set(filename_append) THEN  outfile = outfile + filename_append
  outfile = outfile + '.png'

  save_dev = !d.name
  set_plot, 'z'
  device, set_resolution = [640,300]
  
endif

;f=file_search('sun-orb*.csv')
;delvarx,data

for i=0,nfiles-1 do begin
  new_data = files[i] eq ''? 0 : lat_read_maxlike_file(file=files[i])
  if is_struct(new_data) then data = append_arr(data, new_data)
endfor

tr = [file2time(ts,out='sec'), file2time(te,out='sec')+86400.]
; 17-jul-2017, Kim. Since read extra days, truncate structure to just times requested (and non-zero flux)
q = where (data.flux gt 0. and data.times[0] ge tr[0] and data.times[1] le tr[1], count)
if count gt 0 then data = data[q] else begin
  message, 'No non-zero data in period ' + ts + ' to ' + te, /cont
  return
endelse

tmid = get_edges(data.times, /mean)
twid = get_edges(data.times, /width)/2.
np = n_elements(tmid)

if yes_ps then begin
  thicka = 2
  thicker = 4
  thickest = 10 
  xthick = 2
  ythick = 2
  hsize = 150
endif else begin
  thicka = 2
  thicker = 3
  thickest = 4
  xthick = 1
  ythick = 1
  hsize = 6
endelse

linecolors

multiplot = total(!p.multi) gt 0.
save_multi = !p.multi

;clear_utplot

flux_minus_err = data.flux - data.flux_err
ymin = min(flux_minus_err[where(flux_minus_err gt 0.)])
ymax = max(data.flux + data.flux_err)
yrange=[ymin<1.e-7, ymax]
; First plot just draws axes and labels, no data
utplot, anytim(minmax(data.times),/ext), data.flux, /nodata, timerange=tr, yrange=yrange, /ylog, $
  title='LAT Maximum-Likelihood Flux', ytitle='Flux (100 MeV - 10 GeV) [ph cm!u-2!n s!u-1!n]', $
  background=255, color=0, xmargin=[12,8], xstyle=1, ystyle=8, xthick=xthick, ythick=ythick, charsize=.9

; Overplot the fluxes in blue, showing the width in time, but not showing yerrors (set to 0. here)  
eutplot, anytim(tmid,/ext), data.flux, xerr=twid, yerr=fltarr(np),$
  psym=3, color=10, errcol=10, thick=thicker, hatlength=!D.X_VSIZE/300.
  
; For points with TS < 20 add blue arrow to show they are upper limits  
q = where(data.ts lt 20., complement=qred, ncomplement=nred, count)
z=convert_coord(tmid[q]-tr[0], data[q].flux, /data, /to_normal)
arrow, z[0,*], z[1,*], z[0,*], z[1,*]-.04, /norm, hsize=hsize, color=10, thick=thicker
  
;Any points that have a TS > 20., plot again in red  
if nred gt 0 then begin
  eutplot, tmid[qred]-tr[0], data[qred].flux,$
    xerr=twid[qred], yerr=data[qred].flux_err, $
    psym=6, symsize=.5, color=2, errcol=2, thick=thicker, hatlength=!D.X_VSIZE/300.
    
  ; if flux - flux_err is less then 0, then eutplot doesn't plot the vertical red bar, just 
  ; draws a little red blob at the upper limit.  So force it draw from bottom of plot to  flux+flux_err.  
  ylo = data[qred].flux - data[qred].flux_err
  q = where(ylo lt yrange[0], count)
  if count gt 0 then for i=0,count-1 do begin
    ind = qred[q[i]]
    outplot,anytim(tmid[ind] + [0.,0.],/ext), [data[ind].flux+data[ind].flux_err, (crange('y'))[0]],color=2, thick=thicker
  endfor
endif

; Overplot green dashes to show flux level of quiet sun  
oplot,!x.crange, [5.6e-7,5.6e-7], color=8, linestyle=2, thick=thicka

; Plot exposure data with new axis on right
q = where(data.exp gt 0., count)
if count gt 0 then begin  
  ; If doing multiplot, have to do this to get back to plot we started on, since next comment is
  ; a plot, /noerase which we want on top of previous plot, but normally would move to next panel.
  if multiplot then begin
    new_multi = !p.multi
    !p.multi = save_multi
  endif
  utplot, /noerase, /nodata, anytim(tmid[q],/ext), data[q].exp, timerange=tr, /ylog, $
    color=0, xmargin=[12,8], xstyle=1, ystyle=4, xthick=xthick, ythick=ythick, charsize=.9
    
  eutplot, anytim(tmid[q],/ext), data[q].exp, xerr=twid[q], yerr=fltarr(np), $
    psym=3, color=12, errcol=12, thick=thickest, /nohat
  
  axis, yaxis=1, ytitle='Exposure [m!u2!n s]',color=12, xthick=xthick, ythick=ythick, charsize=.9
  if multiplot then !p.multi = new_multi
endif
  
if ~keyword_set(no_timestamp) then timestamp,/bottom,color=0,charsize=.7

IF png THEN BEGIN
  tvlct, r, g, b, /get
  filename = concat_dir(dir, outfile)
  write_png, filename, tvrd(), r,g,b
  print, 'Wrote plot file ' + filename
  set_plot, save_dev
ENDIF

if keyword_set(cleanup) then begin
  q = where(files ne '', count)
  if count gt 0 then file_delete, files[q]
endif

;cleanplot, /silent
;clear_utplot
if arg_present(data_str) then data_str = data
end