;+
; Name: lat_spec_rsp_scripts
; 
; Purpose: Generate and run scripts to create LAT spectrum and response files
;  
; Calling Sequence:
;   lat_spec_rsp_scripts
;    
; Input Keywords:
;   outdir - Output Directory to write files in
;   do_spec - if set, do spectrum file scripts (if neither do_spec or do_rsp is set, both are done)
;   do_rsp - if set, do response file scripts (if neither do_spec or do_rsp is set, both are done)
;   cleanup - if set, delete intermediate files
;   spec_script - if passed in, run this script instead of calling lat_gen_spectrum_script to generate script
;   rsp_script - if passed, run this script instead of calling lat_gen_rsp_script to generate script
;   _extra - any of the keywords allowed in lat_gen_spectrum_script or lat_gen_rsp_script.  Look in the header
;     of those routines for all the keywords allowed.
;    
; Output Keywords:
;   none 
;   
; Examples:
;  lat_spec_rsp_scripts, 
;  lat_spec_rsp_scripts, outdir='testmergedsc', rsp_ts='24-feb-2011 18:00', rsp_te='24-feb-2011 21:00',/do_rsp
;  lat_spec_rsp_scripts, outdir=outdir[0], rsp_ts=ts[0], rsp_te=te[0],/do_rsp, use_gti=0
;  lat_spec_rsp_scripts, outdir='/data/fermi/lat/20110906_0135', rsp_ts='6-sep-2011 21:10', rsp_te='6-sep-2011 22:55',/do_rsp
;  
; Kim Tolbert, 6-Nov-2012
; 23-Jan-2014, Kim.  Run fermi-init.csh or fermi-init.sh depending on current shell (previously ran ...csh script, but
;   cron job runs in sh shell, don't know why this didn't matter before upgrade science tools to r32p5)
; 
;-


pro lat_spec_rsp_scripts, outdir=outdir, do_spec=do_spec, do_rsp=do_rsp, cleanup=cleanup, $ 
  spec_script=spec_script, rsp_script=rsp_script, _extra=_extra

do_spec = keyword_set(do_spec)
do_rsp = keyword_set(do_rsp)
do_both = (do_spec + do_rsp eq 0) 
cleanup = keyword_set(cleanup)

curdir = curdir()

; If running tcsh or csh shell, csh will be true.
csh = strpos(strlowcase(chklog('SHELL')), 'csh') ne -1
init_script = csh ? '$FERMI_DIR/fermi-init.csh' : '$FERMI_DIR/fermi-init.sh'
if do_spec or do_both then begin
  cd,'/home/softw/lat'
  
  if ~exist(spec_script) then spec_script = lat_gen_spectrum_script(outdir=outdir, _extra=_extra)
  
  cd,file_dirname(spec_script)
  script = file_basename(spec_script)
  cmd = 'source ' + init_script + '; ./' + script
  print,'Spawning this command to run script ' + cmd
  spawn, cmd, result1
  if cleanup then begin
;    fdel = file_search(['*weekly*.fits','*events1*.fits','*events2*.fits'], count=nf)
    fdel = file_search(['*weekly*.fits','*events1*.fits'], count=nf)
    if nf gt 0 then file_delete, fdel, /quiet
  endif
endif

if do_rsp or do_both then begin
  cd,'/home/softw/lat'
  if ~exist(rsp_script) then rsp_script = lat_gen_rsp_script(outdir=outdir, _extra=_extra, status=status)
  
  if status then begin
    cd,file_dirname(rsp_script[0])
    script = file_basename(rsp_script)
    for i=0,n_elements(rsp_script)-1 do begin
      cmd = 'source ' + init_script + '; ./' + script[i]
      print,'Spawning this command to run script ' + cmd
      spawn, cmd, result2
    endfor
    if cleanup then begin
      fdel = file_search(['*weekly*.fits','*events1*.fits','*events2*.fits','*pha1*.fits'], count=nf)
      if nf gt 0 then file_delete, fdel, /quiet
    endif
  endif
endif

cd,curdir

end