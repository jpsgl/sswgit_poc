;+
; Name: lat_plot_exposures
; 
; Purpose: Plot Lat Exposure data, with option to overplot calculated Good Time Intervals (GTI) and/or times
;   covered by LAT Response files
; 
; Calling Sequence:
;   lat_plot_exposures, date[, /show_gti, /show_rsp]
;    
; Input Argument:
;   date - time interval to plot exposure for (in anytim format). 2-element range, or if scalar assumes 1 day.
;   
; Input Keywords:
;   show_gti - If set, overplot the GTI intervals
;   show_rsp - If set, overplot the time intervals that the RSP files were made for
;  
; Output Keywords:
;   plotman_obj - returns plotman object reference 
;   
; Output: Plots the exposure information and GTI and RSP time info in a plotman instance.
;
;Method: Reads LAT daily Lightcurve (LC) file in hesperia LAT solar archive.  If user is not on hesperia computer,
;  copies file to user's local directory.  If requested time spans a day boundary concatenates data from each
;  daily file. GTI info also comes from LC file.  RSP times are found by parsing the file names of the RSP files
;  in the hesperia LAT RSP archive.
;  
; Examples:
; lat_plot_exposures, '25-feb-2014'
; lat_plot_exposures, ['25-feb-2014','27-feb-2014'], /show_gti, /show_rsp
;  
; Kim Tolbert, 6-Nov-2012
; Modifications: 
; Feb-2014, Kim.  Use date input instead of file name input. call lat_get_exposure to copy and read LC file containing
;   exposure data for date(s) requested.  Added show_rsp option.
;
;-

pro lat_plot_exposures, date, show_gti=show_gti, show_rsp=show_rsp, plotman_obj=pl

show_gti = keyword_set(show_gti)
show_rsp = keyword_set(show_rsp)

lc_data = lat_get_exposures(date, gtis=gtis)
if ~is_struct(lc_data) then return

np = n_elements(lc_data)
tplot = anytim(/vms,lc_data.times)
yplot = lc_data.expos
dim1_ids = 'Exposure'

tr = minmax(lc_data.times) 

if show_gti then begin  
  mk_contiguous, gtis, c_gtis, index, epsilon=1.e-12
  ngti = n_elements(c_gtis[0,*])
  y_gti = fltarr(ngti)
  y_gti[index] = 5000.
  t_gtis = [[tr[0],min(c_gtis)], [c_gtis], [max(c_gtis),tr[1]], [dblarr(2,np-ngti-2)+tr[1]]]
  y_gti = [0., y_gti, fltarr(np-ngti-1)]
  tplot = [[[tplot]], [[anytim(/vms,t_gtis)]]]
  yplot = [[yplot], [y_gti]]
  dim1_ids = [dim1_ids, 'Good Time Intervals']
endif

if show_rsp then begin
  trsp = lat_rsp_times(date)
  mk_contiguous, trsp, c_trsp, index, epsilon=1.e-12
  nrsp = n_elements(c_trsp[0,*])
  y_rsp = fltarr(nrsp)
  y_rsp[index] = 10000.
  t_rsp = [[tr[0],min(c_trsp)], [c_trsp], [max(c_trsp),tr[1]], [dblarr(2,np-nrsp-2)+tr[1]]]
  y_rsp = [0., y_rsp, fltarr(np-nrsp-1)]
  tplot = [[[tplot]], [[anytim(/vms,t_rsp)]]]
  yplot = [[yplot], [y_rsp]]
  dim1_ids = [dim1_ids, 'Response File Times']
endif  


utobj = obj_new('utplot', tplot, yplot, id='Fermi LAT Solar Exposures', dim1_ids=dim1_ids, data_unit='cm!u2!n s')

pl=plotman(input=utobj)

end