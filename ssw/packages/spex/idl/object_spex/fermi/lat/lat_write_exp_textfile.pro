;+
; Name: lat_write_exp_textfile
; 
; Purpose: Write a text file (and save file) on lat web site containing LAT solar exposure time intervals.
; 
; Written: 21-Oct-2015, Kim Tolbert
; Modifications:
; 29-Mar-2016.  Realized that looking at RSP times gives a lot of extra times, since sometimes users ask for extra
;  RSP files for an event.  So changed this to call lat_get_exposures instead of lat_rsp_times.  Also added duration and
;  effective area columns.
;-
pro lat_write_exp_textfile, tr=tr, outfile=outfile, savfile=savfile

;This was the old way:
      ;tr = ['12-aug-2008','20-oct-2015']
      ;z = lat_rsp_times(tr)
      ;za = anytim(z,/ccsds)
      ;zas = reform(za[0,*] + '  ' + za[1,*])
      ;out = ['Time intervals when LAT solar exposure is > 200 cm^2 s', $
      ;  'Created ' + !stime + ' by Kim Tolbert (kim.tolbert at nasa.gov)', $
      ;  ' ', $
      ;  zas]
      ;prstr, file='/data/fermi/lat/lat_solar_exposure_times.txt',out

; This is the new way (29-mar-2016)
; 
checkvar, tr, ['12-aug-2008','29-mar-2016']
checkvar, outfile, '/data/fermi/lat/lat_solar_exposure_times.txt'
checkvar, savfile, '/data/fermi/lat/lat_solar_exposure_times.sav'

exp_struct = lat_get_exposures(tr)
if ~is_struct(exp_struct) then return

times = exp_struct.times
expos = exp_struct.expos  ; in cm^2 s
q = where (expos gt 200., count)
if count eq 0 then begin
  message, 'No time periods with exposure > 200.', /cont
  return
endif

ind = find_contig(q,dum,ss)  ; ss contains start/end indices into q of each contiguous group
count = n_elements(ind)

twidth = times[1,0] - times[0,0]
expos = expos / twidth  ; now in cm^2

stimes = reform(times[0, q[ss[*,0]]])
etimes = reform(times[1, q[ss[*,1]]])
dur = round((etimes - stimes) / 60.) ; duration in minutes
tot_dur = round(total(dur))
tot_dur_hr = round(tot_dur / 60.)

ave_exp = fltarr(count)
for i=0,count-1 do ave_exp[i] = total(expos[q[ss[i,0]]:q[ss[i,1]]]) / (ss[i,1]-ss[i,0]+1) / 1.e4

;ave_exp = total(expos[q[ss[*,0]]:q[ss[*,1]]]) / (ss[*,1]-ss[*,0]+1) / 1.e4
zas = anytim(/ccsds,stimes) + '  ' + anytim(/ccsds,etimes) + '  ' + string(dur, '(i6)') + string(ave_exp,'(f7.2)')

out = ['Time intervals when LAT solar exposure is > 200 cm^2 s', $
  'Created ' + !stime + ' by Kim Tolbert (kim.tolbert at nasa.gov)', $
  ' ', $
  'Columns 1,2:  Start, End time of interval with LAT solar exposure', $
  'Column    3:  Duration in minutes', $
  'Column    4:  Effective Area (Average Exposure) in m^2', $
  ' ', $
  zas, $
  ' ', $
  'Total duration = ' + trim(tot_dur) + ' minutes (= ' + trim(tot_dur_hr) + ' hours)']

prstr, file=outfile, out

comment = 'Times with LAT solar exposure in seconds from 1979/1/1, dur in minutes, average exposure (effective area) in m^2.'
save, file=savfile, stimes,etimes,dur,ave_exp, comment

end