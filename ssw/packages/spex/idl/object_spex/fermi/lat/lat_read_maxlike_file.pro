;+
; Name: lat_read_maxlike_file
; 
; Purpose: Read N. Omodei's text file with the maximum-likelihood fluxes for each day and return a structure with the data
; 
; Method: This is called from lat_maxlike_plot to make the 4-day maximum-likelihood plots that are archived on hesperia
;  and displayed in the RHESSI Browser.  lat_maxlike_plot has already copied over the files needed for the time interval
;  requested from the SLAC server.
;  
; Input keywords:
;  file - Name of file to read
;  
; Output:
;  The function returns an array of structures with these fields:
;    times - start,end time of exposure
;    flux -  flux in exposure in (ph cm^-2 s^-1)
;    flux_err - error on flux
;    ts   - test statistic 
;    exp - exposure in (m^2 s)
;  
;  Written, Jan-2014, Kim Tolbert
;  Modifications:
;   17-Jul-2017, Kim, take care of byte values of 0 in csv files (prevented reading via rd_tfile)
;  
;-

function lat_read_maxlike_file, file=file

checkvar, file, 'SUN-ORB-REP_2013-10-25.csv

; 17-Jul-2017, Kim, noticed that these files had 0s in them, which are NULLs, and rd_tfile won't read past them.
; Read the file in as bytes, change all the zeros to 32s and write back out. Then read as normal with rd_tfile.
temp_file = 'lat_temp.txt'
file_delete, temp_file, /ALLOW_NONEXISTENT ; make sure there's not an old one that we use by accident
info = file_info(file)
openr, lun, file, /get_lun
b = bytarr(info.size)
readu, lun, b
free_lun, lun
q = where(b eq 0b)
b[q] = 32
openw, lun, temp_file, /get_lun
writeu, lun, b
free_lun, lun

list = rd_tfile(temp_file, /auto, delim=',')
if list[0] eq '' then return, -1

dur = float(reform(list[2,*]))
time_start = anytim(reform(list[0,*]))
time_end = time_start + dur

struct = {times: [0.d,0.d], flux: 0., flux_err: 0., ts:0., exp: 0.}
struct = replicate(struct, n_elements(dur))
struct.times = transpose([[time_start],[time_end]])
struct.flux = reform(float(list[3,*]))
struct.flux_err = reform(float(list[4,*]))
struct.ts = reform(float(list[5,*]))
struct.exp = reform(float(list[6,*]))

return, struct

end