;+
; Name: gbm_match_trigcat
;
; Purpose: Return GBM trigger number(s) corresponding to input flare(s) (blank string returned if no match)
; 
; Arguments: flare - gbm flare structure(s) to find triggers for
;
; Method: Read trigger catalog and find any triggers whose angle to the sun is less than 2*uncertainty in angle, and
;  the trigger time is between the flare start and peak time
;
; Output: The trigger designation(s) are returned (blank string returned where no match)
;
; Written: Kim Tolbert, May 2010
; 18-Jan-2012, Kim.  Modified to find all triggers that occur during a flare, sort to put the one closest to the
;   peak time first (so it will be listed in ascii list, which just lists one), check that trigger class is
;   SFLARE and ignore angle information (previously used angle to make sure was near angle to Sun, and verified
;   that angle was within RADEC_UNCERT, but then learned that RADEC_UNCERT is unreliable. Flare cat structure
;   was modified to handle up to 10 triggers.
;
;-

function gbm_match_trigcat, flare

trigcat = gbm_read_trigcat(status=status)
if ~status then message,'Error reading GBM Trigger Catalog.'

nflare = n_elements(flare)
ntrig = n_elements(flare[0].trigger)
trig = strarr(ntrig, nflare)

; For each flare in gbm catalog, find triggers whose trigger time is within duration of flare
; For those triggers, find the ones that are labeled SFLARE, then sort those by the difference
; between the trigger time and the flare peak time so that the one closest to the peak time is
; first in list (this one will be displayed in ascii list).
for i=0,nflare-1 do begin
  q = where (trigcat.uttrig ge flare[i].utstart and trigcat.uttrig le flare[i].utend, count)

;  Don't use the angle info anymore.  The radec_uncert is not reliable.  18-jan-2012
;  if count gt 0 then begin
;    radec_sun = solephut(flare[i].utstart)
;    ang = get_angle( [trigcat[q].ra, trigcat[q].dec], radec_sun)
;    z = where (ang lt 2.*trigcat[q].radec_uncert, found)
;    if found gt 0 then trig[i] = trigcat[q[z]].trig_desig
;  endif

  ; Find SFLARE triggers and sort them by proximity to peak of flare
  if count gt 0 then begin
    z = where (trigcat[q].class eq 'SFLARE', nf)
    case 1 of
      nf eq 0:
      nf eq 1: trig[0,i]=trigcat[q[z]].trig_desig
      nf gt 1: begin
        f_trigs = trigcat[q[z]]
        diff = abs(f_trigs.uttrig - flare[i].utpeak)
        q = sort(diff)        
        trig[0:nf-1,i] = f_trigs[q].trig_desig                     
        end
    endcase
  endif
  
endfor

return, trig

end
