;+
; NAME: gbm_qlook_products
;
; PURPOSE: Make gbm qlook products for a specified time period
;
; Arguments:
; dates_in - single date (does full day), or range of dates to make ql products for.
;   If not specified, default is 4 days ago through end of yesterday
;   If more than 10 days are specified in range, does them in 10-day bunches (so we don't have too many
;   raw files accumulating on our disks)
;
; Input keywords:
; if none of do_... keywords are set, then it does all of them otherwise
; do_cat - scan dates_in period for flares and add to flare catalog
; do_dplot - make daily plots for dates_in period
; do_oplot - make orbit plots for dates_in period
; do_rsp - make rsp files for flares in dates_in period
; cleanup - if set, delete daily gbm files copied from gbm archive
;
; Kim Tolbert, May 2010
; 24-Sep-2010, Kim. Changed default (if no dates_in supplied) to be 4 days ago through end of yesterday
; 12-jul-2012, richard.schwartz@nasa.gov, change energy edges to [10.,14, 25.,50,100,300.] and threshold to 0.2e-6
;		to be robust whether gbm nai threshold is 6 or 10 keV
;-

pro gbm_qlook_products, dates_in, do_cat=do_cat, do_dplot=do_dplot, do_oplot=do_oplot, do_rsp=do_rsp, cleanup=cleanup

if chklog('GBM_DATA_DIR') eq '' then set_logenv,file='gbm_env'

today  = anytim(anytim(!stime), /date)  ; start of current day
checkvar, dates_in, [today - 4.*86400., today-1.]  ; default is 4 days ago to last second of yesterday
current_time = anytim(!stime)

; if single date, assume entire day
all_dates = anytim(dates_in)
if n_elements(all_dates) eq 1 then all_dates = [all_dates, all_dates+86399.d0]
all_dates[1] = all_dates[1] < current_time
a_all_dates = anytim(all_dates, /vms)

message, 'Creating ql products for dates ' + a_all_dates[0] + ' to ' + a_all_dates[1], /cont
do_cat = keyword_set(do_cat)
do_dplot = keyword_set(do_dplot)
do_oplot = keyword_set(do_oplot)
do_rsp = keyword_set(do_rsp)
do_all = 0
if do_cat + do_dplot + do_oplot + do_rsp eq 0 then do_all = 1

cleanup = keyword_set(cleanup)

gbm_ql = obj_new('gbm_ql')

; If asked for more than 10 days, do in 10 day bunches so we don't keep too many raw files around (cleanup should be set in that case!)
dates = [all_dates[0], all_dates[1] < (all_dates[0] + 10.*86400.)]
threshold = 1e-6
energy_range = [6.,12.,25.,50.,100., 300]
if dates[1] gt anytim('11-jul-2012') then begin
	energy_range[0] = [10.,14.]
	threshold = 0.2e-6
	endif
gbm_ql->set, erange = energy_range
gbm_ql->set, threshold = threshold

while dates[0] lt all_dates[1] do begin

  adates = anytim(dates, /vms)

  if do_all or do_cat then begin
    gbm_ql -> do_cat, dates, cat=cat, status=status
    if status eq 0 then message,'ERROR making catalog.', /cont
    if status eq 2 then message, 'No new flares found for dates ' + adates[0] + ' to ' + adates[1], /cont
  endif

  if do_all or do_dplot then gbm_ql->daily_plot, dates, /png
  if do_all or do_oplot then gbm_ql->orbit_plot, dates, /png

  if do_all or do_rsp then begin
    cat = gbm_ql->get(/cat,/force) ; force read in case we added new flares
    q = where (cat.utend gt dates[0] and cat.utstart lt dates[1], count)
    for i=0,count-1 do gbm_ql -> make_rsp, flare=cat[q[i]].id
  endif

  if cleanup then begin
    ; delete all the files we copied over for these dates.  For each day, copied previous and next day.
    dir = concat_dir('$GBM_DATA_DIR', time2file(timegrid(dates[0]-86400.,dates[1]+86400.,/days), /date_only))
    file_delete, dir, /recurs, /allow_nonexistent, /quiet
    message,'Deleting files for dates ' + adates[0] + ' to ' + adates[1], /cont
  endif

  message,'Completed making qlook products for ' + adates[0] + ' to ' + adates[1], /cont

  dates[0] = dates[1] + 86400.d0
  dates[1] = dates[0] + 10.*86400.d0
  dates = dates < all_dates[1]
endwhile

destroy, gbm_ql
message,'Exiting qbm_qlook_products.', /cont
end
