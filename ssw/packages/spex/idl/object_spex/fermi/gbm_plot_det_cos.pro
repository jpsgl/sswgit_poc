;+
; Name: gbm_plot_det_cos
;
; Purpose: Plot the detector cosines (of angle between each detector and Sun) for 6 most sunward
;   Fermi GBM detectors
;
; Method: Calls gbm_get_det_cos to locate and read the poshist file(s) for requested time either already 
;   in your current directory, or copies it (them) from the fermi gbm archive.  Determines most sunward
;   by sorting on average of each detector's cosine during the requested interval. Plot in a plotman window.
;   
; Arguments:
; date - Either a single time, or a range in anytim format (if sec, since 1979/1/1). If a single time,
;   then creates interval +- 30 minutes on either side of time.
;
; Input/Output Keywords:
; plotman_obj - If exists, use this plotman instance.  Otherwise create one and pass it out.
;   
; Examples:
;   gbm_plot_det_cos,['12-Jun-2010 00:00','12-Jun-2010 04:00'], plotman_obj=p
;   
; Written: Kim Tolbert 23-Feb-2011
; Modifications:
;-

pro gbm_plot_det_cos, date, plotman_obj=plotman_obj

time_range = n_elements(date) eq 1 ? anytim(date) + [-1800,1800] : anytim(date)
 
cos = gbm_get_det_cos(time_range, ut_cos=ut_cos, status=status)

if status then begin

  ave = average(cos, 2)
  s = reverse(sort(ave))
  dets = 'NAI_'+trim(indgen(12),'(i2.2)')
  
  utplot_obj = obj_new('utplot', ut_cos, transpose(cos[s[0:5],*]), $
    dim1_ids = dets[s[0:5]],dim1_sum=0, id='Fermi GBM Cosine of Detector Angle to Sun', $
    data_unit='cosine')
  
  if ~is_class(plotman_obj,'PLOTMAN') then plotman_obj = plotman()
  plotman_obj -> new_panel, input=utplot_obj, desc='GBM detector cosines'

endif

end