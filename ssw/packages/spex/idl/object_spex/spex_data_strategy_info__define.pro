;+
; Name: spex_data_strategy_info__define
;
; Purpose: Defines info structure for spex_data_strategy object
;
; Category: OSPEX
;
; Modifications:
;	15-Jul-2004, Kim.  Added spex_interval_filter
;	17-Aug-2005, Kim.  Added spex_deconvolved and spex_pseudo_livetime
;	26-May-2006, Kim.  Added spex_data_xyoffset
;	30-Jun-2006, Kim.  Added spex_tband
;-

pro spex_data_strategy_info__define

struct = {spex_data_strategy_info, $
          spex_respinfo: ptr_new(), $	; Response file written with spectrum file, or array of response values
          spex_file_time: [0.d0,0.d0], $ ; Start,end time of data in spectrum file
          spex_ut_edges: ptr_new(), $ ; Time bins of data in spectrum file, (2,n)
          spex_ct_edges: ptr_new(), $ ; Energy bins of data in spectrum file, (2,n)
          spex_area: 0.0, $     ; Area of detector/ instrument from spectrum file
          spex_title: '', $		; Title of data, e.g. 'HESSI SPECTRUM'
          spex_file_units: {spex_units}, $	; Structure of units info for data in spectrum file
          spex_detectors: '',  $	; String of detectors included in spectrum file
          spex_interval_filter: ptr_new(), $	; Filter state for each data interval
          spex_eband: ptr_new(), $ ; energy bands for viewing data
          spex_tband: ptr_new(), $ ; time bands for viewing data
          spex_data_origunits: {spex_units}, $ ; Structure with original units for spectrum data
          spex_data_units: {spex_units}, $ ; Structure with units of last accumulation for spectrum data
          spex_deconvolved: 0, $	; If 1, original data is already photons
          spex_pseudo_livetime: 0, $	; If 1, livetime isn't a real livetime.
          spex_data_pos: [0.,0.] $ ; xyoffset of source in data file
}

end
