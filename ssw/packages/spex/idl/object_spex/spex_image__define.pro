;+
; Time-stamp: <Tue Jul 26 2005 16:47:20 csillag tounesol.gsfc.nasa.gov>
; Name: spex_image__define
;
; Purpose:  This class is inherited by any spex image data class (like spex_hessi_image)
;
; Written, Kim and Andre, August 2005
; Modifications:
; 9-Feb-2006, Kim.  spex_deconvolved now depends on spex_image_full_srm value.  And added
;   no_dialog and done keywords to save_roi.
; 26-May-2006, Kim.  Added spex_data_pos to args.
; 30-Jul-2006, Kim.  Got rid of preview method.
; 3-Aug-2006, Kim.  Since put specfile property in spex_data_strategy structure, remove it from here.
; 3-Oct-2007, Kim. In process, use all_images in getdata, instead of use_single_return_mode
; 18-Jun-2008,  Kim. spex_specfile is now allowed to be an array so self.specfile must now be 
;   a pointer (was a scalar string). Also, use same_data to compare old and new values.
; 11-Mar-2016, Kim. Added spex_roi_use and spex_roi_integrate to explicit keywords in set method,
;  since they're tied together, need to set together.
;  
;---------------------------------------------------------------------------

pro spex_hessi_image_test

o = ospex( /no )
f = findfile( '../fits/cc*fits' )
o -> set,  spex_spec =  f[2]
print, o->get( /spex_spec )
print, o->get( /im_in )
o->set, spex_roi_infile = 'gaga.sav'
data = o->getdata()

; try with the new format
o = obj_new( 'spex_hessi_image' )
f = findfile( '../fits/cc*fits' )
o -> set,  im_in =  f[2]
print, o->get( /spex_spec )
print, o->get( /im_in )
data = o->getdata()

; try with the old format
o = obj_new( 'spex_hessi_image' )
o -> set,  im_in =  'test_cube_small.fits'
print, o->get( /spex_spec )
print, o->get( /im_in )

spectra =  o->getdata()

end

;---------------------------------------------------------------------------

function spex_image::init, _extra = _extra

control = spex_image_control()
info = spex_image_info()

ret = self->spex_data_strategy::INIT( control = control, info=info,  _extra = _extra )

return, ret

end

;---------------------------------------------------------------------------

; Need a separate preview method here because read_data for image files
; has different arguments (image instead of spectrum,  errors,  livetime)

;pro spex_image::preview, $
;    spectrum,  errors,  livetime,  $
;    spex_respinfo, spex_file_time,  spex_ut_edges,  spex_ct_edges,  $
;    spex_area, spex_title, spex_detectors, $
;    spex_interval_filter, spex_units, spex_data_name, $
;    spex_deconvolved, spex_pseudo_livetime, spex_data_pos, $
;    err_code=err_code
;
;self->read_data, image, spex_respinfo, spex_file_time,  $
;                 spex_ut_edges,  spex_ct_edges,  $
;                 spex_area, spex_title, spex_detectors,  $
;                 spex_interval_filter, spex_units, spex_data_name, $
;                 spex_deconvolved, spex_pseudo_livetime, spex_data_pos, $
;                 err_code=err_code
;
;end

;--------------------------------------------------------------------
function spex_image::get, boxes=boxes, _extra=_extra, found=found, not_found=not_found

if keyword_set(boxes) then return, self.boxes else begin
	ret = self -> framework::get(_extra=_extra, found=found, not_found=not_found)
	return, ret
endelse

end

;--------------------------------------------------------------------

pro spex_image::set,  $
	spex_roi_infile = spex_roi_infile, $
	spex_roi_use=spex_roi_use, spex_roi_integrate=spex_roi_integrate, $
	_ref_extra =  extra, done = done, not_found = not_found

if exist(spex_roi_infile) then begin
	if spex_roi_infile ne '' then $
		self -> restore_roi, spex_roi_infile, done=done $
		else $
		done = 1
	if done then self->spex_data_strategy::set, spex_roi_infile=spex_roi_infile, done=done, not_found=not_found
endif

; Ugly, but since roi_use and roi_integrate are tied together have to do this in case user sets from command line.
; If setting roi_use, turn integrate off.  If setting integrate on, set roi_use to 99.  If setting integrate off, set
; roi_use to 0 only if it's currently set to 99, otherwise leave alone. 
if exist(spex_roi_use) then begin
  if spex_roi_use[0] ne 99 then spex_roi_integrate = 0
endif else begin
  if exist(spex_roi_integrate) then begin
    if spex_roi_integrate eq 1 then spex_roi_use = 99 else begin
      if (self->get(/spex_roi_use))[0] eq 99 then spex_roi_use = 0
    endelse 
  endif
endelse

if keyword_set(extra) or exist(spex_roi_use) or exist(spex_roi_integrate) then $
  self->spex_data_strategy::set, spex_roi_use=spex_roi_use, spex_roi_integrate=spex_roi_integrate, _extra=extra, done=done, not_found=not_found

end

;--------------------------------------------------------------------

pro spex_image::process, force=force, _extra = _extra

; If input file has changed, read new file and unset box selection.  Otherwise,
; just compute spectra from currently defined boxes.

if keyword_set(force) or not same_data(self->get( /spex_specfile ), *(self.specfile)) then begin
	print, 'in spex_image::process for new file'

    self->read_data, image, spex_respinfo, spex_file_time,  $
                     spex_ut_edges,  spex_ct_edges,  $
                     spex_area, spex_title, spex_detectors,  $
                     spex_interval_filter, spex_units, spex_data_name, $
                     spex_deconvolved, spex_pseudo_livetime, spex_data_pos, $
                     err_code=err_code

    if err_code then begin
        message, 'Image file is invalid'
        return
    end

    self->setinfoparams, spex_respinfo, spex_file_time,  spex_ut_edges,  spex_ct_edges,  $
                         spex_area, spex_title, spex_detectors,  $
                         spex_interval_filter, spex_units, spex_data_name, $
                         spex_deconvolved, spex_pseudo_livetime, spex_data_pos

	free_var, self.boxes
	*(self.specfile) = self->get( /spex_specfile )

	;if region selector is active, kill it since it's not valid anyamore and will crash
	if xregistered('spex_roi') then xkill, 'spex_roi'

endif else begin
	source = self -> get( /source )
	image = source -> getdata(/all_images)
	spex_ut_edges = self -> get(/spex_ut_edges)
	spex_ct_edges = self -> get(/spex_ct_edges)
endelse

self->compute_spectra, image, spex_ut_edges, spex_ct_edges, $
                       spectra, livetime, error

self->set,spex_deconvolved = ( self->get(/spex_image_full_srm) eq 0 )

self->setspectra, spectra, error, livetime

end

;--------------------------------------------------------------------------

; If t_use, e_use are defined, mark box in that image.  Otherwise use first image
; in t_index, e_index

pro spex_image::select_roi, $
	t_index, e_index, $
	t_use=t_use, e_use=e_use, $
	_extra=_extra
;	colortable=colortable

checkvar, colortable, 5

t_idx = exist(t_use) ? t_use : t_index[0]
e_idx = exist(e_use) ? e_use : e_index[0]

if ptr_valid(self.boxes) then begin
	if ptr_valid( (*self.boxes)[e_idx,t_idx] ) then begin
		nop = (*(*self.boxes)[e_idx,t_idx]).nop
		list = (*(*self.boxes)[e_idx,t_idx]).list
	endif
endif

boxes = self->select_roi_hook( t_idx, e_idx, nop = nop, list= list, cancel = cancel, _extra=_extra )

if not cancel and n_elements( boxes ) ne 0 then begin

    self -> set, /need_update, /this_class_only

    t_index = t_index(sort(t_index))
    e_index = e_index(sort(e_index))
;    if keyword_set(all_e) then e_index = [ 0,n_elements((*self.boxes)[*,0])-1 ]
;    if keyword_set(all_t) then t_index = [ 0,n_elements((*self.boxes)[0,*])-1 ]
    for it = t_index[0],t_index[1] do begin
        for ie = e_index[0],e_index[1] do begin
            ptr_free, (*self.boxes)[ie,it]
            (*self.boxes)[ie,it] = ptr_new(boxes)
        endfor
    endfor

    self -> save_roi, /no_dialog

endif

end

;--------------------------------------------------------------------------

function spex_image::valid_roi, nen, ntim, exists=exists

exists = 0
if not ptr_valid( self.boxes ) then return, 0

exists = 1
if not same_data( size(*self.boxes),  size(ptrarr(nen, ntim)) ) then return, 0

return, 1

end

;--------------------------------------------------------------------------

pro spex_image::clear_roi, t_index, e_index

for it = t_index[0], t_index[1] do begin
	for ie = e_index[0], e_index[1] do begin
		free_var, (*self.boxes)[ie, it]
	endfor

endfor

self -> set, /need_update, /this_class_only

self -> save_roi, /no_dialog

end

;--------------------------------------------------------------------------
; Save ROIs defined in an output save file.  Use name in argument if there, otherwise use
; param spex_roi_outfile.  If neither of those is defined, then if no_dialog is not set,
; prompt user for file name.  Returns done = 1 if actually saved the ROIs.

pro spex_image::save_roi, spex_roi_outfile, no_dialog=no_dialog, done=done, _extra=_extra

checkvar, spex_roi_outfile, self->get( /spex_roi_outfile )

done = 0

catch, err
if err then begin
	catch, /cancel
	help,/last_message
	return
endif

if ptr_valid(self.boxes) then begin
    IF not keyword_set(no_dialog) and not is_string(spex_roi_outfile) THEN BEGIN
        atim = trim( str_replace(anytim(!stime, /vms, /date), '-', '_') )
        outfile = 'ospex_roi_'+atim+'.sav'
        spex_roi_outfile = dialog_pickfile (path=curdir(), filter='*.sav', $
           file=outfile, $
           title = 'Select output ROI file name',  $
           group=group, $
           get_path=path)
    ENDIF


	if spex_roi_outfile ne '' then begin
	    boxes_save = *self.boxes
	    save, boxes_save, filename = spex_roi_outfile, _extra=_extra
	    done = 1
	endif
endif

end

;--------------------------------------------------------------------------

pro spex_image::restore_roi, nen=nen, ntim=ntim, spex_roi_infile, done=done

checkvar, spex_roi_infile, self->get( /spex_roi_infile )

done = 0
if spex_roi_infile ne '' then begin
    f = findfile( spex_roi_infile )
    if f[0] ne '' then begin
        restore, spex_roi_infile
        if exist(boxes_save) then begin
	        doit = 1
	        if keyword_set(nen) and keyword_set(ntim) then $
	        	if not same_data( size(boxes_save), size(ptrarr(nen,ntim)) ) then doit = 0
	        if doit then begin
		        free_var, self.boxes
		        self.boxes = ptr_new( boxes_save )
		        done = 1
		        message, /cont, 'ROIs restored from file ' + f
		    endif else begin
		    	message, /cont, "ROIs NOT restored from file " + f + " because dimensions don't match data."
		    endelse
		endif else message, /cont,'Error.  This is not an OSPEX ROI file ' + spex_roi_infile
    endif else message, /cont, "Can't restore ROIs.  Can't find file " + spex_roi_infile
endif

end

;--------------------------------------------------------------------------

pro spex_image::list_roi, spex_roi_infile

checkvar, spex_roi_infile, self->get( /spex_roi_infile )

if spex_roi_infile ne '' then begin
    f = findfile( spex_roi_infile )
    if f[0] ne '' then begin
        restore, spex_roi_infile
        if exist(boxes_save) then begin
	        dim = size (boxes_save, /dim)
	        nen=dim[0]
	        ntim = n_elements(dim) gt 1 ? dim[1] : 1
	        out = ['ROI file name = ' + f, $
	        	'', $
	        	'Number of energies: ' + trim(nen) + '     Number of times: ' + trim(ntim), $
	        	'', $
	        	'Number of ROIs in each image: ']
	        nbox = intarr(nen,ntim)
	        for ie = 0,nen-1 do begin
	        	for it = 0,ntim-1 do begin
	        		nbox[ie,it] = 0
	        		if ptr_valid(boxes_save[ie,it]) then begin
	        			nop = (*boxes_save[ie,it]).nop
	        			nbox[ie,it] = (n_elements(nop) eq 1 and nop[0] eq 0) ? 0 : n_elements(nop)
	        		endif
	        	endfor
	        endfor
	        for ie = 0,nen-1 do out = [out, arr2str(trim(nbox[ie, *]), ' ') ]
	        xmessage, out
	    endif else message, /cont,'Error.  This is not an OSPEX ROI file ' + spex_roi_infile

	endif else message, /cont, "Can't find ROI file " + spex_roi_infile
endif else message, /cont, 'No ROI file to list.'

end


;--------------------------------------------------------------------------
pro spex_image__define

dummy = {spex_image, $
         boxes: ptr_new(), $
         inherits spex_data_strategy }

end
