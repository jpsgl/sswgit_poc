;+
;
; NAME:
;   gbm_fix_counter_overflow, spec
;
; PURPOSE:
;  Corrects 2 byte counter overflow in the gbm spectral files
;
;
; CATEGORY:
;       SPECTRAL FITTING, SPEX - messenger
;
; CALLING SEQUENCE:
;
; CALLS:
;
; INPUTS:
;	Spec - gbm count rate structure from pha file, only overflow intervals are changed
;	Spec is obtained from mrdfits( phafile, 2, /dscale)
;	dscale must be used or counts will be a 2-byte integer field that can't
;	hold the corrected value properly
;	** Structure <7c92700>, 5 tags, length=1048, data length=1046, refs=1:
;	   COUNTS          DOUBLE    Array[128]
;	   EXPOSURE        FLOAT           4.08290
;	   QUALITY         INT              0
;	   TIME            DOUBLE      3.1930713e+008
;	   ENDTIME         DOUBLE      3.1930713e+008

;
; OUTPUTS:
;

; PROCEDURE:
;	Overflows appear in the spec.counts field and the exposure is also incorrect
;	when there is overflow.  This proc uses jumper to analyze and correct these oveflows
;	and then uses the corrected counts to compute exposure assuming a deadtime of 2.6 e-6 seconds per event
;
; Modification History:
;
; 18-feb-2011 richard.schwartz@nasa.gov
;
;------------------------------------------------------------------------------
pro gbm_fix_counter_overflow, spec
;IDL> help, spec,/st
;	** Structure <7c92700>, 5 tags, length=1048, data length=1046, refs=1:
;	   COUNTS          DOUBLE    Array[128]
;	   EXPOSURE        FLOAT           4.08290
;	   QUALITY         INT              0
;	   TIME            DOUBLE      3.1930713e+008
;	   ENDTIME         DOUBLE      3.1930713e+008
if size(spec.counts,/tname) eq 'INT' or size(spec.time,/tname) ne 'DOUBLE' then begin
	message,/info, ' SPEC extension must be obtained using mrdfits(phafile,2,/dscale). Returning'
	return
	endif
dt = spec.endtime - spec.time
n=n_elements(spec)
counts=spec.counts
for i=0, n-1 do begin &$
	temp =counts[*,i] &$
	jumper, temp, reset  &$
	counts[0,i]=reset   &$
	endfor
oldcounts   = spec.counts
spec.counts = counts
p = where( total(oldcounts,1) ne total(counts,1), np)

if np gt 0 then spec[p].exposure = dt-2.6e-6*total(counts[*,p],1)

end
