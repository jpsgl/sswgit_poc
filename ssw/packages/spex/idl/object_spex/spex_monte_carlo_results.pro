;+
; PROJECT: RHESSI
;
; NAME: spex_monte_carlo_results  
;
; PURPOSE: After running spex::monte_carlo, use this routine to read the savefile containing the 
;   Monte Carlo results and find the mode and 67%, 95%, and 99% credible intervals.
;   
; EXPLANATION: Get the Monte Carlo results from the save file. For each parameter that was fit (not
;   fixed during fit), the values of the parameter are examined and the mode and 
;   the 67, 95, and 99% credible intervals are calculated and printed on the screen.
;   67, 95, and 99% roughly correspond to 1,2,3 sigma errors assuming a normal distribution. The
;   mode is the midpoint of the histogram bin with the highest value.  If requested, a plot of 
;   each parameter binned into a histogram with nbins will be created.
;
; CALLING SEQUENCE: 
;   spex_monte_carlo_results, savefile=savefile, nbins=nbins, plot=plot, ps=ps
; 
; INPUT KEYWORDS:
;  savefile - Name of save file to read (default='monte_carlo.sav')
;  nbins - number of bins to use for histogram plot (default is calculated for each parameter using 
;    'Scott's choice' algorithm for bin size)
;  plot - if set, make plots of distribution of each parameter as well as chisq on screen,
;    each parameter in a new window (default=0)
;  ps - if set, make PostScript file containing all plots.  (if ps=1, then plot is automatically
;    set to 1).  Name of ps file is same as savefile with extension changed from sav to ps.
;  _extra - any additional keywords to pass to plot routine
;  
; OUTPUT KEYWORDS:
;   out_struct - Structure containing mode, nbins used, and 67,95,99% credible intervals for
;     each parameter
;
; WRITTEN: Kim Tolbert, March 2010
; 
; MODIFICATIONS:
; 8-Oct-2011, Kim. Call al_legend instead of legend (IDL 8. name conflict)
;-

pro spex_monte_carlo_results, savefile=savefile, nbins=nbins, plot=plot, ps=ps, out_struct=out_struct, _extra=_extra

if ~ file_test(savefile, /read) then begin
  message, 'Can not find or read save file ' + savefile, /cont
  return
endif

restore, savefile

plot = keyword_set(plot)
ps = keyword_set(ps)
if ps then plot=1

if ps then begin
  ; construct PS output file name.  Same as savefile, but with .ps extension
  psfile = concat_dir( file_dirname(savefile), file_basename(savefile,'.sav',/fold) ) + '.ps' 
  ps, psfile, /color, /land
  !p.thick=5
  !p.charsize=1.
  !p.charthick=3
  !p.font=0
endif

comp = str2arr(fit_function,'+')
for i=0,n_elements(comp)-1 do begin
  fit_struct = fit_model_components(comp[i], /struct)
  param_desc = append_arr(param_desc, comp[i] + ': ' + fit_struct.param_desc)
endfor

if plot then linecolors

nparam = n_elements(savestart)
niter = n_elements(savechi)
nf = average(savefullchi/savechi) ; # deg. of freedom
out_struct = 0

for ip=0,nparam do begin

  chiplot = ip eq nparam
  if chiplot then begin
    p = savechi
    name = 'Reduced Chi-square'
  endif else begin  
    p = savep[ip,*]
    name = param_desc[ip]
  endelse
  
  mm = minmax(p)
  if mm[0] eq mm[1] then continue  ; skip parameters that weren't fit
  
  ; Use user-specified number of bins if exists, otherwise calculate using 'Scott's choice' for bin size
  if exist(nbins) then nb = nbins else $
    nb = round ((mm[1]-mm[0]) / ( 3.5 * stdev(p) / (niter^(1./3.)) ))
    
  y = histogram(p,nbins=nb, locations=x)
  wbin = x[1]-x[0]
  max = max(y,ind)
  mode = x[ind] + wbin/2.  
  
  ; compute 67,95,99% credible intervals
  v67 = dist_ci(p, ci=.67)
  v95 = dist_ci(p, ci=.95)
  v99 = dist_ci(p, ci=.99)

  print,''
  print, name, '   ' + trim(niter) + ' iterations', '   ' + trim(nb) + ' bins'
  print,'Mode -+ 67% CI: ',mode, v67[0] - mode, v67[1] - mode, '  =  ', v67
  print,'Mode -+ 95% CI: ',mode, v95[0] - mode, v95[1] - mode, '  =  ', v95
  print,'Mode -+ 99% CI: ',mode, v99[0] - mode, v99[1] - mode, '  =  ', v99
  
  if keyword_set(plot) then begin
  
    if not ps then window,/free

    plot, x+wbin/2., y / total(y),/yno,title=name, xstyle=1,psym=10,charsize=1.7, xra=xra, $
      ytitle='Fraction of samples in bin', xtitle=name,_extra=_extra
    oplot, [mode,mode], [0,1000], color=2
    if ~chiplot then oplot, [savestart[ip],savestart[ip]], [0,1000], color=2, linestyle=2    
    for i=0,1 do begin
      oplot, [v67[i],v67[i]], [0,1000], color=7
      oplot, [v95[i],v95[i]], [0,1000], color=9
      oplot, [v99[i],v99[i]], [0,1000], color=12
    endfor
    
    ; for chi plot, overplot predicted chisq distribution for this many degrees of freedom
    if chiplot then begin 
      v = findgen(max(savefullchi)+20)
      pdf = chisqr_pdf(v,nf)
      dr = deriv(pdf)
      binwidth = 1. / (nf-2)
      oplot,v/(nf-2), dr* wbin / binwidth, color=10
    endif

    ; write legend and timestamp on plot
    text = [trim(niter)+' Iterations', trim(nb)+' Bins', 'Mode', 'Best-Fit','67% CI', '95% CI', '99% CI']
    bw = !d.name eq 'PS' ? 0 : 255
    color = [bw,bw,2,2,7,9,12]
    linest = [-99,-99,0,2,0,0,0]
    if chiplot then begin
      text = [text, 'Predicted, NF='+trim(nf)]
      color = [color, 10]
      linest = [linest, 0]
    endif
    right_legend = mode lt average(!x.crange)  ; put in upper right corner if mode less than half way across
    al_legend, text, color=color, linest=linest, box=0, right_legend=right_legend
    timestamp, /bottom
  endif
  
  struct = {name:name, nbin:nb, mode:mode, v67:v67, v95:v95, v99:v99}
  out_struct = is_struct(out_struct) ? concat_struct(out_struct, struct) : struct
endfor

if ps then begin
  psclose
  message,'Create PS file ' + psfile, /cont
  cleanplot
endif
end