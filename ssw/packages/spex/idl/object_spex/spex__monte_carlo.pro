;+
; PROJECT: RHESSI
;
; NAME: SPEX::MONTE_CARLO  
;
; PURPOSE: OSPEX method to determine the error in the best-fit parameters by doing many fits to the model  
;   based on the best-fit parameters varied within POISSON statistics levels, and examining the 
;   distribution of the resulting best-fit parameter values.
;   
; EXPLANATION: After the user sets up their ospex object, defines input file, background options, fit 
;   intervals and function, and finds the best-fit parameters, the user can call this method to
;   determine the error on those best-fit parameters for one of those intervals via a Monte Carlo technique.  
;   Here, we calculate the model using the best-fit parameters, convert to counts, and add background. Then
;   we generate a Poisson random deviate of this spectrum (using POIDEV), subtract the background, and 
;   fit this new spectrum using the best-fit parameters from the original spectrum as starting parameters. 
;   
;   We do this the requested number of times, and then save the results of all the fits in an IDL save file.  
;   We call spex_monte_carlo_results to read the save file and examine the distribution of each  
;   parameter to determine the 67%, 95%, and 99% confidence intervals (roughly 1, 2, and 3 sigma errors, 
;   assuming a normal distribution) and print them on the screen.  The user can call 
;   spex_monte_carlo_results directly and request plots of the distributions too.
;   
;   On exit, the ospex object is restored to its state on entering, including resetting the spex_summ
;   parameters (i.e. the values from the last fit iteration in this routine are not what is left in the 
;   spex_summ structure for this interval upon exit).  This means that if desired, you can run this routine
;   again with the same object rather than reinitializing the object with your original best-fit params.
; 
;   RESTRICTIONS:
;   
;   This works on only one interval at a time (specify the interval in the keyword).
;   
;   For good statistics that cover out to the 99% credible interval, you should probably do ~10,000 
;   iterations.  This can take a long time - the time depends mostly on the number of energy bins and
;   what functions are in your model (some functions are slower to compute).  During fitting the 
;   function is calculated with new parameters many times.  On a machine with a time_test3 time of .5,
;   with 100 energy bins and the model='vth+thick2+line+line+drm_mod+pileup_mod', it took ~14 hours to run 10,000
;   iterations. If you save an ospex script and the results file, you can move the script, results file, and
;   input files to any fast server and run the monte_carlo method there.
;   
;   This technique works well in cases where your best-fit parameters are well-defined.  It assumes
;   they are the correct answer, and varies the model around them.  It does not explore parameter space far
;   away from your best-fit parameters.  Chi-square mapping or Bayesian analysis might be more appropriate
;   for cases where there are multiple possible solutions.
;   
; CALLING SEQUENCE: 
;   o -> monte_carlo, niter=niter, interval=interval, savefile=savefile
;  
; KEYWORDS:
;  niter - number of iterations to perform (default = 1000)
;  interval - fit time interval number to find best-fit parameter errors for (default=0)
;  savefile - name of output file in which to store results from niter fits (default='monte_carlo.sav')
;
; WRITTEN: Kim Tolbert, September 2009
; 
; MODIFICATIONS:
;   8-Mar-2010, Kim. Turned routine into an obj method, made it work when there is more than
;     one fit interval defined (but still only works on one interval at a time), and added comments.
;   29-Apr-2010, Kim.  Added setting min,max,free,spectrum,model from previous fit
;   3-Sep-2010, Kim. Call new set_fitting_params method to set params from summ struct. Also, save
;     and restore spex_fit_start_method and spex_fit_init_params_only because user may have set them
;     to something that would screw this up.
;   28-Jun-2011, Kim. Write save file every 500 iterations, just so if crash, don't lose everything.
;
;-

pro spex::monte_carlo, niter=niter, interval=interval, savefile=savefile

checkvar, niter, 1000
checkvar, interval, 0
checkvar, savefile, 'monte_carlo.sav'

; set spex to not put up any windows that require user action during the Monte Carlo runs
nointeractive_orig = get_logenv('OSPEX_NOINTERACTIVE')
set_logenv, 'OSPEX_NOINTERACTIVE', '1'

; fitdata is what we'll replace with the changed model and put back in the object.
; bkbinned is the background spectrum for the requested interval
fitdata = self -> getdata(class='spex_fitint', spex_units='counts')
bkbinned = fitdata.bkdata[*,interval]

; Save values so we can restore them when we're done.
fitdata_orig = fitdata
firstint_method_orig = self -> get(/spex_fit_firstint_method)
start_method_orig = self -> get(/spex_fit_start_method)
init_params_only = self -> get(/spex_fit_init_params_only)
fit_manual_orig = self -> get(/spex_fit_manual)
interval_index_orig = self -> get(/spex_interval_index)
summ_orig = self -> get(/spex_summ)

; Use 'current' instead o 'prev_iter' so that we can store params back into fit_comp_params for
; each iteration, and it will use them to start (otherwise would use results from most recent fit)
; set spex fit methods to use current params, and auto-fit, and to work on selected interval
self -> set, spex_fit_firstint_method='current', $
  spex_fit_start_method='previous_int', $
  spex_fit_init_params_only=0, $
  spex_fit_manual=0, $
  spex_interval_index = interval, $
  spex_intervals_tofit = interval

; Get previously stored best-fit parameters for requested interval
params = summ_orig.spex_summ_params[*,interval]

; set min, max, spectrum, model, erange, uncert, itmax previously used for this interval
set_all=1 & set_all_comp=1
(self->get(/obj,class='spex_fit')) -> set_fitting_params,  set_all, set_all_comp, summ_orig, params, interval
;
;self -> set,  $
;           fit_comp_minima = summ_orig.spex_summ_minima[*,interval], $
;           fit_comp_maxima = summ_orig.spex_summ_maxima[*,interval], $
;           fit_comp_free_mask = summ_orig.spex_summ_free_mask[*,interval], $
;           fit_comp_spectrum = summ_orig.spex_summ_func_spectrum[*,interval], $
;           fit_comp_model = summ_orig.spex_summ_func_model[*,interval]

fitint_obj = self -> get(/obj,class='spex_fitint')

; Calculate model using best-fit parameters, on photon energy edges, and convert to counts.  
; convert_fitfunc_units will use DRM to convert to counts on count energy edges.
func_obj = self -> get(/obj,class='fit_function')
fit_function = self -> get(/fit_function)
drm=self -> getdata(class='spex_drm') ; do this just so drm edges are available for next line
func_obj -> set, fit_xvals = self -> get(/spex_drm_ph_edges), fit_comp_param=params
yfit = func_obj -> getdata()  ; this is photon flux spectrum in photon bins
yfit_count = self -> convert_fitfunc_units(yfit,spex_units='counts')  ; this is count 'counts' spectrum in count energy bins

; Get fit time interval for requested interval.
fit_times = self -> get(/spex_fit_time_used)
fit_time = anytim(fit_times[*,interval],/vms)

savefree = self -> get(/fit_comp_free_mask)

counts = yfit_count + bkbinned  ; model + background

; create arrays for storing results of fit iterations
savep = reproduce(params,niter) + 0.
savechi = fltarr(niter)
savefullchi = savechi
saveinput = reproduce(counts,niter) + 0.
savestart = params

seed=200

;loop, fitting on Poisson deviate of total counts - background
for i = 0L,niter-1 do begin
  new_counts = poidev(counts, seed=seed) - bkbinned
  fitdata.data[*,interval] = new_counts
  fitint_obj -> setdata, fitdata
  self -> set,fit_comp_params = params
  self -> dofit, spex_intervals_tofit=interval
  
  savep[*,i] = self -> get(/fit_comp_params)
  chi2 = self -> get(/mcurvefit_chi2)
  fullchi2 = self -> calc_summ(item='chisq_full') 
  savechi[i] = chi2
  savefullchi[i] = fullchi2[interval]
  saveinput[*,i] = new_counts 
  print,''
  print,' Completed Monte Carlo Iteration # ', trim(i+1), '/',trim(niter), '   Chi2,   Fullchi2  ', chi2, fullchi2[interval] 
  if (i mod 500) eq 0 then save, fit_function, fit_time, savep, savechi, savefullchi, saveinput, savestart, savefree, file=savefile
endfor

; Write save file with results from all iterations
save, fit_function, fit_time, savep, savechi, savefullchi, saveinput, savestart, savefree, file=savefile
print,''
print,'Wrote IDL save file ' + savefile
print,''

; Restore values in object that we changed
fitint_obj -> setdata, fitdata_orig
self -> set, spex_fit_firstint_method=firstint_method_orig
self -> set, spex_fit_start_method=start_method_orig
self -> set, spex_fit_init_params_only=init_params_only
self -> set, spex_fit_manual=fit_manual_orig
self -> set, spex_interval_index=interval_index_orig
fit_obj = self -> get(/obj, class='spex_fit')
fit_obj -> set, _extra=summ_orig, /this_class_only
set_logenv, 'OSPEX_NOINTERACTIVE', nointeractive_orig

; Print error ranges of each fitted parameter
spex_monte_carlo_results, savefile=savefile, nbins=60

end
