; Name: messenger_dist_correct
; Purpose: Compute corrections for time and area to account for MESSENGER's distance from
;  Sun.  Correct to Earth location.
;
; Input Keywords:
;   dist_file - distance file name to use (def is '$SSW_OSPEX/messengerresp/MESSENGER_Sun_Distance_v2.csv')
;   intime - time in seconds (rel. to 79/1/1) of MESSENGER data to correct
; Output Keywords:
;   time_offset - seconds to add to MESSENGER time to convert to Earth time
;   area_factor - scaling factor to multiply MESSENGER area by to convert to area at Earth
; 
; 	 1 Astronomical Unit = 149 598 000 kilometers
;  	 1 light second = 299 792.458 kilometers
;
; Richard, Kim, 29-Apr-2008
; Modifications:
; 25-Sep-2013, Kim. Use SSWDB_MESSENGER env. var. to find sun distance csv file
; 06-Mar-2014, Kim. Use new sun_messenger_heliographic.txt file instead of MESSENGER_Sun_Distance_v2.csv to get
;  distance from Sun vs time.  New file extends into 2015 and has 4-hour time resolution (old was 1-day).
;
pro messenger_dist_correct, dist_file=dist_file, intime=intime, time_offset=time_offset, area_factor=area_factor

;resp_dir = getenv('SSWDB_MESSENGER')
;if resp_dir eq '' then resp_dir = concat_dir ('$SSW_OSPEX', 'messengerresp')
;file = file_search (concat_dir(resp_dir,'MESSENGER_Sun_Distance_v2.csv'))
;
;default, dist_file, file
;
;dd = rd_tfile(dist_file,3,1,/auto)
;dist=reform(str2arr( arr2str( dd[0,*]+' '+dd[1,*],del=','),del=','),3,(size(/dim,dd))[1])
;ut = reform(anytim(timstr2ex(dist[1,*],/mdy)))

str = messenger_read_heliographic_position()

ind = value_locate(str.date, anytim(intime))

au_frac = (str[ind].d_km / 1.49598e8)[0]
time_offset =  499.05* (1.0 - au_frac)  ;1.49598e8/299792.458 = 499.05
area_factor = (1./au_frac)^2

end
