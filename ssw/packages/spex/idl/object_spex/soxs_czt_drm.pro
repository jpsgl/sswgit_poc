;+
; PROJECT:
;	SOXS
; NAME:
;	SOXS_CZT_DRM
;
; PURPOSE:
;	This function constructs and returns the DRM for SOXS czt detector
;
; CATEGORY:
;	Spectral Analysis
;
; CALLING SEQUENCE:
;	soxs_czt_drm, $
;	    fwhm=fwhm, $
;	    offset=offset, $
;	    ein2 = ein2, $
;	    area = area, $ ;nominal geometric area
;	    error = error
;
;
; INPUTS:
;
;		fwhm - constant, uses 2.0 keV
;		offset - empirical channel offset to fix apparent gain
;
;
; OUTPUTS:
;	   Returns DRM as 256 x 256 array
;			for a photon spectrum in photons/cm2/sec input
;			(i.e. integrated over photon energy bins), the output
;			is in units of cnts/cm2/s/keV
;			count_flux = drm # (photon_spectrum * DE)
;			where photon_spectrum is in units of photons/cm2/sec/keV
;			and DE is the input energy bin width in keV
;       EIN2 - edges in 2 x 256 array
;	    AREA - nominal area, 0.25 cm^2
;	    ERROR - 0/1 means no error/error
;
;
;
; RESTRICTIONS:
;	none
;
; PROCEDURE:
;	Reads efficiency, transmission, and edge files provided by
;	SOXS team.  Energy calibration is fit and uses, efficiency and transmission
;	are read and smoothed to energy edge midpoints.
;
; MODIFICATION HISTORY:
;	7-dec-2006, richard.schwartz@gsfc.nasa.gov
;	21-Sep-2007, Kim.  Made offset and fwhm inputs work.
;	28-Sep-2007, Kim.  Set default offset to 2
;
;-


function soxs_czt_drm, $
    fwhm=fwhm, $
    offset = offset, $
    ein2 = ein2, $
    area = area, $ ;nominal geometric area
    error = error

error = 1
drm = 0

area = .18 ; cm^2
default, fwhm, 2.0
default, offset, 2.0 ; empirical. suggested by k.j.shah

ems   = interpol( [4.,56], 243) ;- 1.4  ;;;;;;;;;;;;;;;;!!!!!!!!!!!!!
de    = avg( ems[1:*]-ems)
ems   = [ ems[0]-de*reverse(findgen(13)+1),ems]
if offset ne 0. then begin
	coef = poly_fit(findgen(256), ems, 1)
	ems = poly (findgen(256)-offset, coef)
endif
ein2  = get_edges(/edges_2, [ems-de/2,last_item(ems)+de/2])

oeres = energy_res(ein=ein2, eout=ein2, e_vs=ems, fwhm=ems*0+fwhm, sig_lim=8)
eres = oeres->getdata()
obj_destroy, oeres
czteff  = soxs_czt_photopeak( ems>2, area=area, error=error)
if error then return, 0

czteffmtrx= rebin(transpose(czteff),256,256)
drm = eres * czteffmtrx / rebin( de+fltarr(256), 256, 256)/area
error = 0
return, drm
end
