; Modifications:
; June 2010, Kim.  Added spex_data_dir, spex_panel_replace.  They aren't used yet.


pro spex_data_info__define

  struct = {spex_data_info, $
            spex_plotman_obj: obj_new(), $ ; plotman object reference
            spex_data_dir: '', $
            spex_panel_replace: 0 $
           }
end
