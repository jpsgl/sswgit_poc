;+
; Name: spex_fermi_gbm__define
;
; Purpose: Provide methods to deal with the fermi_gbm data when downloading and finding most
;   sunward files, etc for OSPEX.
;   
; Written: Kim Tolbert, June 2010
; 21-Oct-2010, Kim.  Delete download method. Add inherit spex_instr (and use its download method)
; 08-Aug-2011, Kim.  Add flare_time keyword to time2rsp method
; 02-Oct-2011, Kim.  Added download method.  Use spex_instr::download as usual, but then
;   for the unique dates that were copied by spex_instr::download, download the corresponding poshist files.
;-

function spex_fermi_gbm::init
self.cat_obj = obj_new('gbm_cat')
return,1
end

;-----

pro spex_fermi_gbm::cleanup
destroy, self.cat_obj
end

;-----

function spex_fermi_gbm::search, time_range, pha=pha, rsp=rsp, count=count, pattern=pattern, _ref_extra=extra
checkvar, pattern, 'cspec_'
ngbm = 0 & gbm_url=''
nrsp = 0 & rsp_url=''
checkvar, pha, 0
checkvar, rsp, 0
both = (pha+rsp eq 0)

if pha or both then spex_find_data, 'fermi_gbm', time_range, pattern=pattern, url=gbm_url, count=ngbm, _extra=extra
if rsp or both then spex_find_data, 'fermi_gbm_rsp', time_range, pattern=pattern, url=rsp_url, count=nrsp, _extra=extra
;help,ngbm,nrsp
count = ngbm + nrsp

if count eq 0 then return, ''
url = [gbm_url, rsp_url]
return, rem_blanks(url)
end

;-----
pro spex_fermi_gbm::download, files, copyfiles=copyfiles, dir=dir, err=err

self -> spex_instr::download, files, copyfiles=copyfiles, dir=dir, err=err 

; If we're downloading data files, let's download the poshist files for the corresponding dates too, just
; in case user needs them (and goes offline)
if err eq '' then begin
  times = file2time(file_basename(copyfiles))
  times = get_uniq(times)
  for i=0,n_elements(times)-1 do gbm_find_data, date=times[i], pattern='_poshist_', /copy, count=count
  message, /cont, 'Copied or already have poshist file(s) for day(s) ' + arr2str(anytim(times,/vms,/date),', ')
endif

end

;-----
; Sorts the files in 'files' by the most to least sunward for the flare contained in time_range.
; Returns the sort order (so if s=sort(order), then most sunward is in files[s[0]])
; Don't return sort(order) because, e.g. want to know if there are multiple files at order 0.
function spex_fermi_gbm::order, files, time_range
index = self.cat_obj -> whichflare(time_range, /index, /biggest)
sundets = self.cat_obj -> sunward_dets( index, /array)
sundets = '_'+sundets+'_'
order = intarr(n_elements(files)) + 1000 ; init to 1000
for i=0,n_elements(sundets)-1 do begin
  q = where (stregex(file_basename(files), sundets[i], /bool) eq 1, count)
  if count gt 0 then order[q] = i
endfor 
return, order
end

;-----
; Returns detector number in file name.  e.g. for file glg_cspec_n5_bn100612_0054_038_v00.rsp2,
; returns 'n5'
function spex_fermi_gbm::file2det, file
gbm_dets = arr2str(['b0','b1','n'+trim(indgen(10)), 'na', 'nb'], '|')
return, stregex(file_basename(file), gbm_dets, /extract)
end

;----
; Returns string containing detector and start time of rsp file, used for searching
; for correct rsp file for flare (e.g. '_n5_bn100612_0054').
; If no flare found in time interval, return string 'none' and status 0.
; if flare_time keyword is present return [start,end] of flare found
function spex_fermi_gbm::time2rsp, time_range, det, flare_time=flare_time, status=status
status = 0
flare = self.cat_obj -> whichflare(time_range, /struct, /biggest)
if ~is_struct(flare)then return, 'none' else begin
  status = 1
  flare_time = [flare.utstart,flare.utend]
  atim = time2file(flare.utstart, /year2digit)
  return, '_'+det+'_bn'+ atim
endelse
end 

;-----
; Given a list of pha (and possibly some rsp files), select the pha file for the most sunward detector, and the
; the corresponding rsp file
pro spex_fermi_gbm::select_files, files, time_range, specfile, drmfile

specfile = ''
drmfile = ''

q = where (stregex(file_basename(files), 'pha', /bool) eq 1, count)
if count eq 0 then return
phafiles = files[q] 
order = self->order(phafiles, time_range)
s = sort(order)
specfile = phafiles[s[0]]

; if no rsp files in list, return
q = where (stregex(file_basename(files), 'rsp', /bool) eq 1, count)
if count then return

; otherwise find rsp file that corresponds to specfile selected above
det = self->file2det(specfile)
rspstring = self->time2rsp(time_range, det, status=status)
if status eq 0 then return
q = where (stregex(file_basename(files), rspstring, /bool), count)
if count eq 0 then return
drmfile = files[q[0]]

end

;-----

pro spex_fermi_gbm__define
void = {spex_fermi_gbm, cat_obj: obj_new(), inherits spex_instr} ;, rsp_obj: obj_new(), inherits fermi_gbm}
end