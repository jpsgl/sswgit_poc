;+
; Name: SPEX::XTIME
;
; Purpose: OSPEX method to provide widget to handle input file selection based on time and
; across network to remote sites.
;
; Method:  This handles the data sets that have daily files available on remote sites
;  (currently fermi_gbm, fermi_lat, konus, hxrbs, soxs, messenger).  Has options to choose time based on 
;  flare catalog (the RHESSI, Konus, or FERMI_GBM catalog).  Once time is set, searches across network for 
;  list of files for that instrument and time.  User can download multiple files.  If click Accept, then
;  we make guess as to which of selected files to set as specfile and drmfile - for fermi_gbm we select
;  the most sunward, for messenger we select the .dat (instead of .lbl) file.
;
; Category: OSPEX
;
; Calling sequence:  o -> xtime  (o is an ospex object)
;
; Written: Kim Tolbert, June 2010
;
; Modifications:
; 17-Aug-2011, Kim. If ospex accum_time or file_time is valid, initialize xtime widget to that time and flare,
;   otherwise initialize to big 12-Jun-2010 flare (previously always did that)
; 02-Nov-2011, Kim. Added err keyword in call to download
; 26-Jan-2012, Kim. Added CTIME option for GBM.  Added more info to 'Help' output. For GBM, files are now grouped so
;   cspec (or ctime) file is followed by its rsp file.
; 19-Aug-2012, Kim.  Increased expand default from 2 to 20 minutes
; 18-Dec-2013, Kim.  Pop up a message while searching (if not nointeractive), and use specfile[0] when
;   checking if any files were selected before setting files
; 14-Jul-2015, Kim. Added more info to 'Help' output.  Changed 'Help' button to 'More Info'.
; 29-Jul-2016, Kim. Added konus, and cleaned up setting widget values from flare or time, and made catalog
;   default to type of data already being used (prev. always defaulted to FERMI_GBM)
; 05-Aug-2016, Kim. Don't expand times for konus flares, and unmap expand time widgets if catalog is konus
; 08-Sep-2016, Kim. For konus data, don't set spex_accum_time to flare interval (since if background available, 
;   data from file will be appended by one fake time interval)
;-

pro spex::xtime_update_time_flare, state=state, time=time, flare=flare, expand=expand

  ind = widget_info(state.w_catalog, /droplist_select)
  cat = state.catalogs[ind]
  
  if keyword_set(time) then begin
    state.time_range = time
    num = '' & times = [0.d,0.d] & desc = ''
    case cat of
      'RHESSI': begin
        fl = hsi_whichflare (time, err_msg=err_msg, /struct, /biggest)
        if err_msg eq '' && is_struct(fl) then begin
          num = trim(fl.id_number) & times = [fl.start_time, fl.end_time]
          dum = hsi_getflare(fl.id_number, desc=desc, err_msg=err_msg)
        endif
      end
      'FERMI_GBM': begin
        fl = state.gbm_cat -> whichflare(time, err_msg=err_msg, /struct, /biggest)
        if err_msg eq '' && is_struct(fl) then begin
          num = fl.id & times = [fl.utstart, fl.utend] & desc = state.gbm_cat->desc(fl.id)
        endif
      end
      'KONUS': begin
        fl = state.konus_obj -> whichflare(time, err_msg=err_msg)
        if err_msg eq '' && is_struct(fl) then begin
          num = fl.id & times = [fl.utstart, fl.utend] & desc = fl.desc
        endif
      end
      else:
      
    endcase
    
    state.flare.num = num
    state.flare.times = times
    state.flare.desc = desc
    if err_msg ne '' then a = dialog_message(err_msg)
    
  endif
  
  if keyword_set(flare) then begin
    err_msg = ''
    num = '' & times = [0.d,0.d] & desc = ''
    tim_expand = state.expand
    case cat of
      'RHESSI': begin
        a = hsi_getflare(flare, desc=desc, err_msg=err_msg)
        if err_msg eq '' then begin
          num = long(flare) & times = [a.start_time, a.end_time]
        endif
      end
      'FERMI_GBM': begin
        a = state.gbm_cat -> getflare(flare)
        if is_struct(a) then begin
          num = flare & times = [a.utstart, a.utend] & desc = state.gbm_cat -> desc(flare)
        endif
      end
      'KONUS': begin
        a = state.konus_obj->getflare(flare)
        if is_struct(a) then begin
          num = flare & times = [a.utstart, a.utend] & desc = a.desc
          tim_expand = [0.,0.]
        endif
      end
      else:
    endcase
    
    if num ne '' then begin
      state.flare.num = num
      state.flare.times = times
      state.flare.desc = desc
      state.time_range = [times[0] - tim_expand[0]*60., times[1] + tim_expand[1]*60. ]
    endif else err_msg = 'Flare ' + flare + ' not found.'
    
    if err_msg ne '' then a = dialog_message(err_msg)
  endif
  
  if exist(expand) then begin
    if state.flare.num ne '' then begin
      if expand[0] ne state.expand[0] then state.time_range[0] = state.flare.times[0]  - expand[0]*60.
      if expand[1] ne state.expand[1] then state.time_range[1] = state.flare.times[1]  + expand[1]*60.
    endif
    state.expand = expand
  endif
  
end

;-----

pro spex::xtime_update_widget, wbase
  widget_control, wbase, get_uvalue=state

  widget_control,state.w_ut_range, set_value=state.time_range
  
  ;flare = state.flare.num eq -1 ? '' : trim(state.flare.num)
  widget_control, state.w_flarenum, set_value=state.flare.num
  widget_control, state.w_desc, set_value=state.flare.desc
  
  ; Show the CTIME option only if data type selected is fermi_gbm
  dtind = widget_info(state.w_data, /droplist_select)
  dt = state.names[dtind]
  widget_control,state.w_ctime_base, map = (dt eq 'FERMI_GBM')
  
  ; Unmap the expand times widgets if catalog is konus
  ind = widget_info(state.w_catalog, /droplist_select)
  cat = state.catalogs[ind]
  widget_control, state.w_expandbase, map = (cat ne 'KONUS')
  
  ; If data type is konus create a label widget (if it's not there already) to show usage web site.
  ; Otherwise destroy that label widget if it's there.
  kalive = xalive(state.w_konus_usage)
  if dt eq 'KONUS' then begin
    if ~kalive then begin
      state.w_konus_usage = widget_label(state.w_database0, value=state.konus_usage_text)
      print, state.konus_usage_text ; print on screen too, so user can easily cut and paste into browser
      widget_control, wbase, set_uvalue=state
    endif
  endif else if kalive then widget_control, state.w_konus_usage, /destroy
  
end

;-----
pro spex::xtime_set_dataobj, state
  dtind = widget_info(state.w_data, /droplist_select)
  dt = state.names[dtind]
  class = 'spex_' + dt
  if ~is_class(state.dataobj, class) then begin
    destroy,state.dataobj
    state.dataobj = obj_new(class)
  endif
end

;-----
; Event handlers can't be object methods, so this is just a wrapper to get to the method

pro spex_xtime_event,event
  widget_control,event.top,get_uvalue=state
  if obj_valid(state.object) then state.object->xtime_event,event
  return & end
  
  ;-----
  ; xtime event handler
  
pro spex::xtime_event, event

  widget_control, event.id, get_uvalue=uvalue
  
  struct_name = size(event,/sname)
  
  widget_control, event.top, get_uvalue=state
  
  exit = 0
  download=0
  setfiles=0
  
  case uvalue of
  
    'datatype': begin
;      state.dataind = event.index
    end
    
    'list': begin
      cm = state.datatypes.comment
      text = state.names + '  ' + state.datatypes.life
;      q = where (cm ne '', count)
;      if count gt 0 then text[q] = text[q] + ', ' + state.datatypes[q].comment
      text = [text, $
        '', $
        ' GBM:  Files will be listed in most to least sunward order, unless there is no flare' + $
        ' in your time interval.  The four most sunward will be highlighted.' + $
        ' b0 and b1 are the higher energy BGO detectors; b0 is always the sunward one.' + $
        ' For CSPEC and CTIME data, spectrum files are daily, rsp files are for each solar flare interval.' + $
        ' For CTIME data, use the detector response files (.rsp or .rsp2) for CSPEC.', $
        '', $
        ' LAT:  Spectrum files are daily, rsp files are for each exposure. Select both for your time interval.', $
        '', $
        ' KONUS-WIND: Files are for flare intervals. Only 2012-2016 is available as of Sept. 2016. The ..._1.pha' + $
        ' and ..._1.rmf files cover 20-1250 keV, ..._2.pha and ..._2.rmf cover 0.25-15.8 MeV. If files named' + $
        '..._1_bg.pha and ..._2_bg.pha exist, they contain the background for the corresponding pha file (it will' + $
        ' be appended to data automatically). You need to copy all of the pha, rmf, and arf files.' + $
        ' The pha file will be set as input in OSPEX, the corresponding rmf and arf file will be read' + $
        " automatically, and the response information inserted directly into the ospex object. You don't need to set a" + $
        ' response file.  See http://www.ioffe.ru/LEA/kwsun/ for usage policy.', $
        '', $
        ' MESSENGER:  You need the .dat and .lbl files for each day. Only the .dat file' + $
        ' will be listed in the Select Input widget.  The .lbl file must be in the same directory as the .dat file.' + $
        ' The response matrix is computed - there is no response file.', $
        '', $
        ' HXRBS:  Files are for flare intervals.  The response matrix is computed - there is no response file.', $
        '', $
        ' SOXS:  You need the .les file.  The response matrix is computed - there is no response file. CZT and Si data' + $
        ' are in same file. After selecting file, a popup will allow you to choose CZT or Si.' ]
      a=dialog_message(text, /info)
    end
    
    'time_range': begin
      if ~same_data(state.time_range, event.value) then self -> xtime_update_time_flare, state=state, time=event.value
    end
    
    'catalog': self -> xtime_update_time_flare, state=state, time=state.time_range
    
    'flarenum': begin
      widget_control, event.id, get_value=flare
      flare = flare[0]
      ; if event was from leaving widget, then only act if number changed
      if struct_name eq 'WIDGET_TRACKING' then begin
        if trim(flare) eq '' then return
        if flare eq state.flare.num then return
      endif
      self -> xtime_update_time_flare, state=state, flare=flare
    end
    
    'flare_select': begin
      ind = widget_info(state.w_catalog, /droplist_select)
      cat = state.catalogs[ind]
      case cat of
        'RHESSI': begin
          flare = hsi_ui_flarecat (group=event.top, /select_one, $
            obs_time_interval=state.time_range, error=error)
          if flare[0] ne -1 and trim(flare[0]) ne state.flare.num then self -> xtime_update_time_flare, state=state, flare=flare
        end
        
        'FERMI_GBM': begin
          flare = state.gbm_cat->select(/id)
          if flare[0] ne -1 and flare[0] ne state.flare.num then self -> xtime_update_time_flare, state=state, flare=flare
        end
        
        'KONUS': begin
          flare = state.konus_obj->select(/id)
          if flare[0] ne -1 and flare[0] ne state.flare.num then self -> xtime_update_time_flare, state=state, flare=flare
        end
        
        else:
      endcase
    end
    
    'expand_times': begin
      old_expand = state.expand
      widget_control, event.id, get_value=expand
      if not same_data(expand, state.expand) then self -> xtime_update_time_flare, state=state,expand=expand
    end
    
    'search': begin
      ctime = widget_info(state.w_ctime, /button_set)
      self -> xtime_set_dataobj, state
      if not spex_get_nointeractive() then xmessage,['', '      Searching...       ', ''], wbase=wxmessage
      urls = state.dataobj -> search(state.time_range, ctime=ctime, count=count)
      if xalive(wxmessage) then widget_control, wxmessage, /destroy
      if count gt 0 then begin
        urls = state.dataobj -> order(urls, state.time_range, best=best, /verbose)
        *state.urls = urls
        widget_control, state.w_list, set_value=file_basename(urls), set_list_select=best
      endif else widget_control, state.w_list, set_value='No files found.'
    end
    
    'cancel': exit = 1
    
    'download': download = 1 ;begin
    
    'cancel': exit = 1
    
    'accept': begin
      download = 1
      setfiles = 1
      exit = 1
    end
    
    else:
    
  endcase
  
  if download then begin
    sel = widget_info(state.w_list, /list_select)
    if sel[0] ne -1 then begin
      self -> xtime_set_dataobj, state
      dir = curdir()
      state.dataobj -> download, (*state.urls)[sel], dir=dir, copy=copyfiles, err=err
      count = n_elements(copyfiles)
      if ~same_data(*state.copyfiles, copyfiles) then begin
        (*state.copyfiles) = copyfiles
        if is_string(err) then a=dialog_message(err) else $
          message,'Copied ' + trim(count) + ' files to ' + dir, /cont
      endif
    endif else message,'No files selected.',/cont
  endif
  
  if setfiles then begin
    ; if any files were copied, figure out most likely first choice, and set specfile and drmfile
    if ptr_exist(state.copyfiles) then begin
      state.dataobj -> select_files, *state.copyfiles, state.time_range, specfile, drmfile
      if specfile[0] ne '' then begin
        ; set spex_accum_time (in set_file) if not konus data. For konus, accum_time here is flare time range,
        ; but if background file is set, then a fake background time interval is added to end of times from pha file,
        ; and if we set file times here, we'll never see that last interval.
        accum_time = is_class(state.dataobj,'SPEX_KONUS') ? [0.,0.] : state.time_range
        self->set_file, specfile, accum_time=accum_time, dialog=0  ; set spex_specfile
        ;      self->set_time, state.time_range  ; set spex_accum_time to time_range
      endif
      if drmfile ne '' then self->set, spex_drmfile = drmfile
      if xalive(state.widget_notify) then $
        widget_control, state.widget_notify, send_event={id:0L, top:0L, handler:0L, select:1}
    endif
  endif
  
  if exit then begin
    destroy, state.dataobj
    widget_control, state.wbase, /destroy
  endif else begin
    widget_control, event.top, set_uvalue=state
    self -> xtime_update_widget, state.wbase
  endelse
  
end

;-----
; widget_notify is the widget id of the Refresh button in the xinput widget.  If we set specfile or
; drmfile from here, want that widget to refresh, so send event as though we clicked Refresh.

pro spex::xtime, group=group, gui_label=gui_label, widget_notify=widget_notify

  if xregistered ('spex_xtime') then begin
    xshow,'spex_xtime', /name
    return
  endif
  
  catalogs = ['FERMI_GBM', 'KONUS', 'RHESSI']  
  
  ; Find the ospex data types that allow web searches (datatypes.web eq 1b)
  datatypes = spex_datatypes()
  q = where (datatypes.web eq 1b, count)
  datatypes = datatypes[q]
  names = datatypes.name
  dets = datatypes.dets
  ndets = intarr(count)
  for i=0,count-1 do ndets[i] = n_elements(where(dets[*,i] ne ''))
  
  ; if a data type is already beins using in OPSEX, default to that data type, detector, and catalog
  q = where(strmatch(datatypes.class, obj_class((self->get(/obj,class='spex_data'))->getstrategy()), /fold_case), count)
  if count gt 0 then begin
    dataind = q[0]
    q1 = where(strmatch(datatypes[dataind].dets, self->get(/spex_data_sel), /fold_case), count)
    if count gt 0 then detind = q1[0] else detind = 0
    qcat = where(strpos(catalogs, datatypes[dataind].name) ne -1, kcat)
    if kcat gt 0 then catind = qcat[0]    
  endif else begin
    dataind = 0  ; default to gbm
    detind = 0
    catind = 0  ; default to GBM catalog
  endelse  
  
  konus_usage_text = 'For Konus-Wind usage policy please see http://www.ioffe.ru/LEA/kwsun/'
  
  expand = [20,20]
  checkvar, time_range, self->get(/spex_accum_time)
  if ~valid_time_range(time_range) then time_range = self -> get(/spex_file_time)
  
  checkvar, gui_label, ''
  
  get_font, font, big_font=big_font
  
  wbase = widget_base (group=group, title='SPEX Time Selection Options '+gui_label, $
    /column, space=2, xpad=10, ypad=10)
    
  tmp = widget_label (wbase, value='Select Time or Flare', $
    font=big_font, /align_center)
    
  w_ut_range = cw_ut_range ( wbase, $
    value=obs_time_interval, $
    label='', $
    uvalue='time_range', $
    /noreset, $
    /oneline, $
    frame=1 )
    
  w_flarebase = widget_base (wbase, $
    /column, $
    /align_center, $
    /frame );, sensitive=0 )
    
  w_whichbase2 = widget_base (w_flarebase, /row, /align_center, space=20 )
  
  w_catalog = widget_droplist(w_whichbase2, value=catalogs, uvalue='catalog', title='Flare Catalog: ')
  widget_control, w_catalog, set_droplist_select=catind
  
  w_which = widget_label (w_whichbase2, value = 'Flare number: ' )
  
  w_flarenum = widget_text ( w_whichbase2, value='', uvalue='flarenum', /editable, xsize=12, /tracking_events )
  
  tmp = widget_button (w_whichbase2,value='Flare Selection...', uvalue='flare_select' )
  
  w_desc = widget_label (w_flarebase, value='', uvalue='desc', /dynamic_resize, /frame, /align_center)
  
  w_expandbase = widget_base (w_flarebase, /row)
  
  format = '(i3)'
  w_expand_times = cw_range ( w_expandbase, $
    label1='Expand Times by ', $
    label2='minutes before  ', $
    value=expand, $
    format=format, $
    dropvals1=[1, 2, 3, 4, 5, 6, 8, 10, 15, 20, 25, 30], $
    dropvals2=[1, 2, 3, 4, 5, 6, 8, 10, 15, 20, 25, 30], $
    uvalue='expand_times', $
    xsize=4)
    
  tmp = widget_label (w_expandbase, $
    value='minutes after flare')
    
  w_database0 = widget_base(wbase, /column, /frame, space=10)
  w_database = widget_base(w_database0, /row, space=10)
  w_data = widget_droplist(w_database, value=names, uvalue='datatype', title='Data Type: ')
  widget_control, w_data, set_droplist_select=dataind
  
  tmp = widget_button (w_database, value='More Info', uvalue='list')
  tmp = widget_button (w_database, value='Search', uvalue='search')
  w_ctime_base = widget_base (w_database, /row, /exclusive, space=10)
  w_cspec = widget_button (w_ctime_base, value='CSPEC', uvalue='cspec')
  w_ctime = widget_button (w_ctime_base, value='CTIME', uvalue='ctime')
  widget_control, w_cspec, /set_button
  w_konus_usage = 0L ; will use this for a label widget when data type selected is konus
  
  w_list = widget_list(wbase, value='', ysize=14, /multiple, uvalue='list_select', /frame)
  
  w_button = widget_base(wbase, /row, /align_center, space=15)
  ;tmp = widget_button (w_button, value='Show File', uvalue='show_file')
  
  tmp = widget_button (w_button, value='Download Selected Files', uvalue='download')
  tmp = widget_button (w_button, value='Cancel', uvalue='cancel')
  tmp = widget_button (w_button, value='Accept and Close', uvalue='accept')
  
  state = { object: self, $
    wbase: wbase, $
    w_data: w_data, $
    ;  w_det: w_det, $
    w_ut_range: w_ut_range, $
    w_flarebase: w_flarebase, $
    w_catalog: w_catalog, $
    w_which: w_which, $
    w_flarenum: w_flarenum, $
    w_desc: w_desc, $
    w_expand_times: w_expand_times, $
    w_ctime_base: w_ctime_base, $
    w_cspec: w_cspec, $
    w_ctime: w_ctime, $
    w_database0: w_database0, $
    w_konus_usage: w_konus_usage, $
    konus_usage_text: konus_usage_text, $
    w_list: w_list, $
;    dataind: dataind, $
    detind: detind, $
    datatypes: datatypes, $
    names: names, $
    dets: dets, $
    ndets: ndets, $
    time_range: time_range, $
    catalogs: catalogs, $
    dataobj: obj_new(), $
    flare: {num: '', times: [0.d, 0.d], desc: ''}, $
    w_expandbase: w_expandbase, $
    expand: expand, $
    urls: ptr_new(/alloc), $
    copyfiles: ptr_new(/alloc), $
    gbm_cat: obj_new('gbm_cat'), $
    konus_obj: obj_new('konus'), $
    widget_notify: fcheck(widget_notify, 0L) }
    
  if valid_time_range(time_range) then self -> xtime_update_time_flare, state=state, time=time_range else $
    self -> xtime_update_time_flare, state=state, flare='100612_0054'  ; start with biggest GBM flare so far
    
  widget_control, wbase, set_uvalue=state
  
  self -> xtime_update_widget, wbase
  
  widget_offset, group, newbase=wbase, xoffset, yoffset
  
  widget_control, wbase, xoffset=xoffset, yoffset=yoffset
  
  widget_control, wbase, /realize
  
  xmanager, 'spex_xtime', wbase, /no_block
  
end
