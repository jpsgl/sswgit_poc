;+
;
; NAME:  Help_spex
;
;
; PURPOSE: Provide helpful information on commands, parameters and data arrays within SPEX
;
;
; CALLING SEQUENCE: help_spex, list=list, short=short, outcommand=outcommand
;
;
; INPUTS:
;	list - string array of parameter and command substrings for inquiry
;	short- only list the element names
; OUTPUTS:
;	outcommand - used to pass back help requests to main program level
; MODIFICATION HISTORY:
;	ras, 15-feb-94
;	ras, 19-aug-95, included idl help for variables
;	Version 3,
;	richard.schwartz@gsfc.nasa.gov, 14-nov-1997, place txt help files in
;	ssw distribution and use path_dir to locate.
;	1-oct-2001, ras, fix file finding.
;-


pro help_spex, prm, list=list, short=short, outcommand=outcommand

common help_spex_com, vtab, var,bvar, ptab, par, bpar, ctab, com, cbuf
outcommand = ''
file = loc_file( $
	path=concat_dir(getenv('SSWDB_SPEX'),'doc',/dir),'help_spex.txt',count=count)
if count eq 0 then begin
	message,'Cannot locate HELP_SPEX.TXT',/continue
	return
endif

if n_elements(par) eq 0 then begin
	read_seqfile, buff, file[0]
	stvar= (where(strpos(buff, 'Variables::::::') ne -1))(0)
	stpar= (where(strpos(buff,'Parameters::::::') ne -1))(0)

        stcom= (where(strpos(buff,  'Commands::::::') ne -1))(0)
	endcom= (where(strpos(buff,'Common Blocks:') ne -1))(0)
;Parse variables and descriptions
	bvar= buff(stvar+1:stcom-1)
	vtab= where( (byte(bvar))(0,*) ne 9,ntab)
	var=strarr(ntab)
	for i=0,ntab-1 do var(i)=(parse_comline(bvar(vtab(i)),/nocom))(0)
	vtab = [vtab, n_elements(bvar)]
;Parse parameters and descriptions
	bpar= buff(stpar+1:stvar-1)
	ptab= where( (byte(bpar))(0,*) ne 9,ntab)
	par=strarr(ntab)
	for i=0,ntab-1 do par(i)=(parse_comline(bpar(ptab(i)),/nocom))(0)
	ptab = [ptab, n_elements(bpar)]
;Parse commands and descriptions
	cbuf = buff(stcom+1:endcom-1)
	ctab= where( (byte(cbuf))(0,*) eq 9,ntab)
	com = strcompress(cbuf(ctab))
	ctab = [ctab, n_elements(cbuf)]

endif



case 1 of
	n_elements(list) ne 0: begin
	for k=0,n_elements(list)-1 do begin
		if (where(strpos( [par,com,var], strupcase(list(k))) ne -1))(0) eq -1 then begin
			print,'For input string, ',list
			print,'No command or parameters matched'
			return
		endif
		wp = where( strpos( par, strupcase(list(k))) ne -1, np)
		if np ge 1 then begin
			print, 'Parameter - '+par(wp(0))
			for j= ptab(wp(0))+1,ptab(wp(0)+1)-1 do print, bpar(j)
			iprm = where(par(wp(0)) eq prm.nam, nprm)
			p = prm(iprm)
			if nprm ne 0 then begin
				print,'	Type: ',p.typ, $
				   '   Number of values:	', p.numval
				if p.opt(0) ne '' then $
					print,'	Options: ',p.opt $
				else begin
					if p.min eq -999. then smin = 'Any' else $
					   r=execute('smin=string('+p.typ+'(p.min))')
					if p.max eq -999. then smax = 'Any' else $
					   r=execute('smax=string('+p.typ+'(p.max))')
					print,'	Min/Max values allowed: ' + smin + $
					  '  /  ', + smax
				endelse
			endif
			outcommand= par(wp(0))
		endif
		wp = where( strpos( var, strupcase(list(k))) ne -1, np)
		if np ge 1 then begin
			print, 'Variable - '+var(wp(0))
			for j= vtab(wp(0))+1,vtab(wp(0)+1)-1 do print, bvar(j)
			outcommand= var(wp(0))
		endif
		wp = where( strpos( com, strupcase(list(k))) ne -1, np)
		if np ge 1 then begin
			print, 'Command - '+ com(wp(0))
			for j= ctab(wp(0))+1,ctab(wp(0)+1)-1 do print, string(9b)+cbuf(j)
		endif
	endfor

	end

	else: begin
	if not keyword_set(short) then begin
		for i=0,n_elements(par)-1 do begin
			opar = sort(par)
			print, 'Parameter - '+par(opar(i))
			for j= ptab(opar(i))+1,ptab(opar(i)+1)-1 do print, bpar(j)
		endfor
		for i=0,n_elements(var)-1 do begin
			ovar = sort(var)
			print, 'Variable - '+var(ovar(i))
			for j= vtab(ovar(i))+1,vtab(ovar(i)+1)-1 do print, bvar(j)
		endfor

		for i=0,n_elements(com)-1 do begin
			ocom = sort(com)
			print, 'Command - '+ com(ocom(i))
			for j= ctab(ocom(i))+1,ctab(ocom(i)+1)-1 do print, string(9b)+cbuf(j)
		endfor
		endif else begin
			Print,'Parameters: '
			print,' '+par(sort(par))
			Print,'Variables: '
			print,' '+var(sort(var))
			print,'Commands: '
			print,' '+com(sort(com))
		endelse
	end
endcase

end
