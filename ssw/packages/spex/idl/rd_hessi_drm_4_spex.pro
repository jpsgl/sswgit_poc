;+
; PROJECT: SPEX
; NAME:   rd_hessi_drm_4_spex
;
; PURPOSE:  read HESSI drms from FITS/GENX files into SPEX
;
; CATEGORY: SPEX, spectral analysis
;
; CALLING SEQUENCE:
;   rd_hessi_drm_4_spex, file=_1file, sfile=sfile, edges_out=edges_out, $
;     edges_in=edges_in, drm=drm
;
; INPUTS:
;
; OUTPUTS:
;
; INPUT KEYWORDS:
;   FILE
;   SFILE
;
; OUTPUT KEYWORDS:
;   EDGES_OUT
;   EDGES_IN
;   AREA
;   DRM
;   ERROR
;
; SIDE EFFECTS:
; none
;
; RESTRICTIONS:
; none
;
; PROCEDURE:
; none
;
; Written 31-May-2001 Paul Bilodeau
; 25-jan-2002, ras, ensure area is scalar
;-
;-------------------------------------------------------------------------------
PRO rd_hessi_drm_4_spex, FILE=file, SFILE=sfile, EDGES_OUT=edges_out, $
  EDGES_IN=edges_in, AREA=area, DRM=drm, ERROR=error

IF Strpos( Strupcase(file[0]), 'FITS' ) GT -1l THEN BEGIN
  hessi_fits2drm, FILE=file[0], SFILE=sfile, EDGES_OUT=edges_out, $
    EDGES_IN=edges_in, AREA=area, DRM=drm
ENDIF ELSE BEGIN
  spex_drm_genx, FILE=file[0], DRM=drm, AREA=area, EDGES_OUT=edges_out, $
    EDGES_IN=edges_in, ERROR=error
ENDELSE
If n_elements(area) eq 1 then area = area[0]

END