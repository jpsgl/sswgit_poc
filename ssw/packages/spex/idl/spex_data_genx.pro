;+
; Project     : SDAC
;
; Name        : SPEX_DATA_GENX
;
; Purpose     : This procedure reports the flux from a genx file.
;
; Category    : SPEX, I/O
;
; Explanation : The flux and it's uncertainties are extracted from a genx file after
;		having been previously prepared.
;
; Syntax      : spex_data_genx, files=files, flux=flux, eflux=eflux, ut=ut, units=units,
;	area=area, ltime=ltime, edges=edges
;
; Examples    :
;
; Inputs      :
;
; Opt. Inputs :
;
; Opt. Outputs:
;
; Keywords    :
; files       - file or files with the data to be read
; start_time  -  
; end_time    -
;               optional, read file between start_time and end_time, defaults
;               are the start and end of the files, must be readable by ANYITM
;
; Keyword Outputs:
;
; flux        - fltarr(nchan,nbins) - counts/cm2/sec/keV, overflow and livetime corrected
; eflux       - fltarr(nchan,nbins) - sqrt(counts)/cm2/sec/keV
; ut          - dblarr(2,nbins), seconds referenced to 79/1/1
; units       - e.g. 'cm!u-2!n s!u-1!n keV!u-1!n'
; area        - detector area in cm2
; ltime       - fltarr(1,nbins)  accumulation live time in interval, seconds
;               changed to fltarr(nchan,nbins) - 20-oct-94
; edges       - fltarr(2,nchan)  lo and hi edges in keV
; delta_light - fltarr(nchan), e.g. for BATSE SHER, channel width in pulse height
;               see cal_sher and calibrate, and Band 1992 on SLED problem for 
;               discussion.  This allows channel widths for normalization (counts/keV)
;		to be different from channel edges.
; id_use      - actual detector id, to reference drm
; title       - title string referencing instrument, data
; mode        - special output for YOHKOH, mode=0 for non-flare, 1 for flare
; wchan       - indices of allowed channels
; AUTO        - further instructions to SPEX, e.g. "read_drm", may also be used for input
;
; Calls	      :
;		LOC_FILE, RESTGEN, COPY_STRUCT, ANYTIM, FIND_IX, GET_TAG_VALUE
; Common      :
;
; Restrictions:
;
; Side effects: P_DRM_READER, a string with a procedure name for building the DRM, may
;	be set if the tag P_DRM exists in the structure of the first arg. of the genx
;	file in files(0).
;
; History     : Version 1, RAS, 11-Jul-1997
;
; Contact     : Richard.Schwartz@gsfc.nasa.gov
;-
pro spex_data_genx, files=files, flux=flux, eflux=eflux, $
        ut=ut, units=units, area=area, $ 
        ltime=ltime, edges=edges, wchan=wchan, delta_light=delta_light, $
;       DUMMY PARAMETERS!!! FOR VERSION 3.6c
        start_time = start_time, end_time=end_time, flare=flare, $
        det_id=det_id, id_style=id_style , id_use=id_use,  $    
        auto=auto, noplot=noplot,  title=title, error=error
error = 1

filename = loc_file(files(0), path=[curdir(),'BATSE_DATABASE'], count=count)
if count eq 0 then return

restgen, file=filename, spex_data, packets

ut   = get_tag_value(packets, /ut, /quiet)
units= get_tag_value(spex_data, /units, /quiet)
area = get_tag_value(spex_data, /area, /quiet)
edges= get_tag_value(spex_data, /edges, /quiet)
ex_edges = get_tag_value( spex_data,/ex_edges, /quiet )
if datatype(ex_edges) eq 'STR' then test=execute( ex_edges)
edge_products, edges, width=wedges
delta_light= get_tag_value(spex_data, /delta_light, /quiet)
ex_delta_light = get_tag_value( spex_data,/ex_delta_light, /quiet )
if datatype(ex_delta_light) eq 'STR' then test=execute( ex_delta_light)
if not keyword_set(delta_light) then delta_light=wedges
wchan= get_tag_value(spex_data, /wchan, /quiet)
title = get_tag_value(spex_data,/title, /quiet)
p_drm = get_tag_value(spex_data,/p_drm, /quiet)



ut = reform( anytim( ut,/sec), 2, n_elements(ut)/2 )
start_time_sec = anytim( fcheck(start_time, 0.d0),/sec)
end_time_sec   = anytim( fcheck(end_time, 0.d0),/sec)
w0 = (find_ix( (ut(0,*))(*), start_time_sec) >0 )(0)
w1 = (find_ix( (ut(1,*))(*), end_time_sec) < n_elements(ut)/2 )(0)
if w0 gt n_elements(ut)/2-1 then w0 = 0
if w1 lt 0 then w1 = n_elements(ut)/2 -1
w1 = w1 > w0
ut = ut(*,w0:w1)
w  = lindgen(w1-w0+1)+w0
flux = get_tag_value(packets(w), /flux, /quiet)
if datatype(com_flux) eq 'STR' then test=execute( com_flux)
wkev  = wc_where(strcompress(units),'*/kev*',/case_ignore,kcount)
warea = wc_where(strcompress(units),'*/cm2*',/case_ignore,acount)
eflux= get_tag_value(packets(w), /eflux, /quiet)

ltime = get_tag_value(packets(w), /ltime, /quiet)
if not keyword_set(eflux) then begin
;
;	Assume flux is in counts/cm2/sec/keV, overflow and livetime corrected.
;       Flux assumed to be dimensioned, [nchan, nbin], detector already selected.
	nbin = n_elements( ut )/2
	nchan_live = n_elements( ltime ) / nbin
	nchan= n_elements( flux ) / nbin
	eflux = flux 
	
	if nchan_live eq 1 then for i=0, nchan-1 do $
		eflux(i,*) = sqrt( f_div( flux(i,*), ltime*delta_light(i)*area)) $
		else begin
		eflux  = temporary( sqrt( f_div( flux, ltime*area)))
		for i=0,nchan-1 do eflux(i,*) = f_div( eflux(i,*), sqrt(delta_light(i)))
		endelse
endif
help, p_drm
if keyword_set(p_drm) then result = spex_current('p_drm_reader',input=p_drm)

error=0

end

