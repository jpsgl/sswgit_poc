
;+
; NAME: wsetshow
;	
; PURPOSE: Make the desired window active when switching from
;	   spectral and temporal plotting domains.  Also,
;	   loads in the correct sysvariables for those windows.
;	   Used in SPEX for flipping between windows	
; CALLING SEQUENCE: wsetshow, i
;	
; INPUTS: iout - window index to bring up, novalue if just putting plotting
;	      values in common
; INPUT KEYWORDS:
;         /noshow - set the window, but don't raise it
;         time_win_id - set during initialization of SPEX.  Window
;                       number of time history window.
;         spec_win_id - set during initialization of SPEX.  Window
;                       number of spectral window.
;         /initialize - set during initialization of SPEX.  Set
;                       time_id and spec_id to values to corresponding
;                       win keywords.  Exit after setting these
;                       values.
;         win_name - string, either 'TIME' or 'SPEC' - sets iout to
;                    window number of appropriate window.
; MODIFICATION HISTORY:
;	ras, 22-apr-94, include sysvariables	
;	ras, 19-may-94, protect against current window not being 0 or 1
;	ras, 8-april-1996, always use old ! variables, even if redundant
;	richard.schwartz@gsfc.nasa.gov, 18-mar-1998, use xdevice() for
;	'X'
;       Paul Bilodeau, 20-March-2002, added time_win_id, spec_win_id,
;       initialize, and win_name keywords.  Added time_id and spec_id
;       parameters to wsetshow_common block.
;-
;
PRO wsetshow, iout, $
              noshow=noshow, $
              time_win_id=time_win_id, $
              spec_win_id=spec_win_id, $
              initialize=init, $
              TIMEHISTORY=timehistory, $
              SPECTRAl=spectral

COMMON wsetshow, time_id, spec_id, time_stc, spec_stc

IF n_elements(time_stc) eq 0 THEN $
  time_stc ={winsav, x:!x, y:!y, clip:!p.clip}

IF n_elements(spec_stc) eq 0 THEN $
  spec_stc ={winsav, x:!x, y:!y, clip:!p.clip}

IF Keyword_Set( init ) THEN BEGIN
    time_id = time_win_id
    spec_id = spec_win_id
    RETURN
ENDIF

checkvar, iout, -1	;if just saving state

IF Keyword_Set( timehistory ) THEN iout = time_id
IF Keyword_Set( spectral ) THEN iout = spec_id

device_in = !d.name

IF device_in NE xdevice() THEN set_plot, xdevice()

in = !d.window	;active window

IF iout NE in OR fcheck(test,0) NE 0 THEN BEGIN
    CASE in OF 
        time_id: begin 
            time_stc ={winsav, x:!x, y:!y, clip:!p.clip}
            winold= spec_stc
        END
        spec_id: BEGIN
            spec_stc ={WINSAV, X:!X, Y:!Y, CLIP:!P.CLIP}
            winold=time_stc
        END
        ELSE: BEGIN
            printx,'WARNING! WSETSHOW USE IS NON-STANDARD. ' + $
                   'NEXT OVERPLOT MAY BE WRONG!'
            winold = iout EQ spec_id ? spec_stc : time_stc
        ENDELSE
    ENDCASE
	
	; AKT - CHECK IF RANGE HAS BEEN SET FIRST (A PLOT HAS BEEN DONE)
        IF Total(winold.X.crange) NE 0.0 AND iout NE -1 THEN BEGIN  
            ;; SET THE AXES
            reset_xy, winold
        ENDIF 
    ENDIF
    
IF iout NE -1 THEN BEGIN 
    wset, iout
    IF NOT fcheck(nshow, 0) THEN WShow, iout
ENDIF

set_plot,device_in

END 
