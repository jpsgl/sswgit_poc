;+
;
; PROJECT:
; SDAC
; NAME:
; read_hessi_fits_4_spex
;
; PURPOSE:
; Read HESSI data from a FITS file into Spex internal variables.
;
; CATEGORY:
; SPEX
;
; CALLING SEQUENCE:
; read_hessi_fits_4_spex, _1file, START_TIME=start_time, END_TIME=end_time, $
;     LTIME=ltime, EDGES=edges, DELTA_LIGHT=delta_light, UT=ut, $
;     UNITS=units, AREA=area, WCHAN=wchan
;
; INPUTS:
;
; INPUT KEYWORDS:
;   FILES - Name of file to read.
;
; OUTPUTS:
;
; OUTPUT KEYWORDS:
;   The following keywords are set to values needed by Spex.  Many are described
;   in the Spex documentation.  Those not described are internal variables not
;   meant to be used / accessed by the user.  Those keywords listed in the
;   procedure definition line not listed here are included for Spex comptability,
;   and are not used by this procedure.
;   START_TIME
;   END_TIME
;   FLUX
;   EFLUX
;   UT
;   UNITS
;   AREA
;   LTIME
;   EDGES
;   DELTA_LIGHT
;   WCHAN
;   TITLE
;   COMMAND_OUT
;
; OPTIONAL OUTPUTS:
;
; COMMON BLOCKS:
; read_spex_com, data, ut_data, data_file_read, read_start, read_end
;
; SIDE EFFECTS:
;
; RESTRICTIONS:
;
; PROCEDURE:
;
; EXAMPLE:
; Not to be used directly.  Called from with SPEX to load HESSI data
;
; CALLS:
;   anytim, arr2str, checkvar, fits2spectrum, fxpar, printx, st2num, str_pow_conv,
;   str2arr
;
; Written 31-May-2001 Paul Bilodeau, RITSS/NASA-GSFC
; Mod 14-mar-2002, ras, changed command_out to 'read_drm'
;-------------------------------------------------------------------------------
PRO read_hessi_fits_4_spex, FILES=files, START_TIME=start_time, $
END_TIME=end_time, FLUX=flux, EFLUX=eflux, UT=ut, UNITS=units, AREA=area, $
COSINES=cosines, LTIME=ltime, EDGES=edges, DELTA_LIGHT=delta_light, $
WCHAN=wchan, TITLE=title, ERROR=error, COMMAND_OUT=command_out, $
_EXTRA=_extra

common read_spex_com, data, ut_data, data_file_read, read_start, read_end
checkvar,data,0
checkvar,data_file_read,''
checkvar,read_start,''
checkvar,read_end,''

command_out = 'read_drm'
com_param=command_out

delta_light= 1.0
checkvar,    units, ' s!u-1!n cm!u-2!n keV!u-1!n'

hsi_rd_fits_spectrum, files[0], $
  rate=flux, erate=eflux, ut=ut, $
  units=units, area=area, ltime=ltime, edges=edges, $
  primary_header=p_hdr, ext_header=sp_hdr,$
  ERR_MSG=err_msg, ERR_CODE=err_code, _extra=_extra

;fits2spectrum, files[0], PRIMARY_HEADER=p_hdr, EXT_HEADER=sp_hdr, $
;  EXT_DATA=sp_data, ENEBAND_DATA=en_data, ERR_MSG=err_msg, ERR_CODE=err_code

IF err_code THEN BEGIN
  printx, err_msg
  RETURN
ENDIF

;tags = Tag_Names( sp_data )



; Make any 3-D arrays into 2-D - multiple detector sets must be combined for
; analysis
flux_size = Size( flux )
nbin = flux_size[flux_size[0]]
nchan = flux_size[flux_size[0]-1L]
IF flux_size[0] GT 2L THEN flux = Total( flux, 2 )

IF ( Size( eflux ) )[0] GT 2L THEN eflux = Total( eflux, 2 )

;The next line is an approximation, it needs refinement
;That's why this shouldn't be done here.
IF ( Size( ltime ) )[0] GT 2L THEN ltime = avg( ltime, 2 )

; transpose the flux, eflux, and ltime matrices, if necessary

delta_light = get_edges( edges, /width ) # (1+fltarr(nbin))
acctime = float( get_edges( ut, /width)) ## (1+fltarr(nchan))

if n_elements( ltime ) eq 1 then ltime = ltime[0]
if n_elements( ltime ) ne n_elements(flux) then ltime = ltime ## (1+fltarr(nchan))


if strlowcase( units ) eq 'counts' then begin
  flux = f_div( f_div( flux / area, acctime*ltime),delta_light)
  eflux = f_div( f_div( eflux / area, acctime*ltime),delta_light)
  endif else begin ;It's rate
  flux = f_div(  flux / area,delta_light)
  eflux = f_div(  eflux / area,delta_light)
  endelse
units = ' s!u-1!n cm!u-2!n keV!u-1!n'
wchan = Lindgen( nchan )

start_time = anytim( fxpar( p_hdr, 'DATE-OBS' ), /vms )
end_time = anytim( fxpar( p_hdr, 'DATE-END' ), /vms )

title = 'HESSI SPECTRUM'

END