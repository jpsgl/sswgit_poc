;+
; PROJECT:
;	SPEX
; NAME:
;
;
; PURPOSE:
;	This procedure converts a set of energy intervals into
;	the wuse array in SPEX. Indices in wuse indicate the data bins
;	used in fitting.
;
; CATEGORY:
;
;
; CALLING SEQUENCE:
; Call from inside SPEX
;	IDL,spex_energy_ranges, energy_ranges
;
;	or outside SPEX
;
;	spex_energy_ranges, energy_ranges
;
; CALLS:
;	none
;
; INPUTS:
;       energy_ranges - An array of paired energies used to
;		select data bins used in model fitting in SPEX.
;
; OPTIONAL INPUTS:
;	none
;
; OUTPUTS:
;       none explicit, only through commons;
;
; OPTIONAL OUTPUTS:
;	none
;
; KEYWORDS:
;	none
; COMMON BLOCKS:
;	none
;
; SIDE EFFECTS:
;	none
;
; RESTRICTIONS:
;	none
;
; PROCEDURE:
;	none
;
; MODIFICATION HISTORY:
;	Version 1, richard.schwartz@gsfc.nasa.gov
;	ras, 7-nov-02
;
;-



pro spex_energy_ranges, energy_ranges

edges = spex_current('edges')

n = n_elements( energy_ranges )
energy_ranges = n eq 1 ? [energy_ranges, last_item(edges)] : energy_ranges
m = n /2 * 2

ee = energy_ranges(0:m-1)
func = since_version('5.3') ? 'value_locate' : 'find_ix'
s  = call_function( func, edges[0,*], ee)

n =  m / 2
s = reform( s, 2, n)
list = [0]

for i=0,n-1 do $

	list = [list, lindgen(s(1,i)-s(0,i)+1)+s(0,i)]

list = list(1:*)

list = list[uniq(list, sort(list))]

test = spex_current( 'wuse', input=list )

end

