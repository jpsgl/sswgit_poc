
;+
;Project:
;	SSW/HESSI
;Name:
;	SPEX_RUN_CURVEFIT
;
;Usage
;	SPEX_run_curvefit, interval, apar_in, niter, iter, FREE=FREE
;
;How to use;
;	First obtain accumulation intervals and initial fits on a spectrum
;	using SPEX.  Then exit SPEX and begin using this procedure to
;	easily change the fitting parameters for any accumulation interval without
;	having to use the SPEX interface. You may obtain the resultant values by
;	using the spex_commons common blocks directly or query them using
;	spex_current (e.g. chi = spex_current('chi'))
;
;	To save all of the variables in the common blocks use
;	(idl,)spex_save_data, filename, /all
;
;	[ (idl,) is needed if running from the SPEX prompt ]
;
;	Then in a later sswidl session you can restore using
;	spex_save_data, filename, /restore and continue using this routine,
;	spex_run_curvefit


;PURPOSE:
;	 This procedure provides a modular interface to the
;	fitting routine within SPEX.  It includes most of the SPEX
;   variables via the common blocks in SPEX (see spex_commons.pro)
;
;
;Category:
;	SPEX, ANALYSIS, FITTING
;
;Inputs:
;	APAR_IN - Parameters used in call to F_MODEL.

;	ITER - Number of times in outside loop where conversion factors are
;		recomputed.
;	       If ITER is set to 0, then fitting is done in count rate space,
;	       and there are no conversion factors used. default 0
;	NITER - Number of times in loop inside Curvefit where parameter
;		increments are calculated, default 5
;
;KEYWORD INPUT:
;	FREE - A bytarr for each parameter in the model function.
;	set (1) if parameter is free to vary, reset (0) if it is fixed.
;

;Explanation:
;	This procedure controls the input to the functional minimization
;	     program, usually CURVFIT. In the standard technique there is
;	     an inner and outer loop.  In the outer loop the conversion
;	     factors are recompute with the new values of the parameters
;	     obtained from CURVFIT.  The inner loop counter is used within
;	     CURVFIT.
;	If there are no degrees of freedom for two free parameters and two
;	data values, and one of the free parameters is a normalization parameter,
;	while the other is a shape parameter directly after it,
;	the procedure, SPECTRAL_RATIO, is used to obtain the "fitted" values.
;
;
;In the SPEX_COMMON_BLOCK OR IN THE SPEX PARAMETER STRUCTURE
;	 - SHOULD BE CHANGED OR QUERIED USING SPEX_CURRENT()
;	E_IN - Mean of m input energy bins, geometric mean is best normally, or 2Xm edges
;	W_IN - Width of input energy bins.
;	COUNT_2_FLUX - conversion vector from incident counts to count flux
;		     for hard x-ray data normally the product of energy bin width and area
;	LIVE - Livetime of observing interval in seconds (or unit of choice)
;	DRM  - Detector response matrix dimensioned n x m where n is the number
;	of output energy bins and m is the number of input energy bins.
;	F_MODEL - Name of functional model (string)
;	OBSERVE (OBS) - Source counting rate spectrum
;	EOBSERVE (EOBS) - Uncertainties on OBS calculated from Gaussian stat. on observed
;	USE_MOD_SIG     - If set, use Gaussian statistics on expected rate
;	BACKGROUND (BACK)  - Backgrount counting rate spectrum
;	EBACK (EBACK) - Uncertainties on BCK
;	UNCERT_MIN  - minimum relative uncertainty used to calculate weighting
;	E_OUT - Mean of n output energy bins, or 2XN edges.
;	W_USE - Indices to use in E_OUT and OBS
;	RANGE - Min and Max values allowed for each of APAR,
;		dimensioned 2,n where n is the number of parameters
;	QUIET - If set then informational print statements are suppressed.
;	NOLAMBDA - see curvfit
;	PCONV - pileup conversion factor if set, the ratio of a single photon pulse-ht
;		spectrum divided by the pileup pulse height spectrum
;		These corrections can be obtained using [sdac.spex]convolve.pro
;	NOFIT - Make all computations but don't run curvefit if set.
;	APAR - Input parameter values are adjusted to new values in CURVEFIT
;	SIGMAA - Uncertainties on the APAR from CURVEFIT, somewhat crude
;	NRESID - Residuals on fitted points normalized by uncertainties used, same
;		 length as wuse
;CALLS:
;	run_curvefit, default, spex_current
;History:
;	Written by richard.schwartz@gsfc.nasa.gov 17-apr-03
;-




pro spex_run_curvefit, interval, apar_in, niter, iter, $
	free=free

;include critical common blocks

@spex_commons

default, niter, 5
default, iter,0
apar = apar_in

default, free, spex_current('free')
sigmaa = apar(where(free)) * 0.0

run_curvefit, e_in=e_in, w_in=w_in, drm=drm, f_model=f_model, $
	e_out=edges, count_2_flux=count_2_flux, live=live(*,interval), wuse=wuse, $
	observe=obsi(*,interval), eobserve=eobsi(*,interval),uncert=uncert_vec, $
	use_mod_sig=fcheck(use_mod_sig,0), $
	background=backi(*,interval), eback=ebacki(*,interval),conversion=conversion, $
	iter=iter, niter=niter, quiet=quiet,  nolambda=nolambda, nofit=0, $
	chisqr=chisqr, apar=apar, free=free, range=range, sigmaa=sigmaa

convi(*,interval)    = conversion
apar_arr(*,interval) = apar
apar_last(*,interval) = apar
apar_sigma(*,interval) = apar_sigma(*,interval)*0.0
apar_sigma(where(free),interval) = sigmaa
chi(interval) = chisqr ;reduced chi square
need.fit = 0	;all fit parameters have been defined to something!

apar_in = apar
end

