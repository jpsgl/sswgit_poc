;+
;	add a line to the subtitle of the spectral plots
;	more_info must be set to display this
;-
pro more_subtitle, subtitle, sigmaa=sigmaa, chisqr=chisqr, more_info=more_info

if keyword_set(sigmaa) and keyword_set(chisqr) and keyword_set(more_info) then  $
	subtitle = subtitle+ '!c Red. Chisqr: '+string(chisqr,form='(g8.3)') + $
	' Uncertainties: '+ $
	arr2str(delim=', ',strcompress(/remove, string(sigmaa,form='(g8.3)') ))

end
;+
;
; NAME: spex_spec_plot
;
; PURPOSE: Plot an observed energy spectrum and its model
;
; CATEGORY: SPEX, GRAPHICS
;
;
; CALLING SEQUENCE:
;	spex_spec_plot, plotted, edges=edges, drm=drm, e_in=e_in, apar=apar, cflux=cflux
;
; CALLS TO: Fcount_rate, Model_components, fcheck, fcolors, checkvar, $
;	edge_products, datplot, countsmod_plot, eplot, graphics_page,alpha_page, 
;	option_changer, printx, timestamp, arr2str, atime, chklog, emenu, exist, 
;	f_div, f_use_widget, limits, respond_widg
;	
;
;REQUIRED KEYWORD INPUTS:
;	Required:
;	EDGES:  2 x Num_channels energy edges
;	DRM:    Detector Response defining relationship between input model and output response
;		DRM is Num_channels x Num_photon
;	E_IN:   Input energy edges for photon spectrum, 2 X Num_photon bins
;	APAR:	Parameters of model, If SET is set, then the values of APAR may be changed
;	by a popup menu.
;	CFLUX:   Counting rate flux, counts/uflux, often counts/cm2/s/keV, Num_channels long.
;
;Optional Keywords:
;	ECFLUX:  Uncertainties on cflux, defaults to [0.*cflux]
;	SIGMAA:  Uncertainties on model parameters
;	BFLUX:   Background flux, defaults to [0.*cflux]
;	EBFLUX:  Uncertainty on bflux, defaults to [0.*bflux]
;	UNITS:   Units of cflux, expressed as needed for figure, often' s!u-1!n cm!u-2!n keV!u-1!n'
;	SET:  	if set then interactively select fitting parameters
;	INTERVAL: Interval number used to reference a series of fits. default: [0]
;	TRANGE:  Time range covered by observation, for subtitle.
;	CHISQR:  Value of reduced chi-square for this fit.  Appears in subtitle if at all.
;	SUBTITLE: Subtitle of plot.  Overrides TRANGE if used.
;	EPLOT: If set, plot uncertainties
;	CAL_TEST: An option for experts only.
;	 If set, scaled plot comparing net source count/rate and model count/rate
;	 In the cal_test mode we are trying to highlight the correspondance between
;	 the details of the predicted model spectrum and the observed spectrum looking
;	 for any obvious systematic problems in the gain/detector-response.
;	 Therefore, we plot the net source count rate spectrum over the range
;	 indicated by edges(*,wuse) and overplot with the predicted source count rate.
;	 Furthermore if a linear plot is desired then the plot is autoscaled by
;	 multiplying by a power-law which will best flatten the plot and the yrange
;	 is selected from extrema of the product of the observed and the power-law over the
;	 the channels in wuse.
;	TITLE: Title used on figure, string. 
;	XTYPE: If set log xaxis, otherwise linear
;	YTYPE: If set log yaxis, otherwise linear
;	COLORS: Vector of colors to use, referenced to Linecolors.pro
;		Nominally the indices used are [5,9,3,4,7] for 
;		linecolor 5 (yellow) for source counts
;		linecolor 9 (sky blue) for  folded model counts, 
;		linecolor 3 (scarlet) for continuous model summed over the components,
;		linecolor 4 (orange) for background counts,
;		linecolor 7 (green) for the continuous model components.
;	GROUP:  This array controls the grouping (binning together) of the instrument energy
;		channels.
;		The new edges are the low edge of the first channel in each group and the low
;		edge of the second channel.
;	DELTA_LIGHT: If GROUP is used, then DELTA_LIGHT is mandatory.  Gives the channel widths
;		of the individual channels and may be different from the widths implied by
;		edges.  This still needs some testing, 11 April 1996, RAS
;	UNCERT:  minimum fractional uncertainty on background subtracted flux, def 0.0
; 	PCONV: Additional scaling between counts and photons.  A multiplicative factor
;	applied to the conversion factors calculated in GET_CONVERSION.PRO where the conversion
;	factor is given by the counts/photons.  It may be used to test modifications from
;	considerations such as pileup.
;	OVERPLOT: If set, overplot new spectrum
;	RESIDUALS:  If set, plot residuals.
;	THICK: thick keyword for plotting horizontal bars
; OUTPUTS:
;	no direct outputs other than apar described above.
;
; OPTIONAL OUTPUTS:
;	Plotted - An anonymous structure with the the plotted data points, ordinates and abscissae
;
; COMMON BLOCKS:
;	Function_com
;
; SIDE EFFECTS:
;	none
;
; RESTRICTIONS:
;	none
;
; PROCEDURE:
;	If 'COUNTS' appears in units, flux is displayed with no additional scaling,
;	and the bflux is also plotted. Otherwise, the (cflux-bflux) is divided by the 
;	conversion factor (see GET_CONVERSION.PRO) computed from the
;	drm and model function found in function_com.
;
; MODIFICATION HISTORY:
;	ras, 1994
;	ras, 9-Mar-1995, only 6 parameters per line in the subtitle
;	ras, 22-mar-95, added cal_test keyword
;	ras, 5-apr-95, fixed bug in energy bin averaging, creating gmatrix
;	rasm 24-jan-96, added residuals plot 
;	Version 2, ras, 11-April-1996, cleaned documentation, added overplot.
;	Version 3, ras, 31-May-1996, added subtitle keyword
;	Version 4, ras, 27-aug-1996, fixed for new meaning of group
;	added thick keyword
;	Version 5, ras, 24-sep-1996, e_in given default definition of ecflux
;	Version 6, ras, use call_procedure for batse specific routine, countsmod_plot.
;	Version 7, ras, set_graphics used to overwrite colors in window system, 11-sep-1997
;	Version 8, ras, used execute for set_graphics call to device, 4-nov-1997
;-

pro  spex_spec_plot,  plotted, edges=edges, drm=drm, e_in=e_in, apar=apar, cflux=cflux, $
ecflux=ecflux,  bflux=bflux, ebflux=ebflux, group=group, delta_light=delta_light, $
interval=ix, trange=trange, subtitle=subtitle, eplot=eplot, $
set=set, units=units, xtype=xtype, ytype=ytype, pconv=pconv, $
wuse=wuse, yrange = yrange, xrange = xrange, cal_test=cal_test, residuals=residuals, $
sigmaa=sigmaa, chisqr=chisqr, more_info=more_info, thick=thick, $
uncert=uncert, colors=colors, title=title, overplot=overplot

@function_com   
;FUNCTION_COM - Common and common source for information on fitting functions
;

common function_com, f_model, Epivot, a_cutoff
checkvar, Epivot, 50.0 ;Normal spectral pivot point for Hard X-ray spectra
checkvar, f_model, 'F_VTH_BPOW' ;DEFAULT FITTING FUNCTION, THERMAL AND BPOW
checkvar, a_cutoff, [10.,1.5]  ;parameters of low energy pl cutoff, keV and index
checkvar, linestyle, 0
checkvar, thick, 1
pmulti = !p.multi
checkvar, e_in, edges

checkvar, residuals, 0
if residuals then !p.multi=[0,1,2]

checkvar, colors, [5,9,3,4,7]
if n_elements(colors) eq 1 then colors = [colors,9]
if n_elements(colors) eq 2 then colors = [colors,3]
if n_elements(colors) eq 3 then colors = [colors,4]
if n_elements(colors) eq 4 then colors = [colors,7]

;If fitting parameters are passed, then show the model function
fit = keyword_set(total(abs(fcheck(apar,0.0))))	

;if in the units passed it says COUNTS, then plot in counts
;otherwise show a photon y axis, and project data into photons.
checkvar,units,'Counts s!u-1!n cm!u-2!n keV!u-1!n'
if strpos(strupcase(units),'COUNT') ne -1 then count=1 else count=0



cal_test = keyword_set(cal_test)
if cal_test then begin
    
    ;fit = 0 ;won't show photon model directly
    ;count = 1 ;scaled count units
    endif

;Do you want to set the parameters interactively?
set = fcheck(set, 0)


;log-log is default plot style
checkvar, xtype, 1
checkvar, ytype, 1
spxrange = fcheck(xrange, [0.0,0.0])
spyrange = fcheck(yrange, [0.0,0.0])

checkvar, uncert, 0.0
nchan = n_elements(cflux) ;number of ungrouped channels
wuse = fcheck(wuse, indgen(nchan))
ecflux1= fcheck( ecflux, cflux*0.0) 
bflux1 = fcheck( bflux, cflux*0.0) * count	;no background shown in photon plot
ebflux1 = fcheck( ebflux, cflux*0.0) * count	;no background shown in photon plot

cflux1 = cflux - fcheck( bflux, cflux*0.0)              ;cflux is assumed background subtracted
ecflux1 = sqrt( ecflux1^2 + fcheck(ebflux,cflux*0.0)^2)  

if keyword_set(trange) then begin
    ix = fcheck(ix,0)
    sapar = strcompress(/remove, string(apar,form='(g9.4)'))
    numpar = n_elements(apar)
    apar_list = arr2str(delim=', ',sapar(0:5<(numpar-1)) )
    if numpar gt 6 then for ii=6,numpar-1,6 do $
    apar_list = apar_list+'!c'+arr2str(delim=', ',sapar(ii:(ii+5)<(numpar-1)) )
    
    if not exist(subtitle) then $
    subtitle = 'Interval '+strtrim(ix,2)+'!c' +  $
    strmid(atime(trange(0),/pub),9,12) +' - '+$
    strmid(atime(trange(1),/pub),9,12) +$
    '!c' +f_model+' parameters: '+ apar_list
    endif

more_subtitle, subtitle, sigmaa=sigmaa, chisqr=chisqr, more_info=more_info

edge_products, edges, wid=wedges, mean=emdge
;-------------------------------------------------
;This code bins the channels together as specified by the variable group, if it exists
;The new edges are the low edge of the first channel in each group and the low
;edge of the second channel

if keyword_set(group) then begin
    nchan1 = n_elements(group(0,*)) ;number of grouped channels
    
    ;If the grouping extends to the last channel, then the edge array needs to be extended
    gedges = reform([edges(*),edges(*,nchan-1)],2,nchan+1)
    
    edges1 = transpose( reform( [ (gedges(0,group(0,*)))(*), (gedges(0,group(1,*)))(*)],nchan1,2))
    edge_products, edges1, wid= wedges1
    erange = limits( edges(*,wuse))
    wuse1 = where( edges1(1,*) gt erange(0) and edges1(0,*) lt erange(1))

    ;18-aug-94, ras, allow for difference in delta bin edges and bin width used for count_2_flux
    ;bin edges given by edges
    ;normalization width from pulse-height amplitudes given by delta_light
    ;	wdelta = wedges1*0.0
    ;	for i=0,n_elements(wdelta) do wdelta(i) = $
    ;		total(delta_light(group(0,i):group(1,i)-1)>group(0,1))
    if nchan1*nchan lt 50000L then begin ;gmatrix can be too large for large data vectors!!
      gmatrix = fltarr(nchan1,nchan)	
      for i=0,nchan1-1 do gmatrix(i,group(0,i):group(1,i)>group(0,1)) = $
        f_div(delta_light(group(0,i):group(1,i)>group(0,1) ), $
        total(delta_light(group(0,i):group(1,i)>group(0,1))))
    
    
      if fit then drm1 = gmatrix# drm ;Check this for vector drm!!
      cflux1 = gmatrix# cflux1
      bflux1 = gmatrix# bflux1
      ecflux1 = sqrt(gmatrix^2 # ecflux1^2) 
      endif else begin

      ;The gmatrix would be too large, have to sum over the new channels directly!
      if fit then drm1 = reform( fltarr(nchan1,n_elements(drm(0,*))))
      cflux2 = fltarr(nchan1)
      bflux2 = fltarr(nchan1)
      ecflux2= fltarr(nchan1)       
      for i=0,nchan1-1 do begin
	wsel = indgen( (group(1,i) - group(0,i) )  >1)+group(0,i)
        compscale = f_div(delta_light(wsel),total(delta_light(wsel)) )
	cflux2(i) = total( cflux1(wsel)*compscale ) 
	bflux2(i) = total( bflux1(wsel)*compscale ) 
	ecflux2(i) = total( ecflux1(wsel)*compscale ) 
	if fit then drm1(i)   = total( drm(wsel)*compscale )
      endfor
      cflux1 = cflux2
      bflux1 = bflux2
      ecflux1 = ecflux2

      endelse	

    endif else begin
    edges1 = edges
    wedges1 = wedges
    if fit then drm1 = drm
    wuse1 = wuse
    endelse	


edge_products, edges1, mean=emdge1
    edge_products, e_in, mean = em, width=w_in
    ord_em = sort(em)

if fit then begin 

    get_conversion, e_in=e_in, w_in=w_in, drm=drm1, e_out=edges1, $
    f_model=f_model, apar=apar, conversion=conv, model_in=model, model_out=model_out
    conv = f_div(conv, fcheck(pconv,1.0))
    crate = model_out * conv
    
    ;	crate = fcount_rate(em=em,model=model, drm=drm1, e_in=e_in, par=apar)
    ;	conv = crate(*) / (call_function(f_model, emdge1, apar))(*)
    
    mflux = crate(*)
    endif

conv = fcheck(conv, cflux1*0.+1.)
if count then conv = fltarr(n_elements(cflux1))*0.0 + 1.
if cal_test then begin
    call_procedure,"countsmod_plot", edges, e_in, drm, cflux, bflux, xrange=spxrange, apar=apar, $
    colors=colors, yrange=spyrange, f_model=f_model, wuse=wuse, delta_light=delta_light
    goto, leavehere
    endif

flux = cflux1/conv
eflux = (ecflux1 > uncert*cflux1)/conv

if spyrange(0) eq 0 and spyrange(1) eq 0 then $
spyrange = limits( f_div((flux+bflux1)(wuse1),conv(wuse1))) > 1e-6 < 1e4 

if spxrange(0) eq 0 and spxrange(1) eq 0 then $
spxrange = limits( edges1(*,wuse1)) 

;insert block for testing gain uncertainties
common gain_mod_test, gmod, win11, win12
checkvar, gmod, 1.0

y_obs = (flux+bflux1) / gmod
x_obs =  edges1 * gmod
if not keyword_set(overplot) then begin
    plot, x_obs(0,*),  y_obs, psym=3, xtype=xtype, ytype=ytype, ytitl=units, $
    xtitle = 'Energy (keV)', subtitle=fcheck(subtitle,''),$
    xrange=spxrange, yrange=spyrange,title=title
    ;Save for future overplotting
    win11 = {winsav, x:!x, y:!y, clip:!p.clip}
    
    endif else reset_xy, win11

;
;Plot the observed data points, or deconvolved data points
;

datplot, 0,0, xs=x_obs, y_obs, color=fcolor(colors(0)), thick=thick


b_obs = bflux1/gmod
;
;Plot the  background in counts
;
if total(abs(bflux1)) ne 0.0 then  datplot, 0,0, xs=x_obs, b_obs, color=fcolor(colors(3)), $
	thick=thick 
efluxp = eflux
if total(eflux)*keyword_set(eplot) ne 0 then begin
	efluxp = eflux ## fltarr(2,1)
	ycrange= crange('y')
	ycrange=convert_coord(fltarr(2),ycrange,/to_normal)
	ycrange=[ycrange(1,0)+.001,ycrange(1,1)-.001]
	ycrange=(convert_coord(/normal,fltarr(2),ycrange,/to_data))(1,*)
	efluxp(0,*)= eflux < (ycrange(1)-y_obs)
	efluxp(1,*)=-1.0* ( eflux < (y_obs-ycrange(0)))
	for i=0,n_elements(y_obs) -1 do $
	     oplot, (emdge1*gmod)(i)+fltarr(2), y_obs(i)+efluxp(*,i), color=fcolor(colors(0))
	endif

;
;Plot the  model as expected counts or deconvolved expected counts
;
device_test= execute('device,set_graphics=6')
if fit then begin
    y_exp = f_div(mflux+bflux1,conv)
    datplot, 0,0, xs=x_obs, y_exp,color=fcolor(colors(1)), linestyle=linestyle, $
	thick=thick, psym=4, symsize=0.5
                                               
    if keyword_set(set) * (not count) then begin
        widget=f_use_widget(/test,/continue )
        
        printx,string(/print, 'Parameters for '+f_model+':',apar)
        buttons=['CHANGE PARAMETERS' , 'CONTINUE']
        if widget then $
        ans= respond_widg( title='SPECTRAL PLOT DIALOG BOX', $
        message = [' The Photon Energy Spectrum for the Current Interval',$
        ' will be plotted along with the model spectrum.',     $
        ' To change the parameters of the model, '+f_model + ',', $
        'Click on CHANGE PARAMETERS, otherwise CONTINUE'], /column,$
        xoffset = FCHECK(xoffset,0), yoffset=FCHECK(yoffsets), $
        buttons= buttons) $
        else $
        ans = emenu(buttons, $
        mtitle=   [' The Photon Energy Spectrum for the Current Interval',$
        ' will be plotted along with the model spectrum.',     $
        ' To change the parameters of the model, '+f_model + ',', $
        'Select CHANGE PARAMETERS below, otherwise CONTINUE below.'])
        
        graphics_page
        if ans(0) eq 0 then begin;obtain new model parameters
            if widget then option_changer, 'Fit Parameters for '+f_model,'apar',direct=apar
            endif
        alpha_page
        y_inp = (call_function(f_model, e_in, apar))(ord_em)
        emodel = em(ord_em)
        oplot, em(ord_em), y_inp, color=fcolor(colors(2))
        endif else oplot, em(ord_em), model(ord_em), color = fcolor(colors(2))
    ;Plot model for each individual component
    norm_param = model_components(f_model)
    if n_elements(norm_param) gt 1 then $
    for i=0,n_elements(norm_param)-1 do begin
        apar_norm = apar
        apar_norm(norm_param) = 0.0
        apar_norm(norm_param(i)) = apar(norm_param(i))
;
;Plot the incident model
;
        oplot, em(ord_em), (call_function(f_model, e_in, apar_norm))(ord_em), color=fcolor(colors(4))
        endfor
    
    endif


device_test= execute('device,set_graphics=3')

if not keyword_set(overplot) then timestamp

if residuals then begin
    resid = f_div( flux-f_div(mflux,conv), eflux)
    res_range = minmax(resid(wuse1))
    if not keyword_set(overplot) then begin
        plot, edges1(0,*)*gmod, resid, psym=3, $
        xtype=xtype, ytitl='Normalized Residuals', $
        xtitle = 'Energy (keV)',$
        xrange=spxrange, yrange=res_range
        
        win12 = {winsav, x:!x, y:!y, clip:!p.clip}
        endif else reset_xy, win12
;
;Plot the residuals
;
    datplot, 0,0, xs=edges1*gmod, resid, color=fcolor(colors(0)) 	
    endif

;
; Simple interface into the values actually plotted!
;
    	
    x_inp = (fcheck( e_in, x_obs))(*,fcheck( ord_em,indgen(n_elements(x_obs(0,*)))))
    y_inp = fcheck( y_inp, (x_inp(0,*)*0.0)(*))
    plotted = {edges: x_obs,  $
      ocounts:y_obs,  $
      xcounts: fcheck(y_exp,y_obs),  $
      background:b_obs,  $
      e_in: x_inp,   $
      model:y_inp}

leavehere:
!p.multi = pmulti 
writeplotdata=getenv('WRITEPLOTDATA')
if keyword_set(writeplotdata) then writeplotdata=fix(writeplotdata) else writeplotdata=0

if writeplotdata then begin
  y_inp = (call_function(f_model, e_in, apar))(ord_em)
  emodel = em(ord_em)
  filename=!stime
  filename = strmid(filename,0,11)+'_'+arr2str( str_sep(strmid(filename,12,8),':'),'')+'.sav'
  save,file=filename, x_obs, y_obs, b_obs, efluxp, emodel, y_inp, y_exp, /verb
endif

alpha_page
end
