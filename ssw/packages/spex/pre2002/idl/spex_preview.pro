;+
; PROJECT:
;	SDAC
; NAME:
;	spex_preview
; PURPOSE:
;	find file names and display energy range and aspect info
;	in preparation for file reading in SPEX
; CALLING SEQUENCE:
;	spex_preview, data_tipe, flare, det_id, in_files, in_directory, ;Inputs
;		      dir_path=dir_path, files, det_cosines, dsel, last_found, command              ;Outputs
;
; INPUTS:
;	data_tipe - instrument and data format, gets set to upper case
;	flare     - event number for BATSE
;	det_id    - detector id
;	in_files  - 2 input file names to search for
;	in_directory - possible directory for data files
;	dir_queue - set of data directories to search including
;		    wildcards
;	standard_types - data formats integrated into SPEX environment
;	source_radec - ra and dec of cosmic source, used if ra or dec non-zero
;	event_time - If flare isn't given for BATSE data, then event_time must be
; OUTPUTS:
;	files       - found filenames
;	det_cosines - detector aspects for event
;	dsel        - selected detectors (BATSE)
;	last_found  - structure with dataa_tipe, det_id, flare of found files
;	command     - string which directs output path in spex
;		      set to '' if no file found satisfying condition
;	text_out    - block of text reporting success of preview
; KEYWORDS:
;	BATSE_BURST - optional reference to BATSE database, if non-zero may override Flare
; OPTIONAL OUTPUTS:
;
; PROCEDURE: uses findfile to find standard file names in directories
;		given in dir_queue.
;
; RESTRICTIONS: Defaults set for BATSE data, 14-apr-94. More to come.
;
; MODIFICATION HISTORY:
;	ras, 14-apr-94
;	ras, 5-may-94, yohkoh hxt and sxs update
;	RAS, 25-MAY-94, YOHKOH HXS AND GRS
;	ras, 22-aug-94, text is passed out to calling program
;	and not printed here
;	ras, 26-aug-94 adding smm hxrbs
;	ras, 18-oct-94, adding batse discsp
;	ras, 6-mar-95, integrated data_paths() with dir_queue for Yohkoh
;	ras, 20-sep-95, allowed sher_only or no sherb file condition
;	ras, 13-oct-95, searches current directory as a default
;	ras, 15-apr-96, added source_radec
;	ras, 14-nove-1996, passing event_time to run_file_search
;	ras, 18-jul-1997, replaced findfile, break_file mess with loc_file for non-standard
;	richard.schwartz@gsfc.nasa.gov, 26-sep-1997 added BATSE DISCLA.
;	Version 15
;	richard.schwartz@gsfc.nasa.gov, 29-sep-1997, moved all non-spex
;	calls to call_procedure.
;	richard.schwartz@gsfc.nasa.gov, 29-nov-2000 changed keyword test_dir
;		to spex_test_dir in calls to run_file_search
; 	1-June-2000 Paul Bilodeau added HXRS capability
; 	25-May-2001 Paul Bilodeau added HESSI capability
;-
;
;############################################################
;
pro spex_preview, data_tipe, flare, det_id, in_files, in_directory_in, $
dir_queue=dir_queue, standard_types=standard_types, text_out=text, $
source_radec=source_radec, event_time=event_time, $
files, det_cosines, dsel, last_found, command, batse_burst=batse_burst
if strcompress(in_directory_in,/remove) eq '' then $
cd, current=in_directory $
else in_directory = in_directory_in

checkvar, files, strarr(2)+''
text = ''
;Locate the filenames consistent with the specified directory and filename
;save them in the string array, found_files.

data_tipe = strupcase(data_tipe)

; Check to see if files can be found corresponding to data type and event
data_form = data_tipe(1)


wstandard = where( arr2str(delim=',', data_tipe) eq standard_types, nstandard)
if nstandard eq 0 then begin
    text = [text,string(/print,'data_tipe ',data_tipe,' is non-standard '),$
    'Standard types are: '+standard_types]
    det_cosines = fltarr(1) + 1. ;default det_cosines value
    det_id = 0
    endif

;	Check to see if data specifications have changed before trying to find the matching
;	file(s).
if strupcase(last_found.data(0)) ne strupcase(data_tipe(0)) or $
	strupcase(last_found.data(1)) ne strupcase(data_tipe(1)) or $
	last_found.file ne in_files[0] or $
	last_found.flare ne flare or $
	last_found.id ne det_id then dosearch=1 else dosearch=0

if dosearch then test_file = in_files

case data_tipe(0) of
    'BATSE': begin
        call_procedure,"spex_batse_preview", data_form, flare, det_id, in_files, in_directory_in, $
        dir_queue=dir_queue, standard_types=standard_types, text_out=text, $
        source_radec=source_radec, event_time=event_time, $
        files, det_cosines, dsel, last_found, command, batse_burst=batse_burst
        data_tipe(1) = data_form
        end   ;Close BATSE block
    'YOHKOH': begin
        det_cosines = fltarr(1) + 1.
        if data_form eq 'HXT' or data_form eq 'HXS' then det_id = 0 else $
        if data_form eq 'SXS' or data_form eq 'GRS' then det_id = det_id < 2 > 1 ;
        if data_form eq 'HXT' or data_form eq 'SXS' or data_form eq 'HXS'  or $
        data_form eq 'GRS'  then begin
            if dosearch then begin
                chkarg,'data_paths',foundit
                if foundit(0) then exresult = execute('new_dir_queue=data_paths()') else $
                new_dir_queue=dir_queue
                run_file_search, data_tipe=data_tipe, file_type='DATA', spex_test_dir = in_directory, $
                dir_queue=new_dir_queue, $
                test_file=test_file, det_id=det_id, flare=flare, out_fname=data_fname, out_dir=data_dir

                if data_fname(0) ne '' then files = concat_dir(data_dir(0), data_fname(0)) else begin
                    text = [text, $
                    string(/print,'No file found satisfying data type requirements ',data_tipe),$
                    string(/print,'For detector id ',det_id,' for flare ',flare)]
                    goto, opcom
                    endelse
                endif else text = [text,string(/print,'The data will be read from this file, ',files)]
            endif
        end
    'SMM': begin
        det_cosines = fltarr(1) + 1.
        if dosearch then begin
            run_file_search, data_tipe=data_tipe, file_type='DATA', spex_test_dir = in_directory, $
            dir_queue=dir_queue, $
            test_file=test_file, det_id=det_id, flare=flare, out_fname=data_fname, out_dir=data_dir
            if data_form eq 'HXRBS' then det_id = 0
            if data_fname(0) ne '' then files = concat_dir(data_dir(0), data_fname(0)) else begin
                text = [text, string(/print,'No file found satisfying data type requirements ',data_tipe)]
                goto, opcom
                endelse
            endif else text = [text,string(/print,'The data will be read from this file, ',files)]
        end
    'HIREX': begin
        det_cosines = fltarr(1) + 1. ;default det_cosines value
        det_id = 0
        files = loc_file(path=[curdir(),'SSWDB_HIREX','PERM_DATA'],'hirex.fits', count=nfiles)
        if nfiles eq 0 then  begin
            text=[text, $
            string(/print,'No files found satisfying DIRECTORY +  _1FILE ',test_file)]
            goto, opcom
            endif else begin
            files = files(0)
            text=[text,string(/print,'File ',files,' found.')]
            endelse
        end
    'HXRS': begin
    	; added 1-Jun-2000 by Paul Bilodeau
    	det_cosines = fltarr(1)+1.
    	det_id=0
    	hxr_in=strpos(in_files,'hxr')
    	hxr_def=where(hxr_in gt -1, ndef)
    	if ndef eq 0 then begin
    		files = loc_file(path=[curdir(),'HXRS_DATA'],'hxr*',count=nfiles)
    		if nfiles eq 0 then begin
    			text=[text, $
            	string(/print,'No HXRS data files found.')]
            	goto, opcom
    			endif else files=files[0]
    		endif else begin
    			; Make sure that the fully qualified name is given, not just the file name.
    			files=in_files[hxr_def[0]]
    			tmp=get_logenv('HXRS_DATA')
    			if strpos(files, tmp) eq -1 then files=concat_dir(tmp,files)
    		endelse
    	text=[text,string(/print,'File ',files,' found.')]
        end
    else:	begin
        ;Locate the file
        if dosearch then begin
            files = loc_file( path=in_directory, in_files(0), count=nfiles)

            case nfiles of
                0: begin
                    text=[text, $
                    string(/print,'No files found satisfying DIRECTORY +  _1FILE ', $
           ;         in_directory(0),infiles(0))]
           			in_directory(0),in_files(0))]
                    goto, opcom
                    end
                1: begin
                    files = files(0)
                    text=[text,string(/print,'File ',files,' found.')]
                    end
                2: begin
                    text = [text,'These files were found through findfile on '+test_file(0),$
                    'Change _1FILE and DIRECTORY to select 1 file.']
                    goto, opcom
                    end
                endcase
            endif
        text = [text,string(/print,'Data will be read from ',files)]
        end
    endcase

;Record the last data located.
if dosearch then begin
    last_found.data  = data_tipe
    last_found.file = test_file[0]
    last_found.flare = flare
    last_found.id    = det_id
    endif
return

opcom:
command(0) = '' ;return to command processor in spex.pro

end
