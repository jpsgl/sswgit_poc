;+
;Name: HESSI_SPEX
;
;Purpose: Procedure provides an initialization of SPEX and a procedural
;   interface customized to HESSI analysis.
;
;Keyword:
;   Input = String command for SPEX.  See Spex_proc.pro and SPEX documentation for syntax and
;     allowed commands;
;   CMODE = command_mode - set this to use SPEX in procedural mode.  Without this the
;     SPEX command parser takes over and waits for input.
;   COMMAND_FILE - full path to a command file with commands for the SPEX command parser;
;     each line will be interpreted as a separate SPEX command. ';' specifies a comment field
;     within the file.
;History: Based on spex_proc, see more documentation there
;   richard.schwartz@gsfc.nasa.gov, 10-nov-2003
;-

pro hessi_spex, input=input, cmode=cmode, command_file=command_file


data_tipe = spex_current('data_tipe')
;
;Initialization customized for SPEX,
if strpos(STRUPCASE(data_tipe[0]),'HESSI') eq -1 then begin
    spex_proc,input='data,hessi,allseg',/cmode
    spex_proc,input='_1file,hsi_spectrum*.fits',/cmode
    spex_proc,input='flare,0',/cmode

    IF    FILE_EXIST(COMMAND_FILE) THEN BEGIN
       text = rd_tfile(nocomment=';', command_file)
       ntext = n_elements(text)
       for i=0,ntext-1 do spex_proc,/cmode, input=text[i]
       endif

    spex_proc,input='/'
endif
spex_proc,cmode=cmode, input=input

end
