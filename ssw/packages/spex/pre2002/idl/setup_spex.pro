;+
; PROJECT:
; SDAC
; NAME:
; setup_spex
; PURPOSE:
; Initialize parameters and variables for spex.pro
; CALLING SEQUENCE:
; setup_spex, com_stack, style_sel, need, prompt, comments, $
;   script_opt1, script_opt2, standard_types, bdata, $
;   dir_queue, edg_units, last_found, last_read, wtitles, gaction, $
;   gdaction, goaction,  plotbuffer, bang_enter, setup_sys, merge_list=merge_list, $
;   nowidget=nowidget, last_plot=last_plot, handles=handles
;
; CATEGORY:
; SPEX, analysis
; INPUTS:
;
; OUTPUTS:
; com_stack,
; style_sel,
; need,
; prompt,
; comments,
; script_opt1,
; script_opt2,
; standard_types, A string arrays whose entries determine the valid
;     spacecraft and instruments recognized by SPEX
; bdata,
; dir_queue,
; edg_units,
; last_found,
; last_read,
; wtitles,
; gaction,
; gdaction,
; goaction,
; plotbuffer,
; bang_enter,
; setup_sys,
; KEYWORDS
; merge_list
; nowidget,
; last_plot
; handles
; OPTIONAL INPUTS:
;
; OPTIONAL OUTPUTS:
;
; PROCEDURE:
;
; RESTRICTIONS:
;
; MODIFICATION HISTORY:
; ras, 22-apr-94
; ras, 5-may-94, incorporate need structure and Yohkoh sxs changes
; ras, 17-may-94, update site specific setup
; ras, 15-jun-94, enable widget menus
;       ras, 14-oct-94, add spex_remote
; ras, 10-apr-95, changed data directory on umbra
; ras, 23-aug-95, changed data directory on umbra
; RAS, 13-jan-96, isas installation modified
; Version 2, ras, 8-apr-1996, align structure boundaries
;   added last_plot and handles initialization
; ras, 15-apr-1996, added stte to standard types
; ras, 16-may-1996, added near pin to standard types
; ras, 16-may-1996, added WIND TGRS to standard types
; ras, 14-aug-1996, made the comments section more robust
; ras, 21-oct-1996, added BATSE DISCSP_CONT to standard types
; ras, 2-dec-1996, replace RD_ASCII with RD_TEXT with LOC_FILE
; Version 16,
; ras, 26-mar-1997, added SMM GRS detectors, Spectrometer and Xray detectors merged
; Version 17,
; ras, 7-Jul-1997, changed dir_queue for ibdb files at sdac and umbra
; Version 18,
; richard.schwartz@gsfc.nasa.gov, 26-sep-1997 added BATSE DISCLA.
; Version 19,
; richard.schwartz@gsfc.nasa.gov, 14-nov-1997, place txt help files in
; ssw distribution and use path_dir to locate.
; Version 21,
; richard.schwartz@gsfc.nasa.gov, 3-feb-1998, modified DIR_QUEUE to use
; BATSE_DATABASE.
; richard.schwartz@gsfc.nasa.gov, 27-may-1998, revised to support WIN and MACos
; richard.schwartz@gsfc.nasa.gov, 19-march-1999, for unknown site, root db directories
; for BATSE to BATSE_DATABASE
; 1-June-2000 Paul Bilodeau added HXRS instrument and SHLD,NSHLD, and TOTAL formats
; 18-FEB-2001 richard.schwartz@gsfc.nasa.gov, 18-feb-2001, added HESSI FRONT,REAR,ALLSEG
;
;
;-
;
pro setup_spex, com_stack, style_sel, need, prompt, comments, $
  script_opt1, script_opt2, standard_types, bdata, $
  dir_queue, edg_units, last_found, last_read, wtitles, gaction, $
  gdaction, goaction,  plotbuffer, bang_enter, setup_sys, merge_list=merge_list, $
  nowidget=nowidget, last_plot=last_plot, handles=handles

@winup_common
;               common window_array, windows_index, windows_title

initialize:
  if not fcheck(nowidget,0) then $
    if f_use_widget(use = f_use_widget(/test)) eq 1 then print,'WIDGET MENUS ARE ENABLED!!!
  point, /compile   ;compile the point.pro file
  ;
  is_event_file = 0
  is_default_file = 0
  event_file = get_logenv('SPEX_EVENT')
  if keyword_set( event_file ) then event_file = loc_file( event_file, count=is_event_file)
  is_event_file = is_event_file < 1
  if not is_event_file then begin
    default_file = get_logenv('SPEX_DEFAULT')
    if keyword_set( default_file ) then default_file = loc_file( default_file, count=is_default_file)
    is_default_file = is_default_file < 1
    endif
  ;
  ; Initialize the op_com stack.
  ;
  case 1 of
    is_event_file: com_stack=['sumfile,'+event_file,'restore_event']
    is_default_file: com_stack=['dflts_file,'+default_file,'restore_dflts']
    else: com_stack=''

  endcase
  style_sel = ''
  if (size(need))(2) ne 8 then $
    need = {need_str2, read:1L, back:1L, select:1L, drm:1L, accum:1L, fit:1L,$
      tplot:1L, splot:1L, wuse:1L}
  hxrbs_format
  prompt ='SPEX> '  ;command line prompt
  bang_enter= {bang, p:!p, x:!x, y:!y, z:!z}
  !p.charsize=1.2
  !x.margin = [12,4]
  !y.margin = [7,3]
  setup_sys = {setup_sys, pcharsize:!p.charsize, xmargin:!x.margin, ymargin:!y.margin}
; if n_elements(xold) gt 0 then clear_utplot

; if n_elements(prm_spex) ne 0 then goto, not_first_time

  last_plot = ['graph','bspectrum']
  handles   = replicate( spex_handles_str(), 2) ; last spex_handles read and saved

  try_again:
  file = loc_file(path=[path_dir('spex'),'SSWDB_SPEX'],'spex_comments.txt',count=count)
  if count eq 1 then comments=rd_text( file)
  ncomments = n_elements(comments)
  if ncomments ge 11 then print,format='(a)',$
                [ 'The last ten lines from the COMMENTS command:',$
    comments( (ncomments-11>0):*)] else begin
    help, comments
    goto, try_again
    endelse

  set_utlabel,0

  script_opt1 =['time_history','clear_ut','graph','background','build','zoom','zoom', $
                'select_interval','bspectrum','enrange', 'photon_spectrum','fit']
  script_opt2 =['time_history','clear_ut','graph','zoom','background','create_ps', 'graph','zoom',$
    'build', 'select_interval','graph','display_intervals', 'create_ps', $
    'bspectrum','enrange','create_ps', 'photon_spectrum','fit','create_ps']

  checkvar, standard_types,['BATSE,SHERS','BATSE,STTE','BATSE,HERS','BATSE,CONT','BATSE,DISCSP',$
    'BATSE,DISCLA','BATSE,DISCSP_CONT', $
                'YOHKOH,HXT','YOHKOH,SXS', $
    'YOHKOH,HXS', 'YOHKOH,GRS', 'SMM,HXRBS', 'SMM,GRSX', 'SMM,GRS', $
    'HIREX,GERMANIUM','NEAR,PIN','WIND,TGRS', $
    ; Added 1-June-2000, Paul Bilodeau
    'HXRS,SHIELD','HXRS,NOSHIELD','HXRS,TOTAL',$
    ; Added 18-feb-2001, Richard Schwartz
    'HESSI,FRONT','HESSI,REAR','HESSI,ALLSEG']
  checksite = chklog('BATSE_DATA')
  if strpos(checksite(0), 'batse/batse0') ne -1 then setenv,'SPEX_SITE=SDAC_UMBRA'
      batse_data= chklog('BATSE_DATA')
      batse_cont = chklog('BATSE_CONT')
      batse_discsp = chklog('BATSE_DISCSP')
      batse_fdb = chklog('BATSE_FDB')
        case getenv('SPEX_SITE') of
  'ISAS': begin
     ;ISAS
           checkvar, dir_queue, ['?????',concat_dir(batse_data,'db_?????/'), $
    batse_cont, batse_discsp, batse_fdb]
  end
        'SDAC_UMBRA': begin
           checkvar, bdata, '/usr6/com/schwartz'
            data=chklog('$SCHWARTZ_HOME')
           checkvar, dir_queue, ['?????', concat_dir('$BATSE_DATABASE','db_?????',/dir), $
    batse_cont, batse_discsp, batse_fdb]
            ;data='/service/batse/batse1/batse/db_?????/
           ;checkvar, dir_queue, ['?????',data]

  end
        'SPEX_REMOTE': begin
           checkvar, bdata, '/usr6/com/schwartz'
            data=chklog('$SPEX_HOME')

           checkvar, dir_queue, ['?????',chklog('BATSE_DATA'),batse_cont,batse_discsp,batse_fdb]
           ;checkvar, dir_queue, ['?????',concat_dir(data,'spex_data/db_?????/'), $
  ; chklog('BATSE_DATA'),batse_cont,batse_discsp]

  end
  else:   begin
  if os_family() eq 'vms' then begin
  if file_exist('user_disk0:[richard]batse.dir') then begin
     ;SDAC
     checkvar, bdata, 'user_disk0:[richard.batse.drm]'
     checkvar, dir_queue, ['%%%%%',$
     [ 'batse_disk1:[data.batse.db.db_'] +'%%%%%'+']', batse_cont, batse_discsp, batse_fdb, $
  'batse_data','user_disk0:[richard.batse.1744_28.tjd_10100]',$
    'user_disk0:[richard.batse.1744_28.tjd_10100]']
  endif
  endif else $
           checkvar, dir_queue, ['?????', concat_dir('$BATSE_DATABASE','db_?????',/dir), $
    batse_cont, batse_discsp, batse_fdb]


  end
  endcase

  checkvar, edg_units, 'keV'

  strfound  ={found2, data: ['',''], file: '', flare: 0L, id:-1L }
  checkvar, last_found, strfound
  checkvar, last_read,  strfound

if !d.name eq 'PS' then begin
  print, 'Set_plot,"'+xdevice()+'"'
  set_plot, xdevice()
endif

if !d.name eq xdevice() then begin
  linecolors

  wtitles = ['Time History Window','Spectral Window']
  if n_elements(windows_index) gt 0 then $
    if total( wtitles(windows_index) eq windows_title) $
      eq n_elements(wtitles) then goto, windows_rdy
  xoff = 00 & yoff = 600
  device, get_screen=ssiz
  xsiz = .45*ssiz(0)
  ysiz = .4*ssiz(1)
  yoff = .55*ssiz(1)
  ;winup,r=2,xpos=xoff,ypos=yoff,xsiz=xsiz, ysiz=ysiz, /free, $ 0,  $
  winup,r=2,xpos=xoff,ypos=yoff,xsiz=xsiz, ysiz=ysiz,  0,  $
	titl=wtitles(0)
  ;winup,r=2,xpos=xoff+.55*ssiz(0),ypos=yoff,xsiz=xsiz,ysiz=ysiz,/free, $1, $
  winup,r=2,xpos=xoff+.55*ssiz(0),ypos=yoff,xsiz=xsiz,ysiz=ysiz,1, $
    title=wtitles(1)
endif
windows_rdy:


  ;names of graphics display commands
  gaction= ['time_history','bspectrum','count_spectrum','photon_spectrum',$
  'display_intervals', 'background_interval','fitting','graph']

  ;names of corresponding "display only" commands, no interaction
  gdaction= ['time_history','bspectrum','count_spectrum','photon_spectrum,noset',$
  'display_intervals', 'background_interval,display','fitting,plot','graph']

  ;names of overplotting "display only" commands, no interaction
        goaction= ['display_intervals', 'background_interval,display' ]

plotbuffer=strarr(2)
;
; Merge_list - this structure is used to sequentially read in data for merging
;
  datalist ={datalistxxx, data_tipe:strarr(2), _1file:'', _2file:'', $
  directory:'', flare:0L, det_id:0L, start_time_data:'', end_time_data:'' }
  checkvar,merge_list,{spex_mergelist, number:0L, datalist: replicate(datalist,10) }


end
