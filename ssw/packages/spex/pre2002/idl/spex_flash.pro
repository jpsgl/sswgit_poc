;+
; Project: SDAC
;
; Name: SPEX_FLASH
;	
; Purpose: This procedure highlights the current time interval being fit on
;	the time history plot.
; 
; Inputs:
;	IX - index of current interval
;	Select_int - position array of intervals on x axis, 2XN.
; Keyword Inputs:
;	OFF- Turn the highlighting off.
;	ON - Turn the highlighting on.
; History
;	ras, 19-jan-1996, change variable xselct to select_int to
;	avoid conflicts
;	richard.schwartz@gsfc.nasa.gov, 27-may-1998, support all windowing os
;-
pro  spex_flash, ix, select_int, off=off, on=on

if !d.name eq xdevice() then begin
if keyword_set(on) then color=fcolor(9)
if keyword_set(off) then color=0

 wsetshow,0
;stop
 ;wsetshow has restored !x and !y for the thistory window
 device, set_graphics=6
 oplot, select_int(0,ix)+dblarr(2),crange('y'), linestyle=2,thick=2, color=color
 oplot, select_int(1,ix)+dblarr(2),crange('y'), linestyle=2,thick=2, color=color
 device, set_graphics=3
 wsetshow,1
endif	

end	

