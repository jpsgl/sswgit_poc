;+
; NAME:
;        sb_wcs
; PURPOSE:
;        Converts a WCS structure into the form expected by sbrowser,
;        i.e., variation='CD', 2D only, fixed tag set, & other constraints.
;
;        If input WCS not supplied, create such a WCS structure for the
;        images as to be displayed in sbrowser.  Assumes that axis1
;        shall be parallel to the ecliptic plane.
; CATEGORY:
;        image processing
; CALLING SEQUENCE:
;        wcs_o = sb_wcs(wcs_i)
; INPUTS:
;        wcs_i : input WCS structure (usually from call to fitshead2wcs)
; KEYWORDS (INPUT):
;        disp : display structure.  must have the following tags:
;           naxis  = [sx,xy]     Size of display in pixels
;           delt_1 = float.      Scale in pixel/solrad
;           rpix   = reference pixel.  first pixel = [0,0] (unlike crpix)
;           rval   = heliogr. or carr. lon/lat/dist_from_sunc of refpix
;           carr   = using Carr.lon if true (otherwise Earth heliogr)
;        obswcs : wcs of observer.  Must have position and time substructures
;                                   The other portions of owcs are not used
; OUTPUTS:
;        wcs_o : output WCS structure (has a fixed set of tags)
; KEYWORDS (OUTPUT):
; COMMON BLOCKS:
; SIDE EFFECTS:
; RESTRICTIONS:
;        Various, in particular only for 2D images
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 4/11/2006
;        JPW, 2/27/2007 simple minded implementation of /sc and /helio
;                       proper way would be via expanded disp structure
;-

FUNCTION sb_wcs,wcs,disp=disp,obswcs=owcs  ,sc=sc,helio=helio,swcs=swcs

; fixed format wcs structure for sbrowser
gstim = {observ_date:'',exptime:1d0}
gspos = {soho:0B,pos_assumed:0B,dsun_obs:0d0,solar_b0:0d0,carr_earth:0d0, $
         hgln_obs:0d0,hglt_obs:0d0}
gswcs = {coord_type:'',wcsname:'',variation:'',compliant:0B,projection:'', $
         naxis:lonarr(2),ix:0L,iy:1L,crpix:dblarr(2),crval:dblarr(2), $
         ctype:['',''],cname:['',''],cunit:['',''],cd:dblarr(2,2),simple:0B, $
         time:gstim,position:gspos}
solrad = 6.9599d8   ; m

IF valid_wcs(wcs) eq 1 THEN BEGIN

 if n_elements(wcs.naxis) ne 2 then message,'Error: only NAXIS=2 supported'
 if wcs.coord_type ne 'Helioprojective-Cartesian' or $
    wcs.ix ne 0L or wcs.iy ne 1L $
    then message,'Warning: unexpected WCS coordinate system',/continue

 ; copy selected parameters from wcs into gswcs.  Others are fixed.
 ix               = wcs.ix
 iy               = wcs.iy
 gswcs.coord_type = wcs.coord_type
 gswcs.wcsname    = wcs.wcsname
 gswcs.variation  = 'CD'
 gswcs.compliant  = wcs.compliant
 gswcs.projection = wcs.projection
 gswcs.naxis      = wcs.naxis
 gswcs.ix         = 0L
 gswcs.iy         = 1L
 gswcs.crpix      = wcs.crpix
 gswcs.crval      = wcs.crval[[ix,iy]]
 gswcs.ctype      = wcs.ctype[[ix,iy]]
 gswcs.cname      = wcs.cname[[ix,iy]]
 gswcs.cunit      = ['arcsec','arcsec']
 gswcs.simple     = 0B                    ; cdelt, roll_angle not present

 ; crval, cdelt, cd:
 ; convert pc and crota systems to cd
 CASE strupcase(wcs.variation) OF
  'CD' : gswcs.cd = [[wcs.cd[ix,0],wcs.cd[iy,0]],[wcs.cd[ix,1],wcs.cd[iy,1]]]
  ELSE : BEGIN   ; 'PC' or 'CROTA' (fitshead2wcs creates pc matrix if latter)
         gswcs.cd=[[wcs.cdelt[ix]*wcs.pc[ix,0],wcs.cdelt[iy]*wcs.pc[iy,0]], $
                   [wcs.cdelt[ix]*wcs.pc[ix,1],wcs.cdelt[iy]*wcs.pc[iy,1]]]
         END
  ENDCASE

 ; convert from degrees to arcsec if cunit ne 'arcsec  ' (or 'ARCSEC  ')
 ; apply to cd, crval:
 CASE strtrim(strlowcase(wcs.cunit[ix]),2) OF
  'arcsec' : scal = 1d0
  'arcmin' : scal = 6d1
  'solrad' : scal = solrad*!radeg*3.6d3 / wcs.position.dsun_obs
  ELSE :     scal = 3.6d3         ; 'deg'
 ENDCASE
 gswcs.cd[0,*]  = gswcs.cd[0,*]  * scal
 gswcs.crval[0] = gswcs.crval[0] * scal
 CASE strtrim(strlowcase(wcs.cunit[iy]),2) OF
  'arcsec' : scal = 1d0
  'arcmin' : scal = 6d1
  'solrad' : scal = solrad*!radeg*3.6d3 / wcs.position.dsun_obs
  ELSE :     scal = 3.6d3         ; 'deg'
 ENDCASE
 gswcs.cd[1,*]  = gswcs.cd[1,*]  * scal
 gswcs.crval[1] = gswcs.crval[1] * scal

 ; corrected time
 gswcs.time.observ_date = wcs_get_time(wcs)
 IF TAG_EXIST(wcs.time,'exptime') THEN gswcs.time.exptime = wcs.time.exptime

 ; position in heliographic coordinate system
 gswcs.position.soho        = wcs.position.soho
 gswcs.position.pos_assumed = wcs.position.pos_assumed
 gswcs.position.dsun_obs    = wcs.position.dsun_obs
 gswcs.position.solar_b0    = wcs.position.solar_b0
 gswcs.position.carr_earth  = wcs.position.carr_earth
 IF TAG_EXIST(wcs.position,'hgln_obs') $
    THEN gswcs.position.hgln_obs = wcs.position.hgln_obs $
    ELSE BEGIN
       ; if soho, derive from .gse_obs, else set to 0.0 = earth
       IF TAG_EXIST(wcs.position,'gse_obs') THEN $
          gswcs.position.hgln_obs = (-1.8d2/!dpi) * wcs.position.gse_obs[1] $
                                                 / wcs.position.dsun_obs $
       ELSE gswcs.position.hgln_obs = 0.0
    ENDELSE
 IF TAG_EXIST(wcs.position,'hglt_obs') $
    THEN gswcs.position.hglt_obs = wcs.position.hglt_obs $
    ELSE gswcs.position.hglt_obs = wcs.position.solar_b0
    
ENDIF ELSE BEGIN

 ; create WCS structure based on keywords
 ; this is for a coordinate system with axis1 parallel to the ecliptic
 gswcs.coord_type = 'Helioprojective-Cartesian'
 gswcs.wcsname    = 'Helioprojective-Cartesian'
 gswcs.variation  = 'CD'
 gswcs.compliant  = 1B
 gswcs.projection = 'TAN'
 gswcs.naxis      = disp.naxis
 gswcs.ix         = 0L
 gswcs.iy         = 1L
 gswcs.crval      = [0.0,0.0]               ; suncenter
 gswcs.ctype      = ['HPLN-TAN','HPLT-TAN'] ; heliocentric cartesian
 gswcs.cname      = ['','']
 gswcs.cunit      = ['arcsec','arcsec']
 gswcs.simple     = 0B                      ; cdelt, roll_angle not present

 ; position in heliographic coordinate system
 gswcs.position.soho        = owcs.position.soho
 gswcs.position.pos_assumed = owcs.position.pos_assumed
 gswcs.position.dsun_obs    = owcs.position.dsun_obs
 gswcs.position.solar_b0    = owcs.position.solar_b0
 gswcs.position.carr_earth  = owcs.position.carr_earth
 IF TAG_EXIST(owcs.position,'hgln_obs') $
    THEN hgln = owcs.position.hgln_obs $
    ELSE BEGIN
       ; if soho, derive from .gse_obs, else set to 0.0 = earth
       IF TAG_EXIST(owcs.position,'gse_obs') THEN $
          hgln = (-1.8d2/!dpi) * owcs.position.gse_obs[1] $
                               / owcs.position.dsun_obs $
       ELSE hgln = 0.0
    ENDELSE
 gswcs.position.hgln_obs = hgln
 IF TAG_EXIST(owcs.position,'hglt_obs') $
    THEN gswcs.position.hglt_obs = owcs.position.hglt_obs $
    ELSE gswcs.position.hglt_obs = owcs.position.solar_b0

 ; time
 twcs = wcs_get_time(owcs)
 gswcs.time.observ_date = twcs

 ;
 ; now calculate crpix = suncenter pixels, and CD
 ;
 ; 1. Get the offset of the reference pixel from suncenter
 rof3 = sb_hg2hc(disp.rval[0:1],pos=gswcs.position, $
                     carr=disp.carr,/earth)                       ; unit vec
 rof3 = rof3 * disp.rval[2]                                       ; solRad
 dref_obs = owcs.position.dsun_obs - rof3[2]*solrad               ; dist
 ; adjust scale from pixel/solrad @ ref.pix to pixel/arcsec @ suncenter
 delt_1 = disp.delt_1 * dref_obs * !dpi / (1.8d2*3.6d3 * solrad)

 ; 2. Transf matrix from ecliptic-plane-N/pix to heliocentric/solRad.
 ;    p_a is position angle of solar N measured ccw from display up.
 ;    Sign is opposite to sign of CROTA, i.e., CROTA = -p_a
; p_a = sb_p4display(t=twcs,hgln=hgln) / !radeg
 p_a = sb_p4display(t=twcs,hgln=hgln, $
       owcs=gswcs,helio=helio,sc=sc,swcs=swcs) / !radeg
 pc  = [[cos(p_a),-sin(p_a)],[sin(p_a),cos(p_a)]]
 gswcs.cd  = pc / delt_1               ; CD matrix, use scale at suncenter

 ; 3. transform refpix from heliocentric to pixels
 rof2 = transpose(pc) # rof3[0:1]      ; rotation using inverse of pc
 rof2 = rof2 * disp.delt_1             ; scale with pix/solrad at refpix
 gswcs.crpix = disp.rpix - rof2 + 1    ; crpix rel to first pixel (=[1,1])

ENDELSE

; improve hgln_obs for SOHO if orbit database not available
sb_sohoposit,gswcs

RETURN,gswcs

END

