
;+
; NAME:
;        sb_dslbl
; PURPOSE:
;        derives appropriate dataset labels from the cdss common block
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        lbl = sb_dslbl(w.cdw)
; INPUTS:
;        cdw : number of image display for which to return ds labels
; KEYWORDS (INPUT):
; OUTPUTS:
;        returns vector of ds labels for given image display number
; KEYWORDS (OUTPUT):
; COMMON BLOCKS: sb_com
; SIDE EFFECTS:
; RESTRICTIONS:
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 7/12/2007
;-

function sb_dslbl,cdw

  common sb_com

  ss = ptr_valid(cdss[*,cdw].ptrds)
  maxds = n_elements(ss)
  bnam = strarr(maxds)+' '
  for i=0,maxds-1 do begin
     if ss[i] gt 0 then begin
        bna1 = cdss[i,cdw].detector
        if bna1 eq '' then bna1='OTHER'
        bna1 = bna1+cdss[i,cdw].wave_len+cdss[i,cdw].obsrvtry
     endif else bna1=' '
     bnam[i] = bna1
  endfor

  return,bnam

END
