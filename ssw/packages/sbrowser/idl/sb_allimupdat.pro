
pro sb_allimupdat,pixmap=pixmap0

;+
; $Id$
;
; Project : STEREO SECCHI
;
; Name    : sb_allimupdat
;
; Purpose : renews all images in all image display windows of go_secchi.
;           Typically called after the display time has been changed, e.g.,
;           moving the main slider in the top window.  Uses various
;           parameters in common block sb_com
;
; Use     : IDL> sb_allimupdat
;
; Inputs  :
;
; Outputs :
;
; Keywords:
;     /pixmap : write images first into movie pixmap, then copy to display
;
; Common  : sb_com
;
; Restrictions:
;    No error checking is done.
;
; Side effects:
;
; Category    : Image processing
;
; Prev. Hist. :
;
; Written     : Jean-Pierre Wuelser, LMSAL, 9-Oct-2006
;               JPW, added /pixmap , 14-Sep-2007
;               JPW, display pixmap if in movie mode, 14-Mar-2008
;
; $Log$
;-

common sb_com

maxdw = n_elements(cimd)
; set "pixmap" to create pixmaps
if keyword_set(pixmap0) and ptr_valid(cmov.pwv) then pixmap=1 else pixmap=0
; set "dispmov" to display pixmaps
if ~pixmap and ptr_valid(cmov.pwv) then dispmov=1 else dispmov=0

; loop through all possible display windows, check if each exists
for cdw = 0,maxdw-1 do $
if WIDGET_INFO(cimd[cdw].disp.wtop,/valid) then begin

 if dispmov then begin
    wset,cimd[cdw].disp.win
    device,copy=[0,0,cimd[cdw].disp.naxis[0],cimd[cdw].disp.naxis[1], $
                 0,0,(*cmov.pwv)[cmov.ct,cdw]]
 endif else begin
  ; for window cdw find datasets that are a) defined, b) selected for "show":
  ww = where(ptr_valid(cdss[*,cdw].ptrds) and cimd[cdw].col.show,nds)
  if nds ge 1 then begin
    ; create heap variable
    disp = cimd[cdw].disp
    pbcub = ptr_new(bytarr(disp.naxis[0],disp.naxis[1],nds))
    ; loop through dataset(s) to be shown:
    for i=0,nds-1 do begin
      cds = ww[i]
      col = cimd[cdw].col[cds]
      (*pbcub)[0,0,i] = sb_bytscl(sb_getimagecom(cdw,cds),col)
    endfor
    ; display the image
    if keyword_set(pixmap) then wset,(*cmov.pwv)[cmov.ct,cdw]  $
       else wset,cimd[cdw].disp.win
    tv,sb_byt2col(pbcub,cimd[cdw].col[ww]),true=3
    if keyword_set(pixmap) then begin
       ; add a date/time label
       xyouts,10,10,strmid(anytim((*cmov.ptv)[cmov.ct],/stime),0,20),/dev, $
              charsize=1.2
       ; copy pixmap to display
       wset,cimd[cdw].disp.win
       device,copy=[0,0,cimd[cdw].disp.naxis[0],cimd[cdw].disp.naxis[1], $
                    0,0,(*cmov.pwv)[cmov.ct,cdw]]
    endif
    ; free the heap variable
    ptr_free,pbcub
  endif
 endelse

endif

end
