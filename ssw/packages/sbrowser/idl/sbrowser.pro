
;+
; NAME:
;        sbrowser
; PURPOSE:
;        Image display for STEREO-SECCHI
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        sbrowser
; INPUTS:
; KEYWORDS (INPUT):
; OUTPUTS:
; KEYWORDS (OUTPUT):
; COMMON BLOCKS:
;        sb_com
; SIDE EFFECTS:
; RESTRICTIONS:
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 10/9/2006
;-

PRO sbrowser

  ;Don't allow two copies of go_sec_top to run at once
  IF xregistered("sb_top") THEN begin
    tmp = dialog_message( /error, [ 'Error in sbrowser:', ' ', $
                          'Only one instance at a time is allowed.' ])
    return
  endif

  ssw_path,/stereo,/soho,/trace
  
  sb_initcom                         ; initialize common block variables
  sb_top                             ; create main window

END
