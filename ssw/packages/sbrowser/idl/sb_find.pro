
;+
; NAME:
;        sb_find
; PURPOSE:
;        returns short index for files in time range
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        ii = sb_find(ts,te,ds)
; INPUTS:
;        ts = start time
;        te = end time
;        ds = structure element with (at least) the following tags:
;             .obsrvtry = string.  Should start with 'A' or 'B' for STEREO,
;                                  'S' for SOHO, 'H' for HINODE
;             .detector = string.  'EUVI','COR1','COR2','HI_1',HI_2',
;                                  'EIT','C2','C3','MDI-W','MDI-M',
;                                  'TRACE','XRT','SOT-F','SOT-M', or ''
;                         If .detector starts with 't', then TRACE is assumed.
;             .path = string.  if '' or 'lz': use default lz data path,
;                              if 'pb', 'rt', 'ql' use corresp. default path,
;                              otherwise .path is interpreted as full path.
; KEYWORDS (INPUT):
; OUTPUTS:
;        ff : vector of file names
; KEYWORDS (OUTPUT):
; COMMON BLOCKS:
; SIDE EFFECTS:
; RESTRICTIONS:
;
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 9/15/2006
;-

FUNCTION sb_find,ts0,te0,ds

; calls appropriate routines depending on instrument
; explicit code for user data

obs = strupcase(strmid(ds[0].obsrvtry,0,1))
det = strupcase(ds[0].detector)

ixs0 = sb_mkshortindex()           ; empty shortindex structure element
ion = ixs0

; SECCHI
if obs eq 'A' or obs eq 'B' then ion=sb_findsecchi(ts0,te0,ds)

; SOHO
if obs eq 'S' then ion=sb_findsoho(ts0,te0,ds)

; TRACE
if strmid(det,0,1) eq 'T' then ion=sb_findtrace(ts0,te0,ds)

; HINODE
if obs eq 'H' then ion=sb_findhinode(ts0,te0,ds)
   
; look in provided path for any other files
p   = ds[0].path
if ion[0].filename eq '' and p ne '' then begin
  ff=sb_filesearch(p,'*')
  if ff[0] ne '' then begin
    it0={naxis1:0L,naxis2:0L,date_obs:'',date_d$obs:'',time_d$obs:''}
    ii = sb_qmheadfits(ff,it0,timer=[anytim(ts0),anytim(te0)],outt=tt)
    ; check for valid time (weeds out non-FITS images etc.)
    ww = where(tt gt 0d0,nww)
    if nww gt 0 then begin
      ; populate output structure:
      ion = replicate(ixs0,nww)
      ion.naxis1 = ii[ww].naxis1
      ion.naxis2 = ii[ww].naxis2
      ion.tt = tt[ww]
      ion.filename = ff[ww]
    endif
  endif
endif

if ion[0].naxis1 eq 0L then ion=-1    ; expected by sbrowser for 0 files
return,ion

end
