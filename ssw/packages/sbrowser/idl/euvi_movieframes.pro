
;+
; $Id$
;
; Project : STEREO SECCHI
;
; Name    : euvi_movieframes
;
; Purpose : write time series of stereo images as JPEGs
;           - Default settings create anaglyph movie in 171
;           - /two creates side-by-side frames for Behind/Ahead
;
; Use     : IDL> euvi_movieframes,ts,te,outdir
;
; Inputs  :
;    ts = start time (anytim compatible format)
;    te = end time (anytim compatible format)
;    outdir = directory to put images (must exist!)
;
; Outputs :
;    No explicit outputs
;
; Keywords (input):
;    wave = vector of wavelengths to display in each panel.  Default=[171,171]
;    obs  = vector of spacecraft, same # of elem. as wave. Default=['a','b']
;           ignored if /twopanel set
;    emin = vector of minimum exposure times to be considered. Default=0
;    emax = vector of maximum exposure times to be considered. Default=1000
;    lut  = vector of color table numbers.  Default=[7,5] (=cyan/red)
;           (refers to color table numbers in file sb_colors1.tbl in !path)
;    max  = vector of DN/sec mapped to highest color table value.
;           Default: 3e2 for 284, 1e3 for all other wavelengths
;    min  = vector of DN/sec mapped to lowest color table value. Default=0.0
;    gam  = vector of gamma values to be applied to images.  Default=0.5
;    num  = starting value for numbering images.  Default=0
;    naxis: size of output image.  Default=[1024,768]
;    delt_1: scale of output image, in pixels per solar radius. Default=300.
;    rpix: pixel in output image to be used as reference pixel. Def=naxis/2.
;    rval: [longitude (deg), latitude (deg), radius (in sol.rad)]
;          Point on the sun to be mapped onto reference pixel. Def=[0.,0.,0.]
;    /carr: if set: interpret rval as Carrington coord.
;           else:   interpret rval as Stonyhearst coord.
;    /twopanel: two panels showing Behind | Ahead side by side instead of
;               anaglyph.  If set, same wave,lut, etc used for both panels
;    /ssr1: only consider synoptic images.
;
; Common  : None
;
; Restrictions:
;
; Side effects:
;    Creates images in outdir
;
; Category    :
;
; Prev. Hist. :
;
; Written     : Jean-Pierre Wuelser, LMSAL, 3-Jul-2007
;
; $Log$
;-

pro euvi_movieframes,ts,te,odir,wave=wave,obs=obs,emin=emin,emax=emax, $
         lut=lutn,max=cmax,min=cmin,gam=cgam,num=num0,twopanels=two, $
         naxis=naxis,delt_1=delt_1,rpix=rpix,rval=rval,carr=carr,ssr1=ssr1 , $
         bad=bad0,despike=despik,nosync=nosync

if n_elements(bad0) gt 0 then bad=anytim(bad0) else bad=0d0

if n_elements(odir) ne 1 then odir='~'
if n_elements(wave) eq 0 then if keyword_set(two) then wave=[171] $
                                                  else wave=[171,171]
nwav = n_elements(wave)
if n_elements(obs) ne nwav then begin
   if nwav eq 2 then obs=['a','b'] else obs=strarr(nwav)+'a'
endif
if n_elements(emin) ne nwav then emin=fltarr(nwav)
if n_elements(emax) ne nwav then emax=fltarr(nwav)+1e3
if n_elements(despik) ne nwav then despik=intarr(nwav)
if n_elements(lutn) ne nwav then begin
   if nwav eq 2 then lutn=[7,5] else lutn=intarr(nwav)
endif
if n_elements(num0) ne 1 then num0=0L

sc1 = intarr(nwav)         ; numerical observatory: Ahead=0, Behind=1
if keyword_set(two) then sc1=sc1+1 else begin
  w = where(strupcase(strmid(obs,0,1)) eq 'B',nw)
  if nw gt 0 then sc1[w]=1
endelse
sc2 = 1-sc1   ; number of "the other observatory"

; display and color structures
if keyword_set(two) $
  then disp = {naxis:[960L,768L],delt_1:300.,rpix:[480.,384.], $
               rval:[0.,0.,0.],carr:0} $
  else disp = {naxis:[1024L,768L],delt_1:300.,rpix:[512.,384.], $
               rval:[0.,0.,0.],carr:0}
if n_elements(naxis) eq 2 then disp.naxis=naxis
if n_elements(delt_1) eq 1 then disp.delt_1=delt_1
if n_elements(rpix) eq 2 then disp.rpix=rpix else disp.rpix=disp.naxis/2.0
if n_elements(rval) ge 2 then begin
   if n_elements(rval) eq 3 then disp.rval=rval $
      else disp.rval = [rval[0:1],1.0]
endif
if keyword_set(carr) then disp.carr=1

col0 = {show:1,min:0.0,max:1e3,gam:0.5,lut:bytarr(256,3)}
cvec = replicate(col0,nwav)
nw = n_elements(cmin)
if nw eq nwav then cvec.min=cmin
if nw eq 1 then cvec.min=cmin[0]
nw = n_elements(cmax)
if nw eq nwav then cvec.max=cmax
if nw eq 1 then cvec.max=cmax[0]
if nw eq 0 then begin
  w=where(wave eq 284,nw)
  if nw gt 0 then cvec[w].max=3e2
endif
nw = n_elements(cgam)
if nw eq nwav then cvec.gam=cgam
if nw eq 1 then cvec.gam=cgam[0]

; find appropriate image files
iia = scc_read_summary(date=[ts,te],spacecr='a',tel='euvi',type='img',/nobeac)
iib = scc_read_summary(date=[ts,te],spacecr='b',tel='euvi',type='img',/nobeac)

siia = size(iia)
siib = size(iib)
if siia[siia[0]+1] eq 8 && siib[siib[0]+1] eq 8 $
   then nna = n_elements(iia) else nna = 0L

; SSR1 only if /ssr1 set
if keyword_set(ssr1) && nna gt 0 then begin
   wwa = where(iia.dest eq 'SSR1',nna)
   wwb = where(iib.dest eq 'SSR1',nnb)
   if nna gt 0 && nnb gt 0 then begin
     iia = iia[wwa]
     iib = iib[wwb]
   endif else nna=0L
endif

; find matching A/B pairs only
if nna gt 0 then begin

  ; time vector in seconds from filename (date_obs lacks light travel time corr)
  tta = file2time(iia.filename,out_style='sec')
  ttb = file2time(iib.filename,out_style='sec')

  wwa = lindgen(nna)
  wwb = lonarr(nna)-1L
  for k=0L,nna-1 do begin
    if keyword_set(nosync) $
      then w=where(abs(ttb-tta[k]) le 6d1 and iib.value eq iia[k].value,nw) $
      else w=where(abs(ttb-tta[k]) le 1d0,nw)
    if nw gt 0 then if iia[k].nmiss le 0 and iib[w[0]].nmiss le 0 and $
       min(abs(tta[k]-bad)) gt 1d0 and $                 ; (not bad frame?)
       iia[k].value eq iib[w[0]].value then wwb[k]=w[0]
  endfor
  w = where(wwb ge 0L,nna)
  if nna gt 0 then begin
    wwa = wwa[w]
    wwb = wwb[w]
    iia = iia[wwa]
    iib = iib[wwb]
    tta = tta[wwa]
    ttb = ttb[wwb]
  endif
endif
if nna eq 0 then begin
  print,'No co-temporal files for A and B found'
  return
endif

; full path
path = scc_data_path('a',tel='euvi',type='img')
ffa = iia.filename
ffa = concat_dir(path,concat_dir(strmid(ffa,0,8),ffa))
path = scc_data_path('b',tel='euvi',type='img')
ffb = iib.filename
ffb = concat_dir(path,concat_dir(strmid(ffb,0,8),ffb))

ii = [[iia],[iib]]
ff = [[ffa],[ffb]]
tt = [[tta],[ttb]]

; get color tables
flut = file_which('.:'+!path,'sb_colors1.tbl')  ; look in current path
n_tab = 0B
openr,lun,flut,/get_lun
readu,lun,n_tab                           ; read number of luts in file
lutk = bytarr(256,3,n_tab)
readu,lun,lutk
free_lun,lun
for k=0,nwav-1 do cvec[k].lut=lutk[*,*,lutn[k]]

; first type of images
w = where(fix(ii[*,sc1[0]].value) eq wave[0] $
          and ii[*,sc1[0]].exptime ge emin[0] $
          and ii[*,sc1[0]].exptime le emax[0],nfram)
if nfram le 0 then begin
  print,'No co-temporal files with proper wavelength found'
  return
endif

; find temporally closest images of other wavelengths and/or S/C
wn = lonarr(nfram,nwav)
wn[*,0] = w

for k=1,nwav-1 do begin
   w = where(fix(ii[*,sc1[k]].value) eq wave[k] $
          and ii[*,sc1[k]].exptime ge emin[k] $
          and ii[*,sc1[k]].exptime le emax[k],kfram)
   if kfram le 0 then begin
     print,'No co-temporal files with proper wavelength found'
     return
   endif
   ttw = tt[w,sc1[k]]
   ww = 0L
   for j=0L,nfram-1 do begin
     tref = tt[wn[j,0],sc1[0]]
     dmy = min(abs(ttw-tref),ww)
     wn[j,k] = w[ww]
   endfor
endfor

; create heap variable for images for use in sb_byt2col
if keyword_set(two) $
  then pbcub = ptr_new(bytarr(disp.naxis[0]*2L,disp.naxis[1],nwav)) $
  else pbcub = ptr_new(bytarr(disp.naxis[0],disp.naxis[1],nwav))

; Z buffer for plotting time
set_plot,'z'
device,set_resol=[200,30]
!p.charsize=1.1
!p.charthick=1

; get full file
;loop through movie frames
for j=0L,nfram-1 do begin
  ; time label
  erase
  xyouts,10,10,strmid(anytim(tt[wn[j,0],sc1[0]],/stime),0,20),/dev
  label=tvrd()
  for k=0,nwav-1 do begin
    swcs = fitshead2wcs(headfits(ff[wn[j,k],sc2[k]]))
    o = sb_getimage(ff[wn[j,k],sc1[k]],disp,/sc,swcs=swcs,despike=despik[k])
    (*pbcub)[0,0,k] = sb_bytscl(o,cvec[k])
    (*pbcub)[0,0,k] = label
    if keyword_set(two) then begin
      swcs2 = fitshead2wcs(headfits(ff[wn[j,k],sc1[k]]))
      o = sb_getimage(ff[wn[j,k],sc2[k]],disp,/sc,swcs=swcs2,despike=despik[k])
      (*pbcub)[disp.naxis[0],0,k] = sb_bytscl(o,cvec[k])
      (*pbcub)[disp.naxis[0],0,k] = label
    endif
  endfor
  o = sb_byt2col(pbcub,cvec)

  write_jpeg,concat_dir(odir,'frame'+strtrim(j+num0,2)+'.jpg'),o, $
             true=3,quality=95
  print,j
endfor

; free the heap variable
ptr_free,pbcub

end



  
