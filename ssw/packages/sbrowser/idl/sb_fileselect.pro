
function sb_fileselect,ts,te,ds

;+
; $Id$
;
; Project : STEREO SECCHI
;
; Name    : sb_fileselect
;
; Purpose : returns subscripts of FITS imnages that match selection criteria
;
; Use     : IDL> ss = sb_fileselect(ts,te,ds)
;
; Inputs  :
;    ts = start time.  anytim compatible format
;    te = end time.    antyim compatible format
;    ds = structure (element).  Must have at least the following tags:
;         .obsrvtry = string: 'S', 'A', 'B', 'H', or '' only.
;         .detector = string, e.g., 'EIT','C2','C3','EUVI','COR1','COR2',
;                             'TRACE','MDI-M','MDI-W','XRT','SOT','SOT-M',''
;                             '' means any detector/obsrvtry combination
;                             'MDI-x' currently just means detector='MDI'
;                             (future: 'MDI-M' -> detector='MDI',wave_len='M')
;                             'SOT' means 'FG' or I of 'FGIV'
;                             'SOT-M' means V of 'FGIV'
;         .wave_len = string. '' means any wave_len
;         .nx1min   = int.   minimum number of naxis1 pixels
;         .expmin   = float. minimum exp.time
;         .ptrds    = ptr.  points to vector of structures, 1 elem per image.
;                           must have /shortindex tags from sb_readfits.
; Outputs :
;    ss = subscript vector of images that meet selection criteria.
; Keywords:
;
; Common  : None
;
; Restrictions: This routine assumes that the string tags in the shortindex
;               structure (from sb_readfits) are all uppercase strings.
; Side effects:
;
; Category    : Image processing
;
; Prev. Hist. :
;
; Written     : Jean-Pierre Wuelser, LMSAL, 19-Sep-2006
;
; $Log$
;-

if ptr_valid(ds.ptrds) then begin
   detector = strupcase(ds.detector)
   wave_len = strupcase(strcompress(ds.wave_len,/remove_all))
   obsrvtry = strupcase(ds.obsrvtry)
   nff = n_elements(*ds.ptrds)
   if nff gt 0 then begin
      tt = (*ds.ptrds).tt
      ss = where(tt ge anytim(ts) and tt le anytim(te))
   endif else ss=[-1L]
   ; check detector and obsrvtry:
   if ss[0] ge 0 and detector ne '' then begin
      if strmid(detector,0,3) eq 'MDI' then detector='MDI'  ; 'MDI-M'->'MDI'
      if detector eq 'SOT' then detector='FG'
      if detector eq 'SOT-M' then detector='FGIV'
      obs = strupcase((*ds.ptrds)[ss].obsrvtry)
      det = strupcase((*ds.ptrds)[ss].detector)
      sss = where(obs eq obsrvtry and strpos(det,detector) ge 0)
      if sss[0] ge 0 then ss = ss[sss] else ss=[-1L]
   endif
   ; check wave_len
   if ss[0] ge 0 and wave_len ne '' then begin
      wav = strupcase(strcompress((*ds.ptrds)[ss].wave_len,/remove_all))
      sss = where(strpos(wav,wave_len) ge 0)
      if sss[0] ge 0 then ss = ss[sss] else ss=[-1L]
   endif
   ; check nx1min, expmin
   if ss[0] ge 0 then begin
      nx1  = (*ds.ptrds)[ss].naxis1
      exp = (*ds.ptrds)[ss].exptime
      sss = where(nx1 ge ds.nx1min and exp ge ds.expmin)
      if sss[0] ge 0 then ss = ss[sss] else ss=[-1L]
   endif
   if ss[0] ge 0 then ss = ss[sort(tt[ss])]

endif else ss=[-1L]

return,ss

end
