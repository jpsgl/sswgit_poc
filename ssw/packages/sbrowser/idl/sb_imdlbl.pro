
;+
; NAME:
;        sb_imdlbl
; PURPOSE:
;        sets or updates dataset related labels in the sb_imd widget
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        sb_imdlbl,w,cds
; INPUTS:
;        w : user variable of sb_imd
; KEYWORDS (INPUT):
; OUTPUTS:
; KEYWORDS (OUTPUT):
; COMMON BLOCKS: sb_com
; SIDE EFFECTS:
; RESTRICTIONS:
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 9/27/2006
;-

pro sb_imdlbl,w

  common sb_com

  bnam = sb_dslbl(w.cdw)

  maxds = n_elements(bnam)
  onam = strarr(maxds)+' '
  for i=0,maxds-1 do begin
     if bnam[i] ne ' ' then begin    ; means it's a valid ds
        ona1 = cdss[i,w.cdw].obsrvtry
        if ona1 eq '' or ona1 eq 'H' then ona1='E'
        onam[i] = ona1
     endif
     WIDGET_CONTROL,w.wnam[i],SET_VAL=bnam[i]
  endfor
  WIDGET_CONTROL,w.wods,SET_VAL=onam

END
