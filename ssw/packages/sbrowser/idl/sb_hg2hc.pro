
;+
; NAME:
;        sb_hg2hc
; PURPOSE:
;        transforms heliographic or carrington coord. to heliocentric coord.
; CATEGORY:
;        image processing
; CALLING SEQUENCE:
;        hc = sb_hg2hc(hg,pos=ref_wcs.position)
; INPUTS:
;        hg = fltarr(2). heliographic (observer centered!) [lon,lat]
;                        can also be carrington coord. or Stonyhurst
;                        (earth centered) heliogr. (use /carr, or /earth)
; KEYWORDS (INPUT):
;        pos = wcs.position  Position substructure of WCS for observer
;        Notes: pos keyword is required and must have solar_b0 tag,
;               carr_earth tag (if /carr), hgln_obs tag (if /carr or /earth)
;        /carr  : input is carrington coord.  Overrides /earth
;        /earth : input is Stonyhurst (earth centered) heliogr. coord.
; OUTPUTS:
;        hc : fltarr(3). heliocentric coordinates in solRad.
; KEYWORDS (OUTPUT):
; COMMON BLOCKS:
; SIDE EFFECTS:
; RESTRICTIONS:
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 4/13/2006
;        JPW, 10/26/2007 vectorized
;-

FUNCTION sb_hg2hc,hg0,pos=pos,carr=carr,earth=earth

hg = hg0
nhg = size(hg)
if nhg[0] ge 2 then nhg=nhg[2] else nhg=1

if keyword_set(earth) THEN hg[0,*] = hg[0,*]-pos.hgln_obs

; carr can be vector of length nhg:
if n_elements(carr) gt 0 then begin
  if n_elements(carr) eq nhg then begin
     ww = where(carr ne 0,nww)
     if nww gt 0 then hg[0,ww]=hg0[0,ww]-pos.carr_earth-pos.hgln_obs
  endif else if carr[0] ne 0 then hg[0,*]=hg0[0,*]-pos.carr_earth-pos.hgln_obs
endif

;old version for earth,carr (delete after testing)
;IF keyword_set(carr) THEN hg[0,*] = hg[0,*]-pos.carr_earth-pos.hgln_obs $
; ELSE IF keyword_set(earth) THEN hg[0,*] = hg[0,*]-pos.hgln_obs

hg = hg / !radeg
b0 = pos.solar_b0 / !radeg

hc = fltarr(3,nhg)
hc[0,*] = sin(hg[0,*])*cos(hg[1,*])
hc[1,*] = sin(hg[1,*])
hc[2,*] = cos(hg[0,*])*cos(hg[1,*])
hc0   = hc
hc[1,*] = hc0[1,*]*cos(b0) - hc0[2,*]*sin(b0)
hc[2,*] = hc0[1,*]*sin(b0) + hc0[2,*]*cos(b0)

RETURN, hc

END
