
pro sb_updatcmov,tdel=tdel,toff=toff,dwn=dwn,dsn=dsn,spd=spd

;+
; $Id$
;
; Project : STEREO SECCHI
;
; Name    : sb_updatcmov
;
; Purpose : update the common block variable cmov based on keywords
;           and/or other common block values
;
; Use     : IDL> sb_updatcmov
;
; Inputs  :
;
; Outputs : None
; Keywords:
;   tdel = dbl.  if set and non-zero, use equally spaced movie frame times
;                with interval tdel in seconds.  Otherwise use dwn,dsn
;   toff = dbl.  offset of first frame from ctop.ts in seconds (if tdel gt 0.)
;   dwn  = int.  display window number
;   dsn  = int.  dataset number
;     Notes:
;     - A supplied keyword modifies the corresponding value in cmov
;     - *cmov.ptv is then updated based on those values
;   spd  = int.  Speed of movie in movie mode. between 0 and 100.
; Common  : sb_com
;
; Restrictions:
;
; Side effects: modifies the contents of the common block variable cmov
;
; Category    : image processing, widgets
;
; Prev. Hist. :
;
; Written     : Jean-Pierre Wuelser, LMSAL, 21-Sep-2006
;
; $Log$
;-

common sb_com

siz = size (cdss)

if n_elements(tdel) eq 1 then cmov.tdel = tdel > 0d
if n_elements(toff) eq 1 then cmov.toff= toff > 0d
if n_elements(dwn) eq 1 then cmov.dwn = 0 > dwn < (siz[2]-1)
if n_elements(dsn) eq 1 then cmov.dsn = 0 > dsn < (siz[1]-1)
if n_elements(spd) eq 1 then cmov.spd = 0 > spd < 100

; save current time, then reset time vector
ctim = (*cmov.ptv)[cmov.ct]     ; current time
*cmov.ptv = [0d0]               ; reset time vector

; use dataset times if possible
if cmov.tdel eq 0d0 then begin
   if ptr_valid(cdss[cmov.dsn,cmov.dwn].ptrds) then begin
      tt = (*cdss[cmov.dsn,cmov.dwn].ptrds).tt   ; time vector of ds
      ss = where(tt ge ctop.ts and tt le ctop.te)
      if ss[0] ge 0 then *cmov.ptv = tt[ss]
   endif
endif

; use equally spaced times if time vector not yet defined
if (*cmov.ptv)[0] eq 0. then begin
   tde2 = cmov.tdel
   tof2 = cmov.toff < (ctop.te-ctop.ts)
   if tde2 gt 0d then nn=long((ctop.te-ctop.ts-tof2)/tde2+1d) > 1L $
                 else nn=1L
   *cmov.ptv = dindgen(nn) * tde2 + ctop.ts + tof2
endif

; set current time index to time closest to old current time
dmy = min(abs(*cmov.ptv - ctim),ss)
cmov.ct = ss

end
