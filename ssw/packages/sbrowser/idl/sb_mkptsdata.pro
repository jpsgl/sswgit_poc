
;+
; NAME:
;        sb_mkptsdata
; PURPOSE:
;        defines or appends stucture used for tie point data in sbrowser
;        Image display for STEREO-SECCHI
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        sb_mkptsdata,pts
; INPUTS:
;        if pts exists, pts will be expanded by one element
; KEYWORDS (INPUT):
; OUTPUTS:
;        pts : vector of tie point data structure.  1 element if new,
;              else one element is added to existing vector.
;              Structure has the following tags:
;    - numf:0L        : feature# (numf lt 0: static feature, apply to all times)
;    - nump:0L        : spatial_point_within_feature#
;    - numt:0L        : mov.ct (being updated as needed when cmov.ptv changes)
;                       point with neg. numt is not displayed
;    - tt:0d          : time (cmov.ptv[cmov.ct])
;    - rval:fltarr(3) : 3D coord. (lon, lat, rad)
;    - carr:0         : rval is carrington if carr=1 else stonyhurst
;    - col:bytarr(3)  : color info (RGB) (re-interpreted for anaglyph)
;    - psym:0         : plot symbol,0 or negative if connected w/lines
;    - xy:fltarr(2)   : image x/y-coord (temporary)
;    - dwn:0          : data window # associated with x,y
;      Note: x/y, dwn : used to temporarily store clicked pixel from 1st view
;    - stat:0         : status/validity/type of data point:
;                   0 : invalid / undefined data point
;                   1 : valid first view data x/y @ dwn, rval NOT valid
;                   2 : valid rval (3D data), x/y (first view data) not valid
;                   3 : valid rval (3D data), and valid x/y (first view) data
; KEYWORDS (OUTPUT):
; COMMON BLOCKS:
; SIDE EFFECTS:
; RESTRICTIONS:
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 12/08/2007
;-

pro sb_mkptsdata,pts

pts0 = {numf:0L,nump:0L,numt:0L,tt:0d,rval:fltarr(3),carr:0, $
        col:bytarr(3),psym:0,xy:fltarr(2),dwn:0,stat:0}

if n_elements(pts) eq 0 then pts=pts0 else pts=[pts,pts0]

end
