
;+
; NAME:
;        sb_dss
; PURPOSE:
;        creation and event handling of ds selection widget for go_secchi
;        Image display for STEREO-SECCHI
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        sb_dss,cds,cdw,leader=ev.top
; INPUTS:
;        cds = current DS #
;        cdw = current display window #
;            -> will update cdss[cds,cdw]
; KEYWORDS (INPUT):
;        leader = top level base widget ID of calling widget
; OUTPUTS:
; KEYWORDS (OUTPUT):
; COMMON BLOCKS:
;        sb_com
; SIDE EFFECTS:
; RESTRICTIONS:
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 8/26/2005
;-


PRO sb_dss_event, ev

  COMMON sb_com
  WIDGET_CONTROL, ev.top, GET_UVALUE=udss
  WIDGET_CONTROL, ev.id,  GET_UVALUE=uval
  CASE uval OF
    'btt':        CASE ev.value OF
       0 : BEGIN   ; SEARCH
           WIDGET_CONTROL,/hourglass                  ; could take a while
           ptr_free,udss.ds.ptrds                     ; free old header vector
           sb_dssbut,udss                             ; widget state->udss.ds
           ; search for files
          ; ff = sb_findfiles(ctop.ts,ctop.te,udss.ds) ; search for files
          ; sb_readfits,ff,ii,/shortindex              ; read header info
           ii = sb_find(ctop.ts,ctop.te,udss.ds)      ; search for files
           sii = size(ii)
           IF sii[sii[0]+1] eq 8 THEN BEGIN
              ss = where(ii.tt ge ctop.ts and ii.tt le ctop.te)
              IF ss[0] ge 0 THEN udss.ds.ptrds = ptr_new(ii[ss])
           ENDIF
           udss.search = 0                            ; reset flag
           ; subselect files according to buttons
           *udss.sptr = sb_fileselect(ctop.ts,ctop.te,udss.ds)
           WIDGET_CONTROL,ev.top,SET_UVAL=udss        ; update uval

           ; update message and goesplot
           IF ptr_valid(udss.ds.ptrds) THEN nff = n_elements(*udss.ds.ptrds) $
                                       ELSE nff = 0L
           IF (*udss.sptr)[0] ge 0 THEN nss=n_elements(*udss.sptr) ELSE nss=0L
           txtv = string(nff)+' Images found,'+string(nss)+' selected'
           WIDGET_CONTROL,udss.txt,SET_VAL=txtv
           IF nss gt 0 THEN tt = (*udss.ds.ptrds)[*udss.sptr].tt
           sb_plotgoes,ctop.ts,ctop.te,win=ctop.goesw, $
                       pixm=ctop.goespm,ticks=tt
           END
       1 : BEGIN
           sb_dssexit,udss,/cancel
           IF udss.leader ne -1L THEN $
              WIDGET_CONTROL,udss.leader,SENSITIVE=1    ; top widget on
           WIDGET_CONTROL, ev.TOP, /DESTROY
           END
       2 : BEGIN
           sb_dssexit,udss
           IF udss.leader ne -1L THEN $
              WIDGET_CONTROL,udss.leader,SENSITIVE=1    ; top widget on
           WIDGET_CONTROL, ev.TOP, /DESTROY
           END
       ELSE:
       ENDCASE

    'bti': BEGIN
           udss.search = 1  ; set search needed flag since inst has changed
           WIDGET_CONTROL,ev.top,SET_UVAL=udss             ; update udss
           ; set message string
           txtv = ' click Search to update'
           WIDGET_CONTROL,udss.txt,SET_VAL=txtv
           END

    'txp': BEGIN
           udss.search = 1  ; set search needed flag since path has changed
           WIDGET_CONTROL,ev.top,SET_UVAL=udss             ; update udss
           ; set message string
           txtv = ' click Search to update'
           WIDGET_CONTROL,udss.txt,SET_VAL=txtv
           END

    'btb': BEGIN
           WIDGET_CONTROL,ev.id,GET_VALUE=bnum
           WIDGET_CONTROL,udss.txb,SENSITIVE=bnum[0]
           END

    'txb': 

    ELSE:  BEGIN
           IF udss.search eq 0 THEN BEGIN
              sb_dssbut,udss                           ; widget state->udss.ds
              *udss.sptr = sb_fileselect(ctop.ts,ctop.te,udss.ds)
              WIDGET_CONTROL,ev.top,SET_UVAL=udss      ; update uval
              ; update message
              IF ptr_valid(udss.ds.ptrds) $
                 THEN nff = n_elements(*udss.ds.ptrds) ELSE nff = 0L
              IF (*udss.sptr)[0] ge 0 $
                 THEN nss=n_elements(*udss.sptr) ELSE nss=0L
              txtv = string(nff)+' Images found,'+string(nss)+' selected'
              ; modify tick marks in goesplot:
              IF nss gt 0 THEN tt = (*udss.ds.ptrds)[*udss.sptr].tt
              sb_plotgoes,ctop.ts,ctop.te,win=ctop.goesw, $
                          pixm=ctop.goespm,ticks=tt
           ENDIF ELSE txtv = ' click Search to update'
           WIDGET_CONTROL,udss.txt,SET_VAL=txtv         ; update message
           END
  ENDCASE
END


PRO sb_dss,cds,cdw,leader=leader

  COMMON sb_com

  obs = ['A','A','A','A','A','B','B','B','B','B', $
         'S','S','S','S','S','','H','H','H','']
  det = ['EUVI','COR1','COR2','HI_1','HI_2', $
         'EUVI','COR1','COR2','HI_1','HI_2', $
         'EIT','C2','C3','MDI-M','MDI-W', $
         'TRACE','XRT','SOT','SOT-M','']
  bttt = ['Search','Cancel',' Done ']
  btti = det+' '+obs+strmid('      ',0,6-strlen(det)-strlen(obs))
  btti[n_elements(btti)-1] = 'OTHER  '
  bttr = ['Full Resolution','Any Resolution']
  bttb = ['Subtract Background']

  ds = cdss[cds,cdw]

  w = {cds:cds,cdw:cdw,ds:ds,txt:0L,bti:0L,txp:0L,txc:0L,txe:0L,btr:0L,txs:0L, $
       btb:0L,txb:0L,sptr:ptr_new(-1L),search:1,obs:obs,det:det,leader:-1L}

  IF n_elements(leader) eq 1 THEN IF leader ne -1L THEN BEGIN
     WIDGET_CONTROL,leader,SENSITIVE=0        ; desensitize calling widget
     w.leader=leader                          ; unless ID=-1 (shouldn't be)
  ENDIF

  inum = where(w.ds.obsrvtry eq obs and w.ds.detector eq det)
  IF ptr_valid(w.ds.ptrds) THEN BEGIN
     ii = *w.ds.ptrds
     tt = ii.tt
     txtv = string(n_elements(tt))+' Images selected'
     *w.sptr = lindgen(n_elements(ii))
     ; create new pointer to ii (the old points to the original cdss data):
     w.ds.ptrds = ptr_new(ii,/no_copy)
  ENDIF ELSE BEGIN
     txtv = ' click Search to update'
     w.ds.ptrds = ptr_new()
  ENDELSE

  base  = WIDGET_BASE(/COLUMN, GROUP_LEADER=leader,XOFFSET=60,YOFFSET=400, $
          TITLE='Define Data '+strtrim(cds,2)+' for Display '+strtrim(cdw,2))
  bas1  = WIDGET_BASE(base, /ROW)
  bdmy  = CW_BGROUP(bas1, bttt, /ROW, UVAL='btt')
  txtv = txtv+strmid('                                 ',0,47-strlen(txtv))
  w.txt = WIDGET_LABEL(bas1, VALUE=txtv)
;  w.txt = WIDGET_TEXT(bas1, VALUE=txtv,XSIZE=47)
  w.bti = CW_BGROUP(base,btti,ROW=4,UVAL='bti',/EXCLUSIVE,/NO_REL, $
                    SET_VAL=inum)
  bas2  = WIDGET_BASE(base, /ROW, /FRAME)
  tdmy  = WIDGET_LABEL(bas2, VALUE='Path:')
  w.txp = WIDGET_TEXT(bas2, VALUE=w.ds.path, /EDITABLE, UVAL='txp', XSIZE=64)
  bas3  = WIDGET_BASE(base, /ROW)
  tdmy  = WIDGET_LABEL(bas3, VALUE='Wavelength:')
  w.txc = WIDGET_TEXT(bas3, VALUE=w.ds.wave_len,/EDITABLE,UVAL='txc',XSIZE=24)
  tdmy  = WIDGET_LABEL(bas3, VALUE='    Min Exp Time (s):')
  w.txe = WIDGET_TEXT(bas3, VALUE=string(w.ds.expmin,f='(f8.3)'), /EDITABLE, $
                      XSIZE=8,UVAL='txe')
  bas4  = WIDGET_BASE(base, /ROW)
  w.btr = CW_BGROUP(bas4, bttr, /ROW, UVAL='btr',/EXCLUSIVE,/NO_REL, $
                      SET_VAL=1) ; (w.ds.summax gt 1))
  tdmy  = WIDGET_LABEL(bas4, VALUE='      E-W pixels: Min:')
  w.txs = WIDGET_TEXT(bas4, VALUE=string(w.ds.nx1min,f='(i8)'), /EDITABLE, $
                      XSIZE=8,UVAL='txs')
  bas5  = WIDGET_BASE(base, /ROW, /FRAME)
  w.btb = CW_BGROUP(bas5,bttb,/ROW,UVAL='btb',/NONEXCLUSIVE, $
                      SET_VAL=[(w.ds.backds ne '')])
  tdmy  = WIDGET_LABEL(bas5, VALUE='File: ')
  w.txb = WIDGET_TEXT(bas5, VALUE=w.ds.backds,/EDITABLE,XSIZE=38, $
                      SENSITIVE=(w.ds.backds ne ''),UVAL='txb')

  WIDGET_CONTROL, base, SET_UVAL=w
  WIDGET_CONTROL, base, /REALIZE
  ; update GOES plot: first upate the pixmap, then plot the tick marks
  sb_plotgoes,ctop.ts,ctop.te,win=ctop.goesw,pixm=ctop.goespm,/update
  IF n_elements(tt) gt 0 then $
     sb_plotgoes,ctop.ts,ctop.te,win=ctop.goesw,pixm=ctop.goespm,ticks=tt
  XMANAGER, 'sb_dss', base ,NO_BLOCK=1

END
