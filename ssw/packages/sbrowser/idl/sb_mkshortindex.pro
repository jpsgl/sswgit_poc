
;+
; NAME:
;        sb_mkshortindex
; PURPOSE:
;        defines short index stucture used for file search in sbrowser
;        Image display for STEREO-SECCHI
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        ixs = sb_mkshortindex()
; INPUTS:
; KEYWORDS (INPUT):
; OUTPUTS:
;        ixs = structure element.  Short index with some main data properties
; KEYWORDS (OUTPUT):
; COMMON BLOCKS:
;        sb_com
; SIDE EFFECTS:
; RESTRICTIONS:
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 6/12/2007
;-

function sb_mkshortindex

ixs = {naxis1:0L,naxis2:0L,summed:0,tt:0d,exptime:0., $
       obsrvtry:'',detector:'',wave_len:'',filename:''}

return,ixs

END
