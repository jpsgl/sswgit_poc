
;+
; NAME:
;        sb_plot1feature
; PURPOSE:
;        draws one "feature" onto window win.
;        Image display for STEREO-SECCHI
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        sb_plot1feature,rvec,wcs
; INPUTS:
;        rvec = fltarr(3,n) : n data points, for each [lon,lat,rad)
;        wcs = world coordinate structure for plot area
; KEYWORDS (INPUT):
;        /carr : longitude is Carrington.  Else Earth centered Stonyhurst
;        col  = bytarr(3) : color of feature
;        psym = int.  Plot symbol, 0 or negative if connected with lines
; OUTPUTS:
; KEYWORDS (OUTPUT):
; COMMON BLOCKS:
;        None
; SIDE EFFECTS:
; RESTRICTIONS:
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 11/05/2007
;-

pro sb_plot1feature,rvec,wcs,carr=carr,col=col0,psym=psym

   if n_elements(psym) ne 1 then psym=0
   if n_elements(col0) ne 3 then col=256L*256L*256L-1L $
      else col=col0[2]*256L*256L+col0[1]*256L+col0[0]

   ; calculate plot coordinates
   pix = sb_3d2pix(rvec,wcs,carr=carr,vis=vis)
   npx = n_elements(vis)
   if npx lt 1 then return
   pix = fix(pix+0.5)

   ; plot symbols and line segments separately:
   ; - plot all visible points
   if psym ne 0 then begin
      w = where(vis,nw)
      if nw gt 0 then plots,pix[*,w],/device,psym=abs(psym),col=col
   endif

   ; - plot visible line segments, including ones with a hidden endpoint
   ; find segments (only if to connect points by lines)
   if npx ge 2 and psym le 0 then begin
      v2 = vis
      if npx ge 3 then begin
         ; ignore single hidden points
         w = where(vis[0:npx-3] eq 1 and vis[2:npx-1] eq 1 and $
                   vis[1:npx-2] eq 0,nw)
         if nw gt 0 then v2[w]=1
      endif
      ; detect segment start and end points:
      ws = where([0,v2[0:npx-2]] eq 0 and v2 eq 1,ns)
      we = where(v2 eq 1 and [v2[1:npx-1],0] eq 0)
      ; include nearest hidden point:
      ws = (ws-1L) > 0L
      we = (we+1L) < (npx-1L)
      ; plot all segment lines
      for j=0L,ns-1 do plots,pix[*,ws[j]:we[j]],/device,col=col
   endif

end
