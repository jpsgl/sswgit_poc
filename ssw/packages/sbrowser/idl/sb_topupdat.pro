
pro sb_topupdat,noredraw=noredraw

;+
; $Id$
;
; Project : STEREO SECCHI
;
; Name    : sb_topupdat
;
; Purpose : update time slider, time string, and goesplot in the sb_top widget
;
; Use     : IDL> sb_topupdat
;
; Inputs  :
;
; Outputs : None
;
; Keywords:
;           /noredraw : do not redraw goesplot, only update cursor
;
;
; Common  : sb_com
;
; Restrictions:
;
; Side effects: modifies the contents of the common block variable cmov
;
; Category    : image processing, widgets
;
; Prev. Hist. :
;
; Written     : Jean-Pierre Wuelser, LMSAL, 27-Sep-2006
;
; $Log$
;-

common sb_com

      ; update time slider
      tcur = (*cmov.ptv)[cmov.ct]
      cval = 0 > fix(1d3*(tcur-ctop.ts)/(ctop.te-ctop.ts)) < 1000
      WIDGET_CONTROL,ctop.wsld1,SET_VALUE=cval
      ; update goes plot
      if keyword_set(noredraw) then begin
         sb_plotgoes,ctop.ts,ctop.te,win=ctop.goesw,pixm=ctop.goespm, $
                     cursor=tcur
      endif else begin
         sb_plotgoes,ctop.ts,ctop.te,win=ctop.goesw,pixm=ctop.goespm, $
                     cursor=tcur,ticks=*cmov.ptv,/update
      endelse
      ; rewrite time label
      tcur = strmid(anytim(tcur,/STIME),0,20)
      WIDGET_CONTROL,ctop.wtxt1,SET_VALUE=tcur

end
