
pro sb_mkmovie

;+
; $Id$
;
; Project : STEREO SECCHI
;
; Name    : sb_mkmovie
;
; Purpose : create movie frame pixwins for sbrowser
;
; Use     : IDL> sb_mkmovie
;
; Inputs  :
;
; Outputs :
;
; Keywords:
;
; Common  : sb_com
;
; Restrictions:
;    No error checking is done.
;
; Side effects:
;
; Category    : Image processing
;
; Prev. Hist. :
;
; Written     : Jean-Pierre Wuelser, LMSAL, 14-Sep-2007
;
; $Log$
;-

COMMON sb_com

; loops through all times
; creates pixmaps
; degrades to fewer times if running out of memory
; - not clear how to do this.  Try with a catch routine
; calls sb_allimupdat with /pixmap

; catch out-of-memory error and truncate time vector accordingly:
catch,errstat
if errstat ne 0 then begin
   catch,/cancel
   ; remove pixmaps for current time j
   for k=0,maxdw-1 do if valdw[k] then wdelete,(*cmov.pwv)[j,k]
   ; adjust cmov parameters to reduced number of times
   *cmov.pwv = (*cmov.pwv)[0:j-1L,*]
   *cmov.ptv = (*cmov.ptv)[0:j-1L]
   cmov.ct = ct0 < (j-1L)
   ; goes plot is updated in calling routine, not here
   return
endif

; delete old movie pixmaps, if they exist
if ptr_valid(cmov.pwv) then begin
   ww = where(*cmov.pwv ge 0,nww)
   for k=0L,nww-1 do wdelete,(*cmov.pwv)[k]
   ptr_free,cmov.pwv
endif

; initialize pointer to (new) pixmaps
ntvec = n_elements(*cmov.ptv)
maxdw = n_elements(cimd)
cmov.pwv = ptr_new(lonarr(ntvec,maxdw)-1L)

; vector of realized data windows
valdw = intarr(maxdw)
for k=0,maxdw-1 do valdw[k] = WIDGET_INFO(cimd[k].disp.wtop,/valid)

; keep track of original "current frame"
ct0 = cmov.ct

for j=0L,n_elements(*cmov.ptv)-1L do begin
   ; create pixmap(s) for current frame
   for k=0,maxdw-1 do if valdw[k] then begin
       window,xsize=cimd[k].disp.naxis[0],ysize=cimd[k].disp.naxis[1], $
              /free,/pixmap
       (*cmov.pwv)[j,k] = !d.window
   endif
   ; write image(s) for current frame into pixmap(s) and into display(s)
   cmov.ct = j
   sb_allimupdat,/pixmap
endfor

; restore original "current frame"
cmov.ct = ct0

end
