
;+
; NAME:
;        sb_imd
; PURPOSE:
;        creation and event handling of image display par. widget for go_secchi
;        Image display for STEREO-SECCHI
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        sb_imd,cdw
; INPUTS:
;        cdw: data window # to be created/updated
; KEYWORDS (INPUT):
; OUTPUTS:
; KEYWORDS (OUTPUT):
; COMMON BLOCKS:
;        sb_com
; SIDE EFFECTS:
; RESTRICTIONS:
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 1/17/2006
;-

pro sb_imd_event, ev
  COMMON sb_com
  WIDGET_CONTROL, ev.top, GET_UVALUE=w
  WIDGET_CONTROL, ev.id,  GET_UVALUE=uval
  case uval of
  'wdo' : case ev.value of
          ; action buttons on top
          0 : begin
                ; read all parameters and apply
                sb_imdbut,w
              end
          1 : begin
              ; destroy this widget without applying new changes
                ptr_free,w.pbcub                        ; free byte cube
                ptr_free,w.pcdsim                       ; free float image
                WIDGET_CONTROL,ctop.wtop,SENSITIVE=1    ; top widget on
                WIDGET_CONTROL, ev.top, /DESTROY        ; destroy this widget
              end
;          2 : begin
;              ; destroy this widget with applying new changes
;                sb_imdbut,w
;                ptr_free,w.pbcub                        ; free byte cube
;                ptr_free,w.pcdsim                       ; free float image
;                WIDGET_CONTROL,ctop.wtop,SENSITIVE=1    ; top widget on
;                WIDGET_CONTROL, ev.top, /DESTROY        ; destroy this widget
;              end
          endcase
  ; define dataset
   'wnam': begin
            ; define dataset: first determine which DS button was clicked
            w.cds = where(w.wnam eq ev.id)
            ;  switch LUT subwidget to this dataset
              ; set subwidget values for new cds:
            WIDGET_CONTROL,w.wled,SET_VAL=w.cds         ; set lut edit button
            col = cimd[w.cdw].col[w.cds]
            WIDGET_CONTROL,w.wnam[w.cds],GET_VAL=lnam   ; get DS label
            WIDGET_CONTROL,w.wlna,SET_VAL=lnam          ; set subwidget label
            WIDGET_CONTROL,w.wmin,SET_VAL=string(col.min,f='(g8.2)')  ; min
            WIDGET_CONTROL,w.wmax,SET_VAL=string(col.max,f='(g8.2)')  ; max
            WIDGET_CONTROL,w.wgam,SET_VAL=string(col.gam,f='(f5.2)')  ; gamma
              ; now call the data selection widget
            sb_dss,w.cds,w.cdw,leader=ev.top         ; define DS
          end
  'wsho': begin
            ; show this DS
            WIDGET_CONTROL,ev.id,GET_VAL=bvec
            cimd[w.cdw].col.show = bvec               ; update common block
            ; update displayed image if display exists:
            if WIDGET_INFO(cimd[w.cdw].disp.wtop,/valid) then begin
               wset,cimd[w.cdw].disp.win
               tv,sb_byt2col(w.pbcub,cimd[w.cdw].col),true=3
            endif
          end
  'wrdf': begin
            ; apply running difference
            WIDGET_CONTROL,ev.id,GET_VAL=bvec
            wds = where(bvec ne cimd[w.cdw].col.rundif)
            cimd[w.cdw].col.rundif = bvec             ; update common block
            sb_imdupdat,w,wds                         ; update images&display
          end
  'wprj': begin
            ; switch to projection onto sphere
            WIDGET_CONTROL,ev.id,GET_VAL=bvec
            wds = where(bvec ne cimd[w.cdw].col.proj)
            cimd[w.cdw].col.proj = bvec               ; update common block
            sb_imdupdat,w,wds                         ; update images&display
          end
  'wled': begin
            ; switch LUT subwidget to other dataset
            w.cds = ev.value                            ; new dataset number
            sb_imbcub,w.cdw,w.pbcub,w.cds,w.pcdsim      ; update bcub,cdsim
            col = cimd[w.cdw].col[w.cds]
            ; set subwidget values for new cds:
            WIDGET_CONTROL,w.wnam[w.cds],GET_VAL=lnam   ; get DS label
            WIDGET_CONTROL,w.wlna,SET_VAL=lnam          ; set subwidget label
            WIDGET_CONTROL,w.wmin,SET_VAL=string(col.min,f='(g8.2)')  ; min
            WIDGET_CONTROL,w.wmax,SET_VAL=string(col.max,f='(g8.2)')  ; max
            WIDGET_CONTROL,w.wgam,SET_VAL=string(col.gam,f='(f5.2)')  ; gamma
          end
  'wmin': begin
            ; updated min,max,gam values
            WIDGET_CONTROL,w.wmin,GET_VAL=fvalue
            cimd[w.cdw].col[w.cds].min = fvalue
            WIDGET_CONTROL,w.wmax,GET_VAL=fvalue
            cimd[w.cdw].col[w.cds].max = fvalue
            WIDGET_CONTROL,w.wgam,GET_VAL=fvalue
            cimd[w.cdw].col[w.cds].gam = fvalue
            ; update bcub and displayed image if display exists:
            sb_imdupdat,w
          end
  'wmax': begin
            ; updated min,max,gam values
            WIDGET_CONTROL,w.wmin,GET_VAL=fvalue
            cimd[w.cdw].col[w.cds].min = fvalue
            WIDGET_CONTROL,w.wmax,GET_VAL=fvalue
            cimd[w.cdw].col[w.cds].max = fvalue
            WIDGET_CONTROL,w.wgam,GET_VAL=fvalue
            cimd[w.cdw].col[w.cds].gam = fvalue
            ; update bcub and displayed image if display exists:
            sb_imdupdat,w
          end
  'wgam': begin
            ; updated min,max,gam values
            WIDGET_CONTROL,w.wmin,GET_VAL=fvalue
            cimd[w.cdw].col[w.cds].min = fvalue
            WIDGET_CONTROL,w.wmax,GET_VAL=fvalue
            cimd[w.cdw].col[w.cds].max = fvalue
            WIDGET_CONTROL,w.wgam,GET_VAL=fvalue
            cimd[w.cdw].col[w.cds].gam = fvalue
            ; update bcub and displayed image if display exists:
            sb_imdupdat,w
          end
  'wmma': begin
            ; updated gam value, in case it was changed without CR
            WIDGET_CONTROL,w.wgam,GET_VAL=fvalue
            cimd[w.cdw].col[w.cds].gam = fvalue
            ; calculate min/max of current image and apply
            sb_imdupdat,w,/auto
            col = cimd[w.cdw].col[w.cds]
            WIDGET_CONTROL,w.wmin,SET_VAL=string(col.min,f='(g8.2)')  ; min
            WIDGET_CONTROL,w.wmax,SET_VAL=string(col.max,f='(g8.2)')  ; max
          end
  'wlut': begin
            ; update lut
            sb_imdlut,w,ev.index       ; update common block and color bar
            ; update displayed image if display exists:
            if WIDGET_INFO(cimd[w.cdw].disp.wtop,/valid) then begin
              wset,cimd[w.cdw].disp.win
              tv,sb_byt2col(w.pbcub,cimd[w.cdw].col),true=3
            endif
          end
  else  :
  endcase

  if WIDGET_INFO(ev.top,/valid) then $          ; if not destroyed, save w
     WIDGET_CONTROL, ev.top, SET_UVALUE=w       ; (may have changed)

end


pro sb_imd,cdw

  common sb_com

  ; create display window if it doesn't yet exist
  sb_imwin,cdw

  ; get color table names from file sb_colors1.tbl
  sb_imdlut,names=lut,/getnames
  ; eventually update sb_colors1.tbl with modifyct.pro

  ; some variables:
  cds = 0
  disp = cimd[cdw].disp
  col  = cimd[cdw].col
  maxds = n_elements(col)
  sarr = strarr(maxds)+' '
  
  ; the imd user variable:
  w = {cdw:cdw,cds:cds,pbcub:ptr_new(-1),pcdsim:ptr_new(-1), $
       wdo:0L,wsx:0L,wsy:0L,wdxy:0L, $
       wrlo:0L,wrla:0L,wrr:0L,wrx:0L,wry:0L,wcar:0L,wods:0L, $
       wnam:lonarr(maxds),wsho:0L,wrdf:0L,wprj:0L,wled:0L, $
       wbar:lonarr(maxds),barys:0,wlna:0L,wmin:0L,wmax:0L,wgam:0L,wlut:0L}
  
  ; the widget:

  base   = WIDGET_BASE(/COLUMN, GROUP_LEADER=ctop.wtop,XOFFSET=30, $
           YOFFSET=300,TITLE='Parameters for Image Display '+strtrim(cdw,2))
  ; first geometry parameters for all datasets
;  bas0   = WIDGET_BASE(base, /ROW)
;  w.wdo  = CW_BGROUP(base,[' Apply  ',' Cancel ','  Done  '],/ROW,UVAL='wdo')
  w.wdo  = CW_BGROUP(base,['   Apply    ','    Done    '],/ROW,UVAL='wdo')
  bas1   = WIDGET_BASE(base, /ROW)
  wdmy   = WIDGET_LABEL(bas1, VALUE='Size (pixels): x=' )
  w.wsx  = WIDGET_TEXT (bas1, VALUE=string(disp.naxis[0],f='(i4)'), $
                        XSIZE=7, /EDITABLE, UVAL='wsx')
  wdmy   = WIDGET_LABEL(bas1, VALUE=' y=' )
  w.wsy  = WIDGET_TEXT (bas1, VALUE=string(disp.naxis[1],f='(i4)'), $
                        XSIZE=7, /EDITABLE, UVAL='wsy')
  wdmy   = WIDGET_LABEL(bas1, VALUE='   Scale (pix/solrad) ')
  w.wdxy = WIDGET_TEXT (bas1, VALUE=string(disp.delt_1,f='(g7.5)'), $
                        XSIZE=7, /EDITABLE, UVAL='wdxy')
  wdmy   = WIDGET_LABEL(base, VALUE='Reference point:',/ALIGN_LEFT)
  bas2   = WIDGET_BASE(base, /ROW)
  wdmy   = WIDGET_LABEL(bas2, VALUE='Lon=')
  w.wrlo = WIDGET_TEXT (bas2, VALUE=string(disp.rval[0],f='(f7.2)'), $
                        XSIZE=7, /EDITABLE, UVAL='wrlo')
  wdmy   = WIDGET_LABEL(bas2, VALUE=' Lat=')
  w.wrla = WIDGET_TEXT (bas2, VALUE=string(disp.rval[1],f='(f7.2)'), $
                        XSIZE=7, /EDITABLE, UVAL='wrla')
  wdmy   = WIDGET_LABEL(bas2, VALUE=' R/Rsun=')
  w.wrr  = WIDGET_TEXT (bas2, VALUE=string(disp.rval[2],f='(f7.3)'), $
                        XSIZE=7, /EDITABLE, UVAL='wrra')
  wdmy   = WIDGET_LABEL(bas2, VALUE=' Ref.pix x=')
  w.wrx  = WIDGET_TEXT (bas2, VALUE=string(disp.rpix[0],f='(f7.1)'), $
                        XSIZE=7, /EDITABLE, UVAL='wrx')
  wdmy   = WIDGET_LABEL(bas2, VALUE=' y=' )
  w.wry  = WIDGET_TEXT (bas2, VALUE=string(disp.rpix[1],f='(f7.1)'), $
                        XSIZE=7, /EDITABLE, UVAL='wry')
  bas3   = WIDGET_BASE(base, /ROW)
  wdmy   = WIDGET_LABEL(bas3, VALUE='Ref.Lon: ')
  w.wcar = CW_BGROUP(bas3,['Stonyhurst','Carrington'],/ROW,UVAL='wcar', $
                     /EXCLUSIVE,/NO_REL,SET_VAL=disp.carr)
  wdmy   = WIDGET_LABEL(bas3, VALUE='  Observer Location:')
  w.wods = WIDGET_DROPLIST(bas3, VALUE=sarr, UVAL='wods')
    WIDGET_CONTROL,w.wods,SET_DROPLIST_SELECT=disp.obs
  
  ; next dataset specific parameters
  bas4   = WIDGET_BASE(base, /ROW)
  geom   = WIDGET_INFO(bas4, /GEOM)
  ypad2  = 2*geom.ypad                  ; attempt to fudge button alignment
  bas41  = WIDGET_BASE(bas4, /COL, ypad=ypad2)
  wdmy   = WIDGET_LABEL(bas41, VALUE='DefineDS')
  for i=0,maxds-1 do w.wnam[i] = WIDGET_BUTTON(bas41,UVAL='wnam', $
                                 VALUE='         ') ; value is 9 char long
  w.wsho = CW_BGROUP(bas4,sarr,/COL,UVAL='wsho',/NONEXC, $
                     LABEL_TOP='Show   ',SET_VAL=col.show)
  w.wrdf = CW_BGROUP(bas4,sarr,/COL,UVAL='wrdf',/NONEXC, $
                     LABEL_TOP='Rundiff',SET_VAL=col.rundif)
  w.wprj = CW_BGROUP(bas4,sarr,/COL,UVAL='wprj',/NONEXC, $
                     LABEL_TOP='Project',SET_VAL=col.proj)
  w.wled = CW_BGROUP(bas4,sarr,/COL,UVAL='wled',/EXCLUSIVE, $
                     LABEL_TOP='EditLUT', /NO_REL, SET_VAL=cds)
  bas42  = WIDGET_BASE(bas4, /COL, ypad=ypad2)
  wdmy   = WIDGET_LABEL(bas42, VALUE=' ')       ; for aligning color bar
  ; empirical attempt to calculate vertical size of color bars:
  geom   = WIDGET_INFO(w.wnam[0],/GEOM)
  w.barys  = geom.ysize                       ; trying to get proper bar size
  for i=0,maxds-1 do w.wbar[i] = WIDGET_DRAW(bas42, XSIZE=256, YSIZE=w.barys)

  ; LUT manipulation items
  bas5   = WIDGET_BASE(base, /ROW, /FRAME)
  w.wlna = WIDGET_LABEL(bas5, VALUE='         ')   ; 9 char long
  wdmy   = WIDGET_LABEL(bas5, VALUE='  Min:')
  w.wmin = WIDGET_TEXT (bas5, VALUE=string(col[cds].min,f='(g8.2)'), $
                        XSIZE=8,UVAL='wmin',/EDITABLE)
  wdmy      = WIDGET_LABEL(bas5, VALUE=' Max:')
  w.wmax = WIDGET_TEXT (bas5, VALUE=string(col[cds].max,f='(g8.2)'), $
                        XSIZE=8,UVAL='wmax',/EDITABLE)
  wdmy      = WIDGET_LABEL(bas5, VALUE=' Gamma:')
  w.wgam = WIDGET_TEXT (bas5, VALUE=string(col[cds].gam,f='(f5.2)'), $
                        XSIZE=5,UVAL='wgam',/EDITABLE)
  wdmy   = CW_BGROUP(bas5,'minmax',/ROW,UVAL='wmma')
  w.wlut = WIDGET_DROPLIST(bas5, VALUE=lut,UVAL='wlut')

  WIDGET_CONTROL, base, /REALIZE
  XMANAGER, 'sb_imd', base ,NO_BLOCK=1

  ; init button labels:
  sb_imdlbl,w

  ; init color bars (after realizing widget)
  sb_imdlut,w
  
  ; label for currently selected lut
  WIDGET_CONTROL,w.wnam[w.cds],GET_VAL=lnam
  WIDGET_CONTROL,w.wlna,SET_VAL=lnam

  ; image array activities: populate byte array and current image array
  sb_imbcub,w.cdw,w.pbcub,w.cds,w.pcdsim,/renew

  WIDGET_CONTROL, base, SET_UVAL=w            ; save user variable

end
