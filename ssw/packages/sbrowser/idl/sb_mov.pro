
;+
; NAME:
;        sb_mov
; PURPOSE:
;        creation and event handling of movie par. widget for go_secchi
;        Image display for STEREO-SECCHI
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        sb_mov
; INPUTS:
; KEYWORDS (INPUT):
; OUTPUTS:
; KEYWORDS (OUTPUT):
; COMMON BLOCKS:
;        sb_com
; SIDE EFFECTS:
; RESTRICTIONS:
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 7/11/2007
;-

pro sb_mov_event, ev
  COMMON sb_com
  WIDGET_CONTROL, ev.top, GET_UVALUE=w
  WIDGET_CONTROL, ev.id,  GET_UVALUE=uval
  case uval of
  'bact': case ev.value of
          ; action buttons on top
          0 : begin
                ; Create movie frames, enter movie mode
                WIDGET_CONTROL,/hourglass               ; could take a while
                WIDGET_CONTROL,w.wtm,SENSITIVE=0
                WIDGET_CONTROL,w.bas3,SENSITIVE=0
                WIDGET_CONTROL,w.bas4,SENSITIVE=0
                sb_mkmovie
                ; update GOES plot in case cmov changed due to memory limits
                sb_plotgoes,ctop.ts,ctop.te,win=ctop.goesw,pixm=ctop.goespm, $
                            /update           ; first upate pixmap w/out ticks
                sb_plotgoes,ctop.ts,ctop.te,win=ctop.goesw,pixm=ctop.goespm, $
                            ticks=*cmov.ptv   ; now plot the tick marks
                ; adjust sensitivities for top widget
                WIDGET_CONTROL,ctop.wbut2,SENSITIVE=0   ; DS buttons
                WIDGET_CONTROL,ctop.wbas2,SENSITIVE=1   ; movie controls
              end
          1 : begin
                ; Quit movie mode
                if ptr_valid(cmov.pwv) then begin
                  ; delete pixmaps and pointer
                  ww = where(*cmov.pwv ge 0,nww)
                  for k=0L,nww-1 do wdelete,(*cmov.pwv)[k]
                  ptr_free,cmov.pwv
                  ; redo time vector (may be modified for movie) and goesplot
                  sb_updatcmov
                  ; update GOES plot in case cmov changed due to memory limits
                  sb_plotgoes,ctop.ts,ctop.te,win=ctop.goesw,pixm=ctop.goespm, $
                              /update           ; first upate pixmap w/out ticks
                  sb_plotgoes,ctop.ts,ctop.te,win=ctop.goesw,pixm=ctop.goespm, $
                              ticks=*cmov.ptv   ; now plot the tick marks
                  ; re-establish sensitivities
                  WIDGET_CONTROL,w.wtm,SENSITIVE=1
                  WIDGET_CONTROL,w.bas3,SENSITIVE=(cmov.tdel ne 0.0)
                  WIDGET_CONTROL,w.bas4,SENSITIVE=(cmov.tdel eq 0.0)
                  WIDGET_CONTROL,ctop.wbut2,SENSITIVE=1   ; DS buttons
                  WIDGET_CONTROL,ctop.wbas2,SENSITIVE=0   ; movie controls
                endif
              end
          2 : begin
                ; Done
                WIDGET_CONTROL, ev.top, /DESTROY        ; destroy this widget
                sb_allimupdat
                sb_topupdat
                WIDGET_CONTROL, ctop.wtop,SENSITIVE=1   ; top widget on
              end
          endcase
  'bsav': begin
            ; Save frames as JPEGs
            ;.....
          end
  'btm' : begin
            ; set sensitivity of appropriate widget fields
            WIDGET_CONTROL,w.bas3,SENSITIVE=(ev.value eq 0)
            WIDGET_CONTROL,w.bas4,SENSITIVE=(ev.value ne 0)
            sb_movbut,w
          end
  'tdel': sb_movbut,w
  'toff': sb_movbut,w
  'dwn' : begin
            sb_movbut,w
            ; update DSN droplist labels
            dsnt  = strmid(sb_dslbl(cmov.dwn)+'         ',0,9)
            WIDGET_CONTROL,w.wds,SET_VALUE=dsnt
          end
  'dsn' : sb_movbut,w
  else  :
  endcase

end


pro sb_mov

  common sb_com

  ; some variables:
  but1t = ['Create Movie Pixwins','Terminate Movie','   Done   ']
  dwnt  = 'Image Display '+strtrim(indgen(n_elements(cimd)),2)
  dsnt  = strmid(sb_dslbl(cmov.dwn)+'         ',0,9)
  spath = ''
  notmv = ptr_valid(cmov.pwv) eq 0    ; true if not movie mode

  ; the sb_mov user variable:
  w = {wsw:0L,wpa:0L,wtm:0L,bas3:0L,wtd:0L,wto:0L,bas4:0L,wdw:0L,wds:0L}
  
  ; the widget:

  base   = WIDGET_BASE(/COLUMN, GROUP_LEADER=ctop.wtop,XOFFSET=30, $
           YOFFSET=360,TITLE='Movie Parameters')
  ; first various actions
  wdmy  = CW_BGROUP(base,but1t,/ROW,UVAL='bact')
  ; save frames option
  bas1  = WIDGET_BASE(base, /ROW   $ ; )
                      , SENSITIVE=0)  ; feature not yet implemented!
  wdmy  = CW_BGROUP(bas1,'Save Frames as JPEGs',/ROW,UVAL='bsav')
  wdmy  = WIDGET_LABEL(bas1, VALUE=' for:')
  w.wsw = WIDGET_DROPLIST(bas1, VALUE=dwnt, UVAL='dwn')
  wdmy  = WIDGET_LABEL(bas1, VALUE='   Path:')
  w.wpa = WIDGET_TEXT(bas1, VALUE=spath, /EDITABLE, UVAL='path', XSIZE=40)
  ; frame time mode
  bas2  = WIDGET_BASE(base, /ROW)
  wdmy  = WIDGET_LABEL(bas2, VALUE='Frame Times: ')
  w.wtm = CW_BGROUP(bas2,['Generate Grid','Use DS Times'],/ROW,UVAL='btm', $
             /EXCLUSIVE,/NO_REL,SET_VAL=(cmov.tdel eq 0.0))
    WIDGET_CONTROL,w.wtm,SENSITIVE=notmv
  ; grid mode parameters
  w.bas3= WIDGET_BASE(base, /ROW, SENSITIVE=(cmov.tdel ne 0.0 and notmv))
  wdmy  = WIDGET_LABEL(w.bas3, VALUE='  Grid:  Time Between Frames (sec):' )
  w.wtd = WIDGET_TEXT (w.bas3, VALUE=string(cmov.tdel,f='(f8.1)'), $
                       XSIZE=8, /EDITABLE, UVAL='tdel')
  wdmy  = WIDGET_LABEL(w.bas3, VALUE='  Time Offset (sec):')
  w.wto = WIDGET_TEXT (w.bas3, VALUE=string(cmov.toff,f='(f8.1)'), $
                       XSIZE=8, /EDITABLE, UVAL='toff')
  ; DS mode parameters
  w.bas4= WIDGET_BASE(base, /ROW, SENSITIVE=(cmov.tdel eq 0.0 and notmv))
  wdmy  = WIDGET_LABEL(w.bas4, VALUE='  Times from DS: ' )
  w.wdw = WIDGET_DROPLIST(w.bas4, VALUE=dwnt, UVAL='dwn')
    WIDGET_CONTROL,w.wdw,SET_DROPLIST_SELECT=cmov.dwn
  w.wds = WIDGET_DROPLIST(w.bas4, VALUE=dsnt, UVAL='dsn')
    WIDGET_CONTROL,w.wds,SET_DROPLIST_SELECT=cmov.dsn

  WIDGET_CONTROL, base, /REALIZE
  ; update GOES plot: first upate pixmap w/out ticks, then plot the tick marks
  sb_plotgoes,ctop.ts,ctop.te,win=ctop.goesw,pixm=ctop.goespm,/update
  sb_plotgoes,ctop.ts,ctop.te,win=ctop.goesw,pixm=ctop.goespm,ticks=*cmov.ptv
  XMANAGER, 'sb_mov', base ,NO_BLOCK=1
  
  WIDGET_CONTROL, base, SET_UVAL=w            ; save user variable

end
