
;+
; $Id$
;
; Project : STEREO SECCHI
;
; Name    : sb_imwin
;
; Purpose : creates image display window
;
; Use     : IDL> sb_imwin, cdw
;
; Inputs  :
;    cdw = display number (betw. 0 and 3)
;          Note: a new display window is only created if it doesn't
;                currently exist.  It must be destroyed first before
;                this routine will recreate it (e.g., at a different size)
; Outputs :
;
; Keywords:
;
; Common  : sb_com
;
; Restrictions: No error checking is performed!
;
; Side effects:
;
; Category    : image processing, widgets
;
; Prev. Hist. :
;
; Written     : Jean-Pierre Wuelser, LMSAL, 4-Oct-2006
;
; $Log$
;-

; event handler
pro sb_imwin_event,ev

common sb_com

; react only to button press event:
if ev.type eq 0 && WIDGET_INFO(ctpt.wnp,/valid) then begin

  ; check data structure exists
  if ~ptr_valid(ctpt.pd) then begin
    sb_mkptsdata,pts
    ctpt.pd=ptr_new(pts)
  endif

  ; check if this nump,numt in this feature already exists in data structure
  ww = where((*ctpt.pd).numf eq ctpt.numf and (*ctpt.pd).nump eq ctpt.nump $
             and (*ctpt.pd).numt eq cmov.ct,nww)
  if nww eq 0 then begin
    pts = (*ctpt.pd)
    ; check if any undefined (usually deleted) points, if not, append pts
    ww = where(pts.stat eq 0L,nww)
    if nww gt 0 then j=ww[0] else begin
      sb_mkptsdata,pts
      j = n_elements(pts)-1L
    endelse
    pts[j].numf = ctpt.numf
    pts[j].nump = ctpt.nump
    pts[j].numt = cmov.ct
    *ctpt.pd = pts
  endif else j = ww[0]

  ; get the window # being clicked
  ww = where(ev.top eq cimd.disp.wtop)
  win = ww[0]

  inc = 0   ; initialize variable for autoincrement of point# or time

  ; 3 cases for .stat, 0: not yet defined, 1: 1 view defined, 2: both defined
  if (*ctpt.pd)[j].stat eq 0 then begin
    ; first view: just save xy coords
    (*ctpt.pd)[j].xy = [ev.x,ev.y]
    (*ctpt.pd)[j].dwn = win
    (*ctpt.pd)[j].stat = 1
    wset,cimd[win].disp.win
    plots,[ev.x],[ev.y],/device,psym=1,col=(255L*256L+255L)*256L+255L
    if ctpt.mode eq 0 then inc=1   ; conditions for autoincrement are met
  endif

  if (*ctpt.pd)[j].stat eq 1 then if (*ctpt.pd)[j].dwn ne win then begin
    ; 2nd view: calculate 3D coordinates
    win1 = (*ctpt.pd)[j].dwn
    ; first get wcs and position for both views
    ds1 = cimd[win1].disp.obs
    ds2 = cimd[win].disp.obs
    go = ptr_valid(cdss[ds1,win1].ptrds) && $
         ptr_valid(cdss[ds2,win].ptrds)
    if go then begin
      ; get all the wcs structures
      tc = (*cmov.ptv)[cmov.ct]                       ; current time
      tt  = (*cdss[ds1,win1].ptrds).tt                ; ds times
      dmy = min(abs(tt-tc),s0)                        ; time closest to tc
      fnam=(*cdss[ds1,win1].ptrds)[s0].filename
      sb_readfits,fnam,oii
      owcs = fitshead2wcs(oii)
      wcs1 = sb_wcs(disp=cimd[win1].disp,obswcs=owcs)
      tt  = (*cdss[ds2,win].ptrds).tt                 ; ds times
      dmy = min(abs(tt-tc),s0)                        ; time closest to tc
      fnam=(*cdss[ds2,win].ptrds)[s0].filename
      sb_readfits,fnam,oii
      owcs = fitshead2wcs(oii)
      wcs2 = sb_wcs(disp=cimd[win].disp,obswcs=owcs)
      ; calculate the 3D coordinates
      rval = sb_pix23d((*ctpt.pd)[j].xy,float([ev.x,ev.y]), $
                       wcs1=wcs1,wcs2=wcs2,/carr)
      (*ctpt.pd)[j].tt = (*cmov.ptv)[cmov.ct]
      (*ctpt.pd)[j].rval = rval
      (*ctpt.pd)[j].carr = 1
      (*ctpt.pd)[j].psym = ctpt.psym
      (*ctpt.pd)[j].col = ctpt.col
      (*ctpt.pd)[j].stat = 3
      sb_plot3d,feat=ctpt.numf
      inc=1   ; conditions for autoincrement are met
      txtv  = strmid('                                        ',0,40)
      WIDGET_CONTROL,ctpt.wtx,set_value=txtv
    endif
  endif else begin
    ; clicked in same window -> print message
    txtv  = strmid('Click in other window!                  ',0,40)
    WIDGET_CONTROL,ctpt.wtx,set_value=txtv
  endelse

  if (*ctpt.pd)[j].stat eq 2 then begin
    ; point already defined -> print message
    txtv  = strmid('Point already defined!                  ',0,40)
    WIDGET_CONTROL,ctpt.wtx,set_value=txtv
  endif

  ; autoincrement point# if enabled
  if inc and ctpt.ainc eq 1 then begin
    ctpt.nump=ctpt.nump+1L
    WIDGET_CONTROL, ctpt.wnp, SET_VALUE=string(ctpt.nump,f='(i5)')
  endif
  ; autoincrement time if enabled
  if inc and ctpt.ainc eq 2 then begin
    cmov.ct = (cmov.ct+1) < (n_elements(*cmov.ptv)-1)
    sb_allimupdat
    sb_plot3d
    sb_topupdat,/noredraw     ; update plot,slider,timestring
  endif

endif

end


pro sb_imwin,cdw

 common sb_com

 wex = WIDGET_INFO(cimd[cdw].disp.wtop,/valid)        ; exists?

 if wex eq 0 then begin
   bas = WIDGET_BASE(GROUP_LEADER=ctop.wtop,XOFFSET=630+30*cdw, $
         YOFFSET=30*cdw,TITLE='Display '+strtrim(cdw,2))
   cimd[cdw].disp.wtop = bas
   dra = WIDGET_DRAW(bas, XSIZE=cimd[cdw].disp.naxis[0], $
                          YSIZE=cimd[cdw].disp.naxis[1],/button_events)
   WIDGET_CONTROL, bas, /REALIZE              ; show it
   XMANAGER, 'sb_imwin', bas ,NO_BLOCK=1

   WIDGET_CONTROL, dra,  GET_VALUE=win        ; window #
   cimd[cdw].disp.win = win
 endif

end
