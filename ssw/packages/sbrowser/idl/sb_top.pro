
;+
; NAME:
;        sb_top
; PURPOSE:
;        creation and event handling of main widget for go_secchi
;        Image display for STEREO-SECCHI
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        base = sb_top()
; INPUTS:
; KEYWORDS (INPUT):
; OUTPUTS:
;        base : widget id of top base of this widget
; KEYWORDS (OUTPUT):
; COMMON BLOCKS:
;        sb_com
; SIDE EFFECTS:
; RESTRICTIONS:
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 8/26/2005
;-

PRO sb_top_event, ev

  COMMON sb_com
  WIDGET_CONTROL, ev.id, GET_UVALUE=uval
  CASE uval OF
    'but1': CASE ev.value OF
              0 : IF PTR_VALID(cmov.pwv) eq 0 THEN BEGIN  ; not in movie mode?
                  ; define time window (only if not in movie mode)
                     WIDGET_CONTROL,ev.top,SENSITIVE=0    ; top widget off
                     sb_timwid
                  ENDIF
              1 : BEGIN
                  ; movie definition tool
                    WIDGET_CONTROL,ev.top,SENSITIVE=0     ; top widget off
                    WIDGET_CONTROL,ctop.wbut3,SET_VAL=1   ; pause movie
                    sb_mov
                  END
              2 : sb_tptool
              5 : BEGIN
                    ; delete heap variables and windows
                    ptr_free,cdss.ptrds
                    wdelete,ctop.goespm
                    ptr_free,cmov.ptv
                    IF ptr_valid(cmov.pwv) then begin
                       ww = where(*cmov.pwv ge 0,nww)
                       for k=0L,nww-1 do wdelete,(*cmov.pwv)[k]
                       ptr_free,cmov.pwv
                    ENDIF
                    WIDGET_CONTROL, ev.TOP, /DESTROY
                  END
              ELSE:
            ENDCASE
     'but2': BEGIN
             ; update display
               cim = ev.value
               WIDGET_CONTROL,ev.top,SENSITIVE=0    ; top widget off
               sb_imd,cim
             END
    'sld1': BEGIN
              ; get current slider value, and deduce last/previous value
              WIDGET_CONTROL, ev.id, GET_VALUE=cval
              lval = fix(1d3*((*cmov.ptv)[cmov.ct]-ctop.ts)/(ctop.te-ctop.ts))
              CASE (cval-lval) OF
                -1  : cmov.ct = (cmov.ct-1) > 0
                 1  : cmov.ct = (cmov.ct+1) < (n_elements(*cmov.ptv)-1)
                ELSE: BEGIN
                        tcur = ctop.ts + (ctop.te-ctop.ts)*double(cval)/1d3
                        dsec = min(abs(*cmov.ptv-tcur),ct)
                        cmov.ct = ct
                      ENDELSE
              ENDCASE
              sb_allimupdat
                ; plot overlays for movies if in tie point tool
                ; simple logic of when to do it, subject to change
              if WIDGET_INFO(ctpt.wnp,/valid) then sb_plot3d
              sb_topupdat,/noredraw     ; update plot,slider,timestring
            END
    'but3': CASE ev.value OF
            0 : BEGIN    ; forward
                  cmov.spd = abs(cmov.spd)
                  WIDGET_CONTROL,ctop.wtxt2,timer=1.0/(0.6*abs(cmov.spd))
                END
            1 :          ; pause
            2 : BEGIN    ; backward
                  cmov.spd = abs(cmov.spd)*(-1)
                  WIDGET_CONTROL,ctop.wtxt2,timer=1.0/(0.6*abs(cmov.spd))
                END
            3 : BEGIN    ; back and forth
                  WIDGET_CONTROL,ctop.wtxt2,timer=1.0/(0.6*abs(cmov.spd))
                END
            ENDCASE
    'timr': BEGIN        ; timer event
               WIDGET_CONTROL,ctop.wbut3,GET_VALUE=cval
               IF cval ne 1 THEN BEGIN   ; do unless paused
                  ; reload timer
                  WIDGET_CONTROL,ctop.wtxt2,timer=1.0/(0.6*abs(cmov.spd))
                  ; adjust current time based on direction
                  If cmov.spd ge 0 THEN cmov.ct = cmov.ct+1L $
                                   ELSE cmov.ct = cmov.ct-1L
                  ; handle end-of-range cases, depending on buttons
                  If cmov.ct lt 0 THEN BEGIN
                     IF cval eq 3 THEN BEGIN
                        cmov.spd = abs(cmov.spd)       ; change to forward
                        cmov.ct = 1L
                     ENDIF ELSE cmov.ct = n_elements(*cmov.ptv)-1L
                  ENDIF
                  If cmov.ct ge n_elements(*cmov.ptv) THEN BEGIN
                     IF cval eq 3 THEN BEGIN
                        cmov.spd = abs(cmov.spd)*(-1)  ; change to backward
                        cmov.ct = n_elements(*cmov.ptv)-2L
                     ENDIF ELSE cmov.ct = 0L
                  ENDIF
                  ; copy pixmaps to display windows
                  sb_allimupdat
                    ; plot overlays for movies if in tie point tool
                    ; simple logic of when to do it, subject to change
                  if WIDGET_INFO(ctpt.wnp,/valid) then sb_plot3d
                  sb_topupdat,/noredraw     ; update plot,slider,timestring
               ENDIF
            END
    'sld2': BEGIN
               if cmov.spd lt 0 then cmov.spd = (ev.value > 1)*(-1) $
                                else cmov.spd = (ev.value > 1)
            END
    ELSE:
  ENDCASE
END

PRO sb_top

  COMMON sb_com

  butt1 = ['  Time Range  ','    Movie     ','Tie Point Tool', $
           ' Load Session ',' Save Session ',' Exit Session ']
  butt2 = '   Define Display '+strtrim(indgen(n_elements(cimd)),2)+'    '
  butt3 = [' -> ',' || ',' <- ','<-->']
  CASE !d.name OF
    'X'   : tfont = '10x20'
    'WIN' : tfont = '20'
    ELSE  : tfont = ''
  ENDCASE

  ctop.wtop  = WIDGET_BASE(/COLUMN, TITLE='go_secchi')
  ctop.wbut1 = CW_BGROUP(ctop.wtop, butt1, /ROW, UVAL='but1')
  ctop.wbut2 = CW_BGROUP(ctop.wtop, butt2, /ROW, UVAL='but2')
  wgoes      = WIDGET_DRAW(ctop.wtop, XSIZE=640, YSIZE=160)
  WINDOW,/FREE,/PIXMAP,XSIZE=640,YSIZE=160   ; create pixmap for goesplot
  ctop.goespm = !d.window                    ; store # in common block
  cval = 0 > fix(1d3*((*cmov.ptv)[cmov.ct]-ctop.ts)/(ctop.te-ctop.ts)) < 1000
  ctop.wsld1  = WIDGET_SLIDER(ctop.wtop,MAX=1000,SCROLL=1,/SUPPR, $
                         VAL=cval,UVAL='sld1')
  tinis = strmid(anytim((*cmov.ptv)[cmov.ct],/stime),0,20)
  ctop.wtxt1 = WIDGET_LABEL(ctop.wtop, VALUE=tinis, FONT=tfont)
  ctop.wbas2 = WIDGET_BASE(ctop.wtop, /ROW, SENSITIVE=0)
  ctop.wbut3 = CW_BGROUP(ctop.wbas2, butt3, /ROW, UVAL='but3', SET_VAL=1, $
                        /EXCLUSIVE, /NO_REL)
  ctop.wtxt2 = WIDGET_LABEL(ctop.wbas2, VALUE='Movie Speed:', UVAL='timr')
  ; Note: using this label widget for movie timer events
  ctop.wsld2 = WIDGET_SLIDER(ctop.wbas2, /SUPPRESS, VALU=100, UVAL='sld2')
  
;  WIDGET_CONTROL, ctop.wtop, SET_UVAL=w      ; no uval (yet?), all in ctop
  WIDGET_CONTROL, ctop.wtop, /REALIZE
  XMANAGER, 'sb_top', ctop.wtop ,NO_BLOCK=1
  ; save goes window #, and create initial goesplot
  WIDGET_CONTROL,wgoes,GET_VALUE=goesw       ; get goes window #
  ctop.goesw = goesw                         ; store # in common block
  sb_plotgoes,ctop.ts,ctop.te,win=ctop.goesw,pixm=ctop.goespm, $
              cursor=(*cmov.ptv)[cmov.ct],ticks=*cmov.ptv,/update ; plot

END
