
;+
; NAME:
;        sb_findhinode
; PURPOSE:
;        returns short index for files in time range
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        ii = sb_findhinode(ts,te,ds)
; INPUTS:
;        ts = start time
;        te = end time
;        ds = structure element with (at least) the following tags:
;             .obsrvtry = string.  Should start with 'H'
;             .detector = string.  'FG' or 'XRT' (for now)
;             .path = string.  if '' : use default data path,
;                              otherwise .path is interpreted as full path.
; KEYWORDS (INPUT):
; OUTPUTS:
;        ii : vector of short index structures as defined in sb_mkshortindex
; KEYWORDS (OUTPUT):
; COMMON BLOCKS:
; SIDE EFFECTS:
; RESTRICTIONS:
;
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 6/12/2007
;-

FUNCTION sb_findhinode,ts0,te0,ds

ixs0 = sb_mkshortindex()           ; empty shortindex structure element
ixs = ixs0                         ; return this if nothing found

obs = strupcase(strmid(ds[0].obsrvtry,0,1))

if obs eq 'H' then begin  ; definitely Hinode

  p   = ds[0].path

  ; some of the routines can't handle ts,te in seconds:
  tsi = anytim(ts0,/ints)
  tei = anytim(te0,/ints)

  det = strupcase(ds[0].detector)
  xrt = det eq 'XRT'

  if p eq '' then begin
    ; use catalog: first XRT then SOT FG
    sot_cat,tsi,tei,ii,ff,tcount=nii,xrt=xrt
    if ~xrt and nii gt 0 then begin
      otyp = strupcase(strcompress(ii.obs_type,/remove_all))
      if det eq 'SOT-M' then ww=where(otyp eq 'FGSHUTTEREDIANDV',nii) else $
         ww=where(otyp eq 'FG(SIMPLE)' or otyp eq 'FGSHUTTEREDIANDV',nii)
      if nii gt 0 then begin
        ii=ii[ww]
        ff=ff[ww]
        otyp=otyp[ww]
      endif
    endif
    if nii gt 0 then begin
      ; populate output shortindex
      ixs = replicate(ixs0,nii)
      ixs.naxis1 = ii.naxis1
      ixs.naxis2 = ii.naxis2
      ixs.tt = ii.anytim_dobs
      ixs.exptime = ii.exptime
      ixs.obsrvtry = 'H'
      if xrt then begin
        ixs.detector = 'XRT'
        ixs.wave_len = ii.ec_fw1_+ii.ec_fw2_
      endif else begin
        ixs.detector = 'FGIV'
        ww = where(otyp eq 'FG(SIMPLE)',nww)
        if nww gt 0 then ixs[ww].detector='FG'
        ixs.wave_len = ii.wave
      endelse
      ixs.filename = ff
    endif
    ; check if files exist
    if nii gt 0 then begin
      ww = where(file_test(ixs.filename),nii)
      if nii gt 0 then ixs=ixs[ww]
    endif

  endif else begin
    ; explicit path
    if xrt then patt='XRT*' else patt='FG*'
    ff = sb_filesearch(p,patt,timer=[tsi,tei])

    if ff[0] ne '' then begin
       ; template for fits index (just the minimum)
       if xrt then begin
         i0 = {naxis1:0,naxis2:0,exptime:0.0,instrume:'',date_obs:'', $
               ec_fw1_:'',ec_fw2_:''}
       endif else begin
         i0 = {naxis1:0,naxis2:0,exptime:0.0,instrume:'',date_obs:'', $
               wave:''}
       endelse

       ; read index for all images
       ii = sb_qmheadfits(ff,i0,timer=[tsi,tei],outt=tt)

       ; check instrume
       if xrt then begin
         ww = where(strupcase(strtrim(ii.instrume,2)) eq 'XRT',nww)
       endif else begin
         otyp = strupcase(strcompress(ii.obs_type,/remove_all))
         if det eq 'FGIV' then ww=where(otyp eq 'FGSHUTTEREDIANDV',nww) else $
            ww=where(otyp eq 'FG(SIMPLE)' or otyp eq 'FGSHUTTEREDIANDV',nww)
       endelse

       if nww gt 0 then begin
         ii = ii[ww]
         ff = ff[ww]
         tt = tt[ww]
         otyp = otyp[ww]
         ixs = replicate(ixs0,nww)
         ixs.naxis1 = ii.naxis1
         ixs.naxis2 = ii.naxis2
         ixs.tt = tt
         ixs.exptime = ii.exptime
         ixs.obsrvtry = 'H'
         ixs.detector = strupcase(ii.instrume)
         if xrt then begin
           ixs.detector = 'XRT'
           ixs.wave_len = ii.ec_fw1_+ii.ec_fw2_
         endif else begin
           ixs.detector = 'FGIV'
           ww = where(otyp eq 'FG(SIMPLE)',nww)
           if nww gt 0 then ixs[ww].detector='FG'
           ixs.wave_len = ii.wave
         endelse
         ixs.filename = ff
       endif
    endif

  endelse
endif

return,ixs

end
