
;+
; NAME:
;        sb_initcom
; PURPOSE:
;        creates and initializes common block variables for sbrowser
;        Image display for STEREO-SECCHI
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        sb_initcom
; INPUTS:
; KEYWORDS (INPUT):
; OUTPUTS:
; KEYWORDS (OUTPUT):
; COMMON BLOCKS:
;        sb_com
; SIDE EFFECTS:
; RESTRICTIONS:
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 9/21/2006
;-

PRO sb_initcom

   COMMON sb_com, ctop, cdss, cmov, cimd, ctpt
   
   maxds = 8
   maxdw = 4
   ctop = {ts:0d0,te:0d0,goespm:-1L, $
           wtop:0L,wbut1:0L,wbut2:0L,goesw:-1L,wsld1:0L,wtxt1:0L, $
           wbas2:0L,wbut3:0L,wtxt2:0L,wsld2:0L}

   cdss0 = {ptrds:ptr_new(),backds:'',path:'', $
            detector:'',obsrvtry:'',wave_len:'', $
            nx1min:0L,expmin:0.,expmax:0.}
   cdss  = replicate(cdss0,maxds,maxdw)

;  display parameters
;    rval is in stonyhurst or carrington, depending on carr flag
   disp = {naxis:[720L,720L],delt_1:150.,rpix:[360.,360.],rval:[0.,0.,0.], $
           carr:0,win:-1L,wtop:0L,obs:0}

;  color table parameters for each image layer / dataset
;    show: select this ds for display?  proj: project onto sphere?,
;    refds: ds# of observer location for projection, rundif: running diff?
;    min, max, gam: color table translation parameters float->byte,
;    lut: color table itself
   col0 = {show:1,rundif:0,proj:0,min:0.0,max:1e3,gam:1.0, $
           lut:bindgen(256)#[1B,1B,1B]}
   cimd0 = {disp:disp,col:replicate(col0,maxds)}
   cimd  = replicate(cimd0,maxdw)
   
   get_utc,tcur
   tcur.time = 0L             ; set to beginning of UTC day
   tcur = anytim(tcur)
   ctop.te = tcur+24*3.6d3
   ctop.ts = tcur-48*3.6d3
   
; new movie parameters:

  tvec = [ctop.ts]
  cmov = {ct:0L,ptv:ptr_new(tvec),pwv:ptr_new(), $
          spd:100,tdel:0d0,toff:0d0,dwn:0,dsn:0}
  ; ct:   number of currently displayed movie frame
  ; ptv:  pointer to vector of times of movie frames
  ; pwv:  pointer to (n,maxdw) array of pixmap window numbers for movie frames
  ;       if pointer valid: movie mode.  otherwise: regular mode.
  ; spd:  speed of movie
  ; tdel,toff,dwn,dsn: set of parameters for "autodefinition" of tvec.
  ;   there are two ways of defining the movie frame times:
  ;   1. equally spaced frames using tdel,toff+ctop.ts.  Use if tdel gt 0.
  ;   2. adopt image times of given dataset: dwn, dsn. Use if tdel le 0.

; tie point tool variable:

  ctpt = {pd:ptr_new(),fnam:'',numf:0L,nump:0L,ainc:0,mode:0,psym:-1, $
          col:bytarr(3)+255B,wnf:0L,wnp:0L,wtx:0L}

END
