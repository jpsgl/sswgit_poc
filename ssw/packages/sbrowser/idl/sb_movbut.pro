
;+
; NAME:
;        sb_movbut
; PURPOSE:
;        reads the state of buttons and text in the sb_mov widget,
;        updates common block vars accordingly, and updates display
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        sb_movbut,w
; INPUTS:
; KEYWORDS (INPUT):
; OUTPUTS:
; KEYWORDS (OUTPUT):
; COMMON BLOCKS: sb_com
; SIDE EFFECTS:
; RESTRICTIONS:
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 7/12/2007
;-

pro sb_movbut,w

 common sb_com

; read all the buttons, and update the common block variable cmov

   WIDGET_CONTROL,w.wtm,GET_VALUE=sval
   if sval eq 0 then begin
      ; grid mode
      WIDGET_CONTROL,w.wtd,GET_VALUE=tdel
      tdel = double(tdel)
      if tdel eq 0d0 then begin
         tdel=ctop.te-ctop.ts
         WIDGET_CONTROL,w.wtd,SET_VAL=string(tdel,f='(f8.1)')  ; write back
      endif
      WIDGET_CONTROL,w.wto,GET_VALUE=toff
      toff = double(toff)
      sb_updatcmov,tdel=tdel,toff=toff
   endif else begin
      dwn = WIDGET_INFO(w.wdw,/DROPLIST_SELECT)
      dsn = WIDGET_INFO(w.wds,/DROPLIST_SELECT)
      sb_updatcmov,tdel=0d0,dwn=dwn,dsn=dsn
   endelse

; update the goes plot
   sb_plotgoes,ctop.ts,ctop.te,win=ctop.goesw,pixm=ctop.goespm,ticks=*cmov.ptv

END
