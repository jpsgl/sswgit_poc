
;+
; NAME:
;        sb_dssbut
; PURPOSE:
;        reads the state of buttons and text in the sb_dss widget and
;        updates udss accordingly
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        sb_dssbut,udss
; INPUTS:
; KEYWORDS (INPUT):
;        /backonly : only read the info on background subtraction
; OUTPUTS:
; KEYWORDS (OUTPUT):
; COMMON BLOCKS:
; SIDE EFFECTS:
; RESTRICTIONS:
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 9/27/2006
;-

pro sb_dssbut,udss,backonly=bonly

if keyword_set(bonly) eq 0 then begin
  WIDGET_CONTROL,udss.bti,GET_VALUE=inum        ; which detector
  udss.ds.obsrvtry = udss.obs[inum]
  udss.ds.detector = udss.det[inum]
  WIDGET_CONTROL,udss.txp,GET_VALUE=path        ; file path
  udss.ds.path = path[0]
  WIDGET_CONTROL,udss.txc,GET_VALUE=wave        ; wavelength
  udss.ds.wave_len = strtrim(wave[0],2)
  WIDGET_CONTROL,udss.txe,GET_VALUE=expm        ; min exptime
  udss.ds.expmin = float(expm[0])
  WIDGET_CONTROL,udss.txs,GET_VALUE=nx1m        ; min xsize
  udss.ds.nx1min = long(nx1m[0])
endif
WIDGET_CONTROL,udss.btb,GET_VALUE=bflg        ; background subtr
IF bflg[0] eq 0 THEN udss.ds.backds='' ELSE BEGIN
  WIDGET_CONTROL,udss.txb,GET_VALUE=bfil     ; background file
  udss.ds.backds = bfil[0]
  ENDELSE

END
