
;+
; NAME:
;        sb_imdbut
; PURPOSE:
;        reads the state of buttons and text in the sb_imd widget,
;        updates common block vars accordingly, and updates display
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        sb_imdbut,w
; INPUTS:
; KEYWORDS (INPUT):
; OUTPUTS:
; KEYWORDS (OUTPUT):
; COMMON BLOCKS: sb_com
; SIDE EFFECTS:
; RESTRICTIONS:
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 10/4/2006
;-

pro sb_imdbut,w

 common sb_com

; read all the buttons into the common block variable
   oldnx = cimd[w.cdw].disp.naxis
   WIDGET_CONTROL,w.wsx,GET_VALUE=sval
   cimd[w.cdw].disp.naxis[0] = long(sval)
   WIDGET_CONTROL,w.wsy,GET_VALUE=sval
   cimd[w.cdw].disp.naxis[1] = long(sval)
   WIDGET_CONTROL,w.wdxy,GET_VALUE=sval
   cimd[w.cdw].disp.delt_1 = float(sval)
   WIDGET_CONTROL,w.wrlo,GET_VALUE=sval
   cimd[w.cdw].disp.rval[0] = float(sval)
   WIDGET_CONTROL,w.wrla,GET_VALUE=sval
   cimd[w.cdw].disp.rval[1] = float(sval)
   WIDGET_CONTROL,w.wrr,GET_VALUE=sval
   cimd[w.cdw].disp.rval[2] = float(sval)
   WIDGET_CONTROL,w.wrx,GET_VALUE=sval
   cimd[w.cdw].disp.rpix[0] = float(sval)
   WIDGET_CONTROL,w.wry,GET_VALUE=sval
   cimd[w.cdw].disp.rpix[1] = float(sval)
   WIDGET_CONTROL,w.wcar,GET_VALUE=ival
   cimd[w.cdw].disp.carr = ival
   ival = WIDGET_INFO(w.wods,/DROPLIST_SELECT)
   cimd[w.cdw].disp.obs = ival

; (re)create display widget
 wex = WIDGET_INFO(cimd[w.cdw].disp.wtop,/valid)        ; exists?
 wsne = oldnx[0] ne cimd[w.cdw].disp.naxis[0] or $      ; not same size?
        oldnx[1] ne cimd[w.cdw].disp.naxis[1]
 if wex and wsne then $                                 ; ex. & not same size
    WIDGET_CONTROL,cimd[w.cdw].disp.wtop,/DESTROY       ; destroy old
 sb_imwin,w.cdw                  ; create image window if it it doesn't exist

; prepare and display image
 sb_imbcub,w.cdw,w.pbcub,w.cds,w.pcdsim,/renew
 wset,cimd[w.cdw].disp.win
 tv,sb_byt2col(w.pbcub,cimd[w.cdw].col),true=3

END
