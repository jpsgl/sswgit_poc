
;+
; NAME:
;        sb_findtrace
; PURPOSE:
;        returns short index for files in time range
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        ii = sb_findtrace(ts,te,ds)
; INPUTS:
;        ts = start time
;        te = end time
;        ds = structure element with (at least) the following tags:
;             .detector = string.  Should start with 'T'
;             .path = string.  if '' : use default data path,
;                              otherwise .path is interpreted as full path.
; KEYWORDS (INPUT):
; OUTPUTS:
;        ii : vector of short index structures as defined in sb_mkshortindex
; KEYWORDS (OUTPUT):
; COMMON BLOCKS:
; SIDE EFFECTS:
; RESTRICTIONS:
;
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 6/12/2007
;-

FUNCTION sb_findtrace,ts0,te0,ds

ixs0 = sb_mkshortindex()           ; empty shortindex structure element
ii = ixs0                          ; return this if nothing found

det = strupcase(ds[0].detector)

if strmid(det,0,1) eq 'T' then begin  ; definitely TRACE

  p   = ds[0].path

  ; some of the routines can't handle ts,te in seconds:
  tsi = anytim(ts0,/ints)
  tei = anytim(te0,/ints)

  if p eq '' then begin
    trace_cat,tsi,tei,cat,status=status,count=nim
    if status gt 0 then begin
      trace_cat2data,cat,fcat,/filedset
      ff = strarr(nim)
      ww = where(cat.image_ss eq 0,nww)
      ww = [ww,nim]
      for j=0L,nww-1 do ff[ww[j]:ww[j+1]-1] = fcat[j]
      ; populate output structure
      ii = replicate(ixs0,nim)
      ii.naxis1 = cat.naxis1
      ii.naxis2 = cat.naxis2
      ii.tt = anytim(cat)
      ii.exptime = cat.sht_mdur
      ii.detector = 'TRACE'
      ii.wave_len = strtrim(cat.wave_len,2)
      ii.filename = ff+'#'+string(cat.image_ss,f='(i4.4)')
    endif
  endif

  ; if trace_cat didn't return anything:
  if ii[0].filename eq '' then begin
   if p eq '' then begin
      ff = trace_files(tsi,tei)
   endif else begin
      ; create file list with wildcards and proper times
      tt = [anytim(timegrid(tsi,tei,/hours)),anytim(te0)]
      ff = strmid(time2file(tt,delim='.'),0,11)  ; string & clip off minutes 
      ff = ff(uniq(ff))                          ; make it unique
      ff = concat_dir(p,'*'+ff+'*')
      ff = file_search(ff)
   endelse
   nn = n_elements(ff)
   if nn gt 0 then begin
    ; loop through files, each with possibly multiple images
    for j=0L,nn-1 do begin
      ftr = ff[j]
      read_trace,ftr,-1,itn
      nim = n_elements(itn)
      ion = replicate(ixs0,nim)
      ion.naxis1 = itn.naxis1
      ion.naxis2 = itn.naxis2
      ion.tt = anytim(itn.date_obs)
      ion.exptime = itn.sht_mdur
      ion.detector = strupcase(strtrim(itn.instrume,2))
      ion.wave_len = strtrim(itn.wave_len,2)
      ion.filename = ftr+'#'+string(indgen(nim),f='(i4.4)')
      if ii[0].filename eq '' then ii=ion else ii=[ii,ion]
    endfor
   endif
  endif

endif

return,ii

end
