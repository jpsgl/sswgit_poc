
;+
; NAME:
;        sb_plot3d
; PURPOSE:
;        draws one or more "features" onto display window win.
;        Usse the common block variables of sbrowser (see sb_initcom.pro)
;        Image display for STEREO-SECCHI
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        sb_plot3d,feature=feature
;        sb_plot3d,/all
; INPUTS:
; KEYWORDS (INPUT):
;        feature = feature # to be plotted.  Assumes /all if not present
;        /all : plot all features (overrides feature keyword)
; OUTPUTS:
; KEYWORDS (OUTPUT):
; COMMON BLOCKS:
;        None
; SIDE EFFECTS:
; RESTRICTIONS:
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 11/05/2007
;-

pro sb_plot3d,feature=feat,all=all

common sb_com

if ~ptr_valid(ctpt.pd) then return  ; data ptr?

maxdw = n_elements(cimd)
if n_elements(feat) eq 0 || keyword_set(all) then begin
   ww = where((*ctpt.pd).stat gt 0,nww)
   if nww eq 0 then return
   feat = (*ctpt.pd)[ww].numf
   feat = feat[uniq(feat,sort(feat))]   ; unique features
endif
nfeat = n_elements(feat)

; for each feature find all relevant times
ipts = intarr(n_elements(*ctpt.pd))             ; index to selected points
for j=0L,nfeat-1L do begin
  ; simple logic: only plot times with exact match numt eq cmov.ct or feat lt 0
  if feat[j] lt 0L then  w1=where((*ctpt.pd).numf eq feat[j],nw1) else $
     w1=where((*ctpt.pd).numf eq feat[j] and (*ctpt.pd).numt eq cmov.ct,nw1)
  if nw1 gt 0L then ipts[w1] = 1
endfor
w1 = where(ipts,npts)
if npts gt 0 then pts=(*ctpt.pd)[w1] else return

; loop through all display windows:
for cdw=0,maxdw-1 do begin

   ; check if this display and this view exist, and are selected:
   if WIDGET_INFO(cimd[cdw].disp.wtop,/valid) then begin
      ww = where(cimd[cdw].col.show eq 1 and ptr_valid(cdss[*,cdw].ptrds),nww)
      if nww gt 0 then go=ptr_valid(cdss[cimd[cdw].disp.obs,cdw].ptrds) $
                  else go=0
   endif else go=0

   ; now plot data:
   if go then begin
     ; get proper wcs
     jlr=cimd[cdw].disp.obs
     tc = (*cmov.ptv)[cmov.ct]                       ; current time
     tt  = (*cdss[jlr,cdw].ptrds).tt                 ; ds times
     dmy = min(abs(tt-tc),s0)                        ; time closest to tc
     fnam=(*cdss[jlr,cdw].ptrds)[s0].filename
     sb_readfits,fnam,oii
     owcs = fitshead2wcs(oii)
     dwcs = sb_wcs(disp=cimd[cdw].disp,obswcs=owcs)
     ; plot each feature
     for j=0L,nfeat-1L do begin
       ; first plot points with stat gt 1, then stat eq 1
       ww=where(pts.numf eq feat[j] and pts.stat gt 1,nww)
       if nww gt 0 then begin
         pp = pts[ww]
         wset,cimd[cdw].disp.win
         sb_plot1feature,pp.rval,dwcs,car=pp.carr,col=pp[0].col,psy=pp[0].psym
       endif
       ww=where(pts.numf eq feat[j] and pts.stat eq 1 and pts.dwn eq cdw,nww)
       if nww gt 0 then begin
         pp = pts[ww]
         wset,cimd[cdw].disp.win
         plots,pp.xy[0],pp.xy[1],/device,psym=1,col=(255L*256L+255L)*256L+255L
       endif
     endfor
   endif
endfor

end
