
function sb_getimagecom,cdw,cds

;+
; $Id$
;
; Project : STEREO SECCHI
;
; Name    : sb_getimagecom
;
; Purpose : extracts appropriate parameters from common block variables
;           and then calls sb_getimage
;
; Use     : IDL> sb_getimagecom,cdw,cds
;
; Inputs  :
;     cdw = display window number to use (index into common block variables)
;     cds = display dataset number to use (index into common block variables)
;
; Outputs :
;
; Keywords:
;
; Common  : sb_com
;
; Restrictions:
;    No error checking is done.
;
; Side effects:
;
; Category    : Image processing
;
; Prev. Hist. :
;
; Written     : Jean-Pierre Wuelser, LMSAL, 9-Oct-2006
;
; $Log$
;-

common sb_com

if ptr_valid(cdss[cds,cdw].ptrds) then begin

  ; prepare for looping through datasets
  disp = cimd[cdw].disp
  col = cimd[cdw].col[cds]
  tc = (*cmov.ptv)[cmov.ct]          ; current time, i.e., time of display
  tt  = (*cdss[cds,cdw].ptrds).tt
  dmy = min(abs(tt-tc),s0)                            ; time closest to tc
  file = (*cdss[cds,cdw].ptrds)[s0].filename          ; image file name
  bfil = cdss[cds,cdw].backds                         ; backgound file
  if col.rundif eq 1 then begin
     if cmov.ct ge 1 then t_1 = (*cmov.ptv)[cmov.ct-1L] $
        else t_1 = (*cmov.ptv)[n_elements(*cmov.ptv)-1L]  ; time for rundif
     dmy = min(abs(tt-t_1),s1)
     dfil = (*cdss[cds,cdw].ptrds)[s1].filename       ; rundif file
  endif else dfil=''
  if col.proj eq 1 then begin
     refds=disp.obs
     ttr = (*cdss[refds,cdw].ptrds).tt
     dmy = min(abs(ttr-tc),s2)
     rfil = (*cdss[refds,cdw].ptrds)[s2].filename     ; ref.image for proj.
  endif else rfil=''
  if strupcase(cdss[cds,cdw].detector) eq 'SOT-M' then nx3=1
  return, sb_getimage(file,disp,back=bfil,rundif=dfil,proj=rfil,naxis3=nx3)

endif else return,-1

end
