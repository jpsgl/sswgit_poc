
;+
; NAME:
;        sb_plotgoes
; PURPOSE:
;        (re)draws goes plot in goes window of top widget for go_secchi
;        Image display for STEREO-SECCHI
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        sb_plotgoes,ts,te,win=win,pixmap=pixmap,/update   ; generate plot
;        sb_plotgoes,ts,te,win=win,pixmap=pixmap,cursor=tc ; copy plot from
;                                              ; backup area and add cursor
; INPUTS:
;        ts = dbl. start time in seconds (as output by anytim)
;        te = dbl. end time in seconds (as output by anytim)
; KEYWORDS (INPUT):
;        win = window number of plot area.  Required!
;        pixmap = window number of backup area for plot.  Required!
;        /update : re-plot in both backup area and window.  If not set,
;                  copy plot from the backup area to the plot area.
;                  (cursor and tick marks are always regenerated, however)
;        cursor = dbl.  time for a cursor mark.  (in plot window only)
;        ticks = dblarr.  vector of times for small tick marks (plot win only)
; OUTPUTS:
; KEYWORDS (OUTPUT):
; COMMON BLOCKS:
;        None
; SIDE EFFECTS:
; RESTRICTIONS:
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 8/26/2005
;        JPW, 5/24/2007  add blank plot if GOES unavailable
;        JPW, 6/27/2007  add /ascii to plot_goes (temporary fix)
;-

PRO sb_plotgoes,ts,te,win=win,pixmap=pixmap,cursor=tc,ticks=tt, $
                    update=update
  psav = !p
  wsav = !d.window
  !p.background = !d.n_colors-1
  !p.color = 0
  !p.multi = 0
  ; update goes plot in pixmap
  IF keyword_set(update) THEN BEGIN
    wset,pixmap
    plot_goes,anytim(ts,/int),anytim(te,/int),/timerange,status=pgstat, $
              gcolor=intarr(6),color=0,xmargin=[0,0],/quiet,/low,/ascii
    if pgstat eq 0 then begin
      tran = [anytim(ts,/int),anytim(te,/int)]
      utplot,tran,[1,1],/nodata,xsty=1,xmargin=[0,0],timerange=tran, $
             yminor=1,yticks=1
    endif
    ; draw tick marks for selected time points
    IF keyword_set(tt) THEN BEGIN
      ttx = (tt-ts)/(te-ts)
      FOR j=0L,n_elements(tt)-1 DO $
         plots,[ttx[j],ttx[j]],[.27,.44],/norm,color=0
    ENDIF
  ENDIF
  ; copy pixmap
  wset,win
  device,copy=[0,0,!d.x_size,!d.y_size,0,0,pixmap]
  ; draw time cursor
  IF keyword_set(tc) THEN IF tc ge ts and tc le te THEN BEGIN
     tcx = (tc-ts)/(te-ts)
     plots,[tcx,tcx],[0.,1.],/normal,color=0
  ENDIF
  ; draw tick marks for selected time points
  IF keyword_set(tt) THEN BEGIN
    ttx = (tt-ts)/(te-ts)
    FOR j=0L,n_elements(tt)-1 DO plots,[ttx[j],ttx[j]],[.27,.44],/norm,color=0
  ENDIF
  ; restore plotvariables
  !p = psav
  wset,wsav
END

