
pro sb_imdlut,w,evalue,names=names,getnamesonly=get

;+
; $Id$
;
; Project : STEREO SECCHI
;
; Name    : sb_imdlut
;
; Purpose : initializes and updates the colorbars and lut droplist menu
;           in sb_imd.
;
; Use     : IDL> sb_imdlut,w                ;  initialize colorbars
;                sb_imdlut,names=lutlbls,/getnamesonly   ; get lut names
;                sb_imdlut,w,ev.value       ;  update current lut & colorbar
;
; Inputs  :
;    w = user variable of the sb_imd widget
;    evalue = number of the color table to be used, as returned by a
;             lut selection droplist event.
;             Only update the color table of the currently selected
;             dataset (i.e., w.cds)
;
; Outputs :
;
; Keywords:
;    /getnamesonly : if set, only return names of color tables
;    names = strarr.  Names of color tables (output).  Use with /getnamesonly
;
; Common  : sb_com
;
; Restrictions: No error checking is performed!
;
; Side effects:
;
; Category    : image processing, widgets
;
; Prev. Hist. :
;
; Written     : Jean-Pierre Wuelser, LMSAL, 4-Oct-2006
;
; $Log$
;-

common sb_com

; look for color table file
; eventually update sb_colors1.tbl with modifyct.pro
;flut = file_search(concat_dir('$GO_SECCHI_AUX','sb_colors1.tbl'))
flut = file_which('.:'+!path,'sb_colors1.tbl')  ; look in current path

if keyword_set(get) then begin                ; only get color table names
   names = ''
   loadct,file=flut,get_names=names
endif else begin
  lut=bytarr(256,3,w.barys)                   ; image, colors row-interleaved
  if n_elements(evalue) ne 1 then begin       ; initialize all color bars
    maxds = n_elements(w.wbar)
    for i=0,maxds-1 do begin
      luti = cimd[w.cdw].col[i].lut           ; color table from common block
      for j=0,w.barys-1 do lut[0,0,j] = luti  ; copy into true color image
      WIDGET_CONTROL,w.wbar[i],GET_VALUE=win  ; window #
      wset,win
      tv,lut,true=2                           ; copy image into bar window
    endfor
  endif else begin                            ; update color table # cds
    n_tab = 0B
    luti = lut[*,*,0]                         ; init variable for 1 lut
    openr,lun,flut,/get_lun
    readu,lun,n_tab                           ; read number of luts in file
    for i=0,evalue do readu,lun,luti          ; read the desired lut
    free_lun,lun
    cimd[w.cdw].col[w.cds].lut = luti         ; copy lut into common block
    for j=0,w.barys-1 do lut[0,0,j] = luti    ; copy into true color image
    WIDGET_CONTROL,w.wbar[w.cds],GET_VALUE=win  ; window #
    wset,win
    tv,lut,true=2                             ; copy image into bar window
  endelse
endelse

end
