;+
; NAME:
;        sb_sohoposit
; PURPOSE:
;        Estimates wcs.position.hgln_obs if soho and pos_assumed true.
;        This routine is called from sb_wcs, and improves the results
;        at ssw sites that don't have the SOHO orbit files.
;        Important: the input WCS must already have hgln_obs and it
;        must be 0, otherwise nothing is changed
; CATEGORY:
;        image processing
; CALLING SEQUENCE:
;        sb_sohoposit,wcs
; INPUT/OUTPUT:
;        wcs : input WCS structure (usually from call to fitshead2wcs)
; KEYWORDS (INPUT):
; KEYWORDS (OUTPUT):
; COMMON BLOCKS:
; SIDE EFFECTS:
; RESTRICTIONS:
;        Only hgln_obs is improved.  But this should reduce co-alignment
;        errors due to SOHO position uncertainties to about 1 arcsec
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 9/1/2006
;-

pro sb_sohoposit,wcs

p = wcs.position
if tag_exist(p,'hgln_obs') and p.soho and p.pos_assumed then $
 if p.hgln_obs eq 0.0 then begin
    ;    a significant improvement is achieved by setting
    ;    wcs.position.hgln_obs = 0.257*sin(2.0*!pi*(t-t0)/per)
    ;    with t0 = '2006-03-10T12:00:00' and per = 177.75 days
    ;    (hglt_obs set to b0 at Earth is good to about 1 arcsec)
    t   = anytim(wcs_get_time(wcs))
    t0  = anytim('2006-03-10T12:00:00')
    per = 177.75 * 24.0 * 3600.0
    wcs.position.hgln_obs = 0.257*sin(2.0*!pi*(t-t0)/per)
endif

end
