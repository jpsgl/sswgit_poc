
function sb_secchifiles,t0,t1,obsrvtry=o0,detector=tel0,path=path0, $
                         source=source,type=type,swx=swx
;+
; $Id$
;
; Project : STEREO SECCHI
;
; Name    : sb_secchifiles
;
; Purpose : return online secchi file names
;
; Use     : IDL> files = sb_secchifiles(t0,t1,obs='A',det='euvi')
;
; Inputs  :
;    t0 = start time.  any format accepted by "anytim".
;    t1 = end time.    any format accepted by "anytim".
;
; Outputs :
;    f = string vector of file names.
;
; Keywords (required):
;    obsrvtry = 'a' or 'b'.  Required!
;    detector = 'cor1', 'cor2', 'euvi', 'hi_1', or 'hi_2'.  Required!
;
; Keywords (optional):
;    source = 'lz', 'pb', or 'rt'.  Default: 'lz'
;    type   = 'img', 'seq', or 'cal'.  Default: 'img'
;    path   = scalar string.  Images are either in path/ or path/yyyymmdd/
;    /swx   : space weather images only.  Default: ignore swx images.
;
; Common  : None
;
; Restrictions: t0 and t1 must both exist and be scalar
;               (i.e., matchfiles logic not (yet) implemented)
; Side effects:
;
; Category    : Image handling
;
; Prev. Hist. :
;
; Written     : Jean-Pierre Wuelser, LMSAL, 15-Sep-2006
;
; $Log$
;-

; set defaults and enforce some formats
ts = anytim(t0)
te = anytim(t1)
if n_elements(o0) eq 0 then o='A' else o=strupcase(strmid(o0[0],0,1))
if o ne 'B' then o='A'
if n_elements(tel0) eq 0 then tel='euvi' else tel=strlowcase(tel0[0])
if n_elements(type) ne 1 then type='img' else type=strlowcase(type[0])
if n_elements(path0) eq 0 then path='' else path=path0[0]

; create date sub-paths:
subdirs = time2file([anytim(timegrid(ts,te,/days)),te],/date_only)
subdirs = subdirs(uniq(subdirs))

if path eq '' then begin
   ; get the standard secchi archive path
   path = scc_data_path(o,tel=tel,source=source,type=type)
endif else begin
   ; in the case of an explicit path, the data may be either
   ; - in a subdirectory, - or in the directory itself:
   subdirs = [subdirs,'']
endelse

dirs = concat_dir(path,subdirs)

; create file search pattern
if strmid(tel,0,1) eq 'e' then patt='eu' else begin
   patt = strmid(tel,0,1)
   if strpos(tel,'2') ge 0 then patt=patt+'2' else patt=patt+'1'
endelse
patt = '*'+patt+o+'.fts'

ff = file_search(concat_dir(dirs,patt))
if ff[0] ne '' then begin
   tt = file2time(ff,out_style='sec')
   nn = strmid(ff,7,1,/reverse)       ; is '7' for swx images
   if keyword_set(swx) $
     then ii=where(tt ge ts and tt le te and nn eq '7') $
     else ii=where(tt ge ts and tt le te and nn ne '7')
   if ii[0] ge 0 then ff = ff[ii] else ff = ''
endif

return,ff

end
