
;+
; NAME:
;        sb_findsecchi
; PURPOSE:
;        returns short index for files in time range
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        ii = sb_findsecchi(ts,te,ds)
; INPUTS:
;        ts = start time
;        te = end time
;        ds = structure element with (at least) the following tags:
;             .obsrvtry = string.  Should start with 'A' or 'B'
;             .detector = string.  'EUVI','COR1','COR2','HI_1',HI_2'
;             .path = string.  if '' or 'lz': use default lz data path,
;                              if 'pb', 'rt' use corresp. default path,
;                              otherwise .path is interpreted as full path.
; KEYWORDS (INPUT):
; OUTPUTS:
;        ii : vector of short index structures as defined in sb_mkshortindex
; KEYWORDS (OUTPUT):
; COMMON BLOCKS:
; SIDE EFFECTS:
; RESTRICTIONS:
;
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 6/12/2007
;-

FUNCTION sb_findsecchi,ts0,te0,ds

ixs0 = sb_mkshortindex()           ; empty shortindex structure element
ixs = ixs0                         ; return this if nothing found

obs = strupcase(strmid(ds[0].obsrvtry,0,1))

if obs eq 'A' or obs eq 'B' then begin  ; definitely SECCHI

  ; check path for string with special meaning: lz, pb, rt
  p   = ds[0].path
  if p eq 'lz' or p eq 'pb' or p eq 'rt' then begin
    src = p
    p = ''
  endif

  det = strupcase(ds[0].detector)

  if p eq '' then begin
    ; use summary files
    date = [anytim(ts0,/ccsds),anytim(te0,/ccsds)]
    if strmid(det,0,1) eq 'C' then begin
      ; Coronagraph, check both seq and img
      ii = scc_read_summary(date=date,spacecraft=obs,tel=det, $
                            source=src,/nobeacon)
      ww = where(ii.nmiss le 0 and (ii.type eq 'img' or $
                 (ii.type eq 'seq' and ii.value lt 30.)),nii)
    endif else begin
      ii = scc_read_summary(date=date,spacecraft=obs,tel=det, $
                            source=src,/nobeacon,type='img')
      ww = where(ii.nmiss le 0,nii)
    endelse
    if nii gt 0 then begin
      ii=ii[ww]
      ; populate output shortindex
      ixs = replicate(ixs0,nii)
      ixs.naxis1 = ii.xsize
      ixs.naxis2 = ii.ysize
      ixs.tt = file2time(ii.filename,out_style='sec')  ; not date_obs!
      ixs.exptime = ii.exptime
      ixs.obsrvtry = ii.spacecraft
      ixs.detector = ii.telescope
      ixs.wave_len = strtrim(fix(ii.value),2)
      ; create path: first add yyyymmdd, then path, then update for 'seq' path
      ff = ii.filename
      ff = concat_dir(strmid(ff,0,8),ff)
      path = scc_data_path(obs,tel=det,source=src,type='img')
      ixs.filename = concat_dir(path,ff)
      if strmid(det,0,1) eq 'C' then begin
        ww = where(ii.type eq 'seq',nww)
        path = scc_data_path(obs,tel=det,source=src,type='seq')
        if nww gt 0 then ixs[ww].filename = concat_dir(path,ff[ww])
      endif
      ; check if files exist
      ww = where(file_test(ixs.filename),nii)
      if nii gt 0 then ixs=ixs[ww] else ixs=ixs0
    endif

  endif else begin
    ; explicit path

    ; create file search pattern
    if strmid(det,0,1) eq 'E' then patt='eu' else begin
       patt = strlowcase(strmid(det,0,1))
       if strpos(det,'2') ge 0 then patt=patt+'2' else patt=patt+'1'
    endelse
    patt = '*'+patt+obs+'*.fts'

    ; search for files
    ff = sb_filesearch(p,patt,timer=[anytim(ts0),anytim(te0)],outt=tt)

    if ff[0] ne '' then begin
       ; select non-SWX files
       ww = where(strmid(ff,7,1,/reverse) ne '7',nww)   ; is '7' for SW images
       if nww gt 0 then begin
         ff = ff[ww]
         tt = tt[ww]
         ; template for fits index (just the minimum, we do have tt)
         i0 = {naxis1:0,naxis2:0,exptime:0.0,wavelnth:0,polar:0.0, $
               obsrvtry:'',detector:'',nmissing:0}

         ; read index for all images
         ii = sb_qmheadfits(ff,i0)
         ii.obsrvtry = strupcase(strmid(ii.obsrvtry,7,1))
         ii.detector = strupcase(strtrim(ii.detector,2))
         ww = where(ii.nmissing le 0 and ii.obsrvtry eq obs and $
                    ii.detector eq det,nww)
         if nww gt 0 then begin
           ii = ii[ww]
           ff = ff[ww]
           tt = tt[ww]
           ixs = replicate(ixs0,nww)
           ixs.naxis1 = ii.naxis1
           ixs.naxis2 = ii.naxis2
           ixs.tt = tt              ; date_obs wouldn't have light travel time
           ixs.exptime = ii.exptime
           ixs.obsrvtry = obs
           ixs.detector = det
           if strmid(det,0,1) eq 'C' then ixs.wave_len=strtrim(fix(ii.polar),2)
           if strmid(det,0,1) eq 'E' then ixs.wave_len=strtrim(ii.wavelnth,2)
           ixs.filename = ff
         endif
       endif
    endif

  endelse
endif

return,ixs

end
