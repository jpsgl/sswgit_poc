
pro sb_imdupdat,w,dsvec,auto=auto

;+
; $Id$
;
; Project : STEREO SECCHI
;
; Name    : sb_imdupdat
;
; Purpose : updates appropriate portions of bcub, and redisplays image.
;           called from within sb_imd.
;
; Use     : IDL> sb_imdupdat,w,dsvec
;
; Inputs  :
;    w = user variable of the sb_imd widget
;    dsvec = vector of image planes that need updating
;            if omitted, then plane [w.cds] is updated using w.pcdsim
;    /auto = update using autoscale.  only with dsvec omitted!
; Outputs :
;
; Keywords:
;
; Common  : sb_com
;
; Restrictions: No error checking is performed!
;
; Side effects:
;
; Category    : image processing, widgets
;
; Prev. Hist. :
;
; Written     : Jean-Pierre Wuelser, LMSAL, 4-Oct-2006
;
; $Log$
;-

common sb_com

if n_params() lt 2 then begin 
  dsvec = [w.cds]
  updat = 1
endif else updat = 0

if dsvec[0] ge 0 then begin
  for i=0,n_elements(dsvec)-1 do begin                ; loop through dsvec
    if dsvec[i] eq w.cds $                            ; check if i eq cds
      then sb_imbcub,w.cdw,w.pbcub,w.cds,w.pcdsim, $  ; update bcub, cdsim
                     update_only=updat,auto=auto $    ; opt. /update,/auto
      else sb_imbcub,w.cdw,w.pbcub,dsvec[i]           ; update bcub
  endfor
  ; update displayed image if display exists:
  if WIDGET_INFO(cimd[w.cdw].disp.wtop,/valid) then begin
    wset,cimd[w.cdw].disp.win
    tv,sb_byt2col(w.pbcub,cimd[w.cdw].col),true=3
  endif
endif

end
