
;+
; NAME:
;        sb_findsoho
; PURPOSE:
;        returns short index for files in time range
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        ii = sb_findsoho(ts,te,ds)
; INPUTS:
;        ts = start time
;        te = end time
;        ds = structure element with (at least) the following tags:
;             .obsrvtry = string.  Should start with 'S'
;             .detector = string.  'EIT','C2','C3','MDI-W','MDI-M'
;             .path = string.  if '' or 'lz': use default lz data path,
;                              if 'ql' use corresp. default path,
;                              otherwise .path is interpreted as full path.
; KEYWORDS (INPUT):
; OUTPUTS:
;        ii : vector of short index structures as defined in sb_mkshortindex
; KEYWORDS (OUTPUT):
; COMMON BLOCKS:
; SIDE EFFECTS:
; RESTRICTIONS:
;
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 6/12/2007
;-

FUNCTION sb_findsoho,ts0,te0,ds

ixs0 = sb_mkshortindex()           ; empty shortindex structure element
ion = ixs0                         ; return this if nothing found

obs = strupcase(strmid(ds[0].obsrvtry,0,1))

if obs eq 'S' then begin  ; definitely SOHO

   ; check path for string with special meaning: lz, ql
   p   = ds[0].path
   if p eq 'lz' then p = ''           ; lz is default
   if p eq 'ql' then begin
      ql = 1
      p = ''
   endif

   ; some of the routines can't handle ts,te in seconds:
   tsi = anytim(ts0,/ints)
   tei = anytim(te0,/ints)

   det = strupcase(ds[0].detector)

   if det eq 'EIT' and p eq '' then begin
      ; EIT
      ff = eit_files(tsi,tei,quicklook=ql)
   endif
   if det eq 'MDI-M' and p eq '' then begin
      ; MDI magnetograms
      ff = mdi_time2file(tsi,tei,/local,/flat,/month,paths_prepend='m')
   endif
   if (det eq 'C2' or det eq 'C3') and p eq '' then begin
      ; LASCO
      if keyword_set(ql) then path='$QL_IMG' else path='$LZ_IMG'
      path = concat_dir(path,'level_05')
      patt = concat_dir(strlowcase(det),'*.fts')
      ff = sb_filesearch(path,patt,timer=[tsi,tei],/year2digit)
   endif
   ; if nothing found, search directly in path:
   if ff[0] eq '' and p ne '' then ff=sb_filesearch(p,'*')

   if ff[0] ne '' then begin
    ; index template:
    it0={naxis1: 0L, naxis2: 0L,        $
         date_obs:'',                   $
         date_d$obs:'', time_d$obs:'',  $    ; LASCO+mreadfits workaround
         instrume: '', detector: '',    $
         wavelnth: 0,                   $
       ; obs_type: '',                  $    ; MDI vis. or mag.
         exptime: 0.0}
    ii = sb_qmheadfits(ff,it0,timer=[tsi,tei],outt=tt)
    ii.detector = strupcase(strmid(ii.detector,0,2))
    ii.instrume = strupcase(strmid(ii.instrume,0,3))
    ; select proper instrument, separate logic for C2,3 vs. EIT,MDI
    if strmid(det,0,1) eq 'C' then begin                    ; C2 or C3
      ww = where(ii.detector eq strmid(det,0,2),nww)
    endif else begin                                        ; EIT or MDI
      ww = where(ii.instrume eq strmid(det,0,3),nww)
    endelse

    if nww gt 0 then begin
      ff = ff[ww]
      ii = ii[ww]
      tt = tt[ww]
      ; output structure:
      ion = replicate(ixs0,nww)

      ; fix up various keywords and populate output structure
      ion.naxis1 = ii.naxis1
      ion.naxis2 = ii.naxis2
      ion.obsrvtry = 'S'
      if strmid(det,0,1) eq 'C' $             ; C2 or C3, otherwise EIT or MDI
        then ion.detector = ii.detector $
        else ion.detector = ii.instrume
      if strmid(det,0,3) eq 'EIT' $
        then ion.wave_len = strtrim(ii.wavelnth,2)
    ; if strmid(det,0,3) eq 'MDI' $
    ;   then ion.wave_len = strupcase(strmid(ii.obs_type,0,1))
    ; ion.obs_type = strupcase(ii.obs_type)
      ion.tt = tt
      ion.exptime = ii.exptime
      ion.filename = ff
    endif
  endif
endif

return,ion

end
