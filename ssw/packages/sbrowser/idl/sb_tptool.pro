
;+
; NAME:
;        sb_tptool
; PURPOSE:
;        creation and event handling of tie point tool widget for go_secchi
;        Image display for STEREO-SECCHI
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        sb_tptool
; INPUTS:
; KEYWORDS (INPUT):
; OUTPUTS:
; KEYWORDS (OUTPUT):
; COMMON BLOCKS:
;        sb_com
; SIDE EFFECTS:
; RESTRICTIONS:
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 2007-Oct-04
;-

pro sb_tptool_event, ev

  COMMON sb_com
  WIDGET_CONTROL, ev.top, GET_UVALUE=w
  WIDGET_CONTROL, ev.id,  GET_UVALUE=uval
  case uval of
  'bact': case ev.value of
          ; load, save, and quit buttons
          0 : begin
                ; load an existing tie point file
                WIDGET_CONTROL,w.wfn,GET_VALUE=fnam
                if file_test(fnam) then begin
                  restore,filename=fnam
                  ptr_free,ctpt.pd
                  ctpt.pd = ptr_new(tpt_data)
                  ctpt.fnam = fnam
                  ; calculate proper .numt tags
                  sb_tt2numt
                  ; redisplay image, then plot overlays
                  sb_allimupdat
                  sb_plot3d
                endif
              end
          1 : begin
                ; save the tie points to file
                if ptr_valid(ctpt.pd) then begin
                  WIDGET_CONTROL,w.wfn,GET_VALUE=fnam
                  tpt_data = (*ctpt.pd)
                  save,tpt_data,filename=fnam
                  ctpt.fnam = fnam
                endif
              end
          2 : begin
                ; Quit (no save)
                ; destroying this widget disables response in image displays 
                WIDGET_CONTROL, ev.top, /DESTROY     ; destroy this widget
                WIDGET_CONTROL, ctop.wbut1,SENSITIVE=1  ; turn features back on
                WIDGET_CONTROL, ctop.wbut2,SENSITIVE=1
              end
          endcase
  'incf': begin
            ; modify feature #
            if ptr_valid(ctpt.pd) then mxf=max((*ctpt.pd).numf)+1L else mxf=0L
            case ev.value of
            0 : ctpt.numf = 0L                         ; set numf to 0
            1 : ctpt.numf = mxf                        ; set numf to max+1
            2 : ctpt.numf = (ctpt.numf-1L) > 0L        ; decrement numf
            3 : ctpt.numf = (ctpt.numf+1L) < mxf       ; increment numf
            endcase
            WIDGET_CONTROL, ctpt.wnf, SET_VALUE=string(ctpt.numf,f='(i5)')
          end
  'incp': begin
            ; modify spatial point #
            if ptr_valid(ctpt.pd) then begin
               ww = where((*ctpt.pd).numf eq ctpt.numf,nww)
               if nww gt 0 then mxp=max((*ctpt.pd)[ww].nump)+1L else mxp=0L
            endif else mxp=0L
            case ev.value of
            0 : ctpt.nump = 0L                         ; set nump to 0
            1 : ctpt.nump = mxp                        ; set nump to max+1
            2 : ctpt.nump = (ctpt.nump-1L) > 0L        ; decrement nump
            3 : ctpt.nump = (ctpt.nump+1L) < mxp       ; increment nump
            endcase
            WIDGET_CONTROL, ctpt.wnp, SET_VALUE=string(ctpt.nump,f='(i5)')
          end
  'delp': begin
            ; delete current point from data (and display)
            if ptr_valid(ctpt.pd) then begin
              ww = where((*ctpt.pd).numf eq ctpt.numf and $
                         (*ctpt.pd).nump eq ctpt.nump and $
                         (*ctpt.pd).numt eq cmov.ct and $
                         (*ctpt.pd).stat gt 0,nww)
              if nww gt 0 then begin
                ww3 = where((*ctpt.pd)[ww].stat ge 3,nww3)
                (*ctpt.pd)[ww].stat = 0     ; set points to undefined
                ; do not delete xy values for points w/valid xy AND valid rval
                if nww3 gt 0 then (*ctpt.pd)[ww[ww3]].stat = 1
                ; redisplay windows and replot all features
                sb_allimupdat
                sb_plot3d
              endif
            endif
          end
  'binc': begin
            ; update autoincrement common block variable
            ctpt.ainc = ev.value
          end
  'bmod': begin
            ; update autoincrement mode common block variable
            ctpt.mode = ev.value
          end
  'upda': begin
            ; update psym and color of feature
            WIDGET_CONTROL,w.wsy,GET_VALUE=sval
            ctpt.psym = fix(sval[0])
            WIDGET_CONTROL,w.wcr,GET_VALUE=sval
            ctpt.col[0] = fix(sval[0])
            WIDGET_CONTROL,w.wcg,GET_VALUE=sval
            ctpt.col[1] = fix(sval[0])
            WIDGET_CONTROL,w.wcb,GET_VALUE=sval
            ctpt.col[2] = fix(sval[0])
            if ptr_valid(ctpt.pd) then begin
              ww = where((*ctpt.pd).numf eq ctpt.numf and $
                         (*ctpt.pd).stat gt 0,nww)
              if nww gt 0 then begin
                (*ctpt.pd)[ww].psym = ctpt.psym
                (*ctpt.pd)[ww].col[0] = ctpt.col[0]
                (*ctpt.pd)[ww].col[1] = ctpt.col[1]
                (*ctpt.pd)[ww].col[2] = ctpt.col[2]
                ; replot feature
                  ; only redraw feature graph
                  sb_plot3d,feat=ctpt.numf
              endif
            endif
          end
  else  :
  endcase

  ; Other things are handled in the display window event handlers.

end


pro sb_tptool

  common sb_com

  ; Note: there will be a need for common block variables,
  ;       in particular to communicate with event handlers from
  ;       the image display windows, and to save parameter settings.
  ;       None are implemented yet!

  ; some variables:
  bactt = ['Load Tie Points','Save Tie Points','Quit (no save)']
  binct = ['Off','Point#','Time']
  bmodt = ['1st view','2nd view']

  ; the sb_tptool user variable:
  w = {wfn:0L,wsy:0L,wcr:0L,wcg:0L,wcb:0L}
  
  ; the widget:

  base   = WIDGET_BASE(/COLUMN, GROUP_LEADER=ctop.wtop,XOFFSET=60, $
           YOFFSET=420,TITLE='Tie Point Tool')
  ; loading and saving tie point data with path/filename
  wdmy  = CW_BGROUP(base,bactt,/ROW,UVAL='bact')
  bas1  = WIDGET_BASE(base, /ROW)
  wdmy  = WIDGET_LABEL(bas1, VALUE='   Filename:')
  w.wfn = WIDGET_TEXT(bas1, VALUE=ctpt.fnam, /EDITABLE, UVAL='path', XSIZE=72)

  ; feature#, spatial point#, incr. buttons each
  bas2  = WIDGET_BASE(base, /ROW)
  wdmy  = WIDGET_LABEL(bas2, VALUE='Feature #')
  ctpt.wnf = WIDGET_TEXT(bas2, VALUE=string(ctpt.numf,f='(i5)'), $
                      UVAL='numf', XSIZE=5)
  wdmy  = CW_BGROUP(bas2,['0','top','-1','+1'],/ROW,UVAL='incf')
  wdmy  = WIDGET_LABEL(bas2, VALUE='   Point #')
  ctpt.wnp = WIDGET_TEXT(bas2, VALUE=string(ctpt.nump,f='(i5)'), $
                      UVAL='nums', XSIZE=5)
  wdmy  = CW_BGROUP(bas2,['0','top','-1','+1'],/ROW,UVAL='incp')
  wdmy  = CW_BGROUP(bas2,['Delete Point'],/ROW,UVAL='delp')

  ; Autoincrement (none, point#, time)
  bas3  = WIDGET_BASE(base, /ROW)
  wdmy  = WIDGET_LABEL(bas3, VALUE='Autoincrement:')
  wdmy  = CW_BGROUP(bas3,binct,/ROW,UVAL='binc',/EXCLUSIVE,/NO_REL, $
                    SET_VAL=ctpt.ainc)
  ; Autoincrement mode (after 1 click, 2 clicks)
  wdmy  = WIDGET_LABEL(bas3, VALUE='  after clicking point in:')
  wdmy  = CW_BGROUP(bas3,bmodt,/ROW,UVAL='bmod',/EXCLUSIVE,/NO_REL, $
                    SET_VAL=ctpt.mode)

  ; symbol/line_noline, color
  bas5  = WIDGET_BASE(base, /ROW)
  wdmy  = WIDGET_LABEL(bas5, VALUE='Feature PSYM =')
  w.wsy = WIDGET_TEXT(bas5, VALUE=string(ctpt.psym,f='(i3)'), /EDITABLE, $
                      UVAL='psym', XSIZE=3)
  wdmy  = WIDGET_LABEL(bas5, VALUE=' Color (RGB) =')
  w.wcr = WIDGET_TEXT(bas5, VALUE=string(ctpt.col[0],f='(i3)'), /EDITABLE, $
                      UVAL='pcor', XSIZE=3)
  w.wcg = WIDGET_TEXT(bas5, VALUE=string(ctpt.col[1],f='(i3)'), /EDITABLE, $
                      UVAL='pcog', XSIZE=3)
  w.wcb = WIDGET_TEXT(bas5, VALUE=string(ctpt.col[2],f='(i3)'), /EDITABLE, $
                      UVAL='pcob', XSIZE=3)
  ; Add "update feature" button (not yet implemented)
  wdmy  = CW_BGROUP(bas5,'Update Feature',/ROW,UVAL='upda')

  ; Message window
  bas6  = WIDGET_BASE(base, /ROW)
  txtv  = strmid('                                        ',0,40)
  ctpt.wtx = WIDGET_LABEL(bas6, VALUE=txtv)

  ; set cursor to cross symbol.  Note: might be byteorder dependent!
  curim=intarr(16)
  curim[0:6]=8
  curim[3]=127
  wset,ctop.goesw                             ; need to set to valid window!
  device,cursor_image=intarr(16),cursor_xy=[11,12],cursor_mask=curim

  WIDGET_CONTROL, base, /REALIZE
  XMANAGER, 'sb_tptool', base ,NO_BLOCK=1
  
  WIDGET_CONTROL, base, SET_UVAL=w            ; save user variable

  ; need to de-sensitize some widgets (e.g., data set definition)
  WIDGET_CONTROL, ctop.wbut1,SENSITIVE=0  ; turn some widgets off
  WIDGET_CONTROL, ctop.wbut2,SENSITIVE=0
  ; (response in image windows always enabled, responded to only if applicable)
  ; process (*ctpt.pd).numt if applicable
  sb_tt2numt
  ; overplot images with features, if applicable
  sb_plot3d

end
