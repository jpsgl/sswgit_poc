;+
; NAME:
;        sb_qmheadfits
; PURPOSE:
;       Reads FITS headers of multiple files and puts result into
;       template structure.
;       Fast, if template structure is short.
;       However, reader is not fully general.
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        index = sb_qmheadfits(files,strtemplate)
; INPUTS:
;        file: string (vector)
;        strtemplate: element structure used as template for index
; KEYWORDS (INPUT):
;        timerange = [tstart,tend] , only return index for files in range
;                    requires strtemplate to have appropriate keywords
;                    such as date_d$obs, and/or date_obs
;                    format: anytim compatible (except ext)
; OUTPUTS:
;        index: fits header (index structure)
; KEYWORDS (OUTPUT):
;        outtime: time vector in anytim seconds (i.e., seconds since 1979)
;                 ignored if timerange not present
; COMMON BLOCKS:
; SIDE EFFECTS:
; RESTRICTIONS:
;       - Doesn't handle quotes within strings (clips string there)
;       - Doesn't handle complex numbers
;       - Doesn't handle comments or history keywords
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 6/14/2007
;        JPW, 5/14/2008 logic change in tran: "if ddas" -> "if ddas && ~tdas"
;-

FUNCTION sb_qmheadfits,ff,it0,timerange=tran,outtime=tt

; essentially does the following, but hopefully faster
; mreadfits,ff,ii,strtemplate=it0,/nocom,/nohis

ntg = n_tags(it0)
tg = strupcase(tag_names(it0))
; replace _D$ with dashes in tag names of template
for j=0,ntg-1 do begin
  pos = strpos(tg[j],'_D$')
  while pos ge 0 do begin
    tg[j] = strmid(tg[j],0,pos)+'-'+strmid(tg[j],pos+3)
    pos = strpos(tg[j],'_D$')
  endwhile
endfor
tg = strmid(tg+'       ',0,8)

nn = n_elements(ff)
ii = replicate(it0,nn)

for j=0L,nn-1 do begin
  h = headfits(ff[j])
  for k=0,ntg-1 do begin
    w = where(strmid(h,0,8) eq tg[k],nw)
    if nw gt 0 then begin
      hk = strmid(h[w[0]],10,70)
      hs = strsplit(hk,"'",/preserve_null,/extract)
      if n_elements(hs) gt 1 $
       then val=hs[1] $       ; is string
       else begin
        hs = strsplit(hk,"/",/preserve_null,/extract)
        hs = strtrim(hs[0],2)
        case hs of
          'T': val=1
          'F': val=0
          else: val=hs
        endcase
      endelse
      ii[j].(k) = val
    endif
  endfor
endfor

; subselect for time range if requested and possible
if n_elements(tran) eq 2 then begin
  dusc = tag_exist(it0,'date_obs')
  ddas = tag_exist(it0,'date_d$obs')
  tdas = tag_exist(it0,'time_d$obs')
  if dusc or ddas then begin
    tt = dblarr(n_elements(ii))
    if dusc then begin
      w = where(ii.date_obs ne '',nw)
      if nw gt 0 then tt[w]=anytim(ii[w].date_obs)
    endif
    if ddas && ~tdas then begin
      w = where(ii.date_d$obs ne '',nw)
      if nw gt 0 then tt[w]=anytim(ii[w].date_d$obs)
    endif
    if ddas && tdas then begin
      ; following line prevents use of time-obs if date_obs looks ok (=long)
      if dusc then slen=strlen(ii.date_d$obs) > strlen(ii.date_obs) $
              else slen=strlen(ii.date_d$obs)
      w = where(slen ge 8 and slen le 10 and ii.time_d$obs ne '',nw)
      if nw gt 0 then tt[w]=anytim(ii[w].date_d$obs+' '+ii[w].time_d$obs)
    endif
    w = where(tt ge anytim(tran[0]) and tt le anytim(tran[1]),nw)
    if nw gt 0 then begin
      ii = ii[w]
      tt = tt[w]
    endif else begin
      ii = it0
      tt = 0d0
    endelse
  endif
endif

return,ii

end
