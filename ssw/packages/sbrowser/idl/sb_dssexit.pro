
;+
; NAME:
;        sb_dssexit
; PURPOSE:
;        Cleanup in preparation for exiting data selection tool (sb_dss)
;        Note: does not destroy sb_dss.
;        Image display for STEREO-SECCHI
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        sb_dssexit,udss
; INPUTS:
; KEYWORDS (INPUT):
;        /cancel : restore state prior to entering data selection tool
; OUTPUTS:
; KEYWORDS (OUTPUT):
; COMMON BLOCKS:
;        sb_com
; SIDE EFFECTS:
; RESTRICTIONS:
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 9/27/2006
;-


PRO sb_dssexit,udss,cancel=cancel

  COMMON sb_com

  IF not KEYWORD_SET(cancel) THEN BEGIN
     ptr_free,cdss[udss.cds,udss.cdw].ptrds         ; free old heap variable
     sb_dssbut,udss,/backonly                       ; update backds in udss
     cdss[udss.cds,udss.cdw] = udss.ds              ; copy udss.ds into cdss
     IF (*udss.sptr)[0] ge 0 THEN BEGIN
        ; create pointer to new shortindex vector in cdss
        ii = (*udss.ds.ptrds)[*udss.sptr]
        cdss[udss.cds,udss.cdw].ptrds = ptr_new(ii,/no_copy)
     ENDIF ELSE cdss[udss.cds,udss.cdw].ptrds = ptr_new()
     sb_updatcmov
  ENDIF

  ; free heap variables
  ptr_free,udss.ds.ptrds
  ptr_free,udss.sptr

  sb_topupdat                             ; update top slider, time, goesplot

  ; update various things in calling widget (i.e., leader)
  if WIDGET_INFO(udss.leader,/valid) then begin      ; if leader widget valid
     WIDGET_CONTROL, udss.leader, GET_UVALUE=w       ; get user variable
     if tag_exist(w,'wnam') then begin               ; leader = imd widget?
        sb_imdlbl,w                                  ; update label buttons
        if udss.cds eq w.cds then begin
           WIDGET_CONTROL,w.wnam[w.cds],GET_VAL=lnam ; get DS label
           WIDGET_CONTROL,w.wlna,SET_VAL=lnam        ; set subwidget label
        endif
        ; update image and show (if applicable)
        sb_imdupdat,w,[udss.cds]
     endif
     WIDGET_CONTROL,udss.leader,SENSITIVE=1          ; leader widget on
  endif

END
