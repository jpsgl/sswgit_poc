
;+
; NAME:
;        sb_timwid
; PURPOSE:
;        creation and event handling of time range widget for go_secchi
;        Image display for STEREO-SECCHI
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        sb_timwid
; INPUTS:
; KEYWORDS (INPUT):
; OUTPUTS:
; KEYWORDS (OUTPUT):
; COMMON BLOCKS:
;        sb_com
; SIDE EFFECTS:
; RESTRICTIONS:
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 8/26/2005
;-


PRO sb_timwid_event, ev

  COMMON sb_com
  WIDGET_CONTROL, ev.id, GET_UVALUE=uval
  CASE uval OF
    'but1': BEGIN
              if ev.value ge 1 then begin   ; not "cancel"
                 WIDGET_CONTROL,ev.top,GET_UVALUE=udss
                 ctop.ts = udss.ts
                 ctop.te = udss.te
                 if ev.value eq 2 then begin   ; update t-range of ex. data
                   WIDGET_CONTROL,/hourglass   ; could take a while
                   ; loop through all existing datasets:
                   ; - repeat file search, read header, and fileselect
                   ; - if not empty: use new, else use 1-2 images of old
                   siz = size (cdss)
                   for i=0,siz[2]-1 do for j=0,siz[1]-1 do begin
                     if ptr_valid(cdss[j,i].ptrds) then begin
                       ii_old = *cdss[j,i].ptrds
                      ; ff = sb_findfiles(ctop.ts,ctop.te,cdss[j,i])
                      ; sb_readfits,ff,ii,/short
                       ii = sb_find(ctop.ts,ctop.te,cdss[j,i])
                       sii = size(ii)
                       if sii[sii[0]+1] eq 8 then begin
                         *cdss[j,i].ptrds = ii
                         ss = sb_fileselect(ctop.ts,ctop.te,cdss[j,i])
                         if ss[0] ge 0 then ii=ii[ss] else ii=-1
                       endif
                       sii = size(ii)
                       if sii[sii[0]+1] eq 8 then begin
                         *cdss[j,i].ptrds = ii
                       endif else begin
                         dtm = min(abs(ii_old.tt-ctop.ts),s0)
                         dtm = min(abs(ii_old.tt-ctop.te),s1)
                         if s0 eq s1 then ss=[s0] else ss=[s0,s1]
                         *cdss[j,i].ptrds = ii_old[ss]
                       endelse
                     endif
                   endfor
                 endif
                 sb_updatcmov    ; update cmov common block variable
                 sb_allimupdat
              endif

              sb_topupdat        ; update top slider, time, plot

              WIDGET_CONTROL,ctop.wtop,SENSITIVE=1    ; top widget on
                 ; destroy this widget upon exit
              WIDGET_CONTROL, ev.TOP, /DESTROY

            END

    'utra': BEGIN
            IF ev.value[0] lt ev.value[1] THEN BEGIN
              WIDGET_CONTROL,ev.top,GET_UVALUE=udss
              udss.ts = ev.value[0]
              udss.te = ev.value[1]
              WIDGET_CONTROL,ev.top,SET_UVAL=udss          ; update udss
              sb_plotgoes,udss.ts,udss.te,win=ctop.goesw,pixm=ctop.goespm, $
                          /update                          ; update goesplot
            ENDIF
            END

    ELSE:

  ENDCASE
END


PRO sb_timwid

  COMMON sb_com
  butt1 = ['Cancel','Apply T-Range to new data only', $
                    'Apply T-Range to all data']
  w = {utra:0L,ts:ctop.ts,te:ctop.te}

  base   = WIDGET_BASE(/COLUMN, GROUP_LEADER=ctop.wtop,XOFFSET=300, $
                      YOFFSET=30,TITLE='go_secchi Time Range')
  but1   = CW_BGROUP(base, butt1, /ROW, UVAL='but1')
  w.utra = CW_UT_RANGE(base,/nomsec,/nodur,/noreset,/oneline,UVAL='utra')

  WIDGET_CONTROL, base, SET_UVAL=w
  WIDGET_CONTROL, w.utra, SET_VAL=[w.ts,w.te]
  sb_plotgoes,w.ts,w.te,win=ctop.goesw,pixm=ctop.goespm,/update
  WIDGET_CONTROL, base, /REALIZE
  XMANAGER, 'sb_timwid', base ,NO_BLOCK=1

END
