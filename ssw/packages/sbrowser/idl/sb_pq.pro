
;+
; NAME:
;        sb_pq
; PURPOSE:
;        Calculates the P,Q parameters for poly_2d
; CATEGORY:
;        image processing
; CALLING SEQUENCE:
;        sb_pq,wcs,p,q,disp=cimd[i].disp
; INPUTS:
;        wcs : WCS structure of image
; KEYWORDS (INPUT):
;        disp = disp substructure of cimd (see sb_initcom)
;        Note: disp keyword parameter required!
; OUTPUTS:
;        p,q = arrays defining linear transformation for poly_2d
; KEYWORDS (OUTPUT):
; COMMON BLOCKS:
; SIDE EFFECTS:
; RESTRICTIONS:
; Note: is scaling to solrad at the ref.pixel, not suncenter!
;        Many, including
;        - Only for 2D images
;        - Expects crval in either arcsec, deg, or solRad!
;          Other units give nonsensical results
;        - wcs.position.hgln_obs (and some others) must be present
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 4/13/2006
;        JPW, 2/27/2007 simple minded implementation of /sc and /helio
;                       proper way would be via expanded disp structure
;        JPW, 12/4/2007 added sb_wcs(swcs) to ensure presence of hgln_obs
;-

PRO sb_pq,wcs0,p,q,disp=d  ,sc=sc,helio=helio,swcs=swcs0

; P are coeff. to get x coordinate of pixel in input image. P3 is 0.
; xi = P0 + P1*yo + P2*xo
; Q are coeff. to get y coordinate of pixel in input image. Q3 is 0.
; yi = Q0 + Q1*yo + Q2*xo

; 0. Standardize input wcs (CD variant, cunit in arcsec, etc.)
wcs = sb_wcs(wcs0)
if n_elements(swcs0) gt 0 then swcs=sb_wcs(swcs0)  ; added on 12/4/2007

; 1. Transf matrix from sun-observatories-plane-N/pix to heliocentric/solRad.
;    p_a is position angle of solar N measured ccw from display up.
;    Sign is opposite to sign of CROTA, i.e., CROTA = -p_a

twcs = wcs_get_time(wcs)
;p_a  = sb_p4display(t=twcs,hgln=wcs.position.hgln_obs) / !radeg
p_a  = sb_p4display(t=twcs,hgln=wcs.position.hgln_obs, $
                    owcs=wcs,helio=helio,sc=sc,swcs=swcs) / !radeg
o2s  = [[cos(p_a),-sin(p_a)],[sin(p_a),cos(p_a)]] / d.delt_1

; 2. Transformation matrix from heliocentric to input scale/orientation
;    Apply plate scale (in solRad) to CD matrix and invert
solrad = 6.9599e8   ; m

; 2.1 Get the offset of the reference pixel from suncenter
rof3 = sb_hg2hc(d.rval[0:1],pos=wcs.position,carr=d.carr,/earth) ; unit vec
rof3 = rof3 * d.rval[2]                                              ; solRad
dref_obs = wcs.position.dsun_obs - rof3[2]*solrad

; 2.2 Determine scale factor: solrad per arcsec at ref.pixel
scal = dref_obs / (solrad*!radeg*3.6e3)   ; sb_wcs forces units in arcsec

; 2.3 Establish the transformation matrix by inversion of wcs.cd and scaling
cd   = wcs.cd * scal
d_cd = cd[0,0]*cd[1,1] - cd[1,0]*cd[0,1]
cd_1 = [[cd[1,1],-cd[1,0]],[-cd[0,1],cd[0,0]]] / d_cd

; 3. Overall rotation/stretching matrix

pq = cd_1 # o2s

; 4. Find where pixel coord. of ref.point falls after rotation/stretching

rpq = pq # d.rpix

; 5. Find where pixel coord. of ref.point ought to be in input image

; 5.1 Find offset of ref.point from suncenter in heliocentric/solRad
rof = rof3[0:1]

; 5.2 Transform offset to input image frame via cd_1
rof_in = cd_1 # rof

; 5.3 Find suncenter position in input image (must scale crval to solRad)
s_in = wcs.crpix-1.0 - cd_1 # (wcs.crval*scal)    ; crpix is 1-biased

; 5.4 Find ref.pixel in input image
r_in = s_in + rof_in

; 6. Calculate translation from results of 4. and 5.
;    = location of origin of output image in input image

dpq = r_in - rpq

; 7. Put it all into P, Q

p = [dpq[0],pq[0,1],pq[0,0],0.0]
q = [dpq[1],pq[1,1],pq[1,0],0.0]

END
