function sb_byt2col,pbcub,cvec,ss=ss

;+
; $Id$
;
; Project : STEREO SECCHI
;
; Name    : sb_byt2col
;
; Purpose : Merge multiple (bytescaled) images into one true color image
;           via look-up tables 
;
; Use     : IDL> t_image = sb_byt2col(pbcub,cvec)
;
; Inputs  :
;    pbcub = pointer to bytarr(nx,ny,n_images). cube of input images
;    cvec = cvec(n_images).  vector of structures containing (at least)
;           the following tags:
;           .lut = bytarr(256,3).  look-up table for each image (rgb)
;           .show : if 0: ignore image plane[i], if 1: display image plane
; Outputs :
;    t_image = bytarr(nx,ny,3).  true color image
; Keywords:
;    ss = bytarr[n_images] : vector with one element for each image plane
;                            0: ignore plane, 1: display image plane
;         parameter is optional.  Default: display all image planes
; Common  : None
; Restrictions:
; Side effects:
; Category    : Image processing
; Prev. Hist. :
; Written     : Jean-Pierre Wuelser, LMSAL, 12-Sep-2006
;
; $Log$
;-

s = size(*pbcub)
if s[0] eq 3 then nn=s[3] else nn=1
if nn ne n_elements(cvec) then return,-1

t = intarr(s[1],s[2],3)

for i=0,nn-1 do if cvec[i].show then begin
   b = (*pbcub)[*,*,i]
   for j=0,2 do begin
      l = cvec[i].lut[*,j]
      t[0,0,j] = t[*,*,j] + reform(l[b],s[1],s[2])
   endfor
endif

t = byte(t < 255)

return,t

end
