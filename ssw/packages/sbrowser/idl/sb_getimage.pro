
;+
; NAME:
;        sb_getimage
; PURPOSE:
;        gets an image and prepares it for display:
;        - read FITS file
;        - optionally subtract dark or reference
;          or subtract bias if 16 bit
;        - exposure normalize (if 16 bit)
;        - perform geometric transformation
;        - optionally prepare running difference (by reentrant call of itself)
;        returns floating point image, which only needs (not done here):
;        - intensity transform to 8-bit
;        - look-up table transform
;        - co-added with other images of overlay
;
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        o = sb_getimage(ifil,disp,back=bfil,rundif=dfil,project=proj)
; INPUTS:
;        ifil = string.  Name of image file.
;        disp = structure.  Display parameters = output image parameters
;               Required tags: naxis[2],delt_1,rpix[2],rval[3],carr
; KEYWORDS (INPUT):
;        back = string.  Name of background file
;        rundif = string. Name of file to be subtracted for running difference
;        project = string or struct.  If present: de-project image to time/pos
;                  of a reference image.
;                  Either the file name, or the WCS of the reference image.
;        /noback = suppress background and bias subtraction
;        /helio : put Solar N up
;        /sc    : put normal to SC plane up.  requires swcs
;        swcs   = WCS of the other SC (determining SC plane)
;        /despike : does despiking with parms ok for EUVI (may be slow)
;        /naxis3 : if image file is 3D and /naxis3 set then extract d=d[*,*,1]
;                  (mainly for use with SOT FGIV Stokes-V images)
; OUTPUTS:
;        o = Fltarr.  Output immage array.  
; KEYWORDS (OUTPUT):
;        wcs = wcs structure.
;        index = FITS header structure.
; COMMON BLOCKS:
; SIDE EFFECTS:
; RESTRICTIONS:
;        Only for a single 2D image.
;        Allows reading of a single frame of a multi-image TRACE file
;        (see sb_readfits for details)
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 8/28/2006
;        JPW, 2/27/2007 use i.biasmean for bias subtraction if it exists
;        JPW, 2/27/2007 simple minded implementation of /sc and /helio
;                       proper way would be via expanded disp structure
;        JPW, 2/27/2007 added /despike
;-

function sb_getimage,ifil,disp,back=bfil,rundif=dfil,project=proj,naxis3=nx3, $
  noback=noback,wcs=wcs,index=i ,sc=sc,helio=helio,swcs=swcs,despike=despike

; read the FITS image
sb_readfits,ifil,i,d
if n_elements(d) le 1 then return,-1
if i.naxis eq 3 then begin
  i.naxis=2
  ; extract V image of a FGIV 3D image if /nx3 set
  if keyword_set(nx3) then d=d[*,*,1] else d=d[*,*,0]
endif
wcs  = fitshead2wcs(i)

; force /noback if rundif requested
if n_elements(dfil) eq 1 then if dfil ne '' then noback=1

; remove pixels with -32768 (off-disk pixels in MDI magnetograms)
if min(d) eq -32768 then begin
   ww = where(d eq -32768)
   d(ww) = 0
   minval = min(d) < (-max(d))
   d(ww) = minval
endif

; despike if requested (parms ok for EUVI, untested for other DS)
if keyword_set(despike) then begin
  d = despike_gen(d,tn=8)   ; despike
  if despike gt 1 then $
     d = despike_gen(d,tn=8)   ; 2nd run removes larger cosmic ray hits
endif

; read background file, if it exists, and check for right size
if keyword_set(noback) eq 0 then begin
   if n_elements(bfil) eq 1 then if bfil ne '' then begin
      sb_readfits,bfil,iback,back
      bsiz = size(back)
      if bsiz[0] eq 3 then if bsiz[3] eq 2 then begin
         ; use 2nd image plane as a gain (or mask) image
         gain = back[*,*,1]
         back = back[*,*,0]
         bsiz = size(back)
      endif
      if bsiz[0] ne 2 then bsiz=[-1,-1,-1]
      if bsiz[1] ne wcs.naxis[0] or bsiz[2] ne wcs.naxis[1] then back=0
   endif
   ; if no proper background file, determine bias
   if n_elements(back) eq n_elements(d) then begin
     ; subtract background image
     d = d - back
     if n_elements(gain) gt 0 then d = d * gain  ; apply gain (or mask)
   endif else begin
     ; if no proper background file, subtract bias or minimum
     if tag_exist(i,'biasmean') then d = (d-i.biasmean) > 0 $
                                else d = d - (min(d) > 0)
   endelse
endif

; exposure normalize
; find out if header says anything about exposure normalized
is_expnor = 0
if tag_exist(i,'history') then for j=0,n_elements(i.history)-1 do $
   is_expnor = is_expnor+1+strpos(strupcase(i.history[j]),'XPOSURE NORMALIZ')
is_expnor = is_expnor < 1
; exposure normalize.  MDI m'grams don't have EXPTIME and aren't normalized
if is_expnor eq 0 and tag_exist(wcs.time,'exptime') then begin
   exptime = float(wcs.time.exptime)
   if exptime gt 0.0 then d = d / exptime
endif

; check if de-projection, if so, get the proper reference wcs
if n_elements(proj) eq 1 then begin
   tproj = size(proj)
   tproj = tproj[tproj[0]+1]      ; type of proj
   if tproj eq 7 then if proj ne '' then begin
      ; read fits header and turn into wcs
      sb_readfits,proj,obsi
      tobsi = size(obsi)
      tobsi = tobsi[tobsi[0]+1]   ; type of obsi
      if tobsi eq 8 then begin    ; valid (index) structure
         obswcs = fitshead2wcs(obsi)
         do_proj = 1
      endif
   endif
   if tproj eq 8 then if tag_exist(proj,'position') then begin
      obswcs = proj
      do_proj = 1
   endif
endif
   
; transform image
;d = sb_gtrans(d,wcs,disp=disp,obswcs=obswcs,proj=do_proj)
d = sb_gtrans(d,wcs,disp=disp,obswcs=obswcs,proj=do_proj, $
              helio=helio,sc=sc,swcs=swcs)

; if rundif requested, read dfil with re-entrant call and subtract if valid
if n_elements(dfil) eq 1 then if dfil ne '' then begin
   dd = sb_getimage(dfil,disp,/noback,proj=obswcs,naxis3=nx3)
   if n_elements(dd) gt 1 then d = d - dd
endif

return,d
end
