
;+
; NAME:
;        sb_findfiles
; PURPOSE:
;        returns filenames for time range
; CATEGORY:
;        image processing, widgets
; CALLING SEQUENCE:
;        ff = sb_findfiles(ts,te,ds)
; INPUTS:
;        ts = start time
;        te = end time
;        ds = structure element with (at least) the following tags:
;             .obsrvtry = string.  Should start with 'A' or 'B' for STEREO,
;                                  'S' for SOHO, 'H' for HINODE
;             .detector = string.  'EUVI','COR1','COR2','HI_1',HI_2',
;                                  'EIT','C2','C3','MDI-W','MDI-M',
;                                  'TRACE','XRT','SOT-F','SOT-M', or ''
;                         If .detector starts with 't', then TRACE is assumed.
;             .path = string.  if '' or 'lz': use default lz data path,
;                              if 'pb', 'rt', 'ql' use corresp. default path,
;                              if SECCHI, space delimited combination 'pb'/'rt'
;                              and/or 'seq' and/or 'swx' is also recognized,
;                              otherwise .path is interpreted as full path.
; KEYWORDS (INPUT):
; OUTPUTS:
;        ff : vector of file names
; KEYWORDS (OUTPUT):
; COMMON BLOCKS:
; SIDE EFFECTS:
; RESTRICTIONS:
;
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 9/15/2006
;-

FUNCTION sb_findfiles,ts0,te0,ds

; calls appropriate routines xxxfiles depending on instrument
; with xxx = sb_secchi, eit_, mdi_, trace_
; explicit code for Lasco and for user data

; some of the routines can't handle ts,te in seconds:
tsi = anytim(ts0,/ints)
tei = anytim(te0,/ints)

obs = strupcase(strmid(ds[0].obsrvtry,0,1))
det = strupcase(ds[0].detector)
p   = ds[0].path

if obs eq 'A' or obs eq 'B' then begin
   ; SECCHI
   pp = strsplit(p,' ',/extract,count=npp)
   for i=0,npp-1 do begin
    if pp[i] eq 'lz' or pp[i] eq 'pb' or pp[i] eq 'rt' then begin
      src = pp[i]
      p = ''
    endif
    if pp[i] eq 'img' or pp[i] eq 'seq' then begin
      typ = pp[i]
      p = ''
    endif
    if strlowcase(pp[i]) eq 'swx' then begin
      swx = 1
      p = ''
    endif
   endfor
   ff = sb_secchifiles(ts0,te0,obs=obs,det=det,path=p, $
                       source=src,type=typ,swx=swx)
   if ff[0] ne '' or p eq '' then return,ff
endif
if obs eq 'S' then begin
   ; SOHO
   if p eq 'lz' then p = ''           ; lz is default
   if p eq 'ql' then begin
      ql = 1
      p = ''
   endif
   if det eq 'EIT' and p eq '' then begin
      ; EIT
      ff = eit_files(tsi,tei,quicklook=ql)
      return,ff
   endif
   if det eq 'MDI-M' and p eq '' then begin
      ; MDI magnetograms
      ff = mdi_files(tsi,tei)
      return,ff
   endif
   if (det eq 'C2' or det eq 'C3') and p eq '' then begin
      ; LASCO
      if keyword_set(ql) then p='$QL_IMG' else p='$LZ_IMG'
      p = concat_dir(p,'level_05')
      ; create date sub-paths:
      tt = [anytim(timegrid(ts0,te0,/days)),anytim(te0)]
      dirs = time2file(tt,/date_only,/year2digit)
      dirs = dirs(uniq(dirs))
      ; put path, dirs, detector, and wildcard together:
      dirs = concat_dir(concat_dir(concat_dir(p,dirs),strlowcase(det)),'*.fts')
      ; get the files:
      ff = file_search(dirs)
      return,ff
   endif
endif
if det eq 'TRACE' then begin
   ; TRACE
   if p eq '' then begin
      ff = trace_files(tsi,tei)
      if ff[0] ne '' then ff = ff + '#0000'      ; mark for multi-image FITS
      return,ff
   endif else begin
      ; create file list with wildcards and proper times
      tt = [anytim(timegrid(ts0,te0,/hours)),anytim(te0)]
      ff = strmid(time2file(tt,delim='.'),0,11)  ; string & clip off minutes 
      ff = ff(uniq(ff))                          ; make it unique
      ff = concat_dir(p,'*'+ff+'*')
      ff = file_search(ff)
      if ff[0] ne '' then begin
         ff = ff + '#0000'      ; mark for multi-image FITS
         return,ff
      endif
   endelse
endif
if obs eq 'H' then begin
   ; HINODE
   if det eq 'XRT' and p eq '' then begin
      ; XRT
      ff = ''
      return,ff
   endif
   if det eq 'SOT-F' and p eq '' then begin
      ; SOT filtergrams
      ff = ''
      return,ff
   endif
   if det eq 'SOT-M' and p eq '' then begin
      ; SOT magnetograms
      ff = ''
      return,ff
   endif
endif
   
; look in provided path for any other files

if p ne '' then ff=file_search(concat_dir(p,'*')) else ff=''
return,ff

end
