
;+
; NAME:
;        sb_tt2numt
; PURPOSE:
;        calculates (*ctpt.pd).numt values from (*ctpt.pd).tt and *cmov.ptv
; CATEGORY:
;        image processing
; CALLING SEQUENCE:
;        sb_tt2numt
; INPUTS:
; KEYWORDS (INPUT):
; OUTPUTS:
; KEYWORDS (OUTPUT):
; COMMON BLOCKS:
;        sb_com
; SIDE EFFECTS:
;        modifies (*ctpt.pd).numt and .stat
; RESTRICTIONS:
; PROCEDURE:
; MODIFICATION HISTORY:
;        JPW, 03/14/2008
;-

PRO sb_tt2numt

common sb_com

if ptr_valid(ctpt.pd) then begin

  ; first force .stat to indicate invalid xy:
  stat = (*ctpt.pd).stat AND 2
  (*ctpt.pd).stat = stat

  ; go through each feature, determine unique times, then for each unique
  ; feature time find closest *cmov.ptv, then sort by delta_t and select
  ; unique *cmov.ptv entries.  For other feature times set numt = -1

  ; find unique features
  feat = (*ctpt.pd).numf
  stat = (*ctpt.pd).stat
  ww = where(feat ge 0L and stat ge 2,nfeat)
  if nfeat le 0 then return
  (*ctpt.pd)[ww].numt = -1             ; initialize numt
  feat=feat[ww]
  feat = feat[uniq(feat,sort(feat))]   ; unique features
  nfeat = n_elements(feat)

  for j=0L,nfeat-1 do begin
    ww = where((*ctpt.pd).numf eq feat[j] and stat ge 2,nww)
    if nww gt 0 then begin
      tt = (*ctpt.pd)[ww].tt
      ww = uniq(tt,sort(tt))
      nww = n_elements(ww)
      tt = tt[ww]   ; unique times for this feature
      ; for each tt element find closest *cmov.ptv
      itf = lonarr(nww)-1L            ; index to closest *cmov.ptv for each tt
      dtf = dblarr(nww)               ; time offset for each itf
      for k=0L,nww-1L do begin
        dt = abs((*cmov.ptv)-tt[k])
        ww1 = where(dt eq min(dt))
        ww1 = ww1[0]
        ; check if index ww1 already in itf, if so, choose one with smaller dt
        ww2 = where(itf eq ww1,nww2)
        if nww2 gt 0 then begin
          if dt[ww1] lt dtf[ww2] then begin
            itf[ww2[0]] = -1
            itf[k] = ww1
            dtf[k] = dt[ww1]
          endif
        endif else begin
          itf[k] = ww1
          dtf[k] = dt[ww1]
        endelse
      endfor
      ; eliminate itf's eq -1
      ww3 = where(itf ge 0,nww3)
      itf = itf[ww3]
      tt = tt[ww3]
      ; set numt for all points that have tt with non-neg. itf:
      for k=0L,nww3-1 do begin
         ww = where((*ctpt.pd).numf eq feat[j] and stat ge 2 and $
                    (*ctpt.pd).tt eq tt[k])
         (*ctpt.pd)[ww].numt = itf[k]
      endfor
    endif
  endfor

endif

end
