subr exaread,carr,h
;1/3/94 handle combined runsum and bitslice compression
;7/22/92 debugged (excl. compression) reading VMS-written tape   LS
;3/26/92 unix version, first with swapping activated in driver
;12/11/90 compressed data support
;8/30/89 re-organized for error recovery from bad data, etc
;4/4/89  re-wrote to handle general variables, but not packed data yet
;10/27/88 image # from header put in $img
narg=!narg
;what endian are we?
bigend=bigendian()
;ty,'bigend=', bigend
b=bytarr(1024)
!tapemessages=0
;note, EOF messages still enabled in latest ANA version
if defined($itd) eq 0 then read,'enter tape unit (probably 0)',$itd
taprd,$itd,b
bc=!bc
if bc eq 0 then {ty,'EOF encountered' carr=0 return}
;look for a synch pattern
limit=20	ic=0
;try to allow reads of Wang Wei's tapes also.  9/4/93
while (lmap(b(0:3)) ne -1431677611) and (lmapr(b(0:3)) ne -1431677611) do {
 taprd,$itd,b stat=!errno bc=!bc
 t,'looking for synch'
 if bc eq 0 then {ty,'EOF encountered' carr=0 return}
 ic+=1 if ic gt limit then {
 ty,'*** EXAREAD - read',ic,' records without finding a header'
 ty,'assuming this is some kind of mistake!' carr=0 return } }
;we should now have a header record (or a very big mistake)
;get the dimension and type
ndim=b(8)
ntype=b(7)
h=b(256:(bc-1))
; i=strpos(h,smap(byte(0)))	if i gt 0 then h=h(0:(i-1))
;    ^ does not work in UNIX ANA; smap's of arrays containing 0s is trouble
;bug found 9/22/94, following line was i = sieve(indgen(h), h eq 0) but
;because h was a byte array, the index wrapped at 256 causing errors when the
;header was gt 256 characters
i = sieve(h eq 0) 
if symclass(i) eq 4 then { i = i(0)  h=h(0:i-1) } else i = 0
h = smap(h)
if narg lt 2 then ty,strreplace(h,smap(byte(13)),smap(byte([13,10])),100)
dims=lonarr(ndim)
for i=0,ndim-1 do {i1=i*4+192 i2=i1+3
  if bigend then dims(i)=lmapr(b(i1:i2)) else dims(i)=lmap(b(i1:i2)) }
;check for data compression flag
;t,'c flag =',b(4)
;get compression structure size (if compressed)
if bigend then n=lmapr(b(10:13)) else n=lmap(b(10:13))
;ty,'n=', n
if b(4)%128 eq 0 or n le 0 then {
;non-compressed data case
;case on the data type
ncase ntype
{ carr=bytarr(dims) }
{ carr=intarr(dims) }
{ carr=lonarr(dims) }
{ carr=fltarr(dims) }
{ carr=dblarr(dims) }
{ type,'*** this version of EXAREAD does not handle packed data !' }
else { type,'*** EXAREAD, illegal data type',ntype carr=0 return }
endcase
wait_for_tape,$itd		;just in case
;note on tapebufin - args. are unit,array,record_size
;it keeps reading records on tape until array is filled, note that array
;must be defined to have the right size in bytes, there is some flexibility
;about record sizes within the code in case one doesn't "guess" right
;tapebufin,$itd,carr,64512
tapebufin,$itd,carr,16384
if bigend then {
ncase ntype-1
{ swab,carr }	;swap bytes for I*2
{ reverselong,carr }	;reverse byte order for longs;
endcase
}
wait_for_tape,$itd		;remove this for simultaneous ops, but you must
				;check before changing carr or writing again
} else {
;compressed data case, get size of structure on tape
x=bytarr(n)
;W.W.'s tapes have a long record size, allow for it
;tapebufin,$itd,x,64512
tapebufin,$itd,x,16384
wait_for_tape,$itd		;remove this for simultaneous ops, but you must
				;check before changing carr or writing again
;now decompress, should work for cases with and without runsum

decrunch,x,carr
;carr should be proper type but if ndim>2, the dimensions are wrong, fix now
redim,carr,dims
}
return
endsubr
