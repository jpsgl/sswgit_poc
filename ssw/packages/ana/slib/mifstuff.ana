;routines to open/read pointer based multi-image files
 ;======================================================= 
func get_mif_image(i)
 ;4/29/99 - the mif file is NOT left open, but we should have all
 ;the pointers
 lun = get_lun_and_openr($mif_name)
 if lun lt 0 then {
   errormess, 'can''t re-open the MIF file\n'+$mif_name
   return, 0 }
 fileread, lun, xin, $mif_start_byte(i), $mif_n_bytes(i), 0
 close, lun
 type = $mif_data_type(i) - 1
 ncase type
  xin = bmap(xin)
  xin = wmap(xin)
  xin = lmap(xin)
  xin = fmap(xin)
  xin = dmap(xin)
  else: {  errormess,'bad data in MIF file'  return, 0 }
 endcase
 redim, xin, $mif_nx_out(i), $mif_ny_out(i)
 if $little_endian then endian, xin
 ;put time and position in standard variables for general use
 if isarray($mif_time_tai) $data_time_tai = $mif_time_tai(i)
 if isarray($mif_xcen) $data_xcen = $mif_xcen(i)
 if isarray($mif_ycen) $data_ycen = $mif_ycen(i)
 if isarray($mif_xscale) $data_xscale = $mif_xscale(i)
 return, xin
 endfunc
 ;======================================================= 
subr mif_list_entry_cb
 ;puts the selected image into a view, sets up some view parameters
 iq = $list_item_position - 1
 x = get_mif_image(iq)
 if isscalar(x) then return, 0
 ;get the target window
 win = fix(xmtextfieldgetstring ($mifwin_num))
 switch,x,eval('$data'+string(win))
 zap_view_data, win
 $view_tai(win) = $data_time_tai
 $view_sun_xc(win) = $data_xcen
 $view_sun_yc(win) = $data_ycen
 $view_sun_pixel_scale(win) = $data_xscale
 if $viewdespikeflag(win) then viewdespike, win
 view, win
 endsubr
 ;======================================================= 
subr mif_open_via_text
 ;assumes $miflistwidget exists, reads the file name in the text box
 ;and tries to process as a MIF file
 name = xmtextfieldgetstring($miftext1)
 name = strtrim(name)      ;remove trailing blanks
 iq = mif_open_file(name)
 endsubr
 ;======================================================= 
func mif_open_file(name)
 ;assumes that the file is a special fits file with a binary extension
 ;that contains image pointers to be used to extract images from the
 ;"main" fits data, if it doesn't pass muster, we return a -1
 ;if it looks OK, we process the extension info and keep it but close
 ;the file and just save the name to re-open it for image reads 

 ;zero any previous extensions that might have been read in
 $mif_start_byte=0  $mif_n_bytes=0  $mif_dp_header=0  $mif_instrume=0
 $mif_data_type=0  $mif_date_obs=0 $mif_xscale = 0 $mif_xcen = 0
 $mif_ycen = 0 $mif_cdelt1 = 0

 iq = fits_header(name, h, h2, '$MIF_')
 if iq eq 0 then {
	errormess, sprintf('MIF file\n%s\nis not readable', name)
	return, 0 }
 if isscalar(h2) then {
	errormess, sprintf('%s\nnot really a\nMIF file\ntry again', name)
	return, 0 }

 ;we should have the binary extensions all decoded into variable arrays
 ;with a $mif_ prefix, we have to check that the right ones exist. Also,
 ;if there is a dp_header array we'll assume it is TRACE data and decode
 ;parts of it
 
 $mif_name = name
 bad = isscalar($mif_start_byte) or isscalar($mif_n_bytes)
 bad = bad or isscalar($mif_nx_out) or isscalar($mif_ny_out)
 bad = bad or isscalar($mif_data_type)
 if bad errormess, sprintf('MIF file\n%s\nis missing some essentials', name)
 
 ;OK enough to get started
 $mif_nimages = dimen($mif_start_byte, 0)
 $mif_time_tai = zero(dblarr($mif_nimages))

 ;check for the TRACE dp_header
 trace_flag = 0
 if isarray($mif_dp_header) then {
  trace_flag = 1
  ;dumb operation below can be omitted when trace header decoder is done
  if $little_endian then swab, $mif_dp_header
  ;but!, sometime people put dp_header's in other files, so check the
  ;instrument keyword
  if isscalar($mif_instrume) ne 1 then {
   if strpos('TRACE', $mif_instrume(0)) lt 0 then trace_flag = 0
   }
  }
 if trace_flag then {
  t3d_widget		;because we need many things it defines
 }
  ;we always try for date_obs time, if this is a TRACE file and we can't
  ;find date_obs or another standard time, we should get a time from dp head
  if isstrarr($mif_date_obs) then {
    for i = 0, $mif_nimages - 1 do {
    $mif_time_tai(i) = tai_from_date_obs($mif_date_obs(i)) }
  }
 ;if a $mif_cdelt1 was found but no $mif_xscale, then use $mif_cdelt1
 if isscalar($mif_xscale) then { if isarray($mif_cdelt1) then
 	$mif_xscale = $mif_cdelt1) }

 ;make the list widget
 if defined($miflistwidget) ne 1 then {
 $miflistwidget = xmtoplevel_form(0,0, 'Multiple Image File List',0,0,5, 5)
 wq = topbuttonsetupsanslink_tl('$miflist')
 ;source window #
 $mifwin_num = xmtextfield(wq,'',2,'',$f3,'white')
 xmposition, $mifwin_num, 198, 0, 33,30
 sq = '{$link_action='+'''generic_link_cb,'+string($mifwin_num)+''' link_setup }'
 bb = xmbutton(wq, 'Link', sq, $f4, 'darkgreen') 
 xmposition, bb, 140, 0, 55, 30

 ;below this, a text box for the file name and a label
 lab1 = xmlabel(wq,'file: ',$f4,'', 1)
 xmposition, lab1, 340, 5
 lq = xmlabel(wq,'file: ',$f4,'', 1)
 xmposition, lq, 0, 38
 cb = 'mif_open_via_text'
 $miftext1 = xmtextfield(wq, '', 256, cb, $f3 ,'white')
 xmposition, $miftext1, 30, 33, 500, 33
 bq = xmbutton(wq, 'load', cb, $f4, 'gray')
 xmposition, bq, 540, 33, 0, 33
 sq = 'mif_list_entry_cb'
 list = xmlist($miflistwidget, sq, 40, $f8, 'white', 1)
 lparent = xtparent(list)
 shellw = xtparent(lparent)
 xmposition, lparent, 0, 0, 500, 300
 sep = xmseparator($miflistwidget, 0, 10, 0)
 xmposition, sep, 0, 0, 10, 10
 formvstack, wq, sep, xtparent(list)
 xmattach, xtparent(list), 0, 1,1,0,1
 $miflistwidget = list	;make the list widget the global
 }
 ;some of the operations below can change the size, so save it here and then
 ;re-apply it
 xmgetwidgetsize, shellw, dx, dy
 ;also the position
 xmgetwidgetposition, shellw, xp, yp
 ;delete the old stuff (if any)
 xmlistdeleteall, list

 ;loop over all images and put in list
 xmsetlabel, lab1, sprintf('file: %s, %d images', removepath(name), $mif_nimages)
 xmtextfieldsetstring, $miftext1, name, 1	;the 1 shifts it to left
 ;pop down so that filling list takes less time
 xtpopdown, $miflistwidget
 for i= 0, $mif_nimages - 1 do {
  if trace_flag then {
   sq = sprintf('%5d %s', i, trace_oneliner($mif_dp_header(*,i)))
   } else {
   if $mif_time_tai(i) gt 0 then
   	tq = date_from_tai($mif_time_tai(i)) else tq = 'no date/time'
   sq = sprintf('%5d %s %5dx%d', i, tq, $mif_nx_out(i), $mif_ny_out(i))
   }
  xmlistadditem, list, sq
 }
 xmposition, shellw, xp, yp, dx, dy
 xtpopup, $miflistwidget
 xmgetwidgetposition, shellw, xp, yp

 return, 1
 endfunc
 ;======================================================= 
