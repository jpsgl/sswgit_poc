func trace_single_read(fname, i)
 ;reads the i'th image in a trace hourly file named fname, uses the same
 ;file check as trace_series_time
 if defined($read_list_current_t3d_file) eq 0 then
 	$read_list_current_t3d_file = ''
 ;check if the current file, if not we have to open file, etc
 if  $read_list_current_t3d_file ne fname then {
   ty,'new file needed, fname:', fname,' $read_list_current_t3d_file:', $read_list_current_t3d_file
   $read_list_current_t3d_file = fname
   if trace_open_hourly(fname) ne 1 then errormess,'problem opening file'
 }
 j = i
 xim = get_trace_image(j)
 if isarray(xim) eq 0 then {
   sq = sprintf('bad TRACE image\ncan''t decode\nimage %d in\n%s',j, $read_list_current_t3d_file)
   ty,sq  return, -1 }
 ;differs from readtracelistimage here where we get some parameters needed
 ;modeled after the way it is done in MDI
 $trace_dp_head = wmap($t3d_dp_header(*,j))
 ty,'j =', j
 tq = $trace_dp_head(3:5) if $little_endian then swab, tq
 $trace_tai = trace_tai(tq)
 ty,'$trace_tai =', $trace_tai
 $trace_mech_string = trace_mechs($trace_dp_head, quad, f1, f2, w1, w2, focus)
 $trace_quad = quad
 $trace_f1 = f1
 $trace_f2 = f2
 wedge2solar, w1, w2, x, y
 trace_ccdpositions, $trace_dp_head, ix, iy, ix2, iy2, nx, ny, bm, csum
 ;since the (ix, iy) are already corrected for amp B, we have
 if not(defined($trace_plate_scale)) $trace_plate_scale = .504
 ; <offset> was not defined; do it here, analogous to
 ; trace_series_read_f.ana.  LS 15 Feb 2000
 if $little_endian then {
   xq = $trace_dp_head(296)   swab, xq
   wl =  extract_bits(xq, 7, 5)
 } else {
   wl =  extract_bits($trace_dp_head(296), 7, 5)
 }
 $trace_wave = wl
 offset = trace_wave_offset($trace_wave, $trace_tai)

 $trace_xc = x - $trace_plate_scale*(.5*(iy+iy2) - 511.5 - offset(0))
 $trace_yc = y - $trace_plate_scale*(.5*(ix+ix2) - 511.5 - offset(1))
 $trace_pixel_size = $trace_plate_scale*bm*csum
 $trace_x2 = x - $trace_plate_scale*(iy - 511.5 - offset(0))
 $trace_y2 = y - $trace_plate_scale*(ix - 511.5 - offset(1))
 $trace_x1 = x - $trace_plate_scale*(iy2 - 511.5 - offset(0))
 $trace_y1 = y - $trace_plate_scale*(ix2 - 511.5 - offset(1))
 $trace_et = trace_expose($trace_dp_head)
 ;for raw trace images, we orient them so that they have the normal solar
 ;directions, this depends on the amplifier
 ;replaced undefined <xx> by <$trace_dp_head> in following.  LS 15feb2000
 if $little_endian then { amp_case = extract_bits($trace_dp_head(102),4,1) } else {
 	amp_case = extract_bits($trace_dp_head(102),12,1) }
 if amp_case then {
  ;amp B case
  xim = reorder(xim,4)
 } else {
  ;amp A case
  xim = reorder(xim, 7)
 }
 if defined($trace_keywords_fits) then {
   ;fill some keywords, should be done in a C program before it gets too big
   sq = date_from_tai($trace_tai, 3)
   sq = strreplace(sq, '_UTC', '')	;strip off the _UTC
  
   $trace_keywords_fits(0,1) = '''' + sq(11:*) + ''''
   sq = strreplace(sq, '.','-',2)
   sq = strreplace(sq, '_','T',1) + 'Z'
   $trace_keywords_fits(1,1) = '''' + sq + '''' 
   $trace_keywords_fits(3,1) = sprintf('%0.3f',$trace_tai)
   $trace_keywords_fits(6,1) = sprintf('%8.4f',$trace_et)
   }
 return, xim
 endfunc
 ;=======================================================================
