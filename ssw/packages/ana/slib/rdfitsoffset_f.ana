function rdfitsoffset(x,offset,lun,h, ext_flag)
 ;3/18/98 - make safe for TRACE hourlies, return status of 3
 ;9/3/97 - read a fits file already opened via lun,  with offset into
 ;the file to allow reading extensions as well as a regular read
 ;always provides offset into a possible next extension since EXTEND
 ;keyword is not necessarily set after the first header
 ext_flag = 1
 fileread, lun ,h, offset, 2880, 0		;read header
 s=smap(h(0:80))
 ;ty, s
 
 trace_hourly_flag = 0
 ;note that if this is an extension read, we normally wouldn't set the
 ;trace_hourly_flag, if the flag is set, the data is still read if it is not
 ;1-D, this flag is used to suppress accidently reading the large data blocks
 ;in these files
 if strpos(upcase(s), 'SIMPLE') ge 0 then {
 ;is the value of simple true or is this special?
 s = strskp(s,'=')
 simple = 1
 if strpos(upcase(s),'T') lt 0 then { simple = 0
  ;now check if a special compressed type
  if strpos(s,'F, ana rice compressed') ge 0 then simple = 2
  ;put any future checks here (?)
  }
 if simple eq 0 then { ty,'not a known non-simple FITS format'
   close, lun	return, 0 }
 } else 
  if strpos(upcase(s), 'XTENSION') eq 0 then {
    ;ty, 'assuming simple extension'
    simple = 1
  } else {
  ty,'neither SIMPLE or XTENSION keyword found, not a FITS file'
  close, lun	return, 0 }
 redim, h, 80, 36
 ;print out in 80 column format until we hit end or 2880
 naxis=0
 type=-1
 dim=lonarr(8)
 head=bytarr(80,360)	;max number of headers=10
 insert, head, h, 0, 0
 i=0	nh=0	ns=offset
 s=smap(h(*,0))
 while strpos(upcase(s), 'END ') ne 0 do {
 ;check for needed info
 if strpos(upcase(s), 'NAXIS') eq 0 then {
  ;this could be either just naxis or naxisn
  sq=strskp(upcase(s), 'NAXIS')
  iq=bmap(sq(0))
  if iq ge bmap('0') and iq le bmap('9') then {
  ;naxisn case
  iq=fix(sq(0))
  dim(iq-1)=fix(strskp(s,'='))
   } else {
   naxis = fix(strskp(s,'='))
   if naxis le 0 or naxis gt 8 then {
     ty,'can''t handle this dimension of FITS file, naxis=',naxis
     close, lun		return,0 } }
  }
 if strpos(upcase(s), 'BITPIX') eq 0 then {
   iq = fix(strskp(s,'='))
   case
    iq eq 8: type = 0
    iq eq 16: type =1
    iq eq 32: type =2
    iq eq -32: type =3
    iq eq -64: type =4
    else: { type, 'can''t handle data type, bitpix =', iq  return, 0 }
   endcase
  }
 if strpos(upcase(s), 'EXTEND') eq 0 then { ext_flag = 1
    ;note that we can't count on this to be set for the second, etc extension(s) so
    ;default is to have ext_flag = 1
    ;ty, 'extension flag set'
  }
 if strpos(upcase(s), 'TR_REFORMAT') ge 0 then {
 ;checking for large TRACE hourly files, we don't normally want to read these
 trace_hourly_flag = 1
 }
 i+=1
 if (i eq 36) {nh+=1	ns=nh*2880+offset  fileread,lun,h,ns,2880,0
 redim,h,80,36	insert,head,h,0,nh*36	i=0}
 s=smap(h(*,i))
 }
 i=((nh)*36)+i			;i=final number of header records
 ;ty,'total number of lines =',i
 h=head(*,0:i)
 ;some FITS files have zeroes in the headers, bad for displays so replace
 ;with blanks
 in = sieve(h eq byte(0))
 if isarray(in) then h(in) = zero(bytarr(num_elem(in))) + byte(32)
 h=smap(h)
 
 ;ty,'simple, naxis, type =', simple, naxis, type
 ;for i=0,naxis-1 do ty,'dim',i,' =',dim(i)
 
 ;now it depends on the data type and whether it was simple
 ns = ns +2880
 dim=dim(0:(naxis-1))
 if simple eq 1 then {
   if naxis eq 1 then nq = dim else { nq=1  for i=0,naxis-1 do nq=nq*dim(i) }
   ;we also need to do a sanity check for TRACE hourly files (which are not sane)
   if trace_hourly_flag then {
    ;if not 1-D, might be something else?
    if naxis eq 1 then {
     ;note that header is still returned
     x = 0
     return, 3
    }
   }

   ncase type
   { fileread,lun,x,(ns),nq,0   	redim,x,dim   ns = ns + nq}
   { fileread,lun,x,(ns)/2,nq,1 	redim,x,dim   ns = ns + 2*nq}
   { fileread,lun,x,(ns)/4,nq,2 	redim,x,dim   ns = ns + 4*nq}
   { fileread,lun,x,(ns)/4,nq,3 	redim,x,dim   ns = ns + 4*nq}
   { fileread,lun,x,(ns)/8,nq,4 	redim,x,dim   ns = ns + 8*nq}
   endcase
   ;fits data is supposed to always be big endian, so fix up data on
   ;little endian machines (like alpha's and pc's)
   if bigendian() eq 0 then endian, x
 } else {
 ;only non-simple supported is the rice compressed, simple = 2
   fileread, lun, chd, (ns), 14, 0
   nc = lmap(chd(0:3))  if bigendian() ne 0 then swapl, nc
   ;the total size to be read is then nc, could also have just read
   ;the rest of the file
   fileread, lun, y, (ns), nc, 0
   ns = ns + nc
   decrunch, y, x
   redim, x, dim
 }
 ;if an extension,  figure out where its header would be
 if ext_flag then { ns = fix(ns)  ns = 2880*( (ns-1)/2880 + 1)
    ext_flag = ns  }
 return,1
 endfunc
 ;=========================================================================
