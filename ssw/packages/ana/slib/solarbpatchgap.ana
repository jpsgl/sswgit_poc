subr solarbpatchgap, x
 if symdtype(x) ne 1 then x = short(x)
 ;patch the central gap if appropiate for this image (must be a FG)
 ;2/17/2009 - extend to non-I cases, might be some problems still, start using fgccdshift
 ;;if num_dim(x) gt 2 then { ty,'patch CCD center: we don''t do 3-D cases yet' return }
 if $sb_sum eq 4 then { ty,'not handling 4x4 summing yet in "patch center"'  return }
 if $sb_bin gt 1 then { ty,'not handling binning summing yet in "patch center"'  return }
 ;we also have to give up if this is a shutterless with ROI looping
 if $roiloopflag then {
   ;check if it might have ROI looping, it must be shutterless of course
   if $sb_genid ge 32 then {
     ;worth the expense of a fits keyword check
     roiloop = fits_key($sbhead,'ROILOOP')
     if isscalar(roiloop) eq 0 then {
	errormess, 'no ROILOOP keyword in FITS file?\npatch center disabled'
	return }
     if roiloop ge 1 then { ty,'not doing "patch center" for ROI loops yet'  return }
   }
 }
 if num_dim(x) eq 2 then {
   fgccdshift, x, $sb_roistop, $fg_ix1, $fg_iy1, $sb_sum, $sb_bin, $sb_genid
 } else if num_dim(x) eq 3 then {
   ;;until we make fgccdshift smarter, we have to extract and reinsert each 2-D image
   nd = dimen(x, 2)
   for id = 0, nd-1 do {
     xq = x(*,*,id)
     fgccdshift, xq, $sb_roistop, $fg_ix1, $fg_iy1, $sb_sum, $sb_bin, $sb_genid
     x(0,0,id) = xq
   }
   ;if we have an I image in a 3-D, we have to float now to make any I signal a
   ;unsigned quantity, because the 3-D keeps getting stuff inserted, we have to
   ;do it before this can cause unintended casts between short and float in operations
   ;that follow, needs to be done more efficiently someday
   if $sb_genid ge 32 then iflag = 1 else iflag = $sbhasimage($sb_genid < 18)
   if iflag then {
     x = float(x)
     y = x(*,*,0)
     y = y + 65536. * (y lt 0)
     x(0,0,0) = y
   }
 }
   
//      }
//      ;insert interpolated columns, special cases abound but we have already
//      ;returned if there are no middle columns so we know we have at least one
//      ;and if there is one and not the other, their weight sum ws has already been
//      ;adjusted
//      x1 = rfix((2.0*xa + xb) * ws)
//      x2 = rfix((2.0*xb + xa) * ws)
//      redim, x1, 1, ny
//      redim, x2, 1, ny
//      if im ge 0 and im le nxm1 then insert, x, x1, im
//      if imp1 ge 0 and imp1 le nxm1 then insert, x, x2, imp1
//    }
//  return		;end of the $sb_roistop lt 1025 case
//  }
//  
 ;as a temporary fix for 2x2 partial reads, back out 1/2 pixel of horizontal shift
 ;this should be modified to avoid interpolation (causing a bias to one side instead)
 ;and integrated in fgccdshift
 if $sb_roistop lt 1025 then {
   if $sb_sum eq 2 then {
     mid = 1023 - $sb_ix0
     if mid gt 0 and mid lt $sb_ix1 then {
       if num_dim(x) eq 2 then {
	 z1 = x(0:mid,*)
	 z2 = z1
	 insert, z2, z1(0:(mid-1),*),1,0
	 z1 = 0.5*(z1+z2)
	 z2 = x((mid+1):*,*)
	 insert, x, z1, 0, 0
	 z1 = z2
	 insert, z1, z2(1:*, *),0,0
	 z1 = 0.5*(z1+z2)
	 insert, x, z1, mid+1, 0
       } else if num_dim(x) eq 3 then {
         ;do the stack, keep it simple for now, optimize when included in fgccdshift
            for id = 0, nd-1 do {
	      xq = x(*,*,id)
	      z1 = xq(0:mid,*)
	      z2 = z1
	      insert, z2, z1(0:(mid-1),*),1,0
	      z1 = 0.5*(z1+z2)
	      z2 = xq((mid+1):*,*)
	      insert, xq, z1, 0, 0
	      z1 = z2
	      insert, z1, z2(1:*, *),0,0
	      z1 = 0.5*(z1+z2)
	      insert, xq, z1, mid+1, 0
	      x(0,0,id) = xq
	  }
       }

     }
   }
 }
 if $sb_roistop eq 1025 then {
   ;if not a partial read there are 2 bright central columns that we
   ;are approximately patching for some special cases
   ;we only can remove from an I image, if there is one, we assume first in stack
   if $sb_genid ge 32 then iflag = 1 else iflag = $sbhasimage($sb_genid < 17)
   if iflag then {
     if num_dim(x) eq 3 then {
       ;we already checked for existence of I
       exchange, x, xsave
       x = xsave(*,*,0)		;x is now just the I part
     } else xsave = 0

     if $sb_sum eq 2 then {
       ;check if the 1023 column is in the image
       if $sb_ix0 le 1023 and $sb_ix1 ge 1023 then {
	 inx = 1023 - $sb_ix0
	 c1 = x(inx,*)*0.912
	 redim, c1, 1, num_elem(c1)
	 insert, x, c1, inx, 0
       }
       ;check if the 1024 column is in the image
       if $sb_ix0 le 1024 and $sb_ix1 ge 1024 then {
	 inx = 1024 - $sb_ix0
	 c1 = x(inx,*)*0.921
	 redim, c1, 1, num_elem(c1)
	 insert, x, c1, inx, 0
       }
     }


     if $sb_sum eq 1 then {
       ;check if the 2047 column is in the image
       if $sb_ix0 le 2047 and $sb_ix1 ge 2047 then {
	 inx = 2047 - $sb_ix0
	 c1 = x(inx,*)*0.84
	 redim, c1, 1, num_elem(c1)
	 insert, x, c1, inx, 0
       }
       ;check if the 2048 column is in the image
       if $sb_ix0 le 2048 and $sb_ix1 ge 2048 then {
	 inx = 2048 - $sb_ix0
	 c1 = x(inx,*)*0.852
	 redim, c1, 1, num_elem(c1)
	 insert, x, c1, inx, 0
       }
     }
     ;need to check if we just corrected a 3-D
     if num_dim(xsave) eq 3 then {
       x = short(x)
       xsave(0,0,0) = x
       exchange, x, xsave
     } 
   }
 }
 endsubr
 ;========================================================
