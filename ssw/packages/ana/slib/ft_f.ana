func ft,xin,cutb,cutt,pq
 ;1/17/91 name change from fasttrim to ft (tired of typing the longer one)
 ;returns a byte image which is the input image XIN scaled over a range
 ;which cuts off the top and bottom fractions CUTB and CUTT
 ;if CUTT is not given, a value of 1.0-CUTB is used; if neither is given,
 ;then .001 (0.1%) is trimmed off both ends
 ncase !narg-1
 { cq=0.001	cq2=0.999	pow=1.0}
 { cq=cutb	cq2=1.0-cq	pow=1.0}
 { cq=cutb	cq2=cutt	pow=1.0}
 { cq=cutb	cq2=cutt	pow=pq }
 else { ty,'FT doesn''t accept',!narg,' arguments'	return,0 }
 endcase
 ;use a subset of the image for the histogram
 ;9/5/2010 - change to just take every 10th for  better 1-D interface
 nx=dimen(xin,0)		ny=dimen(xin,1)
 np = (nx * ny)/10
 if np gt 10 then {
   in = indgen(lonarr(np)) * 10
   subim=xin(in)
//  if nx gt 10 then {
//    np=(ny/10)>1	    dy=ny/np	im=indgen(lonarr(1,np))*nx*dy+10*nx
//    in=lonarr(nx,np)	in=indgen(in,0)
//    in=in+im	subim=xin(in)
 } else	subim=xin
 xq = min(subim)
 mxq = !lastmax
 ;;ty, 'min, max =', xq, mxq
 sfac = 1.0
 if symdtype(subim) gt 1 then {
   ;consider range reduction if we have a very large spread here
   ;;ty,'(mxq - xq) = ',mxq - xq 
   if (mxq - xq) gt 200000 then {
     sfac  = 200000.0/(mxq - xq)
     subim = subim * sfac
     xq = xq * sfac
     ;this will also make subim FP
     sfac = 1.0/sfac	;for use on the ranges later
     ty,'sfac =', sfac
   }
 }
 if symdtype(subim) ne 0 then {
   ;3/25/98 - if the range is too large, we cannot subtract a large negative min
   ;from a large positive value without rollover, but safe for any positive min
   if xq lt 0 then hin=histr(subim) else hin=histr(subim-xq)
   ;when submin has neg values, the histr routine accounts for them but if
   ;all positive, the first hin will be for 0
   ;to get a good display when there is a "no data" value that is a
   ;large negative #, check the first entry
   if (xq le -10000) {
   	if (hin(0) lt 1.0) {
		hin = hin - hin(0)
		hin = hin/max(hin)	;renormalize
	}
   }	
 } else {
   ;the byte case
   xq=word(xq)
   hin=histr(subim)	hin=hin(xq:*)
 }
 if isscalar(hin) then hin = [hin]
 hinx=indgen(hin)+xq
 ;catch narrow range FP images here and just scale them
 if symdtype(xin) ge 3 and num_elem(hin) lt 20 then {
   range = sfac * [min(xin), max(xin)]
 } else {
   ;normal case
   if num_elem(hin) gt 1 then range = sfac * polate(hin,hinx,[cq,cq2]) else range = sfac * [hinx,hinx]
   ;note, if the bottom or top bins are populated more than the trim factor,
   ;we have a problem, therefore insure that the range doesn't exceed
   ;the data limits as given by hinx(0) and max(hinx)
   range(0)=range(0)> (sfac * hinx(0))
   range(1)=range(1)< (sfac * max(hinx))
 }
 ;;type,'range',range
 $trim_range = range		;save for possible use
 r0 = range(0)		r1 = range(1)
 ;;ty,'r0, r1 =', r0, r1
 if pow ne 1.0 then return,pscale(xin, r0, r1, pow)
 ;conditional below to bypass bug with F*8 arrays, remove when distributions debugged
 ;;if symdtype(xin)  eq 4 then return, scale(float(xin), r0, r1)
 ;;  else return, scale(xin, r0, r1)
 return, scale(xin, r0, r1)
 endfunc
 ;===========================================================================
