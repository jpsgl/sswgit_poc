block prepaths
 ;this should be called at beginning to init path info arrays, it
 ;can be called again to zap all the current info and start over
 ;a place for slice numbering info
 ;this contains new slice definitions 11/10/94
 max_slices=200
 max_nodes=50
 if defined(#expand_fac) eq 0 then #expand_fac=1
 $slice_info=fltarr(5,max_slices)        ;max_slices slices max
 ;the 5 items here are # of nodes, width, scale (in arc sec/pixel),
 ; first time index, last time index
 zero,$slice_info
 $nslice=0
 first_slice=0
 xd=fltarr(max_nodes)    xd=zero(xd)
 yd=xd   ;our discrete path points, up to max_nodes
 $xnodes=zero(fltarr(max_nodes,max_slices))-word(1)      $ynodes=$xnodes
 endblock
 ;============================================================================
func get_ts_times(w2, im)
 ;used in 2 places now, more later?
 nts = w2 - 20	;assumes we are in 20 - 24 range
 ;returns tinx and also sets the global $ts_times
 if im eq 0 then {
  ;;get_nx_ny_nt, $data15, nx_all, ny_all, nt
  nt = get_nt($data15)
  xq = $MOVIE_TIMES(0:(nt-1))
  ;in case we have undefined times
  if (xq(0) lt 0.0) then xq = indgen(fltarr(nt))
 } else {
  errormess, 'no support for multi-movies yet'
  return, 0
 }
 dt = ts_timescale_read(w2)
 ;should always have the $movie_times array populated though could be fake
 ;;ty,'get_ts_times, dt =', dt
  if dt lt 0 then {
    tinx = indgen(lonarr(nt))
    ;;ty,'tinx in get_ts_times'
    ;;d, tinx
      ;shouldn't be able to get a dt of 0 unless the time slice view exists but
      ;check here anyhow
    $ts_times(nts) = xq
  } else {
    if dt eq 0 then {
      dt = mean(differ(xq))
      ;shouldn't be able to get a dt of 0 unless the time slice view exists but
      ;check here anyhow
      if $viewts_text_widgets(0, w2) then
	xmtextfieldsetstring, $viewts_text_widgets(0, w2), string(dt)
    }
    ntinx = (max(xq) - !lastmin)/dt
    ;;ty,'ntinx = ', ntinx
    tinx = indgen(lonarr(ntinx)) * dt
    tbase = xq(0)
    $ts_times(nts) = tinx + tbase
    tinx = nearest(tinx, xq - tbase)
    xq = 0
  }
 return, tinx
 endfunc
 ;============================================================================
subr tstscale_cb, w2
 ;call back for changing the t scale, also called for spatial scale
 ;actually does both and the final extractline
 nts = w2 - 20
 ;assumes $data15 for initial testing
 tinx = get_ts_times(w2, 0)
 nt = dimen(tinx,0)
 ;and we need to make the paths, based on spatial zoom factor
 zms = $viewts_space_zoom_fac(w2)
 ;local copies so we don't have to extract from sym arrays more than once
 ts_dx = $ts_dx(nts)	ts_dy = $ts_dy(nts)
 ;11/25/2002 - we now have more than one type, lines and circles

 if $ts_type(nts) eq 1 then {
   ;the line case
   len=sqrt( (ts_dx)^2 +(ts_dy)^2 )
   len = len * zms
   if len le 0 then {
     errormess, 'slice has zero length\n(or a zoom problem)\nplease define a good slice'
     return
   }
   np=fix(len)+1
   xs=indgen(fltarr(np))*ts_dx/len
   ys=indgen(fltarr(np))*ts_dy/len
 } else if $ts_type(nts) eq 2 then {
   ;circle case, just one radius so far (no ellipse yet)
   r = ts_dx
   len=r*#2pi
   len = len * zms
   ;and always define theta = 0 along +x and run CCW
   np = fix(len)+1
   th = indgen(fltarr(np))*#2pi/len
   xs = r*cos(th)
   ys = -r*sin(th)    
 } else { errormess,'illegal slice type\ninternal error?'  return }
 redim, xs, np, 1
 redim, ys, np, 1

 xs = xs + $ts_x1(nts)
 ys = ys + $ts_y1(nts)
 vname = '$view_data' + string(w2)
 
 slice = extractline($data15, xs, ys, tinx)
 exchange, slice, eval(vname)
 viewts, w2
 ;also update the solar coordinate stuff, the corners haven't changed but
 ;the time interval may be different
 ;but we need these for all the interpolated times, for that we have tinx
 ;note the extra work for the symbol array formulation
 xs = $ts_sunx1_native(nts)
 $ts_sunx1(nts) = xs(tinx)
 xs = $ts_suny1_native(nts)
 $ts_suny1(nts) = xs(tinx)
 ;note that $ts_sunx1 and friends are used for mouse tracking and possible
 ;future uses of this slice in other movies, not used for generating the slices
 ;here
 endsubr
 ;============================================================================
func ts_timescale_read(win)
 ;read the text box containing the time scale, some codes as well as the
 ;usual FP value
 ;if the widget doesn't exist yet, assume native
 zq = -1
 iq = $viewts_text_widgets(0, win))
 if iq then {
  s = upcase(xmtextgetstring(iq)
  zq = float(s)
  ;but some other possibilities
  if strpos(s, 'NATIVE') ge 0 then zq = -1
  if strpos(s, 'MEAN') ge 0 then zq = 0
  ;and if we got a -1, make sure we put in a native
  if zq lt 0 then xmtextfieldsetstring, $viewts_text_widgets(0, win), 'native'
 }
 $viewts_t_scale(win) = zq
 return, zq
 endfunc
 ;============================================================================
subr ts_xzoom_cb, win
 ;call back for zooming the x scale
 ;;ty,'ts_xzoom_cb called, $option_value =', $option_value
 ;the $radio button varies from 0 to 7 corresponding to factors of
 ;0.25, 0.5, 1, 2, 3, 4, 8, 16
 ;presently this is done by resampling along the line, may always be
 ;the best (though slowest) way
  zm = $option_value - 1
  if zm gt 4 then { if zm eq 5 then zm=8 else if zm eq 6 then zm=16 }
  if zm le 0 then { if zm eq 0 then zm= 0.5  if zm eq -1 then zm= 0.25 }
  ;entry 7 is special, this allows any value to be entered in a text box
  iq = $viewts_text_widgets(1, win)
  if zm eq 7 then {
   ;manage the text box but don't use the value if 0 or less
   xtmanage, iq
   ts_xzoomentry_cb, win
   return
  } else {
   ;remember to unmanage it if we aren't using it
   if xtismanaged(iq) then xtunmanage, iq
  }
  ;try erasing whenever zooming down to avoid leftover junk when window
  ;doesn't resize to fit (no shrinkwrap)
  if zm lt $viewts_space_zoom_fac(win) then erase, win
  $viewts_space_zoom_fac(win) = zm
  tstscale_cb, win
 endsubr
 ;============================================================================
subr ts_xzoomentry_cb, win
 ;don't use the value if 0 or less
 zm = float(xmtextgetstring($viewts_text_widgets(1, win)))
 if zm le 0 then return
 ;doesn't resize to fit (no shrinkwrap)
 if zm lt $viewts_space_zoom_fac(win) then erase, win
 $viewts_space_zoom_fac(win) = zm
 ;;ty,'zm =', zm
 tstscale_cb, win
 endsubr
 ;============================================================================
subr tsmode_cb
 ;call back for zooming the x scale
 ty,'tsmode_cb called'
 endsubr
 ;=============================================================================
subr viewts, win
 ;a version of view specialized for time slices
 ;uses the view_data directly and no data array
 ;;ty,'win = ', win
 dst = '$view_data'+string(win)

 if num_dim(eval(dst)) ne 2 then { ty,'VIEW - array must be 2-D' return, 0 }
 nx = dimen(eval(dst),0)	ny = dimen(eval(dst),1)
 $view_nx(win) = nx
 $view_ny(win) = ny
 ;note that $view_zoom_fac should be 1 for these since zones are not
 ;done by tvplane
 $view_zoom_fac(win) = 1
 sw = string(win)		;a bit indirect

 if defined(eval('$viewer'+string(win))) eq 0 then {
 $view_scaletype(win) = 5
 $win = win
 
 ;doing this in a compile file causes a crash with a segment error in
 ;malloc, don't know why yet but somehow the xmtoplevel_form sets it up
 ;to happen, it does not happen on the PC's (not sure about the Sun's)
 sw = string(win)		;a bit indirect
 viq = xmtoplevel_form(0,0,'X' )
 equate, eval('$viewer'+sw), viq
 ;;;compile_file, getenv('ANA_WLIB') + '/viewts_widgettool'
 ;--------------
 
 s1 = 'File'	s2 = 'Display'	s3='Options'   s5='Help'
 bar = xmmenubar(viq, $f4, 'gray',s1, s2, s3, s5)
 xmattach, bar(0), 0, 1,1,1,0
 xmsetmnemonic, bar(1), 'F'
 xmsetmnemonic, bar(2), 'D'
 xmsetmnemonic, bar(3), 'O'
 xmsetmnemonic, bar(4), 'H'
 ;pull down menu for file
 s1 = 'load file'
 s2 = 'save'
 ;s3 = 'save full data'
 s4 = 'print'
 s5 = 'hide'
 s10 = 'destroy'
 pd1=xmpulldownmenu(bar(0), bar(1), 'load_tsfile,'+sw, $f4, 'gray', s1,s2,s4,s5,s10)
 $file_menu = pd1

 ;pull down menu for display, these options differ from image viewer
 s1 = 'image mapping'
 s2 = 'view range'
 s3 = 'redraw'
 s4 = 'erase'
 s5 = 'hide/show info box'
 pd1=xmpulldownmenu(bar(0),bar(2),'viewts_display,'+sw,$f4,'gray',s1,s2,s3,s4,s5)

 ;pull down menu for options
 s0 = 'no options yet'
 pd1=xmpulldownmenu(bar(0), bar(3), 'viewts_option,'+sw, $f4, 'gray',s0)
 ;pull down menu for help
 s1 = 'General Instructions'
 s2 = 'time slices'
 pd1=xmpulldownmenu(bar(0), bar(4), 'viewts_help,'+sw, $f4, 'gray', s1, s2)

 ;get a board for the top, called bq
 bq = xmboard(viq, 0, 0, 0, 0)
 ;the timeslice version has its own pixmaps
 for i=0,7 do {
 equate, eval('s'+string(i+1)), getenv('ANA_PIXMAPS')+'/orientatets.'+string(i)
 }
 dm = 0

 $view_order($win) = 0		;10/24/97 make default 0
 ;may not work the same, check out
 op2=xmpixmapoptionmenu(bq,'orient_cb,'+sw, $f1, '', ''-
 ,s1,s2,s3,s4,s5,s6,s7,s8, dm)
 xmposition, op2(0), 0, 5
 ;to support connected windows, we have to save the orientation widget
 ;numbers
 $view_order_widgets(0,$win) = op2(0:8)	;better be just 9 elements

 ;the x zoom is done as an option menu here
 s00 = smap(byte(188))	s0 = smap(byte(189))
 s1 = '1'  s2 = '2'  s3 = '3'  s4 = '4' s5 = '8'  s6 = '16' s7 = 'entry'
 iq = xmoptionmenu(bq, 'ts_xzoom_cb,'+sw,$f4,'','s  zoom',s00,s0,s1,s2,s3,s4,s5,s6,s7,2)
 xmposition, iq(0), 90, 5
 $viewts_space_zoom_fac($win) = 1.0	;not the button number but the actual zoom value
 ;for the arbitrary entry we have a text box, normally unmanaged
 xq = xmtextfield(bq,'',4,'ts_xzoomentry_cb,'+sw,$f3,'white')
 xmposition, xq, 230, 5, 50, 33
 xtunmanage, xq
 $viewts_text_widgets(1, $win) = xq

 ;the time scale is specified via a text entry box, it will be blank here and
 ;loadable by the user or set to mean time interval
 xmposition, xmlabel(bq,'t scale',$f4), 92, 47
 xq = xmtextfield(bq,'',10,'tstscale_cb,'+sw,$f3,'white')
 xmposition, xq, 151, 42, 70, 35
 $viewts_text_widgets(0, $win) = xq
 xmposition, xmlabel(bq,'s/pixel',$f4), 230, 47
 $viewts_t_scale($win) = -1	;-1 for native pixels, 0 for mean time

 $view_zoom_widgets(0,$win) = iq(1:*)
 zm = $viewts_space_zoom_fac($win)  ;preset to one but for a copy we get copied value


 ;a label for image info, esp for TRACE
 $view_text_widget($win) = xmtextbox(bq, 5, 30, $f8,'')
 xmtextseteditable, $view_text_widget($win), 1
 xmposition, $view_text_widget($win), 282, 0

 sc = xmscrolledwindowapp(viq, 4, 4)
 formvstack, bar(0), bq, sc
 xmattach, sc, 0, 1,1,0,1

 ;limit initial size of this window to 512x768
 ix = $view_nx($win) < 512
 iy = $view_ny($win) < 768
 $view_xsize($win) =ix
 $view_width($win) =ix
 $view_ysize($win) =iy
 $view_height($win) =iy
 $view_draw($win) = xmdrawingarea(sc, $win, ix, iy, 'view_input_cb,'+sw, -
 	'view_resize_cb,'+sw,'view_expose_cb,'+sw)
 iq = ix/zm
 ;are we part of a connected set of views ?
 if $view_connect($win) then {
 	nxs = $view_super_nx
 	nys = $view_super_ny
 	} else {  nxs = $view_nx($win)  nys = $view_ny($win) }
 $view_sch($win) = xmhscrollbar(sc, nxs, iq, .5*iq, 'view_scrollh_cb,'+sw, 1)
 iq = iy/zm
 $view_scv($win) = xmvscrollbar(sc, nys, iq, .5*iq, 'view_scrollv_cb,'+sw, 1)
 xmsize, $view_sch($win), ix, 20
 xmsize, $view_scv($win), 20, iy
 $view_ix($win) = 0
 $view_iy($win) = 0
 ;also set the center, we want to try to maintain the center when zooming
 ;and changing orientation
 $view_ixc($win) = ix/2
 $view_iyc($win) = iy/2
 $window_title($win) = 'time slice viewer '+sw
 $viewdespikestatus($win) = 0	;not used for time slices
 ;start without a text box
 info_box_hide_toggle, $win
 ;--------------

 win = $win	;needed in case we called view from top level
 viq = eval('$viewer'+sw)
 xmsettitle, viq, $window_title(win)
 xtpopup, viq
 xtloop, 2
 $view_exists(win) = 1
 } else {
 viq = eval('$viewer'+str(win))
 xtpopup, viq
 }
 ;the time slice array is always prepared for iorder = 0 display, if the
 ;current display is not 0, we have to flip it here
 or2 = $view_order(win)
 if or2 then re_orient, 0, or2, win
 
 view_recompute, win
 endsubr
 ;=============================================================================
func tswidget_widget(iq)
 
 if defined($tswidget) eq 0 then {
 compile_file, getenv('ANA_WLIB') + '/timeslicewidgettool'
 }
 return, $tswidget
 endfunc
 ;=============================================================================
func no_ts_link(xq)
 ;return 1 if no valid time slice window link or some other problem
 ;the link window is the one containing the movie
 $ts_win = 0
 s = xmtextfieldgetstring($tstext1)
 if num_elem(s) le 0 then {
   errormess, 'no link window\nspecified for the slice'
   return, 1 }
 $ts_win = fix(s)
 return, 0
 endfunc
 ;=============================================================================
subr setup_slicer, wslice, sw
 if $ts_win eq #play_window then {
  sw = '$data'+string($ts_win)
 } else {
  iq = $ts_win - $play_window(0)
  sw = '$movie'+string(iq)
 }
 $view_tai(wslice) = $movie_times(0)
   
 ;we also have to save the "movie" orientation somehow
 $ts_source_order = $mv_order
 $view_sun_pixel_scale(wslice) =  $movie_pixsc(0)
 endsubr
 ;=============================================================================
subr line_slicer_cb
 ;this routine should only be called if $tswidget has been created
 ;check for a linked window, don't accept a blank
 ;11/14/2002 - should now always have a $movie_times array, might be fake
 if no_ts_link() then return
 ;not very standard yet, the single movie is still called $data15 and the
 ;others are $movie#, we could let line_slicer sort it out ...
 setup_slicer, $lineslicewin, sw
 $ts_type($lineslicewin - 20) = 1	;for a line
 line_slicer, eval(sw), $ts_win, $lineslicewin
 endsubr
 ;=============================================================================
subr slice_window_cb, iq
 ty,'line_slicer_cb called, $option_value = ',$option_value
 win = $option_value + 20
 ncase iq
   { $lineslicewin = win }
   { $xslicewin = win }
   { $yslicewin = win }
   { $cslicewin = win }
 endcase
 endsubr
 ;=============================================================================
subr xyslicer_cb
 ty,'xyslicer_cb called'
 if no_ts_link() then return
 setup_slicer, $xslicewin, sw
 ;the y slice window is slaved to the x slice window, this may
 ;cause some user confusion
 $view_sun_pixel_scale($yslicewin) = $view_sun_pixel_scale($xslicewin)
 $view_tai($yslicewin) = $view_tai($xslicewin)
 $ts_type($xslicewin - 20) = 1	;for a line
 $ts_type($yslicewin - 20) = 1	;for a line
 xyslicer, eval(sw), $ts_win, $xslicewin, $yslicewin
 endsubr
 ;=============================================================================
subr cslicer_cb
 ty,'cslicer_cb called'
 if no_ts_link() then return
 setup_slicer, $cslicewin, sw
 $ts_type($cslicewin - 20) = 2	;for a circle
 c_slicer, eval(sw), $ts_win, $cslicewin
 endsubr
 ;=============================================================================
subr line_slicer,cube,w1,w2
 ;intended for prepared movies cubes and their associated times and positions
 ;w1 is the movie window and w2 is the destination
 ;w2 is restricted to the time slice window zone, 20 - 24
 if no_movie() then return
 xflush
 xwin,w1
 pen,1		pencolor,'red'
 nxp=!ixhigh+1		nyp=!iyhigh+1
 xwin,-1,nxp,nyp		;a pixmap which is only as large as needed
 xcopy,w1,-1
 ;a more general form may be needed to handle file sets
 get_nx_ny_nt, cube, nx_all, ny_all, nt
 ty,'line_slicer, nt =', nt

;setup the vname and set to clear outside the current slice (restore
 ;previous setting before we exit)
 vname = '$view_data' + string(w2)
 save_tvclear = !tvclear_outside
 !tvclear_outside = 1
 
 ;tinx is used by extractline to get specific images for each index,
 ;this can be used to nearest neighbor interpolate on a time scale
 ;for future work, some logic to save the tinx result and only recompute
 ;when necessary would be useful, it often doesn't change from call to call
 
 tinx = get_ts_times(w2, 0)
 xwin,w1
 xtloop, 2
 !kb=0

 ;nothing will change now, so get some window parameters we need later
 ;for conversion from screen to view (or data) space
 ;for movies, the display order is presently always 2 although the movie
 ;may not be in proper solar orientation (this is specified by $mv_order)
 zms = $viewts_space_zoom_fac(w2)
 zm = $view_zoom_fac(w1)
 if zm lt 0 then { zm = 1.0/abs(zm) }	;make zm fp, so have to "fix" a lot
 nxd = $view_nx(w1)
 nyd = $view_ny(w1)
 xoff = $mv_track_dx + $view_ix(w1)
 yoff = $mv_track_dy + $view_iy(w1)
 redim, xoff, 1, nt
 redim, yoff, 1, nt
 
 ;get first position
 while (1) {	;wait until left button pushed in the target window
  if !kb ge 256  and xquery(w1) eq 1 then break
  xtloop, 2
 }
 xymov,!ix,!iyhigh-!iy,-1
 ix=!ix		iy=!iy
 ixs=ix		iys=!iy
 xlast = ix	ylast = iy

 ;get location and draw line

 while (1) { if xquery(w1) then {
 if !ix ne ix or !iy ne iy then {
  xlast = ix	ylast = iy
  ix=!ix	iy=!iy
  xcopy,-1,w1
  ;cope with the num lock key by doing mod 16
  if !kb%16 eq 1 then {
  ;move both ends for this option, this allows moving the line
  dx = ix - xlast	dy = iy - ylast
  ixs += dx
  iys += dy
  }
  if !kb%16 eq 4 then {
  ;;if !kb%256 eq 4 then
  ;keep center fixed
  xc = 0.5*(ixs + xlast)	yc = 0.5*(iys + ylast)
  ixs = 2*xc - ix	iys = 2*yc - iy
  }
  xdrawline,ixs,iys,ix,iy,w1
  ;also draw a + at the center
  xc = (ixs+ix)/2	yc = (iys+iy)/2
  xdrawline, xc - 6, yc, xc +6, yc, w1
  xdrawline, xc, yc - 6, xc, yc +6, w1

  ;now convert ends to image coordinates, this is similar to convertfordata
  ;but specialized for this job, some parameters that don't change were
  ;setup before the loop, this is safe because we have no xtloop here

  x2 = fix(ix/zm)
  x1 = fix(ixs/zm)
  y2 = fix(iy/zm)
  y1 = fix(iys/zm)
  dx = x2 - x1		dy = y2 - y1
  len=sqrt( (dx)^2 +(dy)^2 )
  len = len * zms
  if len gt 0 then {
   np=fix(len)+1
   xs = x1 + indgen(fltarr(np))*dx/len
   ys = y1 + indgen(fltarr(np))*dy/len
   redim, xs, np, 1
   redim, ys, np, 1
   xs = xs + xoff
   ys = ys + yoff
   ;for symarr's and cubes, now have a C module that does much of the work
   slice = extractline(cube, xs, ys, tinx)
   exchange, slice, eval(vname)
   viewts, w2
   }
  }
 if !kb eq 0 then break
 if !kb eq 16 then break
 } }
 ;keep the line information in a global for temporal adjustments

 x1 = x1 + xoff
 y1 = y1 + yoff
 slice_globals, w2, x1, y1, dx, dy, nxd, nyd, nt, tinx

 ;save path
 ;the 3 items here are # of nodes, width, scale (in arc sec/pixel)
 ;$slice_info(0,$nslice)=2
 ;$slice_info(1,$nslice)=1
 ;$slice_info(2,$nslice)=$scale
 ;$slice_info(3,$nslice) = $j1
 ;$slice_info(4,$nslice) = $j2
 ;$xnodes(0,$nslice)=[ixs, ix]
 ;$ynodes(0,$nslice)=[iys, iy]
 ;$nslice=$nslice+1

 !tvclear_outside = save_tvclear
 xflush
 endsubr	;end of line_slicer
 ;=============================================================================
func nearest(t1, t2)
 ;returns the  indices in t1 that have the nearest values to each
 ;entry in t2
 ;result has same size as t1 and represents, for example, the index numbers
 ;of the images with t2 time to use for comparison with the images with t1
 ;times
 ;done in an inefficient n*m way
 n=num_elem(t1)
 near=lonarr(n)
 zero,near
 for i=0,n-1 do {
 near(i) = minloc( abs(t2 - t1(i)) )
 }
 return, near
 endfunc
 ;=============================================================================
subr line_slicer2, cube, cube2, tcindex, w1, w2, w3, w4, color
 ;slices 2 cubes with different time bases with both on the first time
 ;base (the information for this is in the tcindex array)
 ty,'w1,w2,w3', w1,w2,w3
 xflush
 xwin,w1
 pen,1		pencolor,color
 nx=dimen(cube,0)	ny=dimen(cube,1)	nt=dimen(cube,2)
 toffsets=nx*ny*indgen(lonarr(nt))
 if dimen(tcindex,0) ne nt then {
 	ty,'the tcindex in line_slicer2 has wrong size'  return }
 toff2 = nx*ny*tcindex
 nxy=rfix(sqrt(nx*nx+ny*ny)
 ;but this is always bigger than we want, so cut in half
 nxy = nxy/2
 last_len=0
 redim,toffsets,1,nt
 redim,toff2,1,nt
 nx=nx<(!ixhigh+1)	ny=ny<(!iyhigh+1)
 nxm=nx-1		nym=ny-1
 ;xwin,-1,nx,ny		;a pixmap which is only as large as needed
 ;xcopy,w1,-1
 xwin,w2,nxy,nt,790,312
 xwin,w3,nxy,nt,990,312
 xwin,w1
 ;get first position
 ;hairs
 !kb=0
 ;wait for down button
 wait, .5	;a little lag to clear menu
 !kb=0
 while (!kb eq 0) {
 xquery,w1
 }
 xymov,!ix,!iyhigh-!iy,-1
 ix=!ix		iy=!iy
 ixs=ix		iys=!iy
 xlast = ix	ylast = iy
 xslast = ix	yslast = iy
 ;get location and draw line
 while (1) { xquery, w1
 if !ix ne ix or !iy ne iy then {
  ;xcopy,-1,w1
  if !kb%256 eq 1 then {
  ;move both ends for this option, this allows moving the line
  dx = ix - xlast	dy = iy - ylast
  ixs += dx
  iys += dy
  }
  if !kb%256 eq 4 then {
  ;keep center fixed
  xc = 0.5*(ixs + xlast)	yc = 0.5*(iys + ylast)
  ixs = 2*xc - ix	iys = 2*yc - iy
  }
  xinvertline, xslast, yslast, xlast, ylast, w1
  xinvertline, ixs, iys, ix, iy, w1
  xinvertline, xslast, yslast, xlast, ylast, w4
  xinvertline, ixs, iys, ix, iy, w4
  xlast = ix	ylast = iy
  xslast = ixs	yslast = iys
  ;xdrawline,ixs,iys,ix,iy,w1
  dx = ix-ixs		dy = iy-iys
  len=sqrt( (dx)^2 +(dy)^2 )
  if len gt 0 then {
   np=fix(len)+1
   xs=indgen(fltarr(np))*dx/len + ixs
   ys=indgen(fltarr(np))*dy/len + iys
   in=rfix(xs) + nx*rfix(ys)
   redim,in,np,1
   indx = in + toffsets
   slice=reverse(cube(indx),1)	;note reversal in time to make t increase up
   tv,ft(slice),0,0,w2
   indx = in + toff2
   slice2=reverse(cube2(indx),1)  ;note reversal in time to make t increase up
   tv,ft(slice2),0,0,w3
   ;a partial erase is done if we are shrinking, a full erase causes
   ;flickering
   if len lt last_len then {
   erase,w2,len		;erase everything to right of slice
   erase,w3,len
   }
   last_len = len
   }
  }
 if !kb eq 0 then break
 ix=!ix<nxm	iy=!iy<nym
 }
 ;save path
 ;the 3 items here are # of nodes, width, scale (in arc sec/pixel)
 ;$slice_info(0,$nslice)=2
 ;$slice_info(1,$nslice)=1
 ;$slice_info(2,$nslice)=$scale
 ;$slice_info(3,$nslice) = $j1
 ;$slice_info(4,$nslice) = $j2
 ;$xnodes(0,$nslice)=[ixs, ix]
 ;$ynodes(0,$nslice)=[iys, iy]
 ;$nslice=$nslice+1

 xflush
 endsubr
 ;============================================================================
subr xyslicer,cube,w1,w2,w3
 ;11/24/2002 - finally get working for space/time views
 ;8/21/2000 - upgrade for symarr movies, need lots of other work too
 if no_movie() then return
 xflush
 xwin,w1
 pen,1		pencolor,'red'
 nxp=!ixhigh+1		nyp=!iyhigh+1
 xwin,-1,nxp,nyp		;a pixmap which is only as large as needed
 xcopy,w1,-1
 ;a more general form may be needed to handle file sets
 get_nx_ny_nt, cube, nx_all, ny_all, nt
 ;;ty,'xy slicer, nt =', nt

 ;setup the 2 vname's and set to clear outside the current slice (restore
 ;previous setting before we exit)
 vnamex = '$view_data' + string(w2)
 vnamey = '$view_data' + string(w3)
 save_tvclear = !tvclear_outside
 !tvclear_outside = 1

 ;the 2 windows (for x and y) could have different times and zooms set,
 ;there might be a problem if the previous settings are inconsistent with
 ;a new movie, this is not properly checked yet
 tinx = get_ts_times(w3, 0)
 tinx = get_ts_times(w2, 0)
 
 xwin,w1
 xtloop, 2
 !kb=0

 zms = $viewts_space_zoom_fac(w2)
 zm = $view_zoom_fac(w1)
 if zm lt 0 then { zm = 1.0/abs(zm) }	;make zm fp, so have to "fix" a lot
 nxd = $view_nx(w1)
 nyd = $view_ny(w1)
 xoff = $mv_track_dx + $view_ix(w1)
 yoff = $mv_track_dy + $view_iy(w1)
 redim, xoff, 1, nt
 redim, yoff, 1, nt
 d, xoff, yoff
 ;some of the arrays are fixed for x/y slices, in data space the x
 ;range is (xoff) to (xoff+nxp-1)
 xsx = indgen(lonarr(nxp))
 redim, xsx, nxp, 1
 xsx = xsx + xoff
 ysy = indgen(lonarr(nyp))
 redim, ysy, nyp, 1
 ysy = ysy + yoff
 
 xsy = zero(xsx) + yoff
 ysx = zero(ysy) + xoff
 

 ;get first position
 while (1) {	;wait until left button pushed in the target window
  if !kb ge 256  and xquery(w1) eq 1 then break
  xtloop, 2
 }
 ix=!ix		iy=!iy
 ixs=ix		iys=!iy
 xlast = ix	ylast = iy

 ix = -100	iy = -100
 while (1) { if xquery(w1) then {
 if !ix ne ix or !iy ne iy then {
  ix=!ix	iy=!iy
  xcopy,-1,w1
  ;draw the cross hairs
  xdrawline,0,iy,!ixhigh,iy,w1
  xdrawline,ix,0,ix,!iyhigh,w1
  
  ;now convert ends to image coordinates, this is similar to convertfordata
  ;but specialized for this job, some parameters that don't change were
  ;setup before the loop, this is safe because we have no xtloop here

  x2 = fix(ix/zm)
  y2 = fix(iy/zm)
  
  xslice = extractline(cube, xsx, xsy + y2, tinx)
  yslice = extractline(cube, ysx + x2, ysy, tinx)
  exchange, xslice, eval(vnamex)
  exchange, yslice, eval(vnamey)
  viewts, w2
  viewts, w3
   
  ;;xlabel,ist(iy),0,10,w2
  ;;xdrawline,ix,0,ix,!iyhigh,w2
  ;;xlabel,ist(ix),0,10,w3
  ;;xdrawline,iy,0,iy,!iyhigh,w3
 }
 if !kb eq 0 then break
 if !kb eq 16 then break
 } }
 ;need to save the final positions for adjustments and mouse measurements
 ;there are 2 sets for the xy slicer, first do the x set
 x1 = xoff
 y1 = yoff + y2
 dx = nxp - 1
 dy = 0
 slice_globals, w2, x1, y1, dx, dy, nxd, nyd, nt, tinx
 
 y1 = yoff
 x1 = xoff + x2
 dx = 0
 dy = nyp - 1
 slice_globals, w3, x1, y1, dx, dy, nxd, nyd, nt, tinx
 
 !tvclear_outside = save_tvclear
 endsubr
 ;=============================================================================
subr slice_globals, win, x1, y1, dx, dy, nxd, nyd, nt, tinx
 ;11/25/2002 - should work for circles, interpreting x1, y1 as the
 ;center and dx, dy as radius (both the same for circles)
 ;assumes only a single slicer link at a time and that the link is
 ;to the current "main" movie
 ;modifies x1, y1, dx, and dy
 wts = win - 20
 ss = $view_sun_pixel_scale(win)	;was set in line_slice_cb
 $ts_x1(wts) = x1
 $ts_y1(wts) = y1
 $ts_dx(wts) = dx
 $ts_dy(wts) = dy
 nx = nxd
 ny = nyd
 ;if available, also save the solar positions and increments in arc sec,
 xcen = $movie_xc	;assumes main movie
 ycen = $movie_yc
 ;already have usual nx and ny from nxd and nyd
 ;note that slicer codes need the view coords which may not be in the "proper"
 ;solar x/y directions, hence the calculations of $ts_sunx1 and $ts_suny1
 ;need to consider these situations, the "solar" order to use is in the
 ;view_order of the time slice window
 iord = $ts_source_order
 transpose = iord gt 4
 if transpose then { exchange, nx, ny }  
 ;get the solar coordinates of the corner
 xcorner = xcen - (.5*nx -.5)*ss
 ycorner = ycen - (.5*ny -.5)*ss
 redim, x1, nt
 dx = dx * ss

 redim, y1, nt
 dy = dy * ss
 ;these are not necessarily in the proper solar directions
 if transpose then { exchange, x1, y1  exchange, dx, dy }
 ;reverse direction of x and y as needed
 if iord lt 2   then { y1 = ny-1-y1   dy = -dy }
 if iord%2 eq 1 then { x1 = nx-1-x1   dx = -dx }
 x1 = xcorner + x1*ss
 y1 = ycorner + y1*ss
 ;but we need these for all the interpolated times, for that we have tinx
 ;but also save in native index in case we change tinx
 $ts_sunx1_native(wts) = x1
 $ts_suny1_native(wts) = y1

 $ts_sunx1(wts) = x1(tinx)	x1 = 0
 $ts_suny1(wts) = y1(tinx)	y1 = 0
 $ts_sundx(wts) = dx		dx = 0
 $ts_sundy(wts) = dy		dy = 0

 endsubr
 ;=============================================================================
subr redraw_slice, wts
 xbase = $ts_sunx1(wts)
 ybase = $ts_suny1(wts)
 ;this is simple for a limb case and the movie window
 xbase = xbase(0)
 ybase = ybase(0)
 xlast = xbase + $ts_sundx(wts)
 ylast = ybase + $ts_sundy(wts)
 convertsolar2data_or_view, xbase, ybase, 15, $movie_times(0), 1
 mark, xbase, ybase, 15
 convertsolar2data_or_view, xlast, ylast, 15, $movie_times(0), 1
 xdrawline, xbase, ybase, xlast, ylast, 15
 endsubr
 ;=============================================================================
subr c_slicer, cube, w1, w2
 if no_movie() then return
 xflush
 xwin,w1
 pen,1		pencolor,'red'
 nxp=!ixhigh+1		nyp=!iyhigh+1
 xwin,-1,nxp,nyp		;a pixmap which is only as large as needed
 xcopy,w1,-1
 ;a more general form may be needed to handle file sets
 get_nx_ny_nt, cube, nx_all, ny_all, nt
 ty,'c slicer, nt =', nt

 vname = '$view_data' + string(w2)
 save_tvclear = !tvclear_outside
 !tvclear_outside = 1

 tinx = get_ts_times(w2, 0)
 xwin,w1
 xtloop, 2
 !kb=0

 zms = $viewts_space_zoom_fac(w2)
 zm = $view_zoom_fac(w1)
 if zm lt 0 then { zm = 1.0/abs(zm) }	;make zm fp, so have to "fix" a lot
 nxd = $view_nx(w1)
 nyd = $view_ny(w1)
 xoff = $mv_track_dx + $view_ix(w1)
 yoff = $mv_track_dy + $view_iy(w1)
 redim, xoff, 1, nt
 redim, yoff, 1, nt

 ;get center position
 while (1) {	;wait until left button pushed in the target window
  if !kb ge 256  and xquery(w1) eq 1 then break
  xtloop, 2
 }
 xymov,!ix,!iyhigh-!iy,-1
 ixc=!ix		iyc=!iy
 ix=ixc			iy=iyc
 xlast = ix		ylast = iy

 ;get location and draw circle

 while (1) { if xquery(w1) then {
 if !ix ne ix or !iy ne iy then {
  ix=!ix	iy=!iy
  xcopy,-1,w1
  ;cope with the num lock key by doing mod 16
  if !kb%16 eq 1 then {
  ;move circle center for this option
  dx = ix - xlast	dy = iy - ylast
  ixc += dx
  iyc += dy
  ;the radius is re-computed but should be the same
  }
  ;;if !kb%16 eq 4 then {
  ;may use this to allow ellipses in the future
  ;;}
  ;get the path, this one is a circle with center at (xc, yc)
  ;and a circumference point at (ix, iy), we want to avoid
  ;computing all the circle points for both zoom cases, so
  ;we compute the circle in data space first ...
  
  x2 = fix(ix/zm)
  x1 = fix(ixc/zm)
  y2 = fix(iy/zm)
  y1 = fix(iyc/zm)
  xlast = ix		ylast = iy

  dy=y2 - y1        dx=x2 - x1	;components of the radius
  r=sqrt( (dx)^2 +(dy)^2 )
  if r gt 0 then {
   len=r*#2pi
   ;sample according to the space/time zoom factor
   len = len * zms
   ;and always define theta = 0 along +x and run CCW
   np = fix(len)+1
   th = indgen(fltarr(np))*#2pi/len
   xs = r*cos(th) + x1
   ys = -r*sin(th) + y1
   ;extractline does range clipping so none needed here?
   ;;xs = rfix( (xs < nxm) > 0 )
   ;;ys = rfix( (ys < nym) > 0 )
   
   ;xs and ys don't have xoff, yoff applied yet so we can convert back
   ;to view via zm for drawing
   ;eventually want to draw with the arc drawing routine
   
   ixs = xs * zm	iys = ys * zm
   xdrawline,ixs(0:(np-2)),iys(0:(np-2)),ixs(1:*),iys(1:*),w1
   redim, xs, np, 1
   redim, ys, np, 1
   xs = xs + xoff
   ys = ys + yoff
   ;for symarr's and cubes, now have a C module that does much of the work
   slice = extractline(cube, xs, ys, tinx)
   exchange, slice, eval(vname)
   viewts, w2
   }
   last_len = len
   }
 if !kb eq 0 then break
 if !kb eq 16 then break
 } }
 ;keep the line information in a global for temporal adjustments
 ;for circles we have (x1,y2) as the center and r goes to both dx and dy
 x1 = x1 + xoff
 y1 = y1 + yoff
 slice_globals, w2, x1, y1, r, 0, nxd, nyd, nt, tinx

 !tvclear_outside = save_tvclear
 xflush 
 endsubr
 ;============================================================================
subr store_paths,name,comments
 ;construct an fp array containing the current path information
 ;modified from original version which used I*2, fp used to allow accurate
 ;node positions in expanded images (whatever that might be worth)
 n=$nslice
 iq=fltarr(1+n*dimen($slice_info,0))
 iq(0)=float(n)
 nq=fix(n-1)
 iq(1)=$slice_info(*,0:nq)
 for i=0,nq do { jq=$slice_info(0,i)-1
	 iq=[iq,float($xnodes(0:jq,i)),float($ynodes(0:jq,i))] }
 sq = 'path from slicer, use restore_paths in slicers.ana to decode'
 if defined(comments) ne 0 then sq = sq+#nl+comments
 fzwrite, iq, name, sq
 endsubr
 ;============================================================================
subr restore_paths,name
 ;get back a stored set of path nodes, fp version
 fzread,arry,name
 ;first value is number of paths (or slices or boxes)
 $nslice=fix(arry(0))   n=$nslice-1
 ;a place for slice numbering info
 max_slices=200
 max_nodes=50
 $slice_info=fltarr(5, max_slices)        ;max_slices slices max
 ;next items in file are the elements of $slice_info
 nq=$nslice*dimen($slice_info,0)
 $slice_info(0) = arry(1:nq)
 $xnodes=zero(fltarr(max_nodes,max_slices))-word(1)
 $ynodes=$xnodes
 ic=nq+1
 for i=0,n do {
 jq=$slice_info(0,i)-1
 ic2=ic+jq
 $xnodes(0,i)=arry(ic:ic2)  ic=ic2+1  ic2=ic+jq
 $ynodes(0,i)=arry(ic:ic2)  ic=ic2+1 }
 ty,'path numbers 0 to',n,' available'
 endsubr
 ;===============================================================================
subr save_paths_callback
 ty,smap(byte(7))
 xmprompt,'enter any comments to include', $path_comments, 'get_path_comments', 0
 ;control for this op now passes to get_comments
 endsubr
 ;===============================================================================
subr get_path_comments, data
 $path_comments = $textfield_value
 s = 'path from slicer, use restore_paths in slicers.ana to decode'+#nl
 xmprompt,'enter file name', $save_path_name, 'save_path_file', 0
 ;control for this op now passes to save_file
 endsubr
 ;===============================================================================
subr save_path_file
 ty,'save name = ', $textfield_value
 $save_path_name = $textfield_value
 store_paths, $save_path_name, $path_comments
 ty,smap(byte(7))
 endsubr
 ;=============================================================================
subr four_slicer,cube,w1,w2, color, offset
 ;very, very specialized to look at a quad movie cube (4 panels in each movie
 ;frame) and make all 4 time slices in window w2
 xflush
 xwin,w1
 pen,1		pencolor,color
 nx=dimen(cube,0)	ny=dimen(cube,1)	nt=dimen(cube,2)
 toffsets=nx*ny*indgen(lonarr(nt))
 nxy=rfix(sqrt(nx*nx+ny*ny)
 nxy = nxy
 last_len=0
 redim,toffsets,1,nt

 ;if we have tracking on
 zeroifnotdefined, $mv_track_flag
 if $mv_track_flag then {
   t_dx = $mv_track_dx
   t_dy = $mv_track_dy
   if nt gt dimen(t_dx,0) then {
   	errormess,'movie cube and\ntracking data don''t match?'
   	return }
   t_dx = t_dx(0:(nt-1))
   t_dy = t_dy(0:(nt-1))
   redim, t_dx, 1, nt
   redim, t_dy, 1, nt
 }

 nxp=nx<(!ixhigh+1)	nyp=ny<(!iyhigh+1)
 nxm=nx-1		nym=ny-1
 xwin,-1,nxp,nyp		;a pixmap which is only as large as needed
 xcopy,w1,-1
 if defined(kilroy) eq 0 then xwin,w2,nxy,nt,990,312 else { xwin,w2  erase,w2}
 ;but may not be correct, so
 if !ixhigh ne nxy-1 then xwin,w2,nxy,nt,990,312
 kilroy = 1	;kilroy was here
 xwin,w1
 ;get first position
 xtloop, 2
 !kb=0

 ;nothing will change now, so get some window parameters we need later
 ;for conversion from screen to data space
 zm = $view_zoom_fac(w1)
 if zm lt 0 then { zm = 1.0/abs(zm) }	;make zm fp, so have to "fix" a lot
 orient = $view_order(w1)
 reverse_x = (orient%4)%2 eq 1
 reverse_y = (orient%4) lt 2
 transpose_flag = orient ge 4
 nxd = $view_nx(w1)
 nyd = $view_ny(w1)
 xoff = $view_ix(w1)
 yoff = $view_iy(w1)
 if transpose_flag then  nxtr = nyd  else nxtr = nxd

 while (!kb ne 256) {	;wait until left button pushed
 xquery,w1
 }
 xymov,!ix,!iyhigh-!iy,-1
 ix=!ix		iy=!iy
 ixs=ix		iys=!iy
 xlast = ix	ylast = iy
 ;get location and draw line
 while (1) { xquery
 if !ix ne ix or !iy ne iy then {
  xcopy,-1,w1
  if !kb%256 eq 1 then {
  ;move both ends for this option, this allows moving the line
  dx = ix - xlast	dy = iy - ylast
  ixs += dx
  iys += dy
  }
  if !kb%256 eq 4 then {
  ;keep center fixed
  xc = 0.5*(ixs + xlast)	yc = 0.5*(iys + ylast)
  ixs = 2*xc - ix	iys = 2*yc - iy
  }
  xlast = ix	ylast = iy
  xdrawline,ixs,iys,ix,iy,w1
  ;also draw a + at the center
  ;but not for this specialized version
  ;;xc = (ixs+ix)/2	yc = (iys+iy)/2
  ;;xdrawline, xc - 6, yc, xc +6, yc, w1
  ;;xdrawline, xc, yc - 6, xc, yc +6, w1
  
  ;for four_slicer only, draw 3 other lines assuming entire plane in view
  xqq = ixs%offset	yqq = iys%offset
  xqq2 = ix%offset	yqq2 = iy%offset
  xdrawline,xqq,yqq,xqq2,yqq2,w1
  xdrawline,xqq+offset,yqq,xqq2+offset,yqq2,w1
  xdrawline,xqq+offset,yqq+offset,xqq2+offset,yqq2+offset,w1
  xdrawline,xqq,yqq+offset,xqq2,yqq2+offset,w1
  

  ;now convert ends to image coordinates, this is similar to convertfordata
  ;but specialized for this job, some parameters that don't change were
  ;setup before the loop, this is safe because we have no xtloop here

  x1 = fix(ix/zm) + xoff
  x2 = fix(ixs/zm) + xoff
  y1 = fix(iy/zm) + yoff
  y2 = fix(iys/zm) + yoff
  if reverse_x then  { x1 = nxd-1-x1	x2 = nxd-1-x2 }
  if reverse_y then  { y1 = nyd-1-y1	y2 = nyd-1-y2 }
  if transpose_flag then { switch, x1, y1	switch, x2, y2 }

  ;now, what about the time factor? We have to apply the tracking.


  dx = x1 - x2		dy = y1 - y2
  len=sqrt( (dx)^2 +(dy)^2 )
  if len gt 0 then {
   np=fix(len)+1
   xs=indgen(fltarr(np))*dx/len + x2
   ys=indgen(fltarr(np))*dy/len + y2
   ;for four_slicer, we need 4 sets
   xqq = xs%offset	yqq = ys%offset
   xs = [xqq, xqq+offset,xqq, xqq+offset]
   ys = [yqq, yqq, yqq+offset, yqq+offset]
   redim, xs, 4*np, 1
   redim, ys, 4*np, 1
   len = 4*np
    if $mv_track_flag then {
    xs = xs + t_dx
    ys = ys + t_dy
    ;need to check for out of range stuff
    xbad = [sieve(xs lt 0),sieve(xs ge nxd),sieve(ys lt 0),sieve(ys ge nyd)]
    in=rfix(xs) + nx*rfix(ys)
    in(xbad) = 0
   } else {
    in=rfix(xs) + nx*rfix(ys)
   }
   
   indx = in + toffsets
   slice=reverse(cube(indx),1)	;note reversal in time to make t increase up
   tv,ft(slice),0,0,w2
   ;a partial erase is done if we are shrinking, a full erase causes
   ;flickering
   if len lt last_len then {
   erase,w2,len		;erase everything to right of slice
   }
   last_len = len
   }
  }
 if !kb eq 0 then break
 if !kb eq 16 then break
 ix=!ix<nxm	iy=!iy<nym
 }
 ;save path
 ;the 3 items here are # of nodes, width, scale (in arc sec/pixel)
 ;$slice_info(0,$nslice)=2
 ;$slice_info(1,$nslice)=1
 ;$slice_info(2,$nslice)=$scale
 ;$slice_info(3,$nslice) = $j1
 ;$slice_info(4,$nslice) = $j2
 ;$xnodes(0,$nslice)=[ixs, ix]
 ;$ynodes(0,$nslice)=[iys, iy]
 ;$nslice=$nslice+1
 ;save the unscaled slice
 $slice4 = slice
 redim, $slice4,len/4, 4, nt
 $slice4 = $slice4(>0,>2,>1)
 ;and show this with each slice independently scaled
 for i=0,3 do tv,ft($slice4(*,*,i)),i*len/4,0, w2
 xflush
 endsubr
 ;=============================================================================
