func trace_expose(xx)
 ;figure out the exposure time from a dp header for an image, assumes the
 ;header is an I*2 array
 ;note that the open/close times for darks are not valid but the coarse
 ;exposure time is. If a dark has an exposure of less than 20ms, it
 ;was probably near zero because the dark exposures are taken to be
 ;total time for multiple sweeps, hence only a single sweep would be less
 ;than about 50 ms. Roger Rehse knows details. The idea is that the dark
 ;"exposure" covers the same span of total time as the real exposure rather
 ;than the time exposed to the sun. Anyhow, I'm not sure if the dark
 ;"exposure" is correct for the 0 case. Might be better to just get the
 ;intended exposure from the frame header.
 ;nominal exposure time would be
 ;;; et = xx(265) * 0.004
 ;;; sweeps = 1 + extract_bits(xx(268),0,8))	;# of sweeps
 ;modified 8/30/98 for little endian
 tmp1 = xx(265)
 tmp2 = xx(209)
 tmp3 = xx(210)
 tmp4 = xx(268)
 if $little_endian then {
 dark = extract_bits(xx(296),7,1)
 swab,tmp1  swab, tmp2  swab, tmp3  swab, tmp4
 } else {
 dark = extract_bits(xx(296),15,1)
 }
 ;;ty,'dark =', dark
 jq = (long(tmp1) and 0xffff)
 ;;ty,'jq =', jq
 ;darks can't use the open/close and have no sweeps
 if dark then t = jq * 0.004 else {
 ;the open/close times, lsb is 1/256 ms
 dt = 0.00000390625 * (tmp2 - tmp3)	;might be negative
 ;use the coarse exposure to resolve wrap arounds
 nq = (jq - 250.*dt)/64.
 n = fix(nq+.5)		;get wrap count
 t = .256*n + dt	;each wrap is 256 ms, add dt for total
 ;if we get zero, it was a short exposure and we need sweep count
 if t eq 0.0 then t = 0.0016 * (1 + extract_bits(tmp4,0,8))
 }
 return, t
 endfunc
 ;=======================================================================
