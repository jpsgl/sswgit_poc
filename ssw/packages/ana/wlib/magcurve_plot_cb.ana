subr magcurve_plot_cb, overplot
 ;plot or oplot one or more of the 4 light curves
 ;poll the checkbox to see what is set and put in flags
 
 p1_flag = xmtogglegetstate($magcurve_choice(1))
 p2_flag = xmtogglegetstate($magcurve_choice(2))
 p3_flag = xmtogglegetstate($magcurve_choice(3))
 p4_flag = xmtogglegetstate($magcurve_choice(4))
 i = p1_flag or p2_flag or p3_flag or p4_flag
 if not(i) then {
 	errormess,'no magnetic flux\ncurve types selected'
 	return }

 if check_magflux() ne 1 then return
 if overplot ne 2 then {
   if overplot eq 0 then ic = fix(xmtextfieldgetstring($magcurvetext6)
   if overplot eq 1 then ic = fix(xmtextfieldgetstring($magcurvetext9)
   if ic lt 0 or ic ge dimen($magnetfluxcurves,1) then {
     errormess,'flux curve # '+string(ic)+'\ndoes not exist'
     return  }
 }
 ;check the scaling situation if not an overplot
 if overplot ne 1 then {
 !fz = 0	;probably want an option for this
 ;if auto scale, then use min/max of $magnetfluxcurves to set scales
 if xmtogglegetstate($magcurve_auto(1)) eq 1 then {
  ;depending on who we plot, figure the ymin and ymax
    ;overplot eq 2 means all the curves
    ymax = -1.E30  ymin = 1.E30
    if p1_flag then {
       if overplot eq 2 then
         ymax = ymax > max($magnetfluxcurves) else
           ymax = ymax > max($magnetfluxcurves(*,ic))
       ymin = ymin < !lastmin }
    if p2_flag then {
       if overplot eq 2 then
         ymax = ymax > max($magtotalfluxcurves) else
           ymax = ymax > max($magtotalfluxcurves(*,ic))
       ymin = ymin < !lastmin }
    if p3_flag then {
       if overplot eq 2 then
         ymax = ymax > max($magplusfluxcurves) else
           ymax = ymax > max($magplusfluxcurves(*,ic))
       ymin = ymin < !lastmin }
    if p4_flag then {
       if overplot eq 2 then
         ymax = ymax > max($magnegativefluxcurves) else
           ymax = ymax > max($magnegativefluxcurves(*,ic))
       ymin = ymin < !lastmin }
    } else {
 ymin = float(xmtextfieldgetstring($magcurvetext8))
 ymax = float(xmtextfieldgetstring($magcurvetext7))
 }
 ty,'ymin,ymax = ', ymin,ymax
 limits, 0,0, ymin,ymax
 }

 time_flag = time_axis($magcurve_times_flag, $magcurve_times, t, sx)
 if overplot ne 1 then {
  ;this is always a new plot, so if an eps plot is in progress, we must close it
  if $unfinished_eps_file then finish_eps
  pencolor,'black'
  sy = 'magnetic flux, Maxwells'
  stitle = sprintf('area = %8.1f Mm^2', $magcurve_area(ic))

  if $magcurve_plotmode then {
   magcurve_setup_eps
   } else {
   pdev, 0
   ;get plot window
   setup_plot_window, $magcurve_plot_win
  }

  if overplot eq 2 then ic = 0		;for the setup
  if time_flag then {
   	plot, t, $magnetfluxcurves(*, ic), 0, sx, sy, stitle
   } else {
   plot, $magnetfluxcurves(*, ic), 0, sx, sy, stitle
   }
 } else {
  ;if this is an overplot and we are in eps mode, we must have already started
  ;an eps plot
  if $magcurve_plotmode then {
   if $unfinished_eps_file eq 0 then {
     errormess, 'over plot requires\nan initial eps plot\nplease try again'
     return } }
 }

 ;now the actual plots (above just set up the box and labels)
 
 if overplot eq 2 then {
  nc = dimen($magnetfluxcurves, 1) > 1
  if p1_flag then {
   pencolor,'red'
   for i = 0,nc-1 do
    magplot, $magnetfluxcurves, i, time_flag, t
   } 
  if p2_flag then {
   pencolor,'blue'
   for i = 0,nc-1 do
    magplot, $magtotalfluxcurves, i, time_flag, t
   }
  if p3_flag then {
   pencolor,'green'
   for i = 0,nc-1 do
    magplot, $magplusfluxcurves, i, time_flag, t
   }
  if p4_flag then {
   pencolor,'black'
   for i = 0,nc-1 do
    magplot, $magnegativefluxcurves, i, time_flag, t
   }

 } else {
  if p1_flag then { pencolor,'red'  magplot, $magnetfluxcurves, ic, time_flag, t }
  if p2_flag then { pencolor,'blue'  magplot, $magtotalfluxcurves, ic, time_flag, t }
  if p3_flag then { pencolor,'green'  magplot, $magplusfluxcurves, ic, time_flag, t }
  if p4_flag then { pencolor,'black'  magplot, $magnegativefluxcurves, ic, time_flag, t }
 }
 pencolor,'black'
 restore_plot_stuff
 endsubr
 ;========================================================
