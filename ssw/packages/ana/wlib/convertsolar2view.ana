subr convertsolar2view, x1, y1, tai, w1
 ;convert a position on the sun in arc sec from disk center to screen offset
 ;in a view, t is needed only for timeslices, for movies we use only the
 ;position on the currently displayed frame (which is generally at a different
 ;time)
 xwindow, w1
 ;determine if a time slice view or a movie or just an image
 timeslice = 0
 movie = 0
 if w1 ge 20 then timeslice = 1 else if w1 ge #play_window then movie = 1
 viewonly = (timeslice + movie) eq 0
 nx = $view_nx(w1)
 ny = $view_ny(w1)
 iord = $view_order(w1)
 ;nx and ny and iord will be superseded for the movie case, dx and dy
 ;are not used (here) for view only case
 ;check if a transpose
 transpose = iord ge 4
 dx = $view_ix(w1)
 dy = $view_iy(w1)

 if timeslice then {
  ;here we have to account for the orientation of the viewed slice and also the orientation
  ;of the data used for the slice
  ;this will vary depending on the type of slice, assuming line slice now
  xcorner = $ts_sunx1	;note, these are arrays (nt)
  ycorner = $ts_suny1
  ;the spatial direction in the timeslice has both x and y components on the
  ;sun, pre-compute these for any index found below
  if transpose then { nt = nx  ns = ny } else { ns = nx  nt = ny }
  len = float(ns-1)
  if nt ne dimen($ts_times,0) then {
   errormess,'internal error\ninconsistent nt for time slice'
   x1 = 0   y1 = 0
   return
  }
  ;$ts_sundx and $ts_sundy are the solar x and y extents in arc sec
  dxss = $ts_sundx/len
  dyss = $ts_sundy/len
  ss = sqrt(dxss*dxss + dyss*dyss)
  ;the times are already in $ts_times
  }

 if viewonly then {
  ss = $view_sun_pixel_scale(w1)
  xcen = $view_sun_xc(w1)
  ycen = $view_sun_yc(w1)
 }

 if movie then {
  ;note that the time of the displayed frame is used to get the offset
  ;the passed value t is not used except for timeslices
  j = $now+$j1
  zm = float($view_zoom_fac(w1))
  ;for movies, the $view_order should always be 2
  if zm lt 0 then  zm = 1.0/abs(zm)
  iord = $mv_order
  if w1 eq #play_window then {
    vname = '$data'+string(w1)
    it = $play_index(j)
    $mouse_tai = $movie_times(it)
    dx = dx + $mv_track_dx(it)
    dy = dy + $mv_track_dy(it)
    ss = $movie_pixsc(it)
    xcen = $movie_xc(it)
    ycen = $movie_yc(it)
  } else {
    ;one of the multi-movies, use the multi arrays based on j
    i = w1 - $play_window(1)
    vname = '$movie'+string(i+1)
    $mouse_tai = $multi_times_image(j, i)
    it = $play_multi(j, i)
    dx = dx + $multi_track_dx(j, i)
    dy = dy + $multi_track_dy(j, i)
    xcen = $multi_xc(j, i)
    ycen = $multi_yc(j, i)
    ss = $multi_ref_scale
  }
  get_nx_ny, eval(vname), it, nx, ny	;note nx, ny returned
 }

 if timeslice eq 0 then {
  ;get the solar coordinates of the corner
  xcorner = xcen - (.5*nx -.5)*ss
  ycorner = ycen - (.5*ny -.5)*ss
  x1 = (x1 - xcorner)/ss
  y1 = (y1 - ycorner)/ss
  if viewonly then {
   convertforview, x1, y1, w1
   } else {
   if movie then {
     if transpose then { switch, x1,y1 }
     ;reverse direction of x and y as needed
     if iord%4 lt 2   then { y1 = ny-1-y1 }
     if iord%2 eq 1 then { x1 = nx-1-x1 }
     x1 = rfix((x1 - dx) * zm)
     y1 = rfix((y1 - dy) * zm)
   }
   }
 } else { 
  ;more work here, it might not be on the line of course nor in the time range
  jt = minloc(abs($ts_times - tai))
  jt = jt(0)
  ;check if at an end
  if jt le 0 or jt ge (nt - 1) then {
    if !lastmin gt mean(abs(differ($ts_times))) then
    	errormess,'warning: time out of range'
  }
  ix = (x1 - xcorner(jt))/dxss
  iy = (y1 - ycorner(jt))/dyss
  ;if both give the same (basic) answer, then on line (or close)
  offline = 0
  if abs(ix-iy) ge 2 then offline = 1
  if ix lt 0 or iy lt 0 or ix gt len or iy gt len then offline = 1
  if offline then errormess,'warning: position off the line'
  ix = (ix < len) > 0
  iy = (iy < len) > 0
  x1 = rfix( 0.5*(ix + iy))
  y1 = jt	;time goes to y1
  ;we now have the indices in the data in x1, y1
  ;where x1 is along space line and y1 is time
  ;reverse directions as needed, note use of ns and nt
  if iord%4 lt 2   then { y1 = nt-1-y1 }
  if iord%2 eq 1 then { x1 = ns-1-x1 }
  if transpose then { exchange, x1, y1 }
  ;zooms are handled differently for time slices, zm is always 1
  x1 = x1 - dx
  ;the time gets put into y1
  y1 = y1 - dy
 }
 ;it might be outside the window, clamp to edge
 x1=(x1<!ixhigh)>0   y1=(y1<!iyhigh)>0
 return
 endsubr
 ;=============================================================================
