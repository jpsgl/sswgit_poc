func readtracelistimage(zim,i,x1,x2,y1,y2,map_params,subareatype)
 ;5/14/2002 - the scaletype parameter was actually dropped in last year's mods
 ;but not from the argument list (see below), now replace it with a "subareatype"
 ;to allow subareas based on solar coordinates, doing some of the calculations here
 ;func readtracelistimage(zim,i,x1,x2,y1,y2,map_params,scaletype)
 ;3/5/2001 - change the htop, hbot scaling to map_params to include a new mode
 ;10/13/2000 (yes, a Friday) - change to always put out a standard solar
 ;orientation, this simplifies logic for image differences, merges, etc in
 ;movies. Means ord arg no longer used.
 ;;func readtracelistimage(zim,i,ord,x1,x2,y1,y2,hbot,htop,scaletype)
 ;used to load an image from the list id in $mvget_trace_list_id
 ;8/17/2000 - change the way cutouts are done, we now want (x1:x2,y1:y2)
 ;to be in "CCD" coordinates but using the solar orientation, this means
 ;some extra effort to translate the user input but then we can apply it
 ;to any size image and return the overlap between the image read and the
 ;area desired
 ;4/19/2000 - for exposure compensation, using a different flag that
 ;gets set by the caller now rather than $mvget_aec_flag and $mvget_aec_type
 ;the new flags are $trace_aec_flag and $trace_aec_type
 ;4/9/99 - added globals $xc_offset and $yc_offset to return the offsets
 ;from center when doing a cutout, couldn't think of a better way
 ;5/20/98 - also for light curve widget, this makes the program operate
 ;in 2 modes, the light curve mode just has 2 args

 narg = !narg
 ;which file are we from?
 nf = min(sieve($mvget_selected_tops ge i))
 ;ty,'nf =', nf
 ;check if the current file, if not we have to open file, etc
 fname = $mvget_selected_image_files(nf)
 if  $t3d_file_name ne fname then {
   ty,'new file needed, fname:', fname,' $t3d_file_name:', $t3d_file_name
   $t3d_file_name = fname
   if t3d_open_file() ne 1 then errormess,'problem opening file'
 }
 j = $mvget_selected_image_nums(i)
 x = get_trace_image(j)
 if isarray(x) eq 0 then {
   sq = sprintf('bad TRACE image\ncan''t decode\nimage %d in\n%s',j, $t3d_file_name)
   errormess,sq  return, -1 }
 ;5/20/98 - 2 modes now, if narg is 2, the simple one
 ;despiking done here, see comment in mvget_creat_cube
 if $despike_movie_flag then x = despike_internal(x)
 ;emi reduction, here we have the raw image whereas in other place we have a re-order
 zeroifundefined, $emi_movie_flag
 if $emi_movie_flag then x = remove_emi(x, -1)
 xx = $t3d_dp_header(*,j)
 if $little_endian then { amp_case = extract_bits(xx(102),4,1) } else {
 	amp_case = extract_bits(xx(102),12,1) }
 ;some parameters we need for both argument options
 trace_ccdpositions, xx, ix, iy, ix2, iy2, nx_ccd, ny_ccd, bm, xsum
 ;want to pass on the product of summing and binning as a global
 $trace_bsum = bm*xsum
 $trace_et = trace_expose(xx)
 $trace_sum = xsum


 if narg eq 2 then {
   ;we have to do an orientation here for the simple case before returning
   if amp_case then zim = reorder(x, 4) else zim = reorder(x, 7)
   ;also, we need some parameters
   
   return, 1 }
 
 ;note that the simple case used by light_curve_compute and friends does
 ;not do exposure compensation

 nx = dimen(x,0)	ny = dimen(x,1)
 ;do the orientation here since we hide the A/B stuff and align with solar
 ;n/s and e/w
 ;also, the subarea was determined using a re-oriented image so we need to
 ;adjust the cutout
 ;;iord = ord	;it was already xor'ed by 2 in mvget_creat_cube
 ;some checks whilst we debug/test this
 if nx ne nx_ccd or ny ne ny_ccd then {
 	ty,'inconsistent CCD sizes', nx, ny, nx_ccd,ny_ccd }
 ;the $trace_ccdxbase and $trace_ccdybase with the image size can be used
 ;now to crop the cutout
 xq = nx_ccd*$trace_bsum - 1
 yq = ny_ccd*$trace_bsum - 1
 ;5/14/2002 - there are now 2 subarea modes, in the first one, the x1,x2,y1,y2
 ;are the cutout indices. In the second case, they need to be biased by the
 ;size of the extract
 if subareatype then {
  zim = .5*(xq + 1) + $trace_ccdxbase
  y1 = y1 + zim     y2 = y2 + zim
  zim = .5*(yq + 1) + $trace_ccdybase
  x1 = x1 + zim     x2 = x2 + zim
 }
 ;we now have (x1:x2, y1:y2) in absolute CCD coords with solar orientation
 if amp_case then {
  ;amp B case, assuming a re-order 4, change if/when we discover otherwise
  ;;iord = iord xor 4	;since 4 xor 2 is still 4
  zim = 1023 - $trace_ccdxbase - xq
  i1 = y1 - zim
  i2 = y2 - zim
  zqy = 1023 - $trace_ccdybase - yq
  j1 = x1 - zqy
  j2 = x2 - zqy
  } else {
  ;amp A case, assuming a re-order 7, change if/when we discover otherwise
  ;;iord = iord xor 5	;since 7 xor 2 is 5
  zim = 1023 - $trace_ccdxbase
  i1 = zim - y2
  i2 = zim - y1
  zqy = 1023 - $trace_ccdybase
  j1 = zqy - x2
  j2 = zqy - x1
  }
 ;;ord = iord	;returned value
 i1 = i1 > 0	j1 = j1 > 0	i2 = i2 < xq	j2 = j2 < yq
 ;if we got out of range input, the subarea might be outside the image,
 ;catch that here by checking for i1 > i2 or j1 > j2
 if check_subarea(i1,i2,j1,j2) then return, -1
 ;so this is the cutout cropped to the extracted image
 ;for some placement schemes, need to know where this is centered in the
 ;Cart. coordinate space used for (x1:x2,y1:y2), also need the offset wrt
 ;the center used to get the solar position (which is the proper center
 ;of the original CCD cutout)
 ;remember that i1:i2, j1:j2 are in original CCD image, so need to check
 ;amp again
 xq = 0.5*(i1+i2)	;x in CCD
 yq = 0.5*(j1+j2)
 if amp_case then {
  $ccd_yc = zim  + xq
  $ccd_xc = zqy + yq
  } else {
  $ccd_yc = zim  - xq
  $ccd_xc = zqy - yq
  }
 ;for the sun position correction, we need the difference of the above
 ;wrt to the center of image (which may be a cutout from the CCD)
 $xc_offset = $ccd_xc + 0.5*(iy+iy2) -1023
 $yc_offset = $ccd_yc + 0.5*(ix+ix2) -1023
 ;now allow for summing and/or binning
 i1 = i1/$trace_bsum	 i2 = i2/$trace_bsum
 j1 = j1/$trace_bsum	 j2 = j2/$trace_bsum
 ;;ty,'resultant i1,i2,j1,j2 =',i1,i2,j1,j2
 ;;dump, x
 ;keep following check in for testing, delete after we are more certain this works
 if i2 ge dimen(x,0) or j2 ge dimen(x,1) or i1 lt 0 or j1 lt 0 then {
  ty,'bad subarea range'
  beep
  sq = sprintf('bad subarea range\ndimens: %d, %d\nrange: (%d:%d, %d:%d)\ntry again', -
  	dimen(x,0), dimen(x,1), i1, i2, j1, j2)
  errormess, sq
  return, -1  }
 zim = x(i1:i2, j1:j2)
 ;10/13/2000 - always reorder to standard solar coords (N up, W to right)
 if amp_case then zim = reorder(zim, 4) else zim = reorder(zim, 7)
 ;the adjustment of map_params is done for TRACE exposure compensation, it
 ;is only used for some of the parameters
 ;it depends on a ref exposure $trace_base_et. If the limits are both 0, we
 ;default to min/max and don't make an adjustment (which would be meaningless
 ;and cause a divide by zero)
 ;9/14/98 - important, for situations where we are not scaling the data directly,
 ;a different strategy is required and we must scale the entire images

 if $trace_aec_flag then {
 ;two situations, if we are just making a scaled cube, then just mod
 ;hbot and htop, otherwise provide a version of the data with
 ;pedestal removed and scaled to ref exposure and pedestal restored
 ;later we'll have a flatfield option, this is just to be able to make movies
 ;without the levels changing (much)
 if xsum le 1 then ped = map_params(16) else ped = map_params(17)
 et = $trace_et
 if $trace_aec_type then {
     if $trace_base_et lt 0 then $trace_base_et = et
     if $trace_base_sum lt 0 then $trace_base_sum = xsum
     ;adjust
     fac = (et * xsum * xsum)/($trace_base_et * $trace_base_sum * $trace_base_sum)
     ty,'rescaling image, ped, fac =', ped, fac
     ;here we modify zim directly, regardless of any scaling later (which will
     ;not be done for the output file case)
     zim = zim - ped
     zim = zim * (1./fac) +ped
     zim = word(rfix(zim))
 } else {
    ;change all the appropiate parameters
    map_limit_adjust, map_params, et, xsum
    }
 }


 ;the scaling and reorder that was previously done here has been transferred
 ;back to the caller (mvget_create)

 return, 1
 endfunc
 ;===============================================================================
