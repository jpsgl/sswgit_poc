func rdfits_mdi_trace(x, name, h)
 ;9/12/2006 - check for image header extensions
 ;7/26/2004 - start adding checks for Solar B FITS files
 ;4/28/99 tries to detect the strange multi-image files and sets
 ;$multi_image_flag if it seems appropiate, in this case we return x=0
 ;specialized to recognize some cropped MDI data
 ;added the test trace files 10/20/97
 ;3/14/98 (pi day)
 ;because of the TRACE 3-D hourly files, it is now dangerous to read a fits file
 ;without checking first, even the extension can be rather large
 iq = fits_header(name, h, h2)
 if iq ne 1 then return, 0   ;we have to have a header
 
 ;if no extension, we will assume a civilized FITS file
 if isscalar(h2) eq 0 then {
   ;has an extension, look for some suspicious keys
   ;6/10/2010 - for compressed fits files ala cfitsio style, these are done as a binary table
   ;extension, we look for an NAXIS = 0 in h as the first hint
   iq = fits_key(h,'NAXIS')
   if iq le 0 then {
     ;6/21/2010 - this is not completely general yet, we don't look for
     ;a second extension and assume the header we want is in h2, we also don't
     ;handle Solar B or MDI, we assume it must be AIA or HMI or unknown type
     h = h2
     iq = 0
     iq = cfitsio_read(x, name)
     if iq ne 1 then { ty,'error reading compressed file'  x = 0 return, 0 }
     ;try to get times and positions, this also gets BSCALE and BZERO if they exist
     fits_time_decode, h
     return, 1
   }
   ;should run through the keys just once with a more efficient code
   if issolarbsot(x, name, h, 1) then return, 1
   nlines = dimen(h,0)
   for i = 0, nlines - 1 do {
     if strpos(h(i),'TR_WRT_FITS') gt 0 or strpos(h(i),'TR_REFORMAT') gt 0 then {
       $multi_image_flag = 1    x=0   return, 1 }
   }
   ;has an extension but apparently not a known type of multi-image file
   ;the old single image TRACE files have extensions so check first for that
   iq = rdfitsex(x, x2, name, h, h2)
   if iq ne 1 then return, iq
   if isarray(x2) then {
     ;could be, probably not a cropped MDI at least so we'll just return from here
     ;if we don't need to decode a compressed TRACE file, note that trace_decode
     ;will modify the x array if the data was compressed
     return, trace_decode(x, x2, name)
   }
 }

 ;assuming ordinary FITS file (no extension)
 ;check for a Solar B file again to catch ones w/o an extension
 if issolarbsot(x, name, h, 0) then return, 1
 $multi_image_flag = 0
 iq = rdfits(x, name, h)
 ;read in the data as a simple FITS files
 ;5/16/2010 for early AIA level 1 files, need to take out 0x8000000 entries
 if symdtype(x) eq 2 then zeronondata, x
 ;try to get times and positions, this also gets BSCALE and BZERO if they exist
 fits_time_decode, h
 ;check if x is 1-D
 if num_dim(x) eq 1 then {
   ;is 1-D, check if there is a DPC field, likely a cropped MDI image

   if ($data_dpc and 0xffff) eq 0x0fc0 then {
     ;a good case to de-crop
     x = uncrop_mdi(x)
     return, iq
   }
   ;no DPC, assume really just a 1-D array
 }
 ;if a fdmg file, check if we are running with widgets and perform some
 ;ops on the fdmg if certain options set
 if $data_fdmg_flag then {
   ;this has to be safe to run without the browser
   if defined($fdmg_rotate) then {
     if xmtogglegetstate($fdmg_scale_to_gauss(1)) then {
       if $bscale eq 0 then {
	 ;check if bunit already set to gauss (3/5/2010), if so, assume lack of bscale means in gauss
	 if strpos(upcase($bunit),'GAUSS') lt 0 then {
           errormess,'can''t scale to gauss\nNo BSCALE available\nor not a FITS file'
	 }
       } else {
         x = x * $bscale + $bzero
       }
   }
     if xmtogglegetstate($fdmg_rotate(1)) then {
      if $data_p_angle ne 0.0 then {
       ;I think we need to rotate about $data_x0, $data_y0
       ;a nearest neighbor rotate is done to avoid getting a range
       ;of "non-valid data" values from interpolation, this confuses
       ;the scaling code which will drop the lowest single value if it is
       ;less than -10000 but doesn't work if there is a spread
       x = rotate(x,-$data_p_angle, $data_x0,$data_y0)
      }
     }
     ;the mask is done after a possible rotate or else we have zeroes in
     ;the corners
     if xmtogglegetstate($fdmg_mask_limb(1)) then {
      ;we assume dataread has already gotten the parameters
      if $data_r_sun gt 0 then {
       xin = (indgen(fltarr(dimen(x,0))) - $data_x0)^2
       yin = (indgen(fltarr(dimen(x,1))) - $data_y0)^2
       redim, xin, dimen(xin, 0), 1
       redim, yin, 1, dimen(yin, 0)
       r2 = xin + yin
       xin = 0   yin = 0
       mask = sieve( r2 gt ($data_r_sun + 1)^2 )
       x(mask) = min(x)  ;make sure it is black
      }
     }
   }
 } else {
   ;for generic fits images, apply $bscale and $bzero if present, this will
   ;also convert to F*4
   if $bscale ne 0 then  { if $bscale ne 1 or $bzero ne 0 then x = x * $bscale + $bzero }
 }
 return,iq
 endfunc
 ;=======================================================================
