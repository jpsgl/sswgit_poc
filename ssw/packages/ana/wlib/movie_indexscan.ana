subr movie_indexscan
 ;scan the world of multiple movies to make the play index, this is
 ;normally done whenever a change is made or a movie is added to the set
 ;$time_base_type is 0 for "every frame", 1 for "selected movie", 2 for
 ;a fixed cadence, 3 to just use index
 ;presently we assume all the movies have the same pixel scale and it
 ;is constant within each movie. Some parts parts of the logic for a
 ;more general scheme are present. A $multi_ref_scale is defined here
 ;as just the first $movie_pixsc in the first movie, it could be set
 ;elsewhere instead for a future upgrade.
 if no_movie_multi() then return
 n = $num_movies
 ;combine all times and sort into a master array for type 0
 $multi_ref_scale = $movie_pixsc1(0)
 ;have to check if this is valid
 if $multi_ref_scale le 0 then {
   prompt_for_scale, $multi_ref_scale
   ;we need something reasonable so just use 1.0 if user doesn't enter something
   ;valid
   if $multi_ref_scale le 0 then $multi_ref_scale = 1.0
 }
 ref = fix(xmtextfieldgetstring($ref_movie_text))
 if ref le 0 or ref gt n {
   sn = sprintf('movie # for times must be in range 1 to %d\nyou have %d',n,ref)
   errormess,sn
   return
 }
 sn = string(ref)
 if $time_base_type eq 1 then {
   ;for type 1 we just grab the times for the ref movie
   $multi_times = eval('$movie_times'+sn)
 } else if $time_base_type eq 3 then {
   ;for type 3, we find the movie with the most frames for our count
   nt = 1
   for i=1,n do {
     nt = nt > num_elem(eval('$movie_times'+sn))
   }
   ;a fake $multi_times for this mode
   $multi_times = indgen(dblarr(nt))
 } else {
   ;if type 0 or 2, we need min and max and maybe merged set
   t0 = min($movie_times1)
   t1 = !lastmax
   if $time_base_type eq 0 then $multi_times = $movie_times1
   if n gt 1 then {
     for i=2,n do {
       ;get the first and last times for the whole set
       sn = string(i)
       tq = eval('$movie_times'+sn)
       ;only use the ones in the appropiate $play_index (what a pain)
       iq = eval('$play_index'+sn)
       tq = tq(iq)
       if $time_base_type eq 0 then { $multi_times = [$multi_times, tq] }
       t0 = t0 < min(tq)
       t1 = t1 > !lastmax
     }
   }
 }

 ;for type 2, we use the min and max and the cadence to construct a time base
 if $time_base_type eq 2 then {
   xq = float(xmtextfieldgetstring($movie_cadence_text))
   nt = fix((t1 - t0)/xq)
   if nt le 0 then {
     errormess,'problem creating uniform\ntime base'
     return
   }
   $multi_times = indgen(dblarr(nt)) * xq + t0
 }
 ;type 1 and 2 should now have the $multi_times array set up, more work for type 0
 if $time_base_type eq 0 then {
   $multi_times = sort($multi_times)
 } 
 ;$multi_times now has the times we want for our multiple movie
 ;now set up the $play_multi
 
 nt = num_elem($multi_times)
 if nt le 1 then {
   errormess, 'only one time for\nthe multiple movie\nsetup, aborting'
   return
 }
 ;get the 2-D index array for this set of movies and a 2-D array
 ;of corresponding solar positions. Note that these are done for all the play
 ;indices, differently than we did for the single movie case where these
 ;arrays matched the data times.
 $play_multi = intarr(nt, $num_movies)
 $multi_xc = fltarr(nt, $num_movies)
 $multi_yc = fltarr(nt, $num_movies)
 $multi_pixsc = fltarr(nt, $num_movies)
 $multi_nx_all = intarr(nt, $num_movies)
 $multi_ny_all = intarr(nt, $num_movies)
 ;11/16/2000 - but still need all the image times, could compute on the
 ;fly but make an array for now
 $multi_times_image = dblarr(nt, $num_movies)
 
 ;for all types, the following computation of $play_multi should work
 ;though it is unnecessarily complicated for some
 for i=1,n do {
  sn = string(i)
  ;this has to select from only the entries in each $play_index
  ;12/11/2003 - allow just by index, this has an nt that is the count
  ;count for the longest movie
  pq = eval('$play_index'+sn)
  nq = num_elem(pq)
  ;nq is the number of frames for this movie but we need to make entries
  ;for all times (which is different in general)
  ;for type 3 (which doesn't use times) we just want indices but they
  ;still have to pass through $play_index and be clamped
  if $time_base_type eq 3 then {
    iq = indgen(intarr(nt))
  } else {
    tq = eval('$movie_times'+sn)
    tq = tq(pq)
    iq = rfix(polate( tq, indgen(intarr(nq)), $multi_times))
  }
  ;iq now has an entry for each time
  ;need to clamp for some types
  iq = iq > 0
  iq = iq < (nq-1)
  ;pass through $play_index
  iq = pq(iq)
  imo = i-1
  $play_multi(0, imo) = iq
  xq = eval('$movie_times'+sn)
  $multi_times_image(0, imo) = xq(iq)
  xq = eval('$movie_xc'+sn)
  $multi_xc(0, imo) = xq(iq)
  xq = eval('$movie_yc'+sn)
  $multi_yc(0, imo) = xq(iq)
  xq = eval('$movie_pixsc'+sn)
  $multi_pixsc(0, imo) = xq(iq)
  ;unfortunately, we need the size of each image (they are allowed to vary)
  st = '$movie'+sn
  for j = 0, nt -1 do {
  if issymarr(eval(st)) then {
    ;this could be simplified if dimen_symarr is upgraded
    $multi_nx_all(j, imo) = dimen_symarr(eval(st), $play_multi(j,imo), 0)
    $multi_ny_all(j, imo) = dimen_symarr(eval(st), $play_multi(j,imo), 1)
   } else {
    $multi_nx_all(j, imo) = dimen(eval(st), 0)
    $multi_ny_all(j, imo) = dimen(eval(st), 1)
   }
 }
 }
 
 ;we always have to co-align, if tracking is on or off
 multi_track_on_off
 mvplay_reset_limits	;note, depends on $nt being set in multi_track_on_off
 endsubr
 ;========================================================
