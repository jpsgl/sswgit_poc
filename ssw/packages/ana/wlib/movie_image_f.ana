func movie_image(zq,i,track_flag,x1i,x2i,y1i,y2i,nxs,nys,xc,yc,scq,mtime,dx,dx_eff)
  mtime = -1  scq = 0   xc = 0    yc = 0
 ;4/17/2000 - added functioning type 2 and, because types 0 and 2 are
 ;very similar, changed from ncase to if's
 ;1/10/2001 - implementing the solar rotation tracking subareas, note that
 ;all the non-TRACE types are checked for first because of the different
 ;way the subareas are done for raw TRACE files
 ;use a new module to get the full image, used here and for light curves, processing, etc
 ncase $mvget_movie_type
   fname = $fname
   fname = ''
   fname = $mvget_files
   fname = $mvget_files
 endcase

 stat = get_next_image($mvget_movie_type, x, fname, i, $mv_mif_flag, $movieplane2use)
 if stat le 0 then return, stat

 mtime = $data_time_tai
 scq = $data_xscale > 0.0	;note we assume pixels are square
 ;4/29/2009 - if not square (SP maps are an example), do a rescale here and make it so
 if $data_yscale le 0 then $data_yscale = $data_xscale
 if not_close_enough($data_xscale, $data_yscale, .001) then {
   ty,'not square enough!'
   ;for SP we want to use the X scale and upscale to the higher resolution
   if $sbtype eq 2 then {
     ;this will mess up level 0 movies until some other checks are made
     xp = $data_xscale/0.14857
     yp = $data_yscale/0.14857
     ;the level 2 data may not have the right scales so change to 0.1476
     $data_xscale = 0.1476
     $data_yscale = 0.1476
   } else {
     ;no known cases for this situation yet, use x scale
     xp = 1.0    yp = $data_yscale/$data_xscale
   }
   x = mag3(x, xp, yp)
   scq = $data_xscale
   $data_xscale = $data_yscale
 }
 ;don't forget to include any pixel compression, note that if dx and
 ;dy are not equal, we'll mess up; should force dx=dy
 scq = scq * dx
 xc = $data_xcen
 yc = $data_ycen
 
 ;check for bad times and zap $mv_clock_valid and $mv_positions_valid if we
 ;catch any
   
 if $mv_clock_valid then {
   if mtime le 0 and stat eq 1 then {
       errormess,'invalid time\n'+string($data_time_tai)
       $mv_clock_valid = 0
       $mv_positions_valid = 0
    }
 }

 if $mvget_movie_type eq 1 then {
   ;because we don't know yet the size of this TRACE image (and it may
   ;vary over the movie), we set nx, ny to zero now and then apply in
   ;readtracelistimage
   nx = 0   ny = 0
 } else {
   ;? need to check orientation here ?  no ...
   nx = dimen(x,0)
   ny = dimen(x,1)
 }
 
 if track_flag then {
   ;;ty,'in track section'
   ;we have to have valid coords for this option
   if $mv_positions_valid eq 0 then {
     errormess, 'subarea tracking\nfails because of\ninvalid coords.'
     stat = -1  return, stat
   }
   if mtime lt 0 then {
     errormess, 'subarea tracking\nfails because of\ninvalid time.'
     stat = -1  return, stat
   }
   if $mv_coords(0) eq 1 then {
     xy = sunxy($mv_coords(1), $mv_coords(2), mtime)
   } else {
     ;doesn't really work in this case
     xy = $mv_coords(1:2)
   }
   ;find position in image array of the ref point in pixels, the ref point
   ;is the center of the subarea (always a rectangle here) so we have to
   ;compute where on the image the subarea would be to determine the rotation
   ;correction
   ty, 'xc, yc = ', xc, yc,' xy(0), xy(1) = ', xy(0), xy(1)
   xp = (xy(0) - xc)/scq + .5*nx - .5
   yp = (xy(1) - yc)/scq + .5*ny - .5
   ;apply to subarea, clip as needed (what if nothing?)
   ;here nxs and nys are the subarea size (-1) and nx, ny are the full image, note
   ;that the x1,x2,y1,y2 input are not being used
   x1 = rfix(xp - nxs/2)
   x2 = x1 + nxs
   y1 = rfix(yp - nys/2)
   y2 = y1 + nys
   ty, 'xp, yp, nxs, nys =', xp, yp, nxs, nys, '  nx, ny =', nx, ny
   ty, 'x1, x2, y1, y2 =', x1, x2, y1, y2
 }
 ;adjust x and y offset if non-TRACE using final x1,x2,y1,y2
 ;9/6/2010 - this looks wrong, it would undo the track_flag section
 if $mvget_movie_type ne 1 then {
   ;the x1, etc are already adjusted for orientation, they are in native index
   x1 = x1i > 0
   x2 = x2i < (nx -1)
   y1 = y1i > 0
   y2 = y2i < (ny -1)
   ;if out of image, set stat = -1
   if check_subarea(x1,x2,y1,y2) then stat = -1
   x_offset = .5*(x2+x1+1-nx)	;a pixel offset
   y_offset = .5*(y2+y1+1-ny)
   ;note that offsets are added later in a common area
 }
 ;;ty,'adjusted'
 ;;ty, 'x1, x2, y1, y2 =', x1, x2, y1, y2
 if $mvget_movie_type eq 1 then {
   ;trace list, we always get times and positions
   $map_params = $save_map_params
   ;the subarea is done in readtracelistimage because of the flips
   stat = readtracelistimage(x,i,x1i,x2i,y1i,y2i,$map_params,track_flag)
   x1 = x1i  x2 = x2i  y1 = y1i  y2 = y2i
   ;;ty,'TRACE x1,x2,y1,y2 =', x1,x2,y1,y2
   ;for raw TRACE, the dx and dy are for the full CCD coordinates, this means that
   ;images summed or binned 2x2 would normally have dx=dy=2 to display each pixel
   ;hence we change dx and dy for each frame to reflect the actual situation (this will
   ;allow summed and non-summed to be combined) and use dx/(bin*sum)
   ;5/7/2001 - this also leads to situations with dx_eff < 1 which means the
   ;image must be expanded and that dx_eff must be FP
   ;4/26/2002 - assume square pixels so dy_eff is same as dx_eff
   dx_eff = float(dx) / $trace_bsum
   x_offset = $xc_offset/dx
   y_offset = $yc_offset/dx
   ;I think scq is always just $trace_plate_scale and therefore
   ;we should use dx * $trace_plate_scale for movie_pixsc
   scq = scq * dx		;allows for binning (done later), assumes dy = dx
 }

 ;adjust central positions for any cutouts, this depends on scq being valid from
 ;each type above and x_offset and y_offset
 xc = xc + x_offset*scq
 yc = yc + y_offset*scq
 ;;ty,'modified xc, yc = ', xc,yc,',   x_offset,  y_offset = ', x_offset,  y_offset
 ;if we actually read something, get zq, otherwise zq is unchanged
 if stat gt 0 then {
   ;already did subarea for the trace list case, others are done here
   if $mvget_movie_type eq 1 then switch, x, zq else {
       if (x1 ne 0) or (y1 ne 0)  or (x2 ne (nx-1)) or (y2 ne (ny-1)) then { 
   	    ty, 'x1, x2, y1, y2 =', x1, x2, y1, y2
	    ;this may be used for limb enhancement
	    zq = x(x1:x2, y1:y2) } else { switch, x, zq }
   }
 }
 ;;dump, zq
 return, stat
 endfunc
 ;========================================================
