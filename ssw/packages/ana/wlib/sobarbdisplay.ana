subr sobarbdisplay, name
 ;assumes data and header in $sbdata and $sbhead
    ;we have to select the views, start with $current_window and up
    
   ;a major exception is the possibility of combining files into composite images
   ;this is an option for ROI looping and to produce SP maps
   ;first consider the ROI looping, only for FG and we have a check box option for this 

   zeroifundefined, $sbtype, $roiloopflag, $sb_genid
   if $sbtype eq 1 and $roiloopflag then {
     ;check if it might have ROI looping, it must be shutterless of course
     if $sb_genid ge 32 then {
      ;worth the expense of a fits keyword check
      roiloop = fits_key($sbhead,'ROILOOP')
      if isscalar(roiloop) eq 0 then {
         errormess, 'no ROILOOP keyword in FITS file?\ncan't  combine ROI loops'
	 return }
      if roiloop ge 1 then {
        ;OK, we have one, unfortunately we cannot determine nROI so we have to scan the files
	;to even determine how many we are dealing with, first get the right list
	nlist = num_elem(names)
	nroi = fix(roiloop)
	;scan forward until we hit the end or find the highest value
	kk = k + 1
	while (kk lt nlist) {
	  ty,'forward scanning for ROI loop, kk = ', kk
	  if (fits_header(names(kk), h)) then {
	    ;we checked to be sure it was a fits file
	    roiloop2 = fits_key(h,'ROILOOP')
	    if isscalar(roiloop2) eq 0 then {
               errormess, 'no ROILOOP keyword in FITS file?\ncan't  combine ROI loops'
	       return }
	    if roiloop2 gt roiloop then nroi += 1 else break
	    kk += 1
	  }
	}
	k2 = kk - 1
	ty, 'done with forward pass, nroi, last roiloop2, k2 = ', nroi, roiloop2, k2
	;we know how many now
	;now a backward pass
	kk = k -1
	roiloop2 = roiloop
	while (kk ge 0) {
	  ty,'backward scanning for ROI loop, kk =', kk
	  if (fits_header(names(kk), h)) then {
	    ;we checked to be sure it was a fits file
	    roiloop2 = fits_key(h,'ROILOOP')
	    if isscalar(roiloop2) eq 0 then {
               errormess, 'no ROILOOP keyword in FITS file?\ncan''t  combine ROI loops'
	       return }
	    if roiloop2 ge roiloop then break	;too far
	    kk -= 1
	    if roiloop2 eq 1 then break		;the first one
	  }
	}
	k1 = kk + 1
	ty, 'done with backward pass, nroi, last roiloop2, k2 = ', nroi, roiloop2, k1
	if kk lt 0 then {
	  ;check if we are missing some at beginning
	  ibase = roiloop2 - 1
	} else ibase = 0
      ty, 'first and last are (k1,k2) = ', k1, k2, '  total = ', k2-k1+1, ' ibase =', ibase
      ;we now have to read all of these and load $sbdata appropiately, there is an
      ;increase in the x dimension using the already single file $sbdata as a model
      nx = dimen($sbdata, 0)  ny = dimen($sbdata, 1)  nz = dimen($sbdata, 2)
      xq = intarr(nx*nroi, ny, nz)
      nx2 = nx/2
      in1 = indgen(lonarr(nx2, ny, nz),0)
      in2 = indgen(lonarr(nx2, ny, nz),1) * nx*nroi
      in3 = indgen(lonarr(1,1,nz)) * nx*ny*nroi
      in1 = in1 + in2 + in3
      dump, xq
      for kk=k1,k2 do {
	dataread, $sbdata, names(kk)
	;better check
	if nx ne dimen($sbdata, 0) or ny ne dimen($sbdata, 1) or nz ne dimen($sbdata, 2) then {
          errormess, 'bad file in ROI loop set?\ncan''t  combine ROI loops'
	  ty,dimen($sbdata, 0), dimen($sbdata, 1), dimen($sbdata, 2)
	  ty, nx, ny, nz
	  return
	}
	i = kk - k1 + ibase
	in2 = in1 + nx2 * (nroi -1 - i)
	xq(in2) = $sbdata(0:(nx2-1), *, *)
	in2 = in1 + nx2 * (nroi + i)
	xq(in2) = $sbdata(nx2:*, *, *)
      }
      ;note that we have kept the header for the one originally clicked, the extension is
      ;read later (if needed) using the original variable name
      exchange, xq, $sbdata
      }
     }
   }

   if defined($sbroutewidget) then
     $solarbfirstwin = fix(xmtextfieldgetstring($sbroutetext))
   zeroifundefined, $solarbfirstwin

   if $sb_routing_flag then win = $solarbfirstwin else win = $current_window
   $view_header(win) = $sbhead
   $view_filename(win) = name
   nd = num_dim($sbdata)

   ;check for special cases
   if strcontains($sbhead, 'jitter') then {
     jittershow, $sbdata, $sbhead
     return
   }
   if strcontains($sbhead, 'analog') then {
     acshow, $sbdata, $sbhead
     return
   }
   if strcontains($sbhead, 'Scan') then {
     iscanshow, $sbdata, $sbhead
     return
   }

   $view_data_type(win) = 3
   ;loop over any outer dimensions, the only 4D case is the SP IQUV
   ;and this is handled as a special case
   if nd eq 2 then update_globals, win, $sbdata else {
     if nd eq 3 then {
       loop = dimen($sbdata,2) - 1
       ;I think all 3-D arrays are FG and these all have I first (except some
       ;doppler modes we don't use), we want this I to be unsigned like the SP I
       ;so just check the index, if we start using those DG modes, we'll have to
       ;check the gen_id
       for k = 0, loop do {
         y = $sbdata(*,*,k)
	 if (k eq 0) then { y = float(y) + 65536. * (y lt 0) }
	 update_globals, win, y
	 $view_header(win) = $sbhead
	 $view_filename(win) = name
	 win = win + 1
	 }
     } else if nd eq 4 then {
       nside = dimen($sbdata,2)
       nx = dimen($sbdata,0)  ny = dimen($sbdata,1)
       loop = dimen($sbdata,3) - 1
       for k = 0, loop do {
         y = array(1, nside*nx,ny)
	 insert, y, $sbdata(*,*,0,k),0,0
         if (nside eq 2) then insert, y, $sbdata(*,*,1,k), nx, 0
         ;for SP I, we always want unsigned so upgrade here to F*4
	 if (k eq 0) then { y = float(y) + 65536. * (y lt 0) }
	 update_globals, win, y
	 $view_header(win) = $sbhead
	 $view_filename(win) = name
         win = win + 1
	 }
   }
 }
 endsubr
 ;========================================================
