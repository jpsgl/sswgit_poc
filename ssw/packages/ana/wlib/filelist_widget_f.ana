func filelist_widget(fname)
 ;give the name of a file containing a list of files (a file list), creates
 ;an internal list of the names and a widget to access them. Also registers
 ;this list with the list of lists widget. Returns the number of file names
 ;found, 0 if something amiss
 ty,'fname = ', fname
 
 ;use a null file name to specify a re-load of the list widget using the
 ;present $file_names, this is done to change how the list is presented; i.e.,
 ;with or without path, times, etc
 
 if num_elem(fname) gt 0 then {
 ;this is not very efficient but OK for modest lists
 nsource = linecountsanscomments(fname)
 ty,'nsource = ', nsource
 if nsource le 0 then {
   errormess,sprintf('list file\n%s not\nreadable', fname) return }
 lun = get_lun()
 if lun lt 0 then { errormess,'no available LUN for file read!' return }
 openr, lun, fname
 $file_names = strarr(nsource)
 s = ''
 nreadable = 0
 ;being optimistic, setup arrays for times and positions
 $file_times = dblarr(nsource)
 $file_xc = fltarr(nsource)
 $file_yc = fltarr(nsource)
 $file_pixsc = fltarr(nsource)
 zero, $file_times, $file_xc, $file_yc, $file_pixsc

 ty,'scanning files'
 ;now pop the busy widget
 popbusy, 1
 wait, .2
 xtloop, 2
 for i=0, nsource-1 do {
  xmsetlabel, $scan_busy_label, sprintf('working on %d/%d', i, nsource)
  xtloop, 0
  if $panic_stop then {
    errormess, sprintf('panic stop, count = %d\nout of %d',i,nsource)
    close, lun	break }
  if readf(lun,s) ne 1 then {
    errormess,sprintf('problem while reading\nlist file %s', fname)
    close, lun  return }
  ;pass over a few common problems
  s = strtrim(s)
  if s eq './' or s eq '../' then s = ''
  if num_elem(s) gt 0 then {
  $file_names(i) = s
  ;and try to get times and such
  iq = data_type(s, header, params)
  if iq ge 0 then nreadable +=1
  ;only try for times, etc if a FITS file, could extend to others?
  if iq eq 1 then {
    fits_time_decode, header
    $file_times(i) = $data_time_tai
    $file_xc(i) = $data_xcen
    $file_yc(i) = $data_ycen
    $file_pixsc(i) = $data_xscale
  }
  }
 }
 ty,'done scanning files'
 !motif = 1
 if defined($scan_busy_widget) eq 1 then xtunmanage, $scan_busy_widget

 close, lun
 if nreadable eq 0 then {
   errormess, sprintf('Unfortunately, none of the files\nin your list (%s)\nare readable!', fname)
   return, 0
  }
 if nreadable ne nsource then {
   errormess, sprintf('%d files in your list of %d\n were not read',nsource-nreadable,nsource)
   }
 ;if some are unreadable, should we drop them from list? needs work
 ;now put in our list of lists, the tag is the file name of the list file
 imagelist_add_item, 2, fname
 }
 ;now create the list widget if it doesn't exist, otherwise we overwrite the
 ;previous -- so only one at a time

 if defined($filelistwidget) ne 1 then {
  compile_file, getenv('ANA_WLIB') + '/filelistwidgettool.ana'
  }
 ;some of the operations below can change the size, so save it here and then
 ;re-apply it
 xmgetwidgetsize, $filelistshell, dx, dy
 ;also the position
 xmgetwidgetposition, $filelistshell, xp, yp

 ;delete the old stuff (if any)
 ;loop over all files and put in list
 t1 = !systime
 ;pop down so that filling list takes less time
 xtpopdown, $filelistwidget
 xmlistdeleteall, $filelistwidget
 nsource = num_elem($file_names)
 ;also make a string array of all the items
 $filelist_items = strarr(nsource)
 ;;ty,'mark 1'
 ;;ty,'nsource =', nsource
 for i=0, nsource-1 do {
  sq = $file_names(i)
  if $filelist_remove_path then sq = removepath(sq)
  if $filelist_show_times then {
    if $file_times(i) gt 0 then st = date_from_tai($file_times(i),0,1) else
    	st = 'no time available'
    st = strreplace(st,'\n','  ')
    st = strtok(st,'U')
    sq = sprintf('%s   %s @(%3.1f, %3.1f) %4.2f"/pixel',sq, -
    	st, $file_xc(i), $file_yc(i),$file_pixsc(i))
    }
  xmlistadditem, $filelistwidget, sq
  $filelist_items(i) = sq
 }
 t2 = !systime
 ty,'time to load list =', t2 - t1
 xmposition, $filelistshell, xp, yp, dx, dy
 xtpopup, $filelistwidget
 
 iq = nsource
 return, iq
 endfunc
 ;===============================================================================
