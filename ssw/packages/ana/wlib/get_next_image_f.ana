func get_next_image(movie_type, x, fname, i, mif_flag, plane2use)
 ;added mif_flag as an option, used if movie_type is 3
 ;for movie_type of 2, fname must be a strarr containing file names
 if !narg > 4 then mif = mif_flag else mif = 0
 ;use code taken from the movie image code to build a common module here
 ;standardize time and location to $data_xx form
 
 ;;ty,'movie_type = ', movie_type
 ;;d, fname
 zeroifundefined, $roiloopflag, $sbtype, $sb_genid
 if movie_type eq 0 or movie_type eq 2 then {
   ;only difference is how we construct name of file
   ;but we also might be combining files now for Solar B based on $roiloopflag
   if movie_type eq 0 then
   	name_in = fns(fname, i) else name_in = fname(i)
   stat = dataread(x, name_in, h)
   $extra_date_line = ''
   if $instrument_type eq 6 then $extra_date_line = sprintf('AIA %4.0fA ', $instrument_wave)
   if stat le 0 then return, -1
   ;check if we have a roi looping situation, this will currently work only for level 0
   ;file sets, added $instrument_type check 1/31/2008
   if $instrument_type eq 3 and movie_type eq 2 and $sbtype eq 1 and $roiloopflag and $sb_genid ge 32 then {
     ;;ty,'not here!'
     ;worth the expense of a fits keyword check
     roiloop = fits_key(h,'ROILOOP')
      if isscalar(roiloop) eq 0 then {
         errormess, 'no ROILOOP keyword in FITS file?\ncan't  combine ROI loops'
	 return }
      if roiloop ge 1 then {
        ;OK, we have one, unfortunately we cannot determine nROI so we have to scan the files
	;to even determine how many we are dealing with, first get the right list
	nlist = num_elem(fname)
	nroi = fix(roiloop)
	;scan forward until we hit the end or find the highest value
	k = i
	kk = k + 1
	while (kk lt nlist) {
	  ;;ty,'forward scanning for ROI loop, kk = ', kk
	  if (fits_header(fname(kk), h)) then {
	    ;we checked to be sure it was a fits file
	    roiloop2 = fits_key(h,'ROILOOP')
	    if isscalar(roiloop2) eq 0 then {
               errormess, 'no ROILOOP keyword in FITS file?\ncan't  combine ROI loops'
	       return, 0 }
	    if roiloop2 gt roiloop then nroi += 1 else break
	    kk += 1
	  }
	}
	k2 = kk - 1
	ty, 'done with forward pass, nroi, last roiloop2, k2 = ', nroi, roiloop2, k2
	;we know how many now
	;here we shouldn't need a backward scan because the data should bef
	;time ordered
	k1 = i
	;;ty, 'done with backward pass, nroi, last roiloop2, k2 = ', nroi, roiloop2, k1
	;check if we are missing some at beginning
	ibase = roiloop - 1	;where roiloop is the first one
      ty, 'first and last are (k1,k2) = ', k1, k2, '  total = ', k2-k1+1, ' ibase =', ibase
      ;we now have to read all of these and load $sbdata appropiately, there is an
      ;increase in the x dimension using the already single file x as a model
      nx = dimen(x, 0)  ny = dimen(x, 1)  nz = dimen(x, 2)
      xq = intarr(nx*nroi, ny, nz)
      nx2 = nx/2
      in1 = indgen(lonarr(nx2, ny, nz),0)
      in2 = indgen(lonarr(nx2, ny, nz),1) * nx*nroi
      in3 = indgen(lonarr(1,1,nz)) * nx*ny*nroi
      in1 = in1 + in2 + in3
      dump, xq
      for kk=k1,k2 do {
	dataread, x, fname(kk)
	;better check
	if nx ne dimen(x, 0) or ny ne dimen(x, 1) or nz ne dimen(x, 2) then {
          errormess, 'bad file in ROI loop set?\ncan''t  combine ROI loops'
	  ty,dimen(x, 0), dimen(x, 1), dimen(x, 2)
	  ty, nx, ny, nz
	  return
	}
	j = kk - k1 + ibase
	in2 = in1 + nx2 * (nroi -1 - j)
	xq(in2) = x(0:(nx2-1), *, *)
	in2 = in1 + nx2 * (nroi + j)
	xq(in2) = x(nx2:*, *, *)
      }
      exchange, xq, x
      ;we also have to modify i, this affects the caller
      i = i + k2 - k1	;note we return the last i used, not the next one
      }
   }
   
   ;for Solar B we get 3-D files often, use a global to select one of the sets
   if num_dim(x) eq 3 then {
     ;;ty, 'plane2use = ', plane2use
     nq = dimen(x,2)
     if plane2use lt nq then {
         x = x(*,*,plane2use) } else { x = x(*,*,nq-1) }
     ;we use the first one only if plane2use = 0, this will be an I
     ;signal (except for some doppler modes) so
     if (plane2use eq 0) then { x = float(x) + 65536. * (x lt 0) }
   }
   ;for Solar B SP, we may get 4-D files
   if num_dim(x) eq 4 then {
     ;assuming SP, we select plane and combine 2 sides, the plane is the
     ;last dimension
     nq = dimen(x,3)
     if plane2use lt nq then {
         x = x(*,*,*,plane2use) } else { x = x(*,*,*,nq-1) }
     ;we use the first one only if plane2use = 0, this will be an I
     ;signal for SP so
     if (plane2use eq 0) then { x = float(x) + 65536. * (x lt 0) }
     nx = dimen(x,0)  ny = dimen(x,1)
     ;also combine the 2 sides and reduce to a 2-D array
     if num_dim(x) eq 3 then {
       nside = dimen(x,2)  
       y = array(1, nside*nx, ny)
       insert, y, x(*,*,0),0,0
       if (nside eq 2) then insert, y, x(*,*,1), nx, 0
       exchange, x, y
     }
     ;if we had just one side, x should already be 2-D
   }

 }

 ;an issue for movie_type 2 is times and positions from imagelist, if they exist, we
 ;use these rather than any from the file header, the $mv_have_time and $mv_have_xy
 ;flags (from imagelist) tell us
 if movie_type eq 2 then {
   if $mv_have_time then $data_time_tai = $mvget_times(i)
   if $mv_have_xy then {
     ;but if the value is zero, then use any $data_xscale we got
     ;this happens for sbcatselect because it makes a list from a catalog file
     ;that lacks the scale
     if ($mvget_pixsc(i) gt 0.0) then $data_xscale = $mvget_pixsc(i)
     $data_xcen = $mvget_xc(i)
     $data_ycen = $mvget_yc(i)
   }
 }

 if movie_type eq 3 then {
   if mif_flag then {
      x = get_mif_image(i)
      if isscalar(x) then stat = 0 else stat = 1
      ;all the times have to be good or we mark them as invalid
      ;5/6/99 - should be modified to use $data_tai, etc
      if $mif_time_tai(i) gt 0 then {
	  $data_time_tai = $mif_time_tai(i)
	  ;we set $mv_positions_valid earlier in setup for this type
	  $data_xscale = $mif_xscale(i)	;note we assume pixels are square
	  $data_xcen = $mif_xcen(i)
	  $data_ycen = $mif_ycen(i)
      }
   } else  {
      stat = imageread(x, fname, i)
      ;these have no time/location?
   }
 }
 
 if movie_type eq 1 then {
    stat = readtracelistimage(x, i)
    $data_time_tai = $mvget_selected_image_times(i)
    $data_xscale = $mvget_selected_image_pixsc(i)
    $data_xcen = $mvget_selected_image_xc(i)
    $data_ycen = $mvget_selected_image_yc(i)
 } else {
   ;note that despiking for TRACE is done in readtracelistimage, check here
   ;for other types
   if $despike_movie_flag then x = despike_internal(x)
 }
 
 return, stat
 endfunc
 ;========================================================
