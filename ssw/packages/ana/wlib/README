This directory contains movie listings for the browser tool. The top listing
is called known_movies and it can contain movie definitions or pointers to
other files. The format is described below. Please note that the motif list
software seems to have a problem reading tabs so please do not use tabs
when constructing these files.

movie definition:

A line defining a file template and the range of file numbers, e.g.,

/hosts/diapason/fast4/lp94/tfw6328.14jun94.p1.n####.subv  1   297

The file template is the file name (with full path) with # signs replacing
the portion of the file that contains a number. The number of # signs
define the minimum width of this field. This numeric part of the
name can have a fixed or variable width. In the former case, the number of
digits in each file name must be the same and equal to the number of # signs in
this template. An example is /pore2data2/lp93/im04jun93n####.g.cor. These files
might be numbered from 0001 to 1702. Note the fill with leading zeroes.
Variable width fields have no leading zeroes and are represented with a
single # sign in the name. When the numeric value exceeds 10, it will be
expanded as needed with no leading blanks or zeroes. Only files conforming to
these conventions can be processed.

The range of file numbers must follow the template separated by blanks (NOT
tabs!, see note above). Only one definition per line and only one line per
definition.

pointer:

A pointer to another file must start with a "->". The file pointed to must be
preceded by a "@" and then continue to the end of the line. Any intermediate
material is understood to be a comment. An example is:

-> 1992 draining loops     @/hosts/umbra/data0/movies/loops92.movies

----------------------------------------------------------
The browser will list these files in a list widget. When a movie definition
is clicked, the browser will load the name template and range. When a pointer
is clicked, the new movie file is read and displayed in the list widget,
displacing the previous list. The new file may also contain pointers. No
record of the previous list is maintained in the browser but the top level
can always be read back in by clicking the original movie list button.
 