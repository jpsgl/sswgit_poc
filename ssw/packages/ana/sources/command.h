/* because this is used by many files, do not put any declarations
   in here */
/* 7/18/2002 - r. shine - changed the obs table structures */
/* structures and definitions for macro commands and packets */
 /* 9/23/2002 - turn off smart memory reads for early tests of rad6k */
 /* 11/7/2002 -SP smart memory is on but FG still off, also see
 notes in FPPcam.cpp concerning the PMU ISR for the FG */
//#define	NO_FG_SMART_MEMORY_READ 	1
//#define	NO_SP_SMART_MEMORY_READ 	1
#define ALLOW_PRINTF
/* for PMU simulator, define if no real PMU */
//#define SIMULATE_PMU
/* activate the defines below when we have a real SP or FG camera */
#define REAL_SP_CAMERA
#define REAL_FG_CAMERA
#define FPP_FLT_MACRO	0xcc
#define FPP_SPP_MACRO	0xce
#define FPP_FLT_STOP	0xd0
#define FPP_SPP_STOP	0xd2
#define FPP_SPP_PAUSE	0xd4
#define FPP_SPP_RESUME	0xd6
#define MACRO_COMMAND_SIZE 41
#define PMUrate			58
//#define PMUrate			232
//#define PMUrate			58

#if SOLARIS
 typedef unsigned int UINT32;
 typedef unsigned short UINT16;
 typedef short INT16;
 typedef int	SEM_ID;
#endif
#if __FreeBSD__
 typedef unsigned int UINT32;
 typedef int	SEM_ID;
#endif
#if __sgi
 typedef unsigned int UINT32;
 typedef unsigned short UINT16;
 typedef short INT16;
 typedef int	SEM_ID;
#endif
#if __APPLE__
 typedef unsigned int UINT32;
 typedef unsigned short UINT16;
 typedef short INT16;
 typedef int	SEM_ID;
#endif
#if linux
 typedef unsigned int UINT32;
 typedef unsigned short UINT16;
 typedef short INT16;
 typedef int	SEM_ID;
#endif

 /* the compression_parameters structure is only 2 bytes so it gets expanded
 to 4 bytes by the compiler. Hence, usage within other structures causes
 a gap. So beware!  */
typedef struct {
#if linux | LITTLEENDIAN
	unsigned quantization : 3;
	unsigned huffman_dc : 2;
	unsigned huffman_ac : 2;
	unsigned reserved_2 : 1;
	unsigned image_compression_mode : 3;
	unsigned bit_compression_mode : 4;
	unsigned reserved_1 : 1;
#else
	unsigned reserved_1 : 1;
	unsigned bit_compression_mode : 4;
	unsigned image_compression_mode : 3;
	unsigned reserved_2 : 1;
	unsigned huffman_ac : 2;
	unsigned huffman_dc : 2;
	unsigned quantization : 3;
#endif
} compression_parameters;

 /* flt_op_parameters is 8 bytes long */
typedef struct {
#if linux | LITTLEENDIAN
	unsigned char  frame_def_id;
	unsigned char  obs_id;
	/* next 16 bit word */
	unsigned wave_off : 11;
	unsigned wave : 5;
	/* next 16 bit word */
	unsigned dark_flag : 1;
	unsigned exp : 15;		/* exposure time or # of accumulations */
					/* exposure 0 is a special case that uses
					defaultExposure (which can be loaded as
					parameter 22) */
	/* next 16 bit word */
	unsigned wscan_step : 11;	/* mA units */
	unsigned wscan_npos : 5;	/* add 1 before use, 0 means a single */
	/*unsigned char  spare; */
#else
	unsigned char  obs_id;
	unsigned char  frame_def_id;
	/* next 16 bit word */
	unsigned wave : 5;
	unsigned wave_off : 11;
	/* next 16 bit word */
	unsigned exp : 15;		/* exposure time or # of accumulations */
					/* exposure 0 is a special case that uses
					defaultExposure (which can be loaded as
					parameter 22) */
	unsigned dark_flag : 1;
	/* next 16 bit word */
	unsigned wscan_npos : 5;	/* add 1 before use, 0 means a single */
	unsigned wscan_step : 11;	/* mA units */
	/*unsigned char  spare; */
#endif
} flt_op_parameters;

 /* spp_op_parameters is 8 bytes long */
typedef struct {
#if linux | LITTLEENDIAN
	unsigned extract_table : 4;	/* extract table index */
	unsigned repeat_flag : 1;	/* repeat flag, on or off */
	unsigned scan_sum : 1;		/* # of frames to sum in scan direction */
	unsigned scan_step : 8;		/* slit scan step size */
	unsigned bin_slit : 2;		/* camera pixel summing in slit direction */
	/* next 16 bit word */
	unsigned cycles : 4;		/* integration cycles */
	unsigned CCD_size : 1;		/* single or double width */
	unsigned slit_positions : 11;	/* # of slit positions */
	/* next 16 bit word */
	/* unsigned map_center : 12; */	/* map center position */
	unsigned scales : 8;	/* scaling options, 0 for none, 1 for I, 2 for I + V, 3 for all */
	unsigned sparebits : 6;		/* spares */
	unsigned tt_reset : 1;		/* tip tilt mirror reset flag */
	unsigned bad_pixel : 1;		/* bad pixel correction flag */
#else
	unsigned bin_slit : 2;		/* camera pixel summing in slit direction */
	unsigned scan_step : 8;		/* slit scan step size */
	unsigned scan_sum : 1;		/* # of frames to sum in scan direction */
	unsigned repeat_flag : 1;	/* repeat flag, on or off */
	unsigned extract_table : 4;	/* extract table index */
	/* next 16 bit word */
	unsigned slit_positions : 11;	/* # of slit positions */
	unsigned CCD_size : 1;		/* single or double width */
	unsigned cycles : 4;		/* integration cycles */
	/* next 16 bit word */
	/* unsigned map_center : 12; */	/* map center position */
	unsigned bad_pixel : 1;		/* bad pixel correction flag */
	unsigned tt_reset : 1;		/* tip tilt mirror reset flag */
	unsigned sparebits : 6;		/* spares */
	unsigned scales : 8;	/* scaling options, 0 for none, 1 for I, 2 for I + V, 3 for all */
#endif
	/* next 16 bit word */
	unsigned char  spare[2];
} spp_op_parameters;

typedef struct {  /* changed 4/16/2001, now signed 16 bits and arc sec from
		  CCD center is the unit */
	short  center_col;
	short  center_row;
} ext_reg_parameters;
typedef struct {  /* changed 6/26/2001, SP different from FG */
	short  map_center;
	short  spare;
} map_reg_parameters;
/* the # of CCD pixels per lsb of the FDB parameters */
/* 3/27/2003 - change from 32 to 16 */
#define	FDB_PIXEL_UNIT	16
typedef struct {
  unsigned short obsProgramVer;
  unsigned short seqTableVer;
  unsigned short parameterTableVer;
  unsigned short reserved1;
  unsigned char  obsProgramNum;  /* range is 1 - 20, #20 is flare program */
  unsigned char  mainRoutineNrepeats;
  unsigned char  mainRoutineCurrentCount;
  unsigned char  mainRoutinePosition;  /* range is 1 - 8 */
  unsigned char  subRoutineNum;        /* range is 1 - 4 */
  unsigned char  subRoutineNrepeats;
  unsigned char  subRoutineCurrentCount;
  unsigned char  subRoutinePosition;   /* range is 1 - 8 */
  unsigned char  seqTableNum;          /* range is 1 - 100 */
  unsigned char  seqTableNrepeats;
  unsigned char  seqTableCurrentCount;
  unsigned char  seqTablePosition;   /* range is 1 - 8 */
  /* before 6/1/2005, this was just */
  //unsigned char  x[20];
} mdp_obs_table;

 /* macro commands are presently 41 bytes long and are therefore expanded to
 44 by the compiler. The "pad" at the end makes this structure 44 but any
 copies of content should just copy the 41 bytes. Note also that the
 2 compression parameter areas are done inline to avoid gaps (because each
 is only 2 bytes long) */
 
 /* but ... the macro command structures here have the initial ID removed
 to make a 40 byte structure. The ID is handled as a separate item in
 the code reading macro commands, mostly so we can easily copy the rest
 without pads at the end */

typedef struct {		/* fg_macro_command without the ID */
	/* unsigned char id; */

#if linux | LITTLEENDIAN
	unsigned char type;
	unsigned char len;
	unsigned short main_id;
	/* compression_parameters compress1; */  /* for a choice */
	/* put in line to avoid structure alignment gaps */
	unsigned c1_quantization : 3;
	unsigned c1_huffman_dc : 2;
	unsigned c1_huffman_ac : 2;
	unsigned c1_reserved_2 : 1;
	unsigned c1_image_compression_mode : 3;
	unsigned c1_bit_compression_mode : 4;
	unsigned c1_reserved_1 : 1;

	/* compression_parameters compress2; */
	/* put in line to avoid structure alignment gaps */
	unsigned c2_quantization : 3;
	unsigned c2_huffman_dc : 2;
	unsigned c2_huffman_ac : 2;
	unsigned c2_reserved_2 : 1;
	unsigned c2_image_compression_mode : 3;
	unsigned c2_bit_compression_mode : 4;
	unsigned c2_reserved_1 : 1;
#else
	unsigned char len;
	unsigned char type;
	unsigned short main_id;
	/* compression_parameters compress1; */  /* for a choice */
	/* put in line to avoid structure alignment gaps */
	unsigned c1_reserved_1 : 1;
	unsigned c1_bit_compression_mode : 4;
	unsigned c1_image_compression_mode : 3;
	unsigned c1_reserved_2 : 1;
	unsigned c1_huffman_ac : 2;
	unsigned c1_huffman_dc : 2;
	unsigned c1_quantization : 3;

	/* compression_parameters compress2; */
	/* put in line to avoid structure alignment gaps */
	unsigned c2_reserved_1 : 1;
	unsigned c2_bit_compression_mode : 4;
	unsigned c2_image_compression_mode : 3;
	unsigned c2_reserved_2 : 1;
	unsigned c2_huffman_ac : 2;
	unsigned c2_huffman_dc : 2;
	unsigned c2_quantization : 3;
#endif

	flt_op_parameters parameters;
	ext_reg_parameters ext_reg;
	mdp_obs_table  mdp_obs;
} fg_macro_command;

typedef struct {	/* sp_macro_command without the id */
	/* unsigned char id; */

#if linux | LITTLEENDIAN
	unsigned char type;
	unsigned char len;
	unsigned short main_id;
	/* compression_parameters compress1; */  /* for a choice */
	/* put in line to avoid structure alignment gaps */
	unsigned c1_quantization : 3;
	unsigned c1_huffman_dc : 2;
	unsigned c1_huffman_ac : 2;
	unsigned c1_reserved_2 : 1;
	unsigned c1_image_compression_mode : 3;
	unsigned c1_bit_compression_mode : 4;
	unsigned c1_reserved_1 : 1;

	/* compression_parameters compress2; */
	/* put in line to avoid structure alignment gaps */
	unsigned c2_quantization : 3;
	unsigned c2_huffman_dc : 2;
	unsigned c2_huffman_ac : 2;
	unsigned c2_reserved_2 : 1;
	unsigned c2_image_compression_mode : 3;
	unsigned c2_bit_compression_mode : 4;
	unsigned c2_reserved_1 : 1;
#else
	unsigned char len;
	unsigned char type;
	unsigned short main_id;
	/* compression_parameters compress1; */  /* for a choice */
	/* put in line to avoid structure alignment gaps */
	unsigned c1_reserved_1 : 1;
	unsigned c1_bit_compression_mode : 4;
	unsigned c1_image_compression_mode : 3;
	unsigned c1_reserved_2 : 1;
	unsigned c1_huffman_ac : 2;
	unsigned c1_huffman_dc : 2;
	unsigned c1_quantization : 3;

	/* compression_parameters compress2; */
	/* put in line to avoid structure alignment gaps */
	unsigned c2_reserved_1 : 1;
	unsigned c2_bit_compression_mode : 4;
	unsigned c2_image_compression_mode : 3;
	unsigned c2_reserved_2 : 1;
	unsigned c2_huffman_ac : 2;
	unsigned c2_huffman_dc : 2;
	unsigned c2_quantization : 3;
#endif

	spp_op_parameters parameters;
	map_reg_parameters map_reg;
	mdp_obs_table  mdp_obs;
} sp_macro_command;

typedef union {
	sp_macro_command sp;
	fg_macro_command fg;
} macro_command;

/* in pbgui and friends, the fbd_flt structures are loaded in the code and
therefore we don't have to reverse the bit definitions (we don't load with a
bigendian byte stream */

typedef struct {
 /* the frame definition block for the FLT, padded to 6 bytes */
	unsigned fid : 8;
	unsigned ccd_mode : 4;
	unsigned mask_position : 4;
	unsigned row_sum : 2;
	unsigned col_sum : 2;
	unsigned shutter_mode : 2;
	unsigned spare1 : 2;
	unsigned width : 8;		/* width on CCD, N/S on sun */
	unsigned height : 8;		/* E/W on sun */
	unsigned row_bin : 2;
	unsigned col_bin : 2;
	unsigned spare2 : 4;
} fbd_flt;
typedef fbd_flt fdb_sp;		/* sp is the same, may change */
 /* codes for the wavelengths */
#define BFI_NO_MOVE	0
#define BFI_3883	1
#define BFI_3968	2
#define BFI_4305	3
#define BFI_4504	4
#define BFI_5550	5
#define BFI_6684	6
#define NFI_NO_MOVE	7
/* TF wavelengths, the first 10 use last correction */
#define NFI_5172	8
#define NFI_5250	9
#define NFI_5576	10
#define NFI_6302	11
#define NFI_6563	12
#define NFI_5896	13
 /* 4 laser lines for testing */
#define NFI_6328	14
#define NFI_5430	15
#define NFI_5940	16
#define NFI_6117	17
/* the unchangable "base" wavelengths for the TF, these always use a
zero correction */
#define NFI_5172BASE	18
#define NFI_5250BASE	19
#define NFI_5576BASE	20
#define NFI_6302BASE	21
#define NFI_6563BASE	22
#define NFI_5896BASE	23
 /* 4 laser lines for testing */
#define NFI_6328BASE	24
#define NFI_5430BASE	25
#define NFI_5940BASE	26
#define NFI_6117BASE	27
#define FLT_SPARE2	28
 /* the rest, 29 - 31 are undefined for now */

 /* the official spatial scales in milli-arc sec, adjust when necessary */
#define BFI_SCALE	53
#define NFI_SCALE	80

/* data product codes, these are part of the Sub ID in a 5 bit field */
#define DPC_STOKES_I		0
#define DPC_STOKES_Q		1
#define DPC_STOKES_U		2
#define DPC_STOKES_V		3
#define DPC_SP_RAW		4
#define DPC_FG_RAW		5
#define DPC_SPARE_2		6
#define DPC_SPARE_3		7
#define DPC_SPARE_4		8
#define DPC_SPARE_5		9
#define DPC_SPARE_6		10
#define DPC_INTENSITY		11
#define DPC_MAGNETOGRAM		12
#define DPC_DOPPLERGRAM		13
#define DPC_FILTERGRAM		14
#define DPC_DARK		15
#define DPC_CT_FRAME_REF	16
#define DPC_CT_FRAME_LIVE	17
#define DPC_DIAGNOSTIC		18
#define DPC_CALIBRATION_1	19
#define DPC_SP_ENGG_FR		20
#define DPC_FG_ENGG_FR		21
#define DPC_ANALOG_DUMP		22
/* might want to add codes for the dg va and vb products */
/* uncharted region */
#define DPC_MEMORY_DUMP		30
#define DPC_TEST		31

 /* mask definitions as stored in FDB, 0 is none and increasing
 values correspond to smaller masks */
#define	MASK0	0	/* no mask, full CCD */
#define	NOMASK	0	/* no mask, a synomyn for MASK0 */
#define	MASK1	1	/* 800 pixel mask */
#define	MASK2	2	/* 400 pixel mask */
#define	MASK3	3	/* 200 pixel mask */
#define	MASK4	4	/* 100 pixel mask */
#define	MASK5	5	/* closed, blocks entire CCD */

 /* FG obs ID's, this is a 8 bit field so 256 allowed */
#define	FGmaskless		1	/* full FOV filtergraph, no mask */
#define	FGshutteredIV		2	/* I and V at one wavelength */
#define	FGshutteredStokes	3	/* IQUV at one wavelength */

#define	FGshutteredMG4		4	/* 4 wavelength MG, V/I */
#define	FGshutteredMG4IV	5	/* 4 wavelength MG, V & I */
#define	FGshutteredMG2		6	/* 2 wavelength MG, V/I */
#define	FGshutteredMG2IV	7	/* 2 wavelength MG, V & I */
#define	FGshutteredMG1		8	/* 1 wavelength MG, V/I */

#define	FGshutteredDG4		9	/* 4 wavelength DG, single v */
#define	FGshutteredDG4_A_B	10	/* 4 wavelength DG, va and vb */
#define	FGshutteredDG2		11	/* 2 wavelength DG, v only */

/* the wavelength scans are now specified via macro command parameters
for FGmaskless, FGshutteredIV, and FGshutteredStokes */
/*#define	FGshutteredWscan	12 */
/*#define	FGshutteredWscanIV	13 */
/*#define	FGshutteredWscanStoke	14 */
/* re-use 12 for a focus scan */
#define	FGshutteredFocusScan	12
#define	FGintegratedWaveScan	13
#define	FGlaserTune		14
#define	FGemiTest		15
#define	FGintegratedWaveScanUpdate	16
#define	FGshutteredIV2		17	/* I and V at one wavelength, this uses .1s */
#define	FGshutteredDGV		18	/* I, DG, and Stokes V */
/* note that all shutterless must have a value >= 32 */

#define	FGshutterlessIV		32	/* I and V shutterless */	
#define	FGshutterlessIQUV	33	/* full Stokes shutterless */	
#define	FGshutterlessIQ		34	/* I and Q shutterless */	
#define	FGshutterlessIU		35	/* I and U shutterless */	
#define	FGshutterlessIUV	36	/* I, U and V shutterless */	
  /* this next one would be awkward to run and might not be used */
#define	FGshutterlessIQV	37	/* I, Q and V shutterless */
  /* the next 2 are I/V modes that use .2s and .1s cycles and "throw away"
  the other potential results, this is done to bypass possible saturation with
  the .4s cycle and CCD summing */
#define	FGshutterlessIV2	38	/* I and V shutterless using .2s cycles */	
#define	FGshutterlessIV3	39	/* I and V shutterless using .1s cycles */	

 /* for raw exposures, we need to know the row lengths */
#define	SP_NX_RAW_SCI	1024
#define	SP_NX_RAW_ENG	1040
#define	FG_NX_RAW_SCI	2048
#define	FG_NX_RAW_ENG	2064


 /* expanded structure for filtergraph operational parameters, these are
 extracted from the macro command, expand this list as needed */
typedef struct {
	unsigned short obs_id;
	unsigned short frame_def_id;
	unsigned short wave;
	short 	       wave_off;  /* this is signed */
	unsigned short exposure;
	unsigned short dark_flag;
	unsigned short ix0;
	unsigned short ix1;
	unsigned short iy0;
	unsigned short iy1;
	unsigned short nx;
	unsigned short ny;
	unsigned short nxCCD;
	unsigned short nyCCD;
	unsigned short ny1;
	unsigned short ny2;
	unsigned short nyp;
	unsigned short npack1;
	unsigned short npack2;
	unsigned short nframes;
	unsigned short wscan_npos;
	unsigned short wscan_step;
	unsigned short roi_start;
	unsigned short roi_stop;
	unsigned short sumx;
	unsigned short sumy;
	unsigned short binx;
	unsigned short biny;
	unsigned short ccd_mode;
	unsigned short mask;
	unsigned short shutter_mode;
 } fg_expanded_parameters;

 /* DMA page flags, used in the DMA1, etc members of FGimageParameters */
 /* 7/18/2002 - these changed */
#define DMA_ADD		2
#define DMA_SUB		3
#define DMA_LOAD	0
#define DMA_LOADCOMP	1
 /* flags for DMA write mode */
 /* the ENABLE/DISABLE removed */
//#define DMA_ENABLE	0
//#define DMA_DISABLE	1
#define DMA_ONE_PAGE	1
#define DMA_FOUR_PAGE	0	   
#define DMA_SCALE	1
#define DMA_NOSCALE	0

 /* a FG OBS DEF Table consists of a FGobsDefHeader followed by a set
 of FGimageParameters, one set for each image (exposure) taken */
 /* the image blocks for the FG OBS DEF Tables */
 /* 7/18/2002 changed now that we know smart memory interface better */
typedef struct {
#if linux | LITTLEENDIAN
	unsigned  phase : 4;
	unsigned  delay : 12;
	short addWaveOffset;  /* will be used as signed value */
	unsigned	DMA4_op : 2; 
	unsigned	DMA4_scale : 1;
	unsigned	DMA3_op : 2;
	unsigned	DMA3_scale : 1;
	unsigned	DMA2_op : 2;
	unsigned	DMA2_scale : 1;
	unsigned	DMA1_op : 2;
	unsigned	DMA1_scale : 1;
	unsigned	DMA_rw : 1;
	unsigned	DMA_buffer_mode : 1;
	unsigned	DMA_unused : 2;
#else
	unsigned  delay : 12;
	unsigned  phase : 4;
	short addWaveOffset;  /* will be used as signed value */
 /* 7/18/2002 - remove the enable bit, use bit 13 for 4 vs 1 page */
	/* unsigned  DMA1_enable : 1; */
	unsigned  DMA_unused : 2;
	unsigned  DMA_buffer_mode : 1;
	unsigned  DMA_rw : 1;
	unsigned  DMA1_scale : 1;
	unsigned  DMA1_op : 2;
	/* unsigned  DMA2_enable : 1; */
	unsigned  DMA2_scale : 1;
	unsigned  DMA2_op : 2;
	/* unsigned  DMA3_enable : 1; */
	unsigned  DMA3_scale : 1;
	unsigned  DMA3_op : 2;
	/* unsigned  DMA4_enable : 1; */
	unsigned  DMA4_scale : 1;
	unsigned  DMA4_op : 2;
#endif
	char	  junk[2];	/* to align structure,
				not actually used in tables */
 } FGimageParameters;

 /* the header for the  FG OBS DEF Tables */
typedef struct {
#if linux | LITTLEENDIAN
	unsigned char gen_function;
	unsigned char obs_id;
	unsigned char spare;
	unsigned  repeat_count : 4;
	unsigned  nimages : 4;
#else
	unsigned char obs_id;
	unsigned char gen_function;
	unsigned  nimages : 4;
	unsigned  repeat_count : 4;
	unsigned char spare;
#endif
 } FGobsDefHeader;   /* this should be 4 bytes in size */

 /* the structures for the unpacked tables */
typedef struct {
	int	delay;
	int	phase;
	int 	addWaveOffset;
	int	DMA_buffer_mode;
	int	DMA1_scale;
	int	DMA1_op;
	int	DMA2_scale;
	int	DMA2_op;
	int	DMA3_scale;
	int	DMA3_op;
	int	DMA4_scale ;
	int	DMA4_op;
	unsigned  int	DMA_config;
 } FGunpackedImageParameters;

typedef struct {
	unsigned char	fw[2];
	unsigned char	tf[8];
	unsigned char	mask;
	unsigned char	spare;
} FGmechs;

#define MAXOBSIMAGES 24
#define PMUISR_NOOP  0xffff0000
#define CAMCMD_READOUTNEXTPMU 0x2
#define CAMCMD_READOUTNOW 0x22

#define	CAMCMD_INTEGRATENOW 0x2
/* turn off PMU readout is currently assumed to be a camera NO-op command */
#define CAMCMD_TURNOFFREADOUT 0x0

typedef struct {
	unsigned char obs_id;
	unsigned char gen_function;
	unsigned char nimages;
	unsigned char repeat_count;
	FGunpackedImageParameters  p[MAXOBSIMAGES];
 } FGunpackedObsTableType;

/* smart memory configuration structures */

typedef struct {
    int		camcmd;
    short	unused;
#if linux | LITTLEENDIAN
    unsigned	DMA4_op : 2; 
    unsigned	DMA4_scale : 1;
    unsigned	DMA3_op : 2;
    unsigned	DMA3_scale : 1;
    unsigned	DMA2_op : 2;
    unsigned	DMA2_scale : 1;
    unsigned	DMA1_op : 2;
    unsigned	DMA1_scale : 1;
    unsigned	DMA_rw : 1;
    unsigned	DMA_buffer_mode : 1;
    unsigned	DMA_unused : 2;
#else
    unsigned	DMA_unused : 2;
    unsigned	DMA_buffer_mode : 1;
    unsigned	DMA_rw : 1;
    unsigned	DMA1_scale : 1;
    unsigned	DMA1_op : 2;
    unsigned	DMA2_scale : 1;
    unsigned	DMA2_op : 2;
    unsigned	DMA3_scale : 1;
    unsigned	DMA3_op : 2;
    unsigned	DMA4_scale : 1;
    unsigned	DMA4_op : 2; 
#endif
 } SmartMemConfigBits;

typedef struct {
	int		camcmd;
	unsigned int	DMA_config;
 } SmartMemConfig;

 /* a smart memory table, these are passed to the regAcq method,
 note that MAXOBSIMAGES is also used here, each entry is 8 bytes */

typedef struct {
	int	nphases;
	int	start_phase;
	int	unused_1;
	int	unused_2;
	SEM_ID	semaphore;
	SmartMemConfig	p[MAXOBSIMAGES];
 } SmartMemDACT;

 /* 4/20/2005 - support a shorter version for the SP, we only need 8 entries */
 
typedef struct {
	int	nphases;
	int	start_phase;
	int	unused_1;
	int	unused_2;
	SEM_ID	semaphore;
	SmartMemConfig	p[8];
 } SPsmartMemDACT;

typedef struct {
	int	nphases;
	int	start_phase;
	int	unused_1;
	int	unused_2;
	SEM_ID	semaphore;
	SmartMemConfigBits	p[8];
 } SPsmartMemDACTbits;

 /* expanded structure for sp operational parameters, these are
 extracted from the macro command */
typedef struct {
	unsigned short sumx;   /* bin_slit but actually a CCD sum in spatial direction */
	unsigned short scan_step;
	unsigned short scan_sum;   /* sum 2 consecutive slit positions */
	unsigned short repeat_flag;
	unsigned short extract_table;
	unsigned short slit_positions;
	unsigned short CCD_size;
	unsigned short cycles;
	/* 9/30/2003 RAS - map_center must be signed! */
	//unsigned short map_center;
	short map_center;
	unsigned short tt_reset;
	unsigned short scales;
	unsigned short bad_pixel;
	unsigned short nspect;		/* # of spectral pixels */
	unsigned short nspace;		/* # of spatial pixels */
	unsigned short spaceOffset;	/* spatial offset */
	unsigned short specOffset;	/* spectral offset */
	unsigned short roi_start;
	unsigned short roi_stop;
	unsigned short sumy;		/* should be zero but h/w supports it */
 } sp_expanded_parameters;


/* --------------- start Packet definitions ---------------------------*/
#define PCK_FIRST_FRAME		1
#define PCK_LAST_FRAME		2
#define PCK_MIDDLE_FRAME	0
#define PCK_SINGLE_FRAME	3
#define PCK_FIRST_PACKET	1
#define PCK_LAST_PACKET		2
#define PCK_MIDDLE_PACKET	0
#define PCK_SINGLE_PACKET	3
 /* packet header structures, a packet header contains a packet_info,
 compression_parameters, and data_info */
typedef struct {
	unsigned char type;
	unsigned char size[3];  /* packet length in bytes */
	int	 serial_no;
	short	 id;
	unsigned main_seq_flag : 2;
	unsigned main_seq_count : 14;
	unsigned reserved : 2;
	unsigned num_packets : 8;
	unsigned num_frames : 6;
	/* sub ID, 16 packed bits */
	unsigned subIdHdr: 1;
	unsigned subIdCCDseg: 1;
	unsigned subIdAmpA: 1;
	unsigned subIdAmpB: 1;
	unsigned subIdSumMode: 2;
	unsigned subIdFGtype: 1;
	unsigned subIdShutterless: 1;
 /* 5/2/2001 - changed the size of the subIdProduct to 5 (from 4) and
 the spare to 3 (from 4) */
	unsigned subIdSpare: 3;
	unsigned subIdProduct: 5;

	unsigned	sub_seq_flag : 2;
	unsigned	sub_seq_count : 14;
	short	nx;	/* size of image obtained by FPP, called S, N/S for FG */
	short	ny;	/* called P, E/W for FG */
	short	x0;	/* coord (upper left corner) of partial image */
	short	y0;
	short	nxp;	/* size of partial image in a packet */
	short	nyp;
	/* the compression parameters were moved into here to make this
	structure a multiple of 4 bytes long */
	unsigned reserved_1 : 1;
	unsigned bit_compression_mode : 4;
	unsigned image_compression_mode : 3;
	unsigned reserved_2 : 1;
	unsigned huffman_ac : 2;
	unsigned huffman_dc : 2;
	unsigned quantization : 3;
 } packet_info;


typedef struct {
	UINT32	packet_time;
	unsigned char macro_copy[41];
	unsigned char spare[3];
 } data_info;


 /* and here is the packet_header */

typedef struct {	/* PacketHeader */
	packet_info	info;
	data_info	data;	/* not THE data, just data info */
 } PacketHeader;

 /* for the CT packets, we have a different data section, 64 bytes
 in the data section and 96 total */
typedef struct {
	UINT32 packet_time;
	UINT16 RefFrmCnt;
	UINT16 CTFrmNum;
	UINT16 sums[13];
	UINT16 CTmode;
	UINT16 CTMAD;
        UINT16 CTmeanCurrent;
        UINT16 CTmeanNext;
        UINT16 ref_count;
	UINT16 CTstatus;
	INT16  LiveErrX;
	INT16  LiveErrY;
	UINT16 CTMEstatus;
	UINT16 CTMEangleX;
	UINT16 CTMEangleY;
	UINT16 jitter_count;  /* was a spare, may become one again */
	/* the next is the frame count when the packet is emitted, may
	be useful for checking timing */
	UINT16 CTfn_write;      /* was a spare, may become one again */
	unsigned char MDPclockHigh;  /* added 1/17/2004, and made jitter_type a char */
	unsigned char jitter_type;  /* was a spare, may become one again */
	UINT16 focus;	     /* was a spare */
} CTpacket_data_info;

 /* and we define a structure type for CT packet headers */
typedef struct {	/* CTpacketHeader */
	packet_info	info;
	CTpacket_data_info	data;	/* not THE data, just data info */
 } CTpacketHeader;

typedef struct {	/* complete CTpacket for refs and lives */
  CTpacketHeader Head;	// this is our packet header (typedef in command.h)
  UINT16 Image[3136];	// 56*56 shorts
}  CTpacket;	// total size should be 6368 bytes

 /* also define a packet header for diagnostic data, these are also 96 bytes,
 64 in data info section like the jitter packets */
typedef struct {
	UINT32 	packet_time;
	UINT32 	start_time;
	UINT32 	startCTcounter;
	UINT32  ntimes;		/* # of times actually in this packet */
	unsigned char  nchan;
	unsigned char  nticks;	/* this is the cadence */
	UINT16 devices[16];
	UINT16	gcuconfig;	/* added 1/16/2004 - a tag to help ID tests */
	unsigned char MDPclockHigh;  /* added 1/17/2004 */
	unsigned char  pads[11];
} DIAGpacket_data_info;

 /* and we define a structure type for diagnostic packet headers */
typedef struct {	/* DIAGpacketHeader */
	packet_info	info;
	DIAGpacket_data_info	data;	/* not THE data, just data info */
 } DIAGpacketHeader;

 /* another type of diagnostic packet for the I scan results, also 96 bytes
 total with 64 in the data section */
 
typedef struct {
	UINT32	packet_time;
	unsigned char macro_copy[41];
	unsigned char spare[1];
	short	wcorr;		/* used by FGintegratedWaveScanUpdate */
	UINT32	nwave;
	unsigned char peaks[8];  /* only used for laser scans */
	UINT16	gcuconfig;	/* added 1/16/2004 - a tag to help ID tests */
	unsigned char MDPclockHigh;  /* added 1/17/2004 */
	unsigned char spare2[1];
 } Iscan_data_info;

 /* and we define a structure type for I scan packet headers */
typedef struct {	/* IscanPacketHeader */
	packet_info	info;
	Iscan_data_info	data;	/* not THE data, just data info */
 } IscanPacketHeader;

typedef struct {
	UINT32	CTatOpen;
	/* changed 9/15/2003 to remove CTatClose and use 32 bits for
	shutterOpenTime and shutterCloseTime */
	//UINT32	CTatClose; /* could replace if something else needed */
	//UINT16 shutterOpenTime;
	//UINT16 shutterCloseTime;
	UINT32 shutterOpenTime;
	UINT32 shutterCloseTime;
	short	woff;
	unsigned  phaseAtOpen : 4;
	unsigned  phaseAtClose : 4;
	unsigned char spare;
	UINT32	sums[2];
	unsigned char	mtr[8];
	UINT16	therm[8];
	UINT16	eltemp[8];
 } IscanData;	/* 64 bytes for each measurement */
