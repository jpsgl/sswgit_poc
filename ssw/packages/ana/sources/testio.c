/* test some i/o stuff */
#include <sys/types.h>
#include <termios.h> 
#include <unistd.h>
#include <stdio.h>
typedef unsigned char byte;
 /* start main */
main (int argc, char *argv[])
 {
 struct termios new_params, old_params;
 int	batch_flag;
printf("sizes of byte, short, int, long and long long  are:%d,%d,%d,%d,%d\n",
sizeof(byte),sizeof(short),sizeof(int),sizeof(long),sizeof(long long));
printf("pointer size = %d\n", sizeof( byte *));

 if (tcgetattr(1, &old_params) < 0)
    {perror("tcgetattr failed, assuming batch mode"); batch_flag=1; }
 else {
 new_params = old_params;
 new_params.c_cc[VMIN] = 1;
 new_params.c_cc[VTIME] = 0;
 new_params.c_lflag &= ~ICANON;         /* direct read -> no wait for NL */
 new_params.c_lflag &= ~ECHO;           /* no direct echo of input */
 }

 if (tcsetattr(1, TCSAFLUSH, &new_params) < 0)
    {perror("tcsetattr failed"); }
 else {
 	printf("tcsetattr OK\n");
 }

 /* now reset */
 if (tcsetattr(1, TCSAFLUSH, &old_params) < 0)
    {perror("tcsetattr failed"); }
 else {
 	printf("reset tcsetattr OK\n");
 }
 
 exit(0);
 }
