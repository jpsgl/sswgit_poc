/* files.c, begun 12/5/91 using fragments from earlier fzread modules */
/* for little endian machines, define LITTLEENDIAN or this will not compile
   correctly ! */
#include "ana_structures.h"
#include "defs.h"
#include "tiff.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include <sys/param.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <regex.h>
#if defined(__alpha)
#else
#include <sys/statfs.h>
#endif
//#include <unistd.h>
#if NeXT
#include <sys/dir.h>
#else
#if __FreeBSD__
#include <sys/dir.h>
#else
#include  <dirent.h>
#endif
#endif
 /* for SGI only (?) */
#if __sgi
#include  <malloc.h>
#endif
 /*------------------------------------------------------------------------- */
FILE *fopenr_sym(nsym)
 /* decode an ana string symbol and use as a file name to open a file for
 read */
 int	nsym;
 {
 FILE *fopen(),*fin;
 if ( sym[nsym].class != 2 ) { execute_error(70); return NULL; }
 name = (char *) sym[nsym].spec.array.ptr;
						 /* try to open the file */
 if ((fin=fopen(name,"r")) == NULL) { file_open_error(); return NULL; }
 return fin;
 }
 /*------------------------------------------------------------------------- */
int ana_fits_read(narg, ps)		/* read fits files */
 /* status = fits_read(x,name,h,
 int	narg, ps[];
 {
 
 
 
 
 
 }
