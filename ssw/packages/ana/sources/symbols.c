/* symbol module, begun 6/6/91 r. shine */
 /* symbol tables, organized differently from VMS version */
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <setjmp.h>
#include "ana_structures.h"
#include "defs.h"
 /* for SGI only (?) */
#if __sgi
#include  <malloc.h>
#endif
#define N_U_SYMBOLS	3000
#define N_S_SYMBOLS	6000
#define N_EDB2		53000
#define N_EDB		3000
#define N_SPS		64
#define N_TEMPS		64
 /* 5 symbol areas defined by the following bases */
#define 	SUBR_BASE (N_U_SYMBOLS)
#define		EDB2_BASE (SUBR_BASE+N_S_SYMBOLS)
#define		EDB_BASE (EDB2_BASE+N_EDB2)
#define 	SPS_BASE  (EDB_BASE+N_EDB)
#define 	TEMP_BASE (SPS_BASE+N_SPS)
 /* upper limit for each area defined below */
#define	SYM_TOP		SUBR_BASE
#define	SUBR_TOP	EDB2_BASE
#define	EDB_TOP		SPS_BASE
#define	EDB2_TOP	EDB_BASE
#define 	MAX_SYM		N_U_SYMBOLS
#define	MAX_TEMP	(TEMP_BASE+N_TEMPS)
#define 	N_SYMBOLS	(MAX_TEMP)
 extern	int pop();
 extern	jmp_buf	sjbuf;
 extern	struct sym_list		*subr_sym_list[];
 extern	int	execute_error();
 extern	int	edb_context;
 int	n_u_symbols=N_U_SYMBOLS;
 int	n_sps=N_SPS;
 int	temp_base = TEMP_BASE;
 int	n_temps=N_TEMPS;
 int	sps_base=SPS_BASE;
 int	n_symbols=N_SYMBOLS;
 int	vfix_top;
 int	edb_base = EDB_BASE;
 int	edb2_base = EDB2_BASE;
 unsigned int new_symbol(char *s);
 /* symbol descriptor headers as structures and unions */
 struct sym_desc sym[N_SYMBOLS];		/*symbol table */
 /* symbol string tables, use chains and hash lookup */
 struct sym_table {
	char *name; int context; int num; struct sym_table *next; };
#define HASHSIZE 64
 static struct sym_table *symbols[HASHSIZE];		/*ptrs to chains */	
 /*ana scalar types are 0 to 4 */
 int	ana_type_size[5]={1,2,4,4,8};
 /* various symbol #'s and limits */
 int	cur_sym={0}, max_temp={TEMP_BASE}, max_sp={SPS_BASE};
 int	max_user_sym={SUBR_BASE}, max_edb_sym={EDB_BASE};
 int	max_edb2_sym={EDB2_BASE};
 int	max_sym;
 int	lastmax_sym, lastmin_sym;
 int	redim_warn_flag=1, range_warn_flag=1;
 int	num_internal_syms=0;
 int	symbol_context=0;
 char	*strsave(char  *s);
 union	types_ptr { byte *b; short *w; int *l; float *f; double *d;};		
 /*------------------------------------------------------------------------- */
int symbol_init()	/* called once to initialize the internal symbols */
 {
 /* ana system variables that are stored elsewhere */
 extern	int	scalemax, lastmaxloc, lastminloc;
 extern	union scalar	lastmax, lastmin;
 extern	int	maxhistsize, histmin, histmax,scalemin, stretchmark;
 extern	int	maxregridsize, tvsmt, lunplt, resample_type, fgccdshiftDebug;
 extern	float	xmin, xmax, ymin, ymax, callig_xb, callig_yb;
 extern	float	wxb,wxt,wyb,wyt,ticx,ticy,plims[4],xlimit,ylimit;
 extern	float	xfac, yfac, biggest_x, biggest_y;
 extern	float	fsized,symsize,xhair,yhair,symratio,ticxr,ticyr,dvx,dv;
 extern	int	ilabx,ilaby,irxf,iryf,ndx,nd,ipltyp,ifz,ifzx,ndxs,ndys,ier;
 extern	int	ifont,ndlabx,ndlaby,iblank,ndot,ifirstflag;
 extern	int	ybotmost, xleftmost, landscape;
 extern	int	badmatch, stretch_clip, itmax, bias_flag, badCellFlag, nBadCells;
 extern	int	narg_user, iorder, fftdp, crunch_slice, crunch_bits, badValue;
 extern	float	crunch_bpp;
 extern	int	autocon, contour_mode, contour_box, contour_flag;
 extern	int	contour_nlev, contour_sym, contour_ticks;
 extern	float	contour_tick_pen, contour_border;
 extern	float	contour_dash_lev, contour_tick_fac;
 extern  int     ixlow, iylow, ixhigh, iyhigh;
 extern	int	byte_count, tape_messages;
 extern	int	sort_flag, match_mess, seed;
 extern	int	gfit_fix_a, gfit_fix_b, gfit_fix_x0, gfit_fix_del, gfit_show;
 extern	int	gfit_guess_mode, gfit_converged, gfit_warnings;
 extern	int	cosinefit_fix_a, cosinefit_fix_b, cosinefit_fix_x0, cosinefit_fix_del, cosinefit_show;
 extern	int	cosinefit_guess_mode, cosinefit_converged, cosinefit_warnings;
 extern	int	psfontflag, true_color24, motif_flag, motif_called_widget;
 extern	int	motif_echo_flag, tvplanezoom, revolve_mode, true_color16, extractline_mode;
 extern	float	gsmooth_width, display_height, display_width;
 extern	float	solar_rotate_a, solar_rotate_b, solar_rotate_c;
 extern	double	spawn_status, meritc, photometric;
 /* X related externals */
 extern	int	xerrors, xcoord, ycoord, ana_button, ana_keycode, ana_keysym;
 extern	int	last_wid, root_x, root_y, blend_feedback, ana_keystate;
 extern	int	foreground_color, background_color, colormin, colormax;
 extern	int	ncolorcells, color_cells_to_try, visual_class, visual_depth;
 extern	float	tvix, tviy, tvixb, tviyb;
 extern	int	white_pixel,red_pixel,green_pixel,blue_pixel,yellow_pixel;
 extern	int	black_pixel, darkgreen_pixel, purple_pixel;
 extern	int	read_file_class, clear_outside_flag, xfontflag;
 extern	int	socketTimeout, socketTimeoutUsec, maxobjoverlap;
 extern unsigned int	kb;
 extern	double	last_time;
 /* Motif related externals */
 extern int	radio_button, radio_state, zoom_test;
 /* Solar B specific */
 extern int	ccsdsdebug;
 extern int	epsbbx0, epsbbx1, epsbby0, epsbby1;

 int i;
 l_scalar("#ZERO",0);
 l_scalar("#1",1);			/* must be second one, symbol # 1 */
 l_scalar("#MINUS1",-1);
 l_scalar("#42",42);
 l_scalar("#0",0);			/* must be fifth, symbol # 4 */
 f_scalar("#PI",3.1415926);
 f_scalar("#2PI", 6.283185308);
 f_scalar("#E", 2.718281828);
 f_scalar("#C", 2.997929E10);
 f_scalar("#G", 6.668E-8);
 f_scalar("#H", 6.6252E-27);
 f_scalar("#HB", 1.0544E-27);
 f_scalar("#EC", 6.6252E-27);
 f_scalar("#M", 9.1084E-28);
 f_scalar("#K", 1.38046E-16);
 f_scalar("#R", 8.317E7);
 f_scalar("#RSUN", 6.9598E10);
 f_scalar("#R.D", 57.29577951);
 f_scalar("#D.R", .017453293);
 f_scalar("#R2D", 57.29577951);
 f_scalar("#D2R", .017453293);
 l_scalar("#SYMTOP",SYM_TOP);
 l_scalar("#SUBRTOP",SUBR_TOP);
 l_scalar("#EDB2TOP",EDB2_TOP);
 l_scalar("#EDBTOP",EDB_TOP);
 /* some non-writable X related items */
 l_sys_var("#VISUAL_CLASS",&visual_class);
 l_sys_var("#VISUAL_DEPTH",&visual_depth);
 /* end of non-writable X related items */
 l_scalar("$DCT_ARRAY", 0);
 vfix_top = num_internal_syms-1;
 l_sys_var("#MAXSYM",&max_sym);
 l_sys_var("#MAXSUBRSYM",&max_user_sym);
 l_sys_var("#EDB",&max_edb_sym);
 l_sys_var("#EDB2",&max_edb2_sym);
 l_sys_var("!NARG",&narg_user);
 l_sys_var("!REDIM_WARN_FLAG",&redim_warn_flag);
 l_sys_var("!RANGE_WARN_FLAG",&range_warn_flag);
 l_sys_var("!BADMATCH",&badmatch);
 l_sys_var("!STRETCH_CLIP",&stretch_clip);
 l_sys_var("!BIAS_FLAG",&bias_flag);
 l_sys_var("!BAD_CELL_FLAG",&badCellFlag);
 l_sys_var("!BAD_VALUE",&badValue);
 l_sys_var("!NBAD_CELLS",&nBadCells);
 l_sys_var("!ITMAX",&itmax);
 l_sys_var("!SCALEMAX",&scalemax);
 l_sys_var("!SCALEMIN",&scalemin);
 l_sys_var("!LASTMAX",&lastmax);	lastmax_sym = cur_sym;
 l_sys_var("!LASTMIN",&lastmin);	lastmin_sym = cur_sym;
 l_sys_var("!LASTMINLOC",&lastminloc);
 l_sys_var("!LASTMAXLOC",&lastmaxloc);
 l_sys_var("!MAXHISTSIZE",&maxhistsize);
 l_sys_var("!HISTMIN",&histmin);
 l_sys_var("!HISTMAX",&histmax);
 l_sys_var("!STRETCHMARK",&stretchmark);
 l_sys_var("!RESAMPLETYPE",&resample_type);
 d_sys_var("!MERITC",&meritc);
 l_sys_var("!MAXREGRIDSIZE",&maxregridsize);
 l_sys_var("!TVSMT",&tvsmt);
 l_sys_var("!LABX",&ilabx);
 l_sys_var("!LABY",&ilaby);
 f_sys_var("!TICKX",&ticx);
 f_sys_var("!TICKY",&ticy);
 f_sys_var("!TICKXR",&ticxr);
 f_sys_var("!TICKYR",&ticyr);
 l_sys_var("!PDEV",&lunplt);
 l_sys_var("!PLTYP",&ipltyp);
 f_sys_var("!MYB",&ymin);
 f_sys_var("!MYT",&ymax);
 f_sys_var("!MXB",&xmin);
 f_sys_var("!MXT",&xmax);
 f_sys_var("!WYB",&wyb);
 f_sys_var("!WYT",&wyt);
 f_sys_var("!WXB",&wxb);
 f_sys_var("!WXT",&wxt);
 f_sys_var("!YB",&plims[2]);
 f_sys_var("!YT",&plims[3]);
 f_sys_var("!XB",&plims[0]);
 f_sys_var("!XT",&plims[1]);
 l_sys_var("!ERASE",&ier);
 l_sys_var("!RX",&irxf);
 l_sys_var("!RY",&iryf);
 l_sys_var("!NDX",&ndx);
 l_sys_var("!NDY",&nd);
 l_sys_var("!NDXS",&ndxs);
 l_sys_var("!NDYS",&ndys);
 l_sys_var("!FZ",&ifz);
 l_sys_var("!FZX",&ifzx);
 f_sys_var("!DVX",&dvx);
 f_sys_var("!DVY",&dv);
 f_sys_var("!XC",&callig_xb);
 f_sys_var("!YC",&callig_yb);
 l_sys_var("!FONT",&ifont);
 f_sys_var("!FSIZE",&fsized);
 f_sys_var("!SYMSIZE",&symsize);
 f_sys_var("!SYMRATIO",&symratio);
 f_sys_var("!BIGGEST_X",&biggest_x);
 f_sys_var("!BIGGEST_Y",&biggest_y);
 l_sys_var("!DOT", &ndot);
 l_sys_var("!DOTS", &ndot);
 l_sys_var("!DLABX", &ndlabx);
 l_sys_var("!DLABY", &ndlaby);
 l_sys_var("!LANDSCAPE", &landscape);
 l_sys_var("!PSFONT", &psfontflag);
 l_sys_var("!IBLANK", &iblank);
 l_sys_var("!FFTDP", &fftdp);
 l_sys_var("!CRUNCH_SLICE", &crunch_slice);
 l_sys_var("!CRUNCH_BITS", &crunch_bits);
 f_sys_var("!CRUNCH_BPP", &crunch_bpp);
 l_sys_var("!AUTOCON", &autocon);
 l_sys_var("!CONTOUR_MODE", &contour_mode);
 l_sys_var("!CONTOUR_BOX", &contour_box);
 f_sys_var("!CONTOUR_BORDER", &contour_border);
 l_sys_var("!CONTOUR_MODE", &contour_mode);
 l_sys_var("!CONTOUR_TICKS", &contour_ticks);
 f_sys_var("!CONTOUR_TICK_PEN", &contour_tick_pen);
 f_sys_var("!CONTOUR_TICK_FAC", &contour_tick_fac);
 f_sys_var("!CONTOUR_DASH_LEV", &contour_dash_lev);
 l_sys_var("!BC", &byte_count);
 l_sys_var("!TAPEMESSAGES", &tape_messages);
 l_sys_var("!ERRNO", &errno);
 l_sys_var("!SORT_FLAG", &sort_flag);
 l_sys_var("!MATCH_MESSAGES", &match_mess);
 l_sys_var("!REVOLVE_MODE", &revolve_mode);
 f_sys_var("!SOLAR_ROTATE_A", &solar_rotate_a);
 f_sys_var("!SOLAR_ROTATE_B", &solar_rotate_b);
 f_sys_var("!SOLAR_ROTATE_C", &solar_rotate_c);
 l_sys_var("!TIFF_PHOTOMETRIC", &photometric);
 l_sys_var("!READ_FILE_CLASS", &read_file_class);
 l_sys_var("!EXTRACTLINE_MODE", &extractline_mode);
 l_sys_var("!FGCCDSHIFTDEBUG", &fgccdshiftDebug);
 l_sys_var("!MAXOBJOVERLAP", &maxobjoverlap);

 /* X related variables */
 l_sys_var("!XERRORS",&xerrors);
 l_sys_var("!SCREEN_WIDTH",&display_width);
 l_sys_var("!SCREEN_HEIGHT",&display_height);
 l_sys_var("!IX",&xcoord);
 l_sys_var("!IY",&ycoord);
 l_sys_var("!ROOT_X",&root_x);
 l_sys_var("!ROOT_Y",&root_y);
 l_sys_var("!KB",(int *) &kb);
 f_sys_var("!XHAIR",&xhair);
 f_sys_var("!YHAIR",&yhair);
 l_sys_var("!IXHIGH",&ixhigh);
 l_sys_var("!IYHIGH",&iyhigh);
 l_sys_var("!BUTTON",&ana_button);
 l_sys_var("!KEY",&ana_keysym);
 l_sys_var("!KEYSYM",&ana_keysym);
 l_sys_var("!KEYSTATE",&ana_keystate);
 l_sys_var("!KEYCODE",&ana_keycode);
 l_sys_var("!WINDOW",&last_wid);
 l_sys_var("!IORDER",&iorder);
 l_sys_var("!TRUECOLOR",&true_color24);
 l_sys_var("!TRUECOLOR24",&true_color24);
 l_sys_var("!TRUECOLOR16",&true_color16);
 l_sys_var("!FOREGROUND_COLOR",&foreground_color);
 l_sys_var("!BACKGROUND_COLOR",&background_color);
 l_sys_var("!COLORMIN",&colormin);
 l_sys_var("!COLORMAX",&colormax);
 l_sys_var("!NCOLORCELLS",&ncolorcells);
 l_sys_var("!COLOR_CELLS_TO_TRY",&color_cells_to_try);
 f_sys_var("!TVIX",&tvix);
 f_sys_var("!TVIXB",&tvixb);
 f_sys_var("!TVIY",&tviy);
 f_sys_var("!TVIYB",&tviyb);
 d_sys_var("!XTIME",&last_time);
 l_sys_var("!TVPLANEZOOM",&tvplanezoom);
 l_sys_var("!TVBLENDFEEDBACK",&blend_feedback);
 l_sys_var("!TVCLEAR_OUTSIDE",&clear_outside_flag);
 l_sys_var("!WHITE_PIXEL",&white_pixel);
 l_sys_var("!RED_PIXEL",&red_pixel);
 l_sys_var("!GREEN_PIXEL",&green_pixel);
 l_sys_var("!BLUE_PIXEL",&blue_pixel);
 l_sys_var("!YELLOW_PIXEL",&yellow_pixel);
 l_sys_var("!BLACK_PIXEL",&black_pixel);
 l_sys_var("!DARKGREEN_PIXEL",&darkgreen_pixel);
 l_sys_var("!PURPLE_PIXEL",&purple_pixel);
 l_sys_var("!XFONTFLAG",&xfontflag);

 /* end of X stuff */

 /* Motif related variables */
 l_sys_var("!MOTIF",&motif_flag);
 l_sys_var("!MOTIF_ECHO",&motif_echo_flag);
 l_sys_var("!MOTIF_CALLED_WIDGET",&motif_called_widget);
 l_sys_var("$RADIO_BUTTON",&radio_button);
 l_sys_var("$RADIO_STATE",&radio_state);
 l_sys_var("!ZOOMTEST",&zoom_test);
 
 /* socket timeouts */
 l_sys_var("!SOCKET_TIMEOUT",&socketTimeout);
 l_sys_var("!SOCKET_TIMEOUT_USEC",&socketTimeoutUsec);

 /* end of Motif stuff */
 /* Solar B specific */
 l_sys_var("!CCSDSDEBUG",&ccsdsdebug);
 /* end of Solar B stuff */

 l_sys_var("!SPAWN",&spawn_status);
 l_sys_var("!GFIT_FIX_A",&gfit_fix_a);
 l_sys_var("!GFIT_FIX_B",&gfit_fix_b);
 l_sys_var("!GFIT_FIX_X0",&gfit_fix_x0);
 l_sys_var("!GFIT_FIX_DEL",&gfit_fix_del);
 l_sys_var("!GFIT_WARNINGS",&gfit_warnings);
 l_sys_var("!GFIT_SHOW",&gfit_show);
 l_sys_var("!GFIT_CONVERGED",&gfit_converged);
 l_sys_var("!COSINEFIT_FIX_A",&cosinefit_fix_a);
 l_sys_var("!COSINEFIT_FIX_B",&cosinefit_fix_b);
 l_sys_var("!COSINEFIT_FIX_X0",&cosinefit_fix_x0);
 l_sys_var("!COSINEFIT_FIX_DEL",&cosinefit_fix_del);
 l_sys_var("!COSINEFIT_WARNINGS",&cosinefit_warnings);
 l_sys_var("!COSINEFIT_SHOW",&cosinefit_show);
 l_sys_var("!COSINEFIT_sCONVERGED",&cosinefit_converged);
 l_sys_var("!RANDOM_SEED",&seed);
 f_sys_var("!GSMOOTH_WIDTH",&gsmooth_width);
 l_sys_var("!EPSBBX0",&epsbbx0);
 l_sys_var("!EPSBBX1",&epsbbx1);
 l_sys_var("!EPSBBY0",&epsbby0);
 l_sys_var("!EPSBBY1",&epsbby1);
 status_func("!CPUTIME",2);
 status_func("!CTIME",5);
 status_func("!TIME",6);
 status_func("!DATE",7);
 status_func("!SYSTIME",8);
 max_sym = num_internal_syms;
 /*printf("symbol init done, max_sym = %d, max_temp= %d\n",max_sym,max_temp);
 printf("addresses in a symbol structure %d, %d, %d, %d, %d\n",
 &sym[4], &sym[4].spec.scalar, &sym[4].spec.scalar.l, &sym[4].spec.scalar.b,
  &sym[4].spec.scalar.b);
 */
 return 1;
 }
 /*------------------------------------------------------------------------- */
int l_scalar(s,n)	/*used only for initializing internal long scalars */
 char *s;
 int n;
 {
 unsigned int i;
 num_internal_syms +=1;
 i = new_symbol(s);
 sym[i].class=1;	sym[i].type=2;
 sym[i].spec.scalar.l=n;
 return 1;
 }
 /*------------------------------------------------------------------------- */
int l_sys_var(s,n)
 /*used only for initializing long scalars with a pointer to a variable*/
 /*only use for global symbols which can't be passed as arguments in
 subroutines and functions, these are class 8 symbols */
 char *s;
 int *n;
 {
 int i;
 num_internal_syms +=1;
 cur_sym = i = new_symbol(s);
 sym[i].class=8;	sym[i].type=2;
 sym[i].spec.array.ptr = n;
 return 1;
 }
 /*------------------------------------------------------------------------- */
int l_sys_array(s,n)
 /* used only for initializing internal int arrays scalars */
 /* not implemented yet */
 char *s;
 int *n;
 {
 int i;
 num_internal_syms +=1;
 cur_sym = i = new_symbol(s);
 sym[i].class=4;	sym[i].type=2;
 //sym[i].spec.array.ptr = n;
 return 1;
 }
 /*------------------------------------------------------------------------- */
int d_sys_var(s,n)
 /*used only for initializing double scalars with a pointer to a variable*/
 /*only use for global symbols which can't be passed as arguments in
 subroutines and functions, these are class 8 symbols */
 char *s;
 int *n;
 {
 int i;
 num_internal_syms +=1;
 cur_sym = i = new_symbol(s);
 sym[i].class=8;	sym[i].type=4;
 sym[i].spec.array.ptr = n;
 return 1;
 }
 /*------------------------------------------------------------------------- */
int f_sys_var(s,n)
 /*used only for initializing float scalars with a pointer to a variable*/
 /*only use for global symbols which can't be passed as arguments in
 subroutines and functions, these are class 8 symbols */
 char *s;
 int *n;
 {
 int i;
 num_internal_syms +=1;
 cur_sym = i = new_symbol(s);
 sym[i].class=8;	sym[i].type=3;
 sym[i].spec.array.ptr = n;
 return 1;
 }
 /*------------------------------------------------------------------------- */
int f_scalar(s,f)
 /*used only for initializing internal floating scalars */
 char *s;
 double f;
 {
 int i;
 num_internal_syms +=1;
 i = new_symbol(s);
 sym[i].class=1;	sym[i].type=3;
 sym[i].spec.scalar.f=(float) f;
 return 1;
 }
 /*------------------------------------------------------------------------- */
int status_func(s,n)
 /*used only for initializing internal status functions */
 char *s;
 int n;
 {
 int i;
 num_internal_syms +=1;
 i = new_symbol(s);
 sym[i].class=193;	sym[i].type=0;		sym[i].xx=n;
 return 1;
 }
 /*------------------------------------------------------------------------- */
unsigned int new_symbol(char *s)
 /*generate a new named symbol */
 {
 struct sym_table *np;
 unsigned int iq, hv;
 np = (struct sym_table *) malloc(sizeof( *np));	/*space for new link */
 if (!np) return execute_error(23);
 np->name = strsave(s);			/*place for the string */
 hv = hash(np->name);
 np->next = symbols[hv];
 np->context = symbol_context;	/*10/23/91 addition, load context */
 symbols[hv] = np;		/*puts this new one at beginning of chain */
 iq = find_next_sym();
 np->num = iq;
 sym[iq].class = 255;
 sym[iq].xx = hv;			/*load hash value for easier lookbacks */
 return iq;
 }
 /*------------------------------------------------------------------------- */
int ana_malloc_error(mq)
 int  mq;
 {
 /* document a malloc error */
 printf("size attempted = %d  ", mq);
 return execute_error(13);
 }
 /*------------------------------------------------------------------------- */
int ana_assoc_var(narg,ps)		/*set up an associated variable */
 int narg, ps[];
 {
 int	i;				/* symbol # returned and used */
 int	iq, lun, offset;
 size_t	mq;
 struct ahead *h, *h2;
 i=find_next_temp();
 sym[i].class = 6;
 sym[i].xx = -1;
 lun = int_arg( ps[0]);		/* this is lun associated */
 /* the second arg must be an array and we just copy it's particulars */
 iq = ps[1];
 if ( sym[iq].class != 4 ) return execute_error(66);
 mq = sizeof( struct ahead );	/* just header space, no data */
 sym[i].type = sym[iq].type;	/* dup the type */
 if ( (sym[i].spec.array.ptr = (int *) malloc(mq) ) == NULL )
	return ana_malloc_error(mq);
 h = (struct ahead *) sym[i].spec.array.ptr;	/*new one */
 h2 = (struct ahead *) sym[iq].spec.array.ptr;	/*old one */
 bcopy( h2, h, sizeof( struct ahead ) );		/*copy whole header */
 sym[i].spec.array.bstore = mq;
 h->c1 = lun;		/*set lun as c1 char. */
 /* was there an offset? */
 if ( narg > 2) {
  if (int_arg_stat(ps[2], &offset) != 1) return -1;
  if (offset > 0xffff || offset < 0) {
  	printf("ASSOC - offset %d is out of legal range\n", offset);
	return -1;
	}
 h->c2 = (unsigned short) offset;
 } else h->c2 = 0; 
 h->facts = NULL;				/*no known facts */
 return i;					/*return new symbol # */
 }
 /*------------------------------------------------------------------------- */
int get_evb(narg,ntype)			/*set up an evb */
 int narg,ntype;
 /*create an evb executable code symbol, class 200 */
 {
 int i,iq;				/* symbol # returned and used */
 unsigned short	*parg;
 i=find_next_edb();
 sym[i].class=200;	sym[i].type=ntype;	sym[i].xx=narg;
 /*a special case for FOR statements which have 5 arguments */
 if ( ntype == 4 )
	 { sym[i].xx=pop();  narg -=1; } /*sneak first in the narg position*/
 if (narg <= 4 ) for (iq=0;iq<narg;iq++) sym[i].spec.evb.args[iq] = pop();
 else {
 sym[i].spec.array.ptr = (int *) malloc(narg*2);
 if (!sym[i].spec.array.ptr) return execute_error(23);
 parg = (unsigned short *) sym[i].spec.array.ptr;
 for (iq=0;iq<narg;iq++) *parg++ = (unsigned short) pop();
 }
 return i;
 }
 /*------------------------------------------------------------------------- */
int create_edb(class,narg,func_num)	/*set up an edb */
 int class,narg,func_num;
 /*create an expression descriptor code symbol */
 {
 int i,iq;				/* symbol # returned and used */
 unsigned short	*parg;
 i=find_next_edb();
 /*note that narg is stored in type field and the function # in xx field
 for these symbols (in order to use the 16 bit field for the function #) */
 sym[i].class=class;	sym[i].type=narg;	sym[i].xx=func_num;
 if (narg <= 4 ) for (iq=0;iq<narg;iq++) sym[i].spec.evb.args[iq] = (unsigned short) pop();
 else {
 sym[i].spec.array.ptr = (int *) malloc(narg*2);
 if (!sym[i].spec.array.ptr) return execute_error(23);
 parg = (unsigned short *) sym[i].spec.array.ptr;
 for (iq=0;iq<narg;iq++) *parg++ = (unsigned short) pop();
 }
 return i;
 }
 /*------------------------------------------------------------------------- */
int create_subsc_edb(ibeg,iend,ic,it)	/*set up an edb */
 int ibeg,iend,ic,it;
 /*create an expression descriptor code symbol for generalized subscripts */
 /*load items backwards to be consistent with other function types */
 {
 int i,iq;				/* symbol # returned and used */
 i=find_next_edb();
 sym[i].class=193;	sym[i].type=4;	sym[i].xx=4;
 sym[i].spec.evb.args[3]=ibeg;
 sym[i].spec.evb.args[2]=iend;
 sym[i].spec.evb.args[1]=ic;
 sym[i].spec.evb.args[0]=it;
 return i;
 }
 /*------------------------------------------------------------------------- */
int create_fixed(x,ntype)	/*set up a fixed value scalar sym */
 int ntype;
 union scalar *x;
 {
 int i;					/* symbol # returned and used */
 i=find_next_edb();
 sym[i].class=1;	sym[i].type=ntype;	sym[i].xx= -1;
 /* note, no hash code for these because no associated symbol string */
 switch (ntype) {
 case 0: sym[i].spec.scalar.b=x->b; break;
 case 1: sym[i].spec.scalar.w=x->w; break;
 case 2: sym[i].spec.scalar.l=x->l; break;
 case 3: sym[i].spec.scalar.f=x->f; break;
 case 4: sym[i].spec.scalar.d=x->d; break;
 }
 return i;
 }
 /*------------------------------------------------------------------------- */
int create_sub_ptr(nsym,ptr,j)
 /*a subscript pointer, nsym is array (or whatever), ptr is a pointer, often
 to beginning of array, and j is an index offset from ptr */
 /* try making these class 8 symbols 10/31/91 (boo!) */
 int nsym, j;
 char	*ptr;
 {
 int i;					/* symbol # returned and used */
 i = cur_sym = max_sp;
 if (max_sp++ >= TEMP_BASE) return execute_error(33);
 sym[i].class=8;	sym[i].type=sym[nsym].type;	sym[i].xx= -1;
 /* note, no hash code for these because no associated symbol string */
 switch (sym[i].type) {
 case 0: sym[i].spec.array.ptr=  (int *)((byte *)ptr + j);	break;
 case 1: sym[i].spec.array.ptr=  (int *)((short *)ptr + j);	break;
 case 2: sym[i].spec.array.ptr=  (int *)ptr + j;		break;
 case 3: sym[i].spec.array.ptr=  (int *)((float *)ptr + j);	break;
 case 4: sym[i].spec.array.ptr=  (int *)((double *)ptr + j);	break;
 }
 return i;
 }
 /*------------------------------------------------------------------------- */
int create_str_sub_ptr(nsym,ptr,j)
 /*a subscript pointer for strings */
 int nsym, j;
 char	*ptr;
 {
 int i;					/* symbol # returned and used */
 i = cur_sym = max_sp;
 if (max_sp++ >= TEMP_BASE) return execute_error(33);
 sym[i].class=2;	sym[i].type=sym[nsym].type;	sym[i].xx= -1;
 /* note, no hash code for these because no associated symbol string */
 sym[i].spec.array.ptr=  (int *)(ptr + j);
 /* because strings use bstore to determine lengths, this must be set,
 make sure that free_subs never tries to free this address ! */
 sym[i].spec.array.bstore = 2;		/* but not null terminated */
 return i;
 }
 /*------------------------------------------------------------------------- */
int scalar_scratch(ntype)	/*create a temporary scalar */
 int ntype;
 {
 int i;					/* symbol # returned and used */
 i=find_next_temp();
 sym[i].class=1;	sym[i].type=ntype;	sym[i].xx= -1;
 /* note, no hash code for these because no associated symbol string */
 return i;
 }
 /*------------------------------------------------------------------------- */
int scalar_scratch_copy(nsym)	/*create a temporary scalar which is a
				copy of nsym (with value) */
 int nsym;
 {
 int i;					/* symbol # returned and used */
 i=find_next_temp();
 if ( sym[nsym].class != 1) return execute_error(40);
 sym[i].class=1;	sym[i].type=sym[nsym].type;	sym[i].xx= -1;
 /* note, no hash code for these because no associated symbol string */
 switch (sym[i].type) {
 case 0: sym[i].spec.scalar.b= sym[nsym].spec.scalar.b; break;
 case 1: sym[i].spec.scalar.w= sym[nsym].spec.scalar.w; break;
 case 2: sym[i].spec.scalar.l= sym[nsym].spec.scalar.l; break;
 case 3: sym[i].spec.scalar.f= sym[nsym].spec.scalar.f; break;
 case 4: sym[i].spec.scalar.d= sym[nsym].spec.scalar.d; break;
 }
 return i;
 }
 /*------------------------------------------------------------------------- */
int string_scratch(n)		/*create a temporary string */
 /* strings are null terminated and counted, n is the size of string */
 int	n;
 {
 int i;				/* symbol # returned and used */
 size_t mq;
 i=find_next_temp();
 sym[i].class=2;	sym[i].type=14;	sym[i].xx= -1;
 /* note, no hash code for these because no associated symbol string */
 mq = n+1;	/*extra byte for the null */
 sym[i].spec.array.bstore = mq;
 sym[i].spec.array.ptr = (int *) malloc(mq);
 if (!sym[i].spec.array.ptr) return execute_error(23);
 return i;
 }
 /*------------------------------------------------------------------------- */
int fixed_string(p,n)		/*create a fixed string */
 /* strings are null terminated and counted, n is the size of string */
 int	n;
 byte	*p; /*null terminated string pointer */
 {
 int i;
 size_t mq;
 byte	*pp,*nn;
 i=find_next_edb();
 sym[i].class=2;	sym[i].type=15;	sym[i].xx= -1;
 /* use type 15 to differentiate fixed strings from others (type 14) */
 /* note, no hash code for these because no associated symbol string */
 mq = n+1;	/*extra byte for the null */
 sym[i].spec.array.bstore = mq;
 sym[i].spec.array.ptr = (int *) malloc(mq);
 if (!sym[i].spec.array.ptr) return execute_error(23);
 nn = pp = (byte *) sym[i].spec.array.ptr;
 /*copy string in p to symbol space, check count */
 while (*pp++ = *p++ ) ;
 if ( (pp-nn) != mq ) return execute_error(25);
 return i;
 }
 /*------------------------------------------------------------------------- */
int string_clone(nsym)
 /*create a temporary string of same size as the one in nsym, don't load
 contents, not sure how useful this routine is, used by zero function */
 /* strings are null terminated and counted, n is the size of string */
 int	nsym;
 {
 int i;				/* symbol # returned and used */
 size_t mq;
 if ( sym[nsym].class != 2 ) return execute_error(21);
 i=find_next_temp();
 sym[i].class=2;	sym[i].type=14;	sym[i].xx= -1;
 /* note, no hash code for these because no associated symbol string */
 mq = sym[nsym].spec.array.bstore;
 sym[i].spec.array.bstore = mq;
 sym[i].spec.array.ptr = (int *) malloc(mq);
 if (!sym[i].spec.array.ptr) return execute_error(23);
 return i;
 }
 /*------------------------------------------------------------------------- */
int find_next_sym()
 /* 10/9/91 modified to support main and subr class symbols */
 {
 struct	sym_list	*ns;
 /*first check if a subr (or func or prog) */
 if (symbol_context) {					/* subr case */
 cur_sym = max_user_sym;
 if (max_user_sym++ >= SUBR_TOP) parser_error(17);
 /* add to the linked list for this context */
 ns=(struct sym_list *) malloc(sizeof( *ns));	/*space for new link */
 if (!ns) return execute_error(23);
 ns->num = cur_sym;
 ns->next=subr_sym_list[symbol_context];
 subr_sym_list[symbol_context] = ns;		/*puts this new one at
						  beginning of chain */
 } else {						/*main level case */
 cur_sym = max_sym;
 if (max_sym++ >= SYM_TOP)  parser_error(18);
 }
 return cur_sym;
 }
 /*------------------------------------------------------------------------- */
int find_next_edb()
 /* 10/9/91 modified to support main and subr class EDB symbols */
 {
 static	int	last = EDB2_BASE;
 struct	sym_list	*ns;
 int	n;
 /*first check if a subr (or func or prog) */
 if (edb_context) {					/* subr case */
 /* note, edb_context = symbol_context for subr and funcs but for
	 code blocks the two will differ */
 /* 11/2/92, ras, this didn't look right, changed to what I think I meant */
 /*cur_sym=max_edb2_sym;*/
 if (max_edb2_sym < EDB2_TOP)
 {cur_sym =  max_edb2_sym++; /*still have virgin space */
 } else {  /*run to end of this symbol space, but there may be zapped ones
	   somewhere, look starting at last one found */
 n = N_EDB2;
 while (n--)	{ cur_sym = last++;		/*work through list */
  if ( sym[cur_sym].class == 0 ) break;		/*0 class means OK to re-use*/
  if (last >= EDB2_TOP) last = EDB2_BASE;	/*over the top, back to base*/
 }
 if ( n <= 0 ) parser_error(19);		/*if through the whole table*/
					 	/*then we have truly run out */
 }
 /* add to the linked list for this context */
 ns=(struct sym_list *) malloc(sizeof( *ns));	/*space for new link */
 if (!ns) return execute_error(23);
 ns->num = cur_sym;
 ns->next=subr_sym_list[edb_context];
 subr_sym_list[edb_context] = ns;		/*puts this new one at
						  beginning of chain */
 } else {						/*main level case */
 /*printf("getting a new edb, max_edb_sym = %d\n", max_edb_sym);*/
 cur_sym=max_edb_sym;
 if (max_edb_sym++ >= EDB_TOP)  parser_error(20);
 }
 return cur_sym;
 }
 /*------------------------------------------------------------------------- */
int find_next_temp()
{
 cur_sym=max_temp;
 if (max_temp++ >= MAX_TEMP) return execute_error(34);
 return cur_sym;
}
 /*-------------------------------------------------------------------------*/
int hash(s)			/*generates a hash value from a string */
 char *s;
{ int iq;
 for (iq=0; *s != '\0';) iq += *s++; return(iq % HASHSIZE);
}
 /*------------------------------------------------------------------------- */
char *strsave(s)		/*save a copy of a string */
 char *s;
 {
 char *p;
 p = (char *) malloc( (size_t) strlen(s)+1);
 if (p == NULL)	{ execute_error(23); return NULL; }
 strcpy(p,s);
 return(p);
 }
 /*------------------------------------------------------------------------- */
char *strsavsd(sd)		/*save a copy of a string in a sd */
 struct	sdesc	*sd;
 {
 char *p;
 if ( (p = (char *) malloc( (sd->n) + 1)) == NULL) { execute_error(23);
    return NULL; }
 strncpy(p, (char *)sd->p,sd->n);
 p[sd->n] = '\0';
 return(p);
 }
 /*------------------------------------------------------------------------- */
int find_sym(s)			/*looks for symbol in table symbols */
				/*returns sym #, if not found, a new symbol
				is created with class undefined */
 char *s;			/* input is a null terminated string */
 {
 struct sym_table *np;
 int iq, hv, local_context, save;
 char *strsave(), c;
 /* 1/8/92 if a global, ensure top level context */
 local_context = symbol_context;
 c = *s;  if (c=='$' || c=='!' || c=='#') local_context = 0;
 for (np=symbols[hash(s)]; np != NULL; np = np->next)
 /* 10/23/91, also need to check context against current context */
 
   if (strcmp(s,np->name) == 0 && np->context == local_context)
				 return(np->num);	/*found it */
							 /*not found */
 /* 1/8/92 don't allow new symbols beginning with a ! */
 if (c == '!') { printf("symbol name %s\n",s);  parser_error(44);}
 np=(struct sym_table *) malloc(sizeof( *np));	/*space for new link */
 if (!np) return execute_error(23);
 np->name = strsave(s);			/*place for the string */
 np->context = local_context;	/*10/23/91 addition, load context */
 hv = hash(np->name);
 np->next=symbols[hv];
 symbols[hv] = np;		/*puts this new one at beginning of chain */
 save = symbol_context;
 symbol_context = local_context;
 iq = find_next_sym();
 symbol_context = save;
 np->num = iq;
			 /*make this new symbol undefined, class 255*/
			 /*note hash value in xx field for back traces */
 sym[iq].class=255;	sym[iq].type=255;	sym[iq].xx=hv;
 /* printf("new sym # %d\n",iq); */
 return(iq);
 }
 /*------------------------------------------------------------------------- */
int lookfor_sym(s)		/*looks for symbol in table symbols */
				/*returns sym #, if not found, returns -1
				really a cut back find_sym for special
				cases where we need to know if a symbol
				is in the table before next action */
 char *s;			/* input is a null terminated string */
 {
 struct sym_table *np;
 for (np=symbols[hash(s)]; np != NULL; np = np->next)
   if (strcmp(s,np->name) == 0 && np->context == symbol_context)
				 return(np->num);	/*found it */
 return -1;						/*not found */
 }
 /*------------------------------------------------------------------------- */
int get_context(n)
 /* get the context for a symbol */
 int	n;
 {
 struct sym_table *np;
 int iq,hv;
 hv = sym[n].xx;
 /*check if a legal hash value */
 if ( hv < 0 || hv >= HASHSIZE ) return -1;
 for (np=symbols[hv]; np != NULL; np = np->next)
         if (np->num == n) return(np->context);    /*found it */
 return -1;					/* not found, return -1 */
 }
 /*------------------------------------------------------------------------- */
char *find_sym_name(n)
 /*get the name (i.e, in the form of a string) of a named symbol given the
 symbol # */
 int	n;
 {
 struct sym_table *np;
 int iq,hv;
 hv = sym[n].xx;
 /*check if a legal hash value */
 if ( hv < 0 || hv >= HASHSIZE ) return ("..no name..");
 for (np=symbols[hv]; np != NULL; np = np->next)
	 if (np->num == n) return(np->name);	/*found it */
 return ("..no name..");					/*not found */
 }
 /*------------------------------------------------------------------------- */
int clear_all()			/*used in nextl */
{
 clear_temps();	clear_sps();	clear_edb();
 return 1;
}
 /*------------------------------------------------------------------------- */
int clear_temps()		/*clear any temps used */
{
 int i;
 /*start at bottom of temp symbols section and clear anything used */
 /*printf("clear_temps, max_temp = %d\n", max_temp);*/
 for (i=TEMP_BASE; i < max_temp; i++) {
	 /*printf("i = %d, class = %d\n",i,sym[i].class );*/
	 if (sym[i].class != 0) { delete_symbol(i); sym[i].class = 0; } }
 max_temp=TEMP_BASE;
 return 1;
 }
 /*------------------------------------------------------------------------- */
int clear_sps()			/*clear any sps's used */
{
 int i;
 /* these never allocate extra space, so just need to reset pointer */
 max_sp=SPS_BASE;
 return 1;
 }
 /*------------------------------------------------------------------------- */
int clear_edb()			/*clear any top level edb's used */
{
 int i;
 /*start at bottom of edb section and clear anything used */
 for (i=EDB_BASE; i < max_edb_sym; i++) {
	 if (sym[i].class != 0) { delete_symbol(i); sym[i].class = 0; }} 
 max_edb_sym=EDB_BASE;
 return 1;
 }
 /*------------------------------------------------------------------------- */
int array_scratch(int ntype, int nd, int *pd)	/*create a temporary array */
{
 int i, j, n;
 size_t mq;
 struct ahead *h;
 if (nd > 8 ) return execute_error(11);
 i=find_next_temp();
 sym[i].class=4;	sym[i].type=ntype;	sym[i].xx= -1;
 /* note, no hash code for these because no associated symbol string */
 mq = ana_type_size[ntype];
 for (j=0;j<nd;j++) {
   n = *(pd+j);
   if (n < 1) return execute_error(65);
   mq *= n;
 }
 mq += sizeof( struct ahead );	/*total memory required including header */
 sym[i].spec.array.bstore = mq;
 
 if ( (sym[i].spec.array.ptr = (int *) malloc(mq) ) == NULL )
        return  ana_malloc_error(mq);
 h = (struct ahead *) sym[i].spec.array.ptr;
 h->ndim = nd;
 h->c1 = 0; h->c2 = 0;
 for (j=0;j<nd;j++)
  { h->dims[j] = *(pd+j); }
 h->facts = NULL;			/*no known facts */
 return i;
 }
 /*------------------------------------------------------------------------- */
int strarr_scratch(int nd, int *pd)	/*create a temporary string array */
{
 int i, j;
 size_t mq, nelem;
 char	**p;
 struct ahead *h;

 nelem = 1;
 for (j=0;j<nd;j++)  nelem *= *(pd+j);
 if (nelem <= 0) { printf("bad size passed to strarr_scratch\n"); return -1; }
 i=find_next_temp();
 sym[i].class = STRING_PTR;  sym[i].type=TEMP_STRING;  sym[i].xx= -1;
 /* note, no hash code for these because no associated symbol string */
 mq = sizeof ( char * );
 mq = mq * nelem;
 mq += sizeof( struct ahead );	/*total memory required including header */
 sym[i].spec.array.bstore = mq;
 if ( (sym[i].spec.array.ptr = (int *) malloc(mq) ) == NULL )
	 return ana_malloc_error(mq);
 h = (struct ahead *) sym[i].spec.array.ptr;
 h->ndim = nd;
 h->c1 = 0; h->c2 = 0;
 for (j=0;j<nd;j++)
  { h->dims[j] = *(pd+j); if (h->dims[j] < 1) return execute_error(65); }
 h->facts = NULL;			/*no known facts */
 /* this differs from arrays since we must load the pointers with NULLS */
 p = (char **) ((char *)h + sizeof(struct ahead));
 /* NULL these */
 while (nelem--) *p++ = NULL;
 return i;
 }
 /*------------------------------------------------------------------------- */
int symarr_create(int i, int nd, int *pd)	/* create a symbol array using i */
 {
 int j;
 size_t mq, nelem;
 struct sym_desc *p;
 struct ahead *h;
 nelem = 1;
 for (j=0;j<nd;j++)  nelem *= *(pd+j);
 if (nelem <= 0) { printf("bad size passed to symarr_create\n"); return -1; }
 sym[i].class = SYM_ARR;  sym[i].type=MIXED_TYPES;
 mq = sizeof (struct sym_desc);
 mq = mq * nelem;
 mq += sizeof(struct ahead);	/*total memory required including header */
 sym[i].spec.array.bstore = mq;
 if ( (sym[i].spec.array.ptr = (int *) malloc(mq) ) == NULL )
	 return ana_malloc_error(mq);
 h = (struct ahead *) sym[i].spec.array.ptr;
 h->ndim = nd;
 h->c1 = 0; h->c2 = 0;
 for (j=0;j<nd;j++)
  { h->dims[j] = *(pd+j); if (h->dims[j] < 1) return execute_error(65); }
 h->facts = NULL;			/*no known facts */
 /* load each symbol descriptor with an undefined */
 p = (struct sym_desc *) ((char *)h + sizeof(struct ahead));
 /* undefined with NULL pointers */
 while (nelem--) {
 	p->class = 0;
 	p->spec.array.ptr = NULL;
 	p->spec.array.bstore = 0;
 	p++;
  }
 return 1;
 }
 /*------------------------------------------------------------------------- */
int symarr_scratch(int nd, int *pd)	/* create a temporary symbol array */
 {
 int i;
 i=find_next_temp();
 if (symarr_create(i,nd, pd) != 1) return -1;
 sym[i].xx= -1;
 return i;
 }
 /*------------------------------------------------------------------------- */
int notallowed(int i)
 {
 if ( i < num_internal_syms) {
	  printf("symbol # and name:%d %s\n", i, find_sym_name(i) );
	  return 1;
 } else return 0;
 }
 /*------------------------------------------------------------------------- */
int redef_symarr(int i, int nd, int *pd)
 /* redefine a symbol i to a symbol array */
 {
 if (notallowed(i)) return execute_error(116);
 /* for symbol arrays, we presently just delete the old symbol since the
 odds are we will have to reconstruct the memory allocation anyhow, but
 the approach of deleting the old symbol before making a scratch symbol array
 is used to ensure that we free memory before alocating more */
 if ( delete_symbol(i) != 1 ) return execute_error(17);
 return symarr_create(i,nd, pd);
 }
/*------------------------------------------------------------------------- */
int redef_array(i, ntype, nd, pd)
 /* redefine a symbol i to an array */
 int	i, ntype, nd, *pd;
  {
  int	j, *pq;
  size_t mq;
  struct	ahead	*h;
  /* if a class 5 we probably really want the target */
  while (sym[i].class == 5) { i = (int) sym[i].spec.evb.args[0]; }
  /* are we allowed to change this symbol ? */
  if (notallowed(i)) return execute_error(116);
  if (nd > 8 ) return execute_error(11);
  mq = ana_type_size[ntype]; for (j=0;j<nd;j++) mq *= *(pd+j);
  mq += sizeof( struct ahead );	/*total memory required including header */
  /* before deleting, check the current size and use it if it matches,
  this avoids a lot of mallocing in loops */
  if (sym[i].class != 4 || mq != sym[i].spec.array.bstore) {
  if ( delete_symbol(i) != 1 ) return execute_error(17);
  sym[i].spec.array.bstore = mq;
  if ( (sym[i].spec.array.ptr = (int *) malloc(mq) ) == NULL )
	  return ana_malloc_error(mq);
  }
  sym[i].class=4;	sym[i].type=ntype;
  h = (struct ahead *) sym[i].spec.array.ptr;
  h->ndim = nd;
  h->c1 = 0; h->c2 = 0;
  for (j=0;j<nd;j++) h->dims[j] = *(pd+j);
  h->facts = NULL;			/*no known facts */
  return 1;
  }
 /*------------------------------------------------------------------------- */
int redef_array_ptr(i, ntype, nd, pd, ptr)
 /* redefine a symbol i to an array when we already have the data pointed
 to by ptr, we have to free any previous contents in this case */
 int	i, ntype, nd, *pd;
  {
  int	j, *pq;
  size_t mq;
  struct	ahead	*h;
  /* if a class 5 we probably really want the target */
  while (sym[i].class == 5) { i = (int) sym[i].spec.evb.args[0]; }
  /* are we allowed to change this symbol ? */
  if (notallowed(i)) return execute_error(116);
  if (nd > 8 ) return execute_error(11);
  mq = ana_type_size[ntype]; for (j=0;j<nd;j++) mq *= *(pd+j);
  mq += sizeof( struct ahead );	/*total memory required including header */
  /* before deleting, check the current size and use it if it matches,
  this avoids a lot of mallocing in loops */
  if (sym[i].class != 4 || mq != sym[i].spec.array.bstore) {
  if ( delete_symbol(i) != 1 ) return execute_error(17);
  sym[i].spec.array.bstore = mq;
  if ( (sym[i].spec.array.ptr = (int *) malloc(mq) ) == NULL )
	  return ana_malloc_error(mq);
  }
  sym[i].class=4;	sym[i].type=ntype;
  h = (struct ahead *) sym[i].spec.array.ptr;
  h->ndim = nd;
  h->c1 = 0; h->c2 = 0;
  for (j=0;j<nd;j++) h->dims[j] = *(pd+j);
  h->facts = NULL;			/*no known facts */
  return 1;
  }
 /*------------------------------------------------------------------------- */
int ana_deletesymbol(narg,ps)
 /* delete symbol(s) in list */
 int narg, ps[];
 {
 int iq, i, z=0;
 char *p;
 for (i=0;i<narg;i++) {
   iq = ps[i];
   /* check if a protected symbol */
   if (notallowed(i)) {
     return execute_error(116);
     /* note that if there is an illegal, we still do the remaining ones in the list */
   } else {
     if (sym[iq].class != 0) { delete_symbol(iq); sym[iq].class = 0; } 
   }
 }
 return 1;
 }
 /*------------------------------------------------------------------------- */
int extend_array(i, nd, pd)
 /* extend an array */
 int	i, nd, *pd;
  {
  /* extend an existing array using remalloc, original contents are
  supposed to remain. For internal use. */
  int	j, *pq, ntype, nd_old;
  size_t mq;
  struct	ahead	*h;
  int	*ptr;
  /* if a class 5 we probably really want the target */
  while (sym[i].class == 5) { i = (int) sym[i].spec.evb.args[0]; }
  /* are we allowed to change this symbol ? */
  if (notallowed(i)) return execute_error(116);
  if (nd > 8 ) return execute_error(11);
  ntype = sym[i].type;		/* we don't change the type of course */
  if (sym[i].class != 4) {
  	return execute_error(132); }
  /* the inner dimensions must be the same, only the last can change */
  h = (struct ahead *) sym[i].spec.array.ptr;
  nd_old = h->ndim;
  if (nd > (nd_old+1) ) return execute_error(132);
  nd_old = MAX(nd, nd_old) - 1;
  for (j=0;j<nd_old;j++) if ( h->dims[j] != pd[j]) return execute_error(132);
  h->ndim = nd;
  h->dims[nd-1] = pd[nd-1];
  mq = ana_type_size[ntype]; for (j=0;j<nd;j++) mq *= *(pd+j);
  mq += sizeof( struct ahead );	/*total memory required including header */
  sym[i].spec.array.bstore = mq;
  ptr = sym[i].spec.array.ptr;
  if ( (sym[i].spec.array.ptr = (int *) realloc(ptr, mq) ) == NULL )
	  return ana_malloc_error(mq);
  return 1;
  }
/*------------------------------------------------------------------------- */
int redef_strarr(i, nd, pd)	/*redefine a string array */
 int i, nd, *pd;
{
 int	j, nelem;
 size_t mq;
 char	**p;
 struct ahead *h;

 /* are we allowed to change this symbol ? */
 if (notallowed(i)) return execute_error(116);

 nelem = 1;
 for (j=0;j<nd;j++)  nelem *= *(pd+j);
 if (nelem <= 0) { printf("bad size passed to strarr_scratch\n"); return -1; }

 mq = sizeof ( char * );
 mq = mq * nelem;
 mq += sizeof( struct ahead );	/*total memory required including header */
 /* before deleting, check the current size and use it if it matches,
 this avoids a lot of mallocing in loops */
 if (sym[i].class != STRING_PTR || mq != sym[i].spec.array.bstore) {
 if ( delete_symbol(i) != 1 ) return execute_error(17);
 sym[i].spec.array.bstore = mq;
 if ( (sym[i].spec.array.ptr = (int *) malloc(mq) ) == NULL )
	  return ana_malloc_error(mq);
 }

 sym[i].class = STRING_PTR;  sym[i].type=TEMP_STRING;
 h = (struct ahead *) sym[i].spec.array.ptr;
 h->ndim = nd;
 h->c1 = 0; h->c2 = 0;
 for (j=0;j<nd;j++)
  { h->dims[j] = *(pd+j); if (h->dims[j] < 1) return execute_error(65); }
 h->facts = NULL;			/*no known facts */
 /* this differs from arrays since we must load the pointers with NULLS */
 p = (char **) ((char *)h + sizeof(struct ahead));
 /* NULL these */
 while (nelem--) *p++ = NULL;
 return 1;
 }
 /*------------------------------------------------------------------------- */
int array_clone(nsym,ntype)
 /* another temporary array creator, this makes an array of the same structure
 as the one in nsym but with the specified type, otherwise much like
 array_scratch, contents are not copied */
 int	nsym, ntype;		/*input array to mimic */
{
 int	nd, *pd;
 int i, j;
 size_t mq;
 struct ahead *h, *h2;
 /*some checks on what we are cloning */
 if ( sym[nsym].class != 4 ) return execute_error(19);
 mq = sym[nsym].spec.array.bstore - sizeof( struct ahead );
 if ( mq <= 0 ) return execute_error(20);
 mq = (mq / ana_type_size[sym[nsym].type]) * ana_type_size[ntype];
 mq += sizeof( struct ahead );
 i=find_next_temp();
 sym[i].class=4;	sym[i].type=ntype;	sym[i].xx= -1;
 /* note, no hash code for these because no associated symbol string */
 if ( (sym[i].spec.array.ptr = (int *) malloc(mq) ) == NULL )
   {
    printf("array_clone malloc, i, mq = %d, %d\n", i, mq);
    /* try again */
    if ( (sym[i].spec.array.ptr = (int *) malloc(mq) ) == NULL )
    return ana_malloc_error(mq);
   }
 h = (struct ahead *) sym[i].spec.array.ptr;	/*new one */
 h2 = (struct ahead *) sym[nsym].spec.array.ptr;	/*old one */
 bcopy( h2, h, sizeof( struct ahead ) );		/*copy whole header */
 sym[i].spec.array.bstore = mq;
 h->c1 = 0; h->c2 = 0;			/*then zero some fields */
 h->facts = NULL;				/*no known facts */
 return i;					/*return new symbol # */
 }
 /*------------------------------------------------------------------------- */
int symbol_clone_via_ptr(p, q, mode)
 struct sym_desc *p, *q;
 /* presently used only for symbol array operations */
 {
 size_t mq;
 /* mode is 1 if desc block contents must also be copied, this is already
 done for some cases */
 if (mode) {
   bcopy(q, p, sizeof(struct sym_desc));
 }
 switch (q->class) {
  case SYM_ARR:				/*symbol array */
   printf("internal error - symbol array cannot be part of another symbol array\n");
   return -1;
  /* only need to consider symbol classes with pointers */
  case STRING:				/*string */
  case COMPILE_STR:			/*compile string */
  case ARRAY:				/*array */
  case ASSOC:				/*assoc variable */
  case SYM_PTR:				/*symbol pointers */
  case STRING_PTR:			/*string pointer */
   /* in case something goes wrong, null the new bstore and ptr */
   p->spec.array.bstore = 0;
   p->spec.array.ptr = NULL;
   /* check both pointer and bstore */
   if (q->spec.array.ptr && q->spec.array.bstore) {
     mq = p->spec.array.bstore = q->spec.array.bstore;
     if ( (p->spec.array.ptr = (int *) malloc(mq) ) == NULL )
       return execute_error(13);
     bcopy( (char *)q->spec.array.ptr, (char *)p->spec.array.ptr, mq);
   }
   /* for the string arrays (STRING_PTR), also need to copy all the
   strings pointed to */
   if (q->class == STRING_PTR) {
    int	n, nd, j;
    char	**p2, **q2;
    struct	ahead	*h;
    h = (struct ahead *) ((char *) q->spec.array.ptr);
    nd = h->ndim;
    n = 1; for (j=0;j<nd;j++) n *= h->dims[j];
    //printf("nd, n = %d, %d\n", nd, n);
    q2 = (char **) ((char *) h + sizeof(struct ahead));
    /* now the destination */
    p2 = (char **) ((char *) q->spec.array.ptr + sizeof(struct ahead));
    while (n--) { if (*q2) *p2 = strsave(*q2);  p2++;  q2++; }
   }

 }
 return 1;
 }
 /*------------------------------------------------------------------------- */
int ana_array(narg,ps)
 /*create an array of specified type */
 int narg, ps[];
 {
 int	pd[8], type;
 if (int_arg_stat(ps[0], &type) != 1) return -1;
 if (type < 0 || type >4) return execute_error(32);
 narg--;
 if ( get_dims(&narg,&ps[1],pd) != 1) return -1;
 return	array_scratch(type,narg,pd);
 }
 /*------------------------------------------------------------------------- */
int strarr(narg,ps)
 /*create a string array, string pointers */
 int narg, ps[];
 {
 int	pd[8], nd, j, i, nelem, mq;
 struct ahead *h;
 if ( get_dims(&narg,ps,pd) != 1) return -1;
 /* upgraded to multiple dimensions */
 return	strarr_scratch(narg,pd);
 }
 /*------------------------------------------------------------------------- */
int symarr(narg,ps)
 /*create a symbol array */
 int narg, ps[];
 {
 int	pd[8], nd, j, i, nelem, mq;
 struct ahead *h;
 if ( get_dims(&narg,ps,pd) != 1) return -1;
 return	symarr_scratch(narg,pd);
 }
 /*------------------------------------------------------------------------- */
int intarr(narg,ps)
 /*create an array of I*2 elements */
 int narg, ps[];
 {
 int	pd[8];
 if ( get_dims(&narg,ps,pd) != 1) return -1;
 return	array_scratch(1,narg,pd);
 }
 /*------------------------------------------------------------------------- */
int get_dims(narg,ps,pd)
 /*get the dimensions for intarr and friends */
 int *narg, ps[], pd[];
 {
 int	j, iq, n, *p, nd, nelem;
 struct	ahead	*h;
 n = *narg;
 for (j=0;j<n;j++) {
 iq = ps[j];
 if ( sym[iq].class == 4) {	/* if an array, only 1 allowed */
   if (n != 1) { printf("only one arg. allowed if an array is used\n");
		 return -1; }
 /* convert to long array and load into pd */
 iq = ana_long(1, &iq);
 h = (struct ahead *) sym[iq].spec.array.ptr;
 nd = h->ndim;
 nelem = 1; for (j=0;j<nd;j++) nelem *= h->dims[j];
 if (nelem > 8) return execute_error(11);
 n = *narg = nelem;
 p = (int *) ((char *) h + sizeof( struct ahead ));
 for (j=0;j<n;j++)  *pd++ = *p++;
 return 1;
 } else pd[j] = int_arg( iq );
 }
 return 1;
 }
 /*------------------------------------------------------------------------- */
int bytarr(narg,ps)
 /*create an array of I*1 elements */
 int narg, ps[];
{
 int	pd[8], i,j;
 if ( get_dims(&narg,ps,pd) != 1) return -1;
 return	array_scratch(0,narg,pd);
}
 /*------------------------------------------------------------------------- */
int lonarr(narg,ps)
 /*create an array of I*4 elements */
 int narg, ps[];
 {
 int	pd[8], i,j;
 if ( get_dims(&narg,ps,pd) != 1) return -1;
 return	array_scratch(2,narg,pd);
 }
 /*------------------------------------------------------------------------- */
int fltarr(narg,ps)
 /*create an array of F*4 elements */
 int narg, ps[];
 {
 int	pd[8], i,j;
 for (j=0;j<narg;j++)
 pd[j] = int_arg( ps[j] );	/*get the dimensions */
 return	array_scratch(3,narg,pd);
 }
 /*------------------------------------------------------------------------- */
int dblarr(narg,ps)
 /*create an array of F*8 elements */
 int narg, ps[];
 {
 int	pd[8], i,j;
 if ( get_dims(&narg,ps,pd) != 1) return -1;
 return	array_scratch(4,narg,pd);
 }
 /*------------------------------------------------------------------------- */
int int_arg(nsym)
 /*returns the integer value of a scalar symbol with # nsym, if nsym is not
 a scalar, gens an error */
 int nsym;
 {
 if ( sym[nsym].class == 8) nsym = class8_to_1(nsym);	/*scalar ptr case */
 if ( sym[nsym].class != 1) return execute_error(10);
 /*convert value to int */
	 switch (sym[nsym].type) {
	 case 0: return (int) sym[nsym].spec.scalar.b;
	 case 1: return (int) sym[nsym].spec.scalar.w;
	 case 2: return sym[nsym].spec.scalar.l;
	 case 3: return (int) sym[nsym].spec.scalar.f;
	 case 4: return (int) sym[nsym].spec.scalar.d;
	 default: return execute_error(46);
 }
 }
 /*------------------------------------------------------------------------- */
int int_arg_stat(nsym, value)
 /*returns the integer value of a scalar symbol with # nsym, if nsym is not
 a scalar, gens an error */
 int nsym, *value;
 {
 if ( sym[nsym].class == 8) nsym = class8_to_1(nsym);	/*scalar ptr case */
 if ( sym[nsym].class != 1) return execute_error(10);
 /*convert value to int */
	 switch (sym[nsym].type) {
	 case 0: *value = (int) sym[nsym].spec.scalar.b; return 1;
	 case 1: *value = (int) sym[nsym].spec.scalar.w; return 1;
	 case 2: *value = sym[nsym].spec.scalar.l; return 1;
	 case 3: *value = (int) sym[nsym].spec.scalar.f; return 1;
	 case 4: *value = (int) sym[nsym].spec.scalar.d; return 1;
	 default: return execute_error(46);
 }
 }
 /*------------------------------------------------------------------------- */
float float_arg(nsym)
 /*returns the float value of a scalar symbol with # nsym, if nsym is not
 a scalar, gens an error */
 int nsym;
 {
 if ( sym[nsym].class == 8) nsym = class8_to_1(nsym);	/*scalar ptr case */
 if ( sym[nsym].class != 1) return execute_error(10);
 /*convert value to float */
	 switch (sym[nsym].type) {
	 case 0: return (float) sym[nsym].spec.scalar.b;
	 case 1: return (float) sym[nsym].spec.scalar.w;
	 case 2: return (float) sym[nsym].spec.scalar.l;
	 case 3: return sym[nsym].spec.scalar.f;
	 case 4: return (float) sym[nsym].spec.scalar.d;
	 default: return execute_error(46);
 }
 }
 /*------------------------------------------------------------------------- */
int float_arg_stat(nsym, value)
 /*returns the float value of a scalar symbol with # nsym, if nsym is not
 a scalar, gens an error */
 int nsym;
 float *value;
 {
 if ( sym[nsym].class == 8) nsym = class8_to_1(nsym);	/*scalar ptr case */
 if ( sym[nsym].class != 1) return execute_error(10);
 /*convert value to int */
	 switch (sym[nsym].type) {
	 case 0: *value = (float) sym[nsym].spec.scalar.b; return 1;
	 case 1: *value = (float) sym[nsym].spec.scalar.w; return 1;
	 case 2: *value = (float) sym[nsym].spec.scalar.l; return 1;
	 case 3: *value = sym[nsym].spec.scalar.f; return 1;
	 case 4: *value = (float) sym[nsym].spec.scalar.d; return 1;
	 default: return execute_error(46);
 }
 }
 /*------------------------------------------------------------------------- */
int double_arg_stat(nsym, value)
 /*returns the double value of a scalar symbol with # nsym, if nsym is not
 a scalar, gens an error */
 int nsym;
 double *value;
 {
 if ( sym[nsym].class == 8) nsym = class8_to_1(nsym);	/*scalar ptr case */
 if ( sym[nsym].class != 1) return execute_error(10);
 /*convert value to int */
	 switch (sym[nsym].type) {
	 case 0: *value = (double) sym[nsym].spec.scalar.b; return 1;
	 case 1: *value = (double) sym[nsym].spec.scalar.w; return 1;
	 case 2: *value = (double) sym[nsym].spec.scalar.l; return 1;
	 case 3: *value = (double) sym[nsym].spec.scalar.f; return 1;
	 case 4: *value = sym[nsym].spec.scalar.d; return 1;
	 default: return execute_error(46);
 }
 }
 /*------------------------------------------------------------------------- */
double double_arg(nsym)
 /*returns the double value of a scalar symbol with # nsym, if nsym is not
 a scalar, gens an error */
 int nsym;
 {
 if ( sym[nsym].class == 8) nsym = class8_to_1(nsym);	/*scalar ptr case */
 if ( sym[nsym].class != 1) return execute_error(10);
 /*convert value to int */
	 switch (sym[nsym].type) {
	 case 0: return (double) sym[nsym].spec.scalar.b;
	 case 1: return (double) sym[nsym].spec.scalar.w;
	 case 2: return (double) sym[nsym].spec.scalar.l;
	 case 3: return (double) sym[nsym].spec.scalar.f;
	 case 4: return sym[nsym].spec.scalar.d;
	 default: return execute_error(46);
 }
 }
 /*------------------------------------------------------------------------- */
int redef_scalar(nsym, ntype, val)
 /*redefine a symbol to be a scalar of ntype with value val */
 int	nsym, ntype;
 union	scalar	*val;
 {
 /*delete the old symbol's memory (if any) */
 if (notallowed(nsym)) return execute_error(116);
 if ( delete_symbol(nsym) != 1 ) return execute_error(17);
 sym[nsym].class=1;
 switch (sym[nsym].type=ntype) {
	 case 0: sym[nsym].spec.scalar.b=val->b; break;
	 case 1: sym[nsym].spec.scalar.w=val->w; break;
	 case 2: sym[nsym].spec.scalar.l=val->l; break;
	 case 3: sym[nsym].spec.scalar.f=val->f; break;
	 case 4: sym[nsym].spec.scalar.d=val->d; break;
	 default: return execute_error(46);
 }
 return 1;
 }
 /*------------------------------------------------------------------------- */
int set_scalar_value(nsym, ntype, val)
 /* similar for many cases to redef_scalar, used for scalar arguments
 that are returned, does subscript pointers properly, does not redefine
 unless necessary */
 int	nsym, ntype;
 union	scalar	*val;
 {
 if (notallowed(nsym)) return execute_error(116);
 switch ( sym[nsym].class ) {
 case 1:
	/* OK to redefine here, might change type, may change mind
	about this case */
	break;
 case 8:
	/* need to convert and put in target */ 
	
	{ int *p;
	 p = sym[nsym].spec.array.ptr;
	 ana_array_convert( &val, &p, ntype, sym[nsym].type, 1);
	}
	return 1;
 default:
 	/* other cases get really redefined */
	break;
 }
 /* here if we don't like it and it gets redefined */
 return redef_scalar(nsym, ntype, val);
 }
 /*------------------------------------------------------------------------- */
int redef_string(i,n)
 /*redefine a symbol to be a string of length n */
 int	i, n;
 {
 size_t mq;
 if (notallowed(i)) return execute_error(116);
 /*delete the old symbol's memory (if any) */
 if ( delete_symbol(i) != 1 ) return execute_error(17);
 sym[i].class=2;	sym[i].type=14;
 mq = n+1;	/*extra byte for the null */
 sym[i].spec.array.bstore = mq;
 sym[i].spec.array.ptr = (int *) malloc(mq);
 if (!sym[i].spec.array.ptr) return execute_error(23);
 return 1;
 }
 /*------------------------------------------------------------------------- */
int delete_symbol(nsym)
 /*deallocate any memory assigned to a symbol and make it undefined */
 int	nsym;
 {
   return delete_symbol_via_ptr(&sym[nsym]);
 }
 /*------------------------------------------------------------------------- */
int delete_symbol_via_ptr(symp)
 /*deallocate any memory assigned to a symbol and make it undefined */
 struct sym_desc *symp;
 {
 int	iq,mq;
 iq = symp->class;
 symp->class=255;			/*everybody gets undefined */
 /*printf("delete, number, class = %d %d\n", nsym, iq);*/
 switch (iq) {
 case 0: 			/* do nothing */
 case 1: 			/* scalars have no extra memory allocated */
 case 5: 			/* transfers, nothing allocated */
 case 8: /*scalar pointers shouldn't happen here, no memory allocated */
	 return 1;
 case 2:				/* string */
 case 3:				/* compile string */
 case ARRAY:				/* array */
 case SUBSC_DESC:			/* reclam vectors, like array*/
 case ASSOC:
	 /*treat this the same, free memory and make undefined */
	 mq =  symp->spec.array.bstore;
	 /*don't delete if mq (bstore) is 0, usually means contents transferred
	 to another symbol during a calculation */
	 /* if the pointer is null, free shouldn't do anything */
	if (mq !=0 ) { free(symp->spec.array.ptr);
			  symp->spec.array.bstore = 0; }
	 return 1;
 case STRING_PTR:			/* string array */
   {
   int	mq, nd, j, nelem;
   struct	ahead	*h;
   char	**p;
	
   /* more complicated since we may have to delete the space assigned
   to all the strings it points to */
   mq = symp->spec.array.bstore;
   /*don't delete if mq (bstore) is 0, usually means contents transferred
   to another symbol during a calculation */
   if (mq !=0 ) {
   h = (struct ahead *) symp->spec.array.ptr;
   p = (char **) ((char *)h + sizeof(struct ahead));
   nd = h->ndim;
   nelem = 1; for (j=0;j<nd;j++) nelem *= h->dims[j];	/*# of elements */
   while (nelem--) { if (*p) free(*p); p++; }
	 
   free(symp->spec.array.ptr);
  symp->spec.array.bstore = 0; }
   return 1;
   }
	
 case SYM_ARR:			/* symbol array */
   {
   int	mq, nd, j, nelem, stat;
   struct	ahead	*h;
   struct sym_desc *p;
	
   /* more complicated since we may have to delete the symbols and their
   allocated memory */
   mq = symp->spec.array.bstore;
   /*don't delete if mq (bstore) is 0, usually means contents transferred
   to another symbol during a calculation */
   if (mq !=0 ) {
   h = (struct ahead *)symp->spec.array.ptr;
   p = (struct sym_desc *) ((char *)h + sizeof(struct ahead));
   nd = h->ndim;
   nelem = 1; for (j=0;j<nd;j++) nelem *= h->dims[j];	/*# of elements */
   while (nelem--) { 
    /* each symbol in the array must be deleted, use re-entrant call
    this should support nesting but nesting of symbol arrays is not
    implemented elsewhere */
    stat = delete_symbol_via_ptr(p);
    if (stat != 1) return stat;
    p++;
   }
	 
   free(symp->spec.array.ptr);
  symp->spec.array.bstore = 0; }
   return 1;
   }
	
 case EVB:
	 /* check for a FOR statement which is an exception to usual rules */
	 if (symp->type == 4 ) return 1;	/* these have no calloc */
	 if (symp->xx < 5 ) return 1;	
	 /* evd's and edb's, if narg > 4 then need to deallocate some memory */
	 free (symp->spec.array.ptr);
	 symp->spec.array.bstore = 0;
	 return 1;
 case BIN_OP:
 case NON_BIN_OP:
	 /* class 192, 193 only have memory allocated if narg > 4 */
	 if ( symp->type < 5 ) return 1;	
	 /* evd's and edb's, if narg > 4 then need to deallocate some memory */
	 free (symp->spec.array.ptr);
	 symp->spec.array.bstore = 0;
	 return 1;
 case 255:
	 symp->spec.array.bstore = 0;	/* just in case */
	 return 1;
 default: printf("(X)impossible symbol class in delete_symbol_via_ptr\n");
	  printf("class %d\n",iq);
	  return -1;
 }
 }
  /*------------------------------------------------------------------------- */
int zapargs(context)
  /*zaps all the symbols associated with the passed context (i.e., subr) */
  int	context;
  {
  struct	sym_list	*ns, *ns2, **back;
  int	nsym, hv;
  /*printf("zapargs, context = %d\n",context);*/
  ns = subr_sym_list[context];
  back = &(subr_sym_list[context]);
  while ( ns != NULL) { nsym = ns->num;
  ns2 = ns->next;
  /*check if it is named, if not, we delete from this list */
  /*note that edb's, etc, are not named and use the hash field for narg, so
   set hv = -1 if class >=192 */
  if (sym[nsym].class >191 &&  sym[nsym].class < 201 ) hv = -1;  else
		  hv = sym[nsym].xx;
  /*printf("class = %d, hv = %d\n", sym[nsym].class,hv);*/
  /*printf("zapping symbol # %d", nsym);*/
  /*check if a legal hash value */
  if ( hv < 0 || hv >= HASHSIZE ) {	/*not named, so fair to remove it */
  *back = ns2;
  /*printf("free in zapargs, ptr = %d\n",ns);*/
	  free(ns); }
	  else	back = &(ns->next);
  delete_symbol( nsym );	sym[nsym].class = 0;
  /* note that the class = 0 is used when checking formal input arg lists */
  ns = ns2; }
  return 1;
  }
  /*------------------------------------------------------------------------- */
int ana_byte(narg,ps)
  /*create a byte version of nsym in a temporary if necessary */
  int narg,ps[];
  {
  register int n;
  register union	types_ptr q1,q3;
  int	nsym, nd, j;
  int	result_sym, type, new_type;
  struct	ahead	*h;
  nsym=ps[0];				/*only one argument */
  type = sym[nsym].type;
  new_type = 0;
  /*check if already the desired type */
  if ( type == new_type ) return nsym;
  /*switch on the class */
  switch (sym[nsym].class)	{
  case 8:							/*scalar ptr*/
  result_sym = scalar_scratch(new_type); n=1; q1.l = sym[nsym].spec.array.ptr;
  q3.l = &sym[result_sym].spec.scalar.l; break;
  case 1:							/*scalar */
  result_sym = scalar_scratch(new_type); n=1; q1.l = &sym[nsym].spec.scalar.l;
  q3.l = &sym[result_sym].spec.scalar.l; break;
  case 2:							/*string */
  result_sym = scalar_scratch(new_type);
  sym[result_sym].spec.scalar.b = atol( (char *)sym[nsym].spec.array.ptr);
  return result_sym;
  case 4:							/*array */
  h = (struct ahead *) sym[nsym].spec.array.ptr;
  q1.l = (int *) ((char *)h +  sizeof(struct ahead));
  nd = h->ndim;
  n = 1; for (j=0;j<nd;j++) n *= h->dims[j];	/*# of elements for nsym */
  result_sym = array_clone(nsym,new_type);
  h = (struct ahead *) sym[result_sym].spec.array.ptr;
  q3.b = (byte *) ((char *)h + sizeof(struct ahead));
  break;
  default:	return execute_error(30);
  }
  /*now the conversion */
  switch (type)	{
  case 0: return execute_error(3);
  case 1: while (n) { *q3.b++ = (byte) (*q1.w++);n--;} break;
  case 2: while (n) { *q3.b++ = (byte) (*q1.l++);n--;} break;
  case 3: while (n) { *q3.b++ = (byte) (*q1.f++);n--;} break;
  case 4: while (n) { *q3.b++ = (byte) (*q1.d++);n--;} break;
  }
  return result_sym;
  }
  /*------------------------------------------------------------------------- */
int ana_word(narg,ps)
  /*create a word (I*2) version of nsym in a temporary if necessary */
  int narg,ps[];
  {
  register int n;
  register union	types_ptr q1,q3;
  int	nsym, nd, j;
  int	result_sym, type, new_type;
  struct	ahead	*h;
  nsym=ps[0];				/*only one argument */
  type = sym[nsym].type;
  new_type = 1;
  /*check if already the desired type */
  if ( type == new_type ) return nsym;
  /*switch on the class */
  switch (sym[nsym].class)	{
  case 8:							/*scalar ptr*/
  result_sym = scalar_scratch(new_type); n=1; q1.l = sym[nsym].spec.array.ptr;
  q3.l = &sym[result_sym].spec.scalar.l; break;
  case 1:							/*scalar */
  result_sym = scalar_scratch(new_type); n=1; q1.l = &sym[nsym].spec.scalar.l;
  q3.l = &sym[result_sym].spec.scalar.l; break;
  case 2:							/*string */
  result_sym = scalar_scratch(new_type);
  sym[result_sym].spec.scalar.w = atol( (char *)sym[nsym].spec.array.ptr);
  return result_sym;
  case 4:							/*array */
  h = (struct ahead *) sym[nsym].spec.array.ptr;
  q1.l = (int *) ((char *)h + sizeof(struct ahead));
  nd = h->ndim;
  n = 1; for (j=0;j<nd;j++) n *= h->dims[j];	/*# of elements for nsym */
  result_sym = array_clone(nsym,new_type);
  h = (struct ahead *) sym[result_sym].spec.array.ptr;
  q3.w = (short *) ((char *)h + sizeof(struct ahead));
  break;
  default:	return execute_error(30);
  }
  /*now the conversion */
  switch (type)	{
  case 0: while (n) { *q3.w++ = (short) (*q1.b++);n--;} break;
  case 1: return execute_error(3);
  case 2: while (n) { *q3.w++ = (short) (*q1.l++);n--;} break;
  case 3: while (n) { *q3.w++ = (short) (*q1.f++);n--;} break;
  case 4: while (n) { *q3.w++ = (short) (*q1.d++);n--;} break;
  }
  return result_sym;
  }
  /*------------------------------------------------------------------------- */
int ana_long(narg,ps)
  /*create a long (I*4) version of nsym in a temporary if necessary */
  int narg,ps[];
  {
  register int n;
  register union	types_ptr q1,q3;
  int	nsym, nd, j;
  int	result_sym, type, new_type;
  struct	ahead	*h;
  nsym=ps[0];				/*only one argument */
  type = sym[nsym].type;
  new_type = 2;
  /*check if already the desired type */
  if ( type == new_type ) return nsym;
  /*switch on the class */
  switch (sym[nsym].class)	{
  case 8:							/*scalar ptr*/
  result_sym = scalar_scratch(new_type); n=1; q1.l = sym[nsym].spec.array.ptr;
  q3.l = &sym[result_sym].spec.scalar.l; break;
  case 1:							/*scalar */
  result_sym = scalar_scratch(new_type); n=1; q1.l = &sym[nsym].spec.scalar.l;
  q3.l = &sym[result_sym].spec.scalar.l; break;
  case 2:							/*string */
  result_sym = scalar_scratch(new_type);
  sym[result_sym].spec.scalar.l = atoi( (char *)sym[nsym].spec.array.ptr);
  return result_sym;
  case 4:							/*array */
  /* printf("input type and class again = %d %d\n",type, sym[nsym].class); */
  h = (struct ahead *) sym[nsym].spec.array.ptr;
  q1.l = (int *) ((char *)h + sizeof(struct ahead));
  nd = h->ndim;
  n = 1; for (j=0;j<nd;j++) n *= h->dims[j];	/*# of elements for nsym */
  result_sym = array_clone(nsym,new_type);
  h = (struct ahead *) sym[result_sym].spec.array.ptr;
  q3.l = (int *) ((char *)h + sizeof(struct ahead));
  break;
  default:	return execute_error(30);
  }
  /*now the conversion */
  switch (type)	{
  case 0: while (n) { *q3.l++ = (int) (*q1.b++);n--;} break;
  case 1: while (n) { *q3.l++ = (int) (*q1.w++);n--;} break;
  case 2: return execute_error(3);
  case 3: while (n) { *q3.l++ = (int) (*q1.f++);n--;} break;
  case 4: while (n) { *q3.l++ = (int) (*q1.d++);n--;} break;
  }
  return result_sym;
  }
 /*------------------------------------------------------------------------- */
int ana_rfix(narg,ps)
 /*create a long (I*4) version of nsym using nearest integer*/
 int narg,ps[];
 {
 register int n;
 register union	types_ptr q1,q3;
 int	nsym, nd, j;
 int	result_sym, type, new_type;
 struct	ahead	*h;
 nsym=ps[0];				/*only one argument */
 type = sym[nsym].type;
 new_type = 2;
 /*check if already the desired type */
 if ( type == new_type ) return nsym;
 /*switch on the class */
 switch (sym[nsym].class)	{
 case 8:							/*scalar ptr*/
 result_sym = scalar_scratch(new_type); n=1; q1.l = sym[nsym].spec.array.ptr;
 q3.l = &sym[result_sym].spec.scalar.l; break;
 case 1:							/*scalar */
 result_sym = scalar_scratch(new_type); n=1; q1.l = &sym[nsym].spec.scalar.l;
 q3.l = &sym[result_sym].spec.scalar.l; break;
 case 2:							/*string */
 result_sym = scalar_scratch(new_type);
 sym[result_sym].spec.scalar.l = atol( (char *)sym[nsym].spec.array.ptr);
 break;
 case 4:							/*array */
 h = (struct ahead *) sym[nsym].spec.array.ptr;
 q1.l = (int *) ((char *)h + sizeof(struct ahead));
 nd = h->ndim;
 n = 1; for (j=0;j<nd;j++) n *= h->dims[j];	/*# of elements for nsym */
 result_sym = array_clone(nsym,new_type);
 h = (struct ahead *) sym[result_sym].spec.array.ptr;
 q3.l = (int *) ((char *)h + sizeof(struct ahead));
 break;
 default:	return execute_error(30);
 }
 /*now the conversion */
 switch (type)	{
 case 0: while (n) { *q3.l++ = (int) (*q1.b++);n--;} break;
 case 1: while (n) { *q3.l++ = (int) (*q1.w++);n--;} break;
 case 2: return execute_error(3);  /* note we should have handled this above */
 case 3: while (n) { *q3.l++ = (int) rint( (double) *q1.f++);n--;} break;
 case 4: while (n) { *q3.l++ = (int) rint(*q1.d++);n--;} break;
 }
 return result_sym;
 }
 /*------------------------------------------------------------------------- */
int ana_float(narg,ps)
 /*create a float (F*4) version of nsym in a temporary if necessary */
 int narg,ps[];
 {
 register int n;
 register union	types_ptr q1,q3;
 int	nsym, nd, j;
 int	result_sym, type, new_type;
 struct	ahead	*h;
 nsym=ps[0];				/*only one argument */
 type = sym[nsym].type;
 new_type = 3;
 /*check if already the desired type */
 if ( type == new_type ) return nsym;
 /*switch on the class */
 switch (sym[nsym].class)	{
 case 8:							/*scalar ptr*/
 result_sym = scalar_scratch(new_type); n=1; q1.l = sym[nsym].spec.array.ptr;
 q3.l = &sym[result_sym].spec.scalar.l; break;
 case 1:							/*scalar */
 result_sym = scalar_scratch(new_type); n=1; q1.l = &sym[nsym].spec.scalar.l;
 q3.l = &sym[result_sym].spec.scalar.l; break;
 case 2:							/*string */
 result_sym = scalar_scratch(new_type);
 sym[result_sym].spec.scalar.f = atof( (char *)sym[nsym].spec.array.ptr);
 break;
 case 4:							/*array */
 h = (struct ahead *) sym[nsym].spec.array.ptr;
 q1.l = (int *) ((char *)h + sizeof(struct ahead));
 nd = h->ndim;
 n = 1; for (j=0;j<nd;j++) n *= h->dims[j];	/*# of elements for nsym */
 result_sym = array_clone(nsym,new_type);
 h = (struct ahead *) sym[result_sym].spec.array.ptr;
 q3.f = (float *) ((char *)h + sizeof(struct ahead));
 break;
 default:	return execute_error(30);
 }
 /*now the conversion */
 switch (type)	{
 case 0: while (n) { *q3.f++ = (float) (*q1.b++);n--;} break;
 case 1: while (n) { *q3.f++ = (float) (*q1.w++);n--;} break;
 case 2: while (n) { *q3.f++ = (float) (*q1.l++);n--;} break;
 case 3: return execute_error(3);
 case 4: while (n) { *q3.f++ = (float) (*q1.d++);n--;} break;
 }
 return result_sym;
 }
 /*------------------------------------------------------------------------- */
int ana_double(narg,ps)
 /*create a double (F*8) version of nsym in a temporary if necessary */
 int narg,ps[];
 {
 register int n;
 register union	types_ptr q1,q3;
 int	nsym, nd, j;
 int	result_sym, type, new_type;
 struct	ahead	*h;
 nsym=ps[0];				/*only one argument */
 type = sym[nsym].type;
 new_type = 4;
 /*check if already the desired type */
 if ( type == new_type ) return nsym;
 /*switch on the class */
 switch (sym[nsym].class)	{
 case 8:							/*scalar ptr*/
 result_sym = scalar_scratch(new_type); n=1; q1.l = sym[nsym].spec.array.ptr;
 q3.l = &sym[result_sym].spec.scalar.l; break;
 case 1:							/*scalar */
 result_sym = scalar_scratch(new_type); n=1; q1.l = &sym[nsym].spec.scalar.l;
 q3.l = &sym[result_sym].spec.scalar.l; break;
 case 2:							/*string */
 result_sym = scalar_scratch(new_type);
 sym[result_sym].spec.scalar.d = atof( (char *)sym[nsym].spec.array.ptr);
 break;
 case 4:							/*array */
 h = (struct ahead *) sym[nsym].spec.array.ptr;
 q1.l = (int *) ((char *)h + sizeof(struct ahead));
 nd = h->ndim;
 n = 1; for (j=0;j<nd;j++) n *= h->dims[j];	/*# of elements for nsym */
 result_sym = array_clone(nsym,new_type);
 h = (struct ahead *) sym[result_sym].spec.array.ptr;
 q3.d = (double *) ((char *)h + sizeof(struct ahead));
 break;
 default:	return execute_error(30);
 }
 /*now the conversion */
 switch (type)	{
 case 0: while (n) { *q3.d++ = (double) (*q1.b++);n--;} break;
 case 1: while (n) { *q3.d++ = (double) (*q1.w++);n--;} break;
 case 2: while (n) { *q3.d++ = (double) (*q1.l++);n--;} break;
 case 3: while (n) { *q3.d++ = (double) (*q1.f++);n--;} break;
 case 4: return execute_error(3);
 }
 return result_sym;
 }
 /*------------------------------------------------------------------------- */
int ana_switch(narg,ps)
 /*switch 2 symbols, now called exchange */
 int narg,ps[];
 {
 int	nsym1, nsym2, n;
 struct  sym_desc buf;
 nsym1 = ps[0];	nsym2 = ps[1];
 buf = sym[nsym1]; sym[nsym1] = sym[nsym2]; sym[nsym2] = buf;
 /* but preserve the hash of symbol name */
 buf.xx = sym[nsym2].xx;	sym[nsym2].xx = sym[nsym1].xx;
 sym[nsym1].xx = buf.xx;
 return	1;
 }
 /*------------------------------------------------------------------------- */
