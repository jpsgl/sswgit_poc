/* a place for error codes, no declarations please since this
is included in many places
1/16/2005 - use code 0xa... for messages that aren't really errors, these still
increment the rror count presently pending discussion */

/* a list of the task ID's, the errors have the task ID in the upper
byte of the 16 bit error code
these ID's are mostly from FPPctrl.h

OBS  21
MM    8
FG 01          Filtergraph Obs Generator; ObsGen:fg_obs
SP 02          Spectr-Polarimeter Obs Generator; ObsGen:sp_obs
CH 03          Command Handler; CMtask:CMtask
EV 04
HS 05
CT 06
//LM 7
MC 08         Mechanism Control modules; MCmon:MCmon         
IF 09
OS 0a        Obs Sequencer (implemented by command_scan)
RF 0b
PW 0c
TA 13
TH 14
TM 15
CA 16        Camera Control Task; FPPcam:FPPcam
ME 17        CTM-E (Mirror Electronics)
DC 19        DMA control - in dmaMg.cpp

*/

#define	PRINTF_ERR		0x8001	// a printf in code
#define	MARGIN_VIOLATION_ERR	0x002	// margin zone got zapped
/* FG errors, task fg_obs and calls */
#define	FG_LOCK_ERR		0x101	// couldn't lock the FG devices
#define	FG_UNLOCK_ERR		0x102	// couldn't unlock the FG devices
#define	FG_NO_SUCH_FDB_ERR	0x103	// error in finding FDB
#define	FG_BAD_SYCN_OBS_ID_ERR	0x104	// bad synch in FG table
#define	FG_NO_SUCH_OBS_ID_ERR	0x105	// failure finding FG obs table for ID
#define	FG_ILLEGAL_MASK_ERR	0x106	// out of range mask position
#define	FG_NO_SUCH_GENFUNC_ERR	0x107	// no such gen function supported 
#define	FG_BAD_OFFSET_OBS_ERR	0x108	// offset and nsize disagree in obs table
#define	FG_SHUTTER_START_ERR	0x109	// timed out on shutter start semaphore
#define	FG_INTEG_START_ERR	0x110	// timed out on integrate start semaphore
#define	FG_SHUTTER_CLOSE_ERR	0x111	// timed out on shutter close semaphore
#define	FG_TRANSFER_TOUT_ERR	0x112	// timed out waiting for transfer permission
#define	FG_EXPOSE_TOUT_ERR	0x113	// timed out waiting for expose semaphore
/* FG_LAST_READ_TOUT_ERR now a message */
#define	FG_LAST_READ_TOUT_ERR	0x8114  // waiting for previous FG to output
#define	FG_TUNABLE_FLT_ERR	0x115	// 
#define	FG_BAD_IMAGE_NUM_ERR	0x116	// inappropiate # of images for a FG
#define	FG_PMU_START_STATE_ERR	0x117	// new set but PMU state still active
#define	FG_B_FILTER_TOUT_ERR	0x118	// a timeout on BFI filter wheel
#define	FG_N_FILTER_TOUT_ERR	0x119	// a timeout on NFI filter wheel
#define	FG_FILTER_NUM_ERR	0x120	// an illegal filter wheel position requested
#define	FG_SM_ZERO_ERR		0x121	// zero words in smart memory
#define	FG_SM_IMPOSSIBLE_ERR	0x122	// impossible error involving SM, see code
#define	FG_LOST_PENDING_ERR	0x124	// a pending FG macro was replaced by new one
/* FG_BUFFER_TOUT_ERR becomes a message */
#define	FG_BUFFER_TOUT_ERR	0x8125	// timeout in check_fg_buf
//#define	FG_BAD_FRAME_CNT_ERR	0x126	// sequence frame count out of range
#define	FG_BAD_PACKET_CNT_ERR	0x127	// sequence packet count out of range
#define	FG_RAW_FG_BUSY_ERR	0x128	// can't rawFGpacket because output is busy
#define	FG_RAW_SM_READ_ERR	0x129	// smemsize <= 1 for raw FG packet
#define	FG_RAW_SM_SIZE_ERR	0x130	// raw FG packet has 0 or illegal size
#define	FG_TOO_MANY_PACKETS_ERR	0x131	// error in output packet count
#define	FG_RAW_SM_BAD_DATA_ERR	0x132	// raw FG packet, bad status from getData
#define	FG_RAW_EMIT_PACKET_ERR	0x133	// raw FG packet, bad status from emitPacket_SP
#define	FG_ILLEGAL_COMP_ERR	0x134	// illegal compression choice
#define	FG_BAD_NPAGES_ERR	0x135	// pages not 1 for full readout
#define	FG_SHORT_NPAGES_ERR	0x136	// readout short for full camera
#define	FG_PMU_SANITY_ERR	0x137	// FG PMU obs failed sanity count
#define	FG_SHUTTERLESS_ERR	0x138	// error in FG shutterless observation
#define	FG_MASK_TOUT_ERR	0x139	// timeout for mask motion
#define	FG_BAD_FOCUS_SCAN_RANGE	0x140	// bad focus range would hit stop
#define	FG_FOCUS_SCAN_TOUT	0x141	// timeout for focus motor
#define	FG_TF_MOTOR_TOUT	0x142	// timeout for TF motor
#define	FG_WAVE_CNT_ERR		0x143	// wave scan count too high
#define	FG_NOT_NARROWBAND_ERR	0x144	// wave scan received a broad band
#define	FG_LINE_NOT_UPDATABLE	0x145	// illegal attempt to update a line
#define	FG_SM_READ_ERR		0x146	// smart memory read error
#define	FG_SM_SHORT_ERR		0x147	// smart memory shortfall
#define	FG_SM_LONG_ERR		0x148	// smart memory length exceeds limit
#define	FG_BAD_TABLE_ERR	0x149	// bad table for a shutterless FG
#define	FG_RATIO_MISMATCH_ERR	0x150	// mismatched reads for ratio
#define	FG_BUGINRHS_ERR		0x151	// bug in sub-image RHS
#define	FG_BUGINLHS_ERR		0x152	// bug in sub-image LHS
#define	FG_SHUTTERLESS_TOUT_ERR 0x153	// timeout waiting for PMU state machine
#define	FG_SIDE_ERR		0x154	// side consistency error in sendFGhkPacket
/* FG_HK_TOUT_ERR is now a message */
#define	FG_HK_TOUT_ERR		0x8155	// waiting for last HK Packet
#define	FG_SIDE_CONFIG_ERR	0x156	// side consistency error from smem config
#define	FG_EMI_ERR		0x157	// attempt to run obsolete EMI macro command
#define	FG_PMU_BUF_OVRF_ERR	0x158	// overflow in the PMU image header buffer
#define	FG_EXP_BUF_OVRF_ERR	0x158	// overflow in the shuttered header buffer
#define	FG_BAD_MC		0x159	// bad macro command detected
#define	FG_BAD_FRAME_CNT_ERR	0x160	// sequence frame count out of range
#define	FG_GEN_FUNC_ERR		0x161	// generating function returned with an error
/* 1/10/2006 - make this a message instead of an error */
#define	FG_GEN_STOP_ERR		0x8162	// generating function returned because of STOP
#define	FG_BAD_CUTOUT_ERR	0x163	// bad image cutout, should not happen
#define	FG_BAD_4BUF_CUTOUT_ERR	0x164	// bad image cutout for 4 buffer mode
/* SP errors, task sp_obs and calls */
#define	SP_LOCK_ERR		0x201	// couldn't lock the SP devices
#define	SP_UNLOCK_ERR		0x202	// couldn't unlock the SP devices
#define	SP_EXTRACT_SIZE_ERR	0x203	// bad extract table
#define	SP_PMU_START_STATE_ERR	0x204	// new set but PMU state still active
#define	SP_FRAME_SYNCH_ERR	0x205	// problem with first camera reads
#define	SP_ACCUM_TOUT_ERR	0x206	// 
#define	SP_SLIT_TOUT_ERR	0x207	// slit timed out
#define	SP_TOO_MANY_PACKETS_ERR	0x208	// error in output packet count
#define	SP_SIDE_ERR		0x209	// side consistency error in outputSP
#define	SP_HK_ERR		0x210	// problem with HK packet in outputSP
#define	SP_MEM_SIZE_ERR		0x211	// not expected amount of smart memory
#define	SP_I_BUG_ERR		0x212	// bug in SP I image
#define	SP_SM_SHORT_ERR		0x213	// smart memory shortfall
#define	SP_SM_READ_ERR		0x214	// smart memory read error
#define	SP_SM_COLUMN_ERR	0x215	// smart memory column is too small
#define	SP_EMIT_PACKET_ERR	0x216	// error in emitPacket_SP
#define	SP_RAW_SP_BUSY_ERR	0x217	// can't rawSPpacket because outputSP_busy is on
#define	SP_RAW_SM_READ_ERR	0x218	// smemsize <= 1 for raw SP packet
#define	SP_RAW_SM_SIZE_ERR	0x219	// raw SP packet has 0 or illegal size
#define	SP_RAW_SM_BAD_DATA_ERR	0x220	// raw SP packet, bad status from getData
#define	SP_RAW_EMIT_PACKET_ERR	0x221	// raw SP packet, bad status from emitPacket_SP
/* SP_OUTPUT_TOUT_ERR now a message */
#define	SP_OUTPUT_TOUT_ERR	0x8222	// waiting for previous SP output
#define	SP_LOST_PENDING_ERR	0x223	// a pending SP macro was replaced by new one
//#define	SP_BAD_FRAME_CNT_ERR	0x224	// sequence frame count out of range
#define	SP_BAD_PACKET_CNT_ERR	0x225	// sequence packet count out of range
#define	SP_SLIT_SETUP_ERR	0x226	// slit problem in setup
#define	SP_SLIT_WAIT_ERR	0x227	// slit problem in SLITWAIT state
#define	SP_REF_UPDATE_ERR	0x228	// timeout getting ref update semaphore
#define	SP_IMAGE_BUF_OVRF_ERR	0x229	// overflow in the image header buffer
#define	SP_SLIT_LOST_ERR	0x230	// expected and encoder position differ > 5
#define	SP_BAD_FRAME_CNT_ERR	0x231	// mismatch with expected frame count
#define	SP_SPCRASH_ERR		0x232	// SPcrash recovery called
#define	SP_BADSCALE_ERR		0x233	// bad smart memory table (used for scaling)
#define	SP_OUTPUT_TOUT2_ERR	0x8234	// timeout while in SLITWAIT but slit OK

/* CMtask errors, use code 3 */

#define CM_INV_OPCODE_ERR	0x0302  // invalid opcode
#define CM_CMD_LEN_ERR		0x0303	// inconsistent command length
#define CM_INV_LEN_ERR		0x0304  // invalid command length (doesn't match data base)
#define CM_CR_CMD_DIS_ERR	0x0305	// error in critical command enable
#define CM_DUMP_BUSY_ERR	0x8306	// IDLE != DumpBlk.dumpStatus()
#define CM_DIAG_CMD_ERR		0x8307	// not used?
#define CM_MEMLD_ADR_ERR	0x0308	// FPP_UPLOAD_EEPROM, bad address
#define	CM_TOO_MANY_CHANS_ERR	0x8309	// too many diagnostic channels
#define	CM_TIMEOUT_ERR		0x8310	// no command for 6 seconds (FEP down?)
#define CM_HRDUMP_ERR           0x0311  // Error in dumping data via high rate
#define CM_CMDLEN_GT_133        0x0312  // command received length is greater than 133 bytes
#define CM_HRDUMP_NOT_DONE      0x0313  // previous memory dump packet still pending, this one skipped

/* CT errors, use code 6 */
#define	CT_NO_JITTER_BUF_ERR	0x601	// no buffer available for jitter output
#define	CT_NO_AUGJITTER_BUF_ERR	0x602	// no buffer available for augmented jitter output
/* CT_ALIVE_ERR now a message */
#define	CT_ALIVE_ERR		0xA603	// not really an error, CT just came alive
#define	CT_DIED_ERR		0x604	// watcher noticed that CT is dead
/* CT_LIVE_NOTSENT_ERR no longer used */
//#define	CT_LIVE_NOTSENT_ERR	0x605	// a ready live CT image not sent
/* make this a message instead */
#define	CT_REF_NOTSENT_ERR	0x8606	// a ready ref CT image not sent
#define	CT_REF_REJECT_ERR	0x607	// a ref CT image had bad line count
#define	CT_LIVE_REJECT_ERR	0x608	// a live CT image had bad line count
#define	CT_FRAME_CNT_ERR	0x609	// bad CT frame counts, DMA mismatch?
#define	CT_SHORT_INTERVAL_ERR	0x610	// a short interval since last CT interrupt
#define	CT_LONG_INTERVAL_ERR	0x611	// a very long interval since last CT interrupt
#define	CT_PMU_SYNCH_ERR	0x612	// a PMU isr request not processed in time
#define	CT_NOT_ALIVE_ERR	0x613	// a CT cmd refused because CT is not running
#define	CT_NOT_ON_ERR		0x614	// a CT cmd refused because CT is not even on

/* MC error, use code 8 */
#define MC_PMU_PHASE_ERR        0x0801  // PMU phase does not match expected phase
#define MC_INV_DEVNUM_ERR       0x0802  // Invalid device number (> 16)
#define MC_INV_CMD_ERR          0x0803  // Unrecognized motor command, p1=opcode,p2=motor type
#define MC_INV_DEVTYP_ERR       0x0804  // Invalid device type  
#define MC_INV_PRM_ERR          0x0805  // Invalid parameter,p1=devtype,p2=prm#,p3,p4=inv prms
#define MC_DEV_ERR 		0x0806  // device error (locked ?)
#define MC_BAD_SLIT_ERR 	0x0807  // bad state when entering move method
#define MC_PTR_CORRUPTED        0x0808  // Linear motor pointer corrupted
#define MC_LINMTR_TOUT_ERR      0x0809  // timeout while stepping linear motor
#define MC_SSC_TIMOUT           0x080A  // timeout during slit centering operation
#define MC_SS_CENTERED          0x8801  // Slit Scan Mechanism Centered

/* data product errors, use code 32 (20 hex) */
#define	DI_NO_DIAG_BUF_ERR	0x2001	// no buffer available for diagnostics output
#define	DI_ALREADY_ON_ERR	0xA002	// a diagnostic mode already in progress
#define	DI_NO_VALID_CHAN_ERR	0xA003	// no valid channel 

/* CA errors, use code 16 (10 hex) */
#define	CA_DOEXP_BLOCKED_ERR	0x9001	// doExp blocked by a running macro command
#define	CA_DOEXP_FG_TOUT_ERR	0x1002	// FG doExp timed out on expose semaphore
#define	CA_DOEXP_SP_TOUT_ERR	0x1003	// SP doExp timed out on expose semaphore
#define	CA_FG_NOFFLAG_ERR	0x1004	// FG doExp timed out on frame flag
#define	CA_SP_NOFFLAG_ERR	0x1005	// SP doExp timed out on frame flag
#define	CA_BAD_CAMERA_ERR	0x9006	// the camera # is out of range
#define	CA_NO_SUCH_CMD_ERR	0x1007	// an invalid command
#define	CA_CAM_WAS_LOCKED_ERR	0x9008	// the requested camera was locked
#define	CA_ILLEGAL_CT_ERR	0x1009	// an illegal command for CT
#define	CA_ILLEGAL_CNFG_ERR	0x1010	// an illegal camera configuration
#define	CA_ILLEGAL_ROI_ERR	0x9011	// bad ROI start/stop combination
#define	CA_SHUTTER_CLOSE_ERR	0x1012	// timed out on shutter close semaphore
#define	CA_FG_PENDING_ERR	0x9013	// already a FG camera read pending?
#define	CA_SP_PENDING_ERR	0x9014	// already a SP camera read pending?
#define	CA_LOCK_ERR		0x9015	// camera command rejected because locked
#define	CA_FG_MACRO_LOCK_ERR	0x9016	// FG macro running
#define	CA_SP_MACRO_LOCK_ERR	0x9017	// SP macro running

/* OS uses code 10 (A hex) */
#define	OS_MACRO_SIZE_ERR	0x0A01	// unexpected macro command size

/* RF (Reference Frame Update) uses codes 0x0bxx */
#define RF_FRM_COUNT1_ERR      0x0b01   // Frame count did not change after 1000 checks
#define RF_FRM_COUNT2_ERR      0x0b02   // Frame count did not change after 1000 checks
#define RF_DN_LOW_ERR          0x0b03   // Image mean too low
#define RF_OVERFLOW_ERR        0x0b04   // Sums Overflow error
#define RF_SRU_TIMEOUT_ERR     0x0b05   // Timed out waiting for semRefUpdate

/* smart memory, use code33 (hex 21) */
#define	SMART_MEM_READ_ERR		0x2101	// smart memory read error
#define	SMART_MEM_ALIGN_ERR		0x2102	// smart memory alignment error
#define	SMART_MEM_DEST_ERR		0x2103	// smart memory destination error
#define	SMART_MEM_COUNT_ERR		0x2104	// smart memory count error
#define	SMART_MEM_ALIGN2_ERR		0x2105	// smart memory alignment 2 error
#define	SMART_MEM_UNKNOWN_ERR		0x2106	// smart memory unknown error
#define	SMART_MEM_SIDE_ERR		0x2107	// smart memory unknown error
#define SMART_MEM_RETRY_LMT_ERR		0x2108	// DMA could not be started after 5 tries
#define SMART_MEM_BUSY_ERR		0x2109	// DMA still active, retrying
#define SMART_MEM_INC_ERR		0x2110	// DMA incomplete or aborted
#define SMART_TOOLONG_ERR		0x2111  // not enough time for DMA
#define SMART_CMD_OVRFLW_ERR		0x2112  // too many command requests
#define SMART_CONFIG_ERR		0x2113  // config not successful
#define SMART_MEM_SYNCH_ERR		0x2114  // command count changed in queue
#define SMART_MEM_CMD_ERR		0x2115  // timeout for a queued command
#define SMART_MEM_DEAD_DMA_ERR		0x2116  // timeout on Smart Memory DMA
#define SMART_MEM_ILLEGAL_ERR		0x2117  // illegal command in queue

/* heater/thermister errors, use code 14 hex */
#define	BAD_DEADBAND_ERR		0x1401	// bad deadbands, lo > hi
#define THERM_READ_ERR                  0x1402  // Bad thermister reading (0)
#define THERM_INDX_ERR                  0x1403  // Bad thermister in alogMon

/* SCIF DMA and HK Errors */
#define DMA_RETRY_LMT_ERR	0x1901		// DMA could not be started after 10 tries
#define DMA_SRC_ADR_ERR         0x1902		// DMA source address error
#define DMA_DEST_ADR_ERR        0x1903          // DMA destination address error
#define DMA_MEMBASE_ERR         0x1904          // DMA base address error
#define DMA_BUSY_ERR	        0x1905          // DMA still active, retrying
#define DMA_INCOMPLETE	        0x1906          // DMA incomplete or aborted
#define DMA_REQ_TIMEOUT		0x1907		// Timed out waiting for semaphore from dmaMg
#define DMA_CNT_ERR		0x1908		// Timed out waiting for semaphore from dmaMg
#define DMA_SRC_RANGE_ERR	0x1909		// bad address in SCIF buffer
#define DMA_SRC_ALIGN_ERR	0x1910		// bad alignment in SCIF buffer
#define DMA_TOOLONG_ERR		0x1911		// not enough time for scif dma
#define DMA_STATE_ERR		0x1912		// mdState error, an internal error
#define SCIF_PREP_FILL_ERR	0x1913		// not enough time for scif dma
#define SCIF_DEAD_DMA_ERR	0x1914		// timeout on SCIF DMA
#define SCIF_SEND_SYNCH_ERR	0x1915		// Sendflag was never cleared before next
/* make SCIF_DEAD_SEND_ERR a message */
#define SCIF_DEAD_SEND_ERR	0x9916		// timeout on SCIF Send
#define SCIF_HK_SIZE_ERR	0x1917		// bad size for HK packet
#define SCIF_HK_TIMEOUT_ERR	0x1918		// timeout for HK packet
#define SCIF_HK_ALIGN_ERR	0x1919		// buffer alignment error
#define SCIF_HK_STATUS_ERR	0x1920		// HK SCIF status error
#define SCIF_SEND_LOCK_ERR	0x1921		// HK SCIF send lock error
#define HK_ANALOG_TIMEOUT_ERR	0x1922		// timeout in getAnalog
#define HK_SEMAPH_TIMEOUT_ERR	0x1923		// timeout in getAnalog semaphore
#define HK_BAD_RDYBIT_ERR	0x1924		// bit didn't zero in getAnalog
#define SCIF_PCK_SIZE_ERR	0x1925		// packet size mismatch

/* FPPCtrl Errors */
#define CTL_INV_ACTIVE_TASKS    0x1701          // The number of active tasks is < 21
#define CTL_TASK_SUSPENDED      0x1702          // Task has become suspended
#define EXC_ERROR               0x1721          // Exception error

/* dmaMg errors */
#define DMAMG_BEHIND_ERR	0xA201	// dmaMg counter lagging CT_Counter
#define DMAMG_AHEAD_ERR		0xA202	// dmaMg counter ahead of CT_Counter
#define DMAMG_MSG_ERR		0x2203	// dmaMg error with message receive
#define DMAMG_SEMAPHORE_ERR	0x2204	// dmaMg bad semaphore given or ?
#define DMAMG_TOOLONG_ERR	0xA205	// dmaMg delayed too long, skip this one

/* EEPROM Operation */
#define EEPROM_OP_OK            0x8000  // EEPROM operation completed OK
#define EEPROM_OP_ERR           0x8FFF  // EEPROM operation error 

/* some debug messages */

#define DEBUG_MESSAGE_1		0xF001  // a message
#define DEBUG_MESSAGE_2		0xF002  // a message
#define DEBUG_MESSAGE_3		0xF003  // a message
#define DEBUG_MESSAGE_4		0xF004  // a message
#define DEBUG_MESSAGE_5		0xF005  // a message
#define DEBUG_MESSAGE_6		0xF006  // a message
#define DEBUG_MESSAGE_7		0xF007  // a message
#define DEBUG_MESSAGE_8		0xF008  // a message
#define DEBUG_MESSAGE_9		0xF009  // a message
