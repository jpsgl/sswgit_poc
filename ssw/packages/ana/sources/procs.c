/*structure for list of internal subroutines*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "ana_structures.h"
#include <sys/types.h>
#include <regex.h>
//#include <unistd.h>
 /* extern	char	*strsavsd();*/
 extern	char	*strsave(char  *s);
 extern	int	hash(), edb2_base;
 extern	int	execute_error();
 extern	struct sym_desc sym[];
 extern	int	ana_type(),ana_zero(),ana_indgen(), ana_hex(), ana_test_wprint();
 extern	int	bytarr(), dblarr(), fltarr(), intarr(), lonarr(), strarr();
 extern	int	symarr();
 extern	int	ana_zeroifnotdefined(), ana_circle_fit_lsq(), ana_blookup();
 extern	int	ana_neg_func(), ana_greg(), ana_getmatchedfiles();
 extern	int	ana_zerof(), ana_dump(), ana_getenv(), ana_getfiles();
 extern	int	ana_getmatchedfiles_r(), ana_getfiles_r(), ana_getdirectories();
 extern	int	ana_byte(), ana_word(), ana_long(), ana_float(), ana_double();
 extern	int	ana_sin(), ana_cos(), ana_tan(), ana_asin(), ana_acos();
 extern	int	ana_atan(), ana_sinh(), ana_cosh(), ana_tanh(), ana_sqrt();
 extern	int	ana_cbrt(), ana_exp(), ana_expm1(), ana_log(), ana_log1p();
 extern	int	ana_log10(), ana_erf(), ana_erfc(), ana_atan2(), ana_hypot();
 extern	int	ana_j0(), ana_j1(), ana_y0(), ana_y1(), ana_strtol();
 extern	int	ana_cputime(), ana_ctime(), ana_pow(), ana_subsc_func();
 extern	int	ana_mktime(), ana_bang_ctime(), ana_gmtime(), ana_timegm();
 extern	int	ana_symclass(), ana_symdtype(), ana_dimen(), ana_num_elem();
 extern	int	ana_num_dimen(), ana_sub_arg(), ana_quit(), ana_symaddress();
 extern	int	ana_abs(), ana_total(), ana_mean(), ana_dsum(), ana_sdev();
 extern	int	ana_runsum(), ana_switch(), ana_swab(), ana_echo();
 extern	int	ana_noecho(), ana_redim(), ana_concat(), ana_return();
 extern	int	ana_wait(), ana_smooth(), ana_lower(), ana_upper();
 extern	int	ana_strlen(), ana_decomp(), ana_pit(), ana_array();
 extern	int	ana_dsolve(), ana_trend(), ana_polate(), ana_detrend();
 extern	int	ana_poly(), ana_strpos(), ana_strcount(), ana_strloc();
 extern	int	ana_strskp(), ana_skipc(), ana_strsub(), ana_strreplace();
 extern	int	ana_strtrim(), ana_bmap(), ana_smap(), ana_wmap(),ana_strtok();
 extern	int	ana_lmap(), ana_fmap(), ana_dmap(), ana_reverse();
 extern	int	ana_scale(), ana_maxf(),ana_minf(),ana_istring(),ana_fstring();
 extern	int	ana_sieve(), ana_hist(), ana_histr(), ana_fzread();
 extern	int	ana_fzinspect(), ana_dsumproduct(), ana_subshift();
 extern	int	ana_subshiftc();
 extern	int	ana_fzhead(), ana_fzwrite(), ana_openr(), ana_openw();
 extern	int	ana_crunch(), ana_read(), ana_decrunch(), ana_compress();
 extern	int	ana_crunchf(), ana_pathsearch(), ana_strpbrk();
 extern	int	ana_close(), ana_readf(), ana_regrid(), ana_regrid3();
 extern	int	ana_expand(), ana_callig(), ana_plot(), ana_oplot();
 extern	int	ana_xymov(), ana_limits(), ana_window(), ana_pen();
 extern	int	ana_erase(), postrelease(), ana_pdev(), ana_gridmatch();
 extern	int	ana_gridmatch_tell(), ana_shift3();
 extern	int	ana_stretch(), ana_getmin9(), ana_postimage();
 extern	int	ana_postraw(), ana_fcwrite(), ana_openr_f(), ana_regrid3ns();
 extern	int	ana_sun_b(), ana_sun_r(), ana_sun_d(), ana_sun_p(), ana_sun_cl();
 extern	int	ana_sun_b_tai(), ana_sun_r_tai(), ana_sun_d_tai();
 extern	int	ana_sun_p_tai(), ana_sun_cl_tai();
 extern	int	ana_jn(), ana_yn(), ana_readf_f(), ana_printf_f();
 extern	int	ana_printf(), ana_redim_f(), ana_rfix(), ana_show();
 extern	int	ana_assoc_var(), ana_rewindf(), ana_openu(), ana_opena();
 extern	int	ana_openw_f(), ana_flush(), ana_zoom(), ana_openu_f();
 extern	int	ana_opena_f(), ana_array_statistics();
 extern int     ana_fade(), ana_fade_init(), ana_findfile();
 extern int     ana_filesize(), ana_noise(), ana_get_lun(), ana_rscale();
 extern int     ana_resample(), ana_bsmooth(), ana_basins(), ana_basins8();
 extern int     ana_bilinxy(), ana_bilinxy_dp(), ana_isnan(), ana_wherenans(), ana_finite();
 extern int     ana_hexstring(), ana_ifstring(), ana_symname();
 extern	int	ana_tense(), ana_tense_loop(), ana_tense_curve();
 extern	int	ana_eval(), ana_contour(), ana_fzread_f(), ana_fzhead_f();
 extern	int	ana_fzwrite_f(), ana_fcwrite_f(), ana_fcrunwrite_f();
 extern	int	ana_inserter(), ana_minloc(), ana_maxloc(), ana_string();
 extern	int	ana_ulib(), ana_time(), ana_date(), ana_spawn(), ana_swapl();
 extern	int	ana_factors(), ana_fcrunwrite(), ana_key_locations();
 extern	int	ana_randomu(), ana_randomn(), ana_sort(), ana_index();
 extern int	ana_isscalar(), ana_isarray(), ana_isstring(), ana_chdir();
 extern int	ana_gsmooth(), ana_gsmoothy(), ana_gfit(), ana_gauss(), ana_postcolorimage();
 extern int	ana_pencolor(), ana_mkdir(), ana_execute_string();
 extern	int	ana_sc(), ana_scb(), ana_power(), ana_matmul(), ana_defined();
 extern	int	ana_systime(),ana_ccdflatmtrim(),ana_crunchrun(),ana_crunchrunf();
 extern	int	ana_filewrite(), ana_fileread(), ana_applyvfilter();
 extern	int	ana_rdft(), ana_rdftb(), ana_not(), ana_dcqf(), ana_dcqb();
 extern	int	ana_diskspace(), ana_filemtime(), ana_isstrarr(), ana_issymarr();
 extern	int	ana_motif_input(), ana_reorder(), ana_removepath();
 extern	int	ana_compile_file(), ana_sprintf(), ana_getfilepath(), ana_sscanf();
 extern	int	ana_nwhere(), ana_zeronans(), ana_symarr_merge();
 extern	int	ana_symarr_split(), ana_symarr_mask(), ana_notdefined();
 extern	int	ana_dilate(), ana_erode(), ana_dilate2(), ana_extract_bits(), ana_dilate3();
 extern	int	ana_dilate4way(), ana_dilatek(), ana_erodek(), ana_sobel();
 extern	int	ana_extract_bits_f(), ana_tai_from_date(), ana_date_from_tai();
 extern	int	ana_complement(), ana_despike(), ana_qlog(), ana_fits_read();
 extern	int	ana_fits_header(), ana_fits_xread(), ana_endian(), ana_deletesymbol();
 extern	int	ana_dimen_symarr(), ana_extractline(), ana_where_unique();
 extern	int	ana_cterror(), ana_cterrorfp(), ana_cterror_set(), ana_cterror_wtset();
 extern	int	ana_mdi2earth(), ana_zapsym(), ana_retarderscj(), ana_polj();
 extern	int	ana_ppolj(), ana_rhalfwavej(), ana_cosinefit(), ana_poloutj();
 extern	int	ana_removeext(), ana_getfileext(), ana_julian1(), ana_julian2();
 extern	int	ana_getversion(), ana_errormess(), ana_strcmp(), ana_fppmacroread();
 extern	int	ana_getobjects(), ana_getcurvature(), ana_getobjectptrs();
 extern	int	ana_objectoverlap(), ana_despikeaia(), ana_cubemedian(), ana_cfitsio_read();
 /* file ID count */
#if defined(__sgi)
 extern	int	ana_getdtablehi();
#endif
#if __APPLE__
 extern	int	ana_dotproduct();
#endif
 /* trace stuff */
 extern	int	ana_trace_intdct(), ana_trace_rdct();
 extern	int	ana_trace_fpdct(), ana_trace_expdct();
 extern	int	ana_trace_expdct2(), ana_huff_table_vert();
 extern	int	ana_trace_decoder(), ana_tri_name_from_tai();
 extern	int	ana_trace_expdct3(), ana_trace_expdct4(), ana_trace_expdct5();
 extern	int	ana_decode_header(), ana_show_header();
 extern	int	ana_huff_table_gen(), ana_huff_encode_dct(), ana_dcthist();
 extern	int	ana_huff_decode_dct();
 /* some things that only work on sgi and alpha */
#if defined(__alpha) | defined(__sgi)
 extern	int	ana_memstat();
 /* Optical disk and ntsc stuff */
 extern int	ana_op1cmd_send(), ana_op1cmd_check(), ana_op1read();
 extern int	ana_op1cmd(), ana_op1cmd_f();
 extern int	ana_op2cmd_send(), ana_op2cmd_check(), ana_op2read();
 extern int	ana_op2cmd(), ana_op2cmd_f();
#endif
 /* support for various file formats */
 extern	int	ana_tiffwrite(), ana_gifwrite(), ana_gifread(), ana_gifread_f();
 extern	int	ana_read_jpeg(), ana_write_jpeg();
 extern	int	ana_read_jpeg_f(), ana_tiffwrite_f(), ana_gifwrite_f();
 extern	int	ana_write_jpeg_f(), ana_jpeg12_scan();
 extern	int	ana_gifmemload(), ana_getimage(), ana_gif3dopenw(), ana_gif3dopenw_f();
 extern	int	ana_gif3dappend(), ana_gif3dappend_f(), ana_gif3dclose();
 extern	int	ana_tiffread(), ana_tiffread_f(), ana_nikonrawread();
 /* X window related stuff */
 extern	int	ana_xport(),ana_xtv(),ana_xtvtrue(),ana_xpen(),ana_xfont();
 extern int     ana_xdelete(),ana_xcopy(),ana_xlabel(),ana_xtvplane();
 extern	int	ana_xevent(),ana_xplace(),ana_xopen();
 extern	int	ana_xlabelwidth(),ana_xtvread(),ana_xtvlct(),ana_xexist();
 extern	int	ana_xsetbackground(),ana_xsetforeground(),ana_xpurge();
 extern	int	ana_xquery(),ana_xcursor(),ana_xwindowinfo(),ana_xdrawline();
 extern	int	ana_xinvertline(),ana_xtvlctraw(),ana_xraise(),ana_xlower();
 extern	int	ana_xquery_f(),ana_pen_invert(),ana_xdrawarc(),ana_xinvertarc();
 extern	int	ana_xfillarc(),ana_xfillrectangle(),ana_xdrawrectangle();
 extern	int	ana_xroot_coord(),ana_xgetwinid(),ana_xtvrct(),ana_tv16();
 extern	int	ana_xroot_coord_id(), ana_xinvertlabel(), ana_xwarpmouse();
 extern	int	ana_tvrct16(),ana_tvmix16(),ana_tvmix16ct(),ana_tv16lookup();
 extern	int	ana_tvconvert16(),ana_tvmix16win(), ana_tvconvert16torgb();
 extern	int	ana_xwd_id(),ana_xwd(), ana_get_box_root(), ana_xwd_root();
 extern	int	ana_xsetinputfocus(), ana_xw2gif(), ana_rgb2pseudo();
 extern	int	ana_blend_test(), ana_blend_ct(), ana_tvmix24win();
 extern	int	ana_tvblend_mov(), ana_xsetgc(), ana_ximagelabel();
 /* X window related stuff for multiple displays */
//  extern	int	ana_xopen2(),ana_xexist2(), ana_xport2(), ana_xsetbackground2();
//  extern	int	ana_xsetforeground2(),ana_xtvlct2(), ana_xtvlctraw2(), ana_xdelete2();
//  extern	int	ana_xraise2(),ana_xlower2(), ana_xerase2(), ana_xtv2();
//  extern	int	ana_xtvplane2(),ana_xcopy2();
//  extern	int	ana_xopen3(),ana_xexist3(), ana_xport3(), ana_xsetbackground3();
//  extern	int	ana_xsetforeground3(),ana_xtvlct3(), ana_xtvlctraw3(), ana_xdelete3();
//  extern	int	ana_xraise3(),ana_xlower3(), ana_xerase3(), ana_xtv3();
//  extern	int	ana_xtvplane3(),ana_xcopy3();

 /* tape stuff */
 extern	int	ana_weof(), ana_taprd(), ana_tapwrt(), ana_unload();
 extern	int	ana_rewind(), ana_skipf(), ana_skipr(), ana_wait_for_tape();
 extern	int	ana_tapebufout(), ana_tapebufin();
 extern int     ana_tape_status();
 /* mappings */
 extern	int	ana_revolve(), ana_postel(), ana_latlongrid();
 /* Mats' stuff */
 extern  int    usvdcmp(), usvbksb(), ufourn(), ana_pgmread();
 extern  int    ana_cmake(), ana_cdiv(), ana_cmul();
 extern  int    ana_cadd(), ana_csub();
 /* Xt stuff */
 extern int	ana_xtloop(), ana_xtmanage(), ana_xtunmanage(), ana_xtparent();
 extern int	ana_xtwindow(), ana_xtismanaged();
 /* socket stuff */
 extern int	ana_connectport(), ana_read_sock(), ana_write_sock();
 extern int	ana_sock_nbyte();
 /* motif stuff */
 extern int	ana_xmmessage(), ana_xmlist(), ana_xmlistfromfile();
 extern int	ana_xmlistadditem(), ana_xmtextfromfile();
 extern int	ana_xmattach(), ana_xmform(), ana_xmlabel();
 extern int	ana_xmbutton(), ana_xmattach_relative(), ana_xmdialog_form();
 extern int	ana_xmdialog_board(), ana_xmposition(), ana_xmborderwidth();
 extern int	ana_xmhscale(), ana_xmdestroy(), ana_xmlist();
 extern int	ana_xmvscale(), ana_xmframe(), ana_xmcolumns();
 extern int	ana_xmvscrollbar(),ana_xmhscrollbar();
 extern int	ana_xmtextfield(), ana_xmalignment(), ana_xmfont();
 extern int	ana_xmforegroundcolor(), ana_xmbackgroundcolor();
 extern int	ana_xmtopshadowcolor(), ana_xmbottomshadowcolor();
 extern int	ana_xmselectcolor(), ana_xmarmcolor(), ana_xmpulldownmenu();
 extern int	ana_xmbordercolor(), ana_xmsetcolors(), ana_xmset_text_output();
 extern int	ana_xmrows(), ana_xmtextfieldgetstring();
 extern int	ana_xmtextfieldsetstring(), ana_xmscalegetvalue();
 extern int	ana_xmscalesetvalue(), ana_xmscaleresetlimits();
 extern int	ana_xmarrow(), ana_xmlistselect(), ana_xmtext();
 extern int	ana_xmtextappend(), ana_xmtexterase(), ana_xmmenubar();
 extern int	ana_xmlistdeleteitem(),ana_xmlistcount(),ana_xmlistdeleteall();
 extern int	ana_xmradiobox(), ana_xmboard(), ana_xmcheckbox();
 extern int	ana_xmtogglegetstate(), ana_xmtogglesetstate();
 extern int	ana_xmtextfieldarray(), ana_xmsensitive(), ana_xmcommand();
 extern int	ana_xmtextfieldseteditable(), ana_xmtextgetstring();
 extern int	ana_xmtextsetstring(), ana_xmoptionmenu();
 extern int	ana_xmgetoptionselection(), ana_xmgetwidgetaddress();
 extern int	ana_xmsetoptionselection(), ana_xmsetlabel();
 extern int	ana_xmscrolledwindow(), ana_xmdrawingarea(), ana_xmdrawinglink();
 extern int	ana_xmpixmapbutton(), ana_xmtextbox(), ana_xmprompt();
 extern int	ana_xmgetwidgetsize(), ana_xmseparator();
 extern int	ana_xmfileselect(), ana_xmtextsetposition();
 extern int	ana_xmtextseteditable(), ana_xmsetmodal(), ana_xmlistfunc();
 extern int	ana_xmtextsetrowcolumnsize(), ana_xmlistsubr();
 extern int	ana_xmsetpixmap(), ana_xmgetpixmap(), ana_xmsetmnemonic();
 extern int	ana_xmsetmargins(), ana_xmsetdirectory(), ana_xmquery();
 extern int	ana_xmtextfieldsetmaxlength(), ana_xmsize(), ana_xmraise();
 extern int	ana_xmscrolledwindowapp(), ana_xmscrollbarsetvalues();
 extern int	ana_xmaddfiletolist(), ana_xmpixmapoptionmenu(),ana_xmsettitle();
 extern int	ana_xmfilegetlist(), ana_xtpopup(), ana_xtpopdown();
 extern int	ana_xmtoplevel_form(), ana_xmresizepolicy();
 extern int	ana_xmtextgetlastposition(), ana_xmtextreplace();
 extern int	ana_xmtextgetselection(), ana_xmtextgetinsertposition();
 extern int	ana_xmtoplevel_board(), ana_xmgetwidgetposition();
 extern int	ana_xmtextsetselection();
/* solar B support stuff */
 extern int	ana_therm2temp(), ana_patan(), ana_imaxtune();
 extern int	ana_fppratio(), ana_fppclosestatusfile(), ana_fppopenstatusfile();
 extern int	ana_fppgetstatuspacket(), ana_fppdecomp(),ana_fppbinratio() ;
 extern int	ana_hkextract(), ana_thermAvg(), ana_tftune(), ana_fgccdshift();
 extern int	ana_ccsdsopen(), ana_ccsdsclose(), ana_ccsds2fpp(), ana_ccsdsread();
 extern int	ana_testimagerotate();
 int	ana_show_subr(), ana_show_func(), ana_dump_func(), ana_dump_subr();
 /* symbol string tables for user subroutines and functions */
 /* these use the usual hashing and linked lists */
#define HASHSIZE 64
 static struct user_subr_table *user_subrs[HASHSIZE];	/* ptrs to chains */	
 static struct user_subr_table *user_funcs[HASHSIZE];	/* ptrs to chains */	
 static struct user_subr_table *user_block[HASHSIZE];	/* ptrs to chains */	
 unsigned short	*user_subr_ptrs[MAX_USER_SUBRS], *user_func_ptrs[MAX_USER_FUNCS];
 unsigned short	*user_code_ptrs[MAX_USER_BLOCKS];
 struct user_subr_table	*user_subrs_nf[MAX_USER_SUBRS];
 struct user_subr_table	*user_funcs_nf[MAX_USER_FUNCS];
 struct user_subr_table	*user_code_nf[MAX_USER_BLOCKS];
 struct sym_list		*subr_sym_list[MAX_USER_SUBRS+MAX_USER_FUNCS+
				 MAX_USER_BLOCKS]; 
 int  next_code_block_num = 0;
 regex_t re;
 /* ana internal subroutine list */
 /* internal tables are fixed and a binary search is used during compile */
 struct	ana_subr_struct
	ana_subr[] = {		/* the actual list, must be in alphabetical
				order of name(s) for the binary search */
	"APPLYVFILTER",8,8, ana_applyvfilter,
	"ARRAY_STATISTICS",3,7, ana_array_statistics,
	"BLOOKUP",3,3, ana_blookup,
	"CALLIG",6,7, ana_callig,
	"CCDFLATMTRIM",4,4, ana_ccdflatmtrim,
	"CCSDSCLOSE",0,0, ana_ccsdsclose,
	"CHDIR",1,1, ana_chdir,
	"CIRCLE_FIT_LSQ",6,7, ana_circle_fit_lsq,
	"CLOSE",1,5, ana_close,
	"COMPILE_FILE",1,1, ana_compile_file,
	"CONTOUR",1,6, ana_contour,
	"COSINEFIT",3,10, ana_cosinefit,
	"CRUNCH",3,3, ana_crunch,
	"CRUNCHRUN",3,3, ana_crunchrun,
	"CTERRORFP",1,1, ana_cterrorfp,
	"CTERRORSET",2,2, ana_cterror_set,
	"CTERRORWTSET",1,1, ana_cterror_wtset,
	"D",0,32, ana_dump,
	"DCQB",1,1, ana_dcqb,
	"DCQF",1,1, ana_dcqf,
	"DCTHIST",3,3, ana_dcthist,
	"DECODE_HEADER", 1, 3, ana_decode_header,
	"DECOMP",1,1, ana_decomp,
	"DECRUNCH",2,2, ana_decrunch,
	"DELETESYMBOL",0,32, ana_deletesymbol,
	"DESPIKEAIA",1,12,ana_despikeaia,
	"DSOLVE",2,2, ana_dsolve,
	"DUMP",0,32, ana_dump,
	"DUMP_FUNC",0,32, ana_dump_func,
	"DUMP_SUBR",0,32, ana_dump_subr,
	"ECHO",0,0, ana_echo,
	"ENDIAN",1,1, ana_endian,
	"ERASE",0,5, ana_erase,
//	"ERASE2",0,5, ana_xerase2,
//	"ERASE3",0,5, ana_xerase3,
	"ERRORMESS",1,1, ana_errormess,
	"EXCHANGE",2,2, ana_switch,
	"EXECUTE",1,1, ana_execute_string,
	"EXIT",0,1, ana_quit,
	"EXTRACT_BITS",4,4, ana_extract_bits,
	"F0H",1,2, ana_fzhead,
	"F0HEAD",1,2, ana_fzhead,
	"F0R",2,3, ana_fzread,
	"F0READ",2,3, ana_fzread,
	"F0W",2,4, ana_fzwrite,
	"F0WRITE",2,4, ana_fzwrite,
	"FADE",2,2, ana_fade,
	"FADE_INIT",2,2, ana_fade_init,
	"FCRUNWRITE",2,3, ana_fcrunwrite,
	"FCWRITE",2,3, ana_fcwrite,
	"FGCCDSHIFT",7,7, ana_fgccdshift,
	"FILEREAD",5,5, ana_fileread,
	"FILEWRITE",2,3, ana_filewrite,
	"FLUSH",1,5, ana_flush,
	"FPPCLOSESTATUSFILE",0,0, ana_fppclosestatusfile,
	"FZH",1,2, ana_fzhead,
	"FZHEAD",1,2, ana_fzhead,
	"FZINSPECT",2,3, ana_fzinspect,
	"FZR",2,3, ana_fzread,
	"FZREAD",2,3, ana_fzread,
	"FZW",2,4, ana_fzwrite,
	"FZWRITE",2,4, ana_fzwrite,
 	"GETBOX_ROOT",0,4, ana_get_box_root,
 	"GETIMAGE",2,2, ana_getimage,
	"GETMIN9",3,3, ana_getmin9,
	"GFIT",3,10, ana_gfit,
	"GIF3DAPPEND",1,1, ana_gif3dappend,
	"GIF3DCLOSE",0,0, ana_gif3dclose,
	"GIF3DOPENW",3,4, ana_gif3dopenw,
	"GIFMEMLOAD",1,2, ana_gifmemload,
	"GIFREAD",2,3, ana_gifread,
	"GIFWRITE",2,3, ana_gifwrite,
	"HAIRS",0,3, ana_xplace,
 	"HEX",1,32, ana_hex,
	"HUFF_TABLE_GEN",3,3, ana_huff_table_gen,
	"HUFF_TABLE_VERT",4,4, ana_huff_table_vert,
	"INSERT",2,4, ana_inserter,
	"KEY_LOCATIONS",4,4, ana_key_locations,
	"LATLONGRID",12,12, ana_latlongrid,
	"LIMITS",0,4, ana_limits,
	"MDI2EARTH",15,15, ana_mdi2earth,
#if defined(__sgi)
	"MEMSTAT",0,0, ana_memstat,
#else
#endif
	"MKDIR",1,1, ana_mkdir,
	"NIKONRAWREAD",2,2, ana_nikonrawread,
	"NOECHO",0,0, ana_noecho,
 	"OBJECTOVERLAP",5,5, ana_objectoverlap,
#if defined(__sgi)
	"OP1CMD",1,1, ana_op1cmd,
	"OP1CMD_SEND",1,1, ana_op1cmd_send,
	"OP2CMD",1,1, ana_op2cmd,
	"OP2CMD_SEND",1,1, ana_op2cmd_send,
#else
#endif
	"OPENA",2,2, ana_opena,
	"OPENR",2,2, ana_openr,
	"OPENU",2,2, ana_openu,
	"OPENW",2,2, ana_openw,
	"OPLOT",1,3, ana_oplot,
	"PDEV",0,2, ana_pdev,
	"PEN",0,2, ana_pen,
	"PENCOLOR",0,1, ana_pencolor,
	"PENINVERT",1,1, ana_pen_invert,
	"PGMREAD",2,3, ana_pgmread,
	"PLOT",1,6, ana_plot,
	"POSTCOLORIMAGE",3,7, ana_postcolorimage,
	"POSTEL",12,12, ana_postel,
	"POSTIMAGE",1,5, ana_postimage,
	"POSTRAW",1,1, ana_postraw,
	"POSTREL",0,0, postrelease,
	"POSTRELEASE",0,0, postrelease,
	"PRINT",1,32, ana_type,
	"PRINTF",1,32, ana_printf,
	"QUIT",0,1, ana_quit,
	"RDFT",1,1, ana_rdft,
	"RDFTB",1,1, ana_rdftb,
	"READ",1,32, ana_read,
	"READF",2,32, ana_readf,
	"READ_JPEG",2,3, ana_read_jpeg,
	"READ_TIFF",2,2, ana_tiffread,
	"REDIM",2,9, ana_redim,
	"RESTORE",2,3, ana_fzread,
	"RETURN",0,1, ana_return,
	"REVOLVE",8,8,ana_revolve,
	"REWIND",1,1, ana_rewind,
	"REWINDF",1,1, ana_rewindf,
	"RGB2PSEUDO",3,3, ana_rgb2pseudo,
	"RSCALE",3,3, ana_rscale,
	"S",0,1, ana_show,
	"SC",3,3, ana_sc,
	"SCB",3,3, ana_scb,
	"SETBACKGROUND",2,2, ana_xsetbackground,
//	"SETBACKGROUND2",2,2, ana_xsetbackground2,
//	"SETBACKGROUND3",2,2, ana_xsetbackground3,
	"SETFOREGROUND",2,2, ana_xsetforeground,
//	"SETFOREGROUND2",2,2, ana_xsetforeground2,
//	"SETFOREGROUND3",2,2, ana_xsetforeground3,
	"SHOW",0,1, ana_show,
	"SHOW_F",0,1, ana_show_func,
	"SHOW_FUN",0,1, ana_show_func,
	"SHOW_FUNC",0,1, ana_show_func,
	"SHOW_HEADER", 1, 2, ana_show_header,
	"SHOW_S",0,1, ana_show_subr,
	"SHOW_SUB",0,1, ana_show_subr,
	"SHOW_SUBR",0,1, ana_show_subr,
	"SKIPF",1,2, ana_skipf,
	"SKIPR",1,2, ana_skipr,
	"SPAWN",1,1, ana_spawn,
	"STARTX",0,1, ana_xtloop,
	"STORE",2,4, ana_fzwrite,
	"SUBSHIFT",4,4, ana_subshift,
	"SUBSHIFTC",4,5, ana_subshiftc,
	"SWAB",1,1, ana_swab,
	"SWAPB",1,1, ana_swab,
	"SWAPL",1,1, ana_swapl,
	"SWITCH",2,2, ana_switch,
	"SYMARR_MASK",2,2, ana_symarr_mask,
	"SYMARR_SPLIT",4,4, ana_symarr_split,
	"T",1,32, ana_type,
	"TAPEBUFIN",2,5, ana_tapebufin,
	"TAPEBUFOUT",2,5, ana_tapebufout,
        "TAPE_STATUS",0,0, ana_tape_status,
	"TAPRD",2,2, ana_taprd,
	"TAPWRT",2,2, ana_tapwrt,
	"TEST_WPRINT",0,0, ana_test_wprint,
	"THERMAVG",3,3, ana_thermAvg,
	"TIFFREAD",2,2, ana_tiffread,
	"TIFFWRITE",2,3, ana_tiffwrite,
	"TIFFWRITE16",2,3, ana_tiffwrite,
	"TV",1,4, ana_xtv,
	"TV16",1,4, ana_tv16,
	"TV16LOOKUP",2,5, ana_tv16lookup,
//	"TV2",1,4, ana_xtv2,
//	"TV3",1,4, ana_xtv3,
	"TVBLEND_MOV",9,9, ana_tvblend_mov,
	"TVCONVERT16TORGB",4,4, ana_tvconvert16torgb,
	"TVLAB",3,4, ana_xlabel,
	"TVLCT",3,4, ana_xtvlct,
//	"TVLCT2",3,4, ana_xtvlct2,
//	"TVLCT3",3,4, ana_xtvlct3,
	"TVLCTRAW",3,3, ana_xtvlctraw,
//	"TVLCTRAW2",3,3, ana_xtvlctraw2,
//	"TVLCTRAW3",3,3, ana_xtvlctraw3,
	"TVMIX16WIN",3,7, ana_tvmix16win,
	"TVMIX24WIN",3,7, ana_tvmix24win,
	"TVPLANE",1,5, ana_xtvplane,
//	"TVPLANE2",1,5, ana_xtvplane2,
//	"TVPLANE3",1,5, ana_xtvplane3,
	"TVTRUE",1,4, ana_xtvtrue,
	"TY",1,32, ana_type,
	"TYPE",1,32, ana_type,
	"UFOURN",1,4, ufourn,
	"ULIB",0,1, ana_ulib,
	"UNLOAD",1,1, ana_unload,
	"USVBKSB",1,9, usvbksb,
	"USVDCMP",1,7, usvdcmp,
	"WAIT",1,1, ana_wait,
	"WAIT_FOR_TAPE",1,1, ana_wait_for_tape,
	"WEOF",1,1, ana_weof,
	"WHERE_UNIQUE",3,3, ana_where_unique,
	"WINDOW",0,4, ana_window,
	"WRITE_JPEG",2,3, ana_write_jpeg,
	"WRITE_SOCK",3,3, ana_write_sock,
	"XCOPY",2,8, ana_xcopy,
//	"XCOPY2",2,8, ana_xcopy2,
//	"XCOPY3",2,8, ana_xcopy3,
	"XCURSOR",2,4, ana_xcursor,
	"XDELETE",1,32, ana_xdelete,
//	"XDELETE2",1,1, ana_xdelete2,
//	"XDELETE3",1,1, ana_xdelete3,
	"XDRAWARC",4,7, ana_xdrawarc,
	"XDRAWLINE",4,5, ana_xdrawline,
	"XDRAWRECTANGLE",4,5, ana_xdrawrectangle,
	"XEVENT",0,0, ana_xevent,
	"XFILLARC",4,7, ana_xfillarc,
	"XFILLRECTANGLE",4,5, ana_xfillrectangle,
	"XFLUSH",0,0, ana_xpurge,
	"XFONT",1,2, ana_xfont,
	"XIMAGELABEL",3,4, ana_ximagelabel,
	"XINVERTARC",4,7, ana_xinvertarc,
	"XINVERTLABEL",3,4, ana_xinvertlabel,
	"XINVERTLINE",4,5, ana_xinvertline,
	"XLABEL",3,4, ana_xlabel,
	"XLOWER",1,2, ana_xlower,
//	"XLOWER2",1,2, ana_xlower2,
//	"XLOWER3",1,2, ana_xlower3,
	"XMALIGNMENT",2,2, ana_xmalignment,
	"XMARMCOLOR",2,2, ana_xmarmcolor,
	"XMATTACH",6,6, ana_xmattach,
	"XMATTACH_RELATIVE",5,5, ana_xmattach_relative,
	"XMBACKGROUNDCOLOR",2,2, ana_xmbackgroundcolor,
	"XMBORDERCOLOR",2,2, ana_xmbordercolor,
	"XMBORDERWIDTH",2,2, ana_xmborderwidth,
	"XMBOTTOMSHADOWCOLOR",2,2, ana_xmbottomshadowcolor,
//	"XMDATETIME",3,3, ana_xmdatetime,
	"XMDESTROY",1,1, ana_xmdestroy,
	"XMDRAWINGLINK",2,2, ana_xmdrawinglink,
	"XMFONT",2,2, ana_xmfont,
	"XMFOREGROUNDCOLOR",2,2, ana_xmforegroundcolor,
	"XMGETPIXMAP",2,2, ana_xmgetpixmap,
	"XMGETWIDGETPOSITION",3,3, ana_xmgetwidgetposition,
	"XMGETWIDGETSIZE",3,3, ana_xmgetwidgetsize,
	"XMLISTADDITEM",2,3, ana_xmlistadditem,
	"XMLISTDELETEALL",1,1, ana_xmlistdeleteall,
	"XMLISTDELETEITEM",2,2, ana_xmlistdeleteitem,
	"XMLISTFUNC",1,1, ana_xmlistfunc,
	"XMLISTSELECT",2,3, ana_xmlistselect,
	"XMLISTSUBR",1,1, ana_xmlistsubr,
	"XMMESSAGE",1,5, ana_xmmessage,
	"XMPOSITION",3,5, ana_xmposition,
	"XMPROMPT",3,8, ana_xmprompt,
	"XMQUERY",1,1, ana_xmquery,
	"XMRAISE",1,1, ana_xmraise,
	"XMRESIZEPOLICY",2,2, ana_xmresizepolicy,
	"XMSCALERESETLIMITS",3,4, ana_xmscaleresetlimits,
	"XMSCALESETVALUE",2,2, ana_xmscalesetvalue,
	"XMSCROLLBARSETVALUES",1,5, ana_xmscrollbarsetvalues,
	"XMSELECTCOLOR",2,2, ana_xmselectcolor,
	"XMSENSITIVE",2,2, ana_xmsensitive,
	"XMSETCOLORS",2,2, ana_xmsetcolors,
	"XMSETDIRECTORY",2,2, ana_xmsetdirectory,
	"XMSETLABEL",2,3, ana_xmsetlabel,
	"XMSETMARGINS",3,3, ana_xmsetmargins,
	"XMSETMNEMONIC",2,2, ana_xmsetmnemonic,
	"XMSETMODAL",2,2, ana_xmsetmodal,
	"XMSETOPTIONSELECTION",2,2, ana_xmsetoptionselection,
	"XMSETPIXMAP",2,2, ana_xmsetpixmap,
	"XMSETTITLE",2,2, ana_xmsettitle,
	"XMSET_TEXT_OUTPUT",1,1, ana_xmset_text_output,
	"XMSIZE",3,3, ana_xmsize,
	"XMTEXTAPPEND",2,3, ana_xmtextappend,
	"XMTEXTERASE",1,1, ana_xmtexterase,
	"XMTEXTFIELDSETEDITABLE",2,2, ana_xmtextfieldseteditable,
	"XMTEXTFIELDSETMAXLENGTH",2,2, ana_xmtextfieldsetmaxlength,
	"XMTEXTFIELDSETSTRING",2,3, ana_xmtextfieldsetstring,
	"XMTEXTREPLACE",3,4, ana_xmtextreplace,
	"XMTEXTSETEDITABLE",2,2, ana_xmtextseteditable,
	"XMTEXTSETPOSITION",2,2, ana_xmtextsetposition,
	"XMTEXTSETROWCOLUMNSIZE",3,3, ana_xmtextsetrowcolumnsize,
	"XMTEXTSETSELECTION",3,3, ana_xmtextsetselection,
	"XMTEXTSETSTRING",2,2, ana_xmtextsetstring,
	"XMTOGGLESETSTATE",2,3, ana_xmtogglesetstate,
	"XMTOPSHADOWCOLOR",2,2, ana_xmtopshadowcolor,
	"XOPEN",1,1, ana_xopen,
//	"XOPEN2",1,1, ana_xopen2,
//	"XOPEN3",1,1, ana_xopen3,
	"XPLACE",0,3, ana_xplace,
	"XPORT",0,5, ana_xport,
//	"XPORT2",0,5, ana_xport2,
//	"XPORT3",0,5, ana_xport3,
	"XPURGE",0,0, ana_xpurge,
	"XQUERY",0,1, ana_xquery,
	"XRAISE",1,32, ana_xraise,
//	"XRAISE2",1,1, ana_xraise2,
//	"XRAISE3",1,1, ana_xraise3,
	"XROOT_COORD",5,5, ana_xroot_coord,
	"XROOT_COORD_ID",5,5, ana_xroot_coord_id,
	"XSETGC",1,2, ana_xsetgc,
	"XSETINPUTFOCUS",1,1, ana_xsetinputfocus,
	"XTLOOP",0,1, ana_xtloop,
	"XTMANAGE",1,1, ana_xtmanage,
	"XTPOPDOWN",1,1, ana_xtpopdown,
	"XTPOPUP",1,1, ana_xtpopup,
	"XTUNMANAGE",1,1, ana_xtunmanage,
	"XTV",1,4, ana_xtv,
//	"XTV2",1,4, ana_xtv2,
//	"XTV3",1,4, ana_xtv3,
	"XW2GIF",1,6, ana_xw2gif,
	"XWARPMOUSE",3,3, ana_xwarpmouse,
	"XWIN",0,5, ana_xport,
	"XWINDOW",0,5, ana_xport,
	"XWINDOWINFO",0,1, ana_xwindowinfo,
	"XYMOV",2,3, ana_xymov,
	"ZAPSYM",1,32, ana_zapsym,
	"ZERO",1,32, ana_zero,
	"ZEROIFNOTDEFINED",1,32, ana_zeroifnotdefined,
	"ZEROIFUNDEFINED",1,32, ana_zeroifnotdefined,
	"ZERONANS",1,1, ana_zeronans
	};
 int	num_ana_subr = sizeof ( ana_subr )/sizeof (struct ana_subr_struct);
 int  next_user_subr_num =sizeof(ana_subr)/sizeof(struct ana_subr_struct);
 /*ana internal function list */
 struct	ana_subr_struct
	ana_func[] = {		/* the actual list, must be in alphabetical
				order of name(s) for the binary search */
	/*note that first must be the negate function */
	/*the set that begin with % are called internally by # */
	"%A_unary_neg",1,1,ana_neg_func,
	"%B_subscript",1,32,ana_subsc_func,
	"%C_cputime",0,0,ana_cputime,
	"%D_to_a_power",2,2,ana_pow,
	"%E_sub_arg",4,4,ana_sub_arg,
	"%F_ctime",0,0,ana_bang_ctime,
	"%G_time",0,0,ana_time,
	"%H_date",0,0,ana_date,
	"%I_systime",0,0,ana_systime,
	"ABS",1,1,ana_abs,
	"ACOS",1,1,ana_acos,
	"ALOG",1,1,ana_log,
	"ALOG10",1,1,ana_log10,
	"ARRAY",2,9,ana_array,
	"ASIN",1,1,ana_asin,
	"ASSOC",2,3,ana_assoc_var,
	"ATAN",1,1,ana_atan,
	"ATAN2",2,2,ana_atan2,
	"ATOL",1,2,ana_strtol,
	"BASINS",1,1, ana_basins,
	"BASINS8",1,1, ana_basins8,
	"BESSEL_J0",1,1,ana_j0,
	"BESSEL_J1",1,1,ana_j1,
	"BESSEL_JN",2,2,ana_jn,
	"BESSEL_Y0",1,1,ana_y0,
	"BESSEL_Y1",1,1,ana_y1,
	"BESSEL_YN",2,2,ana_yn,
	"BILINXY",5,5, ana_bilinxy,
	"BILINXY_DP",5,5, ana_bilinxy_dp,
	"BJ0",1,1,ana_j0,
	"BJ1",1,1,ana_j1,
	"BJN",2,2,ana_jn,
	"BLEND_CT",0,0,ana_blend_ct,
	"BLEND_TEST",2,2,ana_blend_test,
	"BMAP",1,1,ana_bmap,
	"BSMOOTH",2,2,ana_bsmooth,
	"BY0",1,1,ana_y0,
	"BY1",1,1,ana_y1,
	"BYN",2,2,ana_yn,
	"BYTARR",1,8, bytarr,
	"BYTE",1,1,ana_byte,
	"CADD",0,2,ana_cadd,
	"CBRT",1,1,ana_cbrt,
	"CCSDS2FPP",0,0,ana_ccsds2fpp,
	"CCSDSOPEN",1,1,ana_ccsdsopen,
	"CCSDSREAD",0,0,ana_ccsdsread,
	"CDIV",0,2,ana_cdiv,
	"CFITSIO_READ",2,2,ana_cfitsio_read,
	"CMAKE",0,2,ana_cmake,
	"CMUL",0,2,ana_cmul,
	"COMPLEMENT",1,1,ana_complement,
	"COMPRESS",2,3,ana_compress,
	"CONCAT",1,32,ana_concat,
	"CONNECTPORT",2,2,ana_connectport,
	"COS",1,1,ana_cos,
	"COSH",1,1,ana_cosh,
	"CRUNCH",3,3, ana_crunchf,
	"CRUNCHRUN",3,3, ana_crunchrunf,
	"CSUB",0,2,ana_csub,
	"CTERROR",1,1, ana_cterror,
	"CTIME",1,1, ana_ctime,
	"CUBEMEDIAN",1,1, ana_cubemedian,
	"DATE2TAI",5,5, ana_tai_from_date,
	"DATE_FROM_TAI",1,3, ana_date_from_tai,
	"DBLARR",1,8, dblarr,
	"DEFINED",1,1,ana_defined,
	"DESPIKE",1,6,ana_despike,
	"DETREND",1,2,ana_detrend,
	"DILATE",1,1,ana_dilate,
	"DILATE2",1,1,ana_dilate2,
	"DILATE3",1,1,ana_dilate3,
	"DILATE4WAY",1,1,ana_dilate4way,
	"DILATEK",2,2,ana_dilatek,
	"DIMEN",1,2,ana_dimen,
	"DIMEN_SYMARR",3,3,ana_dimen_symarr,
#if NeXT
#else
	"DISKSPACE",1,1,ana_diskspace,
#endif
	"DMAP",1,1,ana_dmap,
#if __APPLE__
	"DOTPRODUCT",2,2,ana_dotproduct,
#endif
	"DOUB",1,1,ana_double,
	"DOUBLE",1,1,ana_double,
	"DSUM",1,1,ana_dsum,
	"DSUMPRODUCT",2,2,ana_dsumproduct,
	"ERF",1,1,ana_erf,
	"ERFC",1,1,ana_erfc,
	"ERODE",1,1,ana_erode,
	"ERODEK",2,2,ana_erodek,
	"EVAL",1,1,ana_eval,
	"EXP",1,1,ana_exp,
	"EXPAND",2,5,ana_expand,
	"EXPM1",1,1,ana_expm1,
	"EXTRACTLINE",3,4, ana_extractline,
	"EXTRACT_BITS",3,3, ana_extract_bits_f,
	"FACTORS",1,1,ana_factors,
	"FCRUNWRITE",2,3, ana_fcrunwrite_f,
	"FCWRITE",2,3, ana_fcwrite_f,
 	"FILEMTIME",1,1, ana_filemtime,
 	"FILESIZE",1,1,ana_filesize,
 	"FINDFILE",2,2, ana_findfile,
 	"FINITE",1,1,ana_finite,
	"FITS_HEADER",1,4,ana_fits_header,
	"FITS_READ",2,6,ana_fits_read,
	"FITS_XREAD",2,6,ana_fits_xread,
	"FIX",1,1,ana_long,
	"FLOAT",1,1,ana_float,
	"FLTARR",1,8, fltarr,
	"FMAP",1,1,ana_fmap,
	"FPPBINRATIO",4,9, ana_fppbinratio,
	"FPPDECOMP",4,4, ana_fppdecomp,
	"FPPGETSTATUSPACKET",0,0, ana_fppgetstatuspacket,
	"FPPMACROREAD",1,1, ana_fppmacroread,
	"FPPOPENSTATUSFILE",1,1, ana_fppopenstatusfile,
	"FPPRATIO",2,3, ana_fppratio,
	"FSTRING",2,2, ana_fstring,
	"FSUM",1,1,ana_total,
	"FZH",1,2, ana_fzhead_f,
	"FZHEAD",1,2, ana_fzhead_f,
	"FZR",2,3, ana_fzread_f,
	"FZREAD",2,3, ana_fzread_f,
	"FZWRITE",2,4, ana_fzwrite_f,
	"GAUSS",1,1,ana_gauss,
 	"GETCURVATURE",1,1, ana_getcurvature,
 	"GETDIRECTORIES",1,2, ana_getdirectories,
 	"GETDIRECTS",1,2, ana_getdirectories,
#if defined(__sgi)
 	"GETDTABLEHI",0,0, ana_getdtablehi,
#endif
	"GETENV",1,1,ana_getenv,
 	"GETFILEEXT",1,1, ana_getfileext,
  	"GETFILEPATH",1,1, ana_getfilepath,
	"GETFILES",1,2, ana_getfiles,
 	"GETFILES_R",1,2, ana_getfiles_r,
 	"GETMATCHEDFILES",2,3, ana_getmatchedfiles,
 	"GETMATCHEDFILES_R",2,3, ana_getmatchedfiles_r,
 	"GETOBJECTPTRS",1,7, ana_getobjectptrs,
 	"GETOBJECTS",1,2, ana_getobjects,
	"GETVERSION",0,0,ana_getversion,
	"GET_LUN",0,0,ana_get_lun,
	"GIF3DAPPEND",1,1, ana_gif3dappend_f,
	"GIF3DOPENW",3,4, ana_gif3dopenw_f,
	"GIFREAD",2,3, ana_gifread_f,
	"GIFWRITE",2,3, ana_gifwrite_f,
	"GMTIME",1,1, ana_gmtime,
	"GREG",4,4,ana_greg,
	"GRIDMATCH",7,7, ana_gridmatch,
	"GRIDMATCH_TELL",8,8, ana_gridmatch_tell,
	"GSMOOTH",2,3,ana_gsmooth,
	"GSMOOTHY",2,3,ana_gsmoothy,
	"HEXSTRING",1,3,ana_hexstring,
	"HIST",1,1,ana_hist,
	"HISTR",1,1,ana_histr,
	"HKEXTRACT",5,5,ana_hkextract,
	"HUFF_DECODE_DCT",6,7, ana_huff_decode_dct,
	"HUFF_ENCODE_DCT",6,6, ana_huff_encode_dct,
	"HYPOT",2,2,ana_hypot,
	"IFSTRING",2,2, ana_ifstring,
	"IMAXTUNE",1,1,ana_imaxtune,
	"INDEX",1,1,ana_index,
	"INDGEN",1,2, ana_indgen,
	"INT",1,1,ana_word,
	"INTARR",1,8, intarr,
	"ISARRAY",1,1, ana_isarray,
	"ISNAN",1,1, ana_isnan,
	"ISSCALAR",1,1, ana_isscalar,
	"ISSTRARR",1,1, ana_isstrarr,
	"ISSTRING",1,1, ana_isstring,
	"ISSYMARR",1,1, ana_issymarr,
	"IST",1,3, ana_istring,
	"ISTRING",1,3, ana_istring,
	"JPEG12_SCAN",7,7, ana_jpeg12_scan,
	"JULIAN1",3,4, ana_julian1,
	"JULIAN2",3,4, ana_julian2,
	"LMAP",1,1,ana_lmap,
	"LOG",1,1,ana_log,
	"LOG10",1,1,ana_log10,
	"LOG1P",1,1,ana_log1p,
	"LONARR",1,8, lonarr,
	"LONG",1,1,ana_long,
	"LOWCASE",1,1,ana_lower,
	"LOWER",1,1,ana_lower,
	"MATMUL",2,2,ana_matmul,
	"MAX",1,1,ana_maxf,
	"MAXLOC",1,1,ana_maxloc,
	"MEAN",1,1,ana_mean,
	"MIN",1,1,ana_minf,
	"MINLOC",1,1,ana_minloc,
	"MKTIME",1,6,ana_mktime,
	"MOTIF_INPUT",1,6, ana_motif_input,
	"NOISE",1,2,ana_noise,
	"NOT",1,1,ana_not,
	"NOTDEFINED",1,1,ana_notdefined,
	"NUM_DIM",1,1,ana_num_dimen,
	"NUM_ELEM",1,1,ana_num_elem,
	"NWHERE",1,1, ana_nwhere,
#if defined(__sgi)
	"OP1CHECK",0,0,ana_op1cmd_check,
	"OP1CMD",1,1,ana_op1cmd_f,
	"OP1READ",0,1,ana_op1read,
	"OP2CHECK",0,0,ana_op2cmd_check,
	"OP2CMD",1,1,ana_op2cmd_f,
	"OP2READ",0,1,ana_op2read,
#else
#endif
	"OPENA",2,2, ana_opena_f,
	"OPENR",2,2, ana_openr_f,
	"OPENU",2,2, ana_openu_f,
	"OPENW",2,2, ana_openw_f,
 	"PATAN",1,1, ana_patan,
 	"PATHSEARCH",2,2, ana_pathsearch,
	"PIT",1,3,ana_pit,
	"POLATE",3,3,ana_polate,
	"POLJ",2,2,ana_polj,
	"POLOUTJ",2,2,ana_poloutj,
	"POLY",2,2,ana_poly,
	"POWER",1,1,ana_power,
	"PPOLJ",3,3,ana_ppolj,
	"PRINTF",2,32, ana_printf_f,
	"QLOG",1,1, ana_qlog,
	"RANDOMN",1,8, ana_randomn,
	"RANDOMU",1,8, ana_randomu,
	"READF",2,32, ana_readf_f,
	"READ_JPEG",2,3, ana_read_jpeg_f,
	"READ_SOCK",2,2, ana_read_sock,
	"READ_TIFF",2,2, ana_tiffread_f,
	"REDIM",2,9, ana_redim_f,
	"REGRID",5,5,ana_regrid,
	"REGRID3",5,5,ana_regrid3,
	"REGRID3NS",5,5,ana_regrid3ns,
 	"REMOVEEXT",1,1, ana_removeext,
 	"REMOVEPATH",1,1, ana_removepath,
	"REORDER",2,2, ana_reorder,
	"RESAMPLE",3,3, ana_resample,
	"RETARDERSCJ",4,4, ana_retarderscj,
	"REVERSE",1,9,ana_reverse,
	"RFIX",1,1,ana_rfix,
	"RHALFWAVEJ",2,2,ana_rhalfwavej,
	"RINT",1,1,ana_rfix,
	"RUNSUM",1,1,ana_runsum,
	"SCALE",1,3,ana_scale,
	"SDEV",1,1,ana_sdev,
	"SHIFT3",3,3,ana_shift3,
	"SHORT",1,1,ana_word,
	"SHORTARR",1,10, intarr,
	"SIEVE",1,2,ana_sieve,
	"SIN",1,1,ana_sin,
	"SINH",1,1,ana_sinh,
	"SKIPC",2,2,ana_skipc,
	"SMAP",1,1,ana_smap,
	"SMOOTH",2,2,ana_smooth,
	"SOBEL",1,3,ana_sobel,
	"SOCK_NBYTE",1,1,ana_sock_nbyte,
	"SORT",1,1,ana_sort,
	"SPRINTF",1,32, ana_sprintf,
	"SQRT",1,1,ana_sqrt,
	"SSCANF",3,32, ana_sscanf,
	"STR",1,1,ana_string,
	"STRARR",1,8, strarr,
	"STRCMP",2,2,ana_strcmp,
	"STRCOUNT",2,2,ana_strcount,
	"STRETCH",2,2,ana_stretch,
	"STRING",1,1,ana_string,
	"STRLEN",1,1,ana_strlen,
	"STRLOC",2,2,ana_strloc,
	"STRPBRK",2,2, ana_strpbrk,
	"STRPOS",2,2,ana_strpos,
	"STRR",3,4,ana_strreplace,
	"STRREPLACE",3,4,ana_strreplace,
	"STRSKP",2,2,ana_strskp,
	"STRSUB",3,3,ana_strsub,
	"STRTOK",2,2,ana_strtok,
	"STRTOL",1,2,ana_strtol,
	"STRTRIM",1,1,ana_strtrim,
	"SUN_B",2,2,ana_sun_b,
	"SUN_B_TAI",1,1,ana_sun_b_tai,
	"SUN_CL",2,2,ana_sun_cl,
	"SUN_CL_TAI",1,1,ana_sun_cl_tai,
	"SUN_D",2,2,ana_sun_d,
	"SUN_D_TAI",1,1,ana_sun_d_tai,
	"SUN_P",2,2,ana_sun_p,
	"SUN_P_TAI",1,1,ana_sun_p_tai,
	"SUN_R",2,2,ana_sun_r,
	"SUN_R_TAI",1,1,ana_sun_r_tai,
	"SYMADDRESS",1,1,ana_symaddress,
	"SYMARR",1,8, symarr,
	"SYMARR_MERGE",2,2, ana_symarr_merge,
	"SYMCLASS",1,1,ana_symclass,
	"SYMDTYPE",1,1,ana_symdtype,
	"SYMNAME",1,1,ana_symname,
	"TAI2DATE",1,3, ana_date_from_tai,
	"TAI_FROM_DATE",5,5, ana_tai_from_date,
	"TAN",1,1,ana_tan,
	"TANH",1,1,ana_tanh,
	"TENSE",3,6,ana_tense,
	"TENSE_CURVE",3,6,ana_tense_curve,
	"TENSE_LOOP",3,4,ana_tense_loop,
	"TESTIMAGEROTATE",5,6,ana_testimagerotate,
	"TFTUNE",3,4,ana_tftune,
 	"THERM2TEMP",1,1, ana_therm2temp,
	"TIFFREAD",2,2, ana_tiffread_f,
	"TIFFWRITE",2,3, ana_tiffwrite_f,
	"TIFFWRITE16",2,3, ana_tiffwrite_f,
	"TIMEGM",1,6,ana_timegm,
	"TOTAL",1,1,ana_total,
	"TRACE_DECODER",3,3, ana_trace_decoder,
	"TRACE_EXPDCT",2,2, ana_trace_expdct,
	"TRACE_EXPDCT2",2,3, ana_trace_expdct2,
/* #ifdef __alpha */
	"TRACE_EXPDCT3",2,3, ana_trace_expdct3,
	"TRACE_EXPDCT4",2,3, ana_trace_expdct4,
	"TRACE_EXPDCT5",2,3, ana_trace_expdct5,
/* #endif */
	"TRACE_FPDCT",2,2, ana_trace_fpdct,
	"TRACE_INTDCT",2,2, ana_trace_intdct,
	"TRACE_RDCT",3,3, ana_trace_rdct,
	"TREND",1,2,ana_trend,
	"TRI_NAME_FROM_TAI",1,1, ana_tri_name_from_tai,
	"TVCONVERT16",3,3, ana_tvconvert16,
	"TVMIX16",5,5, ana_tvmix16,
	"TVMIX16CT",2,4, ana_tvmix16ct,
	"TVRCT",0,1, ana_xtvrct,
	"TVRCT16",1,3, ana_tvrct16,
	"TVREAD",0,5, ana_xtvread,
	"UPCASE",1,1,ana_upper,
	"UPPER",1,1,ana_upper,
	"WHERE",1,1,ana_sieve,
	"WHERECOUNT",1,1, ana_nwhere,
	"WHERENANS",1,1, ana_wherenans,
	"WMAP",1,1,ana_wmap,
	"WORD",1,1,ana_word,
	"WRITE_JPEG",2,3, ana_write_jpeg_f,
	"XEXIST",1,1, ana_xexist,
//	"XEXIST2",1,1, ana_xexist2,
//	"XEXIST3",1,1, ana_xexist3,
	"XGETWINID",0,1, ana_xgetwinid,
	"XLABELWIDTH",1,1, ana_xlabelwidth,
	"XMADDFILETOLIST",2,2, ana_xmaddfiletolist,
	"XMARROW",3,4, ana_xmarrow,
	"XMBOARD",1,5, ana_xmboard,
	"XMBUTTON",3,5, ana_xmbutton,
	"XMCHECKBOX",5,32, ana_xmcheckbox,
	"XMCOLUMNS",3,5, ana_xmcolumns,
	"XMCOMMAND",3,7, ana_xmcommand,
	"XMDIALOG_BOARD",4,9, ana_xmdialog_board,
	"XMDIALOG_FORM",4,8, ana_xmdialog_form,
	"XMDRAWINGAREA",2,7, ana_xmdrawingarea,
	"XMFILEGETLIST",1,1, ana_xmfilegetlist,
	"XMFILESELECT",3,12, ana_xmfileselect,
	"XMFORM",1,7, ana_xmform,
	"XMFRAME",1,5, ana_xmframe,
	"XMGETOPTIONSELECTION",1,1, ana_xmgetoptionselection,
//	"ANA_XMGETTAI",1,1, ana_xmgetTAI,
	"XMGETWIDGETADDRESS",1,1, ana_xmgetwidgetaddress,
	"XMHSCALE",5,8, ana_xmhscale,
	"XMHSCROLLBAR",5,6, ana_xmhscrollbar,
	"XMLABEL",2,5, ana_xmlabel,
	"XMLIST",3,7, ana_xmlist,
	"XMLISTCOUNT",1,1, ana_xmlistcount,
	"XMLISTFROMFILE",4,6, ana_xmlistfromfile,
	"XMMENUBAR",4,32, ana_xmmenubar,
	"XMOPTIONMENU",6,50, ana_xmoptionmenu,
	"XMPIXMAPBUTTON",3,3, ana_xmpixmapbutton,
	"XMPIXMAPOPTIONMENU",6,32, ana_xmpixmapoptionmenu,
	"XMPULLDOWNMENU",6,32, ana_xmpulldownmenu,
	"XMRADIOBOX",5,32, ana_xmradiobox,
	"XMROWS",3,5, ana_xmrows,
	"XMSCALEGETVALUE",1,1, ana_xmscalegetvalue,
	"XMSCROLLEDWINDOW",3,3, ana_xmscrolledwindow,
	"XMSCROLLEDWINDOWAPP",1,4, ana_xmscrolledwindowapp,
	"XMSEPARATOR",1,4, ana_xmseparator,
	"XMTEXT",1,5, ana_xmtext,
	"XMTEXTBOX",1,5, ana_xmtextbox,
	"XMTEXTFIELD",4,8, ana_xmtextfield,
        "XMTEXTFIELDARRAY",9,40, ana_xmtextfieldarray,
	"XMTEXTFIELDGETSTRING",1,1, ana_xmtextfieldgetstring,
	"XMTEXTFROMFILE",2,6, ana_xmtextfromfile,
	"XMTEXTGETINSERTPOSITION",1,1, ana_xmtextgetinsertposition,
	"XMTEXTGETLASTPOSITION",1,1, ana_xmtextgetlastposition,
	"XMTEXTGETSELECTION",1,3, ana_xmtextgetselection,
	"XMTEXTGETSTRING",1,1, ana_xmtextgetstring,
	"XMTOGGLEGETSTATE",1,1, ana_xmtogglegetstate,
	"XMTOPLEVEL_BOARD",3,5, ana_xmtoplevel_board,
	"XMTOPLEVEL_FORM",3,7, ana_xmtoplevel_form,
	"XMVSCALE",5,8, ana_xmvscale,
	"XMVSCROLLBAR",5,6, ana_xmvscrollbar,
	"XQUERY",0,1, ana_xquery_f,
	"XTISMANAGED",1,1, ana_xtismanaged,
	"XTPARENT",1,1, ana_xtparent,
	"XTVRCT",0,1, ana_xtvrct,
	"XTVREAD",0,5, ana_xtvread,
	"XTWINDOW",1,1, ana_xtwindow,
	"XWD",1,1, ana_xwd,
	"XWD_ID",1,1, ana_xwd_id,
	"XWD_ROOT",4,4, ana_xwd_root,
	"ZERO",1,32, ana_zerof,
	"ZOOM",1,2, ana_zoom,
	"[",1,32,ana_concat
	};
 int	num_ana_func = sizeof ( ana_func )/sizeof (struct ana_subr_struct);
 int  next_user_func_num =sizeof(ana_func)/sizeof(struct ana_subr_struct);
 /*------------------------------------------------------------------------- */
int find_subr(s)			/*return subroutine id */
 /*scans both user and system list, if not found, a new user subr name is
 defined and put in list*/
 char *s;
 {
 int	low,high,mid,cond;
 struct user_subr_table *np;
 int	i,hv;
 /* do a binary search in the internal subroutine table */
 low=0;	high=num_ana_subr-1;
 while (low <= high) {
	 mid=(low+high)/2;
	 if ((cond = strcmp(s,ana_subr[mid].name)) < 0) high=mid-1;
	 else if (cond > 0) low=mid+1;
	 else		return mid;
 }
 /*printf("subr name not in internal table\n");*/
 /*check the existing user list */
 for (np=user_subrs[hash(s)]; np != NULL; np = np->next)
  if (strcmp(s,np->name) == 0) return((np->num)); /*found it */
				 /* not in any list, need a new one */
 i = next_user_subr_num++;
 /* printf("new subr, # %d, limit = %d\n", i, MAX_USER_SUBRS); */
 if ( next_user_subr_num >= MAX_USER_SUBRS) parser_error(15);
					   /*space for new link */
 np = (struct user_subr_table *) malloc( sizeof( *np) );
	 if ( np == NULL)  execute_error(23);
 hv = hash(s);
 np->next=user_subrs[hv];
 user_subrs[hv] = np;		/*puts this new one at beginning of chain */
 user_subrs_nf[i - num_ana_subr] = np;
 /*printf("create entry for subr # = %i\n",i);*/
 np->num = i;
 np->name = strsave(s);				/*place for the name */
	 /* note that line entry not done here, available when defined */
 user_subr_ptrs[i - num_ana_subr] = 0;
 return (i);				/*return the brand new one */
 }
 /*------------------------------------------------------------------------- */
int find_block(s)			/*return code block id */
 char *s;
 {
 int	i,hv;
 struct user_subr_table *np;
 for (np=user_block[hash(s)]; np != NULL; np = np->next)
   if (strcmp(s,np->name) == 0) return (np->num);	/*found it */
				 /* not in any list, need a new one */
 /*printf("no match found, creating new code block entry\n"); */
 np=(struct user_subr_table *) malloc(sizeof( *np));  /*space for new link */
 if (!np) return execute_error(23);
 i = next_code_block_num++;
 //printf("new block, # %d, limit = %d\n", i, MAX_USER_BLOCKS);
 if ( next_code_block_num >= MAX_USER_BLOCKS) parser_error(14);
 hv = hash(s);
 user_code_nf[i] = np;
 np->next=user_block[hv];
 user_block[hv] = np;		/*puts this new one at beginning of chain */
 np->num = i;
 np->name = strsave(s);				/*place for the name */
	 /* note that line entry not done here, available when defined */
 user_code_ptrs[i] = 0;
 return i;					/*return the brand new one */
 }
 /*------------------------------------------------------------------------- */
int setup_subr(line,s)			/*setup a new user subroutine*/
 char *s, *line;
 {
 int	i,hv;
 struct user_subr_table *np;
 /* check if already in a list */
 i = find_subr(s);
 /*printf("subr # %d, num_ana_subr = %d\n",i, num_ana_subr);*/
 if ( i < num_ana_subr ) compiler_error(8);	/*unless name illegal */
 i = i - num_ana_subr;	/* drop to user count from overall count */
 np = user_subrs_nf[i];
 if (user_subr_ptrs[i]) { free( np->line ); }
 /* also need to delete all the old symbols */
 zapargs(i+1);				/*context is i+1 for subroutines */
 np->line = strsave(line);			/*place for the line */
 return i;
 }
 /*------------------------------------------------------------------------- */
int setup_block(line,s)			/*setup a new code block*/
 char *s, *line;
 {
 int	i,hv;
 struct user_subr_table *np;
 /*check the existing user list, one will be created if it is new */
 i = find_block(s);
 np = user_code_nf[i];
 if (user_code_ptrs[i])	free( np->line );	/*was previously defined */
 //printf("block name: %s\n",np->name);

 /* don't need to delete any symbols for blocks (they use top level) but
 edb's have to go */
 zapargs(i+1+MAX_USER_SUBRS + MAX_USER_FUNCS);	/*context for blocks */
 np->line = strsave(line);			/*place for the line */
 return i;
 }
 /*------------------------------------------------------------------------- */
int setup_func(line,s)			/*setup a new user function*/
 char *s, *line;
 {
 int	i,hv;
 struct user_subr_table *np;
 /* check if already in a list */
 if ( (i = find_fun(s)) < 0 ) {
				 /* not in any list, need a new one */
 i = next_user_func_num++;
 /* printf("new func, # %d, limit = %d\n", i, MAX_USER_FUNCS); */
 if ( next_user_func_num >= MAX_USER_FUNCS) parser_error(16);
 np=(struct user_subr_table *) malloc(sizeof( *np));  /*space for new link */
 if (!np) return execute_error(23);
 hv = hash(s);
 np->next=user_funcs[hv];
 user_funcs[hv] = np;		/*puts this new one at beginning of chain */
 user_funcs_nf[i - num_ana_func] = np;
 /*printf("function name = %s\n", s);*/
 /*printf("create entry for func # = %i\n",i);*/
 np->num = i;
 np->name = strsave(s);				/*place for the name */
	 /* note that line entry not done here, available when defined */
 user_func_ptrs[i - num_ana_func] = 0;
 }
 /*printf("func # %d, num_ana_func = %d\n",i, num_ana_func);*/
 if ( i < num_ana_func ) compiler_error(9);	/*unless name illegal */
 i = i - num_ana_func;	/* drop to user count from overall count */
 np = user_funcs_nf[i];
 if (user_func_ptrs[i]) { free( np->line ); }
 /* also need to delete all the old symbols */
 zapargs(i+1+MAX_USER_SUBRS);			/*context for functions */
 np->line = strsave(line);			/*place for the line */
 return i;
 }
 /*------------------------------------------------------------------------- */
int find_fun(s)				/*return function id */
 /*scans both user and system list, if not found, return -1 */
 /* this is used by the parser and compiler as well as setup_func */
 char *s;
 {
 int	low,high,mid,cond;
 struct user_subr_table *np;
 int	i,hv;
 /* do a binary search in the internal subroutine table */
 /*printf("function name in find_fun = %s\n", s);*/
 low=0;	high=num_ana_func-1;
 while (low <= high) {
	 mid=(low+high)/2;
	 if ((cond = strcmp(s,ana_func[mid].name)) < 0) high=mid-1;
	 else if (cond > 0) low=mid+1;
	 else		return mid;
 }
 /*printf("func name not in internal table\n");*/
 /*check the existing user list */
 for (np=user_funcs[hash(s)]; np != NULL; np = np->next)
  if (strcmp(s,np->name) == 0) return((np->num)); /*found it */
 /*printf("find_fun couldn't find %s\n", s);*/
 return -1;				/*return-1 to indicate not found */
 }
 /*------------------------------------------------------------------------- */
unsigned short *get_subr_desc(narg, body)
 /* also used for functions and blocks as well as subroutines,
   setup and load a structure with the arguments */
 int narg, body;
{
 int	iq;
 unsigned short	*parg, *ptr;
 ptr = (unsigned short *) malloc( narg*4+6 );
 if (ptr == NULL) execute_error(23);
 /*contents are: recursion trap, # of subr args, sym # of code body,
	 sym #'s of arguments, all are I*2 (short)
	 space for eval'ed passed arguments			*/
 parg = ptr;  *parg++ = 0;  *parg++ = (unsigned short) narg;  *parg++ = (unsigned short) body;
 if (narg>0) {
  for (iq=0;iq<narg;iq++) *parg++ = (unsigned short) pop();
  for (iq=0;iq<narg;iq++) *parg++ = 0;	/*space for runtime eval'ed args */
 }
 return ptr;
 }
 /*------------------------------------------------------------------------- */
int ana_show_subr(narg, ps)
 int narg, ps[];
 {
 int	j, i;
 char	*s, *s2, *re_comp();
 if (narg == 0) {				/* print them all */
 for (j=0;j<num_ana_subr;j++)
 	 { printf("%-20s",ana_subr[j].name); if (j%4 == 3) printf("\n");}
 if ( num_ana_subr % 4 != 0 ) printf("\n");
 return 1;
 } else {					/*any with arg as substring*/
 if ( sym[ ps[0] ].class != 2 ) return execute_error(70);
 s = (char *) sym[ps[0] ].spec.array.ptr;
 s2 = strsave(s);
 s = s2;
 while (*s2) {*s2 =  toupper( (int) *s2);  s2++; }
 s2 = s;
 i = 0;
#if defined(__alpha) | defined(LINUX) | defined(SOLARIS)
 if (regcomp(&re,s2,REG_NOSUB) ) {
#else
 if (regcomp(&re,s2,REG_BASIC|REG_NOSUB) ) {
#endif
 //if (s = re_comp(s2) ) {
 	printf("SHOW_SUB - error in regular expression for match: %s\n", s2);
        regfree(&re);
	return -1; }
 for (j=0;j<num_ana_subr;j++) {
	 //if ( re_exec(ana_subr[j].name) == 1)
  	 if(regexec(&re, ana_subr[j].name,0,NULL,0)==0)
	 { printf("%-20s",ana_subr[j].name); if (i%4 == 3) printf("\n"); i++;}  
 }
 free(s);
 if (i == 0) printf("no subroutines containing string were found\n");
 if (i % 4 != 0 ) printf("\n");
 regfree(&re);
 return 1;
 }
 }
 /*------------------------------------------------------------------------- */
int ana_show_func(narg, ps)
 int narg, ps[];
 {
 int	j, i;
 char	*s, *s2, *re_comp();
 if (narg == 0) {				/* print them all */
 for (j=0;j<num_ana_func;j++)
	 { printf("%-20s",ana_func[j].name); if (j%4 == 3) printf("\n");}
 if ( num_ana_func % 4 != 0 ) printf("\n");
 return 1;
 } else {					/*any with arg as substring*/
 if ( sym[ ps[0] ].class != 2 ) return execute_error(70);
 s = (char *) sym[ps[0] ].spec.array.ptr;
 s2 = strsave(s);
 s = s2;
 while (*s2) {*s2 =  toupper( (int) *s2);  s2++; }
 s2 = s;
 i = 0;
#if defined(__alpha) | defined(LINUX) | defined(SOLARIS)
 if (regcomp(&re,s2,REG_NOSUB) ) {
#else
 if (regcomp(&re,s2,REG_BASIC|REG_NOSUB) ) {
#endif
 //if (s = re_comp(s2) ) {
 	printf("SHOW_FUN - error in regular expression for match: %s\n", s);
        regfree(&re);
	return -1; }
 for (j=0;j<num_ana_func;j++) {
	 //if ( re_exec(ana_func[j].name) == 1)
  	 if(regexec(&re, ana_func[j].name,0,NULL,0)==0)
	 { printf("%-20s",ana_func[j].name); if (i%4 == 3) printf("\n"); i++;}  
 }
 free(s);
 if (i == 0) printf("no functions containing string were found\n");
 if (i % 4 != 0 ) printf("\n");
 regfree(&re);
 return 1;
 }
 }
  /*------------------------------------------------------------------------- */
int ana_dump_subr(narg,ps)
 /*dump all the symbols associated with a subroutine */
 int narg,ps[];
 {
 int	i, j, nsubs, context;
 struct	sym_list	*ns;
 if (narg == 0) {	/*if no argument, dump them all */
 nsubs = next_user_subr_num - num_ana_subr;
 if ( nsubs <= 0 ) { printf("no user subroutines\n"); return 1; }
 printf("first and last #'s are %d  %d\n", num_ana_subr, next_user_subr_num-1);
 for (i=1; i <= nsubs; i++ ) {
 /* get context for this subroutine */
 context = i;
 printf("subroutine %s, context = %d\n",user_subrs_nf[i-1]->name, context);
 /* check the chain of symbols */
 ns = subr_sym_list[context];
 while ( ns != NULL) { j = ns->num;  ns = ns->next;
 /* dump just the variable symbols, not code */
 if ( j < edb2_base) ana_dump( 1, &j ); }
 }
 }
 return 1;
 }
 /*------------------------------------------------------------------------- */
int ana_dump_func(narg,ps)
 /*dump all the symbols associated with a function */
 int narg,ps[];
 {
 int	i, j, nsubs, context;
 struct	sym_list	*ns;
 if (narg == 0) {	/*if no argument, dump them all */
 nsubs = next_user_func_num - num_ana_func;
 if ( nsubs <= 0 ) { printf("no user functions\n"); return 1; }
 printf("first and last #'s are %d  %d\n", num_ana_func, next_user_func_num-1);
 for (i=1; i <= nsubs; i++ ) {
 /* get context for this subroutine */
 context = i+MAX_USER_SUBRS;
 printf("function %s, context = %d\n",user_funcs_nf[i-1]->name, context);
 /* check the chain of symbols */
 ns = subr_sym_list[context];
 while ( ns != NULL) { j = ns->num;  ns = ns->next;
 /* dump just the variable symbols, not code */
 if ( j < edb2_base) ana_dump( 1, &j ); }
 }
 }
 return 1;
 }
