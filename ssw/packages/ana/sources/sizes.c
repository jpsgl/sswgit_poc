#include <stdio.h>
#include <stdlib.h>
#include <sys/resource.h>;
typedef unsigned char byte;

main()
{
  struct rlimit rlp;
  int	iq;
  char	*mq1, *mq2;
  size_t	msize;
  iq = getrlimit(RLIMIT_NOFILE, &rlp);
  printf("iq = %d\n", iq);
  printf("rlp = %d\n",rlp);
  printf("file number limits %d %d\n", rlp.rlim_cur, rlp.rlim_max);

  printf("sizes of byte, short, int, long and long long  are:  %d,%d,%d,%d,%d\n",
    sizeof(byte),sizeof(short),sizeof(int),sizeof(long),sizeof(long long));
  printf("sizes of long int, float, double, long double are:  %d,%d,%d,%d\n",
    sizeof(long int),sizeof(float),sizeof(double),sizeof(long double));
  printf("pointer size = %d\n", sizeof( byte *));
  printf("size_t size = %d\n", sizeof( size_t));
  
  msize = (long) 1024*1024*1000*8;
  printf("msize = %ld,  %#lx\n", msize, msize);
  mq1 = (char *) malloc(msize);
  printf("mq1 address %#lx\n", mq1);
  msize = (long) 1024*1024*1000*4;
  //mq2 = (char *) malloc(1024*1024*1000*4);
  mq2 = (char *) malloc(msize);
  printf("mq2 address %#lx\n", mq2);

}
