#include <stdio.h>
#include <stdlib.h>
#include <sys/resource.h>;
typedef unsigned char byte;

main()
{
  char	*mq1, *mq2;
  long	msize;

  printf("sizes of byte, short, int, long and long long  are:  %d,%d,%d,%d,%d\n",
    sizeof(byte),sizeof(short),sizeof(int),sizeof(long),sizeof(long long));
  printf("sizes of long int, float, double, long double are:  %d,%d,%d,%d\n",
    sizeof(long int),sizeof(float),sizeof(double),sizeof(long double));
  printf("pointer size = %d\n", sizeof( byte *));
  printf("size_t size = %d\n", sizeof( size_t));
  
  msize = 1024*1024*1000*4;
  mq1 = (char *) malloc(msize);
  printf("mq1 address %#lx\n", mq1);
  mq2 = (char *) malloc(1024*1024*1000*2);
  printf("mq2 address %#lx\n", mq2);

}
