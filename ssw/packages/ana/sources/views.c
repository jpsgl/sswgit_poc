/* for graphview and imageview facilities in ana */
#include <varargs.h>
#include <stdlib.h>     /* allows use of malloc(3C) */
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>
#include <X11/cursorfont.h>
#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <Xm/MwmUtil.h>
#include <Xm/Xm.h>
#include <Xm/PushB.h>
#include <Xm/PushBG.h>
#include <Xm/Command.h>
#include <Xm/CascadeB.h>
#include <Xm/CascadeBG.h>
#include <Xm/Label.h>
#include <Xm/LabelG.h>
#include <Xm/MessageB.h>
#include <Xm/List.h>
#include <Xm/Form.h>
#include <Xm/Text.h>
#include <Xm/Scale.h>
#include <Xm/ScrolledW.h>
#include <Xm/ScrollBar.h>
#include <Xm/DrawingA.h>
#include <Xm/Frame.h>
#include <Xm/RowColumn.h>
#include <Xm/TextF.h>
#include <Xm/ArrowB.h>
#include <Xm/ToggleBG.h>
#include <Xm/ToggleB.h>
#include <Xm/SelectioB.h>
#include <Xm/Separator.h>
#include <Xm/FileSB.h>
#include <stdio.h>
#include <sys/stat.h>
/* #include <bstring.h> */
#include <sys/types.h>
#include <malloc.h>
#include <time.h>
 char *font1 = {"-adobe-times-bold-r-normal--14*"};
 char *font3 = {"-schumacher-clean-bold-r-normal--13*"};
 char *font4 = {"-adobe-helvetica-bold-r-normal--14*"};
 char *font10 = {"-adobe-courier-bold-r-normal--12*"};
 typedef	unsigned char byte;
 typedef struct	Warray {
   int		n;
   Widget	*array;} Warray;
 char *barlabels[] = {"File", "Display", "Options","Help"};
 /*------------------------------------------------------------------------- */
int ana_graphview(int narg, int ps[])
 {
 int	iq, i;
 int	nx, ny;
 char	*s, *p, *title;
 Widget	wg, graphviewtop;
 Warray *bar;           /* wdiget array for the menu bar */
 nx = 512;	ny = 512;
 /* the title is the first arg */
 iq = ps[0];
 if (sym[iq].class != 2) { return execute_error(70); }
 title = (char *) sym[iq].spec.array.ptr;
 
 idrawtop = xmtoplevel_form(nx, ny, title, 0,0,0,0);

 /* 4 args for the callback and labels for menu items  */
 bar = xmmenubar(idrawtop, font4, "gray", barlabels, 4);
 
 return 1;
 }
 /*------------------------------------------------------------------------- */
