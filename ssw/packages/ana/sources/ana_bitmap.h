#define icon_bitmap_width 50
#define icon_bitmap_height 50
static  char icon_bitmap_bits[] = {
   0x00, 0xfc, 0x00, 0x00, 0x00, 0x00, 0x00, 0xe0, 0x83, 0x03, 0x00, 0x00,
   0x00, 0x00, 0x30, 0x00, 0x0e, 0x00, 0x00, 0x00, 0x00, 0x18, 0x00, 0x18,
   0x00, 0x00, 0x00, 0x00, 0x0c, 0x00, 0x70, 0x00, 0x03, 0x00, 0x00, 0xf6,
   0x01, 0xc0, 0x80, 0x03, 0xc0, 0x00, 0x1f, 0x1f, 0x80, 0xc1, 0x02, 0xe0,
   0x00, 0x07, 0xf0, 0x00, 0x32, 0x02, 0x70, 0x00, 0x00, 0x00, 0x03, 0x1e,
   0x02, 0x78, 0x00, 0x80, 0x01, 0xf2, 0x1f, 0x02, 0x2e, 0x00, 0x80, 0x0f,
   0x0c, 0x60, 0x82, 0x33, 0x00, 0x00, 0xf1, 0xe3, 0x8f, 0xff, 0x10, 0x00,
   0x00, 0x81, 0xfc, 0x7f, 0x0e, 0x08, 0x00, 0x00, 0x41, 0xfe, 0xff, 0x04,
   0x04, 0x00, 0x00, 0x21, 0xff, 0xff, 0x09, 0x06, 0x00, 0x00, 0x93, 0xff,
   0xff, 0x13, 0x03, 0x00, 0x00, 0xd2, 0xff, 0xff, 0x97, 0x01, 0x00, 0x00,
   0xec, 0xff, 0xff, 0xef, 0x00, 0x00, 0x00, 0xec, 0xff, 0xff, 0x6f, 0x00,
   0x00, 0x80, 0xe7, 0xff, 0xff, 0xcf, 0x01, 0x00, 0x70, 0xf4, 0xff, 0xff,
   0x5f, 0x07, 0x00, 0x1e, 0xf4, 0x39, 0x77, 0x5e, 0x38, 0x00, 0x0e, 0xf4,
   0xb6, 0xb6, 0x5d, 0xc0, 0x01, 0x38, 0xf4, 0xb6, 0xb6, 0x5d, 0xc0, 0x03,
   0xc0, 0x75, 0xaf, 0xd5, 0xdb, 0x3f, 0x00, 0x00, 0x77, 0xa0, 0x15, 0xd8,
   0x01, 0x00, 0x00, 0x74, 0xaf, 0xd3, 0x5b, 0x03, 0x00, 0x00, 0x67, 0xaf,
   0xd3, 0x4b, 0x0c, 0x00, 0xc0, 0x68, 0xaf, 0xd7, 0x2b, 0x38, 0x00, 0x60,
   0xe8, 0xff, 0xff, 0x2f, 0xe0, 0x00, 0x38, 0xd0, 0xff, 0xff, 0x17, 0x00,
   0x03, 0x0c, 0x90, 0xff, 0xff, 0xf3, 0xff, 0x01, 0x06, 0x30, 0xff, 0xff,
   0x19, 0x00, 0x00, 0x03, 0x5c, 0xfe, 0xff, 0x34, 0x00, 0x00, 0x01, 0xe7,
   0xfc, 0x7f, 0x62, 0x00, 0x00, 0xf1, 0x21, 0xe3, 0x8f, 0x81, 0x00, 0x00,
   0x0f, 0x20, 0x0c, 0x60, 0x80, 0x01, 0x00, 0x00, 0x10, 0xf0, 0xff, 0x01,
   0x01, 0x00, 0x00, 0x10, 0xc0, 0x61, 0x1f, 0x01, 0x00, 0x00, 0x10, 0x60,
   0x41, 0xe0, 0x01, 0x00, 0x00, 0x10, 0x30, 0x41, 0x00, 0x00, 0x00, 0x00,
   0x10, 0x1c, 0x82, 0x00, 0x00, 0x00, 0x00, 0x18, 0x06, 0x86, 0x00, 0x00,
   0x00, 0x00, 0x08, 0x03, 0x8c, 0x01, 0x00, 0x00, 0x00, 0x88, 0x01, 0x18,
   0x01, 0x00, 0x00, 0x00, 0x68, 0x00, 0xe0, 0x03, 0x00, 0x00, 0x00, 0x18,
   0x00, 0x80, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00};
#define back_width 32
#define back_height 32
static  char back_bits[] = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x98, 0x00, 0x00, 0x00, 0x50, 0x00, 0x00,
   0x00, 0x60, 0x00, 0x00, 0x00, 0x70, 0x00, 0x04, 0x00, 0x4c, 0x00, 0x04,
   0x00, 0x80, 0x80, 0x04, 0x00, 0x04, 0x81, 0x05, 0x00, 0x02, 0x06, 0x03,
   0x00, 0x02, 0x0c, 0x0e, 0x00, 0x02, 0x00, 0x1b, 0x00, 0xf2, 0x80, 0x31,
   0x00, 0x8e, 0x11, 0x60, 0xc0, 0x80, 0x11, 0xc0, 0x80, 0xc3, 0x80, 0x00,
   0x00, 0x66, 0x00, 0x03, 0x22, 0x04, 0x00, 0x0e, 0x66, 0x04, 0x00, 0x18,
   0x5c, 0x04, 0x00, 0x60, 0xd0, 0x07, 0x00, 0x00, 0x20, 0x02, 0xe0, 0x00,
   0x60, 0x00, 0x30, 0x01, 0x40, 0x00, 0x10, 0x02, 0x4c, 0x60, 0x10, 0x02,
   0x78, 0x80, 0x11, 0x02, 0x00, 0x00, 0x16, 0x02, 0x00, 0x00, 0xe7, 0x03,
   0x00, 0x00, 0x09, 0x00, 0x00, 0x80, 0x19, 0x00, 0x00, 0x80, 0x10, 0x00,
   0x00, 0x80, 0x31, 0x00, 0x00, 0x00, 0x1f, 0x00};
