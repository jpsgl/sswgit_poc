/* test code for expanding fake FPP imagbes */
 /* use maginifcation of 8 */
 int	sx, sy, q, p;
 sx = sy = 8;
 nxfake = 4096/sx;
 nyfake = 2048/sy;
 out = scratch;
 /* starting values of z00, etc */
 itop = (ix0+sx/2)/sx;
 jtop = (iy0+sy/2)/sy;
 ibot = itop - 1;
 jbot = jtop - 1;
 itop = MIN(itop, nxfake-1);
 jtop = MIN(jtop, nyfake-1);
 ibot = MAX(0, itop);
 jbot = MAX(0, jtop);
 z00 = *(fake + ibot + jbot * nxfake);
 z10 = *(fake + itop + jbot * nxfake);
 z01 = *(fake + ibot + jtop * nxfake);
 z11 = *(fake + itop + jtop * nxfake);
 bfake1 = fake + ibot + jbot * nxfake;
 bfake2 = fake + ibot + jtop * nxfake;
 /* starting weights in j direction */
 q = ( (iy0+4)%8)*2 + 1;	/* assumes x 8 */
 while (ms--) {				        /* y loop */
  bfake = fake + ibot + jbot * nxfake;
  ns2 = ns;
  while (ns2--) {				/* x loop */
    p = p + 2;
    if (p > 16) { p = 1;
    z00 = z10;	z01 = z11;
    z10 = *bfake1++;
    z11 = *bfake2++;
    zc1 = z10 - z00;  zc2 = (z11 - z01 - zc1) * q + 16 * zc1;
    zc1 = (z01 - z00) * q + 16 * z00;
    }


    *out++ = (p *zc2 + zc1) >> 8;
