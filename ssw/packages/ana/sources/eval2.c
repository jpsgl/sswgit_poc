/*eval2 module, contains relational and logical ops*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include  <sys/types.h>
#include "ana_structures.h"
#define get_edb_ptr(ns) \
narg = (int) sym[nsym].type;\
if (narg<5) ap= &sym[ns].spec.evb.args[0];\
else ap = (unsigned short *) sym[ns].spec.array.ptr;
extern	struct sym_desc sym[];
extern	int	execute_error( int n );
extern	int	temp_base;
extern	struct ana_subr_struct ana_func[]; 
extern	int	bsym1, bsym2;			/*symbol #'s for binary ops */
extern	int	result_sym;			/*output symbol */
extern	int	type1, type2, nelem;
extern	int	(*bin_ops[])();
extern	int	(*bin_ops_s_a[])();
extern	int	(*bin_ops_a_s[])();
extern	union
	types_ptr { byte *b; short *w; int *l; float *f; double *d;} p1,p2,p3;		
extern	unsigned short	*ap, *aps;
/*------------------------------------------------------------------------- */
/*relationals, 6 operations and 2 functions defined for each one
all return a long word with 0 or 1 values (a bit of waste of space perhaps)
and all combinations of input types are accepted via many switch statements */
/*------------------------------------------------------------------------- */
int ana_eq()
/*equality relation eq */
{
register union	types_ptr q1,q2,q3;
register int n;
q1.l = p1.l;	q2.l = p2.l;	q3.l = p3.l;	n = nelem;
switch (type1)	{
case 3:	switch (type2)	{
 case 3: while (n) {*q3.l++ =(*q1.f++) == (*q2.f++);n--; }break;
 case 2: while (n) {*q3.l++ =(*q1.f++) == (float)(*q2.l++);n--;}break;
 case 1: while (n) {*q3.l++ =(*q1.f++) == (float)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.f++) == (float)(*q2.b++);n--;}break;
 case 4: while (n) {*q3.l++ =(double)(*q1.f++) == (*q2.d++);n--;}break;
}	break;
case 2:	switch (type2)	{
 case 2: while (n) {*q3.l++ =(*q1.l++) == (*q2.l++);n--;}break;
 case 3: while (n) {*q3.l++ =(float)(*q1.l++) == (*q2.f++);n--;}break;
 case 1: while (n) {*q3.l++ =(*q1.l++) == (int)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.l++) == (int)(*q2.b++);n--;}break;
 case 4: while (n) {*q3.l++ =(double)(*q1.l++) == (*q2.d++);n--;}break;
}	break;
case 1:	switch (type2)	{
 case 1: while (n) {*q3.l++ =(*q1.w++) == (*q2.w++);n--;}break;
 case 3: while (n) {*q3.l++ =(float)(*q1.w++) == (*q2.f++);n--;}break;
 case 2: while (n) {*q3.l++ =(int)(*q1.w++) == (*q2.l++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.w++) == (short)(*q2.b++);n--;}break;
 case 4: while (n) {*q3.l++ =(double)(*q1.w++) == (*q2.d++);n--;}break;
}	break;
case 0:	switch (type2)	{
 case 0: while (n) {*q3.l++ =(*q1.b++) == (*q2.b++);n--;}break;
 case 1: while (n) {*q3.l++ =(short)(*q1.b++) == (*q2.w++);n--;}break;
 case 3: while (n) {*q3.l++ =(float)(*q1.b++) == (*q2.f++);n--;}break;
 case 2: while (n) {*q3.l++ =(int)(*q1.b++) == (*q2.l++);n--;}break;
 case 4: while (n) {*q3.l++ =(double)(*q1.b++) == (*q2.d++);n--;}break;
}	break;
case 4:	switch (type2)	{
 case 4: while (n) {*q3.l++ =(*q1.d++) == (*q2.d++);n--;}break;
 case 3: while (n) {*q3.l++ =(*q1.d++) == (double)(*q2.f++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.d++) == (double)(*q2.b++);n--;}break;
 case 1: while (n) {*q3.l++ =(*q1.d++) == (double)(*q2.w++);n--;}break;
 case 2: while (n) {*q3.l++ =(*q1.d++) == (double)(*q2.l++);n--;}break;
}	break;
}
return result_sym;
}
/*------------------------------------------------------------------------- */
int ana_eq_s_a()
/*equality relation eq for scalars/array case */
{
register union	types_ptr q2,q3;
register union	scalar x;
register int n;
q2.l = p2.l;	q3.l = p3.l;	n = nelem;
switch (type1)	{
case 3:  x.f = *p1.f;
 switch (type2)	{
 case 3: while (n) {*q3.l++ =(x.f) == (*q2.f++);n--; }break;
 case 2: while (n) {*q3.l++ =(x.f) == (float)(*q2.l++);n--;}break;
 case 1: while (n) {*q3.l++ =(x.f) == (float)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.f) == (float)(*q2.b++);n--;}break;
 case 4: x.d = (double) x.f; while (n) {*q3.l++ =(x.d) == (*q2.d++);n--;}break;
}	break;
case 2:	x.l = *p1.l;
 switch (type2)	{
 case 2: while (n) {*q3.l++ =(x.l) == (*q2.l++);n--;}break;
 case 3: x.f=(float)(x.l); while (n) {*q3.l++ =(x.f) == (*q2.f++);n--;}break;
 case 1: while (n) {*q3.l++ =(x.l) == (int)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.l) == (int)(*q2.b++);n--;}break;
 case 4: x.d=(double)(x.l); while (n) {*q3.l++ =(x.d) == (*q2.d++);n--;}break;
}	break;
case 1:	x.w = *p1.w;
 switch (type2)	{
 case 1: while (n) {*q3.l++ =(x.w) == (*q2.w++);n--;}break;
 case 3: x.f=(float)x.w; while (n) {*q3.l++ =(x.f) == (*q2.f++);n--;}break;
 case 2: x.l=(int)x.w; while (n) {*q3.l++ =(x.l) == (*q2.l++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.w) == (short)(*q2.b++);n--;}break;
 case 4: x.d=(double)x.w; while (n) {*q3.l++ =(x.d) == (*q2.d++);n--;}break;
}	break;
case 0:	x.b = *p1.b;
switch (type2)	{
 case 0: while (n) {*q3.l++ =(x.b) == (*q2.b++);n--;}break;
 case 1:  x.w=(short)x.b; while (n) {*q3.l++ =(x.w) == (*q2.w++);n--;}break;
 case 3: x.f=(float)x.b; while (n) {*q3.l++ =(x.f) == (*q2.f++);n--;}break;
 case 2: x.l=(int)x.b; while (n) {*q3.l++ =(x.l) == (*q2.l++);n--;}break;
 case 4: x.d=(double)x.b; while (n) {*q3.l++ =(x.d) == (*q2.d++);n--;}break;
}	break;
case 4:	x.d = *p1.d;
switch (type2)	{
 case 4: while (n) {*q3.l++ =(x.d) == (*q2.d++);n--;}break;
 case 3: while (n) {*q3.l++ =(x.d) == (double)(*q2.f++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.d) == (double)(*q2.b++);n--;}break;
 case 1: while (n) {*q3.l++ =(x.d) == (double)(*q2.w++);n--;}break;
 case 2: while (n) {*q3.l++ =(x.d) == (double)(*q2.l++);n--;}break;
}	break;
}
return result_sym;
}
/*------------------------------------------------------------------------- */
int ana_ne()
/*non-equality relation ne */
{
register union	types_ptr q1,q2,q3;
register int n;
q1.l = p1.l;	q2.l = p2.l;	q3.l = p3.l;	n = nelem;
switch (type1)	{
case 3:	switch (type2)	{
 case 3: while (n) {*q3.l++ =(*q1.f++)!=(*q2.f++);n--; }break;
 case 2: while (n) {*q3.l++ =(*q1.f++)!=(float)(*q2.l++);n--;}break;
 case 1: while (n) {*q3.l++ =(*q1.f++)!=(float)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.f++)!=(float)(*q2.b++);n--;}break;
 case 4: while (n) {*q3.l++ =(double)(*q1.f++)!=(*q2.d++);n--;}break;
}	break;
case 2:	switch (type2)	{
 case 2: while (n) {*q3.l++ =(*q1.l++)!=(*q2.l++);n--;}break;
 case 3: while (n) {*q3.l++ =(float)(*q1.l++)!=(*q2.f++);n--;}break;
 case 1: while (n) {*q3.l++ =(*q1.l++)!=(int)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.l++)!=(int)(*q2.b++);n--;}break;
 case 4: while (n) {*q3.l++ =(double)(*q1.l++)!=(*q2.d++);n--;}break;
}	break;
case 1:	switch (type2)	{
 case 1: while (n) {*q3.l++ =(*q1.w++)!=(*q2.w++);n--;}break;
 case 3: while (n) {*q3.l++ =(float)(*q1.w++)!=(*q2.f++);n--;}break;
 case 2: while (n) {*q3.l++ =(int)(*q1.w++)!=(*q2.l++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.w++)!=(short)(*q2.b++);n--;}break;
 case 4: while (n) {*q3.l++ =(double)(*q1.w++)!=(*q2.d++);n--;}break;
}	break;
case 0:	switch (type2)	{
 case 0: while (n) {*q3.l++ =(*q1.b++)!=(*q2.b++);n--;}break;
 case 1: while (n) {*q3.l++ =(short)(*q1.b++)!=(*q2.w++);n--;}break;
 case 3: while (n) {*q3.l++ =(float)(*q1.b++)!=(*q2.f++);n--;}break;
 case 2: while (n) {*q3.l++ =(int)(*q1.b++)!=(*q2.l++);n--;}break;
 case 4: while (n) {*q3.l++ =(double)(*q1.b++)!=(*q2.d++);n--;}break;
}	break;
case 4:	switch (type2)	{
 case 4: while (n) {*q3.l++ =(*q1.d++)!=(*q2.d++);n--;}break;
 case 3: while (n) {*q3.l++ =(*q1.d++)!=(double)(*q2.f++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.d++)!=(double)(*q2.b++);n--;}break;
 case 1: while (n) {*q3.l++ =(*q1.d++)!=(double)(*q2.w++);n--;}break;
 case 2: while (n) {*q3.l++ =(*q1.d++)!=(double)(*q2.l++);n--;}break;
}	break;
}
return result_sym;
}
/*------------------------------------------------------------------------- */
int ana_ne_s_a()
/*non-equality relation ne for scalars/array case */
{
register union	types_ptr q2,q3;
register union	scalar x;
register int n;
q2.l = p2.l;	q3.l = p3.l;	n = nelem;
switch (type1)	{
case 3:  x.f = *p1.f;
 switch (type2)	{
 case 3: while (n) {*q3.l++ =(x.f)!=(*q2.f++);n--; }break;
 case 2: while (n) {*q3.l++ =(x.f)!=(float)(*q2.l++);n--;}break;
 case 1: while (n) {*q3.l++ =(x.f)!=(float)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.f)!=(float)(*q2.b++);n--;}break;
 case 4: x.d = (double) x.f; while (n) {*q3.l++ =(x.d)!=(*q2.d++);n--;}break;
}	break;
case 2:	x.l = *p1.l;
 switch (type2)	{
 case 2: while (n) {*q3.l++ =(x.l)!=(*q2.l++);n--;}break;
 case 3: x.f=(float)(x.l); while (n) {*q3.l++ =(x.f)!=(*q2.f++);n--;}break;
 case 1: while (n) {*q3.l++ =(x.l)!=(int)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.l)!=(int)(*q2.b++);n--;}break;
 case 4: x.d=(double)(x.l); while (n) {*q3.l++ =(x.d)!=(*q2.d++);n--;}break;
}	break;
case 1:	x.w = *p1.w;
 switch (type2)	{
 case 1: while (n) {*q3.l++ =(x.w)!=(*q2.w++);n--;}break;
 case 3: x.f=(float)x.w; while (n) {*q3.l++ =(x.f)!=(*q2.f++);n--;}break;
 case 2: x.l=(int)x.w; while (n) {*q3.l++ =(x.l)!=(*q2.l++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.w)!=(short)(*q2.b++);n--;}break;
 case 4: x.d=(double)x.w; while (n) {*q3.l++ =(x.d)!=(*q2.d++);n--;}break;
}	break;
case 0:	x.b = *p1.b;
switch (type2)	{
 case 0: while (n) {*q3.l++ =(x.b)!=(*q2.b++);n--;}break;
 case 1:  x.w=(short)x.b; while (n) {*q3.l++ =(x.w)!=(*q2.w++);n--;}break;
 case 3: x.f=(float)x.b; while (n) {*q3.l++ =(x.f)!=(*q2.f++);n--;}break;
 case 2: x.l=(int)x.b; while (n) {*q3.l++ =(x.l)!=(*q2.l++);n--;}break;
 case 4: x.d=(double)x.b; while (n) {*q3.l++ =(x.d)!=(*q2.d++);n--;}break;
}	break;
case 4:	x.d = *p1.d;
switch (type2)	{
 case 4: while (n) {*q3.l++ =(x.d)!=(*q2.d++);n--;}break;
 case 3: while (n) {*q3.l++ =(x.d)!=(double)(*q2.f++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.d)!=(double)(*q2.b++);n--;}break;
 case 1: while (n) {*q3.l++ =(x.d)!=(double)(*q2.w++);n--;}break;
 case 2: while (n) {*q3.l++ =(x.d)!=(double)(*q2.l++);n--;}break;
}	break;
}
return result_sym;
}
/*------------------------------------------------------------------------- */
int ana_gt()
/*relation gt */
{
register union	types_ptr q1,q2,q3;
register int n;
q1.l = p1.l;	q2.l = p2.l;	q3.l = p3.l;	n = nelem;
switch (type1)	{
case 3:	switch (type2)	{
 case 3: while (n) {*q3.l++ =(*q1.f++)>(*q2.f++);n--; }break;
 case 2: while (n) {*q3.l++ =(*q1.f++)>(float)(*q2.l++);n--;}break;
 case 1: while (n) {*q3.l++ =(*q1.f++)>(float)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.f++)>(float)(*q2.b++);n--;}break;
 case 4: while (n) {*q3.l++ =(double)(*q1.f++)>(*q2.d++);n--;}break;
}	break;
case 2:	switch (type2)	{
 case 2: while (n) {*q3.l++ =(*q1.l++)>(*q2.l++);n--;}break;
 case 3: while (n) {*q3.l++ =(float)(*q1.l++)>(*q2.f++);n--;}break;
 case 1: while (n) {*q3.l++ =(*q1.l++)>(int)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.l++)>(int)(*q2.b++);n--;}break;
 case 4: while (n) {*q3.l++ =(double)(*q1.l++)>(*q2.d++);n--;}break;
}	break;
case 1:	switch (type2)	{
 case 1: while (n) {*q3.l++ =(*q1.w++)>(*q2.w++);n--;}break;
 case 3: while (n) {*q3.l++ =(float)(*q1.w++)>(*q2.f++);n--;}break;
 case 2: while (n) {*q3.l++ =(int)(*q1.w++)>(*q2.l++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.w++)>(short)(*q2.b++);n--;}break;
 case 4: while (n) {*q3.l++ =(double)(*q1.w++)>(*q2.d++);n--;}break;
}	break;
case 0:	switch (type2)	{
 case 0: while (n) {*q3.l++ =(*q1.b++)>(*q2.b++);n--;}break;
 case 1: while (n) {*q3.l++ =(short)(*q1.b++)>(*q2.w++);n--;}break;
 case 3: while (n) {*q3.l++ =(float)(*q1.b++)>(*q2.f++);n--;}break;
 case 2: while (n) {*q3.l++ =(int)(*q1.b++)>(*q2.l++);n--;}break;
 case 4: while (n) {*q3.l++ =(double)(*q1.b++)>(*q2.d++);n--;}break;
}	break;
case 4:	switch (type2)	{
 case 4: while (n) {*q3.l++ =(*q1.d++)>(*q2.d++);n--;}break;
 case 3: while (n) {*q3.l++ =(*q1.d++)>(double)(*q2.f++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.d++)>(double)(*q2.b++);n--;}break;
 case 1: while (n) {*q3.l++ =(*q1.d++)>(double)(*q2.w++);n--;}break;
 case 2: while (n) {*q3.l++ =(*q1.d++)>(double)(*q2.l++);n--;}break;
}	break;
}
return result_sym;
}
/*------------------------------------------------------------------------- */
int ana_gt_s_a()
/*relation gt for scalars/array case */
{
register union	types_ptr q2,q3;
register union	scalar x;
register int n;
q2.l = p2.l;	q3.l = p3.l;	n = nelem;
switch (type1)	{
case 3:  x.f = *p1.f;
 switch (type2)	{
 case 3: while (n) {*q3.l++ =(x.f)>(*q2.f++);n--; }break;
 case 2: while (n) {*q3.l++ =(x.f)>(float)(*q2.l++);n--;}break;
 case 1: while (n) {*q3.l++ =(x.f)>(float)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.f)>(float)(*q2.b++);n--;}break;
 case 4: x.d = (double) x.f; while (n) {*q3.l++ =(x.d)>(*q2.d++);n--;}break;
}	break;
case 2:	x.l = *p1.l;
 switch (type2)	{
 case 2: while (n) {*q3.l++ =(x.l)>(*q2.l++);n--;}break;
 case 3: x.f=(float)(x.l); while (n) {*q3.l++ =(x.f)>(*q2.f++);n--;}break;
 case 1: while (n) {*q3.l++ =(x.l)>(int)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.l)>(int)(*q2.b++);n--;}break;
 case 4: x.d=(double)(x.l); while (n) {*q3.l++ =(x.d)>(*q2.d++);n--;}break;
}	break;
case 1:	x.w = *p1.w;
 switch (type2)	{
 case 1: while (n) {*q3.l++ =(x.w)>(*q2.w++);n--;}break;
 case 3: x.f=(float)x.w; while (n) {*q3.l++ =(x.f)>(*q2.f++);n--;}break;
 case 2: x.l=(int)x.w; while (n) {*q3.l++ =(x.l)>(*q2.l++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.w)>(short)(*q2.b++);n--;}break;
 case 4: x.d=(double)x.w; while (n) {*q3.l++ =(x.d)>(*q2.d++);n--;}break;
}	break;
case 0:	x.b = *p1.b;
switch (type2)	{
 case 0: while (n) {*q3.l++ =(x.b)>(*q2.b++);n--;}break;
 case 1:  x.w=(short)x.b; while (n) {*q3.l++ =(x.w)>(*q2.w++);n--;}break;
 case 3: x.f=(float)x.b; while (n) {*q3.l++ =(x.f)>(*q2.f++);n--;}break;
 case 2: x.l=(int)x.b; while (n) {*q3.l++ =(x.l)>(*q2.l++);n--;}break;
 case 4: x.d=(double)x.b; while (n) {*q3.l++ =(x.d)>(*q2.d++);n--;}break;
}	break;
case 4:	x.d = *p1.d;
switch (type2)	{
 case 4: while (n) {*q3.l++ =(x.d)>(*q2.d++);n--;}break;
 case 3: while (n) {*q3.l++ =(x.d)>(double)(*q2.f++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.d)>(double)(*q2.b++);n--;}break;
 case 1: while (n) {*q3.l++ =(x.d)>(double)(*q2.w++);n--;}break;
 case 2: while (n) {*q3.l++ =(x.d)>(double)(*q2.l++);n--;}break;
}	break;
}
return result_sym;
}
/*------------------------------------------------------------------------- */
int ana_ge()
/*relation ge */
{
register union	types_ptr q1,q2,q3;
register int n;
q1.l = p1.l;	q2.l = p2.l;	q3.l = p3.l;	n = nelem;
switch (type1)	{
case 3:	switch (type2)	{
 case 3: while (n) {*q3.l++ =(*q1.f++)>=(*q2.f++);n--; }break;
 case 2: while (n) {*q3.l++ =(*q1.f++)>=(float)(*q2.l++);n--;}break;
 case 1: while (n) {*q3.l++ =(*q1.f++)>=(float)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.f++)>=(float)(*q2.b++);n--;}break;
 case 4: while (n) {*q3.l++ =(double)(*q1.f++)>=(*q2.d++);n--;}break;
}	break;
case 2:	switch (type2)	{
 case 2: while (n) {*q3.l++ =(*q1.l++)>=(*q2.l++);n--;}break;
 case 3: while (n) {*q3.l++ =(float)(*q1.l++)>=(*q2.f++);n--;}break;
 case 1: while (n) {*q3.l++ =(*q1.l++)>=(int)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.l++)>=(int)(*q2.b++);n--;}break;
 case 4: while (n) {*q3.l++ =(double)(*q1.l++)>=(*q2.d++);n--;}break;
}	break;
case 1:	switch (type2)	{
 case 1: while (n) {*q3.l++ =(*q1.w++)>=(*q2.w++);n--;}break;
 case 3: while (n) {*q3.l++ =(float)(*q1.w++)>=(*q2.f++);n--;}break;
 case 2: while (n) {*q3.l++ =(int)(*q1.w++)>=(*q2.l++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.w++)>=(short)(*q2.b++);n--;}break;
 case 4: while (n) {*q3.l++ =(double)(*q1.w++)>=(*q2.d++);n--;}break;
}	break;
case 0:	switch (type2)	{
 case 0: while (n) {*q3.l++ =(*q1.b++)>=(*q2.b++);n--;}break;
 case 1: while (n) {*q3.l++ =(short)(*q1.b++)>=(*q2.w++);n--;}break;
 case 3: while (n) {*q3.l++ =(float)(*q1.b++)>=(*q2.f++);n--;}break;
 case 2: while (n) {*q3.l++ =(int)(*q1.b++)>=(*q2.l++);n--;}break;
 case 4: while (n) {*q3.l++ =(double)(*q1.b++)>=(*q2.d++);n--;}break;
}	break;
case 4:	switch (type2)	{
 case 4: while (n) {*q3.l++ =(*q1.d++)>=(*q2.d++);n--;}break;
 case 3: while (n) {*q3.l++ =(*q1.d++)>=(double)(*q2.f++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.d++)>=(double)(*q2.b++);n--;}break;
 case 1: while (n) {*q3.l++ =(*q1.d++)>=(double)(*q2.w++);n--;}break;
 case 2: while (n) {*q3.l++ =(*q1.d++)>=(double)(*q2.l++);n--;}break;
}	break;
}
return result_sym;
}
/*------------------------------------------------------------------------- */
int ana_ge_s_a()
/*relation ge for scalars/array case */
{
register union	types_ptr q2,q3;
register union	scalar x;
register int n;
q2.l = p2.l;	q3.l = p3.l;	n = nelem;
switch (type1)	{
case 3:  x.f = *p1.f;
 switch (type2)	{
 case 3: while (n) {*q3.l++ =(x.f)>=(*q2.f++);n--; }break;
 case 2: while (n) {*q3.l++ =(x.f)>=(float)(*q2.l++);n--;}break;
 case 1: while (n) {*q3.l++ =(x.f)>=(float)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.f)>=(float)(*q2.b++);n--;}break;
 case 4: x.d = (double) x.f; while (n) {*q3.l++ =(x.d)>=(*q2.d++);n--;}break;
}	break;
case 2:	x.l = *p1.l;
 switch (type2)	{
 case 2: while (n) {*q3.l++ =(x.l)>=(*q2.l++);n--;}break;
 case 3: x.f=(float)(x.l); while (n) {*q3.l++ =(x.f)>=(*q2.f++);n--;}break;
 case 1: while (n) {*q3.l++ =(x.l)>=(int)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.l)>=(int)(*q2.b++);n--;}break;
 case 4: x.d=(double)(x.l); while (n) {*q3.l++ =(x.d)>=(*q2.d++);n--;}break;
}	break;
case 1:	x.w = *p1.w;
 switch (type2)	{
 case 1: while (n) {*q3.l++ =(x.w)>=(*q2.w++);n--;}break;
 case 3: x.f=(float)x.w; while (n) {*q3.l++ =(x.f)>=(*q2.f++);n--;}break;
 case 2: x.l=(int)x.w; while (n) {*q3.l++ =(x.l)>=(*q2.l++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.w)>=(short)(*q2.b++);n--;}break;
 case 4: x.d=(double)x.w; while (n) {*q3.l++ =(x.d)>=(*q2.d++);n--;}break;
}	break;
case 0:	x.b = *p1.b;
switch (type2)	{
 case 0: while (n) {*q3.l++ =(x.b)>=(*q2.b++);n--;}break;
 case 1:  x.w=(short)x.b; while (n) {*q3.l++ =(x.w)>=(*q2.w++);n--;}break;
 case 3: x.f=(float)x.b; while (n) {*q3.l++ =(x.f)>=(*q2.f++);n--;}break;
 case 2: x.l=(int)x.b; while (n) {*q3.l++ =(x.l)>=(*q2.l++);n--;}break;
 case 4: x.d=(double)x.b; while (n) {*q3.l++ =(x.d)>=(*q2.d++);n--;}break;
}	break;
case 4:	x.d = *p1.d;
switch (type2)	{
 case 4: while (n) {*q3.l++ =(x.d)>=(*q2.d++);n--;}break;
 case 3: while (n) {*q3.l++ =(x.d)>=(double)(*q2.f++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.d)>=(double)(*q2.b++);n--;}break;
 case 1: while (n) {*q3.l++ =(x.d)>=(double)(*q2.w++);n--;}break;
 case 2: while (n) {*q3.l++ =(x.d)>=(double)(*q2.l++);n--;}break;
}	break;
}
return result_sym;
}
/*------------------------------------------------------------------------- */
int ana_lt()
/*relation lt */
{
register union	types_ptr q1,q2,q3;
register int n;
q1.l = p1.l;	q2.l = p2.l;	q3.l = p3.l;	n = nelem;
switch (type1)	{
case 3:	switch (type2)	{
 case 3: while (n) {*q3.l++ =(*q1.f++)<(*q2.f++);n--; }break;
 case 2: while (n) {*q3.l++ =(*q1.f++)<(float)(*q2.l++);n--;}break;
 case 1: while (n) {*q3.l++ =(*q1.f++)<(float)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.f++)<(float)(*q2.b++);n--;}break;
 case 4: while (n) {*q3.l++ =(double)(*q1.f++)<(*q2.d++);n--;}break;
}	break;
case 2:	switch (type2)	{
 case 2: while (n) {*q3.l++ =(*q1.l++)<(*q2.l++);n--;}break;
 case 3: while (n) {*q3.l++ =(float)(*q1.l++)<(*q2.f++);n--;}break;
 case 1: while (n) {*q3.l++ =(*q1.l++)<(int)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.l++)<(int)(*q2.b++);n--;}break;
 case 4: while (n) {*q3.l++ =(double)(*q1.l++)<(*q2.d++);n--;}break;
}	break;
case 1:	switch (type2)	{
 case 1: while (n) {*q3.l++ =(*q1.w++)<(*q2.w++);n--;}break;
 case 3: while (n) {*q3.l++ =(float)(*q1.w++)<(*q2.f++);n--;}break;
 case 2: while (n) {*q3.l++ =(int)(*q1.w++)<(*q2.l++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.w++)<(short)(*q2.b++);n--;}break;
 case 4: while (n) {*q3.l++ =(double)(*q1.w++)<(*q2.d++);n--;}break;
}	break;
case 0:	switch (type2)	{
 case 0: while (n) {*q3.l++ =(*q1.b++)<(*q2.b++);n--;}break;
 case 1: while (n) {*q3.l++ =(short)(*q1.b++)<(*q2.w++);n--;}break;
 case 3: while (n) {*q3.l++ =(float)(*q1.b++)<(*q2.f++);n--;}break;
 case 2: while (n) {*q3.l++ =(int)(*q1.b++)<(*q2.l++);n--;}break;
 case 4: while (n) {*q3.l++ =(double)(*q1.b++)<(*q2.d++);n--;}break;
}	break;
case 4:	switch (type2)	{
 case 4: while (n) {*q3.l++ =(*q1.d++)<(*q2.d++);n--;}break;
 case 3: while (n) {*q3.l++ =(*q1.d++)<(double)(*q2.f++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.d++)<(double)(*q2.b++);n--;}break;
 case 1: while (n) {*q3.l++ =(*q1.d++)<(double)(*q2.w++);n--;}break;
 case 2: while (n) {*q3.l++ =(*q1.d++)<(double)(*q2.l++);n--;}break;
}	break;
}
return result_sym;
}
/*------------------------------------------------------------------------- */
int ana_lt_s_a()
/*relation lt for scalars/array case */
{
register union	types_ptr q2,q3;
register union	scalar x;
register int n;
q2.l = p2.l;	q3.l = p3.l;	n = nelem;
switch (type1)	{
case 3:  x.f = *p1.f;
 switch (type2)	{
 case 3: while (n) {*q3.l++ =(x.f)<(*q2.f++);n--; }break;
 case 2: while (n) {*q3.l++ =(x.f)<(float)(*q2.l++);n--;}break;
 case 1: while (n) {*q3.l++ =(x.f)<(float)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.f)<(float)(*q2.b++);n--;}break;
 case 4: x.d = (double) x.f; while (n) {*q3.l++ =(x.d)<(*q2.d++);n--;}break;
}	break;
case 2:	x.l = *p1.l;
 switch (type2)	{
 case 2: while (n) {*q3.l++ =(x.l)<(*q2.l++);n--;}break;
 case 3: x.f=(float)(x.l); while (n) {*q3.l++ =(x.f)<(*q2.f++);n--;}break;
 case 1: while (n) {*q3.l++ =(x.l)<(int)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.l)<(int)(*q2.b++);n--;}break;
 case 4: x.d=(double)(x.l); while (n) {*q3.l++ =(x.d)<(*q2.d++);n--;}break;
}	break;
case 1:	x.w = *p1.w;
 switch (type2)	{
 case 1: while (n) {*q3.l++ =(x.w)<(*q2.w++);n--;}break;
 case 3: x.f=(float)x.w; while (n) {*q3.l++ =(x.f)<(*q2.f++);n--;}break;
 case 2: x.l=(int)x.w; while (n) {*q3.l++ =(x.l)<(*q2.l++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.w)<(short)(*q2.b++);n--;}break;
 case 4: x.d=(double)x.w; while (n) {*q3.l++ =(x.d)<(*q2.d++);n--;}break;
}	break;
case 0:	x.b = *p1.b;
switch (type2)	{
 case 0: while (n) {*q3.l++ =(x.b)<(*q2.b++);n--;}break;
 case 1:  x.w=(short)x.b; while (n) {*q3.l++ =(x.w)<(*q2.w++);n--;}break;
 case 3: x.f=(float)x.b; while (n) {*q3.l++ =(x.f)<(*q2.f++);n--;}break;
 case 2: x.l=(int)x.b; while (n) {*q3.l++ =(x.l)<(*q2.l++);n--;}break;
 case 4: x.d=(double)x.b; while (n) {*q3.l++ =(x.d)<(*q2.d++);n--;}break;
}	break;
case 4:	x.d = *p1.d;
switch (type2)	{
 case 4: while (n) {*q3.l++ =(x.d)<(*q2.d++);n--;}break;
 case 3: while (n) {*q3.l++ =(x.d)<(double)(*q2.f++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.d)<(double)(*q2.b++);n--;}break;
 case 1: while (n) {*q3.l++ =(x.d)<(double)(*q2.w++);n--;}break;
 case 2: while (n) {*q3.l++ =(x.d)<(double)(*q2.l++);n--;}break;
}	break;
}
return result_sym;
}
/*------------------------------------------------------------------------- */
int ana_le()
/*relation le */
{
register union	types_ptr q1,q2,q3;
register int n;
q1.l = p1.l;	q2.l = p2.l;	q3.l = p3.l;	n = nelem;
switch (type1)	{
case 3:	switch (type2)	{
 case 3: while (n) {*q3.l++ =(*q1.f++)<=(*q2.f++);n--; }break;
 case 2: while (n) {*q3.l++ =(*q1.f++)<=(float)(*q2.l++);n--;}break;
 case 1: while (n) {*q3.l++ =(*q1.f++)<=(float)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.f++)<=(float)(*q2.b++);n--;}break;
 case 4: while (n) {*q3.l++ =(double)(*q1.f++)<=(*q2.d++);n--;}break;
}	break;
case 2:	switch (type2)	{
 case 2: while (n) {*q3.l++ =(*q1.l++)<=(*q2.l++);n--;}break;
 case 3: while (n) {*q3.l++ =(float)(*q1.l++)<=(*q2.f++);n--;}break;
 case 1: while (n) {*q3.l++ =(*q1.l++)<=(int)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.l++)<=(int)(*q2.b++);n--;}break;
 case 4: while (n) {*q3.l++ =(double)(*q1.l++)<=(*q2.d++);n--;}break;
}	break;
case 1:	switch (type2)	{
 case 1: while (n) {*q3.l++ =(*q1.w++)<=(*q2.w++);n--;}break;
 case 3: while (n) {*q3.l++ =(float)(*q1.w++)<=(*q2.f++);n--;}break;
 case 2: while (n) {*q3.l++ =(int)(*q1.w++)<=(*q2.l++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.w++)<=(short)(*q2.b++);n--;}break;
 case 4: while (n) {*q3.l++ =(double)(*q1.w++)<=(*q2.d++);n--;}break;
}	break;
case 0:	switch (type2)	{
 case 0: while (n) {*q3.l++ =(*q1.b++)<=(*q2.b++);n--;}break;
 case 1: while (n) {*q3.l++ =(short)(*q1.b++)<=(*q2.w++);n--;}break;
 case 3: while (n) {*q3.l++ =(float)(*q1.b++)<=(*q2.f++);n--;}break;
 case 2: while (n) {*q3.l++ =(int)(*q1.b++)<=(*q2.l++);n--;}break;
 case 4: while (n) {*q3.l++ =(double)(*q1.b++)<=(*q2.d++);n--;}break;
}	break;
case 4:	switch (type2)	{
 case 4: while (n) {*q3.l++ =(*q1.d++)<=(*q2.d++);n--;}break;
 case 3: while (n) {*q3.l++ =(*q1.d++)<=(double)(*q2.f++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.d++)<=(double)(*q2.b++);n--;}break;
 case 1: while (n) {*q3.l++ =(*q1.d++)<=(double)(*q2.w++);n--;}break;
 case 2: while (n) {*q3.l++ =(*q1.d++)<=(double)(*q2.l++);n--;}break;
}	break;
}
return result_sym;
}
/*------------------------------------------------------------------------- */
int ana_le_s_a()
/*relation le for scalars/array case */
{
register union	types_ptr q2,q3;
register union	scalar x;
register int n;
q2.l = p2.l;	q3.l = p3.l;	n = nelem;
switch (type1)	{
case 3:  x.f = *p1.f;
 switch (type2)	{
 case 3: while (n) {*q3.l++ =(x.f)<=(*q2.f++);n--; }break;
 case 2: while (n) {*q3.l++ =(x.f)<=(float)(*q2.l++);n--;}break;
 case 1: while (n) {*q3.l++ =(x.f)<=(float)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.f)<=(float)(*q2.b++);n--;}break;
 case 4: x.d = (double) x.f; while (n) {*q3.l++ =(x.d)<=(*q2.d++);n--;}break;
}	break;
case 2:	x.l = *p1.l;
 switch (type2)	{
 case 2: while (n) {*q3.l++ =(x.l)<=(*q2.l++);n--;}break;
 case 3: x.f=(float)(x.l); while (n) {*q3.l++ =(x.f)<=(*q2.f++);n--;}break;
 case 1: while (n) {*q3.l++ =(x.l)<=(int)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.l)<=(int)(*q2.b++);n--;}break;
 case 4: x.d=(double)(x.l); while (n) {*q3.l++ =(x.d)<=(*q2.d++);n--;}break;
}	break;
case 1:	x.w = *p1.w;
 switch (type2)	{
 case 1: while (n) {*q3.l++ =(x.w)<=(*q2.w++);n--;}break;
 case 3: x.f=(float)x.w; while (n) {*q3.l++ =(x.f)<=(*q2.f++);n--;}break;
 case 2: x.l=(int)x.w; while (n) {*q3.l++ =(x.l)<=(*q2.l++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.w)<=(short)(*q2.b++);n--;}break;
 case 4: x.d=(double)x.w; while (n) {*q3.l++ =(x.d)<=(*q2.d++);n--;}break;
}	break;
case 0:	x.b = *p1.b;
switch (type2)	{
 case 0: while (n) {*q3.l++ =(x.b)<=(*q2.b++);n--;}break;
 case 1:  x.w=(short)x.b; while (n) {*q3.l++ =(x.w)<=(*q2.w++);n--;}break;
 case 3: x.f=(float)x.b; while (n) {*q3.l++ =(x.f)<=(*q2.f++);n--;}break;
 case 2: x.l=(int)x.b; while (n) {*q3.l++ =(x.l)<=(*q2.l++);n--;}break;
 case 4: x.d=(double)x.b; while (n) {*q3.l++ =(x.d)<=(*q2.d++);n--;}break;
}	break;
case 4:	x.d = *p1.d;
switch (type2)	{
 case 4: while (n) {*q3.l++ =(x.d)<=(*q2.d++);n--;}break;
 case 3: while (n) {*q3.l++ =(x.d)<=(double)(*q2.f++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.d)<=(double)(*q2.b++);n--;}break;
 case 1: while (n) {*q3.l++ =(x.d)<=(double)(*q2.w++);n--;}break;
 case 2: while (n) {*q3.l++ =(x.d)<=(double)(*q2.l++);n--;}break;
}	break;
}
return result_sym;
}
/*------------------------------------------------------------------------- */
/* logical operations, 3 binary types, only integer arguments are allowed
(this was checked in eval)
all operations are done bitwise and result has high type of the inputs, fewer
switch statements than other ops because no floating cases, all logical
ops are communative, ops are OR, AND, and XOR */
/*------------------------------------------------------------------------- */
int ana_or()
/*or 2 symbols */
{
register union	types_ptr q1,q2,q3;
register int n;
q1.l = p1.l;	q2.l = p2.l;	q3.l = p3.l;	n = nelem;
switch (type1)	{
case 2:	switch (type2)	{
 case 2: while (n) {*q3.l++ =(*q1.l++)|(*q2.l++);n--;}break;
 case 1: while (n) {*q3.l++ =(*q1.l++)|(int)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.l++)|(int)(*q2.b++);n--;}break;
}	break;
case 1:	switch (type2)	{
 case 1: while (n) {*q3.w++ =(*q1.w++)|(*q2.w++);n--;}break;
 case 2: while (n) {*q3.l++ =(int)(*q1.w++)|(*q2.l++);n--;}break;
 case 0: while (n) {*q3.w++ =(*q1.w++)|(short)(*q2.b++);n--;}break;
}	break;
case 0:	switch (type2)	{
 case 0: while (n) {*q3.b++ =(*q1.b++)|(*q2.b++);n--;}break;
 case 1: while (n) {*q3.w++ =(short)(*q1.b++)|(*q2.w++);n--;}break;
 case 2: while (n) {*q3.l++ =(int)(*q1.b++)|(*q2.l++);n--;}break;
}	break;
}
return result_sym;
}
/*------------------------------------------------------------------------- */
int ana_or_s_a()
/*or 2 symbols, first a scalar, second an array */
/*also used for array/scalar case by switching id's since this op commutes */
{
register union	types_ptr q2,q3;
register union	scalar x;
register int n;
q2.l = p2.l;	q3.l = p3.l;	n = nelem;
switch (type1)	{
case 2:	 x.l = *p1.l;
 switch (type2)	{
 case 2: while (n) {*q3.l++ =(x.l)|(*q2.l++);n--;}break;
 case 1: while (n) {*q3.l++ =(x.l)|(int)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.l)|(int)(*q2.b++);n--;}break;
}	break;
case 1:	 x.w = *p1.w;
 switch (type2)	{
 case 1: while (n) {*q3.w++ =(x.w)|(*q2.w++);n--;}break;
 case 2: x.l=(int)x.w; while (n) {*q3.l++ =(x.l)|(*q2.l++);n--;}break;
 case 0: while (n) {*q3.w++ =(x.w)|(short)(*q2.b++);n--;}break;
}	break;
case 0:	 x.b = *p1.b;
 switch (type2)	{
 case 0: while (n) {*q3.b++ =(x.b)|(*q2.b++);n--;}break;
 case 1: x.w=(short)x.b; while (n) {*q3.w++ =(x.w)|(*q2.w++);n--;}break;
 case 2: x.l=(int)x.b; while (n) {*q3.l++ =(x.l)|(*q2.l++);n--;}break;
}	break;
}
return result_sym;
}
/*------------------------------------------------------------------------- */
int ana_and()
/*and 2 symbols */
{
register union	types_ptr q1,q2,q3;
register int n;
q1.l = p1.l;	q2.l = p2.l;	q3.l = p3.l;	n = nelem;
switch (type1)	{
case 2:	switch (type2)	{
 case 2: while (n) {*q3.l++ =(*q1.l++)&(*q2.l++);n--;}break;
 case 1: while (n) {*q3.l++ =(*q1.l++)&(int)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.l++)&(int)(*q2.b++);n--;}break;
}	break;
case 1:	switch (type2)	{
 case 1: while (n) {*q3.w++ =(*q1.w++)&(*q2.w++);n--;}break;
 case 2: while (n) {*q3.l++ =(int)(*q1.w++)&(*q2.l++);n--;}break;
 case 0: while (n) {*q3.w++ =(*q1.w++)&(short)(*q2.b++);n--;}break;
}	break;
case 0:	switch (type2)	{
 case 0: while (n) {*q3.b++ =(*q1.b++)&(*q2.b++);n--;}break;
 case 1: while (n) {*q3.w++ =(short)(*q1.b++)&(*q2.w++);n--;}break;
 case 2: while (n) {*q3.l++ =(int)(*q1.b++)&(*q2.l++);n--;}break;
}	break;
}
return result_sym;
}
/*------------------------------------------------------------------------- */
int ana_and_s_a()
/*and 2 symbols, first a scalar, second an array */
/*also used for array/scalar case by switching id's since this op commutes */
{
register union	types_ptr q2,q3;
register union	scalar x;
register int n;
q2.l = p2.l;	q3.l = p3.l;	n = nelem;
switch (type1)	{
case 2:	 x.l = *p1.l;
 switch (type2)	{
 case 2: while (n) {*q3.l++ =(x.l)&(*q2.l++);n--;}break;
 case 1: while (n) {*q3.l++ =(x.l)&(int)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.l)&(int)(*q2.b++);n--;}break;
}	break;
case 1:	 x.w = *p1.w;
 switch (type2)	{
 case 1: while (n) {*q3.w++ =(x.w)&(*q2.w++);n--;}break;
 case 2: x.l=(int)x.w; while (n) {*q3.l++ =(x.l)&(*q2.l++);n--;}break;
 case 0: while (n) {*q3.w++ =(x.w)&(short)(*q2.b++);n--;}break;
}	break;
case 0:	 x.b = *p1.b;
 switch (type2)	{
 case 0: while (n) {*q3.b++ =(x.b)&(*q2.b++);n--;}break;
 case 1: x.w=(short)x.b; while (n) {*q3.w++ =(x.w)&(*q2.w++);n--;}break;
 case 2: x.l=(int)x.b; while (n) {*q3.l++ =(x.l)&(*q2.l++);n--;}break;
}	break;
}
return result_sym;
}
/*------------------------------------------------------------------------- */
int ana_xor()
/*xor 2 symbols */
{
register union	types_ptr q1,q2,q3;
register int n;
q1.l = p1.l;	q2.l = p2.l;	q3.l = p3.l;	n = nelem;
switch (type1)	{
case 2:	switch (type2)	{
 case 2: while (n) {*q3.l++ =(*q1.l++)^(*q2.l++);n--;}break;
 case 1: while (n) {*q3.l++ =(*q1.l++)^(int)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(*q1.l++)^(int)(*q2.b++);n--;}break;
}	break;
case 1:	switch (type2)	{
 case 1: while (n) {*q3.w++ =(*q1.w++)^(*q2.w++);n--;}break;
 case 2: while (n) {*q3.l++ =(int)(*q1.w++)^(*q2.l++);n--;}break;
 case 0: while (n) {*q3.w++ =(*q1.w++)^(short)(*q2.b++);n--;}break;
}	break;
case 0:	switch (type2)	{
 case 0: while (n) {*q3.b++ =(*q1.b++)^(*q2.b++);n--;}break;
 case 1: while (n) {*q3.w++ =(short)(*q1.b++)^(*q2.w++);n--;}break;
 case 2: while (n) {*q3.l++ =(int)(*q1.b++)^(*q2.l++);n--;}break;
}	break;
}
return result_sym;
}
/*------------------------------------------------------------------------- */
int ana_xor_s_a()
/*xor 2 symbols, first a scalar, second an array */
/*also used for array/scalar case by switching id's since this op commutes */
{
register union	types_ptr q2,q3;
register union	scalar x;
register int n;
q2.l = p2.l;	q3.l = p3.l;	n = nelem;
switch (type1)	{
case 2:	 x.l = *p1.l;
 switch (type2)	{
 case 2: while (n) {*q3.l++ =(x.l)^(*q2.l++);n--;}break;
 case 1: while (n) {*q3.l++ =(x.l)^(int)(*q2.w++);n--;}break;
 case 0: while (n) {*q3.l++ =(x.l)^(int)(*q2.b++);n--;}break;
}	break;
case 1:	 x.w = *p1.w;
 switch (type2)	{
 case 1: while (n) {*q3.w++ =(x.w)^(*q2.w++);n--;}break;
 case 2: x.l=(int)x.w; while (n) {*q3.l++ =(x.l)^(*q2.l++);n--;}break;
 case 0: while (n) {*q3.w++ =(x.w)^(short)(*q2.b++);n--;}break;
}	break;
case 0:	 x.b = *p1.b;
 switch (type2)	{
 case 0: while (n) {*q3.b++ =(x.b)^(*q2.b++);n--;}break;
 case 1: x.w=(short)x.b; while (n) {*q3.w++ =(x.w)^(*q2.w++);n--;}break;
 case 2: x.l=(int)x.b; while (n) {*q3.l++ =(x.l)^(*q2.l++);n--;}break;
}	break;
}
return result_sym;
}
/*------------------------------------------------------------------------- */
