#include <sys/types.h>
#include <malloc.h>

int ana_memstat()
{
/* check malloc usage for SGI */
struct mallinfo mallinfo(void), m;
m = mallinfo();
printf("total in arena: %d ordinary: %d  small: %d  keep cost: %d\n", m.arena,m.ordblks,m.smblks,
       m.keepcost);
printf("holding: %d %d\n", m.hblkhd, m.hblks);
printf("using: s: %d o: %d, not using s: %d o: %d\n", m.usmblks, m.uordblks,
       m.fsmblks, m.fordblks);
return 1;
}
