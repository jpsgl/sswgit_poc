D       [0-9]
H	[0-9a-fA-F]
%{
#include "parser.h"
#include "y.tab.h"
#undef input
# define input() (((yytchar=yysptr>yysbuf?U(*--yysptr):mygetc(yyin))==10?(yylineno++,yytchar):yytchar)==EOF?0:yytchar)
int	lineno = 1;
char	mybuf[512], *myptr;
int	myleftover = 0;
extern	int	echo_flag, main_type_flag, sid;
void skipcommnts(void)
{
/* printf("skipping a C type comment\n"); */
for(;;) { while(input() != '*');
	if(input()=='/') return;
	else unput(yytext[yyleng-1]);
}
/* printf("done skipping comment\n"); */

}
%}

%%
[ \t\r]	{ ; }	/* ignore whitespace */
"$fef"|"$FEF"	{ yylval.ival = 32; return RWBIV; /* built-in variables */}
"$mms"|"$MMS"	{ yylval.ival = 31; return ROBIV; }
"$sef"|"$SEF"	{ yylval.ival = 33; return RWBIV; }
"$tef"|"$TEF"	{ yylval.ival = 34; return RWBIV; }
"$nbf"|"$NBF"	{ yylval.ival = 35; return ROBIV; }
"$cbf"|"$CBF"	{ yylval.ival = 36; return ROBIV; }
"$trc"|"$TRC"	{ yylval.ival = 37; return RWBIV; }
"$mxr"|"$MXR"	{ yylval.ival = 38; return RWBIV; }
"$mxc"|"$MXC"	{ yylval.ival = 39; return RWBIV; }
"$mxv"|"$MXV"	{ yylval.ival = 40; return ROBIV; }
"$mnr"|"$MNR"	{ yylval.ival = 41; return RWBIV; }
"$mnc"|"$MNC"	{ yylval.ival = 42; return RWBIV; }
"$mnv"|"$MNV"	{ yylval.ival = 43; return ROBIV; }
"$rtn"|"$RTN"	{ yylval.ival = 44; return RWBIV; }
"$pri"|"$PRI"	{ yylval.ival = 45; return RWBIV; }
"$pnd"|"$PND"	{ yylval.ival = 46; return ROBIV; }
"$rpt"|"$RPT"	{ yylval.ival = 47; return ROBIV; }
"$saa"|"$SAA"	{ yylval.ival = 27; return ROBIV; }
"$hlf"|"$HLF"	{ yylval.ival = 28; return ROBIV; }
"$aaz"|"$AAZ"	{ yylval.ival = 29; return ROBIV; }

[$][lL][0-9a-dA-D]  { yylval.ival = strtol( &yytext[2], (char **)NULL, 16);
			/* printf("\ngot a local\n"); */
			return LOCAL; }
[$][rR][0-9a-dA-D]+  { yylval.ival = strtol( &yytext[2], (char **)NULL, 16);
			/* printf("\ngot a local\n"); */
			return LOCAL; }

[$][gG][0-9a-fA-F]  { yylval.ival = strtol( &yytext[2], (char **)NULL, 16);
			return GLOBAL; }

{D}+	{ yylval.ival = atol(yytext);
			return NUMBER; }
{D}+s	{ yylval.ival = 20*atol(yytext); /* convert seconds to ticks */
			return NUMBER; }
{D}+m	{ yylval.ival = 1200*atol(yytext); /* convert minutes to ticks */
			return NUMBER; }
0{D}+	{ yylval.ival = atol(yytext);
			return NUMBER; }
0{D}+s	{ yylval.ival = 20*atol(yytext);
			return NUMBER; }
0{D}+m	{ yylval.ival = 1200*atol(yytext);
			return NUMBER; }
0[xX]{H}+	{ yylval.ival = strtol( yytext, (char **)NULL, 16);
			return NUMBER; }
0[xX]{H}+s	{ yylval.ival = 20*strtol( yytext, (char **)NULL, 16);
			return NUMBER; }
0[xX]{H}+m	{ yylval.ival = 1200*strtol( yytext, (char **)NULL, 16);
			return NUMBER; }

"=="	{ return EQ;   /* conditionals */ }
">="	{ return GE; }
"<="	{ return LE; }
">"	{ return GT; }
"<"	{ return LT; }
"!"	{ return NOT; }
"!="	{ return NE; }
"||"	{ return OR;   /* logicals  */ }
"&&"	{ return AND; }
"++"	{ return PP; }
"--"	{ return MM; }
if	{ return IF;   /* control keywords */ }
else	{ return ELSE; }
while	{ return WHILE; }
for	{ return FOR; }

goto		{ return GOTO;    /* special operation keywords */ }
break		{ return BREAK; }
return		{ return RET; }
frame_list	{ return FRAMELIST; }
set_frame_list	{ return FRAMELIST; }
target_list	{ return TARGETLIST; }
set_target_list	{ return TARGETLIST; }
execute_list	{ return EXECUTELIST; }
@saa_pause	{ yylval.ival = 1; return BITMASK;}
@saa_exit	{ yylval.ival = 2; return BITMASK;}
@fl_exit	{ yylval.ival = 4; return BITMASK;}
@sf_exit	{ yylval.ival = 8; return BITMASK;}
@noflags	{ yylval.ival = 0; return BITMASK;}
@no_flags	{ yylval.ival = 0; return BITMASK;}
@nomove		{ ; }
@no_move	{ ; }

srt		{ yylval.ival =  SRT; return SUB0ARGS; /* routines, 0 args */ }
term		{ yylval.ival =  TERM; return SUB0ARGS; }
resetaec	{ yylval.ival =  RESETAEC; return SUB0ARGS; }
lockbuf		{ yylval.ival =  LOCKBUF; return SUB0ARGS; }
startave	{ yylval.ival =  STARTAVE; return SUB0ARGS; }
updimax		{ yylval.ival =  UPDIMAX; return SUB0ARGS; }
tap		{ yylval.ival =  TAP; return SUB0ARGS; }
openloop	{ yylval.ival =  OPENLOOP; return SUB0ARGS; }
closeloop	{ yylval.ival =  CLOSELOOP; return SUB0ARGS; }

wait		{ yylval.ival =  WAIT; return SUB1ARGS;/* routines, 1 args */ }
delay		{ yylval.ival =  DELAY; return SUB1ARGS; }
tim		{ yylval.ival =  TIM; return SUB1ARGS; }
rundhcp		{ yylval.ival =  RUNDHCP; return SUB1ARGS; }
call		{ return CALL; /* done uniquely now, not a general subr */}
frame		{ return FRAME;/* also */ }
target		{ return TARGET;/* also */ }

dev		{ yylval.ival =  DEV; return SUB2ARGS; }
samp		{ yylval.ival =  SAMP; return SUB2ARGS; }
setdhcr		{ yylval.ival =  SETDHCR; return SUB2ARGS; }
timchk		{ yylval.ival =  TIMCHK; return SUB2ARGS; }
lda		{ yylval.ival =  LDA; return SUB2ARGS; }

framepf		{ yylval.ival =  FRAMEPF; return SUB3ARGS; }

$list$[a-zA-Z0-9_.]*	{ yylval.sym = (char *) malloc(strlen(yytext)+1);
	strcpy(yylval.sym, yytext);  return LIST;  }

[a-zA-Z_][a-zA-Z0-9_.]*	 { yylval.sym = (char *) malloc(strlen(yytext)+1);
	strcpy(yylval.sym, yytext);  return SYMBOL;  }

$type[ \t]*:[ \t]*main.*\n { main_type_flag = 1; main_seq_setup (sid);}
$type[ \t]*:[ \t]*scene.*\n { ; }
$type[ \t]*:[ \t]*sequence.*\n { ; }
$type[ \t]*:[ \t]*sub[-]*sequence.*\n { ; }
$type[ \t]*:[ \t]*sub[-]*routine.*\n { ; }
^"//"Type:[ \t]*main[.]*\n {    main_type_flag = 1; main_seq_setup (sid); }
"//".*	{ /* printf(";%s\n", &yytext[2]); */ }
"/*"	{ skipcommnts(); }
\n 	{ lineno++;  /*printf("\n new line, count is %d\n", lineno);*/
	return '\n';  }
.	{ /*printf("\nsomething else!\n");*/  return yytext[0]; }
%%
 /* -------------------------------------------------------------*/
int warning(s, t)  /* taken from hoc */
 char	*s, *t;
 {
 fprintf(stdout,";%s", s);
 if (t) fprintf(stdout,";%s", t);
 fprintf(stdout, " triggered in line %d\n", lineno);
 fprintf(stdout, ";last token parsed in: %s\n", yytext);
 }
 /* -------------------------------------------------------------*/
int mygetc(fin)
 FILE *fin;
 {
 /* replaces the usual getc, we do this to read in script one line at
 a time and optionally echo the line before feeding characters to lex */
 
 while (myleftover == 0) {
 if (fgets(mybuf, 512, fin) == NULL) return EOF;
 myleftover = strlen(mybuf);
 myptr = mybuf;
 /* if echo_flag is set, print out the line, should have an eol already */
 if (echo_flag) fprintf(yyout, ";%s", mybuf);
 }
 
 myleftover--;
 return *myptr++;
 }
