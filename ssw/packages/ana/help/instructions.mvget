Extract Movie Cube Tool

Define and extract a data cube from a series of files or
certain special multiple image files. The cube can then be
played and/or stored.

See end for suggestions when something goes wrong.

Image Source:

This tool can use several types of image sources selected via
the "source" option menu in the upper RHS. Normally it will be
configured to read all of the images for a given source. The
file template source expects a series of numbered files with a
numeric range specified using # (pound) signs. A range of
numbers (with leading zeroes) are filled into this area to get
the actual file names.

The "image list (TRACE)" expects a TRACE image list prepared
by the TRACE selector tool or one saved earlier.

The "file list" reads an ASCII file containing names of files,
one per line. Only the first token of each line is used. The
files are read as listed, there is no sorting. Lines beginning
with a # sign are regarded as comments, hence files beginning
with a # cannot be used.

The "multi-image file" is a special file such as the ones
created by trace_prep. The initiate button must be pressed to
parse the image list in the file.

Subarea:

A subarea in space and a subrange in time can be defined as
well as sampling intervals in space and time. This may be
necessary to keep the size of the cube within the memory size
of the computer or to play the movie fast enough. There are two
major options; 1) select a subarea fixed on each image (or on
the CCD for TRACE), and 2) select a subarea co-rotating on the
sun. The latter requires, of course, the position of each image
on the sun and the time. It is useful for long time series. For
the first option, the starting (x,y) and the range (nx,ny) on
the image are given. For the second, the central latitude and
Carrington Longitude are given and the range (nx,ny) in image
pixels. When the link button is used to attach the tool to a
specific view (containing an image), a subarea for the entire
image is entered along with the central latitude and Carrington
longitude (if known). A link is done by pressing the green
"link" button, moving the mouse to the desired window, and then
pressing the left mouse button. There should be a confirming
beep. This window would normally contain an image in the series
you are extracting the cube from but the code will accept any
data window.

The spatial subarea can then be set interactively. Hit the
"select subarea" button which starts an interactive rectangle
definition in a data window. Move the mouse (buttons up) to the
data window and then press the left or center mouse button near
one corner of the desired subarea. Keeping the button down,
expand the box as needed. By holding down the shift key, the
box can be moved without changing size instead. When the mouse
button is released, the subarea is defined and the text windows
in the widget are updated with the lower left hand coordinate
of the rectangle and the size in pixels. This can be repeated
but only the last subarea selected is used. The coordinate and
size of the rectangular subarea can also be manually entered or
adjusted in the text boxes. The temporal range needs to be
typed into the text boxes. The best way is to specifiy the
first image number and the "nt" value. Pressing the "scan
sizes" button will then update the last image number and show
the size of the cube. Integer sampling intervals in x, y, or t
can also be specified. In space, the images have pixels
combined. In time, a dt > 1 causes only every dt'th image to be
read.

Intensity Scaling:

The image intensity scaling can be specified from a link window
or using a dedicated display widget brought up with the gray
"Display" button. Note that the checkbox indicating whether the
parameters are taken from the link window must be set to off
for this to have any effect. There is also a "Modifiers" button
that brings up a widget for setting up despiker and filter
functions. See the help in that widget for more details. Note
however that the despiking parameters are taken from the link
window when the aforementioned checkbox is on. The other
modifiers are always used if set.

Do It:

When cube parameters are defined to your satisfaction, press
the red "do it" button to actually read the files. This might
take a while, depending on the files. When finished, the cube
can be played with the VCR tool or saved for later use.

Problems:

If the program chokes for some reason without turning off the
"progress" widget, you should manually turn it off by hitting
the dismiss button. Because this widget is "modal", no other
widget will response until this is done.

To get rid of this window, press help again. To get rid of the
extract cube tool window and this window, press dismiss. The
cube will remain defined.


