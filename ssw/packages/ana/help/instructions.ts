Time Slice Tool

Presently this tool only works on the "main" movie
which is in view 15. It will be eventually extended
to work with file sets. In the meantime, a movie
must first be created to make space/time slices.
The mode option is ignored. There may be things
that don't work yet, be careful.

By default the "assume aligned" option is set. This
affects computation of velocities on the space/time
image when using the mouse tracking tool. If off, the
solar positions of each movie frame are used to compute
the x displacement and solar rotation will be included
even if the movie is tracking.

An interactive space/time slice is generated using
the mouse to control the parameters. First a link
must be made to a movie view by entering the window
number next to the link button or by pressing the
link button and then moving the mouse to the desired
view and left clicking once. Check that the number
is then correctly displayed. As noted above, only
view 15 will actually work at present.

There are 3 types of slices as indicated by the
3 red buttons. Each can also be associated with an
output view. The views 20 - 24 are reserved for
space/time views and are the only ones available.

To do a line slice, press the red line slice button
and then move the mouse to the movie view. Press
and hold the left mouse button at the desired
starting point for your line (you can change it
later). Move it (still holding down the button) to
extend the line. If it did not already exist, a
time slice view will appear containing the space/time
slice along your line (probably in an awkward place).
This will update as you move the line, your mouse
position defines the endpoint while the initial down
position is the start point. You can instead move
the line rigidly by holding down the shift button.
Or, by holding down the control key, you can fix
the line line center and spin the line around.
Release the left mouse button and any other keys
when finished. Initially the space/time view will
show the time scale in "native" mode which is one
screen pixel for each frame in the movie. You can
adjust by entering a time step value (in seconds)
in the t scale window. Entering a zero will get
you the average time spacing. You might be able
to break or hang the code by entering a very
inappropiate value, so be a bit careful. Entering
a -1 will get you a "native" scale again. The spatial
scale alone the line can be adjusted with the s zoom
option. A value of 1 gives you the pixel scale for
the original movie (if it was zoomed, you don't get
the zoomed scale). An arbitrary scale factor can also
be entered. The orientation of the space/time view
be set to any of the 8 possible ways with the t/x
button (which should be intuitive).

The second type of slice is really 2 in one, the x/y
slice. Press the red x/y slice button (after checking
the output views, 2 in this case) and then move the
mouse to the linked movie. Now click and hold the
left mouse button. A vertical and horizontal line
should appear crossing at the mouse position. These
will follow you mouse until the button is release.
The space/time slices for the lines will appear in
the 2 output windows.

To get rid of this window, press help again.

