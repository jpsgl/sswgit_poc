Time Averages

Another data product from a frame is a time average which is
generally used in conjuction with transient event testing. Time
averages should be activated for only one frame in situations
where several frames (e.g., for different wavelengths) are
being run interleaved. This is because there is only one buffer
for time averages.

Between 1 and 16 frames can be specified for the average (the 1
case is useful only for transients). Time averaging can be
specified for either the full CCD image or subarea 1. When active,
the normal output for the indicated area is not made until the
average is complete. The data is then processed and put in the
telemetry as specified by the storage parameters. If the purpose
is to check for transients, it might not be stored at all (by
setting the don't save option for the area).

Although the data is summed while the average is accumulated, the
result is divided by the number of images summed to maintain a
12 bit image in the result. Hence, time averaging is not useful
for increasing dynamic range.
