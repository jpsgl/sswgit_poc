Play Movie Cube Tool

Show a movie using a pre-defined data cube. There are also
options for clocks, notes, and saving, and loading.

A movie cube must exist for this tool to work. You
can either read in a previously saved cube or generate
one from a file series. There is a tool for each of these
operations. You can also use the load button at the top of
the tool.

This widget contains several sliders, options, and a set
of VCR-like buttons which work almost like real VCR
buttons. First the sliders. The frame slider allows you
select any frame. The update follows the slider motion
and you can move it for a manual movie. Fast motions will
cause frames to be skipped. The limit sliders allow a
subset of the movie to be viewed with the VCR buttons.
There is an option button which causes the frame slider
to move when the VCR play buttons are used. On some
computers, this can slow down the movie markedly. The
VCR button functions are, from left to right:

pause
reverse play
reverse single step
rock (forward/reverse play combined)
forward single step
forward play

When one of the play buttons is pressed, the movie plays
continuously until another VCR button is pressed. While
playing the limits can be adjusted and the delay between
frames can be changed. Any positive delay (in seconds)
can be entered. For fastest play, the delay should be zero.
There is also a skip count. This makes the movie play faster
by skipping frames. A count of 1 causes every frame to be
shown, 2 will display every other, and so on. The entry
has no effect until a return is entered in the skip text
window. It can be changed while a movie is running. The
skip count does not affect the single step buttons.

Pressing another VCR button while a movie is running causes
a pause only, the button function is ignored. Once the
play is stopped, another VCR button can be used. For example,
if the movie is playing forward and you want to start it
going backwards, you need to press reverse play (or any other
button) once to stop and again to start the reverse play.
The single step buttons always move the frame slider and
update the frame number. The play buttons do so only when the
"move slider" option is on.

The options button at the top (near the center) activates
an options widget that allows turning on/off clocks and
entering notes. Clocks depend on a time data base which
is read in with the movie cube or generated when the movie
was created. The analog and digital clocks each are contained
in a movable box and are available in 3 sizes. The notes
area will contain any notes associated with a saved movie
if one was loaded. These can then be updated/appended if
desired.

The "save" button allows saving the movie/times/notes into
files. It pops a widget with options. See the help there
for further details.

The "load" button pops the "Load Movie Cube" widget. This
can also be started from the main "movies tools" list.
See the help there for further details.

To get rid of this window, press help again. To get
rid of the play movie tool window and this window, press
dismiss.
