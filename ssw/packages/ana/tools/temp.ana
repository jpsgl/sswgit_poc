 ;===============================================================================
block convert
 ;compress a series of files
 ;the files types supported here are:
 ;0 = uncompressed fz
 ;1 = compressed fz
 ;2 = compressed fz with rle
 ;3 = FITS
 ;4 = JPEG
 ;5 = TIFF
 ;6 = GIF
 ;7 = PGM
 ;8 = YUV for MPEG
 ;get the textfields
 name_in = xmtextfieldgetstring ($t1(1))
 name_out = xmtextfieldgetstring ($t1(2))
 kstart = fix(xmtextfieldgetstring ($twids(1)) 
 kend = fix(xmtextfieldgetstring ($twids(2))
 ;check for single file case (no #'s)
 if strcount(name_in,'#') le 0 then {
  ;probably a single file
  if kstart ne kend then {
  sq = 'no # signs in template\nand kstart ne kend\nfix and try again'
  xmmessage, sq, $f7, 'red',$scr_cx,$scr_cy
  } else { kstart = 0  kend = 0 }
 }
 sq = xmtextfieldgetstring ($twids(3))
 jpeg_quality = fix(xmtextfieldgetstring ($twids(4))
 delete_flag = xmtogglegetstate($deleteopt(1))
 
 if strpos(sq,'default') ge 0 then crunch_flag=1 else { crunch_flag = 0
 	!crunch_slice = fix(sq) }
 if !crunch_slice lt 0 or !crunch_slice gt 10 then {
  errormess,'bad fixed bit width\n"'+sq+'"\ntry again' }
 
 ;if the color_flag is set, figure the color transformations, we only
 ;actually use them for gif and mpeg however
 
 if $color_flag eq 1 then {
 ty,'color mode'
 ;color mode significant only for gif and yuv
  if $file_type eq 6 then {
   	ct = [ [#red_map],[#green_map],[#blue_map] ]
	ct = byte(255.*ct(>1,>0))
  }
  if $file_type eq 8 then {
	redmap = byte(#red_map*255.)
	bluemap = byte(#blue_map*255.)
	greenmap = byte(#green_map*255.)
  }
 }
 ;look for cropping and clipping, clipping to a multiple of 16 is
 ;an option for the MPEG maker
 cfac = 1
 if $crop_flag eq 1 then {
 ix = fix(xmtextfieldgetstring($croprangetext(0)))
 iy = fix(xmtextfieldgetstring($croprangetext(1)))
 ix2 = fix(xmtextfieldgetstring($croprangetext(2)))
 iy2 = fix(xmtextfieldgetstring($croprangetext(3)))
 cfac = fix(xmtextfieldgetstring($cropcfactext))
 if cfac lt 1 then cfac = 1
 if $clip16_flag eq 1 then {
 	iq = (ix2-ix+1)%(16*cfac)	ix2=ix2-iq	
 	iq = (iy2-iy+1)%(16*cfac)	iy2=iy2-iq	
 	}
 }
 ;check it out
 ty,'name_in =',name_in
 ty,'name_out =', name_out
 ty,'$file_type =', $file_type
 ;make the button a stop button now
 xtunmanage,  $startorstop
 xmsetlabel,  $startorstop, 'STOP'
 xmsetcolors, $startorstop, 'red'
 xmposition,  $startorstop, -1, -1, 80, 40
 xtmanage,    $startorstop
 xtloop, 2
 $stop_flag = 0
 kilroy = 0
 for k = kstart, kend do {
 xtloop, 2
 if $stop_flag eq 1 then return
 xmtextfieldsetstring, $statustext, ist(k,1)
 if dataread(xin,fns(name_in,k),head) ne 1 then {
 	ty,'warning, missing or unreadable file at',k } else {
 ;check if we taking a subarea
 if $crop_flag eq 1 then {
 x = xin(ix:ix2, iy:iy2)
 } else {
 if $clip16_flag eq 0 then { switch, xin, x } else {
  nx = dimen(xin,0)	ny = dimen(xin,1)
  iq = nx%(16*cfac)	jq = ny%(16*cfac)
  if jq ne 0 or iq ne 0 then { x = xin(0:(nx-iq-1),0:(ny-jq-1)) } else {
  switch, x, xin }
 }
 }
 if cfac ne 1 then x = compress(x,cfac)
 if kilroy eq 0 then {
  kilroy = 1
  ;save the result size, needed for MPEG maker
  $nxresult = dimen(x,0)	$nyresult = dimen(x,1)
  ;first file read, set up crunch_slice if flagged
  if crunch_flag ne 0 then { !crunch_slice=5
  	if symdtype(x) eq 0 then !crunch_slice = 2 }
 }

 xtloop, 2
 if $stop_flag eq 1 then return
 name = fns(name_out, k)
 
 ;check if an fz type
 if $file_type le 2 then {
 ;since we now know we are one of the fz, wrap it up in here
 
   if $file_type eq 0 then {
	;uncompressed fz
	if fzwrite(x, name, head) eq 0	then write_error, name
   } else {
	if symdtype(x) gt 1 then {
	errormess,'only I*1 or I*2 are\ncompressible, no save'
	return
	} else {
	if $file_type eq 1 then {
	;compressed fz
	if fcwrite(x, name, head) eq 0 then write_error, name
	} else if $file_type eq 2 then
		if fcrunwrite(x, name, head) eq 0 then write_error, name
	}
	}
 } else {
 if $file_type eq 3 then wrfits, x, name, head  else {
 ;another type, may have to convert to byte data, also may not
 ;support graphs
 ;types 4 to 9 all need 2-D byte array
 if $file_type ge 4 and $file_type le 8 then {
 ty,'$file_type in save_file_cb = ', $file_type
 if symclass(x) ne 4 or num_dim(x) ne 2 then {
 ty,'sorry, this type of file only works with 2-D arrays' } else {
 ;if not a byte array, we have to scale; if a byte array, put in xq
 xtype = symdtype(x)
 if xtype eq 0 then switch, xq, x else {
 ;here if not a byte array, get the type of rescale to do
 nq = num_elem($scalesaveopt)
 for i=1,nq do { if xmtogglegetstate($scalesaveopt(i)) eq 1 then {
	  rescale_type_flag = i-1	break }}
 ;use xq as data to store
 ncase rescale_type_flag
  xq = scale256( x)
  { hbot = .001		htop = .999	gam =1.0
   xq = ft256(x, hbot, htop, gam)
  }
 }
 }
 }

 ;whatever it was, it is in xq as a byte array
 ncase $file_type-4
   iq = write_jpeg(xq, name, jpeg_quality)
   iq = tiffwrite( xq, name)
   { ;the gif case, could be in color
     if $color_flag eq 1 then {
	if xtype eq 0 then xq = ft256(xq)
	iq = gifwrite( xq, name, ct)
	} else iq = gifwrite( xq, name)
   }
   iq = pgmwrite( xq, name)
   { ;the yuv case, could be in color
     if $color_flag eq 1 then {
	if xtype eq 0 then xq = ft256(xq)
	iq = yuvwrite( xq, name, redmap, greenmap, bluemap)
     } else iq = yuvwrite_gray( xq, name)
   }
 endcase
 }
 }
 

  } }
}
}

