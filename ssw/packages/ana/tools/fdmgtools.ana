func fdmg_widget(dummy)
; sets up a widget for browsing through the MDI full-disk
; magnetograms.  LS 17mar00
 if defined($fdmgwidget) eq 0 then {
 compile_file, getenv('ANA_WLIB') + '/fdmgwidgettool.ana'
 }
 xtpopup,$fdmgwidget
 return,$fdmgwidget
endfunc
;------------------------------------------------------
subr fdmg_list_entry_cb
 zeroifundefined, $fdcontFlag
 if defined($fdmg_monthwidget) eq 0 then {
 compile_file, getenv('ANA_WLIB') + '/fdmg_monthwidgettool.ana'
 }
 xtpopdown,$fdmg_monthwidget	 ; so list loading goes faster
 $fdmg_month = $list_item
 if $fdcontFlag then files = sort(getfiles($fdmg_base + '/c' + $fdmg_month, 500)) else
   files = sort(getfiles($fdmg_base + '/m' + $fdmg_month, 500))
 nf = num_elem(files)
 xmsetlabel,$fdmg_month_select,$fdmg_month
 xmlistdeleteall,$fdmg_monthwidget
 for i = 0, nf - 1 do xmlistadditem,$fdmg_monthwidget,removepath(files(i))
 xtpopup,$fdmg_monthwidget
endsubr
;---------------------------------------------------------------
subr fdmg_month_entry_cb
 zeroifundefined, $fdcontFlag
 ;use selected file and target window
 if $fdcontFlag then file = $fdmg_base + '/c' + $fdmg_month + '/' + $list_item else
 	file = $fdmg_base + '/m' + $fdmg_month + '/' + $list_item
 view_mg, file, eval(xmtextgetstring($fdmg_trgt))
 endsubr
;---------------------------------------------------------------
subr view_mg, file, win
 $bscale = 0	; so we can be sure if one got input
 $selected_file = file
 h = ''
 if dataread(x, $selected_file, h) eq 1 then {
   update_globals, win, x
   x = 0
   h = 'MDI Full-Disk Magnetogram\n' + date_from_tai($view_tai(win))
   $view_text(win) = h
   ;;xmsetlabel,$view_text_widget(win),h
 } else {
   sq = '\nfile type unknown'
   if filesize($selected_file) lt 0 then sq = '\nfile doesn''t exist'
   errormess, $selected_file+sq
 }
 endsubr
;---------------------------------------------------------------
subr fdmg_selectortool
 if defined($fdmgselectwidget) eq 0 then {
 compile_file, getenv('ANA_WLIB') + '/fdmgselectorwidgettool.ana'
 }
 xtpopup, $fdmgselectwidget
 endsubr
;---------------------------------------------------------------
subr fdmgselectreset_cb
 for i=0,6 do xmtextfieldsetstring, $fdmg_select_time_txt(i), ''
 endsubr
;---------------------------------------------------------------
subr fdmgerasecheckbox_cb
 $fdmg_select_add_flag = $radio_state
 endsubr
;---------------------------------------------------------------
subr fdmgselect_eraseall_cb
 ;erase the whole list
 xmlistdeleteall, $fdmgselectedwidget
 image_selector, -1,-1	;sets variables internal to image_selector
 xmsetlabel, $fdmgselect_lab1, 'empty'
 endsubr
;---------------------------------------------------------------
subr fdmgselector_link_cb
 ;use a link window to set ranges, etc, usually would be a FDMG image
 ;but any window with a time will work
 ;gets called by view_input_cb
 win = $link_window	;before it can change
 s = date_from_tai($view_tai(win))
 
 xmtextfieldsetstring, $fdmg_select_time_txt(0), s(0:3)
 xmtextfieldsetstring, $fdmg_select_time_txt(4), s(0:3)
 xmtextfieldsetstring, $fdmg_select_time_txt(1), s(5:6)
 xmtextfieldsetstring, $fdmg_select_time_txt(5), s(5:6)
 xmtextfieldsetstring, $fdmg_select_time_txt(2), s(8:9)
 xmtextfieldsetstring, $fdmg_select_time_txt(6), s(8:9)
 xmtextfieldsetstring, $fdmg_select_time_txt(3), s(11:12)+':00:00'
 endsubr
;---------------------------------------------------------------
subr fdmg_select_delete_cb
 ;4/11/2005 - this was apparently forgotten and not done until now
 ;deletes the current selection from the "selected" images list
 ;if in the list of lists, that list is also adjusted
 iq = $fdmgselected_item_position - 1
 xmlistdeleteitem, $fdmgselectedwidget, $fdmgselected_item_position
 nf = num_elem($fdmg_names)
 if nf ne $fdmg_select_count then { errormess,'internal list\ncount error\nno action'  return }
 in = indgen(lonarr(nf))
 in = sieve(in ne iq)
 $fdmg_names = $fdmg_names(in)
 $fdmg_times = $fdmg_times(in)
 $fdmg_xc = $fdmg_xc(in)
 $fdmg_yc = $fdmg_yc(in)
 $fdmg_pixsc = $fdmg_pixsc(in)
 $fdmg_select_count -= 1
 xmsetlabel, $fdmgselect_lab1, sprintf('%d files', $fdmg_select_count)
 ;was it included in the list of lists?
 ty,'checking for back modifiy, $fdmgselected_list_of_lists =', $fdmgselected_list_of_lists
 if $fdmgselected_list_of_lists ne -1 then {
  ;change the copy in lists of lists
  imagelist_set_arrays_type4, $fdmgselected_list_of_lists	;sets the arrays
  imagelist_reload_list		;need to re-write list of lists
 }
 endsubr
;---------------------------------------------------------------
subr fdmg_selected_entry_cb
 ;the list item is just the file name here and window is in link window
 view_mg, $list_item, fix(xmtextgetstring($fdmgselectedtext1))
 $fdmgselected_item_position = $list_item_position  ;to avoid misunderstandings
 endsubr
;---------------------------------------------------------------
subr fdmg_cp_select_list_cb
 ;get the file name from the text field and copy the list
 nf = num_elem($fdmg_names)
 if nf le 0 then {
  errormess,'empty list?'
  return }
  
 name = xmtextfieldgetstring($fdmg_copy_select_file_text)
 lun = get_lun()
 if openw(lun, name) ne 1 then {
  close, lun
  errormess,'can''t open file\nfor list results'
  return }
 for i = 0, nf-1 do {
  printf, lun, $fdmg_names(i)
 }
 close, lun
 endsubr
;---------------------------------------------------------------
subr fdmg_selected_list_wq
 if defined($fdmgselectedwidget) ne 1 then {
 $fdmgselectedwidget = std2_tlform_setup('Selected MDI Full Disk MG''s','$fdmgselected', wq)
 ;also an erase button here
 bq=xmbutton(wq,'erase all','fdmgselect_eraseall_cb', $f4,'gray')
 xmposition, bq, 265, 0, 80, 30
 bb = xmbutton(wq, 'delete selection','fdmg_select_delete_cb',$f4,'gray') 
 xmposition, bb, 355, 0, 0, 30

 ;put in a button  to copy the list to a text window
 bb = xmbutton(wq,'copy list to file:','fdmg_cp_select_list_cb', $f4,'gray')
 xmposition, bb, 145, 35, 0, 30
 $fdmg_copy_select_file_text = xmtextfield(wq,'junk.list',256,'',$f10,'white')
 xmposition, $fdmg_copy_select_file_text, 265, 35, 220, 30
 
 $fdmgselect_lab1 = xmlabel(wq,'files, images ',$f4,'', 1)
 xmposition, $fdmgselect_lab1, 0, 35

 sq = 'fdmg_selected_entry_cb'
 list = xmlist($fdmgselectedwidget, sq, 20, $f8, 'white', 1)
 xmposition, xtparent(list), 0, 0, 500, 300
 sep = xmseparator($fdmgselectedwidget, 0, 10, 0)
 xmposition, sep, 0, 0, 10, 10
 formvstack, wq, sep, xtparent(list)
 xmattach, xtparent(list), 0, 1,1,0,1
 $fdmgselectedwidget = list	  ;use the actual list for the global
 $fdmgselected_list_of_lists = -1 ;connection to list of lists, if any
 }
 xtpopup, $fdmgselectedwidget
 endsubr
;---------------------------------------------------------------
subr fdmgapplyselect_cb
 ;adapted from the TRACE selector
 ;actually apply the selection and load a list
 ;widget (or append to one) with the results
 ;start up the selected list widget if not already done

 fdmg_selected_list_wq
 
 ;2/28/2002 - also a minimum delta time
 s = xmtextfieldgetstring($fdmg_select_time_txt(8))
 decode_time_str, s, h3, m3, s3
 min_time_bwt = s3 + 60.*m3 + 3600.*h3
 ;need to erase previous? (if any)
 if $fdmg_select_add_flag eq 0 then {
   fdmgselect_eraseall_cb
   $fdmg_select_count = 0
   $fdmg_times = 0
   $fdmg_xc = 0
   $fdmg_yc = 0
   $fdmg_pixsc = 0
   last_tai = 0		;used by the minimum delta t check
  }
 
 $fdmgselect_stop = 0
 change_button_label, $fdmgapplyselect_but, 'STOP', 'red'
 
 ;these data are organized with a directory per month, so the first step
 ;is to figure out the first and last directory to search
 
 time_span, $fdmg_select_time_txt, t1, t2, t1r, t2r
 if t1 gt t2 then { errormess, 'start time is\nbefore end time\ntry again'
 	return }
 tt = t1r
 y1 = fix(xmtextfieldgetstring($fdmg_select_time_txt(0)))
 mon1 = fix(xmtextfieldgetstring($fdmg_select_time_txt(1)))
 ;for MDI, we expect nothing before 1996
 y1 = y1 > 1996

 y2 = fix(xmtextfieldgetstring($fdmg_select_time_txt(4)))
 mon2 = fix(xmtextfieldgetstring($fdmg_select_time_txt(5)))
 if y2 lt 1996 then { errormess, 'no MDI FDMG''s\nbefore 1996\ntry again'
 	return }
 ;we also need the dom, h, and minute
 dhm1 = 10000*fix(xmtextfieldgetstring($fdmg_select_time_txt(2)))
 s = xmtextfieldgetstring($fdmg_select_time_txt(3))
 decode_time_str, s, h, m, sec
 dhm1 = dhm1 + 100*h + m
 dhm2 = 10000*fix(xmtextfieldgetstring($fdmg_select_time_txt(6)))
 s = xmtextfieldgetstring($fdmg_select_time_txt(7))
 decode_time_str, s, h, m, sec
 dhm2 = dhm2 + 100*h + m
 ;construct and loop through all possible directories and then check
 ;each file in a choosen directory
 ym = y1 * 100 + mon1	;normally a 6 digit #
 mm = mon1
 yy = y1
 ym2 = y2 * 100 + mon2
 while (ym le ym2) {
  ;allow a stop from the button
  xtloop, 2
  if $fdmgselect_stop then break	;stop, but keep what we have
  files = getfiles($fdmg_base + sprintf('m%0.6d',ym), 500)
  if isstrarr(files) then {
    files = sort(files)
    ;make sure no strays in here of course
    nf = num_elem(files)
    ind = indgen(lonarr(nf))
    for i=0,nf-1 do { if strpos(files(i), 'fdmg') lt 0 then ind(i) = -1 }
    ind = sieve(ind, ind ge 0)
    files = files(ind)  nf = num_elem(files)
    ;L.S.'s file names will sort in chronological order and the time can
    ;be derived from the name (nice work Louie, it makes it easier)
      ind = indgen(lonarr(nf))
      for i=0,nf-1 do {
	;we assume that the file name has the time in a particular format
	tai = tai_from_file_name(files(i), 0)
	;if we are using a minimum time between choices, need to do more work
	ty, date_from_tai(tai)
	if tai lt t1 or tai gt t2 then ind(i) = -1 else {
         if tai lt (last_tai + min_time_bwt) then ind(i) = -1 else last_tai = tai
	}
	}
      }
      ind = sieve(ind, ind ge 0)
      files = files(ind)  nf = num_elem(files)
  if nf gt 0 then {
   ;add these files to our list
   if $fdmg_select_count then {
    ;append to them
    $fdmg_names = [$fdmg_names, strarr(nf)]
    $fdmg_times = [$fdmg_times, dblarr(nf)]
    $fdmg_xc = [$fdmg_xc, fltarr(nf)]
    $fdmg_yc = [$fdmg_yc, fltarr(nf)]
    $fdmg_pixsc = [$fdmg_pixsc, fltarr(nf)]
   } else {
    ;init the times, positions, and scale
    $fdmg_names = strarr(nf)
    $fdmg_times = dblarr(nf)
    $fdmg_xc = fltarr(nf)
    $fdmg_yc = fltarr(nf)
    $fdmg_pixsc = fltarr(nf)
   }
   for i=0,nf-1 do {
   xmlistadditem, $fdmgselectedwidget, files(i)
   ;also get the times, positions, and scales
   j = i + $fdmg_select_count
   ;and try to get times and such
   iq = data_type(files(i), header, params)
   $fdmg_names(j) = files(i)
   ;only try for times, etc if a FITS file, could extend to others?
   if iq eq 1 then {
    fits_time_decode, header
    $fdmg_times(j) = $data_time_tai
    $fdmg_xc(j) = $data_xcen
    $fdmg_yc(j) = $data_ycen
    $fdmg_pixsc(j) = $data_xscale
   }
   }
   $fdmg_select_count += nf	
   xmsetlabel, $fdmgselect_lab1, sprintf('%d files', $fdmg_select_count)
   } 
  mm = mm + 1   if (mm gt 12) then { mm = 1   yy = yy + 1}
  ym = yy * 100 + mm
 }
 change_button_label, $fdmgapplyselect_but, 'apply selection', 'darkgreen'
 xmsetlabel, $fdmgselect_lab1, sprintf('%d files', $fdmg_select_count)
 $fdmgselect_stop = 1

 ;don't use if no items selected
 if $fdmg_select_count le 0 then {
  errormess,'Sorry, nothing found'
  return }
 ;now we can put it in the list of lists
 add_selection_to_listoflists, 4
 endsubr
;---------------------------------------------------------------
