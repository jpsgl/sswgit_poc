;note - these assume we are running in the browser environment
 ;=============================================================================
subr get_box, w1, ixm1, iym1, ixm2, iym2, wd
 ;where w1 is the X window; (ix1,iy1) and (ix2, iy2) are coords. of the
 ;two ends along the the center line, and wd is the rectangle width
 ;get first position
 xflush
 xwin, w1

 ;this disables the left click movie start/stop action but saves whatever it was
 save_view_cb_case = $view_cb_case(w1) > 0
 $view_cb_case(w1) = -1

 nx=!ixhigh+1	ny=!iyhigh+1
 xwin,-1,nx,ny		;a pixmap which is only as large as needed
 xcopy,w1,-1

 ;wait for down button
 xwin,w1
 xtloop, 2
 !kb=0

 ;get first position
 while (1) {	;wait until left button pushed in the target window
  if !kb ge 256  and xquery(w1) eq 1 then break
  xtloop, 2
 }
 xymov,!ix,!iyhigh-!iy,-1
 ix1=(!ix<!ixhigh)>0		iy1=(!iy<!iyhigh)>0
 ix2=ix1			iy2=iy1
 ;we use (ix1,iy1) and (ix2, iy2) as coords for opposite
 ;corners
 xlast=ix2	ylast=iy2
 iy3 = iy1	ix3 = ix1 +1
 theta = 0.0	;an angle
 ratio = 1.0	;a side ratio

 while (1) { xquery,w1
   ;3 cases for mouse condition, just holding button moves the
   ;second corner, add a shift and both corners move, use control and
   ;we rotate about center
   ;in all cases, the mouse drags ix2,iy2
   if !ix ne ix2 or !iy ne iy2 then {
     ix2=(!ix<!ixhigh)>0   iy2=(!iy<!iyhigh)>0
     if ix2 ne ix1 and iy2 ne iy1 then {	;avoid points, need a finite box
	;cope with the num lock key by doing mod 16 instead of 256
       if !kb%256 eq 0 then {
	 ;just a bare button, don't move first corner and let size change, theta
	 ;is fixed for this mode so compute it now from last set
	 theta = atan2( iy3-iy1, ix3-ix1)	;just need points 1 and 3
	 ;now use the new #2 point
	 cq=cos(theta)		sq=sin(theta)	tq=tan(theta)
	 xq=sq*cq*(iy2-iy1)
	 yq=cq*cq*(ix2-ix1)
	 x3=ix1 + yq + xq	x4=ix2 - yq - xq
	 ix3 = rfix(x3)		ix4 = rfix(x4)
	 iy3=rfix( tq*(x3 - ix1) + iy1)		iy4=rfix( tq*(x4 - ix2) + iy2)
       }
       if !kb%256 eq 1 then {
	 ;move everything with fixed orientation
	 dx = ix2 - xlast	dy = iy2 - ylast
	 ix1 += dx	ix3 += dx	ix4 += dx
	 iy1 += dy	iy3 += dy	iy4 += dy
       }
       if !kb%256 eq 4 then {
	 ;a rotation mode, we fix the center and the ratio of sides, drag P2
	 ;and change P1, first get old angles and old center
	 theta = atan2( iy3 - iy1, ix3 - ix1)
	 phi  = atan2( ylast - iy1, xlast - ix1)
	 xc=0.5 * (ix1 + xlast)	yc=0.5 * (ylast + iy1)
	 ;the center doesn't change here, so change ix1 and iy1
	 ix1 = rfix(2.*xc -ix2)		iy1 =  rfix(2.*yc -iy2)
	 ;note that we lock to nearest pixel, can cause aspect changes, esp for
	 ;very small rectangles
	 ;find delta phi and apply to make a new theta
	 theta  += atan2( iy2 - iy1, ix2 - ix1) - phi
	 ;now compute all corners
	 cq=cos(theta)		sq=sin(theta)	tq=tan(theta)
	 xq=sq*cq*(iy2-iy1)
	 yq=cq*cq*(ix2-ix1)
	 x3=ix1 + yq + xq	x4=ix2 - yq - xq
	 ix3 = rfix(x3)		ix4 = rfix(x4)
	 iy3=rfix( tq*(x3 - ix1) + iy1)		iy4=rfix( tq*(x4 - ix2) + iy2)
       }
       xlast = ix2	ylast = iy2	;save the current point
       ;now draw the box using the 4 corners, note that they go around as 1,3,2,4
       xcopy,-1,w1
       drawboxinview,ix1,ix2,ix3,ix4,iy1,iy2,iy3,iy4,w1
     }
   }
   if !kb eq 0 then break
   if !kb eq 16 then break
 }
 ;compute the return quantities, always make width le length
 wd1 = sqrt( (ix3 - ix1)^2 + (iy3 - iy1)^2)
 wd2 = sqrt( (ix3 - ix2)^2 + (iy3 - iy2)^2)
 if wd1 le wd2 then {
 ixm1=0.5*(ix1+ix3)	iym1=0.5*(iy1+iy3)
 ixm2=0.5*(ix2+ix4)	iym2=0.5*(iy2+iy4)
 wd = wd1 } else {
 ixm1=0.5*(ix2+ix3)	iym1=0.5*(iy2+iy3)
 ixm2=0.5*(ix1+ix4)	iym2=0.5*(iy1+iy4)
 wd = wd2
 }
 ;make the width an integer or we get into trouble later on
 wd=rfix(wd)
 $view_cb_case(w1) = save_view_cb_case	;restore whatever it was
 endsubr
 ;=============================================================================
subr drawboxinview,ix1,ix2,ix3,ix4,iy1,iy2,iy3,iy4,w1
 ;note that they go around as 1,3,2,4
 xdrawline,ix1,iy1,ix3,iy3,w1
 xdrawline,ix3,iy3,ix2,iy2,w1
 xdrawline,ix2,iy2,ix4,iy4,w1
 xdrawline,ix4,iy4,ix1,iy1,w1
 endsubr
 ;=============================================================================
subr drawellipseinview,ix1,ix2,ix3,ix4,iy1,iy2,iy3,iy4,w1
 ;draw the ellipse for the box
 ;note that they go around as 1,3,2,4
 xc = 0.25*(ix1+ix2+ix3+ix4)
 yc = 0.25*(iy1+iy2+iy3+iy4)
 ;we assume this is a rectangle
 a = 0.5*sqrt( (ix3 - ix1)^2 + (iy3 - iy1)^2)
 b = 0.5*sqrt( (ix3 - ix2)^2 + (iy3 - iy2)^2)
 dx = ix3-ix2   dy = iy3-iy2
 theta = -atan2(dx, dy)
 ;use just 51 points
 th = indgen(fltarr(51))*(#2pi/50)
 r = a*b/(sqrt( (b*cos(th-theta))^2 + (a*sin(th-theta))^2 ))
 xx = xc + r * cos(th)
 yy = yc + r * sin(th)
 n = num_elem(xx) - 2
 xdrawline,xx(0:n),yy(0:n),xx(1:*), yy(1:*), w1
 endsubr
 ;=============================================================================
subr drawellipse4vector,ixa,iya,w1
 drawellipseinview,ixa(0),ixa(1),ixa(2),ixa(3),iya(0),iya(1),iya(2),iya(3),w1
 endsubr
 ;=============================================================================
subr ellipsedef, w1, ixa, iya, xc, yc, a, b, theta
 convertfordata, ixa, iya, w1
 ix1 = ixa(0)   ix2 = ixa(1)   ix3 = ixa(2)   ix4 = ixa(3)
 iy1 = iya(0)   iy2 = iya(1)   iy3 = iya(2)   iy4 = iya(3)
 xc = 0.25*(ix1+ix2+ix3+ix4)
 yc = 0.25*(iy1+iy2+iy3+iy4)
 a = 0.5*sqrt( (ix3 - ix1)^2 + (iy3 - iy1)^2)
 b = 0.5*sqrt( (ix3 - ix2)^2 + (iy3 - iy2)^2)
 dx = ix3-ix2   dy = iy3-iy2
 theta = -atan2(dx, dy)
 endsubr
 ;=============================================================================
subr get_box4view, w1, ixa, iya
 ;similar to get_box but returns raw corners since we have to convert
 ;to data space
 ;where w1 is the X window; (ix1,iy1) and (ix2, iy2) are coords. of the
 ;two ends along the the center line, and wd is the rectangle width
 ;get first position
 xflush
 xwin, w1

 ;this disables the left click movie start/stop action but saves whatever it was
 save_view_cb_case = $view_cb_case(w1) > 0
 $view_cb_case(w1) = -1

 nx=!ixhigh+1	ny=!iyhigh+1
 xwin,-1,nx,ny		;a pixmap which is only as large as needed
 xcopy,w1,-1

 ;wait for down button
 xwin,w1
 xtloop, 2
 !kb=0

 ;get first position
 while (1) {	;wait until left button pushed in the target window
  if !kb ge 256  and xquery(w1) eq 1 then break
  xtloop, 2
 }
 xymov,!ix,!iyhigh-!iy,-1
 ix1=(!ix<!ixhigh)>0		iy1=(!iy<!iyhigh)>0
 ix2=ix1			iy2=iy1
 ;we use (ix1,iy1) and (ix2, iy2) as coords for opposite
 ;corners
 xlast=ix2	ylast=iy2
 iy3 = iy1	ix3 = ix1 +1
 theta = 0.0	;an angle
 ratio = 1.0	;a side ratio

 while (1) { xquery,w1
   ;3 cases for mouse condition, just holding button moves the
   ;second corner, add a shift and both corners move, use control and
   ;we rotate about center
   ;in all cases, the mouse drags ix2,iy2
   if !ix ne ix2 or !iy ne iy2 then {
     ix2=(!ix<!ixhigh)>0   iy2=(!iy<!iyhigh)>0
     if ix2 ne ix1 and iy2 ne iy1 then {	;avoid points, need a finite box
	;cope with the num lock key by doing mod 16 instead of 256
       kq = !kb%256
       if kq eq 0 then {
	 ;just a bare button, don't move first corner and let size change, theta
	 ;is fixed for this mode so compute it now from last set
	 theta = atan2( iy3-iy1, ix3-ix1)	;just need points 1 and 3
	 ;now use the new #2 point
	 cq=cos(theta)		sq=sin(theta)	tq=tan(theta)
	 xq=sq*cq*(iy2-iy1)
	 yq=cq*cq*(ix2-ix1)
	 x3=ix1 + yq + xq	x4=ix2 - yq - xq
	 ix3 = rfix(x3)		ix4 = rfix(x4)
	 iy3=rfix( tq*(x3 - ix1) + iy1)		iy4=rfix( tq*(x4 - ix2) + iy2)
       }
       if kq eq 1 or kq eq 2 then {
	 ;move everything with fixed orientation
	 dx = ix2 - xlast	dy = iy2 - ylast
	 ix1 += dx	ix3 += dx	ix4 += dx
	 iy1 += dy	iy3 += dy	iy4 += dy
       }
       if kq gt 2 then {
	 ;a rotation mode, we fix the center and the ratio of sides, drag P2
	 ;and change P1, first get old angles and old center
	 theta = atan2( iy3 - iy1, ix3 - ix1)
	 phi  = atan2( ylast - iy1, xlast - ix1)
	 xc=0.5 * (ix1 + xlast)	yc=0.5 * (ylast + iy1)
	 ;the center doesn't change here, so change ix1 and iy1
	 ix1 = rfix(2.*xc -ix2)		iy1 =  rfix(2.*yc -iy2)
	 ;note that we lock to nearest pixel, can cause aspect changes, esp for
	 ;very small rectangles
	 ;find delta phi and apply to make a new theta
	 theta  += atan2( iy2 - iy1, ix2 - ix1) - phi
	 ;now compute all corners
	 cq=cos(theta)		sq=sin(theta)	tq=tan(theta)
	 xq=sq*cq*(iy2-iy1)
	 yq=cq*cq*(ix2-ix1)
	 x3=ix1 + yq + xq	x4=ix2 - yq - xq
	 ix3 = rfix(x3)		ix4 = rfix(x4)
	 iy3=rfix( tq*(x3 - ix1) + iy1)		iy4=rfix( tq*(x4 - ix2) + iy2)
       }
       xlast = ix2	ylast = iy2	;save the current point
       ;now draw the box using the 4 corners, note that they go around as 1,3,2,4
       xcopy,-1,w1
       drawboxinview,ix1,ix2,ix3,ix4,iy1,iy2,iy3,iy4,w1
     }
   }
   if !kb eq 0 then break
   if !kb eq 16 then break
 }
 ;some changes here compared to the old slicem code, we return
 ;just the raw coords since they have to be converted into data space
 ixa = [ix1, ix2, ix3, ix4]
 iya = [iy1, iy2, iy3, iy4]
 xflush
 wait, 0.5	;try waiting to avoid catching a mouse action that starts playing
 xflush
 $view_cb_case(w1) = save_view_cb_case	;restore whatever it was
 endsubr
 ;=============================================================================
subr get_ellipse, w1, ixa, iya
 ;this is not done yet
 ;similar to get_box but returns raw corners since we have to convert
 ;to data space
 ;where w1 is the X window; (ix1,iy1) and (ix2, iy2) are coords. of the
 ;two ends along the the center line, and wd is the rectangle width
 ;get first position
 xflush
 xwin, w1

 ;this disables the left click movie start/stop action but saves whatever it was
 save_view_cb_case = $view_cb_case(w1) > 0
 $view_cb_case(w1) = -1

 nx=!ixhigh+1	ny=!iyhigh+1
 xwin,-1,nx,ny		;a pixmap which is only as large as needed
 xcopy,w1,-1

 ;wait for down button
 xwin,w1
 xtloop, 2
 !kb=0

 ;get first position
 while (1) {	;wait until left button pushed in the target window
  if !kb ge 256  and xquery(w1) eq 1 then break
  xtloop, 2
 }
 xymov,!ix,!iyhigh-!iy,-1
 ix1=(!ix<!ixhigh)>0		iy1=(!iy<!iyhigh)>0
 ix2=ix1			iy2=iy1
 ;we use (ix1,iy1) and (ix2, iy2) as coords for opposite
 ;corners
 xlast=ix2	ylast=iy2
 iy3 = iy1	ix3 = ix1 +1
 theta = 0.0	;an angle
 ratio = 1.0	;a side ratio

 while (1) { xquery,w1
   ;3 cases for mouse condition, just holding button moves the
   ;second corner, add a shift and both corners move, use control and
   ;we rotate about center
   ;in all cases, the mouse drags ix2,iy2
   if !ix ne ix2 or !iy ne iy2 then {
     ix2=(!ix<!ixhigh)>0   iy2=(!iy<!iyhigh)>0
     if ix2 ne ix1 and iy2 ne iy1 then {	;avoid points, need a finite box
	;cope with the num lock key by doing mod 16 instead of 256
       kq = !kb%256
       if kq eq 0 then {
	 ;just a bare button, don't move first corner and let size change, theta
	 ;is fixed for this mode so compute it now from last set
	 theta = atan2( iy3-iy1, ix3-ix1)	;just need points 1 and 3
	 ;now use the new #2 point
	 cq=cos(theta)		sq=sin(theta)	tq=tan(theta)
	 xq=sq*cq*(iy2-iy1)
	 yq=cq*cq*(ix2-ix1)
	 x3=ix1 + yq + xq	x4=ix2 - yq - xq
	 ix3 = rfix(x3)		ix4 = rfix(x4)
	 iy3=rfix( tq*(x3 - ix1) + iy1)		iy4=rfix( tq*(x4 - ix2) + iy2)
       }
       if kq eq 1 or kq eq 2 then {
	 ;move everything with fixed orientation
	 dx = ix2 - xlast	dy = iy2 - ylast
	 ix1 += dx	ix3 += dx	ix4 += dx
	 iy1 += dy	iy3 += dy	iy4 += dy
       }
       if kq gt 2 then {
	 ;a rotation mode, we fix the center and the ratio of sides, drag P2
	 ;and change P1, first get old angles and old center
	 theta = atan2( iy3 - iy1, ix3 - ix1)
	 phi  = atan2( ylast - iy1, xlast - ix1)
	 xc=0.5 * (ix1 + xlast)	yc=0.5 * (ylast + iy1)
	 ;the center doesn't change here, so change ix1 and iy1
	 ix1 = rfix(2.*xc -ix2)		iy1 =  rfix(2.*yc -iy2)
	 ;note that we lock to nearest pixel, can cause aspect changes, esp for
	 ;very small rectangles
	 ;find delta phi and apply to make a new theta
	 theta  += atan2( iy2 - iy1, ix2 - ix1) - phi
	 ;now compute all corners
	 cq=cos(theta)		sq=sin(theta)	tq=tan(theta)
	 xq=sq*cq*(iy2-iy1)
	 yq=cq*cq*(ix2-ix1)
	 x3=ix1 + yq + xq	x4=ix2 - yq - xq
	 ix3 = rfix(x3)		ix4 = rfix(x4)
	 iy3=rfix( tq*(x3 - ix1) + iy1)		iy4=rfix( tq*(x4 - ix2) + iy2)
       }
       xlast = ix2	ylast = iy2	;save the current point
       ;now draw the box using the 4 corners, note that they go around as 1,3,2,4
       xcopy,-1,w1
       ;;drawboxinview,ix1,ix2,ix3,ix4,iy1,iy2,iy3,iy4,w1
       drawellipseinview,ix1,ix2,ix3,ix4,iy1,iy2,iy3,iy4,w1
     }
   }
   if !kb eq 0 then break
   if !kb eq 16 then break
 }
 ;some changes here compared to the old slicem code, we return
 ;just the raw coords since they have to be converted into data space
 ixa = [ix1, ix2, ix3, ix4]
 iya = [iy1, iy2, iy3, iy4]
 xflush
 wait, 0.5	;try waiting to avoid catching a mouse action that starts playing
 xflush
 $view_cb_case(w1) = save_view_cb_case	;restore whatever it was
 endsubr
 ;=============================================================================
