func yohkoh_widget(dummy)
; sets up a widget for browsing through the Yohkoh full-disk images.
; LS 13apr00
 if defined($yohkohwidget) eq 0 then {
   $sxt_base = getenv('ANA_SXT_DIR')
   $yohkohwidget = xmtoplevel_form(0,0,'Yohkoh SXT Full-Disk Images',5,5,5,5)
   add_to_hideables,$yohkohwidget
   ; dismiss and help buttons
   wq = topbuttonsetupsanslink_tl('$yohkoh')

   f0 = xmform($yohkohwidget)
   l1 = xmlabel(f0,'Target window:',$f5)
   xmattach,l1,f0,1,0,1,1	 ; label at top left of form
   $yohkoh_trgt = xmtextfield(f0,'10',2,'',$f3)
   xmsize,$yohkoh_trgt,40,0
   xmsetcolors,$yohkoh_trgt,'white'
   xmattach,$yohkoh_trgt,l1,1,0,0,0

   sep = xmseparator($yohkohwidget)

   l2 = xmlabel($yohkohwidget,'Yohkoh SXT\nFull-Disk Images',$f5)

   f1 = xmform($yohkohwidget)
   b1 = xmbutton(f1,'Months','yohkoh_toggle_months_cb')
   $yohkoh_listmonths = xmlist(f1,'yohkoh_month_cb',10,$f8,'white',1)
   formvstack,b1,$yohkoh_listmonths
   xmattach,b1,f1,1,1,1,0
   xmattach,xtparent($yohkoh_listmonths),b1,0,0,1,0
   xmattach,xtparent($yohkoh_listmonths),f1,1,1,0,1

   f2 = xmform($yohkohwidget)
   b2 = xmbutton(f2,'Days','yohkoh_toggle_days_cb')
   $yohkoh_listdays = xmlist(f2,'yohkoh_day_cb',10,$f8,'white',1)
   formvstack,b2,$yohkoh_listdays
   xmattach,b2,f2,1,1,1,0
   xmattach,xtparent($yohkoh_listdays),b2,0,0,1,0
   xmattach,xtparent($yohkoh_listdays),f2,1,1,0,1

   f3 = xmform($yohkohwidget)
   b3 = xmbutton(f3,'Images','yohkoh_toggle_images_cb')
   $yohkoh_listimages = xmlist(f3,'yohkoh_image_cb',10,$f8,'white',1)
   formvstack,b3,$yohkoh_listimages
   xmattach,b3,f3,1,1,1,0
   xmattach,xtparent($yohkoh_listimages),b3,0,0,1,0
   xmattach,xtparent($yohkoh_listimages),f3,1,1,0,1

   formvstack,wq,f0,sep,l2,f1,f2,f3

   xmattach,f1,l2,0,0,1,0
   xmattach,f2,f1,0,0,1,0
   xmattach,f3,f2,0,0,1,0
   xmattach,f3,0,0,0,0,1

   yohkoh_toggle_days_cb
   yohkoh_toggle_images_cb

   xtpopdown,$yohkoh_listmonths
   years = sort(getdirects($sxt_base, 10000))
   nd = num_elem(years)
   for i = 0, nd - 1 {
     year = years(i)
     ; ANA 1 has no comparison of strings, so we must map to a byte array 
     b = bmap(year)
     if b(0) ge bmap('1') and b(0) lt bmap('3') then { ; starts with 1 or 2
       for month = 1,12 {	 ; all months of the year
         pattern = sprintf('ssc%s%02d', year(0:3), month)
	 s = getmatchedfiles(pattern, $sxt_base + year, 2)
	 if isscalar(s) eq 0 then
 	   xmlistadditem,$yohkoh_listmonths,pattern(3:*)
       }
     }
   }
 }
 xtpopup,$yohkohwidget
 return,$yohkohwidget
endfunc
; -----------------------------------------------------------
subr yohkoh_month_cb
; yohkoh_toggle_months_cb,0	 ; hide months list
 xtpopdown,$yohkoh_listdays	 ; so list loading goes faster
 yohkoh_month = $list_item
 yohkoh_year = yohkoh_month(0:3)
 xmlistdeleteall,$yohkoh_listdays
 for i = 1,31 {			 ; all days
   pattern = sprintf('ssc%s%02d', yohkoh_month, i)
   s = getmatchedfiles(pattern, $sxt_base + yohkoh_year, 2)
   if isscalar(s) eq 0 then 	 ; found some
     xmlistadditem,$yohkoh_listdays,pattern(3:*)
 }
 xtpopup,$yohkoh_listdays
 yohkoh_toggle_days_cb,1
endsubr
; -----------------------------------------------------------
subr yohkoh_day_cb
; yohkoh_toggle_days_cb,0	 ; hide days list
 xtpopdown,$yohkoh_listimages	 ; so list loading goes faster
 yohkoh_year = $list_item(0:3)
 xmlistdeleteall,$yohkoh_listimages
 pattern = 'ssc' + $list_item
 t,pattern
 s = sort(getmatchedfiles(pattern, $sxt_base + yohkoh_year))
 if isscalar(s) eq 0 then { 	 ; found some
   for i = 0,num_elem(s) - 1 {
     h = removepath(s(i))
     xmlistadditem,$yohkoh_listimages,h(3:17)
   }
 }
 xtpopup,$yohkoh_listimages
 yohkoh_toggle_images_cb,1
endsubr
; -----------------------------------------------------------
subr yohkoh_image_cb
 $selected_file = $sxt_base + $list_item(0:3) + '/ssc' + $list_item + '.fits'
 win = eval(xmtextgetstring($yohkoh_trgt))
 t,'win: ',win
 read_file,win
 h = 'SXT Full-Disk Image\n' + date_from_tai($view_tai(win))
 $view_text(win) = h
 xmtextsetstring,$view_text_widget(win),h
endsubr
; -----------------------------------------------------------
subr yohkoh_toggle_months_cb,state
 if defined(state) then
   isshown = state
 else if defined(isshown) eq 0 then
   isshown = 0
 else
   isshown = not(isshown)
 if isshown then
   xtmanage,xtparent($yohkoh_listmonths)
 else
   xtunmanage,xtparent($yohkoh_listmonths)
endsubr
; -----------------------------------------------------------
subr yohkoh_toggle_days_cb,state
 if defined(state) then
   isshown = state
 else if defined(isshown) eq 0 then
   isshown = 0
 else
   isshown = not(isshown)
 if isshown then
   xtmanage,xtparent($yohkoh_listdays)
 else
   xtunmanage,xtparent($yohkoh_listdays)
endsubr
; -----------------------------------------------------------
subr yohkoh_toggle_images_cb,state
 if defined(state) then
   isshown = state
 else if defined(isshown) eq 0 then
   isshown = 0
 else
   isshown = not(isshown)
 if isshown then
   xtmanage,xtparent($yohkoh_listimages)
 else
   xtunmanage,xtparent($yohkoh_listimages)
endsubr
; -----------------------------------------------------------
