;===========================================================================
func rigid,x1,x2,lowin,hin,nin
;returns a remapped version of x2 which is aligned with x1
t1=!cputime
if !narg ge 3 then  lowcut=lowin  else lowcut=0
if !narg ge 4 then  hicut=hin  else hicut=0
if !narg ge 5 then  n=nin  else n=0
;rigid align x2 to match x1
nx=dimen(x1,0)	ny=dimen(x1,1)
if n eq 0 then { n = nx<ny }
;use a centered subarea for rigid align if nin specified, also allow the images
;to be different sizes, output will be reference size
if n ne 0 then {
i1r=(nx-n)/2
i2r=i1r+n-1
j1r=(ny-n)/2
j2r=j1r+n-1
;assume same size and then check
i1=i1r		i2=i2r		j1=j1r		j2=j2r
nx2=dimen(x2,0)	 ny2=dimen(x2,1)
if nx2 ne nx then { i1=(nx2-n)/2	i2=i1+n-1 }
if ny2 ne ny then { j1=(ny2-n)/2	j2=j1+n-1 }

c=cross2dsub(x1(i1r:i2r,j1r:j2r),x2(i1:i2,j1:j2),lowcut,hicut)
} else c=cross2dsub(x1,x2,lowcut,hicut)
xc=maxloc(c)
dx=xc(0)	dy=xc(1)
;note minus sign on c below, since we really want the max
if (dx gt 0 and dx lt (dimen(c,0)-1) and dy gt 0 and dy lt (dimen(c,1)-1)) then {
  ;note minus sign on c below, since we really want the max
  getmin9,-c((dx-1):(dx+1),(dy-1):(dy+1)),iq,jq
} else {
  ty,'alignment off edge!'   iq=0  jq=0
}
dx=dx+iq-dimen(c,0)/2
dy=dy+jq-dimen(c,1)/2
;check if they need a multiplicative factor (because of high cutoff)
if hicut gt 0 then { if n eq 0 then {
	dx=dx*.5*nx/hicut  dy=dy*.5*ny/hicut } else {
	dx=dx*.5*n/hicut   dy=dy*.5*n/hicut } }
ty,'offset =',dx,dy
$dx = dx	$dy = dy
 iq=i1-i1r	jq=iq+nx
 cx=[ [[iq,jq]],[[iq,jq]] ] - dx
 iq=j1-j1r	jq=iq+ny
 cy=[ [[iq,iq]],[[jq,jq]] ] - dy
y=regrid3(x2,cx,cy,nx,ny)
t2=!cputime
ty,'time in rigid =',t2-t1
return,y
endfunc
;===========================================================================
subr offset,x1,x2,lowin,hin,nin
;like rigid, but just computes the offset, left in $dx and $dy rather than
;passed back as an argument
t1=!systime
if !narg ge 3 then  lowcut=lowin  else lowcut=0
if !narg ge 4 then  hicut=hin  else hicut=0
if !narg ge 5 then  n=nin  else n=0
;rigid align x2 to match x1
nx=dimen(x1,0)	ny=dimen(x1,1)
;use a centered subarea for rigid align if nin specified
if n ne 0 then {
i1=(nx-n)/2
i2=i1+n-1
j1=(ny-n)/2
j2=j1+n-1
c=cross2dsub(x1(i1:i2,j1:j2),x2(i1:i2,j1:j2),lowcut,hicut)
} else c=cross2dsub(x1,x2,lowcut,hicut)
xc=maxloc(c)
dx=xc(0)	dy=xc(1)
if (dx gt 0 and dx lt (dimen(c,0)-1) and dy gt 0 and dy lt (dimen(c,1)-1)) then {
;note minus sign on c below, since we really want the max
 getmin9,-c((dx-1):(dx+1),(dy-1):(dy+1)),iq,jq } else {
 ty,'alignment off edge!'   iq=0  jq=0 }
dx=dx+iq-dimen(c,0)/2
dy=dy+jq-dimen(c,1)/2
;check if they need a multiplcative factor (because of high cutoff)
if hicut gt 0 then { if n eq 0 then {
	dx=dx*.5*nx/hicut  dy=dy*.5*ny/hicut } else {
	dx=dx*.5*n/hicut   dy=dy*.5*n/hicut } }
ty,'offset =',dx,dy
$dx = dx	$dy = dy
;also compute for a smoothed CC
c=smooth2(c,5)
dx=xc(0)	dy=xc(1)
if (dx gt 0 and dx lt (dimen(c,0)-1) and dy gt 0 and dy lt (dimen(c,1)-1) then {
 getmin9,-c((dx-1):(dx+1),(dy-1):(dy+1)),iq,jq } else {
 ty,'alignment off edge!'   iq=0  jq=0 }
dx=dx+iq-dimen(c,0)/2
dy=dy+jq-dimen(c,1)/2
if hicut gt 0 then { if n eq 0 then {
	dx=dx*.5*nx/hicut  dy=dy*.5*ny/hicut } else {
	dx=dx*.5*n/hicut   dy=dy*.5*n/hicut } }
ty,'smooth offset =',dx,dy
$dxs = dx	$dys = dy
t2=!systime
ty,'time in offset =',t2-t1
endsubr
;===========================================================================
func rigidapod,x1,x2,lowin,hin,nin
t1=!cputime
if !narg ge 3 then  lowcut=lowin  else lowcut=0
if !narg ge 4 then  hicut=hin  else hicut=0
if !narg ge 5 then  n=nin  else n=0
;rigid align x2 to match x1
nx=dimen(x1,0)	ny=dimen(x1,1)
;use a centered subarea for rigid align if nin specified
if n ne 0 then {
i1=(nx-n)/2
i2=i1+n-1
j1=(ny-n)/2
j2=j1+n-1
c=cross2dsub(bhapod(x1(i1:i2,j1:j2)),bhapod(x2(i1:i2,j1:j2)),lowcut,hicut)
} else c=cross2dsub(bhapod(x1),bhapod(x2),lowcut,hicut)
xc=maxloc(c)
dx=xc(0)	dy=xc(1)
;note minus sign on c below, since we really want the max
if (dx gt 0 and dx lt (dimen(c,0)-1) and dy gt 0 and dy lt (dimen(c,1)-1)) then {
;note minus sign on c below, since we really want the max
 getmin9,-c((dx-1):(dx+1),(dy-1):(dy+1)),iq,jq } else {
 ty,'alignment off edge!'   iq=0  jq=0 }
dx=dx+iq-dimen(c,0)/2
dy=dy+jq-dimen(c,1)/2
ty,'offset =',dx,dy
$dx = dx	$dy = dy
cx=[ [[0,nx]],[[0,nx]] ]-dx
cy=[ [[0,0]],[[ny,ny]] ]-dy
y=regrid3(x2,cx,cy,nx,ny)
t2=!cputime
ty,'time in rigid =',t2-t1
return,y
endfunc
;===========================================================================
func rcross(x1,x2,lowin,hin,nin)
t1=!cputime
if !narg ge 3 then  lowcut=lowin  else lowcut=0
if !narg ge 4 then  hicut=hin  else hicut=0
if !narg ge 5 then  n=nin  else n=0
;returns part of the cross correlation, used to evaluate alignments
nx=dimen(x1,0)	ny=dimen(x1,1)
;use a centered subarea for rigid align if nin specified
if n ne 0 then {
i1=(nx-n)/2
i2=i1+n-1
j1=(ny-n)/2
j2=j1+n-1
c=cross2dsub(x1(i1:i2,j1:j2),x2(i1:i2,j1:j2),lowcut,hicut)
} else c=cross2dsub(x1,x2,lowcut,hicut)
xc=maxloc(c)
dx=xc(0)	dy=xc(1)
;note minus sign on c below, since we really want the max
if (dx gt 0 and dx lt (dimen(c,0)-1) and dy gt 0 and dy lt (dimen(c,1)-1)) then {
;note minus sign on c below, since we really want the max
 getmin9,-c((dx-1):(dx+1),(dy-1):(dy+1)),iq,jq } else {
 ty,'alignment off edge!'   iq=0  jq=0 }
dx=dx+iq-dimen(c,0)/2
dy=dy+jq-dimen(c,1)/2
;check if they need a multiplcative factor (because of high cutoff)
if hicut gt 0 then { if n eq 0 then {
	dx=dx*.5*nx/hicut  dy=dy*.5*ny/hicut } else {
	dx=dx*.5*n/hicut   dy=dy*.5*n/hicut } }
ty,'offset =',dx,dy
t2=!cputime
ty,'time in rcross =',t2-t1
return,c
endfunc
;===========================================================================
func rcrossapod,x1,x2,lowin,hin,nin
t1=!cputime
if !narg ge 3 then  lowcut=lowin  else lowcut=0
if !narg ge 4 then  hicut=hin  else hicut=0
if !narg ge 5 then  n=nin  else n=0
;returns part of the cross correlation, used to evaluate alignments
nx=dimen(x1,0)	ny=dimen(x1,1)
;use a centered subarea for rigid align if nin specified
if n ne 0 then {
i1=(nx-n)/2
i2=i1+n-1
j1=(ny-n)/2
j2=j1+n-1
c=cross2dsub(bhapod(x1(i1:i2,j1:j2)),bhapod(x2(i1:i2,j1:j2)),lowcut,hicut)
} else c=cross2dsub(bhapod(x1),bhapod(x2),lowcut,hicut)
xc=maxloc(c)
dx=xc(0)	dy=xc(1)
;note minus sign on c below, since we really want the max
if (dx gt 0 and dx lt (dimen(c,0)-1) and dy gt 0 and dy lt (dimen(c,1)-1)) then {
;note minus sign on c below, since we really want the max
 getmin9,-c((dx-1):(dx+1),(dy-1):(dy+1)),iq,jq } else {
 ty,'alignment off edge!'   iq=0  jq=0 }
dx=dx+iq-dimen(c,0)/2
dy=dy+jq-dimen(c,1)/2
ty,'offset =',dx,dy
t2=!cputime
ty,'time in rcross =',t2-t1
return,c
endfunc
;===========================================================================
