func fits_key(head, key)
 ;scans a fits header (head) for a key word (key) and returns its value
 ;returns a f*4 value or a string depending on what is found in the line
 ;intended to work with either header style
 result = 'undefined'	;in case we don't find anything
 if isstrarr(head) eq 0 then {
   ;not a string array, assume the old style
   hb = bmap(head)
   nl = num_elem(hb)/80
   if nl le 0 then return, 0
   redim, hb, 80, nl
   s2 = ''
   for i=1,nl-1 do {
     s = smap(hb(*,i))
     if strpos(upcase(s(0:7)),upcase(key)) eq 0 then { s2 = strskp(s,'=') break }
   }
 } else {
   nl = num_elem(head)
   for i=0,nl-1 do {
     s2 = ''
     s = head(i)
     if strpos(upcase(s(0:7)),upcase(key)) eq 0 then { s2 = strskp(s,'=') break }
   }
 }
 ;get anything?
 if num_elem(s2) eq 0 then return, result

 ;skip blanks or tabs
 s2 = skipc(s2,' \t')
 ;check is a string value, these must start with '
 if num_elem(s2) eq 0 then { result = 0  return, result }
 if s2(0) eq '''' then {
   if strlen(s2) le 1 then return, s2
   result = strtrim(s2(1:*))  n = num_elem(result)
   if result(n-1) eq '/' then { result = strtrim(result(0:(n-2)))  n = num_elem(result) }
   if n le 1 then return, result
   if result(n-1) eq '''' then result = strtrim(result(0:(n-2)))
 } else {
 ;presumably a number, just float and return a floating point value
 result = float(s2)
 }
 return, result
 endfunc
;======================================================
func fits_key_string(head, key)
 ;scans a fits header (h) for a key word (key) and returns the string
 ;differs from fits_key in that a string is always returned so the
 ;caller can decide what to do, needed for some "illegal" key values
 ;intended to work with either header style
 ;also don't require an = sign
 result = 'undefined'	;in case we don't find anything
 ;;dump, head, key
 s2 = ''
 if isstrarr(head) eq 0 then {
   ;not a string array, assume the old style
   hb = bmap(head)
   nl = num_elem(hb)/80
   if nl le 0 then return, 0
   redim, hb, 80, nl
   for i=1,nl-1 do {
     s = smap(hb(*,i))
     if strpos(upcase(s(0:7)),upcase(key)) eq 0 then {
       if strpos(s,'=') ge 0 then s2 = strskp(s,'=') else  s2 = s(8:*)
       break
     }
   }
 } else {
   nl = num_elem(head)
   ;;ty,'nl =', nl
   for i=0,nl-1 do {
   s = head(i)
     if strpos(upcase(s(0:7)),upcase(key)) eq 0 then {
       if strpos(s,'=') ge 0 then s2 = strskp(s,'=') else  s2 = s(8:*)
       break
     }
   }
 }
 ;get anything?
 if num_elem(s2) eq 0 then return, result
 result = s2
 return, result
 endfunc
;======================================================
func fitsKeyMatch(head, keys, crits)
 ;given a FITS file header in head and a string array of keys, find these
 ;keys in head and check against the crits. The latter must be a symarr
 ;that matches keys. The type of each symbol determines the match critera.
 ;This function returns 1 if we don't match everything and zero if we pass
 ;all the matches. It will find a failure quicker than a match.
 
 nk = num_elem(keys)
 for k = 0, nk -1 do {
   ks = keys(k)
   s = fits_key_string(head, ks)
   ;;ty,'key: ',ks, '  RHS string: ', s
   if s eq 'undefined' then return, 1
   c = crits(k)
   ;what we do depends on type of c
   if isstring(c) then {
     ;the string in c must be contained in s, if not we failed the test
     if strcontains(s, c) eq 0 then return, 1
   } else if isstrarr(c) then {
     ;here we have a set of strings, if any match we pass
     ns = num_elem(c)
     flag = 1
     for i=0,ns-1 do { if strcontains(s, c(i)) then { flag = 0  break } }
     ;get a match ?, flag will still be 1 if we didn't
     if (flag) then return, 1
   } else if isarray(c) then {
     ;this is done as a range, it could probably be extended to a set of
     ;ranges if this is ever desired, now it just requires at least 2 values
     ;and only looks at the first 2 if there are more. If there is just one
     ;value we treat the same as the scalar case
     ns = num_elem(c)
     val = float(s)
     if ns eq 1 then {
       ;like a scalar, values must match
       if c(0) ne val then return, 1
     } else {
       x0 = c(0)   x1 = c(1)
       if x1 lt x0 then exchange, x0, x1
       if val lt x0 or val gt x1 then return, 1
     }
   } else if isscalar(c) then {
     ;for a scalar, the value of the returned string must match
     if c ne float(s) then return, 1
   }
 }
 return, 0
 endfunc
;======================================================
block testfitsKeyMatch
 keys = ['WAVE', 'EXP0', 'XCEN', 'NBFW']
 n = num_elem(keys)
 crits = symarr(n)
 crits(0) = ['5250', '5172', '6302']
 crits(1) = [0.010, 1.00]
 crits(2) = [-10,10]
 crits(3) = [70, 85]

 endblock
;======================================================
 
