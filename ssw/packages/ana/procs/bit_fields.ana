func bit_fields_extract(x, starts, widths)
 ;extracts bit fields from a i*2 array x
 ;this is very inefficient but only normally used for several words
 ;starts and widths must agree
 if dmatch(starts, widths) eq 0 then { ty,'inputs failed match test' return,0 }
 n = num_elem(starts)
 results = intarr(n)
 if symdtype(x) ne 1 then {
 	ty,'warning, changing input to word type via word function'
	x = word(x)
 }
 for i=0,n-1 do {
 j = starts(i)/16	;word #
 xq = x(j)
 j = starts(i)%16	;bit #
 ;do a shift via a divide (ouch) and a mask
 xq = xq / (word(2^j)
 xq = xq and ( word(2^widths(i) -1))
 results(i) = xq
 }
 return, results
 endfunc
 ;=======================================================================
subr set_var(x,value)
 ;trivial, used when x is an evaluated string since we can't
 ;do eval('string') = value
 x = value
 endsubr
 ;=======================================================================
subr extract_bits(xin, template_file)
 ;7/18/96 for extracting parameters from a trace frame definition
 ;template_file is an ASCII file containing the list of symbols and
 ;the starting bit numbers (starts) and field widths (widths)
 ;xin is the parameter block, should be I*2
 ;it is unwise for xin to be one of the names, in general they
 ;should be global
 
 lun = get_lun()
 openr, lun, template_file
 
 s = ''
 while (readf(lun,s) eq 1) {
 ;ty,s
 name = token(s,1)
 if name ne '' then {
 if name(0) ne ';' and name(0) ne '%' then {
 ;ty,'token name = ',name
 start = fix(token(s,2)
 width = fix(token(s,3)
 if width lt 0 then { sign_ext = 1  width = -width } else sign_ext = 0
 j = start/16		;word #
 xq = xin(j) and long(0xffff)	;need to pad upper bits with zeros
 ;ty,'initial xq =', xq, ifstring('%#10.4x', xq)
 j = start%16		;bit #
 ;do a shift via a divide (ouch) and a mask
 xq = xq / (word(2^j)
 xq = xq and ( word(2^width - 1))
 ;ty,'xq before sign check', xq, ifstring('%#10.4x', xq)
 ;now check if the sign must be extended
 if sign_ext eq 1 then {
  if (xq and fix(2^(width-1))) ne 0 then {
   xq = xq or (0xffffffff - (fix(2^width)-1))
  }
 }
 ;ty,'final xq', xq, ifstring('%#10.4x', xq)
 set_var, eval(name), xq
 }}}
 close, lun
 endsubr
 ;=======================================================================
subr insert_bits(xin, template_file)
 ;7/19/96 the inverse of extract_bits, xin must be defined and large enough
 ;or bad things will happen, it gets zeroed so we can do an OR with the
 ;variable, hence we assume that everything gets set here (not in 2 parts
 ;or something)
 ;also, the variables listed in the template must exist
 ;we work with 32 bit longs to set up the 16 bit results in order to use
 ;multiplies and divides for shifts
 
 ;ty,'in insert_bits'
 t1 = !systime
 zero, xin
 lun = get_lun()
 ty,'lun =', lun
 openr,lun,template_file
 t2 = !systime
 
 s = ''
 ic = 0
 while (readf(lun,s) eq 1) {
 ic = ic + 1
 token, s, name, s
 if name ne '' then {
 if name(0) ne ';' and name(0) ne '%' then {
 token, s, t, s
 start = fix(t)
 token, s, t, s
 width = abs(fix(t)
 xq = long( eval(name) and ( long(2^width - 1)) )
 j = start%16		;bit #
 xq = xq * (word(2^j)
 j = start/16		;word #
 xin(j) = xin(j) or xq
 }}
 }
 t3 = !systime
 close, lun
 t4 = !systime
 ty,'ic =', ic
 ty,'open time =',t2-t1
 ty,'loop time =', t3-t2
 ty,'close time =', t4-t3
 endsubr
 ;=======================================================================
 