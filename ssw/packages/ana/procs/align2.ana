func differ,x
 ;+
 ;function differ, call is D=DIFFER(X)
 ;return differences between elements in the first dimension only
 ;2 and 3-D arrays are handled as series of 1-D's
 ;-
 ND=NUM_DIM(X)
 NX=DIMEN(X,0)
 NCASE ND-1
 { DX=X(1:*)-X(0:(NX-2))		RETURN,DX	}
 { DX=X(1:*,*)-X(0:(NX-2),*)	RETURN,DX	}
 { DX=X(1:*,*,*)-X(0:(NX-2),*,*)	RETURN,DX	}
 ELSE { TYPE,'*** error - DIFFER is not intended for dimensions>3' }
 ENDCASE
 ENDFUNC
 ;===========================================================================
func first_elem,x
 ;gets the first element of the input or just the value if not an array
 if symclass(x) eq 4 then xq=x(0) else xq=x
 return,xq
 ENDFUNC
 ;===========================================================================
subr match2,mag,dang,xp,yp,x,y
 ;takes 2 set of coords. from 2 images and derives scale factor and angle
 dx=differ(x)	dy=differ(y)	dxp=differ(xp)	dyp=differ(yp)
 lengtha=sqrt(dxp^2+dyp^2)
 lengthb=sqrt(dx^2+dy^2)
 mag=lengtha/lengthb
 anglea=atan2(dyp,dxp)*#r.d
 angleb=atan2(dy,dx)*#r.d
 dang=anglea-angleb
 type,'lengtha, lengthb, mag =',lengtha,lengthb,mag
 type,'in degress they are..'
 type,'anglea, angleb, dang  =',anglea,angleb, dang
 endsubr
 ;===========================================================================
subr match2fit,mag,dang,xp,yp,x,y,nx,ny
 ;takes sets of matching coords. from 2 images and derives scale factor and angle
 ;using a best fit procedure, also computes control grids in $cx, $cy
 ;uses mean positions as reference points
 ;also computes control grids in $cx, $cy
 ;11/17/98 notes - xp, yp are the ref. here, we want to avoid a possible
 ;mirror problem. Just two points is symmetric and we can make a decision
 ;on whether we have a mirror in the input by checking both solutions
 ;and taking the better of the two.
 x0=mean(x)	y0=mean(y)	xp0=mean(xp)	yp0=mean(yp)
 n=dimen(xp,0)
 n2 = 2*n
 dump, x,y,xp,yp
 if n ne dimen(x,0) or n ne dimen(y,0) or n ne dimen(yp,0) then {
	 ty,'MATCH2FIT: inputs must have same size' }
 ;we are solving an over determined linear system with 2 variables
 ; v1 = mag * cos(theta)  and v2 = mag * sin(theta)
 ;when v1 and v2 are determined, we can obtain mag and theta
 ;the rhs vector has 2*n elements as follows:
 b = [[ x - x0, y - y0 ]]
 bz = zero(fltarr(n))
 ;note that the primes (with p) are the reference coordinates
 ;the matrix equation is Ax = b, x = ( v1, v2), A is a 2*n by 2 matrix
 A = [ [[xp - xp0, yp - yp0]], [[ -yp + yp0, xp - xp0]] ]
 ;now set up the matrix and rhs for the least squares solution
 ;(it is this simple, believe it)
 AT = A(>1,>0)
 AP= matmul( AT, A)
 decomp, AP
 rhs = matmul( AT, b)
 dsolve, AP, rhs
 v=rhs
 redim,rhs,2
 dang=atan2(rhs(1),rhs(0))*#r.d
 mag=sqrt(total(rhs*rhs))
 ty,'mag, dang =', mag, dang
 ;now compute the errors, this is Ax - b
 ;use v for solution vector rather than x (used variable x for image)
 
 error=matmul(A, v) - b
 ;ty,'b',b
 ;dump,b,a,v
 ;ty,'solution', matmul(A, v)
 ;ty,'solution with offset', matmul(A, v) + [[bz+x0, bz+y0]]
 ;ty,'errors', error
 xerr=error(0:(n-1))	yerr=error(n:*)
 derror=sqrt(xerr*xerr+yerr*yerr))
 ty,'distance errors =', derror
 ;get a total error measure
 terr = total(derror)
 
 ;now try the mirror case, this changes b
 b = [[ x - x0, -(y - y0)]]
 rhs = matmul( AT, b)
 dsolve, AP, rhs
 vm=rhs
 redim,rhs,2
 error2=matmul(A, vm) - b
 ;ty,'b',b
 ;ty,'solution', matmul(A, vm)
 ;ty,'solution with offset', matmul(A, vm) + [[bz+x0, bz-y0]]
 ;ty,'errors for mirror', error2
 xerr=error2(0:(n-1))	yerr=error2(n:*)
 derror2=sqrt(xerr*xerr+yerr*yerr))
 ty,'distance errors for mirror =', derror2
 ;get a total error measure
 terr2 = total(derror2)
 ;decide who wins
 if terr2 lt terr then {
  ;mirror wins, re-do dang and mag
  ty,'mirror looks better'
  dang=atan2(rhs(1),rhs(0))*#r.d
  mag=sqrt(total(rhs*rhs))
  ty,'mag, dang =', mag, dang
  mirror_flag = 1
  ;switch, v, vm
  v = vm
 } else mirror_flag = 0
 
 ;get the initial offset in the ref image, this is coords that corr. to (0,0)
 ;in the second image
 A0 = [ [[ - xp0, - yp0]], [[ yp0, - xp0]] ]
 offset = matmul(A0, v) + [ [x0, y0 ]]
 ty,'offset =', offset
 ;the control grid for a nx by ny image, 4 values
 AX = [ [ [0,nx,0,nx]-xp0 ] , [ -[0,0,ny,ny]+yp0] ]
 cgx = matmul(AX, v) + x0
 AY = [ [ [0,0,ny,ny]-yp0 ] , [ [0,nx,0,nx]-xp0] ]
 if mirror_flag then cgy = -matmul(AY, v) + y0 else cgy = matmul(AY, v) + y0
 $cx=cgx		redim,$cx,2,2		$cy=cgy		redim,$cy,2,2
 ty,'cgx=',cgx
 ty,'cgy=',cgy
 endsubr
 ;===========================================================================
SUBR MATCH3,MAGX,MAGY,ANG,XP,YP,X,Y
 ;given 3 points in each image, derive a magx, magy, and angle to apply
 ;to second image (i.e., the first is the reference image)
 DX=float(DIFFER(X))	DY=float(DIFFER(Y))
 DXP=float(DIFFER(XP))	DYP=float(DIFFER(YP))
 ;calculate the 4 coefficients a,b,c,d
 ;these are a=magx*cos(ang), b=magy*sin(ang), c=magy*cos(ang), d=magy*sin(ang)
 ;first a and d
 CASE
 DY(0) EQ 0.0:{ a=dxp(0)/dx(0)	d=(dxp(1)-a*dx(1))/dy(1) }
 DY(1) EQ 0.0:{ a=dxp(1)/dx(1)	d=(-dxp(0)+a*dx(0))/dy(0) }
 ELSE { xq=dy(0)/dy(1)	a=(dxp(0)-dxp(1)*xq)/(dx(0)-dx(1)*xq)
	 d=(-dxp(0)+a*dx(0))/dy(0) }
 ENDCASE
 ;now b and c
 CASE
 DX(0) EQ 0.0: { c=dyp(0)/dy(0)	b=(dyp(1)-c*dy(1))/dx(1) }
 DX(1) EQ 0.0: { c=dyp(1)/dy(1)	b=(dyp(0)-c*dy(0))/dx(0) }
 ELSE { xq=dx(0)/dx(1)	c=(dyp(0)-dyp(1)*xq)/(dy(0)-dy(1)*xq)
	 b=(dyp(0)-c*dy(0))/dx(0) }
 ENDCASE
 ;type out results and some checks
 magx=sqrt(a*a+b*b)
 magy=sqrt(d*d+c*c)
 phi1=atan2(b/magx,a/magx)*#r.d
 phi2=atan2(d/magy,c/magy)*#r.d
 type,'magx, magy =',magx,magy
 type,'phi1, phi2 =',phi1,phi2
 ang=.5*(phi1+phi2)
 ENDSUBR
 ;===========================================================================
SUBR GETGRID3,NX,NY,MAGX,MAGY,ANG,XP,YP,X,Y
 ;given all the inputs, compute a control grid ($cx,$cy)
 ;setup the original corners
 $cx=[[[0,nx]],[[0,nx]]]
 $cy=[[[0,0]],[[ny,ny]]]
 ;get the reference points, note that xp,yp are for master (fixed) image
 xr=first_elem(xp)
 yr=first_elem(yp)
 $cx=$cx-xr	$cy=$cy-yr	;center on this point
 ;rotate first, in direction opposite ang
 theta=-ang*#d.r
 save=$cx		$cx=$cx*cos(theta)-$cy*sin(theta)
 $cy=save*sin(theta)+$cy*cos(theta)
 ;now stretch by recip. of magx and magy
 $cx=$cx/magx	$cy=$cy/magy
 xr=first_elem(x)
 yr=first_elem(y)
 ;now just add in ref. point of input image (the one to be modified)
 $cx=$cx+xr	$cy=$cy+yr
 ty,' $cx =',$cx
 ty,' $cy =',$cy
 ENDSUBR
 ;===========================================================================
subr errors,xp,yp,x,y,mag,ang
 ;uses mag and ang and the first set of points to compute position errors
 ;get the reference points, note that xp,yp are for master (fixed) image
 ;and we compute where these would end up in second image and compare
 ;with actual second image positions
 xr=first_elem(xp)
 yr=first_elem(yp)
 xq=xp-xr		yq=yp-yr
 ;rotate first, in direction opposite ang
 theta=-ang*#d.r
 save=xq
 xq=xq*cos(theta)-yq*sin(theta)
 yq=save*sin(theta)+yq*cos(theta)
 ;now stretch by recip. of magx and magy
 xq=xq/mag	yq=yq/mag
 xr=first_elem(x)
 yr=first_elem(y)
 ;now just add in ref. point of second image
 xq=xq+xr	yq=yq+yr
 ty,'x errors', xq-x
 ty,'y errors', yq-y
 endsubr
 ;===========================================================================
SUBR MARKS,CX,CY,NSYM
 ;mark these points on the grinnell, works for scalars or arrays
 if !narg lt 3 then nsym=-2
 sq=!pdev
 pdev,1
 if symclass(cx) eq 1 then xymov,cx/512.,cy/512.,nsym
 if symclass(cx) eq 4 then { n=num_elem(cx)-1
 if nsym lt 0 then { for i=0,n do xymov,cx(i)/512.,cy(i)/512.,nsym  }
 if nsym gt 0 then { xymov,cx(n)/512.,cy(n)/512.,0
	 for i=0,n do xymov,cx(i)/512.,cy(i)/512.,nsym  }  }
 pdev,sq
 ENDSUBR
 ;===========================================================================
FUNC ALIGN,XIN,nx,ny,XP,YP,X,Y
 ;output is the image XIN distorted to match a reference image using the
 ;3 tie points in (xp,yp) and (x,y) where xp,yp are for ref. image
 ;these tie points will be asked for if they are not present in the list
 if !narg lt 4 then { ntie=0  while (ntie eq 2 or ntie eq 3) eq 0 do {
 read,'enter the number of tie points (2 or 3 only!)',ntie }
 xp=zero(fltarr(ntie))
 read,'enter the x values for the tie points in the master image',xp  }
 ;establish ntie, however we got here, it is defined as size of xp
 ;but less than or equal to 3
 ntie=dimen(xp,0)<3
 if !narg lt 5 then { yp=zero(fltarr(ntie))
 read,'enter the y values for the tie points in the master image',yp  }
 if !narg lt 6 then { x=zero(fltarr(ntie))
 read,'enter the x values for the tie points in the input image',x  }
 if !narg lt 7 then { y=zero(fltarr(ntie))
 read,'enter the y values for the tie points in the input image',y  }
 if ntie eq 3 then MATCH3,MAGX,MAGY,ANG,XP,YP,X,Y
 if ntie eq 2 then { MATCH2,MAGX,ANG,XP,YP,X,Y  MAGY=MAGX }
 GETGRID3,NX,NY,MAGX,MAGY,ANG,XP,YP,X,Y
 XQ=REGRID(XIN,$CX,$CY,NX,NY)
 RETURN,XQ
 ENDFUNC
 ;===========================================================================
FUNC BACKGRID,X,Y,CG,NX,NY
 ;a utility function to return the original coord. from the value (of x and y)
 ;in the regrided image given the corresponding control grid
 ;works for either x or y coord. depending on whether CG is CX or CY
 ;but each needs x, y, nx and ny
 a=float(x)/nx
 b=float(y)/ny
 xq=(1.-a)*(1.-b)*cg(0,0)+a*(1.-b)*cg(1,0)+b*(1.-a)*cg(0,1)+a*b*cg(1,1)
 return,xq
 ENDFUNC
 ;===========================================================================
FUNC REALIGN,XIN,NX,NY,XP,YP,X,Y
 ;a lot of arguments, x and y are coords in the converted image
 ;note that $CX, and $CY are modified !!
 ;first translate these (x,y) pairs back to the original input
 ntie=dimen(xp,0)<3
 xb=backgrid(x,y,$cx,nx,ny)
 yb=backgrid(x,y,$cy,nx,ny)
 type,'xb,yb (in realign) =',xb,yb
 ;that's all we needed the old cx and cy and x,y's for
 if ntie eq 3 then MATCH3,MAGX,MAGY,ANG,XP,YP,XB,YB
 if ntie eq 2 then { MATCH2,MAGX,ANG,XP,YP,XB,YB  MAGY=MAGX }
 ty,'new mag and angle =',magx, ang
 GETGRID3,NX,NY,MAGX,MAGY,ANG,XP,YP,XB,YB
 XQ=REGRID(XIN,$CX,$CY,NX,NY)
 ;we also need new x, y's that are consistent with the new grid
 ;remember that the grid is overdetermined by xg and yg and we won't get
 ;exactly the same result back
 ;CPROJECT,xb,yb,nx,ny,x,y
 RETURN,XQ
 ENDFUNC
 ;===========================================================================
subr collect_pairs, xref, yref, xp, yp
 ;prompt for points in 2 images
 ic = 0
 mess = 1
 repeat {
 ty,'mark a point on the reference image'
 hairs
 mark
 if ic eq 0 then { xref = !ix  yref = !iy } else {
 	xref=[xref,!ix]  yref=[yref,!iy] }
 ty,'mark a point on the other image'
 hairs
 mark
 if ic eq 0 then { xp = !ix  yp = !iy } else {
 	xp=[xp,!ix]  yp=[yp,!iy] }
 ic += 1
 read,'enter 1 for another, 0 to quit', mess
 } until mess ne 1
 endsubr
 ;===========================================================================
