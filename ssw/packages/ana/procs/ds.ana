func ds(m1,m2,n,nwin,ngwin)
;11/29/88 r. shine
;uses the Tarbell-Ferguson destretch method, returns a destretched version of
;m2 which has been stretched to match m1 (if it works right!)
;n is the number of correlation cells (in both directions), nw is the width of
;each cell (also in both directions), and ngw is the width of the gaussian mask
;modifications for rectangular grids and cells would be straightforward
;note that m1 and m2 must be the same size and that all calculations are done
;after converting the image arrays to I*2 (if necessary)
;this routine will primarily useful for testing since production destretching
;should have trends removed in the distortion grids before destretching to
;preserve flows and shears
nx=dimen(m1,0)
ny=dimen(m1,1)
if !narg lt 5 then ngw=fix(2.*(nx>ny)/n)  else ngw=ngwin
if !narg lt 4 then nw=fix(1.25*ngw)       else nw=nwin
ty,'ngw, nw =',ngw,nw
nxg=n
;nyg=n
nyg=rfix(float(n)*ny/nx)
ty,'nxg, nyg =',nxg,nyg

;note the wx calculation should be FP, 3/18/89
wx=float(nx)/nxg
wy=float(ny)/nyg
;this is supposed to compute the grid the same way as Stu's programs
gx=indgen(fltarr(nxg,nyg),0)*wx+wx/2.-1
gy=indgen(fltarr(nxg,nyg),1)*wy+wy/2.-1
dx=nw
dy=nw
gwid=ngw
del=gridmatch(m1,m2,gx,gy,dx,dy,gwid)
return,stretch(m2,del)
endfunc
;============================================================================
func dsc(m1,m2,n,nwin,ngwin)
;12/18/88 a version that uses compressed images to compute distortion
;11/29/88 r. shine
;uses the Tarbell-Ferguson destretch method, returns a destretched version of
;m2 which has been stretched to match m1 (if it works right!)
;n is the number of correlation cells (in both directions), nw is the width of
;each cell (also in both directions), and ngw is the width of the gaussian mask
;modifications for rectangular grids and cells would be straightforward
;note that m1 and m2 must be the same size and that all calculations are done
;after converting the image arrays to I*2 (if necessary)
;this routine will primarily useful for testing since production destretching
;should have trends removed in the distortion grids before destretching to
;preserve flows and shears
m1c=compress(m1,2)	m2c=compress(m2,2)
nx=dimen(m1c,0)
ny=dimen(m1c,1)
if !narg lt 5 then ngw=fix(2.*(nx>ny)/n)  else ngw=ngwin
if !narg lt 4 then nw=fix(1.25*ngw)       else nw=nwin
ty,'ngw, nw =',ngw,nw
nxg=n
nyg=n
;note the wx calculation shoud be FP, 3/18/89
wx=float(nx)/nxg
wy=float(ny)/nyg
;this is supposed to compute the grid the same way as Stu's programs
gx=indgen(fltarr(nxg,nyg),0)*wx+wx/2.-1
gy=indgen(fltarr(nxg,nyg),1)*wy+wy/2.-1
dx=nw
dy=nw
gwid=ngw
del=2.*gridmatch(m1c,m2c,gx,gy,dx,dy,gwid)
return,stretch(m2,del)
endfunc
;============================================================================
func dscl(m1,m2,n,nwin,ngwin)
;12/19/88 this version also uses bi-linear interpolation
;12/18/88 a version that uses compressed images to compute distortion
;11/29/88 r. shine
;uses the Tarbell-Ferguson destretch method, returns a destretched version of
;m2 which has been stretched to match m1 (if it works right!)
;n is the number of correlation cells (in both directions), nw is the width of
;each cell (also in both directions), and ngw is the width of the gaussian mask
;modifications for rectangular grids and cells would be straightforward
;note that m1 and m2 must be the same size and that all calculations are done
;after converting the image arrays to I*2 (if necessary)
;this routine will primarily useful for testing since production destretching
;should have trends removed in the distortion grids before destretching to
;preserve flows and shears
m1c=compress(m1,2)	m2c=compress(m2,2)
nx=dimen(m1c,0)
ny=dimen(m1c,1)
if !narg lt 5 then ngw=fix(2.*(nx>ny)/n)  else ngw=ngwin
if !narg lt 4 then nw=fix(1.25*ngw)       else nw=nwin
ty,'ngw, nw =',ngw,nw
nxg=n
nyg=n
;note the wx calculation shoud be FP, 3/18/89
wx=float(nx)/nxg
wy=float(ny)/nyg
;this is supposed to compute the grid the same way as Stu's programs
gx=indgen(fltarr(nxg,nyg),0)*wx+wx/2.-1
gy=indgen(fltarr(nxg,nyg),1)*wy+wy/2.-1
dx=nw
dy=nw
gwid=ngw
del=2.*gridmatch(m1c,m2c,gx,gy,dx,dy,gwid)
return,stretch_lin(m2,del)
endfunc
