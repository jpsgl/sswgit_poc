typedef struct	expdb { short args[2]; short type[2]; short optype;} expdb;
/* the types for expdb's are:
	0	register
	1	literal
	2	symbol (with assigned value)
 others can be assigned but the register case must be zero and any others
 must have a numerical value in args */
enum sub0args_ids { SRT, TERM, RESETAEC, LOCKBUF, STARTAVE, UPDIMAX, TAP,
	OPENLOOP, CLOSELOOP };
enum sub1args_ids { WAIT, DELAY, TIM, RUNDHCP, CADENCE };
enum sub2args_ids { DEV, SAMP, SETDHCR, TIMCHK, CALL2, LDA, LDR };
enum sub3args_ids { FRAMEPF };
enum symbol_types { UNDEF, VAR, FRAME_SYM, SEQ_SYM, TARGET_SYM };
typedef struct Symbol { /* similar but not the same as used in hoc */
  /* 3/8/97 added status key to support sequence, frame, and target loads */
  char	*name;
  short	type;	/* UNDEF, VAR, FRAME_SYM, SEQ_SYM, TARGET_SYM */
  int	val;
  int	status;	/* 0 for NA or not compiled, 1 for compiled,
  		2 for onboard already */
  struct Symbol	  *next;	/* the link to next one */
  } Symbol;

Symbol *install(), * lookup();

typedef struct ListArray { /* similar to Symbol above but for named lists */
  char	*name;
  short	type;	/* UNDEF, VAR */
  short size;
  int	*values;
  char	**names;
  struct ListArray	  *next;	/* the link to next one */
  } ListArray;

ListArray *list_install(), * list_lookup();
