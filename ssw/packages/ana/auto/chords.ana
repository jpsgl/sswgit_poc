;test area calculations
 ;=================================================================
subr stony, r, bin, ixcin, iycin
 ;r in arc seconds
 if !narg ge 3 then ixc = ixcin else ixc = 255
 if !narg ge 4 then iyc = iycin else iyc = 255
 pen, 2
 nr=200		nrd2=nr/2
 th=fltarr(nr+1)
 th=indgen(th)*#2pi/nr
 x=ixc+r*sin(th)
 y=iyc+r *cos(th)
 xymov,x(0),y(0),0
 xymov,x,y,1
 pen, 1
 nlat=17		nlong=18	dlat=10.	dlong=10.
 b=bin*#d.r
 dth=6.2831853/nr
 sb=tan(b)
 ;the latitude loop, we do nlat of them
 xlat=-90
 for i=1,nlat do {
   ;if i%10 eq 0 then pen,5 else if i%5 eq 0 pen,3 else pen,1
   xlat += dlat
   x1=xlat*#d.r
   ;figure out visible range on longs. for this lat
   xq=sb*sin(x1)/cos(x1)
   ;partially (at least) visible if xq > -1
   if (xq gt -1.0) then {
     xq=xq < 1.0
     ;get arcsin of xq, this is extra part
     xq=asin(xq)
     xs=1.5707963+xq         ;start
     yq=3.1415927+2.*xq      ;range
     thq=yq/nr               ;incre.
     x2=xs-indgen(x)*thq
     x=r*cos(x1)*sin(x2)+ixc
     y=0.5*r*(sin(x1-b)*(1.+cos(x2))+sin(x1+b)*(1.-cos(x2)))+iyc
     ;first point
     xymov,x(0),y(0),0
     ;for each lat., circle around in long
     xymov,x,y,1
  }
 }
 ;the longitude loop, we do nlong of them
 xlong=-110
 for i=1,nlong do {
   ;if i%10 eq 0 then pen,5 else if i%5 eq 0 pen,3 else pen,1
   xlong=xlong+dlong	x2=xlong*#d.r
   ;get the first lat.
   if (sb gt 0) {  xq=-cos(x2)/sb	xs=atan(xq) } else if (sb le 0) {
   xq=-cos(x2)/sb	xs=atan(xq)-3.1415927 } else { xs=0.0 }
   x1=xs+indgen(fltarr(nrd2))*dth
   x=r*cos(x1)*sin(x2)+ixc
   y=0.5*r*(sin(x1-b)*(1.+cos(x2))+sin(x1+b)*(1.-cos(x2)))+iyc
   xymov,x(0),y(0),0
   ;for each long., circle around in lat
   xymov,x,y,1
 }
 endsubr
 ;=================================================================
func chordarea(ys0, ys1, r, rsq)
 carea = 0.5 *(ys1*sqrt(rsq-ys1*ys1)+rsq*(asin(ys1/r)-asin(ys0/r))-ys0*sqrt(rsq-ys0*ys0))
 return, carea
 endfunc
 ;=================================================================
;;subr gfactor, sunx, suny
block gfactor
 ;assume full NFI
 iy1 = 4095
 iy0 = 0
 ix0 = 0
 ix1 = 2047
 pscale = .080
   xs0 = (2047.5 - iy1) * pscale + sunx;
   xs1 = (2047.5 - iy0) * pscale + sunx;
   ys0 = (1023.5 - ix1) * pscale + suny;
   ys1 = (1023.5 - ix0) * pscale + suny;
 ;plot on a disk
 t0 = date2tai(2008, doy(2008,4,19),10,6,0)
 b = sun_b_tai(t0)
 r = sun_r_tai(t0)
 xc = 600
 yc = 550
 xport,0, 1200,1100
 stony, r/2, b, xc, yc
 ;draw the fov
 xx0 = 0.5 * xs0 + xc
 xx1 = 0.5 * xs1 + xc
 yy0 = 0.5 * ys0 + yc
 yy1 = 0.5 * ys1 + yc
 pencolor, 'red
 
 xymov, xx0, yy0, 0
 xymov, xx0, yy1, 1
 xymov, xx1, yy1, 1
 xymov, xx1, yy0, 1
 xymov, xx0, yy0, 1
 pencolor, 'black
 
 ;compute r for the corners
 r1 = xs0*xs0 + ys0*ys0
 r2 = xs0*xs0 + ys1*ys1
 r3 = xs1*xs1 + ys0*ys0
 r4 = xs1*xs1 + ys1*ys1
 rsq = r*r
 nlimbs = 0
 if r1 gt rsq then nlimbs = nlimbs or 0x1
 if r2 gt rsq then nlimbs = nlimbs or 0x2
 if r3 gt rsq then nlimbs = nlimbs or 0x4
 if r4 gt rsq then nlimbs = nlimbs or 0x8
 dx = abs (xs1 - xs0)
 dy = abs (ys1 - ys0)
 carea = 0
 barea = 0
 diskfract = 1.0
 ty,'nlimbs = ', nlimbs
 if nlimbs then {
   ;something over a limb I(maybe everything)
   if nlimbs eq 15 then diskfract = 0.0 else {
      if nlimbs eq 3 then {
        ;on E (left) limb with 2 points over limb
	barea = -dy * xs1	;could be >0 or <0
	yc0 = ys0
	yc1 = ys1
	if xs0 gt -r then {
	  ;we have to subtract the part sticking beyond the rectangle
	  barea = barea + chordarea(-r, xs0, r, rsq)
	  ty,'part removed = ', chordarea(-r, xs0, r, rsq)
	}
      } else if nlimbs eq 12 then {
        ;on W (right) limb with 2 points over limb
	barea = dy * xs0	;could be >0 or <0
	yc0 = ys0
	yc1 = ys1
	if xs1 lt r then {
	  ;we have to subtract the part sticking beyond the rectangle
	  barea = barea + chordarea(xs1, r, r, rsq)
	  ty,'part removed = ', chordarea(xs1, r, r, rsq)
	}
      } else if nlimbs eq 8 then {
	yq = sqrt(rsq - xs1*xs1)
	yc0 = yq
	yc1 = ys1
	;here we actually have 2 rectangles
	barea = (yc1 - yc0) * xs0 - (yq - ys0) * dx
      } else if nlimbs eq 4 then {
	yq = -sqrt(rsq - xs1*xs1)
	yc0 = ys0
	yc1 = yq
	barea = (yc1 - yc0) * xs0 - (ys1 - yq) * dx
      } else if nlimbs eq 2 then {
	yq = sqrt(rsq - xs0*xs0)
	yc0 = yq
	yc1 = ys1
	;here we actually have 2 rectangles
	barea = -(yc1 - yc0) * xs1 - (yq - ys0) * dx
      } else if nlimbs eq 1 then {
	yq = -sqrt(rsq - xs0*xs0)
	yc0 = ys0
	yc1 = yq
	barea = -(yc1 - yc0) * xs1 - (ys1 - yq) * dx
      } else if nlimbs eq 11 then {
	yq = sqrt(rsq - xs1*xs1)
	yc0 = ys0
	yc1 = yq
	barea = -(yc1 - yc0) * xs1
      } else if nlimbs eq 7 then {
	yq = -sqrt(rsq - xs1*xs1)
	yc0 = yq
	yc1 = ys1
	barea = -(yc1 - yc0) * xs1
      } else if nlimbs eq 14 then {
	yq = sqrt(rsq - xs0*xs0)
	yc0 = ys0
	yc1 = yq
	barea = (yc1 - yc0) * xs0
      } else if nlimbs eq 13 then {
	yq = -sqrt(rsq - xs0*xs0)
	yc0 = yq
	yc1 = ys1
	barea = (yc1 - yc0) * xs0
      } else if nlimbs eq 10 then {
	;top pole regions
	yc0 = xs0
	yc1 = xs1
	barea = dx * ys0
	if ys1 lt r then {
	  ;we have to subtract the part sticking beyond the rectangle
	  barea = barea + chordarea(ys1, r, r, rsq)
	  ty,'part removed = ', chordarea(ys1, r, r, rsq)
	}
      } else if nlimbs eq 5 then {
	;bottom pole regions
	yc0 = xs0
	yc1 = xs1
	barea = -dx * ys1
	if ys0 gt -r then  {
	  ;we have to subtract the part sticking beyond the rectangle
	  barea = barea + chordarea(-r, ys0, r, rsq)
	  ty,'part removed = ', chordarea(-r, ys0, r, rsq)
	}
      }
     ;;ty,'yc0, yc1, r, rsq =', yc0, yc1, r, rsq
      carea = chordarea(yc0, yc1, r, rsq)
      ;now subtract the rectangle (though it could be negative and hence
      ;add to the area)
      tarea = carea - barea
   }
 }
 rarea = dx * dy
 ty,'carea, barea = ', carea, barea
 diskfract = tarea/rarea
 ty,'computed area in fov =', tarea, ', fraction = ', diskfract
 ;a check via another brute force method
 xinbox = fltarr(dx, dy)
 xinbox = float(indgen(xinbox, 0) + xs0)
 yinbox = float(indgen(xinbox, 1) + ys0)
 rinbox = sqrt(xinbox*xinbox + yinbox*yinbox)
 image = zero(bytarr(dx,dy))
 inq = sieve(rinbox le r)
 fq = float(num_elem(inq))/num_elem(rinbox)
 ty, 'area on disk = ', num_elem(inq),' fraction = ', fq, ', ratio =', diskfract/fq 
 image(inq) = 1
 !iorder=0
 tv, scale(image), 0,0,2
 ;endsubr
 endblock
 ;=================================================================
block testchordarea
 ;test chordarea
 r = 960.0
 dy = 96*2.0
 xport,1, 1200,1100
 xc = 600
 yc = 550
 b = 5.0
 stony, r/2, b, xc, yc
 ys1 = r
 rsq = r*r
 tq = 0
 for k = 0, 9 do {
   ys0 = ys1 - dy
   xymov, 0, ys1/2+yc, 0   xymov, 1199, ys1/2+yc, 1
   xymov, 0, ys0/2+yc, 0   xymov, 1199, ys0/2+yc, 1
   carea = chordarea(ys0, ys1, r, rsq)
   ty,'k, ys0, ys1, carea =', ys0, ys1, k, carea
   tq = tq + 2.0 * carea
   ys1 = ys1 - dy
 }
 ty,'total of chords =', tq
 endblock
 ;=================================================================
;;gfactor, -960,0
;run testchordarea
sunx = -960
suny = 0
run gfactor
sunx = -880
suny = 0
run gfactor
sunx = 960
suny = 0
run gfactor

sunx = 675
suny = 675
run gfactor

;8
sunx = 675
suny = 500
run gfactor
;4
sunx = 675
suny = -500
run gfactor
;1
sunx = -675
suny = -500
run gfactor
;2
sunx = -675
suny = 500
run gfactor
;11
sunx = -940
suny = 520
run gfactor
;7
sunx = -940
suny = -520
run gfactor
;14
sunx = 940
suny = 520
run gfactor
;13
sunx = 940
suny = -520
run gfactor
;12
sunx = 790
suny = 0
run gfactor
;3
sunx = -790
suny = 0
run gfactor

;10
sunx = 10.
suny = 870.
run gfactor
;5
sunx = 10.
suny = -870.
run gfactor
