;file processmovies.ana, ras
 ;2/3/93 second generation movie processer adapted from alignmovies
 ;user interface to generate parameters
 ;
 ;9/23/90	rotation upgraded and reference angle allowed, if r_ref
 ;		is defined in command file, it is used rather than computing
 ;		a mid range, also lt 0 forces compute -- in a series the
 ;		first movie will define r_ref and the other will use the same
 ;5/23/90	some changes to handle 1024 images, these involve assuming
 ;that images are 512 until the first is read in, then we change the
 ;subarea (for CC) and the rotation center (if a rotation is done)
 ;
 ;an intermediate step to a better system, this file contains routines to
 ;rigidly align a series of images (stored as disk files in FZ format) and
 ;to destretch them
 ;the routines are run via command files which define the file names,
 ;length of sequence, and operation performed
 ;
 ;both the alignment and the destretch use a double pass method, the first
 ;pass determines the image by image offsets and the second does some trend
 ;processing on these before applying them and producing output images
 ;file name conventions are assumed to comply with the following rules:
 ;	last 5 characters of file name are n#### were the #'s represent the
 ;	number of the image in the sequence; i.e., the first file name
 ;	might be LB113w5576N0001. The first part of the file name is
 ;	called name_set within the software. The files must progress from
 ;	0001 up to the number of files in steps of 1.
 ;	The file extensions are .COR for the initial images. The software
 ;	then creates .RIG files and .STRT files for the rigidly aligned and
 ;	destretched results.
@/umbra/people/shine/auto/series_align_new
@/umbra/people/shine/auto/rigplot
@/umbra/people/zoe/look92
 ;==============================================================================
block getstarted
 close,1,2
 #diskout1='/data2/soup/'
 #diskout2='/data0/soup/'
 #diskout3='/data1/soup/'
 #diskout4='/pore2/people/shine/lp92/'
 #diskout5='/data5/soup/'
 #diskout6='/pore2data/soup/'
 #diskout7='/data/soup/'
 #ndisksout=7		;number to use
 ;get id for this data set
 tapeid=''
 read,'enter catalog tape id (e.g., le132f18)',tapeid
 catload,tapeid
 type,'nimage =',nimage,', nexp =',nexp,'
 if nexp gt 0 then { nrepeats=nimage/nexp
	  type,'nrepeats (complete ones) =',nrepeats }
 ;check for duplicates that should be combined
 waveindx=lonarr(nexp)
 nc=1
 ;loop through and assign a number for each image, images with the same state
 ;get the same number
 for k=0,nexp-1 do {
 ;gen the name, assume unique and then check
 ss='namew'+istring(nc,3,2)
 wname,seq_wave(k),seq_off(k),seq_pol(*,k),eval(ss),tapeid
 ;now check against all previous names
 waveindx(k)=nc	nc=nc+1
 if k gt 0 then { for i=0,k-1 do { s2='namew'+istring(waveindx(i),3,2)
   if eval(s2) eq eval(ss) then { nc=nc-1  waveindx(k)=waveindx(i)  break }}}
 }
 nwave=nc-1
 ty,'number of unique wavelengths is',nwave
 ;get subcycles for each wavelength
 subcycle=zero(intarr(nwave))
 for k=0,nexp-1 do {
 subcycle(waveindx(k)-1)+=1
 }
 endblock
 ;==============================================================================
block autotwist 
 ty,'please select an option'
 ty,'0: compute alignments and produce output files'
 ty,'1: only compute alignments, defer output'
 ty,'2: only produce output files (assumes alignments on disk)'
 read,twist_option
 if twist_option eq 0 then ty,'if things get dicey, output might be deferred'
 run getstarted		;load the catalog info and such
 
 wavesave=zero(intarr(nwave))+1
 mess=''
 read,'are we processing all the wavelengths ? [Y/N]',mess
 if upcase(mess) ne 'Y' then {
 type,'for each wavelength set, type Y to untwist, N to skip, Q to skip all remaining'
 zero,wavesave		;a flag array
 for iw=1,nwave do {
 type,eval('namew'+ist(iw,3,2))
 read,mess
 if upcase(mess) eq 'Y' then wavesave(iw-1)=1
 if upcase(mess) eq 'Q' then break
 }
 }
 kfirstrep=1
 kreps=nrepeats
 while 1 eq 1 do {
 mess=''
 read,'Are we processing all the repeats ? [Y/N]',mess
 if upcase(mess) eq 'Y' then break
 read,'enter first and last rep (of the complete cycles) to use',kfirstrep,kreps
 type,'you have first and last rep =', kfirstrep,kreps
 read,'is this OK [Y/N]?',mess
 if upcase(mess) eq 'Y' then break
 type,'let''s try again then'
 }
 repeats_used=kreps-kfirstrep+1
	 ;set up some parameters
 #rotate=1		;we are rotating by default (chance to change later)
 r_ref=-1		;use mean rotation by default
	 ;determine drift parameters, first get duration of run
 dt=3600.*fix(#t(39:40))+60.*fix(#t(42:43))+fix(#t(45:46)
 dt=dt-3600.*fix(#t(27:28))-60.*fix(#t(30:31))-fix(#t(33:34)
 if dt lt 0 then dt=dt+24.*3600.
 dt_rep=dt/nrepeats
	 ;if only a partial run, adjust dt
 dt=dt_rep*(repeats_used)
   ;our scheme - if the duration < 30m, don't use superdrift, just rigid align
   ;if > 30 but < 90, smooth with about a 10 minute window, for longer
   ;runs use 15 m, these may need adjusting as experience improves
   ;also use rigid (no drift) if repeats_used is 10 or less
 if dt le 1800 or repeats_used le 10 then { driftbase=0 } else {
 if dt le 5400 then { driftbase=600./dt_rep } else { driftbase=900./dt_rep }
 }
 ty,'computed driftbase =', driftbase
	 ;choose an output disk
 disk=zero(lonarr(nwave))	;provide for different disks later
 
 if twist_option ne 1 then {
 iq=-1
 while (iq le 0 or iq gt #ndisksout) do {
 ty,'enter a number to choose output disk'
 for i=1,#ndisksout do { ty,i,'  ',eval('#diskout'+ist(i,2,2)) }
 read,iq
 }
 disk=disk+iq-1
 
 read,'enter 1 to turn on delete flag', #dflag
 if #dflag ne 1 then #dflag=0
 }
 
 name_in_ext='.cor'
 name_out_ext='.rig'
 mess=''
 ty,'anything special ?'
 ty,'(turn off rotation, override drift factor, non-standard name extensions,'
 ty,'special subarea)'
 read,'[Y/N]',mess
 if upcase(mess) eq 'Y' then {
 read,'enter 1 for La Palma rotation or 0 for none', #rotate
 #rotate=fix(#rotate)
 mess=''
 if #rotate ne 0 then {
 read,'enter rotation reference angle or -1 to use mean orientation',r_ref
 }
 read,'want to change driftbase ? [Y/N]',mess
	 if upcase(mess) eq 'Y' then {
	 ty,'enter new driftbase (0 for completely rigid)', driftbase }
 read,'want to change input name extension ? [Y/N]',mess
	 if upcase(mess) eq 'Y' then {
	 read,'enter it with the "." (e.g., ".cor")', name_in_ext }
 read,'need to specify the CC subarea ? [Y/N]',mess
	 if upcase(mess) eq 'Y' then {
	 ty,'please be very careful !'
	 read,'enter first and last x index',#i1,#i2
	 read,'enter first and last y index',#j1,#j2
	 #usersubarea=1
	 }
 if twist_option ne 1 then {
 read,'want to change output name extension ? [Y/N]',mess
	 if upcase(mess) eq 'Y' then {
	 read,'enter it with the "." (e.g., ".rig")', name_out_ext }
 }
 
 }
 
 ;also print info block on plot file
 pdev,1
 run infoblock
 
 count=0
 ;main untwist loop
 
 for iw=0,nwave-1 do {
 if wavesave(iw) ne 0 {
 
   if driftbase gt 0 then {
   #drift=fix(driftbase*subcycle(iw))
   #drift=#drift-(#drift%2 eq 0)			;make odd
   #drift=#drift>3
   } else #drift=0
 
   count = count + 1
   ;get the file counts
   kstart=1+ (kfirstrep-1)*subcycle(iw)
   kend=kreps*subcycle(iw)
   name_set=lowcase(eval('namew'+ist(iw+1,3,2)))
   name_in=name_set+'n####'+name_in_ext
   ty,'iw =', iw,' kstart and kend =', kstart,kend
   ty,'name_in =', name_in
 
 
   getrigid,kstart,kend,name_in,r_ref,r,d,seeing,sample_a,sample_b,twist_option
 
	 ;getrigid computes the angles and the pair offsets
	 ;saved in r and d
	 ;note that twist_option can be changed to 1 by getrigid
 
   fzread,seeing,name_set+'.seeing',s
   fzread,r,name_set+'.angles'
   r_ref=r(0)		r=r(1:*)
   fzread,d,name_set+'.offsets'
   ;process the raw offsets
   dhipass,d,dxs,dys
   ;plot some results
   if count eq 1 then  { rotplot,r_ref,r,name_set,kstart }
   ;also postage stamps of first and last images
   stamps,sample_a,sample_b,count,1,name_in_ext,kstart,kend
   ;before plotting, process the raw offsets
   dhipass,d,dxs,dys
   rigplot,count,name_set,dxs,dys,seeing,kstart
 
   sdisk=eval('#diskout'+ist(disk(iw)+1,2,2))
 ;this info printed even if we aren't writing files
   name_out=sdisk+name_set+'n####'+name_out_ext
   ty,'name_out =', name_out
 
 ;note that untwist produces the samples for twist_option = 1
 ;these are not printed for twist_option = 2
 
 untwist,kstart,kend,name_in,name_out,r_ref,r,dxs,dys,sample_a,sample_b-
	 ,twist_option
 
   stamps,sample_a,sample_b,count,0,name_out_ext,kstart,kend
   ;need to eject?
   if count%3 eq 2 then erase
 }}	;end of iw loop
 endblock
 ;=============================================================================
subr wname,wave,offset,pol,name,tapeid
 ;does the wavelength, etc portion of name
 ;wave and offset are binary and pol is a string or byte array of length 3
 s='W'+istring(wave,4,2)
 case
 offset lt 0:	s=s+'M'+istring(abs(offset),4,2)
 offset eq 0:	s=s+'LC'
 offset gt 0:	s=s+'P'+istring(abs(offset),4,2)
 endcase
 sp=bmap(pol)	sp=sieve(sp, sp ne 32)	;remove blanks
 sp=smap(sp)
 if sp ne '0' then s=s+sp
 name=tapeid+s
 name=lowcase(name)
 endsubr
 ;==============================================================================
subr getrigid(kstart,kend,name_in,r_ref,r,deltac,seeing,s1,s2,twist_option)
 ;2/3/93 subroutine version adapted from block version in alignmovies
 ;this version does not produce any output since a rigid runaway is possible
 ;it does a NN rotate to save time and sub-pixel rigid align, saves alignment
 ;results for use in untwist
 ;for VTT data and already derotated movies, disable rotation
 
 ;loop over the images now
 
 ;-----------------------------------------------------------------------------
 k=kstart
 fseek,x,fns(name_in,k),head		;read in the image
 ;get sample images for kstart and kend
 
 s1=ft(compress(x,4))
 k=kend
 fseek,x,fns(name_in,k),head		;read in the image
 s2=ft(compress(x,4))
 
 endsubr
 ;==============================================================================
block align_reset
 switch,$a2,$a	switch,$b2,$b	switch,$c2,$c	switch,$d2,$d
 $align_count-=1
 endblock
 ;==============================================================================
subr dhipass(d,dxs,dys)
 ;conditions the offsets for jitter removal
 ;this used to be included in untwist
 ;uses results from getalign
 #max_shift=3
 dx=d(0,*)
 dy=d(1,*)
 dxs=runsum(dx)	dys=runsum(dy)
 ;drift options, #drift=0 for rigid, 1 for linear drift, gt 1 for keeping
 ;a smoothed version of the displacement with boxcar width = #drift
 if #drift eq 1 then { dxs=detrend(dxs,1)	dys=detrend(dys,1)  }
 ;2/9/93 gsmooth (gaussian smooth) used in place of boxcar
 ;use #drift as the fwhm of the gaussian
 if #drift gt 1 then {
	 dxs=ghipass(dxs, #drift, #max_shift)
	 dys=ghipass(dys, #drift, #max_shift)
 }
 ;11/2/90 - may be safer to use mean as reference, avoids single glitch problem
 dxs=dxs-mean(dxs)		dys=dys-mean(dys)
 endsubr
 ;==============================================================================
subr untwist(kstart,kend,name_in,name_out,r_ref,r,dxs,dys,s1,s2,twist_option)
 ;2/3/93 subroutine version adapted from block version in alignmovies
 ;remove rotation and previously computed rigid alignment
 ;get the rigid alignments to apply to COR files
 
 if twist_option ne 1 then {		;actually do it for options 0 and 2
 
 ;-----------------loop over the images now ------------------------
 for k=kstart,kend do {
 kmo=k-kstart
 fseek,x,fns(name_in,k),head
 ndisk=#last_disk	;save for possible delete
 ;if a w6302m60rcp, then patch
 ;if strpos(name_in,'w6302m60rcp') gt 1 then {  }
 ;for the first value of k, check the array size
 if k eq kstart then { nx=dimen(x,0) ny=dimen(x,1) xrc=nx/2 yrc=ny/2 }
 m2=rotate3(x,r(kmo),xrc,yrc,dxs(kmo),dys(kmo))
 h2=head+smap(byte(13))+'r_ref ='+string(r_ref)
 if num_elem(h2) gt 256 then h2=h2(0:255)
 fcwrite,m2,fns(name_out,k),h2
 ty,'wrote: ',fns(name_out,k)
 
 ;get sample images for kstart and kend if twist_option = 0 (case = 1 below)
 if twist_option eq 0 then {
 if k eq kstart then s1=ft(compress(m2,4))
 if k eq kend then s2=ft(compress(m2,4))
 }
 
 ;the following deletes input file if #dflag = 1
 if #dflag eq 1 then {
 sdel='rm '+eval('#disk'+istring(ndisk,2,2))+fns(name_in,k)
 spawn,sdel	}	;end of delete option
 ;memstat		;ck memory status
 
 }	;of the image loop
 } else {				;for option 1, just make 2 samples
 
 k=kstart
 fseek,x,fns(name_in,k),head
 kmo=k-kstart
 nx=dimen(x,0) ny=dimen(x,1) xrc=nx/2 yrc=ny/2
 m2=rotate3(x,r(kmo),xrc,yrc,dxs(kmo),dys(kmo))
 ;get sample images for kstart and kend
 s1=ft(compress(m2,4))
 k=kend
 fseek,x,fns(name_in,k),head
 kmo=k-kstart
 m2=rotate3(x,r(kmo),xrc,yrc,dxs(kmo),dys(kmo))
 s2=ft(compress(m2,4))
 
 }
 endsubr
 ;=============================================================================
