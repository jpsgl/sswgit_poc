;========================================================================
block track_info
;
;assume catload has been run and parameters are available
;computing tracks is trivial if there are not duplicate wavelengths that
;we want to combine into double or triple (or...) length video sequences,
;all the complication is to accomplish just that. Is it worth it ?
;this generates 3 vectors that together with nvspace and ivstart
;define the track numbers, combining duplicates into longer video sequences
;the formula used is:
;
;  track=ivstart + (ir-1)*tdup(is-1) + toffset(is-1) + nvspace*tplace(is-1)
;
;where ir is the repeat count (starting with 1, hence the -1) and is is the
;sequence count (also starting with 1), nvspace is usually = nrep unless
;several sets are being combined, in which case it is the sum of the reps in
;all the sets
toffset=zero(intarr(nexp))
tdup=toffset+1
tplace=indgen(tdup)
;these are the defaults if no dups, now check for the dups and adjust
iq=indgen(flatindx)
for k=1,nflat do {
if (total(flatindx eq k) gt 1.1) then {	;ck if we have a dup
jq=sieve(iq,flatindx eq k)		;get locations
nq=num_elem(jq)				;get dup count
tdup(jq)=nq
toffset(jq)=indgen(jq)		}}
;now adjust the tplace's
tplace(0)=0	nq=tdup(0)	;nq is for next non-duplicated set
for k=1,nexp-1 do {
if toffset(k) ne 0 then {
;this is a duplicate and not the first in a series, it assumes the same
;tplace as the first in the series (but with a different offset)
kq=k-1
jq=sieve(iq(0:kq),(flatindx(0:kq) eq flatindx(k)) and (toffset(0:kq) eq 0))
tplace(k)=tplace(jq(0))	} else {
;the other, more normal, case
tplace(k)=nq		nq=nq+tdup(k) }}
endblock
;========================================================================
subr set_record_track,itr
;
;sometimes the video recorder gets confused, the search is the most likely
;command to bomb so we try several times if it doesn't work the first time
sq='CS;SR'+istring(itr,5,2)+':'
if opcmd(sq) ne 1 then panic,'bad SR'
endsubr
;========================================================================
func opcmd(s)
;tries several times if necessary
for itry=1,20 do {
if op1cmd(s) eq 1 then return,1
type,'a problem with video recorder, command = ',s,'  trying again'
wait,.2
#vid_errors+=1
if itry ge 5 then {
 type,'problem with video disk, hit return to try again or 0 to abort'
 kay=''
 read,kay
 if num_elem(kay) gt 0 then { if kay eq '0' then retall } }
wait,0.5		;some time to ponder it's input stream
}	;end of itry loop
return,0
endfunc
;========================================================================
func enable_record(itr)
;if the argument is passed, then after enabling the record, it
;reads back the current track # and compares with itr
;which is where it should be, if it isn't, tries to move it there again
narg=!narg
;first the enable command at wherever we are
sq='RM1:'
if opcmd(sq) ne 1 then panic,'bad RM'

ty,'enable done'
wait,.5
;need to check track ?

if narg gt 0 then {
for itry=1,3 do {		;we make a few attempts to reposition
;first verify position, don't want to allow forward slips
repeat {
sq='NO'
ty,'doing NO'
if opcmd(sq) ne 1 then ty,'problem with ACK on NO'
;now readback the result
wait,.5
sck=op1read(1)		;the 1 is the timeout in seconds
ty,'return from op1read =',sck,'in bytes:',bmap(sck)
ok=0
if num_elem(sck) ge 7 then { if sck(0:1) eq 'NO' then ok=1 }
if ok eq 0 then { ty,'a problem with video recorder, bad readback'
	#vid_errors+=1
	;we can try to recover or just ask user to do it!
	ty,'please set to track',itr,' and enable 1 track record'
	iq=1
	read,'enter 1 when ready (0 to quit)',iq
	if iq eq 1 then return,1 else retall
	}
} until ok eq 1
;we have a track # (we hope), is it the expected one ?
if fix(sck(2:6)) eq itr then return,1	;return if OK
ty,'not on right track, trying to correct'
;not on the track? try to put it there again and re-enable record
;wait,.4
ty,'doing AC'
sq='AC'		if opcmd(sq) ne 1 then panic,'bad AC'
wait,.3
ty,'doing set_record_track'
set_record_track,itr
ty,op1read(10)
ty,'doing RM again'
sq='RM1:'
if opcmd(sq) ne 1 then panic,'bad RM'
ty,op1read(2)
}
return,0		;must have failed if we get here
}

return,1
endfunc
;========================================================================
block safe_record
;6/30/92 more changes to handle badly functioning recorders
;also cleaned up a lot
;1/27/90 modified by R. Shine for a safer record
;this looks (and is) complicated, but doesn't take long if all goes OK
;
ok=1		;assume it will work
if enable_record(itr) ne 1 then {	;a problem, can't get on the track
	type,'attempt to record on WRONG track'
	type,'track should be',itr
	if mismatch_flag eq 1 then ok=2 else {
		type,'enter 0 to adjust or abort'
		type,'      1 to not record this frame'
		type,'      2 to not record this or future mismatches'
		type,'      3 to record here anyhow (better be sure!)'
		kay=''  read,kay
		if kay eq '2' then { ok=2  mismatch_flag=1 }
		if kay eq '0' then {
 t,'you can try to manually put the recorder on the right track and then enter-
 a 1, or enter a 0 now to abort'
 read,kay
 if kay eq '0' then  retall
 t,'now enter a 3 to really record (or a 1 to not record)'
 read,kay	}
		if kay eq '1' then ok=2
		if kay eq '3' then ok=1
}
}

;now either record or not depending on OK

if ok eq 2 then {	;ok=2 means don't record, but we have
			;to clear record mode
  sq='AC'    if opcmd(sq) ne 1 then panic,'bad AC'
}			;end of ok eq 2 conditional
if ok eq 1 then {	;actually record (finally)
  sq='GS'  if opcmd(sq) ne 1 then {	;didn't record?
   ty,'problem with GS for recording track',itr
   s=op1read(5)		ty,'response from recorder: ',s
  ;intervene manually
  ty,'please check that this is recorded, track #',itr	iq=1
  read,'enter 1 when ready (0 to quit)',iq
  if iq eq 1 then return else retall

  }
}
endblock
;==============================================================================
subr panic,s
;start of a subroutine called when problems arise, allows optional exit or
;some sort of fix
;9/1/90 r. shine

ty,'panic state'
if s eq 'bad RM' or s eq 'bad SR' or s eq 'bad NO' or s eq 'bad AC' then {
ty,'video disk problem, ',s,' command.'
ty,'You can manually set it and then continue'
ty,'enter 1 to continue or 0 to abort'
read,mess

if mess eq 0 then retall
return
}

if s eq 'bad time' then {
t,'the time for an image file is inconsistent with the *.ccd'
t,'file specified, you may be reading the wrong file or you forgot to'
t,'run, setup !'
xq=0
read,'enter 0 to stop or 1 to go on anyhow (better be sure!)',xq
if xq eq 0 then retall
}

if s eq 'dark file' then {
type,'the dark file definition is not consistent with $mtype'
type,'enter 1 to go on anyhow (?), 2 to just re-enter fnd name'
read,'or 0 to exit and fix problem',mess
if mess eq 2 then {
read,'re-enter ID for darks (e.g., dua1:[soup]21jun90m2.dark)',#fnd	}
if mess eq 0 then retall	}
endsubr
;==============================================================================
