Instructions for Converter

This is a convenience interface to convert an image movie set to another
file type. It can also be used for single files with some care.
Files types supported are FITS, GIF, JPEG (8 bit), PGM (used
by the SVST people), and several FZ formats. On input, the 12 JPEG format
is also supported. Variants of the binary PGM format with I*2 and I*4
data are also supported for input only. These are used at the SVST for
raw data. Only b/w JPEG and GIF files are supported. You have to add
your own false color when displaying the data. You can have the input files
deleted as an option (be careful) or write out to the same file name which
also deletes the inputs.

An image movie set is a series of numbered files with the numbers included
as part of the file names. The file template is the common part of these file
names with the numeric portion replaced by # signs. This numeric part of the
name can have a fixed or variable width. In the former case, the number of
digits in each file name must be the same and equal to the number of # signs in
this template. An example is /pore2data2/lp93/im04jun93n####.g.cor. These files
might be numbered from 0001 to 1702. Note the fill with leading zeroes.
Variable width fields have no leading zeroes and are represented with a
single # sign in the name. When the numeric value exceeds 10, it will be
expanded as needed with no leading blanks or zeroes. Only files conforming to
these conventions can be processed.

The input and output templates correspond to the input and output set of
files. These can be the same. The first and last file entries represent the
numeric range to use. Leading zeroes are not required when entering these
values. If a particular numbered file is missing, a message
will appear in the terminal emulator window but the program will continue.
A failure to write the output file (permission denied, disk full) is
supposed to stop the program.

The output file type is selected via an option menu. Clicking on it
displays all the output types. Move the mouse to the desired one
and then release. You selection remains displayed and the others
vanish.

Entries in some of the fields are only relevant to a particular output type.
The fixed bit width is a parameter used only for the FZ compressed files.
Either a number or the string "default" should be entered. The program
comes up with "default" preloaded. The default value is 5 for I*2 data
and 2 for I*1 data. These are usually optimal for CCD raw images but
magnetograms and dopplergrams usually compress better with a value of
7 or 8. Some I*1 images will work better with a value of 1 but SXT images
often compress best with 4 because of a high noise level. To discover the
best value to use, run the ana elook routine on a typical image in the
image set. (A button to do this for you will appear in the mark II version
of this interface).

The JPEG quality field is used only for JPEG output. It can vary from 1
to 100. A value os 75 is usually a good choice. For nearly lossless JPEG
compression, a value of 100 can be used. For more compression, try 50.
Values below 50 should be used with caution, there will be more compression
but the images will start to degrade. The JPEG Q value does not guarantee
any particular ratio of compression. It affects how much the high frequency
components are approximated. An image with a lot of strong fine detail will
not compress as much for a given Q value as a smooth image. The difference
can be dramatic.

Some of the file types only support 8 bit (I*1) data. When the input files
are something else, they are scaled to fit in a 0-255 range. There are
currently 2 options for this scaling. A straight min/max mapping or a
method that trims off the upper and lower 0.1% before computing the min/max.
The latter is called a histogram trim (HT). This usually eliminates noise
spikes that might otherwise consume a large part of the limited dynamic
range in an 8 bit pixel.

After checking that all the fields are properly entered, use the green "Start"
button to begin. The current file number being converted is displayed. The
green start button changes to a red "Stop" button which can be used to stop
the process. Any files already deleted cannot be recovered however.

Good Luck

