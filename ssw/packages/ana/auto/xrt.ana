;based on solarb.ana with simplifications to the file structure
 ;========================================================
subr xrt_widget
 ;the top level widget for Solar B XRT data access
 if defined($xrtwidget) eq 0 then {
 ;the compile_file is supposed to operate at top level and hence the edb's
 ;are temporary and don't add to our accumulation
 compile_file, getenv('ANA_WLIB') + '/xrtwidgettool.ana'
 }
 xtpopup, $xrtwidget
 xmposition, $xrtwidget, !screen_width/2, !screen_height/3
 endsubr
 ;========================================================
subr xrt_subpop_cb
 ;pops any existing sub widgets showing the paths
 if defined($xrtobsfilelistwidget) then xtpopup, $xrtobsfilelistwidget
 if defined($xrtimgfilelistwidget) then xtpopup, $xrtimgfilelistwidget
 endsubr
 ;========================================================
subr xrt_selectortool
 ;create widget if it doesn't exist yet
 if defined($xrtselectwidget) ne 1 then {
    compile_file, getenv('ANA_WLIB') + '/xrtselectorwidgettool.ana'
 }
 xtpopup, $xrtselectwidget
 endsubr
 ;========================================================
subr xrt_selector_link_cb
 win = $link_window	;before it can change
 h = $view_header(win)	;recover the FITS header
 s = date_from_tai($view_tai(win))
 xmtextfieldsetstring, $xrt_select_time_txt(0), s(0:3)
 xmtextfieldsetstring, $xrt_select_time_txt(4), s(0:3)
 xmtextfieldsetstring, $xrt_select_time_txt(1), s(5:6)
 xmtextfieldsetstring, $xrt_select_time_txt(5), s(5:6)
 xmtextfieldsetstring, $xrt_select_time_txt(2), s(8:9)
 xmtextfieldsetstring, $xrt_select_time_txt(6), s(8:9)
 xmtextfieldsetstring, $xrt_select_time_txt(3), s(11:18)
 
 iq = fits_key(h, 'EC_FW1 ')
 ty, iq
 xmsetoptionselection, $xrt_fw1_wq(0), $xrt_fw1_wq(iq+2)
 iq = fits_key(h, 'EC_FW2 ')
 ty, iq
 xmsetoptionselection, $xrt_fw2_wq(0), $xrt_fw2_wq(iq+2)
 
 ;;xq = fits_key(h, 'EXPTIME ')
 ;;ty, xq
 ;;if isscalar(xq) then s = sprintf('%5.2f', xq)
 ;;xmtextfieldsetstring, $xrt_range_lo_wq(0), s
 ;;xmtextfieldsetstring, $xrt_range_hi_wq(0), s
 ;size?
 n = fits_key(h, 'NAXIS1  ')
 sq = string(n)
 ty,'sq =', sq
 xmtextfieldsetstring, $xrt_range_lo_wq(2), sq
 xmtextfieldsetstring, $xrt_range_hi_wq(2), sq
 n = fits_key(h, 'NAXIS2  ')
 sq = string(n)
 ty,'sq =', sq
 xmtextfieldsetstring, $xrt_range_lo_wq(3), sq
 xmtextfieldsetstring, $xrt_range_hi_wq(3), sq
 endsubr
 ;========================================================
subr xrt_options_cb
 errormess,'nothing happens yet'
 endsubr
 ;========================================================
subr xrt_filepath_cb
 s = getenv('ANA_XRT_PATH')
 if num_elem(s) eq 0 then s ='/Users/shine/fpp/fits/'
 xmtextfieldsetstring, $xrtrootpath, s
 endsubr
 ;========================================================
subr xrt_filepath2_cb
 s = getenv('ANA_XRT_PATH_SEC')
 if num_elem(s) eq 0 then s ='/Users/shine/fpp/fits/'
 xmtextfieldsetstring, $xrtrootpath, s
 endsubr
 ;========================================================
subr xrt_browse_time_txt_cb, k
 if k eq 4 then {
   dy = fix(xmtextfieldgetstring($xrt_browse_time_txt(4))
   xrtnewdatedoy, dy
   return
 }
 if k le 2 then {
   ;read all 3 and make a new doy
   y = fix(xmtextfieldgetstring($xrt_browse_time_txt(0))
   imy = fix(xmtextfieldgetstring($xrt_browse_time_txt(1))
   idm = fix(xmtextfieldgetstring($xrt_browse_time_txt(2))
   dy = doy(y,imy,idm)
 }
 ;check for silliness by running through newdatedoy
 xrtnewdatedoy, dy
 endsubr
 ;========================================================
subr xrtdoy_down
 dy = fix(xmtextfieldgetstring($xrt_browse_time_txt(4))
 dy = dy - 1
 xrtnewdatedoy, dy
 endsubr
 ;========================================================
subr xrtdoy_up
 dy = fix(xmtextfieldgetstring($xrt_browse_time_txt(4))
 dy = dy + 1
 xrtnewdatedoy, dy
 endsubr
 ;========================================================
subr xrtnewdatedoy, dy
 ;checks for wraparounds in dy and sets all date fields
 y = fix(xmtextfieldgetstring($xrt_browse_time_txt(0))
 if dy le 0 then { y = y - 1   dy = 365 + (y%4 eq 0) }
 if dy gt 365 then { if y%4 then { y = y +1 dy = 1 } else {
   if dy gt 366 then { y = y +1 dy = 1} }
 }
 xmtextfieldsetstring, $xrt_browse_time_txt(4), string(dy)
 doytodom, y, dy, imy, idm
 xmtextfieldsetstring, $xrt_browse_time_txt(0), string(y)
 xmtextfieldsetstring, $xrt_browse_time_txt(1), string(imy)
 xmtextfieldsetstring, $xrt_browse_time_txt(2), string(idm)
 endsubr
 ;========================================================
subr xrt_list_cb
 ;action for list files button
 rpath = xmtextfieldgetstring($xrtrootpath)
 y = fix(xmtextfieldgetstring($xrt_browse_time_txt(0))
 imy = fix(xmtextfieldgetstring($xrt_browse_time_txt(1))
 idm = fix(xmtextfieldgetstring($xrt_browse_time_txt(2)) 
 xrt_browse_time_txt_cb, 1
 subpath = sprintf('%0.4d/%0.2d/%0.2d/', y, imy, idm)
 $xrtdatepath = rpath + subpath
 $xrtobsdir = getdirects($xrtdatepath)
 if isscalar($xrtobsdir) then { errormess,'no files found in path\n'+$xrtdatepath  return }
 $xrtobsdir = sort($xrtobsdir)
 nf = num_elem($xrtobsdir)
 ;create widget if it doesn't exist yet
 if defined($xrtobsfilelistwidget) ne 1 then {
    compile_file, getenv('ANA_WLIB') + '/xrtobslistwidgettool.ana'
 }
 ;loop over all files and put in list
 ;;xtpopdown, $xrtobsfilelistwidget
 ;delete the old stuff (if any)
 xmlistdeleteall, $xrtobsfilelist
 for i= 0, nf-1 do {
 xmlistadditem, $xrtobsfilelist, $xrtobsdir(i)
 }
 xtpopup, $xrtobsfilelist
 endsubr
 ;========================================================
subr xrt_obsfilelist_entry_cb
 ;action when choosing a file path
 $xrtscifilepath = $list_item
 $xrtobsfilepos = $list_item_position
 path = $xrtdatepath + $xrtscifilepath
 xrt_img_list, path
 endsubr
 ;========================================================
subr xrt_img_list, path
 $xrtimgdir = getfiles(path, 5000)
 if isscalar($xrtimgdir) then { errormess,'no files found in path\n'+path  return }
 $xrtimgdir = sort($xrtimgdir)
 nf = num_elem($xrtimgdir)
 ;create widget if it doesn't exist yet
 if defined($xrtimgfilelistwidget) ne 1 then {
    compile_file, getenv('ANA_WLIB') + '/xrtimglistwidgettool.ana'
 }
 ;loop over all files and put in list
 xtpopdown, $xrtimgfilelistwidget
 xmsetlabel, $xrtimglistcount, sprintf('%d images found', nf)
 ;delete the old stuff (if any)
 xmlistdeleteall, $xrtimgfilelist
 xmtextfieldsetstring, $xrtimgpathtext, path
 ;11/1/2006 - add a number
 for i= 0, nf-1 do {
   s = xrtoneliner($xrtimgdir(i))
   ;just put description in this list, actual file name not usually
   ;useful to the user
   xmlistadditem, $xrtimgfilelist, sprintf('%3d ',i+1) + s
   ;xmlistadditem, $xrtimgfilelist, removepath($xrtimgdir(i)) + s
 }
 xtpopup, $xrtimgfilelistwidget
 ;no selection to start with
 $xrtimgdir_list_item = -1
 endsubr
 ;========================================================
func xrtoneliner(name)
 ;generates a string with a brief description of file contents
 fty = data_type(name, h, params)
 ;we expect only type 1 (FITS file)
 s = ''
 if fty ne 1 then {
   s = 'not a FITS file, hence probably not Solar B data'
 } else {
   ;look for keywords
   st = fits_key(h, 'TIME-OBS')
   if strcontains(upcase(st), 'UNDEFINED') then st = 'no time'
   iq = fits_key(h, 'CHIP_SUM')
   sobs = ''
   if isscalar(iq) then {
     if iq ge 2 then sobs = sprintf('[S%d]', iq)
   }
   s = s + sprintf('%-12s %-5s', st, sobs)
   nd = params(2)
   s = s + sprintf(':%dD ', nd)
   if nd ge 2 then {
     for j=0,nd-2 do s = s + sprintf('%dx', params(j+3))
   }
   s = s + sprintf('%d', params(nd+2)
   ;a minimum length
   nq = num_elem(s)
   if nq lt 32 then s = s + blanks(32-nq)
   sq = fits_key(h, 'EC_FW1_')
   s = s + sq + ', '
   sq = fits_key(h, 'EC_FW2_')
   s = s + sq + ' '
   sq = fits_key(h, 'EXPTIME')
   if isscalar(sq) then s = s + sprintf(',%5.2fs', sq)
  }
 return, s
 endfunc
 ;======================================================= 
subr myxrtlist
 ;create a text widget for keeping names of favorite images
 if defined($myxrtlistwidget) eq 0 then {
    compile_file, getenv('ANA_WLIB') + '/xrtfavoritewidgettool.ana'
 }
 xtpopup, $myxrtlistwidget
 endsubr
 ;======================================================= 
subr myxrtlist_cb
 if $xrtimgdir_list_item le 0 then {
   errormess,'no image selected'
   return }
 myxrtlist	;makes sure it is created and visible
 name = $xrtimgdir($xrtimgdir_list_item - 1)
 sq = sprintf('%s\n', name)
 xmtextappend, $mysblist_text, sq
 endsubr
 ;======================================================= 
subr xrtlistnext_cb, direction
 ;finds the next or previous available hourly file
 pos = $xrtobsfilepos + direction
 if pos lt 1 or pos gt (num_elem($xrtobsdir)) then {
   errormess,'you are at an\nedge of the list\n'
   return }
 ;fake a click on the list
 xmlistselect, $xrtobsfilelist, pos, 2
 endsubr
 ;======================================================= 
subr myxrtlist_save_cb
 name = xmtextfieldgetstring($myxrtlistsavefile)
 s = xmtextgetstring($mysblist_text)
 lun = get_lun()
 if lun lt 0 then { errormess,'no available LUN for file write!' return }
 stat = openw(lun, name)
 if stat ne 1 then {
 	errormess,'can''t write to file: ',name,'\ncheck name and permissions\n'
 	close, lun
	return }
 printf,lun,s
 close,lun
 endsubr
 ;======================================================= 
subr mysblist_load_cb
 name = xmtextfieldgetstring($mysblistsavefile)
 lun = get_lun_and_openr(name)
 if lun lt 0 then return, 0 
 s =''
 while ( readf(lun,s) ) {
 xmtextappend, $mysblist_text, s, 1
 }
 close, lun
 endsubr
 ;======================================================= 
subr xrtclickaction_cb
 $xrtclickaction = $option_value
 endsubr
 ;========================================================
subr xrt_imgpath_cb
 ;when someone changes the path in the $xrtimgfilelistwidget and hits
 ;return we get here
 path = xmtextfieldgetstring($xrtimgpathtext)
 xrt_img_list, path
 endsubr
 ;========================================================
subr getxrtcompstring(h, s1)
 ;decode the compression parameters from a Solar B XRT FITS header
 s1 = ''
 ;;sq = fits_key(h, 'BITCOMP1')
 ;;if isscalar(sq) then s1 = sprintf('L%d', sq)
 ;;sq = fits_key(h, 'IMGCOMP1=')
 ;;if isscalar(sq) then s1 = s1 + $sbctype(sq)
 sq = fits_key(h, 'QTABLE1')
 if isscalar(sq) then s1 = s1 + $sbqvalue(sq)
 sq = fits_key(h, 'BITSPP')
 if isscalar(sq) then s1 = s1 + sprintf(' %4.1fbpp ', sq)
 endsubr
 ;========================================================
subr xrt_imgfilelist_entry_cb, mode
 ;an image has been choosen, the list of file names is in $xrtimgdir,
 ;the lines in the image selector have descriptions, not file names
 narg = !narg
 $xrtimgdir_list_item = $list_item_position
 k = $xrtimgdir_list_item - 1
 if narg gt 0 then name = $xrt_names(k) else name = $xrtimgdir(k)
 if $xrtclickaction eq 3 then {
   ;this adds the file name to a list keep in another text widget
   mysblist	;makes sure it is created and visible
   sq = sprintf('%s\n', name)
   xmtextappend, $mysblist_text, sq
   return
 }
 if $xrtclickaction eq 0 or $xrtclickaction eq 2 then fty = file_inspect(name)
 if $xrtclickaction eq 1 or $xrtclickaction eq 2 then {
   ;we have a display to do
   dataread, $xrtdata, name, $xrthead
   ;dataread does a fits_time_decode so we have time and position info
   ;we have to select the views, start with $current_window and up

   win = $current_window
   sq = fits_key($xrthead, 'EC_FW1_ ')
   s = sprintf('%s, ', sq)
   sq = fits_key($xrthead, 'EC_FW2_ ')
   s = s + sprintf('%s', sq)
   sq = fits_key($xrthead, 'EXPTIME ')
   if isscalar(sq) then s = s + sprintf(',%5.2fs\n', sq)

   iq = fits_key($xrthead, 'CHIP_SUM')
   sobs = '\n'
   if isscalar(iq) then {
     if iq ge 2 then sobs = sprintf('\n[S%d]', iq)
   }
   $solarbstrs = sobs + s
   getxrtcompstring, $xrthead, c1str
   $solarbstrs += c1str
   ty,'$solarbstrs = ', $solarbstrs
   ;check for special cases
   ;an image or set of images, add a few things for box
   ;FG or SP or something else?
   $view_data_type(win) = 3	;this marks it as Solar B
   $view_header(win) = $xrthead
   update_globals, win, $xrtdata
 }
 endsubr
 ;========================================================
