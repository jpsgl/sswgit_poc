;the TRACE selection tool widgets, also uses the file trace3dselectortool.ana
 ;=======================================================================
subr readtracelistsetup, i
 ;makes copies of the list descriptors for use in readtracelistimage
 type = $image_list(0,i)
 if (type ne 1) then {
 	errormess,'movie list error\nexpected TRACE list'
	return }
 $mvget_selected_tops = eval(sprintf('$image_list_tops%d',i))
 $mvget_selected_image_files = eval(sprintf('$image_list_files%d',i))
 $mvget_selected_image_nums = eval(sprintf('$image_list_nums%d',i))
 $mvget_selected_image_times = eval(sprintf('$image_list_times%d',i))
 $mvget_selected_image_xc = eval(sprintf('$image_list_xc%d',i))
 $mvget_selected_image_yc = eval(sprintf('$image_list_yc%d',i))
 $mvget_selected_image_pixsc = eval(sprintf('$image_list_pixsc%d',i))
 ;to allow for exposure control, set $trace_base_et to -1 here so we
 ;know when we get the first in readtracelistimage
 $trace_base_et = -1
 endsubr
 ;=======================================================================
func readtracelistimage(zq,i,x1,x2,y1,y2,hbot,htop,scaletype)
 ;10/13/2000 (yes, a Friday) - change to always put out a standard solar
 ;oreintation, this simplifies logic for image differences, merges, etc in
 ;movies. Means ord arg no longer used.
 ;;func readtracelistimage(zq,i,ord,x1,x2,y1,y2,hbot,htop,scaletype)
 ;used to load an image from the list id in $mvget_trace_list_id
 ;8/17/2000 - change the way cutouts are done, we now want (x1:x2,y1:y2)
 ;to be in "CCD" coordinates but using the solar orientation, this means
 ;some extra effort to translate the user input but then we can apply it
 ;to any size image and return the overlap between the image read and the
 ;area desired
 ;4/19/2000 - for exposure compensation, using a different flag that
 ;gets set by the caller now rather than $mvget_aec_flag and $mvget_aec_type
 ;the new flags are $trace_aec_flag and $trace_aec_type
 ;4/9/99 - added globals $xc_offset and $yc_offset to return the offsets
 ;from center when doing a cutout, couldn't think of a better way
 ;5/20/98 - also for light curve widget, this makes the program operate
 ;in 2 modes, the light curve mode just has 2 args
 narg = !narg
 ;which file are we from?
 nf = min(sieve($mvget_selected_tops ge i))
 ;ty,'nf =', nf
 ;check if the current file, if not we have to open file, etc
 fname = $mvget_selected_image_files(nf)
 if  $t3d_file_name ne fname then {
   ty,'new file needed, fname:', fname,' $t3d_file_name:', $t3d_file_name
   $t3d_file_name = fname
   if t3d_open_file() ne 1 then errormess,'problem opening file'
 }
 j = $mvget_selected_image_nums(i)
 x = get_trace_image(j)
 if isarray(x) eq 0 then {
   sq = sprintf('bad TRACE image\ncan''t decode\nimage %d in\n%s',j, $t3d_file_name)
   errormess,sq  return, -1 }
 ;5/20/98 - 2 modes now, if narg is 2, the simple one
 ;despiking done here, see comment in mvget_creat_cube
 if $despike_movie_flag then x = despike_internal(x)
 ;emi reduction, here we have the raw image whereas in other place we have a re-order
 zeroifundefined, $emi_movie_flag
 if $emi_movie_flag then x = remove_emi(x, -1)
 xx = $t3d_dp_header(*,j)
 if $little_endian then { amp_case = extract_bits(xx(102),4,1) } else {
 	amp_case = extract_bits(xx(102),12,1) }
 if narg eq 2 then {
   ;we have to do an orientation here for the simple case before returning
   if amp_case then zq = reorder(x, 4) else zq = reorder(x, 7)
   return, 1 }

 nx = dimen(x,0)	ny = dimen(x,1)
 ;do the orientation here since we hide the A/B stuff and align with solar
 ;n/s and e/w
 ;also, the subarea was determined using a re-oriented image so we need to
 ;adjust the cutout
 ;;iord = ord	;it was already xor'ed by 2 in mvget_creat_cube
 trace_ccdpositions, xx, ix, iy, ix2, iy2, nx_ccd, ny_ccd, bm, xsum
 ;want to pass on the product of summing and binning as a global
 $trace_bsum = bm*xsum
 ;some checks whilst we debug/test this
 if nx ne nx_ccd or ny ne ny_ccd then {
 	ty,'inconsistent CCD sizes', nx, ny, nx_ccd,ny_ccd }
 ;the $trace_ccdxbase and $trace_ccdybase with the image size can be used
 ;now to crop the cutout
 xq = nx_ccd*$trace_bsum - 1
 yq = ny_ccd*$trace_bsum - 1
 if amp_case then {
  ;amp B case, assuming a re-order 4, change if/when we discover otherwise
  ;;iord = iord xor 4	;since 4 xor 2 is still 4
  zq = 1023 - $trace_ccdxbase - xq
  i1 = y1 - zq
  i2 = y2 - zq
  zqy = 1023 - $trace_ccdybase - yq
  j1 = x1 - zqy
  j2 = x2 - zqy
  } else {
  ;amp A case, assuming a re-order 7, change if/when we discover otherwise
  ;;iord = iord xor 5	;since 7 xor 2 is 5
  zq = 1023 - $trace_ccdxbase
  i1 = zq - y2
  i2 = zq - y1
  zqy = 1023 - $trace_ccdybase
  j1 = zqy - x2
  j2 = zqy - x1
  }
 ;;ord = iord	;returned value
 i1 = i1 > 0	j1 = j1 > 0	i2 = i2 < xq	j2 = j2 < yq
 ;if we got out of range input, the subarea might be outside the image,
 ;catch that here by checking for i1 > i2 or j1 > j2
 if check_subarea(i1,i2,j1,j2) then return, -1
 ;so this is the cutout cropped to the extracted image
 ;for some placement schemes, need to know where this is centered in the
 ;Cart. coordinate space used for (x1:x2,y1:y2), also need the offset wrt
 ;the center used to get the solar position (which is the proper center
 ;of the original CCD cutout)
 ;remember that i1:i2, j1:j2 are in original CCD image, so need to check
 ;amp again
 xq = 0.5*(i1+i2)	;x in CCD
 yq = 0.5*(j1+j2)
 if amp_case then {
  $ccd_yc = zq  + xq
  $ccd_xc = zqy + yq
  } else {
  $ccd_yc = zq  - xq
  $ccd_xc = zqy - yq
  }
 ;for the sun position correction, we need the difference of the above
 ;wrt to the center of image (which may be a cutout from the CCD)
 $xc_offset = $ccd_xc + 0.5*(iy+iy2) -1023
 $yc_offset = $ccd_yc + 0.5*(ix+ix2) -1023
 ;now allow for summing and/or binning
 i1 = i1/$trace_bsum	 i2 = i2/$trace_bsum
 j1 = j1/$trace_bsum	 j2 = j2/$trace_bsum
 ;;ty,'resultant i1,i2,j1,j2 =',i1,i2,j1,j2
 ;;dump, x
 ;keep following check in for testing, delete after we are more certain this works
 if i2 ge dimen(x,0) or j2 ge dimen(x,1) or i1 lt 0 or j1 lt 0 then {
  ty,'bad subarea range'
  beep
  sq = sprintf('bad subarea range\ndimens: %d, %d\nrange: (%d:%d, %d:%d)\ntry again', -
  	dimen(x,0), dimen(x,1), i1, i2, j1, j2)
  errormess, sq
  return, -1  }
 zq = x(i1:i2, j1:j2)
 ;10/13/2000 - always reorder to standard solar coords (N up, W to right)
 if amp_case then zq = reorder(zq, 4) else zq = reorder(zq, 7)
 ;the adjustment of hbot and htop is done for TRACE exposure compensation, it
 ;is only used for scaletype 0 or 3 and it is important not to change
 ;htop and hbot for case 2
 ;it depends on a ref exposure $trace_base_et. If the limits are both 0, we
 ;default to min/max and don't make an adjustment (which would be meaningless
 ;and cause a divide by zero)
 ;9/14/98 - important, for situations where we are not scaling the data directly,
 ;a different strategy is required and we must scale the entire images

 if $trace_aec_flag then {
 ;two situations, if we are just making a scaled cube, then just mod
 ;hbot and htop, otherwise provide a version of the data with
 ;pedestal removed and scaled to ref exposure and pedestal restored
 ;later we'll have a flatfield option, this is just to be able to make movies
 ;without the levels changing (much)
 if $little_endian then xsum = extract_bits(xx(102),8,8) else
    xsum = extract_bits(xx(102),0,8)
 if xsum le 1 then ped = 87 else ped = 110
 et = trace_expose(xx)
 if $trace_base_et lt 0 then $trace_base_et = et
 ;adjust
 fac = et/$trace_base_et
 if $trace_aec_type then {
     ty,'rescaling image, ped, fac =', ped, fac
     ;here we modify zq directly, regardless of any scaling later (which will
     ;not be done for the output file case)
     zq = zq - ped
     zq = zq * (1./fac) +ped
     zq = word(rfix(zq))
 } else {
    if scaletype eq 0 or scaletype eq 3 then {
    if hbot ne 0 or htop ne 0 then {
    ;do automatic exposure control here, need summing for pedestal estimate
    hbot = (hbot - ped) * fac + ped
    htop = (htop - ped) * fac + ped
    ;;ty,'exposure adjusted scaling ranges', hbot, htop
 } } }
 }

 ;the scaling and reorder that was previously done here has been transferred
 ;back to the caller (mvget_create)

 return, 1
 endfunc
 ;===============================================================================
subr t3d_qopt_cb
 $qs_selection_value = $option_value
 endsubr
 ;===============================================================================
subr t3d_wopt_cb
 $wave_selection_value = $option_value
 endsubr
 ;===============================================================================
subr t3d_copt_cb
 $tclass_selection_value = $option_value
 endsubr
 ;===============================================================================
subr t3d_bopt_cb
 $bin_selection_value = $option_value
 endsubr
 ;===============================================================================
subr t3d_sopt_cb
 $subarea_selection_value = $option_value
 endsubr
 ;===============================================================================
subr t3d_select_time_cb
 if $radio_state then {
 $t3d_time_style_flag = $radio_button
 }
 endsubr
 ;===============================================================================
subr t3dselectreset_cb
 ;the reset, put everybody in the "any" state and apply to all the widgets
 zero,$wave_selection_value,$bin_selection_value, $subarea_selection_value,-
 $tclass_selection_value,$sequence_selection_value,$frame_selection_value
 zero, $target_selection_value, $exposure_selection_lo_value,-
 $exposure_selection_hi_value,$size_selection_lo_value,$size_selection_hi_value
 zero, $qs_selection_value
 xmsetoptionselection, $t3d_wopt_wq(0), $t3d_wopt_wq(1)
 xmsetoptionselection, $t3d_sopt_wq(0), $t3d_sopt_wq(1)
 xmsetoptionselection, $t3d_qopt_wq(0), $t3d_qopt_wq(1)
 ;;;xmsetoptionselection, $t3d_copt_wq(0), $t3d_copt_wq(1)
 xmsetoptionselection, $t3d_bopt_wq(0), $t3d_bopt_wq(1)
 for i=1, num_elem($t3d_select_text_wq)-1 do
 xmtextfieldsetstring, $t3d_select_text_wq(i), ''
 for i=1, num_elem($t3d_range_lo_wq)-1 do
 xmtextfieldsetstring, $t3d_range_lo_wq(i), ''
 for i=1, num_elem($t3d_range_hi_wq)-1 do
 xmtextfieldsetstring, $t3d_range_hi_wq(i), ''
 endsubr
 ;===============================================================================
subr t3d_selected_entry_cb
 ;list entry callback
 $t3dselected_item_position = $list_item_position  ;to avoid misunderstandings
 iq = $list_item_position - 1
 ;which file are we from?
 nf = min(sieve($t3d_selected_tops ge iq))
 ;check if the current file, if not we have to open file, etc
 fname = $t3d_selected_image_files(nf)
 if $t3d_file_name ne fname then {
   ;break into path and name and load the text widgets
   ;xmtextfieldsetstring, $t3dtext1, getfilepath(fname)
   ;xmtextfieldsetstring, $t3dtext2, removepath(fname)
   $t3d_file_name = fname
   if t3d_open_file() ne 1 then errormess,'problem opening file'
 }
 iq = $t3d_selected_image_nums(iq)
 xmtextfieldsetstring, $t3d_image, string(iq)
 if $t3d_view_flag eq 0 or $t3d_view_flag eq 2 then t3d_inspect, iq
 if $t3d_view_flag gt 0 then t3d_view, iq
 ;save the name and position for possible logging
 $t3d_select_file_name = fname
 $t3d_select_file_image = iq
 endsubr
 ;=============================================================================== 
subr t3d_select_delete_cb
 ;deletes the current selection from the "selected" images list
 ;if in the list of lists, that list is also adjusted
 iq = $t3dselected_item_position - 1
 ind = sieve($t3d_selected_tops ge iq)
 if isscalar(ind) then { errormess, 'internal error in\nselected list widget'
 	return }
 xmlistdeleteitem, $t3selectedwidget, $t3dselected_item_position
 ;all $t3d_selected_tops with ind are affected
 $t3d_selected_tops(ind) = $t3d_selected_tops(ind) - 1
 n = dimen($t3d_selected_image_nums,0) - 1
 if iq lt n then {
 for i=iq,n-1 do {
   $t3d_selected_image_nums(i) = $t3d_selected_image_nums(i+1)
   $t3d_selected_xc(i) = $t3d_selected_xc(i+1)
   $t3d_selected_yc(i) = $t3d_selected_yc(i+1)
   $t3d_selected_times(i) = $t3d_selected_times(i+1)
   $t3d_selected_pixsc(i) = $t3d_selected_pixsc(i+1)
 }}
 save = !REDIM_WARN_FLAG
 !REDIM_WARN_FLAG = 0
 redim, $t3d_selected_image_nums, n
 redim, $t3d_selected_xc, n
 redim, $t3d_selected_yc, n
 redim, $t3d_selected_times, n
 redim, $t3d_selected_pixsc, n
 !REDIM_WARN_FLAG = save
 xmsetlabel, $t3select_lab1, -
 	sprintf('%d files, %d images',dimen($t3d_selected_image_files,0), n)
 ;was it included in the list of lists?
 if $t3dselected_list_of_lists ne -1 then {
  ;change the copy in lists of lists
  imagelist_set_arrays_t3d, $t3dselected_list_of_lists	;sets the arrays
  imagelist_reload_list		;need to re-write list of lists
 }
 endsubr
 ;===============================================================================
subr t3dselect_eraseall_cb
 ;erase the whole list
 xmlistdeleteall, $t3selectedwidget
 image_selector, -1,-1	;sets variables internal to image_selector
 xmsetlabel, $t3select_lab1, 'empty'
 endsubr
 ;===============================================================================
subr t3d_selected_list_wq
 if defined($t3selectedwidget) ne 1 then {
 $t3selectedwidget = xmtoplevel_form(0,0, 'Selected TRACE Image List',0,0,5, 5)
 wq = topbuttonsetupsanslink_tl('$t3selected')
 ;also an erase button here
 bq=xmbutton(wq,'erase all','t3dselect_eraseall_cb', $f4,'gray')
 xmposition, bq, 145, 0, 80, 30
 bb = xmbutton(wq, 'put selection\nin list','mylist_cb,1',$f4,'gray') 
 xmposition, bb, 235, 0, 98,50
 bb = xmbutton(wq, 'delete\nselection','t3d_select_delete_cb',$f4,'gray') 
 xmposition, bb, 343, 0, 75,50
 $t3select_lab1 = xmlabel(wq,'files, images ',$f4,'', 1)
 ;xmposition, $t3select_lab1, 340, 5
 xmposition, $t3select_lab1, 0, 35

 sq = 't3d_selected_entry_cb'
 list = xmlist($t3selectedwidget, sq, 20, $f8, 'white', 1)
 xmposition, xtparent(list), 0, 0, 500, 300
 sep = xmseparator($t3selectedwidget, 0, 10, 0)
 xmposition, sep, 0, 0, 10, 10
 formvstack, wq, sep, xtparent(list)
 xmattach, xtparent(list), 0, 1,1,0,1
 $t3selectedwidget = list	;use the actual list for the global
 $t3dselected_list_of_lists = -1	;connection to list of lists, if any
 }
 xtpopup, $t3selectedwidget
 endsubr
 ;===============================================================================
subr time_span, widget, t1, t2, t1r, t2r
 ;get the start and stop times in TAI from a time selection widget assumed
 ;here to be an array of text widgets
 y1 = fix(xmtextfieldgetstring(widget(0)))
 mon1 = fix(xmtextfieldgetstring(widget(1)))
 dom1 = fix(xmtextfieldgetstring(widget(2)))
 d1 = doy(y1,mon1,dom1)
 ty,'y1,mon1,dom1, d1 =', y1,mon1,dom1, d1
 s = xmtextfieldgetstring(widget(3))
 decode_time_str, s, h1, m1, s1
 y2 = fix(xmtextfieldgetstring(widget(4)))
 mon2 = fix(xmtextfieldgetstring(widget(5)))
 dom2 = fix(xmtextfieldgetstring(widget(6)))
 d2 = doy(y2,mon2,dom2)
 ty,'y2,mon2,dom2, d2 =', y2,mon2,dom2, d2
 s = xmtextfieldgetstring(widget(7))
 decode_time_str, s, h2, m2, s2
 ;5/9/99 - the minimum delta time is read in by image_selector
 ;given y1, y2, d1, d2 etc, generate a series of potential hourly files
 ;a special routine has been provided to make a file name given TAI so
 ;we just have to convert the start and end times to tai
 t1 = tai_from_date(y1, d1, h1, m1, s1)
 t2 = tai_from_date(y2, d2, h2, m2, s2)
 if t1 gt t2 then switch, t1, t2
 ;make versions rounded to hour for simpler loop
 t1r = tai_from_date(y1, d1, h1, 0, 0)
 t2r = tai_from_date(y2, d2, h2, 0, 0)
 if m2 gt 0 or s2 gt 0 then t2r = t2r + 3599.0
 endsubr
 ;===============================================================================
subr t3dapplyselect_cb
 ;actually apply the selection to the current dp_head and load a list
 ;widget (or append to one) with the results
 ;start up the selected list widget if not already done

 t3d_selected_list_wq
 ;need to erase previous? (if any)
 if not($t3d_select_add_flag) then t3dselect_eraseall_cb
 
 $select_stop = 0
 change_button_label, $t3dapplyselect_but, 'STOP', 'red'
 ;first we decide what hourly files to use and then apply the selection to
 ;each one, appending the list in the list widget and the internal arrays,
 ;we probably want to be able to later save several such lists and possibly
 ;want to make several independent selections while going through the files
  
 ;there are two cases, either we just use the current hourly file or we have
 ;a time range and have to figure out which to use
 if $t3d_time_style_flag eq 0 then {
 	image_selector, 0, 0	;simple case, just use the current file
 } else {
 ;more complicated, first get the time ranges
 time_span, $t3d_select_time_txt, t1, t2, t1r, t2r
 tt = t1r
 ty,'time range =', t1r, t2r
 while (tt lt t2r) {
  ;;ty,'time is now:', tt
  ;10/14/98 - allow a stop from the button
  xtloop, 2
  if $select_stop then break	;stop, but keep what we have

  ;get the file name
  fname = tri_name_from_tai(tt)
  ty,'file name =', fname
  ;find it
  if open_tri_file(fname) eq 1 then {
   ty,'conditional is:', (tt eq t1r or tt ge (t2r-3600))
   if (tt eq t1r or tt ge (t2r-3600)) then
   	image_selector, t1, t2 else image_selector,0,0
  }
  tt = tt + 3600.
 }
 }
 change_button_label, $t3dapplyselect_but, 'apply selection', 'darkgreen'
 $select_stop = 1

 ;don't use if no items selected
 if $select_count le 0 then {
  errormess,'Sorry, nothing found'
  return }
 $t3dselected_list_of_lists = -1  ;no backlook, set in imagelist_add_item
 ;now we can put it in the list of lists
 add_selection_to_listoflists, 1
 endsubr
 ;===============================================================================
subr add_selection_to_listoflists, type
 image_list_wq
 lparent = xtparent($imagelistwidget)
 xmgetwidgetsize, lparent, dx, dy
 ;the name your list has not been popular so tag each one with the type
 ;;sq = 'This selection of images can be\nadded to the lists of lists.\nEnter an ID tag or just return to not enter it'
 ;;xmprompt, sq,'','prompt_cb', 1, $f4, '',!screen_width/2 -50,!screen_height/2-50
 ;;xtloop, 1
 ;we get here after the prompt_cb lets us (it sets !motif = 0)
 ;;tag = $textfield_value
 ;;if tag ne '' then {
 ;;imagelist_add_item, type, tag
 ;;}
 if type eq 1 then tag = string($image_list_count+1)+': raw TRACE list'
 if type eq 3 then tag = string($image_list_count+1)+': La Palma thumbs'
 imagelist_add_item, type, tag
 xmsize, lparent, dx, dy
 xtpopup, $imagelistwidget
 ;;!motif = 1
 endsubr
 ;===============================================================================
subr getwave, gotwaveinfo, wl, xx
 if gotwaveinfo eq 0 then {
 gotwaveinfo = 1
 if $little_endian then {
 xq = xx(296)   swab, xq
 wl =  extract_bits(xq, 7, 5)
 } else  wl =  extract_bits(xx(296), 7, 5)
 }
 endsubr
 ;===============================================================================
subr image_selector, t1, t2
 ty,'in image_selector, t1, t2 =', t1, t2
 ;works on the currently open hourly file, appends results to existing ones
 ;if t1 is -1, then we reset the internal counters
 if t1 eq -1 then {
  nselected = 0		;init the selection number
  nfiles = 0		;and the list of files
  $select_count = 0	;need a global accumulator also
  last_tai = 0		;used by the minimum delta t check
  return
 }
 
 ;if t1 and t2 are non-zero, it uses them as TAI times as part of the
 ;selection within this hourly file
 if t1 ne 0 or t2 ne 0 then tmatch_flag = 1 else tmatch_flag = 0
 
 if defined($t3d_dp_header) eq 0 then { errormess,'no TRACE hourly\nfile open'
 	return }
 if defined(nselected) eq 0 then {
  nselected = 0		;init the selection number
  nfiles = 0		;and the list of files
  $select_count = 0	;need a global accumulator also
  last_tai = 0		;used by the minimum delta t check
 }

 ;should put in code to avoid reading these each time when doing a series,
 ;could use something similar to the t1 = -1 method above
 ;(but not exactly, since that is called only when the list is erased)
 seqname = strtrim(xmtextfieldgetstring($t3d_select_text_wq(1)))
 frmname = strtrim(xmtextfieldgetstring($t3d_select_text_wq(2)))
 tarname = strtrim(xmtextfieldgetstring($t3d_select_text_wq(3)))
 exp_lo = float(xmtextfieldgetstring($t3d_range_lo_wq(1)))
 size_lo = float(xmtextfieldgetstring($t3d_range_lo_wq(2)))
 exp_hi = float(xmtextfieldgetstring($t3d_range_hi_wq(1)))
 size_hi = float(xmtextfieldgetstring($t3d_range_hi_wq(2)))
 range_flag = exp_lo ne 0 or exp_hi ne 0 or size_lo ne 0 or size_hi ne 0
 ;5/9/99 - also a minimum delta time
 s = xmtextfieldgetstring($t3d_select_time_txt(8))
 decode_time_str, s, h3, m3, s3
 min_time_bwt = s3 + 60.*m3 + 3600.*h3
 ty,'min_time_bwt = ', min_time_bwt
 ;assume everybody a match until we discover different
 matches = zero(intarr($t3d_nimages)) + 1
 ;also save the times and positions
 times = zero(dblarr($t3d_nimages))
 ;save center positions and pixel scale for all images
 xpositions = zero(fltarr($t3d_nimages))
 ypositions = xpositions
 pixel_scale = xpositions
 ty,'$t3d_nimages =', $t3d_nimages
 for i= 0, $t3d_nimages-1 do {
 xx = $t3d_dp_header(*,i)
 gotccdinfo = 0
 gotwaveinfo = 0
 gotbb = 0

 while (1) {	;so we can break out early
 ;10/14/97 bypass "bad" images
 if $t3d_n_bytes(i) le 1 then { matches(i) = 0   break }
 ;always get the time for a clock and a time file
 tq = xx(3:5)
 if $little_endian then swab, tq
 tai = trace_tai(tq)
 ;don't actually set the value in times unless we get a match

 ;check for a t match
 if (tmatch_flag) then {
  if tai lt t1 or tai gt t2 then { matches(i) = 0   break }
 }

 ;if we pass that test, check min delta t
 if tai lt (last_tai + min_time_bwt) then { matches(i) = 0   break }
 ;wavelength, 0 => any
 if ($wave_selection_value) then {
 getwave, gotwaveinfo, wl, xx
 ;now we just have to know what the values mean
  if $wmatches(wl) ne $wave_selection_value then { matches(i) = 0   break }
 }
 if ($qs_selection_value) then {
 ;10/20/2000 - look for QS mismatches, we assume that the correct values are
 ;20, 28, 10, 2
 bb = bmap(xx)
 gotbb = 1
 quad = bb(529) and 0x3f
 getwave, gotwaveinfo, wl, xx
 if defined($qs_righteous) eq 0 then {
  $qs_righteous = [28,28,28,28,10,10,10,10,2,2,2,2,20,20,20,20,20,20,20,20,20]
  $qs_righteous = [$qs_righteous, 20,20,28,28,10,2,28,10,2,20,20]
  }
  if abs($qs_righteous(wl) - quad) lt $qs_selection_value then
  	{ matches(i) = 0   break }
  }
 ;binning, 0 => any
 if ($bin_selection_value) then {
 ;binning, from frame header, just get all CCD position stuff
 trace_ccdpositions, xx, ix, iy, ix2, iy2, nx, ny, bm, csum
 gotccdinfo = 1
 ncase $bin_selection_value-1
  if bm ne 1 then { matches(i) = 0   break }
  if bm ne 2 then { matches(i) = 0   break }
  if bm ne 4 then { matches(i) = 0   break }
  if bm ne 8 then { matches(i) = 0   break }
 endcase
 }

 ;subarea, from frame header
 if ($subarea_selection_value) then {
 sai = xx(413)		;this is subarea
 if $little_endian then { swab, sai } 
 if sai ne $subarea_selection_value-1 then { matches(i) = 0   break }
 }

 ;now the various names
 if num_elem(seqname) gt 0 then {
 seqid = lmap([xx(199), xx(198)])   if $little_endian then swapl, seqid
 if strpos(find_seq_name(seqid), seqname) lt 0 then { matches(i) = 0   break }
 }
 if num_elem(frmname) gt 0 then {
 frid = xx(294)  if $little_endian then swab, frid
 if strpos(find_frame_name(frid), frmname) lt 0 then { matches(i) = 0  break }
 }
 if num_elem(tarname) gt 0 then {
 targid = xx(205)  if $little_endian then swab, targid
 if strpos(find_tar_name(targid), tarname) lt 0 then { matches(i) = 0   break }
 }
 
 ;now the range items
 if range_flag then {
  et = trace_expose(xx)
  if et lt exp_lo then { matches(i) = 0   break }
  if exp_hi gt 0 and et gt exp_hi then { matches(i) = 0   break }
  
  if gotccdinfo eq 0 then
  	trace_ccdpositions, xx, ix, iy, ix2, iy2, nx, ny, bm, csum
  if (nx<ny) lt size_lo then { matches(i) = 0   break }
  if (size_hi gt 0) and ((nx>ny) gt size_hi) then { matches(i) = 0   break }
 }
 
 ;OK, if we are here, it should be a match and we have to set the various
 ;parameters we need
 last_tai = tai
 times(i) = tai
 ;get positions if didn't already
 if gotccdinfo eq 0 then
  	trace_ccdpositions, xx, ix, iy, ix2, iy2, nx, ny, bm, csum
 ;load positions and scale
 pixel_scale(i) = $trace_plate_scale*bm*csum
 ;wavelength, from frame header
 getwave, gotwaveinfo, wl, xx
 offset = trace_wave_offset(wl, tai)
 if gotbb eq 0 then { bb = bmap(xx)  gotbb = 1 }
 w1 = bb(527)
 w2 = 180 - bb(528)	;this one is peculiar
 wedge2solar, w1, w2, x, y
 xpositions(i) = x - $trace_plate_scale*(.5*(iy+iy2) - 511.5 - offset(0))
 ypositions(i) = y - $trace_plate_scale*(.5*(ix+ix2) - 511.5 - offset(1))
 
 break	;this breaks out of the "fake" while if we haven't already
 }
 }
 ;how many did we get
 ty,'# of matches = ', fix(total(matches))
 selects = sieve(matches)
 ;get the times for only these
 if isscalar(selects) then return	;nothing found that matches, go home
 ;get the times, etc for only these
 times = sieve(times, matches)
 xpositions = sieve(xpositions, matches)
 ypositions = sieve(ypositions, matches)
 pixel_scale = sieve(pixel_scale, matches)
 if nselected then
   { $t3d_selected_image_nums = [$t3d_selected_image_nums, selects]
     $t3d_selected_times = [$t3d_selected_times, times]
     $t3d_selected_xc = [$t3d_selected_xc, xpositions]
     $t3d_selected_yc = [$t3d_selected_yc, ypositions]
     $t3d_selected_pixsc = [$t3d_selected_pixsc, pixel_scale]
   } else {
     $t3d_selected_image_nums = selects
     $t3d_selected_times = times
     $t3d_selected_xc = xpositions
     $t3d_selected_yc = ypositions
     $t3d_selected_pixsc = pixel_scale
   }
 nselected = num_elem($t3d_selected_image_nums)
 $select_count = nselected	;to let others know the count
 nq = nselected - 1
 ;register this file
 if nfiles then {
   $t3d_selected_image_files = [$t3d_selected_image_files, $t3d_file_name]
   $t3d_selected_tops = [$t3d_selected_tops, nq]
   } else {
   $t3d_selected_tops = [nq]
   $t3d_selected_image_files = [$t3d_file_name]
   }
 nfiles = num_elem($t3d_selected_image_files)
 ;add these to the list widget
 k = nselected
 nq = num_elem(selects)
 k = k - nq	;the next new one
 for i= 0, nq-1 do {
 j = selects(i)
 sq = sprintf('%3d %3d ',k,j) + trace_oneliner(wmap($t3d_dp_header(*,j)), 1)
 k = k+1
 xmlistadditem, $t3selectedwidget, sq
 }
 xmsetlabel, $t3select_lab1, sprintf('%d files, %d images',nfiles, nselected)
 endsubr
 ;===============================================================================
subr selector_link_cb
 ;use a link window to set ranges, etc
 ;gets called by view_input_cb
 win = $link_window	;before it can change
 equate, x, eval('$view_trace_info_int_'+string(win))
 equate, xf, eval('$view_trace_info_flt_'+string(win))
 $wave_selection_value = $wmatches(x(2))
 xmsetoptionselection, $t3d_wopt_wq(0), $t3d_wopt_wq($wave_selection_value+1)
 iq = x(3)	;the bin parameter
 $qs_selection_value = 0
 xmsetoptionselection, $t3d_qopt_wq(0), $t3d_qopt_wq(1)
 if iq le 1 then jq = 0 else if iq eq 2 then jq = 1 else if iq eq 4 then jq = 2 else if iq ge 8 then jq = 3
 $bin_selection_value = jq + 1
 xmsetoptionselection, $t3d_bopt_wq(0), $t3d_bopt_wq(jq+2)
 jq = x(4)
 $subarea_selection_value = jq + 1
 xmsetoptionselection, $t3d_sopt_wq(0), $t3d_sopt_wq(jq+2)
 ;blank the exposures because they will vary
 xmtextfieldsetstring, $t3d_range_lo_wq(1), ''
 xmtextfieldsetstring, $t3d_range_hi_wq(1), ''
 xmtextfieldsetstring, $t3d_range_lo_wq(2), string(x(0)<x(1))
 xmtextfieldsetstring, $t3d_range_hi_wq(2), string(x(0)>x(1))
 ;3/26/99 - also set the date, year, and hour
 s = date_from_tai($view_tai(win))
 
 xmtextfieldsetstring, $t3d_select_time_txt(0), s(0:3)
 xmtextfieldsetstring, $t3d_select_time_txt(4), s(0:3)
 xmtextfieldsetstring, $t3d_select_time_txt(1), s(5:6)
 xmtextfieldsetstring, $t3d_select_time_txt(5), s(5:6)
 xmtextfieldsetstring, $t3d_select_time_txt(2), s(8:9)
 xmtextfieldsetstring, $t3d_select_time_txt(6), s(8:9)
 xmtextfieldsetstring, $t3d_select_time_txt(3), s(11:12)+':00:00'
 endsubr
 ;===============================================================================
subr t3derasecheckbox_cb
 $t3d_select_add_flag = $radio_state
 endsubr
 ;===============================================================================
subr t3d_selectortool
 if defined($t3dselectwidget) eq 0 then {
 ;the compile_file is supposed to operate at top level and hence the edb's
 ;are temporary and don't add to our accumulation
 compile_file, getenv('ANA_WLIB') + '/trace3dselectorwidgettool.ana'
 }
 xtpopup, $t3dselectwidget
 endsubr
 ;===============================================================================
