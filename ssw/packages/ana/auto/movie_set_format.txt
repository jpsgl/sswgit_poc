proto type rules for movies sets

stored as a symbol array numbered 0 to nt + 2 where nt is the number
of times for the set

sym #		description

 0		1-D F*8 array with the nt TAI times
 1		2-D array (nt x ns) containing F*4 information arranged as
 		a stack of ns 1-D arrays, these are:
 		ns = 0:  vector of xc coordinates for center of each image
