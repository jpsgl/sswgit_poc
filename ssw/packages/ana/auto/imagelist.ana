;the imagelist routines, not just for TRACE anymore
 ;list types:
 ;	1: TRACE raw 3-D files
 ;	2: list of simple files,like a list of 2-D fz or fits files
 ;	3: La Palma thumbnails for 1999 and 2000
 ;	4: full disk MG's (aka FDMG)
 ;	5: Solar B observables
 ;	6: Solar B observables
 ;things we like to see in an image list:
 ;	time, xc, yc, pixsc
 ;===============================================================================
subr equate_i, s, i, x
 equate, eval(sprintf('%s%d',s,i)), x
 endsubr
 ;===============================================================================
subr imagelist_set_arrays_t3d, i
 ;an internal routine used by imagelist_add_item and t3d_select_delete_cb
 ty,'in imagelist_set_arrays_t3d'
 nim = num_elem($t3d_selected_image_nums)
 $image_list(1,i) = nim
 ;need to construct some arrays for the files
 equate_i, '$image_list_files', i, $t3d_selected_image_files
 equate_i, '$image_list_tops', i, $t3d_selected_tops
 equate_i, '$image_list_nums', i, $t3d_selected_image_nums
 ;might not always have a time available
 zeroifnotdefined, $t3d_selected_times, $t3d_selected_xc, $t3d_selected_yc, $t3d_selected_pixsc
 equate_i, '$image_list_times', i, $t3d_selected_times
 equate_i, '$image_list_xc', i, $t3d_selected_xc
 equate_i, '$image_list_yc', i, $t3d_selected_yc
 equate_i, '$image_list_pixsc', i, $t3d_selected_pixsc
 endsubr
 ;===============================================================================
subr imagelist_set_arrays_type2, i
 ;an internal routine used by imagelist_add_item and type2_select_delete_cb
 nim = num_elem($file_names)
 $image_list(1,i) = nim
 ;need to construct some arrays for the files
 equate_i, '$image_list_files', i, $file_names
 ;next 2 aren't used but need to be present for list operations like delete
 equate_i, '$image_list_tops', i, 0
 equate_i, '$image_list_nums', i, 0
 ;might not always have a time available
 zeroifnotdefined, $file_times, i, $file_xc, $file_yc, $file_pixsc
 equate_i, '$image_list_times', i, $file_times
 equate_i, '$image_list_xc', i, $file_xc
 equate_i, '$image_list_yc', i, $file_yc
 equate_i, '$image_list_pixsc', i, $file_pixsc
 endsubr
 ;===============================================================================
subr imagelist_set_arrays_type3, i
 ;an internal routine used by imagelist_add_item and type3_select_delete_cb
 ;the list symbols are used differently here
 ;the selected symbols are $lp_selected_paths, $lp_selected_times, and
 ;$lp_selected_items
 nim = num_elem($lp_selected_times)
 $image_list(1,i) = nim
 ;need to construct some arrays for the files
 equate_i, '$image_list_files', i, $lp_selected_paths
 ;use the tops for our items
 equate_i, '$image_list_tops', i, $lp_selected_items
 ;nums not used here but must be present
 equate_i, '$image_list_nums', i, 0
 equate_i, '$image_list_times', i, $lp_selected_times
 ;normally we don't know where the heck we are nor the scale, if we do get
 ;these data, we'll put them here
 equate_i, '$image_list_xc', i, zero(fltarr(nim))
 equate_i, '$image_list_yc', i, zero(fltarr(nim))
 equate_i, '$image_list_pixsc', i, zero(fltarr(nim))-1.0
 endsubr
 ;===============================================================================
subr imagelist_set_arrays_type4, i
 ;an internal routine used by imagelist_add_item and type4_select_delete_cb
 ;this is very much like type 2 except for the names of the arrays containing
 ;the file names, etc
 nim = num_elem($fdmg_names)
 $image_list(1,i) = nim
 ;need to construct some arrays for the files
 equate_i, '$image_list_files', i, $fdmg_names
 ;next 2 aren't used but need to be present for list operations like delete
 equate_i, '$image_list_tops', i, 0
 equate_i, '$image_list_nums', i, 0
 ;might not always have a time available
 zeroifnotdefined, $fdmg_times, i, $fdmg_xc, $fdmg_yc, $fdmg_pixsc
 equate_i, '$image_list_times', i, $fdmg_times
 equate_i, '$image_list_xc', i, $fdmg_xc
 equate_i, '$image_list_yc', i, $fdmg_yc
 equate_i, '$image_list_pixsc', i, $fdmg_pixsc
 endsubr
 ;===============================================================================
subr imagelist_set_arrays_type5, i
 ;for Solar B SOT data
 ;an internal routine used by imagelist_add_item and type5_select_delete_cb
 ;this is very much like type 2 except for the names of the arrays containing
 ;the file names, etc
 nim = num_elem($sb_names)
 $image_list(1,i) = nim
 ;need to construct some arrays for the files
 equate_i, '$image_list_files', i, $sb_names
 ;next 2 aren't used but need to be present for list operations like delete
 equate_i, '$image_list_tops', i, 0
 equate_i, '$image_list_nums', i, 0
 ;might not always have a time available
 zeroifnotdefined, $sb_times, i, $sb_xc, $sb_yc, $sb_pixsc
 equate_i, '$image_list_times', i, $sb_times
 equate_i, '$image_list_xc', i, $sb_xc
 equate_i, '$image_list_yc', i, $sb_yc
 equate_i, '$image_list_pixsc', i, $sb_pixsc
 endsubr
 ;===============================================================================
subr imagelist_set_arrays_type6, i
 ;for Solar B XRT data
 ;an internal routine used by imagelist_add_item and xrt_select_delete_cb
 ;this just like type 5 except for the names of the arrays containing
 ;the file names, etc
 nim = num_elem($xrt_names)
 $image_list(1,i) = nim
 ;need to construct some arrays for the files
 equate_i, '$image_list_files', i, $xrt_names
 ;next 2 aren't used but need to be present for list operations like delete
 equate_i, '$image_list_tops', i, 0
 equate_i, '$image_list_nums', i, 0
 ;might not always have a time available
 zeroifnotdefined, $xrt_times, i, $xrt_xc, $xrt_yc, $xrt_pixsc
 equate_i, '$image_list_times', i, $xrt_times
 equate_i, '$image_list_xc', i, $xrt_xc
 equate_i, '$image_list_yc', i, $xrt_yc
 equate_i, '$image_list_pixsc', i, $xrt_pixsc
 endsubr
 ;===============================================================================
subr imagelist_set_arrays_type7, i
 ;for SDO data
 ;an internal routine used by imagelist_add_item and sdo_select_delete_cb
 ;this just like type 5 except for the names of the arrays containing
 ;the file names, etc
 nim = num_elem($sdo_names)
 $image_list(1,i) = nim
 ;need to construct some arrays for the files
 equate_i, '$image_list_files', i, $sdo_names
 ;next 2 aren't used but need to be present for list operations like delete
 equate_i, '$image_list_tops', i, 0
 equate_i, '$image_list_nums', i, 0
 ;might not always have a time available
 ;for early SDO, fake the position and scale
 
 zeroifnotdefined, $sdo_times, i, $sdo_xc, $sdo_yc, $sdo_pixsc
 equate_i, '$image_list_times', i, $sdo_times
 equate_i, '$image_list_xc', i, $sdo_xc
 equate_i, '$image_list_yc', i, $sdo_yc
 equate_i, '$image_list_pixsc', i, $sdo_pixsc
 endsubr
 ;===============================================================================
subr imagelist_add_item, type, tag
 ;this adds an image list, not an image, tag will appear in the list widget
 ;type 1 is a list from a trace selection from hourly files
 image_list_wq
 ;put in list widget and also keep
 n = xmlistcount($imagelistwidget)
 ;;;ty,'current list count =', n
 i = $image_list_count
 $image_list(0,i) = type
 $image_list_tags(i) = tag
 if type eq 1 then {
  if defined($t3d_selected_image_nums) eq 0 then return
  $t3dselected_list_of_lists = i		;a backlook
  imagelist_set_arrays_t3d, i		;sets the arrays for TRACE 3-D lists
 }
 if type eq 2 then {
  ;type 2 is a list of "simple" files
  imagelist_set_arrays_type2, i
 }
 if type eq 3 then {
  ;type 3 is a list of La Palma thumbnails
  $lpselected_list_of_lists = i		;a backlook
  imagelist_set_arrays_type3, i
 }
 if type eq 4 then {
  ;type 4 is a list of MDI FDMG's
  $fdmgselected_list_of_lists = i		;a backlook
  imagelist_set_arrays_type4, i
 }
  if type eq 5 then {
  ;type 5 is a list of Solar B SOT observables
  $sbselected_list_of_lists = i		;a backlook
  imagelist_set_arrays_type5, i
 }
  if type eq 6 then {
  ;type 6 is a list of Solar B XRT observables
  $xrtselected_list_of_lists = i		;a backlook
  imagelist_set_arrays_type6, i
 }
  if type eq 7 then {
  ;type 7 is a list of SDO observables
  $sdoselected_list_of_lists = i		;a backlook
  imagelist_set_arrays_type7, i
 }
sq = sprintf('%4d   %-25s %5d', type, tag, $image_list(1,i))
 xmlistadditem, $imagelistwidget, sq
 $image_list_count += 1
 xmlistselect, $imagelistwidget, $image_list_count, 1
 $list_selected = $image_list_count
 endsubr
 ;===============================================================================
subr imagelist_reload_list
 ;reload the entire list, done if list is modified or just read in
 xmlistdeleteall, $imagelistwidget
 for id = 0, $image_list_count - 1 do {
 sq = sprintf('%4d   %-25s %5d', $image_list(0,id), $image_list_tags(id), -
 	$image_list(1,id))
 xmlistadditem, $imagelistwidget, sq
 }
 endsubr
 ;=======================================================================
subr imagelist_deleteall_cb
 ;remove all the lists from the list of lists
 $image_list_count = 0
 xmlistdeleteall, $imagelistwidget
 endsubr
 ;===============================================================================
subr exchange_ip, s, i
 switch, eval(sprintf('%s%d',s,i)), eval(sprintf('%s%d',s,i+1))
 endsubr
 ;===============================================================================
subr imagelist_delete_cb
 ;remove selected list from the list of lists
 ;this is harder than deleting them all
 ;modified 4/4/2000, use exchange_ip
 ;the selected item must be in range or just no action
 n = xmlistcount($imagelistwidget)
 if $list_selected le 0 or $list_selected gt n then return
 if n ne $image_list_count then {
 	ty,'internal problem in image list list'  return }
 xmlistdeleteitem, $imagelistwidget, $list_selected
 $image_list_count = $image_list_count -1
 if n gt 1 then {
   for i = $list_selected - 1, $image_list_count - 1 do {
       $image_list(0, i) = $image_list(*, i+1)
       $image_list_tags(i) = $image_list_tags(i+1)
       if defined(eval(sprintf('$image_list_files%d',i+1))) then {
	;do a series of switches, leaves deleted one at end
	exchange_ip, '$image_list_files', i
	exchange_ip, '$image_list_tops', i
	exchange_ip, '$image_list_nums', i
	exchange_ip, '$image_list_xc', i
	exchange_ip, '$image_list_yc', i
	exchange_ip, '$image_list_pixsc', i
	}
       }
   }
 imagelist_reload_list
 endsubr
 ;=======================================================================
subr imagelist_save_cb
 ty,'imagelist_save_cb called'
 ;get and open the save file
 name = xmtextfieldgetstring($imagelistsavefname)
 if filesize(name) gt 0 then {
    sq = sprintf('File %s already exists\nAre you sure you want to overwrite it?',name)
   xmprompt,sq,'N','prompt_cb',1,$f7,'',200,200
   xtloop, 1
   ;we get here after the prompt_cb lets us (it sets !motif = 0)
   !motif = 1
   if reply_was_not_yes() then return
 }
 lun = get_lun()
 if lun lt 0 then { errormess,'no available LUN for file write!' return }
 stat = openw(lun, name)
 if stat ne 1 then {
 	errormess,sprintf('can''t write to file: %s\ncheck name and permissions\n',name)
 	close, lun
	return }

 i = $list_selected - 1
 type = $image_list(0,i)
 nim = $image_list(1,i)
 if (type eq 1) then {
  ;TRACE raw hourly list require a filename and a # in the file
  selected_tops = eval(sprintf('$image_list_tops%d',i))
  selected_image_files = eval(sprintf('$image_list_files%d',i))
  selected_image_nums = eval(sprintf('$image_list_nums%d',i))
  nfiles = num_elem(selected_image_files)
  nselected = num_elem(selected_image_nums)
  nf = 0
  k = 0
  current_top = -1
  for i= 0, nim - 1 do {
  if i gt current_top then {
   ;need a new file
   fname = selected_image_files(nf)
   ;;ty,'new file = ', fname
   current_top = selected_tops(nf)
   fname = selected_image_files(nf)
   nf += 1
  }
  j = selected_image_nums(i)
  ;now have file name in fname and image # in j
  printf,lun,sprintf('%s %d', fname, j)
  }
 }
 ;need to do something with types 5 and 6
 if (type eq 2 or type eq 5 or type eq 6 or type eq 7) then {
  ;these just require a file name
  for j= 0, nim - 1 do {
  printf,lun, subsc(eval(sprintf('$image_list_files%d',i)), j)
  }
 }
 close, lun
 endsubr
 ;=======================================================================
subr file_type_ok_cb
 ty,'OK hit, $file_list_type =', $file_list_type
 !motif =0
 endsubr
 ;=======================================================================
subr file_type_cb
 if $radio_state then { $file_list_type = $radio_button + 1
 ty,'$file_list_type =', $file_list_type }
 endsubr
 ;=======================================================================
subr filelist_type_widget
 if defined($filelist_type_widget) eq 0 then {
  wq = xmtoplevel_board(0,0, 'file list type',10,10)
  xmposition, xmlabel(wq, 'please specify list type\nbelow and hit OK', $f4), 0, 0
  rb = xmradiobox(wq,'file_type_cb',$f4,'','TRACE raw list','ordinary image files')
  xmposition, rb(0), 0, 45
  for i=1,num_elem(rb)-1 do xmselectcolor, rb(i),'red'
  $file_list_type = 1
  xmposition,xmbutton(wq,'OK','file_type_ok_cb',$f4,'red'), 60, 115, 60, 40
  $filelist_type_widget = wq
  }
 xtpopup, $filelist_type_widget
 endsubr
 ;=======================================================================
subr imagelist_load_cb
 ;this needs work, doesn't restore the ancillary info at present
 ty,'imagelist_load_cb called'
 name = xmtextfieldgetstring($imagelistsavefname)
 ;find out what kind
 ;get lines in the list, assume one line per image
 nselected = linecountsanscomments(name)
 if nselected le 0 then {
 	errormess,'list file has no entries\nor we can''t open it\n'+name
 	return }
 filelist_type_widget
 xtloop,1	;forces a wait here until user answers the question
 !motif = 1
 if defined($filelist_type_widget) then xtpopdown, $filelist_type_widget
 if $file_list_type eq 1 then {
   tag = name
   nfiles = 0		;and the list of files
   lun = get_lun()
   if lun lt 0 then { errormess,'no available LUN for file read!' return }
   stat = openr(lun, name)
   if stat ne 1 then {
     errormess,sprintf('can''t write to file: %s\ncheck name and permissions\n',name)
     close, lun  return }
   nc = 0
   last_name = ''
   $t3d_selected_image_nums = intarr(nselected)
   s = ''
   d, $t3d_selected_tops
   while readf(lun, s) eq 1 do {
   ;want this line?
    s2 = strtrim(s)
    if num_elem(s2) gt 0 then {
     ;check for comment
     sq = s2(0)
     if strpos(sq,'#%!;') lt 0 then  {
     ;looks OK so far, parse the file and count
     name_3d = strtok(s,' ')
     ;ty,'file name = ', name_3d
     fnum = strtok('',' ')
     ;ty, 'file number =', fnum
     $t3d_selected_image_nums(nc) = fix(fnum)
     nc = nc + 1
     if nc gt nselected then {
       erromess,'problem reading list file\n'+name
       break }
     nq = nc - 2
     if nfiles then {
       ;but only if a new name
       if name_3d ne last_name then {
       $t3d_selected_image_files = [$t3d_selected_image_files, name_3d]
       $t3d_selected_tops = [$t3d_selected_tops, nq]
       last_name = name_3d
       nfiles += 1
       }
      } else {
       $t3d_selected_tops = [nq]
       $t3d_selected_image_files = [name_3d]
       last_name = name_3d
       nfiles += 1
     }
     }
   }
   }
   ;5/5/99 - fix bug when only one file
   if dimen($t3d_selected_tops,0) gt 1 then
 	  $t3d_selected_tops = [$t3d_selected_tops(1:*), nselected-1] else
 	  $t3d_selected_tops = [nselected-1]
   close, lun
   imagelist_add_item, $file_list_type, tag
   ;to finish the job, call imagelist_sendlist_cb (note $list_selected set in
   ;imagelist_add_item)
   imagelist_sendlist_cb
 } else
 if $file_list_type eq 2 then {
   ;an ordinary list, implement by just calling filelist_widget. This has the side
   ;effect of loading the image list widget and destroying a previous one. May want
   ;to allow other options in the future
   iq = filelist_widget(name)
   return
 } else { ty,'illegal list type, internal error?' close, lun   return }
 endsubr
 ;=======================================================================
subr imagelist_sendlist_cb
 ;regenerate the list and send to the selection list
 ;presently only for TRACE and Solar B data, some of the TRACE data stuff should be in the load
 ;operation instead of in here (?)
 i = $list_selected - 1
 type = $image_list(0,i)
 nim = $image_list(1,i)
 if (type eq 1) then {
 t3d_selected_list_wq
 xmsettitle, $t3selectedwidget, 'Selected TRACE Image List: '+$image_list_tags(i)
 ;clear the TRACE selected list and re-create the arrays needed
 t3dselect_eraseall_cb
 $t3d_selected_tops = eval(sprintf('$image_list_tops%d',i))
 $t3d_selected_image_files = eval(sprintf('$image_list_files%d',i))
 $t3d_selected_image_nums = eval(sprintf('$image_list_nums%d',i))
 $t3d_selected_times = eval(sprintf('$image_list_times%d',i))
 $t3d_selected_xc = eval(sprintf('$image_list_xc%d',i))
 $t3d_selected_yc = eval(sprintf('$image_list_yc%d',i))
 $t3d_selected_pixsc = eval(sprintf('$image_list_pixsc%d',i))
 ;if the times are bad (perhaps not read in) try to restore them, also
 ;assume we need to restore xc, etc
 if isscalar($t3d_selected_times) then {
 	time_restore_flag=1  il = i
	} else time_restore_flag=0
 nfiles = num_elem($t3d_selected_image_files)
 nselected = num_elem($t3d_selected_image_nums)
 if time_restore_flag then {
 	$t3d_selected_times = dblarr(nselected)
 	$t3d_selected_xc = fltarr(nselected)
 	$t3d_selected_yc = fltarr(nselected)
 	$t3d_selected_pixsc = fltarr(nselected)
   }
 ;try to step through with some efficiency, we have to open hourly files
 ;as necessary and read one liners for all the images
 nf = 0
 k = 0
 current_top = -1
; NOTE: before, used <i> for loop variable here, but this interferes
; with the previous use of <i> as the list selection item, which is
; again required later.  Instead, we use <k> for loop variable.  LS 24apr00
 for k= 0, nim - 1 do {
 if k gt current_top then {
  ;need a new file, might be next one but not always
  ;because the list may have been edited
  while k gt current_top do {
  if nf ge num_elem($t3d_selected_image_files) then {
  	errormess,'internal error\nin list handling'  return }
  fname = $t3d_selected_image_files(nf)
  current_top = $t3d_selected_tops(nf)
  nf += 1	;for next file
  }
  if defined($t3d_file_name) eq 0 then $t3d_file_name = ''
  if  $t3d_file_name ne fname then {
   $t3d_file_name = fname
   if t3d_open_file() ne 1 then errormess,'problem opening\nTRACE file'
  }
 }
 j = $t3d_selected_image_nums(k)
 ;ty,'nf, k, j, current_top =', nf, k, j, current_top
 if j ge dimen($t3d_dp_header, 1) then {
   errormess,sprintf('image # %d is outside/nrange in file\n%s',j,fname)
   sq = 'no such image available'
   } else {
 xmtextfieldsetstring, $t3d_image, string(j)
 sq = sprintf('%3d %3d ',k,j) + trace_oneliner(wmap($t3d_dp_header(*,j)), 1)
 if time_restore_flag then {
 	$t3d_selected_times(k) = $trace_tai
	;to get the positions is much more involved
	trace_get_image_positions, wmap($t3d_dp_header(*,j)), xc, yc, pixsc
	$t3d_selected_xc(k) = xc
	$t3d_selected_yc(k) = yc
	$t3d_selected_pixsc(k) = pixsc
	}
 }
 xmlistadditem, $t3selectedwidget, sq
 }
 ;if restoring times then
 if time_restore_flag then {
  equate_i, '$image_list_times', i, $t3d_selected_times
  equate_i, '$image_list_xc', i, $t3d_selected_xc
  equate_i, '$image_list_yc', i, $t3d_selected_yc
  equate_i, '$image_list_pixsc', i, $t3d_selected_pixsc
 }
 xmsetlabel, $t3select_lab1, sprintf('%d files, %d images',nfiles, nselected)
 }
 if (type eq 2 or type eq 5) then {
  ;the type 2's are easier because everything we put in the list widget should
  ;already be in our arrays, don't have to re-scan as with the TRACE list
  $file_names = eval(sprintf('$image_list_files%d',i))
  $file_times = eval(sprintf('$image_list_times%d',i))
  $file_xc = eval(sprintf('$image_list_xc%d',i))
  $file_yc = eval(sprintf('$image_list_yc%d',i))
  $file_pixsc = eval(sprintf('$image_list_pixsc%d',i))
  ;and just re-make the list, note that the original name of the list file is in the
  ;tag, don't really need it anymore (I think) but it can be retrieved
  iq = filelist_widget('')
 }
 if (type eq 3) then {
  ;already be in our arrays, don't have to re-scan as with the TRACE list
  $lp_selected_paths = eval(sprintf('$image_list_files%d',i))
  $lp_selected_times = eval(sprintf('$image_list_times%d',i))
  $lp_selected_items = eval(sprintf('$image_list_tops%d',i))
  ;and just re-make the list, note that the original name of the list file is in the
  ;tag, don't really need it anymore (I think) but it can be retrieved
  lp_selected_list_reload
 }
 endsubr
 ;=======================================================================
subr imagelist_sendmovie, i, type, nim
 ;regenerate the list and send to the movie widget
 ;if we have the times and positions for type 2 and 3, flag it. This will
 ;allow manipulations to pass through if/when we do that
 xtpopup, mvget_widget()	;define/popup movie creator
 $mv_label = $image_list_tags(i)
 ty,'$mv_label from imagelist_sendmovie =', $mv_label
 xmsetoptionselection, $mvget_sop(0), $mvget_sop($option_value+1)
 mvget_source_cb
 ;always set the deltas to 1
 xmtextfieldsetstring, $mvcubetext(3), '1'
 xmtextfieldsetstring, $mvcubetext(4), '1'
 xmtextfieldsetstring, $mvcubetext(5), '1'
 xmtextfieldsetstring, $mvgettext3, '0'
 xmtextfieldsetstring, $mvgettext4, string(nim-1)
 mv_setsizes, 0, 0, nim, 1, 1, 1
 if type eq 1 then {
   ;TRACE case
   xmtextfieldsetstring, $mvgettext6, $image_list_entry_text
   $mvget_trace_list_id = i
   $mv_have_time = 0
   $mv_have_xy = 0
   return
 }
 $mv_have_time = 1
 $mv_have_xy = 1
 $mvget_times = eval(sprintf('$image_list_times%d',i))
 $mvget_xc = eval(sprintf('$image_list_xc%d',i))
 $mvget_yc = eval(sprintf('$image_list_yc%d',i))
 $mvget_pixsc = eval(sprintf('$image_list_pixsc%d',i))
 if type eq 2 or type eq 4 or type eq 5  or type eq 6   or type eq 7 then {
   xmtextfieldsetstring, $mvgettext7, $image_list_tags(i)
   $mvget_files = eval(sprintf('$image_list_files%d',i))
   return
  }
 ;note that type 3 are La Palma thumbnails, these have a specific file name pattern
 if type eq 3 then {
   xmtextfieldsetstring, $mvgettext7, $image_list_tags(i)
   $mvget_files = strarr(nim)
   items = eval(sprintf('$image_list_tops%d',i))
   paths = eval(sprintf('$image_list_files%d',i))
   for k=0,nim-1 do {
    j = items(0,k)
    $mvget_files(k) = paths(j) + fns('im######.jpg', items(1,k))
    }
  }
 endsubr
 ;=======================================================================
subr imagelist_send_cb, target
 ;regenerate the list and send to a target widget, target #'s start with 1
 ;if we have the times and positions for type 2 and 3, flag it. This will
 ;allow manipulations to pass through if/when we do that
 ;and fake an option menu call
 ty,'in imagelist_send_cb, target =', target
 i = $list_selected - 1
 type = $image_list(0,i)
 nim = $image_list(1,i)
 ;note that list types and movie types aren't the same except for 1 and 2
 if type eq 3 then mtype = 2 else mtype = type
 if type eq 4 then { mtype = 2 }  ;adding type 4
 if type eq 5 then { mtype = 2 }  ;adding type 5
 if type eq 6 then { mtype = 2 }  ;adding type 6
 if type eq 7 then { mtype = 2 }  ;adding type 7
 ;and fake an option menu call
 $option_value = mtype

 ncase (target-1)
   imagelist_sendmovie, i, type, nim
   imagelist_sendlight, i, type, nim
   imagelist_sendmvstat, i, type, nim
   imagelist_sendmagcurve, i, type, nim
   imagelist_process, i, type, nim
 endcase
 endsubr
 ;=======================================================================
subr imagelist_process, i, type, nim
 ;regenerate the list and send to the process widget
 process_widget
 xmsetoptionselection, $process_sop(0), $process_sop($option_value+1)
 light_source_cb
 xmtextfieldsetstring, $processtext3, '0'
 xmtextfieldsetstring, $processtext4, string(nim-1)
 ;also set the deltas to 1
 xmtextfieldsetstring, $processtext5, '1'
 if type eq 1 then {
   xmtextfieldsetstring, $processtext0, $image_list_entry_text
   $light_trace_list_id = i
 } else
   if type eq 2 or type eq 5 or type eq 6 then {
     xmtextfieldsetstring, $processtext2, $image_list_tags(i)
     $process_files = eval(sprintf('$image_list_files%d',i))
   }
 endsubr
 ;=======================================================================
subr imagelist_sendmvstat, i, type, nim
 ;regenerate the list and send to the movie stats widget
 xtpopup, mvstats_widget()
 xmsetoptionselection, $mvstat_sop(0), $mvstat_sop($option_value+1)
 mvstat_source_cb
 ;load the text with the list ID line
 xmtextfieldsetstring, $mvstattext3, '0'
 xmtextfieldsetstring, $mvstattext4, string(nim-1)
 ;also set the deltas to 1
 xmtextfieldsetstring, $mvstattext5, '1'
 if type eq 1 then {
  xmtextfieldsetstring, $mvstattext0, $image_list_entry_text
  $mvstat_trace_list_id = i
  return
 }
 if type eq 2 or type eq 5 or type eq 6 or type eq 7 then {
   xmtextfieldsetstring, $mvstattext10, $image_list_tags(i)
   $mvstat_files = eval(sprintf('$image_list_files%d',i))
  return
 }
 ;note that type 3 are La Palma thumbnails, these have a specific file name pattern
 if type eq 3 then {
   xmtextfieldsetstring, $mvstattext10, $image_list_tags(i)
   $mvstat_files = strarr(nim)
   items = eval(sprintf('$image_list_tops%d',i))
   paths = eval(sprintf('$image_list_files%d',i))
   for k=0,nim-1 do {
    j = items(0,k)
    $mvstat_files(k) = paths(j) + fns('im######.jpg', items(1,k))
    }
 }
 endsubr
 ;=======================================================================
subr imagelist_sendlight, i, type, nim
 ;regenerate the list and send to the light curve widget
 xtpopup, lightcurve_widget()
 xmsetoptionselection, $light_sop(0), $light_sop($option_value+1)
 light_source_cb
 xmtextfieldsetstring, $lighttext3, '0'
 xmtextfieldsetstring, $lighttext4, string(nim-1)
 ;also set the deltas to 1
 xmtextfieldsetstring, $lighttext5, '1'
 if type eq 1 then {
  xmtextfieldsetstring, $lighttext0, $image_list_entry_text
  $light_trace_list_id = i
 } else
 if type eq 2 or type eq 5 or type eq 6 or type eq 7 then {
   xmtextfieldsetstring, $lighttext10, $image_list_tags(i)
   $light_files = eval(sprintf('$image_list_files%d',i))
  }
 ;note that type 3 are La Palma thumbnails, these have a specific file name pattern
 if type eq 3 then {
   xmtextfieldsetstring, $mvstattext10, $image_list_tags(i)
   $light_files = strarr(nim)
   items = eval(sprintf('$image_list_tops%d',i))
   paths = eval(sprintf('$image_list_files%d',i))
   for k=0,nim-1 do {
    j = items(0,k)
    $light_files(k) = paths(j) + fns('im######.jpg', items(1,k))
    }
 }
 endsubr
 ;=======================================================================
subr imagelist_sendmagcurve, i, type, nim
 ;regenerate the list and send to the mag curve widget
 xtpopup, magcurve_widget()	;define/popup movie creator
 xmsetoptionselection, $magcurve_sop(0), $magcurve_sop($option_value+1)
 magcurve_source_cb
 xmtextfieldsetstring, $magcurvetext3, '0'
 xmtextfieldsetstring, $magcurvetext4, string(nim-1)
 ;also set the deltas to 1
 xmtextfieldsetstring, $magcurvetext5, '1'
 if type eq 1 then {
  ;note - it doesn't make much sense to use TRACE data for B fields
  xmtextfieldsetstring, $magcurvetext0, $image_list_entry_text
  $magcurve_trace_list_id = i
 } else
 if type eq 2 or type eq 5 or type eq 6 or type eq 7 then {
   xmtextfieldsetstring, $magcurvetext10, $image_list_tags(i)
   $magcurve_files = eval(sprintf('$image_list_files%d',i))
  }
 ;note that type 3 are La Palma thumbnails, these have a specific file name pattern
 if type eq 3 then {
   xmtextfieldsetstring, $mvstattext10, $image_list_tags(i)
   $magcurve_files = strarr(nim)
   items = eval(sprintf('$image_list_tops%d',i))
   paths = eval(sprintf('$image_list_files%d',i))
   for k=0,nim-1 do {
    j = items(0,k)
    $magcurve_files(k) = paths(j) + fns('im######.jpg', items(1,k))
    }
 }
 endsubr
 ;===============================================================================
subr image_list_wq
 ;2/3/2008 - converted to use compile_file and top level
 if defined($imagelistwidget) eq 0 then {
   compile_file, getenv('ANA_WLIB') + '/imagelistwidgettool.ana'
 }
 xtpopup, $imagelistwidget }
 endsubr
 ;===============================================================================
subr imagelist_entry_cb
 ;list entry callback
 iq = $list_item_position - 1
 ty,'selected list #', iq
 $list_selected = $list_item_position
 $image_list_entry_text = $list_item	;save the line text
 endsubr
 ;===============================================================================
subr filelist_entry_cb
 ;list entry callback
 iq = $list_item_position - 1
 ty,'selected list #', iq
 ;now the action, currently read and display the file
 ;since there is only one file list, we just get from $file_names
 $selected_file = $file_names(iq)
 read_file, $current_window
 endsubr
 ;===============================================================================
subr filelist_removepath_cb
 ;note that flag is complement of checkbox status
 if $radio_state then $filelist_remove_path = 0 else $filelist_remove_path = 1
 ;also, remake the list, the null file name means use the current $file_names
 iq = filelist_widget('')
 endsubr
 ;===============================================================================
subr filelist_showtimes_cb
 $filelist_show_times = $radio_state
 ;remake the list, the null file name means use the current $file_names
 iq = filelist_widget('')
 endsubr
 ;===============================================================================
subr filelist_cp_list_cb
 ;copy the list items to a file, use the current $file_names
 if defined($filelist_items) eq 0 then {
   errormess, 'no list items?'
   return }
 nsource = num_elem($filelist_items)
 if nsource le 0 then {
   errormess,'error in list items'
   return }
 name = xmtextfieldgetstring($filelist_copy_file_text)
 lun = get_lun()
 if openw(lun, name) ne 1 then {
  close, lun
  errormess,'can''t open file\nfor list results'
  return }
 
 for i=0, nsource-1 do printf, lun, $filelist_items(i)
 close, lun
 endsubr
 ;===============================================================================
subr panic_stop_cb
 ty,'panic stop'
 $panic_stop = 1
 endsubr
 ;===============================================================================
func scan_busy_widget(iq)
 ;warn that we are saving a file
 if defined($scan_busy_widget) eq 0 then {
 $scan_busy_widget = xmdialog_board(0,0,0, 'BUSY',0,0,10,10)
 sq = 'scanning your files\nplease stand by\nif a problem, hit STOP!'
 iq = xmlabel($scan_busy_widget, sq, $f4)
 $scan_busy_label = xmlabel($scan_busy_widget, 'working on XXXXX/XXXXX', $f4)
 xmposition, iq, 20, 10
 xmposition, $scan_busy_label, 15, 60
 bq=xmbutton($scan_busy_widget,'STOP!','panic_stop_cb', $f4,'red')
 xmposition, bq, 50, 85, 80, 30
 }
 $panic_stop = 0
 return, $scan_busy_widget
 endfunc
 ;===============================================================================
func filelist_widget(fname)
 ;give the name of a file containing a list of files (a file list), creates
 ;an internal list of the names and a widget to access them. Also registers
 ;this list with the list of lists widget. Returns the number of file names
 ;found, 0 if something amiss
 ty,'fname = ', fname
 
 ;use a null file name to specify a re-load of the list widget using the
 ;present $file_names, this is done to change how the list is presented; i.e.,
 ;with or without path, times, etc
 
 if num_elem(fname) gt 0 then {
 ;this is not very efficient but OK for modest lists
 nsource = linecountsanscomments(fname)
 ty,'nsource = ', nsource
 if nsource le 0 then {
   errormess,sprintf('list file\n%s not\nreadable', fname) return }
 lun = get_lun()
 if lun lt 0 then { errormess,'no available LUN for file read!' return }
 openr, lun, fname
 $file_names = strarr(nsource)
 s = ''
 nreadable = 0
 ;being optimistic, setup arrays for times and positions
 $file_times = dblarr(nsource)
 $file_xc = fltarr(nsource)
 $file_yc = fltarr(nsource)
 $file_pixsc = fltarr(nsource)
 zero, $file_times, $file_xc, $file_yc, $file_pixsc

 ty,'scanning files'
 ;now pop the busy widget
 popbusy, 1
 wait, .2
 xtloop, 2
 for i=0, nsource-1 do {
  xmsetlabel, $scan_busy_label, sprintf('working on %d/%d', i, nsource)
  xtloop, 0
  if $panic_stop then {
    errormess, sprintf('panic stop, count = %d\nout of %d',i,nsource)
    close, lun	break }
  if readf(lun,s) ne 1 then {
    errormess,sprintf('problem while reading\nlist file %s', fname)
    close, lun  return }
  ;pass over a few common problems
  s = strtrim(s)
  if s eq './' or s eq '../' then s = ''
  if num_elem(s) gt 0 then {
  $file_names(i) = s
  ;and try to get times and such
  iq = data_type(s, header, params)
  if iq ge 0 then nreadable +=1
  ;only try for times, etc if a FITS file, could extend to others?
  if iq eq 1 then {
    fits_time_decode, header
    $file_times(i) = $data_time_tai
    $file_xc(i) = $data_xcen
    $file_yc(i) = $data_ycen
    $file_pixsc(i) = $data_xscale
  }
  }
 }
 ty,'done scanning files'
 !motif = 1
 if defined($scan_busy_widget) eq 1 then xtunmanage, $scan_busy_widget

 close, lun
 if nreadable eq 0 then {
   errormess, sprintf('Unfortunately, none of the files\nin your list (%s)\nare readable!', fname)
   return, 0
  }
 if nreadable ne nsource then {
   errormess, sprintf('%d files in your list of %d\n were not read',nsource-nreadable,nsource)
   }
 ;if some are unreadable, should we drop them from list? needs work
 ;now put in our list of lists, the tag is the file name of the list file
 imagelist_add_item, 2, fname
 }
 ;now create the list widget if it doesn't exist, otherwise we overwrite the
 ;previous -- so only one at a time

 if defined($filelistwidget) ne 1 then {
  compile_file, getenv('ANA_WLIB') + '/filelistwidgettool.ana'
  }
 ;some of the operations below can change the size, so save it here and then
 ;re-apply it
 xmgetwidgetsize, $filelistshell, dx, dy
 ;also the position
 xmgetwidgetposition, $filelistshell, xp, yp

 ;delete the old stuff (if any)
 ;loop over all files and put in list
 t1 = !systime
 ;pop down so that filling list takes less time
 xtpopdown, $filelistwidget
 xmlistdeleteall, $filelistwidget
 nsource = num_elem($file_names)
 ;also make a string array of all the items
 $filelist_items = strarr(nsource)
 ;;ty,'mark 1'
 ;;ty,'nsource =', nsource
 for i=0, nsource-1 do {
  sq = $file_names(i)
  if $filelist_remove_path then sq = removepath(sq)
  if $filelist_show_times then {
    if $file_times(i) gt 0 then st = date_from_tai($file_times(i),0,1) else
    	st = 'no time available'
    st = strreplace(st,'\n','  ')
    st = strtok(st,'U')
    sq = sprintf('%s   %s @(%3.1f, %3.1f) %4.2f"/pixel',sq, -
    	st, $file_xc(i), $file_yc(i),$file_pixsc(i))
    }
  xmlistadditem, $filelistwidget, sq
  $filelist_items(i) = sq
 }
 t2 = !systime
 ty,'time to load list =', t2 - t1
 xmposition, $filelistshell, xp, yp, dx, dy
 xtpopup, $filelistwidget
 
 iq = nsource
 return, iq
 endfunc
 ;===============================================================================
