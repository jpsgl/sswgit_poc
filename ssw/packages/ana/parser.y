/* sequence parser */
/* 1/28/98 - modified to work with genload */
%{
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/dir.h>
#include <sys/stat.h>
#include <stdlib.h>
#include "parser.h"
#define NTEMPS 8
/* 7/9/97 - first temp changed to 248 from 240 */
#define FIRST_TEMP 248
#define MAX_COND_NEST 32
#define MAX_ARGS 128
#define EXECUTE_LIST_REG 100
#define EXECUTE_LIST_SUBROUTINE_LO 0
#define EXECUTE_LIST_SUBROUTINE_HI 0x8000
#define FIRST_FRAME_LIST_REG 105
#define FIRST_TARGET_LIST_REG 125
#define ABS(x) ((x)>=0?(x):-(x))
#define	MIN(a,b) (((a)<(b))?(a):(b))
#define	MAX(a,b) (((a)>(b))?(a):(b))
#define SUCCESS_RETURN	1
#define ERROR_RETURN	0
 extern	FILE  *yyin, *yyout;
 FILE	*flst;
 char	*progname;
 char	*sequence_top_path = "/tsw/obs_dev/";
 char	*sequence_suffix = ".useq";
 char	*frame_suffix = ".ufrm";
 Symbol	*symlist = 0;  /* linked symbol list */
 Symbol	*lablist = 0;  /* linked label list, a separate list */
 /* lists below are also in symlist */
 Symbol	*seq_list = 0;  /* linked label list for used sequence names */
 Symbol	*target_list = 0;  /* linked label list for used target names */
 Symbol	*frame_list = 0;  /* linked label list for used frame names */
 Symbol	*list_of_list = 0;  /* linked label list for frame and target lists */
 ListArray *listlist = 0; /* linked list for lists */
 typedef unsigned char byte;
 byte	temp_usage[NTEMPS];
 int	highest_temp_used = 0, iq, jq, cond_end_label, next_label = 0;
 int	serious_errors = 0;
 int	list_reg, list_type, list_max, list_log;
 int	echo_flag, main_type_flag, sid = -1, version_input_flag = 0;
 int	input_symbol_type = 1;	/* used when reading in lists of symbols */
 int	fatal_flag = 0, search_flag = 0;
 extern	int	lineno;
 enum edbtypes { ATOM, ADD, SUB, MUL, DIV, EQ_OP, NE_OP, GT_OP, GE_OP,
	 LT_OP, LE_OP, AND_OP, OR_OP, PRE_INC, POST_INC, PRE_DEC, POST_DEC };
 int temp_err(), get_next_temp();
 char	formstring[80], lstring[16];
 int	break_stack[MAX_COND_NEST], break_index = 0;
 int	s_stack[MAX_COND_NEST], s_index = 0;
 int	b_stack[MAX_COND_NEST], b_index = 0;
 int	arg_stack[MAX_ARGS], arg_index = 0;
 char	*name_stack[MAX_ARGS];
 int	list_buf[MAX_ARGS], list_buf_index = 0;
 int	version_number = 0;
 
 expdb	zatom = { 0,0, 1,0, ATOM};
 expdb	uatom = { 1,0, 1,0, ATOM};
%}
%union	{			/* stack type */
 double	dval;
 int	ival;
 expdb	edb;
 char	*sym;
 ListArray	*listptr;
	}
%token	<ival>	NUMBER GLOBAL LOCAL IF ELSE WHILE BREAK RET FOR NOT
%token	<ival>	GOTO SUB0ARGS SUB1ARGS SUB2ARGS SUB3ARGS RWBIV ROBIV
%token	<ival>	FRAMELIST TARGETLIST EXECUTELIST BITMASK CALL FRAME TARGET
%token	<sym>	SYMBOL LIST SAFEFRAME CONTEXTFRAME
%type	<ival>	reg literal if else cond ifelse while asgn whilecond alias ifstmt
%type	<ival>	label goto arglist setlist bitmasklist for1 for2 for3 listload
%type	<ival>	argelem dload
%type	<listptr>	listinit
%type	<edb>	expr dinc forexpr

%right	'='
%left	OR
%left	AND
%left	GT GE LT LE EQ NE
%left	'-' '+'
%left	'*' '/' '%'
%right	PP MM
%nonassoc	UNARYMINUS NOT

%%
stmtlist:  /* nothing */
	| stmtlist '\n' { /* fprintf(yyout,";line %d\n", lineno+1);*/ }
	| stmtlist stmt
	| stmtlist label
	;
stmt:	  asgn ';'
	| dinc ';' { dinc_eval(&$1); }
	| alias
	| dload
	| listload
	| CALL '(' SYMBOL ')' ';' { call_subr($3);  free($3); }
	| CALL '('  expr ',' expr ')' ';' { gen_subr2(CALL2, &$3, &$5); }
	| TARGET '(' SYMBOL ')' ';' { target_subr($3); }
	| TARGET '(' reg ')' ';' { target_subr_reg($3); }
	| FRAME '(' SYMBOL ')' ';' { frame_subr($3); }
	| FRAME '(' reg ')' ';' { frame_subr_reg($3); }
	| SAFEFRAME '(' SYMBOL ')' ';' { frame_subr($3); call_ccdsafe(); }
	| SAFEFRAME '(' reg ')' ';' { frame_subr_reg($3); call_ccdsafe(); }
	| CONTEXTFRAME '(' SYMBOL ')' ';' { contextframe1(); frame_subr($3);
		call_ccdsafe();  contextframe2(); }
	| CONTEXTFRAME '(' reg ')' ';' { contextframe1(); frame_subr_reg($3);
		call_ccdsafe();  contextframe2(); }
	| SUB0ARGS ';'  { gen_subr0($1); }
	| SUB1ARGS '(' expr ')' ';'
		{ gen_subr1($1, &$3); }
	| SUB2ARGS '(' expr ',' expr ')' ';'
		{ gen_subr2($1, &$3, &$5); }
	| SUB3ARGS '(' expr ',' expr ',' expr ')' ';'
		{ gen_subr3($1, &$3, &$5, &$7); }
	| setlist '(' arglist ')' ';'
		{ list_finish($3, list_reg, list_max); }
	| EXECUTELIST '(' expr ',' expr bitmasklist ')' ';'
		{ execute_list(&$3, &$5, $6); }
	| ifstmt { fprintf(yyout,"L%d:\n", $1);}
	| ifelse stmt { fprintf(yyout,"L%d:\n", $1);}
	| break ';'
	| goto ';'
	| whilecond stmt { fprintf(yyout,"\tUBR\t&L%d\n", $1);
	  fprintf(yyout,"L%d:\n",break_stack[break_index-1] );
	  pop_break_label(); }
	| for3 stmt  {  /* ubr to expr3 and create the break label */
	  fprintf(yyout,"\tUBR\t&L%d\n", $1);
	  fprintf(yyout,"L%d:\n",break_stack[break_index-1] );
	  pop_break_label(); }
	| RET ';' { fprintf(yyout,"\tRET\n"); } /* changed EXIT to RET */
	| RET expr ';' { /* set $RTN (#44) before return */
	   if ($2.optype == ATOM) {
	   sprintf(formstring,"\tSET\tR44\t%s\n", $2.type[0] ? "%d":"R%d");
	   fprintf(yyout,formstring, $2.args[0]);
	   } else  expr_eval(44, &$2);
	   fprintf(yyout,"\tRET\n"); } /* changed EXIT to RET */
	| '{' stmtlist '}'
	;
for1:	FOR '(' forexpr ';'
	  { /* do the preliminary expression and the top label */
	    /* normally the expr above will be an assigment and it will
	    have already generated code */
	  $$ = next_label++;   /* top label */
	  fprintf(yyout,"L%d:\n", $$);  }
	 ;
for2:	for1 expr ';'
	  { /* need to process the second expression like a conditional and
	    set the break label */
	  int iq;
	  $$ = $1;
	  iq = next_label++;  /* break label */
	  cond_eval(iq, &$2);	free_all_temps();
	  push_break_label(iq );  /* need break label for very end */
	  iq = next_label++;  /* "s" label for branch to stmt */
	  fprintf(yyout,"\tUBR\t&L%d\n", iq);
	  push_s_label(iq );  /* need s label after expr3 */
	  iq = next_label++;  /* "b" label for branch to expr3 */
	  fprintf(yyout,"L%d:\n", iq);
	  push_b_label(iq );  /* need b label later */
	  }
	 ;
for3:	for2 forexpr ')'  { /* now after expr3, ubr to top and put in s label*/
	  fprintf(yyout,"\tUBR\t&L%d\n", $1);
	  fprintf(yyout,"L%d:\n",s_stack[s_index-1] ); pop_s_label();
	  $$ = b_stack[b_index-1]; pop_b_label(); }
	;
whilecond:   while cond { /* so we can set the break label */
	   $$ = $1;
	   push_break_label($2);
	   }
	;
ifstmt:	 if cond stmt {
	  /* just gen the target for the branch around using label # from
	  cond */
	  $$ = $2;
	  /* fprintf(yyout,"L%d:\n", $2); */
	  }
	| ifstmt  '\n'
	  ;
ifelse:	  ifstmt else { /* an intermediate so we can get if label to put
			 in code just before the else stmt */
	  /* first a branch around the if stmt (already coded) using
	  a new label */
	  $$ = next_label++;
	  fprintf(yyout,"\tUBR\t&L%d\n", $$);
	  fprintf(yyout,"L%d:\n", $1);
	  }
	| ifelse  '\n'
	 ;
asgn:	  reg '=' expr {
	   /* depends on what expr is */
	   if ($3.optype == ATOM) {
	   sprintf(formstring,"\tSET\tR%%d\t%s\n", $3.type[0] ? "%d":"R%d");
	   fprintf(yyout,formstring, $1, $3.args[0]);
	   /* if the type is a symbol, log it as used */
	   } else  expr_eval($1, &$3);
	$$ = $1;
	free_all_temps();
	}
	;
dload:	  reg '~' SYMBOL ';'{
	   /* loads two consecutive registers with contents of 32 bit sym */
	  Symbol	*s1;
	  int	b16, t16;
	  s1=lookup($3, &symlist); free($3);
	  if (s1->type == 0) {
	   fprintf(stdout,";no ID for symbol: %s", s1->name); }
	  fprintf(yyout,";32 bit symbol value = %d\n", s1->val);
	  b16 = (s1->val) & 0xffff;
	  t16 = ((s1->val)>>16) & 0xffff;
	  if ($1 >= 255)
	   { fprintf(stdout,";fatal error\n");
	   fprintf(stdout,";register value too high (%d) in double load command\n", $1);
	    exit(0); }
	  fprintf(yyout,"\tSET\tR%d\t%d\n", $1, b16);
	  fprintf(yyout,"\tSET\tR%d\t%d\n", $1+1, t16);
	}
	;
alias:	  SYMBOL '=' SYMBOL ';' {
	  Symbol	*s2, *s1;
	  s2=lookup($3, &symlist); free($3);
	  if (s2->type == 0) {
	   fprintf(stdout,";no ID for symbol: %s", s2->name); }
	  s1=lookup($1, &symlist); free($1);
	  $$ = s1->val = s2->val; s1->type = s2->type; s1->status=s2->status;}
	| SYMBOL '=' literal  ';' {
	  Symbol	*s;
	  s=lookup($1, &symlist); free($1);
	  /* the actual type depends on our source and is input_symbol_type */
	  $$ = s->val = $3;  s->type = input_symbol_type; s->status = 0; }
	| SYMBOL '=' literal  '\n' {
	  Symbol	*s;
	  s=lookup($1, &symlist); free($1);
	  /* the actual type depends on our source and is input_symbol_type */
	  $$ = s->val = $3;  s->type = input_symbol_type; s->status = 0; }
	| SYMBOL '=' literal ':' literal ';' {
	  Symbol	*s;
	  int iq;
	  s=lookup($1, &symlist); free($1);
	  $$ = s->val = $3;  s->type = (short) $5;  s->status = 0; }
	| SYMBOL '=' '(' SYMBOL ')' ';' {
	  Symbol	*s2, *s1;
	  s2=lookup($4, &symlist); free($4);
	  if (s2->type == 0) {
	   fprintf(stdout,";no ID for symbol: %s", s2->name); }
	  s1=lookup($1, &symlist); free($1);
	  $$ = s1->val = s2->val; s1->type = s2->type; s1->status=s2->status;}
	;
literal:  NUMBER	{ $$ = $1; }
	| '(' literal ')' { $$ = $2; }
	;
reg:	  GLOBAL	{ $$ = $1 + 16; }	/* set real register # */
	| LOCAL		{ $$ = $1; }
	| RWBIV		{ $$ = $1; }
	| ROBIV		{ $$ = $1; }
	;
cond:	 '(' expr ')'	{ /* gens a BZ code and returns the label used */
	  $$ = next_label++;
	  cond_eval($$, &$2);	free_all_temps();
	 }
break:	  BREAK { code_break(); }
	;
goto:	  GOTO	SYMBOL { /* do a UBR to the label, we check at end for
			undefined labels */
	  Symbol	*s;
	  s=lookup($2, &lablist); free($2);
	  if (s->type == 0) { s->val = next_label++;  s->type =2; }
	  $$ = s->val;
	  fprintf(yyout,"\tUBR\t&L%d\n", $$);
	  }
	;
label:	  SYMBOL ':' { /* label gets "defined" if it appears here */
	  Symbol	*s;
	  s=lookup($1, &lablist); free($1);
	  /* if already defined, a problem */
	  if (s->type == 1) { fprintf(yyout,";duplicate label: %s", s->name); }
	  if (s->type == 0)  { s->val = next_label++; }
	  s->type = 1;
	  $$ = s->val;
	  fprintf(yyout,"L%d:\n", $$); }
	;
if:	  IF { /* printf(";start if construction\n"); */ }
	;
else:	  ELSE { /*fprintf(yyout,";else section\n");*/ }
	;
while:	  WHILE { $$ = next_label++;
	  fprintf(yyout,"L%d:\n", $$);
	  }
	;
expr:     reg  {  $$.optype = ATOM; $$.type[0] = 0; $$.args[0] = $1;} 
	| asgn {  $$.optype = ATOM; $$.type[0] = 0; $$.args[0] = $1;}
	| literal {  $$.optype = ATOM; $$.type[0] = 1; $$.args[0] = $1;}
	| SYMBOL  {
	  Symbol	*s, *s2;
	  s=lookup($1, &symlist);
	  if (s->type == FRAME_SYM) {
	   s2 = lookup($1, &frame_list);
	   s2->val = s->val;
	   s2->type = s->type;
	    }
	  free($1);
	  if (s->type == 0) {
	   fprintf(stdout,";no ID for symbol: %s\n", s->name); }
	   /* don't convert to a literal but do assign the value
	   we use type 2 to denote that it was a symbol */
	   $$.optype = ATOM; $$.type[0] = 2; $$.args[0] = s->val;}
	| dinc { $$ = $1; }
	| '(' expr ')'	{ $$ = $2; }
	| expr '+' expr { $$.optype = ADD; code_expr(&$$, &$1, &$3); }
	| expr '-' expr { $$.optype = SUB; code_expr(&$$, &$1, &$3); }
	| expr '*' expr { $$.optype = MUL; code_expr(&$$, &$1, &$3); }
	| expr '/' expr { $$.optype = DIV; code_expr(&$$, &$1, &$3); }
	| '-' expr %prec UNARYMINUS 
	   { $$.optype = SUB; code_expr(&$$, &zatom, &$2);
	   /* there is no simple negation command so we do a
	   subtract with 0 as first argument */ }
	| NOT expr %prec NOT
	   { $$.optype = EQ_OP; code_expr(&$$, &$2, &zatom);
	   /* handled similarly to unary minus, we create a "fake"
	   == operation such that ! expr ==> expr == 0 */ }
	| expr EQ expr { $$.optype = EQ_OP; code_expr(&$$, &$1, &$3); }
	| expr LT expr { $$.optype = LT_OP; code_expr(&$$, &$1, &$3); }
	| expr GT expr { $$.optype = GT_OP; code_expr(&$$, &$1, &$3); }
	| expr NE expr { $$.optype = NE_OP; code_expr(&$$, &$1, &$3); }
	| expr GE expr { $$.optype = GE_OP; code_expr(&$$, &$1, &$3); }
	| expr LE expr { $$.optype = LE_OP; code_expr(&$$, &$1, &$3); }
	| expr AND expr { $$.optype = AND_OP; code_expr(&$$, &$1, &$3); }
	| expr OR expr  { $$.optype = OR_OP;  code_expr(&$$, &$1, &$3); }
	;
forexpr:  asgn {  $$.optype = ATOM; $$.type[0] = 0; $$.args[0] = $1;}
	| dinc { /* in this case we need to evaluate the dinc */
	  dinc_eval(&$1); $$ = $1; }
dinc:	  PP expr  	{ $$.optype = PRE_INC; code_expr(&$$, &$2, &uatom); }
	| expr PP  	{ $$.optype = POST_INC; code_expr(&$$, &$1, &uatom); }
	| MM expr  	{ $$.optype = PRE_DEC; code_expr(&$$, &$2, &uatom); }
	| expr MM	{ $$.optype = POST_DEC; code_expr(&$$, &$1, &uatom); }
	;
setlist: FRAMELIST { list_reg = FIRST_FRAME_LIST_REG;
		list_max = 16;	list_log = 1;
		list_type = FRAME_SYM;
		list_start(); }
	| TARGETLIST { list_reg = FIRST_TARGET_LIST_REG;
		list_max = 25;	list_log = 1;
		list_type = TARGET_SYM;
		list_start(); }
	;
argelem:  literal { $$ = 1;
	   fprintf(yyout,";WARNING - literal in a frame or target list\n");
	  arg_stacker($1, (char *) NULL); }
	| SYMBOL { $$ = 1;  sym_stacker($1);  free($1);}
	| reg	 { $$ = 1; 
	   fprintf(yyout,";WARNING - register in a frame or target list\n");
	   arg_stacker($1, (char *) NULL); }
	| LIST  {  /* load entire list to arg_stack */
	  $$ = list_stacker($1, &listlist); }
	;
arglist:  /* nothing */  { $$ = 0; }
	/* returns argument count and stacks arguments in arg_stack */
	| argelem		{ $$ = $1; }
	| arglist ',' argelem  		{ $$ = $1 + $3; }
	| arglist ',' '\n' argelem	{ $$ = $1 + $4; }
	| arglist '\n' ',' argelem 	{ $$ = $1 + $4; }
	;
bitmasklist:   /* nothing */  { $$ = 0; }
	| bitmasklist ',' literal  { $$ = $1 | $3; }
	| bitmasklist ',' BITMASK  { $$ = $1 | $3; }
	| bitmasklist ',' '\n' BITMASK  { $$ = $1 | $4; }
	| bitmasklist '\n' ',' BITMASK  { $$ = $1 | $4; }
	;
listinit:	LIST '='   { /* define a named list */
	 ListArray	*s;
	 s = list_lookup($1, &listlist); free($1);
	 $$ = s;
	 s->type = UNDEF;
	 /* we can only be defining one list at a time, so use argstack
	 to load these and later define s->values */
	 list_log = 0;		/* to prevent marking as used right away */
	 list_type = UNDEF;	/* first element will define type */
	 list_start(); }
	;
listload:  listinit '(' arglist ')' ';'
	 { /* the size is in $3, the pointer to the list in $1, set up array
	   in list structure and copy from arg_stack */
	 $1->type = list_type; /* from the elements */
	 (void) list_set_array($1, $3);
	 /* fprintf(yyout,";list was loaded with %d elements\n", $3); */
	 $$ = $3; }
	;
%%
 /* -------------------------------------------------------------*/
int contextframe1()
 {
 /* does the various max saves in local registers for the
 contextframe expansion */
 sprintf(formstring,"\tSET\tR10\tR38\n"); /* $LA = $MXR */
 fprintf(yyout,formstring);
 sprintf(formstring,"\tSET\tR11\tR39\n"); /* $LB = $MXB */
 fprintf(yyout,formstring);
 sprintf(formstring,"\tSET\tR12\tR41\n"); /* $LC = $MNR */
 fprintf(yyout,formstring);
 sprintf(formstring,"\tSET\tR13\tR42\n"); /* $LD = $MNC */
 fprintf(yyout,formstring);
 return 1;
 }
 /* -------------------------------------------------------------*/
int contextframe2()
 {
 /* restores the values saved in contextframe1 */
 sprintf(formstring,"\tSET\tR38\tR10\n"); /* $MXR = $LA */
 fprintf(yyout,formstring);
 sprintf(formstring,"\tSET\tR39\tR11\n"); /* $MXB = $LB */
 fprintf(yyout,formstring);
 sprintf(formstring,"\tSET\tR41\tR12\n"); /* $MNR = $LC */
 fprintf(yyout,formstring);
 sprintf(formstring,"\tSET\tR42\tR13\n"); /* $MNC = $LD */
 fprintf(yyout,formstring);
 return 1;
 }
 /* -------------------------------------------------------------*/
int	dinc_eval(pd)
 expdb	*pd;
 {
 int	iq;
 /* here we should be in the realm of the inc and dec operations */
 /* these should only operate on registers */
 if (pd->type[0] != 0) {
 fprintf(yyout,"inc or dec target is not a register in dinc_eval\n");
 yyerror("syntax error in list ");
 return -1;
 }
 iq = pd->args[0];
 switch (pd->optype) {
 case PRE_INC:
 fprintf(yyout,"\tINC\tR%d\n", iq);
 break;
 case PRE_DEC:
 fprintf(yyout,"\tDEC\tR%d\n", iq);
 break;
 case POST_INC:
 fprintf(yyout,"\tINC\tR%d\n", iq);
 break;
 case POST_DEC:
 fprintf(yyout,"\tDEC\tR%d\n", iq);
 break;
 }
 return 1;
 }
 /* -------------------------------------------------------------*/
int list_start()
 {
 /* actually doesn't do much now */
 arg_index = 0;
 return 1;
 }
 /* -------------------------------------------------------------*/
int execute_list(pd1, pd2, mask)
 expdb	*pd1, *pd2;
 int	mask;
 {
 int	n, i, wait_frame, wait_target, reg_flag = 0;
 /* the wait times must be atomic, in pd1 and pd2 */
 /* 8/27/97, if either of these are registers, we can't use the SETREGS
 command and have to do 3 SET's instead */
 
 if (pd1->optype != ATOM) {
 /* this must be an atom */
  fprintf(yyout,"wait_frame argument in execute_list is not a simple scalar\n");
  yyerror("syntax error in list ");
  return -1;
 }
 if (pd2->optype != ATOM) {
 /* this must be an atom */
  fprintf(yyout,"wait_target argument in execute_list is not a simple scalar\n");
  yyerror("syntax error in list ");
  return -1;
 }
 if (pd1->type[0] && pd2->type[0]) {
 /* both are non-registers */
 wait_frame = pd1->args[0];
 wait_target = pd2->args[0];

 fprintf(yyout,"\tSETREGS\tR%d\t3", EXECUTE_LIST_REG);
 fprintf(yyout,"\n\tDATA\t %7d %7d %7d\n", wait_frame, wait_target, mask);
 } else {
 /* somebody must be a register, so do 3 SET's */
 sprintf(formstring,"\tSET\tR%%d\t%s\n", pd1->type[0] ? "%d":"R%d");
 fprintf(yyout,formstring, EXECUTE_LIST_REG, pd1->args[0]);
 sprintf(formstring,"\tSET\tR%%d\t%s\n", pd2->type[0] ? "%d":"R%d");
 fprintf(yyout,formstring, EXECUTE_LIST_REG+1, pd2->args[0]);
 fprintf(yyout,"\tSET\tR%d\t%d\n", EXECUTE_LIST_REG+2, mask);
 }
 
 fprintf(yyout,"\tCALL\t%d\t%d\n", EXECUTE_LIST_SUBROUTINE_LO,EXECUTE_LIST_SUBROUTINE_HI);
 
 return 1;
 }
 /* -------------------------------------------------------------*/
int arg_stacker(x, sym)
 int	x;
 char *sym;
 {
 arg_stack[arg_index] = x;
 name_stack[arg_index] = sym;
 arg_index += 1;
 if (arg_index >= MAX_ARGS) {
  /* whoops, too many ! */
  fprintf(yyout,"too many arguments in list !\n");
  yyerror("syntax error in list ");
  return -1;
 }
 return 1;
 }
 /* -------------------------------------------------------------*/
int sym_stacker(sym)
 char	*sym;
 {
 Symbol	*s, *s2;
 s=lookup(sym, &symlist);
 if (s->type == 0) {
  fprintf(yyout,
   ";WARNING - no ID for symbol in a list element: %s\n",s->name);}
 if (list_type == UNDEF) {  /* if list is undefined, take first
   element that is defined, this happens with the LIST = type
   of statement */
   list_type = s->type; }
 if (s->type != list_type && s->type != 0) {
  fprintf(stdout,
   ";FATAL - wrong type symbol in a list element: %s\n",s->name);
   exit (0); }
 /* log the symbol into either the target or frame symbol list */
 /* but not if we are reading a list, for those we log the list instead */
 if (list_log) {
 switch (s->type) {
 case FRAME_SYM: s2 = lookup(sym, &frame_list); break;
 case TARGET_SYM: s2 = lookup(sym, &target_list); break;
 case UNDEF: break;
 default:  fprintf(stdout, ";FATAL - list is not frame or target type\n");
   exit (0);	  
 }
 s2->val = s->val;
 s2->type = s->type;
 }  /* end of log option */
 arg_stacker(s->val, s->name);
 return 1;
 }
 /* -------------------------------------------------------------*/
int list_finish(count, list_reg, max_count)
 int count;
 {
 int	n, i;
 if (count > max_count) {
  fprintf(stdout,";FATAL - too many items for list type\n");
  exit (0); }

 if (count != arg_index) {
  fprintf(yyout,"internal count error in list\n");
  yyerror("syntax error in list ");
  return -1;
 }
 /* now we (finally) know how many there are so we can do the first line */
 fprintf(yyout,"\tSETREGS\tR%d\t%d\t%d", list_reg, arg_index+1, arg_index);
 /* note that line return done at start of list */
 n = arg_index;
 /* and the rest in some kind of fixed format (may change) */
 /* try 8 per line */
 for (i=0;i<arg_index;i++) {
 if (i%8 == 0) fprintf(yyout,"\n\tDATA\t");
 fprintf(yyout," %7d", arg_stack[i]);
 }
 fprintf(yyout,"\n");
 return 1;
 }
 /* -------------------------------------------------------------*/
 /* mostly taken from hoc but modified to use several lists */
Symbol	*lookup(s, list)
 char	*s;
 Symbol	**list;
 /* modified to input a list pointer; if not found, automatic install
 as undefined with value 0 */
 {
 Symbol *sp;
 for (sp = *list; sp != (Symbol *) 0; sp = sp->next)
   if (strcmp(sp->name, s) == 0) return sp;
   /* not found */
   return install(s, list, 0, 0);
 }
 /* -------------------------------------------------------------*/
Symbol	*install(s, list, t, d)
 char	*s;
 Symbol	**list;
 int	t;
 int	d;
 {
 Symbol *sp;
 sp = (Symbol *) malloc( sizeof(Symbol));
 sp->name = (char *) malloc(strlen(s)+1);
 strcpy(sp->name, s);
 sp->type = t;
 sp->val  = d;
 sp->next = *list;
 *list = sp;
 return sp;
 }
 /* -------------------------------------------------------------*/
 /* use to remove all entires in a list */
int	zap_list(list)
 Symbol	**list;
 {
 Symbol *sp, *sp2;
 for (sp = *list; sp != (Symbol *) 0; sp = sp2)
   { free(sp->name); sp2 = sp->next;  free(sp); }
 *list = (Symbol *) 0;
 return 1;
 }
 /* -------------------------------------------------------------*/
 /* dump a list */
int dump_list(list, fout, vflag)
 Symbol	**list;
 FILE *fout;
 int	vflag;
 /* runs through the list and prints to specified file */
 {
 Symbol *sp;
 char	*sq;
 for (sp = *list; sp != (Symbol *) 0; sp = sp->next)
   if (vflag) {
     switch (sp->type) {
     case 0: sq = "UNDEFINED"; break;
     case 1: sq = "user"; break;
     case 2: sq = "frame"; break;
     case 3: sq = "sequence"; break;
     case 4: sq = "target"; break;
     default: sq = "UNKNOWN"; break;
     }
     fprintf(fout,";%-40s == %#10x (%10d) %10s  %d\n", sp->name,sp->val,sp->val,sq, sp->status);
   }
   	else fprintf(fout,";%s\n", sp->name); 
 return 1;
 }
 /* -------------------------------------------------------------*/
 /* dump the file names for a list */
int dump_paths(list, fout)
 Symbol	**list;
 FILE *fout;
 /* runs through the list, finds file, and prints to specified file */
 {
 Symbol *sp;
 char	*sq;
 char	*seq_file(), *frame_file();
 for (sp = *list; sp != (Symbol *) 0; sp = sp->next)
   {
   /* printf(";name in dump_paths = %s\n", sp->name); */
   /* we should only encounter frames and sequences here */
   switch (sp->type) {
    case 2: sq = frame_file(sp->name); break;
    case 3: sq = seq_file(sp->name); break;
    default: sq = NULL;
     printf("unexpected symbol type %d encountered, please check, name is %s\n",
     	sp->type, sp->name);
     break;
   }
   if (sq) fprintf(fout,"%s\n", sq); else {
     serious_errors += 1;
     printf(";could not find a file for: %s (type %d)\n", sp->name,sp->type); }
   }
 return 1;
 }
 /* -------------------------------------------------------------*/
ListArray	*list_lookup(s, list)
 char	*s;
 ListArray	**list;
 /* modified to input a list pointer; if not found, automatic install
 as undefined with length of 0 */
 {
 ListArray *sp;
 for (sp = *list; sp != (ListArray *) 0; sp = sp->next)
   if (strcmp(sp->name, s) == 0) return sp;
   /* not found */
   return list_install(s, list);
 }
 /* -------------------------------------------------------------*/
int list_stacker(s, list)
 char	*s;
 ListArray	**list;
 {
 ListArray *sp;
 Symbol	*s2;
 char	*sq, **psym;
 int	n, *p;
 sp =  list_lookup(s, list);
 /* fprintf(yyout,";expanding list %s, n = %d\n", s, sp->size); */
 n = sp->size;
 sq = sp->name;		/* name of list */
 /* actually using this list, so log it in list_of_list */
 s2 = lookup(sq, &list_of_list);
 s2->type = sp->type;
 s2->val = sp->size;	/* use val for the list size */
 if (n <= 0) { fprintf(stdout,";null (undefined) list referenced; %s\n",
 	sp->name);  return 0; }
 /* check if stored type of list agrees with the type we have now */
 if (list_type != sp->type)
   fprintf(yyout,";WARNING - list contains symbols of mismatched type; %s\n",
 	sp->name);
 p = sp->values;
 psym = sp->names;
 while (n--) {
 arg_stack[arg_index] = *p++;
 name_stack[arg_index] = *psym++;
 arg_index += 1;
 if (arg_index >= MAX_ARGS) {
  /* whoops, too many ! */
  fprintf(yyout,"too many arguments in list !\n");
  yyerror("syntax error in list ");
  return -1;
 }
 }
 return sp->size;
 }
 /* -------------------------------------------------------------*/
ListArray	*list_install(s, list)
 /* install a new list */
 char	*s;
 ListArray	**list;
 {
 int	*p;
 ListArray *sp;
 sp = (ListArray *) malloc( sizeof(ListArray));
 sp->name = (char *) malloc(strlen(s)+1);
 strcpy(sp->name, s);
 sp->type = 0;
 sp->size = 0;
 sp->values = NULL;
 sp->names = NULL;
 sp->next = *list;
 *list = sp;
 return sp;
 }
 /* -------------------------------------------------------------*/
int list_set_array(sp, nelem)
 ListArray *sp;
 int	nelem;
 {
 int	*p, i;
 char	**psym;
 /* if the list was already defined, destroy it now */
 if (sp->values != NULL) {
 fprintf(yyout,"redefining a list %s\n", sp->name);
 free(sp->values);
 }
 
 if (sp->names != NULL) {
 fprintf(yyout,"redefining a list %s\n", sp->name);
 free(sp->names);
 }
 sp->size = nelem;  sp->values = (int *) malloc(nelem * sizeof(int));
 sp->names = (char **) malloc( nelem * sizeof(char *));
 
 p = sp->values;
 psym = sp->names;
 /* note that only pointers to names are copied, so we have 2 (or more) pointers
 to some names, careful if they have to be freed ! */
 for (i=0;i<nelem;i++)  {
 p[i] = arg_stack[i];
 psym[i] = name_stack[i];
 }
 return 1;
 }
 /* -------------------------------------------------------------*/
 /* dump the list_of_list */
int dump_list_of_list(fout)
 FILE *fout;
 /* runs through the list_of_list and prints to specified file */
 {
 Symbol	*sp, *s2;
 ListArray	*s;
 char	*sq, *sym, **psym;
 int	type, i, n, xq, *p;
 for (sp = list_of_list; sp != (Symbol *) 0; sp = sp->next) {

 fprintf(fout,";%-40s %d elements\n", sp->name,sp->val);
 fprintf(fout,";(");
 /* find the ListArray and log its members */
 s = list_lookup(sp->name, &listlist);
 n = s->size;
 type = s->type;
 /* for each entry in the list, find the name and log as either a frame or target */
 
 psym = s->names;
 
 p = s->values;
 for (i=0;i<n;i++) {
 sym = *psym++;
 xq = *p++;
 if (i%5 == 0 && i) fprintf(fout,"\n;");
 if (i != (n-1)) fprintf(fout,"%s,", sym); else fprintf(fout,"%s)\n", sym);
 switch (type) {
 case FRAME_SYM: s2 = lookup(sym, &frame_list); break;
 case TARGET_SYM: s2 = lookup(sym, &target_list); break;
 default:  fprintf(stdout, ";FATAL - list is not frame or target type\n");
   exit (0);	  
 }
 s2->val = xq;
 s2->type = type;
 }
 
 
 }
 return 1;
 }
 /* -------------------------------------------------------------*/
int push_break_label(n)
 int	n;
 {
 if (break_index >= MAX_COND_NEST) {
 	fprintf(yyout,"\nexceeded maximum conditional nesting\n");
	yyerror("syntax error in push_break_label ");
	return -1;
 }
 break_stack[break_index++] = n;
 return 1;
 }
 /* -------------------------------------------------------------*/
int push_s_label(n)
 int	n;
 {
 if (s_index >= MAX_COND_NEST) {
 	fprintf(yyout,"\nexceeded maximum conditional nesting\n");
	yyerror("syntax error in s_break_label ");
	return -1;
 }
 s_stack[s_index++] = n;
 return 1;
 }
 /* -------------------------------------------------------------*/
int push_b_label(n)
 int	n;
 {
 if (b_index >= MAX_COND_NEST) {
 	fprintf(yyout,"\nexceeded maximum conditional nesting\n");
	yyerror("syntax error in push_b_label ");
	return -1;
 }
 b_stack[b_index++] = n;
 return 1;
 }
 /* -------------------------------------------------------------*/
int pop_break_label()
 {
 break_index--;
 if (break_index < 0) {
 	fprintf(yyout,"\ninternal error, problem with break stack\n");
	yyerror("syntax error in pop_break_label ");
	return -1;
 }
 return 1;
 }
 /* -------------------------------------------------------------*/
int pop_b_label()
 {
 b_index--;
 if (b_index < 0) {
 	fprintf(yyout,"\ninternal error, problem with b stack\n");
	yyerror("syntax error in pop_b_label ");
	return -1;
 }
 return 1;
 }
 /* -------------------------------------------------------------*/
int pop_s_label()
 {
 s_index--;
 if (s_index < 0) {
 	fprintf(yyout,"\ninternal error, problem with s stack\n");
	yyerror("syntax error in pop_s_label ");
	return -1;
 }
 return 1;
 }
 /* -------------------------------------------------------------*/
int code_break()
 {
 /* just a UBR to the break label, an error if not in a conditional
 although it could be modified to be a warning (in which case this
 would generate no code) */
 if (break_index > 0) fprintf(yyout,"\tUBR\t&L%d\n", break_stack[break_index-1]); else
 	{ fprintf(yyout,"\nerror, break outside a conditional\n"); return -1; }
 return 1;
 }
 /* -------------------------------------------------------------*/
int gen_subr0(n)
 int n;
 /* a simple one, just output the name of the subroutine */
 {
 char	*sq = 0;
 switch (n) {
 case SRT: sq = "SRT"; break;
 case RET: sq = "RET"; break;
 case TERM: sq = "TERM"; break;
 case RESETAEC: sq = "RESETAEC"; break;
 case LOCKBUF: sq = "LOCKBUF"; break;
 case STARTAVE: sq = "STARTAVE"; break;
 case UPDIMAX: sq = "UPDIMAX"; break;
 case OPENLOOP: sq = "OPENLOOP"; break;
 case CLOSELOOP: sq = "CLOSELOOP"; break;
 default: fprintf(yyout,"internal error in gen_subr0\n");
 }
 fprintf(yyout,"\t%s\n", sq);
 return 1;
 }
 /* -------------------------------------------------------------*/
int call_ccdsafe()
 {
 call_subr("STD.CCDsafe");
 return 1;
 }
 /* -------------------------------------------------------------*/
int call_subr(sym)
 char *sym;
 /* 12/15/97 - modified to not free the symbol since it is now called
 with a fixed string from call_ccdsafe, hence the free is up in the
 grammar list */
 /* special for the CALL routine that calls a sequence */
 {
 Symbol	*s, *s2;
 int	lo, hi, iq;
 s = lookup(sym, &symlist);
 /* also enter into a list for seqs actually used */
 s2 = lookup(sym, &seq_list);
 s2->val = s->val;
 s2->type = s->type;
 s2->status = s->status;
 /* must be defined and right type or an error */
 if (s->type != SEQ_SYM) {
  /* whoops, got a mismatch */
  if (s->type == 0 )
    { fprintf(stdout,";undefined symbol used in CALL: %s\n", s->name); }
    else
    { fprintf(stdout,";symbol used in CALL is of wrong type: %s\n", s->name); }
 /* exit (0); */
 }
 /* check status */
 fprintf(stdout,";sequence %s status %d: ", s2->name, s2->status);
 switch (s2->status) {
 case 0:  fprintf(stdout,"not compiled yet\n"); break;
 case 1:  fprintf(stdout,"compiled already\n"); break;
 case 2:  fprintf(stdout,"already on board\n"); break;
 default: fprintf(stdout,"unknown status flag\n"); break;
 }
 /* the symbol value is 32 bits, we use all 32 in this one but must break
 into low and high */
 iq = s->val;
 lo = iq & 0xffff;	hi = (iq >> 16) & 0xffff;
 fprintf(yyout,"\tCALL\t%d\t%d\n", lo, hi);
 return 1;
 }
 /* -------------------------------------------------------------*/
int frame_subr(sym)
 char *sym;
 /* special for the FRAME routine that calls a frame */
 {
 Symbol	*s, *s2;
 int	lo, hi, iq;
 s=lookup(sym, &symlist);
 /* also enter into a list for frames actually used */
 s2 = lookup(sym, &frame_list);
 s2->val = s->val;
 s2->type = s->type;
 free(sym);
 /* must be defined and right type or an error */
 if (s->type != FRAME_SYM) {
  /* whoops, got a mismatch */
  serious_errors += 1;
  if (s->type == 0 )
    { fprintf(stdout,";no ID for symbol used in FRAME: %s\n", s->name); }
    else
    { fprintf(yyout,";symbol used in FRAME is of wrong type: %s\n", s->name); }
 }
 /* the symbol value is 32 bits, we use all 32 in this one but must break
 into low and high */
 iq = s->val;
 lo = iq & 0xffff;	hi = (iq >> 16) & 0xffff;
 if (hi) fprintf(yyout,";upper 16 bits not zero for frame symbol: %s, %x %x\n",
 	s->name, lo, hi);
 fprintf(yyout,"\tFRAME\t%d\n", lo);
 return 1;
 }
 /* -------------------------------------------------------------*/
int frame_subr_reg(r)
 int	r;
 /* for the FRAME routine that calls a frame, must have reg arg here */
 {
 fprintf(yyout,"\tFRAME\tR%d\n", r);
 return 1;
 }
 /* -------------------------------------------------------------*/
int target_subr(sym)
 char *sym;
 /* special for the TARGET routine that calls a target */
 /* this provides the STTG command with 2 values but only the first is really
 used (should have changed the flight s/w), the second should always be 0 */
 {
 Symbol	*s, *s2;
 int	lo, hi, iq;
 s=lookup(sym, &symlist);
 /* also enter into a list for seqs actually used */
 s2 = lookup(sym, &target_list);
 s2->val = s->val;
 s2->type = s->type;
 /* must be defined and right type or an error */
 if (s->type != TARGET_SYM) {
  /* whoops, got a mismatch */
  serious_errors += 1;
  if (s->type == 0 )
    { fprintf(stdout,";no ID for symbol used in TARGET: %s\n", s->name); }
    else
    { fprintf(stdout,";symbol used in TARGET is of wrong type: %s\n", s->name); }
 }
 /* the symbol value is 32 bits, we use all 32 in this one but must break
 into low and high */
 iq = s->val;
 lo = iq & 0xffff;	hi = (iq >> 16) & 0xffff;
 if (hi) fprintf(yyout,";upper 16 bits not zero for target symbol: %s, %x %x\n",
 	s->name, lo, hi);
 /* force hi to 0 in any case */
 hi = 0;
 fprintf(yyout,"\tSTTG\t%d\t%d\n", lo, hi);
 free(sym);
 return 1;
 }
 /* -------------------------------------------------------------*/
int target_subr_reg(r)
 int	r;
 /* for the TARGET routine that calls a frame, must have reg arg here */
 {
 /* note that the hi 16 bits is explicitly set to 0 here */
 fprintf(yyout,"\tSTTG\tR%d\t0\n", r);
 return 1;
 }
 /* -------------------------------------------------------------*/
int gen_subr1(n, pd)
 int n;
 expdb	*pd;
 /* one argument */
 /* 4/3/97, modify TIM such that argument is in seconds rather than ticks */
 /* this involves ... */
 {
 char	*sq = 0;
 int	tq;
 switch (n) {
 case WAIT: sq = "WAIT"; break;
 case DELAY: sq = "DELAY"; break;
 case TIM: sq = "TIM"; break;
 case RUNDHCP: sq = "RUNDHCP"; break;
 case CADENCE: sq = "TIM"; break;
 default: { fprintf(stdout,"internal error in gen_subr1, fatal!\n"); exit(0); }
 }
 /* may have to evaluate the argument */
 if (pd->optype == ATOM) {
  /* just an atom, put it in as argument as either register or literal */
  sprintf(formstring,"\t%s\t%s\n", sq, pd->type[0] ? "%d":"R%d");
  fprintf(yyout,formstring, pd->args[0]);
 } else  {
  /* define a temp for the result */
 if ((tq = get_next_temp()) < 0) return temp_err();
 expr_eval(tq, pd);
 fprintf(yyout,"\t%s\tR%d\n", sq, tq);
 free_all_temps();
 }
 /* if this was a "cadence" command, add the srt as well */
 if (n == CADENCE) fprintf(yyout,"\tSRT\n");
 return 1;
 }
 /* -------------------------------------------------------------*/
int gen_subr2(n, pd1, pd2)
 int n;
 expdb	*pd1, *pd2;
 /* two arguments */
 {
 char	*sq = 0;
 int	tq1, tq2;
 switch (n) {
 case DEV:       sq = "DEV";  break;
 case SAMP:      sq = "SAMP"; break;
 case SETDHCR:   sq = "SETDHCR"; break;
 case TIMCHK:    sq = "TIMCHK"; break;
 case CALL2:     sq = "CALL"; break;
 case LDA:       sq = "LDA"; break;
 case LDR:       sq = "LDR";
   /* for the LDR we want both arguments to be registers */
   if (pd1->type[0] != 0 || pd2->type[0] != 0) {
    fprintf(stdout,"\n;error in LDR, both arguments must be registers\n");
     yyerror("syntax error in LDR "); return -1;
   }
   break;
 default: fprintf(stdout,";internal error in gen_subr2\n");
 }
 /* may have to evaluate the arguments */
 /* several tests, kind of clumsy, but we need to put any evaluations into
 temporaries first */
 if (pd1->optype != ATOM) {
  /* define a temp for the result */
 if ((tq1 = get_next_temp()) < 0) return temp_err();
 expr_eval(tq1, pd1);
 }
 if (pd2->optype != ATOM) {
  /* define a temp for the result */
 if ((tq2 = get_next_temp()) < 0) return temp_err();
 expr_eval(tq2, pd2);
 }

 if (pd1->optype == ATOM) {
  /* just an atom, put it in as argument as either register or literal */
  sprintf(formstring,"\t%s\t%s", sq, pd1->type[0] ? "%d":"R%d");
  fprintf(yyout,formstring, pd1->args[0]);
 } else  {
 fprintf(yyout,"\t%s\tR%d", sq, tq1);
 }
 if (pd2->optype == ATOM) {
  /* just an atom, put it in as argument as either register or literal */
  /* special test for TIMCHK which can't have a literal for the second
     argument */
  if (pd2->type[0] == 1 && n == TIMCHK) {
    fprintf(stdout,"\n;error in TIMCHK, second argument cannot be a literal\n");
    yyerror("syntax error in TIMCHK "); return -1;
  }

  sprintf(formstring,"\t%s\n", pd2->type[0] ? "%d":"R%d");
  fprintf(yyout,formstring, pd2->args[0]);
 } else  {
 fprintf(yyout,"\tR%d\n", tq2);
 }
 free_all_temps();
 return 1;
 }
 /* -------------------------------------------------------------*/
int gen_subr3(n, pd1, pd2, pd3)
 int n;
 expdb	*pd1, *pd2, *pd3;
 /* three arguments */
 {
 char	*sq = 0;
 int	tq1, tq2, tq3;
 switch (n) {
 case FRAMEPF: sq = "FRAMEPF"; break;
 default: fprintf(stdout,";internal error in gen_subr3\n");
 }
 /* may have to evaluate the arguments */
 /* several tests, kind of clumsy, but we need to put any evaluations into
 temporaries first */
 if (pd1->optype != ATOM) {
  /* define a temp for the result */
 if ((tq1 = get_next_temp()) < 0) return temp_err();
 expr_eval(tq1, pd1);
 }
 if (pd2->optype != ATOM) {
  /* define a temp for the result */
 if ((tq2 = get_next_temp()) < 0) return temp_err();
 expr_eval(tq2, pd2);
 }
 if (pd3->optype != ATOM) {
  /* define a temp for the result */
 if ((tq3 = get_next_temp()) < 0) return temp_err();
 expr_eval(tq3, pd3);
 }

 if (pd1->optype == ATOM) {
  /* just an atom, put it in as argument as either register or literal */
  sprintf(formstring,"\t%s\t%s", sq, pd1->type[0] ? "%d":"R%d");
  fprintf(yyout,formstring, pd1->args[0]);
 } else  {
 fprintf(yyout,"\t%s\tR%d", sq, tq1);
 }
 if (pd2->optype == ATOM) {
  /* just an atom, put it in as argument as either register or literal */
  sprintf(formstring,"\t%s", pd2->type[0] ? "%d":"R%d");
  fprintf(yyout,formstring, pd2->args[0]);
 } else  {
 fprintf(yyout,"\tR%d", tq2);
 }
 if (pd3->optype == ATOM) {
  /* just an atom, put it in as argument as either register or literal */
  sprintf(formstring,"\t%s\n", pd3->type[0] ? "%d":"R%d");
  fprintf(yyout,formstring, pd3->args[0]);
 } else  {
 fprintf(yyout,"\tR%d\n", tq3);
 }
 free_all_temps();
 return 1;
 }
 /* -------------------------------------------------------------*/
char	*get_op(pd)
 expdb	*pd;
 {
 char	*sq = 0;
 switch (pd->optype) {
 case ADD: sq = "ADD"; break;
 case SUB: sq = "SUB"; break;
 case MUL: sq = "MUL"; break;
 case DIV: sq = "DIV"; break;
 default: fprintf(yyout,"internal error in get_op\n");
 }
 return sq;
 }
 /* -------------------------------------------------------------*/
char	*get_branch(pd)
 expdb	*pd;
 {
 char	*sq = 0;
 switch (pd->optype) {
 case EQ_OP: sq = "BEQ"; break;
 case NE_OP: sq = "BNE"; break;
 case LT_OP: sq = "BLT"; break;
 case LE_OP: sq = "BLE"; break;
 case GT_OP: sq = "BGT"; break;
 case GE_OP: sq = "BGE"; break;
 default: fprintf(yyout,"internal error in get_branch\n");
 }
 return sq;
 }
 /* -------------------------------------------------------------*/
char	*get_branch_comp(pd)
 expdb	*pd;
 {
 char	*sq = 0;
 switch (pd->optype) {
 case EQ_OP: sq = "BNE"; break;
 case NE_OP: sq = "BEQ"; break;
 case LT_OP: sq = "BGE"; break;
 case LE_OP: sq = "BGT"; break;
 case GT_OP: sq = "BLE"; break;
 case GE_OP: sq = "BLT"; break;
 default: fprintf(yyout,"internal error in get_branch\n");
 }
 return sq;
 }
 /* -------------------------------------------------------------*/
int cond_eval(label, pd)
 /* generates one or more lines of code to evaluate a conditional */
 /* the conditional expression is reduced to an atomic value and then
 a BZ instruction is generated using the label as the destination, normally
 the label will be put into the stream later after a stmt(s) */
 int	label;
 expdb	*pd;
 {
 int	tq;
 int	label_true;
 if (pd->optype == ATOM) {
 /* 8/24/97 - if the atom is a literal, we check value here */
 if (pd->type[0]) {
  /* a literal, check the value and make a ubr if zero */
  if (pd->args[0] == 0) fprintf(yyout,"\tUBR\t&L%d\n", label);
  } else /* not a literal, must be a register */
 fprintf(yyout,"\tBZ\t&L%d\tR%d\n", label, pd->args[0]);
 } else {
 /* a bi op, means we have to evaluate into a temp and then test
 the temp unless it happens to be a conditional or logical */
 if (pd->optype <= DIV) {
  if ((tq = get_next_temp()) < 0) return temp_err();
  sprintf(formstring,"\t%s\tR%%d\t%s\t%s\n", get_op(pd),
  pd->type[0] ? "%d":"R%d", pd->type[1] ? "%d":"R%d");
  fprintf(yyout,formstring, tq, pd->args[0], pd->args[1]);
  fprintf(yyout,"\tBZ\t&L%d\tR%d\n", label, tq);
 } else
  if (pd->optype <= LE_OP) {
  /* these are conditionals so we can use the conditional branches
  more directly, but we need the "complement" of each */
  /* op2 must be a register, if it isn't, use a temp */
  if (pd->type[0] != 0) {
  if ((tq = get_next_temp()) < 0) return temp_err();
  fprintf(yyout,"\tSET\tR%d\t%d\n", tq, pd->args[0]);
  pd->args[0] = tq;	pd->type[0] = 0;
  }
  /* format the format string for the branch */
  sprintf(formstring,"\t%s\t&L%d\t%s\t%s\n", get_branch_comp(pd),
  label, pd->type[0] ? "%d":"R%d", pd->type[1] ? "%d":"R%d");
  /* now format and print the branch statement */
  fprintf(yyout,formstring, pd->args[0], pd->args[1]);
  } else
    if (pd->optype <= OR_OP) {
   /* logical and/or here */
    switch (pd->optype) {
    case AND_OP:
     /* both must be non-zero */
     if (pd->type[0]) { /* a literal, we can test for 0 here */
     	if (pd->args[0] == 0) { fprintf(yyout,"\tUBR\t&L%d\n",label);
	/* and we are done ! */
	break; }}
     if (pd->type[1]) { /* a literal, we can test for 0 here */
     	if (pd->args[1] == 0) { fprintf(yyout,"\tUBR\t&L%d\n",label);
	/* and we are done ! */
	break; }}
     /* any literals are true if we get here */
     if (pd->type[0] == 0) { /* a register, put in a BZ */
     fprintf(yyout,"\tBZ\t&L%d\tR%d\n", label, pd->args[0]); }
     if (pd->type[1] == 0) { /* a register, put in a BZ */
     fprintf(yyout,"\tBZ\t&L%d\tR%d\n", label, pd->args[1]); }
     break;
    case OR_OP:
     /* one must be non-zero */
     if (pd->type[0]) { /* a literal, we can test here */
     	if (pd->args[0]) break; /* if true, no branch ever */
	/* and we are done ! */
	}
     if (pd->type[0]) { /* a literal, we can test here */
     	if (pd->args[0]) break; /* if true, no branch ever */
	/* and we are done ! */
	}
     /* get here only if any literals are false */
     /* need a new label for possible branches around the false UBR */
     label_true = next_label++;
     if (pd->type[0] == 0) { /* a register, put in a BNZ to label_true */
     fprintf(yyout,"\tBNZ\t&L%d\tR%d\n", label_true, pd->args[0]); }
     if (pd->type[1] == 0) { /* a register, put in a BNZ to label_true */
     fprintf(yyout,"\tBNZ\t&L%d\tR%d\n", label_true, pd->args[1]); }

     fprintf(yyout,"\tUBR\t&L%d\n",label); /* if false, we get here in code*/
     fprintf(yyout,"L%d:\n", label_true);
     break;
     }
     } else {
     /* here we should be in the realm of the inc and dec operations */
     /* for the conditional, we have to use a temp for the "post" inc's
     and dec's and test it to insure the action on the register */
     /* these should only operate on registers */
     if (pd->type[0] != 0) {
     fprintf(yyout,"inc or dec target in cond_eval is not a register\n");
     yyerror("syntax error in list ");
     return -1;
     }
     iq = pd->args[0];
     switch (pd->optype) {
     case PRE_INC:
     fprintf(yyout,"\tINC\tR%d\n", iq);
     break;
     case PRE_DEC:
     fprintf(yyout,"\tDEC\tR%d\n", iq);
     break;
     case POST_INC:
     if ((tq = get_next_temp()) < 0) return temp_err();
     fprintf(yyout,"\tSET\tR%d\tR%d\n", tq, iq);
     fprintf(yyout,"\tINC\tR%d\n", iq);
     iq = tq;
     break;
     case POST_DEC:
     if ((tq = get_next_temp()) < 0) return temp_err();
     fprintf(yyout,"\tSET\tR%d\tR%d\n", tq, iq);
     fprintf(yyout,"\tDEC\tR%d\n", iq);
     iq = tq;
     break;
     }
     fprintf(yyout,"\tBZ\t&L%d\tR%d\n", label, iq);
     }
 }
 return 1;
 }
 /* -------------------------------------------------------------*/
int expr_eval(target_r, pd)
 /* generates one or more lines of code to evaluate an expression */
 /* target must be a register */
 int	target_r;
 expdb	*pd;
 {
 int	label_true, label_false, label_end;
 if (pd->optype == ATOM) {
  /* for the atom case, we just set the target to the atom */
  sprintf(formstring,"\tSET\tR%%d\t%s\n", pd->type[0] ? "%d":"R%d");
  fprintf(yyout,formstring, target_r, pd->args[0]);
  return 1;
  }
 if (pd->optype <= DIV) {
  sprintf(formstring,"\t%s\tR%%d\t%s\t%s\n", get_op(pd),
  pd->type[0] ? "%d":"R%d", pd->type[1] ? "%d":"R%d");
  fprintf(yyout,formstring, target_r, pd->args[0], pd->args[1]);
  free_temp( pd->args[0], pd->args[1]);
 } else
  if (pd->optype <= LE_OP) {
  /* these are true/false results, represented by 1/0 here,
  assume true and set the register to 1 */
  /* fprintf(yyout,"\tSET\tR%d\t1\n", target_r); */
  /* op2 must be a register, if it isn't, use a temp */
  if (pd->type[0]) {
  if ((iq = get_next_temp()) < 0) return temp_err();
  fprintf(yyout,"\tSET\tR%d\t%d\n", iq, pd->args[0]);
  pd->args[0] = iq;	pd->type[0] = 0;
  }
  /* format the format string for the branch */
  /* use next label */
  label_true = next_label++;
  sprintf(formstring,"\t%s\t&L%d\t%s\t%s\n", get_branch(pd),
  label_true, pd->type[0] ? "%d":"R%d", pd->type[1] ? "%d":"R%d");
  /* now format and print the branch statement */
  fprintf(yyout,formstring, pd->args[0], pd->args[1]);
  /* if condition not met, the next statement will set the
  result to false */
  fprintf(yyout,"\tSET\tR%d\t0\n", target_r);
  label_end = next_label++;
  fprintf(yyout,"\tUBR\t&L%d\n",label_end); /* done, go to end */
  /* and the label */
  fprintf(yyout,"L%d:\n", label_true);
  fprintf(yyout,"\tSET\tR%d\t1\n", target_r);
  fprintf(yyout,"L%d:\n", label_end);
  free_temp( pd->args[0], pd->args[1]);
  } else
    if (pd->optype <= OR_OP) {
   /* logical and/or here */
    switch (pd->optype) {
    case AND_OP:
     /* both must be non-zero */
     if (pd->type[0]) { /* a literal, we can test for 0 here */
     	if (pd->args[0] == 0) { fprintf(yyout,"\tSET\tR%d\t0\n", target_r);
	break; } }
     if (pd->type[1]) { /* a literal, we can test for 0 here */
     	if (pd->args[1] == 0) { fprintf(yyout,"\tSET\tR%d\t0\n", target_r);
	break; } }
     /* get here only if any literals are true  so if both are literals
     we just put a set to true here and we're done */
     if ((pd->type[0])&&(pd->type[1])) {
       fprintf(yyout,"\tSET\tR%d\t1\n", target_r);  break; }
     /* need a new label */
     label_false = next_label++;
     label_end = next_label++;
     if (pd->type[0] == 0) { /* a register, put in a BZ to label_false */
      fprintf(yyout,"\tBZ\t&L%d\tR%d\n", label_false, pd->args[0]); }
     if (pd->type[1] == 0) { /* a register, put in a BZ to label_false */
      fprintf(yyout,"\tBZ\t&L%d\tR%d\n", label_false, pd->args[1]); }
     fprintf(yyout,"\tSET\tR%d\t1\n", target_r); /* true if executed */
     fprintf(yyout,"\tUBR\t&L%d\n",label_end); /* done, go to end */
     fprintf(yyout,"L%d:\n", label_false);
     fprintf(yyout,"\tSET\tR%d\t0\n", target_r); /* make false */
     fprintf(yyout,"L%d:\n", label_end);
     break;
    case OR_OP:
     /* one must be non-zero */
     if (pd->type[0]) { /* a literal, we can test here */
     	if (pd->args[0]) { fprintf(yyout,"\tSET\tR%d\t1\n", target_r);
	break; } }
     if (pd->type[1]) { /* a literal, we can test here */
     	if (pd->args[1]) { fprintf(yyout,"\tSET\tR%d\t1\n", target_r);
	break; } }
     /* get here only if any literals are false so if both are literals
     we just put a set to false here and we're done */
     if ((pd->type[0])&&(pd->type[1])) {
       fprintf(yyout,"\tSET\tR%d\t0\n", target_r);  break; }
     /* need a new label */
     label_true = next_label++;
     label_end = next_label++;
     if (pd->type[0] == 0) { /* a register, put in a BNZ to label_true */
      fprintf(yyout,"\tBNZ\t&L%d\tR%d\n", label_true, pd->args[0]); }
     if (pd->type[1] == 0) { /* a register, put in a BNZ to label_true */
      fprintf(yyout,"\tBNZ\t&L%d\tR%d\n", label_true, pd->args[1]); }
     fprintf(yyout,"\tSET\tR%d\t0\n", target_r); /* if false, we get here in code*/
     fprintf(yyout,"\tUBR\t&L%d\n",label_end); /* done, go to end */
     fprintf(yyout,"L%d:\n", label_true);
     fprintf(yyout,"\tSET\tR%d\t1\n", target_r); /* make true */
     fprintf(yyout,"L%d:\n", label_end);
     break;
     }
     } else {
     int iq;
     /* here we should be in the realm of the inc and dec operations */
     /* these should only operate on registers, the r++ and r-- have to
     return the value before the operation */
     if (pd->type[0]) {
     fprintf(yyout,"inc or dec target is not a register\n");
     yyerror("syntax error in list ");
     return -1;
     }
     iq = pd->args[0];
     switch (pd->optype) {
     case PRE_INC:
     fprintf(yyout,"\tINC\tR%d\n", iq);
     fprintf(yyout,"\tSET\tR%d\tR%d\n", target_r, iq);
     break;
     case PRE_DEC:
     fprintf(yyout,"\tDEC\tR%d\n", iq);
     fprintf(yyout,"\tSET\tR%d\tR%d\n", target_r, iq);
     break;
     case POST_INC:
     fprintf(yyout,"\tSET\tR%d\tR%d\n", target_r, iq);
     fprintf(yyout,"\tINC\tR%d\n", iq);
     break;
     case POST_DEC:
     fprintf(yyout,"\tSET\tR%d\tR%d\n", target_r, iq);
     fprintf(yyout,"\tDEC\tR%d\n", iq);
     break;
     }

     }
 return 1;
 }
 /* -------------------------------------------------------------*/
int code_expr(pd0, pd1, pd2)
 expdb	*pd0, *pd1, *pd2;
 {
  char	*get_op();
  /* depends on what the expr's are, if either
  is already an operation, we have to generate intermediate
  results and use a temporary */
  if (pd1->optype == ATOM) {
  /* easy case, no calculations yet, arg is reg/literal */
  pd0->type[0] = pd1->type[0];
  pd0->args[0] = pd1->args[0];
  } else { /* otherwise, first expr needs to be evaluated */
  if ((pd0->args[0] = get_next_temp()) < 0) return temp_err();
  pd0->type[0] = 0;
  expr_eval(pd0->args[0], pd1);
  }
  if (pd2->optype == ATOM) {
  pd0->type[1] = pd2->type[0];
  pd0->args[1] = pd2->args[0];
  } else { /* otherwise, second expr needs to be evaluated */
  if ((pd0->args[1] = get_next_temp()) < 0) return temp_err();
  pd0->type[1] = 0;
  expr_eval(pd0->args[1], pd2);
  }
 return 1;
 }
 /* -------------------------------------------------------------*/
int get_next_temp()
 /* finds an available temp and reurns the register value */
 /* if all are used, then returns -1 */
 {
 int	i;
 for (i=0;i<NTEMPS;i++) if (temp_usage[i] == 0) {
 	highest_temp_used = MAX(i+1, highest_temp_used);
 	temp_usage[i] = 1; return i+FIRST_TEMP; }
 fprintf(yyout,"no temps left\n");
 return -1;
 }
 /* -------------------------------------------------------------*/
int free_temp(r1, r2)
 int r1, r2;
 /* checks if r1 or r2 are temprs and re-cycles if so */
 {
 if (r1 >= FIRST_TEMP) temp_usage[r1-FIRST_TEMP] = 0;
 if (r2 >= FIRST_TEMP) temp_usage[r2-FIRST_TEMP] = 0;
 }
 /* -------------------------------------------------------------*/
int free_all_temps()
 /* just clears temp_usage */
 {
 int	i;
 for (i=0;i<NTEMPS;i++) temp_usage[i] = 0;
 }
 /* -------------------------------------------------------------*/
int temp_err()
 {
 fprintf(yyout,"ran out of temporary registers, expression too complicated?\n");
 return -1;
 }
 /* -------------------------------------------------------------*/
int yyerror(s)		/* called for yacc syntax error */
 char	*s;
 {
  warning(s, (char *) 0);  /* note that warning is defined in lex.l */
  if (fatal_flag) exit(1);
 }
 /* -------------------------------------------------------------*/
char *strsave(s)	/* save a copy of a string */
 char *s;
 {
 char *p;
 p = (char *) malloc( strlen(s)+1);
 if (p == NULL)	{ fprintf(stdout,"malloc failure in strsave, abort!\n"); }
 strcpy(p,s);
 return(p);
 }
 /*------------------------------------------------------------------------- */
void slashcheck(s)
 char	*s;
 {
 /* just verifies that the last character is a slash for a presumed path */
 int	n;
 n = strlen(s);
 if (n) {
  if (*(s+n-1) != '/')  strcat(s, "/");
 }
 }
 /*------------------------------------------------------------------------- */
int read_id_tables()
 {
 /* read the 3 tables defining frames, sequences, and targets,
 return 0 if a problem */
 char	fullname[256], *id_path;
 id_path = getenv("TOPS_DBASE_PATH");
 if (!id_path) id_path = "/tsw/obs_dev/dbase/";

 input_symbol_type = 2;  /* for the frames */
 strcpy(fullname, id_path);
 /* check if there is a slash */
 slashcheck(fullname);
 strcat(fullname, "id_map_frm.tab");
 if ((yyin=fopen(fullname,"r")) == NULL) {
      fprintf(stdout,"%s fatal error\n", progname);
      fprintf(stdout,"can't open frame definition file\n%s\n", fullname);
      return 0;
 }
 fprintf(yyout, ";reading frame definition file\n");
 lineno = 0;
 yyparse();
 fprintf(yyout,";%d lines compiled in frame definition file\n", lineno);
 fclose(yyin);
 input_symbol_type = 3;  /* for the sequences */
 strcpy(fullname, id_path);
 slashcheck(fullname);
 strcat(fullname, "id_map_seq.tab");
 if ((yyin=fopen(fullname,"r")) == NULL) {
      fprintf(stdout,"%s fatal error\n", progname);
      fprintf(stdout,"can't open sequence definition file\n%s\n", fullname);
      return 0;
 }
 fprintf(yyout, ";reading sequence definition file\n");
 lineno = 0;
 yyparse();
 fprintf(yyout,";%d lines compiled in sequence definition file\n", lineno);
 fclose(yyin);
 
 input_symbol_type = 4;  /* for the targets */
 strcpy(fullname, id_path);
 slashcheck(fullname);
 strcat(fullname, "id_map_trg.tab");
 if ((yyin=fopen(fullname,"r")) == NULL) {
      fprintf(stdout,"%s fatal error\n", progname);
      fprintf(stdout,"can't open target definition file\n%s\n", fullname);
      return 0;
 }
 fprintf(yyout, ";reading target definition file\n");
 lineno = 0;
 yyparse();
 fprintf(yyout,";%d lines compiled in target definition file\n", lineno);
 fclose(yyin);

 input_symbol_type = 1;  /* for the lists */

 
 strcpy(fullname, id_path);
 slashcheck(fullname);
 strcat(fullname, "list_expand.tab");
 if ((yyin=fopen(fullname,"r")) == NULL) {
      fprintf(stdout,"%s fatal error\n", progname);
      fprintf(stdout,"can't open list definition file\n%s\n", fullname);
      return 0;
 }
 fprintf(yyout, ";reading list definition file\n");
 lineno = 0;
 yyparse();
 fprintf(yyout,";%d lines compiled in list definition file\n", lineno);
 fclose(yyin);
 
 return 1;
 }
 /*------------------------------------------------------------------------- */
int get_sid(s)
 char	*s;
 {
 /* try to deduce the sid from the name in s, probably a file name */
 char *s2, *s3, *s4;
 int	n, mq;
 Symbol	*sq1, *sq2;
 s2 = s;
 n = strlen(s);
 /* find the last / and point just after it */
 s2 = strrchr(s, '/');
 if (s2 == NULL) s2 = s; else s2++;
 n = strlen(s2);
 if (n == 0) {
  fprintf(stdout,";nothing left of sequence name after path removal (?)\n");
  return -1; } 
 /* remove a ".useq" if present */
 if (s3 = strstr(s, sequence_suffix)) n = n -strlen(s3);
 /* n is what we have left, starting at s2 */
 s4 = (char *) malloc(n+1);
 bcopy(s2, s4, n);
 *(s4+n) = 0;
 /* our name should be in s4 */
 /* printf("seq name for search: %s\n",  s4); */
 /* now similar to the code in call_subr with a few differences */
 /* locate it in symbol table */
 sq1 = lookup(s4, &symlist) ;
 /* also enter into a list for seqs actually used */
 sq2 = lookup(s4, &seq_list);
 sq2->val = sq1->val;
 sq2->type = sq1->type;
 /* and, since we are now compiling it, mark it as compiled */
 sq2->status = 1;
 /* must be defined and right type or an error */
 if (sq1->type != SEQ_SYM) {
  /* whoops, got a mismatch, return a sid of -1 */
  serious_errors += 1;
  if (sq1->type == 0 )
    { fprintf(stdout,";no ID found for sequence file: %s\n;stripped to :%s\n",
	 s, s4);
	 free(s4); return -1;}
    else
    { fprintf(stdout,";ID is of wrong type for sequence file: %s\n;stripped to :%s\n",
	 s, s4);
    	 free(s4); return -1; }
 }
 free(s4);
 /* the symbol value is 32 bits, return it */
 return sq1->val;
 }
 /*------------------------------------------------------------------------- */
int set_sid(sid)
 int sid;
 {
 int	lo, hi;
 /* the sid value is 32 bits, we use all 32 but must break into low and high */
 /* 11/25/97 - also set the version number */
 /* 2/20/98 - allow version part of ID through unless we are
 deliberately inputting a version # */
 lo = sid & 0xffff;
 if (version_input_flag) lo = (sid & 0xfff0) | (version_number& 0xf);
 hi = (sid >> 16) & 0xffff;
 fprintf(yyout,";new sequence\n");
 fprintf(yyout,"\tSID\t%d\t%d\t0\n", lo, hi);
 return 1;
 }
 /*------------------------------------------------------------------------- */
int main_seq_setup(sid)
 {
 /* sets some registers to identify this as a main sequence */
 int	lo, hi;
 /* the sid value is 32 bits, we use all 32 but must break into low and high */
 /* 11/25/97 - also set the version number */
 /* 2/20/98 - allow version part of ID through unless we are
 deliberately inputting a version # */
 lo = sid & 0xffff;
 if (version_input_flag) lo = (sid & 0xfff0) | (version_number& 0xf);
 hi = (sid >> 16) & 0xffff;
 fprintf(yyout,"\tSETREGS\tR14\t2\t%d\t%d\n", lo, hi);
 return 1;
 }
 /*------------------------------------------------------------------------- */
char *find_uncompiled_seq()
 {
 /* check the sequence list */
 Symbol	*sp;
 for (sp = seq_list; sp != (Symbol *) 0; sp = sp->next)
   if ( sp->status == 0 ) { 
   	printf(";uncompiled, sp->val = %d, name = %s\n",sp->val,sp->name);
   	sp->status = 1;	/* mark compiled so we don't find it again */
	if (sp->val >= 0) {
   	return sp->name; }  else {
	printf("won't compile because sp->val = %d\n", sp->val);
	}}
 /* none found */
 fprintf(yyout,";no addition uncompiled cases found\n");
 return  NULL;
 }
 /*------------------------------------------------------------------------- */
void list_uncompiled_seq()
 {
 /* check the sequence list */
 Symbol	*sp;
 int	ic = 0;
 for (sp = seq_list; sp != (Symbol *) 0; sp = sp->next) {
   /* list each one */
   ic++;
   printf("status, value %d, %d, name %s\n", sp->status,sp->val,sp->name);
   if ( sp->status == 0 ) { 
   	printf(";uncompiled, sp->val = %d, name = %s\n",sp->val,sp->name);
	if (sp->val >= 0) {
   	printf("expect to compile\n"); }  else {
	printf("won't compile because sp->val = %d\n", sp->val);
	}}
   }
 fprintf(yyout,";end of first uncompiled list, count = %d\n", ic);
 }
 /*------------------------------------------------------------------------- */
char *file_find(sdir, fname)
 char	*sdir, *fname;
 {
 /* check if a file fname is in the directory or any subdirectories, return
 first path found with fname, return NULL if not found */
 /* modified to not search branches unless the b flag (search_flag) is set,
  * this disables opening directories
  */
 DIR	*dirp;
 struct	direct	*dp;
 struct stat statbuf;
 static	char *last_success;
 int	n, mq;
 char *sq, *s2;

 n = strlen(fname);
 /* as a strategy, try the last success first */
 if (last_success) {
 sq = last_success;
 last_success = NULL;
 s2 = file_find(sq, fname);
 last_success = sq;
 if (s2) {  return s2; }
 free(last_success); /* failed */
 last_success = NULL;
 }
 
 dirp = opendir(sdir);	  /* open directory */
 if (!dirp) {
 	/* can't read directory or something, not found */
	printf("file_find: error in directory read %s\n", sdir);
	closedir(dirp);
	return NULL; }
 while ((dp = readdir(dirp)) != NULL) {
  /* printf("dp->d_name: %s\n", dp->d_name); */
  if (strcmp(dp->d_name, ".") == 0) continue;
  if (strcmp(dp->d_name, "..") == 0) continue;
  
  if (dp->d_namlen == n && !strcmp(dp->d_name, fname)) {
   closedir(dirp);  		/* got it !, make the whole name */
   mq = n + strlen(sdir) + 1;
   sq = (char *) malloc(mq);
   last_success = strdup(sdir);
   strcpy(sq, sdir);  strcat(sq, fname);
   /* fprintf(yyout,";got it, final name = %s\n", sq); */
   return sq; }
  
  /* append the file */
  mq = dp->d_namlen + strlen(sdir) + 2;
  sq = (char *) malloc(mq);
  strcpy(sq, sdir);  strcat(sq, dp->d_name);

  /* stat the combined file name and check if a directory */

  if( (stat(sq, &statbuf)) != 0) {
   fprintf(yyout,";stat error for file: %s\n", sq);
  } else
  if ( (statbuf.st_mode & S_IFMT) == S_IFDIR) {  /* got a directory */
   /* add a '/' to the name  */
   strcat(sq, "/");
   /* printf("got a directory: %s, fname = %s\n", sq, fname); */
   /* and call ourselves for the next level */
   s2 = file_find(sq, fname);
   /* did we find it down there ? */
   if (s2) { free(sq);  closedir(dirp);  return s2; }
  }
  /* no joy, free sq */
  free(sq);
 }
  
 closedir(dirp);
 return NULL;
 }
 /*------------------------------------------------------------------------- */
char *seq_file(s)
 char *s;
 {
 char	*s1, *fname;
 int	n, mq;
 /* make a file name from a sequence name */
 if (search_flag) {
 mq = strlen(s) + strlen(sequence_suffix) + 1;
 s1 = (char *) malloc(mq);
 strcpy(s1, s);
 if (!strstr(s1, sequence_suffix))  strcat(s1, sequence_suffix);
 fname = file_find(sequence_top_path, s1);
 free(s1);
 } else {
 /* construct the name with a seq directory assumed */
 FILE	*fin;
 mq = strlen(sequence_top_path)+strlen("/seq/")+strlen(s)+strlen(sequence_suffix)+1;
 fname = (char *) malloc(mq);
 strcpy(fname, sequence_top_path);
 strcat(fname, "/seq/"); strcat(fname, s); strcat(fname, sequence_suffix);
 if ((fin=fopen(fname,"r")) == NULL) {
  printf("can't open seq file %s\nfull name: %s\n", s, fname);
  /* if we can't open it, return a NULL */
  free(fname);  fname = NULL;  fclose(fin);
 }
 fclose(fin);
 }
 if (fname == NULL)
 	/* changed error to go to stdout, 4/3/98 */
 	/* fprintf(yyout,";could not find sequence file: %s\n", s); */
 	printf(";could not find sequence file: %s\n", s);
 return fname;
 }
 /*------------------------------------------------------------------------- */
char *frame_file(s)
 char *s;
 {
 char	*s1, *fname;
 int	n, mq;
 FILE	*fin;
 /* make a file name from a frame name */
 if (search_flag) {
 mq = strlen(s) + strlen(frame_suffix) + 1;
 s1 = (char *) malloc(mq);
 strcpy(s1, s);
 if (!strstr(s1, frame_suffix))  strcat(s1, frame_suffix);
 fname = file_find(sequence_top_path, s1);
 free(s1);
 } else { 
 /* construct the name with a seq directory assumed */
 mq = strlen(sequence_top_path)+strlen("/frame/")+strlen(s)+strlen(frame_suffix)+1;
 fname = (char *) malloc(mq);
 strcpy(fname, sequence_top_path);
 strcat(fname, "/frame/"); strcat(fname, s); strcat(fname, frame_suffix);
 if ((fin=fopen(fname,"r")) == NULL) {
  printf("can't open frame file %s\nfull name: %s\n", s, fname);
  /* if we can't open it, return a NULL */
  fclose(fin);
  free(fname);  fname = NULL; exit(0);
 }
 fclose(fin);
 }
 if (fname == NULL)
 	/* changed error to go to stdout, 4/3/98 */
	/* fprintf(yyout,";could not find frame file: %s\n", s); */
	printf(";could not find frame file: %s\n", s);
 return fname;
 }
 /*------------------------------------------------------------------------- */
int read_file_list(fname, list, in, limit)
 char	*fname, **list;
 int	*in, limit;
 {
 FILE	*fin;
 char	linebuf[256], *p, *q;
 int	ns;
 if ((fin=fopen(fname,"r")) == NULL) {
  fprintf(stdout,"can't open input list file, name: %s\n", fname );
  return ERROR_RETURN; }
 /* read in each line and copy to list */
 /* modified to allow comments with a # */
 while (fgets(linebuf, 255, fin) != NULL) { 
  if ( (*in) >= limit) {
  	fprintf(stdout,"too many names in input file!, name: %s\n", fname );
	return ERROR_RETURN; }
  if (q = strpbrk(linebuf, "#;\n")) *q = NULL;
  if (q = strstr(linebuf,"//")) *q = NULL;
  q = linebuf;
  if (*q == NULL) continue;
  /* remove any blanks/tabs */
  ns = strspn(q, " \t");   q = q + ns;
  ns = strcspn(q, " \t");  *(q+ns) = NULL;
  if (*q == NULL) continue;
  /* printf("q = %s\n", q); */
  p = seq_file(q);
  if (!p) { printf("error in read_file_list\n");  return ERROR_RETURN; }
  list[(*in)] = p;
  (*in)++;
 }
 fclose(fin);
 return SUCCESS_RETURN;
 }
 /*------------------------------------------------------------------------- */
FILE	*seq_output_file(fname, flag)
 char	*fname;
 int	flag;
 {
 /* modify the name input (a *.useq file name) to a *.seq name and open
 a file for write */
 FILE	*fout;
 char	*s, *p;
 
 s = strsave(fname);
 p = strstr(s, sequence_suffix);
 if (!p) { 
  fprintf(stdout,"fatal error - illegal sequence file name?: %s\n", fname );
  exit(0); }
 *p = NULL;
 /* now replace with just .seq, note that this assumes the new suffix is not
 longer than the old ! */
 strcat(s, ".seq");
 /* if the flag is 2, we also strip the path to make a local file (for testing, etc) */
 if (flag == 2) {
 	/* put on the current directory by stripping any path */
	p = strrchr(s, '/');	if (*p) p++; else p = s;
 	/* make sure we still have something */
	if (*p == NULL) {
	fprintf(stdout,"fatal - problem with output seq file, name: %s\n", p );
	exit(0); }
	}
	else p = s;
 /* now open the file */
 if ((fout=fopen(p,"w")) == NULL) {
  fprintf(stdout,"fatal - can't open output seq file, name: %s\n", p );
  exit(0); }
 free(s);
 return fout;
 }
 /*------------------------------------------------------------------------- */
main (argc, argv)
 int	argc;
 char	*argv[];
 {
#define INPUT_LIMIT 200
 Symbol *sp;
 int	n_input_files = 0, n, i, input_flag, id_flag = 0, show_sym_flag = 0;
 int	r_flag = 0, lst_flag = 0, x_flag = 0, j;
 char	*inputs[INPUT_LIMIT];	/* allow up to INPUT_LIMIT input files */
 char	*fname, *sname, *seq_file(), *p;
 FILE	*symout = yyout, *seq_output_file(),*yysave;
 void list_uncompiled_seq();
 progname = argv[0];
 echo_flag = 0;

 /* set the path */
 p = getenv("TOPS_PATH");
 if (p) sequence_top_path = p;		/* there is a default if no environ. */
 
 /* check if any command line arguments */
 if (argc>=2) {
  /* go through entire list checking for a -e or -o and process these first */
  for (i=1;i<argc;i++) {
   if (argv[i][0] == '-') {
    switch (argv[i][1])
    {
    case 'V':
    case 'v':
    	if (argv[i][2]) version_number = strtol(&argv[i][2],NULL,0);
	 else { version_number = strtol(argv[i+1],NULL,0); i++; }
	 /*fprintf(stdout,"version_number = %d\n", version_number); */
    	version_input_flag = 1;
	break;
    case 'B':		/* b for branch search, normally off */
    case 'b': search_flag = 1;	break;
    case 'E':
    case 'e': echo_flag = 1;	break;
    case 'F':
    case 'f': fatal_flag = 1;	break;
    case 'X':
    case 'x': x_flag = 1;
    	      if (argv[i][2] == 'L' || argv[i][2] == 'l') x_flag = 2;
    	      break;
    case 'T':
    case 't': id_flag = 1;	break;
    case 'A':
    case 'a': show_sym_flag = 1;	break;
    case 'R':
    case 'r': r_flag = 1;	break;
    case 'i':
    case 'I': /* input file specified, make sure we have a name here */
     if ( (i+1) >= argc || argv[i+1][0] == '-') {
     fprintf(stdout,"no input file name after -i key (or it begins with a -)\n");
     fprintf(stdout,"%s fatal error\n", progname);
     exit(0); }
     /* read in now */
     if (read_file_list(argv[i+1], inputs, &n_input_files, INPUT_LIMIT)) {
       i++;	/* bump i an extra */
       /* debug, list them */
       for (j=0;j<n_input_files;j++) printf("%d name: %s\n", j, inputs[j]);
       break;
       }
     else { fprintf(stdout,"%s fatal error\n", progname);
     exit(0); }
    case 'o':
    case 'O': /* output file specified, make sure we have a name here */
     if ( (i+1) >= argc || argv[i+1][0] == '-') {
     fprintf(stdout,"no output file name after -o key (or it begins with a -)\n");
     fprintf(stdout,"%s fatal error\n", progname);
     exit(0); }
     /* try to open it */
     if ((yyout=fopen(argv[i+1],"w")) == NULL) {
     fprintf(stdout,"%s fatal error\n", progname);
     fprintf(stdout,"can't open output file, name: %s\n", argv[i+1]);
     exit(0); }
     i++;	/* bump i an extra */
     break;
    case 'l':
    case 'L': /* lst file specified, make sure we have a name here */
     if ( (i+1) >= argc || argv[i+1][0] == '-') {
     fprintf(stdout,"no lst file name after -l key (or it begins with a -)\n");
     fprintf(stdout,"%s fatal error\n", progname);
     exit(0); }
     /* try to open it */
     if ((flst=fopen(argv[i+1],"w")) == NULL) {
     fprintf(stdout,"%s fatal error\n", progname);
     fprintf(stdout,"can't open output file, name: %s\n", argv[i+1]);
     exit(0); }
     i++;	/* bump i an extra */
     lst_flag = 1;
     break;
    case 's':
    case 'S': /* output file specified for symbols, make sure we have a name here */
     if ( (i+1) >= argc || argv[i+1][0] == '-') {
     fprintf(stdout,"no output file name after -s key (or it begins with a -)\n");
     fprintf(stdout,"%s fatal error\n", progname);
     exit(0); }
     /* try to open it */
     if ((symout=fopen(argv[i+1],"w")) == NULL) {
     fprintf(stdout,"%s fatal error\n", progname);
     fprintf(stdout,"can't open symbol output file, name: %s\n", argv[i+1]);
     exit(0); }
     i++;	/* bump i an extra */
     break;
    default:
     fprintf(stdout,"%s warning\n", progname);
     fprintf(stdout,"bad command line key %s, ignored\n", argv[i]);
     break;
    }
   } else { /* assume an input file, load the name */
   /* note that we also load this list now from the -i input_file option */
   inputs[n_input_files++] = strsave( argv[i]);
  }
 } }

 /* done with argument checking */
 /* input files, could be stdin or a list */

 if (n_input_files == 0) { input_flag = 0; n_input_files = 1; } else {
 	input_flag = 1; }
 
 /* if the t option is set, we read the files defining the frames, sequences,
 and targets first, this is controlled by the id_flag */
 
 if (id_flag)  if (read_id_tables() == 0) exit (0); /* read tables */

 /* show symbol usage for debug*/
 /*
 fprintf(symout,"\n;All symbols read in (used or not):\n\n");
 dump_list(&symlist, symout, 1);
 */

 /* now read in the actual sequence files */
 printf(";starting the seq file reads, n_input_files = %d\n", n_input_files);
 for (i=0; i < n_input_files; i++) {
 if (input_flag) {
  /* setup this input file (default is standard input) */
   printf(";%d: name: %s\n", i, inputs[i]);
   if ((yyin=fopen(inputs[i],"r")) == NULL) {
       fprintf(stdout,"%s fatal error\n", progname);
       fprintf(stdout,"list, can't open input file, name: %s\n", inputs[i]);
       perror("problem with input file");

       exit(0); }
   /* if we have the x flag set, these go to separate output files */
   if (x_flag) {yysave = yyout; yyout = seq_output_file(inputs[i], x_flag);}
   fprintf(yyout, ";starting on input file: %s\n", inputs[i]);
   sid = get_sid(inputs[i]);	/* get the sid from file name */
  } else {
   fprintf(yyout, ";reading from stdin\n");
   sid = -1;	/* fake sid for stdin */
  }
  set_sid(sid);	/* this writes out the SID operator */
  main_type_flag = 0;	/* if sequence is main, it will reset to 1 */
  lineno = 0;
  yyparse();
  fclose(yyin);
  if (input_flag) {
  fprintf(yyout,"\n;%d lines compiled in %s\n", lineno, inputs[i]);
  } else {
  fprintf(yyout,"\n;%d lines compiled from stdin\n", lineno);
  }
  /* scan the label list for undefined goto's */
  for (sp = lablist; sp != (Symbol *) 0; sp = sp->next)
    if ( sp->type != 1) {
     fprintf(stdout,";undefined GOTO destination: %s\n", sp->name); }
 
  fprintf(yyout,"\n;Labels used:\n\n");
  dump_list(&lablist, yyout, 0);
 
  /* now zap the label list for this sequence (this may not be what we really
  want to do, depends on how Mathur's code works) */
  
  zap_list(&lablist);
  
  fprintf(yyout,";There were %d temps used out of %d\n", highest_temp_used,
   NTEMPS);
  /* close if a separate output file */
  if (input_flag && x_flag) { fclose(yyout);  yyout = yysave; }
 }
 /* all input files now compiled, check for referenced sequences */

 /*
 fprintf(stdout,"\n;SEQUENCE symbols used:\n\n");
 dump_list(&seq_list, stdout, 1);

 printf("uncompiled list\n");
 list_uncompiled_seq();
 */

 if (r_flag) {			/* if r_flag is set */

 while ( sname = find_uncompiled_seq() ) {
 fprintf(yyout,";uncompiled sequence file: %s\n", sname);
  if ((fname = seq_file(sname)) == NULL) {
 	fprintf(yyout,";WARNING - no such sequence file: %s\n", sname);
  }
  if ((yyin=fopen(fname,"r")) == NULL) {
      fprintf(stdout,"%s fatal error\n", progname);
      fprintf(stdout,"can't open input file, name: %s", fname);
      exit(0); }
  /* if we have the x flag set, these go to separate output files */
  if (input_flag && x_flag) {
  	yysave = yyout; yyout = seq_output_file(fname, x_flag); }
  fprintf(yyout, ";starting on input file: %s\n", fname);
  sid = get_sid(fname);	/* get the sid from file name */
  set_sid(sid);	/* this writes out the SID operator */
  main_type_flag = 0;	/* if sequence is main, it will reset to 1 */
  lineno = 0;
  yyparse();
  fprintf(yyout,"\n;%d lines compiled in %s\n", lineno, fname);
  fprintf(yyout,"\n;Labels used:\n\n");
  fclose(yyin);
  free(fname);		/* free the string */
  dump_list(&lablist, yyout, 0);
  zap_list(&lablist);
  fprintf(yyout,";There were %d temps used out of %d\n", highest_temp_used,
   NTEMPS);
  if (input_flag && x_flag) { fclose(yyout);  yyout = yysave; }

 }
 }	/* end of r_flag option */

 /*list_uncompiled_seq();*/

 /* show symbol usage */
 if (show_sym_flag) {
 fprintf(symout,"\n;All symbols read in (used or not):\n\n");
 dump_list(&symlist, symout, 1);
 }
 /* list all the used names of everything */
 fprintf(symout,"\n;LISTS used:\n\n");
 dump_list_of_list( symout);
 fprintf(symout,"\n;FRAME symbols used:\n\n");
 dump_list(&frame_list, symout, 1);
 fprintf(symout,"\n;SEQUENCE symbols used:\n\n");
 dump_list(&seq_list, symout, 1);
 fprintf(symout,"\n;TARGET symbols used:\n\n");
 dump_list(&target_list, symout, 1);
 
 /* if lst_flag is set, make a lst file, this is the -lst input option */
 if (lst_flag) {
 dump_paths(&frame_list, flst);
 dump_paths(&seq_list, flst);
 fclose(flst);
 }
 if (serious_errors)
 	printf("ATTENTION! - there were %d serious errors during parsing\n"
		,serious_errors);
 else printf("No serious problems logged\n");
 }
