#!/bin/csh -f
#
# NAME:
#       setup_ana
# PURPOSE:
#       To setup environment variables and aliases for ANA use
# HISTORY
#	Created 8-Nov-96 by Dick Shine
#	17-Jul-97 (RAS) - added tracedecode
#	 8-Nov-96 (MDM) - Added header
#			- made all references relative to /tsw
#	 8-Nov-96 (RAS) - added TSW_FRAME_DIR env. var.
#	 8-Nov-96 (MDM) - Corrected typo in TSW_FRAME_DIR
#	18-Feb-97 (RAS) - added TSW_FRAME_DEV_DIR
#	5-Mar-97  (RAS) - added TSW_FRAME_OUT_DIR
#       27-Aug-98 (LHS) - source setup_ana_exe instead of alias ana
#	23-Oct-07 (ZAF) - modified for distribution
#
#setenv ANA_DIR /tsw/ana/
setenv ANA_DIR ${SSW}/packages/ana/
setenv ANA_SLIB $ANA_DIR/slib/
setenv ANA_HERSH $ANA_DIR/fonts/
setenv ANA_HELP $ANA_DIR/help/
setenv ANA_WLIB $ANA_DIR/wlib/
setenv ANA_PIXMAPS $ANA_DIR/pixmaps/
setenv ANA_MOVIE_DIR $ANA_DIR/movies/

# local definitions

# needed for SOLARB data, a SOLARB archive is also needed

setenv ANA_SOLARB_PATH /net/kokuten/archive/hinode/sot/QuickLook/
setenv ANA_SOLARB_PATH_SEC /net/kokuten/archive/hinode/sot/level0/
setenv ANA_XRT_PATH /net/kokuten/archive/hinode/xrt/QuickLook/
setenv ANA_XRT_PATH_SEC /net/kokuten/archive/hinode/xrt/level0/

# needed for SDO data, a local SDO archive is also needed

setenv ANA_SDOHMI0_PATH /net/bigdata/Volumes/cache/sdo/HMI/lev0/
setenv ANA_SDOHMI1_PATH /net/bigdata/Volumes/cache/sdo/HMI/lev1/
setenv ANA_SDOAIA0_PATH /net/bigdata/Volumes/cache/sdo/AIA/lev0/
setenv ANA_SDOAIA1_PATH /net/bigdata/Volumes/cache/sdo/AIA/lev1/

setenv TSW_DATA_DIR_FIL /tsw/dbase/cal/info/data_dirs.lst
setenv TSW_FRAME_DIR /tsw/obs/
setenv TSW_FRAME_DEV_DIR /tsw/obs_dev/
setenv TSW_FRAME_OUT_DIR /tsw/obs_dev/
setenv QTAB /tsw/dbase/tables/
setenv HTAB /tsw/dbase/tables/

# MDI mg's, SXT, La Palma thumbnails

setenv ANA_MDI_FDMG    /net/diapason/disk1/fdmg/ # location of MDI magnetogram
setenv BROWSER_SETS     /net/diapason/fdmg/  # location of browser sets
#setenv ANA_LAPALMA_THUMB_PATH /hosts/shine/data3/lapalma/stage/cat/
setenv ANA_SXT_DIR      /net/diapason/fdmg/sxt/# location of SXT images
# needed for TRACE data, a TRACE archive is also needed

#setenv ANA_TRACE_HOURLY /hosts/vestige/archive/sci/trace/tri/
setenv ANA_TRACE_HOURLY /net/vestige/Volumes/disk1/sci/trace/tri/
setenv ANA_TRACE_HOURLY_SEC /net/solserv/home/shine/trace/scratch/
setenv ANA_LAPALMA_THUMB_PATH /net/shine/data3/lapalma/stage/cat/

source $ANA_DIR/setup_ana_exe	 # set OS-dependent alias for "ana"

alias browser ana $ANA_DIR/auto/browser
alias sat ana $ANA_DIR/tools/browser
alias converter ana $ANA_DIR/auto/converter
alias dataview ana $ANA_DIR/auto/dataview
alias traceframe ana $ANA_DIR/trace_exp4
alias showframe $ANA_DIR/show_frame
alias tracedecode $ANA_DIR/trace_decode
alias traceparser $ANA_DIR/parser
alias traceparser2 $ANA_DIR/parser2
alias tracesim $ANA_DIR/trace_sim

