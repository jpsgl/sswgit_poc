;+
;
; PROJECT:  CHIANTI
;
;       CHIANTI is an atomic database package for the calculation of
;       astrophysical emission line spectra.  It is a collaborative project
;       involving Ken Dere (Naval Research Laboratory, Washington DC), 
;       Brunella Monsignori-Fossi and Enrico Landi (Arcetri Observatory, 
;       Florence), and Helen Mason and Peter Young (DAMTP, Cambridge Univ.).
;
;
; NAME:
;	ZION2FILENAME
;
; PURPOSE:
;
;
;	help locate CHIANTI database files
;
; CATEGORY:
;
;	database.
;
; CALLING SEQUENCE:
;
;       ZION2NAME, Iz, Ion, Name
;
;
; INPUTS:
;
;	Iz:  nuclear charge of ion of interest, i.e. 26 for Fe
;       Ion:   charge state of ion of interest, i.e. 2 for Fe II	
;
;
; OUTPUTS:
;
;	Name:  the generic filename for a CHIANTI database file
;
;
;
; EXAMPLE:
;
;             > zion2name,26,2,name
;             > print,name
;             > fe_2   
;
; MODIFICATION HISTORY:
; 	Written by:	Ken Dere
;	March 1996:     Version 2.0
;
;-
pro zion2name,z,ion,name
;
;  convert Z, ionization state to the name
;  i.e.     z=26  ion=24 > fe_24
;
z_lbl=['h','he','li','be','b','c','n','o','f','ne','na','mg','al','si','p','s','cl','ar','k','ca','sc','ti','v','cr','mn','fe','co','ni','cu','zn']
;
;
if z gt 0 then begin
   name=strtrim(z_lbl(z-1),2)+'_'+strtrim(string(ion,'(i2)'),2)
endif else name=''
;
;
end
