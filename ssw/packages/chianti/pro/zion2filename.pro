;+
;
; PROJECT:  CHIANTI
;
;       CHIANTI is an atomic database package for the calculation of
;       astrophysical emission line spectra.  It is a collaborative project
;       involving Ken Dere (Naval Research Laboratory, Washington DC), 
;       Brunella Monsignori-Fossi and Enrico Landi (Arcetri Observatory, 
;       Florence), and Helen Mason and Peter Young (DAMTP, Cambridge Univ.).
;
;
; NAME:
;	ZION2FILENAME
;
; PURPOSE:
;
;	help locate CHIANTI database files
;
; CATEGORY:
;
;	database.
;
; CALLING SEQUENCE:
;
;       ZION2FILENAME, Iz, Ion, Filename
;
;
; INPUTS:
;
;	Iz:  nuclear charge of ion of interest, i.e. 26 for Fe
;       Ion:   charge state of ion of interest, i.e. 2 for Fe II	
;
;
; OUTPUTS:
;
;	Filename:  the complete filename and path specification for generic
;                  CHIANTI database file, i.e. '.elvlc' suffix is not included
;
;
; RESTRICTIONS:
;
;	!xuvtop must be set
;
;
; EXAMPLE:
;
;             > zion2filename,26,2,filename
;             > print,filename
;             > /data1/xuv/fe/fe_2   assuming !xuvtop = '/data1/xuv'
;
; MODIFICATION HISTORY:
; 	Written by:	Ken Dere
;	March 1996:     Version 2.0
;       Sept  1996:     Modified for use with VMS
;
;-
pro zion2filename,z,ion,filename
;
;  convert Z, ionization state to the filename
;  i.e.     z=26  ion=24 > !xuvtop/fe/fe_24
;
z_lbl=['h','he','li','be','b','c','n','o','f','ne','na','mg','al','si','p','s','cl','ar','k','ca','sc','ti','v','cr','mn','fe','co','ni','cu','zn']
;
;
if !version.os eq 'vms' then begin
   lentop=strlen(!xuvtop)
   top=strmid(!xuvtop,0,lentop-1)
   dir=top+'.'+strtrim(z_lbl(z-1),2)
   name=strtrim(z_lbl(z-1),2)+'_'+strtrim(string(ion,'(i2)'),2)
   filename=dir+'.'+name+']'+name
endif else begin
   dir=!xuvtop+'/'+strtrim(z_lbl(z-1),2)
   name=strtrim(z_lbl(z-1),2)+'_'+strtrim(string(ion,'(i2)'),2)
   filename=dir+'/'+name+'/'+name
endelse
;
;
end


