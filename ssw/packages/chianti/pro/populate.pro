;+
;
; PROJECT:  CHIANTI
;
;       CHIANTI is an atomic database package for the calculation of
;       astrophysical emission line spectra.  It is a collaborative project
;       involving Ken Dere (Naval Research Laboratory, Washington DC), 
;       Brunella Monsignori-Fossi and Enrico Landi (Arcetri Observatory, 
;       Florence), and Helen Mason and Peter Young (DAMTP, Cambridge Univ.).
;
;
; NAME:
;	POPULATE
;
; PURPOSE:
;
;	calculate levels populations at a specific temperature and electron
;       density
;
; CATEGORY:
;
;	science.
;
; CALLING SEQUENCE:
;
;       POPULATE,T, Eden, Pop
;
;
; INPUTS:
;
;	T:   electron temperature
;       Eden:  electron density (cm^(-3) )	
;
;	
;
; OUTPUTS:
;
;	Pop:  populations of all of the ions levels
;       
;
;
;
; COMMON BLOCKS:
;
;      common elvl,mult,ecm
;      common wgfa, wvl,gf,a_value
;      common upsilon,t_type,c_ups,splups
;
;        these must be filled with the necessary data before 
;        populate can be run
;
;
; RESTRICTIONS:
;
;	see above
;
; PROCEDURE:
;
;	this is not a top level routine.
;
; EXAMPLE:
;
;             > convertname,'c_2',iz,ion
;             > print,iz,ion
;             > 6,2
;
; MODIFICATION HISTORY:
; 	Written by:	Ken Dere
;	March 1996:     Version 2.0
;
;-
pro populate,t,xne,pop
;
common elvl,mult,ecm
common wgfa, wvl,gf,a_value
common upsilon,t_type,c_ups,splups
;
;
;
;
hck=1.98648e-16/1.38062e-16
;
n_elvl=n_elements(ecm)
wsize=size(wvl)
n_wgfa1=wsize(1)
n_wgfa2=wsize(2)
usize=size(splups)
n_ups1=usize(1)
n_ups2=usize(2)
;
n_levels=min([n_elvl,n_wgfa1,n_wgfa2,n_ups1,n_ups2])
;
;
c=fltarr(n_levels,n_levels)
d=dblarr(n_levels,n_levels)
b=fltarr(n_levels)
pop=fltarr(n_levels)
;
;  population rates for A values
;
for m=0,n_levels-1 do begin
  for n=0,n_levels-1 do begin
      decm=ecm(n)-ecm(m)
    if(decm gt 0.) then begin
;        n level higher than m
;        contribution of level n to population of level m
;
      c(n,m)=c(n,m)+a_value(m,n)
;
   endif else if(decm lt 0.) then begin
;       n level lower than m
      c(m,m)=c(m,m)-a_value(n,m)
   endif
endfor  ; n
endfor  ;m
;
;
;  population rates for collision values
;
for m=0,n_levels-1 do begin
  for n=0,n_levels-1 do begin
      decm=ecm(n)-ecm(m)
    if(decm lt 0.) then begin
;        n level lower than m
;        contribution of level n to population of level m
;
      if(t_type(n,m) gt 0) then begin
         xt=t/(hck*abs(decm))
         descale_ups,n,m,xt,ups
         c(m,m)=c(m,m)-xne*8.63e-6*ups/(mult(m)*sqrt(t))
         c(n,m)=c(n,m)+xne*8.63e-6*ups*exp(-1./xt)/(mult(n)*sqrt(t))
      endif
   endif else if(decm gt 0.) then begin
;        n level higher than m
;
      if(t_type(m,n) gt 0) then begin
        xt=t/(hck*abs(decm))
        descale_ups,m,n,xt,ups
        c(m,m)=c(m,m)-xne*8.63e-6*ups*exp(-1./xt)/(mult(m)*sqrt(t))
        c(n,m)=c(n,m)+xne*8.63e-6*ups/(mult(n)*sqrt(t))
      endif
    endif
  endfor  ; n
endfor  ;m
;
;  to set total population = 1.
;
m=n_levels-1
  for n=0,n_levels-1 do begin
    c(n,m)=1.  
endfor
b(n_levels-1)=1.
;
;
pop=invert(transpose(c))#b     ; very marginally faster this way
;
      return
	end
