;+
;
; PROJECT:  CHIANTI
;
; NAME:
;	isothermal
;
; PURPOSE:
;
;       calculates an isothermal synthetic spectrum 
;
; CATEGORY:
;	
;	synthetic spectra
;
; CALLING SEQUENCE:
;
;       isothermal,Wmin,Wmax,wavestep,temp,Lambda,Spectrum,
;          List_wvl,List_ident,[/photons],[/ergs],[edensity=edensity]
;          [pressure=pressure],[/noverbose],[sngl_ion=sngl_ion]
;
; INPUTS:
;
;	Wmin:  minimum of desired wavelength range in Angstroms
;	Wmax:  maximum of desired wavelength range in Angstroms
;       Wavestep: wavelength grid for output line spectrum
;       Temp: electron temperature (or array)
;       Specify one of below:
;          Edensity: electron density in emitting region (cm^-3)
;          Pressure: electron pressure (cm^-3 K)
;          Note- these can be single valued or arrays corresponding to 
;             the specified temperatures
;       Photons: outputs units in photons cm^3 s^-1 str^-1 (default)
;       Ergs: outputs units in ergs cm^3 s^-1 str^-1
;       Noverbose: turn off printing of information
;	Sngl_ion:  specified a single ion to be used instead of the complete
;                  set of ions specified in !xuvtop/masterlist/masterlist.ions
;                  e.g. sngl_io='fe_15' for Fe XV
;
; OUTPUTS:
;
;       Lambda:  wavelength array of calculated synthetic spectrum
;       Spectrum:  intensity array (erg or photons cm^3 s^-1 str^-1)
;       List_wvl:  a list of wavelengths for use with synthetic_plot.pro
;       List_ident:  a list of line identifications for use with 
;                        synthetic_plot.pro
;
; PROCEDURE:
;
;	the user will be asked to select an abundance file and a 
;       ionization equilibrium file. For each ion in the masterlist
;       the level inof, waves, gf, A values, and observed and theoretical
;       energy levels are read in.  The level populations for each input
;       temperature/density are calculated. The spectrum is then computed
;       for each line in selected wavelegth range. The spectrum is returned
;       either in photons cm^3 s^-1 str^-1 (default) or 
;       ergs cm^3 s^-1 str^-1
;
; EXAMPLE:
;
;       > isothermal,100.,200.,.1,[1.e6,1.5e6],lambda,spectrum,
;           list_wvl,list_ident,edensity=1.e9,/photons
;
; MODIFICATION HISTORY:
; 	Written by:	Jeff Newmark  1996 October 24
;                       (derived from synthetic.pro: Dere, May 1996)
;       Modified by:    Ken Dere      1997 April 25
;                         to fit into CHIANTI
;
;-
pro isothermal,wmin,wmax,wavestep,temp,lambda,spectrum,list_wvl,list_ident,$
      pressure=pressure,edensity=edensity,ergs=ergs,photons=photons,$
      noverbose=noverbose,sngl_ion=sngl_ion
;
common elvl,mult,ecm
common wgfa, wvl,gf,a_value
common upsilon,t_type,c_ups,splups
;
; check inputs, convert if necessary
ntemps=n_elements(temp)
case 1 of
  keyword_set(edensity): begin
        if n_elements(edensity) ne 1 and n_elements(edensity) ne ntemps then $
          begin
          message,/info,'The dimensions of DENSITY must be 1 or equal to TEMPERATURE'
          return
        endif
        edensity = fltarr(ntemps)+edensity
      end
  keyword_set(pressure): begin
        if n_elements(pressure) ne 1 and n_elements(pressure) ne ntemps then $
          begin
          message,/info,'The dimensions of PRESSURE must be 1 or equal to TEMPERATURE'
          return
        endif
        edensity = pressure/temp
      end
  else: begin
       message,/info,'You must specify either the Electron Density (EDENSITY) or'+$
         ' the Pressure (PRESSURE = cm^-3 K)'
       return
      end
endcase
if keyword_set(photons) and keyword_set(ergs) then begin
      message,/info,'You cannot specify both /photons and /ergs'
      return
end else if keyword_set(ergs) then units=1 else units=0
;
log_temp = alog10(temp)

dlambda  = wavestep
nlambda  = long((wmax-wmin)/dlambda+1.)
lambda   = wmin+findgen(nlambda)*dlambda
spectrum = dblarr(nlambda,ntemps)
;
;
;   abund_name=!xuvtop+'/abundance/'+!abund_file   
path=getenv('SSW')+'/packages/chianti/abundance/'
;path=!xuvtop+'/abundance'   ; mod by kpd 4/97
abund_name = pickfile(path=path,filter='*.abund',title='Select Abundance File')
read_abund,abund_name,abund,abund_ref
;
;   ioneq_name=!xuvtop+'/ioneq/'+!ioneq_file   
path=getenv('SSW')+'/packages/chianti/ioneq/'
;path=!xuvtop+'/ioneq'    ; mod by kpd 4/97
ioneq_name = pickfile(path=path,filter='*.ioneq',title='Select Ionization Equilibrium File')
read_ioneq,ioneq_name,ioneq_t,ioneq,ioneq_ref
n_ioneq_t = n_elements(ioneq_t)
;
;
spd = ['S','P','D','F','G','H','I','K']
jvalue = ['0','1/2','1','3/2','2','5/2','3','7/2','4','9/2','5','11/2','13/2','15/2']
;
maxwvl = 10000
;
mast_z   = intarr(maxwvl)
mast_ion = intarr(maxwvl)
mast_lvl = intarr(maxwvl)
mast_wvl = fltarr(maxwvl)
mast_A   = fltarr(maxwvl)
;
mast_term1  = strarr(80,maxwvl)
mast_desig1 = strarr(80,maxwvl)
mast_term2  = strarr(80,maxwvl)
mast_desig2 = strarr(80,maxwvl)
;
mast_abund = fltarr(maxwvl)
mast_ioneq = fltarr(maxwvl)
mast_tmax  = fltarr(maxwvl)
mast_popazidem = fltarr(maxwvl,ntemps)
mast_spectrum  = lonarr(maxwvl)
;
;
mast_index = 0
;
;  open the file that has the names of the ions
;
print,'  getting contribution functions'
;
mname = !xuvtop+'/masterlist/masterlist.ions'
;
openr,lum,mname,/get_lun
;
gname = ' '
eof_lum = 0
;
;   main input and calculation loop  **************
;
while( not eof_lum) do begin
;
;   read the name of the ions
;
if (not keyword_set(sngl_ion)) then begin
   readf,lum,gname
   index=strpos(gname,';') ;  to sort out comments
;
   if index ge 0 then begin
      gname=strmid(gname,0,index-1)
      gname=strtrim(gname,2)
   endif
endif else begin
   gname=sngl_ion
endelse
;
; Set up inputs for particular ion
;
if not keyword_set(noverbose) then print,gname
convertname,gname,iz,ion
zion2filename,iz,ion,fname
wname     = fname+'.wgfa'
elvlname  = fname+'.elvl'
elvlcname = fname+'.elvlc'
upsname   = fname+'.splups'
;
;
;  read in level information, wavelengths, gf and A values from .wgfa files
;
read_wgfa2,wname,lvl1,lvl2,wvl1,gf1,a_value1,wgfaref;
;
  ntrans = n_elements(lvl1)
  nlvls = max([lvl1,lvl2])
  wvl = fltarr(nlvls,nlvls)
  gf = fltarr(nlvls,nlvls)
  a_value = fltarr(nlvls,nlvls)
  for itrans = 0,ntrans-1 do begin
    wl1 = lvl1(itrans)
    wl2 = lvl2(itrans)
    wvl(wl1-1,wl2-1) = wvl1(itrans)
    gf(wl1-1,wl2-1) = gf1(itrans)
    a_value(wl1-1,wl2-1) = a_value(wl1-1,wl2-1)+a_value1(itrans)
  endfor
;
;   does this ion have lines in the specified wavelength range?
;
anylines = where((wvl1 ge wmin) and (wvl1 le wmax))
;
if max(anylines) ge 0 then begin    ; do this ion
;
;
;
   read_elvlc,elvlcname,l1a,term,conf,ss,lla,jj,ecm,eryd,ecmth,erydth,eref
;
   g = where(ecm eq 0.)
   if(max(g) gt 0) then begin
      ecm(g) = ecmth(g)
   endif
   mult = 2.*jj+1.
;
;
   read_splups,upsname,t_type,gfu,deu,c_ups,splups,upsref
;
;  calculate level populations
;
   this_ioneq = ioneq(*,iz-1,ion-1)
   itmax = where(this_ioneq eq max(this_ioneq))
   ltmax = ioneq_t(itmax)

   popazidem = dblarr(nlvls,ntemps)
   for nt = 0,ntemps-1 do begin
       good = where(this_ioneq ne 0,gcnt)
       if gcnt eq 0 then int_ioneq = 0  else begin
         if log_temp(nt) gt max(ioneq_t(good)) or $
            log_temp(nt) lt min(ioneq_t(good)) then int_ioneq = 0 else $
              int_ioneq = 10^(spline(ioneq_t(good),alog10(this_ioneq(good)),$
                      log_temp(nt))) > 0
       endelse
       if int_ioneq ne 0 then begin
             populate,temp(nt),edensity(nt),pop
             popazidem(0,nt) = pop(*)*int_ioneq/edensity(nt)
       endif
   endfor 

   for itrans = 0,ntrans-1 do begin
      l1 = lvl1(itrans)-1
      l2 = lvl2(itrans)-1
      ww = wvl1(itrans)
;
;  can the level be excited?
      upssiz = size(splups)
      upssiz2 = upssiz(2)-1
      if(l2 le upssiz2) then begin
         maxups = max(splups(*,l2,0))
         if(maxups gt 0.) then upstst = 1 else upstst = 0
      endif else upstst = 0
;
;
      if (ww gt wmin) and (ww le wmax) and (upstst gt 0) then begin
        if(ww gt 0.) then begin   ;  select only observed wvl's
           mast_z(mast_index)   = iz
           mast_ion(mast_index) = ion
           mast_lvl(mast_index) = lvl2(itrans)
           mast_wvl(mast_index) = wvl1(itrans)
           mast_A(mast_index)   = a_value1(itrans)
           mast_abund(mast_index) = abund(iz-1)
;
           mast_tmax(mast_index) = ltmax
           mast_ioneq(mast_index) = this_ioneq(itmax)
           mast_popazidem(mast_index,*) = popazidem(l2,*)
           mast_spectrum(mast_index) = (wvl1(itrans)+dlambda/2.-wmin)/dlambda
;
;
;  get lower level designation
           term0 = strtrim(term(l1),2)
;
           term2 = ''
           blank=strpos(term0,' ')
           while blank gt 0 do begin
              term2 = term2+' '+strmid(term0,0,blank)
              term0 = strmid(term0,blank,100)
              term0 = strtrim(term0,2)
              blank = strpos(term0,' ')
           endwhile
           mast_term1(mast_index) = strlowcase(term2)
;
           jinteger = fix(2.*jj(l1))
           jstring = jvalue(jinteger)
;
           spins = strtrim(string(ss(l1),'(i2)'),2)
           mast_desig1(mast_index) = spins+spd(lla(l1))+jstring
;
;
;  get upper level designation
           term0 = strtrim(term(l2),2)
;
           term2 = ''
           blank = strpos(term0,' ')
           while blank gt 0 do begin
              term2 = term2+' '+strmid(term0,0,blank)
              term0 = strmid(term0,blank,100)
              term0 = strtrim(term0,2)
              blank = strpos(term0,' ')
           endwhile
           mast_term2(mast_index) = strlowcase(term2)
;
           jinteger = fix(2.*jj(l2))
           jstring = jvalue(jinteger)
;
           spins = strtrim(string(ss(l2),'(i2)'),2)
           mast_desig2(mast_index) = spins+spd(lla(l2))+jstring
;
           mast_index = mast_index+1
        endif
     endif
   endfor
;
endif  ;  if block for anylines
;
if keyword_set(sngl_ion) then eof_lum = 1 else eof_lum = eof(lum)
;
endwhile   ;  reading masterlist.ions
;
;   end of main input and calculation loop   ***************
;
print,'  finished getting contribution functions'
print,' number of lines=',mast_index
;
;
mast_z   = mast_z(0:mast_index-1)
mast_ion = mast_ion(0:mast_index-1)
mast_lvl = mast_lvl(0:mast_index-1)
mast_wvl = mast_wvl(0:mast_index-1)
mast_A   = mast_A(0:mast_index-1)
mast_term1  = mast_term1(0:mast_index-1)
mast_desig1 = mast_desig1(0:mast_index-1)
mast_term2  = mast_term2(0:mast_index-1)
mast_desig2 = mast_desig2(0:mast_index-1)
;
mast_abund  = mast_abund(0:mast_index-1)
mast_tmax   = mast_tmax(0:mast_index-1)
mast_ioneq  = mast_ioneq(0:mast_index-1)
mast_popazidem = mast_popazidem(0:mast_index-1,*)
mast_spectrum  = mast_spectrum(0:mast_index-1)
;
; sorting by wavelength
;
srt_index   = sort(mast_wvl)
mast_z(0)   = mast_z(srt_index)
mast_ion(0) = mast_ion(srt_index)
mast_lvl(0) = mast_lvl(srt_index)
mast_wvl(0) = mast_wvl(srt_index)
mast_A      = mast_A(srt_index)
mast_term1  = mast_term1(srt_index)
mast_desig1 = mast_desig1(srt_index)
mast_term2  = mast_term2(srt_index)
mast_desig2 = mast_desig2(srt_index)
;
mast_abund  = mast_abund(srt_index)
mast_tmax   = mast_tmax(srt_index)
mast_ioneq  = mast_ioneq(srt_index)
mast_popazidem = mast_popazidem(srt_index,*)
mast_spectrum  = mast_spectrum(srt_index)
;
list_ident  = strarr(mast_index)
list_wvl    = fltarr(mast_index)
;
nlines = mast_index
;
if not keyword_set(noverbose) then begin
 for i = 0,nlines-1 do begin
  print,mast_z(i),mast_ion(i),mast_lvl(i),mast_wvl(i),mast_A(i),mast_tmax(i),$
   format='$(3i3,f12.3,2e10.3)'
 endfor
endif
;
; Set up return values of spectrum, list_wvl, list_id
;
for i = 0,nlines-1 do begin
;
   zion2spectroscopic,mast_z(i),mast_ion(i),elstage
   wvls   = strtrim(string(mast_wvl(i),'(f12.3)'),2)
   term1  = strtrim(mast_term1(i),2)
   desig1 = strtrim(mast_desig1(i),2)
   term2  = strtrim(mast_term2(i),2)
   desig2 = strtrim(mast_desig2(i),2)
   item1  = string(mast_tmax(i),'(f4.1)')
;
;  select units of photons (default) or ergs
   if units eq 0 then hc = 1.d/(4.d*3.14159d) else $
     hc = 6.626d-27*2.998d+10*1.d+8/(4.d*3.14159d*mast_wvl(i))
;
   intensity = hc*mast_abund(i)*mast_A(i)*mast_popazidem(i,*)
   item2 = string(intensity,'(e10.2)')
   item3 = ''
   for j = 0,ntemps-1 do item3 = item3+item2(j)
;
   list_wvl(i) = mast_wvl(i)
   list_ident(i) = elstage+' '+term1+' '+desig1+' - '+term2+' '+desig2
   list_ident(i) = strpad(list_ident(i),50,/after)+'  Int='+item3+'  Tmax='+item1
;
   spectrum(mast_spectrum(i),*) = spectrum(mast_spectrum(i),*)+intensity
;
endfor
;
free_lun,lum
end

