maxframes=30
gevloc=get_gevloc_data(/merge) ;  restgenx,file=(gev)(0),gevloc
ssok=where(gevloc.helio ne '',okcnt)
which,'image2movie'
for i=okcnt-1,0,-1 do begin
   ssw_gev2eit_movie,gevloc(ssok(i)),/goes, $
      center=[0,0], fov=40, /limb, xsize=256,/last_scale,refresh=refresh
   ssw_gev2sxi_movie,gevloc(ssok(i)),/pgoes, $
      pixfov=100, refresh=refresh,$
      /limb,center=[0,0],fov=45,composite=3,/last_scale, maxframes=maxframes
endfor

end

