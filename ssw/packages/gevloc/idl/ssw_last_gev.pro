pro ssw_last_gev,fstart,fpeak, fend, class=class, $
   goes10=goes10, goes12=goes12, $
   in_progress=in_progress, status_flare=status_flare, $
    sec_url=sec_url, gev_rec=gev_rec, usefile=usefile, debug=debug

;
;+
;   Name: ssw_last_gev
;
;   Purpose: return most recent NRT flare stats from sec/noaa WWW
;
;   Input Paramters:
;      NONE
;
;   Output Parameters:
;      fstart - flare start time
;      fpeak  - flare peak time (current time if in rise phase)
;      fend   - flare end time  (current time if still in progress)
;
;   Keyword Parameters:
;      goes10 - (switch) - if set, use GOES 10 params
;      goes12 - (switch) - if set, use GOES 12 params
;      class -  (output) - GOES class at t(fpeak)
;      in_progress (output) - >0 =flare in progress (fend=current UT)
;         value=2->rise phase value=1->cooling/post-peak
;
;      status_flare (output) - if set(1), indicates flare reported - a value of
;                              0 indicate no flare so output params R undefined
;
;   Calling Sequence:
;      ssw_last_gev, fstart, fpeak, fend [,class=class] [,/goes{10,12}]
;            [,in_progress=in_progress] [,status_flare=status_flare ]
;
;   History:
;      22-Jan-2005 - S.L.Freeland - speed up SEC->SSW latest events
;
;   Method:
;      sock_copy, usual ssw-gen suspects
;
;-
status_flare=0
debug=keyword_set(debug)
if not keyword_set(sec_url) then sec_url= $
   'http://www.sec.noaa.gov/rt_plots/xray_1m.html'

if file_exist(usefile) then begin
   out=rd_tfile(usefile)
endif else begin 
   sock_list,sec_url,out
endelse
out=strcompress(strtrim(strupcase(out),2),/remove_all)
ssstats=where( (strlen(out) le 6) and (strpos(out,'<') eq -1 and strpos(out,'..') eq -1),scnt)
if scnt eq 0 then begin 
   box_message,'No flare currently reported'
   return
endif

; determine file/plot current time
ss=where(strpos(out,'_1M_') ne -1,sscnt)
if sscnt eq 0 then begin 
   box_messsage,'Cannot deterimine plot time(!)'
   return
endif

fnam=strextract(out(ss(0)),'_1M_','.')
ptim=strmid(fnam,8,4)
plottime=file2time(strmid(fnam,0,8)+'_'+ptim,/ecs)
plotday=anytim(plottime,/date_only,/ecs)
yestday=reltime(plotday,days=-1,out='ecs',/date_only)

stats=out(ssstats)
gss=[0,1,4,5,8,9]+ (keyword_set(goes10)*2)
retstat=stats(gss)
strtab2vect,retstat,fstart,scl,fpeak,class,fend,ecli ; vect[n]->nscalars

; handle observed transient value of '0000'
fpeak=([fpeak,'*****'])( (fpeak eq '0000') and (fend eq '*****'))
;
; handle day boundries...
fstart=strmid(fstart,0,2)+':'+strmid(fstart,2,2) + ':00 ' + $
   ([plotday,yestday])(fstart gt ptim)
fpeak= strmid(fpeak, 0,2)+':'+strmid(fpeak, 2,2) + ':00 ' + $
   ([plotday,yestday])(fpeak gt ptim)
fend=  strmid(fend,  0,2)+':'+strmid(fend,  2,2) + ':00 ' + $
   ([plotday,yestday])(fend gt ptim)

in_progress=(strpos(fpeak,'**') ne -1) +  (strpos(fend,'**') ne -1)

status_flare=strpos(fstart,'**') eq -1   ; something found/returned

if debug then stop
return
end



