lastn=1

set_logenv,'plot_goes_goes8','1'
set_logenv,'use_goes12','1'

gevloc=get_gevloc_data(/merge)
gevloc=last_nelem(gevloc,lastn)
ssok=where(gevloc.helio ne '',okcnt)
for i=okcnt-1,0,-1 do begin
   ssw_gev2eit_movie,gevloc(ssok(i)),/goes, $
      center=[0,0], fov=40, /limb, xsize=256,/last_scale,/refresh,/goes12
   ssw_gev2sxi_movie,gevloc(ssok(i)),/pgoes, $
      /refresh,$
      /limb,center=[0,0],fov=45,composite=3,/last_scale, maxframes=30, pixfov=100
      ;img_code='FL'
endfor

end

