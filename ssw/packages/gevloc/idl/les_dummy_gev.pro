pro les_dummy_gev, time0, time1, new_gev, save_gen=save_gen, append_gen=append_gen, use_input=use_input, goes8=goes8, ascii=ascii,goes12=goes12, $
show=show, debug=debug, last=last,remove=remove, class=class, $
current_contents=current_contents, flare_remove=flare_remove, _extra=_extra
;
;+
;   Name: les_dummy_gev 
;
;   Purpose: make "fake" SEC/NOAA events in 'gev' format
;
;   Input Parameters:
;      time0, time1 - time range to consider
;
;   Keyword Parameters:
;      save_gen - if set, save record in local genx file
;      ascii - if set, use ascii SEC xray dbase (default is binary)
;      goes8/goes12 - optional xray dbase (default is goes10)
;      use_input - if set, use time0/time1 verbatim (default is from xray LC data)
;      show - if set, just 'more out' current dummy gev dbase contents to tty
;      remove - if set, remove last nn elements from dbase
;      flare_remove - if set, remove gev data periods within the input
;                     time range (via ssw_gxd_remove_flares.pro) - useful
;                     for seperating underlying LDE from cotemporal larger
;                     flares
;       _extra - inheritance -> ssw_gxd_remove_flares.pro
;
;   Method:
;      look at local GOES xray dbase and derive start time, peak time, end time
;      and GOES class
;
;   Rationale: often, the SEC/NOAA events listing has long lag compared to 
;      $SSWDB goes lightcurve dbase; these 'dummy' records permit gev-like 
;      processing for things like SolarSoft Latest Events on NRT LC data.
;
;   History: 
;      Circa Jan 1, 2003 - S.L.Freeland - to speed up latest_events pipeline
;      20-Oct-2003 - add /SHOW keyword and function
;      17-Feb-2005 - add REMOVE keyword and function
;                    permit PEAK and CLASS input (don't read gxd then)

debug=keyword_set(debug) or n_params() lt 2

fname='$SSW/site/idl/util/dummygev.geny' ; local dummy gev data file

show=keyword_set(show) or keyword_set(last) or keyword_set(remove)

if show then begin 
   restgenx,file=fname,dumgev
   current_contents=dumgev
   nrecs=n_elements(dumgev)
   case 1 of 
      keyword_set(remove): nshow=remove
      keyword_set(last):   nshow=last
      else: nshow=n_elements(dumgev)
   endcase
   decode_gev,dumgev,f0,f1,fp,class=cl,out='ecs'
   retval=f0+' ' + f1 + ' ' + fp + ' ' + cl 
   box_message,['========================',last_nelem(retval,nshow)]
   if keyword_set(remove) then begin
     box_message,'Removing..'
     newrecs=dumgev(0:nrecs-remove-1)
     savegenx,file=fname,newrecs,/overwrite
stop
   endif
   return
endif

append_gen=keyword_set(append_gen)
use_input=keyword_set(use_input)
save_gen=keyword_set(save_gen) or append_gen
levt=last_nelem(get_gevloc_data())

if n_elements(time0) eq 0 then begin 
   time_window,levt.fstop,xxx,time0,min=20
   time1=reltime(/now,out='vms')
endif
goes8=keyword_set(goes8)
goes12=keyword_set(goes12) 
goes10=(1-goes8) and (1-goes12)

case 1 of 
   keyword_set(ascii): rd_goesx_ascii, time0, time1, gxd, $
      goes8=goes8, goes10=goes10,goes12=goes12
   else: rd_gxd,time0, time1, gxd, /one_min, goes10=goes10, goes8=goes8,goes12=goes12
endcase

if keyword_set(flare_remove) then begin
   box_message,'Removing sec flares from gxd data...'
   newgxd=ssw_gxd_remove_flares(gxd,   _extra=_extra)
   help,gxd,newgxd
   gxd=newgxd
endif

if not data_chk(gxd,/struct) then begin 
   box_message,'No GOES XRS data since last GEV event'
   return
endif

sspeak=(where(gxd.lo eq max(gxd.lo)) )(0)

dlo=[0,deriv_arr(gxd.lo)]

rstart=reverse(dlo(1:sspeak))

startss=(where(rstart lt 0))(0)
ssstart=(sspeak-startss)

if n_elements(dlo) eq sspeak+1 then sstop=n_elements(gxd)-1 else $
sstop=(where(dlo(sspeak+1:*) ge 0))(0)
if sstop eq -1 then sstop=n_elements(gxd)-1 else $
   sstop=sspeak+sstop < (n_elements(gxd)-1)

rd_gev,reltime(days=-15),reltime(/now),gev
new_gev=gev(0)
if ssstart lt n_elements(gxd) then newint=anytim(gxd(ssstart),/int) else $
   newint=anytim(gxd(0),/int)
new_gev.time=newint.time
new_gev.day=newint.day
new_gev.peak=abs(ssw_deltat(new_gev,ref=gxd(sspeak>0),/sec))
new_gev.duration=abs(ssw_deltat(new_gev,ref=gxd(sstop(0)),/sec))
if use_input then begin 
   new_gev.time=gxd(0).time
   new_gev.peak=ssw_deltat(new_gev,gxd(sspeak))
   new_gev.duration=ssw_deltat(new_gev,time1)
endif
if keyword_set(class) then bclass=byte(class) else $
   bclass=byte(goes_value2class(gxd(sspeak)))

new_gev.st$class=bclass

decode_gev,new_gev,aa,bb,cc,class=cl,out='ecs'
box_message,aa+' ' + bb + ' ' + cc + ' ' + cl
if debug then stop,'presave'
if keyword_set(save_gen) then begin 
   restgenx,file=fname,old
   new_gev=[old,new_gev]
   if n_elements(new_gev) gt n_elements(old) then begin 
      savegenx,file=fname,new_gev,/overwrite
   endif else begin 
      box_message,'Problem with addition, existing file not changed'
   endelse
endif 

return
end




