pro ssw_gev2sxi_movie,gevloc, parent_path=parent_path , $ 
   window_minutes=window_minutes, pixfov=pixfov, refresh=refresh, $
   pgoes=pgoes, _extra=_extra, debug=debug, goesn=goesn,$
   img_code=img_code, wavelnth=wavelnth, maxframes=maxframes

debug=keyword_set(debug)
if n_elements(goesn) eq 0 then goesn=12
sgoesn=strtrim(goesn,2)

goes=keyword_set(pgoes)

if not required_tags(gevloc,'date_obs,ename,helio,xcen') then begin 
   box_message,'Routine requires record from ssw_gev_locator'
   return
endif

if gevloc.helio eq '' then begin 
   box_message,'This event not yet located (no image data?)'
   return
endif

if not data_chk(movie_dir,/string) then $
   parent_path='/net/diapason/www1/htdocs/solarsoft/latest_events_summary'

movie_dir=strtrim(strcompress(concat_dir(parent_path,gevloc.ename),/remo),2)
if n_elements(pixfov) eq 0 then pixfov=128

if not file_exist(movie_dir) then spawn,['mkdir','-p',movie_dir],/noshell

movie_name=strtrim(strcompress(gevloc.ename+'_sxilm',/rem),2)
makeit=keyword_set(refresh) or $
   1-file_exist((concat_dir(movie_dir,movie_name+'.html')))
if not makeit then begin 
   box_message,[ 'Flare location movie already made for ' + gevloc.ename, $
                 'Use /REFRESH to force update']
   return
endif else if debug then stop,'makeit'

if not keyword_set(window_minutes) then window_minutes=[-15,15]

time_window,[gevloc.fstart,gevloc.fstop],mt0,mt1,minute=window_minutes

sxif=sxi_high_cadence(mt0,mt1,/l1q,/return_files,$
   wavelnth=wavelnth, img_code=img_code,maxframes=maxframes)
if sxif(0) eq '' then begin 
   box_message,'No image data for this event (fell out of queue?)
   return 
endif
nsxi=n_elements(sxif)

if nsxi lt 2 then begin 
   box_message,'Not enough images for a movie, returning
   return
endif

;mreadfits,sxif,iindex,idata
;ssw_register,iindex,idata,index,data,/correl,roll=0     ; sxi needs registration

box_message,'Processing ' + strtrim(nsxi,2) + ' SXI images..'
mreadfits_sxig12,sxif,index,data,/register,/composite,/column

;sdata=safe_log10(index,data>.1,/byte)
sdata=ssw_sigscale(index,data,/log,/corner)
ssw_fill_cube,sdata,window=1
dindex=gevloc
dindex=add_tag(dindex,index.cdelt1,'cdelt1')
dindex=add_tag(dindex,index.cdelt2,'cdelt2')
dindex=add_tag(dindex,pixfov,'naxis1')
dindex=add_tag(dindex,pixfov,'naxis2')
refdata=bytarr(pixfov,pixfov)
index2map,dindex,refdata,ref_map                    ; make a reference map

index2map,index,sdata,smaps
if debug then stop,'presub
estat=execute('sub_map,smaps,fovmaps, ref_map=ref_map,/preserve')
if not estat then begin
  box_message,'WARNING - Limb Region - Fix /PRESERVE problem'  
  ;box_message,'problem with /PRESERVE - trying again without it...
  ;estat=execute('sub_map,smaps,fovmaps, ref_map=ref_map')
   if not estat then begin
      box_message,'Cannot make movie due to sub_map problem, returning...' 
      return
   endif
endif

if debug then stop,'postsub
fddata=cube_edit(sdata,/data,ss=lindgen(data_chk(sdata,/nimag)),nx=256)
fovdata=cube_edit(fovmaps.data,/data,ss=lindgen(data_chk(fddata,/nimag)),nx=256)

if debug then stop,'fovmaps'
map2index,add_tag(fovmaps,0.,'roll'),fovii,fovdd 
fddata=ssw_fov_context_cube(fovii,fovdd,margin=.05,$
   fdmap=smaps,xsize=256,_extra=_extra,composite=3)

movie_data=[fddata,bytscl(fovdata)]
time_window,index,gt0,gt1
rd_gxd,gt0,gt1,gxx,/one,/goes10
ascii=1-data_chk(gxx,/struct)
ascii=ascii or ssw_deltat(gt0,ref='7-apr-2003') gt 0 

if goes then begin 
   event_movie,index,data=temporary(movie_data),$
      movie_data,/goes,/reverse ,ascii=ascii
endif
if debug then stop,'movie_dir
 
set_plot,'z'
trace_colors2,171,r,g,b
image2movie,movie_data,r,g,b,html=html,uttimes=index, $
 movie_dir=movie_dir,movie_name=movie_name,/java,/inctime,thumbsize=200, $
 speed=8, gamma=.7, table=3
 
file_append,movie_name+'.dat',html,/new

if debug then stop,'maps,fovmaps'
return
end

