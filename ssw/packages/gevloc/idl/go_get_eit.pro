pro go_get_eit, time0, time1, outdir=outdir, waves=waves, $
   cadence_hours=cadence_hours, hour_window=hour_window, $
   nodelete_l0=nodelete_l0
;+
;   Name: go_get_eit
; 
;   Purpose: get level0 eit from server->level1->localfits
;
;   Input Parameters:
;      time0 - time vector or start time of range
;      time1 - optional stop time of desired range
;
;   Keyword Parameters:
;      outdir - desired path for output level1 FITS files
;      waves  - desired wavelengths - default is high cadence (usually 195)
;      cadence_hours - for ranges, limit cadence
;      nodelete_l1 - if set, don't delete level zero data after L1 write      
;
;- 
;
; initialize params
if keyword_set(outdir) eq 0 then outdir=curdir()
if n_elements(waves) eq 0 then waves=str2arr('171,195,284,304')    
if n_elements(cadence_hours) eq 0 then cadence_hours=1        ; max cadence
deletel0=1-keyword_set(nodelete_l0)      

if n_elements(time1) eq 0 then time1=time0 
if n_elements(hour_window) eq 0 then hour_window=.2

; ---- l0 (eit server@gsfc) -> l1 (local) loop
for tt=0,n_elements(time0)-1 do begin                     ; for each time/range
   time_window,[anytim(time0(tt),/ecs),anytim(time1(tt),/ecs)],t0,t1,$
          hours=hour_window 
      for w=0,n_elements(waves) -1 do begin                     
        eit_closest_fd,t0,t1,file_before,file_after, $
           window_minute=360,status=status,wave=waves(w),allfiles=allfiles
        if allfiles(0) ne '' then begin 
        ehc=allfiles
        gss=grid_data(file2time(ehc,out=int),hours=cadence_hours,/ss)        
        eitl0=concat_dir(outdir,ssw_strsplit(ehc,'/',/tail,/last))
        if ehc(0) eq '' then delvarx,eitl0      
         for i=0, n_elements(eitl0)-1 do begin           
            box_message,'Processing: ' + ehc(i)
            sock_copy,ehc(i),out_dir=outdir         
            read_eit,eitl0(i),index,data                 
            eit_prep,index,data=data,oindex,odata           
            newpre='eitp_'+strtrim(index(0).wavelnth,2)+'_'
            pfile=str_replace(eitl0(i),'efz',newpre(0))     
            mwritefits,oindex,odata,outfile=pfile+'.fits'           
            if deletel0 then ssw_file_delete,eitl0(i)                          
         endfor
         endif 
      endfor
endfor

end

