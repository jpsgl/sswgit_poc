pro ssw_gev2eit_movie,gevloc, parent_path=parent_path , $ 
   window_minutes=window_minutes, pixfov=pixfov, refresh=refresh, $
   goes=goes, _extra=_extra, debug=debug, xsize=xsize

debug=keyword_set(debug)
if n_elements(xsize) eq 0 then xsize=256
goes=keyword_set(goes)

if not required_tags(gevloc,'date_obs,ename,helio,xcen') then begin 
   box_message,'Routine requires record from ssw_gev_locator'
   return
endif

if gevloc.helio eq '' then begin 
   box_message,'This event not yet located (no image data?)'
   return
endif

if not data_chk(movie_dir,/string) then $
   parent_path='/net/diapason/www1/htdocs/solarsoft/latest_events_summary'

movie_dir=strtrim(strcompress(concat_dir(parent_path,gevloc.ename),/remo),2)
if n_elements(pixfov) eq 0 then pixfov=128

if not file_exist(movie_dir) then spawn,['mkdir','-p',movie_dir],/noshell

movie_name=strtrim(strcompress(gevloc.ename+'_lm',/rem),2)
makeit=keyword_set(refresh) or $
   1-file_exist((concat_dir(movie_dir,movie_name+'.html')))
if not makeit then begin 
   box_message,[ 'Flare location movie already made for ' + gevloc.ename, $
                 'Use /REFRESH to force update']
   return
endif else if debug then stop,'makeit'

if not keyword_set(window_minutes) then window_minutes=[-45,30]

time_window,[gevloc.fstart,gevloc.fstop],mt0,mt1,minute=window_minutes

;  e195=sswdb_files(mt0,mt1,/l1q,pat='_195_')
e195=eit_high_cadence(mt0,mt1,/l1q,high_wave=hwave,/return_files)
if e195(0) eq '' then begin 
   box_message,'No image data for this event (fell out of queue?)
   return 
endif

if n_elements(e195) lt 2 then begin 
   box_message,'Not enough images for a movie, returning
   return
endif

mreadfits,e195,index,data
;sdata=bytscl(do_eit_scaling(data,/log,/no_prep))
sdata=ssw_sigscale(index,data,/log,/corner)
ssw_fill_cube,sdata
dindex=gevloc
dindex=add_tag(dindex,index.cdelt1,'cdelt1')
dindex=add_tag(dindex,index.cdelt2,'cdelt2')
dindex=add_tag(dindex,pixfov,'naxis1')
dindex=add_tag(dindex,pixfov,'naxis2')
refdata=bytarr(pixfov,pixfov)
index2map,dindex,refdata,ref_map                    ; make a reference map


index2map,index,sdata,emaps
index2map,index,data,efmaps
sub_map,efmaps,fovmaps, ref_map=ref_map,/preserve
sub_map,emaps,ffovmaps,ref_map=ref_map,/preserve
map2index,fovmaps,fovii,fovdd
map2index,ffovmaps,ffovii,ffovdd
fovdd=fovdd>0
ssw_fill_cube,fovdd,/track
fovdd=ssw_sigscale(fovii,fovdd,/log)
fddata=cube_edit(sdata,/data,ss=lindgen(data_chk(sdata,/nimag)),nx=xsize)
fovdata=cube_edit(fovdd,/data,ss=lindgen(data_chk(fddata,/nimag)),nx=xsize)

if debug then stop,'fovmaps'
map2index,add_tag(fovmaps,0.,'roll'),fovii,fovdd 
fddata=ssw_fov_context_cube(ffovii,ffovdd,margin=.05,$
   fdmap=emaps,xsize=xsize,_extra=_extra,/goes12)

movie_data=[fddata,bytscl(fovdata)]
time_window,index,gt0,gt1
rd_gxd,gt0,gt1,gxx,/one,/goes10
ascii=1-data_chk(gxx,/struct)
ascii=ascii or ssw_deltat(gt0,ref='7-apr-2003') gt 0

help,ascii

if goes then begin 
   event_movie,index,data=temporary(movie_data),$
      movie_data,/goes,/reverse,ascii=ascii
endif
if debug then stop,'movie_dir
 
set_plot,'z'
eit_colors,hwave,r,g,b
image2movie,movie_data,r,g,b,html=html,uttimes=index, $
 movie_dir=movie_dir,movie_name=movie_name,/java,/inctime,thumbsize=200, $
 speed=8
 
file_append,movie_name+'.dat',html,/new

if debug then stop,'emaps,fovmaps'
return
end

