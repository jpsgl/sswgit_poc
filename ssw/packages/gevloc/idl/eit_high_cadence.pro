function eit_high_cadence, time0, time1, $
      high_wave=high_wave, l1q=l1q, return_files=return_files, $
      lz_genxcat=lz_genxcat, lz_cat=lz_cat, $
      wavelnth=wavelnth, naxis1=naxis1

l1q=keyword_set(l1q)
lz_genxcat=keyword_set(lz_genxcat)
retval=-1
return_files=keyword_set(return_files)
if return_files then retval=''
count=0

case 1 of 
   l1q: begin
      e512=sswdb_files(time0, time1, /l1q,/eit, pat='*512.fits')
      e1024=sswdb_files(time0, time1, /l1q,/eit, pat='*1024.fits')
      e128=sswdb_files(time0, time1, /l1q,/eit, pat='*128.fits')
      alleit=e512   
      if n_elements(e1024) gt n_elements(e512) then $
         alleit=e1024
;       if n_elements(e416) gt n_elements(alleit) then alleit=e128
      if alleit(0) eq '' then $ 
            box_message,'No EIT L1Q files in timerange' else begin 
               wave=str2number(strextract(alleit,'eitp_','_'))
               hwave=histogram([wave],min=0)
               high_wave=(where(hwave eq max(hwave)))(0)
               retval=high_wave
               if keyword_set(return_files) then $ 
                  retval=alleit(where (wave eq high_wave, count))
      endelse
   endcase
   lz_genxcat: begin 
     read_genxcat, time0, time1, topdir='$SSWDB/soho/eit/lz_genxcat',lz_cat 
      if data_chk(lz_cat,/struct) then begin 
         ssok=where(strlowcase(lz_cat.object) eq 'full fov',okcnt) 
         if okcnt gt 0 then begin 
            lz_cat=lz_cat(ssok)
            if keyword_set(wavelnth) then hwl=fix(wavelnth) else begin 
               hwave=histogram([lz_cat.wavelnth],min=0)
               hwl=(where(hwave eq max(hwave)))(0)
            endelse
            high_wave=hwl
            lz_cat=lz_cat(where(lz_cat.wavelnth eq hwl))
            if keyword_set(naxis1) then naxis=fix(naxis1) else begin 
               hnaxis=histogram([lz_cat.naxis1],min=0)
               naxis=(where(hnaxis eq max(hnaxis)))(0)
            endelse
            ssbest=where(lz_cat.naxis1 eq naxis)
            lz_cat=lz_cat(ssbest)
            retval=(lz_cat.wavelnth)(0)
            if return_files then retval=lz_cat.filename
         endif else box_message,'No full disk images in time range'
      endif else begin
         box_message,'No LZ-GENX catalog for you time range'
      endelse
   endcase 
   else: box_message,'Only /L1Q supported for now
endcase

return,retval
end
