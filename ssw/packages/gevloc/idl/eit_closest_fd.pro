pro eit_closest_fd, time0, time1, file0, file1, closeest=closest, $
    allfiles=allfiles , lz_genxcat=lz_genxcat, l1q=l1q, $
    window_minutes=window_minutes, status=status, _extra=_extra, $
    debug=debug,after_time0=after_time0, nourl=nourl,local=local

debug=keyword_set(debug)
local=keyword_set(local)
nourl=local or keyword_set(nourl)
lz_genxcat=keyword_set(lz_genxcat)
l1q=keyword_set(l1q)

if n_elements(time1) eq 0 then begin 
   box_message,'Need time range'
   return
endif

lz_genxcat=lz_genxcat or ssw_deltat(reltime(/now),ref=time0,/days) gt 30 

if n_elements(window_minutes) eq 0 then window_minutes=30

time_window,[time0,time1],t0,t1,minute=window_minutes

if debug then stop,'t0,t1'
efiles=eit_high_cadence(t0,t1,$
   lz_genxcat=lz_genxcat,l1q=l1q, /return,_extra=_extra)
box_message,efiles

ftimes=file2time(efiles,out='int')
fbef=last_nelem(where(ssw_deltat(ftimes,ref=time0,/min) lt 0,bcnt))
case 1 of 
   keyword_set(after_time0): aftref=time0 
   else: aftref=time1
endcase
   
help,time0,aftref
faft=(where(ssw_deltat(ftimes,ref=aftref,/min) gt 0,acnt))(0)
status=bcnt gt 0 and acnt gt 0 
allfiles=efiles
lz=get_logenv('EIT_LZ')
lzwww='http://umbra.nascom.nasa.gov/eit_lz/'

case 1 of 
   nourl and (1-local):
   local: allfiles=eit_file2path(temporary(allfiles))
   else: begin
      set_logenv,'EIT_LZ',lzwww,/quiet
      allfiles=eit_file2path(temporary(allfiles))
      set_logenv,'EIT_LZ',lz,/quiet
   endcase
endcase  

if status then begin
   file0=allfiles(fbef)
   file1=allfiles(faft)
endif
 
if debug then stop

return
end
