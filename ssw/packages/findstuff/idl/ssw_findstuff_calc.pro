pro ssw_findstuff_calc, index, data, regions, region_info, minpix=minpix, $
		  phisort=phisort, status=status, addssw=addssw, $
                  darkstuff=darkstuff
;
;   Name: ssw_findstuff_calc
;
;   Purpose: calculate the ssw_findstuff parameters  (tag values)
;
;   Input Parameters:
;      index, data - the usual 
;      regions     - 'blobs' over the limb (blob coloring) (from ssw_limbstuff)
;
;   Output Parameters 
;      region_info - structure - info on each region w/npix > minpix 
;  
;   Keyword Parameters:
;      status - boolean - 1 if any valid regions found
;
;   Calling Examples:
;      ssw_findstuff_calc, index, data, regions, region_info [,minpix=xx]
;                     (in)  (in)   (in)    (out)
;   History:
;      16-jun-1997 - S.L.Freeland (break code from ssw_limbstuff.pro)
;      18-jun-1997 - S.L.Freeland - add PHISORT keyword and function
;      16-Jul-2001 - S.L.Freeland - add MOMENT and STD_DEV information
;      23-aug-2001 - S.L.Freeland - no region protection
;                                   add multi-moment2d, 
;                                   rename ssw_limbcalc -> ssw_findstuff_calc
;
;-
debug=keyword_set(debug)
if not keyword_set(minpix) then minpix=20

; ------------------------ check input -------------------------------
if  n_params() lt 2 then  begin
    prstr,strjustify(['IDL> ssw_findstuff_calc,regions, rinfo [,rnum]'],/box),/nomore
    return
endif    
; -------------------------------------------------------------------------
status=0
dark=keyword_set(darkstuff)
; ------- define the info structure  --------
if not data_chk(info_temp,/struct) then    begin
   info_temp={rnum:0l,                     $ ; region# (=pix value in REGIONS)
              npix:0l,                     $ ; number of pixels in region
              centroid:fltarr(2),          $ ; region weighted centroid
              inv_centroid:fltarr(2),          $ ; same from inverted data
              rtot:0.0,                    $ ; region total
              rmax:0.0,                    $ ; region maximum
              ravg:0.0,                    $ ; region average
              rmean:0.0,                   $
              rstddev:0.0,                 $
              rss:0.0,                     $ ; cent dist from sun center       
              phi:0.0,                     $ ; angle (degrees)
              extentrss:fltarr(2),         $ ; radial extent of region SS [low, high] pix
              extentphi:fltarr(2),         $ ; extent angle [low,high] deg.
              extentx:intarr(2),           $ ;
              extenty:intarr(2)            $ ;
                                           } ; 
; add moment2d tags
   for mom=-3,3 do begin 
      info_temp=add_tag(info_temp,0.,'mom2d_'+(['N','P'])(mom ge 0) + $
         strtrim(abs(mom),2))
   endfor
; -------------------------------------------
endif
; --------- Tag regions and filter out the too-small ---------------------
rhist=histogram(regions)
if n_elements(rhist) eq 1 then begin 
   box_message,'No regions found!, returning'
   return
endif
rhist=rhist(1:*)                         ; ignore element (0) => no region
nregions=n_elements(rhist)               ; total blobs identified
which=where(rhist ge minpix, rcnt)       ; filter "too small" regions
; ----------------------------------------------------------------------
if rcnt le 0 then begin
   box_message,'No regions found!, returning...'
   return                    ; unstructured exit!!!!!!
endif else status=1

region_info=replicate(info_temp,rcnt)       ; one per "accepted" region
if rcnt eq 1 then which=which(0)
region_info.rnum=which+1                    ; fill in region# (adjust for 0)
region_info.npix=rhist(which)               ; fill in #pix/region

sdata=size(regions)
empty=make_array(size=sdata)                ; make a useful template image 
sreg=strtrim(which+1,2)

if keyword_set(addssw) or get_logenv('ssw_findstuff_addssw') ne '' then begin 
   sswtags=str2arr('date_obs,naxis1,naxis2,cdelt1,cdelt2,crpix1,crpix2,xcen,ycen')
   for i=0,n_elements(sswtags)-1 do $
      region_info=add_tag(region_info,gt_tagval(index,sswtags(i)),$
                     'ref_'+sswtags(i))
   region_info=add_tag(region_info,gt_tagval(index,/date_obs),'date_obs') 
   region_info=add_tag(region_info,gt_tagval(index,/mjd), 'mjd')
   region_info=add_tag(region_info,gt_tagval(index,/day), 'day')
   region_info=add_tag(region_info,gt_tagval(index,/time),'time')
endif

;---- per-region loop for additionl info ---;

if get_logenv('ssw_findstuff_inhibit') ne '' then begin 
   box_message,'Inhibiting calculations
   return
endif

for i=0,rcnt-1 do begin                     ; for each region, gather some info
   message,/info,"Region>>> " + sreg(i)     ; region status message
   tempty=empty                             ; start with empty template
   rmask=regions eq region_info(i).rnum
   ss=where(rmask)                          ; pixel SS for region(i)
   tempty(ss)=data(ss)                      ; get corresponding data
   region_info(i).ravg=average(data(ss))    ; region average->str
   region_info(i).rmax=max(data(ss))        ; region max->str
   region_info(i).rmean=mean(data(ss))      ; region mean->str
   region_info(i).rstddev=stddev(data(ss))   ; retgion standard deviaion->str
   region_info(i).centroid=centroid(tempty) ; centroid->str
   region_info(i).inv_centroid=centroid(tempty,/invert)

   ; calculate  a bunch of moments
   for mom=-1,1 do begin 
      mtag='mom2d_'+(['N','P'])(mom ge 0) + strtrim(abs(mom),2)
      xw=([region_info(i).centroid(0),region_info(i).inv_centroid(0)])(dark)
      yw=([region_info(i).centroid(1),region_info(i).inv_centroid(1)])(dark)
      region_info(i).(tag_index(region_info,mtag))= $
               moment2d(tempty,xw,yw,mom,mask=rmask)
   endfor

   region_info(i).rtot=total(data(ss))      ; total->str
   extentx=ss mod sdata(1)                  ; region -> xcoord (pix)
   extenty=ss/(sdata(2))                    ; region -> ycoord (pix)
   region_info(i).extentx=[min(extentx),max(extentx)]
   region_info(i).extenty=[min(extenty),max(extenty)]
   conv_pix2polar,index,extentx,extenty,extentr,extentphi
   region_info(i).extentrss=[min(extentr),max(extentr)]  ; polar extent
   region_info(i).extentphi=[min(extentphi),max(extentphi)]  ; polar extent
   if debug then stop,'in for loop'
endfor                                     ;
;-------------------------------------------;

; post processing conversions
; -------------- centroid [x,y] -> polar ---------------------
conv_pix2polar,index,region_info.centroid(0),region_info.centroid(1),cr,cphi
region_info.rss=cr
region_info.phi=cphi

if debug then stop

if keyword_set(phisort) then begin
   message,/info,"Sorting by region Low PHI
   region_info=region_info(sort(region_info.extentphi(0)))
endif   

return
end
