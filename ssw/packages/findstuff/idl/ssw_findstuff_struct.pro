function ssw_findstuff_struct, index, data, reg, rinfo
;+
;   Name: ssw_findstuff_struct
;
;   Purpose: make the ssw_limb catalog structure (index+rinfo)
;
;   Input Paramters:
;      index,data,reg,rinfo (output of ssw_limbstuff.pro)
;  
;   Calling Sequence:
;      catstruct=ssw_findstuff_struct(index,data,reg,rinfo)
;  
;   History:
;      Circa August 1996 - For EIT He304 investigations
;      29-June-2000 - S.L.Freeland - for general SSW use
;
;-
retval=-1

if ( (1-data_chk(index,/struct) ) or (1-data_chk(rinfo,/struct)) ) then begin
   message,/info,"IDL> catstr=ssw_limbcatstruct(index,data,reg,rinfo)
endif else begin   
;  ITAGS defines which tags to use from INDEX (via str_subset.pro)
;  Extend the string to include extra tags
;  ----------------------------------------------------------
   itags='time,day,filename,crpix1,crpix2,crval1,crval2,xcen,ycen,solar_r,cdelt1,cdelt2'
   indexpart=replicate(str_subset(index,itags),n_elements(rinfo)) ; one per rinfo
;  ----------------------------------------------------------
   retval=join_struct(rinfo,indexpart)
endelse
return,retval
end
