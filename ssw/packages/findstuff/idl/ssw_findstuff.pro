
function region_data, index, data, rinfo
;
;   Name: region_data
;
;   Purpose: extract data for given region
;
;
regdat=data(rinfo.extentx(0):rinfo.extentx(1), $
              rinfo.extenty(0):rinfo.extenty(1))
return,regdat
end

pro conv_pix2polar, index, x ,y, r, phi
;
;   Name: conv_pix2polar
;  
;   Purpose: convert pixcoorid to polar
;
;   Input Parameters:
;      index - structure w/crpix1,crpix2,solar_r,cdelt1,cdelt2  
;      x,y - x&y pixel coords.
;
;   Output Parameters:
;      r   - radial distance from center (in solar R)  
;      phi - angle
;  
solar_r=gt_tagval(index,/solar_r,missing=0.)
if solar_r eq 0 then begin                              ; no solar_r, use ephemeris
   sunstuff=get_sun(index,sd=solar_r)
   solar_r=solar_r/gt_tagval(index,/cdelt1,missing=.5)
endif

r=sqrt((x - index.crpix1)^2. +  (y - index.crpix2)^2.)/solar_r
phi=atan(-(x - index.crpix1), y - index.crpix2)*!radeg
phi=phi + ([0,360])(phi lt 0.)  

return
end

pro ssw_findstuff, index, data, regions, region_info, $
		   annulus=annulus, annfact=annfact,  $
		   lowcut=lowcut, hicut=hicut, $
                   minpix=minpix,                               $
                   minsep=minsep,                               $
                   display=display,                             $
		   debug=debug,charsize=charsize,               $
                   status=status, _extra=_extra,                $
                   addssw=addssw,                               $
                   limbstuff=limbstuff, diskstuff=diskstuff, darkstuff=darkstuff     
;
;   Name: ssw_findstuff
;
;   Purpose: find and parameterize unspecified features in a solar image
;
;   Input Parameters:
;      index - a structure containing SSW standard pointing/time tags
;      data  - the corresponding DATA array
;  
;   Output Parameters:
;      regions - features ('blobs' via blob coloring) 
;      region_info - structure - info on each region w/npix > minpix 
;  
;   Keyword Parameters:
;      lowcut  - low count rate cutoff - if not supplied, take above limb avg.
;      annulus - 2 element annulus parameters [inner,outer] + solar_r 
;      annfact - calculate LOWCUT from above limb annulus  ANNFACT*most common
;      minpix  - ignore regions with number pixels < minpix         (def=20)
;      min_spasep - minimum spatial seperation between blobs
;      min_phisep - synonym for MINSEP
;      display - if set, show some results to TTY
;      hicut   - optional upper pixel value to consider (filter cosmic rays..)
;      _EXTRA  - unspecified keywords -> label_region via keyword inheritance
;      limbstuff - if set, only consider above limb stuff (def=entire image)
;      diskstuff - if set, only consider disk stuff       (def=entire image)
;      darkstuff - if set, regiions are 'dark'
;      addssw - if set, include ssw tags from the blob/region (full FOV image)
;               in each region structure (see ssw_findstuff_calc.pro)
;               These are mapped like index.XCEN -> region_info.REG_XCEN
;                                     index.DATE_OBS -> region_info.REG_DATE_OBS
;
;   Calling Sequence:
;      ssw_findtuff, index, data, regions, region_info [,lowcut=nn    $
;                                                      [,annfact=xx] $
;                                                      [,minpix=nn]  $
;                                                      [,/display]   $
;                                                      [,/limbstuff] $
;                                                      [,/diskstuff]   
;   Calling Examples:
;      ssw_findstuff, index, data, regions, region_info, lowcut=xxx  ; LOWCUT
;      ssw_findstuff, index, data, regions, region_info              ; annulus
;      ssw_findstuff, index, data, regions, region_info,annfact=2.0  ; annulus
;      ssw_findstuff, index, data, reg, rinfo, minpix=50,/display    ; big  reg          
;
;      ------------ eit sample sequence ----
;      read_eit, eit304file, index, data                 ; read a 304 image
;      eit_prep, index, data=data, oindex, odata         ; clean it up
;      ssw_findstuff, oindex, odata, regions, reg_info   ; find limb stuff
;      ----------------------------------
;
;   History:
;      4-jun-1997 - S.L.Freeland (Written, originally for EIT 304 study, but..)
;      9-jun-1997 - S.L.Freeland (tweaked, fixed polar (swap x,y in PHI calc)
;     11-jun-1997 - S.L.Freeland changed R values from pix->SolarR
;     16-jun-1997 - S.L.Freeland - break code - now calls:
;                                ssw_limbcalc and ssw_limbmark  
;     18-jun-1997 - S.L.Freeland - MINSEP enabled (calls ssw_limbmerge)
;     18-aug-1997 - S.L.Freeland - trap AMASK=-1
;     21-jun-2000 - S.L.Freeland - generalize SSW limb stuff finder
;                                  ssw_limbstuff -> ssw_findstuff (limb/disk..)
;     23-Aug-2001 - S.L.Freeland - call ssw_findstuff_calc
;     19-Sep-2001 - S.L.Freeland - ssw_limbmark -> ssw_findstuff_mark
;     29-Sep-2003 - S.L.Freeland - mod for New ssw_findstuff_mark (via DMZ maps)
;
;   Restrictions:
;      2D only for now - assume data 'cleaned' (dark, degrid, whatever)
;  
;   Method:
;      check data, setup call to label_region, figure out what it all means
;
;   Notes:  
;      annulus parameters is annulus above the limb 
;      inner padding [ annulus(0) ] avoids incomplete limb removal
;      ie, dont include any of the disk in deriving LOWCUT from annulus
;-
if not keyword_set(charsize) then charsize=1.3
debug=keyword_set(debug)

; ------------------------ check input -------------------------------
if (1-data_chk(index,/struct)) or n_params() lt 2 then  begin
    prstr,strjustify(['IDL> ssw_limbstuff, index, data [,/annulus]'],/box),/nomore
    return
endif    
; -------------------------------------------------------------------------

; ------------------------ some defaults ---------------------------------
display=keyword_set(display)
if n_elements(minpix) eq 0 then minpix=20

; -------------------------------------------------------------------------
nx=(size(data))(1)

if display then begin
   wdef,wdata,im=data
   if max(data) le 255 then tvscl,data else tvscl,safe_log10(index,data,/byte)
   xyouts2,nx/2,5,'!6Input Image',/device,align=.5,charsize=charsize
endif
; ------------------------ generate disk mask -----------------------------

case 1 of 
   keyword_set(limbstuff): begin
      dmask=solar_mask(index,data,/outside)
      if n_elements(annulus) ne 2 then annulus=[1.1,1.2]
   endcase
   keyword_set(diskstuff): begin
      dmask=solar_mask(index,data)
      if n_elements(annulus) ne 2 then annulus=[.8,.9]
   endcase
   else: begin
      dmask=make_array(data_chk(data,/nx),data_chk(data,/ny),/byte,value=1)
      if n_elements(annulus) ne 2 then annulus=[.7,1.3]
   endcase
endcase

dinterest=data*dmask                         ; masked data to consider

if display then begin
  wdef, wdabove,im=dinterest
  if max(dinterest) le 255 then tvscl,dinterest else $
      tvscl,safe_log10(index,dinterest,/byte)
  xyouts2,nx/2,5,'!6Masked data (above limb)',/device,align=.5,charsize=charsize
endif

; -------------------------------------------------------------------------
if not keyword_set(hicut)  then hicut=max(data)     ; ** better default?? ***

; ---------- if no LOWCUT, derive default from above-limb annulus ---------
status=0

if n_elements(lowcut) eq 0 then begin
   dannulus=solar_mask(index,dinterest,annulus=annulus,/mask_data)
   annavg=average(dannulus,missing=0)                  ; annulus average
   annhist=histogram(dannulus,min=0)
   annhist(0)=0                                        ; ignore BIN ZERO
   if n_elements(annfact) eq 0 then annfact=1.5        ; Annulus FACTOR 
   lowcut=(where(annhist eq max(annhist)))(0)*annfact  ; Most-Common*ANNFACT
endif

; -------------------------------------------------------------------------

; ---------- make a bilevel image of the neat stuff -----------------------

if keyword_set(darkstuff) then begin
   dinterest=max(data)-dinterest > 0
   dinterest(where(1-dmask))=0
   temp=lowcut
   lowcut=max(data) - hicut
   hicut=max(data)- temp
endif

something=(dinterest ge lowcut and dinterest le hicut)

bilev=dinterest

nothing=1-something

sssomething=where(something,somecnt)
ssnothing=where(nothing,nothcnt)

if debug then stop,'bilev,locut,hicut,something,nothing'

if (somecnt eq 0)  or  (nothcnt eq 0) then return    ; !!! ERROR - EARLY EXIT  
status=1                                        ; OK - set NORMAL EXIT STATUS
bilev(sssomething) = 1 & bilev(ssnothing)  = 0


if display then begin
  wdef,wbilev,im=bilev
  tvscl,bilev
  xyouts2,nx/2,5,'!6BLOB Bi-Level Image',/device,align=.5,charsize=charsize
endif

regions=label_region(bilev, _extra=_extra)      ; <<<<< FIND THE BLOBS
if display then begin
  wdef,wreg,im=regions
  tvscl,regions
  xyouts2,nx/2,5,'!6BLOB Colored Image (regions)',/device,align=.5,charsize=charsize
endif
; -------------------------------------------------------------------------


; ------- call ssw_limbcalc to convert the BLOB array->IDL structure
message,/info,"Calling <ssw_findstuff_calc>"
if debug then stop,'findstuff'
ssw_findstuff_calc, index, data, regions, region_info, minpix=minpix,/phisort,$
    status=status, addssw=addssw, darkstuff=darkstuff
; -------------------------------------------------------------------------

nregions=n_elements(region_info) * status

mess=["Total number of limb regions found: " + strtrim(nregions,2), $
      "(Number after MINPIX (" + strtrim(minpix,2) +") applied)", $
      "Using LOCUT value: " + strtrim(lowcut,2)]
; -------------------------------------------------------------------------

; ----------------  exit if nothing of interest ------------------------
if nregions eq 0 then begin
   mess=[mess,"No regions of interest were found, returning"]
   prstr,strjustify(mess,/box),/nomore
   return
endif
; -------------------------------------------------------------------------

mess=[mess,'', strjustify('Region# ' + strtrim(region_info.rnum,2) + $
                 ' #Pixels: ' + strtrim(region_info.npix,2))]

prstr,strjustify(mess,/box),/nomore

; --------- Display now handled via ssw_findstuff_mark.pro -----------------------
if display then begin
   if max(data) le 255 then dimg=byte(data) else dimg=safe_log10(data,/byte)
   ssw_findstuff_mark,index,data,regions,region_info, $
       c_color=200,/contour,/box,/label,/init
endif  

; -- IF MINSEP supplied, merge 'close' (via ssw_limbmerge) -------------
if keyword_set(min_phisep) then minsep=min_phisep
if keyword_set(minsep) then begin
   if nregions gt 1 then begin 
      message,/info,"MINSEP supplied, Calling <ssw_limbmerge>"
      ssw_limbmerge,index,data,regions,region_info,$
		  min_phisep=minsep, display=display  
   endif else begin 
      box_message,'Only 1 region - not calling ssw_limbmerge'
   endelse
   if keyword_set(display) then begin
      tbeep,2
      prstr,strjustify(['ALL DONE WITH SSW_MERGE, DISPLAYING FINAL ITERATION'],/box)
      tv,bytscl(data,max=100)
      ssw_findstuff_mark,index,data,regions,region_info, c_color=200,/contour,/box,/label
    endif
endif
; -------------------------------------------------------------------------


if debug then stop

return
end
