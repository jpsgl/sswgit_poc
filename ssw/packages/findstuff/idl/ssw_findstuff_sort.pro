function ssw_findstuff_sort, rinfo, extentr, extentphi, $
   area=area,  bright=bright, height=height, $
   rextent=rextent,  phiextent=phiextent, $
   interest=interest,   less2more=less2more
;+
;   Name: ssw_limbfilter
;
;   Purpose: sort ssw_limbstuff regions by "interest" / return some info
;
;   Input Parameters:
;      rinfo - structure output from ssw_limbstuff.pro
;
;   Output Parameters:
;     extentr   - radial extent
;     extentphi - angular extent
;     limb_interest - ~arbitrary 'interest level' 
;  
;   Keyword Parameters:
;      --------- 'sort by' keywords  ------------
;      area      -  region area (npix)
;      bright    -  region average  brightness
;      height    -  centroid heighth above limb
;      rextent   -  radial extent of region
;      phiextent -  angular extent     
;      interest  -  for now ratio of (rextent/phiextent)*(.2*area)*(height^2)
;      --------------------------------------------
;      less2more - if set, smaller to larger (def = large->smaller)
;      uniq_time - (not yet implemented)
;  
;   Calling Sequence:
;    ninfo=ssw_findstuff_sort(rinfo[,/area,/bright,/height,/rextent,/phiextent,/interest]
;  
; calculate some useful values
extentr  =rinfo.extentrss(1) - rinfo.extentrss(0)
dph=rinfo.extentphi(1)-rinfo.extentphi(0)
extentphi=dph+([0,360])(dph lt 0)
limb_interest=(extentr/extentphi)*(rinfo.rss^10.)*(.1*rinfo.npix)
case 1 of
   keyword_set(area):      order=sort(rinfo.npix)
   keyword_set(height):    order=sort(rinfo.rss)
   keyword_set(bright):    order=sort(rinfo.ravg)
   keyword_set(rextent):   order=sort(extentr)
   keyword_set(phiextent): order=sort(extentphi)
   else: order=sort(limb_interest)
endcase

if not keyword_set(less2more) then order=reverse(order)

return, order
end
