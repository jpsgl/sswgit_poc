






pro ssw_findstuff2html, index, data, regions, rinfo, prefix=prefix, $
		   interactive=interactive, $
		   _extra=_extra, $                   ; << pass to findstuff_sort
		   charsize=charsize                  ; << pass to findstff_mark
;+
;   Name: ssw_findstuff2html
;
;   Purpose: Provide summary HTML for one 2D image
;-
interact=keyword_set(interactive)

outwww=concat_dir(get_logenv('path_http'),'findstuff')
if not data_chk(prefix,/string) then prefix='findstuff_'

root=prefix + time2file(index)
www=concat_dir(outwww,root)
gif= root +'.gif'

; ----------- start an html document -----------
html=concat_dir(outwww,root+'.html')
html_doc,html,/header
imgname=index.filename

if imgname eq '' then $
   imgname=gt_tagval(index,/instrume) + '_'+time2file(index)

file_append,html,['SSW_FINDSTUFF Summary for Image: '+ imgname + $
     '<em>', get_infox(index,'telescop,instrume,naxis1,naxis2,cdelt1'),'</em>']
; ----------------------------------------------------
;

; ----------  make full size annotated gif ----------------
wdef,im=data,/zbuffer
set_plot,'z'
loadct,15
tvlct,r,g,b,/get
tvscl,data
ssw_findstuff_mark, index, data, regions, rinfo, bcolor=200, lcolor=250,/contour
gifname=concat_dir(outwww,gif)
zbuff2file,gifname                               ; -> gif

 ; -------------- thumbnail (1/3 size) gif -------------
fact=.5
thumb=mkthumb(data,r,g,b,fact=fact)            ; thumbnail
wdef,xx,/zbuff,im=thumb
tvscl,thumb
ssw_findstuff_mark, index, data, regions, rinfo, fact=fact, bcolor=200, lcolor=250,/contour, charsize=charsize
tname=str_replace(gifname,'.gif','_thumb.gif')
zbuff2file,tname
; --------------------------------------------------------

; ----------- optional display to X --------------
if interact then begin 
   set_plot,'x'
   wdef,full,im=data,/lr
   tvscl,data
   device,get_screen=gs
   xpos=gs(0)-512
endif

; ------ determine graphics parameters -------------
maxsize=256.                                     ; largest thumbnail dimension
rsizex=rinfo.extentx(1)-rinfo.extentx(0)
rsizey=rinfo.extenty(1)-rinfo.extenty(0)
zfact =maxsize/(float(rsizex > rsizey)) < 5.
thumbx=round(rsizex*zfact) & thumby=round(rsizey*zfact)
labels='Region# ' + strtrim(rinfo.rnum,2) 
thumbnames=root+'_'+string(rinfo.rnum,format='(i4.4)')+'.gif'
; -----------------------------------------------------

; ------- loop through regions, make thumbnails, etc ---------
for i=0,n_elements(rinfo)-1 do  begin
   if interact then begin                         ; optional X tracking
      wset,full & wshow
      ssw_findstuff_mark,index, data,regions, rinfo(i),/contour,/box,/label      
   endif
;  -- extract the data and generate thumbnail ---
;   reg=bytscl(region_data(index,data,rinfo(i)),min=(.1*rinfo(i).ravg),max=(rinfo(i).rmax+20))
   reg=bytscl(region_data(index,data,rinfo(i)))
   out=mkthumb(reg, r,g,b,nx=thumbx(i), ny=thumby(i), $  ;label=labels(i), $
        outfile=concat_dir(outwww,thumbnames(i)))

; -----------------------------------------------------
   if interact then begin                          ; optional X tracking
      wdef,i,im=out,/ur,xpos=(i*50) mod 1024
      tvscl,out
   endif
endfor

if not data_chk(_extra,/struct) then _extra={HEIGHT:1}

order=ssw_findstuff_sort(rinfo, exr,exphi, _extra=_extra)

if data_chk(_extra,/struct) then begin
    rinfo=rinfo(order)
    exr=exr(order)
    exphi=exphi(order)
    thumbnames=thumbnames(order)
    labels=labels(order)
endif


; -------- build region status table -------------------------
tnames=str2arr('RNUM,NPIX,RAVG,RSS,PHI')
info=get_infox(rinfo,tnames,format='a,a,a,a,a',header=header)
header=str2arr('Region#,Area(pix),Level,Centroid(R),Centroid(PHI),Radial Extent(R),   Angular Extent(Deg)')
extentd=string(exr,format='(f5.3)') + '     ' + string(exphi,format='(f5.1)')
info=str2cols(info+'  ' + extentd)
tab=[[header],[info]]
table=strtab2html(tab,/row0header)

; prepend the Thumbnail to the table 
thtml=(str2html(gif,link=root+'_thumb.gif',/nopar))(0)
table(0)=str_replace(table(0),'<th>','<th>'+imgname+' (Thumbnail)</th><th>')
rowspan='<td rowspan='+strtrim(n_elements(rinfo)+1,2) +'>'       ; span table
table(7)=str_replace(table(7),'<td>',rowspan+thtml+'</td><td>')
file_append,html,table                                           ; add table
; ----------------------------------------------------

; -------- add region thumbnail graphics -------------
table=[['<em>'+labels+'</em>'],['<img src="'+ thumbnames + '">']]
file_append,html,strtab2html(table)
; -------------------------------------------

html_doc,html,/trailer

hdocs=file_list(outwww,'*_*.html')
index=concat_dir(outwww,'index.html')
html_doc,index,/header
file_append,index,'<h2>ssw_findstuff output index generated: '+systime()+'</h2>
file_append,index,str2html(strsplit(hdocs,'/',/last,/tail))+'<br>'
html_doc,index,/trailer

return
end
