pro ssw_fs_cat2db, cat, index, data , original=original, $
    db_dir=db_dir, orig_dir=orig_dir, initialize=initialize, $
    debug=debug
;
;+   
;   Name: ssw_fs_cat2db
;
;   Purpose: return reqion (blob) DB or original data for findstuff cat entries
;
;   Input Parameters:
;      cat - catalog entry/entries of interest
;
;   Output Parameters:
;      index,data - corresponding region (blob) DB or original data 
;
;   Keyword Parameters:
;      original - if set, output index,data is original data
;                 (default is the region (blob) data base)
;
;   History:
;      31-August-2001 - S.L.Freeland
;-

common ssw_fs_cat2db_blk, files, times

debug=keyword_set(debug)
if not keyword_set(dbdir) then dbdir='/u1/sswarch/yoarch/fsdb'

init=keyword_set(initialize) or n_elements(files) eq 0 

if init then begin
   box_message,'Initial dbase file listing (faster next time...)'
   files=file_list(dbdir,'')
   times=file2time(files,out='ints')
endif

ss=tim2dset(times,cat)

ufiles=all_vals(files(ss))

case 1 of
   keyword_set(mask): begin 
      for f=0,n_elements(ufiles)-1 do begin 
         mreadfits, ufiles(f),tindex,tdata
         stop,'pre2t2
         sss=tim2dset(cat,anytim(file2time(ufiles(f)),out='ints'))
      endfor
   endcase 
   else: begin 
      mreadfits,ufiles,index,data
      box_message,'Number of uniq files/index,data pairs is: ' + $
            strtrim(n_elements(ufiles),2)
   endcase
endcase
   
if debug then stop

return
end




 
