pro ssw_findstuff_mark, index, data, reg, rinfo, rnumber, $
   lcolor=lcolor, bcolor=bcolor, fact=fact, label=label, debug=debug, $
   contour=contour, box=box, _extra=_extra, charsize=charsize
;+
;   Name: region_mark
;
;   Purpose: mark region(s) on current device
;
;   Input Parameters:
;        index,data,reg,rinfo (output from ssw_limbstuff.pro)
;        rnumber - relative region to mark
;
;   Keyword Parameters:
;      fact     - maginification factor (relative to data)
;      label    - optional  label for  region  
;      countour - if set, mark region with contour 
;      box      - if set, mark with box
;      _extra   - keyword inheritapass to ocontour
;      bcolor   - if set, color for boxes
;      lcolor   - if set, color for labels  
;  
;   History:
;      18-Jun-1997 - S.L.Freeland - for ssw_limbstuff work
;
;-  
contour=keyword_set(contour)
box=keyword_set(box) or keyword_set(bcolor)           ; bcolor implies /box
debug=keyword_set(debug)
if n_elements(fact) eq 0 then fact=1.
dxy=fact*4
sx=(size(data))(1)*fact
sy=(size(data))(2)*fact
ninfo=n_elements(rinfo)
if not keyword_set(bcolor) then bcolor=140         
if not keyword_set(lcolor) then lcolor=!p.color else $  ; lcolor implies label
    if not keyword_set(label) then label=1

if n_elements(label) eq 1 then label='!6'+strtrim(rinfo.rnum,2)
if n_params() eq  5 then which=rnumber else which=lindgen(ninfo)


if contour then $
   if fact eq 1 then tregion=reg else tregion=round(congrid(reg,sx,sy))

empty=make_array(sx,sy,/byte)               ; make an empty template

if not keyword_set(charsize) then charsize=1+fact*.5
					     
for i=0,n_elements(which)-1 do begin
   ss=which(i)
   x0=rinfo(ss).extentx(0)*fact & x1=rinfo(ss).extentx(1)*fact
   y0=rinfo(ss).extenty(0)*fact & y1=rinfo(ss).extenty(1)*fact
   if box then draw_boxcorn,x0,y0,x1,y1,/device, color=bcolor
   rnum=rinfo(ss).rnum
   if contour then begin
     regcont=tregion
     sss=where(tregion ne rnum)             ; pixel SS for region(i)
     regcont(sss)=0
     ocontour,regcont,levels=rnum,_extra=_extra
   endif
   if keyword_set(label) then begin
;     label outside box - avoid overlaps via quadrant dependent postioning
      outx=(([x0,x1])(x0 gt sx/2) ) + (dxy*([-1,1])(x0 gt sx/2))
      outy=([y0,y1])(y0 gt sy/2)    + (dxy*([-1,1])(y0 gt sy/2))
      align=([0,1])(x1 lt sx/2)
      xyouts2,outx,outy,align=align,label(ss),/device,charsize=charsize, color=lcolor
   endif
endfor
end
							       
