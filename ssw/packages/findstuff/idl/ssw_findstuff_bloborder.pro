pro ssw_findstuff_bloborder, rinfo, inblob, orinfo, outblob,  $
   addblob=addblob, $
   rstart=rstart, rkeep=rkeep, rlimit=rlimit, $
   _extra=_extra
;+
;   Name: ssw_findstuff_bloborder
;
;   Purpose: reorder regions and stuff into output blob image
;
;   Input Parameters:
;      inblob - blob colored image
;      rinfo  - region info structure
;      oinfo  - output region - sorted & .RNUM updated
;      outblob - output blob colored image (sorted)
;      addblob - optional starting blob template (for merging multiple sets)
;       
;
;   Output:
;      function returns reordered blob colored image
;      	
;   Keyword Parameters:
;      rstart - 1st value to use (useful if more than one "type" of
;               region is stored in iteritative calls)
;               Note: by convention, output of region finding
;               uses level=0 to indicate no region membership.
;               therefore region numbers begin with rstart+1
;
;      rkeep  - number of regions to keep
;      
;      _extra - sort criteria per ssw_findstuff_sort (default=area)
;
;   History:
;      25-July-2000 - S.L.Freeland - for region dbase use
;      27-Aug-2001  - S.L.Freeland - enabled functions
; 
;
case 1 of 
   data_chk(_extra,/struct): ostring='order=ssw_findstuff_sort(rinfo,_extra=_extra)'
   else: ostring='order=ssw_findstuff_sort(rinfo,/area)
endcase
estat=execute(ostring)


if not data_chk(rinfo,/struct) then begin 
   box_message,['Need ssw_findstuff rinfo and region']
   return
endif

if n_elements(rlimit) eq 0 then rlimit=n_elements(rinfo)
if n_elements(rstart) eq 0 then rstart=0

addin=keyword_set(addblob)
case 1 of 
   data_chk(addblob,/nimage) eq 1: outblob=addblob
   else: outblob=make_array(data_chk(inblob,/nx),$
                            data_chk(inblob,/ny),/byte)
endcase

orinfo=rinfo(order)
; outblob=outblob + (rstart*(outblob eq 0))
for ri=rstart, rstart+rlimit-1 do begin 
   ssr=where(inblob eq orinfo(ri-rstart).rnum,cnt)
   if cnt gt 0 then outblob(ssr)=ri+1
   ;outblob=outblob+(inblob eq orinfo(ri-rstart).rnum)*(ri+1)
   print,'Mapping ' + $
         strtrim(orinfo(ri-rstart).rnum,2)+ ' -> ' + strtrim(ri+1,2)

endfor

orinfo.rnum=lindgen(n_elements(rinfo))+1+rstart


return
end
