pro ssw_findstuff_dbase_ch,start_day,stop_day, $
   _extra=_extra, debug=debug, test=test

debug=keyword_set(debug)

testing=keyword_set(test)

tpath=(['','_test'])(testing)
blobdb='/u1/sswarch/yoarch/fsdb' + tpath
if not file_exist(blobdb) then $
   blobdb='/net/diapason/www2/SXT/crb/fsdb_ch'


catdir='/home/sswdb/ydb/findstuff_cat'+tpath
if not file_exist(catdir) then $
   catdir='/net/diapason/www2/SXT/crb/findstuff_cat_ch'


diskstuff=[1,1,1,1]
darkstuff=[1,1,1,1]
lowcut=[1,1,1,1]
hicut= [6,9,12,15]
rlimit=256/n_elements(lowcut)

filts=['AM','A1']

; lets do monthly catalogs
mnts=get_month(indgen(12),/trunc)
years=strtrim(indgen(11)+1991,2)
t0=str_perm('1-'+mnts+'-',years)
t0=t0(sort(anytim(t0)))

if n_params() lt 2 then stop_day=last_nelem(t0)
if n_params() lt 1 then start_day=t0(0)

ss=sel_timrange(t0,start_day,stop_day)
t0=t0(ss)
fmt_timer,t0
pmm,ssw_deltat(t0,/day)

for filt=0,1 do begin                    ; for each 
fname=filts(filt)

   for i=0,n_elements(t0)-2 do begin 
      estring='files=crb_files(t0(i),t0(i+1),/'+fname+',/half)
      estat=execute(estring)
      if files(0) ne '' then begin 
         if testing then nfiles=test else nfiles=n_elements(files)
         delvarx,catstruct
         for f=0,nfiles-1 do begin 
            mreadfits,files(f),index,data
            for tlev=0,n_elements(lowcut)-1 do begin
               if tlev eq 0 then delvarx,addblob

;              find the regions for this parameter set
               ssw_findstuff,index,data,lowcut=lowcut(tlev),hicut=hicut(tlev),$
                  _extra=_extra,region,rinfo, /addssw, minpix=100, $
                  diskstuff=diskstuff(tlev), darkstuff=darkstuff(tlev)

;              reorder the catalog based on some paramter (area...)
               ssw_findstuff_bloborder, rinfo, region, outrinfo, outblob, $
                  addblob=addblob,rstart=tlev*rlimit, _extra=_extra
               addblob=outblob
               outrinfo=add_tag(outrinfo,lowcut(tlev),'lowcut')
               outrinfo=add_tag(outrinfo,hicut(tlev),'hicut')
               outrinfo=add_tag(outrinfo,byte(diskstuff(tlev)),'diskstuff')
               outrinfo=add_tag(outrinfo,byte(darkstuff(tlev)),'darkstuff')

;              append catalog vector
               if n_elements(catstruct) eq 0 then catstruct=outrinfo else $
                  catstruct=concat_struct(catstruct,outrinfo)

;              update dbase index w/history                
               if debug then stop,'index->fits' 
               infile=strsplit(files(f),'/',/last,/tail)
               
            endfor                       ; end of threshold loop (file)

;        write fits / blob -> dbase
         if debug then stop,'index->fits'
         infile=strsplit(files(f),'/',/last,/tail)
         update_history,index,version=1.0
         index=add_tag(index,infile(0),'INFILE')
         index=add_tag(index,arr2str(lowcut,/trim),'lowcut')
         index=add_tag(index,arr2str(hicut,/trim),'hicut')
         index=add_tag(index,arr2str(indgen(n_elements(lowcut))*rlimit,/trim),$
                'rlimits')
       
         prefix='sxtreg_'+fname+'_'+strtrim(data_chk(data,/nx),2)
         mwritefits,index,outblob,prefix=prefix,outdir=blobdb
      endfor                          ; end of file 
;  write catalog file->genx
   outstruct=catstruct(sort(anytim(catstruct)))
   write_genxcat,outstruct,prefix='sxtreg',topdir=catdir,$
        /nelements,/day_round
 
   endif                              ; valid files found?
endfor
endfor                                   ; end of filter loop 

end
