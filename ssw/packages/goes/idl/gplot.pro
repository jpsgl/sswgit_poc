;+
;
; Name: gplot
;
; Purpose: Plot goes data.
;
; Explanation:  goesplot routine was written to do everything - accumulate
;	data, clean, take derivative, and then plot it.  goesplot changed
;	to have a noplot flag to just return the data, this routine makes
;	the same kind of plot that goesplot makes but without hardcoding
;	all the options.  Primarily for use with goes object in goes__define
;	and plotting through plotman object.
;
; Calling sequence:
;	gplot, tdata, ydata, sat, ut_ref, n_pts, yrange=yrange, $
;       timerange=timerange, _extra=extra
;
; Input Arguments:
;	tdata - array of times in sec. relative to ut_ref
;	ydata - array (n,2) containing two channels of GOES data
;	sat - GOES satellite data is from (6,7,8,9,10)
;	ut_ref - reference time in any format accepted by anytim
;	n_pts - number of data points summed together
;
; Keyword Inputs:
;	yrange - fltarr(2) with limits in y direction for plot
;	timerange - dblarr(2) with range of time to plot
;	extra - inclues:
;		extra keywords to pass to plot
;		dcolor -structure with color name tags (e.g. dcolor.blue)
;			that point to colors in current color table
;		deriv - set if ydata contains derivative
;
; Written: Kim Tolbert, 4/12/2000
;
;-


pro gplot, tdata, ydata, sat, ut_ref, n_pts, yrange=yrange, $
	timerange=timerange, _extra=extra

if keyword_set(yrange) then !y.range=yrange
checkvar, timerange, [0.d0, 0.d0]

; if dcolor keyword passed in, then use names for colors, otherwise call linecolors
have_colors = 0
if keyword_set(extra) then begin
	if tag_exist (extra, 'DCOLOR') then begin
		colors = [extra.dcolor.cyan, extra.dcolor.pink]
		have_colors = 1
	endif
endif
if not have_colors then begin
	tvlct, savr, savg, savb, /get
	linecolors
	colors = [9,3]  ; cyan, pink
endif

; Set x axis limits
check_plotlimits, anytim(timerange,/sec), !y.range, tdata, ydata[*,0], new_xrange, new_yrange0, /ylog
check_plotlimits, anytim(timerange,/sec), !y.range, tdata, ydata[*,1], new_xrange, new_yrange1, /ylog

new_yrange = [min([new_yrange0,new_yrange1]), max([new_yrange0, new_yrange1])]

title = 'GOES '+ string(sat,format='(i2)')
if keyword_set(deriv) then title = title + '    Derivative'
ytitle = 'Watts m!u-2!n'
nsum_used = n_pts
if !p.nsum ne 0 then nsum_used = !p.nsum

deriv = 0
if keyword_set(extra) then begin
	if tag_exist(extra,'deriv') then if extra.deriv then begin
		deriv = 1
		title = title + '    Derivative'
		ytitle = ytitle + ' s!u-1!n'
	endif
endif

pmulti=!p.multi

; This is an attempt to correct the charsize when !p.multi=[0,1,2] or !p.multi=[0,1,>2]
chscale=1.
if  !d.y_size le 512 then begin
    if pmulti(2) eq 2 then chscale=.8
	if pmulti(2) gt 2 then chscale=.6
endif
;print,'chscale=', chscale
utplot, tdata, ydata[*,0], ut_ref, $
  ymargin=[7,2],xmargin=[10,5], title=title, ytitle=ytitle, $
  xtitle=' ', timerange=timerange, xstyle=1, yrange=new_yrange, $
  chars = chscale, _extra=extra, /nodata

oplot, tdata, ydata[*,0], _extra=extra, color = colors[0]

oplot, tdata, ydata[*,1], _extra=extra, color = colors[1]

;  Print GOES importance levels on right side of log plots
if not (deriv) then begin
	ylims = crange('y')
	ytickv = 10.^[-11+indgen(9)]
	ytickname = [strarr(3)+' ','A','B','C','M','X',' ']
	q = where(( ytickv ge ylims(0)) and ( ytickv le ylims(1)), kq)
	if kq gt 0 then $
		axis, yaxis=1, ytickv = ytickv(q),/ylog, ytickname=ytickname(q), $
			yrange=ylims, chars=chscale
endif

;--Use normalized x and y window limits to place labels

l = !x.window(0) & r = !x.window(1)
b = !y.window(0) & t = !y.window(1)
xi = !x.s
xychar = convert_coord(!d.x_ch_size, !d.y_ch_size, /device, /to_normal)

;  Construct labels for bottom of plot

if new_xrange[0] eq 0. then datastart = ut_ref else datastart = new_xrange[0]
sub1 = 'Data Start: ' + strmid(atime(datastart,/hxr),0,17) + $
		'   Plot created: ' + strmid(atime(sys2ut(0),/hxr),0,14)
;sub2 = 'Plot created: ' + strmid(atime(sys2ut(0),/hxr),0,14)
sub3 = '!cChannel 1 (1. - 8. Angstroms)  Background not subtracted'
sub4 = '!c!cChannel 2 (.5 - 4. Angstroms)  Background not subtracted'

; This is an attempt to correct the charsize when !p.multi=[0,1,2] or !p.multi=[0,1,>2]
chscale=1.
dist_y=b  - xychar[1] *3.
if !d.y_size le 512 then begin
	if pmulti(2) eq 2 then chscale=.8
	if pmulti(2) gt 2 then chscale=.6
	;dist_y= b - (t-b)/30.
endif

; Print labels under plot

xyouts, l, dist_y, sub1, /normal, chars = chscale
;xyouts, l+(r-l)*.53, dist_y, sub2, /normal, $
;       chars=chscale
xyouts, l, dist_y, sub3, /normal, $
      chars = chscale, color=colors(0)
xyouts, l, dist_y, sub4, /normal, $
             chars=chscale, color=colors(1)

if (nsum_used ne 1) then xyouts, l, dist_y, $
'!c!c!cSample average = '+strtrim(nsum_used,2) , /normal, chars=chscale

; if we called linecolors, restore original color table
if exist(savr) then tvlct, savr, savg, savb
end
