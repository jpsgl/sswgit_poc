;+
; Name:
;	TEM_CALC
; Purpose:
;	TEM_CALC calculates the temperature and emission measure from the GOES ionization X-ray chambers.
;
; 	The data array must already be cleaned of gain change spikes.  The background
;	in each channel is input through avback in the common block, goes_back.
;
; Explanation:
;	This procedure organizes the inputs and outputs into the GOES_TEM procedure.  GOES_TEM interpolates on
;	the ratio between the long and short wavelength channel fluxes using pre-calcuated tables for the
;	fluxes at a series of temperatures for fixed emission measure.  Note that ratios which lie outside of 
;	the default lower limits for the fluxes, 
;	and particularly negative values in the case of background subtracted inputs, are
;	returned with fixed values of 4.0 MegaKelvin and 1.0e47 cm-3 for the temperature and emission measure.
;	Normally, those limits are 1e-7 and 1e-9 for the long and short wavelength channel inputs.
;
; Keywords:
;  Mandatory Inputs:
;	TARRAY- time array
;	YCLEAN-  GOES data cleaned of spikes.
;
;  Optional Inputs:
;	NOBACKSUB- If set, no background is subtracted
;	SAVESAT  - Number of GOES satellite, calibration changes at 8
;	DATE - date in anytim format if tarray is not fully qualified (only sec of day)
;  Outputs:
;
;	NOSUBTEMPR-temperature in Megakelvin, if background not subtracted.
;	NOSUBEMIS-emission measure in 1e49 cm-3, if background subtracted.
;	
;	TEMPR- temperature in Megakelvin, if background subtracted.
;	EMIS-emission measure in 1e49 cm-3, if background subtracted.
; Common Blocks:
;	goes_back
; Calls:
;	GOES_TEM
; History:
;	Kim Tolbert 11/92
;	Documented RAS, 15-May-1996,
;	changed cutoff limits, changed default response
;	to GOES_TEM, eback and sback removed as keywords since they did nothing.
;	ras, 22-july-1996, add savesat
;	ras, 20-nov-1996, added GOES6 and GOES7 as GOES_TEM was upgraded
;	ras, 29-jan-1997, add date keyword needed for correct GOES6 calculation!
;	ras, 4-aug-1998, richard.schwartz@gsfc.nasa.gov, integrate GOES10 and future GOESN.
;-
;
;  -----------------------------------------------------------------------

pro tem_calc, tarray=tarray, yclean=yclean, tempr=tempr, emis=emis, $
    nosubtempr=nosubtempr, nosubemis=nosubemis, savesat=savesat, $
     nobacksub=nobacksub, date=date

common goes_back, sback_str, eback_str, avback

date_in = (anytim(/sec,tarray))(0)
if date_in lt 86400. then  date_in = date

ysub = yclean 

dosub=1
if keyword_set(nobacksub) then dosub=0
if not(dosub) then goto, docalc

for ich = 0,1 do ysub(*,ich) = yclean(*,ich) - avback(ich)
cutoff1 = 1.0e-9/10.
cutoff0 = 1.0e-7/10.
if avback(0) gt 1.5e-6 then begin
	 cutoff0 = 5.e-7
	 cutoff1 = 5.e-9
endif




docalc:
q = where( ysub(*,0) lt cutoff0 or ysub(*,1) lt cutoff1, nq)

;goes_tem,  ysub(*,0), ysub(*,1), te, em, goes8 = (savesat eq 8), goes9=(savesat ge 9),$
;	goes6=(savesat eq 6), goes7=(savesat eq 7), date=date_in
goes_tem,  ysub(*,0), ysub(*,1), te, em, satellite=savesat,  date=date_in
if nq ge 1 then begin
	te(q) = 4.0
	em(q) = .01
endif
em = em 
if dosub then tempr = te
if not(dosub) then nosubtempr = te
if dosub then emis = em
if not(dosub) then nosubemis = em

if dosub then begin
   dosub = 0
   ysub = yclean
   goto, docalc
endif

end
