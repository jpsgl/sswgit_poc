;+
; PROJECT:
;	SDAC
; NAME:
;	GOES
;
; PURPOSE:
;	Provide a widget interface to plot GOES data and derived quantities.
;
; CATEGORY:
;       GOES
;
; CALLING SEQUENCE:
;       GOES
;
; CALLS:
;       HXRBS_FORMAT, SET_GRAPHICS, CHECKVAR, UTIME, YOHKOH_FORMAT, WIDG_TYPE,
;	XSET_VALUE, TWIDGET, RESPOND_WIDG, WCHECK_SET, GOESPLOT, Y_AVERAGE, ANYTIM,
;	TEM_CALC, TEM_PLOT, POINT, ZOOM_COOR, GETUTBASE, TEKPRINT, ATIME,
;	CLEANPLOT
;
; INPUTS:
;       none explicit, only through commons;
;
; OUTPUTS:
;       Goes_str- A structure containing the data obtained by the procedure
;		See the readme tag in the structure.
;
; KEYWORDS:
;       INPUT-OPTIONAL, A structure with tag names that must correspond to named variables
;	within GOES.PRO. May be used to input starting values for
;		STIME- Start time in seconds from 1-jan-1979, anytim( date_string, /sec).
;		ETIME- End time in seconds from 1-jan-1979, anytim( date_string, /sec).
;		MARKBAD- Logical, mark bad points with X.
;		LOGPLOT- Set for logarithmic y scaling.
;		SAT- GOES Satellite default request.
;		N_PTS- Sample average to use.
;		INITIAL_BACKG_STATE- Set(unset def) background subtraction option on
;		raw data.
;	VERSIONRELEASE- Developer's tool, to check performance prior to !version.release of 4
;
; COMMON BLOCKS:
;       goes_widgets, goes_plot, savegoes, goes_back
;
; RESTRICTIONS:
;	Procedure looks to see if the GOES data files containing are
;	found under GOES_FITS or withing the Yohkoh GBO archive.
;       If not, displays error message.
;
; PROCEDURE:
;	Data is read from the GOES archive in GOES_FITS from FITS files.
;	Options are available for cleaning the data of gain change spikes,
;	computing temperature and emission measure, log/linear scaling,
;	overlaying channels, etc.  If the GOES_FITS archive doesn't exist
;	for a particular time, the Yohkoh GBO archive is searched and utilized.
;
; MODIFICATION HISTORY:
; 	Written by Kim Tolbert 11/92
; 	Mod 7/2/93 by KT to add ERR_MSG keyword to call to GOESPLOT and
;		TEM_PLOT. ERR_MSG contains the text of the error message to
;		be displayed in the message window.  Previously, messages
;		were stored in PLOTERR array.
; 	Mod 7/6/95 by AES to avoid specifying any widget sizes - was causing
;		problems at upgrades.  also include yderiv keyword, so that
; 		the time derivative is saved in save files.
; 	Mod 7/31/95 by AES to compare seconds of time instead of string format
;		dates, to avoid possible differences in format  (uses anytim)
; 	Mod 09/95 by RCJ to include SATELLITE field and SAMPLE AVG. droplist.
; 	Mod 3/20/96 by AES to enable satellite goes-9
; 	Mod 22-jul-1996, ras, pass savesat onto TEM_CALC
; 	Mod 08/07/96 by RCJ to include SATELLITE in README when WRITEFILE
;		option is chosen.
; 	Mod 09/12/96 by RCJ to use droplist to choose flux, tempr. or emis.
;		plot, offer sample average to tempr. and emis. plots, make any
;		 x-axis change be reflected on other 2 graphs, add more (bkg's
;		 and averaged) arrays to output file.
;	Mod 09/13/96 by RCJ. Added documentation.
;	Mod 11/01/96 by RCJ to show sample avg. in message window and also
;		save it in README (see WRITEFILE). Make ave_* = fltarr(1)
;		at beginning of program.
;	Mod 27-jan-1997, by RAS, implemented list widgets for droplists for less than
;		IDL release 4 when droplist didn't exist.
;	Mod 28-jan-1997 by RAS, added readme to common savegoes, goes_str output
;	Version 14
;		readme to common savegoes, goes_str outputMod 31-jan-1997 by RAS,
;		 added VERSIONRELEASE and INPUT keywords.  Added INITIAL_BACKG_STATE.
;	Version 15
;		RAS, 13-feb-1997, add start_time and end_time as optional input keywords
;		Use anytim compatible format.t
;	Version 16
;		richard.schwartz@gsfc.nasa.gov, changed wask to respond_widg, 8-sep-1997.
;	Version 17
;		richard.schwartz@gsfc.nasa.gov, cleaned twidget code, 24-sep-1997.
;	Version 18
;		amy@aloha.nascom.nasa.gov, 22-Jul-1998, added code for GOES 10
;	Version 19
;		richard.schwartz@gsfc.nasa.gov, 5-Oct-1999, modified test of time interval
;		to determine validity of save request.
;	Version 20
;		richard.schwartz@gsfc.nasa.gov,10-nov-1999, expand range of twidget to 1980-2020.
;	Version 21
;		kim.tolbert@gsfc.nasa.gov, 20-nov-1999, don't use parse_atime to get year after
;			call to twidget - returns 0 for 2000, which means user selected 'all' -
;			instead use elements of rtime1.  Also call twidget with nowild.
;		kim.tolbert@gsfc.nasa.gov, 18-apr-2000, added savebase_sec to common savegoes
;		kim.tolbert@gsfc.nasa.gov, 27-jul-2000, if plot temperature with no background subtraction
;			before ever plotting with background sub. it crashed because tarray was not defined.
;			Changed so when bkgd not being subtracted, does an accumuluation with /noplot set.
;       kim.tolbert@gsfc.nasa.gov, 28-may-2002, specify xsize on message widget (needed for Windows)
;       kim.tolbert@gsfc.nasa.gov, 21-jun-2002, was using a variable called fstring, but
;          now there's a function called fstring. renamed fstring to tstring.  was causing errors
;          setting times.
;		kim.tolbert@gsfc.nasa.gov, 19-may-2004, had a limit of 6-days accumulation. Increased
;			that to 30 days.  Also added Goes 12.
;
;
;-
; --------------------------------------------------------------------------
;
pro goes_event, event

common goes_widgets, base, $
                     wcstart, wcend, wcdur, wtstart, wtend, wtdur, wthxrbs,$
                     wstart, wend, whxrbs, wselect, wmulti, wchannels, $
		     wavg, r3a1_emis, r3a1_flux, r3a1_temp,$
                     satbut, llbut, prbut, chbut, rawbut, tmpbut, embut, $
                     wmessage, wcurrent, $
		     wxmin_f, wxmax_f, wxauto_f, wymin_f, wymax_f, wyauto_f, $
		     wxmin_t, wxmax_t, wxauto_t, wymin_t, wymax_t, wyauto_t, $
		     wxmin_e, wxmax_e, wxauto_e, wymin_e, wymax_e, wyauto_e

common goes_plot, goes_window, want_plotfile, done_plot, tekfile, psfile, $
                  logplot, ch_select, raw, clean, markbad, deriv, flxsback, $
                  tmpsback, tmpnosback, tmpmarkbad, $
                  emsback, emnosback, emmarkbad, $
                  stime, etime, tarray, yarray, yclean, yderiv,tempr, emis,$
                  nosubemis, nosubtempr, ch0_bad, ch1_bad, $
                  utbase, overlay, combch, overch, flarenum, $
                  prevplot, xzoom, yzoom_f, yzoom_t, yzoom_e, sat, n_pts, $
		  ave_tarray, ave_yclean, ave_emis, ave_tempr, $
		  ave_nosubemis, ave_nosubtempr

common savegoes, savesat, savestime, saveetime, savetarray, saveyarray, $
       		saveyclean, savech0_bad, savech1_bad, loedges, hiedges, readme, savebase_sec

common goes_back, sback_str, eback_str, avback

;widget_control, event.id, get_value = value, get_uvalue = uvalue
       ;the above line is now under the BUTTON wtype only. RCJ 10/24/95
wtype = widg_type(event.id)  ; possibilities: 'DROPLIST','TEXT','BUTTON'
;wtype = strmid (tag_names(event,/structure_name), 7, 1000)

oldstime = stime
oldetime = etime
case wtype of
   'DROPLIST': begin
      widget_control,event.id,get_uvalue=uvalue
      input_drop=uvalue(event.index)
      case input_drop of
	 'Flux': begin
	    widget_control,r3a1_temp,map=0
	    widget_control,r3a1_emis,map=0
	    widget_control,r3a1_flux,map=1
	 end
	 'Temp.': begin
      	    widget_control,r3a1_flux,map=0
      	    widget_control,r3a1_emis,map=0
      	    widget_control,r3a1_temp,map=1
	 end
	 'Em. Meas.': begin
      	    widget_control,r3a1_flux,map=0
      	    widget_control,r3a1_temp,map=0
      	    widget_control,r3a1_emis,map=1
	 end
         '1_pt': n_pts=1
         '10_pts': n_pts=10
         '20_pts': n_pts=20
         '30_pts': n_pts=30
         '40_pts': n_pts=40
         'choose_pts': begin
            value=1
            xset_value,value,min=1,max=200, $
                        title='Select Sample Avg.',group=base
            n_pts=value
         end ; end to 'users choice'
      endcase  ; end input_drop case
      widget_control, wmessage, /append, $
		set_value='Sample average = '+strtrim(n_pts,2)
   end   ; end of DROPLIST block
   'LIST': begin
      widget_control,event.id,get_uvalue=uvalue
      input_drop=uvalue(event.index)
      case input_drop of
	 'Flux': begin
	    widget_control,r3a1_temp,map=0
	    widget_control,r3a1_emis,map=0
	    widget_control,r3a1_flux,map=1
	 end
	 'Temp.': begin
      	    widget_control,r3a1_flux,map=0
      	    widget_control,r3a1_emis,map=0
      	    widget_control,r3a1_temp,map=1
	 end
	 'Em. Meas.': begin
      	    widget_control,r3a1_flux,map=0
      	    widget_control,r3a1_temp,map=0
      	    widget_control,r3a1_emis,map=1
	 end
         '1_pt': n_pts=1
         '10_pts': n_pts=10
         '20_pts': n_pts=20
         '30_pts': n_pts=30
         '40_pts': n_pts=40
         'choose_pts': begin
            value=1
            xset_value,value,min=1,max=200, $
                        title='Select Sample Avg.',group=base
            n_pts=value
         end ; end to 'users choice'
      endcase  ; end input_drop case
      widget_control, wmessage, /append, $
		set_value='Sample average = '+strtrim(n_pts,2)
   end   ; end of LIST block

   'TEXT': begin
      widget_control, event.id, get_value = tstring
      case 1 of
;
         (event.id eq wxmin_f) or (event.id eq wxmin_t) or $
	 (event.id eq wxmin_e): begin
            widget_control, wxmin_f, set_value = tstring(0)
            widget_control, wxmin_t, set_value = tstring(0)
            widget_control, wxmin_e, set_value = tstring(0)
            temp = utime(tstring(0), error=error)
            if not(error) then xzoom(0) = temp
         end
;
         (event.id eq wxmax_f) or (event.id eq wxmax_t) or $
	 (event.id eq wxmax_e): begin
            widget_control, wxmax_f, set_value = tstring(0)
            widget_control, wxmax_t, set_value = tstring(0)
            widget_control, wxmax_e, set_value = tstring(0)
            temp = utime(tstring(0), error=error)
            if not(error) then xzoom(1) = temp
         end
;
         (event.id eq wymin_f): yzoom_f(0) = float(tstring(0))
         (event.id eq wymin_t): yzoom_t(0) = float(tstring(0))
         (event.id eq wymin_e): yzoom_e(0) = float(tstring(0))
         (event.id eq wymax_f): yzoom_f(1) = float(tstring(0))
         (event.id eq wymax_t): yzoom_t(1) = float(tstring(0))
         (event.id eq wymax_e): yzoom_e(1) = float(tstring(0))
;
         (event.id eq wtstart) or (event.id eq wtend): begin
            temp = utime(tstring(0), error=error)
            if not(error) then begin
               if event.id eq wtstart then stime = temp else etime = temp
            endif
            goto, display_times
         end

         (event.id eq wtdur): begin
            val = float(tstring(0))
            if val gt 0 then etime = stime + val
            goto, display_times
         end
      endcase ; end event.id case

;      if event.id eq wthxrbs then begin
;         val = fix(tstring(0))
;         if val gt 0 and val le 12776 then begin
;            read_minicat, fldata, num, flare=val, error=error
;            if  not(error) then begin
;               stime = fldata(0).start_sec
;               etime = fldata(0).start_sec + fldata(0).dur
;               goto, display_times
;            endif
;         endif
;         widget_control, wmessage, /append, set_value = $
;            'Error in flare selection.'
;         goto, goback
;
;      endif

   end  ; end of text block

   'BUTTON': begin  ; start button case block
      widget_control, event.id, get_value = value, get_uvalue = uvalue
      if (event.id eq wstart) or (event.id eq wend) then begin
         widget_control, base, sensitive=0
         str = 'start'
         if event.id eq wend then str = 'end'
         widget_control, wcurrent, set_value = 'Starting '+str+' time ' + $
            'selection widget ...'
         gettime:
         widget_control,wmessage,/append,set_value = 'Select '+str+' time.'
         twidget, year=[1980,2020], outtime=rtime1, group_leader=base, /all, error=error, /nowild
         if error ne 0 then goto, goback
         ;parse_atime, rtime1, year=y, month=m, day=d
         y = rtime1(6)
         m = rtime1(5)
         d = rtime1(4)

         if (y le 0) or (m le 0) or (d le 0) then begin
            widget_control, wmessage, /append, set_value = $
               'Wild card dates not allowed.  Please reselect start time.'
            goto, gettime
         endif
         asctime= (anytim(/sec, rtime1))(0)
         if event.id eq wstart then stime = asctime else etime = asctime
         goto, display_times
      endif

;      if event.id eq whxrbs then begin
;         hflare, hxr_flare, group_leader=base
;         read_minicat, fldata, num, flare=hxr_flare, error=error
;         if error then begin
;            widget_control, wmessage, /append, set_value = $
;               'Error in flare selection.'
;            goto, goback
;         endif
;         stime = fldata(0).start_sec
;         etime = fldata(0).start_sec + fldata(0).dur
;         widget_control, wthxrbs, set_value=strtrim(hxr_flare,2)
;         goto, display_times
;      endif

      if (event.id eq wxauto_f) or (event.id eq wxauto_t) or $
	 (event.id eq wxauto_e) then begin
         xzoom = [0.,0.]
         widget_control, wxmin_f, set_value = '0.'
         widget_control, wxmax_f, set_value = '0.'
         widget_control, wxmin_t, set_value = '0.'
         widget_control, wxmax_t, set_value = '0.'
         widget_control, wxmin_e, set_value = '0.'
         widget_control, wxmax_e, set_value = '0.'
         goto, goback
      endif
      if (event.id eq wyauto_f) then begin
         yzoom_f = [0.,0.]
         widget_control, wymin_f, set_value = '0.'
         widget_control, wymax_f, set_value = '0.'
         goto, goback
      endif

      if (event.id eq wyauto_t) then begin
         yzoom_t = [0.,0.]
         widget_control, wymin_t, set_value = '0.'
         widget_control, wymax_t, set_value = '0.'
         goto, goback
      endif

      if (event.id eq wyauto_e) then begin
         yzoom_e = [0.,0.]
         widget_control, wymin_e, set_value = '0.'
         widget_control, wymax_e, set_value = '0.'
         goto, goback
      endif

      q = where (event.id eq wmulti)
      if q(0) ne -1 then begin
         row = fix(q(0)/2) & col = q(0)-row*2
         widget_control, wmulti(col,0), set_value=value
         goto, goback
      endif

      q=where (event.id eq chbut)
      if q(0) ne -1 then begin
         ch_select(q(0)) = event.select
         goto,goback
      endif

      q=where (event.id eq rawbut)
      if q(0) ne -1 then begin
         if q(0) eq 0 then raw = event.select
         if q(0) eq 1 then clean = event.select
         if q(0) eq 2 then flxsback = event.select
         if q(0) eq 3 then deriv = event.select
         if q(0) eq 4 then markbad = event.select
         goto,goback
      endif

      q=where (event.id eq tmpbut)
      if q(0) ne -1 then begin
         if q(0) eq 0 then tmpsback = event.select
         if q(0) eq 1 then tmpnosback = event.select
         if q(0) eq 2 then tmpmarkbad = event.select
         goto,goback
      endif

      q=where (event.id eq embut)
      if q(0) ne -1 then begin
         if q(0) eq 0 then emsback = event.select
         if q(0) eq 1 then emnosback = event.select
         if q(0) eq 2 then emmarkbad = event.select
         goto,goback
      endif

      input_type = strmid (uvalue, 0, 2)
      input_value = strmid (uvalue, 2, 100)
      case input_type of  ; start input_type case block

         'c_': begin    ; start command (c_) case block
            case input_value of  ; start input_value within c_ case block

            'Newplot': begin
               !p.multi = [0,1,1,0,0]
               erase
            end

            'FPlot': begin
               ; check that start>end and range < 30 days.
               if (stime gt etime) or (etime-stime gt 2.6e6) then begin
                  widget_control, wmessage, /append, set_value = $
                  'Error in time selection.  Try again.'
                  goto, goback
               endif

               if total(ch_select) eq 0 then begin
                  widget_control, wmessage, /append, set_value = $
                        'No channels selected for plotting.'
                  goto,goback
               endif

               widget_control, base, sensitive=0

               widget_control, wcurrent, set_value = $
                   'Accumulating data and/or setting up plot, please wait...'

               temp_multi = intarr(5)
               for i=0,1 do begin
                  widget_control, wmulti(i,0), get_value=val
                  temp_multi(i+1) = val
               endfor
               if (temp_multi(1) ne !p.multi(1)) or $
                  (temp_multi(2) ne !p.multi(2)) then !p.multi=temp_multi

               dofplot:
               if flxsback then getnewback=1 else getnewback=0
               if getnewback and $
                  (abs(stime-utime(sback_str)) lt 86400.) then $
                  getnewback = 1 - respond_widg( title='  Define new background intervals?  ',$
			message = string( bytarr(35)+32b), group_leader=base )
		;wask, quest='  Define new background intervals?  ', $
                 ; answer=getnewback, group_leader=base
               if getnewback then begin
                  wcheck_set, goes_window2, title='GOES Plot 2',retain=2
                  wshow, goes_window2
                  save_multi = !p.multi
                  !p.multi = 0
                  goesplot, stime=atime(stime),etime=atime(etime), $
                     tarray=tarray, yarray=yarray, yclean=yclean, $
                     /clean, ch0_bad=ch0_bad, ch1_bad=ch1_bad, $
                     logplot=logplot, ch_select=[1,0], error=error,$
                     err_msg=err_msg,sat=sat,n_pts=n_pts, $
		     ave_tarray=ave_tarray,ave_yclean=ave_yclean
                  if error ne 0 then begin
                     widget_control,wmessage,/app,set_v=err_msg
                     wdelete, goes_window2
                     goto,goback
                  endif
                  y_average, tarray, yclean, avback, wmessage=wmessage, $
                     stime_str=sback_str, etime_str=eback_str
                  !p.multi = save_multi
                  wdelete, goes_window2
               endif
               wcheck_set, goes_window, title='GOES Plot', retain=2
               wshow, goes_window
               if !p.multi(0) eq 0 then erase
               goesplot,stime=atime(stime),etime=atime(etime), $
                  tarray=tarray, yarray=yarray, yclean=yclean, $
                  clean=clean, markbad=markbad, back=avback*flxsback, $
                  logplot=logplot, plotfile=want_plotfile,$
                  ch_select=ch_select, error=error, err_msg=err_msg, $
                  ch0_bad=ch0_bad, ch1_bad=ch1_bad, yderiv=yderiv, $
                  xrange=xzoom, yrange=yzoom_f, deriv=deriv, $
                  e_bars=e_bars, sat=sat,n_pts=n_pts, $
		  ave_tarray=ave_tarray,ave_yclean=ave_yclean

               widget_control, wmessage, /append, set_value = $
                  'GOES '+ string(sat,format='(i2)') + ' data being retrieved'

               if (sat eq 6) and anytim(stime,/sec) gt anytim('94/1/1') and $
                  anytim(etime,/sec) lt anytim('94/08/18') then $
                  widget_control,wmessage,/append, $
                  set_value='GOES 7 also has data for the selected date range'

               if (sat eq 7) and anytim(stime,/sec) gt anytim('94/1/1') and $
                  anytim(etime,/sec) lt anytim('94/08/18') then $
                  widget_control,wmessage,/append, $
                  set_value='GOES 6 also has data for the selected date range'

               if error ne 0 then begin
                  widget_control,wmessage,/app,set_v=err_msg
                  goto,goback
               endif
; if want both clean and raw data overlaid, already plotted
; clean, so now plot raw.
               if clean+raw eq 2 then $
                  goesplot,stime=atime(stime),etime=atime(etime), $
                     tarray=tarray, yarray=yarray, yclean=yclean, $
                     clean=0, markbad=markbad, back=avback*flxsback, $
                     logplot=logplot, plotfile=want_plotfile, $
                     ch_select=ch_select, error=error, err_msg=err_msg, $
                     xrange=xzoom, yrange=yzoom_f, /noerase, deriv=deriv, $
                     e_bars=e_bars, yderiv=yderiv,sat=sat,n_pts=n_pts, $
		     ave_tarray=ave_tarray,ave_yclean=ave_yclean
               if error ne 0 then begin
                  widget_control,wmessage,/app,set_v=err_msg
                  goto,goback
               endif
               prevplot = 'FPlot'
               done_plot = 1
               set_graphics, printer = p
               psfile = (p eq 'PS')    ; 0 or 1
               tekfile = (p eq 'TEK')  ; 0 or 1
               if psfile or tekfile then begin
                  plotfile = 'goesplot.ps'
                  if tekfile then plotfile = 'goesplot.tek'
                  f = findfile (plotfile, count=count)
                  if count gt 0 then widget_control, wmessage, $
                     /append, set_value = 'Saved in plot file ' + f(0)
               endif
            end

            'TPlot': begin
               if tmpsback+tmpnosback eq 0 then begin
                  widget_control, wmessage, /append, set_value = $
                  'You must select do or don''t do bkgd subtraction'
                  goto, goback
               endif
               getnewback = 1
               if not(tmpsback) then getnewback = 0
               goto, teplot
            end

            'EPlot': begin
               if emsback+emnosback eq 0 then begin
                  widget_control, wmessage, /append, set_value = $
                  'You must select do or don''t do bkgd subtraction'
                  goto, goback
               endif
               getnewback = 1
               if not(emsback) then getnewback = 0

               teplot:
               if stime eq 0 or etime eq 0 then begin
                  widget_control, wmessage, /append, set_value = $
                  'You must first select times or flare number.'
                  goto, goback
               endif
               widget_control,wcurrent,set_value='Accumulating, please wait...'
               if getnewback and $
                  (abs(stime-utime(sback_str)) lt 86400.) then $
                  getnewback = 1 - respond_widg( title='  Define new background intervals?  ',$
			message = string( bytarr(35)+32b), group_leader=base )
		;wask, quest='  Define new background intervals?  ', $
                 ; answer=getnewback, group_leader=base
                	       if getnewback then begin
                    wcheck_set, goes_window2, title='GOES Plot 2',retain=2
                    wshow, goes_window2
                    save_multi = !p.multi
                    !p.multi = 0
                    goesplot,stime=atime(stime),etime=atime(etime), $
                       tarray=tarray, yarray=yarray, yclean=yclean, $
                       /clean, ch0_bad=ch0_bad, ch1_bad=ch1_bad, $
                       logplot=logplot, ch_select=[1,0], error=error, $
                       err_msg=err_msg,sat=sat,n_pts=n_pts, $
		       ave_yclean=ave_yclean, yderiv=yderiv
                    if error ne 0 then begin
                       widget_control,wmessage,/app,set_v=err_msg
                       wdelete, goes_window2
                       goto,goback
                    endif
                    y_average, tarray, yclean, avback, wmessage=wmessage, $
                       stime_str=sback_str, etime_str=eback_str
                    !p.multi = save_multi
                    wdelete, goes_window2
	       endif else begin; end of getnewback
			; accumulate with /noplot so that tarray will be defined
                    goesplot,stime=atime(stime),etime=atime(etime), $
                       tarray=tarray, yarray=yarray, yclean=yclean, $
                       /clean, ch0_bad=ch0_bad, ch1_bad=ch1_bad, $
                       logplot=logplot, ch_select=[1,0], error=error, $
                       err_msg=err_msg,sat=sat,n_pts=n_pts, $
                       ave_yclean=ave_yclean, yderiv=yderiv, /noplot
		endelse

;
; The following lines are only for setting up a demo file.
;tarray = long(tarray)
;save, /xdr, file='goestem_demo.sav', tarray, tempr, emis, $
;   nosubtempr, nosubemis, eback_str, sback_str, avback
;
               tem_calc, tarray=tarray, yclean=yclean, tempr=tempr, $
			emis=emis, nosubtemp=nosubtempr, $
                  	nosubemis=nosubemis, savesat=savesat, date=getutbase()
               temp_multi = intarr(5)
               for i=0,1 do begin
                  widget_control, wmulti(i,0), get_value=val
                  temp_multi(i+1) = val
               endfor
               if (temp_multi(1) ne !p.multi(1)) or $
                  (temp_multi(2) ne !p.multi(2)) then !p.multi=temp_multi
               doteplot:
               wcheck_set, goes_window, title='GOES Plot', retain=2
               wshow, goes_window
               if !p.multi(0) eq 0 then erase
               if input_value eq 'TPlot' then begin
                  tem_plot, do_tempr=tmpsback, do_tnosub=tmpnosback, $
                     tarray=tarray, yarray=tempr, ynosub = nosubtempr, $
                     logplot=logplot, plotfile=want_plotfile, $
                     markbad=tmpmarkbad, badpts=[ch0_bad,ch1_bad], $
                     xrange=xzoom, yrange=yzoom_t, error=error, $
		     err_msg=err_msg, sat=sat, n_pts=n_pts, t_vals=ave_tarray,$
		     y_vals=ave_tempr,y_nosubvals=ave_nosubtempr
               endif else begin
                  tem_plot, do_emis=emsback, do_enosub=emnosback, $
                     tarray=tarray, yarray=emis, ynosub=nosubemis, $
                     logplot=logplot, plotfile=want_plotfile, $
                     markbad=emmarkbad, badpts=[ch0_bad,ch1_bad], $
                     xrange=xzoom, yrange=yzoom_e,error=error, $
                     err_msg=err_msg,sat=sat, n_pts=n_pts, t_vals=ave_tarray,$
		     y_vals=ave_emis,y_nosubvals=ave_nosubemis
               endelse
               if error ne 0 then begin
                  widget_control,wmessage,/app,set_v=err_msg
                  goto,goback
               endif
               prevplot = input_value
               done_plot = 1
               set_graphics, printer = p
               psfile = (p eq 'PS')    ; 0 or 1
               tekfile = (p eq 'TEK')  ; 0 or 1
               if psfile or tekfile then begin
                  plotfile = 'goesplot.ps'
                  if tekfile then plotfile = 'goesplot.tek'
                  f = findfile (plotfile, count=count)
                  if count gt 0 then widget_control, wmessage, $
                     /append, set_value = 'Saved in plot file ' + f(0)
               endif
            end

            'Point': begin
               if not(done_plot) then begin
                  widget_control, wmessage, /append, set_value= $
                  'Nothing plotted yet.'
                  goto,goback
               endif
               widget_control, wmessage, /append, set_value= $
                  'Press left mouse button to get x and y values, ' + $
                  ' middle mouse button to get time and y value.'
               widget_control, wmessage, /append, set_value= $
                  '   PRESS RIGHT MOUSE BUTTON WHILE POINTING IN ' + $
                  'PLOT WINDOW TO EXIT.'
               widget_control, base, sensitive=0
               wshow, goes_window
               point,x,y
            end

            'Zoom': begin
               if not(done_plot) then begin
                  widget_control, wmessage, /append, set_value= $
                  'Nothing plotted yet.'
                  goto,goback
               endif
               widget_control, base, sensitive=0
               wshow, goes_window
               widget_control, wmessage, /append, set_value = $
                  'Position cross at bottom left corner of zoom window ' + $
                  'and press left mouse button.'
               widget_control, wmessage, /append, set_value = $
                  '   Then position cross at opposite corner and press ' + $
                  'left mouse button again.'
               zoom_coor, xzoom, yzoom
               temp0 = strmid(atime(xzoom(0)+getutbase(0)), 10, 12)
               temp1 = strmid(atime(xzoom(1)+getutbase(0)), 10, 12)
               xzoom(0)=strtrim(xzoom(0),2) & xzoom(1)=strtrim(xzoom(1),2)
               widget_control, wxmin_f, set_value = temp0
               widget_control, wxmax_f, set_value = temp1
               widget_control, wxmin_t, set_value = temp0
               widget_control, wxmax_t, set_value = temp1
               widget_control, wxmin_e, set_value = temp0
               widget_control, wxmax_e, set_value = temp1
               case prevplot of
		  'FPlot': begin
                    widget_control, wymin_f, set_value = strtrim(yzoom(0),2)
                    widget_control, wymax_f, set_value = strtrim(yzoom(1),2)
                    yzoom_f(0)=strtrim(yzoom(0),2) & yzoom_f(1)=strtrim(yzoom(1),2)
                    input_value = prevplot
                    goto, dofplot
                  end
		  'TPlot': begin
                    widget_control, wymin_t, set_value = strtrim(yzoom(0),2)
                    widget_control, wymax_t, set_value = strtrim(yzoom(1),2)
                    yzoom_t(0)=strtrim(yzoom(0),2) & yzoom_t(1)=strtrim(yzoom(1),2)
                    input_value = prevplot
                    goto, doteplot
                  end
		  'EPlot': begin
                    widget_control, wymin_e, set_value = strtrim(yzoom(0),2)
                    widget_control, wymax_e, set_value = strtrim(yzoom(1),2)
                    yzoom_e(0)=strtrim(yzoom(0),2) & yzoom_e(1)=strtrim(yzoom(1),2)
                    input_value = prevplot
                    goto, doteplot
                  end
                  else: begin
                     widget_control, wmessage, /append, set_value= $
                     'Nothing plotted yet.'
                  end
               endcase  ; end prevplot case
            end

            'Hardcopy': begin

               if not(done_plot) then begin
                  widget_control, wmessage, /append, set_value= $
                  'Nothing plotted yet.'
                  goto,goback
               endif
               if want_plotfile eq 0 then begin
                  widget_control, wmessage, /append, set_value= $
                  'No hardcopy format selected. ' + $
                  'Select a hardcopy format, then redraw plot.'
                  goto,goback
               endif
               set_graphics, printer=pr
               wrong_pr = ' '
               if pr eq 'TEK' then if not(tekfile) then wrong_pr = 'Tek'
               if pr eq 'PS' then if not(psfile) then wrong_pr = 'PS'
               if wrong_pr ne ' ' then begin
                  widget_control, wmessage, /append, set_value = wrong_pr + $
                  ' format was not selected when you made the plot.'
                  widget_control, wmessage, /append, set_value = $
                  'Please plot again before requesting ' + wrong_pr + $
                  ' hardcopy.'
                  goto,goback
               endif
               tek_print,file='goesplot'
               widget_control, wmessage, /append, set_value = $
                  'Sending plot file to printer.'
            end

; ---          Write XDR Save file with base time, time array, cleaned y
;              array, and temperature and emission measure if they've been
;              calculated

            'Writefile': begin
               ; check that current time interval has been accumulated
               if ( abs( anytim(savestime) - anytim(stime)) gt 1.0 ) or $
                  ( abs(anytim(saveetime)  - anytim(etime)) gt 1.0 ) then begin
                  widget_control, wmessage, /append, set_value= $
                  'Data for selected time interval not accumulated.'
                  widget_control, wmessage, /append, set_value= $
                  '    Accumulate data by plotting it first.'
                  goto, goback
               endif
               ; construct output file name
               at = atime(stime)
               savefile = 'idlsave_goes' + $
                  strmid(at,0,2)+strmid(at,3,2)+strmid(at,6,2)+'.dat'
               ; save base time as seconds since 79/1/1 and ASCII time
               utbase = getutbase(0)
               asciibase = atime(utbase)
               ; if temperature and emission measure haven't been calculated
               ; yet, set them to 0 so we can use the same SAVE command
               if (size(tempr))(1) eq 0 then tempr = fltarr(1)
               if (size(emis))(1) eq 0 then emis = fltarr(1)
               if (size(nosubtempr))(1) eq 0 then nosubtempr = fltarr(1)
               if (size(nosubemis))(1) eq 0 then nosubemis = fltarr(1)
	       satellite='GOES '+ string(sat,format='(i2)')
              save, asciibase, ave_emis, ave_nosubemis, ave_nosubtempr, $
			ave_tarray, ave_tempr, ave_yclean, ch0_bad, ch1_bad, $
			emis, nosubemis, nosubtempr, n_pts, readme, satellite, $
			tarray, tempr, utbase, yclean, yderiv, /xdr, $
			file=savefile
               widget_control, wmessage, /append, set_value= $
	               'Saved in IDL XDR save file ' + savefile
            end

; ---          Exit GOES widget

            'Quit': goto,exit

               else: print,'Error in command.'

            endcase  ; end of input_value case block
            end

      's_': begin
         if (input_value eq 'disable') and (event.select eq 1) then clean=0
         if (input_value eq 'enable') and (event.select eq 1) then clean=1
         end

      'l_': begin
         if (input_value eq '0') and (event.select eq 1) then logplot=0
         if (input_value eq '1') and (event.select eq 1) then logplot=1
         end

      'p_': begin
         if event.select ne 1 then goto,goback
         if input_value eq 'none' then want_plotfile = 0 else want_plotfile = 1
         if input_value eq 'ps'  then set_graphics,printer='PS'
         if input_value eq 'tek' then set_graphics,printer='TEK'
      end


      'g_': begin
	 g_prepend = 'Within GOES_FITS directory, '
         if (input_value eq '6') and (event.select eq 1) then begin
             sat=6
             widget_control, wmessage, /append, set_value= $
             g_prepend + 'GOES 6 data range: 1980/01/04 - 1994/08/18'
         endif
         if (input_value eq '7') and (event.select eq 1) then begin
             sat=7
             widget_control, wmessage, /append, set_value= $
             g_prepend + 'GOES 7 data range: 1994/01/01 - 1996/08/03'
         endif
         if (input_value eq '8') and (event.select eq 1) then begin
             sat=8
             widget_control, wmessage, /append, set_value=  $
             g_prepend + 'GOES 8 data range: 1996/03/21 - 2003/06/18'
         endif
	 if (input_value eq '9') and (event.select eq 1) then begin
	     sat=9
	     widget_control, wmessage, /append, set_value= $
	     g_prepend + 'GOES 9 data range: 1996/03/20 - 1998/07/24'
	 endif
         if (input_value eq '10') and (event.select eq 1) then begin
	    sat=10
	    widget_control,wmessage, /append, set_value=$
	    g_prepend + 'GOES 10 data starts: 1998/07/10'
	 endif
         if (input_value eq '12') and (event.select eq 1) then begin
	    sat=12
	    widget_control,wmessage, /append, set_value=$
	    g_prepend + 'GOES 12 data starts: 2002/12/13'
	 endif
      end
      else: print,'error in widget'

      endcase   ;end of command (c_) case block

   end  ; end button block
   else: print,'error in input - doesn''t match any of wtypes.'
endcase   ; end wtype case block

goto, goback

display_times:
widget_control, wtstart, set_value = (atime(stime))(0)
widget_control, wcstart, set_value = 'Start: ' + atime(stime)
widget_control, wtend, set_value = (atime(etime))(0)
widget_control, wcend, set_value = 'End:   ' + (atime(etime))(0)
widget_control, wtdur, set_value= $
   (strtrim(string(etime-stime,form='(f12.3)'),2) )(0)
widget_control, wcdur, set_value='Duration: ' + $
   (strtrim(string(anytim(etime,/sec)-anytim(stime,/sec),form='(f12.3)'),2))(0)
if stime ne oldstime or etime ne oldetime then begin
   tempr = fltarr(1)
   emis = fltarr(1)
endif
goto, goback

exit:
;xbackregister,'flash_bck',base,/unregister ;-- kill flashing widget
widget_control, event.top, /destroy
cleanplot  ; set !p,!x,!y,!z back to starting values
goto,getout

goback:
tmpsat = sat < 11
for i=0,n_elements(satbut)-1 do widget_control,satbut(i), set_button=(tmpsat-6) eq i
widget_control, base, /sensitive
widget_control, wcurrent, set_value = $
   'Select time interval, plot options, or command button.'

getout:

end



pro goes, goes_str, group_leader=group, versionrelease=versionrelease, input=input,$
	start_time=start_time, end_time=end_time

common goes_widgets, base, $
                     wcstart, wcend, wcdur, wtstart, wtend, wtdur, wthxrbs,$
                     wstart, wend, whxrbs, wselect, wmulti, wchannels, $
		     wavg, r3a1_emis, r3a1_flux, r3a1_temp,$
                     satbut, llbut, prbut, chbut, rawbut, tmpbut, embut, $
                     wmessage, wcurrent, $
		     wxmin_f, wxmax_f, wxauto_f, wymin_f, wymax_f, wyauto_f, $
		     wxmin_t, wxmax_t, wxauto_t, wymin_t, wymax_t, wyauto_t, $
		     wxmin_e, wxmax_e, wxauto_e, wymin_e, wymax_e, wyauto_e

common goes_plot, goes_window, want_plotfile, done_plot, tekfile, psfile, $
                  logplot, ch_select, raw, clean, markbad, deriv, flxsback, $
                  tmpsback, tmpnosback, tmpmarkbad, $
                  emsback, emnosback, emmarkbad, $
                  stime, etime, tarray, yarray, yclean, yderiv,tempr, emis,$
                  nosubemis, nosubtempr, ch0_bad, ch1_bad, $
                  utbase, overlay, combch, overch, flarenum, $
                  prevplot, xzoom, yzoom_f, yzoom_t, yzoom_e, sat, n_pts, $
		  ave_tarray, ave_yclean, ave_emis, ave_tempr, $
		  ave_nosubemis, ave_nosubtempr

common savegoes, savesat, savestime, saveetime, savetarray, saveyarray, $
       		saveyclean, savech0_bad, savech1_bad, loedges, hiedges, readme

common goes_back, sback_str, eback_str, avback
if n_elements(input) gt 0 then begin
	tags=tag_names(input)
	for i=0,n_elements(tags)-1 do extest = execute(tags(i)+'=input.(i)')
	endif
stime = (anytim(/sec, fcheck(fcheck( start_time, stime), savestime)))(0)
etime = (anytim(/sec, fcheck(fcheck( end_time, etime), saveetime)))(0)

checkvar, readme, [ ' ASCIIBASE - base time in ASCII format  	',$
			  'AVE_EMIS - (if more than one array element) - '+$
				'averaged emis	   		',$
			  'AVE_NOSUBEMIS - (if more than one array element) - '+$
				'ave. emis, no bkg subtracted', $
			  'AVE_NOSUBTEMPR - (if more than one array elem.) - '+$
				'ave. tempr, no bkg subtracted', $
			  'AVE_TARRAY - (if more than one array element) - '+$
				'averaged tarray	  ',$
			  'AVE_TEMPR - (if more than one array element) - '+$
				'averaged tempr		  ',$
			  'AVE_YCLEAN - (if more than one array element) - '+$
				'averaged yclean       	  ',$
                          'CH0_BAD - element #s in YCLEAN, TEMPR, EMIS'+$
                           ' that were interpolated for Chan 1',$
                          'CH1_BAD - element #s in YCLEAN, TEMPR, EMIS'+$
                           ' that were interpolated for Chan 2', $
			  'EMIS (if more than one array element) - '+$
                          'emission measure in 10^49 cm^-3',$
			  'NOSUBEMIS - emis, no bkg subtracted			', $
			  'NOSUBTEMPR - tempr, no bkg subtracted                        ', $
                          'N_PTS - sample average                         		',$
			  'SATELLITE - satellite: GOES 6, 7, 8 or 9	    ', $
			  'TARRAY - time in sec since base time     ',$
                          'TEMPR (if more than one array element) - '+$
                           'temperature in MegaKelvin',$
			  'UTBASE - base time in sec since 79/1/1,0',$
			  'YCLEAN - channels 1 and 2 with gain change '+$
                           'spikes smoothed out',$
			  'YDERIV - time derivative for channels 1 and 2']
checkvar, versionrelease, !version.release
hxrbs_format, old_format = old_time_format
if (!d.flags and 65536) eq 0 then message,'Widgets are unavailable'
set_plot,xdevice()

if n_elements(group) eq 0 then widget_control, /reset

set_graphics,printer='PS'
checkvar, stime, utime('80/5/21,2050')
checkvar, etime, utime('80/5/21,2140')
checkvar,savestime, -1 & checkvar, saveetime, -1
checkvar,savesback, -1 & checkvar, saveeback, -1
want_plotfile = 1
done_plot = 0
prevplot = 'none'
tekfile = 0 & psfile = 0
raw = 0 & clean = 1
deriv = 0
markbad = 1
flxsback = 0
tmpsback = 1 & tmpnosback = 0 & tmpmarkbad = 1
emsback = 1 & emnosback = 0 & emmarkbad = 1
logplot = 1
!p.multi = [0,0,0,0,0]
ch_select = intarr(2) + 1
xzoom = [0., 0.]
yzoom_f = [0., 0.]
yzoom_t = [0., 0.]
yzoom_e = [0., 0.]
sback_str = '80/1/1,0'
checkvar,avback, [0,0]
device, get_screen_size = sc
fspace = .0146 * sc(0) * .5
fxpad = .0117 * sc(0) * .5
fypad = .0146 * sc(1) * .5
checkvar,savesat,6
checkvar,sat,6
n_pts=1
ave_emis=fltarr(1)
ave_nosubemis=fltarr(1)
ave_tempr=fltarr(1)
ave_nosubtempr=fltarr(1)
checkvar, initial_backg_state, 0

welcome = 'Welcome to the GOES Plot Workbench'
btitle = 'GOES Plot Workbench'
;base = widget_base (title=btitle, xpad=fxpad, ypad=fypad*2, $
;                    space=fspace, /column, /frame)
base = widget_base (title=btitle, /column, /frame)
;-------------------------------------------------------------------------
;		0th row
;-------------------------------------------------------------------------
r0 = widget_base (base, /column, /frame)
whattodo = 'Select time interval, plot options, or command button.'
wcurrent = widget_label (r0, value=whattodo, uvalue='BACKGROUND')
;widget_flash, whattodo, wcurrent
;-------------------------------------------------------------------------
;		1st row
;-------------------------------------------------------------------------
;r1a = widget_base (base, /row, /frame, space=4.*fspace)
r1a = widget_base (base, /row, /frame)
;-------------------------------------------------------------------------
;		1st row  - Satellite
;-------------------------------------------------------------------------
;
satbut = lonarr(6)
xmenu, ['GOES 6','GOES 7','GOES 8','GOES 9','GOES10', 'GOES12'], r1a, $
       uvalue=['g_6','g_7','g_8','g_9','g_10','g_12'], buttons = satbut, $
       base = wsatellite, /column, /exclusive, title = 'Satellite:'
widget_control,satbut( (sat<11)-6), /set_button ; skips 11, so use 11 for 12
;-------------------------------------------------------------------------
;		1st row - Times selected
;-------------------------------------------------------------------------
r1curr = widget_base (r1a, /column, /frame)
w = widget_label (r1curr, value=' ')
w = widget_label (r1curr, value='Current start/end times selected:  ')
wcstart = widget_label (r1curr, value='Start: ' + atime(stime))
wcend = widget_label (r1curr, value='End:    ' + atime(etime))
wcdur = widget_label (r1curr, value='Duration: ' + $
   strtrim(string(etime-stime,form='(f12.3)'),2) )
;-------------------------------------------------------------------------
;		1st row - Choose date and time
;-------------------------------------------------------------------------
r1type = widget_base (r1a, /column)
w = widget_label (r1type, value = 'Type selection and  !! PRESS RETURN !! :')
w = widget_base (r1type, /row)
w1 = widget_label (w, value='Start time:      ')
wtstart = widget_text (w, /edit, value=atime(stime))
w = widget_base (r1type, /row)
w1 = widget_label (w, value='End time:        ')
wtend = widget_text (w, /edit, value=atime(etime))
w = widget_base (r1type, /row)
w1 = widget_label (w, value='Duration (s):   ')
wtdur = widget_text (w, /edit, $
   value=strtrim(string(etime-stime,form='(f12.3)'),2))
w = widget_base (r1type, /row)
;w1 = widget_label (w, value='HXRBS flare #: ')
;wthxrbs = widget_text (w, /edit, value='538')

;r1wid = widget_base (r1a, /column, space=2*fspace)
r1wid = widget_base (r1a, /column)
w = widget_label (r1wid, value = 'or  Use widgets:')
wstart = widget_button (r1wid, value='Start time')
wend = widget_button (r1wid, value='End time  ')
;whxrbs = widget_button (r1wid, value='HXRBS flare #')
;--------------------------------------------------------------------------
;		2nd row - Multiple plots
;--------------------------------------------------------------------------
;r2 = widget_base (base, /row, space=fspace*2., xpad = fxpad*2.)
r2 = widget_base (base, /row)

;r2c1 = widget_base (r2, /column, /frame, space=2*fspace)
r2c1 = widget_base (r2, /column, /frame)
w = widget_label (r2c1, value='Multiple Plots:  ')
r1bb = widget_base (r2c1, /row)
wmulti = lonarr (2,10)
wmulti(0,0) = widget_button(r1bb, value='1', menu=2)
for j=1,9 do begin
   val = string(j,format='(i1)')
   wmulti(0,j) = widget_button (wmulti(0,0), $
                 value=val, uvalue='m_'+val)
endfor
w = widget_label (r1bb, value=' x ')
wmulti(1,0) = widget_button(r1bb, value='1', menu=2)
for j=1,9 do begin
   val = string(j,format='(i1)')
   wmulti(1,j) = widget_button (wmulti(1,0), $
                 value=val, uvalue='n_'+val)
endfor
wnewplot = widget_button (r2c1, value='Clear Multiple Plot', $
   uvalue='c_Newplot')
;--------------------------------------------------------------------------
;		2nd row - y-axis type
;--------------------------------------------------------------------------
llbut = lonarr(2)
xmenu, ['Linear', 'Logarithmic'], r2, $
       uvalue=['l_0', 'l_1'], buttons = llbut, $
       base = wloglin, /column, /exclusive, title='Y axis:'
widget_control, llbut(1), /set_button
;-------------------------------------------------------------------------
;		2nd row - Hardcopy
;-------------------------------------------------------------------------
prbut = lonarr(3)
xmenu, ['None', 'PostScript', 'Tektronix'], r2, $
       uvalue=['p_none', 'p_ps', 'p_tek'], buttons=prbut, $
       title='Hardcopy Format:', $
       base = wprinter, /column, /exclusive
widget_control, prbut(1), /set_button
;
;-------------------------------------------------------------------------
;		2nd row - Sample average
;-------------------------------------------------------------------------
r2c4=widget_base(r2,/column,/frame)
w = widget_label (r2c4, value='Sample Avg.: ')
val=['1 pt','10 pts','20 pts','30 pts','40 pts',"User's choice"]
uval=['1_pt','10_pts','20_pts','30_pts','40_pts','choose_pts']
widget_type=(['widget_list','widget_droplist'])(versionrelease ge 4)
wavg=call_function(widget_type, r2c4, value=val,uvalue=uval)
;---------------------------------------------------------------
;		3rd row
;---------------------------------------------------------------
;r3 = widget_base (base, /row, space=fspace*2.)
r3 = widget_base (base, /row)
r3col1 = widget_base (r3, /column, /frame)

if versionrelease lt 4 then  r31 = widget_base(r3col1, /row)
;
pl_val=['Flux','Temp.','Em. Meas.']
if versionrelease ge 4 then $
	pl_type = call_function( 'widget_droplist', r3col1, value=pl_val(0), $
                          title=' Plot Type : ', uvalue=pl_val) else begin
	pl_label= widget_label(r31, value=' Plot Type : ')
	pl_type = call_function( 'widget_list', r31, xsize=20, value=pl_val(0), $
                           uvalue=pl_val)
	endelse
;
; These next two text widgets are just space holders
;
if versionrelease lt 4 then begin
r3_a = widget_label(r31,value='')
r3_b = widget_label(r31,value='')
endif
widget_control,pl_type,set_value=pl_val
;---------------------------------------------------------------
;		3rd row - Flux
;---------------------------------------------------------------
r3a1 = widget_base (r3col1)
r3a1_flux = widget_base(r3a1,/column, map=1)
r3b1=widget_base(r3a1_flux,/row)
chbut = lonarr(2)
xmenu, ['1', '2'], r3b1, $
       uvalue = indgen(2), buttons = chbut, $
       base = wchannels, /nonexclusive, /column, title='Channels:'
widget_control, chbut(0), /set_button
widget_control, chbut(1), /set_button
;
rawbut = lonarr(5)
xmenu, ['Raw','Clean','Bkgd subtracted','Derivative','Mark bad'],r3b1, $
       uvalue = indgen(5), buttons = rawbut, $
       /nonexclusive, /column, title='Options:'
widget_control, rawbut(1), /set_button
widget_control, rawbut(4), /set_button
if initial_backg_state then widget_control, rawbut(2), /set_button
;
r3b1c2 = widget_base (r3b1, /column, /frame)
w = widget_label (r3b1c2, value='Set axis ranges and  !! PRESS RETURN !! : ' + $
   ' (0. means autoscale)')
r3b1c2r1 = widget_base (r3b1c2, /row)
r3b1c2c1 = widget_base (r3b1c2r1, /column)
w = widget_label (r3b1c2c1, value='Enter x limits as hhmm:ss')
wxauto_f = widget_button (r3b1c2c1, value='Autoscale x axis', uvalue='autox')
w = widget_base (r3b1c2c1, /row)
w1 = widget_label (w, value='X min:')
wxmin_f = widget_text (w, /edit, value='0.')
w = widget_base (r3b1c2c1, /row)
w1 = widget_label (w, value='X max:')
wxmax_f = widget_text (w, /edit, value='0.')
;
r3b1c2c2 = widget_base (r3b1c2r1, /column)
w = widget_label (r3b1c2c2, value=' ')
wyauto_f = widget_button (r3b1c2c2, value='Autoscale y axis', uvalue='autoy')
w = widget_base (r3b1c2c2, /row)
w1 = widget_label (w, value='Y min:')
wymin_f = widget_text (w, /edit, value='0.')
w = widget_base (r3b1c2c2, /row)
w1 = widget_label (w, value='Y max:')
wymax_f = widget_text (w, /edit, value='0.')
;
wfplot = widget_button (r3a1_flux, $
value='$$$$$$$$$$$$$$$$$$$$$$$$$$  Do Plot  $$$$$$$$$$$$$$$$$$$$$$$$$$', uvalue='c_FPlot')
;-------------------------------------------------------------------------
;		3rd row - Temperature
;-------------------------------------------------------------------------
r3a1_temp = widget_base(r3a1,/column, map=0)
r3b1=widget_base(r3a1_temp, /row)
tmpbut = lonarr(3)
xmenu, ['Bkgd subtracted', 'Bkgd not subtracted', 'Mark bad'], r3b1, $
       uvalue = indgen(3), buttons = tmpbut, $
       /nonexclusive, /column, title='Options:'
widget_control, tmpbut(0), /set_button
widget_control, tmpbut(2), /set_button
;
r3b1c2 = widget_base (r3b1, /column,/frame)
w = widget_label (r3b1c2, value='Set axis ranges and  !! PRESS RETURN !! : ' + $
   ' (0. means autoscale)')
r3b1c2r1 = widget_base (r3b1c2, /row)
r3b1c2c1 = widget_base (r3b1c2r1, /column)
w = widget_label (r3b1c2c1, value='Enter x limits as hhmm:ss')
wxauto_t = widget_button (r3b1c2c1, value='Autoscale x axis', uvalue='autox')
w = widget_base (r3b1c2c1, /row)
w1 = widget_label (w, value='X min:')
wxmin_t = widget_text (w, /edit, value='0.')
w = widget_base (r3b1c2c1, /row)
w1 = widget_label (w, value='X max:')
wxmax_t = widget_text (w, /edit, value='0.')
;
r3b1c2c2 = widget_base (r3b1c2r1, /column)
w = widget_label (r3b1c2c2, value=' ')
wyauto_t = widget_button (r3b1c2c2, value='Autoscale y axis', uvalue='autoy')
w = widget_base (r3b1c2c2, /row)
w1 = widget_label (w, value='Y min:')
wymin_t = widget_text (w, /edit, value='0.')
w = widget_base (r3b1c2c2, /row)
w1 = widget_label (w, value='Y max:')
wymax_t = widget_text (w, /edit, value='0.')
;
wtplot = widget_button (r3a1_temp, $
value='$$$$$$$$$$$$$$$$$$$$$$$$$$  Do Plot  $$$$$$$$$$$$$$$$$$$$$$$$$$', uvalue='c_TPlot')
;------------------------------------------------------------------------
;		3rd row - Emission Measure
;------------------------------------------------------------------------
r3a1_emis = widget_base (r3a1, /column, map=0)
r3b1=widget_base(r3a1_emis, /row)
embut = lonarr(3)
xmenu, ['Bkgd subtracted', 'Bkgd not subtracted', 'Mark bad'], r3b1, $
       uvalue = indgen(3), buttons = embut, $
       /nonexclusive, /column, title='Options:'
widget_control, embut(0), /set_button
widget_control, embut(2), /set_button
;
r3b1c2 = widget_base (r3b1, /column, /frame)
w = widget_label (r3b1c2, value='Set axis ranges and  !! PRESS RETURN !! : ' + $
   ' (0. means autoscale)')
r3b1c2r1 = widget_base (r3b1c2, /row)
r3b1c2c1 = widget_base (r3b1c2r1, /column)
w = widget_label (r3b1c2c1, value='Enter x limits as hhmm:ss')
wxauto_e = widget_button (r3b1c2c1, value='Autoscale x axis', uvalue='autox')
w = widget_base (r3b1c2c1, /row)
w1 = widget_label (w, value='X min:')
wxmin_e = widget_text (w, /edit, value='0.')
w = widget_base (r3b1c2c1, /row)
w1 = widget_label (w, value='X max:')
wxmax_e = widget_text (w, /edit, value='0.')
;
r3b1c2c2 = widget_base (r3b1c2r1, /column)
w = widget_label (r3b1c2c2, value=' ')
wyauto_e = widget_button (r3b1c2c2, value='Autoscale y axis', uvalue='autoy')
w = widget_base (r3b1c2c2, /row)
w1 = widget_label (w, value='Y min:')
wymin_e = widget_text (w, /edit, value='0.')
w = widget_base (r3b1c2c2, /row)
w1 = widget_label (w, value='Y max:')
wymax_e = widget_text (w, /edit, value='0.')
;
weplot = widget_button (r3a1_emis,$
 value='$$$$$$$$$$$$$$$$$$$$$$$$$$  Do Plot  $$$$$$$$$$$$$$$$$$$$$$$$$$', uvalue='c_EPlot')
;-------------------------------------------------------------------------
;		3rd row - Other buttons
;-------------------------------------------------------------------------
;r5 = widget_base (base, /row, space=fspace*2., xpad=fxpad*2, ypad=fypad*2.)
;r5 = widget_base (r3, /column, space=fspace, xpad=fxpad*2, ypad=fypad)
;
r5 = widget_base (r3, /column)
wpoint = widget_button (r5, value='Point', uvalue='c_Point')
wzoom = widget_button (r5, value='Zoom', uvalue='c_Zoom')
wprint = widget_button (r5, value='Hardcopy', uvalue='c_Hardcopy')
wfile = widget_button (r5, value='Write File', uvalue='c_Writefile')
wquit = widget_button (r5, value='       QUIT       ', uvalue='c_Quit', /frame)
;------------------------------------------------------------------------
;		4th row - Message window
;------------------------------------------------------------------------
r6 = widget_base (base, /column, /frame)
wm = widget_label (r6, value='Message Window')
wmessage = widget_text (r6, /scroll, ysize=3, xsize=90)
widget_control,wmessage,/app,set_v='Remember to press RETURN key after ' + $
   'entering selection via text widget.'
widget_control, base, /realize
widget_control,wmessage,/app,set_v='GOES 6 data range: 80/01/04 - 94/08/18 '
;------------------------------------------------------------------------
;xbackregister, 'flash_bck', base
;print,'xregistered? ', xregistered('flash_bck')
;xmanager, 'goes', base, group_leader=group, background='flash_bck'
xmanager, 'goes', base, group_leader=group

if old_time_format eq 'YOHKOH' then yohkoh_format
if n_params() ge 1 then begin
if n_elements(tarray) gt 0 then begin
time = anytim(/ints,getutbase()+tarray)
goes_gen ={goes_data,  satellite:sat, time:time(0).time,day:time(0).day, lo: 0.0, hi: 0.0,$
	emis:0.0,  tempr:0.0}
goes=replicate( goes_gen, n_elements(tarray))
goes.time = time.time
goes.day  = time.day
goes.lo   = (yclean(*,0) - avback(0))(*)
goes.hi   = (yclean(*,1) - avback(1))(*)

if n_elements(emis) eq n_elements(goes.emis) then begin
	goes.emis = emis
	goes.tempr= tempr
endif
checkvar, readme1, [ 'These are the tags and their definition in this structure.',$
'UTBASE - base time in seconds from 1-jan-1979 for AVE_TARRAY',$
'BACKGROUND - background used for Long and Short Wavelength Channels.', $
'N_PTS 	    - number of samples used to average.',$
'CH0_BAD - element #s in YCLEAN, TEMPR, EMIS that were interpolated for Chan 1', $
'CH1_BAD - element #s in YCLEAN, TEMPR, EMIS that were interpolated for Chan 2', $
'In Goes_str.gen,                             ', $
'SATELLITE - satellite: GOES 6, 7, 8, 9, or 10           ', $
'TIME - time in millisec since start of day     ', $
'DAY  - day since 1-jan-1979                    ', $
'LO   - GOES long wavelength channel with spikes interpolated over, see CH0_BAD for indeces, background subtracted.',$
'HI   - GOES short wavelength channel with spikes interpolated over, see CH1_BAD for indeces, background subtracted.',$
'The next two quantities are derived using GOES_TEM.PRO', $
'TEMPR (if more than one array element) - temperature in MegaKelvin', $
'EMIS (if more than one array element) - emission measure in 10^49 cm^-3', $

'If N_PTS, the sample average, used is greater than 1,then Goes_avg is included;',$
'SATELLITE - satellite: GOES 6, 7, 8, 9, or 10           ', $
'TIME - time in millisec since start of day     ', $
'DAY  - day since 1-jan-1979                    ', $
'LO   - GOES long wavelength channel with spikes interpolated over, see CH0_BAD for indeces, background subtracted.',$
'HI   - GOES short wavelength channel with spikes interpolated over, see CH1_BAD for indeces, background subtracted.',$
'The next two quantities are derived using GOES_TEM.PRO', $
'TEMPR (if more than one array element) - temperature in MegaKelvin', $
'EMIS (if more than one array element) - emission measure in 10^49 cm^-3']

if n_pts eq 1 then goes_str = { gen: goes,  readme: byte(readme1), utbase: getutbase(), $
	background: avback, ch0_bad:ch0_bad, ch1_bad:ch1_bad} else begin
	goes_avg = replicate(goes_gen, n_elements(ave_tarray))
	goes_avg.satellite = goes(0).satellite
	time = anytim(/ints,getutbase()+ave_tarray)
	goes_avg.time = time.time
	goes_avg.day  = time.day
	goes_avg.lo   = (ave_yclean(*,0) - avback(0))(*)
	goes_avg.hi   = (ave_yclean(*,1) - avback(1))(*)

	if n_elements(ave_emis) eq n_elements(goes_avg.emis) then begin
	goes_avg.emis = ave_emis
	goes_avg.tempr= ave_tempr
	endif
	goes_str = { gen: goes,   readme: byte(readme1), utbase: getutbase(), $
	background: avback, ch0_bad:ch0_bad, ch1_bad:ch1_bad, avg:goes_avg}
	endelse



endif else goes_str=0
endif

end
