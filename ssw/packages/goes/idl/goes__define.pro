;+
; Project     : HESSI
;
; Name        : GOES__DEFINE
;
; Purpose     : Define a GOES lightcurve object
;
; Category    : Ancillary GBO lightcurves
;
; Explanation : Object wrapper around GOESPLOT
;		All keywords to goesplot can be passed to plot method
;		(e.g. xrange, yrange, n_pts, deriv, log)

; Syntax      : This procedure is invoked when a new GOES object is
;               created via:
;
;               IDL> new=obj_new('goes')
; Examples    :
;		o = obj_new('goes')
;		o -> set, tstart='98/3/13 00', tend='98/3/14, 00'
;		o -> plot
;		params = o -> get()
;		data = o -> get(/data)
;
; Inputs      : 'GOES' = object classname
;
; Methods	get(): retrieve control parameters and/or the data
;		set: set control parameters
;		getdata: get GOES data
;		plot: get and plot GOES data
;
; Keywords	tstart - start time in any anytim format
;		tend - end time in any anytim format
;		yohkoh - 0/1 look in sdac and yohkoh db's / just yohkoh db
;		time - array of times for each data point
;		data [get] - returns 2 channels of GOES data
;		lo [get] - returns low energy channel data
;		hi  [get] - returns high energy channel data
;		trange [get] - returns time range of data (dblarr(2))
;		Note: call to get can pass keyword controlling format of
;			times (e.g. /ecs or /hxrbs or /vms). Default is sec.
;
;                   ;
; History     : Written 20 July 1999, D. Zarro, SM&A/GSFC
;		Mod. 12 Apr 2000, Kim.  Use goesplot instead of plot_goes.
;			Primary source of data will be sdac fits files,
;			secondary will be yohkoh db files.  Change methods
;			to standard set,get,getdata,plot
;
; Contact     : dzarro@solar.stanford.edu
;-

;---------------------------------------------------------------------------

function goes::init, err=err, _extra=extra

self.dstart = anytim(sys2ut(), /date_only, /sec)
self.dend = self.dstart + 86400.d0
self.yohkoh = 0

if keyword_set(extra) then self -> set, _extra=extra

return,1

end

;----------------------------------------------------------------------------

pro goes::cleanup
ptr_free,self.pointer
end

;---------------------------------------------------------------------------

pro goes::help
iprint,obj_methods('goes')
return & end

;---------------------------------------------------------------------------
;-- GOES set method

pro goes::set, tstart=tstart, tend=tend, yohkoh=yohkoh

; TSTART = start time [def= start of current day]
; TEND   = end time [def = end of current day]
; yohkoh = 0 means use sdac db first, then try yohkoh.  1 means use yohkoh db.

if keyword_set(tstart) then begin
	self.dstart=anytim(tstart, /sec, err=err)
	if err then self.dstart = anytim(sys2ut(), /date_only, /sec)
endif

if keyword_set(tend) then begin
	self.dend=anytim(tend, /sec, err=err)
	if err then self.dend = dstart + 86400.
endif

if exist(yohkoh) then self.yohkoh = yohkoh

end

;-----------------------------------------------------------------------------
;-- GOES get method

function goes::get, times=ktimes, data=kdata, lo=klo, hi=khi, $
	tstart=ktstart, tend=ktend, $
	yohkoh=kyohkoh, ut_ref=kut_ref, $
	time_range=ktime_range, trange=ktrange, $
	control_parameters=control_parameters, $
	_extra=extra

tstart = anytim(self.dstart, _extra=extra)
tend   = anytim(self.dend, _extra=extra)
yohkoh = self.yohkoh

if self->valid(err) then begin

	times=anytim((*(self.pointer)).tdata,_extra=extra)
	lo = (*(self.pointer)).ydata[*,0]
	hi = (*(self.pointer)).ydata[*,1]
	data = *self.pointer
	ut_ref = (*(self.pointer)).ut_ref
	np = n_elements(*self.pointer)
	time_range = [times[0], times[np-1]]

endif else begin
	time = -1
	lo=-1
	hi=-1
	data=-1
	ut_ref = self.dstart
	time_range=[self.dstart, self.dend] - ut_ref
endelse

if keyword_set(control_parameters) then $
	return, {ut_ref: ut_ref, tstart: tstart, tend: tend, yohkoh: yohkoh}

if keyword_set(ktimes) then result = add_tag(result, times, 'times')
if keyword_set(kdata) then result = add_tag(result, data, 'data')
if keyword_set(klo) then result = add_tag(result, lo, 'lo')
if keyword_set(khi) then result = add_tag(result, hi, 'hi')
if keyword_set(ktstart) then result = add_tag(result, tstart, 'tstart')
if keyword_set(ktend) then result = add_tag(result, tend, 'tend')
if keyword_set(kyohkoh) then result = add_tag(result, yohkoh, 'yohkoh')
if keyword_set(kut_ref) then result = add_tag(result, ut_ref, 'ut_ref')
if keyword_set(ktime_range) then result = add_tag(result, time_range, 'time_range')
if keyword_set(krange) then result = add_tag(result, time_range, 'trange')
if exist(result) then begin
	if n_tags(result) eq 1 then return, result.(0) else return, result
endif else begin
	result = {times: times, $
		data: data, $
		tstart: tstart, $
		tend: tend, $
		trange: time_range, $
		yohkoh: yohkoh, $
		time_range: time_range }
	return, result
endelse

end

;---------------------------------------------------------------------------
;-- GOES plot method

pro goes::plot, err=err, saved_obj_data=data, _extra=extra

error = 0

if not keyword_set(data) then $
	data = self -> getdata (_extra=extra, plot=0, error=error)

if not error then begin

	gplot, data.tdata, data.ydata, data.sat, data.ut_ref, data.n_pts, $
		/ylog, _extra=extra

endif

return & end

;---------------------------------------------------------------------------
;-- GOES getdata method

function goes::getdata, error=error, _extra=extra, plot=plot, quiet=quiet

if keyword_set(plot) then begin
	self -> plot, _extra=extra
	if self->valid(err) then return,*(self.pointer) else return, -1
endif

if keyword_set(extra) then self -> set, _extra=extra

;-- stuff plot in Z-buffer if just reading

if not keyword_set(quiet) then begin
 print,'% GOES start time: ' + anytim(self.dstart, /ecs)
 print,'% GOES end time:   ' + anytim(self.dend, /ecs)
endif

;no_plot=1-keyword_set(plot)
;if no_plot then begin
; psave=!d.name
; smart_window,!d.window,/set,/noshow
; set_plot,'z'
;endif

sat = 0
n_pts=1		;force n_pts to one so plotman or user can control with !p.nsum

savmulti = !p.multi
goesplot, stime=self.dstart, etime=self.dend, $
	ave_tarray=ave_tarray, ave_yclean=ave_yclean, /log, $
	/cleaned, sat=sat, sdac=(self.yohkoh eq 0), yohkoh=self.yohkoh, $
	/noplot, base_sec=base_sec, _extra=extra, n_pts=n_pts, $
	error=error, err_msg=err_msg
!p.multi = savmulti
if not error then begin

	; store data in array of structures
	ut_ref = base_sec

	data = { $
		sat: sat, $
		n_pts: n_pts, $
		ut_ref: base_sec, $
		tdata: anytim(ave_tarray + base_sec, /sec) - ut_ref, $
		ydata: ave_yclean }

	if not ptr_valid(self.pointer) then self.pointer=ptr_new(/all)

	*(self.pointer)=temporary(data)

	return, *(self.pointer)

endif else begin

	message, err_msg, /cont
	return, -1

endelse

;if no_plot then begin
; set_plot,psave
; smart_window,!d.window,/noshow
;endif




;-- stuff output in pointer for later GET


end

;---------------------------------------------------------------------------

function goes::valid,err

err=''
if not ptr_valid(self.pointer) then begin
 err='first use READ method to read GOES data'
 return,0b
endif else return,1b

end

;---------------------------------------------------------------------------
;-- define GOES object

pro goes__define

pointer=ptr_new()
goes_struct={goes, $
	dstart: 0.d0, $
	dend: 0.d0, $
	yohkoh: 0, $
	pointer:pointer, $
	inherits gen}

return & end


