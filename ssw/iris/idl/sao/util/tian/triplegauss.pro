;;             NAME : triplegauss
; 
;          PURPOSE : Triple Gaussian function. Called by tgf_1lp.pro.
;
; CALLING SEQUENCE : tg =  triplegauss(xarr,p)
; 
;           INPUTS : xarr - wavelength vector
;                    
;                    p - triple gaussian parameters, [background, 1st component peak intensity/centroid/width, 2nd component peak intensity/centroid/width, 3rd component peak intensity/centroid/width]
;
;          HISTORY : Written by Hui Tian at CfA, April 4, 2013

function triplegauss, xarr, p
 
  model = p[0] + p[1]*exp(-((xarr - p[2])/p[3])^2) + p[4]*exp(-((xarr - p[5])/p[6])^2) + p[7]*exp(-((xarr - p[8])/p[9])^2)

  return, model
end