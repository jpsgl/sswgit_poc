;             NAME : gen_rb_profile
; 
;          PURPOSE : Get red wing and blue wing intensities as a function of velocity (spectral distance from line centroid). 
;                    The result will be used to build an RB asymmetry profile (see EXAMPLE below). The RB asymmetry analysis technique was originally 
;                    developped by De Pontieu et al. (2009, ApJ, 701, L1) and later modified by Tian et al. 2011, ApJ, 738, 18 
;                    (see the definition in Section 2).
;
; CALLING SEQUENCE : rbstr = gen_rb_profile(v, profile, steps, dv)
; 
;           INPUTS : v - vector of velocity from line centroid
;                    
;                    profile - line profile
;                    
;                    steps - velocity steps, e.g., [10,20,30,40,...]
;                    
;                    dv - size of velocity bin, e.g., 20 km/s
;                     
;          OUTPUTS : a structure containing the following tags:
;                    red: red wing intensities 
;                    blue: blue wing intensities
;                    steps: same as input parameter steps
; 
;           EXAMPLE: Here x is the vector of wavelength, y is the vector of spectral intensity, nw is the number 
;                    of measured wavelength positions, wave0 is the rest wavelength 
; 
;                    steps = findgen(15)*10. ; RB Steps in km/s
;                    ymax=max(smooth(interpol(y,nw*100,/spline),3),sub_m)  &  xvec=interpol(x,nw*100,/spline)
;                    x_rel = interpol(x,nw*10,/spline)-xvec[sub_m]
;                    rbstr = gen_rb_profile(x_rel/wave0*2.999e5, interpol(y,nw*10,/spline), steps, 20.)
;                    rb= (rbstr.red - rbstr.blue)/ymax  ;RB asymmetry profile
;                    plot,steps+10,rb,title='RBp asymmetry profile',xtitle='Velocity (km/s)',ytitle='RB Asymmetry (percentage)'
;
;          HISTORY : Written by Scott W. McIntosh and commented by Hui Tian at NCAR/HAO, Sept 7, 2010
;
;; **********************************************************************

function gen_rb_profile, v, profile, steps, dv

red = dblarr(n_elements(steps))
blue = dblarr(n_elements(steps))
for kk=0,n_elements(steps)-1 do begin

   lo = steps[kk]
   hi = lo + dv

   BEST_BLUE = WHERE((v GE -hi) AND (v LE -lo), BBC)
   BEST_RED = WHERE((v GE lo) AND (v LE hi), BRC)

   if (bbc gt 0) AND (BRC GT 0) then BEGIN
       blue[kk] = total(profile(best_blue)) / float(bbc)
       red[kk] = total(profile(best_red)) / float(brc)	
   endif

endfor 

str = {red:red, blue:blue, steps:steps}

return, str
end
