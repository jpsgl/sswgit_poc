;             NAME : iris_ne
; 
;          PURPOSE : Derive the electron density from line pair intensity ratio and plot the result
;
; CALLING SEQUENCE : iris_ne,int1,err1,int2,err2,den_file,rat,rat_err,den,den1,den2
; 
;           INPUTS : int1 - intensity of the first line (total line intensity, continuum subtracted), can be a scaler, a vector, or an array of any dimension
;           
;                    err1 - error of the first line intensity
;           
;                    int2 - intensity of the second line
;                    
;                    err2 - error of the second line intensity
;                    
;                    den_file - file of the theoretical relationship between density and ratio. Here is an example on how to 
;                    generate this file:
;                    
;                    density_ratios,'fe_14',264.,275.,7.,12.,den,rat,desc
;                    d=interpol(alog10(den),210,/spline)
;                    r=interpol(rat,210,/spline)
;                    save,filename='fe14_den.sav',d,r
; 
;         KEYWORDS : posi - position of the plot
;                    
;                    ytitle -  lable of y-cooridinate of the density-ratio plot, e.g., 'Ratio of Fe XIV 264/274'
;                    
;                    xrange - range of the x-coordinate of the plot
;                     
;                    savedat - save the result
;
;                    savefig - save the plot
;                   
;          OUTPUTS : rat - line ratio
;                    
;                    rat_err - error of the ratio
;                    
;                    den - density
;                    
;                    den1 - lower bound of the density
;                    
;                    den2 - upper bound of the density
;
;          HISTORY : Written by Hui Tian at CfA, April 8, 2013
;
;; **********************************************************************

Pro iris_ne,int1,err1,int2,err2,den_file,rat,rat_err,den,den1,den2,ytitle=ytitle,posi=posi,xrange=xrange,savedat=savedat,savefig=savefig


!p.font=-1

if not(keyword_set(ytitle)) then ytitle = 'Ratio'
if not(keyword_set(posi)) then posi=[0.10,0.10,0.98,0.95]
if not(keyword_set(xrange)) then xrange=[7,13]

;theoretical relationship between ratio and density
restore,den_file,/ver

npt=n_elements(int1)

;ratio and error
rat=int1/int2
rat_err=rat*sqrt((err1/int1)^2+(err2/int2)^2) 

;density and error
den=fltarr(npt) & den1=fltarr(npt) & den2=fltarr(npt)
for i=0,npt-1 do begin
rmin=min(abs(r - rat[i]),sub) 
den[i]=d[sub]
rmin=min(abs(r - (rat[i]-rat_err[i])),sub) 
den1[i]=d[sub]
rmin=min(abs(r - (rat[i]+rat_err[i])),sub) 
den2[i]=d[sub]
endfor

if npt gt 100 then goto,endprogram

window,0,xs=800,ys=600

;plot the theoretical curve
plot,d,r,xrange=xrange,xstyle=1,position=posi,xtitle='Log (Density / cm!E-3!N)',ytitle=ytitle,charsize=1.5

;plot the calculated densities and errors
if npt gt 1 then begin
  oplot,den,rat,psym=6,symsize=0.5,thick=8
  err_plot,den,rat-rat_err,rat+rat_err,width=0.01,thick=1
  err_plot,rat,den1,den2,width=0.02,thick=1,XDir=1
endif else begin
  oplot,[den,den],[rat,rat],psym=6,symsize=0.5,thick=8
  err_plot,[den,den],[rat,rat]-[rat_err,rat_err],[rat,rat]+[rat_err,rat_err],width=0.01,thick=1
  err_plot,[rat,rat],[den1,den1],[den2,den2],width=0.02,thick=1,XDir=1
endelse

;save the plot
if keyword_set(savefig) then write_png,'Ne.png', tvrd(/true), r, g, b

endprogram:
;save the density and ratio results
if keyword_set(savedat) then save,filename='Ne.sav',rat,rat_err,den,den1,den2
end
