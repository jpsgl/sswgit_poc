; NAME: 
; 
; Gaussfilter
; 
; PURPOSE: 
; 
; Perform Gaussian filtering of a time series. 
; 
; CATEGORY:
; 
; Data analysis
; 
; CALLING SEQUENCES: 
; 
; yf=gaussfilter(y,dt,f0,fw)
;
; yf=gaussfilter(y,dt,f0,fw,/gaussrm)
;
; INPUTS: 
; 
; y:    The input time series
;
; dt:   cadence of the time series, in the unit of second
;
; f0:   Central frequency of the Gaussian, in the unit of Hz
; 
; fw:   Width of the Gaussian, in the unit of Hz
; 
; OPTIONAL INPUTS: 
; 
; None.
; 
; KEYWORDS:
; 
; gaussrm: Gaussian removed from the original time series if set. Default is Gaussian pass.
;
; OUTPUTS: 
; 
; The filtered time series.
; 
; EXAMPLE:
;  
;IDL> x=findgen(3000)  &  y=sin(x*2*!pi/300)  &  plot,x,y
;IDL> f0=1./300   &   fw=0.00002   &  dt=1                              
;IDL> plot,gaussfilter(y,dt,f0,fw,/gaussrm)
;IDL> plot,gaussfilter(y,dt,f0,fw)
;
; HISTORY: 
; 
; Written by Hui Tian at CfA, 23 March 2013
;

Function Gaussfilter,y,dt,f0,fw,gaussrm=gaussrm

  nt=n_elements(y)
  freq=dindgen(nt)/(double(nt)*dt)
  
  for i=0,nt/2-1 do freq(nt-i-1)=freq(i+1)
  filterf=exp( -(freq-f0)^2/fw^2 )
  filterf(0)=0.

  if keyword_set(gaussrm) then filterf=1-filterf

  trans=filterf*fft(y)
  yf=fft(trans,/inverse)
  
  return,yf
End
