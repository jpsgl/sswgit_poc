;             NAME : iris_orbitvar_corr_anyline_l1p5
;             
;           PROJECT: IRIS
; 
;          PURPOSE : Derive the spectral pixel position of a line
;
; CALLING SEQUENCE : iris_orbitvar_corr_anyline_l1p5,files,lineid,wave0,wmin,wmax,w_orb,w_orb_nosc,date_obs
; 
;           INPUTS : files - names of IRIS level 1p5 files
;                    
;                    lineid - the selected line id, e.g., 'NiI2799.474'
;                    
;                    wave0 - the rest wavelength of the selected line in the unit of Angstrom, e.g., 2799.474
;                    
;                    wmin, wmax - wavelength pixel range (around the selected line) selected for Gaussian fitting, 
;                    e.g., wmin=652, wmax=673. Values are in the case of no summing. Make sure that the line is nearly
;                    centered in this range
; 
;         KEYWORDS : showfit - show the Gaussian fitting result
; 
;          OUTPUTS : w_orb - the line position after subtracting the S/C velocity. The unit is unsummed wavelength
;                             pixel of the spectra.
;
;                    w_orb_nosc - the line position before subtracting the S/C velocity. The unit is unsummed wavelength
;                             pixel of the spectra.         
;                   
;                    date_obs -  the vector of observation times
;
;                    sumspec  - spectral summing
;
;                    rollang  - roll angle
;
;                    ophase   - orbital phase
;
;                    saved into the file of datetime+'_orbitvar_'+lineid+'.genx', can be loaded using
;                    restgen,w_orb,date_obs,w_orb_nosc,sumspec,rollang,ophase,file=datetime+'_orbitvar_'+lineid+'.genx'
;
;          HISTORY : Oct 20, 2013: written by HUI TIAN at CfA
;
;; **********************************************************************

pro iris_orbitvar_corr_anyline_l1p5,files,lineid,wave0,wmin,wmax,w_orb,w_orb_nosc,date_obs,$
    showfit=showfit

  
  ;; number of level 1p5 files
  nt=n_elements(files)
  
  ;; ----------------------------------------------------------------------
  ;; derive the orbital variation 
  
  ;; averaged positions of the line at different times
  mean_wpos_nosc=dblarr(nt)
  mean_wpos=dblarr(nt)
  
  ;; observation time of each level 1p5 file
  date_obs=strarr(nt)
  ;; spectral summing
  sumspec=fltarr(nt)
  ;; roll angle
  rollang=fltarr(nt)
  ;; orbital phase
  ophase=fltarr(nt)
  
  if wave0 ge 1500. then posi=-1 else posi=1
  for k=0,nt-1 do begin
    file=files[k]
  
    ;; read a level1.5 data file
    read_iris, file, hdr, dat

    datetime=strmid(files[0],25,17,/rev)
    date_obs[k]=hdr.date_obs

    rollang[k]=hdr.sat_rot
    ophase[k]=hdr.ophase
    sumspec[k]=hdr.SUMSPTRL

    odat=total(dat,2,/nan)
  
    ;odat=congrid(odat,n_elements(odat)*hdr.SUMSPTRL,/minus_one) ;for testing
    ;hdr.SUMSPTRL=1

    ;; wavelength pixel range (around the line) selected for Gaussian fitting
    wmin1=floor(float(wmin)/hdr.SUMSPTRL)  &  wmax1=ceil(float(wmax)/hdr.SUMSPTRL)
    if (wmax1-wmin1) lt 3 then wmax1=wmin1+3  ;you need at least 4 data points for the Gaussian fit
    ii=odat[wmin1:wmax1]

    nw=n_elements(ii)
    xx=findgen(nw)+wmin1  
    if total(ii) le 0 then continue ;check whether the line is included in the window or not
            
        ifit=gaussfit(xx, ii, a, nterms=4)

        ;; find bad fit
        if a[0]*posi lt 0. or $
           finite(a[0]) ne 1 or $
           a[1] gt ((wmin1+wmax1)/2.+8./hdr.SUMSPTRL) or $
           a[1] lt ((wmin1+wmax1)/2.-8./hdr.SUMSPTRL) then a[*]=-!values.f_nan

        ;; show the observed and fitted line profile
        if (keyword_set(showfit)) and finite(a[0]) eq 1 then begin
            plot, xx, ii, xstyle=1, ystyle=1, $
                 title='Gaussian Fit', xtitle='Wavelength pixel', ytitle='Intensity'
            oplot, xx, ifit, color=120
            print,'progress: '+strtrim(string(k),2)+'/'+strtrim(string(nt),2)
            wait, 1
        endif
        
        mean_wpos_nosc[k]=a[1]
        
     
    ;; convert the spacecraft velocity to wavelength pixel, then subtract spacecraft velocity from the line position
    scvelo=hdr.obs_vr 
    ;; spectral pixel size
    if wave0 ge 1500. then specsize=0.02546*hdr.SUMSPTRL  
    if wave0 le 1380. then specsize=0.01298*hdr.SUMSPTRL
    if wave0 ge 1380 and wave0 le 1410 then specsize=0.012728*hdr.SUMSPTRL
    mean_wpos[k]=mean_wpos_nosc[k]-scvelo/(3.e8)*wave0/specsize
  endfor

  w_orb=mean_wpos*hdr.SUMSPTRL+(hdr.SUMSPTRL-1)/2. ;adjust the pix offset caused by summing
  w_orb_nosc=mean_wpos_nosc*hdr.SUMSPTRL+(hdr.SUMSPTRL-1)/2. 
  
  
  time_s=anytim(date_obs)-anytim(date_obs[0])
  
  ;; ----------------------------------------------------------------------
  ;; display the orbital variation 
  window,1,xs=900,ys=450
  linecolors

  ;; plot the curve of the orbital variation derived above
  utplot,date_obs,w_orb,xstyle=1,thick=2,yrange=[wmin+6,wmax-6],ystyle=1,posi=[0.1,0.1,0.9,0.9]

  write_jpeg,datetime+'_orbitvar_'+lineid+'.jpg',tvrd(true=1),true=1,quality=95
  ;; ----------------------------------------------------------------------
  ;; save the result

  savegen,w_orb,date_obs,w_orb_nosc,sumspec,rollang,ophase,file=datetime+'_orbitvar_'+lineid+'.genx'

  
return

end
