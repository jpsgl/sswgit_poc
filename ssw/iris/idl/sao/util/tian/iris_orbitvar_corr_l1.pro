;             NAME : iris_orbitvar_corr_l1
;             
;           PROJECT: IRIS
; 
;          PURPOSE : Corrections for orbital variation of the spectral line positions
;
; CALLING SEQUENCE : iris_orbitvar_corr_l1,files,dw_orb,date_obs,medpos,scvelo
; 
;           INPUTS : files - names of IRIS NUV level 1 files
; 
;         KEYWORDS : ymin, ymax - select a segment (pixel range) in the slit direction for averaging. The default 
;                    range is the central 1/10 portion of the slit.
;                    
;                    nospline - do not do smoothing and spline fitting
;                    to the curve of orbital variation. The spline
;                    can only be done if the duration of the raster is
;                    longer than 300s.
;                    
;                    indiv - Fit individual profiles and then average the line positions. 
;                    Fit the profile averaged over the selected portion of the slit if not set. 
;                    
;                    showfit - show the Gaussian fitting result
; 
;          OUTPUTS : dw_orb - the correction vector for orbital
;                             variation of the Ni I 2799.474 line
;                             position (only the thermal component). The unit is unsummed wavelength
;                             pixel of the NUV spectra. When applying
;                             to the FUV spectra, you need to put a
;                             minus sign (take the negative value) and
;                             consider the possible spectral
;                             summing of FUV spectra.           
;                   
;                    date_obs -  the vector of observation times
;
;                      medpos - median of the line position (pixel) 
;                      
;                      scvelo - S/C velocity at different times
;                  
;                    dw_orb & date_obs & medpos & scvelo saved into the file of datetime+'_orbitvar.genx', can be loaded using 
;                    restgen,dw_orb,date_obs,medpos,scvelo,file=datetime+'_orbitvar.genx'
;
;          HISTORY : Sept 7, 2013: written by HUI TIAN at CfA
;                    Dec 30, 2013: add the output S/C velocity: scvelo
;                    
;; **********************************************************************

pro iris_orbitvar_corr_l1,files,dw_orb,date_obs,medpos,scvelo,$
    ymin=ymin,ymax=ymax,nospline=nospline,showfit=showfit


  ;; rest wavelegnth of the Ni I 2799.474 line, in the unit of Angstrom
  wave0 = 2799.474
  
  ;; number of NUV level 1 files
  nt=n_elements(files)
  
  ;; ----------------------------------------------------------------------
  ;; derive the orbital variation 
  
  ;; averaged positions of the Ni I 2799.474 line at different times
  mean_wpos=dblarr(nt)
  
  ;; S/C velocities at different times
  scvelo=dblarr(nt)
  
  ;; observation time of each level 1 file
  date_obs=strarr(nt)
  
  for k=0,nt-1 do begin
    file=files[k]
  
    ;; read a level1 data file
    read_iris, file, hdr, dat
  
    ;; generate level1.5 data
    iris_prep, hdr, dat, ohdr, odat   
  
    datetime=strmid(files[0],25,17,/rev)
    date_obs[k]=hdr.date_obs

    ;; select the pixel range in the slit direction for averaging, default is the central 1/10 portion of the slit
    if not(keyword_set(ymin)) then ymin = round(hdr.NAXIS2*0.45) else ymin = ymin > 0
    if not(keyword_set(ymax)) then ymax = round(hdr.NAXIS2*0.55) else ymax = ymax < (hdr.NAXIS2-1)
  
    ;; wavelength pixel range (around the NUV Ni I 2799.474 line) selected for Gaussian fitting
    wmin=floor(652./hdr.SUMSPTRL)  &  wmax=ceil(673./hdr.SUMSPTRL)
    if (wmax-wmin) lt 4 then wmax=wmin+4  ;you need at least 5 data points for the Gaussian fit
    odat=odat[wmin:wmax,ymin:ymax]

    nw=(size(odat))[1]
    ny=(size(odat))[2]
    xx=findgen(nw)+wmin  
    
    if keyword_set(indiv) then begin ;; fit individual line profile in the selected slit portion, then average the derived line centriods
      wpos=fltarr(ny)
      for j=2,ny-3 do begin
    
        ;; average over 5 pixels along the slit to improve the S/N
        ii=average(odat[*,j-2:j+2],2) 
  
        ;; perform Gaussian fit to the profile of Ni I 2799.474          
        ifit=gaussfit(xx, ii, a, nterms=5)

        ;; throw out bad fit
        if a[0] gt 0. or $
           finite(a[0]) ne 1 or $
           a[1] gt ((wmin+wmax)/2.+8./hdr.SUMSPTRL) or $
           a[1] lt ((wmin+wmax)/2.-8./hdr.SUMSPTRL) then a[*]=-!values.f_nan

        ;; show the observed and fitted line profile
        if (keyword_set(showfit)) and finite(a[0]) eq 1 then begin
          if j eq round(ny/2.) then begin
            plot, xx, ii, xstyle=1, ystyle=1, $
                 title='Gaussian Fit', xtitle='Wavelength pixel', ytitle='Intensity'
            oplot, xx, ifit, color=120
            print,'progress: '+strtrim(string(k),2)+'/'+strtrim(string(nt),2)
            wait, 0.01
          endif
        endif
        
        wpos[j]=a[1]
 
      endfor

      ;; average the line positions over the selected segment of the slit
      wpos_goodfit=where(wpos gt 0)
      if wpos_goodfit[0] ne -1 then mean_wpos[k]=mean(wpos[wpos_goodfit], /NAN) else mean_wpos[k]=!Values.F_NAN
     
     endif else begin  ;; average all line profiles in the selected range of the slit, then do Gaussian fit
 
        ii=average(odat,2)         
        ifit=gaussfit(xx, ii, a, nterms=5)
        
        ;; find bad fit
        if a[0] gt 0. or $
           finite(a[0]) ne 1 or $
           a[1] gt ((wmin+wmax)/2.+8./hdr.SUMSPTRL) or $
           a[1] lt ((wmin+wmax)/2.-8./hdr.SUMSPTRL) then a[*]=-!values.f_nan

        ;; show the observed and fitted line profile
        if (keyword_set(showfit)) and finite(a[0]) eq 1 then begin
            plot, xx, ii, xstyle=1, ystyle=1, $
                 title='Gaussian Fit', xtitle='Wavelength pixel', ytitle='Intensity'
            oplot, xx, ifit, color=120
            print,'progress: '+strtrim(string(k),2)+'/'+strtrim(string(nt),2)
            wait, 1
        endif
        
        mean_wpos[k]=a[1]
        
     endelse    
     
    ;; convert the spacecraft velocity to wavelength pixel, then subtract spacecraft velocity from the line position
    scvelo[k]=hdr.obs_vr 
    specsize=0.02546*hdr.SUMSPTRL  ;; spectral pixel size
    mean_wpos[k]=mean_wpos[k]-scvelo[k]/(3.e8)*wave0/specsize
  endfor

  ;; relative variation of the line position
  medpos=median(mean_wpos)*hdr.SUMSPTRL
  dw_orb=mean_wpos*hdr.SUMSPTRL-medpos
  
  time_s=anytim(date_obs)-anytim(date_obs[0])
  
  ;; ----------------------------------------------------------------------
  ;; display the orbital variation 
  
  window,1,xs=900,ys=450
  linecolors
  
  ;; plot the curve of the orbital variation derived above
  utplot,date_obs,dw_orb,xstyle=1,thick=2

  ;; perform spline fitting to the derived orbital variation curve 
  ;; check if there is enough data for spline fitting
  delta_t  = 300. ;; spacing for the knots in the spline, in the unit of second
  dt = max(time_s)-min(time_s)
  if (dt le delta_t) and not(keyword_set(nospline)) then begin
    message,'NOT ENOUGH DATA FOR SPLINE FIT',/info
    message,'TURING SPLINE FIT OFF',/info
    nospline = 1
  endif

  ;; check if there is enough good data for spline fitting
  good = where(finite(mean_wpos) eq 1,ngood)
  bad  = where(finite(mean_wpos) ne 1,nbad)
  MAX_BAD = 0.25 
  if float(nbad)/float(ngood) gt MAX_BAD then begin
    message,'NOT ENOUGH GOOD DATA FOR SPLINE FIT',/info
    message,'TURING SPLINE FIT OFF',/info
    nospline = 1
  endif


  if not(keyword_set(nospline)) then begin

    time_good = time_s[good]
    dw_good = dw_orb[good]

    ;; smooth the orbital variation curve, eliminate the 5-min photospheric oscillation
    n_smooth = floor(5*60./mean(deriv(time_s))) 
    dw_good  = smooth(dw_good,n_smooth,/EDGE_TRUNCATE,/NAN)
    
    ;; spline fitting, overplot the fitting result
    dw_orb = spline(time_good,dw_good,time_s)
    outplot,date_obs,dw_orb,color=2,thick=2

    legend,['original','spline fit'],linestyle=[0,0],color=[255,2],$
        box=0,spacing=1.5,pspacing=1.5

    ;save the figure
    write_jpeg,datetime+'_orbitvar.jpg',tvrd(true=1),true=1,quality=95

  endif else begin

    legend,['original'],linestyle=[0],color=[255],$
        box=0,spacing=1.5,pspacing=1.5
    write_jpeg,datetime+'_orbitvar.jpg',tvrd(true=1),true=1,quality=95

  endelse

  ;; ----------------------------------------------------------------------
  ;; save the result

  savegen,dw_orb,date_obs,medpos,scvelo,file=datetime+'_orbitvar.genx'

  
return

end
