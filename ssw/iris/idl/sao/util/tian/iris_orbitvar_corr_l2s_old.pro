;             NAME : iris_orbitvar_corr_l2s_old
;             
;           PROJECT: IRIS
; 
;          PURPOSE : Corrections for orbital variation of the spectral line positions using IRIS level-2 data
;
; CALLING SEQUENCE : iris_orbitvar_corr_l2s_old,files,dw_orb_fuv,dw_orb_nuv,date_obs
; 
;           INPUTS : files - a string containing names of IRIS level-2 spectral data files in one OBS (so all of these raster data have the same size)
; 
;         KEYWORDS : ymin, ymax - select a segment (pixel range) in the slit direction for averaging. The default 
;                                 range is the entire slit.
;                    
;                    nospline - do not do smoothing and spline fitting to the curve of orbital variation. 
;                    
;                    indiv -  Fit individual profiles and then average the line positions. 
;                             Fit the profile averaged over the selected portion of the slit if not set. 
;                    
;                    showfit - show the Gaussian fitting result
;                    
;                    dw_th -  the thermal component of the orbital variation derived by using the Ni I 2799.474 (vacuum wavelength) line. 
;                             The unit is unsummed wavelength pixel (about 0.0256 Angstrom for NUV, 0.013 Angstrom for FUV)
;                    
;                    dw_sc -  the spacecraft velocity (along the Sun-IRIS line) component of the orbital variation, 
;                             positive value means the Sun is moving away from IRIS. The unit is km/s.
;                             
;                    abswvl_nuv - the amount (unit Angstrom) that has to be subtracted from the wavelengths if you want to do absolute wavelength calibration for NUV
; 
;          OUTPUTS : dw_orb_fuv - the correction vector for orbital variation in FUV. 
;                                 Both the thermal and spacecraft velocity components are included. The unit is Angstrom.
;          
;                    dw_orb_nuv - the correction vector for orbital variation in NUV. 
;                                 Both the thermal and spacecraft velocity components are included. The unit is Angstrom.
;                    
;                    date_obs -  the vector of observation times
;                  
;                    dw_orb_fuv, dw_orb_nuv & date_obs saved into the file of datetime+'_orbitvar.genx', can be loaded using 
;                    restgen,dw_orb_fuv,dw_orb_nuv,date_obs,file=datetime+'_orbitvar.genx'
;
;          HISTORY : Oct 14, 2013: written by HUI TIAN at CfA
;                    Nov 27, 2013: use the entire slit by default, improve the empirical sine fitting, eliminate the 5-min photospheric oscillation.
;                    Dec 31, 2013: Outputs are now dw_orb_fuv, dw_orb_nuv & date_obs. Both the thermal and spacecraft velocity components are included.
;                    Mar 21, 2014: check invalid values of the orbital phase, assign zeros to invalid values of the total orbital variation.
;                    Jul 11, 2014: name changed to iris_orbitvar_corr_l2s_old.pro since IRIS data are reprocessed by a new pipleline after April 2014.
;; **********************************************************************

pro iris_orbitvar_corr_l2s_old,files,dw_orb_fuv,dw_orb_nuv,date_obs,dw_th=dw_th,dw_sc=dw_sc,abswvl_nuv=abswvl_nuv,indiv=indiv,$
    ymin=ymin,ymax=ymax,nospline=nospline,showfit=showfit

  ;number of rasters
  nf=n_elements(files)

  ;;date and time of the observation, use that of the 1st raster file
  datetime=strmid(files[0],49,15,/rev)
  
  ;read the 1st raster file and build some parameter containers 
  d=iris_obj(files[0])
  
  ;; rest wavelegnth of the Ni I 2799.474 line, in the unit of Angstrom
  wave0 = 2799.474 ; Note that 2799.474 is the vacuum wavelength
  ;; find the window that covers the selected line
  winid=d->getwindx(wave0)
  ;; return the wavelength vector (in the unit of Angstrom) of window winid
  lam=d->getlam(winid)
  ;; load the level-2 data of window winid
  dat=d->getvar(winid,/load) 
  
  rollang=d->getinfo('SAT_ROT') ;roll angle 
  
  obj_destroy,d
 
  ;; select a portion of the wavelength vector that includes only the Ni I line
  wmin=2799.3
  wmax=2799.8  
  sub_line=where(lam ge wmin and lam le wmax,n_sub)
  if n_sub lt 5 then sub_line=indgen(5)+sub_line[0]  ;you need at least 5 data points for the Gaussian fit
  lam=double(lam[sub_line])
  dat=dat[sub_line,*,*]  

  nw=(size(dat))[1]  ;wavelength pixels
  ns=(size(dat))[2]  ;slit pixels
  nt=(size(dat))[3]  ;time step in one raster
  dat1=fltarr(nw,ns,nt*nf)
  date_obs1=strarr(nt*nf)
  scvelo1=fltarr(nt*nf)
  ophase1=fltarr(nt*nf)

  ;get Ni I line profiles from all raster files 
  i=0 ;time counter
  for f=0,nf-1 do begin
    file=files[f]
  
    ;; read the level-2 spectral data
    d=iris_obj(file)
  
    ;; find the window that covers the Ni I line
    winid=d->getwindx(wave0)
  
    ;; load the level-2 data and retrive only the Ni I line profiles 
    dat=d->getvar(winid,/load)
    dat=dat[sub_line,*,*]
  
    ;; times of all exposures in the raster
    date_obs=d->ti2utc()
  
    ;; spacecraft velocity
    s=d->aux_info()
    scvelo=s.obs_vr
  
    ophase=2*!pi*s.OPHASE ;orbital phase
  
    ; Check for bad ophase or sat_rot in header; PB 2014-03-21
    badpoints = where( (finite(ophase) ne 1) or (finite(rollang)  ne 1), numbad)
    if numbad gt 0 then begin
    timestr = replicate({t_obs:''}, n_elements(date_obs))
    timestr.t_obs = date_obs
    orbinfo = iris_time2hk(timestr, /mag)
    ophase = orbinfo.ophase
    rollang = orbinfo.a_eulerbr_z
    endif
  
    obj_destroy,d
  
    for t=0,nt-1 do begin  
      dat1[*,*,i+t]=dat[*,*,t]
      date_obs1[i+t]=date_obs[t]
      scvelo1[i+t]=scvelo[t]
      ophase1[i+t]=ophase[t]
    endfor
    i=i+nt  
  
    print,'file:',f
  endfor
  
  dat=dat1
  date_obs=date_obs1
  scvelo=scvelo1
  ophase=ophase1
  
  ;; ----------------------------------------------------------------------
  ;; derive the orbital variation (both thermal drift and spacecraft orbital velocity components) 
  
  ;; averaged positions of the Ni I 2799.474 line at different times
  mean_wpos=dblarr(nt*nf)
  
  for k=0,nt*nf-1 do begin

    ;; select the pixel range in the slit direction for averaging, default is the entire slit
    ny=(size(dat))[2]
    ;if not(keyword_set(ymin)) then ymin = round(ny*0.45) else ymin = ymin > 0
    ;if not(keyword_set(ymax)) then ymax = round(ny*0.55) else ymax = ymax < ny
    if not(keyword_set(ymin)) then ymin = 0
    if not(keyword_set(ymax)) then ymax = ny-1
  
    odat=reform(dat[*,ymin:ymax,k])

    nw=(size(odat))[1]
    ny=(size(odat))[2]
    
    if keyword_set(indiv) then begin ;; fit individual line profile in the selected slit portion, then average the derived line centriods
      wpos=fltarr(ny)
      for j=2,ny-3 do begin
        
        ;; average over 5 pixels along the slit to improve the S/N
        ii=average(odat[*,j-2:j+2],2) 
  
        ;; perform Gaussian fit to the profile of Ni I 2799.474          
        ifit=gaussfit(lam, ii, a, nterms=5)

        ;; throw out bad fit
        if a[0] gt 0. or $
           finite(a[0]) ne 1 or $
           a[1] gt wmax or $
           a[1] lt wmin then a[*]=-!values.f_nan

        ;; show the observed and fitted line profile
        if (keyword_set(showfit)) and finite(a[0]) eq 1 then begin
          if j eq round(ny/2.) then begin
            plot, lam, ii, xstyle=1, ystyle=1, $
                 title='Gaussian Fit', xtitle='Wavelength / angstrom', ytitle='Intensity'
            oplot, lam, ifit, color=120
            print,'progress: '+strtrim(string(k),2)+'/'+strtrim(string(nt*nf),2)
            wait, 0.01
          endif
        endif
        
        wpos[j]=a[1]
 
      endfor

      ;; average the line positions over the selected segment of the slit
      wpos_goodfit=where(wpos gt 0)
      if wpos_goodfit[0] ne -1 then mean_wpos[k]=mean(wpos[wpos_goodfit], /NAN) else mean_wpos[k]=!Values.F_NAN
     
     endif else begin  ;; average all line profiles in the selected range of the slit, then do Gaussian fit
 
        ii=average(odat,2)         
        ifit=gaussfit(lam, ii, a, nterms=5)

        ;; throw out bad fit
        if a[0] gt 0. or $
           finite(a[0]) ne 1 or $
           a[1] gt wmax or $
           a[1] lt wmin then a[*]=-!values.f_nan
           
        ;; show the observed and fitted line profile
        if (keyword_set(showfit)) and finite(a[0]) eq 1 then begin
            plot, lam, ii, xstyle=1, ystyle=1, $
                 title='Gaussian Fit', xtitle='Wavelength / angstrom', ytitle='Intensity'
            oplot, lam, ifit, color=120
            print,'progress: '+strtrim(string(k),2)+'/'+strtrim(string(nt*nf),2)
            wait, 1
        endif
        
        mean_wpos[k]=a[1]
        
     endelse    
     
    ;; subtract spacecraft velocity from the line position   
    mean_wpos[k]=mean_wpos[k]-scvelo[k]/(3.e8)*wave0
    
  endfor

  ;; mark abnormal values, thermal drift is of the order of 2 unsummed wavelength pixels peak-to-peak
  sub_ab=where(abs(mean_wpos-median(mean_wpos)) ge 0.0255*2)
  if sub_ab[0] ne -1 then mean_wpos[sub_ab]=-!values.f_nan
  ;; relative variation of the line position 
  dw_th_A=mean_wpos-mean(mean_wpos,/NAN)
  
  ;; change the unit from angstrom into unsummed wavelength pixel
  specsize=0.0255 ;; NUV spectral pixel size in the case of no spectral summing
  dw_th_p=dw_th_A/specsize
  
  ;; adjust the reference wavelength using orbital phase information
  sub_nan=where(finite(ophase) ne 1)
  if sub_nan[0] ne -1 then begin
    message,'WARNING: ORBITAL PHASE VALUES INVALID, THERMAL DRIFT MAY BE OFFSET BY AT MOST ONE PIXEL',/info
    ;thermal component of the orbital variation, in the unit of unsummed wavelength pixel
    dw_th=dw_th_p
    ;for absolute wavelength calibration of NUV the following amount (unit Angstrom) has to be subtracted from the wavelengths
    abswvl_nuv=mean(mean_wpos,/NAN)-wave0
  endif else begin
    sine_params=[-0.66615146, -1.0, 53.106583-rollang/360.*2*!pi] ;empirical sine fitting at 0 roll angle shifted by different phase
    phase_adj=mean(sine_params[0]*sin(sine_params[1]*ophase+sine_params[2]),/NAN)
    ;thermal component of the orbital variation, in the unit of unsummed wavelength pixel
    dw_th=dw_th_p+phase_adj
    ;for absolute wavelength calibration of NUV the following amount (unit Angstrom) has to be subtracted from the wavelengths
    abswvl_nuv=mean(mean_wpos,/NAN)-wave0-phase_adj*specsize
  endelse
  
  ;spacecraft velocity component of the orbital variation, in the unit of km/s
  dw_sc=scvelo/1000.
  
  ;relative times
  time_s=anytim(date_obs)-anytim(date_obs[0])
  
  ;; ----------------------------------------------------------------------
  ;; Remove 5-min photospheric oscillation and apply spline fitting to the thermal drift
  ;; plot the thermal drift and the fitting result
  
  window,1,xs=900,ys=800, retain = 2
  !p.multi=[0,1,3]
  linecolors
  
  ;; plot the curve of the thermal component of the orbital variation derived above
  utplot,date_obs,dw_th,xstyle=1,thick=2,ytitle='Thermal drift of Ni I 2799.474 !C(unsummed wavelength pixel)',$
    charsize=3,background=255,color=0

  ;; perform spline fitting to the derived thermal variation curve 
  ;; check if there is enough data for spline fitting
  delta_t  = 300. ;; spacing for the knots in the spline, in the unit of second
  dt = max(time_s)-min(time_s)
  if (dt le delta_t) and not(keyword_set(nospline)) then begin
    message,'NOT ENOUGH DATA FOR SPLINE FIT',/info
    message,'TURING SPLINE FIT OFF',/info
    nospline = 1
  endif

  ;; check if there is enough good data for spline fitting
  good = where(finite(mean_wpos) eq 1,ngood)
  bad  = where(finite(mean_wpos) ne 1,nbad)
  MAX_BAD = 0.25 ;0.3 
  if float(nbad)/float(ngood) gt MAX_BAD then begin
    message,'NOT ENOUGH GOOD DATA FOR SPLINE FIT',/info
    message,'TURING SPLINE FIT OFF',/info
    nospline = 1
  endif

  if not(keyword_set(nospline)) then begin

    time_good = time_s[good]
    dw_good = dw_th[good]

    ;; smooth the thermal variation curve to eliminate the 5-min photospheric oscillation
    n_smooth = floor(5*60./mean(deriv(time_s))) 
    if n_smooth lt n_elements(dw_good) then dw_good  = smooth(dw_good,n_smooth,/EDGE_TRUNCATE,/NAN)
    
    ;; spline fitting, overplot the fitting result
    dw_th = spline(time_good,dw_good,time_s)
    outplot,date_obs,dw_th,color=2,thick=2

    al_legend,['original','spline fit'],linestyle=[0,0],color=[0,2],$
        box=0,spacing=1.5,pspacing=1.5,charsize=2,textcolors=[0,2]

  endif else begin

    al_legend,['original'],linestyle=[0],color=[0],$
        box=0,spacing=1.5,pspacing=1.5,charsize=2,textcolors=0

  endelse
  
  ;; ----------------------------------------------------------------------
  ;; plot the spacecraft velocity component
  utplot,date_obs,dw_sc,xstyle=1,thick=2,ytitle='Spacecraft orbital velocity !C(km/s)',charsize=3,color=0
  
  ;; ----------------------------------------------------------------------
  ;; derive and plot the total orbital variation curves
  
  ;total orbital variation for FUV, in the unit of Angstrom
  dw_orb_fuv=dw_th*(-0.013)+dw_sc/(3.e5)*1370.
  ;total orbital variation for NUV, in the unit of Angstrom
  dw_orb_nuv=dw_th*0.0255+dw_sc/(3.e5)*2800.
 
  ;assign zeros to invalid values
  sub_nan=where(finite(dw_orb_fuv) ne 1)
  if sub_nan[0] ne -1 then begin
    dw_orb_fuv[sub_nan]=0
    message,'WARNING: ZEROS ASSIGNED TO INVALID VALUES OF FUV ORBITAL VARIATION',/info
  endif
  sub_nan=where(finite(dw_orb_nuv) ne 1)
  if sub_nan[0] ne -1 then begin
    dw_orb_nuv[sub_nan]=0
    message,'WARNING: ZEROS ASSIGNED TO INVALID VALUES OF NUV ORBITAL VARIATION',/info 
  endif
   
  utplot,date_obs,dw_orb_fuv,xstyle=1,yrange=[-0.04,0.04],yst=1,thick=2,ytitle='Total orbital variation !C(Angstrom)',charsize=3,color=0
  outplot,date_obs,dw_orb_nuv,color=2,thick=2
  al_legend,['FUV','NUV'],linestyle=[0,0],color=[0,2],$
        box=0,spacing=1.5,pspacing=1.5,charsize=2,textcolors=[0,2]

  ;; save the figure
  write_jpeg,datetime+'_orbitvar.jpg',tvrd(true=1),true=1,quality=95
  !p.multi=0
  
  ;; ----------------------------------------------------------------------
  ;; save the result

  savegen,dw_orb_fuv,dw_orb_nuv,date_obs,file=datetime+'_orbitvar.genx'

  
return

end
