;             NAME : dgf_rb_1lp
; 
;          PURPOSE : Perform RB asymmetry analyses and RBp-guided double Gaussian fit (see RBs in 
;                    De Pontieu et al. 2009, ApJ, 701, L1; see RBs, RBp, and RBd in Section 2 of Tian et al. 2011, ApJ, 738, 18) for one line profile
;
; CALLING SEQUENCE : dgf_rb_1lp,wvl,lp,ee
; 
;           INPUTS : lp - the line profile
;                    
;                    wvl - wavelength vector
;                    
;                    ee - error vector  
; 
;         KEYWORDS : wave0 - rest wavelength, use the average centroid if not set 
;         
;                    lineid - name of the line
;         
;                    posi - position of the line profile plot
;                    
;                    xtitle - lable of x-cooridinate of the line profile plot and RB profile plot
;                    
;                    xtitleRB -  lable of y-cooridinate of the RB profile plot
;                    
;                    ytitle -  lable of y-cooridinate of the line profile plot
;                    
;                    para_sgf -  output of SGF line parameters: peak intensity, Doppler shift, line width, background intensity
;          
;                    chisq_sgf - output of chisq value for SGF
;                    
;                    para_dgf -  output of DGF two-component line parameters[4,2]: peak intensity, Doppler shift, line width, background intensity
;          
;                    chisq_dgf - output of the chisq value for DGF
;                    
;                    para_rb - output of RB parameters[3,3]: relative intensity, velocity, and width. The last dimension is for RBs,RBp, RBd, respectively
;                    
;                    rb_array - output of RB profiles [15,3]: The last dimension is for RBs,RBp, RBd, respectively                    
;
;          HISTORY : Written by Hui Tian at CfA, July 8, 2013
;
;; **********************************************************************

pro dgf_rb_1lp,wvl,lp,ee,$
    lineid=lineid,wave0=wave0,posi=posi,xtitle=xtitle,xtitleRB=xtitleRB,ytitle=ytitle,$
    para_sgf=para_sgf,chisq_sgf=chisq_sgf,para_dgf=para_dgf,chisq_dgf=chisq_dgf,rb_array=rb_array,para_rb=para_rb
    
!p.font=-1
window,xs=900,ys=700
loadct,0  & tvlct,255L,0L,0L,2L  & tvlct,0L,255L,0L,3L   & tvlct,0L,0L,255L,4L

if not(keyword_set(xtitle)) then xtitle = 'Wavelength (!N!6!sA!r!u!9 %!6 !n!N!3)'
if not(keyword_set(xtitleRB)) then xtitleRB = 'Velocity (km/s)'
if not(keyword_set(ytitle)) then ytitle =  'Counts';'Radiance (erg !Ns!E-1!Ncm!E-2!Nsr!E-1 !N!6!sA!r!u!9 %!6!n!U-1!N!3)'
if not(keyword_set(posi)) then posi=[0.07,0.07,0.60,0.95]
if not(keyword_set(lineid)) then lineid='iris'

dlambda=mean(deriv(wvl)) ;spectral pixel size
nw=n_elements(wvl)

if not(keyword_set(wave0)) then begin
  res = mpfitpeak(wvl,lp, a, nterms = 4, /double, /positive) 
  wave0= a[1] ;set the rest wavelength by assuming 0 shift of the line profile
endif


para_sgf=fltarr(4)
para_dgf=fltarr(4,2)

steps = findgen(15)*10. ; R-B Steps in km/s
rb_array=fltarr(15,3)
para_rb=fltarr(3,3)

;single Gaussian fit
x=wvl
y=lp
err_ave=ee>0.001<(y*0.9) ;make sure that the error value is larger than 0 and smaller than the data value
plot_io,x,y,title='SGF & DGF to '+lineid+' line profile',position=posi,xtitle=xtitle,ytitle=ytitle,psym=4,$;/noerase,$
      xrange=[min(x)-dlambda/2,max(x)+dlambda/2],xstyle=1,yrange=[min(y)*0.95>0.1,max(y)*1.5],ystyle=1,charsize=1.5
errplot,x,y-err_ave,y+err_ave,width=0.005,thick=1
bpp = where(y gt 0., bppc)  &  ee = err_ave
if bppc le 4 then begin ;do not do fit
  a=fltarr(4)-999. & chisq=-999. 
  i_rba1=-999. & v_rba1=-999. & w_rba1=-999. & rba1=fltarr(15)-999.
  i_rba2=-999. & v_rba2=-999. & w_rba2=-999. & rba2=fltarr(15)-999.
  goto,para_array
endif
fitsg = mpfitpeak(x[bpp],y[bpp], a, nterms = 4, /double, /positive)   ;fitsg=a[3]+a[0]*exp(-0.5*(x-a[1])^2./(a[2]^2))
chisq = (1./(bppc - 4)) * total(((y[bpp] - fitsg[bpp])/err_ave[bpp])^2)
oplot,x,spline(x[bpp],fitsg,x),color=3L
xyouts,x[nw-1],10^(!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.92),'SGF',charsize=1.5,color=3L,align=1
xyouts,x[nw-1],10^(!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.86),strmid(strtrim(string((a[1]-wave0)/wave0*3e5),2),0,4),charsize=1.5,color=3L,align=1
xyouts,x[nw-1],10^(!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.80), strmid(strtrim(string(a[2]/wave0*2.999e5*sqrt(2)),2),0,4),charsize=1.5,color=3L,align=1
xyouts,x[nw-1],10^(!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.68),strmid(strtrim(string(chisq),2),0,4),charsize=1.5,color=3L,align=1


;RBs and RBs parameters
x_rel= interpol(x,nw*10,/spline)-a[1]  ;use the centroid of single Gaussian fit to do R-B analysis
rbstr = gen_rb_profile(x_rel/wave0*2.999e5, interpol(y,nw*10,/spline), steps, 20.)
rba1= (rbstr.red - rbstr.blue)/a[0] &  rba1=smooth(rba1,3)
i_rba1=min(interpol(rba1,n_elements(steps)*10,/spline),sub_m)  
v_rba1=(sub_m/10.+1)*10 ;use the minimum of the R-B profile to determine the velocity
sub_e=where(interpol(rba1,n_elements(steps)*10,/spline) le i_rba1/2.72,sub_n) 
if sub_e[0] eq -1 then w_rb1=-999. else w_rba1=(sub_e[sub_n-1]-sub_e[0])/2


;RBp profile and RBp parameters
ymax=max(smooth(interpol(y,nw*100,/spline),3),sub_m)  &  xvec=interpol(x,nw*100,/spline)
x_rel = interpol(x,nw*10,/spline)-xvec[sub_m]
rbstr = gen_rb_profile(x_rel/wave0*2.999e5, interpol(y,nw*10,/spline), steps, 20.)
rba2= (rbstr.red - rbstr.blue)/ymax  &  rba2=smooth(rba2,3)
i_rba2=min(interpol(rba2,n_elements(steps)*10,/spline),sub_m)  
v_rba2=(sub_m/10.+1)*10 ;use the minimum of the R-B profile to determine the velocity
sub_e=where(interpol(rba2,n_elements(steps)*10,/spline) le i_rba2/2.72,sub_n) 
if sub_e[0] eq -1 then w_rb2=-999. else w_rba2=(sub_e[sub_n-1]-sub_e[0])/2  ;exponential width


;do RBp-guided double Gaussian fit for the large-blueward-asymmetry profiles
if (average(rba2[3:9]) le -0.03) then begin  
fit0 = double([a[3], a[0], a[1], sqrt(2)*a[2]])  ;single Gaussian fit parameter
wp = double(a[1]-v_rba2/2.999e5*wave0)
re = mp_dgf_iris(x, y, ee, fit0, wp, double(abs(i_rba2))*ymax, dlambda[0], bpp, /double)
fitdg=re.b + re.i1*exp(-((x - re.p1)/re.w1)^2) + re.i2*exp(-((x - re.p2)/re.w2)^2)
chisq2 = (1./(bppc - 7)) * total(((y[bpp] - fitdg[bpp])/err_ave[bpp])^2) ;sqrt(y[bpp])
oplot,interpol(x,nw*10,/spline),interpol(re.b + re.i1*exp(-((x - re.p1)/re.w1)^2),nw*10,/spline),linestyle=2,color=2L
oplot,interpol(x,nw*10,/spline),interpol(re.b + re.i2*exp(-((x - re.p2)/re.w2)^2),nw*10,/spline),linestyle=2,color=2L
oplot,interpol(x,nw*10,/spline),interpol(fitdg,nw*10,/spline),color=2L
if abs(re.p1) lt 0.0001 then re.p1=0. & if abs(re.p2) lt 0.0001 then re.p2=0. 
xyouts,x[2],10^(!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.92),'1!Est!N     2!End!N',charsize=1.5,color=2L
xyouts,x[0],10^(!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.86),'v:  '+strmid(strtrim(string((re.p1-wave0)/wave0*3e5),2),0,4)+'   '+strmid(strtrim(string((re.p2-wave0)/wave0*3e5),2),0,4),charsize=1.5,color=2L
xyouts,x[0],10^(!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.80),'w: '+strmid(strtrim(string(re.w1/wave0*3e5),2),0,4)+'   '+strmid(strtrim(string(re.w2/wave0*3e5),2),0,4),charsize=1.5,color=2L
xyouts,x[0],10^(!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.74),'i!i2!N/i!i1!N:    '+strmid(strtrim(string(re.i2/re.i1),2),0,4),charsize=1.5,color=2L
xyouts,x[0],10^(!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.68),'!4v!N!3!ir!E2!N:     '+strmid(strtrim(string(chisq2),2),0,4),charsize=1.5,color=2L


;RBd profile and RBd parameters
x_rel = interpol(x,nw*10,/spline)-re.p1 
rbstr = gen_rb_profile(x_rel/wave0*2.999e5, interpol(y,nw*10,/spline), steps, 20.)
rba3= (rbstr.red - rbstr.blue)/re.i1  &  rba3=smooth(rba3,3)
i_rba3=min(interpol(rba3,n_elements(steps)*10,/spline),sub_m)  
v_rba3=(sub_m/10.+1)*10 ;use the minimum of the R-B profile to determine the velocity
sub_e=where(interpol(rba3,n_elements(steps)*10,/spline) le i_rba3/2.72,sub_n) 
if sub_e[0] eq -1 then w_rb3=-999. else w_rba3=(sub_e[sub_n-1]-sub_e[0])/2


plot,steps+10,rba1,title='RB profiles',position=posi+[0.6,0,0.38,0],/noerase,$
    yrange=[-0.4,0.2],ystyle=1,xtitle=xtitleRB,ytitle='RB Asymmetry (percentage)',charsize=1.5
oplot,steps+10,rba2,color=2L
oplot,steps+10,rba3,color=3L
xyouts,steps[2],!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.93,'RB!iS!N',charsize=1.5
xyouts,steps[1],!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.84,'i:   '+strmid(strtrim(string(abs(i_rba1)),2),0,4),charsize=1.5
xyouts,steps[1],!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.77,'v:  '+strmid(strtrim(string(v_rba1),2),0,4),charsize=1.5
xyouts,steps[1],!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.70,'w: '+strmid(strtrim(string(w_rba1),2),0,4),charsize=1.5
xyouts,steps[5],!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.93,'RB!iP!N',charsize=1.5,color=2L
xyouts,steps[5],!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.84,strmid(strtrim(string(abs(i_rba2)),2),0,4),charsize=1.5,color=2L
xyouts,steps[5],!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.77,strmid(strtrim(string(v_rba2),2),0,4),charsize=1.5,color=2L
xyouts,steps[5],!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.70,strmid(strtrim(string(w_rba2),2),0,4),charsize=1.5,color=2L
xyouts,steps[9],!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.93,'RB!iD!N',charsize=1.5,color=3L
xyouts,steps[9],!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.84,strmid(strtrim(string(abs(i_rba3)),2),0,4),charsize=1.5,color=3L
xyouts,steps[9],!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.77,strmid(strtrim(string(v_rba3),2),0,4),charsize=1.5,color=3L
xyouts,steps[9],!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.70,strmid(strtrim(string(w_rba3),2),0,4),charsize=1.5,color=3L

para_dgf[0,0]=re.i1 & para_dgf[0,1]=re.i2
para_dgf[1,0]=(re.p1-wave0)/wave0*3e5 & para_dgf[1,1]=(re.p2-wave0)/wave0*3e5
para_dgf[2,0]=re.w1/wave0*3e5 & para_dgf[2,1]=re.w2/wave0*3e5
para_dgf[3,0]=re.b
chisq_dgf=chisq2
para_sgf[0]=a[0] ;peak intensity
para_sgf[1]=(a[1]-wave0)/wave0*3e5 ;Doppler shift
para_sgf[2]=a[2]/wave0*3e5*sqrt(2) ;exponential width
para_sgf[3]=a[3] ;background
chisq_sgf=chisq
para_rb[0,0]=i_rba1 & para_rb[0,1]=i_rba2 & para_rb[0,2]=i_rba3
para_rb[1,0]=v_rba1 & para_rb[1,1]=v_rba2 & para_rb[1,2]=v_rba3
para_rb[2,0]=w_rba1 & para_rb[2,1]=w_rba2 & para_rb[2,2]=w_rba3
rb_array[*,0]=rba1 & rb_array[*,1]=rba2 & rb_array[*,2]=rba3


endif else begin  ;if no double Gaussian fit and RBd
plot,steps+10,rba1,title='RB profiles',position=posi+[0.6,0,0.38,0],/noerase,$
    yrange=[-0.4,0.2],ystyle=1,xtitle=xtitleRB,ytitle='RB Asymmetry (percentage)',charsize=1.5
oplot,steps+10,rba2,color=2L
xyouts,steps[2],!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.93,'RB!iS!N',charsize=1.5
xyouts,steps[1],!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.84,'i:   '+strmid(strtrim(string(abs(i_rba1)),2),0,4),charsize=1.5
xyouts,steps[1],!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.77,'v:  '+strmid(strtrim(string(v_rba1),2),0,4),charsize=1.5
xyouts,steps[1],!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.70,'w: '+strmid(strtrim(string(w_rba1),2),0,4),charsize=1.5
xyouts,steps[5],!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.93,'RB!iP!N',charsize=1.5,color=2L
xyouts,steps[5],!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.84,strmid(strtrim(string(abs(i_rba2)),2),0,4),charsize=1.5,color=2L
xyouts,steps[5],!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.77,strmid(strtrim(string(v_rba2),2),0,4),charsize=1.5,color=2L
xyouts,steps[5],!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.70,strmid(strtrim(string(w_rba2),2),0,4),charsize=1.5,color=2L

para_array:
para_sgf[0]=a[0]
para_sgf[1]=(a[1]-wave0)/wave0*3e5
para_sgf[2]=a[2]/wave0*3e5*sqrt(2)
para_sgf[3]=a[3]
chisq_sgf=chisq
para_rb[0,0]=i_rba1 & para_rb[0,1]=i_rba2
para_rb[1,0]=v_rba1 & para_rb[1,1]=v_rba2 
para_rb[2,0]=w_rba1 & para_rb[2,1]=w_rba2 
rb_array[*,0]=rba1 & rb_array[*,1]=rba2
endelse

end
