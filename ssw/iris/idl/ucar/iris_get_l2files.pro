pro iris_get_l2files, file_arr, ds = ds, data_home = data_home

; IDL Version 8.2, Mac OS X (darwin x86_64 m64)
; Journal File for mscott@eclipse.hao.ucar.edu
; Working directory: /Volumes/IRIS
; Date: Tue Oct  1 08:48:44 2013

x = file_arr
nx = n_elements(x)

data_series = 'level2_prelim03'
if keyword_set(ds) then data_series = ds[0]
home = './'
if keyword_set(data_home) then home = data_home[0]

for ii=0, nx-1 do begin
    tmp = str_sep(x[ii],data_series)
    tmp = tmp[1]
    tmp = str_sep(tmp, '/')
    nt = n_elements(tmp)
    year = tmp[nt-5]
    month = tmp[nt-4]
    day = tmp[nt-3]
    obs_folder = tmp[nt-2]
    file = tmp[nt-1]
    outdir = home + year + '/' + month+ '/' + day + '/' + obs_folder + '/'
    message, /cont, 'Downloading IRIS L2 FITS to:'+outdir
    spawn, 'mkdir -p '+outdir
    sock_copy, x[ii], outdir+file, out_dir = outdir, /use_network
endfor

end
