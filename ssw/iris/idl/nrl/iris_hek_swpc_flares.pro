

FUNCTION iris_hek_swpc_flares, starttime=starttime, endtime=endtime

;+
; NAME:
;      IRIS_HEK_SWPC_FLARES
;
; PURPOSE:
;      This routine calls the Heliophysics Event Knowledgebase (HEK)
;      to extract information about flares from the SWPC catalog.
;
; CATEGORY:
;      IRIS; HEK; flares.
;
; CALLING SEQUENCE:
;	Result = IRIS_HEK_SWPC_FLARES( StartTime=, EndTime= )
;
; OPTIONAL INPUTS:
;      StartTime:  The start time for the observing period. Can be any
;                  standard SSW time format.
;      EndTime:    The start time for the observing period. Can be any
;                  standard SSW time format.
;	
; OUTPUTS:
;      Returns a structure with the following tags:
;        PeakTime:   Time of the flare peak (string)
;        Fl_GoesCls: GOES class of the flare (string)
;        X:          X position in heliocentric coords (float)
;        Y:          Y position in heliocentric coords (float)
;
;      If no flares are found, then -1 is returned.
;
; EXAMPLE:
;      IDL> output=iris_hek_swpc_flares('22-oct-2013 21:00','23-oct-2013 01:00')
;
; MODIFICATION HISTORY:
;      Ver.1, 12-Mar-2018, Peter Young
;-



t0=starttime
t1=endtime

;
; This is the call to the HEK. Note I'm restricting to only
; SWPC flare events. These should all have GOES classes associated
; with them.
;
search_array='frm_name=SWPC'
query=ssw_her_make_query(t0,t1,/fl,search_array=search_array,result_limit=1000)
events=ssw_her_query(query,/quiet)

IF n_tags(events) EQ 0 THEN return,-1

;
; Construct the output structure from the HEK output.
;
n=n_elements(events.fl)
str={peaktime: '', fl_goescls: '', x: 0., y: 0.}
output=replicate(str,n)
FOR i=0,n-1 DO BEGIN
  output[i].peaktime=events.fl[i].event_peaktime
  output[i].fl_goescls=events.fl[i].fl_goescls
  output[i].x=events.fl[i].hpc_x
  output[i].y=events.fl[i].hpc_y
ENDFOR 

return,output

END
