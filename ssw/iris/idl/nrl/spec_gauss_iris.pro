
PRO spec_gauss_iris, specstr, shift=shift, xrange=xrange, yrange=yrange

;+
; NAME:
;    SPEC_GAUSS_EIS
;
; CATEGORY:
;    IRIS; line fitting.
;
; PURPOSE:
;    This routine is a 'wrapper' for calling the general purpose
;    line fitting routine SPEC_GAUSS_WIDGET. For creating the input
;    spectrum in the correct format, try using the routine
;    EIS_MASK_SPECTRUM. 
;
; INPUTS:
;    Specstr: A structure containing 1D spectra in the form produced by
;             IRIS_SUM_SPEC. The structure must have the following
;             tags:
;             .wvl     Wavelength vector (Angstroms)
;             .int     Intensity vector
;             .err     Error vector 
;             .qual    Quality vector
;
;             Optional tags are:
;             .qual_max The maximum allowed value of the quality vector.
;
; OPTIONAL INPUTS:
;     Shift:  If the spectrum is known to have a systematic velocity
;             shift, then this will affect the routine's
;             ability to automatically identify emission
;             lines. Specifying the shift with SHIFT will remove the
;             offset from the line identification process. E.g., if
;             the spectrum is know to be red-shifted by 30 km/s, then
;             specify SHIFT=30.
;     Xrange: Specify the wavelength range for the spectrum plot.
;     Yrange: Specify the intensity range for the spectrum plot.
;
; OUTPUTS:
;    Line fitting is performed through graphic user interface. The
;    line fit parameters are sent to a text file in the working
;    directory called spec_gauss_fits.txt.
;
; CALLS:
;    SPEC_GAUSS_WIDGET
;
; MODIFICATION HISTORY:
;    Ver.1, 22-Jul-2016, Peter Young
;       Adapted from spec_gauss_eis.pro.
;-


IF n_elements(shift) EQ 0 THEN shift=0.

xx=specstr.wvl
yy=specstr.int
ee=specstr.err
;; qq=specstr.qual
;; IF tag_exist(specstr,'qual_max') THEN qual_max=specstr.qual_max

;
; This is the default width assumed for all EIS emission lines. If the
; user does not specify an initial guess for the width of the line,
; then def_width will be used.
;
def_width=0.030   ; angstroms

;; fname=getenv('SSW')+'/hinode/eis/idl/atest/pyoung/eis_line_ids.txt'
;; IF file_exist(fname) EQ 1 THEN BEGIN
;;   line_list={fname: fname, width:def_width, shift: shift}
;; ENDIF

i=where(yy NE -100.,ni)
IF n_elements(xrange) EQ 0 THEN xrange=[min(xx[i]),max(xx[i])]
IF n_elements(yrange) EQ 0 THEN yrange=[0,max(yy[i])*1.1]

spec_gauss_widget,xx,yy,ee, $
                  def_width=def_width, $
                  line_list=line_list, $
                  xrange=xrange, yrange=yrange, $
                  set_width_range=0, width_range=[0.02,0.15], $
                  parinfo_wvl_step=0.005

END
