
FUNCTION iris_file2date, filename, dir=dir, truncate=truncate, sji_wvl=sji_wvl

;+
; NAME:
;     IRIS_FILE2DATE()
;
; PURPOSE:
;     Given a standard format IRIS filename, this routine extracts the
;     date from the filename string.
;
; CATEGORY:
;     IRIS; file-handling
;
; CALLING SEQUENCE:
;     Result = IRIS_FILE2DATE(Filename)
;
; INPUTS:
;     Filename:  The name of an IRIS data file.
;
; OPTIONAL OUTPUTS:
;     Dir:       A string of the form "YYYY/MM/DD" corresponding to
;                the date of the file.
;     Sji_wvl:   If the filename(s) corresponds to a SJI file, then
;                this output contains the wavelength, given as an
;                integer. If the file is a raster then a value of 0 is
;                returned. 
;
; KEYWORDS:
;     TRUNCATE:  Truncate the returned time string (remove
;                milliseconds). 
;
; OUTPUTS:
;     A string containing the time (in CCSDS format) corresponding to
;     the file.
;
; EXAMPLE:
;     IDL> print,iris_file2date('iris_l2_20131022_205938_3820259443_raster_t000_r00000.fits')
;     2013-10-22T20:59:38.000
;
; CALLS:
;     EXTRACT_FIDS, ANYTIM2UTC, TIME2FID
;
; MODIFICATION HISTORY:
;     Ver.1, 19-Feb-2015, Peter Young
;     Ver.2, 18-Mar-2015, Peter Young
;         Add /truncate.
;     Ver.3, 17-Sep-2015, Peter Young
;         Added sji_wvl= optional output.
;-


fid=extract_fids(filename)

t_ccsds=anytim2utc(fid,/ccsds,truncate=truncate)

dir=time2fid(t_ccsds,/full_year,delim='/')

n=n_elements(filename)
sji_wvl=intarr(n)
fname=file_basename(filename)
chck=strpos(fname,'SJI')
k=where(chck GT 0,nk)
IF nk GE 1 THEN BEGIN
  FOR i=0,nk-1 DO begin
    sji_wvl[k[i]]=fix(strmid(fname[k[i]],39,4))
  ENDFOR
ENDIF 

return,t_ccsds


END
