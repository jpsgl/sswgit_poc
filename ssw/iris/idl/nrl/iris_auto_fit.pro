
PRO iris_auto_fit, windata, fitdata, _extra=extra

;+
; NAME:
;      IRIS_AUTO_FIT
;
; PURPOSE:
;      This is a wrapper routine for calling EIS_AUTO_FIT with IRIS
;      data. Note that the /PERPIXEL is keyword is set to reflect
;      different units compared to EIS.
;
; CATEGORY:
;      IRIS; Gaussian fitting.
;
; CALLING SEQUENCE:
;      IRIS_AUTO_FIT, Windata, Fitdata
;
; INPUTS:
;      Windata:   A structure created with the routine
;                 IRIS_GETWINDATA.
;
; OPTIONAL INPUTS:
;      See the header for EIS_AUTO_FIT for details.
;	
; KEYWORD PARAMETERS:
;      See the header for EIS_AUTO_FIT for details.
;
; OUTPUTS:
;      Fitdata:  A structure containing the Gaussian fit parameters.
;
; OPTIONAL OUTPUTS:
;      See the header for EIS_AUTO_FIT for details.
;
; EXAMPLE:
;      IDL> file=iris_find_file('24-sep-2013 11:45')
;      IDL> windata=iris_getwindata(file,1402.7)
;      IDL> iris_auto_fit, windata, fitdata
;
; MODIFICATION HISTORY:
;      Ver.1, 22-May-2018, Peter Young
;-


IF n_params() LT 2 THEN BEGIN
  print,'Use:  IDL> iris_auto_fit, windata, fitdata'
  print,''
  print,'  Please check http://www.pyoung.org/quick_guides/iris_auto_fit.html for more details.'
  return
ENDIF 

chck=have_proc('eis_auto_fit')
IF chck EQ 0 THEN BEGIN
  print,'% IRIS_AUTO_FIT: This routine makes use of software in the Hinode/EIS branch of Solarsoft, but I did not'
  print,'                 find this software on your computer. Please modify your Solarsoft installation.'
  print,'                 Returning...'
  return
ENDIF 

eis_auto_fit, windata, fitdata, /perpixel,_extra=extra

END
