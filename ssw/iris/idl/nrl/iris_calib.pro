
PRO iris_calib, dn, wavel, date, plot=plot, texp=texp, ybin=ybin, ver_num=ver_num

;+
; NAME:
;      IRIS_CALIB
;
; PURPOSE:
;      This routine converts an IRIS DN value to erg cm-2 s-1 sr-1.
;
; CATEGORY:
;      IRIS; calibration.
;
; CALLING SEQUENCE:
;	IRIS_CALIB, DN, WAVEL, DATE
;
; INPUTS:
;      Dn:    The integrated intensity in data numbers (DN).
;      Wavel: The line's wavelength in angstroms.
;      Date:  The date of the measurement in a standard SSW format.
;
; OPTIONAL INPUTS:
;      Texp:  The exposure time in seconds. If not specified, then 1
;             second is assumed.
;      Ver_num: Specify the version number of the calibration to be
;               used. The default is to use the most recent version.
;
; KEYWORD PARAMETERS:
;      Plot:  If set, then a plot of the effective area curve is made,
;             with the derived value indicated.
;
; OUTPUTS:
;      The line intensity in units erg cm-2 s-1 sr-1 is printed.
;
; EXAMPLE:
;      IDL> iris_calib, 100, 1402.77, '10-sep-2014'
;        Eff. area:   0.9711 cm^2
;        DN:              100.00
;        Exposure time:   1.00 s
;        Intensity:      4.466e+03 erg cm^-2 s^-1 sr^-1
;
; HISTORY:
;      Ver.1, 18-Dec-2015, Peter Young
;      Ver.2, 22-Aug-2016, Peter Young
;         Corrected flux to intensity in print statement.
;      Ver.3, 13-Sep-2016, Peter Young
;         Expanded header, improved output plot.
;      Ver.4, 14-Sep-2016, Peter Young
;         Added YBIN= input.
;      Ver.5, 5-Dec-2016, Peter Young
;         iris_get_response automatically uses the most up-to-date
;         calibration so I no longer specify ver_num in the
;         call; I've added ver_num as an input, though, in case
;         you want to check the results from the earlier version.
;-

IF n_params() EQ 0 THEN BEGIN
  print,'Use:  IDL> iris_calib, dn, wavel, date [, /plot, texp= ]'
  return
ENDIF 

IF n_elements(ver_num) NE 0 THEN BEGIN
  version=strpad(trim(ver_num),3,fill='0')
ENDIF 

iresp=iris_get_response(date,version=version)
print,'% IRIS_CALIB: instrument response version is '+iresp.version


IF n_elements(texp) EQ 0 THEN texp=1.0

;
; Get spatial pixel size
;
pix_x_size=0.33   ; arcsec
pix_y_size=0.16635  ; arcsec
IF n_elements(ybin) NE 0 THEN pix_y_size=pix_y_size*ybin ELSE ybin=1

pix_size=!pi/(180.)*!pi/(180.)*(pix_x_size/3600.)*(pix_y_size/3600.)

getmin=min(abs(iresp.lambda-wavel/10.),imin)
;print,iresp.lambda[imin]

IF wavel GT 1500 THEN j=1 ELSE j=0

area_sg=iresp.area_sg[imin,j]

IF keyword_set(plot) THEN BEGIN
  xrange=[wavel-10,wavel+10]
  plot,iresp.lambda[*,j]*10,iresp.area_sg[*,j], $
       xtit='Wavelength / angstroms', xrange=xrange,charsiz=2.0
  plots,iresp.lambda[imin,j]*10.,iresp.area_sg[imin,j],psym=2,symsize=2
ENDIF 

dn2phot_sg=iresp.dn2phot_sg[j]

en=1.986e-8/wavel

flux=dn*en*dn2phot_sg/area_sg/pix_size/texp

print,format='("Eff. area: ",f8.4," cm^2")',area_sg
print,format='("DN:        ",f12.2)',dn
print,format='("Y-binning: ",i5)',round(ybin)
print,format='("Exposure time: ",f6.2," s")',texp
print,format='("Intensity:     ",e10.3," erg cm^-2 s^-1 sr^-1")',flux

END
