
FUNCTION iris_sum_spec, filename, rnum=rnum, xpix=xpix, ypix=ypix, $
                        wavelength=wavelength, windata=windata, $
                        wrange=wrange, clean=clean, keep_sat=keep_sat

;+
; NAME:
;     IRIS_SUM_SPEC()
;
; PURPOSE:
;     This routine averages a spatial region in IRIS data and returns
;     a spectrum with an associated error array.
;
; CATEGORY:
;     IRIS; data analysis.
;
; CALLING SEQUENCE:
;	Result = IRIS_SUM_SPEC(Filename)
;
; INPUTS:
;     Filename:  The name of an IRIS file. Can be array (e.g., for a
;                sequence of rasters), but then RNUM= should be
;                specified.
;
; OPTIONAL INPUTS:
;     Wavelength: A wavelength that specifes which IRIS wavelength window
;                 to use. Default: 1402.
;     Xpix:  The X-pixel(s) to use. Can be either an integer or a
;            2-element array. For the latter, all pixels between (and 
;            including) the two pixels are summed.
;     Ypix:  The Y-pixel(s) to use. Can be either an integer or a
;            2-element array. For the latter, all pixels between (and 
;            including) the two pixels are summed.
;     Rnum:  If FILENAME contains multiple files, then RNUM specifies
;            which file to use (it is the raster number).
;     Wavelength: A wavelength that is used to select which wavelength
;                 window to load. Default is 1402.
;     Windata:  A windata structure that can be input to save
;               computing the windata structure from the
;               filename if you intend to have multiple calls to
;               iris_sum_spec for the same data-set.
;     Wrange:  This input is passed on to iris_getwindata and it
;              reduces the wavelength range of the final
;              spectrum. This can be useful for speeding up the
;              operation of the routine if the full wavelength range
;              is not needed.
;
; KEYWORDS:
;     Clean:  If set, then the /clean keyword is passed on to
;             iris_getwindata so that a cosmic-ray cleaning routine is
;             applied.
;     Keep_sat: If set, then saturated data are kept in the output
;               spectrum. 
;
; OUTPUTS:
;     A structure containing the summed spectrum. The tags are:
;      .wvl   Wavelength array (angstroms).
;      .int   Intensity array (DN).
;      .err   Error on intensity.
;      .xpix  Contains the XPIX input.
;      .ypix  Contains the YPIX input.
;      .file  The name of the file from which data were obtained.
;      .npix  Number of spatial pixels used in sum.
;      .exp_time  Exposure time in seconds.
;      .xscale Plate scale in arcsec.
;      .yscale Plate scale in arcsec.
;      .missing Missing data value.
;      .date_obs  The date_obs of the data-set.
;      .time_stamp  Time file was created.
;      .int_units  Units for intensity.
;
; EXAMPLE:
;     IDL> file=iris_find_file('22-oct-2013 23:00')
;     IDL> spec=iris_sum_spec(file,rnum=4,xpix=[4,6],ypix=[200,202])
;
; MODIFICATION HISTORY:
;     Ver.1, 21-Aug-2015, Peter Young
;     Ver.2, 10-Sep-2015, Peter Young
;        Added WRANGE= input, and used new features of iris_getwindata
;        to speed up routine when dealing with large data-sets.
;     Ver.3, 14-Sep-2015, Peter Young
;        Added /CLEAN keyword.
;     Ver.4, 26-May-2016, Peter Young
;        Added /KEEP_SAT keyword.
;     Ver.5, 1-Jul-2016, Peter Young
;        Removed print statements from earlier modification; added new
;        tags to output.
;     Ver.6, 13-Jul-2016, Peter Young
;        Added missing data value to output; now exits if wavelength
;        is not available in data-set.
;     Ver.7, 18-Jul-2016, Peter Young
;        Prevented windata from being an output as this caused
;        problems due to ixrange.
;     Ver.8, 22-Jul-2016, Peter Young
;        Pass /quiet keyword to iris_getwindata to get rid of annoying
;        xcen message.
;     Ver.9, 26-Aug-2016, Peter Young
;        Added date_obs, time_stamp, wvl_units and int_units tags to
;        output; forced the filename tag not to be an array.
;-

IF n_params(filename) LT 1 THEN BEGIN
  print,'Use: IDL> iris_sum_spec, filename [, rnum=, xpix=, ypix=, wavelength=, wrange=]'
  return,-1
ENDIF

;
; Choose correct filename to use.
;
nf=n_elements(filename)
IF nf GT 1 THEN BEGIN
  IF n_elements(rnum) EQ 0 THEN BEGIN  
    print,'% IRIS_SUM_SPEC: multiple filenames have been specified, but RNUM= has not been specified.'
    print,'                 Using RNUM=0.'
    rnum=0
  ENDIF
  file=filename[rnum]
ENDIF ELSE BEGIN
  file=filename[0]
ENDELSE 


IF n_elements(wavelength) EQ 0 THEN BEGIN
  print,'% IRIS_SUM_SPEC: WAVELENGTH= has not been specified. Using 1402.'
  wavelength=1402
ENDIF 

;
; The keyword IXRANGE= prevents the entire data array being read when
; calling iris_getwindata. If WINDATA has been input, though, then
; this isn't necessary.
;
; The implementation for single-valued xpix is a bit messy as
; iris_getwindata must accept a range of X-values.
;
; Note: ix and (ix0,ix1) are the pixel positions within the windata
; output that identify the pixel selected by the user. ix is for the
; case when a single pixel is specified.
;
IF n_tags(windata) EQ 0 THEN BEGIN 
  IF n_elements(xpix) EQ 1 THEN BEGIN
    ix0=max([0,xpix-1])
    ix1=xpix+1
    ixrange=[ix0,ix1]
    IF xpix EQ 0 THEN ix=0 ELSE ix=1
  ENDIF ELSE BEGIN
    IF xpix[1]-xpix[0] GT 1 THEN BEGIN
      ixrange=xpix
      ix0=0
      ix1=ixrange[1]-ixrange[0]
    ENDIF ELSE BEGIN
      ixrange=[max([0,xpix[0]-1]),xpix[1]+1]
      IF xpix[0] EQ 0 THEN ix0=0 ELSE ix0=1
      ix1=ixrange[1]-ixrange[0]-2+ix0
    ENDELSE 
  ENDELSE
ENDIF ELSE BEGIN
  ix=xpix
ENDELSE 


;
; I've added this for the cases of big files where I don't want to reload
; windata. 
;
IF n_tags(windata) EQ 0 THEN BEGIN 
  windata=iris_getwindata(file,wavelength,ixrange=ixrange,wrange=wrange, $
                          clean=clean, keep_sat=keep_sat, /quiet)
ENDIF

IF n_tags(windata) EQ 0 THEN BEGIN
  print,'% IRIS_SUM_SPEC: wavelength not available for this data-set. Returning...'
  return,-1
ENDIF 

wvl=windata.wvl
nw=n_elements(wvl)

nx=n_elements(xpix)
ny=n_elements(ypix)
CASE 1 OF
  nx EQ 1 AND ny EQ 1: BEGIN
    int=reform(windata.int[*,ix,ypix])
    err=reform(windata.err[*,ix,ypix])
    npix=1
  END
 ;
  nx GT 1 AND ny EQ 1: BEGIN
    npix=xpix[1]-xpix[0]+1
    int_img=reform(windata.int[*,ix0:ix1,ypix])
    int=average(int_img,2,missing=windata.missing)
   ;
    err_img=reform(windata.err[*,ix0:ix1,ypix])
    err=fltarr(nw)
    FOR i=0,nw-1 DO BEGIN
      k=where(err_img[i,*] NE windata.missing,nk)
      IF nk EQ 0 THEN BEGIN
        err[i]=windata.missing
      ENDIF ELSE BEGIN
        err[i]=sqrt(total(err_img[i,k]^2)) / float(nk)
      ENDELSE
    ENDFOR 
  END
 ;
  nx EQ 1 AND ny GT 1: BEGIN
    npix=ypix[1]-ypix[0]+1
    int_img=reform(windata.int[*,ix,ypix[0]:ypix[1]])
    int=average(int_img,2,missing=windata.missing)
   ;
    err_img=reform(windata.err[*,ix,ypix[0]:ypix[1]])
    err=fltarr(nw)
    FOR i=0,nw-1 DO BEGIN
      k=where(err_img[i,*] NE windata.missing,nk)
      IF nk EQ 0 THEN BEGIN
        err[i]=windata.missing
      ENDIF ELSE BEGIN
        err[i]=sqrt(total(err_img[i,k]^2)) / float(nk)
      ENDELSE
    ENDFOR 
  END
 ;
  nx GT 1 AND ny GT 1: BEGIN
    npix=(ypix[1]-ypix[0]+1)*(xpix[1]-xpix[0]+1)
    int_img=reform(windata.int[*,ix0:ix1,ypix[0]:ypix[1]])
    int=average(int_img,[2,3],missing=windata.missing)
   ;
    err=fltarr(nw)
    FOR i=0,nw-1 DO BEGIN
      err_img=reform(windata.err[i,ix0:ix1,ypix[0]:ypix[1]])
      k=where(err_img NE windata.missing,nk)
      IF nk NE 0 THEN BEGIN
        err[i]=sqrt( total( err_img[k]^2 ) ) / float(nk)
      ENDIF ELSE BEGIN
        err[i]=windata.missing
      ENDELSE 
    ENDFOR 
  END
ENDCASE 


IF nx EQ 1 THEN BEGIN
  exp_time=windata.exposure_time[ix]
ENDIF ELSE BEGIN
  exp_time=average(windata.exposure_time[ix0:ix1])
ENDELSE 

specstr={wvl: wvl, int: int, err: err, $
         xpix: xpix, ypix: ypix, file: file, $
         npix: npix, exp_time: exp_time, $
         xscale: 0.33, yscale: windata.scale[1], $
         missing: windata.missing, $
         date_obs: windata.hdr.date_obs, time_stamp: systime(), $
         int_units: windata.units, $
         wvl_units: 'angstroms'}

;
; This deletes windata if it hadn't been input to the routine.
;
IF n_elements(ixrange) NE 0 THEN junk=temporary(windata)

return,specstr

END
