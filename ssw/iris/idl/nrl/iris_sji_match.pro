
FUNCTION iris_sji_match, rastfile

;+
; NAME:
;     IRIS_SJI_MATCH()
;
; PURPOSE:
;     Given a filename for an IRIS raster, this routine searches in
;     the user's $IRIS_DATA directories for matching SJI files.
;
; CATEGORY:
;     IRIS; file-handling
;
; CALLING SEQUENCE:
;     Result = IRIS_SJI_MATCH(Rastfile)
;
; INPUTS:
;     Rastfile:  The name of an IRIS raster file.
;
; OUTPUTS:
;     A string (or string array) contain the name(s) of SJI files that
;     match the input RASTFILE. If a file isn't found then an
;     empty string is returned.
;
; EXAMPLE:
;     Find a raster file:
;     IDL> file=iris_find_file('29-Mar-2014 17:00')
;
;     Find the matching SJI files:
;     IDL> sji_files=iris_sji_match(file)
;
; CALLS:
;     BREAK_PATH, IRIS_FILE2DATE
;
; MODIFICATION HISTORY:
;     Ver.1, 18-Feb-2015, Peter Young
;     Ver.2, 19-Feb-2015, Peter Young
;       Speeded up search by being more specific about the directory. 
;-


basename=file_basename(rastfile)
rootname=strmid(basename,0,34)

search_str=rootname+'*SJI*.fits'

;
; This extracts the directory of the file in the form YYYY/MM/DD.
;
t=iris_file2date(rastfile,dir=dir)

;
; Note that I only look for level2 files.
;
search_dir=concat_dir('level2',dir)

iris_data=getenv('IRIS_DATA')
IF iris_data EQ '' THEN BEGIN
  print,'% IRIS_FIND_FILE: Please define the environment variable $IRIS_DATA to point to the top directory'
  print,'                  of your IRIS data directory.'
  print,'                  Note that level-2 files are expected to be in $IRIS_DATA/level2.'
  return,''
ENDIF 

iris_paths=BREAK_path(iris_data,/nocurrent)

n=n_elements(iris_paths)
FOR i=0,n-1 DO iris_paths[i]=concat_dir(iris_paths[i],search_dir)

chck=file_search(iris_paths,search_str,count=n)

IF n EQ 0 AND NOT keyword_set(quiet) THEN BEGIN
  print,'% IRIS_SJI_MATCH:  No SJI files found.'
  return,''
ENDIF

return,chck

END
