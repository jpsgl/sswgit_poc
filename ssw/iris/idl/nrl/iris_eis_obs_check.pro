
PRO iris_eis_obs_check, t0, t1, out_string=out_string, margin=margin

;+
; NAME:
;      IRIS_EIS_OBS_CHECK
;
; PURPOSE:
;      Given a start and stop time, this routine checks what
;      observations EIS was doing. The output 'out_string' is intended
;      for display from iris_raster_browser.
;
; CATEGORY:
;      IRIS; Hinode/EIS; metadata.
;
; CALLING SEQUENCE:
;      IRIS_OBS_CHECK, T0, T1
;
; INPUTS:
;      T0:    A start time given in a standard Solarsoft format.
;      T1:    A stop time given in a standard Solarsoft format.
;
; OPTIONAL INPUTS:
;      Margin: A float specifying the time in minutes to extend the
;              time range by when searching for EIS rasters.
;
; OUTPUTS:
;      None.
;
; OPTIONAL OUTPUTS:
;      OUT_STRING:  A string array containing EIS metadata.
;
; RESTRICTIONS:
;      The user needs to have the EIS software tree within Solarsoft. 
;
; CALLS:
;      EIS_OBS_STRUCTURE
;
; EXAMPLE:
;      IDL> iris_eis_obs_check,'29-mar-2014 16:00','29-mar-2014 18:00'
;
; MODIFICATION HISTORY:
;      Beta 1, 31-Oct-2016, Peter Young
;      Beta 2, 1-Nov-2016, Peter Young
;        Added MARGIN= input; adjusted output format; added input
;        check.
;      Beta 3, 2-Nov-2016, Peter Young
;        Minor change to output format; added messages to out_string
;        when no entries are found
;-

IF n_params() LT 2 THEN BEGIN
  print,'Use:  IDL> iris_eis_obs_check, t0, t1 [, margin=, out_string= ]'
  return
ENDIF 

;
; Implement margin keyword to modify t0 and t1
;
IF n_elements(margin) EQ 0 THEN margin=0.
;
t0_tai=anytim2tai(t0) - margin*60.
t1_tai=anytim2tai(t1) + margin*60.
;
tt0=anytim2utc(/ccsds,t0_tai)
tt1=anytim2utc(/ccsds,t1_tai)



IF have_proc('eis_obs_structure') THEN BEGIN
  str=eis_obs_structure(tt0,tt1)
  IF n_tags(str) NE 0 THEN BEGIN
    out_string=['For the period', $
                '   start: '+anytim2utc(/ccsds,/time,/trunc,t0), $
                '   stop:  '+anytim2utc(/ccsds,/time,/trunc,t1), $
                'EIS was running the rasters listed below.', $
                '']
    out_string=[out_string,string(format='(a8,2x,a8,2x,a20,2x,a7,a7,2x,a11,69x)','START','STOP','STUDY ACRONYM','XCEN','YCEN','DESCRIPTION')]
    n_eis=n_elements(str)
    FOR i=0,n_eis-1 DO BEGIN
      t_eis_0=anytim2utc(/ccsds,/time,/trunc,str[i].date_obs)
      t_eis_1=anytim2utc(/ccsds,/time,/trunc,str[i].date_end)
      obstitle=trim(str[i].obstitle)
      obs_dec=trim(str[i].obs_dec)
      IF obs_dec EQ obstitle OR obs_dec EQ '' THEN BEGIN
        desc=strpad(trim(str[i].obstitle),80,/after)
      ENDIF ELSE BEGIN
        desc=strpad(trim(str[i].obstitle)+' ('+trim(str[i].obs_dec)+')',80,/after)
      ENDELSE 
      out_string=[out_string, $
                  string(format='(a8,2x,a8,2x,a20,2x,2f7.1,2x,a80)',t_eis_0,t_eis_1, $
                         str[i].stud_acr, $
                         str[i].xcen,str[i].ycen,desc) ]
    ENDFOR 
  ENDIF ELSE BEGIN
    out_string='No entries are listed in the EIS data catalog.'
  ENDELSE 
ENDIF ELSE BEGIN
  out_string='Your Solarsoft does not contain the Hinode/EIS software tree.'
ENDELSE 


END
