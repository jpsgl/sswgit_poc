
PRO iris_sji_burst_check, date, output=output

;+
; NAME:
;      IRIS_SJI_BURST_CHECK
;
; PURPOSE:
;      Automatically find bursts in IRIS 1400 SJI images.
;
; CATEGORY:
;      IRIS; SJI; image processing.
;
; CALLING SEQUENCE:
;      IRIS_SJI_BURST_CHECK, DATE
;
; INPUTS:
;      DATE:   Specify a time in a standard SSW format corresponding a
;              particularly IRIS file.
;
; OPTIONAL INPUTS:
;      Sig_Factor:  The number of sigmas above the mean image
;                   intensity used to flag a burst. The default is
;                   10. 
;
; OUTPUTS:
;      See optional outputs.
;
; OPTIONAL OUTPUTS:
;      Output:  An IDL list. Each element of the list is an IDL
;               structure. If an image contains at least one burst,
;               then the image will have an entry in the list. The
;               tags of the structure are:
;               .index  An integer array containing indices of burst
;                       pixels.
;               .group  An integer array containing the group index of
;                       each pixel.
;               .intensity  A float array containing the intensity of
;                       each pixel.
;               .npix   Number of burst pixels.
;               .im_index  Integer giving the image index.
;               .nevents Number of burst events (groups).
;               .nx     X-size of SJI image.
;               .ny     Y-size of SJI image.
;
; EXAMPLE:
;      IDL> iris_sji_burst_check,'22-oct-2013 21:00',output=output
;      IDL> help,output[0]
;      ** Structure <205dc98>, 8 tags, length=7200, data length=7197, refs=2:
;         INDEX           LONG      Array[798]
;         GROUP           BYTE      Array[798]
;         INT             FLOAT     Array[798]
;         NPIX            LONG               798
;         IM_INDEX        INT              0
;         NEVENTS         BYTE        25
;         NX              LONG              1393
;         NY              LONG              1093
;
; MODIFICATION HISTORY:
;      Ver.1, 16-Mar-2017, Peter Young
;      Ver.2, 2-May-2017, Peter Young
;         Updates to header.
;-

;
; The routine identifies bursts that are sig_factor times the standard
; deviation of the image intensity above the median value of the image.
;
IF n_elements(sig_factor) EQ 0 THEN sig_factor=10.0

IF n_params() LT 1 THEN BEGIN
  print,'Use:  IDL> iris_sji_burst_check, date [, output=output, sig_factor=]'
  return
ENDIF 

file=iris_find_file(date,sji=1400,count=count)

IF count EQ 0 THEN BEGIN
  print,'%IRIS_SJI_BURST_CHECK: the 1400 SJI filter is not available for this data-set. Returning...'
  return 
ENDIF 

junk=temporary(output)

read_iris_l2,file,index,data
s=size(data,/dim)

n=s[2]


str={time: '', xpix: 0, ypix: 0, thresh: 0., ybin: 0, median: 0., $
     intensity: 0., group: 0, file_index: 0, x: 0., y: 0.}


FOR i=0,n-1 DO BEGIN
  img=reform(data[*,*,i])
  k=where(img NE -200.)
  med_img=median(img[k])
  sig_img=stdev(img[k])
  j=where(img GE 10.*sig_img+med_img,nj)
  print,format='("Image: ",i3," Med: ",f6.1," St.Dev: ",f6.1," Npix: ",i5)', $
        i,med_img,sig_img,nj


  IF nj GT 0 THEN BEGIN
   ;
   ; The following code groups neighboring pixels into groups, with
   ; each group assigned an index (group).
   ; I have to define imgx as region_grow does not work on edge
   ; pixels. 
   ;
    mask=bytarr(s[0],s[1])
    mask[j]=1b
    group=bytarr(s[0],s[1])
    group_num=1b
   ;
    WHILE total(mask) NE 0 DO BEGIN
      ii=where(mask EQ 1b,nii)
      roi_pix=region_grow(mask,ii[0],/all_neigh)
     ;
     ; Ignore any isolated pixels.
     ;
      IF n_elements(roi_pix) EQ 1 THEN BEGIN
        mask[roi_pix]=0b
      ENDIF ELSE BEGIN 
        group[roi_pix]=group_num
        group_num=group_num+1b
        mask[roi_pix]=0b
      ENDELSE
    ENDWHILE
   ;
    junk=temporary(str)
    k=where(group GE 1,nk)
    IF nk NE 0 THEN BEGIN 
      str={index: k, $
           group: group[k], $
           int: img[k], $
           npix: nk, $
           im_index: i, $
           nevents: max(group), $
           nx: s[0], $
           ny: s[1]}
      IF n_elements(output) EQ 0 THEN output=list(str) ELSE output.add,str
    ENDIF 
   ;
   ; Now go through each pixel and store information in the output
   ; structure.
   ;
    ;; FOR j=0,nk-1 DO BEGIN
    ;;   ij=array_indices(int,k[j])
    ;;   tstr=anytim2utc(/ccsds,wd.time_ccsds[ij[0]],/time,/trunc)
    ;;  ;
    ;;   str.time=wd.time_ccsds[ij[0]]
    ;;   str.file=filename[i]
    ;;   str.file_index=i
    ;;   str.xpix=ij[0]
    ;;   str.ypix=ij[1]
    ;;   str.thresh=threshold*float(ybin)
    ;;   str.ybin=ybin
    ;;   str.median=med_int[k[j]]
    ;;   str.intensity=int[k[j]]
    ;;   str.group=group[k[j]]
    ;;   IF n_tags(output) EQ 0 THEN output=str ELSE output=[output,str]
  ENDIF
ENDFOR


END
