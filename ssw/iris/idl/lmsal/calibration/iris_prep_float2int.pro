pro IRIS_PREP_FLOAT2INT, index, data, $
	oindex, odata, $
	bscale = bscale, bzero = bzero, blank = blank, maxval = maxval, $
	long = long
	
;+
;
; Converts floating-point IRIS data (lev1.5 and up) to scaled integers, using
; either 16 or 18 bits
;
; INPUTS: 
;	index	-	FITS header structure (or structure vector)
;	data	-	2D or 3D FLOAT/DOUBLE array of data
;
; OUTPUTS:
;	oindex	-	FITS header(s) with BSCALE, BZERO and BLANK updated
;	odata	-	2D or 3D array of data, of type UINT or ULONG
;
; KEYWORDS:
;	bscale	-	set to the scale factor to be used; defaults to 0.25 for 16-bit
;				or 0.125 for 18-bit
;	bzero	-	set to the zero offset to be used; defaults to -200. Note that the
;				data are blocked from taking this value
;	blank	-	set to the integer value to be interpreted as missing data
;				Defaults to -200
;	maxval	-	set to the highest pixel value (from the floating-point image)
;				that is not to the ceiling; defaults to 16383 + bzero for 16-bit
;				or 32767 + bzero for 18-bit
;	/long	-	set to use the 18-bit option; defaults to the 16-bit option
;				
;-

nanmask = WHERE(data ne data, numnan)
if not KEYWORD_SET(bzero) then bzero = -200.
if not KEYWORD_SET(blank) then blank = -200

if KEYWORD_SET(long) then begin
	ushift = 0l - 2l^31l
	if not KEYWORD_SET(bscale) then bscale = 0.125
	if not KEYWORD_SET(maxval) then maxval = 32767 + bzero
	clipdata = (bzero + 1) > data < maxval
	odata = LONG( ROUND((clipdata - bzero) / bscale + ushift) )
endif else begin
	ushift = 0 - 2^15
	if not KEYWORD_SET(bscale) then bscale = 0.25
	if not KEYWORD_SET(maxval) then maxval = 16383 + bzero
	clipdata = (bzero + 1) > data < maxval
	odata = FIX( ROUND( (clipdata - bzero) / bscale + ushift) )
endelse

odata[nanmask] = blank

oindex = ADD_TAG(index, blank, 'blank')
oindex = ADD_TAG(oindex, bscale, 'bscale')
oindex = ADD_TAG(oindex, bzero, 'bzero')

end
	
