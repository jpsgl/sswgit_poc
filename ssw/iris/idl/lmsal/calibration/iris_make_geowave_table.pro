pro IRIS_MAKE_GEOWAVE_TABLE, kx, ky, $
	filename = filename, append = append, $
	rec_num = rec_num, img_path = img_path, t_start = t_start, $
	t_stop = t_stop, version = version, $
	path = path, history = history, pstop = pstop
	
;+
;
; Construct a table of geometric/wavelength correction coefficients. Wrapper
; for S. Jaeggli's IRIS_SPEC_CAL; see ITN 19
;
; INPUT:
;	kx	-	The x-axis correction matrix 
;	ky	-	The y-axis correction matrix
;
; KEYWORDS:
;	filename	-	the filename of the geometry calibration file to write
;	path=		-	the path to the output file
;	/append		-	If set, then the input data are appended to the specified
;					filename. By default, a new file is written
;
;	The following keywords specify the parameters of the correction entry to write
;	rec_num=	-	Integer record number (this routine adds 300 to it)
;	img_path=	-	string indicating the channel of the image
;	t_start=	-	string indicating the start time for the record entry (ISO)
;	t_stop=		-	string indicating the stop time for the record entry (ISO)
;	version=	-	integer version number
;	history=	-	string specifying the source of the data
;
; WRITTEN:
;	P.Boerner 2013/06 boerner ~ at ~ lmsal ~ dot ~ com
;
;-

tnow = RELTIME(/now, out = 'CCSDS')
tnow_tag = TIME2FILE(tnow, /sec)
if not KEYWORD_SET(rec_num) then rec_num = 0l
if not KEYWORD_SET(img_path) then img_path = 'NUV'
if not KEYWORD_SET(t_start) then t_start = '2013-07-01T00:00:00Z'
if not KEYWORD_SET(t_stop) then t_stop = '2033-01-01T00:00:00Z'
if not KEYWORD_SET(version) then version = 0
if not KEYWORD_SET(history) then history = ''
if N_ELEMENTS(path) eq 0 then path = GET_LOGENV('SSW_IRIS_DATA')
if N_ELEMENTS(filename) eq 0 then begin
	filename = tnow_tag + '_geowave_poly.genx'
	filename = CONCAT_DIR(path, filename)
endif

this_rec_str = {rec_num : LONG(300+rec_num), img_path : img_path, t_start : t_start, t_stop : t_stop, $
	version : version, date : tnow, history : history, kx : kx, ky : ky}

if KEYWORD_SET(pstop) then STOP

if KEYWORD_SET(append) then begin
	RESTGEN, file = filename, current_str
	outstr = [current_str, this_rec_str]
endif else begin
	outstr = [this_rec_str]
endelse
SAVEGEN, file = filename, outstr

end