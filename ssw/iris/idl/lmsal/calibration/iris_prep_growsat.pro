function IRIS_PREP_GROWSAT, data

;+
;
; Given an IRIS image with saturated pixels marked as Infs, expand the region
; of Infs in both X and Y axes. 
;
;
;
;-

satpix = WHERE(data eq data and not FINITE(data), numsat)

if numsat gt 0 then begin
	odata = data
	datsize = SIZE(data)
	bigsatpix = satpix
	for i = 0, 8 do begin
		xshift = (i / 3) - 1
		yshift = (i mod 3) - 1
		shiftx = (satpix mod datsize[1]) + xshift
		shifty = (satpix / datsize[1]) + yshift
		noborder = WHERE(shiftx ge 0 and shiftx lt datsize[1] and $
			shifty ge 0 and shifty lt datsize[2], numthissat)
		if numthissat gt 0 then begin
			shiftsatpix = shifty[noborder] * datsize[1] + shiftx[noborder]
			bigsatpix = [bigsatpix, shiftsatpix]
		endif
	endfor
	odata[bigsatpix] = !values.f_infinity
	RETURN, odata
endif else begin
	RETURN, data
endelse

end