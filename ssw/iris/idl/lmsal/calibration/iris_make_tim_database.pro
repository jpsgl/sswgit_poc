pro IRIS_MAKE_TIM_DATABASE, date, $
	outpath = outpath, fname = fname
	
;+
; 
; Generates a text file giving the commanded roll from the IRIS timeline
;
; INPUT:
;	date	-	The day for which to generate the file
;
; KEYWORDS:
;	outpath	-	path to the output file
;	fname	-	name of the output file (not including the path)
;
;-

nullval = -9999d

if N_ELEMENTS(date) eq 0 then date = '2013-jul-17'
if N_ELEMENTS(outpath) eq 0 then outpath = '/sanhome/boerner/public_html/iris/tim/'

t0	 	= ANYTIM2UTC(date, /ccsds)
tai0	= UTC2TAI(STRMID(t0, 0, 10))
day0	= TAI2UTC(tai0, /ccsds)
tstart 	= STRMID(TAI2UTC(/ccsds, tai0 - 3d * 24d * 3600d), 0, 10) + 'T00:00:00Z'
tstop	= STRMID(day0, 0, 10) + 'T23:59:59.99Z'

data = IRIS_TIME2TIMELINE(tstart, tstop, /roll)
default_data = CREATE_STRUCT('ROLL_START', day0, 'DEGREES', 0., 'RADIANS', 0., $
		'DURATION', 0., 'ROLL_END', day0)
		
numcol = N_TAGS(data)

if numcol eq 0 then begin
	PRINT, 'No roll commands in time window!'
	data = default_data
endif 

pretime = WHERE(ANYTIM2TAI(data.roll_start) le tai0, numpre)
posttime = WHERE(ANYTIM2TAI(data.roll_start) gt tai0, numpost)
predat = default_data
if numpre ne 0 then begin
	predat.degrees = data[pretime[-1]].degrees
	predat.radians = data[pretime[-1]].radians
endif
if numpost eq 0 then data = predat else data = [predat, data[posttime]]
numrow = N_ELEMENTS(data)

tname = TIME2FILE(date, /date)
fname = tname + '_iris_tim.txt'
outfile = CONCAT_DIR(outpath, fname)
OPENW, lun, /get_lun, outfile
PRINTF, lun, TAG_NAMES(data), form = '(a10,a25,2a12,a25)'

for i = 0, numrow - 1 do PRINTF, lun, data[i], form = '(a23,3f12.5,a25)'

FREE_LUN, lun

end




