function IRIS_PREP_ADD_HEADER, iindex

;+
;
; Adds new keywords to the level1 IRIS FITS headers to make them lev1.5 headers
; (Note that the keywords are not actually populated at this point, just added)
;
; WRITTEN: 2013/11/01	PB
;
;-

tagnames = [	['IPRPVER' , 'F'], $		;	Version of iris_prep
				['IPRPPDBV', 'F'], $		;	Version of iris_mk_pointdb
				['IT01PMRF', 'F'], $		;	PM Temperature (for pointing)
				['IT06TELM', 'F'], $		;	MidTel Temperature (for pointing)
;				['IT14SPPX', 'F'], $		;	Spectrograph +X Temperature (for wavelength)
				['IT15SPPY', 'F'], $		;	Spectrograph +Y Temperature (for wavelength)
;				['IT16SPNX', 'F'], $		;	Spectrograph -X Temperature (for wavelength)
				['IT17SPNY', 'F'], $		;	Spectrograph -Y Temperature (for wavelength)
				['IT18SPPZ', 'F'], $		;	Spectrograph +Z Temperature (for wavelength)
				['IT19SPNZ', 'F'], $		;	Spectrograph -Z Temperature (for wavelength)
				['IPRPDTMP', 'S'], $		;	Comma-delimited string list of temperatures used by iris_make_dark
				['IPRPDVER', 'I'], $		;	Version of iris_make_dark
				['IPRPBVER', 'I'], $		;	Version of background subtraction
				['IPRPFVER', 'I'], $		;	Recnum of flatfield
				['IPRPGVER', 'I'], $		;	Version of geometry correction
				['IPRPPVER', 'I'], $		;	Version of bad-pixel mask (not used)
				['IPRPOFFX', 'F'], $		;	X (spectral direction) shift in pixels
				['IPRPOFFY', 'F'], $		;	Y (spatial direction) shift in pixels
				['IPRPOFFF', 'I'] 		]	;	Flag indicating source of X and Y offsets
numtags = N_ELEMENTS(tagnames) / 2

oindex = iindex
for i = 0, numtags - 1 do begin
	case tagnames[1,i] of
		'S'		:	null = ''
		'I'		:	null = 0l
		'F'		:	null = 0.
		else	:	null = 0.
	endcase
	oindex = ADD_TAG(oindex, null, tagnames[0,i])
endfor

RETURN, oindex

end