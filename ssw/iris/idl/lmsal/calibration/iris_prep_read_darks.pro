pro IRIS_PREP_READ_DARKS, verbose = verbose, $
	version = version

;+
;
; Read in all the dark data and store them in a common block
;
;-

if KEYWORD_SET(verbose) then begin
	t0 = SYSTIME(/sec)
	PRINT, 'IRIS_PREP_READ_DARKS: Reading all IRIS darks...'
endif else verbose = 0

common IRIS_DARK_CB, darkstr

sswdata = CONCAT_DIR('$SSW_IRIS','data')
CD, sswdata, current = old_dir

;RPT updating finally NOPE that becomes dark_avg_nuv_20130925.dat
if not KEYWORD_SET(version) then version = '20130925'

darkfile = FILE_SEARCH('dark_avg_*' + version + '.dat')

for i = 0, N_ELEMENTS(darkfile) - 1 do begin
	RESTORE, darkfile[i], verbose = verbose
	thisname = STRSPLIT(/extract, /regex, $
		STRSPLIT(/extract, /regex, darkfile[i], '_' + version + '.dat'), 'dark_avg_')
	thisstr = CREATE_STRUCT('DARK', DARK, 'TBND0', TBND0, 'DT', DT, $
		'CPED', CPED, 'CD', CD, 'SL1', SL1, 'SL2', SL2, 'IT', IT, $
		 'POFF', POFF)
	if i eq 0 then darkstr = CREATE_STRUCT(thisname, thisstr) $
		else darkstr = CREATE_STRUCT(darkstr, thisname, thisstr)
endfor

if KEYWORD_SET(verbose) then begin
	dt0 = SYSTIME(/sec) - t0
	PRINT, 'IRIS_PREP_READ_DARKS: Darks loaded in ' + $
		STRING(dt0, form = '(f6.2)') + ' seconds'	
endif

darkstr = CREATE_STRUCT(darkstr, 'version', version)

CD, old_dir

end
