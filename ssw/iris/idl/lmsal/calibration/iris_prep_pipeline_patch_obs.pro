pro IRIS_PREP_PIPELINE_PATCH_OBS, obs_str, logdir, $
	nopng = nopng, pstop = pstop, nrt = nrt

;+
;
; Write obsfits.genx and description.genx files in /irisa/data/prep/ for a given OBS
;
; Run in the pipeline to perform the OBS-level fitting of fiducial position, 
; wavelength, AIA cross-correlation, etc.  It assumes that the image-level fitting
; has already been done and produced text files.
;
; INPUT:
;	obs_str	-	OBS description structure (e.g. an element from the output of 
;				iris_time2timeline() )
;	logdir	-	string specifying the path where the txt files with the
;				per-image measurements are written (and where the output of
;				this routine should go)
;
; KEYWORDS:
;	/nopng	-	(SWITCH) If not set, then PNGs of the fits are saved in the logdir
;	/nrt	-	(SWITCH) Set if only the lev1_nrt data is available for this OBS
;	/pstop	-	(SWITCH) stop for debugging
;
;-

tt00 = SYSTIME(/sec)

if not KEYWORD_SET(nrt) then nrt = 0
if not KEYWORD_SET(pstop) then pstop = 0
if KEYWORD_SET(nopng) then png = 0 else png = 1

; Now make the description.genx and obsfits.genx files
IRIS_PREP_TREND_META, logdir, /loud, nrt = nrt, obs = obs_str, result = mresult
if N_TAGS(mresult) gt 0 then begin
	IRIS_PREP_FILTER_OBS, logdir, /save, /runtime, png = png, $
		pstop = pstop, result = result, /pipeline
	if N_TAGS(result) gt 0 then begin
		PRINT, 'Finished with OBS, seems ok...', form = '(a60)'
	endif else begin
		PRINT, '**FAILED obsfit (no lev2_nrt?)**', form = '(a60)'
	endelse
endif else begin
	PRINT, '**FAILED description (too many images?)**', form = '(a60)'
endelse

; Clean up and exit
PRINT, 'Elapsed time = ', SYSTIME(/sec) - tt00, form = '(a30, f10.1)'

end
