pro IRIS_PREP_TEST, $
	set = set, logdir = logdir, serial = serial, $
	pstop = pstop, show = show, jpg = jpg, remote = remote, _extra = extra

;+
;
;
;
;
;-

TVLCT, rr, gg, bb, /get
LOADCT, 0

;+++++++++++++++++++++++++++++
; Select the data set
; based on either the set=
; keyword or a single switch
;-----------------------------
if not KEYWORD_SET(set) then begin
	if N_TAGS(extra) eq 1 then begin
		set = STRLOWCASE(TAG_NAMES(extra))
	endif else begin
		set = 'sep'
	endelse
endif

ds = 'iris.lev1'
case STRLOWCASE(set) of
	'dark' : begin		; Unbinned dark frames
		t1 = '12-sep-2013 05:30'
		t2 = '12-sep-2013 06:00'
		remote = 0
	end	
	'bindark' : begin	; Binned dark frames
		t1 = '12-sep-2013 04:57'
		t2 = '12-sep-2013 05:30'
		remote = 0
	end	
	'fast' : begin		; Fast Si IV SJI + Spectra (FUV sumsptrl = 4)
		t1 = '13-sep-2013 05:10'
		t2 = '13-sep-2013 05:20'
	end	
	'chwaist' : begin	; Coronal hole waist
		t1 = '14-sep-2013 01:40'
		t2 = '14-sep-2013 03:40'
		remote = 0
	end	
	'wobble' : begin		; Spectrograph wobble sequence
		t1 = '10-sep-2013 14:15'
		t2 = '10-sep-2013 16:00'
		remote = 0
	end	
	'test' : begin		; Throughput monitoring on test date
		t1 = '29-aug-2013 04:21'
		t2 = '29-aug-2013 04:50'
	end	
	'cur' : begin		; Throughput monitoring on recent (nrt) date
		t1 = RELTIME(/now, hours = -6)
		t2 = RELTIME(t1, hours = -1)	
		ds = 'iris.lev1_nrt'
	end	
	'aug' : begin		; Throughput monitoring from Aug 26
		t1 = '26-aug-2013 13:18'
		t2 = '26-aug-2013 14:00'
	end
	'mosaic' : begin		; FDM 
		t1 = '12-oct-2013 05:00'
		t2 = '12-oct-2013 06:00'
		remote = 0
	end	
	'sat' : begin		; Saturation from C5 flare
		t1 = '12-oct-2013 01:20'
		t2 = '12-oct-2013 01:50'
	end
	'all' : begin
		sets = ['sat', 'mosaic', 'aug', 'cur', 'test', 'wobble', 'chwaist', $
			'fast', 'bindark', 'dark']
		for i = 0, N_ELEMENTS(sets) - 1 do IRIS_PREP_TEST, set = sets[i], /show, /jpg, /loud, /run
	end
	else	:	begin	;	Default; Throughput monitoring from Sep 5
		t1 = '5-sep-2013 04:31'
		t2 = '5-sep-2013 04:50'
	end
endcase

;+++++++++++++++++++++++++++++
; Get the data (or at least 
; the headers/filenames)
;-----------------------------
if KEYWORD_SET(remote) then begin
	ssw_jsoc_time2data, t1, t2, drms, alldat, /jsoc2, ds = ds
endif else begin
	ssw_jsoc_time2data, t1, t2, drms, files, /files, /jsoc2, ds = ds
endelse

;+++++++++++++++++++++++++++++
; Now set up some defaults
;-----------------------------
chans = ['SJI_1330', 'SJI_1400', 'SJI_2796', 'SJI_2832', 'NUV', 'FUV']
maxes = [300, 400, 600, 3000, 800, 100]
if STRLOWCASE(set) eq 'sat' then maxes = maxes * [3., 3., 2., 2., 2., 1.5]
obsid = STRING(MEDIAN(drms.isqoltid), form = '(i10)')
if not KEYWORD_SET(logdir) then $
	logdir = CONCAT_DIR( '/Volumes/disk2/data/iris/iris_prep/test/', STRLOWCASE(set) )

if KEYWORD_SET(pstop) then STOP

for i = 0, N_ELEMENTS(chans) - 1 do begin
	index = WHERE(drms.img_path eq chans[i], numin)
	if numin eq 0 then begin
		PRINT, 'no images of type ', chans[i]
		goto, skiploop
	endif
	
	; Load the image cube
	if KEYWORD_SET(remote) then begin
		hdr = drms[index]
		dat = alldat[*,*,index]
	endif else begin
		if KEYWORD_SET(pstop) then STOP
		READ_IRIS, files[index], hdr, dat, /use_shared
;		READ_IRIS, files[index], hdr, dat, /comp_delete, /uncomp_delete
	endelse
	
	; If /serial, then call iris_prep image by image
	if KEYWORD_SET(serial) then begin
		t0 = SYSTIME(/sec)
		IRIS_PREP, hdr[0], dat[*,*,0], ohdr, odat, logdir = logdir, _extra = extra
		if numin gt 1 then begin
			template = ohdr
			ohdr = REPLICATE(template, numin)
			tdat = odat
			odat = dat
			odat[*,*,0] = tdat
			for j = 1, numin-1 do begin
				IRIS_PREP, hdr[j], dat[*,*,j], thdr, tdat, logdir = logdir, _extra = extra
				STRUCT_ASSIGN, thdr, template
				ohdr[j] = template
				odat[*,*,j] = tdat
			endfor
		endif
		BOX_MESSAGE, $
			[STRING('IRIS_PREP_TEST: ', numin, 'images prepped', format = '(a15, i5, a15)'), $
			 STRING('Run time: ' , SYSTIME(/sec)-t0, 'seconds', form = '(a12, f7.1, a8)')]
	endif else begin
		IRIS_PREP, hdr, dat, ohdr, odat, logdir = logdir, _extra = extra
	endelse
	
	if KEYWORD_SET(show) then begin
	; Generate images showing the location of the fiducials, with reference
	; lines drawn in...

		maxes[i] = maxes[i] / 15. * (hdr[0].exptime > 1.)
		sumspat = hdr[0].sumspat
		sumsptrl = hdr[0].sumsptrl
	
		if chans[i] ne 'FUV' then begin
			oo = [dat[200/sumsptrl:900/sumsptrl,200/sumspat:900/sumspat,0]-100, $
				odat[200/sumsptrl:900/sumsptrl, 200/sumspat:900/sumspat,0]]
			LOADCT, 0
			WDEF, 9+i, 1400/sumsptrl, 700/sumspat, title = chans[i]
			TV, BYTSCL(oo, min = 0, max = maxes[i]) 
			PB_SET_LINE_COLOR
			PLOTS, /dev, [320, 320]/sumsptrl, [0, 700]/sumspat, col = 2
			PLOTS, /dev, [1020, 1020]/sumsptrl, [0, 700]/sumspat, col = 2
			PLOTS, /dev, [0, 1400]/sumsptrl, ([0, 0] + 508 + 270 - 200)/sumspat, col = 2
			PLOTS, /dev, [0, 1400]/sumsptrl, ([0, 0] + 508 - 270 - 200)/sumspat, col = 2
			if chans[i] eq 'NUV' then begin
				PLOTS, /dev, 459.3 * [1, 1] / sumsptrl, [0,700]/sumspat, col = 6
				PLOTS, /dev, 1159.3 * [1, 1] / sumsptrl, [0,700]/sumspat, col = 6
			endif
			filename = TIME2FILE(t1) + '_' + obsid + '_' + chans[i] + '.jpg'
			if KEYWORD_SET(jpg) then PB_WIN2JPG, '/sanhome/boerner/public_html/iris/prep/' + filename
		endif else begin
			; For the FUV, generate two plots showing the two halves
			tdat = FLOAT(dat[*,*,0])
			tdat[WHERE(tdat ne tdat)] = !values.f_nan
			oofs = [tdat[100/sumsptrl:800/sumsptrl,200/sumspat:900/sumspat]-100, $
				odat[100/sumsptrl:800/sumsptrl, 200/sumspat:900/sumspat,0]]
			LOADCT, 0
			WDEF, 14, 1400/sumsptrl, 700/sumspat, title = 'FUVS'
			TV, BYTSCL(oofs, min = 0, max = maxes[i]) 
			PB_SET_LINE_COLOR
			PLOTS, /dev, [320, 320]/sumsptrl, ([0, 700])/sumspat, col = 2
			PLOTS, /dev, [1020, 1020]/sumsptrl, ([0, 700])/sumspat, col = 2
			PLOTS, /dev, [0, 1400]/sumsptrl, ([0, 0] + 508 + 270 - 200)/sumspat, col = 2
			PLOTS, /dev, [0, 1400]/sumsptrl, ([0, 0] + 508 - 270 - 200)/sumspat, col = 2
			PLOTS, /dev, 119.5 * [1, 1] / sumsptrl, [0,700]/sumspat, col = 6
			PLOTS, /dev, 819.5 * [1, 1] / sumsptrl, [0,700]/sumspat, col = 6
			filename = TIME2FILE(t1) + '_' + obsid + '_FUVS.jpg'
			if KEYWORD_SET(jpg) then PB_WIN2JPG, '/sanhome/boerner/public_html/iris/prep/' + filename
			oofl = [dat[2800/sumsptrl:3500/sumsptrl,200/sumspat:900/sumspat,0]-110, $
				odat[2800/sumsptrl:3500/sumsptrl, 200/sumspat:900/sumspat,0]]
			LOADCT, 0
			WDEF, 15, 1400/sumsptrl, 700/sumspat, title = 'FUVL'
			TV, BYTSCL(oofl, min = 0, max = maxes[i]) 
			PB_SET_LINE_COLOR
			PLOTS, /dev, [320, 320]/sumsptrl, ([0, 700])/sumspat, col = 2
			PLOTS, /dev, [1020, 1020]/sumsptrl, ([0, 700])/sumspat, col = 2
			PLOTS, /dev, [0, 1400]/sumsptrl, ([0, 0] + 508 + 270 - 200)/sumspat, col = 2
			PLOTS, /dev, [0, 1400]/sumsptrl, ([0, 0] + 508 - 270 - 200)/sumspat, col = 2
			filename = TIME2FILE(t1) + '_' + obsid + '_FUVL.jpg'
			if KEYWORD_SET(jpg) then PB_WIN2JPG, '/sanhome/boerner/public_html/iris/prep/' + filename
		endelse
	endif
	
	skiploop: if KEYWORD_SET(pstop) then STOP
endfor


if not KEYWORD_SET(pstop) then pstop = 0
if not KEYWORD_SET(win) then win = 10

end
