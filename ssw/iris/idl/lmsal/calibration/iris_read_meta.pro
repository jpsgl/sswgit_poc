function IRIS_READ_META, $
	inpath = inpath
;+
;
;
;
;
;
;-

;if N_ELEMENTS(inpath) eq 0 then inpath = '/net/crom/Volumes/disk2/data/iris/iris_prep/run9/'
;CD, inpath, current = old_dir
;metafile = FILE_SEARCH('*/description.genx')
;templatefile = '20130808_041919/description.genx'
if N_ELEMENTS(inpath) eq 0 then inpath = '/irisa/data/prep/'
CD, inpath, current = old_dir
metafile = FILE_SEARCH('*/*/*/*/description.genx')
templatefile = '2013/08/08/20130808_041919/description.genx'

numf = N_ELEMENTS(metafile)

; Read in a meta file that includes the RMS fields (not all do), and
; use it as a template
RESTGEN, file = templatefile, template
template = CREATE_STRUCT('path', '20130808_041919', template)
for i = 0, 4 do template.(17+i) = 0.	;	0 out the RMS fields

for i = 0, numf - 1 do begin
	RESTGEN, file = metafile[i], thismeta
	thistemplate = template
	STRUCT_ASSIGN, thismeta, thistemplate
;	thistemplate.path = STRMID(metafile[i], 0, 15)
	thistemplate.path = STRMID(metafile[i], 11, 15)
	if i eq 0 then metas = thistemplate else metas = [metas, thistemplate]
endfor

RETURN, metas

end
