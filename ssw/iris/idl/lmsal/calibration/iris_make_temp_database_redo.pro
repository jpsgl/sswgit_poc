pro IRIS_MAKE_TEMP_DATABASE_REDO, date

;+
;
; Run in an interactive IDL session to regenerate temperature
; files for a specified date, and post them to /irisa and to the web.
;
; EXAMPLE:
;	IDL> iris_make_temp_database_redo, '2014-may-11'
;
;-

print, 'remember the setup in /home/rtimmons/workspace/IrisCron/tempcron.csh'

tt0 = SYSTIME(/sec)
timenow = TIME2FILE(RELTIME(/now), /sec)

case 1 of
	FILE_EXIST('/irisa/data/prep/aux/')	:	finalpath = '/irisa/data/prep/aux/'
	FILE_EXIST('/net/brooklyn/Volumes/earth/iris/data/prep/aux/')	:	finalpath = '/net/brooklyn/Volumes/earth/iris/data/prep/aux/'
	FILE_EXIST('/net/bay/Volumes/earth/iris/data/prep/aux/')	:	finalpath = '/net/bay/Volumes/earth/iris/data/prep/aux/'
	else	:	begin
		PRINT, 'Cannot reach irisa...'
		finalpath = '/irisa/data/prep/aux/'
	end
endcase
logpath = CONCAT_DIR(finalpath, 'log')
finalpath = CONCAT_DIR(finalpath, 'temp')

case 1 of
	FILE_EXIST('/sanhome/boerner/public_html/iris/temps')	:	pbpath = '/sanhome/boerner/public_html/iris/temps'
	else	:	begin
		PRINT, 'Cannot reach sanhome public_html...'
		pbpath = '~/iris/cronlog/'
	end
endcase

OPENW, lun, /get, CONCAT_DIR(logpath, timenow + '_temp.txt')
PRINTF, lun & PRINTF, lun, 'IRIS_MAKE_TEMP_DATABASE_REDO running... ', timenow
PRINTF, lun

; Generate data for the previous day
PRINTF, lun, '*************************************
PRINTF, lun, ' Generating data for ', date
PRINTF, lun, '*************************************
IRIS_MAKE_TEMP_DATABASE, date, outpath = '/tmp', fname = fname, /short
newfile = CONCAT_DIR('/tmp', fname)

pbfile = CONCAT_DIR(pbpath, fname)
PRINTF, lun & PRINTF, lun, 'Copying to PB web dir...'
SPAWN, 'cp ' + newfile + ' ' + pbfile, res, err
PRINTF, lun, 'cp result: ', res
PRINTF, lun, 'cp errcode: ', err

oldfile = CONCAT_DIR(finalpath, fname)
PRINTF, lun & PRINTF, lun, 'Copying to /irisa ...'
SPAWN, 'grep 9999 ' + oldfile + ' | wc -l', ofsize, oferr
SPAWN, 'grep 9999 ' + newfile + ' | wc -l', nfsize, nferr
PRINTF, lun, 'Old file: ', 1440-FIX(ofsize), 'New file: ', 1440-FIX(nfsize), '(good lines)', $
	form = '(a15,i8,a15,i8,a15)'
PRINTF, lun, 'Keeping new file ', newfile
SPAWN, 'mv ' + newfile + ' ' + oldfile, res, err
PRINTF, lun, 'mv result: ', res
PRINTF, lun, 'mv errcode: ', err

PRINTF, lun, 'Elapsed time: ', SYSTIME(/sec) - tt0, form = '(a30,f10.1)'
PRINTF, lun

FREE_LUN, lun

end
