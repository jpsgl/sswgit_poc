pro IRIS_PREP_TREND_SENSITIVITY, fiddat, wavedat, aiadat, $
	dowarp = dowarp, doshift = doshift, dofix = dofix, preptest = preptest, $
	pstop = pstop, win = win, fidsep = fidsep, png = png

;+
;
;
;
;
;
;-

if KEYWORD_SET(doshift) then begin
	dowarp = 1
	path = '/Volumes/disk2/data/iris/ops/thru/shift' 
	fidname = '_fiducial_1p5x.png'
	wavename = 'wavecorr_1p5x.png'
	aianame = 'aiacorr_1p5x.png'
endif else begin
	if KEYWORD_SET(dowarp) then begin
		dowarp = 1
		path = '/Volumes/disk2/data/iris/ops/thru/warp' 
		fidname = '_fiducial_1p5no.png'
		wavename = 'wavecorr_1p5no.png'
		aianame = 'aiacorr_1p5no.png'
	endif else begin
		if KEYWORD_SET(dofix) then begin
			dowarp = 1
			path = '/Volumes/disk2/data/iris/ops/thru/fix' 
			fidname = '_fiducial_1p5.png'
			wavename = 'wavecorr_1p5.png'
			aianame = 'aiacorr_1p5.png'
		endif else begin
			if KEYWORD_SET(preptest) then begin
				dowarp = 1
				path = '/irisa/data/prep/'
				fidname = '_fiducial_test.png'
				wavename = 'wavecorr_test.png'
				aianame = 'aiacorr_test.png'			
			endif else begin
				dowarp = 0
				path = '/Volumes/disk2/data/iris/ops/thru/nowarp'
				fidname = '_fiducial_1.png'
				wavename = 'wavecorr_1.png'
				aianame = 'aiacorr_1.png'
			endelse
		endelse
	endelse
endelse
CD, path, current = old_dir

if not KEYWORD_SET(win) then win = 10
if not KEYWORD_SET(fidsep) then fidsep = 269.5
TVLCT, rr, gg, bb, /get
oldpmulti = !p.multi
PB_SET_LINE_COLOR

pdb = IRIS_MK_POINTDB()

;+++++++++++++++++++++++++++++++++
; Generating plots of fiducials
;---------------------------------
fidf = FILE_SEARCH('*/fid/*.txt')
numf = N_ELEMENTS(fidf)
for i = 0, numf - 1 do begin
	thisdat = IRIS_PREP_TXT2STR(fidf[i], /fid)
	if i eq 0 then logdat = thisdat else logdat = [logdat,thisdat]
endfor

; Spectograph Y fiducials
!p.multi = [0, 1, 3]
WDEF, win, 1000, 1200

nind = WHERE(logdat.type eq 'NUV', numn)
if dowarp then pdbp = pdb.cpx2_1p5 - 1 else pdbp = pdb.cpx2_nuv - 1
yrange = [-8,8] + pdbp
UTPLOT, chars = 2, logdat[nind].t_obs, logdat[nind].y1+fidsep, psym = 4, /ystyle, $
	yrange = yrange, title = 'NUV Fiducial Y center', /xstyle, ytit = 'IRIS pix'
OUTPLOT, logdat[nind].t_obs, logdat[nind].y2-fidsep, col = 2, psym = 4
OUTPLOT, [logdat[nind[0]].t_obs, logdat[nind[-1]].t_obs], pdbp + [0,0], $
	col = 5, line = 3
LEGEND, pos = 10, chars = 1.5, ['Bottom', 'Top', 'PDB'], psym = [4,4,-3], $
	col = [255,2,5], linesty = [0,0,3]
	
find = WHERE(logdat.type eq 'FUVS', numfs)
if dowarp then pdbp = pdb.cpx2_1p5 - 1 else pdbp = pdb.cpx2_fu1 - 1
yrange = [-8,8] + pdbp
UTPLOT, chars = 2, logdat[find].t_obs, logdat[find].y1+fidsep, psym = 4, /ystyle, $
	yrange = yrange, title = 'FUVS Fiducial Y center', /xstyle, ytit = 'IRIS pix'
OUTPLOT, logdat[find].t_obs, logdat[find].y2-fidsep, col = 2, psym = 4
OUTPLOT, [logdat[find[0]].t_obs, logdat[find[-1]].t_obs], pdbp + [0,0], $
	col = 5, line = 3
LEGEND, pos = 10, chars = 1.5, ['Bottom', 'Top', 'PDB'], psym = [4,4,-3], $
	col = [255,2,5], linesty = [0,0,3]

lind = WHERE(logdat.type eq 'FUVL', numfl)
if dowarp then pdbp = pdb.cpx2_1p5 - 1 else pdbp = pdb.cpx2_fu2 - 1
yrange = [-8,8] + pdbp
UTPLOT, chars = 2, logdat[lind].t_obs, logdat[lind].y1+fidsep, psym = 4, /ystyle, $
	yrange = yrange, title = 'FUVL Fiducial Y center', /xstyle, ytit = 'IRIS pix'
OUTPLOT, logdat[lind].t_obs, logdat[lind].y2-fidsep, col = 2, psym = 4
OUTPLOT, [logdat[lind[0]].t_obs, logdat[lind[-1]].t_obs], pdbp + [0,0], $
	col = 5, line = 3
LEGEND, pos = 10, chars = 1.5, ['Bottom', 'Top', 'PDB'], psym = [4,4,-3], $
	col = [255,2,5], linesty = [0,0,3]

if KEYWORD_SET(png) then PB_WIN2PNG, '../spec_y' + fidname
if KEYWORD_SET(pstop) then STOP

; SJI Y fiducials
!p.multi = [0, 1, 4]
WDEF, win+1, 1000, 1200

s13ind = WHERE(logdat.type eq 'SJI_1330', nums13)
if dowarp then pdbp = pdb.cpx2_1p5 - 1 else pdbp = pdb.cpx2_133 - 1
yrange = [-8,8] + pdbp
UTPLOT, chars = 2, logdat[s13ind].t_obs, logdat[s13ind].y1+fidsep, psym = 4, /ystyle, $
	yrange = yrange, title = 'SJI_1330 Fiducial Y center', /xstyle, ytit = 'IRIS pix'
OUTPLOT, logdat[s13ind].t_obs, logdat[s13ind].y2-fidsep, col = 2, psym = 4
OUTPLOT, [logdat[s13ind[0]].t_obs, logdat[s13ind[-1]].t_obs], pdbp + [0,0], $
	col = 5, line = 3
LEGEND, pos = 10, chars = 1.5, ['Bottom', 'Top', 'PDB'], psym = [4,4,-3], $
	col = [255,2,5], linesty = [0,0,3]
	
s14ind = WHERE(logdat.type eq 'SJI_1400', nums14)
if dowarp then pdbp = pdb.cpx2_1p5 - 1 else pdbp = pdb.cpx2_140 - 1
yrange = [-8,8] + pdbp
UTPLOT, chars = 2, logdat[s14ind].t_obs, logdat[s14ind].y1+fidsep, psym = 4, /ystyle, $
	yrange = yrange, title = 'SJI_1400 Fiducial Y center', /xstyle, ytit = 'IRIS pix'
OUTPLOT, logdat[s14ind].t_obs, logdat[s14ind].y2-fidsep, col = 2, psym = 4
OUTPLOT, [logdat[s14ind[0]].t_obs, logdat[s14ind[-1]].t_obs], pdbp + [0,0], $
	col = 5, line = 3
LEGEND, pos = 10, chars = 1.5, ['Bottom', 'Top', 'PDB'], psym = [4,4,-3], $
	col = [255,2,5], linesty = [0,0,3]
	
s27ind = WHERE(logdat.type eq 'SJI_2796', nums27)
if dowarp then pdbp = pdb.cpx2_1p5 - 1 else pdbp = pdb.cpx2_279 - 1
yrange = [-8,8] + pdbp
UTPLOT, chars = 2, logdat[s27ind].t_obs, logdat[s27ind].y1+fidsep, psym = 4, /ystyle, $
	yrange = yrange, title = 'SJI_2796 Fiducial Y center', /xstyle, ytit = 'IRIS pix'
OUTPLOT, logdat[s27ind].t_obs, logdat[s27ind].y2-fidsep, col = 2, psym = 4
OUTPLOT, [logdat[s27ind[0]].t_obs, logdat[s27ind[-1]].t_obs], pdbp + [0,0], $
	col = 5, line = 3
LEGEND, pos = 10, chars = 1.5, ['Bottom', 'Top', 'PDB'], psym = [4,4,-3], $
	col = [255,2,5], linesty = [0,0,3]
	
s28ind = WHERE(logdat.type eq 'SJI_2832', nums28)
if dowarp then pdbp = pdb.cpx2_1p5 - 1 else pdbp = pdb.cpx2_283 - 1
yrange = [-8,8] + pdbp
UTPLOT, chars = 2, logdat[s28ind].t_obs, logdat[s28ind].y1+fidsep, psym = 4, /ystyle, $
	yrange = yrange, title = 'SJI_2832 Fiducial Y center', /xstyle, ytit = 'IRIS pix'
OUTPLOT, logdat[s28ind].t_obs, logdat[s28ind].y2-fidsep, col = 2, psym = 4
OUTPLOT, [logdat[s28ind[0]].t_obs, logdat[s28ind[-1]].t_obs], pdbp + [0,0], $
	col = 5, line = 3
LEGEND, pos = 10, chars = 1.5, ['Bottom', 'Top', 'PDB'], psym = [4,4,-3], $
	col = [255,2,5], linesty = [0,0,3]

if KEYWORD_SET(png) then PB_WIN2PNG, '../sji_y' + fidname

; SJI X fiducials
!p.multi = [0, 1, 4]
WDEF, win+2, 1000, 1200

s13ind = WHERE(logdat.type eq 'SJI_1330', nums13)
if dowarp then pdbp = pdb.cpx1_1p5 - 1 else pdbp = pdb.cpx1_133 - 1
yrange = [-8,8] + pdbp
UTPLOT, chars = 2, logdat[s13ind].t_obs, logdat[s13ind].x1, psym = 4, /ystyle, $
	yrange = yrange, title = 'SJI_1330 Fiducial X center', /xstyle, ytit = 'IRIS pix'
OUTPLOT, logdat[s13ind].t_obs, logdat[s13ind].x2, col = 2, psym = 4
OUTPLOT, [logdat[s13ind[0]].t_obs, logdat[s13ind[-1]].t_obs], pdbp + [0,0], $
	col = 5, line = 3
LEGEND, pos = 10, chars = 1.5, ['Bottom', 'Top', 'PDB'], psym = [4,4,-3], $
	col = [255,2,5], linesty = [0,0,3]
	
s14ind = WHERE(logdat.type eq 'SJI_1400', nums14)
if dowarp then pdbp = pdb.cpx1_1p5 - 1 else pdbp = pdb.cpx1_140 - 1
yrange = [-8,8] + pdbp
UTPLOT, chars = 2, logdat[s14ind].t_obs, logdat[s14ind].x1, psym = 4, /ystyle, $
	yrange = yrange, title = 'SJI_1400 Fiducial X center', /xstyle, ytit = 'IRIS pix'
OUTPLOT, logdat[s14ind].t_obs, logdat[s14ind].x2, col = 2, psym = 4
OUTPLOT, [logdat[s14ind[0]].t_obs, logdat[s14ind[-1]].t_obs], pdbp + [0,0], $
	col = 5, line = 3
LEGEND, pos = 10, chars = 1.5, ['Bottom', 'Top', 'PDB'], psym = [4,4,-3], $
	col = [255,2,5], linesty = [0,0,3]
	
s27ind = WHERE(logdat.type eq 'SJI_2796', nums27)
if dowarp then pdbp = pdb.cpx1_1p5 - 1 else pdbp = pdb.cpx1_279 - 1
yrange = [-8,8] + pdbp
UTPLOT, chars = 2, logdat[s27ind].t_obs, logdat[s27ind].x1, psym = 4, /ystyle, $
	yrange = yrange, title = 'SJI_2796 Fiducial X center', /xstyle, ytit = 'IRIS pix'
OUTPLOT, logdat[s27ind].t_obs, logdat[s27ind].x2, col = 2, psym = 4
OUTPLOT, [logdat[s27ind[0]].t_obs, logdat[s27ind[-1]].t_obs], pdbp + [0,0], $
	col = 5, line = 3
LEGEND, pos = 10, chars = 1.5, ['Bottom', 'Top', 'PDB'], psym = [4,4,-3], $
	col = [255,2,5], linesty = [0,0,3]
	
s28ind = WHERE(logdat.type eq 'SJI_2832', nums28)
if dowarp then pdbp = pdb.cpx1_1p5 - 1 else pdbp = pdb.cpx1_283 - 1
yrange = [-8,8] + pdbp
UTPLOT, chars = 2, logdat[s28ind].t_obs, logdat[s28ind].x1, psym = 4, /ystyle, $
	yrange = yrange, title = 'SJI_2832 Fiducial X center', /xstyle, ytit = 'IRIS pix'
OUTPLOT, logdat[s28ind].t_obs, logdat[s28ind].x2, col = 2, psym = 4
OUTPLOT, [logdat[s28ind[0]].t_obs, logdat[s28ind[-1]].t_obs], pdbp + [0,0], $
	col = 5, line = 3
LEGEND, pos = 10, chars = 1.5, ['Bottom', 'Top', 'PDB'], psym = [4,4,-3], $
	col = [255,2,5], linesty = [0,0,3]

if KEYWORD_SET(png) then PB_WIN2PNG, '../sji_x' + fidname

fiddat = logdat
if KEYWORD_SET(pstop) then STOP

;+++++++++++++++++++++++++++++++++
; Generating plots of wavelength
; correction
;---------------------------------
wavef = FILE_SEARCH('*/wavecorr/*.txt')
numf = N_ELEMENTS(wavef)
for i = 0, numf - 1 do begin
	thisdat = IRIS_PREP_TXT2STR(wavef[i], /wave)
	if i eq 0 then logdat = thisdat else logdat = [logdat,thisdat]
endfor

!p.multi = [0, 1, 3]
WDEF, win+3, 1000, 1200

nind = WHERE(logdat.img_path eq 'NUV', numn)
yrange = [650,670]
UTPLOT, chars = 2, logdat[nind].t_obs, logdat[nind].line_bot, psym = 4, /ystyle, $
	yrange = yrange, title = 'NUV Neutral Line Wavelength', /xstyle, ytit = 'IRIS pix'
OUTPLOT, logdat[nind].t_obs, logdat[nind].line_mid, col = 2, psym = 4
OUTPLOT, logdat[nind].t_obs, logdat[nind].line_top, col = 6, psym = 4
OUTPLOT, [logdat[nind[0]].t_obs, logdat[nind[-1]].t_obs], pdb.cpx1_nuv - 1 + [0,0], $
	col = 5, line = 3
LEGEND, pos = 10, chars = 1.5, ['Bottom', 'Mid', 'Top', 'PDB'], psym = [4,4,4,-3], $
	col = [255,2,6,5], linesty = [0,0,0,3]
	
find = WHERE(logdat.img_path eq 'FUVS', numf)
yrange = [1820,1860]
UTPLOT, chars = 2, logdat[find].t_obs, logdat[find].line_bot, psym = 4, /ystyle, $
	yrange = yrange, title = 'FUVS Neutral Line Wavelength', /xstyle, ytit = 'IRIS pix'
OUTPLOT, logdat[find].t_obs, logdat[find].line_mid, col = 2, psym = 4
OUTPLOT, logdat[find].t_obs, logdat[find].line_top, col = 6, psym = 4
OUTPLOT, [logdat[find[0]].t_obs, logdat[find[-1]].t_obs], pdb.cpx1_fu1 - 1 + [0,0], $
	col = 5, line = 3
LEGEND, pos = 10, chars = 1.5, ['Bottom', 'Mid', 'Top', 'PDB'], psym = [4,4,4,-3], $
	col = [255,2,6,5], linesty = [0,0,0,3]
	
lind = WHERE(logdat.img_path eq 'FUVL', numl)
yrange = [3005,3045]
UTPLOT, chars = 2, logdat[lind].t_obs, logdat[lind].line_bot, psym = 4, /ystyle, $
	yrange = yrange, title = 'FUVL Neutral Line Wavelength', /xstyle, ytit = 'IRIS pix'
OUTPLOT, logdat[lind].t_obs, logdat[lind].line_mid, col = 2, psym = 4
OUTPLOT, logdat[lind].t_obs, logdat[lind].line_top, col = 6, psym = 4
; Note that the FUVL reference line doesn't show up because we measure the wavelength 
; shift on the Fe II line but have left the Si IV line as the reference wavelength
OUTPLOT, [logdat[lind[0]].t_obs, logdat[lind[-1]].t_obs], pdb.cpx1_fu2 - 1 + [0,0], $
	col = 5, line = 3
LEGEND, pos = 10, chars = 1.5, ['Bottom', 'Mid', 'Top', 'PDB'], psym = [4,4,4,-3], $
	col = [255,2,6,5], linesty = [0,0,0,3]
	
if KEYWORD_SET(png) then PB_WIN2PNG, '../' + wavename

wavedat = logdat
if KEYWORD_SET(pstop) then STOP

;+++++++++++++++++++++++++++++++++
; Generating plots of AIA 
; co-alignment
;---------------------------------
aiaf = FILE_SEARCH('*/aiacorr/*.dat')
numf = N_ELEMENTS(aiaf)
for i = 0, numf - 1 do begin
	thisdat = IRIS_PREP_TXT2STR(aiaf[i], /aia)
	if N_TAGS(thisdat) gt 0 then if i eq 0 then logdat = thisdat else logdat = [logdat,thisdat]
endfor

!p.multi = [0, 1, 2]
WDEF, win+4, 1000, 1000

s14ind = WHERE(logdat.wave_iris eq 'SJI_1400', num14)
yrange = [-5,5]
UTPLOT, chars = 1.5, logdat[s14ind].t_obs, logdat[s14ind].x_off, psym = 4, /ystyle, $
	yrange = yrange, title = 'AIA X offset', /xstyle, ytit = 'Offset [arcsec]'
OUTPLOT, [logdat[s14ind[0]].t_obs, logdat[s14ind[-1]].t_obs], [0,0], $
	col = 5, line = 3
UTPLOT, chars = 1.5, logdat[s14ind].t_obs, logdat[s14ind].y_off, psym = 4, /ystyle, $
	yrange = yrange, title = 'AIA Y offset', /xstyle, ytit = 'Offset [arcsec]'
OUTPLOT, [logdat[s14ind[0]].t_obs, logdat[s14ind[-1]].t_obs], [0,0], $
	col = 5, line = 3

n14ind = WHERE(logdat.wave_iris ne 'SJI_1400', numno)
if KEYWORD_SET(png) then PB_WIN2PNG, '../' + aianame

aiadat = logdat
if KEYWORD_SET(pstop) then STOP

;+++++++++++++++++++++++++++++++++
; Finish up and exit
;---------------------------------

CD, old_dir
TVLCT, rr, gg, bb
!p.multi = oldpmulti

end
