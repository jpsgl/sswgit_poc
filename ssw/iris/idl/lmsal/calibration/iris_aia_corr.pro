function iris_aia_corr, index_iris, data_iris, $
   ds_sdo=ds_sdo, do_iris_prep=do_iris_prep, max_t_diff_sec=max_t_diff_sec, $  
   sat_rot=sat_rot, status=status, z=z, $
   wave_sdo_arr=wave_sdo_arr, ds_sdo_fin_arr=ds_sdo_fin_arr, use_lev1p5=use_lev1p5, $
   do_sdo_prep=do_sdo_prep, $
   do_xstep=do_xstep, do_plot_map=do_plot_map, $
   xdisp=xdisp, ydisp=ydisp, suffix=suffix, xcen=xcen, ycen=ycen, $
   do_surf=do_surf, peak_str=peak_str, $
   do_log_file=do_log_file, logdir=logdir, $
   verbose=verbose, short=short, $
   nocrop2disk=nocrop2disk, $
   pstop=pstop, _extra=_extra

;+
; NAME:
;	iris_aia_corr
; PURPOSE:
;	Cross correlate IRIS SJI with FOV and time-matched SDO image of appropriate wavelength
;	to determine translational offsets. Log the offsets.
; CATEGORY:
;	Image registration
; CALL:
;	iris_aia_corr, index_iris, data_iris
; OUTPUTS:
;	None;
; RETURNS:
;	iris_align_struct - structure contain various parameters related to the IRIS and AIA
;						images and the cross correlation results:
;		date_obs_iris
;		date_obs_sdo
;		file_sdo
;		x_off
;		y_off
;		crval1_new
;		crval2_new
;		wave_iris
;		wave_sdo
;		raster_xsize
;		raster_ysize
;		sat_rot
;		roll_status_iris
;		roll_discrep_iris
;		peak_str
;
;	xdisp, ydisp - x and y offsets in arcsec
; KEYWORDS:
;	ds_sdo
;	do_iris_prep
;	max_t_diff_sec
;	sat_rot
;	status
;	z
;	wave_sdo_arr
;	ds_sdo_fin_arr
;	use_lev1p5
;   do_sdo_prep
;	do_xstep
;	do_plot_map
;	xdisp
;	ydisp
;	suffix
;	xcen
;	ycen
;	do_surf
;	peak_str
;	do_log_file
;	logdir
;	verbose
;	short
;	_extra
;
; HISTORY:
;	2013-10-09 - GLS
;       2013-11-?? - PB edits
;       2013-11-21 - JPW : - added logic to correct for AIA CROTA2
;                          - removed loop around interpolate
;                          - changed logic to rotate xdisp with PC matrix
;                            and to properly add it to the old CRVALi
;		2013-12-10 - PB : - added some rough error-checking to roll over
;							most pipeline glitches
;       2014-01-13 - JPW : - now uses an on-disk subfield for images at the limb
;                            turn off with /nocrop2disk
;                          - also fixed xcen,ycen and removed scale stuff and
;                            some other unused code
;
; TODO:
;	 Handle roll better:
;		- vectorize update
;		- determine residual roll error vis Ted method
;-

common iris_sdo_common, t_last_sdo_drms, t_first_sdo_drms, drms_1700, drms_cont, $
					files_1700, files_cont, date_1700, date_cont, rdisk, is_lev1

; Initialize the common block, if necessary:
if not exist(t_last_sdo_drms) then begin
	t_last_sdo_drms = replicate(anytim('01-jan-2013', /ccsds), 4)
	t_first_sdo_drms = replicate(anytim('01-jan-2033', /ccsds), 4)
	is_lev1 = bytarr(4)
	DIST_CIRCLE, rdisk_pix, 4096
	rdisk = rdisk_pix * 0.6
	rdisk_pix = 0.
endif

if not exist(days_lag_final_sdo) then days_lag_final_sdo = 14
if not exist(days_1p5_cache_sdo) then days_1p5_cache_sdo = 28
if not exist(max_t_diff_sec) then max_t_diff_sec = 60
if not exist(peak_str_min) then peak_str_min = 3 ;6 ;10.0
if not exist(use_lev1p5) then use_lev1p5 = 1
if not exist(do_sdo_prep) then do_sdo_prep = 1
if not exist(do_xstep) then do_xstep = 0
if not KEYWORD_SET(verbose) then quiet = 1 else quiet = 0
if exist(logdir) then set_logenv, 'IRIS_LOGDIR', logdir, /quiet
if get_logenv('IRIS_LOGDIR') eq '' then $
	set_logenv, 'IRIS_LOGDIR', concat_dir(get_logenv('HOME'), 'logs/iris')

date_iris = time2file(index_iris.date_obs, /date)

if not exist(iris_aia_corr_log_file) then $
   iris_aia_corr_log_file = concat_dir(get_logenv('IRIS_LOGDIR'), $
							'iris_aia_corr_' + date_iris + '.log')
if not exist(iris_aia_corr_data_file) then $
   iris_aia_corr_data_file = concat_dir(get_logenv('IRIS_LOGDIR'), $
							'iris_aia_corr_' + date_iris + '.dat')

if not exist(wave_iris_arr) then $
	wave_iris_arr	= ['sji_1400', 'sji_2832', 'sji_1400', 'sji_1400']
if not exist(wave_sdo_arr) then $
	wave_sdo_arr	= [	   '1700',	   'cont',	   '1700',	   '1700']

	sdo_instr		= [		'aia',		'hmi',		'aia',		'aia']

;iris_max_count_arr = [		10000,		10000,		10000,		   75]
 iris_max_count_arr = [		 4000,		  200,		 4000,		 4000]
 iris_min_count_arr = [		 -300,			0,		 -300,		 -300]
;sdo_max_count_arr	= [		40000,		40000,		40000,		 5000]
;sdo_max_count_arr	= [		 5000,		40000,		 5000,		 5000]
 sdo_max_count_arr	= [		 3000,		70000,		 3000,		 3000]
 sdo_min_count_arr	= [			0,		50000,			0,			0]

if not exist(ds_sdo_nrt_arr) then ds_sdo_nrt_arr  = $
   ['aia.lev1_nrt2',	  'hmi.Ic_45s_nrt',		'aia.lev1_nrt2',	  'aia.lev1_nrt2'	  ]
if not exist(ds_sdo_fin_arr) then ds_sdo_fin_arr  = $
   ['aia.lev1_uv_24s',	  'hmi.Ic_45s',			'aia.lev1_uv_24s',	  'aia.lev1_uv_24s'	  ]

status = 1
wave_iris = index_iris.img_path
sat_rot = index_iris.sat_rot
sunpb0r = pb0r(index_iris.date_obs)
rsun = sunpb0r[2]*60.
offlimb = where(rdisk GT rsun, complement=ondisk)

ss_match_wave_iris = (where(strlowcase(wave_iris) eq wave_iris_arr, n_match_wave_iris))[0]
if n_match_wave_iris ne 0 then begin
	wave_sdo = wave_sdo_arr[ss_match_wave_iris]
	ds_sdo_nrt  = ds_sdo_nrt_arr[ss_match_wave_iris]
	ds_sdo_fin  = ds_sdo_fin_arr[ss_match_wave_iris]
endif else begin
	print, ' Requested IRIS wavelength not recognized.  Returning.'
	status = 0
	return, 0
endelse

; Update the common block, if necessary:
				;	Time range of cached SDO images ends before IRIS image
need_update = ( ( anytim(t_last_sdo_drms[ss_match_wave_iris]) lt (anytim(index_iris.t_obs) + 60l) ) or $
				;	Time range of cached SDO images starts after IRIS image
				( anytim(t_first_sdo_drms[ss_match_wave_iris]) gt (anytim(index_iris.t_obs) - 60l) ) or $				
				;	Using SJI_1400 and cache doesn't have any AIA 1700s
				( (strlowcase(index_iris.img_path) eq 'sji_1400') and (not exist(date_1700)) ) or $
				;	Using SJI_2832 and cache doesn't have any HMI continuum
				( (strlowcase(index_iris.img_path) eq 'sji_2832') and (not exist(date_cont)) ) )

if need_update then begin
	if keyword_set(verbose) then print, ' Updating COMMON block variables.'

	t0_search_sdo = anytim(anytim(index_iris.t_obs) - 60l, /ccsds)
	t1_search_sdo = anytim(anytim(index_iris.t_obs) + 3600l - 60l, /ccsds)

	; Decide which SDO data series to use:
	t_lag_iris_sec = anytim(anytim(ut_time(), /ccsds)) - anytim(index_iris.date_obs)
	if t_lag_iris_sec lt days_lag_final_sdo*86400l then begin
		ds_sdo = ds_sdo_nrt 
	endif else begin
		ds_sdo = ds_sdo_fin
	endelse
	if ( (strlowcase(strmid(ds_sdo,0,3)) eq 'hmi') and (not keyword_set(use_lev1p5)) ) then $
		use_index = 1 else use_index = 0

	; If you are using lev1p5 data and are within the time window cached locally, then
	; read fits headers; otherwise, query for DRMS records
	is_recent = (t_lag_iris_sec lt days_1p5_cache_sdo*86400l)
	if ( (keyword_set(use_lev1p5)) and is_recent ) then begin
		if keyword_set(verbose) then print, ' Using level 1.5 from cache.'
		; Full call for reference:
		;	   files = sdo_time2files(t0_search_sdo, t1_search_sdo, level=level, $
		;							  waves=wave_sdo, aia=aia, hmi=hmi, mag=mag, $
		;							  pre_ops=pre_ops, fast=fast, refresh=refresh, $
		;							  parent=parent, _extra=_extra
		files_sdo = sdo_time2files(t0_search_sdo, t1_search_sdo, wave=strtrim(wave_sdo,2), $
			level=1.5, /fast, quiet=quiet)
		; If no files found, then log and return
		if not exist(files_sdo) then begin
			buff = anytim(index_iris.date_obs, /ccsds) + $
				' : No appropriate SDO files found in . Returning without correlation.'
			if keyword_set(do_log_file) then file_append, iris_aia_corr_log_file, buff
			status = 0
		 	return, 0
		endif
		date_sdo = file2time(files_sdo)
		is_lev1[ss_match_wave_iris] = 0
	endif else begin
		; If HMI lev1 is found, then log and return
		if strupcase(strmid(ds_sdo, 0, 3)) eq 'HMI' then begin
			buff = anytim(index_iris.date_obs, /ccsds) + $
				' : HMI lev1 data not available with full header'
			if keyword_set(do_log_file) then file_append, iris_aia_corr_log_file, buff
			status = 0
			return, 0
		endif
		; Define subset of tags and call ssw_jsoc_time2data:
		keys = 'wavelnth,t_obs,date__obs,crpix1,crpix2,instrume'
		ssw_jsoc_time2data, t0_search_sdo, t1_search_sdo, ds=ds_sdo, wave=wave_sdo, $
			drms_sdo, files_sdo, /files_only, silent=quiet, key=keys
		; If no drms structure returned, then log and return
		if not exist(files_sdo) then begin
			buff = anytim(index_iris.date_obs, /ccsds) + $
				' : Could not read JSOC DRMS. Returning without correlation.'
			if keyword_set(do_log_file) then file_append, iris_aia_corr_log_file, buff
			status = 0
			return, 0
		endif
		; If no drms records found, then log and return
		if size(drms_sdo, /type) ne 8 then begin
			buff = anytim(index_iris.date_obs, /ccsds) + $
				' : No SDO JSOC DRMS found. Returning without correlation.'
			if keyword_set(do_log_file) then file_append, iris_aia_corr_log_file, buff
			status = 0
			return, 0
		endif
		date_sdo = drms_sdo.date__obs
		is_lev1[ss_match_wave_iris] = 1
	endelse

	;ss_exist_sdo = file_exist(files_sdo)
	ss_exist_sdo = bytarr(n_elements(files_sdo)) + 1b

	; If no files found for drms records, then log and return
	if max(ss_exist_sdo) eq 0 then begin
		buff = anytim(index_iris.date_obs, /ccsds) + $
			' : SDO Files for drms records not found. Returning without correlation.'
		if keyword_set(do_log_file) then file_append, iris_aia_corr_log_file, buff
		status = 0
		return, 0
	endif

	ss_exist_sdo = where( ((ss_exist_sdo eq 1) and (files_sdo ne '/')), n_exist_sdo)
	files_sdo = files_sdo[ss_exist_sdo]
	n_rec_sdo = n_elements(files_sdo)
	if exist(drms_sdo) then begin
		drms_sdo = drms_sdo[ss_exist_sdo]
	endif
	
	wave_sdo_arr	 = ['1700',		'cont',		'1700',		'1700'	  ]
	case ss_match_wave_iris of
		0: begin
			if is_lev1[ss_match_wave_iris] eq 1 then drms_1700 = drms_sdo
			;drms_1700 = drms_sdo
			files_1700 = files_sdo
			date_1700 = date_sdo
		end
		1: begin
			if is_lev1[ss_match_wave_iris] eq 1 then drms_cont = drms_sdo
			;drms_cont = drms_sdo
			files_cont = files_sdo
			date_cont = date_sdo
		end
		2: begin
			if is_lev1[ss_match_wave_iris] eq 1 then drms_1700 = drms_sdo
			;drms_1700 = drms_sdo
			files_1700 = files_sdo
			date_1700 = date_sdo
		end
		3: begin
			if is_lev1[ss_match_wave_iris] eq 1 then drms_1700 = drms_sdo
			;drms_1700 = drms_sdo
			files_1700 = files_sdo
			date_1700 = date_sdo
		end
		else: begin
			if is_lev1[ss_match_wave_iris] eq 1 then drms_1700 = drms_sdo
			;drms_1700 = drms_sdo
			files_1700 = files_sdo
			date_1700 = date_sdo
		end
	endcase

	t_last_sdo_drms[ss_match_wave_iris] = date_sdo[n_rec_sdo-1]
	t_first_sdo_drms[ss_match_wave_iris] = date_sdo[0]

endif else begin	;	Update_common

	if not exist(files_sdo) then begin
		case ss_match_wave_iris of
			0: begin
				if is_lev1[ss_match_wave_iris] eq 1 then drms_sdo = drms_1700
				;drms_sdo = drms_1700
				files_sdo = files_1700
				date_sdo = date_1700
			end
			1: begin
				if is_lev1[ss_match_wave_iris] eq 1 then drms_sdo = drms_cont
				;drms_sdo = drms_cont
				files_sdo = files_cont
				date_sdo = date_cont
			end
			2: begin
				if is_lev1[ss_match_wave_iris] eq 1 then drms_sdo = drms_1700
				;drms_sdo = drms_1700
				files_sdo = files_1700
				date_sdo = date_1700
			end
			3: begin
				if is_lev1[ss_match_wave_iris] eq 1 then drms_sdo = drms_1700
				;drms_sdo = drms_1700
				files_sdo = files_1700
				date_sdo = date_1700
			end
			else: begin
				if is_lev1[ss_match_wave_iris] eq 1 then drms_sdo = drms_1700
				;drms_sdo = drms_1700
				files_sdo = files_1700
				date_sdo = date_1700
			end
	   endcase
	endif else PRINT, 'iris_aia_corr: hmmm...'

endelse				;	Update_common

if KEYWORD_SET(pstop) then STOP

; Select closest SDO DRMS match to IRIS image:
ss_sdo_closest = tim2dset(anytim(date_sdo,/ints), anytim(index_iris.date_obs,/ints))
t_diff_sec = abs(anytim(date_sdo[ss_sdo_closest]) - anytim(index_iris.date_obs))

; If no sdo images found close enough to any iris images, then log and return
if t_diff_sec gt max_t_diff_sec then begin
   buff = anytim(index_iris.date_obs, /ccsds) + $
		  ' : No SDO images close enough in time to IRIS image. Returning without correlation.'
   if keyword_set(do_log_file) then file_append, iris_aia_corr_log_file, buff
   status = 0
   return, 0
endif

if exist(drms_sdo) then drms_sdo = drms_sdo[ss_sdo_closest]
files_sdo = files_sdo[ss_sdo_closest]

if keyword_set(verbose) then print, 'SDO file: ' + files_sdo[0]
if keyword_set(use_index) then index_sdo = drms_sdo
read_sdo, files_sdo, index_sdo, data_sdo, /use_shared, /silent, /uncomp_delete, $
		  use_index=use_index

; kluge for mysterious dropouts
if not exist(index_sdo) then begin
	print, 'hmmm...lets try that again'
	wait, 100
	read_sdo, files_sdo, index_sdo, data_sdo, /use_shared, /silent, /uncomp_delete, $
		  use_index=use_index
	if not exist(index_sdo) then STOP
endif

index_iris0 = index_iris
data_iris0 = data_iris
index_sdo0 = index_sdo
data_sdo0 = data_sdo

; Optionally prep IRIS image:
if keyword_set(do_iris_prep) then begin
	iris_prep, index_iris0, data_iris0, indexp_iris0, datap_iris0, /silent
endif else begin
	indexp_iris0 = index_iris0
	datap_iris0 = data_iris0
endelse

; Define size and FOV of IRIS image:
tsc1 = indexp_iris0.tsc1
tec1 = indexp_iris0.tec1
tsr1 = indexp_iris0.tsr1
ter1 = indexp_iris0.ter1
iNx = tec1-tsc1+1
iNy = ter1-tsr1+1

; Optionally prep SDO image:
if ( keyword_set(do_sdo_prep) and is_lev1[ss_match_wave_iris[0]] ) then begin
	aia_prep, index_sdo0, data_sdo0, indexp_sdo0, datap_sdo0, /silent
endif else begin
	indexp_sdo0 = index_sdo0
	datap_sdo0 = data_sdo0
endelse

; Define Helioprojective (arcsec) coordinates of pixels in read-out CCD region
solx = fltarr(iNx,iNy)
soly = fltarr(iNx,iNy)

for i=0,iNx-1 do begin
	for j=0,iNy-1 do begin
		solx[i,j] = (tsc1 + i - indexp_iris0.crpix1)*indexp_iris0.pc1_1 + $
					(tsr1 + j - indexp_iris0.crpix2)*indexp_iris0.pc1_2
		soly[i,j] = (tsc1 + i - indexp_iris0.crpix1)*indexp_iris0.pc2_1 + $
					(tsr1 + j - indexp_iris0.crpix2)*indexp_iris0.pc2_2
	endfor
endfor

solx *= indexp_iris0.cdelt1
soly *= indexp_iris0.cdelt2
solx += indexp_iris0.crval1
soly += indexp_iris0.crval2

; check if limb crosses IRIS cutout field and crop to on-disk portion
if ~keyword_set(nocrop2disk) then begin
  fovmar = 5.0
  fovmin = 20.0
  mx_solr = indexp_iris0.rsun_obs - fovmar                               ; 5 arcsec margin
  solr = sqrt(solx*solx+soly*soly)
  solr_corn = solr[[[0,iNx-1],[0,iNx-1]],[[0,0],[iNy-1,iNy-1]]]          ; corners of IRIS image
  if min(solr_corn) lt mx_solr and max(solr_corn) gt mx_solr then begin
    wcorn = where(solr_corn eq min(solr_corn))                           ; corner closest to suncenter "cctsc"
    cornx = wcorn[0] mod 2
    corny = wcorn[0] / 2
    ; find points where solar limb (minus margin) crosses edges of image
    ; edges adjacent to cctsc (only need nxe1,nye1 = # of edge pixels inside limb)
    dmy = where(solr[*,corny*(iNy-1)] lt mx_solr,nxe1)
    dmy = where(solr[cornx*(iNx-1),*] lt mx_solr,nye1)
    ; edges not adjacent to cctsc
    dmy = where(solr[*,(1-corny)*(iNy-1)] lt mx_solr,nxe2)
    dmy = where(solr[(1-cornx)*(iNx-1),*] lt mx_solr,nye2)
    ; Use limb crossing pts to determine optimum subfield size.
    ; Assuming straight limb for this calculation, which provides
    ; a somewhat conservative result (draw 4 cases to derive eq's)
    case 1 of                                  ; 4 cases
      (nxe1 eq inx) and (nye1 eq iny): begin   ; crossing only non-adjacent edges
        inx1 = nxe2 > fix((float(inx)+float(nye2)*float(inx-nxe2)/float(iny-nye2))/2.0) < inx
        iny1 = nye2 > fix((float(iny)+float(nxe2)*float(iny-nye2)/float(inx-nxe2))/2.0) < iny
        end
      (nxe1 eq inx) and (nye1 lt iny): begin   ; crossing both y edges
        inx1 = fix(float(nye1)*float(inx)/float(2*(nye1-nye2) > nye1))
        iny1 = nye1/2 > nye2
        end
      (nxe1 lt inx) and (nye1 eq iny): begin   ; crossing both x edges
        inx1 = nxe1/2 > nxe2
        iny1 = fix(float(nxe1)*float(iny)/float(2*(nxe1-nxe2) > nxe1))
        end
      else: begin                              ; crossing only adjacent edges
        inx1 = nxe1/2
        iny1 = nye1/2
        end
    endcase
    ; update IRIS image cutout if new subimage large enough (i.e., at least 20 " on the side)
    iFOVx1 = inx1*indexp_iris0.cdelt1
    iFOVy1 = iny1*indexp_iris0.cdelt2
    if iFOVx1 ge fovmin and iFOVy1 ge fovmin then begin
      otsc1 = cornx * (iNx-inx1)
      otsr1 = corny * (iNy-iny1)
      tsc1 = tsc1+otsc1
      tec1 = tsc1+inx1-1
      tsr1 = tsr1+otsr1
      ter1 = tsr1+iny1-1
      iNx = inx1
      iNy = iny1
      solx = solx[otsc1:otsc1+inx1-1,otsr1:otsr1+iny1-1]
      soly = soly[otsc1:otsc1+inx1-1,otsr1:otsr1+iny1-1]
    endif
  endif
endif

; Define IRIS cutout:
cutout_iris = datap_iris0[tsc1-1:tec1-1,tsr1-1:ter1-1] / indexp_iris0.exptime

; Effectively xcen and ycen of center of FOV:
xcen = ( ((tsc1 + tec1 )*0.5 - indexp_iris0.crpix1) * indexp_iris0.pc1_1  $
        +((tsr1 + ter1 )*0.5 - indexp_iris0.crpix2) * indexp_iris0.pc1_2) $
       * indexp_iris0.cdelt1 + indexp_iris0.crval1

ycen = ( ((tsc1 + tec1 )*0.5 - indexp_iris0.crpix1) * indexp_iris0.pc2_1  $
        +((tsr1 + ter1 )*0.5 - indexp_iris0.crpix2) * indexp_iris0.pc2_2) $
       * indexp_iris0.cdelt2 + indexp_iris0.crval2

; Rotate solx/soly to the AIA clocking
solx_sdo =  solx*cos(indexp_sdo0.crota2/!radeg) + soly*sin(indexp_sdo0.crota2/!radeg)
soly_sdo = -solx*sin(indexp_sdo0.crota2/!radeg) + soly*cos(indexp_sdo0.crota2/!radeg)

; Translate xy coordinate of each IRIS pixel to SDO pixel coords
solx_sdo = solx_sdo/indexp_sdo0.cdelt1 + indexp_sdo0.crpix1 - 1 
soly_sdo = soly_sdo/indexp_sdo0.cdelt2 + indexp_sdo0.crpix2 - 1

; Now create an SDO cutout for this SJI image using cubic interpolation of original full frame SDO image
cutout_sdo = interpolate(datap_sdo0, solx_sdo, soly_sdo, cubic=-0.5)

; Crop SDO counts:
;	   cutout_sdo = cutout_sdo<(sdo_max_count_arr[ss_match_wave_iris[0]])
cutout_sdo_cropped = $
	cutout_sdo>(sdo_min_count_arr[ss_match_wave_iris[0]])<(sdo_max_count_arr[ss_match_wave_iris[0]])

; Handle NaN's:
ss_fin = where(finite(cutout_iris) eq 1, n_fin, comp=ss_inf, ncomp=n_inf)
if n_inf gt 0 then cutout_iris[ss_inf] = min(cutout_iris[ss_fin])

; Crop IRIS counts:
;	   cutout_iris = cutout_iris<(iris_max_count_arr[ss_match_wave_iris[0]])
;	   cutout_iris = cutout_iris>(-600)<4000
cutout_iris_cropped = $
	temporary(cutout_iris>(iris_min_count_arr[ss_match_wave_iris[0]])<(iris_max_count_arr[ss_match_wave_iris[0]]))

; Create SDO/IRIS cutout cube and cross correlate to get offsets:
;	   cutout_cube = [[[cutout_sdo]],[[cutout_iris]]]
cutout_cube = [[[cutout_sdo_cropped]],[[cutout_iris_cropped]]]

disp = tr_get_disp(cutout_cube, /shift, do_surf=do_surf, peak_str=peak_str, _extra=_extra)

xdisp = disp[0,1]*indexp_iris0.cdelt1
ydisp = disp[1,1]*indexp_iris0.cdelt2

wcs0 = fitshead2wcs(indexp_iris0)
wcs_decomp_angle, wcs0, roll_angle0, cdelt0, found
	  
if found eq 1 then begin
	roll_discrep_iris = sat_rot - roll_angle0
	roll_status_iris = strtrim(roll_discrep_iris,2)
endif else begin
	roll_discrep_iris = !values.f_nan
	roll_status_iris = 'WCS_DECOMP_ROLL failed. SAT_ROT discrepancy unknown.' 
endelse
if keyword_set(verbose) then print, roll_status_iris

; Now update IRIS CRVALs with (rotated) translation offsets from AIA image correlation:
xdisp_rot = xdisp*indexp_iris0.pc1_1 + ydisp*indexp_iris0.pc1_2
ydisp_rot = xdisp*indexp_iris0.pc2_1 + ydisp*indexp_iris0.pc2_2

crval1_iris_cor = indexp_iris0.crval1 + xdisp_rot
crval2_iris_cor = indexp_iris0.crval2 + ydisp_rot

indexp_iris0.crval1 = crval1_iris_cor
indexp_iris0.crval2 = crval2_iris_cor

; Add CROTA2, XCEN, YCEN, tags to IRIS header for use in map object generation:
if tag_exist(indexp_iris0, 'crota2') then begin
	indexp_iris0.crota2 = sat_rot 
endif else begin
	indexp_iris0 = add_tag(indexp_iris0, sat_rot, 'crota2')
endelse
;	   if not tag_exist(indexp_iris0, 'xcen') then $
;		  indexp_iris0 = add_tag(indexp_iris0, indexp_iris0.crval1, 'xcen')
;	   if not tag_exist(indexp_iris0, 'ycen') then $
;		  indexp_iris0 = add_tag(indexp_iris0, indexp_iris0.crval2, 'ycen')

; Update IRIS PC matrix tags:
; ( CODE HERE )

if ( keyword_set(do_plot_map) and (peak_str ge peak_str_min) ) then begin
;		  wcs_index2map, wcs_iris0, bytscl((cutout_iris>0)<400), map_iris0
	index2map, indexp_iris0, datap_iris0>0<400, mapp_iris0
	plot_map, mapp_iris0, center=[indexp_iris0.crval1, indexp_iris0.crval2], $
		fov=[02,02], grid=5, /limb, charsize=2
	img_iris = tvrd()
	index2map, indexp_sdo0, datap_sdo0<2000, mapp_sdo0
	plot_map, mapp_sdo0, center=[indexp_iris0.crval1, indexp_iris0.crval2], $
		fov=[02,02], grid=5, /limb, charsize=2
	img_sdo = tvrd()
	img_cube = [[[img_iris]],[[img_sdo]]]
	xstepper, img_cube
endif

; Generate output structure
iris_align_struct = { date_obs_iris: indexp_iris0.date_obs, $
					 date_obs_sdo: indexp_sdo0.date_obs, $
					 file_sdo: files_sdo, $
					 x_off: xdisp, $
					 y_off: ydisp, $
					 crval1_new: crval1_iris_cor, $
					 crval2_new: crval2_iris_cor, $
					 wave_iris: indexp_iris0.img_path, $
					 wave_sdo: indexp_sdo0.wavelnth, $
					 raster_xsize: iNx*indexp_iris0.cdelt1, $
					 raster_ysize: iNy*indexp_iris0.cdelt2, $
					 sat_rot: sat_rot, $
					 roll_status_iris: roll_status_iris, $
					 roll_discrep_iris: roll_discrep_iris, $
					 peak_str: peak_str }

if KEYWORD_SET(short) then begin
	buff_log = STRING(iris_align_struct, RELTIME(/now, out='CCSDS'), $
		format = '(2a25,a70,2f9.4,2f11.4,a10,i7,6f10.4,a25)')
endif else begin
	buff_log = string(indexp_iris0.t_obs, '$(a24)') + '	 ' + $
		'x_off: ' + string(xdisp, '$(f7.2)')		  + '	 ' + $
		'y_off: ' + string(ydisp, '$(f7.2)')		  + '	 ' + $
		'crval1_new: '	 + string(crval1_iris_cor,	'$(f8.2)')		   + '	  ' + $
		'crval2_new: '	 + string(crval2_iris_cor,	'$(f8.2)')		   + '	  ' + $
		'wave_iris: '	+ strtrim(indexp_iris0.img_path,2) + '	  ' + $
		'raster size: ' + string(iNx*indexp_iris0.cdelt1, '$(f8.2)') + '	 ' + $
		' by ' + string(iNy*indexp_iris0.cdelt2, '$(f8.2)') + '	 ' + $
		'sat_rot: ' + string(sat_rot, '$(f7.2)')		 + '	' + $
		'roll_discrep_iris: ' + string(sat_rot, '$(f7.2)')		   + '	  ' + $
		'peak_str: ' + string(peak_str, '$(f7.2)')
endelse

if keyword_set(verbose) then print, buff_log
if keyword_set(verbose) then help, iris_align_struct
if keyword_set(do_log_file) then file_append, iris_aia_corr_data_file, buff_log
if keyword_set(do_xstep) then xstepper, cutout_cube

RETURN, iris_align_struct

end
