function IRIS_PREP_TXT2STR, filename, $
	fid = fid, wavecorr = wavecorr, dose = dose, ccdstat = ccdstat, $
	aiacorr = aiacorr, old_aiacorr = old_aiacorr, $
	template = template, pstop = pstop, txt = txt, dedup = dedup

;+
;
; Reads an iris_prep .txt logfile and returns a structure array
;
;-

if not FILE_EXIST(filename[0]) then begin
	BOX_MESSAGE, ['IRIS_PREP_TXT2STR : ', 'No file found!', filename]
	RETURN, ''
endif

padhdr = 0
case 1 of 
	KEYWORD_SET(fid)	:	begin
		ncol = 17
		colform = 'SISIFFFFFFFFFFFFS'
		thdrnames = ['T_OBS', 'FSN', 'TYPE', 'SUMSPAT', 'X1', 'Y1', $
			'X2', 'Y2', 'CHISQ_X1', 'CHISQ_Y1', 'CHISQ_X2', 'CHISQ_Y2', $
			'DIST', 'ANG', 'SUMSPTRL', 'EXPTIME', 'WRITTEN']
		dedup_ind = 2
	end
	KEYWORD_SET(wavecorr)	:	begin
		ncol = 15
		colform = 'SISFFIFFFFFFFFS'
		thdrnames = ['T_OBS', 'FSN', 'IMG_PATH', 'ROLL', 'EXPTIME', 'OBS_VR', $
			'SUMSPTRL', 'SCVELO_PIX', 'LINE_BOT', 'LINE_MID', 'LINE_TOP', $
			'CHISQ_BOT', 'CHISQ_MID', 'CHISQ_TOP', 'WRITTEN']
		dedup_ind = 2
	end
	KEYWORD_SET(dose)	:	begin
		ncol = 5
		colform = 'SISFS'
		thdrnames = ['T_OBS', 'FSN', 'IMG_PATH', 'EXPTIME', 'WRITTEN']
		dedup_ind = 2
	end
	KEYWORD_SET(ccdstat)	:	begin
		ncol = 18
		colform = 'SISFFFFFFFFFFFFFFS'
		thdrnames = ['T_OBS', 'FSN', 'IMG_PATH', 'EXPTIME', 'INT_TIME', $
			'BLS1_MEAN', 'BLS1_STDEV', 'BLS2_MEAN', 'BLS2_STDEV', $
			'BLS3_MEAN', 'BLS3_STDEV', 'BLS4_MEAN', 'BLS4_STDEV', $
			'OSCAN1_MEAN', 'OSCAN1_STDEV', 'OSCAN2_MEAN', 'OSCAN2_STDEV', 'WRITTEN']
		dedup_ind = 2
	end
	KEYWORD_SET(old_aiacorr)	:	begin
		ncol = 21
		colform = 'SSFSFSFSFSSSSFSFSFSFS'
		hdrnames = ['T_OBS', 'd1', 'X_OFF', 'd2', 'Y_OFF', 'd3', 'CRVAL1N', $
			'd4', 'CRVAL2N', 'd5', 'IMG_PATH', 'd6', 'd7', 'Raster1', 'd8', $
			'Raster2', 'd9', 'Roll', 'd10', 'peak_str', 'WRITTEN']
		dedup_ind = 10	;	img_path
	end
	KEYWORD_SET(aiacorr)	:	begin
		ncol = 16
		colform = 'SSSFFFFSIFFFFFFS'
		hdrnames = ['T_OBS', 'DATE_OBS_SDO', 'FILE_SDO', 'X_OFF', 'Y_OFF', $
			'CRVAL1_NEW', 'CRVAL2_NEW', 'WAVE_IRIS', 'WAVE_SDO', 'RASTER_XSIZE', $
			'RASTER_YSIZE', 'SAT_ROT', 'ROLL_STATUS_IRIS', 'ROLL_DISCREP_IRIS', $
			'PEAK_STR', 'WRITTEN']
		padhdr = 1
		dedup_ind = 7	;	wave_iris
	end
	else	:	begin
		PRINT, 'iris_prep_txt2str: wtf?'	
		RETURN, 0
	end
endcase

if KEYWORD_SET(pstop) then STOP

; Read the specified text file
if KEYWORD_SET(template) then begin
	hdrnames = thdrnames
endif else begin
	txt = RD_TFILE(filename[0], ncol)
	if padhdr eq 1 then begin	;	The AIACORR files don't have a header row; add a dummy
		txt = [[hdrnames], [txt]]
	endif 
	nrow = N_ELEMENTS(txt) / ncol
	if nrow le 1 then RETURN, 0
	; Check for badly formatted files
	if txt[1,1] eq '' then begin
		txt = txt[*,INDGEN(nrow/2)*2]
		nrow = nrow / 2
	endif
	if N_ELEMENTS(hdrnames) eq 0 then hdrnames = txt[*,0]
	badhdr = WHERE(hdrnames eq '', numbad)
	if numbad gt 0 then for i = 0, numbad - 1 do hdrnames[badhdr[i]] = 'Dummy_' + STRTRIM(i+1,2)
endelse

; Define structure template
for i = 0, ncol - 1 do begin
	colchar = STRMID(colform, i, 1)
	case colchar of
		'S'	:	null = ''
		'I'	:	null = 0l
		'F'	:	null = 0.
		'D'	:	null = 0d
		else:	null = 0d
	endcase
	if i eq 0 then strtemplate = CREATE_STRUCT(hdrnames[i], null) else $
		strtemplate = CREATE_STRUCT(strtemplate, hdrnames[i], null)
endfor
if KEYWORD_SET(template) then RETURN, strtemplate

; Go through each row in the text file make it into a structure
result = REPLICATE(strtemplate, nrow-1)
for i = 1, nrow - 1 do $
	for j = 0, ncol - 1 do $
		result[i-1].(j) = txt[j,i]

; If multiple filenames were passed in, concatenate them
numf = N_ELEMENTS(filename)
if numf gt 1 then for k = 1, numf-1 do begin
	txt = RD_TFILE(filename[k], ncol)
	if padhdr eq 1 then begin	;	The AIACORR files don't have a header row; add a dummy
		txt = [[hdrnames], [txt]]
	endif 
	nrow = N_ELEMENTS(txt) / ncol
	thisresult = REPLICATE(strtemplate, nrow-1)
	for i = 1, nrow - 1 do $
		for j = 0, ncol - 1 do $
			thisresult[i-1].(j) = txt[j,i]
	result = [result, thisresult]
endfor

if KEYWORD_SET(dedup) then begin
	sorterstr = STRING(ANYTIM(result.t_obs), form='(i010)') + result.(dedup_ind)
	tsort_all = SSW_UNIQ(sorterstr, SORT(sorterstr), /first)
	tsort = tsort_all[SORT(sorterstr[tsort_all])]
endif else begin
	tsort = SORT(ANYTIM(result.t_obs))
endelse

result = result[tsort]
RETURN, result

end