function IRIS_PREP_GET_THERMALCORR, header, msg, $
	path = path

;+
;
; INPUT:
;	header	-	FITS header structure for the image whose correction parameters 
;				are desired
;
; OUTPUT:
;	msg	-	a string describing the resulting parameters; can be appended to the
;			FITS header if/when the corrections are applied
;
; RETURNS:
;	result	-	a structure giving the thermal pointing correction parameters to
;				be appended to the geowave correction
;
; KEYWORDS:
;	path=	-	path to the location of the thermalcorr database
;
;
;-

if N_ELEMENTS(path) eq 0 then path = GET_LOGENV('SSW_IRIS_DATA')

;+++++++++++++++++++++++++++++++++++++++++
; Currently handles array inputs by 
; calling itself recursively; not very 
; efficient, but we'll sort that out later
;-----------------------------------------
numhead = N_ELEMENTS(header)
if numhead gt 1 then begin
	result0 = IRIS_PREP_GET_THERMALCORR(header[0], path = path)
	result = REPLICATE(result0, numhead)
	for i = 1, numhead - 1 do result[i] = IRIS_PREP_GET_THERMALCORR(header[i], path = path)
	RETURN, result
endif

;+++++++++++++++++++++++++++++++++++++++++
; Find and look up the library of 
; thermal pointing correction genx files
;-----------------------------------------
files = FILE_SEARCH(CONCAT_DIR(path, '*thermalcorr.genx'))
if N_ELEMENTS(files) eq 0 then begin
	BOX_MESSAGE, ['IRIS_PREP_GET_THERMALCORR: No thermalcorr files found in', path]
	RETURN, 0
endif

file_tais = ANYTIM2TAI(FILE2TIME(STRMID(files, STRLEN(path)+1, 15)))
usefile = files[WHERE(file_tais eq MAX(file_tais))]	;	use the latest file
RESTGEN, file = usefile, thermalcorr_dat

;+++++++++++++++++++++++++++++++++++++++++++++++++++
; thermalcorr_dat is now a structure array of pointing
; correction table entries. Now we need to figure out 
; which element in the array to use
;---------------------------------------------------

; First, rule out entries that have been superseded by higher versions
start_tais = ANYTIM2TAI(thermalcorr_dat.t_start)
tsort = SORT(start_tais)
uniqstarts = UNIQ(start_tais[tsort], numuniq)
uniqarr = REPLICATE(aiacorr_dat[0], numuniq)
for i = 0, numuniq - 1 do begin
	entries_at_thist = WHERE(start_tais eq start_tais[tsort[uniqstarts[i]]])
	latest = WHERE(aiacorr_dat[entries_at_thist].version eq MAX(thermalcorr_dat[entries_at_thist].version))
	uniqarr[i] = thermalcorr_dat[latest]
endfor

; Then identify entries with the right IMG_PATH
imgpath = uniqarr.img_path
headerpath = header.img_path
pathindex = WHERE(imgpath eq headerpath)
patharr = uniqarr[pathindex]

; Then identify entries where t_start and t_stop span the image's T_OBS
start_tais = ANYTIM2TAI(patharr.t_start)
stop_tais = ANYTIM2TAI(patharr.t_stop)
index = WHERE(start_tais le header.t_obs and stop_tais ge header.t_obs, numin)
if numin ne 1 then begin
	BOX_MESSAGE, ['IRIS_PREP_GET_THERMALCORR: Multiple matching records found', $
		'Using first match...']
	index = index[0]
endif
result = patharr[index]

msg = STRJOIN(['Thermal drift correction rec_num=', result.rec_num, $
	'XCEN=', result.xcen, 'YCEN=', result.ycen], ' ')
	
RETURN, result

end