function IRIS_PREP_FIX_BADPIX, bp, $
	loud = loud

;+
;
; Remove the extra bad pixels flagged in the SJI_2796 images. Takes in a
; bad pixel list and filters out anything with X between 500 and 997 and
; Y greater than 850. No longer smart enough to check whether it has the 
; right channel or anything
;
;
;-

bpx = bp mod 2072
bpy = bp / 2072

bpframe = BYTARR(2072,1096)
bpframe[bp] = 1

keepers = WHERE(bpx lt 500 or bpy lt 850 or bpx gt 997, numkeep)
bpframe_out = BYTARR(2072,1096)
bpframe_out[bp[keepers]] = 1
bp_out = WHERE(bpframe_out eq 1)

if KEYWORD_SET(loud) then PLOOP, bpframe, bpframe_out

RETURN, bp_out

end