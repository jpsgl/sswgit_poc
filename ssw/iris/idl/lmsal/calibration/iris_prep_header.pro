function IRIS_PREP_HEADER, iindex, $
	pstop = pstop, run_time = run_time, _extra = extra

;+
;
; Apply the warping to the IRIS headers to return a header with updated 
; WCS and ROI keywords.
;
;
;-

if KEYWORD_SET(run_time) then t0 = SYSTIME(/sec) else run_time = 0
if not KEYWORD_SET(cubic) then cubic = -0.5

oindex = iindex
n_img = N_ELEMENTS(iindex)

geowave_params = IRIS_PREP_GET_GEOWAVE(oindex[0])
pdb = IRIS_MK_POINTDB(version = pdb_ver)

for i = 0, n_img - 1 do begin
	; Modify the warping parameters to do shifting/rotation/scaling
	this_geowave_params = IRIS_PREP_ALIGN_IMAGE(iindex[i], 0., $
		geowave_params, pdb, pstop = pstop, _extra = extra)
	; Apply the warping and shifting to the image
	nindex = IRIS_PREP_GEOWAVE_ROI(/update, iindex[i], this_geowave_params, $
		/boxwarp, cubic = cubic)
	; Set up the array of the warped headers to replace the old headers
	if i eq 0 then begin
		newindex = nindex
		indtemplate = nindex
	endif else begin
		STRUCT_ASSIGN, nindex, indtemplate
		newindex = [newindex, indtemplate]
	endelse
endfor
oindex = newindex
oindex.lvl_num = 1.5
UPDATE_HISTORY, oindex, 'Used iris_mk_poindtb ver ' + STRTRIM(pdb_ver, 2)

if KEYWORD_SET(run_time) then begin
	tdelt = SYSTIME(/sec)-t0
	BOX_MESSAGE, $
		[STRING('IRIS_PREP_HEADER: ', n_img, 'headers prepped', format = '(a12, i5, a15)'), $
		 STRING('Run time: ' , tdelt, 'seconds', form = '(a12, f7.1, a8)')]
endif

RETURN, oindex

end