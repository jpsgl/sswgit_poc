pro IRIS_MAKE_TIM_DATABASE_REDO, date

;+
;
; Run in an interactive IDL session to regenerate timeline
; files for a specified date, and post them to /irisa and to the web.
;
; EXAMPLE:
;	IDL> iris_make_tim_database_redo, '2014-may-11'
;
;-

tt0 = SYSTIME(/sec)
timenow = TIME2FILE(RELTIME(/now), /sec)

case 1 of
	FILE_EXIST('/irisa/data/prep/aux/')	:	finalpath = '/irisa/data/prep/aux/'
	FILE_EXIST('/net/brooklyn/Volumes/earth/iris/data/prep/aux/')	:	finalpath = '/net/brooklyn/Volumes/earth/iris/data/prep/aux/'
	FILE_EXIST('/net/bay/Volumes/earth/iris/data/prep/aux/')	:	finalpath = '/net/bay/Volumes/earth/iris/data/prep/aux/'
	else	:	begin
		PRINT, 'Cannot reach irisa...'
		finalpath = '/irisa/data/prep/aux/'
	end
endcase
logpath = CONCAT_DIR(finalpath, 'log')
finalpath = CONCAT_DIR(finalpath, 'tim')

case 1 of
	FILE_EXIST('/sanhome/boerner/public_html/iris/tim')	:	pbpath = '/sanhome/boerner/public_html/iris/tim'
	else	:	begin
		PRINT, 'Cannot reach sanhome public_html...'
		pbpath = '~/iris/cronlog/'
	end
endcase

OPENW, lun, /get, CONCAT_DIR(logpath, timenow + '_timeline.txt')
PRINTF, lun & PRINTF, lun, 'IRIS_MAKE_TIM_DATABASE_REDO running... ', timenow
PRINTF, lun

; Generate data for the previous day
PRINTF, lun, '*************************************
PRINTF, lun, ' Generating data for ', date
PRINTF, lun, '*************************************
IRIS_MAKE_TIM_DATABASE, date, outpath = '/tmp', fname = fname
newfile = CONCAT_DIR('/tmp', fname)

pbfile = CONCAT_DIR(pbpath, fname)
PRINTF, lun & PRINTF, lun, 'Copying to PB web dir...'
SPAWN, 'cp ' + newfile + ' ' + pbfile, res, err
PRINTF, lun, 'cp result: ', res
PRINTF, lun, 'cp errcode: ', err

oldfile = CONCAT_DIR(finalpath, fname)
PRINTF, lun & PRINTF, lun, 'Copying to /irisa ...'
ofsize = FILE_SIZE(oldfile)
nfsize = FILE_SIZE(newfile)
PRINTF, lun, 'Old file: ', ofsize/1e6, 'New file: ', nfsize/1e6, '(MB)', $
	form = '(a15,f15.3,a15,f15.3,a5)'
if ofsize gt nfsize then begin
	PRINTF, lun, 'Keeping old file...'
	SPAWN, 'rm ' + newfile, res, err
	PRINTF, lun, 'rm result: ', res
	PRINTF, lun, 'rm errcode: ', err
endif else begin
	PRINTF, lun, 'Keeping new file ', newfile
	SPAWN, 'mv ' + newfile + ' ' + oldfile, res, err
	PRINTF, lun, 'mv result: ', res
	PRINTF, lun, 'mv errcode: ', err
endelse

PRINTF, lun, 'Elapsed time: ', SYSTIME(/sec) - tt0, form = '(a30,f10.1)'
PRINTF, lun

FREE_LUN, lun

end