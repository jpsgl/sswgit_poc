;+
; Name    : iris_mk_pointdb
;
; Purpose : return structure with IRIS WCS relevant parameters
;           and optionally write/update geny file
;
; Use     : IDL> db=iris_mk_pointdb()
;
; Output  :
;    db = structure.  Database structure generated
;
; Keywords input:
;    file = string. name of geny file to be written/updated
;           [not yet implemented]
;    version = long.  Return latest version with db.version le version.
;                     Default: 99999L
; Keywords output:
;
; Common  :
;
; Restrictions:
;
; Side effects:
;
; Category   : Pipeline
;
; Prev. Hist.:
;
; Written    : Jean-Pierre Wuelser                       LMSAL 29-May-2013
;              JPW - added v1 with new wm_xoff,wm_yoff         19-jul-2013
;              JPW - changed db to include separate be and cdelt
;                    for all SJI filters (instead of just channels)
;                  - added v2 with various on-orbit updates    11-sep-2013
;              JPW - only include changes in new versions,
;                  - v3 (fix wm_roll_bias)                     23-sep-2013
;              JPW - add parameters for level 1.5              04-oct-2013
;              JPW - changed Ni I wavelength to vac.value
;                    and filled in db.version tag              10-oct-2013
;              JPW - wedge parameter update                    05-dec-2013
;                  - new WM encoder position correction
;                  - coeff. for temp.correction of wm_yoff
;              JPW - v6: temp & ophase dependent wavelengths   03-feb-2014
;              JPW - v6: temp. dependent fiducials FUV,NUV     28-feb-2014
;              JPW - v6: temp.dep.fid.SJI ; new temp format    04-mar-2014
;              JPW -     added separate fid_fu1 and fid_fu2    06-mar-2014
;              JPW - v7: new pointing & FUV fid. temp. corr.   21-mar-2014
;              PB  - v8: new reference pixel for FUV and NUV
;                        wavelengths and 2796 fiducial         12-apr-2014
;              JPW - v9: new offset for FUV-L                  08-dec-2016
;              JPW - secular coeffs for wvl, fi1 (unpopulated) 14-apr-2017
;              JPW - secular coeffs for fid (unpopulated)      18-apr-2017
;              JPW - v10: populate secular coeffs and update
;                         other coeffs for wvl, fi1, fid       19-apr-2017
;-

function iris_mk_pointdb,version=ver,file=file

if n_elements(ver) ne 1 then ver=99999L

; db structure: first the general stuff
d = {origin:'',telescop:'',date:'',t_start:'',t_stop:'',version:0L, $
; Detector parameters
     slit_rot:0., $
     be_fsi:0.,be_133:0.,be_279:0.,be_140:0., $
     be_283:0.,be_mir:0.,be_nuv:0.,al_nuv:0., $
     be_fu1:0.,al_fu1:0.,be_fu2:0.,al_fu2:0., $
     cdlt_fsi:0.,cdlt_133:0.,cdlt_279:0.,cdlt_140:0., $
     cdlt_283:0.,cdlt_mir:0.,cdlt1_nu:0.,cdlt2_nu:0., $
     cdlt1_f1:0.,cdlt2_f1:0.,cdlt1_f2:0.,cdlt2_f2:0., $
     cpx1_fsi:0.,cpx2_fsi:0.,cpx1_133:0.,cpx2_133:0., $
     cpx1_279:0.,cpx2_279:0.,cpx1_140:0.,cpx2_140:0., $
     cpx1_283:0.,cpx2_283:0.,cpx1_mir:0.,cpx2_mir:0., $
     cpx1_nuv:0.,cpx2_nuv:0.,cvl1_nuv:0., $
     cpx1_fu1:0.,cpx2_fu1:0.,cvl1_fu1:0., $
     cpx1_fu2:0.,cpx2_fu2:0.,cvl1_fu2:0., $
; wavelength corrections (nuv,fuv) for orb.phase, pointing, temperature, sec.drift
     wvl_noc1:0.,wvl_noc2:0.,wvl_npc1:0., $
     wvl_ntc0:0.,wvl_ntc1:fltarr(6),wvl_nsc1:0.,wvl_nsc2:0., $
     wvl_foc1:0.,wvl_foc2:0.,wvl_fpc1:0., $
     wvl_ftc0:0.,wvl_ftc1:fltarr(6),wvl_fsc1:0.,wvl_fsc2:0., $
; fiducial mark temperature corrections and sec.drift correction
     fid_nuvt0:0.,fid_nuvt1:fltarr(6),fid_nuvs1:0.,fid_nuvs2:0., $
     fid_fu1t0:0.,fid_fu1t1:fltarr(6),fid_fu1s1:0.,fid_fu1s2:0., $
     fid_fu2t0:0.,fid_fu2t1:fltarr(6),fid_fu2s1:0.,fid_fu2s2:0., $
     fid_133t0:0.,fid_133t1:fltarr(6),fid_133s1:0.,fid_133s2:0., $ ; along
     fid_140t0:0.,fid_140t1:fltarr(6),fid_140s1:0.,fid_140s2:0., $ ; crpix2
     fid_279t0:0.,fid_279t1:fltarr(6),fid_279s1:0.,fid_279s2:0., $
     fid_283t0:0.,fid_283t1:fltarr(6),fid_283s1:0.,fid_283s2:0., $
     fi1_133t0:0.,fi1_133t1:fltarr(6),fi1_133s1:0.,fi1_133s2:0., $ ; along
     fi1_140t0:0.,fi1_140t1:fltarr(6),fi1_140s1:0.,fi1_140s2:0., $ ; crpix1
     fi1_279t0:0.,fi1_279t1:fltarr(6),fi1_279s1:0.,fi1_279s2:0., $ ; (SJI only)
     fi1_283t0:0.,fi1_283t1:fltarr(6),fi1_283s1:0.,fi1_283s2:0., $
; Level 1.5 common (spatial) parameters
     cpx1_1p5:0.,cpx2_1p5:0.,cdlt_1p5:0., $
; Ted's constants for CRVALi
     ; the first three are identical to slit_rot,-be_283,cdlt_283
     slit_roll_bias:0.,sji_ccd_roll_bias:0.,pzt_off_pixel:0., $
     pzt_off_homea:0.,pzt_off_homeb:0.,pzt_off_homec:0., $
     pzt_off_sjixa:0.,pzt_off_sjixb:0.,pzt_off_sjixc:0., $
     pzt_off_sjiya:0.,pzt_off_sjiyb:0.,pzt_off_sjiyc:0., $
     pzt_x_sign:0.,pzt_y_sign:0.,                        $
     wm1_rmax:0.,wm2_rmax:0.,wm_offset2:0.,wm_m1v:0.,    $
     wm_x_sign:0.,wm_y_sign:0.,wm_xoff:0.,wm_yoff:0.,    $
     wm_roll_bias:0.,wm_focus_cal:0.,focus_regcal:0.,    $
     ; correction for wedge motor encoder position
     ; and temperature coefficients for correcting wm_yoff
     wm1_enco:0.,wm2_enco:0.,wm_ytfit0:0.,wm_ytfit1:fltarr(3),  $
     focus_xregr1:0.,focus_xregr2:0.,focus_yregr1:0.,focus_yregr2:0.}

if ver ge 0 then begin
   ; measurements from ground testing
   d.slit_rot =  0.0      ; offset of S/C system meas. eastward from slit (deg)
   d.be_fsi   =  0.31     ; NUV SJI CCD "y" axis meas. eastward from slit (deg)
   d.be_279   =  0.31     ; NUV SJI CCD "y" axis meas. eastward from slit (deg)
   d.be_283   =  0.31     ; NUV SJI CCD "y" axis meas. eastward from slit (deg)
   d.be_133   = -0.21     ; FUV SJI CCD "y" axis meas. eastward from slit (deg)
   d.be_140   = -0.21     ; FUV SJI CCD "y" axis meas. eastward from slit (deg)
   d.be_mir   = -0.21     ; FUV SJI CCD "y" axis meas. eastward from slit (deg)
   d.be_nuv   =  1.28     ; NUV SG "y" axis meas. shortward from line (deg)
   d.al_nuv   = -0.32     ; NUV SG "x" axis meas. northward from disp. (deg)
   d.be_fu1   = -1.47     ; FUV SG1 "y" axis meas. shortward from line (deg)
   d.al_fu1   = -0.06     ; FUV SG1 "x" axis meas. northward from disp. (deg)
   d.be_fu2   = -1.96     ; FUV SG2 "y" axis meas. shortward from line (deg)
   d.al_fu2   = -0.45     ; FUV SG2 "x" axis meas. northward from disp. (deg)
   d.cdlt_fsi = 0.1684    ; NUV SJ plate scale (arcsec/pixel)
   d.cdlt_279 = 0.1684    ; NUV SJ plate scale (arcsec/pixel)
   d.cdlt_283 = 0.1684    ; NUV SJ plate scale (arcsec/pixel)
   d.cdlt_133 = 0.1656    ; FUV SJ plate scale (arcsec/pixel)
   d.cdlt_140 = 0.1656    ; FUV SJ plate scale (arcsec/pixel)
   d.cdlt_mir = 0.1656    ; FUV SJ plate scale (arcsec/pixel)
   d.cdlt1_nu = 0.02546   ; NUV SG plate scale (Angstrom/pixel)
   d.cdlt2_nu = 0.1665    ; NUV SG plate scale (arcsec/pixel)
   d.cdlt1_f1 = 0.01298   ; FUV SG1 plate scale (Angstrom/pixel)
   d.cdlt2_f1 = 0.1665    ; FUV SG1 plate scale (arcsec/pixel)
   d.cdlt1_f2 = 0.01272   ; FUV SG2 plate scale (Angstrom/pixel)
   d.cdlt2_f2 = 0.1665    ; FUV SG2 plate scale (arcsec/pixel)
   d.cpx1_279 =  501.0    ; SJI 279 pixel location of slit center
   d.cpx2_279 =  509.0    ; SJI 279 pixel location of slit center
   d.cpx1_283 =  501.0    ; SJI 283 pixel location of slit center
   d.cpx2_283 =  509.0    ; SJI 283 pixel location of slit center
   d.cpx1_fsi =  501.0    ; SJI FusedSi pixel location of slit center
   d.cpx2_fsi =  509.0    ; SJI FusedSi pixel location of slit center
   d.cpx1_133 =  531.0    ; SJI 133 pixel location of slit center
   d.cpx2_133 =  521.0    ; SJI 133 pixel location of slit center
   d.cpx1_140 =  531.0    ; SJI 140 pixel location of slit center
   d.cpx2_140 =  521.0    ; SJI 140 pixel location of slit center
   d.cpx1_mir =  531.0    ; SJI Mirror pixel location of slit center
   d.cpx2_mir =  521.0    ; SJI Mirror pixel location of slit center
   d.cpx1_nuv = 1555.0    ; NUV SG pixel loc. of slit center @ ref.wavelength
   d.cpx2_nuv =  520.0    ; NUV SG pixel loc. of slit center @ ref.wavelength
   d.cvl1_nuv = 2796.00   ; NUV SG ref.wavelength
   d.cpx1_fu1 = 1388.0    ; FUV SG1 pixel loc. of slit center @ ref.wavelength
   d.cpx2_fu1 =  481.0    ; FUV SG1 pixel loc. of slit center @ ref.wavelength
   d.cvl1_fu1 = 1349.24   ; FUV SG1 ref.wavelength
   d.cpx1_fu2 = 3460.0    ; FUV SG2 pixel loc. of slit center @ ref.wavelength
   d.cpx2_fu2 =  508.0    ; FUV SG2 pixel loc. of slit center @ ref.wavelength
   d.cvl1_fu2 = 1398.00   ; FUV SG2 ref.wavelength
   d.slit_roll_bias    =  d.slit_rot
   d.sji_ccd_roll_bias = -d.be_283     ; note opposite sign convention!
   d.pzt_off_pixel     =  d.cdlt_283
   ; from Ted's iris_isp2solar.pro version 0.97
   d.pzt_off_homea = -250       ; _home? combine to vector in iris_isp2solar
   d.pzt_off_homeb = -250
   d.pzt_off_homec = -250
   d.pzt_off_sjixa =  0.00130   ; _sjix? combine to vector in iris_isp2solar
   d.pzt_off_sjixb = -0.14929   ; _sjix? combine to vector in iris_isp2solar
   d.pzt_off_sjixc =  0.14783   ; _sjix? combine to vector in iris_isp2solar
   d.pzt_off_sjiya = -0.17203   ; _sjiy? combine to vector in iris_isp2solar
   d.pzt_off_sjiyb =  0.08709   ; _sjiy? combine to vector in iris_isp2solar
   d.pzt_off_sjiyc =  0.08687   ; _sjiy? combine to vector in iris_isp2solar
   d.pzt_x_sign    =  1.0
   d.pzt_y_sign    =  1.0
   d.wm1_rmax      =  634.8
   d.wm2_rmax      =  628.5
   d.wm_offset2    =  113.570
   d.wm_m1v        =  217.875
   d.wm_x_sign     = -1.0
   d.wm_y_sign     =  1.0
   d.wm_xoff       =   73.0       ; arcsec (from JPW ground theodolite meas.)
   d.wm_yoff       = -101.5       ; arcsec (from JPW ground theodolite meas.)
   d.wm_roll_bias  =  0.0
   d.wm_focus_cal  = -100.0
   d.focus_regcal  = -100.0       ; hard wired to -100 in iris_isp2solar
   d.focus_xregr1  = -0.0514701   ; _xregr? combine to vector in iris_isp2solar
   d.focus_xregr2  =  0.000248873 ; _xregr? combine to vector in iris_isp2solar
   d.focus_yregr1  =  0.0739689   ; _yregr? combine to vector in iris_isp2solar
   d.focus_yregr2  = -0.000475100 ; _yregr? combine to vector in iris_isp2solar
endif
if ver ge 1 then begin
   ; updates since previous version
   d.wm_xoff       =  103.0       ; arcsec (from first on-orbit 4-limb test)
   d.wm_yoff       =  -92.0       ; arcsec (from first on-orbit 4-limb test)
   d.version  = 1L
endif
if ver ge 2 then begin
   ; updates since previous version: on-orbit updates
   d.slit_rot =  0.646    ; offset of S/C system meas. E from slit (deg) ; Okamoto 2013-Aug
   d.be_fsi   =  0.28     ; NUV SJI CCD "y" axis meas. E from slit (deg)
   d.be_279   =  0.274    ; NUV SJI CCD "y" axis meas. E from slit (deg) ; JPW 2013-Sep
   d.be_283   =  0.286    ; NUV SJI CCD "y" axis meas. E from slit (deg) ; JPW 2013-Sep
   d.be_133   = -0.200    ; FUV SJI CCD "y" axis meas. E from slit (deg) ; JPW 2013-Sep
   d.be_140   = -0.224    ; FUV SJI CCD "y" axis meas. E from slit (deg) ; JPW 2013-Sep
   d.be_mir   = -0.21     ; FUV SJI CCD "y" axis meas. E from slit (deg)
   d.be_nuv   =  1.26     ; NUV SG "y" axis shortwd from line meas.betw.fid's ; Sarah 2013-Sep
   d.al_nuv   = -0.318    ; NUV SG "x" axis meas. northward from disp. (deg)  ; Sarah 2013-Sep
   d.be_fu1   = -1.50     ; FUV SG1 "y" ax. shortwd from line meas.betw.fid's ; Sarah 2013-Sep
   d.al_fu1   = -0.050    ; FUV SG1 "x" axis meas. northward from disp. (deg) ; Sarah 2013-Sep
   d.be_fu2   = -1.97     ; FUV SG2 "y" ax. shortwd from line meas.betw.fid's ; Sarah 2013-Sep
   d.al_fu2   = -0.437    ; FUV SG2 "x" axis meas. northward from disp. (deg) ; Sarah 2013-Sep
   d.cdlt_fsi = 0.16790   ; NUV SJ plate scale (arcsec/pixel)      ; JPW 2013-Sep
   d.cdlt_279 = 0.16790   ; NUV SJ plate scale (arcsec/pixel)      ; JPW 2013-Sep
   d.cdlt_283 = 0.16790   ; NUV SJ plate scale (arcsec/pixel)      ; JPW 2013-Sep
   d.cdlt_133 = 0.16560   ; FUV SJ plate scale (arcsec/pixel)      ; JPW 2013-Sep
   d.cdlt_140 = 0.16560   ; FUV SJ plate scale (arcsec/pixel)      ; JPW 2013-Sep
   d.cdlt_mir = 0.16560   ; FUV SJ plate scale (arcsec/pixel)      ; JPW 2013-Sep
   d.cdlt1_nu = 0.02546   ; NUV SG ave.plate scale (Angstrom/pix)  ; prelaunch=canonical ave
   d.cdlt2_nu = 0.16637   ; NUV SG plate scale (arcsec/pixel)      ; JPW 2013-Sep
   d.cdlt1_f1 = 0.01298   ; FUV SG1 ave.plate scale (Angstrom/pix) ; prelaunch=canonical ave
   d.cdlt2_f1 = 0.16632   ; FUV SG1 plate scale (arcsec/pixel)     ; JPW 2013-Sep
   d.cdlt1_f2 = 0.01272   ; FUV SG2 ave.plate scale (Angstrom/pix) ; prelaunch=canonical ave
   d.cdlt2_f2 = 0.16632   ; FUV SG2 plate scale (arcsec/pixel)     ; JPW 2013-Sep
   d.cpx1_279 =  504.69   ; SJI 279 pixel location of slit center  ; Paul & Sean 2013-Aug
   d.cpx2_279 =  502.91   ; SJI 279 pixel location of slit center  ; Paul & Sean 2013-Aug
   d.cpx1_283 =  506.47   ; SJI 283 pixel location of slit center  ; Paul & Sean 2013-Aug
   d.cpx2_283 =  502.22   ; SJI 283 pixel location of slit center  ; Paul & Sean 2013-Aug
   d.cpx1_fsi =  505.2    ; SJI FusedSi pixel location of slit center ; JPW 2013-Sep
   d.cpx2_fsi =  504.7    ; SJI FusedSi pixel location of slit center ; JPW 2013-Sep
   d.cpx1_133 =  537.30   ; SJI 133 pixel location of slit center  ; Paul & Sean 2013-Aug
   d.cpx2_133 =  524.03   ; SJI 133 pixel location of slit center  ; Paul & Sean 2013-Aug
   d.cpx1_140 =  529.32   ; SJI 140 pixel location of slit center  ; Paul & Sean 2013-Aug
   d.cpx2_140 =  510.46   ; SJI 140 pixel location of slit center  ; Paul & Sean 2013-Aug
   d.cpx1_mir =  534.0    ; SJI Mirror pixel location of slit center ; JPW 2013-Sep
   d.cpx2_mir =  516.7    ; SJI Mirror pixel location of slit center ; JPW 2013-Sep
   d.cpx1_nuv =  659.3    ; NUV SG pixel of slit center @ ref.wavelength ; Sarah 2013-Sep
   d.cpx2_nuv =  530.35   ; NUV SG pixel of slit center @ ref.wavelength ; Hui/Sean 2013-Aug
   d.cvl1_nuv = 2798.649  ; NUV SG ref.wavelength                        ; Ni line
   d.cpx1_fu1 =  219.5    ; FUV SG1 pixel of slit center @ ref.wavelngth ; Sarah 2013-Sep
   d.cpx2_fu1 =  487.92   ; FUV SG1 pixel of slit center @ ref.wavelngth ; Hui/Sean 2013-Aug
   d.cvl1_fu1 = 1334.53   ; FUV SG1 ref.wavelength                       ; C II line
   d.cpx1_fu2 = 3807.3    ; FUV SG2 pixel of slit center @ ref.wavelngth ; Sarah 2013-Sep
   d.cpx2_fu2 =  518.21   ; FUV SG2 pixel of slit center @ ref.wavelngth ; Hui/Sean 2013-Aug
   d.cvl1_fu2 = 1402.770  ; FUV SG2 ref.wavelength                       ; Si IV line
   ; slit_rot, be_283, cdlt_283 were updated for this version:
   d.slit_roll_bias    =  d.slit_rot
   d.sji_ccd_roll_bias = -d.be_283     ; note opposite sign convention!
   d.pzt_off_pixel     =  d.cdlt_283
   d.version  = 2L
endif
if ver ge 3 then begin
   ; updates since previous version: fix wm_roll_bias
   d.wm_roll_bias  =  -0.646

 ; Level 1.5 common (spatial) parameters (introduced betw. ver 3 and 4)
   d.cpx1_1p5 = 520.+1.   ; iris_prep_align_image.pro + 1 (FITS convention)
   d.cpx2_1p5 = 508.+1.   ; iris_prep_align_image.pro + 1 (FITS convention)
 ; d.cdlt_1p5 = 0.16610   ; hard-wired in iris_prep_align_image.pro
   d.cdlt_1p5 = 0.16635   ; average of FUV, NUV SGs (should use this
                          ; since Sarah's distor.corr. doesn't re-scale)
   d.version  = 3L
endif
if ver ge 4 then begin
   ; updates since previous version: proper vacuum wavelength for Ni I line
   ; slight correction (2 mA) for C II using Kurucz line list
   ; finally fill in version tag
   d.cvl1_nuv = 2799.474  ; NUV SG ref.wavelength (vacuum!)         ; Ni line
   d.cvl1_fu1 = 1334.532  ; FUV SG1 ref.wavelength                  ; C II line
   d.version  = 4L
endif
if ver ge 5 then begin
   ; These are improvements based on fitting the wedge motor parameters to
   ; full disk mosaics on 2013-sep-30 & oct-13.  Setting wm_roll_bias to 0.
   ; Also added a new wedge motor encoder correction
   ; and coefficients for a temperature correction of wm_yoff:
   ; wm_yoff_new = wm_yoff + wm_ytfit0 + (it01pmrf-it06telm)*wm_ytfit1 ;old form
   ; wm_yoff_new=wm_yoff+wm_ytfit0+total([it01pmrf,it06telm,it18sppz]*wm_ytfit1)
   d.wm1_rmax      =  633.2
   d.wm2_rmax      =  628.5      ; this one happes to be unchanged
   d.wm_offset2    =  113.707
   d.wm_m1v        =  217.282
   d.wm_xoff       =  104.6
   d.wm_yoff       =  -82.1
   d.wm_roll_bias  =    0.0
   d.wm1_enco      =    0.127
   d.wm2_enco      =    0.160
   d.wm_ytfit0     =  -45.2
   d.wm_ytfit1     =  [ 3.330,-3.330, 0.000]
   d.version  = 5L
endif
if ver ge 6 then begin
   ; Use calibration lines as references instead of strong lines
   d.cpx1_fu1 = 1840.93   ; FUV SG1 pixel of slit center @ ref.wavelngth ; Sarah 2013-Sep
   d.cpx2_fu1 =  489.34   ; FUV SG1 pixel of slit center @ ref.wavelngth ; adj. for tilt
   d.cvl1_fu1 = 1355.598  ; FUV SG1 ref.wavelength                       ; O I line
   ; Fe II wavelength uncertain -> commented out -> still using Si IV as reference
  ; d.cpx1_fu2 = 3024.74   ; = cpx1_fu1+1183.81 ; FUV SG2 pixel of slit center @ ref.wavelnth
  ; d.cpx2_fu2 =  512.24   ; FUV SG2 pixel of slit center @ ref.wavelngth ; adj. for tilt
  ; d.cvl1_fu2 = 1392.82   ; FUV SG2 ref.wavelength                       ; Fe II line
   ; Wavelength corrections based on fitting measured neutral lines to
   ; orbital phase, E-W pointing, SG temperatures (Level 1 only)
   ; crpix1 = cpx1 + obs_vr*crval1/(2.998e8*cdlt1)
   ;               + oc1*sin(2*!pi*ophase) + oc2*cos(2*!pi*ophase)
   ;               + pc1*crval3*cos(sat_rot/!radeg)
   ;               + tc0 + tc1[0]*it14sppx + tc1[1]*it16spnx
   ;                     + tc1[2]*it15sppy + tc1[3]*it17spny
   ;                     + tc1[4]*it18sppz + tc1[5]*it19spnz
   ; crpix1 = crpix1 /sumsptrl + (sumsptrl-1)/(sumsptrl*2)
   d.wvl_noc1  =  -0.614
   d.wvl_noc2  =  -0.256
   d.wvl_npc1  =   4.0e-4
   d.wvl_ntc0  = -10.744
   d.wvl_ntc1  = [ 0.641, 0.377, 0.491,-0.186,-0.178,-1.637]
   d.wvl_foc1  =   0.752
   d.wvl_foc2  =   0.496
   d.wvl_fpc1  =   4.0e-4
   d.wvl_ftc0  =  63.520
   d.wvl_ftc1  = [-1.377,-0.019,-2.349,-0.201, 0.219, 2.337]
   ; fiducial mark location temp.corrections: along slit
   d.fid_nuvt0 =  13.224
   d.fid_nuvt1 = [-5.369,-3.131, 9.110, 0.783,-2.869, 0.766]
   d.fid_fu1t0 = -65.589
   d.fid_fu1t1 = [ 1.749,-2.409, 2.635, 0.271, 0.546, 0.211]
   d.fid_fu2t0 = -65.589
   d.fid_fu2t1 = d.fid_fu1t1
   d.fid_133t0 =  33.291
   d.fid_133t1 = [ 0.930,-0.172,-1.230,-0.633,-0.255,-0.048]
   d.fid_140t0 =  33.691
   d.fid_140t1 = d.fid_133t1
   d.fid_279t0 =  13.805
   d.fid_279t1 = [-0.565,-0.238, 0.655,-0.084,-0.452, 0.207]
   d.fid_283t0 =  13.005
   d.fid_283t1 = d.fid_279t1
   ; fiducial mark location temp.corrections: across slit (SJI only)
   d.fi1_133t0 =  -5.970
   d.fi1_133t1 = [ 0.703, 0.500,-1.591, 0.616, 0.577,-1.132]
   d.fi1_140t0 =  -5.470
   d.fi1_140t1 = d.fi1_133t1
   d.fi1_279t0 =  29.006
   d.fi1_279t1 = [ 0.771,-0.306,-1.382,-2.219, 1.352, 1.207]
   d.fi1_283t0 =  28.806
   d.fi1_283t1 = d.fi1_279t1
   d.version  = 6L
endif
if ver ge 7 then begin
   ; updates since previous version: new temp.corr. for pointing & FUV fiducials
   ; wm_yoff_new=wm_yoff+wm_ytfit0+total([it01pmrf,it06telm,it18sppz]*wm_ytfit1)
   d.wm_ytfit0     =   25.299
   d.wm_ytfit1     =  [ 0.688, 1.450,-3.422]
   d.fid_fu1t0 = -64.078
   d.fid_fu1t1 = [-1.499,-1.810, 5.993, 1.590,-1.266,-1.147]
   d.fid_fu2t0 = -64.078
   d.fid_fu2t1 = d.fid_fu1t1
   d.version  = 7L
endif
if ver ge 8 then begin
   d.cpx1_fu1 = 1843.43	; FUV SG1 pixel of slit center @ ref.wavelngth ; iris_prep 2014/04/08
   d.cpx1_nuv = 659.9 	; NUV SG1 pixel of slit center @ ref.wavelngth ; iris_prep 2014/04/08
   d.cpx2_279 =  503.402	; SJI 279 pixel location of slit center	; iris_prep 2014/04/08
   d.version  = 8L
endif
if ver ge 9 then begin
; IMPORTANT: most critical for iris_prep is the DIFFERENCE betw cpx1_fu and cpx_fu2 !
   d.cpx1_fu2 = 3028.43   ; = cpx1_fu1+1185.0 ; FUV SG2 pixel of slit center @ ref.wavelnth
   d.cpx2_fu2 =  512.24   ; FUV SG2 pixel of slit center @ ref.wavelngth ; adj. for tilt
   d.cvl1_fu2 = 1392.817  ; FUV SG2 ref.wavelength                       ; Fe II line
   d.version  = 9L
endif
if ver ge 10 then begin
   ; new wvl corrections coefficients
   d.wvl_noc1  =  -0.433
   d.wvl_noc2  =  -0.265
   d.wvl_npc1  =   5.1e-4
   d.wvl_ntc0  = -10.046
   d.wvl_ntc1  = [-0.449, 0.391, 1.066,-0.263,-0.193,-0.808]
   d.wvl_nsc1  =   3.438
   d.wvl_nsc2  =  -0.565
   d.wvl_foc1  =   0.403
   d.wvl_foc2  =   0.453
   d.wvl_fpc1  =   7.6e-4
   d.wvl_ftc0  =  34.218
   d.wvl_ftc1  = [-0.185,-1.909, 0.595, 0.438,-0.913, 2.007]
   d.wvl_fsc1  =  -6.651
   d.wvl_fsc2  =  -1.127
   ; new fiducial corrections coefficients
   d.fid_nuvt0 =  17.093
   d.fid_nuvt1 = [ 0.280,-2.260, 2.217,-0.143,-0.587,-0.360]
   d.fid_nuvs1 =  -4.476
   d.fid_nuvs2 =  -0.196
   d.fid_fu1t0 = -47.741
   d.fid_fu1t1 = [ 0.171,-2.372, 4.006, 0.115,-0.145, 0.150]
   d.fid_fu1s1 =   1.912
   d.fid_fu1s2 =  -1.528
   d.fid_fu2t0 = -45.972
   d.fid_fu2t1 = [ 0.612,-2.397, 3.550,-0.088, 0.030, 0.185]
   d.fid_fu2s1 =   1.885
   d.fid_fu2s2 =  -1.556
   d.fid_133t0 =   3.162
   d.fid_133t1 = [ 1.730,-1.124,-0.786, 0.306,-0.526,-0.130]
   d.fid_133s1 =  11.488
   d.fid_133s2 =  -0.086
   d.fid_140t0 =  22.313
   d.fid_140t1 = [-3.351,-1.340, 4.546, 0.618,-2.210, 0.460]
   d.fid_140s1 =   6.424
   d.fid_140s2 =  -0.086
   d.fid_279t0 =   6.909
   d.fid_279t1 = [-0.402,-0.530, 1.072, 0.131,-0.609, 0.067]
   d.fid_279s1 =  -0.482
   d.fid_279s2 =  -0.597
   d.fid_283t0 =   2.756
   d.fid_283t1 = [ 0.401,-0.923, 0.854, 0.134,-0.529, 0.015]
   d.fid_283s1 =  -0.671
   d.fid_283s2 =  -0.619
   ; new slit location corrections coefficients
   d.fi1_133t0 =  -9.654
   d.fi1_133t1 = [ 1.391,-0.387,-0.942, 0.450, 0.305,-0.813]
   d.fi1_133s1 =   1.097
   d.fi1_133s2 =  -2.884
   d.fi1_140t0 =   5.912
   d.fi1_140t1 = [-1.850, 0.138, 1.163, 0.343,-0.301,-0.426]
   d.fi1_140s1 =   5.666
   d.fi1_140s2 =  -0.088
   d.fi1_279t0 =   1.069
   d.fi1_279t1 = [ 0.501,-0.394,-0.205,-1.273, 1.038, 1.015]
   d.fi1_279s1 =  -2.963
   d.fi1_279s2 =  -1.057
   d.fi1_283t0 =  -3.024
   d.fi1_283t1 = [ 0.943,-0.747,-0.181,-1.208, 1.052, 1.004]
   d.fi1_283s1 =  -2.791
   d.fi1_283s2 =  -1.117
   d.version  = 10L
endif

if ver eq 99999L then ver=d.version

return,d

end
