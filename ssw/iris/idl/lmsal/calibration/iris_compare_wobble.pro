pro IRIS_COMPARE_WOBBLE, files, $
	pickorb = pickorb, ref_orbid = ref_orbid, pztplot = pztplot, $
	png = png, win = win, pstop = pstop, sref = sref, shift = shift, $
	phaseshift = phaseshift, sepaxis = sepaxis, interp = interp, $
	label = label

;+
;
; label	-	set (to 1) to pick the date out of the table description and
;			append that to the legend; or set to a string array to explicitly
;			specify what should be in the legend
;
;-

; Set up defaults
ttag = TIME2FILE(/sec, RELTIME(/now))
if not KEYWORD_SET(opath) then opath = '~/iris/wobble/'
if not KEYWORD_SET(win) then win = 10			; 	Window index
if not KEYWORD_SET(sref) then sref = 0.16635	;	Platescale (arcsec per pixel)
if not KEYWORD_SET(pper) then pper = 5856.		;	Orbital period in sec
orbdir = '/irisa/ops/iris_user_tables/ORB/'
	
; Set up the list of files to be used, including any picked interactively or 
; designated as reference
usefiles = ''
if N_ELEMENTS(files) gt 0 then usefiles = [usefiles, files]
if KEYWORD_SET(pickorb) then begin
	orbfile = DIALOG_PICKFILE(path=orbdir)
	ref_orbid = FIX(STRMID(orbfile, STRLEN(orbfile)-6, 3))
	usefiles = [usefiles, orbfile]
endif
if N_ELEMENTS(usefiles) lt 3 and N_ELEMENTS(ref_orbid) eq 0 then ref_orbid = 29
if N_ELEMENTS(ref_orbid) gt 0 then begin
	orbfile = CONCAT_DIR(orbdir, 'ORB-00' + STRING(ref_orbid, form = '(i03)') + '.xml')
	usefiles = [usefiles, orbfile]
endif
usefiles = usefiles[1:*]
if N_ELEMENTS(usefiles) lt 2 then begin
	PRINT, 'Need at least two files to compare; use /pickorb, ref_orbid= or files parameter'
	STOP	
endif
if N_ELEMENTS(files) eq 0 then files = usefiles[0]

; Load files structures
nref = N_ELEMENTS(usefiles)
if N_ELEMENTS(label) eq 0 then label = STRARR(nref)
CD, orbdir, current = old_dir
for i = 0, nref - 1 do begin
	orbdat = SSW_IRIS_XML2STRUCT(usefiles[i], /add)
	this_orbid = FIX(orbdat.header.id)
	thisname = FILE_BASENAME(usefiles[i], '.xml', /fold)
	datestr = (STRSPLIT(/extract, STRMID(orbdat.info.description, STRPOS(orbdat.info.description, 'based on ') + 9, 13), ' '))[0]
	if N_ELEMENTS(label) eq 1 then thislabel = datestr else thislabel = label[i]
	if i eq 0 then table_label = (thisname + ' ' + thislabel) else table_label = [table_label, (thisname + ' ' + thislabel)]
	thisname = 'i' + STRJOIN(STRSPLIT(/extract, thisname, /regex, '-'))
	if KEYWORD_SET(phaseshift) and i gt 0 then begin
		orbdat.data.time = (pper * 1000. + orbdat.data.time + (phaseshift[i-1] * pper * 1000.)) mod (pper * 1000.)
		shift_t0 = WHERE(orbdat.data.time eq MIN(orbdat.data.time))
		orbdat.data.pztaoffset = STRING(FIX(orbdat.data.pztaoffset) - FIX(orbdat.data[shift_t0].pztaoffset))
		orbdat.data.pztboffset = STRING(FIX(orbdat.data.pztboffset) - FIX(orbdat.data[shift_t0].pztboffset))
		orbdat.data.pztcoffset = STRING(FIX(orbdat.data.pztcoffset) - FIX(orbdat.data[shift_t0].pztcoffset))
		table_label[i] = table_label[i] + ' + phase ' + STRING(phaseshift[i-1], form='(f6.3)')
	endif
	; Generate the X and Y offsets from the reference wobble data
	onpoints = N_ELEMENTS(orbdat.data.time)
	oxy = FLTARR(2,onpoints)
	for j = 0, onpoints-1 do oxy[*,j] = IRIS_PZT2XY(orbdat.data[j].pztaoffset, $
		orbdat.data[j].pztboffset, orbdat.data[j].pztcoffset, /no)/sref
	this_orbdat = {time : orbdat.data.time, pztaoffset : orbdat.data.pztaoffset, $
		pztboffset : orbdat.data.pztboffset,  pztcoffset : orbdat.data.pztcoffset, $
	 	onpoints : onpoints, oxy : oxy}
	if i eq 0 then begin
		ref_orbdat = CREATE_STRUCT(thisname, this_orbdat)
		ref_pztoffs = FIX([this_orbdat.pztaoffset, this_orbdat.pztboffset, this_orbdat.pztcoffset])
		ref_xyoffs = this_orbdat.oxy	;	Only used for setting limits
		if KEYWORD_SET(interp) then begin
			ref_x = this_orbdat.oxy[0,*]
			ref_y = this_orbdat.oxy[1,*]
			ref_t = this_orbdat.time / 1000.
			ref_orbdat.(0).oxy = ref_orbdat.(0).oxy * 0.
			ref_xyoffs = ref_xyoffs * 0.
		endif
	endif else begin
		if KEYWORD_SET(interp) then begin
			interpx = INTERPOL(ref_x, ref_t, this_orbdat.time/1000.)
			interpy = INTERPOL(ref_y, ref_t, this_orbdat.time/1000.)
			this_orbdat.oxy[0,*] = this_orbdat.oxy[0,*] - interpx 
			this_orbdat.oxy[1,*] = this_orbdat.oxy[1,*] - interpy 
			if KEYWORD_SET(shift) then begin
				this_orbdat.oxy[0,*] = this_orbdat.oxy[0,*] - this_orbdat.oxy[0,0] 
				this_orbdat.oxy[1,*] = this_orbdat.oxy[1,*] - this_orbdat.oxy[1,0]
			endif
		endif
		ref_orbdat = CREATE_STRUCT(ref_orbdat, thisname, this_orbdat)
		ref_pztoffs = [ref_pztoffs, FIX([this_orbdat.pztaoffset, this_orbdat.pztboffset, this_orbdat.pztcoffset])]
		ref_xyoffs = [[ref_xyoffs], [this_orbdat.oxy]]
	endelse
endfor
CD, old_dir

; Generate some plots to check the results
TVLCT, rr, gg, bb, /get
PB_SET_LINE_COLOR
oldpmulti = !p.multi
!p.multi = 0
pztsym = [4,6,5]
cols = [255, 2, 6, 5, 3]
;if KEYWORD_SET(interp) then begin
;	ppsym = -4
;	ppsymy = -6
;endif else begin
	ppsym = 4
	ppsymy = 6
;endelse

rresult = ref_orbdat.(0)

if KEYWORD_SET(pztplot) then begin
	WDEF, win, 1200, 800
	PLOT, rresult.time, rresult.pztaoffset, psym = 3, chars = 1.5, $
		xtitle = 'Relative time [msec]', ytitle = 'PZT offset [DN]', $
		yrange = LIMITS(FLOAT([rresult.pztaoffset, rresult.pztboffset, $
			rresult.pztcoffset, ref_pztoffs])), title = 'Comparison with old wobble table : PZT ABC'
	OPLOT, rresult.time, rresult.pztaoffset, psym = 3, col = 0
	for i = 0, 2 do begin
		OPLOT, rresult.time, rresult.(i+1), psym = pztsym[i], thick = 2
		for j = 1, nref - 1 do begin
			this_orbdat = ref_orbdat.(j)
			OPLOT, this_orbdat.time, this_orbdat.(i+1), psym = pztsym[i], col = cols[j]
		endfor
	endfor
	LEGEND, pos = 10, chars = 1.5, 'PZT ' + ['A', 'B', 'C'], psym = pztsym
	LEGEND, pos = 12, chars = 1.5, table_label, col = cols[0:nref-1], psym = 4+INTARR(nref), thick = [2, INTARR(nref)+1]
	if KEYWORD_SET(png) then PB_WIN2PNG, CONCAT_DIR(opath, ttag + '-total_pzt_comp.png')
endif

; And in XY space...
WDEF, win+1, 1200, 800
if KEYWORD_SET(sepaxis) then begin
	!p.multi = [0, 1, 2]
	; Make the X plot
	PLOT, rresult.time, ref_orbdat.(0).oxy[0,*], psym = ppsym, chars = 1.5, $
		xtitle = 'Relative time [msec]', ytitle = 'X Image offset [IRIS pixels]', $
		yrange = LIMITS(ref_xyoffs[0,*]), title = 'Comparison with ' + files[0] + ': X'
	for j = 1, nref - 1 do begin
		this_orbdat = ref_orbdat.(j)
		OPLOT, this_orbdat.time, this_orbdat.oxy[0,*], psym = ppsym, col = cols[j]
	endfor
	if not KEYWORD_SET(interp) then OPLOT, rresult.time, ref_orbdat.(0).oxy[0,*], psym = ppsym, thick = 2
	LEGEND, pos = 12, chars = 1.5, table_label, col = cols[0:nref-1], $
		psym = ppsym+INTARR(nref), thick = [2, INTARR(nref-1)+1]
	; Make the Y plot
	PLOT, rresult.time, ref_orbdat.(0).oxy[1,*], psym = ppsymy, chars = 1.5, $
		xtitle = 'Relative time [msec]', ytitle = 'Y Image offset [IRIS pixels]', $
		yrange = LIMITS(ref_xyoffs[1,*]), title = 'Comparison with ' + files[0] + ': Y'
	for j = 1, nref - 1 do begin
		this_orbdat = ref_orbdat.(j)
		OPLOT, this_orbdat.time, this_orbdat.oxy[1,*], psym = ppsymy, col = cols[j]
	endfor
	if not KEYWORD_SET(interp) then OPLOT, rresult.time, ref_orbdat.(0).oxy[1,*], psym = ppsymy, thick = 2
	!p.multi = oldpmulti
endif else begin
	PLOT, rresult.time, ref_orbdat.(0).oxy[0,*], psym = 4, chars = 1.5, $
		xtitle = 'Relative time [msec]', ytitle = 'Image offset [IRIS pixels]', $
		yrange = LIMITS(ref_xyoffs), title = 'Comparison with ' + files[0] + ': X'
	for j = 1, nref - 1 do begin
		this_orbdat = ref_orbdat.(j)
		OPLOT, this_orbdat.time, this_orbdat.oxy[0,*], psym = 4, col = cols[j]
		OPLOT, this_orbdat.time, this_orbdat.oxy[1,*], psym = 6, col = cols[j]
	endfor
	OPLOT, rresult.time, ref_orbdat.(0).oxy[1,*], psym = 6, thick = 2
	OPLOT, rresult.time, ref_orbdat.(0).oxy[0,*], psym = 4, thick = 2
	LEGEND, pos = 10, chars = 1.5, ['X', 'Y'], psym = [4, 6]
	LEGEND, pos = 12, chars = 1.5, table_label, col = cols[0:nref-1], psym = 4+INTARR(nref), thick = [2, INTARR(nref-1)+1]
endelse
if KEYWORD_SET(png) then PB_WIN2PNG, CONCAT_DIR(opath, ttag + '-total_xy_comp.png')

if KEYWORD_SET(pstop) then STOP

!p.multi = oldpmulti
TVLCT, rr, gg, bb

end
