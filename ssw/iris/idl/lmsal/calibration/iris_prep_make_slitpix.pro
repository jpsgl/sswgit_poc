function IRIS_PREP_MAKE_SLITPIX, flat_index, ttag, $
	thresh = thresh, write = write, vertical = vertical, $
	fix = fix, show = show, spec = spec, path = path, pstop = pstop

;+
;
; Read in the flatfield index and make a slit pixel mask for each entry.
; This is only relevant for SJIs. It works by comparing the version of the 
; flatfield that does not have the slit removed with the official un-slitted
; version and flagging only the pixels that are dark in the one with the slit.
;
; KEYWORDS:
;	thresh	-	the threshold value in the flatfield; defaults to 0.4
;	/write	-	if set, then a genx file of bad pixel masks is written out
;
;-

; Set up some defaults
if not KEYWORD_SET(thresh) then thresh = -0.3
sswdata = CONCAT_DIR('$SSW_IRIS','data')
if N_ELEMENTS(ttag) eq 0 then ttag = TIME2FILE(/sec, RELTIME(/now))

if N_TAGS(flat_index) eq 0 then begin
	; Read the latest flat_index file
	flatinds = FILE_SEARCH(CONCAT_DIR(sswdata, '*flat.genx'))
	flatind_tai = ANYTIM2TAI(FILE2TIME(STRMID(FILE_BASENAME(flatinds), 0, 15)))
	latest = WHERE(flatind_tai eq MAX(flatind_tai))
	RESTGEN, file = flatinds[latest], flat_index
endif

; Loop through the SJI img_paths and generate a slit pixel map for each
; based on Sarah's flat files on a specific date, with and without un-slitting
local_calpath = '/Users/boerner/data/iris/calibration/201502/'
slitfiles = FILE_SEARCH(CONCAT_DIR(local_calpath, '2015*chaeflat_final_slit.fits'))
unslitfiles = FILE_SEARCH(CONCAT_DIR(local_calpath, '2015*chaeflat_final.fits'))
nfiles = N_ELEMENTS(slitfiles)
imgpaths = STRARR(nfiles)
for i = 0, nfiles - 1 do imgpaths[i] = $
	STRJOIN( (STRSPLIT(/extract, FILE_BASENAME(slitfiles[i]), '_'))[1:2], '_')
	
MREADFITS, slitfiles, slithdr, slitdat
MREADFITS, unslitfiles, unslithdr, unslitdat
for i = 0, nfiles - 1 do begin
	tagname = imgpaths[i]
	if KEYWORD_SET(vertical) then begin
		slitprof = TOTAL(slitdat[*,*,i], 2, /nan)/1096.
		slitpix_x = WHERE(slitprof[100:1000] lt (thresh * MEDIAN(slitprof[100:1000])), numslit)
		PRINT, imgpaths[i], numslit
		if numslit gt 0 then begin
			slitpix = LON64ARR(numslit * 1096)
			for j = 0l, numslit-1 do slitpix[j*1096:(j*1096)+1095] = slitpix_x[j] + 98 + FINDGEN(1096) * 2072.
		endif
	endif else begin
		slitpix = WHERE( (slitdat[*,*,i] - unslitdat[*,*,i]) lt thresh, numslit )
	endelse
	if numslit lt 1 then slitpix = [0ll]
	if i eq 0 then slitstr = CREATE_STRUCT(tagname, slitpix) else $
		slitstr = CREATE_STRUCT(slitstr, tagname, slitpix)
endfor

; Loop through the index fields and make a structure tag for each
nflat = N_ELEMENTS(flat_index)
for i = 0, nflat - 1 do begin
	imgpath = flat_index[i].img_path
	slitstr_ind = WHERE(imgpath eq imgpaths)
	if slitstr_ind eq -1 then thisslit = [0ll] else thisslit = LONG64(slitstr.(slitstr_ind))
	; Make a new structure tag for this recnum
	tagname = 'F' + STRTRIM(flat_index[i].recnum, 2)
	if i eq 0 then begin
		result = CREATE_STRUCT(tagname, thisslit)
	endif else begin
		result = CREATE_STRUCT(result, tagname, thisslit)
	endelse
endfor

if KEYWORD_SET(write) then begin
	if N_ELEMENTS(path) eq 0 then path = local_calpath
	fname = CONCAT_DIR(path, ttag + '_slitpix.genx')
	SAVEGEN, file = fname, str = result
endif

if KEYWORD_SET(show) then begin
	maskimg = BYTARR(2072, 1096, nfiles)
	for i = 0, nfiles - 1 do begin
		thismask = BYTARR(2072, 1096)
		thismask[slitstr.(i)] = 1
		maskimg[*,*,i] = thismask
	endfor
	PLOOP, maskimg
endif

if KEYWORD_SET(pstop) then STOP

RETURN, result

end