pro IRIS_PREP_READ_GEOWAVE, verbose = verbose

;+
;
; Read in all the geometric/wavelength correction data and store them 
; in a common block
;
;-

if KEYWORD_SET(verbose) then begin
	t0 = SYSTIME(/sec)
	PRINT, 'IRIS_PREP_READ_GEOWAVE: Reading all IRIS distortion maps...'
endif else verbose = 0

common IRIS_GEOWAVE_CB, geowave_dat

sswdata = CONCAT_DIR('$SSW_IRIS','data')
CD, sswdata, current = old_dir

searchstr = '*geowave_map*.genx'
geowavefile = FILE_SEARCH(searchstr)
geowave_tai = ANYTIM2TAI(STRMID(geowavefile, 0, 15))
latestfile = WHERE(geowave_tai eq MAX(geowave_tai))

RESTGEN, file = geowavefile[latestfile], geowave_dat
CD, old_dir

if KEYWORD_SET(verbose) then begin
	dt = SYSTIME(/sec) - t0
	PRINT, 'IRIS_PREP_READ_GEOWAVE: Distortion maps loaded in ' + $
		STRING(dt, form = '(f6.2)') + ' seconds'	
endif

end
