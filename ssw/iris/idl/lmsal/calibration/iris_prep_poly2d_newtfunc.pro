function IRIS_PREP_POLY2D_NEWTFUNC, incoords

;+
;
; Takes in a set of coordinates [x,y] and returns a new set [x',y'] showing
; where X and Y would map to in an IRIS image that has been warped using
; IRIS_PREP_GEOWAVE_CORRECT. The parameters for the warping are assumed to
; be stored in a common block.
;
; Used by NEWTON to invert the operation of POLY_2D. Is that really the
; best way to do this?
;
;-

; the warping parameters are stored in a common block
common IRIS_PREP_GEOWAVE_CB, params, target_x, target_y
param_tags = TAG_NAMES(params)
kxind = WHERE(param_tags eq 'KX', isnuv)

xin = incoords[0]
yin = incoords[1]

isfuvl = 0
if not isnuv then begin
	if target_x gt 2072 then begin
		kx = params.fuvl.kx
		ky = params.fuvl.ky
		isfuvl = 1
		xin = xin - 2072
	endif else begin
		kx = params.fuvs.kx
		ky = params.fuvs.ky
	endelse
endif else begin
	kx = params.kx
	ky = params.ky
endelse
kxsize = SIZE(kx)
kysize = SIZE(ky)

xarr = yin^DINDGEN(kxsize[1]) # xin^DINDGEN(kxsize[2])

xout = TOTAL(xarr * kx) - target_x
yout = TOTAL(xarr * ky) - target_y

if isfuvl then xout = xout + 2072

RETURN, [xout, yout]

end