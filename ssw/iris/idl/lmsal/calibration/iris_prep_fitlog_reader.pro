function IRIS_PREP_FITLOG_READER, tstart, tstop, $
	inpath = inpath, outpath = outpath, file = file, $
	pstop = pstop, write = write, loud = loud, $
	fid = fid, wave = wave, aia = aia, flat = flat

;+
;
; Reads the text files written by {IRIS_PREP_RUN_BATCH, /measure_*} and returns
; a structure array of best-fit results for each image
;
; RETURNS: 
;	fitlog	-	A structure array containing the results of the pipeline 
;				fitter with one element (structure) per image
;
; INPUTS:
;	tstart	-	Starting time; if left off, then starts at the beginning of the mission
;	tstop	-	Stop time; if left off, then stops at the end of the mission
;
; KEYWORDS:
;	file=	-	If set, then read in a GENX file previously written by this routine
;				instead of going through the txt files. Applies the tstart and 
;				tstop filters, but ignores /write and outpath
;	inpath	-	Top path where the pipeline is writing the fit result text files
;				Defaults to inpath = '/irisa/data/prep/'
;	outpath	-	Path where the GENX file should be written if the /write keyword is set
;				Defaults to outpath = '~/data/iris/trending/XXX/' where XXX is the
;				type string
;	/write	-	Write a GENX file with the resulting structure array to the outpath
;				Filename convention is tstart_tstop_trun_XXXfits.genx where the
;				start, stop and runtimes are in YYYYMMDD_HHMMSS format (so it's
;				a long filename)
;	/pstop	-	STOP for debugging
;	/loud	-	PRINT out some info like run time
;
; 	type keywords:
;		/fid, /wave, /aia
;
;-

tt0 = SYSTIME(/sec)
tnow = TIME2FILE(RELTIME(/now), /sec)
dpos = 0

; Parse the type keywords to tell whether you're looking at fiducials, wavelength, etc.
case 1 of
	KEYWORD_SET(fid)		:	begin
		wave = 0
		aia = 0
		pathtag = 'fid'
		extension = 'txt'
		ncol = 16
	end
	KEYWORD_SET(wave)	:	begin
		fid = 0
		aia = 0
		pathtag = 'wavecorr'
		extension = 'txt'
		ncol = 15
	end
	KEYWORD_SET(aia)	:	begin
		fid = 0
		wave = 0
		pathtag = 'aiacorr'
		extension = 'dat'
		ncol = 16
		dpos = 14
	end
	else	:	begin
		fid = 1
		pathtag = 'fid'
		extension = 'txt'
		ncol = 16
		wave = 0
		aia = 0
	end
endcase


; Use a GENX file written by a previous run of this routine; returns prior to 
; exiting this code block
if N_ELEMENTS(file) ne 0 then begin
	if N_ELEMENTS(inpath) ne 0 then genxfile = CONCAT_DIR(inpath, file) else genxfile = file
	RESTGEN, file = genxfile, fitlog
	if N_ELEMENTS(tstart) gt 0 then begin
		taiobs = ANYTIM2TAI(fitlog.t_obs)
		taistart = ANYTIM2TAI(tstart)
		goodimg = WHERE(taiobs ge taistart, numgood)
		if numgood gt 0 then begin
			fitlog = fitlog[goodimg]
			taiobs = taiobs[goodimg]
		endif else begin
			BOX_MESSAGE, ['No files after tstart', tstart, 'Starting at beginning']
		endelse
	endif
	if N_ELEMENTS(tstop) gt 0 then begin
		if N_ELEMENTS(taiobs) eq 0 then taiobs = ANYTIM2TAI(fitlog.t_obs)
		taistop = ANYTIM2TAI(tstop)
		goodimg = WHERE(taiobs le taistop, numgood)
		if numgood gt 0 then begin
			fitlog = fitlog[goodimg]
		endif else begin
			BOX_MESSAGE, ['No files before tstop', tstop, 'Going to the end']
		endelse
	endif
	if KEYWORD_SET(loud) then begin
		BOX_MESSAGE, ['IRIS_PREP_FITLOG_READER read logdat', $
			' Elapsed time : ', STRING(SYSTIME(/sec) - tt0, form = '(f10.1)') ]
	endif
	if KEYWORD_SET(pstop) then STOP
	RETURN, fitlog
endif

; set up default paths
if N_ELEMENTS(inpath) eq 0 then inpath = '/irisa/data/prep/'
if not KEYWORD_SET(outpath) then outpath = '~/data/iris/trending/' + pathtag + '/'

; Find all the files written by IRIS_PREP_RUN_BATCH
CD, inpath, current = old_dir
if N_ELEMENTS(tstart) gt 0 and N_ELEMENTS(tstop) gt 0 then begin
	if KEYWORD_SET(flat) then begin
		ff = FILE_SEARCH('/irisa/data/prep/' + pathtag + '/*' + extension)
		ttags = STRMID(FILE_BASENAME(ff), dpos, 8)
	endif else begin
		pathlist = SSW_TIME2FILELIST(tstart, tstop, parent = inpath, pattern='*description.genx')
		if STRLEN(pathlist[0]) eq 0 then begin
			BOX_MESSAGE, ['IRIS_PREP_FITLOG_READER', 'Could not find a file in the time range', $
				'Returning...']
			CD, old_dir
			RETURN, ''
		endif
		pathlist = FILE_DIRNAME(pathlist)
		ff = FILE_SEARCH(CONCAT_DIR(pathlist, pathtag + '/*.' + extension))
		ttags = STRMID(ff, 11 + STRLEN(inpath), 15)
	endelse
endif else begin
	ff = FILE_SEARCH('*/*/*/*/' + pathtag + '/*.' + extension)
	ttags = STRMID(ff, 11, 15)
endelse

taiff = ANYTIM2TAI(FILE2TIME(ttags))

; Filter to files between tstart and tstop
if N_ELEMENTS(tstart) gt 0 then begin
	taistart = ANYTIM2TAI(tstart)
	if KEYWORD_SET(flat) then begin
		goodff = WHERE((taiff + 86400d) ge taistart, numgood)
	endif else begin
		goodff = WHERE(taiff ge taistart, numgood)
	endelse
	if numgood gt 0 then begin
		ff = ff[goodff]
		ttags = ttags[goodff]
		taiff = taiff[goodff]
		utstart = TIME2FILE(tstart, /sec)
	endif else begin
		BOX_MESSAGE, ['No files after tstart', tstart, 'Starting at beginning']
		utstart = '20130701_000000'
	endelse
endif else begin
	utstart = '20130701_000000'
endelse
if N_ELEMENTS(tstop) gt 0 then begin
	taistop = ANYTIM2TAI(tstop)
	goodff = WHERE(taiff le taistop, numgood)
	if numgood gt 0 then begin
		ff = ff[goodff]
		ttags = ttags[goodff]
		taiff = taiff[goodff]
		utstop = TIME2FILE(tstop, /sec)
	endif else begin
		BOX_MESSAGE, ['No files before tstop', tstart, 'Going until the end']
		utstop = '20440701_000000'
	endelse
endif else begin
	utstop = '20440701_000000'
endelse
numf = N_ELEMENTS(ff)

if KEYWORD_SET(pstop) then STOP

if numf eq 0 then STOP
fitlog = IRIS_PREP_TXT2STR(ff, fid = fid, wave = wave, aia = aia, /dedup)

; Additional filter step to only look at lines in the file within the time range
if KEYWORD_SET(flat) then begin
	tailog = ANYTIM2TAI(fitlog.t_obs)
	goodlog = WHERE((tailog+30) ge taistart and (tailog-30) le taistop, numgood)
	if numgood lt 1 then begin
		BOX_MESSAGE, ['IRIS_PREP_FITLOG_READER', 'No analyzed lines in the time range', $
			'Returning...']
		CD, old_dir
		RETURN, ''
	endif
	fitlog = fitlog[goodlog]
endif

;++++++++++++++++++++++++++++++++++++++++++++++
; Write a genx file with all the per-image
; fiducial positions
;----------------------------------------------
if KEYWORD_SET(write) then begin
	filename = utstart + '_' + utstop + '_' + tnow + '_' + pathtag + 'fits.genx'
	outfile = CONCAT_DIR(outpath, filename)
	PRINT & PRINT, 'IRIS_PREP_FITLOG_READER writing trend data to log file'
	PRINT, outfile & PRINT
	SAVEGEN, file = outfile, fitlog
endif

if KEYWORD_SET(loud) then begin
	BOX_MESSAGE, ['IRIS_PREP_FITLOG_READER read logdat', $
		' Elapsed time : ', STRING(SYSTIME(/sec) - tt0, form = '(f10.1)') ]
endif

if KEYWORD_SET(pstop) then STOP

CD, old_dir
RETURN, fitlog

end