Pro iris_focus_mosaic,files, nmosaic, outdir=outdir, zbuffer=zbuffer, fmean=fmean, fmedian=fmedian
;
;+
;   Name: iris_focus_mosaic 
;
;   Purpose: Create iris_focus_mosaic plots
;
;   Input Parameters:
;     files   - 
;     nmosaic -
;   Keyword Parameters:
;     outdir=outdir
;
;   Output:
;
;   Calling Examples:
;
;   History:
;
;      Original: Ted Tarbell
;      08-feb-2016 - P.G.Shirts - added header, zbuffer keyword, small cosmetic changes.
;
;-

break_file,files(0),dd,dir,fil,ext

if not keyword_set(outdir) then outdir = dir
outfile = outdir+'/IRIS_focus'
read_iris,files(0),index,/nodata

case nmosaic of
   2:  cen = [250, 500, 400]
   3:  cen = [220, 305, 300]
   4:  cen = [200, 200, 250]
   5:  cen = [150, 180, 220]
   10: cen = [ 80, 100, 120]
   else:  return
endcase

oldDevice = !D.NAME
set_plot, 'Z'

mres = fltarr(nmosaic,nmosaic)
x0 = cen(0)
dx = cen(1)
npts = cen(2)

for i=0,nmosaic-1 do begin
  for j=0,nmosaic-1 do begin
    xc = x0+i*dx
    if (xc gt 930) then xc = 930
    yc = x0+j*dx
    outf = outfile
    if keyword_set(zbuffer) then begin
      res = iris_focus_astig_gfit(files,xc=xc,yc=yc,npts=npts,outfile=outf,/zbuff,/verbose)
    endif else begin
      res = iris_focus_astig_gfit(files,xc=xc,yc=yc,npts=npts,outfile=outf,/verbose)
    endelse 
    mres(i,j) = median(res(*,0),/even)
  end
end

if keyword_set(zbuffer) then begin
  wdef,0,800,850,/zbuffer
endif else begin
  wdef,0,800,850
endelse
!p.charsize = 1.5
tit = 'IRIS FOCUS '+fil
fmean = mean(mres)
fmedian = median(mres,/even)
fstdev = stdev(mres)
fmad = median(abs(mres-fmedian),/even)
xtit = 'Mean, Median, StDev, MAD '+string([fmean,fmedian,fstdev,fmad],form='(2f8.1,2f6.1)')
plot_image,replicate(0b,1000,1056),title=tit,xtit=xtit

!p.charsize = 1.5
if (nmosaic gt 5) then !p.charsize = 1.1
for i=0,nmosaic-1 do begin
  for j=0,nmosaic-1 do begin
    xc = x0+i*dx
    if (xc gt 930) then xc = 930
    yc = x0+j*dx
    xyouts,xc-80,yc-10,string(mres(i,j),form='(f7.1)')
  end
end

!p.charsize = 1.
tvlct,rct,gct,bct, /get

big = 255b-tvrd()
outfile = outfile+'_mosaic_'+strmid(index.img_path,4)
write_gif,outfile+'.gif',big,rct,gct,bct
;print,'Saved plots in ',outfile+'.gif'

set_plot, oldDevice
return
end
