function IRIS_PREP_FILTER_WAVE, wavelog, $
	chisqlim = chisqlim, outlier = outlier, goodfrac = goodfrac, $
	win = win, loud = loud, outfile = outfile, outpath = outpath, $
	typethresh = typethresh, runtime = runtime, png = png, pstop = pstop

;+
;
; Fit a sine wave to the measured neutral line positions of all the images in an OBS, 
; with separate fits for each channel and line position (top, middle or bottom) as well
; as "combo" fits that roll together all the variations that we expect to be 
; perfectly correlated (i.e. there are six separate fits for the top, middle and bottom
; of the FUVS and FUVL, and one combo fit that rolls them all together with optimized offsets).
;
; RETURNS:
;	output - A structure with the following fields:
;			.tstart		-	A string time code for the start of the OBS
;			.chans		-	A string vector listing the channel names
;			.posnames	-	A string vector listing the names of the line positions
;			.paramnames	-	A string vector listing the names of the fit parameters
;			.numfit		-	A long [nchan,npos] array giving the number of images
;							used in the fit for each channel and region
;			.chisqs		-	A double [nchan,npos] array with the chi-squared of the
;							fit for each channel and region
;			.paramvals	-	A double [nchan, npos, nparam] array with the parameters
;							of the best fit for each channel and region
;			.combo_chans	-	A string array with the names of the "combo" channels
;								(those where 2 channels vary together, e.g. FUV 
;								is a combo of the two FUV channels)
;			.combo_rangenames	-	A string array with the names of the "combo" positions
;			.combo_numfit	-	An [ncchan, ncpos] array with the number of images fit
;			.combo_chisqs	-	An [ncchan, ncpos] array with the fit chisquareds
;			.combo_paramvals-	An [ncchan, ncpos, ncparams] array with the best
;								fit parameters
;			.combo_flag		-	An [nchan, ncpos, 6] byte array indicating whether
;								that combo sub-element was used in the fit
;			.combo_tag		-	An [ncchan, ncfid, 6] array with the string tag
;								for each sub-element of the combo
;
; INPUTS:
;	wavelog	-	a structure array containing the measured wavelength positions 
;				(one structure element per image); can use IRIS_PREP_FITLOG_READER
;				to get this, although it should be cut up by OBS
;
; KEYWORDS:
;	chisqlim= -	Can be set to specify the maximum chi-squared value for a good
;				fit. Defaults to 800.
;	outlier= - 	Can be set to specify the number of pixels away from the median
;				for the OBS before an image is considered an outlier and ignored.
;				Defaults to 5.
;	goodfrac= -	Can be set to specify the fraction of the images in the input
;				set that must be good in order to try fitting a sine wave; if
;				fewer are good, then a constant is used (the median value)
;				Defaults to 0.4
;	typethresh= -	Can be set to specify the ratio of the number of images of a 
;				given type to the number of images of the most common type in that
; 				combo before the type is ignored. Uhh...just look in the code.
;				Defaults to 5
;
;	/loud	-	Set to print summary info about the fit, and show plots. 
;				(Can optionally set to 2 for VERBOSE mode...)
;	/runtime - 	If set, then the elapsed time is printed when it finishes
;	/png	-	If set, then png files showing the fit results are saved to the outpath
;	/pstop	-	Brake for debugging
;	outpath= -	Can be set to a path where the PNG file of the fits are written
;				if png is set. Defaults to '~/data/iris/trending/wavecorr/'
;	outfile= -	If set, then a genx file with the fit results is written. Should
;				be the full path and filename; doesn't include outpath
;	win 	-	Can set to a window index to be used for all plots if /loud or 
;				/png is set; defaults to 10
;
; HISTORY : 
;           2017-04-24: JPW - changed NUMPOINTS limit from  GT 20  to  GT 16
;
;-

tt0 = SYSTIME(/sec)

; set up defaults
if not KEYWORD_SET(outpath) then outpath = '~/data/iris/trending/wavecorr/'
if N_ELEMENTS(chisqlim) eq 0 then chisqlim = 800
if N_ELEMENTS(outlier) eq 0 then outlier = 5		;	pixels from median to exclude
if N_ELEMENTS(goodfrac) eq 0 then goodfrac = 0.4
if N_ELEMENTS(typethresh) eq 0 then typethresh = 5
numimg = N_ELEMENTS(wavelog)
if N_ELEMENTS(pstop) eq 0 then pstop = 0

; Set up the parameter structure for using MPFITFUN to fit a sine wave
numparams = 4
params = ['Amplitude', 'Frequency', 'Phase', 'Offset']
values = [-0.3d, (2 * !pi) / 5856d, 1d, 100d]
fixed = [0, 1, 0, 0]
limited = [0, 0, 0, 0]
limits = [1, 1, 1, 1]

parinfo = REPLICATE({name:params[0], value:values[0], fixed:fixed[0], $
	limited:[1,1]*limited[0], limits:[-1d,1d]*limits[0]}, numparams)
for i = 1, numparams-1 do begin
	parinfo[i].name 	= params[i]
	parinfo[i].value 	= values[i]
	parinfo[i].fixed 	= fixed[i]
	parinfo[i].limited 	= limited[i] * [1,1]
	parinfo[i].limits 	= limits[i] * [-1d, 1d]
endfor
initpars = parinfo

; Set up the result structure
chans = ['NUV', 'FUVS', 'FUVL']
numchan = N_ELEMENTS(chans)
rangenames = ['BOT', 'MID', 'TOP']
numpos = N_ELEMENTS(rangenames)
numfit = LONARR(numchan, numpos)
chisqs = DBLARR(numchan, numpos)
paramvals = DBLARR(numchan, numpos, numparams)

; Set up to keep track of combined fits
combo_chans = ['NUV', 'FUV']
combo_subchan = [['NUV', 'NUV'], ['FUVS', 'FUVL']]
combo_flag = BYTARR(2, 6)		;	
combo_tag = STRARR(2, 6)		;	
combo_chisqs = DBLARR(2)
combo_numfit = LONARR(2)
combo_paramvals = DBLARR(2,9)	;	6 for NUV, 9 for FUV

tstart = wavelog[0].t_obs
result = CREATE_STRUCT('tstart', tstart, 'chans', chans, $
	'rangenames', rangenames, 'paramnames', params, $
	'numfit', numfit, 'chisqs', chisqs, 'paramvals', paramvals, $
	'combo_chans', combo_chans, 'combo_chisqs', combo_chisqs, $
	'combo_numfit', combo_numfit, $
	'combo_paramvals', combo_paramvals, 'combo_tag', combo_tag, $
	'combo_flag', combo_flag)
logtags = TAG_NAMES(wavelog)
bottag = WHERE(logtags eq 'LINE_BOT')

if pstop gt 1 then STOP

; Set up plotting
loud = KEYWORD_SET(loud)
png = KEYWORD_SET(png)
if (loud or png) then begin
	if not KEYWORD_SET(win) then win = 10
	usez = GET_LOGENV('IDL_BATCH_RUN') ne ''
	WDEF, win, 1000, 800, zbuffer = usez
	TVLCT, rr, gg, bb, /get
	PB_SET_LINE_COLOR
	oldpmulti = !p.multi
	!p.multi = 0
	if loud then begin
		PRINT
		PRINT, 'Chan', 'Pos', 'Num', 'Chisq', 'Amp', 'Freq', 'Phase', $
			'Off', form = '(a10, a10, a8, 5a10)'
	endif
endif

; Loop through channels for this OBS
for j = 0, numchan - 1 do begin
	thischan = chans[j]
	index = WHERE(wavelog.img_path eq thischan, numin)
	if loud gt 1 then begin
		PRINT, 'Channel: ', chans[j], form = '(a20, a12)'
		PRINT, '# Images: ', numin, form = '(a20, i12)'
	endif
	
	; Loop through the 3 ranges for each channel
	for k = 0, numpos - 1 do begin
		thispos = rangenames[k]
		thistag = thischan + '_' + thispos
		if numin gt 0 then begin
			gooddat = WHERE(wavelog[index].(bottag+k) gt 0 and $		;	This value found
				wavelog[index].(bottag+k+3) lt chisqlim, numgood)			;	Good fit on this value
			if loud gt 1 then PRINT, '# Good images: ', numgood, form = '(a20, i12)'
			if numgood gt 0 then begin
			
				; Check for outliers
				if numgood eq 1 then thismed = wavelog[index[gooddat]].(bottag+k) else $
					thismed = MEDIAN(wavelog[index[gooddat]].(bottag+k))
				notoutlier = WHERE(ABS(wavelog[index[gooddat]].(bottag+k) - thismed) lt outlier, numinlier)
				gooddat = gooddat[notoutlier]
				numgood = numinlier
				fitpoints = wavelog[index[gooddat]]
				parinfo	= initpars
				tgrid	= ANYTIM2TAI(fitpoints.t_obs)
				
				;	Adjust the wavelength position by sumsptrl where appropriate
				fdata	= fitpoints.(bottag+k) * fitpoints.sumsptrl
				err		= 0.5 + fitpoints.(bottag+k+3)/50.
				
				; If not enough images to trace a sine wave, just fit a constant
				if (numgood lt 10) or (numgood lt (numin * goodfrac)) then begin
					if loud gt 1 then PRINT, 'Fitting only a constant...'
					parinfo[0].value = 0d
					parinfo[0].fixed = 1
					parinfo[2].fixed = 1
				endif
				
				; Do the fit, and calculate the residuals
				fsineparams = MPFITFUN('IRIS_MP_SINECHISQ', tgrid, fdata, err, $
					parinfo = parinfo, yfit = sinefit, quiet=1 )	;	quiet = (1-loud)
				dev = ABS(fdata - sinefit)
					
				; Remove degeneracy by enforcing positive amplitude, phase between 0 and 2pi
				if fsineparams[0] lt 0 then begin
					fsineparams[0] = 0 - fsineparams[0]
					fsineparams[2] = fsineparams[2] + !pi
				endif
				minphase = FLOOR(fsineparams[2] / (2 * !pi)) * (2 * !pi)
				fsineparams[2] = (fsineparams[2] - minphase) mod (2 * !pi)
				
				; Plot the data and the fit
				if (loud or png) and numgood gt 1 then begin
					plotname = TIME2FILE(tstart, /sec) + '_' + thistag
					UTPLOT, TAI2UTC(tgrid), fdata, psym = 4, chars = 1.5, $
						ytitle = 'Wavelength shift [pix]', title = plotname, $
						yrange = thismed * fitpoints.sumsptrl + [-5,5], /ystyle
					OUTPLOT, TAI2UTC(tgrid), sinefit, col = 2
					if png then PB_WIN2PNG, CONCAT_DIR(outpath, plotname) + '.png'
					if loud then PRINT, thischan, thispos, numgood, MEAN(DOUBLE(dev)), $
						fsineparams, form = '(a10, a10, i8, f10.3, f10.3, f10.6, f10.3, f10.1)'
				endif
				
			endif else begin
				fsineparams = DBLARR(4)
				dev = 0d
			endelse
		endif else begin
			fsineparams = DBLARR(4)
			dev = 0d
			numgood = 0l
		endelse
		
		result.numfit[j, k] = numgood
		result.chisqs[j, k] = MEAN(DOUBLE(dev))
		result.paramvals[j, k, *] = DOUBLE(fsineparams)
	
	if pstop gt 1 then STOP
			
	endfor	;	Done with position loop
	
endfor	;	Done with channel loop


; "Combo" fitting means doing a single fit for each spectral channel, combining
; the 3 spatial ranges on the NUV and the 3 spatial ranges AND 2 wavelength bands
; on the FUV

if loud then PRINT
initpars = [initpars, REPLICATE(initpars[3], 5)]
ccolors = [2, 5, 6, 7, 8, 9]
; Loop through combo channels for this OBS
for j = 0, 1 do begin
	thischan = combo_chans[j]
	thistag = thischan + '_ALL'
	
	all_tgrid = 0d
	all_fdata = 0d
	all_err = 0d
	all_type = 0
	typetag = STRARR(6)
	; Loop through the sub-offset types and gather the data
	if j eq 1 then numoffset = 6 else numoffset = 3
	for i = 0, numoffset - 1 do begin
		typetag[i] = combo_subchan[i/3,j] + '_' + STRMID(logtags[bottag + (i mod 3)], 5, 3)
		index = WHERE(wavelog.img_path eq combo_subchan[i/3,j], numin)
		thisdat = wavelog[index].(bottag + (i mod 3))
		thischisq = wavelog[index].(bottag + (i mod 3) + 3)
		gooddat = WHERE(thisdat gt 0 and thischisq lt chisqlim, numgood)			;	Good fit on this value
		if loud gt 1 then PRINT, '# Good images: ', numgood, form = '(a20, i12)'
		if numgood gt 0 then begin
		
			; Check for outliers
			if numgood eq 1 then thismed = thisdat[gooddat] else $
				thismed = MEDIAN(thisdat[gooddat])
			notoutlier = WHERE(ABS(thisdat[gooddat] - thismed) lt outlier, numinlier)
			gooddat = gooddat[notoutlier]
			numgood = numinlier
			fitpoints = wavelog[index[gooddat]]
			parinfo	= initpars
			tgrid	= ANYTIM2TAI(fitpoints.t_obs)
			
			;	Adjust the fiducial position by sumsptrl where appropriate
			fdata	= thisdat[gooddat] * fitpoints.sumsptrl
		
			err		= 0.5 + thischisq[gooddat]/50.
			all_tgrid = [all_tgrid, tgrid]
			all_fdata = [all_fdata, fdata]
			all_err = [all_err, err]
			all_type = [all_type, i + BYTARR(numgood)]
		endif
	endfor
	result.combo_tag[j,*] = typetag
	
	; Now that the data are gathered, decide which should be passed to the 
	; fitter. 
	numpoints = N_ELEMENTS(all_tgrid) - 1
	if numpoints gt 0 then begin
		all_tgrid = all_tgrid[1:*]
		all_fdata = all_fdata[1:*]
		all_err = all_err[1:*]
		all_type = all_type[1:*]
		typehist = HISTOGRAM(all_type, min = 0, max = 3)
		maxtype = MAX(typehist)
		parinfo = initpars
		if numpoints gt 16 and maxtype ge 10 then begin
			keeptype = WHERE(typehist ge maxtype / typethresh, comp=notype)
			result.combo_flag[j, keeptype] = 1
			fitind = WHERE(typehist[all_type] ge maxtype / 3, numfit)
			tgrid = [[all_tgrid[fitind]], [all_type[fitind]]]
			fdata = all_fdata[fitind]
			err = all_err[fitind]
			all_type = all_type[fitind]
			parinfo[3:*].fixed = 1
			parinfo[3+keeptype].fixed = 0
			parinfo[3:5].value = REFORM(result.paramvals[j,*,3])
			if j eq 1 then parinfo[6:8].value = REFORM(result.paramvals[2,*,3])
			; Do the fit, and calculate the residuals
			fsineparams = MPFITFUN('IRIS_MP_COMBO_SINECHISQ', tgrid, fdata, err, $
				parinfo = parinfo, yfit = sinefit, quiet=1 )	;	quiet = (1-loud)
			dev = ABS(fdata - sinefit)
			
;				; Remove degeneracy by enforcing positive amplitude, phase between 0 and 2pi
			if fsineparams[0] lt 0 then begin
				fsineparams[0] = 0 - fsineparams[0]
				fsineparams[2] = fsineparams[2] + !pi
			endif
			minphase = FLOOR(fsineparams[2] / (2 * !pi)) * (2 * !pi)
			fsineparams[2] = (fsineparams[2] - minphase) mod (2 * !pi)
			
			; Plot results, with different offsets applied
			shiftdata = fdata - fsineparams[3+all_type]
			shiftsine = sinefit - fsineparams[3+all_type]
			plotname = TIME2FILE(tstart, /sec) + '_' + thistag
			if (loud or png) and numpoints gt 1 then begin
				UTPLOT, TAI2UTC(tgrid[*,0]), shiftdata, psym = 4, $
					yrange = [-4, 4], chars = 1.5, title = plotname, $
					ytit = 'Wavelength shift [pix]', /xstyle, /ystyle, yminor=4
				for i = 0, numoffset - 1 do begin
					thisind = WHERE(all_type eq i, numthis)
					if numthis gt 0 then OUTPLOT, TAI2UTC(tgrid[thisind]), $
						shiftdata[thisind], col = ccolors[i], psym = 4
				endfor
				tsort = SORT(tgrid[*,0])
				OUTPLOT, TAI2UTC(tgrid[tsort,0]), shiftsine[tsort], thick = 2
				IMD_LEGEND, pos = 10, char = 2, col = ccolors[0:numoffset-1], $
					typetag + STRING(fsineparams[3:3+numoffset-1], form = '(f9.3)')
				if png then PB_WIN2PNG, CONCAT_DIR(outpath, plotname) + '.png'
			endif
		endif else begin
			fsineparams = DBLARR(9)
			dev = 0d
			numpoints = 0
		endelse
	endif else begin
		fsineparams = DBLARR(9)
		dev = 0d
		numpoints = 0
	endelse
		
	result.combo_numfit[j] = numpoints
	result.combo_chisqs[j] = MEAN(DOUBLE(dev))
	result.combo_paramvals[j,*] = DOUBLE(fsineparams)

	if loud then PRINT, thischan, 'ALL', numpoints, MEAN(DOUBLE(dev)), fsineparams[0:numoffset+2], $
		form = '(a10, a10, i8, f10.3, f10.3, f10.6, f10.3, ' + STRTRIM(numoffset, 2) + 'f10.1)'

	if pstop gt 0 then STOP
		
endfor	;	Done with combo channel loop
	
; Clean up from plotting
if (loud or png) then begin
	TVLCT, rr, gg, bb
	!p.multi = oldpmulti
endif

if KEYWORD_SET(outfile) then SAVEGEN, result, file = outfile

if KEYWORD_SET(runtime) then PRINT, 'IRIS_PREP_FILTER_WAVE: Elapsed time = ', $
	SYSTIME(/sec) - tt0

RETURN, result

end
