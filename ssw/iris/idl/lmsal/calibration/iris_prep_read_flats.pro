pro IRIS_PREP_READ_FLATS, iindex, $
	recnum = recnum, verbose = verbose, clear = clear

;+
;
; Given the header of an L1 image, this routine loads the appropriate flatfield
; for that IMG_PATH and T_OBS into a common block so that IRIS_PREP_FLAT can
; apply it.
;
; If you want to re-initialize the common block, set the /clear keyword
;
;-

common IRIS_PREP_FLAT_CB, flat_index, flat_loaded, flat_sji_2832, flat_sji_2796, $
	flat_sji_5000w, flat_sji_1330, flat_sji_1400, flat_sji_1600w, $
	flat_fuv_detector, flat_fuv_spatial, flat_nuv_detector, flat_nuv_spatial, $
	flat_sji_nuv

; Set up some constants
t0 = SYSTIME(/sec)
imgpaths = ['SJI_5000W', 'SJI_2832', 'SJI_2796', $
	'SJI_1600W', 'SJI_1400', 'SJI_1330', $
	'FUV_DETECTOR', 'FUV_SPATIAL', $
	'NUV_DETECTOR', 'NUV_SPATIAL', 'SJI_NUV']
npaths = N_ELEMENTS(imgpaths)
sswdata = CONCAT_DIR('$SSW_IRIS','data')
if KEYWORD_SET(clear) then begin
	flat_index = 0
	flat_loaded = 0
endif

; Read in the index of all available flats if necessary
; There should be only one index, but in case there are multiples it uses the
; one with the most recent date in its filename
if N_TAGS(flat_index) eq 0 then begin
	flatinds = FILE_SEARCH(CONCAT_DIR(sswdata, '*flat.genx'))
	flatind_tai = ANYTIM2TAI(FILE2TIME(STRMID(FILE_BASENAME(flatinds), 0, 15)))
	latest = WHERE(flatind_tai eq MAX(flatind_tai))
	RESTGEN, file = flatinds[latest], flat_index
	if KEYWORD_SET(verbose) then PRINT, 'IRIS_PREP_READ_FLATS: Reading flat index...'
endif

; Set up the list of which flats have been loaded (a structure
; where the tag names are the path names, and the values are long
; ints specifying the recnum currently loaded for that path
if N_TAGS(flat_loaded) eq 0 then begin
	flat_loaded = CREATE_STRUCT(imgpaths[0], 0l)
	for i = 1, npaths - 1 do flat_loaded = CREATE_STRUCT(flat_loaded, imgpaths[i], 0l)
endif

; Just bail out at this point if the header of a particular image was not passed in
if N_TAGS(iindex) eq 0 then begin
	if KEYWORD_SET(verbose) then PRINT, 'IRIS_PREP_READ_FLATS: No index, returning...'
	RETURN
endif

; Handle SJIs and spectra differently; spectra have a spatial component and a 
; detector component, while SJIs are just a single file
imgpath = iindex[0].img_path
if STRMID(imgpath, 0, 3) eq 'SJI' then begin
	flattype = ''
endif else begin
	flattype = ['_DETECTOR', '_SPATIAL']
endelse

for i = 0, N_ELEMENTS(flattype) - 1 do begin

	; Check that you have a good img_path in this image
	thispath = imgpath + flattype[i]
	imgind = WHERE(thispath eq imgpaths)
	indexind = WHERE(thispath eq flat_index.img_path)
	if (imgind[0] eq -1) or (indexind[0] eq -1) then begin
		PRINT, 'IRIS_PREP_READ_FLATS: Funny imgpath...'
		STOP
	endif

	; Check the list of available flats (the flat_index) to decide which one
	; should be used for this image
	taiobs = MIN(ANYTIM2TAI(iindex.t_obs))
	thispath_index = flat_index[indexind]
	taidiff = ABS(taiobs - thispath_index.filetai)
	index_addr = WHERE(taidiff eq MIN(taidiff))
	recnum_needed = thispath_index[index_addr[0]].recnum

	; If you have that one loaded, you're done; if not, then you need to load it
	if flat_loaded.(imgind) ne recnum_needed then begin
		if KEYWORD_SET(verbose) then BOX_MESSAGE, ['IRIS_PREP_READ_FLATS: Reading ', $
			STRTRIM(recnum_needed, 2) + ' for ' + thispath]
		flatfile = CONCAT_DIR(sswdata, thispath_index[index_addr].filename)
		MREADFITS, flatfile, hdr, dat
		setstring = 'flat_' + thispath + ' = dat'
		if KEYWORD_SET(verbose) then PRINT, setstring
		dummy = EXECUTE(setstring)
		flat_loaded.(imgind) = recnum_needed
	endif else begin
		if KEYWORD_SET(verbose) then PRINT, 'IRIS_PREP_READ_FLATS: Already loaded'
	endelse

endfor

if KEYWORD_SET(verbose) then begin
	dt = SYSTIME(/sec) - t0
	PRINT, 'IRIS_PREP_READ_FLATS: Flatfields loaded in ' + STRING(dt, form = '(f6.2)') + ' seconds'	
endif

end