function IRIS_PREP_FIX_BADPIX_1330, bp, $
	loud = loud

;+
;
; Remove the extra bad pixels flagged in the SJI_1330 images. Takes in a
; bad pixel list and filters out anything with X between 475 and 597 and
; Y between 480 and 580. Filter to only SJI_2796 before passing in.
;
;
;-

bpx = bp mod 2072
bpy = bp / 2072

bpframe = BYTARR(2072,1096)
bpframe[bp] = 1

keepers = WHERE(bpx lt 475 or bpx gt 597 or bpy lt 480 or bpy gt 580, numkeep)
bpframe_out = BYTARR(2072,1096)
bpframe_out[bp[keepers]] = 1
bp_out = WHERE(bpframe_out eq 1)

if KEYWORD_SET(loud) then PLOOP, bpframe, bpframe_out

RETURN, LONG64(bp_out)

end