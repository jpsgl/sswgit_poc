function IRIS_PREP_FILTER_FID, fidlog, $
	chisqlim = chisqlim, outlier = outlier, goodfrac = goodfrac, $
	win = win, loud = loud, outfile = outfile, outpath = outpath, $
	runtime = runtime, png = png, eps = eps, pstop = pstop

;+
;
; Fit a sine wave to the measured fiducial positions of all the images in an OBS, 
; with separate fits for each channel and fiducial position (top or bottom) as well
; as "combo" fits that roll together all the variations that we expect to be 
; perfectly correlated (i.e. there are four separate fits for the X-axis variation
; of the top and bottom fiducials of the SJI_1330 and SJI_1400 channels, and one
; combo fit that rolls them all together with optimized offsets).
;
; RETURNS:
;	output - A structure with the following fields:
;			.tstart		-	A string time code for the start of the OBS
;			.chans		-	A string vector listing the channel names
;			.fidnames	-	A string vector listing the names of the fiducial coordinates
;			.paramnames	-	A string vector listing the names of the fit parameters
;			.numfit		-	A long [nchan,nfid] array giving the number of images
;							used in the fit for each channel and fiducial
;			.chisqs		-	A double [nchan,nfid] array with the chi-squared of the
;							fit for each channel and fiducial
;			.paramvals	-	A double [nchan, nfid, nparam] array with the parameters
;							of the best fit for each channel and fiducial
;			.combo_chans	-	A string array with the names of the "combo" channels
;								(those where 2 channels vary together, e.g. SJIF
;								is a combo of the two FUV SJI channels)
;			.combo_fidnames	-	A strong array with the names of the "combo" fiducials
;			.combo_numfit	-	An [ncchan, ncfid] array with the number of images fit
;			.combo_chisqs	-	An [ncchan, ncfid] array with the fit chisquareds
;			.combo_paramvals-	An [ncchan, ncfid, ncparams] array with the best
;								fit parameters
;			.combo_flag		-	An [nchan, ncfid, 4] byte array indicating whether
;								that combo sub-element was used in the fit
;			.combo_tag		-	An [ncchan, ncfid, 4] array with the string tag
;								for each sub-element of the combo
;
; INPUTS:
;	fidlog	-	a structure array containing the measured fiducial positions 
;				(one structure element per image); can use IRIS_PREP_FIDFIT_READER
;				to get this, although it should be cut up by OBS
;
; KEYWORDS:
;	chisqlim= -	Can be set to specify the maximum chi-squared value for a good
;				fit. Defaults to 80.
;	outlier= - 	Can be set to specify the number of pixels away from the median
;				for the OBS before an image is considered an outlier and ignored.
;				Defaults to 4.
;	goodfrac= -	Can be set to specify the fraction of the images in the input
;				set that must be good in order to try fitting a sine wave; if
;				fewer are good, then a constant is used (the median value)
;				Defaults to 0.8
;
;	/loud	-	Set to print summary info about the fit, and show plots. 
;				(Can optionally set to 2 for VERBOSE mode...)
;	/runtime - 	If set, then the elapsed time is printed when it finishes
;	/png	-	If set, then png files showing the fit results are saved to the outpath
;	/pstop	-	Brake for debugging
;	outpath= -	Can be set to a path where the PNG file of the fits are written
;				if png is set. Defaults to '~/data/iris/trending/fid/'
;	outfile= -	If set, then a genx file with the fit results is written. Should
;				be the full path and filename; doesn't include outpath
;	win= 	-	Can set to a window index to be used for all plots if /loud or 
;				/png is set; defaults to 10
;	/eps	-	Set if you want to make encapsulated postscript files for the plots
; 				(so that they don't require an active DISPLAY connection)
;
;-

tt0 = SYSTIME(/sec)

; set up defaults
if not KEYWORD_SET(outpath) then outpath = '~/data/iris/trending/fid/'
if N_ELEMENTS(chisqlim) eq 0 then chisqlim = 800
if N_ELEMENTS(outlier) eq 0 then outlier = 3		;	pixels from median to exclude
if N_ELEMENTS(goodfrac) eq 0 then goodfrac = 0.4
numimg = N_ELEMENTS(fidlog)
if N_ELEMENTS(pstop) eq 0 then pstop = 0

; Set up the parameter structure for using MPFITFUN to fit a sine wave
numparams = 4
params = ['Amplitude', 'Frequency', 'Phase', 'Offset']
values = [-0.3d, (2 * !pi) / 5856d, 1d, 100d]
fixed = [0, 1, 0, 0]
limited = [0, 0, 0, 0]
limits = [1, 1, 1, 1]

parinfo = REPLICATE({name:params[0], value:values[0], fixed:fixed[0], $
	limited:[1,1]*limited[0], limits:[-1d,1d]*limits[0]}, numparams)
for i = 1, numparams-1 do begin
	parinfo[i].name 	= params[i]
	parinfo[i].value 	= values[i]
	parinfo[i].fixed 	= fixed[i]
	parinfo[i].limited 	= limited[i] * [1,1]
	parinfo[i].limits 	= limits[i] * [-1d, 1d]
endfor
initpars = parinfo

; Set up the result structure
chans = ['SJI_1330', 'SJI_1400', 'SJI_2796', 'SJI_2832', 'NUV', 'FUVS', 'FUVL']
numchan = N_ELEMENTS(chans)
fidnames = ['X1', 'Y1', 'X2', 'Y2']
numfid = N_ELEMENTS(fidnames)
numfit = LONARR(numchan, numfid)
chisqs = DBLARR(numchan, numfid)
paramvals = DBLARR(numchan, numfid, numparams)

; Set up to keep track of combined fits
combo_chans = ['SJIF', 'SJIN', 'NUV', 'FUV']
combo_subchan = [['SJI_1330', 'SJI_1400'], ['SJI_2796', 'SJI_2832'], ['NUV', 'NUV'], ['FUVS', 'FUVL']]
combo_numchan = N_ELEMENTS(combo_chans)
combo_fid = ['X', 'Y']
combo_numfid = N_ELEMENTS(combo_fid)
combo_numfit = LONARR(combo_numchan, combo_numfid)
combo_chisqs = DBLARR(combo_numchan, combo_numfid)
combo_numparams = 7
combo_flag = BYTARR(combo_numchan, combo_numfid, 4)		;	
combo_tag = STRARR(combo_numchan, combo_numfid, 4)		;	
combo_paramvals = DBLARR(combo_numchan, combo_numfid, combo_numparams)

tstart = fidlog[0].t_obs
result = CREATE_STRUCT('tstart', tstart, 'chans', chans, $
	'fidnames', fidnames, 'paramnames', params, $
	'numfit', numfit, 'chisqs', chisqs, 'paramvals', paramvals, $
	'combo_chans', combo_chans, 'combo_fidnames', combo_fid, $
	'combo_chisqs', combo_chisqs, 'combo_numfit', combo_numfit, $
	'combo_paramvals', combo_paramvals, 'combo_tag', combo_tag, $
	'combo_flag', combo_flag)
logtags = TAG_NAMES(fidlog)
x1tag = WHERE(logtags eq 'X1')

if pstop gt 1 then STOP

; Set up plotting
loud = KEYWORD_SET(loud)
png = KEYWORD_SET(png)
eps = KEYWORD_SET(eps)
if png then begin
	if not KEYWORD_SET(win) then win = 10
	usez = GET_LOGENV('IDL_BATCH_RUN') ne ''
	WDEF, win, 1000, 800, zbuffer = usez
endif
if loud then begin
	PRINT
	PRINT, 'Chan', 'Fid', 'Num', 'Chisq', 'Amp', 'Freq', 'Phase', $
		'Off', form = '(a10, a10, a8, 5a10)'
endif
if eps then begin
	olddev = !d.name
	SET_PLOT, 'ps'
	DEVICE, /encap, /color 	
endif
if (loud or png or eps) then begin
	TVLCT, rr, gg, bb, /get
	PB_SET_LINE_COLOR
	oldpmulti = !p.multi
	!p.multi = 0
endif

; Loop through channels for this OBS
for j = 0, numchan - 1 do begin
	thischan = chans[j]
	index = WHERE(fidlog.type eq thischan, numin)
	if loud gt 1 then begin
		PRINT, 'Channel: ', chans[j], form = '(a20, a12)'
		PRINT, '# Images: ', numin, form = '(a20, i12)'
	endif
	
	; Loop through the 4 fiducial coordinates for each channel
	for k = 0, numfid - 1 do begin
		thisfid = fidnames[k]
		thistag = thischan + '_' + thisfid
		if numin gt 0 then begin
			gooddat = WHERE(fidlog[index].(x1tag+k) gt 0 and $		;	This value found
				fidlog[index].(x1tag+k+4) lt chisqlim, numgood)			;	Good fit on this value
			if loud gt 1 then PRINT, '# Good images: ', numgood, form = '(a20, i12)'
			if numgood gt 0 then begin
			
				; Check for outliers
				if numgood eq 1 then thismed = fidlog[index[gooddat]].(x1tag+k) else $
					thismed = MEDIAN(fidlog[index[gooddat]].(x1tag+k))
				notoutlier = WHERE(ABS(fidlog[index[gooddat]].(x1tag+k) - thismed) lt outlier, numinlier)
				gooddat = gooddat[notoutlier]
				numgood = numinlier
				fitpoints = fidlog[index[gooddat]]
				parinfo	= initpars
				tgrid	= ANYTIM2TAI(fitpoints.t_obs)
				
				;	Adjust the fiducial position by sumspat where appropriate
				sumspat = fitpoints.sumspat
				sumsptrl = fitpoints.sumsptrl
				;if (k mod 2 eq 1) or (j lt 4) then scaler = sumspat else scaler = sumspat * 0 + 1	
				if ( (k mod 2) eq 1 ) then scaler = sumspat else scaler = sumsptrl
				fdata	= fitpoints.(x1tag+k) * scaler
				err		= 0.5 + fitpoints.(x1tag+k+4)/50.
				
				; If not enough images to trace a sine wave, just fit a constant
				if (numgood lt 10) or (numgood lt (numin * goodfrac)) then begin
					if loud gt 1 then PRINT, 'Fitting only a constant...'
					parinfo[0].value = 0d
					parinfo[0].fixed = 1
					parinfo[2].fixed = 1
				endif
				
				; Do the fit, and calculate the residuals
				fsineparams = MPFITFUN('IRIS_MP_SINECHISQ', tgrid, fdata, err, $
					parinfo = parinfo, yfit = sinefit, quiet=1 )	;	quiet = (1-loud)
				dev = ABS(fdata - sinefit)
					
				; Remove degeneracy by enforcing positive amplitude, phase between 0 and 2pi
				if fsineparams[0] lt 0 then begin
					fsineparams[0] = 0 - fsineparams[0]
					fsineparams[2] = fsineparams[2] + !pi
				endif
				minphase = FLOOR(fsineparams[2] / (2 * !pi)) * (2 * !pi)
				fsineparams[2] = (fsineparams[2] - minphase) mod (2 * !pi)
				
				; Plot the data and the fit
				if (loud or png or eps) and numgood gt 1 then begin
					plotname = TIME2FILE(tstart, /sec) + '_' + thistag
					if eps then DEVICE, file = CONCAT_DIR(outpath, plotname) + '.eps'
					UTPLOT, TAI2UTC(tgrid), fdata, psym = 4, chars = 1.5, $
						ytitle = 'Fiducial offset [pix]', title = plotname, $
						yrange = thismed * scaler + [-5,5], /ystyle
					OUTPLOT, TAI2UTC(tgrid), sinefit, col = 2
					if png then PB_WIN2PNG, CONCAT_DIR(outpath, plotname) + '.png'
					if loud then PRINT, thischan, thisfid, numgood, MEAN(DOUBLE(dev)), $
						fsineparams, form = '(a10, a10, i8, f10.3, f10.3, f10.6, f10.3, f10.1)'
				endif
				
			endif else begin
				fsineparams = DBLARR(4)
				dev = 0d
			endelse
		endif else begin
			fsineparams = DBLARR(4)
			dev = 0d
			numgood = 0l
		endelse
		
		result.numfit[j, k] = numgood
		result.chisqs[j, k] = MEAN(DOUBLE(dev))
		result.paramvals[j, k, *] = DOUBLE(fsineparams)
	
	if pstop gt 1 then STOP
			
	endfor	;	Done with fiducial loop
	
endfor	;	Done with channel loop


; "Combo" fitting means doing a single fit to combine the measurements that should
; be the same except for a single constant offset, e.g. the SJI_1330_X1, SJI_1330_X2, 
; SJI_1400_X1 and SJI_1400_X2 should all vary in exactly the same way

if loud then PRINT
initpars = [initpars, REPLICATE(initpars[3], 3)]
ccolors = [2, 5, 6, 7]
; Loop through combo channels for this OBS
for j = 0, combo_numchan - 1 do begin
	thischan = combo_chans[j]
	
	if loud gt 1 then begin
		PRINT, 'Channel: ', thischan, form = '(a20, a12)'
		PRINT, '# Images: ', numin, form = '(a20, i12)'
	endif
	
	; Loop through the 2 combo fiducial axes for each combo channel
	for k = 0, combo_numfid - 1 do begin
		thisfid = combo_fid[k]
		thistag = thischan + '_' + thisfid
		
		all_tgrid = 0d
		all_fdata = 0d
		all_err = 0d
		all_type = 0
		typetag = STRARR(4)
		if j eq 2 then numoffset = 2 else numoffset = 4
		; Loop through the sub-offset types and gather the data
		for i = 0, (numoffset - 1) do begin

			typetag[i] = combo_subchan[i/2,j] + '_' + (['X', 'Y'])[k] + STRTRIM(i mod 2, 2)
			index = WHERE(fidlog.type eq combo_subchan[i/2,j], numin)
			thisdat = fidlog[index].(x1tag + k + 2*(i mod 2))
			thischisq = fidlog[index].(x1tag + k + 2*(i mod 2) + 4)
			gooddat = WHERE(thisdat gt 0 and thischisq lt chisqlim, numgood)			;	Good fit on this value
			if loud gt 1 then PRINT, '# Good images: ', numgood, form = '(a20, i12)'
			if numgood gt 0 then begin
			
				; Check for outliers
				if numgood eq 1 then thismed = thisdat[gooddat] else $
					thismed = MEDIAN(thisdat[gooddat])
				notoutlier = WHERE(ABS(thisdat[gooddat] - thismed) lt outlier, numinlier)
				gooddat = gooddat[notoutlier]
				numgood = numinlier
				fitpoints = fidlog[index[gooddat]]
				parinfo	= initpars
				tgrid	= ANYTIM2TAI(fitpoints.t_obs)
				
				;	Adjust the fiducial position by sumspat where appropriate
				sumspat = fitpoints.sumspat
				sumsptrl = fitpoints.sumsptrl
				;if (k eq 1) or (j lt 2) then scaler = sumspat else scaler = sumspat * 0 + 1
				if ( (k mod 2) eq 1 ) then scaler = sumspat else scaler = sumsptrl
				fdata	= thisdat[gooddat] * scaler
				err		= 0.5 + thischisq[gooddat]/50.
				all_tgrid = [all_tgrid, tgrid]
				all_fdata = [all_fdata, fdata]
				all_err = [all_err, err]
				all_type = [all_type, i + BYTARR(numgood)]
			endif
		endfor
		result.combo_tag[j,k,*] = typetag
		
		; Now that the data are gathered, decide which should be passed to the 
		; fitter. 
		numpoints = N_ELEMENTS(all_tgrid) - 1
		if numpoints gt 0 then begin
			all_tgrid = all_tgrid[1:*]
			all_fdata = all_fdata[1:*]
			all_err = all_err[1:*]
			all_type = all_type[1:*]
			typehist = HISTOGRAM(all_type, min = 0, max = 3)
			maxtype = MAX(typehist)
			parinfo = initpars
			if numpoints gt 20 and maxtype ge 10 then begin
				keeptype = WHERE(typehist ge maxtype / 3, comp=notype)
				result.combo_flag[j, k, keeptype] = 1
				fitind = WHERE(typehist[all_type] ge maxtype / 3, numfit)
				tgrid = [[all_tgrid[fitind]], [all_type[fitind]]]
				fdata = all_fdata[fitind]
				err = all_err[fitind]
				all_type = all_type[fitind]
				parinfo[3:6].fixed = 1
				parinfo[3+keeptype].fixed = 0
				; Do the fit, and calculate the residuals
				fsineparams = MPFITFUN('IRIS_MP_COMBO_SINECHISQ', tgrid, fdata, err, $
					parinfo = parinfo, yfit = sinefit, quiet=1 )	;	quiet = (1-loud)
				dev = ABS(fdata - sinefit)
				
;				; Remove degeneracy by enforcing positive amplitude, phase between 0 and 2pi
				if fsineparams[0] lt 0 then begin
					fsineparams[0] = 0 - fsineparams[0]
					fsineparams[2] = fsineparams[2] + !pi
				endif
				minphase = FLOOR(fsineparams[2] / (2 * !pi)) * (2 * !pi)
				fsineparams[2] = (fsineparams[2] - minphase) mod (2 * !pi)
				
				; Plot results, with different offsets applied
				shiftdata = fdata - fsineparams[3+all_type]
				shiftsine = sinefit - fsineparams[3+all_type]
				plotname = TIME2FILE(tstart, /sec) + '_' + thistag
				if (loud or png or eps) and numpoints gt 1 then begin
					UTPLOT, TAI2UTC(tgrid[*,0]), shiftdata, psym = 4, $
						yrange = [-3, 3], chars = 1.5, title = plotname, $
						ytit = 'Fiducial offset [pix]', /xstyle, /ystyle, yminor=4
					for i = 0, (numoffset - 1) do begin
						thisind = WHERE(all_type eq i, numthis)
						if numthis gt 0 then OUTPLOT, TAI2UTC(tgrid[thisind]), $
							shiftdata[thisind], col = ccolors[i], psym = 4
					endfor
					tsort = SORT(tgrid[*,0])
					OUTPLOT, TAI2UTC(tgrid[tsort,0]), shiftsine[tsort], thick = 2
					IMD_LEGEND, pos = 10, char = 2, col = ccolors[0:numoffset-1], $
						typetag + STRING(fsineparams[3:3+numoffset-1], form = '(f9.3)')
					if png then PB_WIN2PNG, CONCAT_DIR(outpath, plotname) + '.png'
				endif
			endif else begin
				fsineparams = DBLARR(7)
				dev = 0d
				numpoints = 0
			endelse
		endif else begin
			fsineparams = DBLARR(7)
			dev = 0d
			numpoints = 0
		endelse
			
		result.combo_numfit[j, k] = numpoints
		result.combo_chisqs[j, k] = MEAN(DOUBLE(dev))
		result.combo_paramvals[j, k, *] = DOUBLE(fsineparams)

		if loud then PRINT, thischan, thisfid, numpoints, MEAN(DOUBLE(dev)), fsineparams[0:numoffset+2], $
			form = '(a10, a10, i8, f10.3, f10.3, f10.6, f10.3, ' + STRTRIM(numoffset, 2) + 'f10.1)'

		if pstop gt 0 then STOP
			
	endfor	;	Done with axis loop
	
endfor	;	Done with channel loop
	
; Clean up from plotting
if (loud or png or eps) then begin
	TVLCT, rr, gg, bb
	!p.multi = oldpmulti
endif
if eps then begin
	SET_PLOT, olddev
endif

if KEYWORD_SET(outfile) then SAVEGEN, result, file = outfile

if KEYWORD_SET(runtime) then PRINT, 'IRIS_PREP_FILTER_FID: Elapsed time = ', $
	SYSTIME(/sec) - tt0

RETURN, result

end
