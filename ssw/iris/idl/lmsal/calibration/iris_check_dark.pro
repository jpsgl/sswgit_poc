function IRIS_CHECK_DARK, t1, $
	daywindow = daywindow, silent = silent, headers = headers, qs = qs, $
	pstop = pstop

;+
;
; Find datasets for checking the dark level
;
; INPUT:
;	t1	-	End time to check for images to characterize dark subtraction. 
;			Any time format recognized by ANYTIM2UTC
;
; RETURNS:
;	files	-	STRARR(3,3,2) of filenames
;
; KEYWORDS:
;	daywindow=	-	(INT input)
;	/silent	-	(SWITCH)
;	headers	-	(Struct array output) 
;	qs		-	(INTARR(3,3,2) output)
;	/pstop	-	(SWITCH)
;
;-

tt0 = SYSTIME(/sec)

if N_ELEMENTS(daywindow) eq 0 then daywindow = 4	;	Number of days in lookback window
if N_ELEMENTS(t1) eq 0 then t1 = RELTIME(/now)		;	Ending period of lookback interval
t0 = RELTIME(t1, days = 0 - daywindow)	;	Starting period of lookback

l1files_sji = IRIS_TIME2FILES(ANYTIM2UTC(/ccsds, t0), ANYTIM2UTC(/ccsds, t1), /sji, drms_sji, $
	key='IMG_PATH,EXPTIME,SUMSPAT,SUMSPTRL,CRVAL1,CRVAL2,SAT_ROT,RSUN_OBS,TSR1,TER1')
if l1files_sji[0] eq '' then RETURN, ''	;	or STOP
l1files_nuv = IRIS_TIME2FILES(t0, t1, /nuv, drms_nuv, $
	key='IMG_PATH,EXPTIME,SUMSPAT,SUMSPTRL,CRVAL3,CRVAL2,SAT_ROT,RSUN_OBS,TSR1,TER1')
if l1files_nuv[0] eq '' then RETURN, ''	;	or STOP
l1files_fuv = IRIS_TIME2FILES(t0, t1, /fuv, drms_fuv, $
	key='IMG_PATH,EXPTIME,SUMSPAT,SUMSPTRL,CRVAL3,CRVAL2,SAT_ROT,RSUN_OBS,TSR1,TER1')
if l1files_fuv[0] eq '' then RETURN, ''	;	or STOP

; Now look for a few different types of image.
;	1) Off-limb, with both the top and bottom halves of the CCD frame read out and
;		off the limb at some point (so at least a large spatial range, and the slit
;		not perpendicular to the limb) (Could probably calculate this more exactly
;		but I'll do it roughly)
;	2) Different img_path (SJI, NUV, FUV)
;	3) Different summing modes (1x1, 2x2, FUV 1x2) (Also 4x4, FUV 1x4, FUV 2x4 ?)
;	4) Different exposure times (long, > 28 ; short, ie < 5s )

files = STRARR(3,3,2)
qs = INTARR(3,3,2) - 1
for ipath = 0, 2 do begin
	case ipath of
		0	:	begin
			drms = drms_sji
			l1files = l1files_sji
			xx = drms.crval1
		end
		1	:	begin
			drms = drms_nuv
			l1files = l1files_nuv
			xx = drms.crval3
		end
		2	:	begin
			drms = drms_fuv
			l1files = l1files_fuv
			xx = drms.crval3
		end
	endcase
	
	; Set up bitmasks for various image parameters
	tdist 		= SQRT(xx^2 + drms.crval2^2)
	fov			= ((drms.ter1 - drms.tsr1) * drms.sumspat / 6.) 
	limbang 	= ATAN(TAN(ATAN(drms.crval2, xx))) * 180d / !pi
	slitang 	= drms.sat_rot

	off_limb	= tdist gt drms.rsun_obs
	slitperp 	= ABS(ABS(limbang - slitang) - 90) gt 30
	large_fov	= fov gt 100.
	vl_fov		= fov gt 150.
	sum_1		= (drms.sumspat eq 1) and (drms.sumsptrl eq 1)
	sum_2		= (drms.sumspat eq 2) and (drms.sumsptrl eq 2)
	if ipath eq 2 then begin
		sum_4		= (drms.sumspat eq 1) and (drms.sumsptrl eq 2)
	endif else begin
		sum_4		= (drms.sumspat eq 4) and (drms.sumsptrl eq 4)
	endelse
	sums		= [[sum_1], [sum_2], [sum_4]]
	long_exp	= drms.exptime gt 28.
	medl_exp	= drms.exptime gt 14.
	meds_exp	= drms.exptime lt 9.
	short_exp	= drms.exptime lt 5.
	exps		= [ [ [long_exp], [medl_exp] ], [ [short_exp], [meds_exp] ] ]
	
	; Loop through various desired combinations of the bitmasks and identify
	; the files that best meet the criteria (choosing the file with the
	; largest off-limb distance from among those that meet all the binary checks)
	for isum = 0, 2 do begin
		for iexp = 0, 1 do begin
			mask = (off_limb * slitperp * vl_fov * sums[*,isum] * exps[*,0,iexp]) * 8 + $
				(off_limb * slitperp * large_fov * sums[*,isum] * exps[*,0,iexp]) * 4 + $
				(off_limb * slitperp * vl_fov * sums[*,isum] * exps[*,1,iexp]) * 2 + $
				(off_limb * slitperp * large_fov * sums[*,isum] * exps[*,1,iexp]) 
			isfound = 0
			j = 3
			while not isfound do begin
				pickbit = 2^j
				pickmask = (mask and pickbit) / pickbit
				picktot = TOTAL(pickmask)
				if (picktot gt 0) or (j eq 0) then isfound = 1 else j = j - 1
			endwhile
			if picktot gt 0 then begin
				goodmask = WHERE(pickmask gt 0, numgood)
				pickimgs = MAX(tdist[goodmask], subscript = maxdist_ind)
				files[ipath,isum,iexp] = l1files[goodmask[maxdist_ind]]
				qs[ipath,isum,iexp] = j
			endif else begin
				files[ipath,isum,iexp] = ''	;	No image found
			endelse
		endfor
	endfor
endfor


; Make header output and print out the results
paths = ['SJI', 'NUV', 'FUV']
sum = ['1x1', '2x2', '4x4']
sums = [ [sum], [sum], [sum] ]
sums[2,2] = '1x2'
exps = ['long', 'short']
dummy = {fsn : 0l, t_obs : '', quality : 0l, win_flip : 0, $
	exptime : 0., crval1 : 0., crval2 : 0., crval3 : 0., sat_rot : 0., $
	sumspat : 0., sumsptrl : 0., img_path : ''}
headers = REPLICATE(dummy, 3, 3, 2)

if not KEYWORD_SET(silent) then $
	PRINT, 'PATH', 'SUM', 'EXP', 'exptime', 'x_val', 'y_val', 'roll', $
		'sumspat', 'sumsptrl', 'path', form = '(a5,a5,a10,a8,3a9,2a8,a10)'
		
for ipath = 0, 2 do for isum = 0, 2 do for iexp = 0, 1 do begin
	thisfile = files[ipath,isum,iexp]
	hdr = dummy
	if thisfile ne '' then begin
		READ_IRIS, thisfile, fhdr, dat, /silent
		STRUCT_ASSIGN, fhdr, hdr
	endif
	headers[ipath,isum,iexp] = hdr
	if ipath eq 0 then xx = hdr.crval1 else xx = hdr.crval3
	if not KEYWORD_SET(silent) then $
		PRINT, paths[ipath], sums[isum,ipath], exps[iexp], hdr.exptime, $
			xx, hdr.crval2, hdr.sat_rot, hdr.sumspat, hdr.sumsptrl, $
			hdr.img_path, form = '(a5,a5,a10,f8.1,3f9.2,2i8,a10)'
endfor

if not KEYWORD_SET(silent) then $
	PRINT, 'IRIS_CHECK_DARK run time : ', SYSTIME(/sec) - tt0, form = '(a40,f10.1)'
	
if KEYWORD_SET(pstop) then STOP

RETURN, files

end