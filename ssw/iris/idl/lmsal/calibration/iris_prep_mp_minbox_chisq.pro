function IRIS_PREP_MP_MINBOX_CHISQ, fitparams, $
	mask = mask

;+
;
; Chi-squared calculator called by IRIS_PREP_GEOWAVE_ROI. Returns a scalar indicating
; the difference between the size of the transformed output region and the amount
; of good data in the image.
;
; KEYWORDS: mask	--	an integer image specifying 1 for good data and 0 for missing data
;
; INPUT:	fitparams-- a 4-element vector containing the parameters of the fit
;						which are to be optimized. These are the dimensions of the
;						best-fit rectangle:
;							0: x0
;							1: y0
;							2: dx
;							3: dy
;
;-

if N_ELEMENTS(mask) eq 0 then begin
	common IRIS_PREP_MINBOX, fitmask
	mask = fitmask
endif

masksize = SIZE(mask)
x0 = 0>fitparams[0]<(masksize[1]-1)
y0 = 0>fitparams[1]<(masksize[2]-1)
dx = 0>fitparams[2]<(masksize[1]-1-x0)
dy = 0>fitparams[3]<(masksize[2]-1-y0)

newmask = INTARR(masksize[1], masksize[2])
newmask[x0:x0+dx, y0:y0+dy] = 1
badpoints = WHERE(mask eq 0 and newmask eq 1, numbad)
if numbad gt 0 then area = 1000 $
	else area = FLOAT(TOTAL(mask)) / TOTAL(newmask) - 1
RETURN, area

end
