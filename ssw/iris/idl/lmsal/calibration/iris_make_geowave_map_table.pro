pro IRIS_MAKE_GEOWAVE_MAP_TABLE, dmapx, dmapy, kx, ky, kxi, kyi, $
	filename = filename, interactive = interactive, degree = degree, $
	rec_num = rec_num, img_path = img_path, t_start = t_start, $
	t_stop = t_stop, version = version, load = load, append = append, $
	path = path, history = history, pstop = pstop
	
;+
;
; Construct a table of geometric/wavelength correction maps. Wrapper
; for S. Jaeggli's IRIS_SPEC_CAL; see ITN 19
;
; INPUT:
;	dmapx, dmapy	-	coordinate maps used for interpolation
;	kx, ky			-	Polynomial coordinate arrays used for remapping
;	kxi, kyi		-	(OPTIONAL) Polynomials for inverse transformation 
;
; KEYWORDS:
;	filename	-	the filename of the geometry calibration file to write
;	path=		-	the path to the output file
;	/interactive-	If set, then IRIS_SPEC_CAL is called and the user is prompted
;					to calculate new geometric/wavelength calibration parameters
;	/append		-	If set, then the input data are appended to the specified
;					filename. By default, a new file is written
;	/load		-	If set, then previously-calculated corrections are read in
;					and written to a file
;	/pstop		-	Standard debug switch
;
;	The following keywords specify the parameters of the correction entry to write
;	rec_num=	-	Integer record number
;	img_path=	-	string indicating the channel of the image
;	t_start=	-	string indicating the start time for the record entry (ISO)
;	t_stop=		-	string indicating the stop time for the record entry (ISO)
;	version=	-	integer version number
;	history=	-	string specifying the source of the data
;
; WRITTEN:
;	P.Boerner 2013/09 boerner ~ at ~ lmsal ~ dot ~ com
;
;-

if not KEYWORD_SET(append) then append = 0
if N_ELEMENTS(degree) eq 0 then degree = 2
if N_ELEMENTS(path) eq 0 then path = GET_LOGENV('SSW_IRIS_DATA')
tnow = RELTIME(/now, out = 'CCSDS')
tnow_tag = TIME2FILE(tnow, /sec)
if N_ELEMENTS(filename) eq 0 then filename = CONCAT_DIR(path, tnow_tag + '_geowave_map.genx')

if KEYWORD_SET(load) then begin
	; Load the results of previous runs of iris_spec_cal and use them to build a 
	; geowave table
	t_start = '2012-01-01T00:00:00Z'
	t_stop = '2033-01-01T00:00:00Z'

	version = 0
	
;	append = 0
;
;	img_path = 'FUVS'
;	history = 'Correction from IRIS_SPEC_CAL by S. Jaeggli on 20130905'
;	rec_num = 1l
;	RESTORE, /verb, '/Volumes/disk2/data/iris/iris_prep/iris_spec_cal.fuvs.20132908.223359.sav'
;	IRIS_MAKE_GEOWAVE_MAP_TABLE, append = append, wave_solution.dmapx, wave_solution.dmapy, $
;		filename = filename, rec_num = rec_num, img_path = img_path, pstop = pstop, $
;		t_start = t_start, t_stop = t_stop, version = version, history = history
;
;	append = 1
;
;	img_path = 'FUVL'
;	rec_num = 2l
;	history = 'Correction from IRIS_SPEC_CAL by S. Jaeggli on 20130905'
;	RESTORE, /verb, '/Volumes/disk2/data/iris/iris_prep/iris_spec_cal.fuvl.20132908.222115.sav'
;	IRIS_MAKE_GEOWAVE_MAP_TABLE, append = append, wave_solution.dmapx, wave_solution.dmapy, $
;		filename = filename, rec_num = rec_num, img_path = img_path, pstop = pstop, $
;		t_start = t_start, t_stop = t_stop, version = version, history = history
;
;	img_path = 'NUV'
;	rec_num = 3l
;	history = 'Correction from IRIS_SPEC_CAL by S. Jaeggli on 20130905'
;	RESTORE, /verb, '/Volumes/disk2/data/iris/iris_prep/iris_spec_cal.nuv.20132808.220020.sav'
;	IRIS_MAKE_GEOWAVE_MAP_TABLE, append = append, wave_solution.dmapx, wave_solution.dmapy, $
;		filename = filename, rec_num = rec_num, img_path = img_path, pstop = pstop, $
;		t_start = t_start, t_stop = t_stop, version = version, history = history

	append = 0

	img_path = 'ID'
	rec_num = 4l
	xind = DINDGEN(2072)
	yind = DINDGEN(1096)
	dmapx = REBIN(xind, 2072, 1096)
	dmapy = REBIN(TRANSPOSE(yind), 2072, 1096)
	kx = DBLARR(degree+1,degree+1)
	ky = DBLARR(degree+1,degree+1)
	kx[0,1] = 1
	ky[1,0] = 1
	history = 'Identity transform from IRIS_MAKE_GEOWAVE_MAP_TABLE'
	IRIS_MAKE_GEOWAVE_MAP_TABLE, append = append, dmapx, dmapy, kx, ky, kx, ky, $
		filename = filename, rec_num = rec_num, img_path = img_path, pstop = pstop, $
		t_start = t_start, t_stop = t_stop, version = version, history = history

	append = 1
	
;	version = 1
;	
;	img_path = 'NUV'
;	rec_num = 5l
;	history = 'Correction from IRIS_SPEC_CAL by S. Jaeggli on 20140306'
;	RESTORE, /verb, '/Volumes/disk2/data/iris/iris_prep/20140306_174036_NUV_spec_cal.sav'
;	IRIS_MAKE_GEOWAVE_MAP_TABLE, append = append, wave_solution.dmapx, wave_solution.dmapy, $
;		filename = filename, rec_num = rec_num, img_path = img_path, $
;		t_start = t_start, t_stop = t_stop, version = version, history = history
;	rec_num = 6l
;	history = 'Correction from IRIS_SPEC_CAL by S. Jaeggli on 20140407'
;	RESTORE, /verb, '/Volumes/disk2/data/iris/iris_prep/20140407_031616_NUV_spec_cal.sav'
;	IRIS_MAKE_GEOWAVE_MAP_TABLE, append = append, wave_solution.dmapx, wave_solution.dmapy, $
;		filename = filename, rec_num = rec_num, img_path = img_path, pstop = pstop, $
;		t_start = t_start, t_stop = t_stop, version = version, history = history
;
;	img_path = 'FUVS'
;	rec_num = 8l
;	history = 'Correction from IRIS_SPEC_CAL by S. Jaeggli on 20140721'
;	RESTORE, /verb, '/Users/boerner/data/iris/calibration/201407/20140721_160451_FUV1_spec_cal.sav'
;	IRIS_MAKE_GEOWAVE_MAP_TABLE, append = append, wave_solution.dmapx, wave_solution.dmapy, $
;		filename = filename, rec_num = rec_num, img_path = img_path, pstop = pstop, $
;		t_start = t_start, t_stop = t_stop, version = version, history = history
;
;	img_path = 'FUVL'
;	rec_num = 9l
;	history = 'Correction from IRIS_SPEC_CAL by S. Jaeggli on 20140721'
;	RESTORE, /verb, '/Users/boerner/data/iris/calibration/201407/20140721_160451_FUV2_spec_cal.sav'
;	IRIS_MAKE_GEOWAVE_MAP_TABLE, append = append, wave_solution.dmapx, wave_solution.dmapy, $
;		filename = filename, rec_num = rec_num, img_path = img_path, pstop = pstop, $
;		t_start = t_start, t_stop = t_stop, version = version, history = history
;
;	version = 2
;	
;	img_path = 'NUV'
;	rec_num = 7l
;	history = 'Correction from IRIS_SPEC_CAL by S. Jaeggli on 20140523'
;	RESTORE, /verb, '/Users/boerner/data/iris/calibration/201407/20140523_010920_nuv_spec_cal.sav'
;	IRIS_MAKE_GEOWAVE_MAP_TABLE, append = append, wave_solution.dmapx, wave_solution.dmapy, $
;		filename = filename, rec_num = rec_num, img_path = img_path, pstop = pstop, $
;		t_start = t_start, t_stop = t_stop, version = version, history = history	

;	version = 3
;
;	img_path = 'NUV'
;	rec_num = 10l
;	history = 'Correction from IRIS_SPEC_CAL by S. Jaeggli on 20140813'
;	RESTORE, /verb, '/Users/boerner/data/iris/calibration/201408/output/20140813_232220_NUV_spec_cal.sav'
;	IRIS_MAKE_GEOWAVE_MAP_TABLE, append = append, wave_solution.dmapx, wave_solution.dmapy, $
;		filename = filename, rec_num = rec_num, img_path = img_path, pstop = pstop, $
;		t_start = t_start, t_stop = t_stop, version = version, history = history
;
;	img_path = 'FUVS'
;	rec_num = 11l
;	history = 'Correction from IRIS_SPEC_CAL by S. Jaeggli on 20140812'
;	RESTORE, /verb, '/Users/boerner/data/iris/calibration/201408/output/20140812_054709_FUV1_spec_cal.sav'
;	IRIS_MAKE_GEOWAVE_MAP_TABLE, append = append, wave_solution.dmapx, wave_solution.dmapy, $
;		filename = filename, rec_num = rec_num, img_path = img_path, pstop = pstop, $
;		t_start = t_start, t_stop = t_stop, version = version, history = history
;
;	img_path = 'FUVL'
;	rec_num = 12l
;	history = 'Correction from IRIS_SPEC_CAL by S. Jaeggli on 20140812'
;	RESTORE, /verb, '/Users/boerner/data/iris/calibration/201408/output/20140812_054709_FUV2_spec_cal.sav'
;	IRIS_MAKE_GEOWAVE_MAP_TABLE, append = append, wave_solution.dmapx, wave_solution.dmapy, $
;		filename = filename, rec_num = rec_num, img_path = img_path, pstop = pstop, $
;		t_start = t_start, t_stop = t_stop, version = version, history = history
;
;	version = 4
;
;	img_path = 'NUV'
;	rec_num = 13l
;	history = 'Correction from IRIS_SPEC_CAL by S. Jaeggli on 20140911'
;	RESTORE, /verb, '/Users/boerner/data/iris/calibration/201409/output/20140911_010920_NUV_spec_cal.sav'
;	IRIS_MAKE_GEOWAVE_MAP_TABLE, append = append, wave_solution.dmapx, wave_solution.dmapy, $
;		filename = filename, rec_num = rec_num, img_path = img_path, pstop = pstop, $
;		t_start = t_start, t_stop = t_stop, version = version, history = history, degree = degree
;
;	img_path = 'FUVS'
;	rec_num = 14l
;	history = 'Correction from IRIS_SPEC_CAL by S. Jaeggli on 20140915'
;	RESTORE, /verb, '/Users/boerner/data/iris/calibration/201409/output/20140915_030723_FUV1_spec_cal.sav'
;	IRIS_MAKE_GEOWAVE_MAP_TABLE, append = append, wave_solution.dmapx, wave_solution.dmapy, $
;		filename = filename, rec_num = rec_num, img_path = img_path, pstop = pstop, $
;		t_start = t_start, t_stop = t_stop, version = version, history = history, degree = degree
;
;	img_path = 'FUVL'
;	rec_num = 15l
;	history = 'Correction from IRIS_SPEC_CAL by S. Jaeggli on 20140915'
;	RESTORE, /verb, '/Users/boerner/data/iris/calibration/201409/output/20140915_030723_FUV2_spec_cal.sav'
;	IRIS_MAKE_GEOWAVE_MAP_TABLE, append = append, wave_solution.dmapx, wave_solution.dmapy, $
;		filename = filename, rec_num = rec_num, img_path = img_path, pstop = pstop, $
;		t_start = t_start, t_stop = t_stop, version = version, history = history, degree = degree

	version = 5

	img_path = 'NUV'
	rec_num = 16l
	history = 'Correction from IRIS_SPEC_CAL by S. Jaeggli on 20150306'
	RESTORE, /verb, '/Users/boerner/data/iris/calibration/v33/20140306_174036_NUV_spec_cal.sav'
	IRIS_MAKE_GEOWAVE_MAP_TABLE, append = append, wave_solution.dmapx, wave_solution.dmapy, $
		filename = filename, rec_num = rec_num, img_path = img_path, pstop = pstop, $
		t_start = t_start, t_stop = t_stop, version = version, history = history, degree = degree

	img_path = 'FUVS'
	rec_num = 17l
	history = 'Correction from IRIS_SPEC_CAL by S. Jaeggli on 20150306'
	RESTORE, /verb, '/Users/boerner/data/iris/calibration/v33/20131011_235348_FUV1_FL_spec_cal.sav'
	IRIS_MAKE_GEOWAVE_MAP_TABLE, append = append, wave_solution.dmapx, wave_solution.dmapy, $
		filename = filename, rec_num = rec_num, img_path = img_path, pstop = pstop, $
		t_start = t_start, t_stop = t_stop, version = version, history = history, degree = degree

	img_path = 'FUVL'
	rec_num = 18l
	history = 'Correction from IRIS_SPEC_CAL by S. Jaeggli on 20150306'
	RESTORE, /verb, '/Users/boerner/data/iris/calibration/v33/20131011_235348_FUV2_FL_spec_cal.sav'
	IRIS_MAKE_GEOWAVE_MAP_TABLE, append = append, wave_solution.dmapx, wave_solution.dmapy, $
		filename = filename, rec_num = rec_num, img_path = img_path, pstop = pstop, $
		t_start = t_start, t_stop = t_stop, version = version, history = history, degree = degree

	RETURN
endif

; Check to make sure that the needed data are available
if N_ELEMENTS(dmapx) eq 0 then begin
	PRINT, 'IRIS_MAKE_GEOWAVE_MAP_TABLE: Please set /load or specify dmapx/dmapy'
	PRINT, 'Returning...'
	RETURN
endif
if N_ELEMENTS(kx) eq 0 then begin
	xo = REBIN(DINDGEN(2072), 2072, 1096)                                         
	yo = REBIN(TRANSPOSE(DINDGEN(1096)), 2072, 1096)                              
	numel = 2072ll * 1096ll
	POLYWARP, REFORM(dmapx, numel), REFORM(dmapy, numel), REFORM(xo, numel), REFORM(yo, numel), degree, kx, ky, /double  
endif
if N_ELEMENTS(kxi) eq 0 then begin
	xo = REBIN(DINDGEN(2072), 2072, 1096)                                         
	yo = REBIN(TRANSPOSE(DINDGEN(1096)), 2072, 1096)                              
	numel = 2072ll * 1096ll
	POLYWARP, REFORM(xo, numel), REFORM(yo, numel), REFORM(dmapx, numel), REFORM(dmapy, numel), degree, kxi, kyi, /double  
endif

; Set up the table entry
if not KEYWORD_SET(rec_num) then rec_num = 0
if not KEYWORD_SET(img_path) then img_path = 'NUV'
if not KEYWORD_SET(t_start) then t_start = '2013-07-01T00:00:00Z'
if not KEYWORD_SET(t_stop) then t_stop = '2033-01-01T00:00:00Z'
if not KEYWORD_SET(version) then version = 0
if not KEYWORD_SET(history) then history = ''
this_rec_str = {rec_num : rec_num, img_path : img_path, $
	t_start : t_start, t_stop : t_stop, $
	version : version, date : tnow,  history : history, $
	dmapx : dmapx, dmapy : dmapy, $
	kx : kx, ky : ky, kxi : kxi, kyi : kyi}

if KEYWORD_SET(pstop) then STOP

; Save a GENX file with the geowave correction structure
if KEYWORD_SET(append) then begin
	RESTGEN, file = filename, current_str
	outstr = [current_str, this_rec_str]
endif else begin
	outstr = [this_rec_str]
endelse
SAVEGEN, file = filename, outstr


end