pro IRIS_PREP_RUN, t0, t1, $
	pstop = pstop, outpath = outpath, dowarp = dowarp, $
	noback = noback, noflat = noflat, measure_aia = measure_aia, $
	savefits = savefits, saveall = saveall, subdate = subdate, $
	_extra = extra

;+
;
; Wrapper for IRIS_PREP used to create ancillary databases when running locally
; on crom. Writes the data to subdirectories named by YYYYMMDD_HHMMSS of the 
; start of the OBS, in /Volumes/disk2/data/iris/iris_prep/run/ . Should stay under 
; 4 GB of RAM use thanks to the blocksize = 75 setting.
;
;-

; Set up constants
tt0 = SYSTIME(/sec)
strdummy = STRING('', format = '(a100)')
if not KEYWORD_SET(measure_aia) then measure_aia = 1
if not KEYWORD_SET(no_back) then no_back = 1
if not KEYWORD_SET(no_flat) then no_flat = 1
if not KEYWORD_SET(pstop) then pstop = 0
if KEYWORD_SET(dowarp) then begin
	if not KEYWORD_SET(outpath) then outpath = '/Volumes/disk2/data/iris/iris_prep/run11/'
	post = 1
	prefid = 0
	prewave = 0
endif else begin
	if not KEYWORD_SET(outpath) then outpath = '/Volumes/disk2/data/iris/iris_prep/run12/'
	post = 0
	dowarp = 0
	prefid = 1
	prewave = 1
endelse

ds = 'iris.lev1'
blocksize = 75
chans = ['SJI_1330', 'SJI_1400', 'SJI_1600W', 'SJI_2796', 'SJI_2832', 'SJI_5000W', $
	'NUV', 'FUV', 'NUV-SJI']

; Identify OBS run during the requested time interval
obses = IRIS_TIME2TIMELINE(t0, t1)
numobs = N_ELEMENTS(obses)
PRINT, GET_INFOX(obses, 'date_obs,duration,repeats,date_end,size', $
	format = ['a24', 'f8.1', 'i8', 'a24', 'f8.1'], /display)
PRINT

; Loop through OBS
for i = 0, numobs-1 do begin

	thisobs = obses[i]
	otags = TAG_NAMES(thisobs)
	for j = 0, N_ELEMENTS(otags) - 1 do begin
		thisstr = strdummy
		STRPUT, thisstr, thisobs.(j), 0
		PRINT, otags[j], thisstr, format = '(a10, a105)'
	endfor
;	HELP, thisobs, /str

	CATCH, error_status
	if error_status ne 0 then begin
		PRINT, 'Caught error: ', error_status
		PRINT, 'Error message: ', !err_string
		PRINT, 'Skipping this obs...'
		goto, skipobs
	endif

	; Get metadata for all images taken during the OBS
	SSW_JSOC_TIME2DATA, thisobs.date_obs, thisobs.date_end, drms, files, $
		/silent, /files, /jsoc2, ds = ds, key = 't_obs,fsn,img_path'
	if N_TAGS(drms) eq 0 then goto, skipobs

	; Make subdirectories to hold the results
	ttag = TIME2FILE(thisobs.date_obs, /sec)
	if KEYWORD_SET(subdate) then begin
		year = STRMID(ttag, 0, 4)
		month = STRMID(ttag, 4, 2)
		day = STRMID(ttag, 6, 2)
		yrpath = CONCAT_DIR(outpath, year)
		if not FILE_EXIST(yrpath) then SPAWN, 'mkdir ' + yrpath, result, errcode
		monthpath = CONCAT_DIR(yrpath, month)
		if not FILE_EXIST(monthpath) then SPAWN, 'mkdir ' + monthpath, result, errcode
		daypath = CONCAT_DIR(monthpath, day)
		if not FILE_EXIST(daypath) then SPAWN, 'mkdir ' + daypath, result, errcode
		logdir = CONCAT_DIR(daypath, ttag)
	endif else begin
		logdir = CONCAT_DIR(outpath, ttag)
	endelse
	if not FILE_EXIST(logdir) then begin
		SPAWN, 'mkdir ' + logdir, result, errcode
		SPAWN, 'mkdir ' + CONCAT_DIR(logdir, 'dose'), result, errcode
		SPAWN, 'mkdir ' + CONCAT_DIR(logdir, 'ccdstat'), result, errcode
		SPAWN, 'mkdir ' + CONCAT_DIR(logdir, 'fid'), result, errcode
		SPAWN, 'mkdir ' + CONCAT_DIR(logdir, 'wavecorr'), result, errcode
		SPAWN, 'mkdir ' + CONCAT_DIR(logdir, 'aiacorr'), result, errcode
	endif
	
	; Loop through the channels	
	for j = 0, N_ELEMENTS(chans) - 1 do begin
		index = WHERE(drms.img_path eq chans[j], numin)
		PRINT & PRINT, chans[j], numin, format = '(a10,i10)'		
		if numin eq 0 then begin
			PRINT, 'no images of type ', chans[j]
			goto, skipchan
		endif

		; If there are more than blocksize images, split them up into
		; batches of blocksize
		numloop = numin / FLOAT(blocksize)
		tttt = SYSTIME(/sec)
		for k = 0, CEIL(numloop)-1 do begin
			imin = k * blocksize
			imax = (((k+1) * blocksize) < numin) - 1
			PRINT, numin, imin, imax
			ttt0 = SYSTIME(/sec)
			READ_IRIS, files[index[imin:imax]], hdr, dat, /silent, /uncomp_delete
			ttt1 = SYSTIME(/sec) & PRINT, 'READ_IRIS: ', ttt1 - ttt0, form = '(a30,f10.2)'
			
			IRIS_PREP, hdr, dat, ohdr, odat, $
				measure_fid = prefid, measure_wave = prewave, post = post, $
				/measure_aia, /dose, /ccdstat, /write, logdir = logdir, $
				/verb, /run, noflat = noflat, noback = noback, _extra = extra
				
			if KEYWORD_SET(savefits) then begin
				if KEYWORD_SET(saveall) then savind = INDGEN(N_ELEMENTS(ohdr)) else savind = 0
				for m = 0, N_ELEMENTS(savind) - 1 do begin
					thishead = ohdr[savind[m]]
					fname = TIME2FILE(/sec, thishead.t_obs) + '_' + thishead.img_path + '.fits'
					WRITEFITS, CONCAT_DIR(logdir, fname), odat[*,*,savind[m]], thishead
				endfor
			endif
			if KEYWORD_SET(pstop) then STOP

			; Make a struct array with one header from each img_path
			if N_ELEMENTS(typehdr) eq 0 then begin
				typehdr = REPLICATE(ohdr[0], N_ELEMENTS(chans))
				typetemp = ohdr[0]
			endif else begin
				if k eq 0 then begin
					STRUCT_ASSIGN, ohdr[0], typetemp
					typehdr[j] = typetemp
				endif
			endelse
			
		endfor
		ttt7 = SYSTIME(/sec) & PRINT, 'IRIS_PREP: ', ttt7 - tttt, form = '(a40, f10.1)' & PRINT

		skipchan: if KEYWORD_SET(pstop) then STOP
		
	endfor
	
	IRIS_PREP_TREND_META, logdir, /silent, /loud
	IRIS_PREP_TREND_VERSIONS, logdir, typehdr
	IRIS_PREP_FILTER_OBS, logdir, /save, /runtime, /png
	
	skipobs: if KEYWORD_SET(pstop) then STOP
	
endfor

tt1 = SYSTIME(/sec)
PRINT, 'IRIS_PREP_RUN : Run time ', tt1 - tt0, format = '(a30, f10.1)'

end


