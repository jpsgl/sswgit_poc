pro IRIS_PREP_TREND_CCDSTAT, $
	result, logdat, $
	win = win, inpath = inpath, outpath = outpath, pstop = pstop, loud = loud, $
	read_logdat = read_logdat, write_logdat = write_logdat, png = png, save = save

;+
;
; Read the output of various IRIS_PREP ccdstat logs
;
;-

tt0 = SYSTIME(/sec)
if not KEYWORD_SET(win) then win = 10
if not KEYWORD_SET(inpath) then inpath = '/irisa/data/prep/'
if not KEYWORD_SET(outpath) then outpath = '~/data/iris/trending/'

;++++++++++++++++++++++++++++++++++++++++++++++
; Get the data, either by reading a GENX file
; previously written by this routine or by
; wading through all the ccdstat.txt files
;----------------------------------------------
if KEYWORD_SET(read_logdat) then begin
	RESTGEN, file = CONCAT_DIR(outpath, 'iris_prep_all_ccdstat_save.genx'), logdat
	if KEYWORD_SET(loud) then PRINT, 'IRIS_PREP_TREND_CCDSTAT read logdat. Elapsed time : ', $
		STRING(SYSTIME(/sec) - tt0, form = '(f10.1)')
	if KEYWORD_SET(pstop) then STOP
endif else begin
	CD, inpath, current = old_dir
	ff = FILE_SEARCH('*/*/*/*/ccdstat/*.txt') 
	numf = N_ELEMENTS(ff)
	for i = 0, numf - 1 do begin
		thisdat = IRIS_PREP_TXT2STR(ff[i], /ccdstat)
		if i eq 0 then logdat = thisdat else logdat = [logdat,thisdat]
	endfor
	CD, old_dir
endelse

;++++++++++++++++++++++++++++++++++++++++++++++
; If the write_logdat keyword is set, then 
; first write a single genx file with all the 
; wavelength measurements from each individual
; image. Not very efficient; shouldn't be used
; regularly.
;----------------------------------------------
if KEYWORD_SET(write_logdat) then begin
	SAVEGEN, file = CONCAT_DIR(outpath, 'iris_prep_all_ccdstat_save.genx'), logdat
	if KEYWORD_SET(loud) then PRINT, 'IRIS_PREP_TREND_CCDSTAT wrote logdat. Elapsed time : ', $
		STRING(SYSTIME(/sec) - tt0, form = '(f10.1)')
	if KEYWORD_SET(pstop) then STOP
endif

; Note that you can't use bls1 to filter for good BLS regions, in general, 
; because any corrupt images will stick stuff in the E quadrant and give
; spurious results
ff = WHERE(logdat.img_path eq 'FUV' and FINITE(logdat.bls3_mean))
nn = WHERE(logdat.img_path eq 'NUV' and FINITE(logdat.bls3_mean))
ss = WHERE(STRMID(logdat.img_path, 0, 3) eq 'SJI' and FINITE(logdat.bls2_mean))
ffo = WHERE(logdat.img_path eq 'FUV' and FINITE(logdat.oscan1_mean))        
nno = WHERE(logdat.img_path ne 'FUV' and $
	(FINITE(logdat.oscan1_mean) or FINITE(logdat.oscan2_mean)) )

; Generate plots
TVLCT, rr, gg, bb, /get
PB_SET_LINE_COLOR

WDEF, win+0, 1000, 700
UTPLOT, logdat[ff].t_obs, logdat[ff].bls2_mean, psym = 4, yrange = [-4, 4], $
	chars = 1.5, ytit = 'MEAN of BLS Region [DN]', title = 'IRIS FUV BLS'
OUTPLOT, logdat[ff].t_obs, logdat[ff].bls4_mean, psym = 4, col = 7
OUTPLOT, logdat[ff].t_obs, logdat[ff].bls3_mean, psym = 4, col = 6
OUTPLOT, logdat[ff].t_obs, logdat[ff].bls1_mean, psym = 4, col = 5
LEGEND, pos = 10, chars = 2, psym = 4, col = [5,255,6,7], ['E', 'F', 'H', 'G']
if KEYWORD_SET(png) then PB_WIN2PNG, CONCAT_DIR(outpath, 'fuv_bls.png')

WDEF, win+1, 1000, 700
UTPLOT, logdat[nn].t_obs, logdat[nn].bls3_mean, psym = 4, yrange = [-4, 4], $
	chars = 1.5, ytit = 'MEAN of BLS Region [DN]', title = 'IRIS NUV BLS'
OUTPLOT, logdat[ff].t_obs, logdat[ff].bls4_mean, psym = 4, col = 2
LEGEND, pos = 10, chars = 2, psym = 4, col = [255,2], ['H', 'G']
if KEYWORD_SET(png) then PB_WIN2PNG, CONCAT_DIR(outpath, 'nuv_bls.png')

WDEF, win+2, 1000, 700
UTPLOT, logdat[ss].t_obs, logdat[ss].bls1_mean, psym = 4, yrange = [-4, 4], $
	chars = 1.5, ytit = 'MEAN of BLS Region [DN]', title = 'IRIS SJI BLS'
OUTPLOT, logdat[ss].t_obs, logdat[ss].bls2_mean, psym = 4, col = 2
LEGEND, pos = 10, chars = 2, psym = 4, col = [255,2], ['E', 'F']
if KEYWORD_SET(png) then PB_WIN2PNG, CONCAT_DIR(outpath, 'sji_bls.png')

WDEF, win+3, 1000, 700
UTPLOT, logdat[ffo].t_obs, logdat[ffo].oscan1_mean / logdat[ffo].exptime, $
	psym = 4, chars = 1.5, ytit = 'MEAN of Overscan Region / EXPTIME [DN/sec]', $
	title = 'IRIS FUV Overscan', yrange = [-1,1]
OUTPLOT, logdat[ffo].t_obs, logdat[ffo].oscan2_mean / logdat[ffo].exptime, $
	col = 2, psym = 4, thick = 2
LEGEND, pos = 10, chars = 2, psym = 4, col = [255,2], ['FUVS', 'FUVL']
if KEYWORD_SET(png) then PB_WIN2PNG, CONCAT_DIR(outpath, 'fuv_oscan.png')

WDEF, win+4, 1000, 700
UTPLOT, logdat[nno].t_obs, logdat[nno].oscan1_mean / logdat[nno].exptime, $
	psym = 4, chars = 1.5, ytit = 'MEAN of Overscan Region / EXPTIME [DN/sec]', $
	title = 'IRIS NUV/SJI Overscan', yrange = [-1,1]
OUTPLOT, logdat[nno].t_obs, logdat[nno].oscan2_mean / logdat[nno].exptime, $
	col = 2, psym = 4, thick = 2
LEGEND, pos = 10, chars = 2, psym = 4, col = [255,2], ['SJI', 'NUV']
if KEYWORD_SET(png) then PB_WIN2PNG, CONCAT_DIR(outpath, 'nuvsji_oscan.png')

result = CREATE_STRUCT( 'FUV_BLS', logdat[ff], 'NUV_BLS', logdat[nn], $
	'SJI_BLS', logdat[ss], 'FUV_OSCAN', logdat[ffo], 'NUVSJI_OSCAN', logdat[nno] )
if KEYWORD_SET(save) then begin
	SAVEGEN, file = CONCAT_DIR(outpath, 'trend_ccdstat.genx'), str = result
endif


if KEYWORD_SET(loud) then PRINT, 'IRIS_PREP_TREND_CCDSTAT elapsed time : ', $
	STRING(SYSTIME(/sec) - tt0, form = '(f10.1)')
if KEYWORD_SET(pstop) then STOP

end