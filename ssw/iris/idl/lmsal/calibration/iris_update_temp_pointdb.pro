
;+
; Name    : iris_update_temp_pointdb
;
; Purpose : Updates a file with daily averages of IRIS instrument temperatures
;           that are relevant for the pointing.
;           The default file location is $SSW_IRIS_DATA/iris_temp_pointdb.geny
;
; Use     : IDL> iris_update_temp_pointdb
;
; Output  : None  (writes / updates a file)
;
; Keywords input:
;    path = string.  Default: $SSW_IRIS_DATA
;    days = int.     Number of days to go back for the update.
;                    Default = 3.
;    /new : Re-calculates the file from scratch
;    quiet = 0       Turn on diagnostics for iris_time2hk
;                    Default is quiet=1 !
;
; Keywords output:
;
; Common  :
;
; Restrictions:
;
; Side effects:
;
; Category   : Pipeline
;
; Prev. Hist.:
;
; Written    : Jean-Pierre Wuelser                       LMSAL 06-Dec-2013
;              JPW 2014-02-03       added the 6 SG temperatures
;-

pro iris_update_temp_pointdb,path=path,days=days,new=new,quiet=quiet

; defaults
if n_elements(path) ne 1 then path = '$SSW_IRIS_DATA'
if n_elements(days) ne 1 then days=3
if n_elements(quiet) ne 1 then quiet=1

; elementary database structure
tdb0 = {anytim:0d0,it01pmrf:0.,it06telm:0., $
       it14sppx:0.,it15sppy:0.,it16spnx:0., $
       it17spny:0.,it18sppz:0.,it19spnz:0.}
; read the existing database
tdbnam = concat_dir(path,'iris_temp_pointdb.geny')
restgenx,file=tdbnam,tdb
if n_tags(tdb) ne n_tags(tdb0) then new=1

; get 0 UT of current day and start day
secday = 864d2
get_utc,tend
tend = anytim(tend)
tend = long(tend/secday)*secday      ; 0 UT today
if keyword_set(new) then begin
  tsta = anytim('2013-07-18T00:00')
endif else begin
  ; remove all entries after tend from database
  w = where(tdb.anytim lt tend)
  tdb = tdb[w]
  ; define start time for data base update
  tmax = max(tdb.anytim)
  tmax = long(tmax/secday)*secday    ; 0 UT of day "tmax"
  tsta = tmax < (tend - secday * days)
  w = where(tdb.anytim lt tsta,nw)
  if nw gt 0 then tdb = tdb[w] else new=1
endelse

; update database
nd = long((tend+1d0-tsta)/secday)
if keyword_set(new) $
  then tdb1 = replicate(tdb0,nd+1) $
  else tdb1 = replicate(tdb[0],nd+1)

t0 = tsta
for j=0,nd-1 do begin
  t1 = t0+secday-1d0  ; before end-of-day to avoid reading daily file twice
  hk = iris_time2hk(t0,t1,quiet=quiet)
  w = where(hk.it01pmrf ge -273. and hk.it06telm ge -273. and $
            hk.it14sppx ge -273. and hk.it15sppy ge -273. and $
            hk.it16spnx ge -273. and hk.it17spny ge -273. and $
            hk.it18sppz ge -273. and hk.it19spnz ge -273.,nw)
  if nw gt 0 then begin
    tdb1[j].anytim = (t0+t1)/2d0
    tdb1[j].it01pmrf = total(hk[w].it01pmrf)/nw
    tdb1[j].it06telm = total(hk[w].it06telm)/nw
    tdb1[j].it14sppx = total(hk[w].it14sppx)/nw
    tdb1[j].it15sppy = total(hk[w].it15sppy)/nw
    tdb1[j].it16spnx = total(hk[w].it16spnx)/nw
    tdb1[j].it17spny = total(hk[w].it17spny)/nw
    tdb1[j].it18sppz = total(hk[w].it18sppz)/nw
    tdb1[j].it19spnz = total(hk[w].it19spnz)/nw
  endif else tdb1[j] = 0d0
  t0 = t0 + secday
endfor
tdb1[nd] = tdb1[nd-1]
tdb1[nd].anytim = 2d9 ; far in the future (~2042)
w = where(tdb1.anytim gt tsta,nw)
if nw gt 0 then begin
  if keyword_set(new) then tdb = tdb1[w] $
                      else tdb = [tdb,tdb1[w]]
end
tdb = tdb[sort(tdb.anytim)]
savegenx,file=tdbnam,tdb,/over

end
