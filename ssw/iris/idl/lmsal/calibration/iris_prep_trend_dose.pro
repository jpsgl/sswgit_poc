pro IRIS_PREP_TREND_DOSE

;+
;
; Read the output of various IRIS_PREP dose logs
;
;-

CD, '/irisa/data/prep/', current = old_dir

f_ndose = FILE_SEARCH('*/*/*/*/dose/*ndose.fits') 
f_nread = FILE_SEARCH('*/*/*/*/dose/*nread.fits') 
f_fdose = FILE_SEARCH('*/*/*/*/dose/*fdose.fits') 
f_fread = FILE_SEARCH('*/*/*/*/dose/*fread.fits') 
f_log = FILE_SEARCH('*/*/*/*/dose/*_dose.txt') 

for i = 0, N_ELEMENTS(f_log) - 1 do begin
	thisdat = IRIS_PREP_TXT2STR(f_log[i], /dose)
	if i eq 0 then logdat = thisdat else logdat = [logdat,thisdat]
endfor

STOP
MREADFITS, f_ndose, h_ndose, d_ndose
MREADFITS, f_nread, h_nread, d_nread

fuvs = WHERE(logdat.img_path eq 'FUV', numfuv)
nuvs = WHERE(STRMID(logdat.img_path, 0, 3) eq 'NUV', numnuv)
sjif = WHERE(STRMID(logdat.img_path, 0, 5) eq 'SJI_1', numsf)
sjin = WHERE(STRMID(logdat.img_path, 0, 3) eq 'SJI' and $
	STRMID(logdat.img_path, 4, 1) ne '1', numsn)

fuv_dt = TOTAL(logdat[fuvs].exptime)
nuv_dt = TOTAL(logdat[nuvs].exptime)
sjif_dt = TOTAL(logdat[sjif].exptime)
sjin_dt = TOTAL(logdat[sjin].exptime)

STOP
CD, old_dir

end