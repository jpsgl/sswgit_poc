function IRIS_MP_COMBO_SINECHISQ, x, params

;+
;
; Chi-squared calculator to be used with MPFIT to find the best-fit
; sine wave, with M different offset data sets using the same phase and 
; amplitude. 
;
; Note that the X input is [N,2] where x[*,0] is the actual
; X value and x[*,1] is an index from 0 to M-1 specifying which offset
; parameter should be used
;
;-

amplitude	= params[0]
frequency	= params[1]
phase		= params[2]
offset		= params[3:*]

yout = offset[x[*,1]] + amplitude * SIN( (frequency * x[*,0]) + phase )

RETURN, yout

end