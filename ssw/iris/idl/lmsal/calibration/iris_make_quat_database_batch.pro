;+
;
; SSW Batch file for generating IRIS quaternion database
;
;-

timenow = TIME2FILE(RELTIME(/now), /sec)

SET_LOGENV,'GSE_PKT_FILE_DIR', '/irisa/moc/hk_archive-in/packets/vc2/'
;set_logenv,'GSE_PKT_FILE_DIR', '/net/bay/Volumes/earth/iris/moc/hk_archive-in/packets/vc2/'

; Check paths for SAN/network problems
case 1 of
	FILE_EXIST('/irisa/data/prep/aux/')	:	finalpath = '/irisa/data/prep/aux/'
	FILE_EXIST('/net/brooklyn/Volumes/earth/iris/data/prep/aux/')	:	finalpath = '/net/brooklyn/Volumes/earth/iris/data/prep/aux/'
	FILE_EXIST('/net/bay/Volumes/earth/iris/data/prep/aux/')	:	finalpath = '/net/bay/Volumes/earth/iris/data/prep/aux/'
	else	:	begin
		PRINT, 'Cannot reach irisa...'
		finalpath = '/irisa/data/prep/aux/'
	end
endcase
logpath = CONCAT_DIR(CONCAT_DIR('log', STRMID(timenow, 0, 4)), STRMID(timenow, 4, 2))
logpath = CONCAT_DIR(finalpath, logpath)
finalpath = CONCAT_DIR(finalpath, 'pointing')

case 1 of
	FILE_EXIST('/sanhome/boerner/public_html/iris/magnetometer')	:	pbpath = '/sanhome/boerner/public_html/iris/magnetometer'
	else	:	begin
		PRINT, 'Cannot reach sanhome public_html...'
		pbpath = '~/iris/cronlog/'
	end
endcase

OPENW, lun, /get, CONCAT_DIR(logpath, timenow + '_pointing.txt')
PRINTF, lun & PRINTF, lun, 'IRIS_MAKE_QUAT_DATABASE_BATCH running... ', timenow
PRINTF, lun, finalpath & PRINTF, lun

; Generate data for the previous day
PRINTF, lun, '*************************************
PRINTF, lun, ' Generating data for hours=-7'
PRINTF, lun, '*************************************
daynow = RELTIME(hours = -7)
IRIS_MAKE_QUAT_DATABASE, daynow, outpath = '/tmp', fname = fname
newfile = CONCAT_DIR('/tmp', fname)
newdat = RD_TFILE(newfile, 20)
goodnew = WHERE(newdat[1,1:*] eq 0, nfsize)

pbfile = CONCAT_DIR(pbpath, fname)
PRINTF, lun & PRINTF, lun, 'Copying to PB web dir...'
SPAWN, 'cp ' + newfile + ' ' + pbfile, res, err
PRINTF, lun, 'cp result: ', res
PRINTF, lun, 'cp errcode: ', err

oldfile = CONCAT_DIR(finalpath, fname)
PRINTF, lun & PRINTF, lun, 'Copying to /irisa ...'
if FILE_EXIST(oldfile) then begin
	olddat = RD_TFILE(oldfile, 20)
	goodold = WHERE(olddat[1,1:*] eq 0, ofsize)
endif else ofsize = 0
PRINTF, lun, 'Old file: ', ofsize, 'New file: ', nfsize, '(good lines)', $
	form = '(a15,i10,a15,i10,a15)'
if ofsize gt nfsize then begin
	PRINTF, lun, 'Keeping old file...'
	SPAWN, 'rm ' + newfile, res, err
	PRINTF, lun, 'rm result: ', res
	PRINTF, lun, 'rm errcode: ', err
endif else begin
	PRINTF, lun, 'Keeping new file ', newfile
	SPAWN, 'mv ' + newfile + ' ' + oldfile, res, err
	PRINTF, lun, 'mv result: ', res
	PRINTF, lun, 'mv errcode: ', err
endelse


; Once per day at 10 pm, go back and generate 2-day old and 30-day old files
if STRMID(SYSTIME(), 11, 2) eq '22' then begin

	; Generate data for 2 days ago
	PRINTF, lun, '*************************************
	PRINTF, lun, ' Generating data for hours=-48'
	PRINTF, lun, '*************************************
	daynow = RELTIME(hours = -48)
	IRIS_MAKE_QUAT_DATABASE, daynow, outpath = '/tmp', fname = fname
	newfile = CONCAT_DIR('/tmp', fname)
	newdat = RD_TFILE(newfile, 20)
	goodnew = WHERE(newdat[1,1:*] eq 0, nfsize)

	pbfile = CONCAT_DIR(pbpath, fname)
	PRINTF, lun & PRINTF, lun, 'Copying to PB web dir...'
	SPAWN, 'cp ' + newfile + ' ' + pbfile, res, err
	PRINTF, lun, 'cp result: ', res
	PRINTF, lun, 'cp errcode: ', err

	oldfile = CONCAT_DIR(finalpath, fname)
	PRINTF, lun & PRINTF, lun, 'Copying to /irisa ...'
	if FILE_EXIST(oldfile) then begin
		olddat = RD_TFILE(oldfile, 20)
		goodold = WHERE(olddat[1,1:*] eq 0, ofsize)
	endif else ofsize = 0
	PRINTF, lun, 'Old file: ', ofsize, 'New file: ', nfsize, '(good lines)', $
		form = '(a15,i10,a15,i10,a15)'
	if ofsize gt nfsize then begin
		PRINTF, lun, 'Keeping old file...'
		SPAWN, 'rm ' + newfile, res, err
		PRINTF, lun, 'rm result: ', res
		PRINTF, lun, 'rm errcode: ', err
	endif else begin
		PRINTF, lun, 'Keeping new file ', newfile
		SPAWN, 'mv ' + newfile + ' ' + oldfile, res, err
		PRINTF, lun, 'mv result: ', res
		PRINTF, lun, 'mv errcode: ', err
	endelse

	; Generate data for one month ago
	PRINTF, lun, '*************************************
	PRINTF, lun, ' Generating data for days=-30'
	PRINTF, lun, '*************************************
	daynow = RELTIME(days = -30)
	IRIS_MAKE_QUAT_DATABASE, daynow, outpath = '/tmp', fname = fname
	newfile = CONCAT_DIR('/tmp', fname)
	newdat = RD_TFILE(newfile, 20)
	goodnew = WHERE(newdat[1,1:*] eq 0, nfsize)

	pbfile = CONCAT_DIR(pbpath, fname)
	PRINTF, lun & PRINTF, lun, 'Copying to PB web dir...'
	SPAWN, 'cp ' + newfile + ' ' + pbfile, res, err
	PRINTF, lun, 'cp result: ', res
	PRINTF, lun, 'cp errcode: ', err

	oldfile = CONCAT_DIR(finalpath, fname)
	PRINTF, lun & PRINTF, lun, 'Copying to /irisa ...'
	if FILE_EXIST(oldfile) then begin
		olddat = RD_TFILE(oldfile, 20)
		goodold = WHERE(olddat[1,1:*] eq 0, ofsize)
	endif else ofsize = 0
	PRINTF, lun, 'Old file: ', ofsize, 'New file: ', nfsize, '(good lines)', $
		form = '(a15,i10,a15,i10,a15)'
	if ofsize gt nfsize then begin
		PRINTF, lun, 'Keeping old file...'
		SPAWN, 'rm ' + newfile, res, err
		PRINTF, lun, 'rm result: ', res
		PRINTF, lun, 'rm errcode: ', err
	endif else begin
		PRINTF, lun, 'Keeping new file ', newfile
		SPAWN, 'mv ' + newfile + ' ' + oldfile, res, err
		PRINTF, lun, 'mv result: ', res
		PRINTF, lun, 'mv errcode: ', err
	endelse

endif

timeend = TIME2FILE(RELTIME(/now), /sec)
PRINTF, lun & PRINTF, lun, 'IRIS_MAKE_QUAT_DATABASE_BATCH done... ', timeend
PRINTF, lun

end
