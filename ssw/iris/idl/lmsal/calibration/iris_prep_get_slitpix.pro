function IRIS_PREP_GET_SLITPIX, iindex, $
	slitpix_recnum = slitpix_recnum

;+
;
; Load a previously-generated slit pixel map corresponding to a FITS header
;
; INPUTS:
;	iindex	-	FITS header for which you want the slit pixel map. Looks at the
;				img_path and t_obs
;
; RETURNS:
;	slitpix_list -	A vector of LONG64 pixel addresses specifying the location
;					of slitpixels in the input image
;
;-

; Load the flatfield common block, and the slit pixel common block
common IRIS_PREP_SLITPIX_CB, slitpix_str
common IRIS_PREP_FLAT_CB, flat_index, flat_loaded, flat_sji_2832, flat_sji_2796, $
	flat_sji_5000w, flat_sji_1330, flat_sji_1400, flat_sji_1600w, $
	flat_fuv_detector, flat_fuv_spatial, flat_nuv_detector, flat_nuv_spatial, $
	flat_sji_nuv
sswdata = CONCAT_DIR('$SSW_IRIS','data')

; Read in the index of all available flats if necessary
; There should be only one index, but in case there are multiples it uses the
; one with the most recent date in its filename
if N_TAGS(flat_index) eq 0 then begin
	flatinds = FILE_SEARCH(CONCAT_DIR(sswdata, '*flat.genx'))
	flatind_tai = ANYTIM2TAI(FILE2TIME(STRMID(FILE_BASENAME(flatinds), 0, 15)))
	latest = WHERE(flatind_tai eq MAX(flatind_tai))
	RESTGEN, file = flatinds[latest], flat_index
endif

; Read in the list of all available slit pixel maps if necessary
; There should be only one index, but in case there are multiples it uses the
; one with the most recent date in its filename
if N_TAGS(slitpix_str) eq 0 then begin
	slitinds = FILE_SEARCH(CONCAT_DIR(sswdata, '*slitpix.genx'))
	slitind_tai = ANYTIM2TAI(FILE2TIME(STRMID(FILE_BASENAME(slitinds), 0, 15)))
	latest = WHERE(slitind_tai eq MAX(slitind_tai))
	RESTGEN, file = slitinds[latest], str = slitpix_str
endif

; Determine the flatfield recnum needed
taiobs = MIN(ANYTIM2TAI(iindex.t_obs))
imgpath = iindex[0].img_path
if STRMID(imgpath, 0, 3) ne 'SJI' then imgpath = imgpath + '_DETECTOR'
indexind = WHERE(imgpath eq flat_index.img_path)
thispath_index = flat_index[indexind]
taidiff = ABS(taiobs - thispath_index.filetai)
index_addr = WHERE(taidiff eq MIN(taidiff))
slitpix_recnum = thispath_index[index_addr[0]].recnum

; Use that flatfield recnum to look up the slitpixel list
tagind = WHERE(TAG_NAMES(slitpix_str) eq 'F' + STRTRIM(slitpix_recnum, 2))
slitpix_list = slitpix_str.(tagind[0])

RETURN, slitpix_list

end