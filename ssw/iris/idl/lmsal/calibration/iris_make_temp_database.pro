pro IRIS_MAKE_TEMP_DATABASE, date, $
	outpath = outpath, win = win, rms = rms, pstop = pstop, short = short, $
	printmnems = printmnems, long = long, fname = fname
	
;+
; 
; Generates a text file giving the temperatures from the IRIS
; housekeeping as a function of time.
;
; INPUT:
;	date	-	The day for which to generate the file
;
; KEYWORDS:
;	outpath=	-	The path for the text file that is produced (the 
;					filename format is YYYYMMDD_iris_temp.txt)
;
;	win=	-	Size of the boxcar smoothing window, in seconds. Defaults to 
;				60; if set to 0, no smoothing is done
;
;	rms=	-	Set if you also want to produce a text file with the std. dev.
;				of each temperature over the same win interval
;
;-

nullval = -9999d
mnem_map = [['SECONDARY_MIRROR_HOUSING_HOZ4_CONTROL_TS02', 'IT02SMIR'], $
	['PRIMARY_MIRROR_MOUNT_NUB_TS01', 'IT01PMRF'], $
	['TELESCOPE_FORWARD_RING_NEAR_GT_MOUNT_TS04', 'IT04TELF'], $
	['PRIMARY_MIRROR_TS03', 'IT03PMRA'], $
	['TELESCOPE_MID_RING_NEAR_GT_MOUNT_TS06', 'IT06TELM'], $
	['TELESCOPE_AFT_FLANGE_NEAR_PM_TS05', 'IT05TELA'], $
	['GUIDE_TELESCOPE_WEDGE_HC_MOTOR_TS08', 'IT08GTWM'], $
	['PZT_PRENEGAMP_HOUSING_TS07', 'IT07PZTA'], $
	['CEB_1_EXTERNAL_TS10', 'IT10CEBN'], $
	['GUIDE_TELESCOPE_BARLOW_HOZ3_CONTROL_TS09', 'IT09GTB'], $
	['HOPA_ON_DOOR_TS12', 'IT12HOPA'], $
	['CEB_2_EXTERNAL_TS11', 'IT11CEBP'], $
	['SPECTROGRAPH_OPTICS_PACKAGE_HOZ5_CONTROL_TS14', 'IT14SPPX'], $
	['FRONT_APERTURE_ASSEMBLY_NEAR_HINGE_TS13', 'IT13FRAP'], $
	['SPECTROGRAPH_OPTICS_PACKAGE_HOZ7_CONTROL_TS16', 'IT16SPNX'], $
	['SPECTROGRAPH_OPTICS_PACKAGE_HOZ6_CONTROL_TS15', 'IT15SPPY'], $
	['SPECTROGRAPH_OPTICS_PACKAGE_TS18', 'IT18SPPZ'], $
	['SPECTROGRAPH_OPTICS_PACKAGE_HOZ8_CONTROL_TS17', 'IT17SPNY'], $
	['PRIMARY_MIRROR_ABSORBER_PLATE_HOZ2_CONTROL_TS20', 'IT20PMRA'], $
	['SPECTROGRAPH_OPTICS_PACKAGE_TS19', 'IT19SPNZ'], $
	['IEB_POWER_CONVERTER_BASE_TS22', 'IT22PCBP'], $
	['TELESCOPE_TUBE_NEAR_AFT_FLANGE_TS21', 'IT21TELD'], $
	['IEB_PROCESSOR_BOARD_CPU_TS24', 'IT24CPU'], $
	['IEB_POWER_CONVERTER_HOT_COMP_TS23', 'IT23PCPC'], $
	['IEB_MECH_BOARD_1_HOTTEST_COMPONENT_TS26', 'IT26MEC1'], $
	['IEB_ON_BOARD_CLOCK_OSCILLATOR_TS25', 'IT25OSC'], $
	['SECONDARY_MIRROR_ABSORBER_PLATE_TS28', 'IT28SMRA'], $
	['IEB_MECH_BOARD_2_HOTTEST_COMPONENT_TS27', 'IT27MEC2'], $
	['TELESCOPE_MID_RING_AT_GT_MOUNT_TS30', 'IT30TMGT'], $
	['TELESCOPE_FWD_FLANGE_AT_GT_MOUNT_TS29', 'IT29TFGT'], $
	['CCD2_FUV', 'BT12CCD2'], $
	['CCD1_FUV', 'BT11CCD1'], $
	['CCD4_SJI', 'BT14CCD4'], $
	['CCD3_NUV', 'BT13CCD3'], $
	['AFT', 'BT01TELA'], $
	['FOWARD', 'BT02TELF'], $
	['PRIMARY_MIRROR_ABSORBER_PLATE', 'BT03PMAP'], $
	['PRIMARY_MIRROR_RADIATOR', 'BT04PMRD'], $
	['GUIDE_TELESCOPE', 'BT05GTEL'], $
	['CEB_ON_THE_POSX_AXIS', 'BT06CBPX'], $
	['CEB_ON_THE_NEGX_AXIS', 'BT07CBNX'], $
	['SPECTROGRAPH_OPTICS_PACKAGE_POSX_AXIS', 'BT08SPPX'], $
	['SPECTROGRAPH_OPTICS_PACKAGE_NEGX_AXIS', 'BT09SPNX'], $
	['HOPA', 'BT10HOPA'], $
	['ELECTRONICS_BOX', 'BT15IEB'], $
	['PRIMARY_MIRROR_SUPPORT_PLATE', 'BT16PMSP'], $
	['SECONDARY_MIRROR_SUPPORT_PLATE', 'BT17SMAP'], $
	['CCD2_FUV2_OPERATING', 'ITF2CCD2'], $
	['CCD1_FUV1_OPERATING', 'ITF1CCD1'], $
	['CCD3_NUV_OPERATING', 'ITNUCCD3'], $
	['CCD4_SJI_OPERATING', 'ITSJCCD4']]

if KEYWORD_SET(printmnems) then begin
	rocknames = RD_TFILE('/Volumes/disk2/data/iris/iris_prep/iris_temps_fits.txt', 2)
	numtemp = N_ELEMENTS(mnem_map)/2
	OPENW, lun, /get, '/sanhome/boerner/public_html/iris/temps/mnemonics.txt'
	PRINTF, lun, 'Long Mnemonic', 'Short Mnemonic', 'FITS Keyword', form = '(a50, a30, a15)'
	for i = 0, numtemp - 1 do begin
		rockind = WHERE(rocknames[1,*] eq mnem_map[1,i])
		PRINTF, lun, mnem_map[0,i], rocknames[0,rockind], mnem_map[1,i], form = '(a50, a30, a12)'
	endfor
	FREE_LUN, lun
	RETURN
endif

if KEYWORD_SET(pstop) then STOP
if N_ELEMENTS(win) eq 0 then win = 60
if N_ELEMENTS(date) eq 0 then date = '2013-jul-17'
if N_ELEMENTS(outpath) eq 0 then outpath = '/sanhome/boerner/public_html/iris/temps'
if not KEYWORD_SET(long) then short = 1

t0 = STRMID(ANYTIM2UTC(/ccsds, date), 0, 10)
temps = IRIS_TIME2TEMPS(/all, t0+'T00:00:00Z', t0+'T23:59:59.99Z')
tname = TIME2FILE(date, /date)

if win eq 0 then begin
	PRINT, 'Currently option to run without smoothing is disabled'
	RETURN
endif
numtimes = 86400/win
timegrid = DINDGEN(numtimes) * win + win/2. + ANYTIM2TAI(t0)

inst_temps = temps.inst
inst_tai = ANYTIM2TAI(inst_temps.date_obs)
inst_names = TAG_NAMES(inst_temps)
numt_inst = N_ELEMENTS(inst_names) - 1
inst_tags = STRARR(numt_inst)
if KEYWORD_SET(short) then for i = 1, numt_inst do $
	inst_tags[i-1] = mnem_map[1,WHERE(mnem_map[0,*] eq inst_names[i])] $
	else inst_tags = inst_names

sc_temps = temps.sc
sc_tai = ANYTIM2TAI(sc_temps.date_obs)
sc_names = TAG_NAMES(sc_temps)
numt_sc = N_ELEMENTS(sc_names) - 1
sc_tags = STRARR(numt_sc)
if KEYWORD_SET(short) then for i = 1, numt_sc do $
	sc_tags[i-1] = mnem_map[1,WHERE(mnem_map[0,*] eq sc_names[i])] $
	else sc_tags = sc_names

ceb_temps = temps.ceb
ceb_tai = ANYTIM2TAI(ceb_temps.date_obs)
ceb_names = TAG_NAMES(ceb_temps)
numt_ceb = N_ELEMENTS(ceb_names) - 1
ceb_tags = STRARR(numt_ceb)
if KEYWORD_SET(short) then for i = 1, numt_ceb do $
	ceb_tags[i-1] = mnem_map[1,WHERE(mnem_map[0,*] eq ceb_names[i])] $
	else ceb_tags = ceb_names
	
if KEYWORD_SET(pstop) then STOP

if KEYWORD_SET(short) then ww = '10' else ww = '50'
fname = tname + '_iris_temp.txt'
outfile = CONCAT_DIR(outpath, fname)
OPENW, lun, /get_lun, outfile
ncol = numt_inst + numt_sc + numt_ceb + 1
PRINTF, lun, 'DATE_OBS', '', inst_tags[*], sc_tags[*], ceb_tags[*], $
	form = '(a8,a11,' + STRTRIM(ncol-1, 2) + 'a'+ww+')'
datarr = DBLARR(numt_inst + numt_sc + numt_ceb, numtimes) + nullval

if KEYWORD_SET(rms) then begin
	outfile_rms = CONCAT_DIR(outpath, tname + '_iris_temp_rms.txt')
	OPENW, lunrms, /get_lun, outfile_rms
	PRINTF, lunrms, 'DATE_OBS', inst_tags[*], sc_tags[*], ceb_tags[*], form = '(a20,' + STRTRIM(ncol-1, 2) + 'a'+ww+')'
	rmsarr = DBLARR(numt_inst + numt_sc + numt_ceb, numtimes) + nullval
endif

if win ne 0 then for i = 0l, numtimes - 1 do begin
	tmin = timegrid[i] - win/2.
	tmax = timegrid[i] + win/2.
	inst_in = WHERE(inst_tai gt tmin and inst_tai le tmax, numinst)
	if numinst gt 0 then for j = 0l, numt_inst - 1 do begin
			datarr[j,i] = MEAN(inst_temps[inst_in].(j+1))
	endfor
	sc_in = WHERE(sc_tai gt tmin and sc_tai le tmax, numsc)
	if numsc gt 0 then for j = 0l, numt_sc - 1 do begin
			datarr[j+numt_inst,i] = MEAN(sc_temps[sc_in].(j+1))
	endfor
	ceb_in = WHERE(ceb_tai gt tmin and ceb_tai le tmax, numceb)
	if numceb gt 0 then for j = 0l, numt_ceb - 1 do begin
			datarr[j+numt_inst + numt_sc,i] = MEAN(ceb_temps[ceb_in].(j+1))
	endfor
	PRINTF, lun, STRMID(ANYTIM2UTC(/ccsds, timegrid[i]), 0, 19), datarr[*,i], $
		form = '(a19,' + STRTRIM(ncol-1, 2) + 'f'+ww+'.2)'
	if KEYWORD_SET(rms) then begin
		if numinst gt 1 then for j = 0l, numt_inst - 1 do begin
			rmsarr[j,i] = STDEV(inst_temps[inst_in].(j+1))
		endfor
		if numsc gt 1 then for j = 0l, numt_sc - 1 do begin
			rmsarr[j+numt_inst,i] = STDEV(sc_temps[sc_in].(j+1))
		endfor
		if numceb gt 1 then for j = 0l, numt_ceb - 1 do begin
			rmsarr[j+numt_inst + numt_sc,i] = STDEV(ceb_temps[ceb_in].(j+1))
		endfor
		PRINTF, lunrms, STRMID(ANYTIM2UTC(/ccsds, timegrid[i]), 0, 19), rmsarr[*,i], $
			form = '(a20,' + STRTRIM(ncol-1, 2) + 'f'+ww+'.2)'
	endif
endfor

FREE_LUN, lun
if KEYWORD_SET(rms) then FREE_LUN, lunrms

end




