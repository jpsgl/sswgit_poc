function IRIS_PREP_ROT, params, mag, roll, xref, yref, xshift, yshift, $
	cx = cx, cy = cy
	
;+
;
; Adjusts a geometric/wavelength correction parameter structure to also apply
; magnification and roll (mimics the action of IDL's ROT procedure, but doesn't
; send the calculated corrections to POLY_2D)
;
; INPUTS:
;	params	-	The starting parameter structure, including .dmapx and .dmapy
;	mag	-	the magnification to apply
;	roll	-	the roll to apply
;	xref	-	X pixel coordinates of the center of rotation/magnification
;	yref	-	Y pixel coordinates of the center of rotation/magnification
;	xshift	-	X pixel displacement to be applied prior to rotation/magnification
;	yshift	-	Y pixel displacement to be applied prior to rotation/magnification
;
; OUTPUTS:
;	cx	-	The (2x2) polynomial warping matrix for X coordinates
;	cy	-	The (2x2) polynomial warping matrix for Y coordinates
;
; RETURNS:
;	oparams	-	A new parameter structure, with revised .dmapx and .dmapy
;
;-

oparams = params

if N_ELEMENTS(xref) eq 2 then begin
	oparams.fuvs = IRIS_PREP_ROT(params.fuvs, mag, roll, xref[0], yref[0], xshift[0], yshift[0])
	oparams.fuvl = IRIS_PREP_ROT(params.fuvl, mag, roll, xref[1], yref[1], xshift[1], yshift[1])	
	RETURN, oparams
endif

; This code is copied from IDL's ROT function (does rotation and magnification)
theta = (roll mod 360) * !dpi/180   ;angle in degrees CLOCKWISE.
c = COS(theta)*mag      
s = SIN(theta)*mag
kx = -xref + c*xref - s*yref
ky = -yref + s*xref + c*yref
kk = 1./(1. + s^2/c^2)
cx = kk * [s/c^2*ky + kx/c, s/c^2, 1/c, 0.]
cy = kk * [-s/c^2*kx + ky/c, 1/c, -s/c^2, 0.]

if params.img_path eq 'ID' then begin
	; For slitjaws, where there is no default warping, just use the ROT matrix
	tcx = REFORM(cx, 2, 2)
	tcy = REFORM(cy, 2, 2)
	tcx[0,0] = cx[0,0] - xshift		;	Include shifts in the 0th order terms
	tcy[0,0] = cy[0,0] - yshift
	oparams.kx[0:1,0:1] = tcx
	oparams.ky[0:1,0:1] = tcy
	tmpx = tcx[0] + tcx[1]*oparams.dmapy + tcx[2]*oparams.dmapx + tcx[3]*oparams.dmapx*oparams.dmapy
	tmpy = tcy[0] + tcy[1]*oparams.dmapy + tcy[2]*oparams.dmapx + tcy[3]*oparams.dmapx*oparams.dmapy
	oparams.dmapx = tmpx			;	Now update the dmaps using the polynomials
	oparams.dmapy = tmpy
endif else begin
	; For spectra, the ROT matrix must be combined with the warping matrix
	; However, note that ONLY shifts and y-axis scaling is used; X axis scaling
	; and rotation are never applied to spectra
	scaleshift = ( (mag - 1)/mag ) * yref		; 	Y offset needed to scale around yref
	oparams.dmapx	= oparams.dmapx - xshift
	oparams.dmapy	= oparams.dmapy * mag - yshift + scaleshift
	oparams.kx[0,0] = oparams.kx[0,0] - xshift
	oparams.ky[0,0] = oparams.ky[0,0] - yshift + scaleshift
	oparams.ky[1,0] = oparams.ky[1,0] * mag
endelse

RETURN, oparams

end