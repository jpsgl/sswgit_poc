function IRIS_PREP_FID_FINDER, index, data, $
	msgs, $
	nomx1 = nomx1, nomx2 = nomx2, nomy1 = nomy1, nomy2 = nomy2, $
	fuvs = fuvs, fuvl = fuvl, loud = loud, $
	run_time = run_time, logdir = logdir

;+ 
;
; Given a header structure (or struct vector) and image (2D or 3D), returns an
; array giving the shift to put the fiducials in the nominal position.
; Also, writes the information out to a text file.
;
;-

if not KEYWORD_SET(loud) then loud = 0
if KEYWORD_SET(run_time) then t0 = SYSTIME(/sec)
write = KEYWORD_SET(logdir)

; Run IRIS_FID_FINDER to actually fit the fiducial locations
startval = [nomx1, nomy1, nomx2, nomy2]
fidstr = REFORM(IRIS_FID_FINDER(index, data, startval = startval, display = loud))

; Don't shift spectra in the spectral direction		
if STRMID(fidstr[0].type, 1, 2) eq 'UV' then begin
;	fidstr.x1 = nomx1[0]				;	But no reason not to return a value...
;	fidstr.x2 = nomx2[0]
	sxs = 0
endif else sxs = 1

numfiles = N_ELEMENTS(fidstr)
msgs = STRARR(numfiles)
shifter = FLTARR(2,numfiles)
; Pass through the identification of the FUV channel
if index[0].img_path eq 'FUV' then begin
	case 1 of
		KEYWORD_SET(fuvs)	:	fidstr.type = 'FUVS'
		KEYWORD_SET(fuvl)	:	fidstr.type = 'FUVL'
		else	:	PRINT, 'IRIS_PREP_FID_FINDER: Select /fuvs or /fuvl'
	endcase
endif

; Open text file to write output (and, if it's not there yet, print in the column heads)
if write then begin
	date_obs = STRMID(TIME2FILE(index[0].t_obs), 0, 8)
	txtpath = CONCAT_DIR(logdir, 'fid')
	if not FILE_EXIST(txtpath) then begin
		BOX_MESSAGE, ['IRIS_PREP_FID_FINDER: Output path does not exist!', txtpath]
	endif else begin
		filename = CONCAT_DIR(txtpath, date_obs + '_fid.txt')
		if not FILE_EXIST(filename) then begin
			OPENW, lun, /get, filename
			PRINTF, lun, TAG_NAMES(fidstr), 'WRITTEN', form = '(A25,15A10,A25)'
		endif else begin
			OPENW, lun, /get, filename, /append
		endelse
	endelse
endif

; Write the data to the text file and generate the HISTORY messages
for i = 0, numfiles-1 do begin
	if write then PRINTF, lun, fidstr[i], RELTIME(/now, out='CCSDS'), form = '(A25,i10,A10,13F10.3,A25)'
	found1 = fidstr[i].x1 gt 0 and fidstr[i].y1 gt 0
	found2 = fidstr[i].x2 gt 0 and fidstr[i].y2 gt 0
	if found1 then begin
		if found2 then begin
			shiftx = MEAN([nomx1[0] - fidstr[i].x1, nomx2[0] - fidstr[i].x2]) * sxs
			shifty = MEAN([nomy1[0] - fidstr[i].y1, nomy2[0] - fidstr[i].y2])
			msgs[i] = STRING('Found fiducials at [', $
				fidstr[i].x1, ',', fidstr[i].y1, '], [', $
				fidstr[i].x2, ',', fidstr[i].y2, ']; shift by [', $
				shiftx, ',', shifty, ']', $
				format = '(a20,f7.1,a1,f7.1,a4,f7.1,a1,f7.1,a13,f5.1,a1,f5.1,a1)')
		endif else begin
			shiftx = (nomx1[0] - fidstr[i].x1) * sxs
			shifty = nomy1[0] - fidstr[i].y1
			msgs[i] = STRING('Found fiducials at [', $
				fidstr[i].x1, ',', fidstr[i].y1, '], [-,-]; shift by [', $
				shiftx, ',', shifty, ']', $
				format = '(a20,f7.1,a1,f7.1,a20,f5.1,a1,f5.1,a1)')
		endelse
	endif else begin
		if found2 then begin
			shiftx = (nomx2[0] - fidstr[i].x2) * sxs
			shifty = nomy2[0] - fidstr[i].y2
			msgs[i] = STRING('Found fiducials at [-,-], [', $
				fidstr[i].x2, ',', fidstr[i].y2, ']; shift by [', $
				shiftx, ',', shifty, ']', $
				format = '(a28,f7.1,a1,f7.1,a13,f5.1,a1,f5.1,a1)')
		endif else begin
			shiftx = 0
			shifty = 0
			msgs[i] = 'Could not find fiducials; No shift applied'
		endelse
	endelse
	shifter[*,i] = [shiftx, shifty]
endfor

if write then FREE_LUN, lun

if KEYWORD_SET(run_time) then $
	PRINT, 'IRIS_PREP_FID_FINDER: Run time ', SYSTIME(/sec) - t0, form = '(A30,F6.2)'

RETURN, shifter

end
