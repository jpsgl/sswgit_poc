pro IRIS_FLAT_CATALOG, flatindex

;+
;
; Prints out a description of the currently-defined IRIS flatfields
;
;-

IRIS_PREP_READ_FLATS

common IRIS_PREP_FLAT_CB, flat_index, flat_loaded, flat_sji_2832, flat_sji_2796, $
	flat_sji_5000w, flat_sji_1330, flat_sji_1400, flat_sji_1600w, $
	flat_fuv_detector, flat_fuv_spatial, flat_nuv_detector, flat_nuv_spatial, $
	flat_sji_nuv

imgpaths = TAG_NAMES(flat_loaded)
npath = N_ELEMENTS(imgpaths)

for i = 0, npath - 1 do begin
	PRINT & PRINT, imgpaths[i]
	inpath = WHERE(flat_index.img_path eq imgpaths[i], numin)
	for j = 0, numin - 1 do begin
		thisflat = flat_index[inpath[j]]
		if flat_loaded.(i) eq thisflat.recnum then thistag = '*' else thistag = ' '
		PRINT, thistag, thisflat.recnum, thisflat.filetime, form = '(a5,i10,a20)'
	endfor
endfor

flatindex = flat_index

end