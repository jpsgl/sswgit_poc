function IRIS_PREP_CHECKSIZE, index, data, $
	datasize = datasize, sumspat = sumspat, sumsptrl = sumsptrl, imgpath = imgpath, $
	keyword_only = keyword_only, masks = masks

;+
;
; Given an IRIS index and data array, check it for some basic coherence and
; return an integer flag indicating any problems it may have in running through
; IRIS_PREP
;
; INPUTS:
;	index	-	Array of structures containing FITS headers
;	data	-	Array containing FITS image data (OPTIONAL; if not passed in, 
;				bit 4 is not checked)
;
; RETURNS: 
;	result -	An integer where each bit can be set to 1 to indicate a potential
;				problem. If all bits are 0 (result=0) then no problems were identified.
;		 Bitmask:
;		 1	-	Mix of img_paths in input index
;		 2	-	Mix of sumspat or naxis2 in input index
;		 4	-	Mix of sumsptrl or naxis1 in input index
;		 8	-	Data size does not match that predicted by sumspat, sumsptrl and img_path
;		 16 - 	Bad ISP (T_OBS negative, or no T_OBS)
;		 32 -	Win_flip parameter does not agree with the standard value predicted by 
;				the img_path (NUV or FUV)
;		 64	-	Win_flip parameter does not agree with the standard value predicted by 
;				the img_path (SJI)
;		 128-	Decompression error (QUALITY bit 0x04 set, or no quality)
;
; OUTPUT KEYWORDS:
;	datasize	-	Result of the IDL SIZE() function on the data
;	imgpath		-	Scalar string giving the imgpath of the first header
;	sumspat		-	Scalar int giving the sumspat of the first header
;	sumsptrl	-	Scalar int giving the sumsptrl of the first header
;	masks		-	Structure holding a list of BYTE vectors with flags showing
;					which image in the array violates each of the bit tests
;
;-

result = 0

if not KEYWORD_SET(keyword_only) then begin
	datasize = SIZE(data)
	data_ndim = datasize[0]
	data_type = datasize[data_ndim + 1]
	data_dim = datasize[1:data_ndim]
	checkdata = 1
endif else begin
	checkdata = 0
endelse

itags 		= TAG_NAMES(index)
imgpath		= index[0].img_path
sumspat		= index[0].sumspat
sumsptrl	= index[0].sumsptrl
naxis1		= index[0].naxis1
naxis2		= index[0].naxis2
numimg 		= N_ELEMENTS(index)

; Test for bit 1 (image_path mismatch)
diffpath = WHERE(index.img_path ne imgpath, numdiff)
if numdiff ne 0 then begin
	result = result + 1
	unique_i = UNIQ(index.img_path, sort(index.img_path))
	unique_strings = index[unique_i].img_path
	string_code = INTARR(N_ELEMENTS(string_data))
	for i=0l, N_ELEMENTS(unique_strings) - 1 do $
		string_code[WHERE(index.img_path eq unique_strings[i])] = i
	imgpath_hist = HISTOGRAM(string_code, min = 0, binsize = 1)
	imgpath_mode = unique_strings[(WHERE(imgpath_hist eq MAX(imgpath_hist)))[0]]
	imgpath = imgpath_mode
	imgpath_mask = index.img_path ne imgpath_mode
endif else begin
	imgpath_mask = BYTARR(numimg)
endelse

; Test for bit 2 (sumspat/naxis2 mismatch)
diff_sumspat	= WHERE(index.sumspat ne sumspat, numdiff)
diff_naxis2 	= WHERE(index.naxis2 ne naxis2, numdiff2)
if (numdiff + numdiff2) ne 0 then begin
	result = result + 2
	sumspat_hist = HISTOGRAM(index.sumspat, min = 0, binsize = 1)
	sumspat_mode = (WHERE(sumspat_hist eq MAX(sumspat_hist)))[0]
	sumspat = sumspat_mode
	naxis2_hist = HISTOGRAM(index.naxis2, min = 0, binsize = 1)
	naxis2_mode = (WHERE(naxis2_hist eq MAX(naxis2_hist)))[0]
	sumspat_mask = (index.sumspat ne sumspat) or (index.naxis2 ne naxis2_mode)
endif else begin
	sumspat_mask = BYTARR(numimg)
endelse

; Test for bit 3 (sumsptrl/naxis1 mismatch)
diff_sumsptrl	= WHERE(index.sumsptrl ne sumsptrl, numdiff)
diff_naxis1 	= WHERE(index.naxis1 ne naxis1, numdiff2)
if (numdiff + numdiff2) ne 0 then begin
	result = result + 4
	sumsptrl_hist = HISTOGRAM(index.sumsptrl, min = 0, binsize = 1)
	sumsptrl_mode = (WHERE(sumsptrl_hist eq MAX(sumsptrl_hist)))[0]
	sumsptrl = sumsptrl_mode
	naxis1_hist = HISTOGRAM(index.naxis1, min = 0, binsize = 1)
	naxis1_mode = (WHERE(naxis1_hist eq MAX(naxis1_hist)))[0]
	sumsptrl_mask = (index.sumsptrl ne sumsptrl) or (index.naxis1 ne naxis1_mode)
endif else begin
	sumsptrl_mask = BYTARR(numimg)
endelse

; Test for bit 4 (image size mismatch)
size_mask = BYTARR(numimg)
if (checkdata eq 1) then begin
	if imgpath eq 'FUV' then def_x = 4144 else def_x = 2072
	def_y = 1096
	expex = def_x / index[0].sumsptrl
	expey = def_y / index[0].sumspat
	if data_dim[0] ne expex or data_dim[1] ne expey then result = result + 8
endif

; Test for bit 5 (negative T_OBS, indicating bad ISP)
tobsind = WHERE(itags eq 'T_OBS', hastobs)
if hastobs gt 0 then begin
	badisp_mask = (STRMID(index.t_obs, 0, 1) eq '-')
endif else begin
	badisp_mask = BYTARR(numimg) + 1
endelse
result = result + 128 * MAX(badisp_mask)

; Test for bit 6 and 7 (win_flip mismatch)
case imgpath of
	'FUV'		:	def_flip = 1
	'NUV'		:	def_flip = 2
	'SJI_1330'	:	def_flip = 2
	'SJI_1400'	:	def_flip = 2
	'SJI_1600'	:	def_flip = 2
	'SJI_2796'	:	def_flip = 0
	'SJI_2832'	:	def_flip = 0
	'SJI_5000W'	:	def_flip = 0
	'NUV-SJI'	:	def_flip = 0
	else		:	def_flip = 0
endcase
if index[0].win_flip ne def_flip then begin
	if STRMID(imgpath, 0, 3) ne 'SJI' then begin
		result = result + 32
	endif else begin
		result = result + 64
	endelse
endif
flip_mask = index.win_flip ne def_flip

; Test for bit 8 (Quality bit indicating decompression error)
qualind = WHERE(itags eq 'QUALITY', hasqual)
if hasqual gt 0 then begin
	decomp_mask = (index.quality and 4) / 4
endif else begin
	decomp_mask = BYTARR(numimg) + 1
endelse
result = result + 128 * MAX(decomp_mask)

anybit = (imgpath_mask + sumspat_mask + sumsptrl_mask + size_mask + $
	flip_mask + badisp_mask) gt 0
masks = {anybit : anybit, imgpath_mask : imgpath_mask, sumspat_mask : sumspat_mask, $
	sumsptrl_mask : sumsptrl_mask, size_mask : size_mask, flip_mask : flip_mask, $
	decomp_mask : decomp_mask, badisp_mask : badisp_mask}

RETURN, result

end
