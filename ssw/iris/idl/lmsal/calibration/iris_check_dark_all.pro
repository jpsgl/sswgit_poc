function IRIS_CHECK_DARK_ALL, t0, t1, $
	daywindow = daywindow, forceall = forceall, verbose = verbose, pstop = pstop

;+
;
;	Wrapper for IRIS_CHECK_DARK; returns a structure describing a set of images
;	that can be used to characterize the quality of the dark subtraction for a
;	given interval in the IRIS data.
;
; INPUTS:
;	t0	-	Start time to check for images to characterize dark subtraction. 
;			Any time format recognized by ANYTIM2UTC
;	t1	-	(OPTIONAL) End time of interval to check; defaults to t0 + 90 days
;
; RETURNS:
;	result	-	Structure giving filenames, headers, and flags describing the
;				dark images that should be examined to get a feel for how well
;				the dark subtraction was working over this time period.
;		.files	A 3x3x2 string array of filenames giving the name of the lev1 
;				file giving the best example of each case
;		.quality A 3x3x2 int array of flags indicating how good an example of
;				each type was found (3 is best, -1 means no suitable file found)
;		.headers A 3x3x2 structure array of header information for the best 
;				examples
;		.description	A 3x3x2 string array describing the characteristics of 
;						each "type" of dark frame that is being sought.
;
; KEYWORDS:
;	daywindow=	-	(INT input) Number of days to look through for each 
;					iteration; defaults to 3 (larger is faster but might
;					break the JSOC)
;	/forceall	-	(SWITCH) If set, then it tries very hard to find an example 
;					of each type of image that might be used for dark checking. 
;					By default, it just returns what it finds in the
;					middle of the specified interval
;	/verbose	-	(SWITCH) If set, then some stuff is printed (including
;					the results of each iteration through the window)
;	/pstop	-	(SWITCH) If set, then stop in routine for debugging
;
;-

tt0 = SYSTIME(/sec)
if N_ELEMENTS(daywindow) eq 0 then daywindow = 3
if N_ELEMENTS(t0) eq 0 then t0 = '2015-01-01'
if N_ELEMENTS(t1) eq 0 then t1 = ANYTIM2UTC(/ccsds, RELTIME(t0, days = 90))
if N_ELEMENTS(verbose) eq 0 then verbose = 0

; Initialize the result structure
paths = ['SJI', 'NUV', 'FUV']
sum = ['1x1', '2x2', '4x4']
sums = [ [sum], [sum], [sum] ]
sums[2,2] = '1x2'
exps = ['long', 'short']
description = STRARR(3,3,2)
for i = 0, 2 do for j = 0, 2 do for k = 0, 1 do $
	description[i,j,k] = paths[i] + ',' + sums[j,i] + ',' + exps[k]

dummy = {fsn : 0l, t_obs : '', quality : 0l, win_flip : 0, $
	exptime : 0., crval1 : 0., crval2 : 0., crval3 : 0., sat_rot : 0., $
	sumspat : 0., sumsptrl : 0., img_path : ''}
dummyhdr = REPLICATE(dummy, 3, 3, 2)	
result = CREATE_STRUCT('files', STRARR(3,3,2), 'quality', INTARR(3,3,2) - 2, $
	'headers', dummyhdr, 'description', description)

; Set up the timing of the samples
tai0 = ANYTIM2TAI(t0)
tai1 = ANYTIM2TAI(t1)
taiwin = daywindow * 86400d
taimid = (tai0 + tai1) / 2d
maxcount = CEIL(tai1 - tai0) / taiwin
if KEYWORD_SET(forceall) then maxiter = maxcount else maxiter = 5

; Loop through intervals of daywindow length until you've found everything, or 
; you've exceeded the default number of search intervals.
count = 0
while (count lt maxiter) do begin
	countwin = (count + 1)/2 * (1 - 2 * ((count mod 2) eq 0))
	tguess = ANYTIM2UTC(/ccsds, taimid + countwin * taiwin + taiwin / 2d)
	if KEYWORD_SET(verbose) then PRINT, countwin, tguess, form = '(i10,a40)'
	thisfiles = IRIS_CHECK_DARK(tguess, daywindow = daywindow, $
		silent = (verbose eq 0), headers = headers, qs = qs)
	if N_ELEMENTS(thisfiles) gt 1 then begin
		improved_mask = qs gt result.quality
		improved = WHERE(improved_mask gt 0, numimproved)
		if KEYWORD_SET(pstop) then STOP
		if numimproved gt 0 then for i = 0, numimproved - 1 do begin
			; Assign new values into result structure
			result.files[improved[i]] = thisfiles[improved[i]]
			result.quality[improved[i]] = qs[improved[i]]
			result.headers[improved[i]] = headers[improved[i]]
		endfor
	endif else begin
		PRINT, 'IRIS_CHECK_DARK: Got a JSOC timeout, I think; skipping this one'
		if count eq 0 then begin
			PRINT, 'Try re-running IRIS_CHECK_DARK_ALL with a smaller daywindow'
			RETURN, ''
		endif
	endelse
	if MIN(result.quality) ge 2 then count = maxiter
	count = count + 1
endwhile
	
if KEYWORD_SET(verbose) then PRINT, 'IRIS_CHECK_DARK_ALL run time : ', $
	SYSTIME(/sec) - tt0, form = '(a40, f10.1)'
	
RETURN, result

end