function IRIS_PREP_GET_DELTAFLAT, header, msg, $
	path = path

;+
;
; INPUT:
;	header	-	FITS header structure for the image whose correction parameters 
;				are desired
;
; OUTPUT:
;	msg	-	a string describing the resulting flatfield record; can be appended 
;			to the FITS header if/when the correction is applied
;
; RETURNS:
;	result	-	a structure giving the geometric/wavelength calibration parameters
;				to be passed to IRIS_GEOWAVE_CORRECT
;
; KEYWORDS:
;	path=	-	path to the location of the geowave_correct database
;
;
;-

if N_ELEMENTS(path) eq 0 then path = GET_LOGENV('SSW_IRIS_DATA')

;+++++++++++++++++++++++++++++++++++++++++
; Currently handles array inputs by 
; calling itself recursively; not very 
; efficient, but we'll sort that out later
;-----------------------------------------
numhead = N_ELEMENTS(header)
if numhead gt 1 then begin
	result0 = IRIS_PREP_GET_DELTAFLAT(header[0], path = path)
	result = REPLICATE(result0, numhead)
	for i = 1, numhead - 1 do result[i] = IRIS_PREP_GET_GEOWAVE(header[i], path = path)
	RETURN, result
endif

camtype = header.instrume
sumspat = header.sumspat
sumspec = header.sumsptrl
flatrec = header.flat_rec


;...

RETURN, result

end