function IRIS_PREP_SHIFT_READER, iindex, fidmsg, wmsg, $
	nomx = nomx, nomy = nomy

;+
;
;	Given an image header, look up the wavelength and fiducial shift that needs 
;	to be applied (in pixels) to take out orbital and temperature-dependent shifts
;	in X and Y based on JPW's IRIS_ISP2WCS
;
; INPUT:
;	iindex	-	The header structure for the image
;
; OUTPUT:
;	wmsg	-	String constant to put into the image header describing the
;				wavelength correction
;	fidmsg	-	String constant to put into the image header describing the
;				fiducial correction
;
; KEYWORDS:
;	nomx	-	The nominal (desired) value of CRPIX1; for spectra, this is the
;				target pixel for the reference wavelength (for SJIs it is the
;				standard X coordinate of the slit)
;
;	nomy	-	The nominal (desired) value of CRPIX2; this is the target pixel
;				for the center of the slit (midpoint between fiducials)
;
; RETURNS:
;	result	-	A 2-element FLOAT array giving the X and Y shift that should be 
;				applied
;				(NOTE that if the input iindex is an FUV header, then result is
;				a 2x2 array for [FUVS, FUVL], and the msg outputs are also 
;				2-element string arrays
;	
;-


oindex = iindex
IRIS_ISP2WCS, oindex

xshift = nomx[0] - (oindex.crpix1 - 1)
yshift = nomy[0] - (oindex.crpix2 - 1)
result = [xshift, yshift]

case iindex.instrume of
	'FUV'	:	begin
		yshift_l = nomy[1] - (oindex.crpix2a - 1)
		fidmsg_s = STRING('FUVS Fiducial midpoint Y shift [pix]: ', yshift, form = '(a40,f9.2)')
		fidmsg_l = STRING('FUVL Fiducial midpoint Y shift [pix]: ', yshift_l, form = '(a40,f9.2)')
		fidmsg = [fidmsg_s, fidmsg_l]
		; Wavelength shift is not currently computed separately for FUVS and FUVL
		xshift_l = xshift
		wmsg = STRING('O I line wavelength shift [pix]: ', xshift, form = '(a40,f9.2)')
		wmsg = [wmsg, wmsg]
		result = [[xshift, yshift], [xshift_l, yshift_l]]
	end
	'NUV'	:	begin
		wmsg = STRING('Ni I line wavelength shift [pix]: ', xshift, form = '(a40,f9.2)')
		fidmsg = STRING('Fiducial midpoint Y shift [pix]: ', yshift, form = '(a40,f9.2)')
	end
	else	:	begin
		wmsg = 'No wavelength shift applied to SJI'
		fidmsg = STRING('Fiducial midpoint shift X,Y [pix]: ', xshift, yshift, form = '(a40,2f9.2)')
	end
endcase

RETURN, result

end