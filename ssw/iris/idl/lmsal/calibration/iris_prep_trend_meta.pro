pro IRIS_PREP_TREND_META, folder, $
	loud = loud, silent = silent, $
	obses = obses, versions = versions, nrt = nrt, result = result

;+
;
; Add a description.txt file to the subdirs in the specified input folder
;
;-

t0 = SYSTIME(/sec)

result = ''
if not KEYWORD_SET(silent) then silent = 0
if KEYWORD_SET(nrt) then ds = 'iris.lev1_nrt' else ds = 'iris.lev1'
CD, folder, current = old_dir
CD, current = folderout
sepstr = STRSPLIT(/extract, folderout, '/')
timestr = FILE2TIME(sepstr[-1], /ccsds)
tai = ANYTIM2TAI(timestr)

if N_ELEMENTS(obses) eq 0 then obses = IRIS_TIME2TIMELINE(timestr, timestr)

; get metadata; to be on the safe side vis-a-vis JSOC queries, break it into
; chunks of four hours
nchunks = CEIL(obses.duration / (3600. * 4))
for i = 0, nchunks - 1 do begin
	tstart = TAI2UTC( ANYTIM2TAI(obses.date_obs) + i * (3600. * 4.) - 0.1, /ccsds )
	tstop = TAI2UTC( ( ANYTIM2TAI(obses.date_obs) + (i+1) * (3600. * 4.) ) < $
		ANYTIM2TAI(obses.date_end), /ccsds )
;RPT removed files/files_only as per my email and Sam's tip.
	SSW_JSOC_TIME2DATA, tstart, tstop, thisdrms, /jsoc2, $
		ds = ds, silent = silent,  $
		key='sat_rot,exptime,fsn,ophase,t_obs,img_path,sumspat,sumsptrl'
	if i eq 0 then begin
		drms = thisdrms 
		drmstemplate = thisdrms[0]
	endif else begin
		drmsout = REPLICATE(drmstemplate, N_ELEMENTS(thisdrms))
		STRUCT_ASSIGN, thisdrms, drmsout
		drms = [drms, drmsout]
	endelse
endfor

; Now pull some relevant stuff out of the metadata
; RPT obs 3800 can have just one image,lowere from  N_ELEMENTS(drms) gt 1.
if N_TAGS(drms) gt 0 and N_ELEMENTS(drms) gt 0 then begin
;but, fixes for stdev of one element...
        rollrms = 0
	roll = MEAN(drms.sat_rot)
	sumspatrms = 0
	exptimerms = 0
	exptime = MEAN(drms.exptime)
        sumsptrl_rms = 0
	
	ref_fsn = drms[0].fsn
	ophase = drms[0].ophase
	ophase_tai = ANYTIM2TAI(drms[0].t_obs)
	nn = WHERE(drms.img_path ne 'FUV', numn)
	ff = WHERE(drms.img_path eq 'FUV', numf)
	sumspat = MEAN(drms.sumspat)

	sumsptrl = MEAN(drms[nn].sumsptrl)

        if N_ELEMENTS(drms) gt 1 then begin
           sumspatrms = STDEV(drms.sumspat)
           sumsptrl_rms = STDEV(drms.sumsptrl) 
           rollrms = STDEV(drms.sat_rot)
           exptimerms = STDEV(drms.exptime)
        endif



	if numf gt 0 then begin
		sumsptrlf = MEAN(drms[ff].sumsptrl) 
		if numf gt 1 then sumsptrlf_rms = STDEV(drms[ff].sumsptrl) else sumsptrlf_rms = 0.
	endif else begin
		sumsptrlf = sumsptrl
		sumsptrlf_rms = 0.
	endelse	
endif else begin
	CD, old_dir
	PRINT, folder, ' Not enough DRMS records; returning...'
	RETURN
endelse

result = CREATE_STRUCT(obses, 'timestr', timestr, 'tai', tai, 'ref_fsn', ref_fsn, $
	'roll', roll, 'sumspat', sumspat, 'sumsptrl', sumsptrl, 'sumsptrlf', sumsptrlf, $
	'exptime', exptime, 'ophase', ophase, 'ophase_tai', ophase_tai, $
	'rollrms', rollrms, 'exptimerms', exptimerms, 'sumspatrms', sumspatrms, $
	'sumsptrl_rms', sumsptrl_rms, 'sumsptrlf_rms', sumsptrlf_rms)
	
rtags = TAG_NAMES(result)
ntags = N_ELEMENTS(rtags)

OPENW, lun, /get, 'description.txt'
PRINTF, lun, rtags, form = '(2a27,4a12,a100,a27,2a12,4a10,2a10,a14,5a14)'
PRINTF, lun, result, form = '(2a27,2i12,2f12.3,a100,a27,2i12,f10.2,3i10,f10.3,f10.4,f14.1,5f14.3)'
FREE_LUN, lun

SAVEGEN, file = 'description.genx', result

if KEYWORD_SET(versions) then begin
	pathsort = SORT(drms.img_path)
	upath = SSW_UNIQ(drms[pathsort].img_path)
	; Read and prep one image of each image path. Is this useful? Eh.
	for i = 0, N_ELEMENTS(upath) - 1 do begin
		
	endfor
endif

CD, old_dir

if KEYWORD_SET(loud) then PRINT, 'IRIS_PREP_TREND_META: Finished in ', $
	STRING(SYSTIME(/sec) - t0, form = '(f9.2)'), ' sec'

end
