pro focus_calibrate_batch,                           $
    focus_calibdb_dir=focus_calibdb_dir,             $
    focus_calibdb_file=focus_calibdb_file,           $
    focus_cmddb_dir=focus_cmcdb_dir,                 $ 
    focus_cmddb_file=focus_cmcdb_file,               $ 
    base_output_dir=base_output_dir,                 $
    rebuild_histfocus_db=rebuild_histfocus_db,       $
    update_db=update_db,                             $
    perform_all=perform_all,                         $
    nuv=nuv,                                         $
    fuv=fuv,                                         $
    wv1330=wv1330,                                   $
    wv1400=wv1400,                                   $
    outdir=outdir,                                   $
    out_ptg_dir=out_ptg_dir,                         $
    out_dir_fits=out_dir_fits,                       $
    num_complete_iterations=num_complete_iterations, $
    iterate_to_completion=iterate_to_completion

;
;+
;   Name: focus_calibrate_batch
;
;   Purpose: 
;      Run Ted's IRIS focus calibration routines on level 1 data taken by a focus obs, generate 
;        intermediate data products:
;      1.) cleaned up, sj line removed, but not warped fits files, placed in out_dir_fits directory
;      2.) focus test (multiple foci, sharpness plots) gif images, placed in out_ptg_dir directory 
;      3.) focus mosaic .gifs (one for each of the four wavelengths) showing best mean, median, stdev 
;          for each wavelength.
;      4.) IRIS focus trending gif
;          Note: Although focus_calibrate_patch can be run from the IDL command line, it is intended to
;          be called on a daily basis as a cron job that implements sswdb_batch.  There are two datasets
;          used and updated by focus_calibrate_batch as needed and when run: one to maintain the record of 
;          calculated best foci for both 1330 and 1400 wavelengths; the other to record when the standard
;          focus for IRIS is changed.  Both data sets are used in producing the final IRIS Slit Jaw Image
;          Focus Trending plots.
;
;   Input Parameters:
;      None
;
;   Keyword Parameters:
;      focus_calibdb_dir       - the directory to look for the genx file of historical pointing values to restore (there will be a default)
;      focus_calibdb_file      - the genx filename of historical values to restore (there will be a default)
;      focus_cmddb_dir         - 
;      focus_cmddb_file        - 
;
;      rebuild_histfocus_db    - make this a seperate function invoked from this proc. Currently not implemented.
;      update_db               - add the best focus values and date calculate from this run of focus_calibrate_batch to genx file of 
;                                recorded focus values.
;      perform_all             - create mosaic files for all 4 wavelengths: 1330, 1400, nuv, fuv
;      nuv                     - create a mosaic image and related data products for the nuv wavelength
;      fuv                     - create a mosaic image and related data products for the fuv wavelength
;      wv1330                  - create a mosaic image and related data products for the 1330 wavelength
;      wv1400                  - create a mosaic image and related data products for the 1400 wavelength
;      outdir                  - output directory for the IRIS Slit Jaw Image Focus Trending .gif file
;      out_ptg_dir             - output directory for mosaic and .gif intermediate data products
;      out_dir_fits            - output directory for clean up, slit jaw line removed, but non-geometrically modified level 1 
;                                fits files generated as a result of best focus analysis
;      num_complete_iterations - 
;      iterate_to_present      -
;   Output:
;      Output various intermediate and final best focus and focus trend products. 
;      See items 1, 2, 3, 4 in "Purpose" category.
;
;   Calling Examples:
;      IDL> focus_calibrate_batch
;
;   Notes: I have the following line in my .cshrc file to make this:
;          setenv IRIS_DATA /jupiter/iris/data/
;   History:
;      08-feb-2016  - P.G.Shirts - original version
;      09-aug-2016  - P G Shirts
;-

;test start -- maybe remove later
update_db             = 1
iterate_to_completion = 0
;test end




if not keyword_set(num_complete_iterations) then num_complete_iterations = 0

;Determine if we will perform analysis on a complete or partial set of wavelengths.
if ((keyword_set(nuv)) OR (keyword_set(fuv)) OR (keyword_set(wv1330)) OR (keyword_set(wv1400))) then begin
  all_wavelengths = 0
endif else begin
  all_wavelengths = 1
endelse
;if not keyword_set(calib_id_desc) then calib_id_desc='Fine focus 2832, 1330, 2796, 1400; return to -131' ;this isn't a very good identifier

;-START 1------------- focus_calibdb_dir and focus_calibdb_file ---------------START 1-;
;These two keywords are meant to override the default genx file directory or genx file for reading.
;set to default value (if needed) and perform some filtering on focus_calibdb_dir, if set.
if not keyword_set(focus_calibdb_dir) then begin
  ;the below default directory is TEMPORARY during development: swap this line in when ready to test final configuration:  focus_calibdb_dir = getenv('SSWDB')+'/iris/focus_calib/'
;TODO
  ;original focus_calibdb_dir ='./' ; for now.  I wonder if the default directories shouldn't be written as a .genx file and placed in $SSWDB -- that way they could be modified over time without modifying the code.
  focus_calibdb_dir = '$SSW_IRIS_DATA/' ;for cronjob
endif else begin
  focus_calibdb_dir_TYPE = typename(focus_calibdb_dir)
  if 'STRING' ne  focus_calibdb_dir_TYPE then begin
    box_message, ['RETURNING from focus_calibrate_batch.pro:', 'keyword: focus_calibdb_dir must be of STRING type,', 'not type: '+ strtrim(focus_calibdb_dir_TYPE,2)] 
  endif
  if not file_test(focus_calibdb_dir,/directory) then begin
    box_message, ['RETURNING from focus_calibrate_batch.pro:', 'keyword: focus_calibdb_dir: '+ focus_calibdb_dir + ' is NOT a valid directory path']
  endif
endelse

;set default (if needed) and perform some filtering on focus_calibdb_file, if set.
if not keyword_set(focus_calibdb_file) then begin
  focus_calibdb_file = 'focus_calib_datastruct'
endif else begin
  focus_calibdb_file_TYPE = typename(focus_calibdb_file)
  if 'STRING' ne  focus_calibdb_dir_TYPE then begin
    box_message, ['RETURNING from focus_calibrate_batch.pro:', 'keyword: focus_calibdb_file must be of STRING type,', 'not type: '+ strtrim(focus_calibdb_dir_TYPE,2)]
  endif
  if not file_test(focus_calibdb_file) then begin
    box_message, ['RETURNING from focus_calibrate_batch.pro:', 'keyword: focus_calibdb_file: '+ focus_calibdb_dir + ' is NOT a valid file name']
  endif
endelse

;-END 1------------- focus_calibdb_dir and focus_histdb_file ---------------END 1-;

histdb_file_path = strjoin([focus_calibdb_dir, focus_calibdb_file],/single)
restgenx, h_c_d_template, historical_calib_data, file=histdb_file_path
;stop, 'stopped -- just restored historical_calib_data: look at h_c_d_template, and historical_calib_data'

;-START 2------------- focus_cmddb_dir and focus_cmddb_file ---------------START 2-;
;These two keywords are meant to override the default genx file directory or genx file for reading.
;set to default value (if needed) and perform some filtering on focus_cmddb_dir, if set.
if not keyword_set(focus_cmddb_dir) then begin

  ;the below default directory is TEMPORARY during development: swap this line in when ready to test final configuration:  focus_cmddb_dir = getenv('SSWDB')+'/iris/focus_calib/'
  ;focus_cmddb_dir ='./' ; for now.  I wonder if the default directories shouldn't be written as a .genx file and placed in $SSWDB -- that way they could be modified over time without modifying the code.
  focus_cmddb_dir='$SSW_IRIS_DATA/'
  ;TODO!
endif else begin
  focus_cmddb_dir_TYPE = typename(focus_cmddb_dir)
  if 'STRING' ne  focus_cmddb_dir_TYPE then begin
    box_message, ['RETURNING from focus_calibrate_batch.pro:', 'keyword: focus_cmddb_dir must be of STRING type,', 'not type: '+ strtrim(focus_cmddb_dir_TYPE,2)]
  endif
  if not file_test(focus_cmddb_dir,/directory) then begin
    box_message, ['RETURNING from focus_calibrate_batch.pro:', 'keyword: focus_cmddb_dir: '+ focus_cmddb_dir + ' is NOT a valid directory path']
  endif
endelse

;set default (if needed) and perform some filtering on focus_cmddb_file, if set.
if not keyword_set(focus_cmddb_file) then begin
  focus_cmddb_file = 'focus_commanded_datastruct'
endif else begin
  focus_cmddb_file_TYPE = typename(focus_cmddb_file)
  if 'STRING' ne  focus_cmddb_dir_TYPE then begin
    box_message, ['RETURNING from focus_calibrate_batch.pro:', 'keyword: focus_cmddb_file must be of STRING type,', 'not type: '+ strtrim(focus_cmddb_dir_TYPE,2)]
  endif
  if not file_test(focus_cmddb_file) then begin
    box_message, ['RETURNING from focus_calibrate_batch.pro:', 'keyword: focus_cmddb_file: '+ focus_cmddb_dir + ' is NOT a valid file name']
  endif
endelse

;-END 2------------- focus_cmddb_dir and focus_histdb_file ---------------END 2-;

cmddb_file_path = strjoin([focus_cmddb_dir, focus_cmddb_file],/single)
restgenx, iris_focus_commanded_info, historical_focus_vals, file=cmddb_file_path; 
historical_focus_vals = historical_focus_vals[0:1];test
;stop, 'stopped: look at modified historical_focus_vals'

n_restored_focus_elements = n_elements(historical_focus_vals)

last_focus_calibration_date = historical_calib_data[n_elements(historical_calib_data)-1].focus_obs_start_date
last_setfocus_date          = historical_focus_vals[n_elements(historical_focus_vals)-1].COMMANDED_FOCUS_START_DATE

;Determine minimum shared date for commanded foci and focus obs diagnostic runs.
last_historical_focus_vals_date = anytim2tai(historical_focus_vals[n_elements(historical_focus_vals)-1].commanded_focus_start_date)
last_historical_calib_data_date = anytim2tai(historical_calib_data[n_elements(historical_calib_data) - 1].FOCUS_OBS_START_DATE)

;TODO -- I don't think this logic is correct, yet.
;what am I trying to do here?  I think we should just go off of the last historical_calib_data
;if (last_historical_calib_data_date gt last_historical_focus_vals_date ) then begin ;
;  min_shared_focus_date =  historical_calib_data[n_elements(historical_calib_data)-1].focus_obs_start_date
;endif else begin
;  min_shared_focus_date = historical_focus_vals[n_elements(historical_focus_vals)-1].commanded_focus_start_date
;endelse
;stop, 'stopped -- replaced min_'
;original: timeline = iris_time2timeline(last_setfocus_date, reltime(/now), desc='focus');,/focus); this returns very few results because it captures the focus calibration obs only

;these are the most recent calibration focus runs since the last restored calibration focus run time.
timeline  = iris_time2timeline(last_focus_calibration_date, reltime(/now), desc='focus');,/focus); this returns very few results because it captures the focus calibration data capture runs only 
;stop, 'stopped: look at dates'
obsid1 = '4203600004';this is the obsid of one of the focus scans
obsid2 = '4204700089';this is the obsid of another of the focus scans -- add more when they appear.
index_focus_scans = where((timeline.obsid eq obsid1) or (timeline.obsid eq obsid2))
timeline = timeline(index_focus_scans)

num_focus_obs_run = n_elements(timeline) ;number of data collection obs run for analysis since our start time.
print, 'num_focus_obs_run for testing best focus: ', num_focus_obs_run

;FIND the change in focus commands
;original commanded_focus_dates = iris_time2timeline(last_focus_calibration_date, reltime(/now), /focus)  ; this returns very many results because it captures all focus commands

IRIS_FIRST_OBS = '01-nov-2013 04:00'
commanded_focus_dates = iris_time2timeline(IRIS_FIRST_OBS, reltime(/now), /focus)  ; this returns very many results because it captures all focus commands either at start of timeline or in the timeline (for whatever reason)
;stop, 'stop 2'

;original tl = iris_time2timeline(last_focus_calibration_date, reltime(/now), /focus, /timeline)
;original tl = iris_time2timeline(last_setfocus_date, reltime(/now), /focus, /timeline)
tl = iris_time2timeline(IRIS_FIRST_OBS, reltime(/now), /focus, /timeline)
dt_hours=ssw_deltat(tl.date_obs, ref=file2time(tl.timeline_name),/hour)
x=where((dt_hours ge 4.0) AND (dt_hours le 4.17)) ;
;original I think this is redundanct indiv_cmd_focus_days = commanded_focus_dates(x)
indiv_cmd_focus_days = tl(x)
;stop, 'stopped -- look at indiv_cmd_focus_days'
x1 = uniq(strmid(indiv_cmd_focus_days.date_obs,0,10)) ;these will be unique dates. Using the last values if there are more values. this should be ok because x filtered for time range;
indiv_cmd_focus_days=indiv_cmd_focus_days(x1);these are all of the set focus commands (one per timeline)
;stop, 'stopped -- look at new indiv_cmd_focus_days'
;the ranges look good -- but Ted's list is missing a 130 and 140 (one single obs of each)

;try removing the need for a savegen -- can we just create this directly from the return from the timemline call?
temp_test_for_comparison_historical_focus_vals = historical_focus_vals ; ;historical focus vals are restored from file and are the commanded change in default focus to IRIS.
if ( (n_elements(indiv_cmd_focus_days)-2) ge 0) then begin  ;if we have a non-empty set of commanded focus days, check to see if the focus changed
;  original for i=1, (n_elements(indiv_cmd_focus_days)-3) do begin  ;why is this "-3" -- what have I forgotten here? This needs a #define and an explanation.
  j=0
  temp_cmded_focus_vals_end_date = '01-01-2020'; testhistorical_focus_vals[n_restored_focus_elements-1].COMMANDED_FOCUS_END_DATE; was "end_date"

  ;I think this is over-writing the restored values -- need to no longer restore these values.
  historical_focus_vals = iris_focus_commanded_info  ;create
  historical_focus_vals(0).COMMANDED_FOCUS_START_DATE   = '01-nov-2013 04:00'
  historical_focus_vals(0).COMMANDED_FOCUS_END_DATE     = '23-nov-2013 04:00'
  historical_focus_vals(0).MEDIAN_MOSAIC_FOCUS_1330_STR =  -115
  historical_focus_vals(0).MEDIAN_MOSAIC_FOCUS_1400_STR =  -115

  n_restored_focus_elements = 1;temporary overwriting another vaariable

  ;hist_tem  = anytim2utc(historical_focus_vals[n_restored_focus_elements-1].COMMANDED_FOCUS_START_DATE)


  for i=1, (n_elements(indiv_cmd_focus_days)-2) do begin  ;why was this "-3" -- what have I forgotten here? This needs a #define and an explanation.
    indiv_temp = anytim2utc(strmid(indiv_cmd_focus_days[i].date_obs,0,11))
    if ((indiv_cmd_focus_days[i].position) ne (indiv_cmd_focus_days[i+1].position) and (indiv_cmd_focus_days[i].position ge -150) and (indiv_cmd_focus_days[i].position le -110) ) then begin
    historical_focus_vals = [historical_focus_vals,[iris_focus_commanded_info]]
    print, i, ': ', indiv_cmd_focus_days[i].date_obs, indiv_cmd_focus_days[i].position, j, ': ', indiv_cmd_focus_days[i+1].date_obs, indiv_cmd_focus_days[i+1].position
    historical_focus_vals[n_restored_focus_elements + j  ].COMMANDED_FOCUS_START_DATE   = anytim2cal(strmid(indiv_cmd_focus_days[i].date_obs,0,11), form=1)

    historical_focus_vals[n_restored_focus_elements + j-1].COMMANDED_FOCUS_END_DATE     = strmid(historical_focus_vals[n_restored_focus_elements+j].COMMANDED_FOCUS_START_DATE,0,15)
    historical_focus_vals[n_restored_focus_elements + j  ].COMMANDED_FOCUS_END_DATE     = '01-jan-2020 04:00' ;only need to do this if added historical_focus_vals element.
    historical_focus_vals[n_restored_focus_elements + j  ].MEDIAN_MOSAIC_FOCUS_1330_STR = strtrim(string(indiv_cmd_focus_days[i+1].position),2)
    historical_focus_vals[n_restored_focus_elements + j  ].MEDIAN_MOSAIC_FOCUS_1400_STR = strtrim(string(indiv_cmd_focus_days[i+1].position),2)

      ;historical_focus_vals = [historical_focus_vals,[iris_focus_commanded_info]]
      ;print, i, ': ', indiv_cmd_focus_days[i].date_obs, indiv_cmd_focus_days[i].position, j, ': ', indiv_cmd_focus_days[i+1].date_obs, indiv_cmd_focus_days[i+1].position
      ;historical_focus_vals[n_restored_focus_elements + j  ].COMMANDED_FOCUS_START_DATE   = anytim2cal(strmid(indiv_cmd_focus_days[i].date_obs,0,11), form=1)
      ;historical_focus_vals[n_restored_focus_elements + j-1].COMMANDED_FOCUS_END_DATE     = strmid(historical_focus_vals[n_restored_focus_elements+j].COMMANDED_FOCUS_START_DATE,0,15)
      ;historical_focus_vals[n_restored_focus_elements + j  ].COMMANDED_FOCUS_END_DATE     = temp_cmded_focus_vals_end_date                                                  ;only need to do this if added historical_focus_vals element.
      ;historical_focus_vals[n_restored_focus_elements + j  ].MEDIAN_MOSAIC_FOCUS_1330_STR = strtrim(string(indiv_cmd_focus_days[i+1].position),2)
      ;historical_focus_vals[n_restored_focus_elements + j  ].MEDIAN_MOSAIC_FOCUS_1400_STR = strtrim(string(indiv_cmd_focus_days[i+1].position),2)
      j = j+1
    endif 
  endfor
  

endif  
;stop, 'stopped -- this should be the complete set of focus start/stop days, compare with Teds -- check and see if the last one is correct, with respect to end date '

;note, the reason we start a "1" is because we already did the last one.  We want to do the next one.
if (n_elements(timeline) lt 2) then begin ;what is this for?
  ;perhaps we should write to a log file at this point?  Or, perhaps it doesn't matter because this is a cron job.
  print, 'No focus calibration OBS in query time-frame found by iris_time2timeline() call. Returning.'
  return
endif

tl_element_number = 1 ;why is the set to 1?  --...maybe "0" is the last obs we used, and "1" is the "next" obs.

date_obs_starts= timeline[tl_element_number].date_obs
date_obs_ends  = timeline[tl_element_number].date_end
level1 = iris_time2files(timeline[0].date_obs, timeline[0].date_end,obsid=timeline[0].obsid,LEVEL=1,/JSOC,/SJI)
;the top one gives more, but is it correct?
;level1 = iris_time2files(date_obs_starts, date_obs_ends,obsid=timeline[tl_element_number].obsid,LEVEL=1,/JSOC,/SJI);/nrt is a test
if level1[0] eq '' then begin
  print, 'No level1 data found by original iris_time2files call.  Trying again using /nrt flag.'
  level1 = iris_time2files(date_obs_starts, date_obs_ends,obsid=timeline[tl_element_number].obsid,LEVEL=1,/JSOC,/SJI,/nrt);I believe /nrt means look for near real time data.
endif

if level1[0] eq '' then begin
  print, 'No level 1 OBS files returned from iris_time2files(date_obs_starts, date_obs_ends,obsid=timeline[tl_element_number].obsid,LEVEL=1,/JSOC,/SJI, /nrt) query. Returning.'
  return
endif

print, 'Focus OBS start time: ', date_obs_starts, 'OBS end time: ', date_obs_ends
;are we handling hour boundaries?

;-START 3------------- set base_output_dir ---------------START 3-;
;This keyword is meant to override the default output directory.
;set to default value (if needed) and perform some filtering on focus_calibdb_dir, if set.
if not keyword_set(base_output_dir) then begin

  ;the below default directory is TEMPORARY during development
  ;base_output_dir ='/Users/shirts/calcode_test/' ; for now.  I wonder if the default directories shouldn't be written as a .genx file and placed in $SSWDB -- that way they could be modified over time without modifying the code.
  base_output_dir = '$SSW/site/iris/focus/'
endif else begin
  base_output_dir_TYPE = typename(base_output_dir)
  if 'STRING' ne  base_output_dir_TYPE then begin
    box_message, ['RETURNING from focus_calibrate_batch.pro:', 'keyword: base_output_dir must be of STRING type,', 'not type: '+ strtrim(base_output_dir_TYPE,2)]
  endif
  if not file_test(base_output_dir,/directory) then begin
    box_message, ['RETURNING from focus_calibrate_batch.pro:', 'keyword: base_output_dir: '+ base_output_dir + ' is NOT a valid directory path']
  endif
endelse

;-END 3------------- focus_calibdb_dir and focus_histdb_file ---------------END 3-;

if not keyword_set(out_ptg_dir) then begin
  out_parent_dir_ptg = base_output_dir + 'ptg/'
endif else begin
  out_parent_dir_ptg = out_ptg_dir
endelse
out_final_dir_ptg = ssw_time2paths(date_obs_starts,parent=out_parent_dir_ptg,/HOURLY)
mk_dir, out_final_dir_ptg

if not keyword_set(out_dir) then begin
  out_parent_dir = base_output_dir+'iris_trend_summary_imgs/'
endif else begin
  out_parent_dir = out_dir
endelse
out_final_dir = ssw_time2paths(date_obs_starts, parent=out_parent_dir,/HOURLY)
mk_dir, out_final_dir

if not keyword_set(out_dir_fits) then begin
  out_parent_dir_fits = base_output_dir + 'iris_trend_fits/level1_unwarped/'
endif else begin
  out_parent_dir_fits = out_dir_fits
endelse

out_final_dir_fits = ssw_time2paths(date_obs_starts,parent=out_parent_dir_fits,/HOURLY)
mk_dir, out_final_dir_fits
print, 'The output directory for sji fits files is: ', out_final_dir_fits
iris_prep_for_sji_focus_calib, level1, ffout, outdir=out_final_dir_fits
;;//since we just go back to the "last" processed one, we don't have to know how many days back it was

num_focus_obs_run_in_timerange = n_elements(level1)

;temp: need to find these dynamically start
wv1330_files_index = where(strmid(ffout,strlen(ffout(0))-9,4) eq '1330') ;this is new and gives the index set of files for 1330
wv1330_start = wv1330_files_index(0)
wv1330_end = wv1330_files_index(8)
;Also, is there some way to find and throw out a bad one, and only use a good sequence? ...some characteristic of the image?  out-of-family std dev?
;temp: should really find these dynamically end
if ((keyword_set(wv1330)) OR (all_wavelengths)) and (n_elements(ffout) ge (wv1330_end + 1)) then begin
  ;iris_focus_mosaic, ffout[wv1330_start:wv1330_end], 3, outdir=out_final_dir_ptg, fmean=fmean_1330, fmedia=fmedian_1330, /zbuffer
  iris_focus_mosaic, ffout[wv1330_files_index(0:8)], 3, outdir=out_final_dir_ptg, fmean=fmean_1330, fmedia=fmedian_1330, /zbuffer
  print, 'For wavelength 1330, fmean=',fmean_1330, ', fmedian=', fmedian_1330
  print, 'first fits file used for wv 1330: ',ffout[wv1330_start] 
  print, 'last fits file used for wv 1330:  ',ffout[wv1330_end] 
endif else begin
  if ((keyword_set(wv1300)) OR (all_wavelengths)) and (n_elements(ffout lt (wv1300_end + 1))) then begin
    print, 'Returning: cannot find files for wv1330 analysis.'
    return;
  endif
endelse


;make sure these variable truly reflect the wavelength being selected
;temp: need to find these dynamically start
wv1400_files_index = where(strmid(ffout,strlen(ffout(0))-9,4) eq '1400') ;this is new and gives the index set of files for 1400
wv1400_start = wv1400_files_index(0)
wv1400_end   = wv1400_files_index(8)
if ((keyword_set(wv1400)) OR (all_wavelengths)) and (n_elements(ffout) gt (wv1400_end + 1)) then begin
  ;iris_focus_mosaic, ffout[wv1400_start:wv1400_end], 3, outdir=out_final_dir_ptg, fmean=fmean_1400, fmedian=fmedian_1400, /zbuffer
  iris_focus_mosaic, ffout[wv1400_files_index(0:8)], 3, outdir=out_final_dir_ptg, fmean=fmean_1400, fmedian=fmedian_1400, /zbuffer
  print, 'For wavelength 1400, fmean=',fmean_1400, ', fmedian=', fmedian_1400
  print, 'first fits file used for wv 1400: ', ffout[wv1400_files_index(0)]
  print, 'last fits file used for wv 1400:  ', ffout[wv1400_files_index(8)]
endif else begin
  if ((keyword_set(wv1400)) OR (all_wavelengths)) and (n_elements(ffout lt (wv1400_end + 1))) then begin
    print, 'Returning: cannot find files for wv1400 analysis.'
    return;
  endif
endelse

;temp: should really find these dynamicall start
wv2796_files_index = where(strmid(ffout,strlen(ffout(0))-9,4) eq '2796') ;this is new and gives the index set of files for 2796
wvnuv_start = wv2796_files_index(0)
wvnuv_end   = wv2796_files_index(8)
;temp: need to find these dynamically end
if ((keyword_set(nuv)) OR (all_wavelengths)) and (n_elements(ffout) ge (wvnuv_end + 1))   then begin
  ;iris_focus_mosaic, ffout[wvnuv_start:wvnuv_end], 3, outdir=out_final_dir_ptg, fmean=fmean_nuv, fmedian=fmedian_nuv, /zbuffer
  iris_focus_mosaic, ffout[wv2796_files_index(0:8)], 3, outdir=out_final_dir_ptg, fmean=fmean_nuv, fmedian=fmedian_nuv, /zbuffer
  print, 'For wavelength nuv=2796, fmean=',fmean_nuv, ', fmedian=', fmedian_nuv
  print, 'first fits file used: for nuv/2796: ', ffout[wv2796_files_index(0)]
  print, 'last fits file used: for nuv/2796:  ', ffout[wv2796_files_index(8)]
endif else begin
  if ((keyword_set(nuv)) OR (all_wavelengths)) and (n_elements(ffout) lt (wvnuv_end + 1)) then begin
    print, 'Returning: cannot find files for wl_nuv analaysis.'
    return;
  endif
endelse


wv2832_files_index = where(strmid(ffout,strlen(ffout(0))-9,4) eq '2832') ;this is new and gives the index set of files for 2832
wvfuv_start = wv2832_files_index(0)
wvfuv_end   = wv2832_files_index(8)
;temp: need to find these dynamically end
if ((keyword_set(fuv)) OR (all_wavelengths)) and (n_elements(ffout) ge (wvfuv_end + 1)) then begin
  ;iris_focus_mosaic, ffout[wvfuv_start:wvfuv_end], 3, outdir=out_final_dir_ptg, fmean=fmean_fuv, fmedian=fmedian_fuv, /zbuffer
  iris_focus_mosaic, ffout[wv2832_files_index(0:8)], 3, outdir=out_final_dir_ptg, fmean=fmean_fuv, fmedian=fmedian_fuv, /zbuffer
  print, 'For wavelength fuv=2832, fmean=',fmean_fuv, ', fmedian=', fmedian_fuv
  print, 'first fits file used for fuv/2832:', ffout[wv2832_files_index(0)]
  print, 'last fits file used for fuv/2832: ', ffout[wv2832_files_index(8)]
endif else begin
  if ((keyword_set(fuv)) OR (all_wavelengths)) and (n_elements(ffout) lt (wfuv_end + 1)) then begin
    print, 'Returning: cannot find files for wl_fuv analysis.
   ;I need to skip this set, otherwise we will never progress since each time we will stop here.  Can I look elsewhere for ffout?  Perhaps run the number of elements test up above?
   return;
  endif
endelse

;only run the focus trend code if both 1330 and 1400 data is present -- since the fuv and nuv track them they do not have to be checked
;also, the only focus points we are saving are the 1330 and 1400

;working here -- last two elements are wrong/old



if ((keyword_set(wv1330) AND (keyword_set(wv1400))) OR (all_wavelengths eq 1)) then begin
  if (keyword_set(update_db) AND (tl_element_number eq 1)) then begin             ;why does tl_element_number have to be 1?

    ;HOW DO WE DETERMINE when to add a new element to historical_calib_data -- it shouldn't be only if the keywords are set -- it should be if a new focus obs needs to be analyzed.

    ;this still only inserts at the end -- doesn't take into account an insertion into the middle -- but it should filter out a duplicate
    local_filt_time_test = anytim2cal(strmid(date_obs_starts,0,11),form=1)

;stop, 'stopped: look at dates'
    ;this logic is a little bit interesting.  It says if the very last historical_calib_data is less than the current obs then append the current obs.
    ;but could there be multiple obs between those two?  What is the local_file_time_test/date_obs_starts?
    if anytim2tai(historical_calib_data[n_elements(historical_calib_data)-1].focus_obs_start_date) lt anytim2tai(local_filt_time_test) then begin ; 
        ;I need to compare two times here. "lt" doesn't work.
        historical_calib_data = [[historical_calib_data], h_c_d_template]
        historical_calib_data[n_elements(historical_calib_data)-1].is_data_processed_flag       = 0 
        historical_calib_data[n_elements(historical_calib_data)-1].focus_obs_ID                 = 0
        historical_calib_data[n_elements(historical_calib_data)-1].focus_obs_start_date         = anytim2cal(strmid(date_obs_starts,0,11),form=1)  ;check that this is correct
        historical_calib_data[n_elements(historical_calib_data)-1].num_hour_folders             = 0
        historical_calib_data[n_elements(historical_calib_data)-1].median_mosaic_focus_1330_str = fmedian_1330     ;check that this is correct
        historical_calib_data[n_elements(historical_calib_data)-1].median_mosaic_focus_1400_str = fmedian_1400     ;check that this is correct
        historical_calib_data[n_elements(historical_calib_data)-1].num_images                   = 0
        historical_calib_data[n_elements(historical_calib_data)-1].img_dest_directory           = ''
        ;
        ;SAVE updated structures: historical_calib_data and historical_focus_vals
        ;historical_gen_file_savename = 'focus_calib_datastruct_'+strjoin(strsplit(systim(), /extract),'_')
        ;not needed if not in development savegenx, h_c_d_template, historical_calib_data, file = historical_gen_file_savename , /OVERWRITE ;historical (since we are using systim it will never have the same file name.x -- should be written to a generic name, or find latest.)
        savegenx, h_c_d_template, historical_calib_data, file='$SSW_IRIS_DATA/focus_calib_datastruct', /OVERWRITE ;working copy (since we are using systim, it will never have the same file name.  -- should be written to a generic name, or find latest.)

        ;no need to save if nothing added.
        endif

        ;new section start
        ;update historical_focus_vals 
        if (n_elements(temp_test_for_comparison_historical_focus_vals) ne n_elements(historical_focus_vals)) then begin
             historical_focus_vals_filename           = '$SSW_IRIS_DATA/historical_focus_vals'; we may want to pre-pend the absolute path to this 
             savegenx, iris_focus_commanded_info, historical_focus_vals, file =  historical_focus_vals_filename, /overwrite  
             ;historical_focus_vals_filename           =  historical_focus_vals_filename + '_' + strjoin(strsplit(systim(),/extract),'_')                     
             ;not needed since we are out of development.  savegenx, iris_focus_commanded_info, historical_focus_vals, file =  historical_focus_vals_filename, /overwrite
        endif
    
        ;new section end
    endif

    print, 'iris_focus_trend outdir = ', out_final_dir
    ;stop, 'try and see what is wrong with ploting the focus set pointsi -- check the end date..'
    iris_focus_trend, historical_calib_data, historical_focus_vals, outdir=out_final_dir,/quiet
    iris_focus_trend, historical_calib_data, historical_focus_vals, outdir=base_output_dir,/quiet

  ;
endif

;NOTE: need looping exit criteria.
num_complete_iterations = num_complete_iterations+1
if ( (keyword_set(iterate_to_completion)) and (num_focus_obs_run gt 1)) then begin  ;currently, this is NOT RIGHT -- num_focus_obs_run just shows how many times the obs was run, not how many times the default focus was changed.
  print, 'iteratively calling focus_calibrate_batch because num_focus_obs_run gt 1 = ', num_focus_obs_run
  focus_calibrate_batch,                             $ ;is this how it is done -- to pass state through 
    focus_calibdb_dir=focus_calibdb_dir,             $
    focus_calibdb_file=focus_calibdb_file,           $
    focus_cmddb_dir=focus_cmcdb_dir,                 $
    focus_cmddb_file=focus_cmcdb_file,               $
    base_output_dir=base_output_dir,                 $
    rebuild_histfocus_db=rebuild_histfocus_db,       $
    update_db=update_db,                             $
    perform_all=perform_all,                         $
    nuv=nuv,                                         $
    fuv=fuv,                                         $
    wv1330=wv1330,                                   $
    wv1400=wv1400,                                   $
    outdir=outdir,                                   $
    out_ptg_dir=out_ptg_dir,                         $
    out_dir_fits=out_dir_fits,                       $
    num_complete_iterations=num_complete_iterations, $
    iterate_to_completion=iterate_to_completion
endif

print, 'Completed focus_calibrate_batch.'
end
