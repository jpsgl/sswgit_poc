pro IRIS_PREP_FILTER_OBS, obsdir, $
	save = save, runtime = runtime, pstop = pstop, png = png, loud = loud, $
	flat = flat, result = result, pipeline = pipeline, chisqlim = chisqlim

;+
;
; For a given OBS, read the results of the per-image fits of wavelength, 
; fiducial position, and AIA cross-correlation and filter them by fitting
; a sine wave with orbital period.
;
; INPUT:
;	obsdir	-	Path to the directory on /irisa containing the fits for a single
;				OBS. Currently needs to be a scalar.
;
; KEYWORDS:
;	/save	-	Set to save a GENX file in obsdir with the fit results
;	/runtime-	Set to print the time it takes to run
;	/png	-	Set to save PNGs of the fits in the subdirs of obsdir
;	/pstop	-	Set to break at the end prior to returning
;	/pipeline	-	(SWITCH) set if files are organized into subdirectories by
; 					OBS already
;
; HISTORY
; Written: PB?
;                       JPW 2017-03-24 bugfix: changed aiacorr file search from *.txt to *.dat
;-

tt0 = SYSTIME(/sec)

if N_ELEMENTS(obsdir) eq 0 then obsdir = '/irisa/data/prep/2014/07/06/20140706_075943'
if N_ELEMENTS(png) eq 0 then png = 0
if N_ELEMENTS(loud) eq 0 then loud = 0
if N_ELEMENTS(flat) eq 0 then flat = 0
result = ''	;	Initialize the result; it should be a structure on success

metafile = CONCAT_DIR(obsdir, 'description.genx')
if not FILE_EXIST(metafile) then begin
	BOX_MESSAGE, ['IRIS_PREP_FILTER_OBS: No description.genx file found for', obsdir]
	RETURN
endif
RESTGEN, file = metafile, meta

tai0 = ANYTIM2TAI(meta.date_obs)
tstr0 = TIME2FILE(meta.date_obs, /sec)
tai1 = ANYTIM2TAI(meta.date_end)
tstr1 = TIME2FILE(meta.date_end, /sec)

; Fit the fiducials
if KEYWORD_SET(pipeline) then begin
	fidfile = FILE_SEARCH(CONCAT_DIR(obsdir, 'fid/*.txt'))
	fidfits = IRIS_PREP_TXT2STR(fidfile, /fid, /dedup)
endif else begin
	fidfits = IRIS_PREP_FITLOG_READER(meta.date_obs, meta.date_end, /fid, flat = flat)
endelse
if N_TAGS(fidfits) gt 0 then begin
	if N_ELEMENTS(chisqlim) eq 0 then chisqlim = 800
	fchisqlim = chisqlim * meta.sumspat * meta.sumsptrl
	fidstr = IRIS_PREP_FILTER_FID(fidfits, png = png, loud = loud, $
		outpath = CONCAT_DIR(obsdir, 'fid'), chisqlim = fchisqlim)
endif else begin
	; Can't really do anything if you don't at least have a fiducial fit structure
	BOX_MESSAGE, ['IRIS_PREP_FILTER_OBS: No fiducials found for', obsdir]
	RETURN
endelse

; Fit the wavelengths (occasionally missing if no spectra were taken)
if KEYWORD_SET(pipeline) then begin
	wavefile = FILE_SEARCH(CONCAT_DIR(obsdir, 'wavecorr/*.txt'))
	wavefits = IRIS_PREP_TXT2STR(wavefile, /wave, /dedup)
endif else begin
	wavefits = IRIS_PREP_FITLOG_READER(meta.date_obs, meta.date_end, /wave, flat = flat)
endelse
if N_TAGS(wavefits) gt 0 then begin
	if N_ELEMENTS(chisqlim) eq 0 then chisqlim = 800
	wchisqlim = chisqlim * meta.sumspat * meta.sumsptrl
	wavestr = IRIS_PREP_FILTER_WAVE(wavefits, png = png, loud = loud, $
		outpath = CONCAT_DIR(obsdir, 'wavecorr'), chisqlim = wchisqlim)
endif else begin
	wavestr = CREATE_STRUCT('tstart', ' ', 'chans', STRARR(3), $
		'rangenames', STRARR(3), 'paramnames', STRARR(4), $
		'numfit', LONARR(3,3), 'chisqs', DBLARR(3,3), 'paramvals', DBLARR(3,3,4), $
		'combo_chans', STRARR(2), 'combo_chisqs', DBLARR(2), $
		'combo_numfit', LONARR(2), 'combo_paramvals', DBLARR(2,9), $
		'combo_tag', STRARR(2,6), 'combo_flag', BYTARR(2,6))
endelse

; Fit the AIA cross-correlation (often missing if an appropriate channel is not
; found)
hasaia = WHERE(fidfits.type eq 'SJI_1400' or fidfits.type eq 'SJI_2832', numaia)
if numaia gt 0 then begin
	if KEYWORD_SET(pipeline) then begin
		aiafile = FILE_SEARCH(CONCAT_DIR(obsdir, 'aiacorr/*.dat'))
		aiafits = IRIS_PREP_TXT2STR(aiafile, /aia, /dedup)
	endif else begin
		aiafits = IRIS_PREP_FITLOG_READER(meta.date_obs, meta.date_end, /aia, flat = flat)
	endelse
endif else begin
	aiafits = ''
endelse
if N_TAGS(aiafits) gt 0 then begin
	aiastr = IRIS_PREP_FILTER_AIA(aiafits, png = png, loud = loud, $
		outpath = CONCAT_DIR(obsdir, 'aiacorr'))
endif else begin
	aiastr = CREATE_STRUCT('tstart', STRARR(1), 'chans', STRARR(2), $
		'axisnames', STRARR(2), 'paramnames', STRARR(4), $
		'numfit', LONARR(2,2), 'chisqs', DBLARR(2,2), 'paramvals', DBLARR(2,2,4), $
		'combo_chisqs', DBLARR(2), 'combo_numfit', LONARR(2), $
		'combo_paramvals', DBLARR(2,5))
endelse

result = {obs : meta, fid : fidstr, wave : wavestr, aia : aiastr}

if KEYWORD_SET(save) then begin
	outfile = CONCAT_DIR(obsdir, 'obsfits.genx')
	SAVEGEN, str = result, file = outfile
endif

if KEYWORD_SET(runtime) then begin
	BOX_MESSAGE, [obsdir, 'IRIS_PREP_FILTER_OBS run time : ', $
		STRING(SYSTIME(/sec) - tt0, form = '(f10.1)')]
endif

if KEYWORD_SET(pstop) then STOP

end

