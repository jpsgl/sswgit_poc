;translate obsid into structure with expected parameters for obs
;LK v1, apr 24, 2013
;- apr 25: corrected total raster size and modified code to take
;  sparse/dense/sparse rasters into account
;  assumes that there are maximally 3 different subrasters
;  (i.e. sparse/dense/sparse/dense would currently fail)
;May 17, commented out line dum[where(...
;        changed long64(1d9) to long64(2d8)
;Aug 27,2013: changed tlmtab2struct(10) and (2000), now version-dependent
;   added raster description

;tab10 and tab2000 are the csv files (including path) containing the
;obsid definitions created from Bart's numbers files. -> no longer necessary

;Example:
;IDL>  help,iris_obsid2struct(4040512153)
;** Structure <2833608>, 20 tags, length=176, data length=168, refs=1:
;   FILE10          STRING    'not necessary'
;   FILE2000        STRING    'not necessary'
;   OBSVERSION      FLOAT           20.0000
;   RASTER_STEP     FLOAT     Array[3]
;   RASTER_FOVX     FLOAT     Array[3]
;   RASTER_FOVX_TOT FLOAT           127.000
;   RASTER_FOVY     FLOAT           175.000
;   SJI_FOVX        FLOAT           175.000
;   SJI_FOVY        FLOAT           175.000
;   SJI_DESCR       STRING    'C II   Si IV   Mg II h/k   Mg II w s'
;   TEXP_FAC        FLOAT           30.0000
;   SUMSPAT         FLOAT           1.00000
;   SUMSPEC         FLOAT           1.00000
;   FUVSUMSPEC      FLOAT           4.00000
;   SJI_CAD_FAC     FLOAT           1.00000
;   AEC             FLOAT           0.00000
;   FFCAL           FLOAT           0.00000
;   COMPRESSION     STRING    'Default'
;   LINELIST        STRING    'Small linelist'
;   DESCRIPTION     STRING    'Very large sparse raster (tight timing)'




FUNCTION iris_obsid2struct, obsid, tab10=tab10, tab2000=tab2000

;;if ~keyword_set(tab10) then tab10='~/idl/iris/table10_11jun13_tight.csv'
;if ~keyword_set(tab10) then tab10='/archive/ssw/site/idl/iris/csv/v40-table10.csv'
;;if ~keyword_set(tab2000) then tab2000='~/idl/iris/table2000_2apr13.csv'
;;if ~keyword_set(tab2000) then tab2000='~/idl/iris/table2000_11jun13.csv'
;if ~keyword_set(tab2000) then tab2000='/archive/ssw/site/idl/iris/csv/v40-table2000.csv'
tab10 = 'not necessary'  ;keeping variable for compatibility reasons
tab2000 = 'not necessary' ;keep for compatibility


;-------------------------------------------------------
;determine the parameters that the obsid corresponds to


;scheme defines 'our' numbering system, basically the smallest units
;of each category
scheme = [long64(2d8),long64(2d7),long64(1d7),long64(1d6),long64(250000),long64(20000),long64(2000),long64(100),long64(1)]
nscheme=n_elements(scheme)

dum=lon64arr(nscheme,1,2)
idr=obsid
for j=0,nscheme-1 do begin
  if scheme[j] ne 0 then begin
     dum[j,*,0]=long64(idr/scheme[j])*scheme[j]
     dum[j,*,1]=long64(idr-dum[j,*,0])
     idr=dum[j,*,1]  ;left-over part of obsid
    ; dum[where(reform(dum[j,*,0]) eq 0),0,1]=0
  endif else begin
     dum[j,*,0]=idr
     dum[j,*,1]=0
  endelse 
endfor
out=dum[*,*,0]
;out contains the id for each category in the numbers file

;look for correct version of tables
vers = out[0] / scheme[0] *2.   ;*2. because 'versioning' is 40,38,...20. Below 20: linelists

;-- read in predefined tables
struc10=iris_tlmtab2struct('v'+string(vers,format='(I2)')+'-table10')
struc2000=iris_tlmtab2struct('v'+string(vers,format='(I2)')+'-table2000')


if (size(struc10))[0] eq 0 then begin
print,'The version in your input obsid does not correspond to an available table.'
print,'Using v40-table10 and v40-table2000 as default (errors may be a result of this).'
struc10=iris_tlmtab2struct(10)
struc2000=iris_tlmtab2struct(2000)
endif


;--- define output structure
output = { $
  file10: '', $             ;path to file
  file2000: '', $           ;path to file
  obsversion: 0.,$          ;obs numbering
  raster_step: fltarr(3),$  ;arcsec
  raster_fovx: fltarr(3),$  ;arcsec (each sub-raster)
  raster_fovx_tot: 0.,$     ;arcsec (total raster size, taking into account raster_step)
  raster_fovy: 0.,$         ;arcsec
  sji_fovx: 0.,$            ;arcsec
  sji_fovy: 0.,$            ;arcsec
  sji_descr: '',$           ;SJI filter description
  texp_fac: 0.,$            ;factor for exposure time ("Deep")
  sumspat: 0.,$             ;spatial summing
  sumspec: 0.,$             ;spectral summing for SJI and NUV
  fuvsumspec: 0.,$          ;spectral summing for FUV
  sji_cad_fac: 0.,$         ;SJI cadence factor, i.e. 0.25xfaster
  AEC: 0.,$                 ;AEC on? 0=false, 1=true
  ffcal: 0.,$               ;FF/CAL series? 0=false, 1=true
  compression: '',$         ;description of compression {default, lossless}
  linelist: '',$             ;Linelist or full readout description
  description:''}           ;raster description


;--- fill output structure

output.file10 = tab10
output.file2000 = tab2000
output.obsversion = out[0]/scheme[0]

;last 2 digits define raster type
tmp = where(out[8] eq struc10.obs_id)
if tmp[0] eq -1 then message,'Something is wrong with the ID for the raster type'
tmp2 = (struc10.raster_step[tmp])[0]            ;this is a string with a max of 3 entries, plus character "
output.raster_step = strsplit(tmp2,' "',/extr)  ;for simple rasters, this will be [0.33,0,0]
tmp2 = reform(struc10.raster_fovx[tmp,*]) ;array[3], also for simple rasters, given by Sam's routine
output.raster_fovx = tmp2
output.raster_fovx_tot = tmp2[0] + output.raster_step[1] + tmp2[1] + output.raster_step[2] + tmp2[2]
output.raster_fovy = (struc10.raster_fovy[tmp,*])[0]  ;assuming no scanning in y-direction
output.sji_fovx = (struc10.sji_fovx[tmp])[0]
output.sji_fovy = (struc10.sji_fovy[tmp])[0]
output.description = (struc10.description[tmp])[0]

;100 - 1900 for sji type
if out[7] ne 0 then begin
   tmp = where(out[7] eq struc2000.obs_id)
   if tmp[0] eq -1 then message,'Something is wrong with the ID for the SJI type'
   output.sji_descr = (struc2000.size_description[tmp])[0]
endif else begin
   output.sji_descr = 'C II   Si IV   Mg II h/k   Mg II w'
endelse


;2000-12000 for exp time factor
if out[6] ne 0 then begin
   tmp = where(out[6] eq struc2000.obs_id)
   if tmp[0] eq -1 then message,'Something is wrong with the ID for the exposure time factor'
    tmp2 =  (struc2000.size_description[tmp])[0]
    output.texp_fac =  float(strsplit(tmp2,'Deep x',/extr))
endif else begin
    output.texp_fac = 1.
endelse


;20000-220'000 for binning
if out[5] ne 0 then begin
   tmp = where(out[5] eq struc2000.obs_id)
   if tmp[0] eq -1 then message,'Something is wrong with the ID for the summing'
    tmp2 =  (struc2000.size_description[tmp])[0]
    output.sumspat =  float((strsplit(tmp2,'x,',/extr))[1])
    output.sumspec =  float((strsplit(tmp2,'x,',/extr))[3])
endif else begin
    output.sumspat = 1.
    output.sumspec = 1.
endelse

;250'000-750'000 for FUV spectral binning
if out[4] ne 0 then begin
   tmp = where(out[4] eq struc2000.obs_id)
   if tmp[0] eq -1 then message,'Something is wrong with the ID for the FUV summing'
    tmp2 =  (struc2000.size_description[tmp])[0]
    output.fuvsumspec =  float((strsplit(tmp2,'x,',/extr))[1])
endif else begin
    output.fuvsumspec = output.sumspec
endelse


;1'000'000 - 4'000'000 SJI cadence, 8'000'000 aec, 9'000'000 ff and cal
if out[3] ne 0 then begin
   tmp = where(out[3] eq struc2000.obs_id)
   if tmp[0] eq -1 then message,'Something is wrong with the ID for the SJI cadence, AEC, or FF/CAL'
   tmp2 =  (struc2000.size_description[tmp])[0]

   test = strmatch(tmp2,'*SJI*') 
   if test eq 1 then begin
    output.sji_cad_fac = float((strsplit(tmp2,'cadence x,',/extr))[1])
    output.aec = 0.
    output.ffcal = 0.
   endif else begin
      test2 = strmatch(tmp2,'AEC')
      if test2 eq 1 then begin
       output.sji_cad_fac = 1.
       output.aec = 1.
       output.ffcal = 0.
      endif else begin ;it might be smarter to actually test for ff and cal. here I assume that it's the only thing left
       output.sji_cad_fac = 1.
       output.aec = 0.
       output.ffcal = 1.
      endelse
   endelse


endif else begin
    output.sji_cad_fac = 1.
    output.aec = 0.
    output.ffcal = 0.
endelse


;compression
if out[2] ne 0 then begin
   tmp = where(out[2] eq struc2000.obs_id)
   if tmp[0] eq -1 then message,'Something is wrong with the ID for the compression'
   output.compression = (struc2000.size_description[tmp])[0]  
endif else begin
    output.compression = 'Default'
endelse


;Linelists
if out[1] ne 0 then begin
   tmp = where(out[1] eq struc2000.obs_id)
   if tmp[0] eq -1 then message,'Something is wrong with the ID for the linelists'
   output.linelist = (struc2000.size_description[tmp])[0]  
endif else begin
    output.linelist = 'Linelist 1'
endelse



return,output


END
