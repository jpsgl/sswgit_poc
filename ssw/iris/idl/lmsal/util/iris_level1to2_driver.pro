pro iris_level1to2_driver, t0, t1, outparent=outparent, prelaunch=prelaunch, ds=ds,_extra=_extra, percent_cutoff=percent_cutoff, $
   sequences=sequences, vc2=vc2, vc1=vc1 , lucia_analysis=lucia_analysis, check_sequences=check_sequences, $
   prepit=prepit, timeline=timeline
;
 
prepit = keyword_set(prepit)


if n_elements(t0) eq 0 then begin 
   t0='11-apr-2013 08:00'
   t1='11-apr-2013 09:00'
endif


box_message,'Warning - prepending Martins CVS - remove when $SSW_IRIS versions ~stable
ssw_path,'/sanhome/mawiesma/CVS/iris/idl/level1to2',/prepend 
ssw_path,'/sanhome/mawiesma/CVS/iris/idl/iris_simulator',/prepend
ssw_path,'/sanhome/mawiesma/CVS/iris/idl/util',/prepend

timeline=keyword_set(timeline) ; optionally, use as-planned, not as-run

if n_elements(percent_cutoff) eq 0 then percent_cutoff=25. ; don't bother if < this% L1 available

if not keyword_set(outparent) then outparent=get_logenv("IRIS_DATA")
prelaunch=keyword_set(prelaunch) or ssw_deltat(t0,ref='26-jun-2013',/days) lt 0
outparent=concat_dir(concat_dir(outparent,(['','prelaunch'])(prelaunch)),'level2')

if keyword_set(vc1) then iris_set_gse,/vc1 else iris_set_gse,/vc2

; 
case 1 of 
   data_chk(sequences,/struct): ; user supplied
   timeline: begin
      sequences=iris_time2timeline(t0,t1,_extra=_extra,/fsn)
      sequences=add_tag(sequences,sequences.date_obs,'seq_start')
      sequences=add_tag(sequences,sequences.date_end,'seq_end')
   endcase
   else:sequences=iris_time2seq(t0,t1,/fsn,_extra=_extra,/no_prime) ; time->seqeuence
endcase

if not data_chk(sequences,/struct) then begin 
   box_message,'No sequences found for time-range; bailing..
   return
endif

nseq=n_elements(sequences)

if keyword_set(check_sequences) then stop,'before sequences'
ssok=where(sequences.obsid ne 3800ul,okcnt)
case 1 of 
   okcnt eq n_elements(sequences): ;all ok, nop
   okcnt eq 0: begin
      box_message,'Only all slit-jaw - bailing
      return ; early / unstructured exit on all slit jaw only
   endcase 
   else: begin
      box_message,'removing ' + strtrim(nseq-okcnt,2) + ' slit-jaw only sequences'
      sequences=sequences[ssok]
   endcase
endcase

if keyword_set(check_sequences) then stop,'after sequences'


if n_elements(ds) eq 0  then ds=(['iris.lev1_prelim01','iris_ground.lev1_dc1'])(prelaunch) 

seqinfo=get_Infox(sequences,tag_names(sequences))

nseq=n_elements(seqinfo)

box_message,seqinfo
sobsid=string(sequences.obsid,format='(I10.10)')

for s=0,nseq-1 do begin 
   delvarx,drms,files
   if timeline then begin 
      box_message,'Using timeline, not tlm/lev0...'
      files=iris_time2files(sequences[s].date_obs,sequences[s].date_end,/level,/hour)
      if file_exist(files[0]) then read_iris,files,drms,/nodata else box_message,'no files found for this time range via iris_time2files'
   endif else begin 
   help,sequences[s].first_fsn,sequences[s].last_fsn
   ssw_jsoc_time2data,sequences[s].first_fsn,sequences[s].last_fsn,drms,files,/files_only,ds=ds,/jsoc2,/FSN,_extra=_extra
   if data_chk(drms,/struct) then begin 
   ssbad=where(drms.t_obs eq 'MISSING',bcnt)
   if bcnt gt 0 then begin 
      ssok=where(drms.t_obs ne 'MISSING',gcnt)
      drms=drms[ssok]
      files=files[ssok]
   endif
   endif
   endelse
   if timeline then pct=100. else pct=((n_elements(files)>1.)/sequences[s].nfsns)*100
   pct=pct*data_chk(drms,/struct)
   box_message,strtrim(pct,2),nbox=2
   if pct ge percent_cutoff then begin
      if keyword_set(lucia_analysis) then begin 
         xml_files=iris_obstab2depend('OBS-'+sobsid[s]+'.xml',/check,nmiss=nmiss) ; 15-jul-2013 - depend function now accepts obsid (UL) 
         if nmiss eq 0 then begin ; 
            auto_param_check_v5_sub, fits_files=files, xml_files=xml_files
         endif else box_message,'Number of missing .xml = '+ strtrim(nmiss,2)
         
      endif else begin 
         if prepit then begin 
            proot='prep_'+time2file(sequences[s].seq_start,/sec) + '_' + string(sequences[s].obsid,format='(I10.10)')
            pout=concat_dir(ssw_time2paths(sequences[s].seq_start,parent=concat_dir(outparent,'temp/iris_prepped')),proot)
            mk_dir,pout
            pfiles=file_search(pout,'*.fits')
            ssw_file_delete,pfiles ; start with clean slate
            nf=n_elements(files)
            box_message,'Prepping ' + strtrim(nf,2) + ' files...'
            onames=drms.instrume + '_' + time2file(drms.date_obs,/sec) + '_fsn' + string(drms.fsn,format='(i10.10)') + '.fits'
            ofiles=concat_dir(pout,onames)
            for f=0,nf-1 do begin 
 	       print,f
               read_iris,files[f],ii,dd,use_share=(!version.memory_bits eq 64),/noshell,/uncomp_delete,/silent
               iris_prep,ii,dd,pii,pdd, _extra=_extra ;
               mwritefits,pii,pdd,outfile=ofiles[f]
            endfor
         endif
         l2dir=ssw_time2paths(drms[0].t_obs,parent=outparent)+'/'
         obsdir=concat_dir(l2dir,time2file(sequences(s).seq_start,/sec) +'_'+ strtrim(sequences[s].obsid,2)) + (['','_prepped'])(prepit) + '/'
         box_message,'Output path>> ' + obsdir[0]
         mk_dir,obsdir
         iris_level1to2,files,obsdir, _extra=_extra, parent_out=concat_dir(outparent,'temp'), obsid=sobsid[s] ;string(sequences[s].obsid,format='(i10.10)')
    endelse

   endif else box_message,'Not enough L1 files to worry about'
endfor

return
end
   



