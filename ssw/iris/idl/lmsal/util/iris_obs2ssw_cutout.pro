pro iris_obs2ssw_cutout, obsid, t0,t1, query, query_info, _extra=_extra, waves=waves, $
   fov_expand=fov_expand, minute_window=minute_window, no_hcr=no_hcr, description=description, $
   cgiquery=cgiquery, queryout=queryout, iris_obs2fov=iris_obs2fov
;
;+
;   Name: iris_obs2ssw_cutout
;
;   Purpose: Request SSW cutouts (via ssw_cutout_service) implied by one IRIS OBSID via HCR info; default=AIA
;
;   Input Parameters:
;      obsid - a single, fully qualified IRIS OBSID of form: yyyymmdd_hhmmss_<obsid> (-> iris_obs2hcr)
;      t0,t1 - optional timerange, assumed subset of OBSID timerange (for example, flare/event within this OBS, from HER or SSW or ?
;
;   Output Parameters:
;      query - verbatim query isued->cutout Q (output from ssw_cutout_service.pro)
;      query_info - Q server output (output from ssw_cutout_service.pro) - includes "future url" of cutout service output
;
;   Keyword Parameters:
;      waves = list of WAVES passed -> cutout; default= '94,131,171,193,211,304,335,1600,1700,blos,cont' ; for now
;      minute_window - optional temporaral window expand relative to IRIS OBSID start/stop;
;                      (add +/- this number minutes to HCR start/stop) - scalar (-/+ N) or 2 elements [-BEFOREm,AFTERm]
;      fov_expand - optional spatial window expand in arcseconds; may be scalar (X&Y) or 2 elements [X,Y] 
;                      (expands IRIS FOVX & FOVY by this value in SDO cutouts) - default=[0,0]=verbatim IRIS FOV per HCR (caveat emptor)
;      _extra - inherit -> ssw_cutout_service.pro
;               _EXTRA options/keywords are described in : http://www.lmsal.com/solarsoft/ssw_service/ssw_service_track_fov_api.html
;      cgiquery - (output) query generated for This request (per ssw_cutout_service)
;      queryout - (output) output from query/GET (per ssw_cutout_service.pro)
;
;   Calling Sequence:
;      iris_obs2ssw_cutout,OBSID [,waves=waves] [,fov_expand=n"] [minute_window=nMinutes ] [keywords=keywords]
;
;   Calling Examples:
;      IDL> iris_obs2ssw,OBSID,fov_expand=[120,60],minute_window=[-10,5], waves='131,171,193,304,blos', max_frames=500,max_movie_frames=200, $
;              blend_waves='131,193,171',email='freeland@lmsal.com', /goes_movie, maxxy=1024
;      
;  Restrictions:
;     for now, HMI waves (cont/blos) will only show up for "fairly recent" obsids - pending ssw_cutout_service expansion
;     now that I look at this, this could be a 2 liner to (maybe tommorrow)) 'ssw_hcr2ssw_cutout.pro'
;
;   History: 
;      25-feb-2014 - S.L.Freeland - the IRIS L2/HCR->SSW(aia/hmi) cutout
;      18-jun-2014 - S.L.Freeland - documentation only; clarify FOV_EXPAND definition
;      16-apr-2015 - S.L.Freeland - added temporal sub-range via user supplied T0,T1 for events within OBSID
;                                   optional DESCRIPTION to append/expand upon HCR.
;      28-oct-2015 - S.L.Freeland - fix type (minutes_window -> minute_window)
;      13-nov-2015 - S.L.Freeland - add output keywords CGIQUERY and QUERYOUT (supplement positional)
;      17-jun-2016 - S.L.Freeland - add /iris_obs2fov switch
;-
;   
; 1st thing, get HCR record corresponding to input OBSID (includes error checking)

hcr=iris_obs2hcr(obsid,count=count) ; obsid->HCR 
if count ne 1 then begin 
   box_message,'Need one, and only one fully qualified IRIS obsids; no action taken.
   return ; EARLY EXIT on illegal input
endif

if n_elements(waves) eq 0 then waves='94,131,171,193,211,304,335,1600,1700' ; ,blos,cont' ;TODO - best default list?

; hcr->ssw_cutout_service
; -----------  block is ~instrument independent - may end up in "ssw_hcr2ssw_cutout.pro" ----------------
if n_elements(fov_expand) eq 0 then fov_expand=0                        ; TODO - best default FOV window relative to IRIS?
if n_elements(minute_window) eq 0 then minute_window=0                ; TODO - best default temporal window relative to IRIS?
if n_elements(t1) gt 0 then time_window,[t0,t1],ct0,ct1,minutes=minute_window else $
   time_window,[hcr.starttime,hcr.stoptime],ct0,ct1,minutes=minute_window ; this expands input time +/- per user window
if n_elements(description) eq 0 then desc='' else desc='_'+description
if get_logenv('hcr_check') ne '' then stop,'hcr,description'
if n_elements(t1) gt 0  then desc= desc + '_'+arr2str(time2file([t0,t1]),'_')

doobs=1
if keyword_set(iris_obs2fov) then begin 
   iris_obs2fov,obsid,xycen,fovxy,timerange
   if n_elements(xycen) gt 0 then begin 
   xcen=xycen[0]
   ycen=xycen[1]
   fovx=fovxy[0]
   fovy=fovxy[1]
   ct0=timerange[0]
   ct1=timerange[1]
   endif else begin 
      box_message,'Problem OBS per iris_obs2fov, skipping'
      doobs=0
   endelse
endif else begin
   xcen=hcr.xcen
   ycen=hcr.ycen
   fovx=hcr.xfov+fov_expand[0]
   fovy=hcr.yfov+last_nelem(fov_expand)
endelse

if doobs then begin 
help,xcen,ycen,fovx,fovy,waves
ssw_cutout_service,ct0,ct1,query, query_info, waves=waves,xcen=xcen,ycen=ycen,fovx=fovx,fovy=fovy,$
   /hcr,planners=planner,obstitle=hcr.obstitle+desc,description='IRIS '+obsid + ' '+hcr.obstitle+desc, $
   _extra=_extra, query_source='iris_obs2ssw_cutout',ref_time=anytim((anytim(ct0)+anytim(ct1))/2,/ecs)
; -----------------------------------------------------------------
if n_elements(query) gt 0 then cgiquery=query                ; positional params for backward compat
if n_elements(query_info) gt 0 then queryout=query_info
endif else box_message, 'OBS skipped'

return
end




  


