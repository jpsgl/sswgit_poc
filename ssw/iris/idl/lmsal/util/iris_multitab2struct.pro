function iris_multitab2struct, tabfiles, info=info, header=header, data=data
;
;+
;   Name: iris_multitab2struct
;
;   Purpose: read multiple IRIS table files files -> structure vector
;
;   Input Parameters:
;      tabfiles - IRIS table files (for now, ssw_iris_xml2struct compatible; CRS/FDB/FRM/OBS)
;
;   Output:
;      function returns structure vector {thing:<nestedarray>, filenames:<list>
;
;   Keyword Parameters:
;      info (switch) - if set, return .INFO for all TABFILES (default=.DATA)
;      header (switch) - if set, return .HEADER for all TABFILES (default=.DATA)
;
;   Calling Context:
;      IDL> alldata=iris_multitab2struct(tabfiles)
;      IDL> ss=struct_where(alldata.DATA,SEARCH=[search arry])
;      IDL> mfiles=alldata.FILENAMES[ss] ; tabfiles associated with matches.. etc
;
;   History:
;      6-mar-2013 - S.L.Freeland
;-

case 1 of
   keyword_set(info): thing='info'
   keyword_set(header): thing='header'
   else: thing='data' ; default
endcase

nf=n_elements(tabfiles)

flist=''
for i =0,nf-1 do begin 
   tabdat=gt_tagval(ssw_iris_xml2struct(tabfiles[i]),thing,missing=-1)
   if data_chk(tabdat,/struct) then begin 
      if n_elements(alldat) eq 0 then alldat=tabdat else $
         alldat=[temporary(alldat),tabdat]
      flist=[temporary(flist),replicate(tabfiles[i],n_elements(tabdat))]
   endif
endfor

retval=-1
if n_elements(flist) gt 1 then begin
   retval=add_tag(retval,alldat,thing)
   retval=add_tag(retval,flist[1:*],'filenames')
endif else box_message,'No valid stuff in all files?'

return,retval
end
   

