function iris_simlog2struct,simlogfile, add_header=add_header, header=header
;
;+
;   Name: iris_simlog2struct
;
;   Purpose: IRIS simulator log file -> structure vector
;
;   Input Parameters:
;      simlogfile - IRIS simulator log file
;
;   Output:
;      function returns structure vectro analog
;
;   Keyword Parameters:
;      add_header - optional return {header:header, data:data} - default is unnested DATA only
;      header - (output) - header structure associated with input simlog
;
;   History:
;      18-apr-2013 - S.L.Freeland
;      24-apr-2013 - S.L.Freeland - allow for some simlog evolution; initially sumx/sumy
;       8-may-2013 - S.L.Freeland - fovy added
;
;   Restrictions:
;      tested three times on 3 files... 
;-


if not file_exist(simlogfile) then begin 
   box_message,'Need valid IRIS simulator file input
   return,-1
endif


dat=rd_tfile(simlogfile)
dat=strtrim(strarrcompress(dat),2); get rid of null lines


; --- header stuff ---
headss=where(strpos(dat,'===') eq 0,hcnt)
header=dat[0:headss[0]-1]
version=ssw_strsplit(header[0],'sion: ',/tail)
obsid=ssw_strsplit(header[1],': ',/tail)
paths=header[2:headss[0]-1]
paths[0]=ssw_strsplit(paths[0],'; ',/tail)
paths=arr2str(paths)
header={version:version[0],obsid:obsid[0],paths:paths}
; --- end of header stuff ---


; --- DATA stuff ----
template={imnr:0l,time:0.,type:'',obsrep:0l,obsentry:0l,frmrep:0l,frmentry:0l,pztx:0.,pzty:0.,fw:0,focus:0,flush:0,frmid:0ul,fdbid:0ul,crsid:0l,lut:0l,compn:0,compk:0,exp:0l,file1f:0l,file2f:0l,file1n:0l,file2n:0l}
data=dat[headss[1]+1:*]
cols=str2cols(data,'|',/trim,/unalign)


nd=n_elements(data)
nc=n_tags(template)
nx=data_chk(cols,/nx)
  
if nx ne nc then begin ; evolution of simlog/ncolums
   template=add_tag(template,0,'sumx',index='exp')
   template=add_tag(template,0,'sumy',index='sumx')
   nc=n_tags(template)
   if nx ne nc then begin 
      template=add_tag(template,0.0,'fovy',index='sumy')
      nc=n_tags(template)
   endif   
endif

if nx ne nc then begin 
   box_message,'Unexpected #columns in simlog table, bailing...
   return,-1
endif

retval=replicate(template,nd)

for c=0,nc-1 do begin 
   temp=retval.(c)
   reads,reform(cols[c,*]),temp
   retval.(c)=temp
endfor
; --- end of DATA stuff ---

if keyword_set(add_header) then retval={header:header[0], data:retval}

return,retval
end





