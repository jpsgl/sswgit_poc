function iris_obs2hcr, p0, p1 , append=append, obsids=obsids, count=count,_extra=_extra, $
   match_patterns=match_patterns, taglist=taglist, refresh=refresh, loud=loud, debug=debug
;+
;   Name: iris_obs2hcr
;
;   Purpose: IRIS obs info -> HCR glue ( -> ssw_hcr_query/ssw_hcr_make_query)
;
;   Input Parameters:
;      p0 - vector of IRIS OBSIDs -or- "timeline-like" structures -or- start time of range
;      p1 - optional stop time of rante (assumes p0=scalar start time)
;
;   Output:
;      funtion returns HCR structures matching user input 
;
;   Keyword Parameters:
;      append (switch) - if set, and p0 is structure vector, append HCR records
;      obsids - optional list of one or more obsids; in conjunction with time range input
;      match_patterns - optional list of one or more string patterns to match 
;      taglist - optional TAG list for 'match_patterns' search (->ssw_hcr_where.pro) - default={obstitle,goal,sciobjectives}
;
;   Calling Examples:
;      1. Input one or more fully qualified obsids; vector or comma separated list - output 1:1
;      IDL> hcr=iris_obs2hcr(['20140115_085448_3820007404','20140119_165217_3820113644']) 
;      2. Input "timeline-like" structure (any structure vector w/at least .DATE_OBS and .OBSID) - output 1:1
;      IDL> tl=iris_time2timeline('15-jan-2014') 
;      IDL> hcr=iris_obs2hcr(tl) & help,tl,hcr 
;      TL    STRUCT    = -> <Anonymous> Array[14]
;      HCR   STRUCT    = -> <Anonymous> Array[14]
;      IDL> more,get_infox(tl,'date_obs') + ' ' + get_infox(hcr,'starttime,xcen,ycen,observers,target,noaanum,obsid')
;     2014-01-15T00:54:24.000 2014-01-15 00:54:24    949.00   -153.00  mscott      AR  11944  3820025604
;     2014-01-15T02:35:22.000 2014-01-15 02:35:22    923.00   -181.00  mscott      AR  11944  3882010144
;     2014-01-15T05:45:22.000 2014-01-15 05:45:22     -1.00     -5.00  mscott      QS         3882010144
;     (..etc..)
;
;      3. time range, optionally with OBSID list - output generally not 1:1
;      IDL> hcr=iris_obs2hcr('15-jan-2014','16-jan-2014',obsid='3882010144')
;
;      4. optionally, search for one or more string patterns (via ssw_hcr_where.pro) 
;      IDL> hcr=iris_obs2hcr('1-aug-2014','20-aug-2014',patterns=['dense large']
;
;   History:
;      25-feb-2014 - S.L.Freeland - useful glue for apps (likw obs->hcr->ssw/aia cutout client->cutout service)
;       8-apr-2014 - S.L.Freeland add .OBSPATH and .URL 
;      21-aug-2014 - S.L.Freeland - add MATCH_PATTERNS & TAGLIST (-> ssw_hcr_where.pro filter)
;      25-aug-2014 - S.L.Freeland - add cache for repeated calls with same time range, different MATCH_ (for example)
;      27-aug-2014 - S.L.Freeland - repair ripple for OBSID input introduced by cache/25-aug mod
;-

common iris_obs2hcr_blk,t0x,t1x,hcrx

count=0 ; hopefully, this gets larger with time...

loud=keyword_set(loud)
debug=keyword_set(debug)
refresh=keyword_set(refresh)

if n_elements(obsids) eq 0 then obsids=''
case 1 of 
   n_elements(p0) eq 1 and n_elements(p1) eq 1: begin ; user supplied time rang
      t0hcr=p0
      t1hcr=p1
   endcase
   required_tags(p0,'date_obs,date_end,obsid'): begin ; timeline or similar
      pstruct=p0
   endcase
   data_chk(p0,/string): begin ; assume OBSIDS
      pstruct=iris_fullobs2info(p0,/date_end)
      if not data_chk(pstruct,/struct) then begin 
         box_message,'Need time range, timeline structs or OBSID vector...
         return,pstruct
      endif
   endcase
   else: begin 
         box_message,'Need time range, timeline structs or OBSID vector...
         return,-1
   endcase
endcase

one2one=required_tags(pstruct,'date_obs,obsid')
if data_chk(pstruct,/struct) then begin 
   time_window,[pstruct.date_obs,pstruct.date_end],t0hcr,t1hcr,minute=[-2,10] ; slightly expanded HCR search range
   obsids=pstruct.obsid
endif

refresh=keyword_set(refresh) or n_elements(t0x) eq 0

if n_elements(t0x) eq 0 then begin  ; init
   t0x=''
   t1x=''
endif

if n_params() eq 2 then begin 
   if t0x eq t0hcr and t1x eq t1hcr then begin 
      if loud then box_message,'using cached HCR'
      count=n_elements(hcrx)
   endif else begin 
      box_message,'refreshing HCR
      hcrx=ssw_hcr_query(ssw_hcr_make_query(t0hcr,t1hcr,instrument='IRIS',/all_sel,_extra=_extra), $
         /remove_dummy,count=count)
      t0x=t0hcr
      t1x=t1hcr
   endelse
endif else hcrx=ssw_hcr_query(ssw_hcr_make_query(t0hcr,t1hcr,instrument='IRIS',/all_sel,_extra=_extra), $
         /remove_dummy,count=count)

hcr=hcrx

if debug then stop,'t0hcr,t1hcr'

if count eq 0 then begin
   box_message,'Nothing back from HCR query... tbd?  bailing..
   return,-1
endif

if one2one then begin 
   ss=tim2dset(anytim(hcr.starttime,/int),anytim(pstruct.date_obs,/int), delta=dts)
   retval=hcr[ss]
   retval=add_tag(retval,dts,'delta_obs2hcr') ; deltaT InputOBS:HCR (suspect)
   ssdt=where(abs(dts) gt 300,bdt)
   if bdt gt 0 then begin 
      box_message,'At least one hcr record > 2 minutes from OBS - see <output>.DELTA_OBS2HCR magnitudes (good ~0)
   endif
   count=n_elements(retval)
endif else begin
   retval=hcr
   if obsids[0] ne '' then begin 
      obs=obsids
      if n_elements(obs) eq 1 then obs=str2arr(obs)
      ss=where_arr(hcr.obsid,obs,count)
      retval=-1
      if count gt 0 then retval=hcr[ss] else box_message,'HCR in timerange, but none matching your OBSID
   endif
endelse

if required_tags(retval,'umodes') then begin 
   obspath=ssw_strsplit(retval.umodes,'iris_l2',/head)
   url='http://www.lmsal.com/solarsoft/'+obspath
   retval=add_tag(retval,obspath,'obspath')
   retval=add_tag(retval,url,'url')
   retval=add_tag(retval,time2file(retval.starttime,/sec) + '_' + retval.obsid,'full_obsid')
endif

if required_tags(retval,'obstitle,goal,sciobjectives') and data_chk(match_patterns,/string) then begin 
   mcount=count ; 
   ssm=ssw_hcr_where(retval,match_patterns,taglist=taglist,count=mcount)
   case 1 of 
      count eq 0: begin
         box_message,'HCR records in your obs/time range but none matching MATCH_PATTERNS'
         retval=-1
      endcase
      mcount eq count: if loud and count gt 1 then box_message,'all HCR records match MATCH_PATTERNS' 
      else: begin 
         if loud then box_message,strtrim(mcount,2) + ' out of ' + strtrim(count,2) + ' HCR records match desired MATCH_PATTERNS
         retval=retval[ssm]
         count=mcount
      endcase
   endcase
endif





return, retval
end




  


