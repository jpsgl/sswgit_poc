pro IRIS_MAKE_GEOWAVE_TABLE, kx, ky, $
	filename = filename, interactive = interactive, $
	rec_num = rec_num, img_path = img_path, t_start = t_start, $
	t_stop = t_stop, version = version, load = load, append = append, $
	text = text, path = path
	
;+
;
; Construct a table of geometric/wavelength correction coefficients. Wrapper
; for S. Jaeggli's IRIS_SPEC_CAL; see ITN 19
;
; INPUT:
;	kx	-	The x-axis correction matrix 
;	ky	-	The y-axis correction matrix
;
; KEYWORDS:
;	filename	-	the filename of the geometry calibration file to write
;	path=		-	the path to the output file
;	/interactive-	If set, then IRIS_SPEC_CAL is called and the user is prompted
;					to calculate new geometric/wavelength calibration parameters
;	/append		-	If set, then the input data are appended to the specified
;					filename. By default, a new file is written
;	/text		-	If set, then a tab-delimited text file is written; by default,
;					the output is a genx file
;	/load		-	If set, then previously-calculated corrections are read in
;					and written to a file
;
;	The following keywords specify the parameters of the correction entry to write
;	rec_num=	-	Integer record number
;	img_path=	-	string indicating the channel of the image
;	t_start=	-	string indicating the start time for the record entry (ISO)
;	t_stop=		-	string indicating the stop time for the record entry (ISO)
;	version=	-	integer version number
;
; WRITTEN:
;	P.Boerner 2013/06 boerner ~ at ~ lmsal ~ dot ~ com
;
;-

if not KEYWORD_SET(append) then append = 0
if N_ELEMENTS(path) eq 0 then path = GET_LOGENV('SSW_IRIS_DATA')
if KEYWORD_SET(text) then file_ext = '.txt' else file_ext = '.genx'
if N_ELEMENTS(filename) eq 0 then begin
	filename = TIME2FILE(/sec, RELTIME(/now)) + '_geowave' + file_ext
	filename = CONCAT_DIR(path, filename)
endif

if KEYWORD_SET(load) then begin
	; Load the results of previous runs of iris_spec_cal and use them to build a 
	; geowave table
	t_start = '2012-01-01T00:00:00Z'
	t_stop = '2033-01-01T00:00:00Z'
	version = 0
	
	img_path = 'FUVS'
	RESTORE, /verb, '~/idl/iris/iris_spec_cal/IRIS_spec_cal.fuvs.20131706.233310.sav'
	kx_fuvs = wave_solution.kx
	ky_fuvs = wave_solution.ky
	rec_num = 1
	IRIS_MAKE_GEOWAVE_TABLE, kx_fuvs, ky_fuvs, filename = filename, rec_num = rec_num, $
		img_path = img_path, t_start = t_start, t_stop = t_stop, version = version

	img_path = 'FUVL'
	RESTORE, /verb, '~/idl/iris/iris_spec_cal/IRIS_spec_cal.fuvl.20131806.162430.sav'
	kx_fuvl = wave_solution.kx
	ky_fuvl = wave_solution.ky
	rec_num = 2
	IRIS_MAKE_GEOWAVE_TABLE, kx_fuvl, ky_fuvl, filename = filename, rec_num = rec_num, $
		img_path = img_path, t_start = t_start, t_stop = t_stop, version = version, /append

	img_path = 'NUV'
	RESTORE, /verb, '~/idl/iris/iris_spec_cal/IRIS_spec_cal.nuv.20131806.163153.sav'
	kx_nuv = wave_solution.kx
	ky_nuv = wave_solution.ky
	rec_num = 3
	IRIS_MAKE_GEOWAVE_TABLE, kx_nuv, ky_nuv, filename = filename, rec_num = rec_num, $
		img_path = img_path, t_start = t_start, t_stop = t_stop, version = version, /append
		
	; Don't have good estimates for the SJI distortion, so I'll just replicate the 
	; NUV distortion values for now. Obviously this is not correct.
	nuv_paths = ['NUV-SJI', 'SJI_1330', 'SJI_1400', 'SJI_2796', 'SJI_2832']
	for i = 0, 4 do begin
		img_path = nuv_paths[i]
		rec_num = 4+i
		IRIS_MAKE_GEOWAVE_TABLE, kx_nuv, ky_nuv, filename = filename, rec_num = rec_num, $
			img_path = img_path, t_start = t_start, t_stop = t_stop, version = version, /append
	endfor
	
	RETURN
endif

if KEYWORD_SET(interactive) then begin
	; Call IRIS_SPEC_CAL now and interactively measure the geometric/wavelength correction
	PRINT, 'Calculate spectral correction, then save it...'
	RESTORE, file = '~/idl/iris/iris_spec_cal/test_images.sav', /verb
	IRIS_SPEC_CAL, wave = 'fuvs', fuvs
	correctfile = DIALOG_PICKFILE(path = '.', filter = '*.sav')
	RESTORE, correctfile, /verb
	kx = wave_solution.kx
	ky = wave_solution.ky
endif

; Set up the table entry
tnow = RELTIME(/now, out = 'CCSDS')
tnow_tag = TIME2FILE(tnow, /sec)
if not KEYWORD_SET(filename) then filename = '~/idl/iris/' + tnow_tag + '_geowave' + file_ext
if not KEYWORD_SET(rec_num) then rec_num = 0
if not KEYWORD_SET(img_path) then img_path = 'NUV'
if not KEYWORD_SET(t_start) then t_start = '2013-07-01T00:00:00Z'
if not KEYWORD_SET(t_stop) then t_stop = '2033-01-01T00:00:00Z'
if not KEYWORD_SET(version) then version = 0

this_rec_str = {rec_num : rec_num, img_path : img_path, t_start : t_start, t_stop : t_stop, $
	version : version, date : tnow, kx : kx, ky : ky}

if KEYWORD_SET(text) then begin
	; Print a text file contaning the geowave correction parameters
	kxsize = SIZE(kx)
	kysize = SIZE(ky)
	header = ['REC_NUM', 'IMG_PATH', 'T_START', 'T_STOP', 'VERSION', 'DATE']
	for i = 0, kxsize[1]-1 do for j = 0, kxsize[2]-1 do header = [header, 'KX' + STRTRIM(i,2) + STRTRIM(j,2)]
	for i = 0, kysize[1]-1 do for j = 0, kysize[2]-1 do header = [header, 'KY' + STRTRIM(i,2) + STRTRIM(j,2)]
	numhead = N_ELEMENTS(header)
	headdat = STRING(rec_num, form = '(i10)') + STRING(img_path, form = '(a10)') + $
		STRING(t_start, form = '(a25)') + STRING(t_stop, form = '(a25)') + $
		STRING(version, form = '(i10)') + STRING(tnow, form = '(a25)')
	linedat = STRJOIN(STRING(kx, form = '(e15.7)')) + STRJOIN(STRING(ky,form = '(e15.7)'))
	OPENW, lun, /get, filename, append = append
	PRINTF, lun, header, format = '(2a10,2a25,a10,a25,' + STRTRIM(kxsize[-1] + kysize[-1], 2) + 'a15)'
	PRINTF, lun, headdat + linedat
	FREE_LUN, lun
endif else begin
	; Save a GENX file with the geowave correction structure
	if KEYWORD_SET(append) then begin
		RESTGEN, file = filename, current_str
		outstr = [current_str, this_rec_str]
	endif else begin
		outstr = this_rec_str
	endelse
	SAVEGEN, file = filename, outstr
endelse



end