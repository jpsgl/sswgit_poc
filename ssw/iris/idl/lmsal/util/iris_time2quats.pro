function iris_time2quats, t0, t1, _extra=_extra, out_style=out_style, refresh=refresh, $
   lastn_hours=lastn_hours, apid=apid, all=all, quats=quats, pos=pos, cmd=cmd, $
   magc=magc, magu=magu, append=append, pstop = pstop
   
;+
;   Name: iris_time2quats
;
;   Purpose: like iris_time2temps, but gets quaternions and magnetometer data
;
;   Input Parameters:
;      t0 - vector of times or start of time-range
;      t1 - end of time range 
;      lastn_hours - like the name says, in lieu of t0,t1
;      all - return nested structure (multiple apids, curently 600/601/602)
;      apid - optional apid (def=600) 
;		quats - use apid 600 (get quaternions)
;		pos - use apid 601 (get position)
;		cmd	- use apid 602 (get commanded roll)
;		magu - use apid 710 (get uncompressed magnetometer data)
;		magc - use apid 711 (get compressed magnetometer data)
;      append=append - if T0 is a structure vector (image 'index' for example), return That w/temps tags appended
;
;   Method:
;      tlm / IRIS-itos dbase demo
;
;-

common iris_time2temps,tlm
common iris_time2temps1,lastapid, strtemp

if keyword_set(all) then begin
   retval=add_tag(retval,iris_time2quats(t0,t1,lastn_hours=lastn_hours,apid=600),'quats')
   retval=add_tag(retval,iris_time2quats(t0,t1,lastn_hours=lastn_hours,apid=601),'pos')
   retval=add_tag(retval,iris_time2quats(t0,t1,lastn_hours=lastn_hours,apid=602),'cmd')
;   retval=add_tag(retval,iris_time2quats(t0,t1,lastn_hours=lastn_hours,apid=710),'magc')
;   retval=add_tag(retval,iris_time2quats(t0,t1,lastn_hours=lastn_hours,apid=711),'magu')
   return,retval
endif

case 1 of 
   keyword_set(apid): ; user supplied (and caveat emptor..)
   keyword_set(quats): apid=600
   keyword_set(pos): apid=601
   keyword_set(cmd): apid=602
   keyword_set(magu): apid=711
   keyword_set(magc): apid=710
   else: apid=600
endcase

refresh=keyword_set(refresh)
if n_elements(lastapid) gt 0 then begin 
   delvarx,strtemp
   refresh=refresh or (apid ne lastapid)
endif
lastapid=apid

if n_elements(tlm) eq 0 then begin
   ssw_path,'$SSW_IRIS/gse/'
   recompile,'sag_ext_mnem'
   iris_set_gse,/vc2,tlm,/refresh ; force update of 'tlm' dbase
endif

if n_params() lt 1 and 1-keyword_set(lastn_hours) then begin 
   box_message,'No times supplied and LASTN_HOURS not set - defaulting to LASTN_HOURS=6
   lastn_hours=6
endif else begin 
   if n_params() eq 1 then begin ; vector of times?
      tempt0=t0
      if required_tags(t0,/date_obs) then t0=t0.date_obs
      time_window,temporary(t0),t0,t1,minutes=5  
   endif
endelse

if keyword_set(lastn_hours) then begin 
   t0=reltime(hours=(abs(fix(lastn_hours))*(-1)))
   t1=reltime(/now)
endif

apdata=sag_time2apdata(t0,t1,apid)

if KEYWORD_SET(pstop) then STOP

case apid of 
   600: pattern = 'A_QBI_AST_*'
   601: pattern = ['A_EP_*_J2000_*', 'A_EULERBR_*']
   602: pattern = 'A_CG_ROLL_ANGLE'
   710: pattern = 'B_MM_SCI_50C_DATA'
   711: pattern = 'B_MM_SCI_50U_DATA'
   else:	print, 'WTF?'
endcase
for i = 0, N_ELEMENTS(pattern) - 1 do begin
	isst=where(strmatch(tlm.name,pattern[i],/fold) and tlm.apid eq apid)
	if i eq 0 then sst = isst else sst = [sst, isst]
endfor
npoints = n_elements(sst)
names = tlm[sst].name
desc = names

estat=execute('strtemp={date_obs:"",' + arr2str(desc+':0.')+'}')
retval=replicate(strtemp,n_elements(apdata.times))
if n_elements(out_style) eq 0 then out_style='ccsds'
retval.date_obs=anytim(apdata.times,out_style=out_style)
for t=1,n_elements(sst) do retval.(t)=sag_ext_mnem(apdata.data,tlm[sst[t-1]].name)

if n_elements(tempt0) gt 0 then begin 
   mtimes=tempt0
   if required_tags(mtimes,/date_obs) then mtimes=anytim(mtimes.date_obs,/int)
   ss=tim2dset(anytim(retval.date_obs,/ints),mtimes)
   retval=retval[ss]
   if keyword_set(append) and data_chk(retval,/struct) then begin 
      retval=join_struct(tempt0,retval)
   endif   
   t0=tempt0 ; restore clobbered param
endif

return,retval
end














