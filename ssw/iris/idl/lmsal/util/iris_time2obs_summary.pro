pro iris_time2obs_summary, t0, t1, report, out_parent=out_parent, title=title, hcr_taglist=hcr_taglist, html=html, no_more=no_more
;+
;   Name: iris_time2obs_summary
;
;   Purpose: produce IRIS obs summary for user time range; demo 'iris_obs2hcr,MATCH_PATTERNS=<list>' and 'iris_time2timeline,/queue'
;
;   Input Parameters:
;      t0,t1 - desired time range ; default=last 7 days - if only 1 param than +7 days relative 
;
;   Output Parameters:
;      report - summary OBS report
;
;   Keyword Parameters:
;      hcr_taglist - optional comma delimited list of HCR tags to include in output 
;                    default='planners,starttime,stoptime,obsid,sciobjectives'
;      no_more (switch) - if set, don't echo report to tty
;      html (switch) - if set, report is html (not yet implemented)
;
;   History:
;      28-aug-2014 - S.L.Freeland - alpha/demo verions
; 
;   Restrictions: yes - for now, this is just a tty/ascii demo of iris_obs2hcr & iris_time2timline w/queue option.
;-
case n_params() of
   0: begin
         t0=reltime(days=-7)
         t1=reltime(/now)
   endcase
   1: t1=reltime(t0,days=7) ; 
   else:
endcase

moreit=1-keyword_set(no_more) & report=''
if n_elements(hcr_taglist) eq 0 then hcr_taglist='planners,starttime,stoptime,obsid,sciobjectives' ; default output tag list
calhopflare=['cal,sens','hop,coord','flare,watch'] ; category-search-patterns - these just happen to all have 2 elements; your lists may vary
otypes='*** '+strcapitalize(str2arr('calibration sequences, coordinated observations,flare and other watches,Other OBS run from Queue')) +' ***' ; category-titles
for i=0,n_elements(otypes)-2  do report=[temporary(report),$
   otypes[i] + ' (match_pattern='+calhopflare[i]+')', get_infox(iris_obs2hcr(t0,t1,match=calhopflare[i]),hcr_taglist),'']
qobs=iris_time2timeline(t0,t1,/queue)
if required_tags(qobs,'q_start,q_stop')  then qinfo=get_infox(qobs,'q_start,q_stop,duration,qid,description') else qinfo='NONE'
report=[temporary(report),otypes[i],qinfo] 
if moreit then more,report
 
return
end
