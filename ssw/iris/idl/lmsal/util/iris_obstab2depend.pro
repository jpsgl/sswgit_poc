function iris_obstab2depend, table, files_only=files_only, table_path=table_path, retval=retval, inc_data=inc_data, $
   check_files=check_files, debug=debug,_extra=_extra, urls=urls, nmissing=nmissing, test_missing=test_missing, fourpix=fourpix,$
   outdir=outdir, force_remote=force_remote, quiet=quiet, silent=silent, in_situ_fourpix=in_situ_fourpix
;
;+
;   Name: iris_table2dependencies
;
;   Purpose: input table file or structure -> expanded dependency info
;
;   Input Parameters:
;      table - IRIS obs table; file or ssw_iris_xml2struct output (structure image of .xml table)
;
;   Output:
;      returns nested structure w/dpendency info
;
;   Keyword Parameters:
;      files_only - if set, only return dependent file list(s)
;      table_path - path to associated tables (required if TABLE is structure 
;      inc_data - if set, include data for all files (default=file lists)
;      urls - if set, return urls instead of filepaths
;      nmissing (output) - number of files missing (only defined if /check set)
;      test_missing - optional string array of garbage "files" appended to test NMISSING
;      fourpix - check for .4pix. alternatives and preferentailly substitute (implies /CHECK)
;      force_remote - if set, use service
;      outdir - implies service - target for url copies
;
;   Restrictions:
;    sure.
;
;   History:
;      14-mar-2013 - S.L.Freeland
;       1-apr-2013 - use FDBDATA.CRSID for CRS file derivation; 
;                    allow FDB entry point (previous OBS+FRM)
;      30-may-2013 - S.L.Freeland add NMISSING output keyword
;      16-jul-2013 - S.L.Freeland - allow ulong input ; assumes I10.10 format in that case
;                    add /fourpix keyword & function
;      17-jul-2013 - S.L.Freeland - remove duplicate CRSFILES�
;      19-jul-2013 - S.L.Freeland - per Lucia, enforce CRS I05/I5.5 format
;      22-jul-2013 - S.L.Freeland - add cloud bit - remove duplicate FRM 
;      23-jul-2013 - S.L.Freeland - define NMISSING for remote
;      29-jul-2013 - S.L.Freeland - hook for predefined table server (via iris_obstab2depend_server.pro)
;       3-sep-2013 - S.L.Freeland - 4pix parent path /archive -> /irisa
;-
debug=keyword_set(debug)

dtype=data_chk(table,/type)
if dtype eq 13 or dtype eq 14 then begin ; input UL
   temptable=table
   table='Obs-'+string(table,format='(I10.10)')+'.xml'
endif

quiet=keyword_set(quiet) or keyword_set(silent)
loud=1-quiet

force_remote=keyword_set(force_remote) or 1-file_exist('/archive/iris/ops/tables') ; use web service

force_remote=1 ; S.L.Freeland - force server to access predefined tables
four_pix_local='/archive/iris/ops/tables/CRS_ALL'
four_pix_local='/irisa/ops/iris_user_tables/CRS_ALL'

fourpix=keyword_set(fourpix)
add_fourpix=0
case 1 of
   force_remote: begin
      if loud then box_message,'fetching from iris sswidl server..'
      if n_elements(outdir) eq 0 then outdir=curdir()
      fourp=(['','&fourpix=1'])(keyword_set(fourpix))
      service='http://www.lmsal.com/cgi-ssw/ssw_iris_service_obstab2depend.sh?OBSID='+table+fourp
      sock_list,service,xmlurls
      nurls=n_elements(xmlurls)
      if n_elements(xmlurls) le 1 then  begin 
         box_message,'Switching to predefined table server'
         tablex=strtrim(table,2)
         if strpos(tablex,'xml') ne -1 then tablex=strextract(tablex,'-','.')
         allfiles=iris_obstab2depend_server(tablex,nmissing=nmissing,outdir=outdir)
         add_fourpix=fourpix and file_exist(four_pix_local)
      endif else begin 
         obsdir=concat_dir(outdir,strextract(table,'-','.'))
         if loud then box_message,['Transferring ' + strtrim(nurls,2) + ' urls.. to ' + obsdir]
         mk_dir,obsdir
         allfiles=file_search(obsdir,'*xml')
         ssw_file_delete,allfiles
         if loud then box_message,xmlurls
         sock_copy,xmlurls,out_dir=obsdir
         allfiles=file_search(obsdir,'*xml')
         nmissing=n_elements(xmlurls) - n_elements(allfiles)
      endelse
      if add_fourpix then begin
         box_message,'checking for 4pix swapins
         crs=where(strmatch(allfiles,'*crs-*',/fold),crscnt)
         box_message,allfiles[crs]
         crsnames=ssw_strsplit(allfiles[crs],'/',/tail,head=localdir)
         crs4pix=str_replace(crsnames,'.xml','.4pix.xml')
         full4pix=concat_dir(four_pix_local,crs4pix)
         fpe=where(file_exist(full4pix),fpcnt)
         if fpcnt gt 0 then begin
            box_message,'at least one fourpix found
            if keyword_set(in_situ_fourpix) then allfiles[crs[fpe]]=full4pix[fpe] else begin ; copy->
               ssw_file_delete,allfiles[crs[fpe]] ; clean up non fourpix
               for fp=0,fpcnt-1 do spawn,['cp',full4pix[fpe[fp]],localdir[0]],/noshell
               allfiles=file_search(localdir[0],'*.xml')
            endelse
         endif
      endif
      types=str2arr('obs,frm,fdb,crs')
      for t=0,3 do begin 
         ssm=where(strmatch(allfiles,'*'+types[t]+'*',/fold),tcnt)
         tf=allfiles[ssm]
         if tcnt eq 1 then tf=tf(0)
         retval=add_tag(retval,tf,types[t]+'files')
      endfor
      retval=add_tag(retval,allfiles,'allfiles')
         
      ; !! Early Exit if web service !! ; 
      return,retval  ; << Unstructured exit
   endcase
   data_chk(table,/struct) and file_exist(table_path): begin 
      tabstr=table
   endcase
   file_exist(table): begin
      tabstr=ssw_iris_xml2struct(table)
      break_file,table,ll,table_path
   endcase
   data_chk(table,/string): begin 
      table=table+(['.xml',''])(strmatch(table,'*.xml',/fold))
      tabloc=iris_obstable2path(table,/concat,_extra=_extra)
      if file_exist(tabloc) then tabstr=ssw_iris_xml2struct(tabloc) else begin 
         box_message,'Cannot find> ' + table[0] + ' .. Please set $IRIS_TABLES or supply absolute pth'
         return,-1
      endelse
   endcase
   else: begin 
   endcase
endcase
inc_data=keyword_set(inc_data)
check_files=keyword_set(check_files) or keyword_set(test_missing) or keyword_set(fourpix)

data=gt_tagval(tabstr,/data,missing=-1)

if   required_tags(data,'tr,fid,cadence') then begin; OBS input
      if file_exist(table) then obsfile=table else $
         ;obsfile=concat_dir(table_path,'OBS-'+strtab.fid+'.xml')
         obsfile=iris_obstable2path(table,/concat,_extra=_extra)
      retval=add_tag(retval,obsfile,'obsfiles')
      if inc_data then retval=add_tag(retval,tabstr,'obs')
      frmlist=gt_tagval(data,/fid)
      ;frmfiles=concat_dir(table_path,'FRM-'+frmlist+'.xml')
      frmfiles=iris_obstable2path('FRM-'+frmlist+'.xml',/concat,_extra=_extra)
      retval=add_tag(retval,all_vals(frmfiles),'frmfiles')
      fok=total(file_exist(frmfiles)) eq n_elements(frmfiles)
      if inc_data and fok then retval=add_tag(retval,iris_multitab2struct(frmfiles),'frm')
endif


if  required_tags(data,'tr,sji,nuv,fuv') then begin
   fdata = data 
   retval=add_tag(retval,table,'frmfiles')
endif else begin  
   if required_tags(retval,'frmfiles') then  begin 
      fok=total(file_exist(frmfiles)) eq n_elements(frmfiles)
      if fok then fdata=iris_multitab2struct(retval.frmfiles)
   endif
endelse

if required_tags(tabstr,/crsid) then begin 
   fdblist=tabstr.header.id ; FDB entry
endif else begin 
   fdblist=[gt_tagval(fdata,/sji) + ':sji',gt_tagval(fdata,/nuv)+':nuv',gt_tagval(fdata,/fuv)+':fuv']
   fdblist=all_vals(fdblist) ; uniq subset, nulls removed
   fdblist=ssw_strsplit(fdblist,':',tail=crstype)
   fdblist=fdblist(rem_elem(fdblist,'0')) ; remove null elements
endelse
;fdbfiles=concat_dir(table_path,'FDB-'+fdblist+'.xml')
fdbfiles=iris_obstable2path('FDB-'+fdblist+'.xml',/concat,_extra=_extra)
if required_tags(retval,'fdbfiles') then retval=rep_tag_value(retval,[retval.fdbfiles,fdbfiles],'fdbfiles')  else $ 
         retval=add_tag(retval,fdbfiles,'fdbfiles')

fdbinfo=iris_multitab2struct(retval.fdbfiles,/info)
crstype=fdbinfo.info.type
fdbdata=iris_multitab2struct(retval.fdbfiles)

;crslist='CRS-'+fdblist+"-"+crstype
crslist='CRS-'+string(fdbdata.data.crsid,format='(I5.5)')+"-"+crstype
;crsfiles=concat_dir(table_path,crslist+'.xml')
crsfiles=iris_obstable2path(crslist+'.xml',/concat,_extra=_extra)
crsfiles=all_vals(crsfiles)
retval=add_tag(retval,crsfiles,'crsfiles')

if check_files then begin 
   allfiles=''
   patts=str2arr('obs,frm,fdb,crs')
   if keyword_set(fourpix) then begin ; preferentially use .4pix. IF they exist
      fourp=str_replace(str_replace(retval.crsfiles,'.xml','.4pix.xml'),'/CRS/','/CRS_ALL/') ; 
      sse=where(file_exist(fourp),fpcnt)
      if fpcnt gt 0 then retval.crsfiles[sse]=fourp[sse]
   endif
   for f=0,n_elements(patts)-1 do $ 
      allfiles=[allfiles,gt_tagval(retval,patts[f]+'files',missing='')]
   if data_chk(test_missing,/string) then allfiles=[allfiles,test_missing]
   ss=where(allfiles ne '',fcnt)
   if fcnt eq 0 then allfiles='' else allfiles=allfiles[ss]
   fexist=where(file_exist(allfiles),ecnt)
   case 1 of 
      fcnt eq 0 or ecnt eq 0: box_message,["None of the files exist",allfiles]
      ecnt eq fcnt: box_message,"All files exist"
      else: box_message,["Some files missing:",allfiles[ where(1-file_exist(allfiles))]]
   endcase
   nmissing=fcnt-ecnt  ; number missing
   retval=add_tag(retval,allfiles,'allfiles')
endif

if keyword_set(urls) then begin 
    wwwvect='http://www.lmsal.com/solarsoft/'
    rtn=tag_names(retval)
    ssf=where(strpos(rtn,'FILES') ne -1,fcnt)
    for f=0,fcnt-1 do begin
       retval.(ssf[f])=wwwvect+retval.(ssf[f])
    endfor
endif

if debug then stop,'retval' 

if n_elements(temptable) gt 0 then table=temptable ; restore input

return,retval
end
      


