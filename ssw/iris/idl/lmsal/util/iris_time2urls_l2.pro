function iris_time2urls_l2, t0, t1, obsdirs_only=obsdirs_only, pattern=pattern, parent=parent, $
   count=count, debug=debug, loud=loud
;
;+
;   Name: iris_time2urls_l2
;
;   Purpose: helper function for iris_time2files w/some L2 value added
;
;   Restrictions:
;      more of a comment - should make this an ssw_time2filelist option
;      
;      
;
debug=keyword_set(debug)
loud=keyword_set(loud)
if n_elements(obsdirs_only) eq 0 then obsdirs_only=0
if n_elements(parent) eq 0 then parent = $
   'http://www.lmsal.com/solarsoft//irisa/data/level2/'
topurls=ssw_time2paths(t0,t1,parent=parent)+'/'
retval=''
for u=0,n_elements(topurls)-1 do begin 
   if loud then box_message,topurls[u]
   sock_list,topurls[u],listx
   listx=strcompress(strlowcase(listx),/remove)
   ssobs=where(strmatch(listx,'*href="????????_??????_??????????/*'),obscnt)
   if obscnt gt 0 then begin
      urlsx=topurls[u]+strextract(listx[ssobs],'href="','/')
      retval=[retval,urlsx]
   endif else if loud then box_message,'No obsid subdir'
endfor
if n_elements(retval) gt 1 then begin 
   retval=retval[1:*]
   odtimes=anytim(file2time(retval))
   sst=where(odtimes ge anytim(t0) and odtimes le anytim(t1),sstcnt)
   if sstcnt gt 0 then begin
      retval=retval[sst]
   endif else retval=''
endif
if 1-obsdirs_only and retval[0] ne '' then begin 
   l2temp=''
   l2pat='href="iris_l2'
   for obd=0,n_elements(retval)-1 do begin
      sock_list,retval[obd],listx
      listx=strcompress(strlowcase(listx),/remove)
      ssl2=where(strpos(listx,l2pat) ne -1,l2cnt)
      if l2cnt gt 0 then begin
         urlsx=retval[obd]+'/'+'iris_l2'+strextract(listx[ssl2],l2pat,'">') 
         l2temp=[l2temp,urlsx]
      endif
   endfor
   retval=''
   if n_elements(l2temp) gt 1 then begin
      retval=[l2temp[1:*]]
      sssji=where(strpos(retval,'_sji_') ne -1,sjicnt)
      if sjicnt gt 0 then retval[sssji]=str_replace(retval[sssji],'_sji_','_SJI_')
   endif
endif

count=n_elements(retval) * (retval[0] ne '')
if debug then stop,'before return,retval...'
return,retval
end
