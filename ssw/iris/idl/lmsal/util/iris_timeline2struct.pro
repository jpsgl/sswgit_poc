function iris_timeline2struct, timeline, out_style=out_style, debug=debug, loud=loud, timeline_names=timeline_names, dump=dump
;+
;   Name: iris_timeline2struct
;
;   Purpose: IRIS timeline file -> structure/info vector
;
;   Input Paramters:
;      timeline - IRIS timeline.txt file
;
;   Keyword Paramters:
;      out_style - optional 'anytim' time format - default=CCSDS
;      timeline_names=timeline_names - if set, include path/name of timeline file
;
;
;   History:
;      23-jan-2013 - S.L.Freeland
;      19-jul-2013 - S.L.Freeland - evolution/value added for current revision - now, with more info.
;      23-jul-2013 - S.L.Freeland - apply endtime dTDOY to timeline start, not sequence start
;      23-jul-2013 - S.L.Freeland - add /TIMELINE_NAMES keyword & function & new header fmt (backwardly compat I think)
;      25-jul-2013 - S.L.Freeland - handle July 25 format change
;       2-jan-2014 - S.L.Freeland - duh, need to pass year -> doy2utc.pro ... happy new year!
;      21-jan-2014 - S.L.Freeland - re-do the rollover/date_end (tbd, but better than 20-jan-2014 hiccup..)
;   
;   Restrictions:
;-

if not file_exist(timeline) then begin 
   box_message,'Need IRIS timeline file.. bailing
   return,-1
endif
debug=keyword_set(debug)
loud=keyword_set(loud)

dat=rd_Tfile(timeline)
if n_elements(out_style) eq 0 then out_style='ccsds' ; default time format
sst0=where(strmatch(dat,'*T*FIL*'),t0cnt)
sst01=where(strmatch(dat,'*T*REGOTL*'),t01cnt)

case 1 of 
   t0cnt eq 1: yydoy=strextract(ssw_strsplit(dat[sst0],':',/tail),'T','TIMFIL')
   t01cnt eq 1:yydoy=strextract(dat[sst01],'T','REG')
   else: begin 
      box_message,'Unexpected T0, bailing...'
      return,-1 ; early exit
   endcase
endcase 

yy=fix(strmid(yydoy,0,2))
doy=fix(strmid(yydoy,2,1000))
tldoy=doy
if keyword_set(dump) then begin  ; kludge....
   retval={dump_start:'',band:'',duration:0.0,station:'',dump_end:''}
   ssd=where(strmatch(dat,'*dump_start*xmit_path*',/fold),startcnt)
   ssdend=where(strmatch(dat,'*dump_stop*xmit_path*',/fold),endcnt)
   retval=replicate(retval,startcnt)
   date_obs=ssw_strsplit(dat(ssd),' ',/head,tail=rest)
   date_end=ssw_strsplit(dat(ssdend),' ',/head,tail=rest)
   doc=str2cols(date_obs,':',/trim)
   dec=str2cols(date_end,':',/trim)
   strtab2vect,doc,year,doy,hh,mm,ss
   time=hh+':'+mm+':'+ss
   retval.dump_start=anytim(anytim(doy2utc(doy,year),/ecs,/date_only) + ' ' + time,out_style=out_style) 
   strtab2vect,dec,year,doy,hh,mm,ss
   time=hh+':'+mm+':'+ss
   retval.dump_end=anytim(anytim(doy2utc(doy,year),/ecs,/date_only) + ' ' + time,out_style=out_style) 
   retval.duration=ssw_deltat(retval.dump_end,ref=retval.dump_start,/sec)
   retval.band=strextract(strupcase(rest),'XMIT_PATH=','BAND')
   
   



endif else begin 
retval={date_obs:'',date_end:'',obsid:0ul,repeats:0,duration:0.,xcen:0.,ycen:0.,tracking:0, slewed:0, count:0,size:0l,pzta:0l,pztb:0l,pztc:0l,description:''}
ss=where(strmatch(dat,'*OBSID=*rpt*endtime*',/fold),startcnt)
if startcnt eq 0 then begin 
   return,-1
endif

delim='OBSID='
desc='desc='

startdat=dat[ss]

startdata=strextract(delim+startdat,delim,desc)
description=strtrim(strextract(startdat+delim,desc,delim),2)
startdata=str_replace(startdata,' x ',' ,') ; separate reps from duration
startdata=str_replace(startdata,'//','')
startdata=str_replace(startdata,' Mbits','') ; July 25 format change fix

times=ssw_strsplit(startdata,'OBSID=',/head,tail=obsstuff)

cols=str2cols(obsstuff,' ',/trim,/unalign)

strtab2vect,cols,obsid,repeats,duration,size,dtdoy,endtime
 
retval={date_obs:'',date_end:'',obsid:0ul,repeats:0,duration:0.,size:0.,description:''}
retval=replicate(retval,startcnt)
retval.description=description
retval.repeats=str2number(repeats)
retval.duration=str2number(duration)
retval.obsid=ulong(obsid)
retval.size=str2number(size) 

if n_elements(out_style) eq 0 then out_style='ccsds'
cols=str2cols(times,':',/trim)
strtab2vect,cols,yy,doy,hh,mm,ss
time=hh+':'+mm+':'+ss
retval.date_obs=anytim(anytim(doy2utc(doy,yy),/ecs,/date_only) + ' ' + time,out_style=out_style)
retval.date_end=anytim(anytim(doy2utc(tldoy+str2number(dtdoy),yy),/ecs,/date_only) + ' ' + endtime,out_style=out_style)

dt=ssw_deltat(retval.date_end,ref=retval.date_obs,/days)
ssd=where( dt gt 364,ycnt)
if ycnt gt 0 then begin 
   box_message,'rollover; calculating date_end from (date_obs+(duration*repeats))
   retval[ssd].date_end=anytim(anytim(retval[ssd].date_obs) + (retval[ssd].duration*retval[ssd].repeats),out_style=out_style)
endif

endelse

if keyword_set(timeline_names) then begin 
   retval=add_tag(retval,replicate(timeline,n_elements(retval)),'timeline_name')
endif
return,retval
end

