function iris_jsoc_strtemplate, level, generate=generate, merge=merge
;+
;   
;   Name: iris_jsoc_strtemplate
;
;   Purpose: return structure template for desired IRIS data level; assure JSOC data typing for read_iris output 'index'
;
;
;   Input Paramters:
;       level - data level
;
;   Keyword Parameters:
;      generate - access jsoc to derive/update template database (restricted)
;
;   Calling Sequence:
;      IDL> template=iris_jsoc_strtemplate(level) ; 
;
;   History:
;      1-sep-2013 - S.L.Freeland
;     19-sep-2013 - S.L.Freeland - level one data series upped to lev1_prelim04
;
;   This is called transparently by read_iris and expect of little use by user/other apps  
;
;-

ds=str2arr('lev0,lev1_prelim04')

idata=concat_dir('$SSW_IRIS','data')

tempdb=concat_dir(idata,'iris_jsoc_strtemplate.geny')

generate=keyword_set(generate)

retval=-1
if generate then begin 
   if is_member(get_user(),'freeland,bdp,boerner') then begin 
       for d=0,n_elements(ds)-1 do begin 
          serstr=ssw_jsoc(/series_struct,ds='iris.'+ds[d])
          retval=add_tag(retval,serstr,ds[d])
          savegenx,file=tempdb,/over,retval
       endfor
   endif else box_message,'/GENERATE requested but not authorized
endif else begin
   if file_exist(tempdb) then begin
      restgenx,file=tempdb,tempstrs
      if keyword_set(merge) then begin 

      endif else begin 
         if n_elements(level) eq 0 then level=1
         ss=where(strmatch(ds,'*'+strtrim(str2number(level),2)+'*'),mcnt)      
         if mcnt gt 0 then retval=ssw_jsoc_keywords2struct(tempstrs.(ss[0])) else $
            box_message,'No template for input LEVEL
      endelse
   endif else box_message,'No IRIS/JSOC template dbase online(?)'
endelse

return,retval
end


;
;
