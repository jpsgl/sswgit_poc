function iris_obstable2path,tables, parent_path=parent_path, flat=flat, concatdir=concatdir
;
;   Name: iris_table2path
;
;   Purpose: map from table names -> absolute paths  (single point if table organization evolves) - def relative to $IRIS_TABLES 
;
;   Input Parameters:
;      tables - one or more table names
;
;   Keyword Parameters:
;      parent_path - parent path - overrides $IRIS_TABLES 
;      flat - if set, all stuff comingled  - default assumes <PARENT_PATH>/{OBS,FDB,FDT,CRS}/<tabfiles>
;      concatdir - if set, return fully qualified table path (default is implied path)
;
;   Calling Examples:
;      IDL> paths=iris_table2path('OBS...xml' ) ; return $IRIS_TABLES/OBS/
;      IDL> files=iris_table2path('FDT...xml',parent='/archive/blah',/concat) ; return /archive/blah/FDT/FDT...xml'
;
;   History:
;      27-may-2013 - S.L.Freeland
;-

; define parent

def_parent=get_logenv('$IRIS_TABLES')

if n_elements(tables) eq 0 then tables=''

case 1 of 
   file_exist(parent_path):  ; user defined
   file_exist(def_parent):  parent_path=def_parent ; using environmental
   file_exist(tables[0]): begin 
      break_file,tables[0],ll,parent_path
   endcase
   else: parent_path=curdir() ; maybe useless
endcase

retval=parent_path

if n_elements(tables) eq 1 and tables[0] eq ''   then begin
   box_message,'No tables supplied - just returning parent
endif else begin 
   itables=strtrim(tables,2)
   tpatt=strupcase(strmid(itables,0,3))
   if not keyword_set(flat) then retval=concat_dir(retval,tpatt)
   if keyword_set(concatdir) then retval=concat_dir(retval,itables)
endelse

return,retval
end
   



  

