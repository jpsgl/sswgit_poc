function iris_patchcsv, badcsv
;
;
;+
;   Name: iris_patchcsv
;
;   Purpose: clean up control characters, special characters etc.
;
;   History:
;      Circa: 15-mar-2013 - S.L.Freeland
;      27-apr-2013 - protect against -1 (not found cases)
;      19-jun-2013 - use alternate/uniq filename
;
;-
break_file,badcsv,ll,pp,ff,ee

newname=ff+ee+'_'+get_user() + '_' + time2file(reltime(/now),/sec)+'_'+ssw_strsplit(get_logenv('IDL_STARTUP'),'.',/tail)

retval=concat_dir(get_temp_dir(),newname)


dat=rd_Tfile(badcsv)

maxobs=n_elements(dat)/5  

obs=',' + strtrim(indgen(maxobs),2) + ','

obs=obs[9:*]

sss=lonarr(n_elements(obs))
for i=0,n_elements(sss)-1 do begin 
   ss=where(strpos(dat,obs[i]) ne -1)
   sss[i]=ss[0]
endfor

null=where(strpos(dat,',,,,') ne -1)
sss=[1,sss(where(sss ne -1)),null[1]]


ndat=[dat[0],strarr(n_elements(sss))]
for i=0,n_elements(sss)-2 do begin 
   ndat[i]=arr2str(dat[sss[i]:sss[i+1]-1],' ')
endfor
ndat=strarrcompress([dat[0],ndat])

bdat=byte(ndat)
if get_logenv('check_csv') ne '' then stop,'ndat'
ss=where_pattern(bdat,'"',sscnt)
if sscnt gt 0 then bdat[ss]=byte("A")
ss=where_pattern(bdat,226b,sscnt)
if sscnt gt 0 then bdat[ss]=byte("A")
ss=where(bdat ge 128b,sscnt)
if sscnt gt 0 then bdat[ss]=32b
ndat=strcompress(string(bdat))
ndat[1]=str_replace(ndat[1],'A','')
file_append,retval,ndat,/new

return,retval
end
