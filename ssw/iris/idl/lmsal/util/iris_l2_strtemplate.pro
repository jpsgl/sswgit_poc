function iris_l2_strtemplate,raster=raster, sji=sji
;
;+
;   Name: iris_l2_strtemplate
;
;   Purpose: return iris level2 raster templates for strict data typing -> fitshead2struct
;
;   Input Paramters:
;      
;   Keyword Parameters:
;      sji - if, SJI template
;      raster - if set, raster template (default)
;
;-

sji=keyword_set(sji)
raster=1-sji

tags='time,pztx,pzty,exptimef,exptimen,sumsptrf,sumspatf,sumspatn,dsrcfix,dsrcrcnix,lutidf,lutidn'

create_struct_temp,retval,'',tags,replicate('D',n_elements(str2arr(tags)))

return,retval
end    
