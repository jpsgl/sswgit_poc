function next_step,m1now,mm1f,s1,m2now,mm2f,s2,steptable,offset2,defaultstep
; Returns next positions for (m1,m2) after (m1now,m2now) on the
; way to (mm1f,mm2f)
; All of these positions may be negative or > 240:  mod240 done later
; s1 & s2 are +/-1, showing direction of motion for m1 & m2
; steptable is a constant table of steps for the final few moves
; See iris_slew_path5 for the other inputs
; Version in iris_slew_path5.pro
;  TDT  24-May-2011
; Version in iris_slew_path6.pro 
;  Changes to formula for rnow and the case where rnow is used
;  Changes marked with ***********************
;  TDT  12-Sep-2011

d1 = abs(m1now-mm1f)
d2 = abs(m2now-mm2f)
; rnow determines how far from GT boresight we are pointed
;   hence determines how big the steps are when moving azimuthally,
;   ie when moving both motors the same amount in the same direction
rnow = fix(abs(m2now+offset2-m1now)) MOD 240; ***********************
if (rnow gt 120) then rnow = 240-rnow

if (d1 le 12 and d1 gt 0) then begin
; Final few moves for m1
   if ((mm1f-m1now)*s1 lt 0) then $
      m1next = m1now+s1*steptable(0) $  ; overshot, shouldn't ever happen
      else m1next = m1now+s1*steptable(d1)
   return,[m1next,m2now]

end else if (d2 le 12 and d2 gt 0) then begin
; Final few moves for m2
   if ((mm2f-m2now)*s2 lt 0) then $
      m2next = m2now+s2*steptable(0) $  ; overshot, shouldn't ever happen
      else m2next = m2now+s2*steptable(d2)
   return,[m1now,m2next]

end else if (d1 eq 0) then begin
; M1 finished, move m2 only
   return,[m1now,m2now+defaultstep*s2]

end else if (d2 eq 0) then begin
; M2 finished, move m1 only
   return,[m1now+defaultstep*s1,m2now]

end else if (rnow gt 12 and rnow le 78 and s1 eq s2) then begin ;*************
; Move both motors at once (azimuthally on the Sun), if allowed
; New step function definition for Version 6 follows  *************************
;   step = 3+fix((92-rnow)/20)
   if (rnow lt 34) then step = 6 else if (rnow lt 42) then step = 5 else $
      if (rnow lt 54) then step = 4 else step = 3
   return,[m1now+s1*step,m2now+s2*step]

end else if (d1 ge d2) then begin
; Move m1 since it has further to go
   return,[m1now+defaultstep*s1,m2now]

end else begin
; Move m2 since it has further to go
   return,[m1now,m2now+defaultstep*s2]
end

end


function iris_slew_time6,m1i,m2i,m1f,m2f,defaultstep=defaultstep,margin=margin,secperhop=secperhop
;+
; Name:  Function iris_slew_time6
;
; Returns estimated time for a slew in seconds for IRIS to slew
;   from initial (m1i,m2i) to final (m1f,m2f)
; Assumes secperhop seconds per intermediate position [def = 10 s]
; Margin = added time for Mom Mngmnt & uncertainty [def = 120 s]
; Defaultstep = parameter set in FSW by /I_SLEW_STEP_SZ SIZE=5 [def = 5]
; Derived from iris_slew_path6.pro.allow (timeline version)
;  TDT version 1.0  3-Dec-2012
;
; defaultstep = an integer parameter to be set during commissioning,
;   permitted range 3 - 6
;   if 4, then most single motor moves = 4 steps = 63 arcsec, 
;   if 5, then most = 5 steps = 78 arcsec.
;
; The intermediate positions satisfy the following:
;   Each motor moves only 0, +/-3, 4, 5 or 6 steps.
;   Each motor moves in the shortest direction from initial to final.
;   The last move for each motor is always +/-5 steps.
;   All moves are in the range 32 - 95 arcsec.
;
; TDT 24-May-2011
;-
;
; version 6, iris_slew_path6
; changes to next_step, fixing bug in rnow and making maximum step <
; 95 arcseconds
;
; TDT 12-Sep-2011


if not keyword_set(margin) then margin = 120
if not keyword_set(defaultstep) then defaultstep = 5
if not keyword_set(secperhop) then secperhop = 10
; offset2 is set in FSW by /I_SLEW_WM2_OFF OFFSET=114
offset2 = 114

if (defaultstep gt 6) then defaultstep = 6
if (defaultstep lt 3) then defaultstep = 3
defaultstep = fix(defaultstep)


; Make up destinations that ensure motion goes shortest way around
; mm1f & mm2f may be < 0 or > 239;  this is OK for counting hops
if (m1f gt m1i+120) then mm1f = m1f-240 else if (m1f lt m1i-120) then mm1f=m1f+240 else mm1f=m1f
d1 = mm1f-m1i
if (d1 ge 0) then s1=1 else s1=-1
d1 = abs(d1)

if (m2f gt m2i+120) then mm2f = m2f-240 else if (m2f lt m2i-120) then mm2f=m2f+240 else mm2f=m2f
d2 = mm2f-m2i
if (d2 ge 0) then s2=1 else s2=-1
d2 = abs(d2)

; Initialize the path hop counter
m1now = m1i
m2now = m2i
nhops = 0

steptable = [-5,-4,-3,-5,-4, 5,-3,-3,3,4,5,6, 3]

while ( (m1now ne mm1f) or (m2now ne mm2f) ) do begin
   temp = next_step(m1now,mm1f,s1,m2now,mm2f,s2,steptable,offset2,defaultstep)
   m1now = temp(0)
   m2now = temp(1)
   nhops = nhops+1
end

return,margin+secperhop*nhops
end

