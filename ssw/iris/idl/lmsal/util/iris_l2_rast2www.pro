pro iris_l2_rast2www,obspath, index,data, sub_time=sub_time, movie_dir=movie_dir , suffix=suffix,  $
   confact=confact, new_paradigm=new_paradigm, waves=waves, html=html, old_paradigm=old_paradigm, $
   no_output=no_output, no_graphics=no_graphics,_extra=_extra, fe_include=fe_include, $
   no_normalize=no_normalize, timerange=timerange, goes_label=goes_label
;
;+
;   Name: iris_l2_rast2www
;
;   Purpose: one IRIS obs -> www raster+sji summary movie;
;
;   Input Parameters:
;      obspath - absolute path of desired input Level2 obs (e.g. where the raster/sji fits files exist)
;
;   Output Parameters:
;      index,data - optional movie data ouput (conctenated raster+sji+goes)
;
;   Keyword Parameters:
;      sub_time - optional timerange, assumed sub-range within OBS time range (default OBS start:stop)
;      timerange - synonym for SUB_TIME - eg, optional start+stop range within OBS
;      old_paradigm - use original format 
;      new_paradigm - new format (now the default)
;      WAVES - optional comma delimted list of raster lines to include
;      no_grphics=no_graphics - if set, don't generate www graphics/movies, just return,'index,data'
;      confact - spatial scaling factor - default=.6 (applied to each raster window + sji
;      fe_include (swith) - if set, add 'Fe XII' to default WAVES list ('C II, O I, Si IV (one), Mg II')
;      suffix - optional movie_dir suffix (assume writing to online-area <level2>/level2_nrt/www/raster_<suffix>/
;      
;
;   History:
;      1-may-2014 - S.L.Freeland - initially for IRIS WWW summary pages
;     25-aug-2014 - S.L.Freeland - add /FE_INCLUDE
;      9-nov-2014 - S.L.Freeland - add data_ops to users authorized to write to "official" data areas
;     25-nov-2014 - S.L.Freeland - $iris_raster_flare set implies /fe_include and confact=.8
;     12-jan-2015 - S.L.Freeland - fix typo in default WAVES; caused repeat/double C II cube
;     21-mar-2015 - S.L.Freeland - add /no_dustbust & /no_intscale & made defaults dustbuster + iris_intscale
;
;   Method:
;      calls read_iris_l2, read_iris_l2_multi_rast, event_movie, special_movie...
;   
;   Restrictions:
;      high level wrapper - see read_iris_l2_multi_raster
;-
;   

dustbust=1-keyword_set(no_dustbust)
intscale=1-keyword_set(no_intscale) 

no_graphics=keyword_set(no_graphics) or keyword_set(no_output)
do_movie=1-keyword_set(no_graphics)

if no_graphics and n_params() lt 2 then begin 
   box_message,"/NO_GRAPHICS specified, yet no output parameters either - won't waste your time..."
   return
endif

check_other=get_logenv('check_other') ne ''
if n_elements(suffix) eq 0 then suffix='' else suffix=(['','_'])(strpos(suffix,'_') eq -1) + suffix
case 1 of 
   data_chk(movie_dir,/string):  ; user supplied
   is_member(get_user(),'freeland,data_ops'): movie_dir=concat_dir(obspath,concat_dir('www','raster')+suffix) ; need IRIS L2 write permission
   else: begin 
      movie_dir=concat_dir(curdir(),status.obsid) ; some default
   endcase
endcase

ptemp=!d.name
set_plot,'z

if 1-file_exist(obspath) then begin 
   box_message,'Need and existing L2 obspath
   return
endif

status=iris_l2_status(obspath)

nrast=status.nrast
nsji=status.nsji
if nrast eq 0 or nsji eq 0 then begin 
   box_message,'Missing rasters and/or sjis for this obs..
   return
endif

t0=status.l2_obsstart
t1=status.l2_obsend
obsid=status.obsid

rast=file_search(obspath,'*rast*.fits',/fold)
sji =file_search(obspath,'*SJI*.fits',/fold)

read_iris_l2,rast,iix,/silent
rinfo=iris_l2_rastinfo(rast)

f0=t0
f1=t1
if n_elements(sub_time) eq 2 then timerange=sub_time ; synonym
if n_elements(timerange) eq 2 then begin 
   f0=timerange[0]
   f1=timerange[1]
endif

ss=ssw_time2epoch(iix.date_obs,f0,f1)
ssf=where(ss eq 0,fcnt)
if get_logenv('check_sub_time') ne '' then stop,'sub+times,f0,f1
fexii=(['','Fe,'])(keyword_set(fe_include))
if n_elements(waves) eq 0 then begin
   sidef='Si IV '+ (['1403','1394'])(strpos(arr2str(rinfo.desc),'1403') eq -1) ; indclude Si 1403 if present, Si 1394 if not
   waves='C II,'+ fexii +'O I,' + sidef + ',Mg II k'
   waves=str_replace(strtrim(waves,2),',,',',')
   box_message,'Default WAVES='+waves
endif
i0=iix[0]
rss=ssf/i0.nexp
if get_logenv('check_scale') then stop,'rast,rss,rindex,rdata'
read_iris_l2_multi_rast,rast[rss[0]:last_nelem(rss)],rindex,rdata,waves=waves,/scale,/log, _extra=_extra 
rddim=data_chk(rdata,/ndim) ; #dimensions data (3=sit&stare, 4=other)
if n_elements(timerange) eq 2 then begin ; stop,'rindex,rdata'
   ss=ssw_time2epoch(rindex.date_obs,f0,f1)
   sst=where(ss eq 0)
   rindex=rindex[sst]
   if rddim eq 3 then rdata=temporary(rdata[*,*,sst]) else rdata=temporary(rdata[*,*,*,sst])
endif

if get_logenv('check_timerange') ne '' then stop,'rindex,rdata'

if get_logenv('iris_raster_flare') ne '' then begin 
   box_message,'using flare defaults'
   fe_include=1
   if n_elements(confact) eq 0 then confact=.8 ; flare default
endif

if n_elements(confact) eq 0 then confact=.6 ; todo - autofy this based on 'rdata' size
 
case rddim of 
   3: begin 
         best=rdata
         ssb=lindgen(n_elements(rindex))
         bestpos=0
   endcase
   4: begin
      best=iris_l2_best_rastpos(rindex,rdata,bestpos=bestpos,/return)
      nimg=data_chk(best,/nim)
      ssb=indgen(nimg)*rindex[0].nexp+bestpos
   endcase
   else: begin 
      box_message,'Expect 3 or 4 dimensions..
      delvarx,waves ; input only
      return
   endcase
endcase
iris_l2_multi_rast_rebin, rindex[ssb],best,rii,rdd,confact=confact,/label, size=.8, _extra=_extra
rdd=align_label_cube(anytim(rii.date_obs,/ecs,/trunc),rdd,size=.8)

; sji merge
read_iris_l2,sji[0],sjii,sjid,timerange=timerange ; ahellow user select of sji# 
sss=tim2dset(anytim(sjii.date_obs,/int),anytim(rii.date_obs,/int))
sjid=temporary(sjid[*,*,sss])
sjii=sjii[sss]

if dustbust then sjid=iris_dustbuster(sjii,sjid,_extra=_extra)
case 1 of
   intscale: ssjid=iris_intscale(sjid,sjii,_extra=_extra) ; Paul Boerner's scaling function
   else: ssjid=ssw_sigscale(sjii,sjid,/log) ; historical default
endcase
if confact eq 1 then sjidd=ssjid else sjidd=confac(ssjid,confact)
sjiname=strtrim(gt_tagval(sjii[0],/tdesc1,missing='SJI'),2) + '_Raster_Position_' + strtrim(bestpos,2)
sjidd=align_label_cube(replicate(sjiname,data_chk(sjidd,/nim)),sjidd,size=.8,/uc)
sjidd=align_label_cube(anytim(sjii.date_obs,/ecs,/trunc),sjidd,size=.8,/berase)

rastsji=[rdd,sjidd]

new_paradigm=1-keyword_set(old_paradigm) ; new is the new new paradigm

if new_paradigm then begin 
   savesys,/aplot
   wdef,zz,data_chk(rastsji,/nx),300,/zbuffer
   time_window,rii.date_obs,t0x,t1x,minute=5
   !p.multi=[0,1,2]
   plot_goes,t0x,t1x,xmargin=[4,.2],/xstyle,timerange=[t0x,t1x],charsize=.6, gcolor=230, title='!3GOES 15 X-Rays',/ascii
   if keyword_set(goes_label) then begin 
      if data_chk(goes_lable,/struct) then sswfl=goes_label else $
         sswfl=ssw_her_query(ssw_her_make_query(t0x,t1x,/fl,search=['FRM_NAME=SSW Latest Events']))
      if data_chk(sswfl,/struct) then begin 
         value=goes_class2value(sswfl.fl.fl_goescls)
         evt_grid,sswfl.fl.event_peaktime, label='!6'+sswfl.fl.fl_goescls,/data,color=200,/DATA,thick=1.5
      endif else box_message,'no SSW flares for this time range
   endif
   exptime=sjii.exptime
   if keyword_set(no_normalize) then exptime[*]=1
   sjidn=(total(total(sjid,2),1))/exptime
if get_logenv('check_expnorm') ne '' then stop,'exptime,sjidn'
   utplot,sjii.date_obs,array_despike(sjidn),xmargin=[4,.2],/xstyle,charsize=.6 , timerange=[t0x,t1x], $
      title='!3IRIS SJI Intensity - Total DN/Image', $
      ytitle='Total DN/Image', yticklen=.01, /ynozero,/nolabel,ymargin=[0,2],psym=2,symsize=.5
   event_movie3,anytim(sjii.date_obs,out='ecs'),goessji
   goessji=255-goessji
   restsys,/aplot
   irisgoes=[[goessji],[rastsji]]
endif else begin
   event_movie2,anytim(rii.date_obs,/ecs),outgoes,outsize=[data_chk(rastsji,/nx),120],/reverse,twindow=10,gcolor=225 ; optional GOES
   irisgoes=[[outgoes],[rastsji]]
endelse

;read_iris_l2,sji[0],sjii,sjid ; ahellow user select of sji# 


if do_movie then begin 
   mk_dir,movie_dir
   prefiles=file_search(movie_dir,'')
   if n_elements(movie_name) eq 0 then movie_name= $
      'iris_rast_' + strcompress(str_replace(rii[0].wavenames,',','_'),/remov) + $
                  sjiname + '_' + time2file(rii[0].date_obs)
   special_movie2,rii.date_obs,irisgoes,thumbsize=300,/nogif,/flanis, $
      movie_dir=movie_dir, movie_name=movie_name, /enhance, /no_film, /notfilm, html=html, table=3

   if not keyword_set(no_index) then begin 
      index=concat_dir(movie_dir,'index.html')
      if file_exist(index) then begin 
         box_message,'index.html exists, using <raster_index.html>'
         index=str_replace(index,'index','raster_index')
       endif

      html_doc,index,/header
      file_append,index,html
      html_doc,index,/trailer
   endif

endif
set_plot,ptemp
delvarx,waves ; Input Only
if n_params() gt 1 then index=temporary(rii)
if n_params() gt 2 then data=temporary(irisgoes)
end
