function iris_jsoc_time2fsn, time0, time1, ds=ds, add_tags=add_tags, obsid=obsid, _extra=_extra, minute_window=minute_window, silent=silent
;
;+
;   Name: iris_time2fsn
;
;   Purpose: user input time, time range or sequence structures ( iris_time2seq/iris_time2timeline ) 
;
;   Input Parameters:
;      time0 - time, start of time range or "sequence structures"
;      time1 - optional end of time range (time0 -> time1 )
;
;   Keyword Parameters:
;      ds - optional data series - default = iris.lev0
;      obsid - optional OBSID filter -> ssw_jsoc_time2data (implicit if sequence structures are input)
;      minute_window - optional scalar or two element vector [-/+] to expand search times 
;      _extra - keywords -> ssw_jsoc_time2data
;
;   History:
;     25-jul-2013 - S.L.Freeland
;
;  Restrictions:
;     only sequence structures input for
;-

silent=keyword_set(silent)
loud=1-silent

if n_elements(ds) eq 0 then ds='iris.lev0'    ; default is IRIS level 0

nt=n_elements(time0)
first_fsns=lonarr(nt)-1
last_fsns=lonarr(nt)-1
seq_starts=strarr(nt)
seq_ends=strarr(nt)

if required_tags(time0,'obsid') then begin 
   sequences=time0
   for s=0,nt-1 do begin 
      seqs=sequences[s]
      box_message,'obsid > ' + string(seqs.obsid,format='(i10.10)')
      obsid=sequences.obsid
      seq_start=gt_tagval(seqs,/seq_start,missing=gt_tagval(seqs,/date_obs))
      seq_end=gt_tagval(seqs,/seq_end,missing=gt_tagval(seqs,/date_end))
      time_window,[seq_start,seq_end],t0,t1,minute=minute_window
      ssw_jsoc_time2data,t0,t1,drms,/jsoc2,xquery='ISQOLTID='+strtrim(seqs.obsid,2),ds=ds, key='date__obs,isqoltid,fsn,percentd',/silent ; may want more KEYs for future double check
      if get_logenv('jsoc_check') ne '' then stop,'t0,t1,ISQOLTID='+strtrim(seqs.obsid,2)
      if data_chk(drms,/struct) then begin 
         first_fsns[s]=drms[0].fsn
         last_fsns[s]=last_nelem(drms.fsn)
         seq_starts[s]=drms[0].date__obs
         seq_ends[s]=last_nelem(drms.date__obs)
      endif else if loud then box_message,'No JSOC ' + ds + ' for this time range...'
   endfor
  retval=sequences
  if not required_tags(sequences,'first_fsn,last_fsn') then begin 
     retval=add_tag(retval,first_fsns,'first_fsn')
     retval=add_tag(retval,last_fsns,'last_fsn')
     retval=add_tag(retval,last_fsns-first_fsns+1,'nfsns')
     retval.date_obs=seq_starts
     retval.date_end=seq_ends
  endif else begin
     retval.first_fsn=first_fsns
     retval.last_fsn=last_fsns
     retval.nfsns=last_fsns-first_fsns+1
     retval.seq_start=seq_starts
     retval.seq_end=seq_ends
  endelse   
  nret=n_elements(retval)
  ss=where(retval.first_fsn ne -1,rcnt)
  case 1 of 
     rcnt eq nret: ; fsn for all input
     rcnt eq 0: box_message,'No jsoc data for any input sequences
     else: begin 
        retval=retval[ss]
        box_message,'No jsoc data for ' + strtrim(nret-rcnt,2) + ' of your input sequences; filtering those out'
     endcase
  endcase

endif else box_message,'Only works on timeline or sequence structures'

return,retval
end
