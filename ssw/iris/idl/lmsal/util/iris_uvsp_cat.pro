pro iris_uvsp_cat,index, data, wavevect, $
   get=get, outdir=outdir, no_delete=no_delete, $
   ca2=ca2, o2=o2
;
;+
;   Name: iris_uvsp_cat
;
;   Purpose: get meta data and optionally data from Scott McIntosh Mg II UVSP catalog
;
;   Input Parameters:
;      index - optional input time or metadata for desired data
;
;   Output Parameters:
;      index - if undefined on input, or /GET set, returns catalog meta data structures
;      data - if 'index' defined (structure or time) , returns uvsp data matrix
;      wavevect - wavelength vector coorresponding to data 
;
;   Keyword Parameters:
;      get - if set, force 'index' get even if defined on input
;      outdir - if 'data' set, output directory for file transfers
;      no_delete - don't remove files after restore->data
;      ca2/o2 - opionally, Ca II or O II (not yet available; only Mg II for today)
;      
; 
;   Calling examples:
;      IDL> iris_uvsp_cat,index [,/get] - returns catalog/meta data for all Scott's web entries
;      IDL> iris_uvsp_cat,indexi, data [,wavevect ]- return data matrix for input 'indexi' (scalar time or meta)
;
;      1. just get the catalog assciated with Scott's web page -> structure vector
;      IDL> iris_uvsp_cat,index,/get
;      IDL> help,index & help,index,/str
;      INDEX           STRUCT    = -> <Anonymous> Array[73] <<< 1 per Scott's www cat page
;       ** Structure <20e2004>, 25 tags, length=140, data length=136, refs=1:
;   	DATE_OBS        STRING    '11-MAR-1980 01:35:00'
;   	DURATION        LONG                10
;   	TARGET          STRING    'NOAA AR2316'
;   	(... etc ..)
; 	TYPE            INT             10
;   	DATAURL         STRING    'http://download.hao.ucar.edu/pub/mscott/UV'... (few added tags)
;   	WAVEMIN         FLOAT           2801.00
;   	WAVEMAX         FLOAT           2802.50
;
;      2. for desired index(i), get the spectra (transfer->unzip->restore->DATA output param)
;      IDL> indexi=index(10) ; desired item index (or time string from scott's web page)
;      IDL> iris_uvsp_cat,indexi,data,wavevect & help,indexi,data,wavevect
;      INDEXI          STRUCT    = -> <Anonymous> Array[1]
;      DATA            FLOAT     = Array[1, 1, 750]
;      WAVEVECT        FLOAT     = Array[750]
;      
;      then, for example...
;      IDL> plot,wavevect,reform(data),xtit='Wavelength' ;,...etc 
;
;      3. just transfer desired subset (or all) Scott's -> local
;      IDL> iris_uvsp_cat,index,/get ; get the catalog, includes .DATAURL
;      IDL> ss=struct_where(index,search=<youroptionalselection>)
;      IDL> sock_copy,iris_uvsp_cat(ss).dataurl [,out_dir='path' , /progress]  ; hao->local .gz file(s)
;
;   History:
;      24-mar-2012 - S.L.Freeland - ssw api to Scott McIntosh UVSP MgII catalog
;                    >>> http://download.hao.ucar.edu/pub/mscott/UVSP/UVSP_Cat.html
;-

irisd=get_logenv('$SSW_IRIS_DATA')
if irisd eq '' then irisd=concat_dir('$SSW_IRIS','data')

uvspcat=concat_dir(irisd,'uvs_smcat.geny')

if not file_exist(uvspcat) then begin 
   box_message,['Looks like IRIS not up to date at your site...',$
                'IDL> ssw_upgrade,/iris,/spawn,/loud,/passive_ftp']
   return ; !!! Early Exit
endif

get=keyword_set(get)
no_delete=keyword_set(no_delete)
ca2=keyword_set(ca2)
o2=keyword_set(o2)

if ca2 or o2 then begin
   box_message,'Only Mg II available for now..., returning' 
   return ; !!! Early Exit
endif


if n_elements(dparent) eq 0 then dparent='http://download.hao.ucar.edu/pub/mscott/UVSP/savefiles/'
case 1 of 
   n_elements(index) eq 0 or get: restgenx,file=uvspcat,index
   n_elements(index) eq 1 and n_params() ge 2: begin
      if data_chk(index,/string,/scalar) then begin 
         restgenx,file=uvspcat,indexall ; replaces input "index" (time) with meta data
         index=indexall(tim2dset(anytim(indexall.date_obs,/int),index))
      endif
      exp=gt_tagval(index,/experiment,missing= -1)
      if exp eq -1 then box_message,'Missing Experiment#' else begin 
         sname='UVSP-v'+ strtrim(exp,2)+'.sav.gz'
         durl=concat_dir(dparent,sname)
	 delvarx,data
         if n_elements(outdir) eq 0 then outdir=get_temp_dir()
         lname=concat_dir(outdir,sname)
         sock_copy,durl,progress=!d.name eq 'X', err=err, out_dir=outdir
         if not file_exist(lname) then box_message,'problem with transfer>> '+durl else begin 
            spawn,['gunzip',lname],/noshell
            lname=str_replace(lname,'.gz','')
            if file_exist(lname) then restore,lname else $ 
               box_message,'Problem with gunzip/decompress...'
            if not no_delete then ssw_file_delete,lname
         endelse
      endelse
   endcase
   else: box_message,'Not sure what you want me to do yet..., use /GET or supply INDEX,
endcase

; value added 'index' tags.
if data_chk(index,/struct) and gt_tagval(index(0),/dataurl,missing='') eq '' then begin
    exp=gt_tagval(index,/experiment,missing= -1)
    dataurl=concat_dir(dparent,'UVSP-v'+ strtrim(exp,2)+'.sav.gz')
    index=add_tag(index,dataurl,'dataurl')
    index=add_tag(index,float(index.wavelength),'WAVEMIN')
    index=add_tag(index,index.wavelength + (index.dlambda * index.nspec),'WAVEMAX')

endif

if n_params() gt 2 and n_elements(data) gt 0 then $
   wavevect=findgen(index(0).nspec>1)*index(0).dlambda + index(0).wavemin

return
end


