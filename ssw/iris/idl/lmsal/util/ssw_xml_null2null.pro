function ssw_xml_null2null, xmlin, reverse_the_polarity=reverse_the_polarity, loud=loud
;
;   Name: ssw_xml_null2null
;
;   Purpose: change null xml formats  <thing></thing> -> <thing/> (or the reverse via /reverse )
;
;

loud=keyword_set(loud)

retval=xmlin
case 1 of 
   keyword_set(reverse_the_polarity) : begin 
      box_message,'Not yet implented'
   endcase
   else: begin 
      ssn=where(strpos(retval,'><') ne -1, ncnt)
      if loud then box_message,'Number of NULLS = ' + strtrim(ncnt,2)
      if ncnt gt 0 then begin 
         thing=ssw_strsplit(retval[ssn],'><',/head)
         retval[ssn]=thing+'/>'
      endif
   endcase
endcase


return,retval
end
      

