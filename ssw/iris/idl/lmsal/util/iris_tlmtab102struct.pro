function iris_tlmtab102struct, t10, no_transpose=no_transpose, missing=missing
;
;
;+
;   Name: iris_tlmtab102struct
;
;   Purpose: convert IRIS "table10" .csv -> structure
;
;   History:
;      26-aug-2013 - S.L.Freeland
;      27-aug-2013 - S.L.Freeland - restore previous defaults
;   
;   Calling Sequnce:
;      IDL> struct10=iris_tlmtab102struct(<table10>)
;      (usually called via iris_tlmtab2struct.pro wrapper
;-

retval=iris_tlmtab2struct_1(t10) ; 
t10dat=rd_tfile(t10)
cols=str2cols(t10dat,',',/unal)
rastin=tag_index(retval,'raster_fov')
sjiin=tag_index(retval,'sji_fov')
rast_fov=reform(cols(rastin,1:*))
sji_fov=reform(cols(sjiin,1:*)) ; some assmuption of stability (probably a bad idea, based on historical data)

rast_fov=strtrim(ssw_strsplit(rast_fov+'t:','t:',/head),2)
rast_fov=rast_fov+'  -1 x0  -1 x0'
rast_fov='  ' + strtrim(str_replace(rast_fov,'  ',',  '),2)
rast_fov=str_replace(rast_fov,'x',' x')
rast_fov=str_replace(rast_fov,' x','fovy')
rast_fov=str_replace(rast_fov,'  ','fovx')
fovx=fltarr(3,n_elements(rast_fov))
fovy=fovx

retval=add_tag(retval,fovx,'raster_fovx',index='raster_fov')
retval=add_tag(retval,fovy,'raster_fovy',index='raster_fovx')
retval=rem_tag(retval,'raster_fov')

for r=0,n_elements(rast_fov)-1 do begin
   arr=str2arr(rast_fov[r])
   arr=arr[0:2]
   retval.raster_fovx[0:2,r]=float(strextract(arr,'fovx','fovy'))
   retval.raster_fovy[0:2,r]=float(ssw_strsplit(arr,'fovy',/tail))
endfor

sji_fov=strcompress(sji_fov,/remove_all)
sji_fovx=float(ssw_strsplit(sji_fov,'x',/head,tail=sji_fovy))
retval=add_tag(retval,sji_fovx,'sji_fovx',index='sji_fov')
retval=add_tag(retval,float(sji_fovy),'sji_fovy',index='sji_fovx')
retval=rem_tag(retval,'sji_fov')

rt=str2arr('raster_fovx,raster_fovy') ; restore previous defaults
for r=0,1 do begin 
   tind=tag_index(retval,rt[r])
   retval=rep_tag_value(retval,transpose(retval.(tind)),rt[r])
   ssn=where(retval.(tind) eq -1,ncnt)
   if ncnt gt 0 then retval.(tind)[ssn]=0 ;
endfor
   
return,retval
end



