FUNCTION IRIS_wedge2solar_tl, M1, M2,xoff=xoff,yoff=yoff,roll_angle=roll_angle
;+
; Name    : iris_wedge2solar_tl
;
; Purpose : return solar X,Y coordinates for IRIS Fine Sun Pointing
;           using wedge motors; version used in IRIS timeline program
;
; Use     : IDL> xy = iris_wedge2solar_tl(m1,m2)
;
; Input   :
;    M1, M2 = Guide Telescope wedge motor positions (integers, may be arrays)
;
; Output  :
;    xy = [X, Y], solar position [EW,NS] in arcsec, where W & N are +
;    M1 & M2 may be N-element arrays; if so, returns an [N,2] array
;
; Keyword Inputs: 
;    xoff = offset in X of Science Telescope from Guide Telescope [103]
;    yoff = offset in Y of Science Telescope from Guide Telescope [-82]
;    roll_angle = IRIS roll angle, rotation of S/C about +Z axis using right-hand rule, degrees
;         roll_angle small & positive moves a pointing on the N central meridian (0,+y) 
;         into the NW quadrant of the Sun (+x',+y')
;
; History
;     TD Tarbell, LMSAL   Version 1.0  30-Nov-2012
;     TDT  Version 2.0    01-Jul-2013  sign change of X & Y, discovered by
;                         J-P, agreed by TDT, verified in flight 30 June
;     TDT  Version 3.0    28-Aug-2013  add default offsets
;     TDT  Version 3.1    20-Jun-2014  corrected offsets, cleaned up 
;-

if n_elements(m1) ne n_elements(m2) then begin
   print,'M1 & M2 must have same number of elements, returning -9999'
   return,[-9999]
end

; These parameters are from Dave Akin's theodolite calibration of wedges
; offset2 = step position of m2 that produces no image shift when m1=0
; m1v = step position of M1 that produces offset only in Dave's
;       +V direction = IRIS +/-X direction = solar NS
; rmax1,2 = radius of offset circle produced by each wedge, in arcsec
RMAX1 =  634.8
RMAX2 =  628.5
offset2 = 113.57
m1v = 217.875

; These offsets are from 4-limb coalignments in Nov 2013 & 2014 and
;    are the current values in timeline
; Set these keywords to 0.001 if you want to use 0.
if not keyword_set(xoff) then xoff = 103.
if not keyword_set(yoff) then yoff = -82.

; 240 steps = 1 revolution of wedge motor
fac = 2.*!pi/240.
TH1 =  fac*(float(M1)-m1v)
TH2 =  fac*(float(M2)-m1v+120.-offset2)

Y = +RMAX1*cos(TH1) + rmax2*cos(TH2)
X = -RMAX1*sin(TH1) - rmax2*sin(TH2)

x = x + xoff
y = y + yoff

if (keyword_set(roll_angle)) then begin
   th = 2.*!pi*roll_angle/360.
   sth = sin(th)
   cth = cos(th)
   x0 = x
   y0 = y
   x =  x0*cth + y0*sth
   y = -x0*sth + y0*cth
end

RETURN,[[x],[y]]
END






