pro iris_raster_movies,obsid,index,mdata,timerange=timerange,debug=debug, outdir=outdir, online=online, $
   low_wave=low_wave, high_wave=high_wave, waves=waves, plot_lambda=plot_lambda, label_movie_label_movie, $
   write_video=write_video, avi=avi
;
;   Name: iris_raster_movies
;
;   Purpose: Create movies from Level 2 IRIS data (atm limited to Mg
;   II h & K)
;
;   Input Parameters:
;      obsid - IRIS Level2 fully qualified OBSID
;
;   Output Parameters:
;      index,mdata - metadata,moviedata
;
;      Creates a quicktime movie
;
;   Output Parameters:
;      TBD
;
;   Keyword Parameters:
;      outdir - directory for movie output ; default=curdir() 
;      online (switch) - place it in $IRIS_DATA/level2[_nrt] - user data_ops ONLY
;      timerange - optional time range, assumed sub-range of implied OBSID time range - timerange=[t0,t1]
;      write_video (switch) - use IDL intrinsic write_video suite in place of ffmpeg spwan (faster/better/cheaper?)
;      plot_lambda (switch) - include wave/lambda bar@top
;      label_movie (switch) - add label on frames@bottom (original Mark Cheung style)
;
;   Method:
;      Yes, there is one
;
;   Calling Sequence:
;      IDL> iris_raster_movie,OBSID[,t0x,t1x][,outdir=outdir][,/ONLINE][,/WRITE_VIDEO]
;
;   Notes:
;      if default/ffmpeg genrated mp4 is Not playbable via Safari, try adding /WRITE_VIDEO (assumes IDL >= 8.3) 
;
;   History:
;      08-aug-2015 - Mark Cheung
;       5-may-2016 - S.L.Freeland - made this a procedure, generalize,  SSW-ify the api, etc...
;                    use distributed ffmpeg (ssw_ffmpeg.pro/image2movie2)
;                    TODO - generalize WAVEs
;      16-may-2016 - S.L.Freeland - add /WRITE_VIDEO option, replace bunch of SPAWNs w/ssw/idl analogs
;      21-jun-2016 - S.L.Freeland - realize /ONLINE option (hcr still todo)
;      27-jun-2016 - S.L.Freeland - fix /write_video feature (e.g. make it work)
;       6-jul-2016 - S.L.Freeland - add voe/xml/hcr update for /ONLINE
;-
debug=keyword_set(debug)
plot_lambda=keyword_set(plot_lambda)
label_movie=keyword_set(label_movie)

hcr=iris_obs2hcr(obsid,count=count)
if count ne 1 then begin 
   box_message,'Need One, and Only One fully qualified IRIS OBSID input, ... bailing'
   return ; !!! Early exit on bad OBSID input
endif
if debug then stop,'hcr'
if n_elements(timerange) eq 2 then begin ; user wants subset of time within obs ; TODO - Verify TIMERANGE within OBS timerange
   t0=t0x
   t1=t1x
endif else begin 
   time_window,[hcr.starttime,hcr.stoptime],t0,t1,minute=[-1,1]
endelse


; Find OBS in IRIS timelines
if get_logenv('check_t0t1') ne '' then stop,'t0,t1'
obs = iris_time2timeline(t0, t1)
if n_elements(outdir) eq 0 then outdir=curdir()
uncomp_path=concat_dir(outdir,'decompressed')

for o=0,n_elements(obs)-1 do begin

   print, obs[o].description
 
   ; Find IRIS raster files.
   ; First check level2_compressed
   ; Then try level2
   ; Then try level2_nrt
   rasfiles=iris_time2files(obs[o].date_obs,obs[o].date_end,level=2,/raster) ; ,/compressed)
   box_message,'Bypassing decompression logic'
   rasfiles=''
   temp_path=curdir() 
   IF rasfiles[0] NE '' THEN BEGIN
      files=file_search(uncomp_path,'')
      mk_dir,uncomp_path
      thisdir = curdir()
      cd,uncomp_path 
      spawn, ['tar','-zxvf',rasfiles[0]],/noshell
      cd, thisdir
      rasfiles=file_list(uncomp_path,'*rast*fits')
      deletedecompressed = 1
   ENDIF ELSE BEGIN
      ; Couldn't find level2_compressed, true level2
      rasfiles=iris_time2files(obs[o].date_obs,obs[o].date_end,level=2,/raster)
      IF (rasfiles[0] EQ '') THEN BEGIN
      ; Couldn't find level2 data, try level2_nrt
         rasfiles=iris_time2files(obs[o].date_obs,obs[o].date_end,level=2,/raster,/nrt)
      ENDIF
   ENDELSE
   ; If found no files just skip this obs
   if rasfiles[0] EQ '' THEN GOTO, SKIP

   ; Find line id
   line = 'Mg II k 2796'
   d = iris_obj(rasfiles[0])
   d->show_lines
   match = -1
   for w=0,d->getnwin()-1 do if d->getline_id(w) EQ line then match = w
   if match NE -1 then read_iris_l2, rasfiles[0], index, data, wave=line
   if match EQ -1 then GOTO, SKIP

   ; Define wavelength axis
   lambda = (findgen(index[0].naxis1)+index[0].crpix1)*index[0].cdelt1+index[0].crval1
   wposminmax = minmax(where(abs(lambda-2796.5) LT 1.5))
   wposminmax[1] = min(where(lambda GE 2798.8))
   loffset = wposminmax[0]
   
   ;Transpose so that we have [x, y, lambda]
   tdata = transpose(data, [2,1,0])
   missing = where(FINITE(tdata) EQ 0)
   if missing[0] NE -1 then tdata[missing] = 0.0
   
   tdatadim = size(tdata,/dim)
   ; Normalize by exposure time
   if lambda[0] LT 2000. THEN for l=0,tdatadim[0]-1 do tdata[l,*,*] /= index[l].exptimef
   if lambda[0] GT 2000. THEN for l=0,tdatadim[0]-1 do tdata[l,*,*] /= index[l].exptimen

   ;Congrid data so that dx and dy are somewhat commensurate
   xstretch = round(index[0].cdelt3/index[0].cdelt2)
   IF xstretch LT 1.0 then xstretch = 1.0
   sdata = rebin(tdata,index[0].naxis3*xstretch,index[0].naxis2,index[0].naxis1,/sample)
   sdatadim = size(sdata,/dim)

   if debug then stop,'sdata,tdata
   
   IF ((n_elements(rasfiles) GT 1) AND (sdatadim[0] LT 1024))  then begin
      IF (sdatadim[0]*n_elements(rasfiles) GE 1440) THEN BEGIN
         movie = 1
         ; Choose wavelength positions
         MgInd = where(abs(lambda - 2795.0) EQ min(abs(lambda - 2795.0)))
         MgInd = [MgInd,where(abs(lambda - 2796.3) EQ min(abs(lambda - 2796.3)))]
         MgInd = [MgInd,where(abs(lambda - 2796.5) EQ min(abs(lambda - 2796.5)))]
         MgInd = [MgInd,where(abs(lambda - 2798.8) EQ min(abs(lambda - 2798.8)))]
         
         mdata = fltarr(sdatadim[0]*4,sdatadim[1],n_elements(rasfiles))
         scalingfacs = mean(sdata[*,*,MgInd[0]]>0)/ mean(sdata[*,*,MgInd[2]]>0)
         scalingfacs = [scalingfacs,mean(sdata[*,*,MgInd[1]]>0)/ mean(sdata[*,*,MgInd[2]]>0)]
         scalingfacs = [scalingfacs,mean(sdata[*,*,MgInd[2]]>0)/ mean(sdata[*,*,MgInd[2]]>0)]
         scalingfacs = [scalingfacs,mean(sdata[*,*,MgInd[3]]>0)/ mean(sdata[*,*,MgInd[2]]>0)]
         scalingfacs = 1./scalingfacs
         
         mdata[0:sdatadim[0]*4-1,0:sdatadim[1]-1,0] = [sdata[*,*,MgInd[0]]*scalingfacs[0],$
                                                       sdata[*,*,MgInd[1]]*scalingfacs[1],$
                                                       sdata[*,*,MgInd[2]]*scalingfacs[2],$
                                                       sdata[*,*,MgInd[3]]*scalingfacs[3]]
         for n=0,n_elements(rasfiles)-1 do begin
            read_iris_l2, rasfiles[n], index, data, wave=line
                                ;Transpose so that we have [x, y, lambda]
            tdata = transpose(data, [2,1,0])
            tdatadim = size(tdata,/dim)

            missing = where(FINITE(tdata) EQ 0)
            if missing[0] NE -1 then tdata[missing] = 0.0
                                ;Normalize by exposure time
            if lambda[0] LT 2000. THEN for l=0,tdatadim[0]-1 do tdata[l,*,*] /= index[l].exptimef
            if lambda[0] GT 2000. THEN for l=0,tdatadim[0]-1 do tdata[l,*,*] /= index[l].exptimen
            sdata = rebin(tdata,index[0].naxis3*xstretch,index[0].naxis2,index[0].naxis1,/sample)
            mdata[0:sdatadim[0]*4-1,0:sdatadim[1]-1,n] = [sdata[*,*,MgInd[0]]*scalingfacs[0],$
                                                          sdata[*,*,MgInd[1]]*scalingfacs[1],$
                                                          sdata[*,*,MgInd[2]]*scalingfacs[2],$
                                                          sdata[*,*,MgInd[3]]*scalingfacs[3]]
         endfor
         spectrum = total(total(sdata > 0,1),1)/float(sdatadim[0]*sdatadim[1])
         wposminmax=[0,n_elements(rasfiles)-1]     
      ENDIF ELSE BEGIN

         movie=0
         mdata = fltarr(sdatadim[0]*n_elements(rasfiles),sdatadim[1],sdatadim[2])
         mdata[0:sdatadim[0]-1,*,*] = sdata
         for n=0,n_elements(rasfiles)-1 do begin
            read_iris_l2, rasfiles[n], index, data, wave=line
            tdata = transpose(data, [2,1,0])
            tdatadim = size(tdata,/dim)
            
            missing = where(FINITE(tdata) EQ 0)
            if missing[0] NE -1 then tdata[missing] = 0.0
            
                                ;Normalize by exposure time
            if lambda[0] LT 2000. THEN for l=0,tdatadim[0]-1 do tdata[l,*,*] /= index[l].exptimef
            if lambda[0] GT 2000. THEN for l=0,tdatadim[0]-1 do tdata[l,*,*] /= index[l].exptimen
            sdata = rebin(tdata,index[0].naxis3*xstretch,index[0].naxis2,index[0].naxis1,/sample)
            ;mdata[n*sdatadim[0]:(n+1)*sdatadim[0]-1,*,*] = sdata > 0
            mdata[n*sdatadim[0]:n*sdatadim[0]+(size(sdata,/dim))[0]-1,*,*] = sdata > 0

         endfor
         
      ENDELSE
   ENDIF ELSE BEGIN
      mdata = sdata
      movie = 0
   ENDELSE
   mdatadim = size(mdata,/dim)

   ; Compensate for CCD summing
   IF (lambda[0] LT 2000.) THEN mdata /= float(index[0].sumsptrf*index[0].sumspat)
   IF (lambda[0] GT 2000.) THEN mdata /= float(index[0].sumsptrn*index[0].sumspat)
   
   ; Create directory for holding animation files
   online=keyword_set(online)
   if online then begin 
      if get_user() ne 'data_ops' then box_message,'/ONLINE request, user Not data_ops' else begin 
         box_message,'iris_raster_movies Online requested'
         hcr=iris_obs2hcr(obsid)
         opath=strtrim(ssw_strsplit(hcr.umodes,'iris_l2',/head),2)
         wwwpath=concat_dir(opath,'www')
         newpath=concat_dir(opath,'www/irm_raster')
      endelse
   endif
   if n_elements(newpath) eq 0 then newpath = 'irm_'+obsid 
   box_message,'Writing to >> ' + newpath
   mk_dir,newpath
   
; Some graphics preliminaries
; ---- original M.Cheung block ---
set_plot, 'z'
device, decompose=0
loadct,0, /silent
plot, findgen(100)
img = tvrd(/true)
loadct, 3, /silent
tvlct, r, g, b, /get
loadct, 0, /silent
; -----------------
   ; Create image frames
   set_plot, 'Z'
   height = mdatadim[1]
   if height MOD 2 NE 0 then height = height + 1
   dim = size(mdata,/dim)
   wheight = height
   wwidth  = mdatadim[0]
   magfac  = 1
   if (wheight LT 190) AND (wwidth LT 190) THEN magfac = 4
   device, set_resolution=[mdatadim[0]*magfac,height*magfac], set_pixel_depth=24
   
   charsize = 2*float(min([mdatadim[0],height])*magfac)/500.
   charsize = min([charsize,2.15])
   charsize = max([charsize,1.4])

   IF (NOT keyword_set(movie)) THEN spectrum = total(total(mdata>0,1),1)/n_elements(mdata[*,*,0])
   ; Make low intensity obs brighter in movie
   globalscaling = mean(mdata[*,*,wposminmax[0]:wposminmax[1]]>0)

   skip = 1
   for wpos=wposminmax[0],wposminmax[1],skip do begin
      ;loadct, 0, /silent
 
      globalscaling = mean(mdata[*,*,wpos]>0)

      tv, bytscl(sqrt(congrid(mdata[*,*,wpos],magfac*dim[0],magfac*dim[1])>0.0),min=0.4*sqrt(globalscaling/44.),max=12.0*sqrt(globalscaling/44.))
      if plot_lambda then begin 
      plot, lambda, spectrum, /xstyle, /ystyle, /noerase, /ylog, thick=6, $
            xticklen=0.05, yticklen=0.05, xrange=[lambda[loffset],lambda[sdatadim[2]-1]], yrange=[0.1,max([spectrum,1])], $
            color=255, /device, charthick=2, $
            position=[0,0.8*magfac*dim[1]-1,magfac*dim[0]-1,magfac*dim[1]-10], charsize=charsize
      
      ;loadct, 0, /silent
      plot, lambda, spectrum, /xstyle, /ystyle, /noerase, /ylog, thick=3, $
            xticklen=0.05, yticklen=0.05, xrange=[lambda[loffset],lambda[sdatadim[2]-1]], yrange=[0.1,max([spectrum,1])], $
            color=0, charthick=1, /device, $
            position=[0,0.8*magfac*dim[1]-1,magfac*dim[0]-1,magfac*dim[1]-10], charsize=charsize
      endif
      
      IF KEYWORD_SET(movie) THEN BEGIN
         oplot, lambda[replicate(MgInd[0],2)], [1e-5,1e5], thick=5, color=0
         oplot, lambda[replicate(MgInd[1],2)], [1e-5,1e5], thick=5, color=0
         oplot, lambda[replicate(MgInd[2],2)], [1e-5,1e5], thick=5, color=0
         oplot, lambda[replicate(MgInd[3],2)], [1e-5,1e5], thick=5, color=0
         
         oplot, lambda[replicate(MgInd[0],2)], [1e-5,1e5], thick=2
         oplot, lambda[replicate(MgInd[1],2)], [1e-5,1e5], thick=2
         oplot, lambda[replicate(MgInd[2],2)], [1e-5,1e5], thick=2
         oplot, lambda[replicate(MgInd[3],2)], [1e-5,1e5], thick=2
      ENDIF ELSE BEGIN
         oplot, [lambda[wpos],lambda[wpos]], [1e-5,1e5], thick=4, line=2, color=0
         oplot, [lambda[wpos],lambda[wpos]], [1e-5,1e5], thick=2, line=2, color=129
      ENDELSE

      !P.font=1
      ;loadct, 0, /silent
      if label_movie then begin
         IF KEYWORD_SET(movie) THEN BEGIN
            xyouts, 5, 4, 'Date_obs:'+strmid(obs[o].date_obs,0,16),charthick=1, /device, charsize=charsize
         ENDIF ELSE BEGIN
            xyouts, 5, 4, 'Date_obs:'+strmid(obs[o].date_obs,0,16),charthick=1, /device, charsize=charsize
         ENDELSE
         xyouts, 5, 21, 'OBS ID:'+string(obs[o].obsid,format='(I10)'),charthick=1, /device, charsize=charsize
         xyouts, 5, 38, 'Duration:'+string(obs[o].duration,format='(F7.1)') + ' s',charthick=1, /device, charsize=charsize
         xyouts, 5, 55, obs[o].description,charthick=1, /device, charsize=charsize
      endif
      
      tv, [0,255]
      img = tvrd(/true)
      img[0,*,*] = r[img[0,*,*]]
      img[1,*,*] = g[img[1,*,*]]
      img[2,*,*] = b[img[2,*,*]]
      write_png, newpath+'/frame_'+string((wpos-wposminmax[0])/skip,format='(I04)')+'.png', img
      
   endfor

   ; Encode quicktime h264 movie
   pngfiles=file_list(newpath,'frame*png')
   ffmpeg=ssw_bin_path('ffmpeg',/ontology,found=found)
   linestr = strjoin(strsplit(line, ' ',/extract))
   if found then begin 
      cd, newpath
      movie_name='MC_raster_'+obsid+ '_'+linestr+(['.mp4','.avi'])(keyword_set(avi))
      movie_string=ffmpeg + " -y -i 'frame_%04d.png' -c:v libx264 -crf 18 -pix_fmt yuv420p -r 8 " + movie_name
      if debug then stop,'movie_string'
      ;spawn, "ffmpeg -y -i 'frame_%04d.png' -c:v libx264 -crf 18 -pix_fmt yuv420p -r 8 Raster_"$
      ;    +strmid(obs[o].date_obs,0,16)+"_OBS"+string(obs[o].obsid,format='(I10)')+'_'+linestr+'.mov'
;       spawn,movie_string
      gfiles=findfile('frame*png')
      if keyword_set(write_video) then begin 
          f0=read_image(gfiles[0])
          video_dims=[data_chk(f0,/ny),data_chk(f0,/nim)]
          write_video,movie_name,handle=handle,video_dimensions=video_dims,video_fps=24
          for f=0,n_elements(gfiles)-1 do begin 
             frame=read_image(gfiles[f])
             if data_chk(frame,/ndimen) eq 2 then frame=mk_24bit(frame,r,g,b) ; 24b
             write_video,movie_name,handle=handle,frame
          endfor
          write_video,movie_name,handle=handle,frame
      endif else ssw_ffmpeg,gfiles,movie_dir=curdir(),movie_name=movie_name,/mp4
      if online then begin 
         voexml=(concat_dir(wwwpath,ssw_strsplit(hcr.eventid,'VOEvent#',/tail)))(0)
         if file_exist(voexml) then begin 
            movie_path=concat_dir(newpath,movie_name)
            if file_exist(movie_path) then begin
               movieurl='http://www.lmsal.com/solarsoft/'+movie_path
               voedat=rd_tfile(voexml)
               rparam='            <Param name="' + $
                      'URL_MP4_'+str_replace(line,' ','_') +'" ' + $
                      'value="' + movieurl  +'" />'
               new=strarrinsert(voedat,rparam,'<Group name="Raster">')
               new=ssw_hek_touchvoe(new) ; update <Date>date</Date> to force ingest
               file_append,voexml+'_irm_backup',voedat,/new ; make backup of original voe
               file_append,voexml,new,/new
               ssw_coverage_ingest,voexml,/add_link
            endif else box_messgae,'Problem with movie generation?'
         endif else box_message,'Cannot find VOE/xml ??'
      endif
 
      ; spawn, "rm *.png"
      cd, ".."
   endif else box_message,'cannot find ffmpeg for this OS/ARCH'

   SKIP: 

   IF keyword_set(deletedecompressed) THEN file_delete, uncomp_path, /recurse
   cd,temp_path
   
ENDFOR

data=mdata

if debug then stop,'mdata,movie_name'
set_plot, 'X'
end
