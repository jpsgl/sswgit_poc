function iris_tlmtab2struct_1,tlmtab, _extra=_extra, no_delete=no_delete
;
;+ 
;   Name: iris_tlmtab2struct_1
;
;   Purpose: read irs telemtable (.csv?) -> struct (a subroutine, usually called via iris_tlmtab2struct.pro
;
;   Input Parameters:
;      tlmtab - table file name 
;
;   Output:
;      structure containg table data
;
;   Keyword Parameters:
;      no_delete - (switch) if set, and patched table, do Not remove the temporary patched version
;
;   Restrictions:
;      .csv only for now
;
;   History: 
;      5-mar-2013 - S.L.Freeland
;     19-jun-2013 - S.L.Freeland - remove patched version generated via iris_patchcsv.pro (table 10 only)
;     26-aug-2013 - S.L.F - 
;-

if not file_exist(tlmtab) then begin
   box_message,'Need table-file name'
   return,-1
endif

break_file,tlmtab,ll,pp,ff,ext

retval=-1
case ext of 
   '.csv': begin
      infile=tlmtab
      retval=read_csv(infile,header=header)
      tnames=tag_names(retval)
      if n_tags(retval) eq n_elements(header) then begin 
         header=str2cols(header,'(',/unal,/trim)
         strtab2vect,header,hnames
         hnames=str_replace(strcompress(str_replace(hnames,'+','')),' ','_')
         hnames=str_replace(hnames,'-','_')
         for t=0,n_elements(hnames)-1 do retval=rep_tag_name(retval,tnames[t],hnames[t])
      endif
   endcase
   else: begin
      box_message,'csv only for now'
   endcase
endcase

return,retval
end
