function iris_tab2depend, table, files_only=files_only, table_path=table_path, retval=retval, inc_data=inc_data, $
   check_files=check_files, debug=debug
;
;+
;   Name: iris_table2dependencies
;
;   Purpose: input table file or structure -> expanded dependency info
;
;   Input Parameters:
;      table - IRIS obs table; file or ssw_iris_xml2struct output (structure image of .xml table)
;
;   Output:
;      returns nested structure w/dpendency info
;
;   Keyword Parameters:
;      files_only - if set, only return dependent file list(s)
;      table_path - path to associated tables (required if TABLE is structure 
;      inc_data - if set, include data for all files (default=file lists)
;
;   Restrictions:
;    sure.
;
;   History:
;      14-mar-2013 - S.L.Freeland
;       1-apr-2013 - use FDBDATA.CRSID for CRS file derivation; 
;                    allow FDB entry point (previous OBS+FRM)
;-
debug=keyword_set(debug)
case 1 of
   data_chk(table,/struct) and file_exist(table_path): begin 
      tabstr=table
   endcase
   file_exist(table): begin
      tabstr=ssw_iris_xml2struct(table)
      break_file,table,ll,table_path
   endcase
   else: begin 
      box_message,['need either absoute path to TABLE, or TABLE-Structure + TABLE_PATH']
      return,-1
   endcase
endcase
inc_data=keyword_set(inc_data)
check_files=keyword_set(check_files)

data=gt_tagval(tabstr,/data,missing=-1)

if   required_tags(data,'tr,fid,cadence') then begin; OBS input
      if file_exist(table) then obsfile=table else $
         obsfile=concat_dir(table_path,'OBS-'+strtab.fid+'.xml')
      retval=add_tag(retval,obsfile,'obsfiles')
      if inc_data then retval=add_tag(retval,tabstr,'obs')
      frmlist=gt_tagval(data,/fid)
      frmfiles=concat_dir(table_path,'FRM-'+frmlist+'.xml')
      retval=add_tag(retval,frmfiles,'frmfiles')
      fok=total(file_exist(frmfiles)) eq n_elements(frmfiles)
      if inc_data and fok then retval=add_tag(retval,iris_multitab2struct(frmfiles),'frm')
endif

if  required_tags(data,'tr,sji,nuv,fuv') then begin
   fdata = data 
   retval=add_tag(retval,table,'frmfiles')
endif else begin  
   if required_tags(retval,'frmfiles') then  begin 
      fok=total(file_exist(frmfiles)) eq n_elements(frmfiles)
      if fok then fdata=iris_multitab2struct(retval.frmfiles)
   endif
endelse

if required_tags(tabstr,/crsid) then begin 
   fdblist=tabstr.header.id ; FDB entry
endif else begin 
   fdblist=[gt_tagval(fdata,/sji) + ':sji',gt_tagval(fdata,/nuv)+':nuv',gt_tagval(fdata,/fuv)+':fuv']
   fdblist=all_vals(fdblist) ; uniq subset, nulls removed
   fdblist=ssw_strsplit(fdblist,':',tail=crstype)
   fdblist=fdblist(rem_elem(fdblist,'0')) ; remove null elements
endelse
fdbfiles=concat_dir(table_path,'FDB-'+fdblist+'.xml')
if required_tags(retval,'fdbfiles') then retval=rep_tag_value(retval,[retval.fdbfiles,fdbfiles],'fdbfiles')  else $ 
         retval=add_tag(retval,fdbfiles,'fdbfiles')

fdbinfo=iris_multitab2struct(retval.fdbfiles,/info)
crstype=fdbinfo.info.type
fdbdata=iris_multitab2struct(retval.fdbfiles)

;crslist='CRS-'+fdblist+"-"+crstype
crslist='CRS-'+fdbdata.data.crsid+"-"+crstype
crsfiles=concat_dir(table_path,crslist+'.xml')
retval=add_tag(retval,crsfiles,'crsfiles')

if check_files then begin 
   allfiles=''
   patts=str2arr('obs,frm,fdb,crs')
   for f=0,n_elements(patts)-1 do $ 
      allfiles=[allfiles,gt_tagval(retval,patts[f]+'files',missing='')]
   ss=where(allfiles ne '',fcnt)
   if fcnt eq 0 then allfiles='' else allfiles=allfiles[ss]
   fexist=where(file_exist(allfiles),ecnt)
   case 1 of 
      fcnt eq 0 or ecnt eq 0: box_message,["None of the files exist",allfiles]
      ecnt eq fcnt: box_message,"All files exist"
      else: box_message,["Some files missing:",allfiles[ where(1-file_exist(allfiles))]]
   endcase
   retval=add_tag(retval,allfiles,'allfiles')
endif


if debug then stop,'retval' 

return,retval
end
      


