function iris_l2_status2need,l2s, l12l2=l12l2, coverage=coverage, hcr=hcr, mp4=mp4, error=error, count=count, error_override=error_override, $
   l2_minversion=l2_minversion, l1p5_minversion=l1p5_minversion,l122_pipe_minversion=l122_pipe_minversion, jsoc_override=jsoc_override, $
   ingest=ingest
;
;   Name: iris_l2_status2need
;
;   Purpose: return subset of l2-status structures which match users(apps) stage-needed? booleans/tests
;
;   Input Parameters:
;      l2s - iris L2-status vector, usually from iris_l2_time2status(t0,t1 [,/timeline], [/drms] [,/nrt] )
;
;   Keyword Parameters:
;      (Assume following are mutually exclusive)
;      l1p5_minversion=V# - subset generated with Paul's/iris_prep Older than supplied #
;      l2_minversion=V# -   subset generated Martins/l122 Older than supplied#
;      /l12l2 -    Need/Ready for L1->L2 stage? (no L2 -or- additional L1@jsoc since existing L2 -AND- Not In-Progress
;      /coverage - Need/Ready for coverage (www/voe) stage? No coverage -OR- last coverage older than existing L2
;      /mp4 - subset with coverage/SJI movies (no date check) - (mp4 stage can only run on Macs w/qt_tools installed) 
;
;   History:
;      12-dec-2013 - S.L.Freeland - weak AI applied to L2-status structure vector
;      20-dec-2013 - S.L.Freeland - tweak /coverage logic; add This doc-header
;
;-

if not required_tags(l2s,'l2log_exist,l2_errlog_exist,l2_nreceived') then begin 
   box_message,'Need IRIS level 2 status structures (from iris_l2_time2status.pro)'
   return,-1
endif

error=l2s.l2_errlog_exist
noerror=1-(error * keyword_set(error_override))
jsocok=(l2s.l2_nreceived ge l2s.drms_nl1-4) or (keyword_set(jsoc_override)) 
l2complete=l2s.l2log_exist and jsocok 

count=0
case 1 of
   keyword_set(l2_minversion): ss=where(l2s.l2_version lt l2_minversion,count) ; Martin's l122 suite, minimal version# check
   keyword_set(l1p5_minversion): ss=where(l2s.l1p5_version lt l1p5_minversion,count) ; Paul's/iris_prep 1-1p5 minimal verion# check
   keyword_set(l12l2): ss=where((1-l2complete) and noerror and (1-l2s.l2_inprogress), count) ; no L2 or new L1 and not in progress
   keyword_set(coverage): ss=where(l2complete and (l2s.l2_runtime gt l2s.voe_runtime),count) ; L2 complete and no coverage or L2 more recent than coverage
   keyword_set(ingest): begin
      nrt=strpos(l2s.obspath,'_nrt') ne -1
      fin=1-nrt
      ss=where(fin and (l2s.nvoe gt 0) and l2s.l2comp_ncomp eq 0,count)
      if total(nrt) gt 0 then begin
         box_message,'_nrt - need a tweak for /INGEST algorithm'
         ss=where(nrt and (l2s.nvoe gt 0),count)
      endif
   endcase
   keyword_set(mp4): ss=where(l2s.nvoe gt 0 and l2s.nmp4 eq 0,count)
   keyword_set(error):   
   else: begin
      box_message,'need one of {/l12l2,/coverage,/hcr,/mp4,l2_minversion=#,l1p5_minversion=#...}'
      ss=lindgen(n_elements(l2s))
   endcase
endcase


retval=l2s[ss]
return,retval
end





