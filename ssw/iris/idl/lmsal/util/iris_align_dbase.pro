function iris_align_dbase, t0, t1, refresh=refresh, wave=wave, dbfile=dbfile

common iris_align_dbase_1400, db1400
common iris_aling_dbase_1700, db1700
;
if n_elements(wave) eq 0 then wave=1400
swave=string(wave,format='(i4.4)')

idata=concat_dir('$SSW_IRIS','data')

if n_elements(dbfile) eq 0 then dbfile=$
   concat_dir(idata,'iris_align_'+swave+'.txt')

if not file_exist(dbfile) then begin 
   box_message,'No dbfile found, bailing...
   return,-1
endif



refresh=keyword_set(refresh)

refresh=refresh or (swave eq '1400' and n_elements(db1400) eq 0) or $
                   (swave eq '1700' and n_elements(db1700) eq 0)
if refresh then begin 
   box_message,'loading iris_align dbase '+swave
   dbase=rd_tfile(dbfile)       ; txt -> 1D
   cols=str2cols(dbase,/trim)   ; 1D  -> 2D
   template={date_obs:'',x_off:0.,y_off:0.,crval1_new:0.,crval2_new:0.,wave:'',$
      roll:0.,raster_xsize:0.,raster_ysize:0., peak_str:0.}

   retval=replicate(template,n_elements(dbase))
   for t=0,n_tags(template)-1 do begin
      tempx=retval.(t)
      reads,reform(cols[t,*]),tempx
      retval.(t)=tempx
   endfor
   if swave eq '1400' then db1400=retval else db1700=retval ; update cache
endif else begin
   if swave eq '1400' then retval=db1400 else retval=db1700
endelse

return,retval
end


