
ssw_path,'$HOME/soft/idl/idl_startup', /prepend
;tarr = timegrid('01-aug-2012',anytim(addtime(!stime,delta_min=31.*24.*60.),/yoh),month=1)
;tarr = timegrid('01-jun-2013',anytim(addtime(!stime,delta_min=31.*24.*60.),/yoh),month=1)
;tarr = timegrid('01-jan-2013','01-oct-2013',month=1)
;tarr = timegrid('01-feb-2013',month=1)
tarr = timegrid('01-jun-2013',anytim(addtime(!stime,delta_min=31.*24.*60.),/yoh),month=1)
;tarr = '01-jul-2013'
;tarr = '06-aug-2013'

ntim = n_elements(tarr)
t_start_run_sec = anytim(!stime)

for i=0,ntim-1 do begin
  t_start_month_sec = anytim(!stime)
  mk_iris_html_month_tab, tarr(i)
  t_finish_month_sec = anytim(!stime)

  t_minutes_month_total = (t_finish_month_sec-t_start_month_sec)/60d0
  print, 'Completed processing for month ' + tarr[i] + ' in ' + $
    strtrim(t_minutes_month_total,2) + ' minutes.'
endfor
t_finish_run_sec = anytim(!stime)

t_minutes_run_total = (t_finish_run_sec-t_start_run_sec)/60d0
print, 'IRIS Timeline Page generated in ' + strtrim(t_minutes_run_total,2) + ' minutes.'

end
