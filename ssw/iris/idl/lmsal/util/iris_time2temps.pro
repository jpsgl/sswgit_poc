function iris_time2temps, t0, t1, _extra=_extra, out_style=out_style, use_name=use_name, refresh=refresh, $
   lastn_hours=lastn_hours, apid=apid, all_temps=all_temps, ccd=ccd, sc=sc, append=append
;+
;   Name: iris_time2temps
;
;   Purpose: like the Name says;
;
;   Input Parameters:
;      t0 - vector of times or start of time-range
;      t1 - end of time range 
;      lastn_hours - like the name says, in lieu of t0,t1
;      all_temps - return nested structure (multiple apids, curently 1846/77/84)
;      apid - optional apid (def=84) - 77 has a few CCD temps monitored by the CEB
;      ccd - use apid 77 (appears more rational temps)
;		sc - use apid 1846 (spacecraft-monitored temperatures)
;      append=append - if T0 is a structure vector (image 'index' for example), return That w/temps tags appended
;
;   Restrictions:
;      Only instrument, default= apid 84 (use apid=77 aka /CCD for alternate (better?)  CCD)
;
;   Method:
;      tlm / IRIS-itos dbase demo
;
;   History:
;      5-jul-2013 - S.L.Freeland - temps from apid 1846
;     15-jul-2013 - S.L.Freeland - add apid 77 support, aka /CCD
;                   allow T0 to be a vector of times - and /APPEND option if t0 is 'index' vector
;	  19-jul-2013 - P.Boerner	- Add apid 84 (and make it the default)
;      
;-

common iris_time2temps,tlm
common iris_time2temps1,lastapid, strtemp

if keyword_set(all_temps) then begin
   retval=add_tag(retval,iris_time2temps(t0,t1,lastn_hours=lastn_hours,apid=1846,use_name=use_name),'sc')
   retval=add_tag(retval,iris_time2temps(t0,t1,lastn_hours=lastn_hours,apid=77,use_name=use_name),'ceb')
   retval=add_tag(retval,iris_time2temps(t0,t1,lastn_hours=lastn_hours,apid=84,use_name=use_name),'inst')
   return,retval
endif

case 1 of 
   keyword_set(apid): ; user supplied (and caveat emptor..)
   keyword_set(ccd): apid=77
   keyword_set(sc): apid=1846
   else: apid=84
endcase

refresh=keyword_set(refresh)
if n_elements(lastapid) gt 0 then begin 
   delvarx,strtemp
   refresh=refresh or (apid ne lastapid)
endif
lastapid=apid

if n_elements(tlm) eq 0 then begin
   ssw_path,'$SSW_IRIS/gse/'
   recompile,'sag_ext_mnem'
   iris_set_gse,/vc2,tlm,/refresh ; force update of 'tlm' dbase
endif

if n_params() lt 1 and 1-keyword_set(lastn_hours) then begin 
   box_message,'No times supplied and LASTN_HOURS not set - defaulting to LASTN_HOURS=6
   lastn_hours=6
endif else begin 
   if n_params() eq 1 then begin ; vector of times?
      tempt0=t0
      if required_tags(t0,/date_obs) then t0=t0.date_obs
      time_window,temporary(t0),t0,t1,minutes=5  
   endif
endelse

if keyword_set(lastn_hours) then begin 
   t0=reltime(hours=(abs(fix(lastn_hours))*(-1)))
   t1=reltime(/now)
endif

apdata=sag_time2apdata(t0,t1,apid)

case apid of 
   77: pattern='*ccd*temp*
   84: sst=where(strmatch(tlm.conv,/fold,'y_i_ts*') and tlm.apid eq apid)
   else: pattern='*inst*temp*conv*'
endcase
if n_elements(sst) eq 0 then sst=where(strmatch(tlm.descr,pattern,/fold) and tlm.apid eq apid)

ntemp=n_elements(sst)
names=tlm[sst].name
if keyword_set(use_name) then desc=names else $
	case apid of
		77: desc=str_replace(strtrim(strextract('**'+strlowcase(tlm[sst].descr),'**','temperature'),2),' ','_')
		84: desc=str_replace(str_replace(str_replace(tlm[sst].descr, ' ', '_'), ',', ''), '.', '') + '_' + strmid(tlm[sst].name, 2, 4)
		1846:desc=str_replace(strtrim(strextract('**'+strlowcase(tlm[sst].descr),'instrument','temperature'),2),' ','_')
		else: desc=names
	endcase
desc=str_replace(str_replace(desc,'+','pos'),'-','neg')

estat=execute('strtemp={date_obs:"",' + arr2str(desc+':0.')+'}')

retval=replicate(strtemp,n_elements(apdata.times))
if n_elements(out_style) eq 0 then out_style='ccsds'
retval.date_obs=anytim(apdata.times,out_style=out_style)
for t=1,n_elements(sst) do retval.(t)=sag_ext_mnem(apdata.data,tlm[sst[t-1]].name)

if n_elements(tempt0) gt 0 then begin 
   mtimes=tempt0
   if required_tags(mtimes,/date_obs) then mtimes=anytim(mtimes.date_obs,/int)
   ss=tim2dset(anytim(retval.date_obs,/ints),mtimes)
   retval=retval[ss]
   if keyword_set(append) and data_chk(retval,/struct) then begin 
      retval=join_struct(tempt0,retval)
   endif   
   t0=tempt0 ; restore clobbered param
endif

return,retval
end














