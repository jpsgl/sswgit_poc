function iris_time2timeline,t0,t1, debug=debug, description_pattern=description_pattern, force_remote=force_remote, quiet=quiet, _extra=_extra, fsn=fsn, $
   slew=slew, roll=roll, dump=dump,queue=queue, viz2=viz2, science_timeline=science_timeline, keep_all=keep_all, loud=loud
;
;+
;   Name: iris_time2timeline
;
;   Purpose: user time ranges -> timelines -> structures
;
;   Input Parameters:
;      t0,t1 - timerange
;
;   Keyword Parameters:
;      description_pattern - optional description pattern(s) for sub selection - may be array or comma delimited list
;      /timeline_name - optionally include timeline absolute path for each element (inherit->iris_timeline2struct)
;      /fsn - optionally add .first_fsn, .last_fsn, .nfsns via jsoc/drms (via iris_jsoc_time2fsn.pro) 
;      /slew, /roll, /dump,/queue - use TIM files
;      /keep_all - don't apply final time filter; ex, for /ROLL, I want the existing roll value at t0
;      /loud - echo more diagnostics/status 
;
;   Output:
;      function returns stucture vector
;
;   Calling Examples:
;      IDL> tlines=iris_time2timeline(reltime(days=-3),reltime(/now)) ; timeline records for last 3 days
;      IDL> flats=iris_time2timeline(reltime(days=-3),reltime(/now),descript='flat') ; same, with *flat* in timeline description 
;      IDL> cals=iris_time2timeline(reltime(days=-3),reltime(/now),descript='dark,flat,cal') ; same, array of search things
;      IDL> roll=iris_time2timeline(reltime(days=-3),reltime(/now),/roll,/keep_all) ; use TIM files instead of science timelines
;                                                                                   ; don't apply strict time window filter (previous wanted)
;
;   History:
;      19-jul-2013 - S.L.Freeland - combine ssw_time2filelst + iris_timeline2struct (thanks Sarah)
;                    Later that same day, added cloud bit - so should work for all ssw clients (let me know...)
;      20-juk-2013 - S.L.Freeland - changed timeline parent organization (apparently adhoc moving target - just trying to keep up...)
;      22-jul-2013 - S.L.Freeland - fixed a time range check - date_end -> date_obs
;      25-jul-2013 - S.L.Freeland - handle format change ins 'size' reporting
;      26-jul-2013 - S.L.Freeland - add /FSN keyword and function
;       5-aug-2013 - S.L.Freeland - change default path; restore remote/cloud 
;       9-aug-2013 - S.L.Freeland - moving timeline directory fixup
;      16-aug-2013 - S.L.Freeland - change default timeline location viz2->irisa
;      19-aug-2013 - S.L.Freeland - yyyymmdd -> yyyy/mm/dd, archive -> iris_tim_archive
;      22-aug-2013 - S.L.Freeland - add /science - (forces use of science timeline in conjunction with /DUMP
;      23-aug-2013 - S.L.Freeland - allow timelines with No OBS
;       3-sep-2013 - S.L.Freeland - expand lookback window (holiday weekends - like yesterday/labor day for example...)
;      27-sep-2013 - S.L.Freeland - protection for /TIM files which contain nothing of interest, - add /KEEP_ALL keyword&function
;      13-dec-2013 - S.L.Freeland - change parent url -> irisa 
;       6-Jan-2014 - S.L.Freeland - repair long time off by 1
;      28-aug-2014 - S.L.Freeland - add /QUEUE , made /quiet the default (use /LOUD to override)
;      29-aug-2014 - S.L.Freeland - tweak pattern for remote lookup of _tim files(/queue,/slew,/roll,/dump) cloud ok
;       1-dec-2015 - R. Timmons - change lookback window to 6 days, needed for new 5 day timelines
;
;-
loud=keyword_set(loud)
quiet=keyword_set(quiet) or loud ; quiet is the new loud

if keyword_set(viz2) then parent='/viz2/media/iris_timelines/archive/' else parent='/irisa/ops/timeline/iris_tim_archive/'
remote=1-file_exist(parent) or keyword_set(force_remote)

;parent=([parent,'http://sdowww.lmsal.com/sdomedia/ssw/neptune/iris/ops/timeline/iris_tim_archive'])(remote) 
parent=([parent,'http://www.lmsal.com/solarsoft//irisa/ops/timeline/iris_tim_archive'])(remote) 



case n_params() of 
   0: begin
        t0=reltime(days=-1)
        t1=reltime(/now)
   endcase
   1: t1=reltime(t0,/days)
   else:
endcase

time_window,[t0,t1],days=[-6,1],t0x,t1x   ; look back window (Friday plan for weekend for example)


slew=keyword_set(slew) 
roll=keyword_set(roll)
dump=keyword_set(dump)
queue=keyword_set(queue)

seq=1-(slew or roll or dump) 
timfile=(slew or roll or dump or queue) ; use tim file instead of science timeling
timfile=timfile and (1-keyword_set(science_timeline)) ; override 'tim'

pattern=(['*IRIS*timeline*txt','*iris_tim_2*.V??"*'])(timfile)
files=ssw_time2filelist(t0x,t1x,parent=parent,patt=pattern,debug=debug,_extra=_extra, ext='.txt',/quiet)
filedays=ssw_strsplit(files,'.V',/head,tail=versions)
uday=uniq(filedays)
files=files[uday]

if get_logenv('check_tim') then stop,'files,parent'

if files[0] eq '' then begin 
   box_message,'cannot find local or remote timelines for this time range... bailing
   return,-1
endif

if strpos(files(0),'http') ne -1 then begin ; Cloud bit
   if loud then box_message,'running or faking remote access'
   ldir=get_temp_dir()
   tnames=ssw_strsplit(files,'/',/tail)
   lnames=concat_dir(ldir,tnames)
   nf=n_elements(files)
   if total(file_exist(lnames)) ne nf then sock_copy,files,out_dir=ldir
   files=lnames ; local names
   if total(file_exist(files)) ne n_elements(files) then begin 
      box_messagse,'problem with one or more transfers.. bailing
      return,-1 ; Un-Structured, Early exit !!
   endif
endif 
nfiles=n_elements(files)
tcnt=0
f=0
retval=-1
while (f lt nfiles) and 1-data_chk(retval,/struct) do begin 
  if timfile then retval=iris_timeline2struct_tim(files[f],_extra=_extra,roll=roll,slew=slew,dump=dump,queue=queue) else $
                  retval=iris_timeline2struct(files[f],_extra=_extra,dump=dump)
      f++
endwhile 
if data_chk(retval,/struct) then begin 
   for n=f,nfiles-1 do begin
      if timfile then next=iris_timeline2struct_tim(files[n],_extra=_extra,roll=roll,slew=slew,dump=dump,queue=queue) else $
         next=iris_timeline2struct(files[n],_extra=_extra,dump=dump)
      if data_chk(next,/struct) then retval=[retval,next] 
   endfor
   if keyword_set(keep_all) then begin
      sst=lindgen(n_elements(retval)) 
      if data_chk(retval,/struct) then tcnt=n_elements(retval) 
   endif else sst=where(anytim(retval.(0)) ge anytim(t0) and anytim(retval.(0)) le anytim(t1),tcnt)
endif else begin 
   box_message,'No OBS in all files!'
endelse

if tcnt eq 0 then begin
   if loud then box_message,'No timeline record found in your time range
   retval=-1
endif else begin
   retval=retval[sst] 
   if keyword_set(description_pattern) then begin 
      desc=description_pattern
      if n_elements(desc) eq 1 then desc=str2arr(desc) ; allow comma delimted list
      pm=lonarr(n_elements(retval))
      for p=0,n_elements(desc)-1 do begin
         patt=str_replace('*'+desc[p]+'*','**','*')
         pm=pm or strmatch(retval.description,patt,/fold)
      endfor
      ssp=where(pm,pcnt)
      if pcnt eq 0 then begin 
         box_message,'records in your time range but non matching pattern>> ' + patt
         retval=-1
      endif else retval=retval[ssp]
   endif
endelse

if data_chk(retval,/struct) and keyword_set(fsn) then begin
   retval=iris_jsoc_time2fsn(retval,silent=silent,loud=loud,_extra=_extra)
endif

return,retval

end


;
