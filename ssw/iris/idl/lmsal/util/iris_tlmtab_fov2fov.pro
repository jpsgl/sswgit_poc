function iris_tlmtab_fov2fov, tlmstruct, tag=tag, append_tags=append_tags
;
;+
;   Name: iris_tlmtab_fov2fov
;
;   Purpose: add <tag>_FOVX and <tag>_FOVY - may be arrays, padded to max #FOVs
;
;   Input Parameters:
;      tlmstruct - structure, usually from iris_tlmtab2struct.pro
;
;   History:
;      12-mar-2013 - S.L.Freeland - do the <tag>_FOV NX x NY -> <tag>_FOVX + <tag>_FOVY
;-

if n_elements(tag) eq 0 then tag='raster_fov'
if not required_tags(tlmstruct,tag) then begin 
   box_message,'need structure with tagname>> ' + tag
   return,retval
endif

tdata=gt_tagval(tlmstruct,tag)

nel=n_elements(tdata)

nfov=intarr(nel)

for i=0,nel-1 do nfov[i]=n_elements(where_pattern(tdata[i],'x'))
nfov=(nfov-1)>1 
maxfov=max(nfov) ;  pad to max #FOV
tn=strtrim(sindgen(maxfov),2)
tx=tag+'x'
ty=tag+'y'

cols=str2cols(tdata,' ',/unal,/trim)


retval=add_tag(retval,replicate(0.,maxfov),tx)
retval=add_tag(retval,replicate(0.,maxfov),ty)
retval=replicate(retval,nel)
for f=0,maxfov-1 do begin  ; LOOP for max #FOV
   if data_chk(cols,/ny) eq 0 then $
      fx=ssw_strsplit(cols,'x',/head,tail=fy) else $
      fx=ssw_strsplit(cols[f,*],'x',/head,tail=fy) ; separate NX/NY 
   retval.(0)[f,0]=float(str2number(fx))  ; populate FOV[f]
   retval.(1)[f,0]=float(str2number(fy))
endfor
retval=add_tag(retval,nfov,tag+'_nfov')

if keyword_set(append_tags) then begin 
   temp=tlmstruct
   rtn=tag_names(retval)
   for t=0,n_elements(rtn)-1 do $
      temp=add_tag(temp,reform(transpose(retval.(t))),rtn[t])
   retval=temp
endif

return,retval
end
