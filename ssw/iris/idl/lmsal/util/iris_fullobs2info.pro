function iris_fullobs2info, fullobs, date_end=date_end
;
;+
;   Name: iris_fullobs2info
;
;   Purpose: yyyymmdd_hhmmss_<dateobs> string(s) - {date_obs:'',date_end:'',obsid:''}xN
;
;   Input Parameters:
;      vector of one or more fully qualified IRIS obsids like yyyymmdd_hhmmss_<obsid>
;
;   Output:
;      function returns structure vector {date_obs, date_end, obsid} .date_end null unless /DATE_END set
;
;   Keyword Parameters:
;     date_end (switch) - if set, get .DATE_END from iris timelines (may take a which if input obs span months/years)
;
;   Method:
;      ssw-gen stuff 
;   
;   History:
;      24-feb-2014 - S.L.Freeland - simple minded albeit ubiquitous few liner -> one liner
;-
if not data_chk(fullobs,/string) then begin 
   box_message,'Need IRIS fully qualified obsid like YYYYMMSS_HHMMSS_<obsid>
   return,-1
endif

fobs=strcompress(fullobs,/remove)
if n_elements(fobs) eq 1 and strpos(fobs[0],',') ne -1 then fobs=str2arr(fobs)

cols=str2cols(fobs,'_',/trim)
if data_chk(cols,/nx) ne 3 then begin 
   box_message,'Need IRIS fully qualified obsid like YYYYMMSS_HHMMSS_<obsid>
   return,-1
endif

nret=n_elements(fobs)
retval=replicate({date_obs:'',date_end:'',obsid:''},nret)
strtab2vect,cols,ymd,hms,obsids

date_obs=file2time(ymd+'_'+hms,out='ccsds')
retval.date_obs=date_obs
retval.obsid=obsids

if keyword_set(date_end) then begin ; get .DATE_END from timelines
   sdate=sort(date_obs)
   time_window,date_obs(sdate),tl0,tl1
   tl=iris_time2timeline(tl0,tl1)
   if required_tags(tl,'date_obs,obsid') then begin
   ss=tim2dset(anytim(tl.date_obs,/int),anytim(date_obs,/int))
   retval.date_end=tl[ss].date_end
      
   endif else box_message,'timeline access issue; .DATE_OBS will remain null
endif

return,retval
end













