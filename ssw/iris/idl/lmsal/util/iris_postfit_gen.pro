function iris_postfit_gen,xs,g

;PURPOSE-this routine assesses the type of profile
;   that is produced by IRIS_MGFIT
;INPUTS
;       XS-vector of abscissa values
;       G-vector of profile intensities
;
;OUTPUTS-
;       OUTS-11 element vector, not all elements will be populated
;        (defaults 0) depending on the number of extrema detected in
;         the profile
; OUTS[0]-H1V wavelength or index
; OUTS[1]-H1V intensity
; OUTS[2]-H1R wavelength
; OUTS[3]-H1R intensity
; OUTS[4]-H2V wavelength
; OUTS[5]-H2V intensity
; OUTS[6]-H2R wavelength
; OUTS[7]-H2R intensity
; OUTS[8]-H3 wavelength
; OUTS[9]-H3 intensity
; OUTS[10]-profile type
;     0-fit not attempted
;     1-Double peaked with H1 mins
;     2-Single peaked with H1 mins
;     3-Single peaked without H1 mins
;     4-Double peaked without H1 mins
;     5-MPFIT did not converge happily
;
  
flag=5
e=iris_extrema(g,min=mins,max=maxs,/end)
x=n_elements(xs)

outs=fltarr(11)
moms=fltarr(14)
;normal 4 max (2@edge), 3 min
if (n_elements(mins) eq 3)*(n_elements(maxs) eq 4) then begin
   if (maxs(0) eq 0)*(maxs(3) eq x-1) then flag=1
endif
;single peak with k1
if (n_elements(mins) eq 2)*(n_elements(maxs) eq 3) then begin
   if(maxs(0) eq 0)*(maxs(2) eq x-1) then flag=2
endif
;single peak without k1
if (n_elements(mins) eq 2)*(n_elements(maxs) eq 1) then begin
   if(mins(0) eq 0)*(mins(1) eq x-1) then flag=3
endif
;double peak without k1
if (n_elements(mins) eq 3)*(n_elements(maxs) eq 2) then begin
   if(mins(0) eq 0)*(mins(2) eq x-1) then flag=4
endif

case flag of
   0:outs(10)=flag
   1:begin
     outs(0:3)=[xs(mins(0)),g(mins(0)),xs(mins(2)),g(mins(2))]
     outs(4:7)=[xs(maxs(1)),g(maxs(1)),xs(maxs(2)),g(maxs(2))]
     outs(8:9)=[xs(mins(1)),g(mins(1))]
     outs(10)=flag
 
   end
   2:begin             
      outs(0:3)=[xs(mins(0)),g(mins(0)),xs(mins(1)),g(mins(1))]
      outs(4:5)=[xs(maxs(1)),g(maxs(1))]
      outs(10)=flag
   end
   
   3:begin
      outs(4:5)=[xs(maxs(0)),g(maxs(0))]
      outs(10)=flag
   end
   
   4:begin
      outs(4:7)=[xs(maxs(0)),g(maxs(0)),xs(maxs(1)),g(maxs(1))]
      outs(8:9)=[xs(mins(1)),g(mins(1))]
      outs(10)=flag
   end
   
   5:outs(10)=flag
endcase
        
return,outs
end

