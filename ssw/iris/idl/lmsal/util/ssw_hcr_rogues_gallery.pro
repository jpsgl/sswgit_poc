pro ssw_hcr_rogues_gallery,t0,t1,match_pattern=match_pattern,html_dir=html_dir, obsids=obsids, wavelnth=wavelnth, $
   hcr=hcr, nmatch=nmatch,  taglist=taglist
;+
;   Name: ssw_hcr_rogues_gallery
;
;   Purpose: produce thumbnail gallery for input HCR vector -or- HCR records mathcing MATCH pattern
;
;   Input Parameters:
;      t0,t1 - time range for coverage event search (HCR)
;
;   Keyword Parameters:
;      match - optional HCR search pattern (per iris_obs2hcr.pro)
;      hcr - optional user supplied hcr vector ( t0,t1 & MATCH_PATTERN are ignored if supplied) 
;      html_dir - path for output.html (defa
; 
;      

mpatt=''
if data_chk(match,/string) then mpatt='_'+ str_replace(str_replace(match_pattern,',','_'),' ','_')

if n_elements(hcr) eq 0 then hcr=iris_obs2hcr(t0,t1,match=match,obsids=obsids,taglist=taglist)

if n_elements(wavelnth) eq 0 then wavelnth='2796,1330,1400,2832'

swaves=str2arr(wavelnth)
swaves='_'+swaves+'_'
swaves=str_replace(swaves,'__','_')

swave='_'+arr2str(swaves,'_')

time_window,[hcr.starttime,hcr.stoptime],t0x,t1x

troot=arr2str(time2file([t0x,t1x],/date_only),'_')

www=concat_dir(hcr.obspath,'www')

ind=concat_dir(www,'index.html')

host=get_host()
phtml=concat_dir('$HOME','public_html')
case 1 of 
   file_exist(html_dir): ; user supplied HTML_DIR
   strpos(host,'lmsal') ne -1: html_dir='/sanhome/'+get_user()+'/public_html/hcr_rogues_gallery' ; lmsal default
   file_exist(phtml): html_dir=concat_dir(phtml,'hcr_rogues_gallery')
   else: html_dir=curdir()
endcase

mk_dir,html_dir 

hdoc=concat_dir(html_dir,'hcr_rogues_'+hcr[0].instrument + '_' + troot+mpatt+swave+'.html')

box_message,hdoc

html_doc,hdoc,/header
nhcr=n_elements(hcr)
outhtml=strarr(14*nhcr)
info=get_infox(hcr,'obstitle,goal,planners',delim=' ** ')

for i=0,nhcr-1 do begin 
   indat=rd_tfile(ind[i])
   if n_elements(indat) gt 10 then begin
   wcnt=0 & cnt=0
   while wcnt eq 0 and cnt lt n_elements(swaves) do begin
      ss0=where(strmatch(indat,'<A name="*'+swaves[cnt]+'*',/fold),wcnt)
      cnt++
   endwhile
   if wcnt gt 0 then begin 
      ss1=where(indat[ss0:*] eq '</td></table>')
      tdat=indat[ss0:ss1+ss0]
      abs=str_replace(tdat,'"l2','"'+hcr[i].url+'/www/l2')
      abs=str_replace(abs,'<br>','')
      abs=str_replace(abs,'<em>','<font color=brick>'+info[i]+'</font><br><em>')
      file_append,hdoc,abs
   endif else box_message,'No matching waves for this HCR >> ' + hcr[i].full_obsid
   endif else box_message,'corrupt index? >> ' + ind[i]
endfor
html_doc,hdoc,/trailer

return
end


