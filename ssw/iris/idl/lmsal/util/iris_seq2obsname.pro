function iris_seq2obsname,seqstrs, reverse_the_polarity=reverse_the_polarity
;
;+
;   Name: iris_seq2obsname
;
;   Purpose: return "obsname" for input sequence or timeline structures (or the inverse)
;
;   Input Parameters:/t0

;      seqstrs - vector of one or more sequence or timeline structures -or-
;                vector of OBSNAMES in conjunction with /reverse_the_polarity
;
;   Output:
;      function returns "obsname" = yyyymmdd_hhmmss_yyyymmdd_hhmmss_<OBSID>
;      (or structures {date_obs:'',date_end:'',obsid:0ul} if /reverse set and OBSIDS input)
;   
;   Keyword Parameters:
;      reverse_the_polarity - (switch) - if set & OBSNAMEs input, output "sequence" structures 
;
;   Calling Example:
;      IDL> tl=iris_time2timeline() ; get some structures
;      IDL> obsnames=iris_seq2obsname(tl) ; structures -> obsnames
;      
;      IDL> seqstr=iris_seq2obsname(OBSIDS,/reverse) ; the ~inverse (only .date_obs/.date_end/.obsid)
;
;   History:
;      29-aug-2013 - S.L.Freeland - data management utility
;
;-


reverse=keyword_set(reverse_the_polarity)

if reverse and data_chk(seqstrs[0],/string) then begin 
   obsnames=strtrim(seqstrs,2)
   template='yyyymmdd_hhmmss_yyyymmdd_hhmmss_oooooooooo'
   ssb=where(strlen(obsnames) ne strlen(template),bcnt)
   if bcnt gt 0 then begin
      box_message,'One or more invalid OBSNAMES input, bailing...
      return,-1
   endif
   cols=str2cols(obsnames,'_',/trim)
   strtab2vect,cols,dd0,tt0,dd1,tt1,obsids
   template={date_obs:'',date_end:'',obsid:0ul}
   nret=n_elements(obsnames)
   retval=replicate(template,nret)
   retval.date_obs=anytim(file2time(dd0+'_'+tt0),/ccsds,/trunc)
   retval.date_end=anytim(file2time(dd1+'_'+tt1),/ccsds,/trunc)
   retval.obsid=ulong(obsids)
endif else begin
   if not required_tags(seqstrs[0],'obsid') then begin 
      box_message,'Need IRIS sequence or timeline structures..., bailing.
      return,-1
   endif
   t0=gt_tagval(seqstrs,/date_obs, missing=gt_tagval(seqstrs,/seq_start,missing=''))
   t1=gt_tagval(seqstrs,/date_end, missing=gt_tagval(seqstrs,/seq_end,missing=''))
   if t0[0] eq '' or t1[0] eq '' then begin 
      box_message,'Need IRIS sequence or timeline structures..., bailing.
      return,-1
   endif
   retval=time2file(t0,/sec) + '_' + time2file(t1,/sec) + '_' + string(seqstrs.obsid,format='(I10.10)')
endelse

return,retval
end

