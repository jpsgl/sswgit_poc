Contribution to SSW of the IRIS spectrograph point spread functions.
Contains 2 files:

1. iris_sg_deconvolve.pro  
	A simple IRIS specific deconvolution routine.  Users can 
	choose Richardson-Lucy or FFT type deconvolutions.  Calls 
	subroutine FFT_CONV_1D, included in source code, and 
	iris_sg_psfs.geny.  Expects to find iris_sg_psfs.geny in 
	/ssw/iris/response  
	
2. iris_sg_psfs.geny
	Contains 1D spatial PSFs for the 1336, 1394, 2796, and
	2814 spectrograph windows, covering both FUV and NUV channels.  
	
     