; ----------------------------------------- view_uv_atlas.pro -------- ;
FUNCTION regions_Event_Func, event

  widget_control, event.id, GET_UVALUE=uvalue

  return, {ID:event.handler, TOP:event.top, HANDLER:0L, VALUE:uvalue}
END

FUNCTION read_hawaii_uv_atlas, state

  ERG_TO_JOULE   = 1.0E-7
  CM_TO_M        = 1.0E-2

  atlasFile = getenv('UV_ATLAS_PATH') + '/hawaii_UV.dat'

  openr, lun, atlasFile, /GET_LUN, /XDR
  Nspect = 0L
  readu, lun, Nspect

  ;; --- Determine wavelength scale from two known lines -- --------- ;;

  rec1 = 33963L  &  lambda1 = 284.984D0           ;; CrII line
  rec2 = 35132L  &  lambda2 = 285.567D0           ;; CrII/FeII doublet

  dlambda = (lambda2 - lambda1)/(rec2 - rec1)
  lambda0 = lambda1 - rec1*dlambda
  lambda  = lambda0 + dindgen(Nspect) * dlambda

  atlas = fltarr(Nspect)
  readu, lun, atlas
  free_lun, lun

  atlas *= ERG_TO_JOULE / CM_TO_M^2

  state.lambdaMin = lambda0
  state.lambdaMax = lambda[Nspect-1]

  IF (ptr_valid(state.lambda_ptr)) THEN ptr_free, state.lambda_ptr
  IF (ptr_valid(state.atlas_ptr))  THEN ptr_free, state.atlas_ptr

  state.lambda_ptr = ptr_new(lambda)
  state.atlas_ptr  = ptr_new(atlas)

  return, state
END

FUNCTION read_sumer_atlas, state

  IF (state.subName EQ 'quiet_Sun') THEN $
   atlasFile = getenv('UV_ATLAS_PATH') + '/sumer_atlas_quiet.dat' $
  ELSE $
   atlasFile = getenv('UV_ATLAS_PATH') + '/sumer_atlas_network.dat'

  openr, lun, atlasFile, /GET_LUN, /XDR
  Nspect = 0L
  readu, lun, Nspect
  atlas = fltarr(Nspect)  &  lambda = atlas
  readu, lun, lambda, atlas
  free_lun, lun

  lambda = lambda / 5.0           ;; Wavelengths are given in first order
  valid = where(atlas GT 0.0)
  atlas = atlas[valid]  &  lambda = lambda[valid]

  state.lambdaMin = lambda[0]
  state.lambdaMax = lambda[n_elements(lambda) - 1]

  IF (ptr_valid(state.lambda_ptr)) THEN ptr_free, state.lambda_ptr
  IF (ptr_valid(state.atlas_ptr))  THEN ptr_free, state.atlas_ptr

  state.lambda_ptr = ptr_new(lambda)
  state.atlas_ptr  = ptr_new(atlas)

  return, state
END

FUNCTION read_hrts_atlas, state

  CASE (state.subName) OF
    'quiet_Sun': atlasFile = getenv('UV_ATLAS_PATH') + '/qr.dat'
    'quiet_Sun_A': atlasFile = getenv('UV_ATLAS_PATH') + '/qqr_a.dat'
    'quiet_Sun_B': atlasFile = getenv('UV_ATLAS_PATH') + '/qqr_b.dat'
    'quiet_Sun_limb': atlasFile = getenv('UV_ATLAS_PATH') + '/qqr_l.dat'

    'Active_region': atlasFile = getenv('UV_ATLAS_PATH') + '/ar.dat'
    'Active_region_limb': atlasFile = getenv('UV_ATLAS_PATH') + '/ar_l.dat'

    'Lightbridge': atlasFile = getenv('UV_ATLAS_PATH') + '/lb.dat'
    'Explosive_event': atlasFile = getenv('UV_ATLAS_PATH') + '/hr2_ee.dat'
    'Prominence': atlasFile = getenv('UV_ATLAS_PATH') + '/hr2_prot.dat'

    'Sunspot_A': atlasFile = getenv('UV_ATLAS_PATH') + '/spi.dat'
    'Sunspot_B': atlasFile = getenv('UV_ATLAS_PATH') + '/spii.dat'
  ENDCASE
  openr, lun, atlasFile, /GET_LUN

  ANGSTROM_TO_NM = 0.1
  ERG_TO_JOULE   = 1.0E-7
  CM_TO_M        = 1.0E-2
  NM_TO_M        = 1.0E-9

  CLIGHT = 2.99792458E+08

  Nspect = 28924L
  N1 = 0
  readf, lun, N1
  lambda = fltarr(Nspect)
  atlas  = fltarr(Nspect)
  readf, lun, lambda
  readf, lun, atlas
  free_lun, lun

  lambda *= ANGSTROM_TO_NM
  atlas   = (10.0^atlas) * ERG_TO_JOULE / (CM_TO_M^2 * ANGSTROM_TO_NM)

  state.lambdaMin = lambda[0]
  state.lambdaMax = lambda[n_elements(lambda) - 1]

  IF (ptr_valid(state.lambda_ptr)) THEN ptr_free, state.lambda_ptr
  IF (ptr_valid(state.atlas_ptr))  THEN ptr_free, state.atlas_ptr

  state.lambda_ptr = ptr_new(lambda)
  state.atlas_ptr  = ptr_new(atlas)

  return, state
END

; -------- begin -------------------------- setuvLog.pro ------------- ;

FUNCTION setuvLog, stash, toggle
  widget_control, stash, GET_UVALUE=state

  state.log = toggle
  widget_control, stash, SET_UVALUE=state
  return, state
END
; -------- end ---------------------------- setuvLog.pro ------------- ;

; -------- begin -------------------------- setuvAtlas.pro ----------- ;

FUNCTION setuvAtlas, state, atlasName, Event

  IF (n_elements(event)) THEN $
   state.subName = Event.value $
  ELSE $
   state.subName = 'average disk center'

  CASE (atlasName) OF
    'HRTS':      stash = read_hrts_atlas(state)
    'SUMER':     stash = read_sumer_atlas(state)
    'Hawaii_UV': stash = read_hawaii_uv_atlas(state)

     ELSE:
  ENDCASE

  stash.atlasName = atlasName

  widget_control, stash.minField, $
   SET_VALUE=string(stash.lambdaMin, FORMAT='(F8.3)')
  widget_control, stash.maxField, $
   SET_VALUE=string(stash.lambdaMax, FORMAT='(F8.3)')
  widget_control, stash.atlasField, SET_VALUE=stash.atlasName

  widget_control, stash.subField, SET_VALUE=stash.subName

  widget_control, widget_info(state.baseWidget, /CHILD), SET_UVALUE=stash

  return, stash
END
; -------- end ---------------------------- setuvAtlas.pro ----------- ;

; -------- begin -------------------------- setuvAtlasParams.pro ----- ;

FUNCTION setuvAtlasParams, state
 
  widget_control, state.minField, GET_VALUE=lambdaMin
  widget_control, state.maxField, GET_VALUE=lambdaMax
  state.lambdaMin = lambdaMin[0]  &  state.lambdaMax = lambdaMax[0]

  widget_control, widget_info(state.baseWidget, /CHILD), SET_UVALUE=state
  return, state
END
; -------- end ---------------------------- setuvAtlasParams.pro ----- ;

; -------- begin -------------------------- XViewuvAtlas_Event.pro --- ;

PRO XViewuvAtlas_Event, Event

  ON = 1  &  OFF = 0

  ;; --- Main event handler --                          -------------- ;

  stash = widget_info(Event.handler, /CHILD)
  widget_control, stash, GET_UVALUE=state

  widget_control, Event.id, GET_UVALUE=Action
  CASE Action OF
    'QUIT': BEGIN
      IF (ptr_valid(state.lambda_ptr)) THEN ptr_free, state.lambda_ptr
      IF (ptr_valid(state.atlas_ptr))  THEN ptr_free, state.atlas_ptr

      widget_control, Event.top, /DESTROY
    END

    'SET_ATLAS_PARAMS': displayuvAtlas, setuvAtlasParams(state)

    'HAWAII_ATLAS': displayuvAtlas, setuvAtlas(state, "Hawaii_UV")
    'HRTS':         displayuvAtlas, setuvAtlas(state, "HRTS", Event)
    'SUMER':        displayuvAtlas, setuvAtlas(state, "SUMER", $
                                               Event)

    'PRINT': BEGIN
      filename = dialog_pickfile(/WRITE, FILTER='.eps', $
                                 FILE='UV_atlas.eps')

      IF (filename NE '') THEN BEGIN
        default_device = !D.NAME
        set_plot, 'PS', /COPY
        device, /COLOR, FILENAME=filename
        displayuvAtlas, state
        set_plot, default_device
      ENDIF
    END

    'SAVE': BEGIN
      lambda = *state.lambda_ptr
      index = where(lambda GE state.lambdaMin  AND  $
                    lambda LE state.lambdaMax, count)

      IF (count GT 0) THEN BEGIN
        lambda = lambda[index]
        atlas  = (*state.atlas_ptr)[index]
        atlasName = state.atlasName

        filename = dialog_pickfile(/WRITE, FILTER='.sav', $
                                   FILE='UV_atlas.sav')
        IF (filename NE '') THEN $
         save, FILENAME=filename, atlasName, lambda, atlas
      ENDIF
    END
 

    'LOG_ON': BEGIN
      IF (Event.select) THEN $
       displayUVAtlas, setuvLog(stash, ON) $
      ELSE $
       displayUVAtlas, setuvLog(stash, OFF)
    END

    'XLOADCT': XLoadct, GROUP=Event.top

    'INFORMATION': result = dialog_message(/INFORMATION, $
            ["Interactive display of solar spectrum atlases", $
             "", $
             "Enter minimum and maximum wavelength at the top, just", $
             "below the menubar.", $
              "", $
             "Version 1.0, Apr 3, 2013", $
             "Han Uitenbroek (HUitenbroek@nso.edu)"])

    ELSE:
  ENDCASE

END
; -------- end ---------------------------- XViewuvAtlas_Event.pro --- ;

; -------- begin -------------------------- displayuvAtlas.pro ------- ;

PRO displayuvAtlas, state

  IF (!D.NAME EQ 'X') THEN BEGIN
    widget_control, state.drawWidget, GET_VALUE=WindowNo
    wset, WindowNo
  ENDIF

  CASE (state.atlasName) OF
    "Hawaii_UV": BEGIN
      ytitle = 'Intensity [J m!U-2!N s!U-1!N nm!U-1!N sr!U-1!N]'
      xmargin = [13, 2]
    END
    "SUMER": BEGIN
      ytitle = 'Counts/sec/pixel'
      xmargin = [13, 2]
    END 
    "HRTS": BEGIN
      ytitle = 'Intensity [J m!U-2!N s!U-1!N nm!U-1!N sr!U-1!N]'
      xmargin = [13, 2]
    END
  ENDCASE

  thick = (!D.NAME EQ 'PS') ? 3 : 1
  color = (!D.NAME EQ 'PS') ? 128B : 200B

  plot, *state.lambda_ptr, *state.atlas_ptr, $
        XRANGE=[state.lambdaMin, state.lambdaMax], /XSTYLE, $
        XTITLE='Wavelength [nm]', CHARSIZE=1.2, $
        YTITLE=ytitle, XMARGIN=xmargin, $
        YLOG=state.log, XTHICK=3, YTHICK=3, /NODATA
  oplot, *state.lambda_ptr, *state.atlas_ptr, THICK=thick, COLOR=color

END
; -------- end ---------------------------- displayAvg.pro ----------- ;

; -------- begin -------------------------- UVWidgetSetup.pro -------- ;

FUNCTION UVWidgetSetup, ATLASNAME=atlasName, subName

  IF (NOT keyword_set(ATLASNAME)) THEN BEGIN
    atlasName = "Hawaii_UV"
    subName = 'average disk center'
  ENDIF

  IF (keyword_set(ATLASNAME)  AND  atlasName EQ 'HRTS') THEN BEGIN
    IF (NOT keyword_set(SUBNAME)) THEN subName = 'QR_A'
  ENDIF


  IF (keyword_set(ATLASNAME)  AND  atlasName EQ 'SUMER') THEN BEGIN
    IF (NOT keyword_set(SUBNAME)) THEN subName = 'quiet_Sun'
  ENDIF

  IF (keyword_set(ATLASNAME)  AND  atlasName EQ 'Hawaii_UV') THEN BEGIN
    subName = 'average disk center'
  ENDIF

  state = {log: 0, baseWidget: 0L, drawWidget: 0L, $
           atlasName: atlasName,  atlasField: 0L, $
           subName: subName, subField: 0L, $
           lambdaMin: 0.0, lambdaMax: 0.0, $
           minField: 0L, maxField: 0L, sampleField: 0L, absolute: 0, $
           lambda_ptr: ptr_new(), atlas_ptr: ptr_new()}

  state.baseWidget = widget_base(TITLE='XViewUVAtlas', /COLUMN, $
                                 RESOURCE_NAME='XViewUVAtlas', $
                                 MBAR=menuBar)
  
  atlasBase = widget_base( state.baseWidget, /COLUMN )

  fileMenu   = widget_button(menuBar, VALUE='File', /MENU)
  quitButton = widget_button(fileMenu, VALUE='Quit', UVALUE='QUIT', $
                             RESOURCE_NAME='quitbutton')

  atlasMenu = widget_button( menuBar, VALUE='Atlas', /MENU)

  HawaiiMenu = widget_button(atlasMenu,  VALUE='Hawaii (UV)', $
                             UVALUE='HAWAII_ATLAS')

  SumerMenu = widget_button(atlasMenu, VALUE='SUMER/SOHO', $
                            UVALUE='SUMER', /MENU, $
                            EVENT_FUNC='regions_Event_Func')
  button = widget_button(SumerMenu, VALUE='Quiet Sun', $
                         UVALUE='quiet_Sun')
  button = widget_button(SumerMenu, VALUE='Network', $
                         UVALUE='network')

  HRTSMenu = widget_button(atlasMenu, VALUE='HRTS', $
                             UVALUE='HRTS', /MENU, $
                             EVENT_FUNC='regions_Event_Func' )
  button = widget_button(HRTSMenu, VALUE='Quiet Sun', $
                         UVALUE='quiet_Sun')
  button = widget_button(HRTSMenu, VALUE='Quiet Sun A', $
                         UVALUE='quiet_Sun_A')
  button = widget_button(HRTSMenu, VALUE='Quiet Sun B', $
                         UVALUE='quiet_Sun_B')
  button = widget_button(HRTSMenu, VALUE='Quiet Sun limb', $
                         UVALUE='quiet_Sun_limb')
  button = widget_button(HRTSMenu, VALUE='Active region', $
                         UVALUE='Active_region')
  button = widget_button(HRTSMenu, VALUE='Active region limb', $
                         UVALUE='Active_region_limb')
  button = widget_button(HRTSMenu, VALUE='Lightbridge', $
                         UVALUE='Lightbridge')
  button = widget_button(HRTSMenu, VALUE='Explosive event', $
                         UVALUE='Explosive_event')
  button = widget_button(HRTSMenu, VALUE='Prominence', $
                         UVALUE='Prominence')
  button = widget_button(HRTSMenu, VALUE='Sunspot A', $
                         UVALUE='Sunspot_A')
  button = widget_button(HRTSMenu, VALUE='Sunspot B', $
                         UVALUE='Sunspot_B')


  toolMenu = widget_button( menuBar, VALUE='Tools', /MENU )
  xloadctButton = widget_button( toolMenu, VALUE='XLoadct', $
                                 UVALUE='XLOADCT' )

  exportMenu  = widget_button(menuBar, VALUE='Export', /MENU)
  printButton = widget_button(exportMenu, VALUE='PostScript', UVALUE='PRINT')
  saveButton  = widget_button(exportMenu, VALUE='Write save file', $
                              UVALUE='SAVE')

  helpMenu   = widget_button( menuBar, VALUE='Help', /MENU, /HELP )
  infoButton = widget_button( helpMenu, VALUE='XViewUVAtlas', $
                              UVALUE='INFORMATION' )

  fieldFrame = widget_base( atlasBase, /FRAME, /ROW )
  blueLabel  = widget_label( fieldFrame, VALUE='Wavelength  min:' )
  state.minField = widget_text(fieldFrame, $
                               UVALUE='SET_ATLAS_PARAMS', XSIZE=8, YSIZE=1, $
                               VALUE=string(FORMAT='(F8.3)', state.lambdaMin),$
                               /EDITABLE, RESOURCE_NAME='text' )
  redLabel = widget_label( fieldFrame, VALUE='  max:' )
  state.maxField = widget_text(fieldFrame, $
                               UVALUE='SET_ATLAS_PARAMS', XSIZE=8, YSIZE=1, $
                               VALUE=string(FORMAT='(F8.3)', state.lambdaMax),$
                               /EDITABLE, RESOURCE_NAME='text' )
  nmLabel = widget_label( fieldFrame, VALUE='[nm]' )

  atlasLabel = widget_label( fieldFrame, VALUE='   Atlas:' )
  state.atlasField = widget_label( fieldFrame, /FRAME, /DYNAMIC_RESIZE, $
                                  VALUE=state.atlasName )

  subLabel = widget_label(fieldFrame, VALUE='   Region:')
  state.subField = widget_label(fieldFrame, /FRAME, /DYNAMIC_RESIZE, $
                                VALUE=state.subName)

  ;; --- Draw frame --                                  ------------- ;;

  drawFrame  = widget_base(atlasBase, /FRAME, /COLUMN)
  state.drawWidget = widget_draw(drawFrame, XSIZE=1000, YSIZE=500)

  fieldFrame = widget_base(atlasBase, /FRAME, /ROW, /EXCLUSIVE)
  linearButton = widget_button( fieldFrame, VALUE='Lin', $
                                UVALUE='LOG_OFF' )
  logButton    = widget_button( fieldFrame, VALUE='Log', $
                                UVALUE='LOG_ON' )
  widget_control, linearButton, /SET_BUTTON

  widget_control, widget_info(state.baseWidget, /CHILD), SET_UVALUE=state
  return, state
end
; -------- end ---------------------------- UVWidgetSetup.pro -------- ;

; -------- begin -------------------------- XViewUVAtlas.pro --------- ;

PRO view_UV_Atlas, lambdaMin, lambdaMax, $
                   GROUP_LEADER=group_leader, $
                   HAWAII=hawaii, SUMER=sumer, HRTS=hrts

;+
; NAME:
;	XVIEWUVATLAS
;
; PURPOSE:
;
; CATEGORY:
;	Data reduction
;
; CALLING SEQUENCE:
;
; INPUTS:
; OPTIONAL INPUTS:
; KEYWORD PARAMETERS:
; OUTPUTS:
; OPTIONAL OUTPUTS:
; COMMON BLOCKS:
; SIDE EFFECTS:
; RESTRICTIONS:
; PROCEDURE:
; EXAMPLE:
; MODIFICATION HISTORY:
;
; 	Written by:    Han Uitenbroek
;
;   --- Last modified: Thu Apr  4 23:45:32 2013 --
;-

;;  setenv, "UV_ATLAS_PATH=."

  IF (NOT keyword_set(GROUP_LEADER)) THEN group_leader=0

  IF (keyword_set(SUMER)) THEN BEGIN
    atlasName = "SUMER"
  ENDIF ELSE IF (keyword_set(HRTS)) THEN BEGIN
    atlasName = "HRTS"
  ENDIF ELSE BEGIN
    atlasName = "Hawaii_UV"
  ENDELSE

  state = UVWidgetSetup(ATLAS=atlasName)
  widget_control, state.baseWidget, /REALIZE, GROUP_LEADER=group_leader
  displayUVAtlas, setuvAtlas(state, atlasName)

  ;; --- Register with the XManager --                 -------------- ;;

  xmanager, 'XViewUVAtlas', state.baseWidget, $
   EVENT_HANDLER='XViewUVAtlas_Event', GROUP_LEADER=group_leader
END
; -------- end ---------------------------- XViewUVAtlas.pro --------- ;
