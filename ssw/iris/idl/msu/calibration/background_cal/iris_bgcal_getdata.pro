;+
; NAME:
;       iris_bgcal_getdata()
;
; PURPOSE:
;       Retrieves IRIS data Level 1 FUV data from the background
;       calibration observations.  Web data is accessed as default,
;       but JSOC can be used.
;
; CALLING SEQUENCE:
;       result = iris_bgcal_getdata(time_start, time_end, [index, data,
;                                  datadir=, /nosave, /jsoc])
;
; INPUTS:
;       time_start - rough start time for the observation
;       time_end   - rough end time for the observation
;
; OPTIONAL INPUTS:
;       
;
; KEYWORD PARAMETERS:
;       datadir - data directory for temporary and permanent storage
;                 of the data.  If nothing is specified then it uses
;                 the current working directory
;       /nosave - does not save data locally
;       /jsoc   - uses jsoc to access data, if jsoc=2 then jsoc2 is used
;
; OUTPUTS:
;       result - filename of the saved data, or if no data was saved,
;                1, showing the call was successful
;
; OPTIONAL OUTPUTS:
;       index, data - variables which return with the index structure
;                     and data array containing the data/metadata
;
; COMMON BLOCKS:
;       None
;
; PROCEDURES USED:
;       IRIS_TIME2FILES
;       SSW_JSOC_TIME2DATA
;
; COMMENTS:
;
; EXAMPLES:
;  IDL> time_start='2014-06-08T11:05:01' & time_end='2014-06-08T16:00:00'
;     > datadir='/Volumes/animal/iris/data/back_cal_data/'
;     > l1file=iris_bgcal_getdata(time_start, time_end, index, data, $
;     >                           datadir=datadir, /nosave)
;
; MODIFICATION HISTORY:
;       Started 2014-Jun-26 by Sarah A. Jaeggli, Montana State University
;               2015-Jan-07 SAJ changed routine to use iris_time2data
;               and sock_copy to get data, very large datasets are no
;               longer allowed by JSOC
;-

function iris_bgcal_getdata, time_start, time_end, index, data, $
                             datadir=datadir, nosave=nosave, jsoc=jsoc

  if keyword_set(datadir) ne 1 then datadir=''

  if keyword_set(jsoc) then begin
     ds='iris.lev1'
     if jsoc eq 2 then jsoc2=1 else jsoc2=0
     
     ssw_jsoc_time2data, time_start, time_end, index, data, $
                         ds=ds, jsoc2=jsoc2, /uncomp_delete, /comp_delete, $
                         xquery='img_path="FUV"', outdir_top=datadir
  endif else begin
     urls=iris_time2files(time_start, time_end, level=1, /urls, $
                          /jsoc, xquery='img_path="FUV"')
     
     spawn, 'mkdir '+datadir+'temp'
     sock_copy, urls, out_dir=datadir+'temp/'

     read_iris, file_search(datadir+'temp/*.fits'), index, data, /uncomp_delete

     spawn, 'rm -rf '+datadir+'temp'

;     good=where(index.img_path eq 'FUV')
;     index=index[good] & data=data[*,*,good]
  endelse
  
  if not keyword_set(nosave) then begin
     dt=index[0].date_obs
     timestring=strmid(dt, 0,4) + $ ;year
                strmid(dt, 5,2) + $ ;month
                strmid(dt, 8,2) + $ ;day
                '_' + $
                strmid(dt,11,2) + $ ;hour
                strmid(dt,14,2) + $ ;min
                strmid(dt,17,2)     ;sec

     savename=datadir+timestring+'_FUV_l1.sav'
  
     save, index, data, filename=savename
     print, 'Saving level 1 FUV background data in '+savename

     return, savename

  endif else return, 1

end
