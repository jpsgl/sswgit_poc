function iris_bgcal_fitld, x, y, z, s, rsun, verbose=verbose

  ;chuck bad data
  bad=where(s/z gt 3 or z lt 0., complement=good)

  x=x[good]  &  y=y[good]
  z=z[good]  &  s=s[good]

  dr=50.  ;initial step size
  rms=1.
  n=0

  if keyword_set(verbose) then begin
     xarray=findgen(240)*10.-1200.
     yarray=findgen(240)*10.-1200.

     triangulate, x, y, tr
     trinterp=trigrid(x, y, z, tr, xout=xarray, yout=yarray)

     window, 0
     plot, [0,100], [0,100], /nodata, yran=[-1200,1200], xran=[-1200,1200], $
           xstyle=1, ystyle=1, /isotropic
     imgunder, bytscl(trinterp)
     plot, [0,100], [0,100], /nodata, yran=[-1200,1200], xran=[-1200,1200], $
           xstyle=1, ystyle=1, /isotropic, /noerase

      oplot, x, y, psym=4, color=250 
  endif


  ;minimize deviation from the fit function to find best center
  while (dr gt 2.) do begin
     if n eq 0 then begin
        ;make an initial guess at the center of the light distribution
        x0=total(z*x)/total(z)
        y0=total(z*y)/total(z)

        r=sqrt((x-x0)^2 + (y-y0)^2)

        ;set up some parameters for mpfit
        a=[70., max(z)-min(z), min(z), rsun]
        parinfo=replicate({fixed:0},4)
        parinfo[3].fixed=1 ;size of the sun is fixed

        tmp=mpfitfun('iris_bgcal_func', r, z, s, a, $
                     parinfo=parinfo, yfit=yfit, /quiet)

        rms=sqrt(mean((z-yfit)^2))

        if keyword_set(verbose) then begin
           window, 1

           sorted=sort(r)

           plot, r, z, xstyle=1, psym=4
           oplot, r[sorted], yfit[sorted], color=250

           print, '        X0             Y0              Step Size     RMS'
           print, x0, y0
        endif
     endif

     ;test new coordinate pairs
     ;360 degree spiral in increments of 60 with a random phase
     x0_new=x0+randomn(seed)*dr
     y0_new=y0+randomn(seed)*dr

     r=sqrt((x-x0_new)^2 + (y-y0_new)^2)

     tmp=mpfitfun('iris_bgcal_func', r, z, s, a, $
                  parinfo=parinfo, yfit=yfit, /quiet)

     rms_new=sqrt(mean((z-yfit)^2))


     if rms_new lt rms then begin
        dr=sqrt((x0-x0_new)^2 + (y0-y0_new)^2)
        x0=x0_new
        y0=y0_new
        rms=rms_new

        if keyword_set(verbose) then begin
           wset, 0
           plot, [0,100], [0,100], /nodata, $
                 yran=[-1200,1200], xran=[-1200,1200], $
                 xstyle=5, ystyle=5, /isotropic, /noerase
           oplot, [x0], [y0], psym=1, color=250
           oplot, [x0_new], [y0_new], psym=3, color=250


           wset, 1
           r=sqrt((x-x0)^2 + (y-y0)^2)
           sorted=sort(r)

           plot, r, z, xstyle=1, psym=4
           oplot, r[sorted], yfit[sorted], color=250
           
           print, x0, y0, dr, rms
        endif
     endif

     n+=1
  endwhile

  return, {x0:x0, y0:y0, dw:tmp[0], amp:tmp[1], off:tmp[2]}

end
