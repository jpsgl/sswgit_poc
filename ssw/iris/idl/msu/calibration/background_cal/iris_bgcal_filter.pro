;filter the background image to remove noise
;use a butterworth filter of the form 1/(1+(R/Ro)^2)

function iris_bgcal_filter, img, width

  ;separate image into 4 domains
  x0=[0,    0,    2096, 2096]
  x1=[2060, 2060, 4143, 4143]
  y0=[20,   548,  20,   548 ]
  y1=[547,  1075, 547,  1075]

  filtered_img=img

  for i=0,n_elements(x0)-1 do begin
     tmp=img[x0[i]:x1[i],y0[i]:y1[i]]

     bad=where(finite(tmp) ne 1, nbad)
     if nbad ge 1 then tmp[bad]=median(tmp)
     
     ;flip an mirror image
     tmp=[[rotate(tmp,0),rotate(tmp,5)],[rotate(tmp,7),rotate(tmp,2)]]
     nx=(size(tmp))[1]
     ny=(size(tmp))[2]

     filter=1. / (1. + (dist(nx,ny)/width)^2)

     filtered_img[x0[i]:x1[i],y0[i]:y1[i]] = $
        (fft(fft(tmp,-1)*filter,1))[0:nx/2-1,0:ny/2-1]
  endfor

  return, filtered_img

end
