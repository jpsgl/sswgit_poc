;+
; NAME:
;       iris_bgcal_prep()
;
; PURPOSE:
;       Processes Level 1 data from the FUV background calibration
;       into a pseudo-Level 1.5 product.
;       How this is different from normal IRIS processing:
;           Darks obtained during the course of the calibration are
;           applied directly to the light data using the nearest in
;           time.  If no darks are available the IRIS_PREP dark is
;           used.  No geometry, flat, or background is applied, but
;           despiking is done.
;
; CALLING SEQUENCE:
;       result = iris_bgcal_prep(index, data, [filename=, datadir=, /pickfile])
;
; INPUTS:
;       filename - an IDL save file with the Level 1 data in the form
;                  of index, data
;
;      -or-
;
;       index, data - the structure and array of the Level 1 data
;                     supplied directly from the get_bgcal_data
;                     function
;
; OPTIONAL INPUTS:
;
; KEYWORD PARAMETERS:
;       /pickfile - instead of supplying a filename variable, the user
;                   can pick the file starting from the datadir path
;       datadir - data directory for temporary and permanent storage
;                 of the data.  If nothing is specified then it uses
;                 the current working directory
;
; OUTPUTS:
;       result - filename of the IDL save file containing the index
;                structure and data array of the processed data
;
; OPTIONAL OUTPUTS:
;       None
;
; COMMON BLOCKS:
;       None
;
; PROCEDURES USED:
;       IRIS_PREP
;       FIND_CLOSEST_TIME
;
; COMMENTS:
;
; EXAMPLES:
;  IDL> result=iris_bgcal_getdata(time_start, time_end, index, data, $
;     >                           datadir=datadir, /nosave)
;     > result=iris_bgcal_prep(index, data, datadir=datadir)
;
; MODIFICATION HISTORY:
;       Started 2014-Jun-26 by Sarah A. Jaeggli, Montana State University
;               2015-Jan-06 SAJ modified code to save dark images
;-

function iris_bgcal_prep, index, data, filename=filename, datadir=datadir, $
                          pickfile=pickfile

  if keyword_set(datadir) ne 1 then datadir='' 

  if keyword_set(pickfile) then $
     filename=dialog_pickfile(path=datadir, filter='*_l1.sav', $
                              title='Please select a L1 file to process')

  if keyword_set(filename) then restore, filename

  light=where(index.img_type eq 'LIGHT', lightcount)
  dark=where(index.img_type eq 'DARK', darkcount)

  if darkcount eq 0 then begin

     print, 'No darks found in this sequence, using IRIS_PREP dark correction'

     iris_prep, index, data, oindex, odata, $
                /nowarp, /noflat, /noback, /despike_here

     index=oindex
     data=odata

  endif else begin

     darkindex=index[dark]
     darkdata=data[*,*,dark]
     
     ;just run despike and saturation management
     iris_prep, index, data, oindex, odata, $
                /nowarp, /noflat, /nodark, /noback, /despike_here

     index=oindex
     data=odata

     didx=find_closest_time(index[light].date_obs, index[dark].date_obs)

     for i=0,lightcount-1 do $
        data[*,*,light[i]]=data[*,*,light[i]] - data[*,*,dark[didx[i]]]
     
     index=index[light]
     data=data[*,*,light]
  endelse

  dt=index[0].date_obs
  timestring=strmid(dt, 0,4) + $ ;year
             strmid(dt, 5,2) + $ ;month
             strmid(dt, 8,2) + $ ;day
             '_' + $
             strmid(dt,11,2) + $ ;hour
             strmid(dt,14,2) + $ ;min
             strmid(dt,17,2)     ;sec

  savename=datadir+timestring+'_FUV_l1p5.sav'

  save, index, data, filename=savename
  print, 'Saving level 1.5 FUV background data in '+savename

  if keyword_set(darkdata) then begin
     darkname=datadir+timestring+'_FUV_dark.sav'
     save, darkindex, darkdata, filename=darkname
     print, 'Saving FUV dark data in '+darkname
  endif

  return, savename

end
