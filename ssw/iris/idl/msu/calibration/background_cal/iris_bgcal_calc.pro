;+
; NAME:
;       iris_bgcal_calc()
;
; PURPOSE:
;       Takes the data produced by iris_bgcal_prep and determines the
;       background level in the FUV spectrograph for each filter
;       present during observation.  The intensity distribution as a
;       function of pointing and filter is fitted using a smoothed
;       limb darkening function.  The fitted parameters, along with a
;       filtered, average background image is saved as a fits file
;       suitable for insterion into the IRIS pipeline.
;
; CALLING SEQUENCE:
;       result=iris_bgcal_calc(filename=filename, [/pickfile, datadir=])
;
; INPUTS:
;       filename - an IDL save file with the Level 1 data in the form
;                  of index, data
;
; OPTIONAL INPUTS:
;
; KEYWORD PARAMETERS:
;       /pickfile - instead of supplying a filename variable, the user
;                   can pick the file starting from the datadir path
;       datadir - data directory for temporary and permanent storage
;                 of the data.  If nothing is specified then it uses
;                 the current working directory
;
; OUTPUTS:
;       result - filename of the FITS file containing the results
;
; OPTIONAL OUTPUTS:
;       None
;
; COMMON BLOCKS:
;       None
;
; PROCEDURES USED:
;       iris_bgcal_filter
;       iris_bgcal_fitld
;
; COMMENTS:
;
; EXAMPLES:
;  IDL> result1=iris_bgcal_getdata(time_start, time_end, index, data, $
;     >                            datadir=datadir, /nosave)
;     > result2=iris_bgcal_prep(index, data, datadir=datadir)
;     > result3=iris_bgcal_calc(filename=result2, datadir=datadir)
;
; MODIFICATION HISTORY:
;       Started 2014-June-26 by Sarah A. Jaeggli, Montana State University
;-

function iris_bgcal_calc, index, data, filename=filename, datadir=datadir, $
                          pickfile=pickfile

  if keyword_set(datadir) ne 1 then datadir=''

  if keyword_set(pickfile) then $
     filename=dialog_pickfile(path=datadir, filter='*_l1p5.sav', $
                              title='Please select a L1.5 file to process')

  if keyword_set(filename) then restore, filename

  ;determine the median background from the top of the FUV short beyond
  ;the edge of the slit
  x0=0    & x1=2045
  y0=1067 & y1=1071

  filt=[31,61,91,121]

  nimgs=n_elements(index)

  nx=index[0].naxis1
  ny=index[0].naxis2


  ;calculate pointing in terms of solar radius
  xx=index.xcen
  yy=index.ycen

  rsun=mean(index.rsun_obs)
  rv=sqrt(xx^2+yy^2)/rsun


  ;calculate background properties
  med=fltarr(nimgs)
  std=fltarr(nimgs)

  for i=0,nimgs-1 do begin
     ;turn data into a rate by dividing by exposure time
     data[*,*,i]/=index[i].exptime

     tmp=data[x0:x1,y0:y1,i]

     med[i] = mean(tmp, /nan)
     std[i] = stddev(tmp, /nan)
  endfor


  ;make an average image of the background from positions off the limb
  ;each image is normalized to the measured level from above
  good=where(rv gt 1.15, ngood)
  good_data=data[*,*,good]/rebin(reform(med[good],1,1,ngood),nx,ny,ngood)
  avg_back=median(good_data, dimension=3)

  ;filter noise from background image
  filt_back=iris_bgcal_filter(avg_back, 30)


  ;fit the shape of the background for each filter
  ;returns x,y center, amplitide, base level, smoothing gaussian for LD function
  for i=0,3 do begin   
     idx=where(index.ifwpos eq filt[i])
     tmp=iris_bgcal_fitld(xx[idx], yy[idx], med[idx], std[idx], rsun, /verb)

     if i eq 0 then soln=tmp else soln=[soln, tmp]
  endfor


  ;build a fits file
  fxhmake, hdr, filt_back

  spawn, 'echo $USER', creator
  spawn, 'echo $HOSTNAME', host
  fxaddpar, hdr, 'CREATOR', creator[0]+'@'+host[0]

  fxaddpar, hdr, 'ld31_0', soln[0].x0
  fxaddpar, hdr, 'ld31_1', soln[0].y0
  fxaddpar, hdr, 'ld31_2', soln[0].dw
  fxaddpar, hdr, 'ld31_3', soln[0].amp
  fxaddpar, hdr, 'ld31_4', soln[0].off

  fxaddpar, hdr, 'ld61_0', soln[1].x0
  fxaddpar, hdr, 'ld61_1', soln[1].y0
  fxaddpar, hdr, 'ld61_2', soln[1].dw
  fxaddpar, hdr, 'ld61_3', soln[1].amp
  fxaddpar, hdr, 'ld61_4', soln[1].off

  fxaddpar, hdr, 'ld91_0', soln[2].x0
  fxaddpar, hdr, 'ld91_1', soln[2].y0
  fxaddpar, hdr, 'ld91_2', soln[2].dw
  fxaddpar, hdr, 'ld91_3', soln[2].amp
  fxaddpar, hdr, 'ld91_4', soln[2].off

  fxaddpar, hdr, 'ld121_0', soln[3].x0
  fxaddpar, hdr, 'ld121_1', soln[3].y0
  fxaddpar, hdr, 'ld121_2', soln[3].dw
  fxaddpar, hdr, 'ld121_3', soln[3].amp
  fxaddpar, hdr, 'ld121_4', soln[3].off

  dt=index[0].date_obs
  timestring=strmid(dt, 0,4) + $  ;year
             strmid(dt, 5,2) + $  ;month
             strmid(dt, 8,2) + $  ;day
             '_' + $
             strmid(dt,11,2) + $ ;hour
             strmid(dt,14,2) + $ ;min
             strmid(dt,17,2)     ;sec

  savename=datadir+timestring+'_'+index[0].img_path+'_background.fits'

  fxwrite, savename, hdr, filt_back
  print, 'Saving background calibration in '+savename

  return, savename

end
