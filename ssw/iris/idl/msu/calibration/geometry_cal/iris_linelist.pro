;+
; NAME:
;       iris_linelist()
;
; PURPOSE:
;       Retrieve an IRIS line list for use in wavelength calibration
;
; CALLING SEQUENCE:
;       linelist = iris_linelist(wavelength)
;
; INPUTS:
;       A string describing the wavelength for which the line list is
;       desired.  The options are 'nuv', 'fuvs', and 'fuvl'
;
; OPTIONAL INPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None 
;
; OUTPUTS:
;       Returns a structure containing:
;         .wl         - a float array of the line wavelengths
;         .id         - a string array of the line IDs
;         .flag       - the flag that determines whether or no this
;                       line will be a default in the wavelength calibration
;
; OPTIONAL OUTPUTS:
;       None
;
; COMMON BLOCKS:
;       None
;
; PROCEDURES USED:
;
; COMMENTS:
;
; EXAMPLES:
;       To retrieve the NUV line list:
;
;          IDL> linelist = iris_linelist('nuv')
;
; MODIFICATION HISTORY:
;       Started 2013-June-3 by Sarah A. Jaeggli, Montana State University
;
;       2014-Jun-13 Updated some line wavelengths and IDs
;-

FUNCTION iris_linelist, wave

  CASE wave OF
     'FUV1' : BEGIN
        ;mostly from Sandlin 1986
        lines=[{wl:1333.475D, id:'H2     ', dw:0.10, flag:0}, $
               {wl:1333.792D, id:'S  I   ', dw:0.10, flag:0}, $
               {wl:1333.798D, id:'H2     ', dw:0.10, flag:0}, $
               {wl:1334.532D, id:'C  II  ', dw:0.20, flag:0}, $
               {wl:1335.203D, id:'Ni II  ', dw:0.10, flag:1}, $
               {wl:1335.708D, id:'C  II  ', dw:0.20, flag:0}, $
;               {wl:1337.840D, id:'?      ', dw:0.15, flag:0}, $
               {wl:1338.570D, id:'H2     ', dw:0.10, flag:0}, $
               {wl:1338.612D, id:'O  IV  ', dw:0.20, flag:0}, $
;               {wl:1339.570D, id:'?      ', dw:0.15, flag:0}, $
               {wl:1340.374D, id:'Ni II  ', dw:0.10, flag:0}, $
               {wl:1340.794D, id:'H2     ', dw:0.10, flag:0}, $
               {wl:1340.852D, id:'S  I   ', dw:0.10, flag:0}, $
;               {wl:1341.200D, id:'?      ', dw:0.15, flag:0}, $
               {wl:1341.465D, id:'Si III ', dw:0.10, flag:0}, $
               {wl:1341.889D, id:'Ca II  ', dw:0.10, flag:0}, $
               {wl:1342.257D, id:'H2     ', dw:0.10, flag:0}, $
;               {wl:1342.280D, id:'?      ', dw:0.15, flag:0}, $
               {wl:1342.535D, id:'Ca II  ', dw:0.10, flag:0}, $
               {wl:1342.894D, id:'H2     ', dw:0.10, flag:0}, $
               {wl:1343.250D, id:'S  III ', dw:0.10, flag:0}, $
               {wl:1343.640D, id:'?      ', dw:0.10, flag:0}, $
;               {wl:1345.160D, id:'?      ', dw:0.15, flag:0}, $
;               {wl:1345.510D, id:'?      ', dw:0.15, flag:0}, $
               {wl:1345.382D, id:'Fe II  ', dw:0.10, flag:0}, $
               {wl:1345.882D, id:'Ni II  ', dw:0.10, flag:0}, $
               {wl:1346.873D, id:'Si II  ', dw:0.10, flag:0}, $
               {wl:1347.240D, ig:'Cl I   ', dw:0.10, flag:0}, $
;               {wl:1347.060D, id:'?      ', dw:0.15, flag:1}, $
;               {wl:1348.790D, id:'?      ', dw:0.15, flag:0}, $
               {wl:1348.005D, id:'Fe I   ', dw:0.10, flag:0}, $
               {wl:1348.543D, id:'Si II  ', dw:0.10, flag:0}, $
               {wl:1349.400D, id:'Fe XII ', dw:0.20, flag:0}, $
;               {wl:1350.120D, id:'?      ', dw:0.15, flag:0}, $
               {wl:1350.057D, id:'Si II  ', dw:0.10, flag:0}, $
               {wl:1351.657D, id:'Cl I   ', dw:0.10, flag:1}, $
               {wl:1352.507D, id:'H2     ', dw:0.10, flag:0}, $
               {wl:1352.635D, id:'Si II  ', dw:0.10, flag:0}, $
               {wl:1352.750D, id:'C  I   ', dw:0.10, flag:0}, $
               {wl:1353.023D, id:'Fe II  ', dw:0.10, flag:0}, $
               {wl:1353.718D, id:'Si II  ', dw:0.10, flag:0}, $
               {wl:1354.013D, id:'Fe II  ', dw:0.10, flag:0}, $
               {wl:1354.080D, id:'Fe XXI ', dw:0.20, flag:0}, $
               {wl:1354.288D, id:'C  I   ', dw:0.10, flag:1}, $
               {wl:1354.747D, id:'Fe II  ', dw:0.10, flag:0}, $
;               {wl:1354.800D, id:'?      ', dw:0.15, flag:0}, $
               {wl:1355.598D, id:'O  I   ', dw:0.10, flag:1}, $
               {wl:1355.844D, id:'C  I   ', dw:0.10, flag:1}, $
               {wl:1356.470D, id:'H2     ', dw:0.10, flag:0}, $
               {wl:1356.858D, id:'H2     ', dw:0.10, flag:0}, $
               {wl:1357.134D, id:'C  I   ', dw:0.10, flag:1}, $
               {wl:1357.659D, id:'C  I   ', dw:0.10, flag:1}, $
               {wl:1358.188D, id:'C  I   ', dw:0.10, flag:0}, $
               {wl:1358.512D, id:'O  I   ', dw:0.10, flag:0}]
     END

     'FUV2' : BEGIN
        lines=[{wl:1388.435D, id:'S  I  ', dw:0.10, flag:0}, $
               {wl:1389.154D, id:'S  I  ', dw:0.10, flag:0}, $
               {wl:1389.693D, id:'Cl I  ', dw:0.10, flag:0}, $
               {wl:1389.957D, id:'Cl I  ', dw:0.10, flag:0}, $
               {wl:1389.560D, id:'K  XII', dw:0.20, flag:0}, $
               {wl:1390.318D, id:'Fe II ', dw:0.10, flag:0}, $
               {wl:1391.309D, id:'Fe II ', dw:0.10, flag:0}, $
               {wl:1392.149D, id:'Fe II ', dw:0.10, flag:1}, $
               {wl:1392.588D, id:'S  I  ', dw:0.10, flag:1}, $
               {wl:1392.817D, id:'Fe II ', dw:0.10, flag:1}, $
               {wl:1393.330D, id:'Ni II ', dw:0.10, flag:1}, $
               {wl:1393.451D, id:'H2    ', dw:0.10, flag:0}, $
               {wl:1393.510D, id:'CO    ', dw:0.10, flag:0}, $
               {wl:1393.755D, id:'Si IV ', dw:0.20, flag:0}, $
               {wl:1394.713D, id:'Fe II ', dw:0.10, flag:0}, $
               {wl:1395.200D, id:'H2    ', dw:0.10, flag:0}, $
               {wl:1396.112D, id:'S  I  ', dw:0.10, flag:1}, $
               {wl:1396.223D, id:'H2    ', dw:0.10, flag:0}, $
               {wl:1396.527D, id:'Cl  I ', dw:0.10, flag:0}, $
               {wl:1396.740D, id:'CO    ', dw:0.10, flag:0}, $
               {wl:1396.990D, id:'CO    ', dw:0.10, flag:0}, $
               {wl:1397.200D, id:'O  IV ', dw:0.20, flag:0}, $
               {wl:1397.421D, id:'H2    ', dw:0.10, flag:0}, $
               {wl:1397.572D, id:'Fe II ', dw:0.10, flag:0}, $
               {wl:1397.845D, id:'Fe II ', dw:0.10, flag:0}, $
               {wl:1398.130D, id:'S  IV ', dw:0.20, flag:0}, $
               {wl:1398.780D, id:'?     ', dw:0.10, flag:0}, $
               {wl:1398.957D, id:'H2    ', dw:0.10, flag:0}, $
               {wl:1399.026D, id:'Ni II ', dw:0.10, flag:1}, $
               {wl:1399.774D, id:'O  IV ', dw:0.20, flag:0}, $
               {wl:1399.962D, id:'Fe II ', dw:0.10, flag:0}, $
               {wl:1400.611D, id:'H2    ', dw:0.10, flag:0}, $
               {wl:1401.156D, id:'O  IV ', dw:0.20, flag:0}, $
               {wl:1401.514D, id:'S  I  ', dw:0.10, flag:1}, $
               {wl:1401.772D, id:'Fe II ', dw:0.10, flag:0}, $
               {wl:1402.770D, id:'Si IV ', dw:0.20, flag:0}, $
               {wl:1403.381D, id:'H2    ', dw:0.10, flag:0}, $
               {wl:1403.982D, id:'H2    ', dw:0.10, flag:0}, $
               {wl:1404.119D, id:'Fe II ', dw:0.10, flag:0}, $
               {wl:1404.770D, id:'S  IV ', dw:0.20, flag:0}, $
               {wl:1404.812D, id:'O  IV ', dw:0.20, flag:0}, $
               {wl:1405.608D, id:'Fe II ', dw:0.10, flag:1}, $
               {wl:1405.800D, id:'Fe II ', dw:0.10, flag:0}, $
               {wl:1406.060D, id:'S  IV ', dw:0.10, flag:0}]
     END
     
     'NUV' : BEGIN
        ;Based on IS 0181 by Tiago Pereira
        lines=[{wl:2784.643D, id:'Ti II', dw:0.20, flag:0}, $
               {wl:2785.692D, id:'Cr II', dw:0.20, flag:0}, $
               {wl:2786.473D, id:'Cr II', dw:0.20, flag:0}, $
               {wl:2788.105D, id:'Fe I ', dw:0.20, flag:1}, $
               {wl:2791.504D, id:'Fe I ', dw:0.20, flag:0}, $
               {wl:2791.786D, id:'Fe I ', dw:0.20, flag:1}, $
               {wl:2792.399D, id:'Fe I ', dw:0.20, flag:1}, $
               {wl:2794.817D, id:'Mn I ', dw:0.20, flag:1}, $
               {wl:2795.005D, id:'Fe I ', dw:0.20, flag:0}, $
               {wl:2797.775D, id:'Fe I ', dw:0.20, flag:0}, $
               {wl:2798.269D, id:'Mn I ', dw:0.20, flag:1}, $
               {wl:2798.649D, id:'Ni I ', dw:0.20, flag:0}, $
               {wl:2798.962D, id:'Fe I ', dw:0.20, flag:0}, $
               {wl:2799.147D, id:'Fe I ', dw:0.20, flag:0}, $
               {wl:2799.295D, id:'Fe II', dw:0.20, flag:0}, $
               {wl:2799.839D, id:'Mn I ', dw:0.20, flag:1}, $
               {wl:2800.758D, id:'Cr I ', dw:0.20, flag:0}, $
               {wl:2801.081D, id:'Mn I ', dw:0.20, flag:1}, $
               {wl:2803.613D, id:'Fe I ', dw:0.20, flag:0}, $
               {wl:2804.520D, id:'Fe I ', dw:0.20, flag:1}, $
               {wl:2804.863D, id:'Fe I ', dw:0.20, flag:0}, $
               {wl:2805.077D, id:'Ni I ', dw:0.20, flag:1}, $
               {wl:2805.807D, id:'Fe I ', dw:0.20, flag:1}, $
               {wl:2806.070D, id:'Fe I ', dw:0.20, flag:0}, $
               {wl:2806.984D, id:'Fe I ', dw:0.20, flag:1}, $
               {wl:2808.335D, id:'Fe I ', dw:0.20, flag:1}, $
               {wl:2809.756D, id:'C  I ', dw:0.20, flag:1}, $
               {wl:2813.287D, id:'Fe I ', dw:0.20, flag:1}, $
               {wl:2814.350D, id:'Ni I ', dw:0.20, flag:1}, $
               {wl:2815.181D, id:'Ni I ', dw:0.20, flag:0}, $
               {wl:2821.289D, id:'Ni I ', dw:0.20, flag:1}, $
               {wl:2824.394D, id:'Fe I ', dw:0.20, flag:1}]
     END
  ENDCASE

  RETURN, lines

END
