;+
; NAME:
;       find_closest_time()
;
; PURPOSE:
;       A function to determine index of the nearest time in an array of
;       reference times to a given time (or array of times).
;
; CALLING SEQUENCE:
;       image = iris_get_back(index)
;
; INPUTS:
;       time     - The time string or array of stings where the
;                  nearest reference is desired
;       ref_time - The reference array of time stings
;
;            NOTE: stings should be given in terms of
;                  YYYY-MM-DDThh:mm:ss.sss, other formats that are
;                  converted by date_conv may also work.
;
; OPTIONAL INPUTS:
;
; KEYWORD PARAMETERS:
;
; OUTPUTS:
;       Returns the index or array of indices or the same dimensions
;       as 'time' for the nearest element in ref_time.
;
; OPTIONAL OUTPUTS:
;       None
;
; COMMON BLOCKS:
;       None
;
; PROCEDURES USED:
;       date_conv
;       find_closest
;
; COMMENTS:
;       Time stings are converted into hours referenced to the first
;       element in 'ref_time' assuimg the desired times are within a
;       few days of each other, using times which are more widely
;       spaced, by months or years may have some issues with accuracy
;       or unexpected results.
;
; EXAMPLES:
;          IDL> indices = find_closest_time(time, ref_time)
;
; MODIFICATION HISTORY:
;       Started 2014-Apr-03 by Sarah A. Jaeggli, Montana State University
;-

function find_closest_time, time, ref_time

  nt=n_elements(time)
  nr=n_elements(ref_time)

  tr=dblarr(nr)
  tt=dblarr(nt)
  idx=intarr(nt)


  ;convert reference time to hours
  for i=0,nr-1 do begin
     t = date_conv(ref_time[i], 'VECTOR')

     ;use the first reference time as an absolute reference
     if i eq 0 then t0=double(t)
     t=double(t-t0)

     tr[i]=(t[0]*365D + t[1])*24D + t[2] + (t[3] + t[4]/60D)/60D
  endfor


  ;convert times to hours and find index of closest reference time
  for i=0,nt-1 do begin
     t = date_conv(time[i], 'VECTOR')
     t = double(t-t0)

     tt[i]=(t[0]*365D + t[1])*24D + t[2] + (t[3] + t[4]/60D)/60D

     idx[i] = find_closest(tt[i], tr)
  endfor

  return, idx

end
