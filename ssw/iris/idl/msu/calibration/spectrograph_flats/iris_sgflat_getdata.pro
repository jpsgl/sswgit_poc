;+
; NAME:
;       iris_sgflat_getdata()
;
; PURPOSE:
;       Retrieves IRIS data Level 1 NUV or FUV data from the
;       spectrograph calibration observations via the JSOC service,
;       JSOC2 can be used for authorized users
;
; CALLING SEQUENCE:
;       result = iris_sgflat_getdata(time_start, time_end [, index, data,
;                                    datadir=, /nosave, /jsoc2])
;
; INPUTS:
;       time_start - rough start time for the observation
;       time_end   - rough end time for the observation
;
; OPTIONAL INPUTS:
;       
;
; KEYWORD PARAMETERS:
;       datadir - data directory for temporary and permanent storage
;                 of the data.  If nothing is specified then it uses
;                 the current working directory
;       /nosave - does not save data locally
;       /jsoc   - uses jsoc to access data, if jsoc=2 then jsoc2 is used
;
; OUTPUTS:
;       result - filename of the saved data, or if no data was saved,
;                1, showing the call was successful
;
; OPTIONAL OUTPUTS:
;       index, data - variables which return with the index structure
;                     and data array containing the data/metadata
;
; COMMON BLOCKS:
;       None
;
; PROCEDURES USED:
;       SSW_JSOC_TIME2DATA
;
; COMMENTS:
;
; EXAMPLES:
;  IDL> time_start='2014-05-23T01:00:00' & time_end='2014-05-23T03:08:37'
;     > datadir='/Volumes/animal/iris/data/sgflat_data/'
;     > result=iris_sgflat_getdata(time_start, time_end, index, data, $
;     >                            datadir=datadir, /nosave)
;
; MODIFICATION HISTORY:
;       Started 2014-June-30 by Sarah A. Jaeggli, Montana State University
;               2015-Feb-12 SAJ changed routine to use iris_time2data
;               and sock_copy to get data, very large datasets are no
;               longer allowed by JSOC
;-

function iris_sgflat_getdata, time_start, time_end, index, data, jsoc=jsoc, $
                              datadir=datadir, fuv=fuv, nuv=nuv, nosave=nosave

  if keyword_set(datadir) ne 1 then datadir='' 

  if keyword_set(fuv) then img_path='FUV'

  if keyword_set(nuv) then img_path='NUV'

  if keyword_set(fuv) ne 1 and keyword_set(nuv) ne 1 then begin
     print, 'Please specify image path keyword'
     return, 0
  endif

  if keyword_set(jsoc) then begin
     ds='iris.lev1'
     if jsoc eq 2 then jsoc2=1 else jsoc2=0
     
     ssw_jsoc_time2data, time_start, time_end, index, data, $
                         ds=ds, jsoc2=jsoc2, /uncomp_delete, /comp_delete, $
                         xquery='img_path="'+img_path+'"', outdir_top=datadir
  endif else begin
     urls=iris_time2files(time_start, time_end, level=1, /urls, $
                          /jsoc, xquery='img_path="'+img_path+'"')
     
     spawn, 'mkdir '+datadir+'temp'
     sock_copy, urls, out_dir=datadir+'temp/'

     read_iris, file_search(datadir+'temp/*.fits'), index, data, /uncomp_delete

     spawn, 'rm -rf '+datadir+'temp'
  endelse


  dt=index[0].date_obs
  timestring=strmid(dt, 0,4) + $  ;year
             strmid(dt, 5,2) + $  ;month
             strmid(dt, 8,2) + $  ;day
             '_' + $
             strmid(dt,11,2) + $ ;hour
             strmid(dt,14,2) + $ ;min
             strmid(dt,17,2)     ;sec

  savename=datadir+timestring+'_'+img_path+'_l1.sav'

  if not keyword_set(nosave) then begin
     save, index, data, filename=savename
     print, 'Saving level 1 '+img_path+' flat data in '+savename

     return, savename

  endif else return, 1

end
