;+
; NAME:
;       iris_sgflat_prep()
;
; PURPOSE:
;       Processes Level 1 data from the NUV or FUV flatfield series
;       into a pseudo-Level 1.5 product.
;       How this is different from normal IRIS processing:
;             Data is dark subtracted.  In the case of the FUV, darks
;             are taken with the sequence, so the nearest dark in time
;             is applied to each image.  FUV data is also background
;             subtracted, if a background file is supplied this
;             correction, otherwise the IRIS prep correction is used.
;             No geometry transformation is done at this step.
;
; CALLING SEQUENCE:
;       result = iris_sgflat_prep(index, data, [filename=, datadir=, bgfile=, /pickfile])
;
; INPUTS:
;       filename - an IDL save file with the Level 1 data in the form
;                  of index, data
;
;      -or-
;
;       index, data - the structure and array of the Level 1 data
;                     supplied directly from the get_geocal_data
;                     function
;
; OPTIONAL INPUTS:
;
; KEYWORD PARAMETERS:
;       /pickfile - instead of supplying a filename variable, the user
;                   can pick the file starting from the datadir path
;       datadir - data directory for temporary and permanent storage
;                 of the data.  If nothing is specified then it uses
;                 the current working directory
;       bgfile - can be specified as a sting containing the file name
;                or can be set as a keyword which allows the user to
;                pick the file interactively.
;
; OUTPUTS:
;       result - filename of the IDL save file containing the index
;                structure and data array of the processed data
;
; OPTIONAL OUTPUTS:
;       None
;
; COMMON BLOCKS:
;       None
;
; PROCEDURES USED:
;       IRIS_PREP
;
; COMMENTS:
;
; EXAMPLES:
;  IDL> result=iris_sgflat_getdata(time_start, time_end, index, data, $
;     >                            datadir=datadir, /nosave, /jsoc2)
;     > result=iris_sgflat_prep(index, data, datadir=datadir, /bgfile)
;
; MODIFICATION HISTORY:
;       Started 2014-June-30 by Sarah A. Jaeggli, Montana State University
;               2015-Jan-06 SAJ modified code to save dark images
;-

function iris_sgflat_prep, index, data, filename=filename, datadir=datadir, $
                           bgfile=bgfile, pickfile=pickfile

  data=float(data)

  if keyword_set(datadir) ne 1 then datadir=''

  if keyword_set(pickfile) then $
     filename=dialog_pickfile(path=datadir, filter='*_l1.sav', $
                              title='Please select a L1 file to process')

  if keyword_set(filename) then restore, filename

  light=where(index.img_type eq 'LIGHT', lightcount)
  dark=where(index.img_type eq 'DARK', darkcount)

  ;if background file is available do that first
  if typename(bgfile) eq 'INT' then $
     bgfile=dialog_pickfile(path=datadir, filter='*background.fits', $
                            title='Please select an FUV background file')

  if keyword_set(bgfile) then begin
     for i=0,lightcount-1 do $
        data[*,*,light[i]]-=iris_get_back(index[light[i]], bgfile=bgfile)
     noback=1
  endif else noback=0

  if darkcount eq 0 then begin
     
     print, 'No darks found in this sequence, using IRIS_PREP dark correction'

     iris_prep, index, data, oindex, odata, $
                /nowarp, /noflat, /despike_here, noback=noback

     index=oindex
     data=odata

  endif else begin

     ;identify dark image taken nearest in time
     didx=find_closest_time(index[light].t_obs, index[dark].t_obs)

     for i=0,lightcount-1 do $
        data[*,*,light[i]]=data[*,*,light[i]] - data[*,*,dark[didx[i]]]

     index=index[light]
     data=data[*,*,light]
   
     iris_prep, index, data, oindex, odata, $
                /nowarp, /noflat, /nodark, /despike_here, noback=noback

     index=oindex
     data=odata

     darkindex=index[dark]
     darkdata=data[*,*,dark]
  endelse

  dt=index[0].date_obs
  timestring=strmid(dt, 0,4) + $ ;year
             strmid(dt, 5,2) + $ ;month
             strmid(dt, 8,2) + $ ;day
             '_' + $
             strmid(dt,11,2) + $ ;hour
             strmid(dt,14,2) + $ ;min
             strmid(dt,17,2)     ;sec

  savename=datadir+timestring+'_'+index[0].img_path+'_l1p5.sav'

  save, index, data, filename=savename
  print, 'Saving level 1.5 '+index[0].img_path+' flat data in '+savename

  if keyword_set(darkdata) then begin
     darkname=datadir+timestring+'_'+index[0].img_path+'_dark.sav'
     save, darkindex, darkdata, filename=darkname
     print, 'Saving dark data in '+darkname
  endif
  
  return, savename

end
