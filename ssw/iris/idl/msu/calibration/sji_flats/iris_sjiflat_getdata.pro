;+
; NAME:
;       iris_sjiflat_getdata()
;
; PURPOSE:
;       Retrieves IRIS data Level 1 SJI data from the slit-jaw flat field
;       observations via the JSOC service, JSOC2 can be used for
;       authorized users. All SJI channels are saved in the same Level
;       1 file at this stage.
;
; CALLING SEQUENCE:
;       result = iris_sjiflat_getdata(time_start, time_end [, index, data,
;                                     datadir=, /nosave, /jsoc,
;                                     img_path=img_path])
;
; INPUTS:
;       time_start - rough start time for the observation
;       time_end   - rough end time for the observation
;
; OPTIONAL INPUTS:
;
; KEYWORD PARAMETERS:
;       datadir  - data directory for temporary and permanent storage
;                  of the data.  If nothing is specified then it uses
;                  the current working directory
;       /nosave  - does not save data locally
;       img_path - allows specification of the image path to get data
;                  from just one filter (for debugging purposes)
;       /jsoc    - uses jsoc to access data, if jsoc=2 then jsoc2 is used
;
; OUTPUTS:
;       result - filename of the saved data, or if no data was saved,
;                1, showing the call was successful
;
; OPTIONAL OUTPUTS:
;       index, data - variables which return with the index structure
;                     and data array containing the data/metadata
;
; COMMON BLOCKS:
;       None
;
; PROCEDURES USED:
;       SSW_JSOC_TIME2DATA
;
; COMMENTS:
;
; EXAMPLES:
;  IDL> time_start='2014-05-22T17:00:00' & time_end='2014-05-22T17:46:40'
;     > datadir='/Volumes/animal/iris/data/sgflat_data/'
;     > result=iris_sjiflat_getdata(time_start, time_end, index, data, $
;     >                             datadir=datadir, /nosave, /jsoc2)
;
; MODIFICATION HISTORY:
;       Started 2014-Jul-1 by Sarah A. Jaeggli, Montana State University
;
;               2014-Sep-2 added img_path keyword for debugging
;-

function iris_sjiflat_getdata, time_start, time_end, index, data, $
                               datadir=datadir, nosave=nosave, jsoc2=jsoc2, $
                               img_path=img_path

  if keyword_set(datadir) ne 1 then datadir=''

  ds='iris.lev1'

  if keyword_set(img_path) then $
     xquery='IMG_PATH="'+img_path+'"' $
  else begin
     xquery=''
     img_path='SJI'
  endelse

  
  if keyword_set(jsoc) then begin
     ds='iris.lev1'
     if jsoc eq 2 then jsoc2=1 else jsoc2=0
     
     ssw_jsoc_time2data, time_start, time_end, index, data, $
                         ds=ds, jsoc2=jsoc2, /uncomp_delete, /comp_delete, $
                         xquery=xquery, outdir_top=datadir
  endif else begin
     urls=iris_time2files(time_start, time_end, level=1, /urls, $
                          /jsoc, xquery=xquery)
     
     spawn, 'mkdir '+datadir+'temp'
     sock_copy, urls, out_dir=datadir+'temp/'

     read_iris, file_search(datadir+'temp/*.fits'), index, data, /uncomp_delete

     spawn, 'rm -rf '+datadir+'temp'
  endelse

  
  dt=index[0].date_obs
  timestring=strmid(dt, 0,4) + $  ;year
             strmid(dt, 5,2) + $  ;month
             strmid(dt, 8,2) + $  ;day
             '_' + $
             strmid(dt,11,2) + $ ;hour
             strmid(dt,14,2) + $ ;min
             strmid(dt,17,2)     ;sec

  savename=datadir+timestring+'_'+img_path+'_l1.sav'

  if not keyword_set(nosave) then begin
     save, index, data, filename=savename
     print, 'Saving level 1 '+img_path+' flat data in '+savename

     return, savename

  endif else return, 1

end
