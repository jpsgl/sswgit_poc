;+
; NAME:
;       iris_stitch_slit()
;
; PURPOSE:
;       For smoothing over the slit in the SJI flat field.  By
;       removing the slit here, we ensure that the flat remains in the
;       Level 2 data (for reference).  Plus there are all sorts of
;       nasty artifacts that would get introduced if we left the slit
;       in because it is very, very dark and experiences thermal drift.
;
; CALLING SEQUENCE:
;       result=iris_stitch_slit(flat1, flat2, [width=, mask=, slitpos=])
;
; INPUTS:
;       flat1 - the flat to fix
;       flat2 - the reference flat
;
; OPTIONAL INPUTS:
;
; KEYWORD PARAMETERS:
;       width   - number of pixels around the slit to exclude
;       mask    - bad pixel mask
;       slitpos - average x position of the slit in pixels
;
; OUTPUTS:
;       result - the image with the slit removed
;
; OPTIONAL OUTPUTS:
;       None
;
; COMMON BLOCKS:
;       None
;
; PROCEDURES USED:
;
; COMMENTS:
;
; EXAMPLES:
;   IDL > result=iris_stitch_slit(chaeflat, ccdflat, width=12,
;                                 mask=mask, slitps=index.crpix)
;
; MODIFICATION HISTORY:
;       Started 2014-Jul-1 by Sarah A. Jaeggli, Montana State University
;               2015-Feb-2 improvement to slit fitting robustness
;-

function iris_stitch_slit, flat1, flat2, width=width, mask=mask, slitpos=slitpos

  nx=(size(flat1))[1]
  ny=(size(flat1))[2]

  if not keyword_set(width) then width=7
  if not keyword_set(slitpos) then slitpos=nx/2.

  ;do some fitting at the center of the image to get the slit position
  xpos=fltarr(ny)
  ypos=findgen(ny)

  for j=0,ny-1 do begin
     xx=findgen(200)-100 + slitpos
     ii=flat1[xx,j]

     yfit=gaussfit(xx, ii, a, nterms=5)
     xpos[j]=a[1]
  endfor

;  a=linfit(ypos, xpos)
  a=robust_poly_fit(ypos, xpos, 1)
  
  xarray=rebin(reform(findgen(nx), nx, 1), nx, ny)
  yarray=rebin(reform(findgen(ny), 1, ny), nx, ny)

  farray=yarray*a[1]+a[0]

  slit_idxs=where(xarray gt farray-width and $
                  xarray lt farray+width)

  flat3=flat1/flat2

  flat3[slit_idxs]=!values.f_nan
  if keyword_set(mask) then flat3[where(mask eq 0)]=!values.f_nan

;  good=where(finite(flat3) eq 1, complement=bad)

  ;smooth iteratively over slit (and other bad stuff)
;  tmp=smooth(flat3, width*2, /edge_truncate, /nan)
;  tmp=smooth(tmp, width*2, /edge_truncate, /nan)

;  tmp=grid_tps(xarray[good], yarray[good], flat3[good], ngrid=[nx,ny])

  fill_missing, flat3, !values.f_nan, 1

  flat4=flat1
  flat4[slit_idxs]=flat3[slit_idxs]*flat2[slit_idxs]

  return, flat4

end
