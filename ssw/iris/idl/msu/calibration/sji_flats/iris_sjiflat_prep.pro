;+
; NAME:
;       iris_sjiflat_prep()
;
; PURPOSE:
;       Processes Level 1 data from a single SJI flatfield series
;       into a pseudo-Level 1.5 product.  Each SJI filter in the
;       sequence is treated separately in processing and has its own
;       save file.
;       How this is different from normal IRIS processing:
;             Data is dark subtracted.  In modern flat sequences the darks
;             are taken with the sequence, so the nearest dark in time
;             is applied to each image.  The image is not corrected
;             for flat or geometry, but despiking is done.
;
; CALLING SEQUENCE:
;       result = iris_sjiflat_prep(index, data, [filename=, datadir=, $
;                                  /pickfile])
;
; INPUTS:
;       filename - an IDL save file with the Level 1 data in the form
;                  of index, data
;
;      -or-
;
;       index, data - the structure and array of the Level 1 data
;                     supplied directly from the get_geocal_data
;                     function
;
; OPTIONAL INPUTS:
;
; KEYWORD PARAMETERS:
;       /pickfile - instead of supplying a filename variable, the user
;                   can pick the file starting from the datadir path
;       datadir - data directory for temporary and permanent storage
;                 of the data.  If nothing is specified then it uses
;                 the current working directory
;
; OUTPUTS:
;       result - filenames of the IDL save file containing the index
;                structure and data array of the processed data
;
; OPTIONAL OUTPUTS:
;       None
;
; COMMON BLOCKS:
;       None
;
; PROCEDURES USED:
;       IRIS_PREP
;
; COMMENTS:
;
; EXAMPLES:
;  IDL> result=iris_sjiflat_getdata(time_start, time_end, index, data, $
;     >                             datadir=datadir, /nosave, /jsoc2)
;     > result=iris_sjiflat_prep(index, data, datadir=datadir)
;
; MODIFICATION HISTORY:
;       Started 2014-Jul-1 by Sarah A. Jaeggli, Montana State University
;
;               2014-Sep-2 now include the /nobad keyword to avoid
;               setting pixels to zero
;
;               2014-Dec-17 fixed processing redundant paths
;-

function iris_sjiflat_prep, index, data, filename=filename, datadir=datadir, $
                            pickfile=pickfile

  if keyword_set(datadir) ne 1 then datadir=''

  if keyword_set(pickfile) then $
     filename=dialog_pickfile(path=datadir, filter='*_l1.sav', $
                             title='Please select a L1 file to process')

  if keyword_set(filename) then restore, filename

  index0=index
  data0=data

  ;identify the different image paths/filters
  paths=index0[uniq(index0[sort(index0.img_path)].img_path)].img_path
  
  npaths=n_elements(paths)

  savename=strarr(npaths)

  for n=0,npaths-1 do begin

     light = where(index0.img_type eq 'LIGHT' and $
                   index0.img_path eq paths[n], lightcount)
     dark  = where(index0.img_type eq 'DARK'  and $
                   index0.img_path eq paths[n], darkcount)

     if darkcount eq 0 then begin
     
        print, 'No darks found in this sequence, using IRIS_PREP dark'

        iris_prep, index0[light], data0[*,*,light], lindex, ldata, $
                   /nowarp, /noflat, /despike_here, /nobad

     endif else begin

        ;sji flat images have darks as "bookends"
        ;identify and use dark image taken nearest in time
        didx=find_closest_time(index0[light].t_obs, index0[dark].t_obs)

        iris_prep, index0[dark], data0[*,*,dark], dindex, ddata, $
                   /nowarp, /noflat, /nodark, /nobad, /despike_here
        
        iris_prep, index0[light], data0[*,*,light], lindex, ldata, $
                   /nowarp, /noflat, /nodark, /nobad, /despike_here

        ldata=ldata - ddata[*,*,didx]
     endelse

     index=lindex
     data=ldata
     
     dt=index[0].date_obs
     timestring=strmid(dt, 0,4) + $ ;year
                strmid(dt, 5,2) + $ ;month
                strmid(dt, 8,2) + $ ;day
                '_' + $
                strmid(dt,11,2) + $ ;hour
                strmid(dt,14,2) + $ ;min
                strmid(dt,17,2)     ;sec

     savename[n]=datadir+timestring+'_'+paths[n]+'_l1p5.sav'

     print, 'Saving level 1.5 '+paths[n]+' flat data in '+savename[n]
     save, index, data, filename=savename[n]
  endfor

  return, savename

end
