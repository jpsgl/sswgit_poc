;+
; NAME:
;       iris_sjiflat_finalize()
;
; PURPOSE:
;       For making the SJI final flat field.
;       The Chae flats produced by the three sequences taken for each
;       SJI flat can be combined here.  For each flat the slit is
;       smoothed over using data from the preflight lamp flats.  Then
;       the flats are averaged together.  Noise statistics are
;       calculated based on the individual and final flats.
;
; CALLING SEQUENCE:
;       result=iris_sjiflat_finalize(filename=filename, [/pickfile,
;                                    datadir=, /geofile])
;
; INPUTS:
;       filename - the path and name of an IDL save file with the
;                  Chae flat field data.  If the user supplies an
;                  array, then each file is processed *but they must
;                  be for the same SJI filter*.
;
; OPTIONAL INPUTS:
;
; KEYWORD PARAMETERS:
;       /pickfile - instead of supplying a filename variable, the user
;                   can pick the file starting from the datadir path
;       datadir - data directory for temporary and permanent storage
;                 of the data.  If nothing is specified then it uses
;                 the current working directory
;
; OUTPUTS:
;       result - filename of the IDL save file containing the results
;                which will be passed to the final stage of processing
;                where multiple flatfield results are combined.
;
; OPTIONAL OUTPUTS:
;       None
;
; COMMON BLOCKS:
;       None
;
; PROCEDURES USED:
;       iris_stitch_slit
;
; COMMENTS:
;
; EXAMPLES:
;   IDL > result=iris_sjiflat_finalize(filename = files, datadir = datadir, $
;                                      /pickfile, img_path='SJI_1330')
;
; MODIFICATION HISTORY:
;       Started 2014-Jul-1 by Sarah A. Jaeggli, Montana State University
;-

function iris_sjiflat_finalize, filename=filename, datadir=datadir, $
                                pickfile=pickfile, img_path=img_path

  if keyword_set(datadir) ne 1 then datadir=''

  if keyword_set(pickfile) then $
     filename=dialog_pickfile(path=datadir, /multiple_files, $
                              filter='*'+img_path+'_chaeflat.sav', $
                              title='Pick Chae flats to combine')

  if keyword_set(filename) then nflats=n_elements(filename)


  ;restore ccd flat
  if img_path eq 'SJI_5000W' or $
     img_path eq 'SJI_2832'  or $
     img_path eq 'SJI_2796'  then rotidx=0 else rotidx=5

  ccd_flat_file = concat_dir(concat_dir('$SSW_IRIS','data'), $
                             'SJI_flat.fits')
  ccd_flat=rotate(readfits(ccd_flat_file), rotidx)


  for i=0,nflats-1 do begin
     restore, filename[i]

     mask=flat*0. + 1.
     mask[where((flat lt 0.5) or (finite(flat) ne 1))]=0.

     tmp=iris_stitch_slit(flat, ccd_flat, width=12, mask=mask, $
                          slitpos=index[0].crpix1)

     if i eq 0 then begin
        flat_slit=flat
        flat_noslit=tmp

        nx=(size(flat))[1]
        ny=(size(flat))[2]

        ;get the year/month/day of the first file for naming
        dt=index[0].date_obs
        timestring=strmid(dt, 0,4) + $ ;year
                   strmid(dt, 5,2) + $ ;month
                   strmid(dt, 8,2)     ;day

        window, 0, xsize=nx, ysize=ny
     endif else begin
        flat_slit   = [[[flat_slit]],  [[flat]]]
        flat_noslit = [[[flat_noslit]], [[tmp]]]
     endelse

     tv, bytscl(tmp, 0, 1.5)
     wait, 1
  endfor


  ;sum over series
  flats=rebin(flat_slit,   nx, ny)
  flatn=rebin(flat_noslit, nx, ny)

  ;estimate errors
  err=fltarr(nflats)
  for i=0,nflats-1 do err[i]=stddev(flatn - flat_noslit[*,*,i], /nan)
  
  stddev1 = mean(err, /nan) / sqrt(1.+1./nflats)
  stddev2 = stddev1 / sqrt(nflats)

  print, 'mean stddev per flat:', stddev1
  print, 'stddev of final flat:', stddev2

  tv, bytscl(flatn, 0, 1.5)


  ;write fits file
  fxhmake, hdr, flats

  ;put some useful information in the header
  fxaddpar, hdr, 'CREATOR', 'Sarah A. Jaeggli'
  fxaddpar, hdr, 'STDDEV', stddev2, 'Estimated error in the flat'

  for i=0,nflats-1 do begin
     if i eq 0 then fxaddpar, hdr, 'IMG'+string(i+1,FORMAT='(I1)'), filename[0], $
                              'Series used to create flat' $
     else fxaddpar, hdr, 'IMG'+string(i+1,FORMAT='(I1)'), filename[i]
  endfor

  savename=datadir+timestring+'_'+img_path+'_chaeflat_final'

  fxwrite, savename+'_slit.fits', hdr, flats
  fxwrite, savename+'.fits', hdr, flatn

  return, savename

end
