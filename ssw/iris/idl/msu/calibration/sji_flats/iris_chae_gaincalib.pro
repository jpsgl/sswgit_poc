;+
;  NAME:  iris_chae_gaincalib
;
;  PURPOSE:  Produce a gain table from a set of images with relative offsets
;
;  CALLING SEQUENCE:
;          logflat = gaincalib(logimages, x, y, object=object, )
;
;  INPUT:
;        logimages  : a three-dimensional array representing
;                     a sequence of logarithm of two-dimensional images
;                     images(*,*,k) ( k=0, 1,.., N-1).
;        x          : an array of x-shift  (input or output or both)
;        y          : an array of y-shift  (input or output ot both)
;        c          : an array of guess "exposure levels" for the
;                     input images, if all are set to 1 then c is constant
;
; OUTPUT:
;        result     : the flat field image 
;        object     : the "true" image
;        c          : array of "exposure levels" for the input images
;        x, y       : arrays of offsets for the input images
;        niter      : # iterations used
;        last_error : final error
;
; INPUT KEYWORDS:
;        maxiter    : maximum # of iternation (default=20)
;        min_error  : returns when max(abs(change after 1 iteration)) is
;                     less than min_error (default = .003)
;        shift_flag : keyword parameter containing information on how to handle
;                     shift values.
;    If set equal to 0,  x and y are treated as outputs (default)
;                            (this routine determines their initial guesses
;                            and iterates the values)
;                    1,  x and y are treated as both inputs and outputs
;                           (inputs are intial guesses and outputs are
;                             final values to be determined from iteration)
;                    2,  x and y are treated as inputs.
;                            (this program does not affect the values)
;        mask       : binary array of the same format as the logimages
;                     which specifies the pixels to be used (1: use, 
;                     0:do not use) default is to use all the pixels.
;        notvshow   : if set, no on-screen display of images
;
; OUTPUT KEYWORD:
;        object     : flat-field corrected object
;
; History:
;    1999 May,  Jongchul Chae
;    2003 November, Jongchul Chae
;    2004 July, Jongchul Chae. Added keyword: mask
;    2004 August. Generalized the keyword input array to be a 3-D one
;    2010 August  TDT  version for fg_make_flat_chae
;    2012 Sep 11  TDT  return last_error and niter
;    2012 Sep 24  TDT  added notvshow keyword
;    2013 Oct 28  Sarah A. Jaeggli  added optional "C" input/output keyword,
;                 formatting standardiazation, commenting
;-

function iris_chae_gaincalib, logimages, x, y, niter, last_error, $
                              object=object, maxiter=maxiter, $
                              silent=silent, c=c, shift_flag=shift_flag, $
                              mask=mask, min_error=min_error, $
                              notvshow=notvshow

;get image sequence dimensions
s=size(logimages)
nx=s[1]
ny=s[2]
nf=s[3]

if not keyword_set(min_error) then min_error = 0.003
if n_elements(maxiter) eq 0 then maxiter=20
if n_elements(mask) ne nx*ny*nf then mask = replicate(1B, nx, ny, nf)
if n_elements(shift_flag) eq 0 then shift_flag=0
if keyword_set(c) ne 1 then c_flag=1 else c_flag=0

i = indgen(nx)#replicate(1, ny)
j = replicate(1, nx)#indgen(ny)

; Initial Estimate of x and y
if shift_flag eq 0 then begin
   x=fltarr(nf)
   y=fltarr(nf)
   flat = replicate(0., nx, ny)

   if c_flag then begin
      c = fltarr(nf)
      for k=0,nf-1 do c[k] = median((logimages[*,*,k])[where(mask[*,*,k])])
   endif else begin
      c = c + median(logimages[where(mask)]) ;SAJ edit 1
   endelse

   tmp = 0.

   for ix=0,nx-1 do for jy=0,ny-1 do $
      flat[ix,jy] =  total((logimages[ix,jy,*] - c) * mask[ix,jy,*]) / $
                     (total(mask[ix,jy,*])>1.)
   flat = median(flat, 5)

   ss = nf/2-1

   reference = (logimages[*,*,ss] - median(logimages[*,*,ss]) - flat) * $
               mask[*,*,ss]

   for k=0,nf-1 do begin
      tmp = (logimages[*,*,k] - median(logimages[*,*,k])-flat) * mask[*,*,k]
      sh1 =  alignoffset(tmp[0:nx/2-1,*], reference[0:nx/2-1,*])
      sh2 =  alignoffset(tmp[nx/2+1:*,*], reference[nx/2+1:*,*])
      x[k] = (sh1[0] + sh2[0])/2.
      y[k] = (sh1[1] + sh2[1])/2.

      if not keyword_set(silent) then $
         print, sh1[0], sh2[0], sh1[1], sh2[1], $
                ' Offsets, LHS & RHS, X & Y, from alignoffset',form='(4F8.2,A)'

      if not keyword_set(notvshow) then begin
         if k eq 0 then begin
            mlogi = mean(tmp)
            slogi = mean(abs(tmp-mlogi))
            tvlo = mlogi - 8*slogi
            tvhi = mlogi + 8*slogi
         endif
         tv, bytscl(shift_sub(tmp, -x[k], -y[k]), tvlo, tvhi)
      endif

   endfor

endif

x=x-total(x)/nf
y=y-total(y)/nf


; Initial Estimates of Flat, Object, C
Flat=0.

;Object = 0.
;for k=0, nf-1 do Object = Object + logimages(*,*,k)

Object = total(logimages, 3)/nf

if c_flag then begin
   C = fltarr(nf)

   for k=0,nf-1 do $
      C[k]=total(logimages[*,*,k])/(nx*ny) - total(Object)/(nx*ny)
endif

C=C-total(C)/nf


; Start Iteration

for iter=1,maxiter do begin

   t1=systime(/secon)

   aa=0.0 & bb=0.0

   for k=0,nf-1 do begin
      weight = (i+x[k] ge 0) and (i+x[k] le nx-1) $
               and (j+y[k] ge 0) and (j+y[k] le ny-1)

      weight = weight*(shift_sub(mask[*,*,k], -x[k], -y[k]) ge 0.9)

      aa = aa + (C[k] + Object $
                 -shift_sub(Logimages[*,*,k]-Flat, -x[k], -y[k]) )*weight
      bb = bb + weight
   endfor
   
   DelObject = -aa/(bb>1.)
   Object = Object + DelObject
   
   aa = 0.0 & bb = 0.0
   avc = total(C)/nf
   avf = total(Flat)/nx/ny
   avl = total(x)/nf
   avm = total(y)/nf

   for k=0,nf-1 do begin
      weight = (i-x[k] ge 0 ) and (i-x[k] le nx-1) $
               and (j-y[k] ge 0 ) and (j-y[k] le ny-1)
      weight = weight*mask[*,*,k]

      object1 = shift_sub(Object,  x[k], y[k])

      ob = (C[k] + object1 + Flat - Logimages[*,*,k])*weight

      ;only update c if flag in not set
      if c_flag then $
         C[k] = C[k] -(total(ob) + 0.*avc/nf) / (total(weight) + 0./nf/nf)

      if shift_flag le 1 then begin
         Oi = convol(Object1, [-1, 8, 0, -8, 1]/12.)
         Oj = convol(Object1, transpose([-1, 8, 0, -8, 1]/12.))
         x[k] = x[k] - total(ob*oi)/total(weight*oi^2)
         y[k] = y[k] - total(ob*oj)/total(weight*oj^2)
      endif

      aa = aa + ob
      bb = bb + weight
   endfor

   DelFlat = -(aa + 0.*avf/nx*ny)/(bb > 1. + 0./(nx*ny)/(nx*ny) )
   Flat = Flat + DelFlat
   error = max(abs(Delflat))

   t2=systime(/secon)

   if not keyword_set(silent) then print, 'iteration #  =', iter , $
                                          '  time taken =', t2-t1,$
                                          '  max(abs(dellogflat)))=', error, $
                                          form='(A,I4,A,F8.2,A,F10.4)'

   if error le min_error then goto,  final
endfor

final:

niter = iter
last_error = error
mf = total(Flat)/nx/ny
mc = total(C)/nf
object = object + mf + mc
flat = flat - mf
c = c - mc

return, flat

end
