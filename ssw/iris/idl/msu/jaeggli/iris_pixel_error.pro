;+
; NAME:
;       iris_pixel_error()
;
; PURPOSE:
;       Estimate the 1-sigma error for any given data value from the IRIS NUV
;       or FUV spectrograph or slit-jaw dark-subtracted images.  This
;       error is the combination of photon counting, read noise.  In
;       a later version of the program will include the error from the
;       look-up table (LUT) used for compression.
;
; CALLING SEQUENCE:
;       pixel_error = iris_pixel_error(data, wave, [lut=])
;
; INPUTS:
;       data - An array of any dimension containing IRIS intensity
;              values in data numbers (DN).  The data should be
;              dark-subtracted
;
;       wave - A string describing the wavelength.  The options
;              are currently 'nuv' and 'fuv'.
;
; OPTIONAL INPUTS:
;       lut - a discriptor which identifies the LUT used for compression.
;
; KEYWORD PARAMETERS:
;       None 
;
; OUTPUTS:
;       Returns the pixel error as the same type and dimensions as the
;       input.
;
; OPTIONAL OUTPUTS:
;       None
;
; COMMON BLOCKS:
;       None
;
; PROCEDURES USED:
;
; COMMENTS:
;
; EXAMPLES:
;       To estimate the pixel error for each pixel in an image or data
;       cube:
;
;          IDL> pixel_error = iris_pixel_error(data, 'nuv', lut=0)
;
; MODIFICATION HISTORY:
;       Started 2013-June-12 by Sarah A. Jaeggli, Montana State University
;-

FUNCTION iris_pixel_error, data, wave, lut=lut

  IF keyword_set(wave) NE 1 THEN BEGIN
     print, 'pick a wavelength, options are "nuv" or "fuv"'
     RETURN, 0
  ENDIF

  CASE wave OF
     'fuv' : BEGIN
        gain1        = 7.       ;photon counts/DN, pristine gain
        gain2        = 12.5     ;gain measured with charge spreading
        read_noise   = 1.2      ;DN
        dark_current = 0.0      ;photon counts / sec
     END

     'nuv' : BEGIN
        gain1        = 14.5     ;photon counts/DN, pristine gain
        gain2        = 16.0     ;gain measured with charge spreading
        read_noise   = 1.2      ;DN
        dark_current = 0.0      ;photon counts / sec
     END
  ENDCASE

  ;convert data back to photon counts
  counts=data*gain1

  ;calculate the noise for every pixel value
  photon_error=sqrt(read_noise^2 + counts/(gain1*gain2))

  ;calculate LUT error in the future
  IF keyword_set(lut) THEN BEGIN
                                ;load the LUT and inverse LUT,
                                ;something does need to be known about
                                ;the dark level
;     lut_error=data-ilut[lut[data]]
  ENDIF ELSE lut_error=0.

  error=photon_error ;+ lut_error

  RETURN, error

END
