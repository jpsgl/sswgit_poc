pro tb_deriv_rate, info, icol, reftim0, win, out, outavg, convert2local=convert2local, $
	outdir=outdir, qstop=qstop
; $Id: tbtv_mk_rate_table.pro 6328 2012-04-12 21:44:12Z linford $
; $Log: tbtv_mk_rate_table.pro,v $
; Revision 1.3  2007/08/31 19:00:24  linford
; Added qstop switch for debugging, added env var SDO_REMOVE_HTML_DIR to correct auto genrated links
;
; Revision 1.2  2007/08/14 18:01:04  linford
; cvs update from kuna
;
; 
common tb_deriv_rate_blk1, mnem_from, mnem_to

qdoavg = n_elements(outavg) eq 0
if (qdoavg) then outavg = strarr(n_elements(out(0,*))-1)

full_link = '/TandC/DB/104/GIF4HR'
; the following trims the head of the path string to a "relative path", in this case starting at /TandC/...
if (keyword_set(outdir)) then begin
	full_link = concat_dir(str_replace(outdir,getenv('SDO_REMOVE_HTML_DIRS'),'/iris/irisdata/tvac'), 'GIF4HR')
end

if (n_elements(mnem_from) eq 0) then begin
	print,"Reading $SDO_TBTV_DB_DIR/mnem_aliases.tab"
	print,"mnem_aliases mnemonics used to in html and text rate tables"
    tc_map = rd_ulin_col('$SDO_TBTV_DB_DIR/mnem_aliases.tab', /nohead, nocomm='#')
    mnem_from = strupcase(reform(tc_map(0,*)))
    mnem_to = reform(tc_map(2,*))
end

tags = tag_names(info)
ntags = n_elements(tags)
;
fmt1 = '(f6.1)'
fmt2 = '(f6.2)'

if (keyword_set(convert2local)) then reftim2 = ut_time(reftim0, /to_local) $
				else reftim2 = reftim0

out(icol, 0) = '<small>' + gt_day(reftim2, /str) + '</small>'
out(icol+1,0) = '<small>' + gt_time(reftim2, /str) + '</small>'
ii = 1
for itag=0,ntags-1 do begin
    mnem0 = strlowcase(info.(itag).mnem)
    timarr = info.(itag).daytime
    tim1 = anytim2ints(reftim0, off=-1*win)
    tim2 = anytim2ints(reftim0)
    ss = sel_timrange(timarr, tim1, tim2, /between)

    if (qdoavg) then begin
	tim1avg = anytim2ints(reftim0, off=-4*60*60)	;last 4 hrs
	tim2avg = anytim2ints(reftim0)
	ssavg = sel_timrange(timarr, tim1avg, tim2avg, /between)
    end

    for j=0,n_elements(mnem0)-1 do begin
	;;out(0, ii) = info.(itag).mnem(j)
	mnem = info.(itag).mnem(j)
	mnem_lab = mnem
	print,"Rate Table Label: ", mnem_lab
	ss33 = where( mnem_from eq strupcase(info.(itag).mnem(j)), nss33)
	if (nss33 eq 1) then mnem_lab =  mnem_to(ss33)
	;if (keyword_set(qstop)) then stop
	out(0, ii) = '<A HREF=" ' + concat_dir(full_link, strlowcase(mnem) + '.gif') + '">' + $
							mnem_lab + '</A>'
	;
	y = info.(itag).value(*,j)
	;
	if (ss(0) eq -1) then begin
	    out(icol,   ii) = 'N/A'
	    out(icol+1, ii) = 'N/A'
	end else begin
	    v = y(max(ss))
	    out(icol, ii) = string(v, format=fmt1) + ' C'
	    if (n_elements(ss) ge 2) then begin
		coeff = poly_fit( int2secarr(timarr(ss))/60./60., y(ss), 1, yfit)
		out(icol+1, ii) = string(coeff(1), format=fmt2) + ' C/hr'
		color = ''
		;if (abs(coeff(1)) gt 0.5) then color = " bgcolor='orange'>"
		;if (abs(coeff(1)) gt 3.0) then color = " bgcolor='white'>"
		if (coeff(1) gt 0.5) then color = " bgcolor='yellow'>"
		if (coeff(1) gt 3.0) then color = " bgcolor='orange'>"
		if (coeff(1) lt -0.5) then color = " bgcolor='lightblue'>"
		if (coeff(1) lt -3.0) then color = " bgcolor='blue'>"
		out(icol+1, ii) = color + out(icol+1, ii) 
		if (keyword_set(qdebug)) then begin
			utplot, timarr(ss), y(ss), /ynoz
			outplot, timarr(ss), yfit
			print, out(*,ii)
			pause
		end
	    end else begin
		out(icol+1, ii) = 'N/A'
	    end
	end

	if (qdoavg) then begin
	    if (ssavg(0) eq -1) then yy = [9999, 9999.] else yy = y(ssavg)
	    ss33 = where( abs(yy) lt 9999, nss33)
	    if (nss33 le 1) then yy = [9999, 9999.] else yy = yy(ss33)
	    dev = stdev(yy, avg)
	    imin = min(yy, max=imax)
	    outavg(ii-1) = string(mnem_lab+'                        ', avg, dev, imin, imax, $
									format='(a30, 4f8.2)')
	    ;if (mnem_lab(0) eq 'V_T_Hot_Front_Aper') then stop
	end
	ii = ii + 1
    end
end

end
;---------------------------------------------------
pro tbtv_mk1rate_table, info, entim, outfil, convert2local=convert2local, outdir=outdir, $
qstop=qstop
;
;
; -- Define table size
dtims = [0,1,2,4]*60*60.
np = n_elements(dtims)
;fmt_timer, info.(0).daytime, t1, reftim, /quiet

; -- Define fit interval for obtaining rates
reftim = entim
win = 30*60	;30 minutes for fitting
;
n = 0
for i=0,n_elements(tag_names(info))-1 do n = n + n_elements(info.(i).mnem)
out = strarr(2*np+1, n+1)

if (keyword_set(qstop)) then stop
for ip = 0,np-1 do begin
    sttim = anytim2ints(reftim, off=-1D*dtims(ip))
    tb_deriv_rate, info, ip*2+1, sttim, win, out, outavg, convert2local=convert2local, $
		outdir=outdir, qstop=qstop
end
;
out2 = ['<html><head><title>Temperature Real Time and Rates</title><body>',$
	'<meta http-equiv="refresh" content=60" >', $
	'The following are the temperatures currently, 1 hr ago, 2 hrs ago, ', $
	'and 4 hrs ago.  The rate of change in degrees/hour are listed next to the temperature.', $
	'The rates are derived by fitting the data for the prior 30 minutes', $
	'Colors are used for rates ', $
	'<font color="orange">Over 3 C/hr warming</font>', $
	'and <font color="yellow">Between 0.5 and 3 C/hr warming</font>', $
	'and <font color="blue">Faster than -3 C/hr cooling</font>', $
	'and <font color="lightblue">Between -0.5 and -3 C/hr cooling</font>', $
	' ']
out2 = [out2, $
	'Page Generated: ' + !stime, $
	strtab2html(out), $
	' ', $
	'Page Generated: ' + !stime, $
	'</body></html>']
out2 = str_replace(out2, '>bgcolor', ' bgcolor')
print, 'Writing Rates HTML File: ', outfil
prstr, out2, file=outfil

outfil2 = str_replace(outfil, 'rates', 'avg')
outfil2 = str_replace(outfil2, '.html', '.txt')
print, 'Writing Aveages Text File: ', outfil2
if (outfil2 ne outfil) then prstr, outavg, file=outfil2
end
;---------------------------------------------------
pro tbtv_mk_rate_table, sttim, entim, mnem, file=infil, qstop=qstop, outdir=outdir, $
	instr_only=instr_only, noplots=noplots


;;mnem = ['SDO_CEBTMP', 'SDO_GPIB_TC_A01', $
;;		'SDO_PCM_CEBTMP', 'SDO_GPIB_STMPCEB_BR']
;;sttim = '24-Mar 00:00'
;;entim = '24-Mar 07:00'
;
if (n_elements(outdir) eq 0) then outdir = getenv('SDO_TBTV_HTML_DIR')
;
if (n_elements(mnem) eq 0) and (not keyword_set(infil)) then infil = '$SDO_TBTV_DB_DIR/rate_mnem1.tab
if (keyword_set(infil)) then begin
    ;infil = '$SDO_TBTV_DB_DIR/summary_mnem1.tab
    list = rd_tfile(infil, nocomment='#')
    mnem_mat = str2cols(list)
    mnem = strlowcase(strtrim(mnem_mat(0,*),2))
end
;
if (n_elements(sttim) eq 0) then begin
    win = 5
    entim = ut_time()
    sttim = anytim2ints(entim, off=-60*60*win)
end
;

info = sag_get_mnem(sttim, entim, mnem, /nostring)
outfil = concat_dir(outdir, 'real_time_rates.html')
help,info,outfil
if (keyword_set(qstop)) then stop
tbtv_mk1rate_table, info, entim, outfil, /convert2local, outdir=outdir, qstop=qstop
if (keyword_set(qstop)) then stop
;
;-- When on "white-net" process Chamber rates too:
if ( not keyword_set(instr_only) ) then begin
	 lsttim = ut_time(sttim, /to_local)
	 lentim = ut_time(entim, /to_local)
	 tbtv_rd_rocket, lsttim, lentim, info2
	 outfil = concat_dir(outdir, 'real_time_rates_gse.html')
	 tbtv_mk1rate_table, info2, lentim, outfil, outdir=outdir
end
;
if (not keyword_set(noplots)) then begin
    tbtv_plot_singles, info, outdir
	if ( not keyword_set(instr_only) ) then begin
    	tbtv_plot_singles, info2, outdir, /nolocal		; generate single plots chamber-GSE
	end
end
;
end
