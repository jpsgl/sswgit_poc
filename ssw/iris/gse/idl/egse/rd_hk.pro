function rd_hk, infil0, strec0, enrec0, head=head, only_head=only_head, map=map, $
		struct_head=struct_head, new=new, qdebug=qdebug, sci=sci, $
		cleanup=cleanup, qstop=qstop, simple=simple, hkmapfil=hkmapfil
;+
;NAME:
;	rd_hk
;PURPOSE:
;	To directly read from the .mdihk files
;SAMPLE CALLING SEQUENCE:
;	out = rd_hk(infil)
;	out = rd_hk(infil, head=head)
;	out = rd_hk(infil, strec, enrec, head=head)
;	dummy = rd_hk(infil, head=head, /only_head)
;	head = rd_hk(infil, /only_head)
;INPUT:
;	infil
;	strec
;	enrec
;OUTPUT:
;	out	- The .time field is always in seconds past 1-Jan-70
;		  (the epoch is adjusted for the different types)
;OPTIONAL KEYWORD OUTPUT:
;	head
;OPTIONAL KEYWORD INPUT:
;	only_head - If set, then just read the header, and return it
;		    as the function return.
;	new	  - If set, then read the file as the new "REL" organization
;		    which has slightly different structure, and has an
;		    ASCII header.
;	cleanup	  - If set, then check the data for only good data (ie: remove
;		    data which is from when the DEP is off).
;NOTE:
;	The TIME field coming out of RD_HK is
;		SECONDS PAST 1-Jan-70
;	For all epochs (MDI/TRACE) and for both the packet times and map files
;HISTORY:
;	Written 23-May-94 by M.Morrison
;	26-May-94 (MDM) - Revised to not have a HEADER structure in
;			  the .mdihk file and to only have it in the
;			  .mdihkx file
;	31-May-94 (MDM) - Modified to figure the number of records properly
;			  when the input file is .schk
;	11-Jan-95 (MDM) - Modified to work on SCHK1,2,4
;	23-Jan-95 (MDM) - Added /NEW option
;			- Added /QDEBUG option
;	 7-Jun-95 (MDM) - Added code to figure out and handle the New flight
;			  HK database (fool it by creating a false roadmap)
;	12-Jun-95 (MDM) - Added /SCI option and recognizing ".sci"
;			- Removed code and started using RD_PKT_HEAD
;			- Removed the hardwired step size
;	29-Jun-95 (MDM) - Added /CLEANUP keyword
;	18-Sep-95 (MDM) - Made the record numbers be integers
;	19-Sep-95 (MDM) - Fixed size of MAP array output when using /NEW
;			- Added /QSTOP
;	29-Sep-95 (MDM) - Added debug statement
;			- Put capability of reading .REL and .QKL
;	31-Oct-95 (MDM) - Added /SIMPLE option to not apply any corrections
;	16-Nov-95 (MDM) - Modified to allow a workaround where the expected
;			  extensions were .schk1,2,3,4 and they were implemented
;			  as sc1,2,3,4
;	27-Nov-95 (MDM) - Added ".exp" file types (for emergency experiment
;			  packets)
;	28-Nov-95 (MDM) - Modified to allow reading of the EXPHK .REL file
;	29-Nov-95 (MDM) - Modified CLEANUP option to be able to recognize bad
;			  times
;	16-Jan-96 (MDM) - Patched error which was ending the read before the
;			  end of the file (the calculation of the number of
;			  packets in the file based on the file size did not
;			  work for the new file format)
;	27-Mar-96 (MDM) - Patch to avoid bogus data - if the time tag is way
;			  off (less than 0).
;	10-Jun-96 (MDM) - Modified to "clean" sc1 packets properly
;	 7-Oct-96 (MDM) - Added getenv('RD_HK_FIXED_OFFSETRD_HK_FIXED_OFFSET') option
;			   *********************************
;	 9-Oct-96 (MDM) - Added TRACE data products
;	24-Oct-96 (MDM) - Added MDI_CDROM_HKDBASE option
;                       - Added ability to have default step sizes
;	12-Mar-97 (MDM) - Modification to the TRACE .hk4e packet length
;	14-Mar-97 (MDM) - Added "sc0a" and "sc0b"
;	25-Apr-97 (SDW) - Added "sc3" 
;	25-Sep-97 (MDM) - Various modifications to work for TRACE DPS files
;			  with "map" files (time index)
;			   *********************************
;	13-Jan-98 (MDM) - Modified to find relevant information from a file
;			  rather than by CASE statement
;	 5-Mar-98 (MDM) - Small mod
;	24-Jun-98 (MDM) - Added "HK_DBASE_MAP_FILE" option
;			- Added GSE_EPOCH
;-
;
common rd_hk_blk1, last_hkmapfil, hkmap
;
if (n_elements(last_hkmapfil) eq 0) then last_hkmapfil = ''
if (n_elements(hkmapfil) eq 0) and (keyword_set(getenv('HK_DBASE_MAP_FILE'))) then hkmapfil = getenv('HK_DBASE_MAP_FILE')
if (n_elements(hkmapfil) eq 0) then hkmapfil = '$db/trace.hk'
if (last_hkmapfil ne hkmapfil) or (n_elements(hkmap) eq 0) then hkmap = rd_tfile(hkmapfil, 6, nocomment='#')
last_hkmapfil = hkmapfil
;
if (getenv('MDI_NEW_HKDBASE') ne '') then new = 1
qcdrom = (getenv('MDI_CDROM_HKDBASE') ne '')
if (keyword_set(qdebug)) then print, 'QCDROM = ', qcdrom, '  New = ', new
;
if (keyword_set(new)) then infil0 = str_replace(infil0, '.schk', '.sc')		;MDM 16-Nov-95 workaround
;
head ={rd_hk_head, nrec: 0L,  $
	start_time: 0L, end_time: 0L, $
	progverno: 0L, $
	spare1: 0L, spare2: 0L, spare3: 0L}
if (keyword_set(struct_head)) then return, head

break_file, infil0, dsk_log, dir, filnam, extens
if (extens eq '.REL') or (extens eq '.QKL') then begin		;MDM added 29-Sep-95
    if (strmid(filnam, 0, 5) eq 'MDIHK') then extens = '.mdihk'
    if (strmid(filnam, 0, 5) eq 'MDISC') then extens = '.sci'
    if (strmid(filnam, 0, 5) eq 'EXPHK') then extens = '.exp'
end
if (extens eq '.sc1') then extens = '.schk1'		;MDM 16-Nov-95 workaround
if (extens eq '.sc2') then extens = '.schk2'
if (extens eq '.sc3') then extens = '.schk3'
if (extens eq '.sc4') then extens = '.schk4'
;
ss = where(hkmap(2,*) eq extens, nss)
if (nss eq 0) then begin
    stop, 'Unrecognized filename extension: ' + infil0
end
;
iqual = 0	;TO ADD TO trace.hk ?
		;what to do about variable packet length (hk4e)
nn = fix(hkmap(4,ss(0)))
step_def = float(hkmap(5,ss(0)))
;
if (extens eq '.hk4e') then if (filnam lt '970313') then nn = 392
;
out0 = {msg_type: fix(0), length_id: fix(0), request_id: fix(0), $
	ccsds: bytarr(6), time: long(0), ccsds2: bytarr(2), $
	hk: bytarr(nn)}
npkt_head = 18
if (keyword_set(new)) then begin
    out0 = {ccsds: bytarr(6), time: long(0), ccsds2: bytarr(2), $
	hk: bytarr(nn)}
    npkt_head = 12
end
if (keyword_set(map)) then out0 = 0L
;
infil = infil0
if (keyword_set(map)) then infil = infil + 'x'
if (not file_exist(infil)) then begin
    if (keyword_set(new) and keyword_set(map)) then begin	;MDM added 7-Jun-95
	break_file, infil, dsk_log00, dir00, filnam00
	fid00 = filnam00 + '.0000'
	dattim00 = fid2ex( fid00 )
	rd_pkt_head, infil0, nbyte, head, head_info
	time_step = head_info.time_step
	if (time_step eq 0) then time_step = step_def
	nnn = head_info.num_slots > (86400/time_step)		;MDM 19-Sep-95
	out = dindgen(nnn) * time_step + int2secarr(dattim00, '1-Jan-70')
	return, out
    end else begin
	print, 'RD_HK:  Input file: ' + infil + ' does not exist'
	return, -1
    end
end
;
swap_os=['vms','ultrix','OSF']                  ; list of swap machines
chk=where(!version.os eq swap_os,nscount)       ; current OS in noswap list?
qswap = nscount ne 0                            ; assign boolean
;
;
openr, lun, infil, /get_lun, /block
if (keyword_set(map)) then begin
    readu, lun, head
    if (qswap) then dec2sun, head
end
if (keyword_set(only_head)) then begin
    free_lun, lun
    return, head
end
;
off_head = 0
if (keyword_set(new) and (not qcdrom) and (not keyword_set(map))) then begin
    rd_pkt_head, lun, head_arr, off_head, head_info, qdebug=qdebug
end
if (qcdrom) then off_head = 50 
if (getenv('RD_HK_FIXED_OFFSET') ne '') then off_head = long(getenv('RD_HK_FIXED_OFFSET'))

if (keyword_set(map)) then begin
    nrec = head.nrec
    off_head = get_nbytes(head)
end else begin
    siz = file_stat(infil, /size)
    nrec = (siz - off_head) / (nn+npkt_head)
end
if (n_elements(strec0) eq 0) then strec = 0 else strec = strec0
if (n_elements(enrec0) eq 0) then enrec = nrec-1 else enrec = enrec0
strec = long( strec > 0 < (nrec-1) )
enrec = long( enrec > 0 < (nrec-1) )
;
nout = (enrec - strec + 1) >1
out = replicate(out0, nout)
;
skip = strec * get_nbytes(out0) + off_head
if (keyword_set(map)) then skip = skip + get_nbytes(head_arr)
point_lun, lun, skip
if (keyword_set(qdebug)) then print, 'Byte pointer offset to: ', skip
;
readu, lun, out				& if (qswap) then dec2sun, out
if (keyword_set(simple)) then return, out
;
if (not keyword_set(map)) and (keyword_set(new)) then begin
    ;ss = where(out.time ne 0, nss)
    ss = where(out.time gt 0, nss)	;MDM changed to GT 27-Mar-96
    if (nss eq 0) then begin
	if (keyword_set(qdebug)) then print, 'RD_HK: No valid data found in time span specified for ' + infil
	if (keyword_set(qstop)) then stop
	out = -1
    end else begin
	out = out(ss)
	if (getenv('GSE_CONFIG_TYPE') eq 'TRACE') then epoch = '24-May-68' $
					else epoch = '1-Jan-58'
	if keyword_set(getenv('GSE_EPOCH')) then epoch = getenv('GSE_EPOCH')

	out.time = out.time - int2secarr('1-jan-70', epoch)	;turn it from TAI to UTC times
    end
end
;
if (not keyword_set(map)) and (keyword_set(cleanup)) and (data_type(out) eq 8) and (iqual ne -1) then begin
    case cleanup of
	1: ss = where((out.hk(iqual) ne 255) and (out.hk(iqual) ne 0), nss)
	2: ss = where((out.time gt 631152000) and (out.time lt 915148800), nss)
		;check that the time is between 1-Jan-90 and 1-Jan-99
	3: ss = where((out.hk(iqual) ne 255) and (out.hk(iqual) ne 0) and $
			(out.time gt 631152000) and (out.time lt 915148800), nss)
	else: nss=0
    endcase
    if (keyword_set(qdebug)) then begin
	print, iqual
	pmm, out.hk(iqual)
    end
    if (nss eq 0) then begin
	if (keyword_set(qdebug)) then print, 'RD_HK: No good quality data for ' + infil
	out = -1
    end else begin
	out = out(ss)
    end
end
;
if (keyword_set(qstop)) then stop
free_lun, lun
return, out
end
