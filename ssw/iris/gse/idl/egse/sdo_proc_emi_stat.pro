pro sdo_proc_emi_stat, hk, smoot=smoot, color=color, hc=hc,qlinestyle=qlinestyle, filter1d=filter1d, ounit=ounit, $
		outstatfile=outstatfile, boutstatfile=boutstatfile, inst=inst, onedialog=onedialog
;+
; $Id: sdo_proc_emi_stat.pro 4942 2012-01-13 03:52:02Z sabolish $
; $Name:  $
;
; Name: sdo_proc_emi_stat
;
; Purpose: Process housekeeping data (hk) for "shot" noise. Computes stats
;		and writes those numbers to '*outstatfile's.
;
; Parameter:
;	hk 		- hk data structure from sag_get_mnem()
;	smoot	- enable "box car" smoothing function, of size specified
;	color	- enable color plots
;	qlinest	- enable qlinestyles
;       filter1d- Remove bad data by finding where values exceed the
;                 "smoothed 100" values by more than 3 sigma
; 	ounit	  unit for output stat table
;	outstatfile 	filename of output dist stat table file
;	boutstatfile	filename of outlier file
;	inst		instr name e.g. AIA or HMI
;	onedialog	switch to limit the number of dialog alarms on stats
;
; History: G.Linford, 16 July 2007
;		April 2008, G.Linford, modified to run for AIA and HMI.
;
;	$Log: sdo_proc_emi_stat.pro,v $
;	Revision 1.2  2008/04/16 21:19:04  linford
;	New default has continous dialog alarms, Added onedialog switch to limit this to one
;	
;	Revision 1.1  2008/04/11 15:12:50  linford
;	name change from aia_proc_emi_stat.pro to sdo
;	
;-

;-- stats common block: mnemonics, min-stddev of dist., n-sigma val for outliers
common aia_tlm_stats, smnem, minsig, nsigout, fdialog, fnsigma

dmesg = strarr(1)
fnsigma = 0

tags = tag_names(hk)
mnem = hk.(0).mnem		; tlm list
nitems = n_elements(mnem)	; # of tlm items
value = hk.(0).value
dtime = hk.(0).daytime
;print,'Debug aia_proc_emi_stat'
;stop
statfileopen = 0			; set to false
;print, 'stat logging going to unit: ', ounit

for i=0, nitems -1 do begin
	;print, 'processing stats for mnem: ', mnem[i]
	imax = max(value[*,i]) 
	imin = min(value[*,i])
	istd = stddev(value[*,i])
	imean= mean(value[*,i])
	isqrtm = SQRT(imean)
	icom = where( smnem eq strupcase(mnem[i]), nicom)			; lookup index
	if ( nicom eq 1 ) then begin
		oldsig = minsig[icom]
		cursig = istd
		minsig[icom] = MIN( [oldsig, cursig] )	; save the min value
	endif
	;if (mnem[i] eq 'aia_ts11_t1_ceb_external') then begin
	;	print, mnem[i], '[max,min,mean,stddev, minsig]: ',imax, imin, imean, istd, minsig[icom] 
		;print,'DEBUG -STOP
		;stop
	;endif

	if ( istd ge (0.30 * isqrtm) ) then begin	; check actual stddev
		;print, 'processing stats for mnem: ', mnem[i]
		delta1 = (imax - imean)			; find the max outlier
		delta2 = (imean - imin)
		if ( delta1 gt delta2 ) then begin
			delta = delta1
			ii = where( value[*,i] eq imax)
		endif else begin
			delta = delta2
			ii = where( value[*,i] eq imin)
		endelse
		nsig = delta/minsig[icom]		; compute the n-sigma value for max outlier
		nsigout[icom] = nsig			; save n-sigma value
		if (nsig gt 5.0 ) then begin
			fnsigma = 1
			ilast = n_elements(ii)
			ilast = ilast - 1	
			datetime = anytim(dtime[ii[ilast]],/yohkoh)
			print, "FLAG Outliers > 5-sigma: ", datetime, mnem[i], nsig, imean, istd
			openw, aunit, /get_lun, /append, boutstatfile
			if ( strlen(dmesg[0]) eq 0 ) then begin
				dmesg[0] = string(format='("FLAG Outliers > 5-sigma: ",A," ",A," ",f9.3)', datetime, mnem[i], nsig)
				printf,aunit, format='(A,"  ",A25,"  ",f9.3,"  ", f9.3)', datetime, mnem[i], imean, nsig
			endif else begin
				sdmesg = string(format='("FLAG Outliers > 5-sigma: ",A," ",A," ",f9.3)', datetime, mnem[i], nsig)
				dmesg = [dmesg,sdmesg]
				printf,aunit, format='(A,"  ",A25,"  ",f9.3,"  ", f9.3)', datetime, mnem[i], imean, nsig
			endelse	
			nsigma = 1			; set nsigma flag
			FREE_LUN, aunit
		endif else begin
			fnsigma = 0
		endelse
		
		if ( istd ge 5.0 ) then begin		; when the stddev of the dist of tlm value is > 5
			ilast = n_elements(ii)
			ilast = ilast - 1	
			datetime = anytim(dtime[ii[ilast]],/yohkoh)
			openw, ounit, /get_lun, /append, outstatfile
			printf, ounit, format='(A,"  ",A25,"  ",f9.3," ",f9.3," ", f9.3)', datetime, mnem[i], istd, imean, nsig
			print, format='("FLAG STD > 5: ",A,"  ",A,"  ",f9.3,"  ",f9.3,"  ",f9.3)', datetime, mnem[i], istd, imean, nsig
			
			;print,'DEBUG -STOP
			;stop
			FREE_LUN, ounit
		endif
	endif
	
endfor
dialogTitle = strupcase(inst)+' EMI/EMC Statistics Warning'
if ( (fdialog eq 1) && (strlen(dmesg[0]) ne 0) ) then begin  
	dsp_strarr, dmesg, /no_block, title=dialogTitle
	if keyword_set(onedialog) then begin
		fdialog = 0			; set flag to false, and only display alarm dialog once
	endif
endif

end
		
