pro tbtv_plots, sttim, entim, outdir
;
;$Id: tbtv_plots.pro 4942 2012-01-13 03:52:02Z sabolish $
;$Log: tbtv_plots.pro,v $
;Revision 1.2  2007/08/14 18:20:21  linford
;cvs update from kuna
;
;cvs update from kuna
;
;	NOTE: USING $SDO_TBTV_DB_DIR instead of $SXI_TBTV_DB_DIR to understand
;	program flow and to "flush out" all important sub-procedures.
;	Also, using $SDO_TBTV_HTML_DIR instead of $SXI_TBTV_HTML_DIR for the 
;	same reason and to output to a test area.
;
;outdir = concat_dir('$SDO_TBTV_HTML_DIR', 'Debug1_0412')
if (n_elements(outdir) eq 0) then outdir = concat_dir('$SDO_TBTV_HTML_DIR', 'GIF')
if (not file_exist(outdir)) then spawn, ['mkdir', outdir], /noshell
;
tc_map = rd_ulin_col('$SDO_TBTV_DB_DIR/mnem_aliases.tab', /nohead, nocomm='#')
tc_map(0,*) = strlowcase(tc_map(0,*))
;
ff = file_list('$SDO_TBTV_DB_DIR', 'summary_mnem*.tab')
;ff = '$SDO_TBTV_DB_DIR/summary_mnem1.tab
;if (n_elements(ff) gt 1) then begin
;    ii = xmenu_sel(ff, /one)
;    if (ii eq -1) then stop
;    ff = ff(ii)
;end
;
if (n_elements(sttim) eq 0) then begin
    win=4
    entim = anytim2ints(ut_time())
    sttim = anytim2ints(entim, off=-60*60*win)
end
;
!p.multi = 0
tv2, 800, 600, /init
for i=0,n_elements(ff)-1 do begin
    break_file, ff(i), dsk_log, dir00, filnam, ext
    list = rd_tfile(ff(i), nocomment='#')
    mnem_mat = str2cols(list)
    mnem = strlowcase(strtrim(mnem_mat(0,*),2))
    ;
    ss = where_arr(mnem, tc_map(0,*), /map_ss)
    if (max(ss) eq -1) then delvarx, user_labels else begin
	user_labels = strarr( n_elements(mnem) ) + '????'
	ss2 = where(ss ne -1)
	user_labels(ss2) = tc_map(1,ss(ss2))
    end
    ;
    info = sag_get_mnem(sttim, entim, mnem)
    if (data_type(info) ne 8) then stop, 'HK read problems........................................
    !p.thick=2
    sag_hkplot, info, /label, /qlinestyle, /colors, smoo=8, pm_filter=150, $
		timerange=[sttim, entim], user_label = user_labels, tit=filnam+ext + ' (Smooth 8)'
    if (!d.name eq 'Z') then zbuff2file, concat_dir(outdir, filnam+'.gif')
    pause
end
;
tv2, !d.x_size, 800, /init
ff = file_list('$SDO_TBTV_DB_DIR', '*.elst')
for i=0,n_elements(ff)-1 do begin
    break_file, ff(i), dsk_log, dir00, filnam, ext
    plot_set_file = ff(i)
    plot_bakeout_logger, ut_time(sttim, /to_local), ut_time(entim, /to_local), $
			code=0, plot_set_file=plot_set_file
    if (!d.name eq 'Z') then zbuff2file, concat_dir(outdir, filnam+'.gif')
end
;
tv2, 600, 800, /init
ff = file_list('$SDO_TBTV_DB_DIR', '*.plst')
for i=0,n_elements(ff)-1 do begin
    break_file, ff(i), dsk_log, dir00, filnam, ext
    plot_set_file = ff(i)
    sag_plot_plst, plot_set_file, sttim, entim
    if (!d.name eq 'Z') then zbuff2file, concat_dir(outdir, filnam+'.gif')
end

end
