pro sxi_disp_img_sum_s1, x, h, ii, val, label
;
;
;
ss = where(x ge val, nss)
if (nss eq 0) then out = 'Beyond 4096' $
		else out = strtrim(h(ss(0)),2)
;
plottime, .5, .90-ii*.04, label
plottime, .7, .90-ii*.04, '=' + strtrim(val, 2)
plottime, .8, .90-ii*.04, out
;
end
;---------------------------------------------
pro sxi_disp_img_sum, date, inum, recs=recs, qstop=qstop, hc=hc
;
;
;
;
;HISTORY:
;       Written 15-Dec-00 by M.Morrison
;V1.01	21-Dec-00 (MDM) - Added threshold details
;
if (keyword_set(hc)) then setps, /land
;
if (n_elements(recs) eq 0) then begin
    if (n_elements(date) eq 0) then date = ut_time()
    if (n_elements(inum) eq 0) then inum = 0
    fid = time2file( date, /date)
    infil = '$GSE_PKT_FILE_DIR/0x0013/' + fid + '.0x0013'
    if (not file_exist(infil)) then return
    if (file_size(infil) eq 0) then return
    recs = sag_rd_hk(infil, inum, inum)
    print, infil, inum
end
;
v = recs.data
serno = long(v, 0)
lobt = long(v, 4)
flobt = fix(v, 6)
dattim = sxi_time_conv('INSTRUMENT', lobt, flobt, /string, /msec)
pktdattim = sxi_time_conv('INSTRUMENT', struct=recs.pkt_head, /string, /msec)
;
h = unsign( fix(v, 10, 256) ) * 4
x = indgen(256)*16
;
f_st = sag_ext_mnem(v, 'SXI_ST_THRESH_F')
f_en = sag_ext_mnem(v, 'SXI_EN_THRESH_F')
gf_st = sag_ext_mnem(v, 'SXI_ST_THRESH_GF')
gf_en = sag_ext_mnem(v, 'SXI_EN_THRESH_GF')
;
;----
;
label = string(v(800:811))
fdb = long(v, 812)
seqid = fix(v, 816)
FlstID = fix(v, 818)
str0 = string(serno, lobt, flobt, dattim, pktdattim, $
	f_st, f_en, gf_st, gf_en, $
	label, FDB, SeqID, FlstID and 'ffff'x, $
	format='(i6, 1x,z8.8, f8.5, 1x, a, 3x, a, 4i6, 2x,a, 1x,z8.8, 1x,z4.4, 1x,z4.4)')
print, str0
;
;----
;
tit = 'LOBT=' + dattim + ' PKT=' + pktdattim + '   SerNo=' + strtrim(serno,2)
;plot_io, x, h>.1, psym=10, tit=tit
plot, x, h>.1, psym=10, tit=tit, /ytype
!type=0
yy = [-1e+7,1e+7]
oplot, [1,1]*f_st, yy, linestyle=2
oplot, [1,1]*f_en, yy, linestyle=2
oplot, [1,1]*gf_st, yy, linestyle=1
oplot, [1,1]*gf_en, yy, linestyle=1
;
sxi_disp_img_sum_s1, x, h, 0, f_st, 'Flare Start'
sxi_disp_img_sum_s1, x, h, 1, f_en, 'Flare End'
sxi_disp_img_sum_s1, x, h, 2, gf_st, 'Great Flare Start'
sxi_disp_img_sum_s1, x, h, 3, gf_en, 'Great Flare End'
;
plottime, 0, 0, 'SXI_DISP_IMG_SUM  Ver 1.01
plottime
pprint
if (keyword_set(hc)) then set_plot,'x'
if (keyword_set(qstop)) then stop
;
end
