;
;
;
if (n_elements(img_cnt) eq 0) then img_cnt = 0
input, 'Enter initial frame count value', img_cnt, img_cnt
;
lab = ''
input, 'Enter ASCII label', lab, lab
if (strlen(lab) gt 10) then stop, 'Label must be 10 char or less'
;
b = [byte(lab), bytarr(10)]
b = b(0:9)
load = [fix(img_cnt), fix(b, 0, 5)]
print, load
;
fmt = "('/SXI_CCML3 AHI=0x0129 ALO=0 D1HI=0x', z4.4, ' D1LO=0x', z4.4, " + $
			"' D2HI=0x', z4.4, ' D2LO=0x', z4.4, " + $
			"' D3HI=0x', z4.4, ' D3LO=0x', z4.4)" 
out = string(load, format=fmt)
;
print, '# ASCII Label: ' + lab
print, out
;

;
end