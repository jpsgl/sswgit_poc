pro mk_lfft_db_web, tandcnum, datedir, title, photodir0, skip_copy=skip_copy

;NAME:
;       tandc_db_web_maker
;PURPOSE:
;       To generate TandC database web pages
;HISTORY:
;       Written 03-Aug-00 by J. Covington 
;	 7-Jun-01 (MDM) - Modified to copy all files and then move them around
;			- Added /SKIP_COPY keyword
;	18-Jan-02 (MDM) - Added go driver routine
;
;INPUTS: title    : webpage title
;        datedir  : location of ps and gif files and .usr files
;        tandc    : TandC designated number
;        photodir : location of photos for this test
;                 : none = no photos available
;
;Sample Calling Sequence:
;         tandc_db_web_maker, '401','000705.173714', 'SXI Engineering Model Telescope Shock Test', '/SXI/Pics/MMorrison/'
;	  (see go_mk_lfft_db_web.pro)
;
;
; /sxi35/log/lfft/   $SXI_LFFT_DIR

if (keyword_set(skip_copy)) then goto, skip

cmd = 'mkdir /sxi01/html/TandC/DB/' + tandcnum				& print,cmd & spawn,cmd
cmd = 'mkdir /sxi01/html/TandC/DB/' + tandcnum + '/Part1/'		& print,cmd & spawn,cmd
cmd = 'mkdir /sxi01/html/TandC/DB/' + tandcnum + '/Part1/' + 'GIF'	& print,cmd & spawn,cmd
cmd = 'mkdir /sxi01/html/TandC/DB/' + tandcnum + '/Part1/' + 'PS'	& print,cmd & spawn,cmd
  


;spawn, 'cp ' + datedir + '/*.gif /sxi01/html/TandC/DB/' + tandcnum + '/Part1/' + 'GIF'
;spawn, 'cp ' + datedir + '/*.ps /sxi01/html/TandC/DB/' + tandcnum + '/Part1/' + 'PS'

bdir = '/sxi01/html/TandC/DB/' + tandcnum + '/Part1/'
cmd = 'cp -p ' + datedir + '/* ' + bdir		& print,cmd & spawn,cmd
cmd = 'mv ' + bdir+'*.gif ' + bdir+'GIF/.'		& print,cmd & spawn,cmd
cmd = 'mv ' + bdir+'*.ps ' + bdir+'PS/.'		& print,cmd & spawn,cmd

skip:

get_lun,u
;openw,u,'./test' + tandcnum + '.html'
openw,u,'/sxi01/html/TandC/DB/' + tandcnum + '/index.shtml'

spawn,'ls ' + datedir + '/*.usr',usrfiles
;usrfiles_stripped=strtrim(usrfiles,2)
break_file, usrfiles, dsk_log, dir, filnam, ext
usrfiles_stripped = filnam + ext
print,usrfiles_stripped

printf,u,'<HTML>'
printf,u,'<HEAD>'
printf,u,'</HEAD>'
printf,u,'<BODY bgcolor="white" link=red>'
printf,u,'<TABLE bgcolor="lightblue" height=50 width=100% >'
printf,u,'<TR>'
printf,u,'<TD>'

printf,u,'<CENTER><H3>'+title+'</H3></CENTER>'

printf,u,'</TD>'
printf,u,'</TR>'
printf,u,'</TABLE>'
printf,u,'<br><br>'
printf,u,'<h1> Plans and Procedures</h1>'
printf,u,'<ul>'
printf,u,'<li><A HREF="http://www.lmsal.com/SXI/PM/Doc/vcur/SXI991629">Test Procedure</A>'
printf,u,'</ul>'
printf,u,'<h1> Data</h1>'
printf,u,'<ul>'

printf,u,'<li><A HREF="Part1">Raw Directory</A> or <A HREF="Part1/GIF">GIF Summaries</A> or <A HREF="Part1/PS">Postscript Files</A>'

printf,u,'</ul>'
printf,u,'<h1> STOL User Log Files</h1>'

printf,u,'<pre>'
printf,u,usrfiles_stripped,format='(a)'
printf,u,'</pre>'
printf,u,'<h1> Photographs of the Setup</h1>'
printf,u,'<ul>'

if (n_elements(photodir0) eq 0) then photodir0 = 'none'
photodir = photodir0
if (data_type(photodir) ne 7) then photodir = strtrim(photodir,2)
if (photodir eq 'none') then printf,u,'<li> No Photos Available'
if (photodir ne 'none') then printf,u,'<li> <A HREF=http://www.lmsal.com/SXI/PhotoLib?cmd=view_image&photo_id=' + $
								photodir +'>Photographs</A>'

printf,u,'</ul>'
;printf,u,'<h1> Final Results and Reports</h1>'
;printf,u,'<ul>'
;printf,u,'<li>TBD'
;printf,u,'</ul>'
printf,u,'<BR>'
printf,u,'<BR>'
printf,u,'<BR>'

printf,u,'<!--#include virtual="/SXI/trailer_bar.html"-->
printf,u,'<P align=right><FONT size=-2>Date last modified: <!--#echo var="LAST_MODIFIED"-->

printf,u,'</BODY>'
printf,u,'</HTML>'


close,u


return 
end
;--------------------------------------------------------
pro go_mk_lfft_db_web, skip_copy=skip_copy

input, 'Enter tandcnum (ie: 901)', tandcnum, '901
spawn, ['ls', getenv('SXI_LFFT_DIR')], /noshell
input, 'Enter date dir name (ie: 020115.173109)', datedir, '020115.173109'
input, 'Enter title', title, 'FM2 LFFT #1 Pre-Environmental Testing (15-Jan-02)'
input, 'Enter photo ID', photoid, '4075'

mk_lfft_db_web, tandcnum, concat_dir(getenv('SXI_LFFT_DIR'),datedir), title, photoid, skip_copy=skip_copy

end
