;
;	more /sxi01/html/TandC/DB/590/case_c6/real_time_avg.txt
;
function tbtv_summary_all_case_s1, casename0, snode2
    ;
    filnam = casename0 + '.temps'
    filnam = str_replace(filnam, '_', '')               ;remove underscore
    filnam = str_replace(filnam, 'ctb', 'Ctb')          ;name fix
    filnam = str_replace(filnam, 'c6', 'C6')            ;name fix
    filnam = str_replace(filnam, 'ctb2', 'Ctb2')        ;name fix
    filnam = str_replace(filnam, 'h6', 'H6')            ;name fix
    filnam = str_replace(filnam, 'htb', 'Htb')          ;name fix
    filnam = str_replace(filnam, 'htb2', 'Htb2')        ;name fix
    filnam = str_replace(filnam, 'h3tb', 'Htb3')        ;name fix
    filnam = str_replace(filnam, 'h7tb', 'Htb7')        ;name fix


out = ''
                infil2 = '/sxi01/html/Inst/Thermal/TB_FM1/sinks_' + $
                                str_replace(filnam, '.temps', '.inc')
                if (file_exist(infil2)) then begin
                    spawn, ['grep', snode2, infil2], r, /noshell
                    if (n_elements(r) ne 1) then stop, 'spawn resulted in more than 1 hit
                    arr = str2arr(r(0))
                    if (strpos(arr(0), 'GEN') eq -1) then out = arr(1) $
                                                        else out = arr(3)
                end

return, out
end
;----------------------------------------------
;outdir = getenv('SXI_TBTV_HTML_DIR')
outdir = ['/sxi01/html/TandC/DB/589', '/sxi01/html/TandC/DB/590']
dirs = file_list(outdir, 'case*', /cd, file=casename)
ss = where(strpos(dirs,'case_set') eq -1)
dirs = dirs(ss)
casename = casename(ss)
n = n_elements(dirs)
ff = file_list(dirs, 'real_time*.txt')
;
mnem = str2arr('T_T_Cold_Aft, T2_T_Hot_Aft, T_T_Hot_Aft_CEB_TW, T_T_Hot_Aft,  T_T_Hot_Aft_Cen_TW, T_T_Hot_Aft_UL_TW, 9101, 9102, ' + $
		'CCD_Rad_Center, CCD_Rad_Edge, sxi_ccdzonetmp, sxi_pcm_ccdtmp')
outfilnam = 'summary_all_cases.html'
;
mnem = str2arr('T_T_Hot_Aft_Cen_TW, T_T_Hot_Aft_UL_TW, ' + $
		'CCD_Rad_Center, CCD_Rad_Edge, sxi_ccdzonetmp, sxi_pcm_ccdtmp, ' + $
		'sxi_cebtmp, sxi_pcm_cebtmp, CEB_Bak_Rad_Flng, Aft_Tube_CEB_Mnt, Aft_Plat_CCD')
outfilnam = 'summary_ceb_all_cases.html'
;
mnem = str2arr('sxi_pebtmp, sxi_pcm_pebtmp, PEB_Base, PEB_Side_Wall, PEB_Top_Wall, ' + $
		'T_E_Hot_Bottom1, T_E_Hot_Bottom2, T2_E_Hot_Bottom1')
outfilnam = 'summary_peb_all_cases.html'
;
mnem = str2arr('sxi_debtmp, sxi_pcm_debtmp, DEB_Base, DEB_Side_Wall, DEB_Top_Wall, ' + $
		'T_E_Hot_Bottom1, T_E_Hot_Bottom2, T2_E_Hot_Bottom1')
outfilnam = 'summary_deb_all_cases.html'
;
mnem = str2arr('sxi_hebtmp, sxi_pcm_hebtmp, HEB_Base, HEB_Side_Wall, HEB_Top_Wall, ' + $
		'T_E_Hot_Bottom1, T_E_Hot_Bottom2, T2_E_Hot_Bottom1')
outfilnam = 'summary_heb_all_cases.html'
;
mnem = strtrim(strlowcase(mnem),2)
;
nn = n_elements(mnem)
out = strarr(nn+1, n+1)
out(1:*,0) = mnem
out(0,1:*) = casename
for idir=0,n_elements(dirs)-1 do begin
    ff = file_list(dirs(idir), 'real_time*.txt')
    for ifil=0,n_elements(ff)-1 do begin
	print, 'Reading: ', ff(ifil)
	mat = rd_tfile(ff(ifil), 5)
	v0 = strlowcase(reform(mat(0,*)))
	v1 = reform(mat(1,*))
	for imnem=0,nn-1 do begin
	    ss = where( v0 eq mnem(imnem), nss)
	    if (nss ge 1) then out(imnem+1, idir+1) = v1(ss(nss-1))
	    if (strmid(mnem(imnem),0,1) eq '9') then out(imnem+1, idir+1) = tbtv_summary_all_case_s1(casename(idir), mnem(imnem))
	end
    end
end
;
ss = where(out eq '-3276.70', nss)
if (nss ne 0) then out(ss) = 'OPEN TC'
ss = where(out eq '9999.00', nss)
if (nss ne 0) then out(ss) = ''
ss = where(out eq '********', nss)
if (nss ne 0) then out(ss) = ''
ss = where(out eq '****************', nss)
if (nss ne 0) then out(ss) = ''

;
break_file, dirs, dsk_log, dir00, filnam
out2 = ['<html><head><title>FM1 Thermal Balance Temperatures</title><body>',$
        strtab2html(out), $
        ' ', $
        'Page Generated: ' + !stime, $
        '</body></html>']
;
prstr, out2, file=concat_dir(outdir(n_elements(outdir)-1), outfilnam)
;
end