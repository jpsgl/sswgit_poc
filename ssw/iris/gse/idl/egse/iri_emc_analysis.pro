pro iri_emc_analysis,infils,titl,hc=hc,jpg=jpg

blk=100

mreadfits,infils,head

nimg=n_elements(head)
sdarr=fltarr(4,nimg)
avgarr=fltarr(4,nimg)
titarr=['Region 1','Region 2','Region 3','Region 4']

nloops=1
if (nimg gt blk) then nloops=ceil(nimg/(float(blk)))
for k=0,nloops-1 do begin
	first=k*blk
	last=((k+1)*blk)-1
	nimg_loop=blk
	if ((k eq (nloops-1)) and ((nimg mod blk) ne 0)) then begin
		last=first+((nimg mod blk)-1)
		nimg_loop=(nimg mod blk)
	endif
	mreadfits,infils(first:last),head,img
	for i=0L,nimg_loop-1 do begin

		dex=(k*blk)+i

		reg1=img(0:2073,0:19,i)
		reg2=img(0:2073,1076:1095,i)
		reg3=img(2074:4143,0:19,i)
		reg4=img(2074:4143,1076:1095,i)
		
		sdarr(0,dex)=stdev(reg1)
		sdarr(1,dex)=stdev(reg2)
		sdarr(2,dex)=stdev(reg3)
		sdarr(3,dex)=stdev(reg4)
	
		avgarr(0,dex)=mean(reg1)
		avgarr(1,dex)=mean(reg2)
		avgarr(2,dex)=mean(reg3)
		avgarr(3,dex)=mean(reg4)
	endfor
endfor

loadct,7
if ((keyword_set(hc)) and (not keyword_set(jpg))) then begin
	set_plot,'ps'
	fn='/itosout/analysis/emi/'+titl+'.ps'
	device,filename=fn,/land
endif else begin
	window,/free,title=titl,xsiz=1200,ysiz=1080
	xyouts,0,0,'Red Line = Threshold'
endelse

mreadfits,infils,head

for i=0,3 do begin
	!p.multi=[4-i,2,2]
	plot,head.fsn,sdarr(i,*),tit=titarr(i),xtit='FSN',ytit='StDev (thrshold=10)',psym=-2,xtickformat="(i10)",yrange=[0,10]
	thresh=lonarr(n_elements(sdarr(i,*)))+10
	oplot,head.fsn,thresh,color=[128]
endfor

!p.multi=0

if ((keyword_set(hc)) and (not keyword_set(jpg))) then begin
	device,/close
	pprint, fn
	set_plot,'x'
endif

if (keyword_set(hc) and keyword_set(jpg)) then print, '!!!!!!!!!!          jpg keyword overrides hc keyword          !!!!!!!!!!'
if keyword_set(jpg) then begin
	fn='/itosout/analysis/emi/'+titl+'.jpg'
	pb_win2jpg, fn
	print, 'Wrote file '+fn
endif

fn='/itosout/analysis/emi/'+titl+'.txt'
out=fn
out=[out,'FSN       INSTRUME  DATE_OBS                  MEAN1       STDEV1      MEAN2       STDEV2      MEAN3       STDEV3      MEAN4       STDEV4']
for j=0,n_elements(head.fsn)-1 do begin
	fsn=strcompress(head(j).fsn,/rem)
	cam=strcompress(head(j).instrume,/rem)
	dateobs=strcompress(head(j).isppktim)
	rr=where(sdarr(*,j) gt 10, nrr)
	if (nrr ne 0) then fsn='***'+fsn
	out=[out,string(fsn,cam,dateobs,avgarr(0,j),sdarr(0,j), $
		avgarr(1,j),sdarr(1,j),avgarr(2,j),sdarr(2,j),avgarr(3,j),sdarr(3,j),format="(2(a-10),a-26,4(f-012.2,f-12.2))")]
endfor
out=[out,'Processed '+strcompress(nimg,/rem)+' images']
prstr,out,file=fn
if (keyword_set(hc)) then prstr,out,/land
print,'Wrote file '+fn
prstr,out,/nomore

end
