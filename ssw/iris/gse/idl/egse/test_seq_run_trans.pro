;+
;NAME:
;	test_seq_run_trans
;HISTORY:
;	Written 26-Mar-01 by M.Morrison
;-
;
nimg = 20
infil = sxi_stol2files(date, 'seq_run_trans', nimg, out=cmdout)
break_file, infil, dsk_log, dir, filnam, ext
;
if (infil(0) eq '') then begin
    print, 'Cannot find image files.  Stopping...'
    stop
end
;
outdir = '$SXI_LFFT_DIR/tmp'

sttim = fmt_tim(cmdout)
entim = fmt_tim(anytim2ints(sttim, off=22*60.))
sxi_disp_img_head, sttim, entim, /hc, code=0, outfil = concat_dir(outdir, 'seq_trans_'+filnam(0)+'_c0' + '.txt')
sxi_disp_img_head, sttim, entim, /hc, code=4, outfil = concat_dir(outdir, 'seq_trans_'+filnam(0)+'_c4' + '.txt')
;
end