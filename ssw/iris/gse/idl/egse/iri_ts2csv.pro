pro iri_ts2csv, sttim, entim, out_prefix=out_prefix

apid='0x0054'
if (not keyword_set(out_prefix)) then out_prefix='~/iri_ts2csv_'

mnem=['I_TS01_PRI_MIR_MNT_FWD','I_TS02_SEC_MIR_HOUSING','I_TS03_PRI_MIR_MNT_AFT','I_TS04_TELE_FWD_FLANGE','I_TS05_TELE_AFT_FLANGE','I_TS06_TELE_MID_RING','I_TS07_PZT_PRE_AMP', $
'I_TS08_GT_WEDGE_MOTOR','I_TS09_GT_BARLOW','I_TS10_CEB_NEG_X_AXIS','I_TS11_CEB_POS_X_AXIS','I_TS12_HOPA','I_TS13_FRNT_APER_ASSY','I_TS14_SPECT_OP_POS_X','I_TS15_SPECT_OP_POS_Y', $
'I_TS16_SPECT_OP_NEG_X','I_TS17_SPECT_OP_NEG_Y','I_TS18_SPECT_OP_POS_Z','I_TS19_SPECT_OP_NEG_Z','I_TS20_PRI_MIR_ABS','I_TS21_TELE_ADAPTER','I_TS22_IEB_PCONV_BPLATE','I_TS23_IEB_PCONV_HOTCMP', $
'I_TS24_IEB_CPU','I_TS25_IEB_OBC_OSC','I_TS26_IEB_MECH_BOARD_1','I_TS27_IEB_MECH_BOARD_2','I_TS28_SEC_MIR_ABS_PLT','I_TS29_TELE_FWD_FLNG_GT','I_TS30_TELE_MID_RING_GT']
nmnem=n_elements(mnem)

if (n_elements(sttim) eq 0) then sttim = anytim2ints(ut_time(), off=-60.*60.)
if (n_elements(entim) eq 0) then entim = anytim2ints(sttim, off=60.*60.)

outfil=out_prefix+time2file(sttim,/date)+'_'+time2file(entim,/date)+'.txt'
;out=string( mnem, format="(30A-25)")
out=string( mnem, format="(30(',',A))")
info=sag_get_mnem(sttim,entim,mnem)
tim = info.(0).daytime
;help, tim, /str
v = info.(0).value
;help, v
;print, v
if (n_elements(mnem) ne 1) then begin
	tmp =string(fmt_tim(tim), format="(A)")
	for j=0,nmnem-1 do tmp = tmp + string(v(*,j), format="(',',F7.3)")
	v = temporary(tmp)
end

openw, lun, outfil, /get_lun
;printf, lun, string( 'Time', mnem, format="(A,30A)")
printf, lun, string( 'Time'+out)
printf, lun, v
close, lun
free_lun, lun

end
