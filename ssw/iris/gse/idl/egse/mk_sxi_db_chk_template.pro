pro mk_sxi_db_chk_template, sort_option=sort_option, $
				select=select
;
;
;
sag_sxi_rd_hk_db, tlm, alg, dsc, cmd, /qdebug
;
if (n_elements(sort_option) eq 0) then sort_option = 1
case sort_option of
    1: begin
	ss1 = where(tlm.stbyte(0) ge 0, nss)	;source location identified
	ss2 = where(tlm.stbyte(0) lt 0, nss)	;source location not identified
	;
	v1 = tlm(ss1).apid(0)*1000L + tlm(ss1).stbyte(0)
	v2 = tlm(ss2).apid(0)*1000L + tlm(ss2).stbyte(0)
	ss = [ss1(sort(v1)), ss2(sort(v2))]
	end
endcase
;
fmt1 = '(a23,1x, a7,1x, a39, 1x, a7, 1x, a7, 1x, "# ", a40)'
fmtc = '(1x, z5.5,1x,i3,1x,a25)
tit = ['Telemetry Mnemonic', 'ConvTyp', 'Command to issue', 'ValMin', 'ValMax', 'Comments']
dash = replicate('-----------------------------------------------------------------',6)

n = n_elements(ss)
out_tab = strarr(n+2)
out_lst = strarr(n)
out_tab(0) = string(tit, format=fmt1)
out_tab(1) = string(dash, format=fmt1)
for i=0L,n-1 do begin
    tlm0 = tlm(ss(i))
    ;
    comment = string(tlm0.apid(0), tlm0.stbyte(0), $
				tlm0.descr, format=fmtc)
    out0 = string(tlm0.name+'                     ', tlm0.conv_code, $
				'.', '.', '.', $
     			        comment, format=fmt1)
    out_tab(i+2) = out0

    fmt2 = '(1x, a25, 1x,a3, 1x,a50, 1(1x,z5.5,1x,i3), i3, i3, 1x,a1,1x,a12,1x,a9)'
    out0 = string(tlm0.name+'                  ', tlm0.type, tlm0.descr, $
                tlm0.apid(0), tlm0.stbyte(0), $
                tlm0.stbit, tlm0.nbit, $
                tlm0.conv_code, tlm0.conv, tlm0.unit, format=fmt2)
    print, out0
    out_lst(i) = out0
end

prstr, out_lst, file='$SAG_TELEM_INFO/sxi_db_check_template.lst'
prstr, out_tab, file='$SAG_TELEM_INFO/sxi_db_check_template.tab'
end

