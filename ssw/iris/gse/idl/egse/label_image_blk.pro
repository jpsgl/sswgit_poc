pro label_image_blk, in_mnem, in_val, in_time, $
	imgfil=imgfil, infil=infil, zfile=zfile

if (n_elements(imgfil) eq 0) then imgfil = '/sxi01/dbase/tb_tv/telescope_tc_map.gif'
if (n_elements(infil) eq 0) then   infil = '/sxi01/dbase/tb_tv/telescope_tc_map.img_loc'
;
read_gif, imgfil, img, r, g, b
wdef, image=img, /already
tv, img
tvlct, r, g, b
;
mat = rd_ulin_col(infil, /nohead, nocomment='#')
xpos = fix(reform(mat(0,*)))
ypos = fix(reform(mat(1,*)))
mnem = strupcase(reform(mat(3,*)))
n = n_elements(mnem)
;
nin = n_elements(in_mnem)
;
xsiz = 80
ysiz = 20
blank = bytarr(xsiz, ysiz)
for i=0,nin-1 do begin
    ss = where(mnem eq strupcase(in_mnem(i)), nss)
    if (nss eq 1) then begin
	ii = ss(0)
	x0 = (xpos(ii)-xsiz/2) < (!d.x_size-xsiz-1) > 0
	y0 = (ypos(ii)-ysiz/2) < (!d.y_size-ysiz-1) > 0
	tv, blank, x0, y0 
	xyouts, x0+xsiz/2, ypos(ii)-5, in_val(i), align=0.5, /dev, color=255
    end
end
;
if (keyword_set(in_time)) then begin
     tv, bytarr(!d.x_size,30), 0, 0
     xyouts, 0, 0, in_time, /dev, size=2, color=255
end
;
if (!d.name eq 'Z') and (keyword_set(zfile)) then zbuff2file, zfile
end