pro iris_proc_emi_str, hk, smoot=smoot, color=color, hc=hc,qlinestyle=qlinestyle, filter1d=filter1d
;+
; $Id: iris_proc_emi_str.pro 11660 2012-12-15 20:26:53Z gheyman $
; $Name:  $
;
; Name: iris_proc_emi_str
;
; Purpose: Process housekeeping data (hk) into a set of plots using sag_hkplot.
;	Plots default to the screen, but with /hc switch will be writen to *.gif
;	files for archiving or printing.
;
; Parameter:
;	hk 		- hk data structure from sag_get_mnem()
;	smoot	- enable "box car" smoothing function, of size specified
;	color	- enable color plots
;	qlinest	- enable qlinestyles
;       filter1d- Remove bad data by finding where values exceed the
;                 "smoothed 100" values by more than 3 sigma
; 	hc 		- switch to make a plot file
;
; History: G.Linford, 16 July 2007
;			April 2008, G.Linford, modified to run for AIA and HMI
;
;	$Log: sdo_proc_emi_str.pro,v $
;	Revision 1.1  2008/04/11 15:12:23  linford
;	name change from aia_proc_emi_str.pro to sdo
;	
;-
;-- stats common block: mnemonics, min-stddev of dist., n-sigma val for outliers
common aia_tlm_stats, smnem, minsig, nsigout, fdialog, fnsigma

tags = tag_names(hk)
items = hk.(0).mnem		; tlm points
nitems = n_elements(items)	; # of tlm points 

;--- DEBUG CHANGE added
;print,'debug adding stats to ploting:"
;help,hk.(0),/str
;print,hk.(0).mnem
;stop
;---- END DEBUG CHANGE

if (nitems gt 4) then begin		;# groups of 4 or less TLM pts/plot
	nplots = nitems/4
	if ( (nitems mod 4) eq 0 ) then begin 	;# easy case
		sti = 0
		eni = 3
		for i=0, nplots -1 do begin
			xxx = create_struct('DAYTIME',hk.(0).daytime,'VALUE',hk.(0).value[*,sti:eni],'MNEM',hk.(0).mnem[sti:eni], $
				'DESCR',hk.(0).descr[sti:eni],'CODE',hk.(0).code[sti:eni],'FLAG',hk.(0).flag[sti:eni])
			struct = create_struct('Taia', xxx)
			;help,struct.taia,/str
	
			pb_sag_hkplot,struct,/label,color=color, smoo=smoot,qlinestyle=qlinestyle, filter1d=filter1d
			
			sti = sti + 4
			eni = eni + 4
		endfor
	endif else begin 	
		sti = 0
		eni = 3
		for i=0, nplots -1 do begin
			xxx = create_struct('DAYTIME',hk.(0).daytime,'VALUE',hk.(0).value[*,sti:eni],'MNEM',hk.(0).mnem[sti:eni], $
				'DESCR',hk.(0).descr[sti:eni],'CODE',hk.(0).code[sti:eni],'FLAG',hk.(0).flag[sti:eni])
			struct = create_struct('Taia', xxx)
			;help,struct.taia,/str
			if keyword_set(smoot) then begin
				pb_sag_hkplot,struct,/label,color=color, smoo=smoot,qlinestyle=qlinestyle, filter1d=filter1d
			endif else begin
				pb_sag_hkplot,struct,/label,color=color,qlinestyle=qlinestyle, filter1d=filter1d
			endelse
			
			sti = sti + 4
			eni = eni + 4
		endfor
		; - must plot reminder tlm pts
		eni = nitems - 1
		xxx = create_struct('DAYTIME',hk.(0).daytime,'VALUE',hk.(0).value[*,sti:eni],'MNEM',hk.(0).mnem[sti:eni], $
				'DESCR',hk.(0).descr[sti:eni],'CODE',hk.(0).code[sti:eni],'FLAG',hk.(0).flag[sti:eni])
		struct = create_struct('Taia', xxx)
		;help,struct.taia,/str
		if keyword_set(smoot) then begin
				pb_sag_hkplot,struct,/label,color=color, smoo=smoot,qlinestyle=qlinestyle, filter1d=filter1d
		endif else begin
				pb_sag_hkplot,struct,/label,color=color,qlinestyle=qlinestyle, filter1d=filter1d
		endelse
	endelse
endif else begin			;# plots for less then 4 tlm pts
	if ( strMatch(hk.(0).mnem[0], 's_c_ssimpwr??') eq 0 ) then begin		; don't have the SPW packet 
	 sti = 0
	 eni = nitems - 1
	 xxx = create_struct('DAYTIME',hk.(0).daytime,'VALUE',hk.(0).value[*,sti:eni],'MNEM',hk.(0).mnem[sti:eni], $
				 'DESCR',hk.(0).descr[sti:eni],'CODE',hk.(0).code[sti:eni],'FLAG',hk.(0).flag[sti:eni])
	 struct = create_struct('Taia', xxx)
	 ;help,struct.taia,/str
	 if keyword_set(smoot) then begin
			 pb_sag_hkplot,struct,/label,color=color, smoo=smoot,qlinestyle=qlinestyle, filter1d=filter1d
	 endif else begin
			 pb_sag_hkplot,struct,/label,color=color,qlinestyle=qlinestyle, filter1d=filter1d
	 endelse
	endif else begin
	 sti = 0
	 eni = nitems - 1
	 for i=0, 1 do begin
	 	xxx = create_struct('DAYTIME',hk.(0).daytime,'VALUE',hk.(0).value[*,i],'MNEM',hk.(0).mnem[i], $
				 'DESCR',hk.(0).descr[i],'CODE',hk.(0).code[i],'FLAG',hk.(0).flag[i])
	 	struct = create_struct('Taia', xxx)
	 	;help,struct.taia,/str
	 	if keyword_set(smoot) then begin
			 pb_sag_hkplot,struct,/label,color=color, smoo=smoot,qlinestyle=qlinestyle, filter1d=filter1d
	 	endif else begin
			 pb_sag_hkplot,struct,/label,color=color,qlinestyle=qlinestyle, filter1d=filter1d
	 	endelse
	 endfor
	endelse
endelse

end
		
