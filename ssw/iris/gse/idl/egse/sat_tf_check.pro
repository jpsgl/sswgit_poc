pro sat_tf_check, outfil=outfil, hc=hc, date=date, qstop=qstop, $
			qplot=qplot, $
			infil=infil0, ist=ist, nout=nout, $
			qdata=qdata
;
;HISTORY:
;V1.00	Written 30-Oct-00 by M.Morrison
;V1.01	15-Nov-00 (MDM) - Added /QSTOP
;V1.02	17-Nov-00 (MDM) - Added /QPLOT
;V1.03	19-Dec-00 (MDM) - Added keyword infil/ist/nout input
;Brennan Temp fix SAT 25/9/03
;
;
if (n_elements(barr) eq 0) then begin
    if (n_elements(date) eq 0) then date = ut_time()
    fid = time2file( date, /date)
    infil = '$GSE_TF_FILE_DIR/' + fid + '_fep_tm.tf'
    if (keyword_set(infil0)) then infil = infil0
    print, 'Reading ', infil

    if (n_elements(ist) eq 0) then ist = 0
    siz = file_stat(infil, /size)
    n = siz/852
    if (keyword_set(nout)) then n = nout
    ;
    barr = bytarr(852, n)
    close, 1
    openr, 1, infil
    point_lun, 1, ist*852L
    readu, 1, barr
    close, 1
end else begin
    infil = 'INPUT passed in by hand
end
;
;out = string(barr(0:39,*), format='(4(z2.2), 1x,3(1x,2z2.2), ' + $
;		'" - ", 3(1x,2z2.2)," - ", (1x,4z2.2,1x,2z2.2), " - ", 18(1x,z2.2))')
out = strarr(n)
for i=0,n-1 do out(i) = string(barr(0:39,i), format='(4(z2.2), 1x,3(1x,2z2.2), ' + $
		'" - ", 3(1x,2z2.2)," - ", (1x,4z2.2,1x,2z2.2), " - ", 18(1x,z2.2))')
;
out = ['SAT_TF_CHECK  Ver 1.03  Program run: ' + !stime, ' ', $
	'Input file: ' + infil + '  Starting Record: ' + strtrim(ist,2), $
	' ', out]
;
if (keyword_set(qdata)) then out = [out, ' ', string(barr(22:*), format='(40(1x,z2.2))')]
;
if (keyword_set(qplot)) then begin
    v = deriv_arr(fix(barr(6,*)))
    !p.multi=[0,1,2]
    plot, v, xtit='Transfer frame', ytit='Delta VC count'
    plot, v, yr=[-24, 24], psym=1, xtit='Transfer frame', ytit='Delta VC count'
end
;
prstr, out, /nomore
if (keyword_set(outfil)) then prstr, out, file=outfil
if (keyword_set(hc)) then prstr, out, file=outfil, /hc, /land, nodelete=keyword_set(outfil)
if (keyword_set(qstop)) then stop
end
