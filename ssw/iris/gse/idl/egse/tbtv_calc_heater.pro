;HISTORY:
;V1.00	Written 27-Sep-01 by M.Morrison
;V1.01	 1-Oct-01 (MDM) - Made it driven by table
;V1.02	 1-Oct-01 (MDM) - Adjusted operational heater 100% duty
;			- 
;----------------------------------------------------------
pro tbtv_calc_heater_s1, label
;
;
;
print, label
tbeep, 2
;stop
;
end
;----------------------------------------------------------
pro tbtv_calc_heater, out, label, entim, nhr
;
;
;
sttim = anytim2ints(entim, off=-60*60*nhr)
;
print, 'Running ' + label + ' for ' + fmt_tim(sttim) + ' to ' + fmt_tim(entim)
;
out = [out, 'TBTV_CALC_HEATER  Ver 1.02 Program Run: ' + !stime, $
	' ', $
	'------ ' + label + '   Sttim: ' + fmt_tim(sttim) + $
		'  Entim: '+ fmt_tim(entim) + '------']
;

mnem = ['SXI_FORZONETMP', 'SXI_FORZONEDBL', 'SXI_FORZONEDBH', 'SXI_HTRFORE', 'SXI_FORZONEDC', 'SXI_FORZONEHIDC', $
        'SXI_AFTZONETMP', 'SXI_AFTZONEDBL', 'SXI_AFTZONEDBH', 'SXI_HTRAFT',  'SXI_AFTZONEDC', 'SXI_AFTZONEHIDC', $
        'SXI_CCDZONETMP', 'SXI_CCDZONEDBL', 'SXI_CCDZONEDBH', 'SXI_HTRCCD',  'SXI_CCDZONEDC', 'SXI_CCDZONEHIDC', $
        'SXI_POWER_REG']

info = sag_get_mnem( ut_time(sttim), ut_time(entim), mnem, /raw)
info.hk.value(*,0) = 255-info.hk.value(*,0)
info.hk.value(*,6) = 255-info.hk.value(*,6)
;
n = n_elements(info.hk.value(*,0)) * 1. - 1
labarr = ['Fore', 'Aft ', 'CCD ']
;watarr = [17.8, 11.3, 9.4]
watarr0 = [41.4^2/99.2, 41.4^2/156.6, 28.8^2/105.5]	;at heater (voltage drop over harness)
watarr1 = [42.0^2/99.2, 42.0^2/156.6, 30.0^2/105.5]	;at power supply
;
out = [out, ' ', 'Operational Heaters Analysis.  # Samples: ' + strtrim(n,2), ' ']
;
tot_op0 = 0.
tot_op1 = 0.
for ii=0,12,6 do begin
    tics_on = fltarr(n)
    temp = info.hk.value(0:n-1,ii)
    dblo = info.hk.value(0:n-1,ii+1)
    dbhi = info.hk.value(0:n-1,ii+2)
    stat = info.hk.value(1:n,  ii+3)      ;status goes with the previous temperature
    dc   = info.hk.value(0:n-1,ii+4)                      ;duty cycle
    dchi = info.hk.value(0:n-1,ii+5)                      ;hi duty cycle
    dblo0 = dblo(0)
    dbhi0 = dbhi(n-1)
    dc0 = dc(0)
    dchi0 = dchi(0)
    qhi = mask(stat, 3, 1)
    qlow = mask(stat, 2, 1)
    ;
    if (min(dc) ne max(dc)) then tbtv_calc_heater_s1, 'DC not same throughout'
    if (min(dchi) ne max(dchi)) then tbtv_calc_heater_s1, 'DCHI not same throughout'
    if (min(dblo) ne max(dblo)) then tbtv_calc_heater_s1, 'DBLOW not same throughout'
    if (min(dbhi) ne max(dbhi)) then tbtv_calc_heater_s1, 'DBHI not same throughout'
    ;
    ;-- simply showing status of on
    ss=where( (temp ge dblo0) and (temp le dbhi0) and (stat ne 8) and (stat gt 0), nss)     ;on
    if (nss ne 0) then tics_on(ss) = dc0
    ;
    ;-- On, but "hard-on" because value is below DBLOW
    ss2=where( (temp lt dblo0), nss2)
    if (nss2 ne 0) then tics_on(ss2) = dchi0 > dc0       ;fell below low DB
    ;
    ;-- Over high and disabled (off)
    ss3=where( (stat eq 8), nss3)
    ;
    ddc = total(tics_on)/n / 32 * 100.
    if (min(stat) eq max(stat)) and (max(stat) eq 0) then ddc = 0
    ;out0 = out0 + [string(labarr(ii/6), dc0/32.*100., nss/n*100, nss2/n*100, nss3/n*100, ddc, format='(2x, a4, 5f6.1)'), $
    ;		string(ddc/100.*watarr(ii/6), format='(2x,4x,24x,f6.1)')]
    out = [out, 'Operational ' + labarr(ii/6), $
	'       Commanded Duty Cycle:  ' + string(dc0/32.*100., format='(f6.1,"%")') + $
				'   HIDC: ' + string(dchi0/32.*100., format='(f6.1,"%")'), $
	'       % within DB & active:  ' + string(nss/n*100, format='(f6.1,"%")'), $
	'       % below DB Low:        ' + string(nss2/n*100, format='(f6.1,"%")'), $
	'       % disabled (off):      ' + string(nss3/n*100, format='(f6.1,"%")'), $
	'       Derived duty cycle:    ' + string(ddc, ddc/100.*watarr0(ii/6), format='(f6.1,"%", f6.1, " watts")') + $
			' (100% max of ' + string(watarr0(ii/6), format='(f6.1,")")')]
    tot_op0 = tot_op0 + ddc/100.*watarr0(ii/6)
    tot_op1 = tot_op1 + ddc/100.*watarr1(ii/6)
end
;
out = [out, ' ', 'Op Heater Tot Derived Above:         ' + string(tot_op0, format='(f6.1, " watts")'), $
		 'Watts derived drawn from supply:     ' + string(tot_op1, format='(f6.1, " watts")')]

;---------------

mnem=['SXI_GPIB_MP_IOUT', 'SXI_GPIB_SH_IOUT']
info = sag_get_mnem(ut_time(sttim), ut_time(entim), mnem)

v = info.(0).value(*,0)
vavg = total(v)/n_elements(v)
vmin = min(v, max=vmax)
out = [out, ' ', $
		'SXI_GPIB_MP_IOUT (instrument bus)', $
		'       Current Avg/Min/Max ' + string(vavg, vmin, vmax, format='(3f8.3)'), $
		'       Watts Avg/Min/Max   ' + string(vavg*42, vmin*42, vmax*42, format='(3f8.3)'), $
		'       Avg Heater Watts    ' + string((vavg-(vmin>.77))*42, format='(f8.3)'), $
		'       Diff from above     ' + string((vavg-(vmin>.77))*42-tot_op1, format='(f8.3)'), $
		' ']
;
v = info.(0).value(*,1)
vavg = total(v)/n_elements(v)
vmin = min(v, max=vmax)
out = [out, ' ', $
		'SXI_GPIB_SH_IOUT (survival heater bus)', $
		'       Current Avg/Min/Max ' + string(vavg, vmin, vmax, format='(3f8.3)'), $
		'       Watts Avg/Min/Max   ' + string(vavg*53, vmin*53, vmax*53, format='(3f8.3)'), $
		' ', string(12b)]


;prstr, out
;pause

end
;----------------------------------------------------
;
if (keyword_set(qskip)) then goto, skip
if (keyword_set(qtest)) then begin
   tbtv_calc_heater, out, 'fm1_tv_test_h2', '27-SEP-01  03:10', 6
   stop
end
;
out = ''
;infil = '/sxisw/dbase/tb_tv/tbtv_times_fm1_tv.tab'	& prefix = 'fm1_tv_'
;infil = '/sxisw/dbase/tb_tv/tbtv_times_fm2_tv.tab'	& prefix = 'fm2_tv_'
infil = '/sxisw/dbase/tb_tv/tbtv_times_fm2_tb.tab'	& prefix = 'fm2_tb_'
outfil = str_replace(infil, '.tab', '.calc_heat')
mat = rd_ulin_col(infil, /nohead, nocomment='#')
n = n_elements(mat(0,*))
for i=0,n-1 do tbtv_calc_heater, out, prefix + mat(0,i), mat(2,i), float(mat(3,i))
prstr, out, file=outfil
;
stop
skip:
out = ''
infil = '/sxisw/dbase/tb_tv/tbtv_times_fm1_tb.tab'
outfil = str_replace(infil, '.tab', '.calc_heat')
mat = rd_ulin_col(infil, /nohead, nocomment='#')
n = n_elements(mat(0,*))
for i=0,n-1 do tbtv_calc_heater, out, 'fm1_tb_' + mat(0,i), mat(2,i), float(mat(3,i))
prstr, out, file=outfil


;goto, skip
;
;tbtv_calc_heater, out, 'fm1_tb_c09', '16-Jul-01 2:05', 3
;tbtv_calc_heater, out, 'fm1_tb_c10', '16-Jul-01 11:00', 3
;tbtv_calc_heater, out, 'fm1_tb_c11', '14-Jul-01 16:20', 3
;tbtv_calc_heater, out, 'fm1_tb_c13', '15-Jul-01 18:00', 3
;
;stop
;tbtv_calc_heater, out, 'fm1_tv_c1',   '19-sep-01  9:45', 6
;tbtv_calc_heater, out, 'fm1_tv_ctb',  '20-sep-01  2:10', 3
;tbtv_calc_heater, out, 'fm1_tv_ctb2', '21-sep-01  7:00', 3
;tbtv_calc_heater, out, 'fm1_tv_h1',   '22-sep-01  1:30', 6
;tbtv_calc_heater, out, 'fm1_tv_htb',  '25-sep-01  7:00', 3
;tbtv_calc_heater, out, 'fm1_tv_htb2', '25-sep-01 15:20', 3
;tbtv_calc_heater, out, 'fm1_tv_c2',   '26-sep-01  9:40', 6
;skip:
;tbtv_calc_heater, out, 'fm1_tv_h2',   '27-sep-01  3:10', 6
;
;prstr, out, file='/sxisw/dbase/tb_tv/tbtv_calc_heater_v1.txt'
;
end