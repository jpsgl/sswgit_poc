pro tbtv_rd_rocket, sttim, entim, out, short=short, to_ut=to_ut
;+
;NAME:
;	tbtv_rd_rocket
;PURPOSE:
;	Read the envmon rocket data and put into structure
;HISTORY:
;	Written 21-Apr-00 by M.Morrison
;	24-Apr-00 (MDM) - Added "code" and "descr" fields
;	24-Apr-00 (MDM) - Added /short keyword
;			- Added /to_ut keyword
;-

setenv,'BAKEOUT_LOGDIR=/sxi01/bakeouts/rocket

rd_bakeout_logger, sttim, entim, dattim, mat, label
if (keyword_set(to_ut)) then dattim = ut_time(dattim, /int)
;
nmnem = n_elements(label)
mat = rotate(mat, 4)
out = {daytime: dattim, value: mat, mnem: label, code: indgen(nmnem), descr: strarr(nmnem)}
if (keyword_set(short)) then return
out = {envmon: out}
;
end