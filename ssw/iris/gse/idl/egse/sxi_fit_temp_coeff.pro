pro plot_fit_coeff, x, y, coeff, tit0, xmax
;
;
print, '---------', tit0
print, coeff
print, arr2str(strtrim(coeff,2))
;
xx = findgen(1000)/999.*xmax
yy1 = poly(xx, coeff)
yy2 = poly(x, coeff)
;
!p.multi = [0,1,2]
tit = tit0 + '= ' + arr2str(strtrim(coeff,2))
plot, xx, yy1, tit=tit
oplot, x, y, psym=2
;
;plot, x, yy2
plot, xx, yy1, xr=[min(x), max(x)]
oplot, x, y, psym=2

for i=0,n_elements(x)-1 do xyouts, x(i), y(i)+5, string(x(i), y(i), y(i)-yy2(i), format='(i4,",",f5.1,"/",f5.1)'), orient=90

plottime, 0, 0, 'SXI_FIT_TEMP_COEFF  Ver 2.0'
plottime
pause

;
end
;----------------------------------------------------------------------
;
; 6G02 thermistor fit
;
xh = [ 27, 28, 29, 31, 34, 35]
xl = [ 53,115,249,244, 21,121]
y =  [-20,  0, 20, 40, 60, 80]
;
x = xh*256l + xl
x = xh*256. + xl
coeff = poly_fit(x, y, 3, yfit)
plot_fit_coeff, x, y, coeff, '6G07 (PCM) for FM1 (9-Mar-01)', 10000
;
; 6G11 (CCD) thermistor fit
;
xh = [  32,  31, 31, 30, 29, 29, 28, 27,27]
xl = [ 200, 242, 42,109,186, 17,112,215,70]
y =  [-120,-100,-80,-60,-40,-20,  0, 20,40]
;
x = xh*256l + xl
x = xh*256. + xl
coeff = poly_fit(x, y, 2, yfit)
plot_fit_coeff, x, y, coeff, 'PCM_CCDTMP(6G11) for FM1 (9-Mar-01)', 10000


stop
;
;------------------------- Below is EM ---------------------------------
;
; YSI thermistor fit
;
;   fake data		Real data
;x = [200,100,130,    160, 64, 27, 12,  6,  3]
;y = [-21,-11,-17,    -20,  0, 20, 40, 60, 80]
;     Fake                 New real data
x = [ 200,     150, 126, 100, 80, 64, 52, 42, 33, 27, 18, 12,  6,  3]
y = [ -21,     -20, -15, -10, -5,  0,  5, 10, 15, 20, 30, 40, 60, 80]
w = [   1,       1,   1,   1,  5,  1,  1, 50, 50, 50, 20, 20, 20, 10]
;                      New real data
x = [  150, 126, 100, 80, 64, 52, 42, 33, 27, 18, 12,  6,  3]
y = [  -20, -15, -10, -5,  0,  5, 10, 15, 20, 30, 40, 60, 80]
w = [   10,  10,  10,  5,  5,  5, 50, 50, 50, 50, 50, 20, 10]
;w = intarr(n_elements(w))+2
;
x = [175+findgen(50)*2, x]
y = [-22-findgen(50)/49*10, y]
w = [intarr(50)+2, w]

coeff = polyfitw(x, y, w, 5, yfit)
coeff_save1 = coeff
plot_fit_coeff, x, y, coeff, 'YSI(SXI_CEL)', 256

x = 255-x	;invert sense
coeff = polyfitw(x, y, w, 5, yfit)
coeff_save2 = coeff
plot_fit_coeff, x, y, coeff, 'YSI(SXI_CEL_INVERTED)', 256

;
; 6G02 thermistor fit
;
xh = [ 10, 20, 28, 33, 36, 37]
xl = [ 84, 52,190,225,125,190]
y =  [-20,  0, 20, 40, 60, 80]
;
x = xh*256l + xl
coeff = poly_fit(x, y, 3, yfit)
plot_fit_coeff, x, y, coeff, '6G02', 10000
;
; 6G07A thermistor fit
;
xh = [  8,  9, 10, 11, 13, 15]
xl = [135, 68, 72,209,209, 99]
y =  [-20,  0, 20, 40, 60, 80]
;
x = xh*256l + xl
coeff = poly_fit(x, y, 3, yfit)
plot_fit_coeff, x, y, coeff, '6G07A', 10000


; 6G07B thermistor fit
;
xh = [  7,  8,  9, 10, 12, 14]
xl = [125, 60, 61,209,209, 98]
y =  [-20,  0, 20, 40, 60, 80]
;
x = xh*256l + xl
coeff = poly_fit(x, y, 3, yfit)
plot_fit_coeff, x, y, coeff, '6G07B', 10000


; 6G07C thermistor fit
;
xh = [  8,  9, 10, 11, 13, 15]
xl = [144, 76, 77,212,208, 96]
y =  [-20,  0, 20, 40, 60, 80]
;
x = xh*256l + xl
coeff = poly_fit(x, y, 3, yfit)
plot_fit_coeff, x, y, coeff, '6G07C', 10000

;
; SXI_PCM_CCDTMP fit
;
r = [1987.,2105.,2223.,2340.,2457.,2573.,2689.,2782.,2805.,2920.,3035.,3150.,3264.]
y = [-80,  -70,  -60,  -50,  -40,   -30,  -20,  -12,  -10,   0,   10,   20,   30]
x = (1000./(r+2000))*10.0*1000	 ;10.0 volt supply 1mv/dn (factor of 1000)  
;
coeff = poly_fit(x, y, 2, yfit)
plot_fit_coeff, x, y, coeff, 'PCM_CCDTMP(6G11)', 10000

;
; SXI_CCDZONETMP fit
;
r = [59.61,67.81,75.94,84.02,92.03,100.,107.89,115.73,123.53]
y = [-100,  -80,  -60,  -40,  -20,   0,    20,   40,   60]
r= 20*r
x = 10*r/(78.43 + 7.843e-03*r)
print,x
coeff = poly_fit(x, y, 2, yfit)
plot_fit_coeff, x, y, coeff, 'CCDZONETMP', 1000

;
; SXI_PCM_CEL fit (6G07-004)
;
r = [4200, 3900, 3700, 3400, 3100, 2900, 2700, 2400, 2100, 1750, 1500]
y = [ -50,  -40,  -30,  -20,  -10,    0,   10,   20,   30,   40,   50]
x = (1000./(r+2000.))*10.0*1000	 ;10.0 volt supply 1mv/dn (factor of 1000)  
;
coeff = poly_fit(x, y, 2, yfit)
plot_fit_coeff, x, y, coeff, 'PCM_CEL (6G07)', 10000

pprint
;
end










