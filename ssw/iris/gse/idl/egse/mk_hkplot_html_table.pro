
pro mk_hkplot_html_table, use_internal_mnem_table=use_internal_mnem_table

if not keyword_set(use_internal_mnem_table) then begin
  infil_mnem_list = '/net/solserv/home/slater/public_html/sot/hk/sot_hk_plot_params.lis'
  mnem_list = rd_ulin_col(infil_mnem_list, /nohead, nocomment='#')
endif

if not exist(mnem_list) then begin
  mnem_list = $
  [['S_C_SSIMPWRV0',			'Null',		'Null',		 '27.5',	 '28.5'], $
   ['S_C_SSIMPWRIO',			'Null',		'Null',		  '1.4',	  '6.5'], $
   ['E_HMI_PWR_P3_3V',			'3V_Primary',	'Volts',	  '2.95',	  '3.4'], $
   ['E_HMI_PWR_P5V',			'5V_Primary',	'Volts',	  '4.95',	  '5.2'], $
   ['E_HMI_PWR_P15V',			'15V_Primary',	'Volts',	 '14.5',	 '15.2'], $
   ['E_HMI_PWR_M15V',			'15V_Main',	'Volts',	'-15.5',	'-14.5'], $
   ['E_HMI_PWR_MOTOR_CURRENT',		'Motor_Current','Amps',		  '0',		'500'  ], $
   ['HMI_KER_CM_ACCEPT',		'Null',		'Null',		  '1.0',	'999'  ], $
   ['HMI_KER_CM_REJECT',		'Null',		'Null',		  '0',		  '0  '], $
   ['E_HMI_ISS_DIODE1',			'Diode_1',	'Null',		  '9.0',	 '11.0'], $
   ['E_HMI_ISS_DIODE3',			'Diode_3',	'Null',		  '9.0',	 '11.0'], $
   ['E_HMI_ISS_YERROR',			'Y_Err',	'Null',		 '-2.0',	  '2.0'], $
   ['E_HMI_ISS_ZERROR',			'Z_Err',	'Null',		 '-2.0',	  '2.0'], $
   ['E_HMI_ISS_PZTA',			'Null',		'Null',		 '30.0',	 '40.0'], $
   ['E_HMI_ISS_PZTB',			'Null',		'Null',		 '30.0',	 '40.0'], $
   ['E_HMI_ISS_PZTC',			'Null',		'Null'	,	 '30.0',	 '40.0'], $
   ['E_HMI_TS29_CAM1_SIDE_CEB_EXT',	'Null',		'Deg_C',	 '15.0',	 '35.0'], $
   ['E_HMI_TS28_CAM2_FRNT_CEB_EXT',	'Null',		'Deg_C',	 '15.0',	 '35.0'], $
   ['E_HMI_TS23_OPT_AFTBENCH',		'Null',		'Deg_C',	 '15.0',	 '35.0'], $
   ['E_HMI_TS39_OPT_FWDCVR',		'Null',		'Deg_C',	 '15.0',	 '35.0'], $
   ['HMI_TS205_CEB1_PWB_TEMP',		'CEB1_Temp',	'Deg_C',	 '15.0',	 '35.0'], $
   ['HMI_TS206_CEB2_PWB_TEMP',		'CEB2_Temp',	'Deg_C',	 '15.0',	 '35.0'], $
   ['HMI_TS201_CEB1_CCD_TEMP1',		'CCD1_Temp',	'Deg_C',	 '15.0',	 '35.0'], $
   ['HMI_TS204_CEB2_CCD_TEMP2',		'CCD2_Temp',	'Deg_C',	 '15.0',	 '35.0'], $
   ['E_HMI_TS31_HEB_PWR_CNVR_PRI',	'Null',		'Deg_C',	 '15.0',	 '35.0'], $
   ['E_HMI_TS33_HEB_RAD6000_CPU_A',	'Null',		'Deg_C',	 '15.0',	 '35.0'], $
   ['E_HMI_OV1_LOOP',			'Null',		'Deg_C',	 '15.0',	 '35.0'], $
   ['E_HMI_TS15_OVN_PREAMP',		'Null',		'Deg_C',	 '15.0',	 '35.0']]
endif

if not exist(mnem_names) then begin
  mnem_names = strupcase(reform(mnem_list(0,*)))
  mnem_labels = reform(mnem_list(1,*))
  ss_replace = where(mnem_labels eq 'Null',nreplace)
  if nreplace gt 0 then mnem_labels(ss_replace) = mnem_names(ss_replace)
endif
n_plots = n_elements(mnem_names)

; Construct template html table for all plot pages.  Start with header:
html_template =									$
  ["",										$
  "<html>",									$
  "",										$
  "<head>",									$
  "  <title>SOT HouseKeeping Plots</title>",					$
  "  <link rel='stylesheet' href='../housekeeping.css' type='text/css'></link>",	$
  "</head>",									$
  "",										$
  "<body>",									$
  "<div id='param_table_div'>",							$
  "",										$
  "<table class='param_table'>",						$
  "  <tr><th>Param Name</th>",							$
  "    <th colspan='4'>Plot Period (Days)</th>",				$
  "  </tr>"]

; Construct table entries for each plot by looping:
plot_path = '/net/solserv/home/slater/public_html/sot/hk'
path_list = file_list(plot_path,'Last*')
break_file, path_list, disk, parent, dir_nam
num_days = strmid(dir_nam,5,2)
ndirs = n_elements(path_list)
for i=0,n_plots-1 do begin
  html_template = [html_template, "  <tr><td>" +  mnem_names(i) + "</td>"]
  for j=0,ndirs-1 do begin
    ref_html_nam0 = mnem_names(i) + '_' + dir_nam(j) + '.html'
    html_template = [html_template, $
      "    <td><a href='../" + dir_nam(j) + "/" + ref_html_nam0 + "'>" + num_days(j) + "</a></td>"]
  endfor
  html_template = [html_template, "  <tr>"]
endfor

; Add blank gif plot reference and complete the template table:

html_template = [html_template,		$
  "</table>",				$
  "",					$
  "</div>",				$
  "<div id='plot_div'>",		$
  "<img src='plot_name.gif' />",	$
  "</div>",				$
  "</body>",				$
  "",					$
  "</html>"]

; Identify line in html_template which references the gif file:
ref_pos_arr = strpos(html_template, 'plot_name.gif')
ss_match = where(ref_pos_arr ne -1, count_match)
ss_gif_line = ss_match(0)

; Now double loop through every plot variable and each plot durations,
; modifying

for i=0,n_plots-1 do begin
  for j=0,ndirs-1 do begin
    html_strtab = html_template
    ref_html_nam0 = mnem_names(i) + "_" + dir_nam(j) + '.html'
    ref_pos_arr = strpos(html_strtab, ref_html_nam0)
    ss_match = where(ref_pos_arr ne -1, count_match)
    html_strtab(ss_match(0)) = "    <td class='selected_cell'>" + num_days(j) + "</td>"
    ref_gif_nam0 = mnem_names(i) + '_lil.gif'
    html_strtab(ss_gif_line) = "  <img src='../" + dir_nam(j) + "/" + ref_gif_nam0 + "' />"
    outfil_html = concat_dir(path_list(j),ref_html_nam0)
    prstr, html_strtab, file=outfil_html
  endfor
endfor

end


