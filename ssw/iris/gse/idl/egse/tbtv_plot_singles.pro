pro tbtv_plot_singles, info, boutdir, nolocal=nolocal
;
;
;
;
if (n_elements(boutdir) eq 0) then boutdir = getenv('SXI_TBTV_HTML_DIR')
outdir = concat_dir(boutdir, 'GIF4HR')
if (not file_exist(outdir)) then spawn, ['mkdir', outdir], /noshell
;
tv2, 400, 300, /init
;
tags = tag_names(info)
ii = 0
for i=0,n_elements(tags)-1 do begin
    for j=0,n_elements(info.(i).mnem)-1 do begin
	mnem = strlowcase(info.(i).mnem(j))
	sag_hkplot, info, codelist=ii, /lab, nolocal=nolocal
	outfil = concat_dir(outdir, mnem+'.gif')
	print, outfil
	if (!d.name eq 'Z') then zbuff2file, outfil
	ii = ii + 1
    end
end
;
end