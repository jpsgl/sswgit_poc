pro iri_pkt_time_zero, fid, apid
; fid = packet file name (e.g. '20130315')
; apid = 0xxxx (e.g. '0x1113')

filnam=getenv('GSE_PKT_FILE_DIR')+'/'+apid+'/'+fid+'.'+apid


dd=sag_rd_hk(filnam,0,100000)

gg=where(dd.pkt_head.time eq 0, ngg)
print, 'Found '+strcompress(ngg,/rem)+' time stamps of 0'

gg=where(dd.pkt_head.time lt 0, ngg)
print, 'Found '+strcompress(ngg,/rem)+' time stamps of <=66000'

end
