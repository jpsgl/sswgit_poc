if (n_elements(tlm_old) eq 0) then begin
    sag_sxi_rd_hk_db, tlm_old, infil='/sxi01/egse_em/db/sxiem1_v00.183'
    sag_sxi_rd_hk_db, tlm_new, infil='/sxi01/egse_em/db/sxiem1_v01.100'
end
;
ss = where(tlm_old.source eq 'MDL')	& tlm_old = tlm_old(ss)
ss = where(tlm_new.source eq 'MDL')	& tlm_new = tlm_new(ss)
;
all_nam = [tlm_old.name, tlm_new.name]
unam = all_nam( uniq( all_nam, sort(all_nam) ) )
;
ss_old = where_arr(unam, tlm_old.name)
ss_new = where_arr(unam, tlm_new.name)
;
n = n_elements(unam)
qarr = intarr(n)
ss = where(ss_old ne -1, nss)	& qarr(ss_old(ss)) = qarr(ss_old(ss)) + 1	;in old
ss = where(ss_new ne -1, nss)	& qarr(ss_new(ss)) = qarr(ss_new(ss)) + 2	;in new
;
extra_old = 'SXI_SPARE' + string( indgen(50), format='(i3.3)')
nex = n_elements(extra_old)
;
ss1 = where(qarr eq 3, nss) 	& print, nss, '  mnemonic in both new and old'
ss2 = where(qarr eq 2, nss) 	& print, nss, '  mnemonic in new only'
ss3 = where(qarr eq 1, nss) 	& print, nss+nex, '  mnemonic in old only (were deleted or renamed)'

unam = strupcase(unam)
print, '--------- New Mnemonics -------------
prstr, '   ' + unam(ss2), /nomore
print, '--------- Deleted Mnemonics -------------
prstr, '   ' + unam(ss3), /nomore
prstr, '   ' + extra_old, /nomore
end