pro iri_disp_img_head, tlmfile, nomore=nomore, outfil=outfil, hc=hc, every=every, isponly=isponly

nam='iri_disp_img_head'

dat=iri_rd_tlm(tlmfile)
ncadu=n_elements(dat)
found=0
if (not keyword_set(every)) then every=1
if (keyword_set(isponly)) then begin
	every=1
	sss=where(dat.isp.pkt_head.apid eq '38'x, nsss)
	if (nsss gt 0) then begin
		qqq=sss+1
		qqq=[sss,qqq]
		rrr=where(qqq lt n_elements(dat), nrrr)
		dat=dat(qqq(rrr(sort(qqq(rrr)))))
	endif else print, string( 'No ISPs found in ', ncadu, ' CADUs', format="(A,I-3,A)")
endif
str=string('# Read ', ncadu, ' VCDUs', format="(A,I-6,A)")
if (keyword_set(isponly)) then str=string( '# Found ', nsss, ' ISPs in ', ncadu, ' CADUs', format="(A,I-3,A,I-6,A)")
hout=['# '+tlmfile, '#', str, '#']
hout=[hout, string('CamHd1','CamHd2','CamHd3','CamHd4','CropID','RdMode','HdrErr','Overflow','TAPCode','BitSelID','CompID','LookupID','OffHi8','OffLo16','InitV', format="(15A-10)")]
for i=0,n_elements(dat)-1,every do hout=[hout, string(dat(i).isp.pkt_head.code*256+dat(i).isp.pkt_head.apid, dat(i).isp.pkt_head.scnt, dat(i).isp.pkt_head.dlen, dat(i).isp.pkt_head.time/65536,$
((dat(i).isp.pkt_head.time and 'fff0'x)/16), ((dat(i).isp.pkt_head.time and '000c'x)/4), ((dat(i).isp.pkt_head.time and '0002'x)/2), ((dat(i).isp.pkt_head.time and '0001'x)),$
((dat(i).isp.pkt_head.ftime and 'f0000000'x)/'10000000'x), ((dat(i).isp.pkt_head.time and '0f000000'x)/'1000000'x), ((dat(i).isp.pkt_head.ftime and 'ff0000'x)/'10000'x),$
((dat(i).isp.pkt_head.ftime and 'ff00'x)/'100'x), ((dat(i).isp.pkt_head.ftime and 'ff'x)),dat(i).isp.data(0)*256+dat(i).isp.data(1),dat(i).isp.data(2)*256+dat(i).isp.data(3), format="(15('0x'(Z-8.4)))")]

prstr, hout, nomore=nomore, file=outfil, hc=hc

end
