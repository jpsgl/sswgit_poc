pro sag_hkplot_v20, hk, yrange=yrange00, nover=nover, timerange=timerange, $
		codelist=codelist, tit=tit, qdebug=qdebug, qlinestyle=qlinestyle, $
		notyminmax=notyminmax, median_pm=median_pm, sigma=sigma, $
		label=label0,every=every,$
		pm_filter=pm_filter, filter1d=filter1d, last_tim_lab=last_tim_lab, $
		psym=psym, noprint=noprint, colors=colors, $
		smoo=smoo, user_labels=user_labels, nolocal=nolocal, plotfile=plotfile
;
;HISTORY:
;	Written 7-Oct-99 by M.Morrison taking TRACE NEW_HKPLOT.PRO as a start
;V1.01   5-Nov-99 (MDM) - Fixed truncation of mnemonics in plot labeling
;V1.02	20-Nov-99 (MDM) - Added /NOPRINT
;V1.03	22-Nov-99 (MDM) - Added /COLORS option
;V1.04	12-Apr-00 (MDM) - Added SMOO (smooth) option
;V1.05	12-Apr-00 (MDM) - Added user_labels option
;V1.06	13-Apr-00 (MDM) - Corrected problem with plotting one point
;V1.07	13-Apr-00 (MDM) - Allow conversion to local time (SAG_HKPLOT_TO_LOCAL)
;V1.08	16-Apr-00 (MDM) - Adjusted for error introduced in V1.07
;V1.09	24-Apr-00 (MDM) - Added keyword /nolocal
;V1.10	10-May-00 (MDM) - Fixed timerange bug for structures with more than one segment
;-

progver = 'SAG_HKPLOT  Ver 1.10

qlocaltim = (keyword_set(getenv('SAG_HKPLOT_TO_LOCAL'))) and (not keyword_set(nolocal))

linecolors
!p.background = 255
!p.color = 0

if (!d.name eq 'PS') then device,/land
if (keyword_set(timerange)) then timerange0 = timerange

tags = tag_names(hk)
ntags = n_elements(tags)
;
ntot_mnem = 0
for i=0,ntags-1 do ntot_mnem = ntot_mnem + n_elements(hk.(i).mnem)
if (n_elements(codelist) ne 0) then ntot_mnem = n_elements(codelist)
;
if (n_elements(yrange00) eq 2) then yrange = yrange00
if (not keyword_set(yrange)) then begin
	print, 'Auto Y ranging...'
	qfirst = 1
	for i=0,ntags-1 do begin
	    for j=0,n_elements(hk.(i).mnem)-1 do begin
		qplot = 1
		if (n_elements(codelist) ne 0) then dummy = where([codelist] eq hk.(i).code(j), qplot)
		if (qplot) then begin
		    y = hk.(i).value(*,j)
		    ;;if (keyword_set(smoo)) then if (n_elements(y) gt smoo) then y = smooth(y, smoo)
		    labtit = string(hk.(i).mnem(j), hk.(i).descr(j), format='(a, " - ", a)')
		    yrange0 = [min(y), max(y)]
		    case 1 of
			keyword_set(notyminmax): begin
				    ss = where((y ne min(y)) and (y ne max(y)))
				    if (ss(0) ne -1) then yrange0 = [min(y(ss)), max(y(ss))]
				end
			keyword_set(pm_filter): begin
				    ss = where((y gt -1*pm_filter) and (y le pm_filter))
				    if (ss(0) ne -1) then yrange0 = [min(y(ss)), max(y(ss))]
				end
			keyword_set(filter1d): begin
				    junk = filter_1d(y, ix_good=ss)
				    if (ss(0) ne -1) then yrange0 = [min(y(ss)), max(y(ss))]
				end
			keyword_set(median_pm): begin
				    med = median(y)
				    yrange0 = [med-median_pm, med+median_pm]
				end
			keyword_set(sigma): begin
				    idev = stdev(y, iavg)
				    yrange0 = [-1,1]*idev*sigma + iavg
				end
			else: 
		    endcase
		    if (qfirst) then begin
			yrange = yrange0
			qfirst = 0
		    end else begin
			yrange(0) = min([yrange(0), yrange0(0)])
			yrange(1) = max([yrange(1), yrange0(1)])
		    end
		    if (keyword_set(qdebug)) then print, min(y), max(y), yrange
		end
	    end
	end
	if (yrange(0) eq 0) and (yrange(1) eq 0) then yrange(1) = 1
end

if (n_elements(yrange) eq 0) then begin
    print, 'NEW_HKPLOT: Cannot find the information you requested 
    print, '            in the structure passed in.   Returning....'
    return
end
;
if (n_elements(codelist) ne 0) then begin 
  for i=0,ntags-1 do begin	;MDM added so that multiple plots work correctly when forcing yrange
    for j=0,n_elements(hk.(i).mnem)-1 do begin
	dummy = where([codelist] eq hk.(i).code(j), qplot)
	;if (qplot) then labtit = string(hk.(i).mnem(j)+'    ', hk.(i).descr(j), format='(a12,a)')
	if (qplot) then labtit = string(hk.(i).mnem(j), hk.(i).descr(j), format='(a," - ",a)')
    end
  end
end
;
if (yrange(0) eq yrange(1)) and (yrange(0) ne 0) then yrange = yrange(0) + [-0.05,0.05]*yrange(0)

label = keyword_set(label0)
if (keyword_set(label)) and (ntot_mnem eq 1) and (not keyword_set(tit)) then begin
    if (keyword_set(labtit)) then tit = labtit $
			else tit = string(hk.(0).mnem(0), hk.(0).descr(0), format='(a, " - ", a)')
    label = 0
end

if (Keyword_set(colors)) then linecolors
ilin = 0
qfirst = 1
sav_color = !p.color
for i=0,ntags-1 do begin
    daytime = hk.(i).daytime
    if (qlocaltim) then begin
	ut_dt = int2secarr( daytime(0), ut_time( daytime(0) ) )
	daytime = anytim2ints(daytime, off=ut_dt)
	if (keyword_set(timerange0)) and (i eq 0) then timerange0 = anytim2ints(timerange0, off=ut_dt)
    end
    xtit = 'Start Time: ' + fmt_tim(daytime(0)) 
    if (keyword_set(last_tim_lab)) then begin
	fmt_timer, daytime, t1, t2, /noprint
	xtit = 'Last Data: ' + t2
    end
    if (qlocaltim) then xtit = xtit + ' local'
    for j=0,n_elements(hk.(i).mnem)-1 do begin
	qplot = 1
	if (n_elements(codelist) ne 0) then dummy = where([codelist] eq hk.(i).code(j), qplot)
	if (qplot) then begin
	    if (qfirst) then begin
		utplot, daytime, hk.(i).value(*,0), yrange=yrange, $
				/nodata, timerange=timerange0, /ynozero, tit=tit, /xstyle, $
				xtit=xtit, psym=psym
		qfirst = 0
	    end
	    ;
	    if (keyword_set(qlinestyle)) then linestyle = ilin mod 5
	    if (Keyword_set(colors)) then color = (ilin mod 13)+1 $
				else color = !p.color
	    !p.color = color
	    y = hk.(i).value(*,j)
	    if (keyword_set(smoo)) then if (n_elements(y) gt smoo) then y = smooth(y, smoo)
	    if (n_elements(y) ge 2) then begin
	      outplot, daytime, y, linestyle=linestyle, psym=psym, color=color
	      if (keyword_set(label)) then begin		;MDM added 21-Nov-95
		;;labtit = string(hk.(i).mnem(j), hk.(i).descr(j), format='(a, " - ", a)')
		labtmp = hk.(i).descr(j)
		if (keyword_set(user_labels)) then labtmp = user_labels(ilin < (n_elements(user_labels)-1) )
;		labtit = string(hk.(i).mnem(j), labtmp, format='(a, " - ", a)')
		labtit = labtmp
		linelabel_v20, .02, 1-.04*(ilin+1), labtit, linestyle=linestyle, size=(3./((!p.multi(1)*!p.multi(2))>1))<1
	      end
	      ilin = ilin + 1
	    end
	end
    end
end
!p.color = sav_color

if n_elements(every) ne 0 then begin
    if every eq 1 then infostr='VALUE' else infostr=every+' VALUES'
    plottime,.25,0,'PLOTTING EVERY '+infostr
endif

if (not keyword_set(nover)) then begin
    plottime, 0, 0, progver
    plottime
    if (not keyword_set(noprint)) then pprint
;    if (not keyword_set(noprint)) then begin
;      if not exist(plotfile) then plotfile = 'idl.ps'
;      if (!d.name eq 'PS') then device,/close
;      set_plot,'x'
;      spawn,'lpr ' + plotfile
;    endif
end
;
end
