function iri_rd_tlm, tlmfile, files=files

nam='iri_rd_tlm'

if (n_elements(tlmfile) eq 0) then tlmfile=Dialog_Pickfile(path='/imageproc/tlmqac/', filter='*.tlm', title='Choose tlm file(s)', /multiple, /must_exist)

dat=sag_rd_hk(tlmfile(0),0,100000)
if (n_elements(tlmfile) gt 1 ) then for i=1, n_elements(tlmfile)-1 do dat=[dat,sag_rd_hk(tlmfile(i),0,100000)]

files=tlmfile
return, dat
end
