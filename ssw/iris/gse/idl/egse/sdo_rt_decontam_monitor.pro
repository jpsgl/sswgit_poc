pro sdo_rt_decontam_monitor, sttim, entim, smoot=smoot, lookback=lookback, yrange=yrange
;+
;	Name: aia_rt_curvol_monitor
;
;	$Id: sdo_rt_decontam_monitor.pro 4942 2012-01-13 03:52:02Z sabolish $
;
;	$Name:  $
;
;	Purpose: read input packets, extract data and check limits maybe.
;
;	Parameters:
;		smoot
;		lookback
;
;		limits	TBD
;
;	History:
;		9-Jan-08, G.Linford created
;		17-Jan-08, just a quick hack for decontam current.
;		10-Feb-2009, Add CVS tags, update for interpolation problem with packet rate change for
;		x11b,x12f
;	$Log: sdo_rt_decontam_monitor.pro,v $
;	Revision 1.5  2009/04/01 20:38:40  linford
;	Add a fix for case when the two packets have the same rate
;	
;	Revision 1.4  2009/02/11 18:27:35  linford
;	bug fix,typo, on interpolation
;	
;	Revision 1.3  2009/02/11 02:21:23  linford
;	bug fix on interpolation fix
;	
;	Revision 1.2  2009/02/11 01:34:31  linford
;	fix bug seen when packet rates change for x11b,x12f
;	 
;
;  Notes:
;IDL> print,n_elements(dat.(0).daytime)
;        2926
;IDL> print,n_elements(dat.(1).daytime)
;        1464
;IDL> iresult1 = interpol(istruct.scc.value[*,1],2926,/spline)
;IDL> tcur = iresult + iresult1 + dat.sca.value[*,0] + dat.sca.value[*,1]
;IDL> utplot,dat.sca.daytime,tcur
;
;-

if keyword_set(lookback) then begin
        dt = lookback
endif else begin
        dt = 5                          ; default time window is 5 min
endelse

; future idea of appling limits
if keyword_set(infile) then begin
        tlm = rd_ulin_col(infile, /nohead, nocomment='#')
endif else begin
        ;infile = '/home/sdotc/idl/aia_emi_tlm_pts.dat'
        ;tlm = rd_ulin_col(infile, /nohead, nocomment='#')
endelse

tlm_pts =['E_S_C_PA_CDHA_LPSC_DC','E_S_C_PB_CDHB_LPSC_DC']
;tlm_vol =['E_S_C_PA_VBUS','E_S_C_PB_VBUS']
;tlm_pts = tlm[0,*]              ; extract tlm points
;tlm_ll =  float(tlm[1,*])       ; extract low limits
;tlm_ul = float(tlm[2,*])        ; extract upper limits

pts = reform(tlm_pts)           ; reform into linear array
;ll = reform(tlm_ll)
;ul = reform(tlm_ul)

;-- use lookback time to pick duration of time slice
;sttim = anytim2ints(ut_time(), off=-(dt*60))
;entim = anytim2ints(ut_time())

;- following RT line remove and fix time as added for code dev
dat = sag_get_mnem(anytim(sttim,/yohkoh),anytim(entim,/yohkoh), pts)
;dat = sag_get_mnem('14-jan-2008 23:00','14-jan-2008 23:59', pts)   ; Fixed data: for testing
;sttim = anytim2ints('14-jan-2008 23:00')
;entim = anytim2ints('14-jan-2008 23:59')

; debug time span
print, 'Start time of data check: '+anytim(sttim,/yohkoh)
print, 'End time of data check: '+ anytim(entim,/yohkoh)

n = n_elements(pts)

; --- check dat for all structure types!!!!
;All Structures: HKR:2, HMC:1, HPW:16, HSQ:7, SPW:2
; 9 plots into 10 slots
ndat_tags = n_tags(dat)         ; get number of tags
npoints = lonarr(ndat_tags)
dat_tags = tag_names(dat)       ; get list of tags
for i=0, ndat_tags-1 do begin
        print,'Processing Data Tag:' + dat_tags[i]
		
        ;iStruct = struct_subset(dat, dat_tags[i])
			
		npoints[i] = n_elements(dat.(i).daytime)	
		print, 'Number of points: ', npoints[i]
			
        ;exestr = 'help,iStruct.'+dat_tags[i]+',/str'
	exestr = 'help, dat.(i),/str
		
		;validx = dat.dat_tags[i].mnem,
		
        ;prtmnem = 'print,"Mnemonics for this Tag: ", iStruct.'+dat_tags[i]+'.mnem'
	prtmnem = 'print, "Mnemonics for this Tag: ", dat.(i).mnem'
        res = execute(prtmnem)                  ; print all mnemonics for this tag
        res = execute(exestr)                   ; run help on extracted structure
		;print,'call to plotter with iStruct'
		if ( i eq 0 ) then begin
			;iStruct0 = iStruct
		endif else begin
			;iStruct1 = iStruct
		endelse
        ;aia_proc_curvol_str, iStruct, color=color, qlinestyle=qlinestyle, filter1d=filter1d  ; plot the extacted tag data
endfor
imax = where(npoints eq max(npoints))		; == 0  find which data set has max numbers
imin = where(npoints eq min(npoints))		; == 1
if (n_elements(imax) gt 1) then imax = 0	; if the number points are equal pick one and use else block
sc1cur = dat.(0).value[*,0] ;
sc3cur = dat.(1).value[*,0] ;

;- interpolate to the fewest points of max
if ( imax[0] eq 1 ) then begin 	; most points in sc3, sc3 times
	irescur = interpol( sc1cur, anytim(dat.(imin[0]).daytime), anytim(dat.(imax[0]).daytime))
	tcur = irescur + sc3cur;
	utplot, dat.(imax).daytime, tcur, yrange=yrange
endif else begin
	irescur = interpol( sc3cur, anytim(dat.(imin[0]).daytime), anytim(dat.(imax[0]).daytime))
	tcur = irescur + sc1cur;
	utplot, dat.(imax).daytime, tcur, yrange=yrange	
endelse


;iresult0 = interpol(dat.(imin[0]).value[*,0],npoints[imax[0]],/spline)
;;iresult1 = interpol(dat.(imin[0]).value[*,1],npoints[imax[0]],/spline)

;tcur = iresult0 + dat.(imax).value[*,0] 
;utplot,dat.(imax).daytime,tcur, yrange=yrange

;-- AKR infor
;n_items = dat.akr.mnem
;stop   ;- debug
end

