pro sxi_limits_template, sort_option=sort_option, $
				select=select
;
;
;
sag_sxi_rd_hk_db, tlm, alg, dsc, cmd, /qdebug
;
if (n_elements(sort_option) eq 0) then sort_option = 1
case sort_option of
    1: begin
	ss1 = where(strmid(tlm.unit, 0, 3) eq 'deg', nss)
	ss2 = where(is_member(tlm.unit, ['V', 'Volts']))
	ss3 = where(is_member(tlm.unit, ['A', 'mA', 'Amps']))
	ss = [ss1, ss2, ss3]
	end
endcase
;
fmt1 = '(a23,1x, 4(a7,1x), 1x, "# ", a40)'
fmtc = '(1x, z5.5,1x,i3,1x,a25)
tit = ['Telemetry Mnemonic', 'AlrmLo', 'WarnLo', 'WarnHi', 'AlrmHi', 'Comments']
dash = replicate('-----------------------------------------------------------------',6)

n = n_elements(ss)
out_tab = strarr(n+2)
out_tab(0) = string(tit, format=fmt1)
out_tab(1) = string(dash, format=fmt1)
for i=0L,n-1 do begin
    tlm0 = tlm(ss(i))
    ;
    comment = string(tlm0.apid(0), tlm0.stbyte(0), $
				tlm0.descr, format=fmtc)
    out0 = string(strupcase(tlm0.name)+'                     ', $
				'.', '.', '.', '.', $
     			        comment, format=fmt1)
    out_tab(i+2) = out0

end

prstr, out_tab, file='$SAG_TELEM_INFO/sxi_limits_template.tab'
end

