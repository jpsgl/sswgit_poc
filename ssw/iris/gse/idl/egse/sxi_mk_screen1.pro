pro go_do1, lab, mnem, fmt, len, ilin, icol

if (icol eq 0) then print, ' '
print, ilin, icol,   lab, format=' ("TEXT   ", i4, i4, " black white ", a)'
print, ilin, icol+1, fmt, len, mnem, format='("INTEGER", i4, i4, 1x, a, i3, 1x, a)'


end
;--------------------------------------------------------------------
;
;

mnem = 'SXI_HIST_OVER_' + string(indgen(64)*64+32, format='(i4.4)')
lab = '>' + string(indgen(64)*64+32, format='(i4.4)')
fmt = '%4d'
len = 4
nx = 4
ny = 64/nx

for i=0,n_elements(lab)-1 do begin
    go_do1, lab(i), mnem(i), fmt, len, i/nx+3, (i mod nx)*2
end
;
end