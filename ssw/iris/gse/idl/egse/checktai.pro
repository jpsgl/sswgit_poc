;+
; $Id: checktai.pro 4942 2012-01-13 03:52:02Z sabolish $
; $Name:  $
; Name: checktai
;
; Purpose: Display running TAI time in Dec and Hex, for comparsion with 
;		instrument time.  Each run is about 5min. Corrections for UTC-local: 25200(PDST),28800(PST)
;		use: http://www:leapsecond.com/java/gpsclock.htm
;		to verify this code and then verify HEX values to CCSDS timecode.
;
; History: written by G.Linford, 16-Oct-07
;
; $Log: checktai.pro,v $
; Revision 1.1  2007/11/19 16:58:47  linford
; checkin for HMI kernel SWAT
;
;
;-

for i=0,300 do begin
	print,format='("TAI epoch(sec) no leap secs in hex: ",z10)',ulong(utc2tai(ut_time(),/nocorrect))
	print,format='("TAI Time(sec) in hex: ",z10)',ulong(utc2tai(ut_time()))
	print,format='("Formated HR:MM:SS.00 for TAI: ", A30)',anytim(double(utc2tai(ut_time())),/yohkoh)
	print,format='("UTC Time in hex: ",z10)',ulong(anytim(ut_time(),/sec))
	;print,anytim(double(anytim2tai(!stime)+28800-33),/yohkoh)
	;print,anytim(tai2utc(double(anytim2tai(ut_time()))),/yohkoh)
	wait,1.0
	print, " "
endfor

end
