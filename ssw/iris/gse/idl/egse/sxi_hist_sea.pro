function sxi_hist_sea, date, strs, logic=logic, qdebug=qdebug
;+
;NAME:
;	sxi_hist_sea
;PURPOSE:
;	Search SXI EGSE history file for string patterns
;SAMPLE CALLING SEQUENCE:
;	out = sxi_hist_sea('28-oct', 'perform setpower')
;	out = sxi_hist_sea('28-oct', 'perform pictypetest')
;HISTORY:
;	Written 10-Nov-99 by M.Morrison
;-
if (n_elements(logic) eq 0) then logic = 'AND'
;
if (n_elements(date) eq 0) then date = gt_day( ut_time(), /str)
;
dir = getenv('GSE_HISTORY_DIR')
fid = time2file(date, /date_only)
infil = concat_dir(dir, fid+'.hst')
;
case strupcase(logic) of
    'AND': delim = '+'
    'OR':  delim = '|'
    else:  delim = '|'
endcase
;
sea_str = '(' + arr2str(strs, delim=delim) + ')'
cmd = ['egrep', '-i', sea_str, infil]
if (keyword_set(qdebug)) then prstr, cmd
spawn, cmd, result, /noshell
if (keyword_set(qdebug)) then prstr, result
;
out = 0b
out0 = {time: 0L, day: 0, str: ''}
if (result(0) ne '') then begin
    n = n_elements(result)
    out = replicate(out0, n)
    dattim = anytim2ints(gt_day(date, /str) + ' ' + strmid(result, 22, 8))
    out.day = dattim.day
    out.time = dattim.time
    out.str = result
end
;
return, out
end