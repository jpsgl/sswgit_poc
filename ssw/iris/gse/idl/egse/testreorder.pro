function testreorder, unimnem, mnem
; +
; Name testreorder.pro
;
; Purpose using a list of unique mnemonics and the actual list of mnemonics in the data
;	construct the unique list in the proper order for the diagnostic data
;
; history G.Linford written March 27 2006
;
; -

ord = intarr(n_elements(unimnem));	define order array
for i=0, n_elements(ord)-1 do begin
	stuff = where(mnem eq unimnem[i])
	ord[i] = stuff[0];		define the natural order
endfor

a = {smap, mnem:' ', order:0}
mnem_list = replicate({smap}, n_elements(unimnem))

; load into structure
for i=0, n_elements(ord)-1 do begin
	mnem_list[i].mnem = unimnem[i];
	mnem_list[i].order= ord[i];
endfor

new_mnem_list = mnem_list(sort(ord)).mnem;	sort into correct order

return, new_mnem_list

end
