function tbtv_tab_heat_model, out, i, infil, node, loc, scale1

out(i,0) = strtrim(node, 2)

print, 'Reading ' + infil
mat = rd_tfile(infil)
mat = str_replace(mat, '$', ',')
ss = where( (strpos(mat, strtrim(node,2)) ne -1) and (strupcase(strmid(mat, 0, 1)) ne 'C'), nss)
if (nss eq 0) then return, '000.00'
if (nss ne 1) then stop
arr = str2arr(mat(ss(0)), delim=',')
cmd = 'v = ' + arr(loc)
stat = execute(cmd)
v = v*scale1
print, v

return, string(v, format='(f10.2)')
end
;----------------------------------------------------------------
outfil = '/sxi01/html/TandC/DB/590/heaters_and_sinks.html'
indir = '/sxisw/html/Inst/Thermal/TB_FM1'
ff = file_list(indir, 'heaters_*', file=file)
;
nnode = 14
ncase = n_elements(file)
out = strarr(nnode+1, ncase+1)
;
for icase=0,ncase-1 do begin
    infil = ff(icase)
    infil2 = str_replace(infil, 'heaters', 'standoffplate')
    infil3 = str_replace(infil, 'heaters', 'connector_sinks')
    infil4 = str_replace(infil, 'heaters', 'FM1_CEB_power')
    out(0,icase+1) = str_replace(str_replace(file(icase), '.inc', ''), 'heaters_', '')
    out(1,icase+1) = tbtv_tab_heat_model(out, 1, infil, 3027, 3, 24)	;fore
    out(2,icase+1) = tbtv_tab_heat_model(out, 2, infil, 2018, 3, 6)	;aft
    out(3,icase+1) = tbtv_tab_heat_model(out, 3, infil, 1110, 1, 1)	;ccd
    out(4,icase+1) = tbtv_tab_heat_model(out, 4, infil, 1100, 1, 1)	;ccd
    out(5,icase+1) = tbtv_tab_heat_model(out, 5, infil4, 5001, 3, 4)	;CEB part 1
    out(6,icase+1) = tbtv_tab_heat_model(out, 6, infil4, 5004, 3, 4)	;CEB part 2
    out(7,icase+1) = tbtv_tab_heat_model(out, 7, infil4, 5201, 3, 4)	;DEB
    out(8,icase+1) = tbtv_tab_heat_model(out, 8, infil4, 5401, 3, 4)	;HEB
    out(9,icase+1) = tbtv_tab_heat_model(out, 9, infil4, 5601, 3, 4)	;PEB
    out(10,icase+1) = tbtv_tab_heat_model(out, 10, infil2, 3300, 1, 1) - 273	;standoff plate
    out(11,icase+1) = tbtv_tab_heat_model(out, 11, infil3, 6043, 1, 1) - 273	;ADS sink
    out(12,icase+1) = tbtv_tab_heat_model(out, 12, infil3, 6074, 1, 1) - 273	;XRS sink
    out(13,icase+1) = tbtv_tab_heat_model(out, 13, infil3, 6084, 1, 1) - 273	;EUV sink
    out(14,icase+1) = tbtv_tab_heat_model(out, 14, infil3, 6294, 1, 1) - 273	;PSS sink
end
;
out2 = ['<html><head><title>FM1 Thermal Balance Parameters</title><body>',$
        strtab2html(out), $
        ' ', $
        'Page Generated: ' + !stime, $
        '</body></html>']
;
prstr, out2, file=outfil

end