
pro tbtv_plots_v20, sttim, entim, outdir, mk24hr=mk24hr, mk7day=mk7day, $
  test=test

tc_map = rd_ulin_col('$SXI_TBTV_DB_DIR/mnem_aliases.tab', /nohead, nocomm='#')
tc_map(0,*) = strlowcase(tc_map(0,*))

ff = file_list('$SXI_TBTV_DB_DIR', 'summary_mnem*.tab')

if (n_elements(sttim) eq 0) then begin
  if keyword_set(mk24hr) then begin
    win=24.
    if (n_elements(outdir) eq 0) then $
      outdir = '/fpp01/html/TandC/DB/TNCFPP61/TEMP_PLOT_PNGS/GIF24HR'
  endif
  if keyword_set(mk7day) then begin
    win=24*7.
    if (n_elements(outdir) eq 0) then $
      outdir = '/fpp01/html/TandC/DB/TNCFPP61/TEMP_PLOT_PNGS/GIF7DAY'
  endif 
  if not exist(win) then begin
    win=4.
    if (n_elements(outdir) eq 0) then $
      outdir = '/fpp01/html/TandC/DB/TNCFPP61/TEMP_PLOT_PNGS/GIF4HR'
  endif
  if keyword_set(test) then entim = '6-feb-2004 14:00' else $
    entim = anytim2ints(ut_time())
  sttim = anytim2ints(entim, off=-60*60*win)
end
if (n_elements(outdir) eq 0) then outdir = concat_dir('/fpp01/html/TandC/DB/TNCFPP61/TEMP_PLOT_PNGS/GIF')
if (not file_exist(outdir)) then spawn, ['mkdir', outdir], /noshell

!p.multi = 0
tv2, 800, 600, /init
for i=0,n_elements(ff)-1 do begin
  break_file, ff(i), dsk_log, dir00, filnam, ext
  list = rd_tfile(ff(i), nocomment='#')
  mnem_mat = str2cols(list)
  mnem = strlowcase(strtrim(mnem_mat(0,*),2))

  ss = where_arr(mnem, tc_map(0,*), /map_ss)
  if (max(ss) eq -1) then delvarx, user_labels else begin
   user_labels = strarr( n_elements(mnem) ) + '????'
    ss2 = where(ss ne -1)
    user_labels(ss2) = tc_map(1,ss(ss2))
  end

  info = sag_get_mnem(sttim, entim, mnem)
  if (data_type(info) eq 8) then begin
    !p.thick=2
    sag_hkplot_v20, info, /label, /qlinestyle, /colors, smoo=8, pm_filter=150, $
      timerange=[sttim, entim], user_label = user_labels, tit=filnam+ext + ' (Smooth 8)'
    if (!d.name eq 'Z') then zbuff2file, concat_dir(outdir, filnam+'.png'),/png
    pause
  endif else begin
    print, 'HK read problems........................................
  endelse
end
;
tv2, 600, 800, /init
ff = file_list('$SXI_TBTV_DB_DIR', '*.elst')
for i=0,n_elements(ff)-1 do begin
    break_file, ff(i), dsk_log, dir00, filnam, ext
    plot_set_file = ff(i)
    plot_bakeout_logger, ut_time(sttim, /to_local), ut_time(entim, /to_local), $
                        code=0, plot_set_file=plot_set_file
    if (!d.name eq 'Z') then zbuff2file, concat_dir(outdir, filnam+'.png')
end

tv2, 600, 800, /init
;ff = file_list('$SXI_TBTV_DB_DIR', '*.plst')
ff = ['/fpp01/egse_em4/dbase/tb_tv/fg_sp_ccd_inst_tc.plst', $
      '/fpp01/egse_em4/dbase/tb_tv/fpp_flight_sensors_file_1.plst', $
      '/fpp01/egse_em4/dbase/tb_tv/fpp_flight_sensors_file_2.plst', $
      '/fpp01/egse_em4/dbase/tb_tv/fpp_struc_inst_tc.plst', $
      '/fpp01/egse_em4/dbase/tb_tv/fpp_flight_sensors_file_4.plst', $
      '/fpp01/egse_em4/dbase/tb_tv/fpp_flight_sensors_file_3.plst']
for i=0,n_elements(ff)-1 do begin
    break_file, ff(i), dsk_log, dir00, filnam, ext
    plot_set_file = ff(i)
    sag_plot_plst, plot_set_file, sttim, entim
    if (!d.name eq 'Z') then zbuff2file, concat_dir(outdir, filnam+'.png')
end

end

