;bdir = getenv('SXI_TBTV_HTML_DIR')
;bdir = '/sxi01/html/TandC/DB/589
;bdir = '/sxi01/html/TandC/DB/910
bdir = '/sxi01/html/TandC/DB/911
dirs = file_list(bdir, 'case*', /cd, file=file)
;
;outdir = '/sxi01/data/thermal/FM1_TB'
outdir = '/sxi01/data/thermal/FM2_TB'
;
mat = rd_ulin_col('/sxisw/dbase/tb_tv/mk_model_meas.tab', nocomment='#', /nohead)
;
for idir=0,n_elements(dirs)-1 do begin
    ;
    filnam = file(idir) + '.temps'
    filnam = str_replace(filnam, '_', '')		;remove underscore
    filnam = str_replace(filnam, 'ctb', 'Ctb')		;name fix
    filnam = str_replace(filnam, 'c6', 'C6')		;name fix
    filnam = str_replace(filnam, 'ctb2', 'Ctb2')	;name fix
    filnam = str_replace(filnam, 'h6', 'H6')		;name fix
    filnam = str_replace(filnam, 'htb', 'Htb')		;name fix
    filnam = str_replace(filnam, 'htb2', 'Htb2')	;name fix
    filnam = str_replace(filnam, 'h3tb', 'Htb3')	;name fix
    filnam = str_replace(filnam, 'h7tb', 'Htb7')	;name fix
    for i=13,1,-1 do filnam = str_replace(filnam, strtrim(i,2)+'T', string(i,format='(i2.2)'))	;name fix
    outfil = concat_dir(outdir, filnam)
    ;
    out = ['tbtv_mk_model_meas.pro  Ver 2.0  Program run ' + !stime, ' ']
    dir = dirs(idir)
    ff = file_list(dir, 'real_time*.txt')
    if (ff(0) eq '') then nfil = 0 else nfil = n_elements(ff)
    for ifil=0,nfil-1 do begin
	mat0 = rd_tfile(ff(ifil), 5)
	v0 = strlowcase(reform(mat0(0,*)))
	v1 = reform(mat0(1,*))
	for ilin=0,n_elements(mat(0,*))-1 do begin
	    snode = mat(2,ilin)
	    if (snode ne '') then begin
	        inode = fix(snode)
		snode2 = strtrim(abs(inode),2)
		mnems = strlowcase(strtrim(str2arr(mat(1,ilin)),2))
		;print, mnems
		v = -9999
		nn = 0
		for i=0,n_elements(mnems)-1 do begin
		    ss = where_arr( v0, mnems(i), nss)
                    if (nss ge 1) then if (abs(float(v1(ss(nss-1)))) gt 1000) then nss = 0      ;open TC
		    if (nss ne 1) then begin
			;stop, 'Cant find: ' + mnems(i)
		    end else begin
			;v = v + v1(ss(0))
			if (nn eq 0) then v = float(v1(ss(0))) $
				else v = [v, float(v1(ss(0)))]
			nn = nn + 1
		    end
		end
		if (nn ge 1) then begin
		    if (max(v)-min(v) gt 5) then begin
			print, filnam, '  ', mnems
			print, v
			print, 'Average = ', total(v)/nn
			print, 'Big delta
			;pause
		    end
		    v = total(v)/nn
		    ;v = v / nn
		    out = [out, string(inode, v, format='(i10,5x,f12.5)')]
		end
		;
		infil2 = '/sxi01/html/Inst/Thermal/TB_FM1/sinks_' + $
				str_replace(filnam, '.temps', '.inc')
		if (keyword_set(qcheck)) and (inode lt 0) and (nn ge 1) and (file_exist(infil2)) then begin
		    spawn, ['grep', snode2, infil2], r, /noshell
		    if (n_elements(r) ne 1) then stop, 'spawn resulted in more than 1 hit
		    arr = str2arr(r(0))
		    if (strpos(arr(0), 'GEN') eq -1) then v_sink = float(arr(1)) $
							else v_sink = float(arr(3))
		    extra = ''
		    if abs(v-v_sink) gt 5 then extra = ' <<<<'
		    print, filnam, inode, string(v, v_sink, v-v_sink, format='(3f12.2)'), extra
		    ;if abs(v-v_sink) gt 5 then stop, 'Large delta
		end
	    end
	end
    end
    prstr, out, file=outfil
end
;
end



stop
;
;
end