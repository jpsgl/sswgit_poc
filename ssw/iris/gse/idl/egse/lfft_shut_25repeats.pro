pro lfft_shut_25rep_plot, h, hc=hc, gif=gif
;
;
if (keyword_set(gif)) then begin
    save_dev = !d.name
    set_plot,'z'
end
if (keyword_set(hc)) then begin
    save_dev = !d.name
    setps,/port
    if (data_type(hc) eq 7) then psfil = hc else psfil = 'idl.ps'
    device, file=psfil
end
;
dattim = anytim2ints(h.EGSE_TIM)
mdur = h.shutmdur
cmd = h.exposure
if (min(cmd) ne max(cmd)) then stop, 'Exposure set not all same duration'
;
!y.style=2
!p.multi = [0,1,2]
utplot, dattim, mdur, tit='Commanded Exposure: ' + string(min(cmd), format='(f6.3)') + ' sec', $
	ytit='Seconds', /ynoz
plot_hist, mdur, bin=.00005
;
plottime, 0, 0, 'LFFT_SHUT_25REPEATS  Ver 1.00'
plottime
;
if (keyword_set(gif)) then begin
    zbuff2file, gif
    set_plot, save_dev
end
if (keyword_set(hc)) then pprint, psfil, /reset

end
;------------------------------------------
;+
;NAME:
;	lfft_shut_25repeats
;HISTORY:
;	Written 26-Mar-01 by M.Morrison
;-
;
nimg = 25*3
infil = sxi_stol2files(date, 'shut_25repeats', nimg, out=cmdout)
break_file, infil, dsk_log, dir, filnam, ext
;
if (infil(0) eq '') then begin
    print, 'Cannot find image files.  Stopping...'
    stop
end
;
mreadfits, infil, h
;
outdir = '$SXI_LFFT_DIR/tmp'
;
for i=0,2 do begin
    ist = i*25
    ien = ist + 24
    lfft_shut_25rep_plot, h(ist:ien)
    pause
    lfft_shut_25rep_plot, h(ist:ien), hc=concat_dir(outdir, 'shut25_'+filnam(0)+'_P' + strtrim(i+1,2) + '.ps')
    lfft_shut_25rep_plot, h(ist:ien), gif=concat_dir(outdir, 'shut25_'+filnam(0)+'_P' + strtrim(i+1,2) + '.gif')
end
;
end