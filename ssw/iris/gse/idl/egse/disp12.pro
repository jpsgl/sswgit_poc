;HISTORY:
;	Written 30-Nov-99 by M.Morrison
;V1.0	11-Mar-00 (MDM) - Finished up the routine
;V1.01	11-Mar-00 (MDM) - Added option for passing in background file
;V1.02	11-Mar-00 (MDM) - Fixed case where dark_file is not passed in
;V1.03	 7-Apr-00 (MDM) - Reset !p.font after changing
;V1.04	 7-Apr-00 (MDM) - Corrected that background image was saved from
;			  previous run and still used.
;V1.05	21-Jan-01 (MDM) - Added erase statement for z buff prob
;
;--------------------------------------------------------------------
pro disp12_1page, infil0, tit, ipage, ist, ien, $
	lab1_arr=lab1_arr0, hc=hc, gif=gif, option=option, dark_file=dark_file, rotate=rotate
;
common disp12_1page_blk1, last_dark_file, dimg
;
if (n_elements(last_dark_file) eq 0) then last_dark_file = 'xx'
if (keyword_set(dark_file)) then begin
    last_dark_file = dark_file
    dimg = rfits(dark_file)
end else begin
    dimg = 0
end
;
infil = infil0(ist:ien)
if (n_elements(lab1_arr0) eq 0) then lab1_arr0 = strarr(n_elements(infil0))
lab1_arr = lab1_arr0(ist:ien)
if (n_elements(tit) eq 0) then tit = ''
;
if (n_elements(option) eq 0) then option = ''
;

nxstep = 100
nystep= 350
;nout = 160
nout=320
nx = 6
ny = 2
xsz=600
ysz=780
pfont_save = !p.font
!p.font = 1
tv2, xsz, ysz, /ini
erase
spart = strtrim(ipage+1,2)
xyouts2, (xsz/2), (ysz-30), tit + '(Part ' + spart + ')', size=2, /dev, align=0.5
if (keyword_set(dark_file)) then xyouts2, 300, 850, 'Dark subtracted with: ' + dark_file, size=1, /dev, align=0.5

;
nn = n_elements(infil) < (nx*ny)
for ifil=0,nn-1 do begin
    break_file, infil(ifil), dsk_log, dir, filnam, ext
    img = rfits(infil(ifil), h=h)
;    img=rotate(img,1)
    if (keyword_set(dimg)) and (last_dark_file ne infil(ifil)) then img = img - dimg
    nxi = float( data_chk(img, /x) )
    nyi = float( data_chk(img, /y) )
	print, nxi, nyi
    scale = (nout/nxi) < (nout/nyi)
	print, scale
    case strupcase(option) of
	'STDEV': begin
		idev = stdev(img, iavg)
		smin = iavg - idev
		smax = iavg + idev
	      end
	else: begin
		smin = min(img, max=smax)
	      end
    endcase
    bimg = bytscl( congrid(img, nxi*scale, nyi*scale), min=smin, max=smax, top=!d.table_size-1)
	help, bimg 
   print, smin, smax
    ;
    ix = ifil mod nx
    iy = ny-1 - (ifil/nx)
    
    tv2, bimg, ix*nxstep, iy*nystep+50
    xyouts2, ix*nxstep, iy*nystep+35, filnam, /dev
    xyouts2, ix*nxstep, iy*nystep+25, lab1_arr(ifil) + '  ' + strtrim(fix(nxi),2) + 'x' + strtrim(fix(nyi),2), /dev
    slab = 'SMIN: ' + strtrim(fix(smin),2) + '  SMAX: ' + strtrim(fix(smax),2)
    xyouts2, ix*nxstep+nxi*scale+10, iy*nystep+50, slab, /dev, orient=90
;xyouts2, ix*nxstep+30, iy*nystep+35, slab, /dev
end
;
xyouts2, 0, 0, 'DISP12  Ver 1.05  Run: ' + !stime
;
!p.font = pfont_save
end
;----------------------------------------------------------------------
pro disp12, infil, tit, lab1_arr=lab1_arr, hc=hc, gif=gif, option=option, $
	dark_file=dark_file, rotate=rotate
;
if (n_elements(infil) eq 0) then return
if (infil(0) eq '') then return
;
n = n_elements(infil)
npage = ceil(n / 12.)
for i=0,npage-1 do disp12_1page, infil, tit, i, i*12, (i*12+11)<(n-1), $
		lab1_arr=lab1_arr, option=option, dark_file=dark_file, rotate=rotate
;
;---- Optionally write results to GIF file
;
if (keyword_set(gif)) then begin
    save_dev = !d.name
    set_plot,'z'
    for i=0,npage-1 do begin
	disp12_1page, infil, tit, i, i*12, (i*12+11)<(n-1), $
			lab1_arr=lab1_arr, option=option, dark_file=dark_file
	spart = strtrim(i+1,2)
        zbuff2file, str_replace(gif, '.gif', '_P' + spart + '.gif')
    end
    set_plot, save_dev
end

;---- Optionally write results to PS file
;
if (keyword_set(hc)) then begin
    save_dev = !d.name
    setps,/port
    for i=0,npage-1 do begin
	if (data_type(hc) eq 7) then psfil = hc else psfil = 'idl.ps'
	spart = strtrim(i+1,2)
	psfil = str_replace(psfil, '.ps', '_P' + spart + '.ps')
	device, file=psfil
	disp12_1page, infil, tit, i, i*12, (i*12+11)<(n-1), $
			lab1_arr=lab1_arr, option=option, dark_file=dark_file
	device, /close
	sprint, psfil
    end
    set_plot, save_dev
end
;

end
