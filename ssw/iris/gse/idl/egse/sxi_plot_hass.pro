pro sxi_plot_hass, outfil=outfil, hc=hc, date=date, recs=recs, $
	qstop=qstop, lastnrec=lastnrec, gif=gif, $
	timout=timout, y1out=y1out, y2out=y2out
;
;HISTORY:
;	Written Oct-00 by M.Morrison
;V1.01	16-Nov-00 (MDM) - changes required for misunderstanding 
;			  of pktcount.
;			- Reversed plotting label of Y and Z axis
;			- Changed to use drec time, not pkt head time
;V1.02	21-Dec-00 (MDM) - Implemented /hc option
;V1.03	 2-Jan-01 (MDM) - Fixed case where there were not diagnostic data
;V1.04	26-Mar-01 (MDM) - Added gif
;			- For HC option, send to saved file option
;V1.05	30-Jan-02 (MDM) - Added timout,y1out,y2out
;V1.06	19-Jun-02 (MDM) - Added mnemonic names to labels
;
if (keyword_set(gif)) then begin
    save_dev = !d.name
    set_plot,'z'
end
if (keyword_set(hc)) then begin
    save_dev = !d.name
    setps,/land
    if (data_type(hc) eq 7) then psfil = hc else psfil = 'idl.ps'
    device, file=psfil
end
;
if (n_elements(recs) eq 0) then begin
    if (n_elements(date) eq 0) then date = ut_time()
    fid = time2file( date, /date)
    infil = '$GSE_PKT_FILE_DIR/0x0018/' + fid + '.0x0018'
    head = sag_rd_hk(infil, 0, 1000, /map)
    if (data_type(head) ne 8) then begin
	print, 'No data in input file: ' + infil
	return
    end
    n = n_elements(head)
    ist = 0
    if (keyword_set(lastnrec)) then ist = (n-lastnrec)>0
    recs = sag_rd_hk(infil, ist, n-1)
end
;
if (data_type(recs) ne 8) then return
;
;dattim = sxi_time_conv('INSTRUMENT', struct=recs.pkt_head, /string, /msec)	;removed with V1.01
dattim = sxi_time_conv('INSTRUMENT', struct=recs.drec, /string, /msec)
;offset = recs.pkt_head.time + unsign(recs.pkt_head.ftime)/65536.
;offset = recs.drec.time - recs(0).drec.time + unsign(recs.drec.ftime)/65536D + (recs.drec.pcnt)*200D*0.016
pcnt = recs.drec.pcnt - 1 
ss = where(recs.drec.serno eq 0, nss)	;non image packets (diagnostics)
if (nss ne 0) then pcnt(ss) = 0
offset = recs.drec.time - recs(0).drec.time + unsign(recs.drec.ftime)/65536D + pcnt*200D*0.016
;
n = n_elements(recs)
n2 = n*200
;reftim = getenv('SAG_INSTRUMENT_EPOCH')
reftim = dattim(0)
tim = (dindgen(200,n) mod 200) * .016   ;16 msec samples
for i=0,n-1 do tim(*,i) = tim(*,i) + offset(i)    ;offset
;
!p.multi=[0,1,2]
x = tim
y1 = recs.data(0,*)
y2 = recs.data(1,*)
ss = where( (y1 ne 0) and (y2 ne 0), nss)
if (nss eq 0) then stop
utplot, tim(ss), y1(ss), reftim, psym=3, ytit='Rot around Y axis (Z motion) (HASSERRORZ)', /ynoz
utplot, tim(ss), y2(ss), reftim, psym=3, ytit='Rot around Z axis (Y motion) (HASSERRORY)', /ynoz
;
plottime, 0, 0, 'SXI_PLOT_HASS  Ver 1.06
plottime

timout = tim(ss)
y1out = y1(ss)
y2out = y2(ss)

if (keyword_set(gif)) then begin
    zbuff2file, gif
    set_plot, save_dev
end
if (keyword_set(hc)) then pprint, psfil, /reset
;
if (keyword_set(qstop)) then stop
end
