function iri_inhibit_skip_analysis, infils
vers='iri_inhibit_skip_analysis	version 1.0	'+systime()

blk=100

mreadfits,infils,head

nimg=n_elements(head)

out=vers
out=[out,'Image FSN       Inst  NPix        Mean      StDev']

nloops=1
if (nimg gt blk) then nloops=ceil(nimg/(float(blk)))
for k=0,nloops-1 do begin
	first=k*blk
	last=((k+1)*blk)-1
	nimg_loop=blk
	if ((k eq (nloops-1)) and ((nimg mod blk) ne 0)) then begin
		last=first+((nimg mod blk)-1)
		nimg_loop=(nimg mod blk)
	endif
	mreadfits,infils(first:last),head,img

	for i=0L,nimg_loop-1 do begin
		iimg=img(*,*,i)
		imgn=first+i+1
		fsn=head(i).fsn
		inst=head(i).instrume
		kk=where(iimg gt -30000,nkk)
		npix=nkk
		mm=moment(iimg(kk),mean=imean,sdev=isdev)
	;	imean=mm(0)
	;	ivar=mm(1)
		out=[out,string(imgn,fsn,inst,npix,imean,isdev,format="(i-6,i-10,a-6,i-12,i-10,f-12.3)")]
	endfor

endfor

prstr,out,/nomore
return,out
end
