;+
;$Id: hmi_hr_pkt_indicators_main.pro 4942 2012-01-13 03:52:02Z sabolish $
;$Name:  $
; main:  hmi_hr_pkt_indicators_main.pro
;
; call with batch enabled with 'yes' and path for png file.
;
;	History: 
;	23-Jan-07, G.Linford, change font size for plots to a readable size
;						Also change to true-type fonts.
;	11-Jul-07, G.Linford, SSIM Thermal point name changes CCD temps,
;		'E_S_C_H_TS104_CCD1_HDR_PRI' -> 'E_S_C_H_TS105_CCD1_SIDE_PRI'
;		'E_S_C_H_TS105_CCD2_HDR_PRI' -> 'E_S_C_H_TS104_CCD2_FRNT_PRI'
;
;$Log: hmi_hr_pkt_indicators_main.pro,v $
;Revision 1.5  2008/06/08 18:07:10  linford
;changed order, so SC monitor points come first
;
;Revision 1.4  2008/04/21 16:23:02  sdotc
;fix typo on SC_CURR file name
;
;Revision 1.3  2008/04/16 23:15:22  linford
;update hourly housekeeping packet main routine
;
;Revision 1.2  2007/09/30 00:00:57  linford
;fix idl ending mark for header, cvs tags
;
;Revision 1.1  2007/09/28 16:50:47  linford
;Add cvs tags Id,Name, Log
;
;
;-

!p.charsize=2.0			; use a large font size then the default
!p.font=1.0				; use true-type fonts

get_utc,sday,/stime
res = strSplit(sday,' ',/extract)
today = res[0]		; get current UTC day format: dd-MMM-YYYY

; set up indicator plot for packet 0x05 (kernel)
indicators = ['HMI_KER_VER_NUM_HK','HMI_KER_CM_ACCEPT','HMI_KER_CM_ACCEPT_SS']
day_plot_pkt_indicators2, 'yes', '0x05_', indicators, '/home/sdotc/hmi_html/tmp', DAY=today

; set up indicator plot for SC cur and volt
indicators = ['E_S_C_PA_HMIA_DC','E_S_C_PA_HMIB_DC','E_S_C_PB_HMIA_DC','E_S_C_PB_HMIB_DC']
day_plot_pkt_indicators2, 'yes', 'SC_CURR_', indicators,'/home/sdotc/hmi_html/tmp', DAY=today

; setup indicator plot for main SC BUS voltage
indicators = ['E_S_C_PA_VBUS','E_S_C_PB_VBUS']
day_plot_pkt_indicators2, 'yes', 'SC_VOLT_', indicators,'/home/sdotc/hmi_html/tmp', DAY=today

; Heater zone plots:
indicators=['Y_HMI_TS01_WINDOW_MTG_RING1','Y_HMI_TS02_WINDOW_MTG_RING2','Y_HMI_TS05_TELE_FWD1','Y_HMI_TS39_OPT_FWDCVR','Y_HMI_TS40_OPT_AFTCVR','Y_HMI_TS23_OPT_AFTBENCH','Y_HMI_TS37_OPT_FWD_BENCH_EXT']
day_plot_pkt_indicators2, 'yes', 'HTR_ZN_', indicators,'/home/sdotc/hmi_html/tmp', DAY=today

; Set up CCD temp plots:
indicators=['HMI_TS201_CEB1_CCD_TEMP1','HMI_TS203_CEB1_CCD_TEMP2','HMI_TS202_CEB2_CCD_TEMP1','HMI_TS204_CEB2_CCD_TEMP2']
day_plot_pkt_indicators2, 'yes', 'CCD_', indicators,'/home/sdotc/hmi_html/tmp', DAY=today

; Set up CEB temp plots:
indicators=['Y_HMI_TS28_CAM2_FRNT_CEB_EXT','Y_HMI_TS29_CAM1_SIDE_CEB_EXT']
day_plot_pkt_indicators2, 'yes', 'CEB_EXT_', indicators,'/home/sdotc/hmi_html/tmp', DAY=today

; set up indicator plot for HEB for temps:
indicators = ['Y_HMI_TS30_HEB_BPLATE_INT','Y_HMI_TS31_HEB_PWR_CNVR_PRI','Y_HMI_TS33_HEB_RAD6000_CPU_A','Y_HMI_TS34_HEB_RAD6000_CPU_B']
day_plot_pkt_indicators2, 'yes', 'HEB_', indicators, '/home/sdotc/hmi_html/tmp', DAY=today

;set up indicator plot for OVEN for temps:
indicators = ['Y_HMI_TS12_OVN_LYOT','Y_HMI_TS13_OVN_WBM','Y_HMI_TS14_OVN_NBM','Y_HMI_TS15_OVN_PREAMP']
day_plot_pkt_indicators2, 'yes', 'OVEN_', indicators, '/home/sdotc/hmi_html/tmp', DAY=today

;set up indicator plot for WT-ENCODER:
indicators = ['H11_WT1_ENCODER','H11_WT2_ENCODER','H11_WT3_ENCODER','H11_WT4_ENCODER','E_HMI_PWR_MOTOR_CURR_AVE']
day_plot_pkt_indicators2, 'yes', 'HCM-WT_', indicators, '/home/sdotc/hmi_html/tmp', DAY=today

;set up indicator plot for PS-ENCODER:
indicators = ['H11_PS1_ENCODER','H11_PS2_ENCODER','H11_PS3_ENCODER','E_HMI_PWR_MOTOR_CURR_AVE']
day_plot_pkt_indicators2, 'yes', 'HCM-PS_', indicators, '/home/sdotc/hmi_html/tmp', DAY=today

;set up indicator plot for CF-ENCODER:
indicators = ['H11_CF1_ENCODER','H11_CF2_ENCODER','E_HMI_PWR_MOTOR_CURR_AVE']
day_plot_pkt_indicators2, 'yes', 'CAL-FOCUS_', indicators, '/home/sdotc/hmi_html/tmp', DAY=today

;set up indicator plot for SH-ENCODER:
indicators =['HMI_SH1_ENCODER','HMI_SH2_ENCODER','HMI_SH1_ENCODER_ERROR','HMI_SH2_ENCODER_ERROR','E_HMI_PWR_MOTOR_CURR_AVE']
day_plot_pkt_indicators2, 'yes', 'SHUTTER_', indicators, '/home/sdotc/hmi_html/tmp', DAY=today

; set up indicator plot for SEQ status packet 0x015
indicators =['H15_SEQ_STATE','H15_SEQ_FRAME_COUNT','H15_SEQ_FILTERGRAM_SN']
day_plot_pkt_indicators2, 'yes', 'SEQ_FRAME_', indicators, '/home/sdotc/hmi_html/tmp', DAY=today

; setup indicator plot for SEQ FSN status (do not plot values of FSN=0) 
indicators =['H15_SEQ_FILTERGRAM_SN']
day_plot_pkt_indicators2, 'yes', 'SEQ_FSN_', indicators, '/home/sdotc/hmi_html/tmp',/yrange, DAY=today

end
