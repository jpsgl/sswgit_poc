;
;
;
;HISTORY:
;	Written Nov-99 by M.Morrison
;	11-Mar-00 (MDM) - Changed how start time is grabbed if no "ion"
;			- Removed pause
;	10-May-00 (MDM) - Changed to make "sttim" the time of the LAST ion (not first)
;			- Made fix for case where there is no ioff
;	26-May-00 (MDM) - List all on/off times
;	26-Mar-01 (MDM) - Removed hardwired lfft dir
;
set_plot,'x
outdir = getenv('SXI_LFFT_DIR') + '/tmp'
;
if (not keyword_set(qskip_times)) then begin
    if (n_elements(date) eq 0) then date = gt_day( ut_time(), /str)
    out = sxi_hist_sea(date, 'perform ion')
    if (data_type(out) ne 8) then begin
	sttim = anytim2ints(ut_time(), off=-12*60.*60)
	input, 'Enter starting time', tmp, fmt_tim(sttim)
	sttim = anytim2ints(tmp)
    end else begin
	print, '------------ List of ION times -------------
	prstr, fmt_tim(out), /nomore
	sttim = out( n_elements(out)-1 )
    end
    ;
    out2 = sxi_hist_sea(date, 'Perform completed for procedure: ioff')
    if (data_type(out2) ne 8) then begin
	entim = anytim2ints(ut_time())
    end else begin
	print, '------------ List of IOFF times -------------
	prstr, fmt_tim(out2), /nomore
	entim = out2(n_elements(out2)-1)
    end
    ;
    dt = int2secarr(entim, sttim)
    if (dt lt 0) then entim = anytim2ints(ut_time())
end
;
input, 'Start time: ', sttim, fmt_tim(sttim)
input, 'End time:   ', entim, fmt_tim(entim)
;
clearplot
ff = file_list('/sxisw/dbase/lfft/', 'summary_mnem*.tab')
for i=0,n_elements(ff)-1 do begin
    break_file, ff(i), dsk_log, dir00, filnam, ext
    list = rd_tfile(ff(i))
    mnem_mat = str2cols(list)
    mnem = strlowcase(strtrim(mnem_mat(0,*),2))
    info = sag_get_mnem(sttim, entim, mnem)
    sag_hkplot, info, /label, /qlinestyle, /colors
    ;
    if (not keyword_set(qskip_hc)) then begin
	;--- Hardcopy & PS archive
	;
	psfil = concat_dir(outdir, filnam + '.ps')
	setps,/land
	device, file=psfil
	sag_hkplot, info, /label, /qlinestyle, /noprint
	device, /close
	sprint, psfil
	;
	;--- GIF
	;
	set_plot,'Z'
	sag_hkplot, info, /label, /qlinestyle, /colors
	zbuff2file, concat_dir(outdir, filnam + '.gif')
	set_plot, 'x'
    end

    ;pause
end
;
end