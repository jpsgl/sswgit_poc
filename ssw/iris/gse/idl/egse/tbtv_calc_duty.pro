function tbtv_calc_duty, item
;
;
;
backdur = 2.0	;hrs
if (item eq 'case08') then backdur = 0.4
;if (n_elements(item) eq 0) then item = 'case01'
;input, 'Enter the case name', item, item
case item of
    'case01': begin & sttim = '24-Apr  9:45'	& entim = '26-Apr 10:45' 	& end
    'case02': begin & sttim = '26-Apr 10:45'	& entim = '27-Apr  8:20' 	& end
    'case03': begin & sttim = '27-Apr  8:20'	& entim = '27-Apr 21:00' 	& end
    'case04': begin & sttim = '27-Apr 21:00'	& entim = '28-Apr 15:30' 	& end
    'case05': begin & sttim = '28-Apr 15:45'	& entim = '29-Apr 10:00' 	& end
    'case05a':begin & sttim = '29-Apr 10:00'	& entim = '30-Apr 12:30' 	& end
    'case07': begin & sttim = '29-Apr 10:00'	& entim = '1-May 6:00'	 	& end
    'case08': begin & sttim = '1-May 6:00'	& entim = '1-May 15:30'	 	& end
    'case09': begin & sttim = '1-May 15:30'	& entim = '2-May 11:00'	 	& end
    'case06': begin & sttim = '2-May 11:00'	& entim = '3-May 10:30'	 	& end
    'case10': begin & sttim = '3-May 10:30'	& entim = '4-May 10:30'	 	& end
    else:
endcase
sttim2 = anytim2ints(entim, off=-60.*60*backdur)
;
mnem = ['SXI_FORZONETMP', 'SXI_FORZONEDBL', 'SXI_FORZONEDBH', 'SXI_HTRFORE', 'SXI_FORZONEDC', $
	'SXI_AFTZONETMP', 'SXI_AFTZONEDBL', 'SXI_AFTZONEDBH', 'SXI_HTRAFT',  'SXI_AFTZONEDC', $
	'SXI_CCDZONETMP', 'SXI_CCDZONEDBL', 'SXI_CCDZONEDBH', 'SXI_HTRCCD',  'SXI_CCDZONEDC', $
	'SXI_POWER_REG']

info = sag_get_mnem( ut_time(sttim2), ut_time(entim), mnem, /raw)
info.hk.value(*,0) = 255-info.hk.value(*,0)
info.hk.value(*,5) = 255-info.hk.value(*,5)
;
n = n_elements(info.hk.value(*,0)) * 1. - 1
out0 = string(item, format='(a7)')
labarr = ['Fore', 'Aft ', 'CCD ']
for ii=0,10,5 do begin
    sec_on = fltarr(n)
    temp = info.hk.value(0:n-1,ii)
    stat = info.hk.value(1:n,ii+3)	;status goes with the previous temperature
    dblo = info.hk.value(0:n-1,ii+1)
    dc = info.hk.value(0:n-1,ii+4)			;duty cycle
    dblo0 = dblo(0)
    dc0 = dc(0)
    ;
    ;-- simply showing status of on
    ss=where( (temp ge dblo0) and (stat eq 2), nss)	;on
    if (min(dc) ne max(dc)) then stop, 'DC not same throughout'
    if (nss ne 0) then sec_on(ss) = (dc0<4)
    ;
    ;-- On, but "hard-on" because value is below DBLOW
    ;ss2=where( (temp lt dblo0) and (stat ne 1), nss2)
    ss2=where( (temp lt dblo0), nss2)
    if (min(dblo) ne max(dblo)) then stop, 'DBLOW not same throughout'
    if (nss2 ne 0) then sec_on(ss2) = 4		;fell below low DB
    ;
    ;-- Over high and disabled (off)
    ss3=where( (stat eq 4), nss3)
    ;
    ddc = total(sec_on) / (n*4) * 100
    ;if (nss2 eq 0) and (nss3 eq 0) then ddc = dc0*5.	;no correction needed
    if (nss2 eq 0) then ddc = dc0*5.	;no correction needed
    if (min(stat) eq max(stat)) and (max(stat) eq 0) then ddc = 0
    out0 = out0 + string(labarr(ii/5), dc0*5., nss/n*100, nss2/n*100, nss3/n*100, ddc, format='(2x, a, 5f6.1)')
end
out0 = out0 + '  ' + fmt_tim(sttim2) + string(backdur, format='(f4.1)') + ' hrs'
print, out0
;if (item eq 'case07') then stop
;
return, out0
end
;-------------------------------------------------------------
;
;
setenv,'SXI_EGSE_DB_FILE=' + getenv('SXISW') + '/egse_v4.x/dbs/sxiem1_v00.175'
if (keyword_set(qstop)) then stop
;
out = strarr(10)
;cases = 'case' + str2arr('01,02,03,04,05,05a,07,08,09,06')
cases = 'case' + str2arr('01,02,03,04,05,05a,06,07,08')
for i=0,n_elements(cases)-1 do out(i) = tbtv_calc_duty(cases(i))
end