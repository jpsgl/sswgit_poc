;
;
;
;
cfid = time2file(ut_time(), /date_only)
if (n_elements(infil) eq 0) then infil = getenv('GSE_PKT_FILE_DIR') + '/0x0010/' + cfid + '.0x0010'
;
qdone = 0
last_newest = 0
while (not qdone) do begin
    map = sag_rd_hk(infil, /map)
    v = map.egse_time
    n = n_elements(v)
    newest = v(n-1)
    if (newest ne last_newest) then begin
	edattim = fmt_tim(anytim2ints(getenv('SAG_EGSE_EPOCH'), off=double(newest)))
	idattim = fmt_tim(anytim2ints(getenv('SAG_INSTRUMENT_EPOCH'), off=double(map(n-1).time)))
	cdattim = ut_time()
	print, 'EGSE Last Pkt: ' + edattim + '  LOBT: ' + idattim + '  Current GMT: ' + cdattim
	last_newest = newest
    end
    qdone = get_kbrd(0) eq 'q'
    wait, 1
end
;
end