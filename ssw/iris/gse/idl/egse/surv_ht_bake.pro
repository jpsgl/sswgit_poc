
info = {DBLO: 0., DBHI: 0., duty: 0., last_trans: '??', state: 'INI'}
;			STATE = ON (<LO), DB (LO2HI), OFF
tel = info
ceb = info
;
if (n_elements(tel_duty) eq 0) then tel_duty = 50.
if (n_elements(ceb_duty) eq 0) then ceb_duty = 50.
input, 'Telescope duty cycle (%)', tel_duty, tel_duty
input, 'CEB duty cycle (%)      ', ceb_duty, ceb_duty
;
tel.dblo = 35		;deg cel
tel.dbhi = 38		;deg cel
tel.duty = tel_duty
;
;
ceb.dblo = 20		;deg cel
ceb.dbhi = 25		;deg cel
ceb.duty = ceb_duty
;
surv_ht_control, tel, ceb
;
end