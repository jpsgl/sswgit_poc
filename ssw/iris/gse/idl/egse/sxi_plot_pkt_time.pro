pro sxi_plot_pkt_time, infil, new_pkt_time, map, etime, itime, $
		qdebug=qdebug, qstop=qstop, qplot=qplot, $
		timerange=timerange, date=date, apid=apid, hc=hc, $
		noyrange=noyrange
;
;
;HISTORY:
;	Written Oct-99 by M.Morrison
;V1.02	10-Nov-99 (MDM) - Plot with /ynozero
;V1.03	26-Nov-00 (MDM) - Renamed sxi_plot_pkt_time from sxi_patch_pkt_time,
;V1.04	20-Dec-02 (MDM) - Added /noyrange option
;V1.05	10-Jan-03 (MDM) - Added debug info
;
if (n_elements(infil) eq 0) then begin
    if (n_elements(date) eq 0) then date = ut_time()
    if (n_elements(apid) eq 0) then apid = '0x0010'
    fid = time2file( date, /date)
    infil = '$GSE_PKT_FILE_DIR/' + apid + '/' + fid + '.' + apid
end

map = sag_rd_hk(infil, /map)
etime = anytim2ints(getenv('SAG_EGSE_EPOCH'), off=double(map.egse_time))
;
lobt = map.time + unsign(map.ftime)/2D^16
etim = map.egse_time
n = n_elements(etim)
itime0 = anytim2ints(getenv('SAG_INSTRUMENT_EPOCH'), off=double(lobt))
;
new_pkt_time = lobt
epoch_off = int2secarr( getenv('SAG_EGSE_EPOCH'), getenv('SAG_INSTRUMENT_EPOCH') )
offset = etim(0) + epoch_off - lobt(0)
if (keyword_set(qdebug)) then print, 'Init Offset: ', offset
new_pkt_time(0) = new_pkt_time(0) + offset
for i=1,n-1 do begin
    offset0 = offset
    if (lobt(i) lt lobt(i-1)) or ((lobt(i)-lobt(i-1)) gt 1e+4) then begin	;CC reboot - start new reference
	offset = etim(i) + epoch_off - lobt(i)
	if (keyword_set(qdebug)) then print, 'E:' + fmt_tim(etime(i)) + ' I:' + fmt_tim(itime0(i)) + $
				'  New offset: ', offset, '  Delta = ', offset-offset0, ' Raw: ', lobt(i)
    end
    new_pkt_time(i) = new_pkt_time(i) + offset
end

etime = anytim2ints(getenv('SAG_EGSE_EPOCH'), off=double(map.egse_time))
itime = anytim2ints(getenv('SAG_INSTRUMENT_EPOCH'), off=double(new_pkt_time))
;
if (keyword_set(hc)) then setps, /land
    !p.multi = [0,1,3]
    !p.charsize=2
    reftim = etime(0)
    detime = deriv_arr( int2secarr(etime, reftim) )
    ditime = deriv_arr( int2secarr(itime, reftim) )
    detime2 = detime >3<10
    ditime2 = ditime >3<5
    if (keyword_set(noyrange)) then begin
	ss3 = where(detime lt 1000)
	detime2 = detime < max(detime(ss3))
	ditime2 = ditime < max(ditime(ss3))
    end
    utplot, etime, detime2, tit='EGSE Packet receipt delta time', $
		timerange=timerange, psym=1
    utplot, etime, ditime2, tit='LOBT Packet generation delta time', $
		timerange=timerange, psym=1, /ynozero
    ss = where((ditime gt 3) and (ditime lt 5), nss)
    avg = total(ditime(ss))/ nss
    plottimes, .1, .8, 'Average: ' + strtrim(avg,2)
    utplot, etime, (int2secarr(etime, reftim) - int2secarr(itime, reftim))>0, tit='EGSE - LOBT offset', $
		timerange=timerange, psym=1
    plottime, 0, 0, 'SXI_PLOT_PKT_TIME  Ver 1.04
    plottime
    pprint

if (keyword_set(hc)) then set_plot,'x'
if (keyword_set(qstop)) then stop
end
