pro fw_debug_s1, tit, v, ssss, lun, dattim
;
;
;
uv = v(uniq(v,sort(v)))
n = n_elements(uv)
print, tit
printf, lun, tit
for i=0,n-1 do begin
    ss = where(v eq uv(i), nss)
    extra = ''
    ;if (nss lt 10) then extra = arr2str(strtrim(ss,2))
    ;if (nss lt 10) then extra = arr2str(strtrim(ssss(ss),2))
    if (nss lt 10) then extra = arr2str(strtrim(ssss(ss),2) + ' (' + $
			fmt_tim(dattim(ss)) + ')' )
    print, '   Value ', uv(i), uv(i), ' # occ ', nss, extra, $
			format='(a, i4, "  0x",z2.2, a, i6, 4x, a)'
    printf, lun, '   Value ', uv(i), uv(i), ' # occ ', nss, extra, $
			format='(a, i4, "  0x",z2.2, a, i6, 4x, a)'
end
;
end
;---------------------------------------------------------------
pro fw_debug, indir, init=init
;
;
;
common fw_debug_blk2, h, v, dattim
;
if (keyword_set(init)) then delvarx, h
;
if (n_elements(h) eq 0) then begin
    print, 'Processing: ', indir
    ff=file_list(indir,'*.fit')
    if (ff(0) eq '') then return
    print, 'Reading ', n_elements(ff), ' files
    mreadfits,ff(n_elements(ff)-1),h0
    mreadfits,ff,h,strtemplate=h0
    v=h.img_sn  
    ;;ss1=where( (v mod 2) eq 0)
    ;;ss2=where( (v mod 2) eq 1)
    ;;plot,byte(h(ss1).f1enc) 
    dattim=anytim2ints(h.egse_tim)
end
;
outfil = concat_dir(indir, 'fw_debug.txt')
openw, lun, outfil, /get_lun
;
x=int2secarr(dattim)
if (tag_exist(h,'f1enc')) then begin
    f1 = byte(h.f1enc)
    f2 = byte(h.f2enc)
end else begin
    f1 = fix(h.f1pos)
    f2 = fix(h.f2pos)
end
sn = h.img_sn
percentd = h.percentd
dx=deriv_arr(x)
ss = where(dx gt 10, nss)
for i=0,nss do begin
    if (i eq 0) then ist=0 else ist = ss(i-1)+1
    ;if (i eq nss) then ien=n_elements(x)-1 else ien = ss(i)
    if (i eq nss) then ien=n_elements(x)-3 else ien = ss(i)-1
    ist = (ist + 2)       ;skip startup frames
    qstop=0
    if (ien gt ist) then begin
      print, i, ist, ien, ien-ist+1, x(ist), x(ien)
      ;sso = where( (sn(ist:ien) mod 2) eq 1, nsso)
      ;sse = where( (sn(ist:ien) mod 2) eq 0, nsse)
      sso = where( ((sn(ist:ien) mod 2) eq 1) and (percentd(ist:ien) eq 100), nsso)
      sse = where( ((sn(ist:ien) mod 2) eq 0) and (percentd(ist:ien) eq 100), nsse)
      print, '   FW1', min(f1(sso+ist)), max(f1(sso+ist)), nsso, min(f1(sse+ist)), max(f1(sse+ist)), nsse
      print, '   FW2', min(f2(sso+ist)), max(f2(sso+ist)), nsso, min(f2(sse+ist)), max(f2(sse+ist)), nsse
      qstop = (min(f1(sso+ist)) ne max(f1(sso+ist))) or $
        (min(f1(sse+ist)) ne max(f1(sse+ist))) or $
        (min(f2(sso+ist)) ne max(f2(sso+ist))) or $
        (min(f2(sse+ist)) ne max(f2(sse+ist))) 
    end
    ;;if (i eq 14) then qstop = 1
    ;
    print, 'Input Directory: ', indir
    print, 'FW_DEBUG Program Run: ', !stime
    printf, lun, 'Input Directory: ', indir
    printf, lun, 'FW_DEBUG Program Run: ', !stime, '  Ver 2.01'
    qprint = 1
    if (ien lt ist) then qprint = 0
    if (qprint) then begin
	print, 'Series #', i+1, ' Images ', ist, ' to ', ien, ' Total of ', ien-ist+1, ' Images
	printf, lun, 'Series #', i+1, ' Images ', ist, ' to ', ien, ' Total of ', ien-ist+1, ' Images
	;
        !p.multi = [0,1,2]
        plot, f1(ist:ien), psym=10, ytit='F1 Encoder
        plot, f2(ist:ien), psym=10, ytit='F2 Encoder
        print, 'F1 Even frames ---------'
        print, f1(sse+ist)
        print, 'F1 Odd frames ---------'
        print, f1(sso+ist)
        print, 'F2 Even frames ---------'
        print, f2(sse+ist)
        print, 'F2 Odd frames ---------'
        print, f2(sso+ist)

        ssb1 = where( (f1(sse+ist) ne f1(sse(nsse-1)+ist)) or (f1(sso+ist) ne f1(sso(nsso-1)+ist)), nssb1)
        ssb2 = where( (f2(sse+ist) ne f2(sse(nsse-1)+ist)) or (f2(sso+ist) ne f2(sso(nsso-1)+ist)), nssb2)
        print, 'Number of bad positions for FW1', nssb1
        print, 'Number of bad positions for FW2', nssb2
        printf, lun, 'Number of bad positions for FW1', nssb1
        printf, lun, 'Number of bad positions for FW2', nssb2
	fw_debug_s1, 'F1 Even frames ---------', f1(sse+ist), sse+ist, lun, dattim(sse+ist)
        fw_debug_s1, 'F1 Odd frames  ---------', f1(sso+ist), sso+ist, lun, dattim(sse+ist)
        fw_debug_s1, 'F2 Even frames ---------', f2(sse+ist), sse+ist, lun, dattim(sse+ist)
        fw_debug_s1, 'F2 Odd frames  ---------', f2(sso+ist), sso+ist, lun, dattim(sse+ist)
    end
    ;if (min(f1(sso+ist)) ne max(f1(sso+ist))) then stop
    ;if (min(f1(sse+ist)) ne max(f1(sse+ist))) then stop
    ;if (min(f2(sso+ist)) ne max(f2(sso+ist))) then stop
    ;if (min(f2(sse+ist)) ne max(f2(sse+ist))) then stop
end

free_lun, lun
;
;stop
end
;--------------------------------------------------------------

common fw_debug_blk2, h, v, dattim

goto, skip
    ;ff=file_list('/cdrom/cdrom#2/20010809','*.fit')
    ;ff=file_list('/cdrom/cdrom#3/20010808','*.fit')

;fw_debug, '/sxi83/fits/20020305', /init
;fw_debug, '/sxi83/fits/20020306', /init
fw_debug, '/sxi83/fits/20020307'		;, /init
fw_debug, '/sxi83/fits/20020308', /init
fw_debug, '/sxi83/fits/20020309', /init

fw_debug, '/sxi83/fits/20020321', /init
fw_debug, '/sxi83/fits/20020322', /init
fw_debug, '/sxi83/fits/20020326', /init
fw_debug, '/sxi83/fits/20020327', /init

fw_debug, '/sxi83/fits/20020327'

fw_debug, '/sxi83/fits/20020327', /init

fw_debug, '/sxi83/fits/20020426', /init

skip:
fw_debug, '/sxi83/fits/20020409', /init
fw_debug, '/sxi83/fits/20020410', /init
fw_debug, '/sxi83/fits/20020411', /init
fw_debug, '/sxi83/fits/20020412', /init
fw_debug, '/sxi83/fits/20020413', /init
fw_debug, '/sxi83/fits/20020415', /init
fw_debug, '/sxi83/fits/20020416', /init

end
