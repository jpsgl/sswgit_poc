pro sxi_disp_lm_dump, outfil=outfil, hc=hc, date=date, qstop=qstop, $
	lastn=lastn, ist=ist, ien=ien, listall=listall
;
;HISTORY:
;	Written Mar-2003 by B.Gantner
;

;this is 0x012A 0000 in dec form
limit_mon_virt_address=19529728

empty_lm=BYTE(0)
for i=1,23 do empty_lm=[empty_lm,BYTE(0)]
empty_lm[22]=BYTE(32)		;a 0x20 in the state

if (keyword_set(listall)) then empty_lm[23]=BYTE(32) ;emptyies will no longer match

if (n_elements(date) eq 0) then date = ut_time()  ;returns format '12-mar-03'
file_date = time2file(date, /date)		;returns format '20030324'
infil = '$GSE_PKT_FILE_DIR/0x0031/' + file_date + '.0x0031'

;
head = sag_rd_hk(infil, 0, 100000, /map)
;

;
;if (n_elements(recs) eq 0) then begin
    recs = sag_rd_hk(infil, 0, n_elements(head)-1)
    head = head(0:n_elements(head)-1)
;endif 
;
out = ['SXI_DISP_LM_DUMP  Ver 1.1  Program run: ' + !stime, ' ']
;
;search for the start of a limit mon dump
starts=0
extra_pkts=0
for i=0,n_elements(recs)-1 do begin
  if ( recs[i].addr eq limit_mon_virt_address) then begin 
    starts=[starts,i]
    extra_pkts=[extra_pkts,0]
    cnt=1
    x=n_elements(recs)-1
    for j=i,x do begin
      jtemp=(j+1)<(n_elements(recs)-1)
      if (recs(jtemp).addr eq (limit_mon_virt_address+(cnt*180))) then $
        extra_pkts[n_elements(starts)-1]=cnt $
      else x=0
      cnt=cnt+1
    endfor
  endif
endfor

starts=starts[1:n_elements(starts)-1]		;cut off the leading zero
extra_pkts=extra_pkts[1:n_elements(extra_pkts)-1]

if (n_elements(ist) eq 0) then ist = 0
if (n_elements(ien) eq 0) then ien = n_elements(starts)-1
if (keyword_set(lastn)) then ist = n_elements(starts)-lastn

starts=starts[ist:ien]		
extra_pkts=extra_pkts[ist:ien]

if (n_elements(starts) gt 0) then begin

  for i=0, n_elements(starts)-1 do begin
    data_block=recs[starts[i]].data
    for j=1, extra_pkts[i] do data_block=[data_block,recs[starts[i]+j].data]
    
    out =[out,'*********************************',' ']
    
    dattim = sxi_time_conv('INSTRUMENT', struct=recs(starts(i)).pkt_head, /string, /msec)
    edattim = sxi_time_conv('EGSE', head(starts(i)).egse_time, /string, /msec)
    out = [out,'LOBT: ' + dattim  + '  Addr:   0x'+zformat(recs[starts[i]].addr,8) +$
      string(recs[starts[i]].addr, format='(i10)'), $
      'EGSE: ' + edattim + '  Scnt:  ' + string( mask(recs[starts[i]].pkt_head.scnt, 0, 14)),$
      'LM Block Cnt: '+string(i),  ' ']
	
      out=[out, '                   -----Word 1-----  -Word 2-  -Word 3-  -Word 4-  -Word 5-  -------Word 6--------']
      out=[out, 'LM #    Address    Mnem Mask TrgCnt   Lo Rng    Hi Rng     Cmd     Cmd List  NxtMon Cnt State Fill']
      out=[out, '----    --------   ---- ---- ------  --------  --------  --------  --------  ------ --- ----- ----']

    xhigh= ((n_elements(data_block)/24)-1)<31
    for x=0,xhigh do begin
      lm=data_block[(x*24):(x*24)+23]
      
      worthwhile=0
      for q=0,23 do if (lm[q] ne empty_lm[q]) then worthwhile=1
      if (worthwhile) then begin
        if (x lt 10) then xstring='  '+strcompress(string(x),/remove_all) $
          else xstring=' '+strcompress(string(x),/remove_all)
        
	out=[out, xstring+'   0x'+zformat(limit_mon_virt_address+(x*24),8)+'   '+zformat(lm[0],2)+zformat(lm[1],2)+$
         '  '+zformat(lm[2],2)+'  '+zformat(lm[3],2)+'      '+zformat(lm[4],2)+zformat(lm[5],2)+$
         zformat(lm[6],2)+zformat(lm[7],2)+'  '+zformat(lm[8],2)+zformat(lm[9],2)+zformat(lm[10],2)+zformat(lm[11],2)+$
         '  '+zformat(lm[12],2)+zformat(lm[13],2)+zformat(lm[14],2)+zformat(lm[15],2)+'  '+$
         zformat(lm[16],2)+zformat(lm[17],2)+zformat(lm[18],2)+zformat(lm[19],2)+'     '+zformat(lm[20],2)+'  '+$
         zformat(lm[21],2)+'  '+zformat(lm[22],2)+'    00']

	
        ;out=[out,'Limit Monitor :'+string(x),$
	;  'MNEM:     '+zformat(lm[0],2)+zformat(lm[1],2)+'		CMD:      '+$
	;  zformat(lm[12],2)+zformat(lm[13],2)+zformat(lm[14],2)+zformat(lm[15],2)]
	;out=[out,'MASK:     '+zformat(lm[2],2)+'		CMD LIST: '+$
	;  zformat(lm[16],2)+zformat(lm[17],2)+zformat(lm[18],2)+zformat(lm[19],2)]
	;out=[out,'TRIG CNT: '+zformat(lm[3],2)+'		NXT MON:  '+zformat(lm[20],2)]
	;out=[out,'LO RNG:   '+zformat(lm[4],2)+zformat(lm[5],2)+zformat(lm[6],2)+zformat(lm[7],2)+$
	;  '	CNT:      '+zformat(lm[21],2)]
	;out=[out,'HI RNG:   '+zformat(lm[8],2)+zformat(lm[9],2)+zformat(lm[10],2)+zformat(lm[11],2)+$
	;  '	STATE:    '+zformat(lm[22],2)]
	
	;out = [out, ' ']
      endif
      
    endfor
  endfor
  
endif



prstr, out, /nomore
if (keyword_set(outfil)) then prstr, out, file=outfil
if (keyword_set(hc)) then prstr, out, file=outfil, /hc, nodelete=keyword_set(outfil)
if (keyword_set(qstop)) then stop
end
