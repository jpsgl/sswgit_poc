;HISTORY:
;	Written Mar-00 by M.Morrison
;	 1-May-00 (MDM) - Modified to write log dir to /sxisw/dbase/tb_tv
;			- Modified to ask what duty cycle should be used
;
;
;
;-----------------------------------------------------------------
pro surv_ht_mon_eval, logfil, item, curval1, curval2, struct, new_state, cycle
;
;
;
;TODO - look at both values
;
new_state = 'OFF'
if (curval1 lt struct.dblo) then new_state = 'ON'
if (curval1 ge struct.dblo) and (curval1 le struct.dbhi) then new_state = 'DB'
;
print, item, curval1
;
if (new_state ne struct.state) then begin
    outstr = item + ' STATE ' + string(struct.state, format='(a3)') + ' to ' + $
				string(new_state, format='(a3)') + '  (' + $
				string(curval1, struct.dblo, struct.dbhi, struct.duty, cycle, $
						format='(3f7.1, f7.1, "% of ", f5.0, " sec")') + ')'
    print, outstr
    file_append, logfil, !stime + '  ' + outstr
    struct.last_trans = !stime
end
struct.state = new_state
;
end
;-----------------------------------------------------------------
pro surv_ht_mon_cmdgpib, tel_bits, ceb_bits, qred, last_cmd, qtel, qceb
;
;
;
barr = intarr(32)
if (qtel) then barr( tel_bits(qred) ) = 1
if (qceb) then barr( ceb_bits(qred) ) = 1
iarr = intarr(4)
for i=0,3 do begin
    tmp = 0L
    for j=0,7 do tmp = tmp + barr(i*8+j) * 2^j
    iarr(3-i) = tmp
end
addr = '1'
rep_len = '0'
cmd_str = addr + ' ' + rep_len + ' ' + 'o' + arr2str( string(iarr, format='(i3.3)') ) + 'X'
;print, cmd_str
if (cmd_str ne last_cmd) then begin
    print, !stime + ' ' + cmd_str + ' >>>>>>>>>>>>>>> Sending to power supplies'
    outfil = '$GSE_CFL_BASE_DIR/gpib_if.in'
    respfil= '$GSE_CFL_BASE_DIR/gpib_if.out'
    file_delete, respfil
    prstr, cmd_str, file=outfil
    qdone = 0
    cmdtimeout = systime(1) 
    timeout = systime(1) + 3
    while (not qdone) do begin
	if (file_exist(respfil)) then begin
	    ;TODO - read and check
	    ;print, systime(1) - cmdtimeout
	    qdone = 1
	end else begin
	    qdone = systime(1) ge timeout
	    if (qdone) then begin
		print, 'SURV_HT_MON: No response from command sent.  Resending and returning'
		prstr, cmd_str, file=outfil
	    end else begin
		wait, 0.5
	    end
	end
    end
end
last_cmd = cmd_str
end
;-----------------------------------------------------------------
pro surv_ht_control, tel, ceb
;
;
;
;logfil = '/disk4/usr2/people/sxitc/test_surv_ht.log
logfil = '/sxisw/dbase/tb_tv/test_surv_ht.log
;
if (keyword_set(qtest)) then begin
    tel.dblo = 17		;deg cel
    tel.dbhi = 20		;deg cel
    tel.duty = 60.		;percent
    ;
    ;
    ceb.dblo = 17		;deg cel
    ceb.dbhi = 20		;deg cel
    ceb.duty = 50.		;percent
end

mnem = ['SXI_GPIB_STMPFWDTEL_AP', 'SXI_GPIB_STMPFWDTEL_BP', $
	'SXI_GPIB_STMPCEB_AP', 'SXI_GPIB_STMPCEB_BP']
	;'SXI_GPIB_STMPCEB_PSTAT', 'SXI_GPIB_STMPCEB_RSTAT', $
	;'SXI_GPIB_STMPFWDTEL_PSTAT', 'SXI_GPIB_STMPFWDTEL_RSTAT']
;
cycle = 60		;60 sec cycles
qred = 0
tel_bits = [24, 16]	;prime, redundant
ceb_bits = [8, 0]	;prime, redundant
last_cmd = 'xxx'
;
qdone = 0
next_time = systime(1)
while (not qdone) do begin
    print, !stime + '  Waiting to perform status check'
    while (systime(1) lt next_time) do begin & junk = 0 & wait, 2 & end		;junk=0 to allow breaks
    next_time = systime(1) + cycle
    ;
    entim = ut_time()
    if (keyword_set(qtest2)) then entim = '3-apr 14:00'
    sttim = anytim2ints(entim, off=-5*60)
    ;
    info = sag_get_mnem(sttim, entim, mnem)
    if (data_type(info) eq 8) then begin
	n = n_elements(info.gpib.daytime)
	dt = int2secarr(entim, info.gpib.daytime(n-1))
	if (dt gt 60) then begin
	    print, 'WARNING: Stale GPIB telemetry.
	    file_append, logfil, !stime + ' WARNING: Stale GPIB telemetry'
	end
	;
	surv_ht_mon_eval, logfil, 'TEL', info.gpib.value(n-1, 0), info.gpib.value(n-1, 1), tel, tel_stat, cycle
	surv_ht_mon_eval, logfil, 'CEB', info.gpib.value(n-1, 2), info.gpib.value(n-1, 3), ceb, ceb_stat, cycle
	;
	if (tel_stat eq 'ON') then tel_dur = cycle else tel_dur = cycle * tel.duty/100.
	if (ceb_stat eq 'ON') then ceb_dur = cycle else ceb_dur = cycle * ceb.duty/100.
	;
	systime_ref = systime(1)
	qtel = (tel_stat ne 'OFF')
	qceb = (ceb_stat ne 'OFF')
	last_cmd = 'xx'		;always send a command once a minute?
	surv_ht_mon_cmdgpib, tel_bits, ceb_bits, qred, last_cmd, qtel, qceb
	while ((qtel+qceb) ne 0) and (systime(1) lt next_time) do begin
	    wait, 1
	    qtel = ((tel_stat eq 'DB') and (systime(1) lt (systime_ref+tel_dur))) or (tel_stat eq 'ON')
	    qceb = ((ceb_stat eq 'DB') and (systime(1) lt (systime_ref+ceb_dur))) or (ceb_stat eq 'ON')
	    surv_ht_mon_cmdgpib, tel_bits, ceb_bits, qred, last_cmd, qtel, qceb
	end
    end
end
;
end