;
;+
;NAME:
;	lfft_ltc
;PURPOSE:
;	To perform the LFFT light transfer curve analysis
;HISTORY:
;	Written 20-Nov-99 by M.Morrison
;	22-Nov-99 (MDM) - Changed to not use the first frame
;	11-Mar-00 (MDM) - Corrected titles (said FLAT, should be LTC)
;			- Added calls to DISP12
;	 7-Apr-00 (MDM) - added checking SAG_LTC_FMAX
;	 5-Jun-01 (MDM) - Corrected analysis of image 2&4 (was doing 2&3)
;			- Made high gain fit up to 3700 DN
;	22-Mar-02 (MDM) - Added qstop option
;
set_plot,'x
;
qpause = keyword_set(qpause)
;
if (n_elements(date) eq 0) then date = ut_time()
if (n_elements(fmax) eq 0) then begin
    fmax = 4000
    if (keyword_set(getenv('SAG_LTC_FMAX'))) then fmax = float(getenv('SAG_LTC_FMAX'))
end
;
ff = sxi_stol2files(date, 'lfft_ltc', 12, out=cmdout)
if (strpos(cmdout.str, 'lfft_ltc2') ne -1) then fmax = 3700
;
if (ff(0) eq '') then begin
    print, 'Cannot find image files.  Stopping...'
    stop
end
;
img1 = rfits(ff(0))
sel_struct = {nx_block: 120, ny_block: 2, $
		x0: 10, x1: data_chk(img1, /xsize)-10, $
		y0: 10, y1: data_chk(img1, /ysize)-10, $
		b0: 3, b1: 6, $
		offset: 'NONE'}
logfil = '/sxisw/dbase/lfft/ltc.tab'
outdir = '$SXI_LFFT_DIR/tmp'
fid = str_replace(time2file(cmdout), '.', '_')
;
if (keyword_set(qstop)) then stop
;
npe = 2
nexp = 1
info = {tit: 'SXI run of LFFT_LTC ' + fmt_tim(cmdout) + ' South Amp, Image 2&3'}
sag_ltc, ff(2:3), nexp, npe, fmax, sel_struct, out=out, vmask=4, info=info, $
	logfil=logfil, $
	hc=concat_dir(outdir, 'ltc_a_'+fid+'.ps'), $
	gif=concat_dir(outdir, 'ltc_a_'+fid+'.gif')
if (qpause) then pause
info = {tit: 'SXI run of LFFT_LTC ' + fmt_tim(cmdout) + ' South Amp, Image 2&4'}
;sag_ltc, ff(2:4), nexp, npe, fmax, sel_struct, out=out, vmask=4, info=info, $
sag_ltc, [ff(2),ff(4)], nexp, npe, fmax, sel_struct, out=out, vmask=4, info=info, $
	logfil=logfil, $
	hc=concat_dir(outdir, 'ltc_b_'+fid+'.ps'), $
	gif=concat_dir(outdir, 'ltc_b_'+fid+'.gif')
if (qpause) then pause
info = {tit: 'SXI run of LFFT_LTC ' + fmt_tim(cmdout) + ' South Amp, Image 3&4'}
sag_ltc, ff(3:4), nexp, npe, fmax, sel_struct, out=out, vmask=4, info=info, $
	logfil=logfil, $
	hc=concat_dir(outdir, 'ltc_c_'+fid+'.ps'), $
	gif=concat_dir(outdir, 'ltc_c_'+fid+'.gif')
if (qpause) then pause
disp12, ff(0:5), 'SXI run of LFFT_LTC ' + fmt_tim(cmdout) + ' South Amp',  option = 'STDEV', $
	hc=concat_dir(outdir, 'ltc_a_'+fid+'disp12.ps'), $
	gif=concat_dir(outdir, 'ltc_a_'+fid+'disp12.gif')

;-------------------------------------------

info = {tit: 'SXI run of LFFT_LTC ' + fmt_tim(cmdout) + ' North Amp, Image 2&3'}
sag_ltc, ff(8:9), nexp, npe, fmax, sel_struct, out=out, vmask=4, info=info, $
	logfil=logfil, $
	hc=concat_dir(outdir, 'ltc_d_'+fid+'.ps'), $
	gif=concat_dir(outdir, 'ltc_d_'+fid+'.gif')
if (qpause) then pause
info = {tit: 'SXI run of LFFT_LTC ' + fmt_tim(cmdout) + ' North Amp, Image 2&4'}
;sag_ltc, ff(8:10), nexp, npe, fmax, sel_struct, out=out, vmask=4, info=info, $
sag_ltc, [ff(8),ff(10)], nexp, npe, fmax, sel_struct, out=out, vmask=4, info=info, $
	logfil=logfil, $
	hc=concat_dir(outdir, 'ltc_e_'+fid+'.ps'), $
	gif=concat_dir(outdir, 'ltc_e_'+fid+'.gif')
if (qpause) then pause
info = {tit: 'SXI run of LFFT_LTC ' + fmt_tim(cmdout) + ' North Amp, Image 3&4'}
sag_ltc, ff(9:10), nexp, npe, fmax, sel_struct, out=out, vmask=4, info=info, $
	logfil=logfil, $
	hc=concat_dir(outdir, 'ltc_f_'+fid+'.ps'), $
	gif=concat_dir(outdir, 'ltc_f_'+fid+'.gif')
disp12, ff(6:11), 'SXI run of LFFT_LTC ' + fmt_tim(cmdout) + ' North Amp',  option = 'STDEV', $
	hc=concat_dir(outdir, 'ltc_b_'+fid+'disp12.ps'), $
	gif=concat_dir(outdir, 'ltc_b_'+fid+'disp12.gif')

end