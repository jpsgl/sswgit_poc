pro sxi_dump_cam_head, ff, out, outfil=outfil, head_info=head_info
;
;
;
n = n_elements(ff)
break_file, ff, dsk_log, dir, file, ext
;
out = strarr(n)
for i=0,n-1 do begin
    bhead = mrdfits( ff(i), 1)
    lobt = sag_ext_mnem(bhead.hk, 'SXI_LOBT_IMG')
    itime = anytim2ints(getenv('SAG_INSTRUMENT_EPOCH'), off=double(lobt))
    out(i) = file(i) + $
        string(bhead.cam_head and 'ffff'x, format='(16(1x,z4.4))')
    if (keyword_set(head_info)) then begin
	out(i) = out(i) + $
	        string( sag_ext_mnem(bhead.hk, 'SXI_FRAMECOUNTER'), $
        	        bhead.fdb, $
                	sag_ext_mnem(bhead.hk, 'SXI_SHUT_EXP_DUR'), $
               		sag_ext_mnem(bhead.hk, 'SXI_F1ENC'), $
                	sag_ext_mnem(bhead.hk, 'SXI_F2ENC'), $
                	fmt_tim(itime), $
                	format='(1x, i8, 2x, z8.8, 2x, i8, 1x, z2.2,",",z2.2,2x, a)')
    end
    ;
    print, out(i)
    if (keyword_set(qdisp)) then begin
        img=rfits(ff(i))
        img_summary, img, ff(i), margin=4
        if (keyword_set(qpause)) then pause
    end
end
;
prstr, out, /nomore
if (keyword_set(outfil)) then prstr, out, file=outfil
end