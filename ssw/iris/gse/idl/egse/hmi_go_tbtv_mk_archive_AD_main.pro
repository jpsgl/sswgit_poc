;+
;$Id: hmi_go_tbtv_mk_archive_AD_main.pro 4942 2012-01-13 03:52:02Z sabolish $
;$Name:  $
; main: hmi_go_tbtv_mk_archive_AD_main.pro
;
; Purpose: Run tbtv_mk_archive to generate near Real-Time tables of temperatures
;	and thermal rates in the format of Archive Data runs.
;
; History: 
;	25-Sep-2007, G.Linford, created
;
; $Log: hmi_go_tbtv_mk_archive_AD_main.pro,v $
; Revision 1.2  2008/07/29 17:17:40  linford
; update path to TNCHMI14 for SC TB/TV
;
; Revision 1.1  2007/09/29 23:56:28  linford
; Add cvs tags for Id,Name and Log
;
;
;-

; -- Setup directories for Special RT-AD run:
set_logenv,'SDO_TBTV_DB_DIR','/disk1/egsesw/dbase/tb_tv/hmi/TNCHMI14'
set_logenv,'SDO_TBTV_HTML_DIR','/disk2/html/hmi/TandC/DB/TNCHMI14'
set_logenv,'SDO_REMOVE_HTML_DIRS','/disk2/html/hmi'     ; all links relative to /TandC/....
set_logenv,'GSE_PKT_FILE_DIR','/net/hmifm1a/disk2/log/packets'
set_logenv,'GSE_BAD_PKT_FILE_DIR','/net/hmifm1a/disk2/log/packets'
set_logenv,'GSE_HMI_SSIM_TM_PKT_FILE_DIR','/net/hmifm1a/disk2/log/packets'

; Call with special "testAD" switch to generate running 10 window
tbtv_mk_archive,/testAD,/instr_only

end
