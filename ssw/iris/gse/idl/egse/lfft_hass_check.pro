pro lfft_hass_check_s1, info
;
;
;
dattim = info.hk.daytime
y0    = info.hk.value(*,0)
z0    = info.hk.value(*,1)
ystat = info.hk.value(*,2)
zstat = info.hk.value(*,3)
y = (y0 - '800'x) *5/3. / 60.
z = (z0 - '800'x) *5/3. / 60.
;
ystr = 'STATUS OK'
if (min(ystat) ne 4) or (max(ystat) ne 4) then ystr = 'STATUS ERROR.  Minimum='+strtrim(min(ystat),2) + $
							'  Maximum='+strtrim(max(ystat),2)
zstr = 'STATUS OK'
if (min(zstat) ne 0) or (max(zstat) ne 0) then zstr = 'STATUS ERROR.  Minimum='+strtrim(min(zstat),2) + $
							'  Maximum='+strtrim(max(zstat),2)

;
!p.multi = [0,1,2]
!p.charsize = 1.5
utplot, dattim, y, ytit='Arcminutes', tit='SXI_HASSERRORY'	& plottimes, .05, .9, ystr
utplot, dattim, z, ytit='Arcminutes', tit='SXI_HASSERRORZ'	& plottimes, .05, .9, zstr
plottime, 0, 0, 'LFFT_HASS_CHECK  Ver 1.0 
plottime

end
;-----------------------------------------------------------------------------
;sttim = '20-Nov 4:30'	& entim = '20-Nov 8:00'
;sttim = '21-Nov 1:00'	& entim = '21-Nov 2:00'
sea_str = 'hass_check'
out = sxi_hist_sea(date, 'perform hass_check')
if (data_type(out) ne 8) then begin
    print, 'Cannot find any runs of ' + strupcase(sea_str) + ' on ' + gt_day(date, /str)
    stop
end
print, 'Found the following times for runs:'
prstr, fmt_tim(out) + '  ' + out.str
;
if (n_elements(out) gt 1) then begin
    imenu = xmenu_sel( out.str, /one)
    if (imenu eq -1) then stop
    out = out(imenu)
end
sttim = fmt_tim(out)
entim = fmt_tim(anytim2ints(sttim, off=5*60.))
;
mnem = ['SXI_HASSERRORY','SXI_HASSERRORZ', 'SXI_HASSERRORY_LOW4', 'SXI_HASSERRORZ_LOW4']
info = sag_get_mnem(sttim, entim, mnem)
if (data_type(info) ne 8) then begin
    print, 'HK file read problem.  Stopping....
    stop
end
;
lfft_hass_check_s1, info
;
;outdir = '$SXI_LFFT_DIR/tmp'
outdir = getenv('SXI_LFFT_DIR') + '/tmp'
fid = str_replace(time2file(out), '.', '_')
;
;--- Hardcopy & PS archive
;
psfil = concat_dir(outdir, 'hass_check_' + time2file(out) + '.ps')
setps,/land
device, file=psfil
lfft_hass_check_s1, info
device, /close
sprint, psfil
;
;--- GIF
;
set_plot,'Z'
lfft_hass_check_s1, info
zbuff2file, concat_dir(outdir, 'hass_check_' + time2file(out) + '.gif')
set_plot, 'x'
;
end