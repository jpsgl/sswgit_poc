function sxi_time_conv, epoch, sec, fsec, string=string, $
	qprint=qprint, msec=msec, $
	struct=struct
;+
;NAME:
;	sxi_time_conv
;PURPOSE:
;	Routine to convert seconds past epoch to date/time
;HISTORY:
;	Written 10-Nov-99 by M.Morrison
;	16-Nov-99 (MDM) - Changed fractional work to double precision
;			- Added struct input
;
case strupcase(epoch) of
    'EGSE': 		reftim = getenv('SAG_EGSE_EPOCH')
    'INSTRUMENT': 	reftim = getenv('SAG_INSTRUMENT_EPOCH')
    else: 		reftim = epoch
endcase
;
if (keyword_set(struct)) then begin
    sec = struct.time
    fsec = struct.ftime
end
;
v = sec
if (n_elements(fsec) eq 0) then fsec = 0.
if (data_type(fsec) lt 4) then fsec = unsign(fsec,16) / 2D^16
v = double(sec) + fsec
out = anytim2ints(reftim, off=v)
;
if (keyword_set(string)) then out = fmt_tim(out, msec=msec)
if (keyword_set(qprint)) then prstr, fmt_tim(out, msec=msec)
;
return, out
end