PRO day_plot_pkt_indicators2, batch, type, indicators, mypath, oneSig=oneSig, DAY=day, yrange=yrange
;+
;$Id: day_plot_pkt_indicators2.pro 4942 2012-01-13 03:52:02Z sabolish $
;$Name:  $
;Name: day_plot_pkt_indicators2   (for SDO RT-Archive-Trending Plots)
;
; Purpose: Plot a single day of selected packet indicators to check database access, HK
;			data for instrument health, and HK trending. 
;			Note, the default plot day is YESTERDAY, UT.  
;			If you want today or any specific day use the DAY keyword.
;
;	where:
;	oneSig is a switch to get rid of 1 sigma outliers
;	DAY    is a keyword to plot a specific day otherwise, plots YESTERDAY's data, UTC.
;	mypath is the path to the output plot png files.
;	indicators is a list of telemetry points for the pkt indicator plot.
;	type   is a string to indicate the packet name 0x05, 0x25, etc
;	batch  is a flag to indicate if the run is batch only (default).
;		batch = 'no' - means display plot via 'X'
;		batch = 'yes' or blank means do not use an 'X' display
;	yrange  is a SWITCH, when yrange[0]=1, compute non-zero range to plot otherwise,
;			pass the yrange keyword to sag_plot_mnem
; 
;   Keyword DAY:
;       If present will plot the specified input day. Note format of input day
;       must be 'dd-MMM-YYYY'.  Example: '1-Dec-2006' or '12-Jan-2007'
;		No, time is necessary.  Assumes, input date is UT.
;
; Example Calls:
; Plot yesterday's data to an X-screen:
; IDL> day_plot_pkt_indicators2,'no','SC_T_CEB_', indicators,'/home/sdotc/aia_html/tmp' 
;
; Plot Today's data to an X-screen:
; IDL> day_plot_pkt_indicators2,'no','SC_T_CEB_', indicators,'/home/sdotc/aia_html/tmp',DAY='10-Jan-2007'
;
; Plot yesterday's data to a PNG file, from a "batch" job:
; day_plot_pkt_indicators2,'yes','SC_T_CEB_', indicators,'/home/sdotc/aia_html/tmp'
;
;
; History
; Dec-14-2006, G.Linford, modified to plot only yesterdays data
; 10-Jan-2007, G.Linford, modified to plot correct number of subplots/page, nitems.
;			Also added keyword for specific day plots.
; 23-Jan-2007, G.Linford, modified z-buffer to a larger size 850x960 from
;			default size of 640x480.
;
;$Log: day_plot_pkt_indicators2.pro,v $
;Revision 1.4  2008/04/14 23:00:20  linford
;add logic for compute of best yrange for FSN
;
;Revision 1.3  2008/04/14 22:37:19  linford
;correct current ut_time and added yrange switch
;
;Revision 1.2  2007/09/30 00:00:57  linford
;fix idl ending mark for header, cvs tags
;
;Revision 1.1  2007/09/28 16:50:47  linford
;Add cvs tags Id,Name, Log
;
;-

if Keyword_set(DAY) then begin
	startT = day + ' 00:00'		; define start time
	endT = day + ' 23:59'		; define end time
endif else begin

	cur_utc = ut_time()			; the correct way to get UT no matter where you are!
	scur_date = anytim(cur_utc,/ints)

	yesterday = scur_date
	yesterday.day -= 1			; 1-days ago

	startT = yesterday
	startT.time = 0L			; set to beginning of day

	endT = yesterday
	endT.time = 86399000L			; set to end of the day e.g. 23:59:59
endelse

print,anytim(startT,/yohkoh)
print,anytim(endT,/yohkoh)

; Compute Date of 7 days ago
;today = '24-jul-2006 00:00'
;NdaysAgo = '17-jul-2006 00:00'
if Keyword_set(yrange) then begin
	if ( yrange[0] eq 1 ) then begin	; say for ploting FSN
		dat = sag_get_mnem(anytim(startT,/yohkoh),anytim(endT,/yohkoh),indicators)
		ii = where(dat.(0).value gt 0)
		ymax = max(dat.(0).value)
		ymin = min(dat.(0).value[ii])
		yrange = [ymin,ymax]
	endif
endif

; List of mnemonics
;indicators =['HMI_VER_NUM_SEQ_STATUS','HMI_WT1_ENCODER','HMI_SEQ_FILTERGRAM_SN']

nitems = n_elements(indicators)		; no of plot items per page
; Create standard plot?:
if ( N_PARAMS(0) NE 0 ) then begin
	if ( (batch EQ 'NO') OR (batch EQ 'no') ) then begin
		!p.charsize=2.0                 ; use a large font size then the default
		if (Keyword_set(oneSig)) then begin
			sag_plot_mnem,anytim(startT,/yohkoh),anytim(endT,/yohkoh),indicators, pmulti=nitems, psym=10, $
				/filter1d, yrange=yrange
		endif else begin
			sag_plot_mnem,anytim(startT,/yohkoh),anytim(endT,/yohkoh),indicators, pmulti=nitems, psym=10, yrange=yrange
		endelse
		
	endif else begin
		; Create the PNG file:
		set_plot,'Z'
		; !p.font=1.0                             ; use true-type fonts
		device,set_resolution=[850,960]	; increase size of plot area
		device,set_font='Helvetica', /TT_FONT
		if (Keyword_set(oneSig)) then begin
			sag_plot_mnem,anytim(startT,/yohkoh),anytim(endT,/yohkoh),indicators, pmulti=nitems, psym=10, $
				/filter1d, yrange=yrange
		endif else begin
			sag_plot_mnem,anytim(startT,/yohkoh),anytim(endT,/yohkoh),indicators, pmulti=nitems, psym=10, yrange=yrange
		endelse
		b = TVRD()			; read from buffer
		device, /close

		idat = anytim(startT,/ext)	; time in external format
		idate =string(format='(i4,i2.2,i2.2,a1,i2.2,i2.2)',idat[6],idat[5],idat[4],'_',idat[0],idat[1]);	yyyymmdd_hhmm
		pngFile = type + idate + '.png'	; file name
		;pngFile = 'pkf-indicators.png'	; file name

		if ( N_PARAMS(0) GT 1 ) then begin
			filename = mypath + '/' + pngFile
		endif else begin
			filename = '/home/sdotc/hmi_html/tmp/' + pngFile
		endelse

		write_png, filename, b

		set_plot,'x'
	
	endelse
endif



end
