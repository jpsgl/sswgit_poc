pro pr_image_vals, img, xst,yst, nx, ny, header, outfil=outfil, $
	nxpp=nxpp, nypp=nypp, $
	format=format, hex=hex
;
;
;
if (n_elements(nxpp) eq 0) then nxpp = 20
if (n_elements(nypp) eq 0) then nypp = 50
if (n_elements(format) eq 0) then begin
    format = '(i5)'
    if (keyword_set(hex)) then format='(1x,z4.4)'
end
;
tmp = string(0, format=format)
plen = strlen(tmp)
;
nximg = n_elements(img(*,0))
nyimg = n_elements(img(0,*))
;
if (n_elements(xst) eq 0) then xst = 0
if (n_elements(yst) eq 0) then yst = 0
if (n_elements(nx) eq 0) then nx = nximg
if (n_elements(ny) eq 0) then ny = nyimg
;
nxpage = ceil(nx / float(nxpp))
nypage = ceil(ny / float(nypp))
;
tit = ['PR_IMAGE_VALS  Program run: ' + !stime + ' on ' + get_host()]
if (keyword_set(header)) then tit = [tit, header]
tit = [tit, ' ']
;
out = ''
for iypage = 0,nypage-1 do begin
    for ixpage = 0,nxpage-1 do begin
	ist = (ixpage*nxpp +xst) < (nximg-1)
	ien = (ist+nxpp-1)  < (nximg-1)
	jst = (iypage*nypp +yst) < (nyimg-1)
	jen = (jst+nypp-1) < (nyimg-1)
	n1 = ien-ist+1
	n2 = jen-jst+1
	tmp = img(ist:ien, jst:jen)
	fmt = '(' + strtrim(n1,2) + format + ')'
	stmp = string(tmp, format=fmt)
	;
	tmp = lindgen(n1) + ist
	fmt = '(' + strtrim(n1,2) + 'i' + strtrim(plen,2) + ')'
	stmp1 = string(tmp, format=fmt)
	;
	;
	tmp = lindgen(n2) + jst
	fmt = '(i5)'
	stmp2 = string(tmp, format=fmt) + '   '
	;
	out = [out, tit, '        ' + stmp1, stmp2+stmp]
	if (ixpage ne nxpage-1) and (iypage ne nypage-1) then begin
	    out = [out, string(12b)]
	end
    end
end
;
if (keyword_set(outfil)) then prstr, out, file=outfil $
			else prstr, out
end
