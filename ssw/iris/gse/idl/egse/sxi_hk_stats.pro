function sxi_hk_stats_cnt1, array, count=count, cmd_check=cmd_check, zero=zero
;
;
if (n_elements(array) eq 1) then return, 0
;
dx = deriv_arr(array)
case 1 of
    (keyword_set(count)): nss = total(dx>0)
    (keyword_set(zero)): ss = where((array eq 0) and (array(1:*) ne 0), nss)
    else:		 ss = where(dx ne 0, nss)
endcase
out = nss

if (keyword_set(cmd_check)) then begin
    dx2 = deriv_arr(cmd_check)
    ss2 = where((dx2 gt 20) and (dx eq 0), nss2)
    out = out + nss2*4		;emi command list
end
;
if (nss gt 100000) and (keyword_set(count)) then junk = where(dx ne 0, out)	;problem telemetry
return, out
end
;---------------------------------------------------------------
;
;
;V4.0	27-Feb-01 (MDM) - Moved to FM1
;			- Corrected mis-label of shutter moves (was labeled "SH/FW1/FW2" when really FW1/FW2/SH
;	??-Oct-01 (MDM) - Changed to use odometer
;V5.0	29-Oct-01 (MDM) - Changed to use odometer or delta encoder, whichever is larger
;V6.0	27-Feb-02 (MDM) - Moved to FM2
;V6.1	14-Jan-03 (MDM)	- Moved back to FM1 & run on diapason
;
;outfil = '/sxi53/log/hk_stats/summary.txt'
;outfil = '/sxi72/log/hk_stats/summary.txt'
outfil = '/sxi02/sxifm1a/log/hk_stats/summary.txt'
;img_bdir = ['/sxi35/fits', '/sxi45/fits']
;img_bdir = ['/sxi45/fits_fm1']
;img_bdir = ['/sxi63/fits', '/sxi62/fits']
;img_bdir = ['/sxi83/fits']
img_bdir = ['/sxi02/sxifm1a/fits']
;
if (n_elements(sttim) eq 0) then begin
    mat = rd_tfile(outfil, 7, nocomment='#')
    last_fid = max(mat(0,*))
    tmp = file2time(last_fid+'.0000', out='int')
    sttim = gt_day( tmp.day+1, /str)
end
entim = gt_day(!stime, /str)
;
file_append, outfil, '#
file_append, outfil, '# Program run: ' + !stime
file_append, outfil, '# Program Version:  6.0'
file_append, outfil, '#
;
for iday=gt_day(sttim),gt_day(entim) do begin
    sttim2 = gt_day(iday, /str)
    entim2 = gt_day(iday+1, /str)
    fid = time2file(sttim2, /date_only)
    ;
    ;mnem = ['SXI_FRAMECOUNTER', 'SXI_SHENC', 'SXI_F1ENC', 'SXI_F2ENC', 'SXI_CHTCCOUNT', 'SXI_ISPCOUNT']
    mnem = ['SXI_FRAMECOUNTER', 'SXI_SHUT_ODOM', 'SXI_FW1_ODOM', 'SXI_FW2_ODOM', 'SXI_CHTCCOUNT', 'SXI_ISPCOUNT', $
		'SXI_LED_ODOM', 'SXI_LED_SECONDS_ON', 'SXI_SHENC', 'SXI_F1ENC', 'SXI_F2ENC']
    info = sag_get_mnem(sttim2, entim2, mnem)
    ;
    if (data_type(info) eq 8) then begin
	ff = file_list(concat_dir(img_bdir, fid), '*.fit')

	on_time = n_elements(info.hk.daytime)*4./60./60		;hours
	nimg_fil = n_elements(ff)
	if (ff(0) eq '') then nimg_fil = 0
	cmd_count = info.hk.value(*,4)
	nimg_hk = sxi_hk_stats_cnt1( info.hk.value(*,0), /count )
	nsh00 	= sxi_hk_stats_cnt1( info.hk.value(*,8), cmd_check=cmd_count )
	nf100 	= sxi_hk_stats_cnt1( info.hk.value(*,9), cmd_check=cmd_count )
	nf200 	= sxi_hk_stats_cnt1( info.hk.value(*,10), cmd_check=cmd_count )
	nsh 	= sxi_hk_stats_cnt1( info.hk.value(*,1), /count )
	nf1 	= sxi_hk_stats_cnt1( info.hk.value(*,2), /count )
	nf2 	= sxi_hk_stats_cnt1( info.hk.value(*,3), /count )
	nsh = nsh > nsh00
	nf1 = nf1 > nf100
	nf2 = nf2 > nf100
	;;;if (nf2 gt 10000) then nf2 = nf1 + 10	;fudged 7-Nov-00
	nboot 	= sxi_hk_stats_cnt1( info.hk.value(*,5), /zero ) > 1
	nled 	= sxi_hk_stats_cnt1( info.hk.value(*,6), /count )
	nledsec	= sxi_hk_stats_cnt1( info.hk.value(*,7), /count )
	;
	out = string(fid, on_time, nimg_fil, nimg_hk, nsh, nf1, nf2, nboot, nled, nledsec, $
			format='(a, f6.2, 8i8)')
	print, out
	if (on_time gt 0.1) then file_append, outfil, out, /uniq
    end else begin
	print, fid, ' has no data'
    end
end


end