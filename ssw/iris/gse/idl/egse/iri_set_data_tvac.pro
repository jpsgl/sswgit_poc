;+
;	15-Oct-2012	DSS
;
;	Configure the environment for analysis of TVac data
;
;	Inputs: none
;	Outputs: non
;
;	.run iri_set_data_tvac
;-

spawn, 'dbupdate 7925 -f'
setenv, 'SAG_HK_MAP_FILE=/irissw/idl/iris/iri_hkmap_v006.tab'
print, 'SAG_HK_MAP_FILE='+getenv('SAG_HK_MAP_FILE')
iri_data_path,7
end
