;pro eat_plot_with_limits, sttim, entim, mnem, win=win
;+
;NAME:
;	eat_sc_pkt_plot
;PURPOSE:
;	Show that the plots can be made from MRS&S telemetry
;HISTORY:
;	Written 2-Jan-02 by M.Morrison
;-
;
if (n_elements(win) eq 0) then win = 2	;two hours back
;
if (n_elements(sttim) eq 0) then sttim = fmt_tim(anytim2ints(ut_time(), off=-60.*60*win))
if (n_elements(entim) eq 0) then entim = ut_time()
input, 'Starting time', sttim, sttim
input, 'Ending time', entim, entim
if (n_elements(mnem1) eq 0) then mnem1 = 'SXI_PCM1_FRAME_CTR'
if (n_elements(mnem2) eq 0) then mnem2 = 'SXI_MECH_MSG_CNT'
;
sag_plot_mnem, sttim, entim, mnem1, psym=-2
pause
sag_plot_mnem, sttim, entim, mnem2, psym=-2
pause

yesnox, 'Hardcopy?', ans, 'Yes'
if (ans) then begin
    sag_plot_mnem, sttim, entim, mnem1, /hc
    sag_plot_mnem, sttim, entim, mnem2, /hc
end

end