pro sat_show_img
;
;
;
if (n_elements(win) eq 0) then win = 2  ;last 2 hrs
if (n_elements(sttim) eq 0) then sttim = anytim2ints(ut_time(), off=-60.*60.*win)
if (n_elements(entim) eq 0) then entim = ut_time()
;
if (n_elements(ff) eq 0) then begin
    print, 'Looking for all images between ' + fmt_tim(sttim) + ' and ' + fmt_tim(entim)
    ff = sxi_sel_images(sttim, entim, /one)
end
;
infil = ff(0)
img = rfits(infil, h=h)
img_summary, img, infil, h
img_summary, img, infil, h, /hc
;
setps, /land
plot, img(0,*), ytit='Line Number', xtit='Column Number', tit=infil
pprint, /reset
;
end