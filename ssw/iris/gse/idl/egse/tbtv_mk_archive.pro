PRO tbtv_mk_archive, testRT=testRT, testAD=testAD, instr_only=instr_only, qstop=qstop
;$Id: tbtv_mk_archive.pro 6329 2012-04-12 21:45:16Z linford $
;-- Main rountine to create TB Archive page?
;	Input file is the "tbtv_times_*.tab" or table where the wildcard name
;	is input as "test". E.G. input "ATA2_TB" for "tbtv_times_ata2_tb.tab".
;
;	NOTE: USING $SDO_TBTV_DB_DIR instead of $SXI_TBTV_DB_DIR to understand
;	program flow and to "flush out" all important sub-procedures.
;	Also, using $SDO_TBTV_HTML_DIR instead of $SXI_TBTV_HTML_DIR for the 
;	same reason and to output to a test area.
;
;	Summary of Env Vars used and example values:
;	Directory for input files e.g. mnem_aliases.tab,rate_mnem1.tab,tbtv_times*
;	'SDO_TBTV_DB_DIR','/disk1/egsesw/dbase/tb_tv/hmi/TNCHMI9/RT'	
; 	Base Directory for output files e.g. html and gif files 
;	'SDO_TBTV_HTML_DIR','/disk2/html/hmi/TandC/DB/TNCHMI9'
;	Modification to Base Directory to allow correct auto generated links
;	'SDO_REMOVE_HTML_DIRS','/disk2/html/hmi'     ; all links relative to /TandC/...
;	Base Directory for all input packet data files
;	'GSE_PKT_FILE_DIR','/net/hmifm1a/disk2/log/packets'
;
;  Formal Parameters:
;	testRT		- run a Realtime rate calculation for HMI (duration 4 hrs) into caseRT
;	testAD		- run a Realtime rate calculation for HMI (duration 10 hrs) into caseAD
;	instr_only 	- only run rates for instrument data and do not process
;					Chamber GSE data for rate information.
;	qstop		- Flag to aid in debugging, which enables stop/.con
;
;$Log: tbtv_mk_archive.pro,v $
;Revision 1.11  2007/09/26 21:58:55  linford
;change backdur from 4 to 6hrs
;
;Revision 1.10  2007/09/25 18:30:08  linford
;changed test to testRT to avoid conflict with testAD
;
;Revision 1.9  2007/09/25 17:13:18  linford
;added switch option for testAD to do RT runs in AD format
;
;Revision 1.8  2007/08/31 19:02:59  linford
;Added qstop switch, instr_only switch to block running chamber data, test switch to specify RT processing, ensure input is always local time
;
;Revision 1.7  2007/08/30 00:15:29  linford
;correct input start and end times to always be local time
;
;Revision 1.6  2007/08/29 23:06:32  linford
;correct rt offset for start and end times
;
;Revision 1.5  2007/08/14 23:49:39  linford
;fix keyword set
;
;Revision 1.4  2007/08/14 21:47:47  linford
;change to procedure, add hook for RT tables
;
;Revision 1.3  2007/08/14 18:24:51  linford
;cvs update from kuna with mods
;
;

if (n_elements(qone) eq 0) then qone = 1
if (keyword_set(testRT) OR keyword_set(testAD) ) then begin
	test = 'IRIS_RT_TV'
end else begin
	if (n_elements(test) eq 0) then test = 'IRIS_TV'
	print, 'IRIS_TB   IRIS_TV'
	input, 'Enter Test Name: ', test, test 
end
print,'Input control var.test= ', test

if (keyword_set(testAD) ) then begin
	backdur = 10.0	;backup 10.0 hrs
end else begin
	backdur = 6.0	;backup 6.0 hrs
end

if (n_elements(item) eq 0) then item = 'case_bo'


; new IRIS case for RealTime Data:
if (test eq 'IRIS_RT_TV') then begin
	ss = 0
	if (keyword_set(testAD) ) then begin
		item_arr = ['caseAD']
	end else begin
		item_arr = ['caseRT']
	end
	entim_arr = [!stime]		; input in local time
	sttim_arr = [anytim(anytim2ints(entim_arr[0],off=-backdur*60*60),/yohkoh)]
	print, "Start Time (local): ",sttim_arr[0]
	print, "End Time (local): ", entim_arr[0]
end
; EDU-TV Archive Data
if (test eq 'IRIS_TV') then begin
	print,'IRIS_TV Archive Data Run:'
	mat = rd_ulin_col('$SDO_TBTV_DB_DIR/tbtv_times_iris_tv.tab', /nohead, nocomment='#')
    ss = xmenu_sel(mat(0,*), one=qone)
    if (ss(0) eq -1) then stop
    item_arr = mat(0,ss)
    sttim_arr = mat(1,ss)
    entim_arr = mat(2,ss)
end
; new SDO case for ATA2 TB from a file:
if (test eq 'ATA2_TB') then begin
    mat = rd_ulin_col('$SDO_TBTV_DB_DIR/tbtv_times_ata2_tb.tab', /nohead, nocomment='#')
    ss = xmenu_sel(mat(0,*), one=qone)
    if (ss(0) eq -1) then stop
    item_arr = mat(0,ss)
    sttim_arr = mat(1,ss)
    entim_arr = mat(2,ss)
end
; new SDO cases:
if (test eq 'ATA4_TB') then begin
    mat = rd_ulin_col('$SDO_TBTV_DB_DIR/tbtv_times_ata4_tb.tab', /nohead, nocomment='#')
    ss = xmenu_sel(mat(0,*), one=qone)
    if (ss(0) eq -1) then stop
    item_arr = mat(0,ss)
    sttim_arr = mat(1,ss)
    entim_arr = mat(2,ss)
end		
;
if (test eq 'FM2_TV') then begin
    mat = rd_ulin_col('$SXI_TBTV_DB_DIR/tbtv_times_fm2_tv.tab', /nohead, nocomment='#')
    ss = xmenu_sel(mat(0,*), one=qone)
    if (ss(0) eq -1) then stop
    item_arr = mat(0,ss)
    sttim_arr = mat(1,ss)
    entim_arr = mat(2,ss)
end
;
if (test eq 'FM1_PEB_TV') then begin
    mat = rd_ulin_col('$SXI_TBTV_DB_DIR/tbtv_times_fm1_peb_tv.tab', /nohead, nocomment='#')
    ss = xmenu_sel(mat(0,*), one=qone)
    if (ss(0) eq -1) then stop
    item_arr = mat(0,ss)
    sttim_arr = mat(1,ss)
    entim_arr = mat(2,ss)
end
;
if (test eq 'FM2_TB') then begin
    mat = rd_ulin_col('$SXI_TBTV_DB_DIR/tbtv_times_fm2_tb.tab', /nohead, nocomment='#')
    ss = xmenu_sel(mat(0,*), one=qone)
    if (ss(0) eq -1) then stop
    item_arr = mat(0,ss)
    sttim_arr = mat(1,ss)
    entim_arr = mat(2,ss)
end
;
if (test eq 'FM2_EBOX_TV') then begin
    mat = rd_ulin_col('$SXI_TBTV_DB_DIR/tbtv_times_fm2_ebox_tv.tab', /nohead, nocomment='#')
    ss = xmenu_sel(mat(0,*), one=qone)
    if (ss(0) eq -1) then stop
    item_arr = mat(0,ss)
    sttim_arr = mat(1,ss)
    entim_arr = mat(2,ss)
end
;
if (test eq 'FM1_TV') then begin
    mat = rd_ulin_col('/sxisw/dbase/tb_tv/tbtv_times_fm1_tv.tab', /nohead, nocomment='#')
    ss = xmenu_sel(mat(0,*), one=qone)
    if (ss(0) eq -1) then stop
    item_arr = mat(0,ss)
    sttim_arr = mat(1,ss)
    entim_arr = mat(2,ss)
    ;'case_bo':  begin & sttim = '17-Sep  9:50'	& entim = '18-Sep  6:00'	& end
    ;'case_cno': begin & sttim = '18-Sep 6:00'	& entim = '18-Sep 18:20' 	& end
    ;'case_c1':  begin & sttim = '18-Sep 18:20'	& entim = '19-Sep  9:45' 	& end
    ;'case_ctb': begin & sttim = '19-Sep  9:45'	& entim = '20-Sep  2:10' 	& end
    ;'case_ctb2':begin & sttim = '20-Sep  2:10'	& entim = '21-Sep  7:00' 	& end
    ;'case_h1':  begin & sttim = '21-Sep  7:00'	& entim = '22-Sep  1:30' 	& end
    ;'case_htb': begin & sttim = '24-Sep  9:30'	& entim = '25-Sep  7:00' 	& end
    ;'case_htb2':begin & sttim = '25-Sep  7:00'	& entim = '25-Sep 15:20' 	& end
    ;'case_c2':  begin & sttim = '25-Sep 15:20'	& entim = '26-Sep  9:40' 	& end
    ;'case_h2':  begin & sttim = '26-Sep  9:40'	& entim = '27-Sep  3:10' 	& end
    ;'case_c3':  begin & sttim = '27-Sep  3:10'	& entim = '27-Sep 22:30' 	& end
    ;'case_h3':  begin & sttim = '27-Sep 22:30'	& entim = '28-Sep 16:30' 	& end
    ;'case_h3tb':begin & sttim = '28-Sep 16:30'	& entim = '28-Sep 23:00' 	& end
end

if (test eq 'FM1_TB') then case item of
    'case01': begin & sttim = '5-Jul  10:00'	& entim = '9-Jul 9:00' 		& end
    'case02': begin & sttim = '9-Jul   9:00'	& entim = '10-Jul 2:10' 	& end
    'case03': begin & sttim = '10-Jul  2:10'	& entim = '10-Jul 23:30' 	& end
    'case04': begin & sttim = '10-Jul 23:30'	& entim = '11-Jul  9:50' 	& end
    'case05': begin & sttim = '11-Jul  9:50'	& entim = '12-Jul  7:20' 	& end
    'case06': begin & sttim = '12-Jul  7:20'	& entim = '12-Jul 17:30' 	& end
    'case07': begin & sttim = '12-Jul 17:30'	& entim = '13-Jul  7:30' 	& end
    'case08': begin & sttim = '13-Jul  7:36'	& entim = '13-Jul 23:20' 	& end
    'case11': begin & sttim = '13-Jul  23:20'	& entim = '14-Jul 16:20' 	& end
    'case13': begin & sttim = '14-Jul  16:20'	& entim = '15-Jul 18:00' 	& end
    'case09': begin & sttim = '15-Jul  18:00'	& entim = '16-Jul 2:05' 	& end
    'case10': begin & sttim = '16-Jul  2:05'	& entim = '16-Jul 11:00' 	& end
    'case12': begin & sttim = '16-Jul  11:00'	& entim = '16-Jul 18:00' 	& end
endcase

;----- EM instrument
if (test eq 'TB') then case item of
    'case01': begin & sttim = '24-Apr  9:45'	& entim = '26-Apr 10:45' 	& end
    'case02': begin & sttim = '26-Apr 10:45'	& entim = '27-Apr  8:20' 	& end
    'case03': begin & sttim = '27-Apr  8:20'	& entim = '27-Apr 21:00' 	& end
    'case04': begin & sttim = '27-Apr 21:00'	& entim = '28-Apr 15:30' 	& end
    'case05': begin & sttim = '28-Apr 15:45'	& entim = '29-Apr 10:00' 	& end
    'case05a':begin & sttim = '29-Apr 10:00'	& entim = '30-Apr 12:30' 	& end
    'case07': begin & sttim = '29-Apr 10:00'	& entim = '1-May 6:00'	 	& end
    'case08': begin & sttim = '1-May 6:00'	& entim = '1-May 15:30'	 	& end
    'case09': begin & sttim = '1-May 15:30'	& entim = '2-May 11:00'	 	& end
    'case06': begin & sttim = '2-May 11:00'	& entim = '3-May 10:30'	 	& end
    'case10': begin & sttim = '3-May 10:30'	& entim = '4-May 10:30'	 	& end
    else:
endcase
if (test eq 'TV') then case item of
    'case01': begin & sttim = '8-May  10:30'	& entim = '9-May 16:00' 	& end
    'case02a':begin & sttim = '9-May  16:00'	& entim = '9-May 23:20' 	& end
    'case03': begin & sttim = '9-May  23:20'	& entim = '10-May 10:50' 	& end
    'casec1': begin & sttim = '10-May  10:20'	& entim = '10-May 22:20' 	& end
    'caseh1': begin & sttim = '10-May  22:20'	& entim = '11-May 22:00' 	& end
    'casec2': begin & sttim = '11-May  22:00'	& entim = '12-May 22:20' 	& end
    'caseh2': begin & sttim = '12-May  22:20'	& entim = '13-May 23:00' 	& end
    'casec3': begin & sttim = '13-May  23:00'	& entim = '14-May 22:10' 	& end
    'caseh3': begin & sttim = '14-May  22:10'	& entim = '15-May 22:00' 	& end
    'casec4': begin & sttim = '15-May  22:20'	& entim = '16-May 23:30' 	& end
    'caseh4': begin & sttim = '16-May  22:00'	& entim = '17-May 22:00' 	& end
    'case05': begin & sttim = '17-May  22:00'	& entim = '18-May 18:30' 	& end
    else:
endcase

for i=0,n_elements(sttim_arr)-1 do begin
    !p.multi = 0
    clearplot
    ;
    ;if (item eq 'case08') then backdur = 3
    ;if (n_elements(sttim) eq 0) then sttim = '24-Apr 9:45'
    ;if (n_elements(entim) eq 0) then entim = '26-Apr 10:45'
    ;
    item  = item_arr(i)
    sttim = sttim_arr(i)
    entim = entim_arr(i)
    ;input, 'Enter start time (local time)', sttim, sttim
    ;input, 'Enter end time (local time)', entim, entim
    sttim2 = anytim2ints(entim, off=-60.*60*backdur)	;back up 4 hrs
    ;
    set_plot,'z'
    bdir0 = getenv('SDO_TBTV_HTML_DIR')
    bdir1 = concat_dir(bdir0, item)
    if (not file_exist(bdir1)) then spawn, ['mkdir', bdir1], /noshell
    ;
    
    tbtv_mk_rate_table, ut_time(sttim2), ut_time(entim), outdir=bdir1, $
		noplots=keyword_set(qtable_only), instr_only=instr_only, qstop=qstop
    if (not keyword_set(qtable_only)) then begin
    	print,"GIF_FULL start time: ", ut_time(sttim)
	print,"GIF_FULL end time, bdir1: ", ut_time(entim), bdir1
	tbtv_plots, ut_time(sttim), ut_time(entim), concat_dir(bdir1, 'GIF_FULL')
	
	tbtv_plots, ut_time(sttim2), ut_time(entim), concat_dir(bdir1, 'GIF')
    end
end
;
end
