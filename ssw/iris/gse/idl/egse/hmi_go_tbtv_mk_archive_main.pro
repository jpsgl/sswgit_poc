;+
; $Id: hmi_go_tbtv_mk_archive_main.pro 4942 2012-01-13 03:52:02Z sabolish $
; $Name:  $
; main: hmi_go_tbtv_mk_archive_main.pro
;
; Purpose: Run tbtv_mk_archive to generate near Real-Time tables of temperatures
;	and thermal rates.
;
; History: 
;	30-Aug_2007, G.Linford, created
;
; $Log: hmi_go_tbtv_mk_archive_main.pro,v $
; Revision 1.4  2008/07/23 21:05:28  linford
; update paths for SC TB/TV
;
; Revision 1.3  2008/04/12 17:56:04  linford
; update for SC EMI/EMC with new dir no13
;
; Revision 1.2  2007/09/30 00:00:57  linford
; fix idl ending mark for header, cvs tags
;
; Revision 1.1  2007/09/29 23:56:27  linford
; Add cvs tags for Id,Name and Log
;
;
;-

; -- Setup directories for RT run:
set_logenv,'SDO_TBTV_DB_DIR','/disk1/egsesw/dbase/tb_tv/hmi/TNCHMI14/RT'
set_logenv,'SDO_TBTV_HTML_DIR','/disk2/html/hmi/TandC/DB/TNCHMI14'
set_logenv,'SDO_REMOVE_HTML_DIRS','/disk2/html/hmi'     ; all links relative to /TandC/....
set_logenv,'GSE_PKT_FILE_DIR','/net/hmifm1a/disk2/log/packets'
set_logenv,'GSE_BAD_PKT_FILE_DIR','/net/hmifm1a/disk2/log/packets'
set_logenv,'GSE_HMI_SSIM_TM_PKT_FILE_DIR','/net/hmifm1a/disk2/log/packets'

tbtv_mk_archive,/testRT,/instr_only

end
