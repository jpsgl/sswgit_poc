if (n_elements(date) eq 0) then date = ut_time()
;infil = '/sxi35/log/packets/0x0031/19991125.0x0031'
;infil = '/sxi35/log/packets/0x0031/' + time2file(date, /date) + '.0x0031'
infil = '$GSE_PKT_FILE_DIR/0x0031/' + time2file(date, /date) + '.0x0031'
rec = sag_rd_hk(infil, 0, 10000)
map = sag_rd_hk(infil, /map)
idattim = sxi_time_conv('INSTRUMENT', struct=rec.pkt_head)
edattim = sxi_time_conv('EGSE', map.egse_time)
;
;ss = where(rec.addr eq 'c5c00'x, nss)
ss = where(rec.addr eq 'c5c90'x, nss)
if (nss eq 0) then stop
;
qok = bytarr(nss)
;ref = fix(rec(ss(0)).data)
ref = fix(rec(ss(nss-1)).data)
for i=0,nss-1 do qok(i) = total( rec(ss(i)).data - ref ) eq 0
;
clearplot
!p.multi = [0,1,2]
utplot, edattim, rec.addr, /ynoz, psym=2
utplot, edattim(ss), qok, /ynoz, psym=2
plottimes, .05, .9, 'PASS'
plottimes, .05, .1, 'FAIL'
;
plottime, 0, 0, 'EMI_MEM_DUMP_CHECK  Ver 1.0
plottime
end