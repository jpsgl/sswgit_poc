;
;NAME:
;	lfft_leaks_entrance
;HISTORY:
;	Written 20-Nov-99 by M.Morrison
;V1.01	22-Nov-99 (MDM) - Changed dark frame average to be 1500 to 2400 (from 1500 to 2000)
;V1.02	11-Mar-00 (MDM) - Added disp12 call
;V1.03	16-Apr-01 (MDM) - Fixed image size for 4x4 binning
;
if (keyword_set(qskip)) then goto, skip
set_plot,'x
;
outdir = '$SXI_LFFT_DIR/tmp'
if (n_elements(date) eq 0) then date = ut_time()
nimg = 9
infil = sxi_stol2files(date, 'leaks_entrance', nimg, out=cmdout)
;
if (infil(0) eq '') then begin
    print, 'Cannot find image files.  Stopping...'
    stop
end
if (n_elements(infil) ne nimg) then stop, 'Not enough images'

mreadfits, infil, head, img
for i=1,nimg-1 do img(*,*,i) = img(*,*,i) - img(*,*,0)
pr_stats, img, 3, img_info
;
skip:
break_file, infil, dsk_log, dir, filnam, ext
list = filnam+ext+'  ' + get_infox(head, 'EGSE_TIM, naxis1, naxis2')
;
xsiz = intarr(nimg)+132
ysiz = intarr(nimg)+147
amin = [ 100, fltarr(nimg) - 30]
amax = [2400, fltarr(nimg) + 30]
fw = ' ' + ['Drk', 'Drk', ' 12', '  2', '  4', '  6', '  8', ' 10', 'Drk']
stat = strarr(nimg) + '  '
for i=0,nimg-1 do begin
    if (head(i).naxis1 ne xsiz(i)) then stat(i) = stat(i) + 'X_ERR_SIZ '
    if (head(i).naxis2 ne ysiz(i)) then stat(i) = stat(i) + 'Y_ERR_SIZ '
    if (img_info(i).avg lt amin(i)) then stat(i) = stat(i) + 'IMG_AVG_TOO_LOW '
    if (img_info(i).avg gt amax(i)) then stat(i) = stat(i) + 'IMG_AVG_TOO_HI  '
end
ss = where(strmid(stat,2) eq '', nss)
if (nss ne 0) then stat(ss) = stat(ss) + 'OK'
;
imgstr = get_infox(img_info, 'min,max,avg,dev', format=str2arr('f6.0,f6.0,f7.1,f7.1'))
out = ['LFFT_LEAKS_ENTRANCE Ver 1.03  Program run: ' + !stime, $
	'On machine: ' + get_host(), $
	' ', $
	' File/Image             EGSE Time               NX        NY    Min     Max     Avg       Dev FW   Stat', $
	;9991121_020557.fit  21-NOV-99  02:05:57       528       588      0.   4095.   1794.0    276.6 Drk  OK
	' ', $
	list + '  ' + imgstr + fw + stat]

prstr, out, /nomore
if (keyword_set(qstop)) then stop
;
outfil = concat_dir(outdir, 'lfft_leaks_entrance_' + filnam(0) + '.txt')
prstr, out, fil=outfil
sprint, outfil, /land
;
disp12, infil, 'SXI run of LEAKS_ENTRANCE ' + fmt_tim(cmdout), option='STDEV', $
        hc=concat_dir(outdir, 'lfft_leaks_entrance_'+filnam(0)+'disp12.ps'), $
        gif=concat_dir(outdir, 'lfft_leaks_entrance_'+filnam(0)+'disp12.gif'), $
	dark_file=infil(0)

end
