pro eat_seq_demp_plot, head, mark_current=mark_current
;
;
;
dattim = anytim2ints(head.SEXPLOBT)
fmt_timer, dattim, t1, t2, /quiet
if (keyword_set(mark_current)) then t2 = ut_time()
yr = anytim2ints([t1, t2])
;
!p.multi = [0,1,2]
utplot, dattim, head.exposure, psym=2, ytit='Exposure (Seconds)', tit = 'Sequence of images', $
		xmargin=[15,3], ystyle=2, timerange=yr
if (keyword_set(mark_current)) then outplot, [yr(1),yr(1)], !y.crange, thick=3, psym=0, linestyle=2
;
v = fix( head.f1pos) + fix( head.f2pos )
uv = v(uniq(v, sort(v)))
n = n_elements(uv)
uv_lab = strarr(n)
newy = intarr(n_elements(v))
for i=0,n-1 do begin
    ss = where(v eq uv(i))
    uv_lab(i) = head(ss(0)).f1filter + ' / ' + head(ss(0)).f2filter
    newy(ss) = i
end
;
utplot, dattim, newy, ytickname=uv_lab, yticks=n_elements(uv_lab)-1, $
		psym=2, xmargin=[15,3], ystyle=2, timerange=yr
if (keyword_set(mark_current)) then outplot, [yr(1),yr(1)], !y.crange, thick=3, psym=0, linestyle=2

plottime, 0, 0, 'EAT_SEQ_DEMO  Ver 1.02'
plottime
pause
;
end
;-----------------------------------------------------------------------
;
;
;
;HISTORY:
;	Written 5-Mar-01 by M.Morrison
;V1.01	 6-Mar-01 (MDM) - Fixed Y axis label
;V1.02	 2-Jan-02 (MDM) - Added plotting with current time mark also shown
;-
;
if (n_elements(date) eq 0) then date = gt_day( ut_time(), /str)
infil = sxi_stol2files(date, 'eat_seq_demo', 9, out=cmdout)
;
if (infil(0) eq '') then begin
    print, 'Cannot find image files.  Stopping...'
    stop
end
;
mreadfits, infil, head
;
eat_seq_demp_plot, head
eat_seq_demp_plot, head, /mark_current
;
setps, /land
eat_seq_demp_plot, head
eat_seq_demp_plot, head, /mark_current
pprint, /reset
;
end