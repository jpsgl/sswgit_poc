function iri_disp_events_s1, apid, date=date, recs=recs, lastnrec=lastnrec
;
;
;
if (n_elements(apid) eq 0) then apid = '0x0040'
;
eepoch = getenv('SAG_EGSE_EPOCH')
pkt_dir= getenv('GSE_PKT_FILE_DIR')
    if (n_elements(date) eq 0) then date = ut_time()
    out0 = 'No events for APID: ' + apid + ' for date: ' + fmt_tim(date)
    fid = time2file( date, /date)
    infil = pkt_dir+'/' + apid + '/' + fid + '.' + apid
    head = sag_rd_hk(infil, 0, 10000, /map)

    if (data_type(head) ne 8) then begin
        print, 'No data in input file: ' + infil
        return, out0
    endif
    print, 'Reading: ' + infil
    n = n_elements(head)
    ist = 0
    if (keyword_set(lastnrec)) then ist = (n-lastnrec)>0
    recs = sag_rd_hk(infil, ist, n-1)
    head = sag_rd_hk(infil, ist, n-1, /map)
    head = head(ist:n-1)
    ;
;
if (data_type(recs) ne 8) then begin
    out0 = 'No events for APID: ' + apid + ' for date: ' + fmt_tim(date)
    return, out0
endif
;
edattim = iri_time_conv('EGSE', head.egse_time, head.egse_ftime)
dattim = iri_time_conv('INSTRUMENT', struct=recs.pkt_head, /string, /msec)
;out0 = dattim + fstring(recs.data, format='(12(1x,z2.2))')
;^^ doesn't work properly for some reason.
out0 = strarr(n_elements(dattim)+1)
;help, out0, /str

prtlen=114
if (apid eq '0x0040') then prtlen=115
for i=0,n_elements(out0)-2 do begin
	addstr=bytarr(prtlen-(strlen(string(recs(i).data))))+32B
	out0(i) = fmt_tim(edattim(i)) + '	' + string(recs(i).data)+string(addstr) + '	' + dattim(i)	;, format='(12(1x,z2.2))') + string(v1, v2, v3, format='(2a8, a12)') + ' ' + dattim(i)
	if ((apid eq '0x0320') or (apid eq '0x025E')) then begin
		addstr=bytarr(118-(strlen(string(recs(i).data(2:*)))))+32B
		out0(i) = fmt_tim(edattim(i)) + '	' + string(recs(i).data(2:*))+string(addstr)+ '	' + dattim(i)
	endif
endfor
out0=shift(out0, 1)
out0(0)='EGSE Time          	APID '+apid+'                                                                                                       	LOBT'
return, out0
end
;-------------------------------------------------------------
pro iri_disp_events, outfil=outfil, hc=hc, date=date,  $
	surom=surom, kernel=kernel, bm=bm, acs=acs,lastnrec=lastnrec
;
;HISTORY:
;V1.0	26-Mar-2012	DSS	Copied from sxi_disp_events.pro
;V2.0	26-Oct-2012	DSS	Added acs and bm apids and options
;
;surom=0x3f, kernel=0x40
apids = ['0x003F', '0x0040','0x025E', '0x0320']
if (keyword_set(surom)) then apids = '0x003F'	;only surom events
if (keyword_set(kernel)) then apids = '0x0040'	;only kernel
if (keyword_set(bm)) then apids = '0x0320'	;only bus manager
if (keyword_set(acs)) then apids = '0x025E'	;only acs
;
out = ['IRI_DISP_EVENTS  Ver 1.0  Program run: ' + !stime, ' ']
;
for i=0,n_elements(apids)-1 do begin
    out0 = iri_disp_events_s1(apids(i), date=date, lastnrec=lastnrec)
    out = [out, ' ', out0]
end
;
prstr, out, /nomore
if (keyword_set(outfil)) then prstr, out, file=outfil
if (keyword_set(hc)) then prstr, out, file=outfil, /hc, nodelete=keyword_set(outfil)
end
