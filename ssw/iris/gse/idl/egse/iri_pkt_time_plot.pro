pro iri_pkt_time_plot, fid, apid

; fid = packet file name (e.g. '20130315')
; apid = 0xxxx (e.g. '0x1113')

filnam=getenv('GSE_PKT_FILE_DIR')+'/'+apid+'/'+fid+'.'+apid

dd=sag_rd_hk(filnam,0,100000)

plot, dd.pkt_head.time

end
