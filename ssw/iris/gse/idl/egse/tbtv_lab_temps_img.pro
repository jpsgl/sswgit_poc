pro tbtv_lab_temps_img, time, info, mnem, $
	imgfil=imgfil, infil=infil, $
	zfile=zfile, format=format

if (n_elements(imgfil) eq 0) then imgfil = '/sxi01/dbase/tb_tv/telescope_tc_map.gif'
if (n_elements(infil) eq 0) then   infil = '/sxi01/dbase/tb_tv/telescope_tc_map.img_loc'
;
if (n_elements(format) eq 0) then format='(f8.1)'
if (n_elements(time) eq 0) then time = ut_time()
;
if (n_elements(mnem) eq 0) then begin
    tc_map = rd_ulin_col('$SXI_TBTV_DB_DIR/mnem_aliases.tab', /nohead, nocomm='#')
    mnem = strlowcase(tc_map(0,*))
    ss = where(strmid(mnem, 0, 3) eq 'sxi')
    mnem = mnem(ss)
end
;
if (n_elements(info) eq 0) then begin
    win=10*60.	;+/- 10 minutes
    sttim = anytim2ints(time, off=-win)
    entim = anytim2ints(time, off= win)
    
    info = sag_get_mnem(sttim, entim, mnem)
end
;
if (data_type(info) ne 8) then begin
    print, 'No data for mnemonics: ', mnem, ' for time ', fmt_tim(time)
    return
end
;
tags = tag_names(info)
for itag=0,n_elements(tags)-1 do begin
    dattim = info.(itag).daytime
    ss = tim2dset(dattim, time)
    if (itag eq 0) then dattim0 = fmt_tim(ut_time(dattim(ss), /to_local))
    v = info.(itag).value(ss, *)
    vout = string(v, format=format)
    nn = n_elements(info.(itag).mnem)
    if (itag eq 0) then begin
	in_mnem = info.(itag).mnem
	in_val = vout
    end else begin
	in_mnem = [in_mnem, info.(itag).mnem]
	in_val = [in_val, vout]
    end
end

in_time = 'Date/Time of 1st Mnemonic: ' + dattim0 + ' local'	
label_image_blk, in_mnem, in_val, in_time, $
	imgfil=imgfil, infil=infil, zfile=zfile
;
end