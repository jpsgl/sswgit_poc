;HISTORY:
;V1.00	Written 26-Mar-01 by M.Morrison
;
sea_str = 'lfft_hasserror'
out = sxi_hist_sea(date, 'perform ' + sea_str)
if (data_type(out) ne 8) then begin
    print, 'Cannot find any runs of ' + strupcase(sea_str) + ' on ' + gt_day(date, /str)
    stop
end
print, 'Found the following times for runs:'
prstr, fmt_tim(out) + '  ' + out.str
;
if (n_elements(out) gt 1) then begin
    imenu = xmenu_sel( out.str, /one)
    if (imenu eq -1) then stop
    out = out(imenu)
end
isttim = int2secarr(out, '1-jan-70')
;
    if (n_elements(date) eq 0) then date = ut_time()
    fid = time2file( date, /date)
    infil = '$GSE_PKT_FILE_DIR/0x0018/' + fid + '.0x0018'
    head = sag_rd_hk(infil, 0, 1000, /map)
    if (data_type(head) ne 8) then begin
	print, 'No data in input file: ' + infil
	stop
    end
    n = n_elements(head)
    dt = [0,deriv_arr(head.egse_time)]
    ss = where( (head.egse_time ge isttim) and (dt lt 4), nss)
    if (nss eq 0) then stop, 'Cant find HASS data'
    ist = min(ss)
    ien = max(ss)
    recs = sag_rd_hk(infil, ist, ien)
;
if (data_type(recs) ne 8) then return
;
outdir = '$SXI_LFFT_DIR/tmp'
arr = str2arr(out.str, delim=' ')
ss = where( strpos(arr, 'lfft_') ne -1)
filnam = str_replace(arr(ss(0)), 'lfft_', '') + '_' + time2file(out)
;
sxi_plot_hass, rec=recs
sxi_plot_hass, rec=recs,  hc=concat_dir(outdir, filnam + '.ps' )
sxi_plot_hass, rec=recs, gif=concat_dir(outdir, filnam + '.gif')
;
end
