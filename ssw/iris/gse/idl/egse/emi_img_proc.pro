pro emi_img_proc_s1, ff, ist, ien, outdir, outfil, qpause=qpause
;
;
;
common emi_img_proc_blk1, last_ist, last_img, last_dattim
;
if (n_elements(last_ist) eq 0) then last_ist = -1
;if (ist ne last_ist) then last_img = 0
if (n_elements(last_img) eq 0) then last_img = 0
if (n_elements(last_dattim) eq 0) then last_dattim = '1-Jan-70'
;
for ifil=ist,ien do begin
    break_file, ff(ifil), dsk_log, dir00, filnam, ext
    img = rfits(ff(ifil), h=h)
    ;dattim = anytim2ints(sxpar(h, 'PKT_STIM'))
    dattim = anytim2ints(sxpar(h, 'EGSE_TIM'))
    dattim2 = anytim2ints(sxpar(h, 'PKT_STIM'))

    dt = int2secarr(dattim, last_dattim)/60.	;minutes
    ;if (dt gt 1) then begin
    if (dt gt 20/60.) then begin
	if (file_exist(outfil)) then begin
	    tmp = rd_tfile(outfil)
	    if (n_elements(tmp) lt 10) then file_delete, outfil
	end
	outfil = concat_dir(outdir, time2file(dattim, /sec)+'.tab')
	if (keyword_set(qpause)) then pause
	file_delete, outfil
	print, 'New output file: ' + outfil
	last_img = 0
        tit = ['# Day    Time         Full Image Single     | Full Image Diff | 10:* Img Single |  10:* Img Diff    Image Date/Tim   File Date/Time', $
		'#                 Min Max    Avg      Dev      Avg      Dev      Avg      Dev      Avg      Dev']
	file_append, outfil, tit
    end

    imin = min(img, max=imax)
    iavg = total(img)/n_elements(img)
    idev = stdev(img)
    diff = img - last_img
    diff(1,1) = 0	;"hot" bad pixel?
    davg = total(diff)/n_elements(diff)
    ddev = stdev(diff)
    ;
    diff2 = diff(10:*, 10:*)
    davg2 = total(diff2)/n_elements(diff2)
    ddev2 = stdev(diff2)
    img2 = img(10:*, 10:*)
    iavg2 = total(img2)/n_elements(img2)
    idev2 = stdev(img2)
    ;
    out0 = string(dattim.day, dattim.time, $
	imin, imax, iavg, idev, $
	davg, ddev, $
	iavg2, idev2, $
	davg2, ddev2, $
	time2file(dattim2, /sec), filnam+ext, $
	format='(i6,i9, 2i5, 8f9.3, 2x, a, 1x, a)')
    ;
    print, out0
    ;
    file_append, outfil, out0
    last_img = temporary(img)
    last_dattim = dattim
end
;
last_ist = ist
end
;----------------------------------------------------------------
;HISTORY:
;	Written Dec-99 by M.Morrison
;	 3-Dec-99 (MDM) - Changed to use EGSE time rather than LOBT
;	 8-Dec-99 (MDM) - Grab the starting image to process from a file
;			  rather than from a parameter
;	13-Apr-01 (MDM) - Modified to use env var
;	26-Apr-02 (MDM) - Added qpause
;
outdir = getenv('SXI_EMI_DB_DIR')
last_file_name = concat_dir(outdir, 'last_fits.txt')

if (n_elements(fid) eq 0) then fid = time2file(ut_time(), /date)
if (n_elements(dir) eq 0) then dir = concat_dir('$SXI_FITS_DIR', fid)
if (n_elements(last_file) eq 0) then begin
    if (file_exist(last_file_name)) then last_file = (rd_tfile(last_file_name))(0)
end
if (n_elements(last_file) eq 0) then last_file = fid + '_000000.fit'
if (n_elements(en_file) eq 0) then en_file = 'xxxx'
if (n_elements(code) eq 0) then code = 'IMG2'
;
qdone = 0
icount = 0	;# of times didn't find images
while (not qdone) do begin
    ff = file_list(dir, '*.fit', file=file)
    ss = where(file eq last_file, nss)
    if (nss eq 0) then ist = 0 else ist = ss(0)+1
    ss = where(file ge en_file, nss)
    if (nss eq 0) then ien = n_elements(ff)-1 else ien = ss(0)
    ;
    if (ist lt n_elements(ff)) and (ist le ien) then begin
	prstr, ff(ist:ien), /nomore
	emi_img_proc_s1, ff, ist, ien, outdir, outfil, qpause=qpause
	if (file_exist(outfil)) then emi_img_plot, outfil, code=code
	icount = 0
	wait, 2
    end else begin
	print, 'No new files arrived
	icount = icount + 1
	wait, 5
    end
    ;
    last_file = file(ien)
    prstr, file(ien), file=last_file_name
    if (last_file eq en_file) then qdone = 1
    ;if (icount eq 3) then qdone = 1
    if (icount eq 2) then qdone = 1
end
;
if (file_exist(outfil)) then emi_img_plot, outfil, code=code
end
