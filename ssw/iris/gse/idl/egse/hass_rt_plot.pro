pro hass_rt_plot, win=win
;+
;NAME:
;	hass_rt_plot
;PURPOSE:
;	Plot the 4 sec HK data continuously
;HISTORY:
;	Written 22-Feb-01 by M.Morrison
;-
;
if (n_elements(win) eq 0) then win = 0.5
;
sag_plot_mnem,mnem='SXI_HASSERROR'+['Y','Z'],pmulti=2,win=win,/cont,wait=5
;
;
end