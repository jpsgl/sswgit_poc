pro sxi_disp_cmd_dump, outfil=outfil, hc=hc, date=date, qstop=qstop, $
	lastn=lastn, ist=ist, ien=ien, listall=listall, one_time_fix=one_time_fix
;
;HISTORY:
;	Written 15-Apr-2003 by B.Gantner

;first command list virt address, 0x01110000 in dec form
first_list_virt_address=17891328

fixit=0
if (keyword_set(one_time_fix)) then fixit=1

if (n_elements(date) eq 0) then date = ut_time()  ;returns format '12-mar-03'
file_date = time2file(date, /date)		;returns format '20030324'
infil = '$GSE_PKT_FILE_DIR/0x0031/' + file_date + '.0x0031'

head = sag_rd_hk(infil, 0, 100000, /map)

recs = sag_rd_hk(infil, 0, n_elements(head)-1)
head = head(0:n_elements(head)-1)

out = ['SXI_DISP_CMD_DUMP  Ver 1.0  Program run: ' + !stime, ' ']


;search for the start of a cmd list dump
starts=0
extra_pkts=0
which_list=0
for i=0,n_elements(recs)-1 do begin
 for q=0,15 do begin

  if (recs[i].addr eq (first_list_virt_address+(65536*q))) then begin
    
    dummy=execute("list_length='"+zformat(recs[i].data[0],2)+zformat(recs[i].data[1],2)+$
      zformat(recs[i].data[2],2)+zformat(recs[i].data[3],2)+"'x")
    
    num_of_extra_pkts_needed=(list_length/45)
    
    num_of_extra_pkts_available=0
    x=n_elements(recs)-1
    cnt=1
    for j=i,x do begin
      jtemp=(j+1)<(n_elements(recs)-1)
      if (recs(jtemp).addr eq (first_list_virt_address+(65536*q)+(cnt*180))) then $
        num_of_extra_pkts_available=cnt $
      else x=0
      cnt=cnt+1
    endfor
      
    if (num_of_extra_pkts_needed le num_of_extra_pkts_available) then begin
      starts=[starts,i]
      extra_pkts=[extra_pkts,num_of_extra_pkts_needed]
      which_list=[which_list,q]
    endif
    
  endif
  
 endfor
 
;this is a one time fix for the em when the virt addresses were broken
 if (fixit) then begin
   if (recs[i].addr eq 575604) then begin
     starts=[starts,i]
     extra_pkts=[extra_pkts,0]
     which_list=[which_list,9]
   endif
   if (recs[i].addr eq 575632) then begin
     starts=[starts,i]
     extra_pkts=[extra_pkts,0]
     which_list=[which_list,10]
   endif
   if (recs[i].addr eq 575672) then begin
     starts=[starts,i]
     extra_pkts=[extra_pkts,0]
     which_list=[which_list,11]
   endif
   
 endif
 
endfor
  
  

starts=starts[1:n_elements(starts)-1]		;cut off the leading zero
extra_pkts=extra_pkts[1:n_elements(extra_pkts)-1]
which_list=which_list[1:n_elements(which_list)-1]

if (n_elements(ist) eq 0) then ist = 0
if (n_elements(ien) eq 0) then ien = n_elements(starts)-1
if (keyword_set(lastn)) then ist = n_elements(starts)-lastn

starts=starts[ist:ien]		
extra_pkts=extra_pkts[ist:ien]
which_list=which_list[ist:ien]

if (n_elements(starts) gt 0) then begin
  for i=0, n_elements(starts)-1 do begin

    data_block=recs[starts[i]].data
    for j=1, extra_pkts[i] do data_block=[data_block,recs[starts[i]+j].data]
    db=data_block ;just to shorten following code

    out =[out,' ','*********************************',' ']

    dattim = sxi_time_conv('INSTRUMENT', struct=recs(starts(i)).pkt_head, /string, /msec)
    edattim = sxi_time_conv('EGSE', head(starts(i)).egse_time, /string, /msec)
    out = [out,'LOBT: ' + dattim  + '  Addr:   0x'+zformat(recs[starts[i]].addr,8) +$
      string(recs[starts[i]].addr, format='(i10)'), $
      'EGSE: ' + edattim , ' ']

    out=[out, 'Command list #: '+strcompress(string(which_list[i]),/remove_all)+'     '+$
     'Length: 0x'+zformat(db[0],2)+zformat(db[1],2)+zformat(db[2],2)+zformat(db[3],2),' ']

    out=[out, 'Delay(s)  Delay/Len']

    dummy=execute("list_length='"+zformat(db[0],2)+zformat(db[1],2)+zformat(db[2],2)+zformat(db[3],2)+"'x")
    
    if (list_length gt 1) then begin
     if (((list_length+1)*4) lt n_elements(db)) then begin
      list_data=db[4:(((list_length+1)*4)-1)]

      for b=0,list_length-1 do begin
        dummy=execute("cmd_length='"+zformat(list_data[(b*4)+2],2)+zformat(list_data[(b*4)+3],2)+"'x")
        dummy=execute("delay='"+zformat(list_data[(b*4)+0],2)+zformat(list_data[(b*4)+1],2)+"'x")
        delay_in_secs=delay/64.
        delay_in_secs_str=string(delay_in_secs, format='(f6.3)')
        mnem='          '
        strput, mnem, sag_ext_mnem( bytarr(600)+list_data[(((1+b)*4)+1)<(n_elements(list_data)-1)],'sxi_chlist1_mnem'),0
        out=[out,delay_in_secs_str+'   0x'+zformat(list_data[(b*4)+0],2)+zformat(list_data[(b*4)+1],2)+' '+$
                  zformat(list_data[(b*4)+2],2)+zformat(list_data[(b*4)+3],2)+$
                  '  '+mnem]
        out[n_elements(out)-1]=out[n_elements(out)-1]+ '0x'

        for a=1,cmd_length do begin

          out[n_elements(out)-1]=out[n_elements(out)-1]+$
                zformat(list_data[(((a+b)*4)+0)<(n_elements(list_data)-1)],2)+$
	        zformat(list_data[(((a+b)*4)+1)<(n_elements(list_data)-1)],2)+$
		zformat(list_data[(((a+b)*4)+2)<(n_elements(list_data)-1)],2)+$
		zformat(list_data[(((a+b)*4)+3)<(n_elements(list_data)-1)],2)+'  '
        endfor
        b=b+cmd_length
      endfor
      
      out=[out,' ', 'RAW DUMP:']
      
      for b=0, (list_length/4)+1 do begin
        
	fmt = '(4x, z8.8, 4(2x,4(1x,z2.2)))'
	out=[out,string(recs[starts[i]].addr+(b*16), db[(b*16):((b*16)+15)],format=fmt)]
	;b=b+4
	
      endfor
      
      
      
      
     endif 
    endif

  endfor
endif 



prstr, out, /nomore
if (keyword_set(outfil)) then prstr, out, file=outfil
if (keyword_set(hc)) then prstr, out, file=outfil, /hc, nodelete=keyword_set(outfil), /landscape
if (keyword_set(qstop)) then stop
end