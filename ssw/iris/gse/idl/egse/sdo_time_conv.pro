function sdo_time_conv, epoch, sec, fsec, string=string, $
	qprint=qprint, msec=msec, $
	struct=struct
;+
;$Id: sdo_time_conv.pro 4942 2012-01-13 03:52:02Z sabolish $
;$Name:  $
;NAME:
;	sdo_time_conv
;
;PURPOSE:
;	Routine to convert seconds/subsec past epoch to date/time structure
;		of DS79 and MSOD.
;Parameters:
;	epoch 	-	user specified epoch e.g. 'EGSE','INSTRUMENT','1-Jan-1979'
;	sec 	-	input seconds array
;	fsec	- 	input subseconds array
;	string	- 	returned data is string formatted date
;	qprint	-	debug flag to print converted time
;	msec	-	switch to print milliseconds
;	struct	-	input time structure with struct.time and struct.subsec
;
;HISTORY:
;	Written 18-Sep-07 by G.Linford, modeled after sxi_time_conv.
;	modified 28-Sep-07, G.Linford, added the CVS tags for Id,Name, Log.
;
;$Log: sdo_time_conv.pro,v $
;Revision 1.2  2007/09/28 16:24:13  linford
;add cvs tags ID,Name,Log
;		
;
;
case strupcase(epoch) of
    'EGSE': 		reftim = getenv('SAG_EGSE_EPOCH')
    'INSTRUMENT': 	reftim = getenv('SAG_INSTRUMENT_EPOCH')
    else: 		reftim = epoch
endcase
;
if (keyword_set(struct)) then begin
    sec = struct.time
    fsec = struct.ftime
end
;
v = sec
if (n_elements(fsec) eq 0) then fsec = 0.
if (data_type(fsec) lt 4) then BEGIN
	fsec = unsign(fsec,16) / 2D^16
endif else begin
	; should be a ulong
	u16 = ishft(fsec, -16)
	ffsec = float(u16) / (long(2)^16-1)
end
v = double(sec) + ffsec
out = anytim2ints(reftim, off=v)
;
if (keyword_set(string)) then out = fmt_tim(out, msec=msec)
if (keyword_set(qprint)) then prstr, fmt_tim(out, msec=msec)
;
return, out
end
