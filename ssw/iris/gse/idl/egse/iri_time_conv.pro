function iri_time_conv, epoch, sec, fsec, string=string, $
	qprint=qprint, msec=msec, $
	struct=struct
;+
;$Id: iri_time_conv.pro 10016 2012-09-20 23:42:12Z sabolish $
;$Name:  $
;NAME:
;	iri_time_conv
;
;PURPOSE:
;	Routine to convert seconds/subsec past epoch to date/time structure
;		of DS79 and MSOD.
;Parameters:
;	epoch 	-	user specified epoch e.g. 'EGSE','INSTRUMENT','1-Jan-1979'
;	sec 	-	input seconds array
;	fsec	- 	input subseconds array
;	string	- 	returned data is string formatted date
;	qprint	-	debug flag to print converted time
;	msec	-	switch to print milliseconds
;	struct	-	input time structure with struct.time and struct.subsec
;
;HISTORY:
;	26-mar-2012	DSS	copied from sdo_time_conv and modified for iris
;
;$Log:  $
;		
;
;
case strupcase(epoch) of
    'EGSE':		reftim = getenv('SAG_EGSE_EPOCH')
    'INSTRUMENT': 	reftim = getenv('SAG_INSTRUMENT_EPOCH')
    else: 		reftim = epoch
endcase
;
if (keyword_set(struct)) then begin
    sec = struct.time
    fsec = struct.ftime
end
;
v = sec
if (n_elements(fsec) eq 0) then fsec = 0D
if (data_type(fsec) lt 4) then BEGIN
;	ffsec = fsec / 2D^32
;print, ffsec
	if (strupcase(epoch) eq 'EGSE') then ffsec = fsec / (2D^32 -1)
	if (strupcase(epoch) eq 'INSTRUMENT') then begin
		u16=ishft(fsec, -16)
		ffsec = float(u16) / (2L^16 -1)
	endif
endif else begin
	; should be a ulong
	u16 = ishft(fsec, -16)
	ffsec = float(u16) / (long(2)^16-1)
end
v = double(sec) + ffsec
out = anytim2ints(reftim, off=v)
;
if (keyword_set(string)) then out = fmt_tim(out, msec=msec)
if (keyword_set(qprint)) then prstr, fmt_tim(out, msec=msec)
;
return, out
end
