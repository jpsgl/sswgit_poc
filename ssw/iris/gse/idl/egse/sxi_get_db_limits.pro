pro sxi_get_db_limits, mnem, alow, wlow, whi, ahi, qstop=qstop
;
;+
;NAME:
;	sxi_get_db_limits
;PURPOSE:
;	Get the T&C DB limits for a given mnemonic
;SAMPLE CALLING SEQUENCE:
;	sxi_get_db_limits,'SXI_PCM_P42CURMON',alow, wlow, whi, ahi
;HISTORY:
;	Written 6-Mar-01 by M.Morrison
;-
;
infil = sxi_get_egse_db_file(getenv('SXI_EGSE_DB_PREFIX'))
;
cmd = ['grep', '-i', '^LIM', infil]
spawn, cmd, r, /noshell
;
n = n_elements(mnem)
alow = fltarr(n)-1
wlow = fltarr(n)-1
whi = fltarr(n)-1
ahi = fltarr(n)-1
if (r(0) eq '') then begin
    print, 'SXI_GET_DB_LIMITS: No LIM lines found in DB file: ' + infil
    print, 'Returning...'
    return
end

r = str_replace(r, ',,', ',X,')	;work-around null field problem with str2cols
mat = str2cols(r, ',', /unalign)
;
for i=0,n-1 do begin
    ss = where(mat(1,*) eq strupcase(mnem(i)), nss)
    if (nss ne 0) then begin
	alow(i) = float(mat(3,ss(0)))
	wlow(i) = float(mat(4,ss(0)))
	whi(i)  = float(mat(5,ss(0)))
	ahi(i)  = float(mat(6,ss(0)))
    end
end
;
if (keyword_set(qstop)) then stop
end