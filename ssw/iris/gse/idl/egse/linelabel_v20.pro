
pro linelabel_v20, xf, yf, str, linestyle=linestyle, size=size

if (n_elements(linestyle) eq 0) then linestyle = !p.linestyle
if (n_elements(size) eq 0) then size = 0
sav_psym = !psym
!psym = 0
;
lenf = 0.08	;relative length of line
n = 20
xx = findgen(n)/(n-1) * lenf + xf
yy = fltarr(n) + yf
;
bits, !type, b
;
case b(0) of
    0: xx = xx*(!x.crange(1)-!x.crange(0)) + !x.crange(0)
    1: xx = 10.^( xx*(!x.crange(1)-!x.crange(0)) + !x.crange(0) )
endcase
case b(1) of
    0: yy = yy*(!y.crange(1)-!y.crange(0)) + !y.crange(0)
    1: yy = 10.^( yy*(!y.crange(1)-!y.crange(0)) + !y.crange(0) )
endcase
;
oplot, xx, yy, linestyle=linestyle
sav_line = !p.linestyle
!p.linestyle = 0
xyouts, xx(n-1), yy(n-1), '   ' + str, size=size
;
!psym = sav_psym
!p.linestyle = sav_line 
return
end
