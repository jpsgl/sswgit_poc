function sxi_sel_images, sttim, entim, qdebug=qdebug, $
	group=group, one=one, info=info, sel_dir=sel_dir, $
	all_ff=all_ff, nimg=nimg, days_out=days_out, $
	today=today
;+
;NAME:
;	sxi_sel_images
;PURPOSE:
;	To allow selection of SXI images
;HISTORY:
;	Written 1-Oct-99 by M.Morrison
;	16-Nov-99 (MDM) - Select directory option added
;			- Added all_ff option
;	18-Nov-99 (MDM) - Added NIMG input option (no prompting)
;	29-Jun-00 (MDM) - Made it to look only at 19* and 20*.fit files
;	24-Oct-00 (MDM) - Modified to not use GET_SUBDIR
;	30-Nov-00 (MDM) - Added DAYS_OUT option
;	 5-Mar-01 (MDM) - Added TODAY option
;-
;
;
bdir = getenv('SXI_FITS_DIR')
;
if (keyword_set(sel_dir)) then begin
    tmp = file_list(bdir, '',/cd)
    tmp = reverse(tmp)
    ii = xmenu_sel(tmp)
    if (ii(0) eq -1) then return, ''
    dirs = tmp(ii)
end else begin
    ;;dirs = get_subdirs(bdir)
    ;;dirs = dirs(sort(dirs))	;MDM 11-Mar-00
    dirs = file_list(bdir, '',/cd)
end
;
break_file, dirs, dsk_log, dir00, filnam, ext
ss = where(is_number(filnam), nss)
if (nss eq 0) then return, ''
dirs = dirs(ss)
break_file, dirs, dsk_log, dir00, filnam, ext
;
if (keyword_set(today)) then sttim = anytim2ints( ut_time(), off=-10*60.*60.)
if (keyword_set(sttim)) then sttim2 = time2file(sttim, /date_only) $
			else sttim2 = min(filnam)
if (keyword_set(entim)) then entim2 = time2file(entim, /date_only)$
			else entim2 = max(filnam)
if (not keyword_set(entim)) and (keyword_set(days_out)) then $
			entim2 = time2file( anytim2ints( file2time(sttim2, out='int'), off=days_out*86400D) )

ss = where( (filnam ge sttim2) and (filnam le entim2), nss)
if (nss eq 0) then begin
    print, 'SXI_SEL_IMAGES: Cannot find any directories between ' + sttim2 + ' and ' + entim2
    return, ''
end
;
dirs = dirs(ss)
;
if (keyword_set(qdebug)) then print, 'Dirs: ', dirs
;
out = ''
;
;ff = file_list(dirs, '*.fit', file=file)
ff = file_list(dirs, ['19*.fit', '20*.fit'], file=file)
all_ff = ff
if (ff(0) eq '') then return, out
;
if (keyword_set(sttim)) then begin
    if (not keyword_set(entim)) then entim = '1-Jan-2020'
    ff = sel_filetimes(sttim, entim, ff)
    ;
    if (keyword_set(nimg)) then begin
	out = ff(0: (nimg-1)<(n_elements(ff)-1) )
	return, out
    end
end
;
ff = reverse(ff)
list = ff
if (keyword_set(info)) then begin
    mreadfits, ff, head
    list = get_infox(head, 'EGSE_TIM, naxis1, naxis2')
end
;
ss = xmenu_sel(list, group=group, one=one)
if (ss(0) ne -1) then out = ff(ss)
;
return, out
end