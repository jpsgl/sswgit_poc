pro sxi_cmd_hist, sttim, entim, out, cmd_struct, hc=hc
;+
; $Id: sxi_cmd_hist.pro 4942 2012-01-13 03:52:02Z sabolish $
; $Name:  $
;
; Name: sxi_cmd_hist
;
; History:
;	21-Nov-07, GAL, added cvs tags, and update sag_hk_dbase_blk1 common block for ylog conv.
;
; $Log: sxi_cmd_hist.pro,v $
; Revision 1.2  2007/12/11 23:08:21  linford
; cvs header and new ylg conversion
;
;
;-
common sag_hk_dbase_blk1, tlm, alg, ylg, dsc, cmd, cond
if (n_elements(tlm) eq 0) then stat = execute(getenv('SAG_EGSE_DB_RD_PRO'))

if (n_elements(sttim) eq 0) then sttim = '5-oct'
if (n_elements(entim) eq 0) then entim = '6-oct'
;
mnem = ['SXI_CHLIST' + strtrim(indgen(5)+1,2), 'SXI_CHTCCOUNT']
if (n_elements(info) eq 0) then info = sag_get_mnem(sttim, entim, mnem)
;
cc = info.hk.value(*,5)
dcc = deriv_arr(cc)
ss = where(dcc gt 0, nss)
;
;print,rotate(info.hk.value(30:39,*),4)
;
out = 'SXI_CMD_HIST  Ver 1.0  Program Run: ' + !stime
out = [out, ' ']
cmd_struct0 = {day: 0, time: 0L, cmd: 0L, tc: ' '}
cmd_struct = replicate(cmd_struct0, nss*5)		;most cmds which would happen
icmd_struct = 0
;
next_expected = 0
for i=0,nss-1 do begin
    ii = ss(i)+1
    dcc0 = dcc(ii-1)	;number of commands that just happened
    tmp1 = info.hk.value( ii-1, 0:4)
    tmp2 = info.hk.value( ii, 0:4)
    diff = tmp2-tmp1
    ss2 = where(diff ne 0, nss2)
    ;
    if (min(ss2) eq 0) and (max(ss2) eq 4) then begin
	if (nss2 eq 5) then begin	;MDM added 30-Nov-99
	    ;don't know what to do, just take the 5 in the order they existed
	end else begin
	    jjj = max(where(diff eq 0))
	    ss2 = (indgen(nss2) + jjj+1) mod 5	;reorder to be 4,0,1...
	end
    end
    ;
    ; 0 1 2 3 4
    ;   x x
    ;       x 
    ; x       x
    cur_expected = next_expected
    extra = ''
    if (nss2 eq dcc0) then begin
	extra = 'Resync pointer'
	if (min(ss2) eq 0) and (max(ss2) eq 4) then jjj = min(where(diff eq 0))-1 $
						else jjj = max(ss2)
	if (nss2 eq 5) then jjj = 0	;MDM 30-Nov-99
	next_expected = (jjj + 1) mod 5
    end else begin
	next_expected = (next_expected+dcc0) mod 5
    end
    print, i, ii, ss2(0), nss2, cc(ii), dcc(ii-1), $
		ss2(0), next_expected, extra	;;, next_expected, 
    ;
    std = fmt_tim( info.hk.daytime(ii) ) + string( cc(ii), format='(i6)') + '  ' 
    blank = strmid('                                          ', 0, strlen(std))
    comm = '   '
    if (nss2 eq 0) then begin
	;out = [out, std + 'No change in cmd echo buffer']
	nss2 = dcc0
	ss2 = ( indgen(nss2) + cur_expected ) mod 5
	comm = ' G '
    end
    if (nss2 ne 0) and (nss2 ne dcc0) then begin	;found some differences, but not
							;the right number of differences
	nss2 = dcc0<5		;MDM added <5 30-Nov-99
	ss2 = ( indgen(nss2) + cur_expected ) mod 5
	comm = ' I '
    end
    for j=0,nss2-1 do begin
	if (j eq 0) then lead = std else lead = blank
	out0 = lead + comm + zformat( tmp2(ss2(j)), 8 )
	opcode = mask(tmp2(ss2(j)), 16, 8)
	ss3 = where(cmd.fil(0).value eq opcode, nss3)
	cmd_mnems = ''
	if (nss3 ne 0) then cmd_mnems = arr2str(cmd(ss3).name)
	if (nss3 ne 0) then out0 = out0 + '  ' + cmd_mnems
	out = [out, out0]
	;
	cmd_struct(icmd_struct).day = info.hk.daytime(ii).day
	cmd_struct(icmd_struct).time= info.hk.daytime(ii).time
	cmd_struct(icmd_struct).cmd = tmp2(ss2(j))
	cmd_struct(icmd_struct).tc  = cmd_mnems
	icmd_struct = icmd_struct + 1
    end
end
;
if (icmd_struct eq 0) then cmd_struct = 0b $
		else cmd_struct = cmd_struct(0:icmd_struct-1)
prstr, out, /nomore
if (keyword_set(hc)) then prstr, out, /hc, /land
end
