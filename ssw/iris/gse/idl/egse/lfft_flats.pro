;
;
;
if (n_elements(date) eq 0) then date = ut_time()
if (n_elements(fmax) eq 0) then fmax = 4000
ff = sxi_stol2files(date, 'lfft_flats', 24, out=cmdout)
;
if (ff(0) eq '') then begin
    print, 'Cannot find image files.  Stopping...'
    stop
end
;
img1 = rfits(ff(0))
sel_struct = {nx_block: 30, ny_block: 30, $
		x0: 10, x1: data_chk(img1, /xsize)-10, $
		y0: 10, y1: data_chk(img1, /ysize)-10, $
		b0: 3, b1: 6, $
		offset: 'NONE'}
;
logfil = '/sxisw/dbase/lfft/flats.tab'
outdir = '$SXI_LFFT_DIR/tmp'
fid = str_replace(time2file(cmdout), '.', '_')
;
npe = 2
nexp = 5
info = {tit: 'SXI run of LFFT_FLATS ' + fmt_tim(cmdout) + ' South Amp'}
sag_ltc, ff(1:10), nexp, npe, fmax, sel_struct, out=out, vmask=4, info=info, $
	logfil=logfil, $
	hc=concat_dir(outdir, 'flats_a_'+fid+'.ps'), $
	gif=concat_dir(outdir, 'flats_a_'+fid+'.gif')
disp12, ff(0:11), 'SXI run of LFFT_FLATS ' + fmt_tim(cmdout) + ' South Amp', option = 'STDEV', $
	hc=concat_dir(outdir, 'flats_a_'+fid+'disp12.ps'), $
	gif=concat_dir(outdir, 'flats_a_'+fid+'disp12.gif')

pause

info = {tit: 'SXI run of LFFT_FLATS ' + fmt_tim(cmdout) + ' North Amp'}
sag_ltc, ff(13:22), nexp, npe, fmax, sel_struct, out=out, vmask=4, info=info, $
	logfil=logfil, $
	hc=concat_dir(outdir, 'flats_b_'+fid+'.ps'), $
	gif=concat_dir(outdir, 'flats_b_'+fid+'.gif')
disp12, ff(12:23), 'SXI run of LFFT_FLATS ' + fmt_tim(cmdout) + ' North Amp', option = 'STDEV', $
	hc=concat_dir(outdir, 'flats_b_'+fid+'disp12.ps'), $
	gif=concat_dir(outdir, 'flats_b_'+fid+'disp12.gif')

end