;
;
;
menu = ['Survival Set', 'CCD', 'Operational Set']
ii = xmenu_sel(menu, /one)
if (ii eq -1) then stop
if (n_elements(win) eq 0) then win = 2
if (n_elements(waittime) eq 0) then waittime = 20
;
case ii of
    0: sag_plot_mnem,mnem=['SXI_GPIB_STMPCEB_AP','SXI_GPIB_STMPFWDTEL_AP', 'SXI_PCM_MIRTMP'],/cont,wait=waittime,win=win
    1: sag_plot_mnem,mnem=['SXI_CCDZONETMP'],/cont,wait=waittime,win=win
    2: sag_plot_mnem,mnem=['SXI_FORZONETMP','SXI_AFTZONETMP','SXI_PCM_MIRTMP','SXI_PCM_CEBTMP'],/cont,wait=waittime,win=win
endcase
;
end