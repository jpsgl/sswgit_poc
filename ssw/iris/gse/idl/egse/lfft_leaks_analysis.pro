;
;
;
;NAME:
;	lfft_leaks_analysis
;HISTORY:
;	Written 20-Nov-99 by M.Morrison
;V1.02	11-Mar-00 (MDM) - Added disp12 call
;			- Fixed up for the fact that there is a glass filter
;V1.03	29-Mar-00 (MDM) - Fixed for filter population of EM1 (open=4, not 1)
;			- Changed thresholds too
;V1.04	27-Mar-01 (MDM) - Fixed image size for new 4x4 images
;			- Fixed for new filter imaging order
;V1.05	16-Apr-01 (MDM) - Added filter name printing
;			- Fixed error in which image was supposed to be open/glass
;
if (keyword_set(qskip)) then goto, skip
set_plot,'x
;
outdir = '$SXI_LFFT_DIR/tmp'
if (n_elements(date) eq 0) then date = ut_time()
nimg = 13
infil = sxi_stol2files(date, 'leaks_analysis', nimg, out=cmdout)
;
if (infil(0) eq '') then begin
    print, 'Cannot find image files.  Stopping...'
    stop
end
if (n_elements(infil) ne nimg) then stop, 'Not enough images'

mreadfits, infil, head, img
for i=1,nimg-1 do img(*,*,i) = img(*,*,i) - img(*,*,0)
pr_stats, img, 3, img_info
;
skip:
break_file, infil, dsk_log, dir, filnam, ext
list = filnam+ext+'  ' + get_infox(head, 'EGSE_TIM, naxis1, naxis2, f1filter, f2filter')
;
;xsiz = intarr(nimg)+528
;ysiz = intarr(nimg)+588
xsiz = intarr(nimg)+132
ysiz = intarr(nimg)+147
amin = [ 100, fltarr(nimg) - 100] & amin(4) =  300 & amin(9) =  300
amax = [2000, fltarr(nimg) + 100] & amax(4) = 5000 & amax(9) = 5000	;open/open & glass
;fw = ' ' + ['Drk', '1/4', '1/5', '1/6', '1/1', '1/2', '1/3', '2/4', '3/4', '4/4', '5/4', '6/4', 'Drk']
fw = ' ' + ['Drk', '1/1', '1/2', '1/3', '1/4', '1/5', '1/6', '2/4', '3/4', '4/4', '5/4', '6/4', 'Drk']
stat = strarr(nimg) + '  '
for i=0,nimg-1 do begin
    if (head(i).naxis1 ne xsiz(i)) then stat(i) = stat(i) + 'X_ERR_SIZ '
    if (head(i).naxis2 ne ysiz(i)) then stat(i) = stat(i) + 'Y_ERR_SIZ '
    if (img_info(i).avg lt amin(i)) then stat(i) = stat(i) + 'IMG_AVG_TOO_LOW '
    if (img_info(i).avg gt amax(i)) then stat(i) = stat(i) + 'IMG_AVG_TOO_HI  '
end
ss = where(strmid(stat,2) eq '', nss)
if (nss ne 0) then stat(ss) = stat(ss) + 'OK'
;
imgstr = get_infox(img_info, 'min,max,avg,dev', format=str2arr('f6.0,f6.0,f7.1,f7.1'))
out = ['LFFT_LEAKS_ANALYSIS  Ver 1.05  Program run: ' + !stime, $
	'On machine: ' + get_host(), $
	' ', $
	' File/Image             EGSE Time               NX        NY    Min     Max     Avg       Dev FW   Stat', $
	;9991121_020557.fit  21-NOV-99  02:05:57       528       588      0.   4095.   1794.0    276.6 Drk  OK
	' ', $
	list + '  ' + imgstr + fw + stat]

prstr, out
outfil = concat_dir(outdir, 'lfft_leaks_analysis_' + filnam(0) + '.txt')
prstr, out, fil=outfil
sprint, outfil, /land
;
disp12, infil, 'SXI run of LEAKS_ANALYSIS ' + fmt_tim(cmdout), option='STDEV', $
        hc=concat_dir(outdir, 'lfft_leaks_analysis_'+filnam(0)+'disp12.ps'), $
        gif=concat_dir(outdir, 'lfft_leaks_analysis_'+filnam(0)+'disp12.gif'), $
	dark_file=infil(0)

end
