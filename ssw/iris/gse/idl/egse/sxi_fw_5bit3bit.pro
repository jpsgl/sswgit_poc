print, 'SXI Filterwheel commanding
print,  ' Raw Mech Interface == Software HK/Cmd
print,  ' 5bit/3bit command  == 0-179 equiv
print,  ' Dec   Hex Hi5 Lo3      Dec   Hex
print,  ' '
	; 245  0xf5  30   5  ==  185  0xb9

for i=0,255 do begin
    v = i
    bits, v, bv
    v3 = mask(v, 0, 3)
    v5 = mask(v, 3, 5)
    nice = v5*6+v3
    bits, nice, bnice
    extra = ''
    case nice of
	29:  extra = '  ===== FM Position 1
	59:  extra = '  ===== FM Position 2
	89:  extra = '  ===== FM Position 3
	119: extra = '  ===== FM Position 4
	149: extra = '  ===== FM Position 5
	179: extra = '  ===== FM Position 6
	else: 
    endcase
    if (v3 ge 6) then extra = '  << Invalid 5bit/3bit cmd
    print, v, v, v5, v3, reverse(bv(0:7)), nice, nice, reverse(bnice(0:7)), extra, $
		format='(i4, "  0x", z2.2, i4, i4, (1x,4i1,1x,4i1), "  == ", i4, "  0x", z2.2, (1x,4i1,1x,4i1), a)
end
;
end