;HISTORY:
;	Written ?? by M.Morrison
;	17-Sep-01 (MDM) - Changed bake hot temp for CEB to 38-40
;			  from 46-47.
;	20-Sep-01 (MDM) - Put "surv_ht_mon.pro" case in this program (TB-Wide)
;			- Widened that case (TB-Wide) for CEB (made DBHI = 4, was 0)
;	21-Sep-01 (MDM) - Changed CEB setpoints for Hot from 46/47 to 40/41
;	17-Oct-01 (MDM) - Changed CEB setpoints for cold from -6/-4 to 4/6
;	27-Jun-02 (MDM) - Changed CEB setpoints for cold from 4/6 to -18/-16 (FM2 qual)
;	 6-Aug-02 (MDM) - Added CEB-10% (with telescope "cold" setpoints)


info = {DBLO: 0., DBHI: 0., duty: 0., last_trans: '??', state: 'INI'}
;			STATE = ON (<LO), DB (LO2HI), OFF
tel = info
ceb = info
;
menu_str = ['Bake', 'Non-Op Cold', 'Cold-Initial', 'Cold', 'Hot-Transition', 'Hot', 'TB-Wide', 'CEB-10%']
set_plot,'x
ii = xmenu_sel(menu_str, /one)
case ii of
   -1: stop
    0: begin	;Bake
	tel.dblo = 36		;deg cel
	tel.dbhi = 37		;deg cel
	ceb.dblo = 38		;deg cel
	ceb.dbhi = 40		;deg cel
       end
    1: begin	;Non-Op Cold
	tel.dblo = -10		;deg cel
	tel.dbhi = -7		;deg cel
	ceb.dblo = -10		;deg cel
	ceb.dbhi = -8		;deg cel
       end
    2: begin	;Cold-Initial
	tel.dblo = -9		;deg cel
	tel.dbhi = -7		;deg cel
	ceb.dblo = -6		;deg cel
	ceb.dbhi = -4		;deg cel
       end
    3: begin	;Cold
	tel.dblo = -9		;deg cel
	tel.dbhi = -7		;deg cel
	ceb.dblo = -18		;deg cel
	ceb.dbhi = -16		;deg cel
       end
    4: begin	;Hot- Transition
	tel.dblo = -8		;deg cel
	tel.dbhi = -5		;deg cel
	ceb.dblo = -8		;deg cel
	ceb.dbhi = 41		;deg cel
       end
    5: begin	;Hot
	tel.dblo = -8		;deg cel
	tel.dbhi = -5		;deg cel
	ceb.dblo = 40		;deg cel
	ceb.dbhi = 41		;deg cel
       end
    6: begin	;TB-Wide
	tel.dblo = -10		;deg cel
	tel.dbhi = 0		;deg cel
	ceb.dblo = -10		;deg cel
	ceb.dbhi = 4		;deg cel
       end
    7: begin	;CEB-10%
	tel.dblo = -9		;deg cel
	tel.dbhi = -7		;deg cel
	ceb.dblo = -18		;deg cel
	ceb.dbhi = 41		;deg cel
	ceb_duty = 10.
       end
    else: stop
endcase
;
if (n_elements(tel_duty) eq 0) then tel_duty = 50.
if (n_elements(ceb_duty) eq 0) then ceb_duty = 50.
input, 'Telescope duty cycle (%)', tel_duty, tel_duty
input, 'CEB duty cycle (%)      ', ceb_duty, ceb_duty
tel.duty = tel_duty
ceb.duty = ceb_duty
;
surv_ht_control, tel, ceb
;
end