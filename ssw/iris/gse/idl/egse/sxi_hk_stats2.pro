infil = '/sxi35/log/hk_stats/summary.txt'
outfil = '/sxi35/log/hk_stats/summary2.txt'
infil = '/sxi53/log/hk_stats/summary.txt'
outfil = '/sxi53/log/hk_stats/summary2.txt'
infil = '/sxi72/log/hk_stats/summary.txt'
outfil = '/sxi72/log/hk_stats/summary2.txt'
infil = '/sxi02/sxifm1a/log/hk_stats/summary.txt'
outfil = '/sxi02/sxifm1a/log/hk_stats/summary2.txt'
file_delete, outfil
;
tit = ['SXI_HK_STATS2  Ver 1.0  Program Run: ' + !stime, $
	' ', $
	'Month   #Day  #Hrs     #FITS    #Img   #Shut    #FW1    #FW2     Boot #LED  #Min', $
	'               On        Img           Moves   Moves   Moves      Up  Cycle LEDon', $
	;OCT-99    25  129.89     571     658     489     796     685      25
	' ']
prstr, tit
prstr, tit, file=outfil
;
mat = rd_tfile(infil, 10, nocomment='#')
mat2 = float(mat)
day = reform(mat(0,*))
mon = strmid(day, 0, 6)
umon = mon(uniq(mon, sort(mon)))
;
for i=0,n_elements(umon)-1 do begin
    ss = where(mon eq umon(i), nss)
    tmp = file2time( day(ss(0)), out='int')
    monstr = strmid( gt_day(tmp, /str), 3, 6)
    out = string(monstr, nss, $
	total( mat2(1,ss) ), $	;ontime
	total( mat2(2,ss) ), $	;nimg_file
	total( mat2(3,ss) ), $	;nimg
	total( mat2(4,ss) ), $	;sh
	total( mat2(5,ss) ), $	;fw1
	total( mat2(6,ss) ), $	;fw2
	total( mat2(7,ss) ), $	;boots
	total( mat2(8,ss) ), $	;#led cycle
	total( mat2(9,ss) )/60., $	;#Hrs LEDon
	format='(a, i6, f8.2, 8i8)')
    print, out
    file_append, outfil, out
end
;
    out = string('Total ', n_elements(day), $
	total( mat2(1,*) ), $	;ontime
	total( mat2(2,*) ), $	;nimg_file
	total( mat2(3,*) ), $	;nimg
	total( mat2(4,*) ), $	;fw1
	total( mat2(5,*) ), $	;fw2
	total( mat2(6,*) ), $	;sh
	total( mat2(7,*) ), $	;sh
	total( mat2(8,*) ), $	;sh
	total( mat2(9,*) )/60., $	;sh
	format='(a, i6, f8.2, 8i8)')
    print, out
    file_append, outfil, out

file_append, outfil, ' '
file_append, outfil, out
;
end