;
;+
;NAME:
;	lfft_cm_test
;PURPOSE:
;	Analyze the LFFT "cm_test" images
;HISTORY:
;	Written 20-Nov-99 by M.Morrison
;	22-Nov-99 (MDM) - Adjusted for changes to the STOL 2x2,4x4 summing
;	11-Mar-00 (MDM) - Added disp12
;	31-Oct-00 (MDM) - Adjusted for new STOL procedure (S/W Build 5)
;
if (keyword_set(qskip)) then goto, skip
set_plot,'x
;
outdir = '$SXI_LFFT_DIR/tmp'
if (n_elements(date) eq 0) then date = ut_time()
if (n_elements(fmax) eq 0) then fmax = 4000
infil = sxi_stol2files(date, 'cm_test', 18, out=cmdout)
;
if (infil(0) eq '') then begin
    print, 'Cannot find image files.  Stopping...'
    stop
end
nimg = 18
if (n_elements(infil) ne nimg) then stop, 'Not enough images'

mreadfits, infil, head, img
pr_stats, img, 3, img_info
;
skip:
break_file, infil, dsk_log, dir, filnam, ext
list = filnam+ext+'  ' + get_infox(head, 'EGSE_TIM, naxis1, naxis2')
;
xsiz = [528, 528, 528, 528, 528, 256, 128,  64, 528, 528, 528, 528, 528, 128,  64, 528, 528, 528]
ysiz = [588, 588, 588, 588, 588, 256, 128,  64, 588, 588, 588, 588, 588, 128,  64, 588, 588, 588]
amin = fltarr(nimg) +  300
amax = fltarr(nimg) + 2200
stat = strarr(nimg) + '  '
for i=0,nimg-1 do begin
    if (head(i).naxis1 ne xsiz(i)) then stat(i) = stat(i) + 'X_ERR_SIZ '
    if (head(i).naxis2 ne ysiz(i)) then stat(i) = stat(i) + 'Y_ERR_SIZ '
    if (img_info(i).avg lt amin(i)) then stat(i) = stat(i) + 'IMG_AVG_TOO_LOW '
    if (img_info(i).avg gt amax(i)) then stat(i) = stat(i) + 'IMG_AVG_TOO_HI  '
end
ss = where(strmid(stat,2) eq '', nss)
if (nss ne 0) then stat(ss) = stat(ss) + 'OK'
;
imgstr = get_infox(img_info, 'min,max,avg,dev')
out = ['LFFT_CM_TEST  Ver 1.0  Program run: ' + !stime, $
	'On machine: ' + get_host(), $
	' ', $
	' File/Image             EGSE Time               NX        NY      Min      Max      Avg      Dev  Stat', $
	;19991117_052554.fit  17-NOV-99  05:25:54       528       588     0.00  4095.00  1069.61   245.03  OK
	' ', $
	list + '  ' + imgstr + stat]

prstr, out
outfil = concat_dir(outdir, 'lfft_cm_test_' + filnam(0) + '.txt')
prstr, out, fil=outfil
sprint, outfil, /land
;
disp12, infil, 'SXI run of CM_TEST ' + fmt_tim(cmdout), option='STDEV', $
	hc=concat_dir(outdir, 'cm_test_'+filnam(0)+'disp12.ps'), $
        gif=concat_dir(outdir, 'cm_test_'+filnam(0)+'disp12.gif')

end
