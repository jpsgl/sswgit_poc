pro shut_delay_test_plot, dattim, delay, y, ytit

!p.multi = [0,1,2]
utplot, dattim, y, ytit=ytit, /ynoz
plot,    delay, y, ytit=ytit, /ynoz, psym=-2, xtit='Commanded Delay'

plottime, 0, 0, 'SHUT_DELAY_TEST  Ver 1.00
plottime
pprint
pause
end
;------------------------------------------------
;
if (n_elements(date) eq 0) then date = ut_time()
nset = 13
nrep = 4
nimg = nset*nrep	
if (n_elements(infil) eq 0) then begin
    infil = sxi_stol2files(date, 'shut_delay_test', nimg, out=cmdout)
    ;
    mreadfits, infil, h
end
;
dattim = anytim2ints(h.egse_tim)
delay = (indgen(nimg)/nrep)*20

shut_delay_test_plot, dattim, delay, h.shut_opc, 'Shutter Open Timer'
shut_delay_test_plot, dattim, delay, h.shut_clc, 'Shutter Close Timer'
shut_delay_test_plot, dattim, delay, h.shutmdur, 'Shutter Measured Duration (Sec)'

end
