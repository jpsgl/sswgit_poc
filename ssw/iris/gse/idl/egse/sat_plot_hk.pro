pro sat_plot_hk
;+
;NAME:
;	sat_plot_hk
;PURPOSE:
;	Show that HK data comes in every 4 sec
;HISTORY:
;	Written 29-Oct-01
;-
;
sag_plot_mnem, win=1, pmulti=3, mnem=['sxi_hasshdtmp', 'sxi_ccdzonetmp', 'sxi_debtmp']
sag_plot_mnem, win=1, pmulti=3, mnem=['sxi_hasshdtmp', 'sxi_ccdzonetmp', 'sxi_debtmp'], /hc
;
end