pro mk_sxi_egse_fits, infil, ss_img0, percentd, outdir=outdir, qdisp=qdisp, qstop=qstop
;+
;NAME:
;	mk_sxi_egse_fits
;PURPOSE:
;	To read the raw telemetry and make the FITS files
;HISTORY:
;	Written 24-Sep-99 by M.Morrison
;V1.01	12-Oct-99 (MDM) - Do "margin=4" for display
;			- Make binary header in 3 parts (cam_head, hk, fdb)
;V1.02	10-Nov-99 (MDM) - Write LOBT start/end packet time in FITS primary header
;			- Added 12 bit pack data type to SXI_BUILD_IMG
;V1.03	 6-Dec-99 (MDM) - Added loop back retry on the raw packet read because
;			  of read/file_stat error (on "nrec" value)
;V1.04	20-Apr-00 (MDM) - Added making FITS header of information from HK block
;V1.05	 7-Jul-00 (MDM) - Pass the output file to mk_sxi_egse_fits_head (to
;			  see if the output file exists to grab the source keyword (for regens))
;V1.06	20-Sep-00 (MDM) - Added QSTOP option
;V1.07	10-Oct-00 (MDM) - Changed to be over 50 sec between pkts to say it's a new image (old was 10 sec)
;V1.08	30-Oct-00 (MDM) - Added putting bytes beyond HK in image header into binary extension header
;V1.09	18-Dec-00 (MDM) - Added PERCENTD logging & output argument
;V1.10	29-Mar-01 (MDM) - Added IMG_AVG, IMG_DEV, IMG_MIN, IMG_MAX
;V1.11	29-Nov-01 (MDM) - Modified build_img to handle missing packets
;V1.12	11-Mar-02 (MDM) - Protect against image of 1 word
;-
;
head = sag_rd_hk(infil, /map)
if (data_type(head) ne 8) then begin
    print, 'Cannot read the index file for: ' + infil
    print, 'Returning...
    return
end
;
break_file, infil, dsk_log, dir, filnam, ext
boutdir = getenv('SXI_FITS_DIR')
;
v = head.serno
;uv = uniqo(v)	;record of where image starts
t = head.egse_time
b = bytarr(n_elements(v))
b(0) = 1
;for iii=1L,n_elements(v)-1 do b(iii) = (v(iii) ne v(iii-1)) or $
;				((t(iii)-t(iii-1)) gt 10)
for iii=1L,n_elements(v)-1 do b(iii) = (v(iii) ne v(iii-1)) or $
				((t(iii)-t(iii-1)) gt 50)
uv = where(b)
nuv = n_elements(uv)
;
ss_img = ss_img0
if (ss_img(0) eq -1) then ss_img = indgen(nuv)
;
n = n_elements(ss_img)
for i=0,n-1 do begin
    ist = uv(ss_img(i))
    if (ss_img(i) eq nuv-1) then ien = n_elements(head)-1 $
			else ien = uv(ss_img(i)+1)-1
    ;
    itry = 0
    qdone_rd = 0
    while (not qdone_rd) do begin	;MDM added 6-Dec-99
	pkts = sag_rd_hk(infil, ist, ien)
	if (n_elements(pkts) ne (ien-ist+1)) then begin
	    print, '>>>>>>>>>>>>>>>>>>>> MK_SXI_EGSE_FITS: Packet read error.  Retrying
	    print, 'Number of packets: ', n_elements(pkts), $
			'Number of PKTS requested', (ien-ist+1)
	    itry = itry + 1
	    wait, 1
	    if (itry ge 4) then stop, 'MK_SXI_EGSE_FITS: Packet read problem...'
	end else begin
	    qdone_rd = 1
	end
    end
    ;
    sxi_build_img, pkts, bheader, img, percentd
    npkts = n_elements(pkts)
    ;
    if (n_elements(outdir) eq 0) then outdir2 = concat_dir(boutdir, filnam) $
				else outdir2 = outdir
    if (not file_exist(outdir2, /direct)) then spawn, ['mkdir', outdir2], /noshell
    dattim = sxi_time_conv('EGSE', head(ist).egse_time)
    pst_dattim = sxi_time_conv('INSTRUMENT', pkts(0).pkt_head.time, pkts(0).pkt_head.ftime)
    pen_dattim = sxi_time_conv('INSTRUMENT', pkts(npkts-1).pkt_head.time, pkts(npkts-1).pkt_head.ftime)
    fid = time2file(dattim, /sec)
    outfil = concat_dir(outdir2, fid) + '.fit'
    ;
    ;----- FITS making
    ;
    imgver = 1.01
    progver = 1.12
    nimg = 1
    print, 'Writing: ' + outfil
    delvarx, header
    ;bhead = {cam_head: fix(bheader(0:31), 0, 16), hk: bheader(32:631), fdb: pkts(0).drec.fdb}
    bhead = {cam_head: fix(bheader(0:31), 0, 16), hk: bheader(32:631), fdb: pkts(0).drec.fdb, extra: bheader(632:*)}
    ;
    fxhmake, header, img, /extend, /initialize
    fxaddpar, header, 'EGSE_TIM', fmt_tim(dattim)
    mk_sxi_egse_fits_head, header, bhead, outfil=outfil
    fxaddpar, header, 'PERCENTD', percentd, ' percent of image present'
    fxaddpar, header, 'EXT_NROW', nimg
    fxaddpar, header, 'IMG_VER', imgver
    fxaddpar, header, 'PROG_NAM', 'MK_SXI_EGSE_FITS'
    fxaddpar, header, 'PROG_VER', progver
    wrt_fits_bin_exten, wrt_fits_ver, /get_ver
    fxaddpar, header, 'BEXTEN_V', wrt_fits_ver
    fxaddpar, header, 'PROG_RUN', !stime
    fxaddpar, header, 'PKT_STIM', fmt_tim(pst_dattim, /msec)
    fxaddpar, header, 'PKT_ETIM', fmt_tim(pen_dattim, /msec)
    fxaddpar, header, 'INFILE', infil
    fxaddpar, header, 'ST_REC', ist
    ;
    if (n_elements(img) gt 1) then begin
        idev = stdev(img, iavg)
        imin = min(img, max=imax)
        fxaddpar, header, 'IMG_AVG', iavg
        fxaddpar, header, 'IMG_DEV', idev
        fxaddpar, header, 'IMG_MIN', imin
        fxaddpar, header, 'IMG_MAX', imax
    end
    ;
    fxbhmake, bextheader, nimg, 'SXI_EXT', 'Binary extension with per image header info'
    ;
    fxwrite, outfil, header, img
    wrt_fits_bin_exten, bhead, outfil, /append
    ;
    ;------ Optionally display the image
    ;
    if (keyword_set(qdisp)) and (n_dimensions(img) eq 2) then img_summary, img, fid, header, margin=4
end
;
if (keyword_set(qstop)) then stop
end
