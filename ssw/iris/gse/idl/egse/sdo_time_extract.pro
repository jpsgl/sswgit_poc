pro sdo_time_extract, recs, sec, fsec, qdebug=qdebug
;+
;$Id: sdo_time_extract.pro 4942 2012-01-13 03:52:02Z sabolish $
;$Name:  $
;Name:
;	sdo_time_extract
;
;Purpose:
;	Roution to extract time and subsec from sag_rd_hk input struct.
;
;History:
;	Written 18-Sep-07, by G.Linford
;	Modified 28-Sep-07, G.Linford, added CVS tags Id,Name,Log.
;
;$Log: sdo_time_extract.pro,v $
;Revision 1.2  2007/09/28 16:24:13  linford
;add cvs tags ID,Name,Log
;
;
;
;-
sec = ulonarr(n_elements(recs))
fsec= ulonarr(n_elements(recs))

for i=0, n_elements(recs)-1 do begin
	temp = ulong(recs[i].data,0,2)	; extract first two 32 bit words
	sec[i] = temp[0]
	fsec[i]= temp[1]
end

if (keyword_set(qdebug)) then begin
	temp = tai2_sec1979(sec[0])
	print,'First Time: ',anytim(temp,/yohkoh)
	print,'Last Time:  ',anytim(tai2_sec1979(sec[n_elements(recs)-1]),/yohkoh)
end

end
