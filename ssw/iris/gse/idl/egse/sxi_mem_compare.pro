pro sxi_mem_compare, infil, dattim, occurance
;+
;NAME:
;	sxi_mem_compare
;PURPOSE:
;	To compare a memory dump with a memory load file
;HISTORY:
;	Written 26-Feb-01 by M.Morrison
;-
;
if (n_elements(occurance) eq 0) then iocc = 0 $
				else iocc = occurance
;
sxi_rd_mem_stol, infil, st_addr, array
;
    if (n_elements(dattim) eq 0) then dattim = ut_time()
    fid = time2file( dattim, /date)
    dmp_infil = '$GSE_PKT_FILE_DIR/0x0031/' + fid + '.0x0031'
    dump = sag_rd_hk(dmp_infil, 0, 10000)
    if (data_type(dump) ne 8) then begin
        print, 'No data in input file: ' + dmp_infil
        return
    end

delta = dump.addr - st_addr
ss = where( abs(delta) lt 90, nss)
;
if (nss eq 0) then begin
    print, 'No memory dump for address: 0x', zformat(st_addr,8), ' from input file: ', infil
    print, 'Returning...'
    return
end
if (iocc ge nss) then begin
    print, 'Requested occurance: ', iocc, ' (counting from zero)
    print, 'But there are only ', nss, ' occurances'
    print, 'Returning...'
    return
end
;
npkt = ceil(n_elements(array)/180.)
ist = ss( iocc )
ien = ist + npkt - 1
if (ien gt n_elements(dump)) then begin
    print, 'Incomplete dump.  Not enough data for compare
    print, 'Returning...'
    return
end
;
tmp = dump(ist:ien).addr
if (n_elements(tmp) gt 1) then begin
    dtmp = deriv_arr(tmp)
    if (min(dtmp) ne 180) or (max(dtmp) ne 180) then begin
	print, 'Non contiguous dump.  Need ', npkt, ' packets
	print, 'Addresses:', '0x' + zformat(tmp,8)
	print, 'Deltas:   ', dtmp
        print, 'Returning...'
        return
    end
end

dump_data = dump(ist:ien).data
dump_data = reform(dump_data, n_elements(dump_data))
off = dump(ist).addr - st_addr
if (off ne 0) then dump_data = dump_data(off:*)
diff = dump_data - array
;
print, 'Dump: ' + sxi_time_conv('INSTRUMENT', struct=dump(ist).pkt_head, /string)
print, 'Input File: ' + infil
;
ss = where(diff ne 0, nss)
if (nss eq 0) then begin
    print, ' >>> Load and dump match'
end else begin
    print, nss, ' of ', n_elements(array), ' values mis-match
    for i=0,nss-1 do print, '0x' + zformat(st_addr+ss(i),8) + $
		'  Load=0x' + string(array(ss(i)), format='("0x", z2.2)') + $
		'  Dump=0x' + string(dump_data(ss(i)), format='("0x", z2.2)')	
end
;
end