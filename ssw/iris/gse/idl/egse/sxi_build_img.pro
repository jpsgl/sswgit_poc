pro sxi_build_img, pkts, header, img, percentd
;+
;NAME:
;	sxi_build_img
;PURPOSE:
;	Build an image from the raw telemetry
;HISTORY:
;	Written 24-Sep-99 by M.Morrison
;	10-Nov-99 (MDM) - Added packed data type
;	11-Nov-99 (MDM) - Fixed packed data type code
;	29-Mar-00 (MDM) - Added protection of getting only one packet
;	 1-Dec-00 (MDM) - Fixed case where buffer is LARGER than required amount of data
;	18-Dec-00 (MDM) - Capped nx/ny to be no larger than 528x588
;			- Added QSTOP envvar capability
;	18-Dec-00 (MDM) - Added percentd output parameter
;	29-Nov-01 (MDM) - Added handling of missing packets
;-
;
percentd = 0
header = pkts(0).data
img = 0b
if (n_elements(pkts) le 1) then return
print, 'pkts(0).drec.pcnt', pkts(0).drec.pcnt
if (keyword_set(getenv('QSTOP'))) then stop
;
;---- Check for missing packets
;
img = pkts(1:*).data
v = pkts.drec.pcnt
dv = deriv_arr(v)
if (max(dv) ne 1) then begin	;missing packets
    nn = max(v)<580		;should be 580 or less
    img = bytarr(804, nn)
    for i=0,nn-1 do begin
	ss = where(v eq i+1, nss)
	if (nss eq 1) then img(*,i) = pkts(ss(0)).data
    end
end
;
;
img = reform(img, n_elements(img), /overwrite)
nb_have = n_elements(img)
nx = pkts(0).drec.xsiz < 528
ny = pkts(0).drec.ysiz < 588
fdb = pkts(0).drec.fdb
dtype = mask(fdb, 9, 2)
ist = 0
;;if (nx eq 528) and (ny eq 588) then ist = 32

if (nx eq 0) or (ny eq 0) then return
;
case dtype of
    0: begin			;16 bit per pixel
	nb_need = nx* long(ny)*2
	nshort = nb_need - nb_have
	if (nshort gt 0) then img = [img, bytarr(nshort)]
	img = img(ist : ist+nb_need-1)
	img = fix(img, 0, nx, ny)
       end
    1: begin			;12 bit per pixel
	nb_need = nx* long(ny)*1.5
	nshort = nb_need - nb_have
	if (nshort gt 0) then img = [img, bytarr(nshort)]
	img00 = img(ist : ist+nb_need-1)
        img = intarr(nx, ny)
        ss0 = lindgen(nx*long(ny)*1.5/6)*6            ;every 6th input byte
        ss1 = lindgen(nx*long(ny)/4)*4                ;every 4 output word
		;         0 1  2 3  4 5
		;Nibbles FABC HIDE JKLG (pixels ABC, DEF, GHI, JKL)
	;                  High nibble/byte          Low nibble/byte
        img(ss1)   = (img00(ss0) mod 16)*256	+ img00(ss0+1)        	;A BC
        img(ss1+1) = img00(ss0+3)*16		+ img00(ss0)/16		;DE F
        img(ss1+2) = (img00(ss0+5) mod 16)*256	+ img00(ss0+2)		;G HI
        img(ss1+3) = img00(ss0+4)*16		+ img00(ss0+5)/16	;JK L
       end
    else: begin			;8 bit per pixel
	nb_need = nx* long(ny)*2
	nshort = nb_need - nb_have
	if (nshort gt 0) then img = [img, bytarr(nshort)]
	;img = img(ist : ist+nb_need-1)
	ien = (ist+nb_need-1) < (nx*long(ny)-1)		;MDM 1-Dec-00
	img = img(ist : ien)
	img = reform(img, nx, ny, /over)
       end
endcase

percentd = 100.
if (nshort gt 0) then percentd = (nb_need-nshort) / float(nb_need) * 100.

end