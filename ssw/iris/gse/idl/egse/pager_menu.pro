pro pager_menu, infil
;+
;NAME:
;	pager_menu
;PURPOSE:
;	Prompt user to send an email to generate a page on a pager
;HISTORY:
;	Written 30-Apr-00 by M.Morrison
;-
;
;
if (n_elements(infil) eq 0) then infil = '/sxi01/dbase/cal/info/pager_list1.tab'
mat = rd_ulin_col(infil, /nohead, nocomment='#')
;
ii = xmenu_sel(mat(3,*), /one)
if (ii eq -1) then return
;
mail_to = mat(1,ii)
typ = mat(0,ii)
;
case typ of
    'EMAIL': begin
		msg = ''
		input, 'Enter the NUMERIC message', msg, msg
	     end
    'EMAIL_MSG': begin
		msg = ''
		line = ''
		while (strupcase(line) ne 'QUIT') do begin
		    input, 'Enter a line of text (type QUIT to finish)', line, ''
		    if (strupcase(line) ne 'QUIT') then msg = [msg, line]
		end
		if (n_elements(msg) gt 1) then msg = msg(1:*)
	     end
    else: stop, 'Unrecognized pager type: ' + typ
endcase
;
print, 'Sending the following message to: ' + mail_to
prstr, '  >> ' + msg
mail, msg, user=mail_to
;
end
