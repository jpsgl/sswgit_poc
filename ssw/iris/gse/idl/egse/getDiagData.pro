; + 
; Main: getDiagData.pro
; Purpose: return mnem, time and val arrarys from selected data set
; History: written, G.Linford, 9/Aug/2006
;	Quick Patch, G.Linford, 15-May-2009, fix for mislabeled Diagnostic reformatted table data
;
;$Id: getDiagData.pro 4942 2012-01-13 03:52:02Z sabolish $
;
;$Log: getDiagData.pro,v $
;Revision 1.2  2010/03/25 19:52:21  sdotc
;handle cases where a mnemonic is passed in twice; reformat output
;
;Revision 1.1  2009/05/15 18:08:31  linford
;Checkin IDL-Main for reading High-Rate Diagnostic reformatted data
;
;
; -

; pick the data file
thepath = '/home/sdotc/analogDiagnostics'
;file = Dialog_Pickfile(Get_Path=thepath);              Interactive file selection
file = Dialog_Pickfile(path=thepath,filter='*.merged.tbl');
filename = file_basename(file);                         base filename
print,filename

; Read the selected data
header = rdanalog(file,/header);        return header with time
seconds = long(strmid(header[1],9,strlen(header[1])));
strDate = anytim(seconds+anytim('1-jan-1958'),/yohkoh);  seconds were TAI

data = rdanalog(file);          read data file and return string array


mnem = data[0,*]
time = double(data[1,*]);               save time vector
val = fix(data[2,*]);                   save as a two byte word
help,mnem,time,val

unique_mnem = mnem_choice(mnem,/ret_list);			return unique list
; PROBLEM - what original order!!!!!!!!!!!!
; unique_mnem = unique_mnem[0:N_elements(unique_mnem)-2];  	trim off last element old line changed CGE 10/2/07
; Fix for AIA, 15-May-09, G.Linford for quasi-patched Diagnostic reformatter for those large files.
;	That version still has incorrectly flagged HK tlm as ISS_YERROR-HK and ISS_ZERROR-HK.
if ( where(unique_mnem eq 'ISS_YERROR-HK') ne -1 ) then begin
	; We still has HK tlm incorrectly labeled
	len = n_elements(unique_mnem)
	zapyhk = where(unique_mnem eq 'ISS_YERROR-HK')
	unique_mnem = [unique_mnem[0:zapyhk-1],unique_mnem[zapyhk+1:len-1]]	; remove YERROR-HK
	zapzhk = where(unique_mnem eq 'ISS_ZERROR-HK')
	unique_mnem = [unique_mnem[0:zapzhk-1],unique_mnem[zapzhk+1:len-2]]	; remove ZERROR-HK
	unique_mnem = unique_mnem[0:N_elements(unique_mnem)-1];  	trim off last element
endif else begin
	unique_mnem = unique_mnem[0:N_elements(unique_mnem)-1];  	trim off last element
endelse

print,unique_mnem
new_unique_list = testreorder( unique_mnem, mnem);		reorder the unique list to map to diag data

help,new_unique_list
print,new_unique_list
new_unique_list = new_unique_list[1:N_ELEMENTS(new_unique_list)-1]

totpoints = N_ELEMENTS(val)
points256 = totpoints / 10
points128 = totpoints / 20
count256 = 0
count128 = 0
count512 = 0
for i=0, n_elements(new_unique_list)-1 do begin
	ind = where(mnem eq new_unique_list[i], numpoints);		extract indices for specific data
	ti = time[0,ind]
	da = val[0,ind]
	print,new_unique_list[i], numpoints, format = '(a25, i10)'
;	help,ti,da
    if numpoints eq points256 then begin
    	; this mnemonic is one of the 256 Hz points
        count256 = count256 + 1
        if count256 eq 1 then begin
			diag256 = reform(da)
			time256 = reform(ti)
			mnem256 = new_unique_list[i]
        endif else begin
			diag256 = [[diag256],[reform(da)]]
			time256 = [[time256],[reform(ti)]]
			mnem256 = [mnem256, new_unique_list[i]]
        endelse       
    endif else if numpoints eq points128 then begin
    	; this mnemonic is one of the 128 Hz points
        count128 = count128 + 1
        if count128 eq 1 then begin
			diag128 = reform(da)
			time128 = reform(ti)
			mnem128 = new_unique_list[i]
        endif else begin
			diag128 = [[diag128],[reform(da)]]
			time128 = [[time128],[reform(ti)]]
			mnem128 = [mnem128, new_unique_list[i]]
        endelse       
    endif else begin
    	; this mnemonic was taken at some other frequency
        ; we'll call it 512 because sometimes that works...
        PRINT, 'Some data taken at non-standard frequency...'
        count512 = count512 + 1
        if count512 eq 1 then begin
			diag512 = reform(da)
			time512 = reform(ti)
			mnem512 = new_unique_list[i]
        endif else begin
			diag512 = [[diag512],[reform(da)]]
			time512 = [[time512],[reform(ti)]]
			mnem512 = [mnem512, new_unique_list[i]]
        endelse
    endelse
	
endfor

print
print, 'mnem256: ', mnem256
help, time256, diag256			; transpose will flip
print
print, 'mnem128: ', mnem128
help, time128, diag128
if count512 gt 0 then begin
	print
	print, 'mnem512: ', mnem512
    help, time512, diag512
endif
print


end
