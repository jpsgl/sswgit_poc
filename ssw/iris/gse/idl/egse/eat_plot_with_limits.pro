pro eat_plot_with_limits, sttim, entim, mnem, win=win
;+
;NAME:
;	eat_plot_with_limits
;PURPOSE:
;	Show that the plots can be made with limits marked
;HISTORY:
;	Written 6-Mar-01 by M.Morrison
;-
;
if (n_elements(win) eq 0) then win = 2	;two hours back
;
if (n_elements(sttim) eq 0) then sttim = anytim2ints(ut_time(), off=-60.*60*win)
if (n_elements(entim) eq 0) then entim = ut_time()
if (n_elements(mnem) eq 0) then mnem = 'SXI_PCM_P42CURMON'
;
sxi_get_db_limits, mnem, alow, wlow, whi, ahi
;
info = sag_get_mnem(sttim, entim, mnem)
tmp = [info.(0).value, alow, wlow, whi, ahi]
yr = [min(tmp), max(tmp)]
;
!y.style=2
linecolors
sag_hkplot, info, yrange=yr, /lab
oplot, !x.crange, [1,1]*wlow(0), color=5
oplot, !x.crange, [1,1]*whi(0), color=5
oplot, !x.crange, [1,1]*alow(0), color=2
oplot, !x.crange, [1,1]*ahi(0), color=2

end