function sag_time2apdata, t0, t1, apid , _extra=_extra, out_style=out_style
;
;+
;   Name: sag_time2apinfo
;
;   Purpose: ssw style time2apdata (user times + apid(s) -> {time:'',data:<blah>} 
;
;
;   Keyword Parameters:
;      _extra - inherit to gse_time2files (PARENT, VC, APID...)
;

afiles=gse_time2files(t0,t1,apid=apid,/only,_extra=_extra)

if afiles[0] eq '' then begin 
   box_message,'No files for this time/apid
   return,-1
endif

if n_elements(out_style) eq 0 then out_style='ccsds'

; do first
adata=sag_rd_hk(afiles[0])
atimes=iri_time_conv('INSTRUMENT',struct=adata.pkt_head,/strin,/msec)
times=anytim(atimes,out_style=out_style)
data=temporary(adata.data)
nf=n_elements(afiles)

for f=1,nf-1 do begin 
   box_message,'reading>> ' + afiles[f]
   adata=sag_rd_hk(afiles[f])
   atimes=iri_time_conv('INSTRUMENT',struct=adata.pkt_head,/strin,/msec)
   atimes=anytim(atimes,out_style=out_style)
   times=[times,atimes]
   data=[[temporary(data)],[adata.data]]
   delvarx,adata
endfor

if n_elements(times) gt 0 then begin 
   atimes=anytim(times)
   ssok=where(atimes ge anytim(t0) and atimes le anytim(t1),okcnt)
   if okcnt gt 0 then begin 
      retval={times:times[ssok],data:data[*,ssok]}
   endif else begin 
      box_message,'no data in time range..
      retval=-1
   endelse
endif

return,retval
end







 

