function iris_time2seq, t0 , t1, vc=vc, _extra=_extra, fsn_add=fsn_add, $
   seqhead=seqhead,mnem_fsn=mnen_fsn, use_logs=use_logs, new_paradigm=new_paradigm, no_prime=no_prime
;+
;   Name: iris_time2seq
;
;   Purpose: return sequence start & stop times, optionally first/last FSN
;
;   Input Parameters:
;      t0,t1 - desired time range
;
;   Output:
;      function returns structure vector {start,stop,obsid,firstfsn,lastfsn}
;
;   Keyword Parameters;
;      fsn_add - if set, add FSN
;      use_logs (switch) - if set, attempt to use LOGS (default is tlm packets)
;      new_paradigm (swich) - if set, use event apid only (not all sequences have FSNs in event packet?)
;
;   Calling Examples:
;      IDL> iris_set_gse,/vc1 ; (user selected gse/channel)
;      IDL> seq=iris_time2seq('11-apr-2013','12-apr-2013',/fsn)
;
;   History:
;      20-may-2013 - S.L.Freeland - part of as-run log /  Level1->Level2 pipeline, etc.
;       2-jul-2013 - S.L.Freeland - abort protection (/USE_LOGS only for now)
;      17-jul-2013 - S.L.Freeland - valid .num_repeats for vc (flight events)
;-

common iris_time2seq,vc_orig=vc_orig

temp={seq_start:'',seq_end:'',obsid:0ul,first_fsn:0l,last_fsn:0l,num_repeats:0l, nfsns:0l,nmissing:0l}

use_logs=keyword_set(use_logs)

if use_logs then begin 
   events=iris_gse_time2logs(t0,t1,_extra=_extra)
   if not data_chk(events,/struct) then begin 
      box_message,'USE_LOG set, and no logs for this time/env, so bailing..
      return,-1
   endif
   count=n_elements(events)
endif else begin 
   events=iris_gse_time2events(t0,t1,vc=vc,_extra=_extra, count=count)
endelse

if count eq 0 then begin 
   box_message,'No valid event for this time/vc'
   return,-1
endif

seqstartpatt=(['*START*SEQUENCE*','*sequence*started*'])(use_logs)
seqstart=where(strmatch(events.message,seqstartpatt,/fold),scnt)

if scnt eq 0 then begin 
   box_message,'No SEQUENCE STARTs in this set of events...
   return,-1
endif
retval=replicate(temp,scnt)

retval.seq_start=events[seqstart].time

; now end times - may be mismatch..

seqendpatt=(['*END*SEQUENCE*','*Sequence ended*'])(use_logs)
endtimes=replicate(anytim(0,/ccsds),scnt) ; init to "not found"
;  .... slf, 2-jul-2013  - add code for aborts...

tsends=[seqstart,n_elements(events.message)]
for s=0,scnt-1 do begin 
   emess=events[tsends[s]:tsends[s+1]-1].message
   seqend  =where(strmatch(emess,seqendpatt,/fold),ecnt)
   if ecnt gt 0 then endtimes[s]=events[tsends[s] + seqend].time
endfor 

retval.seq_end=endtimes

num_repeats=replicate(-1,n_elements(retval)) ; init to 
case 1 of
   use_logs:  begin 
      obsid=strextract(strlowcase(events[seqstart-1].message),'table_id=',',')
      num_repeats=str2number(ssw_strsplit(events[seqstart-1].message,'peats=',/tail))
   endcase
   else:begin
      obsid=strextract(events[seqstart+1].message,'OLT_ID=',' START_IDX=')
      num_repeats=str2number(strextract(events[seqstart+1].message,'use)=','|'))
   endcase
endcase
retval.obsid=ulong(obsid)
retval.num_repeats=num_repeats

old_paradigm=1-keyword_set(new_paradigm)

if keyword_set(fsn_add) then begin
   if use_logs then begin 
      fsnmessage=replicate('firstfsn:-1,lastfsn:-1,0framestotal.',scnt)
      for s=0,scnt-1 do begin 
         emess=strcompress(strlowcase(events[tsends[s]:tsends[s+1]-1].message),/remove)
         fsnss=where(strmatch(emess,'*firstfsn*',/fold),fcnt)
         if fcnt gt 0 then begin
            retval[s].first_fsn=long(strextract(emess[fsnss],'firstfsn:',','))
            retval[s].last_fsn=long(strextract(emess[fsnss],'lastfsn:',','))
         endif
      endfor 
   endif else begin
      if old_paradigm then begin 
         if n_elements(mnem_fsn) eq 0 then mnem_fsn='i_sq_header'
         apdata=sag_time2apdata(t0,t1,55,_extra=_extra) ; apid 0x0037
         if data_chk(apdata,/struct) then begin 
            fsns=sag_ext_mnem(apdata.data,mnem_fsn)
            seqtimes=anytim(apdata.times,/int)
            isys=fsns and 'C0000000'x
            fsns=fsns and '2FFFFFFF'x
            ; match seq-start:fsn times
            ss0=tim2dset(seqtimes,anytim(retval.seq_start,/int),delta=dts,offset=offset)
            sse=tim2dset(seqtimes,anytim(retval.seq_end,/int))
            retval.first_fsn=fsns[ss0+1]
            retval.last_fsn=fsns[sse]
            retval.nfsns=retval.last_fsn-retval.first_fsn+1
         endif else box_message,'No telem/apid for FSN derivation available'
      endif else begin 
          box_message,'new paradigm
          for s=0,n_elements(seqstart)-1 do begin
             evtsub=events[seqstart[s]:seqend[s]].message
             fsns=where(strmatch(evtsub,'*FSN=*',/fold_case),fsncnt)
             if fsncnt gt 0 then begin 
                retval[s].first_fsn=long(strextract(evtsub[fsns[0]],'FSN=',' '))
                retval[s].last_fsn= long(strextract(evtsub[last_nelem(fsns)],'FSN=',' '))
                retval[s].nmissing=(retval[s].last_fsn-retval[s].first_fsn)-fsncnt
             endif else box_message,'No FSNs' 
          endfor
      endelse
   endelse
endif

if required_tags(retval,'seq_start') and keyword_set(no_prime) then begin ; remove priming frames
   box_message,'removing priming fsns'
   ns=n_elements(retval)
   for s=0,ns-1 do begin 
      ssw_jsoc_time2data,retval[s].first_fsn,retval[s].last_fsn,drms,key='fsn,isqoltid,iiobslid,quality,iicrsid',/silent, ds='iris.lev0',/jsoc2
      if data_chk(drms,/struct) then begin  
         goodss=where(gt_tagval(drms,/iicrsid,missing=0) ne -1 and gt_tagval(drms,/isqoltid,missing=retval[s].obsid) eq retval[s].obsid ,gcnt)
         pcnt=retval[s].nfsns - gcnt
         if gcnt gt 0 then begin
            retval[s].first_fsn=min(drms[goodss].fsn)
            retval[s].last_fsn =max(drms[goodss].fsn)
            retval[s].nfsns=gcnt
         endif else begin
            box_message,'All priming images'
            retval[s].nfsns=0
         endelse
      endif else begin 
         box_message,'No jsoc lev0 yet..
         retval[s].nfsns=0
      endelse
   endfor
   ssg=where(retval.nfsns gt 0,gcnt)
   if gcnt gt 0 then retval=retval[ssg] else retval=-1
endif

return,retval
end






