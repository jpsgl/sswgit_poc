function gse_time2files,t0,t1,apidx, apid=apid, parent_dir=parent_dir, vc=vc, debug=debug, $
   only_exist=only_exist, novc=novc
;+
;   Name: gse_time2files
;
;   Purpose: ssw style api to packet files - vector {day , vc# , apid } support
;
;   Input Parameters:
;      t0,t1 - desired time range
;      apidx - list of one or more apids - (synonym for keyword APID=)
;
;   Keyword Parameters:
;      apid - list of one or more apids (synonym for apidx positional parameter)
;      vc - optional list of one or more vc# (default=1 or per $GSE_PKT_FILE_DIR )
;      parent_dir - optional parent directory (default per $GSE_PKT_FILE_DIR )
;      only_exist - (switch) if set, only return subset which exist (default is deterministic set)
;      novc - (switch) - set if no /vcN/ paths under $GSE_PKT_FILE_DIR
;
;   Calling Examples:
;      gsefiles=gse_time2files('15-mar-2013','17-mar-2013','0x0040', vc=2)
;      gsefiles=gse_time2files('15-mar-2013',apid=[64,28],vc='1,3,5')
;
;   Note:
;      string APIDs are assumed HEX, like '0x00NN' or '00NN'
;      numeric APIDs are assumed decimal
;-
gseenv=get_logenv('GSE_PKT_FILE_DIR')
debug=keyword_set(debug)

break_file,gseenv,ll,epp,eff

case 1 of
   n_params() eq 0: begin 
        box_message,'Need at least one time. bailing...
        return,''
   endcase
   n_elements(t1) eq 0: t1=t0
   else:
endcase 

;set vc
case 1 of 
   keyword_set(novc): vc=-1
   keyword_set(vc):  ; user supplied
   strmatch(eff,'*vc*',/fold): vc=str2number(eff)
   else: vc=0
endcase

; set parent
case 1 of 
   file_exist(parent_dir): pdir=parent_dir ; user supplied 
   file_exist(epp): pdir=gseenv
   else: begin 
      box_message,'No parent - supply PARENT_DIR or defined $GSE_PKT_FILE_DIR
      pdir=''
   endcase
endcase

vcx=''
if strpos(pdir,'/vc') ne -1 then pdir=ssw_strsplit(pdir,'/vc',/head,tail=vcx)

if vc gt 0 then vtop=concat_dir(pdir[0],'vc'+strtrim(str2arr(vc),2)) else vtop=pdir[0]

; set apid
case 1 of 
   n_params() ge 3: apx=apidx
   keyword_set(apid): apx=apid
   else: begin
      box_message,'No APID positional or keyword... picking one at random..
      apx='0x0040'
   endcase
endcase
if data_chk(apx,/string) then apx=(['0x',''])(strlen(apx[0]) eq 6) + apx else $ 
   apx='0x' + strupcase(string(apx,format='(z4.4)'))

appar=str_perm(vtop,'/'+apx)
ap=0
retval=ssw_time2paths(t0,t1,/flat,parent=appar[ap])

for ap=1,n_elements(appar)-1 do retval=[retval,ssw_time2paths(t0,t1,/flat,parent=appar[ap])]

for ap=0,n_elements(apx)-1 do begin 
   ss=where(strpos(retval,apx[ap]) ne -1)
   retval[ss]=retval[ss]+'.'+apx[ap]
endfor

if debug then stop,'retval,appar,vc,apx

if keyword_set(only_exist) then begin 
   sse=where(file_exist(retval),ecnt)
   if ecnt eq 0 then begin 
      box_message,['None of implied packet files exist:' ,retval]
      retval=''
   endif else retval=retval[sse]
endif

return,retval
end
