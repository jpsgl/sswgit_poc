function getenv_dirlist, in, delim=delim
;+
;NAME:
;	getenv_dirlist
;PURPOSE:
;	Remove all environmental variables which may be 
;	imbedded in the list
;SAMPLE CALLING SEQUENCE:
;	print, getenv_dirlist('$GSE_PKT_FILE_DIR/0x0010')
;HISTORY:
;	Written 23-Sep-99 by M.Morrison
;-
;
if (n_elements(delim) eq 0) then delim = '/'
;
out = in
n = n_elements(out)
ss = where(strpos(in, '$') ne -1, nss)
for i=0,nss-1 do begin
    arr = str2arr(in(ss(i)), delim=delim)
    ss2 = where(strmid(arr, 0, 1) eq '$', nss2)
    for j=0,nss2-1 do arr(i) = get_logenv(arr(i))
    out(ss(i)) = arr2str(arr, delim='/')
end
;
return, out
end