function iris_gse_time2events,t0,t1,_extra=_extra, out_style=out_style, apid=apid, count=count, $
   pattern=pattern, keep_duplicates=keep_duplicates 
;
;+
;   Name: iris_gse_time2events
;
;   Purpose: return IRIS events as reported in 
;
;   Input Parameters:
;      t0 [,t1] - user time/timerange - may span days.
;   
;   Keyword Parameters:
;      out_style - optional time format style (per anytim.pro) - default=CCSDS
;      apid - optional APID - default=0x0040 (kernal) - not sure others make sense yet.., APID list ok
;      _extra - keyword inherit -> gse_time2files options ,VC=n[,m,o,p...] , PARENT=<packet parent>
;      count - (ouput) - number of event (strutures) returned
;      pattern - optional message string pattern to match
;      keep_duplicates- if set, (you guessed it) keep duplicates 
;
;   History:
;      8-may-2013 - S.L.Freeland - break ties to "$IRISSW" - run time log helper utility
;     21-jul-2013 - S.L.Freeland - remove duplicates by default - use /NO_UNIQ to keep dupes
;
;-

retval=''
if n_elements(apid) eq 0 then apid='0x0040' 
efiles=gse_time2files(t0,t1,apid=apid,/only,_extra=_extra) ;vc files for this time/apid

if n_elements(out_style) eq 0 then out_style='ccsds'

template={time:'',message:''}
count=0 ; as in life, assume failure
if efiles(0) eq '' then begin 
   box_message,'No events in this time range/vc set'
endif else begin
   for f=0,n_elements(efiles)-1 do begin 
      edata=sag_rd_hk(efiles[f])
      dtimes=iri_time_conv('INSTRUMENT',struct=edata.pkt_head,/string,/msec)
      nd=n_elements(dtimes)
      retvalx=replicate(template,nd)
      retvalx.time=anytim(dtimes,out_style=out_style)
      retvalx.message=string(edata.data)  
      if f eq 0 then retval=retvalx else retval=[temporary(retval),retvalx]
   endfor
   count=n_elements(retval)
   if n_params() ge 2 and data_chk(retval,/struct) then begin 
      sst=where(anytim(retval.time) ge anytim(t0) and anytim(retval.time) le anytim(t1),count)
      if count eq 0 then begin 
          box_message,'No messages in time range'
          retval=-1
      endif else begin
         retval=retval[sst]
         retval=retval(sort(retval.time))
         if 1-keyword_set(keep_duplicates) and n_elements(retval) gt 1 then begin
            info=get_infox(retval,tag_names(retval))
            retval=retval(uniq(info)) 
         endif
      endelse
   endif
   if data_chk(retval,/struct) and data_chk(pattern,/string)  then begin ; user pattern match option
       pattx=pattern[0]
       pattx=(['','*'])(strmid(pattx,0,1) ne '*') + pattx + (['','*'])(strlastchar(pattx) ne '*') ; assume user wants WC..
       ss=where(strmatch(retval.message,pattx,/fold),pcnt)
       if pcnt gt 0 then retval=retval[ss] else begin 
          box_message,'Messages in time range but none matching your PATTERN...'
          retval=-1
       endelse   
   endif
endelse

return,retval
end
