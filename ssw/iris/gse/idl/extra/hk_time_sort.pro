pro hk_time_sort, info
;
;+
;NAME:
;	hk_time_sort
;PURPOSE:
;	Resort the data which was read from the HK database files since
;	some files are corrupted and the data is not in time order.
;HISTORY:
;	Written 5-Dec-95 by M.Morrison
;-
;
;
for i=0,n_tags(info)-1 do begin
    x = int2secarr(info.(i).daytime)
    ss = sort(x)
    info.(i).daytime = info.(i).daytime(ss)
    info.(i).value   = info.(i).value(ss,*)
end
;
;new_hkplot,info,/label,/qline
end
