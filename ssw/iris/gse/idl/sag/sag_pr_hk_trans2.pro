pro  sag_pr_hk_trans2, sttim, entim, mnem, out, outfil=outfil, $
	hex=hex, info=info, hc=hc, raw=raw, win=win, mnem=mnem0, $
	delta=delta, qstop=qstop
;+
;NAME:
;	sag_pr_hk_trans2
;PURPOSE:
;	To log the times that a given set of mnemonics change state
;SAMPLE CALLING SEQUENCE:
;	 sag_pr_hk_trans2, sttim, entim, mnem
;	 sag_pr_hk_trans2, '6-Jun', '7-Jun', 'SXI_CHPXST'
;RESTRICTIONS:
;	All mnemonics must be from the same packet
;HISTORY:
;	V1.00	Written 28-Jun-01 by M.Morrison (taking sag_pr_hk_trans as start)
;	V1.01	 3-Oct-01 (MDM) - fixed printing problem
;				- Added /delta option and /qstop
;	V1.02	21-Jun-02 (MDM) - Modified the header to be column aligned
;	V1.03	15-Jul-02 (MDM) - Fixed 1.02 mods to work with /delta
;	V1.04	10-Mar-03 (MDM) - Fixed for byte type telemetry points
;-
;
if (keyword_set(mnem0)) then mnem = mnem0
if (keyword_set(win)) then begin
    if (n_elements(sttim) eq 0) then sttim = anytim2ints(ut_time(), off=-60.*60.*win)
    if (n_elements(entim) eq 0) then entim = anytim2ints(sttim, off=60.*60.*win)
end
tit0 = ['SAG_PR_HK_TRANS2   Ver 1.04  Program Run: ' + ut_time() + ' UT', $
	'Checking transitions from ' + fmt_tim(sttim) + ' to ' + fmt_tim(entim)]
out = ' '
if (keyword_set(delta)) then out = [out, ' ', 'First difference values', ' ']
;
nday = gt_day(entim) - gt_day(sttim) + 1
for iday=0,nday-1 do begin
    if (iday eq 0) then sttim2 = sttim else sttim2 = gt_day(iday+gt_day(sttim), /str)
    if (iday eq nday-1) then entim2 = entim else entim2 = gt_day(iday+gt_day(sttim)+1, /str)
    ;
    info = sag_get_mnem(sttim2, entim2, mnem, raw=raw)
    if (data_type(info) eq 8) then begin
	tim = info.(0).daytime
	v = info.(0).value
	if (data_type(v) eq 1) then v = fix(v)
	if (keyword_set(delta)) then begin
	    v = deriv_arr(v)
	    tim = tim(1:*)	;shift by one
	end
	;
	if (n_elements(mnem) ne 1) then begin
	    tmp = ''
	    for j=0,n_elements(mnem)-1 do begin
		v0 = info.(0).value(*,j)
		if (data_type(v0) eq 1) then v0 = fix(v0)	;MDM 10-Mar-03
		if (keyword_set(delta)) then v0 = deriv_arr(v0)
		tmp = tmp + ' ' + strjustify( strtrim(v0,2) )
	    end
	    v = temporary(tmp)
	end
	;
	vstr = strtrim(v(0),2)
	if (keyword_set(hex)) then vstr = string(v(0), format='(z8.8)')
	if (iday eq 0) then begin
	    out = [out, fmt_tim(tim(0)) + '  ' + vstr]
	    prstr, out, /nomore
	end
	;if (iday ne 0) then if (v(0) ne last_v) then begin
	if (n_elements(last_v) ne 0) then if (v(0) ne last_v) then begin
	    str = fmt_tim(tim(0)) + '  ' + vstr
	    ;;print, str
	    out = [out, str]	    
	end
	;
	trans = uniq(v)
	if (keyword_set(qstop)) then stop
	for i=0,n_elements(trans)-1 do begin
	    ii = trans(i)
	    vstr = strtrim(v(ii),2)
	    ien = ii+1
	    if (ien gt n_elements(v)-1) then ien = n_elements(v)-1
	    vstr2 = strtrim(v(ien),2) 
	    if (keyword_set(hex)) then vstr2 = string(v(ien), format='(z8.8)')
	    str = fmt_tim(tim(ien)) + '  ' + vstr2
	    if (vstr ne vstr2) then begin	;protect from saying OFF to OFF
		;;print, str
		out = [out, str]
	    end
	    last_v = v(n_elements(v)-1)
	    last_vstr = strtrim(last_v,2)
	    if (keyword_set(hex)) then last_vstr = string(last_v, format='(z8.8)')
	    last_tim = tim(n_elements(v)-1)
	end
    end
end
;
lab = ['Date', 'Time', mnem]
btit2 = bytarr(132,10) + 32b
lin = out(1)
if (keyword_set(delta)) then lin = out(4)
blin = byte( lin )
blinm1 = [32b, blin]
nlin = 1
for i=0,n_elements(lab)-1 do begin
    if (i eq 0) then st = -1 else st = strpos( lin, ' ', p+1)	;find next space
    ss = where( (blin ne 32b) and (indgen(999) ge st+1), nss)
    p = ss(0)	;column to put label first char
    if (p eq 0) then ss = where( (btit2(p,*) eq 32b), nss) $
		else ss = where( (btit2(p,*) eq 32b) and (btit2(p-1,*) eq 32b), nss)
    btit2(p, ss(0)) = byte(lab(i))
    nlin = nlin > (ss(0)+1)
    ;print, i, st, p, lab(i)
    ss = where( btit2(p,*) eq 32b, nss)
    btit2(p, ss) = byte('.')
end
tit2 = strtrim(string(btit2))
tit2 = tit2(0:nlin-1)
;
out = [tit0, ' ', tit2, out]
prstr, out, /nomore
if (keyword_set(outfil)) then prstr, out, file=outfil
if (keyword_set(hc)) then prstr, out, /hc, /land
end