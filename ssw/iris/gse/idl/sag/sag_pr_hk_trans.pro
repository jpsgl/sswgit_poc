pro  sag_pr_hk_trans, sttim, entim, mnem, out, outfil=outfil, $
	hex=hex, info=info, hc=hc, raw=raw, win=win, mnem=mnem0
;+
;NAME:
;	sag_pr_hk_trans
;PURPOSE:
;	To log the times that a given mnemonic changes state
;SAMPLE CALLING SEQUENCE:
;	 sag_pr_hk_trans, sttim, entim, mnem
;	 sag_pr_hk_trans, '6-Jun', '7-Jun', 'mksqrg3'
;	 sag_pr_hk_trans, '18-jun 3:50','18-jun 4:20','mksqid',info=info,/hex
;HISTORY:
;V1.00	Written 23-Oct-00 by M.Morrison (taking pr_mdihk_trans as start)
;V1.01	24-Oct-00 (MDM) - Added /RAW keyword
;V1.02	25-Oct-00 (MDM) - Added multiple mnemonic option
;V1.03	26-Oct-00 (MDM) - Added keyword WIN and MNEM
;-
;
if (keyword_set(mnem0)) then mnem = mnem0
if (keyword_set(win)) then begin
    if (n_elements(sttim) eq 0) then sttim = anytim2ints(ut_time(), off=-60.*60.*win)
    if (n_elements(entim) eq 0) then entim = anytim2ints(sttim, off=60.*60.*win)
end
out = ['SAG_PR_HK_TRANS   Ver 1.03  Program Run: ' + ut_time() + ' UT', $
	'Checking transitions for ' + mnem + ' from ' + fmt_tim(sttim) + ' to ' + fmt_tim(entim), $
	' ']
;
nday = gt_day(entim) - gt_day(sttim) + 1
for iday=0,nday-1 do begin
    if (iday eq 0) then sttim2 = sttim else sttim2 = gt_day(iday+gt_day(sttim), /str)
    if (iday eq nday-1) then entim2 = entim else entim2 = gt_day(iday+gt_day(sttim)+1, /str)
    ;
    info = sag_get_mnem(sttim2, entim2, mnem, raw=raw)
    if (data_type(info) eq 8) then begin
	tim = info.(0).daytime
	v = info.(0).value
	if (data_type(v) eq 1) then v = fix(v)
	;
	if (n_elements(mnem) ne 1) then begin
	    tmp = ''
	    for j=0,n_elements(mnem)-1 do tmp = tmp + string(v(*,j))
	    v = temporary(tmp)
	end
	;
	vstr = strtrim(v(0),2)
	if (keyword_set(hex)) then vstr = string(v(0), format='(z8.8)')
	if (iday eq 0) then begin
	    out = [out, 'Initial value at ' + fmt_tim(tim(0)) + ' was ' + vstr]
	    prstr, out, /nomore
	end
	;if (iday ne 0) then if (v(0) ne last_v) then begin
	if (n_elements(last_v) ne 0) then if (v(0) ne last_v) then begin
	    str = fmt_tim(tim(0)) + '    from ' + last_vstr + ' to ' + vstr
	    print, str
	    out = [out, str]	    
	end
	;
	trans = uniq(v)
	for i=0,n_elements(trans)-1 do begin
	    ii = trans(i)
	    vstr = strtrim(v(ii),2)
	    ien = ii+1
	    if (ien gt n_elements(v)-1) then ien = n_elements(v)-1
	    if (keyword_set(hex)) then vstr = string(v(ii), format='(z8.8)')
	    vstr2 = strtrim(v(ien),2) 
	    if (keyword_set(hex)) then vstr2 = string(v(ien), format='(z8.8)')
	    str = fmt_tim(tim(ien)) + '    from ' + vstr + ' to ' + vstr2
	    if (vstr ne vstr2) then begin	;protect from saying OFF to OFF
		print, str
		out = [out, str]
	    end
	    last_v = v(n_elements(v)-1)
	    last_vstr = strtrim(last_v,2)
	    if (keyword_set(hex)) then last_vstr = string(last_v, format='(z8.8)')
	    last_tim = tim(n_elements(v)-1)
	end
    end
end
;
;;prstr, out
if (keyword_set(outfil)) then prstr, out, file=outfil
if (keyword_set(hc)) then prstr, out, /hc
end