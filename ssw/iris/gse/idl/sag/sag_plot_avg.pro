pro sag_plot_avg_1plot, tit, expos, struct, out, stat
;
;
;
if (!d.name eq 'X') then tv2, 800, 600, /init, /already
;
nx = n_elements(struct(*,0,0))
ny = n_elements(struct(0,*,0))
nz = n_elements(struct(0,0,*))
;
out = replicate({int: 0., slope: 0.}, nx, ny)
;
yr = [min(struct.avg1), max(struct.avg1)]
plot, expos, struct(0,0,*).avg1, /nodata, yr=yr, xtit='Seconds', ytit='DN', tit=tit
for i=0,nx-1 do begin
    for j=0,ny-1 do begin
	x = expos
	y = struct(i,j,*).avg1
	oplot, x, y, psym=2
	coeff = poly_fit(x,y,1,yfit)
	oplot, x, yfit
	out(i,j).int = coeff(0)
	out(i,j).slope = coeff(1)
    end
end
;
stat = ['Slope  Avg: ' + strtrim( total(out.slope)/nx/ny, 2), $
	'Min: ' + strtrim( min(out.slope), 2), $
	'Max: ' + strtrim( max(out.slope), 2), $
	'Int    Avg: ' + strtrim( total(out.int)/nx/ny, 2), $
	'Min: ' + strtrim( min(out.int), 2), $
	'Max: ' + strtrim( max(out.int), 2)]
plottimes, .5, .3, stat(0)
plottimes, .7, .3, stat(1)
plottimes, .85, .3, stat(2)
;
plottimes, .5, .2, stat(3)
plottimes, .7, .2, stat(4)
plottimes, .85, .2, stat(5)

plottime, 0, 0, 'SAG_PLOT_AVG  Ver 1.0  
plottime
;
end
;------------------------------------------------------------
pro sag_plot_avg, tit, expos, struct, out, $
	logfil=logfil, hc=hc, gif=gif
;
sag_plot_avg_1plot, tit, expos, struct, out, stat
;
;---- Optionally write results to log file
;
if (keyword_set(logfil)) then begin
    outlog = [tit, arr2str(stat(0:2), delim=' '), arr2str(stat(3:5), delim=' ')]
    prstr, outlog
    file_append, logfil, outlog, /uniq
end
;
;---- Optionally write results to GIF file
;
if (keyword_set(gif)) then begin
    save_dev = !d.name
    set_plot,'z'
    sag_plot_avg_1plot, tit, expos, struct, out, stat
    zbuff2file, gif
    set_plot, save_dev
end
;
;---- Optionally write results to GIF file
;
if (keyword_set(hc)) then begin
    save_dev = !d.name
    setps,/land
    if (data_type(hc) eq 7) then psfil = hc else psfil = 'idl.ps'
    device, file=psfil
    sag_plot_avg_1plot, tit, expos, struct, out, stat
    device, /close
    sprint, psfil
    set_plot, save_dev
end
;

if (keyword_set(qstop)) then stop

end