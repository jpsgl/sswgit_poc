function sag_hmi_pkt_struct, code, filnam
;
;
;HISTORY:
;	Written 23-Sep-99 by M.Morrison 
;	28-Sep-99 (MDM) - Added GPIB_INDEX_MAP
;			- Added 0x0400
;	29-Sep-99 (MDM) - Added APIDs 12 and 18 (hex)
;	 5-Nov-99 (MDM) - Added 0x0031
;	31-Mar-00 (MDM) - Added optional 2nd parameter "filnam" so that
;			  the output can be dependent on the date
;			- Added 401
;	 5-Oct-00 (MDM) - Added 0x20, 0x20, 0x13
;	21-Dec-01 (MDM) - Added 0x300
;	13-Jan-05 (GAL) - Mods for HMI, to the best of my knowledge. Some things are
;				likely to be incorrect. e.g. APID is 11 bits, but may only need 8?
;				Time Stamp is 64bits long and is in the secondary header.  32-bits
;				of seconds and 32-bits of sub-seconds. Least signficant bit of the
;				time stamp corresponds to 2^-32 or 233 Pico seconds.
;	17-May-05 (GAL) - Mods for updated packet structure for HMI.
;	5-jan-06  (GAL) - Mods for new index file structure
;	27-Oct-07 (GAL) - Mod for TV1,3 GPIB TCs.
;	29-Nov-07 (GAL) - Mod for new SC packets adding x11b,x11f,x12f,x133.
;       17-Jan-08 (GAL) - Mod for new SC packets adding xC1,xD5
;	10-Nov-08 (GAL) - Hand edit adding SC packet 0x81 using generated line from hk_map on hmiioc-mon.
;
;
pkt_CCSDS = {code: 0b, apid: 0b, scnt: 0, dlen: 0}
pkt_heads = {code: 0b, apid: 0b, scnt: 0, dlen: 0, time: 0L, ftime: 0L}
img_drec = {serno: 0L, time: 0L, ftime: 0, pcnt: 0, xsiz: 0, ysiz: 0, $
                fdb: 0L, spare: bytarr(4)}
hass_drec = {serno: 0L, time: 0L, ftime: 0, pcnt: 0, npair_pkt: 0, npair_ser: 0, $
			spare: bytarr(12)}
isi_heads = {code: 0b, apid: 0b, scnt: 0, dlen: 0, sec_head: bytarr(18)}
;
case strupcase(code) of
    'INDEX_MAP': 	out = {ver:0L, egse_time: 0L, egse_sec: 0L, inst_time:0L, inst_ftime:0L}
    'GPIB_INDEX_MAP': 	out = {egse_time: 0L}
    'PKT_HEADS':	out = pkt_heads
    'IMG_DREC':		out = img_drec
	'.0X0022': 		out = {pkt_head: pkt_CCSDS, data: bytarr(618)}
	'.0X012F': 		out = {pkt_head: pkt_CCSDS, data: bytarr(138)}
	'.0X0021': 		out = {pkt_head: pkt_CCSDS, data: bytarr(244)}
	'.0X0012': 		out = {pkt_head: pkt_CCSDS, data: bytarr(46)}
	'.0X002B': 		out = {pkt_head: pkt_CCSDS, data: bytarr(76)}
	'.0X0032': 		out = {pkt_head: pkt_CCSDS, data: bytarr(46)}
	'.0X00D5': 		out = {pkt_head: pkt_CCSDS, data: bytarr(124)}
	'.0X07D3': 		out = {pkt_head: pkt_CCSDS, data: bytarr(0)}
	'.0X0027': 		out = {pkt_head: pkt_CCSDS, data: bytarr(152)}
	'.0X002E': 		out = {pkt_head: pkt_CCSDS, data: bytarr(94)}
	'.0X07D8': 		out = {pkt_head: pkt_CCSDS, data: bytarr(64)}
	'.0X07D4': 		out = {pkt_head: pkt_CCSDS, data: bytarr(128)}
	'.0X0005': 		out = {pkt_head: pkt_CCSDS, data: bytarr(128)}
	'.0X0013': 		out = {pkt_head: pkt_CCSDS, data: bytarr(200)}
	'.0X0033': 		out = {pkt_head: pkt_CCSDS, data: bytarr(82)}
	'.0X07D2': 		out = {pkt_head: pkt_CCSDS, data: bytarr(38)}
	'.0X07EC': 		out = {pkt_head: pkt_CCSDS, data: bytarr(32)}
	'.0X07E8': 		out = {pkt_head: pkt_CCSDS, data: bytarr(64)}
	'.0X0011': 		out = {pkt_head: pkt_CCSDS, data: bytarr(236)}
	'.0X0039': 		out = {pkt_head: pkt_CCSDS, data: bytarr(38)}
	'.0X003E': 		out = {pkt_head: pkt_CCSDS, data: bytarr(120)}
	'.0X0133': 		out = {pkt_head: pkt_CCSDS, data: bytarr(90)}
	'.0X0010': 		out = {pkt_head: pkt_CCSDS, data: bytarr(68)}
	'.0X0029': 		out = {pkt_head: pkt_CCSDS, data: bytarr(160)}
	'.0X0015': 		out = {pkt_head: pkt_CCSDS, data: bytarr(188)}
	'.0X002D': 		out = {pkt_head: pkt_CCSDS, data: bytarr(182)}
	'.0X0435': 		out = {pkt_head: pkt_CCSDS, data: bytarr(288)}
	'.0X002C': 		out = {pkt_head: pkt_CCSDS, data: bytarr(220)}
	'.0X0002': 		out = {pkt_head: pkt_CCSDS, data: bytarr(618)}
	'.0X011B': 		out = {pkt_head: pkt_CCSDS, data: bytarr(138)}
	'.0X0024': 		out = {pkt_head: pkt_CCSDS, data: bytarr(618)}
	'.0X00C1': 		out = {pkt_head: pkt_CCSDS, data: bytarr(124)}
	'.0X0026': 		out = {pkt_head: pkt_CCSDS, data: bytarr(170)}
	'.0X07D5': 		out = {pkt_head: pkt_CCSDS, data: bytarr(0)}
	'.0X0016': 		out = {pkt_head: pkt_CCSDS, data: bytarr(224)}
	'.0X001F': 		out = {pkt_head: pkt_CCSDS, data: bytarr(120)}
	'.0X001A': 		out = {pkt_head: pkt_CCSDS, data: bytarr(70)}
	'.0X0004': 		out = {pkt_head: pkt_CCSDS, data: bytarr(618)}
	'.0X002A': 		out = {pkt_head: pkt_CCSDS, data: bytarr(232)}
	'.0X011F': 		out = {pkt_head: pkt_CCSDS, data: bytarr(90)}
	'.0X001D': 		out = {pkt_head: pkt_CCSDS, data: bytarr(130)}
	'.0X003F': 		out = {pkt_head: pkt_CCSDS, data: bytarr(120)}
	'.0X0019': 		out = {pkt_head: pkt_CCSDS, data: bytarr(20)}
	'.0X0028': 		out = {pkt_head: pkt_CCSDS, data: bytarr(174)}
	'.0X0025': 		out = {pkt_head: pkt_CCSDS, data: bytarr(128)}
	'.0X001E': 		out = {pkt_head: pkt_CCSDS, data: bytarr(120)}
	'.0X0003': 		out = {pkt_head: pkt_CCSDS, data: bytarr(618)}
	'.0X002F': 		out = {pkt_head: pkt_CCSDS, data: bytarr(70)}
	'.0X0023': 		out = {pkt_head: pkt_CCSDS, data: bytarr(618)}
	'.0X0001': 		out = {pkt_head: pkt_CCSDS, data: bytarr(244)}
	'.0X0081':              out = {pkt_head: pkt_CCSDS, data: bytarr(122)}
    else: begin
		print, 'SAG_SXI_PKT_STRUCT:  Unrecognized code= ' + code
		out = 0b
	  end
endcase
;
return, out
end
