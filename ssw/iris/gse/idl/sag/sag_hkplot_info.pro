function sag_hkplot_info, hk, tit=tit, nums=nums, times=times, mnemlist=mnemlist, list=list
;+
;NAME:
;	sag_hkplot_info
;PURPOSE:
;	To extract information out of the HK structure
;SAMPLE CALLING SEQUENCE:
;	out = sag_hkplot_info(hk, /tit)
;	out = sag_hkplot_info(hk, /mnem, /nums)
;INPUT:
;	hk	- The HK structure returned by GET_HK_INFO
;OPTIONAL KEYWORD INPUTS:
;	tit	- If set, then return the titles for the mnemonics
;	nums	- If set, then return the mnemonic number in front
;		  of the title or mnemonic name
;	times	- If set, get the discrete values for the list of
;		  input times.  
;	mnemlist - If set, match the mnemonic name to the "code"
;		  number in the structure.
;OUTPUT:
;	out	- Varies - see optional inputs.  Default is the
;		  mnemonic names
;HISTORY:
;	Written 8-Dec-99 by M.Morrison using HKPLOT_INFO as start
;-
;
;
;   times = ['19-dec-94', '20-dec-94 12:00', '21-dec-94 12:00', $
;		'3-jan-95 16:00', '5-jan-95', '6-jan-95']

;
tags = tag_names(hk)
ntags = n_elements(tags)
;
tits = ' '
clist = 0
mnem_names = ''
for i=0,ntags-1 do begin
    mnem_names = [mnem_names, strlowcase(hk.(i).mnem)]
    tits = [tits, string(strupcase(hk.(i).mnem)+'         ', format='(a10)') + $
		' == ' + string(hk.(i).descr)]
    clist = [clist, hk.(i).code]
end
;
tits       = strtrim(tits(1:*))
clist      = clist(1:*)
;mnem_names = strlowcase(strmid(tits, 0, 8))
mnem_names = mnem_names(1:*)
out = mnem_names			;default output
if (keyword_set(tit)) then out = tits
if (keyword_set(nums)) then out = string(indgen(n_elements(tits)), format='(i3)') + '  ' + out
;
;------
;
if (keyword_set(times)) then begin
    out = 'SAG_HKPLOT_INFO Print Option Ver 1.0  Program Run: ' + !stime
    out = [out, ' ']
    if (data_type(hk) ne 8) then return, [out, '******** ERROR (HK not a structure) ***********']
    ;
    times0 = anytim2ints(times)
    qrange = (not keyword_set(list)) and (n_elements(times0) eq 2)
    tags = tag_names(hk)
    ntags = n_elements(tags)

    if (qrange) then begin
	for i=0,ntags-1 do begin
	    dummy = string(hk.(0).value(0,0))
	    fmt = '(1x, a19, 99a' + strtrim(strlen(dummy),2) + ')'
	    str = string('    Time ', hk.(i).mnem, format=fmt)
	    out = [out, str]
	    ss = sel_timrange(hk.(i).daytime, times0(0), times0(1), /between)
	    if (ss(0) ne -1) then begin
		for j=0,n_elements(ss)-1 do begin
		    tmp = hk.(i).value(ss(j),*)
		    if (data_type(tmp) eq 1) then tmp = fix(tmp)
		    str = fmt_tim(hk.(i).daytime(ss(j))) + arr2str(string(tmp), delim=' ')
		    out = [out, str]
		end
	    end
	end
    end else begin
	for itime=0,n_elements(times0)-1 do begin
	    for i=0,ntags-1 do begin
	        ss = tim2dset(hk.(i).daytime, times0(itime))
		str = fmt_tim(hk.(i).daytime(ss))
		if (i eq 0) then str = '------- HK Values for ' + str + '--------------------' $
			    else str = '................. for ' + str + '....................'
		out = [out, str, ' ']
		for j=0,n_elements(hk.(i).mnem)-1 do begin
	            str = string(i, j, hk.(i).code(j), format='(3i5)') + $
			string(strupcase(hk.(i).mnem(j))+'         ', format='(3x, a10)') + $
			' == ' + string(hk.(i).descr(j)+'                      ', format='(a32)') + $
			string(hk.(i).value(ss,j))
		    out = [out, str]
		end
	    end
	end
    end
end
;
;--------
;
if (keyword_set(mnemlist)) then begin
    n = n_elements(mnemlist)
    out = intarr(n)
    for i=0,n-1 do begin
	ss = where(strtrim(mnem_names,2) eq strtrim(mnemlist(i),2))
	if (ss(0) ne -1) then out(i) = clist(ss)
    end
end
;
return, out
end
