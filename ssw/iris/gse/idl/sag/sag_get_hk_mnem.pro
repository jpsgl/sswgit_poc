function sag_get_hk_mnem, qgroup, sea=sea, $
                lst_file=lst_file, group=group, qdebug=qdebug
;+
;NAME:
;       sag_get_hk_mnem
;PURPOSE:
;       Allow selection of mnemonics from the full list of
;       mnemonics or from a group list file.
;SAMPLE CALLING SEQUENCE:
;       mnem = get_hk_mnem(1)
;       mnem = get_hk_mnem(0, sea='volt')
;INPUT:
;       qgroup  - 0 for full list, 1 for group list
;OPTIONAL KEYWORD INPUT:
;       sea     - If set, then reduce the list of mnemonics to
;                 the ones that have that string in somewhere
;       group   - Optionally the widget ID of the parent
;HISTORY:
;       Written 28-Sep-99 by M.Morrison using MDI GET_HK_MNEM.PRO
;-
;
;
qdebug = keyword_set(qdebug)
;
mnem = ''
if (keyword_set(qgroup)) then begin     ;select from group file
    if (n_elements(lst_file) eq 0) then lst_file = concat_dir('$SAG_TELEM_INFO', 'sag_xhkplot_mnem.lst')
    list = rd_tfile(lst_file)
    ss = where(strmid(list, 0, 3) eq '***')
    tits = strmid(list(ss), 4, 999)
    ntits = n_elements(tits)
    tits = tits(0:ntits-2)
    ;
    if (keyword_set(group)) then mapx, group, /map, /show, sensitive=0
    itit = xmenu_sel(tits, /one, group=group)
    if (keyword_set(group)) then mapx, group, /map, /show, sensitive=1
    ;
    itit = itit(0)
    if (qdebug) then print, itit
    ;
    if (itit ne -1) then begin
        i1 = ss(itit) + 1
        i2 = ss(itit+1) - 1
        mnem_mat = str2cols(list(i1:i2))
        mnem = strlowcase(strtrim(mnem_mat(0,*),2))
        if (qdebug) then print, mnem
    end
end else begin                          ;select from full mnemonic list
    if (n_elements(lst_file) eq 0) then lst_file = concat_dir('$SAG_TELEM_INFO', 'sag_xhkplot_mnem.full_lst')
    list = rd_tfile(lst_file)
    if (keyword_set(sea)) then begin
        ss = wc_where(list, '*' + sea + '*', /case_ignore)
        if (ss(0) ne -1) then list = list(ss)
    end

    if (keyword_set(group)) then mapx, group, /map, /show, sensitive=0
    ss = xmenu_sel(list, group=group)
    if (keyword_set(group)) then mapx, group, /map, /show, sensitive=1
    ;
    if (qdebug) then print, ss
    if (ss(0) ne -1) then begin
        mnem = strlowcase( strmid(list(ss), 1, 10) )
        if (qdebug) then print, mnem
    end
end
;
return, mnem
end
