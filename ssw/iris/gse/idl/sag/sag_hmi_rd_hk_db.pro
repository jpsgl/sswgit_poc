;+
; $Id: sag_hmi_rd_hk_db.pro 4942 2012-01-13 03:52:02Z sabolish $
; $Name:  $
;
;NAME:
;	sag_hmi_rd_hk_db
;PURPOSE:
;	Read the EGSE database definition files (ASCII) into structures
;HISTORY:
;	Written 23-Sep-99 by M.Morrison using TRACE RD_EGSE_HK_TXT
;	29-Sep-99 (MDM) - Added command entry reading
;	 7-Jul-00 (MDM) - Moved sxi_EGSE_DB_FILE override to sxi_get_egse_db_file.pro
;	18-Dec-00 (MDM) - Made infil keyword work
;	 2-Jan-02 (MDM) - Added IFDEF logic to avoid blocks of entries
;	 9-May-02 (MDM) - Added conditional information being passed out in structure
;	12-Jan-05 (GAL) - Modifed for HMI
;	20-Nov-07 (GAL) - Modified for adding YLOG conversions.
;
; $Log: sag_hmi_rd_hk_db.pro,v $
; Revision 1.2  2007/12/04 16:58:32  linford
; Add YSI or ylog conversions
;
;
;-
;--------------------------------------------------------------------------
function hmi_rd_hk_db_define, txt, qdebug=qdebug

out = txt
n = n_elements(txt(0,*))
qok = bytarr(n) + 1	;all ok
v = indgen( n )

ss = where(strmid(txt(0,*),0,6) eq 'DEFINE', nss)
defined = ''
if (nss ne 0) then defined = strtrim(strmid(txt(0,ss), 7, 99), 2)

ss = where(strmid(txt(0,*),0,5) eq 'IFDEF', nss)
for i=0,nss-1 do begin		;for each IFDEF
    name = strtrim(strmid(txt(0,ss(i)), 6, 99), 2)
    if (is_member(name, defined)) then begin
	;do nothing
    end else begin
	ss2 = where( (txt(0,*) eq 'ENDIF') and (v gt ss(i)), nss2)	;find first ENDIF after IFDEF
	if (nss2 eq 0) then stop, 'TROUBLE finding ENDIF in HMI_RD_HK_DB_DEFINE'
	qok(ss(i):ss2(0)) = 0	;mark as bad
	if (keyword_set(qdebug)) then print, 'Removed IFDEF block ', name, ss(i), ss2(0)
    end
end
;
ss = where(qok, nss)
if (keyword_set(qdebug)) then print, 'Kept ', nss, ' out of ', n, ' lines (removed IFDEF)
if (nss eq 0) then out = '' else out = out(*,ss)

return, out
end
;---------------------------------------------------------------------------
function sag_hmi_rd_hk_db_s1, value
;
;
;
n = n_elements(value)
out = lonarr(n)
;
for i=0,n-1 do begin
    v = strtrim(value(i),2)
    iv = 0
    if (strmid(v, 0, 2) eq '0x') then reads, v, iv, format='(2x, z)' $
					else iv = fix(v)
    out(i) = iv
end
;
return, out
end
;---------------------------------------------------------------------------
pro sag_hmi_rd_hk_db, tlm2, alg2, ylg2, dsc2, cmd2, cond2, infil=infil0, qdebug=qdebug, txt=txt
;
;
; 21-Nov-07, added ylg2 to formal parameter list.
;  
common sag_hk_dbase_blk1, tlm, alg, ylg, dsc, cmd, cond
;
if (keyword_set(qdebug)) then idl_speed, 0
;
infil = hmi_get_egse_db_file(getenv('HMI_EGSE_DB_PREFIX'))
if (keyword_set(infil0)) then infil = infil0
;
if (not file_exist(infil)) then begin
    print, 'SAG_RD_HK_DB:  Cannot find input file: ' + infil
    stop
end
;
print, 'Reading: ', infil
txt = rd_tfile(infil, 20, delim=',', nocomment='#')
txt = hmi_rd_hk_db_define(txt, qdebug=qdebug)	;MDM 2-Jan-01
;
;------------------------------------
ylg = {name: '', descr: '', acoeff:0.0d, bcoeff:0.0d, ccoeff:0.0d, $
	dcoeff:0.0d, ecoeff:0.0d, fcoeff:0.0d, gcoeff:0.0d, date_mod: ''}
ss = where( txt(1,*) eq 'YLOGDEF', nss)
if (nss ne 0) then begin
	ylg = replicate(ylg, nss)	; create one entry for each 8 line block
	ylg.name = reform(txt(2,ss))	; Is either "YSI_5K_THERMISTOR" or "YSI_10K_THERMISTOR"
	ylg.descr = reform(txt(3,ss))
	ylg.date_mod = reform(txt(4,ss))
	yss = where( txt(1,*) eq 'YLOGENT', ynss)
	if ( ynss ne 0 ) then begin
		ylg_l = where( ylg.name eq 'YSI_5K_THERMISTOR', nylgk )
		if ( nylgk ne 0 ) then begin
			ss5 = where( txt(2,*) eq 'YSI_5K_THERMISTOR', nss)
			ylg[ylg_l].acoeff = double(txt(3,ss5(1)))
			ylg[ylg_l].bcoeff = double(txt(3,ss5(2)))
			ylg[ylg_l].ccoeff = double(txt(3,ss5(3)))
			ylg[ylg_l].dcoeff = double(txt(3,ss5(4)))
			ylg[ylg_l].ecoeff = double(txt(3,ss5(5)))
			ylg[ylg_l].fcoeff = double(txt(3,ss5(6)))
			ylg[ylg_l].gcoeff = double(txt(3,ss5(7)))
		end
	end
	if ( ynss ne 0 ) then begin
		ylg_l = where( ylg.name eq 'YSI_10K_THERMISTOR', nylgk )
		if ( nylgk ne 0 ) then begin
			ss5 = where( txt(2,*) eq 'YSI_10K_THERMISTOR', nss)
			ylg[ylg_l].acoeff = double(txt(3,ss5(1)))
			ylg[ylg_l].bcoeff = double(txt(3,ss5(2)))
			ylg[ylg_l].ccoeff = double(txt(3,ss5(3)))
			ylg[ylg_l].dcoeff = double(txt(3,ss5(4)))
			ylg[ylg_l].ecoeff = double(txt(3,ss5(5)))
			ylg[ylg_l].fcoeff = double(txt(3,ss5(6)))
			ylg[ylg_l].gcoeff = double(txt(3,ss5(7)))
		end 
	end
	
end
;
;------------------------------------
dsc = {name: '', descr: '', $		;string table description lookup
	low: 0L, hi: 0L, str: '', date_mod: ''}
ss = where( txt(0,*) eq 'DSC', nss)
if (nss ne 0) then begin
    dsc = replicate(dsc, nss)
    dsc.name = reform(txt(1,ss))
    dsc.low = long(reform(txt(2,ss)))
    dsc.hi = long(reform(txt(3,ss)))
    dsc.str = reform(txt(4,ss))
    dsc.descr = reform(txt(5,ss))
    dsc.date_mod = reform(txt(6,ss))
end
;
;------------------------------------
alg = {name: '', order: 0, coeff: dblarr(6), $
	date_mod: '', descr: ''}	;analog coefficient conversion table
ss = where( txt(0,*) eq 'ALG', nss)
if (nss ne 0) then begin
    alg = replicate(alg, nss)
    alg.name = reform(txt(1,ss))
    alg.order = long(reform(txt(2,ss)))
    alg.coeff = double(txt(3:8,ss))
    alg.descr = reform(txt(9,ss))
    alg.date_mod = reform(txt(10,ss))
end
;
;------------------------------------
nmaps = 3
tlm = {name: '', source: '', group: '', $
	unit: '', conv_code: '', conv: '',  $
	descr: '', date_mod: '', $
	type: '', stbit: 0L, nbit: 0L, $	;in SRC
	cond: '', $
	apid: intarr(nmaps), stbyte: lonarr(nmaps)}
;
ss = where( txt(0,*) eq 'TLM', nss)
if (nss ne 0) then begin
    tlm = replicate(tlm, nss)
    tlm.name 	= strlowcase(reform(txt(1,ss)))		
    tlm.source 	= reform(txt(2,ss))			
    tlm.group 	= reform(txt(3,ss))			
    tlm.unit 	= reform(txt(4,ss))			
    tlm.conv_code = reform(txt(5,ss))			
    tlm.conv 	= reform(txt(6,ss))			
    tlm.descr 	= reform(txt(7,ss))		
end

offset = getenv('SAG_PKT_HEAD_LEN')
if (keyword_set(offset)) then offset = long(offset) else offset = 0
ss = where( txt(0,*) eq 'SRC', nss)
for i=0L,nss-1 do begin
    ii = where(tlm.name eq strlowcase(txt(1,ss(i))), nss2)
    case nss2 of
    ;if (nss2 eq 1) then begin
    1: begin
	tlm(ii).apid(0) 	= sag_hmi_rd_hk_db_s1(txt(3,ss(i)))
	tlm(ii).stbyte(0)	= txt(4,ss(i)) - offset
	tlm(ii).stbit		= txt(5,ss(i))
	tlm(ii).nbit		= txt(6,ss(i))
	tlm(ii).type		= txt(7,ss(i))
	tlm(ii).cond		= txt(8,ss(i))
	tlm(ii).date_mod	= txt(10,ss(i))
    end
    0: print, 'WARNING: SRC entry, but no TLM entry for ', txt(1,ss(i))
    ;end else begin
    else: begin
	stop, 'More than on SRC line for a given mnemonic
    end
    end
end
;
;
;------------------------------------
nmax_fil = 8		;max FIL entries
nmax_key = 8		;max KEY entries
;
tc_fil0 = {ifrm: 0, label: '', stbit: 0, length: 0, value: 0L, $
			descr: '', date_mod: ''}
tc_key0 = {ifrm: 0, label: '', stbit: 0, length: 0, low: 0L, hi: 0L, default: 0L, $
			descr: '', date_mod: ''}

cmd = {name: '', group: '', nfrm: 0, nkey: 0, nfil: 0, $	;command structure
	critical: '', descr: '', date_mod: '', $
	fil: replicate(tc_fil0, nmax_fil), $
	key: replicate(tc_key0, nmax_key)}
ss = where( (txt(0,*) eq 'TC') and (txt(1,*) eq 'CMD'), nss)
if (nss ne 0) then begin
    cmd = replicate(cmd, nss)
    cmd.name 	= strlowcase(reform(txt(2,ss)))		
    cmd.group 	= reform(txt(3,ss))			
    cmd.nfrm 	= fix(reform(txt(4,ss)))			
    cmd.nkey 	= fix(reform(txt(5,ss)))			
    cmd.nfil 	= fix(reform(txt(6,ss)))
    cmd.critical= reform(txt(7,ss))
    cmd.descr	= reform(txt(8,ss))
    cmd.date_mod= reform(txt(9,ss))
    ;
    column2 = strlowcase(txt(2,*))
    for i=0,nss-1 do begin
	ss2 = where( (txt(0,*) eq 'TC') and (txt(1,*) eq 'FIL') and (column2 eq cmd(i).name), nss2)
	for j=0,nss2-1 do begin
	    cmd(i).fil(j).ifrm	= fix(txt(3,ss2(j)))
	    cmd(i).fil(j).label	= txt(4,ss2(j))
	    cmd(i).fil(j).stbit	= fix(txt(5,ss2(j)))
	    cmd(i).fil(j).length= fix(txt(6,ss2(j)))
	    cmd(i).fil(j).value	= sag_hmi_rd_hk_db_s1(txt(7,ss2(j)))
	    cmd(i).fil(j).descr	= txt(8,ss2(j))
	    cmd(i).fil(j).date_mod = txt(9,ss2(j))
	end

	ss2 = where( (txt(0,*) eq 'TC') and (txt(1,*) eq 'KEY') and (column2 eq cmd(i).name), nss2)
	for j=0,nss2-1 do begin
	    cmd(i).key(j).ifrm	= fix(txt(3,ss2(j)))
	    cmd(i).key(j).label	= txt(4,ss2(j))
	    cmd(i).key(j).stbit	= fix(txt(5,ss2(j)))
	    cmd(i).key(j).length= fix(txt(6,ss2(j)))
	    cmd(i).key(j).low	 = sag_hmi_rd_hk_db_s1(txt(7,ss2(j)))
	    cmd(i).key(j).hi	 = sag_hmi_rd_hk_db_s1(txt(8,ss2(j)))
	    cmd(i).key(j).default= sag_hmi_rd_hk_db_s1(txt(9,ss2(j)))
	    cmd(i).key(j).descr	= txt(10,ss2(j))
	    cmd(i).key(j).date_mod = txt(11,ss2(j))
	end
    end
end
;------------------------------------
cond = {name: '', mnem: '', list: '', rep: 0}	;conditional definition
ss = where( txt(1,*) eq 'COND', nss)
if (nss ne 0) then begin
    cond = replicate(cond, nss)
    cond.name = reform(txt(2,ss))
    for i=0,nss-1 do begin
	tmp = txt(3,ss(i))
	p = strpos(tmp, '=')
	cond.mnem = strmid(tmp, 0, p)
	tmp = strmid(tmp, p+1, 99)
	p = strpos(tmp, '/')
	cond(i).list = strmid(tmp, 0, p)
	cond(i).rep  = fix(strmid(tmp, p+1, 99))
    end
end
;

;------------------------------------
if (n_params(0) ge 1) then tlm2 = tlm	;pass out common block as a param
if (n_params(0) ge 2) then alg2 = alg
if (n_params(0) ge 3) then ylg2 = ylg	; added 21-Nov-07, GAL
if (n_params(0) ge 4) then dsc2 = dsc
if (n_params(0) ge 5) then cmd2 = cmd
if (n_params(0) ge 6) then cond2 = cond
;
if (keyword_set(qdebug)) then idl_speed, 1
end
;-------------------------------------------------------------------
pro go_test_sag_hmi_rd_hk_db, qalph=qalph
;
sag_hmi_rd_hk_db, tlm, alg, dsc, cmd, /qdebug
pause
;
n = n_elements(tlm)
ss = indgen(n)
if (keyword_set(qalph)) then ss = sort(tlm.name)
out = strarr(n)
for i=0L,n-1 do begin
    tlm0 = tlm(ss(i))
    fmt = '(1x, a25, 1x,a3, 1x,a50, 1(1x,z5.5,1x,i3), i3, i3, 1x,a1,1x,a12,1x,a9)'
    out0 = string(tlm0.name+'                  ', tlm0.type, tlm0.descr, $
		tlm0.apid(0), tlm0.stbyte(0), $
;;		tlm0.apid(1), tlm0.stbyte(1), $
		tlm0.stbit, tlm0.nbit, $
		tlm0.conv_code, tlm0.conv, tlm0.unit, format=fmt)
    print, out0
    out(i) = out0
end

prstr, out, file='sag_xhkplot_mnem.full_lst'
end

