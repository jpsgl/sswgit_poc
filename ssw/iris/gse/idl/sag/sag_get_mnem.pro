function sag_get_mnem, sttim, entim, mnem0, $
	raw_value=raw_value, rec=rec, qstop=qstop, qdebug=qdebug, q2stop=q2stop, $
	timarr=timarr, every=every, nostring=nostring, cleanup=cleanup, $
	exp_pkt=exp_pkt, check_exp=check_exp
;+
; $Id: sag_get_mnem.pro 7851 2012-06-12 20:10:10Z linford $
; $Name:  $
;
;NAME:
;	sag_get_mnem
;PURPOSE:
;	To read the HK data files an extract a list of mnemonics
;SAMPLE CALLING SEQUENCE:
;	info = sag_get_mnem(sttim, entim, mnem, /list)
;INPUT:
;	sttim	- The start date/time
;	entim	- The end date/time
;	mnem	- A list of the memonics to return info for
;OPTIONAL KEYWORD INPUT:
;	every	- Can specify to only return every N values.  This is
;		  useful when specifying a long time range.  It must be
;		  less than 1000.
;	nostring - If set, then do not make the output string type (ie:
;		  do not use lookup table for "mnemonic" -- just send the
;		  raw value).  This only applies to digital mnemonics.
;	cleanup	  - If set, then check the data for only good data (ie: remove
;		    data which is from when the DEP is off).
;OUTPUT:
;	info 	- A nested structure with the information
;RESTRICTIONS:
;	* Do not mix mnemonic types unless you are using the /RAW_VALUE switch
;	  (since floating point and string type do not mix well)
;METHOD:
;	An ASCII text file contains the information on where to find the
;	various files.
;HISTORY:
;	Written 23-Sep-99 by M.Morrison using TRACE GET_HK_INFO.PRO
;	 6-Oct-99 (MDM) - Fixed day rollover for time spans of less
;			  than 24 hours
;	28-Aug-02 (MDM) - Expanded hk_dbase_blk1 common block
;	21-Nov-07 (GAL) - Expanded hk_dbase_blk1 common block again, for ylog conv.
;			  Also added cvs tags to header.
;
; $Log: sag_get_mnem.pro,v $
; Revision 1.2  2007/12/04 16:58:32  linford
; Add YSI or ylog conversions
;
;
;-
;
common sag_hk_dbase_blk1, tlm, alg, ylg, dsc, cmd_db, cond
common sag_rd_hk_map_blk1, last_hkmapfil, hkmap
;
if (n_elements(tlm) eq 0) then stat = execute(getenv('SAG_EGSE_DB_RD_PRO'))
if (n_elements(hkmap) eq 0) then sag_rd_hk_map
;
;----- Derive file names
;
;days = timegrid(sttim, entim, days=1, /quiet)
days = timegrid(gt_day(sttim,/str), gt_day(entim,/str), days=1, /quiet)
fids = time2file(days, /date_only)	;YYYYMMDD
if (keyword_set(qdebug)) then prstr, 'FIDS to read: ' + fids, /nomore
;
;---- Check mnemonic names out
;
n = n_elements(mnem0)
if (n eq 0) then return, -1
mnem = strlowcase(strtrim(mnem0,2))
ss = where_arr(mnem, tlm.name, /map_ss)
ss2 = where(ss eq -1, nss2)

if (nss2 ne 0) then begin
    print, 'GET_HK_INFO: Unrecognized mnemonic... ' + mnem0(ss2)
    print, '             Full list was: ' + mnem0
    return, [-1]
end
apids = tlm(ss).apid(0)
uapids = apids(uniq(apids, sort(apids)))
;
;---- Read the HK files
;
str_cmd = '{dummy '
last_dirstr = ''
for iapid=0,n_elements(uapids)-1 do begin
    ss = where(apids eq uapids(iapid))
    mnem2 = mnem(ss)			;the mnemonics which are in the current APID

    ;--- Find out what file to get that data out of

    ss = where(hkmap.apid eq uapids(iapid), nss)
    if (nss eq 0) then begin
	print, 'GET_HK_INFO: APID in HK database is not in the HK "mapping" file '
	print, '             APID was: ', uapids(iapid)
	stop
	return, -1
    end
	
    hkmap1 = hkmap(ss(0))
    dirstr 	= hkmap1.dirs
    ext 	= hkmap1.filext
    prefix	= hkmap1.prefix
    if (dirstr ne last_dirstr) then begin
	sdir = str2arr(dirstr)
	;;for i=0,n_elements(sdir)-1 do if (strmid(sdir(i),0,1) eq '$') then sdir(i) = getenv(strmid(sdir(i),1,99))
	sdir = getenv_dirlist(sdir)
	dirs = get_subdirs(sdir(0))
	for i=1,n_elements(sdir)-1 do dirs = [dirs, get_subdirs(sdir(i))]
	last_dirstr = dirstr
    end
    if (keyword_set(qdebug)) then prstr, 'Directories: ' + dirs
    ;
    infil_arr = file_list2(dirs, fids+ext)
    infil_arr = infil_arr(uniq(infil_arr, sort(infil_arr)))
    if (keyword_set(qdebug)) then prstr, 'Input files: ' + infil_arr
    if (infil_arr(0) eq '') then begin
	    print, 'GET_HK_INFO: Cannot find input files: ', fids+ext
	    print, '             In directories: ', dirs
	    cmd = prefix + 'hkv = 0b'
	    stat = execute(cmd)
    end else begin
	    cmd = prefix + 'hkv = sag_get1mnem(infil_arr, sttim, entim, mnem2, ' + $
			'list=qlist, every=every, raw_value=raw_value, ' + $
			'qdebug=qdebug, err=err, nostring=nostring, ' + $
			'cleanup=cleanup)'
	    if (keyword_set(qdebug)) then print, cmd
	    stat = execute(cmd)

	    if (err eq 0) then begin
		cmd = 'str_name = tag_names(' + prefix + 'hkv, /structure)'
		stat = execute(cmd) 
		str_cmd = str_cmd + ',' + prefix + ':{' + str_name + '}'
	    end else begin
		print, 'ERROR during SAG_GET1MNEM
	    end
    end
end
str_cmd = str_cmd + '}'
;
;---- Build the full up output structure
;
out = -1
if (strpos(str_cmd, ':') ne -1) then begin
    out = make_str(str_cmd)
    ii = 0
    tags = tag_names(out)
    for i=0,n_elements(tags)-1 do begin
	cmd = 'out.' + tags(i)  + ' = ' + tags(i) + 'hkv'
	stat = execute(cmd)
	out.(i).code = out.(i).code + ii
	ii = ii + n_elements(out.(i).code)
    end
end
;
if (data_type(out) eq 8) then hk_time_sort, out		;MDM added 5-Dec-95
;
if (keyword_set(qstop)) then stop
return, out
end

