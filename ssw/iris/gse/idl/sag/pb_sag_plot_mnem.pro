pro pb_sag_plot_mnem, sttim, entim, mnem2, win=win, style=style,  mnem=mnem, $
		clean=clean, yrange=yrange, nolabel=nolabel, info=info, $
		hc=hc, help=help, nover=nover, qdebug=qdebug, $
		continuous=continuous, wait_time=wait_time, $
		text=text, outfil=outfil, pmulti=pmulti, every=every, $
		raw=raw, rd_group=rd_group, rd_full=rd_full, sea=sea, $
		psym=psym, sctherm=sctherm, thick=thick, color=color, data=outdata, nodot = nodot, $
		notyminmax=notyminmax, median_pm=median_pm, sigma=sigma, $
		pm_filter=pm_filter, filter1d=filter1d, last_tim_lab=last_tim_lab, $
		plot_times=plot_times, smoo=smoo
;+
;NAME:
;	sag_plot_mnem
;PURPOSE:
;	To read a list of mnemonics and plot them
;METHOD:
;	If no start/end date/times are passed in, then the current time is
;	used and the default window is 6 hours.
;SAMPLE CALLING SEQUENCES:
;	sag_plot_mnem, '21-sep', '23-sep', 'sxi_debp15mon'
;	sag_plot_mnem, '14-jun 6:30','14-jun 8:00', ['mipwa', 'mipwb'], /hc
;	sag_plot_mnem, sttim, entim, win=win, style=style, mnem=mnem
;	sag_plot_mnem, style=1, /contin
;	sag_plot_mnem, '30-nov-95 20:12', '30-Nov-95 20:20', 'mipwa', /text, outfil='temps.txt'
;	sag_plot_mnem, /cont, mnem='ivcmp5', wait=10, win=0.5
;	sag_plot_mnem, /cont, pmulti=[0,2,2], mnem=['ivaep5', 'ivaep15', 'ivaen15', 'ivcmp5']
;	sag_plot_mnem, /rd_full, sea='volt'
;	sag_plot_mnem, sttim, entim, /rd_group
;	sag_plot_mnem, sttim, entim, mnem, pm_filter=100
;INPUT:
;	sttim	- starting time
;	entim	- ending time
;OPTIONAL KEYWORD INPUT:
;	win	- the window of time to use when no start/end times are 
;		  specified.  Default is 6 hours
;	style	- the style option (an integer value)
;	mnem	- the mnemonics to plot (manually defined)
;	yrange	- Force the plotting range for the y axis
;	continous - Loop through continuously.  If the letter "h" is hit
;		  while running continuously, a hardcopy is made.  Any other
;		  key will exit the continuous wait.  Default wait is 60 sec.
;	wait	- How much time to wait between real time plotting. Default 
;		  is 60 seconds.
;	text	- If set, then show the mnemonic values in ASCII form to the
;		  IDL terminal
;	outfil	- If /TEXT option is used, the results can also be saved to
;		  a file.
;	every	- If set, only read every "every" data points in the HK
;		  file
;	raw	- If set, then do not convert to engineering units
;	pmulti	- Optionally make several plots per page.
;	rd_group - If set, then the user will be prompted by Xwidget for
;		  the list of mnemonics from the group list 
;		  specified in $MDI_CAL_INFO/xhkplot_mnem.lst
;	rd_full	- If set, then the user will be prompted by Xwidget for
;		  the full mnemonics list 
;	sea	- Used with the /RD_FULL option to specify a string to
;		  search through and reduce the mnemonic list
;	pm_filter - The value to search for valid data, ie: pm_filter=100
;		  will only plot data between -100 and 100 ("pm" is plus/
;		  minus)
;	filter1d- Remove bad data by finding where values exceed the
;		  "smoothed 100" values by more than 3 sigma
;	plot_times- If set, then set the plot range to be the time span
;		  requested
;HISTORY:
;	Written 23-Sep-99 by M.Morrison using QUICK_HKPLOT as the start
;	3-Jan-02 (MDM) - Added smoo keyword
;-
;
top:
;
if (keyword_set(mnem2)) then mnem = mnem2
if (keyword_set(rd_group)) then mnem = get_hk_mnem(1)
if (keyword_set(rd_full)) then mnem = get_hk_mnem(0, sea=sea)
if (keyword_set(rd_group) or keyword_set(rd_full)) then if (mnem(0) eq '') then return
if (n_elements(psym) eq 0) then psym = 0
;
if (n_elements(style) eq 0) then style = 1
if (keyword_set(mnem)) then style = 0	;MDM 29-Nov-95
if (n_elements(win) eq 0) then win = 6		;hours
if (keyword_set(text)) then begin
    if (n_elements(sttim) eq 0) then sttim = ut_time()
    if (n_elements(entim) eq 0) then entim = anytim2ints(sttim, off=15)
end
if (n_elements(sttim) eq 0) then sttim = anytim2ints(ut_time(), off=-60.*60.*win)
if (n_elements(entim) eq 0) then entim = anytim2ints(sttim, off=60.*60.*win)
;
sttim = fmt_tim(sttim)
entim = fmt_tim(entim)
;
save_psym = !psym
!psym = psym
if (keyword_set(hc)) then begin
    save_dev = !d.name
    set_plot, 'ps
    device, /land,/color
end
;
;
n = n_elements(mnem)
print, 'Reading: ', mnem
print, 'Time range: ', sttim, ' to ', entim
info = sag_get_mnem(sttim, entim, mnem, clean=clean, exp_pkt=exp_pkt, qdebug=qdebug, $
				/nostring, every=every, raw=raw)
if (data_type(info) ne 8) then begin
    print, 'Read of the database failed.  Returning......'
    return
end

if KEYWORD_SET(nodot) then qlinestyle = 0 else qlinestyle = 1
if KEYWORD_SET(sctherm) then begin
	pp0 = 3501968.76649d
	pp1 = -7585.43141405d
	pp2 = 6.57112688373d
	pp3 = -0.00284557656829d
	pp4 = 6.15963011894d-7
	pp5 = -5.33183667844d-11
	infoname = TAG_NAMES(info)
	for j = 0, N_TAGS(info)-1 do begin
		substr = info.(j)
		nstr = CREATE_STRUCT('daytime', substr.daytime, 'value', float(substr.value), 'mnem', substr.mnem, $
			'descr', substr.descr, 'code', substr.code, 'flag', substr.flag)
		numvals = N_ELEMENTS(substr.mnem)
		for i = 0, numvals-1 do begin
			mnemcheck = STRMID(STRUPCASE(substr.mnem[i]),11,3)
			thisdata = DOUBLE(substr.value[*,i])			
			if mnemcheck eq 'TSC' then begin
				newdata = pp0 + pp1 * thisdata + pp2 * thisdata^2 + pp3 * thisdata^3 + $
					pp4 * thisdata^4 + pp5 * thisdata^5
			endif else begin
				newdata = thisdata
			endelse
			info.(j).value[*,i] = newdata
			nstr.value[*,i] = newdata
		endfor
		if j eq 0 then ninfo = CREATE_STRUCT(infoname[j], nstr) else ninfo = CREATE_STRUCT(ninfo, infoname[j], nstr)
	endfor
	info=ninfo
endif
if not KEYWORD_SET(thick) then thick=1
if not KEYWORD_SET(color) then color=0
outdata=FLTARR(N_ELEMENTS(info.(0).daytime), N_ELEMENTS(info.(0).mnem)+2)
outdata[*,0] = info.(0).daytime.day
outdata[*,1] = info.(0).daytime.time
outdata[*,2:N_ELEMENTS(info.(0).mnem)+1] = info.(0).value
;
if (keyword_set(text)) then begin
    times = info.(0).daytime
    if (n_elements(times) le 4) then out = hkplot_info(info, times=times, /list) $
				else out = hkplot_info(info,times=[sttim, entim])

    prstr, out, /nomore
    if (keyword_set(outfil)) then prstr, out, file=outfil
    return
end
;
if (keyword_set(plot_times)) then timerange = [anytim2ints(sttim), anytim2ints(entim)]
;
if (keyword_set(pmulti)) then begin
    if (n_elements(pmulti) gt 1) then !p.multi = pmulti else !p.multi = [0,1,n]
    for i=0,n-1 do begin
        npp = (!p.multi(1) * !p.multi(2) )>1
	qnover = ((i mod npp) ne npp-1) and (i ne n-1)
        pb_sag_hkplot, info, yrange=yrange, codelist=i, $
                nover=qnover, label=(1-keyword_set(nolabel)), $
		notyminmax=notyminmax, median_pm=median_pm, sigma=sigma, $
		pm_filter=pm_filter, filter1d=filter1d, $
		last_tim_lab=last_tim_lab, timerange=timerange, smoo=smoo, color=color, thick=thick
    end
end else begin
    pb_sag_hkplot, info, yrange=yrange, label=(1-keyword_set(nolabel)), $
		qlinestyle = qlinestyle, nover=nover, $
		notyminmax=notyminmax, median_pm=median_pm, sigma=sigma, $
		pm_filter=pm_filter, filter1d=filter1d, $
		last_tim_lab=last_tim_lab, timerange=timerange, smoo=smoo, color=color, thick=thick
end
;
if (keyword_set(hc)) then begin
    ;;pprint		;already done in NEW_HKPLOT!
    set_plot, save_dev
	!psym = save_psym
	return
end
;
;----- Logic to run continuously
;
if (keyword_set(continuous) or keyword_set(wait_time)) then begin
    if (n_elements(wait_time) eq 0) then wait_time = 60		;wait 60 seconds
    redo_tim = systime(1) + wait_time
    qdone = 0
    print, 'Waiting', format='($,a)'
    while (not qdone) do begin
	kb_inp = strlowcase(get_kbrd(0))
	case kb_inp of
	    '': wait,1		;nothing typed
	    'h': pb_sag_plot_mnem, sttim, entim, style=style, mnem=mnem, $
		clean=clean, yrange=yrange, nolabel=nolabel, $
		/hc, pmulti=pmulti, /color
	    else: qdone =1
	endcase
	if (not qdone) then print, '.', format='($,a)'
	if (not qdone) and (systime(1) ge redo_tim) then begin
	    delvarx, sttim
	    delvarx, entim
	    print, ''
	    goto, top		;sorry for the after thought
	end
    end
    print ,''
end

!psym = save_psym
end
