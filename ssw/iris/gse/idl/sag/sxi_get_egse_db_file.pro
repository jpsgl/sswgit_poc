function sxi_get_egse_db_file, prefix
;+
;NAME:
;	sxi_get_egse_db_file
;PURPOSE:
;	Return the filename of the current database file
;SAMPLE CALLING SEQUENCE:
;	infil = sxi_get_egse_db_file(getenv('SXI_EGSE_DB_PREFIX'))
;HISTORY:
;	Written 23-Sep-99 by M.Morrison
;	 7-Jul-00 (MDM) - Moved SXI_EGSE_DB_FILE override from sag_sxi_rd_hk_db.pro
;-
;
if (n_elements(prefix) eq 0) then prefix = ''
;
ff = file_list('$GSE_DATABASE_DIR', prefix+'*')
infil = ff(n_elements(ff)-1)
;
tmp = getenv('SXI_EGSE_DB_FILE')
if (keyword_set(tmp)) then infil = tmp
;
return, infil
end