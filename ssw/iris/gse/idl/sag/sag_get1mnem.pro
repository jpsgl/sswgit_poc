function sag_get1mnem, infil_arr, sttim, entim, mnem, list=list, $
	raw_value=raw_value, rec=rec, qstop=qstop, qdebug=qdebug, q2stop=q2stop, $
	every=every, err=err, nostring=nostring, cleanup=cleanup, $
	append=append
;+
;$Id: sag_get1mnem.pro 10014 2012-09-20 23:06:38Z sabolish $
;$Name:  $
;
;NAME:
;	sag_get1mnem
;PURPOSE:
;	To return information from the HK database given a time and
;	a mnemonic.  Should have one call per packet APID
;	Use SAG_GET_MNEM as the front end.
;INPUT:
;	infil_arr- The array of filenames to read and extract from
;	sttim	- The start date/time
;	entim	- The end date/time
;	mnem	- A list of the memonics to return info for
;OPTIONAL KEYWORD INPUT:
;	every	- Can specify to only return every N values.  This is
;		  useful when specifying a long time range
;	nostring - If set, then do not make the output string type (ie:
;		  do not use lookup table for "mnemonic" -- just send the
;		  raw value).  This only applies to digital mnemonics.
;	cleanup	- If set, then check the data for only good data (ie: remove
;		  data which is from when the DEP is off).
;OUTPUT:
;	info 	- A structure with the following tags:
;			.time	- N element array of times
;			.day	- N element array of days
;			.value	- NxM array of values
;			.mnem	- M array of mnemonic names
;			.descr	- M array of short description
;RESTRICTIONS:
;	* The list of mnemonics must be all from the same APID packet
;	* Do not mix mnemonic types unless you are using the /RAW_VALUE switch
;	  (since floating point and string type do not mix well)
;HISTORY:
;	Written 23-Sep-99 by M.Morrison using TRACE GET_1HK_INFO.PRO as a start
;	 5-Oct-99 (MDM) - Fixed case where input file exists, but is zero bytes long
;	31-Mar-00 (MDM) - Put in protection for when map doesn't match data
;	24-Sep-01 (MDM) - Added protection that the read worked properly when
;			  the first data value is -1
;	 9-May-02 (MDM) - Added GSE,COND (conditional telemetry) logic
;	18-Sep-07 (GAL)	- Added the hook to process SDO subsec res. data via
;			  environmental var.'SAG_HK_TIME_USE=SDO_TIME' which can be
;			  set via setenv,'SAG_HK_TIME_USE=SDO_TIME' on the fly.
;			  Also added the cvs tags for id, name, and logging.
;			  idl> set_logenv,'SAG_HK_TIME_USE','SDO_TIME'
;			  Note: valid values of 2nd arg: 
;			  EGSE     for EGSE receipt time
;			  INST     for Observatory or instrument time
;			  SDO_TIME for sub second Observatory/Instrument time
;	21-Nov-07 (GAL) - Expanded hk_dbase_blk1 common block again, for ylog conv.
;	05-May-09 (GAL) - Pass qdebug switch to sag_ext_mnem.
;   	24-Nov-09 (GAL) - Added the hook to process SUVI OBC times via environmental
;   	    	    	    var. 'SAG_HK_TIME_USE=SUV_TIME' which can be set on the
;   	    	    	    fly.  Also use second time field for EGSE time egse_time 
;   	    	    	    and egse_usec. Which has a new name "egse_usec" instead 
;   	    	    	    of "egse_sec", because it is defined to be microsec since
;   	    	    	    last second.
;
;	06-Nov-13 (DSS) - Added case of HK_TIME_USE for ptime, which is the time array used used to search for data.
;			  If 'IRI_TIME' then search will use packet time instead of egse time.
;
;
;$Log: sag_get1mnem.pro,v $
;Revision 1.6  2009/05/06 17:24:47  linford
;Passed the qdebug switch to sag_ext_mnem calls
;
;Revision 1.5  2008/07/03 19:32:50  linford
;fix a bug with the INST time use at 1 sec resol., now use map.inst_time
;
;Revision 1.4  2007/12/04 16:58:32  linford
;Add YSI or ylog conversions
;
;Revision 1.3  2007/09/18 22:42:55  linford
;added cvs tags id,name,log, and description on how to enable subsec processing of packet data
;
;
;-
;
common sag_hk_dbase_blk1, tlm, alg, ylg, dsc, cmd_db, cond
common sag_rd_hk_map_blk1, last_hkmapfil, hkmap
;
if (n_elements(tlm) eq 0) then stat = execute(getenv('SAG_EGSE_DB_RD_PRO'))
if (n_elements(hkmap) eq 0) then sag_rd_hk_map

break_file, infil_arr, dsk_log, dir, filnam, ext
;
err = 1
ss = where_arr(mnem, tlm.name, /map_ss)
;
;-------------------
;
iepoch = getenv('SAG_INSTRUMENT_EPOCH')
eepoch = getenv('SAG_EGSE_EPOCH')
m = n_elements(mnem)
;
out = -1
block = 1000		;do this number of records at a time
nfil = n_elements(infil_arr)
qfirst = 1
for ifil=0,nfil-1 do begin
  infil0 = infil_arr(ifil)
  map = sag_rd_hk(infil0, /map)			;read the roadmap
  if (data_type(map) eq 8) then begin
    	case getenv('SAG_HK_TIME_USE') of
		'IRI_TIME':	ptime=map.inst_time
		else:	ptime = map.egse_time				;packet time
	endcase
;;;replaced in case above    ptime=map.egse_time
if (ifil eq 0) then begin
    	case getenv('SAG_HK_TIME_USE') of
		'IRI_TIME':	dectim = int2secarr(sttim, iepoch)
		else:	dectim = int2secarr(sttim, eepoch)
	endcase
;;;replaced in case above    	dectim = int2secarr(sttim, eepoch)
dummy = min( abs(ptime-dectim), strec)	;find the closest record
	if (ptime(strec) lt dectim) then strec=(strec+1)<(n_elements(map)-1)
    end else begin
	strec = 0
    end	
    ;
    if (ifil eq nfil-1) then begin
    	case getenv('SAG_HK_TIME_USE') of
		'IRI_TIME':	dectim = int2secarr(entim, iepoch)
		else:	dectim = int2secarr(entim, eepoch)
	endcase
;;;replaced in case above    dectim = int2secarr(entim, eepoch)
dummy = min( abs(ptime-dectim), enrec)	;find the closest record
	if (ptime(strec) gt dectim) then enrec=(enrec-1)>0
    end else begin
	enrec = n_elements(map)-1
    end
    ;
    strec = long(strec)
    for strec2=strec,enrec,block do begin
	enrec2 = (strec2+block-1) < enrec
	if (keyword_set(qdebug)) then print, 'Reading ', infil0, ' records ', strec2, enrec2
	rec = sag_rd_hk(infil0, strec2, enrec2, cleanup=cleanup)		;read those records
	;stop,'stop in get1mnem'
	
	if (data_type(rec) eq 8) then begin
	    if (keyword_set(every)) then begin
		nout_every = (n_elements(rec) / every) > 1
		ss_every = indgen( nout_every ) * every
		rec = rec(ss_every) 
	    end
	    ;	Looks like EGSE time and Inst time or other time is 1s time resolution.
	    case getenv('SAG_HK_TIME_USE') of
		'EGSE': timarr0  = anytim2ints(eepoch, off=double(map(strec2:enrec2).egse_time))
		'SDO_TIME':	begin
						sdo_time_extract,rec,sec,fsec,/qdebug
						timarr0 = sdo_time_conv('INSTRUMENT',sec,fsec)
				end
		'IRI_TIME':	begin
						;sdo_time_extract,rec,sec,fsec,/qdebug
						timarr0 = iri_time_conv('INSTRUMENT',rec.pkt_head.time,rec.pkt_head.ftime)
				end
		'SUV_TIME': 	begin
		    	    	    	    	suv_time_extract,rec,obcday,obcms,obcus,/qdebug
						timarr0 = suv_time_conv('INSTRUMENT',obcday,obcms,obcus)
				end
		else:   begin
			;	timarr0  = anytim2ints(iepoch, off=double(rec.pkt_head.time))
				timarr0  = anytim2ints(iepoch,off=double(map(strec2:enrec2).inst_time))
				;stop,'help,rec structure'
			end
	    endcase
	    if (n_elements(timarr0) ne n_elements(rec)) then begin
		print, 'SAG_GET1MNEM:  WARNING.. Index map and data dont match'
		tbeep, 3
		ntrunk = n_elements(timarr0) < n_elements(rec)
		timarr0 = timarr0(0:ntrunk-1)
		rec = rec(0:ntrunk-1)
	    end
	    ;
	    data_ref = sag_ext_mnem(rec(0).data, mnem(0), 'HK', raw_value=raw_value, nostring=nostring, /init, qdebug=qdebug)
	    out0 = replicate(data_ref, n_elements(rec), m)
	    ss_cond = 0
	    for imnem=0,m-1 do out0(0, imnem) = sag_ext_mnem(rec.data, mnem(imnem), 'HK', raw_value=raw_value, $
			nostring=nostring, ss_cond=ss_cond, qdebug=qdebug )
	    if (keyword_set(ss_cond)) then begin
		out0 = out0(0:n_elements(ss_cond)-1, *)
		timarr0 = timarr0(ss_cond)
	    end
	    ;
	    if (qfirst) then begin
		out = temporary(out0) 
		timarr = temporary(timarr0)
	    end else begin
	        out = [out, temporary(out0)]
	        timarr = [timarr, temporary(timarr0)]
	    end
	    qfirst = 0
	end else begin
	    print, 'SAG_GET1MNEM: Problem reading ' + infil0
	    print, 'Result is not a structure...
	end
    end
  end
end
;
out2 = -1
qok = 1
if (data_type(out) ne 7) then qok = (out(0) ne -1) or (n_elements(out) gt 1)
if  (qok) then begin		;found something
    if (keyword_set(append)) then begin
	timarr = [append.daytime, timarr]	;append previous result
	out = [append.value, out]
    end
    ;
    n = n_elements(timarr)
    m = n_elements(mnem)
    cmd = '{dummy, daytime: replicate({anytim2ints}, ' + strtrim(n,2)        + '), ' + $
		'value: '         + fmt_tag(size(out))  + ' , ' + $
		'mnem:   strarr(' + strtrim(m,2)        + '), ' + $
		'descr:  strarr(' + strtrim(m,2)        + '), ' + $
		'code:   lonarr(' + strtrim(m,2)        + '), ' + $
		'flag:   lonarr(' + strtrim(m,2)        + ')}'
    if (keyword_set(qdebug)) then print, cmd
    out2 = make_str(cmd)
    out2.daytime.time = timarr.time
    out2.daytime.day  = timarr.day
    out2.value = out
    out2.mnem = mnem
    out2.code = indgen(m)
    ;
    for i=0,m-1 do begin
	ss = where_arr(tlm.name, mnem(i), count)
	if (ss(0) ne -1) then out2.descr(i) = tlm(ss(0)).descr
    end
    err = 0
end else begin
    if (keyword_set(append)) then return, append
end
;
if (keyword_set(qstop)) then stop
return, out2
end

