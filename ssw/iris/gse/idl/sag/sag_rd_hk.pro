function sag_rd_hk, infil0, strec0, enrec0, head=head, only_head=only_head, map=map, $
		struct_head=struct_head, new=new, qdebug=qdebug, sci=sci, $
		cleanup=cleanup, qstop=qstop, simple=simple, hkmapfil=hkmapfil, $
		rdmapfil=rdmapfil
;+
;$Id: sag_rd_hk.pro 5666 2012-02-23 18:55:23Z sabolish $
;
;$Name$
;
;NAME:
;	sag_rd_hk
;PURPOSE:
;	To directly read Lockheed Martin Solar and Astrophysics Dept (SAG)
;	EGSE HK file
;SAMPLE CALLING SEQUENCE:
;	out   = sag_rd_hk(infil)
;	out   = sag_rd_hk(infil, head=head)
;	out   = sag_rd_hk(infil, strec, enrec, head=head)
;	dummy = sag_rd_hk(infil, head=head, /only_head)
;	head  = sag_rd_hk(infil, /only_head)
;	out   = sag_rd_hk('./data/19990922.0x0010', 0, 100)
;INPUT:
;	infil	- Input file to read
;	strec	- Starting record number to read (0 to N-1)
;	enrec	- Ending record to read (0 to N-1)
;OUTPUT:
;	out	- 
;OPTIONAL KEYWORD OUTPUT:
;	head	- Header/map structure
;OPTIONAL KEYWORD INPUT:
;	only_head - If set, then just read the header, and return it
;		    as the function return.
;DESCRIPTION:
;	Environmental variables used
;		SAG_HK_MAP_FILE 	- Tabular file saying the sizes
;					  of the different APIDs and their
;					  directory locations
;					  (eg: /sxisw/dbase/cal/info/sxi_hkmap_v001.tab)
;		SAG_EGSE_EPOCH		- Epoch for seconds past
;					  (eg: '1-Jan-70')
;		SAG_INDEX_STRUCT	- The name of the routine which has
;					  the index structure
;					  (eg: sag_sxi_pkt_struct('INDEX_MAP') )
;		SAG_DATA_STRUCT		- The name of the routine which has
;					  the data structure
;					  (eg: sag_sxi_pkt_struct(extens) )
;		SAG_TIME_USED		- What time to use on the output structure time
;HISTORY:
;	Written 22-Sep-99 by M.Morrison using TRACE RD_HK.PRO
;	 1-Dec-99 (MDM) - Modified to not read the index/map unless requested
;	18-Oct-00 (MDM) - Put in protection for zero block file
;   	13-Oct-09 (GAL) - Update the code to do byte-swap correctly for Solaris-Intel
;   	    	    	    platform.
;   	24-Nov-09 (GAL) - Added subver keywords for checkin for SUVI.
;	21-Feb-10 (GAL) - Update to code for byte-swap on Mac OS intel machines.
;
;-
;
common sag_rd_hk_map_blk1, last_hkmapfil, hkmap
if (n_elements(hkmap) eq 0) then sag_rd_hk_map
;
;------ Assemble information about what is being read
;
if (not file_exist(infil0)) then begin
    print, 'SAG_RD_HK: Input file does not exist: ' + infil0
    return, 0b
end
;
break_file, infil0, dsk_log, dir, filnam, extens, /last_dot
;
ss = where(hkmap.filext eq extens, nss)
if (nss eq 0) then begin
    stop, 'Unrecognized filename extension: ' + infil0
end
;
hkmap1 = hkmap(ss(0))
nn 	= hkmap1.size
step_def = hkmap1.def_cad
iqual = hkmap1.iqual
index_cmd = hkmap1.index_cmd
if (keyword_set(qdebug)) then begin
    print,'Index call command: ',index_cmd
    print,'Possible input file name: ', filnam
end
;
infil = infil0
out0 = 0L
;
; ---- Do byte swapping for non-SGI machines, updated by G.Linford
machos = !version
qswap = is_member(/swap_os)
if ((qswap eq 0) and (machos.arch eq 'x86_64') and (machos.os eq 'sunos')) then begin
    qswap = 1
end
; Add check for MAC OS, intel 
if ((qswap eq 0) and (machos.arch eq 'x86_64') and (machos.os eq 'darwin')) then begin
	qswap = 1
end
; OLD byte-swap code
;swap_os=['vms','ultrix','OSF']                  ; list of swap machines
;chk=where(!version.os eq swap_os,nscount)       ; current OS in noswap list?
;qswap = nscount ne 0                            ; assign boolean
;
;------------------ Reading the map file
;
cmd = 'head0 = ' + index_cmd
stat = execute(cmd)
if (keyword_set(struct_head)) then return, head
;
if (keyword_set(map)) or (keyword_set(only_head)) then begin	;MDM 1-Dec-99
    map_infil = infil + 'x'
    openr, lun, map_infil, /get_lun
    finfo = fstat(lun)
    nrec = finfo.size / get_nbytes(head0)
    if (nrec le 0) then begin
	print, 'SAG_RD_HK: Input file map has no data: ' + map_infil
	free_lun, lun
	;stop, 'SAG_RD_HK: nrec of map/index file is zero'
	return, 0
    end
    head = replicate(head0, nrec)
    readu, lun, head
    if (qswap) then dec2sun, head
;---@@@ DSS added below byteorder calls to work on RHEL with the Hammers iris archiver for ITOS
	ttt=head.egse_time
	ttt2=head.egse_ftime
	byteorder, ttt, /lswap, /swap_if_little_endian
	byteorder, ttt2, /lswap, /swap_if_little_endian
	head.egse_time=ttt
	head.egse_ftime=ttt2
    free_lun, lun
    if (keyword_set(map)) then return, head
    if (keyword_set(only_head)) then return, head
end
;
;------------------ Reading the data file
;
cmd = 'data0 = ' + getenv('SAG_DATA_STRUCT')
stat = execute(cmd)
;
openr, lun, infil, /get_lun
finfo = fstat(lun)
nrec = finfo.size / get_nbytes(data0)
if (nrec le 0) then begin
	print, 'SAG_RD_HK: Input file map has no data: ' + infil
	free_lun, lun
	return, 0
end
;
if (n_elements(strec0) eq 0) then strec = 0 else strec = strec0
if (n_elements(enrec0) eq 0) then enrec = nrec-1 else enrec = enrec0
strec = long( strec > 0 < (nrec-1) )
enrec = long( enrec > 0 < (nrec-1) )
;
nout = (enrec - strec + 1) >1
out = replicate(data0, nout)
;
off_head = 0L
skip = strec * get_nbytes(data0) + off_head
point_lun, lun, skip
if (keyword_set(qdebug)) then print, 'Byte pointer offset to: ', skip
;
readu, lun, out				& if (qswap) then dec2sun, out
if (keyword_set(simple)) then return, out
;
if (keyword_set(cleanup)) and (data_type(out) eq 8) and (iqual ne -1) then begin
    case cleanup of
	1: ss = where((out.data(iqual) ne 255) and (out.hk(iqual) ne 0), nss)
	2: ss = where((out.time gt 631152000) and (out.time lt 915148800), nss)
		;check that the time is between 1-Jan-90 and 1-Jan-99
	3: ss = where((out.hk(iqual) ne 255) and (out.hk(iqual) ne 0) and $
			(out.time gt 631152000) and (out.time lt 915148800), nss)
	else: nss=0
    endcase
    if (keyword_set(qdebug)) then begin
	print, iqual
	pmm, out.hk(iqual)
    end
    if (nss eq 0) then begin
	if (keyword_set(qdebug)) then print, 'RD_HK: No good quality data for ' + infil
	out = -1
    end else begin
	out = out(ss)
    end
end
;
if (keyword_set(qstop)) then stop
free_lun, lun
return, out
end
