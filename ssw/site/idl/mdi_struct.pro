function mdi_struct_temp, number, version=version, $
	oldversion=olversion, update=update
;+
;   Name: mdi_struct_temp
;
;   Purpose: return mdi_structure 
;
;   Input Parameters:
;      number (optional) - number structures returned  - default is one  
;  
;
;   Calling Sequence:
;      str=mdi_struct_temp( [number] )
;
;   History:
;      15-jan-1997 - from 'eit_struct.pro' as an illustration
;-

version=1

common	mdi_struct_temp_blk, str, catstr

if keyword_set(oldversion) then version=oldversion

if n_elements(str) eq 0 then 					$
   case version of 

   1: str={							$

        version:1,						$

;	----------- fits ----------------------------
	simple:'', bitpix:0b, 					$
        naxis:3,naxis1:0,naxis2:0,naxis3:0,			$
        bscale:0., bzero:0., bunit:'',				$

;       ----------- soho ----------------------------
;
        date:'', mjd:0l, day:0, time:0l,			$
        time_obs:'',date_obs:'',				$
	filename:'',				                $
        origin:'', telescop:'', instrume:'', object:'',		$
        sci_obj:'', obs_prog:'',				$

;       -------- pointing -----------------------
	ctype1:'',  ctype2:'', ctype3:'',			$
	crpix1:0.,  crpix2:0., crpix3:0.,  			$
        crval1:0.,  crval2:0., crval3:0., 		        $
        cdelt1:0.,  cdelt2:0., cdelt3:0.,			$
        solar_r:0., solar_b0:0.,		                $

;	----------- mdi specific  -----------------------------
        comment:strarr(100)} 		;,history:strarr(30)}
    else: message,/info,"Unexpected version number: " + strtrim(version,2)
endcase

outstr=str

if n_elements(number) gt 0 then outstr=replicate(outstr,long(number))

return,outstr
end


