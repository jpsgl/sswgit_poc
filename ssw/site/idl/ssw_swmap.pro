pro ssw_swmap, mapfile=mapfile, all=all, gen=gen
;
;+
;   Name: ssw_swmap
;
;   Purpose: generate IDL procedure map based on !path order
;
;   History:
;      17-aug-1996 - S.L.Freeland (based on earlier routine of same name)
;      23-oct-1996 - S.L.Freeland add /GEN keyword and functions
;      28-oct-1996 - S.L.Freeland SSW_GEN_SETUP->SSW_SETUP
;                    call strrep_logenv(xx,'IDL_DIR')
;       4-nov-1996 - S.L.Freeland - use file_list,/cd instead of findfile
;      24-feb-1997 - S.L.Freeland - add call to "ssw_packages"
;       9-apr-1997 - S.L.Freeland - add path +$SSW/smm
;      10-apr-1997 - S.L.Freeland - add XRAY package
;      12-apr-1997 - S.L.Freeland - add mission HESI,CGRO, packages GOES,SPEX
;
;  Restrictions:
;      UNIX only (but just barely)
;-
deffile=concat_dir('SSW_SITE_SETUP','ssw_map.dat')
genfile=concat_dir('SSW_SETUP','ssw_map.dat')
if not keyword_set(mapfile) then mapfile=deffile        ; default is SITE

filter=(['*.pro','*'])(keyword_set(all))		; .pros=default

; -------- "Non standard" path additions --------
ssw_path,'$SSW/smm'
ssw_path,'$SSW/hesi'
ssw_path,'$SSW/cgro'
ssw_packages,/chianti,/append
ssw_packages,/ztools,/append
ssw_packages,/xray,/append
ssw_packages,/goes
ssw_packages,/spex
; ------------------------------------------------

paths=str2arr(!path,':')
npaths=n_elements(paths)

message,/info,"Number of paths to include: " + strtrim(npaths,2)

routines=''
for i=0,npaths-1 do routines=[routines,file_list(paths(i),filter,/cd)]

sswnames=strrep_logenv(routines(1:*),'SSW')  ; convert local trans to $SSW
sswnames=strrep_logenv(sswnames,'IDL_DIR')   ; local names via $IDL_DIR

file_append,mapfile,sswnames,/new            
if keyword_set(gen) then file_append,genfile,sswnames,/new

return
end
