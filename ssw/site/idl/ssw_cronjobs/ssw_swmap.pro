pro ssw_swmap, mapfile=mapfile, all=all, gen=gen
;
;+
;   Name: ssw_swmap
;
;   Purpose: generate IDL procedure map based on !path order
;
;   History:
;      17-aug-1996 - S.L.Freeland (based on earlier routine of same name)
;      23-oct-1996 - S.L.Freeland add /GEN keyword and functions
;
;  Restrictions:
;      UNIX only (but just barely)
;-
deffile=concat_dir('SSW_SITE_SETUP','ssw_map.dat')
genfile=concat_dir('SSW_GEN_SETUP','ssw_map.dat')
if not keyword_set(mapfile) then mapfile=deffile        ; default is SITE

filter=(['*.pro','*'])(keyword_set(all))		; .pros=default

paths=str2arr(!path,':')
npaths=n_elements(paths)

message,/info,"Number of paths to include: " + strtrim(npaths,2)

routines=''
for i=0,npaths-1 do routines=[routines,findfile(concat_dir(paths(i),filter))]

sswnames=strrep_logenv(routines(1:*),'SSW')  ; convert local trans to $SSW
file_append,mapfile,sswnames,/new
if keyword_set(gen) then file_append,genfile,sswnames,/new

return
end
