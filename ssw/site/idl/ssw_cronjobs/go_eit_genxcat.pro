; SLF - 15-mar-1997 - installed cron job to generate GENX catalog
; SLF - 18-mar-1997 - add call to 'mk_lasteit_movie'
;

ssw_path,'$SSW/site/idl_test',/prepend 
eit_genx_cat,/generate,last=10         ; last = most recent NN days

mk_lasteit_movie, nimg=30, outsize=280, /rename
mk_lasteit_movie, nimg=30, outsize=280, /mpeg

end
