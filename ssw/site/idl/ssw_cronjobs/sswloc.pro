pro sswloc, pattern, matches, nmatch, $
	mapfile=mapfile, quiet=quiet, refresh=refresh, $
	_extra=_extra
;+
;
;   Name: sswloc
;
;   Purpose: use SSW mapfile to see online SSW routines
;
;   Input Paramters:
;      pattern - pattern to match (if not defined, all routines returned)
;
;   Output Paramters
;      matches - full SSW pathname of matches
;      nmatch  - number of matches
;
;   Keyword Parameters:
;      mapfile - optional mapfile (default=$SSW/site/setup/ssw_map.dat)
;      quiet   - if set, dont print (ex: called by programs)
;      refresh - if set, re-read mapfile (default is only read 1st call)
;   
;   Calling Sequence:
;      sswloc,'pattern', matches
;-
common sswloc_blk, swmap

defmap=concat_dir('$SSW_SITE_SETUP','ssw_map.dat')

if not keyword_set(mapfile) then mapfile=defmap   
readmap=n_elements(contents) eq 0 or (mapfile ne defmap) or keyword_set(refresh) 

if readmap then begin
   map=rd_tfile(mapfile)
   if mapfile eq defmap then swmap=map
endif else map=swmap

case 1 of
   keyword_set(pattern): 
   data_chk(_extra,/struct): pattern=(tag_names(_extra))(0)
   else: pattern=""
endcase



ss=wc_where(map,'*'+pattern+ '*',nmatch,/ignore_case)

if nmatch eq 0 then begin
   matches='' 
   mess="No SSW routines matching pattern: " + pattern
endif else begin
   matches=map(ss)
   mess=["Matches...","   " + map(ss(0:10<nmatch-1))]
   mess=[mess, (["", "   (..." + strtrim(nmatch-10,2) + " more...)"])(nmatch gt 10)]
endelse

if not keyword_set(quiet) then prstr,mess(where(mess ne ""))

return
end

