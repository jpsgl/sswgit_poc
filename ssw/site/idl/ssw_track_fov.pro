pro ssw_track_fov, index, data, 		$
                     iout, dout, 		$
                     ref_helio=ref_helio, 	$
                     ref_date=ref_date, 	$
                     outsize=outsize, 		$
                     debug=debug,arcsec=arcsec
;+
; NAME:
;    ssw_extract_fov
;
; PURPOSE:
;    Extract a sub-field from the SSW-compliant (2D) image data
;    based on the reference time and reference coordinates.
; CALLING SEQUENCE:
;    ssw_align_fov, index, data,
;        helio=[-9.8, -20.3], date_helio='14-JUN-92 02:37:41', $
;        outsize=[128, 64]
;
; INPUTS:
;    index      - SSW-compliant index structure.
;    data       - 2 or 3D data cube.
;    ref_helio  - Reference latitude and longitude for the alignment.
;    ref_date   - Reference date/time for the heliocentric input.
;                 (This can be an index structure.)
;    outsize    - Dimension of output image in pixels.
;    arcsec     - set for reference in arsecs instead of lon/lat.
;
; OUTPUTS:
;    index      - modified index header.
;    data       - extracted/aligned data array.  Will be same resolution
;                 as input data.
;
; RESTRICTIONS:
;    This routine relies on getting an index structure passed to it.  
;
; METHOD:
;    Calculates sub-region from ref_helio and ref_date and then 
;    passes the correct sub-region to <extract_arr.pro>.
;
; HISTORY:
;       10-sep-96 MDI_TRACK_FOV Written by G.L. Slater
;       22-Oct-96 - (BNH) - Shamelessly stolen and butchered from GLS
;       16-jan-97 S.L.Freeland (eit/mdi compliant->online)
;       16-sep-97 J. Newmark - added arcsec keyword, handle if off_limb
;
;-
num_images = n_elements(index)
iout = index
;
;  There must be a cool way to do this
;
dout = fltarr(outsize(0), outsize(1), num_images)

; Get the reference time
if (data_chk(ref_date, /struct)) then $
    ref_time = fmt_tim(ref_date) $		; It was an index structure
else $
   ref_time = ref_date				; It was something else

off_limb = 0
if keyword_set(arcsec) then begin
    helio = shift(arcmin2hel(ref_helio(0)/60.,ref_helio(1)/60.,$
               date=ref_date,off_limb=off_limb,/soho),1)
    if not off_limb(0) then ref_helio = helio
endif

for i=0, num_images-1 do begin
  time_img = fmt_tim(index(i))

  del_t = int2secarr(time_img,ref_time)/86400d

  suncenter = [index(i).crpix1, index(i).crpix2]

  xsiz_pix = outsize(0)
  ysiz_pix = outsize(1)

  if not off_limb(0) then begin
       helio = [ref_helio(0)+diff_rot(del_t(0),ref_helio(1),/synodic), $
          ref_helio(1)]
;
;  Try to ignore the fact that it's not required for 
;  cdelt1 == cdelt2.  
;
       fov_cen = conv_h2p(helio, time_img, behind=0, suncenter=suncenter, $
                     pix_size=index.cdelt1, radius=index.solar_r)

  endif else fov_cen = suncenter + ref_helio/index.cdelt1            

  dout(*,*,i) = extract_arr(data(*,*,i), xcen=fov_cen(0), ycen=fov_cen(1), $
                            xsiz=xsiz_pix, ysiz=ysiz_pix)

;
;  Should probably be able to change this in the original FITS header, 
;  But I don't see how...
;
  iout(i).naxis1 = xsiz_pix
  iout(i).naxis2 = ysiz_pix
  iout(i).object = 'partial FOV'

; remember subfield values are physical postions on array, need to add 1 in 
; x-direction and 20 in y-direction?

  if tag_exist(iout,'p1_x') then begin
     sz_data=size(data)
     iout(i).p1_x = (fov_cen(0)-0.5*xsiz_pix+1) + 1 > 0
     iout(i).p2_x = (fov_cen(0)+0.5*xsiz_pix) + 1 < (sz_data(1)-1) + 1
     iout(i).p1_y = (fov_cen(1)-0.5*xsiz_pix+1) + 20 > 0
     iout(i).p2_y = (fov_cen(1)+0.5*xsiz_pix) + 20 < (sz_data(2)-1) + 20
  endif

endfor

end
