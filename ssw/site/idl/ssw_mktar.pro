pro ssw_mktar, notar=notar, nocompress=nocompress, ssw_only=ssw_only, $
               nofilter=nofilter, tar_old_style=tar_old_style
;+
;   Name: ssw_mktar
;
;   Purpose: make compressed tar files for all SSW (installation)
;
;   Keyword Parameters:
;      notar - switch , if set, dont execute tar script
;      nocompress - switch, if set, dont compress 
;      nofilter - switch , if set, dont apply filter (forces update of all)
;      tar_old_style - if set, include 'o' parameter and redirect output
;
;   History:
;      22-May-1996 - S.L.Freeland, for SSW remote installation
;      15-oct-1996 - S.L.Freeland - add G95
;      19-feb-1997 - S.L.Freeland - remove $SSW/yohkoh/ysgen
;                                   (ISAS Yohkoh Master reorganization)
;      13-apr-1997 - S.L.Freeland - SMM,  CGRO,  TRACE, all PACKAGES
;      22-apr-1997 - S.L.Freeland - single instrument missions -> ssw_ssw_xxx
;      12-aug-1997 - S.L.Freeland - call 'ssw_mktar_filter'
;                                   (only regenerate tar if branch updated)
;       5-nov-1997 - S.L.Freeland - allow all yohkoh ucon->tarfile
;                                   (additions controlled by ssw_path)
;      29-May-1998 - S.L.Freeland - added /NOFILTER and /TAR_OLD_STYLE
;                                   added radio "mission"
;       8-Jun-1998 - S.L.Freeland - fix ls.current problem
;      25-feb-1999 - S.L.Freeland - added optical "mission"
;      29-March-2000 - S.L.Freeland - remove $SSW/site (using template)
;      12-Feb-2004 - S.L.Freeland - add VOBS (cosec/egso/vso)
;-

nofilter=keyword_set(nofilter)
tar_old_style=keyword_set(tar_old_style)        ; old SUNOS, for example

oldqual=(['','o'])(tar_old_style)
olddir =(['','_old_style'])(tar_old_style)
outfile='$SSW/site/bin/ssw_mktar'+olddir	; script version
tardir='$SSW/offline/swmaint/tar' + olddir 

tarcmd = 'tar -c'+oldqual+'f '+ tardir + '/'
pr_status,txt,caller='ssw_mktar'
file_append,outfile,/new,['#!/bin/csh -f','#', $
                          '# SSW Compressed Tar Generator', '# ' + txt]

; ----------- Top Level Sets -------
sswlist=str2arr('gen')               ; removed 'site', SLF, 29-mar-2000
ssw_cmd=tarcmd + 'ssw_ssw_' + sswlist + '.tar -C $SSW ' + sswlist + ['']
; ----------------------------------------------------------

; -------------- Multi-Instrument Mission sets --------------------------
; ** TODO?? - CODE M-I-M for auto additions **
soho_list=['gen',ssw_strsplit(str2arr(get_logenv('SSW_SOHO_INSTR'),' '),'/',/tail)]
hinode_list=['gen',ssw_strsplit(str2arr(get_logenv('SSW_HINODE_INSTR'),' '),'/',/tail)]
sdo_list=['gen',ssw_strsplit(str2arr(get_logenv('SSW_SDO_INSTR'),' '),'/',/tail)]
proba2_list=['gen',ssw_strsplit(str2arr(get_logenv('SSW_PROBA2_INSTR'),' '),'/',/tail)]
yo_list=['gen','ucon',ssw_strsplit(str2arr(get_logenv('SSW_YOHKOH_INSTR'),' '),'/',/tail)]
smm_list=['gen',ssw_strsplit(str2arr(get_logenv('SSW_SMM_INSTR'),' '),'/',/tail)]
radio_list=['gen',ssw_strsplit(str2arr(get_logenv('SSW_RADIO_INSTR'),' '),'/',/tail)]
optical_list=['gen',ssw_strsplit(str2arr(get_logenv('SSW_OPTICAL_INSTR'),' '),'/',/tail)]
vobs_list=['gen',ssw_strsplit(str2arr(get_logenv('SSW_VOBS_INSTR'),' '),'/',/tail)]
ster_list=['gen',ssw_strsplit(str2arr(get_logenv('SSW_STEREO_INSTR'),' '),'/',/tail)]
cgro_list=[ssw_strsplit(str2arr(get_logenv('SSW_CGRO_INSTR'),' '),'/',/tail)]
goes_list=[ssw_strsplit(str2arr(get_logenv('SSW_GOES_INSTR'),' '),'/',/tail)]

soho_cmd  =tarcmd + 'ssw_soho_'   + soho_list + '.tar -C $SSW/soho ' + soho_list
hinode_cmd=tarcmd + 'ssw_hinode_'   + hinode_list + '.tar -C $SSW/hinode ' + hinode_list
sdo_cmd=tarcmd + 'ssw_sdo_'   + sdo_list + '.tar -C $SSW/sdo ' + sdo_list
proba2_cmd=tarcmd + 'ssw_proba2_'   + proba2_list + '.tar -C $SSW/proba2 ' + proba2_list
yohkoh_cmd=tarcmd + 'ssw_yohkoh_' + yo_list   + '.tar -C $ys '       + yo_list
smm_cmd   =tarcmd + 'ssw_smm_'    + smm_list  + '.tar -C $SSW/smm '  + smm_list
cgro_cmd  =tarcmd + 'ssw_cgro_'   + cgro_list + '.tar -C $SSW/cgro ' + cgro_list
radio_cmd =tarcmd + 'ssw_radio_'  + radio_list +'.tar -C $SSW/radio '+ radio_list
optical_cmd =tarcmd + 'ssw_optical_'  + optical_list +'.tar -C $SSW/optical '+ optical_list
vobs_cmd =tarcmd + 'ssw_vobs_'  + vobs_list +'.tar -C $SSW/vobs '+ vobs_list
ster_cmd =tarcmd + 'ssw_stereo_'  + ster_list +'.tar -C $SSW/stereo '+ ster_list
goes_cmd=tarcmd + 'ssw_goes_'  + goes_list +'.tar -C $SSW/goes '+ goes_list
mmission_cmd=[soho_cmd,hinode_cmd,sdo_cmd,proba2_cmd,yohkoh_cmd,smm_cmd,cgro_cmd,radio_cmd,optical_cmd,vobs_cmd,ster_cmd,goes_cmd]
; ----------------------------------------------------------

; -------- Single Instrument Mission sets -----------------------
smission=str2arr('hessi,trace,spartan,hxrs,smei')  ; <<*** Add new instruments here
smission_cmd = tarcmd + 'ssw_ssw_' + smission + '.tar -C $SSW ' + smission
; ----------------------------------------------------------

; -------- SSW Packages -------------------------------
packages_list=[ssw_strsplit(str2arr(get_logenv('SSW_PACKAGES_ALL'),' '),'/',/tail)]
packages_cmd=tarcmd + 'ssw_packages_' + packages_list + $
   '.tar -C $SSW/packages ' + packages_list
; ----------------------------------------------------------

; data base sets
ydbsets=str2arr('nar,gev,evn,xad,xbd,g75,g95')
ydbs=tarcmd + 'ssw_ydb_' + ydbsets + '.tar -C $ydb ' + ydbsets
; -------------------------

tarcmds=ssw_cmd
if not keyword_set(ssw_only) then tarcmds= $
  ['#### Top Level SSW ####',                  tarcmds,        '',     	$
   '#### Multi-Instrument Mission Sets ####',  mmission_cmd,   '', 	$
   '#### Single-Instrument Mission Sets ####', smission_cmd,   '',	$
   '#### SSW PACKAGES ####',                   packages_cmd,   '',	$
   '#### data base sets ####',ydbs]

file_append,outfile,tarcmds
if not nofilter then ssw_mktar_filter,outfile                 ; only where changed

if not keyword_set(notar) then spawn,'csh -f ' + outfile else $
    message,/info,"Created script: " + outfile

if not keyword_set(nocompress) then begin
   dat=rd_tfile(outfile,nocom='#')
   dat=dat(where(strtrim(dat,2) ne ''))
   files=ssw_strsplit(strtrim(ssw_strsplit(dat,'-c'+oldqual+'f',/tail),2),' ')
   files=files(where(file_exist(files)))
   for i=0,n_elements(files)-1 do begin
      message,/info,"Compressing: " + files(i)
      spawn,'compress -vf ' + files(i)
      spawn,'csh $SSW/site/bin/mk_lscurrent ' + tardir
   endfor
endif 

; always update the remote "ls mapping file"

return
end
