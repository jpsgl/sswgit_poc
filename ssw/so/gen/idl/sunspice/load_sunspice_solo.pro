;+
; Project     :	SOLO
;
; Name        :	LOAD_SUNSPICE_SOLO
;
; Purpose     :	Load the Solar Orbiter SPICE ephemerides
;
; Category    :	SOLO, SUNSPICE, Orbit
;
; Explanation : Loads the Solar Orbiter ephemeris files in SPICE format.  Also
;               calls LOAD_SUNSPICE_GEN to load the generic kernels.
;
; Syntax      :	LOAD_SUNSPICE_SOLO
;
; Inputs      :	None
;
; Opt. Inputs :	None
;
; Outputs     :	None
;
; Opt. Outputs:	None
;
; Keywords    : RELOAD = If set, then unload the current ephemeris files, and
;                        redetermine which kernels to load.  The default is to
;                        not reload already loaded kernels.
;
;               ORBIT_FILE = Name of the orbit file to load, e.g.
;                            "2020_February_CReMA_Issue4-0.bsp".  If the full
;                            path is not specified, then looks in
;                            $SOLO_SUNSPICE/orbit.  If not passed, or the
;                            specified file cannot be found, then a default
;                            ORBIT file is loaded instead.  Use of the
;                            ORBIT_FILE keyword implies /RELOAD.
;
;               VERBOSE= If set, then print a message for each file loaded.
;
;               ERRMSG = If defined and passed, then any error messages will be
;                        returned to the user in this parameter rather than
;                        depending on the MESSAGE routine in IDL.  If no errors
;                        are encountered, then a null string is returned.  In
;                        order to use this feature, ERRMSG must be defined
;                        first, e.g.
;
;                               ERRMSG = ''
;                               LOAD_SUNSPICE_SOLO, ERRMSG=ERRMSG
;                               IF ERRMSG NE '' THEN ...
;
; Calls       :	TEST_SUNSPICE_DLM, LOAD_SUNSPICE_GEN, FILE_EXIST, CSPICE_FURNSH
;
; Common      :	SOLO_SUNSPICE contains the names of the loaded files.
;
; Env. Vars.  : SOLO_SUNSPICE = points to the directory tree containing the
;                               various Solar Orbiter SPICE ephemerides.  At
;                               the present, prior to Solar Orbiter's launch,
;                               this consists of a spacecraft frame file, a
;                               "fictional" clock kernel for planning, and
;                               several predictive orbit files based on
;                               alternative launch dates and orbit options, one
;                               of which is used as the default.
;
; Restrictions:	This procedure works in conjunction with the Icy/CSPICE
;               package, which is implemented as an IDL Dynamically Loadable
;               Module (DLM).  The Icy source code can be downloaded from
;
;                       ftp://naif.jpl.nasa.gov/pub/naif/toolkit/IDL
;
;               Because this uses dynamic frames, it requires Icy/CSPICE
;               version N0058 or higher.
;
; Side effects:	None
;
; Prev. Hist. :	None
;
; History     :	Version 1, 6-Feb-2017, William Thompson, GSFC
;               Version 2, 10-Mar-2017, WTT, changed to Feb 2019 launch date
;               Version 3, 15-Dec-2017, WTT, changed to Feb 2020 launch date
;               Version 4, 21-Mar-2018, WTT, fixed typos
;
; Contact     :	WTHOMPSON
;-
;
pro load_sunspice_solo, reload=k_reload, verbose=verbose, $
                        orbit_file=orbit_file, errmsg=errmsg
common solo_sunspice, rtnframe, sclk, orbit
on_error, 2
;
;  Make sure that the SPICE/Icy DLM is available.
;
if not test_sunspice_dlm() then begin
    message = 'SPICE/Icy DLM not available'
    goto, handle_error
endif
;
;  If the ORBIT_FILE keyword was passed, then set the /RELOAD keyword.
;
if n_elements(orbit_file) eq 1 then reload=1 else reload=keyword_set(k_reload)
;
;  If the /RELOAD keyword wasn't passed, then check to see if the kernels have
;  already been loaded.
;
n_kernels = n_elements(orbit)
if (not keyword_set(reload)) and (n_kernels gt 0) then return
;
;  Start by unloading any ephemerides which were previously loaded, and then
;  loading the generic kernels.
;
unload_sunspice_solo, verbose=verbose
message = ''
load_sunspice_gen, verbose=verbose, errmsg=message
if message ne '' then goto, handle_error
;
;  Load the RTN frame.
;
solo_sunspice = getenv('SOLO_SUNSPICE')
solo_sunspice_gen = concat_dir(solo_sunspice,'gen')
if !version.os_family eq 'Windows' then $
  solo_sunspice_gen = concat_dir(solo_sunspice_gen, 'dos')
;
rtnframe = concat_dir(solo_sunspice_gen, 'solo_rtn.tf')
if not file_exist(rtnframe) then begin
    message = 'Frame file ' + rtnframe + ' not found'
    goto, handle_error
endif
if keyword_set(verbose) then print, 'Loaded ' + rtnframe
cspice_furnsh, rtnframe
;
;  Load the spacecraft clock file.
;
solo_sunspice_sclk = concat_dir(solo_sunspice,'sclk')
if !version.os_family eq 'Windows' then $
  solo_sunspice_sclk = concat_dir(solo_sunspice_sclk, 'dos')
sclk = concat_dir(solo_sunspice_sclk, $
                  'solo_ANC_soc-sclk_20000101_20160712_V01.tsc')
if not file_exist(sclk) then begin
    message = 'Clock file ' + sclk + ' not found'
    goto, handle_error
endif
if keyword_set(verbose) then print, 'Loaded ' + sclk
cspice_furnsh, sclk
;
;  Load the orbit file.
;
solo_sunspice_orbit = concat_dir(solo_sunspice, 'orbit')
def_orbit = concat_dir(solo_sunspice_orbit, '2020_February_CReMA_Issue4-0.bsp')
;
if n_elements(orbit_file) eq 1 then begin
    orbit = orbit_file
    if not file_exist(orbit) then begin
        orbit = concat_dir(solo_sunspice_orbit, orbit_file)
        if not file_exist(orbit) then orbit = def_orbit
    endif
end else orbit = def_orbit
;
if not file_exist(orbit) then begin
    message = 'Orbit file ' + orbit + ' not found'
    goto, handle_error
endif
if keyword_set(verbose) then print, 'Loaded ' + orbit
cspice_furnsh, orbit
;
return
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message else $
  errmsg = 'LOAD_SUNSPICE_SOLO: ' + message
;
end
