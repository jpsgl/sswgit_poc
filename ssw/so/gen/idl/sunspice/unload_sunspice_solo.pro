;+
; Project     :	SOLO
;
; Name        :	UNLOAD_SUNSPICE_SOLO
;
; Purpose     :	Unload the Solar Orbiter SPICE kernels
;
; Category    :	SOLO, SUNSPICE, Orbit
;
; Explanation :	Unloads any previously loaded SPICE kernels loaded by
;               LOAD_SUNSPICE_SOLO.
;
; Syntax      :	UNLOAD_SUNSPICE_SOLO
;
; Inputs      :	None
;
; Opt. Inputs :	None
;
; Outputs     :	None
;
; Opt. Outputs:	None
;
; Keywords    :	VERBOSE = If set, then print a message for each file unloaded.
;
; Calls       :	CSPICE_UNLOAD, DELVARX
;
; Common      :	SOLO_SUNSPICE contains the names of the loaded files.
;
; Restrictions:	This procedure works in conjunction with the Icy/CSPICE
;               package, which is implemented as an IDL Dynamically Loadable
;               Module (DLM).  The Icy source code can be downloaded from
;
;                       ftp://naif.jpl.nasa.gov/pub/naif/toolkit/IDL
;
;               Because this uses dynamic frames, it requires Icy/CSPICE
;               version N0058 or higher.
;
; Side effects:	None
;
; Prev. Hist. :	None
;
; History     :	Version 1, 6-Feb-2017, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
pro unload_sunspice_solo, verbose=verbose
;
common solo_sunspice, rtnframe, sclk, orbit
on_error, 2
;
;  Unload the files.
;
if n_elements(rtnframe) eq 1 then begin
    cspice_unload, rtnframe
    if keyword_set(verbose) then print, 'Unloaded ' + rtnframe
    delvarx, rtnframe
endif
;
if n_elements(sclk) gt 0 then begin
    for i=0,n_elements(sclk)-1 do begin
        cspice_unload, sclk[i]
        if keyword_set(verbose) then print, 'Unloaded ' + sclk[i]
    endfor
    delvarx, sclk
endif
;
if n_elements(orbit) eq 1 then begin
    cspice_unload, orbit
    if keyword_set(verbose) then print, 'Unloaded ' + orbit
    delvarx, orbit
endif
;
end
