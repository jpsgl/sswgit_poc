function sxi_addcrota, indexin, update_input=update_input
;
;
;+
;   Name: sxi_addrota
;
;   Purpose: add CROTA2 to sxi index (via P-angle)
;
;   Input Parameters
;      indexn - input vector of sxi 'index'
;
;   Output:
;      function returns 'index' with .CROTA2 added/populated
;
;   Keyword _Parameters:
;      update_input (switch) - if set, update input 'indexin' 
;
;   History:
;      S.L.Freeland
;-
;
if not data_chk(indexin,/struct) then begin 
   box_message,'Need sxi index structure(s)'
   return,-1
endif

index=indexin

pangle=get_rb0p(struct2ssw(index,/nopoint), /pang, /deg, /quiet)
roll=-1.*pangle
index=rep_tag_value(index,roll,'crota1',/quiet)
index=rep_tag_value(index,roll,'crota2',/quiet)
index=add_tag(index,pangle,'p_angle')
if tag_exist(index,'crota') then index.crota=roll else $
index=add_tag(index,roll,'crota') ; ? todo - talk to dom

index.crota1=roll & index.crota2=roll
;index.crotacn1=index.crpix1
;index.crotacn2=index.crpix2

if keyword_set(update_input) then indexin=index ; clobber requested
return,index
end

