pro read_sxi, files, index, data, _extra=_extra, $
   hass_index=hass_index, hass_data=hass_data, $
   satpix_index=satpix_index, satpix_data=satpix_data, no_crota2=no_crota2
;
;+
;   Name: read_sxi
;
;   Purpose: read one or more sxi files -> index[,data] via mreadfits; optionally hass + sat_pix extensions
;
;   Input Parameters:
;      files - list of one or more GOES-N series SXI files (GOES 13/14/15)
;
;   Output Parameters:
;      index - structure vector "index" = per image metadata
;      data  - data cube "data", one image per FILE
;
;   Method:
;      Primary 'index,data' via mreadfits - tweak as required - add optional keywords for Extension 'EXT_index,EXT_data'
;
;   Keyword Parameters:
;      hass_index - (output) - optionally return HASS extension "index" vector
;      hass_data -  (output) - optionally return HASS data arrays "data" (LIST type since mixed nx/ny)
;      satpix_index - (output) - optionally return SATPIX extension "index" vector
;      satpix_data - (output)  - optionally return SATPIX extension "data" cube (LIST type)
;      no_crota - (switch) - if set, add populate missing crota2
;
;   History:
;      15-may-2016 - S.L.Freeland - derive from earlier adhoc stuff (mreadfits_sxi13 etc).

nfiles=n_elements(files)

mreadfits,files,index,data, nodata=(n_params() lt 3), _extra=_extra
; CROTA2 fill 
docrota2=1-keyword_set(no_crota2)
if docrota2 then index=sxi_addcrota(index) 


if arg_present(hass_index) then mreadfits_header,files,hass_index,ext=1
if arg_present(hass_data) then begin
   hass_data=list(mrdfits(files[0],1,_extra=_extra))
   for f=1,nfiles-1 do hass_data.add,mrdfits(files[f],1,_extra=_extra)
endif 
if arg_present(satpix_index) then begin
   nsat=gt_tagval(index,/sat_pix,missing=-1)
   satpix_index=str_subset(index,'bitpix,naxis,naxis1,naxis2')
   satpix_index.naxis1=2
   satpix_index.naxis2=nsat
endif
if arg_present(satpix_data) then begin 
   satpix_data=list(mrdfits(files[0],2,_extra=_extra))
   for f=1,nfiles-1 do satpix_data.add,mrdfits(files[f],2,_extra=_extra)
endif

return
end
