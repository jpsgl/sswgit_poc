function sxi_cat2files,catrecs, parent=parent, urls=urls
;+
;   Name: goes_cat2files
;
;   Purpose: map from goesNN catalog structures to urls/filenames
;
;   Input Parameters:
;      catrecs - records from genx catalog
;
;   Keyword Parameter:
;      urls - if set, return urls (default are local names)
;      parent - top level parent nfs - assumed <parent>/yyyy/mm/dd/<files>
;
;   Output:
;      function returns local filenames or associcated urls 
;   
;   History:
;      Circa Feb-2008 - S.L.Freeland
;      6-dec-2009 - allow <parent> via $sxigN_data, def=N=13

if not required_tags(catrecs,'filename,f1filter,f2filter') then begin 
   box_message,'Function requires input GOES<nn> catalog records
   return,''
endif

tele=gt_tagval(catrecs(0),/TELESCOP,missing='GOES-13')
ntele=strtrim(abs(str2number(tele)),2)
snum=str_replace(tele,'GOES-','')

datalog=get_logenv('sxig'+snum+'_data')

if get_logenv('check') ne '' then stop,'tele'
case 1 of 
   n_elements(parent) gt 0: 
   is_member(ntele,'13,14,15'): $
      parent=concat_dir('$SSWDB','goes/sxig'+ntele+'/level0')
   datalog ne '': parent=datalog ; via logical
   else: parent='/sxi02/sxifm1a/fits_sec'  ; LMSAL Orig = default
endcase 

fnames=catrecs.filename

case 1 of 
   keyword_set(urls): begin 
      box_message,'URLS keyword not yet implemented...
      return,fnames   ; Early Exit!!!! 
   endcase
   1-file_exist(parent): begin
      box_message,'Warning: cannot find parent, but returning implied names anyway'
   endcase
   else:
endcase
   
subdir=anytim(catrecs.date_obs,/ecs,/date_only)  ; yyyy/mm/dd
if os_family(/lower) eq 'windows' then subdir=str_replace(subdir,'/','\')
retval=concat_dir(concat_dir(parent,subdir),fnames)+'.FTS'
if not file_exist(retval(0)) then begin ; check for compressed counterparts
   testf=[retval(0),last_nelem(retval)]
   compexts=str2arr('.gz,.Z')
   for c=0,n_elements(compexts)-1 do begin 
      compext=compexts(c)
      if total(file_exist(testf+compext)) gt 0 then begin ;
         box_message,'some files are compressed with extension='+compext
         ss=where(file_exist(retval+compext))
         retval(ss)=retval(ss)+compext
      endif
   endfor
endif

return,retval
end

