function sxi_umodes, time0, time1, info=info, $
   mcount=mcount , taglist=taglist, incat=incat, _extra=_extra, $
   display=display, nottest=notest , zbuffer=zbuffer, interactive=interactive , $
   goes=goes, where_mode=where_mode, raw_exptime=raw_exptime , graphic=graphic, $
   nots=nots
;
;+
;   Name: sxi_umodes
;
;   Purpose: return string array of uniq sxi "modes"
;
;   Input Paramters:
;      time0,time1 - time range of interest (->sxi_cat)
;
;   Output:
;      function returns string array of uniq "modes"
;
;   Keyword Parameters
;      taglist (in) - optional user supplied taglist to define a mode
;                default='img_code,wavelnth,f1filter,f2filter,exptime'
;      incat (in) - vector of sxi structures - in lieu of times (no cat re-read)
;      mcount (out) - number of matches per mode; long(n_elements umodes)
;      info (out) - mode summary for All catalog records 
;      _extra - unspecified keywords -> sxi_cat.pro
;      raw_exptime (switch) - if set, use .EXPTIME verbatim - default rounds to nearest .01 sec (to bin ~same exposures)
;      xszize=xsize
;
zbuffer=keyword_set(zbuffer)
display=keyword_set(display) or zbuffer or keyword_set(xsize)
if n_elements(xsize) eq 0 then xsize=1280
case 1 of 
   data_chk(incat,/struct): cat = incat ; user supplied
   n_params() ge 2: begin 
      sxi_cat,time0,time1,cat, count=count, _extra=_extra
      if count eq 0 then begin 
         box_message,'No records in your time range'
         return,-1 ; !!! Early Exit
      endif 
   endcase 
   else: begin 
       box_message,'Require input time range...
      return, -1
   endcase
endcase

rexptime=1-keyword_set(raw_exptime) ; default rounds/bins to .01s 

if keyword_set(rexptime) then begin 
   rexptime=float(string(cat.exptime,format='(F8.2)'))
   cat=add_tag(cat,rexptime,'rexptime')
endif

if n_elements(taglist) eq 0 then taglist=$
   'img_code,wavelnth,f1filter,f2filter,shutmode,' + (['','r'])(keyword_set(rexptime))+'exptime'
info=get_infox(cat,taglist)

umodes=ssw_uniq_modes(cat,taglist,mcount=mcount,interactive=interactive,where_mode=where_mode)
if keyword_set(nots) then begin
   ts=strpos(umodes,'TS') eq 0
   nt=where(1-ts,ntcnt)
   if ntcnt eq 0 then begin 
      box_message,'/NOTS set, but zero Non-TS img_codes, so bailing
      return,''
   endif
   umodes=umodes[where(nt)]
endif
   
nmodes=n_elements(umodes)


if keyword_set(display) then begin
   if n_elements(ysize) eq 0 then ysize=800
   wdef,xx,xsize,ysize, zbuffer=zbuffer   ; default = 1280x800
   device,decompose=0
   linecolors
   if keyword_set(no_test) then smodes=umodes(where(strpos(umodes,'T') ne 0)) else $
      smodes=umodes
   first=strmid(smodes,0,1)
   nsmodes=n_elements(smodes)
   carr=[4,5,9,2,7]
   sarr=['A','C','F','T']
   time_window,cat.date_obs,t0x,t1x
   dt=ssw_deltat(t0x,t1x,/hours)
   tzero=reltime(cat(0).date_obs,hours=(dt/10)*(-1),out='ccsds')
   utplot,[tzero,last_nelem(cat.date_obs)],[0,nsmodes],$
      /nodata,/xstyle,ystyle=5, back=11, $
      title=gt_tagval(cat[0],/TELESCOP,missing='GOES') + ' SXI Image Modes '+ $
      'MODE=uniq(' + taglist + '), _extra=_extra
   if n_elements(seperation) eq 0 then seperation=.04
   for i=0,nsmodes-1 do begin 
      ss=where(info eq smodes(i),sscnt)
       evt_grid,cat(ss).date_obs,ticklen=.005,tickpos=.15+(i*seperation), $ ;.013
          color=carr(where(first(i) eq sarr)),thick=2
       evt_grid,cat(ss(0)).date_obs,/no_blank,ticklen=.000001,labpos=.15+(i*seperation), $
          label=strcompress(smodes(i)), labcolor=7, $
          labsize=([.9,.8])(zbuffer) ,$
          align=1,/noarrow
   endfor
   graphic=tvrd()
   if keyword_set(goes) then begin 
      dat=tvrd()
      wdef,xxx,/zbuffer,xsize,256
      loadct,0
      linecolors
      plot_goes,anytim(tzero,/vms),anytim(last_nelem(cat.date_obs),/vms), $
         /xstyle,back=11,color=5,/ascii,_extra=_extra
      gdat=tvrd()
      cdat=[[dat],[gdat]]
      wdef,im=cdat
      tv,cdat 
      graphic=tvrd() 
   endif

endif
   
return,umodes
end 
