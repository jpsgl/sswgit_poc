pro sxi_cat,time0,time1,catrecs, filenames, goesnn=goesnn, catdir=catdir, $
   search=search, count=count, tcount=tcount, debug=debug, temp=temp, $
   g12=g12, g13=g13, g14=g14, g15=g15, urls=urls, $
   force_remote=force_remote, force_missing=force_missing
;
;+
;   Name: sxi_cat
;
;   Purpose: read GOES/SXI<MNOP> catalog for user time range; option search
;
;   Input Paramters:
;      time0, time1 - time range of interest
;
;   Output Paramters:
;      catrecs - matching catalog records
;      filenames - optionally, local filenames (via sxi_cat2files.pro
;
;   Keyword Parameters:
;      goesnn - satallite (default=13)
;      catdir - catalog directory - default=$SSWDB/goes[n]/sxigNN/genxcat 
;      search - optional input search  array - see struct_where.pro header
;      count - number of records returned
;      tcount - number of records in time range (might differ if SEARCH input)
;      temp - if set, read pre-release version (in parallel ...<releasedir>_temp )
;      gNN - alternate specificaiton of GOENN via switch {/g13, /g14,... etc}
;      urls (switch) - if set, <filenames> are urls (default=lmsal)
;      force_remote - ignore $SSWDB/goes/sxigNN/...  (for testing)
;      force_missing - ignore $SSDB/goesn/sxigNN/... (for testing)
;
;   Calling Examples:
;      IDL> sxi_cat,'15-aug-2006','20-aug-2006',cat ; goes13 catalog->cat
;      IDL> sxi_cat,'15-aug-2006','20-aug-2006',cat,filenames,  $
;              search=['wavelnth=PTHN*','miss_pix<10','exptime>.5']
;      
;HISTORY:
; Circa 2001 - S.L.Freeland written - sxi wrapper for read_genxcat.pro
; 19-Jul-2013, J.R. Lemen, Fixed case when read_genxcat returns only one record
; 15-jul-2016 - S.L.Freeland - allow $SSWDB/goesn/sxig<nn>/genxcat access
;-

count=0 ; pessimistically, assume failure...

case 1 of
   keyword_set(goesnn): ; user supplied
   keyword_set(g12): goesnn=12
   keyword_set(g13): goesnn=13
   keyword_set(g14): goesnn=14
   keyword_set(g15): goesnn=15
   else: goesnn=13
endcase

sgn=strtrim(goesnn,2)

sxig=concat_dir('sxig'+sgn,'genxcat')
sswdb=concat_dir('$SSWDB',['goes','goesn'])
sswdb_genxcat=concat_dir(sswdb,sxig)

if keyword_set(force_remote) then sswdb_genxcat[0]=''
if keyword_set(force_missing) then sswdb_genxcat[*]=''
if get_logenv('check_cat') ne '' then stop,'sswdb_genxcat'

tcount=0
case 1 of 
   data_chk(catdir,/string): ; user supplied
   file_exist(sswdb_genxcat[0]): catdir=sswdb_genxcat[0] ;'goes'
   file_exist(sswdb_genxcat[1]): catdir=sswdb_genxcat[1] ;'goesn'
   else: begin 
      box_message,['Cannot find local catalogs in $SSWDB; Try:', $
                   "IDL> sswdb_upgrade,'goesn',/spawn,/loud,/passive",$
                   "(above run by owner of local $SSW/$SSWDB)" ]
      return ; EARLY Exit on No local catalogs found under $SSWDB/goesn/..
   endcase
endcase 
if get_logenv('check_cat') ne '' then stop,'sswdb_genxcat'

read_genxcat,time0,time1,catrecs,error=error, count=tcount, $
                         topdir=catdir+(['','_temp'])(keyword_set(temp))

if error then begin
    box_message,['Problem with at least one catalog file...', $
           'Re-initializing and retrying...', $
           '(Patience is a virtue ....)']
    read_genxcat,time0,time1,catrecs,error=error, count=tcount, $
                     topdir=catdir+(['','_temp'])(keyword_set(temp)),/init
    if error then begin 
       box_message,'Sorry, could not recover..??'
       return
    endif
endif

; Check that the single record returned is within time0,time1 range
if tcount eq 1 then begin
   chk_tim = anytim2tai(catrecs.date_obs)
   chk = where(chk_tim ge anytim2tai(time0) and $
               chk_tim le anytim2tai(time1), tcount)	; May set tcount to 0
endif

if tcount eq 0 then begin 
   box_message,'No records within your time range...'
   return ; !!!! Early Exit...!!
endif

count=tcount ; 


if data_chk(search,/string) then begin  ; optional search via struct_where
  ss=struct_where(catrecs,search_array=search,count=count)
   if count eq 0 then box_message,$ 
   ['No records match your SEARCH params:',search] else begin 
   catrecs=catrecs(ss)
   endelse
endif

if data_chk(catrecs,/struct) then begin 
  catrecs=add_tag(temporary(catrecs),'GOES-'+sgn,'telescop')
  uss=uniq(catrecs.date_obs,sort(catrecs.date_obs))
  if n_elements(uss) lt n_elements(catrecs) then begin 
      catrecs=catrecs[uss]
  endif
endif
if count gt 0 and n_params() gt 3 then filenames=sxi_cat2files(catrecs) 

if keyword_set(urls) then begin 
    filenames=str_replace(filenames,get_logenv('SSWDB'),'http://www.lmsal.com/solarsoft/sdb')
endif

return
end
