function	sxi_fwpos, param1, f1pos=f1pos, f2pos=f2pos, name=name
;+
;NAME		sxi_fwpos
;PURPOSE	Return the index in the SRE structure that
;		corresponds to f1pos and f2pos
;
;		Called by sxi_flux
;
;CALLING SEQUENCE:
;	index = sxi_fwpos(fits_header)
;	index = sxi_fwpos(FM,f1pos=f1pos,f2pos=f2pos)
;	index = sxi_fwpos(FM,f1pos=f1pos,f2pos=f2pos,name=name)
;
;RETURNS:
;	index		= The index in the SRE structure that corresponds
;			  to the f1pos and f2pos analysis filters
;
;   f1pos (forward wheel) and f2pos (aft wheel) must be betwen 1 and 6
;
;   There are some combinations involving the glass filter in the front
;   filter wheel that do not transmit X-rays and thus, there is no
;   corresponding index in the SRE file.  For those, an index of -1 is 
;   returned.
;
;INPUTS:
;	fits_header	= SXI fits header (if a structure)
;	FM		= SXI FM # (if a scalar or vector)
;
;OPTIONAL INPUT PARAMETER:
;	f1pos,f2pos	= Filter wheel positions if fits_header not supplied
;
;OPTIONAL OUTPUT PARAMETER:
;	name		= The name of the filter (= sre[index].name)
;
;PROCEDURE:
;  Uses the SRE data structure to generate a look-up table.
;
;
;  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;  The following lines of code show how to get a listing of the names
;  of all filter combinations for SXI FM-1:
;
;  IDL> FM =replicate(1,6,6)			; 6x6 filter combinations
;  IDL> f1pos = rotate((indgen(6,6) mod 6)+1,1)	; Front wheel
;  IDL> f2pos = (indgen(6,6) mod 6)+1		; Aft wheel
;  IDL> index = sxi_fwpos(FM,f1pos=f1pos,f2pos=f2pos,name=name)
;  IDL> for i=0,35 do print,index[i],f1pos[i],f2pos[i],'  ',name[i]
;  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;
;HISTORY:
; 28-Jun-2006, J. R. Lemen, LMSAL, Written.
;-

;** The first parameter can be a SXI FM number (e.g., 1,2, or 3) or
;** it can be a Fits header (from which the FM number is extracte

if size(param1,/type) ne 8 and size(param1,/type) ne 9 then begin
   FM = param1
   f1 = f1pos
   f2 = f2pos
   if n_elements(FM) ne n_elements(f1) or 	$
      n_elements(FM) ne n_elements(f2) then	$
	message,'Length of FM, f1pos, and f2pos must match'
   if min(f1pos) lt 1 or max(f1pos) gt 6 then 	$
	message,'F1pos must be between 1 and 6'
   if min(f2pos) lt 1 or max(f1pos) gt 6 then 	$
	message,'F2pos must be between 1 and 6'
endif else begin
   FM = gt_sxi_params(param1,/FM)
   f1 = gt_sxi_params(param1,/f1pos)
   f2 = gt_sxi_params(param1,/f2pos)
endelse


npts = n_elements(FM)
index = lonarr(npts)
name =  strarr(npts)

FM_last = 0
for i=0,npts-1 do begin

; Generate a new look-up table when a new FM # is encountered
  if FM_last ne FM[i] then begin
    sre = rd_sxi_sre(FM=FM[i]) 
    fff = replicate(-1,7,7) & ffname = replicate('Glass',7,7)
    for k=0,n_elements(sre.name)-1 do begin
      fff[sre.fwpos[k,0],sre.fwpos[k,1]] = k
      ffname[sre.fwpos[k,0],sre.fwpos[k,1]] = sre.name[k]
    endfor
    FM_last = FM[i]
  endif

  if f1[i] ne 0 and f2[i] ne 0 then begin
    index[i] = fff[f1[i],f2[i]]
    name[i]  = ffname[f1[i],f2[i]]
  endif
    

endfor


return, index
end
