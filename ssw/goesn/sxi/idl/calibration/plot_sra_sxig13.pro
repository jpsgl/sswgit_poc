;+
;NAME:		plot_sra_sxig13
;PURPOSE:	Plot the sra files for GOES SXI-N/O/P
;		The sra files contain the area as a function of
;		wavelength for each filter
;
;HISTORY:
; 16-Mar-2006, J. R. Lemen, Written
; 19-Mar-2006, N. V. Nitta, modified
; 21-Mar-2006, N. V. Nitta, added default output file
; 27-Mar-2007, N. V. Nitta, removed definition of SXI_RESPONSE 
;-



if n_elements(fm) eq 0 then fm=1
input,'* Enter FM 1, 2, or 3: ', fm, fm , 1, 3

file='$SXI_RESPONSE/'+string('fm',fm,'_sra_???.geny',form='(a,i1,a)')

; Next line is temporarily hardcoded (there should be 3 ???s)
;  file='$SXI_RESPONSE/'+string('fm',fm,'_sra_??.geny',form='(a,i1,a)')

sra_files = findfile(file)

sra_files = sra_files[n_elements(sra_files)-1]
if not file_exist(sra_files) then begin
  print,'**  Error:  Could not find the sra file'
  print,'**          file = ',file
  stop
endif

; Read the sra file

restgenx,file=sra_files,sra

linecolors

if !d.name eq 'PS' then begin
  qfont = 0
  outfil='sxi_'+strmid(file_break(sra_files),0,strlen(file_break(sra_files))-5)+'.ps'
  print,'device = ps     output file = ',outfil
  device,/color,set_font='Helvetica-Bold',file=outfil
endif else begin
  qfont = -1
endelse

; nfilt = 8
s0=where(sra.name eq 'Open/Thin_poly')
if s0 ne -1 then sfilt=s0(0)
s1=where(sra.name eq 'Tin/Open')
if s1 ne -1 then sfilt=[sfilt,s1(0)]
s2=where(sra.name eq 'Thick_poly/Open')
if s2 ne -1 then sfilt=[sfilt, s2(0)]
s3=where(strmid(sra.name,0,12) eq 'Open/Be12') ; FM3 has the same be_thin 
if s3 ne -1 then sfilt=[sfilt, s3(0)]
s4=where(sra.name eq 'Open/Al12')
if s4 ne -1 then sfilt=[sfilt, s4(0)]
s5=where(sra.name eq 'Open/Be50')
if s5 ne -1 then sfilt=[sfilt, s5(0)]
s6=where(sra.name eq 'Open/Silver')
if s6 ne -1 then sfilt=[sfilt, s6(0)]
nfilt=n_elements(sfilt)

color=[255, 2, 9, 13, 7, 10, 4]
if !d.name eq 'PS' then color(0)=0

plot_oo,sra.lambda,sra.area[*,0]*sra.entrance,		$
	xtitle='Wavelength (A)',			$
  	ytitle='Effective area cm!U2!N',		$
	title='GOES SXI FM'+string(fm,form='(i1)'),	$
  	yrange=[1.e-10,1],xrange=[1.,300],font=qfont, $
        xstyl=1,ystyl=1,color=color(0),thick=3

for i=1,nfilt-1 do oplot,sra.lambda,sra.area[*,sfilt(i)]*sra.entrance,	$
  	color=color[i],thick=3

for i=0,nfilt-1 do xyouts,.18,.88-.05*i,sra.name[sfilt(i)],font=qfont,	$
  	color=color[i],/norm


if !d.name eq 'PS' then begin
    xyouts,.05,-.05,string('SRA file =  $SXI_RESPONSE/',file_break(sra_files)),font=qfont,/norm
    device,/close
endif else wshow

end
