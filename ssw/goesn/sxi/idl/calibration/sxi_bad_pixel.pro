function get_sxi_badpixel_DB, INDEX, FM=FM, OP_AMP=OP_AMP, TIME=TIME, $
	YAW_FLIP=YAW_FLIP, BADVALUE=BADVALUE, BLS=BLS, EPOCH=EPOCH
;
;NAME:		GET_SXI_BADPIXEL_DB
;
;PURPOSE:	Read the SXI Bad Pixel Database and return bad pixel
;		map that correspond to the information in index
;
;CALLING SEQUENCE:
;	bad_map = GET_SXI_BAD_PIXEL_DB( INDEX, EPOCH=EPOCH )
;	bad_map = GET_SXI_BAD_PIXEL_DB( FM=FM, OP_AMP=OP_AMP, TIME=TIME, $
;		YAW_FLIP=YAW_FLIP[, EPOCH=EPOCH, BADVALUE=BADVALUE, /BLS] )
;
;INPUT:
;      INDEX	FITs file header index structure (Scalar)
;
;OPTIONAL INPUT KEYWORDS:
;	If INDEX is not present, the following four scalar keywords MUST 
;	be present:
;	  TIME	= Time of interest
;	  FM	= 1, 2, or 3 for FM1 (13), FM2 (O), or FM3 (P)
;	  OP_AMP= 'A' or 'B' for the amplifier (string type)
;	  YAW_FLIP= 'SA_NORTH' or 'SA_SOUTH'
;
;	BADVALUE= Scalar value with which to mark the bad pixels
;	 		(default=-999)
;	BLS	= If set, add BLS and overclock pixels to output

;
;RETURNS:
;	Return an array that is in the coordinate system of FITs Level 0
;	files with the bad pixels set to the value of BADVALUE (default=-999)
;
;	If /BLS is present, return an 588x528 integer*2 array 
; 	If BLS=0, or is not present, return a 512x512 integer*2 array
;	If index is supplied, BLS is determined from naxis2
;
;	EPOCH	= The time epoch of the returned map (starting with 0)
;
;PROCEDURE:
;
;  GET_SXI_BADPIXEL_DB reads the bad pixel database for the requested
;  SXI FM (1, 2, or 3, correspondening to GOES 13, 14, or 15).  The
;  appropriate epoch is selected based on the time of the SXI image (in
;  INDEX).  The appropriate map is returned depending on the OP_AMP and
;  YAW_FLIP parameters.
;
;  Since we expect that this routine typically will be called mulitple
;  times for a single FM istrument, the maps are stored in a common
;  block for further use.  A new read of the database is triggered if
;  the value of FM changes.
;
;REQUIRED ENVIRONMENTAL:
;     Must define 'SXI_DARK' to access the genx file that contains the
;     bad pixel data.
;  e.g., set_logenv, 'SXI_DARK', '/ssw/goes/sxig13/response/data/dark/'
;
;
;HISTORY:
; 17-Feb-2009, J. R. Lemen, Written (V 0.01)
; 23-Dec-2009, J. R. Lemen, (V0.9) Changed to store bad pixel database in the
;			same directory as dark database
;  4-Mar-2011, J. R. Lemen, Corrected so that a null bad pixel file 
;			    returns all zeros.
; 29-Dec-2015, J. R. Lemen, Re-wrote to use the same format BPM as the
; 				flight software uses.
;-
prog_ver = 0.01		; 17-Feb-2009 (Written)
prog_ver = 0.9		; 23-Dec-2009 
prog_ver = 1.0		; 29-Dec-2015 

common badpix, FM_Num, epoch_time, delta_epoch_time, db_map


if n_elements(badvalue) ne 0 then badvalue0 = badvalue else badvalue0= 1

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; If Index is supplied, extract the required information

; If Index is NOT supplied, then FM, OP_AMP, and TIME keywords must be
; provided when this routine is called.

if n_params() eq 1 then begin		; INDEX is provided
  if n_elements(index) ne 1 then message,'Index must be a scalar'

  FM0     = gt_sxi_params(index,/FM)
  OP_AMP0 = gt_sxi_params(index,/OP_AMP)
  Time0   = gt_sxi_params(index,/Time)
  Yaw_flip0 = get_tag_value(index,/Yaw_Flip)
  if index.naxis2 eq 528 then BLS0 = 1 else BLS0 = 0
  binning = gt_sxi_params(index,/binning)
  
endif else begin			; INDEX is not specified
  FM0     = FM
  OP_AMP0 = OP_AMP
  Time0   = TIME
  Yaw_flip0 = YAW_FLIP
  BLS0 = keyword_set(BLS)
  binning = 1

  if n_elements(FM0) ne 1 or n_elements(OP_AMP0) ne 1 or $
    n_elements(Time0) ne 1 or n_elements(Yaw_flip0) ne 1 then $
    message,'FM, OP_AMP, TIME, and YAW_FLIP must be scalars'
  
endelse

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; Read the bad pixel database for the specified SXI if not already in common
;
; Bad pixel database files are located on $SXI_DARK directory for 
; GOES 13, 14, 15 instruments

; Initialize an invalid value to force a read the first time
if n_elements(FM_Num) eq 0 then FM_Num = 0

if FM0 ne FM_Num then begin

  dir = get_logenv('SXI_DARK')

; Set up the file name search.

; ===================================================================
; Old Method
; This is of the form sxi_g13_badpix_060524.txt'
; fn_search = 'sxi_g'+strtrim(([13,14,15])[FM0-1],2)+'_badpix_*.txt'
; ===================================================================

; This is of the form BPM13_20110224_B.txt'

  fn_search = 'BPM'+strtrim(([13,14,15])[FM0-1],2)+'_2???????_'+(['A','B'])[OP_AMP0]+'.txt'

  files = file_search(concat_dir(dir,fn_search),count=count)

  if count eq 0 then $
     message,' No Bad Pixel Specification Files located: '+fn_search

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; Read the bad pixel database files

  epoch_time = strarr(count)			; Set up some variables
  db_map = bytarr(528,588,count)		; One map per epoch

  for i=0,count-1 do begin			; Loop on database files
    map0 = bytarr(528,588)			; Basic map is all 0s
    print,' Reading file: ',files[i]

    buff = rd_tfile(files[i],nocomm='#')	; Store the epoch time
    tmp = (str2arr(buff[0],'_'))[1]		; Get the epoch time
    epoch_time[i] = anytim(fix([intarr(4),strmid(tmp,6,2),	$
                                strmid(tmp,4,2),strmid(tmp,0,4)]),/yo)

    if n_elements(buff) gt 1 then begin
      buff = strtrim(strcompress(buff[1:*]),2)
      ij = where(strlen(buff) gt 0)
      buff = buff[ij]
    
      ncol = n_elements(buff)-1			; Because last entry is 65535, 32767
      if ncol gt 0 then begin			; If ncol = 0, no bad pixels
         bad_spec = intarr(3,ncol) ; Column, Line, Column Blem (1-Yes)
         for j=0,ncol-1 do begin
            bad_spec[0,j] = $
               (fix(str2arr(strcompress(strtrim(buff[j],2)),' ')))[1:3]
            if bad_spec[2,j] eq 1 then map0[bad_spec[0,j],*] = 1 else $  ; Column Blem
           			   map0[bad_spec[0,j],bad_spec[1,j]] = 1 ; Individual pixel
           			   
            map0[bad_spec[0,j],bad_spec[1,j]:bad_spec[2,j]] = 1
         endfor
      endif					; ncol gt 0
      db_map[0,0,i] = map0 
    endif			; n_elements(buff) gt 1
  endfor
  delta_epoch_time = addtime(epoch_time,diff=epoch_time[0],/s)
  FM_num = FM0
  
endif 				; n_elements(FM_Num) eq 0 

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; Determine the appropriate epoch

in_time = addtime(Time0, diff=epoch_time[0],/s)		; Time (s) since launch
epoch = max(where(in_time ge delta_epoch_time,nc))	; Epoch number

if nc eq 0 then message,'Input time precedes database values'

map1 = db_map[*,*,epoch]*badvalue0


; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; Add BLS pixels if /BLS is set or index.naxis2 eq 528

if keyword_set(BLS0) then begin

;  map2 = intarr(528,588)
;  if strupcase(op_amp0) eq 0 then n0 = 9 else n0 = 7
; print,'n0=',n0
   map2 = map1                  ; Copy map1 into map2
endif else begin
;  map2 = intarr(512,512)
;  if strupcase(op_amp0) eq 0 then n0 = 3 else n0 = 1
  n0=9 & n1=521
  map2 = map1[n0:n1,0:511] 	; Extract first 512 rows
endelse

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; Rotate CCW 90 if SA_NORTH or CW 90 if SA_SOUTH (Yaw flipped)

if strupcase(yaw_flip0) eq 'SA_SOUTH' then $
  		map2 = rotate(map2,3) else $	; SA_SOUTH CW  90
  		map2 = rotate(map2,1) 		; SA_NORTH CCW 90

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; Rebin the map if the image was binned

if binning gt 1 then map2 = rebin(map2,512/binning,512/binning)

return,map2
end

pro sxi_bad_pixel, index, data, oindex, odata, bad_map, badvalue=badvalue,	$
	maponly=maponly,fix=fix, debug=debug
;+
;NAME:		SXI_BAD_PIXEL
;
;PURPOSE:	Replace the bad pixels of the SXI images with a specific
;		value (default = -999)
;
;CALLING SEQUENCE:
;		SXI_BAD_PIXEL, INDEX, DATA
;		SXI_BAD_PIXEL, INDEX, DATA, OINDEX, ODATA
;		SXI_BAD_PIXEL, INDEX, DATA, OINDEX, ODATA, $
;				[/FIX,BADVALUE=BADVALUE,/MAPONLY]
;
;INPUTS:
;	INDEX & DATA	GOES 13, 14, or 15 FITs Header and Data
;
;OUTPUTS:
;	INDEX  & DATA if OINDEX & ODATA are not supplied (inputs are clobbered)
;	OINDEX & ODATA Updated FITs Header and Data cubes
;
;OPTIONAL INPUT KEYWORD:
;	badvalue - A scalar value that is to be inserted into the array 
;		   If not specified, the default value is -999
;	maponly  - Set this keyword to return the map of bad pixels (set
;                  to the value of badvalue)
;	fix      - Set this keyword to interpolate along rows to repair
;                  bad pixels.  (If maponly is set, /fix keyword is ignored).
;		   BAD_INTERP 
;
;PROCEDURE:
;   This routine calls get_sxi_bad_pixel_db to retrieve the appropriate
;   full-resolution (528x588) bad pixel map for the epoch that
;   corresponds to the date of the image.  The routine checks for the
;   instrument (FM1, FM2, or FM3), the time (to obtain the appropriate
;   epoch), and the amplifier (A vs B swaps the column definitions).
;
;   Next SXI_BAD_PIXEL sets the value in the bad pixel map to the
;   specified badvalue or to -999.
;
;   If the data is summed, then the badvalue map is also appropriately
;   summed.
;
;   Except for 588x528 images, the routine simply returns if naxis1 is
;   not equal to naxis2.  The SXI FITs headers to not provide sufficient
;   information to work with non-full frame (summed or not) images.
;
;REQUIRED ROUTINES:
;   GET_SXI_BAD_PIXEL_DB to return the full-resolution bad pixel map
;   BAD_INTERP to interpolate over the bad pixels.  A small amount of
;   		statistical noise is added for to improve esthetic appearance.
;   INPAINT to replace BAD_INTERP
;HISTORY:
; 13-Jul-2016, S. J. Timothy, Written, V0.3, replace bad_interp with inpaint
; 13-Apr-2009, J. R. Lemen, Written, V0.2
; 24-Dec-2009, J. R. Lemen, Trap the non-symmetric (parial ROI cass).
; 				Added noise keyword in call to bad_interp
;  4-Feb-2011, J. R. Lemen, V1.0 Corrected a bug for when data is 3-D.
;				Don't add noise to darks
;-

Version = 'V0.2'	; 13-Apr-2009
Version = 'V0.9'	; 23-Dec-2009
Version = 'V1.0'	; 16-Feb-2011
Bad_Version= float(strmid(Version,1,strlen(Version)))

; Make sure the index and data arrays are consistent

nti=n_elements(index)
szd=size(data) & nx = szd[1] & ny = szd[2]


; Try to determine if we have a partial field of view:


; Make sure data has not already had the bad pixel map applied.

Bad_flag = gt_tagval(index,/bad_pix,found=found)
if not found then begin
  oindex = add_tag(index, 0., 'BAD_PIX') 
  oindex = add_tag(oindex,-1,'BADEPOCH')
endif else oindex = index
Bad_flag = gt_tagval(oindex,/bad_pix)
odata = data 				; Set up the output variables


iBad = where(Bad_flag eq 0, nfiles)	; These files need bad pixel treatment

;if N_params() eq 4 then odata = data 


if n_elements(badvalue) eq 0 then badvalue = 1


for i=0,nfiles-1 do begin

  bad_map = get_sxi_badpixel_db(oindex[iBad[i]],epoch=epoch,badvalue=badvalue)
  print,iBad[i],oindex[iBad[i]].naxis1,oindex[iBad[i]].naxis2
  help, bad_map


; Compute G = DN / photons to properly add statistical noise 
; 100 = 364 eV / 3.63 eV/e- hole pair	(average SXI energy = 364 eV=34 A)

;start edit -- pgs
;commenting out call to sxi_gain -- as it does not appear to be used (or needed?): PGS 20160719
;  G = (sxi_gain(oindex[iBad[i]]))[0]	; Gain e-/DN
;end edit   -- pgs

     ;if gt_sxi_params(oindex[iBad[i]],/filt) ne 'Glass/Open' then G = G/100.
     ;if gt_sxi_params(oindex[iBad[i]],/shut) eq 1 then G=0 ; Don't add to darks
  odata[*,*,iBad[i]] = inpaint(data[*,*,iBad[i]],bad_map,n=1000)
  help, odata

  
  ;   ii = where(bad_map eq badvalue)
  ;   Temp = odata[*,*,iBad[i]]
  ;   Temp[ii] = 0;bad_map[ii]
   ;  odata[0,0,iBad[i]] = Temp
  
  oindex[iBad[i]].badepoch = epoch
  oindex[iBad[i]].bad_pix = Bad_version
endfor

if N_params() eq 2 then begin
  index = oindex
  data  = odata
endif

end
