function	sxi_gain, fits_header, 	$
		FM=FM,Amp_CCD=Amp_CCD,T_CCD=T_CCD,ampgain=ampgain, $
		date=date, $
		prog_ver=prog_ver
;+
;NAME:		sxi_gain
;PURPOSE:	Return the appropriate system gain (eletrons/DN)
;		for the GOES-N/O/P SXI telescopes.  The gain depends 
;		on the 	telecope (FM-1, FM-2, or FM-3), Amp_CCD (A or B), 
;		amplifier gain (Hi or Lo), and the CCD temperature
;
;CALLING SEQUENCE:
;	gain = sxi_gain(fits_header)
;	gain = sxi_gain(FM=1,Amp_CCD='A',T_CCD=Temp_CCD,[,gain='Lo'])
;
; Input parameters:
;
;  Fits_header - The Fits file structure and if it is a
;		vector, then gain will be returned as vector.
;
;	If Fits_header is not specified, then the keywords FM=,
;	Amp_CCD=, and T_CCD= must be specified.  The optional ampgain=
;	keyword defaults to the Low-gain configuration (high gain
;	is mainly used for ground testing).
;
; Input keywords that are required if Fits_header is not specified:
;	FM	Must be 1, 2, or 3 
;	Amp_CCD	Must be A or B	   
;	T_CCD	CCD temperature in degrees C 
;
; Optional keywords (if Fits_header is not specified):
;	AmpGain	Amplier Gain. Valid options are 'Hi' or 'Lo' [Default = 'Lo']
;	Date	Date when Gain should apply [not implemented]
;
;	FM, Amp_CCD and AmpGain must be scalars.  T_CCD may be a vector.
;
; Optional output keyword:
;	prog_ver Version of the program
;
; Return:
;	SXI system gain in electrons/DN, the same length as Fits_header
;	or as T_CCD
;
;APPROACH:
;	The gain returned is based on analyses performed by N. Nitta
;	and J. R. Lemen of light transfer curve (or photon transfer
;	curve in Jim Janesick's terminology) data obtained during 
;	ground testing.  Data sets taken in the LMSAL 20-meter system
;	and during end-to-end calibration at MSFC XRCF at various CCD
;	were analyzed and used to determine a linear gain dependence
;	for CCD temperature for the various combinations of FM
;	instrument #, Amp_CCD, ampgain, and temperature.  Results of this
;	analysis are summarized in Design Note XX by TBD dated XYZ.
;
;HISTORY:
; 10-Jul-2006, V 0.9, J. R. Lemen, Written
;-

prog_ver = 0.9		; 10-Jul-2006 (Written)



;*********************************************
;Set up the DC offset and linear coefficients
;*********************************************

; (i,j,k) = (FM #, A[0] or B[1] amp, Hi[1] or Lo[0] gain)

DCoff = fltarr(3,2,2)		; DC offset
Slope = fltarr(3,2,2)		; Slope

; --- FM 1 ----------------------------------------

DCoff[0,0:1,0] 	= [65.18,57.11]	; Amp_CCD A and B, Low
Slope[0,0:1,0]	= [0.091,0.063]
DCoff[0,0:1,1] 	= [13.68,10.80]	; Amp_CCD A and B, High
Slope[0,0:1,1]	= [0.011,0.009]

; --- FM 2 ----------------------------------------
DCoff[1,0:1,0] 	= [66.24,63.45]	; Amp_CCD A and B, Low
Slope[1,0:1,0]	= [0.081,0.075]
DCoff[1,0:1,1] 	= [12.47,12.73]	; Amp_CCD A and B, High
Slope[1,0:1,1]	= [0.006,0.006]

; --- FM 3 ----------------------------------------
DCoff[2,0:1,0] 	= [60.18,46.04]	; Amp_CCD A and B, Low
Slope[2,0:1,0]	= [0.068,0.046]
DCoff[2,0:1,1] 	= [00.00,00.00]	; Amp_CCD A and B, High
Slope[2,0:1,1]	= [0.000,0.000] ; Hi gain measurements
				;- are not available


;**************************************
; Decode the input parameters/keywords
;**************************************


; Case 1 -- Fits header is specified

if n_elements(Fits_header) ne 0 then begin	;=======================

; Get the Telescope

 qFM = gt_sxi_params(Fits_header,/FM)

 ij = where(qFM lt 1 or qFM gt 3, nc)
 if nc gt 0 then begin
   message,'FM must be 1, 2, or 3', /continue
   help,qFM
   stop
 endif

; Get the specific amplifier  AMP_CCD:  A = 0, B = 1 

 qAmp = gt_sxi_params(Fits_header,/op_amp)

 ij = where(qAmp lt 0 or qAmp gt 1, nc)
 if nc gt 0 then begin
   message,"Amp_CCD must be 'A' or 'B'",/continue
   help,qAmp
   stop
 endif


; Get the CCD temperature

 T_CCD = gt_sxi_params(Fits_header,/T_CCD)

; Get the amplifier gain (ampgain): qgain  Lo = 0, Hi = 1

 qgain = gt_sxi_params(Fits_header,/ampgain)
 ij = where(qgain lt 0 or qgain gt 1, nc)
 if nc gt 0 then begin
   message,"AmpAgain or AmpBgain must be 'Lo' or 'Hi'",/continue
   help,qgain
   stop
 endif

endif else begin  ;=======================================================

; Case 2 -- Keywords are specified


  if n_elements(FM) eq 0 or n_elements(Amp_CCD) eq 0 or $
     n_elements(T_CCD) eq 0 then begin
    message,'No parameters specified or impartially specified - returning',$
		/continue
    return,null
  endif

  if FM lt 1 or FM gt 3 then begin
    message,'FM must be 1, 2, or 3',/continue
    return,null
  endif else qFM = FM

  if strupcase(Amp_CCD) ne 'A' and strupcase(Amp_CCD) ne 'B' then begin
    message,"Amp_CCD must be 'A' or 'B'",/continue
    return,null
  endif else if strupcase(Amp_CCD) eq 'A' then qamp = 0 else qamp = 1

  if n_elements(ampgain) eq 0 then qgain = 0 else begin
    if strupcase(ampgain) eq 'HI' then qgain = 1 else qgain = 0
  endelse

endelse		;=======================================================


return,DCoff[qFM-1,qAmp,qgain] + T_CCD * Slope[qFM-1,qAmp,qgain]
end




