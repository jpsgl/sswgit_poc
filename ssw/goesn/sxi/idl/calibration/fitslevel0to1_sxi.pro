pro fitslevel0to1_sxi, iindex_l0, oindex_l1, level1_template=level1_template, verbose=verbose

;+
;NAME:    FITSLEVEL0TO1
;
;PURPOSE: Modify a Level 0 SXI FITS header structure to be Level 1 compliant:
;         1.) Delete input FITS header fields that are not defined for SXI level 1 FITS files
;         2.) Add level 1 tags that do not exist in input structure.
;
;METHOD:  1.) Make local copy of iindex_l0 fits header structure: oindex_l1 (make a hash_table of terms?)
;         2.) Iterate over iindex_l0 -- 
;             Iterate over template/list
;
;CALLING SEQUENCE:
;         fitslevel01to1_sxi, iindex, l0 
;
;INPUTS:
;         IINDEX_L0      -- the Level0 data structure    
;
;OUTPUTS:
;         OINDEX_L1 
;
;OPTIONAL INPUT KEYWORD:
;         level1_template: (not yet supported) a list, or structure of l1 tags -- to be used as a conversion template.
;         verbose:         prints level0 tags that are copied to level 1 header structure.
;
;PROCEDURE:
;         1.) instantiate a Level 1 FITS header. 
;         0.) for each element of the level 1 tag template, copy any values in the fits header structure that share the same tag into that tag.
;
;HISTORY:
;         07/08/2016 P. G. Shirts, added crpix1/2, x/ycen, fileoutput test, vignetting calls..
;-

proc_name = 'fitslevel0to1_sxi:'

;define the structure for the level 1 fits header.  This is the first iteration and may change.
;note: there should be the ability to pull in a template as a keyword and skip this -- so it will be more flexible
;alternately, we could save this as a genx file and restore it, if we had a good place for the genx file.
;either of these two approaches would make the code more general, and we could remove the "_sxi" in the name.

goesN_Lvl1tags= $
create_struct(NAME="GOES_LVL1_FITS_HDR", $
'SIMPLE'  , 0,  $ 'T' if the header conforms to the FITS standard, 'F' if it does not .  Supposed to be a logical constant -- how do we indicate this 0/1, T/F?
;'IMG_CODE', '',  $ ;moved below 'NAXISn', per 'IMG_CODE', '',   
'BITPIX'  , -64,  $ ;note: -32 == single precision floating point; -64 == double precision floating point.  which one?
'NAXIS'   , 0,    $ ;note: this is mandatory per http://heasarc.gsfc.nasa.gov/docs/fcg/standard_dict.html -- verify final value is correct.  Should it be 2?
'NAXIS1'  , 0,    $
'NAXIS2'  , 0,    $
'IMG_CODE', '',   $
'EXTEND'  , 0,    $
'BLANK'   , 0,    $
'FDATE'   , '',   $
'FNAME'   , '',   $
'CRPIX1'  , 0.0D, $
'CRPIX2'  , 0.0D, $
'CRVAL1'  , 0,    $
'CRVAL2'  , 0,    $
'CDELT1'  , 0.0D, $
'CDELT2'  , 0.0D, $
'CTYPE1'  , '',   $
'CTYPE2'  , '',   $
'CROTA'   , 0,    $
'XCEN'    , 0.0D, $
'YCEN'    , 0.0D, $
'BZERO'   , '0',  $ ; was '' -- Sam Test
'BSCALE'  , 1,    $ ; was 0.0D -- Sam Test
'BUNIT'   , '',   $
'DATE_OBS', '',   $
'DATEOBS' , '',   $
'TELESCOP', '',   $
'INSTRUME', '',   $
'OBJECT'  , '',   $
'VERINGST', '',   $
'VERLEVL0', '',   $
'LEVEL0ID', '',   $
'SHRTDARK', '',   $
'LONGDARK', '',   $
'VERLEVL1', '',   $
'WAVELNTH', '',   $
'EXPTIME' , 0.0D, $
'LIN_DSBL', 0,    $
'SAD_DSBL', 0,    $
'EXP_INDX', 0,    $
'INT_TIME', 0.0D, $
'SADA_OFF', 0.0D, $
'CCD_TMP' , 0.0,  $
'MIRR_TMP', 0.0,  $
'IMG_WDI' , 0,    $
'IMG_MIN' , 0,    $
'IMG_MAX' , 0,    $
'IMG_MED' , 0.0,  $
'IMG_MEAN', 0.0,  $
'IMG_SDEV', 0.0,  $
'MISS_PIX', 0,    $
'MISS_LIN', 0,    $
'FIX_PIX' , 0,    $
'FIX_LIN' , 0,    $
'MED_PIX' , 0,    $
'MED_THRS', 0,    $
'SAT_PIX' , 0,    $
'ZERO_PIX', 0,    $ ; this is the last entry from the table list of sxi keywords
;                   The following set of elements are from SXI167406 GOESNOP_SXI_LEVEL_1_FITS_KEYWORDS 
'CROTA1'  , 0.0,  $
'CROTA2'  , 0.0,  $
'MRSSDATE', '',   $
'PKT_DATE', '',   $
'LEVEL'   , 0,    $
'VER_FSW' , '',   $
'F1POS'   , '',   $
'F2POS'   , '',   $
'F1FILTER', '',   $
'F2FILTER', '',   $
'OP_AMP'  , '',   $
'AMP_GAIN', '',   $
'YAW_FLIP', '',   $
'BINNING' , 0,    $
'SSV_CROT', 0,    $
;'GROUPS'  , 1,    $; added to conform to mandatory keywords in FITS standard: http://heasarc.gsfc.nasa.gov/docs/fcg/standard_dict.html
'PCOUNT'  , 0,    $; note: this is mandatory per http://heasarc.gsfc.nasa.gov/docs/fcg/standard_dict.html -- verify final value is correct.
'GCOUNT'  , 1);,    $; note: this is mandatory per http://heasarc.gsfc.nasa.gov/docs/fcg/standard_dict.html -- verify final value is correct.
;'END'     ) ;added to end, per https://archive.stsci.edu/fits/fits_standard/node39.html
;
;Binary FITS EXTENSION HEADER #1 - Image Quality Mask
;not including FITS Extension Header #1 Image Quality Mask
;"XTENSION",$ ;string
;"PCOUNT",  $ ;integer
;"GCOUNT",  $ ;integer
;"EXTNAME"]   ;string
;FITS Extension Data Array #1 - Data Quality Indicators, pixel by pixel
;Pix_Qual_Values byte 
;End of the unique sxi GOESN level 1 Table that weren't contained in the first list.
; 


num_el_lev0 = size(iindex_l0,/N_ELEMENTS)
if (num_el_lev0 gt 0) then begin
  oindex_l1=replicate({GOES_LVL1_FITS_HDR}, num_el_lev0)
endif else begin
  box_message, [proc_name, 'Missing input index.', 'Ending proc.']
endelse

l0_tags = tag_names(iindex_l0)
num_l0_tags = size(l0_tags,/n_elements); note: could also use n_tags(iindex_l0)

;patch filename to be fname
;stop,'see if we will stop here'
  if (tag_exist(iindex_l0,'FILENAME')) then begin
;stop, 'in fits0tolevel1: see if iindex_l0 has a FILENAME tag and no FNAME tag.'
    rep_tag_name, iindex_l0,'FILENAME', 'FNAME'
;stop, 'in fit0tolevel1: after tag replacement call -- see if iindex_l- *now* has an FNAME tag.'
  endif


for i=0, (num_l0_tags -1) do begin
  if keyword_set(verbose) then print, ''
;  if ((tag_exist(oindex_l1[0], l0_tags[i],/top_level,/quiet)) AND (0 eq (strcmp(l0_tags[i],'BITPIX',/FOLD_CASE)))) then begin; moved bitpix 'conservation' to a rep_tag_value call, below.

if (tag_exist(oindex_l1[0], l0_tags[i],/top_level,/quiet)) then begin
    if keyword_set(verbose) then print, 'Level 0 tag (number '+strtrim(string(i),2) + ') ' + strtrim(l0_tags[i],2) + ' - also EXISTS in Level 1 FITS header: copying to Level 1.'
    com = 'oindex_l1.' + strtrim(l0_tags[i],2) + ' = ' + 'iindex_l0.' + strtrim(l0_tags[i],2); 
    if keyword_set(verbose) then print, 'Executing command: ' + com
    void = execute(com)
    ;note: there was no check that the data type in both cases were equal (or equivalent for the purpose).
    if keyword_set(verbose) then begin
      if (void) then begin 
        print, 'command SUCCESSfully executed!
      endif else begin
        print, 'command FAILED.'
      endelse
    endif
  endif else begin
   if keyword_set(verbose) then print, 'Level 0 tag (number '+strtrim(string(i),2) + ') ' + strtrim(l0_tags[i],2) + ' - does NOT EXIST in Level 1 FITS header: skipping'
  endelse
  if keyword_set(verbose) then print, ''
endfor
oindex_l1 = rep_tag_value(oindex_l1,   1, 'bscale')
oindex_l1 = rep_tag_value(oindex_l1,   0, 'bzero' )
oindex_l1 = rep_tag_value(oindex_l1, -64, 'bitpix'); consider pulling these call into a small proc that accepts a data structure of tags/values for flexibility
;
end
