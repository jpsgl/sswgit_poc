pro sxi_dc_sub, index, data, oindex, odata, debug=debug,	$
                force=force, missing=missing, newmissing=newmissing, $
                Npix=Npix, Corner_pix=Corner_pix
;+ 
;NAME: 		SXI_DC_SUB
;
;PURPOSE: 	Examine the corners of the SXI image and subtract on a
;		constant (DC) term from the entire image.  The amount
;		subtracted mainly corresponds to the camera pedestal, 
;		but also includes read noise and CCD dark current.
;
;CALLING SEQUENCE: 
;		SXI_DC_SUB, INDEX, DATA
;		SXI_DC_SUB, INDEX, DATA, OINDEX, ODATA
;		SXI_DC_SUB, INDEX, DATA, OINDEX, ODATA, /FORCE
;		SXI_DC_SUB, INDEX, DATA, OINDEX, ODATA, MISSING=MISSING
;		SXI_DC_SUB, INDEX, DATA, MISSING=MISSING, NEWMISSING=NEWMISSING
;
;INPUTS: 
;	INDEX & DATA	SXI-13 Fits Header and Data cube
;
;OUTPUTS: 
;	INDEX  & DATA  if OINDEX & ODATA not supplied (inputs are clobbered)
;	OINDEX & ODATA	Updated Fits Header and Data cubes
;
;OPTIONAL INPUT KEYWORD:
;	force	- If it appears that a DC value has previously been 
;		  subtracted, the default is not to perform another DC
;		  subtraction.  Adding /force will force the routine to
;		  perform the subtraction (even if it is zero).  The
;		  DC_SUB keyword will be set to the previous value plus
;		  the value derived when called.
;
;	MISSING	- A scalar value that indicates what is missing data in DATA.
;                 Default (unsubtracted): Missing = -1
;		  Default (previously subtracted and /Force): Missing = -999
;	NEWMISSING - Default is -999
;
;	NPIX	- Defines the number of pixels on the side of a square
;		  that is used to compute the average level in each corner.
;		  The default is NPIX=3 for a 3x3 array (9 pixels).
;	CORNER_PIX - Defines the first pixel from the bottom left corner of
;                 of the array that is used to compute the DC level in each
;		  of the four corners of the array.  The default is 
;		  CORNER_PIX = 10.  Thus, if no values are specified for
;		  NPIX and CORNER_PIX, the lower left corner array will be
;		  DATA[10:12,10:12,i], and the other three corners will be
;		  selected in a symmetric manner.
;
;PROCEDURE:
;
;   If the dc_sub keyword already exists, no operation will occur unless
;   the /FORCE switch is set.
;
;   If the original data has -1 values (indicating missing data), these
;   will be set to -999 (if MISSING and NEWMISSING keywords are not
;   specified).
;
;   The output index is updated with a the tag DC_SUB and the value that
;   was subtracted.  The routine looks at the four corners.  By default
;   the routine looks as DATA[10:12,10:12,i] and the three other 
;   corresponding corners.  It uses the minimum of the four individual
;   corner results.  Use Npix (def=3, for a 3x3) and Corner_pix (def=10)
;   to change the defined region where the pixels are averaged.
;
;REQUIRED ROUTINES: 
;     None.
;
;HISTORY:
; 29 September 2006, J.R. Lemen, Written, V0.9
;-

Version = 'V0.9'
if keyword_set(debug) then print,'********  SXI_DC_SUB, '+Version+' *********'


; Make sure the index and data arrays are consistent

nti=n_elements(index)
szd=size(data) & nx = szd[1] & ny = szd[2]
if (szd[0] eq 3 and nti ne szd[3]) or (szd[0] eq 2 and nti ne 1) $
	or szd[0] eq 1 or szd[0] gt 3 then begin
   print,'The dimension of index must match the number of images. ' 
   print,'  Specified:  dimension of index = ',strtrim(nti,2),	$
     '   # of images = ',strtrim(szd[3],2)
   return
endif


; Prepare the output variables
oindex	= index
odata	= data
if size(odata,/type) ge 4 then qfloat = 1 else qfloat = 0

; Setup of the defaults for the optional input keywords:

if n_elements(Missing) ne 0 then qMissing = Missing[0] else qMissing = -1
if n_elements(NewMissing) ne 0 then qNewMiss = NewMissing[0] else $
    				      qNewMiss = -999

if n_elements(Npix) eq 0 then Num = 3 else Num = Npix[0]
if n_elements(Corner_pix) eq 0 then Cor = 10 else Cor = Corner_pix[0]

; Make sure data has not already been DC corrected
if tag_exist(index,'dc_sub') then begin		; Data already DC subtracted
  dc_sub = gt_tagval(index,/dc_sub)
  qDC = 0
  if keyword_set(force) then begin		; If already DC subtracted,
    qDC = 1					;- set default missing to -999
    if n_elements(Missing) eq 0 then qMissing = -999
  endif
endif else begin				; Data not DC subtracted
  qDC = 1
  dc_sub = fltarr(nti)	; DC offset		; Set up DC_SUB array
  if not qfloat then dc_sub = round(dc_sub)  	; Match the type of DATA
  oindex = add_tag(oindex,dc_sub,'DC_SUB')	; Add new tag
endelse

if qDC then begin

  if keyword_set(debug) then begin
    print,'Missing,Newmissing = ',qMissing, qNewMiss
    print,'Npix,Corner_pix    = ',Num, Cor
  endif

  a0 = Cor & a1 = a0 + Num -1	; Default is a0 = 10 and a1 = 12

  for i=0,nti-1 do begin

; Following code examines four regions near the corners of the image
; Caution:  This approach may not work well for partial frame images
; It also assumes that missing pixels will not significant affect the
; result.  This last point could be corrected, if necessary.

    nx=index[i].naxis1
    ny=index[i].naxis2
    subi=fltarr(4)
    subi[0]=average(float(odata[a0:a1,a0:a1,i]))
    subi[1]=average(float(odata[a0:a1,ny-a1:ny-a0,i]))
    subi[2]=average(float(odata[nx-a1:nx-a0,ny-a1:ny-a0,i]))
    subi[3]=average(float(odata[nx-a1:nx-a0,a0:a1,i]))

    if qfloat then dc_sub[i]=min(subi) else dc_sub[i]=round(min(subi))
    odata[0,0,i]=odata[*,*,i]-dc_sub[i]

; Update the index
     oindex[i].dc_sub = oindex[i].dc_sub + dc_sub[i] 

; Convert missing flag values, -1 [Missing], to -999 [NewMissing]

    ij = where(data[*,*,i] eq qMissing, nc)
    if nc gt 0 then odata[nx*ny*i+ij] = qNewMiss
   
  endfor   
endif else if keyword_set(debug) then $
	print,'sxi_dc_sub:  Data are DC subtracted--No subtract applied'

; Clobber input variables if oindex & odata are NOT supplied:
if n_params() lt 4 then begin	
  index = oindex
  data  = odata
endif

end

