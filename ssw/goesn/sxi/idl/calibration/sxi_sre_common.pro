;NAME:		sxi_sre_common
;PURPOSE:	Define common block that contains the SRE data for 
;		SXI on GOES-N/O/P
;MODIFICATION HISTORY:
; 27-Jun-2006, J. R. Lemen
common sxi_sre_db, sxi_sre_common, Filename_common, Full_Filename_common
