function sxi_lutc, INDEX, DATA
;NAME:		sxi_lutc
;PURPOSE:	Compute the LUT correction for sxi_lut_correction
;
;	LUTed_Data = Raw_Data + sxi_lutc( Index, Raw_Data )
;
;CALLING SEQUENCE:
;	LUT_correction = sxi_lutc(INDEX, DATA)
;
;INPUTS:
;	INDEX	- Scalar SXI FITS header (structure) 
;		or 
;		  scalar integer of the coefficients to use
;		  (normally used for test purposes only)
;	DATA	- Uncorrected values (DN)
;HISTORY:
; 22-Aug-2011, J. R. Lemen, Written
;-

common comm_sxi_lut, Lut, factor


; Read the coefficients from lut_5poly_coeff into structure LUT
; The LUT structure also contains the original LUT file names

if n_elements( LUT ) eq 0 then begin
   file = concat_dir(get_logenv('SXI_LUT'),'lut_5poly_coeff')
   print,'Reading LUT file: ',	file
   restgen, lut, file=file
   if n_elements(factor) eq 0 then factor = 0.	; Unused 22-Aug-2011
endif


if datatype(INDEX,2) eq 8 then begin
; Determine which LUT coefficients should be applied
   fm  =  gt_sxi_params(INDEX,/fm)-1 	; 0 for FM1, 1 for FM2, 2 for FM3
   op  = (gt_sxi_params(INDEX,/op))[0]  ; 0 for A, 1 for B
   amp = (gt_sxi_params(INDEX,/amp))[0] ; 0 for Lo and 1 for Hi
   ilut= 4*fm + 2^op + (1-amp) 		; for example, FM1, B and LO --> ilut=3
endif else ilut = INDEX

plut0=lut.coefficients[*,ilut]

return, poly(DATA, plut0)
end


pro sxi_lut_correction, index, data, oindex, odata, debug=debug,	$
                missing=missing, newmissing=newmissing,no_lut=no_lut
 
 $
;+ 
;NAME: 		SXI_LUT_CORRECTION
;
;PURPOSE: 	Linearize the output of the SXI-13 camera using look-up
;		table (LUT) results from pre-launch light transfer curve
;		analysis
;
;CALLING SEQUENCE: 
;		SXI_LUT_CORRECTION, INDEX, DATA
;		SXI_LUT_CORRECTION, INDEX, DATA, OINDEX, ODATA
;		SXI_LUT_CORRECTION, INDEX, DATA, OINDEX, ODATA, NO_LUT=NO_LUT
;
;INPUTS: 
;	INDEX & DATA	SXI-13 Fits Header and Data cube
;
;OUTPUTS: 
;	INDEX  & DATA  if OINDEX & ODATA not supplied (inputs are clobbered)
;	OINDEX & ODATA	Updated Fits Header and Data cubes
;
;OPTIONAL INPUT KEYWORD:
;	no_lut	- Vector containing values of index[i] specifying which
;		  images should NOT be corrected.  
;		  E.g. if no_lut = [0,5], the images corresponding to 
;		  index[0] and index[5] will not be LUT corrected.
;		  However, they will be DC subtracted and "-1"s will be
;		  converted to "-999"
;	MISSING	- A scalar value that flags the missing data in DATA.
;                 Default (unsubtracted): Missing = -1
;	NEWMISSING - Default is -999 (This is also an output variable)
;
;PROCEDURE:
;   The look-up table (LUT) for the FM-1/2/3 instruments were determined
;   for A and B outputs and Hi and Lo amplifier gains by analyzing pre-launch
;   light transfer curve data.  The amplifier gain was adjusted to derive
;   the most linear light transfer curve.  Fifth order polynomial fits were
;   made to the results and are stored in lut_5poly_coeff.genx.
;
;   First step is to call SXI_BLS_SUB to subtract off the BLS offset (The
;   LUT correction can not be performed until the camera DC offset is 
;   removed.)  
;
;   CAUTION:  The above approach has not been tested on 2x2 or greater
;   	      summed data.
;
;   If the original data has -1 values (indicating missing data), these
;   will be set to -999, as the BLS offset and LUT corrections will result
;   in some negative values, and setting the missing flag to -999 will
;   distinguish statistical variation from true missing data.
;
;   The output index is updated with two tags:  BLS_OFFSET and LUT.  
;
;   Previously LUT corrected images will not be LUT corrected.

;REQUIRED ROUTINES: 
;     GT_SXI_PARAMS to determine the telescope (1,2,3), op (a,b) and
;     amp gain (hi,lo). 
;     SXI_BLS_SUB to subtract off the BLS offset from the image, which is
;     the camera pedestal plus read noise.
;     SXI_BLS_SUB calls SXI_DARKDB
;     SXI_LUTC	to read the LUT coefficients and computed the corrections
;REQUIRED ENVIRONMENTALS:
;     Must define 'SXI_LUT' to access the genx file that contains the
;     LUT coefficients.
;  e.g., set_logenv, 'SXI_LUT', '/ssw/goes/sxig13/response/data/lut/'
;     Must define 'SXI_DARK' to access the genx files that contain the
;     dark database information.
;  e.g., set_logenv, 'SXI_DARK', '~lemen/sxi/darkdb/'
;    
;
;HISTORY:
; 18 Aug 2006, NVN, Written, V0.1
; 29 Sep 2006, JRL, Added oindex, odata, no_lut variables, V0.4
; 23 Dec 2009, JRL, Changed to call sxi_bls_sub V0.6
; 22 Apr 2010, JRL, Experimental version. Multiply (14 Low B) Lut[2,7] by factor
; 22 Aug 2011, JRL, LUT coefficients are updated. (factor not used)
; 21 Oct 2011, JRL, Removed the >0 constraint on the output result
;-

;Version = 'V0.4'
Version = 'V0.6'
if keyword_set(debug) then $
   	print,'********  SXI_LUT_CORRECTION, '+Version+' *********'

; Make sure the index and data arrays are consistent

nti=n_elements(index)
szd=size(data) & nx = szd[1] & ny = szd[2]
if (szd[0] eq 3 and nti ne szd[3]) or (szd[0] eq 2 and nti ne 1) $
	or szd[0] eq 1 or szd[0] gt 3 then begin
   print,'The dimension of index must match the number of images. ' 
   print,'  Specified:  dimension of index = ',strtrim(nti,2),	$
     '   # of images = ',strtrim(szd[3],2)
   return
endif

; Make sure data has not already been LUT corrected
; Lut_flag = -1 ==> not corrected

Lut_flag = strpos(strmid(gt_tagval(index,/lut,missing='No'),0,3),'Yes')

if n_elements(no_lut) ne 0 then begin
   ij = where(no_lut ge 0 and no_lut lt n_elements(index), nc)
   if nc gt 0 then Lut_flag[no_lut[ij]] = 0	; don't correct these
endif

; Setup of the defaults for the optional input keywords:

if n_elements(Missing) ne 0 then qMissing = Missing[0] else begin
  if tag_exist(index,'BLS_OFFSET') then qMissing = -999 else qMissing = -1
endelse
if n_elements(NewMissing) eq 0 then NewMissing = -999 else $
    				      NewMissing = NewMissing[0]

; Call sxi_bls_sub to subtract BLS offset and convert -1s to -999s

    sxi_bls_sub, index, data, oindex, odata, debug=debug, $
		Missing=qMissing, NewMissing=NewMissing

; Add the LUT tag

if not tag_exist(oindex,'lut') then oindex = add_tag(oindex,'No ','LUT')

; Read the coefficients from lut_5poly_coeff into structure LUT
; The LUT structure also contains the original LUT file names

if size(odata,/type) ge 4 then qfloat = 1 else qfloat = 0

for i=0,nti-1 do begin

  if Lut_flag(i) eq -1 then begin	; Yes? Not yet corrected

;; Determine which LUT coefficients should be applied
;   fm  = gt_sxi_params(index(i),/fm)-1     ; 0 for FM1, 1 for FM2, 2 for FM3
;   op  = (gt_sxi_params(index(i),/op))(0)  ; 0 for A, 1 for B
;   amp = (gt_sxi_params(index(i),/amp))(0) ; 0 for Lo and 1 for Hi
;   ilut= 4*fm+ 2^op+ (1-amp)  ; for example, FM1, B and LO --> ilut=3
;   plut0=lut.coefficients(*,ilut)

; Compute the LUT correction

   XXX = sxi_lutc(index[i], odata[*,*,i])
   if not qfloat then XXX = round(XXX)

; If the BLS pixels exist, preserve the camera row
    Status = get_sxi_bls(odata[*,*,i],/force,rot=qrot)

    odata[0,0,i]=odata[*,*,i] + XXX		; Add the LUT correction

    if Status then begin			; Yes, BLS rows exist
      xdata = rotate(odata[*,*,i],qrot) 	; Rotate 
      xdata[0,0] = indgen(588) + 1		; Re-insert the camera row
      odata[0,0,i] = rotate(xdata,qrot)		; Rotate to original orientation

    endif 

; Update the index
    oindex[i].lut= 'Yes, '+Version 

; Convert missing flag values, -1 [Missing], to -999 [NewMissing]

   ij = where(data[*,*,i] eq qMissing, nc)
   if nc gt 0 then odata[nx*ny*i+ij] = NewMissing
   
  endif	else begin			; Lut_flag(i) ne -1
    if keyword_set(debug) then $
      print,i,':  Data LUT corrected or NO_LUT specified--No correction applied'
  endelse

endfor   

; Clobber input variables if oindex & odata are NOT supplied:
if n_params() lt 4 then begin	
  index = oindex
  data  = odata
endif

end
