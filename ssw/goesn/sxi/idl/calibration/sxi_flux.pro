function sxi_flux, Temp, fits_header, FM=FM, date=date, 	$
		   AMP_CCD=AMP_CCD,AMPGAIN=AMPGAIN,T_CCD=T_CCD,	$
                   exposure=exposure, photons=photons, 		$
		   f1pos=f1pos, f2pos=f2pos,			$
		   use_sre_file=use_sre_file,			$
		   apec=apec,feldman=feldman,all_lines=all_lines,$
                   version=version,sre_file=sre_file,		$ ; Output
		   FilterName=FilterName,Gain=Gain, 		$ ; Output
		   prog_version=prog_version			  ; Output
;+
;NAME:		sxi_flux
;PURPOSE:	Return the predicted number of DN as a function of 
;		coronal temperature, filter position, amplifier, and 
;		CCD temperature for GOES-N/O/P SXI.
;
;		The results assume a volume EM = 1.e44 cm^3
;
;		sxi_flux uses information from the appropriate SRE file
;		as read by the function subroutine rd_sxi_sre.  Results
;		using Chianti (with and without /all_lines) and Apec
;		are available for two assumptions of elemental abundances.
;
;		The system gain is obtained from the function sxi_gain. The
;		gain depends on the which flight model is requested (FM#), 
;		amplifier [A or B], amplifier gain [Hi or Lo], and CCD 
;		temperature.  These parameters are normally extracted
;		from the FITS header.
;
;		If the /photons switch is set, return number of photons
;		that are presented to the CCD.  In this case, the amplifier,
;		CCD temperature are not used, as the number of photons is
;		independent of the system gain.
;
;		If the Glass filter is specified, sxi_flux returns 0 DN.
;
;CALLING SEQUENCE:
;  F = sxi_flux(Temp,fits_head)		   ; Defaults to Chianti, Fludra
;  F = sxi_flux(Temp,fits_head,/apec)	   ; Use Apec calculation
;  F = sxi_flux(Temp,fits_head,/Feldman)   ; Use Feldman abundances
;  F = sxi_flux(Temp,fits_head,All_lines=0); Don't use all lines in Chianti
;  F = sxi_flux(Temp,/phot)		   ; Return photons (instead of DN)
;  F = sxi_flux(Temp,fits_head,use_sre_file=sre_file)	; Provide SRE filename
;
; The following examples illustrate how to use sxi_flux in isolation of
; a specific SXI FITS header:
; 
;  F = sxi_flux(Temp,filter_index,info=info)  ; Results by index in SRE file
;  F = sxi_flux(Temp,f1pos=f1pos,f2pos=f2pos) ; Results by specific filter IDs
;  
;
;INPUTS:
;  Temp		= Temperature (K).  May be a scalar or a vector
;
;  fits_header	= If the second parameter is a structure, it is assumed
;			that this is the SXI FITS header.  The SXI instrument
;			number, filter positions, amplifier, and CCD 
;			temperature are determined from the FITS header.
;
;  filter_number= If the second parameter is a scalar or a vector, it is
;			assumed to be equal to the index in the SRE file.
;			The number of unique filters may vary for each 
;			SXI instrument, however, the specified value must
;			be between 0 and 29
;
;			If filter_number is supplied, several optional
;			keywords may be supplied to specify the instrument
;			parameters:  FM, date, amp_CCD, ampgain, T_CCD
;
;
;  Either Temp or fits_header [or filter_number] may be vectors.
;  If both are vectors then the returned array is two dimensional.  If 
;  TEMP=TEMP[i] and fits_header=fits_header[j], then F[i,j] is returned.
;
;OUTPUTS:
;  This function returns DN be default.
;  If /photons is set, return the number of photons that impinge on the CCD.
;
;OPTIONAL INPUT KEYWORDS (if fits_header is NOT specified):
;  All of these parameters must be scalars
;
;  FM		= 1, 2, or 3 to specify SXI on  [Default = 1]
;			GOES N, O, or P
;  date		= Time in any format.  Used to update on-orbit calibration
;		  NOTE: Ues of date is not currently implemented
;  amp_CCD	= Output amplifier (A or B) 	[Default = 'B']
;  ampgain	= Camera gain (Hi or Lo)	[Default = 'Lo']
;  T_CCD	= CCD temperature in C		[Default = -40.]
;  f1pos,f2pos	= Filterwheel positions if 	[Default = 1,4]
;		  filter_number input parameter
;		  is not specified.		
;		  Valid values are between 1 and 6
;  exposure     = The exposure time in sec	[Default = 1.0]
;
;OTHER OPTIONAL INPUT KEYWORDS:
;  All of these parameters must be scalars
;
;  photons      = If set, return  photon flux (rather than DN).
;  use_sre_file = If provided, read the specified SRE file.
;		   Must include the entire pathname.  This will override
;		   the FM= keyword and the three atomic keywords (Apec,
;		   Feldman, and all_lines).
;  Apec		= If set, use the Apec calculation instead of Chianti
;  Feldman	= If set, use Feldman abundances instead of Fludra
;  all_lines	= Default is set.  Specify all_lines = 0, to use the
;		  limited line list calculation (only relevant for Chianti)
;
;OPTIONAL OUTPUT KEYWORDS:
;  version      = version number of the [First] SRE file
;  sre_file	= Name of the [first] SRE file from which data is extracted.
;		  If fits_header is supplied, it is possible that multiple
;		  FM instruments are included, and thus multiple SRE file will
;		  be used.  However, only the first file name is returned.
;  FilterName	= The name of the filter returned by gt_sxi_params.  This
;		  is especially helpful when sxi_flux is called with the
;		  filter_number or f1pos=,f2pos= parameters.
;  Gain		= The gain (e-/DN) used for each filter (if /phot not set)
;  prog_version	= A structure containing the version of this program and the
;		  version of the sxi_gain program
;
;MODIFICATION HISTORY:
; 11-Jul-2006, J. R. Lemen, V1.00  Written (first release)
;-

prog_version = {sxi_flux:1.00, sxi_gain:0.}	; sxi_gain is updated below

@sxi_sre_common
;common sxi_sre_db, sxi_sre_common, Filename_common, Full_filename_common

; if no parameters are present, assume information mode

if n_params() lt 1 then begin
  print,format="(75('-'))"
  doc_library,'sxi_flux'
  print,format="(75('-'))"
  return,-1
endif


;----------------------------------------------------------------------------
;  ****  Step 1:  Set up values optional input parameters:
;----------------------------------------------------------------------------

n_Temp = n_elements(Temp)
if n_Temp eq 0 then message,'Temp variable is undefined'
n_Filt = n_elements(fits_header)

; qcase = 1 ==> Fits_header = structure
; qcase = 2 ==> Fits_header = filter index
; qcase = 3 ==> Filters provided in f1pos and f2pos

if n_Filt eq 0 then begin
  qcase = 3
  if n_elements(f1pos) ne 1 or n_elements(f2pos) ne 1 then $
    message,'F1POS and F2POS must be scalar variables' else n_Filt = 1
endif else begin
  if size(fits_header,/type) eq 8 or $
     size(fits_header,/type) eq 9 then qcase=1 else qcase=2
endelse

case qcase of 
  1: begin					; FITS header structure 
	qFM = gt_sxi_params(Fits_header,/FM)
	qf1 = gt_sxi_params(Fits_header,/f1pos)
	qf2 = gt_sxi_params(Fits_header,/f2pos)
     end
  2: begin					; SRE index provided
	if n_elements(FM) eq 0 then qFM = 1 else qFM = FM
     end
  3: begin					; f1pos and f2pos provided
	if n_elements(FM) eq 0 then qFM = 1 else qFM = FM
	if n_elements(f1pos) eq 0 then qf1 = 1 else qf1 = f1pos
	if n_elements(f2pos) eq 0 then qf2 = 4 else qf2 = f2pos
     end
endcase

; *** Set up the remaining return keywords ***

FilterName = strarr(n_Filt)
Gain = fltarr(n_Filt)

;----------------------------------------------------------------------------
;  ****  Step 2:  Loop on filter counter (e.g. FITS header)	
;----------------------------------------------------------------------------

for i=0,n_Filt-1 do begin

  sre = rd_sxi_sre(Fits_header, FM=qFM, date=date, 		$
               use_sre_file=use_sre_file, apec=apec,           	$
                Feldman=Feldman, All_lines = All_lines,         $
                debug=debug,                                    $
                sre_filename=sre_file1)                          ; Output
  if i eq 0 then begin
	sre_file = sre_file1
        version = sre.version
        if qcase ne 2 then index1 = sxi_fwpos(qFM,f1pos=qf1,f2pos=qf2,name=name)
        outarr = fltarr(n_elements(Temp),n_Filt)
  endif

  if qcase eq 2 then index = Fits_header[i] else index = index1[i]

  if index ne -1 then FilterName[i] = sre.name[index] else $
    		      FilterName[i] = 'Glass'
  
  if keyword_set(photons) then begin
    if index ne -1 then outarr[0,i] = 	$
      	dspline(sre.Temp,sre.phot[*,index,0],Temp,interp=0)
  endif else begin
    if qcase ne 1 then begin
       if n_elements(T_CCD) eq 0 then T_CCD = -40.	; Default CCD temp
       if n_elements(Ampt_CCD) eq 0 then Amp_CCD = 'B'	; DEfault CCD Amp
    endif

; *** Apply the appropriate gain (e-/DN)        ***

    if qcase eq 1 then Fits = Fits_header
    qgain = sxi_gain(Fits, FM=qFM, date=date,				$
                                 Amp_CCD=Amp_CCD,T_CCD=T_CCD,		$
                                 ampgain=ampgain, 			$
                		 prog_ver=prog_ver)	; Elec/DN

    if qcase eq 1 then gain[i] = qgain[i] else gain[i] = qgain

    if index ne -1 then outarr[0,i] = 	$
	dspline(sre.Temp,sre.elec[*,index,0],Temp,interp=0)/gain[i]

    if i eq 0 then prog_version.sxi_gain = prog_ver

  endelse

; *** Apply the exposure correction		***

  if qcase eq 1 then qExp = gt_sxi_params(fits_header[i],/exposure) else $
    if n_elements(exposure) ne 0 then qExp = exposure else qExp = 1.0
  outarr[*,i] = outarr[*,i] * qExp

endfor 				; End of main loop: i=0,n_Filt-1 

return, outarr
end
