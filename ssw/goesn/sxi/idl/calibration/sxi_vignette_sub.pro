pro sxi_vignette_sub, index, data, oindex, odata,vfcn

;+
;NAME:	SXI_VIGNETTE_SUB
;
;PURPOSE:
;	Subtract the vignette correction from SXI image.	
;
;METHOD:	
;	1)Read Zerodur .dat file into matrix, reflectivity(R), 
;	wavelength(lambda)in angstroms, incidence angle(theta) in degrees 
;	2)Create vector of reflectivities for a given wavelength
;	for all values of theta
;	3)Generate cubic spline coefficients 
;	4)Compute the effective area integral (Spiga etal)
;	5)Compute vignette correction function,
;	Vcfn, which is saved to vcfn.genx
;	6)Save file is restored and correction applied input data and returns
;	vignette corrected data with tag
;INPUTS:
;       INDEX & DATA    SXI-13 Fits Header and Data cube
;
;OUTPUTS:
;       INDEX  & DATA  if OINDEX & ODATA not supplied (inputs are clobbered)
;       OINDEX & ODATA  Updated Fits Header and Data cubes
;
;PROCEDURE:
;
;	Creates a vignette flatfield for a given wavelength, using the Spiga etal 
;	method by calculating the effective area integral and dividing by the grazing 
;	angle. Reflectivity data is read from stored file. Correction is applied to 
;	output image.
;
;HISTORY:
;	Written by Shelbe Timothy Jun 20, 2016
;
;
;


;Restore .genx
;
restgenx,file='vfcn.genx', vfcnindex, vfcn
nfiles = n_elements(index)
print, nfiles
odata = data
;oindex=index

for i=0,nfiles-1 do begin
oindex =add_tag(index,0., 'VFCN') ;add tag

;find image size
	width = n_elements(data[*,0,i])
	height = n_elements(data[0,*,i])

	xpix = findgen(width)-(width/2)
	ypix = findgen(height)-(height/2)

;calculate offset angle in arcsec
	;osangle = fltarr(width,height)
	;help, osangle
	;for i = 0,width-1 do begin 
	;	osangle[i,*] = sqrt((xpix[i]*index.cdelt1)^2 + (ypix*index.cdelt2)^2)
	;endfor

	;osangle = osangle/3600 ;convert to deg

;Vfcn
;Apply correction, add tag
	odata[*,*,i] = data[*,*,i] - Vfcn[*,*]
	;oindex =add_tag(index,0., 'VFCN') ;add tag

endfor
	
if N_params() eq 2 then begin
  	index = oindex  
	data  = odata
endif



END

