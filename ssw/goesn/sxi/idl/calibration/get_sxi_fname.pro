function get_sxi_fname, time, g13=g13, g14=g14, g15=g15, goesnn=goesnn,	$
			twindow=twindow, valid=valid
;+
;NAME:		get_sxi_fname
;PURPOSE:	Given a UT time, return the file name of the SXI image
;		that is closest to the given time
;
;CALLING SEQUENCE:
;   Files = get_sxi_fname(time, /g13)
;
;   Files = get_sxi_fname(Fits_header)		; Fits_header is a structure
;

;
;INPUT:
;   Time	- A vector of times
; or 
;   Fits_header - A FITs header or sxi_cat style structure
;                 If Fits_header is supplied, the routine will determine 
;		  whether it is g13, g14, or g15 from the structure
;
;OPTIONAL INPUTS:
;  goesnn	 - 13, 14, or 15
;  g13, g14, g15 - Set to choose the instrument.  If not supplied,
;                  default to /g13
;
;  twindow	 - Search +/-twindow seconds from the supplied time.  If
;                  no file is found within the twindow, the returned
;                  fill name will be a null string: ""  Defaults to 1 sec
;
;OPTIONAL OUTPUTS:
;  valid	- Vector set to 1 if filename is found within +/-twindow
;                 or 0 if no file is found
;
;PROCEDURE:
;  Call sxi_cat search for closest file
;
;HISTORY:
; 10-Jun-2010, J. R. Lemen, Written
;  4-Mar-2011, J. R. Lemen, Added GOESNN keyword
;-


if n_elements(twindow) eq 0 then twind = 1. else twind = abs(twindow)

if datatype(time,2) eq 8 then begin
   tt = gt_sxi_params(time, /atime) 
   if have_tag(time, 'telescop') then goesnn = fix((gt_sxi_params(time, /fm))[0])
endif else tt = atime( time )


case 1 of
   keyword_set(g13): goesnn=13
   keyword_set(g14): goesnn=14
   keyword_set(g15): goesnn=15
   keyword_set(goesnn): 	; Use structure value
   else: goesnn = 13		; Default to G13
endcase

T0 = atime(addtime(TT[0],del=-1*Twind,/sec))
T1 = atime(addtime(last_nelem(TT), del=Twind,/sec))

sxi_cat,goesnn=goesnn,T0, T1, cat, files	; Assume something is returned

TX = addtime(gt_sxi_params(cat,/atime),diff = gt_sxi_params(cat[0],/atime),/sec)
TY = addtime(TT,diff=gt_sxi_params(cat[0],/atime),/sec)

npts = n_elements(TT)
fname = strarr(npts)
valid = intarr(npts)

for i=0,npts-1 do begin
  if min(abs(TY[i] - TX),ic) le Twind then begin
     fname[i] = files[ic]
     valid[i] = 1
  endif
endfor

return, fname
end
