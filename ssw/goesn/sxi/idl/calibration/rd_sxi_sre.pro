function	rd_sxi_sre, Fits_header, FM=FM, date=date, 	$
		use_sre_file=use_sre_file, apec=apec, 		$
		Feldman=Feldman, All_lines = All_lines, 	$
                debug=debug,			 		$
                sre_filename=sre_file1				; Output

;+
;NAME:		rd_sxi_sre
;PURPOSE:	Read the appropriate SRE file that contains the
;		emission response function for the GOES-N/O/P SXI
;
;		The SRE data structure is returned.
;
;		This routine is called by sxi_flux
;
;CALLING SEQUENCE:
;
; sre = rd_sxi_sre(Fits_header)
; sre = rd_sxi_sre(FM=1, date=date)
; sre = rd_sxi_sre(Fits_header, use_sre_file = sre_filename); Provide filename
; sre = rd_sxi_sre(Fits_header,/apec,/Feldman,/all_lines)
; sre = rd_sxi_sre(Fits_header, sre_file=sre_filename)	; Return filename used
;
; Either an SXI Fits_header or the FM= keyword must be supplied 
;
; The date= keyword is included for future possible use, but is
; not currently used.
;
; Note: All input parameters are assumed to be scalars
;
;INPUT PARAMETERS:
; Fits_header		= SXI FITS file header from which the Flight
;			  model (FM) number is extracted and the 
;			  date (if the instrument response function
;			  changes with time)
;
;OPTIONAL INPUT KEYWORDS:
; FM			= Flight model number.  FM overrides the value found
;			  in Fits_header if Fits_header is present
; Date			= Date of the data.  Date ovverides the value found
;			  in Fits_header if Fits_header is present
;
; use_sre_file		= If present, read the specified file regardless
;			  of all other keywords
;
; Apec			= If set, use the Apec calucation instead of Chianti
; Feldman		= If set, use Feldman abundances instead of Fludra
; all_lines		= Use all_lines = 1 for  fm#_sre_chianti2_*genx file
;			  Use all_lines = 0 for  fm#_sre_chianti1_*genx file
;			  Default is /all_lines
;
;OPTIONAL OUTPUT KEYWORDS:
; sre_filename		= The full path name of the SRE file that was read.
;
;PROCEDURE:
; Unless USE_SRE_FILE input keyword is specified, rd_sxi_sre will
; assume that target directory for the sre files is defined by 
; $SXI_RESPONSE.  If file name is passed in explicitly via use_sre_file, 
; the full file path name must be explicitly provided.
;
; After the first call, will check to see if the file name of the
; file to be read is different than the current file.  If not, no
; read is performed and the contents of the previously read file 
; are returned.
;
;RESTRICTIONS:
; All input parameters and keywords must be scalars.
;
;COMMON BLOCKS:
; sxi_sre_common	Contains the contents of the SRE file.
;
;HISTORY: 
; 10-Jul-2006, J. R. Lemen, Written
;-

@sxi_sre_common
;common sxi_sre_db, sxi_sre_common, Filename_common, Full_Filename_common

target_dir = '$SXI_RESPONSE'	; Default target directory for SRE files

; --- Step 1:  Create the target file name ------------------------


if keyword_set(use_sre_file) then begin
  target_filename = use_sre_file
endif else begin
  if keyword_set(FM) then qFM   = FM else $
		          qFM   = gt_sxi_params(Fits_header, /FM)  
  qFM = qFM[0]		; In case a scalar was not passed in

; ****************************************************************
; JRL: The date keyword is not currently used 
;
; If the response function of the SXI changes with time, it may
; be necessary to uncomment the following lines and to change
; the algorithm to determine the sre filename.
;
;  if keyword_set(date) then qdate = date else $
;		          qdate = gt_sxi_params(Fits_header, /date)
; ****************************************************************


  code = ['chianti','apec']
  if n_elements(all_lines) eq 0 then q_all = 1 else q_all = all_lines
  if keyword_set(apec) then alines = ['', ''] else alines = ['1','2']
  abun = ['fludra','feldman']
  target_filename = string('fm',strtrim(qFM,2),'_sre_',		$
			code[keyword_set(apec)],		$
			alines[keyword_set(q_all)]+'_',		$
			abun[keyword_set(Feldman)],		$
			'_mazzotta_*.geny')
  target_filename = concat_dir( target_dir, target_filename )
endelse

; --- Step 2:  Search for the target file name ------------------------

files = file_search(target_filename,count=count,/expand_environ)

if count eq 0 then begin
  message,'No sre files found: ', /cont
  print,' ' + target_filename
  return, -1
endif

;  More than one version of the file may exist.
;
;  The following statement chooses the "highest" version (assuming
;  that the conversion of version = 001, 002, ... is followed).

infile = files[count-1]

; --- Step 3:  Read the file if this is the first call ----------------
;              or read the file if a different file is requested

break_file, infile, disk_log, dir, filnam, ext
if n_elements(Filename_common) eq 0 then Filename_common = ''
if filnam ne Filename_common then begin
  restgenx, file=infile, sxi_sre_common
  Filename_common=filnam		; Set common variables
  Full_Filename_common = infile
debug = 1
  if keyword_set( debug ) then print,'** Reading = ',filnam
endif

sre = sxi_sre_common
sre_file1 = Full_Filename_common

return, sre
end
