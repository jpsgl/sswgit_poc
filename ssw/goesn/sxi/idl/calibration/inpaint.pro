pro biharmonic_sweep,repaired,mask,minx,maxx,miny,maxy
  for j=miny,maxy do begin
     m=mask[minx:maxx,j]
      if max(m) gt 0 then begin ; only do lines that have something to fix
         c=-8.*m
         a=-8.*m
         b=20.*m+(1.-m)
     
         rhs=-(-8*(repaired[minx:maxx,j+1]+repaired[minx:maxx,j-1])+(repaired[minx:maxx,j-2]+repaired[minx:maxx,j+2]+repaired[minx-2:maxx-2,j]+repaired[minx+2:maxx+2,j]) $
               +2*(repaired[minx-1:maxx-1,j-1]+repaired[minx-1:maxx-1,j+1]+repaired[minx+1:maxx+1,j-1]+repaired[minx+1:maxx+1,j+1]))*m $
             +repaired[minx:maxx,j]*(1-m)
         repaired[minx,j]=trisol(a,b,c,rhs)
      endif
  endfor
end

pro laplacian_sweep,repaired,mask,minx,maxx,miny,maxy
  for j=miny,maxy do begin
     m=mask[minx:maxx,j]
     if max(m) gt 0 then begin ; only do lines that have something to fix
        a=float(m)
        c=a
        b=-4.*m+(1.-m)
        rhs=-(repaired[minx:maxx,j+1]+repaired[minx:maxx,j-1])*m+repaired[minx:maxx,j]*(1-m)
        
        repaired[minx,j]=trisol(a,b,c,rhs)
        endif
  endfor
end

function add_noise,image,mask,noise=noise
if not keyword_set(noise) then noise=1.0
repaired=image
tofix=where(mask eq 1)
rms=stddev(image[10:110,10:110]>0)
avg=mean(image[10:110,10:110]>0)
repaired[tofix]=repaired[tofix]+noise*rms*(randomu(seed,n_elements(tofix))-0.5)*sqrt(avg/(repaired(tofix)>1.))
return,repaired
end


function inpaint,image,mask,order=order,niter=niter,tol=tol,reuse=reuse,watch=watch,quiet=quiet,frame=frame,fvalue=fvalue
; a simple inpaint routine based on harmonic and biharmonic equations
; where laplacian(image)=0 or laplacian^2(image)=0 in masked regions
;
; Uses a relaxation method to iterate toward solution.
; INPUT
; image=image to be fixed
; mask=mask of bad pixels (1=bad, 0=good) same size as image
; KEYWORDS
; order=order of method
;   0 (default) is harmonic, 1 is biharmonic
; niter=number of iterations in relaxation (default=1000)
; tol=tolerance for error estimate (default=image median/10^4)
; reuse= set to reuse previous solution (default: don't)
; watch= set to display iterations (default: don't)
; quiet= set to be quiet
; frame= number of pixels forming frame
; fvalue= value for the image frame in cases where the area surrounding the mask are bad,
; e.g. bad columns
; AUTHOR
;   Neal Hurlburt LMSAL
;   7/15/2016
; NOTES
;   A better solver would help improve the speed.
;
;  And the image outside of the mask area should be reasonable: in the
;  case of SXI, at least two rows in the image on either size of the
;  mask should be zeroed in at least the bad rows

sz=size(image) & nx=sz[1] &ny=sz[2]
if not keyword_set(niter) then niter=1000
if not keyword_set(order) then order=0
if not keyword_set(tol) then tol=1e-4

tofix=where(mask eq 1)
ix=tofix mod nx
iy=tofix/nx

maxy=(max(iy,min=miny)+1)
maxx=(max(ix,min=minx)+1)
miny=(miny-1)
minx=(minx-1)
if (minx lt 2 or miny lt 2 or maxx gt nx-2 or maxy gt ny-2) then frame=2; we need a frame!

repaired=float(image) ; doesn't work with integers, and double seems like overkill

if keyword_set(frame) then begin
   if not keyword_set(fvalue) then fvalue=0
   repaired(0:frame-1,*)=fvalue
   repaired(nx-frame:*,*)=fvalue
   repaired(*,0:frame-1)=fvalue
   repaired(*,ny-frame:*)=fvalue
   maxy=maxy<(ny-frame-1); crop the mask around the edges
   maxx=maxx<(nx-frame-1)
   miny=(miny)>frame
   minx=(minx)>frame
endif

if not keyword_set(reuse) then begin
   not_tofix=where(mask eq 0)
   repaired[tofix]=mean(image[not_tofix])
endif
change=100
iter=0
old=image
maskt=transpose(mask)

while (change gt tol and iter lt niter) do begin
   case order of
      0: begin 
         laplacian_sweep,repaired,mask,minx,maxx,miny,maxy 
         repairedt=transpose(repaired)
         laplacian_sweep,repairedt,maskt,miny,maxy,minx,maxx 
      end
      else: begin
         biharmonic_sweep,repaired,mask,minx,maxx,miny,maxy         
         repairedt=transpose(repaired)
         biharmonic_sweep,repairedt,maskt,miny,maxy,minx,maxx         
      end
   endcase
   repaired=transpose(repairedt)
   change=max(abs(repaired-old))/median(repaired)
   
   if (not keyword_set(quiet)) and iter mod 100 eq 0 then begin
      print,'change=',change,iter
      if keyword_set(watch) then tvscl,repaired
   endif
   old=repaired
   iter=iter+1
endwhile

return,repaired
end
