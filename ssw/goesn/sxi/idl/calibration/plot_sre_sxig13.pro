;+
;NAME:		plot_sre_sxig13
;PURPOSE:	Plot the sre files for GOES SXI-N/O/P
;		The sre files contain the SXI response as a function of
;		temperature for each filter
;
;HISTORY:
; 16-Mar-2006, J. R. Lemen, Written
; 19-Mar-2006, N. V. Nitta, modified
; 21-Mar-2006, N. V. Nitta, added default output file name
; 27-Mar-2007, N. V. Nitta, removed definition of SXI_RESPONSE 
;-



spawn,'ls $SXI_RESPONSE/*sre*geny',aa
bb='$SXI_RESPONSE/'+file_break(aa)
nbb=n_elements(bb)
print,'The following sre files exist.'
for i=0,nbb-1 do print,i+1,bb(i),format='(i2,3x,a)'

if n_elements(nsre) eq 0 then nsre=1


input,'* Enter number of the sre file', nsre, nsre, 1, nbb

file=bb(nsre-1)
fm=strmid(file_break(file),2,1)

; Read the sre file

restgenx,file=file,sre

linecolors

if !d.name eq 'PS' then begin
  qfont = 0
  outfil='./sxi_'+strmid(file_break(file),0,strlen(file_break(file))-5)+'.ps'
  print,'device = ps     output file = ',outfil
  device,/color,set_font='Helvetica-Bold',file=outfil
endif else begin
  qfont = -1
endelse

; nfilt = 8
s0=where(sre.name eq 'Open/Thin_poly')
if s0 ne -1 then sfilt=s0(0)
s1=where(sre.name eq 'Tin/Open')
if s1 ne -1 then sfilt=[sfilt,s1(0)]
s2=where(sre.name eq 'Thick_poly/Open')
if s2 ne -1 then sfilt=[sfilt, s2(0)]
s3=where(strmid(sre.name,0,12) eq 'Open/Be12') ; FM3 has the same be_thin 
if s3 ne -1 then sfilt=[sfilt, s3(0)]
s4=where(sre.name eq 'Open/Al12')
if s4 ne -1 then sfilt=[sfilt, s4(0)]
s5=where(sre.name eq 'Open/Be50')
if s5 ne -1 then sfilt=[sfilt, s5(0)]
s6=where(sre.name eq 'Open/Silver')
if s6 ne -1 then sfilt=[sfilt, s6(0)]
nfilt=n_elements(sfilt)

color=[255, 2, 9, 13, 7, 10, 4]
if !d.name eq 'PS' then color(0)=0

plot_oo,sre.temp/1e6,sre.elec[*,sfilt(0)],		$
	xtitle='Temperature (MK)',			$
  	ytitle='Electrons for EM=10!u44!n cm!u3!n',		$
	title='SXI FM'+fm+' Response with Temperature',	$
  	yrange=[1,3e5],xrange=[0.1,100],font=qfont, $
        xstyl=1,ystyl=1,color=color(0),thick=2

for i=1,nfilt-1 do oplot,sre.temp/1e6,sre.elec[*,sfilt(i)],	$
  	color=color[i],thick=2

for i=0,nfilt-1 do xyouts,.18,.88-.04*i,sre.name[sfilt(i)],font=qfont,	$
  	color=color[i],/norm


if !d.name eq 'PS' then begin
    xyouts,.05,-.05,string('SRE file =  $SXI_RESPONSE/',file_break(file)),font=qfont,/norm
    device,/close
endif else begin
    xyouts,.05,.003,string('SRE file =  $SXI_RESPONSE/',file_break(file)),/norm,font=qfon   
   wshow
endelse
end
