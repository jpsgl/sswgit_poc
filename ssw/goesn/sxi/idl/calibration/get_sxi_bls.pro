function get_sxi_bls, data, bls=bls, offset=offset, 	$
		rms=rms, force_miss=force_miss, rotate=qrotate
;+
;NAME:		get_sxi_bls
;PURPOSE:	Extract the BLS columns from an SXI image
;
;CALLING SEQUENCE:
;	Status = get_sxi_bls(data,bls=bls)
;	Status = get_sxi_bls(data,offset=offset,qrotate=qrotate)
;
;		The array must be 2-D and 588x528
;
;		The routine look for the camera row counter in the first
;		row of data:  data[*,0].  If this is not the row
;		counter, it will look for the row in (rotate(data),2))[*,0],
;		that is, the last row of data.
;
;RESTRICTIONS:
;	1. data must be a 2-d, 588x528 array
;	2. data must not be background subtracted
;	3. Unless force_miss is set, will return Status=0 if there are
;	   any negative values in data (indicating missing data)
;
; This routine returns the offset based on the mean of most of the bls pixels:
;
;	offset = mean(bls[32:528,2:7])
;
;INPUTS:	
;	data		The 2-D input array
;	force_miss	Set to force the routine to try to return the
;			bls pixels even if there are negative
;			(i.e. missing) values in data
;OUTPUTS:
;	bls		A 8x588 array containing the BLS pixels(does not
;			include the camera row counter)
;	offset		The offset   cacluated as  mean(bls[32:528,2:7])
;	rms		Standard dev calcuated as stdev(bls[32:528,2:7])
;	rotate		The value of rotate that is applied (0, 2, 5, or 7)
;			to put camera row counter in the 0th row, ascending.
;	Status		1 if BLS array is returned.  0 if not.
;
;			0 is returned if the array is not 2-D
;			0 is returned if the array is not 588x528
;			0 is returned if the row counter can not be
;			  identified.  This might happen if there is bad
;			  or missing data and /force_miss is not set, in
;			  which case, the check may fail.
;
;HISTORY:
; 5-Nov-2009, J. R. Lemen, Written
;21-Dec-2009, J. R. Lemen, Changed some keywords
;11-Jun-2010, J. R. Lemen, Added rms keyword.  Eliminated first two BLS
;				columns when computing the mean and stdev
;29-Nov-2012, J. R. Lemen, Trap a case where all values are negative for an
;				orientation that is not the camera row counter
;-

Status = 1			; Assume success
delvarx, bls, offset, qrotate	; Clear the output arrays

sz = size(data)

; Check to see if the array is the right size
if sz[0] ne 2   then Status = 0
if sz[1] ne 588 then Status = 0
if sz[2] ne 528 then Status = 0

; Check for missing data
if min(data) lt 0 then miss = 1 else miss = 0

if miss and not keyword_set(force_miss) then Status = 0
if Status eq 0 then return,Status



rdata = data			; The array rotated, as necessary

; Search for the location of the camera row counter line

rowcnt = indgen(588) + 1	; Set up the row count vector

Status = 0
rr = [0,2,5,7]
for i=0,3 do begin		; Look at four possible orientations
  if not Status then begin
    rdata = rotate(data,rr[i])
    if keyword_set(force_miss) then ss = where(rdata[*,0] ge 0) else $
 				    ss=indgen(588)

; If ss[0]= -1, then force_miss must have been set and all values were
; negative.  In this case, it is clearly not the camera row counter, so
; set xmin= -1 to skip to the next case.

    if ss[0] eq -1 then begin
       xmin = -1 & xmax = -1
    endif else xmin = min(rowcnt[ss]-(rdata[*,0])[ss],max=xmax)

    if xmin eq 0 and xmax eq 0 then begin
       Status = 1		; Found the correct orientation
       qrotate=rr[i]
    endif
  endif
endfor

if not Status then return, Status 	; Could not find the camera row counter

; Extract bls columns and compute mean offset

bls = rdata[*,1:8]		; Extract the BLS columns

xoffset = bls[32:528,2:7]	; Extract a portion to compute the offset
ss = where(xoffset ge 0)	; Don't include the missing data in the mean
offset = mean(xoffset[ss])
rms    = stdev(xoffset[ss])

return, Status
end
