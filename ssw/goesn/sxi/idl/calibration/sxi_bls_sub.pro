pro sxi_bls_sub, index, data, oindex, odata, debug=debug,	$
                missing=missing, newmissing=newmissing 
;+ 
;NAME: 		SXI_BLS_SUB
;
;PURPOSE: 	Subtract the BLS offset from the entire SXI image.
;
;		Two different methods are used. 
;
;		Method 1:  If the image is a 588x528 array including 
;		the BLS pixels, the mean of the BLS pixels are used 
;		unless.  Calls get_sxi_bls.  For this method, the DC
;		term that is subtracted includes the camera pedestal
;		and the read noise.
;
;		Method 2:  If the image does not include the BLS pixels, 
;		then call sxi_darkdb to read and interpolate the dark
;		database that is generated by mk_sxi_dark_db 
;
;CALLING SEQUENCE: 
;		SXI_BLS_SUB, INDEX, DATA
;		SXI_BLS_SUB, INDEX, DATA, OINDEX, ODATA
;		SXI_BLS_SUB, INDEX, DATA, OINDEX, ODATA, MISSING=MISSING
;		SXI_BLS_SUB, INDEX, DATA, MISSING=MISSING, NEWMISSING=NEWMISSING
;
;INPUTS: 
;	INDEX & DATA	SXI-13 Fits Header and Data cube
;
;OUTPUTS: 
;	INDEX  & DATA  if OINDEX & ODATA not supplied (inputs are clobbered)
;	OINDEX & ODATA	Updated Fits Header and Data cubes
;
;OPTIONAL INPUT KEYWORD:
;	MISSING	- A scalar value that flags the missing data in DATA.
;                 Default (unsubtracted): Missing = -1
;	NEWMISSING - Default is -999 (This is also an output variable)
;
;PROCEDURE:
;
;   If the BLS_OFFSET keyword already exists, no operation will occur unless
;   the value is 0.
;
;   If the original data has -1 values (indicating missing data), these
;   will be set to -999 (if MISSING and NEWMISSING keywords are not
;   specified).
;
;   The output index is updated with a the tag BLS_OFFSET and the value that
;   was subtracted.  
;
;
;REQUIRED ROUTINES: 
;     get_sxi_bls, sxi_darkdb
;
;HISTORY:
; 22 December 2009, J. R. Lemen, V0.92, Written (based on sxi_dc_sub)
; 11 June 2010, J. R. Lemen, V0.93, Continue with method 2 if BLS pixels
;  				are not found.  Add bls_rms tag.
;-

Version = 'V0.93'
if keyword_set(debug) then print,'********  SXI_BLS_SUB, '+Version+' *********'


; Make sure the index and data arrays are consistent

nti=n_elements(index)
szd=size(data) & nx = szd[1] & ny = szd[2]
if (szd[0] eq 3 and nti ne szd[3]) or (szd[0] eq 2 and nti ne 1) $
	or szd[0] eq 1 or szd[0] gt 3 then begin
   print,'The dimension of index must match the number of images. ' 
   print,'  Specified:  dimension of index = ',strtrim(nti,2),	$
     '   # of images = ',strtrim(szd[3],2)
   return
endif


; Prepare the output variables
oindex	= index
odata	= data
if size(odata,/type) ge 4 then qfloat = 1 else qfloat = 0

; Setup of the defaults for the optional input keywords:

if n_elements(Missing) ne 0 then qMissing = Missing[0] else qMissing = -1
if n_elements(NewMissing) eq 0 then NewMissing = -999 else $
    				      NewMissing = NewMissing[0]


; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; Make sure data has not already been BLS subtracted
if tag_exist(index,'bls_offset') then begin	; Data already BLS subtracted
  BLS_offset = gt_tagval(index,/bls_offset)
endif else begin				; Data not DC subtracted
  qDC = 1
  bls_offset = fltarr(nti)			; Set up BLS_OFFSET array
  oindex = add_tag(oindex,0.,'BLS_OFFSET')	; Add new tag
  bls_rms = fltarr(nti)				; Set up BLS_OFFSET array
  oindex = add_tag(oindex,0.,'BLS_RMS')		; Add new tag
endelse

if keyword_set(debug) then $
    print,'Missing,Newmissing = ',qMissing, NewMissing

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; Begin the main loop

for i=0,nti-1 do begin	; Loop over the images


  if BLS_offset[i] eq 0 then begin	; Only perform if BLS_offset =0
    nx=index[i].naxis1
    ny=index[i].naxis2

; Determine which method to use
    Status = get_sxi_bls(odata[0:nx-1,0:ny-1,i],off=off,rms=rms,/force,rot=qrot)

    if Status then begin	; Yes, Method eq 1
      BLS_offset[i] = off
      BLS_rms[i] = rms
      if qfloat then off = BLS_offset[i] else off = round(BLS_offset[i])

      xdata = rotate(odata[0:nx-1,0:ny-1,i],qrot) - off
      xdata[0,0] = indgen(588) + 1		; Re-insert the camera row
      odata[0,0,i] = rotate(xdata,qrot)		; Rotate to original orientation
    endif else begin		; No, Mmethod eq 2

      if nx eq 588 and ny eq 528 then 	$
		message,'** Could not find BLS pixels' ,/cont

      Ierror = sxi_darkdb(index[i],bls_offset=off,bls_rms=rms,	$
		delta_days=delta_days)
      if Ierror ne 1 or abs(delta_days) gt 2 then begin
        message,string('Problem with call to sxi_darkdb:  Ierror=',Ierror),/cont
        print,' Delta_days = ',Delta_days
        stop
      endif

      BLS_offset[i] = off		
      BLS_rms[i]    = rms

      if not qfloat then off = round(off)
      odata[0,0,i]=odata[*,*,i] - off[0] ; Off comes back as a vector of [1]

    endelse			; Method eq 2

; Update the index
     oindex[i].BLS_offset = oindex[i].BLS_offset + BLS_offset[i] 
     oindex[i].BLS_rms    = sqrt(oindex[i].BLS_rms^2 + BLS_rms[i]^2)

; Convert missing flag values, -1 [Missing], to -999 [NewMissing]

    ij = where(data[*,*,i] eq qMissing, nc)
    if nc gt 0 then odata[nx*ny*i+ij] = NewMissing
   
  endif else if keyword_set(debug) then begin
	print,'sxi_BLS_sub:  Data are BLS subtracted--No subtract applied'
        print,gt_sxi_params(index[i])
  endif

endfor   			; i=0,nti-1 Loop over the images

; Clobber input variables if oindex & odata are NOT supplied:
if n_params() lt 4 then begin	
  index = oindex
  data  = odata
endif


end

