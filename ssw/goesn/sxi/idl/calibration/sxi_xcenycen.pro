pro sxi_xcenycen, index, oindex, clobber=clobber, debug=debug, xcen=xcen, ycen=ycen
;+
;NAME:    sxi_xcenycen
;
;PURPOSE: Fix the value of xcen, ycen
;
;METHOD:  PREP DATA
;
;CALLING SEQUENCE:
;         sxi_xcenycen, index
;         sxi_xcenycen, index, /clobber
;         sxi_xcenycen, index, oindex
;
;INPUTS:
;         index - (sxi) catalog structure with .crpix1, .crpix2, .xcen, .ycen
;
;OUTPUTS:
;         index (if /clobber keyword is set, then a modified index is returned.)
;         oindex (if parameter provided)
;
;OPTIONAL INPUT KEYWORD:
;         clobber -- modify and return the original index parameter.    
;         debug   -- print version number
;
;PROCEDURE:
;           If invoked, use index values of .crpix1 and .crpix2 to update .xcen and .ycen in index.
;
;REQUIRED ROUTINES:
;
;HISTORY:
;         07 July 2016 PG Shirts
;-

Version = 'V0.91'
if keyword_set(debug) then print,'********  SXI_XCENYCEN, '+Version+' *********'
parent_proc = 'sxi_xcenycen'

box_message, [parent_proc, 'sxi_xcenycen.pro is currently a stub.', 'No action has been performed on the input index', 'It will be updated when algorithm is complete.']

if tag_exist(index, 'mod_xcenycen') then begin
  box_message, [parent_proc, 'The input index .xcen and .ycen were previously modified', 'Returning with no modification to .xcen, .ycen values'];is this necessary?
  if (n_params() gt 1) then index_mod = index
  return
endif

index_mod = index
;index_mod.xcen = function of (index.crpix1) ;this is the meat of the program
xcen = index_mod.xcen
;index_mod.ycen = function of (index.crpix2) ;this is the meat of the program
ycen = index_mod.ycen

index_mod = add_tag(index,1,'mod_xcenycen')         ; Generate the new structure

if (keyword_set(clobber) or (n_params() lt 2)) then begin 
  if (n_params() eq 1) then box_message, [parent_proc, 'clobbering input index with new results.', 'No oindex param provided.'] 
  index = index_mod

endif

end
