pro sxi_fitsv1to1_5, files, iindex, iidata, oindex, odata, file_write=file_write, dev_ver_num=dev_ver_num, base_dir=base_dir

;note: start with  sxi14 data only (since inpainting isn't working it)
;They are going to have crota corrections, and call ssw_register.
;
;This will be a pipeline piece of software -- so, I think it should 
;1.) read fits files (the level 1 fits files that we made earlier)
;2.) Make a new directory, change the file name in the fits header to reflect the version
;    (note-- I should check and see if there is a "version number" tag.
;3.) Perhaps we can also have a flag for a movie -- which would make a movie output.
;
;
;As I understand it, Sam asked me to remove this call and replace it will a call to read_sxi, and another to ssw_register.
;mreadfits_sxi13, files, oindex_l1_5, odata_l1_5, /register, roll=0 ;replace this with a call to read_sxi, 
                                                                    ;but might need to add code to add crota
;stop,'in sxi_fitsv1to1_5, check files'
read_sxi, files, temp_oindex_l1_5, temp_odata_l1_5
;stop, 'in sxi_fitsv1to1_5: check for valid input to ssw_register.'
ssw_register, temp_oindex_l1_5, temp_odata_l1_5, oindex_l1_5, odata_l1_5, roll=0

parent_proc = 'sxi_fitsv1to1_5'
file_suffix = '_'
;
;ADD VERSION LETTER CODE CODE TO FILE SUFFIX
file_suffix = strjoin([file_suffix, 'CA'],/SINGLE) ;equivalent of version 1.5 in their terminology

if not keyword_set(dev_ver_num) then dev_ver_num = '1.0'
if not keyword_set(base_dir) then base_dir = strjoin([getenv('SXIPL'),'/L1.5b/'], /single)
base_dir=strjoin([base_dir, strmid(strtrim(string(dev_ver_num),2),0,3),'/'], /single);currently, a maximum of 3 digits allowed.
file_suffix=strjoin([file_suffix, '_', strmid(strtrim(string(dev_ver_num),2),0,3)], /single);currently, a maximum of 3 digits allowed.

telescope = !NULL ;initialization
;ADD INSTRUMENT NUMBER (13, 14 or 15) TO *.FTS FILE NAME SUFFIX
if (strcmp(oindex_l1_5[0].telescop, 'GOES-13', 7, /FOLD_CASE)) then begin
  file_suffix = strjoin([file_suffix, '_13'],/SINGLE)
  telescope = 'sxi13/'
endif
if(strcmp(oindex_l1_5[0].telescop, 'GOES-14', 7, /FOLD_CASE)) then begin
  file_suffix = strjoin([file_suffix, '_14'],/SINGLE)
  telescope = 'sxi14/'
endif
if (strcmp(oindex_l1_5[0].telescop, 'GOES-15', 7, /FOLD_CASE)) then begin
  file_suffix = strjoin([file_suffix, '_15'],/SINGLE)
  telescope = 'sxi15/'
endif
;
;ADD Instrument Level directory to path
if telescope ne !NULL then begin
  base_dir=strjoin([base_dir, telescope],/single)
endif




;modify the .FNAME tag on a case-by-case basis below -- would it work to update the FNAME in a vector call?
;stop, 'in sxi_fitsv1to1_5.pro: check base_dir and file_suffix'
if keyword_set(file_write) then begin
;
  for i=0, (size(oindex_l1_5.fname,/n_elements)-1) do begin
    ;CREATE PARENT DIRECTORY for each distinct .FTS file, if needed, i.e., if it traverses a month.
    fits_file_output_dir= ssw_time2paths(oindex_l1_5[i].date_obs, parent=base_dir, /MONTHLY) ;is this vectorized?
    fits_file_name = strjoin([strmid(oindex_l1_5[i].fname, 0, strlen(oindex_l1_5[i].fname)-9), file_suffix],/single)
    oindex_l1_5[i].fname = fits_file_name

    iindex = oindex_l1_5 ;clobbering any input index -- do we want that?
    idata = odata_l1_5   ;clobbering any input index -- do we want that?

    fits_full_file_name  = strjoin([fits_file_output_dir, '/', oindex_l1_5[i].fname, '.FTS'],/single)

    ;WRITE FITS FILES TO APPRORIATE DIRECTORY
    ;
    if (file_test(fits_full_file_name) and (not keyword_set(file_overwrite))) then begin;     file already exists with this name and in this location and overwrite not called out
      box_message,  [parent_proc, '* Will overwrite existing FITS file: ', fits_full_file_name, ' * 1 = Abort, 2 = OK For This File Only, 3 = Turn Off Overwrite Check For All Files']
      read, ans
      if (ans ne 2) and (ans ne 3) then begin
        box_message, [parent_proc, 'Overwrite of existing Fits Files with sxi_prep result aborted.', 'Ending Proc']
        return
      endif
      if ans eq 3 then  file_overwrite = 3
    endif
    ;
    if not file_test(fits_file_output_dir,/DIRECTORY) then  mk_dir, fits_file_output_dir ;this will fail if it already exists
    print, 'fits_full_file_name =', fits_full_file_name
    
    index_fits_write = oindex_l1_5[i]
    data_fits_write = reform(odata_l1_5[*,*,i])
;stop, 'in sxi_fitsv1to1_5: check fits_full_file_name, write occurs after this step.'
    mwritefits, index_fits_write, data_fits_write, outdir=fits_file_output_dir, outfile=fits_full_file_name,/nocomment
  endfor
    ;
endif



end
