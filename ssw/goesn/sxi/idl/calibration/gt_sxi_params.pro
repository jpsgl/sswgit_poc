;+
;NAME:		gt_sxi_params
;PURPOSE:	Return various information from GOES N/O/P SXI FITs header.
;
;CALLING SEQUENCE:
;	text_info = gt_sxi_params(fits_header)		; Text string
;	text_info = gt_sxi_params(fits_header,/info)	; Text string
;
; Most parameters are returned one at a time.  Adding the /info switch;
; will convert the returned parameter to a text string.
; 
;	time_date = gt_sxi_params(fits_header,/atime)	; Date and time
;	FM	  = gt_sxi_params(fits_header,/FM)	; 1, 2, or 3
;	OP_Amp	  = gt_sxi_params(fits_header,/OP_Amp)	; A or B Amp
;	AmpGain	  = gt_sxi_params(fits_header,/AmpGain)	; Lo or Hi gain
;	T_CCD	  = gt_sxi_params(fits_header,/T_CCD)	; CCD Temp (T_CCDS1)
;	shutmode  = gt_sxi_params(fits_header,/shutmode); normal, dark, ltc
;	Exposure  = gt_sxi_params(fits_header,/exposure); Cmd'ed exposure (sec)
;	Exptime	  = gt_sxi_params(fits_header,/exptime) ; Actual exposure time
;	Filter	  = gt_sxi_params(fits_header,/filter)	; In range 0 to 29
;	f1pos	  = gt_sxi_params(fits_header,/f1pos)	; Filter Wheel 1
;	f2pos	  = gt_sxi_params(fits_header,/f2pos)	; Filter Wheel 2
;	binning	  = gt_sxi_params(fits_header,/binning)	; Binning, e.g. 1x1
;	filename  = gt_sxi_params(fits_header,/fname)	; Filename
;	VerLevL0  = gt_sxi_params(fits_header,/verlevl0); Fits header version
;
;
; Input parameters:
;
;  Fits_header - The Fits file structure and if it is a
;		vector, then results will be returned as vector.
;
;		It is assumed that the FITS header was generated from
;		the LMSAL ground system or from the Level 0 SEC processing
;
;
; Optional input keywords 
;	info	If the only keyword specified, return a pre-formatted
;		text string.  If specified with one of the other key
;		words, then change the specified result into a string.
;
;	/time,/date,/y2k,/hxrbs = optional keywords passed to atime()
;	used to control the formatting of the time (see atime documentation).
;	These keywords have no effect if /atime is not also supplied.
;
; For the following keywords, only one must be set when called
;	ATIME		Set to call atime() to return date and/or time
;	FM		Set to return the SXI instrument number.
;	OP_Amp		Set to return the output amplifier
;	Ampgain		Set to return the camera gain	
;	T_CCD		Set to return the CCD temperature in C (uses T_CCDS1)
;	Shutmode	Set to return the shutter mode	
;	Exposure	Set to return the commanded exposure time (sec)
;	Exptime		Set to return the actual shutter open time (sec)
;	Filter		Set to return the composite filter number
;	f1pos		Set to return the Front Filterwheel position
;	f2pos		Set to return the Back  Filterwheel position
;	binning		Set to return the binning
;	fname		Set to return file name.  Tag was "filename"
;				pre-6-May-2010 and changed to "fname" after.
;	VerLevL0	If this keyword is missing (e.g. cat), will check fname.
;			If fname contains _AA_ then  0.1 is returned.
;			If fname contains _AB_ then 
;
;	Note: For darks, Exptime is set to Exposure 
;
; The OP_Amp, Ampgain, Shutmode, f1pos, f2pos, and binning keywords will
; return numbers or if /info is specified, the corresponding names:
;
;			/INFO			INFO=0
;			-----------------	----------------------------
;	OP_Amp		A or B 			0 or 1
;	Ampgain		Lo or Hi		0 or 1
;	Shutmode	Normal, Dark, LTC	0 or 1 or 2
;	f1pos		1 to 6			Front filterwheel filter name
;	f2pos		1 to 6			Back  filterwheel filter name
;	binning		1x1 or 2x2 or 4x4	1 or 2 or 4 (0=Invalid)
;
; Optional output keyword:
;	prog_ver Version of the program
;
; Return:
;	The requested information from the FITS header.
;
; Error handling:
;	If the Filter position shows "?", gt_sxi_params will trap the
;       invalid entry and return 0 (or "Invalid" if /info is set)
;
;HISTORY:
; 11-Jul-2006, V 0.90, J. R. Lemen, Written
; 14-Nov-2008, V 0.91, J. R. Lemen, Trap more non-numeric cases of f1pos, f2pos
; 25-Jul-2009, V 0.92, J. R. Lemen, Fixed Binning 
; 21-Dec-2009, V 0.93, J. R. Lemen, Fixed AmpGain for large numbers
; 21-May-2010, V 0.94, J. R. Lemen, SWPC updated L0 Fits headers and
; 				    changed file name to _AB_ on 6 May 2010
;			Fixed: ampagain/ampbgain to amp_gain
;			Fixed: change of exposure defintion (sec to msec)
;			Added: fname, verlevl0, exptime (actual	open/close time)
;  1-Jun-2010, V 0.95, J. R. Lemen, April 2010 G13 data had invalid values for
;				    f1pos and f2pos.  Try to decode the correct
;				    values from f1filter and f2filter
;-

function	gt_sxi_filter, fits_header, FW_num
;
;NAME:		gt_sxi_filter
;
;PURPOSE:	Called by gt_sxi_params to return the SXI filter position
;
;CALLING SEQUENCE:
;	f1pos = gt_sxi_filter(fits_header, 1)		; Filter wheel 1
;	f2pos = gt_sxi_filter(fits_header, 2)		; Filter wheel 2
;
;INPUTS:
;	Fits_header	- Fits file structure
;	FW_num		- 1 or 2 for the filter wheel.  If FW_num is a
;                         vector, it must be the same length as Fits_header
;
;Note:
; In April 2010 the G13 f1pos and f2pos values were invalid.  This
; routine tries to "recover" the f1pos and f2pos values by looking at
; the f1filter and f2filter tag values.
;
;HISTORY:
;  1-Jun-2010, J. R. Lemen, Written.  

; Check that length of FW_num is compatible with fits_header

FW = FW_num
if n_elements(FW) eq 1 then FW = replicate(FW, n_elements(Fits_header))

if n_elements(Fits_header) ne n_elements(FW) then $
	message,string('Length of Fits_header (',		$
		strtrim(n_elements(Fits_header),2),		$
		') does not match length of FW (', 		$
        	strtrim(n_elements(FW),2),')')
if min(FW) lt 1 or max(FW) gt 2 then message,'FW must be 1 or 2'

fxpos = strarr(2,n_elements(Fits_header))
fypos = strarr(n_elements(Fits_header))
fxpos[0,*] = Fits_header.f1pos
fxpos[1,*] = Fits_header.f2pos

for i=0L,n_elements(Fits_header)-1 do fypos[i] = fxpos[FW[i]-1,i]
fzpos = intarr(n_elements(Fits_header))

ij = where(strlen(fypos) eq 1 and (fypos eq '1' or fypos eq '2' or 	$
	fypos eq '3' or fypos eq '4' or fypos eq '5' or fypos eq '6'))
fzpos[ij] = fix(fypos[ij])

; If f1pos or f2pos is missing, try to decode from f1filter or f2filter

ij = where(fzpos eq 0 and strlen(fypos) gt 1, nc)

if nc gt 0 then begin
  fxfilter = strarr(2,n_elements(Fits_header))
  fyfilter = strarr(n_elements(Fits_header))
  fxfilter[0,*] = Fits_header.f1filter
  fxfilter[1,*] = Fits_header.f2filter

  for i=0L,n_elements(Fits_header)-1 do fyfilter[i] = fxfilter[FW[i]-1,i]

  FW1 = [['Invalid','Open','12uBe','Tinm','Glass','Thinpoly1','Thickpoly'],$
       ['Invalid','Open','12uBe','TinM','Glass','Thinpoly1','Thickpoly'],$
       ['Invalid','Open','12uBe','TinM','Glass','Thinpoly1','Thickpoly']]
  FW2 = [['Invalid','50uBe','12uAl','Thinpoly2','Open','Silver','12uBe2'], $
       ['Invalid','50uBe','12uAl','Thinpoly2','Open','Silver','12uBe2'],$
       ['Invalid','50uBe','12uAl','Thinpoly2','Open','Silver','12uBe2']]

  FMn = gt_sxi_params(Fits_header, /FM)	; Get the FM #

  for i=0,nc-1 do begin
    case FW[ij[i]] of
    1: ss = strpos(strupcase(fw1[*,FMn[ij[i]]]),fyfilter[ij[i]])
    2: ss = strpos(strupcase(fw2[*,FMn[ij[i]]]),fyfilter[ij[i]])
    endcase
    kk = where(ss ne -1, nk)

if nk eq 0 then begin
  print,'i,fyfilter:',i,' ',fyfilter[ij[i]]
  stop
endif

    if nk gt 0 then fzpos[ij[i]] = kk[0]
  endfor
endif



return, fzpos

end

function	gt_sxi_params, fits_header, atime=atimeq,	$
		time=time, date=date, y2k=y2k, hxrbs=hxrbs,	$ ; for atime
		FM=FM,OP_Amp=OP_Amp,ampgain=ampgain,T_CCD=T_CCD,$
                shutmode=shutmode,exposure=exposure,		$
                exptime=exptime,fname=fname,verlevl0=verlevl0,	$
		filter=filter,f1pos=f1pos,f2pos=f2pos,		$
		binning=binning,info=info,prog_ver=prog_ver
                            

prog_ver = 0.90		; 22-Jul-2006 (Written)
prog_ver = 0.91		; 14-Nov-2008 
prog_ver = 0.92		; 25-Jul-2009
prog_ver = 0.93		; 21-Dec-2009
prog_ver = 0.94		; 21-May-2010
prog_ver = 0.95		;  1-Jun-2010




;***********************************************
; Make sure that only one parameter is specified
;***********************************************

qq=keyword_set(atimeq)  +keyword_set(FM)      +		$
   keyword_set(OP_Amp)  +keyword_set(ampgain) +		$
   keyword_set(T_CCD)	+keyword_set(shutmode)+		$
   keyword_set(exposure)+keyword_set(filter)  +		$
   keyword_set(f1pos)	+keyword_set(f2pos)   +		$
   keyword_set(binning) +keyword_set(exptime) +		$
   keyword_set(fname)   +keyword_set(verlevl0)


;***********************************************
; Define the filter wheel positions
;***********************************************

FW1 = [['Invalid','Open','Be12','Tin','Glass','Thin_poly','Thick_poly'],$
       ['Invalid','Open','Be12','TinMesh','Glass','Thin_poly','Thick_poly'],$
       ['Invalid','Open','Be12','TinMesh','Glass','Thin_poly','Thick_poly']]
FW2 = [['Invalid','Be50','Al12','Thin_poly','Open','Tin','Be12'],	$
       ['Invalid','Be50','Al12','Thin_poly','Open','Silver','Be12'],	$
       ['Invalid','Be50','Al12','Thin_poly','Open','Silver','Be12']]

;***********************************************
; Execute one of the three possible cases
;***********************************************

case qq of 
0: begin

; Return a text string with information

; Format the shutter mode and filter so all shutter modes have matching
; string lengths and similarly for filter descriptions.

    shutmode_form = string(gt_sxi_params(Fits_header,/Shut,/info) + 	$
		'   ',form='(a6)')
    filter_form = string(gt_sxi_params(Fits_header,/filter)+		$
		'             ',form='(a20)')

    param = gt_sxi_params(Fits_header,/atime) 		+ ' ' +	$
	    filter_form					+ ' ' +	$
	    shutmode_form 					+ ' ' + $
            gt_sxi_params(Fits_header,/exposure,/info)	+ ' ' +	$
	    gt_sxi_params(Fits_header,/OP_Amp,/info)	+ ' ' +	$
	    gt_sxi_params(Fits_header,/Ampgain,/info)	+ ' ' +	$
	    gt_sxi_params(Fits_header,/Binning,/info)

   end

1: begin

; Return the time and date:	/ATIME
     if keyword_set(ATIMEq) then begin
	get_fits_time, Fits_header, Fits_time
	param = atime(Fits_time,time=time,date=date,y2k=y2k,hxrbs=hxrbs)
     endif

; Return the Telescope Number:	/FM

   if keyword_set(FM)    or keyword_set(FILTER) or $
      keyword_set(F1POS) or keyword_set(F2Pos) then begin
      telescop = strupcase(fits_header.telescop)
     FM_num = strpos(telescop,'FM1') and strpos(telescop,'GOES-13')
     ij = where(FM_num ne -1, nc) & if nc gt 0 then FM_num[ij] = 1
     FM2 = strpos(telescop,'FM2') and strpos(telescop,'GOES-14')
     ij = where(FM2 ne -1, nc) & if nc gt 0 then FM_num[ij] = 2
     FM3 = strpos(telescop,'FM3') and strpos(telescop,'GOES-15')
     ij = where(FM3 ne -1, nc) & if nc gt 0 then FM_num[ij] = 3

     if keyword_set(FM) then $
     if keyword_set(info) then param = string(FM_num,form='(i1)') $
			  else param = FM_num
   endif


; Return the Amplifier:		/OP_AMP	[A = 0;  B = 1]
   if keyword_set(OP_Amp) then begin
     OP_Amp = Fits_header.op_amp
     if keyword_set(INFO) then param = OP_Amp else begin
	param = replicate(-9,n_elements(OP_Amp))	; Initialize with -9
	ij = where(strupcase(OP_Amp) eq 'A', nc)
	if nc gt 0 then param[ij] = 0
	ij = where(strupcase(OP_Amp) eq 'B', nc)
	if nc gt 0 then param[ij] = 1
     endelse
   endif

; Return the camera gain:	/AMPGAIN  [Lo = 0;  Hi = 1]
   if keyword_set(Ampgain) then begin
     if have_tag(Fits_header,'amp_gain') then $		; Added V0.94
			param = fits_header.amp_gain else begin $ 
       qAmp1 = gt_sxi_params(Fits_header,/OP_amp)
       n_img = n_elements(Fits_header)

       param = $
        ([[fits_header.ampagain],[fits_header.ampbgain]])[lindgen(n_img),qAmp1]
     endelse

     if not keyword_set(INFO) then begin
       xx_param = replicate(-1,n_elements(param)) 
       ij = where(strupcase(param) eq 'LO',nc)
       if nc gt 0 then xx_param[ij] = 0
       ij = where(strupcase(param) eq 'HI',nc)
       if nc gt 0 then xx_param[ij] = 1
       param = xx_param
     endif
   endif

; Return the CCD Temperature:	/T_CCD  (use parameter T_CCDS1)
   if keyword_set(T_CCD) then begin
     T_CCD = gt_tagval(Fits_header,/CCD_TMP,missing=$	; SEC format
	     gt_tagval(Fits_header,/T_CCDS1,missing=0.)); LMSAL format
     if keyword_set(info) then param = string(T_CCD,form='(f7.2)') $
			  else param = T_CCD
   endif

; Return the Shutter mode:	/SHUTMODE  [Norm = 0, Dark = 1, LTC = 2]
   if keyword_set(shutmode) then begin
     shutmode = Fits_header.shutmode 
     if keyword_set(INFO) then param = shutmode else begin
        param = intarr(n_elements(shutmode))
        ij = where(strupcase(shutmode) eq 'DARK', nc) 
        if nc gt 0 then param[ij] = 1
        ij = where(strupcase(shutmode) eq 'LTC', nc) 
        if nc gt 0 then param[ij] = 2
     endelse
   endif


; Return the Exposure time:	/EXPOSURE
   if keyword_set(exposure) then begin
     Exposure = Fits_header.exposure
     
; - - - - - - - L0 Header Format Change (6 May 2010) - - - - - - - - - - -
; Examine VERLEVL0.  					; Added V0.94
; If VERLEVL0 is >= 1.0 then divide Exposure by 1000 to convert to sec
     Ver = float(gt_sxi_params(Fits_header,/verlevl0))
     ii = where(Ver ge 1.0, nc)
     if nc gt 0 then begin
        Exposure = float(Exposure)
        Exposure[ii] = Exposure[ii] / 1000.
     endif
; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

     if keyword_set(info) then param = string(exposure,form='(f7.3)') $
			  else param = Exposure
   endif

; Return the Exptime time:	/EXPTIME
   if keyword_set(exptime) then begin
     Exptime = Fits_header.exptime

; For darks, convert EXPTIME (which will be 0 sec) to EXPOSURE (commanded)
     Shutmode = gt_sxi_params(Fits_header,/Shutmode)
     ii = where(Shutmode eq 1, nc) 	; 1=dark
     if nc gt 0 then Exptime[ii] = gt_sxi_params(Fits_header[ii],/exposure)

     if keyword_set(info) then param = string(exptime,form='(f7.3)') $
			  else param = Exptime
   endif

; Return the file name:		/FNAME
   if keyword_set(fname) then begin
     if have_tag(Fits_header,'filename') then $
	Filname = Fits_header.filename else Filname = Fits_header.Fname
     param = Filname
   endif


; Return FITS header version: 	/VERLEVL0
   if keyword_set(verlevl0) then begin
      if have_tag(Fits_header,'verlevl0') then $
         VerLevL0 = Fits_header.verlevl0 else begin	
     ; CAT (catalog) doesn't have verlevl0 tag
         Filnam = gt_sxi_params(Fits_header,/fname)	; Infer from filename
         verlevl0 = replicate(0.,n_elements(Fits_header))
         ij = where(strpos(Filnam,'_AA_') ne -1,nc)
         if nc gt 0 then verlevl0[ij] = 0.1
         ij = where(strpos(Filnam,'_AB_') ne -1,nc)
         if nc gt 0 then verlevl0[ij] = 1.0		; Post 6-May-2010
         verlevl0 = string(verlevl0,form='(f3.1)')
      endelse
      param = VerLevL0
   endif

; Return the FW1 Filter:	/F1POS
   if keyword_set(F1POS) or keyword_set(FILTER) then begin
;;     FW1_Filter = Fits_header.f1pos
;;     ij = where(FW1_Filter eq '?' or FW1_Filter eq '_' or $
;;       strupcase(FW1_filter) eq 'R', nc) & if nc gt 0 then FW1_Filter[ij] = '0'
;;     FW1_Filter = fix(FW1_Filter)
     FW1_Filter = gt_sxi_filter(Fits_header,1)
     if keyword_set(F1POS) then $
     if keyword_set(INFO) then param = FW1[FW1_Filter,FM_num-1] else $
       			       param = FW1_Filter
   endif


; Return the FW2 Filter:	/F2POS
   if keyword_set(F2POS) or keyword_set(FILTER) then begin
;;     FW2_Filter = Fits_header.f2pos
;;     ij = where(FW2_Filter eq '?' or FW2_Filter eq '_' or $
;;       strupcase(FW2_filter) eq 'R', nc) & if nc gt 0 then FW2_Filter[ij] = '0'
;;     FW2_Filter = fix(FW2_Filter)
     FW2_Filter = gt_sxi_filter(Fits_header,2)
     if keyword_set(F2POS) then $
     if keyword_set(INFO) then param = FW2[FW2_Filter,FM_num-1] else $
			       param = FW2_Filter
   endif

; Return the Filter:		/FILTER
   if keyword_set(FILTER) then begin
       param = FW1[FW1_Filter,FM_num-1] + '/' + $
               FW2[FW2_Filter,FM_num-1] 
   endif


; Return the Binning:		/BINNING [0 = 1x1, 1 = 2x2, 2 = 3x3]
   if keyword_set(BINNING) then begin
      Binning = strupcase(long(Fits_header.binning))
	param = replicate(-9,n_elements(Binning))	; Initialize with -9
	Bin1 = strpos(Fits_header.Binning,'1X1') and strpos(Binning,'1')
	ij = where(Bin1 ne -1, nc) & if nc gt 0 then param[ij] = 1
	Bin2 = strpos(Fits_header.Binning,'2X2') and strpos(Binning,'2')
	ij = where(Bin2 ne -1, nc) & if nc gt 0 then param[ij] = 2
	Bin4 = strpos(Fits_header.Binning,'4X4') and strpos(Binning,'3')
	ij = where(Bin4 ne -1, nc) & if nc gt 0 then param[ij] = 4
        if keyword_set(info) then begin
            param1 = param & param = replicate('Invalid',n_elements(Binning))
	    ij = where(param1 ne -9, nc)
	    if nc gt 0 then param[ij] = $
			(['??','1x1','2x2','3x3','4x4'])[param1[ij]]
        endif
   endif

   end
else: begin

  message,'Can not specify multiple input parameters'
end
endcase


return,param
end




