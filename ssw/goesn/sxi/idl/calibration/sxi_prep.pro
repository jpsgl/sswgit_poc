pro sxi_prep, index, data, oindex, odata,      $
              debug          = debug,          $
              base_dir       = base_dir,       $
              nocrpix        = nocrpix,        $
              newmissing     = newmissing,     $
              qfloat         = qfloat,         $
              nolut          = nolut,          $
              nobias         = nobias,         $
              usedark        = usedark,        $
              novignette     = novignette,     $
              notpitchtyaw   = notpitchtyaw,   $
              noxcenycen     = noxcenycen,     $
              file_write     = file_write,     $
              file_overwrite = file_overwrite, $
              dev_ver_num    = dev_ver_num,    $
              verbose        = verbose,        $
              e_extra        = e_extra
;
;+ 
;NAME:    SXI_PREP
;
;PURPOSE: Prep the SXI RAW data, update the FITS header catalog values, 
;         optionally: 
;         1.) save a new file to  a user defined L1 directory,
;
;METHOD:  PREP DATA
;         1. Call sxi_bls_sub to subtract camera offset & 
;            read noise, or optionally subtract darks.     [Bias correction]
;	  2. Call sxi_bad_pixel to correct for bad pixels. [Identify missing/non-responsive/saturated pixels]
;         3. Call sxi_lut_correction to LUT correct data.  [Gain/non-linearity correction]
;         4. Call inpaint to perform inpainting            [Inpainting of bad/missing/saturated pixels]
;	  5. Call sxi_vignette_sub to correct for optical
;            vignetting.                                   [Vignetting]
;         ;
;         CORRECT FITS HEADER VALUES
;         5. Correct TPITCH & TYAW FITS values
;         6. Correct XCEN, YCEN, CRPIX1 & CRPIX2, 
;            which are based on TPITCH and TYAW
;         SIDE EFFECTS/OUTPUT
;         7.) Creates and write FITS file to output directory (if /file_write keyword is set).
;         8.) Return data cube of modified index structure 
;             and data arrays (if oindex, odata parameters in call -- see ex 3., below) 
;CALLING SEQUENCE: 
;         ex 1.) SXI_PREP, INDEX, DATA
;         ex 2.) SXI_PREP, INDEX, DATA, OINDEX, ODATA
;         ex 3.) SXI_PREP, INDEX, DATA, NEWMISSING=NEWMISSING
;
;INPUTS: 
;         INDEX & DATA    SXI-13 Fits Header and Data cube
;
;OUTPUTS: 
;         INDEX  & DATA  if OINDEX & ODATA not supplied (inputs are clobbered)
;         OINDEX & ODATA  Updated Fits Header and Data cubes
;       
;
;OPTIONAL INPUT KEYWORD:
;
;         DEBUG          - Print version number.
;         BASE_DIR       - Set base directory for
;         NOCRPIX        - Set to inhibit the call to sxi_crpix (which fixes
;                          approximately the values or CRPIX1 and CRPIX2)
;         NEWMISSING     - Default is -999 (This is also an output variable)
;	  QFLOAT	 - Default is FLOAT=1.  Set FLOAT=0 to not float data.
;         NOLUT          - If set, don't call sxi_lut_correction
;         NOBIAS         - If set, dont' call sxi_bias_sub
;	  USEDARK        - If set, read a Dark image to subtract the bias.
;                          The default is to interpolate the dark database.
;         NOVIGNETTE     - If set, do not call the vignette correction proc(s).
;         NOTPITCHYAW    - If set, do not call the pitch and yaw correction proc(s).
;         NOXCENYCEN     - If set, do not call the xcen, ycen correction (proc(s).
;         FILE_WRITE     - If set, write output .FTS files.
;         FILE_OVERWRITE - If file_overwrite = 0
;         DEV_VER_NUM    - If set, (up to) 3 characters of number or string will be added to output .FTS file name.
;                          Also added to .FNAME tag in FITS file.
;         VERBOSE        - 
;         E_EXTRA        -
;
;PROCEDURE:
;
;
;REQUIRED ROUTINES: 
;         sxi_lut_correction, 
;         rd_sxi_darkdb, 
;         sxi_darkdb, 
;         sxi_bls_sub,
;         sxi_bias_sub, 
;         sxi_bad_pixel
;
;HISTORY:
;         24 Dec 2009, J. R. Lemen, Written V0.9
;          4 Jan 2011, J. R. Lemen, Added USEDARK keyword
;         13 Apr 2011, J. R. Lemen, Added call to sxi_crpix
;         23 Aug 2011, J. R. Lemen, Added nolut and nobls keywords
;          9 Jan 2013, J. R. Lemen, Corrected spelling of nocrpix in definition
;         24 Jun 2016, P. G. Shirts, added crpix1/2, x/ycen, fileoutput test, vignetting calls..
;         12 Aug 2016, P. G. Shirts, better name length, frame=16 addition. 
;-
Version = 'V0.92'
if keyword_set(debug) then print,'********  SXI_PREP, '+Version+' *********'

parent_proc = 'sxi_prep.pro: '

;TEST THIS FEATURE.  --I think I should have 'L_a' something that can be passed
if not keyword_set(base_dir) then begin 
  if 1 eq strcmp(getenv('SXIPL'),'',/fold_case) then begin
    base_dir = './L1_b/'
  endif else begin
    base_dir = strjoin([getenv('SXIPL'),'/L1_b/'], /single)
  endelse
endif

;ADD LEVEL 1 VERSION CODE TO *.FTS FILE NAME SUFFIX
if keyword_set(file_write) then begin;
  file_suffix = '_'
  ;
  ;ADD VERSION LETTER CODE CODE TO FILE SUFFIX
  file_suffix = strjoin([file_suffix, 'BC'],/SINGLE)
  ;currently, this is in test.
  if keyword_set(dev_ver_num) then begin
    base_dir=strjoin([base_dir, strmid(strtrim(string(dev_ver_num),2),0,3),'/'], /single);currently, a maximum of 3 digits allowed.
    file_suffix=strjoin([file_suffix, '_', strmid(strtrim(string(dev_ver_num),2),0,3)], /single);currently, a maximum of 3 digits allowed.
  endif
  ;
  telescope = !NULL ;initialization
  ;ADD INSTRUMENT NUMBER (13, 14 or 15) TO *.FTS FILE NAME SUFFIX
  if (strcmp(index[0].telescop, 'GOES-13', 7, /FOLD_CASE)) then begin 
    file_suffix = strjoin([file_suffix, '_13'],/SINGLE)
    telescope = 'sxi13/'
  endif 
  if(strcmp(index[0].telescop, 'GOES-14', 7, /FOLD_CASE)) then begin 
    file_suffix = strjoin([file_suffix, '_14'],/SINGLE)
    telescope = 'sxi14/'
  endif 
  if (strcmp(index[0].telescop, 'GOES-15', 7, /FOLD_CASE)) then begin 
    file_suffix = strjoin([file_suffix, '_15'],/SINGLE)
    telescope = 'sxi15/'
  endif
  ;
  ;ADD Instrument Level directory to path
  if telescope ne !NULL then begin 
    base_dir=strjoin([base_dir, telescope],/single)
  endif
  ;
  ;
endif

if keyword_set(qFLOAT) then qq = 1. else qq = 1

;TODO REMOVE COMMENTARY is this the dark subtraction call?  And is the purpose of the new function
;to upgrade it to use darks instead of just subtracting the mean of the bls
;pixel values.  --also, how often are the bls pixels available?

; ------ PREP DATA ------
if keyword_set(verbose) then box_message,[parent_proc,'Beginning sxi_bls_sub call']
;1.) BLS camera offset subtraction -- either mean BLS pixel value, or interpolated dark image (use_dark  MAKE SURE TAG IS ADDED)
sxi_bls_sub, index, qq*data, oindex, odata, NewMissing=NewMissing, debug=debug

if keyword_set(verbose) then box_message,[parent_proc, 'Beginning sxi_bad_pixel call.']
;2.) BAD PIXEL SUBTRACTION

sxi_bad_pixel, oindex, odata, /FIX, debug=debug

;3.) SXI LUT
if not keyword_set(nolut) then begin
  if keyword_set(verbose) then box_message, [parent_proc, 'Beginning sxi_lut_correction call']
  sxi_lut_correction, oindex, odata, Missing=NewMissing, NewMissing=NewMissing, debug=debug
endif else begin
  if keyword_set (verbose) then box_message, [parent_proc, 'keyword nolut set', 'skipping sxi_lut_correction call']
endelse
;
;----------------------------------------------------------------------
;START -- pulling inpainting code from sxi_bad_pixel.pro code.  -- pgs
;note: this should probably be pulled into it own proc, and then referenced here.
;note: we probably need at least a line defining Bad_flag above this. TODO pgs

Bad_flag = gt_tagval(oindex,/bad_pix)
iBad = where(Bad_flag eq 0, nfiles)     ; These files need bad pixel treatment

if n_elements(badvalue) eq 0 then badvalue = 1
 
for i=0,nfiles-1 do begin
  bad_map = get_sxi_badpixel_db(oindex[iBad[i]],epoch=epoch,badvalue=badvalue)
  ;print,iBad[i],oindex[iBad[i]].naxis1,oindex[iBad[i]].naxis2 ;is there any need for this?  PGS
  ;help, bad_map                                               ;is there any need for this?  PGS 
 

  ;odata[*,*,iBad[i]] = inpaint(data[*,*,iBad[i]],bad_map,n=1000) ;original
  odata[*,*,iBad[i]] = inpaint(data[*,*,iBad[i]],bad_map,frame=16, order=1,quiet=1)
  ;help, odata ;Commented out -- why do we need "help, odata"
  oindex[iBad[i]].badepoch = epoch
  oindex[iBad[i]].bad_pix = Bad_version
endfor
; ------------------------------------------------------------------
;END -- pulling inpainting code from sxi_bad_pixel.pro code.  -- pgs
;
;4.) SXI VIGNETTING CORRECTION 
if not keyword_set(novignette) then begin
  if keyword_set (verbose) then box_message,[parent_proc, 'Before sxi_vignette_sub call']
  sxi_vignette_sub, oindex, odata
endif else begin
  if keyword_set(verbose) then box_message, [parent_proc, 'keyword novignette set: skipping sxi_vignette_sub call']
endelse

; ------ PREP FITS HEADER ------
;5.) CORRECT TPITCH & YAW 
; TODO need to add this function, perhaps make a stub for it?  if not keyword_set(notpitchtyaw) then sxi_tpitchtyaw_update; <-- add parameters.


;6.) UPDATE CRPIX1 & CRPIX2
if not keyword_set(nocrpix) then sxi_crpix, oindex, index_mod, /clobber


;6.) UPDATE XCEN, YCEN
if ((not keyword_set(noxcenycen)) and (not keyword_set(nocrpix))) then begin ;no need to fix xcen, ycen if crpix1 and crpix1 not adusted -- correct?
  sxi_xcenycen, oindex, /clobber
endif

;
; Clobber input variables if oindex & odata are NOT supplied:
if n_params() lt 4 then begin   
  index = oindex
  data  = odata
endif

;------ PREPARE LEVEL 1 INDEX/FITS HEADER STRUCTURE (BUILD FROM LEVEL 0 INDEX) PRIOR TO A LEVEL 1 FITS FILE WRITE ------

fitslevel0to1_sxi, oindex, oindex_l1
;                                      
;------ WRITE TO FILE AS AN UPDATED .FITS FILE ------
;7.) OUTPUT
;
;CREATE PARENT DIRECTORY AND ARRAY OF FILE NAMES 

if keyword_set(file_write) then begin 
;
  for i=0, (size(oindex_l1.fname,/n_elements)-1) do begin
    ;CREATE PARENT DIRECTORY for each distinct .FTS file, if needed, i.e., if it traverses a month.
    fits_file_output_dir= ssw_time2paths(index[i].date_obs, parent=base_dir, /MONTHLY) ;is this vectorized?
    if (tag_exist(index[i], 'FNAME', /top_level, /quiet)) then begin
      fits_file_name = strjoin([strmid(oindex_l1[i].fname, 0, 22), file_suffix],/single) ;was -6
      oindex_l1[i].fname = fits_file_name
    endif else begin ;with recent change to fitslevel0to1_sxi.pro (replacing filename tag, if it exists, with fname tage might might this branch unnecessary.
      if (tag_exist(index[i], 'FILENAME', /top_level, /quiet)) then begin ;for some 'AA' SXI13 fits files.
        fits_file_name = strjoin([strmid(index[i].filename, 0, strlen(index[i].filename)-(strlen(index[i].filename)-22)), file_suffix],/single) ;was -6
        oindex_l1[i].fname = fits_file_name
      endif
    endelse
    fits_full_file_name  = strjoin([fits_file_output_dir, '/', oindex_l1[i].fname, '.FTS'],/single)
    ;is this the problem? oindex_l1[i].fname = strjoin([strmid(oindex_l1[i].fname, 0, strlen(oindex_l1[i].fname)-6), file_suffix],/single)
    ;fits_file_name = strjoin([strmid(oindex_l1[i].fname, 0, strlen(oindex_l1[i].fname)-6), file_suffix, '.FTS'],/single)

    ;WRITE FITS FILES TO APPRORIATE DIRECTORY
    ;
    if (file_test(fits_full_file_name) and (not keyword_set(file_overwrite))) then begin;     file already exists with this name and in this location and overwrite not called out
      box_message,  [parent_proc, '* Will overwrite existing FITS file: ', fits_full_file_name, ' * 1 = Abort, 2 = OK For This File Only, 3 = Turn Off Overwrite Check For All Files']
      read, ans 
      if (ans ne 2) and (ans ne 3) then begin
        if keyword_set(verbose) then box_message, [parent_proc, 'Overwrite of existing Fits Files with sxi_prep result aborted.', 'Ending Proc']
        return
      endif 
      if ans eq 3 then  file_overwrite = 3
    endif 
    ;
    ;print, fits_file_output_dir ;test only remove
    if not file_test(fits_file_output_dir,/DIRECTORY) then  mk_dir, fits_file_output_dir ;this will fail if it already exists 
    print, 'fits_full_file_name =', fits_full_file_name
    index_fits_write = oindex_l1[i]
    data_fits_write = reform(odata[*,*,i])
    mwritefits, index_fits_write, data_fits_write, outdir=fits_file_output_dir, outfile=fits_full_file_name,/nocomment 
    ;can this be vectorized?  Each write is to a different file, and possibly to a different directory.
  endfor
    ;
endif
;

;the reason this might be okay is that if there is no file write, there is no .FNAME/.FILENAME change.
; Clobber input variables if oindex & odata are NOT supplied:
if n_params() lt 4 then begin
  index = oindex_l1 ;the data was already updated.
endif

;
end
