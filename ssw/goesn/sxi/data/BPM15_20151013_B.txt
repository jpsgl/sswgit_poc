BPM_20151013_B.txt                                           C    4  R    16
# FILE:
#       initial_none.bpm
# HISTORY:
#       Created 05-Jun-09 by M.England for GOES-O AmpB ("south")
#       Modified 13-Oct-15 by D Busnardo new values per LMSAL
#
# DESCRIPTION:
#	Bad pixel map is used to identify hot pixels which 
#	should not be used in the histogram generation or in
#	the bright pixel location alogorithm
#
#	The definition is the column/line for the bad pixel
#	in the 528x588 array for amplifier "South".  The
#	"Column Blem?" column indicates whether all pixels
#	in the column AFTER that bad pixel are also bad
#	(ie: a bad column). 
#
#	The last entry must be 65535,32767,1 to mark the end of
#	the variable length table (FFFF,FFFF).  
#
#	Maximum length is 1024 pixels.
#
# Entry    Column	Line	Column
#				Blem? (1=yes)
#
#		Word0	Word1	Word0
#		Bit1-15	bit0-15	bit0 (MSB)
#
	0	241	0	1
	1	242	0	1
	2	243	0	1
	3	244	0	1
	4	245	0	1
	5	246	0	1
	6	247	0	1
	7	248	0	1
	8	249	0	1
	9	250	0	1
	10	251	0	1
	11	252	0	1
	12	253	0	1
	13	254	0	1
	14	255	0	1
	15	65535	32767	1
