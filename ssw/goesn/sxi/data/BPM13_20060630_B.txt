BPM_20060630_B.txt                                           C    4  R    1
# FILE:
#       initial_none.bpm
# HISTORY:
#       Created 26-Feb-11 by M.Morrison
#		07-May-16 J.Lemen   - Construct table for early ops
#
# DESCRIPTION:
#	Bad pixel map is used to identify hot pixels which 
#	should not be used in the histogram generation or in
#	the bright pixel location alogorithm
#
#	The definition is the column/line for the bad pixel
#	in the 528x588 array for amplifier "South".  The
#	"Column Blem?" column indicates whether all pixels
#	in the column AFTER that bad pixel are also bad
#	(ie: a bad column). 
#
#	The last entry must be 65535,32767,1 to mark the end of
#	the variable length table (FFFF,FFFF).  
#
#	Maximum length is 1024 pixels.
#
# Entry    Column	Line	Column
#				Blem? (1=yes)
#
#		Word0	Word1	Word0
#		Bit1-15	bit0-15	bit0 (MSB)
#
	0 	65535	32767	1
