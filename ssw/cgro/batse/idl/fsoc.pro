;+
; PROJECT:
;	SDAC
; NAME: 
;	FSOC
;
; PURPOSE:
; 	This procedure provides operator communications for the Flare search plotting task, fsplot. 
;
; CATEGORY:
; BATSE
;
; CALLING SEQUENCE:
;	FSOC, Cmnd, Listoc, INCOMMANDS=INCOMMANDS
;
; CALLS:
;	RD_TEXT, EXIST, ATIME, FCHECK, CHKARG, FSOC_AUX,
;	SETUT, GETUT, GETUTBASE, FIND_DBFILE, BATSE_TIME_INPUT
;
; INPUTS:
;       none explicit, only through commons;
;
; OPTIONAL INPUTS:
;	Listoc: If set, Menu and Options are listed
;
; OUTPUTS:
;	Cmnd: Returned interpreted command action for FSPLOT
;	
; OPTIONAL OUTPUTS:
;	none
;
; KEYWORDS:
;	INCOMMANDS: An array of command strings for command input in lieu of input 
;	from terminal (sys$input).  Command strings are in the same format as
;	for keyboard input.
;
; COMMON BLOCKS:
;	fscom,fs_saveaccum,scalecom,remotecom
;
; SIDE EFFECTS:
;	none
;
; RESTRICTIONS:
;	none
;
; PROCEDURE:
;	none
;
; MODIFICATION HISTORY:
; History:
; Shelby Kennard                               1Feb1991
; Modified by AES 94/08/30  Added new menu option, autosize.  Starts
; enabled.  Causes program to resize the plot window larger if the user
; picks the "all4" data option.  Window returns to normal size for other
; options.
; Modified by AES 94/09/01 If two or fewer graphs in either direction,
; set global sizing variable to 1.  Otherwise set to 2.
; Mod. by AES 95/05/23 Added extra command - MakeGif
; Mod. by AES 95/06/28 to check for OS before printing filename
; Mod ras 29-mar-1996, change maxseconds algorithm
; Version 6, ras, 16-april-1996, added INCOMMANDS
; Version 7, ras, 4-dec-1996, RD_TEXT not RD_ASCII
; Version 8, richard.schwartz@gsfc.nasa.gov, pass original command line to
;	find_dbfile, 4-sep-1997
; Version 9, richard.schwartz@gsfc.nasa.gov, now accepts all ANYTIM compatible time strings for
;	time parameters INPUT, START, and END.
;-


pro fsoc,cmnd,listoc, INCOMMANDS=INCOMMANDS
@fscom
@fs_saveaccum
@scalecom
@remotecom

checkvar, incommands, strarr(1)
wcommands = where(incommands ne '', ncommands)
if ncommands ge 1 then incommands=incommands(wcommands)

;
;	Limit is packet limit in setup_batse.pro
;
if not exist(maxseconds) then begin
    packlim = 6500
    setup_batse ='setup_batse.pro'
    chkarg,'setup_batse',proc, loc,/reset,/search
    proc = rd_text(concat_dir(loc(0), setup_batse))
    wproc=where( strpos(proc,'packlim = ') ne -1,nwproc)
    if nwproc ge 1 then loc =execute( proc(wproc(0)) )
    maxseconds = packlim * 2
endif
command = $
	'ACCGRAZOOXY0POILOGEXILASPRIFILMARFLATYPPLOHELSAVMAIHARARCSELRETQUIMAK'
comma=','
inline = ' '
ayaxis=['Linear','Log']
able=['Disable','Enable']
dable=['Enable','Disable']
adata =['DISCLA', 'CONT', 'DISCSP', 'GOES', 'ALL4']
autosize =['Disable','Enable']
aoutprint =['Terminal','Printer','File']

if fcheck(listoc,0) eq 0 then goto,read
listoc=0
list:
a_stime = atime(sec_st, /hxr)
a_etime = atime(sec_en, /hxr)

;if xmin ne 0 then a_xmin = strmid(atime(xmin,/hxr),10,11) else a_xmin = '0.000'
;if xmax ne 0 then a_xmax = strmid(atime(xmax,/hxr),10,11) else a_xmax = '0.000'
if xmin ne 0 then a_xmin = anytim(xmin,/hxr,/time) else a_xmin = '0.000'
if xmax ne 0 then a_xmax = anytim(xmax,/hxr,/time) else a_xmax = '0.000'

alpha_page
;
; Display current values for parameters and menu of commands.
;
print, ' '
print, $
 '---------------------------------------------------------------------------'
print, 'BATSE Rates Plot Program: Interactive plotting of user-selected ', $
       'time '
print, 'intervals, detectors, channels, and resolution.'
print, $
 '---------------------------------------------------------------------------'
print,' '
print,'Change options by entering first 3 letters of item, comma, new value.'
print, $
'(To select input file, type  INP,FILENAME   INP,T,time   INP,F,flare# )'
print, ' '
print,'After setting options, enter one of the available commands listed below.'
print,'  '
if n_elements(discla_open) ne 0 then file = discla_open.filename else file=' '
print,format='("Input file",T46,A)', file
print,format='("Data type  DISCLA/CONT/DISCSP/GOES/ALL4",T46,A)',$
   adata(dd_type-1 > 0)
print,format='("Start time of plot",T46,A)',a_stime
print,format='("Total time (seconds)",T46,F14.3)',sec_to
print,format='("End time of plot",T46,A)',a_etime 
;print,format='("Sample average (DISCLA)",T43,I4)',sam_av
;print,format='("Channels selected (DISCLA) ",T45,8i2)',ch_arr
;print,format='("Detectors selected (DISCLA)",T45,8i2)',det_arr
;print,format='("Aspect ordered detectors (DISCLA)  Enable/Disable",T56,A)',able(aspord)

case 1 of
   dd_type le 1: fsoc_aux, 'DISCLA', sam_av, ch_arr, det_arr, aspord, $
                            usefdb, /list
   dd_type eq 2: fsoc_aux, 'CONTINUOUS', cont_sam_av, cont_ch_arr, $
                            cont_det_arr, cont_aspord, usefdb, /list
   dd_type eq 3: fsoc_aux, 'DISCSP', dsp_sam_av, dsp_ch_arr, $
                            dsp_det_arr, dsp_sensord, usefdb, /list
   dd_type eq 4: fsoc_aux, 'GOES', goes_sam_av, goes_ch_arr, dum1, dum1, $
                            usefdb, /list
   else: 
endcase

print,format='("Histogram  Enable/Disable",T46,A)',able(histogram)
print,format='("Yaxis Log/Linear",T46,A)',ayaxis(yaxis)
print,format='("Triggertime Xaxis Enable/Disable",T46,A)',able(reltotrig)
print,format='("Fiducial mark at triggertime Enable/Disable",T46,A)', $
   able(fiducmark)
print,format='("Xmin,Xmax",T46,A14,",",A14)',a_xmin,a_xmax
print,format='("Ymin,Ymax",T46,F14.3,",",F14.3)',ymin,ymax
print,format='("Multiple plots",T46,i1,"x",i1)',xmulti,ymulti
print,format='("Create plot file  Enable/Disable", T46,A)',dable(noplotfile)
if !d.name eq 'X' then $
print,format='("Autosize Plot Window  Enable/Disable", T46,A)',autosize(winset)
print,format='("Outprint Terminal/Printer/File",T46,A8)',aoutprint(outprint)
addrexist = size(mailaddr)
if addrexist(1) ne 0 then print,format='("Email address", T46, A)', mailaddr
print,'  '
print, $
 'Commands:  ListMenu  Help  Graph  PlotFlare  ArchivePlot  MailPlot '
print, $
 '  Overlay  Zoom  XY0  Point  HardPlot  Print  FileList  MarkFlare  Flarelist '
print, '  TypeFlare  SelectDevices  MakeGif Save  Exit'
print,'Change parameters or enter a command'

; Prompt for input and read response from operator.

on_ioerror, errorinput
on_error, 3

read:
if incommands(0) ne '' then begin
	inline = incommands(0)
	if n_elements(incommands) gt 1 then $
		incommands=incommands(1:*) else incommands=strarr(1)
endif else $
read,inline
origline = inline
;
;
; Convert any times into hxrbs standard time strings for the INPUT, START, and END parameters.
;
inline = batse_time_input( inline, ['STA','END','INP'])
inline = strupcase(strcompress(inline,/remove_all))


; Compare commands.
inline0 = inline
cmnd = strmid(inline, 0, 3)

if cmnd eq 'LIS' then begin
  goto, list
endif

if cmnd eq 'DEL' then begin  ;delete plotfile command is not publicly available
  del_plotfile
  goto,read
endif

for j=0,22 do begin
   if cmnd eq strmid(command, j*3, 3) then goto,getout
endfor

; Compare parameters.

param = strmid(inline, 0, 3)
param_start = strpos(inline, comma) + 1
case 1 of
  param eq 'INP': begin
    if param_start eq 0 then goto, errorinput
    if usefdb eq 1 then temp_type = 1 else temp_type = 0
    find_dbfile, inline0, param_start, discla_open, temp_type, sec_st, sec_to, sec_en
    xmin = 0. & xmax = 0.
    ddata_accumed = 0
    cdata_accumed = 0
    spdata_accumed = 0
  end

  param eq 'DAT': begin
    if param_start eq 0 then goto, errorinput
    strng = strmid(inline,param_start,3)
    q = where (strpos(adata, strng) ne -1, count)
    sav_dd_type = dd_type
    case 1 of 
       count eq 1: dd_type = q(0) + 1
       count eq 2: $
          if strmid(inline,param_start,5) eq 'DISCL' then dd_type = 1 $
             else if strmid(inline,param_start,5) eq 'DISCS' then dd_type = 3 $
                else goto, errorinput
       else: goto, errorinput
    endcase
;    if dd_type eq 2 or dd_type eq 3 or dd_type eq 5 then begin
;       print,'CONT and DISCSP data types not available yet.  Will be soon.'
;       dd_type = sav_dd_type
;    endif else goto, list
  end        

  param eq 'STA': begin
    strng = strmid(inline,param_start,21)
    if param_start eq 0 then goto, errorinput
    setut,utbase=strng, utstart=strng, error = error
    if error then goto, errorinput
    getut,utstart=sec_st
    sav_en_sec = sec_en
    sec_en = sec_st + sec_to
    setut,utend=atime(sec_en),error = error
    if error then goto, errorinput
    ddata_accumed = 0
    cdata_accumed = 0
    spdata_accumed = 0
  end

  param eq 'TOT': begin
    if param_start eq 0 then goto, errorinput
    tem_to = double (strmid(inline,param_start,21) )
    if tem_to lt 0 then begin
      print,'Value must be greater than zero. Please reenter.'
      goto,read
    endif 
    ;maxseconds = n_elements(seconds)
    if tem_to gt maxseconds then begin
      print,'Asking for too much data.  Maximum is ', maxseconds, ' seconds.'
      goto,read
    endif
    sav_to = sec_to
    sec_to = tem_to
    sav_en_sec = sec_en
    sec_en = sec_st + sec_to
    setut,utend=atime(sec_en),error = error
    if error then begin
      print,'Please reenter.'
      sec_en = sav_en_sec
      sec_to = sav_to
    endif
    ddata_accumed = 0
    cdata_accumed = 0
    spdata_accumed = 0
  end

  param eq 'END': begin
    if param_start eq 0 then goto,errorinput
    strng = strmid(inline,param_start,21)
    getut,utend=sav_en_sec
    setut,utend=strng, error = error
    if error then goto, errorinput
    getut,utend=en_sec
    if en_sec lt sec_st then begin
      print,'End time can not be before start time. Please reenter.'
      setut,utend=atime(sav_en_sec), error=error
      goto,read
    endif
    ;maxseconds = n_elements(seconds)
    if en_sec-sec_st gt maxseconds then begin
      print,'Asking for too much data.  Maximum is ',maxseconds, ' seconds.'
      goto,read
    endif
    sec_en = en_sec
    sec_to = sec_en - sec_st
    ddata_accumed = 0
    cdata_accumed = 0
    spdata_accumed = 0
  end

  param eq 'HIS': begin
    if param_start eq 0 then goto, errorinput
    strng = strmid(inline,param_start,3)
    temp = -1
    if strng eq 'ENA' then temp = 1 
    if strng eq 'DIS' then temp = 0
    if temp eq -1 then goto, errorinput
    histogram = temp
  end

  param eq 'YAX': begin
    if param_start eq 0 then goto, errorinput
    strng = strmid(inline,param_start,3)
    case 1 of
      strng eq 'LOG': begin
        yaxis = 1
      end
      strng eq 'LIN': begin
        yaxis = 0
      end
      else: begin
        print,'Enter either ''log'' or ''linear''.  Try again'
      end
    endcase
  end        

  param eq 'TRI': begin
    if param_start eq 0 then goto, errorinput
    strng = strmid (inline,param_start,3)
    case 1 of
      strng eq 'ENA': reltotrig = 1
      strng eq 'DIS': reltotrig = 0
      else: print,'Enter either Enable or Disable.  Try again'
    endcase
  end

  param eq 'FID': begin
    if param_start eq 0 then goto, errorinput
    strng = strmid(inline,param_start,3)
      case 1 of
        strng eq 'ENA': fiducmark = 1
        strng eq 'DIS': fiducmark = 0
        else: print,'Enter either ''enable'' or ''disable''.  Try again'
      endcase
  end
;
  param eq 'OUT': begin
    if param_start eq 0 then goto, errorinput
    strng = strmid(inline,param_start,3)
    case 1 of
      strng eq 'PRI': outprint = 1
      strng eq 'TER': outprint = 0
      strng eq 'FIL': outprint = 2
      else: print,'Enter Terminal, Printer, or File.  Try again.'
    endcase
  end        
;
  param eq 'XMI': begin
    if param_start eq 0 then goto, errorinput
    strng = strmid (inline, param_start, 21)
    setut, utbase=strng, error = error
    if error then goto, errorinput
    xmin = getutbase(0)
  end
;
  param eq 'XMA': begin
    if param_start eq 0 then goto,errorinput
    strng = strmid (inline, param_start, 21)
    setut, utbase=strng, error = error
    if error then goto, errorinput
    xmax= getutbase(0)
  end
;
  param eq 'YMI': begin
    if param_start eq 0 then goto, errorinput
    strng = strmid (inline, param_start, 10)
    ymin = float(strng)
  end
;
  param eq 'YMA': begin
    if param_start eq 0 then goto, errorinput
    strng = strmid (inline, param_start, 10)
    ymax = float(strng)
  end
;
  param eq 'MUL': begin
    if param_start eq 0 then goto, errorinput
     s = strmid(inline, param_start, 20)
     xposition = strpos(s, 'X')
     if xposition eq -1 then begin
        ix = fix(s)
        if ix ne 1 then begin
           print, 'Error - enter either:'
           print, '1 to disable multiple plots or '
           print, 'm x n for m plots across, n plots down.'
        endif else begin
           xmulti = 1
           ymulti = 1
           !p.multi = [0,1,1]
           global = 1
        endelse
     endif else begin
        ix = fix (strtrim(strmid(s,0,xposition),2))
        iy = fix (strtrim(strmid(s,xposition+1,20),2))
        if (ix lt 1) or (iy lt 1) or (ix gt 9) or (iy gt 9) then goto,errorinput
        xmulti = ix
        ymulti = iy
        !p.multi = [0,xmulti,ymulti]
        if xmulti gt 2 or ymulti gt 2 then global=2 else global=1
     endelse     
  end
;
  param eq 'CRE': begin
    if param_start eq 0 then goto, errorinput
    strng = strmid(inline,param_start,3)
    temp = -1
    if strng eq 'ENA' then temp = 0
    if strng eq 'DIS' then temp = 1
    if temp eq -1 then goto, errorinput
    noplotfile = temp
    if not noplotfile then begin
       cd,current=dir
       print, 'Plot file will be created called '
       case (!version.os) of
	'OSF' : print, dir,'/fsplot.tek or .ps for Tektronix or PostScript' 
        'vms' : print, dir,'fsplot.tek or .ps for Tektronix or PostScript'
	endcase
    endif
  end
;
  param eq 'EMA': begin
    if param_start eq 0 then goto, errorinput
    mailaddr = strmid (inline, param_start, 100)
  end
;
  param eq 'AUT': begin
    if param_start eq 0 then goto, errorinput
    strng = strmid(inline,param_start,3)
    temp = -1
    if strng eq 'ENA' then temp = 1
    if strng eq 'DIS' then temp = 0
    if temp eq -1 then goto, errorinput
    winset = temp
  end
;
; OVERLAY command is an unusual case.  It is a command with a parameter.
; User types OVE,2. to overplot with a scale factor of 2.  Get the scale
; factor and store it in overlay_scale, then set cmnd to OVE and return.
; If user didn't enter a scale factor, set it to 1.
  param eq 'OVE': begin
    if param_start eq 0 then begin
       overlay_scale = 1.
    endif else begin
       overlay_scale = float (strmid (inline, param_start, 20))
    endelse
    cmnd = 'OVE'
    goto,getout
  end
;
  else: begin

     case 1 of
       dd_type le 1: begin
                     fsoc_aux, 'DISCLA', sam_av, ch_arr, det_arr, aspord, $
                     usefdb, origline=origline, error=error
                     ; if data type selected is DISCLA, and want to use 
                     ;BDB database, set dd_type=0
                     if usefdb eq 0 then dd_type = 0
       end
       dd_type eq 2: fsoc_aux, 'CONTINUOUS', cont_sam_av, cont_ch_arr, $
                     cont_det_arr, cont_aspord, usefdb, $
                     origline=origline, error=error
       dd_type eq 3: fsoc_aux, 'DISCSP', dsp_sam_av, dsp_ch_arr, $
                     dsp_det_arr, dsp_sensord, usefdb, $
                     origline=origline, error=error
       dd_type eq 4: fsoc_aux, 'GOES', goes_sam_av, goes_ch_arr, dum1, dum2, $
                     usefdb, origline=origline, error=error
       else: error = 1
    endcase
    if not(error) then goto, read

    errorinput:
    print,'Error in input -- please reenter.'
    goto,read

  end

endcase
goto, read

getout:

; if data type selected is DISCLA, and want to use BDB database, set dd_type=0
if dd_type lt 2 and usefdb eq 0 then dd_type = 0

end

