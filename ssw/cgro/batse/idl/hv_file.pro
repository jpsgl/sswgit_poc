;
;+
; PROJECT:
;	SDAC
; NAME: 
;       HV_FILE
;
; PURPOSE:
; HV_FILE creates and updates a file containing all time intervals  when 
; the BATSE high voltage was off.  Used with the gap file and the day/night
; file, this info gives the BATSE solar observing times. 
;
; CATEGORY:
;       BATSE
;
; CALLING SEQUENCE:
;	hv_file, endday
;
; CALLS:
;	none
;
; INPUTS:
;       endday- Time to process to.  Interpreted by Utime (anytim).
;
; OPTIONAL INPUTS:
;	none
;
; OUTPUTS:
;       none explicit, only through commons;
;
; OPTIONAL OUTPUTS:
;	none
;
; KEYWORDS:
;	none
; COMMON BLOCKS:
;	none
;
; SIDE EFFECTS:
;	none
;
; RESTRICTIONS:
;	This is an SDAC database maintenance procedure and not
;	intended for general use.  VMS specific.
;
; PROCEDURE:
; The hv file has the following format:
;    header record - bytes 0-7:   latest time in seconds since 79/1/1,0
;                    bytes 8-11:  spare
;                    bytes 12-15: number of data records following header
;    data records  - bytes 0-7:   start time of hv off interval
;                    bytes 8-15:  end time of hv off interval
; HV_FILE reads the header record to determine start time, then opens
; each quality DB file from that day up to and including the end day specified
; in the calling argument, recording time intervals for which the high voltage
; is off.
;
; This hv file must be updated in time order.
; Can be killed anytime, and the good intervals from the last quality file
; it finished will be recorded (after each quality file, it writes the hv off
; intervals, and rewrites the header record).
;
;
; MODIFICATION HISTORY:
;
; Kim Tolbert  5/20/92
; AES 12/17/96  Write hv_file.dat in stream mode.  Fixed for unix.
; Version 3, richard.schwartz@gsfc.nasa.gov, 24-jun-1998, forced old VAX float format on write.
;
;-
pro hv_file, endday
;
header = {header_str, latest_time: 0.d0, spare: 0L, numrec:0L}
;
;
print,'Requested end time: ', endday
;
hvfile = concat_dir((chklog('BATSE_DATA'))(0),'hv_file.dat')
fileexist = findfile (hvfile, count=count)
if count eq 0 then begin
   print, 'Opening new output file ', hvfile
   openw, lunout, hvfile, 16, /get_lun, /stream
   header.latest_time = utime('91/4/19')
   header.numrec = 0
   writeu, lunout, use_vax_float(/new2old, header)
endif else begin
   spawn, 'copy/lo ' + hvfile + ' ' + hvfile
   print,'Opening existing hv file ',hvfile
   openr, lunout, hvfile, 16, /get_lun
   readu, lunout, header
   header = conv_vax_unix( use_vax_float( /old2new, header ))
   close, lunout
   openu, lunout, hvfile, 16, /append, /stream
endelse
;
; h = assoc (lunout, header)   ; akt 9/17/93
;
; time will be the current time we're on.  Initialized to one day after
; latest time in file, then incremented by one day after we finish each
; quality file.
time = fix(header.latest_time / 86400.d0) * 86400
print,'Latest time in current hv file = ', atime(header.latest_time,/hxrbs)
print,'First time to process =', atime(time,/hxrbs)
;
; Do this loop until we are past the end time
;
while time lt utime(endday)+86400.d0 do begin
   print, ' '
   hv_finder, time, offtimes, koff
   if koff gt 0 then begin
      writeu, lunout, use_vax_float(/new2old, offtimes(*,0:koff-1))
      header.numrec = header.numrec + koff
   endif
   header.latest_time = time + 86400.d0
   print, 'Latest time = ',atime(header.latest_time,/hxrbs), $
          '  # off intervals in file = ',header.numrec
   point_lun, lunout, 0
   writeu, lunout, use_vax_float(/new2old, header)
   ;h(0) = header   ; akt 9/17/93
   close, lunout
   openu, lunout, hvfile, 16, /append, /stream
   time = time + 86400.d0
endwhile
close, lunout
goto,getout
;
getout:
end
