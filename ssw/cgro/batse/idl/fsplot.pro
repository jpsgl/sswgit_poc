;+
; NAME:	FSPLOT
;
; PURPOSE:
;   	Plots light curves of BATSE data using the detectors, channels, 
;	and sample averages specified by the operator.
;
; CATEGORY:   BATSE
;
; CALLING SEQUENCE:
;   	Fsplot
;
; CALLS:
;	HXRBS_FORMAT, FIND_DBFILE, FSOC, FS_ACC, FS_GEN_ARR, FS_ACC_CONT,
;	FS_ACC_DISCSP, FS_ACC_GOES, FS_GRAPH, FS_OVERLAY, GET_UTBASE,
;	SETUT, ATIME, FS_AUTO_LOG, TEK_PRINT, FS_PRINT, FILELIST,
;	SHOW_FLARE, FLIST, TYPE_FLARE, FSELECT, FINDFILE, FS_ARCHIVE_RW,
;	FS_ARCHIVE_DRAW, PLOT_FLARE, READ_SEQFILE, CONCAT_DIR, MAILFILE,
;	FILE2FLARE, CHKLOG, USER_CHOOSE, SPECTRA2FITS, YOHKOH_FORMAT
;
; INPUTS:
;	None
;
; OUTPUTS:
;	Information saved in file *.out
;
; OPTIONAL OUTPUTS:
;	Save fits file, ascii file or plot mailed to given address. 
;
; KEYWORDS:
;	WPLOT: If WPLOT is set and IFLARE is a flare number, then the 4 panel
;		figure is plotted and the program returns
;	IFLARE: A flare number used with WPLOT
;	INCOMMANDS: A buffer of command strings to be executed sequentially
;	EXTEND - Added to expand the time interval software searches for pointing data
;		 while looking at omni events if 3x15min intervals is not long enough.
;		 Expanded to 4x60 min intervals.
;		
; COMMON BLOCKS:
;    FSCOM, FS_SAVEPLOT, FS_SAVEACCUM, SCALECOM
;	
; MODIFICATION HISTORY:
;    Written by:  Shelby Kennard            30Jan1991
;    Mod. by Amy Skowronek 8/18/94 to use getenv instead of trnlog
;    Mod. by AES 8/24/94 to save cont_cos8 for cont data and sp_cos8
;         for discsp data rather than trying to save cos8.
;    Mod. by AES 8/29/94 to change variable global in scalecom
;         for multiple plots so characters remain readable.
;    Mod. by AES 8/30/94 to make a large screen if the user chooses "all4"
;         as a data option, so the words on the plot remain legible.  The
;         user has the option of disabling this automatic resizing of the
;         plot window, using a menu command, autosize.
;    Mod. by AES 10/13/94 to *not* replace long size window with standard
;         size window when leaving fsplot.
;    Mod. by AES 10/24/94 to allow fsplot to be called with a flare
;         parameter.  If called with iflare=number and /wplot, it
;         will display all four data types for that flare number, without
;         asking questions, and then exit fsplot.
;    Mod. by AES 11/1/94 to call window with retain=2 so windows don't get
;         lost.
;    Mod. by AES 1/17/95 to reset variable global to 1 before exiting to
;         main menu, so type size is reset after using the dat,all4 
;         option.
;    Mod. by AES 4/6/95 to allow for FITS save - however as software is not
;	  completely ready, asking for fits save reroutes to XDR.
;    Mod. by AES 4/11/95 to save livetime in SAVE option
;    Mod. by AES 4/27/95 to save BATSE user's files to anonymous.input.batse,
;         and only the required segment of the livetime array.  Also save
;         files with flare number in name, e.g. discla_299.sav
;    Mod. by AES 5/23/95 to save plot in .gif format using command MakeGif
;    Mod. by AES 6/28/95 to check for architecture before printing filename.
;    Mod. by RCJ 12/04/95 to make global=1 when entering ARC command.
;    Mod. by RCJ 02/23/95 to save correct livetimes for cont and discsp data.
;    Mod. by RAS 16-apr-1996 to add input buffer command processing, also
;	defaults are no longer reset.
;    Mod. by RCJ 05/06/97 to save arrays in binary fits table using 
;	spectra2fits. Also update documentation.
;    Version 21, RAS, 7-jun-1997, minor changes using loc_file to replace concat_dir
;    VERSION 22, richard.schwartz@gsfc.nasa.gov, 26-aug-1997,
;    used loc_file with path_dir('batse') to locate help files.
;    VERSION 23, eva@kano.nascom.nasa.gov, 13-May-1999.
;	Added flag EXTEND -- when set, will expand the time interval
;	that software searches through to find pointing info when event is omni 
;	flare. Search changes from original 3x15 min intervals to 4x60min intervals.
;    Version 24, 	richard.schwartz@gsfc.nasa.gov, 6-jun-2000.
;	replaced incorrect block terminators (ENDIF) with correct END statements.	
;-
;****************************************************************************
pro fsplot, iflare=iflare, wplot=wplot, incommands=incommands, EXTEND=EXTEND
!quiet = 1
@fscom
@fs_saveplot
@fs_saveaccum
@scalecom

hxrbs_format, old_format = old_time_format

; Set defaults for operator communications.
;
defaults:
plotfile_created = 0
if not exist( ch_arr ) then fsdef,x
xfac=1. & yfac=1.
if !d.name eq 'X' then begin
device,get_screen_size=sc
xfac=(sc(0)/1280.)
yfac=(sc(1)/1024.)
endif 
;
; if keyword wplot is set, means graph all four data types for this flare,
; with fiducial mark and trigger plot enabled, and get out of fsplot.
; do not ask the user any questions.
if keyword_set(wplot) then begin   $
         fiducmark = 1
	 winset=1
	 reltotrig=1        
         flarenum=strtrim(iflare,2)
         find_dbfile,'INP,F,'+flarenum,4,discla_open,1,sec_st,sec_to,sec_en
         ddtype=5
 	 goto,graph_wplot
         endif
;
if (size(discla_open))(2) eq 8 then $
   find_dbfile,discla_open.filename,0,discla_open, discla_open.type,sec_st,sec_to,sec_en
;
; Print operator communications to the screen.
;
listoc = 1
op_com:
fsoc,cmnd,listoc, incommands=incommands
;
; Determine command specified and act accordingly.
;
execute_command:

case 1 of

cmnd eq 'ACC' or cmnd eq 'GRA': begin
graph_wplot:
   if keyword_set(wplot) then dd_type=5
  if n_elements(discla_open) eq 0 then begin
     print,'Please select an input file before attempting to accumulate.'
     goto, op_com
  endif

  if discla_open.type eq 0 and (dd_type eq 2 or dd_type eq 3 or $
     dd_type eq 5) then begin
     print,'Cannot plot CONT or DISCSP data corresponding to a BDB file.'
     print,'You must first open a DISCLA FDB file for the flare you want.'
     goto, op_com
  endif

  if sec_en le sec_st then begin
    print,'Error--end time is less then or equal to start time, please correct.'
    goto, op_com
  endif
  
  global=1
  sub_back = 0 & div_area = 0 & corr_angle = 0

  save_dd_type = dd_type
  save_multi = !p.multi

  if dd_type eq 5 then begin
     type1 = 1  &  type2 = 4
     !p.multi=[0,1,4]
     global=2                     ;set global to 2 to compensate for halving
                                  ;of charsize in multiplotting
;make a large window to accomodate graphs
     if winset and !d.name eq 'X' then begin     
     if !d.window eq -1 then window,retain=2,ysize=959*yfac else $
     window,retain=2,!d.window,ysize=959*yfac  
     print,'The plot window size has been enlarged to ensure that the'
     print,'characters remain legible.'
     endif
  endif else begin
     type1 = dd_type  &  type2 = dd_type 
  endelse

  for dd_type = type1,type2 do begin
     if type1 eq type2 and winset and !d.window ne -1 and !d.y_size ne 512 $
      and !d.name eq 'X' then window,retain=2,!d.window

     case 1 of
        dd_type le 1:  begin
           if not(ddata_accumed) then begin
              fs_acc, error = error
              if error then goto, reset_plot
              ddata_accumed = 1
           endif
           fs_gen_arr, error = error
           if error then goto, reset_plot
           end

        dd_type eq 2:  fs_acc_cont, error = error

        dd_type eq 3:  fs_acc_discsp, error = error

        dd_type eq 4:  fs_acc_goes, error=error

     endcase

     if error then goto,reset_plot

     if dd_type le 3 then fs_graph, error = error 

  endfor

  reset_plot:
  dd_type = save_dd_type
  if  dd_type ne 5 then global=1
  if dd_type eq 5 then !p.multi = save_multi

  plotfile_created = 0
  if not(error) then begin
     dd_type_plotted = dd_type
     data_plotted = 1
     if not(noplotfile) then plotfile_created = 1 
  endif

  if not(error) and noplotfile then begin
     print,'NOTE: To mail a plot to yourself or to make a hardcopy ', $
           'of this plot (via '
     print,'      HardPlot command), CREATE PLOT FILE must be ENABLED ', $
           'before graphing.'
  endif
  if keyword_set(wplot) then goto, getout
end
;
cmnd eq 'OVE': begin
   if data_plotted eq 0 then begin
      print, 'No data plotted.'
      goto,op_com
   endif
   if dd_type_plotted gt 3 then begin
      print, 'OVERLAY invalid for this type of plot.'
      goto, op_com
   endif
   if (corr_angle eq 1) or (sub_back eq 1) then begin
      print, 'Can not overlay plot on background-subtracted plot.'
      goto,op_com
   endif
  case 1 of
     dd_type le 1:  begin
        if not(ddata_accumed) then begin
           fs_acc, error = error
           if error then goto,op_com
           ddata_accumed = 1
        endif
        fs_gen_arr, error = error
        if error then goto,op_com
        end
     dd_type eq 2:  fs_acc_cont, error = error
     dd_type eq 3:  fs_acc_discsp, error = error
     else:
  endcase
  if error then goto,op_com
  fs_overlay, scale = overlay_scale, error = error
end
;
cmnd eq 'ZOO': begin
   if data_plotted eq 0 then begin
      print, 'No data plotted.'
      goto,op_com
   endif
  if dd_type_plotted gt 3 then begin
     print, 'ZOOM invalid for this type of plot.'
     goto, op_com
  endif
  zoom_coor,x,y
  xmin = x(0) + getutbase(0)
  xmax = x(1) + getutbase(0)
  ymin = y(0) 
  ymax = y(1)
  fs_graph, error=error
  print,'Note:  These plot limits are now the default.'
  print,'       Enter XY0 command to return to autoscaling.'
end
;
cmnd eq 'XY0': begin
  xmin = 0.0
  xmax = 0.0
  ymin = 0.0
  ymax = 0.0  
  setut,utstart=atime(sec_st),utend=atime(sec_en)
end 
;
cmnd eq 'POI': begin
  point
end
;
cmnd eq 'LOG': begin
;  test = trnlog("sys$login",trans,/full)
;  if strpos(trans(0), 'BATSE') ne -1 then begin
    if getenv('USER') eq 'BATSE' then begin
    print,'Logging flares is not allowed on this account.'
    goto,op_com
  endif
  fs_auto_log, EXTEND=EXTEND		; 13-May-1999 -- eva
;  fs_auto_log
end
;
(cmnd eq 'LAS') or (cmnd eq 'HAR'): begin    ; allow LASER or HARDCOPY
  if plotfile_created then begin
    tek_print,filename='fsplot'
  endif else begin
    if not(data_plotted) then print,'No data to plot'
    if plotfile_created eq 0 then print,'You didn''t create a plot file.'
    if plotfile_created eq 0 then print,'Set CREATE PLOT FILE to ENABLE ', $
                             'before making plot.'
  endelse
end
;
cmnd eq 'PRI': begin
  if data_plotted then begin
    fs_print
  endif else begin
    print,'No data to print'
  endelse
end
;
cmnd eq 'FIL': begin
   filelist, dd_type 
end
;
cmnd eq 'MAR': begin
   if data_plotted then begin
      show_flare
   endif else begin
      print,'No data plotted.'
   endelse
end
;
cmnd eq 'FLA': begin
   flist
end
;
cmnd eq 'TYP': begin
   type_flare
end
;
cmnd eq 'ARC': begin
   global=1                    ;to halve charsize after doing all4 graph
   fselect, flares, fcount, what = 'flares as '
   if fcount eq 0 then goto,op_com
   for i = 0, fcount-1 do begin
      filesearch = 'flare_' + strtrim(string(flares(i)),2) + '.sav'
      files = loc_file (path='BATSE_FLARES',filesearch, count=count)
      if count eq 0 then begin
         print,'No plot file for flare ', flares(i)
         goto,next_flare
      endif
      data_plotted = 1
      if winset and !d.window ne -1 and !d.name eq 'X' and $
        !d.y_size ne 512*yfac then window,retain=2,!d.window
      if not(noplotfile) then plotfile_created = 1
      fs_archive_rw, file=files(0), /read
      fs_archive_draw, flare=flares(i)
   next_flare:
   endfor
end
;
cmnd eq 'PLO': begin
   if winset and !d.window ne -1 and !d.y_size ne 512*yfac and $
      !d.name eq 'X' then window,retain=2,!d.window
   plot_flare
end
;
cmnd eq 'HEL': begin
   helpname  = 'help_fsplot.txt'
   helpfile=loc_file(path=[curdir(), path_dir('batse'),'BATSE_DATA'],helpname) 
   if helpfile ne '' then read_seqfile, morebf, helpfile else print,'Unable to find '+helpname
   more, morebf
   mailfile, helpfile
end
;
cmnd eq 'SAV': begin
;   fits=0
   flnum=file2flare(discla_open.filename)
   datadir=chklog('batse_out')
   q = where ([maxindex,maxcindex,maxspindex] ne 0, kq)
   if kq eq 0 then begin
      print, 'No data accumulated.'
      goto, op_com
   endif
   if kq gt 1 then begin
      choices = ['DISCLA', 'CONTINUOUS', 'DISCSP']
      user_choose, 'Which type of data do you want to save? ', $
         'Choices are: ', choices, selection
      if selection eq -1 then begin 
		selection=0
		print,'DISCLA data will be saved.'
		end
   endif else begin            
      selection = q(0)
   endelse
   selection = selection + 1    ; make it consistent with dd_type values
   formatchoices = ['FITS', 'XDR']
    user_choose,'Which format would you like the data saved in? ',$
      'Choices are: ',formatchoices, formatselection
   
   if (formatselection eq 0) then ending='fits' else ending='dat'
;   if fits then ending='fits' else ending='dat'
;   ending='dat'
;   if fits then print,'FITS writer is not yet implemented.  File will be ',+ $
;	'saved in XDR format'
;   print, 'Variables Saved:'
   case 1 of
      selection eq 1: begin
         if maxindex eq 0 then goto,errorsave
         discla2 = discla(*,*,0:maxindex) & seconds2 = seconds(0:maxindex)
	 livet2= livet(*,0:maxindex)
	 file='discla_'+strtrim(flnum,2)+'.'+ending
         if (formatselection eq 0) then begin
   ; Prepare keywords and comments to be passed to spectra2fits.
   ; Some of these keywords are from discsc_bfits_105.fits
           comments=['DISCLA, CONT or DISCSP data type','LAD or SD detectors',$
               'Instrument used for observation','Telescope (mission) name',$
               'Batse flare number','','Origin of FITS file', $
               'Program that produced this file','J2000 coordinates']
           head_key={datatype:'DISCLA',det_mode:'LAD',instrume:'BATSE',$
               telescop:'CGRO',flare_no:flnum,$
               filetype:'batse flare spectra', origin:'SDAC/GSFC',$
               creator:'spectra2fits',equinox:2000,comments:comments}
           com_head=['There is no primary data array']
           date_obs=anytim(seconds2(0),/hxrbs,/date)
           spectra2fits,seconds2-seconds2(0),discla2, date_obs,$
              livetime=livet2, filename=file,direction_cosines=cos8, $
              flux_unit='counts /s /detector',$
              head_key=head_key, com_head=com_head
              help,discla2,seconds2,cos8,livet2
         endif else begin
	    save, maxindex, discla2, seconds2, cos8, livet2, $
		filename=concat_dir(datadir,file), /xdr
            help,maxindex,discla2,seconds2,cos8,livet2
         endelse
      end
      selection eq 2: begin
         if maxcindex eq 0 then goto,errorsave
         cont2 = cont(*,*,0:maxcindex) & cseconds2 = cseconds(0:maxcindex)
	 cont_livet2 = cont_livet(*,0:maxcindex)
	 file='cont_'+strtrim(flnum,2)+'.'+ending
         if (formatselection eq 0) then begin
            comments=['DISCLA, CONT or DISCSP data type','LAD or SD detectors',$
                'Instrument used for observation','Telescope (mission) name',$
                'Batse flare number','', 'Origin of FITS file', $
                'Program that produced this file','J2000 coordinates']
            head_key={datatype:'CONT',det_mode:'LAD',instrume:'BATSE',$
                      telescop:'CGRO',flare_no:flnum, $
                      filetype:'batse flare spectra', origin:'SDAC/GSFC', $
                      creator:'spectra2fits',equinox:2000,comments:comments}
            com_head=['There is no primary data array']
            date_obs=anytim(cseconds2(0),/hxrbs,/date)
            spectra2fits,cseconds2-cseconds2(0),cont2,date_obs,$
               livetime=cont_livet2, filename=file,direction_cosines=cont_cos8, $
               flux_unit='counts /s /detector', $
               head_key=head_key, com_head=com_head
            help, cont2, cseconds2, cont_cos8, cont_livet2
         endif else begin
	    save, maxcindex, cont2, cseconds2, cont_cos8,cont_livet2, $
		filename=concat_dir(datadir,file),/xdr
            help, maxcindex, cont2, cseconds2, cont_cos8, cont_livet2
         endelse
      end
      selection eq 3: begin
         if maxspindex eq 0 then goto,errorsave
         discsp2 = discsp(*,*,0:maxspindex) & spseconds2=spseconds(0:maxspindex)
	 sp_livet2 = sp_livet(*,0:maxspindex)
 	 file='discsp_'+strtrim(flnum,2)+'.'+ending
         if (formatselection eq 0) then begin
            comments=['DISCLA, CONT or DISCSP data type','LAD or SD detectors',$
                'Instrument used for observation', 'Telescope (mission) name',$
                'Batse flare number','','Origin of FITS file', $
                'Program that produced this file', 'J2000 coordinates']
            head_key={datatype:'DISCSP',det_mode:'SD',instrume:'BATSE',$
                      telescop:'CGRO',flare_no:flnum, $
                      filetype:'batse flare spectra', origin:'SDAC/GSFC', $
                      creator:'spectra2fits',equinox:2000,comments:comments}
            com_head=['There is no primary data array']
            date_obs=anytim(spseconds2(0),/hxrbs,/date)
            spectra2fits,spseconds2-spseconds2(0),discsp2,date_obs,$
               livetime=sp_livet2, filename=file,direction_cosines=sp_cos8, $
               flux_unit='counts /s', $
               head_key=head_key, com_head=com_head
            help, discsp2, spseconds2, sp_cos8, sp_livet2
         endif else begin
	    save, maxspindex, discsp2, spseconds2, sp_cos8, sp_livet2, $
		filename=concat_dir(datadir,file), /xdr
            help, maxspindex, discsp2, spseconds2, sp_cos8, sp_livet2
         endelse
      end
   endcase
   
   print, ' '
   if (formatselection eq 0) then print,'Saved in FITS file ',file $
   else begin
       case !version.os of
	'OSF' : print,'Saved in IDL save/restore file ',datadir+'/'+file, $
		' in XDR format'
	'vms' : print, 'Saved in IDL save/restore file ',datadir+':'+file, $
		' in XDR format.'
	endcase
   endelse
   print, ' '
   goto, op_com
   
   errorsave:
   print, 'No data accumulated.'
end
;
cmnd eq 'MAI': begin                  
   if plotfile_created then begin
      mailplot, file='fsplot', /verify
   endif else begin
    if not(data_plotted) then print,'No data plotted'
    if plotfile_created eq 0 then print,'You didn''t create a plot file.'
    if plotfile_created eq 0 then print,'Set CREATE PLOT FILE to ENABLE ', $
                             'before making plot.'
  endelse
end
;
cmnd eq 'MAK': begin
   if data_plotted then begin
	gif_image=tvrd()
	filnam=discla_open.filename
	gif_flarenum=strtrim(strmid(filnam,strpos(filnam,'.FDB_')+5,5),2)
	gif_filename='flare_'+ gif_flarenum +'.gif'
	write_gif,concat_dir('batse_out',gif_filename),gif_image
	print,'Image has been written to ', $
		chklog('batse_out') + ':' + gif_filename
   endif else print,'No plot is available from which to make a .gif file.'
end
;
cmnd eq 'SEL': begin
   select_dev
end
;
else: goto,getout ; Command is exit.
;
endcase
;
goto,op_com
;
getout:
;
; Reset type size to normal
global=1

; Close all files that might be open

dd_close, discla_open
dd_close, cont_open
dd_close, discsp_open

savedname = !d.name & set_plot,'ps' & device,/close & set_plot,savedname
;

if old_time_format eq 'YOHKOH' then yohkoh_format
end
