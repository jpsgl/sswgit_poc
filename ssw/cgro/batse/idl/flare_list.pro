;+
;
; NAME: 
;	FLARE_LIST
;
; PURPOSE:
;	This procedures prints a formatted list of catalog values for the 
;	input flare list.  Prints to screen or file.
;
; CATEGORY:
;	BATSE
;
; CALLING SEQUENCE:
;	flare_list, good, latest=latest, htmlout=htmlout
;
; CALLS:
;	PAGE_PRINT, ATIME
;
; INPUTS:
;       Good: Vector of flare catalog indices found in FLARE_CATALOG common
;       writebatse:  Name of the output file to print to
;
; OPTIONAL INPUTS:
;	LATEST: (input) = 1 means we're just printing the 10 latest flares
;               (format of output is slightly different from normal).
;
; OUTPUTS:
;       Creates a file BATSE_FLARES.out to the directory specified by BATSE_OUT 
;		environment Variable.
;
; OPTIONAL OUTPUTS:
;	HTMLOUT = If set, creates BATSE_FLARES.html to BATSE_OUT
;       0: no link, 1: GIF and PS links
;
; COMMON BLOCKS:
;	FLISTCOM, FLARE_CATALOG
;
; SIDE EFFECTS:
;	BATSE_OUT will be set to sys$login for vms and $PWD for unix if not 
;	preset. If BATSE_OUT is set to be other than your local directory, 
;		you won't see it unless you know where to look;
;
; RESTRICTIONS:
;	Better be able to open a file in BATSE_OUT.
;
; PROCEDURE:
;	none
;
; MODIFICATION HISTORY:
; Kim Tolbert  6/91
; Mod. by S Kennard to print UNKNOWN for peak rate or total counts if = 1.
; Mod by KT 3/25/93 to print unknown for peak and/or totalcounts if = 1.e13.
; Mod by KT 1/94 to print burst trigger number instead of total counts.
; Mod by Amy Skowronek 94/7/11 - replaced call to pprint with call to 
; 	pageprint (pprint has been renamed to pageprint)
; Mod by EC 5/6/96 to send output to BATSE_OUT to allow redirection of
; 	output to web directories
; Mod by EC 5/14/96 to create and html file with links to GIF and PS files
; 	on demand
; Mod by EC 5/15/96 add variable writebatse which holds output file name
;  	and add variables 2 and 3 to htmlout
; 	[variable is redundantly set in flist.pro to allow joint or seperate
;  	operation of these programs]
;	FORMAL DOCUMENTATION, RAS, 31-MAR-1996
; Mod by RCJ 19/03/97 due to html remodeling, htmlout options are now only 0 
;	and 1. Made room for variable totcounts.
;-
pro flare_list, good, writebatse, latest=latest, htmlout=htmlout
!quiet=1
;
@flistcom
@flare_catalog
;
; Open the file BATSE_OUT:BATSE_FLARES.out, usu. your login dir.
;   (set the environment BATSE_OUT if it isn't already set)
if getenv('BATSE_OUT') eq '' then begin
  if !version.os eq 'vms' then setlog,'BATSE_OUT',chklog('sys$login') else $
    setenv,'BATSE_OUT=$PWD'
endif

; setup output variables for htmlout keyword or output is BATSE_FLARES.out

if keyword_set(htmlout) then begin
  writebatse = 'BATSE_FLARES.html' 
  if htmlout lt 0 or htmlout gt 1 then begin
    Print,'You failed to set the value of HTMLOUT CORRECTLY, Setting to 0!'
    htmlout = 0
  endif
  beg_gif ='<a href='+'"ftp://umbra.nascom.nasa.gov/pub/batse/batsegifs/batse_'
  end_gif='.gif'+'">'     ;GIF</a>'
  beg_ps='<a href='+'"ftp://umbra.nascom.nasa.gov/pub/batse/batseplots/batse_'
  end_ps='.ps'+'">'       ;PS</a>'
  pre='<pre>' & spre='</pre>'
  nofile = 'N/Avail'  ; this var. not in use right now
endif else writebatse = 'BATSE_FLARES.out'
 
openw, lu, concat_dir(getenv('BATSE_OUT'),writebatse), /get_lun,width=90
print,'Writing file:',$
  concat_dir(getenv('BATSE_OUT'),writebatse)
;
nselected = n_elements(good)  ; number of flares selected to print
count = 0 ; initialize counter for number of lines printed to 0

; Procedure to tabulate the selected flares.  Uses page_print procedure to
; print on terminal and/or file and prompt user after printing 18 lines
; instead of scrolling off page.

; if htmlout is set, put an <html> tag in the document

if keyword_set(htmlout) then page_print,outprint,count,lu,'<html><pre><br>'
page_print, outprint, count, lu, ' '
if keyword_set(latest) then begin
   page_print, outprint, count, lu, $
      'Last ten flares identified and entered into BATSE flare catalog: '
   page_print, outprint, count, lu, ' '
endif else begin
   page_print, outprint, count, lu, $
      format='("BATSE Flare List", t42, "Current time:", a)', $
      strmid (atime(sys2ut(0),/hxr), 0, 14)
   page_print, outprint, count, lu, ' '
   page_print, outprint, count, lu, $
      'Number of flares selected = ', nselected
   page_print, outprint, count, lu, ' '
endelse
;
format1 = '(" BATSE", T10,"Start", T20,"Start", T30,"Peak", T37,"Duration",' + $
           'T47,"Peak rate", T60,"Total",T67," Burst", T75,"Triggertime")'
format2 = '(" Event", T10,"Date", T20,"Time", T30,"Time", T40,"(sec)",' + $
           'T47,"(counts /", T59,"Counts",T67,"Trigger", T77,"(seconds")' 
format3 = '(t9,"yy/mm/dd", t19,"hhmm:ss", t29,"hhmm:ss", T46,"s/2000cm^2)",' + $
           'T58,"(counts)",T69, "#",T77,"of day)")'
;
page_print, outprint, count, lu, format=format1
page_print, outprint, count, lu, format=format2
page_print, outprint, count, lu, format=format3
page_print, outprint, count, lu, ' '
;
; EAC 5/96, add 6 spaces on the end for html </pre> will print blanks otherwise
formata = '(1X, I5, T9,A8, T19,A7, T29,A7, T38,I6, T47,I6, T56,I9,T67,I6'+$
		',T77,I6)'   
formatb = '(1X, I5, T9,A8, T19,A7, T29,A7, T38,I6, T47,E8.2, T58, E8.2,T67,I6'+$
		',T77,I6)'   
formatc = '(1X, I5, T9,A8, T19,A7, T29,A7, T38,I6, T47,I6, T58,"Unknown",'+ $
		'T67,I6,T77,I6)'    
formatd = '(1X, I5, T9,A8, T19,A7,T29,A7,T38,I6,T47,"Unknown",T58,"Unknown",'+ $
		'T67,I6,T77,I6)'    
formate = '(1X, I5, T9,A8, T19,A7, T29,A7,T38,I6,T47,"Unknown",T56,I6,'+ $
		'T67,I6,T77,I6)'    
;

flares = fldata(good).flare_num
stimes = atime(fldata(good).start_secs,/hxr)
ptimes = atime(fldata(good).peak_secs,/hxr)
durs = fldata(good).duration
prates = fldata(good).peak_rate
totcounts= fldata(good).total_counts   
burstnum = fldata(good).burst_num
trigs = fldata(good).burst_trig_secs

for i=0,nselected-1 do begin

; if htmlout is set add the html stuff before data

     if keyword_set(htmlout) then begin
         if htmlout eq 1 then htmlstring= $   ;GIF and PS links
            beg_gif+strcompress(flares(i),/remove)+end_gif+'GIF</a> '+beg_ps+$
            strcompress(flares(i),/remove)+end_ps+'PS</a>'+pre
         page_print,outprint,count,lu,htmlstring
        	; print </pre> if you are doing GIF and PS else print </a>
         if htmlout eq 1 then tail=spre else tail='</a>'
     endif else tail=' '

     if totcounts(i) eq 1.e13 and prates(i) eq 1.e13 then begin
       format = formatd
       page_print, outprint, count, lu, abort = abort, format=format, $
          flares(i), $
          strmid (stimes(i), 0, 8), $
          strmid (stimes(i), 10, 7), $
          strmid (ptimes(i), 10, 7), $
          durs(i), burstnum(i), trigs(i)
	  goto,getout
     endif

     if prates(i) eq 1.e13 then begin
       format = formate
       page_print, outprint, count, lu, abort = abort, format=format, $
          flares(i), $
          strmid (stimes(i), 0, 8), $
          strmid (stimes(i), 10, 7), $
          strmid (ptimes(i), 10, 7), $
          durs(i), totcounts(i), burstnum(i), trigs(i) 
	  goto,getout
     endif

     if totcounts(i) eq 1.e13 then begin
       format = formatc
       page_print, outprint, count, lu, abort = abort, format=format, $
          flares(i), $
          strmid (stimes(i), 0, 8), $
          strmid (stimes(i), 10, 7), $
          strmid (ptimes(i), 10, 7), $
          durs(i), prates(i), burstnum(i),trigs(i)
	  goto,getout
     endif 

    if prates(i) ge 1.0e6 then begin
      format = formatb
      page_print, outprint, count, lu, abort = abort, format=format, $
          flares(i), $
          strmid (stimes(i), 0, 8), $
          strmid (stimes(i), 10, 7), $
          strmid (ptimes(i), 10, 7), $
          durs(i), prates(i), totcounts(i), burstnum(i), trigs(i) 
          goto,getout
    endif

; if none of the above occurred, we are left with formata
    format=formata
    page_print, outprint, count, lu, abort = abort, format=format, $
          flares(i), $
          strmid (stimes(i), 0, 8), $
          strmid (stimes(i), 10, 7), $
          strmid (ptimes(i), 10, 7), $
          durs(i), prates(i), totcounts(i), burstnum(i), trigs(i) 
getout:
   if abort then goto,done
endfor
;
done:
if keyword_set(htmlout) then page_print,outprint,count,lu,'</html>'
close,lu
free_lun,lu
;
return & end
