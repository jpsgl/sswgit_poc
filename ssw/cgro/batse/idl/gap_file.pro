;+
; PROJECT:
;	SDAC
; NAME: 
;       GAP_FILE
;
; PURPOSE:
; GAP_FILE created and updates a file containing all times when BATSE
; was collecting data, i.e. the file is a record of data gaps (but it is
; good intervals that are recorded, not the gap intervals).  The gap 
; file has the following format:
;    header record - bytes 0-7:   latest time in seconds since 79/1/1,0
;                    bytes 8-11:  spare
;                    bytes 12-15: number of data records following header
;    data records  - bytes 0-7:   start time of observing interval
;                    bytes 8-15:  end time of observing interval
; GAP_FILE reads the header record to determine start time, then opens
; each BDB file from that day up to and including the end day specified 
; in the calling argument, recording time intervals for which there is data.
; Gaps of less than one minute are ignored.
;
; This gap file must be updated in time order.
; Can be killed anytime, and the good intervals from the last bdb file 
; it finished will be recorded (after each bdb file, it writes the good 
; intervals, and rewrites the header record).
;
;
; CATEGORY:
;       BATSE, CATALOG, DATABASE
;
; CALLING SEQUENCE:
;
;
; CALLS:
;	none
;
; INPUTS:
;       none explicit, only through commons;
;
; OPTIONAL INPUTS:
;	none
;
; OUTPUTS:
;       none explicit, only through commons;
;
; OPTIONAL OUTPUTS:
;	none
;
; KEYWORDS:
;	none
; COMMON BLOCKS:
;	none
;
; SIDE EFFECTS:
;	none
;
; RESTRICTIONS:
;	9-dec-1996, can only be used on SDAC to write new gap file
;
; PROCEDURE:
;	none
;
; MODIFICATION HISTORY:
; Kim Tolbert  5/18/92
; RAS, 9-dec-1996, gap_file.dat converted to stream file for unix compatibility
; Version 3, richard.schwartz@gsfc.nasa.gov, 22-oct-1997.
;	now uses read_dd to read the bdb file eliminating common block
;	sec_array formerly in gap_file and gap_finder
; Version 4, richard.schwartz@gsfc.nasa.gov, 23-oct-1997.
;	subtract 10 seconds from last 10 to be sure we're not crossing
;	two day boundaries.
; Version 5, richard.schwartz@gsfc.nasa.gov, 24-jun-1998, forced old VAX float format on write.
;-
pro gap_file, endday
;
;
header = {header_str, latest_time: 0.d0, spare: 0L, numrec:0L}
;
;
;
print,'Requested end time: ', endday
; If the catalog doesn't exist (count=0) open a new file.
;
; Either open a new file, or if file exists, open it and read header to get
; latest time in file.
;
gapfile = concat_dir((chklog('BATSE_DATA'))(0),'gap_file.dat')
fileexist = findfile (gapfile, count=count)
if count eq 0 then begin
   print, 'Opening new output file ', gapfile
   openw, lunout, gapfile, /stream, /get_lun
   header.latest_time = utime('92/3/11')
   header.numrec = 1
   writeu, lunout, use_vax_float(/new2old, header)
   writeu, lunout, use_vax_float(/new2old,utime('91/4/19')), use_vax_float(/new2old,utime('92/3/11'))
endif else begin
   if os_family() eq 'vms' then spawn, 'copy/lo ' + gapfile + ' ' + gapfile else begin
	;
	; Developmental code for RAS.
	;
	spawn,'cp '+gapfile+ ' ./'
	gapfile = 'gap_file.dat'
	endelse
   print,'Opening existing gap file ',gapfile
   openr, lunout, gapfile,  /get_lun
   readu, lunout, header
   header = conv_vax_unix( use_vax_float(/old2new, header) )
   free_lun, lunout
   openu, lunout, gapfile,/get,  /append
endelse
;
;h = assoc (lunout, header)

; time will be the current time we're on.  Initialized to start of the day after 
; latest complete day in file, then incremented by one day after we finish each
; BDB file.
;
; RAS, Get the day of the last completed bdb daily file. Subtract several seconds to be sure the last
; record doesn't contain a time from the following day.  Add 1 day.
;
time = anytim( header.latest_time - 20.0, /date,/sec ) + 86400.0
print,'Last time in current gap file = ', anytim(header.latest_time,/vms,/trunc)
print,'First time to process =', anytim(time,/vms,/date)
;
; Do this loop until we are past the end time 
while time lt utime(endday)+86400.d0 do begin
   print, ' '
   bdbfile = (concat_dir('BATSE_BDB',time2file( time, /year2digit, /date_only) +'.bdb'))(0)
   ; gap_finder returns the good intervals from the bdb file

   gap_finder, bdbfile, fileend, goodtimes, kgood 

   ; If found good intervals, write them and update the latest time for
   ; header.  If not, still update latest time.
   if kgood gt 0 then begin
      writeu, lunout, use_vax_float(/new2old,goodtimes(*,0:kgood-1))
      header.numrec = header.numrec + kgood
      header.latest_time = goodtimes(1,kgood-1)
   endif else header.latest_time = time+86400.d0
   print, 'Last time = ',anytim(header.latest_time,/vms,/trunc), $
          '  # good intervals in file = ',header.numrec
   ; Write header with updated latest time and number of records.
   point_lun, lunout, 0
   ;h(0) = header
   header1 = use_vax_float(/new2old, header)
   writeu, lunout,use_vax_float(/new2old, header)
   free_lun, lunout
   openu, lunout, gapfile,/get, /append  ; reopen positioned at end of file
   time = time + 86400.d0
endwhile
free_lun, lunout
goto,getout
;
getout:

end
