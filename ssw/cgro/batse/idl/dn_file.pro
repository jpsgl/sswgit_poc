;+
; Project:
;	SDAC
; Name:
;       DN_FILE

; Purpose:
; DN_FILE creates and updates a file containing all times when BATSE
; was in spacecraft day. The dn file has the following format:
;    header record - bytes 0-7:   latest time in seconds since 79/1/1,0
;                    bytes 8-11:  spare
;                    bytes 12-15: number of data records following header
;    data records  - bytes 0-7:   start time of spacecraft day 
;                    bytes 8-15:  end time of spacecraft day 

; Procedure:
; DN_FILE first gets an array of all the files available on BATSE_EPHEM
; for the requested year.  It then orders these by the day-of-year given in 
; the extension of the file names.  The files overlap, so in order to be sure
; we get the data from the latest file, we always have two files read, and
; for time intervals that overlap, only use the latest version.  We write
; any intervals greater than the latest end time written in the file, and
; update the header to reflect the current latest time and total number of
; data records in the file.
;
; This day/night file must be updated in time order.
; Can be killed anytime, and the good intervals from the last ephem file
; it finished will be recorded (after each input file, it writes the good
; intervals, and rewrites the header record).
;
; Kim Tolbert  5/18/92
;
; Modified 94/8/17 by Amy Skowronek - uses concat dir with disknames to
; accomodate UNIX.
; Mod. 95/2/2 by AES to search on "*-year" in stead of "*year" so we'll
; be less likely to get bogus files in the findfile - like 1994.dir
; Mod. 97/1/22 by RCJ to order files correctly. Problem when year changed.
;		Also made open files as stream.
; Version 5, richard.schwartz@gsfc.nasa.gov, 24-jun-1998, forced old VAX float format on write.
; Version 6, eva@kano.nascom.nasa.gov, 23-oct-1998, added old VAX conversion onto procedure that
;						    writes day/night intervals.
;						    (had been omitted in Version 5)
;-
pro dn_file, year=year

header = {header_str, latest_time: 0.d0, spare: 0L, numrec:0L}
;
;print,'Requested start/end times: ', startday, '  ', endday
; If the catalog doesn't exist (count=0) open a new file.
;
dnfile = concat_dir((chklog('BATSE_DATA'))(0),'dn_file.dat')
fileexist = findfile (dnfile, count=count)
if count eq 0 then begin
   print, 'Opening new output file ', dnfile
   openw, lunout, dnfile, /stream, /get_lun
   header.latest_time = utime('91/4/18')
   header.numrec = 0
   writeu, lunout, use_vax_float(/new2old, header)
endif else begin
   spawn, 'copy/lo ' + dnfile + ' ' + dnfile
   print,'Opening existing dn file ',dnfile
   openr, lunout, dnfile, /stream, /get_lun
   readu, lunout, header
   header = conv_vax_unix( use_vax_float( /old2new, header ))
   print,'Latest night start time in file = ', atime(header.latest_time,/hxr), $
         ' #rec=',header.numrec
   close, lunout
   openu, lunout, dnfile, /stream, /append
endelse
;
;h = assoc (lunout, header)  ; akt 9/24/93


; get all ephemeris files for this year in array files
files1 = findfile (concat_dir((chklog('BATSE_EPHEM'))(0),'*-'+year+'.*'),count=kfile)        
print,'Number of files = ',kfile
; sort on the filename extension which give the days contained in the file
break_file, files1, disk, dir, filnam,ext
q = sort(ext)
; ************
;files = files(q)    unfortunately, things are not this simple!
;----This piece of code orders the files correctly. Big problem
;    when go from one year to the next!
ext=ext(q) & files1=files1(q)
parts=strarr(2,n_elements(ext)) & sub=intarr(n_elements(ext)) & files=strarr(n_elements(files1))
ext=strmid(ext,1,7)  ; don't need '.'
for i=0,n_elements(ext)-1 do begin
   parts(*,i)=str_sep(ext(i),'-')
   sub(i)=fix(parts(1,i))-fix(parts(0,i))
endfor
q_good=where(sub ge 0,count) & q_bad=where(sub lt 0)
for i=0,n_elements(q_good)-1 do files(i)=files1(q_good(i))
if q_bad(0) eq -1 then goto,no_bad_files
for i=count,n_elements(files)-1 do files(i)=files1(q_bad(i-count))
;----End. This should fix the problem!
; **************
no_bad_files:
read_ephem, files(0), days, ndays, nites, nnites
for ifile = 1,kfile-1 do begin
   ; read next ephem file, so that we get latest version of any overlapping
   ; times
   read_ephem, files(ifile), days2, ndays2, nites2, nnites2
   print,'current file: ',ndays,nnites,' next file: ', ndays2, nnites2
   q=where ((days lt days2(0)-1800.d0) and (nites gt header.latest_time))
   if q(0) eq -1 then begin
      print,'No times fall between latest time in d/n file and first ' + $
            'time in next file.'
      goto,nextfile
   endif
   nrec = n_elements(q)
   print,'Number of elements to use: ', nrec
   intervals = transpose (reform ([days(q),nites(q)], nrec, 2))
   if (intervals(1,0) - header.latest_time gt 7200.d0) then goto,error
;   writeu, lunout, intervals
   writeu, lunout, use_vax_float(/new2old, intervals)	; 23-oct-98 - eva 
   header.latest_time = intervals(1,nrec-1)
   header.numrec = header.numrec + nrec
   writeheader:
   point_lun, lunout, 0
   writeu, lunout, use_vax_float(/new2old, header)
   ;h(0) = header   ; akt 9/24/93
   close, lunout
   for i=0,nrec-1 do print, atime(intervals(0,i),/hxr), ' ', $
      atime(intervals(1,i),/hxr)
   print,'New latest time = ', atime(header.latest_time,/hxr), $
         '  # records=', header.numrec
   openu, lunout, dnfile, /stream, /append
   nextfile:
   days = days2 & ndays = ndays2 & nites = nites2 & nnites = nnites2
   if ifile eq kfile-1 then begin
      print,'On last file.'
      q=where(nites gt header.latest_time)
      ;if q(0) eq -1 then stop
      if q(0) eq -1 then goto,writeheader
      nrec = n_elements(q)
      intervals = transpose (reform ([days(q),nites(q)], nrec, 2))
      writeu, lunout, use_vax_float(/new2old, intervals)
      header.latest_time = intervals(1,nrec-1)
      header.numrec = header.numrec + nrec
      point_lun, lunout, 0
      writeu, lunout, use_vax_float(/new2old, header)
      ;h(0) = header    ; akt 9/24/93
      close, lunout & free_lun, lunout
      for i=0,nrec-1 do print,atime(intervals(0,i),/hxr), ' ', $
         atime(intervals(1,i),/hxr)
      print,'New latest time = ', atime(header.latest_time,/hxr), $
            '  # records=', header.numrec
   endif
endfor

goto,getout

error:
print,'Error. New night start = ',atime(intervals(1,0),/hxr)
print,'       Old night start = ',atime(header.latest_time,/hxr)
stop
goto,getout

getout:
close, lunout & free_lun, lunout
end
