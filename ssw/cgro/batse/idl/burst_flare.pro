;+
; PROJECT:
;		SDAC
; NAME:		
;		BURST_FLARE
;
; PURPOSE:	Function to find BATSE bursts corresponding to a
;		BATSE flare number, or vice versa.
;
; CALLING SEQUENCE:  result = burst_flare(event, [/flare, /burst)]
;
; INPUTS: 
;		EVENT - Either the burst number or the flare number
;		depending on whether /burst or /flare is set. Default
;		if neither /burst or /flare is set is flare number.
;
; OUTPUTS:	
;		RESULT - Corresponding burst or flare numbers
;
; OPTIONAL KEYWORD INPUTS:
;		FLARE - If set, then input is flare number.  Find
;		corresponding bursts.
;		BURST - If set, then input is burst number.  Find
;		corresponding flares.
;
; SAMPLE CALL:  bursts = burst_flare (761)
;		flares = burst_flare (525, /burst)
;
; MODIFICATION HISTORY:
;		Written  Kim Tolbert 93/11/23
;               mod, ras, 27-apr-95, truncate at end of real list
;		mod, ras, 27-feb-96, make consistent with merge bursts
;		by expanding time window to cover later starting flares
;		due to telemetry coverage
;		Version 3 ras, 27 mar 1996, expand coverage in both directions
;		Version 4 ras, 7-apr-1997 fixed up parsing
;
;-        


function burst_flare, event, burst=burst, flare=flare
common burst_flare_com, b_num_arr, b_times
;
@log_hdr_struct
@flare_catalog

; Read BATSE flare catalog into common flare_catalog
batse_read_cat


checkvar, quiet, !quiet

if n_elements(b_num_arr) eq 0 then begin

	checkvar, filename, (concat_dir(chklog('BATSE_DATA'),'burst_trigger.lst'))(0)

; Read burst trigger list into string array s.
	read_seqfile, s, filename



; s now contains all lines read from file.  We want s to contain only
; the first line of data (some entries take two lines).
; Find first line of data in file (we know it is burst no. 146)
	q = where (strmid(s,0,7) eq '    146')
; Get rid of header info.
	s = s(q(0):*)
; Get rid of completely blank lines
	notblank = where(s ne '')
	s = s(notblank)
; Get rid of second line for an entry (burst entry will be blank)
	data_lines = where(strmid(s,0,7) ne '       ')
	s = s(data_lines)
;look in the last fifty lines for the end
	ns = n_elements(s)
	b50 = byte(strmid(strcompress(s(ns-50:*),/remove),0,1))
	wafter = where(b50 lt 48 or b50 gt 57, nafter)
	if nafter ge 1 then s = s(0: ns-50+wafter(0)-1)
;Extract appropriate columns to get burst numbers and times
	s_num_arr = strcompress(/rem,strmid(s,0,7))
	wdo_em = where(s_num_arr ne '', ndo_em)
	if ndo_em ge 1 then    begin
		b_num_arr = fix(s_num_arr(wdo_em))
		b_times = (fix(strmid(s(wdo_em),8,5))-3874)*86400.d0 + $
			long(strmid(s(wdo_em),14,5))
		s_num_arr=0
	endif
endif


fl_end_secs = fldata.start_secs + fldata.duration ; Flare end times

if keyword_set(burst) then begin
   q = where (event eq b_num_arr, count)
   if count eq 0 then begin
      print, 'Burst ', event, ' not in burst list.'
      goto, error_exit
   endif
   ind = q(0)
   burst_time = b_times(ind)

   ;Allow up to a 5 minute delay in the start of the flare
   ;This gap is caused by the difference in memory and real time telemetry

   q = where ((burst_time gt fldata.start_secs -300) and $
              (burst_time le fl_end_secs), count)
   if count eq 0 then goto, error_exit
   result = fldata(q).flare_num
endif else begin
   q = where (event eq fldata.flare_num, count)
   if count eq 0 then begin
      print, 'Flare ', event, ' not in flare catalog.'
      goto, error_exit
   endif
   ind = q(0)
   flare_stime = fldata(ind).start_secs
   flare_etime = fl_end_secs(ind)
   q = where ((flare_stime -300 le b_times) and $
              (flare_etime ge b_times), count)
   if count eq 0 then goto, error_exit
   result = b_num_arr(q)
endelse
goto, getout

error_exit:
result = 0

getout:
return,result & end
