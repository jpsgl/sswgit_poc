function ssw_hcr_newapi_expand,_extra=_extra, no_query=no_query, rtags=rtags
;
;   Name: ssw_hcr_newapi_expand
;
;   Purpose: expand ranges for HCR events3 API (xcen,ycen,fovx,fovy,radius) & other newapi tweaks
;
;   Keyword Parameters:
;      _extra - inheritance _EXTRA from caller; expand ranges, handle verbatim new api stuff...
;               OBSDESC='HOP NNN', RADIUS='850~1200', GOES_ABOVE='M' ... stuff like that
;     no_query (switch) - if set, return array of <param>=<value> strings - default is cgi query-ready
;     rtags (output) - vector of _EXTRA tag#s which were identified by this routine - caller may REM_TAG those
;     valid='xcen,ycen,radius,fovx,fovy,limit' 
;     verbatim='herevents,iris_prep_version,full_obsid,obsshort,obsdesc'
;     full_obsid - comma delimited list of expicit yyyymmdd_hhmmss_<obsid> 
;     struct (switch) - return structure vector of subset of info - defalut=LIST with all info
;
;   Calling Sequence:
;      IDL> hcr=iris_time2hcr(t0,t1 [,options] [,/struct] [,/expand_eventid] [,LIMIT=#] )
;
;   Calling Examples:
;      IDL> hcr=iris_time2hcr('1-jul-2013',reltime(/now),goes_above='M',/struct,/expand_eventid) ; + Mflares for mission
;      IDL> hcr=iris_time2hcr('1-oct-2017','1-dec-2017',obsdesc='hop 307',/struct)
;      IDL> hcr=iris_time2hcr('1-jan-2014','1-jan2015',obsdesc='Hinode',/struct,/expand_eventid,limit=2000) ; SOT overlap
;      IDL> hcr=iris_time2hcr('1-jan-2016','15-mar-2016',radius='900~1200',/struct) ; limb stuff
;
;   History:
;      24-nov-2014 - S.L.Freeland - ssw_hcr_make_query helper, for new API extensions
;      21-mar-2017 - S.L.Freeland - inherit single valued params
;      15-nov-2017 - S.L.Freeland - fix hiccup for multiple valid _EXTRA <param>=<value> 
;      16-nov-2017 - S.L.Freeland - add OBSDESC to verbatim list, some documentation
;-

valid=strupcase(str2arr('xcen,ycen,radius,fovx,fovy,limit')) ; add to this list 
verbatim=strupcase(str2arr('herevents,iris_prep_version,full_obsid,obsshort,obsdesc'))
nt=n_tags(_extra)
tn=tag_names(_extra)
rtags=[]
retval=''
for t=0,nt-1 do begin 
  if is_member(tn[t],valid) then begin 
     tdata=_extra.(t)
     case 1 of 
        data_chk(tdata,/string): if strpos(tdata,'~') ne -1 then tdata=fix(str2cols(tdata,'~',/trim))
        else:
     endcase
     if n_elements(tdata) eq 2 then begin ; valid range
        thing=tn[t]
        if tn[t] eq 'RADIUS' then thing='HPCRADIUS' ; special case w/non-obvious PARAM:NEWAPI name
        box_message,thing
        retval=[retval,'min'+ thing + '=' + strtrim(tdata[0],2),'max'+thing + '=' + strtrim(tdata[1],2)]
        rtags=[rtags,t]
     endif else begin
        retval=[retval,tn[t]+'='+strtrim(tdata[0],2)]
        rtags=[rtags,t]
     endelse
  endif else begin 
     if is_member(tn[t],verbatim) then begin
        retval=[retval,tn[t]+'='+arr2str(strtrim(_extra.(t),2))]
        rtags=[rtags,t]
     endif
  endelse
endfor 

if n_elements(retval) gt 1 then begin
   retval=retval[1:*]
   if not keyword_set(no_query) then begin
      retval=arr2str(retval,'&')
      synlist=strupcase(str2arr('full_obsid'))
      tranlist=strupcase(str2arr('obsshort'))
      for s=0,n_elements(synlist)-1 do $ 
         if strpos(retval,synlist[s]) ne -1 then retval=str_replace(retval,synlist[s],tranlist[s])
   endif    
endif

if get_logenv('check_newapi') ne '' then stop,'retval,tn,t'
return,retval
end
