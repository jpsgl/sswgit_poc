function ssw_hpkb_refs2struct, reftag
;
;+
;   Name: ssw_hpkb_refs2struct
;
;   Purpose: HPKB 'refs' tag(s) -> structure 
;
;   Input Paramters:
;      reftag - .REFS from original ssw_hpkb_query

nrefs=n_elements(reftag)
ev=struct4event('FL') ; for single point array dimensions
tn='reference_'+str2arr('names,links,types')
retval=str_subset(ev,tn) 
if n_elements(reftag) gt 1 then retval=replicate(retval,nrefs)
tab=str_replace(reftag,'{"','{,","') 
tab=str_replace(tab,'"}','","}')
ss=where_pattern(tab,'","')
dss=deriv_arr(ss)

bref=byte(reftag)
ssq=where_pattern(reftag,'"',count)

words=strarr(count)
for i=0,count-1,2 do begin 
   if ssq(i+1)-ssq(i) gt 1 then $
      words(i) =string(bref(ssq(i)+1:ssq(i+1)-1))
endfor
ss=indgen(count/2)*2
words=words(ss)
nx=data_chk(bref,/nx)
which=ssq/nx

nwords=n_elements(words)
if nwords mod 3 ne 0 then begin 
    box_message,'Unexpected word count...'
    retval=-1
endif else begin 
    nret=nwords/3
    retval=replicate({name:'',type:'',link:''},10)
    temp=retval(0:nret-1)
    reads,words,temp
    retval(0)=temp
endelse

return,retval
end
