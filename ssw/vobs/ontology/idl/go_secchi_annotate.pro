
pro go_secchi_annotate, t0, t1, wavelnth=wavelnth, $
  index_a=index_a, data_a=data_a, index_b=index_b, data_b=data_b

if ( (not exist(index_a)) or (not exist(index_b)) or $
     (not exist(data_a)) or (not exist(data_b)) ) then begin
  if not exist(t0) or not exist(t1) then begin
    t0 = '28-mar-2008'
    t1 = '28-mar-2008 02:00'
  endif
  if not exist(wavelnth) then wavelnth = 171

  secchi_stereo_pairs, anytim(t0, /ccsds), anytim(t1, /ccsds), files_b, files_a, wavelnth=wavelnth

  mreadfits, files_a(0), index_a, data_a
  mreadfits, files_b(0), index_b, data_b

;  map_a = mk_secchi_map(index_a, data_a)
;  map_b = mk_secchi_map(index_b, data_b)
endif

ssw_stereo_fov, index0=index_a, data0=data_a, index1=index_b, data1=data_b, smap0=smap_a, smap1=smap_b, /read_out, $
  wnum_arr=[0,1,2,3], xsiz_win=1024, /no_grid
STOP
ssw_hek_annotator, map=smap_a, data=data_a
ssw_hek_annotator, map=smap_b, data=data_b

end
