function ssw_hpkb_make_query, t0, t1, _extra=_extra, $
   search_array=search_array, $
   coordsys=coordsys, event_type=event_type, full_fov=full_fov, $
   hpc=hpc, hra=hra, hgs=hgs, hgc=hgc, debug=debug, $
   day_window=day_window, hour_window=hour_window, minute_window=minute_window
;
;+
;   Name: ssw_hpkb_make_query  *****NOTE: - Use ssw_her_make_query.pro instead
;
;   Purpose: form HPKB query string from users input/keywords
;
;   Input Parameters:
;      t0 - start time of range or time vector - may be ssw 'index' record(s)
;      t1 - optional stop time of range (assumes t0 is scalar)
;
;   Keyword Parameters:
;      event_type - type of desired event (see struct4event(/list)); *=all
;      (alternate to event_type is via inherit; ex: /AR,/CE,/CH... etc )
;      _extra - inherit -> 
;     coordsys - optional coordsys - default=HPC (helio projective)
;     day_window, hour_window, minute_window - optional time window
;     (per: time_window.pro)
;     full_fov=full_fov - consider full FOV (implied by event_type)
;     search_array - optional array of additional search critera.. for
;        example: search_array=['fl_goescls>=X',...]
;        (equalities can use keyword inherit: p1=v1, p2=v2.. - 
;        search_array elements may include verbtim <param><relation><value>
; 
;         
;   Calling Sequence:
;      hpkbq=ssw_hpkb_make_query(t0,t1,/ar,/fl) ; AR&FL between t0&t2
;      hpkbq=ssw_hpkb_make_query(index,day_window=[-1,2] ) ; indexT -1/+2 days   
;      hpkbq=ssw_hpkb_make_query(t0,t1,PARAM=VALUE) ; keyword inherit (= only)
;      hpkbq=ssw_hpkb_make_query(t0,t1,search_array=['<p1><op1><val1',
;        '<p2><op2><val2>',..<pn><opn><valn>'] ; opt array of search criteria
;      hpkbq=ssw_hpkb_make_query('1-JAN-2004','1-JAN-2005', /FL, $
;         SEARCH=['FL_GOESCL>=M5','FRM_IDENTIFIER=SolarSoft'])
;
;   History:
;      Circa 1-aug-2008 - S.L.Freeland Written
;      27-jan-2009 - S.L.Freeland - url_encode the SEARCH_ARRAY values/operators
;       6-May-2010 - S.L.Freeland - suite/naming rationalization - 
;                                   This routine depracated/replcated by ssw_her_make_query.pro
;-

common ssw_hpkb_make_query_blk, etypes

debug=keyword_set(debug)

case 1 of 
   required_tags(_extra,'event_starttime,event_endtime'): begin 
      time_window,[_extra.event_starttime,_extra_endtime], t0x, t1x, $
         minute=minute_window, days=day_window,hours=hour_window, /ccsds
      if n_tags(_extra) eq 2 then delvarx,_extra else $
         _extra=str_subset(_extra,'event_starttime,event_endtime',/exclude)
   endcase   
   n_params() eq 0: begin 
      box_message,'Need at least one time and/or "index" record'
      return,''
   endcase
   data_chk(t0,/struct): begin 
      t0in=t0
      if required_tags(t0,'date_obs') then t0in=t0.date_obs
      time_window,t0in,t0x,t1x,minute=minute_window, days=day_window, $
         hours=hour_window, /ccsds
   endcase
   else: begin 
      if n_params() eq 1 then t1=t0
      time_window,[anytim(t0,/ecs),anytim(t1,/ecs)], t0x, t1x, $
         minute=minute_window, days=day_window,hours=hour_window, /ccsds
   endcase
endcase

t0x=anytim(t0x,/ccsds,/truncate)
t1x=anytim(t1x,/ccsds,/truncate)
query='event_starttime='+t0x+'&event_endtime='+t1x

asys=['HPC','HRA','HGS','HGC']
nsys=['helioprojective','helioradial','heliographicstoneyhurs','heliographiccarrington']
case 1 of 
   required_tags(_extra,/event_coordsys): csys=_extra.event_coordsys
   else:begin
     for i=0,n_elements(asys)-1 do $
        estat=execute('if keyword_set('+asys(i)+') ne "" then csys=asys(i)')
      if n_elements(csys) eq 0 then csys='HPC'
      query=query+'&event_coordsys='+ $
         nsys(where(csys eq asys)<0)  ; !!! force HPC
   endcase
endcase

if 1-required_tags(_extra,'x1,x2,y1,y2') then begin 
   case 1 of 
      keyword_set(full_fov): begin 
         x1=-5000 & x2=5000 & y1=-5000 & y2=5000
      endcase
      data_chk(t0,/struct): index2fov,t0,x1,x2,y1,y2,/extreme
      else: begin
         x1=-5000 & x2=5000 & y1=-5000 & y2=5000
      endcase 
   endcase
   query=query+'&x1='+strtrim(x1,2)+'&x2='+strtrim(x2,2) + $
               '&y1='+strtrim(y1,2)+'&y2='+strtrim(y2,2)
endif

if n_elements(event_type) eq 0 then begin 
   if n_elements(etypes) eq  0  then begin 
      evlist=strtrim(struct4event(/list),2)
      etypes=evlist(where(strlen(evlist) eq 2))
   endif
   if data_chk(_extra,/struct) then begin 
      tchk=tag_index(_extra,etypes)
      tss=where(tchk ne -1,ecnt)
      if ecnt eq 0 then etype='**' else begin
         etype=arr2str(etypes(tss))
         if n_tags(_extra) eq ecnt then delvarx,_extra else $
           _extra=str_subset(_extra,etype,/exclude)
      endelse
   endif else etype='**' ; all
event_type=etype
endif 

if 1-required_tags(_extra,'event_type') then $
   query=query+'&event_type='+ strtrim(event_type(0),2)

; if not explicitly set via keyword (inherit), set some required query params
if 1-required_tags(_extra,'cosec') then $
   query=query+'&cosec=1

if 1-required_tags(_extra,'type') then $
   query=query+'&type=column'

if 1-required_tags(_extra,'cmd') then $
   query=query+'&cmd=search'

if data_chk(_extra,/struct) then begin 
   tn=strlowcase(tag_names(_extra))
   for i=0,n_tags(_extra)-1 do begin 
      query=query+'&'+tn(i)+'='+strtrim(_extra.(i),2)
   endfor
endif

if debug then stop,'search_array'
if data_chk(search_array,/string) then begin 
   log_op=  ['>=','<=','<>' ,'==','<', '>','=', 'like'] ; valid operators
   nop=n_elements(log_op)
   for i=0,n_elements(search_array)-1 do begin
      oo=0
      val=(ssw_strsplit(search_array(i),log_op(0),/tail,head=param))(0)
      param=param(0)
      while(param eq '' and oo lt nop) do begin 
         oo=oo+1
         val=(ssw_strsplit(search_array(i),log_op(oo),/tail,head=param))(0)
      endwhile
      if val ne '' and param ne '' then begin ; valid triplet
         si=strtrim(i,2)
         query=query+'&param'+ si +'='+param + $
                     '&op'   + si +'='+log_op(oo) + $
                     '&value'+ si +'='+url_encode(val) 
      endif 
   endfor
endif

; query=url_encode(query)

if debug then stop,'before return'
return,query
end
