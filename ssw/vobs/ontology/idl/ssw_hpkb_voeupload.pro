pro ssw_hpkb_voeupload, voe, _extra=_extra, $
   post=post, curl=curl, old_paradigm=old_paradigm, force_login=force_login, $
  uploadurl=uploadurl, status=status, lstatus=lstatus, login_only=login_only, istatus=istatus, $
  revise=revise
;+
;   Name: ssw_hpkb_voeupload
;
;   Purpose: upload one or more files/urls -> HPKB
;
;   Input Parameters:
;      voe - one or more VOEvents - local nfs paths or urls
;
;   Keyword Parameters:
;      curl (switch) - if set, use curl (local VOE assumed)
;      post (switch) - if set, use cgi POST (local or URL)
;      old_paradigm (switch) - if set, attempt 'old' authentication 
;      force_login (switch) - if set, force login (override deltaT check)
;      status (output) - 1 if OK, 0 if Problem
;      revise (switch) - if set, this is a revision, not a new entry
;   
;   Restrictions:
;      /POST not implemented until tommorrow 
;
;   History:
;      1-Jul-2008 - (circa) - S.L.Freeland - ssw piplines -> HPKB
;      3-nov-2008 - S.L.Freeland - update upload URL
;     25-may-2009 - S.L.Freeland - new authentication paradigm w/https login
;     28-may-2009 - S.L.Freeland - avoid "too many" logins via deltT check
;      3-dec-2009 - S.L.Freeland - add STATUS output & check for success
;     19-dec-2009 - S.L.Freeland - add LSTATUS (login status) - 1=ok
;     14-may-2010 - S.L.Freeland - change default upload url
;     17-jun-2010 - S.L.Freeland - add /REVISE keyword & function
;
;-
;
common ssw_voeupload_blk, last_spawn ; time of last succesful login or upload
status=0
lstatus=0
fexist=file_exist(voe)
old_paradigm=keyword_set(old_paradigm)
new_paradigm=1-old_paradigm
logonly=keyword_set(login_only)
revise=keyword_set(revise)

local=where(fexist,ecnt)
if ecnt eq 0 and (1-logonly) then begin 
   box_message,'One or more of your VOEs not found (local only for now)'
   box_message,'Aborting...
   return
endif
lfiles=voe(local)
nf=n_elements(lfiles)

remote=where(1-fexist,rcnt)
acode='abcdefg' 

if n_elements(last_spawn) eq 0 or keyword_set(force_login) then $
         last_spawn=reltime(days=-1) ; force login

case 1 of 
   keyword_set(uploadurl): upurl=uploadurl 
   revise: upurl='https://www.lmsal.com/hek/her/voerevise' ; 17-jun-2010
   else: upurl='https://www.lmsal.com/hek/her/voeupload' ;  14-may-2010
endcase

status=0
if keyword_set(curl) then begin
   spawn,['which','curl'],chkcrl,/noshell
   if not file_exist(chkcrl(0)) then begin 
      box_message,'Cannot find curl..., aborting'
      return
   endif
   if new_paradigm then begin 
      if n_elements(user) eq 0 then user=get_user()
      acct=get_logenv('ssw_hpkb')
      if acct eq '' then begin 
         box_message,'You need to define environmental $ssw_hpkb
         return
      endif
      cookie_jar=concat_dir('$HOME','cookiejar.txt')
      login=ssw_deltat(last_spawn,reltime(/now),/minutes) gt 30 or logonly 
      if login then begin 
         box_message,'Issuing secure login command..'
         lcmd='curl -k -c '+ cookie_jar +' -d username='+user+  $

           ' -d password=' + acct + ' -d cmd=login -d cosec=1 ' + $
           ' https://www.lmsal.com/hek/her/heks' 
         spawn,lcmd,lstatus,lerror
         box_message,['Login Status',lstatus]
         box_message,['Login Command:',lcmd]
         sucss=where(strpos(lstatus,'success') ne -1,scnt)
         status = scnt gt 0
         lstatus=status
         if scnt eq 0 then begin
            box_message,'Login failed..' 
            return
         endif else box_message,'Login successful, proceding...'
         last_spawn=reltime(/now)
         if logonly then begin 
            box_message,'Login Only, returning...' ; !!! Unstructured Early Exit (
            return
         endif
      endif else print,'no login required'
      lstatus=1 
   
      cmds='curl -k -b '+ cookie_jar+' -F uploadedFile=@'+lfiles + $
           ' ' + upurl 
   endif else $
        cmds='curl -F uploadedFile=@'+lfiles + " -F accessCode=" + acode + $
      ' ' + upurl
   status=1
   for i=0,nf-1 do begin
      box_message,['Upload command:',cmds(i)]
      spawn,str2arr(cmds(i),' '),out,/noshell, istatus
      if out(0) ne '' then box_message,out
      status=status * strpos(arr2str(out),'Successfully') ne -1
      last_spawn=reltime(/now)
if get_logenv('hpkb_check') ne '' then stop,'out'
   endfor
endif else begin
   queries='uploadedFile='+lfiles+'&accessCode='+acode
   for i=0,nf-1 do begin
      ssw_post_query,upurl,queries(i),status=status
   endfor
endelse

return
end

