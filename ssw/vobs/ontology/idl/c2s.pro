
; (4-feb-91)
FUNCTION C2S,V0,YOFF=YOFF,ZOFF=ZOFF,ROLL=ROLL,B0=B0
;+
; NAME:
;	S2C
; PURPOSE:
;	Returns Cartesian coordinates (X,Y,Z) of a position vector
;	or array of position vectors whose spherical coordinates are
;	specified by V0.
; CALLING SEQUENCE:
;	V1 = C2S(V0,YOFF=YOFF,ZOFF=ZOFF,ROLL=ROLL,B0=B0)
; INPUTS:
;	V0 = Cartesian coordinates (x,y,z) of a 3-vector or
;	     array of 3-vectors.
; OPTIONAL INPUTS:
;       YOFF, ZOFF = Y AND Z TRANSLATIONS
;       ROLL
;       B0
; OUTPUTS:
;       V1 = 3-vector containing spherical coordinates (r,theta,phi)
;       corresponding to Cartesian coordinates specified in V0.  It is
;       a 3xn array if v0 is.
; MODIFICATION HISTORY:
;       Written, Jan, 1991, G. L. Slater, LPARL
;       GLS - Modified to allow translation, roll, and b0 corrections
;-

  if n_elements(yoff) eq 0 then yoff = 0
  if n_elements(zoff) eq 0 then zoff = 0
  if n_elements(roll) eq 0 then roll = 0
  if n_elements(b0) eq 0 then b0 = 0
  v0 = makarr(v0)
  x = makvec(v0(0,*)) & y = makvec(v0(1,*)) & z = makvec(v0(2,*))
  y = y - yoff & z = z - zoff
  cosroll = cos(-roll/!radeg)
  sinroll = sin(-roll/!radeg)
  x1 = x
  y1 = y*cosroll - z*sinroll
  z1 = y*sinroll + z*cosroll
  cosb0 = cos(-b0/!radeg)
  sinb0 = sin(-b0/!radeg)
  x2 = x1*cosb0 - z1*sinb0
  y2 = y1
  z2 = x1*sinb0 + z1*cosb0
  r = sqrt(x*x+y*y+z*z)
  theta = atan(z2/sqrt(x2*x2+y2*y2))*!radeg
  phi = atan(y2,x2)*!radeg

  return,transpose([[r],[theta],[phi]])

  end

