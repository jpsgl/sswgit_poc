function ssw_email_address, no_machine=no_machine
;
;+
;   Name: ssw_email_address
;
;   Purpose: return implied or expicit email address for current user
;
;   Input Parameters:
;      none
;
;   Output:
;      function returns email address
;;
;   Keyword Parameters:
;      no_machine - if set, eliminate 1st 'segment' from get_host() 
;
;   Method:
;      1st check '$ssw_email' and use if that exists - 
;      else return get_host()@get_user() ; 
;  
;   History:
;      28-feb-2010 - S.L.Freeland - 
;-

chkenv=get_logenv('ssw_email') ; user set?
if chkenv eq '' then begin 
   host=get_host() ;
   if os_family() eq 'unix' and strpos(host,'.')  eq -1 then begin 
      spawn,['hostname','-f'],thost,/noshell  ; try harder
      if strpos(thost,'.') ne -1 then host=thost
   endif
   cols=str2cols(host,'.',/trim)
   if n_elements(cols) eq 1 then begin
      box_message,["Warning, incomplete email?", $
        "Suggest IDL> set_logenv,'ssw_email','<youremailhere>'"]
   endif else begin 
      strtab2vect,cols,machine,domain
      no_machine=keyword_set(no_machine) or $
         is_member(domain,'lmsal')
      host=([host,str_replace(host,machine+'.')])(no_machine)
   endelse
   retval=get_user()+'@'+host
endif else retval=chkenv

return,retval
end
