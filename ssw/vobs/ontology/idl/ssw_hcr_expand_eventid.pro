function ssw_hcr_expand_eventid,hcr,add_tag=add_tag
;
;+
;   Name: ssw_hcr_expand_eventid
;
;   Purpose: expand hcr evenid/ivo -> url via ssw_hcr_make_query,EVENTID=<ivo>), optionally add EVENTID_URL
;
;   Input Parameters:
;      hcr - vector of one or more HCR structures, assumed .EVENTID exists (old or new HCR paradigm ok)
;
;   Output:
;      function returns expanded EVENTID_URL -OR- input HCR with appended .EVENTIDL_URL tag added
;
;   Keyword Parameters:
;      add_tag - (switch) - if set, return input HCR with expanded eventids in new tag .EVENTIDL_URL
;    
;   Calling Sequence:
;      IDL> eventurl=ssw_hcr_expand_eventid(HCR) ; hcr records -> eventidurls (1:1)
;      IDL> hcrexpand=ssw_hcr_expand_eventid(HCR,/ADD_TAG) ; input HCR w/ .EVENTID_URL tag added
;
;   History:
;      15-aug-2018 - S.L.Freeland - single point thing for old/new hcr eventid -> www-ready url
;
;-
retval=''
if not required_tags(hcr,/EVENTID) then begin 
   box_message,'Need input HCR record(s)'
endif else begin 
   if required_tags(hcr,/EVENTID_URL) then box_message,'tag .EVENTID_URL already Exists!' else begin 
      nhcr=n_elements(hcr)
      retval=strarr(nhcr)
      for i=0,nhcr-1 do retval[i]=ssw_hcr_make_query(eventid=hcr[i].eventid)
      if keyword_set(add_tag) then retval=add_tag(hcr,retval,'eventid_url')
   endelse
endelse
return,retval
end

