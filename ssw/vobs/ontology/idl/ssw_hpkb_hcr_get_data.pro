pro ssw_hpkb_hcr_get_data, t0, t1, interactive=interactive, instrument=instrument

if ~keyword_set(instrument) then instrument='trace'

hcr=ssw_hpkb_hcr_query(ssw_hpkb_hcr_make_query(t0,t1,instrument=instrument))

stop,'hcr'

return
end


