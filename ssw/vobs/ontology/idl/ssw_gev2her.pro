function ssw_gev2her, gev, found=found
;
retval=-1
time_window,[gev.fstart,gev.fstop],t0,t1,minutes=5
query=ssw_her_make_query(t0,t1,FRM_NAME="SSW Latest Events",/fl)
her=ssw_her_query(query)
sscnt=0
if data_chk(her,/struct) then begin 
   ss=where(ssw_deltat(her.fl.event_starttime,ref=gev.fstart) eq 0,sscnt)
   if sscnt gt 0 then retval=her.fl(ss(0))
endif 

found=sscnt gt 0

return,retval
end





