pro ssw_nnwaves_getinfo,nnstr,index, data, waves=waves,int=int,bdiff=bdiff,rdiff=rdiff, $
   mpegs=mpegs, javascripts=javascripts, thumbnails=thumbnails, micons=micons, $
   debug=debug
;
;+
;   Name: ssw_nnwaves_getinfo
;
;   Purpose: derive/return info associated with "Nariaki Wave Catalog" (output from ssw_nnwaves.pro)
;
;   Input Parameters:
;      nnstr - one or more structures from Nariaki Nittas EUV wave catalog (output from ssw_nwaves.pro)
;
;   Output Parameters:
;      index - if 2nd parameter supplied, returns imagelist associcate with First javascript movie
;      data  - if supplied, DATA (cube) from first javascript movie
;
;   Keyword Parameters:
;      waves - list of one or more waves (94,131,171,193,211,304,335,euvia,euvib)
;      int,bdiff,rdiff - "type" (intensity, base-difference, or running difference)
;      mpegs,javascripts,thumbnails,micons (OUTPUT) - derived urls associated with catalog parent (not checked for validity)
;
;   History:
;      18-aug-2014 - S.L.Freeland - ssw utility , helper for NN catalog -> HER VOE generation
;-
;

if not required_tags(nnstr,'date_obs,goes_class,location,noaa_ar') then begin 
   box_message,'Expect one or more structures from Nariaki Nitta wave catalog (output from ssw_nnwaves.pro)
   return
endif

if n_elements(waves) eq 0 then waves=193
 
debug=keyword_set(debug)

case 1 of
   keyword_set(rdiff): type='rdiff'
   keyword_set(bdiff): type='bdiff'
   else: type='int
endcase 
type=strupcase(type)

swaves=strtrim(waves,2)
if n_elements(swaves) eq 1 and strpos(swaves,',') ne -1 then swaves=str2arr(swaves)

tnames=tag_names(nnstr)
iwaves=str2number(swaves)

aia=where(iwaves ne 0 and iwaves ne 195,aiacnt)
stereo=where(iwaves eq 0 or iwaves eq 195,scnt)
ntags=''
if aiacnt gt 0 then ntags='AIA_'+string(iwaves[aia],format='(i4.4)')
if scnt gt 0 then begin 
   stags=strtrim(strupcase(swaves[stereo]),2)
   nsat=where(strpos(stags,'EUVI') eq -1,nscnt)
   if nscnt then stags[nsat]='EUVIA'+stags[nsat]
   stags=str_replace(stags,'195','')+'195'
   ntags=[ntags,stags]
endif
ntags=ntags+'_'+type

chk=tag_index(nnstr,ntags)
ss=where(chk ne -1, nn)
if nn eq 0 then begin
   box_message,'No valid waves found
   return
endif

ntags=ntags[ss]
tind=chk[ss]
tind=tind(sort(tind))
tind=all_vals(tind)

urls=strarr(n_elements(tind)*n_elements(nnstr))

pnt=0
for i=0,n_elements(tind)-1 do begin 
   utemp=nnstr.(tind[i])
   urls[pnt]=utemp
   pnt=pnt+n_elements(utemp)
endfor

uroots=ssw_strsplit(urls,'.html',/head)
javascripts=uroots+'_j.html'
thumbnails=uroots+'_j_mthumb.gif'
micons=uroots+'_j_micon.gif'

if n_params() gt 1 then begin 
   index=ssw_jsurl2imagelist(javascripts[0])
   if n_params() ge 3 then begin
      ssw_jsurl2data,javascripts[0],data,times=times,/get_times
   endif
endif

if debug then stop,'before return'

return
end

