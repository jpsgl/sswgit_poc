
pro mk_voe_obs, evstruct, buff=buff, outdir=outdir, write_file=write_file, $
   utility=utility, goal=goal, purpose=purpose

;+
;   Name:
;	mk_voe_obs
;   Purpose:
;	Create an OBSERVATION VOEvent XML file from catalog data.
;   Input Parameters:
;
;   Keyword Parameters:
;
;   Output:
;
;   Calling Sequence:
;
;   Calling Examples:
;
;   Procedure:
;	IVOA VOEvent 1.1 specification used as starting point:
;	http://www.ivoa.net/Documents/REC/VOE/VOEvent-20061101.html
;   History:
;	2007-02-27 (GLS) 
;       2010-06-21 - S.L.Freeland - add <Why> + Goal and Purpose keywords
;   Restrictions:                                                                     
;
;-                                                                                    

if not exist(outdir) then outdir = '/home/slater/soft/idl/ontology/protege/temp'
if not exist(outfil_voevent) then outfil_voevent = concat_dir(outdir,evstruct.EventID)

role=(['observation','utility'])(keyword_set(utility))

buff = '<?xml version="1.0" encoding="UTF-8" ?>'
buff = [buff,	'<VOEvent role="'+role+'" id="ivo://sot.lmsal.com/VOEvent#'+ evstruct.EventID + $
		'" version="1.1" xmlns="http://www.ivoa.net/xml/VOEvent/v1.1" ' + $
		'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' + $
		'xmlns:lmsal="http://sot.lmsal.com/solarb" ' + $
		'xsi:schemaLocation="http://www.ivoa.net/xml/VOEvent/v1.1 ' + $
		'http://www.ivoa.net/internal/IVOA/IvoaVOEvent/VOEvent-v1.1.xsd">']

buff = [buff, '']
buff = [buff, '']

; Add standard VOEvent XML 'Who' header appropriate to instrument:
buff = [buff, get_voevent_blorch(evstruct,/who)]

buff = [buff, '']
buff = [buff, '']

if n_elements(goal) gt 0 or n_elements(purpose) gt 0  then begin 
   if n_elements(purpose) eq 0 then purpose=''
   if n_elements(goal) eq 0 then goal=''
   buff=[buff,'     <Why>' , $
              '        <Concept>', $
              '             <lmsal:Goal>' + goal +'</lmsal:Goal>',$
              '             <lmsal:Purpose>' + purpose + '</lmsal:Purpose>',$
              '        </Concept>',$
              '     </Why>']
endif

; '<What>' portion:
buff = [buff, '    <What>']
buff = [buff, '        <!-- Data about what was measured/observed.  Some tags come from predicted event. -->']
buff = [buff, '        <Param name="URLParent" value="' + evstruct.url_parent + '" />']

nmovies = n_elements(evstruct.movie_info)
tags = tag_names(evstruct.movie_info)
ntags = n_elements(tags)
  for i=0, nmovies-1 do begin

    buff = [buff, '']

    buff = [buff, '        <Group name="' + evstruct.movie_info(i).wave +'">']
    for j=0,ntags-1 do begin
      value = evstruct.movie_info(i).(j)
      if size(value_buff,/type) eq 1 then value = fix(value)
      buff = [buff, '            <Param name="' + tags(j) + '" value="' + strtrim(value,2) + '" />']
    endfor

    buff = [buff, '        </Group>']
  endfor

buff = [buff, '']
buff = [buff, '    </What>']

buff = [buff, '']
buff = [buff, '']

buff = [buff, '    <WhereWhen>']
buff = [buff, '']
obs=gt_tagval(evstruct,/telescop,missing='Hinode')

buff = [buff, '        <ObservatoryLocation ID="' + obs + '" />']
buff = [buff, '']
buff = [buff, '        <ObservationLocation>']
buff = [buff, '            <lmsal:xCen>' +  strtrim(evstruct.movie_info(0).xcen,2) + '</lmsal:xCen>']
buff = [buff, '            <lmsal:yCen>' +  strtrim(evstruct.movie_info(0).ycen,2) + '</lmsal:yCen>']
buff = [buff, '            <lmsal:fovx>' +  strtrim(evstruct.movie_info(0).fovx,2) + '</lmsal:fovx>']
buff = [buff, '            <lmsal:fovy>' +  strtrim(evstruct.movie_info(0).fovy,2) + '</lmsal:fovy>']

; Adopted coord_system_id="UTC-HGS-TOPO"  from VOEvent 1.1 specification:
; http://www.ivoa.net/Documents/REC/VOE/VOEvent-20061101.html

coords = arcmin2hel(evstruct.movie_info(0).xcen, evstruct.movie_info(0).ycen, $
		    date=evstruct.movie_info(0).date_obs)
btmlt = [evstruct.movie_info(0).xcen - 0.5*evstruct.movie_info(0).fovx, $
	 evstruct.movie_info(0).ycen - 0.5*evstruct.movie_info(0).fovy]/60.
toprt = [evstruct.movie_info(0).xcen + 0.5*evstruct.movie_info(0).fovx, $
	 evstruct.movie_info(0).ycen + 0.5*evstruct.movie_info(0).fovy]/60.
coordsbl=arcmin2hel(btmlt(0),btmlt(1),date=evstruct.movie_info(0).date_obs)
coordstr=arcmin2hel(toprt(0),toprt(1),date=evstruct.movie_info(0).date_obs)

buff = [buff, '            <crd:AstroCoords coord_system_id="UTC-HGS-TOPO">']
buff = [buff, '                <crd:Time>']
buff = [buff, '                    <crd:TimeInterval>']
buff = [buff, '                        ' + strtrim(evstruct.prog_start,2) + ' ' + strtrim(evstruct.prog_stop,2)]
buff = [buff, '                    </crd:TimeInterval>']
buff = [buff, '                </crd:Time>']
buff = [buff, '']
buff = [buff, '                <crd:Position2D unit="arcsec">' + strtrim(coords(0),2) + ' ' + strtrim(coords(1),2) + '</crd:Position2D>']
buff = [buff, '                <crd:SpatialRegion>']
buff = [buff, '                    <crd:Region>Box</crd:Region>']
buff = [buff, '                    <crd:Value2>' + strtrim(coordsbl(0),2) + ' ' + strtrim(coordsbl(1),2) + '</crd:Value2>']
buff = [buff, '                    <crd:Value2>' + strtrim(coordstr(0),2) + ' ' + strtrim(coordstr(1),2) + '</crd:Value2>']
buff = [buff, '                </crd:SpatialRegion>']
buff = [buff, '            </crd:AstroCoords>']
buff = [buff, '        </ObservationLocation>']
buff = [buff, '']
buff = [buff, '    </WhereWhen>']
buff = [buff, '']
buff = [buff, '']
;buff = [buff, '    <Citations>']
;buff = [buff, '    </Citations>']
;buff = [buff, '']
;buff = [buff, '']
buff = [buff, '</VOEvent>']

if keyword_set(write_file) then file_append, outfil_voevent, buff, /newfile

end

