function hek_find_events,t0,t1,x0,y0,radius=radius,target=target,instrument=instrument,url=url,type=type,count=count,query=querystring,maxcount=maxcount,version=version,showtest=showtest
; finds macthing events in target registry
if not keyword_set(instrument) then instrument=''
if not keyword_set(version) then version=2
if keyword_set(maxcount) then maxcount=strtrim(maxcount,2) else maxcount='200'
if not keyword_set(radius) then radius=5000

case strup(target) of
   'HCR': begin
      case version of
         3: begin
            querystring='http://www.lmsal.com/hek/hcr?cmd=search-events3&outputformat=json'
            querystring=querystring+'&startTime='+url_encode(t0)+'&stopTime='+url_encode(t1)
         end
         else: begin
            querystring=ssw_hcr_make_query(t0,t1)
            querystring=querystring+'&xCenMin='+strtrim(x0-radius,2)
            querystring=querystring+'&xCenMax='+strtrim(x0+radius,2)
            querystring=querystring+'&yCenMin='+strtrim(y0-radius,2)
            querystring=querystring+'&yCenMax='+strtrim(y0+radius,2)
            querystring=querystring+'&instrument='+strtrim(strup(instrument))
            querystring=querystring+'&select=maxCadence&select=minCadence'
         end
      endcase
      result=ssw_hcr_query(querystring,count=count)
      count=count-1; hcr always returns 'any'
      if count gt 0 then begin
         print,'number of hits=',count
         result=result(0:count-1)
      endif else result=-1	 
   end
   'HER' or 'her': begin
      if not keyword_set(type) then type='ar'
      ntypes=n_elements(type)
      querystring='cosec=2&&cmd=search&type=column&result_limit='+maxcount+'&event_type='+type+'&event_region=all&event_coordsys=helioprojective&x1='+strtrim(x0-radius,2)+'&x2='+strtrim(x0+radius,2)+'&y1='+strtrim(y0-radius,2)+'&y2='+strtrim(y0+radius,2)+'&event_starttime='+strtrim(anytim(t0,/ccsds),2)+'&event_endtime='+url_encode(anytim(t1,/ccsds))
      if keyword_set(showtest) then querystring=querystring+'&showtests=show'
;      print,querystring
      result=ssw_her_query(querystring,/struct4)
      types=str_sep(type,',')
      ntypes=n_elements(types)
      count=lonarr(ntypes)
      for i=0,n_elements(types)-1 do begin
         status=execute('count('+strtrim(i,2)+')=n_elements(result.'+types(i)+')') 
         if status eq 0 then print,'number of hits for type=',types(i),' is ',count(i)
      endfor
   end
endcase
return,result
end

