function ssw_hpkb_hcr_query,query, ssw=ssw, count=count, eventid=eventid, $
   debug=debug, xml_only=xml_only
;
;+
;   Name: ssw_hpkb_hcr_query **** NOTE: use ssw_hcr_query.pro instead
;
;   Purpose: issue HPKB/HCR query and return results (json->idl structures)
;
;   Input Paramters:
;      query - valid hcr query (via ssw_hpkb_hcr_make_query for example'
;
;   Output:
;      function returns results (structure vector)
;
;   Keyword Paramters:
;      ssw (switch) - if set, add some SSW derived params (date_obs...)
;      count - number of items/structures returned
;      eventid - optional event id ~ivo://sot.lmsal.com/VOEvent#VOEvent...<etc>.xml
;                If supplied, full details for specified event
;      xml - if set, return verbatim xml
;
;   History:
;      26-aug-2009 - S.L.Freeland - query/json wrapper
;      10-sep-2009 - S.L.Freeland - count=0 properly
;      27-sep-2009 - S.L.Freeland - eventid support (query via ssw_hpkb_hcr_make_query(eventid=eid)
;       2-oct-2009 - S.L.Freeland - allow EVENTID via keyword
;      24-nov-2009 - S.L.Freeland - more conservative SP check for eventid
;      28-jan-2010 - S.L.Freeland - add /XML
;       6-may-2010 - S.L.Freeland - Depracted/Replaced by ssw_hcr_query.pro
;
;-
; 
count=0 ; assume failure
debug=keyword_set(debug)

case 1 of
   keyword_set(eventid): qquery=ssw_hpkb_hcr_make_query(eventid=eventid)
   ~data_chk(query,/string): begin
      box_message,'Requires valid HPKB-HCR query string
      return,''
    endcase 
    strpos(query,'http') eq 0: qquery=query ; verbatim
    strpos(query,'cmd') eq 0:  qquery='http://sot.lmsal.com/sot-data?'+query
    else: begin
       box_message,'Not sure that this is a valid HPKB/HCR query - baling...'
       return,''
    endcase
endcase

sock_list,qquery,output
if get_logenv('ssw_hpkb_jsoncheck') ne '' then begin 
   fname=concat_dir(curdir(),get_caller()+'_'+ time2file(reltime(/now)))
   box_message,'dummping json > ' + fname
   file_append,fname,output,/new
endif

if strpos(output(0),'xml') ne -1 then begin
   box_message,'Assuming event html'
   cmd='?cmd=get-voevent-xml&event-id='
   ss=where(strpos(output,cmd) ne -1, ccnt)
   if ccnt gt 0 then getxml='http://sot.lmsal.com/sot-data' + cmd + strextract(output(ss(0)),cmd,"'>VOE")
   sock_list,getxml,xml
   if keyword_set(xml_only) then retval=xml else begin
   ss=where(strpos(xml,'URLParent') ne -1,pcnt)
   if pcnt gt 0 then begin 
      parstr=strcompress(str_replace(strlowcase(xml(ss(0))),'""','"'),/remove)
      urlp=strextract(parstr,'value="','"/')
      spbase=strextract(urlp,'sp3d/','/')
      geny=([str_replace(urlp,'html','geny'),str_replace(urlp,'sp3d','SP3D')+spbase+'.geny'])(strpos(urlp,'sp3d') ne -1)
      break_url,geny,server,path,file
      retval=''
      if file(0) ne '' then begin 
         temp=get_temp_dir()
         lfile=concat_dir(temp,file)
         sock_copy,geny,out_dir=temp
         restgenx,file=lfile, retval
      endif else box_message,'Corrupt coverage event?'
   endif
   endelse
endif else begin
   retval=ssw_hpkb_hcrjson2struct(output)
   if n_elements(retval) gt 1 then retval=retval(sort(retval.starttime))
endelse
count=n_elements(retval) * data_chk(retval,/struct) 

if debug then stop,'retval
return,retval
end

