pro struct4event_save, struct4event_dbase, online=online
;
;+
;   Name: struct4event_save
;
;   Purpose: generate struct4event 'database' => savefile (for no-execute use)
;
;   calling sequence:
;      IDL> struct4event_save
;
;   Keyword Parameters:
;
;   Restrictions:
;      This routine runs on a full license machine and generates the 
;      dbase containing all event structures currently defined by struct4event
;      The dbase is then available to restricted (runtime/VM) licensed machines
;      which do not support the execute statement.
;   
;   6-feb-2009 - S.L.Freeland
;-

etypes=struct4event(/list)
ess=where(strlen(etypes) eq 2,ecnt)

for i=0,ecnt-1 do begin 
   struct4event_dbase= $
      add_tag(struct4event_dbase,struct4event(etypes(ess(i))),etypes(ess(i)))
endfor
dbname='struct4event_dbase.save'
online=keyword_set(online) and get_user() eq 'freeland'
dbfile=concat_dir((['HOME','SSW_ONTOLOGY_DATA'])(online),dbname)
save, struct4event_dbase, file=dbfile

return
end


