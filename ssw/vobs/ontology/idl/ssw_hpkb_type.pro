function ssw_hpkb_type, hpkbout, only_typesi=only_typesi, status=status
;+
;   Name: ssw_hpkb_type
;
;   Purpose: convert ssw_hpkb string output -> struct4event data types
;
;   Input Parameters:
;      hpkbout - nested structure returned by ssw_hpkb_query
;
;   Output:
;      quasi-analogous nested structure approximating struct4event types 
;
;   Keyword Parameters:
;      only_types - optional subset of event types to include in output
;
;   History:
;      3-sep-2009 - S.L.Freeland - prototype
;     28-feb-2012 - S.L.Freeland - mods for variable REFERENCE_<N>_{stuff}
;-
status=0
if n_elements(only_typesi) eq 0 then only_types=struct4event(/list) else $
   only_types=str2arr(strupcase(only_typesi))

sset=where(strlen(only_types) eq 2 and only_types eq strupcase(only_types),cn)

if cn eq 0 then begin 
   box_message,'No valid ONLY_TYPE entries'
   return,''
endif

etypes=tag_names(hpkbout) 
nt=n_elements(etypes)

for t=0,nt -1 do begin 
   etx=etypes(t)
   if is_member(etx,only_types) then begin 
   box_message,'Event Type: ' + etx
   if is_member(etx,only_types) then begin 
      s4e=struct4event(etx)
      nev=n_elements(hpkbout.(t)) ; number of This type
      newt=replicate(s4e,nev)     ; event type per struct4event

      nreq=s4e.required           ; REQUIRED fields
      rt=tag_names(nreq) & ntgs=n_tags(nreq)
      missing='REQUIRED:'
      for tg=0,ntgs-1 do begin 
        tx=tag_index(hpkbout.(t),rt(tg))
        if tx ne -1 then newt.required.(tg) = hpkbout.(T).(tx) else $
           missing=[missing,rt(tg)] 
      endfor
      nopt=s4e.optional           ; OPTIONAL fields
      missing=[missing,'OPTIONAL:']
      rt=tag_names(nopt) & ntgs=n_tags(nopt)
      for tg=0,ntgs-1 do begin 
        tx=tag_index(hpkbout.(t),rt(tg))
        if tx ne -1 then newt.optional.(tg) = hpkbout.(T).(tx) else $
           missing=[missing,rt(tg)] 
      endfor
     
      if n_elements(missing) gt 3 then box_message,$
         ['Warning: No HEK:struct4event match for:','', $
             missing(rem_elem(missing,['REQUIRED:','OPTIONAL:']))]
;      refs=gt_tagval(hpkbout.(t),/refs,missing='')
;      newt.reference_names(0)=refs ; TODO - need another references tweak
      refthings=['NAME','TYPE','URL']
      outthings=['NAMES','TYPES','LINKS']
      htags=tag_names(hpkbout.(t))
      for ref=0,n_elements(refthings) -1 do begin 
         ss=where(strmatch(htags,'REF_*_'+refthings(ref)),rcnt)
         intag=tag_index(newt,'REFERENCE_'+outthings(ref))
         for rn=0,rcnt-1 do begin 
            newt.(intag)(rn)=hpkbout.(t).(ss(rn))
         endfor 
      endfor
      retval=add_tag(retval,newt,etx) ; done this event type 
   endif else box_message,'Invalid event type?? ' + etx
   endif
endfor

return,retval
end
