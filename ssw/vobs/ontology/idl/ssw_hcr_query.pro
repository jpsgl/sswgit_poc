function ssw_hcr_query,query, ssw=ssw, count=count, eventid=eventid, $
   json_verbatim=json_verbatim, $
   debug=debug, xml_only=xml_only, no_convert=no_convert, $
   remove_dummy=remove_dummy, keep_dummy=keep_dummy, $$
   output_only=output_only, loud=loud
;+
;   Name: ssw_hcr_query
;
;   Purpose: issue HPKB/HCR query and return results (json->idl structures)
;
;   Input Paramters:
;      query - valid hcr query (via ssw_hpkb_hcr_make_query for example'
;
;   Output:
;      function returns results (structure vector)
;
;   Keyword Paramters:
;      ssw (switch) - if set, add some SSW derived params (date_obs...)
;      count - number of items/structures returned
;      eventid - optional event id ~ivo://sot.lmsal.com/VOEvent#VOEvent...<etc>.xml
;                If supplied, full details for specified event
;      xml - if set, return verbatim xml
;      no_convert (switch) - if set, do not convert some tags -> floating
;      remove_dummy (switch) - if set, remove the last/dummy element
;                              if that is the ONLY element, return -1
;      keep_dummy (switch) - made /REMOVE_DUMMY dfeault 25-aug-2016 - set /KEEP_DUMMY to restore old (dumb) behavior
;      qoutput - optionally, input the query output (skip the internal sock_list)
;      
;                            
;
;   History:
;      26-aug-2009 - S.L.Freeland - query/json wrapper
;      10-sep-2009 - S.L.Freeland - count=0 properly
;      27-sep-2009 - S.L.Freeland - eventid support (query via ssw_hcr_make_query(eventid=eid)
;       2-oct-2009 - S.L.Freeland - allow EVENTID via keyword
;      24-nov-2009 - S.L.Freeland - more conservative SP check for eventid
;      28-jan-2010 - S.L.Freeland - add /XML
;       6-may-2010 - S.L.Freeland - suite/routine rationalization
;                                   This replaces ssw_hpkb_hcr_query.pro
;      18-sep-2010 - S.L.Freeland - convert subset -> float - add /NO_CONVERT
;                                   to keep old paradigm (string)
;       4-apr-2011 - S.L.Freeland - sot.lmsal.com/ -> lmsal.com/hek
;      13-jul-2011 - S.L.Freeland - ssw_set_proxy.pro (look at $http_proxy & $no_proxy)
;      5-dec-2013 - S.L.Freeland - add /OUTPUT_ONLY keyword; remove ssw_set_proxy (let me know if this breaks something)
;      2-jun-2014 - S.L.Freeland - hooks for new hcr api (only IDL +=8.2 for today)
;     16-jun-2014 - S.L.Freeland - oops, wrap + json_parse call in a call_function
;     25-nov-2014 - S.L.Freeland - some cleanup for new api (see ssw_hcr_make_query.pro)
;      9-apr-2015 - S.L.Freeland - add correlated search option to new-api path
;     25-aug-2016 - S.L.Freeland - made REMOVE_DUMMY the default, add /KEEP_DUMMY
;
;-
count=0 ; pessimistically, assume failure 
debug=keyword_set(debug)
loud=keyword_set(loud)

case 1 of
   keyword_set(eventid): qquery=ssw_hcr_make_query(eventid=url_encode(str_replace(eventid,'#','%23')))
   ~data_chk(query,/string): begin
      box_message,'Requires valid HPKB-HCR query string
      return,''
    endcase 
    strpos(query,'http') eq 0: qquery=query ; verbatim
    strpos(query,'cmd') eq 0:  qquery='http://www.lmsal.com/hek/hcr? ' ; qquery='http://sot.lmsal.com/sot-data?'+query
    else: begin
       box_message,'Not sure that this is a valid HPKB/HCR query - baling...'
       return,''
    endcase
endcase

if data_chk(json_verbatim,/string) then output=json_verbatim else $ 
   sock_list,qquery,output

if get_logenv('ssw_hpkb_jsoncheck') ne '' then begin 
   fname=concat_dir(curdir(),get_caller()+'_'+ time2file(reltime(/now)))
   box_message,'dummping json > ' + fname
   file_append,fname,output,/new
endif

newsearch=strpos(qquery,'search-events3') ne -1
corrsearch=strpos(qquery,'events-corr') ne -1

if newsearch or corrsearch then begin 
   if not since_version(8.2) then begin 
      box_message,'Sorry, as of today requires IDL >= 8.2'
      return,'' ; EARLY Exit on New API + Version constraint
   endif
   count=0
   if output[0] eq '' then box_message,'No matches returned' else begin
      output=call_function('json_parse',output,/TOSTRUCT)
      if newsearch then output=output[0].(0) ; Events LIST 
      count=n_elements(output)
      if debug then stop,'search3,output,qquery
      if loud then print,'Beta api - returning '+strtrim(count,2) + ' HCR matches (IDL List returned)'
   endelse
   return,output ; EARLY EXIT for New API (uses json_parse intrinsic)
endif ; else continue with original/old API paradigm

output=strarrcompress(web_dechunk(output))

if keyword_set(output_only) then begin ; return "raw" output from query & exit
   return,output  ; !!! Early/unstructured exit if /OUTPUT_ONLY set
endif

if strpos(output(0),'xml') ne -1 then begin
   box_message,'Assuming event html'
   cmd='?cmd=get-voevent-xml&event-id='
   ss=where(strpos(output,cmd) ne -1, ccnt)
   if ccnt gt 0 then getxml='http://www.lmsal.com/hek/hcr' + cmd + strextract(output(ss(0)),cmd,"'>VOE")
   ;ssw_set_proxy,getxml
   sock_list,getxml,xml
   ;ssw_set_proxy,/restore
   if keyword_set(xml_only) then retval=xml else begin
   ss=where(strpos(xml,'URLParent') ne -1,pcnt)
   if pcnt gt 0 then begin 
      parstr=strcompress(str_replace(strlowcase(xml(ss(0))),'""','"'),/remove)
      urlp=strextract(parstr,'value="','"/')
      spbase=strextract(urlp,'sp3d/','/')
      geny=([str_replace(urlp,'html','geny'),str_replace(urlp,'sp3d','SP3D')+spbase+'.geny'])(strpos(urlp,'sp3d') ne -1)
      break_url,geny,server,path,file
      retval=''
      if file(0) ne '' then begin 
         temp=get_temp_dir()
         lfile=concat_dir(temp,file)
         sock_copy,geny,out_dir=temp
         restgenx,file=lfile, retval
      endif else box_message,'Corrupt coverage event?'
   endif
   endelse
endif else begin
   retval=ssw_hpkb_hcrjson2struct(output)
   if n_elements(retval) gt 1 then retval=retval(sort(retval.starttime))
   if data_chk(retval,/struct) and ~keyword_set(no_convert) then begin
      ftags=str2arr('maxcadence,mincadence,xcen,ycen,xfov,yfov')
      for t=0,n_elements(ftags)-1 do begin 
         tind=tag_index(retval,ftags(t))
         if tind ne -1 then begin 
            val=gt_tagval(retval,ftags(t))
            ssa=where(val eq 'any',acnt) 
            if acnt gt 0 then val(ssa)='-999999.' ; undefined value
            retval=rep_tag_value(retval,float(val),ftags(t))
         endif
      endfor
   endif
endelse
remove_dummy=1-keyword_set(keep_dummy)

if keyword_set(remove_dummy) and data_chk(retval,/struct) then begin 
   ssok=where(retval.starttime ne 'any',okcnt)
   if okcnt gt 0 then retval=retval(ssok) else retval = -1
endif 
count=n_elements(retval) * data_chk(retval,/struct) >  (keyword_set(eventid) and n_elements(retval) gt 0)

if count eq 0 then box_message,'No HCR records match your query'
if debug then stop,'retval
return,retval
end

