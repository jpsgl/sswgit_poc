function ssw_extra2struct, _extra=_extra
return,_extra
end
;
function ssw_her_query, query, _extra=_extra, $
   lmsal=lmsal, uri_query=uri_query, inlist=inlist, debug=debug, $
   struct4event_types=struct4event_types, save_query=save_query, $
   chunking=chunking, incref=incref, padref=padref, $
   result_limit=result_limit, all_pages=all_pages, loud=loud
;
;+
;   Name: ssw_her_query
;
;   Purpose: query HPKB and return matches as sswidl structures
;
;   Input Paramters:
;      query - properly formatted verbatim query (alternate via keyword)
;
;   Output:
;      function returns vector of matches as IDL structures
;
;   Keyword Parameters:
;      param=value - via inheritance, any accepted 
;      lmsal - if set, use LMSAL query uri (default)
;      struct4event_types - switch - if set, attempt typing per struct4event 
;      save_query - if set, save query response ->  file (her_query)
;      chunking - if set, enable server chunking (tbd mod to web_dechunk.pro)
;      incref - if set, include reference tags (may vary# call to call)
;      padref - if set, include references; pad all tags to max allowable#(20)
;      result_limit - -> ssw_her_make_query - optionally expand limit on #records (per event type)
;      ALL_PAGES (switch) - if set, internally loop/concatenate if #recs=result limit
;       
;
;   Calling Context:
;      Within SSW, this function "often" use in conjunction with ssw_her_make_query.pro
;      IDL> her=ssw_her_query(ssw_her_make_query(t0,t1)) ; All HER events within time range (nested structure outpu)
;      IDL> her=ssw_her_query(ssw_her_make_query(t0,t1,/FL,result_limit=500),/ALL_PAGES) ; FLare only, internally loop/concatenate HER till "done"
;
;   History:
;      13-aug-2008 - S.L.Freeland
;      28-aug-2008 - handle mixed event types
;      27-jan-2008 - reinstate mixed types (which stopped working circa 2008 due
;                    to change in cgi-output/event_type ordering)
;       3-sep-2009 - add /STRUCT4EVENT_TYPES keyword (via ssw_hpkb_types.pro)
;      27-sep-2009 - change default HER query url (match dbase update)
;      16-dec-2009 - add /SAVE_QUERY keyword & function (previsously, and
;                    accidently, that was the previous default...)
;       6-may-2010 - changed HER url to "released" version
;      29-apr-2011 - use "special"  de-chunk algorithm (beta) +  post process
;       2-may-2011 - disable chunking on HER server by def. /CHUNKING override
;       1-apr-2012 - support HER references - added /INCREF & /PADREF
;      11-dec-2012 - change FORLOOP variable CC -> CCC (conflicted with event type=CC!)
;      11-dec-2012 - clarify nothing returned message 
;       1-mar-2013 - added /ALL_PAGES keyword and logic
;                    Per Ryan Timmons request, append &requestfrom=SSW to HER queries for tracking stats.
;      16-mar-2015 - S.L.Freeland - ll->ll_longer (avoid eis 'll' collision) ; list -> listx just because 'list' may be future problem.
;
;   Restrictions:
;      assumes homogenous PARAM xml for all matches  (# & order of NAME="xxx")
;      /ALL_PAGES currently only for single EVENT_TYPE
;-
debug=keyword_set(debug)
loud=keyword_set(loud)

lmsal=keyword_set(lmsal)

case 1 of 
   data_chk(uri_query,/string):   ; user supplied 
   lmsal: uri_query='http://www.lmsal.com/hek/her' ;her/dev/search-hpkb/hek' ;apisearch'
   else:  uri_query='http://www.lmsal.com/hek/her' ;her/dev/search-hpkb/hek' ;apisearch'
endcase

if n_elements(query) eq 0 then query=''
if data_chk(_extra,/struct) then begin 
   tn=tag_names(_extra)
   nt=n_elements(tn)
   qarr=strarr(nt)
   for i=0,nt-1 do begin
      qarr(i)=tn(i)+'='+url_encode(strtrim(_extra.(i),2))
   endfor
   qstring=arr2str(qarr,'&')
   query=query+(['','&'])(query ne '') + qstring
endif

if strpos(query,'http:') ne -1 then full_query=query else $
  full_query=uri_query + '?' + query

if strpos(query,'cosec=2') ne -1 then begin 
   box_message,'at least for now, forcing cosec=1'
   full_query=str_replace(full_query,'cosec=2','cosec=1')
endif

if 1-strmatch(full_query,'*requestfrom=SSW*') then $
   full_query=full_query+'&requestfrom=SSW'

if debug then stop,'request

dechunk=keyword_set(chunking) 
if keyword_set(inlist) then listx=inlist else begin
   protocol=(['1.0','1.1'])(dechunk)
   sock_list,full_query,listx, protocol=protocol
   if keyword_set(save_query) then $
       file_append,'hpkb_query',['; ---- ' + systime(),listx],/new
endelse
if debug then stop,'full_query,listx'

listtemp=listx

if dechunk then listx=web_dechunk_special(listx) ; s.l.f. 29-apr-2011 ; "beta"

pss=where(strpos(listx,'<param') ne -1, pcnt)
psse=where(strpos(listx,'</param>') ne -1, pecnt)

if pcnt eq 0 then begin 
   pval=1  ; default page1
   ssp=where(strmatch(full_query,'*page=*'),pcnt)
   if pcnt gt 0 then pval=fix(strextract(full_query+'&','page=','&'))
   if pval eq 1 then box_message,'No HER matches found for this query'
   if debug then stop
   return,-1
endif

pssb=where(strpos(listx,'<param') ne -1 and strpos(listx,'</param>') eq -1, pbcnt)
if pbcnt gt 0 and dechunk then begin 
   if debug then stop,'mismatch'
   listx[pssb]=strmids(listx[pssb],0,strlen(listx[pssb])-1) + $
      listx[pssb+2]
   listx[pssb+1]=''
   listx[pssb+2]=''
endif

results=where(strpos(listx,'<result>' ) ne -1,rcnt)
eresults=where(strpos(listx,'</result>')  ne -1, recnt)

if rcnt eq 0 then begin 
   box_message,'No matches...'
   return,-1
endif

listx=listx[results(0):last_nelem(eresults)]
listx=ssw_hpkb_order_event(listx) ; restore old paradigm
results=where(strpos(listx,'<result>' ) ne -1,rcnt)
eresults=where(strpos(listx,'</result>')  ne -1, recnt)
dres=eresults-results

etss=where(strpos(listx,'event_type') ne -1)
uet=uniq(listx[etss])  ; uniq Event Types
etypes=strextract(listx[etss[uet]],'>','<')

uet=[-1,uet] + 1

for t=0,n_elements(uet)-2 do begin 
   ll_longer=listx[results[uet[t]]+1:eresults[uet(t+1)-1]]   
   ll_longer=ll_longer(where(strpos(ll_longer,'<param') ne -1))
   names=strextract(ll_longer,'"') & values=strextract(ll_longer,'>','<')
   unames=names(uniqo(names))
   nret=uet(t+1)-uet(t)
   nu=n_elements(unames)
   ; check that all names have consistent#matches
   chkvar=lonarr(nu)
   for ccc=0,nu-1 do begin 
      chkvar(ccc)=n_elements(where(strpos(listx,unames(ccc)) ne -1))
   endfor
   ssc=where((chkvar mod nret) ne 0,ecnt)

   estat=execute(etypes(t)+'=create_struct(unames,'+ $     ; generate template structure
      arr2str(replicate('""',n_elements(unames)))+')')
   estat=execute('retval='+etypes(t))
   retval=replicate(retval,nret)
   pdata=values
   reads,pdata,retval ; string array -> structure vector
   estat=execute(etypes(t) + '=temporary(retval)')
endfor

estat=execute('retval=ssw_extra2struct('+arr2str(etypes+'='+etypes) +')')
if debug then stop,'before return,retval'

if data_chk(retval,/struct) and keyword_set(struct4event_types) then $
   retval=ssw_hpkb_type(retval,_extra=_extra)

padref=keyword_set(padref)
incref=keyword_set(incref) or padref
case 1 of 
   padref: box_message,'no quite yet'
   incref: ; verbatim
   else: begin ; remove reference tags
      for t=0,n_tags(retval)-1 do begin
         tr=tag_names(retval) 
         tn=tag_names(retval.(t))
         ssr=where(strmatch(tn,'REF_*_*'),rcnt)
         if rcnt gt 0 then begin
            thist=retval.(t)
            nor=str_subset(thist,tn(ssr),/exclude)
            retval=rep_tag_value(retval,nor,tr(t))
         endif

      endfor

   endcase
endcase

if keyword_set(all_pages) and data_chk(retval,/struct) then begin 
   nt=n_tags(retval)
   tnames=tag_names(retval)
   case 1 of 
      strmatch(full_query,'*result_limit*'): result_limit=fix(strextract(full_query+'&','result_limit=','&'))
      n_elements(result_limit) gt 0: 
      else: result_limit=100
   endcase
   if n_elements(result_limit) eq 0 then result_limit=100 ; default
      for t=0,nt-1 do begin 
         pretval=retval.(t)
         page=1 
         trynext=n_elements(pretval.(0)) eq result_limit
         while n_elements(retval.(t)) eq result_limit or trynext do begin  
            page=page+1
            if strmatch(full_query,'*page=*') then begin 
               pval=strextract(full_query+'&','page=','&')
               event_type=strextract(full_query+'&','event_type=','&')
               full_queryp=str_replace(full_query,'page='+pval,'page='+strtrim(fix(pval)+1,2))
               full_queryp=str_replace(full_queryp,'event_type='+event_type,'event_type='+tnames[t])
            endif else full_queryp=full_query + '&page=' + strtrim(page,2)
            if loud then box_message,'Next &page='+strextract(full_queryp+'&','page=','&')
            pretval=ssw_her_query(full_queryp,loud=loud, debug=debug)
            if data_chk(pretval,/struct) then begin 
               cpage=[retval.(t),temporary(pretval.(0))] 
               if loud then time_window,cpage.event_starttime
               retval=rep_tag_value(retval,cpage,strextract(full_query+'&','event_type=','&'))
               trynext=n_elements(pretval.(0)) eq result_limit
            endif else trynext=0
            if loud then help,retval,/str
         endwhile         
      endfor
endif


return,retval

end
