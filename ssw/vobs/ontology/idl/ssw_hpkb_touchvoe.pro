function ssw_hpkb_touchvoe, voe, expire=expire, ingest=ingest
if not file_exist(voe) then begin 
   box_message,'expects VOE full path name'
   return,0
endif

voedat=rd_Tfile(voe)
udat=strupcase(voedat)

dss=where(strpos(voedat,'<Date>') ne -1 and strpos(voedat,'</Date>') ne -1,dcnt)
if dcnt ne 1 then begin 
   box_message,'Unexpected VOE...
   return,0
endif

old=strextract(voedat(dss),'<Date>','</Date>')
box_message,voedat(dss)
new=reltime(/now,out='ccsds')
voedat(dss)=str_replace(voedat(dss),old,new)
box_message,voedat(dss)

stop

if keyword_set(expire) then begin 
   wss=where(strpos(voedat,'<Why') ne -1, wcnt)
   stop
endif
return,1
end

