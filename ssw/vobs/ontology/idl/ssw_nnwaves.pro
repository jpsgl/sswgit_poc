pro ssw_nnwaves, t0, t1, cat, only_waves=only_waves, aec_off=aec_off, refresh=refresh, search=search, loud=loud, $
   expand_urls=expand_urls 
;+
;   Name: ssw_nnwaves
;
;   Purpose: ssw api Nariaki Nitta SDO/STEREO global wave catalog
;
;   Input Parameters:
;      t0, t1 (optional) - time range
;
;   Output Paramters:
;      cat - structure vector 
;
;   Keyword Parameters:
;      aec_off (switch) - uses AEC OFF catalog
;      search - optional SEARCH array (see struct_where.pro doc header)
;      expand_urls (switch) - if set, add explicit wave/type tags/urls (call ssw_nnwaves_addtags.pro)
;
;   Calling Examples:
;      IDL> ssw_nnwaves,nncat - only one param? then return entire catlog in first param
;      IDL> ssw_nnwaves,t0,t1,cat ; desired time range, return CAT
;
;   Notes:
;      1st read reads/translates/caches (structure vector) entire catalog - use cached for subsequent calls
;-

common ssw_time2nn_waves_aec,aec_text,aec_cat
common ssw_time2nn_waves_aec_off,aec_off_text,aec_off_cat

aec_off=keyword_set(aec_off)
name='aec'+(['','_off'])(aec_off)
loud=keyword_set(loud)
if n_params() le 2 then delvarx,t0,t1

aiatop='http://aia.lmsal.com/'
indtop=aiatop+'/AIA_Waves/'

index=(['','o'])(aec_off) + 'index.html' 

ctext='aec' + (['','_off'])(aec_off) + '_text'
ccat=str_replace(ctext,'text','cat')

estat=execute('needcat=n_elements('+ccat+') eq  0')
refresh=keyword_set(refresh) or needcat

template={date_obs:'',goes_class:'',location:'',noaa_ar:''}
waves=['AIA_'+string(str2arr('94,131,171,193,211,304,335'),format='(I4.4)'),'EUVIA195','EUVIB195']
create_struct_temp,wtemplate,'',waves,replicate('3a',n_elements(waves))
template=join_struct(template,wtemplate)

if refresh then begin
   sock_list,concat_dir(indtop,index),tcat
   estat=execute(ctext+'=tcat')
   ssu=where(strmatch(tcat,'*href="*html*'))
   ssr=where(strpos(tcat,'<tr><td>20') eq 0,rcnt)
   cat=replicate(template,rcnt)
   recs=ssw_strsplit(tcat[ssr],'href="',/head,tail=root_urls)
   info=strextract(recs,'<tr><td>','</td><td></td><td><a') 
   cols=str2cols(info,'</td><td>',/unali,/trim) 
   for c=0,data_chk(cols,/nx)-1 do cols[c,0]=transpose(str_replace(reform(cols[c,*]),'/td> td>',''))
   strtab2vect,cols,day,time,goes,location,ar
   cat.date_obs=day+ ' ' + time
   cat.goes_class=goes
   cat.location=location
   cat.noaa_ar=ar
   urls=concat_dir(aiatop,strextract(tcat,'..','"'))
   ssr=[ssr,last_nelem(ssr)+deriv_arr(last_nelem(ssr,2))]
   box_message,'Initializing... subsequent calls will speed up..'
   for r=0,rcnt-1 do begin ; if no missing data, could/should vectorize this
      rcat=tcat[ssr[r]:ssr[r+1]-1]
      urls=concat_dir(aiatop,strextract(rcat,'..','"'))
      for w=0,n_elements(waves)-1 do begin 
         sst=where(strpos(rcat,waves[w]) ne -1,wcnt)
         cat[r].(tag_index(cat,waves[w]))=urls[sst]
      endfor
   endfor
   estat=execute(ccat+'=cat')
endif else begin 
   estat=execute('cat='+ccat)
   if loud then box_message,'using cached catalog'
endelse

tcount=n_elements(cat)
count=tcount

if n_params() eq 3 then begin 
   sst=where(anytim(cat.date_obs) ge anytim(t0) and anytim(cat.date_obs) le anytim(t1),tcount)
   if tcount gt 0 then cat=cat[sst] else begin 
      box_messge,'No entries in your time range'
      cat=-1
   endelse
endif else t0=cat

if data_chk(cat,/struct) and data_chk(search,/string) then begin 
   ss=struct_where(cat,search=search,count)
   if count gt 0 then cat=cat[ss] else begin
      box_message,'No records match your SEARCH parameters...'
      cat=-1
   endelse
endif

if data_chk(cat,/struct) and keyword_set(expand_urls) then begin 
   cat=ssw_nnwaves_addtags(cat)
   if n_params() le 2 then t0=temporary(cat)
endif

return
end


