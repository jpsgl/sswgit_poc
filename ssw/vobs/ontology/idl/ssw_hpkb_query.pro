function ssw_extra2struct, _extra=_extra
return,_extra
end
;
function ssw_hpkb_query, query, _extra=_extra, $
   lmsal=lmsal, uri_query=uri_query, inlist=inlist, debug=debug, $
   struct4event_types=struct4event_types, save_query=save_query
;
;+
;   Name: ssw_hpkb_query **** NOTE: Use ssw_her_query.pro instead ***
;
;   Purpose: query HPKB and return matches as sswidl structures
;
;   Input Paramters:
;      query - properly formatted verbatim query (alternate via keyword)
;
;   Output:
;      function returns vector of matches as IDL structures
;
;   Keyword Parameters:
;      param=value - via inheritance, any accepted 
;      lmsal - if set, use LMSAL query uri (default)
;      struct4event_types - switch - if set, attempt typing per struct4event 
;      save_query - if set, save query response ->  file (hpkb_query)
;
;   History:
;      13-aug-2008 - S.L.Freeland
;      28-aug-2008 - handle mixed event types
;      27-jan-2008 - reinstate mixed types (which stopped working circa 2008 due
;                    to change in cgi-output/event_type ordering)
;       3-sep-2009 - add /STRUCT4EVENT_TYPES keyword (via ssw_hpkb_types.pro)
;      27-sep-2009 - change default HER query url (match dbase update)
;      16-dec-2009 - add /SAVE_QUERY keyword & function (previsously, and
;                    accidently, that was the previous default...)
;       6-may-2010 - changed HER url to "released" version
;       ***********  NOTE, this routine depracted/replaced by ssw_her_query.pro
;
;   Restrictions:
;      assumes homogenous PARAM xml for all matches  (# & order of NAME="xxx")
;-
debug=keyword_set(debug)

lmsal=keyword_set(lmsal)

case 1 of 
   data_chk(uri_query,/string):   ; user supplied 
   lmsal: uri_query='http://www.lmsal.com/hek/her' ;her/dev/search-hpkb/hek' ;apisearch'
   else:  uri_query='http://www.lmsal.com/hek/her' ;her/dev/search-hpkb/hek' ;apisearch'
endcase

if n_elements(query) eq 0 then query=''
if data_chk(_extra,/struct) then begin 
   tn=tag_names(_extra)
   nt=n_elements(tn)
   qarr=strarr(nt)
   for i=0,nt-1 do begin
      qarr(i)=tn(i)+'='+url_encode(strtrim(_extra.(i),2))
   endfor
   qstring=arr2str(qarr,'&')
   query=query+(['','&'])(query ne '') + qstring
endif

if strpos(query,'http:') ne -1 then full_query=query else $
  full_query=uri_query + '?' + query

if strpos(query,'cosec=2') ne -1 then begin 
   box_message,'at least for now, forcing cosec=1'
   full_query=str_replace(full_query,'cosec=2','cosec=1')
endif

if keyword_set(inlist) then list=inlist else begin
   sock_list,full_query,list

   if keyword_set(save_query) then $
       file_append,'hpkb_query',['; ---- ' + systime(),list],/new
endelse
if debug then stop,'full_query,list'

list=strtrim(web_dechunk(list,/compress,debug=debug),2) 
pss=where(strpos(list,'<param') ne -1, pcnt)
if pcnt eq 0 then begin 
   box_message,'No PARAMS(??)'
   if debug then stop
   return,-1
endif

results=where(strpos(list,'<result>' ) ne -1,rcnt)
eresults=where(strpos(list,'</result>')  ne -1, recnt)

if rcnt eq 0 then begin 
   box_message,'No matches...'
   return,-1
endif

list=list[results(0):last_nelem(eresults)]
list=ssw_hpkb_order_event(list) ; restore old paradigm
results=where(strpos(list,'<result>' ) ne -1,rcnt)
eresults=where(strpos(list,'</result>')  ne -1, recnt)
dres=eresults-results

etss=where(strpos(list,'event_type') ne -1)
uet=uniq(list(etss))  ; uniq Event Types
etypes=strextract(list(etss(uet)),'>','<')

uet=[-1,uet] + 1

for t=0,n_elements(uet)-2 do begin 
   ll=list(results(uet(t))+1:eresults(uet(t+1)-1))  
   ll=ll(where(strpos(ll,'<param') ne -1))
   names=strextract(ll,'"') & values=strextract(ll,'>','<')
   unames=names(uniqo(names))
   nret=uet(t+1)-uet(t)
   nu=n_elements(unames)
   ; check that all names have consistent#matches
   chkvar=lonarr(nu)
   for cc=0,nu-1 do begin 
      chkvar(cc)=n_elements(where(strpos(list,unames(cc)) ne -1))
   endfor
   ssc=where((chkvar mod nret) ne 0,ecnt)

   estat=execute(etypes(t)+'=create_struct(unames,'+ $     ; generate template structure
      arr2str(replicate('""',n_elements(unames)))+')')
   estat=execute('retval='+etypes(t))
   retval=replicate(retval,nret)
   pdata=values
   reads,pdata,retval ; string array -> structure vector
   estat=execute(etypes(t) + '=temporary(retval)')
endfor

estat=execute('retval=ssw_extra2struct('+arr2str(etypes+'='+etypes) +')')
if debug then stop,'before return,retval'

if data_chk(retval,/struct) and keyword_set(struct4event_types) then $
   retval=ssw_hpkb_type(retval,_extra=_extra)

return,retval

end
