
pro fits_rescale, iindex=iindex, idata=idata, oindex=oindex, odata=odata, outsiz=outsiz, scale_fac=scale_fac, $
  infil=infil, outfil=outfil, n_chunk=n_chunk, interp=interp, debug=debug, fov=fov

;+
; NAME:
;   fits_rescale
; PURPOSE:
;   Re-scale FITS image(s) and update header(s).
;   Optionally write them out, preserving the naming convention (TODO).
; CALLING SEQUENCE EXAMPLES:
;   IDL> fits_rescale, iindex=iindex, idata=idata, oindex=oindex, scale_fac=2
;   IDL> fits_rescale, infil=infil, outfil=outfil, outsiz=4096
; INPUTS:
;   Input from memory:
;     iindex   - Input index structure (or vector of structures)
;     idata    - Input data arary (or cube)
;   Input from file:
;     infil    - Input FITS file name and path (or vector of file names)
;   Output size specification options:
;     outsiz   - Absolute output dimensions(s) OR...
;     scale_fac - Scale factor
;   infil      - Input file name specification
;   outfil     - Output file name specification
;   interp     - If set, call CONGRID with bilinear interpolation
;      fov     - If set, add tags 'FOVX1' and 'FOVY1' to the output index  
; OUTPUTS:
;   oindex     - Updated index records consistent with new image size
;   odata      - Re-sized image cube
;   AND/OR:
;   file(s) written to filename(s) specified by input 'outfil')
; OPTIONAL KEYWORD PARAMETERS:
; MODIFICATION HISTORY:
;   2009-10-07 - GLS - Written

if not exist(fov) then fov = 0
if not exist(interp) then interp = 0

;if exist(infil) then begin
;  if not exits(n_chunk) then begin
;    bparr = [ 08, 16, 32, 64,-32,-64] ; FITS BITPIX values
;    tparr = [ 01, 02, 03, 14, 04, 05] ; Corresponding IDL datatypes
;    dtype = where(bpx eq bparr,bpcnt)
;    if bpcnt eq 0 then begin
;      box_message, 'Unknown BITPIX. Bailing...'
;      return
;    endif
;  datatype = tparr(dtype)

if exist(infil) then mreadfits, infil, iindex, idata

n_img = n_elements(iindex)

if exist(outsiz) then begin
  if n_elements(outsiz) eq 2 then begin
    scale_fac_x = float(outsiz[0]) / iindex.naxis1
    scale_fac_y = float(outsiz[1]) / iindex.naxis2
  endif else begin
    scale_fac_x = float(outsiz) / iindex.naxis1
    scale_fac_y = float(outsiz) / iindex.naxis2
  endelse
endif else begin
  if exist(scale_fac) then begin
    if n_elements(scale_fac) eq 2 then begin
      scale_fac_x = float(scale_fac[0])
      scale_fac_y = float(scale_fac[1])
    endif else begin
      scale_fac_x = float(scale_fac)
      scale_fac_y = float(scale_fac)
    endelse
  endif else begin
    print, ' Neither outsiz nor scale factor supplied.  Returning.'
    return
  endelse
endelse

naxis1_out = iindex.naxis1 * scale_fac_x
naxis2_out = iindex.naxis2 * scale_fac_y

if (( fix(naxis1_out) ne naxis1_out ) or ( fix(naxis1_out) ne naxis1_out )) then begin
  print, ' Non-integral output image size requested.  Returning.'
  return
endif

xcen_out = iindex.xcen
ycen_out = iindex.ycen

crpix1_out = iindex.crpix1 * scale_fac_x
crpix2_out = iindex.crpix2 * scale_fac_y

crval1_out = 0
crval2_out = 0

cdelt1_out = iindex.cdelt1 / scale_fac_x
cdelt2_out = iindex.cdelt2 / scale_fac_y

if (fov lt 1) then begin
  oindex = iindex
endif else begin
  oindex = add_tag(iindex, cdelt1_out*naxis1_out, 'FOVX1')
  oindex = add_tag(oindex, cdelt2_out*naxis2_out, 'FOVY1')
endelse

oindex.naxis1 = naxis1_out
oindex.naxis2 = naxis2_out
oindex.xcen = xcen_out
oindex.ycen = ycen_out
oindex.crpix1 = crpix1_out
oindex.crpix2 = crpix2_out
oindex.crval1 = crval1_out
oindex.crval2 = crval2_out
oindex.cdelt1 = cdelt1_out
oindex.cdelt2 = cdelt2_out

odata = congrid(idata, naxis1_out, naxis1_out, n_img, interp=interp)

if exist(outfil) then $
  mwritefits, oindex, odata, outfile=outfil

if keyword_set(debug) then stop,' Stopping on request.'

end
