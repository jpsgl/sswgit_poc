function ssw_jp20002struct, jpfiles
;
;+
;   Name: ssw_jp2002struct
;
;   Purpose: jpeg 2000 metadata -> sswidl structure vector - mreadjeg2000.pro meta data piece
;
;   Input Paramters:
;      jpfiles - jpeg 2000 file name(s)
;
;   Restrictions:
;      assumes all jp2000 from same process (so have same params)
;
;   Method:
;      extract meta <param>value</param> -> all-files string array -> "fake FITS" header -> mreadfits_header.pro
;
;   History: 
;      Circa 1-jan-2012 - S.L.Freeland - SSW gen routine
;      25-sep-2012 - S.L.Freeland - dusted off, finished last .1% - added This doc line
;
;-


nf=n_elements(jpfiles)

allmeta=strarr(nf*500) ;

buff=bytarr(1000000l)
pnt=0
for i=0,nf-1 do begin 
   buff(*)=0b 
   openr,lun,jpfiles(i),/get_lun
   readu,lun,buff
   free_lun,lun
   fits=where_pattern(buff,'fits')
   tbuff=buff(fits(0):fits(1))
   ssl=where_pattern(tbuff,10b,lcnt)
   temp=strarr(lcnt)
   for p=0,lcnt-2 do begin
      temp(p)=string(tbuff(ssl(p)+1:ssl(p+1)-1))
   endfor
   temp=temp(where(strmid(temp,0,1) eq '<' and strlastchar(temp) eq '>'))
   temp=temp(0:where(strpos(temp,'<history') ne -1)-1)
   temp=[temp,'<END>END</END>']
   allmeta(pnt)=temp


   pnt=pnt + n_elements(temp)

endfor
allmeta=allmeta(where(allmeta ne ''))
allmeta=allmeta[where(strpos(allmeta,'descr=""') eq -1)]
allmeta=strmid(allmeta,1,1000)
values=strextract(allmeta,'>','<')
params=strjustify(ssw_strsplit(allmeta,'>',/head))
headers=params+'= ' + strjustify(values)
ssend=where(strpos(strcompress(headers,/remove),'END=END') ne -1,ecnt) ; this is the end
if ecnt gt 0 then headers(ssend)='END' ; now FITs-like... so I may use the very-vectorized:
if get_logenv('check_jp2') ne '' then stop,'headers
mreadfits_header,jpfiles,all_headers=headers, retval, template=fitshead2struct(headers[0:ssend(0)])

return, retval

end
