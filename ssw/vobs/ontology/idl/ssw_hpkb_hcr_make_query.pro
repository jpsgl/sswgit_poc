function ssw_hpkb_hcr_make_query,t0,t1,_extra=_extra, $
   initialize=initialize, allow_all=allow_all, $
   no_defsel=no_defsel, select=select, all_select=all_select, $
   help=help, debug=debug, search_valid=search_valid, $
   day_window=day_window, hour_window=hour_window, minute_window=minute_window, $
   eventid=eventid
;
;+
;   Name: ssw_hpkb_hcr_make_query **** NOTE: use ssw_hcr_make_query.pro instead
;
;   Purpose: generate valid HEK-HCR query string from user/ssw options
;
;   Input Parameters:
;      t0,t1 - time range (required)
;      select - optional list of desired OUTPUT params to include
;      all_select - enable all available OUTPUT params (see restrictions)
;      {day,hour,minute}_window - optionally expand time range by this window
;      initialize (switch) - if set, refresh the cache from data files
;      help (switch) - if set, show search & select params and exit
;      evendid - special case - if supplied, construct eventid search query and exit
;
;   Calling Examples:
;      IDL> qstring=ssw_hpkb_hcr_make_query(t0,t1,instrument='trace')
;      ISL> query=ssw_hpkb_hcr_query(qstring) ; use above -> HCR search
;      IDL> qstring=ssw_hpkb_hcr_make_query(/help) ; show search&select params
;
;   History:
;      Circa early March 2009 - S.L.Freeland - prototype 
;      26-aug-2009 - S.L.Freeland - populate/document
;      10-sep-2009 - S.L.Freeland - force parameter typing (case sensitive!)sensitive
;      28-sep-2009 - S.L.Freeland - eventid keyword & function
;       6-may-2010 - S.L.Freeland - suite/routine name rationalization
;                                   Depracted/replaced by ssw_hcr_make_query.pro
;
;   Restrictions:
;      currently inhibiting purpose & descriptions (deal with '"' and "'"s)
;-
common ssw_hpkb_hcr_make_query_blk,asel,aquery
debug=keyword_set(debug)
hcrcgi='http://sot.lmsal.com/sot-data'

ontdata=get_logenv('SSW_ONTOLOGY_DATA')
if not file_exist(ontdata) then ontdata=concat_dir('$SSW_ONTOLOGY','data')

qfile=concat_dir(ontdata,'ssw_hpkb_hcrparam_query.dat')
sfile=concat_dir(ontdata,'ssw_hpkb_hcrparam_select.dat')

if total(file_exist([qfile,sfile])) ne 2 then begin 
   box_message,'problem with configuration...'   
   return,''
endif  

init=n_elements(asel) eq 0 or keyword_set(initialize)

if init then begin 
   asel=rd_tfile(sfile)
   aquery=rd_tfile(qfile)
endif

; if event id is supplied, construct that search query and exit
if data_chk(eventid,/string) then begin 
   retval=hcrcgi+'?cmd=view-event&event-id='+url_encode(eventid(0))
   return,retval ; !!!! EARLY EXIT
endif


if keyword_set(help) then begin 
   sparams=[aquery,replicate('-',n_elements(asel)-n_elements(aquery))]
   retval=strjustify(asel)+' '+sparams
   box_message,[strjustify(['Select Params','-------------',asel]) + '  ' + $
                ['Search Params','-----------',sparams]]
   return,retval  ; !!!! Early Exit

endif

defsel=['cadences','jopId','instrument','eventId', $
        'uModes','starttime','stoptime','target','telescope','wavelengths', $
        'xCen','YCen','xFov','yFov']
case 1 of 
   keyword_set(select): begin  ; user supplied at least one...
      if keyword_set(no_defsel) then sel=str2arr(select) else $
         sel=[defsel,str2arr(select)]  ; default append user to default set
   endcase
   keyword_set(all_select): sel=asel ; full list
   else: sel=defsel ; default subset 
endcase

; temporarily, remove 'purpose' 
sel=strlowcase(sel)
if not keyword_set(allow_all) then sel=sel(rem_elem(sel,['purpose','descriptions']))


if n_elements(t0) eq 0 then begin 
   box_message,'Need at least a time/time range
   return,''
endif  
 
case 1 of 
   n_elements(t1) gt 0: tx=[t0,t1]
   else: tx=t1
endcase

time_window,tx,time0,time1,$
   day_window=day_window, hour_window=hour_window, minute_window=minute_window
time0=anytim(time0,/ccsds,/trunc)
time1=anytim(time1,/ccsds,/trunc)

_extra=add_tag(_extra,time0,'starttime') ; lets call these 2 required for now
_extra=add_tag(_extra,time1,'stoptime')

search=''
tn=tag_names(_extra)

selstr=''
serstr=''

for i=0,n_tags(_extra)-1 do begin
   selss=where(tn(i) eq strupcase(asel), selcnt)
   qss=where(tn(i) eq strupcase(aquery),qcnt)
   searchp=''
   if qcnt gt 0 then $  
        serstr=temporary(serstr)+ '&' + aquery(qss(0))+'=' + $
           url_encode(strtrim(_extra.(i),2)) else $
              box_message,'Don"t know search param:'+tn(i)
endfor
if n_elements(ser) eq 0 then ser=['startTime','stopTime']

for i=0,n_elements(sel)-1 do begin
   sss=where(strupcase(sel(i)) eq strupcase(asel),scnt)
   if scnt gt 0 then selstr= selstr + $ 
    (['','&select='+asel(sss(0))])(strpos(selstr,'&select='+asel(sss(0))) eq -1)
endfor

scmd='submit-search-events2'
searchcmd=hcrcgi+'?cmd='+scmd+ (['','&'])(strmid(serstr,0,1) ne '&') + serstr+selstr  ; fully qualified query
retval=searchcmd

if debug then stop,retval
return,retval
end
