function ssw_make_query,t0,t1,_extra=_extra, structure=structure, $
   minute_window=minute_window, hour_window=hour_window, day_window=day_window
;
;+
;   Name: ssw_make_query
;
;   Purpose: construct a cgi-friendly query via SSW stds & keyword inherit
;
;   Input Parameters:
;      t0,t1 - optional positional STARTTIME & ENDTIME (or use keywords..)
;
;   Keyword Parameters:
;      <keyword>=<value> [,etc...] -> KEYWORD=<urlencoded value> [& etc...]
;
;   Calling Example:
;      IDL> t0=reltime(/now,out='ccsds')
;      IDL> print,ssw_make_query(date_obs=t0,p2="blah blah", p3=[1,2])
;
;   History:
;      Dec 2009 - S.L.Freeland 
;
;-

if data_chk(structure,/struct) then begin 
   hgs_x=string(gt_tagval(structure,/hgs_x,missing=''))
   hgs_y=string(gt_tagval(structure,/hgs_y,missing=''))
   if hgs_x ne '' then begin ; looks like HEK... 
      _extra=add_tag(_extra,ssw_helio2string(float([hgs_x,hgs_y])),'ref_helio')
      if n_elements(t0) eq 0 then t0=structure.event_starttime  
   endif else begin ; ssw standard...
      if n_elements(t0) eq 0 then t0=anytim(structure(0),/yohkoh)
      if n_elements(t1) eq 0 then t1=anytim(last_nelem(structure,/yohkoh) )
   endelse
endif else begin ; 
   case n_params() of
      0: query='' ; allow free form, no time/time range
      1: t1=t0
      else:
   endcase
endelse
 
if n_elements(t0) gt 0 then begin
   time_window,[anytim(t0,/yoh),anytim(t1,/yoh)],minute_window=minute_window,$
      hour_window=hour_window,day_window=day_window, time0, time1
   query='starttime='+url_encode(anytim(time0,/yohkoh,/trunc)) + '&endtime=' + $
         url_encode(anytim(time1,/yohkoh,/trunc))
endif

tn=tag_names(_extra)
for i=0,n_tags(_extra)-1 do begin 
   query=query+(['','&'])(query ne '') +tn(i)+'='+url_encode(strtrim(arr2str(_extra.(i)),2))
endfor

return,query
end

