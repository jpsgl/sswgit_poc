function ssw_hpkb_moviename2struct,movie_names

temp={system:'',whomodule:'',starttime:'',stoptime:'',obs:'',instrument:'',$
        waves:'',nframes:0,identifier:''}

test= $
 'EDS_FlareDetective_20091118T191115-20091118T223423_AIA_171-193-305_20_S30W48'

if n_elements(movie_names) eq 0 then movie_names=test

segments=str2cols(movie_names,'_',/unali,/trim)

strtab2vect,segments,system,whomod,trange,instr,waves,nframes,ident
trange=str2cols(trange,'-',/trim)
strtab2vect,trange,startt,stopt

retval=temp
retval.starttime=file2time(ssw_patt_replace(startt,'T','_'),out='ccsds')
retval.stoptime=file2time(ssw_patt_replace(stopt,'T','_'),out='ccsds')
retval.system=system
retval.whomodule=whomod
retval.instrument=instr
retval.waves=ssw_patt_replace(waves,'-',',')
retval.nframes=str2number(nframes)
retval.identifier=ident


return,retval
end



