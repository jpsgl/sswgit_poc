function ssw_list2structvect, listx, taglist=taglist, superset=superset, verbatim=verbatim
;+
;   Name: ssw_list2structvect
;
;   Purpose: convert IDL LIST of structures to an structure-vector, optionally with sub-set or super set of LIST elements
;
;   Input Parameters:
;      listx - and IDL LIST containing one or more structures, assume at least subset of overlapping tags
;
;   Output:
;      function returns structure vector (ala ssw 'index' vector)
;
;   Keyword Paramters;
;      taglist - optional taglist subset to include - assume present in all LIST structures
;      superset (switch) - if set, include supser set of all LIST-structure tags (first pass makes template)
;      verbatim (switch) - if set, assume all elements of LIST are "the same" structure (ntags/types)
;
;   History:
;      20-jun-2014 - S.L.Freeland - originally for HCR/new api support (output from ssw_hcr_query with "new" api)
;      19-sep-2014 - S.L.Freeland - dusted off, implement /SUPERSET
;      30-mar-2017 - S.l.Freeland - avoid datatype issues for "unexpected" '!NULL' (strings in numeric field)
;
;-

if data_chk(listx,/type) ne 11 then begin 
   box_message,'Expects input parameter of LIST type. bailing...'
   return,-1
endif


temp=listx[0]  ; starting template
nlist=n_elements(listx)
verbatim=keyword_set(verbatim)
superset=keyword_set(superset)
subset=keyword_set(taglist)
retval= -1

case 1 of 
  verbatim: begin
     retval=replicate(temp,nlist)
     for r=1,nlist-1 do retval[r]=listx[r]
  endcase
  subset: begin 
     if data_chk(taglist,/string) then tlist=str2arr(taglist) else $
        tlist=tag_names(temp)
     tlist=tlist(rem_elem(tlist,'GROUPS'))
     tlist=arr2str(tlist)
     retval=replicate(str_subset(temp,tlist),nlist)
     tname=tag_names(retval)
     tnum=n_elements(tname)
     for r=1,nlist-1 do begin 
         tempx=str_subset(listx[r],tlist)
         for t=0,tnum-1 do begin 
            if (datatype(retval[r].(t)) ne datatype(tempx.(t))) then begin 
               box_message,'type conflict>> ' + tname[t] + ' (attempting fix...'
               tempx=rep_tag_value(tempx,retval[r].(t),t)
               tempx.(t)=-9999
            endif
         endfor
         retval[r]=tempx[0]
     endfor
   endcase
   superset: begin 
      template=temp
      tnt=tag_names(template)
      for r=1,nlist-1 do begin 
         lx=listx[r]
         tn=tag_names(listx[r])
         tind=tag_index(lx,tnt)
         ss=where(tind eq -1,nccnt)
         if nccnt gt 0 then stop,'template,lx,tn[ss]


      endfor
stop,'alltags'
   endcase
   else:box_message,'Need /superset, TAGLIST, or /verbatim
endcase

return,retval
end
