function ssw_service_queue_status,jobids, urls=urls, decode=decode, resubmit=resubmit, $
    stalled_urls=stalled_urls, stall_hours=stall_hours
;+
;   Name: ssw_service_queue_status
;
;   Purpose: check Q status of one or more ssw service JobIDs
;
;   Input Parameters:
;      jobids - one or more SSW service jobids
;
;   Ouput:
;      function returns status for each jobid {"pending", "running", "finished", "stalled", "notfound"}

;   Keword Parameter:
;     resubmit - if set, resubmit ->  (not yet implmented)
;     urls (output) - current Q location/url for each JOBIDS (null if not found anywere in Q)
;     stalled_urls (output) - subset of urls flagged as 'stalled' (null if none)
;     stall_hours - optional #hours defined as "stalled" (eventually, will look at logs instead... but not today)
;
;   History:
;      5-feb-2013 - S.L.Freeland - functional place holder
;
;  Restrictions:
;      use /RESUBMIT with caution...
;-

if not data_chk(jobids,/string) then begin 
   box_message,'Need one or more SSW service jobids (ssw_service_yymmdd_hhmmss_<pid> )
   return,-1
endif

service='http://www.lmsal.com/cgi-ssw/ssw_service_queue_info.sh?'

if n_elements(jobids) eq 1 then sswjobs=strtrim(jobids,2) else $
   sswjobs=arr2str(strtrim(jobids,2)) ; comma delimited list


sock_list,service+'jobids='+sswjobs,urls , /use_network

nret=n_elements(urls)

retval=replicate('notfound',nret) ; assume failure...

rtypes=str2arr('pending,running,finished')
qtypes=str2arr('requested,current,finished') + '/'
for t=0,n_elements(rtypes)-1 do begin 
  ss=where(strpos(urls,qtypes[t]) ne -1,scnt)
  if scnt gt 0 then retval[ss]=rtypes[t]
endfor

; check running vs stalled
ssr=where(retval eq 'running',rcnt)
stalled_urls=''
if rcnt gt 0 then begin 
   if n_elements(stall_hours) eq 0 then stall_hours=12 ; 
   now=reltime(/now,hours=-8,out='ecs') ; local 
   jtimes=file2time(urls[ssr],out='ecs')
   dt=ssw_deltat(now,ref=jtimes,/hours)
   ssstalled=where(dt gt stall_hours,scnt)
   if scnt gt 0 then begin
      retval[ssr[ssstalled]]='stalled'
      stalled_urls=urls[ssr[ssstalled]]
   endif
endif


if keyword_set(resubmit) then begin 
   box_message,'sorry, /RESUBMIT not yet implemented - tell Sam you are interested in this option..
endif

return,retval

end


