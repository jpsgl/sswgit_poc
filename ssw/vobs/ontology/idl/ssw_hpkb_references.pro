function ssw_hpkb_references, hpkb

reftemp=str_subset(struct4event('fl'),'reference_names,reference_types,reference_links')

refs=gt_tagval(hpkb,/refs)
nout=n_elements(refs)
temp=replicate({names:'',type:'',item:''},nout)
retval=replicate(reftemp,nout) ; padded -> struct4event max
qss=where_pattern(refs,'"')
nstrings=n_elements(qss)/2
s0=qss(indgen(nstrings)*2)
s1=qss(indgen(nstrings)*2+1)
dt=s1-s0
strings=strmid(refs,s0+1,dt-1)

cut=(where(strings(*,0) eq ''))(0) -1

strings=temporary(strings(0:cut,*))

for i = 0, cut, 3 do begin 
   reads,strings(i:i+2,*),temp ; semi vectorized.. maybe look at this later
   retval.reference_names(i/3)=temp.names
   retval.reference_links(i/3)=temp.item
   retval.reference_types(i/3)=temp.type
endfor

return,retval
end










