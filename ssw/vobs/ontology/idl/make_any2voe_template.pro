pro make_any2voe_template, fname, fe_code=fe_code, $
    optional_uncomment=optional_uncomment, outdir=outdir, no_suffix=no_suffix, $
    inthing=inthing
;
;
;+
;   Name; make_any2voe_template
;
;   Purpose: generate code template for a "something to HEK voevent" function
;            Where "something" is usually a non-HEK catalog structure/vector
;
;   Input Parameters:
;      fname - function name - for example 'ssw_findstuff'
;
;   Keyword Parameters:
;      fe_code - feature code per struct4event - determines contents of template
;      outdir - output directory (def is current)
;      no_suffix - if set, suppress appending '_2voe' to 'fname' 
;      optional_uncomment - overrides default which includes prependend 
;              ild comment char (;) for optional tags.
;
;   Example:
;      IDL> make_any2voe_template,'ssw_latest_events',fe='FL'
;       

;
;   Method:
;      use rsi/idl 'help' function output on 'struct4event' output structure
;      and generate a function timeplate to be tweaked by COE.
;
;   History:
;      Circa summer 2008 - S.L.Freeland
;      15-may-2009 - S.L.Freeland - add history line
;-
;   

if n_elements(fe_code) eq 0 then begin 
    box_message,'No FE_CODE, defaulthing to "FL" (!!)'
    fe_code='FL'
endif

if n_elements(fname) eq 0 then begin 
   box_message,'You did not supply a function name, so I am using "ssw_x2voe"'
   fname='ssw_x2voe'
endif
com_opt=1-keyword_set(uncomment_optional) 

no_suffix=keyword_set(no_suffix) 
if strpos(fname,'2voe') eq -1 and (1-no_suffix) then begin 
   func=fname+'2voe'
endif else func=fname

if not keyword_set(outdir) then outdir=curdir()

fullname=concat_dir(outdir,func+'.pro')

strtemp=struct4event(fe_code)  ; get current FE:structure definition

help,strtemp.required, out=required,/str
help,strtemp.optional, out=optional,/str
specfile=gt_tagval(strtemp,/specfile,missin='??')

strtemp=struct4event(fe_code)  ; get current FE:structure definition

help,strtemp.required, out=required,/str
help,strtemp.optional, out=optional,/str
specfile=gt_tagval(strtemp,/specfile,missin='??')

required=strtrim(required(1:*),2)
optional=strtrim(optional(1:*),2)

; since some KEYWORD/TAGS are "very" long, help splits them - restore here...

all = [required,optional]
ssl=where(strlen(all) eq strlen(strcompress(all)),sslcnt)
if sslcnt gt 0 then begin 
   all(ssl)=all(ssl)+ ' ' + all(ssl+1)
   all(ssl+1)=''
   all=strarrcompress(all)
endif

cols=str2cols(all,/unal) 
strtab2vect,cols,tn,dtype,values
dtype=strlowcase(dtype)
sext=strextract(all,"'",/inc)
sss=where(sext ne '',scnt)
if scnt gt 0 then values(sss)=sext(sss)
ssinf=where(strlowcase(values) eq 'inf',icnt)
if icnt gt 0 then values(ssinf)="'Inf'"
ssopt=where(tn eq (tag_names(strtemp.optional))(0))
now=anytim(reltime(/now),/vms,/date_only)
init=['function ' + func + ',inthing ', $
      '',';+','',';   Name: ' + func, '', $
      '','; Input Parameters: ', $ 
      '', ';   inthing ','',';-', $
      '; Define output structure via struct4event', $
      '','','; History: generated by ssw_makeanyvoe_template on ' + now + $
         ' run by ' + get_user() + ' @'+ get_host(),  $
      'retval=struct4event("'+fe_code+'")','']
outarr=['; ------ Required Tag Assignments ----', $
        strjustify('retval.required.'+tn(0:ssopt-1) + ' = '+ $
        values(0:ssopt-1)) + $
         strjustify('; ' + dtype(0:ssopt-1)), '', $
        '; ------ Optional Tag Assignments ----', $
        (['',';'])(com_opt) + $
        strjustify('retval.optional.'+tn(ssopt:*)   + ' = '+ $
        values(ssopt:*)) + $  
        strjustify(' ; ' + dtype(ssopt:*)), '']
         

file_append,fullname,init,/new
file_append,fullname,outarr
file_append,fullname,['return,retval','end']
box_message,'Wrote function template: ' + fullname
return
end



