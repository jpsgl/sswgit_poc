
pro ssw_hek_annotator, index=index, data=data, map=map, $
   _extra=_extra, $	; Keyword inheritence -> annotator options
   nospawn=nospawn, showcommand=showcommand, background=background, debug=debug, annocmd=annocmd

;+
;   Name: ssw_hek_annotator
;
;   Purpose: Construct UNIX command for and launch the HEK Annotator
;
;   Input Parameters: None at this time.
;
;   Output: None at this time
;
;   Keyword Parameters:
;	_extra - unspecified keyword options
;	annocmd (output) - The fully specified Annotator command
;	nospawn (switch) - If set, do not actually execute/spawn (return ANNOCMD only for example)
;	showcommand (switch) - If set, print the implied annotator command
;	background (switch) - If set, spawn annotator as background task (Unix only..)
; 
;   Calling Sequence:
;	IDL> ssw_annotator
;
;   Calling Examples:
;
;   History:
;	04-apr-2008 - G.L.Slater
;-

;if not data_chk(parameter_x) then begin 
;  box_message,' You must supply parameter ' + strtrim(parameter_x)
;  return
;endif

if not exist(map) then begin
  map = mk_secchi_map(index, data)
  map = add_tag(map, map.data[*,0], 'naxis1')
  map = add_tag(map, map.data[0,*], 'naxis2')
  map = add_tag(map, map.naxis1, 'xfov')
  map = add_tag(map, map.naxis2, 'yfov')
  map = add_tag(map, 0, 'crval1')
  map = add_tag(map, 0, 'crval2')
  map = add_tag(map, xcen_earth, 'xcen_earth')
  map = add_tag(map, ycen_earth, 'ycen_earth')
  map = add_tag(map, index.obsrvtry, 'obsrvtry')
  map = add_tag(map, index.obsrvtry, 'telescop')
  map = add_tag(map, index.instrume, 'instrume')
  map = add_tag(map, index.wavelnth, 'wavelnth')

; Calculate xcen and ycen in Earth view:

  rsun_au = (1.38d6/2.0) / 149.60d6
  sun_data = get_sun(anytim(map.time,/yoh), dist=dist, sd=sd)
  r0_sol_radii = ((sd/map.rsun)*dist) / rsun_au

; Create normalized (rsun=1) Cartesian corrdinates:
  yn = map.xc/map.rsun
  zn = map.yc/map.rsun
  xn = sqrt(1 - yn*yn + zn*zn)
; Convert to local view spherical coordinates using local b0, p angles:
  rtp0 = c2s([xn,yn,zn], b0=-map.b0, roll=-map.roll_angle)
; Local view lat and lon:
  lat = rtp0[1]
  lon = rtp0[2]
; Earth-view lat and lon:
  lat_earth = lat
  lon_earth = lon + map.l0
; Earth-view heliographic:
  xy_arcmin = hel2arcmin(lat_earth, lon_earth, date=map.time)
  xcen_earth = xy_arcmin[0]
  ycen_earth = xy_arcmin[1]

  map = add_tag(map, xcen_earth, 'xcen_earth')
  map = add_tag(map, ycen_earth, 'ycen_earth')
endif

spawnit = 1-keyword_set(nospawn)    ; don't actually spawn the annotator command
debug = keyword_set(debug)
	
javadir = concat_dir(get_logenv('SSW_ONTOLOGY'), 'java')
annocmd = 'exec java -jar ' + concat_dir(javadir, 'annotator_gui.jar')

;Construct and append additional parameters to command:
sttim = anytim(map.time, /ccsds)
sttim = str_replace(sttim,'T',':')
entim = sttim
t_current = anytim(!stime, /ccsds)
t_current = str_replace(t_current,'T',':')
dir_event = concat_dir(get_logenv('HOME'), 'annotator_' + strtrim(t_current,2))
espawn, 'mkdir ' + dir_event
file_image = concat_dir(dir_event, strtrim(map.obsrvtry,2) + '_event_image_' + strtrim(sttim,2))
write_jpeg, file_image, data
annocmd = annocmd + ' -moviefile none'
annocmd = annocmd + ' -imagefile ' + strtrim(file_image,2)
annocmd = annocmd + ' -xcen ' + strtrim(fix(map.xcen_earth),2)
annocmd = annocmd + ' -ycen ' + strtrim(fix(map.ycen_earth),2)
annocmd = annocmd + ' -bboxllx ' + strtrim(fix(map.crval1),2)
annocmd = annocmd + ' -bboxlly ' + strtrim(fix(map.crval2),2)
annocmd = annocmd + ' -bboxurx ' + strtrim(fix(map.crval1+map.naxis1),2)
annocmd = annocmd + ' -bboxury ' + strtrim(fix(map.crval2+map.naxis2),2)
annocmd = annocmd + ' -xfov ' + strtrim(fix(map.xfov),2)
annocmd = annocmd + ' -yfov ' + strtrim(fix(map.yfov),2)
annocmd = annocmd + ' -observatory ' + strtrim(map.obsrvtry,2)
annocmd = annocmd + ' -telescope ' + strtrim(map.obsrvtry,2)
annocmd = annocmd + ' -instrument ' + strtrim(map.instrume,2)
annocmd = annocmd + ' -starttime ' + strtrim(sttim,2)
annocmd = annocmd + ' -stoptime ' + strtrim(entim,2)
annocmd = annocmd + ' -channels ' + strtrim(map.wavelnth,2)

; global = ''
; TODO? get these lists from Annotator README (single point maint...)
; TODO? allow shorter synonyms for all keywords/options
; booleans=str2arr([as yet unspecified])
; params=str2arr([as yet unspecified])

; if data_chk(_extra, /struct) then begin ; process global options (inheritance) 
;   tns=tag_names(_extra)
;   ltns=strlowcase(tns)
;   for i=0,n_tags(_extra)-1 do begin
;     case 1 of 
;       is_member(ltns(i),booleans): global=global+' -'+ ltns(i)
;       is_member(ltns(i),params): $
;         global=global+' -'+ ltns(i) + ' ' + strtrim(_extra.(i),2)
;       else: box_message,'Don"t recognize global option>' + ltns(i)
;     endcase
;   endfor
; endif

; annocmd = annocmd + ' ' + global + ' '

; for i=0,n_params()-1 do begin 
;   estat=execute('fn=fitslist'+pn(i))
;   if n_elements(fn) eq 1 and file_exist(fn(0)) and $
;     (1-valid_fits(fn(0))) then fn=rd_tfile(fn(0))  ; file-list file
;   lists(i)=arr2str(fn," ")
; endfor
; lists=' '+lists

annocmd=strcompress(annocmd)

if keyword_set(showcommand) then begin 
  prstr,['----------------', '', ' Annotator Command>> ', strrep_logenv(annocmd,'SSW_HEK_ANNOTATOR'), '', '---------------']
endif

if spawnit then begin 
  uname_hek = ''
  read, 'Enter valid HEK username: ', uname_hek
  pw_hek = ''
  read, 'Enter password for the username: ', pw_hek
  set_logenv, 'HKB_USERNAME', uname_hek
  set_logenv, 'HKB_PASSWORD', pw_hek
  anno_output = ''
  espawn, annocmd, anno_output, background=background
  if anno_output(0) ne '' then more, ['--- output from annotator ---', anno_output]
endif
 
if debug then stop 

return
end
