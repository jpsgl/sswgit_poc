
function xcen2xcen, index, data

rsun_au = (1.38d6/2.0) / 149.60d6

map = mk_secchi_map(index, data)
sun_data = get_sun(anytim(map.time,/yoh), dist=dist, sd=sd)

r0_sol_radii = ((sd/map.rsun)*dist) / rsun_au

; Create normalized (rsun=1) Cartesian corrdinates:
yn = map.xc/map.rsun
zn = map.yc/map.rsun
temp = yn*yn + zn*zn
xn = sqrt(1 - temp)
; Convert to local view spherical coordinates using local b0, p angles:
rtp0 = c2s([xn,yn,zn], b0=-map.b0, roll=-map.roll_angle)
; Local view lat and lon:
lat = rtp0[1]
lon = rtp0[2]
; Earth-view lat and lon:
lat_earth = lat
lon_earth = lon + map.l0
; Earth-view heliographic:
xy_arcmin = hel2arcmin(lat_earth, lon_earth, date=map.time)
xcen_earth = xy_arcmin[0]
ycen_earth = xy_arcmin[1]

return, [xcen_earth, ycen_earth]*60.

end
