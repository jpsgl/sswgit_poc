function ssw_hpkb_hcrjson2struct, hcrjson, status=status, loud=loud
;
;+
;   Name: ssw_hpbk_hcrjson2struct
;
;   Purpose: HPKP Coverage Dbase - query response json->IDL structure
;
;   Input Paramters:
;      ssw_hpkb_hcrjson2struct
;
;   Keyword Parameters:
;      status - returns 1 if ~ok, 0 otherwise
;
;   History;
;       Circa March 2009, S.L.Freeland
;       10-sep-2009 - S.L.Freeland - minimal error checking add STATUS output
;                     fixed an off by one hiccup
;-
loud=keyword_set(loud)
results=where(strpos(hcrjson,'"results"') ne -1,rcnt)
status=rcnt gt 0 and n_elements(hcrjson) gt 6
if not status then begin 
    if loud then box_message,'No valid json structures'
    return,''
endif

valid=hcrjson(results(0)+2:n_elements(hcrjson)-5)
valid=strtrim(valid,2)
valid=valid(where(strmid(valid,0,1) eq '"'))
valid=strextract('{'+valid,'{','}')

cols=str2cols(valid,'",',/unal,/trim)
ntags=data_chk(cols,/nx)
nvals=data_chk(cols,/ny)
tagnames=strarr(ntags)

temp='t'+strtrim(indgen(ntags),2)

create_struct,stemp,'',temp,arr2str(replicate(strtrim(nvals>1,2)+'A',ntags))
for i=0,ntags-1 do begin 
   if is_member(tagnames(i),'description,purpose') then stop,tagnames(i)
if i eq 2 then stop,'i=2
   tnames=ssw_strsplit(reform(cols(i,*)),':',tail=values)
   ssc=where(strmid(tnames,0,1) eq ',',ccnt)
   if ccnt gt 0 then tnames(ssc)=strmid(tnames,1,strlen(tnames(0)))
   tnames=strtrim(tnames,2)
   tagnames(i)=tnames(0)
   values=strtrim(values,2)
   stemp.(i)=temporary(values)
endfor
create_struct,retval,'',tagnames,arr2str(replicate(strtrim(nvals>1,2)+'A',ntags))
for i=0,ntags-1 do retval.(i)=stemp.(i) 

retval=ssw_flatten_vecttags(retval) ; str.tag[NN] -> str[NN].tag

return,retval
end



