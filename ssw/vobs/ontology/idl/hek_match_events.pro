pro hek_bounds,event,bbox,center,timespan,box_center=box_center
; extract details of bounding box, center and timespan from a hek
; event
; center
  
  if (where(tag_names(event) eq 'EVENTID') gt 0) then begin ; an HCR event type
     
     bbox=transpose(reform([event.xcen+0.5*event.xfov*[-1,1],event.ycen+0.5*event.yfov*[-1,1]],2,2)) ;bounding box
     center=[event.xcen,event.ycen]
     
     timespan=[event.starttime,event.stoptime]
     
  endif else begin              ; an HER event type
     case 1 of
        event.required.event_coordsys eq "UTC-HGS-TOPO": begin
           bbox=60.*hel2arcmin([event.required.boundbox_c2ll,event.required.boundbox_c2ur], $
                               [event.required.boundbox_c1ll,event.required.boundbox_c1ur],date=event.required.event_starttime) ;bounding box
           ;bbox=transpose(bbox)
           print,bbox
           if keyword_set(box_center) then $
              center=0.5*(total(bbox,2)) else $
                 center=60*hel2arcmin(event.required.event_coord2,event.required.event_coord1,date=event.required.event_starttime)
        end
        event.required.event_coordsys eq "UTC-HPC-TOPO": begin
           bbox=[[event.required.boundbox_c1ll,event.required.boundbox_c2ll],[event.required.boundbox_c1ur,event.required.boundbox_c2ur]] ;bounding box
           if keyword_set(box_center) then $
              center=0.5*(total(bbox,2)) else center=[event.required.event_coord1,event.required.event_coord2]
        end
        else: print,'unknown coordinate system'
     endcase
     
     timespan=[event.required.event_starttime,event.required.event_endtime]
  endelse
  return
end

function hek_match_events,event,radius,trange,target=target,instrument=instrument,url=url,type=type,count=count,tshift=tshift,query=querystring,maxcount=maxcount
; finds macthing events in target registry
; INPUTS:
;  event -- hek event structure
;  radius -- size of search box (not really a radius!) in arcseconds
;  trange -- size of search interval (seconds)
; OUTPUTS
;   eventlist structure
; KEYWORDS
;  target -- HCR or HER
; instrument -- Instrument name for HCR
; url -- url created for query
; type -- HER event type (e.g. 'fl','ar'...)
; count -- number of matching events
; tshift -- timeshift of search window in seconds
; querystring -- returns resulting querystring
; maxcount -- maximum number of events to return
;
; AUTHOR
;   Neal Hurlburt, LMSAL 2011
;
if not keyword_set(instrument) then instrument=''
if keyword_set(maxcount) then maxcount=strtrim(maxcount,2) else maxcount='200'
hek_bounds,event,bbox,center,timespan
tcenter=(anytim(timespan(0))+anytim(timespan(1)))/2.0
if trange eq 0.0 then trange=anytim(timespan(1))-anytim(timespan(0))
if keyword_set(tshift) then tcenter=tcenter+tshift; offset search
if radius eq 0.0 then begin; use bounding box
   fovx=(bbox(0,1)-bbox(0,0))/2.
   fovy=(bbox(1,1)-bbox(1,0))/2.
endif else begin
   fovx=radius
   fovy=radius
endelse
case strup(target) of
   'HCR': begin
      querystring=ssw_hcr_make_query(tcenter-trange/2,tcenter+trange/2)
      querystring=querystring+'&xCenMin='+strtrim(center(0)-fovx,2)
      querystring=querystring+'&xCenMax='+strtrim(center(0)+fovx,2)
      querystring=querystring+'&yCenMin='+strtrim(center(1)-fovy,2)
      querystring=querystring+'&yCenMax='+strtrim(center(1)+fovy,2)
      querystring=querystring+'&instrument='+strtrim(strup(instrument))
      querystring=querystring+'&select=maxCadence&select=minCadence'
      result=ssw_hcr_query(querystring,count=count)
      print,'number of hits=',count
   end
   'HER' or 'her': begin
      if not keyword_set(type) then type='ar'
      ntypes=n_elements(type)
      querystring='cosec=2&&cmd=search&type=column&result_limit='+maxcount+'&event_type='+type+'&event_region=all&event_coordsys=helioprojective&x1='+strtrim(center(0)-radius,2)+'&x2='+strtrim(center(0)+radius,2)+'&y1='+strtrim(center(1)-radius,2)+'&y2='+strtrim(center(1)+radius,2)+'&event_starttime='+strtrim(anytim(tcenter-trange/2,/ccsds),2)+'&event_endtime='+url_encode(anytim(tcenter+trange/2,/ccsds))
;      print,querystring
      result=ssw_her_query(querystring,/struct4)
      types=str_sep(type,',')
      ntypes=n_elements(types)
      count=lonarr(ntypes)
      for i=0,n_elements(types)-1 do begin
         status=execute('count('+strtrim(i,2)+')=n_elements(result.'+types(i)+')') 
         if status eq 0 then print,'number of hits for type=',types(i),' is ',count(i)
      endfor
   end
endcase
return,result
end

