function ssw_round_times,itimes, hour=hour, second=second, out_style=out_style

ecs=anytim(itimes,/ecs) ; yyyy-mm-dd

hour=keyword_set(hour)
second=keyword_set(second)

append=([':00:00',':00'])(second)

ext=([13,16])(second)

if n_elements(out_style) eq 0 then out_style='ccsds'
retval=anytim(strmid(ecs,0,ext) + append,out_style=out_style)

return,retval
end
