function ssw_hpkb_order_event, xlist
;
;+
;   Name: ssw_hpkb_order_event
;
;   Purpose: kindof forget, but integral part of ssw_her_query suite...
;
;   History:
;      Circa 1-jun-2010 - S.L.Freeland
;      19-feb-2011 - S.L.Freeland - rename variable 'list' for IDL V8.0 compatibility
;                    (and added this sparse doc header)
;-

results=where(strpos(xlist,'<result>' ) ne -1,rcnt)
eresults=where(strpos(xlist,'</result>')  ne -1, recnt)
dres=eresults-results

etss=where(strpos(xlist,'event_type') ne -1)
uet=uniq(xlist(etss),sort(xlist(etss)))  ; uniq Event Types
nlist=strarr(n_elements(xlist)) ; same dimensions
utypes=xlist(etss(uet))
ipos=0 ; next insertion point
allrecs=-1
for i=0,n_elements(utypes)-1 do begin
  sse=where(xlist(etss) eq utypes(i), recnt)
  for r=0,recnt-1 do begin ; I'll eventually vectorize this (but at least by <result> chunks...
     rss=where(etss(sse(r)) ge results and etss(sse(r)) le eresults,cnt)
     allrecs=[temporary(allrecs),rss]
     thisr=xlist(results(rss):eresults(rss)      )
     nlist(ipos)=thisr
     ipos=ipos+(n_elements(thisr))
  endfor
endfor

return,nlist
end
