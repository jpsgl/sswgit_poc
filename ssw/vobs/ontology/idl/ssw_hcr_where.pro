function ssw_hcr_where,hcr,patterns, _extra=_extra, $
   taglist=taglist, count=count, debug=debug, $
   verbatim=verbatim, no_fold_case=no_fold_case
;+
;   Name: ssw_hcr_where
;
;   Purpose: find HCR records with tag(s) matching pattern(s)
;
;   Input Paramters:
;      hcr - vector of one HCR structures (from ssw_hcr_query.pro)
;      patterns - vector of one or more strings to match; optionally, comma delimited string of patterns
;
;   Output:
;      function returns subscripts where(hcr.TAGLIST match PATTERNS)
;
;   Keyword Parameters:
;      taglist - one or more tags to include - default={obstitle,goal,sciobjectives}
;      count (output) - number of matches/returned 
;      verbatim - if set, use PATTERNS verbatim (expert users) - this is default if any wild characters are found in PATTERNS
;                 otherwise, This routine takes some liberty with white space 
;      no_fold_case - set this to make search case sensitive; default is case insensitive
;
;   Method:
;      concatenate all desired tags; then strmatch all PATTERNS 
;
;   History:
;      19-jun-2014 - S.L.Freeland - hcr helper
;      21-aug-2014 - S.L.Freeland - add /VERBATIM and /NO_FOLD_CASE - fixed logic flaw
;      26-aug-2014 - S.L.Freeland - allow PATTERNS = comma delimited string
;-

if n_elements(taglist) eq 0 then taglist='obstitle,goal,sciobjectives'
no_fold_case=keyword_set(no_fold_case)
debug=keyword_set(debug)

count=0

if not required_tags(hcr,taglist) then begin
   box_message,'Need HCR structure vector and/or valid TAGLIST'
   return,-1
endif

if n_params() lt 2 then begin 
   box_message,'Need to specify one or more PATTERNS
   return,-1
endif

nhcr=n_elements(hcr)
hstrings=strarr(nhcr)

tags=str2arr(arr2str(taglist)) ;

patts=patterns
if n_elements(patts) eq 1 and strpos(patts[0],',') ne -1 then patts=strtrim(str2arr(patts),2) ; comma delimited list
wcs=where_pattern(patts,'*',count)
verbatim=keyword_set(verbatim) or count gt 0

if not verbatim then begin 
   patts='*' + str_replace(strtrim(patts,2),'*',' ') + '*'
   patts=str_replace(patts,' ','*') 
endif

for t=0,n_elements(tags)-1 do begin
   hstrings=hstrings + ' ' + hcr.(tag_index(hcr,tags[t]) )
endfor
if debug then stop,'hstrings'

retval=lonarr(nhcr)

for p=0,n_elements(patts)-1 do begin 
   retval=retval or strmatch(hstrings,patts[p],fold_case=1-no_fold_case)
endfor

found=where(retval,count)

if count eq 0 then retval=-1 else retval=found




return,retval
end 

;
;   


