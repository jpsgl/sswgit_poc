pro ssw_cutout_service,starttime, endtime, cgiquery, queryout, ref_helio=ref_helio, ref_time=ref_time, fovx=fovx, fovy=fovy, $
   _extra=_extra, waves=waves, email=email, instrument=instrument, hcr=hcr, goes_movie=goes_movie, movie=movie, nosubmit=nosubmit, $
   testcgi=testcgi, jsocurl=jsocurl
;
;+  
;
;   Name: ssw_cutout_service
;
;   Purpose: compose and issue a query to the ssw "cutout service"
;
;   Input Paramters:
;      (see api: http://www.lmsal.com/solarsoft//ssw_service/ssw_service_track_fov_api.html )
;      starttime - starttime of range -or- center time of window
;      endtime - endtime of range
;
;   Output Parameters:
;      cgiquery - the full query implied by This call (and submitted unless /NOSUBMIT is set)
;      queryout - output from cgi query when queued (undefined if /NOSUBMIT is set)
;
;   Keyword Parameters:
;      ref_helio - reference coord in Heliographic coords like N23W75
;      ref_time - time UT associated with REF_HELIO (ignored if NOTRACK=1)
;      fovx - field of view in X in arcseconds (def=1024)
;      fovy - field of view in Y in arcseconds (def=1024)
;      waves - list of desired wavelengths like : waves='171,211,304' (comma delimited list)
;              (see api html for instrument dependent list)
;      email - if user desires notification when 'job/data ready'
;      max_frames - maximum number of frames (per sequence/wave movie) - def=50
;      minute_window - in liue of ENDTIME, optional window around STARTTIME (in minutes)
;      instrument - currently, one of 'aia', 'eit', or 'mdi' (check api for updates...) 
;      data_level - optional reformatter/data level, if known
;      goes_movie - if set, append a GOES XRS plot, inclding movie cadence annotations
;      hcr - if set, register this in the HCR (make sure you know what this means...)
;      nosubmit - if set, construct but don't issue the cgiquery (return in cgiquery param)
;      testcgi - name of test cgi script - if switch, default='...track_fov2.sh'
;
;   History:
;      Circa fall 2010 - S.L.Freeland
;      26-apr-2011 - S.L.Freeland - add TESTCGI keyword and function
;       8-jan-2013 - S.L.Freeland - remove ssw_set_proxy references
;-

testing=keyword_set(testcgi)
topcgi='http://www.lmsal.com/cgi-ssw/'
cutout_service='ssw_service_track_fov.sh'
cutoutcgi='http://www.lmsal.com/cgi-ssw/ssw_service_track_fov.sh?'
if testing then begin 
   case 1 of 
      data_chk(testcgi,/string): begin 
         cutoutcgi=([topcgi,''])(strpos(testcgi,'http:') ne -1) + testcgi
      endcase
      keyword_set(testcgi): cutoutcgi=str_replace(cutoutcgi,'fov','fov2')
      else:box_message,'Unknown test??'
   endcase      
   box_message,'test cgi>> ' + cutoutcgi
endif
cutoutcgi=cutoutcgi+(['','?'])(strpos(cutoutcgi,'?') eq -1)

hcr=keyword_set(hcr)
goes_movie=keyword_set(goes_movie)
movie=keyword_set(movie) or hcr or goes_movie
notrack=keyword_set(notrack)

if n_elements(minute_window) eq 0 then minute_window=30
jsocorder=0
case 1 of 
   keyword_set(jsocurl): begin 
      jsocinfo=ssw_jsoc_order2info(jsocurl,status=status)
      if not status then begin 
         box_message,'problem with JSOC order info..., bailing with no action.
         return
      endif
      box_message,'JSOC setup...
      t0=jsocinfo.starttime
      t1=jsocinfo.endtime
      ref_time=jsocinfo.ref_time
     
      max_frames=jsocinfo.max_frames
      waves=jsocinfo.waves
      notrack=1 ; jsoc did this part...
      jsocorder=1
   endcase
   n_params() eq 1: begin
         time_window,starttime,t0,t1,minute=minute_window
   endcase
   n_params() ge 2: begin  
         t0=anytim(starttime,/ecs,/trunc)
         t1=anytim(endtime,/ecs,/trunc)
   endcase
   else: begin 
      if n_elements(ref_time) gt 0 then time_window,ref_time,t0,t1,minute=minute_window else begin 
         box_message,'Need a time, time range or at least REF_TIME to proceed... sorry
         return
      endelse
   endcase
endcase 

if n_elements(max_frames) then max_frames=50
if n_elements(fovx) eq 0 then fovx=1024
if n_elements(fovy) eq 0 then foxy=1024
case n_elements(waves) of
   0: swaves='171,211,304'
   1: swaves=waves
   else: swaves=arr2str(waves,/trim)
endcase
if n_elements(instrument) eq 0 then inst='aia' else inst=instrument

query=ssw_make_query(starttime=t0, endtime=t1, /queue_job,  movie=movie, waves=swaves, instrume=inst, $
    email=email, fovx=fovx, fovy=fovy, ref_helio=ref_helio, ref_time=ref_time, $
    hcr=hcr, goes_movie=goes_movie,_extra=_extra,jsocurl=jsocurl) 


cgiquery=cutoutcgi+query
if not keyword_set(nosubmit) then begin 
   box_message,['Submitting query:',cgiquery]
if get_logenv('check_movie') then stop,'cgiquery'
   sock_list,cgiquery,queryout,/cgi
endif

return
end 
