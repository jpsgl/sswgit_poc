pro mreadjp2000, jp2files, index, data, xll, yll, nx, ny, _extra=_extra, outsize=outsize
;
;+
;   Name: mreadjp2000
;
;   Purpose: read one or more jpeg2000 -> "index [,data]" (mreadfits analog)
;
;   Input Parameters:
;      jp2files - vector of jpeg2000 filename(s)
;
;  Output Parameters
;      index - jpeg2000 meta data -> sswidl structures
;      data - optional data arrays (not implented until later this aftoernoon...)
;
;  Keyword Parameters:
;      _extra - undefined keywords -> read_jpeg2000 via inheritance
;      discard_levels,max_layers,order,region
;
;  History:
;      25-sep-2012 - S.L.Freeland - mreadfits.pro analog for jp2000 (jhelioviewer style)
;
;  Restrictions:
;     May only work on FITS->JP2000 as converted by jhelioviewer engine
;     jp2meta->structures ("index") via ssw_jp20002struct function; this is SSW SOP wrapper
; 
;-

case 1 of 
   n_params() lt 2: begin 
      box_message,'Need at least file(s) and one output ("index") variable; or why bother?
      return
   endcase
   1-file_exist(jp2files(0)): begin 
      box_message,'Cannot find first file, so bailing...
      return
   endcase
   else:
endcase

index=ssw_jp20002struct(jp2files)

if n_params() gt 2 then begin ; user wants data also
   nii=n_elements(index)
   img0=read_jpeg2000(jp2files(0),_extra=_extra) ; first is template
   data=make_array(data_chk(img0,/nx),data_chk(img0,/ny),nii,type=data_chk(img0,/type))
   data(0,0,0)=temporary(img0)
   for i=1,nii-1 do begin  ; do the rest
      data(0,0,i)=read_jpeg2000(jp2files(i),_extra=_extra)
   endfor
   mreadfits_fixup,index ; adjusts crpixN et al if rebinned
endif

return
end
   

