function ssw_jsoc_keywords2struct, jsoc_info, debug=debug
;
;+
;   Name: ssw_jsoc_keywords2struct
;
;   Purpose: map from JSOC info (keyword+type definitions) -> corresonding IDL structure 
;
;   Input Parameters:
;      jsoc_info - ouput of ssw_json2struct(json_info) - or <preceding>.KEYWORDS
;   
;   Output:
;      function output is IDL structure equivilent ; tags are 1:1 to input NAME, type:<keywords>.type
;
;   Calling Sequence:
;      IDL> jsoc_keywordstruct=ssw_jsoc_keywords2struct(jsoc_info)
;
;   Calling Example:
;      IDL> help,jsoc_info.keywords & help,jsoc_info.keywords,/str  ; show what input looks like... (jsoc info)
; <Expression>    STRUCT    = -> <Anonymous> Array[73]
;** Structure <1d4a424>, 4 tags, length=48, data length=48, refs=2:
;   NAME            STRING    'ORIGIN'
;   TYPE            STRING    'string'
;   UNITS           STRING    'none'
;   NOTE            STRING    'ORIGIN Location where file made'
;
;   IDL> jsoc_keywordstruct=ssw_jsoc_keywords2struct(jsoc_info)
;   IDL> help,jsoc_keywordstruct,/str
;** Structure <2144804>, 73 tags, length=416, data length=394, refs=1:
;   ORIGIN          STRING    ' '
;   DATE            STRING    ' '
;   DATE__OBS       STRING    ' '
;   T_OBS           STRING    ' '
;   EXPTIME         DOUBLE           0.0000000
;   TIME            DOUBLE           0.0000000
;   MJD             DOUBLE           0.0000000
;   TELESCOP        STRING    ' '
;   INSTRUME        STRING    ' '
;   CAMERA          INT              0
;   WAVELNTH        FLOAT           0.00000
;   (...etc...)
;
;   Motivation:
;      Alternate option for JSOC JSON->IDL structure mapping (via 'reads')
;
;   History:
;       25-feb-2009 - S.L.Freeland - overcome limitations in direct structure execution (json->idl)   
;       29-jan-2010 - S.L.Freeland - data type int->long (like FSN..)
;        9-sep-2010 - S.L.Freeland - check/guess type for data type='link'(?)
;       15-jul-2013 - S.L.Freeland - longlong -> create_struct 'K' (was J) - long64
;       29-jul-2013 - S.L.Freeland - use create_struct_temp.pro (avoid stupid crash in create_struct)
; 
;-
debug=keyword_set(debug)


case 1 of 
   required_tags(jsoc_info,/keywords):  keywords=jsoc_info.keywords ; input is jsoc_info
   required_tags(jsoc_info,'name,type'):  keywords=jsoc_info          ; input is jsoc_info.keywords 
   else: begin ; input is wrong...
      box_message,'Expect jsoc info structure via ssw_json2struct including KEYWORDS info
      return,-1
   endcase
endcase

tagnames=gt_tagval(keywords,/name)
tagtypes=gt_tagval(keywords,/type)

ntags=n_elements(tagnames)
ntypes=n_elements(tagtypes)

if ntags ne ntypes then begin ; should Never happen, so it will
   box_message,'Mismatch #TAGS #TYPES'
   return,-1
endif

cstypes=strarr(ntags)

jsoctype=str2arr('short,int,long,longlong,float,double,string,time')
cstype  =str2arr('J,J,J,K,F,D,A,A') ; longlong -> K slf 15-jul-2013

for i=0,n_elements(jsoctype)-1 do begin 
   ss=where(tagtypes eq jsoctype(i),sscnt)
   if sscnt gt 0 then cstypes(ss)=cstype(i) 
endfor

bad=where(cstypes eq '', bcnt)
if bcnt gt 0 then begin 
   box_message,'attempting data type for "link" types...'
;  Default is double Except for explicit string type list - extend as needed'
   stypes=str2arr(strupcase('instrume,camera,origin,wcsname,cunit1,cunit2,ctype1,ctype2'))
   cstypes(bad)='D'  ; defult=float
   sss=where_arr(tagnames(bad),stypes)
   if sss(0) ne -1 then cstypes(bad(sss))='A'
endif

create_struct_temp,retval,'',tagnames,cstypes ; create the struct
if get_logenv('keycheck') ne '' then stop,'tagnames,cstypes
if debug then stop

return,retval
end
