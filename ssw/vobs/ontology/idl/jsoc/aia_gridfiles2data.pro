pro aia_gridfiles2data, gfiles, dw0, dw1, dw2, dw3, dw4, dw5, dw6, dw7, dw8, fovpix=fovpix, debug=debug
;
;   Name: aia_griidfiles2data
;
;   Purpose: output from ssw_aia_gridfiles -> data cube(s), optionally user subfield
;
;   Input Paramters:
;      gridfiles - structure output from ssw_aia_gridfiles
;
;   Output Parameters:
;      dw1 [, dw2, dw3, ... dw8] - data cubes
;
;   Keyword Paramters:
;      fovpix - optional subfield; 4 element [llx, lly, nx, ny] in pixels 
;               (per mreadfits_shm; llx & lly  may be vectors 1:1 filelists
;
;   Calling Context/Example:
;      IDL> gridfiles=ssw_aia_gridfiles(t0,t1, waves='171,211,304', sec_grid=300)
;      IDL> aia_gridfiles2data, gridfiles, w171, w211, w304 , fovpix=[500,2000,640,500] 
;
;   History:
;      7-apr-2010 - S.L.Freeland - testing some idl-idl bridge possibilites; 
;                   In this case, multi-thread file reads via mreadfits_shm
;-
box_message,'This is primarilly a demo of idl_idl bridge for benchmarking multi-thread mreadfits_shm
debug=keyword_set(debug)

if not required_tags(gfiles,'t0,t1,xfiles') then begin 
   box_message,'Require structure output from ssw_aia_gridfiles.pro'
   return
endif

if data_chk(gfiles,/struct) then begin 
   fov=''
   if n_elements(fovpix) eq 4 then fov=','+arr2str(fovpix,/no_dup,/trim) 
   tnames=tag_names(gfiles)
   ss=where(strpos(tnames,'WLIST') eq 0)
   waves=str2number(tnames(ss)) ; assume at least one list...
   swaves=strtrim(waves,2)
   nw=n_elements(swaves)
   allobs=objarr(nw)
   for i=0,nw-1 do begin ; one thread per wave 
      box_message,'creating bridge for wave> ' + swaves(i)
      allobs(i)=obj_new('idl_idlbridge') ; make a new bridge/thread
      allobs(i)->setvar,'files',gfiles.(tag_index(gfiles,'WLIST'+swaves(i))) ; use that for This wavelist
      allobs(i)->execute,'mreadfits_shm,files,index,data'+fov,/nowait ; background an mreadfits_shm thread
   endfor
   lastbridge=last_nelem(allobs)
   status=lastbridge->status(error=error)
   while status lt 2 do begin 
      box_message,'waiting for final read to complete...' ; ?? should check All threads, but prototype for now
      wait,.2
      status=lastbridge->status(error=error)
   endwhile
   for i=0,nw-1 do estat=execute('dw'+ strtrim(i,2)+'=allobs(i)->getvar("data")') ; getvar -> output params 
   estat=execute('help,out=out,'+ arr2str('dw'+strtrim(sindgen(3),2)))
 
   box_message,['Your data arrays for waves> '+ arr2str(swaves) + '...',out]
   delvarx,allobs ; object cleanup
endif else box_message,'need to define "gfiles" via ssw_aia_gridfiles.pro'

if debug then stop
return

end
   
