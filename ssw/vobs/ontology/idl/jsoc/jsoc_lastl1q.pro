pro jsoc_lastl1q, ds, nimages

top='$SSWDB/sdo/'
ssw_last_jsoc,ds,index,lastn=nimages

instrume=strlowcase(gt_tagval(index,/instrume))
index=jsoc_index2ssw(index)
index.instrume=instrume
inst=ssw_strsplit(instrume,'_',/head)
stop,'inst
topq=concat_dir(concat_dir(top,inst),'last_l1q')

fnames=instrume+'_'+time2file(index,/sec)+'.fits'
outfits=concat_dir(topq,fnames)

fexist=file_exist(outfits)

need=where(1-fexist,ncnt)
if ncnt gt 0 then begin 
   ssw_last_jsoc,ds,index,data, lastn=ncnt
   instrume=strlowcase(gt_tagval(index,/instrume))
   index=jsoc_index2ssw(index)
stop,'fnames'
   mwritefits,index,data,outfile=outfits
endif else box_message,'No new files since last run

return
end

