function ssw_jsoc_datajson2struct, jsonin, debug=debug

debug=keyword_set(debug) or get_logenv('jsoc_jsoncheck') ne ''
if debug then file_append,'datajson.dat',jsonin,/new

temp={record:'',filename:''}
json=web_dechunk(jsonin,/compress)
sjson=arr2str(json,'')
sjson=strextract(sjson,'{','"status":0}',/inc)

sdata=strextract(sjson,'[{','}]')
dpos=strpos(sjson,sdata)

remain=strmid(sjson,0,dpos) + $  
       strmid(sjson,strpos(sjson,'}]'),str_lastpos(sjson,'}'))
remain=str_replace(remain,'[{}]','""')

sarr=str2arr(sdata)


tn='"'+strlowcase(tag_names(temp))+'":"
nt=n_elements(tn)

ssrec=where(strpos(sarr,tn(0)) ne -1, rcnt)
if rcnt eq 0 then begin 
   box_message,'No data records??'
   return,-1
endif

data=replicate(temp,rcnt)

for i=0,nt-1 do begin 
   sst=where(strpos(sarr,tn(i)) ne -1, sscnt)
   data.(i)=strextract(sarr(sst),tn(i),'"')

endfor

retval=ssw_json2struct(remain) ; ~generic piece
retval=rep_tag_value(retval,data,'data') ; .data tag

return,retval
end






