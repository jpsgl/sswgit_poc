pro mreadfits_shm_multi, fitsfiles, index, data, llx, lly, nx, ny, $
   nthreads=nthreads, fovpix=fovpix, tilecomp=tilecomp
;
;+
;   Name: mreadfits_shm_multi
;
;   Purpose: multi thread analog of mreadfits_shm using idl-idl bridge
;
;   Input Parameters:
;      fitfiles - list (1d vector)  of fits files
;      llx,lly,nx,ny - optional subfield in pixels per mreadfits_shm
;                      (positional param equiv of FOVPIX keyword)
;
;   Output Parameters:
;      index [,data] - metadata [, optional data cube]
;
;   Keyword Parameters:
;     nthreads - number of read threads to use - default=4 
;     fovpix - optional subfield - 4 element vector [llx,lly,nx,ny] in pixels
;     tilecomp - switch - if set, use mreadfits_tilecomp (def=mreadfits_shm)
;
;   History:
;      8-apr-2010 - S.L.Freeland - idl-idl bridge applied to multi-thread fits
;
;   Restrictions:
;      /TILECOMP (e.g., for tile compressed FITS not implemented "yet")
;
;-
nf=n_elements(fitsfiles)
if n_elements(nthreads) eq 0 then nthreads=4 
nt = nthreads < nf ; don't need #threads>#files

tilecomp=keyword_set(tilecomp)
reader=(['mreadfits_shm','mreadfits_tilecomp'])(tilecomp)

fov=''
if n_elements(fovpix) eq 4 then fov=','+arr2str(fovpix,/no_dup,/trim)
allobs=objarr(nt)
lf=nf/nt
f0=indgen(nt)*lf
f1=f0+(lf-1) 
f1(nt-1)=nf-1 ; stick excess in last thread for simplicity

for i=0,nt-1 do begin
   files=fitsfiles(f0(i):f1(i))
   box_message,'Creating thread for files ' + $
      strcompress(arr2str([f0(i),f1(i)],'-'))
   allobs(i)=obj_new('idl_idlbridge')
   allobs(i)->setvar,'files',files
   allobs(i)->execute,reader+',files,index,data'+fov,/nowait ; backgnd reader
endfor 

lastbridge=last_nelem(allobs)
status=lastbridge->status(error=error)
while status lt 2 do begin
   box_message,'Waiting for last read thread to complete...'
   wait,.2
   status=lastbridge->status(error=error)
endwhile

data=allobs(0)->getvar('data')
for i=1,nt-1 do begin
   temp=allobs(i)->getvar('data')
   data=[[[temporary(data)]],[[temporary(temp)]]]
   delvarx,allobs(i) ; done with this
endfor

return
end




