pro ssw_last_jsoc, ds, index, data, filter=filter, menu=menu, lastn=lastn, $
   debug=debug, outdir_top=outdir_top, urls_only=urls_only, outsize=outsize, $
   keywordss=keywords, local_files=local_files
;+
;   Name: ssw_last_jsoc
;
;   Purpose: ssw_jsoc.pro demo - most recent N image "index,data" for data set
;
;   Input Parameters:
;      DS - desired data series name - default='aia.lev0'
;
;   Output Paramters:
;      index - index/indices of most recent N 
;      data - optional data ( -OR- urls if /URLS_ONLY set)
;
;   Keyword Parameters:
;      filter - filter for DS search (if explicit DS not supplied)
;      menu - if set, user DS selection from DS list
;      lastn - Number of index/data (default=1 aka, The Last)
;      urls_only - if set, output DATA are URLS, not data cube
;      keywordss - optional list of desired KEYS -> ssw_jsoc def=all
;      local_files - if set, assume JSOC/DRMS is Local (ssw_jsoc_files2data.pro)
;
;   Calling Sequence:
;      IDL> ssw_last_jsoc,DS,index[,data],[keywords=keylist'][,/urls_only]
;
;   Calling Examples:
;      IDL> ssw_last_jsoc,'aia.lev0',index,lastn=4      ; meta data only
;      IDL> ssw_last_jsoc,'hmi.lev0,index,data,lastn=2  ; index&data
;      IDL> ssw_last_jsoc,'aia.lev0',index,urls,/urls_only ; URLS ouput
;      IDL> ssw_last_jsoc,'hmi.lev0',index,data,lastn=5,/local_files ; @JSOC
;
;   24-Jun-2008 - S.L.Freeland - ~useful + help remember 'ssw_jsoc.pro' api
;    7-jul-2008 - S.L.Freeland - add /LOCAL_FILES - fast reads@local jsoc/sums
;
;   Comments:
;      Originally intended for ssw client remote access via JSOC http
;      When used at JSOC w/local DRMS/SUMS, use /LOCAL_FILES switch
;      (which uses memory mapped files for speed)
;-
debug=keyword_set(debug)
menu=keyword_set(menu)
op=''
if n_elements(ds) eq 0 then  begin ; if no series name input, get one...
   if keyword_set(menu) then begin 
      if n_elements(filter) eq 0 then filter='aia.lev0'
      dss=ssw_jsoc(service='show_series',filter=filter) ; <<<< SSW_JSOC
      if data_chk(dss,/struct) then begin 
          names=dss.names.name
          ss=xmenu_sel(get_infox(dss.names,'name,note'),/ONE)
          names=dss.names.name & ds=names(ss>0)
      endif else begin 
          box_message,'Nothing matching your filter...
      endelse
   endif else ds='aia.lev0'   ; default if none supplied and ~/MENU
endif

if debug then box_message,'serstr=ssw_jsoc(ds=ds,/series_struct)'
serstr=ssw_jsoc(ds=ds,/series_struct,debug=debug) ; <<< SSW_JSOC SERIES STRUCTURE
int=serstr.interval ; DS series structure
last=int.lastrecord

dsname=ssw_strsplit(last,'[',tail=lastnum)
lastnum=str2number(lastnum)
if keyword_set(lastn) then begin   ; user wants more than 1
   liststring=dsname+'['+arr2str([lastnum-lastn,lastnum],'-')+']
   liststring=strcompress(strtrim(liststring,2),/remove)
  ; testlist=ssw_jsoc(ds=liststring(0),/rs_list,key='FSN')
  ; ln=strtrim(last_nelem(testlist.fsn,lastn),2)
  ; liststring=dsname+'['+ln(0)+'-'+last_nelem(ln)+']'
endif else liststring=last ; verbatim <ds>[LASTRECNUM]
box_message,liststring

case n_elements(keywords) of 
   1:  keys=keywords ; user suplied - use verbatim 
   0:  keys=arr2str(serstr.keywords.name)
   else: keys=arr2str(keywords) ; user supplied keyword vector
endcase
keys=strcompress(strtrim(keys,2),/remove)
if debug then box_message,'list=ssw_jsoc(ds=liststring(0),/rs_list, key=keys)
if debug then stop,'keys,listring'
list=ssw_jsoc(ds=liststring(0),/rs_list, key=keys, serstr=serstr)
if list(0).status then begin 
   box_message,"Problem is /RS_LIST of DS="+liststring(0)
   stop,'list??'
endif
;index=ssw_jsoc_struct2index(list) ; JSOC struct -> SSW "index" struct
index=list ; jsoc_series_json2struct(list) ; now done in ssw_jsoc
nout=n_elements(index)

if n_params() ge 3 then begin ; a FETCH request
   local_files=keyword_set(local_files)
   urls_only=keyword_set(urls_only) 
   exp=ssw_jsoc(ds=liststring(0),/export) ; <<< SSW_JSOC /EXPORT
   topurl='http://jsoc.stanford.edu'
   files=exp.data.filename
   files=str_replace(files,'\','')
   urls=concat_dir(topurl,files)
   case 1 of 
      local_files: data=ssw_jsoc_files2data(files) 
      urls_only: data=urls
      else: begin 
      if n_elements(outdir_top) eq 0 then outdir_top=curdir()
      locfiles=concat_dir(outdir_top,files)
      break_url,urls,ip,paths,fnames
      outdirs=concat_dir(outdir_top,paths)
      for i=0,nout-1 do begin ; sock_copy OUT_DIR assumes scalar?
         mk_dir,outdirs(i)
         sock_copy,urls(i),out_dir=outdirs(i), progress=progress
      endfor
      if total(file_exist(locfiles)) eq nout then begin
          mreadfits,locfiles,dummy,data,outsize=outsize
          index=join_struct(dummy,temporary(index))
      endif else begin
         box_message,'not all files found...
      endelse
      endcase
   endcase
endif

if debug then stop,'before return
return
end


