function ssw_imcopy,infile, outdir=outdir, error=error, outname=outname, $
   debug=debug, compress=compress, add_imcopy=add_imcopy, force_shell=force_shell, $
   in_situ=in_situ
;+   
;   Name: ssw_imcopy
;
;   Purpose: run imcopy on input file
;
;   Input Parameters:
;      infile - input file
;
;   Output:
;      returns output file name or "" if error
;
;   Keyword Parameters:
;      compress - if set, compress (assume input is UNcompressed)
;      add_imcopy - if string, append to output name- if switch, append '_imcopy' or '_compress' to to output file names
;      in_situ - if set, use OUTDIR=INDIR
;
;   Circa August 2010 - S.L.Freeland
;   18-jan-2011 - removed dependency on ssw_bin (wich I should rewrite...)
;   11-dec-2012 - use ssw_bin_path - made /NOSHELL default (use /FORCE_SHELL to use old)
;    2-dec-2013 - fix typo/truncated line 'imc' (?)
;   
;   pre-remove output file to avoid imcopy collision
;
;-

error=1
if not file_exist(infile) then begin 
   box_message,'Need input file...'
   return,''
endif

break_file,infile,lll,ppp,fff,vvv,eee

in_situ=keyword_set(in_situ)

if keyword_set(in_situ) then outdir=([curdir(),ppp(0)])(ppp ne '')
if n_elements(outdir) eq 0 then outdir=get_temp_dir()

imcopy=ssw_bin_path('imcopy',/ontology,found=found)
if ~found then  begin 
   box_message,'Cannot find IMCOPY for this os/arch... bailing.
   return,''
endif

compit=keyword_set(compress)

compress=(["'","[compress]'"])(compit)

case 1 of 
   data_chk(add_imcopy,/string): imc=add_imcopy
   compit: imc='_compress'
   outdir eq ppp or keyword_set(add_imcopy) : imc=(['_imcopy','_compress'])(compress)
   else:imc=''
endcase
if n_elements(outname) eq 0 then outname=fff+ imc +vvv+eee

debug=keyword_set(debug)
outfile=concat_dir(outdir,outname)

imcmd=imcopy +" '" + infile + "' '" + outfile + compress
noshell=1-keyword_set(force_shell)
if noshell then imcmd=str2arr(str_replace(imcmd,"'",''),' ')


if debug then stop,'imcmd'
spawn,imcmd, out, noshell=noshell
if out(0) ne '' then box_message,['Output from imcopy...',out]
if keyword_set(debug) then stop,outfile

retval=outfile
error=1-file_exist(retval)

if in_situ and file_exist(outfile) then begin 
   box_message,'renaming'
   box_message,['Before/After',file_size([infile,outfile],/auto,/string)]
   ssw_file_delete,infile
   spawn,['mv',outfile,infile],/noshell
   retval=infile
endif

return,retval
end
