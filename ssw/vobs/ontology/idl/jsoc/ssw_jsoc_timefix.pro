function ssw_jsoc_timefix, index

;+
;   Name: ssw_jsoc_timefix
;
;   Purpose: clean a a couple of hiccups & DATE__OBS -> DATE_OBS (ssw)
;
;   
;
if not data_chk(index,/struct) then begin 
   box_message,'Require "JSOC Structure" vector'
   return,-1
endif
retval=index

ttags=str2arr('t_obs,date__obs,date,t_obs,ISPPKTIM,IMGFPT')
tind=tag_index(retval,ttags)
chk=where(tind ge 0,ncnt)
for i=0,ncnt-1 do begin  ; time tags which exist in This input structure
   tt=strtrim(retval.(tind(chk(i))),2)
   fst=strmid(tt,0,1)
   yy=(['1','2'])(fst eq '0')
   retval.(tind(chk(i))) =(['',yy])(fst ne '1' and fst ne '2') + tt
endfor

if not required_tags(index,'DATE__OBS') then begin 
   box_message,'Input has already been Fixed   
endif else retval=rep_tag_name(retval,'DATE__OBS','DATE_OBS')

return,retval
end
