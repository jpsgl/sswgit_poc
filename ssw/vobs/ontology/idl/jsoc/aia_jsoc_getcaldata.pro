pro aia_jsoc_getcaldata, index, indexout, dataout, $
   badpix=badpix, spikes=spikes, flat=flat, dark=dark, $
   outdir=outdir, uncomp_file=uncomp_file, refresh=refresh, $
   dseries=dseries
;+
;   Name: aia_jsoc_getcaldata
;
;   Purpose: return selected input image or cal data associated with input index
;
;   Input Parameters:
;      index - input L1 or L1.5 index record of interest
;
;   Output Parameters:
;      indexout - corresponding Lev-1 or calibration "header" 
;      dataout  - coreesponding Lev-1 or calibration "data"
;
;   Keyword Parameters:
;     badpix - if set, copy/uncompress/return badpixel array
;     spikes - if set, ditto for spikes files
;     dseries - optional data series - default='aia.lev1'
;
;   History:
;      31-aug-2010 - S.L.Freeland - prototype using jsoc client (ssw_jsoc.pro)
;      20-jan-2011 - S.L.Freeland - add DSERIES keyword and change
;                                   default aia_test.lev1 -> aia.lev1
;      15-may-2012 - S.L.Freeland - add wave filter for duplicate jsoc T_REC
;
;   Restrictions:
;      as of today, only /badpix, /spikes and default 1.5 index->L1 index,data
;-

common aia_jsoc_getcaldata_blk, ltobs, lindout, lexp

if n_params() lt 1 then begin 
   box_message,'Need input index or T_OBS
   return
endif

if data_chk(index,/struct) then tobs=gt_tagval(index,/t_obs) else $
  tobs=index

if n_elements(ltobs) eq 0 then ltobs=anytim('1-jan-2010',/ccsds)

if n_elements(dseries) eq 0 then dseries='aia.lev1' ; test->rel 20-jan-2011

case 1 of 
   keyword_set(dark): begin
      box_message,'Darks not handled yet'
   endcase
   keyword_set(flat): begin 
      box_message,'Flats not handled yet'
   endcase
   else: begin 
      case 1 of
         keyword_set(badpix): segment=1
         keyword_set(spikes): segment=2
         else: segment=0 ; image
      endcase
      query=dseries+'['+anytim(tobs,/ccsds)+']'
      need=ssw_deltat(ltobs,ref=tobs,/sec) ne 0 or keyword_set(refresh)
      if need then begin 
         indexout=ssw_jsoc(/rs_list,ds=query,/loud)
         exp=ssw_jsoc(ds=query,/export,/loud)
         if n_elements(indexout) gt 1 then begin 
            box_message,'correcting multi-jsoc entries'
            ssw=where(index.wavelnth eq indexout.wavelnth,wcnt)
            if wcnt ne 1 then begin 
               box_message,'No wavelnth match(??), have to abort..'
               return
            endif
            indexout=indexout(ssw)
            if get_logenv('check_multi') ne '' then stop,'exp.data'
            exp=rep_tag_value(exp,exp.data(ssw*3:ssw*3+2),'data')
         endif 
         ltobs=tobs
         lindout=indexout
         lexp=exp
      endif else begin 
         box_message,'Same index as last, reusing jsoc stuff...'
         indexout=lindout
         exp=lexp
      endelse

      if required_tags(exp,'data') then begin 
         urls=ssw_jsoc_export_struct2files(exp)
         urls=urls(segment)
         fname=ssw_strsplit(urls,'/',/tail)
         if n_elements(outdir) eq 0 then outdir=curdir()
         lfile=concat_dir(outdir,fname)
         ssw_file_delete,lfile ; start with clean slate
         box_message,'Copying> ' +urls
         sock_copy,urls,out_dir=outdir, progress=(segment eq 0)
         if segment eq 0 then begin 
             read_sdo,lfile,indexout,dataout,outdir=outdir,/use_index
         endif else begin 
             oname=ssw_jsoc_index2filenames(indexout)
             oname=str_replace(oname,'.fits','_'+fname)
             uncomp=concat_dir(outdir,oname)
             if not file_exist(uncomp) then $
                 uncomp=ssw_imcopy(lfile,outname=oname,outdir=outdir)
             uncomp_file=uncomp
             dataout=mrdfits(uncomp,1) ; first extension holds stuff
         endelse
      endif else box_message,'Could not export as expected...
   endcase
endcase

return
end

