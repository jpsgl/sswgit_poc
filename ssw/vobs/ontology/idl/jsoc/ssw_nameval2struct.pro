function ssw_nameval2struct, namvalin, template, debug=debug 
;
;+
;   Name: ssw_nameval2struct
;
;   Purpose: name/value json -> IDL structure, optionally typing per TEMPLATE
;
;   Input Paramters:
;      namvalin - json "name":<blah>,"values":[blah1,blah2,blah3....blahn]
;
;   Output:
;      function returns IDL structure vector; typing per TEMPLATE if supplied
;
;   History:
;      Circa March 2009 - S.L.Freeland JSOC/JSON helper function
;      17-Mar-2009 - S.L.Freeland - namvalin names subset of TEMPLATE 
;                    (for KEY subset in ssw_jsoc(/rs_list... call)
;      12-aug-2009 - S.L.Freeland - deal with non string w/ "[MISSING]"
;                    add HEX list workaround (QUALITY) - maybe temporary
;      18-feb-2010 - S.L.Freeland - eliminate an execute statement
;                    and massaged a few things - (firehose preparation...)
;       2-jul-2010 - S.L.Freeland - fixed a subscripting error (caused some mismatched TAG:VALUE)
;      22-jul-2010 - S.L.Freeland - protect against a few not yet handled hmi tags
;       9-jun-2011 - S.L.Freeland - skip mismatched (TODO - look at this...)
;      11-jul-2011 - S.L.Freeland - account for new JSOC 'runtime' bookeeping info
;      19-aug-2011 - S.L.Freeland - rationalize HEX translations a bit
;       6-nov-2012 - S.L.Freeland - vectorize MISSING logic for non-string types (which also fixed a bug...)
;      18-jul-2013 - S.L.Freeland - eliminated some (now) useless messages
;
;-

namval=''
for i=0,n_elements(namvalin)-1 do namval=namval+namvalin(i)

debug=keyword_set(debug)

nstring='"name":'
vstring='"values":'
nlen=strlen(nstring)
vlen=strlen(vstring)

names=where_pattern(namval,nstring,ncount)
values=where_pattern(namval,vstring,vcnt)

if ncount ne vcnt then begin 
   box_message,'mismatch name:value
   stop
endif

ss=where(values lt names, dcnt)
if dcnt gt 0 then begin
   box_message,'name:value position issue?
   stop
endif

nn=strarr(ncount)
vv=nn

tnamval=strmid(namval,strlen(namval)-50,50) 
if strpos(tnamval,'"runtime"') ne -1 then begin 
   count=str2number(strextract(namval,'"count":',',"runtime":')) ; #records
   runtime=str2number(strextract(namval,'"runtime":',',"status":'))
endif else count=str2number(strextract(namval,'"count":',',"status":')) ; #records
status=str2number(strextract(namval,'"status":','}'))

dnv=values-names

vlim=shift(names,-1) - values - nlen - 5

for i=0,ncount-1 do begin 
   nn(i)=strmid(namval,names(i)+nlen+1,dnv(i)-(vlen+1))
   vv(i)=strmid(namval,values(i)+vlen, vlim(i))
endfor
; rationalize last element..
term=(['}',']}'])(count gt 0) ; scalar/arry fixup
temp=strmid(namval,values(i-1)+vlen,strlen(namval))
vv(i-1)=strmid(temp,0,strpos(temp,term)+1)

; create structure
create_struct_temp,retval,'',nn,replicate('A',n_elements(nn))

for i=0, n_tags(retval)-1 do $
   retval.(i)=vv(i)
missx='-999999'  ; non string -&- non-HEX MISSING replacement 
missh='0xFFFF'    ; HEX MISSING replacement

recs=str2arr('FLAT,MPO,ASD,ORB')+'_REC'
recs=[recs,str2arr('lutquery')]
for i=0,n_elements(recs)-1 do begin 
   template=rem_tag(template,recs(i))
   retval=rem_tag(retval,recs(i))
endfor

if data_chk(template,/struct) then begin ; data fill/type
   subtemp=str_subset(template,tag_names(retval))
   tretval=replicate(subtemp,count)  ; one per
   rtags=tag_names(retval)
   ttags=tag_names(subtemp)
   ntags=n_elements(ttags)
   ;;;;rss=tag_index(tretval,tag_names(retval))
   rss=tag_index(retval,tag_names(tretval))
   for t=0,ntags-1 do begin 
      buff=retval.(rss(t))
      repc=([" ","'"])(data_chk(subtemp.(t),/string)) ;  
      repc=' ' ; changed when execute -> reads algorithm
      buff=ssw_patt_replace(buff,'"',repc)
      chkmiss=strpos(string(buff),'MISSING') ne -1 
      chkinval=strpos(string(buff),'Invalid KeyLink') ne -1
      hexhand=strpos(retval.(rss(t)),'["0x') ne -1

      case 1 of ; special cases...
         chkmiss: begin 
            nstr=1-data_chk(tretval.(t),/string)
            if nstr then begin 
               sswmiss=where_pattern(buff,'MISSING',mcnt)
               buff=ssw_patt_replace(buff,'MISSING',([missx,missh])(hexhand))
            endif
         endcase
         chkinval: buff=([missx,buff])(data_chk(tretval.(t),/string))
         ;is_member(nn(t),'QUALITY'): begin ; HEX handler
         hexhand: begin 
            buff=ssw_patt_replace(buff,'"',"'")
         endcase
         else:
      endcase 
      temp=tretval.(t)
      if strpos(buff,'[') ne -1 then $ 
          buff=strextract(buff,'[',']')
      buff=strtrim(str2arr(buff),2)

      if chkmiss and n_elements(buff) eq 1 then $
         buff=replicate(missx,n_elements(temp))
      if n_elements(buff) eq n_elements(temp) then begin 
      ;if is_member(nn(t),'QUALITY') then begin 
      if hexhand then begin 
          reads,buff,temp,format='(Z)' 
      endif else   reads,buff,temp 
      endif else begin
         ; box_message,'mismatch count'
      endelse
      ;estat=execute('temp='+buff)
      ;if not estat then stop,'estat TODO - numerical fields which include ["MISSING...]
if get_logenv('keycheck') ne '' then stop,'buff'
      tretval.(t)=temp
   endfor
   retval=tretval
endif
retval=add_tag(retval,count,'count')
if n_elements(runtime) eq 1 then retval=add_tag(retval,runtime,'runtime')
retval=add_tag(retval,status,'status')

return,retval
end
