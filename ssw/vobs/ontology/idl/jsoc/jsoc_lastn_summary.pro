pro jsoc_lastn_summary, ds, lastn, keywords=keywords

if n_elements(ds) eq 0 then ds='aia.lev0'
if n_elements(lastn) eq 0 then lastn=6

instr=ssw_strsplit(ds,'.',/head)
instr=instr(0)

def_keywords= $
 (file_search(get_logenv('$SSW_JSOC'),'def_keywords_'+instr+'.dat'))(0)

if def_keywords ne '' and n_elements(keywords) eq 0 then $
   keywords=reform((rd_tfile(def_keywords,2))(0,*))
ssw_last_jsoc,ds,index,data,lastn=lastn,keywords=keywords,/local_files ; this does the jsoc->ssw work...
titles=ds+' ' + get_infox(index,'date_obs,instrume')
comments=strarr(n_elements(index))
for i=0,lastn-1 do begin
   help,index(i),/str,out=out & outc=str2cols(out(1:*),/un,/trim)
   strtab2vect,outc,tag,type,value
   out=strjustify(tag)+' '+value
   comments(i)='!3' + arr2str(out,'!C')
endfor
sum=ssw_multi_img_summary(index,data,titles=titles,comments=comments) ; ssw-gen img_summ
outsize=[data_chk(sum,/nx),data_chk(sum,/ny)] ; native output of img_summary.pro
mdir=concat_dir('$path_http',instr+'last'+strtrim(lastn,2)) ; where to stick it
if not file_exist(mdir) then mk_dir,mdir ; create if required
mnam=instr+'last'+strtrim(lastn,2) + '_img_summary'
set_plot,'Z'
sum(where(sum eq min(sum)))=100 & sum(where(sum eq max(sum)))=10 ; background & plot values
image2movie,sum,uttimes=index.date_obs,/inctimes,movie_dir=mdir,$
   movie_name=mnam, outsize=outsize, thumbsize=outsize/2.5, $ 
   html=html,/still,table=39,tperline=4, label=titles
image2movie,sum,uttimes=index.date_obs,/inctimes,movie_dir=mdir,$
   movie_name=mnam, outsize=outsize, thumbsize=outsize/2.5, $
   html=htmlj,table=39,label=titles,/notfilm,/java
; ------------ now make the index.html --------------------
hdoc=concat_dir(mdir,'index.html')
html_doc,hdoc,/header
file_append,hdoc,'<font color=orange size=+1>Last ' +strtrim(lastn,2) + ds + $
   ' Last run: ' + anytim(reltime(/now),/vms,/trunc) + '</font><p>'
file_append,hdoc,[html,htmlj] ; append image2movie html output
html_doc,hdoc,/trailer
; ----------------- done html ---------------------------
return
end

