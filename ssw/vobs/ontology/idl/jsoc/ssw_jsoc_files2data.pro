function ssw_jsoc_files2data,jsocfiles, debug=debug
;
;+ 
;   Name: ssw_jsoc_files2data
;
;   Purpose: Local JSOC/SUMS files -> "data"  via shared memory/filemapping
;
;   Input Parameters:
;      jsocfiles - assummed local jsoc/SUMS paths (e.g. "url_quick")
;
;   Output:
;      function returns data - 2D or 3D 
;
;   History:
;      8-jul-2008 - S.L.Freeland - shared memory analog of 'mreadfits' data portion
;
;
;  Restrictions: yes
;     for one thing, assume each file is an HMI or AIA 4096^2 
;-

; quick check on first filename
debug=keyword_set(debug)
if not data_chk(jsocfiles,/string) then  begin
   box_message,'require one or more jsoc filenames'
   return,-1
endif

if not file_exist(jsocfiles(0)) then begin
   box_message,'Require local filenames
   return,-1
endif
nf=n_elements(jsocfiles)
segs=strtrim(alphagen(nf),2)
template={header:bytarr(2880),data:intarr(4096,4096)};
retval=make_array(4096,4096,nf,/int,/nozero)
for i=0,nf-1 do begin 
   
   shmmap,segs(i),get_name=gn,template=template,filename=jsocfiles(i),/private
   img=shmvar(gn)
   retval(0,0,i)=temporary(img.data)
   shmunmap,gn
   if debug then help,/shared_memory
endfor
byteorder,retval,/swap_if_little
return,retval
end







