function jsoc_index2ssw, jsocindex
;
;+
;
;   Name: jsoc_index2ssw
;
;   Purpose: fill in "missing" ssw tags 
;
;   Input Parameters:
;      jsoindex
;
;   Output:
;      function returns SSW compliant index
;
;   History:
;      30-jun-2008 - S.L.Freeland - pre-launch service apps
;-
nout=n_elements(jsocindex)
ssw_fake_index,jsocindex(0).date_obs,index,xcen=.00001,ycen=.00001, cdelt1=.5, $
   nx=4096, ny=4096

nodobs=rem_tag(index,'DATE_OBS') 

if n_elements(jsocindex) gt 0 then index=replicate(index,nout)

retval=join_struct(jsocindex,index)

return,retval
end
