function ssw_json2struct, jsondatain, debug=debug, status=status, names=names, $
   noexe=noexe
;+
;   Name: ssww_json2struct
;
;   Purpose: convert JSON string -> idl structure
;
;   Input Parameters:
;      jsondata - string or string array containing one JSON object
;
;   Output:
;      function returns idl structure representation (-1 if error)
;
;   Keyword Parameters:
;      names (output) - strarray of params/names
;      status (output) =1 if ~ok (structure returned)
;      noexe (switch) - if set, generate&execute *.pro rather than execute statement 
;
;   11-Jun-2008 - S.L.Freeland - ssw client/SDO JSOC communication
;   20-jun-2008 - S.L.Freeland - add /NOEXE keyword&function
;   18-feb-2009 - S.L.Freeland - replace str_replace w/byte op ([] -> "")
;
;   Method:
;      tweak JSON -> IDL structure definition string/string array, then
;      execute it (idl-only functional ~equivilent to JavaScript 'eval' of JSON)
;
;   Restrictions:
;      yes....
;      proto version uses execute so: not VM compat & longish JSON
;      (check back tommorrow for non-exe)
;-

status=0
debug=keyword_set(debug)
if not data_chk(jsondatain,/string) then begin 
   box_message,'Expect JSON string definition/object..
   return,-1
endif

jsondata=web_dechunk(jsondatain)

bjson=arr2str(jsondata,'')
 
noexe=keyword_set(noexe) or strlen(bjson) gt 32000
if noexe then begin 
   box_message,'NOEXE set, creating *.pro
   bjson=jsondata
endif
if debug then stop,'bjson'
null=''
;
;  ----- block does JSON {"name":Value...} -> IDL {name:Value...} ---
;  ----- while allowing pass through of Value="Quoted Strings"
ssq=where_pattern(bjson,'"',qcnt)
ssn=where_pattern(bjson,'":',ncnt)
ssq=ssq(rem_elem(ssq,ssn))
nnam=n_elements(ssn)
names=strarr(nnam)

bbjson=byte(bjson)
for i=0,nnam -1 do begin 
   sse=ssn(i)
   sss=last_nelem(where(ssq lt sse))
   ds=sse-ssq(sss)
   if noexe then names(i)=string(bbjson(ssq(sss)+1:sse-1)) else $
      names(i)=strmid(bjson,ssq(sss)+1,ds-1)
   bbjson(sse)= 32b
   bbjson(ssq(sss))=32b
endfor
;----------------- end of block ------------------

ss=where_pattern(bbjson,'[]',sscnt) ; problem for idl compiler
if sscnt gt 0 then begin 
   bbjson(ss)=byte('"')
   bbjson(ss+1)=byte('"')
endif 
json=string(bbjson)

; if sscnt gt 0 then json=temporaray(str_replace(json,'[]','[null]'))

if debug then stop, 'pre Octal'

keychk=1
knames=['keywords','name','values']
for i=0,n_elements(knames)-1 do keychk=keychk and $
   total(strmatch(names,knames(i))) gt 0 

if keychk then json=ssw_json_namevalue(json) ; Keyword handling

bjson=byte(json)

cpats='"'+strtrim(indgen(8),2)
npats=n_elements(cpats)
ss=-1
for i=0,npats-1 do ss=[ss,where_pattern(bjson,cpats(i))] ; avoid Octal problem
sss=where(ss ne -1, sscnt)
if sscnt gt 0 then begin
   bjson(ss(sss)+1)=32b
   if debug then box_message,'constant suppress ; some quoted strings may suffer...'
   json=string(bjson)
endif

if debug then stop,'json (pre execute)'

jstruct=-1

if noexe then begin  ; /NOEXE (long for example)? create/execute *.pro
   fname=get_user()+'_'+time2file(reltime(/now))
   cdir=curdir() & outdir=get_temp_dir() & funct=concat_dir(outdir,fname+'.pro')
   file_append,funct,['function ' + fname,'retval= $',  $
      json(0:n_elements(json)-2)+ ' $', last_nelem(json), 'return,retval','end'],/new
      if debug then stop,'funct>> ' + funct
   cd,outdir  ; 
   jstruct=call_function(fname)
   cd,cdir
   if 1-keyword_set(no_delete) then ssw_file_delete,funct
endif else begin 
   estring='jstruct='+json ; 1-NOEXE? just execute the structure definition
   estat=execute(estring)
endelse

if debug then stop,'names
status=data_chk(jstruct,/struct)

return,jstruct
end





