function drms_wave_subsample, drms, waves=waves
wave=gt_tagval(drms,/wavelnth,missing='')
if wave(0) eq '' then return,drms  ; EARLY RETURN!
av=all_vals(drms.wavelnth)
nw=n_elements(av)
retval=replicate(drms(0),nw)
for w=0,nw-1 do begin 
  ssw=where(drms.wavelnth eq av(w),sscnt)
  retval(w)=drms(ssw(sscnt/2))
endfor
times=gt_tagval(retval,/date_obs,missing='')
if times(0) eq '' then times=gt_tagval(retval,/t_obs,missing='')
if n_elements(all_vals(times)) gt 1 then retval=retval(sort(anytim(times)))

return,retval
end
pro ssw_jsoc_time2data_sample,t0,t1,drms, $ 
   minute_cadence=minute_cadence, hour_cadence=hour_cadence, $
   cadence=cadence, loud=loud, quiet=quiet,  uniq_wave=uniq_wave, $
   sample_width=sample_width, ds=ds, _extra=_extra
;
;+
;   Name: ssw_jsoc_time2data_sample
;
;   Purpose: subsample non-slotted series via pseudo-slotted
;
;   Input Parameters:
;      t0,t1 - time range or sample times
;
;   Output Parameters:
;      drms - drms "structures" 
;
;   Keyword Parameters:
;      ds - data series name ; default = 'aia.lev1'
;      minute_cadence - cadence in #minutes -
;      hour_cadence - cadence in #hours
;      sample_width - width of sample in SECONDS =
;      uniq_wave - (switch) if set, only one sample/wave/width
;      _extra - implicit keywords - > ssw_jsoc_time2data (/jsoc2, etc)
;               for example KEY='t_obs,fsn,wavelnth,datamean' 
;      loud - enable ssw_jsoc/ssw_jsoc_time2data messages
;
;   Calling Examples:
;      IDL> ssw_jsoc_time2data_sample,t0,t1,drms,cadence='60m', ds='aia.lev1' ; 
;      IDL> ssw_jsoc_time2data,<sampletimes>,xx,drms,sample_width=120,
;
;   Restrictions:
;      same as for underlying ssw_jsoc/ssw_jsoc_time2data
;      "unpublished" series require /JSOC2 switch -AND- jsoc2 priviledges(why??)
;-
; 
uniq_wave=keyword_set(uniq_wave)
quiet=keyword_set(quiet)
silent=1-keyword_set(loud) ; default is ~silent

if n_elements(sample_width) eq 0 then sample_width=30
case 1 of 
   n_elements(t0) eq 1 and n_elements(t1) eq 1: begin 
      if data_chk(cadence,/string) then begin 
         cadence=strlowcase(cadence)
         value=str2number(cadence)
         case strlastchar(cadence) of
            'm': minute_cadence=value
            'h': hour_cadence=value
            'd': day_cadence=value
            else:day_cadence=value ; really no default...
         endcase
      endif
      grid=timegrid(t0,t1,minute=minute_cadence, hours=hour_cadence, days=day_cadence)
      t0x=anytim(anytim(grid) - (.5*sample_width),/ecs)
      t1x=anytim(anytim(grid) + (.5*sample_width),/ecs)
   endcase
   n_elements(t0)  gt 1: begin 
      if n_elements(t1) eq n_elements(t0) then begin ; user supplied start & stop vectors
         t0x=t0
         t1x=t1
      endif else begin 
         t0x=anytim(anytim(t0) - (.5*sample_width),/ecs)
         t1x=anytim(anytim(t0) + (.5*sample_width),/ecs)
      endelse
   endcase
   else: begin 
      box_message,'Not time syntax yet handled; need (time range + cadence) or time sample vector(s)
      return
   endcase
endcase

i=0
if n_elements(giveup) eq 0 then giveup=10 
ssw_jsoc_time2data,t0x(i),t1x(i),drms,_extra=_extra,ds=ds, silent=silent, serstr=serstr
while ~data_chk(drms,/struct) and i lt giveup do begin
   i=i+1
   ssw_jsoc_time2data,t0x(i),t1x(i),drms,_extra=_extra,ds=ds, silent=silent, serstr=serstr
endwhile

if not data_chk(drms,/struct) then begin 
   box_message,'No data after ' + strtrim(giveup,2) + ' checks...bailing'
   return
endif

if uniq_wave then drms=drms_wave_subsample(drms) ; only one sample per wave per sample bin

nsamps=n_elements(t0x)

for i=i,nsamps-1 do begin ; 1st sample already done
   tsamp=arr2str([t0x(i),t1x(i)],' -TO- ')
   if quiet then box_message,'Next sample>> ' + tsamp
   delvarx,drmsx
   ssw_jsoc_time2data,t0x(i),t1x(i),drmsx,_extra=_extra,ds=ds, silent=silent, serstr=serstr
   if data_chk(drmsx,/struct) then begin 
       if uniq_wave then drmsx=drms_wave_subsample(drmsx)
       drms=concat_struct(drms,drmsx) ; could bog down if "big" TODO: insertion rather than concat.
   endif else box_message,'no records for ' + tsamp 
endfor
   
return
end


