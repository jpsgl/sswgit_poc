function ssw_jsoc_fsn2query, fsn0, fsn1, serstr=serstr, ds=ds, lastn=lastn, _extra=_extra, debug=debug
;
;+
;   Name: ssw_jsoc_fsn2query
;
;   Purpose: user in put FSN, FSN-range or FSN list -> jsoc query string
;
;   Input Parameters:
;      fsn0 - 1st FSN if start of range or vector of discrete FSNs
;      fsn1 - last FSN if range
;
;   Keyword Parameters:
;      serstr - input series structure - useful, albeit optional (may get this internally if DS supplied but not serstr)
;      ds - data series name - suggest SERSTR -or- DS 
;      lastn - optional most recent LASTN FSN query is derived (assumes either DS or SERSTR was supplied)
;      _extra - unknown keywords -> ssw_jsoc via inherit (for example, /jsoc2)
;-

debug=keyword_set(debug)

case 1 of 
   keyword_set(ds): begin 
      if not required_tags(serstr,'note,primekeys,keywords') then serstr=ssw_jsoc(/series_struct,ds=ds,_extra=_extra)
   endcase
   required_tags(serstr,'note,primekeys,keywords') : if n_elements(ds) eq 0 then $
      ds=ssw_strsplit(serstr.interval.lastrecord,'[',/head)
   else:
endcase
 
case 1 of 
   keyword_set(lastn): begin 
      if not required_tags(serstr,'interval') then begin
         box_message,'LASTN set, but need either DS or SERSTR to derive.. 
         f0='' ; null query
      endif else begin 
         lastrec=str2number(strextract(serstr.interval.lastrecord,'[',']'))
         f0=strtrim(lastrec-(lastn-1),2)
         f1=strtrim(lastrec,2)
      endelse
   endcase
   n_elements(fsn0) gt 1: f0=arr2str(strtrim(fsn0,2)) ; -> comma delim list
   is_number(fsn0) and is_number(fsn1): begin
     f0=strtrim(fsn0,2)
     f1=strtrim(fsn1,2)
   endcase
   n_elements(fsn0) eq 1: begin 
      f0=strtrim(fsn0,2)
   endcase
   else: begin 
      box_message,'Not trained for this input yet
      f0=''
   endcase
endcase

if n_elements(ds) eq 0 then ds=''
query=f0
if n_elements(f1) gt 0 then if f1 ne f0 then query=f0+'-'+f1 ; range
pkskip=''
pkeys=strtrim(gt_tagval(serstr,/primekeys,missing='FSN'),2)
ssfsn=where(pkeys eq 'FSN',fsncnt)
if ssfsn[0] gt 0 then pkskip=arr2str(replicate('[]',ssfsn[0]),'')

retval=ds+pkskip+'['+query+']'

if debug then stop,'query,ds'

return,retval

end
   






