function ssw_jsoc_wave2ds, wave, endtime,  cadence=cadence, jsoc2=jsoc2

if n_elements(wave) eq 0 then begin 
   box_message,'No wave supplied, assuming EUV'
   wave=171
endif

swave=strtrim(wave,2)

hmi=is_member(swave,'blos,cont,mag')
ds=(['aia.lev1','hmi.'])(hmi)
cadence=(['12s','45s'])(hmi)  

case 1 of
   hmi: begin
      type=(['Ic','M'])(swave eq 'blos' or swave eq 'mag')
   endcase
   is_member(swave,'94,131,171,193,211,304,335') : begin
      type='euv'
   endcase
   is_member(swave,'1600,1700'): begin 
      cadence='24s'
      type='uv'
   endcase
   is_member(swave,'vis,4500'): begin 
      cadence='1h'
      type='vis'
   endcase
   else: begin
      box_message,'wave not recognized! defaulting to aia euv..'
      cadence='12s'
      type='euv'
   endcase
endcase

retval=arr2str([ds,type,cadence],'_')
retval=str_replace(retval,'._','.')
jsoc2=0

if n_elements(endtime) gt 0 then begin 
   dt=ssw_deltat(reltime(/now),ref=endtime,/days)
   case 1 of 
      dt gt 7: ; assume ok
      else: begin 
         serstr=ssw_jsoc(/series_struct,ds=retval)
         interval=gt_tagval(serstr,/interval,missing='')
         if required_tags(interval,/lastrecord) then begin 
            send=strextract(interval.lastrecord,'[',']')
            dt=ssw_deltat(send,ref=endtime,/hours)
            if dt lt 0 then  begin
               dstype=(['slotted/default','default'])(hmi)
               box_message,'Endtime later than '+ dstype +'( '+retval +') series end; using _nrt'
               retval=([ds+'_nrt2',retval+'_nrt'])(hmi)
               cadence=(['',cadence])(hmi)
               jsoc2=1
            endif
         endif
      endcase
    endcase
endif 


return,retval
end
   

   
         
   

