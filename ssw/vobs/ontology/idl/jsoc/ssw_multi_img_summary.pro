function ssw_multi_img_summary,index,data, _extra=_extra, $
   titles=titles, comments=comments
;
;+
;   Name: ssw_multi_img_summary
;
;   Purpose: SSW "index,data" (2D/3D) -> 1:1 img_summary.pro summary (2D/3D)
;
;   Input Parameters:
;      index,data - at least roughly SSW compliant "index,data" (2D or 3D)
;
;   Output:
;      function returns 2D/3D cube of 'img_summary.pro' 
;
;   Keyword Parameters:
;      _extra - unspecified keywords -> img_summary.pro (see that header...)
;
;   History:
;      25-jun-2008 - playing around w/AIA/HMI NRT ground test->SSW compat. 
;-

nout=n_elements(index)
nimg=data_chk(data,/nimage)

if (nout ne nimg) or nout eq 0 then begin 
   box_message,'Expects 2D or 3D "index,data" pairs'
   return,-1
endif

dtemp=!d.name
set_plot,'Z'  ; this is a "memory only" function

if n_elements(titles) ne nout then begin 
  titles=index.date_obs

endif

if n_elements(comments) ne nout then begin 
   comments=index.comment(0)
endif

img_summary,data(*,*,0),_extra=_extra, titles(0), str2arr(comments(0))
temp=tvrd()

retval=bytarr(data_chk(temp,/nx),data_chk(temp,/ny),nout)

retval(0,0,0)=temp

for i=1,nout-1 do begin 
   img_summary,data(*,*,i),_extra=_extra, titles(i), comments(i)
   temp=tvrd()
   retval(0,0,i)=temp 
endfor

set_plot,dtemp
return,retval
end
