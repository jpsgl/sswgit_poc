pro ssw_jsoc_lastn2shm, nn, aia=aia, hmi=hmi, $
   destroy_segment=destroy_segment, os_handle=os_handle, seg_name=seg_name, $
   initialize=initialize, index_seg_name=index_seg_name
;+
;   Name: ssw_jsoc_lastn2shm
;
;   Purpose: attach to shared memory "lastN" images (create if not yet done)
;
;   Input Parameters:
;      nn - number of images in shared memory segment (def=20)
;
;   Keyword Parameters:
;      aia (switch) - if set, lastn AIA (default)
;      hmi (switch) - if set, lastn HMI 
;      destroy_segment - destroy when segment unmapped [ssw_]shmunmap
;      seg_name (output) system derived segment name (data)
;      os_handle (output) - determistic OS handle /last<nn>{AIA,HMI} data seg.
;      index_seg_name (output) - system derived segment name ("index")
;
;   History: 
;      9-jul-2008 - S.L.Freeland
;     25-jul-2008 - S.L.Freeland 
;
;   Restrictions:
;      yes - for at least today, assume 4096^2 aia or hmi
;-

hmi=keyword_set(hmi)
aia=keyword_set(aia) or (1-hmi)

inst=(['aia','hmi'])(hmi)

if n_elements(nn) eq 0 then nn=20 ; number of images in lastn shared memory
delvarx,seg_name ; force output only 
snn=strtrim(nn,2)
os_handle='/last'+snn+inst
template=intarr(4096,4096,nn) 
catch,errno
if errno ne 0  or keyword_set(initialize) then begin 
   catch,/cancel
   delvarx,initialize
   box_message,'Initializing shared memory for '+os_handle
   shmmap,template=template,os_handle=os_handle,get_name=seg_name, $
     destroy_segment=destroy_segment
   ssw_last_jsoc,inst+'.lev0',index,data,lastn=nn,/local_files,$
         keywords='FSN,TIME,MJD,CAMERA'
   shmmap,template=index,os_handle=os_handle, $
      get_name=index_seg_name, destroy_segment=destroy_segment
   var=shmvar(seg_name)
   var(0,0,0)=temporary(data)   ; data -> shared
   vari=shmvar(index_seg_name)
   vari(0)=index ; "index" -> shared
endif

var=shmvar(seg_name) ; this throws an error 1st call

return
end
