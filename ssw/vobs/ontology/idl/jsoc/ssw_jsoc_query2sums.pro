function ssw_jsoc_query2sums, ds, urls=urls, jsoc2=jsoc2, debug=debug
;+
;   Name: ssw_jsoc_query2sums
;
;   Input Parameter:
;      ds - fully formed jsoc query 
;
;   Output:
;      function returns /SUMxx paths or urls
;
;   Keyword Parameters'
;      urls (switch) - if set, return jsoc urls
;      jsoc2 (switch) -f set, jsoc2  (requires jsoc2 access privildege)
;
;   History:
;      circa 1-aug-2012 - S.L.Freeland - jsoc query -> SUMS paths/urls
;      23-sep-2012 - S.L.Freeland - url encode optional verbatim "xquery"
;                    (tbd: jsoc_info vs. show_info diffrence in q string?)
;
debug=keyword_set(debug) or get_logenv('sums_check') ne '' 
if not data_chk(ds,/string) then begin 
   box_message,'need jsoc data series string'
   return,''
endif

urls=keyword_set(urls)


jsoc2=keyword_set(jsoc2) or get_logenv('jsoc2') ne ''

jsoc='http://jsoc' + (['','2'])(keyword_set(jsoc2)) +'.stanford.edu/'
jop= 'cgi-bin/ajax/show_info?

sids=(['','ds='])(strpos(ds,'ds=') eq -1) + ds
segment=''
if strpos(sids,'{') ne -1 then begin 
   sids=ssw_strsplit(sids,'{',/head,tail=segment)
   segment=ssw_strsplit(segment,'}',/head)
   segment='&seg='+segment
endif else begin 
   serstr=ssw_jsoc(ds=ds,jsoc2=jsoc2,/series_struct)
   if required_tags(serstr,'segments') then $
      segment='&seg='+strtrim(serstr.segments(0).name,2)
endelse

qstring=jsoc+jop+sids+'&q=1&P=1'+segment
qstring=strcompress(qstring,/remove)
qstring=arr2str(qstring,'')

qchars=str2arr('?,!')
for q=0,n_elements(qchars)-1 do begin
   openq='['+qchars(q)
   closeq=qchars(q) + ']'
   if strpos(qstring,openq) ne -1 and strpos(qstring,closeq) ne -1 then begin 
     if debug then box_message,'url encoding xquery
     xquery=strextract(qstring,openq,closeq,/include)
     nquery=url_encode(xquery,include=qchars)
     qstring=str_replace(qstring,xquery,nquery)
   endif
endfor

sock_list,qstring,retval,err=err
if debug then stop,'query,segment'
case 1 of 
   err(0) ne '': retval=err
   urls: retval=jsoc+retval
   else:
endcase

return,retval
end









