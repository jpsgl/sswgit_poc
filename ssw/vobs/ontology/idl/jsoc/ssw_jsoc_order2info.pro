function ssw_jsoc_order2info, jsocurl, status=status, out_wcnt=out_wcnt, $
   wcnt_only=wcnt_only, nohead=nohead, header_only=header_only, debug=debug
;
;+
;   Name: ssw_jsoc_order2info
;
;   Purpose: return info re: jsoc sdo order
;
;   Input Parameters:
;      jsocurl - the output jsoc tar url
;
;   History:
;     Circa 1-jun-2012 - S.L.Freeland - allow aia/hmi downline pipeline for JSOC cutout orders
;     30-jan-2013 - S.L.Freeland - sock_list2 -> sock_list
; 
;
debug=keyword_set(debug)
status=0
if n_elements(jsocurl) eq 0 then begin 
   box_message,'Need url for jsoc tar output
   return,-1
endif


if strpos(jsocurl,'http') eq -1 or strpos(jsocurl,'.tar') eq -1 then begin 
   box_message,'Expect absolute URL for jsoc order/tar files'
   return,-1
endif


break_url,jsocurl,server,path,tarfile

index=concat_dir(server,path) + 'index.txt'

sock_list,index,idat

datass=where(strpos(idat,'# DATA') ne -1, dcnt)

if dcnt ne 1 then begin 
   box_message,'Unexpected index.txt file, bailing...
   return,-1
endif

header=idat(1:datass-1)
cols=str2cols(header,'=',/trim,/unaligned)
strtab2vect,cols,param,value

headinfo={version:0.,requestid:'',method:'',protocol:'',count:0l,size:0l,exptime:'',dir:'',status:0}
bookkeep={fovx:'64',fovy:'64',ref_time:'',starttime:'',endtime:'',max_frames:0l,waves:''}
tn=strlowcase(tag_names(headinfo))
for i=0,n_tags(headinfo)-1 do begin 
  ss=where(strlowcase(param(i)) eq tn,tcnt)
  if tcnt eq 1 then headinfo.(tag_index(headinfo,param(ss)))=value(ss)
endfor

ssfits=where(strpos(idat,']') ne -1 and strpos(idat,'_image') ne -1, fcnt)

if fcnt eq 0 then begin 
   box_message,'No FITS/image files??'
   return, -1
endif

fits=strcompress(idat(ssfits),/remove)
fnames=ssw_strsplit(fits,']',/tail)
waves=strextract(fnames,'Z_','_image')
uwaves=all_vals(waves) 

max_frames=-1
for w=0,n_elements(uwaves)-1 do begin 
   ss=where(strpos(fnames,'_'+ uwaves(w) + '_') ne -1, wxcnt)
   wcnt=add_tag(wcnt,wxcnt,'w'+uwaves(w))
   max_frames=max([max_frames,wxcnt])
endfor
times=strextract(fits,'hgpatch_','Z')
times=times(sort(times))
time_window,times,starttime,endtime

; predetermined per jsoc order
bookkeep.starttime=starttime
bookkeep.endtime=endtime
bookkeep.ref_time=anytim( (anytim(starttime)+anytim(endtime))/2,/ccsds)
bookkeep.max_frames=max_frames 
bookkeep.waves=arr2str(uwaves)

headinfo=join_struct(headinfo,bookkeep)

out_wcnt=wcnt 

case 1 of
   keyword_set(nohead) or keyword_set(wcnt_only): retval=wcnt
   keyword_set(header_only): retval=headinfo
   else: retval=join_struct(headinfo,wcnt)
endcase

status=1

if debug then stop,'wcnt,headinfo,retval'
return,retval
end

