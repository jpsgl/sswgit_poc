function ssw_nameval2struct, namvalin, template, debug=debug

namval=''
for i=0,n_elements(namvalin)-1 do namval=namval+namvalin(i)

debug=keyword_set(debug)

nstring='"name":'
vstring='"values":'
nlen=strlen(nstring)
vlen=strlen(vstring)

names=where_pattern(namval,nstring,ncount)
values=where_pattern(namval,vstring,vcnt)

if ncount ne vcnt then begin 
   box_message,'mismatch name:value
   stop
endif

ss=where(values lt names, dcnt)
if dcnt gt 0 then begin
   box_message,'name:value position issue?
   stop
endif

nn=strarr(ncount)
vv=nn

count=str2number(strextract(namval,'"count":',',"status":')) ; #records
status=str2number(strextract(namval,'"status":','}'))

dnv=values-names

vlim=shift(names,-1) - values - nlen - 5

for i=0,ncount-1 do begin 
   nn(i)=strmid(namval,names(i)+nlen+1,dnv(i)-(vlen+1))
   vv(i)=strmid(namval,values(i)+vlen, vlim(i))
endfor
; rationalize last element..
term=(['}',']}'])(count gt 0) ; scalar/arry fixup
temp=strmid(namval,values(i-1)+vlen,strlen(namval))
vv(i-1)=strmid(temp,0,strpos(temp,term)+1)

; create structure
create_struct,retval,'',nn,replicate('A',n_elements(nn))

for i=0, n_tags(retval)-1 do $
   retval.(i)=vv(i)

if data_chk(template,/struct) then begin ; data fill/type
   stop,'retval'
   tretval=replicate(template,count)  ; one per
   rtags=tag_names(retval)
   ttags=tag_names(template)
   ntags=n_elements(ttags)
   rss=tag_index(tretval,tag_names(retval))
   for t=0,ntags-1 do begin 
      buff=retval.(rss(t))
      repc=([" ","'"])(data_chk(template.(t),/string)) ; Octal prob; string vs numeric 
      buff=str_replace(buff,'"',repc)
      estat=execute('temp='+buff)
      ; if not estat then stop,'estat TODO - numerical fields which include ["MISSING...]
      tretval.(t)=temp
   endfor
   retval=tretval
endif
retval=add_tag(retval,count,'count')
retval=add_tag(retval,status,'status')


return,retval
end
