function ssw_cutout_service_query2jsoc, query_file, process=process, debug=debug

;+
;   Name: ssw_cutout_service_query2jsoc
;
;   Purpose: generate/return JSOC cutout query analog(s) from ssw_cutout_service (SSW cutout service) query 
;
;   Input Parameters:
;      query_file - one ssw_cutout_service query file or string
;
;
debug=keyword_set(debug)

;IDL> ssw_jsoc_time2data,'2:30 15-apr-2011','2:36 15-apr-2011',index,data,cadence='1m',waves='94,335',/debug
;IDL> print,new
;http://jsoc2.stanford.edu/cgi-bin/ajax/jsoc_fetch?ds=aia.lev1_euv_12s[2011-04-15T02%3A30%3A00Z%2F360s@1m][94,335]{image}&method=url-tar&protocol=FITS,compress Rice&notify=freeland@lmsal.com&requestor=freeland@lmsal.com&op=exp_request&sizeratio=0.017434358596801758&process=n=0|im_patch,t_start=2011-04-15T02:30:02Z,t_stop=2011-04-15T02:31:02Z,t=0,r=0,c=0,cadence=1m,locunits=arcsec,boxunits=arcsec,t_ref=2011-04-15T02:30:02Z,x=-700,y=350,width=650,height=450
;filenamefmt=aia.lev1_euv_12s.{T_REC:A}.{WAVELNTH}.{segment}
; IDL> more,out
;{"status":2,"requestid":"JSOC_20151123_5236_IN","method":"url-tar","protocol":"FITS,compress Rice","wait":10,"rcount":12,"size":87}i;

case 1 of 
   file_exist(query_file): qinfo=url_decode(qfile=query_file)
   data_chk(query_file,/string): qinfo=url_decode(query_file)
   else: begin 
      box_message,'Need ssw cutout service query file or query string...
      return,'' ; EARLY EXIT on incorrect input !!!
   endcase
endcase

cadence=gt_tagval(qinfo,/cadence,missing='')
t0=anytim(gt_tagval(qinfo,/starttime),/ccsds)
t1=anytim(gt_tagval(qinfo,/endtime),/ccsds)

tstart='t_start='+t0
tstop= 't_stop=' +t1
tref=  't_ref='+ gt_tagval(qinfo,/ref_time,missing=gt_tagval(qinfo,/ref_date,missing=t0))
track='t='+(['1','0'])(gt_tagval(qinfo,/notrack,missing='0') eq '0')
reg  ='r='+(['0','1'])(gt_tagval(qinfo,/data_level,missing='1') gt 1)

; pointing options

locunits='locunits=arcsec'
boxunits='boxunits=arcsec'
case 1 of 
   required_tags(qinfo,'ref_helio'): begin
      hyx=ssw_helio2string(gt_tagval(qinfo,'ref_helio'))
      y='y='+strtrim(hyx[0],2)
      x='x='+strtrim(hyx[1],2) 
      locunits='locunits=stony'
help,x,y,locunits
   endcase
   required_tags(qinfo,'xcen,ycen'): begin 
      x='x='+gt_tagval(qinfo,/xcen)
      y='y='+gt_tagval(qinfo,/ycen)
   endcase
   else: begin 
      box_message,'Not sure about pointing, suncenter for now (email sam...)'
      x='x=0'
      y='y=0'
   endcase
endcase
      
fovx='width='+gt_tagval(qinfo,/fovx)
fovy='height='+gt_tagval(qinfo,/fovy,missing=gt_tagval(qinfo,/fovx))

jsoc_query='&sizeratio=1&process=n=0|im_patch,'+arr2str([tstart,tstop,tref,track,reg,x,y,fovx,fovy])

if n_elements(filenamefmt) eq 0 then filenamefmt='{series}.{T_REC:A}.{WAVELNTH}.{segment}'
jsoc_query=jsoc_query+'&filenamefmt='+filenamefmt


maxf=gt_tagval(qinfo,/max_frames,missing=50)
dt=ssw_deltat(t0,t1)
awaves=str2arr(qinfo.waves)
ds=strarr(n_elements(awaves))
cadxx=ds
if debug then stop,'ds'
time_types=ds

for s=0,n_elements(ds) -1 do begin
   ds[s]=ssw_jsoc_wave2ds(awaves[s],t1, cadence=cadence,time_type=tt) ; + ':'+cadence
   cad=ssw_maxframes2cadence(maxf,str2number(cadence),dt)
   ds[s]=ds[s]+':'+strtrim(cad,2)+'s'
   time_types[s]=tt 
endfor
; adjust cadence
uds=all_vals(ds)

nc=n_elements(uds)
jsoc_calls=strarr(nc)
for u=0,nc-1 do begin 
   ssu=where(ds eq uds[u])
   dsx=ssw_strsplit(ds[ssu[0]],':',/head,tail=cadx)
   jsoc_calls[u]=ssw_jsoc_time2query(t0,t1,cadence=cadx,tai=time_types[ssu[0]] eq 'tai', ds=dsx, ccsds=time_types[ssu[0]] eq 'ccsds') + jsoc_query
endfor


if debug then stop,'jsoc_calls'

return,jsoc_calls


end
