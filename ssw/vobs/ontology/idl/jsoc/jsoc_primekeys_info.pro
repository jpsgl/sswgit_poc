function jsoc_primekeys_info,jsonin, remove=remove


ss=where(strpos(jsonin,'"primekeysinfo":') ne -1,pkcnt)
; this is temporarily ugly - please don't look...

if pkcnt gt 0 then begin 
   vv=where(strpos(jsonin(ss),'[{') ne -1,vvcnt)
   endpat=(['}','}]'])(vvcnt gt 0)
   ssend=ss+where(strpos(jsonin(ss(0):*),endpat) ne -1, ecnt)
   pkdat=jsonin(ss(0):ssend)
   pkdat=ssw_patt_replace(pkdat,': "',':" ') ; octal protect
   ssname=[where(strpos(pkdat,'"name":') ne -1, ncnt),n_elements(pkdat)-1]
   pktemp={name:'',slotted:0,step:-1.}
   retval=replicate(pktemp,ncnt)
   for i=0,ncnt-1 do begin 
      kdat=pkdat(ssname(i):ssname(i+1)-1)
      retval(i).name=strtrim(strextract(kdat(0)+',','"name":"','"'),2)
      ssslot=where(strpos(kdat,'"slotted":') ne -1, slotcnt)
      if slotcnt gt 0 then retval(i).slotted = str2number(kdat(ssslot(0)))
      ssstep=where(strpos(kdat,'"step":') ne -1,stepcnt)
      if stepcnt gt 0 then retval(i).step=str2number(kdat(ssstep(0)))
   endfor
   if keyword_set(remove) then begin 
      jsonin(ss:ssend)=''
   endif
endif
return,retval
end
