function ssw_jsoc_drms2lc, meta, plot=plot, stack=stack, _extra=_extra, peak_label=peak_label
;+
;   Name: ssw_jsoc_drms2lc
;
;   Purpose: ssw/jsoc structure vector -> "light curve" structure, optionally summary plot
;
;   Input Parameters:
;      meta - meta/drms-like structure vector, (aia/iris...) - output from ssw_jsoc_time2data for examp.
;
;   Output:
;      returns nested struture w/per-WAVELNTH lightcuve {DATA_OBS,LCDATA}
;
;   Method:
;      Use IDL HISTOGRAM w/.WAVELNTH input & REVERSE_INDICES output keyword
;
;   Calling Context:
;      IDL> waves='94,131,171,193,304'                                                  
;      IDL> keys='date__obs,datamean,wavelnth,exptime,quality,aectype' ; OPTIONAL/minimal set for LC
;      IDL> ssw_jsoc_time2data,t0,t1,drms,waves=waves,key=keys,xquery='AECTYPE=0' ; exclude AEC frames
;          
;      IDL> lc=ssw_jsoc_drms2lc(drms) & help,lc,/str  <<< THIS FUNCTION CALL & Result
;         ** Structure <884b008>, 9 tags, length=48152, data length=48146, refs=1:
;         WAVES           INT       Array[5]
;         PEAK_TIME       STRING    Array[5]
;         PEAK_COUNTS     DOUBLE    Array[5]
;         MIN_COUNTS      DOUBLE    Array[5]
;         W94             STRUCT    -> <Anonymous> Array[1]
;         W131            STRUCT    -> <Anonymous> Array[1]
;         W171            STRUCT    -> <Anonymous> Array[1]
;         W193            STRUCT    -> <Anonymous> Array[1]
;         W304            STRUCT    -> <Anonymous> Array[1]
;
;   History:
;      5-Jan-2015 - S.L.Freeland - optimized via HISTOGRAM for wave-segmentation;
;      6-jan-2014 - S.L.Freeland - document, minimal cleanup - add /PLOT option
;-

if not required_tags(meta,'wavelnth,datamean,exptime,date_obs') then begin 
   box_message, 'Expect struture "like" jsoc AIA/IRIS...;
   return,-1
endif

waves=fix(all_vals(meta.wavelnth)) ; uniq waves, implied low->high order
nw=n_elements(waves)
retval={waves:waves,peak_time:replicate('',nw),peak_counts:replicate(0.d,nw),min_counts:replicate(0.d,nw)}
waves=[waves,max(waves)+1] ; add fake last wave (avoid down-line/loop IF)
whist=histogram(meta.wavelnth,min=0,reverse=wrev) ; per-wave bins, pw-subscripts in WREV
ndata=meta.datamean/meta.exptime ; normalize

for w=0,nw-1 do begin ; for each wave, except the last/fake...

   ssw=wrev[wrev[waves[w]]:wrev[waves[w+1]]-1] ; per-Wave subscripts
   dobs= meta[ssw].date_obs
   wdat=ndata[ssw]
   pss=(where(wdat eq max(wdat)))(0)
   retval.peak_time[w]=dobs[pss]
   retval.peak_counts[w]=wdat[pss]
   retval.min_counts[w]=min(wdat)
   lcdat={date_obs:dobs,data:wdat} ; per-Wave LC time/lcdata
   retval=add_tag(retval,lcdat,'w'+strtrim(waves[w],2)) ; append per-Wave LC tag
endfor

; ------------ optional plotting stuff for demo ----
stack=keyword_set(stack)
plot=keyword_set(plot) or stack
stack=stack or plot ; Only /STACK for now
if plot then begin ; optionally, produce summary plot
   savesys,/aplot 
   wdef,xx,1280,([512,nw*150])(stack),_extra=_extra
   time_window,meta.date_obs,out='ecs',t0x,t1x
   linecolors
   swaves=strtrim(retval.waves,2)
   goodcolors=[4,5,7,9,12,2] & goodcolors=[goodcolors,goodcolors]  
      !p.multi=[0,1,nw]
      if not required_tags(_extra,'charsize') then _extra=add_tag(_extra,1.5,'charsize')
      lpos=retval.min_counts+((retval.peak_counts-retval.min_counts)*.1)
      for w=0,nw-1 do begin 
         wind=tag_index(retval,'W'+swaves[w])
         utplot,retval.(wind).date_obs,retval.(wind).data,_extra=_extra,title='Wave='+swaves[w] + ' ' + arr2str([t0x,t1x],' -to -'),$
            /xstyle,/ystyle,timerange=[t0x,t1x]
         if keyword_set(peak_label) then evt_grid,retval.peak_time[w],labpos=lpos[w],/data,label='Peak ' + swaves[w]+'@' + $
            anytim(retval.peak_time[w],/time_only,/ecs,/trunc) ,labcol=goodcolors[w],align=1, color=2,linestyle=0
      endfor
   restsys,/aplot,/init
endif

; ---------------------------------------------------------------------------------

return,retval
end
   
   





