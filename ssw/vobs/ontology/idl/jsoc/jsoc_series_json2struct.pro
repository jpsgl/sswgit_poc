function jsoc_series_json2struct, jsoninx, keyword_only=keyword_only
;
;+
;   Name: jsoc_series_json2struct
;
;   Purpose: map jsoc json /SERIES_STRUCT -> idl nested structure ~equivilent 
;
;   Input Parameters:
;      jsonin - jsoc json from  /SERIES_STRUCT method
;
;   History:
;      circa Feb 2009 - S.L.Freeland
;      10-nov-2009 - handle null segments 
;      16-feb-2010 - additional primekeysinfo flexibility...
;       1-sep-2010 - fix headerstruct derivation (arbitrary # of primekeys ok)
;       6-jul-2011 - fix required, series_struct json alternate
;      25-apr-2013 - predefine NULL - (iris pkt series required)
;      16-jun-2014 - handle change in jsoc/json json library output.
;      28-jul-2014 - 'null' filter (some HMI series at least)
;       1-apr-2015 - keep up with the jsocs 
;-
;   

null=''

jsonin=jsoninx ; don't clobber input
ssnull=where(strpos(jsonin,' null') ne -1,ncnt)
if ncnt gt 0 then jsonin[ssnull]=str_replace(jsonin[ssnull],' null',' ""')

jlow=strlowcase(jsonin)
keyword=where(strpos(strlowcase(jlow),'"keywords":') ne -1)
keywords=jsonin(keyword(0):*)
keywords=keywords(0:(where(strpos(keywords,'}]') ne -1))(0))
dbindex=where(strpos(strlowcase(jlow),'"dbindex":') ne -1,dbcnt)

keylo=strlowcase(keywords)
ssn=where(strpos(keylo,'"name":') ne -1, ncnt)
sst=where(strpos(keylo,'"type":') ne -1, tcnt)
ssnt=where(strpos(keylo,'"note":') ne -1, tcnt)

keycomp=strtrim(strcompress(keywords),2) + ','
names=strextract(keycomp(ssn),'"name": "','",')
types=strextract(keycomp(sst),'"type": "','",')
notes=strextract(keycomp(ssnt),'"note": "','",')
keystruct={name:names,type:types,note:notes}
if keyword_set(keyword_only) then retval=keystruct else begin 
   sspki=where(strpos(jsonin,'primekeysinfo') ne -1, pkicnt)
   if pkicnt then begin 
      pkistr=jsoc_primekeys_info(jsonin,/remove)
   endif
   hend=keyword 
   header=jsonin(0:hend-1)
   head=strarrcompress(header)
   patts=str2arr('note,retention,unitsize,archive,stagingretention,tapegroup,primekeys,dbindex,owner')
   head=arr2str(head,'')
   spats='"'+patts+'":'
   for i=0,n_elements(patts)-1 do begin 
      head=ssw_patt_replace(head,spats(i),patts(i)+':')
   endfor
   estring=strmid(head,0,strlen(head)-1)+'}'
   if get_logenv('ssw_hpkb_jsoncheck') ne '' then stop,'estring'
   estat=execute('headstruct='+estring)
;  SEGMENTS
   segss=(where(strpos(jlow,'segments":') ne -1))(0)
   if strpos(jlow(segss),'[]') ne -1 then begin 
      segstruct='NULL'
   endif else begin 
   segments=jsonin(segss(0):*)
   segments=segments(0:(where(strpos(segments,'}]') ne -1))(0))
   stags=str2arr('name,units,protocol,dims,note')
   sname='"'+stags+'": "'
   segstruct={name:'',units:'',protocol:'',dims:'',note:''}
   ssn=where(strpos(segments,'"name":') ne -1, nseg)
   segstruct=replicate(segstruct,nseg)
   for i=0,n_elements(stags)-1 do begin 
     ssn=where(strpos(segments,sname(i)) ne -1) 
     if ssn(0) eq -1 then begin 
        ssn=where(strpos(segments,str_replace(sname(i),': "')) ne -1,nseg)
     endif
     segstruct.(i)=strextract(segments(ssn),sname(i),'"') 
   endfor
   endelse
;  Interval
   intss=(where(strpos(jlow,'interval":') ne -1))(0)
   interval=strarrcompress(strtrim(jsonin(intss:*),2))
   interval=interval(0:(where(strpos(interval,'}') ne -1))(0))
   interval(0)=ssw_strsplit(interval(0),'{',/tail)
   if strmid(last_nelem(interval),0,1) eq '}' then $
      interval=interval(0:n_elements(interval)-2)
   tags=ssw_strsplit(interval,'":',tail=values)
   tags=strtrim(str_replace(tags,'"'),2)
   estring='intstruct={'+ arr2str(strarrcompress(tags)+':'+strarrcompress(values),'') + '}
   estat=execute(estring)
   statss=where(strpos(jlow,'status":') ne -1)
   retval=headstruct
   if data_chk(pkistr,/struct) then begin
      retval=add_tag(retval,pkistr,'primekeysinfo',index='primekeys')
   endif
   retval=add_tag(retval,keystruct,'keywords')
   retval=add_tag(retval,segstruct,'segments')
   retval=add_tag(retval,intstruct,'interval')
   retval=add_tag(retval,str2number(jlow(statss)),'status')
endelse

return,retval
end





