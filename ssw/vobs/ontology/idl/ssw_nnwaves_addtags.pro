function ssw_nnwaves_addtags, nnstr, debug=debug

debug=keyword_set(debug)

if not required_tags(nnstr,'date_obs,noaa_ar,location') then begin 
   box_message, 'need vector of one or more output structures from ssw_nnwaves.pro; bailing...'
   return,''
endif

tnames=tag_names(nnstr)
ssw=where(str2number(tnames) ne 0,wcnt)
wnames=tnames[ssw]

types='_'+str2arr('int,rdiff,bdiff')
tcnt=n_elements(types)
newtags=reform(str_perm(wnames,types))
create_struct_temp,ntstr,'',newtags,replicate('a',n_elements(newtags))
ntstr=replicate(ntstr,n_elements(nnstr))
i=-1
for w=0,wcnt-1 do begin 
   for t=0,tcnt-1 do begin ; one day, I'll vectorize this if Moore's law/IDL can't keep up with Nariaki's typing...
      i=i+1   
      ntstr.(i)=reform(nnstr.(ssw[w])(t,*))
   endfor
endfor
if debug then stop,'ntstr

retval=join_struct(nnstr,ntstr)

return,retval
end
