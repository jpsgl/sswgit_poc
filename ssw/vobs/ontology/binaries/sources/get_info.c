#include <fitsio.h>
#include <idl_export.h>

char *get_info(int argc, char *argv[])
{
    fitsfile *fp;
    int status = 0;
    int status2 = 0;
    int scaled;
    int hdutype, bitpix;
    int hduok, dataok;
    double bscale, bzero;
    static char errmsg[81];
    char *filename = (char *) argv[0];
    int *hdunum = (int *) argv[1];
    int *dtype = (int *) argv[2];
    int *naxis = (int *) argv[3];
    long *naxes = (long *) argv[4];
    short *do_chksum = (short *) argv[5];

    fits_open_image(&fp, filename, READONLY, &status);
    if (status) {
	fits_get_errstatus(status, errmsg);
	return errmsg;
    }

    fits_get_hdu_type(fp, &hdutype, &status);
    if (hdutype == IMAGE_HDU)
	fits_get_img_param(fp, 8, &bitpix, naxis, naxes, &status);

    if (!naxis || hdutype != IMAGE_HDU) {
	sprintf(errmsg, "no image found in FITS file");
	return errmsg;
    }

    if (*do_chksum) {
	fits_verify_chksum(fp, &dataok, &hduok, &status);
	if (dataok == -1) {
	    sprintf(errmsg, "bad DATASUM in header");
	    return errmsg;
	}
	if (hduok == -1) {
	    sprintf(errmsg, "bad CHECKSUM in header");
	    return errmsg;
	}
    }

    fits_get_hdu_num(fp, hdunum);
    --*hdunum;

    fits_get_img_equivtype(fp, &bitpix, &status);

    fits_read_key(fp, TDOUBLE, "BSCALE", &bscale, NULL, &status2);
    if (status2) {
	bscale = 1.0;
	status2 = 0;
    }
    fits_read_key(fp, TDOUBLE, "BZERO", &bzero, NULL, &status2);
    if (status2) {
	bzero= 0.0;
	status2 = 0;
    }
    if (bzero == 0.0 && bscale == 1.0)
	scaled = 0;
    else
	scaled = 1;

    switch (bitpix) {
	case BYTE_IMG: *dtype = scaled ? IDL_TYP_FLOAT : IDL_TYP_BYTE; break;
	case SBYTE_IMG: *dtype = scaled ? IDL_TYP_FLOAT : IDL_TYP_INT; break;
	case SHORT_IMG: *dtype = scaled ? IDL_TYP_FLOAT : IDL_TYP_INT; break;
	case USHORT_IMG: *dtype = scaled ? IDL_TYP_FLOAT : IDL_TYP_UINT; break;
	case LONG_IMG: *dtype = scaled ? IDL_TYP_DOUBLE : IDL_TYP_LONG; break;
	case ULONG_IMG: *dtype = scaled ? IDL_TYP_DOUBLE : IDL_TYP_ULONG; break;
	case LONGLONG_IMG: *dtype = scaled ? IDL_TYP_DOUBLE : IDL_TYP_LONG64; break;
	case FLOAT_IMG: *dtype = IDL_TYP_FLOAT; break;
	case DOUBLE_IMG: *dtype = IDL_TYP_DOUBLE; break;
	default: 
	    sprintf(errmsg, "unsupported data type (bitpix = %d)", bitpix);
	    return errmsg;
    }

    fits_close_file(fp, &status);

    if (status) {
	fits_get_errstatus(status, errmsg);
	return errmsg;
    } else
	return NULL;
}
