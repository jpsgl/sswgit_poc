Coverage_Ingest
===============

This README file covers usage for the ingest program for the coverage database.
The coverage_ingest program will ingest both planning and observation events
for TRACE, SOT, and XRT.


Run Instructions:
-----------------
At shell, type:
java -jar /net/helios/home/hinode/bin/coverage_ingest.jar <path to planning files or observation/movie files>
* Note that an upper level directory can be given, and the program will iterate
through any subdirectories finding acceptable files to ingest.

Run Example:
------------
> java -jar /net/helios/home/hinode/bin/coverage_ingest.jar /net/vestige2/archive/trace/metadata/trace_planning/2003/11/
(This will ingest all TRACE planning events in November of 2003)
> java -jar /net/helios/home/hinode/bin/coverage_ingest.jar /net/vestige2/archive/trace/wwwmovies_p/voevent_movies_trace/2003/11/
(This will ingest all Trace utility/movie events in November of 2003)
