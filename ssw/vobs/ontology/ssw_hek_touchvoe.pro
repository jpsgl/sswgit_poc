function ssw_hek_touchvoe, voe, expire=expire, ingest=ingest, status=status, backup=backup
;
;+
;   Name: ssw_hek_touchvoe
;
;   Purpose: update <Date></Date> with current UT, file or xml array
;
;   Input Parameters:
;      voe - VOE .xml file name -or- xml array 
;
;   Output
;      function returns xml string array with <Date><currentUT></Date>
;   
;   Keyword Parameters:
;      rewrite (switch) - if set, and VOE is file, rewrite with new <Date>
;      ingest (swich) -if set, and VOE is file, ingest -> HEK ; implies rewrite
;      status (output) - 0=failure, 1=success
;      backup (switch) - if set , make a backup with name <voename>.xml.YYYYMMDD_HHMMSS (ut)
;
;   History:
;      circa 2011 - S.L.Freeland 
;      18-apr-2015 - added doc header, allow array-only option
;      21-jul-2016 - allow coverage ingests (hcr) in addition to her voe, add /backup switch
;-
;
if n_params() eq 0 then begin 
   box_message,'Need xml array or .xml filename
   return,''
endif
file_in=file_exist(voe)
case 1 of 
   file_in: voedat=rd_tfile(voe)
   data_chk(voe,/string): voedat=voe ; xml array input
   else: begin
      box_message,'Need xml array or .xml filename
      return,''
   endcase
endcase

udat=strupcase(voedat)
dss=where(strmatch(voedat,'*<Date>*</Date>*') ,dcnt)
if dcnt ne 1 then begin 
   box_message,'Unexpected VOE...
   return,voedat
endif

old=strextract(voedat[dss],'<Date>','</Date>')
new=reltime(/now,out='ccsds')
voedat_orig=voedat
voedat[dss]=str_replace(voedat[dss],old,new)

if keyword_set(expire) then begin 
   box_message,'Not yet implemented...
   wss=where(strpos(voedat,'<Why') ne -1, wcnt)
endif

ingest=file_in and keyword_set(ingest) 
rewrite=keyword_set(rewrite) and file_in or ingest

if rewrite then begin 
   if keyword_set(backup) then file_append,voe+'_'+time2file(reltime(/now),/sec),voedat_orig,/new ; backup on request
   file_append,voe,voedat,/new
   ssutil=where(strmatch(voedat,'*role=*utility*',/fold_case),utilcnt)
   if ingest then begin 
      case 1 of 
         utilcnt gt 0: ssw_coverage_ingest,voe,/add_link
         else: ssw_hpkb_voeupload,voe,/curl, revise=found
      endcase
   endif
endif

return,voedat
end
