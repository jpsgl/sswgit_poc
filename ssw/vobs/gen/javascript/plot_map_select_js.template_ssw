<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html><head>
  
  
  <title>Region selection with JavaScript - Demo</title><style type="text/css">
<!--
table { font-family:Arial, Helvetica, sans-serif; font-size:10pt; }
-->
  </style></head>
<body>
<div align="center">
  <table bgcolor="#ddeeff" border="0" cellpadding="0" cellspacing="0" width="800">
    <tbody><tr>
      <td><div style="font-size: 14pt;" align="center"><strong>Observation image region selector.</strong><br>
        Greg Paperin, UCL<br>
Updated: Bob Bentley, UCL-MSSL
SSW/CoSEC Service: Sam Freeland, LMSAL<br>
SSW/IDL Mapping Engine: Dominic Zarro, L-3 Communications
</div>
		<hr>
        <p align="center"><strong>Explanation:</strong></p>

        <p align="justify">This
page is implemented using standard HTML and JavaScript, i.e. it will
run in any browser which supports DHTML (i.e. layers) and JavaScript. 
It has been tested with Firefox 1.0 and IE 6.0, both running on WinXP
with SP2 as well as with Safari and Netscape 7.2 running on Max OS X 10.3.5. 
<!--
It should
be tested on Opera and popular Linux browsers and in Win systems
without XP-SP2, but until now, there was no possibility to do so.
-->
</p>
        <p align="justify">Define the selected area by placing the cursor over one corner, 
depressing mouse select key and dragging the cursor to the other extreme of the
desired field of view. Repeat this if you wish to revise the selection, then
press the GO button. </p><p>
The GO-button can obviously have any label and pass the selected image
and/or physical coordinates together with other information to a web
page, a Servlet or a JSP.

		</p><hr>
		<div align="center">
		  <table border="0" cellpadding="10" cellspacing="0">
            <tbody><tr>
              <td align="left" valign="middle" width="PNGXSIZE"><div style="position: relative; width: PNGXSIZEpx; height: PNGYSIXEpx; z-index: 1; background-color: rgb(0,0,255);" id="SelectLayer"><img alt="Observation image" src="IMAGEPNG" style="cursor: crosshair;" border="1" height="" width="PNGXSIZE"></div></td>
              <td align="center" valign="top"><strong>Selection:</strong>
                <br><br><table border="0" cellpadding="0" cellspacing="0">
			  <tbody><tr>
                  <td colspan="2" align="center">Physical (arc seconds): </td>
                  </tr>
                <tr>
                  <td align="center" width="100">X1: 
                    <input maxlength="5" size="5" value="--" id="physX1" name="physX1" type="text"></td>
                  <td align="center" width="100">Y1:
                    <input maxlength="5" size="5" value="--" id="physY1" name="physY1" type="text"></td>
                </tr>
                <tr>
                  <td align="center" width="100">X2:
                    <input maxlength="5" size="5" value="--" id="physX2" name="physX2" type="text"></td>
                  <td align="center" width="100">Y2:
                    <input maxlength="5" size="5" value="--" id="physY2" name="physY2" type="text"></td>
                </tr>
                <tr>
                  <td> </td>
                  <td align="right"><input value="As typed" id="TypePhysical" name="TypePhysical" type="submit"></td>
                </tr>
              </tbody></table>
			  <br>
			  <table border="0" cellpadding="0" cellspacing="0">
			  <tbody><tr>
                  <td colspan="2" align="center">Image (pixels): </td>
                  </tr>
                <tr>
                  <td align="center" width="100">X1: 
                    <input maxlength="5" size="5" value="--" id="imgX1" name="imgX1" type="text"></td>
                  <td align="center" width="100">Y1:
                    <input maxlength="5" size="5" value="--" id="imgY1" name="imgY1" type="text"></td>
                </tr>
                <tr>
                  <td align="center" width="100">X2:
                    <input maxlength="5" size="5" value="--" id="imgX2" name="imgX2" type="text"></td>
                  <td align="center" width="100">Y2:
                    <input maxlength="5" size="5" value="--" id="imgY2" name="imgY2" type="text"></td>
                </tr>
                <tr>
                  <td> </td>
                  <td align="right"><input value="As typed" id="TypeImage" name="TypeImage" type="submit"></td>
                </tr>
              </tbody></table>
			  <br>
			  <input disabled="disabled" value="GO" id="Go" name="Go" type="submit">
			  </td>
            </tr>
          </tbody></table>
	    </div></td>
    </tr>
  </tbody></table>
</div>





<script src="wz_jsgraphics.js" type="text/javascript"></script>
<script language="JavaScript1.2" type="text/javascript">
<!--
	// DYNAMICALLY GENERATED PARAMETERS (START) 
        // ORIG_FITS_URL=XXX
	var imageWidth = IMAGEWIDTH;
	var imageHeight = IMAGEHEIGHT;
	var imagePhysicalTop =    IMAGEPHYSICALTOP;
	var imagePhysicalBottom = IMAGEPHYSICALBOTTOM;
	var imagePhysicalLeft =   IMAGEPHYSICALLEFT;
	var imagePhysicalRight =  IMAGEPHYSICALRIGHT;
        var imagedate = 'IMAGEDATE';
	// DYNAMICALLY GENERATED PARAMETERS (END) 

	var IE = document.all ? true : false;
	document.getElementById("SelectLayer").onmousemove = updateSelectionBox;
	document.getElementById("SelectLayer").onmousedown = startSelectionBox;
	document.getElementById("SelectLayer").onmouseup = stopSelectionBox;
	
	document.getElementById("TypeImage").onclick = imageCoordsTyped;
	document.getElementById("TypePhysical").onclick = physicalCoordsTyped;
	document.getElementById("Go").onclick = submitSelection;

	var startX = 0;
	var startY = 0;
	var stopX = 0;
	var stopY = 0;
	var selectingNow = false;

	var jg = new jsGraphics("SelectLayer");
	jg.setColor("Red");

	function updateSelectionBox(e) {
		if (!selectingNow)
    		return;
	
		var x, y;
		if (IE) {
			x = event.offsetX;
			y = event.offsetY;
		} else {								
			x = e.layerX;
			y = e.layerY;			
		}
		
		// Update only if mouse didnt move too far to prevent jumpung of the box:
		if (Math.sqrt(Math.pow(Math.abs(x - stopX), 2) + Math.pow(Math.abs(y - stopY), 2)) > 50)
			return;
		
		stopX = x;
		stopY = y;
		selectionUpdated();
	} // updateSelectionBox

	function selectionUpdated() {
	
		var x1, y1, x2, z2;
		if (stopX >= startX) {
    		x1 = startX; x2 = stopX;
		} else {
		x1 = stopX; x2 = startX;
  		}
  		if (stopY >= startY) {
    		y1 = startY; y2 = stopY;
  		} else {
    		y1 = stopY; y2 = startY;
		}
		
		if (x1 < 0) x1 = 0;
		if (x1 >= imageWidth) x1 = imageWidth-1;
		if (x2 < 0) x2 = 0;
		if (x2 >= imageWidth) x2 = imageWidth-1;
		if (y1 < 0) y1 = 0;
		if (y1 >= imageHeight) y1 = imageHeight-1;
		if (y2 < 0) y2 = 0;
		if (y2 >= imageHeight) y2 = imageHeight-1;
		
		jg.clear();
		if (x1 == x2 && y1 == y2) {
			document.getElementById("imgX1").value="--";
			document.getElementById("imgY1").value="--";
			document.getElementById("imgX2").value="--";
			document.getElementById("imgY2").value="--";
			document.getElementById("physX1").value="--";
			document.getElementById("physY1").value="--";
			document.getElementById("physX2").value="--";
			document.getElementById("physY2").value="--";
			document.getElementById("Go").disabled = true;
			return;
		}
	
		document.getElementById("Go").disabled = false;
		jg.drawRect(x1, y1, x2-x1, y2-y1);
  		jg.paint();
		document.getElementById("imgX1").value=""+x1;
		document.getElementById("imgY1").value=""+y1;
		document.getElementById("imgX2").value=""+x2;
		document.getElementById("imgY2").value=""+y2;
		
		var scaleX = (imagePhysicalRight - imagePhysicalLeft) / imageWidth;
		var scaleY = (imagePhysicalTop - imagePhysicalBottom) / imageHeight;
		var physX1 = imagePhysicalLeft + x1 * scaleX;
		var physY1 = imagePhysicalTop - y1 * scaleY;
		var physX2 = imagePhysicalLeft + x2 * scaleX;
		var physY2 = imagePhysicalTop - y2 * scaleY;
		document.getElementById("physX1").value=""+physX1;
		document.getElementById("physY1").value=""+physY1;
		document.getElementById("physX2").value=""+physX2;
		document.getElementById("physY2").value=""+physY2;
	} // selectionUpdated

	function stopSelectionBox(e) {
		selectingNow = false;
		selectionUpdated();
	} // stopSelectionBox


	function startSelectionBox(e) {
		if (IE) {
			startX = event.offsetX;
			startY = event.offsetY;
		} else {
			startX = e.layerX;
			startY = e.layerY;
 		} 
		stopX = startX;
		stopY = startY;
		selectingNow = true;
		selectionUpdated();
	} // startSelectionBox
	
	function physicalCoordsTyped(e) {
		var x1 = parseInt(document.getElementById("physX1").value);
		var y1 = parseInt(document.getElementById("physY1").value);
		var x2 = parseInt(document.getElementById("physX2").value);
		var y2 = parseInt(document.getElementById("physY2").value);
		var scaleX = (imagePhysicalRight - imagePhysicalLeft) / imageWidth;
		var scaleY = (imagePhysicalTop - imagePhysicalBottom) / imageHeight;
		var imgX1 = (x1 - imagePhysicalLeft) / scaleX;
		var imgY1 = (y1 - imagePhysicalTop) / -scaleY;
		var imgX2 = (x2 - imagePhysicalLeft) / scaleX;
		var imgY2 = (y2 - imagePhysicalTop) / -scaleY;
		coordsTyped(imgX1, imgY1, imgX2, imgY2);
	} // physicalCoordsTyped
	
	function imageCoordsTyped(e) {
		var x1 = parseInt(document.getElementById("imgX1").value);
		var y1 = parseInt(document.getElementById("imgY1").value);
		var x2 = parseInt(document.getElementById("imgX2").value);
		var y2 = parseInt(document.getElementById("imgY2").value);
		coordsTyped(x1, y1, x2, y2);
	} // imageCoordsTyped
	
	function coordsTyped(x1, y1, x2, y2) {
		if (isNaN(x1)) {
			alert("The value '" + document.getElementById("imgX1").value + "'specified for X1 is not a valid integer.");
			selectionUpdated();
			return;
		} else if (isNaN(y1)) {
			alert("The value '" + document.getElementById("imgY1").value + "'specified for y1 is not a valid integer.");
			selectionUpdated();
			return;
		} else if (isNaN(x2)) {
			alert("The value '" + document.getElementById("imgX2").value + "'specified for X2 is not a valid integer.");
			selectionUpdated();
			return;
		} else if (isNaN(y2)) {
			alert("The value '" + document.getElementById("imgY2").value + "'specified for Y2 is not a valid integer.");
			selectionUpdated();
			return;
		}
		startX = x1; startY = y1;
		stopX = x2; stopY = y2;
		selectionUpdated();
	} // coordsTyped
	
	function submitSelection() {
                var url = "ssw_service_cgi/SSW_SERVICE"
                        + "?param1=ORIG_FITS_URL"
                        + "&reference=" 
                        + "physX1=" +  document.getElementById("physX1").value
                        + ",physY1=" + document.getElementById("physY1").value
                        + ",physX2=" + document.getElementById("physX2").value
                        + ",physY2=" + document.getElementById("physY2").value
			+ ",imagedate=" + imagedate
                        + "&extra=/log,grid=10,gcolor=100,margin=.1"
                        + "&cosec_mode=2";
		location.href = url;
	} // submitSelection
//-->
</script>

</body></html>
