function make_plot_map_select_js, map, template=template, pngfile=pngfile, $
   orig_fits_url=orig_fits_url, service_cgi=service_cgi, ssw_service=ssw_service 
;
;+
;   Name: make_plot_map_select_js
;
;   Purpose: generate javascript gui for solar feature selection by client
;
;   Input Parameters:
;      map - a solar image map per Dominic Zarro mapping suite
;
;   Keyword Parameters
;      template - desired JS template (default from $SSW/vobs/gen/javascript )
;      pngfile - if not supplied, generate from current Z-buffer image
;      orig_fits_url - url implied by 'map' (inserted in JS for future use)
;      service_cgi - server cgi URL
;      ssw_service - desired downline service (will get the subfield coords)
;
;  Circa 15-May-2005 - S.L.Freeland - prototype
;  1-Jun-2005 - add ssw_service and service_cgi keywords (enable downline pipe)
; 29-Jun-2005 - add a doc header
;
;  Restrictions:
;     run after plot_map->Zbuffer
;
;-
;      
status=0
if not valid_map(map) then begin 
   box_message,'Function requires valid input map...'
   return,''
endif

ssw=keyword_set('ssw_service')

deftemp='plot_map_select_js.template' + $
   (['','_ssw'])(ssw)
vobs=concat_dir(concat_dir(concat_dir('$SSW','vobs'),'gen'),'javascript')
vobstemp=concat_dir(vobs,deftemp)
case 1 of
   file_exist(template):     
   file_exist(deftemp): template=concat_dir(curdir(),deftemp)
   file_exist(vobstemp): template=vobstemp
   else: begin 
      box_message,'Cannot find JavaScript template file...'
      return,''
   endcase
endcase

jsdata=rd_tfile(template(0))
if data_chk(orig_fits_url,/string,/scalar) then begin
   ss=where(strpos(jsdata,'ORIG_FITS_URL=XXX') ne -1,sscnt)
   if sscnt eq 1 then jsdata(ss)=str_replace(jsdata(ss),'XXX',orig_fits_url) 
   ss=where(strpos(jsdata,'=ORIG_FITS_URL') ne -1,sscnt)
   if sscnt eq 1 then jsdata(ss)= $ 
      ssw_strsplit(jsdata(ss),'ORIG') + url_encode(orig_fits_url)  +'"'
endif else box_message,'NO ORIG_FITS URL'

scaling=get_plot_map_select_scale(map)
tnames=tag_names(scaling)
for i=0,n_elements(tnames)-1 do begin 
  ss=where(strpos(jsdata,tnames(i)) ne -1,sscnt)
  if sscnt eq 1 then begin 
     jsdata(ss)=str_replace(jsdata(ss),tnames(i),strtrim(scaling.(i),2))
  endif
endfor

if ssw then begin ; coordinates past -> ssw_service
   ss_serv=where(strpos(jsdata,'SSW_SERVICE') ne -1,scnt)
   if scnt eq 1 then jsdata(ss_serv)=$
      str_replace(jsdata(ss_serv),'SSW_SERVICE',ssw_service)
   ssw_service_cgi=get_logenv('ssw_service_cgi') ; local environmental?
   case 1 of 
      data_chk(service_cgi,/string,/scalar):    ; user supplied
      ssw_service_cgi ne '': service_cgi=ssw_service_cgi
      else: service_cgi='http://www.lmsal.com/solarsoft/cgi-diapason'
   endcase
   sscgi=where(strpos(jsdata,'ssw_service_cgi') ne -1,sscnt)
   if sscnt eq 1 then jsdata(sscgi)=$
      str_replace(jsdata(sscgi),'ssw_service_cgi',service_cgi)
endif

; insert jslib routine if not a copy in "current" directory
wzjs='wz_jsgraphics.js'
if not file_exist(wzjs) then begin    ; insert an in-line copy from $SSW_VOBS
   vobsv=concat_dir(vobs,wzjs)
   if file_exist(vobsv) then begin 
      ss=where(strpos(jsdata,wzjs) ne -1, sscnt)
      if sscnt eq 1 then begin 
         jsdata=[jsdata(0:ss(0)-1),'<script type="text/javascript">',$
                    rd_tfile(vobsv),'</script>',jsdata(ss(0)+1:*)]
      endif else box_messsage,'Unexpected problem with WZ-JS parse??'
   
   endif else box_message,'Could not find WZ-javascript module...'

endif

; other misc substitutions...

subarr=['PNGXSIZE','PNGYSIZE']   ; search strings
usearr=strtrim([!d.x_size,!d.y_size],2) ; corresponding substitution strings
for i=0,n_elements(subarr)-1 do begin
  ss=where(strpos(jsdata,subarr(i)) ne -1, sscnt)
  if sscnt gt 0 then jsdata(ss)=str_replace(jsdata(ss),subarr(i),usearr(i))
endfor


if not file_exist(pngfile) then begin 
   pngfile='ssw_plot_map2js_'+time2file(map.time)+'.png'
   zbuff2file2,pngfile
endif
sssrc=where(strpos(jsdata,'IMAGEPNG') ne -1,sscnt)

ssw_client=get_logenv('ssw_client') ; top of ssw_client tree
if ssw_client eq '' then ssw_client='ssw_client' ; defaul=htdocs/ssw_client
if sscnt eq 1 then jsdata(sssrc)=$
      str_replace(jsdata(sssrc),'IMAGEPNG', $
         concat_dir(concat_dir(ssw_client,'data'), $
         ssw_strsplit(pngfile,'/',/last,/tail)))


status=1
return,jsdata
end
