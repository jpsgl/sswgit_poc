;+
; NAME:
;	decode_VOTable
; PURPOSE:
;	Converts VOTable string into an IDL structure
; CALLING SEQUENCE:
;	struct = decode_VOTable(votable_string)
;
; INPUTS:
;	votable_string	string read from file or received through SOAP
; KEYWORDS
;	quiet	removes some debug messages
; HISTORY:
;       2 Dec 2003  RDB  First version
;      20 Feb 2004  RDB  Added short, redefined INT, LONG - fudge...
;       9 Aug 2004  RDB  changed arr2str to strjoin; added comvert time
;      23 Aug 2004  RDB  optimized code, removed most SSW calls to increase
;			 speed; incl. changes suggested by S.Zharkov
;      22 Nov 2004  RDB  Made more forgiving about absence of <?xml...> string and
;                        whether FIELD variable type defined...
;
;-

function vot_strsegs,tabledata,expr,count=count

;  routine similar to strsplit but low level and only returns pointers
;  this makes it much faster for very long strings... 

pntrs = lonarr(3,20000)

pos=-1
for kpnt=0,19999 do begin
  pos = strpos(tabledata,expr,pos+1)
  if pos lt 0 then goto, done
  pntrs (0,kpnt) = pos
endfor

done:
pntrs(0,kpnt) = strlen(tabledata)
pntrs(1,0:kpnt-1) = pntrs(0,1:kpnt)-1	;pntrs(0,0:kpnt-1)
pntrs = pntrs(*,0:kpnt-1)
pntrs(2,*) = pntrs(1,*)-pntrs(0,*)

count = kpnt

return, pntrs
end


function  decode_votable, input_string, quiet=quiet

;  This is a cludge until someone writes the proper routine
;  However, it has the advantage that it runs under any version of IDL...
  
ccstart=systime(/sec)

qq = input_string

;  use strjoin rather than arr2str which is very slow...
if n_elements(qq) gt 1 then qq=strjoin(qq,/single)

;  Check there is VOTable header
p1 = strpos(strupcase(qq),'<VOTABLE VERSION')
if p1(0) ge 0 then begin              ; was gt!
  p2 = strpos(strmid(qq,p1,100),'>')
  if not keyword_set(quiet) then print,strmid(qq,0,p1+p2+1)
endif else begin
  message,'NOT a VOTable file',/info
  return,-1
  endelse

;  Get VOTABLE part of file
VOTABLE=strmid(qq,strpos(qq,'<VOTABLE>')+9,strpos(qq,'</VOTABLE>')-strpos(qq,'<VOTABLE>')-9)

;  could be other stuff in here related to definitions...
;
;  TABLE is really within RESOURCE...
;  Currently, only looks for TABLEDATA and makes structure by what is
;  defined in the TABLE definition...

;  Get TABLE part of VOTABLE
TABLE=strmid(VOTABLE,strpos(votable,'<TABLE>')+7,strpos(votable,'</TABLE>')-strpos(votable,'<TABLE>')-7)

; this may be wrong if there is a <DESCRIPTION> field!!
table_head=STRMID(TABLE,strpos(TABLE,'FIELD')-1,strpos(TABLE,'<DATA'))
fields = strsplit(table_head,'<FIELD',/extr,/regex)

;fields = fields(1:*)
if not keyword_set(quiet) then begin
  help,fields
  print,fields,format='(x,a)'
  endif
ncol = (n_elements(fields))[0]    ;No. of columns in TABLEDATA

;  Get DATA part within TABLE
;;?DATA=strmid(table,strpos(table,'<DATA>')+6,strpos(table,'</DATA>')-strpos(table,'<DATA>')-6)

;  Get TABLEDATA part  -  skip extraction layer for DATA...
TABLEDATA=strmid(table,strpos(table,'<TABLEDATA>')+11,strpos(table,'</TABLEDATA>')-strpos(table,'<TABLEDATA>')-11)

;  row by row, pick out value in each table elements and put into output array
if strlen(tabledata) eq 0 then begin
  message,'VOTable contains nothing in TABLEDATA area',/cont
  return,''
endif 

pntrs = vot_strsegs(tabledata,'<TR>', count=nrow)
table_array = strarr(ncol,nrow)

btab = byte(tabledata)
for jrow=0,nrow-1 do begin
  brow = btab(pntrs(0,jrow)+4:pntrs(1,jrow)-5)		;eliminate <TR> & </TR>
  pp = vot_strsegs(string(brow),'<TD>')
  for jcol=0,ncol-1 do $
    if pp(2,jcol)-9 ge 0 then $                         ;support case of zero length string
    table_array(jcol,jrow) = string(brow(pp(0,jcol)+4:pp(1,jcol)-5))	; eliminate <TD> & </TD>
endfor
  
; form the structure by building a string as work through the FIELDs
ss = '{'
for jcol=0,ncol-1 do begin
  if jcol gt 0 then ss=ss+', '            ;variable seperator

;  separate different parts of FIELD definition
;  CLUDGE - assume only spaces between definitions, none elsewhere
  zz=strsplit(strtrim(FIELDS(jcol),2),' ',/extr)

;  extract name and data type
  pname = (where(strpos(zz,"name") ge 0))[0]
  name = (strsplit(zz(pname),'"',/extr))[1]      ;variable name
  bname = byte(name) & wmin = where(bname eq 45b)    ;'-' char not allowed! => '$'
  if wmin(0) ge 1 then begin  ; first char of '_' not allowed!
      bname(wmin) = 36b & name = string(bname)
    endif
  type = 'char'   ; default to 'char' in case not defined
  pdtype = (where(strpos(zz,"datatype") ge 0))[0]
  if pdtype gt -1 then type = strlowcase((strsplit(zz(pdtype),'"',/extr))[1])     ;variable type

;  Note that VOTable uses slightly different meanings of variable
;  types to IDL and we need to fudge it a little
  ss = ss+name+':'           ; add to string that will define struct
  if type eq 'char'           then ss=ss+'" "'
  if type eq 'unsignedbyte'   then ss=ss+'0B'
  if type eq 'float'          then ss=ss+'0.0'
  if type eq 'double'         then ss=ss+'0.D0'
  if type eq 'short'          then ss=ss+'0'        ; VOTable I*2
  if type eq 'int'            then ss=ss+'0L'       ; VOTable I*4
  if type eq 'long'           then ss=ss+'0LL'      ; VOTable I*8
  if type eq 'boolean'        then ss=ss+'" "'      ;?? not sure how to deal
  endfor
ss=ss+'}

qflag = execute('pp='+ss)
if qflag eq 0 then message,'HELP - problem with structure'
struct = replicate(pp,nrow)

;  load the structure
;  CLUDGE - should really pass names from above - is this necessary...
tags = tag_names(struct)
for jcol = 0,ncol-1 do begin
  jflag = execute('struct.'+tags(jcol)+'=reform(table_array(jcol,*))')
  endfor

if not keyword_set(quiet) then begin
  help,struct
  help,/st,struct
  endif

print,'VOTable conversion took:',systime(/sec)-ccstart,' secs'
print,'TABLEDATA was',fix(ncol),' cols x',fix(nrow),' rows'

ans=''
;read,'Pause: ',ans

return,struct
end
