function vobs_Struct2VOTable2, struct, file=filename, $
		 votable_description = votable_description, $
		 resource_description = resource_description, $
		 table_description = table_description

;+
; NAME:
;         vobs_Struct2VOTable
; PURPOSE:
;         Converts IDL structure into VOTable file
;
; CALLING SEQUENCE:
;         vobs_Struct2VOTable, struct, filename
;
; KEYWORDS
;         votable_description     puts description just after <VOTABLE ...>
;         resource_description    puts description just after <RESOURCE>
;         table_description       puts description just after <TABLE>
;
; HISTORY:
;         20-Feb-2004  RDB  First version
;         01-Dec-2004  rdb  End FIELD with />
;          9-feb-2005  S.L.Freeland - made function equiv + memory operation
;                      (file writing is optional, not default) 
;
;-

;
;  Quick and dirty routine to make an IDl structure and create a VOTable file
;  Fudge until someone does it better
;  *** I still have concerns about the type match between IDL and VOTable
;
  
;  Check number of parameters and their type
if not data_chk(struct,/struct) then begin  
    box_message,'IDL> vot=vobs_struct2votable2(struct [,options])' 
    return,''
endif


if keyword_set(votable_description) then descr0=votable_description else descr0=''
if keyword_set(resource_description) then descr1=resource_description else descr1=''
if keyword_set(table_description) then descr2=table_description else descr2=''

;  get some useful things about the structure
nel   = n_elements(struct)
ntag  = n_tags(struct)
names = tag_names(struct)

;
;     Mapping IDL variable types (returned by size) to VOTable/FITS types
;
; Code  Data Type                     Included?   VOTable/FITS
; 0     Undefined                        N        -
; 1     Byte                             Y        unsignedByte
; 2     Integer                          Y        short
; 3     Longword integer                 Y        int
; 4     Floating point                   Y        float
; 5     Double-precision floating        Y        double
; 6     Complex floating                 Y        floatComplex
; 7     String                           Y        char
; 8     Structure                        N        -
; 9     Double-precision complex         Y        doubleComplex
; 10    Pointer                          N        -
; 11    Object reference                 N        -
; 12    Unsigned Integer                 ?        -
; 13    Unsigned Longword Integer        ?        -
; 14    64-bit Integer                   Y        long
; 15    Unsigned 64-bit Integer          ?        -
;
; VOTable types not defined:  unicodeChar, bit, boolean
;

vtype = ['','unsignedByte','short','int', $
         'float','double','floatComplex','char', $
         '', 'doubleComplex','','', $
         '','','long','']

votab=['<?xml version="1.0"?>',$
       '<!DOCTYPE VOTABLE SYSTEM "http://us-vo.org/xml/VOTable.dtd">',$
       '<VOTABLE version="1.0">', $
       ' <DESCRIPTION>', $
        string(descr0), $
       '   Created: '+systime(), $
       ' </DESCRIPTION>', $
       ' <RESOURCE>', $
       '  <DESCRIPTION>'+string(descr1)+'</DESCRIPTION>', $
       '  <TABLE>', $
       '   <DESCRIPTION>'+string(descr2)+'</DESCRIPTION>']

;  now create the VOTable FIELD definitions using information about variable types, etc.
qq=''
for j=0,ntag-1 do begin
  typ = size(struct.(j),/type)
  qq = [qq,'<FIELD' $
          +' name="'+names(j)+'"' $
          +' datatype="'+vtype(typ)+'"/>']
endfor
votab=[temporary(votab),qq(1:*)]

;  and write the elements of each instance of the structure to a row
qq=''
for j=0,ntag-1 do qq=qq+['<TD>'+strtrim(string(struct.(j)),2)+'</TD>']
qq = '<TR>'+qq+'</TR>'
votab=[temporary(votab), $
          '   <DATA>', $
          '    <TABLEDATA>', $
          '     '+qq, $
          '    </TABLEDATA>', $
          '   </DATA>', $
          '  </TABLE>', $
          ' </RESOURCE>', $
          '</VOTABLE>' ]

return,votab
end
