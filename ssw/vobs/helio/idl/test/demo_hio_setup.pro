;    The first Object allows you to look at light curves and event lists to establish 
;    a time interval, select a set of instruments and retrieve the list of URLs

hio_setup = obj_new('hio_setup')

print,''
yesnox,'Would you like to see the crib sheet?',ians,'no'
if ians then $
  hio_setup -> help

;--    Establish time interval

box_message,'You need to define a base time interval'
hio_setup -> set_basetime			; choose from predefined set of times

;  ** Note: If you have not set a base time it will ask for one when you plot, etc.

;--    Plot light-curve covering selected time interval

print,''
yesnox,'Do you want to plot a lightcurve?',ians,'yes'
if ians then $
  hio_setup -> plot, /both			; GOES X-rays and protons

;hio_setup -> select_times			; use cursors to select interval

;--  Load event list covering selected time interval; select event

hio_setup -> load_eventlist, /choose  		; [,/cme][,/flare][,/particle]

hio_setup -> select_event

;--    Various routines that provide context information

print,''
yesnox,'Do you want to see context information, etc.?',ians,'no'
if not ians then goto, skip_context

hio_setup -> show_context			; plot of flare locations

hio_setup -> browser,/show_event		; if url available
hio_setup -> browser,/solarmonitor		; for selected start date

skip_context:

;--    Select the instruments that your are interested in

print,''
yesnox,'Do you want to select instruments and see what data are available?',ians,'no'
if not ians then goto, skip_down

hio_setup -> select_instruments, /widget

;--    Retrieve list of observations that match time range and instrument selection

hio_setup -> summary				; check on time range and instruments
filelist = hio_setup -> get_filelist()		; output the list of URLs 

;    Examine the results and/or copy 
hio_setup -> plot_filelist, /goes		; examine the results

print,''
yesnox,'Do you want to download files?',ians,'no'
if ians then $
  hio_setup -> download_filelist, filelist, /select	; copy the files

skip_down:

endit:

end