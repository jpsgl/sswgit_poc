;+
; NAME:
;       test_display_hfc
; PURPOSE:
;       Quick test that SSW can display HFC features on images.
;
; CALLING SEQUENCE:
;       .run test_display_hfc 
;
; RESTRICTIONS:
;       Internet connection must be available
;
; HISTORY:
;       22-JUN-2011  Written by X.Bonnin
;	12-Jul-2011  RDB  Pick up data from test_data subdir
;
;-

print,'Test filaments overplotting'
tm0 = systime(1)

;Fits file of Meudon Halpha spectroheliogram
;fits = 'mh990625.064400_subtract_processed'
  
;cd,current=data_path 

data_path = concat_dir('$SSW_HELIO','test_data')
hio_get_fits,data,header,map=map,/LOCAL,data_paths=data_path,/CONVERT2MAP
 
hio_oplot_hefc,map,features='filaments'

print,'Query took:',systime(1)-tm0

tm0 = systime(1)

print,'Test active regions overplotting'

;Fits file of SOHO/MDI magnetogram
;fits = 'fd_M_96m_01d.4438.0009'

hio_get_fits,data,header,map=map,/LOCAL,data_paths=data_path,/CONVERT2MAP

hio_oplot_hefc,map,features='arplages'

print,'Query took:',systime(1)-tm0

tm0 = systime(1)

print,'Test coronal holes overplotting'

;Fits file of SOHO/EIT
;fits = 'efz20090622.232408'

hio_get_fits,data,header,map=map,/LOCAL,data_paths=data_path,/CONVERT2MAP

hio_oplot_hefc,map,features='coronalholes'

print,'Query took:',systime(1)-tm0

end
