function setup_prop_struct, times=times, objects=objects

;    parameter "times" needed to determine object locations

message,'Create Propagation Structure',/info 

if n_elements(times) gt 0 then fmt_timer, times

;    check ICS to determine which observatories are active at this time

sql = 'select * from observatory'
query = hio_form_ics_query(sql=sql)	; defaults to Observatory
print,query
ics_obs = ssw_hio_query(query,/conv)
;help,ics_obs,/st

qv = where(ics_obs.loc_gen ne 'GBO', nqv)
ics_obs = ics_obs(qv)
valid_sbo = ics_obs.name
valid_planet = ['Mercury','Venus','Earth','Mars']		;????
valid_all = strupcase([valid_planet,valid_sbo])

;    examine the objects to see if valid (operational)

print,'Candidate: ',objects
nobj = n_elements(objects)
valid_obj = intarr(nobj)

for j=0,nobj-1 do begin			; planets
  qv = where(strupcase(valid_planet) eq strupcase(objects(j)), nqv)
;  print,objects(j),nqv
  if nqv gt 0 then valid_obj(j)=2
endfor
 
for j=0,nobj-1 do begin			; missions
  if valid_obj(j) eq 0 then begin
    cobj = strupcase(objects(j))
    qv = where(strupcase(valid_sbo) eq cobj, nqv)  
;    print,objects(j),nqv, qv(0)
    if nqv gt 0 then begin
      cics_obs = ics_obs(qv(0))
      loc_gen = cics_obs.loc_gen
      print, objects(j), loc_gen, format='(a,t15,a)'

      ; '2003-10-28' <=time_end and '2003-11-03' >=time_start
      
      case 1 of 
        loc_gen eq 'HP4': begin
            if times(0) le cics_obs.time_end and times(1) ge cics_obs.time_embark then begin
                message,'Include Cruise',/info
                valid_obj(j)=1
              endif else message,objects(j)+' not operational',/info
          end
        else: begin
           if times(0) le cics_obs.time_end and times(1) ge cics_obs.time_start $
             then valid_obj(j)=1 $
             else message,objects(j)+' not operational',/info
          end
      endcase
      
    endif
  endif
endfor

print, objects(where(valid_obj gt 0))
objects = objects(where(valid_obj gt 0))
nobj = n_elements(objects)

;--    Form propagation structure

delays = replicate(propagation_struct(), nobj)
;help, /st, delays

;    load the object names
for j=0,nobj-1 do delays(j).object = objects(j)

;    get the positional information from the ILS
;    exactly which time should be used???

tt = strmid(anytim(times,/ccs),0,10)		; only pass date?????
print,tt
message,'Retrieving position information from the ILS',/info
ils = ssw_hio_query(hio_form_ils_query([tt(0),tt(0)]), /conv)
help,/st,ils

print,'Found in ILS: ',all_vals(ils.target_obj)

print,''
print,'Objects:  ',arr2str(delays.object,del=', ')

;>>>>    cope with name MISMATCH in ILS
sobs = str_replace(delays.object,'_','')
sobs = str_replace(sobs,'-','')
sobs = strupcase(sobs)

;    sort out indexation into the ILS wrt list of objects
opnt = intarr(nobj)                                                   
;;;;for k=0,nobj-1 do opnt(k)=where(strupcase(objects(k)) eq ils.target_obj)
for k=0,nobj-1 do opnt(k)=where(sobs(k) eq ils.target_obj)

;    load the location of the objects
astronomical_unit = 149.6e6		; km
for j=0,nobj-1 do begin
  if opnt(j) ne -1 then begin		; -1 means not in ILS for this time
    delays(j).flag_object = 1		; flag was found
    delays(j).r_hci = ils(opnt(j)).r_hci*astronomical_unit/1.e6    ; M km
    delays(j).long_hci = ils(opnt(j)).long_hci
    delays(j).lat_hci = ils(opnt(j)).lat_hci
    delays(j).long_carr = ils(opnt(j)).long_carr    ;Carrington longitude
  endif else message,'>> Object not found in ILS for this time: '+objects(j),/info
endfor

;    compare the number to check...
;print, ils(opnt).r_hci*astronomical_unit
;print, delays.r_hci

return, delays
end

function hio_propagation_delays, base_event		;times

;    calculate relative propagation delays  (delta times!)

box_message,[' ','Using Ballistic Propagation Model',' ']

;    parameter "times" needed to determine object locations

help, base_event

time = base_event.time
objects = base_event.objects

delays = setup_prop_struct(times=[time,time], objects=objects)
nobj = n_elements(delays)

;    velocity of propagating phenomena - what should we use 
;    default values define in base_event

vel_light = 300e3		; km/s
vel_proton = vel_light*Base_event.c_fract_proton
vel_cme = base_event.vel_cme	; km/s

;    duration of events  - what should be use

dur_xray = 12.			; min
dur_proton = 120.		; min
dur_cme = 1500.			; min
;   should this be a -/+ either side of average velocity

;    now populate the structure

;>>>  what should base time be ??
;  if seen at earth, Venus would be earlier...

for jp=0,nobj-1 do begin

;   flag the object that made the observations
;   value=3   main observation
;   value=??  also seen from ???

  cobject = delays(jp).object
  print, 'Object: ', cobject

  if strlowcase(delays(jp).object) eq strlowcase(base_event.object) then begin
    if base_event.obs_type eq 'remote' then delays(jp).flag_remote=3
  endif

  r_hci = delays(jp).r_hci * 1.e6	; km
  
;    delays related to photons

  delays(jp).dtstart_remote = r_hci/vel_light/3600.
  delays(jp).dtend_remote = delays(jp).dtstart_remote + dur_xray/60.
  
;    delay related to energetic particles

  delays(jp).dtstart_prompt = r_hci/vel_proton/3600.
  delays(jp).dtend_prompt = delays(jp).dtstart_prompt + dur_proton/60.
  
  t0 = fmt_tim(addtime(time, delta=delays(jp).dtstart_prompt))
  t1 = fmt_tim(addtime(time, delta=delays(jp).dtend_prompt))
  stimes = [t0,t1]
  sql = hio_keyword_ics_sql(stimes, object=cobject, /insitu, /energetic)
  print,sql
  
  ;;sql = str_replace(sql,'instrument','instrument_new')
  resp = ssw_hio_query(hio_form_ics_query(sql=sql), /conv)
  if datatype(resp) eq 'STC' then begin
    cinst = allowed_ics(resp.OBSINST_KEY)
    delays(jp).obsinst_prompt = arr2str(cinst,del=', ')
  endif
  print, 'Instruments>>: ', delays(jp).obsinst_slowSW
  
;    delays related to the solar wind

  vv = r_hci/base_event.vel_fastsw/3600.
  delays(jp).dtstart_fastsw = vv(1)
  delays(jp).dtend_fastsw = vv(0)
;;  delays(jp).obsinst_fastSW

  vv = r_hci/base_event.vel_slowsw/3600.
  delays(jp).dtstart_slowsw = vv(1)
  delays(jp).dtend_slowsw = vv(0)

;??  only one instrumnet set for solar wind?  [sta_fast, end_slow]
  t0 = fmt_tim(addtime(time, delta=delays(jp).dtstart_slowsw))
  t1 = fmt_tim(addtime(time, delta=delays(jp).dtend_slowsw))
  stimes = [t0,t1]
  sql = hio_keyword_ics_sql(stimes, object=cobject, /insitu)
  print,sql
  
  ;;sql = str_replace(sql,'instrument','instrument_new')
  resp = ssw_hio_query(hio_form_ics_query(sql=sql), /conv)
  if datatype(resp) eq 'STC' then begin
    cinst = allowed_ics(resp.OBSINST_KEY)
    delays(jp).obsinst_slowSW = arr2str(cinst,del=', ')
  endif
  print, 'Instruments>>: ', delays(jp).obsinst_slowSW

;    delays related to the Coronal Mass Ejection (CME)

  delays(jp).vel_CME = vel_cme
  delays(jp).DTest_CME = r_hci/vel_cme/3600.
  delays(jp).dtstart_cme = r_hci/(vel_cme+50)/3600.
  delays(jp).dtend_cme = r_hci/(vel_cme-50)/3600.

endfor

;    messenger is not always close to mercury
pt_mercury = where(strlowcase(delays.object) eq 'mercury')
pt_messenger = where(strlowcase(delays.object) eq 'messenger')
nomess = 0
if abs(delays(pt_messenger).r_hci - delays(pt_mercury).r_hci) gt 5 then begin
  message,'>>>>>> Messenger NOT near Mercury',/info
  delays(pt_mercury).obsinst_slowsw = ''
  delays(pt_mercury).obsinst_prompt = ''
endif

;
kp = where(strlowcase(delays.object) eq 'earth', nkp)
if nkp ne 0 then begin
  help, delays(kp(0)), /st
  if base_event.event_type eq 'cme' and base_event.obs_type eq 'r' then $
	print,'***** set something ****'
endif else help, delays, /st

return, delays
end
