function base_event_struct

;    define the base_event structure 

temp = {select_mode: '', $		; selected from event list or light-curve
	select_entity: '', $		; name of event list or type of light-curve

	obs_type: '', $			; remote or in-situ

	event_type: '', $		; if event list, primary event type

	time: '', $			; time used as start time of propagation
	duration: 0.0, $		; ???????

	target: '', $			; target if observations remote-sensed

					; location of target on sun???
					
	object: '', $			; observatory events/data were from

;????    should these be here		; location of observatory????
	r_hci:0., lat_hci:0., long_hci:0., $


;    define default set of objects - should thes be passed as a parameter ????????
;    what about STA, STB??  - not in the ILS

	objects: ['Mercury','Venus','Earth','Mars', $
		'Messenger','STEREO-A','STEREO-B','Ulysses'], $

;    velocity of propagating phenomena - what should we use

	c_fract_proton: 0.9,	 $	; fraction of speed of light 
	vel_cme: 459, $			; km/s  (average from Gopalswamy 2004)
	vel_fastsw: [800,900], $	; km/s
	vel_slowsw: [300,500]}		; km/s

return, temp

end
