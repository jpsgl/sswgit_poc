function propagation_struct		;, numrec=numrec

;    structure used to hold information returned by propagation model

temp = {object: '', $			; name of object (planet or spacecraft)
	flag_object: 0, $		; only set if object could be located in ILS
	r_hci: 0.0, $			; location of object in HCI frame
	lat_hci: 0.0, $
	long_hci: 0.0, $
        long_carr: 0.0, $		; Carrington longitude

;	base_time: '', $		; base time of event ??????  WHERE LOADED?
;	base_object: '', $		; object event is related to ??????
					; or should this all be in the baseevent struct

	flag_remote: 0, $		; flag if will be observed remotely ??????
					; if -1 then no suitable instruments available
	DTstart_remote: 0.0, $		; delay in arrival of photons
	DTend_remote: 0.0, $		;
	obsinst_remote: '', $		; instruments at this "location"

	flag_prompt: 0, $		; flag if "location" affected by particles
					; if -1 then no suitable instruments available
	DTstart_prompt: 0.0, $		; delay in arrival of particles
	DTend_prompt: 0.0, $		;
	obsinst_prompt: '', $		; instruments at this "location"

	flag_insitu: 0, $		; no. of suitable instruments for in-situ plasma/mag field
					; if -1 then not possible to monitor
					; 0 if not checked

	flag_fastSW: 0, $		; flag if "location" affected by fast solar wind
					; if -1 then no suitable instruments available
	DTstart_fastSW: 0.0, $		; delay in arrival of fast solar wind
	DTend_fastSW: 0.0, $		;
	obsinst_fastSW: '', $		; instruments at this "location"

	flag_slowSW: 0, $		; flag if "location" affected by slow solar wind
					; if -1 then no suitable instruments available
	DTstart_slowSW: 0.0, $		; delay in arrival of slow solar wind
	DTend_slowSW: 0.0, $		;
	obsinst_slowSW: '', $		; instruments at this "location"

	flag_CME: 0, $			; flag if "location" affected by CME
					; if -1 then no suitable instruments available
	vel_CME: 0.0, $			; velocity of CME  (or just in base_event ??)
	DTest_CME: 0.0, $		; guess
	DTstart_CME: 0.0, $		; delay in arrival of CME
	DTend_CME: 0.0, $		;
;;;	dt_CME: fltarr(2), $		;
	obsinst_CME: '' $		; instruments at this "location"

}

;??? actually observed flag for each???
;??? score

;?  state actual velocity (range) used in calculations


return, temp		;delays
end		