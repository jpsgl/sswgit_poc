;----------------------------------------------------------------

pro hio_setup::make_prop_base_event

message,'Creating BASE_EVENT structure',/info
base_event = base_event_struct()

print,''
print,'Selection Mode:  ', self.flag_select_mode
print,'Plot Obs. type:  ', self.flag_plot_otype		; name????????
Print,'Plot of: ', self.timeseries_data
print,''

select_mode = self.flag_select_mode
base_event.select_mode = select_mode

;    NO - should be event time   <<<<<<<<

if n_elements(*(self.timewindow)) eq 2 then begin
;;  fmt_timer,*(self.timewindow), tmin, tmax, /quiet
;;  base_event.time = tmin
endif else begin
  box_message,'Time not define/Event not selected'
  return
endelse

; ????  if by light-curve??

if select_mode eq 'event-list' then begin

if n_elements(*(self.hec_tabinfo)) gt 0 then begin
  clist = (*self.hec_tabinfo)(self.hec_tab_row)
  print,'Details of selected event list:'
  help,/st,clist
endif else begin
  box_message,'Event list not selected/defined'
  return
endelse

;    this event list is not suitable as base for propagation !!
if clist.flag eq '????' then begin
  cmessage = ['Event list is not suitable as base for propagation: '+clist.name, $
		'Times that are defined do not relate to a specific location']
  box_message,cmessage
  return
endif

base_event.select_entity = clist.name

print,'clist.flag: ',clist.flag
cevent = (*self.eventlist_table)(self.eventlist_row)
help,/st, cevent

base_event.time = anytim(cevent.time_start,/ecs)

;    not sure about this - need to know it somehow...
if clist.cme eq 'y' then base_event.event_type = 'cme'		; primary????

case 1 of
  strupcase(clist.otyp) eq 'R': begin
    otype = 'remote'
    target = '????'
    if clist.solar eq 'y' then target = 'Sun'
    print, 'Remote observations of the '+target
    base_event.obs_type = otype
    base_event.target = target
    base_event.object = clist.object
;    some lists with STEREO mix spacecraft ?????????????
    if clist.flag eq 'ssss' then base_event.object = strlowcase(cevent.sat_id)
  end

  strupcase(clist.otyp) eq 'I': begin
    otype = 'in-situ'
;;    object = '????'
    environ = '????'
    if clist.planet eq 'y' then $
      if clist.flag eq 'oooo' then environ = clist.object	;'planet xxxx'
    if clist.geo eq 'y' then environ = 'Earth'
    if clist.ips eq 'y' then $
      if clist.flag eq 'oooo' then environ = clist.object	;'mission xxxx'
    print,'In-situ observations of environment near '+environ
;;    print,'Object for ILS search: '+object
    base_event.obs_type = otype
    base_event.object = environ
  end

  else: message,'****** problem ******'
endcase

endif

if select_mode eq 'light-curve' then begin
  base_event.duration = addtime(tmax,diff=tmin)/60.
  base_event.obs_type = self.flag_plot_otype
  base_event.select_entity = self.timeseries_data

;    this stuff needs to be set in better place!!!!
  if base_event.obs_type eq 'remote' then base_event.target = 'Sun'
  if base_event.select_entity eq 'ace_swind' then base.object = 'Earth ?'
endif

print,''
help,/st, base_event		;<<<<<<<< load location????

*self.prop_base_event = base_event

end

;----------------------------------------------------------------

function hio_setup::hecinfo_test

;--    ??? purpose

hec_tabinfo = *self.hec_tabinfo
nrow = n_elements(hec_tabinfo)

hec_tabinfo = add_tag(hec_tabinfo,'','target')
hec_tabinfo = add_tag(hec_tabinfo,'','obj2')

for jrow=0,nrow-1 do begin

clist = hec_tabinfo(jrow)
help,/st, clist

case 1 of
  strupcase(clist.otyp) eq 'R': begin
    otype = 'remote'
    target = '????'
    if clist.solar eq 'y' then target = 'Sun'
    print, 'Remote observations of the '+target
;    base_event.obs_type = otype
;    base_event.target = target
     hec_tabinfo(jrow).target = target
;    base_event.object = clist.object
     hec_tabinfo(jrow).obj2 = clist.object
;    some lists with STEREO mix spacecraft ?????????????
;    if clist.flag eq 'ssss' then base_event.object = strlowcase(cevent.sat_id)
  end

  strupcase(clist.otyp) eq 'I': begin
    otype = 'in-situ'
;;    object = '????'
    environ = '????'
    if clist.planet eq 'y' then $
      if clist.flag eq 'oooo' then environ = clist.object	;'planet xxxx'
    if clist.geo eq 'y' then environ = 'Earth'
;>>    if clist.ips eq 'y' then $
      if clist.flag eq 'oooo' then environ = clist.object	;'mission xxxx'
    print,'In-situ observations of environment near '+environ
;;    print,'Object for ILS search: '+object
;    base_event.obs_type = otype
;    base_event.object = environ
     hec_tabinfo(jrow).obj2 = environ
  end

  else: message,'****** problem ******',/info
endcase

if clist.flag eq 'ssss' then hec_tabinfo(jrow).obj2 = clist.flag
if clist.flag eq 'xxxx' then hec_tabinfo(jrow).obj2 = clist.flag

;;hec_tabinfo(jrow) = clist
endfor

hio_stc2html, hec_tabinfo, file='ggtt.html',tit='fixit'

return, hec_tabinfo
end

;----------------------------------------------------------------

pro hio_setup::propagate_times

;--  constant velocity propagation of time on Sun to different distances

;  only up to 2AU?

message,'Calculating propagation delays',/info

;self -> summary
if n_elements(*(self.prop_base_event)) gt 0 then begin
  print,''
  print,'>>>>>>>> Prop. model base info.'
  help,/st,*self.prop_base_event

; ????? how are the base_event parameters used
; ????? should they include velocity ranges, etc..

endif else begin
  message,'>>>> Need to have BASE EVENT established',/info
  self -> make_prop_base_event
  self -> propagate_times
  return
endelse

if n_elements(*self.timewindow) eq 0 then begin
  box_message,'*** Time range NOT selected ***'
  return
endif else times = *self.timewindow

;    is this the right time ????
;    use base_event??

base_event = *self.prop_base_event 

cevent = (*self.eventlist_table)(self.eventlist_row)
if have_tag(cevent, 'v') then $
  base_event.vel_cme = cevent.v		; only works for CMEs

help, base_event

delays = hio_propagation_delays(base_event)		;times)
help, delays

;    flag where expect events to be seen
;    need to know that a CME will affect earth
;    halo event if observed from _ERO_ observatory

hec_tabinfo = *self.hec_tabinfo
kq = where(hec_tabinfo.name eq base_event.select_entity, nkq)
if nkq ne 1 then message,'wwwwwopsss 1'
clist = hec_tabinfo(kq(0))

kp = where(strlowcase(delays.object) eq 'earth', nkp)  ;????? present
if nkp ne 1 then message,'wwwwwopsss 2'

if have_tag(cevent, 'pa_width') then $		; view point ??????
  if cevent.pa_width gt 180. and clist.object eq 'soho' then delays(kp).flag_cme=1

*self.prop_delays = delays
*self.prop_base_event = base_event

end

;----------------------------------------------------------------

pro hio_setup::plot_delays, pwind=pwind, wide=wide, $
	insitu=insitu, ace=ace, proton=proton

;--  plot delayed emission

delays = *self.prop_delays 
base_event = *self.prop_base_event 

self -> show_event

kp = where(strlowcase(delays.object) eq 'earth')

;    define limits of the plot window in terms of -/+ hours wrt base event time

tlims = [15,20]		; hours
if keyword_set(pwind) then tlims = pwind

t0 = fmt_tim(addtime(base_event.time,delt=(delays(kp).DTest_cme-tlims(0))*60))
t1 = fmt_tim(addtime(base_event.time,delt=(delays(kp).DTest_cme+tlims(1))*60))
times = [t0,t1]

t0 = fmt_tim(addtime(base_event.time,delt=-12*60.))
t1 = fmt_tim(addtime(base_event.time,delt=+6*24*60.))
if keyword_set(wide) then times = [t0,t1]

fmt_timer, times

;????    should the plot type be selectable?
;????    should type of match required be defined to help indicate offset

if not keyword_set(proton) then ace=1
self -> plot, insitu=insitu, ace=ace, proton=proton, timer=times

;    plot fiducial lines to show CME arrival at velocity V +/- 50 km/s

tx = fmt_tim(addtime(base_event.time,delt=delays(kp).DTest_cme*60))
outplot,[tx,tx],[0,1], color=100
tx = fmt_tim(addtime(base_event.time,delt=delays(kp).DTstart_cme*60))
outplot,[tx,tx],[0,1], color=100
tx = fmt_tim(addtime(base_event.time,delt=delays(kp).DTend_cme*60))
outplot,[tx,tx],[0,1], color=100

;    could also plot other fiducial marks but required plot type may be different ????

tx = fmt_tim(base_event.time)
print,'Base Event time: ',tx
outplot,[tx,tx],[0,1], color=180

end

