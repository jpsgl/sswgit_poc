function hfr_Raster2Image, str2, nx, code=code, debug=debug

;+
; NAME:
;         HFR_RASTER2IMAGE
; PURPOSE:
;         reconstructs the bounding rectangle image from the raster scan
; CALLING SEQUENCE:
;         rast = hfr_raster2image, str, nx
;
; INPUT:
;         str2	Raster Scan as a string
;         nx	image size in x-axis
; OUTPUT
;	  rast	reconstructed raster scan image
;		(nx by n/nx where n = length of the raster scan string)
; KEYWORDS
;	  code	reconstructs length-coded raster scan
;				(not used for this data)
;
; HISTORY:
;         12-Aug-2004  Written by Bob Bentley, based on WP5 raster2image
;-


;  not sure what this part is for - leave it for the time being

len_coded=0
if strpos(str2,'.') gt 0 then len_coded=1

;if keyword_set(code) then begin

if len_coded then begin
  if keyword_set(debug) then print,'** len code raster'

  separator='.'
  x=strsplit(strtrim(str2,2), separator, count=n0, /extract)
  str=''
  for i=0, n0-1 do begin
    k=fix(strmid(x[i], 1))
    symbol=strmid(x[i], 0, 1)
    for j=0, k-1 do str=str+symbol
  endfor
		
;help,x,str

endif else str=str2

;;	n=strlen(str)
;;	ny= n/nx
;;	image=bytarr(nx, n/nx)
;;	for i=0, n-1 do $
;;		image[i]=fix(strmid(str, i, 1))

bstr = byte(str) -"60b 
img = reform(bstr,nx,n_elements(bstr)/nx)

ans=''
if keyword_set(debug) then read,'* Pause?: ',ans

return, img
end
