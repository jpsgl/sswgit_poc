function hio_form_hfc_query, listname=listname, timerange=timerange, $
		show_tables=show_tables, describe_table=describe_table, $
		characterise_tables=characterise_tables, $
		verbose=verbose

;;		find_parameters=find_parameters, find_lists=find_lists, $
;;    find_list         get list of available tables
;;    find_parameters   get list of parameters for table supplied in listname

;    show_tables      get list of available tables
;    describe_table   get list of parameters for table supplied in listname

message,'Forming query for HELIO HFC',/info

list = 'FRC_INFO'
if keyword_set(listname) then list = listname
list = strtrim(list,2)		; make sure no leading or trailing blanks

;    Syntax of some system commands for PostgreSQL is different to mySQL

;    List available tables
if keyword_set(show_tables) then begin
  message,'SHOW TABLES',/info
  ;com = "SELECT table_name FROM information_schema.tables WHERE table_type='VIEW'" 
  com = "SELECT table_name FROM information_schema.tables WHERE table_schema='hfc1'"
  goto, go_psql
endif

;    Postgres equivalent of DESCRIBE
if keyword_set(describe_table) then begin
  message,'DESCRIBE TABLE '+list,/info
  com = "SELECT column_name FROM information_schema.columns WHERE table_name='" +list+ "'"
  goto, go_psql
endif

;    Table used to see types of features available
if keyword_set(characterise_tables) then begin
  message,'Get table characterisation information',/info
  com = "SELECT feature_name,code FROM FRC_INFO"
  goto, go_psql
endif


message,'Using TABLE '+list,/info

times = ['2002-07-15 00:00:00','2002-07-15 23:59:59']
if keyword_set(timerange) then begin
    times = strtrim(anytim(timerange,/CCSDS),2)
    times[0] = strjoin((strsplit(times[0],'T.',/EXTRACT))[0:1],' ')
    times[1] = strjoin((strsplit(times[1],'T.',/EXTRACT))[0:1],' ')
endif

qcols = '*'

col_select = hfc_column_select()
qc = where(col_select.table_name eq list,nqc)
if nqc eq 1 then qcols = col_select(qc).sql_columns $
    else message,'Not special case '+list,/info


;    formulate the SQL query
com = "SELECT " +qcols+ " FROM " + list 
com = com + " WHERE date_obs BETWEEN '"+times(0)+"' AND '"+times(1)+"'"
com = com + " ORDER BY date_obs"
print,com

go_psql:
psql = '&sql='+com

;    concatonate the URL
root = 'http://voparis-helio.obspm.fr:8080/stilts/task/sqlclient'
rdtb = '?db=jdbc:mysql://voparis-mysql5-paris.obspm.fr:3306/hfc1&user=guest&password=guest'
ofmt = '&ofmt=votable'

url  = root + rdtb + psql + ofmt
if keyword_set(verbose) then print, url

return, url
end
