pro  hfr_raster2cont, cspot, xy, info, debug=debug

;+
; NAME:
;         HFR_RASTER2CONT
; PURPOSE:
;         
;
; CALLING SEQUENCE:
;         hfr_raster2cont, cspot, xy, info
;
; INPUT:
;         cspot		Structure containing HFC spot data
; OUTPUT
;         xy		2d Array of virtices of contour outlining spot
;         info		Structure that identifies segments of xy array represent 
;			each contour
;			See IDL routine CONTOUR for details on these
;
; KEYWORDS
;
; HISTORY:
;         15-Aug-2004  RDB  written
;-

raster = cspot.raster
nx = cspot.br_pix_x1 - cspot.br_pix_x0+1
im = efr_raster2image(raster,nx)

;  add a row of pixels around outside to ensure closed contours...
;  not sure about this!!!
sz = size(im)
;print,sz
imm = bytarr(sz(1)+2,sz(2)+2)
imm(1:sz(1),1:sz(2))=im(*,*)

;  Switch to Z buffer so that can use CONTOUR to generate the contour outline
;  Tried using ISOCONTOUR but it gives slightly different reults
dev_save = !d.name
sx=!x & sy=!y & sp=!p
set_plot,'z
contour,imm,lev=[1,2],path_xy=xy,/path_data,path_inf=info
xy(0,*) = cspot.br_pix_x0 + xy(0,*)-1
xy(1,*) = cspot.br_pix_y0 + xy(1,*)-1
set_plot,'x'    ;dev_save
!x=sx & !y=sy & !p=sp


if keyword_set(debug) then begin
  window,1
  contour,imm,lev=[1,2],xra=[0,sz(1)+1],yra=[0,sz(2)+1],xst=1,yst=1
  nn = 6 & tvscl,congrid(imm,28*nn,15*nn)

  window,2
  for j=0,n_elements(info)-1 do begin
    s = [indgen(info(j).n),0]
    ss = info(j).offset+s
;;  print,ss
    if j eq 0 then plot, xy(0,ss),xy(1,ss), $
     xra=[cspot.br_pix_x0-1,cspot.br_pix_x1+1],yra=[cspot.br_pix_y0-1,cspot.br_pix_y1+1],xst=1,yst=1 $
      else oplot, xy(0,ss), xy(1,ss)
  endfor
endif

end
