function  match_hfc_index, hfc_struct, ref_time, pause=pause, debug=debug

;+
; NAME:
;         match_hfc_index
; PURPOSE:
;         Find records in segment of HFC that match reference time
;
; CALLING SEQUENCE:
;         irecs = match_hfc_index(hfc_struct,map)
;
; INPUT
;         hfc_struct   structure containing HFC data 
;         ref_time     reference time, e.g. of a map object
; OUTPUTS
;         irecs        indices in structure that represent features 
;                      from a single image closest to the reference time
; HISTORY:
;         22-JUN-2011  Written by Bob Bentley (adapted from match_sfc_index.pro)
;
;-
  

times = all_vals(hfc_struct.date_obs)
fids = time2fid(anytim(times,/ecs),/full,/time)
rq_fid = time2fid(ref_time,/full,/time)

ff_idx = find_nearest_fid(rq_fid,fids)
qq = where(hfc_struct.date_obs eq times(ff_idx(0)))

if keyword_set(debug) then begin
  print,fids
  print,rq_fid    
  print,ff_idx(0)
endif

ans=''
if keyword_set(pause) then read,'Pause: ',ans
help,qq
return,qq

end
