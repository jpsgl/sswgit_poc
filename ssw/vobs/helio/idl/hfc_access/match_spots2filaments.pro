function  match_spots2filaments, ss, ff, map, dd, debug=debug

;+
; NAME:
;         match_spots2filaments
; PURPOSE:
;         Determine if any filaments in EGSO Solar Feature Catalog
;         are really wrongly identified sunspots. 
;
; CALLING SEQUENCE:
;         ww = match_spots2filaments(ss, ff, map [, dd]) 
;
; INPUT:
;         ss    structure containing EGSO SFC sunspot data
;         ff    structure containing EGSO SFC filaments data
;         map	map object - defines time of reference image 
;
; OUTPUTS:
;         ww    vector listing any wrongly identified filaments 
; 
; OPIONAL OUTPUTS:         
;         dd    stucture containing information on distance between features	
;
; METHOD:
;         Find closest filament and sunspot data to time of reference image. 
;         Find locations of all umbra in the sunspots and then determine 
;         distance from each umbra to all filaments. 
;         Flag as wrongly identified if closer than 30 arcsecs         
;
; HISTORY:
;            Jun-2004  Written by Bob Bentley (MSSL/UCL)
;         23-Aug-2004  Released version
;
;-

;common ss_ff, dd
common umbra, umbra_desc

;   find the records filament data that correspond to the image
idx_ff=match_sfc_index(ff,map.time)
pmm,idx_ff

;   now find sunspot feature data nearest in time to this 
;   use this rather than matching both to the map time
idx_ss=match_sfc_index(ss,ff(idx_ff(0)).date_obs)
pmm,idx_ss

;   only consider the larger spots ???
sqx = where(ss(idx_ss).feat_npix ge 10)		;20?
nidx_ff = n_elements(idx_ff)
nsqx = n_elements(sqx)

;  find centers of umbra so that can get more precise sunspot position

umbra_desc = replicate({idx:0,k_umb:0,xp:0.0,yp:0.0},total(ss(idx_ss(sqx)).n_umbra))
help, umbra_desc
pumb = 0
for jsp=0,nsqx-1 do begin
  qidx = idx_ss(sqx(jsp))

  cen_x = ss(qidx).center_x
  cen_y = ss(qidx).center_y
  xscl  = ss(qidx).cdelt1
  yscl  = ss(qidx).cdelt2

;;  cc_x0 = ss(qidx).cc_pix_x
;;  cc_y0 = ss(qidx).cc_pix_y

    efr_raster2cont,ss(qidx),xy,info

;;  add relative posn. to contour(s) start posn., $
;;  make relative to sun centre and convert to arcsec
    xrcc = (xy(0,*) - cen_x)*xscl
    yrcc = (xy(1,*) - cen_y)*yscl

;  print,ss(qidx).n_umbra,n_elements(info)
    if ss(qidx).n_umbra gt 0 then begin
      for j=0,n_elements(info)-1 do begin
        s = [indgen(info(j).n),0]
        ww = info(j).offset+s
;  print,pumb,qidx,j,info(j).value
        if info(j).value eq 2 then begin
          umbra_desc(pumb).idx = qidx
          umbra_desc(pumb).k_umb = j
          umbra_desc(pumb).xp = average(xrcc(ww))
          umbra_desc(pumb).yp = average(yrcc(ww))
          pumb = pumb+1
        endif
      endfor
    endif
endfor
umbra_desc=umbra_desc(0:pumb-1)
nsqx = n_elements(umbra_desc)

;   rotate the features to the time of the image -- ROLL?
tim_ff = ff(idx_ff(0)).date_obs
rff=rot_xy(ff(idx_ff).sc_arc_x,ff(idx_ff).sc_arc_y,tstart=tim_ff,tend=map.time)
tim_ss = ss(idx_ss(0)).date_obs
;;;rss=rot_xy(ss(idx_ss(sqx)).gc_arc_x,ss(idx_ss(sqx)).gc_arc_y,tstart=tim_ff,tend=map.time)
;rss=rot_xy(ss(idx_ss(sqx)).gc_arc_x,ss(idx_ss(sqx)).gc_arc_y,tstart=tim_ss,tend=map.time)
rss=rot_xy(umbra_desc.xp,umbra_desc.yp,tstart=tim_ss,tend=map.time)


;   determine distance between features and store results in structure
dd = replicate({dd:0.0,kd_ff:0,kd_ss:0},nidx_ff*nsqx)                                                                          
for j=0,nsqx-1 do begin 
  dr = sqrt((rff(*,0)-rss(j,0))^2 + (rff(*,1)-rss(j,1))^2)
  p0 = 0+j*nidx_ff  &  p1 = (nidx_ff-1)+j*nidx_ff
  dd(p0:p1).dd = dr
  dd(p0:p1).kd_ff = idx_ff
  dd(p0:p1).kd_ss = intarr(nidx_ff)+j		;+idx_ss(sqx(j))
endfor

;   only include those within 30 arcsec -- ???
threshold = 30		;20
zz=where(dd.dd lt threshold)

w_ss = -1  &  ks = -1
if zz(0) ge 0 then begin
  ks = dd(zz).kd_ss
  print,ks

;w_ss = idx_ss(sqx(ks))
  w_ss=dd(zz).kd_ff 
  print,w_ss
endif else message,'* No filaments tagged as sunspots',/cont

if keyword_set(debug) then begin
  plot,rff(*,0),rff(*,1),psym=4
  linecolor,1,'yellow'  
  oplot,rss(*,0),rss(*,1),psym=1, color=1

  if ks(0) ge 0 then begin
    linecolor,1,'red'
    oplot,rss(ks,0),rss(ks,1),psym=1,color=1
  endif

  ans=''
  read,'Pause: ',ans

  ddr = sqrt((rff(*,0)-rss(0,0))^2 + (rff(*,1)-rss(0,1))^2)                                              
  plot,ddr,psym=10,yra=[0,300]                                                                           
  for j=1,nsqx-1 do begin ddr = sqrt((rff(*,0)-rss(j,0))^2 + (rff(*,1)-rss(j,1))^2) & oplot,ddr,psym=10 & end

  oplot,[0,nidx_ff-1],[threshold,threshold],linestyle=1
endif

;if w_ss(0) ge 0 then begin
;  print,'x:        ',ff(w_ss).sc_arc_x
;  print,'y:        ',ff(w_ss).sc_arc_y
;  print,'min int:  ',ff(w_ss).feat_min_int
;  print,'bkg:      ',ff(w_ss).qsunint
;  print,'mean2qsun:',ff(w_ss).feat_mean2qsun
;endif

return, w_ss

end
