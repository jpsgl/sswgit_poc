pro  hfr_ccode2xy,ccode,xf,yf

;+
; NAME:
;         HFR_CCODE2XY
; PURPOSE:
;         Convert chain code string produced by the HELIO Feature 
;         Recognition (HFR) software into position relative to the
;         starting point.
;
; CALLING SEQUENCE:
;         hfr_ccode2cy,ccode,xf,yf
;
; INPUT
;         ccode        string containg chain code
; OUTPUTS
;         xf           vector of x values, relative to start
;         yf           vector of y values, relative to start
;
; HISTORY:
;         26-Jan-2004  Written by Bob Bentley (adapted from efr_ccode2xy.pro)
;
;-
  

;  define the moves in x,y that correspond to chain code
;  values of 0,1,2,3,4,5,6,7
ccr = [[-1,0],[-1,-1],[0,-1],[+1,-1], $
       [+1,0],[+1,+1],[0,+1],[-1,+1]]

;  transform chain code string into a byte array of moves
ccv = byte(ccode)-"60b
npos = n_elements(ccv)+1
;print,ccv

;  convert chain code into position relative to starting point
xyr = intarr(2,npos)
for j=1,npos-1 do xyr[*,j] = xyr[*,j-1] + ccr[*,ccv(j-1)]
;print,xyr

;  form x and y vectors of feature outline
xf = reform(xyr(0,*))
yf = reform(xyr(1,*))

return
end
