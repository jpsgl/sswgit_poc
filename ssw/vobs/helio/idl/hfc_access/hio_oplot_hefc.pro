pro hio_oplot_hefc, map, features=features, events=events, $
	_extra=extra, verbose=verbose, pause=pause	

;+
; NAME:
;         hio_oplot_hefc
; PURPOSE:
;         Retrieve and overplot HEC and HFC data on reference image
;
; CALLING SEQUENCE:
;         hio_oplot_hefc, map  [,features=features] [,events=events]
;
; INPUT:
;         map		SSW map object of reference image
;
; KEYWORD INPUT:
;         feature	if set, do overplot features (e.g. features = ['filaments','arplage'])
;         event	    if set, do overplot events
;
; RESTRICTIONS:
;         Must be connected to Internet to work correctly
;
; METHOD:
;         HEC and HFC data spanning time of image retrieved from nodes in
;         Trieste and Meudon and overplotted on the image 
;
; HISTORY:
;         22-JUN-2011  Written by Bob Bentley (adapted by X.Bonnin from egso_oplot_sefc.pro)
;
;-

quiet = 1-keyword_set(verbose)

;    force proper scaling of the image...
ang = pb0r(map.time)
rsun = ang(2)*60/map.dx & print,'Rsun',rsun
mask=0

;;;if strpos(fits_file,'meud') lt 0 then $
;mask = solar_mask(hh, map.data, solar_r=rsun*0.99)
;clip_map_crange,map,cmin,cmax,/DEBUG ;, /center_weighted;, mask=mask  

;TEMPORARY
hh = histogram(map.data,omin=omin,omax=omax,location=xh,/NAN)
clip = 10
w = where(hh gt clip)
cmin = min(xh[w],max=cmax)

w = where(~finite(map.data))
if (w[0] ne -1) then map.data(w) = cmin - 100

help,title
wtit = 'HELIO HEC & HFC Overplot'

if datatype(map) eq 'STC' then begin
  window, xsiz=700, ysiz=700, /free, xpos=1500, title=wtit
  loadct,0
  mtit = map.id+'!c'+map.time
  plot_map, map, fov=[40,40], title=mtit, dmin=cmin, dmax=cmax
endif else begin
  message,'Map object was not supplied', /cont
  return
endelse

;  define start and end of time window
ref_time = map.time
win = 24	;get +/- 24hrs

t0 = fmt_tim(addtime(ref_time,delt=-win*60))
t1 = fmt_tim(addtime(ref_time,delt=win*60))
timerange = [t0,t1]


;  Get feature data from the HELIO Feature Catalogue (in Meudon)

if keyword_set(features) then begin
	feat = strtrim(strlowcase(features),2)
	for i=0L,n_elements(feat)-1L do begin
		case feat[i] of
			'arplages':begin
  				cmd = hio_form_hfc_query(timerange=timerange,listname='VIEW_AR_FULL')
  				arplage = decode_votable(ssw_hio_query(cmd,verb=verbose), quiet=quiet)
  				if datatype(arplage) eq 'STC' then hfr_oplot_feature, arplage, ref_map=map
  			end
			'filaments':begin
  				cmd = hio_form_hfc_query(timerange=timerange,listname='VIEW_FILAMENTS_FULL')
  				filament = decode_votable(ssw_hio_query(cmd,verb=verbose), quiet=quiet)
  				if datatype(filament) eq 'STC' then hfr_oplot_feature, filament, ref_map=map
			end
			'coronalholes':begin
  				cmd = hio_form_hfc_query(timerange=timerange,listname='VIEW_CH_FULL')
  				coronalhole = decode_votable(ssw_hio_query(cmd,verb=verbose), quiet=quiet)
  				if datatype(coronalhole) eq 'STC' then hfr_oplot_feature, coronalhole, ref_map=map	
			end
			else:message,'Unknown feature!'
		endcase
	endfor
  ;Sunspots Not yet available 
  ;cmd = hio_form_hfc_query(timerange=timerange,listname='VIEW_SP_FULL')
  ;spots = decode_votable(ssw_hio_query(cmd,verb=verbose), quiet=quiet)
  ;if datatype(spots) eq 'STC' then hfr_oplot_feature, spots, ref_map=map

  ;Coronal Holes Not yet available
  ;cmd = hio_form_hfc_query(timerange=timerange,listname='VIEW_CH_FULL')
  ;coronalholes = decode_votable(ssw_hio_query(cmd,verb=verbose), quiet=quiet)
  ;if datatype(coronalholes) eq 'STC' then hfr_oplot_feature, coronalholes, ref_map=map
endif

;
;  Get event data from the HELIO Event Catalogue (in Trieste)
;  (do flare location last so appears on top of everything else)

if keyword_set(events) then begin

  ;Not yet available
  ;query = hio_form_hec_query(timerange=timerange)
  ;flares = decode_votable(ssw_hio_query(query, verb=verb), quiet=quiet)
  ;if datatype(flares) eq 'STC' then egso_oplot_secflare,flares,ref_map=map

endif

ans=''
if keyword_set(pause) then read,'Pause: ',ans

end
