pro  hfr_oplot_feature, struct, index, ref_file=ref_file, ref_map=ref_map, $
	debug=debug, pause=pause, roll_cludge=roll_cludge

;+
; NAME:
;         HFR_OPLOT_FEAT
; PURPOSE:
;         Overplot one or more features decribed in the HELIO HFC on top
;         of a reference image. Rotate to epoch if necessary
;
; CALLING SEQUENCE:
;         hfr_oplot_feat, struct, index
;
; INPUT:
;         struct           Structure containing feature data
;         index            Array contining indices of features in
;                          structure that should be plotted
; OUTPUT
;         Plots to current device
;
; KEYWORDS
;         ref_file         File containing reference image
;         pause            Pause at start of each feature.
;
; HISTORY:
;         22-JUN-2011  Written by Bob Bentley (adapted from efr_oplot_feature.pro)
;		  
;-


;    Image to be used for overplotting
;    If file name provided then read the file and plot the image

if keyword_set(ref_file) and not keyword_set(ref_map) then begin
  img_file=ref_file
  print,'* Using FITS file: ',img_file
  print,''

;  Make a map object of the reference image
  fits2map,img_file,map,head=head
  stc=head2stc(head)          ; convert FITS header to structure (in case...)
  img_time = map.time

  window,xsiz=700,ysiz=600,/free
  load_eit_color
  contour_color = 'yellow'
  plot_map,window=!d.window,map,dmax=1500

;;;  if keyword_set(roll_cludge) then map.roll_angle=roll_cludge  ;<<<<<<<<<<<<<<<
endif
;;;  if keyword_set(roll_cludge) then map.roll_angle=roll_cludge  ;<<<<<<<<<<<<<<<

;    Or, if the map is provided, assume already plotted???

if not keyword_set(ref_file) and keyword_set(ref_map) then begin
  map = ref_map
  img_time = map.time
;;;  observatory = strupcase(map.observatory)   ;???
endif

;    Put tick marks to show the north/south axis, and write label
ang = pb0r(map.time,/arcsec)
xt=[0,0] & yt=[ang(2),1.1*ang(2)]	; from solar limb outwards
roll_xy,xt,yt,map.roll_angle,xr,yr,center=map.roll_center  &  oplot,xr,yr
roll_xy,xt,-yt,map.roll_angle,xr,yr,center=map.roll_center  &  oplot,xr,yr

linecolor,1,'red'		;'orange'
if map.roll_angle ne 0 then begin
  print,'Oplot_Feat, Roll angle:',map.roll_angle
;  linecolor,1,'red'		;'yellow'
  xl = 1.03*(!x.crange(1)-!x.crange(0))+!x.crange(0)
  yla = 0.95*(!y.crange(1)-!y.crange(0))+!y.crange(0)
  ylb = 0.91*(!y.crange(1)-!y.crange(0))+!y.crange(0)
  xyouts,xl,yla,'Roll: ',charsiz=1.5,color=1
  xyouts,xl,ylb,strtrim(string(map.roll_angle,format='(f8.2)'),2),charsiz=1.5,color=1
endif

x0 = 0.88*(!x.crange(1)-!x.crange(0))+!x.crange(0)
y0 = 0.88*(!y.crange(1)-!y.crange(0))+!y.crange(0)
l0 = 0.1*(!y.crange(1)-!y.crange(0))
arrow2,x0,y0,90-map.roll_angle,l0,/angle,thick=2,color=1,/data       

;------------------------------------------------------------------------


;print,'Time of Reference Image: ',img_time

if n_params() eq 1 then $
    index = match_hfc_index(struct,img_time,pause=pause)
mm = minmax(index)
print,'** Indices in HFC fragment:',mm,'('+strtrim(string(n_elements(struct)),2)+')', $
       format='(a,2i10,6x,a)'
nidx = n_elements(index)
ans=''

sunspot = 0 & filament = 0
contour_color = 'red'  ;'green'  ;'purple'
if have_tag(struct, 'ske_len_deg') then begin
	contour_color = 'green'
	filament = 1
endif
if have_tag(struct, 'n_umbra') then begin
;;;;and have_tag(struct,'umb_npix')
  sunspot=1
  print,'** HFC structure describes Sunpots'
  contour_color = 'yellow'
  kreject=0			;no of small spots rejected
endif

;help,/st,struct
;help,sunspot
if keyword_set(pause) then $
    read,'Pause: ',ans


;  for each feature, overplot registered data on the reference image
for jidx=0,nidx-1 do begin
  qidx = index(jidx)
  tstart = fmt_tim(anytim(struct(qidx).date_obs,/ecs))
  if jidx eq 0 then begin
    print,'Time of SFC data: ',tstart
    hfc_time = tstart
  endif

  if keyword_set(debug) then begin
    print,''
    print,'Time of Reference Image: ',img_time
    print,'Time of HFC data:  ',tstart,qidx
  endif
  
  cen_x = struct(qidx).center_x
  cen_y = struct(qidx).center_y
  xscl  = struct(qidx).cdelt1
  yscl  = struct(qidx).cdelt2

  if not sunspot then begin

	if have_tag(struct,'cc_pix_x') then begin
    	cc_x0 = struct(qidx).cc_pix_x
    	cc_y0 = struct(qidx).cc_pix_y
   		hfr_ccode2xy,struct(qidx).chain_code,xf,yf
	endif else begin
	    cc_x0 = struct(qidx).cc_x_pix
    	cc_y0 = struct(qidx).cc_y_pix
   		hfr_ccode2xy,struct(qidx).cc,xf,yf
	endelse

;  add relative posn. to chain code start posn.,
;  make relative to sun centre and convert to arcsec
    xrcc = ((xf + cc_x0) - cen_x)*xscl
    yrcc = ((yf + cc_y0) - cen_y)*yscl

;?????????????????????????????????????????????????????????????????
;  Could use struct.cc_arc_x and struct.cc_arc_y instead, depending
;  on the precision of the data.

  endif else begin         ; Sunspot

    if (struct(qidx).feat_npix lt 4) $
	or (struct(qidx).br_pix_x1-struct(qidx).br_pix_x0 eq 0) $
	or (struct(qidx).br_pix_y1-struct(qidx).br_pix_y0 eq 0) $
	then begin
;      if kreject lt 10 then print,'** Too small a spot:',struct(qidx).feat_npix
      kreject = kreject+1
      goto, next
    endif

    hfr_raster2cont,struct(qidx),xy,info

;  make relative to sun centre and convert to arcsec
    xrcc = (xy(0,*) - cen_x)*xscl
    yrcc = (xy(1,*) - cen_y)*yscl
			  
  endelse

;  help,xrcc,yrcc
  if keyword_set(debug) then begin
    print,'* Starting corner:',xrcc(0),yrcc(0),n_elements(xrcc)
    ;if not sunspot then print,'* cc_arc_x and cc_arc_y',struct(qidx).cc_arc_x,struct(qidx).cc_arc_y
  endif

;-----------
;  feature data are at zero roll (???), so rotate them then roll if necessary

;drot_xy, xrcc,yrcc, tstart,img_time, rx,ry, roll_angle=map.roll_angle
;;rx = xrcc & ry = yrcc
;;goto, skip

;  rotate the features to the time of the reference image
  if fmt_tim(tstart) ne fmt_tim(img_time) then begin
    rr=rot_xy(xrcc,yrcc,tstart=tstart,tend=img_time)
  endif else begin
;  SFC data at same time as reference image
    rr = [[xrcc],[yrcc]]
  endelse

;  If roll of reference image is non zero, also roll the features. The roll angle is 
;  determined by reference image, but the roll is about the center of the feature image
  if map.roll_angle ne 0 then begin
    center_roll = [struct.center_x,struct.center_y]
    roll_xy, rr(*,0), rr(*,1), map.roll_angle, rx, ry	;, center=center_roll
  endif else begin
    rx = rr(*,0) & ry = rr(*,1)
  endelse

skip:

;  now plot the feature  
  linecolor,1,contour_color

  if not sunspot then begin
;  Filament structure may have tag flagging filament segment as wrongly identified sunspot 
    if have_tag(struct,'false_ss') then if struct(qidx).false_ss then linecolor,1,'orange'
;;    wgd = where(rx ne -9999 and ry ne -9999)
    wgd = where(rx ge -9000 and ry ge -9000)
    oplot, rx(wgd), ry(wgd), color=1	;, thick=2

  endif else begin
;  plot each contour of a sunspot - penumbra and (multiple) umbra
    for j=0,n_elements(info)-1 do begin
      s = [indgen(info(j).n),0]
      ss = info(j).offset+s
      oplot, rx(ss), ry(ss),color=1
    endfor
		    
  endelse

  ;read,'Pause? '+string(fix(struct(qidx).feat_npix))+': ',ans
  next:
endfor

if sunspot then print,'** Spots rejected (too small size/shape): ',string(kreject,nidx,format='(i3,1h/,i3)')

linecolor,1,contour_color
ypos = 0.95						; filaments
if not have_tag(struct, 'ske_len_deg') then ypos=0.92	; active regions
if sunspot then ypos=0.03				; sunspots
xl = 0.05*(!x.crange(1)-!x.crange(0))+!x.crange(0)
yl = ypos*(!y.crange(1)-!y.crange(0))+!y.crange(0)
xyouts,xl,yl,'HFC:  '+hfc_time,charsiz=1.5,color=1

end
