pro hio_get_fits, data, header, local=local, data_paths=data_paths, $
		map=map, convert2map=convert2map, $
		wavelength=wavelength, observatory=observatory, timerange=timerange, $
		debug=debug, used_url=used_url, _extra=extra

;+
; NAME:
;         hio_get_fits
; PURPOSE:
;         Ingest a FITS files, either from local directory or from GSFC (via QLK) 
;
; CALLING SEQUENCE:
;         hio_get_fits, data, header  [,map=map, /conv]
;         hio_get_fits, map=map, /conv
;         hio_get_fits, /wave, map=map, /conv  [,used=url]
;
; OUTPUTS:
;         data          Data array from FITS file
;         header        Header of FITS file
; KEYWORD INPUT:
;         time_range	2 element array containing start/end times used in QLK search
;         observatory   select from QLK using observatory as primary key
;         wavelength    select from QLK using wavelength as primary key
;         local         Use locally stored image  (saves time...)
;         data_paths    Local directory to use, def. is $EGSO_TEST_SEFC
;         convert2map   If set, convert FITS image to map object
; KEYWORD OUTPUT
;         map           Map object returned if keyword /convert2map set 
;         used_url      URL used to retrieve file
;
; RESTRICTIONS:
;         Must be connected to Internet to work correctly
;;;;;         Required Java V1.4 or later
;
; METHOD:
;         User asked to select FITS image within specified time range (using
;         EGSO QLK catalogue). Image retrieved from GSFC. 
;
; HISTORY:
;         22-JUN-2011, Written by RDB (adapted from egso_get_fits.pro)
;
;-
  
data = -1 & header = ''
used_url = ''

if not keyword_set(local) then begin
  filename = sell_up(wavelength=wavelength, observatory=observatory, timerange=timerange, debug=debug)
  used_url = filename

  print,'* Reading: ',filename
  if not keyword_set(debug) then begin

    if is_compressed(filename) eq 0 then begin
      sock_fits,filename,data,header,/verb,err=err
      if err ne '' then return

    endif else begin
      odir = curdir()		;store in current directory unless /tmp exists
;      if dir_exist('/tmp') then odir = '/tmp'
;      if dir_exist('temp') then odir = 'temp'
      break_file,filename,aa,bb,cc,dd,ee
      local_filename = concat_dir(odir,cc+dd)

      mess = ['** File is compressed ** -> '+local_filename, $
              '****  Copy and Read ****']
      box_message,mess

      sock_copy, filename, /verb, err=err, out_dir=odir
      if err ne '' then return
      
      data = readfits(local_filename,header)
    endelse

  endif

endif else begin
;  look for local files in $EGSO_TEST_SEFC unless directory defined
  if keyword_set(data_paths) then paths = data_paths $
    else paths = '$EGSO_TEST_SEFC'
  list = file_list(paths,'*.*')

;  sort the list by observation type and date
  break_file,list,aa,bb,cc,dd
  vv=strpos(cc+dd,'_')
  ss=strarr(n_elements(cc))
  for j=0,n_elements(cc)-1 do ss(j)=strmid((cc+dd)(j),vv(j)+1)
  files = list(sort(ss))


;  X.B, 22-JUN-2011: use IDL dialog_pickfile routine instead of wmenu_sel
	fx = dialog_pickfile(path=paths,filter='*.*')

;  fx = wmenu_sel(files,/one)
  if fx(0) eq '' then return $ 
    else filename = fx(0)
  used_url = filename

  print,'* Reading: ',filename
;;  data = readfits(filename,header)
  data = mrdfits(filename,0,header)  ; hopefully less OS dependant

endelse
help,data,header

if keyword_set(convert2map) then begin
  index = fitshead2struct(header)
  index2map, index,data, map, err=err, _extra=extra
endif


end
