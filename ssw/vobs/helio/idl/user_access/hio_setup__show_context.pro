
;----------------------------------------------------------------

pro hio_setup::context_cursor, boxcursor=boxcursor, verbose=verbose

;--  use cursor on context plot from HELIO CXS

message,'Define band or region of interset',/info

cxs_return = *self.cxs_return

if datatype(cxs_return) ne 'STC' then begin
  box_message,['>>> CXS Return is not of the correct type ', $
    		'>>> Have you executed helio -> show_context ? ']
  return
endif
if self.flag_new_cxs eq 1 then begin
  box_message,'>>> New times - you need to execute  helio -> show_context'
  return  
endif

info_url = cxs_return.info_url

csrstc = hio_cxsimage_cursor(info_url, boxcursor=boxcursor, verbose=verbose)

;    returned structure has info about the image, its scaling and the cursor values
;if keyword_set(verbose) then $
	help,/st, csrstc

;    store retults in the object   <<<<<< ????? not sure this is the right stuff

self.cxsimage_roi = [csrstc.box_xd, csrstc.box_yd]
help, self.cxsimage_roi

self.flag_roi_mode = 'cxs-cursor'					; way that ROI was selected

;>>>>>>> 
;>>>>>>>  need to convert coordinates tp lat_hg, long_carr
;>>>>>>> 

lat_hg = 0.0 & long_hg=0.0 & long_carr=0.0
fov = [csrstc.box_xd(1)-csrstc.box_xd(0), csrstc.box_yd(1)-csrstc.box_yd(0)]
print,fov

temp = {time_start:csrstc.time_ref, lat_hg:lat_hg, $
  			long_hg:long_hg, long_carr:long_carr, fov:fov}
*self.roi_info = temp

end

;----------------------------------------------------------------

pro hio_setup::show_context, verbose=verbose, $
	goes_plot=goes_plot, request_data=request_data, $
	nodisplay=nodisplay, test=test, $
	parker=parker, velocity=velocity, hee=hee

;--  get context plot from HELIO CXS

;???? which time range
;;  times = *self.timewindow
;;  times = *(self.timerange)

;help, *self.timewindow
;times = *self.timewindow

if n_elements(*self.timewindow) eq 0 then begin
  if n_elements(*(self.timerange)) eq 0 then begin
    message,'No times set',/info
    return
  endif else times = *(self.timerange)
endif else times = *self.timewindow


fmt_timer, times

rqtime = times
if not keyword_set(goes_plot) then rqtime = times(0)
;help, rqtime

cxs_return = hio_do_cxs(rqtime, parker=parker, velocity=velocity, hee=hee, $ 
				goes_plot=goes_plot, request_data=request_data, $
				nodisplay=nodisplay, test=test, verbose=verbose)

*self.cxs_return = cxs_return
self.flag_new_cxs = 0

end
