;    This routine allows the plots in a multi-plot to be selected by "name"
;    This makes it more flexible than earlier versions... 

pro sel_mplot_sep, times, sep_sel, over=over, xmargin=xmargin, charsiz=csiz, verbose=verbose

psel = strupcase(sep_sel)
message, 'plotting '+psel, /info
case psel of
  'STA':  hio_plot_stereop, times, miss='STA', /over, xmargin=xmargin, charsiz=csiz, verbose=verbose
  'STB':  hio_plot_stereop, times, miss='STB', /over, xmargin=xmargin, charsiz=csiz, verbose=verbose
  'GOES': eau_plot_goesp, times, xmargin=xmargin, charsiz=csiz, tit='GOES Proton Flux'  ,/force
  'LRO':  hio_plot_lro, times, /over, xmargin=xmargin, charsiz=csiz
  else:   message,'Selected dataset not recognised: '+psel, /info
endcase

return
end


;+
; PROJECT:
;        HELIO
; NAME:
;        plot_multi_proton
; PURPOSE:
;        Produce 3 panel plot of STB, GOES and STA protons <<<<<<<<<<<<<
;        Overplot with CMEs from cactus_all and proton events seen by GOES
; CALLING SEQUENCE:
;        helio -> plot_multi_proton  [, timer=rtimes]
; CATEGORY:
;        
; INPUTS:
;        timerange   limits of plot if supplied; requests choice from vobs_presets if not???
; INPUT KEYWORDS:
;        oplot_cme   overplot the times of CME events from the HEC - def is cactus_all
;        oplot_sep   overplot the times of SEP events from the HEC - def is goes_proton_event
;                    oplot_sep=2 will cause the trutine to ask user to choose the list
;        verbose     prints extra information
; OUTPUTS:
;        
; OUTPUT KEYWORDS:
;        
; RESTRICTIONS:
;        
; HISTORY:
;    01-Apr-2014  rdb  written
;    05-Oct-2014  rdb  added the HELIO logo call
;    20-Oct-2014  rdb  updated; added the xmargin keyword
;    Jan 2017 heavy rework
;    22-Mar-2017  rdb  introduced same timerange code as normal plot
;
;-

;    should this be plot_multi_proton????

pro hio_setup::plot_multi_proton, timerange=timerange, zoom=zoom, $
		oplot_cme=oplot_cme, oplot_sep=oplot_sep, $
		use=use, verbose=verbose
;, select_times=select_times

; , rtimes
;;;;    is this done right??? what is rtimes not supplied?
;;;self -> set_basetime, timerange=rtimes
;;;
;;;times = *self.timerange
;;;fmt_timer, times

;----   set time range, either from pbject or as supplied

if not keyword_set(timerange) then begin

;    default is to use time window from the object

  message,'Using times defined in object',/info

  if not keyword_set(zoom) then begin 
;    use the overall basetime interval

    if n_elements(*(self.timerange)) eq 0 then begin
      box_message,'Need to set a base time'
      self -> set_basetime, timerange=timerange
    endif
    base_times = *(self.timerange)
    
  endif else begin

;    allow zoom to smaller time window if interval suplied and zoom keywrd used
;    if zoom is a 2 eleemnt array, this represents [time_before, time_after]

    if n_elements(*(self.timewindow)) lt 2 then begin
      box_message, ['    ** Time Window is not defined ** ', $
      				'Select an event or select times from plot']
      return
    endif
    
    base_times = *(self.timewindow)
    if n_elements(zoom) eq 2 then begin
      base_times(0) = anytim(addtime(base_times(0),delt=-zoom(0)),/ecs)
      base_times(1) = anytim(addtime(base_times(1),delt=zoom(1)),/ecs)
    
      self.twind_zoom = zoom
    endif
    
  endelse
  
;;  print, 'Plot time range:', base_times, format='(a,t20,a,2x,2hto,2x,a)'

endif else begin

  message,'Using specifically supplied time',/info
  base_times = timerange

endelse

print, ''
print, 'Plot time range:', base_times, format='(a,t20,a,2x,2hto,2x,a)'

times = base_times

;    try to catch very long time intervals ??????????????


clearplot
hio_plot_setup,/inv
!p.multi=0
 
!noeras=0
ptitle = 'Multi-mission Protons2'
window, xsiz=700, ysiz=900, title=ptitle
!p.multi=[0,1,3]

csiz = 1.9
;;;;if !p.multi(2) eq 2 then csiz = 1.3

if not keyword_set(use) then begin

;    plot protons from STEREO-B HET
hio_plot_stereop, times, miss='STB', /over, xmargin=[10,6], charsiz=csiz, verbose=verbose

plims = [!x.window(0),!y.window(0),!x.window(1),!y.window(1)]			; setup for overplot

;    plot protons from GOES 
eau_plot_goesp, times, xmargin=[10,6], charsiz=csiz, tit='GOES Protons'  ,/force

;    plot protons from STEREO-A HET
hio_plot_stereop, times, miss='STA', /over, xmargin=[10,6], charsiz=csiz, verbose=verbose

endif else begin
  if n_elements(use) ne 3 then begin
    message,'Must supply 3 element array',/info
    return
  endif
  
  for j=0,2 do begin
    sep_sel = use(j) 		;& help, sep_sel
    xmargin = [10,6]
    csiz = 1.9
    sel_mplot_sep, times, sep_sel, xmargin=xmargin, charsiz=csiz, verbose=verbose
    
    if j eq 0 then plims = [!x.window(0),!y.window(0),!x.window(1),!y.window(1)]			; setup for overplot
  endfor
endelse

plims(1) = !y.window(0)													; extend for all 3 plots

hio_oplot_icon							; put logo on plot

fmt_timer, times
    
;    allow overplotting of events that span all panels

print,plims
!p.position = plims 		; set window used by cursors

!noeras = 1
utplot, times,[0,1], /nodata, timer=times, xst=5,yst=5, xmargin=[10,6]

if keyword_set(oplot_cme) then begin
  self -> load_eventlist, name='cactus_all'
  self -> show_event, vel=600, pawid=90, /over,  /all, color=3
endif

if keyword_set(oplot_sep) then begin
  if oplot_sep eq 1 then self -> load_eventlist, name='goes_proton_event' $
  	else self -> load_eventlist, /choose,/part
  self -> show_event, /over,  /all, color=2
endif

self.flag_plot_done = 1					; need plot before can user corsors

hio_save_prec, 'tplot', ptitle

end