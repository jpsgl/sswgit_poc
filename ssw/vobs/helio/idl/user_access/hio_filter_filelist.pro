;+
; NAME:
;    
; PURPOSE:
;    
;    filter by time
;      observations within or overlapping supplied time range
;
;    allow cadence filter for GONG-Ha images
;
;    should also filter spatially
;      by FOV for remote-sensed image
;      by volume of interset for in-situ data
;
;    return is an index of the files that fit the selection criteria
;
; CALLING SEQUENCE:
;    
; CATEGORY:
;    
; INPUTS:
;    
; INPUT KEYWORDS:
;    
; OUTPUTS:
;    
; OUTPUT KEYWORDS:
;    
; RESTRICTIONS:
;    
; HISTORY:
;    01-Apr-2011  rdb  written
;
;-

function hio_filter_filelist, struct, timerange, fold=fold, debug=debug, $
		cadence=cadence, rate_perhour=rate_perhour, $
		plot_gongha=plot_gongha


t0 = systime(/sec)

fmt_timer, timerange

twok1 = -1 & twok2 = -1

;goto, sect2

;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;    include only files that overlap with the time window

time_before = -60.    ;allow one hour before flagged time
help,time_before

times = anytim(timerange,/ecs)
times(0) = anytim(addtime(times(0),delt=time_before),/ecs)
fmt_timer,times
print,''

message,'Number of file names:'+string(n_elements(struct)),/cont

;    RHESSI and SOHO only return valid files
;    NB: some SOHO may start well before window, but start and end times known
;      GOLF, COSTEP, CELIAS, VIRGO and CDS, UVCS - needs care...

whsi = where(struct.observatory eq 'RHESSI') ; or struct.observatory eq 'SOHO')
wnhsi =  where(struct.observatory ne 'RHESSI') ; and struct.observatory ne 'SOHO')

twok = -1
if wnhsi(0) ge 0 then begin
  wwsta = anytim2tai(struct(wnhsi).time_start)
  wwstp = anytim2tai(struct(wnhsi).time_end)  
  tstart = anytim2tai(times(0)) & tend = anytim2tai(times(1))

  wok1 = where(wwsta ge tstart and wwsta lt tend)
  wok2 = where(wwstp gt tstart and wwstp le tend)
  wok3 = where(wwsta le tstart and wwstp ge tend)

  if keyword_set(debug) then begin
    print,'* time window:  ',long([tstart,tend])
    print,'* min/max times:',long([min(wwsta),max(wwstp)])
    help,wok1,wok2,wok3
  endif

  wok = all_vals([wok1,wok2,wok3])
  if wok(0) eq -1 and n_elements(wok) gt 1 then wok=wok(1:*)
  if wok(0) ge 0 then twok = [wnhsi(wok),whsi] else twok = whsi
endif

twok = twok(sort(twok))					; ?????????
qwok = where(twok ge 0, nqwok)

if nqwok gt 0 then begin
  message,'Number falling within time range:'+string(nqwok),/cont
  twok = twok(qwok)

endif else begin
  message,'No files fall in time range',/cont
  return,-1

endelse

twok1 = twok			; all observaions within time window

qv = where(struct(twok).obsinst_key eq 'GONG__HALPH', nqv)
if nqv gt 0  then twok2 = twok1(qv)
;;qv = where(struct(twok).obsinst_key eq 'PROBA_2__SWAP', nqv)
;;if nqv gt 0  then twok4 = twok1(qv)

;goto, endit

sect2:

;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;    now identify images that match desired cadence

twok = -1		;<<<<<<<

if not keyword_set(cadence) then goto, endit
  box_message,'Filter images by cadence'
  
  dt = 5					; time window - nins. either side of desired time 
  nph = 1					; default no. images per hour

  sdate = timerange(0)
  edate = timerange(1)
  ndays = addtime(edate,diff=sdate)/60./24.
  help, ndays

  all_keys = all_vals(struct.obsinst_key)
  isdone = intarr(n_elements(all_keys))

  print, 'Included keys:  ', all_keys

;    GONG H-alpha images are from 6 (or more) sites with overlaps - makes cadence more complex
;    time difference from local noon is inserted by the repair routine - try to minimize

twok = -1
qv = where(struct.obsinst_key eq 'GONG__HALPH', ngong)
if ngong gt 0 then begin
  print,''
  message, '>>>> GONG-Ha images by cadence', /info
  qp = where(all_keys eq 'GONG__HALPH', nqp)
  if nqp eq 1 then isdone(qp(0)) = 1
  
;;  twok2 = qv					; time window??
  
  if keyword_set(rate_perhour) then nph = rate_perhour		; modified frame rate
  nmin = 60./nph			; cadence in minutes
  if nmin lt 60 then dt=3
  ncad = ndays*24*nph		; total number of images
  help, ncad

  idx = intarr(ncad)-1

;    convert times for GONG -Ha images in the filelist to FIDS

  rfids = str_replace(time2fid(struct(qv).time_start,/full,/time,/sec),'_','')
  if keyword_set(debug) then print,rfids(0:2), format='(a)'

  for j=0,ncad-1 do begin
  
;    define a time window around the cadence time (using value of DT)

    ctime = addtime(sdate,delta=j*nmin)
    ct0 = str_replace(time2fid(anytim(addtime(ctime,delta=-dt),/ecs),/full,/time),'_','')+'00'
    ct1 = str_replace(time2fid(anytim(addtime(ctime,delta=dt),/ecs),/full,/time),'_','')+'00'

;    identify imgaes within the time window and find one with minimum tdiff

    qx = where((rfids ge ct0) and (rfids le ct1), nqx)
    if keyword_set(debug) then print, j, ct0, ct1, nqx, format='(i3,2x,a,2x,a,i5)'

    if nqx gt 0 then begin
      atdiff = abs(struct(qv(qx)).tdiff)
      cidx = where(atdiff eq min(atdiff))
      idx(j) = qv(qx(cidx))
    endif

  endfor

  qwok = where(idx ge 0, nqwok)
  
  if nqwok gt 0 then begin
    message,'Number GONG-Ha matching cadence:'+string(nqwok),/cont
;;    twok3 = idx(qwok)					;??? qv?
    twok3 = qv(idx(qwok))					;??? qv?

  endif else begin
    message,'No files fall in time range',/cont
;;    return,-1			;???

  endelse

;    plot the offset from local noon of the images and overplot selected images

  if keyword_set(plot_gongha) and nqwok gt 0 then begin    
    cwind = !d.window
    window, 28

    plot_gongha_tdiff, struct

    linecolor,1,'red'
    idx2 = idx(where(idx ge 0))  &  nidx2 = n_elements(idx2)
    outplot,struct(qv(idx2)).time_start, struct(qv(idx2)).tdiff, psym=2,color=1
    outplot,struct(qv(idx2)).time_start, intarr(nidx2)-0.5, psym=2,color=1

    wset, cwind
  endif

endif

;;twok3 = twok

;>>>>    fold the results tgother

folded = -1
if keyword_set(fold) then begin					; always fold ????????
  folded = intarr(n_elements(struct))
  folded(twok1) = 1								; all in time window
  
  twok0 = where(folded eq 0)					; convert to mask...

  folded = intarr(n_elements(struct))
  if ngong gt 0 then begin
    folded(twok2) = 0		; take out gongha all
    folded(twok3) = 1		; add      gongha cadence
  endif
endif

print,'+++++++1 NFOLD: ',n_elements(where(folded gt 0))

;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;    Other instruments may need to be selected by cadence - simpler that GONG-Ha?
;    For example, PROBA2/SWAP, SDO/AIA. Doed Hinode/MXTproduce images?
; 'PROBA_2__SWAP', 'HINODE__XRT', SDO__AIA_*

;    potentially this section should loop through "simple" instruments
;    SDO will probably need to be sorted by wavelength...

;    Instruments like SOHO/CDS and Hinode/EIS probably need different method 
;    Not survey instrument, complex modes and small FOVs


fl_oinst = ['PROBA_2__SWAP','HINODE__XRT']
nflo = n_elements(fl_oinst)

tidx = -1
tall = -1

for kk=0,nflo-1 do begin
  coinst = fl_oinst(kk)
  qv = where(struct.obsinst_key eq coinst, nqv)

  if nqv gt 0 then begin
  print,''
  clab = str_replace(coinst,'__','/')
  print, clab, nqv, format='(a,t20,i7)'
    qp = where(all_keys eq coinst, nqp)
    if nqp eq 1 then isdone(qp(0)) = 1

    idx = hio_do_cadence(struct(qv), timerange, use=rate_perhour, label=clab)
    help,idx
    tidx = [tidx, qv(idx)]
    tall = [tall, qv]
  endif
endfor

  tidx = tidx(where(tidx ge 0, ntidx))
  tall = tidx(where(tall ge 0, ntall))
  message,'>>>>> Total No. of selected Other images: ' + string(ntidx),/info
  
  twok4 = tall
  twok5 = tidx

if keyword_set(fold) then begin					; always fold ????????
  if ntidx gt 0 then begin
    folded(twok4) = 0		; take out Other all
    folded(twok5) = 1		; add      Other cadence
  endif
endif

print,'+++++++2 NFOLD: ',n_elements(where(folded gt 0))

;+++++++++++++++++++++++++++++++++++++++++++

print,''

goto, txtx

twok5 = -1
qv = where(struct.obsinst_key eq 'PROBA_2__SWAP', nswap)
help, nswap, n_elements(struct)
if nswap gt 0 then begin
  message, '>>>> PROBA2/SWAP images by cadence', /info
  
  twok4 = qv				; timee window ??
  
  if keyword_set(rate_perhour) then nph = rate_perhour		; modified frame rate
  nmin = 60./nph			; cadence in minutes
  ncad = ndays*24*nph		; total number of images
  help, ncad

  idx = intarr(ncad)-1

  dsec = addtime(struct(qv).time_start, diff=sdate)
  help,dsec
;  print,dsec
  
  for j=0,ncad-1 do begin
    cdsec = abs(dsec - nmin*j)
;    cdsec = dsec + nmin*j
;    print,cdsec
    px = where(cdsec eq min(cdsec))
    idx (j) = px(0)
  endfor
;;  twok5 = qv(idx)

  qwok = where(idx ge 0, nqwok)
  
  if nqwok gt 0 then begin
    message,'Number PROBA2/SWAP matching cadence:'+string(nqwok),/cont
    twok5 = qv(idx(qwok))

  endif else begin
    message,'No files fall in time range',/cont
;;    return,-1			;???

  endelse

;>>>>    fold the results tgother
  
if keyword_set(fold) then begin					; always fold ????????
  if nswap gt 0 then begin
    folded(twok4) = 0		; take out proba2/swap all
    folded(twok5) = 1		; add      proba2/swap cadence
  endif
endif

endif

txtx:

;    examine what is in SDO

twok6 = -1 & twok7 = -1
qv = where(strpos(struct.obsinst_key,'SDO__AIA') eq 0, nqv)
if nqv gt 0 then begin
  message, '>>>> SDO/AIA images by cadence', /info
  print,'Total No. of SDO/AIA images: ',nqv

    qp = where(strpos(all_keys,'SDO__AIA') eq 0, nqp)
    if nqp gt 0 then isdone(qp) = 1
  
  aia_waves = all_vals(struct(qv).channel)
  aia_waves = aia_waves(sort(fix(aia_waves)))
  nwv = n_elements(aia_waves)
  kwaves = intarr(nwv)
  tidx = -1
  
  for j=0,nwv-1 do begin
    cwave = aia_waves(j)
    qw = where(struct(qv).channel eq cwave, nqw)
    kwaves(j) = nqw
    
print,''
help, nqw		;, cwave		;n_elements(struct)
if nqw gt 0 then begin
  idx = hio_do_cadence(struct(qv(qw)), timerange, use=rate_perhour, label='SDO/AIA-'+cwave)
;  help,idx
  tidx = [tidx, qv(qw(idx))]
endif

  endfor
  
  print,aia_waves, format='(15i6)'
  print,kwaves, format='(15i6)'
  
  tidx = tidx(where(tidx ge 0, ntidx))
  print,'TNo. of selected SDO/AIA images: ', ntidx
  
  twok6 = qv
  twok7 = tidx

;>>>>    fold the results tgother
  
if keyword_set(fold) then begin					; always fold ????????
  if ntidx gt 0 then begin
    folded(twok6) = 0		; take out sdo/aia all
    folded(twok7) = 1		; add      sdo/aia cadence
  endif
endif

print,'+++++++3 NFOLD: ',n_elements(where(folded gt 0))

endif

endit:

print,all_keys
print,isdone
  
qz = where(isdone eq 0, nqz)
if nqz gt 0 then begin
  for j=0,nqz-1 do begin
    ckey = all_keys(qz(j))
    qv = where(struct.obsinst_key eq ckey, nqv)
    print,'>>>> '+ckey,nqv
    if nqv gt 0 then folded(qv) = 1
  endfor
endif

folded(twok0) = 0			; mak out what not in time window
  
;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;    finalize the folding of the instrument cadences

if keyword_set(fold) then begin					; always fold ????????
  qf = where(folded gt 0, nqf)
  if nqf gt 0 then folded = qf		;folded(qf)
endif

print,'+++++++4 NFOLD: ',n_elements(where(folded gt 0))

;help, twok1, twok2, twok3, twok4, twok5, twok6, twok7
print,''
print,'Took: ',systime(/sec)-t0

;    return is an index of the files that fit the selection criteria

;;return, twok

;;return, {all:twok1, gongha_cad:twok3, swap_cad:twok5, aia_cad:twok7, folded:folded}
return, {all:twok1, gongha_cad:twok3, other_cad:twok5, aia_cad:twok7, folded:folded}

end
