;       hio_setup__define
; PURPOSE:

;  March 2011
;  10-April-2011 @ TCD

;----------------------------------------------------------------

function hio_setup::init, notest=notest

;help, notest

;-- allocate memory to pointer when initialising the object

;;message,'work in progress',/info

self.timerange = ptr_new(/allocate)
self.timewindow = ptr_new(/allocate)
self.twind_zoom = [0,0]

self.eventlist_table = ptr_new(/allocate)
self.eventlist_row = -1		; nothing selected

;   load allowed instruments?
self.pat_oinst = ptr_new(/allocate)
self.obs_inst = ptr_new(/allocate)

;   structure created from return by the DPAS
self.filelist = ptr_new(/allocate)
self.flist_timewindow = ptr_new(/allocate)
self.flist_filters = ptr_new(/allocate)

;   allocate pointer for the HEC info table
self.hec_tabinfo = ptr_new(/allocate)

self.flag_new_etimes = 1
self.flag_new_cxs = 1

self.prop_delays = ptr_new(/allocate)
;    ?? create the structure
self.prop_base_event = ptr_new(/allocate)

self.flag_plot_otype = 'none'
self.flag_select_mode = 'NOT set'

self.cxsimage_roi = fltarr(4)

self.cxs_return = ptr_new(/allocate)

self.roi_info = ptr_new(/allocate)

;   load hio_defaults and plot defaults

self -> set_defaults, /reset, notest=notest    	; force load of nodes from registry

return, -1
end

;----------------------------------------------------------------

pro hio_setup::set_defaults, verbose=verbose, reset=reset, notest=notest

;   load hio_defaults and plot defaults
;   allows resetting these - "init" covered by lifecycle rules

;use_network				; seem to need this, 2015-03-18

if not have_network(/reset) then begin
  message,'**** SEVERE Problem - are you connected to the network?', /info
  return
endif

hio_load_defaults, verbose=verbose, reset=reset, /short
;;temp = hio_hrs_find()

;;hio_check_roots, verbose=verbose
if not keyword_set(notest) then self -> test_services, verbose=verbose

if keyword_set(verbose) then help, !hio_sysvar
 
hio_plot_setup
self.bkg_invert = 1					; default to white bakcground

;    only load the table from the HEC if it is needed
;self -> load_hec_tabinfo

end

;----------------------------------------------------------------

pro hio_setup::test_services, verbose=verbose		;, reset=reset

;    test that the nodes work switch if not...

if not have_network(/reset) then begin
  message,'**** SEVERE Problem - are you connected to the network?', /info
  return
endif

hio_check_roots, verbose=verbose

end

;----------------------------------------------------------------

function hio_setup::cleanup

;-- free memory allocated to pointer when destroying object

message,'Not yet implemented',/info

return, -1
end

;----------------------------------------------------------------

pro hio_setup::help, browser=browser, sheet=sheet

;--  provide user with help information

if keyword_set(browser) then begin

;--  display help information in browser

  self -> browser,/help

endif else begin

;--  shows crib sheet in a pop-up window

  qsheet = 0
  if keyword_set(sheet) then qsheet=sheet

  case qsheet of
    2: help_file = '$HELIO_DOC/hio_crib_sheet_2.txt'
    3: help_file = '$HELIO_DOC/hio_crib_sheet_3.txt'
    else: help_file = '$HELIO_DOC/hio_crib_sheet.txt'
  endcase

  text = rd_tfile(help_file)
  xpopup, text, xsiz=95,ysiz=30, title='HELIO SSW Crib Sheet'

endelse

end

;----------------------------------------------------------------

pro hio_setup::set_basetime, timerange=timerange, nar=nar

;--  set up the basic time range that will be used

;    default time range...
def_times = ['20-jan-2005 00:00','22-jan-2005 04:00']
basetime_mode = 'default ??'		; will this ever be seen

self.nar_used = 0					; must always clear for new event
self.plot_longbasetime = 0			; by default ask about long base times when plotting

;    select some kind of time interval

case 1 of 
keyword_set(timerange): begin
    mess = 'BASETIME set using supplied time range'
    base_times = timerange
    basetime_mode = 'timerange'
  end
keyword_set(nar): begin
    mess = 'BASETIME set using NOAA Active Region (NAR) No.'
    base_times = get_nar_timerange(nar)
    self.nar_used = nar
    basetime_mode = 'NAR'
  end
else: begin
    mess = 'BASETIME set using VOBS Presets'
    base_times = vobs_preset_times(action, /force)
    basetime_mode = 'presets'
  end
endcase

help, base_times
if datatype(base_times) ne 'STR' then begin
  box_message, 'ERROR of some kind in specifying Base Time'
  return
endif

;    try to catch badly specified time interval  
t0 = anytim(base_times(0),/ecs,/trunc,err=err0)
t1 = anytim(base_times(1),/ecs,/trunc,err=err1)
if err0 ne 0 or err1 ne 0 then begin
  t0 = base_times(0) & t1 = base_times(1)
  if err0 gt 0 then t0 = t0 + '      <<<<'
  if err1 gt 0 then t1 = t1 + '      <<<<'
  box_message,['***  ERROR in specifying Base Time  ***', t0,t1]		;base_times]
  return
endif else base_times = [t0,t1]

;    is this redundant??  
if n_elements(base_times) eq 1 then begin
  box_message,['','No time range defined - using default','']
  base_times = def_times
endif

;    check the validity of the time range

fmt_timer,base_times, tim0,tim1, /quiet
print,''
print, '>>>> ' + mess
print, "(first, last) = '" + tim0 + "', '" + tim1 + "'"

tdiff = addtime(base_times(1),diff=base_times(0))
if tdiff le 0 then begin
  help,tdiff
  box_message,['** time_end LESS than time_start **', $
               '**      Time range NOT set       **']
  return
endif
if tdiff/60/24 gt 30 then begin
  box_message,'** WARNING - Time range GREATER than 30 days **'
endif

;    save the interval and set the flag

  *(self.timerange) = fmt_tim(base_times)
  self.flag_basetime_mode = basetime_mode

  self.flag_new_etimes = 1		;set flags
  self.flag_new_cxs = 1
  
end

;----------------------------------------------------------------

pro hio_setup::select_times, use_basetime=use_basetime, apply_zoom=apply_zoom

;        2017-03-26  rdb  Added /apply_zoom option

;--  set selected time range to values in base time

if keyword_set(use_basetime) then begin
  message, 'Setting selected time to base time',/info
  times = *(self.timerange)

  self.flag_select_mode = 'use basetime'
  self.flag_new_etimes = 0
  goto, set_time
endif

;--  Apply any zoom offsets to timewindow and set as new timewindow  ??????????

if keyword_set(apply_zoom) then begin
  message, 'Apply any zoom offsets to timewindow and set as new timewindow',/info
  if n_elements(*(self.timewindow)) eq 2 then begin
    fmt_timer,*(self.timewindow), tmin, tmax, /quiet
    print,'Time Window: ',tmin,tmax, format='(a,t26,a,6h  to  ,a)'
  endif else begin
    print,'Time Window:','NOT Set', format='(a,t26,a)'
    return
  endelse

  zoom = self.twind_zoom
  print,'Zoom offsets (mins):', zoom, format='(a,t30,2i6)'
  
  if n_elements(zoom) eq 2 then begin
    times = *(self.timewindow)
    print,'Time Window (pre): ',times, format='(a,t26,a,6h  to  ,a)'
    times(0) = anytim(addtime(times(0),delt=-zoom(0)),/ecs)
    times(1) = anytim(addtime(times(1),delt=zoom(1)),/ecs)
    fmt_timer,times, tmin, tmax, /quiet
    times = [tmin,tmax]
    print,'Time Window (post): ',times, format='(a,t26,a,6h  to  ,a)'
    
    self.twind_zoom = [0,0]
  endif
;  return
  goto, set_time

endif

;--  use cursors to select time interval on a light curve

if not self.flag_plot_done then begin
  box_message,' ** You need to establish a plot first **'
  return
endif

  times = utcur_times()
  if self.debug then print,times

  self.flag_select_mode = 'light-curve'
  self.flag_new_etimes = 0

set_time:

;??? ensure tstart < tstop

  *(self.timewindow) = fmt_tim(times)

end

;----------------------------------------------------------------

pro hio_setup::ingest_eventlist, remote_url=remote_url

;--  retrieve the event list from a remote URL, e.g. the HEC

;;keyword_set(remote_url): begin

  print, ''
  message,'Ingest Event list from Remote URL',/info
  
  if datatype(remote_url) ne 'STR' then begin
    link=''
    read,'Please enter Remote URL: ',link
  endif else link = remote_url
  
  cmd = wget_stream() + link
  spawn, cmd, resp

  q0 = where(strpos(resp, 'sql> select') eq 0, nq0)
  q1 = where(strpos(resp, 'Elapsed time') eq 0, nq1)
  vot_text = resp
  if nq0 eq 1 and nq1 eq 1 then vot_text = resp(q0(0)+1:q1(0)-1)

  vv = decode_votable(vot_text,/quiet)
  help,vv & help,/st,vv
  if datatype(vv) ne 'STC' then begin
    box_message,'Supplied list is not valid!'	;,/info
    return
  endif
  
;    this method provides an alternate way of establsihing the base time
  
  times = vv.time_start
  fmt_timer, times, t0, t1, /quiet
  base_times = anytim([t0,t1], /ccs, /date)
  fmt_timer, base_times

  *(self.timerange) = base_times

  hec_list = 'unknown'
  help, hec_list
  
  self.eventlist_name = hec_list		; save name of selected event list
  *(self.eventlist_table) = vv			; save structure containing event list
  
;;end

end

;----------------------------------------------------------------

pro hio_setup::select_event, show=show, $
		pawidth_min=pawidth_min, xrayclass_min=xrayclass_min, $
		nar=nar

;--  select event from currently loaded event list

;    load table if it is not already loaded or interval has changed!

if n_elements(*self.eventlist_table) eq 0 or self.flag_new_etimes eq 1 then begin
  box_message,'NO Event List loaded OR Time Interval has changed'
  self -> load_eventlist, /choose	; defaults to goes_sxr_flare
endif

;    Must be a problem!!!!

  if self.eventlist_name eq '' then begin			; ?????????????????
    message,'NO Events available',/info
    return
  endif

if n_elements(*self.eventlist_table) eq 0 then $
    message,'You have not selected an Event List'

  clist = (*self.hec_tabinfo)(self.hec_tab_row)
  if keyword_set(show) then begin
    print,'Details of selected Event List:'
    help,/st,clist
  endif


;?? make adjustments selectable

hec_list = self.eventlist_name
resp = *(self.eventlist_table)

if keyword_set(show) then begin
  help, resp
  help,/st, resp
endif

;    select subset of columns if one is defined 

resp = select_hec_cols(resp, hec_list)		;, /verb)

;    allow only events above threshold for certain lists

flare_thresh = 'C1'
if keyword_set(xrayclass_min) then flare_thresh = strupcase(xrayclass_min)

;;if hec_list eq 'goes_sxr_flare' then begin

if have_tag(resp, 'xray_class') and have_tag(resp, 'time_end') then begin
  yy = fmt_gev_records(resp, thresh=flare_thresh, valid=valid)
  qv = where(valid ge 0, nqv)
  if nqv gt 0 then yy = yy(qv)
  ioff=0   ;1??
  
endif else begin
  yy = hio_fmt_stc_table(resp, /name)
  nqv = n_elements(resp)
  valid = intarr(nqv)+1				; needs to be defined 14/06/29
  ioff=1
  
endelse

;    allow only flares in specific NOAA Active Region (NAR)

if have_tag(resp,'nar') then begin		; flare specific <<<<<<<<<<
  if keyword_set(nar) then begin
    qx = where(resp.nar eq nar, nqx)
    print,'Limiting events plotted to NOAA Active Region (NAR): ',nar, nqx, n_elements(resp)
    yy = hio_fmt_stc_table(resp(qx), /name)
    nqv = nqx
    valid = qx
    ioff=1
  endif
endif


;    if list is related to CME allow user to only look at halo events

if clist.cme eq 'y' then begin
  if keyword_set(pawidth_min) then begin
    qv = where(resp.pa_width gt pawidth_min, nqv)
    valid = intarr(n_elements(resp))		;??
    calid(qv) = 1							;??
    if nqv gt 0 then yy = yy([0,qv+1]) $	; include title line
    else begin
      message,'No events with this pawidth_min:'+string(pawidth_min),/info
      return
    endelse
  endif
endif

;----

if nqv gt 0 then begin
  sel = hio_xmenu_sel(yy,/one,size=14, $
	title='Event List:  '+hec_list, $
	banner='Select require event')
	
  print,'Selected row:',sel
  print,yy(sel)

  if sel eq -1 then begin
    message,'NO event was selected',/info
    return
  end
  
;;  help,resp,valid,yy
  qx = where(valid gt 0)
;  help,/st,resp(qx(sel) - ioff)
  
;  self.eventlist_row = sel - ioff
  self.eventlist_row = qx(sel) - ioff
;;;  self.eventlist_row = qx(sel - ioff)

  event = resp(self.eventlist_row)

  if keyword_set(show) then begin
    print,'** Selected event **'
    help,/st,event
  endif

;??????   should this only be done when requested rather than automatically ????????

;>>>> need to check if there is a time_end field!!!
;>>>> add one, with a duration of ??? hours <<<< defaults ??  48 ??  

;  help,/st,!hio_defs  

  message,'Adjusting time ranges',/info
  if not tag_exist(event,'time_end') then begin
    message,'Tag time_end is absent - adding',/info
    event = add_tag(event,'','time_end')
    
    if tag_exist(event,'time_peak') then begin
      tpeak_plus = addtime(anytim(event.time_peak,/ecs),delt=!hio_defs.time_end_buff*60.)
    endif else $
      tpeak_plus = addtime(anytim(event.time_start,/ecs),delt=!hio_defs.time_end_buff*60.)
    event.time_end = anytim(tpeak_plus,/ecs,/trunc)
    help,/st,event

;    should the time before be multiplied for proton events?????

    dt_before = !hio_defs.ptn_buff_before * 60.			;!hio_defs.fl_buff_before * 4
    tstart_minus = addtime(anytim(event.time_start,/ecs), delt=-dt_before)
    tstart_minus = anytim(tstart_minus,/ecs,/trunc)
    
    event_times = [tstart_minus, event.time_end]
    event_times = anytim(event_times,/ecs)	; make sure times in same format...
    fmt_timer, event_times
;;ans=''
;;read,'Pause: ',ans

  endif else $
    event_times = hio_adjust_flaretimes(event)

  *(self.timewindow) = fmt_tim(event_times)

  self.flag_select_mode = 'event'			; way time window was selected
  
  self.flag_roi_mode = 'event'				; way that ROI was selected
  
  fov = [200.,200.]
  temp = {time_start:event.time_start, lat_hg:event.lat_hg, $
  			long_hg:event.long_hg, long_carr:0.0, fov:fov}
  if have_tag(event, 'long_carr') then temp.long_carr = event.long_carr
  
  *self.roi_info = temp

endif else begin
  message,'NO event could be selected',/info
  self.eventlist_row = -1
endelse

end

;----------------------------------------------------------------

pro hio_setup::show_event, browser=browser, $
	overplot=overplot, color=color, all=all, _extra=extra, $
	pawidth_min=pawidth_min, velocity_min=velocity_min, $
	xrayclass_min=xrayclass_min, $
	nar=nar, use_nar=use_nar, longhg_range=longhg_range
	
if self.eventlist_row lt 0 and not keyword_set(all) then begin
  message,'NO Event is currently selected',/info
  return
endif

;    load table if it is not already loaded or interval has changed!   ?????????????????

if n_elements(*self.eventlist_table) eq 0 or self.flag_new_etimes eq 1 then begin
  box_message,'NO Event List loaded OR Time Interval has changed'
  self -> load_eventlist, /choose	; defaults to goes_sxr_flare
endif

;--  general over-plot of events on plot

if keyword_set(overplot) then begin
  evlist = self.eventlist_name
  if evlist eq '' then begin
    message,'NO Events available',/info
    return
  endif
  message,'Over-plotting events from loaded table',/info
  print,'Current Event List:  ',evlist		;self.eventlist_name

  events = *self.eventlist_table
  nev = n_elements(events)
  rtemp = intarr(nev) + 1
  nqv = nev

  case 1 of

;    If a CME list, by default only plot where pa_width >90 degrees
  have_tag(events,'pa_width'): begin		; CME specific <<<<<<<<<<
      message,'CME list',/info      
      min_wid = 90
      min_vel = 300
      ctemp = intarr(nev) + 1

      if keyword_set(pawidth_min) then begin
        min_wid = pawidth_min
        print,'Limiting events plotted to PA Width >= ',min_wid
        qv = where(events.pa_width ge min_wid, nqv)
        ctemp = intarr(nev) 
        if nqv gt 0 then ctemp(qv) = 1 
      endif
      rtemp = rtemp and ctemp

      if keyword_set(velocity_min) then begin
        min_vel = velocity_min
        print,'Limiting events plotted to Velocity >= ',min_vel
        qv = where(events.v ge min_vel, nqv)
        ctemp = intarr(nev) 
        if nqv gt 0 then ctemp(qv) = 1 
      endif
      rtemp = rtemp and ctemp
      
      qv = where(rtemp gt 0, nqv)
    end 

  have_tag(events,'pa_widthx'): begin		; CME specific <<<<<<<<<<
      message,'CME list',/info
      min_wid = 90
      if keyword_set(pawidth_min) then min_wid = pawidth_min
      print,'Limiting events plotted to PA Width >= ',min_wid
      qv = where(events.pa_width ge min_wid, nqv)
    end 

  have_tag(events,'vx'): begin				; CME specific <<<<<<<<<<
      message,'CME list',/info
      min_vel = 300
      if keyword_set(velocity_min) then min_vel = velocity_min
      print,'Limiting events plotted to Velocity >= ',min_vel
      qv = where(events.v ge min_vel, nqv)
    end 

;    If a flare list, only plot where NAR matches value
  have_tag(events,'narx'): begin				; flare specific <<<<<<<<<<
      message,'Flare list',/info
      all_nars = all_vals(events.nar)
      qn = where(all_nars gt 10000,nqn)
      if nqn gt 0 then print, all_nars(qn)
      
      if keyword_set(nar) then begin
        qv = where(events.nar eq nar, nqv)
        print,'Limiting events plotted to NAR ',nar, nqv
      endif
    end

;    If a flare list, by default only plot where x-ray class > M1.0
  have_tag(events,'zxray_class'): begin		; flare specific <<<<<<<<<<
      message,'Flare list',/info
      min_xrc = 'M1.0'
      if keyword_set(xrayclass_min) then min_xrc = strupcase(xrayclass_min)
      print,'Limiting events plotted to X-ray class >= ',min_xrc
      qv = where(events.xray_class ge min_xrc, nqv)
    end

;    If a flare list, show events abov certain X-ray clas or in specofoc NAR

  have_tag(events,'xray_class'): begin		; flare specific <<<<<<<<<<
      message,'Flare list',/info
      min_xrc = 'M1.0'
      all_nars = all_vals(events.nar)
      qn = where(all_nars gt 10000,nqn)
      if nqn gt 0 then print, all_nars(qn), format='(8i8)'
               
      ctemp = intarr(nev) + 1
      if keyword_set(xrayclass_min) then begin
        min_xrc = strupcase(xrayclass_min)
        qv = where(events.xray_class ge min_xrc, nqv)
        print,'Limiting events plotted to X-ray class >= ',min_xrc, nqv, format='(a,a,t60,i3)'
        ctemp = intarr(nev) 
        if nqv gt 0 then ctemp(qv) = 1 
      endif 		;else $
      rtemp = rtemp and ctemp
      
      ctemp = intarr(nev) + 1
      if keyword_set(nar) or self.nar_used gt 0 then begin
        if keyword_set(use_nar) then rqnar = self.nar_used
        if keyword_set(nar) then rqnar = nar
        qv = where(events.nar eq rqnar, nqv)
        print,'Limiting events plotted to NAR = ', strtrim(string(rqnar),2), nqv, format='(a,a,t60,i3)'
        ctemp = intarr(nev) 
        if nqv gt 0 then ctemp(qv) = 1		;else ctemp++
      endif
      rtemp = rtemp and ctemp
      
      ctemp = intarr(nev) + 1
      if keyword_set(longhg_range) then begin
        if n_elements(longhg_range) eq 2 then begin
          qv = where(events.long_hg ge longhg_range(0) and events.long_hg le longhg_range(1), nqv)
          print,'Limiiing events plotted by LONGHG_RANGE:', longhg_range, nqv, format='(a,2i5,t60,i3)'
          ctemp = intarr(nev) 
          if nqv gt 0 then ctemp(qv) = 1 
        endif
      endif 
      rtemp = rtemp and ctemp
      
      qv = where(rtemp gt 0, nqv)
    end

  else: qv = indgen(nqv)

  endcase
  help, qv, nqv

;    note - should pick up scaling from the plot
  if nqv ge 1 then begin
     is_selected = 0
     if self.eventlist_row ge 0 then is_selected = 1

;    write out the name of the current catalogue
     xyouts, 0.01,0.98,/norm, self.eventlist_name, charsiz=1.1

;    if all keyword set, plot all events
    if keyword_set(all) or (is_selected eq 0) then begin
;      if is_selected eq 0 then box_message,['No event selected - default to all']
      if is_selected eq 0 then message,'No event selected - default to all',/info
      help,qv
      for j=0,nqv-1 do begin
        tt = events(qv(j)).time_start
        evcol = 100 & if keyword_set(color) then evcol=color
        outplot,[tt,tt],[0,1], color=evcol,  _extra=extra
      endfor
    endif

;    by default only plot the selected event
    if is_selected then begin
      tt = events(self.eventlist_row).time_start
      outplot,[tt,tt],[0,1], color=200
    endif

  endif else message,'No events in list',/info

  return
endif

;--  show event selected from currently loaded event list

;    check if an event has been selected, etc.

if self.eventlist_row eq -1 then begin
  message,'NO event has been selected, please select one!!',/info
  return
endif

;--  try to show web-based information about the event

if keyword_set(browser) then begin
  self -> browser,/show_event
  return
endif

;    show list name and selected event

cevent = (*self.eventlist_table)(self.eventlist_row)
print,''
print,'Current Event List:  ',self.eventlist_name
print,'Selected Event row No;: ',self.eventlist_row
print,'Selected Event details:'
help,/st, cevent
print,''

end

;----------------------------------------------------------------

pro hio_setup::select_instruments, widget=widget, myfav=myfav, names=names, $
		verbose=verbose, pause=pause, full=full, force_newpat=force_newpat

;--  select list of required instruments

;    names         string array or csv string of obs/instrument names
;    widget        use a widget to select the instruments
;    myfav         select from lists of favourite instruments are stored in a file

;    full          passed to widget - WHY? 
;    force_newpat  force reload of the PAT table (useful if dpas has changed!)


;    load the allowed list of instruments from the PAT

if n_elements(*self.pat_oinst) eq 0 or keyword_set(force_newpat) then *self.pat_oinst = hio_get_dpaspat(/force)
allowed_oinst = *self.pat_oinst

help,allowed_oinst										; filter for time??

case 1 of

;    make the selection using a widget based on info from the ICS
;    normally filters return using dpas_pat table, /full returns all

keyword_set(widget): begin
    if n_elements(*self.timewindow) eq 0 then begin
      message = [' ***   Time window NOT selected   *** ', $
                 '   Please select one and try again']
      box_message, message
      return
    endif
    times = *self.timewindow
    oinst = hio_ics_widget(timer=times, pause=pause, verbose=verbose, full=full)
;    help,oinst
    if strlen(oinst(0)) eq 0 then return
    
    *self.obs_inst = oinst
;    return, oinst
  end

;    use the favourite list of instruments a user has defined

keyword_set(myfav): begin
    oinst = rd_myfav_obsinst(verbose=verbose)			; time check??
    
    if keyword_set(verbose) then print, oinst
    if datatype(oinst) ne 'STR' then begin
      message,'NO instruments returned',/info
      return
    endif

;    check that the instruments are valid
    noinst = n_elements(oinst)
    valid = intarr(noinst)
    for j=0,noinst-1 do begin
      qv = where(strpos(allowed_oinst, oinst(j)) ge 0, nqv)
      if keyword_set(verbose) then print,'%'+oinst(j)+'%', qv, format='(a,t30,i3)'
      if nqv eq 0 then message,'>>>>>>>>>> BAD oinst value: '+oinst(j), /info $
        else valid(j)=1
    endfor
    oinst = oinst(where(valid gt 0))
;    print, oinst
;    help,oinst
    *self.obs_inst = oinst
  end

;    use supplied list of names

keyword_set(names): begin
    oinst = strupcase(names)							; time check??
    if n_elements(oinst) eq 1 then if strpos(oinst,',') gt 0 then oinst = str2arr(str_replace(oinst,' ',''))
    if keyword_set(verbose) then print, oinst
    if datatype(oinst) ne 'STR' then begin
      message,'NO instruments returned',/info
      return
    endif

;    check that the instruments are valid
    noinst = n_elements(oinst)
    valid = intarr(noinst)
    for j=0,noinst-1 do begin
      qv = where(strpos(allowed_oinst, oinst(j)) ge 0, nqv)
      if keyword_set(verbose) then print,'%'+oinst(j)+'%', qv
      if nqv eq 0 then message,'>>>>>>>>>> BAD oinst value: '+oinst(j), /info $
        else valid(j)=1
    endfor
    oinst = oinst(where(valid gt 0))
;    print, oinst
;    help,oinst
    *self.obs_inst = oinst
  end

;    default to choosing from the complete list of instrument (based on the PAT)

else: begin
    sel = hio_xmenu_sel(allowed_oinst,size=14, $
		title='HELIO Data Provider Access Service', $
		banner='Select desired set of instruments')

    if sel(0) ge 0 then begin
      oinst = allowed_oinst(sel)
;      help,oinst
      *self.obs_inst = oinst
    endif else $
;    default to previous selection (if there is one)
	message,'No Instruments Selected',/info
;    return, allowed_oinst(sel)
  end

endcase

;    looks better if name uses - and / instead of _ and __ 
coinst = *self.obs_inst
coinst = str_replace(coinst,'__','/')
coinst = str_replace(coinst,'_','-')

box_message, ['Selected Instruments:  ', ' ' + coinst]

return

end

;----------------------------------------------------------------

function hio_setup::get_filelist, verbose=verbose

;--  use the DPAS to search for data based on the selected times and instruments

common dpas_req, ssroot

;    check whether everything has been set up for the search

allok = 1
if n_elements(*self.timewindow) eq 0 or self.flag_new_etimes eq 1 then begin
  message = [' ***   Time window NOT selected   *** ', $
                 '   Please select one and try again']
  box_message, message
  allok = 0
endif else times = *self.timewindow

if n_elements(*self.obs_inst) eq 0 then begin
  box_message,'*** NO Instruments selected ***'
  allok = 0
endif else oinst = *self.obs_inst

;    only proceed if have both sets of information

if not allok then begin
  box_message,['','Please complete your selection and resubmit request','']
  return, ''
endif

;    make the request to the DPAS

filelist = hio_dpas_request(timer=times, req=oinst, rep=rep, verbose=verbose)		;, /nofix)

if datatype(filelist) ne 'STC' then begin
  box_message,['','>>>>  NO Data was returned by the DPAS  <<<<','']
  return, -1
endif

ssroot = 'hio-' + time2fid(reltime(/now),/time)		; name to use for a save set

;    save information about the request

temp = concat_dir(get_temp_dir(),'dpas')
if not is_dir(temp) then mk_dir,temp
ff = concat_dir(temp, ssroot + '_dpas-request.txt')

file_append, ff, /new, '# Request made to the DPAS on ' + systime()
file_append, ff, arr2str(times, ',')
file_append, ff, arr2str(oinst, ',')
file_append, ff, 'No. records returned:' + string(n_elements(filelist))

;    store the stucture associacied with the DPAS results before making any corrections

*self.filelist = filelist

self.flag_new_flist = 1					; set new filelist flag (used by time select)


;    create a save set for safety

if !hio_sysvar.save_dpasvot then self -> save_filelist


;    apply standard repairs, etc. to the filelist structure

hio_repair_filelist, filelist, debug=debug, /domain, /egso_time, $
						/fix_gongha, /split_aia, /split_sxi

return, filelist

end

;----------------------------------------------------------------

function hio_setup::recover_filelist, repair=repair, debug=debug, _EXTRA = ex

;--  retrieve the filelist structure from the object and optionally repair

files = *self.filelist		; retrieve structure

;    note that the stored filelist structure does not have any corrections applied
;    these can be selectively applied

if keyword_set(repair) then begin
  message,'Apply corrections',/info
  
;    if any keywords are set they are what will be passed, otherwise set defaults
  
  if n_elements(ex) gt 0 then begin
;    help,ex
    extra = ex
  endif else begin
    print,'Setting Defaults'				; extra passed as a structure
    extra = {domain:1, fix_gongha:1, split_aia:1}
  endelse
  if n_elements(extra) gt 0 then help, extra
  
  hio_repair_filelist, files, debug=debug, _STRICT_EXTRA = extra

endif else message,'NO corrections applied',/info

; fix_gongha=fix_gongha, domain=domain

return, files

end

;----------------------------------------------------------------

pro hio_setup::save_filelist, outfile=outfile

;--  save the filelist as an IDL save set

common dpas_req, ssroot

filename = ssroot + '_dpas.sav'
if keyword_set(outfile) then filename = outfile

box_message, '  Saving FILELIST structure to: '+filename+'  '

filelist = *self.filelist
save_ssroot = ssroot
help, filelist, save_ssroot

save, filelist, save_ssroot, filename=filename

end

;----------------------------------------------------------------

pro hio_setup::restore_filelist, filename=filename 

;--  restore the filelist from an IDL save set

common dpas_req, ssroot

restore, filename
help, filelist, save_ssroot

if n_elements(filelist) gt 0 then begin
  if datatype(filelist) eq 'STC' then begin
;    more chwcks????
  *self.filelist = filelist
  self.flag_new_flist = 1					; set new filelist flag (used by time select)
  
  ssroot = str_replace(filename, '_dpas.sav' ,'')
  if n_elements(save_ssroot) gt 0 then ssroot = save_ssroot

  box_message,['','  FILELIST structure restored from: '+filename+'  ', $
                  '      *** Note that no corrections have been applied ***','']
  endif
endif

end

;----------------------------------------------------------------

pro hio_setup::plot_filelist, filelist=filelist, goes=goes, data=data, verbose=verbose

;--  time plot of returned list of files with separate line per instrument
;    optionally plat the GOES light-curve

files = *self.filelist		; retrieve structure
if keyword_set(filelist) then files=filelist

;    default to limits of timewindow unless /data keyword set
times = *self.timewindow
print,'* Limits defined by timewindow:', anytim(times, /ecs,/trunc), format='(a,t34,a,2x,a)'

if keyword_set(data) then times=''

clearplot
hio_plot_filelist, files, goes=goes, /invert, timer=times, debug=verbose

end

;----------------------------------------------------------------

function hio_setup::select_filelist, rq_filelist, $
    timerange=timerange, $
    select_time=select_time, goes=goes, $		;sta=sta, stb=stb, $
	cadence=cadence, rate_perhour=rate_perhour, $
	plot_gongha=plot_gongha, $
;    apply=apply, $
    nofilter=nofilter, $
	debug=debug, force=force
	
;    30-sep-2014  rdb  added /nofilter option

;--  do plot and establish filters to select which file will be copied

;    check a sensible list of files has been supplied

if n_params() ne 1 then begin
  message,'NO filelist parameter supplied',/info
  return, -1
endif

if datatype(rq_filelist) ne 'STC' then begin
  help,filelist
  message,'Supplied filelist parameter is not a structure',/info
  return, -1
endif


filelist = rq_filelist

;    warning not filtered??

;    do we need the more generic filter routine of "links"
;    also allows selection of instruments

;   use /force to reselect?

;>>>>>>>>>>>  need to reset window if reload data  <<<<<<<<<<<<<

if n_elements(*self.flist_timewindow) eq 0 or self.flag_new_flist  $
		or keyword_set(force) then begin

  self -> plot_filelist, filelist=filelist, verbose=debug, goes=goes
  
;  if keyword_set(select_time) then $
    times = utcur_times()						; /select?????
  help,times
  fmt_timer, times
  self.flag_new_flist = 0						; reset new filelist flag
  
;    save the time window in the object

  *(self.flist_timewindow) = times

endif else begin

  box_message, 'Time window for filelist already set; use /force to redefine'
  times = *(self.flist_timewindow)
;;  outplot,[times(0),times(0)],[0,1]			; times from base?
;;  outplot,[times(1),times(1)],[0,1]

endelse

;    keyword /nofilter allows use of time selection only

if keyword_set(nofilter) then begin
  message, 'Returning with time selection but without applying any filters',/info
  alltt = anytim2tai(filelist.time_start)
  treq = anytim2tai(times)
  qv = where(alltt ge treq(0) and alltt le treq(1), nqv)
  if nqv gt 0 then idx = qv else idx = indgen(n_elements(filelist))
  print, 'Selected files:', nqv, n_elements(filelist), format='(a,i4,1h/,i5)'
  return, idx									; set index to all files
endif

;    apply filters accoring to keywords

;    timerange ?????

  rfile_struct = hio_filter_filelist(filelist, times, debug=debug, /fold, $
  			/cadence, rate_perhour=rate_perhour, plot_gongha=plot_gongha)
  help, rfile_struct
  if datatype(rfile_struct) ne 'STC' then begin
    message,'NO selection made',/info
    return, -1
  endif
  rfiles = rfile_struct.folded
  help,rfiles

  print,''
  box_message,'Summary of return from DPAS'
  hio_summ_filelist, filelist

  box_message,'Summary of return after filter is applied'
  hio_summ_filelist, filelist(rfiles)
  
;    return is a struct with the index of the file to be retrieved
;    should this be saved??

*(self.flist_filters) = rfile_struct

return, rfiles

end

;----------------------------------------------------------------

pro hio_setup::summ_filelist, rq_filelist, plot=plot, make_ack=make_ack, $
			_extra=_extra			; /goes under _extra

;--  summarizes what selected in supplied ilelist structure

hio_summ_filelist, rq_filelist, make_ack=make_ack

;    optionally plots the fileist times (with GOES)

if keyword_set(plot) then self->plot_filelist, filelist=rq_filelist, _extra=_extra

end

;----------------------------------------------------------------

;;pro hio_setup::download_filelist, rq_filelist, select=select, $
;;	outdir=outdir, debug=debug	;, compact=compact

pro hio_setup::download_filelist, rq_filelist, debug=debug, $
	outdir=outdir, compact=compact, form_copylist=form_copylist

;--  copy files specified in filelist structure

;    check a sensible list of files has been supplied

if n_params() ne 1 then begin
  message,'NO filelist parameter supplied',/info
  return
endif

if datatype(rq_filelist) ne 'STC' then begin
  help,filelist
  message,'Supplied filelist parameter is not a structure',/info
  return
endif

filelist = rq_filelist

;    summary of number of files per instrument

;;box_message,'Summary of return after time-selection filter'
box_message,'Summary of supplied file list'
hio_summ_filelist, filelist, short=short

;>>  check to see if filer was applied - no. of elements
;    optionally not continue

;    now go out and request selected files

;;help, self.flag_compact_dir
rcompact = self.flag_compact_dir
if keyword_set(compact) then rcompact = 1
help, rcompact

hio_retrieve_filelist, filelist, debug=debug, $		;, max_copy=max_copy, $
		outdir=outdir, compact=compact, form_copylist=form_copylist

end

;----------------------------------------------------------------

pro hio_setup::save_struct, outfile=outfile, debug=debug, $
	structure=structure, $			; equv. to help,/st,struct
	idlsave=idlsave, $				; save as IDL save set
	html=html, show=show, $			; write Web page and display
	csv=csv, $						; save as CSV file
	json=json, $					; save as JSON file
;	votable=votable, text=text, $	; not yet implemented ******
	help=help, $					; displays the help information

;	timerange=timerange, $
	eventlist=eventlist, $
	filelist=filelist, $
;	instruments=instruments, $
	delays=delays, $
	base_event=base_event, $
	hec_tabinfo=hec_tabinfo

if keyword_set(help) then begin
  help_info = [ $ 
    'Help information for the "-> save_struct" method', $
	'', $
	'  Options to select which structure to save:', $
    '/hec_tabinfo    Details of Event Lists availabile in the HEC', $
    '/eventlist      Section of the Event List loaded from the HEC', $
    '/filelist       List of file URLs returned by the DPAS', $
    '/base_event     Description of the Base Event used in the Propagation code', $
    '/delays         Delays for each location returned by the Propagation code', $
	'', $
	'  Different ways to save selected structure (to current directory):', $
	'/html           Saves as a Web page; /show launches the browser', $
	'/idlsave        Saves as an IDL save file  (/filelist only!!)', $
	'/csv            Saves as a CSV file', $
	'/json           Saves as a JSON file', $
	'/votable        Not implemented yet ***', $
;	'/text           Not implemented yet ***', $
	'', $
	'  Provide information about the structure', $
	'/debug          Shows the format of the structure (equv. to help,/st,struct)', $
	'/structure      Shows the format of the structure (equv. to help,/st,struct)']
  box_message,help_info
  return
endif

;--  Return a selected parameter or structure

struct = self -> get_param(filelist=filelist, $
	delays=delays, base_event=base_event, $
	hec_tabinfo=hec_tabinfo, eventlist=eventlist, name=param)

if datatype(struct) ne 'STC' then begin
  message,'ERROR of some kind',/info
  return
endif

if keyword_set(debug) then begin
  message,'Sturcture content: '+param,/info
  help,/st, struct
endif

;????????  should there be a return at the end of each case?

trailer=''
case 1 of
  keyword_set(html): begin
    swap=0
    if keyword_set(delays) then begin
      swap=1
      bstruct = self -> get_param(/base_event)
      trailer = '<b>Base Time: '+ strmid(bstruct.time,0,19) + '</b>
    endif
    if keyword_set(base_event) then swap=1
;    if keyword_set(hec_tabinfo) then begin
;      tags = strlowcase(tag_names(struct))
;      ptags = where(strpos(tags,'time') eq 0)
;      for kpt=0,n_elements(ptags)-1 do struct.(ptags(kpt))=strmid(struct.(ptags(kpt)),0,10)
;    endif

    fname = param+'.html'
    if keyword_set(outfile) then fname = outfile
    message,'Creating HTML file: '+fname,/info
    if keyword_set(delays) then begin
      cwid=[20,100,100,300,100,100,100,100,100,100]
      ccwid = cwid(0:n_elements(struct))		; allow for RH column
	endif else ccwid=0
    hio_stc2html, struct, swap=swap, cwid=ccwid, $
      file=fname, title='HELIO Structure: '+param, trailer=trailer

    cd,curr=croot
    url = concat_dir(croot,fname)
    if keyword_set(show) then hio_launch_browser,use_url=url
  end

;    name of structure is lost by the way that it is recovoured

  keyword_set(idlsave): begin
    message,'Creating IDL saveset file',/info
    fname = 'idlsave.sav'
    if keyword_set(filelist) then begin
      fname='DPAS_resp_idlsave.dat'		; should this be unique by including time?
      print,'>>>> Saving filelist to IDL saveset: ',fname
      filelist = struct
      save, filelist, file=fname, description='Structure "filelist" decoded from DPAS VOTable'
    endif else begin
      print,'>>>> Saving filelist to IDL saveset: ',fname
      save, struct, file=fname, description='hio_setup structure '+param
      print,'>>>> save NOT yet implemented'
    endelse
  end

  keyword_set(votable): begin
    message,'Creating VOTable file',/info
    print,'>>>> NOT yet implemented'
  end

  keyword_set(csv): begin
;    print,'>>>> save NOT yet implemented'
    message,'Creating CSV file of structure: '+param,/info
    fname = param+'_struct.csv'
    if keyword_set(outfile) then fname=outfile
    struct2csv, struct, file=fname
  end

  keyword_set(json): begin
;    print,'>>>> save NOT yet implemented'
    message,'Creating JSON file of structure: '+param,/info
    fname = param+'_struct.json'
    if keyword_set(outfile) then fname=outfile
    json = JSON_SERIALIZE(struct)
    file_append, fname, json, /new
  end

  keyword_set(text): begin
    print,'>>>> save NOT yet implemented'
  end

;     same as /debug but uses output of the help/st command
  keyword_set(structure): begin
    message,'Listing structure parameters: '+param,/info
    help,/st,struct,out=temp
    print,temp,format='(a)'
;     should this be save to a file?
  end

  else:  message,'No save option supplied',/info
endcase   

end

;----------------------------------------------------------------

;    returns structure containing the object

pro hio_setup::getProperty, all=all
  if (arg_present(all)) then begin
    all = create_struct(name=obj_class(self))
    struct_assign, self, all
  endif
end

;----------------------------------------------------------------

function hio_setup::get_param, name=name, $
	timerange=timerange, $					; the base time range of interest
	timewindow=timewindow, $				; time window established for search
	twind_zoom=twind_zoom, $				; offsets on timewindow, if set
    cxsimage_roi=cxsimage_roi, $
	eventlist=eventlist, $
	filelist=filelist, instruments=instruments, $
	delays=delays, base_event=base_event, $
	hec_tabinfo=hec_tabinfo, pat_oinst=pat_oinst

;--  Return a selected parameter or structure

;??    ought to test that these have been set...  !!!!!!
;??  oinst

case 1 of
  keyword_set(timerange): begin
    param = 'timerange'
    if n_elements(*self.timerange) eq 0 then goto, err
    out = *self.timerange
  end
  keyword_set(timewindow): begin
    param = 'timewindow'
    if n_elements(*self.timewindow) eq 0 then goto, err
    out = *self.timewindow
  end
  keyword_set(twind_zoom): begin
    param = 'twind_zoom'
    if n_elements(self.twind_zoom) eq 0 then goto, err
    out = self.twind_zoom
  end
  keyword_set(filelist):  begin
    param = 'filelist'
    if n_elements(*self.filelist) eq 0 then goto, err
    out = *self.filelist
  end
  keyword_set(eventlist):  begin
    param = 'eventlist'
    if n_elements(*self.eventlist_table) eq 0 then goto, err
    out = *self.eventlist_table
  end
  keyword_set(instruments): begin
    param = 'instruments'
    if n_elements(*self.obs_inst) eq 0 then goto, err
    out = *self.obs_inst
  end
  keyword_set(pat_oinst): begin
    param = 'pat_oinst'
    if n_elements(*self.pat_oinst) eq 0 then goto, err
    out = *self.pat_oinst
  end
  keyword_set(delays): begin
    param = 'delays'
    if n_elements(*self.prop_delays) eq 0 then goto, err
    out = *self.prop_delays
  end
  keyword_set(base_event): begin
    param = 'base_event'
    if n_elements(*self.prop_base_event) eq 0 then goto, err
    out = *self.prop_base_event
  end
  keyword_set(hec_tabinfo): begin
    param = 'hec_tabinfo'
    if n_elements(*self.hec_tabinfo) eq 0 then goto, err
    out = *self.hec_tabinfo
  end
  keyword_set(cxsimage_roi): begin
    param = 'cxsimage_roi'
    if n_elements(self.cxsimage_roi) eq 0 then goto, err
    out = self.cxsimage_roi
  end
  else: begin
      message,'No parameter selected',/info
      return, -1
    end
endcase

message,'Return selected parameter:  '+param, /info
name = param
return, out

err:
message,'Structure NOT defined:  '+param,/info
return, -1

end

;----------------------------------------------------------------

pro hio_setup::set_param, debug=debug, $
    timerange=timerange, $				;??????? disable this?
	timewindow=timewindow, $
	eventlist=eventlist, $
	filelist=filelist, instruments=instruments, $
	delays=delays, base_event=base_event, $
	compact_dir=compact_dir, nocompact_dir=nocompact_dir, $
	white_bkg=white_bkg, black_bkg=black_bkg

;--  Set a selected parameter or structure

;??    ought to test that these have been set...
;?? debug

case 1 of
  keyword_set(timerange): begin
      *self.timerange = timerange
      param = 'timerange'
    end
  keyword_set(timewindow): begin
      *self.timewindow = timewindow
      param = 'timewindow'
    end
  keyword_set(filelist): begin
      *self.filelist = filelist
      param = 'filelist'
    end
  keyword_set(eventlist):  begin
      *self.eventlist_table = eventlist
      param = 'eventlist'
  end
  keyword_set(instruments): begin
      *self.obs_inst = strupcase(instruments)
      param = 'instruments'
    end
  keyword_set(delays): begin
      *self.prop_delays = delays
      param = 'delays'
    end
  keyword_set(base_event): begin
      *self.prop_base_event = base_event
      param = 'base_event'
    end
  keyword_set(debug): begin
      self.debug = debug
      param = 'debug'
    end
  keyword_set(compact_dir): begin
      self.flag_compact_dir = 1
      param = 'compact_dir'
    end
  keyword_set(nocompact_dir): begin
      self.flag_compact_dir = 0
      param = 'nocompact_dir'
    end
  keyword_set(white_bkg): begin
      self.bkg_invert = 1
      param = 'white_bkg'
    end
  keyword_set(black_bkg): begin
      self.bkg_invert = 0
      param = 'black_bkg'
    end
  else: begin
      message,'No parameter selected',/info
      return
    end
endcase

message,'Set selected parameter: '+param, /info

return
end

;----------------------------------------------------------------

pro hio_setup__define, notest=notest

  void={hio_setup, $
	timerange: ptr_new(), $				; basetime interval 
	timewindow: ptr_new(), $			; time window for selection
	twind_zoom: intarr(2), $			; values set in zoom to shift timewindow

	flag_new_etimes: 0, $				; set if time range changed and
										; reset by first event load

	flag_plot_done: 0, $				; plot has been done
	flag_plot_otype: '', $				; indication of obs. type of data used in plot
										; 0=none, 1=remote, 2=in-situ
	timeseries_data: '', $				; goes_sxr, goes_proton, ace_swind
;??? should this be more generic - plot, or event?

	flag_basetime_mode: '', $			; method used to define basetime interval
	flag_select_mode: '', $				; way time range derived
										; 0=base time, 1=event-list, 2=light-curve
										
	nar_used:0, $						; NOAA Active Region (NAR) used to set basetime
	plot_longbasetime:0, $				; by default ask about long base times when plotting
									
	cxsimage_roi: fltarr(4), $			; ROI determined using cursors
	;roi_coords: fltarr(4), $			; ROI coordinates from image using cursors
										; or from selected event
	roi_info:  ptr_new(), $
	flag_roi_mode: '', $				; 0=none, 1=event, 2=cxs cursor

	hec_tabinfo: ptr_new(), $			; Table describing lists in the HEC
	hec_tab_row: 0, $					; row in table describing loaded event list
	eventlist_name: '', $				; name of loaded event list
	
	eventlist_table: ptr_new(), $		; loaded event list table
	eventlist_row: 0, $					; selected row in event list table

	prop_base_event: ptr_new(), $		; Base event for the propagation model
	prop_delays: ptr_new(), $			; Table of propagation delays

	pat_oinst: ptr_new(), $				; Instruments loaded from the PAT
	obs_inst: ptr_new(), $				; List of selected instruments
	
	cxs_return: ptr_new(), $			; structure with URLs to copies of CXS products 
	flag_new_cxs:0, $					; if set, new data loded or CXS not executed

	filelist: ptr_new(), $				; Filelist sructure returned by the DPAS
	flist_timewindow: ptr_new(), $		; Time window selected within filelist
	flag_new_flist: 0, $				; set if new filist structure loaded
										; reset when new filelist time window set
	flist_filters: ptr_new(), $			; structure containg the filters

	flag_compact_dir: 0, $				; if set, store downloaded data 
										; with single directory root
	
	bkg_invert: 0, $					; plot on white background if set

	debug: 0}

  return
end
