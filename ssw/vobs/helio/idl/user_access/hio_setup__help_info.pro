pro hio_setup::help_info, save_struct=save_struct, select_instrument=select_instrument

case 1 of

keyword_set(save_struct): begin
  help_info = [ $ 
    'Help information for the "-> save_struct" method', $
	'', $
	'  Options to select which structure to save:', $
    '/hec_tabinfo    Details of Event Lists availabile in the HEC', $
    '/eventlist      Section of the Event List loaded from the HEC', $
    '/filelist       List of file URLs returned by the DPAS', $
    '/base_event     Description of the Base Event used in the Propagation code', $
    '/delays         Delays for each location returned by the Propagation code', $
	'', $
	'  Different ways to save selected structure (to current directory):', $
	'/html           Saves as a Web page; /show launches the browser', $
	'/idlsave        Saves as an IDL save file  (/filelist only!!)', $
	'/csv            Saves as a CSV file', $
	'/json           Saves as a JSON file', $
	'/votable        Not implemented yet ***', $
;	'/text           Not implemented yet ***', $
	'', $
	'  Provide information about the structure', $
	'/debug          Shows the format of the structure (equv. to help,/st,struct)', $
	'/structure      Shows the format of the structure (equv. to help,/st,struct)']
  box_message, help_info
 end

keyword_set(select_instrument): begin
  help_info = [ $ 
    'Help information for the "-> select_instruments" method', $
	'', $
	'                If no option select, displays list of available instruments', $
    '/widget         Display widget based on the ICS service database', $
    '/myfav          Select options defined in user favourites file ???', $
    'names=names     Use supplied comma separated string of instrument names ???', $
	'', $
    '/full           Do not filter according to the DPAS PAT' ]
  box_message, help_info
 end
		
else:	message,'Nothing defined for this option',/info
endcase

end
