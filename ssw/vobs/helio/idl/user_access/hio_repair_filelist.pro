;+
; NAME:
;	    HIO_REPAIR_FILELIST
; PURPOSE:
;	    Fix inconsistencies in names of observatories, instruments and observing 
;       domains. Fix some errors in the duration, remove bad time records
;       Split provider returns where several instruments/channels or sites have
;       been grouped under a single obsinst_key
;
; CALLING SEQUENCE:
;       hio_repair_filelist, file_list
;
; INPUTS
;       struct          List of Files returned by DPAS access routines
; KEYWORDS INPUTS:
;       debug           Provides extra info...
;
;       domain          Fix problems with observatory/obsinst_key and observing domain ??
;       fix_gongha      Call routine fix_gong_files to split the GONG/HALPH and /MAGMP
;                       records by observatory site (modifies group and groupmember)
;       split_aia       Modify the channel, observingdomian and instrument tags to 
;                       reflect the different wavelengths in the observations
;       split_hmi       Modify the channel tag to reflect the different SDO/HMI products
;       split_sxi       Modify the observatory tag to split the records by GOES mission
;       egso_time       Fix end times for instruments where not properly set
;
;       dpas            Seems to fix some HALPA and MAGxx issues  (obselete????)
;       egso_detail     Fix some observatory/instrument name issues  (obselete????)

; OUTPUTS:
;       struct          Modified input structure (list of files...)
;
;       bad time records saved to common
; RESTRICTIONS:
; HISTORY:
;           Oct 2005  rdb  Written
;           Oct 2006  rdb  added removal of records with bad time spec.
;        04-Mar-2007  rdb  Replace correction of observing domain with code using SSR
;                          General tidying and documenting
;
;        18-Jul-2013  rdb  HELIO version - based on egso_repair_filelist
;                          Lot of codes retained for reference by "commented out"
;        02-Aug-2014  rdb  Added splitting out of SDO/AIA channel
;        27-Jul-2016  rdb  Fixed issue of instrument not found in ICS - now ignores them!!!!
;        10-Aug-2016  rdb  Added splitting out of GOES/SXI mission
;                          Added test that supplied variable is a structure
;        15-Aug-2016  rdb  Added splitting out of SDO/HMI products
;                          Added group and group member for GOES/SXI
;        26-Mar-2017  rdb  Added ODY/GRS to time extension routines
;                          Enhanced the description of the keywords
;
;-

pro hio_repair_filelist, struct, debug=debug, $
		fix_gongha=fix_gongha, domain=domain, $
		split_aia=split_aia, split_hmi=split_hmi, split_sxi=split_sxi, $
		egso_detail=egso_detail, egso_time=egso_time, $
		dpas=dpas

common  repair_keep, bad_records

message,'>>>> Starting',/info

if keyword_set(verbose) then help,/st,struct
if datatype(struct) ne 'STC' then begin
  message,'>>>> Supplied variable is not a Structure',/info
  return
end

;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;++++    Code that fixes various issues with tags - e.g. domain 

if keyword_set(domain) then begin
message,'>>>> Starting Fixing domain, etc. stuff',/info

;    fix some instrument name problems

  qv = where(struct.observatory eq 'PROBA2', nqv)
  if nqv gt 0 then begin
    struct(qv).observatory = 'PROBA_2'
    struct(qv).obsinst_key = struct(qv).observatory + '__' + struct(qv).instrument
  endif

;    fix contents of domains, etc.

  obsinst = all_vals(struct.obsinst_key)
;  print,obsinst
  ics_struct = query_ics_oinst(obsinst, /quiet)
  if keyword_set(debug) or !hio_sysvar.debug eq 1 then help,ics_struct,/st
  if datatype(ics_struct) ne 'STC' then begin
    message,'**** ICS did not return info on instrument: '+obsinst,/cont
    goto, xxcc			;?????
  endif
  
;;  for j=0,n_elements(obsinst)-1 do begin
  for j=0,n_elements(ics_struct)-1 do begin
  
    qv = where(struct.obsinst_key eq ics_struct(j).OBSINST_KEY, nqv)
    print, ics_struct(j).OBSINST_KEY, nqv, format='(a,t26,i5)'
    if nqv gt 0 then begin
    
      struct(qv).observingdomain = ics_struct(j).INST_OE2
      
      if ics_struct(j).OBSINST_KEY eq 'GONG__HALPH' then begin
        struct(qv).provider = 'NSO-GONG' 
;        struct(qv).group = 'GONG'
      endif
    
    endif
    

  endfor

;>>>>>>>>>
;>>>>>>>>>  should the pflag stuff be here rather than in hio_dpas_request?
;>>>>>>>>>

;;goto, xxcc

;    set flag if the mode duration exceeds threshold
;    flag used in plot routine to plot bar for observations with longer duration 

dur = long(addtime(struct.time_end, diff=struct.time_start)*60)		; secs
qdur = where(dur gt 300, nqdur)					; ???? how long - instrument dependant?
if nqdur gt 0 then struct(qdur).pflag = 1

;    JSOC returns a TAR set containing several images with duraions of few secs or less
;    duration shows interval covered by images - reset flag

qv = where(strpos(struct.obsinst_key,'SDO__AIA') eq 0, nqv)
if nqv gt 0 then struct(qv).pflag = 0
qv = where(strpos(struct.obsinst_key,'SDO__HMI') eq 0, nqv)
if nqv gt 0 then struct(qv).pflag = 0

xxcc:

endif					; /domain


;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;++++    Code that calculates GONG H-alpha image time offsets from local noon

if keyword_set(fix_gongha) then begin

qv = where(strpos(struct.obsinst_key,'GONG__') eq 0, nqv)
if nqv gt 0 then begin
  message,'>>>> Starting GONG Files routine; includes calculating time offsets',/info

  struct = fix_gong_files(struct, debug=debug)
endif

endif					; /fix_gongha


;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;++++    Split out the SDO/AIA channels

if keyword_set(split_aia) then begin

qv = where(strpos(struct.obsinst_key,'SDO__AIA') eq 0, nqv)
if nqv gt 0 then begin
  message,'>>>> Splitting out the SDO/AIA channels',/info
  print,'Total No. of SDO/AIA images: ',nqv
  
  for j=0,nqv-1 do begin
    channel = gt_brstr(struct(qv(j)).url,'record=','_')
    struct(qv(j)).channel = channel
  endfor
  
;    correct observing domain, instrument and obsinst_key
  qx = where(struct(qv).channel gt 1000, nqx)
  if nqx gt 0 then begin
    struct(qv(qx)).observingdomain = 'UV'
    struct(qv(qx)).instrument = 'AIA_UV'
  endif
  qx = where(struct(qv).channel lt 1000, nqx)
  if nqx gt 0 then begin
    struct(qv(qx)).observingdomain = 'EUV'
    struct(qv(qx)).instrument = 'AIA_EUV'
  endif
  struct(qv).obsinst_key = struct(qv).observatory + '__' + struct(qv).instrument

  aia_waves = all_vals(struct(qv).channel)
  aia_waves = aia_waves(sort(fix(aia_waves)))
  nwv = n_elements(aia_waves)
  kwaves = intarr(nwv)
  
  for j=0,nwv-1 do begin
    cwave = '=' + aia_waves(j) + '_'
    qw = where(strpos(struct(qv).url,cwave) gt 0, nqx)
    kwaves(j) = nqx
  endfor
  print,aia_waves, format='(15i6)'
  print,kwaves, format='(15i6)'
endif

endif					; /split_aia


;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;++++    Split out the SDO/HMI products

if keyword_set(split_hmi) then begin

qv = where(strpos(struct.obsinst_key,'SDO__HMI') eq 0, nqv)
if nqv gt 0 then begin
  message,'>>>> Splitting out the SDO/HMI products',/info
  print,'Total No. of SDO/HMI images: ',nqv
  
  for j=0,nqv-1 do begin
    channel = gt_brstr(struct(qv(j)).url,'hmi__',';')
    struct(qv(j)).channel = channel
  endfor

endif

endif					; /split_hmi


;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;++++    Split out the GOES/SXI missions

if keyword_set(split_sxi) then begin

qv = where(strpos(struct.obsinst_key,'GOES__SXI') eq 0, nqv)
help, qv
if nqv gt 0 then begin
  message,'>>>> Splitting out the GOES/SXI missions',/info
  print,'Total No. of GOES/SXI images: ',nqv
  
;  for j=0,nqv-1 do begin
;    channel = gt_brstr(struct(qv(j)).url,'record=','_')
;    struct(qv(j)).channel = channel
;  endfor

  break_file,struct(qv).url,aa,bb,cc,dd
  mission = strmid(cc,26,2)			; GOES mission no. 2 chars before the .fits
  new_obs = struct(qv).observatory+'_'+mission
  qx = where(strpos(struct(qv).observatory,'_') gt 0 , nqx)
  if keyword_set(debug) then help, qx
  if nqx eq 0 then struct(qv).observatory = new_obs		; don't add multiple times
  
  struct(qv).group = 'GOES'
  struct(qv).groupmember = struct(qv).observatory

endif

endif					; /split_sxi

;>>>>>>>>>
;>>>>>>>>>  should SOHO/EIT (and PROBA@/SWAP?) be split in the same fashion
;>>>>>>>>>  not clear where get information from - not in file/directory name
;>>>>>>>>>


;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;++++    Code that was part of EGSO routine

if keyword_set(egso_detail) then begin
message,'Starting ESGO detail routines',/info

goto, xxxx0

;-------------------------------------------------------------------------------
;    Some observatories given in long form or otherwise in a non-standard format
;    Identify and correct - makes life easier later on...
;    Eventually, need to make Broker and Provider use same list
  
message,'Correcting Obs/Inst names and ObsDomains',/info

if keyword_set(debug) then print,'Before: ',all_vals(struct.observatory)

obs = [['Meudon','MEUD'],['Nancay','NANC'],['Pic du Midi','PDMO'], $
       ['Coronas','Coronas-F'],['CORONAS_F','Coronas-F'], $
       ['ULYSSES','Ulysses'], $
       ['NOBEYAMA','NOBE'],['???','BBSO'], $
       ['GOES_12','GOES-12']]
;;       ['TRACE_EUV','TRACE-EUV'],['TRACE_UV','TRACE-UV'],['TRACE_VIS','TRACE-VIS']]

for j=0,(size(obs))(2)-1 do begin
  qv= where(struct.observatory eq obs(0,j))
  if qv(0) ge 0 then struct(qv).observatory=obs(1,j)
endfor

if keyword_set(debug) then print,'After: ',all_vals(struct.observatory)

xxxx0:

;-------------------------------------------------------------------------------
;    Standardize the instrument names and fix errors in the observing domain
;    Eventually we would like to fix at the Provider

if keyword_set(debug) then begin
  print,''
  print,'Before: ',all_vals(struct.observatory+'/'+struct.instrument+' %'+struct.observingdomain),format='(a)'
endif

;    Correct records with both observatory and instrument in instrument field

wq = where(strpos(struct.instrument,'__') gt 0)
if wq(0) ge 0 then $
  for j=0,n_elements(wq)-1 do struct(wq(j)).instrument $
   = str_replace(struct(wq(j)).instrument, str_replace(strupcase(struct(wq(j)).observatory)+'__','-','_'), '')

;    replace "_" with "-" in instruments names

wq = where(strpos(struct.instrument,'_') gt 0)
if wq(0) ge 0 then struct(wq).instrument = str_replace(struct(wq).instrument,'_','-')

;    fold all Coronas-F SRT channels together?

wq = where(struct.observatory eq 'Coronas-F')
if wq(0) ge 0 then begin
  wx = where(strpos(struct(wq).instrument,'SRT') ge 0)
  if wx(0) ge 0 then for j=0,n_elements(wx)-1 do struct(wq(wx(j))).instrument = 'SRT'
  endif

;***** MEUD/MSH & NANC/NRH seem to return wavelength in observing domain - how to HANDLE?????

;    use EGSO SSR to repair the observing domain

;>>>>>>>>>>> ssr_inst = egso_ssr_search(filelist=struct)
if keyword_set(debug) then begin
  help,ssr_inst
  print,ssr_inst.observatory+'/'+ssr_inst.instrument
endif

obsinst = struct.observatory+'__'+struct.instrument
obsinst = strupcase(str_replace(obsinst,'-','_'))

for jinst=0,n_elements(ssr_inst)-1 do begin
  qv = where(obsinst eq ssr_inst(jinst).key)
  if qv(0) ge 0 then struct(qv).observingdomain = ssr_inst(jinst).oe2
;    should this also include oe1 ??  (=> photons/EUV)
endfor

if keyword_set(debug) then begin
  print,''
  print,'After: ',all_vals(struct.observatory+'/'+struct.instrument+' %'+struct.observingdomain),format='(a)'
endif

endif					; /egso_detail


;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;++++    Code that was part of EGSO routine

if keyword_set(egso_time) then begin
message,'Starting ESGO time routines',/info

;-------------------------------------------------------------------------------
;    Correct some of the times declared

message, 'Fixing time problems',/info
print, all_vals(struct.instrument)

;;;  correct GOES/SXI times
;;wq = where(struct.instrument eq 'SXI')
;;if wq(0) ge 0 then begin
;;  struct(wq).time_start = strmid(struct(wq).time_start,0,19)
;;  struct(wq).time_end = strmid(struct(wq).time_end,0,19)
;;
;;  endif

;    correct duration of in-situ data, assume 24 hours...

inst = ['EPAM', 'MAG', 'SWEPAM', 'EPAC', 'VHM_FGM', 'DIFOS', 'GRS_DHD']
for j=0,n_elements(inst)-1 do begin
  wq = where(struct.instrument eq inst(j), nwq)
  if nwq gt 0 then begin
    print,'>>>> Fix Duration: '+ inst(j), nwq, format='(a,t35,i6)'
    for k=0,nwq-1 do begin
      struct(wq(k)).time_end = anytim(addtime(anytim(struct(wq(k)).time_start,/ecs), delt=24*60), /ecs,/trunc)
      struct(wq(k)).pflag = 1
    endfor
  endif
endfor

wq = where(struct.instrument eq 'GRS_CGS', nwq)
if nwq gt 0 then begin
  print,'>>>> Fix Duration: GRS_CG', nwq, format='(a,t35,i6)'
  for k=0,nwq-1 do begin
    struct(wq(k)).time_end = anytim(addtime(anytim(struct(wq(k)).time_start,/ecs), delt=2*60), /ecs,/trunc)
    struct(wq(k)).pflag = 1
  endfor
endif

;    occasionally, no time information included - reject these records

qv = where(struct.time_start eq 'null' and struct.time_end eq 'null')
if qv(0) ge 0 then begin
  message,'>>>> Bad time-ranges',/info
  help,/st,struct(qv(0))

  print,'>>>> Rejecting:', fix(qv)
  print,'>>>> Obs/Inst: ', all_vals(struct(qv).observatory+'/'+struct(qv).instrument)
  bad_records = struct(qv)
  qvx = where(struct.time_start ne 'null' and struct.time_end ne 'null')
  struct = struct(qvx)

  message,'<<<<',/info
endif

endif					; /egso_time


;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;++++    Code that was within the DPAS get code

if keyword_set(dpas) then begin
message,'Starting DPAS routines',/info

    case (1) of 

    strpos(report(j).obsinst,'HALPH') gt 0: begin
      message,'+++++>>>>>> Check H-alpha observatory assignment ',/info
      print, 'REP: ',report(j).obsinst
      print, 'VOT: ',zz.instrument_name
      
      if all_vals(zz.provider) eq 'VSO:HANET' then begin
        print,all_vals(zz.provider_instrument)
;;        break_file,zz.url,aa,bb,cc,dd
;;        wgobs = all_vals(strmid(cc,0,5))
;;        print,'>>>>>>>> WOBS: ',wgobs
;;        obsinst = strupcase(strmid(cc,0,4))+'__HALPH'
;;        obsinst = str_replace(obsinst,'MEDN','MEUD')
        obsinst = zz.provider_instrument+'__HALPH'
      endif
;;      if max(strpos(zz.instrument_name,'__')) lt 0 then $
;;	obsinst = zz.instrument_name+'__HALPH
      obsinst = str_replace(obsinst,'OBSPM','MEUD')
;      print, obsinst

     end

    strpos(report(j).obsinst,'MAGxx') gt 0: begin
      message,'+++++>>>>>> Check MAG observatory assignment ',/info
;;      print,report(j).obsinst, zz.instrument_name
      print, 'REP: ',report(j).obsinst
      print, 'VOT: ',zz.instrument_name
      
      if all_vals(zz.provider) eq 'CDAWEB' then begin
        obsinst = replicate(report(j).obsinst, nel)
        gqv = where(strpos(zz.url,'goes') gt 0, ngqv)
        if ngqv gt 0 then obsinst(gqv) = 'GOES__MAG'
      endif
      
     end

    else: message,'+++++ Not a special case',/info
;;  obsinst = replicate(report(j).obsinst, nel)

    endcase
;;;    dpas_return(ktot:ktot+nel-1).obsinst_key = obsinst

endif					; /dpas

if keyword_set(debug) then help,/st, struct

message,'>>>> Ending',/info

end 
