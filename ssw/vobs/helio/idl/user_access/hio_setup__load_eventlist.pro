;+
; PROJECT:
;        HELIO
; NAME:
;        load_eventlist
; PURPOSE:
;        Loads an event list from the Heliophysics Event Catalogue (HEC)
;        If not already loaded, the hec_catalogue is loaded and augmented by -> load_hec_tabinfo
; CALLING SEQUENCE:
;        helio -> load_eventlist [,name=name] [,/choose] [,error=error]
; CATEGORY:
;        
; INPUTS:
;
; INPUT KEYWORDS:
;        name        if specified, load event list of this name
;        gevloc      load the gevloc_sxr_flare list, default is goes_sxr_flare
;
;        choose      if specified, allows user to choose event list from a widget and loads
;        cme         focus on CME lists in the widget (/choose)
;        flare       focus on flare lists in the widget (/choose)
;        particle    focus on particle lists in the widget (/choose)
;        swind       focus on solar wind lists in the widget (/choose)
;
;        saved_sql
;        force
;        verbose     prints additional information        
; OUTPUTS:
;        
; OUTPUT KEYWORDS:
;        error       null sting unless there is an error of some kind
; RESTRICTIONS:
;        
; HISTORY:
;        01-Apr-2011  rdb  Originally part of the manil hio_setup__define file
;           May-2014  rdb  Split into its own file; some routines embedded
;           Sep-2014  rdb  added the saved_sql keyword
;        24-Oct-2014  rdb  added error keyword to trap condition if list not loaded...
;
;-

;----------------------------------------------------------------

pro hio_setup::load_gev

;    kludge to get up-to-date event list

message,'Entering',/info

base_times = *(self.timerange)

get_gsfc_sswdb, base_times, /gev

rd_gev,base_times(0),base_times(1),gev
events = hio_conv_gev2hec(gev)

fmt_timer, events.time_start
help,events
help,/st,events

(*self.eventlist_table) = events

;    also need to set flag that list is loaded!!

self.eventlist_name = 'SSWRDB GEV'  ;hec_list

self.flag_new_etimes = 0			;reset flag

end

;----------------------------------------------------------------

pro hio_setup::load_gevloc

;    kludge to get up-to-date event list
;    GEVLOC only returns events data for the last 7 days

message,'Entering',/info

;; base_times = *(self.timerange)

events = get_lmsal_gevloc()

fmt_timer, events.time_start
help,events
help,/st,events

(*self.eventlist_table) = events

;    also need to set flag that list is loaded!!

self.eventlist_name = 'GEVLOC'  		;hec_list

self.flag_new_etimes = 0				;reset flag

end

;----------------------------------------------------------------

pro hio_setup::load_hec_tabinfo, save=save, verbose=verbose

;--  load the table in the HEC that describes the nature of the tables

query = hio_form_hec_query(/char)	; get HEC table characteristics
hec_tabinfo = ssw_hio_query(query, /conv, verbose=verbose)
;help,/st,hec_tabinfo

;    temporary fix!!!!
hec_tabinfo = fix_hectab(hec_tabinfo)
help,/st,hec_tabinfo

if datatype(hec_tabinfo) eq 'STC' then begin
  *self.hec_tabinfo = hec_tabinfo

;    if keyword set then save table as csv file

if keyword_set(save) then struct2csv, hec_tabinfo, file='hec_control_tab.csv'

goto, xxxx
  if keyword_set(save) then begin
    nrow = n_elements(hec_tabinfo)
    ntag = n_tags(hec_tabinfo)
    tags = strlowcase(tag_names(hec_tabinfo))
    cc = tags(0)
    for jtag=1,ntag-1 do cc=cc+','+strtrim(tags(jtag),2)
    file_append,'hec_control_tab.csv',/new,cc
    for jrow=0,nrow-1 do begin
      crow = hec_tabinfo(jrow)
      cc = crow.(0)
      for jtag=1,ntag-1 do cc=cc+','+strtrim(string(crow.(jtag)),2)
      if keyword_set(verbose) then print,cc
      file_append,'hec_control_tab.csv',cc
    endfor
  endif
xxxx:
  
endif

end

;----------------------------------------------------------------

pro hio_setup::load_eventlist, name=name, choose=choose, widget=widget, error=error, $
;;	remote_url=remote_url, 
	saved_sql=saved_sql, gevloc_flare=gevloc_flare, $
	search_string=search_string, $
	flare=flare, cme=cme, swind=swind, particle=particle, $
	solar=solar, ips=ips, geospace=geospace, planet=planet, $
;;	summ_nar=summ_nar, $
	force=force, verbose=verbose

if n_elements(*self.hec_tabinfo) eq 0 or keyword_set(force) then $
	self -> load_hec_tabinfo

;--  get specified event list for this time interval from HEC

if not keyword_set(saved_sql) then begin		; >>>>>>

if n_elements(*(self.timerange)) eq 0 then begin
  box_message,'Need to set a base time'
  self -> set_basetime, timerange=timerange
endif

base_times = *(self.timerange)
search_times = anytim(base_times,/ccs,/date)
;print,'Base Time:  ',search_times

endif											; >>>>>>

hec_list = ''
error = ''

case 1 of

;-->    Event list name is supplied

keyword_set(name): begin
  message,'Using supplied Event list',/info
  print,''

  hec_list = name
  help,hec_list

;????    check if the event list covers the required time intervals
  hec_tabinfo = *self.hec_tabinfo
  qv = where(hec_tabinfo.name eq hec_list, nqv)
  if nqv ne 1 then begin
    help,qv
    message,'List name not found',/info
    return
  endif
  qv = qv(0)
;  help, qv

  list_times = anytim([hec_tabinfo(qv).timefrom, hec_tabinfo(qv).timeto],/ccs,/date)
  print,'Base Time:  ',search_times
  print,'List Time:  ',list_times
  print,''

  if (search_times(0) le list_times(1)) and (search_times(1) ge list_times(0)) then $
    goto, getlist  else begin
      mess = 'NAMED List does not cover base time range'
      message,mess,/info
      error = mess
      return
    endelse
end

;-->    if /choose keyword set, use widget to decide which list to use

keyword_set(choose): begin
  message,'Choose Event list',/info

  hec_tabinfo = *self.hec_tabinfo

  temp = select_hec_listname(search_times, search_string=search_string, $
    use_tabinfo=hec_tabinfo, verbose=verbose, $
	flare=flare, cme=cme, swind=swind, particle=particle, $
	solar=solar, ips=ips, geospace=geospace, planet=planet)
  if datatype(temp) eq 'STR' then hec_list = temp

end

keyword_set(widget): begin
  message,'Choose Event list using a widget',/info

  hec_tabinfo = *self.hec_tabinfo

  temp = select_heclist_widget(timerange=search_times, search_string=search_string, $
    use_tabinfo=hec_tabinfo, verbose=verbose)
  if datatype(temp) eq 'STR' then hec_list = temp

end

;-->    if /choose keyword set, use widget to decide which list to use

keyword_set(saved_sql): begin
  print, ''
  message,'Load list using saved SQL',/info
  
  qq = rd_tfile('saved_hec_sql.txt')
  if qq(0) eq '' and n_elements(qq) eq 1 then begin
      message,'Error of some kind 1',/info
      return
  endif  
  
  sel = xmenu_sel(qq,/one)
  if sel lt 0 then begin
    message,'Error of some kind 2',/info
    return
  endif

  sql = qq(sel)
;;  print,sql
  query = hio_form_hec_query(sql=sql)
  print,query
  vv = ssw_hio_query(query,/conv)
  if datatype(vv) ne 'STC' then begin
    message,'Error of some kind 3'
    return
  endif
  
  help,vv,/st
  hec_list = 'unknown'
  times = vv.time_start
  fmt_timer, times, t0, t1

end

;-->    if /choose keyword set, use widget to decide which list to use

keyword_set(remote_url): begin
  print, ''
  message,'Load from supplied Remote URL',/info
  
  if datatype(remote_url) ne 'STR' then begin
    link=''
    read,'Please enter Remote URL: ',link
  endif else link = remote_url
  
  cmd = wget_stream() + link
  spawn, cmd, resp

  q0 = where(strpos(resp, 'sql> select') eq 0, nq0)
  q1 = where(strpos(resp, 'Elapsed time') eq 0, nq1)
  vot_text = resp
  if nq0 eq 1 and nq1 eq 1 then vot_text = resp(q0(0)+1:q1(0)-1)

  vv = decode_votable(vot_text,/quiet)
  help,vv & help,/st,vv
  
  hec_list = 'unknown'
  times = vv.time_start
  fmt_timer, times, t0, t1
  
end

;-->    by default, retrieve the GOES X-ray events; after 1-Jan-2006 allow gevloc_sxr_flare

else:  begin
    hec_list ='goes_sxr_flare'
    ftdiff = addtime(base_times(0),diff='1-jan-2006')/60/24.		; days
    help,ftdiff
    if keyword_set(gevloc_flare) and ftdiff gt 0 then hec_list = 'gevloc_sxr_flare'
    box_message,'Defaulting to:   '+hec_list
  end
endcase

print,''
help, hec_list
if hec_list eq '' then begin		; should not get here!!!!!
  message,'NO selection made - PROBLEM',/info
  return
endif

goto, bbbb

;???  times
if n_elements(*(self.timerange)) eq 0 then begin
  box_message,'Need to set a base time'
  self -> set_basetime, timerange=timerange
endif

help, *(self.timerange)
base_times = *(self.timerange)

print,''
print,'>>>> List validity check:'

search_times = anytim(base_times,/ccs,/date)
print,'Search: ',search_times

list_times = anytim([(*self.hec_tabinfo)(qv(sel)).timefrom, (*self.hec_tabinfo)(qv(sel)).timeto],/ccs,/date)
print,'List:   ',list_times

;    check if the event list covers the required time intervals

;>>>> check still needed when /name keyword used !!!!

bbbb:
getlist:
help, hec_list

;;if (search_times(0) le list_times(1)) and (search_times(1) ge list_times(0)) then begin

  query = hio_form_hec_query(timer=base_times, list=hec_list)	;, /short)

;;  resp = ssw_hio_query(query, /conv)
  vv = ssw_hio_query(query, verbose=verbose)
  vv = str_replace(vv,'double','float')		; kludge until INAF fixes problem
  resp = decode_votable(vv,/quiet)
;;  help,/st,resp & help,resp

;???  just return if no events in this interval
  if datatype(resp) ne 'STC' then begin
    box_message,'No events in defined time range!!'	;,/info
    self.eventlist_name = ''
    return
  endif

  lpnt = where((*self.hec_tabinfo).name eq hec_list)
  self.hec_tab_row = lpnt(0)			; row in table describing the list

  self.eventlist_name = hec_list		; save name of selected event list
  *(self.eventlist_table) = resp		; save structure containing event list

  self.flag_new_etimes = 0			;reset flag

  message,'Reset selected event number',/info
  self.eventlist_row = -1

;;endif else begin
;;
;;  box_message,['Event list does not cover this time range!!', '>>>>  '+hec_list]	;,/info
;;  return
;;
;;endelse

;--    make summary of no. of flare in each active region

;if keyword_set(summ_nar) then begin
  if have_tag(resp,'nar') then begin
    print,''
    message,'Flare list containing NARs:   '+hec_list, /info
    
    all_nars = all_vals(resp.nar)
    qn = where(all_nars gt 10000,nqn)
    
    if nqn gt 0 then begin
      print,'>> Summary of events in each NAR in loaded time interval:'
      all_nars = all_nars(qn)
;      print, all_nars, format='(8i8)'
      ktot = 0
      for j=0,nqn-1 do begin
        qf = where(resp.nar eq all_nars(j), nqf)
        ktot = ktot + nqf
        if nqf gt 0 then print, all_nars(j), nqf, format='(i10, i5)'
      endfor
      nresp = n_elements(resp)
      if ktot lt nresp then print, 'N/A', nresp-ktot, format='(a10, i5)'
    endif
    print,''
    
  endif
;endif

end
