;+
; PROJECT:
;        HELIO
; NAME:
;        multi_proton
; PURPOSE:
;        Produce 3 panel plot of STB, GOES and STA protons
;        Overplot with CMEs from cactus_all and proton events seen by GOES
; CALLING SEQUENCE:
;        helio -> multi_proton  [, rtimes]
; CATEGORY:
;        
; INPUTS:
;        rtimes      limits of plot if supplied; requests choice from vobs_presets if not
; INPUT KEYWORDS:
;        verbose     prints extra information
; OUTPUTS:
;        
; OUTPUT KEYWORDS:
;        
; RESTRICTIONS:
;        
; HISTORY:
;    01-Apr-2014  rdb  written
;    05-Oct-2014  rdb  added the HELIO logo call
;    20-Oct-2014  rdb  updated; added the xmargin keyword
;
;-

pro hio_setup::multi_proton, rtimes, verbose=verbose	;, select_times=select_times

self -> set_basetime, timerange=rtimes

times = *self.timerange
fmt_timer, times

clearplot
hio_plot_setup,/inv
!p.multi=0
 
!noeras=0
window, xsiz=700, ysiz=900, title='Multi-mission Protons'
!p.multi=[0,1,3]

csiz = 1.9
if !p.multi(2) eq 2 then csiz = 1.3

;    plot protons from STEREO-B HET

hio_plot_stereop, times, miss='STB', /over, xmargin=[10,6], charsiz=csiz, verbose=verbose

plims = [!x.window(0),!y.window(0),!x.window(1),!y.window(1)]			; setup for overplot

;    plot protons from GOES 

eau_plot_goesp, times, xmargin=[10,6], charsiz=csiz, tit='GOES Protons'  ,/force

;    plot protons from STEREO-A HET

hio_plot_stereop, times, miss='STA', /over, xmargin=[10,6], charsiz=csiz, verbose=verbose

plims(1) = !y.window(0)													; extend for all 3 plots

hio_oplot_icon							; put logo on plot

fmt_timer, times
    
;    allow overplotting of events that span all panels

print,plims
!p.position = plims 		; set window used by cursors

!noeras = 1
utplot, times,[0,1], /nodata, timer=times, xst=5,yst=5, xmargin=[10,6]

;    overploy acorss all three plots
self -> load_eventlist, name='cactus_all'
self -> show_event, vel=600, pawid=90, /over,  /all, color=3


self -> load_eventlist, name='goes_proton_event'
self -> show_event, /over,  /all, color=2

end