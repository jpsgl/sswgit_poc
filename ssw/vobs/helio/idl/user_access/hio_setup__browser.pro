
;--------------------------------------------------------------------------------

;    create temporary web page of CXS flare by replacing strings in a template

function make_cxs_fimage_page, cxs_return

if cxs_return.png_url eq '' or cxs_return.map_url eq '' then begin
  message,'CXS structure fields do not contain required information',info
  help, cxs_return
  return, -1
endif

qqx = rd_tfile(cxs_return.map_url)		; lines containing map recrords
;print,qqx,format='(a)'

;    read the file, replace the strings and write it to temp

outfile = 'cxs-fimage_' + cxs_return.rid + '.html'

qq = rd_tfile('$HELIO_DTB/cxs_flareplot_template.html')
qq = str_replace(qq,'xxcxs_imagefilexx', cxs_return.png_url)

pqx = where(strpos(qq,'xxcxs_mapfilexx') ge 0, npqx)
if npqx gt 0 then begin
  pqx = pqx(0)
  seg1 = qq(0:pqx-1)					; section before dummy record
  seg2 = qq(pqx+1:*)					; section after dummy record
  qq1 = [seg1, qqx, seg2]				; concatonate the sections
  
endif else message,'>>>>> Problem!!!!!!', /info

ofile = concat_dir(get_temp_dir(),outfile)
file_append,ofile,/new,qq1

return, ofile
end

;--------------------------------------------------------------------------------

;    create temporary web page of JMAPs by replacing strings in a template

; http://www.ukssdc.ac.uk/solar/stereo/movies/MOVIES/
; 2010/04_April/201004_hiB_jmap_ecliptic.jpeg

function make_ukssdc_jmap_page, time

tt = anytim2ex(time)

;    create the field that are dependant on the date

subdd = string(tt([6,5]),(list_months())(tt(5)-1),tt([6,5]), $
		format='(i4,1h/,I2.2,1h_,a,1h/,i4,i2.2,1h_)')

root = 'http://www.ukssdc.ac.uk/solar/stereo/movies/MOVIES/'

jmap_A = 'hiA_jmap_ecliptic.jpeg'
jmap_B = 'hiB_jmap_ecliptic.jpeg'

jmap_img1 = root+subdd+jmap_A
jmap_img2 = root+subdd+jmap_B

jmap_date = string((list_months())(tt(5)-1),tt(6),format='(a,x,i4.4)')

;    read the file, replace the strings and write it to temp

outfile = 'ukssdc-jmap_' + string(tt([6,5]),format='(i4,i2.2)') + '.html'

qq = rd_tfile('$HELIO_DTB/ral_jmap_template.html')
qq = str_replace(qq,'jmap_img1',jmap_img1)
qq = str_replace(qq,'jmap_img2',jmap_img2)
qq = str_replace(qq,'jmap_date',jmap_date)

ofile = concat_dir(get_temp_dir(),outfile)
file_append,ofile,/new,qq

return, ofile
end

;--------------------------------------------------------------------------------

pro hio_setup::browser, verbose=verbose, url=url, _extra=extra, $
	solarmonitor=solarmonitor, use_nar=use_nar, $
	stereo_movie=stereo_movie, $show_event=show_event, $
	help=help, crib=crib, hec_tabinfo=hec_tabinfo, $
	current_activity=current_activity, flareloc=flareloc

;--  start the web browser to display useful information

;>>  these options are not specific to any time

;    show SSW help information on HELIO Web page

if keyword_set(help) then begin
  url = 'http://www.helio-vo.eu/documents/help/ssw/'
  print,'Using: ',url
  hio_launch_browser, use_url=url
  return
endif

if keyword_set(crib) then begin		; ????
  url = 'http://www.helio-vo.eu/documents/help/ssw/hio_cribsheet.php'
  print,'Using: ',url
  hio_launch_browser, use_url=url
  return
endif

message,'Time Dependant browser options',/info

if keyword_set(current_activity) then begin
  url = 'http://www.helio-vo.eu/solar_activity/current/'
  print,'Using: ',url
  hio_launch_browser, use_url=url
  return
endif

if keyword_set(flareloc) then begin
  cxs_return = *self.cxs_return

  if datatype(cxs_return) ne 'STC' then begin
    box_message,['>>> CXS Return is not of the correct type ', $
    			'>>> Have you executed helio -> show_context ? ']
    return
  endif
  if self.flag_new_cxs eq 1 then begin
    box_message,'>>> New times - you need to execute  helio -> show_context'
    return  
  endif
  
  url = make_cxs_fimage_page(cxs_return)
  print,'Using: ',url
  hio_launch_browser, use_url=url
  return
endif

if keyword_set(solarmonitor) and keyword_set(use_nar) then begin
  if n_elements(self.nar_used) gt 0 then begin
; http://www.solarmonitor.org/region.php?date=20140817&region=12139
; http://www.helio-vo.eu/services/interfaces/cxs-soap_nar.php
; ?cxs_process=smarmovie&mtype=wlmag&format=normal&narno=12141

    cnar = self.nar_used
    help,cnar
  endif
endif

message,'time dependant',/info

if keyword_set(hec_tabinfo) then self -> save_struct,/hec, /html,/show

;>>  but these use selected time to define the entry point into the pages

;    use url supplied within the event list, if one exists

if keyword_set(show_event) then begin
  if n_elements(*self.eventlist_table) eq 0 then begin
    message,'Event list has not been selected',/info
    return
  endif

  self -> show_event
;;  print,''
;;  print,'Current List:  ',self.eventlist_name
;;  cevent = (*self.eventlist_table)(self.eventlist_row)
;;  print,'Selected Event:'
;;  help,/st, cevent
;;  print,''

  cevent = (*self.eventlist_table)(self.eventlist_row)
  if tag_exist(cevent,'event_detail') then begin
    url = cevent.event_detail
    hio_launch_browser, use_url=url
  endif else message,'NO URL available for this event',/info
  return
endif

;    start a web browser pointing to SolarMonitor, etc.

; help, *self.timewindow, *(self.timerange)

if n_elements(*self.timewindow) eq 0 then begin
  if n_elements(*(self.timerange)) eq 0 then begin
    message,'No times set',/info
    return
  endif else times = *(self.timerange)
endif else times = *self.timewindow

fmt_timer, times

hio_launch_browser, date=times(0), verbose=verbose, $
	solarmonitor=solarmonitor, stereo_movie=stereo_movie, $
	_extra=extra, use_url=url

end
