;+
; NAME:
;    
; PURPOSE:
;    
; CALLING SEQUENCE:
;    
; CATEGORY:
;    
; INPUTS:
;    
; INPUT KEYWORDS:
;    
; OUTPUTS:
;    
; OUTPUT KEYWORDS:
;    
; RESTRICTIONS:
;    
; HISTORY:
;    01-Apr-2011  rdb  written as part of hio_setup__define
;       Jul-2014  rdb  split from hio_setup__define
;    31-Jul-2014  rdb  added zoom to time window, [before,after] if 2 element array
;    19-Sep-2014  rdb  added option to plot STA/STB proton in /both
;    17-Apr-2016  rdb  added des_insitu keyword and call to DES plot routine
;                      changed plot window keyword to plot_window; confused with /WIND
;
;-

pro hio_setup::plot, timerange=timerange, $
	goes=goes, ace=ace, sta=sta, stb=stb, wind=wind, ulysses=ulysses, $
	xray=xray, proton=proton, both=both, insitu=insitu, des_insitu=des_insitu, $
	timewindow=timewindow, png=png, plot_window=plot_window, $
	refresh=refresh, invert=invert, zoom=zoom

;----   set time range, either from pbject or as supplied

if not keyword_set(timerange) then begin

;    default is to use time window from the object

  message,'Using times defined in object',/info

  if not keyword_set(zoom) then begin 
;    use the overall basetime interval

    if n_elements(*(self.timerange)) eq 0 then begin
      box_message,'Need to set a base time'
      self -> set_basetime, timerange=timerange
    endif
    base_times = *(self.timerange)
    
  endif else begin

;    allow zoom to smaller time window if interval suplied and zoom keywrd used
;    if zoom is a 2 eleemnt array, this represents [time_before, time_after]

    if n_elements(*(self.timewindow)) lt 2 then begin
      box_message, ['    ** Time Window is not defined ** ', $
      				'Select an event or select times from plot']
      return
    endif
    
    base_times = *(self.timewindow)
    if n_elements(zoom) eq 2 then begin
      base_times(0) = anytim(addtime(base_times(0),delt=-zoom(0)),/ecs)
      base_times(1) = anytim(addtime(base_times(1),delt=zoom(1)),/ecs)
    
      self.twind_zoom = zoom
    endif
    
  endelse
  
;;  print, 'Plot time range:', base_times, format='(a,t20,a,2x,2hto,2x,a)'

endif else begin

  message,'Using specifically supplied time',/info
  base_times = timerange

endelse

print, ''
print, 'Plot time range:', base_times, format='(a,t20,a,2x,2hto,2x,a)'

;    try to catch very long time intervals

tdiff = addtime(base_times(1), diff=base_times(0))/60./24.
print, 'Plot duration (days): ',tdiff

if tdiff gt 14. and self.plot_longbasetime eq 0 then begin
  box_message, ['** This is a very long time interval to plot **', $
  				'You can select an event and zoom around the time', $
  				'  e.g.    helio -> plot, /zoom  [,/proton']
  
  yesnox, '*** Proceed with plot', goplot
  if not goplot then return
  self.plot_longbasetime = 1			; flag OK to plot long base times

endif

if (keyword_set(ace)+keyword_set(sta)+keyword_set(stb)) gt 1 then begin

  message,'TOO many instruments set',/info
  help, keyword_set(ace), keyword_set(sta), keyword_set(stb)
  return

endif

;----    set up the plot title according to keywords...

case 1 of
  keyword_set(both):	ptitle = 'Soft X-rays & Protons'
  keyword_set(xray):	ptitle = 'GOES Soft X-rays'
  keyword_set(proton):	begin
      ptitle = 'GOES Protons'
      if keyword_set(sta) then  ptitle = 'STEREO-A Protons'
      if keyword_set(stb) then  ptitle = 'STEREO-B Protons'
    end
  keyword_set(insitu):	begin
      ptitle = 'ACE MAG and SWEPAM'
      if keyword_set(sta) then  ptitle = 'STEREO-A MAG & SWEPAM'
      if keyword_set(stb) then  ptitle = 'STEREO-B MAG & SWEPAM'
    end
  keyword_set(ace):	ptitle = '>>>> PLEASE USE /INSITU <<<<'	;'ACE MAG and SWEPAM'
;;  keyword_set(ace):	ptitle = 'ACE MAG & SWEPAM'
  else:			ptitle = 'GOES X-rays'
endcase
help, ptitle

if keyword_set(sta) or keyword_set(stb) then begin
  message,'STA or STB selected <<<<<<<<<<<<<<',/info
;;  message,'STA and STB not enabled yet <<<<<<<<<<<<<<',/info
;;  return
endif

;----    create the plot window and name it using the title

if not keyword_set(des_insitu) then begin

window, xsiz=700,ysiz=500, title=ptitle		; single plot ???
!noeras = 0
cleanplot, /silent
white_bkg = 0
if keyword_set(invert) or self.bkg_invert then white_bkg=1
hio_plot_setup, invert=white_bkg

endif

;----    do plot according to which keywords are set

case 1 of

;>>--    plot both X-rays (GOES) and protons (source selectable)

  keyword_set(both): begin
    !p.multi = [0,1,2]

    eau_plot_goesx,  base_times, xmargin=[10,6]	;, title='GOES Soft X-rays'
    
;    save plot limmit parameters - order of !p.position is  x0, y0, x1, y1
    plims = [!x.window(0),!y.window(0),!x.window(1),!y.window(1)]

    case 1 of					; protons default to GOES, optionally select STA or STB
      keyword_set(sta):  hio_plot_stereop, base_times, mission='STA', /over, xmargin=[10,6]
      keyword_set(stb):  hio_plot_stereop, base_times, mission='STB', /over, xmargin=[10,6]
      else:  eau_plot_goesp, base_times, xmargin=[10,6]  
    endcase
    
;    save plot limmit parameters - need to modify after 2nd plot
    plims(1) = !y.window(0)

;    ??? should this be done here or in the cursor routine??

    print,plims
    !p.position = plims 		; set window used by cursors
    fmt_timer, base_times
    !noeras = 1
    utplot,base_times,[0,1],/nodata,xst=5,yst=5, timer=base_times, xmargin=[10,6]

    self.flag_plot_otype = 'remote'	; remote-sensed (sort of)
    self.timeseries_data = 'goes_sxr and goes_proton'
    if keyword_set(sta) then self.timeseries_data = 'goes_sxr and sta_proton'
    if keyword_set(stb) then self.timeseries_data = 'goes_sxr and stb_proton'
;    print, 'new - GOES X-rays and Protons'
  end

;>>--    plot protons only (source selectable)

  keyword_set(proton): begin
  
    case 1 of					; protons default to GOES, optionally select STA or STB
      keyword_set(sta):  hio_plot_stereop, base_times, mission='STA', window=plot_window, xmargin=[10,6]
      keyword_set(stb):  hio_plot_stereop, base_times, mission='STB', window=plot_window, xmargin=[10,6]
      else: eau_plot_goesp, base_times, xmargin=[10,6]	;, title='GOES Integrated Proton flux'
    endcase
    
    !noeras = 1  ;??
   
    utplot,base_times,[0,1],/nodata,xst=5,yst=5, timer=base_times, xmargin=[10,6]

    self.flag_plot_otype = 'remote'	; remote-sensed (sort of)
    self.timeseries_data = 'goes_proton;
    if keyword_set(sta) then self.timeseries_data = 'sta_proton'
    if keyword_set(stb) then self.timeseries_data = 'stb_proton'
;    print, 'new - GOES Protons only'
  end

;>>--    plot ACE MAG and SWEPAM

  keyword_set(insitu): begin
    case 1 of
      keyword_set(ace):  hio_mplot_ace, base_times, title=ptitle, window=plot_window
      keyword_set(sta):  hio_mplot_stereo, base_times, title=ptitle, mission='STA', $
				window=plot_window
      keyword_set(stb):  hio_mplot_stereo, base_times, title=ptitle, mission='STB', $
				window=plot_window
      else:  hio_mplot_ace, base_times, title=ptitle, window=plot_window
    endcase
    fullwin_mpan, base_times		; set window used by cursors

    self.flag_plot_otype = 'in-situ'		; in-situ
    self.timeseries_data = 'ace_swind'
;    print, 'new - ACE solar wind multi-plot'
  end

;>>--    plot ACE, WIND, etc in-situ data using the DES

  keyword_set(des_insitu): begin
    message, 'Doing plot of in-situ data using the DES', /info
    
    make_des_insitu_plot, base_times, $					; verbose=verbose, 
        ace=ace, wind=wind, ulysses=ulysses, sta=sta, stb=stb
    
    self.flag_plot_otype = 'in-situ'		; in-situ
    self.timeseries_data = 'DES'
  end

;>>--    default to GOES X-rays only

  else: begin
    eau_plot_goesx, base_times, xmargin=[10,6]		;, title='GOES Soft X-rays'
    !noeras = 1  ;??
    utplot,base_times,[0,1],/nodata,xst=5,yst=5, timer=base_times, xmargin=[10,6]

    self.flag_plot_otype = 'remote'			; remote-sensed 
    self.timeseries_data = 'goes_sxr'
;    print, 'new - GOES X-rays only'
  end

endcase

self.flag_plot_done = 1					; need plot before can user corsors

hio_save_prec, 'tplot', ptitle

end
