;+
; PROJECT:
;        HELIO
; NAME:
;        hio_serup__multi_insitu
; PURPOSE:
;        This example show how to plot the in-situ data are ACE and the STEREO Behind and 
;        Ahead spacecraft. Event data from lists held in the the HEC showing the times of  
;        Stream Interaction Regions (SIR) are over-plotted on the light-curves.
;
; CALLING SEQUENCE:
;        helio -> multi_insit
; CATEGORY:
;        
; INPUTS:
;        time range is determined from the object
; INPUT KEYWORDS:
;        
; OUTPUTS:
;        
; OUTPUT KEYWORDS:
;        
; RESTRICTIONS:
;        
; HISTORY:
;        01-Apr-2011  rdb  written
;
;-

pro hio_setup::multi_insitu

;times=['01-jan-2008','1-mar-2008']

;    Plot in-situ data for STA; load SIR event list and overplot

self -> plot, /insitu,/sta, win=20
self -> load_eventlist, name='stereoa_impactplastic_sir'
self -> show_event, /over, /all

;    Plot in-situ data for ACE; load SIR event list and overplot

self -> plot, /insitu,/ace, win=21
self -> load_eventlist, name='wind_ace_sir'
self -> show_event, /over, /all

;    Plot in-situ data for STB; load SIR event list and overplot

self -> plot, /insitu,/stb, win=22
self -> load_eventlist, name='stereob_impactplastic_sir'
self -> show_event, /over, /all

end
