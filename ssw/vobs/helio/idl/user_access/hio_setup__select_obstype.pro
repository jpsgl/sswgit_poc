;----------------------------------------------------------------

pro hio_setup::select_obstype, $
		halpha=halpha, rhelio=rhelio, magmap=magmap, sep=sep, structure=structure, $
		verbose=verbose, pause=pause, full=full, force_newpat=force_newpat

;    /append or /add  ?????
;    /edit

;--  select list of required instruments by observing type

if n_elements(*self.timerange) eq 0 then begin
  message = [' ***   Time window NOT selected   *** ', $
              '   Please select one and try again']
  box_message, message
  return
endif else times = *self.timerange

;;times = *self.timerange


;    load the allowed list of instruments from the PAT

if n_elements(*self.pat_oinst) eq 0 or keyword_set(force_newpat) then *self.pat_oinst = hio_get_dpaspat(/force)
allowed_oinst = *self.pat_oinst
help, allowed_oinst

oinst = hio_sel_obsinst_bytype(timer=times, pat=allowed_oinst, verbose=verbose, $
		halpha=halpha, rhelio=rhelio, magmap=magmap, sep=sep, structure=structure)

*self.obs_inst = oinst
 
 ;    looks better if name uses - and / instead of _ and __ 
coinst = *self.obs_inst
coinst = str_replace(coinst,'__','/')
coinst = str_replace(coinst,'_','-')

box_message, ['Selected Instruments:  ', ' ' + coinst]

return

end
