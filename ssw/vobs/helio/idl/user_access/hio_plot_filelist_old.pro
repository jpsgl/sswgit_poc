;+
; NAME:
;       hio_plot_filelist
; PURPOSE:
;       Plots the times of files in file_list structure returned by EGSO API
; CALLING SEQUENCE:
;       hio_plot_filelist, file_list  [,/png] [,/goes]
;
; INPUTS:
;       file_list    "List of Files" structure returned by EGSO API
; KEYWORDS:
;       timerange    two element array defining time range over which to plot
;                    [default defined by time limits of structure file_list]
;       goes         if set, plot goes light curve below file times
;                    (requires connection to the Internet)
;       timewindow   two element array defining vertical lines that are plotted
;       png          if set, create PNG file of plot
;
; RESTRICTIONS:
;       Must be connected to Internet to use /goes keyword
; HISTORY:
;          Jul-2005  Written by Bob Bentley (MSSL/UCL)
;          Oct-2005  rdb; moved goes plot code into separate routine that uses CURL
;                    to get data since sockets do not always work...
;       11-Jan-2007  rdb; renamed goes plot routine, added parameters
;                    clip start of time range if excessive - cludge bad data!
;       19-Feb-2007  rdb; better phrasing of setting of time range
;
;       19-Jul-2014  rdb  HELIO version
;       06-Aug-2014  rdb  Some tidying...
;		30-Sep-2014  rdb  Added ability to plot protons...
;       23-Jan-2017  rdb  Added call to hio_save_prec to store plot window parameters
;-

pro hio_plot_filelist, struct, timerange=timerange, timewindow=timewindow, $
			title=title, window_number=window_number, png=png, $
;			sta=sta, stb=stb, proton=proton, lightcurve=lightcurve, $
			goes=goes, force=force, debug=debug, invert=invert

common eplot, window_index, ogoes, gsta, gstp


mtit = 'Times from FileList'
if keyword_set(title) then mtit=title

;    set the time range used for the plot

if not keyword_set(timerange) then begin
  times = get_plot_timerange(struct)
  message,'*** New way for time range',/info
endif else times = timerange


;  create index for sorting data

obs_inst = struct.observatory+'/'+struct.instrument

qg = where(struct.group eq 'GONG',nqg)
if nqg gt 0 then obs_inst(qg) = struct(qg).groupmember +'/'+ struct(qg).instrument

noinst = n_elements(obs_inst)
gtext = strarr(noinst)
if nqg gt 0 then gtext(qg) = '(G) '
allinx = all_vals(gtext+obs_inst)

allin = all_vals(obs_inst)
;allin = reverse(allin)
n_allin = n_elements(allin)

if keyword_set(debug) then begin
  help, allin, allinx
  box_message, ['Compare obs_inst strings', '< '+allin, '> '+allinx]
endif

if n_elements(allin) ne n_elements(allinx) then begin
  help, allin, allinx
  print, allin
  print, allinx
  message,'>>>>>> Array sizes differ, big problem', /info
endif

;
;    sort obs/instruments so GBO by longitude, then SBO alpabetic

aobs = struct.observatory
if nqg gt 0 then aobs(qg) = struct(qg).groupmember
obs = all_vals(aobs)

;obs = all_vals(struct.observatory)

sobs = hio_sort_gbosbo(obs, /long, debug=debug)

oo = obs(sobs)
if n_elements(sobs) ne n_elements(obs) then begin
  message,'>> Unable to sort by Obs. type - default to as supplied',/info
  oo = obs
endif

out=''
for j=0,n_elements(oo)-1 do begin
  qv = where(strpos(allin,oo(j)) eq 0)
  if qv(0) ge 0 then out=[out,allin(qv)]
endfor
allin = reverse(out(1:*))

;    sort the prefixed names into the same order

ord = intarr(n_allin)
for jor=0,n_allin-1 do begin
  qor = where(strpos(allinx, allin(jor)) ge 0, nqor)
  if nqor eq 1 then ord(jor) = qor(0)
endfor
allinx = allinx(ord)

if keyword_set(debug) then begin
  print, obs
  print, oo
  print, allin
  print, allinx
endif

;   form array used for labelling the plot
ynames = ([' ',allinx,' '])   ;(n_allin+1-indgen(n_allin+2))
ynames = str_replace(ynames, '_','-')					; make names nore normal

;    speed by doing conversions once...

tstart = anytim(struct.time_start,/ecs)
;tend = anytim(struct.time_end,/ecs)

;
;-----------------------------------------------------------------------------------------

;    load the colour table
hio_plot_setup, invert=invert

;;!p.noerase=0

if !d.name eq 'X' then begin

;    define the window number
;    wish could do a /free, but cannot then use the window command...

;  help,window_index
  if n_elements(window_index) eq 0 then window_index=30
  window_idx = window_index
  if keyword_set(window_number) then window_idx = window_number

;    define the size of the plot window  
;;;;  ywinst = 45. + 20*(n_allin+2) + 25			; height depend on no. of instruments
  ywinst = 45. + 21*(n_allin+2) + 25			; height depend on no. of instruments
  ywsiz = ywinst
  if keyword_set(goes) then ywsiz = ywsiz + 200
  xwsiz = 750.		;640.
  ptitle = 'Times of observations'
  window, window_idx, xsiz=xwsiz, ysiz=ywsiz, title=ptitle

endif


xstyle = 1
!p.region = 0
nolabel = 0

if keyword_set(goes) then begin
;;   !p.region=[0.0,0.38,1.0,1.0]
;>>   yff = (1. - (20.*(n_allin+2) + 25 + 45)/ywsiz)
   yff = (1. - ywinst/ywsiz)
  !p.region = [0.0,yff,1.0,1.0]
  print,!p.region
  xstyle = 1  ;+4
  nolabel = 1
endif

xmargin = [18,4]
charsize = 1.1

;-------------------------------------------------
;    plot first instrument to establish plot

qv = where(obs_inst eq allin(0))
utplot, tstart(qv), intarr(n_elements(qv))+1, psym=1, $
	timer=times, xst=xstyle, title=mtit, charsize=charsize, $
	yrange=[0,n_allin+1], yst=4, xmargin=xmargin, $
	ytickname=ynames, yticks=n_allin+1, yminor=1, yticklen=0.01, $
	nolabel=nolabel, /year

;;;;;;;;axis,xaxis=0,xtickname=strarr(7),xst=8
DEVICE, SET_FONT='Courier', /TT_FONT
axis, yaxis=0, yrange=[0,n_allin+1], yst=1, yticks=n_allin+1, yminor=1, yticklen=0.01, $
		ytickname=ynames, font=1, charsize=1.5
axis, yaxis=1, yrange=[0,n_allin+1], yst=1, yticks=n_allin+1, yminor=1, yticklen=0.01, $
		ytickname=replicate(' ', n_elements(ynames))
!p.font=-1


;-------------------------------------------------
;    plot all instruments

;    define instruments that need a bar plotted (start->end times)
sinst = ['CELIAS', 'COSTEP', 'GOLF', 'VIRGO', 'ERNE', $   ; SOHO ptcl/helioseismology
         'EPAM', 'MAG', 'SWEPAM', 'EPAC', 'VHM_FGM', $    ; ACE & Ulysses ptcl, etc.
         'DIFOS', $
         'SWAN', 'CDS', 'UVCS', 'SUMER', $    ; SOHO images  (complex files?)
         'NDA', 'NTRFA','DAN']             ; Nancay Radio


for j=0,n_allin-1 do begin    ;was 1 - repeat 0 in case it is a special_case...

  cinst =  allin(j)
  qv = where(obs_inst eq cinst, nqv)
  special_case = 0
  for js = 0,n_elements(sinst)-1 do if (strpos(cinst,sinst(js)) ge 0) then special_case=1
;;  print,'>>>> ',cinst

;  help,qv
;  fmt_timer,struct(qv).time_start
;  help,special_case

;  print,tag_names(struct)

  if not tag_exist(struct,'pflag') then begin
;;    message,'NOT using pflag',/info
    print,'>>>> '+cinst, 'NOT using pflag', format='(a,320,a)'

;    some datasets are for extended time intervals -> plot as line
    if special_case then begin
      for k=0,n_elements(qv)-1 do $
        outplot, anytim([struct(qv(k)).time_start,struct(qv(k)).time_end],/ecs), [j,j]+1, psym=-1
        
      if keyword_set(debug) then begin
        print,j,cinst,n_elements(qv),format='(i3,2x,a,t20,i5)'
        tdiff = addtime(anytim(struct(qv).time_end,/ecs),diff=anytim(struct(qv).time_start,/ecs))
        print,tdiff
      endif

    endif else $
    outplot, tstart(qv), intarr(n_elements(qv))+j+1, psym=1

  endif else begin

;;    message,'Using pflag',/info
;;    print,'>>>> '+cinst, 'Using pflag', format='(a,t30,a)'

    outplot, tstart(qv), intarr(n_elements(qv))+j+1, psym=1
    qpf = where(struct(qv).pflag, npf)
;    help,nqv  ;,npf
    if !hio_sysvar.debug eq 1 then print,'>>>> '+cinst, 'Using pflag', npf, format='(a,t30,a,i10)'

    if npf gt 0 then begin
;      help,npf
      for k=0, npf-1 do $
        outplot, [struct(qv(qpf(k))).time_start,struct(qv(qpf(k))).time_end], [j,j]+1, psym=-1
;        outplot, [tstart(qv(qpf(k))),tend(qv(qpf(k)))], [j,j]+1, psym=-1
    endif

  endelse

endfor

;-------------------------------------------------
;    plot daylight intervals of the GBOs

doy = anytim2doy(anytim(struct(0).time_start,/ecs)) 
for j=0,n_allin-1 do begin
  qv = min(where(obs_inst eq allin(j)))
;  help, allin(j), qv
;;  srss = obs_srss(doy ,obs=struct(qv).observatory)
;;  if srss(0) ge 0 then uplot_band, j, range=srss
;;;;  srss = gbo_daylight(doy ,obs=struct(qv).observatory, debug=debug)		;, /local)

  srss = gbo_daylight(doy, obs=aobs(qv), debug=debug)		;, /local)
  if srss(0) ne -1 then utplot_band, j, range=srss, debug=debug
endfor

;    if timewindow supplied, overplot as vertical lines 

if keyword_set(timewindow) then begin
  print,'Time Window:' & fmt_timer, timewindow
  vline = anytim(timewindow,/ecs)
;;  for j=0,1 do outplot,[timewindow(j),timewindow(j)],[0,n_allin+1],psym=-1,linestyle=1
  for j=0,1 do outplot,[vline(j),vline(j)],[0,n_allin+1],psym=-1,linestyle=1
endif

;-------------------------------------------------
;    plot the GOES panel if requested

if keyword_set(goes) then begin
;if keyword_set(lightcurve) then begin

  yff = (200.+45)/ywsiz
  !p.region=[0.0,0.0,1.0,yff]
  print,!p.region
  !p.noerase=1
  
;;  xmargin = [36,4]

;    see if the goes keyword defined type of plot

  sgoes = strlowcase(strtrim(string(goes),2))
;;  sgoes = strlowcase(strtrim(string(lightcurve),2))
  if sgoes eq 'goes' then sgoes='1'
  if sgoes eq 'proton' then sgoes='2'
;;  sgoes='2'
  help,sgoes
  
;;  if (sgoes eq '1' or sgoes eq 'xray') then begin
  case 1 of 
  (sgoes eq '1'): begin
    message,'Plotting GOES X-rays',/info
    eau_plot_goesx, times, xmargin=xmargin, charsize=charsize
    end
  (sgoes eq '2'): begin
    message,'Plotting selected Protons',/info
    eau_plot_goesp, times, xmargin=xmargin, charsize=charsize
    end
  (sgoes eq '3'): hio_plot_stereop, times, mission='STA', xmargin=xmargin, charsize=charsize, /over
  (sgoes eq '4'): hio_plot_stereop, times, mission='STB', xmargin=xmargin, charsize=charsize, /over
  else: eau_plot_goesx, times, xmargin=xmargin, charsize=charsize
  endcase

;    case 1 of
;      keyword_set(sta):  hio_plot_stereop, base_times, mission='STA'
;      keyword_set(stb):  hio_plot_stereop, base_times, mission='STB'
;      else: eau_plot_goesp, times, xmargin=xmargin, charsize=charsize
;    endcase

  !p.noerase=0 & !p.region=0

endif

;-------------------------------------------------
;

if !d.name eq 'PS' then device,/close

if !d.name eq 'X' and keyword_set(png) then begin
  message,'Creating PNG file',/cont
  img = tvrd()
  tvlct,rr,gg,bb,/get
  if rr(0) eq 0 then img=255b-img 
  write_png,'idl.png',img
endif
    
hio_save_prec, 'filelist', ptitle
  
end
