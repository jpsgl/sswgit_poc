;+
; NAME:
;	hio_retrieve_filelist
; PURPOSE:
;	Retrieves file contained in the filelist structure from the providers
; INPUTS:
;	filelist	filelist structure
; INPUT KEYWORDS:
;	outdir		output directory to use; default define by env. var. hio_datatree
;	compact		if set store under general directory rather than creating a
;				sub-directory for the request
;	debug		extra information printed; does not do copy!
; OUTPUTS:
;	None		(copies files to a directory)
; CALLING SEQUENCE:
;	hio_retrieve_filelist, filelist	[,outdir=outdir] [,/compact]
;
; RESTRICTIONS:
;
; HISTORY:
;    14-Mar-2011  rdb  Added debug and addition question before proceeding
;    14-Apr-2011  rdb  Added max copy override
;    20-Aug-2011  rdb  Use URL supplied as env. variable 'hio_datatree' to store data
;
;    19-Jul-2014  rdb  rename to helio version
;    05-Aug-2014  rdb  added /form_copyllist; general tidying and better messages
;
;-

pro hio_retrieve_filelist, flist, $			;, max_copy=max_copy, $
		outdir=outdir, compact=compact, $	;same_root=same_root, $
		form_copylist=form_copylist, $
		debug=debug

common dpas_req, ssroot
;help,ssroot

;    compact all to current directory, or form tree??

datatree_env = '$hio_datatree'
save_root = get_logenv(datatree_env)
;save_root = ''
if keyword_set(outdir) then save_root = outdir
help,save_root

;    should this be the time of the DPAS return?  (== ssroot)

sroot = 'hio-' + time2fid(reltime(/now),/time)
if n_elements(ssroot) gt 0 then sroot = ssroot

;;if keyword_set(same_root) then sroot = 'hio-root'

if keyword_set(compact) then begin
  message,'Using COMPACT Directory Structure',/info
  if keyword_set(outdir) then sroot='' else sroot = 'hio-root' 
endif
help, sroot

;;root = sroot
;if keyword_set(outdir) then root=concat_dir(outdir,root)
root = concat_dir(save_root, sroot)

print,''
print, '* Files will be stored under Directory: ',root

;
;    create output tree unless told not to do so

if not keyword_set(compact) then begin

  topdirs = strlowcase(all_vals(flist.observatory))                        
  subdirs = strlowcase(all_vals(flist.observatory+'/'+flist.instrument)) 

  print,''
  print, '> Directories that would be created:'
  print, root+'/'+topdirs, format='(a)'
  print, '> Sub-directories that would be created:'
  print, root+'/'+subdirs, format='(a)'
  print,''

  if not keyword_set(debug) then begin

    yesnox,'Do you want to proceed with creating the directories: ',okgo
    if not okgo then return

;    first create the root of the directory tree

    print,'* Creating Directory Tree in: '+root

    if not is_dir(root) then begin
      mk_dir,root
      message,'Root directory created',/info
    endif else $
      message,'Root diectory already exists',/info
    
    if not is_dir(root) then begin
      help, outdir, root
      box_message,['  Problem with root outdir: ' + root + '  ', $
                   '  Are you allowed to do this?']
      return
    endif

;    then create all the directories and sub-directories

    for j=0,n_elements(topdirs)-1 do mk_dir,root+'/'+topdirs(j)
    for j=0,n_elements(subdirs)-1 do mk_dir,root+'/'+subdirs(j)
  
  endif

endif

if keyword_set(debug) then return

;=====================================================================================

print,''
yesnox,'Do you want to proceed with the copy: ',okgo
if not okgo then return

max_copy=''						; use string so that will accept <CR>
read, 'Enter max no. of files to copy [def=10]: ',max_copy
max_copy = fix(max_copy)
print,''

;    okay, get the data...

host = get_logenv('HOST') 					;& host='msslmq'
if strpos(host,'mssl') ge 0 then doftp=0  else doftp=1

httpconnect = obj_new( 'http' )

maxcopy = 10  								;nrec/10      ;???????????
if max_copy gt 0 then maxcopy=max_copy

nrec = n_elements(flist)
kount = 0

;    Copy each records in turn
;??    should this be done an instrument at a time or by time ordered

for j=0,nrec-1 do begin

  url = flist(j).url		;???  tag_exist(filelist,'url')
;;  print,'Get: ',url

  break_file, url, aa,bb,cc,dd
  filename = cc+dd
  outname = root+'/'+filename
  if not keyword_set(compact) then begin
;;      outdir = root+'/'+strlowcase(flist(j).observatory+'/'+flist(j).instrument)
;; root = concat_dir(save_root, sroot)
      outpath = concat_dir(sroot, strlowcase(flist(j).observatory+'/'+flist(j).instrument))
      outdir = concat_dir(save_root, outpath)
      outname = concat_dir(outdir, filename)
  endif
  
    isftp=0  &  if strpos(url,'ftp') ge 0 then isftp=1
;  ishttp=0  &  if strpos(inname,'http') ge 0 then ishttp=1

  if kount lt maxcopy then begin
  
    if isftp and not doftp then begin
;       print,'>>>> Skipping FTP', j
;;      if kount eq 0 then print,'Get: '+url else $
      print,'Geting: '+filename
      
      if keyword_set(form_copylist) then begin      
        outdir = concat_dir(datatree_env, outpath, /notran)
        print,outdir
        cmd = wget_copy(url, outdir=outdir)
        print,cmd 					; append_file ???
      endif else begin
        cmd = wget_copy(url, outdir=outdir)
        spawn, cmd
        print,'Write: ',outname, j, kount
      endelse
      kount = kount+1

    endif else begin			; ???? why is this here
;;      if kount eq 0 then print,'Get: '+url else $
      print,'Geting: '+filename
      
      if keyword_set(form_copylist) then begin
        print,'httpconnect' 		; append_file
      endif else begin
        httpconnect->copy, url, outname
        print,'Write: ',outname, j, kount
      endelse
      kount = kount+1

    endelse
    
    if kount eq maxcopy then $
        box_message,['',' ****          Skipping the rest          **** ', $
                        ' ** Set Max Copy to copy more than 10 files ** ','']
  endif
 
endfor

end
