;+
; NAME:
;    
; PURPOSE:
;    
; CALLING SEQUENCE:
;    
; CATEGORY:
;    
; INPUTS:
;    
; INPUT KEYWORDS:
;    
; OUTPUTS:
;    
; OUTPUT KEYWORDS:
;    
; RESTRICTIONS:
;    
; HISTORY:
;    01-Apr-2011  rdb  written
;    various updates
;    22-Mar-2017  rdb  Added twind_zoom
;-


;----------------------------------------------------------------

pro hio_setup::summary, value, short=short, full=full

;--  summary of what currently set??

box_message,'Summary of values set in hio_setup object',/info

;--------------------------
print,''
;help, *(self.timerange)
if n_elements(*(self.timerange)) eq 2 then begin
  fmt_timer,*(self.timerange), tmin, tmax, /quiet
  print,'Time Range:  ',tmin,tmax, format='(a,t20,a,6h  to  ,a)'
endif else print,'Time Range:','NOT Set', format='(a,t20,a)'

;help, *(self.timewindow)
if n_elements(*(self.timewindow)) eq 2 then begin
  fmt_timer,*(self.timewindow), tmin, tmax, /quiet
  print,'Time Window: ',tmin,tmax, format='(a,t20,a,6h  to  ,a)'
endif else print,'Time Window:','NOT Set', format='(a,t20,a)'

print,'Zoom offsets (mins):', self.twind_zoom, format='(a,t30,2i6)'

print,''
print,'BaseTime selection Mode:', self.flag_basetime_mode, format='(a,t30,a)'
print,'Time selection Mode:', self.flag_select_mode, format='(a,t30,a)'

;--------------------------
print,''
if n_elements(*(self.obs_inst)) ne 0 then begin
;  print,''
  print, 'Selected Instruments:'
  print, arr2str(*(self.obs_inst), delim=', ')
endif else print,'NO Instruments selected'

;--------------------------
print,''
if n_elements(*(self.hec_tabinfo)) gt 0 then begin
  if keyword_set(full) then begin
;    print,''
    print,'>>>>>>>> Event table information/classification'
    help, self.eventlist_name
    help, (*self.hec_tabinfo)(self.hec_tab_row),/st
  endif else print,'HEC Info Table is loaded'
endif else print,'HEC Info Table NOT loaded'

;--------------------------
print,''
print,'Selected Event List: ', self.eventlist_name, format='(a,t30,a)'

if n_elements(*(self.eventlist_table)) gt 0 then begin
;  if keyword_set(full) then begin
    print,''
    print,'>>>>>>>> No. of events within time interval'
    help, *self.eventlist_table
;  endif
endif else print,'NO Events within time interval'

;--------------------------
;print,''
if self.eventlist_row ge 0 then begin
    print,''
    print,'>>>>>>>> Details of selected event:'
    print,'Selected Row:',  self.eventlist_row, format='(a,t30,i3)'
  if not keyword_set(short) then begin
    help,(*self.eventlist_table)(self.eventlist_row),/st
  endif
endif else print,'NO Event selected'

;????   should these be reset when switching modes

;--------------------------
;print,''
if n_elements(*self.cxs_return) gt 0 then begin
    print,''
    print,'>>>>>>>> Information from the last CXS Interaction:'
    help, *self.cxs_return
endif else print,'NO Event selected'

;--------------------------
print,''
print,'>>>>>>>> Details of selected ROI:'
print,'ROI selection Mode:', self.flag_roi_mode, format='(a,t30,a)'
if n_elements(*self.roi_info) gt 0 then help, *self.roi_info

if n_elements(self.cxsimage_roi) gt 0 then help, self.cxsimage_roi

;--------------------------
print,''
print,'>>>>>>>> Details of FILELIST Filters:'
if n_elements(*(self.flist_timewindow)) eq 2 then begin
  print,'Filter time window:', *(self.flist_timewindow), format='(a,t20,a,6h  to  ,a)'
  flist_filters = *(self.flist_filters)
  help,flist_filters
endif else print,'NO Filters established'

;--------------------------
;    is thi releevnt tomodel run?
print,''
print,'Plot Observation type:', self.flag_plot_otype, format='(a,t30,a)'

;--------------------------
print,''
if n_elements(*(self.prop_base_event)) gt 0 then begin
  print,''
  print,'>>>>>>>> Prop. model base info.'
  help,/st,*self.prop_base_event
endif else print,'Prop. Model structure NOT defined'

print,''

end
