
;----------------------------------------------------------------

function hio_setup::hecinfo_test

;--    ??? purpose

hec_tabinfo = *self.hec_tabinfo
nrow = n_elements(hec_tabinfo)

hec_tabinfo = add_tag(hec_tabinfo,'','target')
hec_tabinfo = add_tag(hec_tabinfo,'','obj2')

for jrow=0,nrow-1 do begin

clist = hec_tabinfo(jrow)
help,/st, clist

case 1 of
  strupcase(clist.otyp) eq 'R': begin
    otype = 'remote'
    target = '????'
    if clist.solar eq 'y' then target = 'Sun'
    print, 'Remote observations of the '+target
;    base_event.obs_type = otype
;    base_event.target = target
     hec_tabinfo(jrow).target = target
;    base_event.object = clist.object
     hec_tabinfo(jrow).obj2 = clist.object
;    some lists with STEREO mix spacecraft ?????????????
;    if clist.flag eq 'ssss' then base_event.object = strlowcase(cevent.sat_id)
  end

  strupcase(clist.otyp) eq 'I': begin
    otype = 'in-situ'
;;    object = '????'
    environ = '????'
    if clist.planet eq 'y' then $
      if clist.flag eq 'oooo' then environ = clist.object	;'planet xxxx'
    if clist.geo eq 'y' then environ = 'Earth'
;>>    if clist.ips eq 'y' then $
      if clist.flag eq 'oooo' then environ = clist.object	;'mission xxxx'
    print,'In-situ observations of environment near '+environ
;;    print,'Object for ILS search: '+object
;    base_event.obs_type = otype
;    base_event.object = environ
     hec_tabinfo(jrow).obj2 = environ
  end

  else: message,'****** problem ******',/info
endcase

if clist.flag eq 'ssss' then hec_tabinfo(jrow).obj2 = clist.flag
if clist.flag eq 'xxxx' then hec_tabinfo(jrow).obj2 = clist.flag

;;hec_tabinfo(jrow) = clist
endfor

hio_stc2html, hec_tabinfo, file='ggtt.html',tit='fixit'

return, hec_tabinfo
end
