;+
; NAME:
;    hio_summ_filelist
; PURPOSE:
;    Produces a summary of the filelist structure returned by the DPAS
; CALLING SEQUENCE:
;    hio_summ_filelist, struct [, /make_ack]
; CATEGORY:
;    
; INPUTS:
;    struct
; INPUT KEYWORDS:
;    make_ack        makes a web page containing the acknowledgements for each provider
;    short
;    verbose
; OUTPUTS:
;    
; OUTPUT KEYWORDS:
;    
; RESTRICTIONS:
;    
; HISTORY:
;    01-Apr-2011  rdb  written
;    10-Feb-2007  rdb  added /short switch to suppress list of obs/inst pairs 
;    28-Feb-2007  rdb  small mod of format of list
;    09-Mar-2012  rdb  added summary of providers
;    21-Jul-2016  rdb  Added the breakdown of file types
;    10-Aug-2016  rdb  Added test that supplied variable is a structure
;    24-Mar-2017  rdb  Small adjustments
;-

pro hio_summ_filelist, struct, short=short, verbose=verbose, make_ack=make_ack

;  hio_summ_filelist, file_list(hio_filter_filelist(file_list,timerange))

if keyword_set(verbose) then help,/st,struct
if datatype(struct) ne 'STC' then begin
  message,'>>>> Supplied variable is not a Structure',/info
  return
end

obs_inst = struct.observatory+'/'+struct.instrument
sum_obs_inst = all_vals(obs_inst)

print,''
print,'* Total No. of Obs./Inst. pairs and No. of files:', $
       n_elements(sum_obs_inst),n_elements(struct), format='(a,i6,i10)'

if not keyword_set(short) then begin
  print,'* Summary of No. of files per Observatory/Instrument pair'

  for j=0,n_elements(sum_obs_inst)-1 do begin
    qv = where(obs_inst eq sum_obs_inst(j), nqv)

;    identify the supplier of the data
;    possible that there may be multiple providers - not sure how to handle...
    suppliers = '(' + arr2str(all_vals(struct(qv).provider),',') + ')'

;    determine no of each type of file for this obs/inst
    break_file, struct(qv).url, aa,bb,cc,dd,ee
    extns = all_vals(dd)
    if !hio_sysvar.debug eq 1 then print, extns

goto, xxxc 
    rep_str = ''
    for jex=0,n_elements(extns)-1 do begin
      qx = where(dd eq extns(jex), nqx)
      rep_str = [rep_str, extns(jex)+' ('+strtrim(string(nqx, format='(I6)'),2)+')']
    endfor
xxxc:

    lcurl = strlowcase(struct(qv).url)
    nff = n_elements(lcurl) & if !hio_sysvar.debug eq 1 then help,lcurl

    rep_str = '' & nqf = 0 & nqi = 0
    qf = where(strpos(lcurl,'fits') gt 0 or strpos(lcurl,'fts') gt 0, nqf)
    if nqf gt 0 then rep_str = [rep_str, 'fits ('+strtrim(string(nqf, format='(I6)'),2)+')']
;;    qi = where(strpos(lcurl,'jpg') gt 0 or strpos(lcurl,'jpeg') gt 0 or strpos(lcurl,'png') gt 0, nqi) 
    qi = where(strpos(lcurl,'jpg') gt 0 or strpos(lcurl,'jpeg') gt 0 or strpos(lcurl,'png') gt 0 or strpos(lcurl,'gif') gt 0, nqi) 
    if nqi gt 0 then rep_str = [rep_str, 'jpg/png/gif ('+strtrim(string(nqi, format='(I6)'),2)+')']

    if nqf+nqi lt nff then begin
      pp = indgen(nff) & if nqf gt 0 then pp(qf) = -1 & if nqi gt 0 then pp(qi) = -1
      ppx = where(pp ge 0, npp)
      if npp gt 0 then rep_str = [rep_str, 'other ('+strtrim(string(npp, format='(I6)'),2)+')']
    endif

    if rep_str(0) eq '' then rep_str=rep_str(1:*)
    ftypes = '['+ arr2str(rep_str, delim='; ') +']'

    print,sum_obs_inst(j), nqv, suppliers, ftypes, format='(a,t25,i5,2x,a20,2x,a)'

  endfor

endif

print,''

;    produce a summary of the acknowledgements 

if keyword_set(make_ack) then begin
  box_message, 'Summary of DPAS Acknowledgements writtem to Web page'
  ack = hio_get_dpasack(struct, /brief, /html)  
endif

end
