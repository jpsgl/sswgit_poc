function match_idx_window, index_times, ref_time, window_hours=window_hours

;+
; NAME:
;	MATCH_IDX_WINDOW
; PURPOSE:
;	Select index records that fall within a time window that symmetrically spans 
;	a reference time
;
; CALLING SEQUENCE:
;	idx = match_idx_window(index_times, ref_time  [, window_hours=window_hours])
; INPUT:
;	index_times	list of times from an index (structure)
;	ref_time	reference time, e.g. from a SSW map object
; OUTPUT:
;	idx
; KEYWORDS:
;	window_hours	number of hours to use for time window [def=12]
;
; PROJECT:
;	EGSO
; HISTORY:
;	17-Sep-2004  rdb  Written
;-

win = [-12,12]	; default to window of +/- 12 hours
if keyword_set(window_hours) then begin
 if n_elements(window_hours) eq 1 then win = [-window_hours,window_hours]
 if n_elements(window_hours) eq 2 then begin
   win = window_hours
   if win(0) gt 0 then win(0)=-win(0)
 endif
endif

;  define start and end of window in FIDs
t0 = fmt_tim(addtime(ref_time,delt=win(0)*60))
t0 = time2fid(t0,/full,/time,/sec)

t1 = fmt_tim(addtime(ref_time,delt=win(1)*60))
t1 = time2fid(t1,/full,/time,/sec)

;  convert index times to FIDs
tidx = time2fid(index_times,/full,/time,/sec)

;  determine those that fall within the window
ss = where(tidx ge t0 and tidx le t1)

return,ss
end
