;+
; NAME:
;       fmt_stc_table
; PURPOSE:
;       Formats structure into string array so that it can be "printed"
; CALLING SEQUENCE:
;       result = hio_fmt_stc_table(struct)
;
; INPUTS:
;       struct       IDL structure
; OUTPUTS:
;       result       formatted string array - tabular version of structure contents
; KEYWORDS:
;       subset       subset of tags names to be used (by tag index no.) 
;       maxwid       maximum width of table row  (i.e. max. length of string) 
;       name_columns if set, include the column names in the first row
;
; METHOD:
;       Establishes max length of field in each column and the assembles table,
;
; HISTORY:
;          May-2005  rdb  Written by Bob Bentley (MSSL/UCL)
;       16-Jun-2005  rdb  added /subset keyword 
;       17-Jun-2005  rdb  add names of columns
;       29-Jun-2014  rdb  made format for TIME_ fields better
;
;-

function hio_fmt_stc_table, instruct, debug=debug, maxwid=maxwid, subset=subset, name_columns=name_columns

struct = instruct
tnames = tag_names(instruct)			; get tag names

;help,/st,struct

if keyword_set(subset) then begin
  struct = str_subset(instruct,tnames(subset))
endif

nrow = n_elements(struct)
ntg = n_tags(struct)
tg_nams = tag_names(struct)
;;print, tg_nams
print, strlowcase(tg_nams)
mlen = intarr(ntg)

;    define format for some named parameters - not set propely in HEC

f8p1 = ['PA','PA_WIDTH','LAT_HG','LONG_HG','LONG_CARR', $
	'V','V_INIT','V_FINAL','V_20R','ACCEL','PA_MEASURE', $
	'PROTON_FLUX','FE_MAGN','KPMAX','DST_MIN','B_MAX','VSW_MAX', $
	'AXY_MIN','AZ_R','TMIN','DC_MIN','TDMN','AFTOB','TILT']

;    step through the parameters assembling the format statments

ffmt = '(' & ffmt2 = ffmt & tcom=''

for j=0,ntg-1 do begin
  if j gt 0 then tcom=','
  mlen(j) = max(strlen(struct.(j)))

  typ = datatype(struct.(j))
  fmt = '' & fmt2 = ''

  if typ eq 'STR' then begin 
    fmt = '3x,a' & fmt2 = '3x,a'
    fmt = '3x, a'+strtrim(string(max([mlen(j),4]),format='(i2)'),2)
    fmt2 = fmt
    if strmid(tg_nams(j),0,5) eq 'TIME_' then begin			;?? what time format
;;      fmt2 = '3x,a19'
;      help, mlen(j)
      fmt2 = '3x,a' + strtrim(string(mlen(j), format='(i2)'),2)
;      print,fmt2
    endif
  endif
  if typ eq 'LON' then begin fmt = 'i8'   & fmt2 = 'x,a7' & endif
  if typ eq 'FLO' then begin
    fmt = 'g10.6' & fmt2 = 'x,a9'
;  if tg_nams(j) eq 'PA' or tg_nams(j) eq 'PA_WIDTH' then fmt = '8.1'
    for kk=0,n_elements(f8p1)-1 do if tg_nams(j) eq f8p1(kk) then fmt = 'f10.1'
  endif

;  print, tg_nams(j), typ, fmt, format='(a,2x,a,2x,a)'
  ffmt = ffmt + tcom+fmt
  ffmt2 = ffmt2 + tcom+fmt2
;  print,j,mlen(j)
endfor

ffmt = ffmt+')'
ffmt2 = ffmt2+')'


if keyword_set(debug) then begin
  print,ffmt
  print,ffmt2
  print,mlen
endif

;    construct the format statement, leaving 2 spaces between each column

ff = '(a'
for j=0,ntg-1 do ff = ff + ',t' + strtrim(string(fix(total(mlen(0:j)+2)+1)),2) + ',a'
ff = ff + ')'
;print,ff

ff = ffmt
;print,ff
 
if keyword_set(debug) then begin
  help,struct
  help,struct,/st
  print,ff                                                                   
  print,''
endif

nrow = n_elements(struct)
min_nbsp = 100000l
array = strarr(nrow)
for k=0,nrow-1 do begin
  array(k) = string(struct(k),format=ff)
  min_nbsp = min([min_nbsp,where(byte(array(k)) gt 32b)])
endfor

;    get rid of leading spaces in orderly way and max out length of string

for k=0,nrow-1 do begin
  temp = strmid(array(k),min_nbsp-2,strlen(array(k)))
  if keyword_set(maxwid) then maxlen=maxwid else maxlen=strlen(strtrim(temp,0))
  array(k) = strmid(temp,0,maxlen)
  array(k) = str_replace(array(k),'*',' ')		; null values end as ****..
endfor

;    add names in the first row

if keyword_set(name_columns) then begin
  names = tag_names(struct)
  header = string(names, format=ffmt2)
  array = [strmid(header, min_nbsp-2,strlen(header)), array]
endif

return,array
end
