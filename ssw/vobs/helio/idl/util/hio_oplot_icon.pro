;    Put HELIO logo on a plot
;    Default is the in bottom RH corner of frame

pro hio_oplot_icon, xp, yp

xpos = !d.x_size-50
ypos = 2

if n_params() eq 2 then begin
  xpos = xp & ypos = yp
endif

fgif = '$SSW_HELIO/doc/helio_icon.gif' 
read_gif,fgif,hio_icon,rr,gg,bb
help,hio_icon

tvlct,ro,go,bo,/get		; save colour table

tvlct,rr,gg,bb			; load colour table of logo
tv,hio_icon, xpos, ypos				;!d.x_size-50,2

tvlct,ro,go,bo			; restore colour table

return
end