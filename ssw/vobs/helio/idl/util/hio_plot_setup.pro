pro hio_plot_setup, invert=invert

csiz = 1.5	;2.0
lsiz = 1.0	;1.2

loadct,0,/silent

;  tvlct,255b-rr,255b-gg,255b-bb

if keyword_set(invert) then begin
  tvlct,rr,gg,bb,/get

;    save colour of the band in utplot_band
  gb_rr = rr(80) & gb_gg = gg(80) & gb_bb = bb(80)
;    flip colour table and reload table
  rr = 255b-rr & gg = 255b-gg & bb = 255b-bb
  rr(80) = gb_rr & gg(80) = gb_gg & bb(80) = gb_bb 

  tvlct,rr,gg,bb
endif

;    now set the line colours

linecolor,1,'red'
linecolor,2,'blue'
linecolor,3,'green'
linecolor,4,'yellow'

tvlct,rr,gg,bb,/get
rr(5)=100 & gg(5)=0 & bb(5)=100    ;make the purple we need...
tvlct,rr,gg,bb

end