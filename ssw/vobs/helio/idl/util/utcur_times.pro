; IDL Version 6.1.1, Mac OS X (darwin ppc m32)
; Journal File for robertbentley@Robert-Bentleys-Computer.local
; Working directory: /Users/rdb/Documents/new_stuff/rfr_200507
; Date: Sat Jul 30 11:40:05 2005

;    rdb  24-Mar-2011  Changed to xycursor

function utcur_times, debug=debug
 
;;common utcommon
@utcommon

message = ['Use cursors to set Time-range:', $
           'Left button to specify the Start Time', $
           'Right button to specify the End Time', $
           'Centre button to Exit']
box_message,message

!mouse.button = -1
csta = utbase+!x.crange(0)
cstp = utbase+!x.crange(1)

color = 255
tvlct,rr,gg,bb,/get
if rr(0) eq 255 then color=0

while !mouse.button ne 2 do begin
;;  cursor,x,y,/down
  xycursor, x,y, /data, button=b1, print=debug, color=color

  if keyword_set(debug) then print,x,!mouse.button. b1

  if !mouse.button eq 1 then csta = utbase+x
  if !mouse.button eq 4 then cstp = utbase+x
  fmt_timer,(anytim([csta,cstp],/ecs))
endwhile

times = anytim([csta,cstp],/ecs)
print,''
print,'* Selected Time-range'
fmt_timer,times

return,times
end
