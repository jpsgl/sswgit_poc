;+
; NAME:
;	descr_plot_limits
; PURPOSE:
;	Writes a file giving the scaling information of the last platted IDL image,  
;	for example a plot of intensity against time or a scaled image of the Sun. 
;
;	Information allows applications software to use cursors on the image; the values
;	returned by the cursor can be used to define a time or select a region of interest.
;
;	Two files are produced in stored in the same directory as the image:
;	.info  is read by the HELIO SSW routine "get_cxs_plot_info" and used in "hio_cxsimage_cursor"
;	.vers  can be read/executed to support cursors in PHP code
;	
; CALLING SEQUENCE:
;	descr_plot_limits, time_reference, xaxis_unit, yaxis_unit
;
;	descr_plot_limits, '2013-05-10:10:23:10', 'seconds', 'log intensity'
;
; INPUTS:
;	time_reference  String defining the refernce time of the plot or image
;	                Must be present - string should be in CCSDS format
;	xaxis_unit      String defining the units of the x-axis
;	                Must be present - included in file to aid interpretation
;	yaxis_unit      String defining the units of the y-axis
;	                Must be present - included in file to aid interpretation
; KEYWORDS
;	plotfile        Name of the output file
;	                If non supplied then defaults to "cplot"
;	xtrim           IDL sometimes puts excessive space either side on the x-axis
;	                This keyword allows user to specify pixels to trim [xtim0,xtrim1]
;	info            String contiaining an informaton record to include in the output file
;
; HISTORY:
;	   Apr-2003  rdb  First version
;	21-Sep-2013  rdb  Generalized, added comments.
;-

pro descr_plot_limits, time_reference, xaxis_unit, yaxis_unit, $
    xtrim=xtrim, plotfile=plotfile, info=info

;--------------------------------------------------
;    must have the three parameters supplied to work at all

if n_params() ne 3 then begin
  message,'NO Reference Time provided or UNIT type missing',/info
  return
endif else begin
  rtime = anytim(time_reference,/ecs)  
  rtime_var = anytim(time_reference,/stime,/trunc)
endelse

;--------------------------------------------------
;    establish the scale on the current window

xs = (!x.window(1)-!x.window(0))/(!x.crange(1)-!x.crange(0))
ys = (!y.window(1)-!y.window(0))/(!y.crange(1)-!y.crange(0))

;    find the limits on the current window in the x-axis

x0 = !x.crange(0)-!x.window(0)/xs
x1 = !x.crange(1)+(1-!x.window(1))/xs

;    allow for trimming of the size of the image in the x-axis

if keyword_set(xtrim) then begin
  x0 = (x1-x0)/!d.x_size*(0+xtrim(0))+x0
  x1 = (x1-x0)/!d.x_size*(!d.x_size-xtrim(1))+x0
endif

;    find the limits on the current window in the y-axis

y0 = !y.crange(0)-!y.window(0)/ys
y1 = !y.crange(1)+(1-!y.window(1))/ys

print, 'x0, y0 ,x1 ,y1: ', x0,y0,x1,y1

;--------------------------------------------------
;    get the image size

xsiz = !d.x_size
if keyword_set(xtrim) then xsiz = xsiz - xtrim(0) - xtrim(1)
ysiz = !d.y_size

print, 'xsiz, ysiz: ', xsiz,ysiz

;--------------------------------------------------
;    define the name of the output files

pfile = 'cplot.png'
if keyword_set(plotfile) then pfile=plotfile
;print,pfile
break_file, pfile, aa,bb,cc,dd
;;ofile = aa+bb+cc+'.info'

;--------------------------------------------------
;    code specific to the HELIO CXS
;    extract part of the image location that is the same internally and externally

ppfile = pfile
if strpos(pfile,'cea') gt 0 then $
  ppfile = strmid(pfile,strpos(pfile,'cea'),200)


;--------------------------------------------------
;    write info file used by IDL/SolarSoft

ofile = aa+bb+cc+'.info'
print,'Writing INFO file: ',ofile

file_append,ofile,/new, '# File describing limits of plot file:  '+ppfile
file_append,ofile, '# Written: '+systime()
if keyword_set(info) then file_append,ofile,'# '+info 
file_append,ofile, '#>>'

file_append,ofile, 'x0=  '+string(x0)
file_append,ofile, 'y0=  '+string(y0)
file_append,ofile, 'x1=  '+string(x1)
file_append,ofile, 'y1=  '+string(y1)

file_append,ofile, 'time_ref=  '+rtime

file_append,ofile, 'x_unit=    '+string(xaxis_unit)
file_append,ofile, 'y_unit=    '+string(yaxis_unit)


;--------------------------------------------------
;    write vars file used in PHP code/ Javascript

;	var imageWidth  = 550;
;	var imageHeight = 400;
;	var imagePhysicalTop    = 1.0;
;	var imagePhysicalBottom = 0.0;
;	var imagePhysicalLeft   = -10983.05;
;	var imagePhysicalRight  =  89694.92;
;	var imagetime = '16-Jul-2002 13:57';

ofile = aa+bb+cc+'.vars'
print,'Writing VARS file: ',ofile

file_append,ofile,/new, '// File describing limits of plot file:  '+ppfile
file_append,ofile, '// Written: '+systime()
if keyword_set(info) then file_append,ofile,'// '+info 
file_append,ofile, '// >>'

file_append,ofile, 'var imageWidth  = ' + strtrim(string(xsiz),2) + ';'
file_append,ofile, 'var imageHeight = ' + strtrim(string(ysiz),2) + ';'

file_append,ofile, 'var imagePhysicalLeft   = ' + strtrim(string(x0),2) + ';'
file_append,ofile, 'var imagePhysicalRight  = ' + strtrim(string(x1),2) + ';'
file_append,ofile, 'var imagePhysicalBottom = ' + strtrim(string(y0),2) + ';'
file_append,ofile, 'var imagePhysicalTop    = ' + strtrim(string(y1),2) + ';'

file_append,ofile, 'var imagetime = "' + rtime_var + '";'

file_append,ofile, '// x_unit=    '+string(xaxis_unit)
file_append,ofile, '// y_unit=    '+string(yaxis_unit)


end

