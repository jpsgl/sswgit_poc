function find_setup_file, setup_filename, database_dir=database_dir

;    cascade down through some directories looking for file

file=''
proj_dtb = '$HELIO_DTB'
if keyword_set(database_dir) then proj_dtb=database_dir

if n_params() lt 1 then begin
  message,'Need to specify name of set-up file'
  return, file		;??
endif

;    setup file is defined by an environmant variable
;;  file = get_logenv(my_presets)

;    if not defined, look in current directory
pfile = setup_filename
if file eq '' then if file_exist(pfile) then file = pfile

;    if still not defined, look in home directory
pfile = concat_dir('$HOME', setup_filename)
if file eq '' then if file_exist(pfile) then file = pfile

;    otherwise, cascade to system defined defaults
if file eq '' then file = concat_dir(proj_dtb, setup_filename)

return, file
end