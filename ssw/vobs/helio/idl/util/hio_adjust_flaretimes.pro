function hio_adjust_flaretimes, gev, verbose=verbose

message,'Using values defined in !hio_defs',/info
if keyword_set(verbose) then begin
  help,/st,gev
  help,/st,!hio_defs
endif

time_start = anytim(gev.time_start,/ecs)
time_end = anytim(gev.time_end,/ecs)
times = [time_start, time_end]

if tag_exist(gev,'time_peak') then begin
  time_peak = anytim(gev.time_peak,/ecs)

  delt0 = addtime(time_peak, diff=time_start)*!hio_defs.gev_mult_before
  delt0 = delt0 + !hio_defs.fl_buff_before
  delt1 = addtime(time_end, diff=time_peak)*!hio_defs.gev_mult_after
  delt1 = delt1 + !hio_defs.fl_buff_before

  ntime_start = fmt_tim(addtime(time_peak,delt=-delt0))
  ntime_end = fmt_tim(addtime(time_peak,delt=delt1))

  times = [ntime_start, ntime_end]
endif

if keyword_set(verbose) then begin
  fmt_timer,[time_start, time_end]
  fmt_timer,times
endif

return, times
end