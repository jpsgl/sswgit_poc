;+
; NAME:
;    
; PURPOSE:
;    
; CALLING SEQUENCE:
;    
; CATEGORY:
;    
; INPUTS:
;    
; INPUT KEYWORDS:
;    
; OUTPUTS:
;    
; OUTPUT KEYWORDS:
;    
; RESTRICTIONS:
;    
; HISTORY:
;    01-Apr-2011  rdb  written
;
;-

pro hio_recall_commands, nrecall=nrecall

nrec = 30
if keyword_set(nrecall) then nrec = nrecall

cc = recall_commands()

print,''
print,';-------------------------------------------------------------------'
print, reverse(cc(0:nrec-1)), format='(a)'
print,';-------------------------------------------------------------------'

end