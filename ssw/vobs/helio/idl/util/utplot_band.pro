; IDL Version 6.1.1, Mac OS X (darwin ppc m32)
; Journal File for robertbentley@Robert-Bentleys-Computer.local
; Working directory: /Users/rdb/Documents/new_stuff/rfr_200507
; Date: Fri Jul 22 18:37:58 2005
 
pro utplot_band, kinst, range=range, debug=debug

;common utcommon
utbase = getutbase()   ; no need for common declaration...

if keyword_set(debug) then begin
  print, range
  print, 'plot start: ',anytim(utbase,/ecs)
  print,'!x.crange',!x.crange
endif

;    determine no. of days to cover
;nd = fix((!x.crange(1)+12.5*60*60)/(24.*60*60))+2   ;was 1
nd = fix(!x.crange(1)/(24.*60*60)+3.) 
tr = intarr(7,nd)
ts = intarr(7,nd)

;    get the base date
exbase = anytim(utbase,/ex)
exbase(0:3) = 0		; ensure it is the day boundary
exbase = addtime(exbase, delt=-24*60.)  ; and back off a day
;    and add the offset times
tr(*,0) = addtime(exbase, delt=range(0))
ts(*,0) = addtime(exbase, delt=range(1))

;    replicate over the days
for j=1,nd-1 do begin
  tr(*,j) = addtime(fmt_tim(tr(*,0)),delt=24.*60*j)
  ts(*,j) = addtime(fmt_tim(ts(*,0)),delt=24.*60*j)
endfor

if keyword_set(debug) then begin
  print,nd
  print,tr,ts
endif

;    and plot
for j=0,nd-1 do begin
  tt = [fmt_tim(tr(*,j)),fmt_tim(ts(*,j))]
  if keyword_set(debug) then print, tt, format='(a,2x,a)'
  outplot, tt, [kinst-0.3,kinst-0.3]+1 ,color=80, thick=3
endfor

end
