;+
; PROJECT:
;        HELIO
; NAME:
;        hio_annotate_map
; PURPOSE:
;        Annotates a plot of a map object with the NOAA actuve region (NAR) numbers.
;        Also produces an image map fie with HTML links for each NAR so that the image can 
;        be used within a Web page
; CALLING SEQUENCE:
;        hio_annotate_map, map, xtrim=xtrim, soars=soars, outdir=outdir
; CATEGORY:
;        map
; INPUTS:
;        map          map object created from FITS image
; INPUT KEYWORDS:
;        xtrim        value usd to trims the image (left and right) before it is stored 
;        soars        sets the xtrim value to the value used for the SOARS codes
;        outdir       define the directory used for the image and map files
; OUTPUTS:
;        
; OUTPUT KEYWORDS:
;        
; RESTRICTIONS:
;        plot must already have been created with plot_map
; HISTORY:
;       Apr-2014  rdb  Extracted from other code
;    24-Oct-2014  rdb  Improved documentation
;
;-

pro hio_annotate_map, map, outdir=outdir, soars=soars, xtrim=xtrim

;if not keyword_set(nonar) then begin

;    for centre panel, image will be triimed later - need to subtract from map coords.

  img_xtrim = 0
  if keyword_set(soars) then img_xtrim = 50		
  if keyword_set(xtrim) then img_xtrim = xtrim
    
  out_frag = 'ar_coords.html'
  if keyword_set(outdir) then out_frag = concat_dir(outdir, out_frag)

;    always need to write header so that old records do not linger...

  file_append,out_frag,'<!-- active regions with and without spots -->',/new

;    first part of the URL to access SolarMonotor

  smurl = 'http://www.solarmonitor.org/region.php?date=' + time2fid(map.time,/full)
  print,smurl

;    for soars, get latest set of NAR values
;  if keyword_set(soars) then nar = strip_srs() else $
    nar = get_nar(map.time)
  help,nar & help,/st,nar

;****    really need to rotate locations to time of the image...

  if datatype(nar) eq 'STC' then begin

    qar = where(nar.region_flag ne 'II', nqar) & help,nqar
    if nqar gt 0 then begin

;    annotate image with NOAA AR No.

      oplot_nar, nar(qar), color=1, charsiz=csiz,charthick=2, $
               offset=[50,75], $         ;[100,50]
               /imap, imcircle=50, imagemap_coord=imagemap_coord, imars=imars
      help,imagemap_coord,imars
;      print,imagemap_coord & print,imars

;    write out each Web map record in turn, applying xtrim correction

      for jar=0,nqar-1 do begin
        print,imars(jar)
        print,nar(jar).location
        print,imagemap_coord(jar)

        xyr = str2arr(imagemap_coord(jar), ',')
        xyr(0) = xyr(0) - img_xtrim
        imagemap_coord(jar) = strtrim(arr2str(xyr),2)
        print,imagemap_coord(jar)

        head = '<area shape="circle" coords="' + imagemap_coord(jar) + '" '
        url = 'href="' + smurl + '&region=1' + imars(jar) + '"'
        map_rec = head + url + ' target="ar_look">'

        file_append,out_frag, map_rec
      endfor

    endif
  
  endif

;endif

return
end