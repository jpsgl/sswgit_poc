;+
; NAME:
;	load_hio_defaults
; PURPOSE:
;	Sets up default system values
;	Includes service URL and WGET/CUTL
; CALLING SEQUENCE:
;	load_hio_defaults
; INPUT KEYWORDS:
;	verbose			print additional information
; RESTRICTIONS:
;
; HISTORY:
;	   Apr-2011  rdb  written
;	19-Jul-2011  rdb  Made WGET default for copy  (except for Mac)
;
;-

pro load_hio_defaults, verbose=verbose

;    load some values related to event start/end and create sysvar

;    look for the set-up file
file = find_setup_file('helio_defaults.txt')

if keyword_set(verbose) then print,'Loading: ', file
qq = rd_tfile(file,/nocomm)

lqq = strlen(strcompress(qq))
qq = qq(where(lqq gt 0))
;help,qq

nq=n_elements(qq)
cc = '{'
for j=0,nq-1 do begin
  cline = strcompress(strtrim(str2arr(qq(j),';'),2))
;  print,str2arr(cline,':')
;  print,cline(0)
  if j eq 0 then cc = '{'+cline(0) else $
    cc = cc + ', ' + cline(0)
endfor
cc = cc +'}'

temp = execute('hio_defs='+cc)
;;help,hio_defs,/st

;  ?? define default set and copy??
;;if not exist(!hio_defs) then $
  defsysv,'!hio_defs', hio_defs

if keyword_set(verbose) then help,/st,!hio_defs

;---------------------------------------------------------

;    load the roots url's of all the services and create sysvar

;    look for the set-up file
file = find_setup_file('hio_service_roots.txt')

if keyword_set(verbose) then print,'Loading: ', file
qq=rd_tfile(file,nocomm='#')

nq=n_elements(qq)
cc = '{'
for j=0,nq-1 do begin
  cline = strcompress(strtrim(str2arr(qq(j),';'),2))
  croot = 'root_' + cline(0)
;  print,str2arr(cline,':')
;  print,cline(0)
  if j eq 0 then cc = '{'+croot else $
    cc = cc + ', ' + croot
endfor
cc = cc +', wget_flag:0}'
;print,cc

temp = execute('hio_sysvar='+cc)
defsysv,'!hio_sysvar', hio_sysvar

;---------------------------------------------------------

;    decide whether should use WGET or CURL for copying

wget_flag = 1						; default to WGET

if !version.os eq 'darwin' then wget_flag = 0		; use CURL on a Mac

temp = get_logenv('HIO_WGET_FLAG')			; env. var. overrides 
if strlowcase(strtrim(temp)) eq 'wget' then wget_flag=1
if strlowcase(strtrim(temp)) eq 'curl' then wget_flag=0

!hio_sysvar.wget_flag = wget_flag			; set the method

;---------------------------------------------------------

if keyword_set(verbose) then help,/st,!hio_sysvar


end
