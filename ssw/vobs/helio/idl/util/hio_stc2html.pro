function strtab2html, table_array, $
    cellspacing=cellspacing, cellpadding=cellpadding, 	border=border,	$
    spacing=spacing,         padding=padding,				$
    row0header=row0header, header=header, null_fill=null_fill, rules=rules, $
		 cwid=cwid, $
    right=right, left=left, center=center
;+
;   Name: strtab2html
;
;   Purpose: generate html Table (V3 table format) from table (string array)
;
;   Input Parameters:
;      table_array - 1D or 2D table (1D will be -> 2D)
; 
;   Optional Keyword Parameters:
;      cellpadding, cellspacing - per html table formatting spec.
;      padding, spacing		- synonyms for above
;      border			- per html table formatting spec.
;      row0header - switch, if set, use row0 values for header labels (bold)
;      right,left,center - switches - alignment of values in cells
;   
;   Calling Sequence:
;      table_html=strtab2html(table_array, /center, /right, /left, $
;    		                           border=NN, cellpad=NN, cellspace=NN)
;
;   Calling Examples:
;      table_html=strtab2html(strarr_1D)	; break into columns, -> html
;      table_html=strtab2html(strarr_2D)	; user table -> html
;      table_html=strtab2html(strarr_2D,cellspac=10,border=20)
;      table_html=strtab2html(strarr_2D,/row0head) ; use row 0 as header lables
;
;   History:
;      8-march-1996 S.L.Freeland
;      7-May-1996   S.L.Freeland - changed to keyword inheritance
;      9-May-1996   S.L.Freeland - force output to be 1D vector 
;     10-May-1996   S.L.Freeland - add NULL_FILL keyword (default to '-')
;     14-May-1996   S.L.Freeland - call 'strarrcompress' to remove null rows
;     15-May-1996   S.L.Freeland - remove keyword inheritance
;     23-Jul-1997   S.L.Freeland - dont call str2cols (assume user knows)     
;     19-jan-1998   S.L.Freeland - added missing /LEFT, /RIGHT, /CENTER KWs
;     08-Jul-2011   R.D.Bentley  - added the cwid and rules keywords, some rewoeking
;     14-Sep-2015   R.D.Bentley  - Fixed </tr>
;-
; ----------- check keywords and assign defaults ------------

if keyword_set(padding) then cellpadding=padding	; synonym
if keyword_set(spacing) then cellspacing=spacing	; synonym
if n_elements(cellpadding) eq 0  then cellpadding=5	; pad default
if n_elements(cellspacing) eq 0  then cellspacing=3	; space default
if n_elements(border)  eq 0      then border=5
if n_elements(rules)  eq 0       then rules='all'
case 1 of 						; set cell alignment
   keyword_set(right):  align='right'
   keyword_set(left):   align='left'
   else: 		align='center'			; default
endcase
; ---------------------------------------------------------------
stable=size(table_array)
;if stable(0) eq 1 then $			; 1D passed in - columnize it
;   columns=str2cols(table_array) else $        ; SLF - remove auto-str2cols

columns=table_array			; 2D (already columnized)

columns=strarrcompress(columns,/rows)		; remove null rows
stable=size(columns)
columns=strtrim(columns,2)
if n_elements(null_fill) eq 0 then null_fill='-'
null=where(columns eq '',null_cnt)
if null_cnt gt 0 then columns(null) = null_fill
; ---------------------------------------------------------------

wd = strarr(stable(1))
if n_elements(cwid) ne 0 then begin
  if n_elements(cwid) eq n_elements(wd) then wd = ' width='+strtrim(cwid,2)
endif
if keyword_set(row0header) then begin
   columns(*,0)='<th'+wd(*)+'>' + columns(*,0) + '</th>'		; label header cells
;;   columns(*,1:*)='<td>' + columns(*,1:*) + '</td>'	; label data cells 
;;endif else columns(*)='<td>' + columns(*) + '</td>'	; label data cells
endif else columns(*,0)='<td'+wd(*)+'>' + columns(*,0) + '</td>'	; label data cells

columns(*,1:*)='<td>' + columns(*,1:*) + '</td>'	; label data cells 

;columns(0,*)='<tr align=' + align + '>' + columns(0,*)  ; add row html

newcols = strarr(stable(1)+2,stable(2))				; add row html
newcols(1:stable(1),*) = columns
newcols(0,*) = '<tr align=' + align + '>'
newcols(stable(1)+1,*) = '</tr>'


; --------------- define table header html --------------------
head_html='<table border='      + strtrim(border,2) + $
	        ' rules='       + strtrim(rules,2) + $
                ' cellpadding=' + strtrim(cellpadding,2) + $
	        ' cellspacing=' + strtrim(cellspacing,2) + '>'
; ---------------------------------------------------------------

; --------------- now glue it all together ----------------
;;columns(0)=head_html + columns(0)
;;columns(n_elements(columns)-1)=$
;;   columns(n_elements(columns)-1) + '</table>

;;table_cols=reform(columns,n_elements(columns))		; make 1D vector
table_cols=reform(newcols,n_elements(newcols))		; make 1D vector
table_html=[head_html,table_cols,'</table>']
; ---------------------------------------------------------------

return, table_html
end


;+
; NAME:
;		hio_stc2html
; PURPOSE:
;		Writes structure out as Web page
; INPUTS:
;		struct	structure to be output
; INPUT KEYWORDS:
;		swap	switched rows and columns in output table
;		title	title to be used on page
;		file	output name of file - defaults to hh.html
; OUTPUTS:
;		writes HYML to file
; CALLING SEQUENCE:
;		hio_stc2html, struct, title=title  ,/swap  ,file=file
; RESTRICTIONS:
;		uses modified version of strtab2html
; HISTORY:
;		   Jun 2011  rdb  Written
;		23-Jul-2011  rdb  Added style sheet link
;		01-Feb-2012  rdb  removed call run rules - Firfox 9 does not display
;						  added trailer line after table
;		10-Mar-2013  rdb  fudge to fix the URL if it is supposed to be ftp://
;
;-

pro hio_stc2html, struct, swap=swap, file=file, title=title, trailer=trailer, cwid=cwid, $
			css_url=css_url, _extra=extra

ofil = 'hh.html'
if keyword_set(file) then ofil = file

ctitle = 'An array'
if keyword_set(title) then ctitle = title

;url_css = concat_dir(concat_dir(get_logenv('$SSW_HELIO'),'doc'),'spweather.css')
url_css = concat_dir(get_logenv('$HELIO_DTB'),'spweather.css')
if keyword_set(css_url) then url_css = css_url

help, struct
html = stc2html(struct, swap=swap, border=1, padding=3, cwid=cwid)		;, rules='cols,rows')

;    fudge to fix the URLs that should be ftp://
qv = where(strpos(html,'ftpxx-') ge 0, nhref)		; assumes have inserted a flag string
if nhref gt 0 then begin
  help, nhref
  for j=0,nhref-1 do html(qv(j)) = str_replace(html(qv(j)),'"http://ftpxx-','"ftp://')
endif

;    fudge to fix the URLs that should be relative to current
qv = where(strpos(html,'relxx-') ge 0, nhref)		; assumes have inserted a flag string
if nhref gt 0 then begin
  help, nhref
  for j=0,nhref-1 do html(qv(j)) = str_replace(html(qv(j)),'"http://relxx-','"')
endif

file_append, ofil, /new, '<!--  written by hio_stc2html, '+systime()+'  -->'
file_append, ofil, '<html>'
file_append, ofil, '<head>'
file_append, ofil, '<link href="'+url_css+'" rel="stylesheet" type="text/css">'
file_append, ofil, '<title>' + ctitle + '</title>'
file_append, ofil, '</head>'
file_append, ofil, '<body>'
file_append, ofil, '<h2>' + ctitle + '</h2>'
; file_append, ofil, '<font size=-2>'
file_append, ofil, html
; file_append, ofil, '</font>'
if keyword_set(trailer) then file_append, ofil, '<p> '+trailer
file_append, ofil, '</body>'
file_append, ofil, '</html>'

end