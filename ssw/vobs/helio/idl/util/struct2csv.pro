pro struct2csv, struct, file=file, delimiter=delimiter, $
		noquote=noquote, verbose=verbose

outfile = 'temp.csv'
if keyword_set(file) then outfile = file

nrow = n_elements(struct)
ntag = n_tags(struct)
tags = strlowcase(tag_names(struct))

;    from the tag names into a line
cc = tags(0)
for jtag=1,ntag-1 do cc=cc+','+strtrim(tags(jtag),2)
if keyword_set(verbose) then print,cc

;    save this line
file_append,outfile,/new,cc

quote = '"'
if keyword_set(noquote) then quote = ''
delim = ','
if keyword_set(delimiter) then delim = delimiter
print,'>>>> Using delimiter: "' + delim + '"'

;    now write out each row
for jrow=0,nrow-1 do begin
  crow = struct(jrow)
  cc = quote + strtrim(string(crow.(0)),2) + quote
  for jtag=1,ntag-1 do cc = cc + delim + quote + strtrim(string(crow.(jtag)),2) + quote
  if keyword_set(verbose) then print,cc
  file_append,outfile,cc
endfor

end