;   was at front of decode_votable

function vot_strsegs,tabledata,expr,count=count

;  routine similar to strsplit but low level and only returns pointers
;  this makes it much faster for very long strings... 

pntrs = lonarr(3,80000)

pos = -1L
for kpnt=0L,79999L do begin
  pos = strpos(tabledata,expr,pos+1)
  if pos lt 0 then goto, done
  pntrs (0,kpnt) = pos
endfor

done:
pntrs(0,kpnt) = strlen(tabledata)
pntrs(1,0:kpnt-1) = pntrs(0,1:kpnt)-1	;pntrs(0,0:kpnt-1)
pntrs = pntrs(*,0:kpnt-1)
pntrs(2,*) = pntrs(1,*)-pntrs(0,*)

count = kpnt

return, pntrs
end
