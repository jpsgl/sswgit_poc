; IDL Version 6.1.1, Mac OS X (darwin ppc m32)
; Journal File for robertbentley@Robert-Bentleys-Computer.local
; Working directory: /Users/rdb/Documents/other_new/SOARS/idl
; Date: Sun Apr 16 19:00:11 2006
 
pro set_mpan, initialize=initialize

common save_mpan, plims, count

if n_elements(plims) eq 0 or keyword_set(initialize) then begin
  ;help,!d,/st
  xwin = !d.x_size  &  ywin = !d.y_size
;;  pwd = 415  &  pht = 150
  pwd = !d.x_size-75  &  pht = 150

;   !p.position parameter order:  x0, y0, x1, y1
  plims = fltarr(4,4)

  xlim = (45.+[0,pwd-1])/xwin
  plims(0,*) = xlim(0)  &  plims(2,*) = xlim(1)

  for j=0,3 do begin
    ylim = (40. + j*(pht+5)+[0,pht])/ywin
    plims(1,j) = ylim(0) & plims(3,j) = ylim(1)
  endfor

  ;print,plims
  count = -1

  ;;!p.multi=0
  !noeras=1

  return
endif

count=count+1

!p.position = plims(*,3-count)
;print,'Plot position:',!p.position

if count(0) gt 3 then count = 0

end