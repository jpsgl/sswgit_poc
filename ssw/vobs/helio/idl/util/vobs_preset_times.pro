;+
; NAME:
;       vobs_preset_times
; PURPOSE:
;       Read predefined time intervals and allow user selection
;       Used by various VOBS routines
; CALLING SEQUENCE:
;       result = vobs_preset_times(action [/force])
;
;       Cascades down through possible locations for the file:
;         first looked to see if file is define by env. variable MY_VOBS_PRESETS
;         if not it looks in the current directory for file 'vobs_presets.txt'
;         if not found it looks for this file in the users home directory
;         if still not defined, uses the file stored under $proj_DTB
;           where proj is HELIO (def.) or EGSO
;
;        For EGSO these names are 'MY_EGSO_PRESETS' and 'egso_api_presets.txt'
;
; INPUTS
;       note
; KEYWORDS INPUTS:
;       force           table held in common if already input
;                       /force forces it to read the table
;       egso            if set, set proj to EGSO
; OUTPUTS:
;       result          2 elements string defining time interval
;                       -1 if no file found and nothing defined
;       action          character defining what the interval represents
;                       (I=interval, F=flare)
; RESTRICTIONS:
;       none
; HISTORY:
;          Aug-2005  rdb  written for EGSO
;       14-Feb-2007  rdb  added search in current and users home directories
;       15-Mar-2011  rdb  Generalize to vobs
;
;-

function vobs_preset_times, action, helio=helio, egso=egso, $
                force=force, debug=debug

common save_presets, presets, file

action = ''	;default return

if n_elements(presets) eq 0 or keyword_set(force) then begin

  my_presets = 'MY_VOBS_PRESETS'
  presets_name = 'vobs_presets.txt'
  proj_dtb = '$HELIO_DTB'

  if keyword_set(helio) then begin
    my_presets = 'MY_EGSO_PRESETS'
    presets_name = 'egso_api_presets.txt'
    proj_dtb = '$EGSO_DTB'
  endif

;    allow user to select a predefined time intervals
  file = get_logenv(my_presets)

;    look for the set-up file
  if file eq '' then file = find_setup_file(presets_name,database_dir=proj_dtb)
  goto, xxxx

;    if not defined, look in current directory
  pfile = presets_name
  if file eq '' then if file_exist(pfile) then file = pfile

;    if still not defined, look in home directory
  pfile = concat_dir('$HOME', presets_name)
  if file eq '' then if file_exist(pfile) then file = pfile

;    otherwise, default to system defined intervals
  if file eq '' then file = concat_dir(proj_dtb, presets_name)

xxxx:
  print,'Loading: ', file
  qq=rd_tfile(file,delim='|',/aut, nocomment='#')
  temp = {type:'',t1:'',t2:'',title:'',times:''}
  presets = replicate(temp,(size(qq,/dim))(1))
  for j=0,n_elements(presets)-1 do for k=0,4 do presets(j).(k)=qq(k,j)

endif

if keyword_set(debug) then begin
  help,file
  help,presets
  help,/st,presets
endif

preset_recs = fmt_stc_table(presets)
;qsel = wmenu_sel(preset_recs+'   ', /one)
qsel = hio_xmenu_sel(preset_recs+'   ', /one,size=14, $
	title='vobs_presets: Time intervals covering interesting events, etc')

if qsel(0) ge 0 then begin
  print, 'Selected preset:   ',presets(qsel).title
  print,''
  action = presets(qsel).type
  status = execute('times='+presets(qsel).times)
  return, times

endif else begin
  message,'No preset time range selected',/cont
  return, -1

endelse

end