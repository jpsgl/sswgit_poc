function stc2html, struct, swap=swap, border=border, rules=rules, padding=padding, cwid=cwid

;    good values are:
;      border=1, rules='cols,rows'

tags = tag_names(struct)
print,tags

ncols = n_elements(tags)
nrows = n_elements(struct)
;help,nrows,ncols

out = strarr(ncols,nrows+1000)
;;+'<font size=-5>&nbsp;</font>'		;'<small>&nbsp;</small>'

crow = strarr(ncols)

krow = 1		; in case need to insert things....

for jrow=0,nrows-1 do begin
;  print,krow
  for jcol=0,ncols-1 do begin
    temp = strtrim(string(struct(jrow).(jcol)),2)
    if n_elements(temp) gt 1 then temp = arr2str(temp,', ')
    if strpos(temp,'http') eq 0 then $
      temp = '<a href="'+temp+'">Link</a>'
    if temp eq '' then temp = '&nbsp;'
    crow(jcol) = temp
  endfor
  out(*,krow) = crow
  krow++
endfor
out(*,0)='<small>'+tags+'</small>'

help,krow
out=out(*,0:krow-1)
help,out

;    switch between the columns and rows
;    useful for a structure with a few instances but many parameters

if keyword_set(swap) then out = rotate(out,4)

html = hio_strtab2html(out, border=border, rules=rules, padding=padding, cwid=cwid)   ;border=1, rules='cols,rows')

return, html
end
