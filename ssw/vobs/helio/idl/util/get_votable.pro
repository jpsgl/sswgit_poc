;+
; NAME:
;	get_VOTable
; PURPOSE:
;	Wrapper around decode_VOTable for the case where to table is 
;	read from a file.
; CALLING SEQUENCE:
;	act = get_votable(votable_filename)
;
; INPUTS:
;	votable_filename	name of file to be read
; KEYWORDS
;	quiet	removes some debug messages
; HISTORY:
;        2 Dec 2003  RDB  First version
;	23-aug-2004  rdb  read bytes instead of strings to increase speed
;	04 Nov 2004  rdb  decode_votable code now completely separate
;   22-Dec-2013  rdb  handke case where STILTs votable uses <TD/>
;-

function get_votable,votable_filename,quiet=quiet

break_file,votable_filename,aa,bb,cc,dd
print,'VOTable file:  ',cc+dd

;  Make sure it appears as a continous string - works and more complex case...

openr,lun,votable_filename,/get_lun
res=fstat(lun)
kk=bytarr(res.size)
readu,lun,kk
free_lun,lun

;  get rid of CR and LF
wn_crlf = where(kk ne 10b and kk ne 13b)
vot_string = string(kk(wn_crlf))

;  STILTs votable defines empty cell as <TD/>; replace with <TD></TD>
if strpos(vot_string,'<TD/>') gt 0 then $
  vot_string = str_replace(vot_string,'<TD/>','<TD></TD>')

goto, andmore

vot_string = rd_tfile(votable_filename)

;  use strjoin rather than arr2str which is very slow...
if n_elements(qq) gt 1 then vot_string = strjoin(vot_string,/single)

andmore:

vv = decode_votable(vot_string, quiet=quiet)

return,vv
end
