pro xcom_showpng, png_file, title=title, wnum=wnum

;    read a png file and display image

read_png, png_file, img, rr,gg,bb

sz = size(img)
xz = sz(1) & yz = sz(2)

window, xsize=xz,ysize=yz, title=title, /free

tvlct, rr,gg,bb
tv,img

wnum = !d.window                 

end