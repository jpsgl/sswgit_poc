function hio_do_cadence, struct, timerange, use_cadence=use_cadence, label=label

  dofor = 'NOT defined'
  if keyword_set(label) then dofor = label
  dofor = ' for ' + dofor
;  print,''
  message, '>>>> Starting Cadence routine' + dofor, /info
  
  dt = 5					; time window - nins. either side of desired time 
  nph = 1					; default no. images per hour

  sdate = timerange(0)
  edate = timerange(1)
  ndays = addtime(edate,diff=sdate)/60./24.
;  help, ndays

  
  if keyword_set(use_cadence) then nph = use_cadence		; modified frame rate
  nmin = 60./nph			; cadence in minutes
  ncad = ndays*24*nph		; total number of images
;  help, nph, ncad

  idx = intarr(ncad)-1

  dsec = addtime(struct.time_start, diff=sdate)
;  help,dsec
;  print,dsec
  
  for j=0,ncad-1 do begin
    cdsec = abs(dsec - nmin*j)
;    cdsec = dsec + nmin*j
;    print,cdsec
    px = where(cdsec eq min(cdsec))
    idx(j) = px(0)
  endfor

  qwok = where(idx ge 0, nqwok)
  
  if nqwok gt 0 then begin
    message,'No. of images matching cadence:'+string(nqwok),/cont
    twok5 = idx(qwok)
    twok5 = all_vals(twok5)

  endif else begin
    message,'No files fall in time range',/cont

  endelse

return, twok5
end