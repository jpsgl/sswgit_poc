;+
; NAME:
;	mget_VOTable
; PURPOSE:
;	Wrapper around decode_VOTable for the case where to table is 
;	read from a file.
; CALLING SEQUENCE:
;	act = mget_votable(votable_input, string=string)
;
; INPUTS:
;	votable_input	name of file to be read if /string not set
;			string containing votable is /string set 
; KEYWORDS
;	quiet	removes some debug messages
; HISTORY:
;          Nov 2010  RDB  First version
;       11 Jul-2011  rdb  MDES updates
;       16 Aug 2014  rdb  Made count in report long
;-

function mget_votable, votable_input, string=string, quiet=quiet, $
		pointers=pntrs, num_rec=num_rec, report=report, verbose=verbose

;help, verbose

;  Make sure it appears as a continous string - works and more complex case...

ntab = -1

if not keyword_set(string) then begin

;    Read VOTable from a file

  break_file,votable_input,aa,bb,cc,dd
  message,'Reading VOTable file:  '+cc+dd,/info
  openr,lun,votable_input,/get_lun
  res = fstat(lun)
  kk = bytarr(res.size)
  readu,lun,kk
  free_lun,lun

;  get rid of CR and LF
  wn_crlf = where(kk ne 10b and kk ne 13b)
;  help,kk,wn_crlf
  vot_string = string(kk(wn_crlf))

endif else begin

;    Input is supplied as a string via the input parameter

  message,'Using string input',/info
  print,''
;    use strjoin rather than arr2str which is very slow...
  vot_string = votable_input
  if n_elements(votable_input) gt 1 then vot_string = strjoin(votable_input,/single)

endelse

goto, andmore

vot_string = rd_tfile(votable_filename)

;  use strjoin rather than arr2str which is very slow...
if n_elements(qq) gt 1 then vot_string = strjoin(vot_string,/single)

andmore:

vv = decode_votable(vot_string, quiet=quiet, ntab=ntab, info=kinfo)

if datatype(vv) ne 'STC' then begin
  help, vv
  message,'NOT a VOTABLE',/info
  return, -1
endif

if datatype(kinfo) ne 'STC' then begin
  help, vv, kinfo
  message,'NO info records',/info
  goto, noinfo
endif

;;if strpos(strlowcase(kinfo.description), 'exception') ge 0 then begin
;;  for j=0,n_elements(kinfo.info)-1 do print, kinfo.info(j)
;;endif 

if keyword_set(verbose) then begin
;  pp = where(kinfo.info.name eq 'HELIO_INSTRUMENT_NAME',npp)
  pp = where(kinfo.info.name eq 'QUERY_URL',npp)
  if npp eq 1 then print,kinfo.info(pp(0)).value
endif
;;print,pp,npp

noinfo:

;    need to consider how to keep HEAP pointers tidy <<<<<<<
pntrs = ptr_new(vv)

;    return structures as heap variables - more flexible

nel = n_elements(vv)
if datatype(vv) eq 'STR' and nel eq 1 then nel=0
;help, nel
if nel eq 0 and keyword_set(verbose) then begin
  message,'>>>>  VOTable did not return values  <<<<',/info
  print, kinfo.description
  for j=0,n_elements(kinfo.info)-1 do print, kinfo.info(j)
endif else begin
  coinst = kinfo.info(2).value
  carch  = str_replace(kinfo.description,' query response',':')
  print, coinst, carch, format='(a,t30,a)'
endelse

num_rec = nel

if datatype(kinfo) ne 'STC' then goto, noinfo2

report = replicate({obsinst:'',count:0L},ntab)

pp = where(kinfo.info.name eq 'HELIO_INSTRUMENT_NAME',npp)
if npp eq 1 then begin
  report(0).obsinst = kinfo.info(pp(0)).value
  report(0).count = nel

endif else begin
  box_message,'>>>> VOTable not properly formed - GUESSING !!!!'
  guess_obsinst = gt_brstr(vot_string,'INSTRUMENT=',']')
  oi = str2arr(guess_obsinst,delim=',')
  print,oi
  if n_elements(oi) eq n_elements(report) then begin
    report.obsinst = oi
    report(0).count = nel
  endif
endelse

noinfo2:

help,ntab
if ntab gt 1 then begin

  for jtab=1,ntab-1 do begin
  
    print,''
    message,'Working on Resource No.:'+string(jtab),/info
    
    cvv = 'ww'+string(jtab,format='(i2.2)')
    ss = cvv+' = decode_votable(vot_string, quiet=quiet, seltab=jtab, info=kinfo)'
;    if keyword_set(verbose) then print,'><><>: ',ss 
    ;; & help,execute(ss)
    vret = execute(ss)
;    if keyword_set(verbose) then help,vret
;    help, kinfo

if datatype(kinfo) ne 'STC' then goto,noinfo3
    
;    if keyword_set(verbose) then begin
;      pp = where(kinfo.info.name eq 'QUERY_URL',npp)
;      if npp eq 1 then print,kinfo.info(pp(0)).value else box_message,'** NO QUERY_URL **'
;    endif
    pp = where(kinfo.info.name eq 'HELIO_INSTRUMENT_NAME',npp)
    if npp eq 1 then report(jtab).obsinst = kinfo.info(pp(0)).value

noinfo3:

    if vret eq 0 then begin
      message,'>>>> **** VOTable did not return values, set null ****',/info
      vret1 = execute(cvv+'=-1')
      
    endif else begin
      ss = 'dtyp = datatype('+cvv+')'
;      if keyword_set(verbose) then print,'><><>: ',ss 
      vret2 = execute(ss)
;      if keyword_set(verbose) then help, dtyp
      
      ss = 'nel = n_elements('+cvv+')'
;      if keyword_set(verbose) then print,'><><>: ',ss 
      vret3 = execute(ss)
      if dtyp eq 'STR' and nel eq 1 then nel=0
;      if keyword_set(verbose) then help, nel
      
      if nel eq 0 and keyword_set(verbose) then begin
        message,'>>>>  VOTable did not return values  <<<<',/info
        print, kinfo.description
        for j=0,n_elements(kinfo.info)-1 do print, kinfo.info(j)
;;      endif else print, kinfo.info(2), kinfo.info(4)
      endif else begin
        coinst = kinfo.info(2).value
        carch  = str_replace(kinfo.description,' query response',':')
        print, coinst, carch, format='(a,t30,a)'
      endelse

      num_rec = num_rec+nel
      if datatype(kinfo) eq 'STC' then report(jtab).count = nel
      
    endelse
    
;;    ww = decode_votable(vot_string, quiet=quiet, seltab=jtab)
;;    help,/st,ww

;;    pntrs = [pntrs, ptr_new(vv)]
    ss = 'pntrs = [pntrs, ptr_new('+cvv+')]'
;    if keyword_set(verbose) then print,'><><>: ',ss 
    vret4 = execute(ss)
    
  endfor
  
endif

print,''
help,pntrs

;    how should the tables be returned?????

return,vv
end
