;+
; PROJECT:
;        HELIO
; NAME:
;        
; PURPOSE:
;        
; CALLING SEQUENCE:
;        
; CATEGORY:
;        
; INPUTS:
;        
; INPUT KEYWORDS:
;        
; OUTPUTS:
;        
; OUTPUT KEYWORDS:
;        
; RESTRICTIONS:
;        
; HISTORY:
;        01-Apr-2011  rdb  written
;
;-

;    copies a remote file to a designated output directory
;    optionally can name the ouput file differently to remote name

;    11-06-2011  rdb  Added -O field in WGET
;    20-07-2011  rdb  Took it out, but added -P
;    22-07-2011  rdb  Put -O back, remove -P - problems renaming the file
;    04-02-2012  rdb  Added -N to WGET


function wget_copy, url, raw=raw, verbose=verbose, $
		    outdir=outdir, outfile=outfile, file_uri=file_uri

if n_params() ne 1 then begin
  message,'Usage: wget_copy, url, outdir=outdir',/info
  message,'Wrong number of parameters'
endif

do_wget = 0
if !hio_sysvar.wget_flag then do_wget=1

;    get the remote file name

break_file,url, aa,bb,cc,dd

;    form local filename and where it is stored 

ofile = cc+dd
if keyword_set(outfile) then ofile = outfile

file_uri = ofile
if keyword_set(outdir) then file_uri = concat_dir(outdir,ofile)
if keyword_set(verbose) then help, file_uri

;    assemble command dependant on whether WGET or CORL

if not do_wget then begin
  cmd = 'curl -s '
  if keyword_set(outdir) then cmd = cmd + '-o ' + file_uri

endif else begin			; other Linux
  cmd = 'wget -N -q '
  if keyword_set(outdir) then cmd = cmd + ' -O ' + file_uri
endelse

if not keyword_set(raw) then cmd = cmd + ' ' + url $
    else cmd = cmd + ' '

if keyword_set(verbose) then print,cmd

return, cmd
end

;+
; NAME:
;	wget_post
; PURPOSE:
;	Constructs a command to do a POST type request using WGET or CURL
; INPUTS:
;	None
; OUTPUTS:
;	cstring		command string
; METHOD:
;	Syntax of command formed according to selected command WGET or CURL
;	Determines which to use according to to env. variable (HIO_WGET_FLAG)
;	whose value is loaded into !hio_sysvar.wget_flag when starting
;	Defaults to WGET  (CURL on under Mac OS-X)
; CALLING SEQUENCE:
;	cstring = wget_post() + post_command + url
;
;	where post_command is the command to be used in a request to url
; RESTRICTIONS:
;
; HISTORY:
;	July 2011  rdb  written
;
;-

function wget_post, dummy

;    form command to post a request related to HTML form

do_wget = 0
if !hio_sysvar.wget_flag then do_wget=1

if not do_wget $
  then cmd = 'curl -s --data '  $  		; Mac OS?
  else cmd = 'wget -q -O - --post-data='	; other Linux

return, cmd
end


;+
; PROJECT:
;        HELIO
; NAME:
;        
; PURPOSE:
;        
; CALLING SEQUENCE:
;        
; CATEGORY:
;        
; INPUTS:
;        
; INPUT KEYWORDS:
;        
; OUTPUTS:
;        
; OUTPUT KEYWORDS:
;        
; RESTRICTIONS:
;        
; HISTORY:
;        01-Apr-2011  rdb  written
;        xx
;        20-Mar-2016  rdb  added the tries and insecure keywords; much reworking
;
;-

function wget_stream, use_timeout=use_timeout, notimeout=notimeout, tries=tries, $
			insecure=insecure, curl=curl

;    cmd = 'wget --read-timeout=5 --tries=1 -qO- --no-check-certificate'
;    cmd = 'curl --max-time 5 --retry 3 -s -k '

do_wget = 0												; default to CURL
if !hio_sysvar.wget_flag then do_wget=1
if keyword_set(curl) then do_wget = 0					; force curl for just this call

; default was:  timeout_str = '--tries=1 --read-timeout=5 '
;    should these be parameters in !hio_sysvar???

;    set default timeout - curl does not have a default!
;    Note: this time could depend on the user's network connection bandwidth

val = 5													; default timeout (secs)
;timeout_str = ''
if keyword_set(use_timeout) then $
  if valid_num(use_timeout, pval, /int) then val = pval(0)
val = strtrim(string(val),2)
timeout_str = '--timeout='+val+' '
if not do_wget then timeout_str = '--max-time '+val+' '

if keyword_set(notimeout) then timeout_str = ''

;    set the number of times the transfer will be retried

val = 1													; default number of tries
;tries_str = ''
if keyword_set(tries) then $
  if valid_num(tries, pval, /int) then val = pval(0)
val = strtrim(string(val),2)
tries_str = '--tries='+val+' '
if not do_wget then tries_str = '--retry '+val+' '

insecure_str = ''
if keyword_set(insecure) then begin
  insecure_str = '--no-check-certificate '
  if not do_wget then insecure_str = '-k '
endif

;    form command so will return as stream to terminal rather than a file

if not do_wget $
  then cmd = 'curl -s ' + insecure_str + timeout_str + tries_str  $
  else cmd = 'wget -qO- ' + insecure_str + timeout_str + tries_str
;;  else cmd = 'wget ' + timeout_str + '-qO- '				; other Linux??

return, cmd
end


;--------------------------------------------------------------------

;    dummy to allow compilation...

pro hio_wget_routines, dummy

end