pro hio_set_prec_prime

common xxsave_prec, prec_save, prec_prime_window

cwindow = fix(!d.window)
if n_elements(prec_prime_window) gt 0 then begin
  print, 'Setting prec_prime_window as ',cwindow
  prec_prime_window = cwindow
endif else message,'>>>> prec_save has not been set up yet',/info

end

function hio_get_prec, name

common xxsave_prec, prec_save, prec_prime_window

qp = where(prec_save.name eq name, nqp)
help, nqp

prec = prec_save(min(qp))
help, prec

return, prec
end


;    2014-10-05  rdb  Original version written
;    2017-01-24  rdb  More fully implemented


;;    if window is replotted with different size & limits, how do we remove the old record?????
;;    it really should overwrite - compare name and title? window no.?

;;    save index of record in the object? - too decoupled...

;    First prime-capable time plot becomes prime; superseded if another prime-capable time plot used
;    If changed made in prime time plot window, update its stored parameters

;    Which window is prime is stored in prec_prime_window
;    Should this be set inside or outside of the plot routine itself?

;    In the cursor routine, always reset window to the prime time plot window - i.e. cursors default there
;    Specifically request other window if desired for cursors using switch on cursor call 

;    Cannot use cursor until a prime has been established? Required to do a plot anyway!!

;    Behaviour if prec_prime_window is not set ?????????

pro hio_save_prec, name, ptitle, show=show, reset=reset, set_prime=set_prime

common xxsave_prec, prec_save, prec_prime_window

temp = {name:'', window:-1, title:'', xrange:fltarr(4), yrange:fltarr(4), position:fltarr(4)}

if n_elements(prec_save) eq 0 or keyword_set(reset) then begin
  print,'creating prec'
  prec_save = replicate(temp, 5)
  prec_prime_window =  -1
endif

;    /show just lists what is stored
if keyword_set(show) or keyword_set(reset) then begin
  for j=0,n_elements(prec_save)-1 do print, prec_save(j)
  return
endif

;  possible names include:  
;    tplot      time plot using goes, etc.
;    parker     parker spiral plot
;    filelist   plot showing when observations were made

prec = temp
prec.name = name					; named purpose of the window
prec.window = !d.window				; window number
prec.title = ptitle					; title naming the window (if supplied)
prec.position = !p.position			; several system plot parameters
prec.xrange = !x.crange
prec.yrange = !y.crange
help, prec

;    check if an identical record already exists

for j=0,n_elements(prec_save)-1 do begin
  idiff = str_diff(prec_save(j),prec)
  if idiff eq 0 then begin
    message,'Identical record for this Window has already been saved', /info
;    what if want to make this the prime??????
    return						   ; <<<<<<?????
  endif
;  check for windows with same name and title but other things have changed? Window no.?
endfor

;    min could cause problems if window deleted and added afresh????
;    should they be time tagged?
qp = min(where(prec_save.window lt 0, nqp))
help, nqp
if nqp ge 1 then begin
  prec_save(qp) = prec
  if keyword_set(set_prime) then prec_prime_window = prec.window
  
endif else message,'****** NO Window slots AVAILABLE', /info

end