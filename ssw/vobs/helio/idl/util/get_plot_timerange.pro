function get_plot_timerange, dpas_return,  verbose=verbose, $
	duration_limit=duration_limit, dwindow=dwindow

;    try to define time range of plot based of observations with duration
;    that is less than a defined limit  (defult is 300 sec)

;    dwinndow		Amount to add at either end
;    duration_limit	threshold of observation duration (tend=tstart)

;    11-Mar-2011  rdb  Written
;    26-Mar-2017  rdb  Switched from /quiet to /verbose

tlim = 300	; default to 300 seconds
if keyword_set(duration_limit) then tlim = duration_limit

dwin = 60
if keyword_set(dwindow) then dwin = dwindow

dur = long(addtime(dpas_return.time_end,diff=dpas_return.time_start)*60)
qdur = where(dur lt tlim, nqdur)

if nqdur gt 0 then  begin
  fmt_timer,dpas_return(qdur).time_start,ts0,ts1, /quiet
  fmt_timer,dpas_return(qdur).time_end,te0,te1, /quiet
endif else begin
  fmt_timer,dpas_return.time_start,ts0,ts1, /quiet
  fmt_timer,dpas_return.time_end,te0,te1, /quiet
endelse

;;trange = anytim([addtime(ts0,delt=-dwin), addtime(te1,delt=dwin)] ,/ccs,/trunc)

trange = [fmt_tim(addtime(ts0,delt=-dwin)),fmt_tim(addtime(te1,delt=dwin))]

if keyword_set(verbose) then fmt_timer,trange

return, trange
end
