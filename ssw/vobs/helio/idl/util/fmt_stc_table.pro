;+
; NAME:
;       fmt_stc_table
; PURPOSE:
;       Formats structure into string array so that it can be "printed"
; CALLING SEQUENCE:
;       result = fmt_stc_table(struct)
;
; INPUTS:
;       struct       IDL structure
; OUTPUTS:
;       result       formatted string array - tabular version of structure contents
; KEYWORDS:
;       subset       subset of tags names to be used (by tag index no.) 
;       maxwid       maximum width of table row  (i.e. max. length of string) 
;       name_columns if set, include the column names in the first row
;
; METHOD:
;       Establishes max length of field in each column and the assembles table,
;
; HISTORY:
;          May-2005  rdb  Written by Bob Bentley (MSSL/UCL)
;       16-Jun-2005  rdb  added /subset keyword 
;       17-Jun-2005  rdb  add names of columns
;
;-

function fmt_stc_table, instruct, debug=debug, maxwid=maxwid, subset=subset, name_columns=name_columns

struct = instruct
if keyword_set(subset) then begin
  tnames = tag_names(instruct)
  struct = str_subset(instruct,tnames(subset))
endif

ntg = n_tags(struct)
mlen = intarr(ntg)
for j=0,ntg-1 do begin
  mlen(j) = max(strlen(struct.(j)))
;  print,j,mlen(j)
endfor

;  construct the format statement, leaving 2 spaces between each column
ff='(a'
for j=0,ntg-1 do ff = ff + ',t'+strtrim(string(fix(total(mlen(0:j)+2)+1)),2)+',a'
ff=ff+')'
 
if keyword_set(debug) then begin
  help,struct
  help,struct,/st
  print,ff                                                                   
  print,''
endif

nrow = n_elements(struct)
min_nbsp=100000l
array = strarr(nrow)
for k=0,nrow-1 do begin
  array(k) = string(struct(k),format=ff)
  min_nbsp = min([min_nbsp,where(byte(array(k)) gt 32b)])
endfor

; get rid of leading spaces in orderly way and max out length of string
for k=0,nrow-1 do begin
  temp = strmid(array(k),min_nbsp-2,strlen(array(k)))
  if keyword_set(maxwid) then maxlen=maxwid else maxlen=strlen(strtrim(temp,0))
  array(k) = strmid(temp,0,maxlen)
endfor

if keyword_set(name_columns) then begin
  names = tag_names(struct)
  header = string(names, format=ff)
  array = [strmid(header, min_nbsp-2,strlen(header)), array]
endif

return,array
end
