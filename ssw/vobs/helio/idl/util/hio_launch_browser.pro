
;    create temporary web page of JMAPs by replacing strings in a template

; http://www.ukssdc.ac.uk/solar/stereo/movies/MOVIES/
; 2010/04_April/201004_hiB_jmap_ecliptic.jpeg

function make_ukssdc_jmap_page, time

;>>>>> needs to check date is after start of STEREO

tt = anytim2ex(time)

;    create the field that are dependant on the date

subdd = string(tt([6,5]),(list_months())(tt(5)-1),tt([6,5]), $
		format='(i4,1h/,I2.2,1h_,a,1h/,i4,i2.2,1h_)')

root = 'http://www.ukssdc.ac.uk/solar/stereo/movies/MOVIES/'

jmap_A = 'hiA_jmap_ecliptic.jpeg'
jmap_B = 'hiB_jmap_ecliptic.jpeg'

jmap_img1 = root+subdd+jmap_A
jmap_img2 = root+subdd+jmap_B

jmap_date = string((list_months())(tt(5)-1),tt(6),format='(a,x,i4.4)')

;    read the file, replace the strings and write it to temp

outfile = 'ukssdc-jmap_' + string(tt([6,5]),format='(i4,i2.2)') + '.html'

qq = rd_tfile('$HELIO_DTB/ral_jmap_template.html')
qq = str_replace(qq,'jmap_img1',jmap_img1)
qq = str_replace(qq,'jmap_img2',jmap_img2)
qq = str_replace(qq,'jmap_date',jmap_date)

ofile = concat_dir(get_temp_dir(),outfile)
file_append,ofile,/new,qq

return, ofile
end


;    possible URLs
;
;    http://www.solarmonitor.org/index.php?date=20050701
;    http://www.solarmonitor.org/region.php?date=20050701&region=10781
;    http://www.mssl.ucl.ac.uk/~rdb/soars/soars_FlareLocations.gif
;
;    http://helioviewer.org/index.php?date=2011-06-01T00:00:00Z&imageScale=8
;    http://cdaw.gsfc.nasa.gov/CME_list/daily_plots/sephtx/1998_05/sephtx_19980502.png
;    http://cdaw.gsfc.nasa.gov/CME_list/daily_plots/dsthtx/2009_08/dsthtx_20090830.html

;+
; NAME:
;		hio_launch_browser
; PURPOSE:
;		Launch Web browser for supplied URL or according to switch
; INPUT KEYWORDS:
;		date		start date  (?? end)
;		use_url		explicit URL to use   (e.g for CACTUS site)
;		solarmonitor	solar monitor page for supplied date
;		stereo_movie	GSFC CDAW movie maker for supplied date (+24hrs)
;		helioviewer		Helioviewer Web page for supplied date
;				see inside for possible options
; OUTPUTS:
;		none
; CALLING SEQUENCE:
;		hio_launch_browser, date=date, use_url=use_url
;		hio_launch_browser, date=date, /solarmonitor
; RESTRICTIONS:
;		tested on Mac OS-X and linux
; HISTORY:
;		   Jun-2011  rdb  Writtem
;		19-Jul-2011  rdb  tidied, added stereo movies, "" marks around url
;		15-Aug-2011  rdb  added " &" after command in linux (no wait from idlde)
;		03-Mar-2012  rdb  allow end time in stereo movie
;		06-Mar-2012  rdb  added Helioviewer call
;
;-

pro hio_launch_browser, date=date, use_url=use_url, $
	verbose=verbose, test=test, $
	soars=soars, solarmonitor=solarmonitor, $
	stereo_movie=stereo_movie, cdaw_movie=cdaw_movie, $
	jmap_ukssdc=jmap_ukssdc, $
	helioviewer=helioviewer

;    check if date supplied??

if not keyword_set(use_url) then begin
  if not keyword_set(date) then begin
    message,'>>> NO Date supplied <<<',/info
    return
  endif
endif

;--------------------------------------------------
;    assemble URL depending on the option selected

case 1 of

;----    just use the supplied URL

keyword_set(use_url): url = use_url

;----    display Solar Monitor page for the specified date 

keyword_set(solarmonitor): begin

    message,'Display SolarMonitor page for specified date',/info

    url = 'http://www.solarmonitor.org/index.php'
    if keyword_set(date) then begin
      qdate = '?date='+time2fid(date,/full)
      url = url + qdate
    endif
    
  end

;----    display the STEREO movies

;    supplying two string array will show selected movies
;    supplying single string will show that instrument and GOES light-curve
;    just setting switch will default to ['stb_cor2','stb_cor2']

;    possible instruments are
;    sta_cor1, sta_cor2,  sta_e171, sta_e195, sta_e284, sta_e304,  sta_wav1
;    stb_cor1, stb_cor2,  stb_e171, stb_e195, stb_e284, stb_e304,  stb_wav1
;    sdo_a094, sdo_a171, sdo_a193, sdo_a304
;    lasc2rdf
;    goesx

;    ????? create a widget to allow selection?

;    display for 24 hours - start time -> +24hrs    ???

keyword_set(stereo_movie) or keyword_set(cdaw_movie): begin

    message,'Display CDAW Movies for specified date',/info
    
    isok = addtime(date(0),diff='1-dec-06')/60./24.
;;    if keyword_set(stereo_movie) and isok lt 0 then begin
    if isok lt 0 then begin
      box_message,' >>> STEREO mission not operational at this time <<< '
      return
    endif

    help,stereo_movie,date
    root = 'http://cdaw.gsfc.nasa.gov/movie/make_javamovie.php?'

    if keyword_set(date) then begin
    
      if datatype(stereo_movie) eq 'STR' then begin
        img1 = 'img1='+stereo_movie(0)
        img2 = ''
        if n_elements(stereo_movie) eq 2 then img2 = '&img2='+stereo_movie(1)
        img_str = img1 + img2
        
      endif else $		; default to COR2 from behind and ahead
        img_str = 'img1=stb_cor2&img2=sta_cor2'
 
      stime = '&stime=' + time2fid(date(0),/full,/time)

      nhrs = 18					; should this be +/- 12 hours?
      time_to = time2fid(addtime(date(0),del=nhrs*60.),/full,/time)
      if n_elements(date) eq 2 then begin
        hr_diff = addtime(date(1),diff=date(0))/60.
        if hr_diff lt 72 then $
          time_to = time2fid(date(1),/full,/time) $
        else begin
          message,'Time interval TOO long (>72 hr)', /info
          return
        endelse
      endif 
      etime = '&etime=' + time_to
      
      url = root + img_str + stime + etime

    endif else message,'kkkkkasnsldldld'
    
  end

;----    create a temporary web page to display JMAPS from UKSSDC

keyword_set(jmap_ukssdc): begin

    message,'Display STEREO-HI JMAPs from UKSSDC for specified date',/info
    
    isok = addtime(date(0),diff='1-dec-06')/60./24.
    if isok lt 0 then begin
      box_message,' >>> Mission not operational at this time <<< '
      return
    endif

    url = make_ukssdc_jmap_page(date)
    
  end

;----    create a temporary web page to display Helioviewer

keyword_set(helioviewer): begin

    message,'Display Helioviewer for specified date',/info

    datestr = 'date=' + anytim(date(0),/ccsds) + 'Z'

;    set the scale so that you see solar disk within coronagraph field
    cscale = 'imageScale=16'
    ; if keyword_set(hv_scale) then...
    
    url = 'http://helioviewer.org/index.php?'+ datestr + '&' + cscale
    
    box_message,['****            Please be patient            ****', $
                 'It may time for all the image segments to come up']
    
  end

;----    show the SOARS Space Weather page - current date for the time being

keyword_set(soars):  url = 'http://www.mssl.ucl.ac.uk/~rdb/portal2/soars_SWx_II.php'

;----    there is no default...

else: begin
    message,'No target URL selected',/info
    return
  end

endcase

;--------------------------------------------------
;    launch browser window

url = '"' + url + '"'
print,'Using URL: ',url

;    which browser will depend on the OS and user

;?????    define browser using env. variable

;; open -a /Applications/Firefox.app
;; open -a /Applications/Safari.app
;; c:>start iexplore www.yahoo.com
;; c>start firefox www.yahoo.com

;; app = 'open -a /Applications/Firefox.app/Contents/MacOS/firefox-bin '	; Max OS-X

if keyword_set(test) then help,/st,!version
os_name = !version.os_name
os_family = !version.os_family

if keyword_set(verbose) then help,os_name

case 1 of
  os_name eq 'Mac OS X':  app = 'open -a /Applications/Firefox.app '
  os_name eq 'linux':     app = 'firefox '
  os_family eq 'Windows': app = 'start firefox '
  else: begin
      message,'Operating System not supported',/info
      return
    end
endcase

cmd = app + url
if keyword_set(verbose) then print, cmd
if os_name eq 'linux' then cmd=cmd + ' &'

if not keyword_set(test) then $
  spawn, cmd

end
