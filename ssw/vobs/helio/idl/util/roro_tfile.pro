function roro_tfile, in_text

;	write a temporary file of text containing a table and
;	use capabilities of rd_tfile to read back into tabular array

temp_file = concat_dir(get_temp_dir(), 'temp.temp')
file_append, temp_file, /new, in_text                                                           
out_text = rd_tfile(temp_file,/auto)   
temp = delete_file(temp_file)

return, out_text
end