;cd,'../PDS-access
;@deriv_hend_data_cols_fmt_struct

; dd = [285,310] & print, dd, anytim(doy2utc(dd,2002),/ecs,/date)   
; dd = [197,210] & print, dd, anytim(doy2utc(dd,2002),/ecs,/date)

;tempx = {ody_grs_sgs}
;;    don't really want the spectra in "ody_grs_sgs" so delete last item in structure
;temp = rem_tag(tempx,'GAMMA_SPECTRUM')
;rec_len = 131356L
;search_string = 'sgs*.dat'

tempx = {ody_grs_cgs}
;    don't really want the spectra in "ody_grs_cgs" so delete last item in structure
temp = rem_tag(tempx,'CORRECTED_SPECTRUM')
rec_len = 65935L
search_string = 'cgs*.dat'

ff=file_list('data',search_string)
help, ff
nff = n_elements(ff)
nff = 7+4+5
print, ff(0:nff-1),format='(a)'

stc = replicate(temp,nff*5000L)

kount = 0L
for kff=0,nff-1 do begin

cfile = ff(kff)
print, cfile

openr,1,cfile
fs = fstat(1) 
nrec = fs.size/rec_len
help, nrec

;;stc = replicate(temp, nrec)

for j=0,nrec-1 do begin
  rdwrt,'R',1,rec_len*j,0,temp,/do_swap
  stc(kount) = temp
  kount++
endfor

close,1

endfor

stc = stc(0:kount-1)
help, stc

time = string(stc.UTC)
help, time

;time_start = string(stc.UTC_START_TIME)
;time_end = string(stc.UTC_STOP_TIME)
;help, time_start,time_end

goto, endit

;    need to integrate and avearge

!p.multi = [0,1,2]
utplot,time,stc.SUN_ACTIVITY,yra=[0,1.5] 
;;utplot,time,stc.SC_HIGH_ADJUSTED_COUNTS			;,psym=3
utplot_io,time,stc.SC_HIGH_RAW_COUNTS, yra=[0.05,100],/yst			;,psym=3
!p.multi = 0

endit:
end

