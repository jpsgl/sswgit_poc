pro pds_access_struct, verbose=verbose

print, 'Compiling structures used to access PDS data'

ody_grs_dhd_struct
ody_grs_cgs_struct

if keyword_set(verbose) then begin
;  message,'Structure: ody_grs_dhd',/info
  help, {ody_grs_dhd}
;  message,'Structure: ody_grs_cgs',/info
  help, {ody_grs_cgs}
endif

end
