;cd,'../PDS-access

; dd = [285,310] & print, dd, anytim(doy2utc(dd,2002),/ecs,/date)   
; dd = [197,210] & print, dd, anytim(doy2utc(dd,2002),/ecs,/date)

pds_access_struct
temp = {ody_grs_dhd}

ff=file_list('data','dhd*')
help, ff
nff = n_elements(ff)

stc = replicate(temp,nff*5000L)

kount = 0L
for kff=0,nff-1 do begin

cfile = ff(kff)
print, cfile

openr,1,cfile
fs = fstat(1) 
rec_len = 108L
nrec = fs.size/rec_len
help, nrec

;;stc = replicate(temp, nrec)

for j=0,nrec-1 do begin
  rdwrt,'R',1,rec_len*j,0,temp,/do_swap
  stc(kount) = temp
  kount++
endfor

close,1

endfor

stc = stc(0:kount-1)
time = string(stc.utc)
help, stc,time

;    need to integrate and avearge

!p.multi = [0,1,3]
utplot,time,stc.SUN_ACTIVITY,yra=[0,1.5] 
utplot,time,stc.SC_HIGH_ADJUSTED_COUNTS/stc.PIXEL_DURATION			;,psym=3
utplot_io,time,stc.SC_HIGH_RAW_COUNTS/stc.PIXEL_DURATION, yra=[0.5,10000],/yst			;,psym=3
!p.multi = 0
  
end

