HELIO and unformatted PDS data files

rdwrt and sun2dec are routines written for Yohkoh
turn record in an unformatted file into record in an IDL structure

Currently some modern variable types cannot be converted - e.g. 64bit unsigned integer
Pad out the record with the equivalent number of bytes until this is fixed  