pro ody_grs_cgs_struct

;  Definition of structure generated from corr_gamma_spectra_cols.fmt

stc_temp = {ody_grs_cgs, $
  SC_RECV_TIME:bytarr(8), $                 ; The time the raw spectrum packet was received by the spacecraft, in ticks (256 per second). 
  SC_EV_TIME:bytarr(8), $                   ; Spacecraft time at the middle of the pixel, in ticks. 
  CEB_TIME:bytarr(8), $                     ; Clock count from the GRS Common Electronics Box at the beginning of the pixel. 
  UTC:bytarr(23), $                         ; SC_EV_TIME converted to UTC, stored as yyyy-mm-ddThh:mm:ss.sss. 
  PIXEL_DURATION:0, $                       ; Length of the collection period in milliseconds. 
  GRS_PIXEL_NUMBER:0L, $                    ; Sequential counter of accumulation intervals, starts with one as grs orbit begins. Special case on reboot when the the CEB is in orbit 0, pixel 0. 
  GRS_ORBIT_NUMBER:0L, $                    ; Sequential counter of orbits from GRS CEB boot. 
  ODY_ORBIT_NUMBER:0L, $                    ; Orbit number common to all instruments aboard Odyssey. This orbit number is incremented by one as the spacecraft passes through the orbital descending node. 
  AREOCENTRIC_LATITUDE:0.D0, $              ; Sub spacecraft latitude in Mars fixed coordinates at the middle of the pixel. 
  AREOCENTRIC_LONGITUDE:0.D0, $             ; Sub spacecraft longitude in Mars fixed coordinates at the middle of the pixel. Longitude increases towards the East. 
  INSTR_BORESIGHT_MARS:dblarr(3), $         ; Sub instrument boresight (x,y,z) in Mars fixed coordinates at the middle of the pixel. 
  SUB_SCPOS_MARS:dblarr(3), $               ; Sub spacecraft vector (x,y,z) in Mars fixed coordinates at the middle of the pixel. 
  SCALT:0.D0, $                             ; Areocentric altitude of the sub-spacecraft point in Mars-fixed rotating frame at the middle of the pixel. 
  DELTA_ANGLE:0.D0, $                       ; Difference between instrument +y direction and true north at the middle of the pixel. 
  MARS_SOL:0.D0, $                          ; Longitude of the Sun at 0 hours UT on the date of the record. Taken from the Association of Lunar and Planetary Observers 'Ephemeris for Physical Observation of Mars'. 
  DAY_INDEX:0, $                            ; Day of Martian year. 
  LOCAL_HOUR:0B, $                          ; Local Sun hour at the sub-spacecraft point. 
  LOCAL_MINUTE:0B, $                        ; Local Sun minute at the sub-spacecraft point. 
  POINTING:BOOLEAN(0), $                    ; True if pointing data was available. 
  INTERSECTING:BOOLEAN(0), $                ; True if the pointing vector intersects Mars. 
  BAD_CODE:0L, $                            ; If non-zero, the data has been flagged bad. Definitions in bad_code.txt. 
  IS_TEMP_A:0.0, $                          ; Inner Stage A temperature, smoothed and interpolated to the center of the pixel. 
  IS_TEMP_B:0.0, $                          ; Inner Stage B temperature, smoothed and interpolated to the center of the pixel. 
  OS_TEMP_A:0.0, $                          ; Outer Stage A temperature, smoothed and interpolated to the center of the pixel. 
  OS_TEMP_B:0.0, $                          ; Outer Stage B temperature, smoothed and interpolated to the center of the pixel. 
  B170K_TEMP:0.0, $                         ; The 170K card temperature, smoothed and interpolated for the middle of the pixel. 
  B170K_NORM_TEMP:0.0, $                    ; Temperature used to normalize the 170K card gain. 
  IS_NORM_TEMP:0.0, $                       ; Temperature used to normalize the detector gain. 
  GPA_TEMP:0.0, $                           ; Gamma Pulse Analyzer temperature, smoothed, and interpolated to the center of the pixel. 
  GAMMA_VREF_TEMP:0.0, $                    ; V-reference temperature smoothed, and interpolated to the center of the pixel. 
  HVBS_MONITOR:0.0, $                       ; High voltage bias supply value smoothed, and interpolated to the center of the pixel. 
  IS_TEMP_COEFFICIENT:0.0, $                ; Temperature coefficient of gain for the detector (parts-per-million/degree C) 
  B170K_TEMP_COEFFICIENT:0.0, $             ; Temperature coefficient of gain for the 170K card (parts-per-million/degree C). 
  APPS_GAIN_DHK:0, $                        ; APPS shaping amp gain setting from gamma digital housekeeping. 
  LLD_CNTR:0, $                             ; Lower level discriminator count. 
  ULD_CNTR:0, $                             ; Upper level discriminator count. 
  L1_CNTR:0, $                              ; Total number of gamma events within the L1 energy range. 
  L2_CNTR:0, $                              ; Total number of gamma events within the L2 energy range. 
  L3_CNTR:0, $                              ; Total number of gamma events within the L3 energy range. 
  PHA_CNTR:0L, $                            ; Pulse Height Analyzer counter. 
  PHA_TIMER:bytarr(8), $                    ; Pulse Height Analyzer timer. 
  FIRST_CHANNEL:0, $                        ; First valid channel in the spectrum. 
  LAST_CHANNEL:0, $                         ; Last valid channel in the spectrum. 
  FIRST_FULL_CHANNEL:0, $                   ; First channel with real counts. 
  LAST_FULL_CHANNEL:0, $                    ; Last channel with real counts. 
  GAIN:0.0, $                               ; Target gain after correction, as determined by, as determined by instrument settings and spectral analysis. Units are KeV/channel. 
  OFFSET:0.0, $                             ; Target offset after correction, as determined by instrument settings and spectral analysis. Units are KeV. 
  GAIN_CORRECTION:0.0, $                    ; After summing the corrected spectra, we may find that due to aging, drift, etc. we did not correct to the target gain. This field is then set to the ratio of the actual to the target gain, and the spectrum is recorrected with this factor included. 
  OFFSET_CORRECTION:0.0, $                  ; Set to the ratio of the actual to the target offset. 
  GPA_COEFFICIENTS:dblarr(3), $             ; Temperature coefficients for the GPA. 
  INL_COEFFICIENTS:dblarr(15), $            ; Coefficients of the polynomial used to correct the INL for this spectrum. Calculated from GAMMA_VREF_TEMP. 
  CORRECTED_SPECTRUM:fltarr(16384)}         ; Gamma spectrum, corrected counts for each channel. 

end

