pro ody_grs_dhd_struct

;  Definition of structure generated from deriv_hend_data_cols.fmt

stc_temp = {ody_grs_dhd, $
  SC_RECV_TIME:bytarr(8), $                 ; The time this packet was received by the spacecraft, in ticks (256 per second). 
  UTC:bytarr(23), $                         ; UTC time at the middle of pixel, stored as yyyy-mm-ddThh:mm:ss.sss. 
  SUN_ACTIVITY:BOOLEAN(0), $                ; 1(active sun during pixel interval); 0(no active sun during pixel interval) 
  AREOCENTRIC_LATITUDE:0.0, $               ; Latitude in Mars fixed coordinates at the middle of the pixel. 
  AREOCENTRIC_EAST_LONGITUDE:0.0, $         ; Longitude in Mars fixed coordinates at the middle of the pixel. 
  LS:0.0, $                                 ; Solar longitude (L-sub-s) at the middle of pixel. 
  PIXEL_DURATION:0.0, $                     ; Duration of pixel in seconds. 
  SD_BACKGROUND:0.0, $                      ; Background counts for the Small Detector. 
  MD_BACKGROUND:0.0, $                      ; Background counts for the Medium Detector. 
  LD_BACKGROUND:0.0, $                      ; Background counts for the Large Detector. 
  SC_LOW_BACKGROUND:0.0, $                  ; Background counts for the low channels of the Inner Scintillator. 
  SC_HIGH_BACKGROUND:0.0, $                 ; Background counts for the high channels of the Inner Scintillator. 
  SD_RAW_COUNTS:0.0, $                      ; Counts gathered in the Small Detector during the pixel interval. 
  MD_RAW_COUNTS:0.0, $                      ; Counts gathered in the Medium Detector during the pixel interval. 
  LD_RAW_COUNTS:0.0, $                      ; Counts gathered in the Large Detector during the pixel interval. 
  SC_LOW_RAW_COUNTS:0.0, $                  ; Counts gathered in the low channels of the Inner Scintillator during the pixel interval. 
  SC_HIGH_RAW_COUNTS:0.0, $                 ; Counts gathered in the high channels of the Inner Scintillator during the pixel interval. 
  SD_ADJUSTED_COUNTS:0.0, $                 ; Counts gathered in the Small Detector during the pixel interval with background subtracted. 
  MD_ADJUSTED_COUNTS:0.0, $                 ; Counts gathered in the Medium Detector during the pixel interval with background subtracted. 
  LD_ADJUSTED_COUNTS:0.0, $                 ; Counts gathered in the Large Detector during the pixel interval with background subtracted. 
  SC_LOW_ADJUSTED_COUNTS:0.0, $             ; Counts gathered in the low channels of the Inner Scintillator during the pixel interval with background subtracted. 
  SC_HIGH_ADJUSTED_COUNTS:0.0}              ; Counts gathered in the high channels of the Inner Scintillator during the pixel interval with background subtracted. 

end

