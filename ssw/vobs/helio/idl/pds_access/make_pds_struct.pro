;;;;;obsinst_access_info = ['ody_grs_sgs','summed_gamma_spectra_cols.fmt']

;    need to make a definition here when adding a new PDS file type

obsinst_access_info = ['ody_grs_dhd','deriv_hend_data_cols.fmt']
;obsinst_access_info = ['ody_grs_cgs','corr_gamma_spectra_cols.fmt']

fname  = obsinst_access_info(1)
stcname  = obsinst_access_info(0)

infile = concat_dir('information', fname)
if ~file_exist(infile) then message,'>> File does NOT exist: '+infile

;    read file and find the start and end of the parameter definition blocks

qq = rd_tfile(infile)

p0 = where(strpos(qq,'OBJECT') eq 0, np0)
p1 = where(strpos(qq,'END_OBJECT') eq 0, np1)

if np0 ne np1 then begin
  help, np0, np1
  message,'Severe ERROR in format definition'
endif else nrec = np0
help, nrec

;    build structure so that keep track of each parameter definition and derive IDL record

temp = {NAME:'', COLUMN_NUMBER:0, BYTES:0, DATA_TYPE:'', START_BYTE:0, $
			ITEMS :0, ITEM_BYTES:0, UNIT:'', DESCRIPTION:'', $
			idl_stc:'', running:0}
stc = replicate(temp, nrec)

kount = 0

ndo = nrec
for j=0,ndo-1 do begin

;    extract each definition block in turn

  record = qq(p0(j)+1:p1(j)-1)
;  print,record,format='(a)'
  nrecord = n_elements(record)

;    fill the structure from the records in the block

  qv = where(strpos(record, 'NAME') gt 0, nqv)
  if nqv eq 1 then stc(j).NAME = gt_brstr(record(qv)+'*','= ','*')
  qv = where(strpos(record, 'COLUMN_NUMBER') gt 0, nqv)
  if nqv eq 1 then stc(j).COLUMN_NUMBER = gt_brstr(record(qv)+'*','= ','*')
  qv = where(strpos(record, 'BYTES') gt 0, nqv)
  if nqv eq 1 then stc(j).BYTES = gt_brstr(record(qv)+'*','= ','*')
  qv = where(strpos(record, 'DATA_TYPE') gt 0, nqv)
  if nqv eq 1 then stc(j).DATA_TYPE = gt_brstr(record(qv)+'*','= ','*')
  qv = where(strpos(record, 'START_BYTE') gt 0, nqv)
  if nqv eq 1 then stc(j).START_BYTE = gt_brstr(record(qv)+'*','= ','*')
  qv = where(strpos(record, 'UNIT') gt 0, nqv)
  if nqv eq 1 then stc(j).UNIT = gt_brstr(record(qv)+'*','= ','*')
  qv = where(strpos(record, 'ITEMS') gt 0, nqv)
  if nqv eq 1 then stc(j).ITEMS = gt_brstr(record(qv)+'*','= ','*')
  qv = where(strpos(record, 'ITEM_BYTES') gt 0, nqv)
  if nqv eq 1 then stc(j).ITEM_BYTES = gt_brstr(record(qv)+'*','= ','*')
  qv = where(strpos(record, 'DESCRIPTION') gt 0, nqv)
  if nqv eq 1 then begin				; description may be split over several lines
    description = arr2str(record(qv:nrecord-1),delim=' ',/trim,/compress)
;;    print, description
    stc(j).DESCRIPTION = gt_brstr(description+'*','= "','"*')
  endif

;    keep track of where we are in terms of bytes in the record

  stc(j).running = kount + 1
  kount = kount + max([stc(j).BYTES, stc(j).ITEMS*stc(j).ITEM_BYTES])

;    Define IDL variable equivalent of the PDS variable type as string
;    Routines rdwrt/dec2sun cannot handle some of the more modern variable types - pad!
;    For example, pad out record with 8 bytes to represent unsigned 64bit integer

  fstring = ''
  fxstring = 'xx'			; should only have xx in format string it we have missed something

  if stc(j).BYTES eq 1 and stc(j).DATA_TYPE eq 'MSB_UNSIGNED_INTEGER' then fstring = '0B'
  if stc(j).BYTES eq 2 and stc(j).DATA_TYPE eq 'MSB_UNSIGNED_INTEGER' then fstring = '0'
  if stc(j).BYTES eq 4 and stc(j).DATA_TYPE eq 'MSB_UNSIGNED_INTEGER' then fstring = '0L'
;;  if stc(j).BYTES eq 8 and stc(j).DATA_TYPE eq 'MSB_UNSIGNED_INTEGER' then fstring = '0ULL'
;    the rdwrt routine does not seem to know 0ULL - save as bytes until resolved...
  if stc(j).BYTES eq 8 and stc(j).DATA_TYPE eq 'MSB_UNSIGNED_INTEGER' then fstring = 'bytarr(8)'
  if stc(j).BYTES eq 4 and stc(j).DATA_TYPE eq 'IEEE_REAL' then fstring = '0.0'
  if stc(j).BYTES eq 8 and stc(j).DATA_TYPE eq 'IEEE_REAL' then fstring = '0.D0'
;;  if stc(j).DATA_TYPE eq 'CHARACTER' then fstring = 'strarr('+strtrim(string(stc(j).BYTES),2)+')'
;    not sure how strarr maps into bytes - use bytarr instead
  if stc(j).DATA_TYPE eq 'CHARACTER' then fstring = 'bytarr('+strtrim(string(stc(j).BYTES),2)+')'
  if stc(j).BYTES eq 1 and stc(j).DATA_TYPE eq 'BOOLEAN' then fstring = 'BOOLEAN(0)'
;  if stc(j).BYTES eq 4 and stc(j).DATA_TYPE eq 'MSB_UNSIGNED_INTEGER' then fstring = '0L'
  if stc(j).ITEMS gt 0 then begin
    if stc(j).ITEM_BYTES eq 2 then fxstring = 'intarr('+strtrim(string(stc(j).ITEMS),2)+')'
    if stc(j).ITEM_BYTES eq 4  and stc(j).DATA_TYPE eq 'IEEE_REAL' then fxstring = 'fltarr('+strtrim(string(stc(j).ITEMS),2)+')'
    if stc(j).ITEM_BYTES eq 8  and stc(j).DATA_TYPE eq 'IEEE_REAL' then fxstring = 'dblarr('+strtrim(string(stc(j).ITEMS),2)+')'
;    fstring = ''
    fstring = fxstring
  endif
  if strlen(fstring) gt 0 then begin
    stc(j).idl_stc = stc(j).NAME + ':' + fstring
  endif else begin
    print, '-----------'
    print, record,format='(a)'
    print, stc(j).ITEMS, stc(j).ITEM_BYTES
    print, fxstring
  endelse

endfor

;    if all is OK, there should be no missing records

qv = where(stc.idl_stc eq '',nmiss)
help, nmiss

;;;; ff = str_replace(fname,'.','_')      ; ?????

;    create the IDL structure representing the decoded PDS file definition

ofile = stcname+'_struct.pro'

if nmiss eq 0 then begin
  file_append, ofile, 'pro '+stcname+'_struct', /new
  file_append, ofile, ''
  file_append, ofile, ';  Definition of structure generated from '+fname
  file_append, ofile, ''
  file_append, ofile, 'stc_temp = {' +stcname+ ', $'
  for j=0, nrec-1 do begin
    eline = ', $'
    if j eq nrec-1 then eline = '}'
    crec = string(stc(j).idl_stc, eline, stc(j).description, format='(2x,a,a,t45,1h;,a)')
    file_append, ofile, crec
  endfor
  file_append, ofile, ''
  file_append, ofile, 'end'
  file_append, ofile, ''
endif

;    write a web page summarising the decoded PDS file definition

wfile = stcname+'_stcdef.html'

trail = ['No. of Records:'+string(n_elements(stc)),'Written: '+reltime(/now,out='ecs')]
hio_stc2html, stc, file=wfile, title='Structure for PDS files: '+fname, trailer=trail

end


;    examples of parameter definitions in PDS unformatted files

;   NAME          = BIAS_SUPPLY_TEMP
;   COLUMN_NUMBER = 77
;   BYTES         = 8
;   DATA_TYPE     = IEEE_REAL
;   START_BYTE    = 340
;   UNIT          = "Degrees (C)"
;   DESCRIPTION   = "Bias supply temperature measurement, smoothed."

;   NAME          = SOLAR_STABILITY
;   COLUMN_NUMBER = 170
;   BYTES         = 20
;   DATA_TYPE     = MSB_UNSIGNED_INTEGER
;   START_BYTE    = 564
;   ITEMS         = 10
;   ITEM_BYTES    = 2
;   DESCRIPTION   = "A 10 element array of solar monitor detector rate
