;+
; Project     : SOHO - CDS
;
; Name        : OPLOT_NAR
;
; Purpose     : Oplot NOAA AR pointing structures from GET_NAR
;
; Category    : planning
;
; Syntax      : IDL> oplot_nar,nar or oplot_nar,time,nar
;
; Inputs      : NAR = NOAA AR pointing structures from GET_NAR
;               (if TIME is input, GET_NAR is called)
;
; Keywords    : EXTRA = plot keywords passed to XYOUTS
;               OFFSET = offset coordinates to shift labelling 
;               [data units, e.g., off=[100,100])
;               IMAP - if set, generate imagemap coords   
;               IMAGEMAP_COORD - (output) return the coords if /IMAP set
;               IMCIRCLE - if set, IMAP coords are "xc,yc,diam" (circle)
;                          default corrds are "minx,miny,maxx,maxy"
;               IMARS - (output) if IMAP, then return 1:1 AR#:IMAGEMAP_COORD
;               IMNOCONVERT - return IMAGEMAP_COORD in IDL convention
;                             (default=html imagemap w/(0,0)=upper right 
; Restrictions: A base plot should exist
;
; History     : Version 1,  20-June-1998,  D.M. Zarro.  Written
;             : Version 1.1, 28-March-2002, S.L. Freeland - IMAP support
;             : Version 2, 17-June-2002, Zarro (LAC/GSFC) - 'I4.4' AR plotting
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro oplot_nar,time,nar,quiet=quiet,offset=offset,_extra=extra, $
   imap=imap, imagemap_coord=imagemap_coord, $
   imnoconvert=imnoconvert, imcircle=imcircle, imars=imars

on_error,1
count=0
imap=keyword_set(imap)
nar_entered=datatype(time) eq 'STC'
if nar_entered then nar_entered=tag_exist(time,'NOAA')
if nar_entered then begin
 nar=time 
 count=n_elements(nar)
endif else begin
 if not exist(time) then begin
  pr_syntax,'oplot_nar,time,nar OR oplot_nar,nar'
  return
 endif else nar=get_nar(time,count=count,quiet=quiet,/unique)
endelse

;-- any offsets

xs=0. & ys=0.
if exist(offset) then begin
 xs=offset(0)
 ys=xs
 if n_elements(offset) eq 2 then ys=offset(1)
endif

if imap then begin                 ; imagemap coordinate return requested
  imagemap_coord=strarr(count)     ; init imap coord vector
  imars=strarr(count)              ; init AR list (1:1 map to imap coord)
endif

if count gt 0 then begin
 for i=0,count-1 do begin
  ari=trim(str_format(nar(i).noaa,'(i4.4)'))
  cnar = ari 
  if nar(i).noaa lt 10000 then $
	cnar = trim(str_format(nar(i).noaa+10000,'(i5.5)'))
  x=xs+nar(i).x & y=ys+nar(i).y
  if imap then dat0=tvrd()			  ; before AR annotate
  xyouts,x,y,cnar,/data,_extra=extra              ; AR annotate
  if imap then begin                              ; IMAP request? 
     imars(i)=ari				  ; AR(i)->output list	
     dat1=tvrd()         			  ; after AR annotate 
     imagemap_coord(i)= $                         ; Annotate->IMAP coord
       get_imagemap_coord(dat0,dat1,/string, $
          circle=imcircle,noconvert=imnoconvert)   
     delvarx,dat0,dat1
  endif
 endfor
endif

return & end

;================================================================================

pro hio_oplot_nar, map, nar, $
	write_htmlmap=write_htmlmap, is_narrep=is_narrep, $
	use_suffix=use_suffix, outname=outname, outdir=outdir, $
	charsize=charsize, xtrim=xtrim

message,'>>>> Entering',/info

;    trim width of plot - this will affect the html map
if n_elements(xtrim) eq 0 then xtrim = 0

;			offset of label?

;    labels the active regions on the plotted image
;    write the records that can be used in an html map definition
;    (optional?)


  help,nar
  if datatype(nar) ne 'STC' then begin
    message, '>>> No NAR structure supplied', /info
    return
  endif

if keyword_set(write_htmlmap) then begin
  suffix = ''
  if keyword_set(use_suffix) then suffix = use_suffix
  out_frag = 'ar_coords' + suffix + '.html'
  if keyword_set(outname) then out_frag = outname
  if keyword_set(outdir) then out_frag = concat_dir(outdir, out_frag)
  
;    always need to write header so that old records do not linger...

  maptime = anytim(map.time, /ecs, /trunc)
  
  print,'Writing file: ', out_frag
  file_append,out_frag,'<!--         active regions with and without spots         -->',/new
  file_append,out_frag,'<!--  image time associated with map: ' + maptime + '  -->'
endif
  
;    output the active regions numbers on the disk

qar = where(nar.region_flag ne 'II', nqar) & help,nqar		;<<<<<<<<
  
if nqar gt 0 then begin

  oplot_nar, nar(qar), color=1, charsize=charsize, charthick=2, $
	       offset=[50,75], $         ;[100,50]
	       /imap, imcircle=50, imagemap_coord=imagemap_coord, imars=imars
  help,imagemap_coord, imars ; & print, imagemap_coord & print,imars

  if keyword_set(write_htmlmap) then begin
  
;    url in Web map depends on where want to jump to

    qurl = 'http://www.solarmonitor.org/region.php?'
    if keyword_set(is_narrep) then qurl = 'solar_activity/arstats/arstats_page4.php?'

;    write out each Web map record in turn, applying xtrim correction

    for jar=0,nqar-1 do begin
      print,imars(jar)
;      print,nar(jar).location
      print,imagemap_coord(jar)
      
      xyr = str2arr(imagemap_coord(jar), ',')
      xyr(0) = xyr(0)-xtrim
      imagemap_coord(jar) = strtrim(arr2str(xyr),2)      
      print,imagemap_coord(jar)
      
;    prepare the pieces of the Web map records and assemble

      coords = 'coords="' + imagemap_coord(jar) + '" '
      region = 'region=1' + imars(jar)
      date = '&date='+time2fid(map.time,/full)			      ;20100524
      keys = region
      if not keyword_set(is_narrep) then keys = keys+date
      href = 'href="' + qurl + keys + '"'
      cmd = '<area shape="circle" ' + coords + href + ' target=ar_look>'
      print,cmd
      file_append, out_frag, cmd
;;      file_append,out_frag,string(imars(jar),imagemap_coord(jar),format='(a,2h; ,a)')
    endfor

  endif

endif
  
;    output the returning region numbers in different colour and on LHS
 
qar = where(nar.region_flag eq 'II', nqar) & help,nqar		;<<<<<<<<

if nqar gt 0 then begin
;    linecolor,1,'turquoise'
  oplot_nar, nar(qar), color=2, charsize=charsize, charthick=2         ;[100,50]
endif

;****    really need to rotate locations to time of the image...
  
end