pro cxs_ar_flareloc, map, nstr, ref_time, center, fov, use_ref=use_ref

sdate = map.time

ccstart = systime(/sec)		; ++++++ timing information

verbose = 0

;***  need to tilt by P angle!!!!!
;center = [0,0]
;fov = [40,25]
print,'Center: ',center
print,'FOV:    ',fov

;-----------------------------------------------------------------

;;;;map = set_map_params(map, obs_inst)
;help,/st,map
;help,/st,map.scale

cmin = map.scale.cmin
cmax = map.scale.cmax
log = map.scale.log
print,'cmin,cmax,log:',cmin,cmax,log

message,'CMAX s: '+string(cmax),/info

;-----------------------------------------------------------------
;    open plot device

if !d.name eq 'X' then begin
;  window, xsiz=900, ysiz=450
  window, xsiz=780, ysiz=750
  csiz=1.3
endif

if !d.name eq 'Z' then begin
;  device,set_resolution=[900,450]
  device,set_resolution=[900,550]
  csiz=1.0
endif

;    now lets plot everything

mtit = map.id+'!c'+anytim(map.time,/vms,/trun)
print,'<<<<<< Title?: ',mtit

gcolor = 200
if map.scale.obs_inst eq 'shmi_maglc' then gcolor=255

plot_map, map, fov=fov, center=center, title=mtit, $
	  dmin=cmin, dmax=cmax, log=log, $
	  grid=10, gstyle=1, gcolor=gcolor, bottom=4, charsiz=csiz

;;egso_oplot_secflare, nstr, ref=map, window=[-72,12], /verb

;    put east/west labels on
;??? tilt
xyouts, -825, -760-150, 'E', charsiz=1.4
xyouts,  800, -760-150, 'W', charsiz=1.4

;    prepare the legend giving time coverage
;;ttx = anytim(infiles(3),/vms,/date)
;;ttx = anytim(time,/vms,/date)

;;legend = 'Flares from '+ttx+' to '+strtrim(strcompress(strmid(fmt_tim(sdate),0,16)),2)+' UT'
legend = strmid(anytim(ref_time, /ecs), 0, 16) + ' UT'

;    blank bit behind label
;;polyfill,[-1100,-1100,600,600,-1100],[-550,-490,-490,-550,-550],color=0

;    define time ranges for different coloured dots
tref = sdate
if keyword_set(use_ref) then tref = ref_time
trange = [fmt_tim(addtime(tref,delt=-24*60)), fmt_tim(addtime(tref,delt=-12*60)), fmt_tim(tref)]

help,nstr

qv=-1
if datatype(nstr) eq 'STC' then begin
print,'>>>>> Available Flares:'
print,nstr.time_peak

;    over plot the events (if any)
;;ss = tim2match(anytim(infiles(3),/date,/vms), nstr.time_end, entim_ref=sdate) 

;;;;;ss = tim2match(anytim(time,/date,/vms), nstr.time_end, entim_ref=sdate) 

ss = intarr(n_elements(nstr))+1

qv = where(ss ge 0, kqv)
if keyword_set(verbose) then begin
  print,'ss: ',ss
  print,'qv: ',qv
endif

endif else message,'>>>> No Events',/info


;    still call egso_oplot_secflare even if no events valid - it writes legend...
;;if kqv gt 0 then
;help,nstr, /st
if datatype(nstr) eq 'STC' then begin
  hio_oplot_hecflare, nstr, idx_struct=[qv,-1], ref=map, $      ; /exact
    flag_range=trange, verbose=verbose, legend=legend, charsiz=1.4		;csiz
endif else message,'>>>> NO Events found',/info


cctime4 = systime(/sec)-ccstart		; ++++++ timing information

print,''
;print,'Got event list data:    ', cctime1
;print,'Identified image file:  ', cctime2
;print,'Copied/read image file: ', cctime3
print,'Finished plot:          ', cctime4

end
