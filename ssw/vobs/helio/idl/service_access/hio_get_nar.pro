;+
; Project     : SOHO - CDS
;
; Name        : GET_NAR
;
; Purpose     : Wrapper around RD_NAR
;
; Category    : planning
;
; Explanation : Get NOAA AR pointing from $DIR_GEN_NAR files
;
; Syntax      : IDL>nar=get_nar(tstart)
;
; Inputs      : TSTART = start time 
;
; Opt. Inputs : TEND = end time
;
; Outputs     : NAR = structure array with NOAA info
;
; Opt. Outputs: None
;
; Keywords    : COUNT = # or entries found
;               ERR = error messages
;               QUIET = turn off messages
;               NO_HELIO = don't do heliographic conversion
;;;;               LIMIT=limiting no of days in time range
;???               UNIQUE = return unique NOAA names
;
; History     : 20-Jun-1998, Zarro (EITI/GSFC) - written
;               20-Nov-2001, Zarro - added extra checks for DB's
;               24-Nov-2004, Zarro - fixed sort problem
;                3-May-2007, Zarro - added _extra to pass keywords to hel2arcmin
;               22-Aug-2013, Zarro - filtered entries from outside
;                                    requested period
;               31-Mar-2015, Zarro - added check for valid NAR directory
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function hio_get_nar, tstart, tend, count=count, err=err, quiet=quiet, _extra=extra, $
                 no_helio=no_helio, limit=limit, unique=unique

err=''
delvarx,nar
count=0

ttx = anytim([tstart,tend], /ecs)		; limit?
if ttx(0) eq ttx(1) then ttx(1) = anytim(addtime(ttx(0),delt=1*24*60.),/ecs) 

;ttx = anytim(ttx, /ecs)
;tfin = anytim(ttx(1), /ccs)
;ttx(1) = anytim(addtime(ttx(1),delt=14*24*60.),/ecs) 
print, ttx

;    get the data from the HEC

list = 'noaa_active_region_summary'
query = hio_form_hec_query(list=list, timer=ttx)
nars = ssw_hio_query(query, /conv)
help, nars
;help,/st,nars

if ~is_struct(nars) then begin
 err='NOAA data not found for specified times.'
 return,''
endif

;    correct an error in HEC database <<<<<<<<<<<<<<<<<<<
qnn = where(nars.time_start eq '2014-11-16T00:00:00' and nars.nar eq 12113, nqnn)
if nqnn eq 1 then nars(qnn(0)).nar=12213

;;count = n_elements(nars)

;    need to add some tabs

; x, y, noaa

nar = add_tag(nars, '', 'noaa')
nar.noaa = strmid(strtrim(string(nar.nar),2),1,100)			;????????

nar = add_tag(nar, 0.0, 'x')
nar = add_tag(nar, 0.0, 'y')
nar = add_tag(nar, '', 'region_flag')
help, nar, /st

;    region_type for compatibility with get_srs ??

goto, xxxx


err=''
t1=anytim2utc(tstart,err=err)
if err ne '' then get_utc,t1
t1.time=0

;-- default to start of next day

use_def=0b
t2=anytim2utc(tend,err=err)
if err ne '' then begin
 t2=t1
 t2.time=0
 t2.mjd=t2.mjd+1
 use_def=1b
endif

err=''

loud=~keyword_set(quiet)
if (t2.mjd lt t1.mjd) then begin
 err='Start time must be before end time.'
 if loud then mprint,err
 return,''
endif

if is_number(limit) then begin
 if (abs(t2.mjd-t1.mjd) gt limit) then begin
  err='Time range exceeds current limit of '+num2str(limit)+' days.'
  if loud then mprint,err
  return,''
 endif
endif

;-- strip off times that spill over into next day
          
if use_def then begin
 times=anytim(nar,/tai)
 chk=where( (times ge anytim2tai(t1)) and (times lt anytim2tai(t2)),count)
 if count gt 0 then nar=nar[chk]
endif

xxxx:
         
;-- determine unique AR pointings

count=n_elements(nar)

if ~keyword_set(no_helio) then begin

 if keyword_set(unique) then begin
  sorder = uniq([nar.noaa], sort([nar.noaa]))
  nar=nar[sorder]
 endif
 count=n_elements(nar)
 
 for i=0,count-1 do begin
  xy = hel2arcmin(nar[i].lat_hg, nar[i].long_hg, _extra=extra, date=nar[i].time_start)*60.
  nar[i].x = xy[0]
  nar[i].y = xy[1]
 endfor

nar.region_flag = 'I'
qv = where(nar.region_type eq 'REGIONS DUE TO RETURN', nqv)
if nqv gt 0 then begin
  nar(qv).region_flag = 'II'
  nar(qv).x = -1100			; off east limb
endif

 return, nar
 
endif else return,nar

end


