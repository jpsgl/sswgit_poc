function allowed_ics, ics_arr, pat_allowed=pat_allowed, verbose=verbose

;    compares return from ICS with the PAT used by the DPAS
;    only returns instruments that are present in the PAT

;    06-Oct-2012  rdb  chnaged where gets PAT table

;;common pat_comm, pat_allowed

if n_elements(pat_allowed) eq 0 then begin
;;  pat_allowed = get_heliopat()
;;  pat_allowed = reform(pat_allowed)

  pat_allowed =  hio_get_dpaspat()
;;  message,'PAT allowed not supplied as input'
endif

box_message,' * Filtering so only instruments known by DPAS are returned * '
message,'Current method of access to CDAWeb may remove "duplicates" for CLUSTER & VOYAGER',/info

nvv = n_elements(ics_arr)
ics_out = strarr(nvv)
kics = 0

for j=0,nvv-1 do begin
  coinst = ics_arr(j)
;    kludge until ICS reconciled with the DPAS mods for CDAWeb
  coinst = str_replace(coinst,'CLUSTER_1','CLUSTER')
  coinst = str_replace(coinst,'CLUSTER_2','CLUSTER')
  coinst = str_replace(coinst,'CLUSTER_3','CLUSTER')
  coinst = str_replace(coinst,'CLUSTER_4','CLUSTER')
  coinst = str_replace(coinst,'VOYAGER_1','VOYAGER')
  coinst = str_replace(coinst,'VOYAGER_2','VOYAGER')
  coinst = str_replace(coinst,'BLEIEN','BLEN')
  coinst = str_replace(coinst,'PROBA_2','PROBA2')
  coinst = str_replace(coinst,'NOBE__NORH','NOBE__RHELIO')
  coinst = str_replace(coinst,'NANC__NTRFA','NANC__TRFA')
  coinst = str_replace(coinst,'NANC__NRH','NANC__RHELIO')
  coinst = str_replace(coinst,'NANC__NDA','NANC__DAN')

  qv = where(strpos(pat_allowed, coinst) ge 0, nqv)
  if nqv gt 0 then begin
    ics_out(kics) = coinst
    kics++
  endif else if keyword_set(verbose) then print,'Removing: ',coinst
endfor

ics_out = ics_out(0:kics-1)
ics_out = all_vals(ics_out)

help, ics_arr, pat_allowed, ics_out

return,ics_out
end