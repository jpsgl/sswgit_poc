;+
; NAME:
;    
; PURPOSE:
;
;    the parameters depend on the CXS task
;    
; CALLING SEQUENCE:
;    
; CATEGORY:
;    
; INPUTS:
;    
; INPUT KEYWORDS:
;    
; OUTPUTS:
;    
; OUTPUT KEYWORDS:
;    
; RESTRICTIONS:
;    
; HISTORY:
;    11-06-2011  Written
;    01-02-2012  rdb  Updated call for Parker Spiral for new params.
;    27-02-2012  rdb  Updated call for flare plotter
;    25-07-2014  rdb  added call for goes plotter and data retrieval request
;    27-09-2014  rdb  always copy info file, if it exists
;    17-04-2015  rdb  added /noretrieve to allow functional check of the CXS
;
;-

function hio_do_cxs, time, verbose=verbose, $
;	flare_location=flare_location, $	; default
	goes_plot=goes_plot, request_data=request_data, $
	nodisplay=nodisplay, noretrieve=noretrieve, test=test, $
	parker=parker, velocity=velocity, hee=hee

;
;--    Prepare the XML files suing the templates - one for each task
;      The parameter values are subsututued for the dummy strings

case 1 of

keyword_set(parker): begin

;--    plot parker spiral, complex

    message, 'Plot Parker Spiral (complex)', /info
    title = 'HELIO CXS:  Parker Spiral'

    xml_template = concat_dir('$HELIO_JAVA/templates','ParkerFinal2.xml')
    qq = rd_tfile(xml_template)
    if n_elements(qq) eq 1 then message,'Problem locating XML template file'
    ;help,qq

    time_start = anytim(time,/ccs,/trunc)
    xml_file = str_replace(qq,'xfffx',time_start)

    vel = '400'					; default to 400 km/sec
    if keyword_set(velocity) then vel=string(velocity)
    print,'Using velocity: ',vel
    xml_file = str_replace(xml_file,'xvvvx',vel)

    xml_file = str_replace(xml_file,'xsoux','0')
    xml_file = str_replace(xml_file,'xsinx','1')
    cview = 'Normal'			; default to non-rotated view
    if keyword_set(hee) then cview = 'HEE' 
    xml_file = str_replace(xml_file,'xviewx',cview)

  end

keyword_set(goes_plot): begin				; SXR or proton??

;--    plot goes lightcurve

    message, 'Plot GOES Lightcurve', /info
    title = 'HELIO CXS:  GOES Lightcurve'

    if n_elements(time) ne 2 then begin
      message,'>>>> TWO element time paameter required',/cont
      return, -1
    endif

    xml_template = concat_dir('$HELIO_JAVA/templates','GoesFinal2.xml')
    qq = rd_tfile(xml_template)
    if n_elements(qq) eq 1 then message,'Problem locating XML template file'
;    help,qq

    time_start = anytim(time(0),/ccs,/trunc)
    time_end   = anytim(time(1),/ccs,/trunc)
    
    xml_file = str_replace(qq,'xfffx',time_start)
    xml_file = str_replace(xml_file,'xtttx',time_end)
    
    ptype = 'xray'
    if datatype(goes_plot) eq 'STR' then $
      if goes_plot eq 'proton' then ptype = 'proton'
    if keyword_set(request_data) then ptype = ptype + ':save'  
    xml_file = str_replace(xml_file,'xtypx',ptype)

  end

else: begin

;--    default to over-plot flares on context image

    message,'Over-plot flare locations on Context Image',/info
    title = 'HELIO CXS:  Flare Locations'

    xml_template = concat_dir('$HELIO_JAVA/templates','FlareTool.xml')
    qq = rd_tfile(xml_template)
    if n_elements(qq) eq 1 then message,'Problem locating XML template file'
;    help,qq

    time_start = anytim(time,/ccs,/trunc)
    xml_file = str_replace(qq,'xfffx',time_start)

  end

endcase

;----------------------

;--    make the request to the HELIO Context Server (CXS)

if keyword_set(test) then print,xml_file, format='(a)'
if keyword_set(test) then return, -1

furl = hio_cxs_execute2(xml_file, verbose=verbose)			;, request_data=request_data)

rid = gt_brstr(gt_brstr(furl, '-128','/')+'*','-','*') 		; unique ID from CXS

if keyword_set(verbose) then help, furl, rid

cxs_urls = {return:'', png_url:'', info_url:'', map_url:'', sav_url:'', rid:''}
cxs_urls.return = furl
cxs_urls.rid = rid

;    do not copy the data, just return the URLs structure
if keyword_set(noretrieve) then begin
  message,'Returning without retrieving', /info
  return, cxs_urls
endif

;----------------------

;--    define the temporary directory

temp_dir = concat_dir(get_temp_dir(),'cxs')
if not is_dir(temp_dir) then mk_dir,temp_dir

;--    always copy the PNG file - the name depends on the CXS process

break_file, furl, aa,bb,cc,dd
png_file = rid + '_' + cc+dd
if keyword_set(verbose) then message,'Copying file: ' + png_file, /info

cmd = wget_copy(furl, outdir=temp_dir, outfile=png_file, file_uri=file_uri)
cxs_urls.png_url = file_uri
if keyword_set(verbose) then begin
  print,cmd
  help, png_file, file_uri
endif
spawn, cmd, resp, errs

message,'Copying various files to: ' + temp_dir, /info

;--    copy the scaling information for goes plots and flare plots

;if not keyword_set(parker) then begin

  surl = str_replace(furl,'.png','.info')
  
if sock_check(surl) eq 1 then begin
  
  break_file, surl, aa,bb,cc,dd
  info_file = rid + '_' + cc+dd
  if !hio_sysvar.debug eq 1 then message,'Copying file: ' + info_file, /info

  cmd = wget_copy(surl, outdir=temp_dir, outfile=info_file, file_uri=info_uri)
  cxs_urls.info_url = info_uri  
  if keyword_set(verbose) then print,cmd
  spawn, cmd, resp, errs 

endif

;--    copy the map file if this is the flace plot

if not keyword_set(goes_plot) and not keyword_set(parker) then begin

  map_url = str_replace(furl,'flare_plot.png','ar_coords.html')
  map_file = rid + '_ar_coords.html'
  if !hio_sysvar.debug eq 1 then message,'Copying file: ' + map_file, /info

  cmd = wget_copy(map_url, outdir=temp_dir, outfile=map_file, file_uri=map_uri)
  cxs_urls.map_url = map_uri
  if keyword_set(verbose) then print,cmd
  spawn, cmd, resp, errs 

endif

;--    copy the IDL save file containng the GOES data (if reqested)

if keyword_set(goes_plot) and keyword_set(request_data) then begin

  qpr = where(strpos(xml_file, 'proton') gt 0, nqpr)
  gofile = 'hio-goesx.sav'
  if nqpr eq 1 then gofile = 'hio-goesp.sav'
  
  surl = str_replace(furl, 'goes_plot.png', gofile)  
  sav_file = str_replace(png_file, 'goes_plot.png', gofile)
  if !hio_sysvar.debug eq 1 then message,'Copying file: ' + sav_file, /info
    
  cmd = wget_copy(surl, outdir=temp_dir, outfile=sav_file, file_uri=sav_uri)
  cxs_urls.sav_url = sav_uri
  if keyword_set(verbose) then begin
    print,cmd
    help, sav_file, file_uri
  endif
  spawn, cmd, resp, errs

endif


;----------------------

;    display the returned image
;    should the display be optional?? 

if not keyword_set(nodisplay) then begin
  message,'Display plot',/info
  date = anytim(time(0),/ecs,/date)
  title = title +'  -  '+ date
  xcom_showpng, file_uri, title=title, wnum=wnum

  if keyword_set(verbose) then help, wnum, title, ffile
endif else message,'Display suppressed',/info

if keyword_set(verbose) then help, cxs_urls
print,''

return, cxs_urls
end
