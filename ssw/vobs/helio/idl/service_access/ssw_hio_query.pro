;+
; PROJECT:
;        HELIO
; NAME:
;        
; PURPOSE:
;        
; CALLING SEQUENCE:
;        
; CATEGORY:
;        
; INPUTS:
;        
; INPUT KEYWORDS:
;        
; OUTPUTS:
;        
; OUTPUT KEYWORDS:
;        
; RESTRICTIONS:
;        
; HISTORY:
;        01-Apr-2011  rdb  written
;        17-May-2015  rdb  Changed wget to have a timeout and one try
;        07-FEb-2017  rdb  Only print convert message if !hio_sysvar.debug set
;-

function ssw_hio_query, command, convert_votable=convert_votable, verbose=verbose

if keyword_set(verbose) then message,'Submitting query to HELIO Service',/info
if !hio_sysvar.debug eq 1 then print,command

;    assemble command with "wget" dependant on the operating system
;; cmd = wget_stream() + '"' + command + '"'

cmd = 'wget --read-timeout=5 --tries=1 -qO- ' + '"' + command + '"'
;;if strpos(command, 'dpas') ge 0 then cmd = 'wget --read-timeout=60 --tries=1 -qO- ' + '"' + command + '"'
if strpos(command, 'dpas') ge 0 then cmd = str_replace(cmd, 'timeout=5', 'timeout=60')

;    issue command and retrieve response

if keyword_set(verbose) then print,cmd
spawn, cmd, resp

;    STILTs puts an undesirable line at start and end

p1 = where(strpos(resp,'sql> ') eq 0, np1)
p2 = where(strpos(resp,'Elapsed time: ') eq 0, np2)
;print,p1,p2 & help,resp

stilts_out = resp
if np1 eq 1 and np2 eq 1 then stilts_out = resp(p1(0)+1:p2(0)-1)

if keyword_set(convert_votable) then begin
  if !hio_sysvar.debug eq 1 then message,'VOTable conversion requested',/info
  stilts_out = decode_votable(stilts_out, quiet=1-keyword_set(verbose))
endif else message,'VOTable conversion NOT requested',/info

return, stilts_out
end