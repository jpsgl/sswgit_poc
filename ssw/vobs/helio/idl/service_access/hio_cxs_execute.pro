;    22-07-2011  rdb  Tidied all the o/p name stuff - change to wget_copy
;    22-08-2011  rdb  Clean up temporay file
;    01-02-2012  rdb  Slaved node used to the Registry; extra param in call

function hio_cxs_execute, xml_file, verbose=verbose

ccstart = systime(/sec)

;message,'Making request to HELIO CXS',/info
box_message,['         Making request to HELIO CXS', $
             'This could take a few seconds, please be patient']

temp_dir = get_temp_dir()

;   save modified xml control file 
ofile = concat_dir(temp_dir,'qq_cxs.xml')
file_append, ofile,/new, xml_file

cnode = hio_get_snode(/cxs)
cxs_url = 'http://'+cnode			;'http://msslkz.mssl.ucl.ac.uk'
cmd = '$HELIO_JAVA/scripts/post_cxs.sh '+ofile+' '+cxs_url+' cxs'
if keyword_set(verbose) then print,cmd

spawn, cmd, resp, errs

if keyword_set(verbose) then begin
  help,resp,errs
  print,resp, format='(x,a)'
  print,errs, format='(x,a)'
endif

;print,''
if keyword_set(verbose) then print,'CXS Request time1:',systime(/sec)-ccstart,' secs'
;print,''

;    find URL of the PNG file and copy it

png_file = ''
qv = where(strpos(resp, 'Resulting URL') eq 0, nqv)

if nqv gt 0 then begin
  rurl = resp(qv(0))
  url = gt_brstr(rurl+'**','URL ','**')

  break_file,url, aa,bb,cc,dd
;  def_file = concat_dir(temp_dir,cc+dd)
;  help, def_file

  rid = gt_brstr(gt_brstr(url, '-128','result'),'-','/') 	; unique ID from CXS
;  png_file = rid+'.png'
  png_file = rid+'_'+cc+dd

;    CXS execution changed (Jan 2012) so return url to find url
  cmd0 = wget_stream() + url
  spawn, cmd0, resp
  furl = gt_brstr(resp,"<a href='","'>")
  if keyword_set(verbose) then print,furl  

;    now copy the file ...
  cmd = wget_copy(furl,outdir=temp_dir,outfile=png_file,file_uri=file_uri)
  if keyword_set(verbose) then begin
    print,cmd
    help, png_file, file_uri
  endif
  spawn, cmd, resp, errs

endif else begin
  message,'**** Problem with return from CXS',/info
  print, cmd
  help,resp,errs
  print, resp
  print, errs
  return,-1
endelse

;    get rid of temporary file
if not keyword_set(verbose) then ssw_file_delete, ofile		;, delete_status

print,'CXS Request time2:',systime(/sec)-ccstart,' secs'

return, file_uri    ;png_file
end
