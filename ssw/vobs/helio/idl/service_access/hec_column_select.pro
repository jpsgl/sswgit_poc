function hec_column_select, dummy

message,'Getting column selection definitions',/info

column_sel = 'hec_column_selections.txt'

;    look for the set-up file
file = find_setup_file(column_sel,database_dir=proj_dtb)


;    Ingest file that specifies which columns to return in search by HEC

qq = rd_tfile(file,delim=':',/auto,/comp,nocomm='#')
help,qq

nrow = (size(qq))(2)

temp = {table_name:'', sql_columns:''}
struct = replicate(temp, nrow)

for j=0,nrow-1 do begin
  struct.table_name = reform(qq(0,*))
  struct.sql_columns = reform(qq(1,*))
endfor

return, struct
end

