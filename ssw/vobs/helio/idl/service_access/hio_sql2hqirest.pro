;function hio_sql2hqirest, sql, service=service

;    convert to lower case and get rid of surpless spaces
sql = strtrim(strcompress(strlowcase(com)),2)

;    replace normal clause words with required form
cc1 = str_replace(sql,'select ','?SELECT=')
cc1 = str_replace(cc1,' from ','&FROM=')
cc1 = str_replace(cc1,' where ','&SQLWHERE=')

snode = hio_get_snode(/ics, cnode=cnode, verbose=verbose)

root = cnode		; don't need to manipulate????
url = root+cc1
print, url

end