;+
; NAME:
;	hio_get_dpaspat
; PURPOSE:
;	Read the HELIO Provider Access Table (PAT) - this contains
;	the list of instruments that can be accessed by the DPAS
;
;	Currently copies file from helio-vo.eu site to "/tmp" and reads
;	((eventually should access DPAS after mod has been made))
; INPUTS:
;	None
; INPUT KEYWORDS:
;	verbose		outputs more diagnostic messages
; OUTPUTS:
;	pat		short-form PAT as structure. This only contains a
;			single record for each instrument and little other info
; CALLING SEQUENCE:
;	pat =  hio_get_dpaspat()  [/verbose]
; RESTRICTIONS:
;
; HISTORY:
;	    Jun 2011  rdb  written
;	 21-Aug-2011  rdb  Changed goes_temp_dir to get_temp_dir
;			           use file id from wget_copy and delete file
;    16-Mar-2012  rdb  Delete copied file if fault occurs and zero lentgth
;    07-Apr-2012  rdb  Rebnamed; switched retrieve from DPAS
;    06-Oct-2012  rdb  Added help of No. of returns
;                      Added save in common and /force keyword
;    21-Jul-2016  rdb  Added debug printing of cmd to retrieve PAT
;    17-Dec-2016  rdb  Check for bad return from servlet; used old method if problem
;                      (required some rework of the old method)
;    01-Jan-2017  rdb  /raw keyword added to return array as read
;    04-Jan-2017  rdb  /compact to concatenate the array elements
;-

function hio_get_dpaspat, dummy, old=old, verbose=verbose, force=force, raw=raw, compact=compact

common pat_comm, pat_allowed

message,'GET HELIO Provider Access Table (PAT)',/info
;;help, pat_allowed

if not keyword_set(force) then begin
  if n_elements(pat_allowed) gt 0 then begin
    allowed_obsinst = pat_allowed
    message,'Using values in common',/info
    
    help, allowed_obsinst
    return, allowed_obsinst
  endif
endif

;goto, xxxx

if not keyword_set(old) then begin
;  message,'>> Retrieving latest PAT from the DPAS <<',/info
  box_message,'>> Retrieving latest PAT from the DPAS <<'
  
  snode = hio_get_snode(/dpas)			; 'msslkz.mssl.ucl.ac.uk'
  uu = 'http://'+snode+'/helio-dpas/HelioPatServlet'
  if !hio_sysvar.debug eq 1 then print, uu
  ofile = 'dpas_pat.csv'
  temp = get_temp_dir()
  cmd = wget_copy(uu, outfile=ofile, outdir=temp, file_uri=file_uri)
  if !hio_sysvar.debug eq 1 then print, cmd
  spawn, cmd, resp
    
  qq = rd_tfile(file_uri,delim=',',/auto)
  if !hio_sysvar.debug eq 1 then help,resp,qq
  
  if n_elements(qq) lt 100 then begin
    message,'>>>> Prime PAT source did not work, trying backup', /info
    goto, xxxx				; failed to get PAT, use backup
  endif

;    if /raw set, return array as read otherwise default to the first column

  if not keyword_set(raw) then begin
;    allowed = qq(0,*)
    allowed = qq(0,1:*)
    allowed_obsinst = reform(all_vals(allowed))
    
  endif else begin
;;    allowed_obsinst = qq(*,1:*)
    qqx = qq(*,1:*)
    oinst = all_vals(qqx(0,*))
    n_oinst = n_elements(oinst) 
    allowed_oinst = strarr(2,n_oinst)			;strarr(3,n_oinst)
    allowed_oinst(0,*) = oinst
;;    allowed_obsinst(2,*) = 'PAT'+string(indgen(n_oinst),format='(i3.3)')
    for j=0,n_oinst-1 do begin
      qv = where(qqx(0,*) eq oinst(j), nqv)
      allowed_oinst(1,j) = strtrim(fix(nqv),2)
    endfor
    allowed_obsinst = allowed_oinst
    if keyword_set(compact) then begin
      allowed_obsinst = strarr(n_oinst)
      for k=0,n_oinst-1 do allowed_obsinst(k) = arr2str(allowed_oinst(*,k),delim=',')
    endif
  endelse

;;  ssw_file_delete, file_url

  pat_allowed = allowed_obsinst
  
  help, allowed_obsinst
  return, allowed_obsinst
endif

xxxx:
;    copy pat_summary file and return allowed instruments

message,'Retrieving HELIO DPAS PAT - old method',/info

url = 'http://www.helio-vo.eu/services/other/dpas_pat.csv'			;pat_summary.csv
print,url

break_file,url, aa,bb,cc,dd

temp = get_temp_dir()                               
file_url = temp+'/'+cc+dd

retry_pat: 

if not file_exist(file_url) then begin

  if not is_dir(temp) then mk_dir,temp

  cmd = wget_copy(url, outdir=temp, file_uri=file_url) 
  if keyword_set(verbose) then  print,cmd
  spawn, cmd 
  
endif else begin

  message,'File already exists in '+temp,/info
  if file_size(file_url) eq 0 then begin
    print, 'Removing file - zero length: ',file_url
    help, delete_file(file_url)
    goto, retry_pat
  endif
  
endelse

qq = rd_tfile(file_url,delim=',',/auto)
help,qq
;;allowed_obsinst = qq(2,1:*)	; top line comment????

;    drop top line comment????
if not keyword_set(raw) then allowed_obsinst = qq(0,1:*) $
else allowed_obsinst = qq(*,1:*)

ssw_file_delete, file_url

if keyword_set(verbose) then more, allowed_obsinst

allowed_obsinst = reform(allowed_obsinst)

help, allowed_obsinst
return, allowed_obsinst

end      

;  pat = hio_get_dpaspat(/raw,/force,/comp)
;  file_append,'dpas_pat_compact.csv',pat