;+
; NAME:
;	hio_get_servroots
; PURPOSE:
;	Requests the HRS to provide information on available Service nodes
; INPUTS:
;	None
; INPUT KEYWORDS:
;	verbose		Outputs additional debug messages
; OUTPUTS:
;	urls		List of available root URLs
; CALLING SEQUENCE:
;	urls = hio_get_servroots( [/verbose] )
; RESTRICTIONS:
;	must be connected to the internet
; HISTORY:
;	20-Aug-2011  rdb  Split from block of routines
;   06-Aug-2012  rdb  work on alternate registry, etc.
;   20-Mar-2013  rdb  extract of urls made more sophisticated
;   17-Apr-2016  rdb  small adjustment to output with /verbose 
;
;-

function hio_get_servroots, substr, reset=reset, verbose=verbose

common save_servroots, resp1

xml_file = concat_dir('$HELIO_JAVA/templates','CheatingKeywordSearch.xml')
qq = rd_tfile(xml_file)
if n_elements(qq) eq 1 then message,'Problem locating XML file'

; print,qq

;help,substr
if n_elements(substr) eq 0 then substr = 'helio-'
help,substr

;    request the information from the HRS

qq1 = str_replace(qq,'xccccx',substr)
resp1 = hio_hrs_execute(qq1, reset=reset, verbose=verbose)

if keyword_set(verbose) then begin
  print,'------'
  box_message,'First Query to HELIO Registry'		;,/info
  print,qq1,format='(a)'
  print,''
  box_message,'Response to HELIO Registry Query'
  help,resp1
  print,resp1,format='(a)'
  print,'------'
endif

resp = resp1

;      easier to split is make string - 1st records not part of return
zz = (arr2str(resp1(2:*),delim=''))
segs = vot_strsegs(zz,'<myResults>', count=count)
;help,count
;print,segs

if count gt 0 then begin

if keyword_set(verbose) then box_message,'Looking for Records'

urls = strarr(count)
kurl = 0

for j=0,count-1 do begin
  crec = strmid(zz,segs(0,j),segs(2,j)+1)
  if strpos(crec,'<URLS>') gt 0 then begin
    if strpos(crec,'VORegistry') le 0 then begin
      if keyword_set(verbose) then print,crec
      useg = vot_strsegs(crec,'<accessURL', count=nurl)
      if keyword_set(verbose) then help, nurl
;      if keyword_set(verbose) then print,useg
      for k=0,nurl-1 do begin
        xx = strmid(crec,useg(0,k),useg(2,k)+1)
        url = gt_brstr(xx,'>','<')
        if strpos(url,'.helio-vo.eu') gt 0 then continue	; drop generic records
        urls(kurl) = url
        kurl++
      endfor
    endif
  endif
endfor

urls = urls(0:kurl-1)		; DON'T sort!!!
help,urls

if keyword_set(verbose) then begin
  print,''
;  print,'Available URLs:'
;  print,urls,format='(a)'
  box_message,['Available URLs:',urls]
  print,''
endif

;;      urls(j) = gt_brstr(recs(j),'full">','</accessURL>')

end else begin
  print,resp
  message,'Urcghhhhhh',/info
;;  return, ''
endelse

return, urls
end
