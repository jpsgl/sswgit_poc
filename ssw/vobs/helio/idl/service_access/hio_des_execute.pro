;+
; PROJECT:
;        HELIO
; NAME:
;        hio_des_execute
; PURPOSE:
;        Performs the hand-shaking with the DES to process a request
; CALLING SEQUENCE:
;        outstc = hio_des_execute(request  [, /verb])
; CATEGORY:
;        Service access
; INPUTS:
;        request      string containing the request for the DES
;                     should include the PHP action to be used
; INPUT KEYWORDS:
;        verbose      print additional debug information
; OUTPUTS:
;        outstc       Structure containing the decoded VOTable from the DES 
; OUTPUT KEYWORDS:
;        
; RESTRICTIONS:
;        
; HISTORY:
;        18-Apr-2016  rdb  extracted from larger block
;        19-Apr-2016  rdb  added url_xml keyword
;
;-

function hio_des_execute, request, url_xml=url_xml, verbose=verbose
	
if n_params() ne 1 or datatype(request) ne 'STR' then begin
  message,'**** Error in call to hio_des_execute - request absent or wrong type',/info
  return, -1
endif

print, request

;    Check if the system variable exists, it may contain a different DES node name
defsysv,'!hio_sysvar', exist=hio_isdef 

mdes_node = 'amda-dev.irap.omp.eu'
if hio_isdef eq 1 then if !hio_sysvar.root_des ne '' then mdes_node = !hio_sysvar.root_des
if keyword_set(verbose) then help, mdes_node

mdes_root = 'http://' + mdes_node + '/Amda-Helio/DDHTML/WebServices/helio-des/'

;    Assemble the command for the request

query = mdes_root + request

;???? tries, timeout?? - replace stream??

cmd1 = wget_stream() + '"' + query + '"'
if keyword_set(verbose) then print,cmd1

spawn,cmd1,resp

;    The response contains a Query ID that is used to track the process, etc.

if keyword_set(verbose) then print,resp
qv = where(strpos(resp,'jobid') gt 0, nqv)
if nqv eq 1 then begin
  query_id = gt_brstr(resp(qv(0)),'<jobid>','</jobid>')
  print, 'Query ID: ', query_id
endif  else begin
  print, resp
  message,'Groan - should NOT get here'
endelse

;    Need to keep polling the DES with the Query ID to look for completion

cmd2 = mdes_root + 'jobStatus.php?MODE=phase&ID=' + query_id
cmd2 = wget_stream() + '"' + cmd2  + '"'
if keyword_set(verbose) then print,cmd2

for j=0,180 do begin
  spawn,cmd2,resp2

  qv = where(strpos(resp2,'ERROR') gt 0, nqv)
  if nqv gt 0 then message,'Major ERROR in request to the DES'

  qv = where(strpos(resp2,'COMPLETE') gt 0, nqv)
  if nqv gt 0 then begin
    print, j, resp2, format='(i4,2x,a)'
    message,'DES Request completed, looking for return',/info
    goto, done_des		;break
  endif

  if (j mod 5) eq 0 then print, j, resp2, format='(i4,2x,a)' 
  wait,1			; wait a second before retrying
endfor

;    should only get here is the request has not completed
message,'MAJOR PROBLEM - Time-out without DES job finishing'

done_des:

;    The process has COMPLETED, ask for the results

cmd3 = mdes_root + 'jobStatus.php?MODE=result&ID=' + query_id
cmd3 = wget_stream() + '"' + cmd3  + '"'
if keyword_set(verbose) then print,cmd3

spawn,cmd3,resp3
if keyword_set(verbose) then print,resp3

arr = str2arr(resp3,';')
qv3 = where(strpos(arr,'http') gt 0, nqv3)
if nqv3 gt 0 then url = gt_brstr(arr(qv3(0)),'<resultURI>','</resultURI>')
print,url
url_xml = url

;    Use the URL to retrieve the VOTable containing information about the results

cmd = wget_stream() + url 
if keyword_set(verbose) then print,cmd
spawn, cmd, respx
if keyword_set(verbose) then begin
  help, respx
  print,respx
endif

quiet = 1
;if keyword_set(verbose) then quiet = 0
;help, quiet
vv = decode_votable(respx, quiet=quiet)
if keyword_set(verbose) then help, vv

return, vv
end
