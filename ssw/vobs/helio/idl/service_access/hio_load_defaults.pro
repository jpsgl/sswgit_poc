;+
; NAME:
;	load_hio_defaults
;
; PURPOSE:
;	Sets up default system values; includes service URLs and WGET/CURL
;   Root so servives loaded from "hio_service_roots.txt"
;
; CALLING SEQUENCE:
;	load_hio_defaults
;
; INPUT KEYWORDS:
;	verbose			print additional information
;
; RESTRICTIONS:
;
; HISTORY:
;       Apr-2011  rdb  written
;    19-Jul-2011  rdb  Made WGET default for copy  (except for Mac)
;    07-aug-2012  rdb  Added read from Registry on /reset
;    23-aug-2012  rdb  Forced it load msslkz as default for HEC   <<<<<<<<
;    09-mar-2013  rdb  block *.helio-vo.eu - don't know if should be port 8080
;    17-Jun-2014  rdb  removed default to CURL for Darwin. WGET is defailt.
;    25-Jul-2014  rdb  added flag to switch between hisperia and CXS for GOES data
;    04-Aug-2014  rdb  Added flag for deleting temporary files; stop making DES and SMS roots
;    07-Aug-2014  rdb  Added flag to detemine if structure of DPAS return should be saved 
;    26-Feb-2015  rdb  Added /short keyword to stop access to regisry, etc.
;    18-May-2015  rdb  Reordered code - put changes to wget before regustry access
;    17-Apr-2016  rdb  Changed order of services - moved dpas before uoc and cxs
;                      Added passing verbose to make_hrs_stc
;
;-

pro hio_load_defaults, verbose=verbose, reset=reset, short=short

;---------------------------------------------------------
;    load some values related to event start/end and create sysvar

;    look for the set-up file
file = find_setup_file('helio_defaults.txt')

if keyword_set(verbose) then print,'Loading: ', file
qq = rd_tfile(file,/nocomm)

lqq = strlen(strcompress(qq))
qq = qq(where(lqq gt 0))
;help,qq

nq=n_elements(qq)
cc = '{'
for j=0,nq-1 do begin
  cline = strcompress(strtrim(str2arr(qq(j),';'),2))
;  print,str2arr(cline,':')
;  print,cline(0)
  if j eq 0 then cc = '{'+cline(0) else $
    cc = cc + ', ' + cline(0)
endfor
cc = cc +'}'

temp = execute('hio_defs='+cc)
;;help,hio_defs,/st

;  ?? define default set and copy??
;;if not exist(!hio_defs) then $
  defsysv,'!hio_defs', hio_defs

if keyword_set(verbose) then help,/st,!hio_defs

;---------------------------------------------------------
;    load the roots url's of all the services and create sysvar

;    first part of hio_sysvar are the root URLs of the services

rserv = ['hec', 'hfc', 'ics', 'ils', 'dpas', 'uoc', 'cxs', 'des', 'sms']
cc = '{' + arr2str("root_" + rserv + ":''", ", ")

;    two things related to the Registry

cc = cc +', hrs_alt:0'				; flag for alternate HELIO Registry
cc = cc +', hrs_root:""'

;    variable setting behaviour of the code

cc = cc +', wget_flag:1'			; flag for WGET (1) ot CURL (0)
cc = cc +', cxs_goes:1'				; flag: if set use CXS to get GOAS data
cc = cc +', temp_delete:1'			; if set, delete temporary files
cc = cc +', save_dpasvot:0'			; if set, save structure of DPAS return from hio_dpas_request
cc = cc +', debug:0'				; if set, print additional debug information
cc = cc + '}'

if keyword_set(verbose) then print,cc
temp = execute('hio_sysvar = ' + cc)

defsysv,'!hio_sysvar', hio_sysvar

;---------------------------------------------------------
;    decide whether should use WGET or CURL for copying

wget_flag = 1						; default to WGET

;if !version.os eq 'darwin' then wget_flag = 0		; use CURL on a Mac

temp = get_logenv('HIO_WGET_FLAG')				; env. var. overrides 
if strlowcase(strtrim(temp)) eq 'wget' then wget_flag = 1
if strlowcase(strtrim(temp)) eq 'curl' then wget_flag = 0

!hio_sysvar.wget_flag = wget_flag			; set the method

;---------------------------------------------------------
;    Use CXS as a source of GOES data - faster than using other sites

!hio_sysvar.cxs_goes = 1

;---------------------------------------------------------
;    define whether decoded VOTable returned by the DPAS is automatically saved

!hio_sysvar.save_dpasvot = 0

temp = get_logenv('HIO_SAVE_DPASVOT')			; env. var. overrides
if strlowcase(strmid(temp,0,1)) eq 'y' then !hio_sysvar.save_dpasvot = 1
if strlowcase(strmid(temp,0,1)) eq 'n' then !hio_sysvar.save_dpasvot = 0


;---------------------------------------------------------
;    exit after establishing variables but before accessing registry, etc. 

;if keyword_set(short) then return

;    now load the desired set of default URLs for the services
;    note thet these are loaded without checking against the Registry

file = find_setup_file('hio_service_roots.txt')
if keyword_set(verbose) then print,'Loading: ', file
qq = rd_tfile(file, nocomm='#')
nq = n_elements(qq)

for j=0,nq-1 do begin
  cline = strcompress(strtrim(str2arr(qq(j),': '),2))
;  croot = '!hio_sysvar.root_' + str_replace(cline(0), ': ',' = ')
  cnode = gt_brstr(cline(1)+'/', '://', '/')
  croot = "!hio_sysvar.root_" + cline(0) + " = '" + cnode + "'"		;cline(1)
  
  if keyword_set(verbose) then print, croot
  temp = execute(croot)
endfor

;---------------------------------------------------------
;    decide which registry to use

hrs_flag = 0
temp = get_logenv('HIO_HRS_ALT')			; env. var. overrides 

if strlowcase(strtrim(temp)) eq 'true' then  hrs_flag = 1

!hio_sysvar.hrs_alt = hrs_flag

;---------------------------------------------------------
;    exit after establishing variables but before accessing registry, etc. 

if keyword_set(short) then return

;---------------------------------------------------------
;    load the roots from the HELIO Registry if required

hrs_struct = make_hrs_stc(verbose=verbose)

;help, hrs_struct,/st
;help, !hio_sysvar

if keyword_set(resetxx) then begin

;    get the possible service roots
servroots = hio_get_servroots() 

;;qq = where(strpos(servroots,'helio-vo.eu') lt 0)		; 130309 - is port 8080?
;;servroots = servroots(qq)
;;;print,servroots

rserv = ['hec', 'hfc', 'ics', 'ils', 'uoc', 'cxs', 'dpas', 'des', 'sms']

for j=0,n_elements(rserv)-1 do begin
  csnode = servroots(where(strpos(servroots,rserv(j)) gt 0, ncsn))
  if ncsn gt 0 then begin
    if rserv(j) eq 'hec' then csnode = csnode(where(strpos(csnode,'inaf') lt 0))  ;<<<<<<<<<<<<<<
    snode = csnode(0)
    temp = execute('!hio_sysvar.root_'+rserv(j)+'="'+snode+'"')
  endif
endfor

endif

;---------------------------------------------------------

if keyword_set(verbose) then help,/st,!hio_sysvar

end
