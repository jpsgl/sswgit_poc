;+
; NAME:
;    dpas_stc2api
; PURPOSE:
;    Form a query using information in the supplied structure and submit to the DPAS
;    Request submitted using wget_stream, a VOTable is returned 
; CALLING SEQUENCE:
;    dpas_stc2api, struct, resp
; CATEGORY:
;    
; INPUTS:
;    struct        structure containing the start ad end times and list of instruments
; INPUT KEYWORDS:
;    
; OUTPUTS:
;    resp          VOTable returned by the DPAS - segmented by instrument
; OUTPUT KEYWORDS:
;    
; RESTRICTIONS:
;    Instrument list should be based on the PAT from the selected DPAS
;    Request times out after 300 seconds
; HISTORY:
;      Oct-2010  rdb  written
;    2011-03-05  rdb  updated the servlet address, generalised command get command
;    2012-02-01  rdb  Slaved node used to the Registry
;    2015-07-27  rdb  Changed wait message, improved code structure and documentation
;
;-

pro dpas_stc2api, struct, resp

message,'Forming request to DPAS',/info

;    Determine which DPAS to use and form servlet URL
cnode = hio_get_snode(/dpas)
dpas_servlet = 'http://' + cnode + '/helio-dpas/HelioQueryServlet'

nrec = n_elements(struct)

;    clause related to start time of search
if nrec gt 1 then tstart = arr2str(anytim(struct.time_start,/cc,/trunc),delim=',') $
  else tstart = anytim(struct.time_start,/cc,/trunc)
time_start = '?STARTTIME=' + tstart

;    clause related to end time of search
if nrec gt 1 then tend = arr2str(anytim(struct.time_end,/cc,/trunc),delim=',') $
  else tend = anytim(struct.time_end,/cc,/trunc)
time_end = '&ENDTIME=' + tend

;    clause related to instruments in the search
if nrec gt 1 then oinst = arr2str(struct.obs_inst,delim=',') $
  else oinst = struct.obs_inst
instruments = '&INSTRUMENT=' + oinst

;    assemble query
qurl = dpas_servlet + time_start + time_end + instruments

;    command that is used depends on the operating system
cmd = wget_stream(use_timeout=300) + '"' + qurl + '"'

print,cmd
box_message,['','  Waiting for a reply from the DPAS  ', $
                '    This could take a few minutes    ', '']

spawn,cmd,resp

end
