;+
; NAME:
;       hio_service_node
; PURPOSE:
;       Find active node of HELIO Services
;
; CALLING SEQUENCE:
;       result = hio_service_node([/hec] [/hfc] [/ics] [/ils] [/dpas] [/uoc])
; OUTPUTS:
;       result       node to use for query to the HELIO Services
;                    if none working, or not connected to internet, returns -1
; KEYWORDS: 
;       hec          find node for the HEC
;       hfc          find node for the HFC
;       cxs          find node for the CXS
;       ics          find node for the ICS
;       ils          find node for the ILS
;       uoc          find node for the UOC
;       dpas         find node for the DPAS
;       backup       select instance specified by value of backup keyword
;
;	testnode     test if the supplied node is available
;
; METHOD:
;       Try primary and secondary nodes of requested the HELIO Services 
;       until one works. Try each twice; default is HEC
;       If /backup keyword set, use instance specified by its value
;       
;	Loads list of available nodes from the HRS
;
; RESTRICTIONS:
;       Must be connected to the Internet to return node
; HISTORY:
;       24-Jul-2011  Written by Bob Bentley (MSSL/UCL); based on EGSO
;       30-Jul-2011  rdb  added more nodes
;       20-Aug-2011  rdb  get service urls from the HRS
;       23-Aug-2014  rdb  remove referebce to msslxw, defn. of cxs root
;       18-May-2016  rdb  changed how knodes derived (problem if single instance)
;
;-

FUNCTION hio_service_node, node_root, backup=backup, $
		hec=hec, hfc=hfc, uoc=uoc, ics=ics, ils=ils, dpas=dpas, cxs=cxs, des=des, $
		test_node=test_node, force=force, verbose=verbose


;    save the urls returned by the HRS to speed up the query
;;common hio_nodes, urls
common hio_nodes, hrs_struct

if keyword_set(verbose) then begin
  help, hrs_struct
  help, hrs_struct,/st
endif

if keyword_set(test_node) then begin
  if have_network(test_node,/reset) then return, test_node else return, -1
endif

;    load the urls of the services if this is the first call

if n_elements(hrs_struct) eq 0 or keyword_set(force) then begin

  message,'Loading Service Roots',/info
  
;;  urls = hio_get_servroots(verbose=verbose)
;  if keyword_set(verbose) then $
;;  print,urls, format='(a)'

  hrs_struct = make_hrs_stc(verbose=verbose) 

endif

;    select the nodes related to the keyword
	
case 1 of
  keyword_set(hec):  qserv = 'hec'
  keyword_set(uoc):  qserv = 'uoc'
  keyword_set(ics):  qserv = 'ics'
  keyword_set(ils):  qserv = 'ils'
  keyword_set(dpas): qserv = 'dpas'
  keyword_set(hfc):  qserv = 'hfc'
  keyword_set(cxs):  qserv = 'cxs'
  keyword_set(des):  qserv = 'des'
  else: message,'Unknown service'				; qserv = 'hec'
endcase
message,'Searching for new instance, type: ' + strupcase(qserv), /info


;qv =  where(strpos(urls,qserv) gt 0, nqv)
;if nqv gt 0 then nodes = urls(qv)  $
;  else message,'No node available - ' + qserv
  
qv = where(hrs_struct.service eq strupcase(qserv), nqv)
if nqv gt 0 then begin
  pserv = qv(0)
  nodes = hrs_struct(pserv).nodes
  nodes = nodes(where(hrs_struct(pserv).node_flags ge 0) )
endif else message,'No node available - ' + qserv


if keyword_set(verbose) then begin
  help,nodes
  print,nodes, format='(a)'
endif

knodes = hrs_struct(pserv).n_nodes			;n_elements(nodes)
help, knodes

goto, xxx2

;    allow node selection using the /backup keyword

if keyword_set(backup) then begin
  qnode = min([knodes-1,backup])
  node = nodes(qnode)
  if not have_network(node,/reset) then begin
    message, 'Problem with node:', /cont
    print,'>>>>>>>> '+node
    message, 'Verify that you are connected to the network and retry',/cont
    return, -1
  endif else begin
    message, 'Network connection check OK:', /cont
    print,'>> '+node
    return, node
  endelse
endif

xxx2:

;    find a node (instance of the service) that works

for k = 0,knodes-1 do begin
  cnode = nodes(k)
  
;;  node_root = gt_brstr(node,'http://',':')
  snode = cnode			;;gt_brstr(cnode,'http://','/')
  if strpos(snode,':') gt 0 then snode = gt_brstr(cnode,'http://',':')		; remove port no.
  node_root = snode

;  try each node twice in attempt to find one
  for j = 0,1 do begin
    print, 'k,j:', k,j
    is_good = 0
    cc = 'is_good = hio_test_snode(cnode, /' + qserv + ')'
    print,cc
    temp = execute(cc)
    help, is_good
    
    if not is_good then begin
      message, 'Problem with node:', /cont
      hrs_struct(pserv).node_flags(k) = -1
      print,'>>>>>>>> ' + cnode
    endif else begin
      message, 'Network connection check OK:', /cont
      hrs_struct(pserv).node_flags(k) = 1
      print,'>> ' + cnode
      return, cnode
    endelse
    
  endfor

endfor  

message, 'Verify that you are connected to the network and retry',/cont
return, -1

END
