;+
; PROJECT:
;        HELIO
; NAME:
;        hio_test_snode
; PURPOSE:
;        Try to execute simple command to check that a service node is working
;        Formulated command is issued using function ssw_hio_query
; CALLING SEQUENCE:
;        status = hio_test_snode(cnode, /ics)
; CATEGORY:
;        
; INPUTS:
;        cnode      URI of the node to be tested, e.g. "helio.mssl.ucl.ac.uk"
; INPUT KEYWORDS:
;        hec        switch to select the test of the HEC 
;        hfc        switch to select the test of the HFC
;        uoc        switch to select the test of the UOC
;        ics        switch to select the test of the ICS
;        ils        switch to select the test of the ILS
;        dpas       switch to select the test of the DPAS
;        cxs		switch to select the test of the CXS
;        verbose    output more diagnostics
; OUTPUTS:
;        status     result of test - 1 for success, 0 for fail
; OUTPUT KEYWORDS:
;        
; RESTRICTIONS:
;        
; HISTORY:
;           Jul-2014  rdb  written
;        19-May-2015  rdb  Added the test for the HFC and UOC
;        17-Apr-2016  rdb  Added GOES /noretrieve test of the CXS
;        09-Feb-2017  rdb  Added snode to the report message
;
;-

function hio_test_snode, cnode, verbose=verbose, $
	hec=hec, hfc=hfc, uoc=uoc, ics=ics, ils=ils, dpas=dpas, cxs=cxs

if n_params() ne 1 then begin
  message,'Wrong number of parameters'
endif

if keyword_set(verbose) then print,'Service node: ', cnode
;;snode = gt_brstr(cnode+'/', 'http://', '/')
snode = cnode
if keyword_set(verbose) then help,snode

;
;    Test whether can make a network connection to the service

if not have_network(cnode,/reset, /use) or not is_url(cnode) then begin
  message,'Node could not be contacted or null URL'
  return, 0							; return error if not
endif

;
;    Test whther the service is functional by creating a simple request and making a query
;    Explicitly define the command so that do not need to use the "form" functions 
;    since these try to establish which node t use

istat = 1				; default to OK so that "else" does not cause failure

case 1 of

  keyword_set(hec): begin
    cserv = 'HEC'
    c1 = str_replace('http://xxnodexx/helio-hec/HelioQueryService', 'xxnodexx', snode)
    query = c1 + "?SELECT=table_name&FROM=information_schema.tables&SQLWHERE=table_schema='public'"
    if keyword_set(verbose) then print,query
    temp = ssw_hio_query(query)
    if datatype(temp) ne 'STR' then istat = 0 
    if n_elements(temp) lt 10  then istat = 0
  end

  keyword_set(hfc): begin
    cserv = 'HFC'
    c1 = 'http://' + snode + '/helio-hfc/HelioQueryService'
    query = c1 + "?FROM=VIEW_AR_HQI&STARTTIME=2008-01-01T00:00:00&ENDTIME=2008-01-01T06:00:00"
    if keyword_set(verbose) then print,query
    temp = ssw_hio_query(query)
    if datatype(temp) ne 'STR' then istat = 0 
    if n_elements(temp) lt 10  then istat = 0
  end
  
  keyword_set(ics): begin    
    cserv = 'ICS'
;;    c1 = str_replace('http://xxnodexx/stilts/task/sqlclient', 'xxnodexx', snode)
;;    query = c1 + '?db=jdbc:mysql://localhost/helio_ics&user=helio_guest&sql=SHOW TABLES&ofmt=vot'
    c1 = 'http://' + snode + '/helio-ics/HelioQueryService'
    query = c1 + "?SELECT=table_name&FROM=information_schema.tables&SQLWHERE=table_schema='helio_ics'"
    if keyword_set(verbose) then print,query
    temp = ssw_hio_query(query)
    if datatype(temp) ne 'STR' then istat = 0 
    if n_elements(temp) lt 10  then istat = 0
  end

  keyword_set(ils): begin
    cserv = 'ILS'
;;    c1 = str_replace('http://xxnodexx/stilts/task/sqlclient', 'xxnodexx', snode)
;;    query = c1 + '?db=jdbc:mysql://localhost/helio_ils&user=helio_guest&sql=SHOW TABLES&ofmt=vot'
    c1 = 'http://' + snode + '/helio-ils/HelioQueryService'
    query = c1 + "?SELECT=table_name&FROM=information_schema.tables&SQLWHERE=table_schema='helio_ils'"
    if keyword_set(verbose) then print,query
    temp = ssw_hio_query(query)
    if datatype(temp) ne 'STR' then istat = 0 
    if n_elements(temp) lt 10  then istat = 0
  end

  keyword_set(dpas): begin
    cserv = 'DPAS'
    query = 'http://' + snode + '/helio-dpas/HelioPatServlet'
    if keyword_set(verbose) then print,query
    temp = ssw_hio_query(query)
    if datatype(temp) ne 'STR' then istat = 0 
    if n_elements(temp) lt 10  then istat = 0
  end

  keyword_set(uoc): begin
    cserv = 'UOC'
    c1 = 'http://' + snode + '/helio-uoc/HelioQueryService'
    query = c1 + "?STARTTIME=2000-10-20T20:30:56&ENDTIME=2000-10-25T20:30:56&FROM=planetary_cat"
    if keyword_set(verbose) then print,query
    temp = ssw_hio_query(query)
    if datatype(temp) ne 'STR' then istat = 0 
    if n_elements(temp) lt 10  then istat = 0
  end

  keyword_set(cxs): begin
    cserv = 'CXS'
;;    temp = get_cxs_goes(['1-jun-12 00:00', '1-jun-12 00:05'])
    temp = hio_do_cxs(['1-jun-12 00:00', '1-jun-12 00:05'], /goes, /noret)
    if datatype(temp) ne 'STC' then istat = 0 
    if istat eq 1 then begin
;;      times = temp.time
;;      if n_elements(times) lt 25  then istat = 0
      if strpos(temp.return, '/jobs/cea-') lt 0 then istat = 0
    endif
  end

  keyword_set(des): begin
    cserv = 'DES'
    message, cserv + ' - No test currently defined', /info
    return, 1
  end

  keyword_set(sms): begin
    cserv = 'SMS'
    message, cserv + ' - No test currently defined', /info
    return, 1
  end

  else: begin
    message, 'XXXXXXX - No test currently defined !', /info
    return, 1
  end

endcase

if istat eq 1 then message, cserv + ' - Tested OK,  using Node ' + snode, /info  $
else begin
  message, cserv + ' - Simple test did NOT WORK,  using Node ' + snode,', /info
  print, query
endelse
  
;print,'Service node: ', cnode

return, istat
end