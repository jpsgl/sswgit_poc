;+
; NAME:
;		rd_myfav_obsinst
; PURPOSE:
;		read in a user's favourite set of HELIO observatory/instrument keys
; INPUTS:
;		none
; INPUT KEYWORDS:
;		/verbose
; OUTPUTS:
;		none
; CALLING SEQUENCE:
;		favs = rd_myfav_obsinst([/verbose])
; RESTRICTIONS:
;
; HISTORY:
;		14-Jul-2011  rdb  written
;		13-Aug-2014  rdb  Added ability to have multiple lines, some rewriting
;		                  No longer accepts "sta" insterad of "stereo-a"
;-

function rd_myfav_obsinst, verbose=verbose

;    default to the file in working directory  (HOME?)
infile = 'myfav_obsinst.txt'

;    it could also be define by the environment variable HIO_MYFAV_OBSINST
fav_env = get_logenv('HIO_MYFAV_OBSINST')
if fav_env ne '' then infile = fav_env

if not file_exist(infile) then begin
  message,'NO Favourite Instruments file defined',/info
  return, -1
endif

message,'Using defined set of favourite instruments',/info

print,'Reading:  ', infile
qqin = rd_tfile(infile)

help, qqin
if keyword_set(verbose) then print, qqin

qqinx = qqin(0)
if n_elements(qqin) gt 1 then begin
  sel = hio_xmenu_sel(qqin, size=14, /one, $
	title='My list of Favorite Instruments', banner='Select the required list')
;  help,sel
  qqinx = qqin(sel)
endif
;print,qqinx

;;qq = arr2str(strtrim(str2arr(qqin),2),delim=',')
qq = strtrim(str2arr(qqinx),2)

qq = str_replace(qq,'/','__')        
qq = str_replace(qq,'-','_')        

;    GONG is currently wrong in the PAT!!!!!
;qq = str_replace(qq,'gong__','gong-')

qq = strupcase(qq)

if keyword_set(verbose) then print, qq

return,qq
end