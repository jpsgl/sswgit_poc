;+
; PROJECT:
;        HELIO
; NAME:
;        make_des_insitu_plot
; PURPOSE:
;        Issue a command to the DES to make a plot of solar wind parameters
;        Retrieve the plot and display it
; CALLING SEQUENCE:
;        make_des_insitu_plot, times, /wind
; CATEGORY:
;        Plotting, service access
; INPUTS:
;        times        time and and time; 2 string elements
; INPUT KEYWORDS:
;        ace          select ACE mission [default]
;        wind         select WIND mission
;        ulysses      select ULYSSES mission
;        sta          select STEREO-A mission
;        stb          select STEREO-B mission
;        verbose      print additional debug information
; OUTPUTS:
;        none
; OUTPUT KEYWORDS:
;        
; RESTRICTIONS:
;        
; HISTORY:
;        16-Apr-2016  rdb  written
;
;-

pro make_des_insitu_plot, times, verbose=verbose, no_plot=no_plot, $
	ace=ace, wind=wind, ulysses=ulysses, sta=sta, stb=stb		;, mission=mission
	
;    Check if the system variable exists, it may contain a different DES node name
defsysv,'!hio_sysvar', exist=hio_isdef 

mdes_node = 'amda-dev.irap.omp.eu'
if hio_isdef eq 1 then if !hio_sysvar.root_des ne '' then mdes_node = !hio_sysvar.root_des
if keyword_set(verbose) then help, mdes_node

mdes_root = 'http://' + mdes_node + '/Amda-Helio/DDHTML/WebServices/helio-des/'

;    Formulate the time range and mission sub-strings

tt = strmid(anytim(times, /ccs), 0,16)
rq_times = 'STARTTIME=' + tt(0) + '&ENDTIME=' + tt(1)

rq_mission = '&FROM=ACE'
stitle = 'ACE'
if keyword_set(wind) then begin
  rq_mission = '&FROM=WIND'
  stitle = 'WIND'
endif
if keyword_set(ulysses) then begin
  rq_mission = '&FROM=ULYSSES'
  stitle = 'ULYSSES'
endif
if keyword_set(sta) then begin
  rq_mission = '&FROM=STEREO-A'
  stitle = 'STEREO-A'
endif
if keyword_set(stb) then begin
  rq_mission = '&FROM=STEREO-B'
  stitle = 'STEREO-B'
endif

ptitle = stitle + ' In-situ observations (HELIO DES)'

;    Assemble the REST query for the DES

query_clause = 'plot.php?'
query = rq_times + rq_mission
request = query_clause + query

vv = hio_des_execute(request, verbose=verbose)

;if keyword_set(verbose) then help, vv
if datatype(vv) ne 'STC' then begin
  message,'Major problem in request to the DES',/info
  return
endif
print, vv.url

;    The VOTable contains the URL of the image

tempdir = concat_dir(get_temp_dir(),'mdes')
if not is_dir(tempdir) then mk_dir,tempdir

cmd4 = wget_copy(vv.url, outdir=tempdir, file_uri=ofile)
if keyword_set(verbose) then print,cmd4

spawn,cmd4
print, 'Image file: ',ofile

if not keyword_set(no_plot) then $
	xcom_showpng, ofile, title = ptitle, wnum=wnum 

end
