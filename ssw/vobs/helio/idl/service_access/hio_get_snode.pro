;+
; NAME:
;	hio_get_snode
; PURPOSE:
;	Returns a node to be used to access a service
;	Calls hio_service_node to find active node and sets this as active node
;	in system variable !hio_sysvar
; INPUTS:
;	None
; INPUT KEYWORDS:
;       hec		find node for the HEC
;       hfc		find node for the HFC
;       cxs		find node for the CXS
;       ics		find node for the ICS  [default]
;       ils		find node for the ILS
;       uoc		find node for the UOC
;       dpas		find node for the DPAS
;	verbose		output more diagnostic messages
; OUTPUTS:
;	node	Node to use
; CALLING SEQUENCE:
;	node = hio_get_snode()  [/hec] [/hfc] [/ics] ...
; RESTRICTIONS:
;	only returns a single node at a time
; HISTORY:
;	 01-08-2011  rdb  written
;    01-02-2012  rdb  Some work on CXS access; ILS search for node
;    14-03-2012  rdb  Added search fro ICS and DPAS
;    24-08-2914  rdb  Tidying, added search for HFC node
;-

function hio_get_snode, verbose=verbose, $
	hec=hec, hfc=hfc, uoc=uoc, ics=ics, ils=ils, dpas=dpas, cxs=cxs, $
	cnode=cnode


case 1 of

  keyword_set(hec): begin
    cnode = !hio_sysvar.root_hec
    if keyword_set(verbose) then help,cnode
    if not have_network(cnode,/reset) or not is_url(cnode) then begin
;    if not hio_test_snode(cnode, /hec) then begin
      print,'>>>> Problem with HEC node: ', cnode
      temp = hio_service_node(node, /hec)
      if datatype(temp) eq 'STR' then begin
        print, 'New HEC node: ', temp
        !hio_sysvar.root_hec = temp
        cnode = temp
      endif else message,'MAJOR PROBLEM'
    endif
  end

  keyword_set(uoc): begin
    cnode = !hio_sysvar.root_uoc
    if keyword_set(verbose) then help,cnode
    if not have_network(cnode,/reset) or not is_url(cnode) then begin
      print,'>>>> Problem with UOC node: ', cnode
      temp = hio_service_node(node, /uoc)
      if datatype(temp) eq 'STR' then begin
        print, 'New UOC node: ', temp
        !hio_sysvar.root_ouc = temp
        cnode = temp
      endif else message,'MAJOR PROBLEM'
    endif
  end

  keyword_set(ics): begin
    cnode = !hio_sysvar.root_ics
    if keyword_set(verbose) then help,cnode
    if not have_network(cnode,/reset) or not is_url(cnode) then begin
      print,'>>>> Problem with ICS node: ', cnode
      temp = hio_service_node(node, /ics)
      if datatype(temp) eq 'STR' then begin
        print, 'New ICS node: ', temp
        !hio_sysvar.root_ics = temp
        cnode = temp
      endif else message,'MAJOR PROBLEM'
    endif
  end

  keyword_set(ils): begin
    cnode = !hio_sysvar.root_ils
    if keyword_set(verbose) then help,cnode
    if not have_network(cnode,/reset) or not is_url(cnode) then begin
      print,'>>>> Problem with ILS node: ', cnode
      temp = hio_service_node(node, /ils)
      if datatype(temp) eq 'STR' then begin
        print, 'New ILS node: ', temp
        !hio_sysvar.root_ils = temp
        cnode = temp
      endif else message,'MAJOR PROBLEM'
    endif
  end
  
  keyword_set(dpas): begin
    cnode = !hio_sysvar.root_dpas
    if keyword_set(verbose) then help,cnode
    if not have_network(cnode,/reset) or not is_url(cnode) then begin
      print,'>>>> Problem with DPAS node: ', cnode
      temp = hio_service_node(node, /dpas)
      if datatype(temp) eq 'STR' then begin
        print, 'New DPAS node: ', temp
        !hio_sysvar.root_dpas = temp
        cnode = temp
      endif else message,'MAJOR PROBLEM'
    endif
  end

  keyword_set(hfc): begin
    cnode = !hio_sysvar.root_hfc
    if keyword_set(verbose) then help,cnode
    if not have_network(cnode,/reset) or not is_url(cnode) then begin
      print,'>>>> Problem with HFC node: ', cnode
      temp = hio_service_node(node, /hfc)
      if datatype(temp) eq 'STR' then begin
        print, 'New HFC node: ', temp
        !hio_sysvar.root_hfc = temp
        cnode = temp
      endif else message,'MAJOR PROBLEM'
    endif
  end

  keyword_set(cxs): begin
    cnode = !hio_sysvar.root_cxs
    if keyword_set(verbose) then help,cnode
    if not have_network(cnode,/reset) or not is_url(cnode) then begin
      print,'>>>> Problem with CXS node: ', cnode
      temp = hio_service_node(node, /cxs)
      if datatype(temp) eq 'STR' then begin
        print, 'New CXS node: ', temp
        !hio_sysvar.root_cxs = temp
        cnode = temp
      endif else message,'MAJOR PROBLEM'
    endif
  end

  else: begin
    cnode = !hio_sysvar.root_ics		; assume mssl ??????
  end

endcase

if keyword_set(verbose) then help, cnode

;snode = gt_brstr(cnode,'http://','/')
snode = cnode
if strpos(snode,':') gt 0 then snode = gt_brstr(cnode,'http://',':')		; remove port no.
if keyword_set(verbose) then help, snode

return, snode

end
