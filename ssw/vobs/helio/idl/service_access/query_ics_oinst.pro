;+
; NAME:
;    
; PURPOSE:
;    
; CALLING SEQUENCE:
;    
; CATEGORY:
;    
; INPUTS:
;    
; INPUT KEYWORDS:
;    
; OUTPUTS:
;    
; OUTPUT KEYWORDS:
;    
; RESTRICTIONS:
;    
; HISTORY:
;    01-Apr-2011  rdb  written
;
;-

function query_ics_oinst, oinst, quiet=quiet

;    get details of instrument(s) from the ICS based on the obsinst_key

help,oinst

c1 = 'obsinst_key,date(time_start) as time_start,date(time_end) as time_end,'
cols = c1 + 'inst_od1,inst_od2,inst_type,inst_oe1,inst_oe2,keywords,experiment_id,group_name'

where_clause = ' where ' + arr2str("obsinst_key='" + oinst + "'", delim=' or ')

psql = 'select ' + cols + ' from instrument' + where_clause
if not keyword_set(quiet) then print,psql

query = hio_form_ics_query(sql=psql)
vv = ssw_hio_query(query,/conv)

if not keyword_set(quiet) then help,vv

return,vv
end


; select 
; cols = "name,loc_gen,loc_p1 as 'long',loc_p2 as 'lat',
; date(time_start) as time_start,date(time_end) as time_end,longname" 
; from observatory where loc_gen='GBO'