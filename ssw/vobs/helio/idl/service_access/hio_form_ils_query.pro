function hio_form_ils_query, timerange, columns=columns, $
	target=target, key_events=key_events, $
;;trajectories=trajectories, $
	show_tables=show_tables, describe_table=describe_table, $
	verbose=verbose

;    24-Mar-2011  rdb  Created 
;    30-Jan-2012  rdb  Select node automatically
;    19-Feb-2012  rdb  Removed selection of only date field
;    24-Feb-2012  rdb  Added suppport of list of targets

;
;
message,'Forming query for HELIO ILS',/info

;if keyword_set(trajectories) then begin
  qcat = 'trajectories'
  qcols ='*'
  date_field = "time"
;;  qcols = "id,target_obj,date(time) as time,julian_int,r_hci,long_hci,lat_hci,long_hee,lat_hee,long_carr"
;;;  qcols = "target_obj, date(time) as time, r_hci, long_hci, lat_hci, long_carr"
  qcols = "target_obj, time, r_hci, long_hci, lat_hci, long_carr"

if keyword_set(columns) then qcols=columns

;endif

list = qcat

;    Syntax of some system commands for PostgreSQL is different to mySQL

;    List available tables
if keyword_set(show_tables) then begin
  com = "SHOW TABLES"
  goto, go_psql
endif

;    Determine the parameters in a table
if keyword_set(describe_table) then begin
  com = "DESCRIBE " +list		; ==  show columns from 'list'
  goto, go_psql
endif


;    default times
date_from = '2003-10-25T22:00'
date_to = '2003-10-31T04:00'

if n_params() eq 1 then begin
  date_from = anytim(timerange(0),/ccs,/trunc)
  date_to = anytim(timerange(1),/ccs,/trunc)
endif

if keyword_set(verbose) then fmt_timer,[date_from, date_to]

if keyword_set(key_events) then begin
  message,'Requesting Key Events from ILS',/info
  qcat = 'keyevents'
  qcols= "Observatory, Sat_ID, time_event, Type, Description"
  date_field = "time_event"
endif

com = "SELECT " +qcols+ " FROM " +qcat+ " WHERE " +date_field+ " BETWEEN '" +date_from+ "' AND '" +date_to+ "'"

;    add list of targets if supplied
if keyword_set(target) then begin
  ntarg = n_elements(target)
  for jtarg=0,ntarg-1 do begin
    if jtarg eq 0 then qtarget = "target_obj='" +target(jtarg)+ "'" $
      else qtarget = qtarget + " OR target_obj='" +target(jtarg)+ "'"
  endfor
  com = com + "AND (" + qtarget + ")"
  com = com + ' ORDER BY time'
endif

go_psql:
goto, tryhqi

psql = '&sql='+com
if keyword_set(verbose) then print, psql

;    concatonate the URL
;;root = 'http://msslxv.mssl.ucl.ac.uk:8080/stilts/task/sqlclient'
;;rdtb = '?db=jdbc:mysql://msslxt.mssl.ucl.ac.uk/helio_ils&user=helio_guest'

cnode = hio_get_snode(/ils)
clauses = hio_form_stilts_clauses(cnode, /ils)
root = clauses(0)
rdtb = clauses(1)

ofmt = '&ofmt=vot'
url  = root + rdtb + psql + ofmt

goto, endit

tryhqi:
;    convert to lower case and get rid of surpless spaces
message,'ILS now with HQI',/info

sql = strtrim(strcompress(strlowcase(com)),2)

;    replace normal clause words with required form
cc1 = str_replace(sql,'select ','?SELECT=')
cc1 = str_replace(cc1,' from ','&FROM=')
cc1 = str_replace(cc1,' where ','&SQLWHERE=')
cc1 = str_replace(cc1,' limit ','&LIMIT=')
cc1 = str_replace(cc1,' order by ','&ORDERBY ')

snode = hio_get_snode(/ils, cnode=cnode, verbose=verbose)
snode = cnode

;    allow use of supplied node name
if keyword_set(test_node) then snode=test_node

help, snode

root = snode		; don't need to manipulate????

root = 'http://' + snode + '/helio-ils/HelioQueryService'
url = root + cc1

;;;if keyword_set(show_tables) then url = root + com

endit:

if keyword_set(verbose) then print, url

return, url
end

