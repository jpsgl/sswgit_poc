function look_for_html, struct, nhtml=nhtml, verbose=verbose

tags = strlowcase(tag_names(struct))

ntags = n_tags(struct)
bb=intarr(ntags)                                     
for j=0,ntags-1 do begin
  qv =  where(strpos(struct(*).(j),'http') ge 0, nqv)
  bb(j) = nqv
endfor

qv = where(bb gt 0, nhtml)

if keyword_set(verbose) then begin
  print,bb
  if nhtml gt 0 then print,tags(qv)
endif

if nhtml gt 0 then message,'** HTML field(s) found **',/info

return, qv
end


function select_hec_cols, struct, list_name, verbose=verbose

;    29-jun-2014  rdb  Auto removal of HTML field(s)

;--  Select a subset of columns in the structure id one is defined

col_select = hec_column_select()			; load any defined lists of parameters
;help,col_select

;;help,/st,struct

tags = strlowcase(tag_names(struct))
help, tags
print,tags

qhtml = look_for_html(struct, nhtml=nhtml)

qc = where(col_select.table_name eq list_name,nqc)
if nqc eq 1 then begin
  message,'>> Columnn selection found:  '+list_name,/info
  qcols = col_select(qc).sql_columns
											; ???? HTML found

endif else begin
  message,'>> Not special case:  '+list_name,/info

  if nhtml gt 0 then begin
    message,'^^ HTML field present **',/info
    ;print, qhtml
    print, tags(qhtml)
    tags(qhtml) = ''
    qt = where(tags ne '', nqt)
    if nqt gt 0 then qcols = arr2str(tags(qt), delim=', ')
  endif else return,struct					; ????

endelse
;help, qcols
if keyword_set(verbose) then print, qcols

message,'Selecting columns as specified',/info

rq_tags = strtrim(str2arr(qcols,','),2)
nrq_tags = n_elements(rq_tags)
if keyword_set(verbose) then print,rq_tags

delvarx,s1

for j=0,nrq_tags-1 do begin
  qv = where(rq_tags(j) eq tags, nqv)
  if not have_tag(s1,rq_tags(j)) then s1=add_tag(s1,struct(0).(qv(0)),tags(qv(0)))
endfor

outstr = str_copy_tags(s1,struct) 

if keyword_set(verbose) then begin
  help, struct
  help,/st, struct
  help, outstr
  help,/st, outstr
endif

return,outstr
end