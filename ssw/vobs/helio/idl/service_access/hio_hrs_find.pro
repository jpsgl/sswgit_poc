;+
; PROJECT:
;        HELIO
; NAME:
;        hio_hrs_find
; PURPOSE:
;        Find Registry by polling list determined from file
; CALLING SEQUENCE:
;        reg_url = hio_hrs_find(/reset)
; CATEGORY:
;    
; INPUTS:
;    
; INPUT KEYWORDS:
;        reset            Forces read of list of known Registries
;        verbose          Output more information
; OUTPUTS:
;        reg_url          URL of the selected Registry 
; OUTPUT KEYWORDS:
;    
; RESTRICTIONS:
;    
; HISTORY:
;    30-01-2012  rdb  Written
;    15-03-2012  rdb  Corrected logic after msslkr became avalable
;    06-08-2012  rdb  work on alternate registry
;    06-08-2014  rdb  Added the /use_network to the have_network call
;                     Supeesses the delay in finding node not there
;    24-08-2014  rdb  Changed block of conditions to stop new search unless forced
;    24-10-2014  rdb  Added abort if no Registries can be contacted
;    06-12-2014  rdb  Changed HRS node check to other than "have_network"
;
;-

function hio_hrs_find, reset=reset, verbose=verbose

;    store suitable registries in common
common hio_hrs, registry_roots, flag_ok

if !hio_sysvar.hrs_root eq '' or keyword_set(reset) then begin

;   find the HELIO Registries from file

if n_elements(registry_roots) eq 0 or keyword_set(reset) then begin
  message,'Read HRS file & check available Registries', /info
  file = '$HELIO_DTB/helio-registry_list.txt'
  qq = rd_tfile(file,/auto,delim=';')
  registry_roots = reform(qq(0,*))
  if keyword_set(verbose) then $
  		box_message, ['Known HELIO Registries: ', registry_roots]
endif 

;    check each registry in turn to determine if available
n_hrs = n_elements(registry_roots)

flag_ok = intarr(n_hrs)
hrs_root = ''
for j=0,n_hrs-1 do begin
;;  chk_root = 'http://' + registry_roots(j)
;;  if have_network(chk_root, /reset, /use_network) then flag_ok(j)=1
  if hio_test_hrs(registry_roots(j)) then flag_ok(j)=1
  print, flag_ok(j), registry_roots(j), format='(i3,2x,a)'
;  if keyword_set(verbose) then  $
;;  	print, chk_root, flag_ok(j)
;    hrs_root = chk_root
;    break
;  endif
endfor

hrs_root = ''
for j=0,n_hrs-1 do begin
  if flag_ok(j) then begin
    hrs_root = registry_roots(j)
    break
  endif
endfor

hrs_root = 'http://' + hrs_root
!hio_sysvar.hrs_root = hrs_root

endif

;    If nothing set by now there ia a problem - are we connected?

if !hio_sysvar.hrs_root eq 'http://' then begin
  help, hrs_root
  box_message,['*****************************************', $
               'Major problem in accessing the Registries', $
			   '    PLEASE CHECK Internet connection', $
			   '*****************************************']
  message,'Aborting'
endif

;    switch to alternate if system variable set  << needs MORE

if !hio_sysvar.hrs_alt and flag_ok(1) then begin
  message,'**** Alternate REGISTRY selected by flag ****',/info
  hrs_root = registry_roots(1)

  hrs_root = 'http://' + hrs_root
  !hio_sysvar.hrs_root = hrs_root
endif

hrs_root = !hio_sysvar.hrs_root
if keyword_set(verbose) then $
  help, hrs_root

message,'Selected REGISTRY: '+hrs_root,/info

return, hrs_root
end