;+
; PROJECT:
;        HELIO
; NAME:
;        make_hrs_stc
; PURPOSE:
;        Creates a structure containing all the nodes for each of the HELIO services
; CALLING SEQUENCE:
;        struct = make_hrs_stc( [/verbose] )
; CATEGORY:
;        Registry
; INPUTS:
;        
; INPUT KEYWORDS:
;        verbose          Outputs additional information for debug purposes
; OUTPUTS:
;        struct           Structure containing the information with a record per service 
; OUTPUT KEYWORDS:
;        
; RESTRICTIONS:
;        
; HISTORY:
;           Aug-2014  rdb  written
;        17-Apr-2014  rdb  added SMS service and some additional documentation
;                          pass verbose keyword through to "hio_get_servroots"
;
;-

function make_hrs_stc, verbose=verbose

serv = ['HEC','HFC','ICS','ILS','UOC','CXS','DPAS','DES','SMS']
nserv = n_elements(serv)

temp = {service:'', n_nodes:0, nodes:strarr(6), node_flags:intarr(6)-3}
hrs_roots = replicate(temp, nserv) 

;    make a normal request to the HELIO Registry Service (HRS)

zz = hio_get_servroots(verbose=verbose) 

;    populate the structure from the response

for j=0,nserv-1 do begin

  cserv = serv(j)
  hrs_roots(j).service = cserv
  
  hio_cserv = strlowcase('helio-' + cserv)
  cnodes = zz(where(strpos(zz, hio_cserv) gt 0, nx))

  if nx gt 0 then begin
    for k=0,nx-1 do cnodes(k) = gt_brstr(cnodes(k)+'/', 'http://', '/')
    hrs_roots(j).n_nodes = nx                                      
    hrs_roots(j).nodes = cnodes
    hrs_roots(j).node_flags = intarr(nx) 					; initially flag OK ??
    
;    mark nodes on festung3 as unavailable    
    f3 = where(strpos(cnodes, 'festung3') gt 0, nf3)
    if nf3 gt 0 then hrs_roots(j).node_flags(f3) = -2
  endif
  
endfor

;    For some reason, some services do not show up in the query to the HRS
;    Retrieve CXS, DES and SMS from text output from the Registry

cxs_nodes = get_hrstext_roots('CXS', count=n_cxs)
qx = where(hrs_roots.service eq 'CXS', nqx)
if nqx eq 1 then begin
    hrs_roots(qx(0)).n_nodes = n_cxs                                      
    hrs_roots(qx(0)).nodes = cxs_nodes  
    hrs_roots(qx(0)).node_flags = intarr(n_cxs)   			; initially flag OK ??
endif

des_nodes = get_hrstext_roots('DES', count=n_des)
qx = where(hrs_roots.service eq 'DES', nqx)
if nqx eq 1 then begin
    hrs_roots(qx(0)).n_nodes = n_des                                      
    hrs_roots(qx(0)).nodes = des_nodes  
    hrs_roots(qx(0)).node_flags = intarr(n_des)		  		; initially flag OK ??
endif

ses_nodes = get_hrstext_roots('SMS', count=n_ses)
qx = where(hrs_roots.service eq 'SMS', nqx)
if nqx eq 1 then begin
    hrs_roots(qx(0)).n_nodes = n_ses                                      
    hrs_roots(qx(0)).nodes = ses_nodes  
    hrs_roots(qx(0)).node_flags = intarr(n_ses)		  		; initially flag OK ??
endif

if keyword_set(verbose) then begin
  help, hrs_roots
  help, hrs_roots,/st
  for j=0,nserv-1 do print, hrs_roots(j).service, hrs_roots(j).node_flags, format='(a,t10,6i4)
endif

return, hrs_roots
end
