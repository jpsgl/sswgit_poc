dpas_valid = hio_get_dpaspat() 

sql = "select * from instrument"     
query = hio_form_ics_query(sql=sql)
vv = ssw_hio_query(query, /conv)   

ics = add_tag(vv,'','valid')

for j=0,n_elements(dpas_valid)-1 do begin
 qv = where(ics.obsinst_key eq dpas_valid(j),nqv)
 if nqv eq 1 then ics(qv(0)).valid=1
endfor

qx = where(ics.valid eq 1, nqx)
;help,qx,dpas_valid

ics_valid = ics(qx).obsinst_key
;ics_valid = ics_valid(sort(ics_valid))
help,ics_valid,dpas_valid

file_append,'compare_ics_dpas.txt','# compare entries in the DPAS/PAT with records in the ICS',/new

for j=0,n_elements(dpas_valid)-1 do begin
  qz = where(ics_valid eq dpas_valid(j),nqz)
  print, dpas_valid(j), qz(0)
  file_append,'compare_ics_dpas.txt',string(dpas_valid(j), qz(0), format='(a,t40,i4)')
endfor

end