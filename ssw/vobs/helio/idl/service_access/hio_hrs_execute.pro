;    18-08-2011  rdb  written
;    20-08-2011  rdb  Clean up temporay file
;    30-01-2012  rdb  Find Registry by polling list determined from file
;    24-02-2012  rdb  Trap no registry found
;    14-03-2012  rdb  Add registry root to !hio_sysvar
;    06-08-2012  rdb  added verbose swicth in call to hio_hrs_find

function hio_hrs_execute, xml_file, reset=reset, verbose=verbose

ccstart = systime(/sec)     ; ++++++ timing information

message,'Making a request to the HELIO HRS',/info

if keyword_set(verbose) then help, xml_file

if keyword_set(verbose) then begin
  temp = str_replace(xml_file,'&lt;','<')
  temp = str_replace(temp,'&gt;','>')
  print,temp, format='(a)'
endif

temp_dir = get_temp_dir()
ofile = concat_dir(temp_dir,'qq_hrs.xml')		;?? randomize
file_append, ofile,/new, xml_file

;   find the HELIO Registry

hrs_root = hio_hrs_find(reset=reset, verbose=verbose)
;help, hrs_root
if hrs_root eq '' then message,'MAJOR problem - UNABLE to locate HELIO Registry'
!hio_sysvar.hrs_root = hrs_root

;hrs_root = 'http://msslxw.mssl.ucl.ac.uk:8080'
hrs_url = hrs_root + '/helio_registry/xqueryresults.jsp'

cmd = '$HELIO_JAVA/scripts/post_hrs.sh ' + ofile + ' ' + hrs_url
if keyword_set(verbose) then print,cmd
spawn,cmd,resp                                      

if keyword_set(verbose) then help,resp

;    get rid of temporary file
if not keyword_set(verbose) then ssw_file_delete, ofile		;, delete_status

print,'>> HRS Request took:',systime(/sec)-ccstart,' secs', format='(a,f8.3,a)'

return,resp
end
