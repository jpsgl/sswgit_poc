function hio_cxsimage_cursor, info_url, boxcursor=boxcursor, verbose=verbose

;    read the information about the plot scaling

message,'Reading: '+ info_url, /info

;    read the file and put variables into the return structure

qq = rd_tfile(info_url, delim='=',/auto)
if keyword_set(verbose) then help,qq

;     possibly shoukd do this from names; types??
temp = {x0:0.0,y0:0.0,x1:0.0,y1:0.0,time_ref:'',x_unit:'',y_unit:''}
for j=0,6 do temp.(j)=qq(1,j)
vv = temp

if keyword_set(verbose) then help,/st,vv
if datatype(vv) ne 'STC' then return, -1
;help,/st,!d

;    reconstruct the scale information
xscl = (vv.x1-vv.x0)/!d.x_size
yscl = (vv.y1-vv.y0)/!d.y_size

if not keyword_set(boxcursor) then begin

;    use the cursor with cross-hairs

;;  mouse_notes = ['Drag Left button to move box.', $
;;				'Drag Middle button near a corner to resize box. ', $
				
  mouse_notes = ['Any button to select position and exit. ']
  box_message, ['Please follow instructions below:', ' ' + mouse_notes]

  xycursor,xc,yc,/dev
  print,'Position in pixels:           ',xc,yc            

  xd = xc*xscl + vv.x0
  yd = yc*yscl + vv.y0
  print,'Position in data coordinates: ',xd,yd
  
;    >>>>  should these be a cebtre and an FOV ????

  vv = add_tag(vv, xd,'cur_xd')
  vv = add_tag(vv, yd,'cur_yd')

endif else begin

;    use a box cursor

  mouse_notes = ['Drag Left button to move box.', $
				'Drag Middle button near a corner to resize box. ', $
				'Right button when done.']
  box_message, ['Please follow instructions below:', ' ' + mouse_notes]

  box_cursor, x0,y0, nx,ny			;, /message
  print,''
  if keyword_set(verbose) then help, x0,y0, nx,ny
  
;    outline selected area
  draw_boxcorn, x0,y0,x0+nx,y0+ny, /dev
  
  xd = [x0,x0+nx]*xscl + vv.x0
  yd = [y0,y0+ny]*yscl + vv.y0
  print,'X limits in arcsec: ',xd
  print,'Y limits in arcsec: ',yd
  
;    >>>>  should these be a cebtre and an FOV ????

  vv = add_tag(vv, xd,'box_xd')
  vv = add_tag(vv, yd,'box_yd')
  
endelse

return, vv			;[xd,yd]
end