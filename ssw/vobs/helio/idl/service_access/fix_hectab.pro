function fix_hectab, struct, verbose=verbose

;--  add columns to HEC control until fixed by INAF

message,'Fixing the HEC info table',/info

;    get the information to be added as new columns

message,'Getting the information about observatory location',/info

hec_ccfile = find_setup_file('cc_hec_control_mods.csv')
print,'Reading: ', hec_ccfile
qq=rd_tfile(hec_ccfile,/auto,delim=',')
nrow = (size(qq))(2)-1

;    form the structure and populate it
ett='tstr={'+arr2str(qq(*,0)+":''")+'}'
temp = execute(ett)                      
stc = replicate(tstr,nrow)

for j=0,2 do stc.(j) = reform(qq(j,1:*))
for j=0,nrow-1 do stc(j).(2)=str_replace(stc(j).(2),'"','')
;help, nrow

;    now make the corrections

struct = add_tag(struct,'','flag')
struct = add_tag(struct,'','object')

struct.flag = '----'
;help,/st,struct

nrx = n_elements(struct)
;help, nrx

;    add the extra columns
for j=0,nrx-1 do begin
  qx = where(stc.name eq struct(j).name,nqx)
  if nqx eq 1 then begin
    struct(j).flag = stc(qx(0)).flag
    struct(j).object = stc(qx(0)).observatory
  endif
endfor

qv = where(struct.name ne '' and struct.flag eq '----', nqv)
if nqv gt 0 and keyword_set(verbose) then $
  for j=0,nqv-1 do print, struct(qv(j)).name, format='(a,4h,,,,)'


goto, endit

;    skip past this stuff - done elsewhere

;    get the date information

;;hec_dates = strip_hec_dates()
;help,hec_dates,/st
;;nrec = n_elements(hec_dates)
;help, nrec

message,'Getting corrected dates of when lists valid',/info

query = hio_form_hec_query(/status)
hec_dates = ssw_hio_query(query, /conv)
nrec = n_elements(hec_dates)
help,hec_dates,/st

;    correct the to/from dates
for j=0,nrx-1 do begin
  qx = where(hec_dates.table_name eq struct(j).name,nqx)
;  help, struct(j).name, qx(0)
  if nqx eq 1 then begin
    struct(j).timefrom = strmid(hec_dates(qx(0)).time_from,0,10)
    struct(j).timeto = strmid(hec_dates(qx(0)).time_to,0,10)
  endif
endfor   

endit:

return, struct
end
