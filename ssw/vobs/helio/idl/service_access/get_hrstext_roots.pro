function get_hrstext_roots, service, count=count, verbose=verbose

common hrstext, zz

if keyword_set(verbose) then help, zz

if n_elements(zz) eq 0 then begin

  hrs_root = 'helio.mssl.ucl.ac.uk'			; might need to use differnt registry
  hrs_url = 'http://' + hrs_root + '/helio_registry/helio_node_overview_txt.jsp

  cmd = wget_stream() + hrs_url

  spawn,cmd,resp
  help,resp

  qv = where(strlen(resp) gt 0)
  qq = resp(qv)
  nr = n_elements(qq)

  zz = strarr(2,nr)
  for j=0,nr-1 do zz(*,j)=str2arr(qq(j),'=')

endif

cserv = strupcase(service)

nodes = ''
qx = where(strpos(zz(1,*), cserv ) ge 0, nqx)
if nqx gt 0 then nodes = zz(0,qx)

count = nqx
return, nodes
end
