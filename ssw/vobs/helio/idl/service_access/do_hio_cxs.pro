pro do_hio_cxs, time, verbose=verbose, $
;	flare_location=flare_location, $	; default
;	goesplot=goesplot, $
	parker=parker, velocity=velocity, view

;    the parameters depend on the CXS task

;    11-06-2011  Written
;    01-02-2011  rdb  Updated call for Parker Spiral for new params. 

case 1 of

keyword_set(parker): begin

;--    plot parker spiral, complex

    message, 'Plot Parker Spiral (complex)', /info
    title = 'HELIO CXS:  Parker Spiral'

;;    xml_file = concat_dir('$HELIO_JAVA/templates','parkercomplex.xml')
    xml_file = concat_dir('$HELIO_JAVA/templates','ParkerFinal2.xml')
    qq = rd_tfile(xml_file)
    ;help,qq
    if n_elements(qq) eq 1 then message,'Problem locating XML file'

    time_start = anytim(time,/ccs,/trunc)
    qq1 = str_replace(qq,'xfffx',time_start)

    vel = '400'					; default to 400 km/sec
    if keyword_set(velocity) then vel=string(velocity)
    print,'Using velocity: ',vel
    qq1 = str_replace(qq1,'xvvvx',vel)

    qq1 = str_replace(qq1,'xsoux',0)
    qq1 = str_replace(qq1,'xsinx',1)
    cview = 'Normal'			; default to non-rotated view
    qq1 = str_replace(qq1,'xviewx',cview)

  end

else: begin

;--    over-plot flares on context image

    message,'Over-plot flare locations on Context Image',/info
    title = 'HELIO CXS:  Flare Locations'

    xml_file = concat_dir('$HELIO_JAVA/templates','flareplotter.xml')
    qq = rd_tfile(xml_file)
;    help,qq
    if n_elements(qq) eq 1 then message,'Problem locating XML file'

    time_start = anytim(time,/ccs,/trunc)
    qq1 = str_replace(qq,'xfffx',time_start)

  end

endcase

;    make the request to the HELIO Context Server (CXS)
ffile = hio_cxs_execute(qq1, verbose=verbose)

;    display the retuned image
date = anytim(time,/ecs,/date)
title = title +'  -  '+ date
xcom_showpng, ffile, title=title, wnum=wnum

help, wnum, title, ffile

;    should the display be optional?? 
;    return or store the image files name, etc?? (start time, vel...)


end
