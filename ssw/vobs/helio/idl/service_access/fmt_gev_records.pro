function fmt_gev_records, resp, $
	threshold_xray=threshold_xray, valid=valid

;help,/st,gev
nel = n_elements(resp)

out = strarr(8, nel)
valid = intarr(nel)-1
ss = strarr(nel)

out(0,*) = strmid(anytim(resp.time_start,/ecs,/trunc),0,16)
out(1,*) = strmid(anytim(resp.time_peak ,/ecs,/trunc),0,16)
out(2,*) = strmid(anytim(resp.time_end  ,/ecs,/trunc),0,16)

knt=0
for j=0,nel-1 do begin
gev = resp(j)

nar = gev.nar
temp = string(nar, format='(i6)')
if nar lt 0 then temp='      '
  out(3,j) = temp

lat_hg = gev.lat_hg
temp = string(lat_hg, format='(f7.2)')
if nar lt 0 and lat_hg eq 0 then temp='       '
  out(4,j) = temp
long_hg = gev.long_hg
temp = string(long_hg, format='(f8.2)')
if nar lt 0 and long_hg eq 0 then temp='        '
  out(5,j) = temp
long_carr = gev.long_carr
temp = string(long_carr, format='(f8.2)')
if nar lt 0 and long_carr eq 0 then temp='        '
  out(6,j) = temp
if lat_hg eq 0 and long_hg eq 0 then begin
  out(4,j) = '       '
  out(5,j) = '        '
  out(6,j) = '        '
endif

temp = gev.xray_class
  out(7,j) = temp

if temp lt threshold_xray then continue

valid(j) = j

ss(j) = arr2str(out(*,j),delim='   ')+'  '
knt++

endfor

help,nel,knt
;help,valid

return, ss
end
