;+
; PROJECT:
;        HELIO
; NAME:
;        get_nar_timerange
; PURPOSE:
;        Make a request to the HEC for the interval that am Active Region was on the solar disk
;        Add a time window???
;    
; CALLING SEQUENCE:
;        get_nar_timerange, nar  [,struct=struct] [,window=window] [,/debug]
; CATEGORY:
;    
; INPUTS:
;        nar          Required NOAA Actuve Region (NAR) number
; INPUT KEYWORDS:
; ;;;;       window       Time to added before and after interval derived from HEC ???? 
;        make_html    Produce an HTML page summarizing the structure returned by the HEC
;        debug        Provides extra information
; OUTPUTS:
;
; OUTPUT KEYWORDS:
;        struct       Returns the NAR structure 
; RESTRICTIONS:
;    
; HISTORY:
;    27-Jul-2014  rdb  written - based on CXS NAR movie code
;    25-Oct-2014  rdb  Added the struct keyword
;    09-Apr-2016  rdb  small mod to debug messages
;    14-Jun-2016  rdb  added "order by time_start" phrase to SQL
;
;-

function get_nar_timerange, rqnar, window=window, struct=struct, debug=debug, make_html=make_html

;    default if AR No. not supplied

if n_params() eq 0 then rqnar = '12087'

nar = strtrim(string(rqnar),2)
help, nar

;    form a query to the HEC and submit

message, '>>>>  Requesting details of the NAR from the HEC', /info

nar_clause = "nar="+nar

sql = "select * from noaa_active_region_summary where " + nar_clause
sql = sql + " order by time_start"				; need records in time order...
print,sql

query = hio_form_hec_query(sql=sql)
vv = ssw_hio_query(query, /conv)
if keyword_set(debug) then begin
  help,vv,/st
  help,vv
endif

struct = ''
if datatype(vv) eq 'STC' then begin

;    should this be after the removel of the returning regions?

  if keyword_set(make_html) then hio_stc2html, vv, file='NAR-resp.html', title='NAR Information returned by the HEC'
  
;  qv = where(vv.region_type eq 'REGIONS WITH SUNSPOTS', nqv)
  qv = where(vv.region_type ne 'REGIONS DUE TO RETURN', nqv)
  if nqv gt 0 then vv = vv(qv) $
  else begin
    box_message,'*** Problem, AR records problem'
    return, -1
  endelse
  if keyword_set(debug) then help,vv
  
  tsta = anytim(min(vv.time_start), /ecs)
  tend = anytim(max(vv.time_start), /ecs)
  
;    add a time window ?????????????
  
  ndays = fix(addtime(tend,diff=tsta)/60./24.)+1

  if keyword_set(debug) then begin
    fmt_timer,[tsta,tend]
    help,ndays
    help,nar
  endif
  struct = vv

endif else begin
  box_message,'*** Problem, AR may be out of range'
  return, -1
  
endelse

return, [tsta, tend]
end