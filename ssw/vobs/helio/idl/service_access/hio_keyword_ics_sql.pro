function hio_keyword_ics_sql, timerange, verbose=verbose, $
	remote=remote, insitu=insitu, $
	energetic=energetic, $
	object=object, $
	sun=sun, mercury=mercury, venus=venus, earth=earth, mars=mars, $
	heliosphere=heliosphere		; ?????

;    --    SELECT

qcols ='*'
;if keyword_set(instrument) then $
;  qcols = "obsinst_key,date(time_start) as time_start,date(time_end) as time_end,inst_od1,inst_od2,inst_type,inst_oe1,inst_oe2,keywords"

;    --    FROM

;qcat = 'observatory'
;if keyword_set(instrument) then $
  qcat = 'instrument'

;    --    WHERE

;    default times
date_from = '2003-10-25T22:00'
date_to = '2003-10-31T04:00'

;help,timerange
print,timerange

if n_params() eq 1 then begin
  date_from = anytim(timerange(0),/ccs,/trunc)
  date_to = anytim(timerange(1),/ccs,/trunc)
endif

;??    shorten time string

if keyword_set(verbose) then fmt_timer,[date_from, date_to]

qwhere = "'" +date_from+ "'<=time_end and '" +date_to+ "'>=time_start"


;;inst_type = 'remote'	
;;if keyword_set(insitu) then 


inst_type = 'in-situ'

;    select observed object and observation type

;oe1 = '' & tor = ''
inst_od1 = ''
case 1 of
  keyword_set(sun):	begin
      inst_od1 = 'Sun'
      inst_type = 'remote'
    end	
  keyword_set(mercury):	inst_od1 = 'Mercury'
  keyword_set(venus):	inst_od1 = 'Venus'
  keyword_set(earth):	inst_od1 = 'Earth'
  keyword_set(mars):	inst_od1 = 'Mars'
  keyword_set(object):	begin
      cobject = strlowcase(object) 
      if cobject eq 'mercury' or cobject eq 'venus' or cobject eq 'earth' or cobject eq 'mars' $
        then inst_od1 = cobject
    end
  else:		inst_od1 = ''
endcase  

if inst_od1 ne '' then begin
;    od1 = od1 + tor + "inst_od1 LIKE '%" + od1_values(qv(j)) + "%'"

  qwhere = qwhere + " AND inst_od1 LIKE '%" + inst_od1 + "%'"
;  qwhere = qwhere + ' AND ' + "inst_od1='" + inst_od1 + "'"
endif else begin
qwhere = qwhere + ' AND ' + "observatory_name='" + strlowcase(object) + "'"
endelse
  qwhere = qwhere + ' AND ' + "inst_type='" + inst_type + "'"


;    other where clauses

;    keys = keys + tor + "keywords LIKE '%" + key_values(qv(j)) + "%'"

if inst_type eq 'in-situ' then begin
  keys = '' & tor = '' 
  if not keyword_set(energetic) then begin
    key_values = ['magnet','plasma']
    for j=0,n_elements(key_values)-1 do begin
      keys = keys + tor + "keywords LIKE '%" + key_values(j) + "%'"
      tor = " OR "
    endfor
    qwhere = qwhere + ' AND (' + keys + ')'
  endif else begin
    qwhere = qwhere + " AND (inst_oe2 LIKE '%energ%')"
  endelse
endif

;    --->    form SQL Query by combining the strings

sql = "SELECT " +qcols+ " FROM " +qcat+ " WHERE " +qwhere

psql = sql
psql = str_replace(psql,  "%","%25")
sql = psql
if keyword_set(verbose) then print,sql

return, sql

end