function hio_form_uoc_query, timerange, sxi=sxi, instrument=instrument, $
		listname=listname, $
		show_tables=show_tables, describe_table=describe_table, $
		verbose=verbose

;    16-Jan-2007  rdb  Added proper termination of cmd. 

;
;  * select * from search_uoc_simple(2452982.298,2452982.350,0,0,1500,3,300); *
;
;  Input Parameters:
;  1) time_start (JD)
;  2) time_end (JD)
;  3) X center
;  4) Y center
;  5) half size of square side
;  6) Spec Range start
;  7) Spec Range end
;

message,'Forming query for HELIO UOC',/info

list = 'planetary_cat'
if keyword_set(listname) then list = listname
list = strtrim(list,2)		; make sure no leading or trailing blanks

;    Syntax of some system commands for PostgreSQL is different to mySQL

;    List available tables
if keyword_set(show_tables) then begin
  message,'SHOW TABLES',/info
  com = "SELECT table_name FROM information_schema.tables WHERE table_schema='public'"
  goto, go_psql
endif

;    Postgres equivalent of DESCRIBE
if keyword_set(describe_table) then begin
  message,'DESCRIBE TABLE '+list,/info
  com = "SELECT column_name FROM information_schema.columns WHERE table_name='" +list+ "'"
  goto, go_psql
endif

;>>>>>>>>>>>>>>>>>>>>>
;    this is mostly rubbish

;  Need to supply dates in Julian Days
t0 = anytim2jd(timerange(0))
t1 = anytim2jd(timerange(1))
jd0 = double(t0.int)+t0.frac
jd1 = double(t1.int)+t1.frac

if keyword_set(verbose) then begin
  fmt_timer, timerange
  print,'Julian Date range:',jd0,jd1, format='(a,2f14.4)'
  print,''
endif

jtimes = string(jd0,jd1,format='(f12.4,1h,,f12.4)')
cmd = "SELECT * FROM search_uoc_simple(" + jtimes +  ",-1,-1,-1,-1,-1)"

;;if not keyword_set(sxi) then cmd = cmd + " where instrument='TRACE';" $
;;    else cmd = cmd + " where instrument='SXI';"
if not keyword_set(instrument) then begin
  
  if not keyword_set(sxi) then $
    which_instrument = " where instrument='TRACE';" else $
    which_instrument = " where instrument='SXI';"
    
endif else begin
;  need to check for valid instruments....    
  which_instrument = " where instrument='"+strupcase(strtrim(instrument,2))+"';"
    
endelse

cmd = cmd + which_instrument    
;print,cmd

;>>>>>>>>>>>>>>>>>>>>>

go_psql:
psql = '&sql='+com

;    concatonate the URL
root = 'http://festung1.oats.inaf.it:8081/stilts/task/sqlclient'
rdtb = '?db=jdbc:postgresql://festung1.oats.inaf.it/uoc&user=apache'
ofmt = '&ofmt=votable'

url  = root + rdtb + psql + ofmt
if keyword_set(verbose) then print, url

return, url

end

