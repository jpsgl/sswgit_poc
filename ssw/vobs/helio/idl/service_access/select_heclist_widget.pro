;+
; NAME:
;    select_heclist_widget
; PURPOSE:
;    
; CALLING SEQUENCE:
;    
; CATEGORY:
;    
; INPUTS:
;    
; INPUT KEYWORDS:
;    
; OUTPUTS:
;    
; OUTPUT KEYWORDS:
;    
; RESTRICTIONS:
;    
; HISTORY:
;    11-Apr-2016  rdb  written
;
;-

;---------------------------------------------------------------
pro do_hec_widget_event, event

common hec_comm, hec_tabinfo, tab_selected

uname = WIDGET_INFO( event.id, /uname ) 
widget_control, event.top, get_uvalue = info, /no_copy

if uname ne 'ilist' then begin
  widget_control, event.id,  get_value = buttonvalue
endif else begin
  print, uname
  if info.verbose eq 1 then help, event
  print, info.ilist_values(event.index)		;+'*'
  tab_selected = info.ilist_array(event.index)
  info.selected = tab_selected
  if info.verbose eq 1 then $
  	help, info

  widget_control, event.top, set_uvalue = info, /no_copy
  return
endelse

;help,event & help,event,/st
;help,info  & help,info,/st

nbt = n_elements(buttonvalue)
fmt = '(a, t10, ' + strtrim(string(nbt),2) + 'i6)'
print, string(uname, buttonvalue, format=fmt)

if uname eq 'od1a' then info.od1a_array = buttonvalue
;if uname eq 'od1b' then info.od1b_array = buttonvalue

if uname eq 'od2a' then info.od2a_array = buttonvalue
;if uname eq 'od2b' then info.od2b_array = buttonvalue

if uname eq 'ityp' then begin
  info.ityp_array = buttonvalue
  info.rem_insitu = buttonvalue
endif

if uname eq 'ilist' then info.ilist_array = buttonvalue
;if uname eq 'oe1' then info.oe1_array = buttonvalue

;if uname eq 'oe2a' then info.oe2a_array = buttonvalue
;if uname eq 'oe2b' then info.oe2b_array = buttonvalue
;if uname eq 'oe2c' then info.oe2c_array = buttonvalue

;if uname eq 'keya' then info.keya_array = buttonvalue
;if uname eq 'keyb' then info.keyb_array = buttonvalue

widget_control, event.top, set_uvalue = info, /no_copy
end

;---------------------------------------------------------------
pro do_hec_widget_filter, event

common hec_comm, hec_tabinfo, tab_selected

widget_control, event.top, get_uvalue = info, /no_copy

;if info.verbose eq 1 then help, info					;& help,info,/st
if info.verbose eq 1 then help, hec_tabinfo, /st

date_from = info.time_start
date_to = info.time_end

flags = intarr(n_elements(hec_tabinfo))

;    Type of Event list

flags1 = intarr(n_elements(hec_tabinfo))
qv = where(info.od1a_array gt 0, nqv)
;print, qv
if nqv gt 0 then begin
  print, info.od1a_values(qv)
  
  if info.od1a_array(0) eq 1 then begin
    qx0 = where(strlowcase(hec_tabinfo.flare) eq 'y', nqx)
    if nqx gt 0 then flags1(qx0) = flags1(qx0)+1
  endif
  if info.od1a_array(1) eq 1 then begin
    qx1 = where(strlowcase(hec_tabinfo.cme) eq 'y', nqx)
    if nqx gt 0 then flags1(qx1) = flags1(qx1)+2
  endif
  if info.od1a_array(2) eq 1 then begin
    qx2 = where(strlowcase(hec_tabinfo.swind) eq 'y', nqx)
    if nqx gt 0 then flags1(qx2) = flags1(qx2)+3
  endif
  if info.od1a_array(3) eq 1 then begin
    qx3 = where(strlowcase(hec_tabinfo.part) eq 'y', nqx)
    if nqx gt 0 then flags1(qx3) = flags1(qx3)+4
  endif
endif else flags1 = flags1+1

;    Domain where event was detected

flags2 = intarr(n_elements(hec_tabinfo))
qv = where(info.od2a_array gt 0, nqv)
;print, qv
if nqv gt 0 then begin
  print, info.od2a_values(qv)
  
  if info.od2a_array(0) eq 1 then begin
    qx0 = where(strlowcase(hec_tabinfo.solar) eq 'y', nqx)
    if nqx gt 0 then flags2(qx0) = flags2(qx0)+1
  endif
  if info.od2a_array(1) eq 1 then begin
    qx1 = where(strlowcase(hec_tabinfo.ips) eq 'y', nqx)
    if nqx gt 0 then flags2(qx1) = flags2(qx1)+2
  endif
  if info.od2a_array(2) eq 1 then begin
    qx2 = where(strlowcase(hec_tabinfo.geo) eq 'y', nqx)
    if nqx gt 0 then flags2(qx2) = flags2(qx2)+3
  endif
  if info.od2a_array(3) eq 1 then begin
    qx3 = where(strlowcase(hec_tabinfo.planet) eq 'y', nqx)
    if nqx gt 0 then flags2(qx3) = flags2(qx3)+4
  endif
endif else flags2 = flags2+1

;    Observation type can be remote or in-situ

flags3 = intarr(n_elements(hec_tabinfo))
;print, 'info.ityp_array: ', info.ityp_array
;print, 'info.ityp_values: ', info.ityp_values
if info.rem_insitu ge 0 then begin
  if info.rem_insitu eq 0 then qx = where(strlowcase(hec_tabinfo.otyp) eq 'r', nqx)
  if info.rem_insitu eq 1 then qx = where(strlowcase(hec_tabinfo.otyp) eq 'i', nqx)
  if info.rem_insitu eq 2 then begin
    nqx = n_elements(hec_tabinfo) & qx = indgen(nqx)
  endif
  if nqx gt 0 then flags3(qx) = 1
endif else flags3 = flags3+1

message,'Applying Filters',/info
;print, flags1
;print, flags2
;print, flags3

qxa = where(flags1 gt 0 and flags2 gt 0 and flags3 gt 0, nqxa)
help, nqxa		; qxa, 
clist_descr = '' & clist_names = ''
if nqxa gt 0 then begin
  clist_descr = hec_tabinfo(qxa).description
  clist_names = hec_tabinfo(qxa).name
endif

info.ilist_values = clist_descr
info.ilist_array = clist_names 

widget_Control, info.listID, Set_Value=clist_descr, $
         Set_UValue=clist_descr

legend = info.leg3 + string(nqxa, format='(2x,1h(, i3, 1h))')
widget_Control, info.ban3ID, Set_Value=legend, Set_UValue=legend

goto, endit

if info.pause eq 0 then  do_hec_widget_quit,event

endit:
widget_control, event.top, set_uvalue = info, /no_copy

end

;---------------------------------------------------------------
pro do_hec_widget_quit,event

  widget_control, event.top, /destroy

end

;---------------------------------------------------------------
function do_hec_widget_init, info, pause=pause

common hec_comm, hec_tabinfo, tab_selected

tab_selected = ''

text_info = ['','This Widget allows you to select an Event List applying Filters if needed', $
	'']
box_message, text_info


base = WIDGET_BASE(/column, event_pro='do_hec_widget', $
	title='HELIO Heliopysics Event Catalogue (HEC) list selector')			; , /grid

info_text1 = 'This Widget allows you to select an Event List. Filters can be applied if so desired'
Result = WIDGET_LABEL(base, value=info_text1,/align_center)

;--    Event Type

od1a_values  = ['Flare', 'CME  ', 'Solar Wind', 'Particle'] 
bgroup1  = CW_BGROUP(base, od1a_values, uname='od1a', /NONEXCLUSIVE, /row, $
		LABEL_left='Event Type:              ') 


;--    Observing Domain

od2a_values  = ['Solar', 'IPS  ', 'Geospace  ', 'Planetary'] 
bgroup2  = CW_BGROUP(base, od2a_values, uname='od2a', /NONEXCLUSIVE, /row, $
		LABEL_left='Observing Domain:        ')


;--    Instrument Type

ityp_values = ['Remote ', 'In-situ', 'Both   ']   
bgroup3 = CW_BGROUP(base, ityp_values, uname='ityp', /EXCLUSIVE, /row, $
		LABEL_left='Observation Type:        ', set_value=2, /FRAME)


Result = WIDGET_LABEL(base, value='  ',/align_center)			; blank line
legend2 = 'Time Range:                                          '
ban2 = WIDGET_LABEL(base, value=legend2 ,/align_left)	
legend2a = 'Search String:                                       '
ban2a = WIDGET_LABEL(base, value=legend2a ,/align_left)	
Result = WIDGET_LABEL(base, value='  ',/align_center)			; blank line

fin_text = ['Form Request, Execute and Exit','Quit']
;if keyword_set(pause) then $
	fin_text = ['Apply Filters','Select / Quit']

result = WIDGET_BUTTON(base, value=fin_text(0), event_pro = 'do_hec_widget_filter', ysize=40)
;;result = WIDGET_BUTTON(base, value=ColorButtonBitmap(fin_text(0), FGCOLOR='black', BGCOLOR='yellow'), $
;;	event_pro = 'do_hec_widget_filter', ysize=40)

Result = WIDGET_LABEL(base, value=' ' ,/align_center)			; blank line

legend3 = 'Event Lists matching selection criteria, including supplied date range'
legend = legend3 + string(n_elements(hec_tabinfo), format='(2x,1h(, i3, 1h))')
ban3 = WIDGET_LABEL(base, value=legend,/align_center)

if n_elements(size_font) eq 0 then size_font=15
clist = hec_tabinfo.description
cname = hec_tabinfo.name
listid = widget_list(base, value=clist, uname='ilist', /frame, ysize=12, $  ;, scr_ysize=90
            font=get_xfont(/only_one,/fixed,closest=size_font))

Result = WIDGET_LABEL(base, value='  ',/align_center)

Quitid = widget_button(base, value=fin_text(1), event_pro = 'do_hec_widget_quit', ysize=40)

size_font = 15		;13
font=get_xfont(/only_one,/fixed,closest=size_font) 

WIDGET_CONTROL, base, /REALIZE		;, default_font=font

;;help,clist, od1a_values
help, base

info = {od1a_values:od1a_values, od1a_array:intarr(n_elements(od1a_values)), $
	od2a_values:od2a_values, od2a_array:intarr(n_elements(od2a_values)), $
	ityp_values:ityp_values, ityp_array:intarr(n_elements(ityp_values)), $

	ilist_values:clist, ilist_array:cname, $
	listID:listid, $												; Event lists matching search criteria

	time_start:'', time_end:'', search_string:'', $
	rem_insitu:-1, selected:'', $
	baseID:base, $
	ban2ID:ban2, leg2:legend2, ban2aID:ban2a, leg2a:legend2a, $		; Time Range, Search String
	ban3ID:ban3, leg3:legend3, $									; banner of no. of lists
	full:0, verbose:0, pause:0}


return, base
end

;---------------------------------------------------------------

function select_heclist_widget, timerange=timerange, search_string=search_string, $
	use_tabinfo=use_tabinfo, default_times=default_times, $
	pause=pause, verbose=verbose, force_reload=force_reload			; full=full, no_times=no_times

common hec_comm, hec_tabinfo, tab_selected

;if n_elements(hec_tabinfo) lt 0 or keyword_set(force_reload) then $
	hec_tabinfo = -1
if keyword_set(use_tabinfo) then begin
  message,'Using supplied HEC Master Catalogue',/info
  hec_tabinfo = use_tabinfo
endif

;    if we do not have a structure by now, load from the HEC

if datatype(hec_tabinfo) ne 'STC' then begin
  message,'Reading the HEC Master Catalogue',/info
  query = hio_form_hec_query(/char)	; get HEC table characteristics
  hec_tabinfo = ssw_hio_query(query, /conv, verbose=verbose)
;    temporary fix!!!!
;?????  set all qualification chars to lower case
  hec_tabinfo = fix_hectab(hec_tabinfo, verbose=verbose)
  hec_tabinfox = hec_tabinfo
endif

;    filter the lists using the supplied time range

trange = 0
if keyword_set(timerange) then trange = timerange
if keyword_set(default_times) then trange = ['12-jan-2005', '28-jan-2005']

if n_elements(trange) eq 2 then begin
  print,'** Time Selection, using time period as filter'

  trange = anytim(trange, /ccs, /date)
  fmt_timer, trange
  if keyword_set(verbose) then print, trange

  ccc = hec_tabinfo
  nqt = n_elements(ccc)  & help, nqt
  pnts = intarr(nqt)
  for jk=0,nqt-1 do begin
    if keyword_set(verbose) then $
      print,'List:   ',ccc(jk).name, ccc(jk).timefrom,ccc(jk).timeto, format='(a,a,t38, 2a12)'
    if (trange(0) le ccc(jk).timeto) and (trange(1) ge ccc(jk).timefrom) then begin
      if keyword_set(verbose) then $
        print,ccc(jk).name, jk, format='(a32,i6)'
      pnts(jk) = 1
    endif
  endfor
  qv = where(pnts eq 1, nqv)
  help, nqv
  hec_tabinfo = hec_tabinfo(qv)

endif else print,'** No time selection, using all HEC records'
;help, hec_tabinfo 

;    filter the lists using the supplied search string

if keyword_set(search_string) then begin
  print,'** Filter using Search String'
  
  descriptions = strlowcase(hec_tabinfo.description)
  qv = where(strpos(descriptions, strlowcase(search_string)) ge 0, nqv)
  help, nqv
  if nqv le 0 then begin
    box_message,['** NO lists match Search String **']
    return, -1
  endif
  if nqv gt 0 then hec_tabinfo = hec_tabinfo(qv)
end
;help, hec_tabinfo 


;    Set up Widget

message,'HELIO HEC Selection Widget',/info

base = do_hec_widget_init(info, pause=pause)

;    time not really used internally except as legend

if n_elements(trange) eq 2 then begin
  info.time_start = anytim(trange(0),/ccs,/trunc)
  info.time_end = anytim(trange(1),/ccs,/trunc)
;;help,info,/st
legend = 'Time Range:                ' + anytim(info.time_start, /ccs,/date) + ' to ' + anytim(info.time_end, /ccs,/date)
;;print, legend
info.leg2 = legend
widget_Control, info.ban2ID, Set_Value=legend, Set_UValue=legend
endif

if keyword_set(full) then info.full = 1			; purpose?
if keyword_set(pause) then info.pause = 1
if keyword_set(verbose) then info.verbose = 1
if keyword_set(search_string) then begin
  info.search_string = search_string
  legend = 'Search String:             ' + search_string
  widget_Control, info.ban2aID, Set_Value=legend, Set_UValue=legend
endif
;help,info,/st

size_font = 15		;13
font=get_xfont(/only_one,/fixed,closest=size_font) 

widget_control, base, set_uvalue=info, /no_copy, default_font=font
XMANAGER, 'do_hec_widget', base 

message,'Back from Widget',/info

return, tab_selected
end
