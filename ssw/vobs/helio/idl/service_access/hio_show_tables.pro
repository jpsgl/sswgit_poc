;+
; NAME:
;	hio_show_tables
; PURPOSE:
;	Returns tables in the selected HELIO service that manages at 
;	relational database, including HEC, ICS, ILS, HFC
; CALLING SEQUENCE:
;	tables = hio_show_tables(/hec)
;
; INPUTS:
;	none
; KEYWORDS
;       ics, ils, hec, hfc   [default is ICS]
;	verbose		Added debug messages
; HISTORY:
;      26 Mar 2011  RDB  First version
;       7 Jul 2011  XB   Added HFC option
;
;-

function hio_show_tables, hec=hec, ics=ics, ils=ils, hfc=hfc, $
	verbose=verbose

case 1 of
  keyword_set(ics): cmd = 'query = hio_form_ics_query(/show)'
  keyword_set(ils): cmd = 'query = hio_form_ils_query(/show)'
  keyword_set(hec): cmd = 'query = hio_form_hec_query(/show)'
  keyword_set(hfc): cmd = 'query = hio_form_hfc_query(/show)'
  else: cmd = 'query = hio_form_ics_query(/show)'
endcase

if keyword_set(verbose) then print,cmd
temp = execute(cmd)
resp = ssw_hio_query(query, /conv)

if keyword_set(ics) then begin
;    could just extract different tag name...
  if keyword_set(verbose) then begin
    help,resp
    print,tag_names(resp)
  endif
  resp = rep_tag_name(resp,'TABLES_IN_HELIO_ICS_ILS', 'table_name',found=found)
  if keyword_set(verbose) then help,found
endif 

if keyword_set(verbose) then begin
  help,resp          
  print,tag_names(resp)
endif

names = all_vals(resp.table_name)                                                                   

return, names
end