;    Convert the GEV returned from RD_GEV into HEC complient structure
;
;    13-Mar-2012  rdb  Written

; rd_gev,times(0),times(1),gev
; vv = hio_conv_gev2hec(gev)
; hio_setup -> set_param,eventlist=vv
;    also need to set flag that list is loaded!!
; (*self.eventlist_table) = vv
; self.eventlist_name = 'GEV list from GSFC'  ;hec_list
; self.flag_new_etimes = 0			;reset flag



function hio_conv_gev2hec, gev

temp={hec_id:-1, time_start:'', time_peak:'', time_end:'', $
	nar:0, lat_hg:0.0, long_hg:0.0, long_carr:0.0, $
	xray_class:'', optical_class:''}

sxr_flarec = ''

;;qv = where(gev.location(0) ne -999, nqv)
nqv = n_elements(gev)
qv = indgen(nqv)

if nqv gt 0 then begin
  sxr_flare = replicate(temp, nqv)
  
  for j=0,nqv-1 do begin
    cgev = gev(qv(j))
    sxr_flare(j).time_start = anytim(cgev, /ecs)
    sxr_flare(j).time_peak = anytim(addtime(anytim2ex(cgev),delta=cgev.peak/60), /ecs)
    sxr_flare(j).time_end = anytim(addtime(anytim2ex(cgev),delta=cgev.duration/60), /ecs)
    sxr_flare(j).long_hg = cgev.location(0)
    sxr_flare(j).lat_hg = cgev.location(1)
;    could fill in the Carrington longitude values if needed...
    sxr_flare(j).xray_class = string(cgev.ST$CLASS)
    sxr_flare(j).optical_class = string(cgev.ST$HALPHA)
  endfor
endif

return, sxr_flare
end
