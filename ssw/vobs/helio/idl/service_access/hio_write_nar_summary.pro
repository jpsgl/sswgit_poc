pro hio_write_nar_summary, nars, use_suffix=use_suffix, outdir=outdir

suffix = ''
if keyword_set(use_suffix) then suffix = use_suffix

;    save list of regions on the disk; when no regions need to flag

qx = where(nars.region_type ne 'REGIONS DUE TO RETURN', nqx)
if nqx ge 1 then begin
  curr_nars = all_vals(nars(qx).nar)
  curr_nars = arr2str(strtrim(string(curr_nars),2), delim=';')
endif else curr_nars = 'None'

print, 'Current Regions:  ',curr_nars
;; ofile = 'regions_current.txt'
;; file_append, ofile, curr_nars, /new

;    also returning regions; when no regions need to flag

qx = where(nars.region_type eq 'REGIONS DUE TO RETURN', nqx)
if nqx ge 1 then begin
  due_nars = all_vals(nars(qx).nar)
  due_nars = arr2str(strtrim(string(due_nars),2), delim=';')
endif else due_nars = 'None'

print, 'Regions due to Return:  ',due_nars
;; ofile = 'regions_due.txt'
;; file_append, ofile, due_nars, /new

;    now do it as a set of php varable defintions
 
now = reltime(/now)
;;;cdate = anytim(now, /ecs, /date)
cdate = anytim(all_vals(nars.time_start),/ecs,/date)
if n_elements(cdate) gt 1 then message,'Should only be one data supplied (cdate)'

ofile = 'regions_summary' + suffix + '.html'
if keyword_set(outdir) then ofile = concat_dir(outdir, ofile)
print,'Writing file: ', ofile

file_append, ofile, "<!--  Summary information - processed " + now + "  -->",/new
file_append, ofile, "<?php $creation_date = '" + cdate + "'; ?>"
file_append, ofile, "<?php $regions_current = '" + curr_nars + "'; ?>"
file_append, ofile, "<?php $regions_due = '" + due_nars + "'; ?>"

end