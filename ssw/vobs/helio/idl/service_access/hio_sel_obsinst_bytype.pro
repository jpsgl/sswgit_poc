function hio_sel_obsinst_bytype, timerange=timerange, $
			halpha=halpha, rhelio=rhelio, magmap=magmap, sep=sep, structure=structure, $
			pat_oinst=pat_oinst, full=full, verbose=verbose, $
			test_oinst=test_oinst

if keyword_set(test_oinst) then begin
  oinst = test_oinst
  message, 'Using test string: '+ arr2str(oinst,delim=','),/info
  goto, skip_ics
endif

timer = ['2012-10-28','2012-11-03']
if keyword_set(timerange) then timer = anytim(timerange, /ccs,/date)
fmt_timer, timer
time_clause = "'" + timer(0) + "' <=time_end and '" + timer(1) + "' >=time_start"

;    assemble the type clause if keywords supplied

type_clauses = ""
if keyword_set(halpha) then type_clauses = [type_clauses, "(keywords LIKE '%H-alpha%')"]
if keyword_set(rhelio) then type_clauses = [type_clauses, "(keywords LIKE '%radioheliograph%')"]
if keyword_set(magmap) then type_clauses = [type_clauses, "(keywords LIKE '%magnetograph%')"]
if keyword_set(sep) then type_clauses = [type_clauses, "(keywords LIKE '%SEP%')"]
if keyword_set(structure) then type_clauses = [type_clauses, "(inst_od1 LIKE '%heliosphere%') AND (inst_od2 LIKE '%solar-wind%' OR inst_od2 LIKE '%structure%')"]
;;help, type_clauses

if n_elements(type_clauses) eq 1 then begin
  message,'NO instruments types identified',/info
  return, -1
endif 
type_clause = type_clauses
if n_elements(type_clauses) gt 1 then type_clause = type_clauses(1:*)
if n_elements(type_clauses) gt 1 then type_clause = arr2str(type_clause, ' OR ')
print, type_clause

psql = "select * from instrument where " + time_clause
if type_clause ne '' then psql = psql + " and " + type_clause

if keyword_set(verbose) then print, psql

;    retrieve the list of instruments from the ICS that are valid within time range

query = hio_form_ics_query(sql=psql)
vv = ssw_hio_query(query,/conv)

if keyword_set(full) then begin
  message,'Returning unfiltered list',/info
  return, vv
endif

oinst = vv.obsinst_key

skip_ics:

;    supress the individual GONG sites

qv = where(strpos(oinst, '__GONG') lt 0, nqv)  
oinst = oinst(qv)
if keyword_set(verbose) then help, oinst

;    check if the PAT has been passed

;;if keyword_set(pat_oinst) then allowed_oinst=pat_oinst $
;;	else return, oinst

if not keyword_set(pat_oinst) then return, oinst

;    If PAT supplied, use as filter to only include instruments DPAS knows about
;    Some instruments have sub-instruments in the PAT not defined in the ICS

allowed_oinst = pat_oinst
if keyword_set(verbose) then help, allowed_oinst, oinst

    noinst = n_elements(oinst)
    valid = intarr(noinst)
    extra_oinst = ''
    for j=0,noinst-1 do begin
;    in case instrument has sub-instruments, check for match of first part of string
;    this can give some false matches
      qv = where(strpos(allowed_oinst, oinst(j)) ge 0, nqv)
      if keyword_set(verbose) then print,'%'+oinst(j)+'%', qv, format='(a,t30,20i4)'
      if nqv eq 0 then begin
        if keyword_set(verbose) then message,'>>>>>>>>>> BAD oinst value: '+oinst(j), /info

      endif else begin
;    check if the name was complete without a sub-instrument
        qx = where(allowed_oinst eq oinst(j), nqx)
        if nqx eq 1 then begin
          valid(j)=1
          continue			; no further checking
        endif
;    then check if there are sub_instruments - save if so, discard if not
        zq = allowed_oinst(qv)
        for ji = 0,nqv-1 do begin
          zx = str2arr(str_replace(zq(ji),'__','$$'),'_')
          if n_elements(zx) eq 2 then extra_oinst = [extra_oinst,zq(ji)]
        endfor
      endelse
    endfor

;    return should be valid instruments with some expanded where appropriate

    oinst = oinst(where(valid gt 0))
    if n_elements(extra_oinst) gt 1 then begin
      print, 'Expanded:', extra_oinst(1:*)
      oinst = [oinst, extra_oinst(1:*)]
    endif

if keyword_set(verbose) then help, oinst

return, oinst
end