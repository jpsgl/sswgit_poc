;+
; NAME:
;	hio_form_hec_query
;
; PURPOSE:
;	Creates the command to be issued to the HELIO Heliophysics Event Catalogue (HEC)
;   Uses the HQI interface - originally used the STILTS interface
;
; INPUTS:
;	None
;
; INPUT KEYWORDS:
;	listname			Name of the required HEC list/table
;	timerange			Time range of the query
;						(only used when querying a table)
;	show_tables			Request to list all available tables 
;	describe_table		Request to for names of parameters in a table
;	characterise_tables	Request to read the table describing the tables
;						(remote/in-situ, CME, Flare, etc.)
;	status_tables		Request to read the table giving table status
;						(dates table valid, etc.)
;	port_forward		If set then port-fowarding should be used. this changes
;						the exact format of the command
;	verbose				If set, outputs more diagnostic messages
;
; OUTPUTS:
;	query               String containg the query to subbmit to the HEC
;
; CALLING SEQUENCE:
;	query = hio_form_hec_query([listname=listname] [,timerange=timerange]
;			[,/show_tables] [,/describe_table]...)
;
; RESTRICTIONS:
;
; HISTORY:
;	  March 2011  rdb  written
;	   June 2011  rdb  added various switches
;    01-Aug-2011  rdb  Added fail-over if HEC node is down
;    07-Aug-2011  rdb  Finalized issues relate to port forwarding
;    07-Aug-2012  rdb  Added code to handle HEC in VM at MSSL
;                      Genrealized the RTDB statement
;    15-aug-2012  rdb  Switched to use the HQI style of command
;    23-aug-2012  rdb  Corrected error with /char option, corrected SQLWHERE clause
;    31-Aug-2014  rdb  Modofied formation of HQI command - only node name supplied
;                      Added the LIMIT translation
;    11-sep-2014  rdb  Removed call to strlowcase
;    08-Feb-2107  rdb  Only print some messages of !hio_sysvar.debug set
;
;-

function hio_form_hec_query, listname=listname, timerange=timerange, $
		show_tables=show_tables, describe_table=describe_table, $
		characterise_tables=characterise_tables, $
		status_tables=status_tables, $
		sql=sql, csv=csv, $
		port_forward=port_forward, $
		test_node=test_node, $
		verbose=verbose			;, short=short

if !hio_sysvar.debug eq 1 then message,'Forming query for HELIO HEC',/info

list = 'goes_xray_flare'
if keyword_set(listname) then list = listname
list = strtrim(list,2)		; make sure no leading or trailing blanks

;    Syntax of some system commands for PostgreSQL is different to mySQL

;    List available tables
if keyword_set(show_tables) then begin
  message,'SHOW TABLES',/info
  com = "SELECT table_name FROM information_schema.tables WHERE table_schema='public'"
  goto, go_psql
endif

;    Postgres equivalent of DESCRIBE
if keyword_set(describe_table) then begin
  message,'DESCRIBE TABLE '+list,/info
  com = "SELECT column_name FROM information_schema.columns WHERE table_name='" +list+ "'"
  goto, go_psql
endif

;    Table used to sort types of events  (from Excel spread sheet)
;    HQI does not return tables with sort>100 - remove clause
if keyword_set(characterise_tables) then begin
  message,'Get table characterisation information',/info
  com = "SELECT name,description,timefrom,timeto,flare,cme,swind,part,otyp,solar,ips,geo,planet,sort FROM hec_catalogue"  ;; WHERE sort<100 ORDER BY sort"
  goto, go_psql
endif

;    Get status of tables - ought to be in characteristics
if keyword_set(status_tables) then begin
  message,'Get table status information',/info
  com = "SELECT * FROM catalogues"
  goto, go_psql
endif

;    Use SQL string supplied...
if keyword_set(sql) then begin
  if !hio_sysvar.debug eq 1 then message,'Using supplied SQL query',/info
  com = sql
  goto, go_psql
endif

message,'Using TABLE '+list,/info

times = ['15-Jul-2002 00:00','15-Jul-2002 23:59:59']
if keyword_set(timerange) then $
    times = strtrim(fmt_tim(anytim(timerange,/ecs)),2)

qcols = '*'

;    formulate the SQL query
com = "SELECT " +qcols+ " FROM " + list 
com = com + " WHERE time_start BETWEEN '"+times(0)+"' AND '"+times(1)+"'"
com = com + " ORDER BY time_start"
if keyword_set(verbose) then print,com

go_psql:
goto, tryhqi

psql = '&sql='+com

;    concatonate the URL
hec_forward=1
;if hec_forward then snode = 'festung1.oats.inaf.it' else $
snode = hio_get_snode(/hec, verbose=verbose)

;    allow use of supplied node name
if keyword_set(test_node) then snode=test_node

if strpos(snode,'festung') ge 0 then begin

do_forward = 0
if snode eq 'festung4.oato.inaf.it' then do_forward=1
if snode eq 'festung1.oats.inaf.it' then do_forward=1

if keyword_set(port_forward) or do_forward then begin
  root = 'http://' + snode + '/stilts8081/task/sqlclient'
;  rdtb = '?db=jdbc:postgresql://' + snode + '/hec&user=apache'
  rdtb = '?db=jdbc:postgresql://localhost/hec&user=apache'
  psql = str_replace(psql,' ','%20')
endif else begin
  root = 'http://' + snode + ':8081/stilts/task/sqlclient'
;  rdtb = '?db=jdbc:postgresql://' + snode + '/hec&user=apache'
  rdtb = '?db=jdbc:postgresql://localhost/hec&user=apache'
endelse

endif else if strpos(snode,'mssl') ge 0 then begin

  root = 'http://' + snode + '/vmstilts/task/sqlclient'  
  rdtb = '?db=jdbc:postgresql://localhost/hec&user=apache'
  
endif

;;root = 'http://festung1.oats.inaf.it:8081/stilts/task/sqlclient'
;;rdtb = '?db=jdbc:postgresql://festung1.oats.inaf.it/hec&user=apache'

ofmt = '&ofmt=votable'
if keyword_set(csv) then ofmt = '&ofmt=csv'

if keyword_set(verbose) then begin
  print,root,rdtb
  print,psql,ofmt
endif

url  = root + rdtb + psql + ofmt
goto, endit

tryhqi:
;    convert to lower case and get rid of surpless spaces
sql = strtrim(strcompress(com),2)
;sql = strlowcase(com)
sql = str_replace(sql, 'SELECT ', 'select ')
sql = str_replace(sql, ' FROM ', ' from ')

;    replace normal clause words with required form
cc1 = str_replace(sql,'select ','?SELECT=')
cc1 = str_replace(cc1,' from ','&FROM=')
cc1 = str_replace(cc1,' where ','&SQLWHERE=')
cc1 = str_replace(cc1,' limit ','&LIMIT=')

snode = hio_get_snode(/hec, cnode=cnode, verbose=verbose)
snode = cnode
;;snode = str_replace(cnode,':8080','')

;    allow use of supplied node name
if keyword_set(test_node) then snode=test_node

if !hio_sysvar.debug eq 1 then help, snode

root = snode		; don't need to manipulate????

root = 'http://' + snode + '/helio-hec/HelioQueryService'
url = root + cc1

endit:
if keyword_set(verbose) then $
  print, url

return, url
end
