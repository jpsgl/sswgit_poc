pro hio_plot_narpanel, date=date, map, obs_inst=obs_inst, nonar=nonar, image_name=image_name

;    image_name is the name of the output image

sdate = ''
;;do_flares = 0
;;do_nars = 0

if sdate eq '' then sdate = map.time
help, obs_inst
help, /st, map

;    SolarMonitor seems to skimp on info in the FITS header...
;    Also seems to have "processed" the data in the FITS file!!!!

angstrom = '!3'+string(197B)+'!X'
if obs_inst eq 'seit_00195' then map.id = 'SOHO EIT 195'+angstrom
if obs_inst eq 'seit_00171' then map.id = 'SOHO EIT 171'+angstrom
if obs_inst eq 'saia_00193' then map.id = 'SDO AIA 193'+angstrom
if obs_inst eq 'saia_00171' then map.id = 'SDO AIA 171'+angstrom
if obs_inst eq 'swap_00174' then map.id = 'PROBA-2 SWAP 174'+angstrom
if obs_inst eq 'gong_maglc' then map.id = 'GONG Magnetogram'
if obs_inst eq 'smdi_maglc' then map.id = 'SOHO MDI Magnetogram'
if obs_inst eq 'bbso_halph' then map.id = 'BBSO H-Alpha'
help,obs_inst
;print,'map_id: ',map.id
help,/st,map
;print,head

;    try to sort out image scaling - depends on instrument...
;    comsic rays can cause problems with EIT...

cmin=0 & cmax=0
;    Does not seem to work for magnetograms, although it should...
if strmid(obs_inst,0,4) ne 'smdi' then $
;>>>>  clip_map_crange,map,cmin,cmax    ; clip CR spikes...
if strmid(obs_inst,0,4) eq 'seit' and cmax lt 2000 then cmax=2000
map.data(0,0) = 0


;-----------------------------------------------------------------
;    open plot device

if !d.name eq 'X' then begin
;  window, xsiz=900, ysiz=450
  window, xsiz=900, ysiz=550
  csiz=1.2
endif

if !d.name eq 'Z' then begin
;  device,set_resolution=[900,450]
  device,set_resolution=[900,550]
  csiz=1.0
endif

message,'CMAX b: '+string(cmax),/info

;; for EIT 4000 does not tend to show coronal holes on quiet sun...
;if oi_struct.instrument eq 'seit' then cmax = max([cmax,3500])
;;if obs_inst eq 'trce_m0171' then cmax = 256
;;if obs_inst eq 'bbso_halph' then cmax = 1500
;help,cmax

;    set colour table and scaling - depends on the instrument...

message,'Colour table and scaling - '+obs_inst, /info
log = 1
oinst = strmid(obs_inst,0,4)
help,oinst


cmin=0 & cmax=0
case 1 of

   oinst eq 'saia': begin
     message,'Colour table - SDO AIA', /info
;     load_eit_color, head    ;default to 304?
;     if obs_inst eq 'saia_00193' then eit_colors,195
;     if obs_inst eq 'saia_00171' then eit_colors,171
     if obs_inst eq 'saia_00193' then aia_lct,wavelnth=193,/load
     if obs_inst eq 'saia_00171' then aia_lct,wavelnth=171,/load

     if obs_inst eq 'saia_00193' then $
     map.data = map.data * 2.0/map.dur           ; normalize to 2 secs
     
     pmm,map.data,mm=mm
     print,'SDO AIA mm:',mm
     cmin = 50 & cmax = 7000 & pmax = mm(1)
     log=1
   end

   oinst eq 'swap': begin
     message,'Colour table - AIA like', /info
;;     load_eit_color, head    ;default to 304?
;;     if obs_inst eq 'swap_00174' then eit_colors,171
     if obs_inst eq 'swap_00174' then aia_lct,wavelnth=171,/load
     pmm,map.data,mm=mm
     print,'PROBA-2 SWAP mm:',mm
     cmin = 10 & cmax = mm(1) & pmax = cmax
     log=1
   end

   oinst eq 'seit': begin
     message,'Colour table - EIT like', /info
     load_eit_color, head    ;default to 304?
     if obs_inst eq 'seit_00195' then eit_colors,195
     if obs_inst eq 'seit_00171' then eit_colors,171
;      for EIT cosmic rays sometimes fool sceling code
;      note - 4000 does not tend to show coronal holes on quiet sun...
;     cmax = max([cmax,3500])
;     cmax = max([cmax,4000])
;     cmax=4000
     cmax=4500 & pmax = cmax
     log=1
   end
					   
   oinst eq 'trce': begin
     message,'Colour table - EIT like', /info
     load_eit_color, head    ;default to 304?
     if obs_inst eq 'trce_m0171' then begin
       eit_colors,171
       cmax = 256 & pmax = cmax
     endif
     log=1
   end

   oinst eq 'smdi': begin
     loadct,0
     pmm,map.data,mm=mm
     cmin = mm(0) & cmax=mm(1) & pmax = cmax
     log=0
   end

   oinst eq 'bbso': begin
     loadct,0
     pmm,map.data,mm=mm
     cmin = mm(0) & cmax=mm(1) & pmax = cmax
     log=0
   end	       

   else : begin
     message,'Colour table - defaulting',/info
     loadct,3  ; log??
   end

 endcase

message,'CMAX s: '+string(cmax),/info

;-----------------------------------------------------------------
;    now lets plot everything

help,cmin,cmax,pmax

;***  need to tilt by P angle!!!!!

angs = pb0r(map.time,/arcsec,soho=soho)
map.l0 = angs[0]
map.b0 = angs[1]

center = [0,0]
print,'Center: ',center
mtit = map.id+'!c'+anytim(map.time,/vms,/trun)
print,'<<<<<< Title?: ',mtit
help,/st,map

xtrim = 50        ; will trim image later, need to subtract from Web coordinates

plot_map, map, fov=[40,25], center=center, title=mtit, $
	  dmin=cmin, dmax=cmax, $
	  log=log, $
	  grid=10, gstyle=1, gcolor=200, bottom=4, charsiz=csiz

;;egso_oplot_secflare, nstr, ref=map, window=[-72,12], /verb

;    put east/west labels on
;??? tilt
xyouts, -825, -760-150, 'E', charsiz=1.4
xyouts,  800, -760-150, 'W', charsiz=1.4

;    prepare the legend giving time coverage
;>>>>ttx = anytim(infiles(3),/vms,/date)	
ttx = anytim(map.time,/vms,/date)
help, ttx

;legend = 'SEC: Back to 3 days before present (recent in red)'
;legend = 'Flares from '+ttx+' to present ('+strtrim(strmid(fmt_tim(sdate),0,16),2)+' UT)'
legend = 'Flares from '+ttx+' to '+strtrim(strcompress(strmid(fmt_tim(sdate),0,16)),2)+' UT'

;    blank bit behind label
;;polyfill,[-1100,-1100,600,600,-1100],[-550,-490,-490,-550,-550],color=0


;    define time ranges for different coloured dots
;;trange = [ttx,fmt_tim(now)]
;trange = [fmt_tim(addtime(now,delt=-12*60)), now]
trange = [fmt_tim(addtime(sdate,delt=-24*60)), fmt_tim(addtime(sdate,delt=-12*60)), sdate]


noevents:

end


