function hio_form_ics_query, timerange, instrument=instrument, listname=listname, $
		show_tables=show_tables, describe_table=describe_table, $
		sql_string=sql_string, $
		verbose=verbose

;    24-Mar-2011  rdb  Created
;    30-Jan-2010  rdb  Select node automatically

;
;
message,'Forming query for HELIO ICS',/info

if keyword_set(sql_string) then begin
  message,'Using supplied SQL string',/info
  com = sql_string
  com = str_replace(com, "%","%25")		;  change % to %25 - STILTS is particular
;  print, com
  goto, go_psql
endif

qcat = 'observatory'
if keyword_set(instrument) then qcat = 'instrument'

list = qcat
if keyword_set(listname) then list = listname
list = strtrim(list,2)		; make sure no leading or trailing blanks

;    Syntax of some system commands for PostgreSQL is different to mySQL

;    List available tables
;sql> select table_name from information_schema.tables where table_schema='helio_ics'

if keyword_set(show_tables) then begin
;  com = "SHOW TABLES"
  com = "select table_name from information_schema.tables where table_schema='helio_ics'"
  goto, go_psql
endif

;    Determine the parameters in a table
; sql> select column_name from information_schema.columns where table_name='instrument'

if keyword_set(describe_table) then begin
;  com = "DESCRIBE " +list		; ==  show columns from 'list'
  com = "select column_name from information_schema.columns where table_name='" + list + "'"
  goto, go_psql
endif


;    default times
date_from = '2003-10-25T22:00'
date_to = '2003-10-31T04:00'

if n_params() eq 1 then begin
  date_from = anytim(timerange(0),/ccs,/trunc)
  date_to = anytim(timerange(1),/ccs,/trunc)
endif

if keyword_set(verbose) then fmt_timer,[date_from, date_to]

qcols ='*'
if keyword_set(instrument) then $
  qcols = "obsinst_key,date(time_start) as time_start,date(time_end) as time_end,inst_od1,inst_od2,inst_type,inst_oe1,inst_oe2,keywords"



; select obsinst_key,date(time_start),date(time_end),inst_od1,inst_od2,inst_type,inst_oe1,inst_oe2,keywords 
; from instrument where '2003-10-28'<=time_end and '2003-11-03'>=time_start  

com = "select " +qcols+ " from " +qcat+ " where '" +date_from+ "'<=time_end and '" +date_to+ "'>=time_start"

go_psql:
goto, tryhqi

;;com = str_replace(com, "%","%25")		;  change % to %25 - STILTS is particular
psql = '&sql='+com

;    concatonate the URL
;;root = 'http://msslxv.mssl.ucl.ac.uk:8080/stilts/task/sqlclient'
;;rdtb = '?db=jdbc:mysql://msslxt.mssl.ucl.ac.uk/helio_ics_ils&user=helio_guest'

cnode = hio_get_snode(/ics)
clauses = hio_form_stilts_clauses(cnode, /ics)
root = clauses(0)
rdtb = clauses(1)

ofmt = '&ofmt=vot'
url  = root + rdtb + psql + ofmt

goto, endit

tryhqi:
;    convert to lower case and get rid of surpless spaces
message,'ICS now with HQI',/info

sql = strtrim(strcompress(strlowcase(com)),2)

;    replace normal clause words with required form
cc1 = str_replace(sql,'select ','?SELECT=')
cc1 = str_replace(cc1,' from ','&FROM=')
cc1 = str_replace(cc1,' where ','&SQLWHERE=')
cc1 = str_replace(cc1,' limit ','&LIMIT=')

snode = hio_get_snode(/ics, cnode=cnode, verbose=verbose)
snode = cnode

;    allow use of supplied node name
if keyword_set(test_node) then snode=test_node

help, snode

root = snode		; don't need to manipulate????

root = 'http://' + snode + '/helio-ics/HelioQueryService'
url = root + cc1

;;;if keyword_set(show_tables) then url = root + com

endit:

if keyword_set(verbose) then print, url

return, url
end

