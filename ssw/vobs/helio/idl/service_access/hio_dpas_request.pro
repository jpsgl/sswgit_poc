;+
; NAME:
;    hio_dpas_request
; PURPOSE:
;    Gather required information for the reqtest to dpas_stc2api
;    Extract VOTable segments from return using mget_votable and combine into single table
; CALLING SEQUENCE:
;    struct = hio_dpas_request(timerange=timerange, required_obsinst=obsinst, report=report)
; CATEGORY:
;    
; INPUTS:
;    
; INPUT KEYWORDS:
;    timerange            requested time range (two elements)
;    required_obsinst     string containing list of requested observatories/instruments
;    test                 do test using default set of instruments and times
; OUTPUTS:
;    
; OUTPUT KEYWORDS:
;    report               return from mget_voltable - summary of instruments and records
; RESTRICTIONS:
;    
; HISTORY:
;    28-Oct-2010  rdb  written
;           2012  rdb  various updates; code to repair various things stripped out
;                      code to do plotting removed
;    19-May-2015  rdb  Slight correction to logic when nothing returned
;    27-Jul-2016  rdb  Some tidying, improved documentation
;    23-Jan-2017  rdb  Add </VOTABLE> if not present at end; DPAS may have returned early
;    24-Mar-2017  rdb  Ensure that does not fall over if VOTable contains the wrong TAGS
;
;-

; IDL Version 7.0, Mac OS X (darwin ppc m32)
; Journal File for rdb@msslmq.local
; Working directory: /Users/rdb/Documents/Docs_1/HELIO/Workpackages/Metadata Services/DPAS
; Date: Thu Oct 28 16:43:44 2010

function hio_dpas_request, $
	timerange=timerange, required_obsinst=required_obsinst, $
	test=test, verbose=verbose, $
;;	presets=presets, $
;;  nofix=nofix, $
;;	plot=plot, $				; /goes passed as _extra
	report=report, _extra=_extra

;    save VOTable that DPAS supplied

common dpas_response, raw_resp, hiovot_resp    ; "raw" and ingested.

;---------------------------------------------------------------------
;    initial setup code

;    set some default values

times = ['2003-10-30T20:00:00','2003-10-31T03:59:59']						; time range
rq_obsinst = 'SOHO__EIT,SOHO__CDS,SOHO__LASCO,ACE__MAG,RHESSI__HESSI_HXR'	; instruments

if keyword_set(test) then begin

  box_message,['','>>>>  Using default time range and instruments  <<<<','']

endif else begin

  message,'Trying to use supplied parameters',/info
  if keyword_set(timerange) then times = anytim(timerange,/ecs)				; time range
  if keyword_set(required_obsinst) then rq_obsinst = required_obsinst		; instruments

endelse

;
;    times ought to have been set by now...

if times(0) eq -1 then begin
  message,'NO Time Range set',/cont
  return,-1
endif

time0 = times(0)
time1 = times(1)
fmt_timer,[time0,time1]

;    make requested instruments into a string if supplied as an array
if n_elements(rq_obsinst) gt 1 then rq_obsinst = arr2str(rq_obsinst,',')


;---------------------------------------------------------------------
;    form the request to the DPAS and submit it

temp = {time_start:'', time_end:'', obs_inst:''}		;request structure
dpas_req = temp

dpas_req.time_start = time0(0)
dpas_req.time_end   = time1(0)
dpas_req.obs_inst   = rq_obsinst
help,/st,dpas_req

;    make the call to the DPAS

ccstart = systime(/sec)

dpas_stc2api, dpas_req, resp

ccend_dpas = systime(/sec)
time_took = ccend_dpas-ccstart

box_message,'    Back from DPAS_STC2API    '

raw_resp = resp		; save in common
help,resp

print,''
print,'>>>> Elapse time No. 1:', time_took, 'secs', format='(a,f9.2,x,a)'
print,''

nresp = n_elements(resp)
if nresp lt 100 then begin
  message,'NO data returned by the DPAS',/info
  return, -1
endif
qx = where(strpos(resp,'</VOTABLE>') ge 0, nqx)
if nqx eq 0 then begin
  message,'**** </VOTABLE> missing from end; APPENDING', /info
  resp = [resp,'</VOTABLE>']
  nresp = n_elements(resp)
endif

;    examine the response from the DPAS

box_message,'    Going to MGET_VOTABLE    '

vv = mget_votable(resp, /string, point=point, /quiet, num_rec=num_rec, report=report, verbose=verbose)
;help, vv

box_message,'    Back from MGET_VOTABLE    '
print, ''

help, num_rec
help, point, report
if !hio_sysvar.debug eq 1 then print, report

;    return from mget_votable only contains values for last dataset, ignore

;;if num_rec eq 0 or datatype(vv) ne 'STC' then begin
if num_rec eq 0 then begin
  message,'**** NO DATA returned ****',/info
  return, -1
endif
;print,report

;    MGET_VOTABLE returns info as a set of HEAP pointers  
np = n_elements(point)

temp = {obsinst_key: '', $ 				; HELIO obs/inst key
	provider: '', $ 					; archive that supplied data
	time_start: '', time_end:'', $ 		; start and end time of obs.
	url: '', $							; url file can be found at
	observatory: '', $					; observatory used for observations (pt. obsinst_key)
	instrument: '', $					; instrument used for observations (pt. obsinst_key)
	channel:'', $						; some instruments have multiple detectors
	observingdomain: '', $				; type of observation ?????
	pflag: 0, $							; set if duration > threshold
	group: '', $						; if set, shows is group - e.g. GONG
	groupmember: '', $					; ungrouped obsservatory if needed
	tdiff: -99.0}						; used when setting cadence
	
dpas_return = replicate(temp, num_rec)

;help,/st, dpas_return
;help,/st, report


;---------------------------------------------------------------------
;    Go through the returned VOTable and transfer if to the structure

message, 'Forming VOTable from returned sections', /info
ktot=0
for j=0,np-1 do begin 

;    retrieve the VOTable section

;  print,''
  print, 'Examining VOTable section', j, report(j).obsinst, report(j).count, format='(a,i3,a20,i7)'
  if report(j).count eq 0 then continue

  zz = *point(j)						; recover the VOTable from the HEAP
;  help,zz 		;& help,zz,/st
  nel = n_elements(zz)		;& help,nel
;  print, datatype(zz)

;    only add to dpas_return if output is a structure

  if datatype(zz) eq 'STC' then begin
    tags = tag_names(zz)
;    print, strlowcase(tags)

    dpas_return(ktot:ktot+nel-1).time_start = anytim(zz.time_start,/ecs,/trunc)
    dpas_return(ktot:ktot+nel-1).time_end = anytim(zz.time_end,/ecs,/trunc)

    if tag_exist(zz,'url') then dpas_return(ktot:ktot+nel-1).url = zz.url
    if tag_exist(zz,'provider') then dpas_return(ktot:ktot+nel-1).provider = zz.provider
    
    if not tag_exist(zz,'instrument_name') then begin		; PDS from UOC currently broken
      message,'>>>>>>>> PROBLEM with contents of VOTable structure, REMAPPING as UOC:PDS records',/info
;      help, /st, zz
      obsinst = zz.obsinst_key
;;      dpas_return(ktot:ktot+nel-1).obsinst_key = obsinst
      dpas_return(ktot:ktot+nel-1).url = zz.url
      provider = str_replace(zz.provider,'/','-')
      dpas_return(ktot:ktot+nel-1).provider = provider
      
;      continue												; skip past this resource
    endif else begin
    
      obsinst = zz.instrument_name
;;      dpas_return(ktot:ktot+nel-1).obsinst_key = obsinst
    endelse

goto, tftfc
;    do we need to fix some stuff?????

;;   obsinst = replicate(report(j).obsinst, nel)
    obsinst = zz.instrument_name
;    px = where(strpos(tags,'INSTR_CODE') eq 0, npx)
;    if npx eq 1 then begin

    if tag_exist(zz,'instr_code') then begin
      message,'>>>> Non-standard - Could be planetary data via the UOC:PDS <<<<',/info
      dpas_return(ktot:ktot+nel-1).obsinst_key = zz.instr_code
      dpas_return(ktot:ktot+nel-1).url = zz.filepath
      dpas_return(ktot:ktot+nel-1).provider = 'UOC:PDS ???'
;      help,/st,dpas_return(ktot)
;      print, obsinst
;;;      goto, skip_fix
    endif

;    help, obsinst & help,zz.instrument_name
;    print, all_vals(obsinst), all_vals(zz.instrument_name), format='(a)'

;;;skip_fix:

tftfc:

    dpas_return(ktot:ktot+nel-1).obsinst_key = obsinst

    oi2 = strarr(2,nel) 		;& help,oi2
    for knel=0,nel-1 do oi2(*,knel)=str2arr(obsinst(knel), delim='__')
    dpas_return(ktot:ktot+nel-1).observatory =  reform(oi2(0,*))   ;???
    dpas_return(ktot:ktot+nel-1).instrument =  reform(oi2(1,*))    ;???

    ktot=ktot+nel
  endif else message,'VOTable section not valid',/info

endfor

;    trim back in case total was wrong
dpas_return = dpas_return(0:ktot-1)

;help,num_rec,ktot
;help,/st,dpas_return

;>>> heap_free,point,/verb

;---------------------------------------------------------------------
;   produce a summary of what was returned by the DPAS

print,''
box_message,'   Summary of results from DPAS Query   ' 
print,'Requested time interval:'
fmt_timer,[time0,time1]
print,''
print,'No. of records by Requested OBSINST_KEY:'
help, dpas_return
print,''
print,'No. of Keys in the Request:',n_elements(report)
print,report, format='(a,t25,a)'
print,''
print,'No. of records by OBSINST_KEY in returned structure, after corrections:'
hio_summ_filelist, dpas_return, /short

ccend_interp = systime(/sec)
time_took2 = ccend_interp-ccend_dpas

;print,''
print,'DPAS call took:',time_took,' secs', format='(a,t20,f7.1,a)'
print,'Ingestion took:',time_took2,' secs', format='(a,t20,f7.1,a)'
print,''

;    pass the structure back

hiovot_resp = dpas_return		; save in common

return, dpas_return
end
