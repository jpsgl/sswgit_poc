function hio_get_dpasack, struct, verbose=verbose, html=html, outfile=outfile, brief=brief

;help,brief

archives = struct.provider

;    basic query to acknowledgement database

sql = "select * from acknowledgement"

;    assemble SQL substring based on supplied list of providers

narch = 0
if n_params() eq 1 then begin
  warch = all_vals(archives)
  narch = n_elements(warch)
  
  for j=0,narch-1 do warch(j) = str_replace(warch(j),'VSO:','')

  dpas_archives = " where " + arr2str("archive='" + warch + "'", delim=' or ')
  sql = sql + dpas_archives
  sql = str_replace(sql, 'UOC:','')
endif

;if keyword_set(verbose) then 

;    DPAS acknowledgements table is stored in the ICS

print,sql
query = hio_form_ics_query(sql=sql)
if keyword_set(verbose) then print, query

resp = ssw_hio_query(query,/conv)
nresp = n_elements(resp)

if keyword_set(verbose) then help,resp,/st
help, narch, nresp

;    allow selection of subset of tags

if keyword_set(brief) then begin
  tags = strlowcase(tag_names(resp))
  newtags = tags([0,1,2,4])
;    if brief is an array, it contains the tags no.s to include
  if n_elements(brief) gt 1 then newtags = tags(brief)
  new = str_subset(resp, arr2str(newtags,delim=','))
  if keyword_set(verbose) then help,/st,new
  resp = new
endif

;    write HTML file if requested

if keyword_set(html) then begin
  wfile = 'dpas_ack.html'
  if keyword_set(outfile) then wfile = outfile
  hio_stc2html, resp, file=wfile, title='DPAS Acknowledgements'
  
  hio_launch_browser, date=date, use_url=wfile
endif

return,resp
end
