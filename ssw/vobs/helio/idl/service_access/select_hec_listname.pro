function select_hec_listname, timerange, use_tabinfo=use_tabinfo, verbose=verbose, $
    search_string=search_string, $
	flare=flare, cme=cme, swind=swind, particle=particle, $
	solar=solar, ips=ips, geospace=geospace, planet=planet
	
;    time range of interest

if n_params() eq 1 then begin
  if n_elements(timerange) ne 2 then begin
    box_message,['** NO Time Range must contain 2 elements **']
    return, -1  
  endif
endif else begin
  box_message,['** NO Time Range supplied **']
  return, -1
endelse

base_times = timerange			; *(self.timerange)
search_times = anytim(base_times,/ccs,/date)

;    get the master table from the HEC if necessary

hec_tabinfo = -1
if keyword_set(use_tabinfo) then hec_tabinfo = use_tabinfo

;    if we do not have a structure by now, load from the HEC

if datatype(hec_tabinfo) ne 'STC' then begin
  message,'Reading the HEC Master Catalogue',/info
  query = hio_form_hec_query(/char)	; get HEC table characteristics
  hec_tabinfo = ssw_hio_query(query, /conv, verbose=verbose)
;    temporary fix!!!!
;?????  set all qualification chars to lower case
  hec_tabinfo = fix_hectab(hec_tabinfo, verbose=verbose)
endif

;hec_tabinfo.flare  = strlowcase(hec_tabinfo.flare)
;hec_tabinfo.cme    = strlowcase(hec_tabinfo.cme)
;hec_tabinfo.swind  = strlowcase(hec_tabinfo.swind)
;hec_tabinfo.part   = strlowcase(hec_tabinfo.part)
hec_tabinfo.otyp   = strlowcase(hec_tabinfo.otyp)
;hec_tabinfo.solar  = strlowcase(hec_tabinfo.solar)
;hec_tabinfo.ips    = strlowcase(hec_tabinfo.ips)
;hec_tabinfo.geo    = strlowcase(hec_tabinfo.geo)
;hec_tabinfo.planet = strlowcase(hec_tabinfo.planet)

if keyword_set(verbose) then $
	help,/st, hec_tabinfo

;   switches can be used to refine by type of list

  case 1 of
    keyword_set(cme):      qv = where(strlowcase(hec_tabinfo.cme) eq 'y', nqv)
    keyword_set(flare):    qv = where(strlowcase(hec_tabinfo.flare) eq 'y', nqv)
    keyword_set(particle): qv = where(strlowcase(hec_tabinfo.part) eq 'y', nqv)
    keyword_set(swind):    qv = where(strlowcase(hec_tabinfo.swind) eq 'y', nqv)
    
    keyword_set(search_string): begin
        descriptions = strlowcase(hec_tabinfo.description)
        qv = where(strpos(descriptions, strlowcase(search_string)) ge 0, nqv)
        if nqv le 0 then begin
          box_message,['** NO lists match Search String **']
          return, -1
        endif
      end
    else: begin
      nqv = n_elements(hec_tabinfo)
      qv = indgen(nqv)
      end
  endcase
  
  message, /info, string('After selecting for List Type:', nqv, format='(a, t35, i4)')

;   switches can also identify the region the list refers to

  shec_tabinfo = hec_tabinfo(qv)
  
  case 1 of
    keyword_set(solar):    qv2 = where(strlowcase(shec_tabinfo.solar) eq 'y', nqv2)
    keyword_set(ips):      qv2 = where(strlowcase(shec_tabinfo.ips) eq 'y', nqv2)
    keyword_set(geospace): qv2 = where(strlowcase(shec_tabinfo.geo) eq 'y', nqv2)
    keyword_set(planet):   qv2 = where(strlowcase(shec_tabinfo.planet) eq 'y', nqv2)
    else: nqv2 = -1
  endcase 
  
  if nqv2 gt 0 then begin
    hec_tabinfo = shec_tabinfo(qv2)
    nqv = nqv2
  endif
  
  message, /info, string('After selecting for List Domain:', nqv, format='(a, t35, i4)')

;    check if the event list covers the required time intervals

  print,'>>>> List Time Range validity check:', search_times, format='(a,2a12)

  ccc = hec_tabinfo
  ccc = ccc(qv)
  if keyword_set(verbose) then help,ccc

  pnts = intarr(nqv)
  for jk=0,nqv-1 do begin
    if keyword_set(verbose) then $
      print,'List:   ',ccc(jk).name, ccc(jk).timefrom,ccc(jk).timeto, format='(a,a,t38, 2a12)'
    if (search_times(0) le ccc(jk).timeto) and (search_times(1) ge ccc(jk).timefrom) then begin
      if keyword_set(verbose) then $
        print,ccc(jk).name, jk, format='(a32,i6)'
      pnts(jk) = 1
    endif
  endfor
  qv = where(pnts eq 1, nqv)
  
  message, /info, string('After selecting for Time Range:', nqv, format='(a, t35, i4)')
  
  if nqv le 0 then begin
    box_message,['** NO lists match the selection criteria **']
    return, -1
  endif

  clist = ccc	;.description

;    some of the HEC descriptions have "&" as html string
  clist.description = str_replace(clist.description,'&amp;','&')

;   menu to allow the user to make selection

  message, /info, 'Displaying HEC list select widget'

  sel = hio_xmenu_sel(clist(qv).description,/one,size=14, $
	title='HELIO Heliophysics Event Catalogue', $
	banner='Please select from event lists matching criteria')

  if sel ne -1 then hec_list = clist(qv(sel)).name $  ;(*self.hec_tabinfo)(qv(sel)).name $
    else begin
      box_message,['    NO Selection made','*** Keeping previous selection ***']
      return, -1
    endelse

;;sel = (where((*self.hec_tabinfo).name eq hec_list))(0)   ;<<<<<<<<<<<

  print,''
  help, sel

  if keyword_set(verbose) then help,/st, clist(qv(sel))

  help, hec_list

  print,'Base Time:  ',search_times
  list_times = anytim([clist(qv(sel)).timefrom, clist(qv(sel)).timeto],/ccs,/date)
  print,'List Time:  ',list_times

return, hec_list
end