;+
; NAME:
;    get_cxs_goes
; PURPOSE:
;    Obtain GOES time-series data byt making a request to the HELIO CXS
; CALLING SEQUENCE:
;    ogoes = get_cxs_goes(times [, /proton])
; CATEGORY:
;    
; INPUTS:
;    times             Time range of required data
; INPUT KEYWORDS:
;    proton            If set return proton data - default is soft X-rsya
;    verbose           Provode more information
; OUTPUTS:
;    
; OUTPUT KEYWORDS:
;    
; RESTRICTIONS:
;    
; HISTORY:
;    01-Apr-2011  rdb  written
;
;-

function get_cxs_goes, times, proton=proton, verbose=verbose, test=test

;;common cxs_info, cxs_return

message,'Getting GOES data from CXS',/info

;    add a small margin either side to avoid time rounding errors...
fmt_timer,times
nt0 = anytim(addtime(times(0), delt=-10), /ecs)
nt1 = anytim(addtime(times(1), delt=+10), /ecs)
ntimes = [nt0,nt1]
fmt_timer,ntimes

goes_plot = 1			;xray
if keyword_set(proton) then goes_plot='proton'

cxs_return = hio_do_cxs(ntimes, verbose=verbose, goes_plot=goes_plot, /request_data, /nodisplay)

if keyword_set(verbose) then help,/st, cxs_return
if keyword_set(test) then return, -1

goes_url = cxs_return.sav_url				; URL where copied save set has been stored

message, '>>>> Restoring data from: '+goes_url, /info
restore, goes_url

if !hio_sysvar.temp_delete then begin
  ssw_file_delete, cxs_return.sav_url, status 		;, /quiet
  if status then message,'Deleted after ingestion',/info
endif

print,'Returned Times: ',ttr, format='(a,t20,a,2x,a)'

if not keyword_set(proton) then begin

;---------------------------------------------------------------------------------------
;    GOES X-ray data

  message,'Converting GOES X-ray data',/info

  if keyword_set(verbose) then begin
    help, goesx 
    help,/st, goesx
  endif

;    define the output structure

  ngx = n_elements(goesx)
  ogoes  = {time: fltarr(ngx) , flux: fltarr(2, ngx), base_time: '', mjd_base: fltarr(11)}

;    now populate it

  ogoes.flux(0,*) = reform(goesx.lo)
  ogoes.flux(1,*) = reform(goesx.hi)

  dd = string(goesx.date)
  date = strcompress(reform(dd(0,*)+'/'+dd(1,*)+'/'+dd(2,*)),/rem)
  xx = anytim2ints(date)
  ogoes.time = (xx.day-xx(0).day)*86400. + goesx.time/1000.

  tsta = xx(0)
;;  tsta.time = goesx(0).time			; NO, from start of the day !!!!!
  ogoes.base_time = anytim(tsta,/yoh)

;?????  mjd_base

endif else begin

;---------------------------------------------------------------------------------------
;    GOES Proton data

  message,'Converting GOES Proton data',/info
  
;  if keyword_set(verbose) then begin
    help, goesp 
    help,/st, goesp
;  endif

  ogoes = goesp
  help, ogoes
  
goto, xxxx

;    define the output structure

  ngx = n_elements(goesp)
  ogoes  = {time: fltarr(ngx) , flux: fltarr(2, ngx), base_time: '', mjd_base: fltarr(11)}
  
;   which count data?

  dd = string(goesp.date)
  date = strcompress(reform(dd(0,*)+'/'+dd(1,*)+'/'+dd(2,*)),/rem)
  xx = anytim2ints(date)
  ogoes.time = (xx.day-xx(0).day)*86400. + goesp.time/1000.

  tsta = xx(0)
  tsta.time = goesp(0).time
  ogoes.base_time = anytim(tsta,/yoh)

;?????  mjd_base

  box_message,'GOES Prootons NOT YET available from CXS'
;  ogoes = -1
xxxx:
endelse

if keyword_set(verbose) then help,ogoes,/st

return, ogoes
end

