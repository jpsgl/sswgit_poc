function hio_form_stilts_clauses, node, ils=ils, ics=ics

snode = node
dnode = node
if strpos(dnode,'msslxv') ge 0 then dnode = 'msslxt.mssl.ucl.ac.uk'

; msslxv.mssl.ucl.ac.uk:8080
; msslxt.mssl.ucl.ac.uk/helio_ils

serv = '/helio_ics'
if keyword_set(ics) then serv = '/helio_ics_ils'
if keyword_set(ils) then serv = '/helio_ils'
dnode = dnode + serv

; help, node, snode, dnode

root = 'http://' + snode + '/stilts/task/sqlclient'
rdtb = '?db=jdbc:mysql://' + dnode + '&user=helio_guest'

clauses = [root, rdtb]

return, clauses
end
