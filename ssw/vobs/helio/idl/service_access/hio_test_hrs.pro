;+
; PROJECT:
;        HELIO
; NAME:
;        hio_test_hrs
; PURPOSE:
;        Tests that the HELIO Registry at the supplied URL is working 
; CALLING SEQUENCE:
;        flag = hio_test_hrs(test_url [,/verbose] [,/show_command])
; CATEGORY:
;        Housekeeping (of HELIO capabilities)
; INPUTS:
;        test_url        URL of the Registry to be tested
; INPUT KEYWORDS:
;        show_command    show the command used in the test
;        verbose         print additional information
; OUTPUTS:
;        flag            ??
; OUTPUT KEYWORDS:
;        
; RESTRICTIONS:
;        
; HISTORY:
;           Dec-2015  rdb  written
;
;-

function hio_test_hrs, test_url, verbose=verbose, show_command=show_command

url = 'voparis-helio.obspm.fr'
if n_params() eq 1 then url = test_url

sub_url = '/helio_registry/'
if strpos(url, 'voparis') ge 0 then sub_url = '/helio-registry/'

hrs_url = 'http://' + url + sub_url
;;cmd = wget_stream() + hrs_url
cmd = 'wget --read-timeout=2 --tries=1 -qO- ' + hrs_url

if keyword_set(verbose) or keyword_set(show_command) then print,cmd

spawn,cmd,resp
if keyword_set(verbose) then print,resp

qv = where(strpos(resp,'Welcome to Registry') ge 0, nqv)

return, nqv
end