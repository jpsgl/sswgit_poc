;+
; NAME:
;    hio_ics_widget
; PURPOSE:
;    
; CALLING SEQUENCE:
;    
; CATEGORY:
;    
; INPUTS:
;    
; INPUT KEYWORDS:
;    
; OUTPUTS:
;    
; OUTPUT KEYWORDS:
;    
; RESTRICTIONS:
;    
; HISTORY:
;    01-Apr-2011  rdb  written
;
;-

;---------------------------------------------------------------
pro do_widget1_event, event

widget_control, event.top, get_uvalue = info, /no_copy
widget_control, event.id,  get_value = buttonvalue
uname = WIDGET_INFO( event.id, /uname ) 

;help,event & help,event,/st
;help,info  & help,info,/st

;help, uname
;print, buttonvalue
nbt = n_elements(buttonvalue)
fmt = '(a, t10, ' + strtrim(string(nbt),2) + 'i6)'
print, string(uname, buttonvalue, format=fmt)

if uname eq 'od1a' then info.od1a_array = buttonvalue
if uname eq 'od1b' then info.od1b_array = buttonvalue

if uname eq 'od2a' then info.od2a_array = buttonvalue
if uname eq 'od2b' then info.od2b_array = buttonvalue

if uname eq 'ityp' then info.ityp_array = buttonvalue
if uname eq 'oe1' then info.oe1_array = buttonvalue

if uname eq 'oe2a' then info.oe2a_array = buttonvalue
if uname eq 'oe2b' then info.oe2b_array = buttonvalue
if uname eq 'oe2c' then info.oe2c_array = buttonvalue

if uname eq 'keya' then info.keya_array = buttonvalue
if uname eq 'keyb' then info.keyb_array = buttonvalue
if uname eq 'keyc' then info.keyc_array = buttonvalue


widget_control, event.top, set_uvalue = info, /no_copy
end

;---------------------------------------------------------------
pro do_widget1_ics, event

common ics_comm, psql

widget_control, event.top, get_uvalue = info, /no_copy

message,'Forming SQL from components',/info

;help,info  & help,info,/st

date_from = info.time_start
date_to = info.time_end

cl0='' & cl1='' & cl2='' & cl4='' & cl5='' & cl3=''
nshow=''
tand = ''
tand = " AND "

;//    Instrument type

;  $oe1= $oe1 . $TOR . "inst_oe1='" . $oe1z . "'";

qv = where(info.ityp_array ne 0, nqv)
if nqv gt 0 then begin
;  print,info.oe1_values(qv)
  ityp = '' & tor = '' 
  for j=0,nqv-1 do begin
    ityp = ityp + tor + "inst_type='" + info.ityp_values(qv(j)) + "'"
    tor = " OR "
  endfor
  print,ityp
  cl0 = tand + '(' + ityp + ')'
endif

;//    Observable Entity 1

;  $oe1= $oe1 . $TOR . "inst_oe1='" . $oe1z . "'";

qv = where(info.oe1_array ne 0, nqv)
if nqv gt 0 then begin
;  print,info.oe1_values(qv)
  oe1 = '' & tor = '' 
  for j=0,nqv-1 do begin
    oe1 = oe1 + tor + "inst_oe1='" + info.oe1_values(qv(j)) + "'"
    tor = " OR "
  endfor
  print,oe1
  cl1 = tand + '(' + oe1 + ')'
endif

;//    Observable Entity 2

;  $oe2= $oe2 . $TOR . "inst_oe2 LIKE '%" . $oe2z . "%'";

oe2_array = [info.oe2a_array,info.oe2b_array,info.oe2c_array]
oe2_values = [info.oe2a_values,info.oe2b_values,info.oe2c_values]

qv = where(oe2_array ne 0, nqv)
if nqv gt 0 then begin
;  print,info.oe1_values(qv)
  oe2 = '' & tor = '' 
  for j=0,nqv-1 do begin
    oe2 = oe2 + tor + "inst_oe2 LIKE '%" + oe2_values(qv(j)) + "%'"
    tor = " OR "
  endfor
  print,oe2
  cl2 = tand + '(' + oe2 + ')'
endif

;//  Observable Entity 2 additions
;//  need to or some stuff in from keywords...

;  $oe2= $oe2 . $TOR . "keywords LIKE '%" . $oe2pz . "%'";

;//    Observing Domain 1

;  $od1= $od1 . $TOR . "inst_od1 LIKE '%" . $od1z . "%'";

od1_array = [info.od1a_array,info.od1b_array]
od1_values = [info.od1a_values,info.od1b_values]

qv = where(od1_array ne 0, nqv)
if nqv gt 0 then begin
;  print,info.od1_values(qv)
  od1 = '' & tor = '' 
  for j=0,nqv-1 do begin
    od1 = od1 + tor + "inst_od1 LIKE '%" + od1_values(qv(j)) + "%'"
    tor = " OR "
  endfor
  print,od1
  cl4 = tand + '(' + od1 + ')'
endif

tand = " AND "

;//    Observing Domain 2

;  $od2= $od2 . $TOR . "inst_od2 LIKE '%" . $od2z . "%'";

od2_array = [info.od2a_array,info.od2b_array]
od2_values = [info.od2a_values,info.od2b_values]

qv = where(od2_array ne 0, nqv)
if nqv gt 0 then begin
;  print,info.od2_values(qv)
  od2 = '' & tor = '' 
  for j=0,nqv-1 do begin
    od2 = od2 + tor + "inst_od2 LIKE '%" + od2_values(qv(j)) + "%'"
    tor = " OR "
  endfor
  print,od2
  cl5 = tand + '(' + od2 + ')'
endif

;//    Keywords

;  $keys= $keys . $TOR . "keywords LIKE '%" . $keyz . "%'";

key_array = [info.keya_array, info.keyb_array, info.keyc_array]
key_values = [info.keya_values, info.keyb_values, info.keyc_values]

qv = where(key_array ne 0, nqv)
if nqv gt 0 then begin
;  print,info.key_values(qv)
  keys = '' & tor = '' 
  for j=0,nqv-1 do begin
    keys = keys + tor + "keywords LIKE '%" + key_values(qv(j)) + "%'"
    tor = " OR "
  endfor
  print,keys
  cl3 = tand + '(' + keys + ')'
endif

;//    which parameters should be returned


cat = 'instrument'
if (cat eq 'instrument') then $
  cols = "obsinst_key,longname,time_start,time_end,inst_od1,inst_od2,inst_type,inst_oe1,inst_oe2,keywords"

;;;;;;if not info.full then cat = 'ics_pat'   ;<<<<<<

print, cat
print, cols

;//    assemble the SQL string


psql= "select " + cols + " from " + cat + " where '" + date_from + "'<=time_end and '" + date_to + "'>=time_start"
if (cat eq 'instrument') then $
  psql =  psql + cl0 + cl1 + cl2 + cl4 + cl5 + cl3 + nshow

;;psql = str_replace(psql,'instrument','instrument_new')
print,psql

if info.pause eq 0 then  do_widget1_quit,event

end

;---------------------------------------------------------------
pro do_widget1_quit,event

  widget_control, event.top, /destroy

end


;---------------------------------------------------------------
function do_widget1_init, info, pause=pause

text_info = ['','This Widget allows you to select instruments based on type of', $
	'observation required. The check boxes act as constraints - with', $
	'none checked all instruments are selected','']
box_message, text_info


base = WIDGET_BASE(/column, event_pro='do_widget1', $
	title='HELIO Instrument Capabilities Service/DPAS', /grid)

info_text1 = 'This Widget allows you to select instruments based on type type of observation required. '
Result = WIDGET_LABEL(base, value=info_text1,/align_center)

;--    Observing Domain 1

od1a_values  = ['Sun', 'Mercury', 'Venus', 'Earth', 'Mars', 'Jupiter', 'Saturn'] 
od1b_values = ['Heliosphere','Planetary','Comet','Heliopause','Galactic']   

bgroup1  = CW_BGROUP(base, od1a_values, uname='od1a', /NONEXCLUSIVE, /row, $
		LABEL_left='Obs. Domain 1:           ') 
bgroup1a = CW_BGROUP(base, od1b_values, uname='od1b', /NONEXCLUSIVE, /row, $
		LABEL_left='                         ') 


;--    Observing Domain 2

od2a_values  = ['Interior', 'Disk/inr. cor.', 'Outer corona', 'Disk/helios.', 'Solar-wind'] 
od2b_values = ['Environment','Magnetosphere','Ionosphere','Aurora']   
;;         Interstellar    Energy release    Structure   

bgroup2  = CW_BGROUP(base, od2a_values, uname='od2a', /NONEXCLUSIVE, /row, $
		LABEL_left='Obs. Domain 2:     Solar:')
bgroup2a = CW_BGROUP(base, od2b_values, uname='od2b', /NONEXCLUSIVE, /row, $
		LABEL_left='               Planetary:')


;--    Instrument Type

ityp_values = ['Remote', 'In-situ']   

bgroup3 = CW_BGROUP(base, ityp_values, uname='ityp', /NONEXCLUSIVE, /row, $
		LABEL_left='Observation Type:        ', /FRAME)


;--    Observable Entity

oe1_values = ['Photons','Particles','Fields']
bgroup4x = CW_BGROUP(base, oe1_values, uname='oe1', /NONEXCLUSIVE, /row, $
		LABEL_left='Obs. Entity 1            ', /FRAME)

oe2a_values = ['GMR', 'HXR', 'SXR', 'EUV', 'UV', 'visible', 'micro-wave', 'radio'] 
;;    H-alpha    He 10830   
oe2b_values = ['Charged', 'Energetic', 'Neutral', 'Dust', 'Cosmic-ray']
oe2c_values = ['Electric', 'Magnetic', 'Gravity']

bgroup4a = CW_BGROUP(base, oe2a_values, uname='oe2a', /NONEXCLUSIVE, /row, $
		LABEL_left='Obs. Entity 2    Photons:')	;, /FRAME)
bgroup4b = CW_BGROUP(base, oe2b_values, uname='oe2b', /NONEXCLUSIVE, /row, $
		LABEL_left='               Particles:')
bgroup4c = CW_BGROUP(base, oe2c_values, uname='oe2c', /NONEXCLUSIVE, /row, $
		LABEL_left='                  Fields:')


;--    Keywords

keya_values = ['Imager','Spectrometer','Polarimeter','Coronagraph','Magnetograph','Magnetometer']  
keyb_values = ['Oscillations','Composition','Irradiance','Photometer','Radiometer','Plasma']  
keyc_values = ['H-alpha']  		;,'He 10830' ??

bgroup5  = CW_BGROUP(base, keya_values, uname='keya', /NONEXCLUSIVE, /row, $
		LABEL_left='Keywords:                ')
bgroup5a = CW_BGROUP(base, keyb_values, uname='keyb', /NONEXCLUSIVE, /row, $
		LABEL_left='                         ')
bgroup5b = CW_BGROUP(base, keyc_values, uname='keyc', /NONEXCLUSIVE, /row, $
		LABEL_left='                         ')


info_text2 = 'Note that the check boxes act as constraints - with none checked all instruments are selected'
;Result = WIDGET_TEXT(base, value=info_text2,/wrap, frame=0)
Result = WIDGET_LABEL(base, value=info_text2,/align_center)

;;result = WIDGET_BUTTON(base, value='Form ICS Request', event_pro = 'do_widget1_ics')
;;Quitid = widget_button(base, value='Execute Request and Quit', event_pro = 'do_widget1_quit')

fin_text = ['Form Request, Execute and Exit','Quit']
if keyword_set(pause) then fin_text = ['Form ICS Request','Execute Request and Quit']

result = WIDGET_BUTTON(base, value=fin_text(0), event_pro = 'do_widget1_ics')
Quitid = widget_button(base, value=fin_text(1), event_pro = 'do_widget1_quit')

size_font = 15		;13
font=get_xfont(/only_one,/fixed,closest=size_font) 

WIDGET_CONTROL, base, /REALIZE		;, default_font=font


info = {od1a_values:od1a_values, od1a_array:intarr(n_elements(od1a_values)), $
	od1b_values:od1b_values, od1b_array:intarr(n_elements(od1b_values)), $

	od2a_values:od2a_values, od2a_array:intarr(n_elements(od2a_values)), $
	od2b_values:od2b_values, od2b_array:intarr(n_elements(od2b_values)), $

	ityp_values:ityp_values, ityp_array:intarr(n_elements(ityp_values)), $
	oe1_values:oe1_values, oe1_array:intarr(n_elements(oe1_values)), $

	oe2a_values:oe2a_values, oe2a_array:intarr(n_elements(oe2a_values)), $
	oe2b_values:oe2b_values, oe2b_array:intarr(n_elements(oe2b_values)), $
	oe2c_values:oe2c_values, oe2c_array:intarr(n_elements(oe2c_values)), $

	keya_values:keya_values, keya_array:intarr(n_elements(keya_values)), $
	keyb_values:keyb_values, keyb_array:intarr(n_elements(keyb_values)), $
	keyc_values:keyc_values, keyc_array:intarr(n_elements(keyc_values)), $

	time_start:'', time_end:'', full:0, debug:0, pause:0}


return, base
end

;---------------------------------------------------------------

function hio_ics_widget, timerange=timerange, full=full, $
	pause=pause, verbose=verbose

common ics_comm, psql

;    Set up Widget

message,'HELIO ICS Widget',/info

base= do_widget1_init(info, pause=pause)
;help,info,/st

;    Load time and launch Widget

trange = fmt_tim(['12-jan-2005', '28-jan-2005'])
if keyword_set(timerange) then trange = timerange
fmt_timer, trange

info.time_start = anytim(trange(0),/ccs,/trunc)
info.time_end = anytim(trange(1),/ccs,/trunc)

;;if keyword_set(full) then info.full = 1
if keyword_set(pause) then info.pause = 1

;;help,info,/st
psql = ''

size_font = 15		;13
font=get_xfont(/only_one,/fixed,closest=size_font) 

widget_control, base, set_uvalue=info, /no_copy, default_font=font
XMANAGER, 'do_widget1', base 

message,'Back from Widget',/info

;    Get SQL from common and form into a query to submit to the ICS

;print,psql
help,psql

if strlen(psql) gt 1 then begin

;  change % to %25 - STILTS is particular
;  psql = str_replace(psql, "%","%25")
;  print,psql

  query = hio_form_ics_query(sql=psql)
  vv = ssw_hio_query(query,/conv)

endif else vv = ''

;    nothing returrd by the ICS
if datatype(vv) ne 'STC' then begin
  box_message,' ** No Instruments/ Nothing returned by the ICS ** '
  return,''
endif

if keyword_set(verbose) then help,/st,vv
oinst = vv.obsinst_key

;    filter return to only show instruments know to DPAS 
if not keyword_set(full) then oinst = allowed_ics(oinst)

;    looks better if name uses - and / instead of _ and __ 
coinst = str_replace(oinst,'__','/')
coinst = str_replace(coinst,'_','-')

;obsinst_list = string(coinst, format='(a,t25,x)') + vv.longname
;;;;obsinst_list = coinst
sel = hio_xmenu_sel(coinst, size=14, $
	title='HELIO Instrument Capabilities Service/DPAS', $
	banner='Select the required observatories/instruments')
help,sel

if sel(0) ge 0 then begin
  oinst = oinst(sel)		; but return what that the DPAS knows
;  box_message, coinst(sel)	; show the pretty version
  message,'No of instrument selected: '+string(n_elements(oinst)), /info
endif else begin
  oinst=''
  box_message,' *** No Instruments selected *** '
endelse

return, oinst
end
