;+
; NAME:
;    
; PURPOSE:
;    
; CALLING SEQUENCE:
;    
; CATEGORY:
;    
; INPUTS:
;    
; INPUT KEYWORDS:
;    
; OUTPUTS:
;    
; OUTPUT KEYWORDS:
;    
; RESTRICTIONS:
;    
; HISTORY:
;    01-04-2011  rdb  written
;
;    22-07-2011  rdb  Tidied all the o/p name stuff - change to wget_copy
;    22-08-2011  rdb  Clean up temporay file
;    01-02-2012  rdb  Slaved node used to the Registry; extra param in call
;    25-07-2014  rdb  Added copying of GOES save file if request_data set
;
;-

function hio_cxs_execute2, xml_file, verbose=verbose

message,'Making request to HELIO CXS',/info
ccstart = systime(/sec)

;   save modified XML control file so that Java can include it

temp_dir = concat_dir(get_temp_dir(),'cxs')
if not is_dir(temp_dir) then mk_dir,temp_dir

ofile = concat_dir(temp_dir,'qq_cxs.xml')
file_append, ofile,/new, xml_file

;    construct the Java command 

cnode = hio_get_snode(/cxs)
cxs_url = 'http://'+cnode
cmd = '$HELIO_JAVA/scripts/post_cxs.sh '+ofile+' '+cxs_url+' cxs'

if keyword_set(verbose) then print,cmd
spawn, cmd, resp, errs

if keyword_set(verbose) then begin
  help,resp,errs
  print,resp, format='(x,a)'
  print,errs, format='(x,a)'
endif

print,'CXS Request time1:',systime(/sec)-ccstart,' secs'

;    find URL of the PNG file               and copy it

png_file = ''
qv = where(strpos(resp, 'Resulting URL') eq 0, nqv)
help, nqv

;    CXS execution changed (Jan 2012) so return url to find url

if nqv gt 0 then begin
  rurl = resp(qv(0))
  url = gt_brstr(rurl+'**','URL ','**')
  cmd0 = wget_stream() + url

  if keyword_set(verbose) then print,cmd0
  spawn, cmd0, resp
  
  furl = gt_brstr(resp(0),"<a href='","'>")
  if keyword_set(verbose) then print,furl  

endif else begin
  message,'**** Problem with return from CXS',/info
  print, cmd
  help,resp,errs
  print,resp, format='(x,a)'
  print,errs, format='(x,a)'
  return,-1
  
endelse

;    get rid of temporary file
if not keyword_set(verbose) then ssw_file_delete, ofile		;, delete_status

print,'CXS Request time2:',systime(/sec)-ccstart,' secs'

help, furl

return, furl
end
