;+
; NAME:
;		hio_launch_browser
; PURPOSE:
;		Launch Web browser for supplied URL or according to switch
; INPUT KEYWORDS:
;		date		start date  (?? end)
;		use_url		explicit URL to use   (e.g for CACTUS site)
;		solarmonitor	solar monitor page for supplied date
;		stereo_movie	stereo movie for supplied date (+24hrs)
;				see inside for possible options
; OUTPUTS:
;		none
; CALLING SEQUENCE:
;		hio_launch_browser, date=date, use_url=use_url
;		hio_launch_browser, date=date, /solarmonitor
; RESTRICTIONS:
;		tested on Mac OS-X and linux
; HISTORY:
;		   Jun-2011  rdb  Writtem
;		19-Jul-2011  rdb  tidied, added stereo movies, "" marks around url
;		15-Aug-2011  rdb  added " &" after command in linux (no wait from idlde)
;-

pro hio_launch_browser, date=date, use_url=use_url, $
	verbose=verbose, test=test, $
	soars=soars, solarmonitor=solarmonitor, $
	stereo_movie=stereo_movie, jmap_ukssdc=jmap_ukssdc

;--    launch browser window

;    http://www.solarmonitor.org/index.php?date=20050701
;    http://www.solarmonitor.org/region.php?date=20050701&region=10781

;    http://www.mssl.ucl.ac.uk/~rdb/soars/soars_FlareLocations.gif

case 1 of

;    just use the supplied URL

keyword_set(use_url): url = use_url

;    show the SOARS Space Weather page - current date for the time being

keyword_set(soars):  url = 'http://www.mssl.ucl.ac.uk/~rdb/portal2/soars_SWx_II.php'

;    display Solar Monitor page for the specified date 

keyword_set(solarmonitor): begin
    message,'Display SolarMonitor page for specified date',/info

    url = 'http://www.solarmonitor.org/index.php'
    if keyword_set(date) then begin
      qdate = '?date='+time2fid(date,/full)
      url = url + qdate
    endif
  end

;    display the STEREO movies for 24 hours - start time -> +24hrs

;    just setting switch will default to ['stb_cor2','stb_cor2']
;    supplying single string will show that instrument and GOES light-curve
;    supplying two string array will show both images

keyword_set(stereo_movie): begin
    message,'Display Movies from STEREO-SC for specified date',/info
    isok = addtime(date(0),diff='1-dec-06')/60./24.
    if isok lt 0 then begin
      box_message,' >>> Mission not operational at this time <<< '
      return
    endif

    help,stereo_movie
    root = 'http://cdaw.gsfc.nasa.gov/movie/make_javamovie.php?'
    if keyword_set(date) then begin
      if datatype(stereo_movie) eq 'STR' then begin
        img1 = 'img1='+stereo_movie(0)
        img2 = ''
        if n_elements(stereo_movie) eq 2 then img2 = '&img2='+stereo_movie(1)
        img_str = img1 + img2
      endif else $		; default to COR2 from behind and ahead
        img_str = 'img1=stb_cor2&img2=sta_cor2'

;    should this be +/- 12 hours
      stime = '&stime=' + time2fid(date,/full,/time)
      nhrs = 18
      etime = '&etime=' + time2fid(addtime(date,del=nhrs*60.),/full,/time)
      url = root + img_str + stime + etime

    endif else message,'kkkkkasnsldldld'
  end

;    create a temporary web page to display JMAPS from UKSSDC

keyword_set(jmap_ukssdc): begin
    message,'Display STEREO-HI JMAPs from UKSSDC for specified date',/info
    isok = addtime(date(0),diff='1-dec-06')/60./24.
    if isok lt 0 then begin
      box_message,' >>> Mission not operational at this time <<< '
      return
    endif

    url = make_ukssdc_jmap_page(date)
  end

else: begin
    message,'No target URL selected',/info
    return
  end

endcase

url = '"' + url + '"'
print,'Using URL: ',url

;    launch browser - which will depend on the OS and user

;?????    define browser using env. variable

;; open -a /Applications/Firefox.app
;; open -a /Applications/Safari.app
;; c:>start iexplore www.yahoo.com
;; c>start firefox www.yahoo.com

;; app = 'open -a /Applications/Firefox.app/Contents/MacOS/firefox-bin '	; Max OS-X

if keyword_set(test) then help,/st,!version
os_name = !version.os_name
os_family = !version.os_family

if keyword_set(verbose) then help,os_name

case 1 of
  os_name eq 'Mac OS X':  app = 'open -a /Applications/Firefox.app '
  os_name eq 'linux':     app = 'firefox '
  os_family eq 'Windows': app = 'start firefox '
  else: begin
      message,'Operating System not supported',/info
      return
    end
endcase

cmd = app + url
if keyword_set(verbose) then print, cmd
if os_name eq 'linux' then cmd=cmd + ' &'

if not keyword_set(test) then $
  spawn, cmd

end
