pro hio_check_roots, verbose=verbose

common hio_nodes, hrs_struct

isverb = ''
if keyword_set(verbose) then isverb = ' ,/verb'

if n_elements(hrs_struct) eq 0 then begin
  message,'Need to load the Registry',/info
  hrs_struct = make_hrs_stc(verbose=verbose) 
endif

if keyword_set(verbose) then help,!hio_sysvar
ccstart=systime(/sec)

;    examine !hio_sysvar and test all services based on tag names 'root_xxxx'

rtags = strlowcase(tag_names(!hio_sysvar))
rtags = rtags(where(strpos(rtags,'root') eq 0, nrtag))

for j=0,nrtag-1 do begin
  ctag = rtags(j)
  
  cserv = str_replace(ctag,'root_','')
;  print, 'Testing Service:  ', strupcase(cserv)
  
  kount = 0
  allok = 0
  ocroot = ''

  while not allok do begin

    croot = ''
    cc = 'croot = !hio_sysvar.' + ctag
    temp = execute(cc)					; get service url from !hio_sysvar
;    help, croot 
    if croot eq '' then break
    if kount eq 0 then ocroot = croot
  
    allok = 0
    cc = 'allok = hio_test_snode(croot, /' + cserv + isverb + ')'
    temp = execute(cc)					; test whether the service is there and functional
    help, allok

;    if there was a problem, look for a new instance of the service

    if not allok then begin
      message,'Trying to find new node',/info
      cc = 'result = hio_service_node(/' + cserv + ')'
      temp = execute(cc)				; find a new instance of the service
      help, result
      if result eq -1 then break
      cc = '!hio_sysvar.' + ctag + ' = result'
      temp = execute(cc)				; and save it in !hio_sysvar
    endif
  
    kount++								; kill if goes into a loop
    if kount gt 10 then message,'**** Major problem finding service: '+cserv
    
  endwhile
  
;  cc = 'croot = !hio_sysvar.' + ctag
;  temp = execute(cc)
;  print, croot

if keyword_set(verbose) then help, cserv, croot, ocroot
qv = where(hrs_struct.service eq strupcase(cserv), nqv)
if nqv gt 0 and croot ne '' then begin
  qx = where(hrs_struct(qv(0)).nodes eq croot, nqx)				; flag selected node
  if nqx gt 0 then hrs_struct(qv(0)).node_flags(qx(0)) = 1
  qz = where(hrs_struct(qv(0)).nodes eq ocroot, nqz)			; negate old node (if changed)
  if nqz gt 0 and qx(0) ne qz(0) then hrs_struct(qv(0)).node_flags(qz(0)) = -1
  
endif

endfor

time_took = systime(/sec)-ccstart
print,'Service Check took:',time_took,' secs', format='(a,f9.3,a)'

if keyword_set(verbose) then help,!hio_sysvar

end