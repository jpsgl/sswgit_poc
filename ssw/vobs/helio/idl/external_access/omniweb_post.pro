function omniweb_post, timerange, verbose=verbose, ulysses=ulysses
;  type=type, vars=vars

; ace_merge
; uly_cospin_flux_1d

ctype = 'ace_merge' & cres = '4-min'
cvars = [07, 08, 09, 10, 13, 14, 16, 11]
csubdir = 'ace'

if keyword_set(ulysses) then begin
  ctype = 'uly_cospin_flux_1d' & cres = 'daily'
  cvars = [13, 15]
  csubdir = 'ulysses'
endif

; http://ftpbrowser.gsfc.nasa.gov/ace_merge.html
; http://ftpbrowser.gsfc.nasa.gov/uly_cospin_1d.html

;curl --data "spacecraft=ace_merge&activity=ftp
;&res=4-min&start_date=19990101&end_date=19990331
;&vars=07&vars=08&vars=09&vars=10&vars=13&vars=14&vars=16" 
; http://omniweb.gsfc.nasa.gov/cgi/nx1.cgi

; http://omniweb.gsfc.nasa.gov/cgi/nx1.cgi
; ?activity=ftp&spacecraft=uly_cospin_flux_1d&res=daily
; &start_date=19980101&end_date=19981231&vars=13&vars=15&maxdays=30
; &scale=Log10&view=0&linestyle=solid&paper=0&charsize=&xstyle=0&ystyle=0&symbol=0&symsize=&table=0&imagex=640&imagey=480&color=&back=

if n_params() eq 1 then begin
  fmt_timer, timerange
  times = time2fid(timerange,/full)
endif else times = ['19990101','19990110']
print,times

;--    Use POST command to Web processor to retrieve information about data and format

;    form the command string

sect1 = 'activity=ftp&start_date='+times(0) + '&end_date='+times(1)

sect2 = '&spacecraft=' +  ctype + '&res=' + cres + '&maxdays=30'

; '&vars=07&vars=08&vars=09&vars=10&vars=13&vars=14&vars=16&vars=11'

sect3 = arr2str('&vars='+string(cvars,format='(i2.2)'),delim='')

post_command = '"' + sect1+sect2+sect3 + '" '

;    URL of the page that processes the OnmiWeb page

post_url = 'http://omniweb.gsfc.nasa.gov/cgi/nx1.cgi'

;    Syntax of POST using CURL
; curl --data post_string post_url
;    Equivalent in WGET
; wget --post-data=post_string post_url

cmd = wget_post() + post_command + post_url

print,cmd
spawn, cmd, resp
if keyword_set(verbose) then help,resp

qv = where(strpos(resp,ctype) gt 0, nqv)
help,nqv

if nqv ne 2 then begin
  message,'Problem with the return from OMNIWeb',/info
  qerr =  where(strpos(resp,'WRONG START/STOP DAY') gt 0, nqerr)
  if nqerr gt 0 then box_message, gt_brstr(resp(qerr(0)),'<TT>','</TT>')
  return, -1                        
endif

qq = resp(qv)
url_data = gt_brstr(qq(0),'href="','">')
url_fmt  = gt_brstr(qq(1),'href="','">')

print, url_data
print, url_fmt

;    get information about file format

;;cmd = 'curl -s '+url_fmt
cmd = wget_stream() + url_fmt
print,cmd
spawn,cmd,qq
help,qq
print,qq, format='(a)'
                
;    create the structure
     
qqx = qq(4:n_elements(qq)-3)
nqx = n_elements(qqx)
tags = strarr(nqx)
cc = "tstruct = {time:''"

n = 0 & tag = '' & fmt = ''
for j=0,nqx-1 do begin
  reads,qqx(j),n,tag,fmt,format='(i2,x,a25,t32,a)'
;  print,n,tag,fmt
  tagx = strtrim(tag,2)
  tagx = (str2arr(tagx,','))(0)
  tagx = strlowcase(str_replace(tagx,' ','_'))
  tagx = strlowcase(str_replace(tagx,'-','_'))
  tagx = strlowcase(str_replace(tagx,'.','p'))
;  if tagx eq 'sw_temperature' then tagx = 'sw_temp'
  tags(j) = tagx
  if j ge 4 then cc = cc + ", x"+tags(j)+":0.0"                            
endfor
print,tags
cc = cc + "}"
print,cc
temp = execute(cc)

;    copy the data file

temp = concat_dir(get_temp_dir(), csubdir)
if not is_dir(temp) then mk_dir,temp
;;ace_file = concat_dir(temp,'ace_data.lst')

;;cmd = 'curl -s -o '+ ace_file + ' ' + url_data
cmd = wget_copy(url_data, outdir=temp, file_uri=ace_file)
print,cmd
spawn,cmd,resp

;    now read the file in

vals = rd_tfile(ace_file,11,/auto)
help,vals
nrow = (size(vals))(2)
ncol = (size(vals))(1)

;    populate the structure
                  
ace = replicate(tstruct,nrow)

tt = doy2ex(vals(1,*),vals(0,*))
tt(0,*) = vals(2,*)
tt(1,*) = vals(3,*)
ace.time = anytim(tt,/ccs,/trunc)
for kc = 0,ncol-5 do ace.(kc+1) = reform(vals(kc+4,*))

print,vals(*,18)
help,/st,ace(18)
help,ace
fmt_timer,tt

;    get rid of temporary file
if not keyword_set(verbose) then ssw_file_delete, ace_file	;, delete_status

return, ace
end
