;+
; PROJECT:
;        HELIO
; NAME:
;        hio_plot_swind_3pan
; PURPOSE:
;        
; CALLING SEQUENCE:
;        hio_plot_swind_3pan, times
; CATEGORY:
;        
; INPUTS:
;        times
; INPUT KEYWORDS:
;        
; OUTPUTS:
;        
; OUTPUT KEYWORDS:
;        
; RESTRICTIONS:
;        
; HISTORY:
;        01-Apr-2011  rdb  written
;
;-

pro hio_plot_swind_3pan, times

;    It is also possible to use low-level routines developed bu HELIO.
;    In this example, the solar wind velocity at the three spacecraft are compared.

;    NOTE: The spacraft move apart during time of plot - really need to timeshift!!!

if n_params() eq 0 then times=['01-jan-2008','1-mar-2008']
fmt_timer, times

window, 30, xsiz=600, ysiz=700, title='Multi-mission - Solar Wind Velocity'
clearplot
hio_plot_setup,/invert

!p.multi = [0,1,3]

ace = rd_ace_insitu(times)
sta = rd_stereo_insitu(times,'STA')
stb = rd_stereo_insitu(times,'STB')

utplot, stb.time, stb.speed, timer=times, /xst, yra=[200,800], /yst, $
	psym=3, charsiz=2.0, tit='STEREO-B', ytit='SW Speed (km/s)' ,xtit=''
utplot, ace.time, ace.speed, timer=times, /xst, yra=[200,800], /yst, $
	psym=3, charsiz=2.0, tit='ACE', ytit='SW Speed (km/s)', xtit=''
utplot, sta.time, sta.speed, timer=times, /xst, yra=[200,800], /yst, $
	psym=3, charsiz=2.0, tit='STEREO-A', ytit='SW Speed (km/s)'

hio_oplot_icon		; put logo on plot

end