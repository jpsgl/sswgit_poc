function omniweb_ace_post, timerange, verbose=verbose, insitu=insitu, particle=particle

;--    Use POST command to Web processor to retrieve information about data and format

if n_params() eq 1 then begin
  fmt_timer, timerange
  times = time2fid(timerange,/full)
endif else times = ['19990101','19990110']
print,times

;    form the command string

post_command = omniweb_form_ace_command(times, insitu=insitu, particle=particle, $
	search_fragment=search_fragment)

;    URL of the page that processes the OnmiWeb page

post_url = 'http://omniweb.gsfc.nasa.gov/cgi/nx1.cgi'

;    Syntax of POST using CURL
; curl --data post_string post_url
;    Equivalent in WGET
; wget --post-data=post_string post_url

cmd = wget_post() + post_command + post_url

print,cmd
spawn, cmd, resp

;    two URLs should be returned, the data file and its format

qv = where(strpos(resp,search_fragment) gt 0, nqv)
help,nqv

if nqv ne 2 then begin
  message,'Problem with the return from OMNIWeb',/info
  qerr =  where(strpos(resp,'WRONG START/STOP DAY') gt 0, nqerr)
  if nqerr gt 0 then box_message, gt_brstr(resp(qerr(0)),'<TT>','</TT>')
  return, -1                        
endif

qq = resp(qv)
url_data = gt_brstr(qq(0),'href="','">')		; data file
url_fmt  = gt_brstr(qq(1),'href="','">')		; format definition

print, url_data
print, url_fmt

;    get information about file format

cmd = wget_stream() + url_fmt
print,cmd
spawn,cmd,qq
help,qq
if keyword_set(verbose) then print,qq,format='(a)'
                
;    create the structure
     
qqx = qq(4:n_elements(qq)-3)
nqx = n_elements(qqx)
tags = strarr(nqx)
cc = "tstruct = {time:''"

n = 0 & tag = '' & fmt = ''
for j=0,nqx-1 do begin
  reads,qqx(j),n,tag,fmt,format='(i2,x,a25,t32,a)'
;  print,n,tag,fmt
  tagx = strtrim(tag,2)
  tagx = (str2arr(tagx,','))(0)
  tagx = strlowcase(str_replace(tagx,' ','_'))
  print,tagx
  if keyword_set(particle) then begin
    if j ge 3 then begin
      tagx = 'epam' + string(j,format='(i2.2)')
      cc = cc + ", "+tagx+":0.0"
    endif
  endif else if j ge 4 then cc = cc + ", "+tagx+":0.0"                            
;  if tagx eq 'sw_temperature' then tagx = 'sw_temp'
  tags(j) = tagx
endfor
print,tags
cc = cc + "}"
print,cc
temp = execute(cc)

;    copy the data file

temp = concat_dir(get_temp_dir(),'ace')
if not is_dir(temp) then mk_dir,temp
;;ace_file = concat_dir(temp,'ace_data.lst')

;;cmd = 'curl -s -o '+ ace_file + ' ' + url_data
cmd = wget_copy(url_data, outdir=temp, file_uri=ace_file)
print,cmd
spawn,cmd,resp

;    now read the file in

vals = rd_tfile(ace_file,11,/auto)
help,vals
nrow = (size(vals))(2)
ncol = (size(vals))(1)

;    populate the structure
                  
ace = replicate(tstruct,nrow)

tt = doy2ex(vals(1,*),vals(0,*))
tt(0,*) = vals(2,*)
tt(1,*) = vals(3,*)
ace.time = anytim(tt,/ccs,/trunc)
for kc = 0,ncol-5 do ace.(kc+1) = reform(vals(kc+4,*))

print,vals(*,18)
help,/st,ace(18)
help,ace
fmt_timer,tt

;    get rid of temporary file
if not keyword_set(verbose) then ssw_file_delete, ace_file	;, delete_status

return, ace
end


function omniweb_form_ace_command, times, insitu=insitu, particle=particle, $
	search_fragment=search_fragment

;    ACE in-situ plasma, mag field

; http://ftpbrowser.gsfc.nasa.gov/ace_merge.html

;curl --data "spacecraft=ace_merge&activity=ftp
;&res=4-min&start_date=19990101&end_date=19990331
;&vars=07&vars=08&vars=09&vars=10&vars=13&vars=14&vars=16" 
; http://omniweb.gsfc.nasa.gov/cgi/nx1.cgi

; <INPUT TYPE="checkbox" VALUE="04" NAME="vars">X, GSE (km)                                                              
; <INPUT TYPE="checkbox" VALUE="05" NAME="vars">Y, GSE (km)                                                               
; <INPUT TYPE="checkbox" VALUE="06" NAME="vars">Z, GSE (km)
; <INPUT TYPE="checkbox" VALUE="07" NAME="vars">Bmag (nT)
; <INPUT TYPE="checkbox" VALUE="08" NAME="vars">Bgse_x (nT)
; <INPUT TYPE="checkbox" VALUE="09" NAME="vars">Bgse_y (nT)
; <INPUT TYPE="checkbox" VALUE="10" NAME="vars">Bgse_z (nT)
; <INPUT TYPE="checkbox" VALUE="11" NAME="vars">No of points
; <INPUT TYPE="checkbox" VALUE="12" NAME="vars">Quality flag
; <INPUT TYPE="checkbox" VALUE="13" NAME="vars">Density, (N/cm^3)
; <INPUT TYPE="checkbox" VALUE="14" NAME="vars">Temperature, K
; <INPUT TYPE="checkbox" VALUE="15" NAME="vars">He/H ratio
; <INPUT TYPE="checkbox" VALUE="16" NAME="vars">Speed, (km/s)
; <INPUT TYPE="checkbox" VALUE="17" NAME="vars">Vx, GSE (km/s)
; <INPUT TYPE="checkbox" VALUE="18" NAME="vars">Vy, GSE (km/s)
; <INPUT TYPE="checkbox" VALUE="19" NAME="vars">Vz, GSE (km/s)

;    ACE particles

; http://omniweb.gsfc.nasa.gov/cgi/nx1.cgi?
; spacecraft=ace_epam_flux_1d&activity=ftp
; &start_date=19990101&end_date=19991231&vars=03&vars=05

; &maxdays=365
; &scale=Log10&view=0&linestyle=solid&paper=0&charsize=&xstyle=0
; &ystyle=0&symbol=0&symsize=&table=0&imagex=640&imagey=480&color=&back=

; <input value="03" name="vars" type="checkbox">0.443-0.531 MeV/n
; <input value="05" name="vars" type="checkbox">0.531-0.637 MeV/n
; <input value="07" name="vars" type="checkbox">0.637-0.765 MeV/n
; <input value="09" name="vars" type="checkbox">0.765-0.917 MeV/n
; <input value="11" name="vars" type="checkbox">0.917-1.100 MeV/n 
; <input value="13" name="vars" type="checkbox">1.100-1.320 MeV/n
; <input value="15" name="vars" type="checkbox">1.320-1.580 MeV/n
; <input value="17" name="vars" type="checkbox">1.580-1.900 MeV/n
; <input value="19" name="vars" type="checkbox">1.900-2.280 MeV/n
; <input value="21" name="vars" type="checkbox">2.280-2.730 MeV/n
; <input value="23" name="vars" type="checkbox">2.730-3.280 MeV/n
; <input value="25" name="vars" type="checkbox">3.280-3.930 MeV/n


;    form the command string

sect2 = '&start_date='+times(0) + '&end_date='+times(1)

case 1 of

keyword_set(particle): begin
    sect1 = 'spacecraft=ace_epam_flux_1d&activity=ftp'
;    sect3 = '&vars=03&vars=05'
    vars = [3,5]
    sect3 = arr2str('&vars='+string(vars,'(i2.2)'),delim='')
    search_fragment = 'ace_epam_'
  end

keyword_set(insitu): begin
    sect1 = 'spacecraft=ace_merge&res=4-min&activity=ftp'
;    sect3 = '&vars=07&vars=08&vars=09&vars=10&vars=13&vars=14&vars=16&vars=11'
    vars = [7,8,9,10,13,14,16,11]
    sect3 = arr2str('&vars='+string(vars,'(i2.2)'),delim='')
    search_fragment = 'ace_merge_'
  end

else: message,'Need to define required dataset'

endcase

post_command = '"' + sect1+sect2+sect3 + '" '

return, post_command

end