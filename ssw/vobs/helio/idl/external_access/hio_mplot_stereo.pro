;+
; PROJECT:
;        HELIO
; NAME:
;        hio_mplot_stereo
; PURPOSE:
;        Reads and plots in-situ data for selected STEREO mission
;        Plot has four panel (BT/Bz, Density, Vp, Tp) 
; CALLING SEQUENCE:
;        hio_mplot_stereo, times, title=title, err=err
; INPUTS:
;        times	time-range of the plot  (2 elements array)
; INPUT KEYWORDS:
;        title	title of the plot window
; OUTPUT KEYWORD:
;        err	set if error occurred in reading the data	
; RESTRICTIONS:
;
; HISTORY:
;       Jun-2011  rdb  written
;    24-Oct-2011  rdb  Adjusted plot ranges for mag and vel 
;    14-Feb-2012  rdb  Split read routine into "rd_stereo_insitu"
;    24-Oct-2014  rdb  Improved documentation
;-

pro hio_mplot_stereo, times, title=title, mission=mission, $
	window=window, verbose=verbose, err=err

;    set up stuff that depend on the mission

cmiss = 'STA'
if keyword_set(mission) then begin
  cmiss=strupcase(mission)
  if cmiss ne 'STA' then cmiss='STB'
endif
fmiss = 'STEREO-A'
if cmiss eq 'STB' then fmiss = 'STEREO-B'
ctit = fmiss + ' Mag & Plasma'

ptit = ctit
if keyword_set(title) then ptit = title

;    read the data for this mission

ace = rd_stereo_insitu(times, cmiss, verbose=verbose, err=err)
if datatype(ace) ne 'STC' then begin
  message,'>>>>>> ERROR in read of some kind in reading STEREO in-situ data',/info
  return
endif

;    now do the multi=panel plot

trange = times
fmt_timer, trange

pwind = 0
if keyword_set(window) then pwind=window

window, pwind, xsiz=600, ysiz=700, title=ptit

!p.position=0
!p.multi=[0,1,4]
!y.margin=[2,0.5]& !x.margin=[9,8]

csiz = 2.	;1.8

linecolor,1,'red'
linecolor,2,'orange'
linecolor,3,'yellow'
linecolor,4,'green'

xst = 1	;5
xtknm = replicate(' ', 60)
     nint= fix(addtime(trange(1),diff=trange(0))/(24.*60)+0.5)

set_mpan,/init

set_mpan
qv = where(ace.btsc lt 9.9e3)          
pmm, ace(qv).btsc
utplot, ace(qv).time,ace(qv).btsc, $
  yra=[-30,30], yst=1, ytit='Btsc Bn (rtn)', yminor=1, $
  timer=trange, xst=xst, psym=3, charsiz=csiz, /nolabel, xtickname=xtknm
qv = where(ace.bn lt 9.9e3)          
pmm, ace(qv).bn
outplot,ace(qv).time,ace(qv).bn, psym=3, color=1
;axis, xaxis=0, xtickname=strarr(60)+' '	;, xticks=nint	;,xminor=4
;axis, xaxis=1, xtickname=strarr(60)+' '	;, xticks=nint	;,xminor=4
outplot, trange,[0,0], linestyle=2  

title=ctit
xyouts,!x.window(0)+0.27,!y.window(1)+0.02,/norm, charsiz=2,title

set_mpan
qv = where(ace.sw_density lt 999)          
pmm, ace(qv).sw_density
utplot, /ylog, ace(qv).time,ace(qv).sw_density, $ 
;  yra=[0.1,10], yst=1, ytit='Density (/cm3)', $
  yra=[1,100], yst=1, ytit='Density (/cm3)', $
  timer=trange, xst=xst, psym=3, charsiz=csiz, color=2, /nolabel, xtickname=xtknm
;axis, xaxis=0, xtickname=strarr(60)+' '	;, xticks=nint	;,xminor=4
;axis, xaxis=1, xtickname=strarr(60)+' '	;, xticks=nint	;,xminor=4
outplot, trange,[1,1]*10   

set_mpan
qv = where(ace.speed lt 9999)
pmm, ace(qv).speed         
utplot, ace(qv).time,ace(qv).speed, $ 
  yra=[200,1000], yst=1, ytit='Vp (km/s)', yminor=1, $
  timer=trange, xst=xst, psym=3, charsiz=csiz, color=3, /nolabel, xtickname=xtknm
;axis, xaxis=0, xtickname=strarr(60)+' '	;, xticks=nint	;,xminor=4
;axis, xaxis=1, xtickname=strarr(60)+' '	;, xticks=nint	;,xminor=4

xst = 1
set_mpan
qv = where(ace.sw_temperature lt 9.9e6)     
pmm, ace(qv).sw_temperature
utplot, /ylog, ace(qv).time,ace(qv).sw_temperature, $
  yra=[1.e4,1.e7], yst=1, ytit='Temp (K)', $
  timer=trange, xst=xst, psym=3, charsiz=csiz, color=4, /nolabel
outplot, trange,[1e5,1e5], linestyle=2   
outplot, trange,[1e6,1e6], linestyle=2

;     axis, xaxis=0, xticks=nint, xtickname=strarr(60)+' ',xminor=2
;axis, xaxis=1, xticks=nint, xtickname=strarr(60)+' ',xminor=2

;;xticknam = ['06/01',' ','06/03',' ','06/05',' ','06/07',' ','06/09',' ','06/11']
tt = addtime(replicate(trange(0),nint+1),delt=indgen(nint+1)*24*60.)   
xticknam = strmid(anytim(tt,/ecs,/date),5,5)
xticknam = strmid(anytim(tt,/ecs,/date),8,2)
help,nint
if nint gt 5 then xticknam(indgen((nint+1)/2)*2+1)=' '                             
;axis, xaxis=0, xticks=nint, xtickname=xticknam,xminor=2,charsiz=2

;    annotate with start time of the plot
ut_start = 'Begin: '+strmid(anytim(trange(0),/ecs),0,16)+' UT'
xyouts,10,5,/dev, ut_start, charsiz=1.2

;    write HELIO name - use logo??
xyouts,540,5,/dev, 'HELIO', charsiz=1.2		;???

end
