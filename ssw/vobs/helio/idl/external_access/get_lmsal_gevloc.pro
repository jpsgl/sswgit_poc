function hio_conv_gevloc2hec, gev

temp={hec_id:-1, time_start:'', time_peak:'', time_end:'', $
	nar:0, lat_hg:0.0, long_hg:0.0, long_carr:0.0, $
	xray_class:'', optical_class:''}

sxr_flarec = ''

;;qv = where(gev.location(0) ne -999, nqv)
nqv = n_elements(gev)
qv = indgen(nqv)

if nqv gt 0 then begin
  sxr_flare = replicate(temp, nqv)
  
  ss = strarr(4)
  for j=0,nqv-1 do begin
    cgev = gev(qv(j))
    sxr_flare(j).time_start = cgev.fstart
    sxr_flare(j).time_peak = cgev.fpeak
    sxr_flare(j).time_end = cgev.fstop
    sxr_flare(j).xray_class = cgev.class
;;    sxr_flare(j).optical_class = ''		; not available
    
    reads,cgev.helio,ss,format='(a1,a2,a1,a2)'
    lat = fix(ss(1))  & if ss(0) eq 'S' then lat = -lat
    long = fix(ss(3)) & if ss(2) eq 'E' then long = -long
    sxr_flare(j).long_hg = long
    sxr_flare(j).lat_hg = lat    
;    could fill in the Carrington longitude values if needed...
  endfor
endif

return, sxr_flare
end

function get_lmsal_gevloc

;    gevloc contains last 7 days

gevloc=get_gevloc_data()
help,gevloc
help,gevloc,/st

events = hio_conv_gevloc2hec(gevloc)
;fmt_timer, events.time_start

return, events
end

