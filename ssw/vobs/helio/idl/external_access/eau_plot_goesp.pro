function get_ogoesp, times, debug=debug

;    read the required data if not already loaded
;    can switch between reading hesperia ar making a request to the CXS

;;;;common egoes, ogoes, gsta, gstp
common egoes2, goesp, gpsta, gpstp

  rsta = time2fid(times(0),/full,/time)
  rstp = time2fid(times(1),/full,/time)
  
  print,'++ Dates requested: ', rsta, rstp, format='(a,t35,a,2x,a)'
  if n_elements(gpsta) gt 0 then $
  print,'++ Dates for data in memory: ', gpsta, gpstp, format='(a,t35,a,2x,a)'
  help, gpsta, gpstp

  if n_elements(goesp) eq 0 then begin
    gpsta='' & gpstp=''
  endif

  if (rsta lt gpsta) or (rsta gt gpstp) or (rstp gt gpstp) or (rstp lt gpsta) then begin
    
    message,'Required data needs to be ingested',/info

;    data can be from various Web sites or the CXS

    if !hio_sysvar.cxs_goes eq 1 then begin
      goesp = get_cxs_goes(times, /proton, verbose=debug)
      
    endif else begin    
      if rsta ge '20110101_0000' then $			; NGDC does not hace data beyonf 1-Jan-2011
        mssl_goesp_files, times, goesp, force=force  $
      else $
        ngdc_goes_files, times, goesp
        
    endelse      
      
    if datatype(goesp) ne 'STC' then begin
      message,'>>>> Unable to plot GOES Proton data',/info
      return, -1
    endif

    if !hio_sysvar.cxs_goes eq 1 then begin
  
      dd = string(goesp.date)
      date = strcompress(reform(dd(0,*)+'/'+dd(1,*)+'/'+dd(2,*)),/rem)
      xx = anytim2ints(date)
      xx.time = goesp.time
  
      nel = n_elements(goesp)
      gpsta = time2fid(anytim(xx(0),/ecs),/full,/time)
      gpstp = time2fid(anytim(xx(nel-1),/ecs),/full,/time)
  
    endif else begin
  
      nel = n_elements(goesp)
      gpsta = time2fid(goesp(0).time,/full,/time)
      gpstp = time2fid(goesp(nel-1).time,/full,/time)  ;; <<<<
    
    endelse
  
;    help, gpsta, gpstp
;    help, goesp, /st
      
    print,'++ Dates for ingested data: ', gpsta, gpstp, format='(a,t35,a,2x,a)'
      
  endif else message,'Data is already loaded',/info

;  help, goesp

return, goesp
end


;+
; NAME:
;    eau_plot_goesp
;
; PURPOSE:
;    UTPlot the GOES SXR light-curve for the required time interval
;    Ingests the data as required
;
;    Designed to be used where plot window has already been established
;    Makes it simpler to do two panel plots
;
; CALLING SEQUENCE:
;    eau_plot_goesp, times, xmargin=xmargin, charsize=charsize  [, /debug]
;
; CATEGORY:
;    
; INPUTS:
;    times			time range to be plotted
;
; INPUT KEYWORDS:
;    xmargin        margin in x-axis
;    charsize       charsize for the text
;    debug			provide additional information
;
; OUTPUTS:
;    
; OUTPUT KEYWORDS:
;    
; RESTRICTIONS:
;    
; HISTORY:
;       Apr-2011  rdb  written
;
;    25-Jul-2014  rdb  split out data read routine and added request to CXS
;
;-

pro eau_plot_goesp, times, xmargin=xmargin, charsize=charsize, title=title, $
			force=force, debug=debug

;common egoes2, goesp, gpsta, gpstp

  message,'Starting',/info
  
;    read the required data

  goesp = get_ogoesp(times, debug=debug)

;    do the plot

  message,'Now do the plot',/info

ctitle = 'GOES Protons'
if keyword_set(title) then ctitle = title

lsiz=1.4

utplot_io, times, [1.e-9,1.e-9], /nodata, $
	timer=times, xst=1, /year, $
	charsize=charsize, xmargin=xmargin, $
	title=ctitle, $
	ytickname=['10!U-2!N','10!U-1!N','10!U0!N','10!U1!N','10!U2!N','10!U3!N','10!U4!N'], $
	yrange=[1.e-2, 1.e4], yst=1, ytit='Protons (cm!U-2!Ns!U-1!Nsr!U-1!N)'

;    plot some horizontal grid lines
ls = [0,0,2,0,0]
for j=0,4 do outplot,[!x.crange(0),!x.crange(1)],[1e-1,1e-1]*10^j,linestyle=ls(j)

;    explain the colour of the lines
xsc=(!x.crange(1)-!x.crange(0))
xp = !x.crange(1)+xsc*.04
if xmargin(0) eq 18 then xp = !x.crange(0)-xsc*.15
xyouts, xp, 5e-2,/data, orient=90,'>=10' ,color=1, charsiz=lsiz*0.7
xyouts, xp, 8e-1,/data, orient=90,'>=50' ,color=2, charsiz=lsiz*0.7
xyouts, xp, 2e1, /data, orient=90,'>=100',color=3, charsiz=lsiz*0.7
xyouts, xp, 8e2, /data, orient=90,'MeV', charsiz=lsiz*0.7

;    plot routines for actual data depend on where they came from

if !hio_sysvar.cxs_goes eq 0 then begin

;    Proton data provided by NGDC or MSSL Web

message,'Plot using GOES data provided old methods',/info

  if n_elements(goesp) eq 0 then begin
;;    message,'No GOES data available',/cont
    gtime=0 & gdata=0 	;& utbase=0
  end else begin
    gtime = goesp.time		;;ogoes->get(/times)
    gdata = goesp.i7	;flux		;;ogoes->get(/data)
;;    utbase = 'xxxx' ;ogoes.base_time
  endelse

  help, gtime, gdata   ;, utbase
  print,times

;    find index limits within the data to improve the speed 

t0 = strmid(anytim(times(0),/ecs),0,16)
t1 = strmid(anytim(times(1),/ecs),0,16)
p0 = max(where(goesp.time le t0))
p1 = min(where(goesp.time ge t1))
print,t0,t1
print,'Index limits:',p0,p1

goesp_pdx = goesp(p0:p1)
goesp_pdx = goesp

qq = where(goesp_pdx.i3 lt 3.27e4, nqq)		; 10 Mev
;pmm, goesp_pdx.i3
if nqq gt 0 then outplot, goesp_pdx(qq).time, goesp_pdx(qq).i3, color=1

qq = where(goesp_pdx.i5 lt 3.27e4, nqq)		; 50 Mev
;pmm, goesp_pdx.i5
if nqq gt 0 then outplot, goesp_pdx(qq).time, goesp_pdx(qq).i5, color=2

qq = where(goesp_pdx.i7 lt 3.27e4, nqq)		; 100 Mev
;pmm, goesp_pdx.i7
if nqq gt 0 then outplot, goesp_pdx(qq).time, goesp_pdx(qq).i7, color=3

endif else begin

;    Proton data provided by the CXS

  message,'Plot using GOES data provided by the CXS',/info

  outplot, goesp,goesp.p(2), color=1		;>10MeV
  outplot, goesp,goesp.p(4), color=2		;>50MeV
  outplot, goesp,goesp.p(5), color=3		;>100MeV

endelse

end
