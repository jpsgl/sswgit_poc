function rd_impact_het, filename, verbose=verbose

print,'Reading: ',filename
qq = rd_tfile(filename,23,21,/auto)

sz = size(qq)
nrow = sz(2)

temp = {time_start:'', time_end:'', $
		elect:fltarr(3), elect_err:fltarr(3), $
		proton:fltarr(11), proton_err:fltarr(11)}
struct = replicate(temp,nrow)

dd = string(qq([3,2,1],*),format='(2(a,1h-),a)')
time_start = fmt_tim(fid2ex(time2fid(dd)+'.'+qq(4,*)))

dd = string(qq([7,6,5],*),format='(2(a,1h-),a)')
time_end = fmt_tim(fid2ex(time2fid(dd)+'.'+qq(8,*)))

struct.time_start = time_start
struct.time_end = time_end    

qq1 = reform(qq(9:*,*),2,14,nrow)

struct.elect = reform(qq1(0,indgen(3),*))
struct.elect_err = reform(qq1(1,indgen(3),*))

struct.proton = reform(qq1(0,indgen(11)+3,*))
struct.proton_err = reform(qq1(1,indgen(11)+3,*))

if keyword_set(verbose) then begin
  fmt_timer, struct.time_start
  help,struct,/st
endif

return, struct
end


;  http://www.srl.caltech.edu/STEREO/DATA/HET/Ahead/15minute/AeH06Dec.15m
;  B eH 06Dec .1m
;  A eH 06Dec .15m

date = '1-jun-2008'
date = '6-dec-06'
date=['20-jun-2008','2-jul-2008']
date=['10-dec-2006','8-jan-2007']
fmt_timer, date

root = 'http://www.srl.caltech.edu/STEREO/DATA/HET/'

dir = 'Ahead/15minute/'
stx = 'AeH'
if keyword_set(stb) then begin
  dir = 'Benind/15minute/'
  stx = 'BeH'
endif

;;dstr = arr2str((str2arr(strmid(anytim(date,/yoh),0,9),delim='-'))([2,1]),delim='')

dstr0 = arr2str((str2arr(strmid(anytim(date(0),/yoh),0,9),delim='-'))([2,1]),delim='')
dstr1 = arr2str((str2arr(strmid(anytim(date(1),/yoh),0,9),delim='-'))([2,1]),delim='')
dstr = [dstr0,dstr1]
if dstr0 eq dstr1 then dstr = dstr0
nfiles = n_elements(dstr)

for j=0,nfiles-1 do begin

  ff = stx + dstr(j) + '.15m'
  file = root + dir + ff
  print,file
  cmd = wget_copy(file, /verb)

;  note: need -N to force overwrite

  spawn,cmd

  temp = rd_impact_het(ff, /verb)
;  help, temp,/st
  help, temp
  if j eq 0 then het = temp else het = [het, temp]
  help,het

endfor

fmt_timer, date
utplot_io,het.time_start,het.proton(0), yra=[1.e-4,1e3],yst=1, timer=date,xst=1 
outplot,het.time_start,het.proton(10)                                         


end