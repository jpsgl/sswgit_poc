function def_monfiles, times, caltech=caltech, ngdc=ngdc

;    Produce list on year/month for request on monthly files

;    If date range spans more than one month, using the start and end times is
;    not enough. Need to look for all possible values.

;    01-02-2012  rdb  written

dstr=''			; nothing is no keyword set

tints = anytim2ints(times)
fmt_timer, tints

dys = indgen(max(tints.day)-min(tints.day)+1) + min(tints.day) 
ndys = n_elements(dys)
help, ndys

tx = anytim2ints(times)            
txx = replicate(tx(0),ndys)    
txx.day=dys
;help,txx,/st

;    naming convention for monthly particle data at Caltech
if keyword_set(caltech) then begin
  ss = uniq(strmid(anytim(txx,/yoh),3,6))   
  mons = (strmid(anytim(txx,/yoh),3,6))(ss)      
  dstr = strmid(mons,4,2)+strmid(mons,0,3)
endif

;    naming cenvention for GOES files at NGDC
if keyword_set(ngdc) then begin
  fids = time2fid(txx,/full)		; yyyymmdd
  yyyymm = strmid(fids,0,6)
  ss = uniq(yyyymm)
  dstr = yyyymm(ss)
endif

;print,dstr
help, dstr

return, dstr

end
