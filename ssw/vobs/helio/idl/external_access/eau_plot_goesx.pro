function get_ogoesx, times, debug=debug

;    read the required data if not already loaded
;    can switch between reading hesperia ar making a request to the CXS

common egoes, ogoes, gsta, gstp

  rsta = time2fid(times(0),/full,/time)
  rstp = time2fid(times(1),/full,/time)
  
  print,'++ Dates requested: ', rsta, rstp, format='(a,t35,a,2x,a)'
  if n_elements(gsta) gt 0 then $
  print,'++ Dates for data in memory: ', gsta, gstp, format='(a,t35,a,2x,a)'

;;  help, ogoes
  if n_elements(ogoes) eq 0 then begin
    gsta='' & gstp=''
  endif

    if (rsta lt gsta) or (rsta gt gstp) or (rstp gt gstp) or (rstp lt gsta) then begin
    
      message,'Required data needs to be ingested',/info
      
      if !hio_sysvar.cxs_goes eq 1 then ogoes = get_cxs_goes(times, verbose=debug) $
        else goes_files, times, ogoes, debug=debug
      
      pmm,ogoes.time/60,mm=mm
      gsta = time2fid(addtime(ogoes.base_time, delt=mm(0)),/full,/time)
      gstp = time2fid(addtime(ogoes.base_time, delt=mm(1)),/full,/time)
      
      print,'++ Dates for ingested data: ', gsta, gstp, format='(a,t35,a,2x,a)'
      
    endif else message,'Data is already loaded',/info

;  help, gsta, gstp
;  help, ogoes
  help, /st, ogoes

return, ogoes
end


;+
; NAME:
;    eau_plot_goesx
;
; PURPOSE:
;    UTPlot the GOES SXR light-curve for the required time interval
;    Ingests the data as required
;
;    Designed to be used where plot window has already been established
;    Makes it simpler to do two panel plots
;
; CALLING SEQUENCE:
;    eau_plot_goesx, times, xmargin=xmargin, charsize=charsize  [, /debug]
;
; CATEGORY:
;    
; INPUTS:
;    times			time range to be plotted
;
; INPUT KEYWORDS:
;    xmargin        margin in x-axis
;    charsize       charsize for the text
;    debug			provide additional information
;
; OUTPUTS:
;    
; OUTPUT KEYWORDS:
;    
; RESTRICTIONS:
;    
; HISTORY:
;       Apr-2011  rdb  written
;
;    25-Jul-2014  rdb  split out data read routine and added request to CXS
;
;-

pro eau_plot_goesx, times, xmargin=xmargin, charsize=charsize, debug=debug

  message,'Starting',/info
  
;    read the required data

  ogoes = get_ogoesx(times, debug=debug)


  message,'Now do the plot',/info

  if n_elements(ogoes) eq 0 then begin
;;    message,'No GOES data available',/cont
    gtime=0 & gdata=0 & utbase=0
  end else begin
    gtime = ogoes.time		;;ogoes->get(/times)
    gdata = ogoes.flux		;;ogoes->get(/data)
    utbase = ogoes.base_time
  endelse

  help, gtime, gdata, utbase
  print,times

;    set up the y-axis plot ranges and titles

  yrange=[1.e-8, 1.e-3]
  ytickname=['A  ','B  ','C  ','M  ','X  ','X10']
  if max(gdata(*,0)) ge 1.e-3 then begin
    yrange=[1.e-7, 1.e-2]
    ytickname=['B  ','C  ','M  ','X  ','X10','']  ;1E-2']
  endif

;    establish the plot and plot the horozontal scale lines

  utplot_io, times, [1.e-9,1.e-9], utbase, /nodata, $
    timer=times, xst=1, /year, $
    charsize=charsize, xmargin=xmargin, title='GOES Soft X-rays', $
    yticks=5, ytickname=ytickname, yrange=yrange, yst=1, $
    ytit='GOES X-ray Class', yticklen=0.01

  for j=0,5 do outplot,[!x.crange(0),!x.crange(1)],[1e-8,1e-8]*10^j

;    now plot the data

  if n_elements(gtime) gt 100 then begin
    outplot, gtime, gdata(0,*), utbase, color=1  ;,psym=3
    outplot, gtime, gdata(1,*), utbase, color=2  ;,psym=3
  
  endif else begin
    mess = 'No GOES data available'
    xyouts,280,150,mess,/dev,charsiz=1.5
    message, mess, /cont
  endelse

return

end
