;+
; PROJECT:
;        HELIO
; NAME:
;        rd_stereo_insitu
; PURPOSE:
;        
; CALLING SEQUENCE:
;        struct = rd_stereo_insitu(times, cmiss  [,verbose=verbose] [,error=error])
; CATEGORY:
;        
; INPUTS:
;        times
;        cmiss
; INPUT KEYWORDS:
;        verbose
; OUTPUTS:
;        
; OUTPUT KEYWORDS:
;        error
; RESTRICTIONS:
;        
; HISTORY:
;        01-Apr-2011  rdb  written
;
;-

;--------------------------------------------------------------------------

function ucla_stereo_post, timerange=timerange, mission=mission, verbose=verbose

;    web page: http://aten.igpp.ucla.edu/ssc/stereo/

message,'Reading STEREO data - Entering',/info

root = 'http://aten.igpp.ucla.edu/

top_qu = 'forms/stereo/level2_plasma_and_magnetic_field.html
;cgi_qu = 'cgi-bin/stereoL2magpla.v2011Feb04
cgi_qu = 'cgi-bin/stereoL2magpla.v2012Oct22

;    output directory for the temporary files
odir = concat_dir(get_temp_dir(),'stereo')
if not is_dir(odir) then mk_dir, odir

;?type=ascii&res=med
;&start=2007+jan+01+00%3A00%3A00&stop=2007+feb+01+00%3A00%3A00
;&view=browser&LBBV=-2.00&UBBV=%2B2.00&SC=rtn&pos=heeq&param=1
;&spacecraft=stereo_A

trange=timerange

ccstart = systime(/sec)

;	res = temporal resolution (med=10 min; full=1 minute)
;	SC = coordinate system used for magnetic field  [use RTN]
;	pos = coordinate system used for spacecraft position  [use ???]

;	http://aten.igpp.ucla.edu/forms/stereo/PLASTIC_parameter_definition.html

cc1 = 'type=ascii&view=browser&res=med&SC=rtn&pos=hae

months = list_months(/lower,/trunc)
tt = anytim(trange(0),/ex)              
t0 = string(tt(6),months(tt(5)-1),tt(4),tt(0:2), $
	format='(i4.4,1h+,a,1h+,i2.2,1h+,i2.2,2(1h:,i2.2))')
tt = anytim(trange(1),/ex)              
t1 = string(tt(6),months(tt(5)-1),tt(4),tt(0:2), $
	format='(i4.4,1h+,a,1h+,i2.2,1h+,i2.2,2(1h:,i2.2))')
cc2 = '&start=' + t0 + '&stop=' + t1 

cc4 = '&spacecraft=stereo_A'
if keyword_set(mission) then begin
  if mission eq 'STB' then cc4 = '&spacecraft=stereo_B'
endif
;help,cc4

for param=1,2 do begin

cc3 = '&param=1'
if param eq 2 then cc3 = '&param=2'

post_command = '"' + cc1+cc2+cc3+cc4 + '" '

url = root + cgi_qu		; cgi-bin query

;-------

;    make the request for data and retrieve the URL of the results

cmd = wget_post() + post_command + url
if keyword_set(verbose) then print,cmd                                    
spawn, cmd, resp
if keyword_set(verbose) then help,resp

qv = where(strpos(resp,'<A HREF') eq 0,nqv)
if nqv eq 1 then sdir = gt_brstr(resp(qv(0)), '="','">') $
  else message,'HFDGFHGGH'

print,'Time - Segment:',systime(/sec)-ccstart,' secs',format='(a,f8.2,a)'

;    for the command and copy the file

url = root + sdir
cmd = wget_copy(url, outdir=odir, file_uri=file_uri)
if keyword_set(verbose) then print,cmd
spawn,cmd

;    read the file and extract the parameters
;    do this a two pass operation to identify the start of the data
 
ofile = file_uri
qq = rd_tfile(ofile)

p0 = where(strpos(qq,'# NAME') ge 0)
p1 = where(strpos(qq,'ABSTRACT') ge 0)
parecs = qq(p0(0)+1:p1(0)-1)
if keyword_set(verbose) then print,parecs,format='(a)'
tags = strlowcase(str_replace(strtrim(strmid(parecs,3,10),2),' ','_'))
;print,tags

nskip = where(strpos(qq,'DATA:') eq 0)

qq1 = rd_tfile(ofile, 25,nskip+1, /auto)
help,qq1


if param eq 1 then begin
  times = anytim(fix(qq1([3,4,5,6,2,1,0],*)),/ecs)
  if keyword_set(verbose) then print,times(0:4)

  qv = where(strpos(tags,'np') ge 0, nqv)
  if nqv eq 1 then print,tags(qv(0)) else message,'Help'
  np = reform(float(qq1(6+qv(0),*))) 	;& print,tags(1)

  qv = where(strpos(tags,'vp') ge 0, nqv)
  if nqv eq 1 then print,tags(qv(0)) else message,'Help'
  vp = reform(float(qq1(6+qv(0),*))) 	;& print,tags(2)

  qv = where(strpos(tags,'tp') ge 0, nqv)
  if nqv eq 1 then print,tags(qv(0)) else message,'Help'
  tp = reform(float(qq1(6+qv(0),*))) 	;& print,tags(3)

  if keyword_set(verbose) then help,np,vp,tp
  for j=0,1 do print,times(j),np(j),vp(j),tp(j)

  nel_1 = n_elements(np)
endif $

else if param eq 2 then begin
  qv = where(strpos(tags,'bn') ge 0, nqv)
  if nqv eq 1 then print,tags(qv(0)) else message,'Help'
  bn = reform(float(qq1(6+qv(0),*))) 	;& print,tags(3),qv(0)

  qv = where(strpos(tags,'btsc') ge 0, nqv)
  if nqv eq 1 then print,tags(qv(0)) else message,'Help'
  btsc = reform(float(qq1(6+qv(0),*))) 	;& print,tags(4),qv(0)

  if keyword_set(verbose) then help,bn,btsc
  for j=0,1 do print,times(j),bn(j),btsc(j)

  nel_2 = n_elements(bn)
endif

; idate=intarr(7)
; vals=fltarr(ncol-1)
; reads,qqx(0),idate,vals,format=fmt 

xxx:

;    remove the temporary file
if not keyword_set(verbose) then ssw_file_delete, ofile, delete_status

print,'Time - end loop:',systime(/sec)-ccstart,' secs',format='(a,f8.2,a)'

endfor

if nel_1 ne nel_2 then message,'BBiigg problem - array mismatch'

temp = {time:'', bn:0.0, btsc:0.0, sw_density:0.0, speed:0.0, sw_temperature:0.0}
struct = replicate(temp, nel_1)

struct.time = times
struct.bn = bn
struct.btsc = btsc
struct.sw_density = np
struct.speed = vp
struct.sw_temperature = tp

print,'Time - exit:',systime(/sec)-ccstart,' secs',format='(a,f8.2,a)'
if keyword_set(verbose) then help,struct

message,'Reading STEREO data - Exiting',/info

return, struct

end

;--------------------------------------------------------------------------

function rd_stereo_insitu, times, cmiss, verbose=verbose, error=error

;    ace structure supplied by read routine...

common omstereo, ace, acsta, acstp, cmiss_sav

common stx_xx, sta_struct, sta_sta, sta_stp, stb_struct, stb_sta, stb_stp

if cmiss ne 'STA' and cmiss ne 'STB' then begin
  message,'ERROR in defining mission: '+cmiss, /info
  return, -1
endif

;    only copy if data is not already loaded
;    then only copy if falls outside of what is already loaded
;????    extend interval copied so can shift slightly ????

tdiff = addtime(times(0),diff='25-oct-2006')/(60*24) 
help,tdiff
if tdiff lt 30 then begin
  box_message,'**** STEREO missions not operational ****'
  return, -1
endif

rsta = time2fid(times(0),/full,/time)
rstp = time2fid(times(1),/full,/time)
;help,rsta,rstp
print,'++ Dates requested: ', rsta, rstp, format='(a,t35,a,2x,a)'

if n_elements(ace) eq 0 then begin
  acsta='' & acstp=''
endif

if n_elements(sta_struct) eq 0 then begin
  sta_sta='' & sta_stp=''
endif

if n_elements(stb_struct) eq 0 then begin
  stb_sta='' & stb_stp=''
endif

;help, sta_struct, sta_sta, sta_stp, stb_struct, stb_sta, stb_stp

;????  yaxis title for mag. field depends on spacecraft  ace/stx


doit = 0
if n_elements(cmiss_sav) gt 0 then begin
  message,'>>>> Matching saved information for '+cmiss,/info
  if cmiss eq 'STA' then begin
;    help, sta_struct		;, sta_sta, sta_stp
    print,'++ Dates for stored data: ', sta_sta, sta_stp, format='(a,t35,a,2x,a)'
    if n_elements(sta_struct) gt 0 then begin
      ace=sta_struct & acsta=sta_sta & acstp=sta_stp
    endif else doit=1
  endif
  if cmiss eq 'STB' then begin
;    help, stb_struct		;, stb_sta, stb_stp
    print,'++ Dates for stored data: ', stb_sta, stb_stp, format='(a,t35,a,2x,a)'
    if n_elements(stb_struct) gt 0 then begin
      ace=stb_struct & acsta=stb_sta & acstp=stb_stp
    endif else doit=1
  endif
endif

err = 0


;    need to also trap case where the mission has changed...
;doit = 0
;if n_elements(cmiss_sav) eq 0 then doit=1 else if cmiss_sav ne cmiss then doit=1

if (rsta lt acsta) or (rsta gt acstp) or (rstp gt acstp) or (rstp lt acsta) or doit then begin
  message,'>>>> Reading data for '+cmiss,/info

  ace = ucla_stereo_post(timerange=times, mission=cmiss, verbose=verbose)
  if keyword_set(verbose) then help,/st,ace
  if datatype(ace) ne 'STC' then begin
    message,'Problem reading STEREO Data',/info
    err = 1			; flag error to calling routine
    return, -1
  endif

  nel = n_elements(ace)
  acsta = time2fid(ace(0).time,/full,/time)
  acstp = time2fid(ace(nel-1).time,/full,/time)  ;; <<<<

  if cmiss eq 'STA' then begin		; STA
    sta_struct=ace & sta_sta=acsta & sta_stp=acstp
    print,'++ Dates for ingested data: ', sta_sta, sta_stp, format='(a,t35,a,2x,a)'
  endif else begin					; STB
    stb_struct=ace & stb_sta=acsta & stb_stp=acstp
    print,'++ Dates for ingested data: ', stb_sta, stb_stp, format='(a,t35,a,2x,a)'
  endelse

endif else message, 'Data is already loaded',/info

cmiss_sav = cmiss
;help, ace, acsta, acstp, cmiss_sav

return, ace
end
