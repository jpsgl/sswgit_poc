pro hio_plot_lro, times, title=title, $
	xmargin=xmargin, charsize=charsize, yrange=yrange, $
	window=window, verbose=verbose, err=err, over=over

stc = rd_lro_crater_dreq(times, verbose=verbose)

ctitle = 'LRO CRaTER (CR and SEP)'
if keyword_set(title) then ctitle = title

pwind = 0
if keyword_set(window) then pwind=window

if not keyword_set(over) then begin
  window, pwind, xsiz=700,ysiz=500, title='LRO Crater'
  clearplot
  hio_plot_setup,/inv
  xmargin=[10,6]
endif

cyrange = [1e-2, 1e2]
if keyword_set(yrange) then cyrange = yrange

vv = fix(alog10(cyrange))
yticks = vv(1)-vv(0) & rvv = yticks+1
yticknames = strarr(rvv)
for j=0,rvv-1 do yticknames(j) = str_replace('10!U%%!N', '%%', strtrim(string(vv(0)+j),2))

print,'yrange = ', cyrange, format='(a,1h[, e7.1, 2h, , e7.1, 1h])' 
fmt_timer, times
;help, yticks

utplot_io, times, [1.e-9,1.e-9], /nodata, $
	timer=times, xst=1, /year, $
	yrange=cyrange, yst=1, ytickname=yticknames, yticks=yticks, yminor=9, $
	charsize=charsize, xmargin=xmargin, $
	title=ctitle, ytit='Dose Equivalent Rate (cSv/day)

;;for j=1,rvv-2 do print,[1,1]*cyrange(0)*10^j		; values for horizontal lines

ls = [0,0,0,0,0]			; set one value to "2" if want dashed line at that level
for j=1,rvv-2 do outplot,[!x.crange(0),!x.crange(1)],[1,1]*cyrange(0)*10^j, $
		linestyle=ls(j)		;, color=60
  
outplot,stc.time,stc.cdr12, color=1
outplot,stc.time,stc.cdr34, color=3           
outplot,stc.time,stc.cdr56, color=2

;    explain the colour of the lines
lsiz=1.				; character size for legend on RHS
xsc=(!x.crange(1)-!x.crange(0))
xp = !x.crange(1)+xsc*.04
if xmargin(0) eq 18 then xp = !x.crange(0)-xsc*.15
xyouts, xp, 7e-2, /data, orient=90,'Ch 1&2', color=1, charsiz=lsiz
xyouts, xp, 7e-1, /data, orient=90,'Ch 3&4', color=2, charsiz=lsiz
xyouts, xp, 7e0,  /data, orient=90,'Ch 5&6', color=3, charsiz=lsiz

end

