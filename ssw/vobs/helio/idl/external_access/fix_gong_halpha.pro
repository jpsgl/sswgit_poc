;+
; NAME:
;    
; PURPOSE:
;    
; CALLING SEQUENCE:
;    
; CATEGORY:
;    
; INPUTS:
;    
; INPUT KEYWORDS:
;    
; OUTPUTS:
;    
; OUTPUT KEYWORDS:
;    
; RESTRICTIONS:
;    
; HISTORY:
;    01-Apr-2011  rdb  written
;
;-

pro plot_gongha_tdiff, ff

;    Time plot of the time differnce of each GONG-Ha image from local noon

  fmt_timer, ff.time_start, tmin,tmax		; derive time range

  hio_plot_setup, /invert				;load the colour table

  hxrbs_format, old=old
  utplot, ff.time_start, ff.tdiff, color=60, $
		yra=[-1,8], /yst, psym=1, timer=[tmin,tmax], /xst, $
		ANYTIM_LABEL_EXTRA = {ecs:1}, /year, /nolabel,  $
		ytit='Time Difference (hrs)', charsiz=1.2, $ 
		title='Time relative to local noon for GONG-Ha Images'
  if old eq 'YOHKOH' then yohkoh_format

end

function fix_gong_halpha, filelist, debug=debug

if keyword_set(debug) then help,filelist,/st

ff = filelist

;    Table to crossmatch obsevatories to file key
;    add time offsetfor noon and do delta from noon?

ocodes =   [['Lh', 'LEAR', 'Learmonth',   '-114'], $		; 19:30
			['Uh', 'UDPR', 'Udaipur',      '-73'], $		; 16:30
			['Th', 'TEID', 'El Teide',     '+16'], $		; 12:30
;															; 11:30 UTC
			['Ch', 'CTIO', 'Cerro Tololo', '+70'], $		; 07:30
			['Ah', 'TSCA', 'Tucson',      '+110'], $		; 04:30
			['Zh', 'TSCB', 'Tucson', '     +110'], $		; 04:30
			['Bh', 'BBSO', 'Big Bear',    '+116'], $		; 04:30
			['Mh', 'MLSO', 'Mauna Loa',   '+155']]			; 01:30
;print,ocodes
nsites = n_elements(ocodes)/4
help, nsites

sdate = anytim(ff(0).time_start,/ecs,/date)

;    get details of this instrument

oinst = 'GONG__HALPH'
ics_struct = query_ics_oinst(oinst, /quiet)


qx = where(ff.obsinst_key eq 'GONG__HALPH',nqx)
if nqx gt 0 then begin

  break_file,filelist(qx).url,aa,bb,cc,dd
  print,'File keys: ',all_vals(strmid(cc+dd,14,4))

;    resolve the GONG observatory from the 2 letter code

  for j=0,nsites-1 do begin
    ccode = ocodes(0,j) 		;& help,ccode
    qv = where(strpos(ff(qx).url, ccode) gt 0,nqv)
    if nqv gt 0 then ff(qx(qv)).groupmember = ocodes(1,j)		; observing site
  endfor

  ff(qx).group = 'GONG'					; used to group the GONG sites together

  
;    calculate the time difference of the image from local noon and save

  for j=0,nsites-1 do begin

;    local noon depend on the longitude
    cnoon = reltime(sdate+' 12:00', hour = fix(ocodes(3,j))/15., out='ecs')
    if keyword_set(debug) then print, ocodes(1,j) +'   '+ cnoon

    qz = where(ff.groupmember eq ocodes(1,j), nqz)
    if nqz gt 0 then begin
      tdiff = addtime(ff(qz).time_start,diff=cnoon)/60.
      tdiff = abs(tdiff) mod 24
      q12 = where(tdiff gt 12, nq12)
      if nq12 gt 0 then tdiff(q12) = 24-tdiff(q12)			;<<<<<<<<<<<
      ff(qz).tdiff = tdiff    
    endif
    
  endfor

endif

if keyword_set(debug) then begin
  help,ff(qx(0)),/st
  cwind = !d.window
  window, 28			; alwats use 28 for debug plots  

  plot_gongha_tdiff, ff
  
  wset, cwind
endif  
  

return, ff
end
