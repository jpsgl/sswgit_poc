;+
; NAME:
;		hio_mplot_ace
; PURPOSE:
;    		Reads and plots ACE in-situ data from MAG and SWEPAM
;		Plot has four panel (BT/Bz, Density, Vp, Tp) 
; INPUTS:
;		times	time-range of the plot  (2 elements array)
; INPUT KEYWORDS:
;		title	title of the plot window
; OUTPUT KEYWORD:
;		err	set if error occurred in reading the data	
; CALLING SEQUENCE:
;		hio_mplot_ace, times, title=title, err=err
; RESTRICTIONS:
;
; HISTORY:
;		   Jun-2011  rdb  written
;		24-Oct-2011  rdb  Changed defaault yrange for mag and vel
;		14-Feb-2012  rdb  Split read routine into "rd_ace_insitu"
;
;-

pro hio_mplot_ace, times, title=title, window=window, err=err

;    read the data for this mission

ace = rd_ace_insitu(times, verbose=verbose, err=err)
if datatype(ace) ne 'STC' then begin
  message,'>>>>>> ERROR in read of some kind in reading ACE in-situ data',/info
  return
endif

;    now do the multi=panel plot

;fmt_timer, ace.time, tmin, tmax, /quiet
;trange = [tmin, tmax]
;if keyword_set(timerange) then trange=timerange

trange = times
fmt_timer, trange

pwind = 0
if keyword_set(window) then pwind=window

window, pwind, xsiz=600, ysiz=700, title=title

!p.position=0
!p.multi=[0,1,4]
!y.margin=[2,0.5]& !x.margin=[9,8]

csiz = 2.	;1.8

linecolor,1,'red'
linecolor,2,'orange'
linecolor,3,'yellow'
linecolor,4,'green'

xst = 1	;5
xtknm = replicate(' ', 60)
     nint= fix(addtime(trange(1),diff=trange(0))/(24.*60)+0.5)

set_mpan,/init

set_mpan
qv = where(ace.bmag lt 9.9e3)          
pmm, ace(qv).bmag
utplot, ace(qv).time,ace(qv).bmag, $
  yra=[-30,30], yst=1, ytit='Bt Bz (gsm)', yminor=1, $
  timer=trange, xst=xst, psym=3, charsiz=csiz, /nolabel, xtickname=xtknm
qv = where(ace.bgse_z lt 9.9e3)          
pmm, ace(qv).bgse_z
outplot,ace(qv).time,ace(qv).bgse_z, psym=3, color=1
;axis, xaxis=0, xtickname=strarr(60)+' '	;, xticks=nint	;,xminor=4
;axis, xaxis=1, xtickname=strarr(60)+' '	;, xticks=nint	;,xminor=4
outplot, trange,[0,0], linestyle=2  

title='ACE MAG & SWEPAM'
xyouts,!x.window(0)+0.27,!y.window(1)+0.02,/norm, charsiz=2,title

set_mpan
qv = where(ace.sw_density lt 999)          
pmm, ace(qv).sw_density
utplot, /ylog, ace(qv).time,ace(qv).sw_density, $ 
;  yra=[0.1,10], yst=1, ytit='Density (/cm3)', $
  yra=[1,100], yst=1, ytit='Density (/cm3)', $
  timer=trange, xst=xst, psym=3, charsiz=csiz, color=2, /nolabel, xtickname=xtknm
;axis, xaxis=0, xtickname=strarr(60)+' '	;, xticks=nint	;,xminor=4
;axis, xaxis=1, xtickname=strarr(60)+' '	;, xticks=nint	;,xminor=4
outplot, trange,[1,1]*10   

set_mpan
qv = where(ace.speed lt 9999)
pmm, ace(qv).speed         
utplot, ace(qv).time,ace(qv).speed, $ 
  yra=[200,1000], yst=1, ytit='Vp (km/s)', yminor=1, $
  timer=trange, xst=xst, psym=3, charsiz=csiz, color=3, /nolabel, xtickname=xtknm
;axis, xaxis=0, xtickname=strarr(60)+' '	;, xticks=nint	;,xminor=4
;axis, xaxis=1, xtickname=strarr(60)+' '	;, xticks=nint	;,xminor=4

xst = 1
set_mpan
qv = where(ace.sw_temperature lt 9.9e6)     
pmm, ace(qv).sw_temperature
utplot, /ylog, ace(qv).time,ace(qv).sw_temperature, $
  yra=[1.e4,1.e7], yst=1, ytit='Temp (K)', $
  timer=trange, xst=xst, psym=3, charsiz=csiz, color=4, /nolabel
outplot, trange,[1e5,1e5], linestyle=2   
outplot, trange,[1e6,1e6], linestyle=2

;     axis, xaxis=0, xticks=nint, xtickname=strarr(60)+' ',xminor=2
;axis, xaxis=1, xticks=nint, xtickname=strarr(60)+' ',xminor=2

;;xticknam = ['06/01',' ','06/03',' ','06/05',' ','06/07',' ','06/09',' ','06/11']
tt = addtime(replicate(trange(0),nint+1),delt=indgen(nint+1)*24*60.)   
xticknam = strmid(anytim(tt,/ecs,/date),5,5)
xticknam = strmid(anytim(tt,/ecs,/date),8,2)
help,nint
if nint gt 5 then xticknam(indgen((nint+1)/2)*2+1)=' '                             
;axis, xaxis=0, xticks=nint, xtickname=xticknam,xminor=2,charsiz=2

ut_start = 'Begin: '+strmid(anytim(trange(0),/ecs),0,16)+' UT'
xyouts,10,5,/dev, ut_start, charsiz=1.2

end
