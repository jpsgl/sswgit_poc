function rd_lro_crater_dreq, times, verbose=verbose, force=force

common lro_crater, lro_data, dsta, dstp

;tt = ['2012/07/06','2012/07/16']

rsta = time2fid(times(0), /full, /time)
rstp = time2fid(times(1), /full, /time)

if n_elements(lro_data) eq 0 then begin
  dsta = '' &  dstp = ''
endif

doit = 0
if keyword_set(force) then doit = 1

if (rsta lt dsta) or (rsta gt dstp) or (rstp gt dstp) or (rstp lt dsta) or doit then begin
  message,'>>>> Reading all events, dose-rate equivalent data for LRO/CRATER',/info

;
;    read file that goes back 31 days from the specified end time
;    end time sometimes truncates events; may need date N days after specified end time

; http://prediccs.sr.unh.edu/data/craterProducts/doserates/data/
; 2015060/doserates_equivalent_2015060_31days_allevents.txt

deld = 2			; step deld days in to future in case end of event truncated
ttx = anytim(times,/ecs)
ttx(1) = anytim(addtime(ttx(1),del=deld*24*60.),/ecs)

;    define the filename and storage location of required file 

doys = anytim2doy(ttx,year=yys)
yydoy = string(yys(1),doys(1),format='(i4,i3.3)')

ndays = addtime(ttx(1),diff=ttx(0))/60./24.
if ndays le 31 then dur=31 else dur=183
durn = strtrim(string(dur),2)

dfname = yydoy + '/doserates_equivalent_' + yydoy + '_' + durn + 'days_allevents.txt'

;    form the command and request the data

root = 'http://prediccs.sr.unh.edu/data/craterProducts/doserates/data/'

url = root+dfname
cmd = wget_stream() + url
print,cmd

spawn, cmd, resp
;    >>>> what if get no return...

qq = roro_tfile(resp)			; make it appear as through reading a file...
help, qq

sz = size(qq,/dim)
nrow = sz(1)

; columns: Julian Date,Year,DOY,Year Fraction,H2O Factor,Altitude Factor,Good Event Factor,
;      D1&2 dose rate,D3&4 dose rate,D5&6 dose rate,
;      D1 dose rate,D2 dose rate,D3 dose rate,D4 dose rate,D5 dose rate,D6 dose rate

; H2O Factor,Altitude Factor,Good Event Factor
; h20_fact, alt_fact, goodev_fact

temp = {time:'', $
		cdr12:0.0, cdr34:0.0, cdr56:0.0, $										; combined dose rates
		drate1:0.0, drate2:0.0, drate3:0.0, drate4:0.0, drate5:0.0, drate6:0.0}	; channel dose rates
stc = replicate(temp, nrow)

for jrow=0,nrow-1 do begin
  mjd = double(qq(0,jrow)) - 2400000.5
  stc(jrow).time = mjd2any(mjd, /ecs)
  for j=1,9 do stc(jrow).(j) = qq(j+6,jrow)
endfor

lro_data = stc

if keyword_set(verbose) then help,/st,stc(10)
fmt_timer, stc.time, tmin, tmax, /quiet

dsta = time2fid(tmin, /full, /time)
dstp = time2fid(tmax, /full, /time)

;;ptimes = anytim([tmin, tmax], /ecs,/trunc)
;;print, ptimes

endif else begin
  message, 'Data is already loaded',/info
  stc = lro_data
endelse

print, [rsta, rstp, dsta, dstp]

if keyword_set(verbose) then begin
  title = 'LRO CRATER Data'
  utplot_io,stc.time,stc.cdr12,yra=[1e-2,1e4],timer=times,/xst, tit=title		;, color=1
  outplot,stc.time,stc.cdr34                 
  outplot,stc.time,stc.cdr56
endif

return, lro_data
end

; zz = rd_lro_crater_dreq( ['2012/07/06','2012/8/01'], /verbose)