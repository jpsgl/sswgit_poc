function rd_mssl_goesp, files, verbose=verbose

nf = n_elements(files)
print,'No. of files:', nf

;    guess at no of elements in output structure
nv = nf * 12*24		;*31
print,'Guess at No. of records:', nv

; YR MO DA  HHMM    Day     Day     
; P > 1     P > 5     P >10     P >30     P >50     P>100     
; E>0.8     E>2.0     E>4.0

;    match the names to those in the NGDC files so that plot routine works...     
temp = {time:'', $
	I1:0.0,  I2:0.0,  I3:0.0,  I4:0.0,  I5:0.0,  I7:0.0, $	; integrated proton
	E0:0.0,  E1:0.0,  E2:0.0}	;electrons
out = replicate(temp,nv)

knt = 0L

message, 'Reading files',/info

kount = 0
for kf = 0,nf-1 do begin

  file = files(kf)
  if keyword_set(verbose) then print,'Reading: ',file
  qq = rd_tfile(file,2,10,/auto)
;  help,qq

  if n_elements(qq) lt n_tags(temp) then continue		; file problem
  kount++
  sz = size(qq)
  nrow = sz(2)
  ncol = sz(1)

  recs = replicate(temp,nrow)
;help,recs

  tt = string(reform(byte(qq(3,*)),2,2,nrow))  
  times = qq(0,*)+'/'+qq(1,*)+'/'+qq(2,*)+' '+tt(0,*)+':'+tt(1,*)
;  help,nrow,times

;    load the structure
  recs.time = reform(times) 
  for j=6,ncol-2 do recs.(j-5) = reform(qq(j,*))

  out(knt:knt+nrow-1) = recs
;  help,knt
  knt = knt+nrow
endfor

;help, kount
box_message,['No of files read:'+string(kount,nf,format='(i8,1h/,i3)'), $
             'No. of records:  '+string(knt)]
;help,knt
help,out

;    check is anything found
if knt gt 0 then out = out(0:knt-1) else begin
  message,'>>>>> NO GOES Proton data found',/info
  out=-1
endelse

return,out
end


pro mssl_goesp_files, times, struct, verbose=verbose, force=force

message,'Using files from MSSL',/info

;    create array of required filed
tints = anytim2ints(times)
fmt_timer, tints

dys = indgen(max(tints.day)-min(tints.day)+1) + min(tints.day) 
ndys = n_elements(dys)
help, ndys

tx = anytim2ints(times)            
txx = replicate(tx(0),ndys)    
txx.day = dys

;    now form the file names

;    http://www.swpc.noaa.gov/ftpdir/lists/particle/ 20120310_Gp_part_5m.txt
;    20110108_Gp_part_5m.txt

files = time2fid(txx,/full) + '_Gp_part_5m.txt'
root = 'http://www.mssl.ucl.ac.uk/~rdb/goes-particle/'			; mssl
alt_root = 'http://www.swpc.noaa.gov/ftpdir/lists/particle/'	; noaa

url = root+files
;    check if file aavilable at mssl; if not try noaa
if sock_size(url(0)) le 0 then begin
  message,'MSSL not avaiable, trying NOAA site',/info
  url = alt_root+files
  if keyword_set(verbose) then print,url
endif

temp = goes_temp_dir()
if not is_dir(temp) then mk_dir,temp

break_file,url,aa,bb,cc,dd
out = temp+'/'+cc+dd		; need to check is already available...

message, 'Copying required file(s)', /info
print,'Local Directory: ',temp

nurl = n_elements(url)
for j=0,nurl-1 do begin

  curl = url(j)
  print, files(j)		; curl

goto, xxxcc
  if not file_exist(out(j)) then begin
    exist = sock_check(curl) 
    if exist then begin
;      print,'Exists, size (MB): ',sock_size(curl,err=err)/1.e6

      cmd = wget_copy(url(j),outdir=temp)		; should be o/s independent

      if keyword_set(verbose) then print,cmd
      spawn, cmd
      
    endif else message,'>>>> file not found <<<<',/info    
  endif else print,'File exists locally: '+out(j)

xxxcc:

  cmd = wget_copy(url(j),outdir=temp)		; should be o/s independent

  doit = 0
  if not keyword_set(force) then rsiz = 100  else $
    rsiz = sock_size(curl)
  if file_exist(out(j)) then begin
    lsiz = file_size(out(j))
    print,'File exists locally', lsiz,rsiz
    if lsiz lt rsiz then begin
      print,'MISMATCH in file size - RECOPY'
      if rsiz gt 0 then doit=1
    endif
  endif else if rsiz gt 0 then doit=1
  if doit then spawn,cmd 


endfor

struct = rd_mssl_goesp(out, verbose=verbose)

help,struct
help,struct,/st

end