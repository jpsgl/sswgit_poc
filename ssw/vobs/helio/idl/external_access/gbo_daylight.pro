function gbo_daylight, doy, observatory=observatory, $
		       local=local, force=force, debug=debug

common gbo_srss, obs_times

;  read in data is not already loaded
if n_elements(obs_times) eq 0 or keyword_set(force) then begin
  gbo_dtb = concat_dir('$GBO_INFO','gbo_srss.sav')
  if keyword_set(local) then gbo_dtb = 'gbo_srss.sav'
  restore, gbo_dtb 
  help,obs_times
  help,obs_times, /st
endif

obs = 'BBSO'
if keyword_set(observatory) then obs=strupcase(observatory(0))
;help, obs

qv = where(obs_times.observatory eq obs)
if qv(0) ge 0 then kobs = qv(0) $
  else begin
;    message,'Observatory not matched',/cont
    return,-1
  endelse

;  spline interpolation to get date required
sr = spline(obs_times(1).doy, obs_times(kobs).sunrise, doy)
ss = spline(obs_times(1).doy, obs_times(kobs).sunset, doy)
;help, obs

;    add in offset dependent on the longitude of the observatory
toff = obs_times(kobs).longitude/360.*(24.*60)

if keyword_set(debug) then print, obs, sr, ss, toff

return, [sr, ss] + toff
end
