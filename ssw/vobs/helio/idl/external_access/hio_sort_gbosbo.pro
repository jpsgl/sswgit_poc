;+
; PROJECT:
;    HELIO
; NAME:
;    hio_sort_gbosbo
; PURPOSE:
;    Sort the list of observatories for the plot routine
;    Ground-based observatories on bottom, space-based on top, alphabetic
; CALLING SEQUENCE:
;    sobs = hio_sort_gbosbo(observatory, /longitude)
; CATEGORY:
;    
; INPUTS:
;    observatory      list of observatories to sort
; INPUT KEYWORDS:
;    longitude        sort GBO by longitude [default is alphabetic]
;    debug            additional debug info
;    force            force a reload of the information from the ICS
; OUTPUTS:
;    sobs             order of observatories
; OUTPUT KEYWORDS:
;    
; RESTRICTIONS:
;    
; HISTORY:
;    22-Jul-2005  rdb  written
;    02-Jun-2011  rdb  Use call to ICS instead of saved EGSO related data
;    09-Mar-2012  rdb  Mdified searcg date range; tidied
;    11-Mar-2012  rdb  Took out bugs related to ICS param names for locn.
;    27-Jul-2016  rdb  Added fix for GONG sites in Tucson; some reordering 
;
;-

; IDL Version 6.0 (linux x86 m32)
; Journal File for rdb@mssll1.mssl.ucl.ac.uk
; Working directory: /disk/home/rdb/tran_0705b
; Date: Fri Jul 22 14:18:37 2005

 
function hio_sort_gbosbo, observatory, longitude=longitude, debug=debug, force=force

common ics_return, ics_obs

;;ww = egso_ssr_search(obs=observatory, debug=debug)

message,'Entering',/info

;    make a query to the ICS to get the information about the Observatories

if n_elements(ics_obs) eq 0 or keyword_set(force) then begin
  tt = ['1-jan-1980','1-jan-2020']
  query = hio_form_ics_query(tt)		; defaults to observatory
  if keyword_set(debug) then print,query
  ics_obs = ssw_hio_query(query, /conv)		;<<<<<<< should only need to do this once !!!!!!!!!!!!
  
;    fix to handle test facility of GONG in Tucson (needed until all ICS instances updated)
  qx = where(ics_obs.name eq 'TUCS', nqx)
  if nqx eq 0 then begin
    print, 'Adding information about Tucson to ics_obs'			; missing from old DTB
    stc_rec = ics_obs(0)
    tucson = ['TUCS','GBO','111.00','32.25','','1980-01-01','2020-01-01','O','NSO at Tucson, AZ USA','']
    for j=0,9 do stc_rec.(j)=tucson(j)
  endif else stc_rec = ics_obs(qx(0))
  
  new_rec1 = stc_rec & new_rec1.name = 'TCSA'                                                           
  new_rec2 = stc_rec & new_rec2.name = 'TCSB'
  ics_obs = [ics_obs, new_rec1, new_rec2]                                                            

  ics_obs = add_tag(ics_obs,'','observatory')
  ics_obs.observatory = str_replace(ics_obs.name,'-','_')
  ics_obs.observatory = strupcase(ics_obs.observatory)		; ICS uses mixed case, DPAS upper

  qv = where(strpos(ics_obs.observatory, 'PROBA') eq 0, nqv)  		;<<<<<<
  if nqv eq 1 then ics_obs(qv(0)).observatory = 'PROBA2'			;<<<<<<
endif

if keyword_set(force) then begin
  message,'Forced reload of the ICS info.',/info
  return, -1
end

nobs = n_elements(observatory)
pnts = intarr(nobs)-1
for j=0,nobs-1 do begin
  qv = where(ics_obs.observatory eq observatory(j), nqv)
  if nqv eq 1 then pnts(j)=qv(0)
  if !hio_sysvar.debug eq 1 then print, observatory(j), nqv
endfor

if keyword_set(debug) then begin
  print,pnts
  print,observatory
endif

;if keyword_set(debug) then begin
;  help,ww & help,ww,/st
;  print,ww.observatory
;endif


ww = ics_obs(pnts)

;    if there is more than 1 Observtory, sort it

if n_elements(ww) gt 1 then begin

;    check for mismatch - should only happen if not in SSR file
  if n_elements(ww) ne n_elements(observatory) then begin
    box_message,'>>>>  Problem with HELIO ICS return  <<<<'
    print,observatory
    print,ww.observatory
    return, indgen(n_elements(observatory))			; FUDGE !!!!
  endif

  qqg = where(ww.loc_gen eq 'GBO', nqqg)
  qqs = where(ww.loc_gen ne 'GBO', nqqs)
  if keyword_set(debug) then help,qqs,qqg

;    Ground-based observatories sorted by alphabetic or longitude

  if nqqg gt 0 then begin
    sqqg = qqg(sort(ww(qqg).observatory))			; GBO alpabetic
    if keyword_set(longitude) then begin
      box_message,'** Sort of GBO by LONGITUDE requested **'
      sqqg = qqg(sort(float(ww(qqg).loc_p1)))		; GBO by longitude
    endif else box_message,'** Default to ALPHABETIC Sort of GBO **'
  endif else sqqg=-1

;    Sort Space-based observatories alphabetically

  if nqqs gt 0 then begin
    sqqs = qqs(sort(ww(qqs).observatory))			; SBO alphabetic
  endif else sqqs=-1
  
  sobs = [sqqg,sqqs]
  sobs = sobs(where(sobs ge 0))
  
endif else sobs=0

if keyword_set(debug) then print,sobs

return, sobs
end
