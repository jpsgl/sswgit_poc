function goes_set_satnum, t0, t1, prefix=prefix, $
primary=primary, secondary=secondary, goes8=goes8, goes9=goes9, goes10=goes10, $
goes11=goes11, goes12=goes12, goes14=goes14, goes15=goes15 

primary=keyword_set(primary)
secondary=keyword_set(secondary)
case 1 of                         ; Which data base (satellite)?
  primary: sat='Gp_'
  secondary: sat='Gs_'
  keyword_set(goes8): sat='G8'
  keyword_set(goes9): sat='G9'
  keyword_set(goes10): sat='G10'
  keyword_set(goes12): sat='G12'
  keyword_set(goes11): sat='G11'
  keyword_set(goes14): sat='G14'	;sat='Gp_'
  keyword_set(goes15): sat='G15'	;sat='Gp_'
  else: begin
     sat=(['G9','G10'])(ssw_deltat(t1,ref='25-jul-1998') gt 0)       
     sat=([sat,'G11'])(ssw_deltat(t0,ref='21-jun-2006') gt 0)
;;;;;     sat=([sat,'G10'])(ssw_deltat(t0,ref='1-apr-2009') gt 0)
;>>     sat=([sat,'G10'])(ssw_deltat(t0,ref='01-dec-2008') gt 0)
     sat=([sat,'G10'])(ssw_deltat(t0,ref='11-feb-2008') gt 0)
;;     sat=([sat,'Gp_'])(ssw_deltat(t0,ref='26-nov-2009') gt 0)
     sat=([sat,'G14'])(ssw_deltat(t0,ref='26-nov-2009') gt 0)
     sat=([sat,'G15'])(ssw_deltat(t0,ref='28-oct-2010') gt 0)
  endcase
endcase  

if keyword_set(prefix) then begin
  if strlen(sat) eq 2 then sat = str_replace(sat,'G','G0')
  sat = str_replace(sat,'G',prefix)
endif

;help,sat
return, sat
end
