function goes_fitsid, time

;	form the names of the GOES files - depends on satellite and date

common taquila, qqgxd

zz = anytim2ints(time)
nd = max(zz.day,min=dmn)-dmn+1
dd = replicate(zz(0),nd)            
dd.day = indgen(nd)+dmn               
ftime = time2fid(fmt_tim(dd),/full)

stime = strmid(ftime(0),0,6)
help,stime
sat = 'go06'
if stime ge '199406' then sat = 'go07' 
if stime ge '199606' then sat = 'go08' 
if stime ge '200206' then sat = 'go10' 

help,time
sat = goes_set_satnum(time(0),time(1),prefix='go')
help,sat

;; before 15-jan-99 names did not include century; afterwards they did
files=sat+ftime+'.fits'
for j=0,nd-1 do begin
;;  td = addtime(fmt_tim(dd(j)),diff='15-jan-99') 
;;  print,fmt_tim(dd(j)),td
;;  if td lt 0 then files(j)=sat+strmid(ftime(j),2,6)+'.fits
  if ftime(j) lt '19990115' $
	then files(j)=sat+strmid(ftime(j),2,6)+'.fits
endfor
;;files = strmid(ftime,0,4)+'/'+files

goto, skip

if n_elements(qqgxd) eq 0 then begin
  print,'Ingest GOES data map'
  qqgxd = rd_tfile('list_of_goes_at_SDAC.txt')   ;<<<<<<<<<<<<<<<<<<<<<
  help,qqgxd
endif

if addtime(time(0),diff='1-jan-00') lt 0 then begin
  qvx = -1
  for j=0,n_elements(ftime)-1 do begin
    qv = where(strpos(qqgxd, strmid(ftime(j),2,6)) gt 0)
    qvx = [qvx,qv]
  endfor
  qvx = qvx(where(qvx ge 0))

  qx = where(strpos(qqgxd(qvx), sat) ge 0)
  files = qqgxd(qvx(qx))

endif else files=sat+ftime+'.fits'

skip:

return, files
end

pro goes_files, time, ostruct, debug=debug, verbose=verbose    ;silent=silent

;	routine to access GOES files at GSFC using "curl" instead of "sockets"
;	recognizes that sockets do not work on all sites
;	should method using sockets be folded into this??

;  14/06/28  rdb  check if file exists, skip if not

files = goes_fitsid(time)
gfiles = strmid(files,4,4)+'/'+files

;print,files

root = 'http://hesperia.gsfc.nasa.gov/goes/'
url = root+gfiles
if keyword_set(verbose) then print,url

temp = goes_temp_dir()
if not is_dir(temp) then mk_dir,temp
out = temp+'/'+files
;print,out

;	first get the GOES files

if not keyword_set(debug) then begin

  if not have_network(root) then begin
    message,'GOES Server not online',/cont
    return
  end
  message,'Copying required file(s)',/info

;	curl commands:
;	-s == silent; -R == same dates as source; -V version
;	should we check if CURL is present  (use curl -V)??

  for j=0,n_elements(files)-1 do begin
;    check if files exists - require override??
    if not file_exist(out(j)) then begin
      print,files(j)
      chk = sock_check(url(j))
      if chk eq 0 then begin		;<<<<<<<<<<<< file not found  14/06/28
        files(j) = ''
        continue
      endif
;      cmd = 'curl -o '+out(j)+' '+url(j)	;-s
      cmd = wget_copy(url(j), outdir=temp)
      if keyword_set(verbose) then print,cmd
      spawn, cmd
    endif else message,'File exists: '+out(j),/cont
  endfor

  qv = where(files ne '', nqv)
  if nqv gt 0 then files = files(qv) else begin
    message,'*** ERROR - no GOES data files ***',/info
    return
  endelse

endif

;	then read the data from the files

nd = n_elements(files)
;nd3 = 86400./3*nd
nd3 = 86400./2*nd			; need more points...
ostruct = {time:fltarr(nd3), flux:fltarr(2,nd3), base_time:'', mjd_base:fltarr(nd)}
;;help,ostruct,/st

message,'Reading files',/info
for j=0,nd-1 do begin

  if keyword_set(verbose) then print,out(j)
  struct = mrdfits(out(j),2,head,/silent)
;  help,struct,/st
  hh=fitshead2struct(head)
;;  help,hh,/st
;;  print,head,format='(a)'

;    save input days
  if j eq 0 then begin
    base_mjd = hh.timezero
    ostruct.base_time = mjd2any(base_mjd,/yoh)
    nel = n_elements(struct.time)
    ostruct.time(0:nel-1) = struct.time(*)
    ostruct.flux(*,0:nel-1) = struct.flux(*,*)
    idx = nel

  endif else begin
    delta = (hh.timezero-base_mjd)*86400.
    nel = n_elements(struct.time)
    ostruct.time(idx+0:idx+nel-1) = struct.time(*)+delta
    ostruct.flux(*,idx+0:idx+nel-1) = struct.flux(*,*)
    idx = idx+nel
  endelse

  if keyword_set(verbose) then print,j,nel,idx
  ostruct.mjd_base(j) = hh.timezero

endfor

;    truncate arrays
ostruct.time = ostruct.time(0:idx-1)
ostruct.flux = ostruct.flux(*,0:idx-1)

end
