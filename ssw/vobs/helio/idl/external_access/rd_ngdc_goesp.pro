;
; http://satdat.ngdc.noaa.gov/sem/goes/data/avg/2005/Z1050501.TXT
;
;  XL   1-8 Ang X-ray Flux (Watts/Meter2)
;  XS  .5-3 Ang X-ray Flux (Watts/Meter2)
;  Hp  (parallel) Northward Magnetic Flux (nanotesla)
;  He  Earthward Magnetic Flux            (nanotesla)
;  Hn  (normal) Eastward  Magnetic Flux   (nanotesla)
;  Ht  Total Magnetic Flux                (nanotesla)
;  
;  E1 >= 2 MeV electrons (Counts/cm2 sec sr) Corrected, but remain unreliable during ion storms.
;  P1    .8 -   4.0 MeV protons (Counts/cm2 sec sr MeV) Corrected
;  P2   4.0 -   9.0 MeV protons (Counts/cm2 sec sr MeV) Corrected
;  P3   9.0 -  15.0 MeV protons (Counts/cm2 sec sr MeV) Corrected
;  P4  15.0 -  40.0 MeV protons (Counts/cm2 sec sr MeV) Corrected
;  P5  40.0 -  80.0 MeV protons (Counts/cm2 sec sr MeV) Corrected
;  P6  80.0 - 165.0 MeV protons (Counts/cm2 sec sr MeV) Corrected
;  P7 165.0 - 500.0 MeV protons (Counts/cm2 sec sr MeV) Corrected
;  
;  Bad Mag data are filled with: 327.11
;  Bad X-ray and Particle data are filled with 3.27E+04 
;
;
; http://satdat.ngdc.noaa.gov/sem/goes/data/avg/2005/I1050501.TXT
;
; XL   1-8 Ang X-ray Flux (Watts/Meter2)
; XS  .5-3 Ang X-ray Flux (Watts/Meter2)
; Hp  (parallel) Northward Magnetic Flux (nanotesla)
; He  Earthward Magnetic Flux            (nanotesla)
; Hn  (normal) Eastward  Magnetic Flux   (nanotesla)
; Ht  Total Magnetic Flux                (nanotesla)
; 
; E1 >= 2 MeV electrons (Counts/cm2 sec sr) Corrected, but remain unreliable during ion storms.
; I1 >   1 MeV protons (Counts/cm2 sec sr) Corrected
; I2 >   5 MeV protons (Counts/cm2 sec sr) Corrected
; I3 >  10 MeV protons (Counts/cm2 sec sr) Corrected
; I4 >  30 MeV protons (Counts/cm2 sec sr) Corrected
; I5 >  50 MeV protons (Counts/cm2 sec sr) Corrected
; I6 >  60 MeV protons (Counts/cm2 sec sr) Corrected
; I7 > 100 MeV protons (Counts/cm2 sec sr) Corrected
; 
; Bad Mag data are filled with: 327.11
; Bad X-ray and Particle data are filled with 3.27E+04 


function rd_ngdc_goesp, files

nf = n_elements(files)
print,'No. of files:', nf

;    guess at no of elements in output structure
nv = nf * 12*24*31
print,'Guess at No. of records:', nv

; first 3 columns are the date.. - YYMMDD HHMM DAY
; followed by 14 other parameters
     
temp = {time:'',XL:0.0, XS:0.0, HP:0.0, HE:0.0, HN:0.0, HT:0.0, E1:0.0, $ 
;	P1:0.0,  P2:0.0,  P3:0.0,  P4:0.0,  P5:0.0,  P6:0.0,  P7:0.0}	; ranges
	I1:0.0,  I2:0.0,  I3:0.0,  I4:0.0,  I5:0.0,  I6:0.0,  I7:0.0}	; integrated
out = replicate(temp,nv)

knt = 0

for kf = 0,nf-1 do begin

  file = files(kf)
  print,'Reading: ',file
  qq = rd_tfile(file,18,26)

  help,qq
  if n_elements(qq) lt n_tags(temp) then continue		; file problem
  sz = size(qq)
  nrow = sz(2)
  ncol = sz(1)

  times = strarr(nrow)
  recs = replicate(temp,nrow)
;help,recs

;    extract the time from the first two fields
  tt = qq(0,*)+qq(1,*)
  ttx = string(reform(byte(tt),2,5,nrow))
  for j=0,nrow-1 do times(j)=string(ttx(*,j),format='(2h20,a,2(1h/,a),x,a,1h:,a)')

;    load the structure
  recs.time = times 
  for j=3,ncol-2 do recs.(j-2) = reform(qq(j,*))

  out(knt:knt+nrow-1) = recs
  help,knt
  knt = knt+nrow
endfor

help,out
help,knt

;    check is anything found
if knt gt 0 then out = out(0:knt-1) else begin
  message,'>>>>> NO GOES Proton data found',/info
  out=-1
endelse

return,out
end
