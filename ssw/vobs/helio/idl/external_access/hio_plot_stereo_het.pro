;+
; NAME:
;		hio_mplot_ace
; PURPOSE:
;    		Reads and plots ACE in-situ data from MAG and SWEPAM
;		Plot has four panel (BT/Bz, Density, Vp, Tp) 
; INPUTS:
;		times	time-range of the plot  (2 elements array)
; INPUT KEYWORDS:
;		title	title of the plot window
; OUTPUT KEYWORD:
;		err	set if error occurred in reading the data	
; CALLING SEQUENCE:
;		hio_mplot_ace, times, title=title, err=err
; RESTRICTIONS:
;
; HISTORY:
;		   Jan 2912  rdb  written
;-

pro hio_plot_stereo_het, times, title=title, mission=mission, $
	window=window, verbose=verbose, err=err

;    ace structure supplied by read routine...

common omstereo, ace, acsta, acstp, cmiss_sav

common stx_xx2, sta_struct, sta_sta, sta_stp, stb_struct, stb_sta, stb_stp

;    only copy if data is not already loaded
;    then only copy if falls outside of what is already loaded
;????    extend interval copied so can shift slightly ????

rsta = time2fid(times(0),/full,/time)
rstp = time2fid(times(1),/full,/time)
help,rsta,rstp

if n_elements(ace) eq 0 then begin
  acsta='' & acstp=''
endif

if n_elements(sta_struct) eq 0 then begin
  sta_sta='' & sta_stp=''
endif

if n_elements(stb_struct) eq 0 then begin
  stb_sta='' & stb_stp=''
endif

;help, sta_struct, sta_sta, sta_stp, stb_struct, stb_sta, stb_stp

cmiss = 'STA'
if keyword_set(mission) then begin
  cmiss=strupcase(mission)
  if cmiss ne 'STA' then cmiss='STB'
endif
fmiss = 'STEREO-A'
if cmiss eq 'STB' then fmiss = 'STEREO-B'
ctit = fmiss + ' Mag & Plasma'

ptit = ctit
if keyword_set(title) then ptit = title

;????  yaxis title for mag. field depends on spacecraft  ace/stx


doit = 0
if n_elements(cmiss_sav) gt 0 then begin
  message,'>>>> Matching saved information for '+cmiss,/info
  if cmiss eq 'STA' then begin
    help, sta_struct, sta_sta, sta_stp
    if n_elements(sta_struct) gt 0 then begin
      ace=sta_struct & acsta=sta_sta & acstp=sta_stp
    endif else doit=1
  endif
  if cmiss eq 'STB' then begin
    help, stb_struct, stb_sta, stb_stp
    if n_elements(stb_struct) gt 0 then begin
      ace=stb_struct & acsta=stb_sta & acstp=stb_stp
    endif else doit=1
  endif
endif

err = 0

;    need to also trap case where the mission has changed...
;doit = 0
;if n_elements(cmiss_sav) eq 0 then doit=1 else if cmiss_sav ne cmiss then doit=1

if (rsta lt acsta) or (rsta gt acstp) or (rstp gt acstp) or (rstp lt acsta) or doit then begin
  message,'>>>> Reading data for '+cmiss,/info

;;  ace = ucla_stereo_post(timerange=times, mission=cmiss, verbose=verbose)
  caltech_stereop_files, times, ace, mission=mission, debug=verbose
  
  help,/st,ace
  if datatype(ace) ne 'STC' then begin
    message,'Problem reading STEREO Data',/info
    err = 1			; flag error to calling routine
    return
  endif

  nel = n_elements(ace)
  acsta = time2fid(ace(0).time,/full,/time)
  acstp = time2fid(ace(nel-1).time,/full,/time)  ;; <<<<

  if cmiss eq 'STA' then begin		; STA
    sta_struct=ace & sta_sta=acsta & sta_stp=acstp
  endif else begin			; STB
    stb_struct=ace & stb_sta=acsta & stb_stp=acstp
  endelse

endif else message, 'Data is already loaded',/info

cmiss_sav = cmiss
help, ace, acsta, acstp, cmiss_sav

;    now do the multi=panel plot

;fmt_timer, ace.time, tmin, tmax, /quiet
;trange = [tmin, tmax]
;if keyword_set(timerange) then trange=timerange

trange = times
fmt_timer, trange

ctitle = fmiss+' IMPACT-HET'
if keyword_set(title) then ctitle=title

csiz = 2.	;1.8

goesp=ace

pwind = 0
if keyword_set(window) then pwind=window

window, pwind, xsiz=700,ysiz=500, title=ctitle	

;!p.position=0
;!p.multi=[0,1,4]
;!y.margin=[2,0.5]& !x.margin=[9,8]

;    do the plot

;;  yrange=[1.e-2, 1.e4]
	xmargin=[10,6] & charsiz=1.2
 
lsiz=1.4

utplot_io, times, [1.e-9,1.e-9], /nodata, $
	timer=times, xst=1, /year, $
	title=ctitle, $
	charsize=charsize, xmargin=xmargin, $
;	ytickname=['10!U-2!N','10!U-1!N','10!U0!N','10!U1!N','10!U2!N','10!U3!N','10!U4!N'], $
;	yrange=[1.e-2, 1.e4], yst=1, ytit='Protons (cm!U-2!Ns!U-1!Nsr!U-1!N)'
	ytickname=['10!U-4!N','10!U-3!N','10!U-2!N','10!U-1!N','10!U0!N','10!U1!N','10!U2!N','10!U3!N'], $
	yrange=[1.e-4, 1.e3], yst=1, ytit='Protons (cm!U-2!Ns!U-1!Nsr!U-1!N)'

;    plot some horizontal grid lines
ls = [0,0,2,0,0]
ls = [0,0,0,0,0,0]
for j=0,5 do outplot,[!x.crange(0),!x.crange(1)],[1e-3,1e-3]*10.^j,linestyle=ls(j)

;    explain the colour of the lines
xsc=(!x.crange(1)-!x.crange(0))
xp = !x.crange(1)+xsc*.04
if xmargin(0) eq 18 then xp = !x.crange(0)-xsc*.15
xyouts, xp, 5e-3,/data, orient=90,'>=10' ,color=1, charsiz=lsiz*0.7
xyouts, xp, 8e-2,/data, orient=90,'>=50' ,color=2, charsiz=lsiz*0.7
xyouts, xp, 2e0, /data, orient=90,'>=100',color=3, charsiz=lsiz*0.7
xyouts, xp, 8e1, /data, orient=90,'MeV', charsiz=lsiz*0.7

;    find index limits within the data to improve the speed 
t0 = strmid(anytim(times(0),/ecs),0,16)
t1 = strmid(anytim(times(1),/ecs),0,16)
;;p0 = max(where(goesp.time le t0))
;;p1 = min(where(goesp.time ge t1))
ddd = strmid(anytim(goesp.time,/ecs),0,16)
p0 = max([where(ddd le t0),0])                
p1 = min(where(ddd ge t1))                

print,'Index limits:',p0,p1
goesp_pdx = goesp(p0:p1)

qq = where(goesp_pdx.proton(0) lt 3.27e4, nqq)		; 10 Mev
;help,nqq
if nqq gt 0 then outplot, goesp_pdx(qq).time, goesp_pdx(qq).proton(0), color=1

qq = where(goesp_pdx.proton(8) lt 3.27e4, nqq)		; 50 Mev
;help,nqq
if nqq gt 0 then outplot, goesp_pdx(qq).time, goesp_pdx(qq).proton(8), color=2

qq = where(goesp_pdx.proton(10) lt 3.27e4, nqq)		; 100 Mev
;help,nqq
if nqq gt 0 then outplot, goesp_pdx(qq).time, goesp_pdx(qq).proton(10), color=3

ut_start = 'Begin: '+strmid(anytim(trange(0),/ecs),0,16)+' UT'
xyouts,10,5,/dev, ut_start, charsiz=1.2
xyouts,540,5,/dev, 'HELIO', charsiz=1.2		;???

end
