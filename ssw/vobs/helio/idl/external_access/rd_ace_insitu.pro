function omniweb_ace_post, timerange, verbose=verbose

; http://ftpbrowser.gsfc.nasa.gov/ace_merge.html

;curl --data "spacecraft=ace_merge&activity=ftp
;&res=4-min&start_date=19990101&end_date=19990331
;&vars=07&vars=08&vars=09&vars=10&vars=13&vars=14&vars=16" 
; http://omniweb.gsfc.nasa.gov/cgi/nx1.cgi

; <INPUT TYPE="checkbox" VALUE="04" NAME="vars">X, GSE (km)                                                              
; <INPUT TYPE="checkbox" VALUE="05" NAME="vars">Y, GSE (km)                                                               
; <INPUT TYPE="checkbox" VALUE="06" NAME="vars">Z, GSE (km)
; <INPUT TYPE="checkbox" VALUE="07" NAME="vars">Bmag (nT)
; <INPUT TYPE="checkbox" VALUE="08" NAME="vars">Bgse_x (nT)
; <INPUT TYPE="checkbox" VALUE="09" NAME="vars">Bgse_y (nT)
; <INPUT TYPE="checkbox" VALUE="10" NAME="vars">Bgse_z (nT)
; <INPUT TYPE="checkbox" VALUE="11" NAME="vars">No of points
; <INPUT TYPE="checkbox" VALUE="12" NAME="vars">Quality flag
; <INPUT TYPE="checkbox" VALUE="13" NAME="vars">Density, (N/cm^3)
; <INPUT TYPE="checkbox" VALUE="14" NAME="vars">Temperature, K
; <INPUT TYPE="checkbox" VALUE="15" NAME="vars">He/H ratio
; <INPUT TYPE="checkbox" VALUE="16" NAME="vars">Speed, (km/s)
; <INPUT TYPE="checkbox" VALUE="17" NAME="vars">Vx, GSE (km/s)
; <INPUT TYPE="checkbox" VALUE="18" NAME="vars">Vy, GSE (km/s)
; <INPUT TYPE="checkbox" VALUE="19" NAME="vars">Vz, GSE (km/s)


if n_params() eq 1 then begin
  fmt_timer, timerange
  times = time2fid(timerange,/full)
endif else times = ['19990101','19990110']
print,times

;--    Use POST command to Web processor to retrieve information about data and format

;    form the command string

sect1 = 'spacecraft=ace_merge&res=4-min&activity=ftp'
;;sect2 = '&start_date=19990101&end_date=19990110'
sect2 = '&start_date='+times(0) + '&end_date='+times(1)
sect3 = '&vars=07&vars=08&vars=09&vars=10&vars=13&vars=14&vars=16&vars=11'
post_command = '"' + sect1+sect2+sect3 + '" '

;    URL of the page that processes the OnmiWeb page

post_url = 'http://omniweb.gsfc.nasa.gov/cgi/nx1.cgi'

;    Syntax of POST using CURL
; curl --data post_string post_url
;    Equivalent in WGET
; wget --post-data=post_string post_url

cmd = wget_post() + post_command + post_url

print,cmd
spawn, cmd, resp

qv = where(strpos(resp,'ace_merge_') gt 0, nqv)
help,nqv

if nqv ne 2 then begin
  message,'Problem with the return from OMNIWeb',/info
  qerr =  where(strpos(resp,'WRONG START/STOP DAY') gt 0, nqerr)
  if nqerr gt 0 then box_message, gt_brstr(resp(qerr(0)),'<TT>','</TT>')
  return, -1                        
endif

qq = resp(qv)
url_data = gt_brstr(qq(0),'href="','">')
url_fmt  = gt_brstr(qq(1),'href="','">')

print, url_data
print, url_fmt

;    get information about file format

;;cmd = 'curl -s '+url_fmt
cmd = wget_stream() + url_fmt
print,cmd
spawn,cmd,qq
help,qq
                
;    create the structure
     
qqx = qq(4:n_elements(qq)-3)
nqx = n_elements(qqx)
tags = strarr(nqx)
cc = "tstruct = {time:''"

n = 0 & tag = '' & fmt = ''
for j=0,nqx-1 do begin
  reads,qqx(j),n,tag,fmt,format='(i2,x,a25,t32,a)'
;  print,n,tag,fmt
  tagx = strtrim(tag,2)
  tagx = (str2arr(tagx,','))(0)
  tagx = strlowcase(str_replace(tagx,' ','_'))
;  if tagx eq 'sw_temperature' then tagx = 'sw_temp'
  tags(j) = tagx
  if j ge 4 then cc = cc + ", "+tags(j)+":0.0"                            
endfor
print,tags
cc = cc + "}"
print,cc
temp = execute(cc)

;    copy the data file

temp = concat_dir(get_temp_dir(),'ace')
if not is_dir(temp) then mk_dir,temp
;;ace_file = concat_dir(temp,'ace_data.lst')

;;cmd = 'curl -s -o '+ ace_file + ' ' + url_data
cmd = wget_copy(url_data, outdir=temp, file_uri=ace_file)
print,cmd
spawn,cmd,resp

;    now read the file in

vals = rd_tfile(ace_file,11,/auto)
help,vals
nrow = (size(vals))(2)
ncol = (size(vals))(1)

;    populate the structure
                  
ace = replicate(tstruct,nrow)

tt = doy2ex(vals(1,*),vals(0,*))
tt(0,*) = vals(2,*)
tt(1,*) = vals(3,*)
ace.time = anytim(tt,/ccs,/trunc)
for kc = 0,ncol-5 do ace.(kc+1) = reform(vals(kc+4,*))

print,vals(*,18)
help,/st,ace(18)
help,ace
fmt_timer,tt

;    get rid of temporary file
if not keyword_set(verbose) then ssw_file_delete, ace_file	;, delete_status

return, ace
end







function rd_ace_insitu, times, verbose=verbose, err=err

;    ace structure supplied by read routine...

common omace, ace, acsta, acstp

;    only copy if data is not already loaded
;    then only copy if falls outside of what is already loaded
;????    extend interval copied so can shift slightly ????

rsta = time2fid(times(0),/full,/time)
rstp = time2fid(times(1),/full,/time)
help,rsta,rstp

if n_elements(ace) eq 0 then begin
  acsta='' & acstp=''
endif

err = 0
if (rsta lt acsta) or (rsta gt acstp) or (rstp gt acstp) or (rstp lt acsta) then begin
  ace = omniweb_ace_post(times)
  if datatype(ace) ne 'STC' then begin
    message,'Problem reading ACE Data',/info
    err = 1			; flag error to calling routine
    return, -1
  endif
  nel = n_elements(ace)
  acsta = time2fid(ace(0).time,/full,/time)
  acstp = time2fid(ace(nel-1).time,/full,/time)  ;; <<<<
endif else message, 'Data is already loaded',/info

help, ace, acsta, acstp

return, ace
end
