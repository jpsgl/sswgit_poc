pro eau_plot_stxp, times, stb=stb, xmargin=xmargin, charsize=charsize, debug=debug

common stx2, goesp, gpsta, gpstp

sat = 'STEREO-A'
if keyword_set(stb) then sat = 'STEREO-B'

  rsta = time2fid(times(0),/full,/time)
  rstp = time2fid(times(1),/full,/time)
  help,rsta,rstp

  if n_elements(goesp) eq 0 then begin
    gpsta='' & gpstp=''
  endif

  if (rsta lt gpsta) or (rsta gt gpstp) or (rstp gt gpstp) or (rstp lt gpsta) then begin
  
    caltech_stereop_files, times, goesp

;  should these be set to values on ogoes????
;      gsta = time2fid(times(0),/full,/time)
;      gstp = time2fid(times(1),/full,/time)

    nel=n_elements(goesp)
    if datatype(goesp) ne 'STC' then begin
      message,'>>>> Unable to plot STEREO-HET Proton data',/info
      return
    endif

    gpsta = time2fid(goesp(0).time,/full,/time)
    gpstp = time2fid(goesp(nel-1).time,/full,/time)  ;; <<<<
  endif else message, 'Data is already loaded',/info

  help, goesp, gpsta, gpstp

  if n_elements(goesp) eq 0 then begin
;;    message,'No GOES data available',/cont
    gtime=0 & gdata=0 	;& utbase=0
  end else begin
    gtime = goesp.time		;;ogoes->get(/times)
    gdata = goesp.proton(10)	;flux		;;ogoes->get(/data)
;;    utbase = 'xxxx' ;ogoes.base_time
  endelse

  print,'hello'
  help, gtime, gdata   ;, utbase
  print,times


;    do the plot

;;  yrange=[1.e-2, 1.e4]
;;	xmargin=[10,6] & charsiz=1.2
 
lsiz=1.4

utplot_io, times, [1.e-9,1.e-9], /nodata, $
	timer=times, xst=1, /year, $
	title=sat+' IMPACT-HET', $
	charsize=charsize, xmargin=xmargin, $
;	ytickname=['10!U-2!N','10!U-1!N','10!U0!N','10!U1!N','10!U2!N','10!U3!N','10!U4!N'], $
;	yrange=[1.e-2, 1.e4], yst=1, ytit='Protons (cm!U-2!Ns!U-1!Nsr!U-1!N)'
	ytickname=['10!U-4!N','10!U-3!N','10!U-2!N','10!U-1!N','10!U0!N','10!U1!N','10!U2!N','10!U3!N'], $
	yrange=[1.e-4, 1.e3], yst=1, ytit='Protons (cm!U-2!Ns!U-1!Nsr!U-1!N)'

;    plot some horizontal grid lines
ls = [0,0,2,0,0]
ls = [0,0,0,0,0,0]
for j=0,5 do outplot,[!x.crange(0),!x.crange(1)],[1e-3,1e-3]*10.^j,linestyle=ls(j)

;    explain the colour of the lines
xsc=(!x.crange(1)-!x.crange(0))
xp = !x.crange(1)+xsc*.04
if xmargin(0) eq 18 then xp = !x.crange(0)-xsc*.15
xyouts, xp, 5e-3,/data, orient=90,'>=10' ,color=1, charsiz=lsiz*0.7
xyouts, xp, 8e-2,/data, orient=90,'>=50' ,color=2, charsiz=lsiz*0.7
xyouts, xp, 2e0, /data, orient=90,'>=100',color=3, charsiz=lsiz*0.7
xyouts, xp, 8e1, /data, orient=90,'MeV', charsiz=lsiz*0.7

;    find index limits within the data to improve the speed 
t0 = strmid(anytim(times(0),/ecs),0,16)
t1 = strmid(anytim(times(1),/ecs),0,16)
;;p0 = max(where(goesp.time le t0))
;;p1 = min(where(goesp.time ge t1))
ddd = strmid(anytim(goesp.time,/ecs),0,16)
p0 = max(where(ddd le t0))                
p1 = min(where(ddd ge t1))                

print,'Index limits:',p0,p1
goesp_pdx = goesp(p0:p1)

qq = where(goesp_pdx.proton(0) lt 3.27e4, nqq)		; 10 Mev
;help,nqq
if nqq gt 0 then outplot, goesp_pdx(qq).time, goesp_pdx(qq).proton(0), color=1

qq = where(goesp_pdx.proton(8) lt 3.27e4, nqq)		; 50 Mev
;help,nqq
if nqq gt 0 then outplot, goesp_pdx(qq).time, goesp_pdx(qq).proton(8), color=2

qq = where(goesp_pdx.proton(10) lt 3.27e4, nqq)		; 100 Mev
;help,nqq
if nqq gt 0 then outplot, goesp_pdx(qq).time, goesp_pdx(qq).proton(10), color=3

end
