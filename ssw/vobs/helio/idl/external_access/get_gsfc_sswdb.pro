; ftp://sohoftp.nascom.nasa.gov/sdb/ydb/    

pro get_gsfc_sswdb, times, nar=nar, gev=gev, verbose=verbose

;    writes to temp_dir

anytim2weeks,times(0),times(1), ww,yy

weeks = string(ww,format='(i2.2)')
years = string(yy,format='(i2.2)')
week_id = years +'_'+ weeks
;print,week_id

prefix = ''
if keyword_set(gev) then prefix = 'gev'
if keyword_set(nar) then prefix = 'nar'
if prefix eq '' then begin
  message,'Need to request a file',/info
  return
endif

file = prefix + week_id + 'a.01'
print,'Required Files:'
print, file

root = 'ftp://sohoftp.nascom.nasa.gov/sdb/ydb/' 

temp = concat_dir(get_temp_dir(),'ydb')
if not file_exist(temp) then mk_dir, temp

rfile = root + prefix + '/' + file

box_message,['Retrieving GEV files from SSWDB at GSFC',rfile]

cmd = wget_copy(rfile, outdir=temp, file_uri=file_uri)
;print,cmd
;print,file_uri

print,'Reading the required files:'
for j=0,n_elements(cmd)-1 do begin
  print,file(j)
  if file_exist(file_uri(j)) then print,'File exists locally' $
  else begin
;    print, cmd(j)
    if file_exist(rfile(j)) then $
    spawn, cmd(j), resp
    help,resp
  endelse
endfor

; set_logenv,'DIR_GEN_GEV','/Users/temp/hio_temp_dir/ydb'

break_file,file_uri(0), aa,bb,cc,dd
if keyword_set(gev) then set_logenv,'DIR_GEN_GEV', aa+bb
if keyword_set(nar) then set_logenv,'DIR_GEN_NAR', aa+bb

end