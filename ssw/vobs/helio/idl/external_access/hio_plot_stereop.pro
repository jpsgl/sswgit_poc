;+
; PROJECT:
;        HELIO
; NAME:
;        hio_plot_stereop
; PURPOSE:
;        Reads and plots STEREO proton data
; CALLING SEQUENCE:
;        hio_plot_stereop, times, title=title, err=err
; INPUTS:
;        times      time-range of the plot  (2 elements array)
; INPUT KEYWORDS:
;        mission    Select mission to be plotted: STA, STB, STEREO-A or STEREO-B
;        title      title of the plot window; defaults to mission name, etc.
;        window     window no. in which t make the plot
;        several plot keywords can be passed xmargin, charsize, yrange 
; OUTPUT KEYWORD:
;        err        set if error occurred in reading the data	
; RESTRICTIONS:
;
; HISTORY:
;       Jan 2012  rdb  written
;    24-Oct-2014  rdb  Improved documentation
;-

pro hio_plot_stereop, times, title=title, mission=mission, $
	xmargin=xmargin, charsize=charsize, yrange=yrange, $
	window=window, verbose=verbose, err=err, over=over

cmiss = 'STA'
if keyword_set(mission) then begin
  cmiss=strupcase(mission)
  if cmiss ne 'STA' then cmiss='STB'
endif
fmiss = 'STEREO-A'
if cmiss eq 'STB' then fmiss = 'STEREO-B'
ctit = fmiss + ' Protons'

ptit = ctit
if keyword_set(title) then ptit = title

ctitle = fmiss + ' IMPACT-HET'
if keyword_set(title) then ctitle=title

pwind = 0
if keyword_set(window) then pwind=window

if not keyword_set(over) then begin
  window, pwind, xsiz=700,ysiz=500, title=fmiss + ' Protons'
  clearplot
  hio_plot_setup,/inv
endif

cyrange = [1.e-4, 1.e2]
if keyword_set(yrange) then cyrange = yrange
yyl = alog10(cyrange)
yticks = fix(yyl(1)-yyl(0))

print,'yrange = ', cyrange, forma='(a,1h[, e7.1, 2h, , e7.1, 1h])' 
help, yticks
fmt_timer, times


;    now do the plot - do a blank plot then fill in if there are data available

;;xmargin = [10,6]

csiz = 2.	;1.8
lsiz = 1.4

utplot_io, times, [1.e-9,1.e-9], /nodata, $
	timer=times, xst=1, /year, $
	yrange=cyrange, yst=1, $
	ytickname=['10!U-4!N','10!U-3!N','10!U-2!N','10!U-1!N','10!U0!N','10!U1!N','10!U2!N','10!U3!N','10!U4!N'], yticks=yticks, $
	charsize=charsize, xmargin=xmargin, $
	title=ctitle, ytit='Protons (cm!U-2!Ns!U-1!Nsr!U-1!N)'

;    plot some horizontal grid lines

ls = [0,0,2,0,0]
ls = [0,0,0,0,0,0,0]
for j=0,6 do outplot,[!x.crange(0),!x.crange(1)],[1e-3,1e-3]*10.^j,linestyle=ls(j)

;    explain the colour of the lines

xsc=(!x.crange(1)-!x.crange(0))
xp = !x.crange(1)+xsc*.04
if xmargin(0) eq 18 then xp = !x.crange(0)-xsc*.15
xyouts, xp, 5e-3,/data, orient=90,'>=10' ,color=1, charsiz=lsiz*0.7
xyouts, xp, 8e-2,/data, orient=90,'>=50' ,color=2, charsiz=lsiz*0.7
xyouts, xp, 2e0, /data, orient=90,'>=100',color=3, charsiz=lsiz*0.7
xyouts, xp, 8e1, /data, orient=90,'MeV', charsiz=lsiz*0.7


;    structure supplied by read routine...

ace = rd_stereop(times, cmiss, verbose=verbose, err=err)
if datatype(ace) ne 'STC' then begin
  message,'ERROR in read of some kind',/info
  return
endif
goesp = ace


;    find index limits within the data to improve the speed 

t0 = strmid(anytim(times(0),/ecs),0,16)
t1 = strmid(anytim(times(1),/ecs),0,16)
;;p0 = max(where(goesp.time le t0))
;;p1 = min(where(goesp.time ge t1))
ddd = strmid(anytim(goesp.time,/ecs),0,16)
p0 = max([where(ddd le t0),0])                
p1 = min(where(ddd ge t1))                

print,'Index limits:',p0,p1
goesp_pdx = goesp(p0:p1)

;    now overplot he selected bands 

qq = where(goesp_pdx.proton(0) lt 3.27e4, nqq)		; 10 Mev
;help,nqq
if nqq gt 0 then outplot, goesp_pdx(qq).time, goesp_pdx(qq).proton(0), color=1

qq = where(goesp_pdx.proton(8) lt 3.27e4, nqq)		; 50 Mev
;help,nqq
if nqq gt 0 then outplot, goesp_pdx(qq).time, goesp_pdx(qq).proton(8), color=2

qq = where(goesp_pdx.proton(10) lt 3.27e4, nqq)		; 100 Mev
;help,nqq
if nqq gt 0 then outplot, goesp_pdx(qq).time, goesp_pdx(qq).proton(10), color=3

ut_start = 'Begin: '+strmid(anytim(times(0),/ecs),0,16)+' UT'
xyouts,10,5,/dev, ut_start, charsiz=1.2
xyouts,540,5,/dev, 'HELIO', charsiz=1.2		;???

end
