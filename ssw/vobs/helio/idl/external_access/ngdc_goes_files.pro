function goes_ngdc_url, tstart, tend, verbose=verbose

;    01-02-2012  rdb  Generalized month filter

message,'Starting',/info

times=[tstart,tend]
fmt_timer,times

root = 'http://satdat.ngdc.noaa.gov/sem/goes/data/avg/'
;;;;;;;root = 'http://satdat.ngdc.noaa.gov/sem/goes/data/new_avg/'

;;fid = time2fid(times,/full)		; yyyymmdd
fid = def_monfiles(times,/ngdc)		; mane sure cover all months
yyyy = strmid(fid,0,4)
yymm = strmid(fid,2,4)  


;    find the default mission for this interval
;;satid = goes_set_satnum(tstart,tend,prefix='Z')
;;;;satid = goes_set_satnum(tstart,tend,prefix='I')		; integrated

;
;    find the default mission for this interval
;    taken from  http://www.swpc.noaa.gov/ftpdir/lists/pchan/README
;
t0 = tstart  &  t1 = tend
sat = (['I08','I11'])(ssw_deltat(t1,ref='2003/06/19') gt 0)	; integrated
;;sat = ([ sat, 'I13'])(ssw_deltat(t0,ref='2010/04/14') gt 0)
sat = ([ sat, 'I11'])(ssw_deltat(t0,ref='2010/04/14') gt 0)

sat = (['I07','I08'])(ssw_deltat(t1,ref='1996/01/01') gt 0)	; integrated
sat = ([ sat, 'I11'])(ssw_deltat(t0,ref='2003/06/19') gt 0)
;sat = ([ sat, 'I10'])(ssw_deltat(t0,ref='2002/01/01') gt 0)
;sat = ([ sat, 'I11'])(ssw_deltat(t0,ref='2005/01/01') gt 0)

url = root + yyyy + '/' + sat + '5' + yymm + '.TXT'

url = all_vals(url)		; try to catch same month for start and end

if not keyword_set(debug) then return, url

;--------
nel = n_elements(url)
for j=0,nel-1 do begin
  curl = url(j)
  if keyword_set(verbose) then print, curl
  exist = sock_check(curl) 
  if exist then print,'Exists, size (MB): ',sock_size(curl,err=err)/1.e6  $
   else message,'>>>> file not found <<<<',/info
endfor

return, url

end               


pro ngdc_goes_files, trange, struct, verbose=verbose

;    29-02-2012  rdb  changed cjeck for existance of remote file

tstart = trange(0)
tend = trange(1)

;;tstart='19-jan-2005' & tend='03-feb-2005'

url = goes_ngdc_url(tstart, tend, verbose=verbose)

temp = goes_temp_dir()
if not is_dir(temp) then mk_dir,temp

break_file,url,aa,bb,cc,dd
out = temp+'/'+cc+dd		; need to check is already available...

message, 'Copying required file(s)', /info
nurl = n_elements(url)
for j=0,nurl-1 do begin

  curl = url(j)
  print, cc(j)+dd(j)	;curl

  if not file_exist(out(j)) then begin
    exist = sock_check(curl) 
    if exist then begin
;      print,'Exists, size (MB): ',sock_size(curl,err=err)/1.e6

      cmd = wget_copy(url(j),outdir=temp)		; should be o/s independent

      if keyword_set(verbose) then print,cmd
      spawn, cmd
      
    endif else message,'>>>> file not found <<<<',/info    
  endif else print,'File already exists: '+out(j)

endfor


struct = rd_ngdc_goesp(out)

help,struct
help,struct,/st


end