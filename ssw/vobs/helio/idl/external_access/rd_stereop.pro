function rd_impact_het, filename, verbose=verbose

print,'Reading: ',filename
qq = rd_tfile(filename,23,21,/auto)

sz = size(qq)
nrow = sz(2)

;;temp = {time_start:'', time_end:'', $
temp = {time:'', $
		elect:fltarr(3), elect_err:fltarr(3), $
		proton:fltarr(11), proton_err:fltarr(11)}
struct = replicate(temp,nrow)

dd = string(qq([3,2,1],*),format='(2(a,1h-),a)')
time_start = fmt_tim(fid2ex(time2fid(dd)+'.'+qq(4,*)))

;dd = string(qq([7,6,5],*),format='(2(a,1h-),a)')
;time_end = fmt_tim(fid2ex(time2fid(dd)+'.'+qq(8,*)))

struct.time = fmt_tim(addtime(time_start,del=7.5))

qq1 = reform(qq(9:*,*),2,14,nrow)

struct.elect = reform(qq1(0,indgen(3),*))
struct.elect_err = reform(qq1(1,indgen(3),*))

struct.proton = reform(qq1(0,indgen(11)+3,*))
struct.proton_err = reform(qq1(1,indgen(11)+3,*))

if keyword_set(verbose) then begin
  fmt_timer, struct.time
  help,struct,/st
endif

return, struct
end

pro caltech_stereop_files, times, het, mission=mission, force=force, verbose=verbose

;    23-01-2012  rdb  written
;    01-02-2012  rdb  Generalized month filter

;  http://www.srl.caltech.edu/STEREO/DATA/HET/Ahead/15minute/AeH06Dec.15m
;  B eH 06Dec .1m
;  A eH 06Dec .15m

fmt_timer, times

root = 'http://www.srl.caltech.edu/STEREO/DATA/HET/'

;  possible resolutions - 1 mn, 15 mn, 1hr, 12 hr, 24 hr
;  choose resolution suitable for time range????

cmiss = 'STA'
dir = 'Ahead/15minute/'
stx = 'AeH'

if keyword_set(mission) then begin
  cmiss = strupcase(mission)
  if cmiss ne 'STA' then cmiss='STB'
endif

if cmiss eq 'STB' then begin
  dir = 'Behind/15minute/'
  stx = 'BeH'
endif

goto,xxxcc
;;dstr0 = arr2str((str2arr(strmid(anytim(times(0),/yoh),0,9),delim='-'))([2,1]),delim='')
;;dstr1 = arr2str((str2arr(strmid(anytim(times(1),/yoh),0,9),delim='-'))([2,1]),delim='')
;;dstr = [dstr0,dstr1]
;;if dstr0 eq dstr1 then dstr = dstr0
tints = anytim2ints(times)
dys = indgen(max(tints.day)-min(tints.day)+1) + min(tints.day) 
ndys = n_elements(dys)

tx = anytim2ints(times)            
txx = replicate(tx(0),ndys)    
txx.day=dys
;help,txx,/st

ss = uniq(strmid(anytim(txx,/yoh),3,6))   
mons = (strmid(anytim(txx,/yoh),3,6))(ss)      
dstr = strmid(mons,4,2)+strmid(mons,0,3)

xxxcc:
dstr = def_monfiles(times,/caltech)

print,dstr
nfiles = n_elements(dstr)

;    output directory for the temporary files
odir = concat_dir(get_temp_dir(),'stereo')

message, 'Copying required file(s)', /info
print,'Local Directory: ',odir

if not is_dir(odir) then mk_dir, odir

for j=0,nfiles-1 do begin

  ff = stx + dstr(j) + '.15m'
  file = root + dir + ff
  print,'Searching for: ',ff		;file
  cmd = wget_copy(file, outdir=odir, file_uri=file_uri, verbose=verbose)
;  help, file_uri

;  note: need -N to force overwrite

  doit = 0
  fsiz = sock_size(file)					; size of remote file
  if fsiz eq 0 then begin
    print,'>>>> Remote file not found'
    continue
  endif

  if file_exist(file_uri) then begin
    lsiz = file_size(file_uri)				; size of local file
    print,'File exists locally', lsiz,fsiz
    if lsiz lt fsiz then begin
      print,'MISMATCH in file size - RECOPY'
      print,file
      doit=1
    endif
  endif else doit=1
  
  if doit then spawn,cmd 

  if not file_exist(file_uri) or file_stat(file_uri,/siz) eq 0 then begin
    print,'>>>> No data found: ',ff
    continue
  endif

  temp = rd_impact_het(file_uri)		;, /verb)
;  help, temp,/st
;  help, temp
  if j eq 0 then het = temp else het = [het, temp]
  help,het

endfor

if keyword_set(verbose) then begin
  fmt_timer, times
  utplot_io,het.time,het.proton(0), yra=[1.e-4,1e3],yst=1, timer=times,xst=1 
 outplot,het.time,het.proton(10)                                         
endif

end


function rd_stereop, times, cmiss, verbose=verbose, err=err

common omstereo, ace, acsta, acstp, cmiss_sav

common stx_xx2, sta_struct, sta_sta, sta_stp, stb_struct, stb_sta, stb_stp

;    only copy if data is not already loaded
;    then only copy if falls outside of what is already loaded
;????    extend interval copied so can shift slightly ????

rsta = time2fid(times(0),/full,/time)
rstp = time2fid(times(1),/full,/time)
;;help,rsta,rstp
print,'++ Dates requested: ', rsta, rstp, format='(a,t35,a,2x,a)'

if n_elements(ace) eq 0 then begin
  acsta='' & acstp=''
endif

if n_elements(sta_struct) eq 0 then begin
  sta_sta='' & sta_stp=''
endif

if n_elements(stb_struct) eq 0 then begin
  stb_sta='' & stb_stp=''
endif

;help, sta_struct, sta_sta, sta_stp, stb_struct, stb_sta, stb_stp

help, cmiss

doit = 0
if n_elements(cmiss_sav) gt 0 then begin
  message,'>>>> Matching saved information for '+cmiss,/info
  if cmiss eq 'STA' then begin
;;    help, sta_struct		;, sta_sta, sta_stp
    print,'++ Dates for ingested data: ', sta_sta, sta_stp, format='(a,t35,a,2x,a)'
    if n_elements(sta_struct) gt 0 then begin
      ace=sta_struct & acsta=sta_sta & acstp=sta_stp
    endif else doit=1
  endif
  if cmiss eq 'STB' then begin
;;    help, stb_struct		;, stb_sta, stb_stp
    print,'++ Dates for ingested data: ', stb_sta, stb_stp, format='(a,t35,a,2x,a)'
    if n_elements(stb_struct) gt 0 then begin
      ace=stb_struct & acsta=stb_sta & acstp=stb_stp
    endif else doit=1
  endif
endif

err = 0

;    need to also trap case where the mission has changed...
;doit = 0
;if n_elements(cmiss_sav) eq 0 then doit=1 else if cmiss_sav ne cmiss then doit=1

;help, rsta, rstp, acsta, acstp, doit

if (rsta lt acsta) or (rsta gt acstp) or (rstp gt acstp) or (rstp lt acsta) or doit then begin
  message,'>>>> Reading data for '+cmiss,/info

;;  ace = ucla_stereo_post(timerange=times, mission=cmiss, verbose=verbose)
  caltech_stereop_files, times, ace, mission=cmiss, verbose=verbose
  
  help,/st,ace
  if datatype(ace) ne 'STC' then begin
    message,'Problem reading STEREO Data',/info
    err = 1			; flag error to calling routine
    return, -1
  endif

  nel = n_elements(ace)
  acsta = time2fid(ace(0).time,/full,/time)
  acstp = time2fid(ace(nel-1).time,/full,/time)  ;; <<<<

  if cmiss eq 'STA' then begin		; STA
    sta_struct=ace & sta_sta=acsta & sta_stp=acstp
  endif else begin			; STB
    stb_struct=ace & stb_sta=acsta & stb_stp=acstp
  endelse

endif else message, 'Data is already loaded',/info

  cmiss_sav = cmiss
if keyword_set(verbose) then begin
  help, ace, acsta, acstp, cmiss_sav
endif

return, ace
end


