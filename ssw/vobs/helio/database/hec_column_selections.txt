# define which columns should be selected in displays of tables from the HEC. These form part of the query to the HEC.
goes_sxr_flare: time_start, time_peak, time_end, nar, lat_hg, long_hg, long_carr, xray_class, optical_class
gevloc_sxr_flare: time_start, time_peak, time_end, xray_class, lat_hg, long_hg, long_carr, nar
goes_proton_event: time_start, time_peak, proton_flux, cme_parameters, time_peak_flare, nar, lat_hg, long_hg, xray_class, optical_class
cactus_soho_cme: sat_id, r_hci, long_hci, lat_hci, time_start, duration, pa, pa_width, v, dv, v_min, v_max, flag_halo, cme_number
cactus_stereoa_cme: sat_id, r_hci, long_hci, lat_hci, time_start, duration, pa, pa_width, v, dv, v_min, v_max, flag_halo, cme_number
cactus_stereob_cme: sat_id, r_hci, long_hci, lat_hci, time_start, duration, pa, pa_width, v, dv, v_min, v_max, flag_halo, cme_number
cactus_all: sat_id, r_hci, long_hci, lat_hci, time_start, duration, pa, pa_width, v, dv, v_min, v_max, flag_halo, cme_number
