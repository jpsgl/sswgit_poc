function ssw_cosec_make_actors, service_file, defcgi=defcgi, $
   outdir=outdir, no_write=no_write, no_param_change=no_param_change, debug=debug
;+ 
;   Name: ssw_cosec_make_actors
;
;   Purpose: generate one or more cosec/ssw actor (moml) files from ascii table
;
;   Input Parameters:
;      service_file - ascii file, one line per service - 5 column, colon delimited of form:
;      <service>:<ssw_pnames>:<port_types>:<cosec_names>:<host_ip/cgi>
;      (see ssw_actor.pro for expected field formats)
;
;   Output:
;      function returns concatenated ascii/moml for all configured services
;
;   Keyword Parameters:
;      outdir - output directory for moml/ascii files (1 file per service)
;      defcgi - optional default hostip/cgi directory ; 
;               (only applied to services which did not specify a value in last column)
;               (default for defcgi='http://www.lmsal.com/cgi-diapason' , historical...)
;      no_param_change - historically, PARAM1 & PARAM2 are oft used paramter
;               names - for brevity/convenience this routine allows use of 'p1' and 'p2' synonyms
;               in the service configuration file; these are changed to the longer PARAMn names
;               prior to moml generation  - /NO_PARAM_CHANGE will override that mapping
;
; Sample line in 'service_file' for example:       
; ssw_service_plot_goes_xrays:p1,p2,extra,p1:i,i,i,o:startT,stopT,extra,graphic:penumbra.nascom.nasa.gov
;
;   Side Effects:
;     By default, one ascii/moml file is written to OUTDIR per service line;
;     You may override this with /NO_WRITE keyword
;
;   History:
;      29-Nov-2006 - S.L.Freeland - step in mass production ssw->cosec service pipeline I believe
;
;-

if n_elements(service_file) eq 0 then service_file='allservices.dat'
if not file_exist(service_file) then begin 
  box_message,'Cannot find service data file: ' + service_file(0)
  return,''
endif

sdat=rd_tfile(service_file(0))
ss=where(strpos(sdat,':') ne -1,okcnt)
if okcnt eq 0 then begin 
   box_message,'No services to configure...'
   return,''
endif

ns=okcnt
box_message,'Configuring ' + strtrim(ns,2) + ' services...'

if n_elements(defcgi) eq 0 then defcgi='http://www.lmsal.com/cgi-diapason/' ; historical default

sdat=strtrim(strcompress(sdat(ss),/remove),2)
cols=str2cols(sdat,':',/unaligned)
strtab2vect,cols,services,ssw_pnames,port_types,cosec_pnames,hostip_cgi
if 1-keyword_set(no_param_change) then begin ; default p1->param1 & p2->param2
   ssw_pnames=','+ssw_pnames+','  ; simplify check/parse
   ssw_pnames=str_replace(ssw_pnames,',p1,',',param1,')
   ssw_pnames=str_replace(ssw_pnames,',p2,',',param2,')
   ssw_pnames=strmids(ssw_pnames,1,strlen(ssw_pnames)-2) ; eliminate leading/trailing ','
endif
usedef=where(hostip_cgi eq '',ucnt)
if ucnt gt 0 then begin 
   box_message,['No host/cgi specified for ' + strtrim(ucnt,2) + ' services', $
      'Using default> ' + defcgi + ' for...', services(usedef)]
   hostip_cgi(usedef)=defcgi
endif 
ssnocgi=where(strpos(hostip_cgi,'cgi') eq -1, ncnt)
if ncnt gt 0 then hostip_cgi(ssnocgi)=concat_dir(hostip_cgi(ssnocgi),'cgi-bin') ; def->cgi-bin
hostip_cgi=(['','http://'])(strpos(hostip_cgi,'http://') eq -1) + hostip_cgi
service_urls=concat_dir(hostip_cgi,services)

if n_elements(outdir) eq 0 then outdir=curdir()
writeit=1-keyword_set(no_write)  ; default writes one moml/ascii file per service

onames=str_replace(services,'.sh','.xml')
retval=''
for i=0,ns -1 do begin  ; for each service/config...
   moml=ssw_actor(ssw_pnames(i),port_types(i),cosec_pnames(i),service=service_urls(i))
   retval=[temporary(retval),moml]
   if writeit then file_append,onames(i),moml,/new
endfor
if n_elements(retval) gt 1 then retval=temporary(retval(1:*))

return,retval
end
